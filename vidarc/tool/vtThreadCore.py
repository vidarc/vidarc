#----------------------------------------------------------------------------
# Name:         vtThreadCore.py
# Purpose:      basic threading interface
#               based on vtThread.py
# Author:       Walter Obweger
#
# Created:      20060724
# CVS-ID:       $Id: vtThreadCore.py,v 1.26 2011/08/26 09:36:46 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import Queue
import thread,threading,traceback
import sys as _sys
import copy
import atexit
import pprint

pp=pprint.PrettyPrinter(indent=2)

# Support for profile and trace hooks

#_profile_hook = threading._profile_hook     #None
#_trace_hook = threading._trace_hook         #None
import vidarc
#import vidarc.config.vcLog as vcLog
VERBOSE=0#vcLog.is2Log(__name__,'__VERBOSE__')

def getLock():
    return thread.allocate_lock()

def getRLock():
    return threading.RLock()

def setprofile(func):
    #global _profile_hook
    threading._profile_hook = func

def settrace(func):
    #global _trace_hook
    threading._trace_hook = func

_dActThd={}
_iActNum=0
_iActCount=0
_semThd=threading.Lock()#threading.Semaphore()
def _acquireThd():
    global _semThd
    _semThd.acquire()
def _releaseThd():
    global _semThd
    _semThd.release()

def addActiveThread(thd):
    _acquireThd()
    try:
        global _iActCount
        global _iActNum
        global _dActThd
        ident=thread.get_ident()
        thd.SetNum(_iActNum)
        thd.SetIdent(ident)
        _iActCount+=1
        _dActThd[ident]=thd
        _iActNum+=1
        #print _dActThd
    except:
        #import traceback
        #traceback.print_exc()
        pass
    _releaseThd()
#def delActiveThread(thd):
def delActiveThread(ident):
    _acquireThd()
    try:
        global _iActCount
        global _dActThd
        #print _dActThd
        _iActCount-=1
        if ident in _dActThd:
            del _dActThd[ident]
        #print _dActThd
    except:
        #import traceback
        #traceback.print_exc()
        pass
    _releaseThd()

def stopThreads():
    _acquireThd()
    try:
        for k,thd in _dActThd.iteritems():
            try:
                thd.Stop()
            except:
                pass
    except:
        import traceback
        traceback.print_exc()
    _releaseThd()
    iTimeOut=600
    q=Queue.Queue()
    while iTimeOut>0:
        _acquireThd()
        try:
            if len(_dActThd)==0:
                iTimeOut=0
                q.put(0)
            #else:
            #    for ident,thd in _dActThd.iteritems():
            #        print ident,thd.ident,thd.GetOrigin(),thd
            #    print
        except:
            pass
        _releaseThd()
        try:
            q.get(block=True,timeout=0.1)
            break
        except:
            pass
        iTimeOut-=1
    
#vidarc.AddCleanUp(stopThreads)     # 080205:wro don't clean threads
#atexit.register(stopThreads)

def getActiveThreads():
    _acquireThd()
    try:
        global _dActThd
        d=copy.copy(_dActThd)
    except:
        d={}
        traceback.print_exc()
    _releaseThd()
    return d

class vtThreadCore:
    TIME_2_SLEEP_PAUSED=0.5
    def __init__(self,bPost=False,verbose=0,origin=None):
        self.verbose=verbose
        if self.verbose<0:
            self.verbose=0
        self.iThdNum=-1
        self.ident=0
        self.__origin__(origin)
        self.origin=self.originRaw
        self.Clear()
        self.keepGoing=True
        self.running=False
        self.pause=False
        self.paused=False
        #self.semThd=threading.Semaphore()
        self.semThd=getLock()
        self.qSched=Queue.Queue()
        self.qWakeUp=Queue.Queue()
        #self.__runThread__=thread.start_new_thread
    def __del__(self):
        self.__clrSched__()
        self.__clrWakeUp__()
        try:
            del self.semThd
            del self.qSched
            del self.qWakeUp
            del self.originRaw
            del self.origin
        except:
            traceback.print_stack()
            traceback.print_exc()
    def __clrSched__(self):
        try:
            while self.qSched.empty()==False:
                self.qSched.get()
        except:
            pass
    def __clrWakeUp__(self):
        try:
            while self.qWakeUp.empty()==False:
                self.qWakeUp.get()
        except:
            pass
    def __logDebug__(self,msg='',verbose=0):
        try:
            if verbose>0:
                traceback.print_stack(file=_sys.stderr)
                _sys.stderr.write(self.GetOrigin())
                _sys.stderr.write(msg.replace(';','\n'))
                _sys.stderr.write('\n')
        except:
            pass
    def __logInfo__(self,msg='',verbose=0):
        try:
            if verbose>0:
                traceback.print_stack(file=_sys.stderr)
                _sys.stderr.write(self.GetOrigin())
                _sys.stderr.write(msg.replace(';','\n'))
                _sys.stderr.write('\n')
        except:
            pass
    def __logError__(self,msg=''):
        try:
            traceback.print_stack(file=_sys.stderr)
            _sys.stderr.write(self.GetOrigin())
            _sys.stderr.write(msg)
        except:
            pass
    def __logFmt__(self,l):
        try:
            global pp
            return pp.pformat(l)
        except:
            return ''
    def __logTB__(self,bLogAsDbg=False):
        try:
            traceback.print_exc(file=_sys.stderr)
            _sys.stderr.write(self.GetOrigin())
        except:
            pass
    def __origin__(self,origin):
        if origin is None:
            self.originRaw=self.__class__.__name__
        else:
            self.originRaw=origin
    def __repr__(self):
        return self.origin
    def __str__(self):
        self._acquire()
        s=''
        try:
            s=','.join(['%s:%d'%(a,getattr(self,a)) for a in ['running','keepGoing','pause','paused']]+['empty:%d'%self.qSched.empty()])
        finally:
            self._release()
        return s
    def SetIdent(self,ident):
        self.ident=str(ident)
    def SetNum(self,i):
        self.iThdNum=i
        self.origin=':'.join([self.originRaw,str(self.iThdNum)])
    def GetNum(self):
        return self.iThdNum
    def SetOrigin(self,origin):
        self._acquire()
        try:
            self.__origin__(origin)
            if self.iThdNum>=0:
                self.SetNum(self.iThdNum)
            else:
                self.origin=self.originRaw
        except:
            pass
        self._release()
    def GetOrigin(self):
        return self.origin
    def Clear(self):
        pass
    def _acquire(self):
        self.semThd.acquire()
    def _release(self):
        self.semThd.release()
    def Do(self,func,*args,**kwargs):
        self._acquire()
        if self.keepGoing==True:
            if self.running:
        #if self.running:
        #    if self.keepGoing==True:
                self.qSched.put((func,args,kwargs))
                self._release()
                return
            #self._release()
            #return
        else:
            self._release()
            self.__logError__('thread is stopped')
            return
            
        self.keepGoing=self.running=True
        self._release()
        self.qSched.put((func,args,kwargs))
        self.__runThread__()
    def __runThread__(self):
        thread.start_new_thread(self.__run__,())
    def Pause(self):
        if VERBOSE>=5:
            self.__logInfo__()
        self._acquire()
        self.pause = True
        self.qWakeUp.put(0)
        self._release()
    def Resume(self):
        if VERBOSE>=5:
            self.__logInfo__()
        self._acquire()
        self.pause = False
        self.qWakeUp.put(1)
        self._release()
    def IsPause(self):
        self._acquire()
        bRet=self.pause
        self._release()
        return bRet
    def __Paused__(self,flag):
        self._acquire()
        self.paused = flag
        self._release()
    def IsPaused(self):
        self._acquire()
        bRet=self.paused
        self._release()
        return bRet
    def Start(self):
        if VERBOSE>=5:
            self.__logInfo__()
        self._acquire()
        self.keepGoing = True
        self.qWakeUp.put(-1)
        self._release()
    def Stop(self):
        if VERBOSE>=5:
            self.__logInfo__()
        self._acquire()
        self.keepGoing = False
        self.qWakeUp.put(-1)
        self._release()
    def Is2Stop(self):
        self._acquire()
        bRet=self.keepGoing==False
        self._release()
        return bRet
    def IsRunning(self):
        self._acquire()
        bRet=self.running
        self._release()
        return bRet
    def IsBusy(self):
        self._acquire()
        bRet=self.running
        if bRet:
            if self.paused:
                bRet=False
        self._release()
        return bRet
    def IsEmpty(self):
        return self.qSched.empty()
    def SetValue(self,attr,v):
        try:
            self._acquire()
            self.__dict__[attr]=v
        finally:
            self._release()
    def GetValue(self,attr):
        try:
            self._acquire()
            return self.__dict__[attr]
        finally:
            self._release()
    def AddValue(self,attr,v):
        try:
            self._acquire()
            self.__dict__[attr]=self.__dict__[attr]+v
            return self.__dict__[attr]
        finally:
            self._release()
    def __runStart__(self):
        if threading._trace_hook:
            _sys.settrace(threading._trace_hook)
        if threading._profile_hook:
            _sys.setprofile(threading._profile_hook)
        addActiveThread(self)
    def __runEnd__(self):
        try:
            delActiveThread(int(self.ident))
        except:
            pass
        #delActiveThread(self)
        self.ident=''
    def __pauseSleep__(self):
        try:
            self.qWakeUp.get(block=True,timeout=self.TIME_2_SLEEP_PAUSED)
        except:
            pass
    def __doPause__(self):
        """ 
        return True         ... stop processing
        return False        ... continue processing
        """
        try:
            if self.Is2Stop():
                return True
            if self.IsPause():
                self.__Paused__(True)
                while self.IsPause():
                    self.__pauseSleep__()
                    if self.Is2Stop():
                        self.__Paused__(False)
                        return True
                self.__Paused__(False)
                if self.Is2Stop():
                    return True
        except:
            traceback.print_exc()
        return False
    def _doProc(self,t):
        try:
            func,args,kwargs=t
            if func is not None:
                func(*args,**kwargs)
        except:
            traceback.print_exc()
    def __run__(self):
        #self.running=True
        try:
            self.__runStart__()
            try:
                #while self.keepGoing:# and (self.qSched.empty()==False):
                while self.Is2Stop()==False:
                    if self.__doPause__()==True:
                        break
                    else:
                        t=self.qSched.get(0)
                        self._doProc(t)
                self._acquire()
                self.running=False   # 070630:wro
                if self.keepGoing==False:
                    self.__clrSched__()
            except:
                self._acquire()
                self.running=False   # 070620:wro
        except:
            self._acquire()
            self.running=False    # 070620:wro
            traceback.print_exc()
        self.__runEnd__()
        self.__clrWakeUp__()
        #self.keepGoing=True
        self.running=False   # 070620:wro activated
        self._release()
    def getLock(self):
        return getLock()
    
    def CallBack(self,func,*args,**kwargs):
        try:
            func(*args,**kwargs)
        except:
            self.__logTB__()
    def CallBackDelayed(self,zSleep,func,*args,**kwargs):
        try:
            self.__logInfo__()
            func(*args,**kwargs)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def DoCallBack(self,func,*args,**kwargs):
        self.Do(self.CallBack,func,*args,**kwargs)
    DoCB=DoCallBack
