#Boa:Dialog:vtmCompareResultsSettingsDialog
#----------------------------------------------------------------------------
# Name:         vtmCompare.py
# Purpose:      core class to provide a standard compare interface
# Author:       Walter Obweger
#
# Created:      20070515
# CVS-ID:       $Id: vtmCompareResultsSettingsDialog.py,v 1.1 2007/05/23 13:01:32 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog

def create(parent):
    return vtmCompareResultsSettingsDialog(parent)

[wxID_VTMCOMPARERESULTSSETTINGSDIALOG, 
 wxID_VTMCOMPARERESULTSSETTINGSDIALOGCBAPPLY, 
 wxID_VTMCOMPARERESULTSSETTINGSDIALOGCBCANCEL, 
 wxID_VTMCOMPARERESULTSSETTINGSDIALOGCLSTRESULTS, 
 wxID_VTMCOMPARERESULTSSETTINGSDIALOGLBLSHOW, 
] = [wx.NewId() for _init_ctrls in range(5)]

class vtmCompareResultsSettingsDialog(wx.Dialog):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)
        parent.AddGrowableCol(0)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblShow, 1, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT | wx.TOP)
        parent.AddWindow(self.clstResults, 0, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.ALIGN_CENTER | wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.TOP)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=4, rows=4, vgap=4)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTMCOMPARERESULTSSETTINGSDIALOG,
              name=u'vtmCompareResultsSettingsDialog', parent=prnt,
              pos=wx.Point(66, 66), size=wx.Size(400, 232),
              style=wx.DEFAULT_DIALOG_STYLE,
              title=_(u'vtmCompare Results Settings Dialog'))
        self.SetClientSize(wx.Size(392, 205))

        self.lblShow = wx.StaticText(id=wxID_VTMCOMPARERESULTSSETTINGSDIALOGLBLSHOW,
              label=_(u'show results'), name=u'lblShow', parent=self,
              pos=wx.Point(4, 4), size=wx.Size(384, 13), style=0)
        self.lblShow.SetMinSize(wx.Size(-1, -1))

        self.clstResults = wx.CheckListBox(choices=[],
              id=wxID_VTMCOMPARERESULTSSETTINGSDIALOGCLSTRESULTS,
              name=u'clstResults', parent=self, pos=wx.Point(4, 21),
              size=wx.Size(384, 130), style=0)
        self.clstResults.SetMinSize(wx.Size(-1, -1))

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTMCOMPARERESULTSSETTINGSDIALOGCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'), name=u'cbApply',
              parent=self, pos=wx.Point(104, 171), size=wx.Size(76, 30),
              style=0)
        self.cbApply.SetMinSize(wx.Size(-1, -1))
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VTMCOMPARERESULTSSETTINGSDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTMCOMPARERESULTSSETTINGSDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'), name=u'cbCancel',
              parent=self, pos=wx.Point(212, 171), size=wx.Size(76, 30),
              style=0)
        self.cbCancel.SetMinSize(wx.Size(-1, -1))
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VTMCOMPARERESULTSSETTINGSDIALOGCBCANCEL)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vtMisc')
        self._init_ctrls(parent)
        self.lShown=[]
    def SetPossibleResult4DispLst(self,lMap):
        self.clstResults.Clear()
        i=0
        for t in lMap:
            self.clstResults.Append(t[1])
            self.clstResults.Check(i)
            self.lShown.append(t[0])
            i+=1
    def GetShownResults(self):
        d={}
        iCount=self.clstResults.GetCount()
        for i in xrange(iCount):
            if self.clstResults.IsChecked(i):
                d[self.lShown[i]]=True
        return d
    def OnCbApplyButton(self, event):
        event.Skip()
        self.EndModal(1)
    def OnCbCancelButton(self, event):
        event.Skip()
        self.EndModal(0)
