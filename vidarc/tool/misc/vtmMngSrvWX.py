#----------------------------------------------------------------------------
# Name:         vtmMngSrvWX.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20091114
# CVS-ID:       $Id: vtmMngSrvWX.py,v 1.1 2009/11/14 22:59:19 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vtmMngWX import vtmMngWX
from vtmMngSrv import vtmMngSrv

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

class vtmMngSrvWX(vtmMngWX,vtmMngSrv):
    def __init__(self,*args,**kwargs):
        vtmMngWX.__init__(self,*args,**kwargs)
        vtmMngSrv.__init__(self,*args,**kwargs)
        vtmMngWX.__setupImageList__(self,kwargs.get('bSmall',True))
