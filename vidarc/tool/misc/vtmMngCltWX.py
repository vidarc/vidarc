#----------------------------------------------------------------------------
# Name:         vtmMngCltWX.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20091114
# CVS-ID:       $Id: vtmMngCltWX.py,v 1.1 2009/11/14 22:59:19 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vtmMngWX import vtmMngWX
from vtmMngClt import vtmMngClt

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

class vtmMngCltWX(vtmMngWX,vtmMngClt):
    def __init__(self,*args,**kwargs):
        vtmMngWX.__init__(self,*args,**kwargs)
        vtmMngClt.__init__(self,*args,**kwargs)
        vtmMngWX.__setupImageList__(self,kwargs.get('bSmall',True))
