#----------------------------------------------------------------------------
# Name:         vtmDrop.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20091030
# CVS-ID:       $Id: vtmDrag.py,v 1.1 2009/11/04 17:52:13 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from wx import TextDropTarget

class vtmDropTargetText(TextDropTarget):
    def __init__(self,func,*args,**kwargs):
        TextDropTarget.__init__(self)
        self.func=func
        self.args=args
        self.kwargs=kwargs
    def OnDropText(self,x,y,s):
        self.func(s,*self.args,**self.kwargs)
