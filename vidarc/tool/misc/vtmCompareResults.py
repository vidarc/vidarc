#Boa:FramePanel:vtmCompareResults
#----------------------------------------------------------------------------
# Name:         vtmCompareResults.py
# Purpose:      core class to provide a standard compare interface
# Author:       Walter Obweger
#
# Created:      20070515
# CVS-ID:       $Id: vtmCompareResults.py,v 1.4 2007/07/29 09:56:50 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import wx.gizmos

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.misc.vtmCompareItemViewDialog import vtmCompareItemViewDialog
from vidarc.tool.misc.vtmCompareResultsSettingsDialog import vtmCompareResultsSettingsDialog

[wxID_VTMCOMPARERESULTS, wxID_VTMCOMPARERESULTSCBACTION, 
 wxID_VTMCOMPARERESULTSCBCFG, wxID_VTMCOMPARERESULTSCBSHOWX, 
 wxID_VTMCOMPARERESULTSCBSHOWY, wxID_VTMCOMPARERESULTSCHCACTION, 
 wxID_VTMCOMPARERESULTSLBLACTION, wxID_VTMCOMPARERESULTSTRLSTCMP, 
] = [wx.NewId() for _init_ctrls in range(8)]

from wx import ImageFromStream, BitmapFromImage
from wx import EmptyIcon
import cStringIO

def getApplyAttrData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xabIDAT8\x8d\xa5\x93\xd1\x15\xc2 \x0cEo\xb0St\x00\xe7`\x88\xae\xa3\
\x9e\xb6+t\x0c\x86\xc84\xae\x81\x1f5\x9eX[@}\x9f\x90\xfb\xf2\x92\x03"\xe1\
\x84\xe9v\xbdd\x80\x18#{RU\x00\xc6i\x16;\xeb<|\x04\x9a\xdc}6\x93\xd0\n\x9b\
\x86\xfb\xc0r^^i\xbboa\x80\xd4\'\xe8\xd7$\xa1\x89\xdc\xc2N\xa2\xaa\xf9W\x18\
\x9e;h\xd1\x1e\xdcd`\xdd\x8f\x14\xac\xa8VX4\xf0\xdd\xbcQi\xf67\x83\xd4\xa7bQ\
1\x81=O\xdf\xa9u\x1cU\xfd\\\xe2\xd6\xa4\x96,\x8c\xd3,>E\xabT\x95q\x9a%\xc0\
\xfa\xbb\xf6F\xa9\xc1\x00\xf2\xefw~\x00ZIYh6\x8e~k\x00\x00\x00\x00IEND\xaeB`\
\x82' 

def getApplyAttrBitmap():
    return BitmapFromImage(getApplyAttrImage())

def getApplyAttrImage():
    stream = cStringIO.StringIO(getApplyAttrData())
    return ImageFromStream(stream)

#----------------------------------------------------------------------
def getSearchData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xe8IDAT8\x8d\x9d\xd2\xdfkRq\x18\xc7\xf1\xf7\xf9\x91:5G\x1e\xa7\xd5\
\xc9\xa3E\xb2IDFgcX\xecbw]4\x82(\xa2\xbb\xeab\xff@t\xd1\xd5v\xd1\xf5\xfe\x84\
.\x1b\xfd\x05]\xc5\xe8baQ\xb3YN\x89\xad\x8d\xc2\xa1\xe0A\x8fGpj\xfbv1\xaa-\
\x95\xa4\x0f<W\x0f\xbcx\xbe\xdf\xe7A\x92\x15\x0e\x97y9%|\xb2Ohc\x9a\x88\x1aQ\
\xf1w\xbf\xa7$Y\x01 \x1c\x0e\x89\xf1\x89q\x12\xf1K$\xa7&\x90\xed=2\xf9\x0c\
\xc5OE>fs\x12\x03\xa2\x02\xc4c\x86\x88\x18:\xf7\x1f\xce3;\x95\xa6\xd1mbY\x16\
\xa9\xab\xd3|x\xb7\xca^{Y\x14\xf2k}\x11\x15\xc0-\xdc\xcc]\x9f\xe3\xdaL\x1a\
\xaf\xd7Ovu\x9d\xf2n\x19\xa1\xee\xa3\x9f\x8a`^\xb8\xc8\xe8\x88Gd\xde\xbf\xe9\
Ad\x80\xd3\x89s\xe8\xd1(~\x97\x87J\xa9D1\xb7I\xab\xd3\xa2\xdb\x94\xa8\xd6\
\xeaLN\x9b|\xd9\xde\x18\xfc\x84\xae\xe5Po\xd4\x01\xf0\xfa\xfd\xe8\xe7u\xb4\
\x13\x1a\x01\xaf\x0b\xa7\xd5\xa2\xd3\xeeP\xadV\x91Pz\x00\x19\xc0\x1b\x18\xc5\
\xde\xada[6\x81`\x00\xf3\x8a\x89q\xe6$\xc1p\x18#n\xf0ug\x87d29x\x82\xcf\x85,\
c\xb10[\xdb\xdf\xf0\x07\x03\x08\xb5\xc3~[E\x95eT\xd7\x08/\x9e/\xe3T\x9d\xbe\
\x80\x0c\xf0\xbd\\\x92VV^\xb3\x96[\xa7i7\x90\xdb]\x8e{\xdc\xd8\x8e\xc3\xbd\
\xbb\xb7\xd1c\tR\x93f_\xe0\xf7\x1d\x00\x04\xb5\x90H\xa7fp\x85\\\xd0Q\xc9\x17\
\xde\xe2;\xe6#\x968\xcb\xcd\x1bw\xa8lex\xb4\xb8tt\x13\xff\xbc4Y\x01\t1\x7f+-\
63/\xc5\xd2\xe2cq\xb47\x04\xf0\x0bYx0{\x80<\xfd\x83\x0c\r\xf4"O\x84$+\x07[\
\x186\x92\xa4H\x0b\xcf^\t\x80\x8dF\xb7\xf7\x13\x87\x8d\x10?DD\xd3\xa8X5\xe9\
\xbf\x80\xc3\xf9\t\xee-\xaf\xfe\xf5.\xce.\x00\x00\x00\x00IEND\xaeB`\x82' 

def getSearchBitmap():
    return BitmapFromImage(getSearchImage())

def getSearchImage():
    stream = cStringIO.StringIO(getSearchData())
    return ImageFromStream(stream)


class vtmCompareResults(wx.Panel):
    REL_SIZE_MAIN_COLUMN=0.3
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbShowX, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbShowY, 0, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbCfg, 0, border=4, flag=wx.TOP | wx.EXPAND)

    def _init_coll_bxsAction_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblAction, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.chcAction, 4, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbAction, 0, border=4, flag=wx.LEFT)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.trlstCmp, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsBt, 0, border=0, flag=0)
        parent.AddSizer(self.bxsAction, 1, border=2, flag=wx.TOP | wx.EXPAND)

    def _init_coll_trlstCmp_Columns(self, parent):
        # generated method, don't edit

        parent.AddColumn(text=_(u'key'))

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=2, hgap=4, rows=1, vgap=4)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsAction = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsAction_Items(self.bxsAction)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTMCOMPARERESULTS,
              name=u'vtCompareResults', parent=prnt, pos=wx.Point(88, 88),
              size=wx.Size(379, 169), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(371, 142))

        self.trlstCmp = wx.gizmos.TreeListCtrl(id=wxID_VTMCOMPARERESULTSTRLSTCMP,
              name=u'trlstCmp', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(291, 106), style=wx.TR_MULTIPLE | wx.TR_HAS_BUTTONS)
        self._init_coll_trlstCmp_Columns(self.trlstCmp)
        self.trlstCmp.Bind(wx.EVT_TREE_ITEM_RIGHT_CLICK,
              self.OnTrlstCmpRightDown)
        self.trlstCmp.Bind(wx.EVT_TREE_SEL_CHANGED, self.OnTrlstCmpSelChanged)
        self.trlstCmp.Bind(wx.EVT_SIZE, self.OnTrlstCmpSize)

        self.cbShowX = wx.lib.buttons.GenBitmapTextToggleButton(ID=wxID_VTMCOMPARERESULTSCBSHOWX,
              bitmap=wx.ArtProvider.GetBitmap(wx.ART_FIND, size=(16, 16)),
              label=u'x', name=u'cbShowX', parent=self, pos=wx.Point(295, 0),
              size=wx.Size(76, 29), style=0)
        self.cbShowX.SetMinSize(wx.Size(-1, -1))
        self.cbShowX.Bind(wx.EVT_BUTTON, self.OnCbShowXButton,
              id=wxID_VTMCOMPARERESULTSCBSHOWX)

        self.cbShowY = wx.lib.buttons.GenBitmapTextToggleButton(ID=wxID_VTMCOMPARERESULTSCBSHOWY,
              bitmap=wx.ArtProvider.GetBitmap(wx.ART_FIND, size=(16, 16)),
              label=u'y', name=u'cbShowY', parent=self, pos=wx.Point(295, 33),
              size=wx.Size(76, 29), style=0)
        self.cbShowY.SetMinSize(wx.Size(-1, -1))
        self.cbShowY.Bind(wx.EVT_BUTTON, self.OnCbShowYButton,
              id=wxID_VTMCOMPARERESULTSCBSHOWY)

        self.lblAction = wx.StaticText(id=wxID_VTMCOMPARERESULTSLBLACTION,
              label=_(u'action'), name=u'lblAction', parent=self,
              pos=wx.Point(0, 112), size=wx.Size(51, 30), style=wx.ALIGN_RIGHT)
        self.lblAction.SetMinSize(wx.Size(-1, -1))

        self.chcAction = wx.Choice(choices=[],
              id=wxID_VTMCOMPARERESULTSCHCACTION, name=u'chcAction',
              parent=self, pos=wx.Point(55, 112), size=wx.Size(200, 21),
              style=0)
        self.chcAction.SetMinSize(wx.Size(-1, -1))

        self.cbAction = wx.lib.buttons.GenBitmapButton(ID=wxID_VTMCOMPARERESULTSCBACTION,
              bitmap=vtArt.getBitmap(vtArt.Apply), name=u'cbAction',
              parent=self, pos=wx.Point(259, 112), size=wx.Size(31, 30),
              style=0)
        self.cbAction.SetMinSize(wx.Size(-1, -1))
        self.cbAction.Bind(wx.EVT_BUTTON, self.OnCbActionButton,
              id=wxID_VTMCOMPARERESULTSCBACTION)

        self.cbCfg = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTMCOMPARERESULTSCBCFG,
              bitmap=vtArt.getBitmap(vtArt.Settings), label=_(u'Config'),
              name=u'cbCfg', parent=self, pos=wx.Point(295, 74),
              size=wx.Size(76, 30), style=0)
        self.cbCfg.SetMinSize(wx.Size(-1, -1))
        self.cbCfg.Bind(wx.EVT_BUTTON, self.OnCbCfgButton,
              id=wxID_VTMCOMPARERESULTSCBCFG)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vtMisc')
        self._init_ctrls(parent)
        self.trlstCmp.SetMainColumn(0)
        self.imgLstTyp=wx.ImageList(16,16)
        self.imgDict={}
        img=wx.ArtProvider.GetBitmap(wx.ART_TICK_MARK, size=(16, 16))
        self.imgDict['def']=self.imgLstTyp.Add(img)
        img=wx.ArtProvider.GetBitmap(wx.ART_QUESTION, size=(16, 16))
        self.imgDict['quest']=self.imgLstTyp.Add(img)
        img=wx.ArtProvider.GetBitmap(wx.ART_WARNING, size=(16, 16))
        self.imgDict['warn']=self.imgLstTyp.Add(img)
        img=wx.ArtProvider.GetBitmap(wx.ART_INFORMATION, size=(16, 16))
        self.imgDict['info']=self.imgLstTyp.Add(img)
        img=wx.ArtProvider.GetBitmap(wx.ART_ERROR, size=(16, 16))
        self.imgDict['error']=self.imgLstTyp.Add(img)
        img=vtArt.getBitmap(vtArt.Invisible)
        self.imgDict['empty']=self.imgLstTyp.Add(img)
        self.trlstCmp.SetImageList(self.imgLstTyp)
        self.lColumn=None
        self.lColumnItem=None
        self.dAction=None
        self.iMainColumnSize=None
        self.dlgShowX=None
        self.bShowX=False
        self.funcShowX=None
        self.argsShowX=None
        self.kwargsShowX=None
        self.dlgShowY=None
        self.bShowY=False
        self.funcShowY=None
        self.argsShowY=None
        self.kwargsShowY=None
        self.selectedTi=None
        self.selectedKey=None
        self.dlgCfg=None
        self.oCmp=None
        self.lMap=None
        self.dShownResults=None
        self.dTi={}
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        try:
            del self.dAction
            del self.lColumn
            del self.lColumnItem
            del self.lMap
            del self.dShownResults
            del self.dTi
        except:
            #vtLog.vtLngTB('del')
            pass
    def SetToolTip(self,objName,sToolTip):
        if hasattr(self,objName):
            getattr(self,objName).SetToolTip(sToolTip)
    def SetToolTipX(self,sToolTip):
        self.cbShowX.SetToolTip(wx.ToolTip(sToolTip))
    def SetToolTipY(self,sToolTip):
        self.cbShowY.SetToolTip(wx.ToolTip(sToolTip))
    def SetShowPanelX(self,clsPanel,namePanel,sTitle,funcShow,*args,**kwargs):
        self.funcShowX=funcShow
        self.argsShowX=args
        self.kwargsShowX=kwargs
        self.dlgShowX=vtmCompareItemViewDialog(self,clsPanel,namePanel,sTitle)
        #self.dlgShowX.Centre()
        wx.EVT_CLOSE(self.dlgShowX,self.OnCloseShowX)
    def SetShowPanelY(self,clsPanel,namePanel,sTitle,funcShow,*args,**kwargs):
        self.funcShowY=funcShow
        self.argsShowY=args
        self.kwargsShowY=kwargs
        self.dlgShowY=vtmCompareItemViewDialog(self,clsPanel,namePanel,sTitle)
        #self.dlgShowY.Centre()
        wx.EVT_CLOSE(self.dlgShowY,self.OnCloseShowY)
    def GetPanelX(self):
        if self.dlgShowX is not None:
            return self.dlgShowX.GetPanel()
        else:
            return None
    def GetPanelY(self):
        if self.dlgShowY is not None:
            return self.dlgShowY.GetPanel()
        else:
            return None
    def __addColumns__(self,l):
        try:
            if l is None:
                return
            for t in l:
                self.trlstCmp.AddColumn(t[0])
        except:
            vtLog.vtLngTB(self.GetName())
    def SetCompareObj(self,oCmp):
        del self.lMap
        del self.dTi
        self.dTi={}
        del self.dAction
        self.dShownResults=None
        self.trlstCmp.DeleteAllItems()
        iCount=self.trlstCmp.GetColumnCount()
        #wx.MutexGuiEnter()
        for i in xrange(1,iCount):
            self.trlstCmp.RemoveColumn(i)
        try:
            # setup tree
            self.oCmp=oCmp
            self.lMap=self.oCmp.GetPossibleResult4DispLst()
            self.lColumn=self.oCmp.GetColumns4DispLst()
            self.__addColumns__(self.lColumn)
            oCmpItem=self.oCmp.GetCompareItem()
            if oCmpItem is not None:
                lMap=oCmpItem.GetPossibleResult4DispLst()
                d={}
                self.imgDict['resultsItems']=d
                for t in lMap:
                    try:
                        d[t[0]]=self.imgLstTyp.Add(t[2])
                        img=d[t[0]]
                    except:
                        pass
            self.lColumnItem=oCmpItem.GetColumns4DispLst()
            self.__addColumns__(self.lColumnItem)
            # build general results
            d={}
            self.imgDict['results']=d
            self.tiRoot=self.trlstCmp.AddRoot(_(u'results'))
            ti=self.tiRoot
            for t in self.lMap:
                try:
                    d[t[0]]=self.imgLstTyp.Add(t[2])
                    img=d[t[0]]
                except:
                    img=self.imgDict['empty']
                tic=self.trlstCmp.AppendItem(ti,t[1],img,img)
                self.dTi[t[0]]=tic
            self.__calcColumnSizes__()
            self.UpdateResult()
            self.trlstCmp.Expand(self.tiRoot)
        except:
            vtLog.vtLngTB(self.GetName())
        #wx.MutexGuiLeave()
        try:
            self.chcAction.Clear()
            lAction=self.oCmp.GetPossibleActionLst()
            if lAction is not None:
                self.dAction={}
                i=0
                for sAction,iAction in lAction:
                    self.chcAction.Append(sAction,iAction)
                    self.dAction[iAction]=i
                    i+=1
                self.lblAction.Show(True)
                self.chcAction.Show(True)
                self.cbAction.Show(True)
            else:
                self.dAction=None
                self.lblAction.Show(False)
                self.chcAction.Show(False)
                self.cbAction.Show(False)
        except:
            vtLog.vtLngTB(self.GetName())
    def ClearResult(self):
        for ti in self.dTi.itervalues():
            self.trlstCmp.DeleteChildren(ti)
        self.selectedTi=None
        self.selectedKey=None
        self.chcAction.Enable(False)
        self.cbAction.Enable(False)
    def UpdateResult(self):
        self.ClearResult()
        if self.oCmp is None:
            return
        dRes=self.oCmp.GetResultDict()
        d=self.imgDict['resultsItems']
        bFilterResults=self.dShownResults is not None
        for k,t in dRes.iteritems():
            iMain,iImgNr,l=self.oCmp.ConvertResultItem4Disp(k,t)
            if bFilterResults:
                if iImgNr not in self.dShownResults:
                    continue
            tip=self.dTi[iMain]
            try:
                img=d[iImgNr]
            except:
                img=self.imgDict['empty']
            if len(t)>2:
                tid=wx.TreeItemData()
                tid.SetData(t[2])
            else:
                tid=None
            tic=self.trlstCmp.AppendItem(tip,k,img,img,tid)
            if l is not None:
                i=1
                for s in l:
                    self.trlstCmp.SetItemText(tic,s,i)
                    i+=1
        for ti in self.dTi.itervalues():
            self.trlstCmp.SortChildren(ti)
    def OnTrlstCmpRightDown(self, event):
        event.Skip()
        try:
            if self.selectedKey is None:
                return
            iAction=self.trlstCmp.GetPyData(self.selectedTi)
            if iAction is not None:
                dRes=self.oCmp.GetResultDict()
                t=dRes[self.selectedKey]
                if len(t)<3:
                    return
                iAction=t[2]
                keys=self.dAction.keys()
                iLen=len(keys)
                i=keys.index(iAction)
                i+=1
                if i==iLen:
                    i=0
                self.__setAction__(keys[i])
        except:
            vtLog.vtLngTB(self.GetName())
    def OnTrlstCmpSelChanged(self, event):
        ti=event.GetItem()
        try:
            iAction=self.trlstCmp.GetPyData(ti)
            if iAction is None:
                self.chcAction.Enable(False)
                self.cbAction.Enable(False)
            else:
                self.chcAction.SetSelection(self.dAction[iAction])
                self.chcAction.Enable(True)
                self.cbAction.Enable(True)
        except:
            pass
        self.selectedTi=ti
        self.selectedKey=self.trlstCmp.GetItemText(ti)
        self.__updateShow__()
    def __updateShow__(self):
        self.__updateShowX__()
        self.__updateShowY__()
    def __updateShowX__(self):
        if self.dlgShowX is not None:
            if self.dlgShowX.IsShown():
                if self.funcShowX is not None:
                    self.funcShowX(self.selectedKey,*self.argsShowX,**self.kwargsShowX)
    def __updateShowY__(self):
        if self.dlgShowY is not None:
            if self.dlgShowY.IsShown():
                if self.funcShowY is not None:
                    self.funcShowY(self.selectedKey,*self.argsShowY,**self.kwargsShowY)
    def __calcColumnSizes__(self):
        sz=self.trlstCmp.GetClientSize()
        try:
            iWidth=sz[0]
            iFree=iWidth
            iColumn=1
            self.trlstCmp.Freeze()
            if self.lColumn is not None:
                for t in self.lColumn:
                    j=t[1]
                    if j<0:
                        j=-int(iWidth*j)
                    iFree-=j
                    self.trlstCmp.SetColumnWidth(iColumn,j)
                    iColumn+=1
            if self.lColumnItem is not None:
                for t in self.lColumnItem:
                    j=t[1]
                    if j<0:
                        j=-int(iWidth*j)
                    iFree-=j
                    self.trlstCmp.SetColumnWidth(iColumn,j)
                    iColumn+=1
            iFree-=wx.SystemSettings.GetMetric(wx.SYS_VSCROLL_X)+5*iColumn
            if self.iMainColumnSize is None:
                # apply remaining size
                j=max(iFree,int(iWidth*self.REL_SIZE_MAIN_COLUMN))
                self.trlstCmp.SetColumnWidth(0,j)
            else:
                j=self.iMainColumnSize
                if j<0:
                    j=-int(iWidth*j)
                else:
                    j=self.iMainColumnSize
                self.trlstCmp.SetColumnWidth(0,j)
        except:
            vtLog.vtLngTB(self.GetName())
        self.trlstCmp.Thaw()
    def OnTrlstCmpSize(self,event):
        event.Skip()
        self.__calcColumnSizes__()
    def OnCbShowXButton(self, event):
        event.Skip()
        if self.cbShowX.GetValue()==True:
            if self.dlgShowX is not None:
                if self.bShowX==False:
                    self.bShowX=True
                    self.dlgShowX.Centre()
                self.dlgShowX.Show(True)
            self.__updateShowX__()
        else:
            if self.dlgShowX is not None:
                self.dlgShowX.Show(False)
    def OnCloseShowX(self,event):
        self.dlgShowX.Show(False)
        self.cbShowX.SetValue(False)
    def OnCbShowYButton(self, event):
        event.Skip()
        if self.cbShowY.GetValue()==True:
            if self.dlgShowY is not None:
                if self.bShowY==False:
                    self.bShowY=True
                    self.dlgShowY.Centre()
                self.dlgShowY.Show(True)
            self.__updateShowY__()
        else:
            if self.dlgShowY is not None:
                self.dlgShowY.Show(False)
    def OnCloseShowY(self,event):
        self.dlgShowY.Show(False)
        self.cbShowY.SetValue(False)
    def __setAction__(self,iAction):
        self.trlstCmp.Freeze()
        try:
            dRes=self.oCmp.GetResultDict()
            lTi=self.trlstCmp.GetSelections()
            for ti in lTi:
                sKey=self.trlstCmp.GetItemText(ti)
                t=dRes[sKey]
                if len(t)>2:
                    t[2]=iAction
                iMain,iImgNr,l=self.oCmp.ConvertResultItem4Disp(sKey,t)
                if l is not None:
                    i=1
                    for s in l:
                        self.trlstCmp.SetItemText(ti,s,i)
                        i+=1
        except:
            vtLog.vtLngTB(self.GetName())
        self.trlstCmp.Thaw()
    def OnCbActionButton(self, event):
        event.Skip()
        iSel=self.chcAction.GetSelection()
        iAction=self.chcAction.GetClientData(iSel)
        self.__setAction__(iAction)
    def OnCbCfgButton(self, event):
        event.Skip()
        if self.dlgCfg is None:
            self.dlgCfg=vtmCompareResultsSettingsDialog(self)
            self.dlgCfg.Centre()
            if self.oCmp is not None:
                oCmpItem=self.oCmp.GetCompareItem()
                if oCmpItem is not None:
                    lMap=oCmpItem.GetPossibleResult4DispLst()
                    self.dlgCfg.SetPossibleResult4DispLst(lMap)
        if self.dlgCfg.ShowModal()>0:
            d=self.dShownResults
            self.dShownResults=self.dlgCfg.GetShownResults()
            if d!=self.dShownResults:
                self.UpdateResult()
