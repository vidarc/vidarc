#----------------------------------------------------------------------------
# Name:         vtmPwd.py
# Purpose:      password input
# Author:       Walter Obweger
#
# Created:      20100224
# CVS-ID:       $Id: vtmPwd.py,v 1.1 2010/02/26 00:42:01 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

from vidarc.tool.gui.vtgPanel import vtgPanel
from vidarc.tool.gui.vtgDlg import vtgDlg

class vtmPwdPanel(vtgPanel):
    def __init__(self,parent,pos=None,size=None,name='pnPwd',
                bEnMark=False,bEnMod=False):
        vtgPanel.__init__(self,parent,pos=pos,size=size,name=name,style=0,
                bEnMark=bEnMark,bEnMod=bEnMod,lWid=[
                ('lblRg', (0,0),(1,1),{'name':'lblPwd','label':_(u'password')},None),
                ('txtPwd',(0,1),(1,3),{'name':'txtPwd','value':''},None),
                
                ])

class vtmPwdDlg(vtgDlg):
    def __init__(self,parent,title='vtmPwdDlg',pos=None,sz=None,
                name='dlgPwd',iArrange=-1,widArrange=None,
                kind='dialog',bmp=None):
        vtgDlg.__init__(self,parent,vtgPanel,pos=pos,sz=sz,name=name,
                title=title,bmp=bmp,
                iArrange=iArrange,widArrange=widArrange,kind=kind,
                pnName='pnPwd',lGrowCol=[(1,1)],
                bEnMark=False,bEnMod=False,lWid=[
                ('lblRg', (0,0),(1,1),{'name':'lblPwd','label':_(u'password')},None),
                ('txtPwdEnt',(0,1),(1,3),{'name':'txtPwd','value':''},{
                    'ent':self.OnPwdEnter,
                    'char':self.OnPwdKeyUp,
                    }),
                ])
    def Show(self,*args,**kwargs):
        self.pn.txtPwd.SetValue('')
        vtgDlg.Show(self,*args,**kwargs)
        self.pn.txtPwd.SetFocus()
    def ShowModal(self,*args,**kwargs):
        self.pn.txtPwd.SetValue('')
        self.pn.txtPwd.SetFocus()
        return vtgDlg.ShowModal(self,*args,**kwargs)
    def GetValue(self):
        return self.pn.GetValue('txtPwd')
    def SetValue(self,val):
        return self.pn.SetValue('txtPwd',val)
    def OnPwdEnter(self,evt):
        evt.Skip()
        try:
            self.__logDebug__(''%())
            self.OnCbApplyButton(None)
        except:
            self.__logTB__()
    def OnPwdKeyUp(self,evt):
        evt.Skip()
        try:
            self.__logDebug__(''%())
            #self.OnCbApplyButton(None)
        except:
            self.__logTB__()
        self.HandleKey(evt)
