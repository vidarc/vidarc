#----------------------------------------------------------------------------
# Name:         vtmListCtrl.py
# Purpose:      list control with stretch calculation
# Author:       Walter Obweger
#
#   cols    ... [lColDef0,lColDef1,...]
#
#        lColDefx =[
#        name,       ... column name
#        align,      ... column alignment 
#                                -1=left
#                                -3=center
#                                -2=right
#                                else unchanged
#        width,      ... column width
#        sort,       ... column sortable 
#                                0=not sortable
#                                1=sortable initially ascending 
#                               -1=sortable initially descending
#        ]
#
#   map     ... [(key0,col0),(key1,col1)]
#
#   set value by dict
#      {-1:data,            ... data to set, value is hashed in dict self.dDat
#       0:imgIdx,           ... image index for fist column
#       key0:(s0,img0),     ... s0=string value, img0=image index
#       key1:s1,            ... s1=string value, image is cleared with emtpy index
#       key2:img2,          ... img2=image index, string value is cleared by u''
#       }
#
#   set value by list
#      [data,               ... data to set, value is hashed in dict self.dDat
#       [
#       (s0,img0),          ... s0=string value, img0=image index
#       s1,                 ... s1=string value, image is cleared with emtpy index
#       img2                ... img2=image index, string value is cleared by u''
#       ]
#
# Created:      20080312
# CVS-ID:       $Id: vtmListCtrl.py,v 1.13 2013/06/02 14:39:14 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import types,sys

import wx
import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
#from vidarc.tool.log.vtLog import vtLogOriginWX
from vidarc.tool.gui.vtgPanelMixinWX import vtgPanelMixinWX
from vidarc.tool.misc.vtmColumnStretch import vtmColumnStretch

class vtmListCtrl(vtgPanelMixinWX,vtmColumnStretch,wx.ListCtrl):
    def __init__(self,id=-1,parent=None,pos=(0,0),size=(400,100),name='lstLog',
                style=wx.LC_REPORT,cols=None,default_col_width=100,
                map=None,value=None):
        wx.ListCtrl.__init__(self,id=id, name=name,parent=parent, pos=pos, size=size,
              style=style)
        vtgPanelMixinWX.__init__(self)
        vtmColumnStretch.__init__(self)
        self._GetColCount=self.GetColumnCount
        self._GetColWidth=self.GetColumnWidth
        self._SetColWidth=self.SetColumnWidth
        wx.CallAfter(wx.EVT_SIZE,self,self.OnSizeStretch)
        wx.CallAfter(self.OnSizeStretch,None)
        
        self.Bind(wx.EVT_LIST_COL_CLICK,self.OnListColClick,
                id=self.GetId())
        
        self._iColDftWidth=default_col_width
        self.IMG_SORT_ASC=-1
        self.IMG_SORT_DESC=-1
        self.IMG_EMPTY=-1
        self.MAP=map
        self.iDat=1L
        self.dDat={}
        self._dSort=None
        self._iSort=0
        self._iSortCol=-2
        self._funcSort=None
        #self._argsSort=()
        self._kwargsSort={}
        self._dSortDef=None
        self._it=wx.ListItem()
        self.SetColumns(cols)
        if value is not None:
            self.SetValue(value)
    def BindEvent(self,name,func,par=None):
        if name.startswith('lst_'):
            if name in ['lst_item_sel','lst_item_selected']:
                self.Bind(wx.EVT_LIST_ITEM_SELECTED,func, par or self)
            elif name in ['lst_item_desel','lst_item_deselected']:
                self.Bind(wx.EVT_LIST_ITEM_DESELECTED,func, par or self)
            elif name in ['lst_item_right_click','lst_item_rgClk']:
                self.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK,func, par or self)
            elif name in ['lst_col_drag_begin']:
                self.Bind(wx.EVT_LIST_COL_BEGIN_DRAG,func, par or self)
            elif name in ['lst_col_dragging']:
                self.Bind(wx.EVT_LIST_COL_DRAGGING,func, par or self)
            elif name in ['lst_col_drag_end']:
                self.Bind(wx.EVT_LIST_COL_END_DRAG,func, par or self)
            elif name in ['lst_col_left_click']:
                self.Bind(wx.EVT_LIST_COL_LEFT_CLICK,func, par or self)
            elif name=='lst_col_right_click':
                self.Bind(wx.EVT_LIST_COL_RIGHT_CLICK,
                        func, par or self)
        else:
            vtgPanelMixinWX.BindEvent(self,name,func,par=par)
    def GetCacheData(self):
        return self.dDat
    def GetData(self,iCltData):
        if iCltData in self.dDat:
            return self.dDat[iCltData]
        return None
    def GetDataKey(self,cltData):
        for k,v in self.dDat.iteritems():
            if v==cltData:
                return k
        return -1
    def SetMap(self,l):
        self.MAP=l
    def CreateImageList(self,lDef,which=None):
        """ 
        lDef
            [
            ['name0',bmp0],
            ['name1',[bmp0,bmp1],
            ]
        """
        dImg={}
        if (which is None) or (which==wx.IMAGE_LIST_SMALL):
            imgLst=wx.ImageList(16,16)
        else:
            imgLst=wx.ImageList(16,16)
            which=None
        for sK,it in lDef:
            t=type(it)
            if t==types.TupleType:
                lImg=[imgLst.Add(bmp) for bmp in it]
            elif t==types.ListType:
                lImg=[imgLst.Add(bmp) for bmp in it]
            else:
                lImg=imgLst.Add(it)
            dImg[sK]=lImg
        return imgLst,dImg
    def SetImageList(self,imgLst,iImgEmptyIdx=0,iImgSortAsc=-1,iImgSortDesc=-1,
                which=None):
        wx.ListCtrl.SetImageList(self,imgLst,which=which or wx.IMAGE_LIST_SMALL)
        self.IMG_SORT_ASC=iImgSortAsc
        self.IMG_SORT_DESC=iImgSortDesc
        self.IMG_EMPTY=iImgEmptyIdx
    def DeleteAllItems(self):
        wx.ListCtrl.DeleteAllItems(self)
        self.iDat=1L
        self.dDat={}
        self._dSort=None
    def ClearAll(self):
        wx.ListCtrl.ClearAll(self)
        self.iDat=1L
        self.dDat={}
        self._dSort=None
    Clear=ClearAll
    def __add_data__(self,data,i=0):
        try:
            if i is 0:
                i=self.iDat
                self.iDat+=1
            self.dDat[i]=data
            return i
        finally:
            pass
        return -1
    def _getAlignment(self,iAlign):
        if iAlign is None:
            iAlign=wx.ALIGN_LEFT
        elif iAlign<0:
            if iAlign==-1:
                iAlign=wx.ALIGN_LEFT
            elif iAlign==-2:
                iAlign=wx.ALIGN_RIGHT
            elif iAlign==-3:
                iAlign=wx.ALIGN_CENTRE
            else:
                self.__logError__('unknown format;%d',(iAlign))
                iAlign=wx.ALIGN_LEFT
        if wx.VERSION < (2,8):
            if iAlign==wx.ALIGN_LEFT:
                iAlign=wx.LIST_FORMAT_LEFT
            elif iAlign==wx.ALIGN_CENTRE:
                iAlign=wx.LIST_FORMAT_CENTRE
                #iAlign=wx.LIST_FORMAT_RIGHT
            elif iAlign==wx.ALIGN_RIGHT:
                iAlign=wx.LIST_FORMAT_RIGHT
            else:
                self.__logError__('unknown format;%d;0x%x'%(iAlign,iAlign))
                iAlign=wx.LIST_FORMAT_LEFT
        else:
            pass
        return iAlign
    def SetColumnAlignment(self,iCol,iAlign):
        self.SetColumn(iCol,None,iAlign,None,None)
    def SetColumn(self,iCol,sName,iAlign=None,iWidth=None,iSort=None,iImg=None):
        try:
            if self.__isLogDebug__():
                self.__logDebug__('iCol:%d;sName:%r;iAlign:%r;iWith:%r;iSort:%r;iImg:%r'%(\
                        iCol,sName,iAlign,iWidth,iSort,iImg))
            if type(sName)==type(self._it):
                wx.ListCtrl.SetColumn(self,iCol,sName)
                return
            it=self.GetColumn(iCol)
            mask=it.m_mask
            if sName is not None:
                it.m_text=sName
                mask|=wx.LIST_MASK_TEXT
            if iAlign is not None:
                it.m_format=self._getAlignment(iAlign)
                mask|=wx.LIST_MASK_FORMAT
            if iImg is not None:
                it.m_image=iImg
                mask|=wx.LIST_MASK_IMAGE
            elif iSort is not None:
                if iSort==0:
                    it.m_image=self.IMG_EMPTY
                elif iSort<0:
                    it.m_image=self.IMG_SORT_DESC
                elif iSort>0:
                    it.m_image=self.IMG_SORT_ASC
                else:
                    self.__logError__('undef sort:%d'%(iSort))
                    it.m_image=IMG_EMPTY
                mask|=wx.LIST_MASK_IMAGE
            if mask!=0:
                it.m_mask=mask
                wx.ListCtrl.SetColumn(self,iCol,it)
                return 1
            return 0
        except:
            self.__logTB__()
        return -1
    def SetColumns(self,cols):
        try:
            self.__logDebug__(''%())
            if self.IsMainThread()==False:
                return
            #self.DeleteAllItems()
            iCnt=self.GetColumnCount()
            bDbg=False
            if VERBOSE>0:
                if self.__isLogDebug__():
                    bDbg=True
            if bDbg==True:
                self.__logDebug__('cols:%s'%(self.__logFmt__(cols)))
            if cols is None:
                self.__logDebug__('exit nothing to do'%())
                return
            iCol=0
            lStretch=[]
            iSort=0
            for lCol in cols:
                if bDbg==True:
                    self.__logDebug__('iCol:%d;iCnt:%d;lCol;%s'%(iCol,iCnt,self.__logFmt__(lCol)))
                iW=lCol[2] or self._iColDftWidth
                if type(iW)==types.FloatType:
                    lStretch.append((iCol,iW))
                    iW=self._iColDftWidth
                elif iW<0:
                    lStretch.append((iCol,iW))
                    iW=self._iColDftWidth
                if iSort==0:
                    iSortTmp=lCol[3]
                    iSort=iSortTmp
                else:
                    iSortTmp=0
                if iCol<iCnt:
                    self.SetColumn(iCol,lCol[0],lCol[1],iW,iSortTmp,None)
                else:
                    self.InsertColumn(iCol,lCol[0],self._getAlignment(lCol[1]),iW)
                    self.SetColumn(iCol,lCol[0],lCol[1],None,iSortTmp,None)
                iCol+=1
            for i in xrange(iCol,iCnt):
                self.DeleteColumn(iCol)
            self.SetStretchLst(lStretch)
        except:
            self.__logTB__()
    def SetItem(self,idx,i,v):
        try:
            if self.IsMainThread()==False:
                return -2
            if i<0:
                i=self.GetItemData(idx)
                i=self.__add_data__(v,i)
                self.SetItemData(idx,i)
                return 0
            t=type(v)
            if t==types.TupleType:
                self.SetStringItem(idx,i,v[0],v[1])
            elif t==types.IntType:
                self.SetStringItem(idx,i,u'',v)
            else:
                self.SetStringItem(idx,i,v,-1)
            return 0
        except:
            self.__logTB__()
    def GetItemTup(self,idx,i):
        try:
            if self.IsMainThread()==False:
                return None
            if i==-1:
                i=self.GetItemData(idx)
                if i>0:
                    return self.dDat.get(i,None)
                return None
            elif i==-3:
                return self.GetItemData(idx)
            elif i<0:
                return None
            it=self.GetItem(idx,i)
            t=(it.m_text[:],self._it.m_image)
            return t
        except:
            self.__logTB__()
    def GetItemStr(self,idx,i):
        try:
            if self.IsMainThread()==False:
                return None
            if i==-1:
                i=self.GetItemData(idx)
                if i>0:
                    return self.dDat.get(i,None)
                return None
            elif i==-3:
                return self.GetItemData(idx)
            elif i<0:
                return None
            it=self.GetItem(idx,i)
            return it.m_text[:]
        except:
            self.__logTB__()
    def __setItemDict__(self,lst,idx,d):
        #print tr.GetItemText(ti,0)
        for s,i in self.MAP:
            if s in d:
                v=d[s]
                t=type(v)
                if t==types.TupleType:
                    lst.SetStringItem(idx,i,v[0],v[1])
                elif t==types.IntType:
                    lst.SetStringItem(idx,i,u'',v)
                else:
                    lst.SetStringItem(idx,i,v,-1)
        if 0 in d:
            self._it.Clear()
            self._it.SetImage(d[0])
            self._it.SetMask(wx.LIST_MASK_IMAGE)
            lst.SetItem(idx,self._it)
        if -1 in d:
            i=self.GetItemData(idx)
            i=self.__add_data__(d[-1],i)
            lst.SetItemData(idx,i)
        #if -2 in d:
        #    self.__setValue__(tr,ti,d[-2])
    def __setItemLst__(self,lst,idx,l):
        try:
            if l[0] is not None:
                i=self.GetItemData(idx)
                i=self.__add_data__(l[0],i)
                lst.SetItemData(idx,i)
            i=0
            for v in l[1]:
                t=type(v)
                if t==types.TupleType:
                    lst.SetStringItem(idx,i,v[0],v[1])
                elif t==types.IntType:
                    lst.SetStringItem(idx,i,u'',v)
                else:
                    lst.SetStringItem(idx,i,v,-1)
                i+=1
        except:
            self.__logTB__()
            self.__logError__('l:%s;l[0]:%s;l[1]:%s'%(self.__logFmt__(l),
                    self.__logFmt__(l[0]),self.__logFmt__(l[1])))
    def __setItemTup__(self,tr,ti,t):
        pass
    def __setItem__(self,lst,idx,val):
        t=type(val)
        if t==types.DictType:
            self.__setItemDict__(lst,idx,val)
        elif t==types.ListType:
            self.__setItemLst__(lst,idx,val)
        elif t==types.TupleType:
            self.__setItemTup__(lst,idx,val)
    def __setValue__(self,lst,l,idx=-1):
        if idx<0:
            idx=0
        iCnt=lst.GetItemCount()
        for val in l:
            if idx>=iCnt:
                idx=self.InsertImageStringItem(idx,'',self.IMG_EMPTY)
                iCnt+=1
            self.__setItem__(lst,idx,val)
            idx+=1
        return idx-1
    def __getValue__(self,lst,idx,lMap,l):
        def getVal(lst,idx,i):
            if i==-1:
                i=lst.GetItemData(idx)
                if i>0:
                    return lst.dDat.get(i,None)
                return None
            elif i==-2:
                return idx
            elif i==-3:
                return lst.GetItemData(idx)
            else:
                it=lst.GetItem(idx,i)
                return it.m_text[:]
        l.append([getVal(lst,idx,i) for i in lMap])
    def __getValueData__(self,lst,idx,l):
        i=lst.GetItemData(idx)
        if i>0:
            l.append(lst.dDat.get(i,None))
    def __insert__(self,lst,l,idx=-1):
        try:
            if idx<0:
                idx=sys.maxint
                for val in l:
                    idx=lst.InsertImageStringItem(sys.maxint,'',self.IMG_EMPTY)
                    self.__setItem__(lst,idx,val)
            else:
                idx-=1
                for val in l:
                    idx+=1
                    idx=lst.InsertImageStringItem(idx,'',self.IMG_EMPTY)
                    self.__setItem__(lst,idx,val)
            return idx
        except:
            self.__logTB__()
        return -1
    def Insert(self,l,idx=-1):
        try:
            if self.IsMainThread()==False:
                return -2
            return self.__insert__(self,l,idx=idx)
        except:
            self.__logTB__()
        return -1
    def SetValue(self,l,idx=-1):
        try:
            if self.IsMainThread()==False:
                return -2
            self.__logDebug__(''%())
            w=self
            #iCols=w.GetColumnCount()
            if idx==-1:
                w.DeleteAllItems()
                return self.__insert__(w,l,idx=idx)
            else:
                return self.__setValue__(w,l,idx=idx)
        except:
            self.__logTB__()
        return -1
    def GetValue(self,lMap=None,idx=None):
        try:
            self.__logDebug__(''%())
            l=[]
            lst=self
            if idx is None:
                idx=0
                iCnt=-1
            elif idx<0:
                idx=0
                iCnt=-1
            else:
                iCnt=1
            if iCnt<0:
                iCnt=self.GetItemCount()
            if lMap is None:
                for i in xrange(idx,idx+iCnt):
                    self.__getValueData__(lst,i,l)
            else:
                for i in xrange(idx,idx+iCnt):
                    self.__getValue__(lst,i,lMap,l)
            if self.__isLogDebug__():
                self.__logDebug__(self.__logFmt__(l))
            return l
        except:
            self.__logTB__()
        return None
    def PrcFct(self,idx,bDbg,fct,*args,**kwargs):
        try:
            if idx is None:
                idx=0
                iCnt=-1
            elif idx<0:
                idx=0
                iCnt=-1
            else:
                iCnt=1
            if iCnt<0:
                iCnt=self.GetItemCount()
            iOfs=0
            for i in xrange(idx,idx+iCnt):
                iRet=fct(iOfs,iCnt,i,*args,**kwargs)
                if iRet<=0:
                    return iRet
                iOfs+=1
            return 1
        except:
            self.__logTB__()
            return -1
    def __setState__(self,lst,l,bFlag,iState):
        try:
            iCnt=self.GetItemCount()
            if bFlag==True:
                for i in l:
                    lst.SetItemState(i,iState,iState)
            else:
                for i in l:
                    lst.SetItemState(i,0,iState)
            return 0
        except:
            self.__logTB__()
        return -1
    def __setSelected__(self,lst,l,bFlag):
        return self.__setState__(lst,l,bFlag,wx.LIST_STATE_SELECTED)
    def __getState__(self,lst,lMap,l,iState):
        try:
            iCnt=self.GetItemCount()
            iSel=0
            for i in xrange(0,iCnt):
                if (lst.GetItemState(i,iState)&iState)==iState:
                    self.__getValue__(lst,i,lMap,l)
                    iSel+=1
            return iSel
        except:
            self.__logTB__()
        return -1
    def __getSelected__(self,lst,lMap,l):
        return self.__getState__(lst,lMap,l,wx.LIST_STATE_SELECTED)
    def SetSelected(self,l,bFlag=True):
        try:
            self.__logDebug__(''%())
            lst=self
            return self.__setSelected__(lst,l,bFlag=bFlag)
        except:
            self.__logTB__()
        return -1
    def GetSelected(self,lMap=None):
        try:
            self.__logDebug__(''%())
            l=[]
            lst=self
            self.__getSelected__(lst,lMap or [-2,-1],l)
            return l
        except:
            self.__logTB__()
        return None
    def __getStateByName__(self,state):
        if state in ['sel','selected']:
            iState=wx.LIST_STATE_SELECTED
        elif state in ['foc','focus','focused']:
            iState=wx.LIST_STATE_FOCUSED
        elif state in ['sel_foc','sel_focus']:
            iState=wx.LIST_STATE_SELECTED|wx.LIST_STATE_FOCUSED
        elif state=='cut':
            iState=wx.LIST_STATE_CUT
        elif state=='sel_cut':
            iState=wx.LIST_STATE_SELECTED|wx.LIST_STATE_CUT
        else:
            iState=wx.LIST_STATE_SELECTED
        return iState
    def SetByStateName(self,l,bFlag=True,state='sel'):
        try:
            self.__logDebug__(''%())
            lst=self
            iState=self.__getStateByName__(state)
            self.__setState__(lst,l,bFlag,iState)
            return 0
        except:
            self.__logTB__()
        return None
    def GetByStateName(self,state='sel',lMap=None):
        try:
            self.__logDebug__(''%())
            l=[]
            lst=self
            iState=self.__getStateByName__(state)
            self.__getState__(lst,lMap or [-2,-1],l,iState)
            return l
        except:
            self.__logTB__()
        return None
    def OnListColClick(self, evt):
        evt.Skip()
        try:
            self.__logDebug__(''%())
            if self._dSortDef is None:
                return
            iCol=evt.GetColumn()
            bSort=True
            if iCol in self._dSortDef:
                dDef=self._dSortDef[iCol]
            elif -1 in self._dSortDef:
                dDef=self._dSortDef[-1]
            else:
                dDef={}
                bSort=False
            if VERBOSE:
                if self.__isLogDebug__():
                    self.__logDebug__('iCol:%d;dDef;%s;dSortDef;%s'%(iCol,
                            self.__logFmt__(dDef),self.__logFmt__(self._dSortDef)))
            iSort=dDef.get('iSort',0)
            func=dDef.get('func',None)
            if iSort>0:
                if iCol==self._iSortCol:
                    iSort=-self._iSort
                #else:
                #    iSort=self._iSort
            if func is None:
                self.SetSort(iCol,iSort,bSort,self._funcSort,
                            **self._kwargsSort)
            else:
                #arg=dDef.get('args',())
                kwargs=dDef.get('kwargs',{})
                
                self.SetSort(iCol,iSort,bSort,func,**kwargs)
        except:
            self.__logTB__()
    def SetSortDef(self,d):
        try:
            self.__logDebug__('l:%s'%(self.__logFmt__(d)))
            self._dSortDef=d
        except:
            self.__logTB__()
    def SetSort(self,iCol,iSort,bSort=False,func=None,**kwargs):
        try:
            self.__logDebug__('iCol:%d,iSort:%d;bSort:%d'%(iCol,iSort,
                        bSort))
            iOldSort=self._iSort
            iOldCol=self._iSortCol
            self.__logDebug__('iCol:%d;iOldCol:%d;iSort:%d;iOldSort:%d;bSort:%d'%(iCol,iOldCol,
                        iSort,iOldSort,bSort))
            self._iSort=iSort
            self._iSortCol=iCol
            
            self._funcSort=func
            #self._argsSort=args
            self._kwargsSort=kwargs
            if (iOldSort!=self._iSort) or (iOldCol!=self._iSortCol):
                if iOldCol>=0:
                    self.SetColumn(iOldCol,None,iSort=0)
                elif (iOldCol==-1) and (self._iSortCol!=0):
                    self.SetColumn(0,None,iSort=0)
                if iOldCol>=0:
                    self._dSort=None
                if self._iSortCol>=0:
                    self.SetColumn(self._iSortCol,None,iSort=self._iSort)
                else:
                    self.SetColumn(0,None,iSort=self._iSort)
            if bSort:
                self.Sort()
        except:
            self.__logTB__()
    def OnCompare(self,iA,iB):
        dA=self.dDat.get(iA,None)
        dB=self.dDat.get(iB,None)
        i=cmp(dA,dB)
        if self._iSort<0:
            return -i
        return i
    def OnCompareFunc(self,iA,iB):
        dA=self.dDat.get(iA,None)
        dB=self.dDat.get(iB,None)
        i=self._funcSort(dA,dB,**self._kwargsSort)
        if self._iSort<0:
            return -i
        return i
    def OnCompareCol(self,iA,iB):
        dA=self._dSort.get(iA,'')
        dB=self._dSort.get(iB,'')
        i=cmp(dA,dB)
        if self._iSort<0:
            return -i
        return i
    def OnCompareColFunc(self,iA,iB):
        dA=self._dSort.get(iA,'')
        dB=self._dSort.get(iB,'')
        i=self._funcSort(dA,dB,**self._kwargsSort)
        if self._iSort<0:
            return -i
        return i
    def OnCompareColDatLst(self,iA,iB):
        dA=self.dDat.get(iA,'')
        dB=self.dDat.get(iB,'')
        i=cmp(dA[self._iSortCol],dB[self._iSortCol])
        if self._iSort<0:
            return -i
        return i
    def OnCompareColDatFunc(self,iA,iB):
        dA=self.dDat.get(iA,None)
        dB=self.dDat.get(iB,None)
        i=self._funcSort(dA,dB,self._iSortCol,**self._kwargsSort)
        if self._iSort<0:
            return -i
        return i
    def OnCompareColDatFuncOrder(self,iA,iB):
        dA=self.dDat.get(iA,None)
        dB=self.dDat.get(iB,None)
        i=self._funcSort(dA,dB,self._iSortCol,self._iSort,**self._kwargsSort)
        return i
    def Sort(self):
        try:
            self.__logDebug__('iSort:%d;iSortCol:%d'%(self._iSort,self._iSortCol))
            if self._iSort==0:
                self._dSort=None
                return
            iSort=abs(self._iSort)
            if self._iSortCol<0:
                if iSort==1:
                    if self._funcSort is None:
                        self.SortItems(self.OnCompare)
                    else:
                        self.SortItems(self.OnCompareFunc)
                elif iSort==2:
                    _dSort={}
                    self._dSort=_dSort
                    if self._funcSort is None:
                        self.SortItems(self.OnCompareColDatLst)
                    else:
                        self.SortItems(self.OnCompareColDatFunc)
                elif iSort==3:
                    _dSort={}
                    self._dSort=_dSort
                    if self._funcSort is None:
                        self.SortItems(self.OnCompareColDatLst)
                    else:
                        self.SortItems(self.OnCompareColDatFuncOrder)
            else:
                _dSort={}
                if iSort==1:
                    iCnt=self.GetItemCount()
                    for idx in xrange(iCnt):
                        i=self.GetItemData(idx)
                        s=self.GetItem(idx,self._iSortCol).m_text
                        _dSort[i]=s
                    self._dSort=_dSort
                    if self._funcSort is None:
                        self.SortItems(self.OnCompareCol)
                    else:
                        self.SortItems(self.OnCompareColFunc)
                elif iSort==2:
                    self._dSort=_dSort
                    if self._funcSort is None:
                        self.SortItems(self.OnCompareColDatLst)
                    else:
                        self.SortItems(self.OnCompareColDatFunc)
                elif iSort==3:
                    self._dSort=_dSort
                    if self._funcSort is None:
                        self.SortItems(self.OnCompareColDatLst)
                    else:
                        self.SortItems(self.OnCompareColDatFuncOrder)
                self._dSort=None
        except:
            self.__logTB__()
