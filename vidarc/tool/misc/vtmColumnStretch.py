#----------------------------------------------------------------------------
# Name:         vtmColumnStretch.py
# Purpose:      automatic column strech calculation on size event
# Author:       Walter Obweger
#
# Created:      20080312
# CVS-ID:       $Id: vtmColumnStretch.py,v 1.4 2009/10/22 19:08:16 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

class vtmColumnStretch:
    def __init__(self,iMinWidth=100,iMinProp=10,iScrollBarWidth=20):
        self._lStretchFact=None
        self._dStretchFact=None
        self._iMinWidth=iMinWidth
        self._iMinProp=iMinProp
        self._iScrollBarWidth=iScrollBarWidth
    def __setup__(self):
        self._dStretchFact={'__width':0,'__prop':[],'__rel':0}
        iRel=0
        if self._lStretchFact is None:
            return
        for iCol,iFact in self._lStretchFact:
            self._dStretchFact[iCol]=iFact
            if iFact<0:
                self._dStretchFact['__prop'].append(iCol)
                iRel+=-iFact
        self._dStretchFact['__rel']=iRel
        iCols=self._GetColCount()
        iFixedW=0
        for i in xrange(iCols):
            if i in self._dStretchFact:
                continue
            iW=self._GetColWidth(i)
            self._dStretchFact[i]=iW
            iFixedW+=iW
        self._dStretchFact['__width']=iFixedW
    def SetStretchLst(self,l):
        self._lStretchFact=l
        self._dStretchFact=None
        self.OnSizeStretch(None)
    def OnSizeStretch(self,evt):
        if evt is not None:
            evt.Skip()
        try:
            if self._dStretchFact is None:
                self.__setup__()
            sz=self.GetClientSize()
            iW=max(self._iMinWidth,sz.GetWidth()-self._iScrollBarWidth)
            iWUsed=0
            if self._lStretchFact is not None:
                for iCol,iFact in self._lStretchFact:
                    if iFact>0:
                        iWh=int(iFact*iW)
                        iWUsed+=iWh
                        self._SetColWidth(iCol,iWh)
            iWUsed+=self._dStretchFact['__width']
            l=self._dStretchFact['__prop']
            if self._dStretchFact['__rel']>0:
                iWtmp=max(iW-iWUsed,self._iMinProp)/float(self._dStretchFact['__rel'])
                for iCol in l:
                    self._SetColWidth(iCol,int(iWtmp*(-self._dStretchFact[iCol])))
        except:
            self.__logTB__()
