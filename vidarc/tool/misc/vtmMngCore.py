#----------------------------------------------------------------------------
# Name:         vtmMngCore.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20091114
# CVS-ID:       $Id: vtmMngCore.py,v 1.6 2010/03/03 02:16:47 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import time
from Queue import Queue

from vidarc.tool.vtSched import vtSched
from vidarc.tool.vtRngBuf import vtRngBuf,vtRngBufItem

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.vtThreadCore import vtThreadCore
from vidarc.tool.vtStateFlagSimple import vtStateFlagSimple
from vidarc.tool.misc.vtmThread import vtmThreadInterFace
import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

class vtmMngCoreRngBufDict(vtRngBufItem):
    def __init__(self,lK):
        self.dVal={}
        for k in lK:
            self.dVal[k]=-1
        self.Clr()
    def Clr(self):
        for k in self.dVal.iterkeys():
            self.dVal[k]=-1
    def Set(self,d):
        for k in self.dVal.iterkeys():
            if k in d:
                self.dVal[k]=d[k]
    def Get(self):
        return self.dVal

class vtmMngCore(vtmThreadInterFace):
    def __init__(self,bPost=False,verbose=-1,*args,**kwargs):
        if hasattr(self,'thdMng')==False:
            self.thdMng=vtThreadCore(bPost=bPost,verbose=max(verbose-1,VERBOSE),
                                origin=u':'.join([self.GetOrigin(),'thdMng']))
        if hasattr(self,'thdProc')==False:
            self.thdProc=vtThreadCore(bPost=bPost,verbose=max(verbose-1,VERBOSE),
                                origin=u':'.join([self.GetOrigin(),'thdProc']))
        if hasattr(self,'oSF')==False:
            self.oSF=vtStateFlagSimple({},{},
                funcNotify=self.NotifyStateChange)
        try:
            self.oSF.AddFlagDict(self.__initFlagDict__())
            self.oSF.AddStateDict(self.__initStateDict__())
            if 'mng_flags' in kwargs:
                self.oSF.AddFlagDict(kwargs['mng_flags'])
            if 'mng_states' in kwargs:
                self.oSF.AddStateDict(kwargs['mng_states'])
            
            self.oSF.Clear()
        except:
            self.__logTB__()
        if hasattr(self,'semAccess')==False:
            self.semAccess=self.thdMng.getLock()
        if VERBOSE>0:
            self.__logDebug__('oSF:%s'%(self.oSF.GetStrFull(';')))
        self.dFunc={}
        #self.SetPar(None)
        
        self.__initCtrl__(*args,**kwargs)
        vtmThreadInterFace.__init__(self,self.thdMng)
        self.thdMng.IsBusy=self.IsBusy
        if self.thdProc is not None:
            self.thdProc.IsBusy=self.IsBusy
        self.__start__()
        if VERBOSE>0:
            self.__logDebug__('status;%s'%(self.oSF.GetStrFull(';')))
    def __initCtrl__(self,*args,**kwargs):
        if 'qIn' in kwargs:
            self.qIn=kwargs['qIn']
        else:
            self.qIn=Queue(kwargs.get('inSize',0))
        if 'qOut' in kwargs:
            self.qOut=kwargs['qOut']
        else:
            self.qOut=Queue(kwargs.get('inSize',0))
        #self.shdComm=vtSched(kwargs.get('scheduleSize',32))
        #self.qVal=vtRngBuf(kwargs.get('valSize',32),vtmMngCoreRngBufDict,kwargs.get('valKey',['state','flag']))
        #self.qStat=vtRngBuf(kwargs.get('statSize',32),vtmMngCoreRngBufDict,kwargs.get('valKey',['status',]))
        #self.qCmd=vtRngBuf(kwargs.get('cmdSize',32),vtmMngCoreRngBufDict,kwargs.get('cmdKey',['cmd','args','kwargs']))
        self.zWait=kwargs.get('zWait',1)
        self.dCmd=kwargs.get('dCmd',{})
        
        self.tNotify=kwargs.get('notify',None)
        self.funcNotifyStateChange=kwargs.get('NotifyStateChange',None)
    def __del__(self):
        #if self.par is not None:
        #    self.par=None
        pass
    def _acquire(self,blocking=True):
        return self.semAccess.acquire(blocking)
    def _release(self):
        self.semAccess.release()
    def __doFuncClasses__(self,sN,*args,**kwargs):
        for o in self.__class__.__bases__:
            if hasattr(o,sN):
                f=getattr(o,sN)
                f(self,*args,**kwargs)
    def doFunc(self,sName):
        try:
            if sName in self.dFunc:
                func,args,kwargs=self.dFunc[sName]
                if VERBOSE>10:
                    if self.__isLogDebug__():
                        self.__logDebug__('sName:%s;func:%r;args:%s;kwargs:%s'%(sName,
                            func,self.__logFmt__(args),self.__logFmt__(kwargs)))
                self.__callFuncTup__(func,*args,**kwargs)
        except:
            vtLog.vtLngTB(self.GetName())
    def DoFunc(self,sName):
        self.doFunc(sName)
        sN='.'.join([sName,'CB'])
        if sN in self.dFunc:
            self.CallBack(self.doFunc,sN)
        sN='.'.join([sName,'CallBack'])
        if sN in self.dFunc:
            self.CallBack(self.doFunc,sN)
        sN='.'.join([sName,'CBWX'])
        if sN in self.dFunc:
            self.CallBackWX(self.doFunc,sN)
        sN='.'.join([sName,'CallBackWX'])
        if sN in self.dFunc:
            self.CallBackWX(self.doFunc,sN)
    def SetFunc(self,sName,func,*args,**kwargs):
        if func is None:
            if sName in self.dFunc:
                del self.dFunc[sName]
        else:
            self.dFunc[sName]=(func,args,kwargs)
    def __callFuncTup__(self,func,*args,**kwargs):
        try:
            func(*args,**kwargs)
        except:
            self.__logTB__()
    def __initStateDict__(self):
        self.__logDebug__(''%())
        return {
                0:{'name':'IDLE','is':'IsIdle','init':True,
                        'enter':{'set':['LINKED',],
                                 'clr':['ACTIVE','PAUSING','PAUSED','STOPPING',
                                     'SERVING','ABORTING','STARTING']}
                    },
                10:{'name':'START','is':'IsStart',
                        'enter':{'set':['LINKED',],
                                 #'func':(self.SocketClosed,(),{}),
                                 'clr':['ACTIVE','PAUSED',]},
                    },
                15:{'name':'RUNNING','is':'IsRunning',
                        'enter':{'set':['ACTIVE','LINKED'],
                                 #'func':(self.SocketClosed,(),{}),
                                 'clr':['PAUSING','PAUSED','STOPPING','ABORTING','STARTING']},
                    },
                60:{'name':'HOLD','is':'IsHold',
                        'enter':{'set':['PAUSING'],'clr':['ACTIVE','PAUSED']}},
                65:{'name':'HELD','is':'IsHeld',
                        'enter':{'set':['PAUSED'],'clr':['ACTIVE']}},
                30:{'name':'ABORT','is':'IsAbort',
                        'enter':{'set':['ABORTING'],'clr':['ACTIVE']}},
                35:{'name':'ABORTED','is':'IsAborted',
                        'enter':{'set':['ABORTED'],'clr':['ABORTING','ACTIVE']}},
                40:{'name':'STOP','is':'IsStop',
                        'enter':{'set':['STOPPING'],'clr':['ACTIVE']}},
                45:{'name':'STOPPED','is':'IsStopped',
                        'enter':{'clr':['ACTIVE','STOPPING']}},
                19:{'name':'COMPLETED','is':'IsCompleted',
                        'enter':{'clr':['ACTIVE']}},
                
                90:{'name':'SHUTTINGDOWN','is':'IsShuttingDown',
                        'enter':{#'func':(self.NotifyConnected,(),{}),
                                 'clr':['ACTIVE','PAUSED','LINKED']}
                    },
                95:{'name':'SHUTDOWN','is':'IsShutDown',
                        'enter':{#'func':(self.NotifyConnected,(),{}),
                                 'clr':['ACTIVE','PAUSED','LINKED','SERVING']}
                    },
                }
    def __initFlagDict__(self):
        self.__logDebug__(''%())
        return {
                0x0001:{'name':'ACTIVE','is':'IsActive','order':0},
                0x0002:{'name':'PAUSED','is':'IsPaused','order':1},
                0x0004:{'name':'LINKED','is':'IsLinked','order':2,'init':True},
                0x0008:{'name':'SERVING','is':'IsServing','order':3},
                0x0010:{'name':'STARTING','is':'IsStarting','order':4},
                0x0020:{'name':'PAUSING','is':'IsPausing','order':5},
                0x0040:{'name':'STOPPING','is':'IsStopping','order':1},
                0x0080:{'name':'ABORTING','is':'IsAborting','order':1},
                }
    def SetServing(self,flag):
        self.__logInfo__(''%())
        self.oSF.SetFlag(self.oSF.SERVING,flag)
    def IsServing(self):
        return self.oSF.IsServing
    def GetQueueIn(self):
        return self.qIn
    def GetQueueOut(self):
        return self.qOut
    def IsBusy(self):
        return False
    def __start__(self):
        self.__logInfo__(''%())
        try:
            if self.oSF.IsServing:
                self.__logError__('already served')
                return
            if self.oSF.IsStarting:
                self.__logError__('already starting')
                return
            if self.thdMng.Is2Stop():
                if self.thdMng.IsRunning():
                    self.__logError__('is2Stop active')
                    return
            if self.oSF.IsShutDown:
                self.__logError__('shut down is active')
                return
            if self.oSF.IsShuttingDown:
                self.__logError__('shutting down is active')
                return
            self.thdMng.Start()
            #self.oSF.SetState(self.oSF.START)
            self.thdMng.Do(self.procWatchQueue)
        except:
            self.__logTB__()
    def StopOld(self):
        self.__logInfo__(''%())
        self.oSF.SetState(self.oSF.STOP)
        self.thdMng.Stop()
        try:
            pass
        except:
            self.__logTB__()
    def __postState__(self,iState):
        try:
            self._acquire()
            d={'stat':iState,'ident':self._ident}
            self.GetQueueOut().put(d)
        except:
            self.__logTB__()
        self._release()
    def ShutDown(self):
        self.__logInfo__(''%())
        if self.oSF.IsShutDown:
            return
        if self.oSF.IsServing:
            self.oSF.SetState(self.oSF.SHUTTINGDOWN)
        else:
            self.oSF.SetState(self.oSF.SHUTDOWN)
        #self.__postState__(self.oSF.SHUTTINGDOWN)
        #self.thdMng.Stop()
    def Notify(self,iTyp,sock):
        try:
            if self.tNotify is not None:
                func,args,kwargs=self.tNotify
                func(iTyp,sock,*args,**kwargs)
        except:
            self.__logTB__()
    #def DoResp(self):
    #    try:
    #        while self.shdComm.pop()>0:
    #            pass
    #    except:
    #        self.__logTB__()
    def __getattr__(self,name):
        if hasattr(self.thdMng,name):
            if name.endswith('Thd'):
                return getattr(self.thdMng,name[:-3])
        raise AttributeError(name)
    def __procState__(self,iState,iStatePrev):
        try:
            self._acquire()
        except:
            self.__logTB__()
        self._release()
    # +++++ StateFlag object
    def NotifyStateChange(self,ident,statePrev,state,zStamp,*args,**kwargs):
        """ called in case of state change from wx MainLoop
        """
        if VERBOSE>0:
            if self.__isLogDebug__():
                self.__logDebug__('%s'%(self.oSF.GetStrFull(';')))
        self.__postState__(state)
        try:
            if self.funcNotifyStateChange is not None:
                self.CallBack(self.funcNotifyStateChange,ident,statePrev,state,zStamp,*args,**kwargs)
        except:
            self.__logTB__()
        self.__procState__(state,statePrev)
        #self.Refresh()
    # ----- StateFlag object

class vtmMngCoreLg(vtLog.vtLogMixIn,vtLog.vtLogOrigin,vtmMngCore):
    def __init__(self,*args,**kwargs):
        vtLog.vtLogOrigin.__init__(self,**{'origin':kwargs.get('origin',None)})
        vtmMngCore.__init__(self,*args,**kwargs)
