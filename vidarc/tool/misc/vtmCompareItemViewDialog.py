#Boa:Dialog:vtmCompareItemViewDialog
#----------------------------------------------------------------------------
# Name:         vtmCompareItemViewDialog.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20070515
# CVS-ID:       $Id: vtmCompareItemViewDialog.py,v 1.2 2007/09/02 14:56:40 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

def create(parent):
    return vtmCompareItemViewDialog(parent)

[wxID_VTMCOMPAREITEMVIEWDIALOG, wxID_VTMCOMPAREITEMVIEWDIALOGCBAPPLY, 
 wxID_VTMCOMPAREITEMVIEWDIALOGCBCANCEL, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vtmCompareItemViewDialog(wx.Dialog):
    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsBt, (1, 0), border=4,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER, span=(1, 1))

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=0, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_gbsData_Items(self.gbsData)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTMCOMPAREITEMVIEWDIALOG,
              name=u'vtCompareItemViewDialog', parent=prnt, pos=wx.Point(348,
              174), size=wx.Size(400, 420),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE,
              title=u'vtCompare Item View Dialog')
        self.SetClientSize(wx.Size(392, 393))

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTMCOMPAREITEMVIEWDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Close), label=_(u'Close'),
              name=u'cbCancel', parent=self, pos=wx.Point(108, 24),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VTMCOMPAREITEMVIEWDIALOGCBCANCEL)

        self._init_sizers()

    def __init__(self, parent,clsPanel,namePanel,sTitle):
        global _
        _=vtLgBase.assignPluginLang('vtMisc')
        self._init_ctrls(parent)
        self.SetTitle(sTitle)
        try:
            self.doc=None
            self.node=None
            self.gbsData.AddGrowableCol(0)
            self.gbsData.AddGrowableRow(0)
            self.pn=clsPanel(self,id=wx.NewId(),pos=(0,0),size=wx.DefaultSize,
                            style=0,name=namePanel)
            self.gbsData.AddWindow(self.pn, (0, 0), border=4,
                  flag=wx.BOTTOM | wx.TOP | wx.LEFT | wx.RIGHT | wx.EXPAND, span=(1, 1))
            self.gbsData.Layout()
            self.gbsData.Fit(self)
        except:
            vtLog.vtLngTB(self.GetName())
    def GetPanel(self):
        return self.pn
    def OnCbApplyButton(self,evt):
        evt.Skip()
        try:
            self.pn.Close()
            self.pn.GetNode(None)
            if self.IsModal():
                self.EndModal(1)
            else:
                self.Show(False)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbCancelButton(self,evt):
        evt.Skip()
        try:
            #self.pn.Close()
            #self.pn.Cancel()
            if self.IsModal():
                self.EndModal(0)
            else:
                self.Close()
                #self.Show(False)
        except:
            vtLog.vtLngTB(self.GetName())
