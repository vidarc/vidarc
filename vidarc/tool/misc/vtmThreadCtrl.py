#----------------------------------------------------------------------------
# Name:         vtmThreadCtrl.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080826
# CVS-ID:       $Id: vtmThreadCtrl.py,v 1.9 2010/03/10 22:39:20 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.state.vtArtState as vtArtState
import vidarc.tool.art.vtThrobber

import types

from vidarc.tool.vtThread import EVT_VT_THREAD_PROC
from vidarc.tool.vtThread import EVT_VT_THREAD_ABORTED
from vidarc.tool.vtThread import EVT_VT_THREAD_FINISHED
from vidarc.tool.vtThread import EVT_VT_THREAD_PROC_DISCONNECT
from vidarc.tool.vtThread import EVT_VT_THREAD_ABORTED_DISCONNECT
from vidarc.tool.vtThread import EVT_VT_THREAD_FINISHED_DISCONNECT

[
  wxID_VTTHREADCTRLTHRPROC,wxID_VTTHREADCTRLCBSTART,
  wxID_VTTHREADCTRLCBSTOP,wxID_VTTHREADCTRLTXTVAL,
  wxID_VTTHREADCTRLTXTCOUNT,wxID_VTTHREADCTRLPBPROC
] = [wx.NewId() for __init__ in xrange(6)]

class vtmThreadCtrl(wx.Panel,vtLog.vtLogOrigin):
    def __init__(self,*_args,**_kwargs):
        vtLog.vtLogOrigin.__init__(self)
        if 'size' not in _kwargs:
            sz=wx.Size(100,34)
            _kwargs['size']=sz
        else:
            sz=_kwargs['size']
        kwargs={}
        for s in ['id','name','parent','pos','size','style']:
            if s in _kwargs:
                kwargs[s]=_kwargs[s]
        apply(wx.Panel.__init__,(self,) + _args,kwargs)
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #self.SetMinSize((-1,48))
        bxsMain = wx.FlexGridSizer(cols=1, hgap=0, rows=2, vgap=0)
        bxsMain.AddGrowableRow(0,1)
        bxsMain.AddGrowableCol(0,1)
        #bxsMain.SetFlexibleDirection(wx.BOTH)
        #bxsMain = wx.GridBagSizer(hgap=4, vgap=4)
        
        
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.thrProc = vidarc.tool.art.vtThrobber.vtThrobberDelay(id=wxID_VTTHREADCTRLTHRPROC,
              name=u'thrProc', parent=self, pos=wx.Point(0, 158),
              size=wx.Size(15, 15), style=0)
        bxs.AddWindow(self.thrProc, 0, border=8, flag=wx.TOP)
        
        self.cbStart = wx.lib.buttons.GenBitmapButton(bitmap=vtArtState.getBitmap(vtArtState.Start),
              id=wxID_VTTHREADCTRLCBSTART, 
              name=u'cbExec', parent=self, pos=wx.Point(70, 158),
              size=wx.Size(30, 30), style=0)
        self.cbStart.SetMinSize(wx.Size(-1, -1))
        #self.cbStart.Enable(False)
        self.cbStart.Bind(wx.EVT_BUTTON, self.OnCbStartButton,
              id=wxID_VTTHREADCTRLCBSTART)
        bxs.AddWindow(self.cbStart, 0, border=4, flag=wx.LEFT|wx.RIGHT)
        
        self.cbStop = wx.lib.buttons.GenBitmapButton(bitmap=vtArtState.getBitmap(vtArtState.Stop),
              id=wxID_VTTHREADCTRLCBSTOP, 
              name=u'cbStop', parent=self, pos=wx.Point(150, 158),
              size=wx.DefaultSize, style=0)
        self.cbStop.Bind(wx.EVT_BUTTON, self.OnCbStopButton,
              id=wxID_VTTHREADCTRLCBSTOP)
        self.cbStop.Enable(False)
        bxs.AddWindow(self.cbStop, 0, border=4, flag=wx.RIGHT)
        
        #bxsTxt = wx.BoxSizer(orient=wx.HORIZONTAL)
        bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_MENU)
        #print bkgCol
        style=wx.TE_READONLY#|wx.NO_BORDER
        self.txtVal = wx.TextCtrl(id=wxID_VTTHREADCTRLTXTVAL,
              name=u'txtVal', parent=self, pos=wx.DefaultPosition,
              size=(40,32),#wx.DefaultSize, 
              style=style, value=u'')
        self.txtVal.SetMinSize(wx.Size(-1, -1))
        self.txtVal.SetBackgroundColour(bkgCol)
        #self.txtVal.Enable(False)
        bxs.AddWindow(self.txtVal, 3, border=4, flag=wx.RIGHT|wx.EXPAND)
        #bxsTxt.AddWindow(self.txtVal, 3, border=4, flag=wx.RIGHT|wx.EXPAND)

        self.txtCount = wx.TextCtrl(id=wxID_VTTHREADCTRLTXTCOUNT,
              name=u'txtCount', parent=self, pos=wx.DefaultPosition,
              size=(10,32),#,wx.DefaultSize, 
              style=style|wx.TE_RIGHT, value=u'')
        self.txtCount.SetMinSize(wx.Size(-1, -1))
        #szTxt=self.txtCount.GetBestSize()
        #self.txtCount.SetInitialBestSize(wx.Size(20, szTxt[1]))
        self.txtCount.SetBackgroundColour(bkgCol)
        #self.txtCount.Enable(False)
        bxs.AddWindow(self.txtCount, 1, border=0, flag=wx.RIGHT|wx.EXPAND)
        #bxsTxt.AddWindow(self.txtCount, 1, border=0, flag=wx.RIGHT|wx.EXPAND)
        #bxs.AddSizer(bxsTxt, 1, border=0, flag=wx.EXPAND)
        bxsMain.AddSizer(bxs, 1, border=0, flag=wx.EXPAND)
        #bxsMain.AddSizer(bxs, (0,0),(1,1), border=2, flag=wx.EXPAND|wx.TOP)
        
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.pbProc = wx.Gauge(id=wxID_VTTHREADCTRLPBPROC,
              name=u'pbProc', parent=self, pos=wx.DefaultPosition,
              range=1000, size=wx.DefaultSize, style=wx.GA_HORIZONTAL|wx.GA_SMOOTH)
        self.pbProc.SetMinSize(wx.Size(-1, 8))
        self.pbProc.SetMaxSize(wx.Size(-1, 12))
        bxs.AddWindow(self.pbProc, 1, border=4, flag=wx.TOP|wx.EXPAND)
        bxsMain.AddSizer(bxs, 1, border=2, flag=wx.EXPAND|wx.TOP)
        #bxsMain.AddSizer(bxs, (1,0),(1,1), border=2, flag=wx.EXPAND|wx.TOP)
        bxsMain.AddGrowableRow(0,1)
        bxsMain.AddGrowableCol(0,1)
        self.SetSizer(bxsMain)
        #self.SetSize(sz)
        bxsMain.Layout()
        #bxsMain.Fit(self)
        #wx.CallAfter(bxsMain.Layout)
        self.thd2Ctrl=None
        self.funcStart=None
        self.funcStop=None
        self.fProcLast=-1.0
    def GetCtrlSizer(self):
        bxsMain=self.GetSizer()
        bxs=bxsMain.GetChildren()[0].GetSizer()
        return bxs
    def InsertWidget(self,pos,wid,iProp=0,iBorder=0,iFlag=0,bCenter=False,bLeft=False,bRight=False):
        bxs=self.GetCtrlSizer()
        if bLeft:
            iFlag|=wx.LEFT
        if bRight:
            iFlag|=wx.RIGHT
        if bCenter:
            iFlag|=wx.CENTRE
        if iProp>0:
            iFlag|=wx.EXPAND
        bxs.Insert(pos,wid,iProp,border=iBorder,flag=iFlag)
        bxs.Layout()
    def SetFuncStart(self,func,*args,**kwargs):
        if func is None:
            self.funcStart=None
        else:
            self.funcStart=(func,args,kwargs)
    def SetFuncStop(self,func,*args,**kwargs):
        if func is None:
            self.funcStop=None
        else:
            self.funcStop=(func,args,kwargs)
    def GetThread(self):
        return self.thd2Ctrl
    def SetThread(self,thd):
        if self.thd2Ctrl is not None:
            vtLog.vtLogOrigin.__init__(self)
            EVT_VT_THREAD_PROC_DISCONNECT(thd.par,self.OnThreadProc)
            EVT_VT_THREAD_ABORTED_DISCONNECT(thd.par,self.OnThreadAborted)
            EVT_VT_THREAD_FINISHED_DISCONNECT(thd.par,self.OnThreadFinished)
        if thd is not None:
            self.thd2Ctrl=thd
            if thd.par is None:
                thd.SetParent(self)
            vtLog.vtLogOrigin.__init__(self,thd.GetOrigin(),'ctrl')
            EVT_VT_THREAD_PROC(thd.par,self.OnThreadProc)
            EVT_VT_THREAD_ABORTED(thd.par,self.OnThreadAborted)
            EVT_VT_THREAD_FINISHED(thd.par,self.OnThreadFinished)
    def OnThreadProc(self,evt):
        evt.Skip()
        try:
            if evt.GetCount() is None:
                t=type(evt.GetAct())
                if t==types.StringType or t==types.UnicodeType:
                    self.txtVal.SetValue(evt.GetAct())
            else:
                if evt.GetCount()!=0:
                    v=100.0*(evt.GetAct()/float(evt.GetCount()))
                else:
                    v=evt.GetAct()
                #print v,self.fProcLast,abs(v-self.fProcLast)
                if abs(v-self.fProcLast)>0.01:
                    self.fProcLast=v
                    #print evt.GetAct(),evt.GetCount(),v
                    self.txtCount.SetValue(u'%6.2f'%v)
                    #self.txtCount.SetValue(u'%d - %d'%(evt.GetAct(),evt.GetCount()))
                    self.pbProc.SetValue(evt.GetNormalized())
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnThreadAborted(self,evt):
        evt.Skip()
        try:
            vtLog.vtLngCur(vtLog.INFO,'',self.GetOrigin())
            self.txtVal.SetValue('aborted')
            self.pbProc.SetValue(0)
            self.__stop__()
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnThreadFinished(self,evt):
        evt.Skip()
        try:
            vtLog.vtLngCur(vtLog.INFO,'',self.GetOrigin())
            self.txtVal.SetValue('finished')
            self.pbProc.SetValue(0)
            self.__stop__()
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def Start(self):
        try:
            vtLog.vtLngCur(vtLog.INFO,'',self.GetOrigin())
            if self.thd2Ctrl is not None:
                self.thd2Ctrl.Start()
            self.__start__(not self.thd2Ctrl.IsRunning())
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def __start__(self,bClr=True):
        vtLog.vtLngCur(vtLog.DEBUG,'',self.GetOrigin())
        if bClr==True:
            self.txtVal.SetValue(u'')
            self.txtCount.SetValue(u'')
            self.pbProc.SetValue(0)
        self.thrProc.Start()
        self.cbStart.Enable(False)
        self.cbStop.Enable(True)
    def __stop__(self,bEnStart=True,bClr=True):
        vtLog.vtLngCur(vtLog.DEBUG,'',self.GetOrigin())
        if bEnStart:
            if bClr==True:
                self.txtVal.SetValue(u'')
                self.txtCount.SetValue(u'')
            self.pbProc.SetValue(0)
            self.thrProc.Stop()
            self.cbStart.Enable(True)
        self.cbStop.Enable(False)
        self.fProcLast=-1
    def EnableStart(self,bFlag=True):
        self.cbStart.Enable(bFlag)
    def EnableStop(self,bFlag=True):
        self.cbStop.Enable(bFlag)
    def Do(self,f,*args,**kwargs):
        if self.thd2Ctrl is not None:
            self.thd2Ctrl.Do(f,*args,**kwargs)
            self.thd2Ctrl.CallBack(self.Start,)
            #if self.thd2Ctrl.IsRunning()==False:
            #    
        else:
            f(*args,**kwargs)
    def OnCbStartButton(self,evt):
        evt.Skip()
        try:
            vtLog.vtLngCur(vtLog.INFO,'',self.GetOrigin())
            self.Start()
            if self.funcStart is not None:
                func,args,kwargs=self.funcStart
                func(*args,**kwargs)
            #self.Start()
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnCbStopButton(self,evt):
        evt.Skip()
        try:
            vtLog.vtLngCur(vtLog.INFO,'',self.GetOrigin())
            if self.funcStop is not None:
                func,args,kwargs=self.funcStop
                func(*args,**kwargs)
            if self.thd2Ctrl is not None:
                self.thd2Ctrl.Stop()
                self.__stop__(bEnStart=not self.thd2Ctrl.IsRunning(),bClr=False)
            else:
                self.__stop__(bEnStart=True,bClr=False)
        except:
            vtLog.vtLngTB(self.GetOrigin())

