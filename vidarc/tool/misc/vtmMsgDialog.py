#Boa:Dialog:vtmMsgDialog
#----------------------------------------------------------------------------
# Name:         vtmMsgDialog.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20070703
# CVS-ID:       $Id: vtmMsgDialog.py,v 1.4 2007/07/24 11:22:06 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import cStringIO

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.tool.gui.vtgCore as vtgCore

def create(parent):
    return vtmMsgDialog(parent)

#----------------------------------------------------------------------
def getApplData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x00\x96IDAT(\x91c\\~n\x07\x03)\x80\x05S\xa8}\xcf|8\xbb\xd2%\x11M\x96\tS\x03\
\\\x11\xa6j\x06\x06\x06F\xb8\x93\x90\r\xc6jJ\xfb\x9e\xf9\x95.\x89Xl\xc0\n\
\xe0\xc6\xa1k8_\xb2\xf2|\xc9J\\\\\xa8\x06d\xc7\x18\xf6\x84\xa3\x19\x01\x11\
\x81\xfb\x87\t\xd3\x03\x86=\xe1\x10S\xcf\x97\xacD\xd6\x0f\xd1C\xac\x1fP\x9c\
\xc4\x80\x11\x82\x10K\xd0\x9c\x07q\x05\x13\xa6j\xac\x00g(![\x82\xcfI\x04\x01\
\xdc\x15\x8cX\x13\x1f\xc4\x01X\x9d\x8a\xc5\x06\xb8s\xb1&\x16\xec6\xe0\x01\
\x00\x98\xff>\xd3\x83on\xdb\x00\x00\x00\x00IEND\xaeB`\x82' 

def getApplBitmap():
    return wx.BitmapFromImage(getApplImage())

def getApplImage():
    stream = cStringIO.StringIO(getApplData())
    return wx.ImageFromStream(stream)

_icon=None
def getApplIcon():
    global _icon
    if _icon is None:
        _icon = wx.EmptyIcon()
        _icon.CopyFromBitmap(getApplBitmap())
    return _icon
    
vtmxEVT_MISC_OK=wx.NewEventType()
def EVT_MISC_OK(win,func):
    win.Connect(-1,-1,vtmxEVT_MISC_OK,func)
def EVT_MISC_OK_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtmxEVT_MISC_OK,func)
class vtmxMiscOk(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_MISC_OK(<widget_name>, xxx)
    """
    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtmxEVT_MISC_OK)

vtmxEVT_MISC_CANCEL=wx.NewEventType()
def EVT_MISC_CANCEL(win,func):
    win.Connect(-1,-1,vtmxEVT_MISC_CANCEL,func)
def EVT_MISC_CANCEL_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtmxEVT_MISC_CANCEL,func)
class vtmxMiscCancel(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_MISC_CANCEL(<widget_name>, xxx)
    """
    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtmxEVT_MISC_CANCEL)

[wxID_VTMMSGDIALOG, wxID_VTMMSGDIALOGBMPINFO, wxID_VTMMSGDIALOGCBAPPLY, 
 wxID_VTMMSGDIALOGCBCANCEL, wxID_VTMMSGDIALOGLBLMSG, 
] = [wx.NewId() for _init_ctrls in range(5)]

class vtmMsgDialog(wx.Dialog):
    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_fgsMain_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.bmpInfo, 0, border=8, flag=wx.ALL)
        parent.AddWindow(self.lblMsg, 0, border=4, flag=wx.EXPAND | wx.ALL)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.BOTTOM | wx.ALIGN_CENTER | wx.RIGHT | wx.LEFT | wx.TOP)

    def _init_coll_fgsMain_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(1)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsMain = wx.FlexGridSizer(cols=2, hgap=0, rows=2, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsMain_Items(self.fgsMain)
        self._init_coll_fgsMain_Growables(self.fgsMain)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.fgsMain)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTMMSGDIALOG, name=u'vtmMsgDialog',
              parent=prnt, pos=wx.Point(47, 8), size=wx.Size(244, 97),
              style=wx.STAY_ON_TOP | wx.DEFAULT_DIALOG_STYLE,
              title=u'vtmMsgDialog')
        self.SetClientSize(wx.Size(236, 70))

        self.bmpInfo = wx.StaticBitmap(bitmap=wx.EmptyBitmap(32,32),
              id=wxID_VTMMSGDIALOGBMPINFO, name=u'bmpInfo', parent=self,
              pos=wx.Point(8, 8), size=wx.Size(32, 32), style=0)

        self.lblMsg = wx.StaticText(id=wxID_VTMMSGDIALOGLBLMSG, label=u' ',
              name=u'lblMsg', parent=self, pos=wx.Point(36, 4),
              size=wx.Size(196, 24), style=wx.TE_MULTILINE | wx.TE_READONLY )
        self.lblMsg.SetMinSize(wx.Size(-1, -1))

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Apply),
              id=wxID_VTMMSGDIALOGCBAPPLY, label=_(u'Ok'), name=u'cbApply',
              parent=self, pos=wx.Point(36, 36), size=wx.Size(76, 30), style=0)
        self.cbApply.SetMinSize(wx.Size(-1, -1))
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VTMMSGDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Cancel),
              id=wxID_VTMMSGDIALOGCBCANCEL, label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(144, 36),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.SetMinSize(wx.Size(-1, -1))
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VTMMSGDIALOGCBCANCEL)
        self._init_sizers()
    def __init__(self, parent,sMsg,sCaption,style=wx.OK|wx.CANCEL,bmp=None,sApply=None,sCancel=None,bFindCore=True):
        global _
        _=vtLgBase.assignPluginLang('vtMisc')
        if bFindCore:
            widDlgPar=vtgCore.vtGuiCoreGetBaseWidget(parent)
        else:
            widDlgPar=parent
        self._init_ctrls(widDlgPar)
        try:
            if style & wx.YES_NO:
                self.bYesNo=True
                self.cbApply.SetLabel(_(u'Yes'))
                self.cbCancel.SetLabel(_(u'No'))
                if style & wx.YES_DEFAULT:
                    self.SetDefaultItem(self.cbApply)
                    self.cbApply.SetFocus()
                elif style & wx.NO_DEFAULT:
                    self.SetDefaultItem(self.cbCancel)
                    self.cbCancel.SetFocus()
                else:
                    pass
            else:
                self.bYesNo=False
                wid=None
                if (style & wx.CANCEL)==0:
                    self.cbCancel.Show(False)
                    wid=self.cbCancel
                if (style & wx.OK)==0:
                    self.cbApply.Show(False)
                    wid=self.cbApply
                if wid is not None:
                    self.SetDefaultItem(wid)
                    wid.SetFocus()
            if bmp is None:
                if style & wx.ICON_EXCLAMATION:
                    bmp=wx.ArtProvider.GetBitmap(wx.ART_WARNING,size=(32,32))
                elif style & wx.ICON_ERROR:
                    bmp=wx.ArtProvider.GetBitmap(wx.ART_ERROR,size=(32,32))
                elif style & wx.ICON_INFORMATION:
                    bmp=wx.ArtProvider.GetBitmap(wx.ART_INFORMATION,size=(32,32))
                elif style & wx.ICON_INFORMATION:
                    bmp=wx.ArtProvider.GetBitmap(wx.ART_INFORMATION,size=(32,32))
                elif style & wx.ICON_QUESTION:
                    bmp=wx.ArtProvider.GetBitmap(wx.ART_QUESTION,size=(32,32))
            if bmp is not None:
                self.bmpInfo.SetBitmap(bmp)
            else:
                self.bmpInfo.Show(False)
            #img=wx.EmptyIcon()
            #img.CopyFromBitmap(bmp)
            self.SetIcon(getApplIcon())#img)
            self.lblMsg.SetLabel(sMsg)
            self.SetTitle(sCaption)
            self.fgsMain.Layout()
            self.fgsMain.Fit(self)
        except:
            vtLog.vtLngTB(self.GetName())
        self.Centre()
    def OnCbApplyButton(self, event):
        event.Skip()
        try:
            if self.IsModal():
                if self.bYesNo:
                    self.EndModal(wx.ID_YES)
                else:
                    self.EndModal(wx.ID_OK)
            else:
                wx.PostEvent(self,vtmxMiscOk(self))
                self.Show(False)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbCancelButton(self, event):
        event.Skip()
        try:
            if self.IsModal():
                if self.bYesNo:
                    self.EndModal(wx.ID_NO)
                else:
                    self.EndModal(wx.ID_CANCEL)
            else:
                wx.PostEvent(self,vtmxMiscCancel(self))
                self.Show(False)
        except:
            vtLog.vtLngTB(self.GetName())
    def BindEvents(self,funcOk=None,funcCancel=None):
        if funcOK is not None:
            EVT_MISC_OK(self,funcOk)
        if funcCancel is not None:
            EVT_MISC_CANCEL(self,funcCancel)
