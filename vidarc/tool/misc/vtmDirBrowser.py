#----------------------------------------------------------------------
# Name:         vtmDirBrowser.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20100502
# CVS-ID:       $Id: vtmDirBrowser.py,v 1.2 2010/05/02 20:43:32 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import os

from vidarc.tool.gui.vtgPanel import vtgPanel
import vidarc.tool.art.vtArt as vtArt

class vtmDirBrowser(vtgPanel):
    def __initCtrl__(self,*args,**kwargs):
        lWid=[]
        self.bNew=kwargs.get('bNew',False)
        self.bMulti=kwargs.get('bMulti',False)
        self.bPosixFN=kwargs.get('bPosixFN',True)
        self.sMsg=kwargs.get('sMsg',_(u'choose directory'))
        #self.sWildCard=kwargs.get('sWildCard',_(u'xml files|*.xml|all files|*.*'))
        self.sDftDN=kwargs.get('sDftDN',os.path.expanduser(u'~'))
        self.sDN=kwargs.get('sDN',self.sDftDN)
        
        lWid.append(('txt',(0,0),(1,1),{'name':'txtDN','value':self.sDN,},
                {'txt':self.OnTxtDirNameText,'ent':self.OnTxtDirNameEnter}))
        lWid.append(('cbBmp',(0,1),(1,1),
                    {'name':'cbBrowse','bitmap':vtArt.getBitmap(vtArt.Browse),},
                    {'btn':self.OnCbBrowseClick}))
        vtgPanel.__initCtrl__(self,lWid=lWid)
        return None,[(0,1)]
    def GetValue(self):
        return self.sDN
    def GetDN(self):
        try:
            return self.sDN
        except:
            return None
    def SetValue(self,sDN):
        try:
            if self.bPosixFN==True:
                sDN=sDN.replace('\\','/')
            else:
                sDN=sDN.replace('/','\\')
            self.sDN=sDN
            vtgPanel.SetValue(self,'txtDN',self.sDN)
        except:
            self.__logTB__()
    def OnTxtDirNameText(self,evt):
        try:
            self.sDN=self.txtDN.GetValue()
        except:
            self.__logTB__()
    def OnTxtDirNameEnter(self,evt):
        try:
            self.sDN=self.txtDN.GetValue()
            self.Post('dir',self.sDN)
        except:
            self.__logTB__()
    def OnCbBrowseClick(self,evt):
        try:
            self.__logDebug__('sDN:%r'%(self.sDN))
            if self.ChooseDir() is not None:
                self.__logDebug__('sDN:%r'%(self.sDN))
                self.txtDN.SetValue(self.sDN)
        except:
            self.__logTB__()
    def ChooseDir(self):
        try:
            self.__logInfo__(''%())
            if self.bNew==True:
                iStyle=wx.DD_DEFAULT_STYLE|wx.DD_NEW_DIR_BUTTON 
            else:
                iStyle=wx.DEFAULT_DIALOG_STYLE|wx.RESIZE_BORDER
            #if self.bMulti==True:
            #    iStyle|=wx.MULTIPLE
            dlg=wx.DirDialog(self,message=self.sMsg,
                #defaultDir='.',
                defaultPath=self.sDftDN,
                #wildcard=self.sWildCard,
                style=iStyle)
            dlg.Centre()
            try:
                if len(self.sDN)>0:
                    dlg.SetPath(self.sDN)
            except:
                self.__logTB__()
            iRet=dlg.ShowModal()
            if iRet==wx.ID_OK:
                sDN=dlg.GetPath()
                if self.bPosixFN==True:
                    sDN=sDN.replace('\\','/')
                dlg.Destroy()
                self.sDN=sDN
                self.Post('dir',self.sDN)
                return sDN
                if self.bMulti==True:
                    lTmpFN=dlg.GetFilenames()
                    if self.bPosixFN==True:
                        lFN=['/'.join([sTmpDN,sTmpFN]) for sTmpFN in lTmpFN]
                    else:
                        lFN=[os.path.join(sTmpDN,sTmpFN) for sTmpFN in lTmpFN]
                    lFN.sort()
                    dlg.Destroy()
                    self.sFN=lFN[0]
                    self.Post('files',lFN)
                    return lFN
                else:
                    sTmpFN=dlg.GetFilename()
                    if self.bPosixFN==True:
                        sFN='/'.join([sTmpDN,sTmpFN])
                    else:
                        sFN=os.path.join(sTmpDN,sTmpFN)
                    dlg.Destroy()
                    self.sFN=sFN
                    self.Post('file',self.sFN)
                    return sFN
            dlg.Destroy()
        except:
            self.__logTB__()
        return None
