#----------------------------------------------------------------------------
# Name:         vtmCompare.py
# Purpose:      core class to provide a standard compare interface
# Author:       Walter Obweger
#
# Created:      20070515
# CVS-ID:       $Id: vtmCompare.py,v 1.1 2007/05/23 13:01:32 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.lang.vtLgBase as vtLgBase

import vidarc.tool.misc.images as imgMisc

class vtmCompare:
    def __init__(self,dMap):
        """
        dMap={  0x01:{'name':'','translation':'','image':None},
                0x02:{'name':'','translation':'','image':None},
                }
        """
        self.dMapRes={}
        self.dMapRes.update(dMap)
    def __del__(self):
        pass
    def GetPossibleActionLst(self):
        return None
    def GetMapResDict(self):
        return self.dMapRes
    def GetPossibleResult4DispLst(self):
        l=[]
        for k,v in self.dMapRes.iteritems():
            ll=[k]
            if 'translation' in v:
                ll.append(v['translation'])
            else:
                ll.append(v['name'])
            if 'image' in v:
                ll.append(v['image'])
            l.append(ll)
        l.sort()
        return l
    def ConvertResultItem4Disp(self,tRes):
        """return 
        """
        iResMain=tRes[0]
        iResSub=tRes[1]
        lInfos=None
        return iResMain,iResSub,lInfos
    def GetColumns4DispLst(self):
        """return list of column headers and size
        [('header0',10),('header2',-10)]
        negative size values indicate a percentage size
        """
        return None
    def DoCmp(self,x,y,*args,**kwargs):
        return 0
    def DoProc(self,t,x,y,*args,**kwargs):
        return 0

class vtmCompareList(vtmCompare):
    def Do(self,x,y,*args,**kwargs):
        pass
class vtmCompareDict(vtmCompare):
    VERBOSE=0
    def __init__(self,oCmp):
        global _
        _=vtLgBase.assignPluginLang('vtMisc')
        vtmCompare.__init__(self,{
            0x0001:{
                    'name':'ok',
                    'translation':_(u'okay'),
                    'image':imgMisc.getCmpXYlinkedBitmap(),
                    },
            0x0002:{
                    'name':'missInX',
                    'translation':_('missing in x'),
                    'image':imgMisc.getCmpMissingXBitmap(),
                    },
            0x0004:{
                    'name':'missInY',
                    'translation':_('missing in y'),
                    'image':imgMisc.getCmpMissingYBitmap(),
                    },
                })
        self.oCmp=oCmp
        self.Clear()
    def Clear(self):
        self.dRes={}
    def GetResultDict(self):
        return self.dRes
    def GetCompareItem(self):
        return self.oCmp
    def GetColumns4DispLst(self):
        """return list of column headers and size
        [('header0',10),('header2',-10)]
        negative size values indicate a percentage size
        """
        return None
    def ConvertResultItem4Disp(self,key,tRes):
        """return 
        """
        return self.oCmp.ConvertResultItem4Disp(tRes)
    def DoCmp(self,dX,dY,*args,**kwargs):
        self.Clear()
        keysY=dY.keys()
        for k,x in dX.iteritems():
            if k in dY:
                y=dY[k]
                keysY.remove(k)
                iRes,iAction=self.DoCmpEntry(x,y,*args,**kwargs)
                self.dRes[k]=[0x0001,iRes,iAction]
            else:
                self.dRes[k]=[0x0004,0,0]
        for k in keysY:
            self.dRes[k]=[0x0002,0,0]
        return self.dRes
    def DoCmpEntry(self,x,y,*args,**kwargs):
        return self.oCmp.DoCmp(x,y,*args,**kwargs)
    def DoProc(self,dX,dY,*args,**kwargs):
        for k,t in self.dRes.iteritems():
            if k in dX:
                x=dX[k]
            else:
                x=None
            if k in dY:
                y=dY[k]
            else:
                y=None
            iRes=self.DoProcEntry(k,t,x,y,*args,**kwargs)
            t.append(iRes)
    def DoProcEntry(self,k,t,x,y,*args,**kwargs):
        return self.oCmp.DoProc(t,x,y,*args,**kwargs)
