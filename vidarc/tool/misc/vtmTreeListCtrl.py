#----------------------------------------------------------------------------
# Name:         vtmTreeListCtrl.py
# Purpose:      tree list control with stretch calculation
# Author:       Walter Obweger
#
# Created:      20080320
# CVS-ID:       $Id: vtmTreeListCtrl.py,v 1.19 2010/07/25 11:20:53 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.gizmos
import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.misc.vtmColumnStretch import vtmColumnStretch

import types

# defined event for vtmTreeListCtrl item selected
wxEVT_VTMTREELISTCTRL_ITEM_SELECTED=wx.NewEventType()
vEVT_VTMTREELISTCTRL_ITEM_SELECTED=wx.PyEventBinder(wxEVT_VTMTREELISTCTRL_ITEM_SELECTED,1)
def EVT_VTMTREELISTCTRL_ITEM_SELECTED(win,func):
    win.Connect(-1,-1,wxEVT_VTMTREELISTCTRL_ITEM_SELECTED,func)
def EVT_VTMTREELISTCTRL_ITEM_SELECTED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,wxEVT_VTMTREELISTCTRL_ITEM_SELECTED)
class vtmTreeListCtrlItemSelected(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VTMTREELISTCTRL_ITEM_SELECTED(<widget_name>, self.OnItemSel)
    """

    def __init__(self,obj,tn,data):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTMTREELISTCTRL_ITEM_SELECTED)
        self.obj=obj
        self.treeNode=tn
        self.treeNodeData=data
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,obj.GetOrigin())
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def GetTree(self):
        return self.obj
    def GetTreeItem(self):
        return self.treeNode
    def GetTreeItemData(self):
        try:
            return self.treeNodeData
        except:
            return None

class vtmTreeListCtrl(vtLog.vtLogMixIn,vtmColumnStretch,wx.gizmos.TreeListCtrl):
    def __init__(self,id=-1,parent=None,pos=(0,0),size=(400,100),name='lstLog',style=0,
                bAutoExpand=False,bForceChildren=False,bButtons=False,bMultiple=False):
        if bButtons==True:
            style|=wx.TR_HAS_BUTTONS
        if bMultiple==True:
            style|=wx.TR_MULTIPLE
        wx.gizmos.TreeListCtrl.__init__(self,id=id, name=name,parent=parent, pos=pos, size=size,
              style=style)
        if style & wx.TR_MULTIPLE:
            self.__getSelection__=self.__getSelectionMulti__
        else:
            self.__getSelection__=self.__getSelectionSng__
        vtmColumnStretch.__init__(self)
        self._GetColCount=self.GetColumnCount
        self._GetColWidth=self.GetColumnWidth
        self._SetColWidth=self.SetColumnWidth
        wx.CallAfter(wx.EVT_SIZE,self,self.OnSizeStretch)
        wx.CallAfter(self.OnSizeStretch,None)
        self._originLogging=self.GetName()
        self.MAP=None
        self.IMG_EMPTY=-1
        self.FORCE_CHILDREN=bForceChildren
        self._bAutoExpand=bAutoExpand
        wx.EVT_TREE_SEL_CHANGED(self, self.GetId(), self.OnTcNodeTreeSelChanged)
    def BindEvent(self,name,func,par=None):
        if name.startswith('tr_'):
            if name in ['tr_item_selected','tr_item_sel',]:
                self.Bind(wx.EVT_TREE_SEL_CHANGED,func, par or self)
            if name in ['tr_item_selecting']:
                self.Bind(wx.EVT_TREE_SEL_CHANGING,func, par or self)
            if name in ['tr_item_expanded']:
                self.Bind(wx.EVT_TREE_ITEM_EXPANDED,func, par or self)
            if name in ['tr_item_expanding']:
                self.Bind(wx.EVT_TREE_ITEM_EXPANDING,func, par or self)
        if name.startswith('lst_'):
            if name in ['lst_item_sel','lst_item_selected']:
                self.Bind(wx.EVT_LIST_ITEM_SELECTED,func, par or self)
            elif name in ['lst_item_desel','lst_item_deselected']:
                self.Bind(wx.EVT_LIST_ITEM_DESELECTED,func, par or self)
            elif name in ['lst_item_right_click','lst_item_rgClk']:
                self.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK,func, par or self)
            elif name in ['lst_col_drag_begin']:
                self.Bind(wx.EVT_LIST_COL_BEGIN_DRAG,func, par or self)
            elif name in ['lst_col_dragging']:
                self.Bind(wx.EVT_LIST_COL_DRAGGING,func, par or self)
            elif name in ['lst_col_drag_end']:
                self.Bind(wx.EVT_LIST_COL_END_DRAG,func, par or self)
            elif name in ['lst_col_left_click']:
                self.Bind(wx.EVT_LIST_COL_LEFT_CLICK,func, par or self)
            elif name=='lst_col_right_click':
                self.Bind(wx.EVT_LIST_COL_RIGHT_CLICK,
                        func, par or self)
        else:
            #vtgPanelMixinWX.BindEvent(self,name,func,par=par)
            pass
    def BindEvents(self,funcSel=None):
        if funcSel is not None:
            EVT_VTMTREELISTCTRL_ITEM_SELECTED(self,funcSel)
    def UnBindEvents(self,funcSel=None):
        if funcSel is not None:
            EVT_VTMTREELISTCTRL_ITEM_SELECTED_DISCONNECT(self,funcSel)
    def SetName(self,name):
        wx.gizmos.TreeListCtrl.SetName(self,name)
        self._originLogging=name
    def GetOrigin(self):
        return self._originLogging
    def SetMap(self,l):
        self.MAP=l
    def SetForceChildren(self,bFlag=True):
        self.FORCE_CHILDREN=bFlag
    def SetAutoExpand(self,bFlag=True):
        self._bAutoExpand=bFlag
    def GetAutoExpand(self):
        return self._bAutoExpand
    def OnTcNodeTreeSelChanged(self,evt):
        evt.Skip()
        try:
            self.__logDebug__(''%())
            ti=evt.GetItem()
            data=self.GetPyData(ti)
            if self._bAutoExpand==True:
                if self.ItemHasChildren(ti):
                    if self.IsExpanded(ti):
                        self.Collapse(ti)
                    else:
                        self.Expand(ti)
            wx.PostEvent(self,vtmTreeListCtrlItemSelected(self,ti,data))
        except:
            self.__logTB__()
    def CreateImageList(self,lDef,which=None):
        """ 
        lDef
            [
            ['name0',bmp0],
            ['name1',[bmp0,bmp1],
            ]
        """
        dImg={}
        if (which is None) or (which==wx.IMAGE_LIST_SMALL):
            imgLst=wx.ImageList(16,16)
        else:
            imgLst=wx.ImageList(16,16)
            which=None
        for sK,it in lDef:
            t=type(it)
            if t==types.TupleType:
                lImg=[imgLst.Add(bmp) for bmp in it]
            elif t==types.ListType:
                lImg=[imgLst.Add(bmp) for bmp in it]
            else:
                lImg=imgLst.Add(it)
            dImg[sK]=lImg
        return imgLst,dImg
    def AddColumn(self,text=''):
        wx.gizmos.TreeListCtrl.AddColumn(self,text)
    def _getAlignment(self,iAlign):
        if iAlign is None:
            iAlign=wx.ALIGN_LEFT
        elif iAlign<0:
            if iAlign==-1:
                iAlign=wx.ALIGN_LEFT
            elif iAlign==-2:
                iAlign=wx.ALIGN_RIGHT
            elif iAlign==-3:
                iAlign=wx.ALIGN_CENTRE
            else:
                self.__logError__('known format;%d',(iAlign))
                iAlign=wx.ALIGN_LEFT
        if wx.VERSION < (2,8):
            if iAlign==wx.ALIGN_LEFT:
                iAlign=wx.LIST_FORMAT_LEFT
            elif iAlign==wx.ALIGN_CENTRE:
                iAlign=wx.LIST_FORMAT_CENTRE
                #iAlign=wx.LIST_FORMAT_RIGHT
            elif iAlign==wx.ALIGN_RIGHT:
                iAlign=wx.LIST_FORMAT_RIGHT
            else:
                self.__logError__('unknown format;%d;0x%x'%(iAlign,iAlign))
                iAlign=wx.LIST_FORMAT_LEFT
        else:
            pass
        return iAlign
    def SetColumnAlignment(self,iCol,iAlign):
        wx.gizmos.TreeListCtrl.SetColumnAlignment(self,iCol,
                    self._getAlignment(iAlign))
    def SetImageList(self,imgLst,iImgEmptyIdx=0):
        wx.gizmos.TreeListCtrl.SetImageList(self,imgLst)
        self.IMG_EMPTY=iImgEmptyIdx
    def __getHier__(self,tr,ti,l):
        l.append(tr.GetItemText(ti,0))
        tp=tr.GetItemParent(ti)
        if tp.IsOk():
            self.__getHier__(tr,tp,l)
    def GetHier(self,ti):
        l=[]
        self.__getHier__(self,ti,l)
        l.reverse()
        return l
    def __getTreeItemByHier__(self,tr,ti,lHier):
        if self.__isLogDebug__():
            self.__logDebug__(';'.join([lHier[0],tr.GetItemText(ti,0),
                    '.'.join(lHier)]))
        if lHier[0]==tr.GetItemText(ti,0):
            if len(lHier)>1:
                t=tr.GetFirstChild(ti)
                while t[0].IsOk():
                    r=self.__getTreeItemByHier__(tr,t[0],lHier[1:])
                    if r is not None:
                        return r
                    t=tr.GetNextChild(ti,t[1])
            return ti
        return None
    def __setTreeItemDict__(self,tr,ti,d):
        #print tr.GetItemText(ti,0)
        for s,i in self.MAP:
            if s in d:
                v=d[s]
                t=type(v)
                if t==types.TupleType:
                    tr.SetItemText(ti,v[0],i)
                    tr.SetItemImage(ti,v[1],i,which=wx.TreeItemIcon_Normal)
                elif t==types.IntType:
                    tr.SetItemImage(ti,v,i,which=wx.TreeItemIcon_Normal)
                else:
                    tr.SetItemText(ti,v,i)
                    #tr.SetItemImage(ti,self.IMG_EMPTY,i,which=wx.TreeItemIcon_Normal)
        if 0 in d:
            tr.SetItemImage(ti,d[0],0,which=wx.TreeItemIcon_Normal)
        if -1 in d:
            tr.SetPyData(ti,d[-1])
        if -2 in d:
            self.__setValue__(tr,ti,d[-2])
    def __setTreeItemLst__(self,tr,ti,l):
        try:
            if l[0] is not None:
                tr.SetPyData(ti,l[0])
            i=0
            for v in l[1]:
                t=type(v)
                if t==types.TupleType:
                    tr.SetItemText(ti,v[0],i)
                    tr.SetItemImage(ti,v[1],i,which=wx.TreeItemIcon_Normal)
                elif t==types.IntType:
                    tr.SetItemImage(ti,v,i,which=wx.TreeItemIcon_Normal)
                else:
                    tr.SetItemText(ti,v,i)
                    #tr.SetItemImage(ti,self.IMG_EMPTY,i,which=wx.TreeItemIcon_Normal)
                i+=1
            iLen=len(l)
            if iLen>2:
                self.__setValue__(tr,ti,l[2])
        except:
            self.__logTB__()
            self.__logError__('l:%s;l[0]:%s;l[1]:%s'%(self.__logFmt__(l),
                    self.__logFmt__(l[0]),self.__logFmt__(l[1])))
    def __setTreeItemTup__(self,tr,ti,t):
        pass
    def SetItem(self,ti,i,v):
        if i<0:
            self.SetPyData(ti,v)
            return
        t=type(v)
        if t==types.TupleType:
            self.SetItemText(ti,v[0],i)
            self.SetItemImage(ti,v[1],i,which=wx.TreeItemIcon_Normal)
        elif t==types.IntType:
            self.SetItemImage(ti,v,i,which=wx.TreeItemIcon_Normal)
        else:
            self.SetItemText(ti,v,i)
    def GetItem(self,ti,i):
        if i<0:
            return self.GetPyData(ti)
        t=(self.GetItemText(ti,i),self.GetItemImage(ti,i))
        return t
    def Delete(self,ti,*args,**kwargs):
        #wx.gizmos.TreeListCtrl.SelectItem(self,ti,False)
        wx.gizmos.TreeListCtrl.Unselect(self)
        wx.gizmos.TreeListCtrl.UnselectAll(self)
        wx.gizmos.TreeListCtrl.Delete(self,ti,*args,**kwargs)
        #wx.gizmos.TreeListCtrl.UnselectAll(self)
    def __setTreeItem__(self,tr,ti,val):
        t=type(val)
        if t==types.DictType:
            self.__setTreeItemDict__(tr,ti,val)
        elif t==types.ListType:
            self.__setTreeItemLst__(tr,ti,val)
        elif t==types.TupleType:
            self.__setTreeItemTup__(tr,ti,val)
    def __setValue__(self,tr,tp,l,bRoot=False):
        for ll in l:
            if tp is None:
                ti=tr.AddRoot('')
            else:
                ti=tr.AppendItem(tp,'')
            if self.FORCE_CHILDREN:
                tr.SetItemHasChildren(ti,True)
            self.__setTreeItem__(tr,ti,ll)
            if tp is None:
                return
    def __getValue__(self,tr,ti,lHier,lMap,l):
            def getVal(tr,ti,lHier,i):
                if i is None:
                    return lHier
                if i==-1:
                    return tr.GetPyData(ti)
                elif i==-2:
                    return ti
                else:
                    return tr.GetItemText(ti,i)
            l.append([getVal(tr,ti,lHier,i) for i in lMap])
    def __getValueData__(self,tr,ti,l):
            data=tr.GetPyData(ti)
            if data is not None:
                l.append(data)
    def __getSelectionSng__(self,tr):
        l=[]
        def cmpSel(tr,ti,iMpdSub,l):
            if tr.IsSelected(ti):
                l.append(ti)
                return 1
            return 0
        iRet=tr.WalkDeepFirst(cmpSel,l)
        if iRet>0:
            return False,l[0]
        return False,None
    def __getSelectionMulti__(self,tr):
        l=[]
        def cmpSel(tr,ti,iMpdSub,l):
            if tr.IsSelected(ti):
                l.append(ti)
            return 0
        iRet=tr.WalkDeepFirst(cmpSel,l)
        if len(l)>0:
            return True,l
        return True,[]
    def __walkFind__(self,tr,ti,func,*args,**kwargs):
        if ti.IsOk()==False:
            return None
        t=tr.GetFirstChild(ti)
        while t[0].IsOk():
            if func(tr,t[0],*args,**kwargs):
                return t[0]
            if tr.ItemHasChildren(t[0]):
                r=self.__walkFind__(tr,t[0],func,*args,**kwargs)
                if r is not None:
                    return r
            t=tr.GetNextChild(ti,t[1])
        return None
    def __walk__(self,tr,ti,func,*args,**kwargs):
        if ti.IsOk()==False:
            return
        t=tr.GetFirstChild(ti)
        while t[0].IsOk():
            func(tr,t[0],*args,**kwargs)
            if tr.ItemHasChildren(t[0]):
                self.__walk__(tr,t[0],func,*args,**kwargs)
            t=tr.GetNextChild(ti,t[1])
    def __walkHier__(self,tr,ti,lHier,func,*args,**kwargs):
        if ti.IsOk()==False:
            return
        t=tr.GetFirstChild(ti)
        while t[0].IsOk():
            lH=lHier+[tr.GetItemText(t[0],0)]
            func(tr,t[0],lH,*args,**kwargs)
            if tr.ItemHasChildren(t[0]):
                self.__walkHier__(tr,t[0],lH,func,*args,**kwargs)
            t=tr.GetNextChild(ti,t[1])
    def __walkDeepFirst__(self,tr,ti,iMod,func,*args,**kwargs):
        if ti.IsOk()==False:
            return iMod
        t=tr.GetFirstChild(ti)
        iModRet=iMod
        while t[0].IsOk():
            if tr.ItemHasChildren(t[0]):
                iRet=self.__walkDeepFirst__(tr,t[0],iMod,func,*args,**kwargs)
                if iRet<0:
                    return iRet
                iModSub=max(iRet,iMod)
            else:
                iModSub=iMod
            iModRet=max(iModSub,iModRet)
            iRet=func(tr,t[0],iModSub,*args,**kwargs)
            if iRet<0:
                return iRet
            iModRet=max(iRet,iModRet)
            t=tr.GetNextChild(ti,t[1])
        return iModRet
    def __walkBreathFirst__(self,tr,ti,iLimit,func,*args,**kwargs):
        if ti.IsOk()==False:
            return None
        t=tr.GetFirstChild(ti)
        while t[0].IsOk():
            ret=func(tr,t[0],*args,**kwargs)
            if ret>0:
                return t[0]
            elif ret<0:
                return None
            t=tr.GetNextChild(ti,t[1])
        if iLimit>0:
            t=tr.GetFirstChild(ti)
            while t[0].IsOk():
                if tr.ItemHasChildren(t[0]):
                    r=self.__walkBreathFirst__(tr,t[0],iLimit-1,func,*args,**kwargs)
                    if r is not None:
                        return r
                t=tr.GetNextChild(ti,t[1])
        return None
    def WalkDeepFirst(self,func,*args,**kwargs):
        try:
            ti=self.GetRootItem()
            iRet=func(self,ti,0,*args,**kwargs)
            if iRet!=0:
                return iRet
            return self.__walkDeepFirst__(self,ti,0,func,*args,**kwargs)
        except:
            self.__logTB__()
        return None
    def WalkBreathFirst(self,ti,iLimit,func,*args,**kwargs):
        try:
            if ti is None:
                ti=self.GetRootItem()
            return self.__walkBreathFirst__(self,ti,iLimit,func,*args,**kwargs)
        except:
            self.__logTB__()
        return None
    def Walk(self,func,*args,**kwargs):
        try:
            self.__walk__(self,self.GetRootItem(),func,*args,**kwargs)
        except:
            self.__logTB__()
    def WalkHier(self,ti,lHier,func,*args,**kwargs):
        try:
            if ti is None:
                ti=self.GetRootItem()
            if lHier is None:
                tp=ti
                lHier=[]
                while tp is not None:
                    lHier.append(self.GetItemText(tp,0))
                    tp=self.GetItemParent(tp)
                    if tp.IsOk()==False:
                        break
                lHier.reverse()
                
            self.__walkHier__(self,ti,lHier,func,*args,**kwargs)
        except:
            self.__logTB__()
    def WalkFind(self,ti,func,*args,**kwargs):
        try:
            if ti is None:
                ti=self.GetRootItem()
            return self.__walkFind__(self,ti,func,*args,**kwargs)
        except:
            self.__logTB__()
        return None
    def WalkSel(self,func,*args,**kwargs):
        try:
            lTi=self.GetSelections()
            for ti in lTi:
                self.__walk__(self,ti,func,*args,**kwargs)
                func(self,ti,*args,**kwargs)
        except:
            self.__logTB__()
    def WalkSelDeepFirst(self,func,*args,**kwargs):
        try:
            lTi=self.GetSelections()
            iMod=0
            for ti in lTi:
                iRet=self.__walkDeepFirst__(self,ti,0,func,*args,**kwargs)
                if iRet<0:
                    return iRet
                iMod=max(iRet,iMod)
                iRet=func(self,ti,iMod,*args,**kwargs)
                if iRet<0:
                    return iRet
                iMod=max(iRet,iMod)
            return iMod
        except:
            self.__logTB__()
        return None
    def SetValue(self,l,lHier=None,bChildren=False):
        try:
            self.__logDebug__(''%())
            tr=self
            iCols=tr.GetColumnCount()
            
            if lHier is None:
                tr.DeleteAllItems()
                ti=tr.AddRoot('')
                self.__setTreeItem__(tr,ti,l)
                tr.Expand(ti)
            else:
                if type(lHier)==types.ListType:
                    ti=self.__getTreeItemByHier__(tr,tr.GetRootItem(),lHier)
                    if ti is None:
                        self.__logError__('%s;ti not found'%('.'.join(lHier)))
                        return
                else:
                    ti=lHier
                if bChildren==True:
                    self.__setValue__(tr,ti,l)
                else:
                    tc=tr.AppendItem(ti,'')
                    self.__setTreeItem__(tr,tc,l)
                #tc=tr.AppendItem(ti,'')
                #self.__setTreeItem__(tr,tc,l)
        except:
            self.__logTB__()
    def GetValue(self,lMap=None,ti=None):
        try:
            self.__logDebug__(''%())
            l=[]
            tr=self
            if ti is None:
                ti=tr.GetRootItem()
            if lMap is None:
                self.__getValueData__(tr,ti,l)
                self.__walk__(tr,ti,self.__getValueData__,l)
            else:
                lH=self.GetHier(ti)
                self.__getValue__(tr,ti,lH,lMap,l)
                self.__walkHier__(tr,ti,lH,
                                self.__getValue__,lMap,l)
            if self.__isLogDebug__():
                self.__logDebug__(self.__logFmt__(l))
            return l
        except:
            self.__logTB__()
        return None
    def Build(self,root,lData):
        try:
            self.__logDebug__(''%())
            if root is None:
                pass
            else:
                self.SetValue(root)
        except:
            self.__logTB__()
    def GetSelected(self,lMap=None):
        try:
            self.__logDebug__(''%())
            tr=self
            bMulti,lTi=self.__getSelection__(tr)
            if bMulti:
                l=[]
                if lMap is None:
                    for ti in lTi:
                        self.__getValueData__(tr,ti,l)
                else:
                    for ti in lTi:
                        self.__getValue__(tr,ti,
                                self.GetHier(ti),lMap,l)
                return l
            else:
                if lTi is None:
                    return None
                if lTi.IsOk()==False:
                    return None
                l=[]
                if lMap is None:
                    self.__getValueData__(tr,lTi,l)
                else:
                    self.__getValue__(tr,lTi,
                                self.GetHier(lTi),lMap,l)
                if len(l)>0:
                    return l[0]
                return None
            return []
        except:
            self.__logTB__()
        return None
