#----------------------------------------------------------------------------
# Name:         vtmMngClt.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20091114
# CVS-ID:       $Id: vtmMngClt.py,v 1.4 2009/11/21 15:03:18 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vtmMngCore import vtmMngCore

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

class vtmMngClt(vtmMngCore):
    def __init__(self,*args,**kwargs):
        vtmMngCore.__init__(self,*args,**kwargs)
        self.__logInfo__(''%())
        self._req=0
        self._dReq={}
        #self._identMng=None
        #self.SetPar(par)
        #self.sock=None
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        pass
    def __initCtrl__(self,*args,**kwargs):
        vtmMngCore.__initCtrl__(self,*args,**kwargs)
        o=kwargs.get('ident',None)
        if o is None:
            self.__logCritical__('no ident provided')
            raise
        self._ident=o
    def __initStateDict__old(self):
        return {
                0x00:{'name':'CLOSED','is':'IsCLosed','init':True,
                        'enter':{#'func':(self.SocketClosed,(),{}),
                                 'clr':['ACTIVE''PAUSING','PAUSED','SERVING','STARTING']}},
                0x01:{'name':'CONNECTING','is':'IsConnecting'},
                0x02:{'name':'CONNECTION','is':'IsConnectionOk'},
                0x04:{'name':'CONNECTED','is':'IsConnected',
                        #'enter':{#'func':(self.NotifyConnected,(),{}),
                        #         'set':['ACTIVE','SERVING'],
                        #         'clr':['PAUSING','PAUSED']}
                                     },
                0x10:{'name':'CONNECTION_OK','is':'IsConnectionOk',
                        #'enter':{'func':(self.NotifyConnectionOk,(),{})}
                            },
                0x11:{'name':'CONNECTION_FLT','is':'IsConnectionFlt',
                        #'enter':{'func':(self.NotifyConnectionFault,(),{})}
                            },
                0xff:{'name':'ABORTED','is':'IsAbortd','init':True,
                        'enter':{#'func':(self.SocketClosed,(),{}),
                                 'clr':['ACTIVE''PAUSING','PAUSED','SERVING','STARTING']}},
                }
    def __initFlagDict__old(self):
        return {
                0x0001:{'name':'ACTIVE','is':'IsActive','order':0},
                0x0002:{'name':'PAUSING','is':'IsPausing','order':1},
                0x0004:{'name':'PAUSED','is':'IsPaused','order':2},
                0x0008:{'name':'SERVING','is':'IsServing','order':3},
                0x0020:{'name':'STARTING','is':'IsStarting','order':4},
                0x0040:{'name':'STOPPING','is':'IsStopping','order':5},
                0x0080:{'name':'SELECT','is':'IsSelect','order':5},
                }
    def procResponse(self,d):
        try:
            iReq=d.get('req')
            if iReq in self._dReq:
                dCmd=self._dReq[iReq]
                del self._dReq[iReq]
            if 'cmd' in d:
                iCmd=d.get('cmd',None)
                if iCmd in self.dCmd:
                    func,args,kwargs=self.dCmd[iCmd]
                    res=d.get('res',None)
                    if res is None:
                        self.CallBack(func,*args,**kwargs)
                    else:
                        self.CallBack(func,res,*args,**kwargs)
            if 'stat' in d:
                self.oSF.SetState(d['stat'])
                if d['stat']==self.oSF.SHUTTINGDOWN:
                    self.thdMng.Stop()
                if d['stat']==self.oSF.SHUTDOWN:
                    self.thdMng.Stop()
        except:
            self.__logTB__()
    def procWatchQueue(self):
        bDbg=False
        if VERBOSE>10:
            if self.__isLogDebug__():
                bDbg=True
        #self.oSF.SetState(self.oSF.RUNNING)
        self.oSF.SetFlag(self.oSF.SERVING)
        while self.thdMng.Is2Stop()==False:
            self._acquire()
            zWait=self.zWait
            self._release()
            try:
                d=self.qIn.get(True,zWait)
                if bDbg:
                    self.__logDebug__('d;%s'%(self.__logFmt__(d)))
                self.procResponse(d)
            except:
                pass
        if self.oSF.IsShuttingDown:
            self.oSF.SetState(self.oSF.SHUTDOWN)
        #elif self.oSF.IsShutDown:
        #    self.__logCritical__('you are not supposed to be here')
        #    self.oSF.SetState(self.oSF.SHUTDOWN)
        #else:
        #    self.oSF.SetState(self.oSF.STOPPED)
    def Start(self):
        self.__logInfo__(''%())
        try:
            if self.IsServing():
                self.__logWarn__('already served')
                return
            if self.oSF.IsStarting:
                self.__logWarn__('already starting')
                return
            if self.oSF.IsStopping:
                self.__logWarn__('stopping active')
                return
            self.thdMng.Start()
            #self.oSF.SetFlag(self.oSF.STARTING,True)
            self.thdMng.Do(self.procWatchQueue)
        except:
            self.__logTB__()
    def Stop(self):
        self.__logInfo__(''%())
        self.thdMng.Stop()
        self.oSF.SetFlag(self.oSF.STOPPING,True)
        self._acquire()
        try:
            pass
        except:
            self.__logTB__()
        self._release()
    def Pause(self,flag):
        try:
            self.oSF.SetFlag(self.oSF.PAUSING,flag)
        except:
            self.__logTB__()
    def SetServing(self,flag):
        self.__logInfo__(''%())
        self.oSF.SetFlag(self.oSF.SERVING,flag)
    def IsServing(self):
        return self.oSF.IsServing
    def DoState(self,sName):
        if self.oSF.IsLinked==False:
            self.__logError__('not linked')
        try:
            self._acquire()
            d={'ident':self._ident,'stat':sName}
            self.qOut.put(d)
        except:
            self.__logTB__()
        self._release()
    def DoCmd(self,cmd,*args,**kwargs):
        if self.oSF.IsStopping:
            self.__logError__('stopping')
            return
        if self.thdMng.Is2Stop():
            self.__logError__('Is2Stop')
            return
        try:
            self._acquire()
            d={'ident':self._ident,'req':self._req,'cmd':cmd,'args':args,'kwargs':kwargs}
            self._dReq[self._req]=d
            self._req+=1
            if self._req>65535:
                self._req=1
            self.qOut.put(d)
        except:
            self.__logTB__()
        self._release()

class vtmMngCltLg(vtLog.vtLogMixIn,vtLog.vtLogOrigin,vtmMngClt):
    def __init__(self,*args,**kwargs):
        vtLog.vtLogOrigin.__init__(self,**{'origin':kwargs.get('origin',None)})
        vtmMngClt.__init__(self,*args,**kwargs)
