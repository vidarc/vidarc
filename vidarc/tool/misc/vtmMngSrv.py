#----------------------------------------------------------------------------
# Name:         vtmMngSrv.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20091114
# CVS-ID:       $Id: vtmMngSrv.py,v 1.8 2010/03/03 02:16:47 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import types
from Queue import Queue

from vtmMngCore import vtmMngCore

#from vidarc.tool.vtSched import vtSched
#from vtRngBuf import vtRngBuf,vtRngBufItemFunc

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

class vtmMngSrv(vtmMngCore):
    def __init__(self,*args,**kwargs):
        vtmMngCore.__init__(self,*args,**kwargs)
        self.thdMng.Do=self.__doFuncSched__
        self.Do=self.__doFuncSched__
    def __initCtrl__(self,*args,**kwargs):
        vtmMngCore.__initCtrl__(self,*args,**kwargs)
        self.lCon=[]
        self.dCon={}
        self.iLenCon=0
        self._ident=0
        self._busy=False
        #self.shdCmd=vtSched(kwargs.get('scheduleSize',32))
        #self.shdCmd=vtRngBuf(kwargs.get('scheduleSize',32),vtRngBufItemFunc)
        self.shdCmd=Queue(kwargs.get('scheduleSize',32))
    def IsBusy(self):
        if self.thdProc.IsPaused():
            return False
        try:
            self._acquire()
            return self._busy
        finally:
            self._release()
    def __start__(self):
        self.thdProc.Start()
        self.thdProc.Pause()
        self.thdProc.Do(self.procWatchSched)
        vtmMngCore.__start__(self)
        #self.__doFuncClasses__('__start__')
    def SchedPut(self,func,*args,**kwargs):
        self.shdComm.put(func,*args,**kwargs)
    #def SchedPop(self):
    #    self.shdComm.pop()
    def procCmd(self,d,bDbg=False):
        try:
            ident=d.get('ident',None)
            cmd=d.get('cmd',None)
            if cmd in self.dCmd:
                args=d.get('args',())
                kwargs=d.get('kwargs',{})
                f,reply=self.dCmd[cmd]
                if reply>=10:
                    res=self.CallBackWX(f,*args,**kwargs)
                    reply-=10
                else:
                    res=f(*args,**kwargs)
                if bDbg:
                    self.__logDebug__('reply:%d;d;%s;res:%s'%(reply,
                            self.__logFmt__(d),self.__logFmt__(res)))
                if res is not None:
                    if reply==0:
                        if ident in self.dCon:
                            self.dCon[ident].put({'res':res,'req':d.get('req',-1),'cmd':cmd})
                    else:
                        if ident is not None:
                            for k,q in self.dCon.iteritems():
                                if k!=ident:
                                    q.put({'res':res,'cmd':cmd})
                            if reply==2:
                                if ident in self.dCon:
                                    self.dCon[ident].put({'res':res,'req':d.get('req',-1),'cmd':cmd})
                        else:
                            for k,q in self.dCon.iteritems():
                                q.put({'res':res,'cmd':cmd})
        except:
            self.__logTB__()
    def __doFuncSched__(self,func,*args,**kwargs):
        try:
            self._acquire()
            if VERBOSE>0 and self.__isLogDebug__():
                self.__logDebug__('%s;scheduled;args:%s;kwargs:%s'%(
                        func.__name__,self.__logFmtLm__(args),
                        self.__logFmtLm__(kwargs)))
            self.qIn.put((func,args,kwargs))
        except:
            self.__logTB__()
        self._release()
    def procWatchSched(self):
        self.__logInfo__('started')
        bDbg=False
        if VERBOSE>10:
            if self.__isLogDebug__():
                bDbg=True
        while self.thdProc.Is2Stop()==False:
            try:
                self._acquire()
                self._busy=not self.shdCmd.empty()
                self._release()
                func,args,kwargs=self.shdCmd.get()
                if bDbg:
                    self.__logDebug__('func:%s;args:%s;kwargs:%s;thdProc:%s'%(func,
                            self.__logFmt__(args),self.__logFmt__(kwargs),
                            self.thdProc))
                if self.thdProc.__doPause__()==True:
                    self.__logInfo__('exit due pause;thdProc:%s'%(str(self.thdProc)))
                    return
                if func is not None:
                    self._acquire()
                    self._busy=True
                    self._release()
                    func(*args,**kwargs)
            except:
                self.__logTB__()
        self.__logInfo__('done')
    def procWatchQueue(self):
        bDbg=False
        if VERBOSE>10:
            if self.__isLogDebug__():
                bDbg=True
        #self.oSF.SetState(self.oSF.RUNNING)
        self.oSF.SetFlag(self.oSF.SERVING)
        while self.thdMng.Is2Stop()==False:
            try:
                self._acquire()
                zWait=self.zWait
                self._release()
                lDel=[]
                try:
                    d=self.qIn.get(True,zWait)
                    try:
                        if bDbg:
                            self.__logDebug__('d;%s'%(self.__logFmt__(d)))
                        if type(d)==types.TupleType:
                            #func,args,kwargs=d
                            #self.shdCmd.put(func,*args,**kwargs)
                            self.shdCmd.put(d)
                        elif 'cmd' in d:
                            #self.shdCmd.put(self.procCmd,d,bDbg)#d['ident'],d['cmd'],*d['args'],**d['kwargs'])
                            self.shdCmd.put((self.procCmd,(d,),{'bDbg':bDbg}))
                        elif 'get' in d:
                            pass
                        elif 'set' in d:
                            pass
                        elif 'stat' in d:
                            iState=d.get('stat',self.oSF.SHUTDOWN)
                            ident=d.get('ident',None)
                            if ident is not None:
                                if bDbg:
                                    self.__logDebug__('ident:%r;lCon;%s;dCon;%s'%(ident,
                                        self.__logFmt__(self.lCon),self.__logFmt__(self.dCon)))
                                if iState==self.oSF.SHUTTINGDOWN:
                                    iOfs=self.lCon.index(ident)
                                    dRes={'stat':self.oSF.SHUTDOWN,'ident':ident}
                                    self.dCon[ident].put(dRes)
                                    lDel.append(iOfs)
                                elif iState==self.oSF.SHUTDOWN:
                                    if ident in self.dCon:
                                        iOfs=self.lCon.index(ident)
                                        lDel.append(iOfs)
                                else:
                                    if type(iState)==types.StringType:
                                        if iState=='SHUTDOWN':
                                            self.ShutDown()
                                        else:
                                            self.oSF.SetState(getattr(self.oSF,iState))
                            elif ident is None:
                                if type(iState)==types.StringType:
                                    if iState=='SHUTDOWN':
                                        self.ShutDown()
                                    else:
                                        self.oSF.SetState(getattr(self.oSF,iState))
                                
                    except:
                        self.__logTB__()
                    #for ident in 
                except:
                    pass
                #self.shdCmd.pop()
                self._acquire()
                lDel.reverse()
                for iOfs in lDel:
                    del self.dCon[self.lCon[iOfs]]
                    del self.lCon[iOfs]
                    self.iLenCon=len(self.lCon)
                    keys=self.dCon.keys()
                    keys.sort()
                    self.__logInfo__('ident;%s'%(self.__logFmt__(keys)))
                if self.oSF.IsShuttingDown:
                    if self.iLenCon==0:
                        self.thdMng.Stop()
                if self.oSF.IsHold:
                    if self.thdProc is not None:
                        if self.thdProc.IsPaused():
                            self.DoState('HELD')
            except:
                self.__logTB__()
            self._release()
            #self.Sleep(zWait)
        if self.oSF.IsShuttingDown:
            self.oSF.SetState(self.oSF.SHUTDOWN)
        elif self.oSF.IsShutDown:
            self.__logCritical__('you are not supposed to be here')
            self.oSF.SetState(self.oSF.SHUTDOWN)
        #else:
        #    self.oSF.SetState(self.oSF.STOPPED)
        self.thdMng.Stop()
    def AddMngClt(self,clsMngClt,*args,**kwargs):
        oMng=None
        try:
            if self.oSF.IsLinked==False:
                return None
            self._acquire()
            oMng=clsMngClt(ident=self._ident,qOut=self.GetQueueIn(),
                    *args,**kwargs)
            self.lCon.append(self._ident)#oMng.GetQueueOut())
            self.iLenCon=len(self.lCon)
            self.dCon[self._ident]=oMng.GetQueueIn()
            keys=self.dCon.keys()
            keys.sort()
            self.__logInfo__('ident;%s'%(self.__logFmt__(keys)))
            self._ident+=1
        except:
            self.__logTB__()
        self._release()
        return oMng
    def __postState__(self,iState):
        try:
            self._acquire()
            d={'stat':iState}
            for qOut in self.dCon.itervalues():
                qOut.put(d)
        except:
            self.__logTB__()
        self._release()
    def __procState__(self,iState,iStatePrev):
        """ add manager behaviour here
        """
        try:
            sN=self.oSF.GetStateName(iState)
            if VERBOSE>10:
                if self.__isLogDebug__():
                    self.__logDebug__('sN:%s(%d->%d);;state;%s'%(sN,iStatePrev,iState,
                            self.oSF.GetStrFull(';')))
            self._acquire()
            self.DoFunc(''.join(['__',sN,'__']))
            self.DoFunc(sN)
            self.DoFunc('__notifyState__')
            if self.oSF.HOLD==iState:
                self.thdProc.Pause()
                self.shdCmd.put((None,None,None))
            elif self.oSF.START==iState:
                self.thdProc.Resume()
            #elif self.oSF.SHUTTINGDOWN==iState:
            #    self.thdProc.Stop()
        except:
            self.__logTB__()
        self._release()
    def DoState(self,sName,bSched=True):
        if self.__isLogDebug__():
            self.__logDebug__('state to change:%s;state;%s'%(sName,
                    self.oSF.GetStrFull(';')))
        if bSched:
            self.qIn.put({'stat':sName,'ident':None})
        else:
            self.oSF.SetState(getattr(self.oSF,sName))
    def ShutDown(self):
        self.__logInfo__('state;%s'%(self.oSF.GetStrFull(';')))
        if self.oSF.IsShutDown:
            return
        if self.thdMng.IsRunning():
            self.oSF.SetState(self.oSF.SHUTTINGDOWN)
            self.__postState__(self.oSF.SHUTTINGDOWN)
            if self.thdProc is not None:
                self.shdCmd.put((None,None,None))
                self.thdProc.Stop()
        else:
            if self.thdProc is not None:
                if self.thdProc.IsRunning():
                    self.oSF.SetState(self.oSF.SHUTTINGDOWN)
                    self.__postState__(self.oSF.SHUTTINGDOWN)
                    self.shdCmd.put((None,None,None))
                    self.thdProc.Stop()
                else:
                    self.oSF.SetState(self.oSF.SHUTDOWN)
                    self.__postState__(self.oSF.SHUTDOWN)
            else:
                self.oSF.SetState(self.oSF.SHUTDOWN)
                self.__postState__(self.oSF.SHUTDOWN)
        #self.thdMng.Stop()

class vtmMngSrvLg(vtLog.vtLogMixIn,vtLog.vtLogOrigin,vtmMngSrv):
    def __init__(self,*args,**kwargs):
        vtLog.vtLogOrigin.__init__(self,**{'origin':kwargs.get('origin',None)})
        vtmMngSrv.__init__(self,*args,**kwargs)
