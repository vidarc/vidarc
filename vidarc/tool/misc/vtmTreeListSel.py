#----------------------------------------------------------------------------
# Name:         vtmTreeListSel.py
# Purpose:      tree list control with stretch calculation and select
# Author:       Walter Obweger
#
# Created:      20090426
# CVS-ID:       $Id: vtmTreeListSel.py,v 1.11 2010/07/02 07:31:35 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import vidarc.tool.misc.vtmTreeListCtrl
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
from vidarc.tool.gui.vtgPanel import vtgPanel

import types

# defined event for vtmTreeListSel item selected
wxEVT_VTMTREELISTSEL_ITEM_SELECTED=wx.NewEventType()
vEVT_VTMTREELISTSEL_ITEM_SELECTED=wx.PyEventBinder(wxEVT_VTMTREELISTSEL_ITEM_SELECTED,1)
def EVT_VTMTREELISTSEL_ITEM_SELECTED(win,func):
    win.Connect(-1,-1,wxEVT_VTMTREELISTSEL_ITEM_SELECTED,func)
def EVT_VTMTREELISTSEL_ITEM_SELECTED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,wxEVT_VXMTREELISTSEL_ITEM_SELECTED,func)
class vtmTreeListSelItemSelected(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VTMTREELISTSEL_ITEM_SELECTED(<widget_name>, self.OnItemSel)
    """

    def __init__(self,obj,tn,data):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTMTREELISTSEL_ITEM_SELECTED)
        self.obj=obj
        self.treeNode=tn
        self.treeNodeData=data
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,obj.GetOrigin())
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def GetTree(self):
        return self.obj
    def GetTreeItem(self):
        return self.treeNode
    def GetTreeItemData(self):
        try:
            return self.treeNodeData
        except:
            return None

[wxID_VTMTREELISTSEL, wxID_VTMTREELISTSELCBADD, wxID_VTMTREELISTSELCBDEL, 
 wxID_VTMTREELISTSELTRLSTDATA, 
] = [wx.NewId() for _init_ctrls in range(4)]

class vtmTreeListSel(vtgPanel):
    MAP=None
    COL_SEL=1
    COL_SEL_MARKER_SET='X'
    COL_SEL_MARKER_CLR=''
    IMG_EMPTY=0
    FORCE_CHILDREN=False#True
    def __initCtrl__(self,*args,**kwargs):
        style=kwargs.get('style',0)
        lWid=kwargs.get('lWid',[])
        lWidBt=kwargs.get('lWidBt',[])
        lWid.append((vidarc.tool.misc.vtmTreeListCtrl.vtmTreeListCtrl,(0,0),(1,1),{
                    'name':'trlstData','style':style,
                    'bButtons':kwargs.get('bButtons',False),
                    'bMultiple':kwargs.get('bMultiple',False),
                    },
                    {wx.EVT_TREE_SEL_CHANGED:self.OnVtmTreeListDataTreeSelChanged}))
        lWid.append(('szBoxVert',(0,1),(1,1),
                    {'name':'bxsBt',},[
                    ('cbBmp',0,4,
                    {'name':'cbAdd','bitmap':vtArt.getBitmap(vtArt.AttrSel),},
                    {'btn':self.OnCbAddButton}),
                    ('cbBmp',0,4,
                    {'name':'cbDel','bitmap':vtArt.getBitmap(vtArt.AttrUnSel),},
                    {'btn':self.OnCbDelButton})]+lWidBt))
        vtgPanel.__initCtrl__(self,lWid=lWid)
        if 'name' in kwargs:
            self.trlstData.SetName(kwargs['name'])
            self.SetName(kwargs['name']+'Panel')
        self.__setSel__=self.__setMarkerText__
        self.__setClr__=self.__setMarkerText__
        self.__getSel__=self.__getMarkerText__
        self.__logTB__=self.trlstData.__logTB__
        self.__logDebug__=self.trlstData.__logDebug__
        self.__logFmt__=self.trlstData.__logFmt__
        self.__isLogDebug__=self.trlstData.__isLogDebug__
        self.__getTreeItemByHier__=self.trlstData.__getTreeItemByHier__
        #self.__setTreeItem__=self.trlstData.__setTreeItem__
        #self.__setValue__=self.trlstData.__setValue__
        self.__getValue__=self.trlstData.__getValue__
        self.__getValueData__=self.trlstData.__getValueData__
        self.__walkFind__=self.trlstData.__walkFind__
        self.__walk__=self.trlstData.__walk__
        self.__walkHier__=self.trlstData.__walkHier__
        
        idTr=self.trlstData.GetId()
        wx.EVT_TREE_ITEM_RIGHT_CLICK(self, idTr,
                self.OnTreeTreeLstDataRight)
        return [(0,1)],[(0,1)]
    def GetMainWidget(self):
        return self.trlstData
    def SetImageList(self,imgLst,iImgEmptyIdx=0):
        self.trlstData.SetImageList(imgLst)
        self.IMG_EMPTY=iImgEmptyIdx
    def SetMap(self,l):
        self.MAP=l
        self.trlstData.SetMap(l)
    def GetId(self):
        return self.trlstData.GetId()
    def Bind(self,*args,**kwargs):
        self.trlstData.Bind(*args,**kwargs)
    def Unbind(self,*args,**kwargs):
        self.trlstData.Unbind(*args,**kwargs)
    def BindEvent(self,name,func,par=None):
        vtgPanel.BindEvent(self,name,func,par=par)
        self.trlstData.BindEvent(name,func,par=par)
    def UnBindEvent(self,name,func,par=None):
        vtgPanel.UnBindEvent(self,name,func,par=par)
        self.trlstData.UnBindEvent(name,func,par=par)
    def SetSelColumn(self,i,marker):
        self.COL_SEL=i
        self.COL_SEL_MARKER_SET,self.COL_SEL_MARKER_CLR=marker
        if type(self.COL_SEL_MARKER_SET)==types.IntType:
            self.__setSel__=self.__setMarkerImg__
            self.__getSel__=self.__getMarkerImg__
        else:
            self.__setSel__=self.__setMarkerText__
            self.__getSel__=self.__getMarkerText__
        if type(self.COL_SEL_MARKER_CLR)==types.IntType:
            self.__setClr__=self.__setMarkerImg__
            #self.__getSel__=self.__getMarkerText__
        else:
            self.__setClr__=self.__setMarkerText__
            #self.__getClr__=self.__getMarkerText__
    def __del__(self):
        wx.Panel.__del__(self)
    def GetHier(self,ti):
        l=[]
        self.trlstData.__getHier__(self.trlstData,ti,l)
        l.reverse()
        return l
    def __setMarkerText__(self,tr,ti,v):
        tr.SetItemImage(ti,-1,self.COL_SEL,which=wx.TreeItemIcon_Normal)
        if v:
            tr.SetItemText(ti,self.COL_SEL_MARKER_SET,self.COL_SEL)
        else:
            tr.SetItemText(ti,self.COL_SEL_MARKER_CLR,self.COL_SEL)
    def __setMarkerImg__(self,tr,ti,v):
        tr.SetItemText(ti,'',self.COL_SEL)
        if v:
            tr.SetItemImage(ti,self.COL_SEL_MARKER_SET,self.COL_SEL,which=wx.TreeItemIcon_Normal)
        else:
            tr.SetItemImage(ti,self.COL_SEL_MARKER_CLR,self.COL_SEL,which=wx.TreeItemIcon_Normal)
    def __getMarkerText__(self,tr,ti):
        return tr.GetItemText(ti,self.COL_SEL)==self.COL_SEL_MARKER_SET
    def __getMarkerImg__(self,tr,ti):
        return tr.GetItemImage(ti,self.COL_SEL,which=wx.TreeItemIcon_Normal)==self.COL_SEL_MARKER_SET
    def __sel__(self,tr,ti,v):
        if v:
            self.__setSel__(tr,ti,v)
        else:
            self.__setClr__(tr,ti,v)
    def __getTreeItem__(self,trlst,ti,lHier):
        if ti is None:
            ti=trlst.GetRootItem()
        tup=trlst.GetFirstChild(ti)
        #tup=self.trlstRes.GetLastChild(ti)
        tt=tup[0]
        tc=None
        s=lHier[0]
        while tt.IsOk():
            if s==trlst.GetItemText(tt,0):
                tc=tt
                break
            tup=trlst.GetNextChild(ti,tup[1])
            tt=tup[0]
        if tc is not None:
            if len(lHier)>1:
                return self.__getTreeItem__(trlst,tc,lHier[1:])
        return None
    def __setTreeItemDict__(self,tr,ti,d):
        #print tr.GetItemText(ti,0)
        #print d
        for s,i in self.MAP:
            if i==self.COL_SEL:
                pass
            if s in d:
                v=d[s]
                t=type(v)
                if t==types.TupleType:
                    tr.SetItemText(ti,v[0],i)
                    tr.SetItemImage(ti,v[1],i,which=wx.TreeItemIcon_Normal)
                elif t==types.IntType:
                    tr.SetItemImage(ti,v,i,which=wx.TreeItemIcon_Normal)
                else:
                    tr.SetItemText(ti,v,i)
                    tr.SetItemImage(ti,self.IMG_EMPTY,i,which=wx.TreeItemIcon_Normal)
        if -1 in d:
            tr.SetPyData(ti,d[-1])
        if -2 in d:
            self.__sel__(tr,ti,d[-2])
        else:
            self.__sel__(tr,ti,False)
        if -3 in d:
            self.__setValue__(tr,ti,d[-3])
    def __setTreeItemLst__(self,tr,ti,l):
        if l[0] is not None:
            tr.SetPyData(ti,l[0])
        i=0
        for v in l[1]:
            t=type(v)
            if t==types.TupleType:
                tr.SetItemText(ti,v[0],i)
                tr.SetItemImage(ti,v[1],i,which=wx.TreeItemIcon_Normal)
            elif t==types.IntType:
                tr.SetItemImage(ti,v,i,which=wx.TreeItemIcon_Normal)
            else:
                tr.SetItemText(ti,v,i)
                tr.SetItemImage(ti,self.IMG_EMPTY,i,which=wx.TreeItemIcon_Normal)
            i+=1
        iLen=len(l)
        if iLen>2:
            self.__sel__(tr,ti,l[2])
        else:
            self.__sel__(tr,ti,False)
        if iLen>3:
            self.__setValue__(tr,ti,l[3])
    def __setTreeItem__(self,tr,ti,val):
        if type(val)==types.DictType:
            self.__setTreeItemDict__(tr,ti,val)
        elif type(val)==types.ListType:
            self.__setTreeItemLst__(tr,ti,val)
    def __setValue__(self,tr,tp,l,bRoot=False):
        for ll in l:
            if tp is None:
                ti=tr.AddRoot('')
            else:
                ti=tr.AppendItem(tp,'')
            if self.FORCE_CHILDREN:
                tr.SetItemHasChildren(ti,True)
            self.__setTreeItem__(tr,ti,ll)
            if tp is None:
                return
    def SetValue(self,l,lHier=None,bChildren=False):
        try:
            self.__logDebug__(''%())
            tr=self.trlstData
            iCols=tr.GetColumnCount()
            
            if lHier is None:
                tr.DeleteAllItems()
                ti=tr.AddRoot('')
                self.__setTreeItem__(tr,ti,l)
                tr.Expand(ti)
            else:
                if type(lHier)==types.ListType:
                    ti=self.__getTreeItemByHier__(tr,tr.GetRootItem(),lHier)
                    if ti is None:
                        self.__logError__('%s;ti not found'%('.'.join(lHier)))
                        return
                else:
                    ti=lHier
                if bChildren==True:
                    self.__setValue__(tr,ti,l)
                else:
                    tc=tr.AppendItem(ti,'')
                    self.__setTreeItem__(tr,tc,l)
        except:
            self.__logTB__()
    def __isSel__(self,tr,ti):
        return self.__getSel__(tr,ti)
    def __getValueSel__(self,tr,ti,lHier,lMap,l):
        if self.__getSel__(tr,ti):
            self.__getValue__(tr,ti,lHier,lMap,l)
    def __getValueDataSel__(self,tr,ti,l):
        if self.__getSel__(tr,ti):
            self.__getValueData__(tr,ti,l)
    def IsSel(self,ti):
        return self.__isSel__(self.trlstData,ti)
    def GetValue(self,lMap=None,ti=None):
        try:
            self.__logDebug__(''%())
            l=[]
            tr=self.trlstData
            if ti is None:
                ti=tr.GetRootItem()
                if lMap is None:
                    #self.__getValueData__(tr,ti,l)
                    self.__walk__(tr,ti,self.__getValueDataSel__,l)
                else:
                    self.__walkHier__(tr,ti,[tr.GetItemText(ti,0)],
                                self.__getValueSel__,lMap,l)
            else:
                if lMap is None:
                    self.__getValueData__(tr,ti,l)
                else:
                    self.__getValue__(tr,ti,self.GetHier(ti),lMap,l)
            if self.__isLogDebug__():
                self.__logDebug__(self.__logFmt__(l))
            return l
        except:
            self.__logTB__()
        return None
    def __getattr__(self,name):
        if hasattr(self.trlstData,name):
            return getattr(self.trlstData,name)
        raise AttributeError(name)
    def OnVtmTreeListDataTreeSelChanged(self, event):
        event.Skip()
    def GetSelected(self,lMap=None):
        try:
            self.__logDebug__(''%())
            return self.trlstData.GetSelected(lMap=lMap)
        except:
            self.__logTB__()
        return None
    def __setTreeItemSel__(self,tr,ti,bFlag):
        self.__setSel__(tr,ti,bFlag)
        self.__walk__(tr,ti,self.__setTreeItemSel__,bFlag)
    def OnCbAddButton(self, event):
        event.Skip()
        try:
            self.__logDebug__(''%())
            tr=self.trlstData
            bMulti,lTi=self.__getSelection__(tr)
            if bMulti:
                for ti in lTi:
                    self.__setTreeItemSel__(tr,ti,True)
                    self.Post('sel',ti)
            else:
                self.__setTreeItemSel__(tr,lTi,True)
                self.Post('sel',lTi)
        except:
            self.__logTB__()
    def OnCbDelButton(self, event):
        event.Skip()
        try:
            self.__logDebug__(''%())
            tr=self.trlstData
            bMulti,lTi=self.__getSelection__(tr)
            if bMulti:
                for ti in lTi:
                    self.__setTreeItemSel__(tr,ti,False)
                    self.Post('unsel',ti)
            else:
                self.__setTreeItemSel__(tr,lTi,False)
                self.Post('unsel',lTi)
        except:
            self.__logTB__()
    def OnTreeTreeLstDataRight(self,evt):
        evt.Skip()
        try:
            self.__logDebug__(''%())
            ti=evt.GetItem()
            tr=self.trlstData
            v=not self.__getSel__(tr,ti)
            self.__setTreeItemSel__(tr,ti,v)
            if v:
                self.Post('sel',ti)
            else:
                self.Post('unsel',ti)
        except:
            self.__logTB__()
    def OnTcNodeTreeSelChanged(self,evt):
        evt.Skip()
        try:
            self.__logDebug__(''%())
            ti=evt.GetItem()
            data=self.trlstData.GetPyData(ti)
            wx.PostEvent(self,vtmTreeListSelItemSelected(self.trlstData,ti,data))
        except:
            self.__logTB__()
