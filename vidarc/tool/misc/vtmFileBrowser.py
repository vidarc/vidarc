#----------------------------------------------------------------------
# Name:         vtmFileBrowser.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20100214
# CVS-ID:       $Id: vtmFileBrowser.py,v 1.2 2010/02/28 00:41:22 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import os

from vidarc.tool.gui.vtgPanel import vtgPanel
import vidarc.tool.art.vtArt as vtArt

class vtmFileBrowser(vtgPanel):
    def __initCtrl__(self,*args,**kwargs):
        lWid=[]
        self.bSave=kwargs.get('bSave',False)
        self.bMulti=kwargs.get('bMulti',False)
        self.bPosixFN=kwargs.get('bPosixFN',True)
        self.sMsg=kwargs.get('sMsg',_(u'choose file'))
        self.sWildCard=kwargs.get('sWildCard',_(u'xml files|*.xml|all files|*.*'))
        self.sDftFN=kwargs.get('sDftFN',u'tmpl.xml')
        self.sFN=kwargs.get('sFN',self.sDftFN)
        
        lWid.append(('txt',(0,0),(1,1),{'name':'txtFN','value':self.sFN,},
                {'txt':self.OnTxtFileNameText,'ent':self.OnTxtFileNameEnter}))
        lWid.append(('cbBmp',(0,1),(1,1),
                    {'name':'cbBrowse','bitmap':vtArt.getBitmap(vtArt.Browse),},
                    {'btn':self.OnCbBrowseClick}))
        vtgPanel.__initCtrl__(self,lWid=lWid)
        return None,[(0,1)]
    def GetValue(self):
        return self.sFN
    def GetDN(self):
        try:
            sTmpDN,sTmpFN=os.path.split(self.sFN)
            return sTmpDN
        except:
            return None
    def GetFN(self):
        try:
            sTmpDN,sTmpFN=os.path.split(self.sFN)
            return sTmpFN
        except:
            return None
    def SetValue(self,sFN):
        try:
            if self.bPosixFN==True:
                sFN=sFN.replace('\\','/')
            else:
                sFN=sFN.replace('/','\\')
            self.sFN=sFN
            vtgPanel.SetValue(self,'txtFN',self.sFN)
        except:
            self.__logTB__()
    def OnTxtFileNameText(self,evt):
        try:
            self.sFN=self.txtFN.GetValue()
        except:
            self.__logTB__()
    def OnTxtFileNameEnter(self,evt):
        try:
            self.sFN=self.txtFN.GetValue()
            self.Post('file',self.sFN)
        except:
            self.__logTB__()
    def OnCbBrowseClick(self,evt):
        try:
            self.__logDebug__('sFN:%r'%(self.sFN))
            if self.ChooseFile() is not None:
                self.__logDebug__('sFN:%r'%(self.sFN))
                self.txtFN.SetValue(self.sFN)
        except:
            self.__logTB__()
    def ChooseFile(self):
        try:
            self.__logInfo__(''%())
            if self.bSave==True:
                iStyle=wx.SAVE
            else:
                iStyle=wx.OPEN
            if self.bMulti==True:
                iStyle|=wx.MULTIPLE
            dlg=wx.FileDialog(self,message=self.sMsg,
                defaultDir='.',defaultFile=self.sDftFN,
                wildcard=self.sWildCard,
                style=iStyle)
            dlg.Centre()
            try:
                if len(self.sFN)>0:
                    sTmpDN,sTmpFN=os.path.split(self.sFN)
                    dlg.SetDirectory(sTmpDN)
                    dlg.SetFilename(sTmpFN)
            except:
                self.__logTB__()
            iRet=dlg.ShowModal()
            if iRet==wx.ID_OK:
                sTmpDN=dlg.GetDirectory()
                if self.bPosixFN==True:
                    sTmpDN=sTmpDN.replace('\\','/')
                if self.bMulti==True:
                    lTmpFN=dlg.GetFilenames()
                    if self.bPosixFN==True:
                        lFN=['/'.join([sTmpDN,sTmpFN]) for sTmpFN in lTmpFN]
                    else:
                        lFN=[os.path.join(sTmpDN,sTmpFN) for sTmpFN in lTmpFN]
                    lFN.sort()
                    dlg.Destroy()
                    self.sFN=lFN[0]
                    self.Post('files',lFN)
                    return lFN
                else:
                    sTmpFN=dlg.GetFilename()
                    if self.bPosixFN==True:
                        sFN='/'.join([sTmpDN,sTmpFN])
                    else:
                        sFN=os.path.join(sTmpDN,sTmpFN)
                    dlg.Destroy()
                    self.sFN=sFN
                    self.Post('file',self.sFN)
                    return sFN
            dlg.Destroy()
        except:
            self.__logTB__()
        return None
