#----------------------------------------------------------------------------
# Name:         vtmThread.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20091010
# CVS-ID:       $Id: vtmThread.py,v 1.5 2010/02/26 00:42:20 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import time

class vtmThreadInterFace:
    def __init__(self,thd=None):
        self.__interFace__(thd=thd)
    def __interFace__(self,thd=None):
        def get_set(self,thd,f,l):
            for key in l:
                if thd is not None:
                    setattr(self,key,getattr(thd,key,f))
                else:
                    setattr(self,key,f)
        if hasattr(self,'Sleep')==0:
            if thd is not None and hasattr(thd,'Sleep'):
                self.Sleep=getattr(thd,'Sleep')
            else:
                self.Sleep=self.__fbSleep__
        #get_set(self,thd,self.__fbFalse__,['Is2Stop',])
        get_set(self,thd,self.__fbTrue__,['Is2Stop',])
        get_set(self,thd,self.__fbPost__,['Post',])
        
        get_set(self,thd,self.__fbCall__,['Do','CallBack','CallBackWX',
                    'DoCallBack','DoCallBackWX'])
        get_set(self,thd,self.__fbCallBranch__,['Branch','DoBranch',
                    'CallBackBranch','CallBackBranchWX',
                    'DoCallBackBranch','DoCallBackBranchWX',])
        get_set(self,thd,self.__fbCallDly__,['CallBackDelayed',])
        get_set(self,thd,self.__fbDummy__,['BindEvents',])
        if 0:
            if thd is not None and hasattr(thd,'Is2Stop'):
                self.Is2Stop=getattr(thd,'Is2Stop')
            else:
                self.Is2Stop=self.__fbFalse__
            if thd is not None and hasattr(thd,'Post'):
                self.Post=getattr(thd,'Post')
            else:
                self.Post=self.__fbPost__
            if thd is not None and hasattr(thd,'Do'):
                self.Do=getattr(thd,'Do')
            else:
                self.Do=self.__fbCall__
            if thd is not None and hasattr(thd,'CallBack'):
                self.CallBack=getattr(thd,'CallBack')
            else:
                self.CallBack=self.__fbCall__
            if thd is not None and hasattr(thd,'CallBackBranch'):
                self.CallBackBranch=getattr(thd,'CallBackBranch')
            else:
                self.CallBackBranch=self.__fbCallBranch__
            if thd is not None and hasattr(thd,'CallBackWX'):
                self.CallBackWX=getattr(thd,'CallBackWX')
            else:
                self.CallBackWX=self.__fbCall__
            if thd is not None and hasattr(thd,'CallBackBranchWX'):
                self.CallBackBranchWX=getattr(thd,'CallBackBranchWX')
            else:
                self.CallBackBranchWX=self.__fbCallBranch__
            if thd is not None and hasattr(thd,'DoCallBack'):
                self.DoCallBack=getattr(thd,'DoCallBack')
            else:
                self.DoCallBack=self.__fbCall__
            if thd is not None and hasattr(thd,'DoCallBackWX'):
                self.DoCallBackWX=getattr(thd,'DoCallBackWX')
            else:
                self.DoCallBackWX=self.__fbCall__
            
            if thd is not None and hasattr(thd,'CallBackDelayed'):
                self.CallBackDelayed=getattr(thd,'CallBackDelayed')
            else:
                self.CallBackDelayed=self.__fbCallDly__
            
            if thd is not None and hasattr(thd,'BindEvents'):
                self.BindEvents=getattr(thd,'BindEvents')
            else:
                self.BindEvents=self.__fbDummy__
    def __fbCall__(self,func,*args,**kwargs):
        return func(*args,**kwargs)
    def __fbCallBranch__(self,funcCB,branchOkCB,branchFltCB):
        func,args,kwargs=funcCB
        ret=func(*args,**kwargs)
        if ret>=0:
            func,args,kwargs=branchOkCB
            func(*args,**kwargs)
        else:
            if branchFltCB is None:
                self.__fbCallBranch__(funcCB,branchOkCB,branchFltCB)
            else:
                func,args,kwargs=branchFltCB
                func(*args,**kwargs)
    def __fbCallDly__(self,zSleep,func,*args,**kwargs):
        time.sleep(zSleep)
        func(*args,**kwargs)
    def __fbSleep__(self,zSleep):
        time.sleep(zSleep)
    def __fbTrue__(self):
        return True
    def __fbFalse__(self):
        return False
    def __fbPost__(self,sEvent,*args,**kwargs):
        return False
    def __fbDummy__(self,*args,**kwargs):
        pass
