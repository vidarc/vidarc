#----------------------------------------------------------------------------
# Name:         vtmUsrPwd.py
# Purpose:      password input
# Author:       Walter Obweger
#
# Created:      20100225
# CVS-ID:       $Id: vtmUsrPwd.py,v 1.2 2010/02/26 00:51:06 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

from vidarc.tool.gui.vtgPanel import vtgPanel
from vidarc.tool.gui.vtgDlg import vtgDlg

class vtmUsrPwdPanel(vtgPanel):
    def __init__(self,parent,pos=None,size=None,name='pnPwd',
                bEnMark=False,bEnMod=False):
        vtgPanel.__init__(self,parent,pos=pos,size=size,name=name,style=0,
                bEnMark=bEnMark,bEnMod=bEnMod,lWid=[
                ('lblRg', (0,0),(1,1),{'name':'lblUsr','label':_(u'user')},None),
                ('txt',   (0,1),(1,3),{'name':'txtUsr','value':''},None),
                ('lblRg', (1,0),(1,1),{'name':'lblPwd','label':_(u'password')},None),
                ('txtPwd',(1,1),(1,3),{'name':'txtPwd','value':''},None),
                ])

class vtmUsrPwdDlg(vtgDlg):
    def __init__(self,parent,title='vtmPwdDlg',pos=None,sz=None,
                name='dlgusrPwd',iArrange=-1,widArrange=None,
                kind='dialog',bmp=None):
        vtgDlg.__init__(self,parent,vtgPanel,pos=pos,sz=sz,name=name,
                title=title,bmp=bmp,
                iArrange=iArrange,widArrange=widArrange,kind=kind,
                pnName='pnUsrPwd',lGrowCol=[(1,1)],
                bEnMark=False,bEnMod=False,lWid=[
                ('lblRg', (0,0),(1,1),{'name':'lblUsr','label':_(u'user')},None),
                ('txtEnt',   (0,1),(1,3),{'name':'txtUsr','value':''},{
                    'ent':self.OnUsrEnter}),
                ('lblRg', (1,0),(1,1),{'name':'lblPwd','label':_(u'password')},None),
                ('txtPwdEnt',(1,1),(1,3),{'name':'txtPwd','value':''},{
                    'ent':self.OnPwdEnter,
                    'char':self.OnPwdKeyUp,
                    }),
                ])
    def Show(self,*args,**kwargs):
        self.pn.txtUsr.SetValue('')
        self.pn.txtPwd.SetValue('')
        vtgDlg.Show(self,*args,**kwargs)
        self.pn.txtUsr.SetFocus()
    def ShowModal(self,*args,**kwargs):
        self.pn.txtUsr.SetValue('')
        self.pn.txtPwd.SetValue('')
        self.pn.txtUsr.SetFocus()
        return vtgDlg.ShowModal(self,*args,**kwargs)
    def GetValue(self):
        return self.pn.GetValue('txtUsr'),self.pn.GetValue('txtPwd')
    def SetValue(self,usr,pwd):
        self.pn.SetValue('txtUsr',usr)
        self.pn.SetValue('txtPwd',pwd)
    def OnUsrEnter(self,evt):
        evt.Skip()
        try:
            self.pn.txtPwd.SetFocus()
        except:
            self.__logTB__()
    def OnPwdEnter(self,evt):
        evt.Skip()
        try:
            self.__logDebug__(''%())
            self.OnCbApplyButton(None)
        except:
            self.__logTB__()
    def OnPwdKeyUp(self,evt):
        evt.Skip()
        try:
            self.__logDebug__(''%())
            #self.OnCbApplyButton(None)
        except:
            self.__logTB__()
        self.HandleKey(evt)
