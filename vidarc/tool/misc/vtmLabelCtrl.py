#----------------------------------------------------------------------------
# Name:         vtmLabel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20090820
# CVS-ID:       $Id: vtmLabelCtrl.py,v 1.3 2010/04/02 18:49:41 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

class vtmLabelCtrl(wx.TextCtrl):
    def __init__(self,*_args,**_kwargs):
        kwargs=_kwargs.copy()
        kwargs['style']=kwargs.get('style',0) | wx.TE_READONLY
        apply(wx.TextCtrl.__init__,(self,) + _args,_kwargs)
        bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_MENU)
        
        self.SetMinSize(wx.Size(-1, -1))
        self.SetBackgroundColour(bkgCol)
        self.bActive=False
    def SetActive(self,bFlag=True):
        if bFlag:
            bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_ACTIVECAPTION)
            fgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_CAPTIONTEXT)
        else:
            bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_INACTIVECAPTION)
            fgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_INACTIVECAPTIONTEXT)
        self.bActive=bFlag
        self.SetBackgroundColour(bkgCol)
        self.SetForegroundColour(fgCol)
        self.Refresh()
    def IsActive(self):
        return self.bActive
        
