#----------------------------------------------------------------------------
# Name:         vtmMngWX.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20091114
# CVS-ID:       $Id: vtmMngWX.py,v 1.5 2009/11/21 15:03:19 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.art.state.vtArtState as vtArtState
from vidarc.tool.vtThread import vtThreadWX
from vidarc.tool.gui.vtgButtonStateFlag import vtgButtonStateFlag

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

class vtmMngWX(vtgButtonStateFlag):
    def __init__(self,*args,**kwargs):
        vtgButtonStateFlag.__init__(self,*args,**kwargs)
        if hasattr(self,'thdMng')==False:
            self.thdMng=vtThreadWX(self,bPost=True,verbose=VERBOSE,
                    origin=u':'.join([self.GetOrigin(),'mng']))
    def __initStateDict__(self):
        self.__logDebug__(''%())
        return {
                0:{'name':'IDLE','is':'IsIdle','init':True,
                        'img':(vtArtState.getBitmap,(vtArtState.Stopped,),{}),
                        'enter':{'set':['LINKED',],
                                 'clr':['ACTIVE','PAUSING','PAUSED','STOPPING',
                                     'SERVING','ABORTING','STARTING']}
                    },
                10:{'name':'START','is':'IsStart',
                        'img':(vtArtState.getBitmap,(vtArtState.Starting,),{}),
                        'enter':{'set':['LINKED',],
                                 #'func':(self.SocketClosed,(),{}),
                                 'clr':['ACTIVE','PAUSED',]},
                    },
                15:{'name':'RUNNING','is':'IsRunning',
                        'img':(vtArtState.getBitmap,(vtArtState.Running,),{}),
                        'enter':{'set':['ACTIVE','LINKED'],
                                 #'func':(self.SocketClosed,(),{}),
                                 'clr':['PAUSING','PAUSED','STOPPING','ABORTING','STARTING']},
                    },
                60:{'name':'HOLD','is':'IsHold',
                        'img':(vtArtState.getBitmap,(vtArtState.Hold,),{}),
                        'enter':{'set':['PAUSING'],'clr':['ACTIVE','PAUSED']}},
                65:{'name':'HELD','is':'IsHeld',
                        'img':(vtArtState.getBitmap,(vtArtState.Held,),{}),
                        'enter':{'set':['PAUSED'],'clr':['ACTIVE']}},
                30:{'name':'ABORT','is':'IsAbort',
                        'img':(vtArtState.getBitmap,(vtArtState.Aborting,),{}),
                        'enter':{'set':['ABORTING'],'clr':['ACTIVE']}},
                35:{'name':'ABORTED','is':'IsAborted',
                        'img':(vtArtState.getBitmap,(vtArtState.Aborted,),{}),
                        'enter':{'set':['ABORTED'],'clr':['ABORTING','ACTIVE']}},
                40:{'name':'STOP','is':'IsStop',
                        'img':(vtArtState.getBitmap,(vtArtState.Stop,),{}),
                        'enter':{'set':['STOPPING'],'clr':['ACTIVE']}},
                45:{'name':'STOPPED','is':'IsStopped',
                        'img':(vtArtState.getBitmap,(vtArtState.Stopped,),{}),
                        'enter':{'clr':['ACTIVE','STOPPING']}},
                19:{'name':'COMPLETED','is':'IsCompleted',
                        'img':(vtArtState.getBitmap,(vtArtState.Completed,),{}),
                        'enter':{'clr':['ACTIVE']}},
                
                90:{'name':'SHUTTINGDOWN','is':'IsShuttingDown',
                        'img':(vtArtState.getBitmap,(vtArtState.ShuttingDown,),{}),
                        'enter':{#'func':(self.NotifyConnected,(),{}),
                                 'clr':['ACTIVE','PAUSED','LINKED']}
                    },
                95:{'name':'SHUTDOWN','is':'IsShutDown',
                        'img':(vtArtState.getBitmap,(vtArtState.ShutDown,),{}),
                        'enter':{#'func':(self.NotifyConnected,(),{}),
                                 'clr':['ACTIVE','PAUSED','LINKED','SERVING']}
                    },
                }
    def NotifyStateChange(self,ident,statePrev,state,zStamp,*args,**kwargs):
        """ called in case of state change from wx MainLoop
        """
        if VERBOSE>10:
            if self.__isLogDebug__():
                self.__logDebug__('%s'%(self.oSF.GetStrFull(';')))
        self.Refresh()
        self.__postState__(state)
        try:
            if self.funcNotifyStateChange is not None:
                self.CallBack(self.funcNotifyStateChange,ident,statePrev,state,zStamp,*args,**kwargs)
        except:
            self.__logTB__()
        self.__procState__(state,statePrev)
