#Boa:Dialog:vtmListCtrlPopupCtrlAttached
#----------------------------------------------------------------------------
# Name:         vtmListCtrlPopupCtrlAttached.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080810
# CVS-ID:       $Id: vtmListCtrlPopupCtrlAttached.py,v 1.2 2008/08/10 15:52:55 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.misc.vtmListCtrl

import sys

try:
    import vidarc.tool.log.vtLog as vtLog
    from vidarc.tool.gui.vtgCore import vtGuiCoreLimitWindowToScreen
except:
    vtLog.vtLngTB('import')

def create(parent):
    return vtmListCtrlPopupCtrlAttached(parent)

[wxID_VTMLISTCTRLPOPUPCTRLATTACHED, wxID_VTMLISTCTRLPOPUPCTRLATTACHEDLSTPOS, 
] = [wx.NewId() for _init_ctrls in range(2)]

class vtmListCtrlPopupCtrlAttached(wx.Dialog):
    def _init_coll_fgsMain_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstPos, 0, border=0, flag=wx.EXPAND)

    def _init_coll_fgsMain_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_lstPos_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT, heading=u'',
              width=-1)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsMain = wx.FlexGridSizer(cols=1, hgap=0, rows=1, vgap=0)

        self._init_coll_fgsMain_Items(self.fgsMain)
        self._init_coll_fgsMain_Growables(self.fgsMain)

        self.SetSizer(self.fgsMain)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTMLISTCTRLPOPUPCTRLATTACHED,
              name=u'vtmListCtrlPopupCtrlAttached', parent=prnt,
              pos=wx.Point(154, 154), size=wx.Size(400, 250),
              style=wx.SIMPLE_BORDER, title=u'vtmListCtrlPopupCtrlAttached')
        self.SetClientSize(wx.Size(392, 223))

        self.lstPos = vidarc.tool.misc.vtmListCtrl.vtmListCtrl(id=wxID_VTMLISTCTRLPOPUPCTRLATTACHEDLSTPOS,
              name=u'lstPos', parent=self, pos=wx.Point(0, 0), size=wx.Size(392,
              223), style=wx.LC_REPORT | wx.LC_NO_HEADER)
        self._init_coll_lstPos_Columns(self.lstPos)
        self.lstPos.Bind(wx.EVT_KILL_FOCUS, self.OnLstPosKillFocus)
        self.lstPos.Bind(wx.EVT_SET_FOCUS, self.OnLstPosSetFocus)
        self.lstPos.Bind(wx.EVT_CHAR, self.OnLstPosChar)

        self._init_sizers()

    def __init__(self, parent,funcData,*argsData,**kwargsData):
        self._init_ctrls(parent)
        self.lstPos.SetStretchLst([(0,-1.0)])
        self.SetDataFunc(funcData,*argsData,**kwargsData)
        self.funcCB=None
        self.argsCB=None
        self.kwargsCB=None
    def SetSingleStyle(self,style,add=True):
        self.lstPos.SetSingleStyle(style,add)
    def SetDataFunc(self, funcData,*argsData,**kwargsData):
        self.funcData=funcData
        self.argsData=argsData
        self.kwargsData=kwargsData
    def SetCB(self,funcCB,*argsCB,**kwargsCB):
        self.funcCB=funcCB
        self.argsCB=argsCB
        self.kwargsCB=kwargsCB
    def Attach(self,wid):
        iX,iY = wid.ClientToScreen( (0,0) )
        iW,iH =  wid.GetSize()
        iPopW,iPopH=self.GetSize()
        iX,iY,iW,iH=vtGuiCoreLimitWindowToScreen(iX,iY+iH,iW,iPopH)
        self.Move((iX,iY))
        self.SetSize((iW,iH))
    def UpdateData(self):
        try:
            self.lstPos.DeleteAllItems()
            if self.funcData is not None:
                l=self.funcData(*self.argsData,**self.kwargsData)
                for s in l:
                    idx=self.lstPos.InsertStringItem(sys.maxint,s)
        except:
            vtLog.vtLngTB(self.GetName())
    def Show(self,flag):
        if flag:
            self.UpdateData()
        else:
            if self.funcCB is not None:
                iCount=self.lstPos.GetItemCount()
                for i in xrange(iCount):
                    if self.lstPos.GetItemState(i,wx.LIST_STATE_SELECTED)==wx.LIST_STATE_SELECTED:
                        s=self.lstPos.GetItemText(i)
                        self.funcCB(i,s,*self.argsCB,**self.kwargsCB)
        wx.Dialog.Show(self,flag)
    def OnLstPosKillFocus(self, event):
        event.Skip()
        self.Show(False)
    def OnLstPosChar(self, event):
        iKey=event.GetKeyCode()
        if iKey in [wx.WXK_ESCAPE,wx.WXK_RETURN]:
            self.Show(False)
        else:
            event.Skip()

    def OnLstPosSetFocus(self, event):
        event.Skip()
