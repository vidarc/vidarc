#----------------------------------------------------------------------------
# Name:         vtmMngPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20091115
# CVS-ID:       $Id: vtmMngPanel.py,v 1.3 2009/11/18 20:19:32 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vtmThreadCtrl import vtmThreadCtrl
from vtmMngWX import vtmMngWX

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

class vtmMngPanel(vtmThreadCtrl):
    def __init__(self,*args,**kwargs):
        vtmThreadCtrl.__init__(self,*args,**kwargs)
        if 'clsMng' in kwargs:
            clsMng=kwargs.get('clsMng')
            _kwargs=kwargs.copy()
            _kwargs['parent']=self
            _kwargs['size']=(30,30)
            self.mngCtrl=clsMng(*args,**_kwargs)
            bxs=self.GetCtrlSizer()
            self.InsertWidget(0,self.mngCtrl,bCenter=True,bLeft=True,bRight=True,iBorder=4)
            self.SetThread(self.mngCtrl)
        else:
            self.mngCtrl=None
            self.__logCritical__('manager class not defined;kwargs:%s'%(self.__logFmt__(kwargs)))
    def __getattr__(self,name):
        if hasattr(self.mngCtrl,name):
            return getattr(self.mngCtrl,name)
        raise AttributeError(name)
    def OnCbStartButton(self,evt):
        evt.Skip()
        try:
            self.__logInfo__(''%())
            self.DoState('START',bSched=False)
            #self.DoFunc('Start')
            return
            self.Start()
            if self.funcStart is not None:
                func,args,kwargs=self.funcStart
                func(*args,**kwargs)
            #self.Start()
        except:
            self.__logTB__()
    def __stop__(self,bEnStart=True,bClr=True):
        if bEnStart:
            if bClr==True:
                self.txtVal.SetValue(u'')
                self.txtCount.SetValue(u'')
            self.pbProc.SetValue(0)
            self.cbStart.Enable(True)
        self.cbStop.Enable(False)
        self.fProcLast=-1
    def OnCbStopButton(self,evt):
        evt.Skip()
        try:
            self.__logInfo__(''%())
            self.DoState('STOP',bSched=False)
            #self.DoFunc('Stop')
            return
            if self.funcStop is not None:
                func,args,kwargs=self.funcStop
                func(*args,**kwargs)
            if self.thd2Ctrl is not None:
                #self.thd2Ctrl.Stop()
                self.__stop__(bEnStart=True,bClr=False)
            else:
                self.__stop__(bEnStart=True,bClr=False)
        except:
            self.__logTB__()
    def Do(self,func,*args,**kwargs):
        self.mngCtrl.Do(func,*args,**kwargs)
