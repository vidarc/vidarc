#----------------------------------------------------------------------------
# Name:         vtSlotBuffer.py
# Purpose:
# Author:       Walter Obweger
#
# Created:      20061119
# CVS-ID:       $Id: vtSlotBuffer.py,v 1.3 2009/05/01 20:04:13 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vtThreadCore import getLock

class vtSlotBuffer:
    def __init__(self,size):
        self.iSize=size
        self.iFill=0
        self.lSlot=[None]*self.iSize
        self.semAccess=getLock()
    def isFill(self):
        try:
            self.semAccess.acquire()
            if self.iFill>0:
                return True
            else:
                return False
        finally:
            self.semAccess.release()
    def isFree(self):
        try:
            self.semAccess.acquire()
            if self.iFill<self.iSize:
                return True
            else:
                return False
        finally:
            self.semAccess.release()
    def put(self,t):
        try:
            self.semAccess.acquire()
            if self.iFill<self.iSize:
                iAct=0
                while self.lSlot[iAct] is not None:
                    iAct+=1
                    if iAct>=self.iSize:
                        return -1
                self.lSlot[iAct]=t
                self.iFill+=1
                return iAct
            else:
                return -1
        finally:
            self.semAccess.release()
    def pop(self,iPos):
        try:
            self.semAccess.acquire()
            if self.iFill>0:
                t=self.lSlot[iPos]
                self.lSlot[iPos]=None
                self.iFill-=1
                return t
            else:
                return None
        finally:
            self.semAccess.release()
    def get(self,iPos):
        try:
            self.semAccess.acquire()
            return self.lSlot[iPos]
        finally:
            self.semAccess.release()
    def proc(self,func,*args,**kwargs):
        try:
            self.semAccess.acquire()
            for i in xrange(0,self.iSize):
                func(self.lSlot[i],*args,**kwargs)
        finally:
            self.semAccess.release()
