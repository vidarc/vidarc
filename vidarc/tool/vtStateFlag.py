#----------------------------------------------------------------------------
# Name:         vtStateFlag.py
# Purpose:      general object for state and flag representation
#
# Author:       Walter Obweger
#
# Created:      20070719
# CVS-ID:       $Id: vtStateFlag.py,v 1.16 2009/10/11 21:08:18 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

from threading import RLock
#from threading import Semaphore
#from threading import Lock
from vidarc.tool.vtThread import CallBack

import vidarc.tool.log.vtLog as vtLog
from vidarc.vTimeStamp import getTimeStamp

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

wxEVT_VT_STATEFLAG_CHANGED=wx.NewEventType()
vEVT_VT_STATEFLAG_CHANGED=wx.PyEventBinder(wxEVT_VT_STATEFLAG_CHANGED,1)
def EVT_VT_STATEFLAG_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VT_STATEFLAG_CHANGED,func)
def EVT_VT_STATEFLAG_CHANGED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,wxEVT_VT_STATEFLAG_CHANGED)
class vtStateFlagChanged(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VT_STATEFLAG_CHANGED(<widget_name>, self.OnChanged)
    """
    def __init__(self,obj,ident,statePrev,state,flagPrev,flag,idWid=None):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        if idWid is None:
            self.SetId(obj.GetId())
        else:
            self.SetId(idWid)
        self.SetEventType(wxEVT_VT_STATEFLAG_CHANGED)
        self.ident=ident
        self.statePrev=statePrev
        self.state=state
        self.flagPrev=flagPrev
        self.flag=flag
    def GetIdent(self):
        return self.ident
    def GetStatePrev(self):
        return self.statePrev
    def GetState(self):
        return self.state
    def GetFlagPrev(self):
        return self.flagPrev
    def GetFlag(self):
        return self.flag

class vtStateFlag:
    UNDEF_NAME='???'
    UNDEF_TRANS='???'
    def __init__(self,dState,dFlag,par=None,ident=None,
                    funcNotify=None,funcNotifyCB=None,verbose=0,origin=None):
        """         dState=
            { 0x0001 :                              # state id
               { 'name'         : 'STOPPED' ,       # engineering state name
                 'translation'  : _(u'stopped') ,   # translated state name
                 'image'        : wx.Bitmap ,       # image for state
                 'init'         : True ,            # 
                 'is'           : None ,            # export capitialized is-attribute here IsStopped 
                 'enter'        : {'set':['aly'],   #
                                   'func':(func,*args,**kwargs),
                                   'funcs':[
                                        (func1,*args1,**kwargs1),
                                        (func2,*args2,**kwargs2),
                                        ],
                                   'CB':(func.*args,**kwargs),
                                   },
                 'leave'        : {'set':['fin','rdy'],
                                   'clr':['aly','wrk'],
                                   'CBs':[
                                        (func1,*args1,**kwargs2),
                                        (func2,*args2,**kwargs2),
                                        ],
                                   },

                } ,
              0x0002 :                              # state id
               { 'name'         : 'DISCONN' ,       # 
                 'is'           : 'IsDisConn',      # export fixed is-attribute
                    } ,
              None :                                # fallback infos
                { 'name'        : 'undef' ,         # fallback name
                  'translation' : _(u'undefined') , # fallback translation name
                  'image'       : wx.Bitmap ,       # fallback image
                } ,
            }
        dFlag= {
                0x001:{
                  'name'        : 'aly',
                  'translation' : 'analyzed',
                  'order'       : 1,                # for flag output
                  'is'          : None},
                0x002:{
                  'name'        : 'fin',
                  'translation' : 'finished',
                  'is'          : None},
                0x004:{'name':'wrk','translation':'finished','is':None},
                0x008:{'name':'rdy','translation':'finished','is':None},
                0x010:{
                    'name':'cfg',
                    'translation':'config',
                    'keep':True,                    # keep this flag in case of clearing data
                    'is':None},
                    }
        """
        self.__dState={}
        self.__dFlag={}
        self.__dMapState={}
        self.__dMapFlag={}
        self.__lPriorFlag=[]
        self.__state = None
        self.__statePrev = None
        self.__iFlag = 0x0000
        self.__iFlagPrev = 0x0000
        self.__iFlagKeep = 0x0000
        self.__verbose=verbose
        if VERBOSE>0:
            self.__verbose=VERBOSE
        self.__ident=ident
        self.par=par
        try:
            if hasattr(par,'GetWidId'):
                self.idWid=oar.GetWidId()
            elif hasattr(par,'GetId'):
                self.idWid=par.GetId()
            else:
                self.idWid=-1
        except:
            self.idWid=-2
        self.__origin__(origin)
        self.__funcNotify=funcNotify
        self.__funcNotifyCB=funcNotifyCB
        self.__semAccess=RLock()#Semaphore()
        self.__zStamp=getTimeStamp()
        self.__zStampPrev=self.__zStamp
        self.AddStateDict(dState)
        self.AddFlagDict(dFlag)
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,''%(),'del')
        try:
            del self.__semAccess
            del self.__dState
            del self.__dFlag
            del self.__dMapState
            del self.__dMapFlag
            del self.__lPriorFlag
        except:
            #vtLog.vtLngTB('del')
            pass
    def __str__(self):
        return self.GetStr(u';')
    def GetStr(self,sep):
        self._acquire()
        try:
            return sep.join([
                u''.join(['%d'%((self.__iFlag & flag )== flag) for flag in self.__lFlag]),
                self.__getStateInfo(self.__state,'name',default=self.UNDEF_NAME),
                self.__getStateInfo(self.__statePrev,'name',default=self.UNDEF_NAME),
                self.__zStamp,
                ])
        finally:
            self._release()
    def GetStrFull(self,sep):
        self._acquire()
        try:
            l=[
                u' '.join([self.__zStamp,
                    self.__getStateInfo(self.__state,'name',default=self.UNDEF_NAME)]),
                u' '.join([self.__zStampPrev,
                    self.__getStateInfo(self.__statePrev,'name',default=self.UNDEF_NAME)]),
                u'flag',
                ]
            for flag in self.__lFlag:
                l.append(u''.join([
                    self.__getFlagInfo(flag,'name',default=self.UNDEF_NAME) ,
                    '=%d(%d)'%((self.__iFlag & flag )== flag,(self.__iFlagPrev & flag )== flag)]))
            return sep.join(l)
        finally:
            self._release()
    def __logInfo__(self,msg=''):
        vtLog.vtLngCur(vtLog.INFO,msg,self.GetOrigin(),level2skip=2)
    def __logVerbose__(self,msg=''):
        if self.__verbose:
            vtLog.vtLngCur(vtLog.DEBUG,msg,self.GetOrigin(),level2skip=2)
    def __origin__(self,origin):
        if origin is None:
            if self.par is not None:
                try:
                    self.origin=vtLog.vtLngGetRelevantNamesWX(self.par,self.__class__.__name__)
                except:
                    try:
                        baseWid=vtLog.GetPrintMsgWid(self.par)
                        if baseWid is not None:
                            self.origin=''.join([baseWid.GetName(),'.',par.GetName(),':',self.__class__.__name__])
                        else:
                            self.origin=''.join([self.par.GetName(),':',self.__class__.__name__])#':thread'
                    except:
                        vtLog.vtLngTB(self.__class__.__name__)
                        self.origin=self.__class__.__name__
            else:
                vtLog.vtLngCurCls(vtLog.WARN,'no parent defined and no origin given'%(),self)
                self.origin=self.__class__.__name__
        else:
            self.origin=origin
    def GetOrigin(self):
        return self.origin
    def SetOrigin(self,origin):
        self.__origin__(origin)
    def _acquire(self,blocking=True):
        if self.__semAccess.acquire(blocking):
            return True
        return False
    def _release(self):
        self.__semAccess.release()
    def __getStateInit(self):
        for k,d in self.__dState.iteritems():
            if 'init' in d:
                self.__state=k
                self.__statePrev=k
                return True
        return False
    def __getFlagInit(self):
        for k,d in self.__dFlag.iteritems():
            if 'init' in d:
                self.__iFlag=k
                return True
        return False
    def SetPostWid(self,par):
        self.par=par
        try:
            if hasattr(par,'GetWidId'):
                self.idWid=oar.GetWidId()
            elif hasattr(par,'GetId'):
                self.idWid=par.GetId()
            else:
                self.idWid=-1
        except:
            self.idWid=-2
        self.__origin__(None)
    def BindEvents(self,func=None):
        if self.par is not None:
            if func is not None:
                EVT_VT_STATEFLAG_CHANGED(self.par,func)
    def AddStateDict(self,dState):
        self._acquire()
        try:
            iRet=0x00
            self.__dState.update(dState)
            if self.__getStateInit()==False:
                iRet=0x01
            self.__dMapState={}
            for k,d in self.__dState.iteritems():
                if 'is' in d:
                    if d['is'] is None:
                        if 'name' in d:
                            sN=''.join(['Is',d['name'].capitalize()])
                    else:
                        sN=d['is']
                    if sN in self.__dMapState:
                        iRet|=0x02
                    else:
                        self.__dMapState[sN]=k
                    #eval('self.%s=self.%s'%sN,sN)
                if 'name' in d:
                    self.__dMapState[d['name']]=k
        finally:
            self._release()
        return iRet
    def AddFlagDict(self,dFlag):
        self._acquire()
        try:
            iRet=0x00
            self.__dFlag.update(dFlag)
            self.__getFlagInit()
            self.__dMapFlag={}
            self.__lFlag=[]
            l=[]
            for k,d in self.__dFlag.iteritems():
                if 'is' in d:
                    if d['is'] is None:
                        if 'name' in d:
                            sN=''.join(['Is',d['name'].capitalize()])
                    else:
                        sN=d['is']
                    if sN in self.__dMapFlag:
                        iRet|=0x02
                    else:
                        self.__dMapFlag[sN]=k
                    #eval('def %s(self):\n\treturn True'%sN)
                    #eval('self.%s=self.%s'%(sN,sN))
                if 'name' in d:
                    self.__dMapFlag[d['name']]=k
                if 'order' in d:
                    l.append((d['order'],k))
                if 'keep' in d:
                    if d['keep']:
                        self.__iFlagKeep |= k
            l.sort()
            self.__lFlag=[k for n,k in l]
        finally:
            self._release()
        return iRet
    def Clear(self):
        self._acquire()
        try:
            self.__getStateInit()
            flag=self.__iFlag & self.__iFlagKeep
            self.__iFlag = flag
            self.__iFlagPrev = flag
            self.__getFlagInit()
            self.__zStamp=getTimeStamp()
            self.__zStampPrev=self.__zStamp
        finally:
            self._release()
    def GetState(self):
        self._acquire()
        try:
            return self.__state
        finally:
            self._release()
    def __IsStateChanged__(self,statePrev=None,state=None):
            if statePrev is None:
                statePrev=self.__statePrev
            if state is None:
                state=self.__state
            return state!=statePrev
    def IsStateChanged(self,statePrev=None,state=None):
        self._acquire()
        try:
            return self.__IsStateChanged__(statePrev=statePrev,state=state)
        finally:
            self._release()
    def SetState(self,state,*args,**kwargs):
        self._acquire()
        try:
            if state==self.__state:
                return
            if state not in self.__dState:
                vtLog.vtLngCur(vtLog.CRITICAL,'state:%s invalid state'%(state),self.GetOrigin())
                return
            self.__zStampPrev=self.__zStamp
            self.__zStamp=getTimeStamp()
            self.__statePrev=self.__state
            self.__state=state
            if self.__calcFlags():
                if self.__verbose:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'state:%-15s %d;prev:%-15s %d;flag:0x%08d,0x%08d'%(\
                                self.__getStateInfo(self.__state,'name',default=self.UNDEF_NAME),self.__state,
                                self.__getStateInfo(self.__statePrev,'name',default=self.UNDEF_NAME),
                                self.__statePrev,self.__iFlag,self.__iFlagPrev),self.GetOrigin())
                if self.__funcNotify:
                    self.__funcNotify(self.__ident,self.__statePrev,self.__state,self.__zStamp,*args,**kwargs)
                if self.__funcNotifyCB:
                    CallBack(self.__funcNotifyCB,self.__ident,self.__statePrev,self.__state,self.__zStamp,*args,**kwargs)
                if self.par is not None:
                    wx.PostEvent(self.par,vtStateFlagChanged(self.par,self.__ident,
                                self.__statePrev,self.__state,
                                self.__iFlagPrev,self.__iFlag,
                                self.idWid))
        finally:
            self._release()
    def __isFlagChanged__(self,flagPrev=None,flag=None):
        if flagPrev is None:
            flagPrev=self.__flagPrev
        if flag is None:
            flag=self.__flag
        return flag!=flagPrev
    def IsFlagChanged(self,flagPrev=None,flag=None):
        self._acquire()
        try:
            return self.__isFlagChanged__(flagPrev=flagPrev,flag=flag)
        finally:
            self._release()
    def __calcFlags(self):
        if self.__IsStateChanged__()==False:
            return False
        try:
            def calc(d,flag):
                if 'set' in dd:
                    for s in dd['set']:
                        flag |= self.__dMapFlag[s]
                if 'clr' in dd:
                    for s in dd['clr']:
                        flag &= ~ self.__dMapFlag[s]
                return flag
            self.__iFlagPrev=self.__iFlag
            d=self.__dState[self.__statePrev]
            if 'leave' in d:
                dd=d['leave']
                self.__iFlag=calc(dd,self.__iFlag)
                try:
                    if 'CB' in dd:
                        func,args,kwargs=dd['CB']
                        CallBack(func,*args,**kwargs)
                except:
                    vtLog.vtLngTB(self.GetOrigin())
                    vtLog.vtLngCur(vtLog.ERROR,'CB;%r'%(dd['CB']),self.GetOrigin())
                try:
                    if 'CBs' in dd:
                        for func,args,kwargs in dd['CBs']:
                            CallBack(func,*args,**kwargs)
                except:
                    vtLog.vtLngTB(self.GetOrigin())
                    vtLog.vtLngCur(vtLog.ERROR,'CBs;%r'%(dd['CBs']),self.GetOrigin())
                try:
                    if 'func' in dd:
                        func,args,kwargs=dd['func']
                        func(*args,**kwargs)
                    if 'funcs' in dd:
                        for func,args,kwargs in dd['funcs']:
                            func(*args,**kwargs)
                except:
                    vtLog.vtLngTB(self.GetOrigin())
            d=self.__dState[self.__state]
            if 'enter' in d:
                dd=d['enter']
                self.__iFlag=calc(dd,self.__iFlag)
                try:
                    if 'CB' in dd:
                        t=dd['CB']
                        if t is not None:
                            func,args,kwargs=t
                            CallBack(func,*args,**kwargs)
                except:
                    vtLog.vtLngTB(self.GetOrigin())
                    vtLog.vtLngCur(vtLog.ERROR,'CB;%r'%(dd['CB']),self.GetOrigin())
                try:
                    if 'CBs' in dd:
                        for func,args,kwargs in dd['CBs']:
                            CallBack(func,*args,**kwargs)
                except:
                    vtLog.vtLngTB(self.GetOrigin())
                    vtLog.vtLngCur(vtLog.ERROR,'CBs;%r'%(dd['CBs']),self.GetOrigin())
                try:
                    if 'func' in dd:
                        func,args,kwargs=dd['func']
                        func(*args,**kwargs)
                    if 'funcs' in dd:
                        for func,args,kwargs in dd['funcs']:
                            func(*args,**kwargs)
                except:
                    vtLog.vtLngTB(self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
        return True
    def CallBack(self,func,*args,**kwargs):
        CallBack(func,*args,**kwargs)
    def _setFunc(self,state,kind,which,func,*args,**kwargs):
            if state in self.__dState:
                d=self.__dState[state]
                if kind in d:
                    dd=d[kind]
                else:
                    dd={}
                    d[kind]=dd
                dd[which]=(func,args,kwargs)
    def _addFunc(self,state,kind,which,func,*args,**kwargs):
            if state in self.__dState:
                d=self.__dState[state]
                if kind in d:
                    dd=d[kind]
                else:
                    dd={}
                    d[kind]=dd
                if which in dd:
                    l=dd[which]
                else:
                    l=[]
                    dd[which]=l
                l.append((func,args,kwargs))
    def _delFunc(self,state,kind,which,func,*args,**kwargs):
            if state in self.__dState:
                d=self.__dState[state]
                if kind in d:
                    dd=d[kind]
                else:
                    dd={}
                    d[kind]=dd
                if which in dd:
                    l=dd[which]
                    try:
                        l.remove((func,args,kwargs))
                    except:
                        vtLog.vtLngTB(self.GetOrigin())
    def _clrFunc(self,state,kind,which):
            if state in self.__dState:
                d=self.__dState[state]
                if kind in d:
                    dd=d[kind]
                    if which in dd:
                        del dd[which]
    def SetFunc(self,state,kind,func,*args,**kwargs):
        self._acquire()
        try:
            self._setFunc(state,kind,'func',func,*args,**kwargs)
        finally:
            self._release()
    def AddFunc(self,state,kind,func,*args,**kwargs):
        self._acquire()
        try:
            self._addFunc(state,kind,'funcs',func,*args,**kwargs)
        finally:
            self._release()
    def DelFunc(self,state,kind,func,*args,**kwargs):
        self._acquire()
        try:
            self._delFunc(state,kind,'funcs',func,*args,**kwargs)
        finally:
            self._release()
    def ClrFunc(self,state,kind):
        self._acquire()
        try:
            self._clrFunc(state,kind,'func')
        finally:
            self._release()
    def ClrFuncs(self,state,kind):
        self._acquire()
        try:
            self._clrFunc(state,kind,'funcs')
        finally:
            self._release()
    def SetCB(self,state,kind,func,*args,**kwargs):
        self._acquire()
        try:
            self._setFunc(state,kind,'CB',func,*args,**kwargs)
        finally:
            self._release()
    def AddCB(self,state,kind,func,*args,**kwargs):
        self._acquire()
        try:
            self._addFunc(state,kind,'CBs',func,*args,**kwargs)
        finally:
            self._release()
    def DelCB(self,state,kind,func,*args,**kwargs):
        self._acquire()
        try:
            self._delFunc(state,kind,'CBs',func,*args,**kwargs)
        finally:
            self._release()
    def ClrCB(self,state,kind):
        self._acquire()
        try:
            self._clrFunc(state,kind,'CB')
        finally:
            self._release()
    def ClrCBs(self,state,kind):
        self._acquire()
        try:
            self._clrFunc(state,kind,'CBs')
        finally:
            self._release()
    def __getStateDict(self,state):
        if state in self.__dState:
            return self.__dState[state]
        else:
            if None in self.__dState:
                return self.__dState[None]
        return None
    def __getStateNameDict(self,state):
        if state in self.__dMapState:
            return self.__getStateDict(self.__dMapState[state])
        else:
            if None in self.__dState:
                return self.__dState[None]
        return None
    def __getFlagDict(self,flag):
        if flag in self.__dFlag:
            return self.__dFlag[flag]
        else:
            if None in self.__dFlag:
                return self.__dFlag[None]
        return None
    def __getFlagNameDict(self,Flag):
        if state in self.__dMapFlag:
            return self.__getFlagDict(self.__dMapFlag[flag])
        else:
            if None in self.__dFlag:
                return self.__dFlag[None]
        return None
    def GetStateAct(self):
        self._acquire()
        try:
            return self.__state
        finally:
            self._release()
    def GetStatePrev(self):
        self._acquire()
        try:
            return self.__statePrev
        finally:
            self._release()
    def __getStateInfo(self,state,info,default=None):
        try:
            d=self.__getStateDict(state)
            if d is None:
                return default
            else:
                if info in d:
                    return d[info]
                else:
                    d=self.__getStateDict(None)
                    if info in d:
                        return d[info]
                    else:
                        return default
        except:
            return default
    def GetStateNameAct(self):
        self._acquire()
        try:
            return self.__getStateInfo(self.__state,'name',default=self.UNDEF_NAME)
        finally:
            self._release()
    def GetStateTransAct(self):
        self._acquire()
        try:
            return self.__getStateInfo(self.__state,'translation',default=self.UNDEF_TRANS)
        finally:
            self._release()
    def GetStateImgAct(self):
        self._acquire()
        try:
            return self.__getStateInfo(self.__state,'image',default=None)
        finally:
            self._release()
    def GetStateInfo(self,state,info,default=None):
        self._acquire()
        try:
            return self.__getStateInfo(state,info,default=default)
        finally:
            self._release()
        return default
    def GetStateName(self,state):
        self._acquire()
        try:
            return self.__getStateInfo(state,'name',default=self.UNDEF_NAME)
        finally:
            self._release()
    def GetStateTrans(self,state):
        self._acquire()
        try:
            return self.__getStateInfo(state,'translation',default=self.UNDEF_TRANS)
        finally:
            self._release()
    def GetStateImg(self,state):
        self._acquire()
        try:
            return self.__getStateInfo(state,'image',default=None)
        finally:
            self._release()
    def GetStateInfoLst(self,info,default=None):
        self._acquire()
        try:
            return [(state,self.__getStateInfo(state,info,default=default)) for state in self.__dState.iterkeys()]
        finally:
            self._release()
        return [(default,default,)]
    def GetStateLst(self):
        self._acquire()
        try:
            l=self._dState.keys()
            return l
        finally:
            self._release()
    def __getFlagInfo(self,flag,info,default=None):
        try:
            d=self.__getFlagDict(flag)
            if d is None:
                return default
            else:
                if info in d:
                    return d[info]
                else:
                    d=self.__getFlagDict(None)
                    if info in d:
                        return d[info]
                    else:
                        return default
        except:
            return default
    def GetFlagLst(self,flag=None):
        self._acquire()
        try:
            l=[]
            if flag is None:
                flag=self.__iFlag
            for id in self.__lFlag:
                l.append((id,(flag & id )== id))
            return l
        finally:
            self._release()
    def GetFlagNameLst(self,flag=None,sep=' '):
        l=self.GetFlagLst(flag=flag)
        return [self.GetFlagName(id) for id,bVal in l]
    def GetFlagTransLst(self,flag=None,sep=' '):
        l=self.GetFlagLst(flag=flag)
        return [self.GetFlagTrans(id) for id,bVal in l]
    def GetFlagNameStr(self,flag=None,sep=' '):
        l=self.GetFlagLst(flag=flag)
        return sep.join([':'.join([self.GetFlagName(id),'%d'%bVal]) for id,bVal in l])
    def GetFlagTransStr(self,flag=None,sep=' '):
        l=self.GetFlagLst(flag=flag)
        return sep.join([':'.join([self.GetFlagTrans(id),'%d'%bVal]) for id,bVal in l])
    def GetFlagPrev(self):
        self._acquire()
        try:
            return self.__iFlagPrev
        finally:
            self._release()
    def GetFlagAct(self):
        self._acquire()
        try:
            return self.__iFlag
        finally:
            self._release()
    def GetFlagInfo(self,flag,info,default=None):
        self._acquire()
        try:
            return self.__getFlagInfo(flag,info,default=default)
        finally:
            self._release()
    def GetFlagName(self,flag):
        self._acquire()
        try:
            return self.__getFlagInfo(flag,'name',default=self.UNDEF_NAME)
        finally:
            self._release()
    def GetFlagTrans(self,flag):
        self._acquire()
        try:
            return self.__getFlagInfo(flag,'translation',default=self.UNDEF_TRANS)
        finally:
            self._release()
    def GetFlagImg(self,flag):
        self._acquire()
        try:
            return self.__getFlagInfo(flag,'image',default=None)
        finally:
            self._release()
    def __getattr__(self,name):
        #if self._acquire(blocking=True):
        if self.__semAccess.acquire(True):
            try:
                if name.startswith('Is'):
                    if name in self.__dMapState:
                        return self.__state == self.__dMapState[name]
                    if name in self.__dMapFlag:
                        id=self.__dMapFlag[name]
                        return (self.__iFlag & id )== id
                if name in self.__dMapFlag:
                    return self.__dMapFlag[name]
                if name in self.__dMapState:
                    return self.__dMapState[name]
                raise AttributeError(name)
            finally:
                #self._release()
                self.__semAccess.release()
        raise AttributeError(name)
    def SetFlagByName(self,s,bVal=True):
        self.__iFlagPrev=self.__iFlag
        flag=self.__iFlag
        if bVal:
            if s in self.__dMapFlag:
                flag |= self.__dMapFlag[s]
        else:
            if s in self.__dMapFlag:
                flag &= ~ self.__dMapFlag[s]
        self.__iFlag=flag
    def SetFlag(self,flag,bVal=True):
        self.__iFlagPrev=self.__iFlag
        if bVal:
            self.__iFlag |= flag
        else:
            self.__iFlag &= ~ flag

if __name__=='__main__':
    o=vtStateFlag(
            {
                0x000:{'name':'init','translation':'initialized','init':True},
                0x001:{'name':'busy','translation':'busy'},
                0x002:{'name':'pause','translation':'pause'},
            },
            {
                0x001:{'name':'aly','translation':'analyzed','is':None},
                0x002:{'name':'fin','translation':'finished','is':None},
            },None)
    print 'example'
    print '%20s:'%'IsAly',o.IsAly
    print '%20s:'%'init',o.init
    print '%20s:'%'aly',o.aly
    print '%20s:'%'act',o.GetStateNameAct(),',',o.GetStateTransAct()
    print '%20s:'%'some state',o.GetStateName(o.init),',',o.GetStateTrans(o.init)
    print '%20s:'%'some state wrong',o.GetStateName(4),',',o.GetStateTrans(4)
    print '%20s:'%'some flag',o.GetFlagName(o.aly),',',o.GetFlagTrans(o.aly)
    print '%20s:'%'some flag wrong',o.GetFlagName(o.init),',',o.GetFlagTrans(o.init)
    
