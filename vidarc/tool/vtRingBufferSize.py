#----------------------------------------------------------------------------
# Name:         vtRingBufferSize.py
# Purpose:
# Author:       Walter Obweger
#
# Created:      20061119
# CVS-ID:       $Id: vtRingBufferSize.py,v 1.5 2009/05/09 19:58:58 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vtThreadCore import getRLock

class vtRingBufferSize:
    def __init__(self,size):
        self.iSize=size
        self.iAct=0
        self.iFill=0
        self.lSlot=[None]*self.iSize
        self.semAccess=getRLock()
    def __fill__(self):
        i=self.iFill-self.iAct
        if i<0:
            i+=self.iSize
        return i
    def fill(self):
        try:
            self.semAccess.acquire()
            return self.__fill__()
        finally:
            self.semAccess.release()
    def __free__(self):
        return self.iSize-self.__fill__()
    def free(self):
        try:
            self.semAccess.acquire()
            return self.__free__()
        finally:
            self.semAccess.release()
    def __isFree__(self):
        return self.__free__()>1
    def isFree(self):
        if self.free()>1:
            return True
        else:
            return False
    def put(self,t):
        try:
            self.semAccess.acquire()
            if self.__isFree__():
                self.lSlot[self.iFill]=t
                self.iFill= (self.iFill+1) % self.iSize
                return 0
            else:
                return -1
        finally:
            self.semAccess.release()
    def pop(self):
        try:
            self.semAccess.acquire()
            if self.__fill__()>0:
                t=self.lSlot[self.iAct]
                self.lSlot[self.iAct]=None
                self.iAct= (self.iAct+1) % self.iSize
                return t
            else:
                return None
        finally:
            self.semAccess.release()
    def get(self,iPos=0):
        try:
            self.semAccess.acquire()
            if self.__fill__()>0:
                iPos=iPos % self.iSize
                iAct=self.iAct
                iAct+=iPos
                iAct=iAct % self.iSize
                return self.lSlot[iAct]
            else:
                return None
        finally:
            self.semAccess.release()
    def proc(self,func,*args,**kwargs):
        try:
            self.semAccess.acquire()
            iAct=self.iAct
            for i in xrange(0,self.fill()):
                func(self.lSlot[iAct],*args,**kwargs)
                iAct+=1
                if iAct>=self.iSize:
                    iAct=0
        finally:
            self.semAccess.release()
