#----------------------------------------------------------------------------
# Name:         vtPrtTable.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060311
# CVS-ID:       $Id: vtPrtTable.py,v 1.2 2008/03/16 22:20:21 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.prt.vtPrtDocument import vtPrtDocument
from vidarc.tool.draw.vtDrawCanvasText import *
from vidarc.tool.draw.vtDrawCanvasLine import *

import vidarc.tool.log.vtLog as vtLog

import types,string

class vtPrtTable(vtPrtDocument):
    def __init__(self):
        vtPrtDocument.__init__(self)
        self.data={}
        self.legend={}
        self.raster=[[],[]]
        self.lColLabel=[]
        self.lColWidth=None
    def SetData(self,data):
        #vtLog.pprint(data)
        self.data=data
    def SetLegend(self,legend):
        self.legend=legend
    def SetColumnLabel(self,colLabel):
        self.lColLabel=colLabel
    def SetColumnWidth(self,colWidth):
        self.lColWidth=colWidth
    def __calcData__(self):
        try:
            self.pagesObjs=[]
            self.pageObjs=[]
            dc = self.GetDC()
            
            self.iCol=0
            self.iColInfo=0
            self.iColNum=0
            self.iNumIdx=0
            self.iNums=[0]
            self.iY=self.iYstart+1
            def calc(data,l):
                iCol=0
                for d in data:
                    self.iNumIdx+=1
                    if type(d)==types.ListType:
                        iLen=len(d)
                        if iLen>self.iCol:
                            self.iCol=iLen
                        # add here
                    elif type(d)==types.TupleType:
                        self.iNums[-1]+=1
                        #print d
                        #print '   ',d[-1]
                        iLen=len(d)-1
                        if iLen>self.iColInfo:
                            self.iColInfo=iLen
                        lNum=[str(self.iNumIdx)]
                        for i in self.iNums:
                            lNum.append(str(i))
                        lInfo=[]
                        for dd in d[:-1]:
                            lInfo.append(dd)
                        l=[lNum,lInfo]
                        self.pageObjs.append(l)
                        self.iY+=1
                        if self.iY>=self.iYmax:
                            self.pagesObjs.append(self.pageObjs)
                            self.pageObjs=[]
                            self.iY=self.iYstart+1
                        self.iNums.append(0)
                        iLen=len(self.iNums)
                        if iLen>self.iColNum:
                            self.iColNum=iLen
                        calc(d[-1],l)
                        self.iNums.pop()
                    else:
                        iCol+=1
                        if iCol>self.iCol:
                            self.iCol=iCol
                        self.iNumIdx-=1
                        l.append(d)
            calc(self.data,[])
            self.pagesObjs.append(self.pageObjs)
            
            vtLog.vtLngCur(vtLog.DEBUG,'pages:%d'%len(self.pagesObjs),'printout')
            vtLog.vtLngCur(vtLog.DEBUG,
                    'col:%d,colInfo:%d,colNum:%d'%(self.iCol,self.iColInfo,self.iColNum),
                    'printout')
            #vtLog.CallStack('')
            #vtLog.pprint(self.pagesObjs)
            #vtLog.pprint(self.pageObjs)
            #print 'col:%d,colInfo:%d,colNum:%d'%(self.iCol,self.iColInfo,self.iColNum)
            
            # build output objects
            
            iCol=self.iCol+self.iColInfo+self.iColNum
            iColInfo=iCol-self.iCol
            
            bCalcExtend=False
            self.lObjs=[]
            for iY in range(self.iYmax-self.iYstart):
                l=[]
                #align=vtDrawCanvasText.ALIGN_RIGHT
                #l.append(vtDrawCanvasText(text='',align_horizontal=align))
                
                for i in range(iCol):
                    sText=''
                    if i<iColInfo and i>=(self.iColNum):
                        align=vtDrawCanvasText.ALIGN_LEFT
                        try:
                            sText=self.lColLabel[i]
                        except:
                            pass
                    else:
                        def getColLabel(iCol):
                            iNum=iCol/26
                            if iNum>0:
                                sText=string.letters[iNum]+string.letters[iCol%26]
                            else:
                                sText=string.letters[iCol]
                            return sText
                        if i>=iColInfo:
                            sText=getColLabel(i)
                        else:
                            try:
                                sText=self.lColLabel[i]
                            except:
                                pass
                        align=vtDrawCanvasText.ALIGN_RIGHT
                    try:
                        w=self.lColWidth[i]
                    except:
                        w=-1
                        bCalcExtend=True
                    l.append(vtDrawCanvasText(text=sText,w=w,h=1,align_horizontal=align))
                self.lObjs.append(l)
            lExtend=[]
            for i in range(iCol):
                lExtend.append([0,0])
            
            self.iPages=len(self.pagesObjs)
            
            for i in range(self.iPages):
                self.__setPageObjs__(i)
                self.__calcPageExtend__(lExtend)
            for j in range(2):
                if j==0:
                    iH=self.iYmax-self.iYstart
                else:
                    iH=0
                    for l in self.lObjs:
                        if len(l[0].GetAttr(6)[1])>0:
                            iH=l[0].GetY()
                    iH-=self.iYstart
                self.raster[j].append(vtDrawCanvasLine(x=self.mX,y=self.mY+self.iYstart+1,w=self.iW,h=0))
                o=self.lObjs[0][1]
                iX=o.GetX()
                self.raster[j].append(vtDrawCanvasLine(x=self.mX+iX,y=self.mY+self.iYstart,w=0,h=iH))
                o=self.lObjs[0][self.iColInfo+self.iColNum-1]
                iX=o.GetX()+o.GetWidth()
                o=self.lObjs[0][self.iColInfo+self.iColNum]
                self.raster[j].append(vtDrawCanvasLine(x=iX,y=self.mY+self.iYstart,w=0,h=iH))
                for i in range(self.iCol/5):
                    o=self.lObjs[0][self.iColInfo+self.iColNum-1+(i+1)*5]
                    iX=o.GetX()+o.GetWidth()
                    self.raster[j].append(vtDrawCanvasLine(x=iX,y=self.mY+self.iYstart,w=0,h=iH))
        except:
            vtLog.vtLngTB('printout')
        #print lExtend
    def __setPageObjs__(self,page):
        lPageObjs=self.pagesObjs[page]
        iRow=1
        #print 'page len',len(lPageObjs)
        for lObjs in self.lObjs[1:]:
            for o in lObjs:
                o.SetAttr(6,'')
            
        for tup in lPageObjs:
            #print 'row',iRow
            lObjs=self.lObjs[iRow]
            iCol=0
            #for o in lObjs:
            #    o.SetAttr(6,str(iRow))
            #lObjs[0].SetAttr(6,str(iRow))
            #print tup
            i=0
            for objs in tup:
                if type(objs)==types.ListType:
                    for d in objs:
                        #print d,iCol,len(lObjs)
                        o=lObjs[iCol]
                        o.SetAttr(6,d)
                        #print o
                        iCol+=1
                    if i==0:
                        #print 'num',iCol,self.iColNum
                        for j in range(iCol,self.iColNum):
                            o=lObjs[iCol]
                            o.SetAttr(6,'')
                            iCol+=1
                    elif i==1:
                        #print 'info',iCol,self.iColNum+self.iColInfo
                        for j in range(iCol,self.iColNum+self.iColInfo):
                            o=lObjs[iCol]
                            o.SetAttr(6,'')
                            iCol+=1
                else:
                    o=lObjs[iCol]
                    o.SetAttr(6,objs)
                    #print o
                    iCol+=1
                i+=1
            #print lObjs
            iRow+=1
    def __calcPageExtend__(self,lExtend):
        dc = self.GetDC()
        
        for lObjs in self.lObjs:
            iCol=0
            for o in lObjs:
                iXd=o.GetWidth()
                iYd=o.GetHeight()
                if iXd<0 or iYd<0:
                    xa,ya,xe,ye=o.__getExtend__(self)
                    iXd=xe-xa
                    iYd=ye-ya
                    te = dc.GetTextExtent(o.text)
                    #print iCol,o,o.text,xa,ya,xe,ye,'d',iXd,iYd,te
                    iXd,iYd= map(int,self.calcPos(te[0],te[1],True))
                tup=lExtend[iCol]
                if iXd>tup[0]:
                    tup[0]=iXd
                if iYd>tup[1]:
                    tup[1]=iYd
                iCol+=1
        #print lExtend
        iY=self.mY+self.iYstart
        for lObjs in self.lObjs:
            iCol=0
            iX=self.mX
            for o in lObjs:
                o.SetX(iX)
                o.SetY(iY)
                o.SetWidth(lExtend[iCol][0])
                o.SetHeight(lExtend[iCol][1])
                #print iCol,lExtend[iCol]
                iX+=lExtend[iCol][0]+1
                iCol+=1
            iY+=lExtend[0][1]
    def __getPageObjs__(self,page):
        vtLog.vtLngCur(vtLog.DEBUG,'page:%d/%d (%d)'%(page,self.iPages,len(self.pagesObjs)),'printout')
        
        self.__setPageObjs__(page-1)
        if page==self.iPages:
            return self.raster[1]+self.lObjs
        return self.raster[0]+self.lObjs
