#----------------------------------------------------------------------------
# Name:         vtPrtDocument.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060311
# CVS-ID:       $Id: vtPrtDocument.py,v 1.1 2006/03/14 14:31:08 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import types

from vidarc.tool.time.vtTime import vtDateTime
from vidarc.tool.draw.vtDrawCanvasObjectBase import *
from vidarc.tool.draw.vtDrawCanvasRectangle import *
from vidarc.tool.draw.vtDrawCanvasText import *

import vidarc.tool.log.vtLog as vtLog

class vtPrtDocument(wx.Printout):
    DFT_COLOR=[
        (  0,   0,   0),
        (178, 107, 000),
        (178, 145, 000),
        ( 76, 152, 127),
        ( 79,  79,  59),
        (120,  78, 146),
        ( 31, 156, 127),
        (178, 100, 178)
        ]
    DFT_DIS_COLOR=[
        (  0,   0,   0),
        (167, 100, 000),
        (167, 136, 000),
        ( 67, 135, 112),
        (116, 130, 147),
        (100, 166, 100),
        (150, 114,  22),
        (147,  82, 147)
        ]
    OUTPUT_SINGLE_SIDE=0
    OUTPUT_DOUBLE_SIDE=1
    
    def __init__(self):
        wx.Printout.__init__(self)
        self.header=[]
        self.footer=[]
        self.margin=(3,3,1,1)
        self.grid=(20,20)
        self.objBase=vtDrawCanvasObjectBase()
        self.iDriver=self.objBase.DRV_WX
        self.__setDftColors__()
        self.output=self.OUTPUT_SINGLE_SIDE
        self.rectHdr=vtDrawCanvasRectangle()
        self.rectFtr=vtDrawCanvasRectangle()
    def SetDriver(self,i):
        if i in self.DRIVERS:
            self.iDriver=i
    def getDriver(self,iDriver=-1):
        if iDriver<0:
            return self.iDriver
        return iDriver
    def getDriverData(self,iDriver=-1):
        if iDriver<0:
            iDriver=self.iDriver
        return self.drvInfo[iDriver]
    def isLayerActive(self,layer):
        return True
    def setActLayer(self,layer,bActive,iDriver=-1):
        if iDriver<0:
            iDriver=self.iDriver
        try:
            self.drvInfo[iDriver]['act_layer']
        except:
            self.drvInfo[iDriver]['act_layer']=-1
        try:
            self.drvInfo[iDriver]['act_active']
        except:
            self.drvInfo[iDriver]['act_active']=-1
        if self.drvInfo[iDriver]['act_layer']!=layer or self.drvInfo[iDriver]['act_active']!=bActive:
            self.drvInfo[iDriver]['act_layer']=layer
            self.drvInfo[iDriver]['act_active']=bActive
            if iDriver==self.objBase.DRV_WX:
                dc=self.drvInfo[iDriver]['dc']
                if layer<0:
                    color=self.colorBkg
                    pen=self.penBkg
                    brush=self.brushBkg
                else:
                    if self.IsEnabled():
                        if bActive:
                            color=self.colorAct[layer%len(self.DFT_COLOR)]
                            pen=self.penAct[layer%len(self.DFT_COLOR)]
                            brush=self.brushAct[layer%len(self.DFT_COLOR)]
                            #dc.SetBrush(brush)
                        else:
                            color=self.color[layer%len(self.DFT_COLOR)]
                            pen=self.pen[layer%len(self.DFT_COLOR)]
                            brush=self.brush[layer%len(self.DFT_COLOR)]
                            #dc.SetBrush(brush)
                    else:
                        if bActive:
                            color=self.colorActDis[layer%len(self.DFT_DIS_COLOR)]
                            pen=self.penActDis[layer%len(self.DFT_DIS_COLOR)]
                            brush=self.brushActDis[layer%len(self.DFT_DIS_COLOR)]
                            #dc.SetBrush(brush)
                        else:
                            color=self.colorDis[layer%len(self.DFT_DIS_COLOR)]
                            pen=self.penDis[layer%len(self.DFT_DIS_COLOR)]
                            brush=self.brushDis[layer%len(self.DFT_DIS_COLOR)]
                        #dc.SetBrush(brush)
                #dc.SetPen(wx.Pen(color,1))
                dc.SetPen(pen)
                dc.SetBrush(brush)
                if bActive:
                    dc.SetFont(wx.Font(12, wx.SWISS, wx.NORMAL, wx.BOLD))
                else:
                    dc.SetFont(wx.Font(12, wx.SWISS, wx.NORMAL, wx.NORMAL))
                dc.SetTextForeground(color)
            elif iDriver==self.objBase.DRV_LATEX:
                d=self.drvInfo[iDriver]
                if layer<0:
                    d['color']='\\color[rgb]{%3.2f,%3.2f,%3.2f}'%(1.0 ,1.0 ,1.0)
                else:
                    if bActive:
                        c=self.colorAct[layer%len(self.DFT_COLOR)]
                        d['color']='\\color[rgb]{%3.2f,%3.2f,%3.2f}'%(float(c.Red()/255.0) ,float(c.Green()/255.0) ,float(c.Blue()/255.0))
                    else:
                        c=self.color[layer%len(self.DFT_COLOR)]
                        d['color']='\\color[rgb]{%3.2f,%3.2f,%3.2f}'%(float(c.Red()/255.0) ,float(c.Green()/255.0) ,float(c.Blue()/255.0))
    def __isWxDrv__(self):
        try:
            if self.iDriver==self.objBase.DRV_WX:
                return True
        except:
            pass
        return False
    def __setDftColors__(self):
        def convColor2Act(a):
            a=int(a*0.75)
            if a<0:
                a=0
            if a>255:
                a=255
            return a
        self.color=[]
        self.colorAct=[]
        if hasattr(self,'DFT_ACT_COLOR'):
            for c in self.DFT_ACT_COLOR:
                self.colorAct.append(wx.Colour(c[0], c[1], c[2]))
            for c in self.DFT_COLOR:
                self.color.append(wx.Colour(c[0], c[1], c[2]))
        else:
            for c in self.DFT_COLOR:
                self.colorAct.append(wx.Colour(c[0], c[1], c[2]))
            for c in self.DFT_COLOR:
                r,g,b=map(convColor2Act,c)
                self.color.append(wx.Colour(r, g, b))
            
        self.colorDis=[]
        self.colorActDis=[]
        if hasattr(self,'DFT_ACT_DIS_COLOR'):
            for c in self.DFT_ACT_DIS_COLOR:
                self.colorActDis.append(wx.Colour(c[0], c[1], c[2]))
        else:
            for c in self.DFT_DIS_COLOR:
                self.colorActDis.append(wx.Colour(c[0], c[1], c[2]))
        for c in self.DFT_DIS_COLOR:
            r,g,b=map(convColor2Act,c)
            self.colorDis.append(wx.Colour(r, g, b))
        
        self.__setDftBrush__()
        self.__setDftPen__()
    def __setDftBrush__(self):
        self.brush=[]
        self.brushAct=[]
        for c in self.color:
            self.brush.append(wx.Brush(c))
        for c in self.colorAct:
            self.brushAct.append(wx.Brush(c))
        self.brushDis=[]
        self.brushActDis=[]
        for c in self.colorDis:
            self.brushDis.append(wx.Brush(c))
        for c in self.colorActDis:
            self.brushActDis.append(wx.Brush(c))
    def __setDftPen__(self):
        self.pen=[]
        self.penAct=[]
        for c in self.color:
            self.pen.append(wx.Pen(c,1))
        for c in self.colorAct:
            self.penAct.append(wx.Pen(c,1))
        self.penDis=[]
        self.penActDis=[]
        for c in self.colorDis:
            self.penDis.append(wx.Pen(c,1))
        for c in self.colorActDis:
            self.penActDis.append(wx.Pen(c,1))
    def calcCoor(self,x,y):
        #return (self.mX+x)*self.grid[0],(self.mY+y)*self.grid[1]
        return x*self.grid[0],y*self.grid[1]
    def calcCoorLatex(self,x,y):
        return x*self.grid[0]*self.fScale,y*self.grid[1]*self.fScale
    def getLatexTextExtent(self,t):
        sFont=self.drvInfo[self.objBase.DRV_LATEX]['font_size']
        if sFont=='\\tiny':
            return (0.2*len(t),0.2)
        elif sFont=='\\scriptsize':
            return (0.2*len(t),0.2)
        elif sFont=='\\small':
            return (0.2*len(t),0.2)
        return (0.2*len(t),0.2)
    def calcPos(self,x,y,flag):
        if flag:
            x,y=round((x/self.grid[0])+0.5),round((y/self.grid[1])+0.5)
        else:
            x,y=round((x/self.grid[0])-0.5),round((y/self.grid[1])-0.5)
        if x<0:
            x=0
        if y<0:
            y=0
        return x,y
    def IsEnabled(self):
        return True
    def SetMargin(self,marginXleft,marginYtop,marginXright,marginYbottom):
        self.margin=(marginXleft,marginYtop,marginXright,marginYbottom)
    def __checkOutput__(self):
        i=max(len(self.header),len(self.footer))
        if i<=1:
            self.output=self.OUTPUT_SINGLE_SIDE
        else:
            self.output=self.OUTPUT_DOUBLE_SIDE

    def SetHeader(self,data):
        self.header=data
        
    def SetFooter(self,data):
        self.footer=data
        
    def OnBeginDocument(self, start, end):
        return self.base_OnBeginDocument(start, end)

    def OnEndDocument(self):
        self.base_OnEndDocument()

    def OnBeginPrinting(self):
        self.base_OnBeginPrinting()

    def OnEndPrinting(self):
        self.base_OnEndPrinting()

    def OnPreparePrinting(self):
        self.base_OnPreparePrinting()
        dc = self.GetDC()
        self.dt=vtDateTime(True)
        self.drvInfo={self.objBase.DRV_WX:{}}
        self.drvInfo[self.objBase.DRV_WX]['dc']=dc
        self.drvInfo[self.objBase.DRV_WX]['printing']=False
        
        (w, h) = dc.GetSizeTuple()
        if self.IsPreview()==False:
            fScalePrt=self.GetPPIPrinter()[0]/float(self.GetPPIScreen()[0])
            w/=fScalePrt
            h/=fScalePrt
            
        self.iWidthOrignal=w
        
        iW=int((w-((self.margin[0]+self.margin[1])*self.grid[0]))/float(self.grid[0]))
        iH=int((h-((self.margin[2]+self.margin[3])*self.grid[1]))/float(self.grid[1]))
        iW=int(w/float(self.grid[0]))
        iH=int(h/float(self.grid[1]))
        iW=iW-(self.margin[1]+1)
        self.iW=iW
        self.rectHdr.SetWidth(iW)
        self.rectFtr.SetY(iH-self.margin[3]-1-1)
        self.rectFtr.SetWidth(iW)
        self.rectFtr.SetHeight(1)
        
        self.drvInfo[self.objBase.DRV_WX]['dc']=dc
        self.drvInfo[self.objBase.DRV_WX]['act_layer']=-1
        
        if self.output==self.OUTPUT_SINGLE_SIDE:
            self.mX=self.margin[0]
            self.mY=self.margin[2]
        else:
            if (page%2)==0:
                self.mX=self.margin[1]
                self.mY=self.margin[2]
            else:
                self.mX=self.margin[0]
                self.mY=self.margin[2]
        self.rectHdr.SetX(self.mX)
        self.rectHdr.SetY(self.mY)
        self.rectFtr.SetX(self.mX)
                
        hdr=self.header
        iWUsed=0
        iFreeCount=0
        iHUsed=0
        for it in hdr:
            iWTmp=it.GetWidth()
            if iWTmp>=1:
                iWUsed+=iWTmp
            else:
                iFreeCount+=1
            iHTmp=it.GetHeight()
            if iHTmp>=1:
                if iHUsed<iHTmp:
                    iHUsed=iHTmp
        self.rectHdr.SetHeight(iHUsed)
        iY=iHUsed+2
        self.iYstart=iY
        if iFreeCount>0:
            iWFree=iW-iWUsed
            i=iWFree/iFreeCount
            j=0
            for it in hdr:
                it.SetX(j)
                iWTmp=it.GetWidth()
                if iWTmp<=0:
                    if iFreeCount>1:
                        it.SetWidth(i)
                        iFreeCount-=1
                        iWFree-=i
                    else:
                        it.SetWidth(iWFree)
                        #break
                iWTmp=it.GetWidth()
                j+=iWTmp
        for it in hdr:
            it.SetX(it.GetX()+self.mX)
            it.SetY(it.GetY()+self.mY)
        
        hdr=self.footer
        iWUsed=0
        iFreeCount=0
        iHUsed=0
        for it in hdr:
            iWTmp=it.GetWidth()
            if iWTmp>=1:
                iWUsed+=iWTmp
            else:
                iFreeCount+=1
            iHTmp=it.GetHeight()
            if iHTmp>=1:
                if iHUsed<iHTmp:
                    iHUsed=iHTmp
        self.rectFtr.SetHeight(iHUsed)
        self.iYmax=iH-self.iYstart-2
        iY=self.iYmax+1
        if iFreeCount>0:
            iWFree=iW-iWUsed
            i=iWFree/iFreeCount
            j=0
            for it in hdr:
                it.SetX(j)
                iWTmp=it.GetWidth()
                if iWTmp<=0:
                    if iFreeCount>1:
                        it.SetWidth(i)
                        iFreeCount-=1
                        iWFree-=i
                    else:
                        it.SetWidth(iWFree)
                        #break
                iWTmp=it.GetWidth()
                j+=iWTmp
        for it in hdr:
            it.SetX(it.GetX()+self.mX)
            it.SetY(iY+self.mY)
        
        self.__calcData__()
        
        self.lPage=[]
        for it in self.header:
            self.__updateSpecial__(it)
        for it in self.footer:
            self.__updateSpecial__(it)
    
    def __calcData__(self):
        self.iPages=1
        
        
    def HasPage(self, page):
        if page <= self.iPages:
            return True
        else:
            return False

    def GetPageInfo(self):
        #vtLog.CallStack('')
        #print self.iPages
        return (1, self.iPages, 1, self.iPages)
    
    def __getPageObjs__(self,page):
        return []
        
    def OnPrintPage(self, page):
        self.iActPage=page
        dc = self.GetDC()
        
        self.drvInfo[self.objBase.DRV_WX]['dc']=dc
        self.drvInfo[self.objBase.DRV_WX]['act_layer']=-1
        
        marginX = 50
        marginY = 50
        
        # Get the size of the DC in pixels
        (w, h) = dc.GetSizeTuple()
        if self.IsPreview()==False:
            fScalePrt=self.GetPPIPrinter()[0]/float(self.GetPPIScreen()[0])
            w/=fScalePrt
            h/=fScalePrt
        #vtLog.CallStack('')
        #print 'sizetuple',dc.GetSizeTuple(),w,self.iWidthOrignal
        #print 'pagesizeMM',self.GetPageSizeMM()
        #print 'pagesizePx',self.GetPageSizePixels()
        #print 'printerPPI',self.GetPPIPrinter()
        #print 'screenPPI',self.GetPPIScreen()
        #fScalePrt=self.GetPPIPrinter()[0]/float(self.GetPPIScreen()[0])
        fScale=w/float(self.iWidthOrignal)
        if self.IsPreview()==False:
            fScale*=fScalePrt
        dc.SetUserScale(fScale, fScale)
        #print fScale
        
        for o in self.lPage:
            o.SetAttr(6,str(page))
        self.rectHdr.__draw__(self)
        self.rectFtr.__draw__(self)
        for it in self.header:
            it.__draw__(self)
        for it in self.footer:
            it.__draw__(self)
        
        objs=self.__getPageObjs__(page)
        for o in objs:
            if type(o)==types.ListType:
                for oo in o:
                    oo.__draw__(self)
            else:
                o.__draw__(self)
        #dc.DrawText("Page: %d" % page, marginX/2, maxY-marginY)
        
        return True
    def __updateSpecial__(self,o):
        try:
            if isinstance(o,vtDrawCanvasText):
                tup=o.GetAttr(6)
                s=tup[1]
                if s=='%date':
                    s=self.dt.GetDateStr()
                    o.SetAttr(6,s)
                elif s=='%datetime':
                    s=self.dt.GetDateTimeLocalStr(' ')
                    o.SetAttr(6,s)
                elif s=='%pages':
                    o.SetAttr(6,str(self.iPages))
                elif s=='%page':
                    self.lPage.append(o)
        except:
            vtLog.vtLngTB('printout')
            pass
                    
