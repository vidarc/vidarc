#----------------------------------------------------------------------------
# Name:         vtSlotKeyBuffer.py
# Purpose:
# Author:       Walter Obweger
#
# Created:      20061119
# CVS-ID:       $Id: vtSlotKeyBuffer.py,v 1.7 2009/05/09 19:59:07 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import threading
from vtThreadCore import getRLock

class vtSlotKeyBuffer:
    def __init__(self,size):
        self.iSize=size
        self.iFill=0
        self.lSlot=[None]*self.iSize
        self.lKey=[None]*self.iSize
        self.semAccess=getRLock()#threading.Semaphore()
        self.semSpace=threading.Semaphore(self.iSize)
    def isFill(self):
        try:
            self.semAccess.acquire()
            if self.iFill>0:
                return True
            return False
        finally:
            self.semAccess.release()
    def isFree(self):
        try:
            self.semAccess.acquire()
            if self.iFill<self.iSize:
                return True
            else:
                return False
        finally:
            self.semAccess.release()
    def put(self,k,t,blocking=False):
        if self.semSpace.acquire(blocking)==False:
            return -1
        try:
            self.semAccess.acquire()
            if self.__find__(k)>=0:
                self.semSpace.release()
                return -2
            if self.iFill<self.iSize:
                iAct=0
                while self.lSlot[iAct] is not None:
                    iAct+=1
                    if iAct>=self.iSize:
                        return -1
                self.lSlot[iAct]=t
                self.lKey[iAct]=k
                self.iFill+=1
                return iAct
            else:
                return -1
        except:
            self.semAccess.release()
    def pop(self,iPos):
        self.semAccess.acquire()
        if self.iFill>0:
            t=self.lSlot[iPos]
            self.lSlot[iPos]=None
            self.lKey[iPos]=None
            self.iFill-=1
            self.semAccess.release()
            self.semSpace.release()
            return t
        else:
            self.semAccess.release()
            return None
    def __find__(self,k):
        if self.iFill>0:
            for iAct in xrange(self.iSize):
                if self.lKey[iAct]==k:
                    return iAct
            return -2
        else:
            return -1
    def find(self,k):
        try:
            self.semAccess.acquire()
            return self.__find__(k)
        finally:
            self.semAccess.release()
    def get(self,iAct):
        self.semAccess.acquire()
        try:
            t=self.lSlot[iAct]
        except:
            t=None
        self.semAccess.release()
        return t
    def getKey(self,iAct):
        self.semAccess.acquire()
        try:
            k=self.lKey[iAct]
        except:
            k=None
        self.semAccess.release()
        return k
    def proc(self,func,*args,**kwargs):
        try:
            #self.semAccess.acquire()
            for i in xrange(0,self.iSize):
                func(self.lKey[i],self.lSlot[i],*args,**kwargs)
        finally:
            #self.semAccess.release()
            pass
