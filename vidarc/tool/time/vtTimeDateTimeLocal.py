#----------------------------------------------------------------------
# Name:         vtTimeDateTimeLocal.py
# Purpose:      text control with tree popup window
#
# Author:       Walter Obweger
#
# Created:      20060210
# CVS-ID:       $Id: vtTimeDateTimeLocal.py,v 1.11 2008/03/26 23:12:09 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import cStringIO
import string

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomConsumer import *
from vidarc.tool.xml.vtXmlDomConsumerLang import *
from vidarc.tool.time.vtTimeInputDate import vtTimeInputDate
from vidarc.tool.time.vtTime import vtDateTime
    
VERBOSE=0

#----------------------------------------------------------------------
def getCancelData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00oIDAT8\x8d\xad\x93\xc1\x11\xc0 \x08\x04\x0f\xd3E\xfa\xaf\xcd6\xcc+\
\x19\xd1;p\x86\xf0fw\x80S\xb3v\xa1R\xadD\xff*\xe8\x86\xd1\r#\x03\xd6\xbeOp\
\x0f\xd8\xdb\x10\xc1s\xaf\x13d\x12\x06o\x02%Q0\x00\x98\x8aq\x9d\x82\xc1t\x02\
\x06(8\x14\xb0\x15\x8e\x05\xf3\xceY:\x9b\x80\x1d,\x928Atm%q/Q\xc1\x91D\xc6xZ\
\xe5\xcf\xf4\x00\xe0\xc8:\xc8\xd18`E\x00\x00\x00\x00IEND\xaeB`\x82' 

def getCancelBitmap():
    return wx.BitmapFromImage(getCancelImage())

def getCancelImage():
    stream = cStringIO.StringIO(getCancelData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getApplyData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00pIDAT8\x8dcddbf\xa0\x040\x91\xa3Ix\x89\xe0\x7f\xe1%\x82\xff\x19\x18\
\x18\x18XH\xd5\x08c\xbf\x8dy\xcfH\x92\x010\xcd0\x8d0@\x94\x17pi&\xda\x00\\\
\x9a\x892\x00\xd9\xdfd\x19@\x08\xe05\x00\x9f\xdfQ\x0c@\x8eW\x8a\\@\x8eAL0\'\
\xa2;\x93\x18\xe7c\xb8\x00\xa6\x98\x14W`\x04"\xb2\x8d\x84l\xc7j\x00\xa9\x80\
\x91\xd2\xec\x0c\x00y\x1c/\xbdxe+\x9e\x00\x00\x00\x00IEND\xaeB`\x82' 

def getApplyBitmap():
    return wx.BitmapFromImage(getApplyImage())

def getApplyImage():
    stream = cStringIO.StringIO(getApplyData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getDownData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x83IDAT8\x8d\xb5\x92\xd1\r\x84 \x10D\xdf\xaa5l\x05\xd4y\xa2\xdfj\
\x85TA\x03\xde\x17fUH\xee\x02\xcc\x17\t\xcc\xcc\xdb\r"\xc3H\x8d\x86*70\xa5\
\xc3\xba\xf83\x84\xf0\x93IU\xd9\xf6C\x00\xc4\x8e\xb0.\xfe\x04(\x05\xa9*\xc0e\
\xbe\x11X9\xe7\xb0A\xc9\x98\x93<\x97\x98(\x92b\x8c\xb7{\xdb\x0e\r\x96\xf8\n\
\xf8\xcc^r\x0fs\xed}\x08J\x14\xb9\xf6~\x04M\x02\xec\x18%\xfc&\x04\xaf\x8f\
\xf4\xaf\xaa\t\xbe\x1el!\xed\xde\xc2%\x86\x00\x00\x00\x00IEND\xaeB`\x82' 

def getDownBitmap():
    return wx.BitmapFromImage(getDownImage())

def getDownImage():
    stream = cStringIO.StringIO(getDownData())
    return wx.ImageFromStream(stream)

# defined event for vtInputTextML item changed
wxEVT_VTTIME_DATETIMELOCAL_CHANGED=wx.NewEventType()
vEVT_VTTIME_DATETIMELOCAL_CHANGED=wx.PyEventBinder(wxEVT_VTTIME_DATETIMELOCAL_CHANGED,1)
def EVT_VTTIME_DATETIMELOCAL_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTTIME_DATETIMELOCAL_CHANGED,func)
class vtTimeDateTimeLocalChanged(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTTIME_DATETIMELOCAL_CHANGED(<widget_name>, self.OnChanged)
    """

    def __init__(self,obj,val):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTTIME_DATETIMELOCAL_CHANGED)
        self.val=val
    def GetValue(self):
        return self.val
    
[wxID_WXTIMEEDIT, wxID_WXTIMEEDITwin, wxID_WXTIMEEDITTGHR00, 
 wxID_WXTIMEEDITTGHR01, wxID_WXTIMEEDITTGHR02, 
 wxID_WXTIMEEDITTGHR03, wxID_WXTIMEEDITTGHR04, 
 wxID_WXTIMEEDITTGHR05, wxID_WXTIMEEDITTGHR06, 
 wxID_WXTIMEEDITTGHR07, wxID_WXTIMEEDITTGHR08, 
 wxID_WXTIMEEDITTGHR09, wxID_WXTIMEEDITTGHR10, 
 wxID_WXTIMEEDITTGHR11, wxID_WXTIMEEDITTGHR12, 
 wxID_WXTIMEEDITTGHR13, wxID_WXTIMEEDITTGHR14, 
 wxID_WXTIMEEDITTGHR15, wxID_WXTIMEEDITTGHR16, 
 wxID_WXTIMEEDITTGHR17, wxID_WXTIMEEDITTGHR18, 
 wxID_WXTIMEEDITTGHR19, wxID_WXTIMEEDITTGHR20, 
 wxID_WXTIMEEDITTGHR21, wxID_WXTIMEEDITTGHR22, 
 wxID_WXTIMEEDITTGHR23, wxID_WXTIMEEDITTGMIN00, 
 wxID_WXTIMEEDITTGMIN05, wxID_WXTIMEEDITTGMIN10, 
 wxID_WXTIMEEDITTGMIN15, wxID_WXTIMEEDITTGMIN20, 
 wxID_WXTIMEEDITTGMIN25, wxID_WXTIMEEDITTGMIN30, 
 wxID_WXTIMEEDITTGMIN35, wxID_WXTIMEEDITTGMIN40, 
 wxID_WXTIMEEDITTGMIN45, wxID_WXTIMEEDITTGMIN50, 
 wxID_WXTIMEEDITTGMIN55, wxID_WXTIMEEDITGCBBCANCEL, 
 wxID_WXTIMEEDITGCBBDEL, 
] = map(lambda _init_ctrls: wx.NewId(), range(40))


class vtTimeDateTimeLocalTransientPopup(wx.Dialog):
    #SIZE_PN_SCROLL=30
    def __init__(self, parent, size,style,name=''):
        self.SIZE_PN_SCROLL=wx.SystemSettings.GetMetric(wx.SYS_VSCROLL_X)
        wx.Dialog.__init__(self, parent,style=wx.RESIZE_BORDER)#wx.BORDER_SIMPLE)#|wx.STAY_ON_TOP)
        id=wx.NewId()
        wx.EVT_SIZE(self,self.OnSize)
        st = wx.StaticText(self, -1,name,pos=(70,4))
        self.cbCancel = wx.BitmapButton(id=-1,
              bitmap=getCancelBitmap(), name=u'cbCancel',
              parent=self, pos=(0,0), size=(30,30), style=wx.BU_AUTODRAW)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              self.cbCancel)
              
        self.cbApply = wx.BitmapButton(id=-1,
              bitmap=getApplyBitmap(), name=u'cbApply',
              parent=self, pos=wx.Point(32, 00), size=wx.Size(30, 30),
              style=wx.BU_AUTODRAW)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              self.cbApply)
        if size[1]<40:
            size=(size[0],40)
        i=0
        iY=40
        iH=size[1]+4
        iSzHeight=0
        sVal=parent.GetValue()
        #self.dt=wx.DateTime.Now()
        #self.dt.ParseFormat(sVal,'%H%M')
        dt=wx.DateTime.Today()
        dt.ParseFormat(sVal,'%Y-%m-%d')
        
        self.calCalendar = vtTimeInputDate(
              id=-1, name=u'calCalendar',draw_grid=False,
              parent=self, pos=wx.Point(4, 40),size=wx.Size(192, 192))
        iDx=200
        self.tgHr00 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR00, label=u'0',
              name=u'tgHr00', parent=self, pos=wx.Point(iDx+0, 40),
              size=wx.Size(30, 20), style=0)
        self.tgHr00.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr00, wxID_WXTIMEEDITTGHR00,
              self.OnTgHrTogglebutton)

        self.tgHr01 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR01, label=u'1',
              name=u'tgHr01', parent=self, pos=(iDx+30, 40),
              size=(30, 20), style=0)
        self.tgHr01.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr01, wxID_WXTIMEEDITTGHR01,
              self.OnTgHrTogglebutton)

        self.tgHr02 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR02, label=u'2',
              name=u'tgHr02', parent=self, pos=(iDx+60, 40),
              size=(30, 20), style=0)
        self.tgHr02.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr02, wxID_WXTIMEEDITTGHR02,
              self.OnTgHrTogglebutton)

        self.tgHr03 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR03, label=u'3',
              name=u'tgHr03', parent=self, pos=(iDx+90, 40),
              size=(30, 20), style=0)
        self.tgHr03.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr03, wxID_WXTIMEEDITTGHR03,
              self.OnTgHrTogglebutton)

        self.tgHr04 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR04, label=u'4',
              name=u'tgHr04', parent=self, pos=(iDx+120, 40),
              size=(30, 20), style=0)
        self.tgHr04.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr04, wxID_WXTIMEEDITTGHR04,
              self.OnTgHrTogglebutton)

        self.tgHr05 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR05, label=u'5',
              name=u'tgHr05', parent=self, pos=(iDx+150, 40),
              size=(30, 20), style=0)
        self.tgHr05.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr05, wxID_WXTIMEEDITTGHR05,
              self.OnTgHrTogglebutton)

        self.tgHr06 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR06, label=u'6',
              name=u'tgHr06', parent=self, pos=(iDx+180, 40),
              size=(30, 20), style=0)
        self.tgHr06.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr06, wxID_WXTIMEEDITTGHR06,
              self.OnTgHrTogglebutton)

        self.tgHr07 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR07, label=u'7',
              name=u'tgHr07', parent=self, pos=(iDx+210, 40),
              size=(30, 20), style=0)
        self.tgHr07.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr07, wxID_WXTIMEEDITTGHR07,
              self.OnTgHrTogglebutton)

        self.tgHr08 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR08, label=u'8',
              name=u'tgHr08', parent=self, pos=(iDx+240, 40),
              size=(30, 20), style=0)
        self.tgHr08.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr08, wxID_WXTIMEEDITTGHR08,
              self.OnTgHrTogglebutton)

        self.tgHr09 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR09, label=u'9',
              name=u'tgHr09', parent=self, pos=(iDx+270, 40),
              size=(30, 20), style=0)
        self.tgHr09.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr09, wxID_WXTIMEEDITTGHR09,
              self.OnTgHrTogglebutton)

        self.tgHr10 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR10, label=u'10',
              name=u'tgHr10', parent=self, pos=(iDx+300, 40),
              size=(30, 20), style=0)
        self.tgHr10.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr10, wxID_WXTIMEEDITTGHR10,
              self.OnTgHrTogglebutton)

        self.tgHr11 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR11, label=u'11',
              name=u'tgHr11', parent=self, pos=(iDx+330, 40),
              size=(30, 20), style=0)
        self.tgHr11.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr11, wxID_WXTIMEEDITTGHR11,
              self.OnTgHrTogglebutton)

        self.tgHr12 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR12, label=u'12',
              name=u'tgHr12', parent=self, pos=(iDx+0, 60),
              size=(30, 20), style=0)
        self.tgHr12.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr12, wxID_WXTIMEEDITTGHR12,
              self.OnTgHrTogglebutton)

        self.tgHr13 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR13, label=u'13',
              name=u'tgHr13', parent=self, pos=(iDx+30, 60),
              size=(30, 20), style=0)
        self.tgHr13.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr13, wxID_WXTIMEEDITTGHR13,
              self.OnTgHrTogglebutton)

        self.tgHr14 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR14, label=u'14',
              name=u'tgHr14', parent=self, pos=(iDx+60, 60),
              size=(30, 20), style=0)
        self.tgHr14.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr14, wxID_WXTIMEEDITTGHR14,
              self.OnTgHrTogglebutton)

        self.tgHr15 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR15, label=u'15',
              name=u'tgHr15', parent=self, pos=(iDx+90, 60),
              size=(30, 20), style=0)
        self.tgHr15.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr15, wxID_WXTIMEEDITTGHR15,
              self.OnTgHrTogglebutton)

        self.tgHr16 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR16, label=u'16',
              name=u'tgHr16', parent=self, pos=(iDx+120, 60),
              size=(30, 20), style=0)
        self.tgHr16.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr16, wxID_WXTIMEEDITTGHR16,
              self.OnTgHrTogglebutton)

        self.tgHr17 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR17, label=u'17',
              name=u'tgHr17', parent=self, pos=(iDx+150, 60),
              size=(30, 20), style=0)
        self.tgHr17.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr17, wxID_WXTIMEEDITTGHR17,
              self.OnTgHrTogglebutton)

        self.tgHr18 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR18, label=u'18',
              name=u'tgHr18', parent=self, pos=(iDx+180, 60),
              size=(30, 20), style=0)
        self.tgHr18.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr18, wxID_WXTIMEEDITTGHR18,
              self.OnTgHrTogglebutton)

        self.tgHr19 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR19, label=u'19',
              name=u'tgHr19', parent=self, pos=(iDx+210, 60),
              size=(30, 20), style=0)
        self.tgHr19.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr19, wxID_WXTIMEEDITTGHR19,
              self.OnTgHrTogglebutton)

        self.tgHr20 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR20, label=u'20',
              name=u'tgHr20', parent=self, pos=(iDx+240, 60),
              size=(30, 20), style=0)
        self.tgHr20.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr20, wxID_WXTIMEEDITTGHR20,
              self.OnTgHrTogglebutton)

        self.tgHr21 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR21, label=u'21',
              name=u'tgHr21', parent=self, pos=(iDx+270, 60),
              size=(30, 20), style=0)
        self.tgHr21.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr21, wxID_WXTIMEEDITTGHR21,
              self.OnTgHrTogglebutton)

        self.tgHr22 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR22, label=u'22',
              name=u'tgHr22', parent=self, pos=(iDx+300, 60),
              size=(30, 20), style=0)
        self.tgHr22.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr22, wxID_WXTIMEEDITTGHR22,
              self.OnTgHrTogglebutton)

        self.tgHr23 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR23, label=u'23',
              name=u'tgHr23', parent=self, pos=(iDx+330, 60),
              size=(30, 20), style=0)
        self.tgHr23.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr23, wxID_WXTIMEEDITTGHR23,
              self.OnTgHrTogglebutton)

        self.tgMin00 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN00,
              label=u':00', name=u'tgMin00', parent=self, pos=(iDx+0,
              80), size=(60, 20), style=0)
        self.tgMin00.SetValue(False)
        self.tgMin00.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin00, wxID_WXTIMEEDITTGMIN00,
              self.OnTgMinTogglebutton)

        self.tgMin05 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN05,
              label=u':05', name=u'tgMin05', parent=self, pos=(iDx+60,
              80), size=(60, 20), style=0)
        self.tgMin05.SetValue(False)
        self.tgMin05.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin05, wxID_WXTIMEEDITTGMIN05,
              self.OnTgMinTogglebutton)

        self.tgMin10 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN10,
              label=u':10', name=u'tgMin10', parent=self,
              pos=(iDx+120, 80), size=(60, 20), style=0)
        self.tgMin10.SetValue(False)
        self.tgMin10.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin10, wxID_WXTIMEEDITTGMIN10,
              self.OnTgMinTogglebutton)

        self.tgMin15 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN15,
              label=u':15', name=u'tgMin15', parent=self,
              pos=(iDx+180, 80), size=(60, 20), style=0)
        self.tgMin15.SetValue(False)
        self.tgMin15.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin15, wxID_WXTIMEEDITTGMIN15,
              self.OnTgMinTogglebutton)

        self.tgMin20 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN20,
              label=u':20', name=u'tgMin20', parent=self,
              pos=(iDx+240, 80), size=(60, 20), style=0)
        self.tgMin20.SetValue(False)
        self.tgMin20.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin20, wxID_WXTIMEEDITTGMIN20,
              self.OnTgMinTogglebutton)

        self.tgMin25 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN25,
              label=u':25', name=u'tgMin25', parent=self,
              pos=(iDx+300, 80), size=(60, 20), style=0)
        self.tgMin25.SetValue(False)
        self.tgMin25.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin25, wxID_WXTIMEEDITTGMIN25,
              self.OnTgMinTogglebutton)

        self.tgMin30 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN30,
              label=u':30', name=u'tgMin30', parent=self, pos=(iDx+0,
              100), size=(60, 20), style=0)
        self.tgMin30.SetValue(False)
        self.tgMin30.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin30, wxID_WXTIMEEDITTGMIN30,
              self.OnTgMinTogglebutton)

        self.tgMin35 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN35,
              label=u':35', name=u'tgMin35', parent=self, pos=(iDx+60,
              100), size=(60, 20), style=0)
        self.tgMin35.SetValue(False)
        self.tgMin35.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin35, wxID_WXTIMEEDITTGMIN35,
              self.OnTgMinTogglebutton)

        self.tgMin40 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN40,
              label=u':40', name=u'tgMin40', parent=self,
              pos=(iDx+120, 100), size=(60, 20), style=0)
        self.tgMin40.SetValue(False)
        self.tgMin40.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin40, wxID_WXTIMEEDITTGMIN40,
              self.OnTgMinTogglebutton)

        self.tgMin45 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN45,
              label=u':45', name=u'tgMin45', parent=self,
              pos=(iDx+180, 100), size=(60, 20), style=0)
        self.tgMin45.SetValue(False)
        self.tgMin45.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin45, wxID_WXTIMEEDITTGMIN45,
              self.OnTgMinTogglebutton)

        self.tgMin50 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN50,
              label=u':50', name=u'tgMin50', parent=self,
              pos=(iDx+240, 100), size=(60, 20), style=0)
        self.tgMin50.SetValue(False)
        self.tgMin50.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin50, wxID_WXTIMEEDITTGMIN50,
              self.OnTgMinTogglebutton)

        self.tgMin55 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN55,
              label=u':55', name=u'tgMin55', parent=self,
              pos=(iDx+300, 100), size=(60, 20), style=0)
        self.tgMin55.SetValue(False)
        self.tgMin55.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin55, wxID_WXTIMEEDITTGMIN55,
              self.OnTgMinTogglebutton)

        
        self.tgHr24 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN55,
              label=u'24:00', name=u'tgHr24', parent=self,
              pos=(iDx+300, 120), size=(60, 20), style=0)
        self.tgHr24.SetValue(False)
        self.tgHr24.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgHr24, wxID_WXTIMEEDITTGMIN55,
              self.OnTgHr24Togglebutton)

        #self.calCalendar.Bind(wx.calendar.EVT_CALENDAR_SEL_CHANGED,
        #      self.OnCalCalendarCalendarSelChanged,
        #      id=wxID_VCALENDARPANELCALCALENDAR)
        self.tgActHr=None
        self.tgActMin=None
        self.tgPrevHr=None
        self.tgPrevMin=None
        self.objTimeCtrl=None

        self.szOrig=(iDx+370,244)
        self.SetSize(self.szOrig)
        
    def OnSize(self,evt):
        iW,iH=self.GetSize()
        if iW<self.szOrig[0]:
            iW=self.szOrig[0]
        #if iH<self.szMin[1]:
        #    iH=self.szMin[1]
        if iH<self.szOrig[1]:
            iH=self.szOrig[1]
        self.SetSize((iW,iH))
        #if self.calCalendar is not None:
        #    self.calCalendar.SetSize((iW-16,iH-52))
        evt.Skip()
    def OnCbCancelButton(self,evt):
        self.Show(False)
    def OnCbApplyButton(self,evt):
        #self.Apply()
        try:
            par=self.GetParent()
            dt=self.GetValue()
            sVal=dt.GetDateTimeStr(' ')[:16]
            par.__apply__(sVal)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        self.Show(False)
    def OnTgHrTogglebutton(self, event):
        obj=event.GetEventObject()
        if obj.GetValue()==False:
            obj.SetValue(True)
        else:
            if self.tgHr24.GetValue()==True:
                self.tgHr24.SetValue(False)
            if self.tgActHr!=None:
                self.tgActHr.SetValue(False)
            self.tgActHr=obj

    def OnTgMinTogglebutton(self, event):
        obj=event.GetEventObject()
        if obj.GetValue()==False:
            obj.SetValue(True)
        else:
            if self.tgHr24.GetValue()==True:
                obj.SetValue(False)
                return
            if self.tgActMin!=None:
                self.tgActMin.SetValue(False)
            self.tgActMin=obj
        self.tgPrevHr=self.tgActHr
        self.tgPrevMin=self.tgActMin
        #dt=self.GetValue()
        #s=self.dt.FormatTime()
        #s=self.dt.Format('%H:%M')
        #self.SetValue(s)
        #wx.PostEvent(self,vTimeTimeEditChanged(self,s))
        #self.PopDown()
    def OnTgHr24Togglebutton(self,event):
        obj=event.GetEventObject()
        if obj.GetValue()==False:
            obj.SetValue(True)
        else:
            if self.tgActHr!=None:
                self.tgActHr.SetValue(False)
            self.tgActHr=None
            if self.tgActMin!=None:
                self.tgActMin.SetValue(False)
            self.tgActMin=None
        self.tgPrevHr=self.tgActHr
        self.tgPrevMin=self.tgActMin
        #dt=self.GetValue()
        #s=self.dt.FormatTime()
        s='24:00:00'
        #self.SetValue(s)
        #wx.PostEvent(self,vTimeTimeEditChanged(self,s))
        #self.PopDown()
    def Show(self,flag):
        try:
            par=self.GetParent()
            if flag:
                sVal=par.GetValue()
                self.SetVal(sVal)
                #self.calCalendar.Refresh()
                #self.calCalendar.Update()
            else:
                #par.cbPopup.SetValue(False)
                par.__setPopupState__(False)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        wx.Dialog.Show(self,flag)
    def GetValue(self):
        min=0
        hr=0
        if self.tgActMin is not None:
            if self.tgActMin == self.tgMin00:
                min=0
            elif self.tgActMin==self.tgMin05:
                min=5
            elif self.tgActMin == self.tgMin10:
                min=10
            elif self.tgActMin == self.tgMin15:
                min=15
            elif self.tgActMin == self.tgMin20:
                min=20
            elif self.tgActMin == self.tgMin25:
                min=25
            elif self.tgActMin == self.tgMin30:
                min=30
            elif self.tgActMin == self.tgMin35:
                min=35
            elif self.tgActMin == self.tgMin40:
                min=40
            elif self.tgActMin == self.tgMin45:
                min=45
            elif self.tgActMin == self.tgMin50:
                min=50
            elif self.tgActMin == self.tgMin55:
                min=55
        if self.tgActHr is not None:
            if self.tgActHr==self.tgHr00:
                hr=0
            elif self.tgActHr==self.tgHr01:
                hr=1
            elif self.tgActHr==self.tgHr02:
                hr=2
            elif self.tgActHr==self.tgHr03:
                hr=3
            elif self.tgActHr==self.tgHr04:
                hr=4
            elif self.tgActHr==self.tgHr05:
                hr=5
            elif self.tgActHr==self.tgHr06:
                hr=6
            elif self.tgActHr==self.tgHr07:
                hr=7
            elif self.tgActHr==self.tgHr08:
                hr=8
            elif self.tgActHr==self.tgHr09:
                hr=9
            elif self.tgActHr==self.tgHr10:
                hr=10
            elif self.tgActHr==self.tgHr11:
                hr=11
            elif self.tgActHr==self.tgHr12:
                hr=12
            elif self.tgActHr==self.tgHr13:
                hr=13
            elif self.tgActHr==self.tgHr14:
                hr=14
            elif self.tgActHr==self.tgHr15:
                hr=15
            elif self.tgActHr==self.tgHr16:
                hr=16
            elif self.tgActHr==self.tgHr17:
                hr=17
            elif self.tgActHr==self.tgHr18:
                hr=18
            elif self.tgActHr==self.tgHr19:
                hr=19
            elif self.tgActHr==self.tgHr20:
                hr=20
            elif self.tgActHr==self.tgHr21:
                hr=21
            elif self.tgActHr==self.tgHr22:
                hr=22
            elif self.tgActHr==self.tgHr23:
                hr=23
        if self.tgHr24.GetValue()==True:
            hr=0
            min=0
        self.dt=self.calCalendar.GetDate()
        self.dt.SetHour(hr)
        self.dt.SetMinute(min)
        
        #self.dt=wx.DateTime()
        #self.dt.Set(10)
        if self.tgHr24.GetValue()==True:
            self.dt.SetDay(self.dt.GetDay()+1)
        #    self.dt.SetSecond(59)
        return self.dt
    def __setTime(self,hr,min):
        try:
            #vtLog.CallStack('hr:%02d mn:%02d'%(hr,min))
            if self.tgActMin is not None:
                self.tgActMin.SetValue(False)
            if self.tgActHr is not None:
                self.tgActHr.SetValue(False)
            self.tgActHr=None
            self.tgActMin=None
            self.tgHr24.SetValue(False)
            if hr==24 and min==0:
                self.tgHr24.SetValue(True)
                self.tgActMin=None
                self.tgActHr=None
                self.tgPrevHr=self.tgActHr
                self.tgPrevMin=self.tgActMin
                #self.SetValue('24:00:00')
                return
            min=round(min/5.0)*5
            if min==0:
                self.tgActMin=self.tgMin00
            elif min==5:
                self.tgActMin=self.tgMin05
            elif min==10:
                self.tgActMin=self.tgMin10
            elif min==15:
                self.tgActMin=self.tgMin15
            elif min==20:
                self.tgActMin=self.tgMin20
            elif min==25:
                self.tgActMin=self.tgMin25
            elif min==30:
                self.tgActMin=self.tgMin30
            elif min==35:
                self.tgActMin=self.tgMin35
            elif min==40:
                self.tgActMin=self.tgMin40
            elif min==45:
                self.tgActMin=self.tgMin45
            elif min==50:
                self.tgActMin=self.tgMin50
            elif min==55:
                self.tgActMin=self.tgMin55
            if hr==0:
                self.tgActHr=self.tgHr00
            elif hr==1:
                self.tgActHr=self.tgHr01
            elif hr==2:
                self.tgActHr=self.tgHr02
            elif hr==3:
                self.tgActHr=self.tgHr03
            elif hr==4:
                self.tgActHr=self.tgHr04
            elif hr==5:
                self.tgActHr=self.tgHr05
            elif hr==6:
                self.tgActHr=self.tgHr06
            elif hr==7:
                self.tgActHr=self.tgHr07
            elif hr==8:
                self.tgActHr=self.tgHr08
            elif hr==9:
                self.tgActHr=self.tgHr09
            elif hr==10:
                self.tgActHr=self.tgHr10
            elif hr==11:
                self.tgActHr=self.tgHr11
            elif hr==12:
                self.tgActHr=self.tgHr12
            elif hr==13:
                self.tgActHr=self.tgHr13
            elif hr==14:
                self.tgActHr=self.tgHr14
            elif hr==15:
                self.tgActHr=self.tgHr15
            elif hr==16:
                self.tgActHr=self.tgHr16
            elif hr==17:
                self.tgActHr=self.tgHr17
            elif hr==18:
                self.tgActHr=self.tgHr18
            elif hr==19:
                self.tgActHr=self.tgHr19
            elif hr==20:
                self.tgActHr=self.tgHr20
            elif hr==21:
                self.tgActHr=self.tgHr21
            elif hr==22:
                self.tgActHr=self.tgHr22
            elif hr==23:
                self.tgActHr=self.tgHr23
            self.tgActMin.SetValue(True)
            self.tgActHr.SetValue(True)
            self.tgPrevHr=self.tgActHr
            self.tgPrevMin=self.tgActMin
            #dt=self.GetValue()
            #s=self.dt.FormatTime()
            #s=self.dt.Format('%H:%M')
            #self.SetValue(s)
        
        except:
            vtLog.vtLngTB(self.GetName())
    def GetValF(self):
        try:
            dt=self.calCalendar.GetDate()
            sVal=dt.Format('%Y-%m-%d')
            return sVal
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
            return ''
    def SetVal(self,sVal):
        try:
            par=self.GetParent()
            
            dtPar=par.__getDateTime__()
            self.calCalendar.SetValueStr(sVal[:10])
            self.__setTime(dtPar.GetHour(),dtPar.GetMinute())
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
class vtTimeDateTimeLocal(wx.Panel,vtXmlDomConsumer,vtXmlDomConsumerLang):
    def __init__(self,*_args,**_kwargs):
        vtXmlDomConsumer.__init__(self)
        vtXmlDomConsumerLang.__init__(self)
        try:
            sz=_kwargs['size']
        except:
            sz=wx.Size(140,30)
            _kwargs['size']=sz
        try:
            szCb=_kwargs['size_button']
            del _kwargs['size_button']
        except:
            szCb=wx.Size(24,24)
        try:
            szTxt=_kwargs['size_text']
            del _kwargs['size_text']
        except:
            szTxt=wx.Size(sz[0]-szCb[0]-2,sz[1]-4)
        
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        self.SetAutoLayout(True)
        
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        size=(szTxt[0],szTxt[1])
        pos=(0,(sz[1]-szTxt[1])/2)
        self.txtVal = wx.TextCtrl(id=-1, name='txtVal',
              parent=self, pos=pos, size=size,
              style=0, value='')
        self.txtVal.SetMaxLength(16)
        self.txtVal.SetToolTipString('YYYY-MM-DD HH:MM')
        #self.txtVal.SetConstraints(LayoutAnchors(self.txtVal, True, True,
        #      True, False))
        bxs.AddWindow(self.txtVal, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        self.txtVal.Bind(wx.EVT_TEXT, self.OnTextText,self.txtVal)
        
        size=(szCb[0],szCb[1])
        pos=(szTxt[0]+(sz[0]-(szTxt[0]+szCb[0]))/2,(sz[1]-szCb[1])/2)
        id=wx.NewId()
        self.cbPopup = wx.lib.buttons.GenBitmapToggleButton(ID=id,
              bitmap=wx.EmptyBitmap(8, 8), name=u'cbPopup',
              parent=self, pos=pos, size=size, style=wx.BU_AUTODRAW)
        #self.cbPopup.SetConstraints(LayoutAnchors(self.cbPopup, False, True,
        #      True, False))
        bxs.AddWindow(self.cbPopup, 0, border=0, flag=wx.EXPAND)
        self.SetSizer(bxs)
        self.cbPopup.SetBitmapLabel(getDownBitmap())
        self.cbPopup.SetBitmapSelected(getDownBitmap())
        self.cbPopup.Bind(wx.EVT_BUTTON,self.OnPopupButton,self.cbPopup)#id=id)
        self.popWin=None
        self.tagName='datetime'
        self.bBlock=True
        self.dt=vtDateTime(True)
        #self.dftBkgColor=self.txtVal.GetBackgroundColour()
        self.bEnableMark=True
        self.bBusy=False
    def Enable(self,flag):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        wx.Panel.Enable(self,flag)
        self.cbPopup.Refresh()
    def SetEnableMark(self,flag):
        self.bEnableMark=flag
    def __markModified__(self,flag=True):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.bEnableMark==False:
            return
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'__markModified__;flag:%s'%(flag),
        #                origin=self.GetName())
        if flag:
            f=self.txtVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_BOLD)
            self.txtVal.SetFont(f)
        else:
            f=self.txtVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
            self.txtVal.SetFont(f)
        self.txtVal.Refresh()
    def __markFlt__(self,flag=False):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if flag:
            color=wx.TheColourDatabase.Find('YELLOW')
            self.txtVal.SetBackgroundColour(color)
        else:
            color=wx.SystemSettings_GetColour(wx.SYS_COLOUR_WINDOW)
            self.txtVal.SetBackgroundColour(color)
    def SetTagNames(self,tagName):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'SetTagNames;tagname:%s,%s'%(tagName,tagNameInt),
        #                origin=self.GetName())
        self.tagName=tagName
    def ClearLang(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'ClearLang',
        #                origin=self.GetName())
        #if VERBOSE:
        #    vtLog.CallStack('')
        self.txtVal.SetValue('')
    def UpdateLang(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'UpdateLang',
        #                origin=self.GetName())
        return
        if VERBOSE:
            vtLog.CallStack('')
        try:
            if self.popWin is None:
                sVal=self.doc.getNodeText(self.node,self.tagName)
            else:
                sVal=self.popWin.GetVal(self.doc.GetLang())
            self.bBlock=True
            self.txtVal.SetValue(string.split(sVal,'\n')[0])
            wx.CallAfter(self.__clearBlock__)
            #self.txtVal.SetValue(sVal)
        except:
            vtLog.vtLngTB(self.GetName())

    def Clear(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'Clear',
        #                origin=self.GetName())
        #if VERBOSE:
        #    vtLog.CallStack('')
        self.ClearLang()
        self.node=None
        self.__markModified__(False)
    def __setPopupState__(self,flag):
        self.bBusy=flag
        self.cbPopup.SetValue(flag)
    def IsBusy(self):
        return self.bBusy
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'IsBusy',
        #                origin=self.GetName())
        if self.popWin is not None:
            return self.popWin.IsShown()
        else:
            return False
    def __Close__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.popWin is not None:
            self.popWin.Show(False)
            self.__setPopupState__(False)
    def Stop(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'Stop',self)
        wx.CallAfter(self.__Close__)
    def SetDoc(self,doc):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'SetDoc',
        #                origin=self.GetName())
        vtXmlDomConsumer.SetDoc(self,doc)
        vtXmlDomConsumerLang.SetDoc(self,doc)
    def SetNode(self,node):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'SetNode;node:%s'%(node),
        #                origin=self.GetName())
        if VERBOSE:
            vtLog.CallStack('')
            print node
        self.Clear()
        self.node=node
        if self.doc is None:
            return
        if self.node is None:
            return
        try:
            sVal=string.split(self.doc.getNodeText(self.node,self.tagName),'\n')[0]
            #sVal=sVal[:4]
            bOk,bMod,sVal=self.__validate__(sVal,True)
            self.bBlock=True
            self.__markModified__(bMod)
            self.__markFlt__(not bOk)
            self.txtVal.SetValue(sVal)
            wx.CallAfter(self.__clearBlock__)
        except:
            pass
            #vtLog.vtLngTB(self.GetName())
        #self.__getSelNode__()
        self.UpdateLang()
    def __validate__(self,sVal,bSet2NowOnFlt=False,bTxt=False):
        #if VERBOSE:
        #    vtLog.CallStack(sVal)
        bMod=False
        bOk=False
        try:
            if bTxt:
                ret=self.dt.SetStr(sVal)
                #iLen=5
            else:
                ret=self.dt.SetStr(sVal)
                #iLen=4
            #if iLen!=len(sVal):
            #    ret=-1
                #sVal=sVal[:iLen]
                #bMod=True
            #if ret!=iLen:
            #    if bSet2NowOnFlt:
            #        bMod=True
            #else:
            #    bOk=True
            bOk=True
        except:
            if len(sVal)>0:
                vtLog.vtLngTB(self.GetName())
                vtLog.vtLngCurWX(vtLog.WARN,sVal,self)
            if bSet2NowOnFlt:
                bMod=True
        if bMod:
            if bSet2NowOnFlt:
                self.dt.Now()
        sVal=self.dt.GetDateTimeStr(' ')[:16]
        return bOk,bMod,sVal
    def __getDateTime__(self):
        return self.dt
    def __clearBlock__(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'__clearBlock__',
        #                origin=self.GetName())
        self.bBlock=False
    def GetNode(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'GetNode',
        #                origin=self.GetName())
        #if VERBOSE:
        #    vtLog.CallStack('')
        try:
            sVal=self.dt.GetDateTimeStr()
            self.doc.setNodeText(self.node,self.tagName,sVal)
            #self.__setNode__()

            self.__markModified__(False)
            self.doc.AlignNode(self.node)
        except:
            #vtLog.vtLngTB(self.GetName())
            pass
            
    def __getDoc__(self):
        return self.doc
    def __getNode__(self):
        return self.node
    def __apply__(self,sVal):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'__apply__;val:%s'%(sVal),
        #                origin=self.GetName())
        #if VERBOSE:
        #    vtLog.CallStack(sVal)
        try:
            #sVal=self.popWin.GetVal()
            #if self.trSetNode is not None:
            #    sVal=self.trSetNode(self.docTreeTup[0],node)
            #else:
            #    sVal=''
            bOk,bMod,sVal=self.__validate__(sVal)
            self.__markFlt__(not bOk)
            if bMod or self.txtVal.GetValue()!=sVal:
                if self.bBlock==False:
                    self.__markModified__(True)
                    wx.PostEvent(self,vtTimeDateTimeLocalChanged(self,sVal))
        except:
            sVal=_(u'fault')
            vtLog.vtLngTB(self.GetName())
        self.bBlock=True
        self.txtVal.SetValue(string.split(sVal,'\n')[0])
        wx.CallAfter(self.__clearBlock__)
        return sVal
        
    def __createPopup__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCS(vtLog.DEBUG,'__createPopup__',
                        origin=self.GetName())
        if self.popWin is None:
            sz=self.GetSize()
            sz=(sz[0],sz[1]-20)
            try:
                self.popWin=vtTimeDateTimeLocalTransientPopup(self,sz,wx.SIMPLE_BORDER)
            except:
                vtLog.vtLngTB(self.GetName())
            #self.popWin.SetVal(self.txtVal.GetValue(),self.doc.GetLang())
        else:
            pass
    def SetValue(self,sVal):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        try:
            bOk,bMod,sVal=self.__validate__(sVal,True)
            self.bBlock=True
            self.__markModified__(bMod)
            self.__markFlt__(not bOk)
            self.txtVal.SetValue(sVal)
            wx.CallAfter(self.__clearBlock__)
        except:
            pass
            #vtLog.vtLngTB(self.GetName())
        #self.__getSelNode__()
        self.UpdateLang()
    def GetValue(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'GetValue:%s'%self.txtVal.GetValue(),
        #                origin=self.GetName())
        return self.txtVal.GetValue()
    def OnTextText(self,evt):
        if self.bBlock:
            return
        sVal=self.txtVal.GetValue()
        try:
            bOk,bMod,val=self.__validate__(sVal,False,bTxt=True)
            self.__markFlt__(not bOk)
            if bMod:
                self.txtVal.SetValue(val)
        except:
            vtLog.vtLngTB(self.GetName())
        self.__markModified__()
        wx.PostEvent(self,vtTimeDateTimeLocalChanged(self,sVal))
    def OnPopupButton(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.__createPopup__()
        if self.cbPopup.GetValue()==True:
            btn=self
            iX,iY = btn.ClientToScreen( (0,0) )
            iW,iH =  btn.GetSize()
            iPopW,iPopH=self.popWin.GetSize()
            iScreenW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            iScreenH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            if iX+iPopW>iScreenW:
                iX=iScreenW-iPopW-4
                if iX<0:
                    iX=0
            iY+=iH
            if iY+iPopH>iScreenH:
                iY=iScreenH-iPopH
                if iY<0:
                    iY=0
            
            self.popWin.Move((iX,iY))
            #self.popWin.Move((pos[0],pos[1]+sz[1]))
            self.bBusy=True
            self.popWin.Show(True)
        else:
            self.popWin.Show(False)
            self.bBusy=False
        evt.Skip()

