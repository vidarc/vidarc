#----------------------------------------------------------------------------
# Name:         vtTime.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051217
# CVS-ID:       $Id: vtTime.py,v 1.16 2010/02/27 03:09:39 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import string,time,re
import datetime
import calendar
import traceback

# A UTC class.

class UTC(datetime.tzinfo):
    """UTC"""

    def utcoffset(self, dt):
        return ZERO

    def tzname(self, dt):
        return "UTC"

    def dst(self, dt):
        return ZERO

utc = UTC()

# A class capturing the platform's idea of local time.
ZERO = datetime.timedelta(0)
HOUR = datetime.timedelta(hours=1)


import time as _time

STDOFFSET = datetime.timedelta(seconds = -_time.timezone)
if _time.daylight:
    DSTOFFSET = datetime.timedelta(seconds = -_time.altzone)
else:
    DSTOFFSET = STDOFFSET

DSTDIFF = DSTOFFSET - STDOFFSET

class LocalTimezone(datetime.tzinfo):

    def utcoffset(self, dt):
        if self._isdst(dt):
            return DSTOFFSET
        else:
            return STDOFFSET

    def dst(self, dt):
        if self._isdst(dt):
            return DSTDIFF
        else:
            return ZERO

    def tzname(self, dt):
        return _time.tzname[self._isdst(dt)]

    def _isdst(self, dt):
        tt = (dt.year, dt.month, dt.day,
              dt.hour, dt.minute, dt.second,
              dt.weekday(), 0, -1)
        stamp = _time.mktime(tt)
        tt = _time.localtime(stamp)
        return tt.tm_isdst > 0
Local = LocalTimezone()

#from wx import DateTime as wxDateTime

class vtDateTimeParseException(Exception):
    def __init__(self,val):
        self.val=val
    def __str__(self):
        return self.val

class vtDateTime:
    def __init__(self,bLocal=False):
        #self.dt=wxDateTime.Now()
        #self.dt=time.time()
        self.bValid=True
        if bLocal:
            self.tz=Local
        else:
            self.tz=utc
        self.dt=datetime.datetime(2006,02,10,tzinfo=self.tz)
        self.Now()
    def Now(self):
        #print '  ',self.dt.resolution
        self.bValid=True
        self.dt=self.dt.now(self.tz)
        #print self.dt.utcnow()
    def Clear(self):
        self.bValid=False
    def SetValid(self,flag):
        self.bValid=flag
    def IsValid(self):
        return self.bValid
    def __getInvalid__(self):
        return '____-__-__ --:--:--'
    def Convert2GM(self):
        if self.bValid:
            self.dt=self.dt.astimezone(utc)
    def Convert2Local(self):
        if self.bValid:
            self.dt=self.dt.astimezone(Local)
    def GetGM(self):
        if self.bValid:
            return self.dt.astimezone(utc)
        else:
            return self.__getInvalid__()
    def GetLocal(self):
        if self.bValid:
            return self.dt.astimezone(Local)
        else:
            return self.__getInvalid__()
    def __str__(self):
        if self.bValid:
            return self.dt.isoformat()
        else:
            return self.__getInvalid__()
    def __repr__(self):
        if self.bValid:
            return self.dt.isoformat()
        else:
            return self.__getInvalid__()
    def SetStr(self,s):
        try:
            self.SetDateStr(s[:10])
            self.SetTimeStr(s[11:])
            self.bValid=True
        except:
            self.bValid=False
            raise vtDateTimeParseException(s)
    def SetDateStr(self,s):
        try:
            strs=string.split(s,'-')
            nums=map(int,strs)
            self.dt=self.dt.replace(year=nums[0],month=nums[1],day=nums[2])
            self.bValid=True
        except:
            self.bValid=False
            raise vtDateTimeParseException(s)
    def SetDateSmallStr(self,s):
        try:
            strs=[s[:4],s[4:6],s[6:8]]
            nums=map(int,strs)
            self.dt=self.dt.replace(year=nums[0],month=nums[1],day=nums[2])
            self.bValid=True
        except:
            self.bValid=False
            raise vtDateTimeParseException(s)
    def SetTimeStr(self,s):
        try:
            #strs=string.split(s,':')
            #m=re.match(r"(\d+)\:(\d+)\:?(\d?)\.?(\d?)",s)
            #m=re.match(r"(\d{2}):(\d{2})?:?(\d{2})?\.?(\d{6})?[-+]?(\d{2})?:?(\d{2})",s)
            m=re.match(r"(\d{2})\:(\d{2})?:?(\d{2})?\.?(\d{6})?([-+]+\d{2})?:?(\d{2})?",s)
            iLen=len(m.groups())
            if iLen==0:
                raise vtDateTimeParseException(s)
            l=[]
            for i in m.groups():
                if i is None:
                    i=0
                l.append(int(i))
            #groups=map(int,m.groups())
            kwargs={}
            ofs=self.dt.tzinfo.utcoffset(self.dt)
            if m.groups()[4] is not None and m.groups()[4] is not None:
                delta=datetime.timedelta(hours=l[4],minutes=l[5])
            else:
                delta=ofs
            for i in range(iLen):
                if i==0:
                    kwargs['hour']=l[i]
                elif i==1:
                    kwargs['minute']=l[i]
                elif i==2:
                    kwargs['second']=l[i]
                elif i==3:
                    kwargs['microsecond']=l[i]
            self.dt=self.dt.replace(**kwargs)+ofs-delta
            l=m.groups()
            if (l[0] is None) or (l[1] is None):
                raise vtDateTimeParseException(s)
            #self.dt
            self.bValid=True
        except:
            self.bValid=False
            raise vtDateTimeParseException(s)
        #return self.dt.isoformat()[11:-6]#self.dt.strftime('%H:%M:%S.%u')
    def SetTimeSmallStr(self,s):
        try:
            strs=[s[:2],s[2:4],s[4:6]]
            nums=map(int,strs)
            self.dt=self.dt.replace(hour=nums[0],minute=nums[1],second=nums[2])
            self.bValid=True
        except:
            self.bValid=False
            raise vtDateTimeParseException(s)
    def SetTimeTzStr(self,s):
        try:
            self.bValid=True
            return self.dt.isoformat()[11:]#self.dt.strftime('%H:%M:%S%z')
        except:
            self.bValid=False
            raise vtDateTimeParseException(s)
    def GetDateTimeStr(self,sep='T'):
        if self.bValid:
            return self.dt.isoformat(sep)
        else:
            return '____-__-__%s--:--:--'%sep
    def GetDateTimeGMStr(self,sep='T'):
        if self.bValid:
            dt=self.GetGM()
            return dt.isoformat(sep)
        else:
            return '____-__-__%s--:--:--'%sep
    def GetDateTimeLocalStr(self,sep='T'):
        if self.bValid:
            dt=self.GetLocal()
            return dt.isoformat(sep)
        else:
            return '____-__-__%s--:--:--'%sep
    def GetDateStr(self):
        if self.bValid:
            return self.dt.strftime('%Y-%m-%d')
        else:
            return '____-__-__'
    def GetDateSmallStr(self):
        if self.bValid:
            return self.dt.strftime('%Y%m%d')
        else:
            return '________'
    def GetTimeStr(self):
        if self.bValid:
            return self.dt.isoformat()[11:-6]#self.dt.strftime('%H:%M:%S.%u')
        else:
            return '--:--:--'
    def GetTimeSmallStr(self):
        if self.bValid:
            return self.dt.strftime('%H%M%S')
        else:
            return '--:--:--'
    def GetTimeTzStr(self):
        if self.bValid:
            return self.dt.isoformat()[11:]#self.dt.strftime('%H:%M:%S%z')
        else:
            return '--:--:--'
    
    def SetYear(self,iYear):
        self.dt=self.dt.replace(year=iYear)
    def SetMonth(self,iMon):
        self.dt=self.dt.replace(month=iMon)
    def SetDay(self,iDay):
        self.dt=self.dt.replace(day=iDay)
    def SetHour(self,iHr):
        self.dt=self.dt.replace(hour=iHr)
    def SetMinute(self,iMn):
        if iMn>=60:
            self.SetMinute(iMn-60)
            self.SetHour(self.GetHour()+1)
        else:
            self.dt=self.dt.replace(minute=iMn)
    def SetSecond(self,iSc):
        self.dt=self.dt.replace(second=iSc)
    def SetDate(self,iYear,iMon,iDay):
        self.dt=self.dt.replace(year=iYear,month=iMon,day=iDay)
    def GetCalendarISO(self):
        return self.dt.isocalendar()
    def GetUtcTimeTuple(self):
        return self.dt.utctimetuple()
    def GetYear(self):
        if self.bValid:
            return self.dt.year
        else:
            return -1
    def GetMonth(self):
        if self.bValid:
            return self.dt.month
        else:
            return -1
    def GetDay(self):
        if self.bValid:
            return self.dt.day
        else:
            return -1
    def GetDayName(self):
        return calendar.day_abbr[self.GetCalendarISO()[2]-1]
    def GetWeek(self):
        return self.GetCalendarISO()[1]
    def GetHour(self):
        if self.bValid:
            return self.dt.hour
        else:
            return -1
    def GetMinute(self):
        if self.bValid:
            return self.dt.minute
        else:
            return -1
    def GetSecond(self):
        if self.bValid:
            return self.dt.second
        else:
            return -1
    def GetMicroSecond(self):
        if self.bValid:
            return self.dt.microsecond
        else:
            return -1
    def CalcDiffSec(self,zEnd):
        if not self.IsValid() or not zEnd.IsValid():
            #zDiff.Clear()
            return 0
        else:
            zDelta=zEnd.dt-self.dt
            return zDelta.days*86400+zDelta.seconds
    def CalcDiffSecFloat(self,zEnd):
        if not self.IsValid() or not zEnd.IsValid():
            #zDiff.Clear()
            return 0
        else:
            zDelta=zEnd.dt-self.dt
            return float(zDelta.days*86400+zDelta.seconds)+float(zDelta.microseconds)/1.0e6
    def CalcDiffStr(self,zEnd):
        if not self.IsValid() or not zEnd.IsValid():
            return ''
        else:
            zDelta=zEnd.dt-self.dt
            l=[]
            bAdd=False
            if zDelta.days>0:
                l.append('%d '%zDelta.days)
                bAdd=True
            sec=zDelta.seconds
            i=sec/3600
            if i>0:
                l.append('%02d:'%i)
                sec-=i*3600
                bAdd=True
            i=sec/60
            if i>0:
                l.append('%02d:'%i)
                sec-=i*60
                bAdd=True
            if bAdd==True:
                s='%02d.'%sec
            else:
                s='%d.'%sec
            l.append(s)
            s='%06d'%zDelta.microseconds
            l.append(s[:3])
            return u''.join(l)
    def GetWxDateTime(self):
        return self.dt

class vtDateTimeStartEnd(vtDateTime):
    def __init__(self,bLocal=False):
        self.zStart=vtTime()
        self.zEnd=vtTime()
        self.zDiff=vtTime()
        vtDateTime.__init__(self,bLocal)
    def Now(self):
        vtDateTime.Now(self)
        self.zStart.Now()
        self.zEnd.Now()
    def SetTimeStr(self,s):
        try:
            vtDateTime.SetTimeStr(self,s)
            self.zStart.Set(self.GetHour(),self.GetMinute(),self.GetSecond())
        except:
            self.zStart.Clear()
            raise vtDateTimeParseException(s)
    def SetEndTimeStr(self,sVal,sep=''):
        self.zEnd.SetStr(sVal,sep)
    def GetDateTimeStartEndStr(self):
        return self.GetDateTimeStr()+' '+self.zEnd.GetStr(':')
    def SetStrOld(self,s,sep=''):
        try:
            strs=s.split(' ')
            print strs
            if len(strs[0])>12:
                sDt=strs[0]
                sTm=[1]
            else:
                sDt=strs[0]+' '+strs[1]
                sTm=strs[2]
            if len(sTm)==5:
                sTm=sTm+sep+'00'
            print sDt,sTm
            vtDateTime.SetStr(self,sDt)
            self.zEnd.SetStr(sTm,sep)
        except:
            raise vtDateTimeParseException(s)
    def SetEnd(self,hour,min=0,sec=0):
        self.zEnd.Set(hour,min,sec)
    def SetHourEnd(self,hour):
        self.zEnd.SetHour(hour)
    def SetMinuteEnd(self,min):
        self.zEnd.SetMinute(min)
    def SetSecondEnd(self,sec):
        self.zEnd.SetSecond(sec)
    def GetHourStart(self):
        return self.zStart.GetHour()
    def GetMinuteStart(self):
        return self.zStart.GetMinute()
    def GetSecondStart(self):
        return self.zStart.GetSecond()
    def GetHourEnd(self):
        return self.zEnd.GetHour()
    def GetMinuteEnd(self):
        return self.zEnd.GetMinute()
    def GetSecondEnd(self):
        return self.zEnd.GetSecond()
    def GetHourDiff(self):
        return self.zDiff.GetHour()
    def GetMinuteDiff(self):
        return self.zDiff.GetMinute()
    def GetSecondDiff(self):
        return self.zDiff.GetSecond()
    def CalcDiff(self):
        if not self.zStart.IsValid() or not self.zEnd.IsValid():
            self.zDiff.Clear()
        else:
            self.zStart.Set(self.GetHour(),self.GetMinute(),self.GetSecond())
            self.zStart.CalcDiff(self.zEnd,self.zDiff)
    def GetTimeStartStr(self,sep):
        self.CalcDiff()
        return self.zStart.GetStr(sep)
    def GetTimeEndStr(self,sep):
        return self.zEnd.GetStr(sep)
    def GetTimeDiffStr(self,sep):
        self.CalcDiff()
        return self.zDiff.GetStr(sep)
    def SetStr(self,sVal,sepDt='-',sepTm=':'):
        strs=sVal.split(' ')
        try:
            if sepDt=='':
                s=strs[0]
                sDt=s[0:4]+'-'+s[4:6]+'-'+s[6:8]
            else:
                sDt=strs[0]
            vtDateTime.SetDateStr(self,sDt)
        except:
            self.bValid=False
            raise vtDateTimeParseException(sVal)
        iReq=6+len(sepTm)*2
        sTmEmpty='00%s00%s00'%(sepTm,sepTm)
        if len(strs[1])<iReq:
            sTm=strs[1]+sTmEmpty[len(strs[1])-iReq:]
        else:
            sTm=strs[1]
        if sepTm=='':
            sTm=sTm[0:2]+':'+sTm[2:4]+':'+sTm[4:6]
        self.SetTimeStr(sTm)
        if len(strs[2])<iReq:
            sTm=strs[2]+sTmEmpty[len(strs[2])-iReq:]
        else:
            sTm=strs[2]
        if sepTm=='':
            sTm=sTm[0:2]+':'+sTm[2:4]+':'+sTm[4:6]
        if self.zEnd.SetStr(sTm,':')<0:
            raise vtDateTimeParseException(sVal)
        self.CalcDiff()
    def GetDateTimeStartEndStr(self,sepDt='-',sepTm=':'):
        sDt='%04d%s%02d%s%02d'%(self.GetYear(),sepDt,self.GetMonth(),sepDt,self.GetDay())
        return sDt+' '+self.GetTimeStartStr(sepTm)+' '+self.GetTimeEndStr(sepTm)+' ('+self.GetTimeDiffStr(sepTm)+')'
    def GetDateTimeStartEndShortStr(self,sepDt='-',sepTm=':'):
        sDt='%04d%s%02d%s%02d'%(self.GetYear(),sepDt,self.GetMonth(),sepDt,self.GetDay())
        return sDt+' '+self.GetTimeStartStr(sepTm)[:5]+' '+self.GetTimeEndStr(sepTm)[:5]+' ('+self.GetTimeDiffStr(sepTm)[:5]+')'
    def GetDateTimeStartEndStoreStr(self,sepDt='-',sepTm=':'):
        if self.zStart.IsValid():
            return self.GetDateTimeStr(' ')+' '+self.zEnd.GetStr(':')
        else:
            sDt='%04d%s%02d%s%02d'%(self.GetYear(),sepDt,self.GetMonth(),sepDt,self.GetDay())
            return sDt+' '+self.GetTimeStartStr(sepTm)+' '+self.GetTimeEndStr(sepTm)

class vtTime:
    def __init__(self):
        self.iHr=0
        self.iMin=0
        self.iSec=0
    def Now(self):
        z=time.time()
        tm=time.localtime(z)
        self.iHr,self.iMin,self.iSec=tm[3:6]
    def Clear(self):
        self.iHr,self.iMin,self.iSec=-1,-1,-1
    def GetHour(self):
        return self.iHr
    def GetMinute(self):
        return self.iMin
    def GetSecond(self):
        return self.iSec
    def GetFloatHour(self):
        fVal=float(self.GetHour())+float(self.GetMinute())/60.0
        return fVal
    def GetFloatHourFull(self):
        fVal=float(self.GetHour())+float(self.GetMinute())/60.0+float(self.GetSecond())/3600.0
        return fVal
    def SetHour(self,val):
        self.iHr=val
    def SetMinute(self,val):
        self.iMin=val
    def SetSecond(self,val):
        self.iSec=val
    def Set24hr(self):
        self.iHr,self.iMin,self.iSec=24,00,00
    def Set(self,hr=0,min=0,sec=0):
        self.iHr,self.iMin,self.iSec=hr,min,sec
    def IsValid(self):
        if self.iHr==-1 or self.iMin==-1 or self.iSec==-1:
            return False
        return True
    def Ceil(self,toleranceMin=5):
        if self.iHr==-1 or self.iMin==-1 or self.iSec==-1:
            return
        if toleranceMin>0:
            self.iMin=round((self.iMin/float(toleranceMin))+0.5)*int(toleranceMin)
            self.iSec=0
        #elif toleranceSec>0:
        #    self.iSec=round((self.iSec/float(toleranceSec))+0.5)*int(toleranceSec)
        if self.iMin>59:
            self.iHr+=1
            self.iMin=0
            if self.iHr>24:
                self.iHr=0
    def Floor(self,toleranceMin=5):
        if self.iHr==-1 or self.iMin==-1 or self.iSec==-1:
            return
        if toleranceMin>0:
            self.iMin=round((self.iMin/float(toleranceMin))-0.5)*int(toleranceMin)
            self.iSec=0
        #elif toleranceSec>0:
        #    self.iSec=round((self.iSec/float(toleranceSec))-0.5)*int(toleranceSec)
    def Round(self,toleranceMin=5):
        if self.iHr==-1 or self.iMin==-1 or self.iSec==-1:
            return
        if toleranceMin>0:
            iMn=self.iMin%toleranceMin
            fSec=(float(self.iMin%toleranceMin)+self.iSec/60.0)/float(toleranceMin)
            self.iMin=(self.iMin/toleranceMin+round(fSec))*int(toleranceMin)
            self.iSec=0
            if self.iMin>59:
                self.iHr+=1
                self.iMin=0
                if self.iHr>24:
                    self.iHr=0
        #elif toleranceSec>0:
        #    self.iSec=round((self.iSec/float(toleranceSec)))*int(toleranceSec)
    def SetStr(self,s,sep=''):
        try:
            if sep=='':
                if s=='------':
                    self.Clear()
                    return 0
                self.iHr=int(s[0:2])
                self.iMin=int(s[2:4])
                self.iSec=int(s[4:6])
            else:
                if '%s%s%s%s%s'%('--',sep,'--',sep,'--')==s:
                    self.Clear()
                    return 0
                strs=s.split(sep)
                self.iHr,self.iMin,self.iSec=map(int,strs)
            if self.iHr<0 or self.iMin<0 or self.iSec<0:
                self.Clear()
                return -1
            return 0
        except:
            self.Clear()
            return -1
    def GetStr(self,sep=''):
        if self.iHr==-1 or self.iMin==-1 or self.iSec==-1:
            return '%s%s%s%s%s'%('--',sep,'--',sep,'--')
        else:
            return '%02d%s%02d%s%02d'%(self.iHr,sep,self.iMin,sep,self.iSec)
    def GetDiffStr(self,zEnd,sep=''):
        if not self.IsValid() or not zEnd.IsValid():
            return '%s%s%s%s%s'%('--',sep,'--',sep,'--')
        try:
            iHr=zEnd.GetHour()-self.GetHour()
            iMn=zEnd.GetMinute()-self.GetMinute()
            iSc=zEnd.GetSecond()-self.GetSecond()
            if iSc<0:
                iSc+=60
                iMn-=1
            if iMn<0:
                iMn+=60
                iHr-=1
            return '%02d%s%02d%s%02d'%(iHr,sep,iMn,sep,iSc)
        except:
            return '%s%s%s%s%s'%('--',sep,'--',sep,'--')
    def CalcDiff(self,zEnd,zDiff):
        try:
            zDiff.Clear()
            iHr=zEnd.GetHour()-self.GetHour()
            iMn=zEnd.GetMinute()-self.GetMinute()
            iSc=zEnd.GetSecond()-self.GetSecond()
            if iSc<0:
                iSc+=60
                iMn-=1
            if iMn<0:
                iMn+=60
                iHr-=1
            zDiff.Set(iHr,iMn,iSc)
        except:
            pass

class vtTimeDiff(vtTime):
    def __init__(self):
        self.Clear()
    def Clear(self):
        self.iHr=0
        self.iMin=0
        self.iSec=0
        self.iDy=0
        self.iMl=0
    def SetDiffSec(self,fSec):
        self.Clear()
        i=int(fSec/86400)
        if i>0:
            self.iDy=i
            fSec-=i*86400
        i=int(fSec/3600)
        if i>0:
            self.iHr=i
            fSec-=i*3600
        i=int(fSec/60)
        if i>0:
            self.iMin=i
            fSec-=i*60
        self.iSec=int(fSec)
        self.iMl=(fSec-self.iSec)*1e6
    def GetStr(self):
        l=[]
        bAdd=False
        if self.iDy>0:
            l.append('%d '%self.iDy)
            bAdd=True
        if self.iHr>0:
            l.append('%02d:'%self.iHr)
            bAdd=True
        if self.iMin>0:
            l.append('%02d:'%self.iMin)
            bAdd=True
        if bAdd==True:
            s='%02d.'%self.iSec
        else:
            s='%d.'%self.iSec
        l.append(s)
        s='%06d'%self.iMl
        l.append(s[:3])
        return u''.join(l)

if __name__=='__main__':
    import traceback
    print '++++++++++++++++++++ gm'
    o=vtDateTime()
    print o
    print o.GetDateStr()
    print o.GetTimeStr()
    print o.GetTimeTzStr()
    print '--------------------'
    print '++++++++++++++++++++ local'
    o=vtDateTime(True)
    print o
    print o.GetDateStr()
    print o.GetTimeStr()
    print o.GetTimeTzStr()
    print '--------------------'
    print '++++++++++++++++++++ local parsing'
    o=vtDateTime(True)
    o.SetDateStr('2006-03-29')
    print o
    print o.GetDateStr()
    print o.GetTimeStr()
    print o.GetTimeTzStr()
    for val in ['13:12:14.012345','13:12:15','13:13',
                '13:12:14.012345+00:00','13:12:14.012345+02:00',
                '13:12:14.012345-02:00','01:12:14.012345-02:00',
                '13:12:14.012345+02:00','01:12:14.012345+03:00']: 
        try:
            print 'parse   ',val
            o.SetTimeStr(val)
            print '   local',o.GetTimeStr()
            print '   local',o.GetTimeTzStr()
            print
        except:
            traceback.print_exc()
    
    print o.dt.astimezone(utc)
    print o.GetGM()
    print o.GetLocal()
    print o.GetDateTimeStr()
    print o.GetDateTimeGMStr()
    print o.GetDateTimeLocalStr()
    #print wxDateTime.GetTm()
    print '--------------------'
    print '++++++++++++++++++++ local'
    o=vtDateTime(True)
    print o.GetDateTimeStr()
    print o.GetDateTimeGMStr()
    print o.GetDateTimeLocalStr()
    print '--------------------'
    print '++++++++++++++++++++ gm'
    o=vtDateTime(False)
    print o.GetDateTimeStr()
    print o.GetDateTimeGMStr()
    print o.GetDateTimeLocalStr()
    print '--------------------'
    
    print '++++++++++++++++++++ gm / local parsing'
    o1=vtDateTime(False)
    o2=vtDateTime(True)
    o1.SetDateStr('2006-03-29')
    o2.SetDateStr('2006-03-29')
    for val in ['13:12:14.012345','13:12:15','13:13',
                '13:12:14.012345+00:00','13:12:14.012345+02:00',
                '13:12:14.012345-02:00','01:12:14.012345-02:00',
                '13:12:14.012345+02:00','01:12:14.012345+03:00',
                '13:12:14-02:00','01:12-02:00',
                '13:12+02:00','01:12+03:00']: 
        try:
            print 'parse   ',val
            o1.SetDateStr('2006-03-29')
            o2.SetDateStr('2006-03-29')
            o1.SetTimeStr(val)
            print '   gm   ',o1.GetDateTimeStr()
            o2.SetTimeStr(val)
            print '   local',o2.GetDateTimeStr()
            print
        except:
            pass
    
    print '++++++++++++++++++++ gm / local parsing'
    o1=vtDateTime(False)
    o2=vtDateTime(True)
    for val in ['2006-03-29T13:12:14.012345',
                '2006-03-29T13:12:15',
                '2006-03-29T13:13',
                '2006-03-29T13:12:14.012345+00:00',
                '2006-03-29T13:12:14.012345+02:00',
                '2006-03-29T13:12:14.012345-02:00',
                '2006-03-29T01:12:14.012345-02:00',
                '2006-03-29T13:12:14.012345+02:00',
                '2006-03-29T01:12:14.012345+03:00',
                '2006-03-29T13:12:14-02:00',
                '2006-03-29T01:12:00-02:00',
                '2006-03-29T13:12+02:00',
                '2006-01-29T23:59:59+02:00',
                '2006-03-29T01:12+03:00']: 
        try:
            print 'parse   ',val
            o1.SetStr(val)
            print '   gm   ',o1.GetDateTimeStr()
            o2.SetStr(val)
            print '   local',o2.GetDateTimeStr()
            print
        except:
            pass
    print '++++++++++++++++++++ time'
    v=vtTime()
    print v.GetStr(),v.GetStr(':')
    v.Clear()
    print v.GetStr(),v.GetStr(':')
    v.SetStr('12:34:56')
    print v.GetStr(),v.GetStr(':')
    v.SetStr('12:34:56',':')
    print v.GetStr(),v.GetStr(':')
    v.SetStr('223456')
    print v.GetStr(),v.GetStr(':')
    v.SetStr('223456',':')
    print v.GetStr(),v.GetStr(':')
    def rounding(sVal,tolMn):
        print '+++%s+++++++++ time rounding %s'%(sVal,tolMn)
        for f in [v.Round,v.Ceil,v.Floor]:
            v.SetStr(sVal,':')
            f(tolMn)
            print '  ',v.GetStr(':')
    
    rounding('09:34:56',5)
    rounding('09:32:56',5)
    rounding('09:32:26',5)
    rounding('09:58:26',5)
    
    #rounding('09:37:56',7.5)
    
    rounding('09:37:56',15)
    rounding('09:36:56',15)
    rounding('09:37:29',15)
    rounding('09:37:30',15)
    
    rounding('09:37:30',30)
    
    rounding('09:37:30',60)
    print 
    print '++++++++++++++++++++ datestartendtime'
    v=vtDateTimeStartEnd(True)
    v.Now()
    print v.GetDateTimeStartEndStr()
    print '   gm   ',v.GetDateTimeStr()
    val='13:12:14.012345+02:00'
    v.SetTimeStr(val)
    print '   local',v.GetDateTimeStr()
    print v.GetDateTimeStartEndStr()
    
    v.SetDateStr('2006-03-29')
    v.SetDateStr('2006-03-29')
    
    
