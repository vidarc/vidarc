#----------------------------------------------------------------------
# Name:         vtTimeDateGM.py
# Purpose:      text control with tree popup window
#
# Author:       Walter Obweger
#
# Created:      20060210
# CVS-ID:       $Id: vtTimeDateGM.py,v 1.22 2013/07/20 12:50:11 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import cStringIO
import string

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.time.vtTimeInputDate import vtTimeInputDate
from vidarc.tool.xml.vtXmlDomConsumer import *
from vidarc.tool.xml.vtXmlDomConsumerLang import *
    
VERBOSE=0

#----------------------------------------------------------------------
def getCancelData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00oIDAT8\x8d\xad\x93\xc1\x11\xc0 \x08\x04\x0f\xd3E\xfa\xaf\xcd6\xcc+\
\x19\xd1;p\x86\xf0fw\x80S\xb3v\xa1R\xadD\xff*\xe8\x86\xd1\r#\x03\xd6\xbeOp\
\x0f\xd8\xdb\x10\xc1s\xaf\x13d\x12\x06o\x02%Q0\x00\x98\x8aq\x9d\x82\xc1t\x02\
\x06(8\x14\xb0\x15\x8e\x05\xf3\xceY:\x9b\x80\x1d,\x928Atm%q/Q\xc1\x91D\xc6xZ\
\xe5\xcf\xf4\x00\xe0\xc8:\xc8\xd18`E\x00\x00\x00\x00IEND\xaeB`\x82' 

def getCancelBitmap():
    return wx.BitmapFromImage(getCancelImage())

def getCancelImage():
    stream = cStringIO.StringIO(getCancelData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getApplyData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00pIDAT8\x8dcddbf\xa0\x040\x91\xa3Ix\x89\xe0\x7f\xe1%\x82\xff\x19\x18\
\x18\x18XH\xd5\x08c\xbf\x8dy\xcfH\x92\x010\xcd0\x8d0@\x94\x17pi&\xda\x00\\\
\x9a\x892\x00\xd9\xdfd\x19@\x08\xe05\x00\x9f\xdfQ\x0c@\x8eW\x8a\\@\x8eAL0\'\
\xa2;\x93\x18\xe7c\xb8\x00\xa6\x98\x14W`\x04"\xb2\x8d\x84l\xc7j\x00\xa9\x80\
\x91\xd2\xec\x0c\x00y\x1c/\xbdxe+\x9e\x00\x00\x00\x00IEND\xaeB`\x82' 

def getApplyBitmap():
    return wx.BitmapFromImage(getApplyImage())

def getApplyImage():
    stream = cStringIO.StringIO(getApplyData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getDownData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x83IDAT8\x8d\xb5\x92\xd1\r\x84 \x10D\xdf\xaa5l\x05\xd4y\xa2\xdfj\
\x85TA\x03\xde\x17fUH\xee\x02\xcc\x17\t\xcc\xcc\xdb\r"\xc3H\x8d\x86*70\xa5\
\xc3\xba\xf83\x84\xf0\x93IU\xd9\xf6C\x00\xc4\x8e\xb0.\xfe\x04(\x05\xa9*\xc0e\
\xbe\x11X9\xe7\xb0A\xc9\x98\x93<\x97\x98(\x92b\x8c\xb7{\xdb\x0e\r\x96\xf8\n\
\xf8\xcc^r\x0fs\xed}\x08J\x14\xb9\xf6~\x04M\x02\xec\x18%\xfc&\x04\xaf\x8f\
\xf4\xaf\xaa\t\xbe\x1el!\xed\xde\xc2%\x86\x00\x00\x00\x00IEND\xaeB`\x82' 

def getDownBitmap():
    return wx.BitmapFromImage(getDownImage())

def getDownImage():
    stream = cStringIO.StringIO(getDownData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getToDayData():
    return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\\IDAT8\x8d\xed\x93\xb1\r\xc00\x08\x04\x0f\xdb\x8by\xb4L\x16){\x19R\
\x11Y\xae\x82)\xd2\xe4;\x8a?\xbd\xe0\x11)\x95\x8cJ\xca\r\x88\x81e\x00\r\xe0\
\xda4\x9f\x0e\xf0!\xa2>'p\x1d\xa5\xca\x1b\xb3\xea0O\x9d^\xe2\x0fX\xae\xa0:\
\xc2\xa5z\x00\x9d\xbdB5\x88\x97h\x96|\xfe\x8d7>\x12\x0f\xb0\xb3\xf3Wj\x00\
\x00\x00\x00IEND\xaeB`\x82" 

def getToDayBitmap():
    return wx.BitmapFromImage(getToDayImage())

def getToDayImage():
    stream = cStringIO.StringIO(getToDayData())
    return wx.ImageFromStream(stream)


# defined event for vtInputTextML item changed
wxEVT_VTTIME_DATE_CHANGED=wx.NewEventType()
vEVT_VTTIME_DATE_CHANGED=wx.PyEventBinder(wxEVT_VTTIME_DATE_CHANGED,1)
def EVT_VTTIME_DATE_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTTIME_DATE_CHANGED,func)
class vtTimeDateChanged(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTTIME_DATE_CHANGED(<widget_name>, self.OnChanged)
    """

    def __init__(self,obj,val):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTTIME_DATE_CHANGED)
        self.val=val
    def GetValue(self):
        return self.val
    

class vtTimeDateGMTransientPopup(wx.Dialog):
    #SIZE_PN_SCROLL=30
    def __init__(self, parent, size,style,name=''):
        self.SIZE_PN_SCROLL=wx.SystemSettings.GetMetric(wx.SYS_VSCROLL_X)
        wx.Dialog.__init__(self, parent,style=wx.RAISED_BORDER)#|wx.STAY_ON_TOP)
        id=wx.NewId()
        #wx.EVT_SIZE(self,self.OnSize)
        bxsAll=wx.FlexGridSizer(cols=1, hgap=0, rows=2, vgap=0)#wx.BoxSizer(wx.VERTICAL)
        bxsAll.AddGrowableRow(1)
        #bxsAll.AddGrowableCol(0)
        bxsBt=wx.BoxSizer(wx.HORIZONTAL)
        self.cbCancel = wx.BitmapButton(id=-1,
              bitmap=getCancelBitmap(), name=u'cbCancel',
              parent=self, pos=(0,0), size=(30,30), style=wx.BU_AUTODRAW)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              self.cbCancel)
        bxsBt.AddWindow(self.cbCancel,0,border=4,flag=0)
        
        self.cbApply = wx.BitmapButton(id=-1,
              bitmap=getApplyBitmap(), name=u'cbApply',
              parent=self, pos=wx.Point(32, 00), size=wx.Size(30, 30),
              style=wx.BU_AUTODRAW)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              self.cbApply)
        bxsBt.AddWindow(self.cbApply,0,border=4,flag=wx.LEFT)
        
        self.cbNow = wx.BitmapButton(id=-1,
              bitmap=getToDayBitmap(), name=u'cbNow',
              parent=self, pos=wx.Point(220, 00), size=wx.Size(30, 30),
              style=wx.BU_AUTODRAW)
        self.cbNow.Bind(wx.EVT_BUTTON, self.OnCbNowButton,self.cbNow)
        bxsBt.AddWindow(self.cbNow,0,border=32,flag=wx.LEFT)
        
        if size[1]<40:
            size=(size[0],40)
        i=0
        iY=40
        iH=size[1]+4
        iSzHeight=0
        sVal=parent.GetValue()
        dt=wx.DateTime.Today()
        dt.ParseFormat(sVal,'%Y%m%d')
        
        self.calCalendar = vtTimeInputDate(
              id=-1, name=u'calCalendar',draw_grid=False,
              parent=self, pos=wx.Point(4, 40),size=wx.Size(192, 192))#, size=wx.DefaultSize,#size=wx.Size(172, 137),
              #style=wx.calendar.CAL_SHOW_HOLIDAYS | wx.calendar.CAL_SHOW_SURROUNDING_WEEKS)
        #self.calCalendar.Bind(wx.calendar.EVT_CALENDAR_SEL_CHANGED,
        #      self.OnCalCalendarCalendarSelChanged,
        #      id=wxID_VCALENDARPANELCALCALENDAR)
        bxsAll.AddSizer(bxsBt,1,border=4,flag=wx.EXPAND|wx.LEFT|wx.RIGHT|wx.TOP)
        #bxsAll.AddSizer(bxsInp,1,border=4,flag=wx.EXPAND|wx.LEFT|wx.RIGHT|wx.TOP|wx.BOTTOM)
        bxsAll.AddWindow(self.calCalendar,1,border=4,flag=wx.EXPAND|wx.ALL)
        self.szOrig=(220,240)
        self.SetSize(self.szOrig)
        self.SetSizer(bxsAll)
        bxsAll.Layout()
        #bxsAll.Fit(self)
        self.bxsAll=bxsAll
        self.szOrig=self.GetSize()
        
    def OnSize(self,evt):
        iW,iH=self.GetSize()
        if iW<self.szOrig[0]:
            iW=self.szOrig[0]
        #if iH<self.szMin[1]:
        #    iH=self.szMin[1]
        if iH<self.szOrig[1]:
            iH=self.szOrig[1]
        self.SetSize((iW,iH))
        if self.calCalendar is not None:
            self.calCalendar.SetSize((iW-16,iH-52))
        evt.Skip()
    def OnCbCancelButton(self,evt):
        self.Show(False)
    def OnCbApplyButton(self,evt):
        #self.Apply()
        try:
            par=self.GetParent()
            sVal=self.calCalendar.GetValueSmallStr()
            par.__apply__(sVal)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        self.Show(False)
    def OnCbNowButton(self,evt):
        self.calCalendar.SetNow()
        #self.viTime.SetNow()
        evt.Skip()
    def Show(self,flag):
        try:
            par=self.GetParent()
            if flag:
                #vtLog.CallStack('')
                sVal=par.GetValue()
                self.SetVal(sVal)
                #self.calCalendar.Refresh()
                #self.calCalendar.Update()
            else:
                #par.cbPopup.SetValue(False)
                par.__setPopupState__(False)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        wx.Dialog.Show(self,flag)
    def GetVal(self):
        try:
            sVal=self.calCalendar.GetValueSmallStr()
            return sVal
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
            return ''
    def SetVal(self,sVal):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'val:%s'%(sVal),self)
        try:
            self.calCalendar.SetValueSmallStr(sVal)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
class vtTimeDateGM(wx.Panel,vtXmlDomConsumer,vtXmlDomConsumerLang):
    def __init__(self,*_args,**_kwargs):
        vtXmlDomConsumer.__init__(self)
        vtXmlDomConsumerLang.__init__(self)
        try:
            sz=_kwargs['size']
        except:
            sz=wx.Size(120,30)
            _kwargs['size']=sz
        try:
            szCb=_kwargs['size_button']
            del _kwargs['size_button']
        except:
            szCb=wx.Size(24,24)
        try:
            szTxt=_kwargs['size_text']
            del _kwargs['size_text']
        except:
            szTxt=wx.Size(sz[0]-szCb[0]-2,sz[1]-4)
        
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        self.SetAutoLayout(True)
        
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        size=(szTxt[0],szTxt[1])
        pos=(0,(sz[1]-szTxt[1])/2)
        self.txtVal = wx.TextCtrl(id=-1, name='txtVal',
              parent=self, pos=pos, size=size,
              style=0, value='')
        self.txtVal.SetMaxLength(10)
        self.txtVal.SetToolTipString('YYYY-MM-DD')
        #self.txtVal.SetConstraints(LayoutAnchors(self.txtVal, True, True,
        #      True, False))
        bxs.AddWindow(self.txtVal, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        self.txtVal.Bind(wx.EVT_TEXT, self.OnTextText,self.txtVal)
        
        size=(szCb[0],szCb[1])
        pos=(szTxt[0]+(sz[0]-(szTxt[0]+szCb[0]))/2,(sz[1]-szCb[1])/2)
        id=wx.NewId()
        self.cbPopup = wx.lib.buttons.GenBitmapToggleButton(ID=id,
              bitmap=wx.EmptyBitmap(8, 8), name=u'cbPopup',
              parent=self, pos=pos, size=size, style=wx.BU_AUTODRAW)
        #self.cbPopup.SetConstraints(LayoutAnchors(self.cbPopup, False, True,
        #      True, False))
        bxs.AddWindow(self.cbPopup, 0, border=0, flag=wx.EXPAND)
        self.SetSizer(bxs)
        self.cbPopup.SetBitmapLabel(getDownBitmap())
        self.cbPopup.SetBitmapSelected(getDownBitmap())
        self.cbPopup.Bind(wx.EVT_BUTTON,self.OnPopupButton,self.cbPopup)#id=id)
        self.popWin=None
        self.tagName='date'
        self.bBlock=True
        self.dt=wx.DateTime().Today()
        #self.dftBkgColor=self.txtVal.GetBackgroundColour()
        self.bEnableMark=True
        self.bBusy=False
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
    def Enable(self,flag):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        wx.Panel.Enable(self,flag)
        self.cbPopup.Refresh()
    def SetEnableMark(self,flag):
        self.bEnableMark=flag
    def IsModified(self):
        return self.bMod
    def __markModified__(self,flag=True):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.bEnableMark==False:
            return
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'__markModified__;flag:%s'%(flag),
        #                origin=self.GetName())
        if flag:
            f=self.txtVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_BOLD)
            self.txtVal.SetFont(f)
        else:
            f=self.txtVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
            self.txtVal.SetFont(f)
        self.bMod=flag
        self.txtVal.Refresh()
    def __markFlt__(self,flag=False):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if flag:
            #vtLog.vtLngCurWX(vtLog.DEBUG,'val:%s'%(self.txtVal.GetValue()),self)
            color=wx.TheColourDatabase.Find('YELLOW')
            self.txtVal.SetBackgroundColour(color)
        else:
            color=wx.SystemSettings_GetColour(wx.SYS_COLOUR_WINDOW)
            self.txtVal.SetBackgroundColour(color)
    def SetTagName(self,tagName):
        self.tagName=tagName
    def SetTagNames(self,tagName):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'SetTagNames;tagname:%s,%s'%(tagName,tagNameInt),
        #                origin=self.GetName())
        self.tagName=tagName
    def ClearLang(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'ClearLang',
        #                origin=self.GetName())
        #if VERBOSE:
        #    vtLog.CallStack('')
        self.bBlock=True
        self.txtVal.SetValue('')
        wx.CallAfter(self.__clearBlock__)
    def UpdateLang(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'UpdateLang',
        #                origin=self.GetName())
        return
        #if VERBOSE:
        #    vtLog.CallStack('')
        try:
            if self.popWin is None:
                sVal=self.doc.getNodeText(self.node,self.tagName)
            else:
                sVal=self.popWin.GetVal(self.doc.GetLang())
            self.bBlock=True
            self.txtVal.SetValue(string.split(sVal,'\n')[0])
            wx.CallAfter(self.__clearBlock__)
            #self.txtVal.SetValue(sVal)
        except:
            vtLog.vtLngTB(self.GetName())

    def Clear(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'Clear',
        #                origin=self.GetName())
        #if VERBOSE:
        #    vtLog.CallStack('')
        self.bBlock=True
        self.ClearLang()
        self.node=None
        self.__markModified__(False)
        wx.CallAfter(self.__clearBlock__)
    def __setPopupState__(self,flag):
        self.bBusy=flag
        self.cbPopup.SetValue(flag)
    def IsBusy(self):
        return self.bBusy
    def __Close__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.popWin is not None:
            self.popWin.Show(False)
            self.__setPopupState__(False)
    def Stop(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'Stop',self)
        wx.CallAfter(self.__Close__)
    def SetDoc(self,doc):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'SetDoc',
        #                origin=self.GetName())
        vtXmlDomConsumer.SetDoc(self,doc)
        vtXmlDomConsumerLang.SetDoc(self,doc)
    def SetNode(self,node):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'SetNode;node:%s'%(node),
        #                origin=self.GetName())
        #if VERBOSE:
        #    vtLog.CallStack('')
        #    print node
        self.Clear()
        self.node=node
        if self.doc is None:
            return
        if self.node is None:
            return
        try:
            sVal=string.split(self.doc.getNodeText(self.node,self.tagName),'\n')[0]
            bOk,bMod,sVal=self.__validate__(sVal,True)
            self.bBlock=True
            self.__markModified__(bMod)
            self.__markFlt__(not bOk)
            self.txtVal.SetValue(sVal)
            wx.CallAfter(self.__clearBlock__)
        except:
            vtLog.vtLngTB(self.GetName())
        #self.__getSelNode__()
        self.UpdateLang()
    def __validate__(self,sVal,bSet2NowOnFlt=False,bTxt=False):
        #if VERBOSE:
        #    vtLog.CallStack(sVal)
        bMod=False
        bOk=False
        try:
            if bTxt:
                ret=self.dt.ParseFormat(sVal,'%Y-%m-%d')
                iLen=10
            else:
                ret=self.dt.ParseFormat(sVal,'%Y%m%d')
                iLen=8
            if iLen!=len(sVal):
                ret=-1
                #sVal=sVal[:iLen]
                #bMod=True
            if ret!=iLen:
                if bSet2NowOnFlt:
                    bMod=True
            else:
                bOk=True
        except:
            #vtLog.vtLngCurWX(vtLog.WARN,sVal,self)
            if bSet2NowOnFlt:
                bMod=True
        if bMod:
            if bSet2NowOnFlt:
                self.dt=self.dt.Now()
        sVal=self.dt.FormatISODate()
        return bOk,bMod,sVal
    def __clearBlock__(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'__clearBlock__',
        #                origin=self.GetName())
        self.bBlock=False
    def GetNode(self,node=None):
        if node is None:
            node=self.node
        if node is None:
            return
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'GetNode',
        #                origin=self.GetName())
        #if VERBOSE:
        #    vtLog.CallStack('')
        try:
            sVal=self.dt.Format('%Y%m%d')
            self.doc.setNodeText(node,self.tagName,sVal)
            #self.__setNode__()

            self.__markModified__(False)
            self.doc.AlignNode(node)
        except:
            pass
            #vtLog.vtLngTB(self.GetName())
            
    def __getDoc__(self):
        return self.doc
    def __getNode__(self,node=None):
        if node is None:
            return self.node
        return node
    def __apply__(self,sVal):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'__apply__;val:%s'%(sVal),
        #                origin=self.GetName())
        #if VERBOSE:
        #    vtLog.CallStack(sVal)
        try:
            #sVal=self.popWin.GetVal()
            #if self.trSetNode is not None:
            #    sVal=self.trSetNode(self.docTreeTup[0],node)
            #else:
            #    sVal=''
            bOk,bMod,sVal=self.__validate__(sVal)
            self.__markFlt__(not bOk)
            if bMod or self.txtVal.GetValue()!=sVal:
                if self.bBlock==False:
                    self.__markModified__(True)
                    wx.PostEvent(self,vtTimeDateChanged(self,sVal))
        except:
            sVal=_(u'fault')
            vtLog.vtLngTB(self.GetName())
        self.bBlock=True
        self.txtVal.SetValue(string.split(sVal,'\n')[0])
        wx.CallAfter(self.__clearBlock__)
        return sVal
        
    def __createPopup__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'__createPopup__',
        #                origin=self.GetName())
        if self.popWin is None:
            sz=self.GetSize()
            sz=(sz[0],sz[1]-20)
            try:
                self.popWin=vtTimeDateGMTransientPopup(self,sz,wx.SIMPLE_BORDER)
            except:
                vtLog.vtLngTB(self.GetName())
            #self.popWin.SetVal(self.txtVal.GetValue(),self.doc.GetLang())
        else:
            pass
    def SetValueStr(self,sVal):
        #self.txtVal.SetValue(val)
        self.__apply__(sVal[0:4]+sVal[5:7]+sVal[8:10])
        #self.__apply__(sVal)
        #self.__validate__(sVal,bTxt=True)
    def SetValue(self,sVal):
        #self.txtVal.SetValue(val)
        self.__apply__(sVal)
    def GetValueStr(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%self.txtVal.GetValue(),self)
        return self.txtVal.GetValue()
    def GetValue(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.__validate__(self.txtVal.GetValue(),bTxt=True)
        sVal=self.dt.Format('%Y%m%d')
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%sVal,self)
        return sVal
            
    def OnTextText(self,evt):
        if self.bBlock:
            return
        sVal=self.txtVal.GetValue()
        try:
            bOk,bMod,val=self.__validate__(sVal,False,bTxt=True)
            self.__markFlt__(not bOk)
            if bMod:
                self.txtVal.SetValue(val)
        except:
            vtLog.vtLngTB(self.GetName())
        self.__markModified__()
        wx.PostEvent(self,vtTimeDateChanged(self,sVal))
    def OnPopupButton(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.__createPopup__()
        if self.cbPopup.GetValue()==True:
            btn=self
            iX,iY = btn.ClientToScreen( (0,0) )
            iW,iH =  btn.GetSize()
            iPopW,iPopH=self.popWin.GetSize()
            iScreenW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            iScreenH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            if iX+iPopW>iScreenW:
                iX=iScreenW-iPopW-4
                if iX<0:
                    iX=0
            iY+=iH
            if iY+iPopH>iScreenH:
                iY=iScreenH-iPopH
                if iY<0:
                    iY=0
            
            self.popWin.Move((iX,iY))
            #self.popWin.Move((pos[0],pos[1]+sz[1]))
            self.bBusy=True
            self.popWin.Show(True)
        else:
            self.popWin.Show(False)
            self.bBusy=False
        evt.Skip()

