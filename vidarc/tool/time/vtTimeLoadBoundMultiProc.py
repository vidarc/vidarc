#----------------------------------------------------------------------------
# Name:         vtTimeLoadBoundMultiProc.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080205
# CVS-ID:       $Id: vtTimeLoadBoundMultiProc.py,v 1.1 2008/02/04 16:34:44 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vtTimeLoadBound import vtTimeLoadBound
from vtTimeUsageCPU import getCountCPU

gLdBdMP=None

class vtTimeLoadBoundMultiProc(vtTimeLoadBound):
    def __init__(self,fBound=0.75):
        vtTimeLoadBound.__init__(self,fBound=fBound)
        self.fLimit=fBound*getCountCPU()

def vtTimeLoadBoundMultiProcInit(fBound=0.75):
    global gLdBdMP
    gLdBdMp=vtTimeLoadBoundMultiProc(fBound)
def check():
    global gLdBdMP
    if gLdBdMP is None:
        return -1
    try:
        return gLdBdMP.Check()
    except:
        import traceback
        traceback.print_exc()
        vtTimeLoadBoundMultiProcInit(fBound=0.95)
    return -1

