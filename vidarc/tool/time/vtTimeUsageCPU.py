#----------------------------------------------------------------------------
# Name:         vtTimeUsageCPU.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080204
# CVS-ID:       $Id: vtTimeUsageCPU.py,v 1.5 2009/02/22 17:56:42 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import time,os,traceback
import vidarc
from vidarc.tool.vtThreadCore import getLock
#from vidarc.tool.vtThreadCore import vtThreadCore
from vidarc.tool.vtThreadCoreCyclic import vtThreadCoreCyclic
from vidarc.tool.vtRingBuffer import vtRingBuffer

gUsageCPU=None

_semUsage=getLock()

RUSAGE_SIM_USE_OS_TIMES=1
try:
    from resource import getrusage, RUSAGE_SELF
except ImportError:
    try:
        RUSAGE_SELF = 0
        if RUSAGE_SIM_USE_OS_TIMES:
            def getrusage(who=0):
                t=os.times()
                return t[:4]
                return [t[0],t[1]]
        else:
            import win32process
            HPROCMAIN=win32process.GetCurrentProcess()
            def getrusage(who=0):
                d=win32process.GetProcessTimes(HPROCMAIN)
                return [d['UserTime'], d['KernelTime']] # on non-UNIX platforms cpu_time always 0.0
        try:
            import win32process
            HPROCMAIN=win32process.GetCurrentProcess()
            def getfullusage():
                l=win32process.EnumProcesses()
                fU,fS=0.0,0.0
                for pid in l[1:]:
                    try:
                        d=win32process.GetProcessTimes(pid)
                        fU+=d['UserTime']
                        fS+=d['KernelTime']
                    except:
                        pass
                return [fU,fS]
        except:
            pass
    except ImportError:
        RUSAGE_SELF = 0
        def getrusage(who=0):
            t=os.times()
            return [t[0],t[1]]
            return [0.0, 0.0] # on non-UNIX platforms cpu_time always 0.0

gProcCount=1.0
try:
    import sys
    if sys.platform.startswith('win'):
        import win32com.client
        objWMIService = win32com.client.GetObject("winmgmts:\\root\\cimv2")
        colItems = objWMIService.ExecQuery("Select ProcessorId from Win32_Processor")
        gProcCount=float(len(colItems))
except:
    pass

def getCountCPU():
    global gProcCount
    return gProcCount

def sumUsageCPU():
    global _semUsage
    _semUsage.acquire()
    try:
        t=getrusage()
        v=reduce(lambda x,y:x+y,t)
        #print t,v
        #v=t[0]+t[1]
    except:
        v=0.0
    _semUsage.release()
    return v

def getUsageCPU():
    global _semUsage
    _semUsage.acquire()
    try:
        t=getrusage()
    except:
        t=[0.0,0.0,0.0,0.0]
    _semUsage.release()
    return t

def getFullUsageCPU():
    global _semUsage
    _semUsage.acquire()
    try:
        t=getfullusage()
    except:
        traceback.print_exc()
        t=[0.0,0.0]
    _semUsage.release()
    return t

class vtTimeUsageCPU(vtThreadCoreCyclic):#vtThreadCore):
    Z_MEASURE=0.25
    SIZE_MEDIAN=5
    def __init__(self,zInterval=0.25,iSz=-1,verbose=0):
        #vtThreadCore
        vtThreadCoreCyclic.__init__(self,bPost=False,verbose=verbose)
        global gProcCount
        if iSz<=0:
            iSz=self.SIZE_MEDIAN
        self.rbLoad=vtRingBuffer(iSz)
        for i in xrange(self.SIZE_MEDIAN):
            self.rbLoad.put(0.0)
        if zInterval<0:
            zInterval=self.Z_MEASURE
        #self.fDelay=
        self.SetCB(self.__measure__)
        self.SetDelay(max(0.05,zInterval))
        self.iProcCount=gProcCount
        self.zLast=time.time()
        self.fConsumed=0.0
        self.lCB=[]
        self.lIdxLess=[]
        self.iPosLess=-1
        self.lIdxAbove=[]
        self.iPosAbove=-1
        self.fLoad=0.0
        self.Start()
    def AddCB(self,fVal,bBelow,func,*args,**kwargs):
        t=(func,args,kwargs)
        iPos=len(self.lCB)
        self.lCB.append(t)
        if bBelow:
            self.lIdxLess.append((fVal,iPos))
            self.lIdxLess.sort()
        else:
            self.lIdxAbove.append((fVal,iPos))
            self.lIdxAbove.sort()
            #self.lIdxAbove.reverse()
    def DelCB(self,func,*args,**kwargs):
        t=(func,args,kwargs)
        try:
            if t in self.lCB:
                idx=self.lCB.index(t)
                self.lCB.remove(t)
        except:
            pass
    def StartOld(self):
        vtThreadCore.Start(self)
        while self.qWakeUp.empty()==False:
            self.qWakeUp.get()
        self.Do(self.__procMeasure__)
        #vtThreadCore.Do(self,self.__measure)
    #def Do(self,func,*args,**kwargs):
    #    pass
    def __procMeasure__(self):
        try:
            self.qWakeUp.get(block=True,timeout=self.fDelay)
        except:
            self.funcCB(*self.argsCB,**self.kwargsCB)
        #while self.Is2Stop()==False:
            self.Do(self.__procMeasure__)
    def GetLoad(self,iSz=-1):
        self._acquire()
        try:
            return self.fLoad
        finally:
            self._release()
    def __calc__(self):
        if iSz<=0:
            self.fLoad=self.rbLoad.get(0)
        iSz=min(iSz,self.SIZE_MEDIAN)
        fLoad=0.0
        for i in xrange(iSz):
            fLoad+=self.rbLoad.get(i)
        self.fLoad=fLoad/float(iSz)
    def __measure__(self):
            try:
                self._acquire()
                zCons=sumUsageCPU()
                #print zCons
                #print getFullUsageCPU()
                #print getUsageCPU()
                clk=time.time()
                dZ=clk-self.zLast
                dC=zCons-self.fConsumed
                #dT=((zCons)-self.fConsumed)/self.iProcCount
                fLoad=(dC/self.iProcCount/dZ)#self.fDelay)
                self.zLast=clk
                self.fConsumed=zCons
                #print
                #print dC,dZ,fLoad
                #print fLoad
                self.rbLoad.put(fLoad)
                self.__calc__()
                if 0:
                    i=0
                    if self.iPosLess<0:
                        iOld=len(self.lIdxLess)
                    else:
                        iOld=self.iPosLess
                    iNew=-1
                    for fVal,iPos in self.lIdxLess:
                        try:
                            print fLoad,fVal,i,iOld,iNew
                            if fVal>fLoad:
                                iNew=i
                                break
                                #if iNew<0:
                                #    iNew=i
                                #if i<iOld:
                                #    func,args,kwargs=self.lCB[iPos]
                                #    func(*args,**kwargs)
                                #else:
                                #    break
                        except:
                            pass
                        i+=1
                    if iNew>=0:
                        print 'less',self.iPosLess,iNew
                        print fLoad,fVal,i,iOld,iNew
                        print self.lIdxLess
                        self.iPosLess=iNew
                    i=0
                    if self.iPosAbove<0:
                        iOld=len(self.lIdxAbove)
                    else:
                        iOld=self.iPosAbove
                    iNew=-1
                    for fVal,iPos in self.lIdxAbove:
                        try:
                            print fLoad,fVal,i,iOld,iNew
                            if fLoad<fVal:
                                break
                                #iNew=i
                                #continue
                                #if iNew<0:
                                #    iNew=i
                                #if i<iOld:
                                #    func,args,kwargs=self.lCB[iPos]
                                #    func(*args,**kwargs)
                            #else:
                            #        break
                        except:
                            pass
                        i+=1
                    if i<self.iPosAbove:
                        print 'above',self.iPosAbove,i
                        print fLoad,fVal,i,iOld,i
                        print self.lIdxAbove
                        self.iPosAbove=i
                                #    func,args,kwargs=self.lCB[iPos]
                                #    func(*args,**kwargs)
            finally:
                self._release()
            #time.sleep(self.fDly)
            
        

def vtTimeUsageCPUInit(zInterval=0.25,iSz=-1,verbose=0):
    global gUsageCPU
    gUsageCPU=vtTimeUsageCPU(zInterval,iSz,verbose)
def vtTimeUsageCPUDestroy():
    global gUsageCPU
    if gUsageCPU is not None:
        gUsageCPU.Stop()
        while gUsageCPU.IsRunning():
            time.sleep(0.1)
        gUsageCPU=None

vidarc.AddCleanUp(vtTimeUsageCPUDestroy)

def start(zInterval=-1,iSz=-1):
    global gUsageCPU
    if gUsageCPU is None:
        vtTimeUsageCPUInit(zInterval=zInterval,iSz=iSz)
        return
    gUsageCPU.Start()

def addCB(fVal,bBelow,func,*args,**kwargs):
    global gUsageCPU
    if gUsageCPU is None:
        vtTimeUsageCPUInit()
    if gUsageCPU is None:
        return
    gUsageCPU.AddCB(fVal,bBelow,func,*args,**kwargs)

def getLoad(iSz=-1):
    global gUsageCPU
    if gUsageCPU is None:
        vtTimeUsageCPUInit()
    if gUsageCPU is not None:
        return gUsageCPU.GetLoad(iSz)
    return 0

def stop():
    global gUsageCPU
    if gUsageCPU is not None:
        gUsageCPU.Stop()

