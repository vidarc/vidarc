#----------------------------------------------------------------------------
# Name:         vtTimeLoadIdle.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20070324
# CVS-ID:       $Id: vtTimeLoadIdle.py,v 1.10 2011/01/16 22:09:47 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import time,os
from vidarc.tool.vtRingBuffer import vtRingBuffer
from vtTimeUsageCPU import sumUsageCPU
from vtTimeUsageCPU import getLoad

LOG_DEBUG=0
import vidarc.tool.log.vtLog as vtLog

gLdIdle=None


#DEBUG_LOOP_TIMEOUT=5
#if DEBUG_LOOP_TIMEOUT>0:
#    import pywin.debugger
class vtTimeLoadIdle:
    Z_MEASURE=0.25
    SIZE_MEDIAN=5
    def __init__(self,fBound=0.05,zInterval=0.025,zTimeOut=60,verbose=0):
        self.fBound=fBound
        self.zInterval=zInterval
        self.zTimeOut=zTimeOut
        self.zLast=time.time()
        self.fConsumed=0.0
        self.VERBOSE=verbose
        self.rbLoad=vtRingBuffer(self.SIZE_MEDIAN)
        for i in xrange(self.SIZE_MEDIAN):
            self.rbLoad.put(0.0)
    def Wait(self,zInterval=-1,zTimeOut=-1):
        try:
            if zTimeOut<0:
                zTimeOut=self.zTimeOut
            if zInterval<0:
                zInterval=self.zInterval
            if zInterval<0:
                return True
            
            #clk=time.time()
            zWait=0
            #t=getrusage()#os.times()
            zConsE=sumUsageCPU()#t[0]+t[1]#+t[2]+t[3]
            if LOG_DEBUG:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurCls(vtLog.DEBUG,'for idle;interval:%4.3f;bound:%4.3f'%(zInterval,self.fBound),self)
            if self.VERBOSE>0:
                print 'Wait for idle','interval',zInterval,'bound',self.fBound
                print '  ',zConsE#t
                #print '  ',getrusage()
                print '  ',zWait,zTimeOut,zWait<zTimeOut
            #for i in xrange(self.SIZE_MEDIAN):
            #    self.rbLoad.put(0.0)
            if zInterval>self.Z_MEASURE:
                zSleep=self.Z_MEASURE
            else:
                zSleep=zInterval
            zIdle=0
            iOfs=0
            while zWait<zTimeOut:
                time.sleep(zSleep)
                if 1:
                    zConsS=zConsE
                    #t=getrusage()
                    zConsE=sumUsageCPU()#t[0]+t[1]#+t[2]+t[3]
                    dT=zConsE-zConsS
                    #zIntervalReal=t[4]
                    fLoad=(dT/zSleep)
                    self.rbLoad.put(fLoad)
                    iOfs+=1
                    if iOfs>=self.SIZE_MEDIAN:
                        iOfs=self.SIZE_MEDIAN
                    fLoad=0.0
                    for i in xrange(iOfs):
                        fLoad+=self.rbLoad.get(i)
                    fLoad=fLoad/float(iOfs)
                    if self.VERBOSE>0:
                        print '  ','zConsS:%7.3f zConsE:%7.3f'%(zConsS,zConsE),
                        print '  ',zConsE#t#,zIntervalReal
                        print '  ',zSleep,zInterval,zIdle
                        print '  ','dT:%6.3f load:%5.2f'%(dT,fLoad)
                    
                    #if RUSAGE_SIM_USE_OS_TIMES:
                    #    bIdle=fLoad<self.fBound
                    #else:
                    #    bIdle=(dT)<self.fBound
                else:
                    fLoad=getLoad(self.SIZE_MEDIAN)
                bIdle=fLoad<self.fBound
                if LOG_DEBUG:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurCls(vtLog.DEBUG,
                                    'done=%d load:%5.2f;bound:%4.3f'%(
                                    bIdle,fLoad,self.fBound),self)
                if bIdle:
                    zIdle+=zSleep
                    if zIdle>=zInterval:
                        return True
                else:
                    zIdle=0
                if LOG_DEBUG:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurCls(vtLog.DEBUG,
                                'timeout;interval:%4.3f;bound:%4.3f;zWait:%5.3f;zTimeOut:%5.3f'%(
                                zInterval,self.fBound,zWait,zTimeOut),self)
                zWait+=zSleep
                #t=getrusage()
                zConsE=sumUsageCPU()#t[0]+t[1]
                #if zWait>DEBUG_LOOP_TIMEOUT:
                #    pywin.debugger.GetDebugger().set_trace()
            if LOG_DEBUG:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurCls(vtLog.DEBUG,
                            'timeout;interval:%4.3f;bound:%4.3f;zWait:%5.3f;zTimeOut:%5.3f'%(
                            zInterval,self.fBound,zWait,zTimeOut),self)
            if self.VERBOSE>0:
                print '  ',zWait,zTimeOut,zWait<zTimeOut
                print '  ','time out'
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return False
def vtTimeLoadIdleInit(fBound=0.05,zInterval=0.05,zTimeOut=60,verbose=0):
    global gLdIdle
    gLdIdle=vtTimeLoadIdle(fBound,zInterval,zTimeOut,verbose)
def wait(zInterval=-1,zTimeOut=-1):
    global gLdIdle
    if gLdIdle is None:
        return
    try:
        return gLdIdle.Wait(zInterval=zInterval,zTimeOut=zTimeOut)
    except:
        vtTimeLoadIdleInit(fBound=0.05,zInterval=0.5,zTimeOut=60)
        return gLdIdle.Wait()
