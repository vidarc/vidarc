#Boa:Dialog:vtTimeSelectorHourDialog
#----------------------------------------------------------------------------
# Name:         vtTimeSelectorHoursDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtTimeSelectorHourDialog.py,v 1.2 2006/02/14 12:26:43 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import time
import images

def create(parent):
    return vtTimeSelectorHourDialog(parent)

[wxID_VTTIMESELECTORHOURDIALOG, wxID_VTTIMESELECTORHOURDIALOGCBAPPLY, 
 wxID_VTTIMESELECTORHOURDIALOGCBCANCEL, 
 wxID_VTTIMESELECTORHOURDIALOGLBLENDTIME, 
 wxID_VTTIMESELECTORHOURDIALOGLBLSTARTTIME, 
 wxID_VTTIMESELECTORHOURDIALOGSPNENDHR, 
 wxID_VTTIMESELECTORHOURDIALOGSPNENDMIN, 
 wxID_VTTIMESELECTORHOURDIALOGSPNSTARTHR, 
 wxID_VTTIMESELECTORHOURDIALOGSPNSTARTMIN, 
] = [wx.NewId() for _init_ctrls in range(9)]

class vtTimeSelectorHourDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTTIMESELECTORHOURDIALOG,
              name=u'vtTimeSelectorHourDialog', parent=prnt, pos=wx.Point(424,
              180), size=wx.Size(186, 126), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'Selector Hour Dialog')
        self.SetClientSize(wx.Size(178, 99))

        self.lblStartTime = wx.StaticText(id=wxID_VTTIMESELECTORHOURDIALOGLBLSTARTTIME,
              label=u'Start Time', name=u'lblStartTime', parent=self,
              pos=wx.Point(8, 8), size=wx.Size(48, 13), style=0)

        self.lblEndTime = wx.StaticText(id=wxID_VTTIMESELECTORHOURDIALOGLBLENDTIME,
              label=u'End Time', name=u'lblEndTime', parent=self,
              pos=wx.Point(8, 36), size=wx.Size(45, 13), style=0)

        self.spnStartHr = wx.SpinCtrl(id=wxID_VTTIMESELECTORHOURDIALOGSPNSTARTHR,
              initial=0, max=24, min=0, name=u'spnStartHr', parent=self,
              pos=wx.Point(64, 4), size=wx.Size(50, 21),
              style=wx.SP_ARROW_KEYS)
        self.spnStartHr.Bind(wx.EVT_TEXT, self.OnSpnStartHrText,
              id=wxID_VTTIMESELECTORHOURDIALOGSPNSTARTHR)
        self.spnStartHr.Bind(wx.EVT_SPINCTRL, self.OnSpnStartHrSpinctrl,
              id=wxID_VTTIMESELECTORHOURDIALOGSPNSTARTHR)

        self.spnStartMin = wx.SpinCtrl(id=wxID_VTTIMESELECTORHOURDIALOGSPNSTARTMIN,
              initial=0, max=59, min=0, name=u'spnStartMin', parent=self,
              pos=wx.Point(120, 4), size=wx.Size(50, 21),
              style=wx.SP_ARROW_KEYS)
        self.spnStartMin.Bind(wx.EVT_TEXT, self.OnSpnStartMinText,
              id=wxID_VTTIMESELECTORHOURDIALOGSPNSTARTMIN)
        self.spnStartMin.Bind(wx.EVT_SPINCTRL, self.OnSpnStartMinSpinctrl,
              id=wxID_VTTIMESELECTORHOURDIALOGSPNSTARTMIN)

        self.spnEndHr = wx.SpinCtrl(id=wxID_VTTIMESELECTORHOURDIALOGSPNENDHR,
              initial=0, max=24, min=0, name=u'spnEndHr', parent=self,
              pos=wx.Point(64, 32), size=wx.Size(50, 21),
              style=wx.SP_ARROW_KEYS)
        self.spnEndHr.Bind(wx.EVT_TEXT, self.OnSpnEndHrText,
              id=wxID_VTTIMESELECTORHOURDIALOGSPNENDHR)
        self.spnEndHr.Bind(wx.EVT_SPINCTRL, self.OnSpnEndHrSpinctrl,
              id=wxID_VTTIMESELECTORHOURDIALOGSPNENDHR)

        self.spnEndMin = wx.SpinCtrl(id=wxID_VTTIMESELECTORHOURDIALOGSPNENDMIN,
              initial=0, max=100, min=0, name=u'spnEndMin', parent=self,
              pos=wx.Point(120, 32), size=wx.Size(50, 21),
              style=wx.SP_ARROW_KEYS)
        self.spnEndMin.Bind(wx.EVT_TEXT, self.OnSpnEndMinText,
              id=wxID_VTTIMESELECTORHOURDIALOGSPNENDMIN)
        self.spnEndMin.Bind(wx.EVT_SPINCTRL, self.OnSpnEndMinSpinctrl,
              id=wxID_VTTIMESELECTORHOURDIALOGSPNENDMIN)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTTIMESELECTORHOURDIALOGCBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Apply', name=u'cbApply',
              parent=self, pos=wx.Point(8, 64), size=wx.Size(76, 30), style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VTTIMESELECTORHOURDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTTIMESELECTORHOURDIALOGCBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel', name=u'cbCancel',
              parent=self, pos=wx.Point(96, 64), size=wx.Size(76, 30), style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VTTIMESELECTORHOURDIALOGCBCANCEL)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        img=images.getApplyBitmap()
        self.cbApply.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.cbCancel.SetBitmapLabel(img)
        
    def SetValue(self,start,end):
        zNow=time.localtime(time.time())
        try:
            hr=int(start[0:2])
        except:
            hr=zNow[2]
        self.spnStartHr.SetValue(hr)
        try:
            min=int(start[2:4])
        except:
            min=zNow[3]
        self.spnStartMin.SetValue(min)
        try:
            hr=int(end[0:2])
        except:
            hr=zNow[2]
        self.spnEndHr.SetValue(hr)
        try:
            min=int(end[2:4])
        except:
            min=zNow[3]
        self.spnEndMin.SetValue(min)

    def GetValue(self):
        start='%02d%02d'%(self.spnStartHr.GetValue(),self.spnStartMin.GetValue())
        end  ='%02d%02d'%(self.spnEndHr.GetValue()  ,self.spnEndMin.GetValue())
        return start,end

    def OnCbApplyButton(self, event):
        self.EndModal(1)
        #event.Skip()

    def OnCbCancelButton(self, event):
        self.EndModal(0)
        #event.Skip()

    def __checkStart__(self):
        hr=self.spnStartHr.GetValue()
        if hr==24:
            self.spnStartMin.SetValue(0)
        if self.spnEndHr.GetValue()<hr:
            self.spnEndHr.SetValue(hr)
        elif self.spnEndHr.GetValue()==hr:
            if self.spnEndMin.GetValue()<self.spnStartMin.GetValue():
                self.spnEndMin.SetValue(self.spnStartMin.GetValue())
    def __checkEnd__(self):
        hr=self.spnEndHr.GetValue()
        if hr==24:
            self.spnEndMin.SetValue(0)
        if self.spnStartHr.GetValue()>hr:
            self.spnStartHr.SetValue(hr)
        elif self.spnStartHr.GetValue()==hr:
            if self.spnStartMin.GetValue()>self.spnEndMin.GetValue():
                self.spnStartMin.SetValue(self.spnEndMin.GetValue())
    
    def OnSpnStartHrText(self, event):
        self.__checkStart__()
        self.spnStartHr.SetValue(self.spnStartHr.GetValue())
        event.Skip()

    def OnSpnStartHrSpinctrl(self, event):
        self.__checkStart__()
        event.Skip()

    def OnSpnStartMinText(self, event):
        self.__checkStart__()
        event.Skip()

    def OnSpnStartMinSpinctrl(self, event):
        self.__checkStart__()
        event.Skip()

    def OnSpnEndHrText(self, event):
        self.__checkEnd__()
        self.spnEndHr.SetValue(self.spnEndHr.GetValue())
        event.Skip()

    def OnSpnEndHrSpinctrl(self, event):
        self.__checkEnd__()
        event.Skip()

    def OnSpnEndMinText(self, event):
        self.__checkEnd__()
        event.Skip()

    def OnSpnEndMinSpinctrl(self, event):
        self.__checkEnd__()
        event.Skip()
