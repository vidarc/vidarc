#----------------------------------------------------------------------------
# Name:         vtTimeLoadBound.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20061219
# CVS-ID:       $Id: vtTimeLoadBound.py,v 1.5 2008/02/04 16:34:44 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import time,os
from vidarc.tool.vtThreadCore import getLock
from vtTimeUsageCPU import sumUsageCPU,getCountCPU

gLdBd=None

class vtTimeLoadBound:
    def __init__(self,fBound=0.75):
        #global gProcCount
        self.fBound=fBound
        self.fLimit=fBound#gProcCount
        self.zLast=time.time()
        self.fConsumed=0.0
        self.semAccess=getLock()
    def Check(self):
        try:
            self.semAccess.acquire()
            clk=time.time()
            dZ=clk-self.zLast
            if dZ<0.1:
                return -1
            #t=os.times()
            zCons=sumUsageCPU()#t[0]+t[1]
            dT=(zCons)-self.fConsumed
            
            #dZ=clk-self.zLast+0.001
            #print t
            #print '   dT:%6.3f,dZ:%6.3f,delta:%6.3f,fBound:%6.3f'%(dT,dZ,(dT/dZ),self.fBound)
            if dZ>2.0:
                self.zLast=clk
                self.fConsumed=zCons
                return -1
            elif (dT/dZ)>self.fLimit:
                #print zCons-self.fConsumed
                #print '   dT:%6.3f,dZ:%6.3f,delta:%6.3f,fBound:%6.3f'%(dT,dZ,(dT/dZ),self.fBound)
                self.zLast=clk
                self.fConsumed=zCons
                #print '  ',1.0-self.fBound
                time.sleep((1.0-self.fBound)*0.1)    # 070801:wro deact load bounding
                #self.zLast=time.time()
                return 1.0-self.fBound
        finally:
            self.semAccess.release()
        return -1
def vtTimeLoadBoundInit(fBound=0.75):
    global gLdBd
    gLdBd=vtTimeLoadBound(fBound)
def check():
    global gLdBd
    if gLdBd is None:
        return -1
    try:
        return gLdBd.Check()
    except:
        import traceback
        traceback.print_exc()
        vtTimeLoadBoundInit(fBound=0.95)
    return -1
#vtTimeLoadBoundInit(0.75)
