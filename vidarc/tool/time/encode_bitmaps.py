#!/usr/bin/env python
#----------------------------------------------------------------------
# Name:         encode_bitmap.py
# Purpose:      convert icons to python script
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: encode_bitmaps.py,v 1.1 2006/02/14 12:26:43 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    "-u -i -n Cancel img/abort_2.png images.py",
    "-a -u -n Apply img/ok_2.png images.py",
    "-a -u -n Down img/Down07.png images.py",
    "-a -u -n Left img/Left07.png images.py",
    "-a -u -n Right img/Right07.png images.py",
    "-a -u -n Now img/Timer02_16.png images.py",

    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)

