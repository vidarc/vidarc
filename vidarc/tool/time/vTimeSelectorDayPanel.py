#Boa:FramePanel:vtTimeSelectorDayPanel
#----------------------------------------------------------------------------
# Name:         vimeSelectorDayPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vTimeSelectorDayPanel.py,v 1.2 2006/02/14 12:26:43 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors
import calendar,time
import string,math

#import boa.apps.xmlBase.vtXmlDomTree as vtXmlDomTree

BUFFERED=1

# defined event for vgpXmlTree item selected
#wxEVT_VGPPRRJ_TIME_SUMMARY_SELECTED=wx.NewEventType()
#def EVT_VGPPRRJ_TIME_SUMMARY_SELECTED(win,func):
#    win.Connect(-1,-1,wxEVT_VGPPRRJ_TIME_SUMMARY_SELECTED,func)
#class vgTimeSelectorDay(wx.PyEvent):
#    """
#    Posted Events:
#        Project Info changed event
#            EVT_VGPPRRJ_TIME_SUMMARY_SELECTED(<widget_name>, self.OnInfoSelected)
#    """

#    def __init__(self,obj,node):
#        wx.PyEvent.__init__(self)
#        self.SetEventType(wxEVT_VGPPRRJ_TIME_SUMMARY_SELECTED)
#        self.obj=obj
#        self.node=node
#    def GetVgpPrjTimeSummary(self):
#        return self.obj
#    def GetNode(self):
#        return self.node

class vTimeSelectorDayPanel(wx.Panel):
    def _init_ctrls(self, prnt, id, pos, size, style,name=u'vgTmSelectorDay'):
        # generated method, don't edit
        print size
        wx.Panel.__init__(self, id=id,
              name=name, parent=prnt, pos=pos,size=size, style=style)
        #self.SetClientSize(wx.Size(492, 273))
        self.SetAutoLayout(True)
        self.Bind(wx.EVT_SIZE, self.OnVgpPrjTimeSummarySize)

        self.scwDrawing = wx.ScrolledWindow(id=wx.NewId(),
              name=u'scwDay', parent=self, pos=wx.Point(0, 0),
              size=size, style=wx.HSCROLL | wx.VSCROLL)
        self.scwDrawing.SetConstraints(LayoutAnchors(self.scwDrawing, True, True,
              True, True))
        self.scwDrawing.SetAutoLayout(False)

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent,id,pos,size,style,name)
        self.doc=None
        
        self.__setDftColors__()
        self.lst=None
        self.drawing=False
        self.selectableBlks=[]
        self.selectedBlk=None
        self.processedPrj=[]
        
        self.gridHr=15
        self.maxHrs=1
        self.ofsX=35
        self.gridX=self.gridHr*self.maxHrs
        #def grid y
        
        self.gridY=35
        self.countWidth=25
        self.countHeight=1
        self.countSum=1
        
        self.sumX=60
        self.legendX=80
        self.maxWidth=self.gridX*self.countWidth+self.ofsX+self.sumX+self.legendX
        self.maxHeight=self.gridY*self.countHeight
        self.scwDrawing.SetVirtualSize((self.maxWidth+5, self.maxHeight+5))
        self.scwDrawing.SetScrollRate(20,20)
        if BUFFERED:
            # Initialize the buffer bitmap.  No real DC is needed at this point.
            self.buffer = wx.EmptyBitmap(self.maxWidth+1, self.maxHeight+1)
            dc = wx.BufferedDC(None, self.buffer)
            dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
            dc.Clear()
            self.DoDrawing(dc)
        self.scwDrawing.Bind(wx.EVT_LEFT_DOWN, self.OnLeftButtonEvent)
        self.scwDrawing.Bind(wx.EVT_LEFT_UP,   self.OnLeftButtonEvent)
        self.scwDrawing.Bind(wx.EVT_MOTION,    self.OnLeftButtonEvent)
        
        self.scwDrawing.Bind(wx.EVT_PAINT, self.OnPaint)
    def __setDftColors__(self):
        self.color=[]
        self.color.append(wx.Colour(165, 145, 182))
        self.color.append(wx.Colour(169, 121, 165))
        self.color.append(wx.Colour(231, 125, 107))
        self.color.append(wx.Colour(255, 154, 000))
        self.color.append(wx.Colour(255, 207, 000))
        self.color.append(wx.Colour(25, 25, 112))
        self.color.append(wx.Colour(123, 0x20, 0x8F))
        self.color.append(wx.Colour(147, 112, 219))
        self.color.append(wx.Colour(186, 85, 211))
        self.color.append(wx.Colour(102, 205, 170))
        self.color.append(wx.Colour(176, 196, 222))
        self.color.append(wx.Colour(119, 136, 153))
        self.color.append(wx.Colour(144, 238, 144))
        self.color.append(wx.Colour(173, 216, 230))
        self.color.append(wx.Colour(75, 000, 130))
        self.color.append(wx.Colour(205, 92, 92))
        self.color.append(wx.Colour(218, 165, 32))
        self.color.append(wx.Colour(34, 139, 34))
        self.color.append(wx.Colour(178, 34, 34))
        self.color.append(wx.Colour(148, 000, 211))
        self.color.append(wx.Colour(47, 79, 79))
        self.color.append(wx.Colour(72, 61, 139))
        self.color.append(wx.Colour(85, 107, 47))
        self.color.append(wx.Colour(210, 105, 30))
        self.color.append(wx.Colour(000, 000, 128))
        self.color.append(wx.Colour(128, 128, 000))
        self.color.append(wx.Colour(218, 112, 214))
        self.color.append(wx.Colour(46, 139, 87))
        self.color.append(wx.Colour(192, 192, 192))
        self.color.append(wx.Colour(106, 90, 205))
        self.__setDftBrush__()
        self.__setDftPen__()
    def __setDftBrush__(self):
        self.brushPrj=[]
        for c in self.color:
            self.brushPrj.append(wx.Brush(c))
    def __setDftPen__(self):
        self.penPrj=[]
        for c in self.color:
            self.penPrj.append(wx.Pen(c,1))
    def SetXY(self, event):
        self.x, self.y = self.ConvertEventCoords(event)

    def ConvertEventCoords(self, event):
        xView, yView = self.scwDrawing.GetViewStart()
        xDelta, yDelta = self.scwDrawing.GetScrollPixelsPerUnit()
        return (event.GetX() + (xView * xDelta),
                event.GetY() + (yView * yDelta))
    def SelectNode(self,node):
        if BUFFERED:
            # If doing buffered drawing, create the buffered DC, giving it
            # it a real DC to blit to when done.
            cdc = wx.ClientDC(self.scwDrawing)
            self.PrepareDC(cdc)
            dc = wx.BufferedDC(cdc, self.buffer)
            dc.BeginDrawing()
        else:
            dc = wx.ClientDC(self)
            self.PrepareDC(dc)

        if self.selectedBlk is not None:
             # paint old
             oBlk=self.selectedBlk
             indexColor=self.processedPrj.index([oBlk[-2]])
             color=self.color[indexColor]
             brush=self.brushPrj[indexColor]
             dc.SetBrush(brush)
             dc.DrawRectangle(oBlk[0], oBlk[1], 
                                oBlk[2]-oBlk[0], oBlk[3]-oBlk[1])
        self.selectedBlk=None
        for blk in self.selectableBlks:
            if blk[-1]==node:                                
                    dc.SetPen(wx.Pen('BLUE', 1))
                    dc.SetBrush(wx.LIGHT_GREY_BRUSH)
                    dc.DrawRectangle(blk[0], blk[1], 
                                blk[2]-blk[0], blk[3]-blk[1])
                    self.selectedBlk=blk
                    break
                    #wx.PostEvent(self,vgpPrjTimeSummarySelected(self,
                    #                self.selectedBlk[-1]))
        #self.selectedBlk=node
        dc.EndDrawing()
                        
    def OnLeftButtonEvent(self, event):
        if event.LeftDown():
            self.SetFocus()
            self.SetXY(event)
            #self.curLine = []
            #self.CaptureMouse()
            self.drawing = True
            for blks in self.selectableBlks:
                if (self.x>blks[0]) and (self.x<blks[2]):
                    if (self.y>blks[1]) and (self.y<blks[3]):
                        #print "found",blks[4]
                        if BUFFERED:
                            # If doing buffered drawing, create the buffered DC, giving it
                            # it a real DC to blit to when done.
                            cdc = wx.ClientDC(self.scwDrawing)
                            self.PrepareDC(cdc)
                            dc = wx.BufferedDC(cdc, self.buffer)
                            dc.BeginDrawing()
                            if self.selectedBlk is not None:
                                # paint old
                                oBlk=self.selectedBlk
                                indexColor=self.processedPrj.index([oBlk[-2]])
                                color=self.color[indexColor]
                                brush=self.brushPrj[indexColor]
                                dc.SetBrush(brush)
                                
                                dc.DrawRectangle(oBlk[0], oBlk[1], 
                                    oBlk[2]-oBlk[0], oBlk[3]-oBlk[1])
                            dc.SetPen(wx.Pen('BLUE', 1))
                            dc.SetBrush(wx.LIGHT_GREY_BRUSH)
                            dc.DrawRectangle(blks[0], blks[1], 
                                    blks[2]-blks[0], blks[3]-blks[1])
                            dc.EndDrawing()
                            self.selectedBlk=blks
                            wx.PostEvent(self,vgpPrjTimeSummarySelected(self,
                                    self.selectedBlk[-1]))
                            #self.scwDrawing.Refresh()
            pass
        elif event.Dragging() and self.drawing:
            if BUFFERED:
                # If doing buffered drawing, create the buffered DC, giving it
                # it a real DC to blit to when done.
                cdc = wx.ClientDC(self)
                self.PrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
            else:
                dc = wx.ClientDC(self)
                self.PrepareDC(dc)

            #dc.BeginDrawing()
            #dc.SetPen(wx.Pen('MEDIUM FOREST GREEN', 4))
            #coords = (self.x, self.y) + self.ConvertEventCoords(event)
            #self.curLine.append(coords)
            #dc.DrawLine(*coords)
            #self.SetXY(event)
            #dc.EndDrawing()


        elif event.LeftUp() and self.drawing:
            #self.lines.append(self.curLine)
            #self.curLine = []
            #self.ReleaseMouse()
            self.drawing = False
            self.scwDrawing.Refresh()
            #xView, yView = self.scwDrawing.GetViewStart()
            #xDelta, yDelta = self.scwDrawing.GetScrollPixelsPerUnit()
            #print xView,yView,xDelta,yDelta
            #s=self.scwDrawing.GetSize()
            #self.scwDrawing.RefreshRect(wx.Rect(xView*xDelta,yView*yDelta,
            #        s[0],s[1]))
            pass

    def OnPaint(self, event):
        if BUFFERED:
            # Create a buffered paint DC.  It will create the real
            # wx.PaintDC and then blit the bitmap to it when dc is
            # deleted.  Since we don't need to draw anything else
            # here that's all there is to it.
            dc = wx.BufferedPaintDC(self.scwDrawing, self.buffer, wx.BUFFER_VIRTUAL_AREA)
        else:
            dc = wx.PaintDC(self.scwDrawing)
            self.PrepareDC(dc)
            # since we're not buffering in this case, we have to
            # paint the whole window, potentially very time consuming.
            self.DoDrawing(dc)


    def DoDrawing(self, dc, printing=False):
        dc.BeginDrawing()
        self.__drawGrid__(dc,printing=printing)
        self.__drawValues__(dc,printing=printing)
        self.__drawGridText__(dc,printing=printing)
        self.__drawLegend__(dc,printing=printing)
        dc.EndDrawing()

    def oldDoDrawing(self):
        dc.SetPen(wx.Pen('RED'))
        dc.DrawRectangle(5, 5, 50, 50)

        dc.SetBrush(wx.LIGHT_GREY_BRUSH)
        dc.SetPen(wx.Pen('BLUE', 4))
        dc.DrawRectangle(15, 15, 50, 50)

        dc.SetFont(wx.Font(14, wx.SWISS, wx.NORMAL, wx.NORMAL))
        dc.SetTextForeground(wx.Colour(0xFF, 0x20, 0xFF))
        te = dc.GetTextExtent("Hello World")
        dc.DrawText("Hello World", 60, 65)

        dc.SetPen(wx.Pen('VIOLET', 4))
        dc.DrawLine(5, 65+te[1], 60+te[0], 65+te[1])

        lst = [(100,110), (150,110), (150,160), (100,160)]
        dc.DrawLines(lst, -60)
        dc.SetPen(wx.GREY_PEN)
        dc.DrawPolygon(lst, 75)
        dc.SetPen(wx.GREEN_PEN)
        dc.DrawSpline(lst+[(100,100)])

        #dc.DrawBitmap(self.bmp, 200, 20, True)
        #dc.SetTextForeground(wx.Colour(0, 0xFF, 0x80))
        #dc.DrawText("a bitmap", 200, 85)

##         dc.SetFont(wx.Font(14, wx.SWISS, wx.NORMAL, wx.NORMAL))
##         dc.SetTextForeground("BLACK")
##         dc.DrawText("TEST this STRING", 10, 200)
##         print dc.GetFullTextExtent("TEST this STRING")

        font = wx.Font(20, wx.SWISS, wx.NORMAL, wx.NORMAL)
        dc.SetFont(font)
        dc.SetTextForeground(wx.BLACK)

        for a in range(0, 360, 45):
            dc.DrawRotatedText("Rotated text...", 300, 300, a)

        dc.SetPen(wx.TRANSPARENT_PEN)
        dc.SetBrush(wx.BLUE_BRUSH)
        dc.DrawRectangle(50,500, 50,50)
        dc.DrawRectangle(100,500, 50,50)

        dc.SetPen(wx.Pen('RED'))
        dc.DrawEllipticArc(200,500, 50,75, 0, 90)

        if not printing:
            # This has troubles when used on a print preview in wxGTK,
            # probably something to do with the pen styles and the scaling
            # it does...
            y = 20

            for style in [wx.DOT, wx.LONG_DASH, wx.SHORT_DASH, wx.DOT_DASH, wx.USER_DASH]:
                pen = wx.Pen("DARK ORCHID", 1, style)
                if style == wx.USER_DASH:
                    pen.SetCap(wx.CAP_BUTT)
                    pen.SetDashes([1,2])
                    pen.SetColour("RED")
                dc.SetPen(pen)
                dc.DrawLine(300,y, 400,y)
                y = y + 10

        dc.SetBrush(wx.TRANSPARENT_BRUSH)
        dc.SetPen(wx.Pen(wx.Colour(0xFF, 0x20, 0xFF), 1, wx.SOLID))
        dc.DrawRectangle(450,50,  100,100)
        old_pen = dc.GetPen()
        new_pen = wx.Pen("BLACK", 5)
        dc.SetPen(new_pen)
        dc.DrawRectangle(470,70,  60,60)
        dc.SetPen(old_pen)
        dc.DrawRectangle(490,90, 20,20)

        #self.DrawSavedLines(dc)
        dc.EndDrawing()


    def __drawGrid__(self,dc,printing):
        dc.SetPen(wx.Pen('VIOLET', 1))
        
        for i in range(0,self.countHeight-self.countSum+1):
            y=i*self.gridY
            dc.DrawLine(0,y,self.maxWidth-self.legendX,y)
        dc.DrawLine(0,0,0,self.maxHeight-self.gridY*self.countSum)
        dc.DrawLine(self.ofsX,y,self.ofsX,self.maxHeight-self.gridY*self.countSum)
        for i in range(1,self.countWidth+1):
            x=(i-1)*self.gridX+self.ofsX
            if i==0 or i==self.countWidth:
                y=0
            else:
                y=0
                #y=self.gridY
            dc.DrawLine(x,y,x,self.maxHeight-self.gridY*self.countSum)
        dc.DrawLine(x+self.sumX+self.gridX,
                    y,
                    x+self.sumX+self.gridX,
                    self.maxHeight-self.gridY*self.countSum)
        
        try:
            days=calendar.monthcalendar(self.year,self.month)
        except:
            t=time.localtime(time.time())
            days=calendar.monthcalendar(t[0],t[1])
            #print "uupp"
        #print self.year,self.month,days
        #dc.SetFont(wx.Font(8, wx.SWISS, wx.NORMAL, wx.NORMAL))
        #dc.SetTextForeground(wx.Colour(0x8F, 0x20, 0x8F))
        y=0
        #y=self.gridY
        gXh=self.gridX/2
        gYh=self.gridY/2
        #for i in range(0,7):
        #    sDay=calendar.day_abbr[i]
        #    te = dc.GetTextExtent(sDay)
        #    dc.DrawText(sDay, i*self.gridX +gXh-te[0]/2, y+gYh-te[1]/2)
        self.coorDay={}
        self.ofsDay={}
        for w in days:
            y=y+self.gridY
            i=0
            for d in w:
                if d>0:
                    sDay="%d"%d
                    self.coorDay[d]=(i*self.gridX,y)
                    self.ofsDay[d]=(0,0)
                    #te = dc.GetTextExtent(sDay)
                    #dc.DrawText(sDay, i*self.gridX +gXh-te[0]/2, y+gYh-te[1]/2)
                i=i+1
            #self.countWidth
        #dc.DrawLine(5, 65+te[1], 60+te[0], 65+te[1])

        #lst = [(100,110), (150,110), (150,160), (100,160)]
        #dc.DrawLines(lst,0)
        pass
    def __drawGridText__(self,dc,printing):
        dc.SetPen(wx.Pen('BLACK', 1))
        try:
            days=calendar.monthcalendar(self.year,self.month)
        except:
            t=time.localtime(time.time())
            days=calendar.monthcalendar(t[0],t[1])
            
        y=0
        #y=self.gridY
        gXh=self.gridX/2
        gYh=self.gridY/2
        #for i in range(0,7):
        #    sDay=calendar.day_abbr[i]
        #    te = dc.GetTextExtent(sDay)
        #    dc.DrawText(sDay, 0 +gXh-te[0]/2, i*self.gridY+gYh-te[1]/2)
        dc.SetFont(wx.Font(7, wx.SWISS, wx.NORMAL, wx.NORMAL))
        dc.SetTextForeground(wx.Colour(0x0, 0x0, 0x0))
        for i in range(1,self.countWidth):
            sDay="%02d"%(i-1)
            #self.coorDay[d]=(i*self.gridX,y)
            #self.ofsDay[d]=(8,0)
            te = dc.GetTextExtent(sDay)
            dc.DrawText(sDay, self.ofsX+(i-1)*self.gridX +gXh-te[0]/2, gYh-te[1]/2+1)
        sTxt="Sum"
        te = dc.GetTextExtent(sTxt)
        dc.DrawText(sTxt,self.gridX*self.countWidth+self.ofsX+gXh-te[0]/2,gYh-te[1]/2+1)
        sTxt="Legend"
        te = dc.GetTextExtent(sTxt)
        dc.DrawText(sTxt,self.gridX*self.countWidth+self.ofsX+self.sumX+gXh,gYh-te[1]/2+1)
                    
        for w in days:
            y=y+self.gridY
            i=0
            for d in w:
                if d>0:
                    sDay=calendar.day_abbr[i]+"  %02d"%d
                    #self.coorDay[d]=(i*self.gridX,y)
                    #self.ofsDay[d]=(8,0)
                    te = dc.GetTextExtent(sDay)
                    #dc.DrawText(sDay, 0 +gXh-te[0]/2, d*self.gridY+gYh-te[1]/2)
                    
                    sDay=calendar.day_abbr[i]
                    dc.DrawText(sDay, 2, d*self.gridY+gYh-te[1]/2+1)
                    sDay="%02d"%d
                    te = dc.GetTextExtent(sDay)
                    dc.DrawText(sDay, self.ofsX - te[0]-2, d*self.gridY+gYh-te[1]/2+1)
                i=i+1
    def __drawLegend__(self,dc,printing):
        x=self.gridX*self.countWidth+self.ofsX+self.sumX#+self.gridX/2
        y=self.gridY
        i=0
        prjNames=[]
        for prj in self.processedPrj:
            prjNames.append(prj[0])
        prjNames.sort()
        #for prj in self.processedPrj:
        i=0
        dc.SetFont(wx.Font(7, wx.SWISS, wx.NORMAL, wx.NORMAL))
        dc.SetTextForeground(wx.Colour(0x0, 0x0, 0x0))
        #y=y+self.gridY
        #for prj in self.processedPrj:
        for prjName in prjNames:
            #sTxt=prj[0]
            #te = dc.GetTextExtent(prjName)
            dc.DrawText(prjName,x+2,y+2)
            y=y+self.gridY
            i=i+1
        y=self.gridY
        w=self.legendX/4-2
        xa=x+self.legendX-w
        for prjName in prjNames:
            indexColor=self.processedPrj.index([prjName])
            color=self.color[indexColor]
            brush=self.brushPrj[indexColor]
            dc.SetBrush(brush)
            dc.DrawRectangle(xa, y, w, self.gridY)
            y=y+self.gridY
            i=i+1
        
    def __getCoor__(self,day):
        try:
            return self.coorDay[day]
        except:
            return (-1,-1)
    def __getOfsDay__(self,day):
        try:
            return self.ofsDay[day]
        except:
            return -1
    def SetYearMonth(self,year,month):
        self.year=year
        self.month=month
        if BUFFERED:
            # Initialize the buffer bitmap.  No real DC is needed at this point.
            self.buffer = wx.EmptyBitmap(self.maxWidth+2, self.maxHeight+2)
            dc = wx.BufferedDC(None, self.buffer)
            dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
            dc.Clear()
            self.DoDrawing(dc)
        
        pass
    def SetDate(self,dt):
        if BUFFERED:
            # Initialize the buffer bitmap.  No real DC is needed at this point.
            self.buffer = wx.EmptyBitmap(self.maxWidth, self.maxHeight)
            dc = wx.BufferedDC(None, self.buffer)
            dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
            dc.Clear()
            self.DoDrawing(dc)
        
        pass
    def SetValue(self,lst,actNode=None,year=None,month=None):
        if year is not None:
            self.year=year
        if month is not None:
            self.month=month
        self.lst=lst
        if BUFFERED:
            # Initialize the buffer bitmap.  No real DC is needed at this point.
            self.buffer = wx.EmptyBitmap(self.maxWidth+2, self.maxHeight+2)
            dc = wx.BufferedDC(None, self.buffer)
            dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
            dc.Clear()
            self.DoDrawing(dc)
        if actNode is not None:
            for blk in self.selectableBlks:
                if blk[-1]==actNode:                                
                    dc.SetPen(wx.Pen('BLUE', 1))
                    dc.SetBrush(wx.LIGHT_GREY_BRUSH)
                    dc.DrawRectangle(blk[0], blk[1], 
                                blk[2]-blk[0], blk[3]-blk[1])
                    self.selectedBlk=blk
                    break
        else:
            self.selectedBlk=None
        self.scwDrawing.Refresh()
        #self.OnPaint()
    def GetValue(self):
        return self.lst
    def GetYear(self):
        return self.year
    def GetMonth(self):
        return self.month
    def __drawValues__(self,dc,printing):
        if self.lst is None:
            return
        self.selectableBlks=[]
        self.processedPrj=[]
        sumTotal=0
        for item in self.lst:
            sum=0
            
            coor=self.__getCoor__(item[0])
            ofs=self.__getOfsDay__(item[0])
            ofsHr=ofs[0]
            ofsPrj=ofs[1]
            i=0
            coor=(self.ofsX,self.gridY*item[0])
            yem=y=coor[1]
            drwPrj={}
            for prj in item[1]:
                # assign color first
                try:
                    indexColor=self.processedPrj.index([prj[0]])
                except:
                    indexColor=len(self.processedPrj)
                    self.processedPrj.append([prj[0]])
                try:
                    color=self.color[indexColor]
                    brush=self.brushPrj[indexColor]
                    pen=self.penPrj[indexColor]
                    #dc.SetPen(pen)
                    dc.SetPen(wx.Pen('BLACK', 1))
                    dc.SetBrush(brush)
                except:
                    dc.SetPen(wx.Pen('BLUE', 1))
                    dc.SetBrush(wx.LIGHT_GREY_BRUSH)
                hr=string.atoi(prj[1][:2])
                min=string.atoi(prj[1][2:4])
                sHr=hr+min/60.0
                min=round(min/5.0)
                xa=coor[0]
                if hr>=ofsHr:
                    xa=(hr-ofsHr)*self.gridHr+(min*self.gridHr/12.0)
                if xa<0:
                    xa=0
                hr=string.atoi(prj[2][:2])
                min=string.atoi(prj[2][2:4])
                eHr=hr+min/60.0
                min=round(min/5.0)
                if sHr<eHr:
                    sum=sum+(eHr-sHr)
                else:
                    sum=sum+(sHr-eHr)
                xe=coor[0]
                if hr>=ofsHr:
                    xe=(hr-ofsHr)*self.gridHr+(min*self.gridHr/12.0)
                #dc.SetPen(wx.Pen('BLUE', 1))
                #dc.SetBrush(wx.LIGHT_GREY_BRUSH)
                #if self.gridX <=xe:
                #    xe=self.gridX
                dc.DrawRectangle(coor[0]+xa, y, xe-xa, self.gridY)
                self.selectableBlks.append((coor[0]+xa,y,coor[0]+xe,y+self.gridY,prj[0],prj[-1]))
            dc.SetFont(wx.Font(7, wx.SWISS, wx.NORMAL, wx.NORMAL))
            dc.SetTextForeground(wx.Colour(0x0, 0x0, 0x0))
            sTxt="""%4.2f  %02d"%02d'"""%(sum,math.floor(sum),round(sum%1*60))
            te = dc.GetTextExtent(sTxt)
            xS=self.gridX*self.countWidth+self.ofsX+self.sumX#+self.gridX/2
            #y=self.gridY
            dc.DrawText(sTxt,xS-2-te[0],y+self.gridY/2-te[1]/2+1)
            sumTotal=sumTotal+sum
        dc.SetFont(wx.Font(8, wx.SWISS, wx.NORMAL, wx.BOLD))
        dc.SetTextForeground(wx.Colour(0x0, 0x0, 0x0))
        sum=sumTotal
        sTxt="""%5.2f'"""%(sum)
        te = dc.GetTextExtent(sTxt)
        xS=self.gridX*self.countWidth+self.ofsX+self.sumX#+self.gridX/2
        y=self.gridY*self.countHeight-self.gridY-self.gridY
        dc.DrawText(sTxt,xS-2-te[0],y+self.gridY/2-te[1]/2+1)
        
        sTxt="""%03d"%02d'"""%(math.floor(sum),round(sum%1*60))
        te = dc.GetTextExtent(sTxt)
        xS=self.gridX*self.countWidth+self.ofsX+self.sumX#+self.gridX/2
        y=self.gridY*self.countHeight-self.gridY
        dc.DrawText(sTxt,xS-2-te[0],y+self.gridY/2-te[1]/2+1)
                
        sTxt="""%s:  %5.2f    %03d"%02d'"""%('Total',sum,math.floor(sum),round(sum%1*60))
        te = dc.GetTextExtent(sTxt)
        xS=2
        y=self.gridY*self.countHeight-self.gridY
        dc.DrawText(sTxt,xS-2,y+self.gridY/2-te[1]/2+1)
        
                    
    def __convertNode2InterVal__(self,node):
        sPrj=self.doc.getNodeText(node,'project')
        #sTask=self.doc.getNodeText(node,'task')
        #sSubTask=self.doc.getNodeText(node,'subtask')
        #sLoc=self.doc.getNodeText(node,'loc')
        sPerson=self.doc.getNodeText(node,'person')
        #sDesc=self.doc.getNodeText(node,'desc')
        sDate=self.doc.getNodeText(node,'date')
        sStartTime=self.doc.getNodeText(node,'starttime')
        sEndTime=self.doc.getNodeText(node,'endtime')
        try:
            i=string.atoi(sStartTime[0])
            i=string.atoi(sEndTime[0])
        except:
            return None
        try:
            val=(string.atoi(sDate[-2:]),[sPrj,sStartTime,sEndTime,sPerson,node])
        except:
            val=None
        return val
    def SetDoc(self,doc,bNet=False):
        self.doc=doc
    def SetNodes(self,nodes,actNode=None):
        now=time.localtime(time.time())
        year=now[0]
        month=now[1]
        firstNode=None
        self.selectedBlk=None
        lst=[]
        d={}
        if self.doc is None:
            self.SetValue(lst,actNode,year,month)
        
        for n in nodes:
            if firstNode is None:
                firstNode=n
                try:
                    sDate=self.doc.getNodeText(firstNode,'date')
                    year=string.atoi(sDate[:4])
                    month=string.atoi(sDate[4:6])
                except:
                    firstNode=None
            intVal=self.__convertNode2InterVal__(n)
            if intVal is None:
                continue
            try:
                v=d[intVal[0]]
                v[1].append(intVal[1])
            except:
                d[intVal[0]]=(intVal[0],[intVal[1]])
        keys=d.keys()
        keys.sort()
        for k in keys:
            lst.append(d[k])
        self.SetValue(lst,actNode,year,month)

    def OnVgpPrjTimeSummarySize(self, event):
        s=event.GetSize()
        self.scwDrawing.SetSize(s)
        event.Skip()
