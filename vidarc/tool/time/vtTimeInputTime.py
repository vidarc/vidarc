#----------------------------------------------------------------------------
# Name:         vtTimeInputTime.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060214
# CVS-ID:       $Id: vtTimeInputTime.py,v 1.8 2008/03/26 23:12:09 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from vidarc.tool.draw.vtDrawCanvasPanel import vtDrawCanvasPanel
from vidarc.tool.draw.vtDrawCanvasPanel import EVT_VTDRAW_CANVAS_ITEM_ACTIVATED
from vidarc.tool.draw.vtDrawCanvasPanel import EVT_VTDRAW_CANVAS_ITEM_DEACTIVATED
from vidarc.tool.draw.vtDrawCanvasObjectBase import *
from vidarc.tool.draw.vtDrawCanvasGroup import *
from vidarc.tool.draw.vtDrawCanvasRectangle import *
from vidarc.tool.draw.vtDrawCanvasRectangleFilled import *
from vidarc.tool.draw.vtDrawCanvasText import *
import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.time.vtTime import vtTime

from vidarc.tool.xml.vtXmlDomConsumer import *

wxEVT_VTTIME_INPUT_TIME_CHANGED=wx.NewEventType()
vEVT_VTTIME_INPUT_TIME_CHANGED=wx.PyEventBinder(wxEVT_VTTIME_INPUT_TIME_CHANGED,1)
def EVT_VTTIME_INPUT_TIME_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTTIME_INPUT_TIME_CHANGED,func)
class vtTimeInputTimeChanged(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VTTIME_INPUT_TIME_CHANGED(<widget_name>, self.OnItemSel)
    """

    def __init__(self,obj,iHrS,iMnS):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTTIME_INPUT_TIME_CHANGED)
        self.iHrS=iHrS
        if iMnS<0:
            iMnS=-1
        self.iMnS=iMnS
    def GetCanvas(self):
        return self.obj
    def GetHourStart(self):
        return self.iHrS
    def GetMinStart(self):
        return self.iMnS
    def GetTime(self):
        return self.iHrS,self.iMnS
    def IsValid(self):
        if self.iHrS<0 or self.iMnS<0:
            return False
        return True
    

class vtTimeHr(vtDrawCanvasGroup):
    def __init__(self,id=-1,name='',text='',x=0,y=0,w=0,h=0,layer=0):
        vtDrawCanvasGroup.__init__(self,id,name,x,y,w,h,layer)
        obj=vtDrawCanvasRectangle(id=-1,name=name+'_rct',
                        x=0,y=0,w=w,h=h,layer=layer)
        self.Add(obj)
        obj=vtDrawCanvasRectangleFilled(id=-1,name=name+'_rcf',
                        x=0,y=0,w=w,h=h,layer=layer)
        obj.SetVisible(False)
        self.Add(obj)
        obj=vtDrawCanvasText(id=-1,name=name+'_txt',text=text,
                        x=0,y=0,w=w,h=h,layer=layer)
        self.Add(obj)
    def __draw__(self,canvas,aX=0,aY=0,aW=-1,aH=-1,iDriver=-1):
        if self.bActive:
            self.objs[1].SetVisible(True)
            #self.objs[1].SetLayer(self.GetLayer())
            self.objs[2].SetLayer(-1)
        else:
            self.objs[1].SetVisible(False)
            self.objs[2].SetLayer(self.GetLayer())
            
        vtDrawCanvasGroup.__draw__(self,canvas,aX,aY,aW,aH,iDriver)
        
        #if self.bActive:
        #    self.objs[1].SetFocus(True)
            #self.objs[1].SetLayer(-1)
        #    self.objs[1].__drawSelect__(canvas,aX,aY,aW,aH,iDriver)
        #    self.objs[1].SetFocus(False)
        #else:
        #    self.objs[0].SetActive(True)
        #    self.objs[0].__drawSelect__(canvas,aX,aY,aW,aH,iDriver)
        self.SetSelected(False)
        if self.bActive:
            iLayer=self.GetLayer()
            self.SetLayer(-1)
        self.__drawSelect__(canvas,aX,aY,aW,aH,iDriver)
        if self.bActive:
            self.SetLayer(iLayer)
        #self.txt.__draw__(canvas,aX,aY,aW,aH,iDriver)

class vtTimeInputTime(vtDrawCanvasPanel):
    def __init__(self, parent, 
                id=wx.NewId(), pos=wx.DefaultPosition, size=wx.Size(100, 50), 
                style=wx.TAB_TRAVERSAL, name=u'vtTimeInputTime',
                draw_grid=True,small=False,blks_per_line=12,
                start_hour=0, end_hour=24,min_tolerance=5):
        self.iHrStart=start_hour
        self.iHrEnd=end_hour
        self.zTime=vtTime()
        self.zTime.Clear()
        self.iHrS=-1
        self.iMnS=-1
        self.iMinTol=min_tolerance
        self.iMin=60/min_tolerance
        self.tagName='time'
        
        vtDrawCanvasPanel.__init__(self,parent,id,pos,size,style,name,
                    draw_grid,max_x=12,max_y=4)
        
        self.SetActivateOnSelect(True)
        
        obj=vtDrawCanvasObjectBase()
        self.RegisterObject(vtTimeHr())
        x=0
        y=0
        for i in range(self.iHrStart,self.iHrEnd):
            self.Add(vtTimeHr(id=i,name='hr_%02d'%i,text='%02d'%i,
                            x=x,y=y,w=1,h=1))
            x+=1
            if x%blks_per_line==0:
                x=0
                y+=1
        if x!=0:
            y+=1
        x=0
        for i in range(0,self.iMin):
            self.Add(vtTimeHr(id=100+i,name='mn_%02d'%i,text='%02d'%(i*self.iMinTol),
                            x=x,y=y,w=1,h=1,layer=3))
            x+=1
            if x%blks_per_line==0:
                x=0
                y+=1
        if x==0:
            y-=1
            
        if small:
            grid=(15,15)
            i=blks_per_line
        else:
            grid=(20,20)
            i=blks_per_line
        self.SetConfig(obj,obj.DRV_WX,max=(i,y+1),
                    grid=grid)
        EVT_VTDRAW_CANVAS_ITEM_ACTIVATED(self,self.OnActivated)
        EVT_VTDRAW_CANVAS_ITEM_DEACTIVATED(self,self.OnDeActivated)
    def SetTagNames(self,tagName):
        self.tagName=tagName
    def OnActivated(self,evt):
        blk=evt.GetBlock()
        sName=blk.GetName()
        self.BeginBatch()
        if sName[:3]=='hr_':
            iHr=int(sName[3:5])
            if self.iHrS==-1:
                self.iHrS=iHr
                #self.iMnS=-1
            else:
                if iHr!=self.iHrS:
                    # select other
                    start=[]
                    blk=self.FindByAttr(self.iHrS,start,'id')
                    if blk is not None:
                        blk.SetActive(False)
                        self.ShowObj(blk,False)
                    self.iHrS=iHr
        elif sName[:3]=='mn_':
            ID=blk.GetId()
            if ID<200:
                min=ID-100
                start=[]
                blk=self.FindByAttr(100+self.iMnS,start,'id')
                if blk is not None:
                    blk.SetActive(False)
                    self.ShowObj(blk,False)
                self.iMnS=min
        self.zTime.Set(self.iHrS,self.iMnS*self.iMinTol)
        wx.PostEvent(self,vtTimeInputTimeChanged(self,
                                self.iHrS,self.iMnS*self.iMinTol))
        self.EndBatch()
        evt.Skip()
    def OnDeActivated(self,evt):
        blk=evt.GetBlock()
        sName=blk.GetName()
        self.BeginBatch()
        if sName[:3]=='hr_':
            iHr=int(sName[3:5])
            if self.iHrS==iHr:
                blk.SetActive(False)
                self.ShowObj(blk,False)
                self.iHrS=-1
        elif sName[:3]=='mn_':
            ID=blk.GetId()
            if ID<200:
                min=ID-100
                start=[]
                blk=self.FindByAttr(100+self.iMnS,start,'id')
                if blk is not None:
                    blk.SetActive(False)
                    self.ShowObj(blk,False)
                self.iMnS=-1
        self.zTime.Set(self.iHrS,self.iMnS*self.iMinTol)
        wx.PostEvent(self,vtTimeInputTimeChanged(self,
                                self.iHrS,self.iMnS*self.iMinTol))
        self.EndBatch()
        evt.Skip()
    def OnLeftButtonEventO(self, event):
        vtDrawCanvasPanel.OnLeftButtonEvent(self,event)
        if self.selectedBlk is not None:
            self.selectedBlk.SetActive(not self.selectedBlk.GetActive())
    def Clear(self):
        self.node=None
    def ClearInt(self):
        self.zTime.Clear()
        self.iHrS=-1
        self.iMnS=-1
        self.__clearHrMn__()
    def __clearHrMn__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        start=[]
        self.BeginBatch()
        for id in range(self.iHrStart,self.iHrEnd):
            blk=self.FindByAttr(id,start,'id')
            if blk is not None:
                blk.SetActive(False)
                self.ShowObj(blk,False)
        start=[]
        for id in range(self.iMin):
            blk=self.FindByAttr(100+id,start,'id')
            if blk is not None:
                blk.SetActive(False)
                self.ShowObj(blk,False)
        self.EndBatch()
        
    def __selectHrMn__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.__clearHrMn__()
        self.zTime.Round(self.iMinTol)
        self.iHrS=self.zTime.GetHour()
        self.iMnS=self.zTime.GetMinute()/self.iMinTol
        self.BeginBatch()
        start=[]
        for id in range(self.iHrStart,self.iHrEnd):
            blk=self.FindByAttr(id,start,'id')
            if blk is not None:
                blk.SetActive(self.iHrS==id)
                self.ShowObj(blk,False)
        start=[]
        for id in range(self.iMin):
            blk=self.FindByAttr(100+id,start,'id')
            if blk is not None:
                blk.SetActive(id==self.iMnS)
                self.ShowObj(blk,False)
        self.EndBatch()
    def SetHour(self,hour):
        self.zTime.SetHour(hour)
        self.__selectHrMn__()
    def SetMinute(self,min):
        self.zTime.SetMinute(min)
        self.__selectHrMn__()
    def SetSecond(self,sec):
        self.zTime.SetSecond(sec)
        self.__selectHrMn__()
    def Set(self,hour,min=0,sec=0):
        self.zTime.Set(hour,min,sec)
        self.__selectHrMn__()
    def GetHour(self):
        return self.zTime.GetHour()
    def GetMinute(self):
        return self.zTime.GetMinute()
    def GetSecond(self):
        return self.zTime.GetSecond()
    def SetNode(self,node):
        self.ClearInt()
        self.node=node
        if self.doc is not None:
            sVal=self.doc.getNodeText(self.node,self.tagName)
            self.zTime.SetStr(sVal)
            self.__selectHrMn__()
    def GetNode(self,node=None):
        if node is None:
            node=self.node
        if self.doc is not None:
            if node is not None:
                sVal=self.zTime.GetStr()
                self.doc.setNodeText(self.node,self.tagName,sVal)
    def SetValue(self,sVal):
        if self.zTime.SetStr(sVal,':')<0:
            self.SetNow()
        else:
            self.__selectHrMn__()
    def GetValue(self):
        return self.zTime.GetStr(':')
    def SetNow(self):
        self.ClearInt()
        self.zTime.Now()
        self.zTime.Floor(self.iMinTol)
        self.__selectHrMn__()

