#----------------------------------------------------------------------------
# Name:         timeconverter.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: timeconvert.py,v 1.1 2005/12/11 23:04:49 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import string
import calendar

def convertStrTime2Time(str):
    try:
        s=(string.atoi(str[0:4]),string.atoi(str[4:6]),string.atoi(str[6:8]),
            string.atoi(str[9:11]),string.atoi(str[11:13]),string.atoi(str[13:15]),
            0,0,0)
        return calendar.timegm(s)
    except:
        return None
def calcDiff(start,end):
    try:
        diff=end-start
        ds=diff%60
        diff=diff/60
        dm=diff%60
        diff=diff/60
        dh=diff
        st = "%02d%02d%02d"%(dh,dm,ds)
        return st
    except:
        return None
