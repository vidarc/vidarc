#----------------------------------------------------------------------------
# Name:         vTimeTimeEdit.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vTimeTimeEdit.py,v 1.3 2006/04/19 19:40:45 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.masked.maskededit
import wx.lib.popupctl
import wx.lib.buttons
import cStringIO

import time
import calendar
import string
from timeconvert import *

#----------------------------------------------------------------------
def getCancelData():
    return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00lIDATx\x9c\xad\x93\xc1\r\xc00\x08\x03M\xb7\xe8\xfe\xc3e\x0c\xfa\x8bB\
bC$\xca\x9b;\x01N\x0cpG\xa3\x9e\x0e\xfc\xaf`\xc00`%\xb0\xf7M\xc1\x0b\x9f\r\
\x19\xbc\xf6\x06A%a\xf0!P\x12\x05\x03\x80\xa9\x18\xf7)\x18L'`\x80\x82S\x01[\
\xe1Z\xb0\xee\\\xa5s\x08\xd8\xc12I\x10d\xd7V\x92\xf0\x12\x15\x9cId\x8c\xb7\
\xd5\xfeL\x1f$\x07+\xb8\xd6Q\x0bp\x00\x00\x00\x00IEND\xaeB`\x82" 

def getCancelBitmap():
    return wx.BitmapFromImage(getCancelImage())

def getCancelImage():
    stream = cStringIO.StringIO(getCancelData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getDelData():
    return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x00\xe2IDAT(\x91\x85\x92Ar\x830\x0cE\x9f\x19.\x95N\xbb\x08\xcb\xe6F\xa0\xb2\
pEo\x94.\xc9\x82\x8es,u\xa1D1eH\xff0\x1a\xc9\xfa_\xdf6N`\xdc\xa1y\x8a\\\xc6\
\x81\x1d\x18\x98f\xd5\xacf\xe6\xd1\x13\xcd\xea\xdd\xd5\x17T'\x01e)e){\xb2\
\x16\x18>\x06`\xfa\xbc\xedg\xbe\xcc\xdd\xb1\xbb\xfe\\\xa3\x05Sl2i\xd6\xee\
\xd8\x01\x87\xd7\x03\x90Rr\x1f_tD\xeb\xe6\xe0\xf0\x91fV\xbb\x05;VZ\x19\x07\
\xcdS\xcc;\xbd\x9f\x80\xf3\xf79\x04\xb5\x18h\xebB\xbfTz\t\x99\xf42_\xe6?w\
\xda\xd4\xec\x88[\xc8(\xfe\xc7\x1e\x02\x9f\x1d\xd1\x93-\x1a@\xc6\xa1\xb6~n\
\xd5\xd4\x85\xf4\xf2\x9c\r\xa4\xfb[J@Y\n\xd5\xd1\x01w\x8e\x03P\xdd\x92\x01/o\
\x0f\xd9\x1e\xdau\xf9\xbf,\xd5\xcf{\xd3Z\rr\xfc\x02\x1fr\x8f\xf6KRW\xed\x00\
\x00\x00\x00IEND\xaeB`\x82" 

def getDelBitmap():
    return wx.BitmapFromImage(getDelImage())

def getDelImage():
    stream = cStringIO.StringIO(getDelData())
    return wx.ImageFromStream(stream)

# defined event for vgpXmlTree item selected
wxEVT_VTIME_TIMEEDIT_CHANGED=wx.NewEventType()
vEVT_VTIME_TIMEEDIT_CHANGED=wx.PyEventBinder(wxEVT_VTIME_TIMEEDIT_CHANGED,1)
def EVT_VTIMEEDIT_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTIME_TIMEEDIT_CHANGED,func)
class vTimeTimeEditChanged(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTIME_TIMEEDIT_CHANGED(<widget_name>, self.OnChanged)
    """

    def __init__(self,obj,text):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTIME_TIMEEDIT_CHANGED)
        self.text=text
    def GetText(self):
        return self.text
    
[wxID_WXTIMEEDIT, wxID_WXTIMEEDITwin, wxID_WXTIMEEDITTGHR00, 
 wxID_WXTIMEEDITTGHR01, wxID_WXTIMEEDITTGHR02, 
 wxID_WXTIMEEDITTGHR03, wxID_WXTIMEEDITTGHR04, 
 wxID_WXTIMEEDITTGHR05, wxID_WXTIMEEDITTGHR06, 
 wxID_WXTIMEEDITTGHR07, wxID_WXTIMEEDITTGHR08, 
 wxID_WXTIMEEDITTGHR09, wxID_WXTIMEEDITTGHR10, 
 wxID_WXTIMEEDITTGHR11, wxID_WXTIMEEDITTGHR12, 
 wxID_WXTIMEEDITTGHR13, wxID_WXTIMEEDITTGHR14, 
 wxID_WXTIMEEDITTGHR15, wxID_WXTIMEEDITTGHR16, 
 wxID_WXTIMEEDITTGHR17, wxID_WXTIMEEDITTGHR18, 
 wxID_WXTIMEEDITTGHR19, wxID_WXTIMEEDITTGHR20, 
 wxID_WXTIMEEDITTGHR21, wxID_WXTIMEEDITTGHR22, 
 wxID_WXTIMEEDITTGHR23, wxID_WXTIMEEDITTGMIN00, 
 wxID_WXTIMEEDITTGMIN05, wxID_WXTIMEEDITTGMIN10, 
 wxID_WXTIMEEDITTGMIN15, wxID_WXTIMEEDITTGMIN20, 
 wxID_WXTIMEEDITTGMIN25, wxID_WXTIMEEDITTGMIN30, 
 wxID_WXTIMEEDITTGMIN35, wxID_WXTIMEEDITTGMIN40, 
 wxID_WXTIMEEDITTGMIN45, wxID_WXTIMEEDITTGMIN50, 
 wxID_WXTIMEEDITTGMIN55, wxID_WXTIMEEDITGCBBCANCEL, 
 wxID_WXTIMEEDITGCBBDEL, 
] = map(lambda _init_ctrls: wx.NewId(), range(40))


class vTimeTimeEdit(wx.lib.popupctl.PopupControl):
    def __init__(self,*_args,**_kwargs):
        apply(wx.lib.popupctl.PopupControl.__init__,(self,) + _args,_kwargs)

        self.win = wx.Window(self,-1,pos = (2,2),style = 0)
        
        self.tgHr00 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR00, label=u'0',
              name=u'tgHr00', parent=self.win, pos=wx.Point(0, 0),
              size=wx.Size(30, 20), style=0)
        self.tgHr00.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr00, wxID_WXTIMEEDITTGHR00,
              self.OnTgHrTogglebutton)

        self.tgHr01 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR01, label=u'1',
              name=u'tgHr01', parent=self.win, pos=(30, 0),
              size=(30, 20), style=0)
        self.tgHr01.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr01, wxID_WXTIMEEDITTGHR01,
              self.OnTgHrTogglebutton)

        self.tgHr02 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR02, label=u'2',
              name=u'tgHr02', parent=self.win, pos=(60, 0),
              size=(30, 20), style=0)
        self.tgHr02.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr02, wxID_WXTIMEEDITTGHR02,
              self.OnTgHrTogglebutton)

        self.tgHr03 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR03, label=u'3',
              name=u'tgHr03', parent=self.win, pos=(90, 0),
              size=(30, 20), style=0)
        self.tgHr03.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr03, wxID_WXTIMEEDITTGHR03,
              self.OnTgHrTogglebutton)

        self.tgHr04 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR04, label=u'4',
              name=u'tgHr04', parent=self.win, pos=(120, 0),
              size=(30, 20), style=0)
        self.tgHr04.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr04, wxID_WXTIMEEDITTGHR04,
              self.OnTgHrTogglebutton)

        self.tgHr05 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR05, label=u'5',
              name=u'tgHr05', parent=self.win, pos=(150, 0),
              size=(30, 20), style=0)
        self.tgHr05.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr05, wxID_WXTIMEEDITTGHR05,
              self.OnTgHrTogglebutton)

        self.tgHr06 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR06, label=u'6',
              name=u'tgHr06', parent=self.win, pos=(180, 0),
              size=(30, 20), style=0)
        self.tgHr06.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr06, wxID_WXTIMEEDITTGHR06,
              self.OnTgHrTogglebutton)

        self.tgHr07 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR07, label=u'7',
              name=u'tgHr07', parent=self.win, pos=(210, 0),
              size=(30, 20), style=0)
        self.tgHr07.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr07, wxID_WXTIMEEDITTGHR07,
              self.OnTgHrTogglebutton)

        self.tgHr08 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR08, label=u'8',
              name=u'tgHr08', parent=self.win, pos=(240, 0),
              size=(30, 20), style=0)
        self.tgHr08.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr08, wxID_WXTIMEEDITTGHR08,
              self.OnTgHrTogglebutton)

        self.tgHr09 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR09, label=u'9',
              name=u'tgHr09', parent=self.win, pos=(270, 0),
              size=(30, 20), style=0)
        self.tgHr09.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr09, wxID_WXTIMEEDITTGHR09,
              self.OnTgHrTogglebutton)

        self.tgHr10 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR10, label=u'10',
              name=u'tgHr10', parent=self.win, pos=(300, 0),
              size=(30, 20), style=0)
        self.tgHr10.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr10, wxID_WXTIMEEDITTGHR10,
              self.OnTgHrTogglebutton)

        self.tgHr11 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR11, label=u'11',
              name=u'tgHr11', parent=self.win, pos=(330, 0),
              size=(30, 20), style=0)
        self.tgHr11.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr11, wxID_WXTIMEEDITTGHR11,
              self.OnTgHrTogglebutton)

        self.tgHr12 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR12, label=u'12',
              name=u'tgHr12', parent=self.win, pos=(0, 20),
              size=(30, 20), style=0)
        self.tgHr12.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr12, wxID_WXTIMEEDITTGHR12,
              self.OnTgHrTogglebutton)

        self.tgHr13 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR13, label=u'13',
              name=u'tgHr13', parent=self.win, pos=(30, 20),
              size=(30, 20), style=0)
        self.tgHr13.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr13, wxID_WXTIMEEDITTGHR13,
              self.OnTgHrTogglebutton)

        self.tgHr14 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR14, label=u'14',
              name=u'tgHr14', parent=self.win, pos=(60, 20),
              size=(30, 20), style=0)
        self.tgHr14.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr14, wxID_WXTIMEEDITTGHR14,
              self.OnTgHrTogglebutton)

        self.tgHr15 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR15, label=u'15',
              name=u'tgHr15', parent=self.win, pos=(90, 20),
              size=(30, 20), style=0)
        self.tgHr15.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr15, wxID_WXTIMEEDITTGHR15,
              self.OnTgHrTogglebutton)

        self.tgHr16 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR16, label=u'16',
              name=u'tgHr16', parent=self.win, pos=(120, 20),
              size=(30, 20), style=0)
        self.tgHr16.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr16, wxID_WXTIMEEDITTGHR16,
              self.OnTgHrTogglebutton)

        self.tgHr17 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR17, label=u'17',
              name=u'tgHr17', parent=self.win, pos=(150, 20),
              size=(30, 20), style=0)
        self.tgHr17.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr17, wxID_WXTIMEEDITTGHR17,
              self.OnTgHrTogglebutton)

        self.tgHr18 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR18, label=u'18',
              name=u'tgHr18', parent=self.win, pos=(180, 20),
              size=(30, 20), style=0)
        self.tgHr18.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr18, wxID_WXTIMEEDITTGHR18,
              self.OnTgHrTogglebutton)

        self.tgHr19 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR19, label=u'19',
              name=u'tgHr19', parent=self.win, pos=(210, 20),
              size=(30, 20), style=0)
        self.tgHr19.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr19, wxID_WXTIMEEDITTGHR19,
              self.OnTgHrTogglebutton)

        self.tgHr20 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR20, label=u'20',
              name=u'tgHr20', parent=self.win, pos=(240, 20),
              size=(30, 20), style=0)
        self.tgHr20.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr20, wxID_WXTIMEEDITTGHR20,
              self.OnTgHrTogglebutton)

        self.tgHr21 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR21, label=u'21',
              name=u'tgHr21', parent=self.win, pos=(270, 20),
              size=(30, 20), style=0)
        self.tgHr21.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr21, wxID_WXTIMEEDITTGHR21,
              self.OnTgHrTogglebutton)

        self.tgHr22 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR22, label=u'22',
              name=u'tgHr22', parent=self.win, pos=(300, 20),
              size=(30, 20), style=0)
        self.tgHr22.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr22, wxID_WXTIMEEDITTGHR22,
              self.OnTgHrTogglebutton)

        self.tgHr23 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR23, label=u'23',
              name=u'tgHr23', parent=self.win, pos=(330, 20),
              size=(30, 20), style=0)
        self.tgHr23.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr23, wxID_WXTIMEEDITTGHR23,
              self.OnTgHrTogglebutton)

        self.tgMin00 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN00,
              label=u':00', name=u'tgMin00', parent=self.win, pos=(0,
              40), size=(60, 20), style=0)
        self.tgMin00.SetValue(False)
        self.tgMin00.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin00, wxID_WXTIMEEDITTGMIN00,
              self.OnTgMinTogglebutton)

        self.tgMin05 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN05,
              label=u':05', name=u'tgMin05', parent=self.win, pos=(60,
              40), size=(60, 20), style=0)
        self.tgMin05.SetValue(False)
        self.tgMin05.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin05, wxID_WXTIMEEDITTGMIN05,
              self.OnTgMinTogglebutton)

        self.tgMin10 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN10,
              label=u':10', name=u'tgMin10', parent=self.win,
              pos=(120, 40), size=(60, 20), style=0)
        self.tgMin10.SetValue(False)
        self.tgMin10.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin10, wxID_WXTIMEEDITTGMIN10,
              self.OnTgMinTogglebutton)

        self.tgMin15 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN15,
              label=u':15', name=u'tgMin15', parent=self.win,
              pos=(180, 40), size=(60, 20), style=0)
        self.tgMin15.SetValue(False)
        self.tgMin15.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin15, wxID_WXTIMEEDITTGMIN15,
              self.OnTgMinTogglebutton)

        self.tgMin20 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN20,
              label=u':20', name=u'tgMin20', parent=self.win,
              pos=(240, 40), size=(60, 20), style=0)
        self.tgMin20.SetValue(False)
        self.tgMin20.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin20, wxID_WXTIMEEDITTGMIN20,
              self.OnTgMinTogglebutton)

        self.tgMin25 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN25,
              label=u':25', name=u'tgMin25', parent=self.win,
              pos=(300, 40), size=(60, 20), style=0)
        self.tgMin25.SetValue(False)
        self.tgMin25.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin25, wxID_WXTIMEEDITTGMIN25,
              self.OnTgMinTogglebutton)

        self.tgMin30 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN30,
              label=u':30', name=u'tgMin30', parent=self.win, pos=(0,
              60), size=(60, 20), style=0)
        self.tgMin30.SetValue(False)
        self.tgMin30.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin30, wxID_WXTIMEEDITTGMIN30,
              self.OnTgMinTogglebutton)

        self.tgMin35 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN35,
              label=u':35', name=u'tgMin35', parent=self.win, pos=(60,
              60), size=(60, 20), style=0)
        self.tgMin35.SetValue(False)
        self.tgMin35.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin35, wxID_WXTIMEEDITTGMIN35,
              self.OnTgMinTogglebutton)

        self.tgMin40 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN40,
              label=u':40', name=u'tgMin40', parent=self.win,
              pos=(120, 60), size=(60, 20), style=0)
        self.tgMin40.SetValue(False)
        self.tgMin40.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin40, wxID_WXTIMEEDITTGMIN40,
              self.OnTgMinTogglebutton)

        self.tgMin45 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN45,
              label=u':45', name=u'tgMin45', parent=self.win,
              pos=(180, 60), size=(60, 20), style=0)
        self.tgMin45.SetValue(False)
        self.tgMin45.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin45, wxID_WXTIMEEDITTGMIN45,
              self.OnTgMinTogglebutton)

        self.tgMin50 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN50,
              label=u':50', name=u'tgMin50', parent=self.win,
              pos=(240, 60), size=(60, 20), style=0)
        self.tgMin50.SetValue(False)
        self.tgMin50.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin50, wxID_WXTIMEEDITTGMIN50,
              self.OnTgMinTogglebutton)

        self.tgMin55 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN55,
              label=u':55', name=u'tgMin55', parent=self.win,
              pos=(300, 60), size=(60, 20), style=0)
        self.tgMin55.SetValue(False)
        self.tgMin55.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin55, wxID_WXTIMEEDITTGMIN55,
              self.OnTgMinTogglebutton)

        
        self.tgHr24 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN55,
              label=u'24:00', name=u'tgHr24', parent=self.win,
              pos=(300, 80), size=(60, 20), style=0)
        self.tgHr24.SetValue(False)
        self.tgHr24.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgHr24, wxID_WXTIMEEDITTGMIN55,
              self.OnTgHr24Togglebutton)


        img=getCancelBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        #self.gcbbCancel.SetBitmapLabel(img)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_WXTIMEEDITGCBBCANCEL,
              bitmap=img, label=u'Cancel', name=u'gcbbCancel',
              parent=self.win, pos=wx.Point(0, 80), size=wx.Size(70, 30),
              style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_WXTIMEEDITGCBBCANCEL)

        img=getDelBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.gcbbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_WXTIMEEDITGCBBDEL,
              bitmap=img, label=u'Del', name=u'gcbbDel',
              parent=self.win, pos=wx.Point(70, 80), size=wx.Size(72, 30),
              style=0)
        self.gcbbDel.Bind(wx.EVT_BUTTON, self.OnGcbbDelButton,
              id=wxID_WXTIMEEDITGCBBDEL)
        
        self.win.SetSize((365,115))
        # This method is needed to set the contents that will be displayed
        # in the popup
        self.SetPopupContent(self.win)

        self.tgActHr=None
        self.tgActMin=None
        self.tgPrevHr=None
        self.tgPrevMin=None
        self.objTimeCtrl=None
        self.bEnableMark=True
    def SetEnableMark(self,flag):
        self.bEnableMark=flag
    def OnGcbbDelButton(self,event):
        if self.tgActHr is not None:
            self.tgActHr.SetValue(False)
        if self.tgActMin is not None:
            self.tgActMin.SetValue(False)
        self.tgActHr=None
        self.tgActMin=None
        self.tgPrevHr=None
        self.tgPrevMin=None

        s='--:--:--'
        self.SetValue(s)
        wx.PostEvent(self,vTimeTimeEditChanged(self,s))
        self.PopDown()
    def OnGcbbCancelButton(self,event):
        if self.tgActHr is not None:
            self.tgActHr.SetValue(False)
        if self.tgActMin is not None:
            self.tgActMin.SetValue(False)
        self.tgActHr=self.tgPrevHr
        self.tgActMin=self.tgPrevMin
        if self.tgActHr is not None:
            self.tgActHr.SetValue(True)
        if self.tgActMin is not None:
            self.tgActMin.SetValue(True)
        dt=self.GetValue()
        s=self.dt.FormatTime()
        #wx.PostEvent(self,wxTimeEditChanged(s))
        self.PopDown()
    def OnTgHrTogglebutton(self, event):
        obj=event.GetEventObject()
        if obj.GetValue()==False:
            obj.SetValue(True)
        else:
            if self.tgHr24.GetValue()==True:
                self.tgHr24.SetValue(False)
            if self.tgActHr!=None:
                self.tgActHr.SetValue(False)
            self.tgActHr=obj

    def OnTgMinTogglebutton(self, event):
        obj=event.GetEventObject()
        if obj.GetValue()==False:
            obj.SetValue(True)
        else:
            if self.tgHr24.GetValue()==True:
                obj.SetValue(False)
                return
            if self.tgActMin!=None:
                self.tgActMin.SetValue(False)
            self.tgActMin=obj
        self.tgPrevHr=self.tgActHr
        self.tgPrevMin=self.tgActMin
        dt=self.GetValue()
        #s=self.dt.FormatTime()
        s=self.dt.Format('%H:%M:%S')
        self.SetValue(s)
        wx.PostEvent(self,vTimeTimeEditChanged(self,s))
        self.PopDown()
    def OnTgHr24Togglebutton(self,event):
        obj=event.GetEventObject()
        if obj.GetValue()==False:
            obj.SetValue(True)
        else:
            if self.tgActHr!=None:
                self.tgActHr.SetValue(False)
            self.tgActHr=None
            if self.tgActMin!=None:
                self.tgActMin.SetValue(False)
            self.tgActMin=None
        self.tgPrevHr=self.tgActHr
        self.tgPrevMin=self.tgActMin
        #dt=self.GetValue()
        #s=self.dt.FormatTime()
        s='24:00:00'
        self.SetValue(s)
        wx.PostEvent(self,vTimeTimeEditChanged(self,s))
        self.PopDown()
    def SetTime(self,dtNew):
        try:
            min=dtNew.GetMinute()
            hr=dtNew.GetHour()
            min=round(min/5.0)*5
            self.__setTime(hr,min)
        except:
            pass
    def SetTimeCtrl(self,obj):
        self.objTimeCtrl=obj
    def SetTimeStr(self,str,format=''):
        if format is None:
            format="%H%M%S"
        else:
            if len(format)<=0:
                format="%H%M%S"
        if len(str)==0:
            # clear time info
            if self.tgActMin is not None:
                self.tgActMin.SetValue(False)
            if self.tgActHr is not None:
                self.tgActHr.SetValue(False)
            self.tgActHr=None
            self.tgActMin=None
            self.tgPrevHr=None
            self.tgPrevMin=None
            wxPopupControl.SetValue(self,'--:--:--')
            return
        try:
            if str=='240000' or str=='24:00:00' or str=='2400' or str=='24:00':
                self.__setTime(24,00)
                return
            dtNew=wx.DateTime()
            dtNew.ParseFormat(str,format)
            min=dtNew.GetMinute()
            hr=dtNew.GetHour()
            min=round(min/5.0)*5
            self.__setTime(hr,min)
        except:
            if self.tgActMin is not None:
                self.tgActMin.SetValue(False)
            if self.tgActHr is not None:
                self.tgActHr.SetValue(False)
            self.tgActHr=None
            self.tgActMin=None
            self.tgPrevHr=None
            self.tgPrevMin=None
            wx.lib.popupctl.PopupControl.SetValue(self,'--:--:--')
            pass
    def GetValue(self):
        min=0
        hr=0
        if self.tgActMin is not None:
            if self.tgActMin == self.tgMin00:
                min=0
            elif self.tgActMin==self.tgMin05:
                min=5
            elif self.tgActMin == self.tgMin10:
                min=10
            elif self.tgActMin == self.tgMin15:
                min=15
            elif self.tgActMin == self.tgMin20:
                min=20
            elif self.tgActMin == self.tgMin25:
                min=25
            elif self.tgActMin == self.tgMin30:
                min=30
            elif self.tgActMin == self.tgMin35:
                min=35
            elif self.tgActMin == self.tgMin40:
                min=40
            elif self.tgActMin == self.tgMin45:
                min=45
            elif self.tgActMin == self.tgMin50:
                min=50
            elif self.tgActMin == self.tgMin55:
                min=55
        if self.tgActHr is not None:
            if self.tgActHr==self.tgHr00:
                hr=0
            elif self.tgActHr==self.tgHr01:
                hr=1
            elif self.tgActHr==self.tgHr02:
                hr=2
            elif self.tgActHr==self.tgHr03:
                hr=3
            elif self.tgActHr==self.tgHr04:
                hr=4
            elif self.tgActHr==self.tgHr05:
                hr=5
            elif self.tgActHr==self.tgHr06:
                hr=6
            elif self.tgActHr==self.tgHr07:
                hr=7
            elif self.tgActHr==self.tgHr08:
                hr=8
            elif self.tgActHr==self.tgHr09:
                hr=9
            elif self.tgActHr==self.tgHr10:
                hr=10
            elif self.tgActHr==self.tgHr11:
                hr=11
            elif self.tgActHr==self.tgHr12:
                hr=12
            elif self.tgActHr==self.tgHr13:
                hr=13
            elif self.tgActHr==self.tgHr14:
                hr=14
            elif self.tgActHr==self.tgHr15:
                hr=15
            elif self.tgActHr==self.tgHr16:
                hr=16
            elif self.tgActHr==self.tgHr17:
                hr=17
            elif self.tgActHr==self.tgHr18:
                hr=18
            elif self.tgActHr==self.tgHr19:
                hr=19
            elif self.tgActHr==self.tgHr20:
                hr=20
            elif self.tgActHr==self.tgHr21:
                hr=21
            elif self.tgActHr==self.tgHr22:
                hr=22
            elif self.tgActHr==self.tgHr23:
                hr=23
        if self.tgHr24.GetValue()==True:
            hr=23
            min=59
        self.dt=wx.DateTime()
        self.dt.Set(10)
        self.dt.SetHour(hr)
        self.dt.SetMinute(min)
        if self.tgHr24.GetValue()==True:
            self.dt.SetSecond(59)
        return self.dt
    def GetTimeStr(self,format=''):
        if self.tgHr24.GetValue()==True:
            if format is None:
                return '240000'
            if format=="%H%M%S" or len(format)<=0:
                return '240000'
            return '24:00:00'
        if self.tgActHr is None or self.tgActMin is None:
            # time info not set completly
            return '--:--:--'
        dt=self.GetValue()
        if format is None:
            format="%H%M%S"
        else:
            if len(format)<=0:
                format="%H%M%S"
        return dt.Format(format)
    def __setTime(self,hr,min):
        try:
            if self.tgActMin is not None:
                self.tgActMin.SetValue(False)
            if self.tgActHr is not None:
                self.tgActHr.SetValue(False)
            self.tgActHr=None
            self.tgActMin=None
            self.tgHr24.SetValue(False)
            if hr==24 and min==0:
                self.tgHr24.SetValue(True)
                self.tgActMin=None
                self.tgActHr=None
                self.tgPrevHr=self.tgActHr
                self.tgPrevMin=self.tgActMin
                self.SetValue('24:00:00')
                return
            if min==0:
                self.tgActMin=self.tgMin00
            elif min==5:
                self.tgActMin=self.tgMin05
            elif min==10:
                self.tgActMin=self.tgMin10
            elif min==15:
                self.tgActMin=self.tgMin15
            elif min==20:
                self.tgActMin=self.tgMin20
            elif min==25:
                self.tgActMin=self.tgMin25
            elif min==30:
                self.tgActMin=self.tgMin30
            elif min==35:
                self.tgActMin=self.tgMin35
            elif min==40:
                self.tgActMin=self.tgMin40
            elif min==45:
                self.tgActMin=self.tgMin45
            elif min==50:
                self.tgActMin=self.tgMin50
            elif min==55:
                self.tgActMin=self.tgMin55
            if hr==0:
                self.tgActHr=self.tgHr00
            elif hr==1:
                self.tgActHr=self.tgHr01
            elif hr==2:
                self.tgActHr=self.tgHr02
            elif hr==3:
                self.tgActHr=self.tgHr03
            elif hr==4:
                self.tgActHr=self.tgHr04
            elif hr==5:
                self.tgActHr=self.tgHr05
            elif hr==6:
                self.tgActHr=self.tgHr06
            elif hr==7:
                self.tgActHr=self.tgHr07
            elif hr==8:
                self.tgActHr=self.tgHr08
            elif hr==9:
                self.tgActHr=self.tgHr09
            elif hr==10:
                self.tgActHr=self.tgHr10
            elif hr==11:
                self.tgActHr=self.tgHr11
            elif hr==12:
                self.tgActHr=self.tgHr12
            elif hr==13:
                self.tgActHr=self.tgHr13
            elif hr==14:
                self.tgActHr=self.tgHr14
            elif hr==15:
                self.tgActHr=self.tgHr15
            elif hr==16:
                self.tgActHr=self.tgHr16
            elif hr==17:
                self.tgActHr=self.tgHr17
            elif hr==18:
                self.tgActHr=self.tgHr18
            elif hr==19:
                self.tgActHr=self.tgHr19
            elif hr==20:
                self.tgActHr=self.tgHr20
            elif hr==21:
                self.tgActHr=self.tgHr21
            elif hr==22:
                self.tgActHr=self.tgHr22
            elif hr==23:
                self.tgActHr=self.tgHr23
            self.tgActMin.SetValue(True)
            self.tgActHr.SetValue(True)
            self.tgPrevHr=self.tgActHr
            self.tgPrevMin=self.tgActMin
            dt=self.GetValue()
            #s=self.dt.FormatTime()
            s=self.dt.Format('%H:%M:%S')
            self.SetValue(s)
        
        except:
            pass

    # Method called when button clicked
    def __set_timeold__(self,t):
        # Format the date that was selected for the text part of the control
        h=time.localtime(time.time())
        t=time.gmtime(time.mktime(t+[0,0,h[8]]))
        s='%02d%02d%02d' % (t[3],t[4],t[5])
        self.SetValue(s)
        wx.PostEvent(self,wxTimeGMChanged(s))
            
    def OnClose(self,evt):
        self.PopDown()
        #try:
        #    t=(zHour,zMin,zSec)=map(string.atoi,string.split(self.txt.GetValue(),':'))
        #except:
        #    t=(zHour,zMin,zSec)=(0,0,0)
        #date = self.cal.GetDate()
        #self.__set_date_time__([0,0,0,zHour,zMin,zSec])
        evt.Skip()

    