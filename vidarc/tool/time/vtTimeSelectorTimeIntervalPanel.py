#Boa:FramePanel:vtTimeSelectorHoursPanel
#----------------------------------------------------------------------------
# Name:         vtTimeSelectorTimePanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060213
# CVS-ID:       $Id: vtTimeSelectorTimeIntervalPanel.py,v 1.1 2006/02/14 12:26:43 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------


from vidarc.tool.time.vtTimeSelectorHoursPanel import *

import vidarc.tool.log.vtLog as vtLog

VERBOSE=1
BUFFERED=1

class vtTimeSelectorTimePanel(vtTimeSelectorHoursPanel):
    def __init__(self, parent, days, hours, 
                min=15,gridMin=5,
                start_hour=0,min_hour=0,max_hour=24,
                show_hours=-1, sum=True, dayLabels=None, 
                id=wx.NewId(), pos=wx.DefaultPosition, size=wx.Size(100, 50), 
                style=wx.TAB_TRAVERSAL, name=u'vgTmSelectorDay',gridDay=15,gridHr=15):
        self.minTol=min
        self.iMin=60/self.minTol
        self.gridMin=gridMin
        self.gridMinPerDay=self.gridMin<<1
        #self.gridDay=gridDay
        vtTimeSelectorHoursPanel.__init__(self, parent, days, hours, start_hour,min_hour,max_hour,
                show_hours, sum, dayLabels, 
                id, pos, size, 
                style, name,gridDay,gridHr)
        #self.gridDay=self.gridY
        #self.gridY=self.gridDay+self.gridMin<<1
        self.gridMinX=(self.gridX*self.iShowHr)/self.iMin
        
        self.__clrXYrel__()
    def __setDrawingSize__(self):
        self.maxWidth=self.gridX*self.iShowHr+self.ofsX+self.iHrWidth*2+self.sumX
        self.maxHeight=(self.gridY+self.gridMinPerDay)*self.days
        self.iXNavHrDec=self.iHrWidth
        self.iXNavHrInc=self.ofsX+self.gridX*self.iShowHr+self.sumX+self.iHrWidth
        self.iDrawWidth=self.gridX*self.iShowHr
        self.gridMinX=(self.gridX*self.iShowHr)/self.iMin
        
        self.scwDrawing.SetVirtualSize((self.maxWidth+5, self.maxHeight+5+self.sumY))
        self.scwDrawing.SetScrollRate(20,20)
        if BUFFERED:
            # Initialize the buffer bitmap.  No real DC is needed at this point.
            self.buffer = wx.EmptyBitmap(self.maxWidth+1, self.maxHeight+1+self.sumY)
            dc = wx.BufferedDC(None, self.buffer)
            dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
            dc.Clear()
            self.DoDrawing(dc)
    def __clrXYrel__(self):
        vtTimeSelectorHoursPanel.__clrXYrel__(self)
        self.iMinStart=-1
        self.iMinEnd=-1
    def __calcXYrel__(self):
        try:
            iDay=self.iDay
            iHour=self.iHour
            iMinStart=self.iMinStart
            iMinEnd=self.iMinEnd
        except:
            iHour=-1
            iDay=-1
            iMinStart=-1
            iMinEnd=-1
        self.__clrXYrel__()
        if self.x<self.iXNavHrDec:
            self.iNav=-1
            return
        elif self.x>self.iXNavHrInc:
            self.iNav=1
            return
        self.iDay=self.y/(self.gridY+self.gridMinPerDay)
        if self.iDay>=self.days:
            self.iDay=-1
            self.__clrXYrel__()
            return
        if self.iDay<0:
            self.__clrXYrel__()
            return
        yMin=((self.gridY+self.gridMinPerDay)*self.iDay)+self.gridY
        if self.y>=yMin:
            #calc min
            self.bKeepSel=True
            iMin=((self.x-self.ofsX-self.iHrWidth)/self.gridMinX)
            if self.y>yMin+self.gridMin:
                self.iMinEnd=iMin
            else:
                self.iMinStart=iMin
            self.iDay=iDay
            self.iHour=iHour
            
        else:
            self.iHour=((self.x-self.ofsX-self.iHrWidth)/self.gridX)+self.iOfsHr
            if self.iHour>=self.hours:
                self.iHour=-1
                self.__clrXYrel__()
            self.bKeepSel=False
        #vtLog.vtLngCurWX(vtLog.DEBUG,'day:%d;hour:%d;keep%d'%(self.iDay,self.iHour,self.bKeepSel),self)
        
    def __getHourXYrel__(self):
        return ['%02d%02d'%(self.iHour,0),'%02d%02d'%(self.iHour+1,0)]
    def __modifyHourXYrel__(self,blk,dc=None,printing=False):
        iMinStart=self.iMinStart
        iMinEnd=self.iMinEnd
        hour=blk[-1]
        day=self.lst[self.iDay]
        #print iMinStart,iMinEnd
        #print hour
        if iMinStart>=0:
            iMinStart*=self.minTol
            iHrStart=int(hour[0][0:2])
            
            lst=[(blk,hour)]
            if iHrStart==self.iHour:
                iHrEnd=int(hour[1][0:2])
                if iHrEnd==iHrStart:
                    iMinEndTmp=int(hour[1][2:4])
                    if iMinEndTmp<=iMinStart:
                        return
                # modify start min
                hrNew=['%02d%02d'%(self.iHour,iMinStart),hour[1]]
                vtLog.vtLngCurWX(vtLog.DEBUG,'hour:%s;hournew:%s'%(hour,hrNew),self)
                self.__drawHour__(blk,False,dc,False)
                self.__clearHour__(self.iDay,hour,dc,False)
                day.remove(hour)
                self.selectableBlks.remove(blk)
                #blkNew=self.__setHour__(self.iDay,hrNew)
                #self.__drawHour__(blkNew,True,dc,False)
                #self.selectedBlk=blkNew
            else:
                hrNew=['%02d%02d'%(self.iHour,iMinStart),hour[1]]
                vtLog.vtLngCurWX(vtLog.DEBUG,'hour:%s;hournew:%s'%(hour,hrNew),self)
                self.__drawHour__(blk,False,dc,False)
                self.__clearHour__(self.iDay,hour,dc,False)
                blk=self.selectedBlk
                blk=self.__checkHour__(blk,hour,self.iDay,dc,False)
                self.selectedBlk=None
                #self.__clearHour__(self.iDay,hour,dc,False)
            blkNew=self.__setHour__(self.iDay,hrNew)
            self.__drawHour__(blkNew,True,dc,False)
            self.selectedBlk=blkNew
        if iMinEnd>=0:
            iMinEnd+=1
            iMinEnd*=self.minTol
            iHrEnd=int(hour[1][0:2])
            #iHrEnd=int(hour[0][0:2])
            iHrStart=int(hour[0][0:2])
            if iHrStart==self.iHour:
                iMinStart=int(hour[0][2:4])
                if iMinEnd<=iMinStart:
                    return
            if iMinEnd==60:
                iMinEnd=0
                iHour=self.iHour+1
            else:
                iHour=self.iHour
            lst=[(blk,hour)]
            if iHrEnd==self.iHour:
                # modify start min
                hrNew=[hour[0],'%02d%02d'%(iHour,iMinEnd)]
                vtLog.vtLngCurWX(vtLog.DEBUG,'hour:%s;hournew:%s;days:%s;hr:%d,min:%d'%(hour,hrNew,day,iHour,iMinEnd),self)
                self.__drawHour__(blk,False,dc,False)
                self.__clearHour__(self.iDay,hour,dc,False)
                day.remove(hour)
                self.selectableBlks.remove(blk)
                #blkNew=self.__setHour__(self.iDay,hrNew)
                #self.__drawHour__(blkNew,True,dc,False)
                #self.selectedBlk=blkNew
            else:
                hrNew=[hour[0],'%02d%02d'%(iHour,iMinEnd)]
                vtLog.vtLngCurWX(vtLog.DEBUG,'hour:%s;hournew:%s'%(hour,hrNew),self)
                self.__drawHour__(blk,False,dc,False)
                self.__clearHour__(self.iDay,hour,dc,False)
                blk=self.selectedBlk
                blk=self.__checkHour__(blk,hour,self.iDay,dc,False)
                self.selectedBlk=None
                #self.__clearHour__(self.iDay,hour,dc,False)
            blkNew=self.__setHour__(self.iDay,hrNew)
            self.__drawHour__(blkNew,True,dc,False)
            self.selectedBlk=blkNew
            #return [hour[0],'%02d%02d'%(self.iHour,iMinEnd)]
        self.__check__()
        return None
    def __getSelectableBlk__(self,x,y):
        xa=((x-self.ofsX-self.iHrWidth)/self.gridX)*self.gridX+self.ofsX+self.iHrWidth
        xe=xa+self.gridX
        ya=(y/(self.gridY+self.gridMinPerDay))*(self.gridY+self.gridMinPerDay)
        ye=ya+self.gridY
        if y<ya or y>ye:
            return None
        print '(%d,%d),(%d,%d),(%d,%d)'%(xa,ya,xe,ye,x,y)
        for blks in self.selectableBlks:
            print blks
            if (y>blks[1]) and (y<blks[3]):
                if (x>blks[0]) and (x<blks[2]):
                    return blks
                if (xa<=blks[0]) and (xe>=blks[0]):
                    return blks
        return None
    
    def OnLeftButtonEvent(self, event):
        event.Skip()
        #vtLog.CallStack('')
        if event.LeftDown():
            self.SetFocus()
            self.SetXY(event)
            if self.x<self.iXNavHrDec:
                ofs=self.iOfsHr-1
                self.iOfsHr=self.__limit__(ofs,self.iMinHr,self.iMaxHr)
                if self.iOfsHr==ofs:
                    self.__invokeRefresh__()
                    return
                    pass
            elif self.x>self.iXNavHrInc:
                ofs=self.iOfsHr+1
                self.iOfsHr=self.__limit__(ofs,self.iMinHr,self.iMaxHr-self.iShowHr)
                if self.iOfsHr==ofs:
                    self.__invokeRefresh__()
                    return
                    pass
            
            if self.verbose:
                vtLog.vtLngCurWX(vtLog.DEBUG,'left down',self)
                vtLog.vtLngCurWX(vtLog.DEBUG,'x:%d y:%d'%(self.x,self.y),self)
                vtLog.vtLngCurWX(vtLog.DEBUG,'shift:%d ctrl:%d'%(event.ShiftDown(),event.ControlDown()),self)
                vtLog.vtLngCurWX(vtLog.DEBUG,'day:%d hour:%d'%(self.iDay,self.iHour),self)
                vtLog.vtLngCurWX(vtLog.DEBUG,vtLog.pformat(self.selectableBlks),self)
                vtLog.vtLngCurWX(vtLog.DEBUG,vtLog.pformat(self.lst),self)
            if self.iDay==-1 or self.iHour==-1:
                #if self.selectedBlk is not None:
                #    cdc = wx.ClientDC(self.scwDrawing)
                #    self.scwDrawing.DoPrepareDC(cdc)
                #    dc = wx.BufferedDC(cdc, self.buffer)
                #    dc.BeginDrawing()
                #    self.__drawHour__(self.selectedBlk,False,dc,False)
                #    dc.EndDrawing()
                #    self.selectedBlk=None
                return
            self.drawing = True
            bUpdateSum=False
            if self.verbose:
                vtLog.vtLngCurWX(vtLog.DEBUG,'at begin lst:%s'%repr(self.lst[self.iDay]),self)
            blk=self.__getSelectableBlk__(self.x,self.y)
            if BUFFERED:
                cdc = wx.ClientDC(self.scwDrawing)
                self.scwDrawing.DoPrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
                dc.BeginDrawing()
            try:
                if self.verbose:
                    vtLog.vtLngCurWX(vtLog.DEBUG,blk,self)
                if blk is None:
                    if self.selectedBlk is None or (self.bLastSel==True and self.bKeepSel==False):
                        if self.selectedBlk is not None:
                            self.__drawHour__(self.selectedBlk,False,dc,False)
                        blk=self.__createBlkByXY__()
                        #self.__drawHour__(self.selectedBlk,False,dc,False)
                        self.__drawHour__(blk,True,dc,False)
                        self.selectedBlk=blk
                        if self.verbose:
                            vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%repr(blk),self)
                        bUpdateSum=True
                        wx.PostEvent(self,vTmSelectorHourChanged(self,self.iDay))
                        #if self.selectedBlk==blk:
                        #    self.selectedBlk=None
                    else:
                        self.__modifyHourXYrel__(self.selectedBlk,dc,False)
                        bUpdateSum=True
                        if self.iMinStart>=0:
                            pass
                        if self.iMinStart>=0:
                            pass
                else:
                    if self.selectedBlk is not None and self.bKeepSel==False:
                        if self.selectedBlk==blk:
                            bUpdateSum=True
                            #hour=blk[-1]
                            self.__drawHour__(blk,False,dc,False)
                            blk=self.__checkHour__(blk,self.__getHourXYrel__(),self.iDay,dc,False)
                            self.selectedBlk=None
                            self.__clearHour__(self.iDay,self.__getHourXYrel__(),dc,False)
                            wx.PostEvent(self,vTmSelectorHourChanged(self,self.iDay))
                        
                if blk is not None and 1==0:
                    # If doing buffered drawing, create the buffered DC, giving it
                    # it a real DC to blit to when done.
                    if self.selectedBlk is not None:
                        if self.selectedBlk!=blk:
                            # paint old
                            self.__drawHour__(self.selectedBlk,False,dc,False)
                        else:
                            bUpdateSum=True
                            blk=self.__checkHour__(blk,self.__getHourXYrel__(),self.iDay,dc,False)
                            wx.PostEvent(self,vTmSelectorHourChanged(self,self.iDay))
                    if blk is not None:
                        #print 'selec',blk
                        #bUpdateSum=True
                        self.__drawHour__(blk,True,dc,False)
                    if self.verbose:
                        vtLog.vtLngCurWX(vtLog.DEBUG,'    blk:%s'%repr(blk),self)
                    if self.verbose:
                        vtLog.vtLngCurWX(vtLog.DEBUG,'    blk:%s'%repr(blk),self)
                    self.selectedBlk=blk
                    if self.selectedBlk is not None:
                        wx.PostEvent(self,vTmSelectorHourSelected(self,self.iDay,self.selectedBlk[-1]))
                    else:
                        wx.PostEvent(self,vTmSelectorHourSelected(self,self.iDay,None))
            except:
                vtLog.vtLngTB(self.GetName())
            if bUpdateSum:
                self.__drawSum__(dc,False)
            if BUFFERED:
                dc.EndDrawing()
            if self.verbose:
                vtLog.vtLngCurWX(vtLog.DEBUG,'at end   lst:%s'%repr(self.lst[self.iDay]),self)
                vtLog.vtLngCurWX(vtLog.DEBUG,'    blk:%s'%repr(blk),self)
            pass
        elif event.Dragging() and self.drawing:
            #print 'dragging'
            if BUFFERED:
                # If doing buffered drawing, create the buffered DC, giving it
                # it a real DC to blit to when done.
                cdc = wx.ClientDC(self)
                self.PrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
            else:
                dc = wx.ClientDC(self)
                self.PrepareDC(dc)

            #dc.BeginDrawing()
            #dc.SetPen(wx.Pen('MEDIUM FOREST GREEN', 4))
            #coords = (self.x, self.y) + self.ConvertEventCoords(event)
            #self.curLine.append(coords)
            #dc.DrawLine(*coords)
            #self.SetXY(event)
            #dc.EndDrawing()


        elif event.LeftUp() and self.drawing:
            #self.lines.append(self.curLine)
            #self.curLine = []
            #self.ReleaseMouse()
            self.drawing = False
            self.scwDrawing.Refresh()
            #xView, yView = self.scwDrawing.GetViewStart()
            #xDelta, yDelta = self.scwDrawing.GetScrollPixelsPerUnit()
            #print xView,yView,xDelta,yDelta
            #s=self.scwDrawing.GetSize()
            #self.scwDrawing.RefreshRect(wx.Rect(xView*xDelta,yView*yDelta,
            #        s[0],s[1]))
            pass
        else:
            try:
                self.SetXY(event)
                cdc = wx.ClientDC(self.scwDrawing)
                self.scwDrawing.DoPrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
                dc.BeginDrawing()
                if self.x<self.iXNavHrDec:
                    ofs=self.iOfsHr-1
                    ofsNew=self.__limit__(ofs,self.iMinHr,self.iMaxHr)
                    self.__drawHourNav__(dc,ofsNew==ofs,1)
                elif self.x>self.iXNavHrInc:
                    ofs=self.iOfsHr+1
                    ofsNew=self.__limit__(ofs,self.iMinHr,self.iMaxHr-self.iShowHr)
                    self.__drawHourNav__(dc,ofsNew==ofs,2)
                else:
                    self.__drawHourNav__(dc,False,3)
                    iDay=self.y/self.gridY
                    if iDay>=self.days:
                        iDay=-1
                        #if self.selectedBlk is not None:
                        #    self.__drawHour__(self.selectedBlk,False,dc,False)
                        #self.selectedBlk=None
                    iHour=((self.x-self.ofsX-self.iHrWidth)/self.gridX)+self.iOfsHr
                    #print iHour
                    if iHour>=self.hours:
                        iHour=-1
                    #if self.verbose:
                    #    vtLog.vtLngCurWX(vtLog.DEBUG,'left move',self)
                    #    vtLog.vtLngCurWX(vtLog.DEBUG,'x:%d y:%d'%(self.x,self.y),self)
                    #    vtLog.vtLngCurWX(vtLog.DEBUG,'shift:%d ctrl:%d'%(event.ShiftDown(),event.ControlDown()),self)
                    #    vtLog.vtLngCurWX(vtLog.DEBUG,'day:%d hour:%d'%(iDay,iHour),self)
                    #    vtLog.vtLngCurWX(vtLog.DEBUG,vtLog.pformat(self.selectableBlks),self)
                    #    vtLog.vtLngCurWX(vtLog.DEBUG,vtLog.pformat(self.lst),self)
                    if iDay==-1 or iHour==-1:
                        if self.selectedBlk is not None:
                            #cdc = wx.ClientDC(self.scwDrawing)
                            #self.scwDrawing.DoPrepareDC(cdc)
                            #dc = wx.BufferedDC(cdc, self.buffer)
                            #dc.BeginDrawing()
                            self.__drawHour__(self.selectedBlk,False,dc,False)
                            self.selectedBlk=None
                        dc.EndDrawing()
                        return
                    self.drawing = True
                    bUpdateSum=False
                    #if self.verbose:
                    #    vtLog.vtLngCurWX(vtLog.DEBUG,'at begin lst:%s'%repr(self.lst[iDay]),self)
                    blk=self.__getSelectableBlk__(self.x,self.y)
                    #print blk
                    
                    if blk is None:
                        if self.selectedBlk is not None:
                            self.__drawHour__(self.selectedBlk,False,dc,False)
                        self.selectedBlk=None
                    else:
                        if self.selectedBlk is not None:
                            if self.selectedBlk!=blk:
                                # paint old
                                self.__drawHour__(self.selectedBlk,False,dc,False)
                        else:
                            self.selectedBlk=blk
                            self.__drawHour__(self.selectedBlk,True,dc,False)
                    #    hour=['%02d%02d'%(iHour,0),'%02d%02d'%(iHour+1,0)]
                    #    blk=self.__setHour__(iDay,hour)
                    #    if self.verbose:
                    #        vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%repr(blk),self)
                    #    bUpdateSum=True
                    #    wx.PostEvent(self,vTmSelectorHourChanged(self,iDay))
                    #    if self.selectedBlk==blk:
                    #        self.selectedBlk=None
                    #print blk
                    #if blk is not None:
                    #    if BUFFERED:
                dc.EndDrawing()
            except:
                pass
            #self.x
            #iDay=self.y/self.gridY
    def OnMoveEvent(self,event):
            event.Skip()
            try:
                iHour=self.iHour
                self.SetXY(event)
                cdc = wx.ClientDC(self.scwDrawing)
                self.scwDrawing.DoPrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
                dc.BeginDrawing()
                if self.iNav==-1:
                    ofs=self.iOfsHr-1
                    ofsNew=self.__limit__(ofs,self.iMinHr,self.iMaxHr)
                    self.__drawHourNav__(dc,ofsNew==ofs,1)
                elif self.iNav==1:
                    ofs=self.iOfsHr+1
                    ofsNew=self.__limit__(ofs,self.iMinHr,self.iMaxHr-self.iShowHr)
                    self.__drawHourNav__(dc,ofsNew==ofs,2)
                else:
                    self.__drawHourNav__(dc,False,3)
                    if self.iDay==-1 or self.iHour==-1 and self.bKeepSel==False:
                        if self.selectedBlk is not None:
                            self.__drawHour__(self.selectedBlk,False,dc,False)
                            self.selectedBlk=None
                        dc.EndDrawing()
                        return
                    self.drawing = True
                    bUpdateSum=False
                    self.__checkSel__(dc,False)
                    #blk=self.__getSelectableBlk__(self.x,self.y)
                    #print blk
                dc.EndDrawing()
            except:
                pass
    def __getHourRect__(self,iDay,hour,ofsHr=0):
        hr =string.atoi(hour[0][:2])
        min=string.atoi(hour[0][2:4])
        sHr=hr+min/60.0
        min=round(min/5.0)
        x=self.ofsX+self.iHrWidth
        #y=(self.gridY+self.iMin*self.gridMin)*iDay
        y=(self.gridY+self.gridMinPerDay)*iDay
        
        ofsHr+=self.iOfsHr
        #if hr>=ofsHr:
        xa=(hr-ofsHr)*self.gridHr+(min*self.gridHr/12.0)
        xa=self.__limit__(xa,0,self.iDrawWidth)
        
        #if xa<self.iXHrNavMin:
        #    xa=self.iXHrNavMin
        hr =string.atoi(hour[1][:2])
        min=string.atoi(hour[1][2:4])
        eHr=hr+min/60.0
        min=round(min/5.0)
        if sHr<eHr:
            sum=eHr-sHr
        else:
            sum=sHr-eHr
        #xe=coor[0]
        #if hr>=ofsHr:
        xe=(hr-ofsHr)*self.gridHr+(min*self.gridHr/12.0)
        if xe<0:
            xe=0
        xe=self.__limit__(xe,0,self.iDrawWidth)
        #print [x+xa, y, xe-xa, self.gridY]
        return [x+xa, y, xe-xa, self.gridY],sum
    def __updateGrid__(self,iDay,hour,dc,printing):
        iStartHr=int(hour[0][0:2])-self.iOfsHr
        iEndHr=int(hour[1][0:2])-self.iOfsHr
        iEndMin=int(hour[1][2:4])
        if iEndMin>0:
            iEndHr+=1
        iStartHr=self.__limit__(iStartHr,0,self.iShowHr)
        iEndHr=self.__limit__(iEndHr,0,self.iShowHr)
        
        y=iDay*(self.gridY+self.gridMinPerDay)
        
        xa=iStartHr*self.gridX+self.ofsX+self.iHrWidth
        xe=iEndHr*self.gridX+self.ofsX+self.iHrWidth
        self.__setGridColor__(dc,printing)
        dc.DrawLine(xa,y,xe,y)
        dc.DrawLine(xa,y,xe,y)
        dc.DrawLine(xa,y,xa,y+self.gridY)
        dc.DrawLine(xe,y,xe,y+self.gridY)
        for iHr in range(iStartHr,iEndHr):
            x=iHr*self.gridX+self.ofsX+self.iHrWidth
            dc.DrawLine(x,y,x,y+self.gridY)
        #dc.SetPen(wx.Pen('VIOLET', 1))
        dc.SetFont(wx.Font(7, wx.SWISS, wx.NORMAL, wx.NORMAL))
        self.__setGridTextColor__(dc,printing)
        #dc.SetTextForeground(wx.Colour(0x60, 0x60, 0x60))
        gXh=self.gridX/2
        gYh=self.gridY/2
        for iHr in range(iStartHr,iEndHr):
            x=iHr*self.gridX+self.ofsX+self.iHrWidth
            #dc.DrawLine(x,y,x,y+self.gridY)
            sHr='%02d'%(iHr+self.iOfsHr)
            te = dc.GetTextExtent(sHr)
            x=iHr*self.gridX + gXh - te[0]/2 + 2+ self.ofsX +self.iHrWidth
            y=iDay*(self.gridY+self.gridMinPerDay) + gYh - te[1]/2 + 1
            #if (x>=xLim) and (y>=yLim):
            dc.DrawText(sHr, x, y)
        y=iDay*(self.gridY+self.gridMinPerDay)+self.gridY
        iW=self.gridMinX
        iH=self.gridMin
        for i in range(0,self.iMin):
            x=i*iW+self.ofsX+self.iHrWidth
            #for j in range(self.days):
            #    y=(self.gridY+self.gridMinPerDay)*j+self.gridY
            dc.DrawLine(x,y,x,y+iH+iH)
            dc.DrawLine(x,y,x+iW,y)
            dc.DrawLine(x,y+iH,x+iW,y+iH)
            dc.DrawLine(x,y+iH+iH,x+iW,y+iH+iH)
            
    def __clearHour__(self,iDay,hour,dc,printing):
        r,s=self.__getHourRect__(iDay,hour)
        #color=self.color[oBlk[-2]%7]
        #brush=self.brush[oBlk[-2]%7]
        dc.SetPen(wx.Pen(self.scwDrawing.GetBackgroundColour(),1))
        brush=wx.Brush(self.scwDrawing.GetBackgroundColour())
        dc.SetBrush(brush)
        dc.DrawRectangle(r[0], r[1], r[2]+1, r[3]+1)
        self.__setGridColor__(dc,printing)
        self.__setGridTextColor__(dc,printing)
        #dc.SetPen(wx.Pen(wx.Colour(0xC0, 0xC0, 0xC0), 1))
        self.__updateGrid__(iDay,hour,dc,printing)
        
    def __drawHour__(self,blk,selected,dc,printing):
        vtLog.vtLngCurWX(vtLog.DEBUG,blk,self)
        hour=blk[-1]
        iStartHr=int(hour[0][0:2])-self.iOfsHr
        iEndHr=int(hour[1][0:2])-self.iOfsHr
        iStartMin=int(hour[0][2:4])
        iEndMin=int(hour[1][2:4])
        if iEndMin>0:
            iEndHr+=1
        #-self.iOfsHr
        if iEndHr<0:
            return
        if iStartHr>=self.iShowHr:
            return
        def setPen(selected):
            if self.IsEnabled()==False:
                selected=False
            if selected==False:
                if self.IsEnabled():
                    color=self.color[blk[-2]%7]
                    brush=self.brush[blk[-2]%7]
                    dc.SetBrush(brush)
                else:
                    color=self.colorDis[blk[-2]%7]
                    brush=self.brushDis[blk[-2]%7]
                    dc.SetBrush(brush)
                #dc.DrawRectangle(blk[0], blk[1], blk[2]-blk[0], blk[3]-blk[1])
            else:
                dc.SetPen(wx.Pen('BLUE', 1))
                dc.SetBrush(wx.WHITE_BRUSH)
            return selected
        setPen(selected)
        dc.DrawRectangle(blk[0], blk[1], blk[2]-blk[0], blk[3]-blk[1])
        
        def setPenMin(selected):
            selected=setPen(selected)
            if selected==False:
                self.__setGridColor__(dc,printing)
                self.__setGridTextColor__(dc,printing)
                #dc.SetPen(wx.Pen(wx.Colour(0xC0, 0xC0, 0xC0), 1))
                brush=wx.Brush(self.scwDrawing.GetBackgroundColour())
                dc.SetPen(wx.Pen(self.scwDrawing.GetBackgroundColour()))
                dc.SetBrush(brush)
        iDay=blk[-2]
        hour=blk[-1]
        iStartHr=int(hour[0][0:2])
        iEndHr=int(hour[1][0:2])
        iStartMin=int(hour[0][2:4])
        iEndMin=int(hour[1][2:4])
        
        min=iStartMin/self.minTol
        y=(self.gridY+self.gridMinPerDay)*iDay+self.gridY
        x=self.iHrWidth+self.ofsX+self.gridMinX*min
        if self.iHour==iStartHr:
            setPenMin(selected)
        else:
            setPenMin(False)
        dc.DrawRectangle(x, y, self.gridMinX, self.gridMin)
        y+=self.gridMin
        
        if iEndMin==0:
            iEndMin=60
            iEndHr-=1
        #if iEndHr==iStartHr:
        #    iEndMin=iEndMin/self.iMin
        #else:
        #    iEndMin=59
        min=iEndMin/self.minTol
        min-=1
        x=self.iHrWidth+self.ofsX+self.gridMinX*min
        if self.iHour==iEndHr:
            setPenMin(selected)
        else:
            setPenMin(False)
        dc.DrawRectangle(x, y, self.gridMinX, self.gridMin)
        
        self.__updateGrid__(iDay,hour,dc,printing)
    def __drawGrid__(self,dc,printing):
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        
        self.__setGridColor__(dc,printing)
        
        y=0
        for i in range(0,self.days):
            #y=i*(self.gridY+self.iMin*self.gridMin)
            dc.DrawLine(self.iHrWidth,y,self.maxWidth-(self.iHrWidth),y)
            y+=self.gridY
            for j in range(2):
                #y=i*(self.gridY+j*self.gridMin)
                dc.DrawLine(self.iHrWidth,y,self.maxWidth-(self.iHrWidth+self.sumX),y)
                y+=self.gridMin
        dc.DrawLine(self.iHrWidth,y,self.maxWidth-(self.iHrWidth),y)
        
        dc.DrawLine(self.iHrWidth,0,self.iHrWidth,self.maxHeight)
        y=0
        for i in range(0,self.iShowHr+1):
            x=i*self.gridX+self.ofsX+self.iHrWidth
            for j in range(self.days):
                y=(self.gridY+self.gridMinPerDay)*j
                dc.DrawLine(x,y,x,y+self.gridY)
        y=0
        dc.DrawLine(x,y,x,self.maxHeight)
        if self.bSum:
            x+=self.sumX
            dc.DrawLine(x,y,x,self.maxHeight)
        iW=self.gridMinX
        iH=self.gridMin<<1
        for i in range(0,self.iMin):
            x=i*iW+self.ofsX+self.iHrWidth
            for j in range(self.days):
                y=(self.gridY+self.gridMinPerDay)*j+self.gridY
                dc.DrawLine(x,y,x,y+iH)
        
        self.__drawHourNav__(dc,False)
    def __drawGridText__(self,dc,printing):
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.__setGridColor__(dc,printing)
        y=0
        gXh=self.gridX/2
        gYh=self.gridY/2
        dc.SetFont(wx.Font(7, wx.SWISS, wx.NORMAL, wx.NORMAL))
        self.__setGridTextColor__(dc,printing)
        if self.dayLabels is not None:
            i=0
            for sDay in self.dayLabels:
                te = dc.GetTextExtent(sDay)
                x=2
                y=i*self.gridY+gYh-te[1]/2+1
                #if (x>=xLim) and (y>=yLim):
                dc.DrawText(sDay, x, y)
                i+=1
        for h in range(self.iShowHr):
            sHr='%02d'%(self.iOfsHr+h)
            te = dc.GetTextExtent(sHr)
            for d in range(self.days):
                x=h*self.gridX + gXh - te[0]/2 + 2+ self.ofsX + self.iHrWidth
                y=d*(self.gridY+self.gridMinPerDay) + gYh - te[1]/2 + 1
                #if (x>=xLim) and (y>=yLim):
                dc.DrawText(sHr, x, y)
    def __drawSum__(self,dc,printing):
        if self.lst is None:
            return
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,self.iHrWidth,self)
        if self.bSum==False:
            return
        brush=wx.Brush(self.scwDrawing.GetBackgroundColour())
        dc.SetPen(wx.Pen(self.scwDrawing.GetBackgroundColour(), 1))
        dc.SetBrush(brush)
        x=self.gridX*self.iShowHr+self.ofsX+1+self.iHrWidth
        for day in range(self.days+2):
            y=(self.gridY+self.gridMinPerDay)*day+1
            dc.DrawRectangle(x, y, self.sumX-2, self.gridY-2)
        self.__setGridColor__(dc,printing)
        self.__setGridTextColor__(dc,printing)
            
        iDay=0
        ofsHr=0
        sumTotal=0
        for day in self.lst:
            sum=0
            y=(self.gridY+self.gridMinPerDay)*iDay
            for hour in day:
                rect,val=self.__getHourRect__(iDay,hour,ofsHr)
                sum+=val
            dc.SetFont(wx.Font(7, wx.SWISS, wx.NORMAL, wx.NORMAL))
            #dc.SetTextForeground(wx.Colour(0x0, 0x0, 0x0))
            sTxt="""%4.2f  %02d"%02d'"""%(sum,math.floor(sum),round(sum%1*60))
            te = dc.GetTextExtent(sTxt)
            xS=self.gridX*self.iShowHr+self.ofsX+self.sumX+self.iHrWidth#+self.gridX/2
            #y=self.gridY
            dc.DrawText(sTxt,xS-2-te[0],y+self.gridY/2-te[1]/2+1)
            sumTotal=sumTotal+sum
            iDay+=1
        dc.SetFont(wx.Font(8, wx.SWISS, wx.NORMAL, wx.BOLD))
        #dc.SetTextForeground(wx.Colour(0x0, 0x0, 0x0))
        sum=sumTotal
        sTxt="""%5.2f'"""%(sum)
        te = dc.GetTextExtent(sTxt)
        xS=self.gridX*self.iShowHr+self.ofsX+self.sumX+self.iHrWidth#+self.gridX/2
        y=(self.gridY+self.gridMinPerDay)*self.days#-self.gridY-self.gridY
        dc.DrawText(sTxt,xS-2-te[0],y+self.gridY/2-te[1]/2+1)
        sTxt="""%03d"%02d'"""%(math.floor(sum),round(sum%1*60))
        te = dc.GetTextExtent(sTxt)
        xS=self.gridX*self.iShowHr+self.ofsX+self.sumX+self.iHrWidth#+self.gridX/2
        y=(self.gridY+self.gridMinPerDay)*self.days+self.gridY
        dc.DrawText(sTxt,xS-2-te[0],y+self.gridY/2-te[1]/2+1)

    def OnSize(self,event):
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        s=event.GetSize()
        iW=s[0]
        i=iW-self.ofsX-(self.iHrWidth<<1)-self.sumX-5
        i/=self.gridX
        i-=1
        if i<0:
            i=1
        self.iShowHr=i
        iHrDelta=self.iMaxHr-self.iMinHr
        self.iShowHr=self.__limit__(self.iShowHr,1,iHrDelta)
        if self.iOfsHr+self.iShowHr>=self.iMaxHr+self.iMinHr:
            self.iOfsHr=self.iMaxHr-self.iShowHr
        self.iOfsHr=self.__limit__(self.iOfsHr,self.iMinHr,self.iMaxHr-iHrDelta)
        
        self.maxWidth=self.gridX*self.iShowHr+self.ofsX+self.iHrWidth*2
        self.gridMinX=(self.gridX*self.iShowHr)/self.iMin
        self.__setDrawingSize__()
        self.__invokeRefresh__()
        event.Skip()
        

