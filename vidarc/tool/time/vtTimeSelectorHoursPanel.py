#Boa:FramePanel:vtTimeSelectorHoursPanel
#----------------------------------------------------------------------------
# Name:         vtTimeSelectorHoursPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtTimeSelectorHoursPanel.py,v 1.5 2008/03/26 23:12:09 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
from wx.lib.anchors import LayoutAnchors
import calendar,time
import string,math,traceback
import cStringIO

from vidarc.tool.time.vtTimeSelectorHourDialog import *
import vidarc.tool.log.vtLog as vtLog

VERBOSE=1
BUFFERED=1

#----------------------------------------------------------------------
def getLeftData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x89IDAT8\x8d\xc5\x93;\x0e\x80 \x10Dw\xc1\x13\xab\xf4j\r\x1cB=\x16\'\
\xd8\x92\x0b`\x05\x01B\x08\x9f\xc2IHX\x92\x99y\xc5\x82\xc88\xcc\x88M\xb9G\
\x03\xae\xf3p\xfe\xbe\x8c\x18\x8d1\xe1\xad) n\xccU\r(\x19\xe3\xf6b@\xad\xadJ\
\xd0b\xcc\xdb\x93\x00kmOq\x10\xe6\x8b\xb4ok\x95\x84\x88\x00\x00\xe0~^L\x08\
\xbc\xa4\xd2\xd8\x1aV$()\x0f"\xa2@\xd0\xb4\x89Ri\x8c\xc9R\x04\xc6\xbb\x8f\
\x10\xc2\x85\xf9\xf7\xdf\xf8\x01-\x971\xe7\xc3]\x80\xb0\x00\x00\x00\x00IEND\
\xaeB`\x82' 

def getLeftBitmap():
    return wx.BitmapFromImage(getLeftImage())

def getLeftImage():
    stream = cStringIO.StringIO(getLeftData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getRightData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x80IDAT8\x8d\xc5\x93\xc1\r\x80 \x0cEi\xf1\xdc\x15\xd9\x01\x18C\x1dB\
\x1d\x8b\tX\x02O\x1a \x95\xb4\xe1`\x93&\xbd\xbc\xd7_\x12\x00\xd0\x9a\x99\xc2\
)\xba\x16\xc4\xe0\xcb\x94\x80\x88L\x0c\xbehE\xec\t\x1aQ# "\xb5h\x91l\xa9%\
\xeb\xb6\xc3g\x02.\x05\'\xab\x85l\x82\x9c\xb3$\x18/H)\r\x81\xe3\xbc\x9a\x13D\
o\xd0C\x9f\x82~\xfb\x08\x1c&\x90\x80o\x01Z\x03h\x8ds\xae<\xb3\xaa\x7f\xff\
\x8d7\x00\x9d+\xcf\xef\xe1r/\x00\x00\x00\x00IEND\xaeB`\x82' 

def getRightBitmap():
    return wx.BitmapFromImage(getRightImage())

def getRightImage():
    stream = cStringIO.StringIO(getRightData())
    return wx.ImageFromStream(stream)

# defined event for vgpXmlTree item selected
wxEVT_VTM_SELECTOR_HOUR_SELECTED=wx.NewEventType()
def EVT_VTM_SELECTOR_HOUR_SELECTED(win,func):
    win.Connect(-1,-1,wxVTM_SELECTOR_HOUR_SELECTED,func)
class vTmSelectorHourSelected(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTM_SELECTOR_HOUR_SELECTED(<widget_name>, self.OnInfoSelected)
    """

    def __init__(self,obj,day,hour):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VTM_SELECTOR_HOUR_SELECTED)
        self.obj=obj
        self.day=day
        self.hour=hour
    def GetObject(self):
        return self.obj
    def GetDay(self):
        return self.day
    def GetHour(self):
        return self.hour

# defined event for vgpXmlTree item selected
wxEVT_VTM_SELECTOR_HOUR_CHANGED=wx.NewEventType()
def EVT_VTM_SELECTOR_HOUR_CHANGED(win,func):
    win.Connect(-1,-1,wxVTM_SELECTOR_HOUR_CHANGED,func)
class vTmSelectorHourChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTM_SELECTOR_HOUR_CHANGED(<widget_name>, self.OnInfoChanged)
    """

    def __init__(self,obj,day):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VTM_SELECTOR_HOUR_CHANGED)
        self.obj=obj
        self.day=day
    def GetObject(self):
        return self.obj
    def GetDay(self):
        return self.day

class vtTimeSelectorHoursPanel(wx.Panel):
    def _init_ctrls(self, prnt, id, pos, size, style,name=u'vgTmSelectorDay'):
        # generated method, don't edit
        wx.Panel.__init__(self, id=id,
              name=name, parent=prnt, pos=pos,size=size, style=style)
        #self.SetClientSize(wx.Size(492, 273))
        self.SetAutoLayout(True)
        self.Bind(wx.EVT_SIZE, self.OnSize)
        if 1==0:
            self.cbDec = wx.lib.buttons.GenBitmapButton(ID=-1,
                  bitmap=getLeftBitmap(), name=u'cbDec',
                  parent=self, pos=(0,(size[1]>>1)-12), size=(24,24), style=wx.BU_AUTODRAW)
            self.cbDec.SetConstraints(LayoutAnchors(self.cbDec, True, True,
                  False, True))
            self.cbDec.Bind(wx.EVT_BUTTON, self.OnDec,self.cbDec)
            self.cbInc = wx.lib.buttons.GenBitmapButton(ID=-1,
                  bitmap=getRightBitmap(), name=u'cbInc',
                  parent=self, pos=(size[0]-24,(size[1]>>1)-12), size=(24,24), style=wx.BU_AUTODRAW)
            self.cbInc.SetConstraints(LayoutAnchors(self.cbInc, False, True,
                  True, True))
            self.cbInc.Bind(wx.EVT_BUTTON, self.OnInc,self.cbInc)
            sz=(size[0]-48,size[1])
            iX=24
        else:
            iX=0
            sz=size

        self.scwDrawing = wx.ScrolledWindow(id=wx.NewId(),
              name=u'scwDay', parent=self, pos=wx.Point(iX, 0),
              size=sz, style=0)#wx.HSCROLL | wx.VSCROLL)
        #self.scwDrawing.SetAutoLayout(False)
        self.scwDrawing.SetConstraints(LayoutAnchors(self.scwDrawing, True, True,
              True, True))
        self.scwDrawing.Bind(wx.EVT_LEAVE_WINDOW, self.OnKillFocus)
            
    def __init__(self, parent, days, hours, start_hour=0,min_hour=0,max_hour=24,
                show_hours=-1, sum=True, dayLabels=None, 
                id=wx.NewId(), pos=wx.DefaultPosition, size=wx.Size(100, 50), 
                style=wx.TAB_TRAVERSAL, name=u'vgTmSelectorDay',gridDay=15,gridHr=15):
        self._init_ctrls(parent,id,pos,size,style,name)
        self.doc=None
        self.dlg=None
        self.buffer=None
        self.verbose=VERBOSE
        
        self.__setDftColors__()
        self.lst=None
        self.drawing=False
        self.selectableBlks=[]
        self.selectedBlk=None
        
        self.days=days
        self.dayLabels=dayLabels
        self.bSum=sum
        self.iStartHr=start_hour
        self.iShowHr=show_hours
        self.iMinHr=min_hour
        self.iMaxHr=max_hour
        self.hours=hours
        self.gridHr=gridHr
        self.maxHrs=1
        if self.dayLabels is not None:
            #dc = wx.PaintDC(self)
            #dc.SetFont(wx.Font(7, wx.SWISS, wx.NORMAL, wx.NORMAL))
            #dc.SetTextForeground(wx.Colour(0x80, 0x80, 0x80))
            #self.ofsX=0
            #for sDay in self.dayLabels:
            #    te = dc.GetTextExtent(sDay)
            #    if te[0]>self.ofsX:
            #        self.ofsX=te[0]
            #self.ofsX+=4
            self.ofsX=25
        else:
            self.ofsX=0
        self.gridX=self.gridHr*self.maxHrs
        #def grid y
        
        self.gridY=gridDay
        #self.countWidth=25
        #self.countHeight=1
        
        self.__validateCfg__()
        self.__initDayLst__()
        
        self.bShowHrChg=False
        self.iHrWidth=0
        if self.iShowHr<self.iMaxHr-self.iMinHr:
            #self.ofsX+=25
            self.bShowHrChg=True
            self.iHrWidth=25

        self.iOfsHr=self.iStartHr
        
        self.iLastDay=-1
        self.iLastHr=-1
        self.iLastX=-1
        self.iLastY=-1
        self.bLastSel=False

        self.sumX=60
        self.sumY=0
        self.legendX=80
        if sum:
            #self.maxWidth+=self.sumX
            self.sumY=self.gridY<<1
        else:
            self.sumX=0
        self.scwDrawing.Bind(wx.EVT_LEFT_DOWN, self.OnLeftButtonEvent)
        self.scwDrawing.Bind(wx.EVT_LEFT_UP,   self.OnLeftButtonEvent)
        self.scwDrawing.Bind(wx.EVT_MOTION,    self.OnMoveEvent)
        self.scwDrawing.Bind(wx.EVT_RIGHT_DOWN,self.OnRightButtonEvent)
        self.scwDrawing.Bind(wx.EVT_PAINT, self.OnPaint)
        self.__setDrawingSize__()
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
    def __setDrawingSize__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.maxWidth=self.gridX*self.iShowHr+self.ofsX+self.iHrWidth*2+self.sumX
        self.maxHeight=self.gridY*self.days
        self.iXNavHrDec=self.iHrWidth
        self.iXNavHrInc=self.ofsX+self.gridX*self.iShowHr+self.sumX+self.iHrWidth
        self.iDrawWidth=self.gridX*self.iShowHr
        
        self.scwDrawing.SetVirtualSize((self.maxWidth+5, self.maxHeight+5+self.sumY))
        self.scwDrawing.SetScrollRate(20,20)
        if BUFFERED:
            # Initialize the buffer bitmap.  No real DC is needed at this point.
            self.buffer = wx.EmptyBitmap(self.maxWidth+1, self.maxHeight+1+self.sumY)
            dc = wx.BufferedDC(None, self.buffer)
            dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
            dc.Clear()
            self.DoDrawing(dc)
        
    def __limit__(self,val,min,max):
        if val<min:
            return min
        if val>max:
            return max
        return val
    def __validateCfg__(self):
        self.hours=self.__limit__(self.hours,1,23)
        self.iMinHr=self.__limit__(self.iMinHr,0,23)
        self.iMaxHr=self.__limit__(self.iMaxHr,0,24)
        self.iShowHr=self.__limit__(self.iShowHr,1,self.iMaxHr-self.iMinHr)
    def __initDayLst__(self):
        self.lst=[]
        for i in range(self.days):
            self.lst.append([])
            
    def GetDlg(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.dlg is None:
            self.dlg=vtTimeSelectorHourDialog(self)
        return self.dlg
    def __setDftColors__(self):
        self.color=[]
        self.color.append(wx.Colour(255, 154, 000))
        self.color.append(wx.Colour(255, 207, 000))
        self.color.append(wx.Colour(102, 205, 170))
        self.color.append(wx.Colour(176, 196, 222))
        self.color.append(wx.Colour(144, 238, 144))
        self.color.append(wx.Colour(218, 165, 32))
        self.color.append(wx.Colour(226, 126, 226))
        
        self.colorDis=[]
        self.colorDis.append(wx.Colour(167, 100, 000))
        self.colorDis.append(wx.Colour(167, 136, 000))
        self.colorDis.append(wx.Colour( 67, 135, 112))
        self.colorDis.append(wx.Colour(116, 130, 147))
        self.colorDis.append(wx.Colour(100, 166, 100))
        self.colorDis.append(wx.Colour(150, 114,  22))
        self.colorDis.append(wx.Colour(147,  82, 147))
        
        self.colorNav=[wx.Colour(174, 174, 174),wx.Colour(128, 128, 128)]
        self.__setDftBrush__()
        self.__setDftPen__()
        
    def __setDftBrush__(self):
        self.brush=[]
        for c in self.color:
            self.brush.append(wx.Brush(c))
        self.brushDis=[]
        for c in self.colorDis:
            self.brushDis.append(wx.Brush(c))
        self.brushNav=[wx.Brush(self.colorNav[0]),wx.Brush(self.colorNav[1])]
    def __setDftPen__(self):
        self.pen=[]
        for c in self.color:
            self.pen.append(wx.Pen(c,1))
        self.penDis=[]
        for c in self.colorDis:
            self.penDis.append(wx.Pen(c,1))
        self.penNav=[wx.Pen(self.colorNav[0],1),wx.Pen(self.colorNav[1],1)]
    def __clrXYrel__(self):
        #vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.iNav=0
        self.iDay=-1
        self.iHr=-1
        self.bKeepSel=False
    def SetXY(self, event):
        self.x, self.y = self.ConvertEventCoords(event)
        #if self.verbose:
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.__calcXYrel__()
    def __calcXYrel__(self):
        self.__clrXYrel__()
        if self.x<self.iXNavHrDec:
            self.iNav=-1
            return
        elif self.x>self.iXNavHrInc:
            self.iNav=1
            return
        self.iDay=self.y/self.gridY
        if self.iDay>=self.days:
            self.iDay=-1
        self.iHour=((self.x-self.ofsX-self.iHrWidth)/self.gridX)+self.iOfsHr
        if self.iHour>=self.hours:
            self.iHour=-1
        self.bKeepSel=False
    def __getHourXYrel__(self):
        return ['%02d%02d'%(self.iHour,0),'%02d%02d'%(self.iHour+1,0)]
    def __createBlkByXY__(self):
        blk=self.__setHour__(self.iDay,self.__getHourXYrel__())
        return blk
    def __checkSel__(self,dc,printing):
        blk=self.__getSelectableBlk__(self.x,self.y)
        self.bLastSel=False
        #print blk
        if blk is not None:
            self.iLastDay=self.iDay
            self.iLastHr=self.iHour
            self.iLastX=self.x
            self.iLastY=self.y
        else:
            iDiff=self.gridX
            if iDiff<15:
                iDiff=15
            if abs(self.iLastX-self.x)>iDiff or abs(self.iLastY-self.y)>iDiff:
                self.iLastDay=-1
                self.iLastHr=-1
                self.iLastX=-1
                self.iLastY=-1
            else:
                #blk=self.selectedBlk
                #vtLog.vtLngCurWX(vtLog.DEBUG,'keep last',self)
                #print self.iDay,self.iHour,self.x,self.y,self.bKeepSel
                #print self.iLastDay,self.iLastHr,self.iLastX,self.iLastY
                self.iDay,self.iHour=self.iLastDay,self.iLastHr
                self.bLastSel=True
                #self.iDay,self.iHour,self.x,self.y=self.iLastDay,self.iLastHr,self.iLastX,self.iLastY
                #blk=self.__getSelectableBlk__(self.x,self.y)
                
                return
        #print self.iDay,self.iHour,self.x,self.y,self.bKeepSel
        #print self.iLastDay,self.iLastHr,self.iLastX,self.iLastY
        #print 'sel',self.selectedBlk
        if blk is None:
            if self.selectedBlk is not None and self.bKeepSel==False:
                if self.verbose:
                    vtLog.vtLngCurWX(vtLog.DEBUG,self.selectedBlk,self)
                    vtLog.vtLngCurWX(vtLog.DEBUG,self.bKeepSel,self)
                
                self.__drawHour__(self.selectedBlk,False,dc,False)
                self.selectedBlk=None
        else:
            #self.bLast
            if self.selectedBlk is not None and self.bKeepSel==False:
                if self.selectedBlk!=blk:
                    # paint old
                    self.__drawHour__(self.selectedBlk,False,dc,printing)
                #else:
                #    if self.iHour>=0 and self.iHour!=iHour:
                        #self.__drawHour__(self.selectedBlk,False,dc,False)
                #        self.__drawHour__(self.selectedBlk,True,dc,False)
                #else:
            if self.bKeepSel==False:
                if self.verbose:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'select:%s,old:%s'%(blk,self.selectedBlk),self)
                    
                self.selectedBlk=blk
                self.__drawHour__(self.selectedBlk,True,dc,printing)
    def __check__(self):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        try:
            for i in range(self.days):
                day=self.lst[i]
                l=[]
                for hour in day:
                    iHrS=int(hour[0][0:2])
                    iMnS=int(hour[0][2:4])
                    iHrE=int(hour[1][0:2])
                    iMnE=int(hour[1][2:4])
                    l.append([iHrS,iMnS,iHrE,iMnE,hour])
                    
                print day
                print l
                def cmpFunc(a,b):
                    ret=cmp(a[0],b[0])
                    if ret==0:
                        return cmp(a[1],b[1])
                    return ret
                l.sort(cmpFunc)
                
        except:
            vtLog.vtLngTB(self.GetName())
    def __checkHour__(self,blk,hour,iDay2Check,dc=None,printing=False):
        vtLog.vtLngCurWX(vtLog.DEBUG,iDay2Check,self)
        try:
            day=self.lst[iDay2Check]
            vtLog.vtLngCurWX(vtLog.DEBUG,day,self)
            sHrS=hour[0]
            sHrE=hour[1]
            if self.verbose:
                vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%repr(blk),self)
            tup=blk[-1]
            h=[]
            if self.verbose:
                vtLog.vtLngCurWX(vtLog.DEBUG,'    blk:%s'%repr(blk),self)
                vtLog.vtLngCurWX(vtLog.DEBUG,'%s;%s;%s'%(tup,sHrS,sHrE),self)
            if tup[0]==sHrS:
                if tup[1]>sHrE:
                    h.append([sHrE,tup[1]])
            elif tup[1]==sHrE:
                if tup[0]<sHrS:
                    h.append([tup[0],sHrS])
            else:
                h.append([tup[0],sHrS])
                h.append([sHrE,tup[1]])
            try:
                day.remove(tup)
                self.selectableBlks.remove(blk)
            except Exception,list:
                vtLog.vtLngTB(self.GetName())
                vtLog.vtLngCurWX(vtLog.ERROR,'   tup %s;blk %s'%(repr(tup),repr(blk)),self)
            for tup in h:
                blk=self.__setHour__(self.iDay,tup)
                if dc is not None:
                    self.__drawHour__(blk,False,dc,False)
            if dc is not None:
                #print 'clear'
                self.__clearHour__(self.iDay,[sHrS,sHrE],dc,False)
            blk=None
            return blk
        except:
            vtLog.vtLngTB(self.GetName())
        return blk
    def ConvertEventCoords(self, event):
        xView, yView = self.scwDrawing.GetViewStart()
        xDelta, yDelta = self.scwDrawing.GetScrollPixelsPerUnit()
        return (event.GetX() + (xView * xDelta),
                event.GetY() + (yView * yDelta))
    def SelectValue(self,day,hr):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if BUFFERED:
            # If doing buffered drawing, create the buffered DC, giving it
            # it a real DC to blit to when done.
            cdc = wx.ClientDC(self.scwDrawing)
            self.scwDrawing.DoPrepareDC(cdc)
            dc = wx.BufferedDC(cdc, self.buffer)
            dc.BeginDrawing()
        else:
            dc = wx.ClientDC(self)
            self.PrepareDC(dc)
        
        if self.selectedBlk is not None:
            # paint old
            self.__drawHour__(self.selectedBlk,False,dc,False)
            #oBlk=self.selectedBlk
            #color=self.color[oBlk[-2]%7]
            #brush=self.brush[oBlk[-2]%7]
            #dc.SetBrush(brush)
            #dc.DrawRectangle(oBlk[0], oBlk[1], 
            #                    oBlk[2]-oBlk[0], oBlk[3]-oBlk[1])
        self.selectedBlk=None
        blk=self.__getSelectableBlk__(day,hr)
        if blk is not None:
            self.__drawHour__(blk,True,dc,False)
            #dc.SetPen(wx.Pen('BLUE', 1))
            #dc.SetBrush(wx.LIGHT_GREY_BRUSH)
            #dc.DrawRectangle(blk[0], blk[1], 
            #                    blk[2]-blk[0], blk[3]-blk[1])
            self.selectedBlk=blk
            wx.PostEvent(self,vTmSelectorHourSelected(self,day,hr))
        #self.selectedBlk=node
        self.__drawGrid__(dc,False)
        self.__drawGridText__(dc,False)
        dc.EndDrawing()
    def __getSelectableBlk__(self,x,y):
        for blks in self.selectableBlks:
            if (x>blks[0]) and (x<blks[2]):
                if (y>blks[1]) and (y<blks[3]):
                    return blks
        return None
    def __getSelectableIdx__(self,day,hour):
        idx=0
        for tup in self.selectableBlks:
            if tup[-2]==day:
                if tup[-1]==hour:
                    return idx
            idx+=1
        return -1
    def __setHour__(self,iDay,hour):
        try:
            day=self.lst[iDay]
            idx=0
            idxSel=-1
            for hr in day:
                if (hour[0]>=hr[0]) and (hour[0]<=hr[1]):
                    # start inside
                    if hour[1]<=hr[1]:
                        hour=None
                        break
                    else:
                        idxSel=self.__getSelectableIdx__(iDay,hr)
                        if self.verbose:
                            vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%repr(idxSel),self)
                        hr[1]=hour[1]
                        hour=hr
                        #break
                if (hour[1]>=hr[0]) and (hour[1]<=hr[1]):
                    #end inside
                    if hour[0]<hr[0]:
                        hr[0]=hour[0]
                        idxSel=self.__getSelectableIdx__(iDay,hr)
                        hr[0]=hour[0]
                        hour=hr
                        #break
            if hour is not None:
                rect,sum=self.__getHourRect__(iDay,hour)
                if idxSel>=0:
                    r=self.selectableBlks[idxSel]
                    if self.verbose:
                        vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%repr(r),self)
                    r[0]=rect[0]
                    r[1]=rect[1]
                    r[2]=rect[0]+rect[2]
                    r[3]=rect[1]+rect[3]
                    if self.verbose:
                        vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%repr(r),self)
                    for i in range(len(day)-1):
                        hr1=day[i]
                        hr2=day[i+1]
                        if hr1[0]==hr2[0]:
                            if hr1[1]<hr2[1]:
                                idxSel=self.__getSelectableIdx__(iDay,hr1)
                                if self.verbose:
                                    vtLog.vtLngCurWX(vtLog.DEBUG,'hr1:%s hr2:%s idx:%d'%(hr1,hr2,idxSel),self)
                                self.selectableBlks=self.selectableBlks[:idxSel]+self.selectableBlks[idxSel+1:]
                                day.remove(hr1)
                                break
                    return r
                else:
                    day.append(hour)
                    def compFunc(a,b):
                        return cmp(a[0],b[0])
                    day.sort(compFunc)
                    self.lst[iDay]=day
                    r=[rect[0], rect[1], rect[0] + rect[2], rect[1] + rect[3] , iDay,hour]
                    self.selectableBlks.append(r)
                    if self.verbose:
                        vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%repr(r),self)
                    return r
        except Exception,list:
            vtLog.vtLngTB(self.GetName())
            return None
    def OnLeftButtonEvent(self, event):
        event.Skip()
        #vtLog.CallStack('')
        if event.LeftDown():
            self.SetFocus()
            self.SetXY(event)
            if self.iNav==-1:
                ofs=self.iOfsHr-1
                self.iOfsHr=self.__limit__(ofs,self.iMinHr,self.iMaxHr)
                if self.iOfsHr==ofs:
                    self.__invokeRefresh__()
                    return
            elif self.iNav==1:
                ofs=self.iOfsHr+1
                self.iOfsHr=self.__limit__(ofs,self.iMinHr,self.iMaxHr-self.iShowHr)
                if self.iOfsHr==ofs:
                    self.__invokeRefresh__()
                    return
            
            if self.verbose:
                vtLog.vtLngCurWX(vtLog.DEBUG,'left down',self)
                vtLog.vtLngCurWX(vtLog.DEBUG,'x:%d y:%d'%(self.x,self.y),self)
                vtLog.vtLngCurWX(vtLog.DEBUG,'shift:%d ctrl:%d'%(event.ShiftDown(),event.ControlDown()),self)
                vtLog.vtLngCurWX(vtLog.DEBUG,'day:%d hour:%d'%(self.iDay,self.iHour),self)
                vtLog.vtLngCurWX(vtLog.DEBUG,vtLog.pformat(self.selectableBlks),self)
                vtLog.vtLngCurWX(vtLog.DEBUG,vtLog.pformat(self.lst),self)
            if self.iDay==-1 or self.iHour==-1:
                #if self.selectedBlk is not None:
                #    cdc = wx.ClientDC(self.scwDrawing)
                #    self.scwDrawing.DoPrepareDC(cdc)
                #    dc = wx.BufferedDC(cdc, self.buffer)
                #    dc.BeginDrawing()
                #    self.__drawHour__(self.selectedBlk,False,dc,False)
                #    dc.EndDrawing()
                #    self.selectedBlk=None
                return
            self.drawing = True
            bUpdateSum=False
            if self.verbose:
                vtLog.vtLngCurWX(vtLog.DEBUG,'at begin lst:%s'%repr(self.lst[self.iDay]),self)
            blk=self.__getSelectableBlk__(self.x,self.y)
            if blk is None:
                blk=self.__createBlkByXY__()
                if self.verbose:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%repr(blk),self)
                bUpdateSum=True
                wx.PostEvent(self,vTmSelectorHourChanged(self,self.iDay))
                if self.selectedBlk==blk:
                    self.selectedBlk=None
            if blk is not None:
                if BUFFERED:
                    # If doing buffered drawing, create the buffered DC, giving it
                    # it a real DC to blit to when done.
                    cdc = wx.ClientDC(self.scwDrawing)
                    self.scwDrawing.DoPrepareDC(cdc)
                    dc = wx.BufferedDC(cdc, self.buffer)
                    dc.BeginDrawing()
                    if self.selectedBlk is not None:
                        if self.selectedBlk!=blk:
                            # paint old
                            self.__drawHour__(self.selectedBlk,False,dc,False)
                        else:
                            bUpdateSum=True
                            blk=self.__checkHour__(blk,self.__getHourXYrel__(),self.iDay,dc,False)
                            wx.PostEvent(self,vTmSelectorHourChanged(self,self.iDay))
                    if blk is not None:
                        #print 'selec',blk
                        #bUpdateSum=True
                        self.__drawHour__(blk,True,dc,False)
                    if self.verbose:
                        vtLog.vtLngCurWX(vtLog.DEBUG,'    blk:%s'%repr(blk),self)
                    if self.verbose:
                        vtLog.vtLngCurWX(vtLog.DEBUG,'    blk:%s'%repr(blk),self)
                    if bUpdateSum:
                        self.__drawSum__(dc,False)
                    dc.EndDrawing()
                    self.selectedBlk=blk
                    if self.selectedBlk is not None:
                        wx.PostEvent(self,vTmSelectorHourSelected(self,self.iDay,self.selectedBlk[-1]))
                    else:
                        wx.PostEvent(self,vTmSelectorHourSelected(self,self.iDay,None))
            if self.verbose:
                vtLog.vtLngCurWX(vtLog.DEBUG,'at end   lst:%s'%repr(self.lst[self.iDay]),self)
                vtLog.vtLngCurWX(vtLog.DEBUG,'    blk:%s'%repr(blk),self)
            pass
        elif event.Dragging() and self.drawing:
            #print 'dragging'
            if BUFFERED:
                # If doing buffered drawing, create the buffered DC, giving it
                # it a real DC to blit to when done.
                cdc = wx.ClientDC(self)
                self.PrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
            else:
                dc = wx.ClientDC(self)
                self.PrepareDC(dc)

            #dc.BeginDrawing()
            #dc.SetPen(wx.Pen('MEDIUM FOREST GREEN', 4))
            #coords = (self.x, self.y) + self.ConvertEventCoords(event)
            #self.curLine.append(coords)
            #dc.DrawLine(*coords)
            #self.SetXY(event)
            #dc.EndDrawing()


        elif event.LeftUp() and self.drawing:
            #self.lines.append(self.curLine)
            #self.curLine = []
            #self.ReleaseMouse()
            self.drawing = False
            self.scwDrawing.Refresh()
            #xView, yView = self.scwDrawing.GetViewStart()
            #xDelta, yDelta = self.scwDrawing.GetScrollPixelsPerUnit()
            #print xView,yView,xDelta,yDelta
            #s=self.scwDrawing.GetSize()
            #self.scwDrawing.RefreshRect(wx.Rect(xView*xDelta,yView*yDelta,
            #        s[0],s[1]))
            pass
    def OnMoveEvent(self,event):
            event.Skip()
            try:
                self.SetXY(event)
                cdc = wx.ClientDC(self.scwDrawing)
                self.scwDrawing.DoPrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
                dc.BeginDrawing()
                if self.iNav==-1:
                    ofs=self.iOfsHr-1
                    ofsNew=self.__limit__(ofs,self.iMinHr,self.iMaxHr)
                    self.__drawHourNav__(dc,ofsNew==ofs,1)
                elif self.iNav==1:
                    ofs=self.iOfsHr+1
                    ofsNew=self.__limit__(ofs,self.iMinHr,self.iMaxHr-self.iShowHr)
                    self.__drawHourNav__(dc,ofsNew==ofs,2)
                else:
                    self.__drawHourNav__(dc,False,3)
                    if self.iDay==-1 or self.iHour==-1 and self.bKeepSel==False:
                        if self.selectedBlk is not None:
                            #cdc = wx.ClientDC(self.scwDrawing)
                            #self.scwDrawing.DoPrepareDC(cdc)
                            #dc = wx.BufferedDC(cdc, self.buffer)
                            #dc.BeginDrawing()
                            self.__drawHour__(self.selectedBlk,False,dc,False)
                            self.selectedBlk=None
                        dc.EndDrawing()
                        return
                    self.drawing = True
                    bUpdateSum=False
                    #if self.verbose:
                    #    vtLog.vtLngCurWX(vtLog.DEBUG,'at begin lst:%s'%repr(self.lst[iDay]),self)
                    blk=self.__getSelectableBlk__(self.x,self.y)
                    #print blk
                    
                    if blk is None:
                        if self.selectedBlk is not None and self.bKeepSel==False:
                            vtLog.vtLngCurWX(vtLog.DEBUG,self.selectedBlk,self)
                            vtLog.vtLngCurWX(vtLog.DEBUG,self.bKeepSel,self)
                            
                            self.__drawHour__(self.selectedBlk,False,dc,False)
                            self.selectedBlk=None
                    else:
                        if self.selectedBlk is not None and self.bKeepSel==False:
                            if self.selectedBlk!=blk:
                                # paint old
                                self.__drawHour__(self.selectedBlk,False,dc,False)
                        else:
                            self.selectedBlk=blk
                            self.__drawHour__(self.selectedBlk,True,dc,False)
                    #    hour=['%02d%02d'%(iHour,0),'%02d%02d'%(iHour+1,0)]
                    #    blk=self.__setHour__(iDay,hour)
                    #    if self.verbose:
                    #        vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%repr(blk),self)
                    #    bUpdateSum=True
                    #    wx.PostEvent(self,vTmSelectorHourChanged(self,iDay))
                    #    if self.selectedBlk==blk:
                    #        self.selectedBlk=None
                    #print blk
                    #if blk is not None:
                    #    if BUFFERED:
                dc.EndDrawing()
            except:
                pass
            #self.x
            #iDay=self.y/self.gridY
    def OnKillFocus(self,evt):
        try:
            cdc = wx.ClientDC(self.scwDrawing)
            self.scwDrawing.DoPrepareDC(cdc)
            dc = wx.BufferedDC(cdc, self.buffer)
            dc.BeginDrawing()
            self.__drawHourNav__(dc,False,3)
            if self.selectedBlk is not None:
                self.__drawHour__(self.selectedBlk,False,dc,False)
            self.selectedBlk=None
            dc.EndDrawing()
        except:
            pass
        evt.Skip()
    def OnDec(self,evt):
        ofs=self.iOfsHr-1
        self.iOfsHr=self.__limit__(ofs,self.iMinHr,self.iMaxHr)
        if self.iOfsHr==ofs:
            self.__invokeRefresh__()
        evt.Skip()
    def OnInc(self,evt):
        ofs=self.iOfsHr+1
        self.iOfsHr=self.__limit__(ofs,self.iMinHr,self.iMaxHr-self.iShowHr)
        if self.iOfsHr==ofs:
            self.__invokeRefresh__()
        evt.Skip()
    def __invokeRefresh__(self):
            if wx.Thread_IsMain()==False:
                vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
            cdc = wx.ClientDC(self.scwDrawing)
            self.scwDrawing.DoPrepareDC(cdc)
            dc = wx.BufferedDC(cdc, self.buffer)
            #self.OnPaint(None)
            dc.BeginDrawing()
            self.__clear__(dc,False)
            self.__drawValues__(dc,printing=False)
            #self.__drawGrid__(dc,printing=printing)
            #self.__drawGridText__(dc,printing=printing)
            #self.__drawLegend__(dc,printing=printing)
            dc.EndDrawing()
            #self.scwDrawing.Refresh()
                    
    def OnRightButtonEvent(self, event):
        if event.RightDown():
            self.SetFocus()
            self.SetXY(event)
            iDay=self.y/self.gridY
            if iDay>self.days:
                iDay=-1
            iHour=(self.x-self.ofsX)/self.gridX
            if iHour>self.hours:
                iHour=-1
            if self.verbose:
                vtLog.vtLngCurWX(vtLog.DEBUG,'right down',self)
                vtLog.vtLngCurWX(vtLog.DEBUG,'x:%d y:%d'%(self.x,self.y),self)
                vtLog.vtLngCurWX(vtLog.DEBUG,'shift:%d ctrl:%d'%(event.ShiftDown(),event.ControlDown()),self)
                vtLog.vtLngCurWX(vtLog.DEBUG,'day:%d hour:%d'%(iDay,iHour),self)
            #blk=self.__getSelectableBlk__(self.x,self.y)
            #if blk is not None:
                #cdc = wx.ClientDC(self.scwDrawing)
                #self.scwDrawing.DoPrepareDC(cdc)
                #dc = wx.BufferedDC(cdc, self.buffer)
                #dc.BeginDrawing()
                #if self.selectedBlk is not None:
                #    self.__drawHour__(self.selectedBlk,False,dc,False)
                #self.selectedBlk=blk
                #self.__drawHour__(blk,True,dc,False)
                #dc.EndDrawing()
            #    wx.CallAfter(self.__openDlg__)
            #else:
            #wx.CallAfter(self.__openDlg__)
            self.__openDlg__()
        event.Skip()
                
    def __openDlg__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        #vtLog.CallStack('')
        #print self.selectableBlks
        iDay=self.y/self.gridY
        if iDay>self.days:
            iDay=-1
        iHour=(self.x-self.ofsX-self.iHrWidth)/self.gridX+self.iOfsHr
        if iHour>self.hours:
            iHour=-1
            
        dlg=self.GetDlg()
        dlg.Centre()
        blk=self.selectedBlk
        if blk is not None:
            dlg.SetValue(blk[-1][0],blk[-1][1])
        else:
            #print iHour
            dlg.SetValue('%02d00'%iHour,'%02d00'%(iHour+1))
        if dlg.ShowModal()>0:
            try:
                cdc = wx.ClientDC(self.scwDrawing)
                self.scwDrawing.DoPrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
                dc.BeginDrawing()
                zStart,zEnd=dlg.GetValue()
                hour=[zStart,zEnd]
                vtLog.vtLngCurWX(vtLog.DEBUG,'ok;%s'%repr(hour),self)
                #print hour
                #print self.selectableBlks
                #print hour
                day=self.lst[iDay]
                #print 'before',day
                if blk is not None:
                    vtLog.vtLngCurWX(vtLog.DEBUG,blk,self)
                    vtLog.vtLngCurWX(vtLog.DEBUG,self.selectableBlks,self)
#                    self.selectableBlks.append(blkEdit)
#                try:
                    for hr in day:
                        idxSel=self.__getSelectableIdx__(iDay,hr)
                        blk=self.selectableBlks[idxSel]
                        self.__clearHour__(iDay,blk[-1],dc,False)
                    idxSel=self.__getSelectableIdx__(iDay,blk[-1])
                    blk=self.selectableBlks[idxSel]
                    #print blk
                    #print 'before',self.lst[iDay]
                    self.lst[iDay].remove(blk[-1])
                    #print 'after',self.lst[iDay]
                    self.selectableBlks=self.selectableBlks[:idxSel]+self.selectableBlks[idxSel+1:]
                    vtLog.vtLngCurWX(vtLog.DEBUG,self.selectableBlks,self)
                blkEdit=self.__setHour__(iDay,hour)
                #except:
                #    pass
                #print blkEdit
                day=self.lst[iDay]
                #print 'after',day
                dc.SetPen(wx.Pen('BLACK', 1))
                for hr in day:
                    idxSel=self.__getSelectableIdx__(iDay,hr)
                    blk=self.selectableBlks[idxSel]
                    #print blk
                    self.__drawHour__(blk,False,dc,False)
                    #color=self.color[blk[-2]%7]
                    #brush=self.brush[blk[-2]%7]
                    #dc.SetBrush(brush)
                    #dc.DrawRectangle(blk[0], blk[1], 
                    #        blk[2]-blk[0], blk[3]-blk[1])
                    #self.__updateGrid__(blk[-1],dc,False)
                if blkEdit is not None:
                    #self.__drawHour__(blkEdit,True,dc,False)
                    self.__drawHour__(blkEdit,False,dc,False)
                vtLog.vtLngCurWX(vtLog.DEBUG,self.selectableBlks,self)
                #self.__drawSum__(dc,False)
                #self.selectedBlk=blkEdit
                self.selectedBlk=None
                wx.PostEvent(self,vTmSelectorHourChanged(self,iDay))
            except:
                vtLog.vtLngTB(self.GetName())
            dc.EndDrawing()
                
    def OnPaint(self, event):
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,'buffer:%d'%BUFFERED,self)
        event.Skip()
        #return
        try:
            if BUFFERED:
                # Create a buffered paint DC.  It will create the real
                # wx.PaintDC and then blit the bitmap to it when dc is
                # deleted.  Since we don't need to draw anything else
                # here that's all there is to it.
                dc = wx.BufferedPaintDC(self.scwDrawing, self.buffer, wx.BUFFER_VIRTUAL_AREA)
            else:
                dc = wx.PaintDC(self.scwDrawing)
                self.PrepareDC(dc)
                # since we're not buffering in this case, we have to
                # paint the whole window, potentially very time consuming.
                self.DoDrawing(dc)
            #if self.verbose:
            #    print
            #    print
        except:
            #vtLog.vtLngTB(self.GetName())
            #traceback.print_exc()
            pass

    def DoDrawing(self, dc, printing=False):
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        dc.BeginDrawing()
        try:
            self.__drawValues__(dc,printing=printing)
            self.__drawGrid__(dc,printing=printing)
            self.__drawGridText__(dc,printing=printing)
            #self.__drawLegend__(dc,printing=printing)
        except:
            vtLog.vtLngTB(self.GetName())
        dc.EndDrawing()

    
    def __clear__(self,dc,printing):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        brush=wx.Brush(self.scwDrawing.GetBackgroundColour())
        dc.SetBrush(brush)
        dc.SetPen(wx.Pen(self.scwDrawing.GetBackgroundColour(), 1))
        dc.DrawRectangle(0, 0, self.maxWidth+1, self.maxHeight+1+self.sumY)
        self.__drawGrid__(dc,printing=printing)
        self.__drawGridText__(dc,printing=printing)
        
    def __clearHour__(self,iDay,hour,dc,printing):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        r,s=self.__getHourRect__(iDay,hour)
        #color=self.color[oBlk[-2]%7]
        #brush=self.brush[oBlk[-2]%7]
        self.__setGridColor__(dc,printing)
        self.__setGridTextColor__(dc,printing)
        #dc.SetPen(wx.Pen(wx.Colour(0xC0, 0xC0, 0xC0), 1))
        brush=wx.Brush(self.scwDrawing.GetBackgroundColour())
        dc.SetBrush(brush)
        dc.DrawRectangle(r[0], r[1], r[2]+1, r[3]+1)
        self.__updateGrid__(iDay,hour,dc,printing)
        
    def __drawHour__(self,blk,selected,dc,printing):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        vtLog.vtLngCurWX(vtLog.DEBUG,blk,self)
        hour=blk[-1]
        iStartHr=int(hour[0][0:2])-self.iOfsHr
        iEndHr=int(hour[1][0:2])-self.iOfsHr
        iEndMin=int(hour[1][2:4])
        if iEndMin>0:
            iEndHr+=1
        #-self.iOfsHr
        if iEndHr<0:
            return
        if iStartHr>=self.iShowHr:
            return
        if self.IsEnabled()==False:
            selected=False
        if selected==False:
            if self.IsEnabled():
                color=self.color[blk[-2]%7]
                brush=self.brush[blk[-2]%7]
                dc.SetBrush(brush)
            else:
                color=self.colorDis[blk[-2]%7]
                brush=self.brushDis[blk[-2]%7]
                dc.SetBrush(brush)
            dc.DrawRectangle(blk[0], blk[1], blk[2]-blk[0], blk[3]-blk[1])
        else:
            dc.SetPen(wx.Pen('BLUE', 1))
            dc.SetBrush(wx.WHITE_BRUSH)
            dc.DrawRectangle(blk[0], blk[1], blk[2]-blk[0], blk[3]-blk[1])
        iDay=blk[-2]
        hour=blk[-1]
        self.__updateGrid__(iDay,hour,dc,printing)
        
    def __updateGrid__(self,iDay,hour,dc,printing):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        iStartHr=int(hour[0][0:2])-self.iOfsHr
        iEndHr=int(hour[1][0:2])-self.iOfsHr
        iEndMin=int(hour[1][2:4])
        if iEndMin>0:
            iEndHr+=1
        iStartHr=self.__limit__(iStartHr,0,self.iShowHr)
        iEndHr=self.__limit__(iEndHr,0,self.iShowHr)
        
        y=iDay*self.gridY
        
        xa=iStartHr*self.gridX+self.ofsX+self.iHrWidth
        xe=iEndHr*self.gridX+self.ofsX+self.iHrWidth
        self.__setGridColor__(dc,printing)
        #dc.DrawLine(xa,y,xe,y)
        #dc.DrawLine(xa,y,xe,y)
        #dc.DrawLine(xa,y,xa,y+self.gridY)
        #dc.DrawLine(xe,y,xe,y+self.gridY)
        for iHr in range(iStartHr,iEndHr):
            x=iHr*self.gridX+self.ofsX+self.iHrWidth
            dc.DrawLine(x,y,x,y+self.gridY)
        #dc.SetPen(wx.Pen('VIOLET', 1))
        dc.SetFont(wx.Font(7, wx.SWISS, wx.NORMAL, wx.NORMAL))
        self.__setGridTextColor__(dc,printing)
        #dc.SetTextForeground(wx.Colour(0x60, 0x60, 0x60))
        gXh=self.gridX/2
        gYh=self.gridY/2
        for iHr in range(iStartHr,iEndHr):
            x=iHr*self.gridX+self.ofsX+self.iHrWidth
            #dc.DrawLine(x,y,x,y+self.gridY)
            sHr='%02d'%(iHr+self.iOfsHr)
            te = dc.GetTextExtent(sHr)
            x=iHr*self.gridX + gXh - te[0]/2 + 2+ self.ofsX +self.iHrWidth
            y=iDay*self.gridY + gYh - te[1]/2 + 1
            #if (x>=xLim) and (y>=yLim):
            dc.DrawText(sHr, x, y)
        
    def __setGridColor__(self,dc,printing):
        if self.IsEnabled():
            #dc.SetPen(wx.Pen('VIOLET', 1))
            dc.SetPen(wx.Pen(wx.Colour(0x60, 0x60, 0x60),1))
        else:
            dc.SetPen(wx.Pen(wx.Colour(0xC0, 0xC0, 0xC0), 1))
    def __setGridTextColor__(self,dc,printing):
        if self.IsEnabled():
            dc.SetTextForeground(wx.Colour(0x60, 0x60, 0x60))
        else:
            dc.SetTextForeground(wx.Colour(0xC0, 0xC0, 0xC0))
    def __drawHourNav__(self,dc,flag,bt=3):
        if flag:
            pen=self.penNav[1]
            brush=self.brushNav[1]
        else:
            pen=self.penNav[0]
            brush=self.brushNav[0]
        dc.SetPen(pen)
        dc.SetBrush(brush)
        xM=self.iHrWidth>>1
        xD=self.iHrWidth>>2
        yM=self.maxHeight>>1
        if bt&1!=0:
            dc.DrawPolygon([(xM+xD,yM-xD),
                        (xM+xD,yM+xD),
                        (xM-xD,yM)])
        
        xOfs=self.iXNavHrInc
        if bt&2!=0:
            dc.DrawPolygon([(xOfs+xM-xD,yM-xD),
                        (xOfs+xM-xD,yM+xD),
                        (xOfs+xM+xD,yM)])
        
        ##dc.DrawLine(0,0,self.iHrWidth,self.maxHeight)
        #dc.DrawLine(self.iHrWidth,0,0,self.maxHeight)

        #xOfs=self.iShowHr*self.gridX+self.ofsX+self.sumX+self.iHrWidth
        #dc.DrawLine(xOfs+0,0,xOfs+self.iHrWidth,self.maxHeight)
        #dc.DrawLine(xOfs+self.iHrWidth,0,xOfs+0,self.maxHeight)

    def __drawGrid__(self,dc,printing):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        
        self.__setGridColor__(dc,printing)
        for i in range(0,self.days+1):
            y=i*self.gridY
            dc.DrawLine(self.iHrWidth,y,self.maxWidth-(self.iHrWidth),y)
        
        dc.DrawLine(self.iHrWidth,0,self.iHrWidth,self.maxHeight)
        y=0
        for i in range(0,self.iShowHr+1):
            x=i*self.gridX+self.ofsX+self.iHrWidth
            dc.DrawLine(x,y,x,self.maxHeight)
        if self.bSum:
            x+=self.sumX
            dc.DrawLine(x,y,x,self.maxHeight)
        self.__drawHourNav__(dc,False)
        #self.coorHr={}
        #for h in range(self.hours):
        #    for d in range(self.days):
        #        dc.DrawText(sHr, 
        #                h*self.gridX + gXh - te[0]/2 + 2+ self.ofsX, 
        #                d*self.gridY + gYh - te[1]/2 + 1)
        if 1==0:
            y=0
            gXh=self.gridX/2
            gYh=self.gridY/2
            
            self.ofsDay={}
            for w in days:
                y=y+self.gridY
                i=0
                for d in w:
                    if d>0:
                        sDay="%d"%d
                        self.coorDay[d]=(i*self.gridX,y)
                        self.ofsDay[d]=(0,0)
                    i=i+1
        pass
    def __drawGridText__(self,dc,printing):
        #xScroll,yScroll=self.scwDrawing.GetScrollPixelsPerUnit()
        #xView,yView=self.scwDrawing.GetViewStart()
        #xLim=xView*xScroll
        #yLim=yView*yScroll
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            #vtLog.vtLngCurWX(vtLog.DEBUG,'view:(%d,%d)'%(xView,yView))
            #vtLog.vtLngCurWX(vtLog.DEBUG,'lim:(%d,%d)'%(xLim,yLim),self)
        #dc.SetPen(wx.Pen('BLACK', 1))
        self.__setGridColor__(dc,printing)
        y=0
        gXh=self.gridX/2
        gYh=self.gridY/2
        dc.SetFont(wx.Font(7, wx.SWISS, wx.NORMAL, wx.NORMAL))
        self.__setGridTextColor__(dc,printing)
        if self.dayLabels is not None:
            i=0
            for sDay in self.dayLabels:
                te = dc.GetTextExtent(sDay)
                x=2
                y=i*self.gridY+gYh-te[1]/2+1
                #if (x>=xLim) and (y>=yLim):
                dc.DrawText(sDay, x, y)
                i+=1
        for h in range(self.iShowHr):
            sHr='%02d'%(self.iOfsHr+h)
            te = dc.GetTextExtent(sHr)
            for d in range(self.days):
                x=h*self.gridX + gXh - te[0]/2 + 2+ self.ofsX + self.iHrWidth
                y=d*self.gridY + gYh - te[1]/2 + 1
                #if (x>=xLim) and (y>=yLim):
                dc.DrawText(sHr, x, y)
        
    def __getCoor__(self,day):
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        try:
            return self.coorDay[day]
        except:
            return (-1,-1)
    def __getOfsDay__(self,day):
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        try:
            return self.ofsDay[day]
        except:
            return -1
    
    def Clear(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.lst=None
        self.lst=[]
        del self.selectableBlks
        self.selectableBlks=[]
        self.selectedBlk=None
        #print 'clear'
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if BUFFERED:
            #if self.buffer is None:
                # Initialize the buffer bitmap.  No real DC is needed at this point.
            #    self.buffer = wx.EmptyBitmap(self.maxWidth+2, self.maxHeight+2+self.sumY)
            #    dc = wx.BufferedDC(None, self.buffer)
            #    dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
            #    dc.Clear()
            #    self.DoDrawing(dc)
            #else:
                cdc = wx.ClientDC(self.scwDrawing)
                self.scwDrawing.DoPrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
                dc.BeginDrawing()
                self.__clear__(dc,False)
                dc.EndDrawing()
        self.scwDrawing.Refresh()
        
    def SetValue(self,lst):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #print 'setvale',lst
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.lst=lst
        if BUFFERED:
            #if self.buffer is None:
                # Initialize the buffer bitmap.  No real DC is needed at this point.
            #    self.buffer = wx.EmptyBitmap(self.maxWidth+2, self.maxHeight+2+self.sumY)
            #    dc = wx.BufferedDC(None, self.buffer)
            #    dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
            #    dc.Clear()
            #    self.DoDrawing(dc)
            #else:
                cdc = wx.ClientDC(self.scwDrawing)
                self.scwDrawing.DoPrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
                dc.BeginDrawing()
                self.__clear__(dc,False)
                #dc.EndDrawing()
            
                self.__drawValues__(dc,False)
                self.__drawGrid__(dc,False)
                self.__drawGridText__(dc,False)
                dc.EndDrawing()
            
        
        self.scwDrawing.Refresh()
        #self.OnPaint()
    def GetValue(self):
        return self.lst
    def __getHourRect__(self,iDay,hour,ofsHr=0):
        hr =string.atoi(hour[0][:2])
        min=string.atoi(hour[0][2:4])
        sHr=hr+min/60.0
        min=round(min/5.0)
        x=self.ofsX+self.iHrWidth
        y=self.gridY*iDay
        
        ofsHr+=self.iOfsHr
        #if hr>=ofsHr:
        xa=(hr-ofsHr)*self.gridHr+(min*self.gridHr/12.0)
        xa=self.__limit__(xa,0,self.iDrawWidth)
        
        #if xa<self.iXHrNavMin:
        #    xa=self.iXHrNavMin
        hr =string.atoi(hour[1][:2])
        min=string.atoi(hour[1][2:4])
        eHr=hr+min/60.0
        min=round(min/5.0)
        if sHr<eHr:
            sum=eHr-sHr
        else:
            sum=sHr-eHr
        #xe=coor[0]
        #if hr>=ofsHr:
        xe=(hr-ofsHr)*self.gridHr+(min*self.gridHr/12.0)
        if xe<0:
            xe=0
        xe=self.__limit__(xe,0,self.iDrawWidth)
        #print [x+xa, y, xe-xa, self.gridY]
        return [x+xa, y, xe-xa, self.gridY],sum
    def __drawValues__(self,dc,printing):
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if self.lst is None:
            return
        self.selectableBlks=[]
        self.selectedBlk=None
        sumTotal=0
        iDay=0
        ofsHr=0
        for day in self.lst:
            sum=0
            y=self.gridY*iDay
            for hour in day:
                rect,val=self.__getHourRect__(iDay,hour,ofsHr)
                sum+=val
                blk=[rect[0],rect[1],rect[0]+rect[2],rect[1]+rect[3],iDay,hour]
                self.__drawHour__(blk,False,dc,False)
                y=rect[1]
                self.selectableBlks.append(blk)
            iDay+=1
        self.__drawSum__(dc,printing)
        
    def __drawSum__(self,dc,printing):
        if self.lst is None:
            return
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,self.iHrWidth,self)
        if self.bSum==False:
            return
        brush=wx.Brush(self.scwDrawing.GetBackgroundColour())
        dc.SetPen(wx.Pen(self.scwDrawing.GetBackgroundColour(), 1))
        dc.SetBrush(brush)
        x=self.gridX*self.iShowHr+self.ofsX+1+self.iHrWidth
        for day in range(self.days+2):
            y=self.gridY*day+1
            dc.DrawRectangle(x, y, self.sumX-2, self.gridY-2)
        self.__setGridColor__(dc,printing)
        self.__setGridTextColor__(dc,printing)
            
        iDay=0
        ofsHr=0
        sumTotal=0
        for day in self.lst:
            sum=0
            y=self.gridY*iDay
            for hour in day:
                rect,val=self.__getHourRect__(iDay,hour,ofsHr)
                sum+=val
            dc.SetFont(wx.Font(7, wx.SWISS, wx.NORMAL, wx.NORMAL))
            #dc.SetTextForeground(wx.Colour(0x0, 0x0, 0x0))
            sTxt="""%4.2f  %02d"%02d'"""%(sum,math.floor(sum),round(sum%1*60))
            te = dc.GetTextExtent(sTxt)
            xS=self.gridX*self.iShowHr+self.ofsX+self.sumX+self.iHrWidth#+self.gridX/2
            #y=self.gridY
            dc.DrawText(sTxt,xS-2-te[0],y+self.gridY/2-te[1]/2+1)
            sumTotal=sumTotal+sum
            iDay+=1
        dc.SetFont(wx.Font(8, wx.SWISS, wx.NORMAL, wx.BOLD))
        #dc.SetTextForeground(wx.Colour(0x0, 0x0, 0x0))
        sum=sumTotal
        sTxt="""%5.2f'"""%(sum)
        te = dc.GetTextExtent(sTxt)
        xS=self.gridX*self.iShowHr+self.ofsX+self.sumX+self.iHrWidth#+self.gridX/2
        y=self.gridY*self.days#-self.gridY-self.gridY
        dc.DrawText(sTxt,xS-2-te[0],y+self.gridY/2-te[1]/2+1)
        sTxt="""%03d"%02d'"""%(math.floor(sum),round(sum%1*60))
        te = dc.GetTextExtent(sTxt)
        xS=self.gridX*self.iShowHr+self.ofsX+self.sumX+self.iHrWidth#+self.gridX/2
        y=self.gridY*self.days+self.gridY
        dc.DrawText(sTxt,xS-2-te[0],y+self.gridY/2-te[1]/2+1)
        
                    
    
    def OnSize(self, event):
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        s=event.GetSize()
        iW=s[0]
        i=iW-self.ofsX-(self.iHrWidth<<1)-self.sumX-5
        i/=self.gridX
        i-=1
        if i<0:
            i=1
        self.iShowHr=i
        iHrDelta=self.iMaxHr-self.iMinHr
        self.iShowHr=self.__limit__(self.iShowHr,1,iHrDelta)
        if self.iOfsHr+self.iShowHr>=self.iMaxHr+self.iMinHr:
            self.iOfsHr=self.iMaxHr-self.iShowHr
        self.iOfsHr=self.__limit__(self.iOfsHr,self.iMinHr,self.iMaxHr-iHrDelta)
        
        self.maxWidth=self.gridX*self.iShowHr+self.ofsX+self.iHrWidth*2
        self.__setDrawingSize__()
        self.__invokeRefresh__()
        #self.scwDrawing.SetSize(s)
        event.Skip()
    def Enable(self,flag):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        wx.Panel.Enable(self,flag)
        if BUFFERED:
            #if self.buffer is None:
                # Initialize the buffer bitmap.  No real DC is needed at this point.
            #    self.buffer = wx.EmptyBitmap(self.maxWidth+2, self.maxHeight+2+self.sumY)
            #    dc = wx.BufferedDC(None, self.buffer)
            #    dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
            #    dc.Clear()
            #    self.DoDrawing(dc)
            #else:
                cdc = wx.ClientDC(self.scwDrawing)
                self.scwDrawing.DoPrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
                dc.BeginDrawing()
                self.__clear__(dc,False)
                self.__drawValues__(dc,False)
                self.__drawGrid__(dc,False)
                self.__drawGridText__(dc,False)
                dc.EndDrawing()
            