#Boa:FramePanel:vtTimeSelectorHoursPanel
#----------------------------------------------------------------------------
# Name:         vTimeSelectorHoursPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vTimeSelectorHoursPanel.py,v 1.4 2006/07/17 10:54:24 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors
import calendar,time
import string,math,traceback

from vidarc.tool.time.vTimeSelectorHourDialog import *
import vidarc.tool.log.vtLog as vtLog

VERBOSE=0
BUFFERED=1

# defined event for vgpXmlTree item selected
wxEVT_VTM_SELECTOR_HOUR_SELECTED=wx.NewEventType()
vEVT_VTM_SELECTOR_HOUR_SELECTED=wx.PyEventBinder(wxEVT_VTM_SELECTOR_HOUR_SELECTED,1)
def EVT_VTM_SELECTOR_HOUR_SELECTED(win,func):
    win.Connect(-1,-1,wxVTM_SELECTOR_HOUR_SELECTED,func)
class vTmSelectorHourSelected(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTM_SELECTOR_HOUR_SELECTED(<widget_name>, self.OnInfoSelected)
    """

    def __init__(self,obj,day,hour):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTM_SELECTOR_HOUR_SELECTED)
        self.obj=obj
        self.day=day
        self.hour=hour
    def GetObject(self):
        return self.obj
    def GetDay(self):
        return self.day
    def GetHour(self):
        return self.hour

# defined event for vgpXmlTree item selected
wxEVT_VTM_SELECTOR_HOUR_CHANGED=wx.NewEventType()
vEVT_VTM_SELECTOR_HOUR_CHANGED=wx.PyEventBinder(wxEVT_VTM_SELECTOR_HOUR_CHANGED,1)
def EVT_VTM_SELECTOR_HOUR_CHANGED(win,func):
    win.Connect(-1,-1,wxVTM_SELECTOR_HOUR_CHANGED,func)
class vTmSelectorHourChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTM_SELECTOR_HOUR_CHANGED(<widget_name>, self.OnInfoChanged)
    """

    def __init__(self,obj,day):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTM_SELECTOR_HOUR_CHANGED)
        self.obj=obj
        self.day=day
    def GetObject(self):
        return self.obj
    def GetDay(self):
        return self.day

class vTimeSelectorHoursPanel(wx.Panel):
    def _init_ctrls(self, prnt, id, pos, size, style,name=u'vgTmSelectorDay'):
        # generated method, don't edit
        wx.Panel.__init__(self, id=id,
              name=name, parent=prnt, pos=pos,size=size, style=style)
        #self.SetClientSize(wx.Size(492, 273))
        #self.SetAutoLayout(True)
        self.Bind(wx.EVT_SIZE, self.OnSize)

        self.scwDrawing = wx.ScrolledWindow(id=wx.NewId(),
              name=u'scwDay', parent=self, pos=wx.Point(0, 0),
              size=size, style=wx.HSCROLL | wx.VSCROLL)
        self.scwDrawing.SetAutoLayout(False)

    def __init__(self, parent=None, days=7, hours=24, sum=True, 
                dayLabels=None, 
                id=wx.NewId(), pos=wx.DefaultPosition, size=wx.Size(100, 50), 
                style=wx.TAB_TRAVERSAL, name=u'vgTmSelectorDay',gridDay=15,gridHr=15):
        self._init_ctrls(parent,id,pos,size,style,name)
        self.doc=None
        self.dlg=None
        self.buffer=None
        self.verbose=VERBOSE
        
        self.__setDftColors__()
        self.lst=None
        self.drawing=False
        self.selectableBlks=[]
        self.selectedBlk=None
        
        self.days=days
        self.dayLabels=dayLabels
        self.bSum=sum
        self.hours=hours
        self.gridHr=gridHr
        self.maxHrs=1
        if self.dayLabels is not None:
            #dc = wx.PaintDC(self)
            #dc.SetFont(wx.Font(7, wx.SWISS, wx.NORMAL, wx.NORMAL))
            #dc.SetTextForeground(wx.Colour(0x80, 0x80, 0x80))
            #self.ofsX=0
            #for sDay in self.dayLabels:
            #    te = dc.GetTextExtent(sDay)
            #    if te[0]>self.ofsX:
            #        self.ofsX=te[0]
            #self.ofsX+=4
            self.ofsX=25
            if len(dayLabels)==0:
                for i in range(days):
                    self.dayLabels.append(calendar.day_abbr[i])
        else:
            self.ofsX=0
        self.gridX=self.gridHr*self.maxHrs
        #def grid y
        
        self.gridY=gridDay
        #self.countWidth=25
        #self.countHeight=1
        
        self.sumX=60
        self.sumY=0
        self.legendX=80
        self.maxWidth=self.gridX*self.hours+self.ofsX
        self.maxHeight=self.gridY*self.days
        if sum:
            self.maxWidth+=self.sumX
            self.sumY=self.gridY<<1
        self.scwDrawing.SetVirtualSize((self.maxWidth+5, self.maxHeight+5+self.sumY))
        self.scwDrawing.SetScrollRate(20,20)
        if BUFFERED:
            # Initialize the buffer bitmap.  No real DC is needed at this point.
            self.buffer = wx.EmptyBitmap(self.maxWidth+1, self.maxHeight+1+self.sumY)
            dc = wx.BufferedDC(None, self.buffer)
            dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
            dc.Clear()
            self.DoDrawing(dc)
        self.scwDrawing.Bind(wx.EVT_LEFT_DOWN, self.OnLeftButtonEvent)
        self.scwDrawing.Bind(wx.EVT_LEFT_UP,   self.OnLeftButtonEvent)
        self.scwDrawing.Bind(wx.EVT_MOTION,    self.OnLeftButtonEvent)
        self.scwDrawing.Bind(wx.EVT_RIGHT_DOWN,self.OnRightButtonEvent)
        
        self.scwDrawing.Bind(wx.EVT_PAINT, self.OnPaint)
        self.scwDrawing.Bind(wx.EVT_LEAVE_WINDOW, self.OnKillFocus)

    def GetDlg(self):
        if self.dlg is None:
            self.dlg=vTimeSelectorHourDialog(self)
        return self.dlg
    def __setDftColors__(self):
        self.color=[]
        self.color.append(wx.Colour(255, 154, 000))
        self.color.append(wx.Colour(255, 207, 000))
        self.color.append(wx.Colour(102, 205, 170))
        self.color.append(wx.Colour(176, 196, 222))
        self.color.append(wx.Colour(144, 238, 144))
        self.color.append(wx.Colour(218, 165, 32))
        self.color.append(wx.Colour(226, 126, 226))
        
        self.colorDis=[]
        self.colorDis.append(wx.Colour(167, 100, 000))
        self.colorDis.append(wx.Colour(167, 136, 000))
        self.colorDis.append(wx.Colour( 67, 135, 112))
        self.colorDis.append(wx.Colour(116, 130, 147))
        self.colorDis.append(wx.Colour(100, 166, 100))
        self.colorDis.append(wx.Colour(150, 114,  22))
        self.colorDis.append(wx.Colour(147,  82, 147))
        self.__setDftBrush__()
        self.__setDftPen__()
        
    def __setDftBrush__(self):
        self.brush=[]
        for c in self.color:
            self.brush.append(wx.Brush(c))
        self.brushDis=[]
        for c in self.colorDis:
            self.brushDis.append(wx.Brush(c))
    def __setDftPen__(self):
        self.pen=[]
        for c in self.color:
            self.pen.append(wx.Pen(c,1))
        self.penDis=[]
        for c in self.colorDis:
            self.penDis.append(wx.Pen(c,1))
    def SetXY(self, event):
        self.x, self.y = self.ConvertEventCoords(event)
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        
    def ConvertEventCoords(self, event):
        xView, yView = self.scwDrawing.GetViewStart()
        xDelta, yDelta = self.scwDrawing.GetScrollPixelsPerUnit()
        return (event.GetX() + (xView * xDelta),
                event.GetY() + (yView * yDelta))
    def SelectValue(self,day,hr):
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if BUFFERED:
            # If doing buffered drawing, create the buffered DC, giving it
            # it a real DC to blit to when done.
            cdc = wx.ClientDC(self.scwDrawing)
            self.scwDrawing.DoPrepareDC(cdc)
            dc = wx.BufferedDC(cdc, self.buffer)
            dc.BeginDrawing()
        else:
            dc = wx.ClientDC(self)
            self.PrepareDC(dc)
        
        if self.selectedBlk is not None:
            # paint old
            self.__drawHour__(self.selectedBlk,False,dc,False)
            #oBlk=self.selectedBlk
            #color=self.color[oBlk[-2]%7]
            #brush=self.brush[oBlk[-2]%7]
            #dc.SetBrush(brush)
            #dc.DrawRectangle(oBlk[0], oBlk[1], 
            #                    oBlk[2]-oBlk[0], oBlk[3]-oBlk[1])
        self.selectedBlk=None
        blk=self.__getSelectableBlk__(day,hr)
        if blk is not None:
            self.__drawHour__(blk,True,dc,False)
            #dc.SetPen(wx.Pen('BLUE', 1))
            #dc.SetBrush(wx.LIGHT_GREY_BRUSH)
            #dc.DrawRectangle(blk[0], blk[1], 
            #                    blk[2]-blk[0], blk[3]-blk[1])
            self.selectedBlk=blk
            wx.PostEvent(self,vTmSelectorHourSelected(self,day,hr))
        #self.selectedBlk=node
        self.__drawGrid__(dc,False)
        self.__drawGridText__(dc,False)
        dc.EndDrawing()
    def __getSelectableBlk__(self,x,y):
        for blks in self.selectableBlks:
            if (x>blks[0]) and (x<blks[2]):
                if (y>blks[1]) and (y<blks[3]):
                    return blks
        return None
    def __getSelectableIdx__(self,day,hour):
        idx=0
        for tup in self.selectableBlks:
            if tup[-2]==day:
                if tup[-1]==hour:
                    return idx
            idx+=1
        return -1
    def __setHour__(self,iDay,hour):
        try:
            day=self.lst[iDay]
            idx=0
            idxSel=-1
            for hr in day:
                if (hour[0]>=hr[0]) and (hour[0]<=hr[1]):
                    # start inside
                    if hour[1]<=hr[1]:
                        hour=None
                        break
                    else:
                        idxSel=self.__getSelectableIdx__(iDay,hr)
                        if self.verbose:
                            vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%repr(idxSel),self)
                        hr[1]=hour[1]
                        hour=hr
                        #break
                if (hour[1]>=hr[0]) and (hour[1]<=hr[1]):
                    #end inside
                    if hour[0]<hr[0]:
                        hr[0]=hour[0]
                        idxSel=self.__getSelectableIdx__(iDay,hr)
                        hr[0]=hour[0]
                        hour=hr
                        #break
            if hour is not None:
                rect,sum=self.__getHourRect__(iDay,hour)
                if idxSel>=0:
                    r=self.selectableBlks[idxSel]
                    if self.verbose:
                        vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%repr(r),self)
                    r[0]=rect[0]
                    r[1]=rect[1]
                    r[2]=rect[0]+rect[2]
                    r[3]=rect[1]+rect[3]
                    if self.verbose:
                        vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%repr(r),self)
                    for i in range(len(day)-1):
                        hr1=day[i]
                        hr2=day[i+1]
                        if hr1[0]==hr2[0]:
                            if hr1[1]<hr2[1]:
                                idxSel=self.__getSelectableIdx__(iDay,hr1)
                                if self.verbose:
                                    vtLog.vtLngCurWX(vtLog.DEBUG,'hr1:%s hr2:%s idx:%d'%(hr1,hr2,idxSel),self)
                                self.selectableBlks=self.selectableBlks[:idxSel]+self.selectableBlks[idxSel+1:]
                                day.remove(hr1)
                                break
                    return r
                else:
                    day.append(hour)
                    def compFunc(a,b):
                        return cmp(a[0],b[0])
                    day.sort(compFunc)
                    self.lst[iDay]=day
                    r=[rect[0], rect[1], rect[0] + rect[2], rect[1] + rect[3] , iDay,hour]
                    self.selectableBlks.append(r)
                    if self.verbose:
                        vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%repr(r),self)
                    return r
        except Exception,list:
            vtLog.vtLngTB(self.GetName())
            return None
    def OnLeftButtonEvent(self, event):
        event.Skip()
        if event.LeftDown():
            self.SetFocus()
            self.SetXY(event)
            iDay=self.y/self.gridY
            if iDay>=self.days:
                iDay=-1
            iHour=(self.x-self.ofsX)/self.gridX
            if iHour>=self.hours:
                iHour=-1
            if self.verbose:
                vtLog.vtLngCurWX(vtLog.DEBUG,'left down',self)
                vtLog.vtLngCurWX(vtLog.DEBUG,'x:%d y:%d'%(self.x,self.y),self)
                vtLog.vtLngCurWX(vtLog.DEBUG,'shift:%d ctrl:%d'%(event.ShiftDown(),event.ControlDown()),self)
                vtLog.vtLngCurWX(vtLog.DEBUG,'day:%d hour:%d'%(iDay,iHour),self)
                vtLog.vtLngCurWX(vtLog.DEBUG,vtLog.pformat(self.selectableBlks),self)
                vtLog.vtLngCurWX(vtLog.DEBUG,vtLog.pformat(self.lst),self)
            if iDay==-1 or iHour==-1:
                if self.selectedBlk is not None:
                    cdc = wx.ClientDC(self.scwDrawing)
                    self.scwDrawing.DoPrepareDC(cdc)
                    dc = wx.BufferedDC(cdc, self.buffer)
                    dc.BeginDrawing()
                    self.__drawHour__(self.selectedBlk,False,dc,False)
                    dc.EndDrawing()
                    self.selectedBlk=None
                return
            self.drawing = True
            bUpdateSum=False
            if self.verbose:
                vtLog.vtLngCurWX(vtLog.DEBUG,'at begin lst:%s'%repr(self.lst[iDay]),self)
            blk=self.__getSelectableBlk__(self.x,self.y)
            if blk is None:
                hour=['%02d%02d'%(iHour,0),'%02d%02d'%(iHour+1,0)]
                blk=self.__setHour__(iDay,hour)
                if self.verbose:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%repr(blk),self)
                bUpdateSum=True
                wx.PostEvent(self,vTmSelectorHourChanged(self,iDay))
                if self.selectedBlk==blk:
                    self.selectedBlk=None
            if blk is not None:
                if BUFFERED:
                    # If doing buffered drawing, create the buffered DC, giving it
                    # it a real DC to blit to when done.
                    cdc = wx.ClientDC(self.scwDrawing)
                    #self.PrepareDC(cdc)
                    self.scwDrawing.DoPrepareDC(cdc)
                    dc = wx.BufferedDC(cdc, self.buffer)
                    dc.BeginDrawing()
                    #print 'check',blk
                    if self.selectedBlk is not None:
                        if self.selectedBlk!=blk:
                            # paint old
                            self.__drawHour__(self.selectedBlk,False,dc,False)
                            #oBlk=self.selectedBlk
                            #color=self.color[oBlk[-2]%7]
                            #brush=self.brush[oBlk[-2]%7]
                            #dc.SetBrush(brush)
                            #dc.DrawRectangle(oBlk[0], oBlk[1], 
                            #    oBlk[2]-oBlk[0], oBlk[3]-oBlk[1])
                        else:
                            bUpdateSum=True
                            day=self.lst[iDay]
                            sHrS='%02d00'%iHour
                            sHrE='%02d00'%(iHour+1)
                            if self.verbose:
                                vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%repr(blk),self)
                            tup=blk[-1]
                            h=[]
                            if self.verbose:
                                vtLog.vtLngCurWX(vtLog.DEBUG,'    blk:%s'%repr(blk),self)
                                print tup,sHrS,sHrE
                            if tup[0]==sHrS:
                                if tup[1]>sHrE:
                                    h.append([sHrE,tup[1]])
                            elif tup[1]==sHrE:
                                if tup[0]<sHrS:
                                    h.append([tup[0],sHrS])
                            else:
                                h.append([tup[0],sHrS])
                                h.append([sHrE,tup[1]])
                            #print h
                            try:
                                day.remove(tup)
                                self.selectableBlks.remove(blk)
                            except Exception,list:
                                print 'exception:%s'%list
                                print '   tup %s',repr(tup)
                                print '   blk %s',repr(blk)
                            #print iDay,blk
                            for tup in h:
                                blk=self.__setHour__(iDay,tup)
                                self.__drawHour__(blk,False,dc,False)
                                #print blk
                                #color=self.color[blk[-2]%7]
                                #brush=self.brush[blk[-2]%7]
                                #dc.SetBrush(brush)
                                #dc.DrawRectangle(blk[0], blk[1], 
                                #    blk[2]-blk[0], blk[3]-blk[1])
                            #print iDay,blk
                            self.__clearHour__(iDay,[sHrS,sHrE],dc,False)
                            #r,s=self.__getHourRect__(iDay,[sHrS,sHrE])
                            #color=self.color[oBlk[-2]%7]
                            #brush=self.brush[oBlk[-2]%7]
                            #brush=wx.Brush(self.scwDrawing.GetBackgroundColour())
                            #dc.SetBrush(brush)
                            #dc.DrawRectangle(r[0], r[1], r[2]+1, r[3]+1)
                            #if self.verbose:
                            #    vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%repr(r),callstack=False)
                            blk=None
                            wx.PostEvent(self,vTmSelectorHourChanged(self,iDay))
                    if blk is not None:
                        #print 'selec',blk
                        #idxSel=self.__getSelectableIdx__(iDay,hr)
                        #blk=self.selectableBlks[idxSel]
                        self.__drawHour__(blk,True,dc,False)
                        #dc.SetPen(wx.Pen('BLUE', 1))
                        #dc.SetBrush(wx.WHITE_BRUSH)
                        #dc.DrawRectangle(blk[0], blk[1], 
                        #            blk[2]-blk[0], blk[3]-blk[1])
                    if self.verbose:
                        vtLog.vtLngCurWX(vtLog.DEBUG,'    blk:%s'%repr(blk),self)
                    #self.__drawGrid__(dc,False)
                    #self.__drawGridText__(dc,False)
                    if self.verbose:
                        vtLog.vtLngCurWX(vtLog.DEBUG,'    blk:%s'%repr(blk),self)
                    if bUpdateSum:
                        self.__drawSum__(dc,False)
                    dc.EndDrawing()
                    self.selectedBlk=blk
                    if self.selectedBlk is not None:
                        wx.PostEvent(self,vTmSelectorHourSelected(self,iDay,self.selectedBlk[-1]))
                    else:
                        wx.PostEvent(self,vTmSelectorHourSelected(self,iDay,None))
            if self.verbose:
                vtLog.vtLngCurWX(vtLog.DEBUG,'at end   lst:%s'%repr(self.lst[iDay]),self)
                vtLog.vtLngCurWX(vtLog.DEBUG,'    blk:%s'%repr(blk),self)
            pass
        elif event.Dragging() and self.drawing:
            #print 'dragging'
            if BUFFERED:
                # If doing buffered drawing, create the buffered DC, giving it
                # it a real DC to blit to when done.
                cdc = wx.ClientDC(self)
                self.PrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
            else:
                dc = wx.ClientDC(self)
                self.PrepareDC(dc)

            #dc.BeginDrawing()
            #dc.SetPen(wx.Pen('MEDIUM FOREST GREEN', 4))
            #coords = (self.x, self.y) + self.ConvertEventCoords(event)
            #self.curLine.append(coords)
            #dc.DrawLine(*coords)
            #self.SetXY(event)
            #dc.EndDrawing()


        elif event.LeftUp() and self.drawing:
            #self.lines.append(self.curLine)
            #self.curLine = []
            #self.ReleaseMouse()
            self.drawing = False
            self.scwDrawing.Refresh()
            #xView, yView = self.scwDrawing.GetViewStart()
            #xDelta, yDelta = self.scwDrawing.GetScrollPixelsPerUnit()
            #print xView,yView,xDelta,yDelta
            #s=self.scwDrawing.GetSize()
            #self.scwDrawing.RefreshRect(wx.Rect(xView*xDelta,yView*yDelta,
            #        s[0],s[1]))
            pass
        else:
            try:
                self.SetXY(event)
                cdc = wx.ClientDC(self.scwDrawing)
                self.scwDrawing.DoPrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
                dc.BeginDrawing()
                
                iDay=self.y/self.gridY
                if iDay>=self.days:
                    iDay=-1
                iHour=((self.x-self.ofsX)/self.gridX)
                if iHour>=self.hours:
                    iHour=-1
                if iDay==-1 or iHour==-1:
                    if self.selectedBlk is not None:
                        self.__drawHour__(self.selectedBlk,False,dc,False)
                        self.selectedBlk=None
                    dc.EndDrawing()
                    return
                self.drawing = True
                bUpdateSum=False
                blk=self.__getSelectableBlk__(self.x,self.y)
                if blk is None:
                    if self.selectedBlk is not None:
                        self.__drawHour__(self.selectedBlk,False,dc,False)
                    self.selectedBlk=None
                else:
                    if self.selectedBlk is not None:
                        if self.selectedBlk!=blk:
                            # paint old
                            self.__drawHour__(self.selectedBlk,False,dc,False)
                    else:
                        self.selectedBlk=blk
                        self.__drawHour__(self.selectedBlk,True,dc,False)
                dc.EndDrawing()
            except:
                pass
            #self.x
            #iDay=self.y/self.gridY
    def OnKillFocus(self,evt):
        try:
            cdc = wx.ClientDC(self.scwDrawing)
            self.scwDrawing.DoPrepareDC(cdc)
            dc = wx.BufferedDC(cdc, self.buffer)
            dc.BeginDrawing()
            self.__drawHourNav__(dc,False,3)
            if self.selectedBlk is not None:
                self.__drawHour__(self.selectedBlk,False,dc,False)
            self.selectedBlk=None
            dc.EndDrawing()
        except:
            pass
        evt.Skip()
    def __invokeRefresh__(self):
            cdc = wx.ClientDC(self.scwDrawing)
            self.scwDrawing.DoPrepareDC(cdc)
            dc = wx.BufferedDC(cdc, self.buffer)
            #self.OnPaint(None)
            dc.BeginDrawing()
            self.__clear__(dc,False)
            self.__drawValues__(dc,printing=False)
            #self.__drawGrid__(dc,printing=printing)
            #self.__drawGridText__(dc,printing=printing)
            #self.__drawLegend__(dc,printing=printing)
            dc.EndDrawing()
            #self.scwDrawing.Refresh()
    def OnRightButtonEvent(self, event):
        if event.RightDown():
            self.SetFocus()
            self.SetXY(event)
            iDay=self.y/self.gridY
            if iDay>self.days:
                iDay=-1
            iHour=(self.x-self.ofsX)/self.gridX
            if iHour>self.hours:
                iHour=-1
            if self.verbose:
                vtLog.vtLngCurWX(vtLog.DEBUG,'right down',self)
                vtLog.vtLngCurWX(vtLog.DEBUG,'x:%d y:%d'%(self.x,self.y),self)
                vtLog.vtLngCurWX(vtLog.DEBUG,'shift:%d ctrl:%d'%(event.ShiftDown(),event.ControlDown()),self)
                vtLog.vtLngCurWX(vtLog.DEBUG,'day:%d hour:%d'%(iDay,iHour),self)
            #blk=self.__getSelectableBlk__(self.x,self.y)
            #if blk is not None:
            #    cdc = wx.ClientDC(self.scwDrawing)
            #    self.scwDrawing.DoPrepareDC(cdc)
            #    dc = wx.BufferedDC(cdc, self.buffer)
            #    dc.BeginDrawing()
            #    if self.selectedBlk is not None:
            #        self.__drawHour__(self.selectedBlk,False,dc,False)
            #    self.selectedBlk=blk
            #    self.__drawHour__(blk,True,dc,False)
            #    wx.CallAfter(self.__openDlg__)
            #    dc.EndDrawing()
            #    pass
            self.__openDlg__()
        event.Skip()
    def __openDlg__(self):
        iDay=self.y/self.gridY
        if iDay>self.days:
            iDay=-1
        iHour=(self.x-self.ofsX)/self.gridX
        if iHour>self.hours:
            iHour=-1
            
        dlg=self.GetDlg()
        dlg.Centre()
        blk=self.selectedBlk
        dlg.SetValue(blk[-1][0],blk[-1][1])
        if dlg.ShowModal()>0:
            try:
                cdc = wx.ClientDC(self.scwDrawing)
                self.scwDrawing.DoPrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
                dc.BeginDrawing()
                zStart,zEnd=dlg.GetValue()
                hour=[zStart,zEnd]
                #print hour
                day=self.lst[iDay]
                #print 'before',day
                if blk is not None:
                    for hr in day:
                        idxSel=self.__getSelectableIdx__(iDay,hr)
                        blk=self.selectableBlks[idxSel]
                        self.__clearHour__(iDay,blk[-1],dc,False)
                    idxSel=self.__getSelectableIdx__(iDay,blk[-1])
                    blk=self.selectableBlks[idxSel]
                    #print blk
                    #print 'before',self.lst[iDay]
                    self.lst[iDay].remove(blk[-1])
                    #print 'after',self.lst[iDay]
                    self.selectableBlks=self.selectableBlks[:idxSel]+self.selectableBlks[idxSel+1:]
                blkEdit=self.__setHour__(iDay,hour)
                day=self.lst[iDay]
                #print 'after',day
                dc.SetPen(wx.Pen('BLACK', 1))
                for hr in day:
                    idxSel=self.__getSelectableIdx__(iDay,hr)
                    blk=self.selectableBlks[idxSel]
                    #print blk
                    self.__drawHour__(blk,False,dc,False)
                    #color=self.color[blk[-2]%7]
                    #brush=self.brush[blk[-2]%7]
                    #dc.SetBrush(brush)
                    #dc.DrawRectangle(blk[0], blk[1], 
                    #        blk[2]-blk[0], blk[3]-blk[1])
                    #self.__updateGrid__(blk[-1],dc,False)
                if blkEdit is not None:
                    self.__drawHour__(blkEdit,False,dc,False)
                    #self.__drawHour__(blkEdit,True,dc,False)
                    #dc.SetPen(wx.Pen('BLUE', 1))
                    #dc.SetBrush(wx.WHITE_BRUSH)
                    #dc.DrawRectangle(blkEdit[0], blkEdit[1], 
                    #            blkEdit[2]-blkEdit[0], blkEdit[3]-blkEdit[1])
                    #self.__updateGrid__(blkEdit[-1],dc,False)
                #self.__drawGrid__(dc,False)
                #self.__drawGridText__(dc,False)
                self.__drawSum__(dc,False)
                self.selectedBlk=None
                wx.PostEvent(self,vTmSelectorHourChanged(self,iDay))
            except:
                vtLog.vtLngTB(self.GetName())
            dc.EndDrawing()
                
    def OnPaint(self, event):
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,'buffer:%d'%BUFFERED,self)
        if BUFFERED:
            # Create a buffered paint DC.  It will create the real
            # wx.PaintDC and then blit the bitmap to it when dc is
            # deleted.  Since we don't need to draw anything else
            # here that's all there is to it.
            dc = wx.BufferedPaintDC(self.scwDrawing, self.buffer, wx.BUFFER_VIRTUAL_AREA)
        else:
            dc = wx.PaintDC(self.scwDrawing)
            self.PrepareDC(dc)
            # since we're not buffering in this case, we have to
            # paint the whole window, potentially very time consuming.
            self.DoDrawing(dc)
        #if self.verbose:
        #    print
        #    print
        

    def DoDrawing(self, dc, printing=False):
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        dc.BeginDrawing()
        self.__drawValues__(dc,printing=printing)
        self.__drawGrid__(dc,printing=printing)
        self.__drawGridText__(dc,printing=printing)
        #self.__drawLegend__(dc,printing=printing)
        dc.EndDrawing()

    
    def __clear__(self,dc,printing):
        brush=wx.Brush(self.scwDrawing.GetBackgroundColour())
        dc.SetBrush(brush)
        dc.SetPen(wx.Pen(self.scwDrawing.GetBackgroundColour(), 1))
        dc.DrawRectangle(0, 0, self.maxWidth+1, self.maxHeight+1+self.sumY)
        self.__drawGrid__(dc,printing=printing)
        self.__drawGridText__(dc,printing=printing)
        
    def __clearHour__(self,iDay,hour,dc,printing):
        r,s=self.__getHourRect__(iDay,hour)
        #color=self.color[oBlk[-2]%7]
        #brush=self.brush[oBlk[-2]%7]
        brush=wx.Brush(self.scwDrawing.GetBackgroundColour())
        dc.SetBrush(brush)
        dc.DrawRectangle(r[0], r[1], r[2]+1, r[3]+1)
        self.__updateGrid__(iDay,hour,dc,printing)
        
    def __drawHour__(self,blk,selected,dc,printing):
        if self.IsEnabled()==False:
            selected=False
        if selected==False:
            if self.IsEnabled():
                color=self.color[blk[-2]%7]
                brush=self.brush[blk[-2]%7]
                dc.SetBrush(brush)
            else:
                color=self.colorDis[blk[-2]%7]
                brush=self.brushDis[blk[-2]%7]
                dc.SetBrush(brush)
            dc.DrawRectangle(blk[0], blk[1], blk[2]-blk[0], blk[3]-blk[1])
        else:
            dc.SetPen(wx.Pen('BLUE', 1))
            dc.SetBrush(wx.WHITE_BRUSH)
            dc.DrawRectangle(blk[0], blk[1], blk[2]-blk[0], blk[3]-blk[1])
        iDay=blk[-2]
        hour=blk[-1]
        self.__updateGrid__(iDay,hour,dc,printing)
        
    def __updateGrid__(self,iDay,hour,dc,printing):
        iStartHr=int(hour[0][0:2])
        iEndHr=int(hour[1][0:2])
        iEndMin=int(hour[1][2:4])
        if iEndMin>0:
            iEndHr+=1
        y=iDay*self.gridY
        xa=iStartHr*self.gridX+self.ofsX
        xe=iEndHr*self.gridX+self.ofsX
        self.__setGridColor__(dc,printing)
        #dc.SetPen(wx.Pen('VIOLET', 1))
        dc.SetFont(wx.Font(7, wx.SWISS, wx.NORMAL, wx.NORMAL))
        self.__setGridTextColor__(dc,printing)
        #dc.SetTextForeground(wx.Colour(0x60, 0x60, 0x60))
        dc.DrawLine(xa,y,xe,y)
        dc.DrawLine(xa,y,xe,y)
        gXh=self.gridX/2
        gYh=self.gridY/2
        for iHr in range(iStartHr,iEndHr):
            x=iHr*self.gridX+self.ofsX
            dc.DrawLine(x,y,x,y+self.gridY)
            sHr='%02d'%iHr
            te = dc.GetTextExtent(sHr)
            x=iHr*self.gridX + gXh - te[0]/2 + 2+ self.ofsX
            y=iDay*self.gridY + gYh - te[1]/2 + 1
            #if (x>=xLim) and (y>=yLim):
            dc.DrawText(sHr, x, y)
        
    def __setGridColor__(self,dc,printing):
        if self.IsEnabled():
            dc.SetPen(wx.Pen('VIOLET', 1))
        else:
            dc.SetPen(wx.Pen(wx.Colour(0xC0, 0xC0, 0xC0), 1))
    def __setGridTextColor__(self,dc,printing):
        if self.IsEnabled():
            dc.SetTextForeground(wx.Colour(0x60, 0x60, 0x60))
        else:
            dc.SetTextForeground(wx.Colour(0xC0, 0xC0, 0xC0))
    def __drawGrid__(self,dc,printing):
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        
        self.__setGridColor__(dc,printing)
        for i in range(0,self.days+1):
            y=i*self.gridY
            dc.DrawLine(0,y,self.maxWidth,y)
        dc.DrawLine(0,0,0,self.maxHeight)
        y=0
        for i in range(0,self.hours+1):
            x=i*self.gridX+self.ofsX
            dc.DrawLine(x,y,x,self.maxHeight)
        if self.bSum:
            x+=self.sumX
            dc.DrawLine(x,y,x,self.maxHeight)
        #self.coorHr={}
        #for h in range(self.hours):
        #    for d in range(self.days):
        #        dc.DrawText(sHr, 
        #                h*self.gridX + gXh - te[0]/2 + 2+ self.ofsX, 
        #                d*self.gridY + gYh - te[1]/2 + 1)
        if 1==0:
            y=0
            gXh=self.gridX/2
            gYh=self.gridY/2
            
            self.ofsDay={}
            for w in days:
                y=y+self.gridY
                i=0
                for d in w:
                    if d>0:
                        sDay="%d"%d
                        self.coorDay[d]=(i*self.gridX,y)
                        self.ofsDay[d]=(0,0)
                    i=i+1
        pass
    def __drawGridText__(self,dc,printing):
        #xScroll,yScroll=self.scwDrawing.GetScrollPixelsPerUnit()
        #xView,yView=self.scwDrawing.GetViewStart()
        #xLim=xView*xScroll
        #yLim=yView*yScroll
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            #vtLog.vtLngCurWX(vtLog.DEBUG,'view:(%d,%d)'%(xView,yView))
            #vtLog.vtLngCurWX(vtLog.DEBUG,'lim:(%d,%d)'%(xLim,yLim),callstack=False)
        #dc.SetPen(wx.Pen('BLACK', 1))
        self.__setGridColor__(dc,printing)
        y=0
        gXh=self.gridX/2
        gYh=self.gridY/2
        dc.SetFont(wx.Font(7, wx.SWISS, wx.NORMAL, wx.NORMAL))
        self.__setGridTextColor__(dc,printing)
        if self.dayLabels is not None:
            i=0
            for sDay in self.dayLabels:
                te = dc.GetTextExtent(sDay)
                x=2
                y=i*self.gridY+gYh-te[1]/2+1
                #if (x>=xLim) and (y>=yLim):
                dc.DrawText(sDay, x, y)
                i+=1
        for h in range(self.hours):
            sHr='%02d'%h
            te = dc.GetTextExtent(sHr)
            for d in range(self.days):
                x=h*self.gridX + gXh - te[0]/2 + 2+ self.ofsX
                y=d*self.gridY + gYh - te[1]/2 + 1
                #if (x>=xLim) and (y>=yLim):
                dc.DrawText(sHr, x, y)
        
    def __getCoor__(self,day):
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        try:
            return self.coorDay[day]
        except:
            return (-1,-1)
    def __getOfsDay__(self,day):
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        try:
            return self.ofsDay[day]
        except:
            return -1
    
    def Clear(self):
        self.lst=None
        del self.selectableBlks
        self.selectableBlks=[]
        self.selectedBlk=None
        #print 'clear'
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if BUFFERED:
            #if self.buffer is None:
                # Initialize the buffer bitmap.  No real DC is needed at this point.
            #    self.buffer = wx.EmptyBitmap(self.maxWidth+2, self.maxHeight+2+self.sumY)
            #    dc = wx.BufferedDC(None, self.buffer)
            #    dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
            #    dc.Clear()
            #    self.DoDrawing(dc)
            #else:
                cdc = wx.ClientDC(self.scwDrawing)
                self.scwDrawing.DoPrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
                dc.BeginDrawing()
                self.__clear__(dc,False)
                dc.EndDrawing()
        self.scwDrawing.Refresh()
        
    def SetValue(self,lst):
        #print 'setvale',lst
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.lst=lst
        if BUFFERED:
            #if self.buffer is None:
                # Initialize the buffer bitmap.  No real DC is needed at this point.
            #    self.buffer = wx.EmptyBitmap(self.maxWidth+2, self.maxHeight+2+self.sumY)
            #    dc = wx.BufferedDC(None, self.buffer)
            #    dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
            #    dc.Clear()
            #    self.DoDrawing(dc)
            #else:
                cdc = wx.ClientDC(self.scwDrawing)
                self.scwDrawing.DoPrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
                dc.BeginDrawing()
                self.__clear__(dc,False)
                #dc.EndDrawing()
            
                self.__drawValues__(dc,False)
                self.__drawGrid__(dc,False)
                self.__drawGridText__(dc,False)
                dc.EndDrawing()
            
        
        self.scwDrawing.Refresh()
        #self.OnPaint()
    def GetValue(self):
        return self.lst
    def __getHourRect__(self,iDay,hour,ofsHr=0):
        hr =string.atoi(hour[0][:2])
        min=string.atoi(hour[0][2:4])
        sHr=hr+min/60.0
        min=round(min/5.0)
        x=self.ofsX
        y=self.gridY*iDay
                
        if hr>=ofsHr:
            xa=(hr-ofsHr)*self.gridHr+(min*self.gridHr/12.0)
        if xa<0:
            xa=0
        hr =string.atoi(hour[1][:2])
        min=string.atoi(hour[1][2:4])
        eHr=hr+min/60.0
        min=round(min/5.0)
        if sHr<eHr:
            sum=eHr-sHr
        else:
            sum=sHr-eHr
        #xe=coor[0]
        if hr>=ofsHr:
            xe=(hr-ofsHr)*self.gridHr+(min*self.gridHr/12.0)
        return [x+xa, y, xe-xa, self.gridY],sum
    def __drawValues__(self,dc,printing):
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if self.lst is None:
            return
        self.selectableBlks=[]
        self.selectedBlk=None
        sumTotal=0
        iDay=0
        ofsHr=0
        for day in self.lst:
            sum=0
            y=self.gridY*iDay
            for hour in day:
                rect,val=self.__getHourRect__(iDay,hour,ofsHr)
                sum+=val
                blk=[rect[0],rect[1],rect[0]+rect[2],rect[1]+rect[3],iDay,hour]
                self.__drawHour__(blk,False,dc,False)
                y=rect[1]
                self.selectableBlks.append(blk)
            iDay+=1
        self.__drawSum__(dc,printing)
        
    def __drawSum__(self,dc,printing):
        if self.lst is None:
            return
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if self.bSum==False:
            return
        brush=wx.Brush(self.scwDrawing.GetBackgroundColour())
        dc.SetPen(wx.Pen(self.scwDrawing.GetBackgroundColour(), 1))
        dc.SetBrush(brush)
        x=self.gridX*self.hours+self.ofsX+1
        for day in range(self.days+2):
            y=self.gridY*day+1
            dc.DrawRectangle(x, y, self.sumX-2, self.gridY-2)
        self.__setGridColor__(dc,printing)
        self.__setGridTextColor__(dc,printing)
            
        iDay=0
        ofsHr=0
        sumTotal=0
        for day in self.lst:
            sum=0
            y=self.gridY*iDay
            for hour in day:
                rect,val=self.__getHourRect__(iDay,hour,ofsHr)
                sum+=val
            dc.SetFont(wx.Font(7, wx.SWISS, wx.NORMAL, wx.NORMAL))
            #dc.SetTextForeground(wx.Colour(0x0, 0x0, 0x0))
            sTxt="""%4.2f  %02d"%02d'"""%(sum,math.floor(sum),round(sum%1*60))
            te = dc.GetTextExtent(sTxt)
            xS=self.gridX*self.hours+self.ofsX+self.sumX#+self.gridX/2
            #y=self.gridY
            dc.DrawText(sTxt,xS-2-te[0],y+self.gridY/2-te[1]/2+1)
            sumTotal=sumTotal+sum
            iDay+=1
        dc.SetFont(wx.Font(8, wx.SWISS, wx.NORMAL, wx.BOLD))
        #dc.SetTextForeground(wx.Colour(0x0, 0x0, 0x0))
        sum=sumTotal
        sTxt="""%5.2f'"""%(sum)
        te = dc.GetTextExtent(sTxt)
        xS=self.gridX*self.hours+self.ofsX+self.sumX#+self.gridX/2
        y=self.gridY*self.days#-self.gridY-self.gridY
        dc.DrawText(sTxt,xS-2-te[0],y+self.gridY/2-te[1]/2+1)
        sTxt="""%03d"%02d'"""%(math.floor(sum),round(sum%1*60))
        te = dc.GetTextExtent(sTxt)
        xS=self.gridX*self.hours+self.ofsX+self.sumX#+self.gridX/2
        y=self.gridY*self.days+self.gridY
        dc.DrawText(sTxt,xS-2-te[0],y+self.gridY/2-te[1]/2+1)
        
                    
    
    def OnSize(self, event):
        if self.verbose:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        s=event.GetSize()
        self.scwDrawing.SetSize(s)
        event.Skip()
    def Enable(self,flag):
        wx.Panel.Enable(self,flag)
        if BUFFERED:
            #if self.buffer is None:
                # Initialize the buffer bitmap.  No real DC is needed at this point.
            #    self.buffer = wx.EmptyBitmap(self.maxWidth+2, self.maxHeight+2+self.sumY)
            #    dc = wx.BufferedDC(None, self.buffer)
            #    dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
            #    dc.Clear()
            #    self.DoDrawing(dc)
            #else:
                cdc = wx.ClientDC(self.scwDrawing)
                self.scwDrawing.DoPrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
                dc.BeginDrawing()
                self.__clear__(dc,False)
                self.__drawValues__(dc,False)
                self.__drawGrid__(dc,False)
                self.__drawGridText__(dc,False)
                dc.EndDrawing()
            