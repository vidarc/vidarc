#----------------------------------------------------------------------
# Name:         vtTimeDatedValues.py
# Purpose:      text control with tree popup window
#
# Author:       Walter Obweger
#
# Created:      20060526
# CVS-ID:       $Id: vtTimeDatedValues.py,v 1.9 2008/03/26 23:12:09 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import string

from vidarc.tool.time.vtTimeInputDate import vtTimeInputDate
from vidarc.tool.time.vtTime import vtDateTime
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
from vidarc.tool.xml.vtXmlDomConsumer import *
from vidarc.tool.xml.vtXmlDomConsumerLang import *
    
VERBOSE=0

# defined event for vtTimeDatedValues item changed
wxEVT_VTTIME_DATED_VALUES_CHANGED=wx.NewEventType()
vEVT_VTTIME_DATED_VALUES_CHANGED=wx.PyEventBinder(wxEVT_VTTIME_DATED_VALUES_CHANGED,1)
def EVT_VTTIME_DATED_VALUES_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTTIME_DATED_VALUES_CHANGED,func)
class vtTimeDatedValuesChanged(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTTIME_DATED_VALUES_CHANGED(<widget_name>, self.OnChanged)
    """

    def __init__(self,obj,val):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTTIME_DATED_VALUES_CHANGED)
        self.val=val
    def GetValue(self):
        return self.val
    
# defined event for vtTimeDatedValues item selected
wxEVT_VTTIME_DATED_VALUES_SELECTED=wx.NewEventType()
vEVT_VTTIME_DATED_VALUES_SELECTED=wx.PyEventBinder(wxEVT_VTTIME_DATED_VALUES_SELECTED,1)
def EVT_VTTIME_DATED_VALUES_SELECTED(win,func):
    win.Connect(-1,-1,wxEVT_VTTIME_DATED_VALUES_SELECTED,func)
class vtTimeDatedValuesSelected(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTTIME_DATED_VALUES_SELECTED(<widget_name>, self.OnSelected)
    """

    def __init__(self,obj,val):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTTIME_DATED_VALUES_SELECTED)
        self.val=val
    def GetValue(self):
        return self.val
    
# defined event for vtTimeDatedValues item added
wxEVT_VTTIME_DATED_VALUES_ADDED=wx.NewEventType()
vEVT_VTTIME_DATED_VALUES_ADDED=wx.PyEventBinder(wxEVT_VTTIME_DATED_VALUES_ADDED,1)
def EVT_VTTIME_DATED_VALUES_ADDED(win,func):
    win.Connect(-1,-1,wxEVT_VTTIME_DATED_VALUES_ADDED,func)
class vtTimeDatedValuesAdded(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTTIME_DATED_VALUES_ADDED(<widget_name>, self.OnAdded)
    """

    def __init__(self,obj,val):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTTIME_DATED_VALUES_ADDED)
        self.val=val
    def GetValue(self):
        return self.val
    
# defined event for vtTimeDatedValues item deleted
wxEVT_VTTIME_DATED_VALUES_DELETED=wx.NewEventType()
vEVT_VTTIME_DATED_VALUES_DELETED=wx.PyEventBinder(wxEVT_VTTIME_DATED_VALUES_DELETED,1)
def EVT_VTTIME_DATED_VALUES_DELETED(win,func):
    win.Connect(-1,-1,wxEVT_VTTIME_DATED_VALUES_DELETED,func)
class vtTimeDatedValuesDeleted(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTTIME_DATED_VALUES_DELETED(<widget_name>, self.OnDeleted)
    """

    def __init__(self,obj,val):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTTIME_DATED_VALUES_DELETED)
        self.val=val
    def GetValue(self):
        return self.val
    

class vtTimeDatedValuesTransientPopup(wx.Dialog):
    #SIZE_PN_SCROLL=30
    def __init__(self, parent, size,style,name=''):
        self.SIZE_PN_SCROLL=wx.SystemSettings.GetMetric(wx.SYS_VSCROLL_X)
        wx.Dialog.__init__(self, parent,style=wx.RESIZE_BORDER)#wx.BORDER_SIMPLE)#|wx.STAY_ON_TOP)
        id=wx.NewId()
        wx.EVT_SIZE(self,self.OnSize)
        st = wx.StaticText(self, -1,name,pos=(70,4))
        self.cbCancel = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Cancel), name=u'cbCancel',
              parent=self, pos=(0,0), size=(30,30), style=wx.BU_AUTODRAW)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              self.cbCancel)
              
        self.cbApply = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Apply), name=u'cbApply',
              parent=self, pos=wx.Point(32, 00), size=wx.Size(30, 30),
              style=wx.BU_AUTODRAW)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              self.cbApply)
        
        self.cbAdd = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Add), name=u'cbAdd',
              parent=self, pos=wx.Point(64, 00), size=wx.Size(30, 30),
              style=wx.BU_AUTODRAW)
        self.cbAdd.Bind(wx.EVT_BUTTON, self.OnCbAddButton,
              self.cbAdd)
        
        self.cbDel = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Del), name=u'cbDel',
              parent=self, pos=wx.Point(96, 00), size=wx.Size(30, 30),
              style=wx.BU_AUTODRAW)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              self.cbDel)
        if size[1]<40:
            size=(size[0],40)
        i=0
        iY=40
        iH=size[1]+4
        iSzHeight=0
        sVal=parent.GetValue()
        dt=wx.DateTime.Today()
        dt.ParseFormat(sVal,'%Y%m%d')
        
        self.calCalendar = vtTimeInputDate(
              id=-1, name=u'calCalendar',draw_grid=False,
              parent=self, pos=wx.Point(4, 40),size=wx.Size(192, 192))
        #self.calCalendar.Bind(wx.calendar.EVT_CALENDAR_SEL_CHANGED,
        #      self.OnCalCalendarCalendarSelChanged,
        #      id=wxID_VCALENDARPANELCALCALENDAR)

        self.szOrig=(220,244)
        self.SetSize(self.szOrig)
    def OnSize(self,evt):
        iW,iH=self.GetSize()
        if iW<self.szOrig[0]:
            iW=self.szOrig[0]
        #if iH<self.szMin[1]:
        #    iH=self.szMin[1]
        if iH<self.szOrig[1]:
            iH=self.szOrig[1]
        self.SetSize((iW,iH))
        if self.calCalendar is not None:
            self.calCalendar.SetSize((iW-16,iH-52))
        evt.Skip()
    def OnCbCancelButton(self,evt):
        self.Show(False)
    def OnCbApplyButton(self,evt):
        #self.Apply()
        try:
            par=self.GetParent()
            sVal=self.calCalendar.GetValueStr()
            par.__apply__(sVal)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        self.Show(False)
    def OnCbAddButton(self,evt):
        try:
            par=self.GetParent()
            sVal=self.calCalendar.GetValueStr()
            par.__apply__(sVal,bAdd=True)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        self.Show(False)
    def OnCbDelButton(self,evt):
        try:
            par=self.GetParent()
            sVal=self.calCalendar.GetValueStr()
            par.__apply__(sVal,bDel=True)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        self.Show(False)
    def Show(self,flag):
        try:
            par=self.GetParent()
            if flag:
                #vtLog.CallStack('')
                #print par.GetValueIdx()
                if par.GetValueIdx()<0:
                    self.cbApply.Enable(False)
                    self.cbDel.Enable(False)
                else:
                    self.cbApply.Enable(True)
                    self.cbDel.Enable(True)
                sVal=par.GetValueStr()
                self.SetVal(sVal)
                #self.calCalendar.Refresh()
                #self.calCalendar.Update()
            else:
                #par.cbPopup.SetValue(False)
                par.__setPopupState__(False)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        wx.Dialog.Show(self,flag)
    def GetVal(self):
        try:
            sVal=self.calCalendar.GetValueStr()
            return sVal
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
            return ''
    def SetVal(self,sVal):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'val:%s'%(sVal),self)
        try:
            self.calCalendar.SetValueStr(sVal)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
class vtTimeDatedValues(wx.Panel,vtXmlDomConsumer,vtXmlDomConsumerLang):
    VERBOSE=0
    def __init__(self,*_args,**_kwargs):
        vtXmlDomConsumer.__init__(self)
        vtXmlDomConsumerLang.__init__(self)
        try:
            sz=_kwargs['size']
        except:
            sz=wx.Size(120,30)
            _kwargs['size']=sz
        try:
            szCb=_kwargs['size_button']
            del _kwargs['size_button']
        except:
            szCb=wx.Size(24,24)
        try:
            szTxt=_kwargs['size_text']
            del _kwargs['size_text']
        except:
            szTxt=wx.Size(sz[0]-szCb[0]-2,sz[1]-4)
        
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        self.SetAutoLayout(True)
        
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        size=(szTxt[0],szTxt[1])
        pos=(0,(sz[1]-szTxt[1])/2)
        self.chcDate = wx.Choice(id=-1, name='chcDate',
              parent=self, pos=pos, size=size,
              style=0, choices=[])
        self.chcDate.SetMinSize((-1,-1))
        self.chcDate.SetToolTipString('YYYY-MM-DD')
        #self.chcDate.SetConstraints(LayoutAnchors(self.chcDate, True, True,
        #      True, False))
        bxs.AddWindow(self.chcDate, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        #self.chcDate.Bind(wx.EVT_TEXT, self.OnTextText,self.chcDate)
        self.chcDate.Bind(wx.EVT_CHOICE, self.OnChoice,self.chcDate)
        
        size=(szCb[0],szCb[1])
        pos=(szTxt[0]+(sz[0]-(szTxt[0]+szCb[0]))/2,(sz[1]-szCb[1])/2)
        id=wx.NewId()
        self.cbPopup = wx.lib.buttons.GenBitmapToggleButton(ID=id,
              bitmap=wx.EmptyBitmap(8, 8), name=u'cbPopup',
              parent=self, pos=pos, size=size, style=wx.BU_AUTODRAW)
        #self.cbPopup.SetConstraints(LayoutAnchors(self.cbPopup, False, True,
        #      True, False))
        bxs.AddWindow(self.cbPopup, 0, border=0, flag=wx.EXPAND)
        self.SetSizer(bxs)
        self.cbPopup.SetBitmapLabel(vtArt.getBitmap(vtArt.Edit))
        self.cbPopup.SetBitmapSelected(vtArt.getBitmap(vtArt.Edit))
        self.cbPopup.Bind(wx.EVT_BUTTON,self.OnPopupButton,self.cbPopup)#id=id)
        self.popWin=None
        self.tagName='dated'
        self.bBlock=True
        self.lDated=[]
        self.dt=vtDateTime()
        self.args=()
        self.kwargs={}
        self.func=None
        self.lWid=[]
        #self.dftBkgColor=self.chcDate.GetBackgroundColour()
        self.bEnableMark=True
        self.bBusy=False
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
    def SetCB(self,func,*args,**kwargs):
        self.func=func
        self.args=args
        self.kwargs=kwargs
    def DoCB(self,idx):
        try:
            if self.VERBOSE:
                vtLog.CallStack('')
                print idx
                print self.lDated
            node=None
            try:
                if idx>=0:
                    tup=self.lDated[idx]
                    node=tup[2]
            except:
                pass
        except:
            vtLog.vtLngTB(self.GetName())
        if self.func is not None:
            self.func(node,*self.args,**self.kwargs)
    def Enable(self,flag):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        wx.Panel.Enable(self,flag)
        self.cbPopup.Refresh()
    def SetEnableMark(self,flag):
        self.bEnableMark=flag
    def __markModified__(self,flag=True):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.bEnableMark==False:
            return
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'__markModified__;flag:%s'%(flag),
        #                origin=self.GetName())
        if flag:
            f=self.chcDate.GetFont()
            f.SetWeight(wx.FONTWEIGHT_BOLD)
            self.chcDate.SetFont(f)
        else:
            f=self.chcDate.GetFont()
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
            self.chcDate.SetFont(f)
        self.chcDate.Refresh()
    def __markFlt__(self,flag=False):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if flag:
            color=wx.TheColourDatabase.Find('YELLOW')
            self.chcDate.SetBackgroundColour(color)
        else:
            color=wx.SystemSettings_GetColour(wx.SYS_COLOUR_WINDOW)
            self.chcDate.SetBackgroundColour(color)
    def SetTagNames(self,tagName):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'SetTagNames;tagname:%s,%s'%(tagName,tagNameInt),
        #                origin=self.GetName())
        self.tagName=tagName
    SetTagName=SetTagNames
    def ClearLang(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'ClearLang',
        #                origin=self.GetName())
        #if VERBOSE:
        #    vtLog.CallStack('')
        self.chcDate.Clear()
        pass
    def UpdateLang(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'UpdateLang',
        #                origin=self.GetName())
        return
        #if VERBOSE:
        #    vtLog.CallStack('')
        try:
            if self.popWin is None:
                sVal=self.doc.getNodeText(self.node,self.tagName)
            else:
                sVal=self.popWin.GetVal(self.doc.GetLang())
            self.bBlock=True
            self.chcDate.SetValue(string.split(sVal,'\n')[0])
            wx.CallAfter(self.__clearBlock__)
            #self.chcDate.SetValue(sVal)
        except:
            vtLog.vtLngTB(self.GetName())

    def Clear(self):
        self.lDated=[]
        self.ClearLang()
        vtXmlDomConsumer.Clear(self)
        self.__markModified__(False)
        self.DoCB(-1)
    def __setPopupState__(self,flag):
        self.bBusy=flag
        self.cbPopup.SetValue(flag)
    def IsBusy(self):
        return self.bBusy
    def __Close__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.popWin is not None:
            self.popWin.Show(False)
            self.__setPopupState__(False)
    def Stop(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'Stop',self)
        wx.CallAfter(self.__Close__)
    def SetDoc(self,doc):
        vtXmlDomConsumer.SetDoc(self,doc)
        vtXmlDomConsumerLang.SetDoc(self,doc)
    def __addDated__(self,node,*args):
        sTag=self.doc.getTagName(node)
        if sTag==self.tagName:
            sVal=self.doc.getAttribute(node,'validBy')
            if self.VERBOSE:
                print sVal
            try:
                self.dt.SetDateStr(sVal)
                sVal=self.dt.GetDateStr()
                self.lDated.append([sVal,sVal,node,len(self.lDated)])
            except:
                self.__markModified__(True)
                vtLog.vtLngTB(self.GetName())
                pass
        return 0
    def SetNode(self,node):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'SetNode;node:%s'%(node),
        #                origin=self.GetName())
        if self.VERBOSE:
            vtLog.CallStack('')
            print node
        self.Clear()
        vtXmlDomConsumer.SetNode(self,node)
        if self.doc is None:
            return
        if self.node is None:
            return
        try:
            self.doc.procChilds(self.node,self.__addDated__)
            self.lDated.sort()
            self.lDated.reverse()
            
            for tup in self.lDated:
                self.chcDate.Append(tup[0],tup[-1])
            self.bBlock=True
            self.chcDate.Enable(True)
            self.cbPopup.Enable(True)
            if len(self.lDated)>0:
                self.chcDate.SetSelection(0)
                self.DoCB(0)
            else:
                self.DoCB(-1)
            wx.CallAfter(self.__clearBlock__)
        except:
            vtLog.vtLngTB(self.GetName())
        #self.__getSelNode__()
        self.UpdateLang()
    def __validate__(self,sVal,bSet2NowOnFlt=False):
        #if VERBOSE:
        #    vtLog.CallStack(sVal)
        bMod=False
        bOk=False
        try:
            self.dt.SetDateStr(sVal)
            bOk=True
        except:
            #vtLog.vtLngCurWX(vtLog.WARN,sVal,self)
            if bSet2NowOnFlt:
                bMod=True
        if bMod:
            if bSet2NowOnFlt:
                self.dt=self.dt.Now()
        sVal=self.dt.GetDateStr()
        return bOk,bMod,sVal
    def __clearBlock__(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'__clearBlock__',
        #                origin=self.GetName())
        self.bBlock=False
    def Enable(self,flag):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.chcDate.Enable(flag)
        self.cbPopup.Enable(flag)
    def __setDated__(self,node,*args):
        i=args[0][0]
        nodePar=args[0][1]
        sTag=self.doc.getTagName(node)
        if sTag==self.tagName:
            sVal=self.doc.getAttribute(node,'validBy')
            if self.VERBOSE:
                print sVal
            for tup in self.lDated:
                if tup[1]==sVal:
                    if tup[0]!=None:
                        if tup[0]!=tup[1]:
                            self.doc.setAttribute(node,'validBy',tup[0])
                        if i==self.iAct:
                            self.node2Sel=node
                    else:
                        self.doc.deleteNode(node,nodePar)
            self.iAct+=1
        return 0
    def GetNode(self,node=None):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if node is None:
            node=self.node
        if node is None:
            return
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'GetNode',
        #                origin=self.GetName())
        if self.VERBOSE:
            vtLog.CallStack('')
        try:
            try:
                idx=self.chcDate.GetSelection()
                i=self.chcDate.GetClientData(idx)
            except:
                i=-1
            self.iAct=0
            self.node2Sel=None
            self.doc.procChilds(node,self.__setDated__,i,node)
            if self.VERBOSE:
                print self.node2Sel
            for tup in self.lDated:
                if tup[1] is None:
                    if tup[0] is not None:
                        c=self.doc.createSubNode(node,self.tagName)#'dated')
                        self.doc.setAttribute(c,'validBy',tup[0])
                        self.node2Sel=c
                        tup[2]=c
            l=[]
            i=0
            for tup in self.lDated:
                if tup[0] is not None:
                    tup[1]=tup[0]
                    tup[3]=i
                    l.append(tup)
                    i+=1
            self.lDated=l
            if self.VERBOSE:
                print self.node2Sel
            self.Enable(True)
            self.__markModified__(False)
            self.doc.AlignNode(node)
            return self.node2Sel
        except:
            vtLog.vtLngTB(self.GetName())
        return None
    def __getDoc__(self):
        return self.doc
    def __getNode__(self,node=None):
        if node is None:
            return self.node
        return node
    def __apply__(self,sVal,bAdd=False,bDel=False):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'__apply__;val:%s'%(sVal),
        #                origin=self.GetName())
        #if VERBOSE:
        #    vtLog.CallStack(sVal)
        try:
            #sVal=self.popWin.GetVal()
            #if self.trSetNode is not None:
            #    sVal=self.trSetNode(self.docTreeTup[0],node)
            #else:
            #    sVal=''
            bOk,bMod,sVal=self.__validate__(sVal)
            self.__markFlt__(not bOk)
            if bAdd==False:
                try:
                    idx=self.chcDate.GetSelection()
                    i=self.chcDate.GetClientData(idx)
                    if bDel==False:
                        s=self.chcDate.GetStringSelection()
                        self.lDated[idx][0]=sVal
                        if sVal!=s:
                            bMod=True
                        self.chcDate.SetString(idx,sVal)
                        self.chcDate.SetSelection(idx)
                        if bMod:
                            if self.bBlock==False:
                                self.__markModified__(True)
                                wx.PostEvent(self,vtTimeDatedValuesChanged(self,sVal))
                    else:
                        self.lDated[idx][0]=None
                        self.chcDate.Delete(idx)
                        self.chcDate.SetSelection(0)
                        self.chcDate.Enable(False)
                        self.cbPopup.Enable(False)
                        wx.PostEvent(self,vtTimeDatedValuesDeleted(self,sVal))
                except:
                    bMod=True
            else:
                self.lDated.append([sVal,None,None,len(self.lDated)])
                bMod=True
                self.chcDate.Append(sVal,len(self.lDated)-1)
                self.chcDate.SetSelection(len(self.lDated)-1)
                self.chcDate.Enable(False)
                self.cbPopup.Enable(False)
                #vtLog.CallStack('')
                wx.PostEvent(self,vtTimeDatedValuesAdded(self,sVal))
        except:
            sVal=_(u'fault')
            vtLog.vtLngTB(self.GetName())
        self.bBlock=True
        #self.chcDate.SetValue(string.split(sVal,'\n')[0])
        wx.CallAfter(self.__clearBlock__)
        return sVal
        
    def __createPopup__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'__createPopup__',
        #                origin=self.GetName())
        if self.popWin is None:
            sz=self.GetSize()
            sz=(sz[0],sz[1]-20)
            try:
                self.popWin=vtTimeDatedValuesTransientPopup(self,sz,wx.SIMPLE_BORDER)
            except:
                vtLog.vtLngTB(self.GetName())
            #self.popWin.SetVal(self.chcDate.GetValue(),self.doc.GetLang())
        else:
            pass
    def SetValueStr(self,sVal):
        #self.chcDate.SetValue(val)
        self.__apply__(sVal[0:4]+sVal[5:7]+sVal[8:10])
        #self.__apply__(sVal)
        #self.__validate__(sVal,bTxt=True)
    def SetValue(self,sVal):
        #self.chcDate.SetValue(val)
        self.__apply__(sVal)
    def GetValueIdx(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'%d %s'%(self.chcDate.GetSelection(),self.chcDate.GetStringSelection()),self)
        return self.chcDate.GetSelection()
    def GetValueStr(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%self.chcDate.GetValue(),self)
        return self.chcDate.GetStringSelection()
    def GetValue(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        sVal=self.chcDate.GetStringSelection()
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%sVal,self)
        return sVal
            
    def OnTextText(self,evt):
        if self.bBlock:
            return
        sVal=self.chcDate.GetValue()
        try:
            bOk,bMod,val=self.__validate__(sVal,False)
            self.__markFlt__(not bOk)
            if bMod:
                self.chcDate.SetValue(val)
        except:
            vtLog.vtLngTB(self.GetName())
        self.__markModified__()
        wx.PostEvent(self,vtTimeDateChanged(self,sVal))
    def OnChoice(self,evt):
        try:
            sVal=self.chcDate.GetStringSelection()
            idx=self.chcDate.GetSelection()
        except:
            sVal=''
            idx=-1
        self.DoCB(idx)
        wx.PostEvent(self,vtTimeDatedValuesSelected(self,sVal))
    def OnPopupButton(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.__createPopup__()
        if self.cbPopup.GetValue()==True:
            btn=self
            iX,iY = btn.ClientToScreen( (0,0) )
            iW,iH =  btn.GetSize()
            iPopW,iPopH=self.popWin.GetSize()
            iScreenW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            iScreenH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            if iX+iPopW>iScreenW:
                iX=iScreenW-iPopW-4
                if iX<0:
                    iX=0
            iY+=iH
            if iY+iPopH>iScreenH:
                iY=iScreenH-iPopH
                if iY<0:
                    iY=0
            
            self.popWin.Move((iX,iY))
            #self.popWin.Move((pos[0],pos[1]+sz[1]))
            self.bBusy=True
            self.popWin.Show(True)
        else:
            self.popWin.Show(False)
            self.bBusy=False
        evt.Skip()

