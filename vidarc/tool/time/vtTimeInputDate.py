#----------------------------------------------------------------------------
# Name:         vtTimeInputDate.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060609
# CVS-ID:       $Id: vtTimeInputDate.py,v 1.7 2008/03/26 23:12:09 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import cStringIO

from vidarc.tool.draw.vtDrawCanvasPanel import *
from vidarc.tool.draw.vtDrawCanvasObjectBase import *
from vidarc.tool.draw.vtDrawCanvasGroup import *
from vidarc.tool.draw.vtDrawCanvasLine import *
from vidarc.tool.draw.vtDrawCanvasRectangle import *
from vidarc.tool.draw.vtDrawCanvasRectangleFilled import *
from vidarc.tool.draw.vtDrawCanvasText import *
from vidarc.tool.draw.vtDrawCanvasTextMultiLine import *
from vidarc.tool.draw.vtDrawCanvasTextSelectable import *
from vidarc.tool.draw.vtDrawCanvasPercentage import *
from vidarc.tool.draw.vtDrawCanvasPercentageLabel import *
from vidarc.tool.draw.vtDrawCanvasHourStartEnd import *
from vidarc.tool.draw.vtDrawCanvasHourStartEndLabel import *

import vidarc.tool.time.__init__ as __initTm__
import vidarc.tool.art.vtColor as vtColor
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
import calendar,types,copy
import vidarc.tool.time.vtTime as vtTime

[wxID_VTDRAWCANVASPANEL, wxID_VTDRAWCANVASPANELSCWDRAWING, 
 wxID_VTDRAWCANVASPANELCHCMONTH, wxID_VTDRAWCANVASPANELSPYEAR, 
 wxID_VTDRAWCANVASPANELSPWEEK, 
] = [wx.NewId() for _init_ctrls in range(5)]

wxEVT_VTTIME_INPUT_DATE_CHANGED=wx.NewEventType()
vEVT_VTTIME_INPUT_DATE_CHANGED=wx.PyEventBinder(wxEVT_VTTIME_INPUT_DATE_CHANGED,1)
def EVT_VTTIME_INPUT_DATE_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTTIME_INPUT_DATE_CHANGED,func)
class vtTimeInputDateChanged(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VTTIME_INPUT_DATE_CHANGED(<widget_name>, self.OnItemSel)
    """

    def __init__(self,obj,sDate):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTTIME_INPUT_DATE_CHANGED)
        self.sDt=sDate
    def GetCanvas(self):
        return self.obj
    def GetDate(self):
        return self.sDt
    def IsValid(self):
        if self.sDt[0]=='_':
            return False
        return True

class vtTimeDy(vtDrawCanvasGroup):
    def __init__(self,id=-1,name='',text='',x=0,y=0,w=0,h=0,layer=0):
        vtDrawCanvasGroup.__init__(self,id,name,x,y,w,h,layer)
        obj=vtDrawCanvasRectangleFilled(id=-1,name=name+'_rcf',
                        x=0,y=0,w=w,h=h,layer=layer)
        obj.SetVisible(False)
        self.Add(obj)
        obj=vtDrawCanvasText(id=-1,name=name+'_txt',text=text,
                        x=0,y=0,w=w,h=h,layer=layer)
        self.Add(obj)
    def GetAttr(self,idx):
        if idx<6:
            return vtDrawCanvasObjectBase.GetAttr(self,idx)
        return self.objs[1].GetAttr(idx)
    def SetAttr(self,idx,val):
        if idx<6:
            return vtDrawCanvasObjectBase.SetAttr(self,idx,val)
        return self.objs[1].SetAttr(idx,val)
    def __draw__(self,canvas,aX=0,aY=0,aW=-1,aH=-1,iDriver=-1):
        if self.bActive:
            self.objs[0].SetVisible(True)
            #self.objs[1].SetLayer(self.GetLayer())
            self.objs[1].SetLayer(-1)
        else:
            self.objs[0].SetVisible(False)
            self.objs[1].SetLayer(self.GetLayer())
            
        vtDrawCanvasGroup.__draw__(self,canvas,aX,aY,aW,aH,iDriver)
        
        #if self.bActive:
        #    self.objs[1].SetFocus(True)
            #self.objs[1].SetLayer(-1)
        #    self.objs[1].__drawSelect__(canvas,aX,aY,aW,aH,iDriver)
        #    self.objs[1].SetFocus(False)
        #else:
        #    self.objs[0].SetActive(True)
        #    self.objs[0].__drawSelect__(canvas,aX,aY,aW,aH,iDriver)
        self.SetSelected(False)
        if self.bActive:
            iLayer=self.GetLayer()
            self.SetLayer(-1)
        self.__drawSelect__(canvas,aX,aY,aW,aH,iDriver)
        if self.bActive:
            self.SetLayer(iLayer)
        #self.txt.__draw__(canvas,aX,aY,aW,aH,iDriver)


class vtTimeInputDate(vtDrawCanvasPanel):
    VERBOSE=0
    if __initTm__.DATE_MULITCOLOR==1:
        DFT_COLOR=[
            vtColor.MONDAY,
            vtColor.TUESDAY,
            vtColor.WEDNESDAY,
            vtColor.THURSDAY,
            vtColor.FRIDAY,
            vtColor.SATURDAY,
            vtColor.SUNDAY,
            
            vtColor.SEAGREEN,           # week layer = 7
            vtColor.SADDLE_BROWN,
            vtColor.WHITE,
            vtColor.BLACK,
            ]
        DFT_DIS_COLOR=[
            vtColor.Scale(vtColor.MONDAY,0.8),
            vtColor.Scale(vtColor.TUESDAY,0.8),
            vtColor.Scale(vtColor.WEDNESDAY,0.8),
            vtColor.Scale(vtColor.THURSDAY,0.8),
            vtColor.Scale(vtColor.FRIDAY,0.8),
            vtColor.Scale(vtColor.SATURDAY,0.8),
            vtColor.Scale(vtColor.SUNDAY,0.8),
            vtColor.Scale(vtColor.SEAGREEN,0.8),
            vtColor.Scale(vtColor.SADDLE_BROWN,0.8),
            vtColor.Scale(vtColor.WHITE,0.8),
            vtColor.Scale(vtColor.BLACK,0.8),
            ]
    else:
        DFT_COLOR=[
            vtColor.BLACK,
            vtColor.BLACK,
            vtColor.BLACK,
            vtColor.BLACK,
            vtColor.BLACK,
            vtColor.RED,
            vtColor.RED,
            
            vtColor.SEAGREEN,           # week layer = 7
            vtColor.SADDLE_BROWN,
            vtColor.WHITE,
            vtColor.BLACK,
            ]
        DFT_DIS_COLOR=[
            vtColor.Scale(vtColor.MONDAY,0.8),
            vtColor.Scale(vtColor.TUESDAY,0.8),
            vtColor.Scale(vtColor.WEDNESDAY,0.8),
            vtColor.Scale(vtColor.THURSDAY,0.8),
            vtColor.Scale(vtColor.FRIDAY,0.8),
            vtColor.Scale(vtColor.SATURDAY,0.8),
            vtColor.Scale(vtColor.SUNDAY,0.8),
            vtColor.Scale(vtColor.SEAGREEN,0.8),
            vtColor.Scale(vtColor.SADDLE_BROWN,0.8),
            vtColor.Scale(vtColor.WHITE,0.8),
            vtColor.Scale(vtColor.BLACK,0.8),
            ]
    FOCUS_LAYER=7
    SELECTED_LAYER=7
    ACT_COLOR_FACTOR=0.85
    DIS_COLOR_FACTOR=0.7
    DFT_COLOR_FACTOR=0.85
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTDRAWCANVASPANEL, name='', parent=prnt,
              pos=wx.Point(0, 0), size=wx.Size(100, 100),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(100, 100))
        self.SetAutoLayout(True)
        fgs = wx.FlexGridSizer(cols=1, hgap=0, rows=2, vgap=0)
        fgs.AddGrowableRow(1)
        fgs.AddGrowableCol(0)
        #fgs.AddGrowableCol(1)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        self.spYear = wx.SpinCtrl(id=wxID_VTDRAWCANVASPANELSPYEAR, initial=2000, max=2100,
              min=0, name=u'spYear', parent=self, pos=wx.DefaultPosition,
              size=wx.DefaultSize, style=wx.SP_ARROW_KEYS)
        self.spYear.Bind(wx.EVT_SPINCTRL, self.OnUpdateDate,self.spYear)
        self.spYear.Bind(wx.EVT_TEXT, self.OnUpdateDate,self.spYear)
        
        id=wx.NewId()
        self.cbDec = wx.lib.buttons.GenBitmapButton(ID=id,
              bitmap=vtArt.getBitmap(vtArt.Left), name=u'cbDec',
              parent=self, style=wx.BU_AUTODRAW)
        #self.cbDec.SetBitmapSelected(getLeftBitmap())
        self.cbDec.Bind(wx.EVT_BUTTON,self.OnDecButton,self.cbDec)
        
        id=wx.NewId()
        self.cbInc = wx.lib.buttons.GenBitmapButton(ID=id,
              bitmap=vtArt.getBitmap(vtArt.Right), name=u'cbInc',
              parent=self, style=wx.BU_AUTODRAW)
        #self.cbInc.SetBitmapSelected(getRightBitmap())
        self.cbInc.Bind(wx.EVT_BUTTON,self.OnIncButton,self.cbInc)
        
        self.chcMonth = wx.Choice(choices=[], id=wxID_VTDRAWCANVASPANELCHCMONTH,
              name=u'chcMonth', parent=self, pos=wx.DefaultPosition,
              size=wx.DefaultSize, style=0)
        self.chcMonth.Bind(wx.EVT_CHOICE, self.OnUpdateDateChc,self.chcMonth)
        
        bxs.AddWindow(self.spYear, 1, border=0, flag=wx.EXPAND)
        bxs.AddWindow(self.cbDec, 0, border=0, flag=wx.ALIGN_CENTRE)
        bxs.AddWindow(self.chcMonth, 1, border=0, flag=wx.EXPAND)
        bxs.AddWindow(self.cbInc, 0, border=0, flag=wx.ALIGN_CENTRE)
        
        self.scwDrawing = wx.ScrolledWindow(id=wxID_VTDRAWCANVASPANELSCWDRAWING,
              name=u'scwDrawing', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(100, 100), style=wx.SUNKEN_BORDER | wx.HSCROLL | wx.VSCROLL)
        #self.scwDrawing.SetConstraints(LayoutAnchors(self.scwDrawing, True,
        #      True, True, True))
        self.scwDrawing.Bind(wx.EVT_MOUSEWHEEL, self.OnDrawMouseWheel)
        
        fgs.AddSizer(bxs, 0, border=0, flag=wx.EXPAND|wx.LEFT|wx.RIGHT)
        fgs.AddWindow(self.scwDrawing, 0, border=4, flag=wx.EXPAND|wx.TOP)
        self.SetSizer(fgs)
        #EVT_VTDRAW_CANVAS_ITEM_ACTIVATED(self,self.OnActivatedElement)
        #EVT_VTDRAW_CANVAS_ITEM_DEACTIVATED(self,self.OnDeActivatedElement)
        #EVT_VTDRAW_CANVAS_ITEM_SELECTED(self,self.OnSelectedElement)
        #EVT_VTDRAW_CANVAS_ITEM_UNSELECTED(self,self.OnUnSelectedElement)
    def __init__(self, parent, 
                id=wx.NewId(), pos=wx.DefaultPosition, size=wx.Size(192, 192), 
                style=wx.TAB_TRAVERSAL, name=u'vtdDate',
                draw_grid=True,is_gui_wx=True,
                grid_x=20,grid_y=20):
        max=(9,7)
        vtDrawCanvasPanel.__init__(self, parent, 
                id=wx.NewId(), pos=pos, size=size, 
                style=style, name=name,
                draw_grid=draw_grid,
                is_gui_wx=is_gui_wx,
                max_x=max[0],max_y=max[1],
                grid_x=grid_x,grid_y=grid_y)
        self.SetActivateOnSelect(True)
        
        self.RegisterObject(vtTimeDy())
        self.__updateLang__()
        self.dt=vtTime.vtDateTime(True)
        #self.dt.SetDay(1)
        self.__initDayLblBlks__()
        self.__showDate__()
        
        obj=vtDrawCanvasObjectBase()
        self.SetConfig(obj,obj.DRV_WX,max=max,
                    grid=(grid_x,grid_y))
        EVT_VTDRAW_CANVAS_ITEM_ACTIVATED(self,self.OnActivated)
        EVT_VTDRAW_CANVAS_ITEM_DEACTIVATED(self,self.OnDeActivated)
        self.scwDrawing.SetFocus()
    def OnActivated(self,evt):
        blk=evt.GetBlock()
        sName=blk.GetName()
        bMnS=bMnE=False
        self.BeginBatch()
        if self.VERBOSE:
            vtLog.CallStack('')
            print sName
            print blk.GetId()
            print blk.bSelected,blk.bActive
        for obj in self.objs:
            if obj.GetActive():
                if obj.GetId()!=blk.GetId():
                    obj.SetActive(False)
                    self.ShowObj(obj,False)
        try:
            self.dt.SetValid(True)
            self.dt.SetDay(int(blk.GetAttr(6)[1]))
        except:
            pass
        self.EndBatch()
        wx.PostEvent(self,vtTimeInputDateChanged(self,
                                self.dt.GetDateSmallStr()))
    def OnDeActivated(self,evt):
        blk=evt.GetBlock()
        sName=blk.GetName()
        bMnS=bMnE=False
        if self.VERBOSE:
            vtLog.CallStack('')
            print sName
            print blk.GetId()
        self.dt.SetValid(False)
        wx.PostEvent(self,vtTimeInputDateChanged(self,
                                self.dt.GetDateSmallStr()))
    def __initDayLblBlks__(self):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.lblBlks=[]
        self.objs=[]
        
        mrng=calendar.monthrange(self.dt.GetYear(),self.dt.GetMonth())
        iMon=self.dt.GetMonth()
        iWk=self.dt.GetCalendarISO()[1]
        iLv=mrng[0]
        for iDy in xrange(0,8):
            if iDy<7:
                sLbl=calendar.day_abbr[iDy]
            else:
                sLbl=_('wk')
                iDy+=1
            blk=vtDrawCanvasText(id=100+iDy,name='lblday_%02d'%(iDy),
                    text=sLbl,
                    x=iDy,y=0,w=1,h=1,layer=iDy)
            self.lblBlks.append(blk)
        for iWk in xrange(1,7):
            for iDy in xrange(0,8):
                if iDy<7:
                    blk=vtTimeDy(id=iWk*10+iDy,name='day_%02d'%(iWk*10+iDy),
                            text='%02d'%(iDy),
                            x=iDy,y=iWk,w=1,h=1,layer=iDy)
                else:
                    blk=vtDrawCanvasText(id=iWk*10+iDy,name='day_%02d'%(iWk*10+iDy),
                            text='%02d'%(iDy),
                            x=iDy+1,y=iWk,w=1,h=1,layer=iDy)
                self.objs.append(blk)
    def __clearInt__(self):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        
    def __updateDayLblBlks__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        bValid=self.dt.IsValid()
        self.dt.SetValid(True)
        mrng=calendar.monthrange(self.dt.GetYear(),self.dt.GetMonth())
        iMon=self.dt.GetMonth()
        iLv=mrng[0]
        iDyMx=mrng[1]
        if bValid:
            iDaySel=self.dt.GetDay()
        else:
            iDaySel=-1
        self.dt.SetDay(1)
        iWeek=self.dt.GetCalendarISO()[1]
        if bValid:
            self.dt.SetDay(iDaySel)
        self.dt.SetValid(bValid)
        vtLog.vtLngCurWX(vtLog.DEBUG,'year:%d month:%d;day:%d;monthrange:%s;month:%d;week:%d'%(self.dt.GetYear(),self.dt.GetMonth(),self.dt.GetDay(),vtLog.pformat(mrng),iMon,iWeek),self)
        #i=1
        #for blk in self.lblBlks:
        bWk=True
        idx=0
        iDay=-1
        self.BeginBatch()
        for iWk in xrange(0,6):
            for iDy in xrange(0,7):
                blk=self.objs[idx]
                if iWk==0:
                    if iDy<iLv:
                        blk.SetVisible(False)
                    else:
                        blk.SetVisible(True)
                        if iDay<0:
                            iDay=1
                        else:
                            iDay+=1
                else:
                    iDay+=1
                if iDay>0:
                    blk.SetAttr(6,'%02d'%(iDay))
                    blk.SetVisible(True)
                if iDay>iDyMx:
                    blk.SetVisible(False)
                    if iDy==0:
                        bWk=False
                if iDay==iDaySel:
                    blk.SetActive(True)
                else:
                    blk.SetActive(False)
                idx+=1
            blk=self.objs[idx]
            blk.SetAttr(6,'%02d'%(iWeek))
            iWeek+=1
            if iMon==1:
                if iWeek>52:
                    iWeek=1
            else:
                if iWeek>53:
                    iWeek=1
            if iDay>iDyMx:
                if bWk:
                    bWk=False
                    blk.SetVisible(True)
                else:
                    blk.SetVisible(False)
            idx+=1
        self.EndBatch()
    def __drawObjects__(self,dc,printing):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        # overload me
        #vtLog.vtLogCallDepth(None,'',self.VERBOSE=1)
        try:
            self.drvInfo[self.objBase.DRV_WX]['dc']=dc
            self.drvInfo[self.objBase.DRV_WX]['printing']=printing
        except:
            #traceback.print_exc()
            return
        if 1==0:
            if self.thdDraw.IsRunning():
                return
            self.drvInfo[self.objBase.DRV_WX]['act_layer']=-1
            self.thdDraw.Start(self)
        else:
            #self.semDraw.acquire()
            #vtLog.CallStack('')
            self.drvInfo[self.objBase.DRV_WX]['act_layer']=-1
            #for blk in self.lblBlksHr:
            #    blk.__draw__(self)
            for blk in self.lblBlks:
                blk.__draw__(self)
            for o in self.objs:
                o.__draw__(self)
            self.drvInfo[self.objBase.DRV_WX]['dc']=None
            #self.semDraw.release()
        #vtDrawCanvasPanel.__drawGrid__(self,dc,printing)
        #vtDrawCanvasPanel.__drawObjects__(self,dc,printing)
        #for blk in self.lblBlks:
        #    blk.__draw__(self)
    def __updateLang__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.chcMonth.Clear()
        for i in range(1,13):
            self.chcMonth.Append(calendar.month_name[i])
    def __showDate__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        bValid=self.dt.IsValid()
        self.dt.SetValid(True)
        self.spYear.SetValue(self.dt.GetYear())
        self.chcMonth.SetSelection(self.dt.GetMonth()-1)
        self.dt.SetValid(bValid)
        #mrng=calendar.monthrange(self.dt.GetYear(),self.dt.GetMonth())
        self.__updateDayLblBlks__()
    def OnDrawMouseWheel(self,evt):
        if evt.GetWheelRotation()<0:
            #self.filLog.MoveValue(-event.GetLinesPerAction())
            self.OnDecButton(None)
        else:
            #self.filLog.MoveValue(event.GetLinesPerAction())
            self.OnIncButton(None)
        pass
    def OnDecButton(self,evt):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        bValid=self.dt.IsValid()
        self.dt.SetValid(True)
        iYear=self.dt.GetYear()
        iMon=self.dt.GetMonth()
        iDay=self.dt.GetDay()
        iMon-=1
        if iMon<1:
            iYear-=1
            iMon=12
        mrng=calendar.monthrange(iYear,iMon)
        if iDay>mrng[1]:
            iDay=mrng[1]
        self.dt.SetDate(iYear,iMon,iDay)
        self.dt.SetValid(bValid)
        self.__showDate__()
        self.ShowAll()
    def OnIncButton(self,evt):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        bValid=self.dt.IsValid()
        self.dt.SetValid(True)
        iYear=self.dt.GetYear()
        iMon=self.dt.GetMonth()
        iDay=self.dt.GetDay()
        iMon+=1
        if iMon>12:
            iYear+=1
            iMon=1
        mrng=calendar.monthrange(iYear,iMon)
        if iDay>mrng[1]:
            iDay=mrng[1]
        self.dt.SetDate(iYear,iMon,iDay)
        self.dt.SetValid(bValid)
        self.__showDate__()
        self.ShowAll()
        #evt.Skip()
    def OnUpdateDate(self,evt):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        try:
            bValid=self.dt.IsValid()
            self.dt.SetValid(True)
            iYear=int(self.spYear.GetValue())
            self.dt.SetYear(iYear)
            self.dt.SetValid(bValid)
            self.__showDate__()
            self.ShowAll()
            self.scwDrawing.SetFocus()
        except:
            pass
        evt.Skip()
    def OnUpdateDateChc(self,evt):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        bValid=self.dt.IsValid()
        self.dt.SetValid(True)
        self.dt.SetMonth(self.chcMonth.GetSelection()+1)
        self.dt.SetValid(bValid)
        self.__showDate__()
        self.ShowAll()
        self.scwDrawing.SetFocus()
        evt.Skip()
    def SetValueStr(self,sDate):
        try:
            self.dt.SetDateStr(sDate)
        except:
            vtLog.vtLngCurWX(vtLog.WARN,'conversion error:%s'%(sDate),self)
            #vtLog.vtLngTB(self.GetName())
        self.__showDate__()
        self.ShowAll()
        return
    def SetValueSmallStr(self,sDate):
        try:
            self.dt.SetDateSmallStr(sDate)
        except:
            vtLog.vtLngCurWX(vtLog.ERROR,'',self)
        self.__showDate__()
        self.ShowAll()
        return
    def ClearInt(self):
        self.__clrActDay__()
    def __clrActDay__(self):
        for obj in self.objs:
            if obj.GetActive():
                obj.SetActive(False)
                self.ShowObj(obj,False)
    def __setActDay__(self,iDay):
        sDay='%02d'%iDay
        self.__clrActDay__()
        for obj in self.objs:
            tup=obj.GetAttr(6)
            if tup[1]==sDay:
                obj.SetActive(True)
                self.ShowObj(obj,False)
                break
    def __getActDay__(self):
        for obj in self.objs:
            if obj.GetActive():
                tup=obj.GetAttr(6)
                sDay=tup[1]
                return sDay
        return '01'
    def GetDate(self):
        return self.dt
    def GetValueStr(self):
        try:
            iYear=self.dt.GetYear()
            iMonth=self.dt.GetMonth()
            sDay=self.__getActDay__()
            sVal='%04d-%02d-%s'%(iYear,iMonth,sDay)
            return sVal
        except:
            vtLog.vtLngCurWX(vtLog.ERROR,'',self)
        return None
    def GetValueSmallStr(self):
        try:
            iYear=self.dt.GetYear()
            iMonth=self.dt.GetMonth()
            sDay=self.__getActDay__()
            sVal='%04d%02d%s'%(iYear,iMonth,sDay)
            return sVal
        except:
            vtLog.vtLngCurWX(vtLog.ERROR,'',self)
        return None
    def __getCurDateValues__(self):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        try:
            iYear=self.dt.GetYear()
            iMonth=self.dt.GetMonth()
        except:
            vtLog.vtLngTB(self.GetName())
            return {}
    def __showValues__(self):
        vtLog.vtLngCurWX(vtLog.ERROR,'',self)
        return
    def OnMoveEvent(self,evt):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtDrawCanvasPanel.OnMoveEvent(self,evt)
        xCoor,yCoor=self.calcPos(self.x,self.y,False)
        for o in self.objs:
            if o.GetVisible():
                iInterSect=o.IsInsideTup(self,xCoor,yCoor)
                if iInterSect:
                    #print 'found',o
                    return
            else:
                break
    def OnKillFocus(self,evt):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtDrawCanvasPanel.OnKillFocus(self,evt)
        #self.__clearInt__()
    def __setHourValues__(self,blk):
        vtLog.vtLngCurWX(vtLog.ERROR,'',self)
        return
    def __drawBorder__(self):
        vtLog.vtLngCurWX(vtLog.ERROR,'',self)
    def SetNow(self):
        self.dt.Now()
        self.__showDate__()
        self.ShowAll()
