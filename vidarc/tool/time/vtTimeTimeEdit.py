#----------------------------------------------------------------------
# Name:         vtTimeTimeEdit.py
# Purpose:      text control with tree popup window
#
# Author:       Walter Obweger
#
# Created:      20060210
# CVS-ID:       $Id: vtTimeTimeEdit.py,v 1.18 2010/03/10 22:41:09 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
from wx.lib.anchors import LayoutAnchors

import cStringIO
import string,time

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomConsumer import *
from vidarc.tool.xml.vtXmlDomConsumerLang import *
from vidarc.tool.time.vtTime import vtTime
import vidarc.tool.lang.vtLgBase as vtLgBase

if __name__ == "__main__":
    import vidarc.tool.lang.vtLgBase as vtLgBase
    from vidarc.tool.xml.vtXmlDom import *
    import sys
    
VERBOSE=0

#----------------------------------------------------------------------
def getCancelData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00oIDAT8\x8d\xad\x93\xc1\x11\xc0 \x08\x04\x0f\xd3E\xfa\xaf\xcd6\xcc+\
\x19\xd1;p\x86\xf0fw\x80S\xb3v\xa1R\xadD\xff*\xe8\x86\xd1\r#\x03\xd6\xbeOp\
\x0f\xd8\xdb\x10\xc1s\xaf\x13d\x12\x06o\x02%Q0\x00\x98\x8aq\x9d\x82\xc1t\x02\
\x06(8\x14\xb0\x15\x8e\x05\xf3\xceY:\x9b\x80\x1d,\x928Atm%q/Q\xc1\x91D\xc6xZ\
\xe5\xcf\xf4\x00\xe0\xc8:\xc8\xd18`E\x00\x00\x00\x00IEND\xaeB`\x82' 

def getCancelBitmap():
    return wx.BitmapFromImage(getCancelImage())

def getCancelImage():
    stream = cStringIO.StringIO(getCancelData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getApplyData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00pIDAT8\x8dcddbf\xa0\x040\x91\xa3Ix\x89\xe0\x7f\xe1%\x82\xff\x19\x18\
\x18\x18XH\xd5\x08c\xbf\x8dy\xcfH\x92\x010\xcd0\x8d0@\x94\x17pi&\xda\x00\\\
\x9a\x892\x00\xd9\xdfd\x19@\x08\xe05\x00\x9f\xdfQ\x0c@\x8eW\x8a\\@\x8eAL0\'\
\xa2;\x93\x18\xe7c\xb8\x00\xa6\x98\x14W`\x04"\xb2\x8d\x84l\xc7j\x00\xa9\x80\
\x91\xd2\xec\x0c\x00y\x1c/\xbdxe+\x9e\x00\x00\x00\x00IEND\xaeB`\x82' 

def getApplyBitmap():
    return wx.BitmapFromImage(getApplyImage())

def getApplyImage():
    stream = cStringIO.StringIO(getApplyData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getDownData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x83IDAT8\x8d\xb5\x92\xd1\r\x84 \x10D\xdf\xaa5l\x05\xd4y\xa2\xdfj\
\x85TA\x03\xde\x17fUH\xee\x02\xcc\x17\t\xcc\xcc\xdb\r"\xc3H\x8d\x86*70\xa5\
\xc3\xba\xf83\x84\xf0\x93IU\xd9\xf6C\x00\xc4\x8e\xb0.\xfe\x04(\x05\xa9*\xc0e\
\xbe\x11X9\xe7\xb0A\xc9\x98\x93<\x97\x98(\x92b\x8c\xb7{\xdb\x0e\r\x96\xf8\n\
\xf8\xcc^r\x0fs\xed}\x08J\x14\xb9\xf6~\x04M\x02\xec\x18%\xfc&\x04\xaf\x8f\
\xf4\xaf\xaa\t\xbe\x1el!\xed\xde\xc2%\x86\x00\x00\x00\x00IEND\xaeB`\x82' 

def getDownBitmap():
    return wx.BitmapFromImage(getDownImage())

def getDownImage():
    stream = cStringIO.StringIO(getDownData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getDelData():
    return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x00\xe2IDAT(\x91\x85\x92Ar\x830\x0cE\x9f\x19.\x95N\xbb\x08\xcb\xe6F\xa0\xb2\
pEo\x94.\xc9\x82\x8es,u\xa1D1eH\xff0\x1a\xc9\xfa_\xdf6N`\xdc\xa1y\x8a\\\xc6\
\x81\x1d\x18\x98f\xd5\xacf\xe6\xd1\x13\xcd\xea\xdd\xd5\x17T'\x01e)e){\xb2\
\x16\x18>\x06`\xfa\xbc\xedg\xbe\xcc\xdd\xb1\xbb\xfe\\\xa3\x05Sl2i\xd6\xee\
\xd8\x01\x87\xd7\x03\x90Rr\x1f_tD\xeb\xe6\xe0\xf0\x91fV\xbb\x05;VZ\x19\x07\
\xcdS\xcc;\xbd\x9f\x80\xf3\xf79\x04\xb5\x18h\xebB\xbfTz\t\x99\xf42_\xe6?w\
\xda\xd4\xec\x88[\xc8(\xfe\xc7\x1e\x02\x9f\x1d\xd1\x93-\x1a@\xc6\xa1\xb6~n\
\xd5\xd4\x85\xf4\xf2\x9c\r\xa4\xfb[J@Y\n\xd5\xd1\x01w\x8e\x03P\xdd\x92\x01/o\
\x0f\xd9\x1e\xdau\xf9\xbf,\xd5\xcf{\xd3Z\rr\xfc\x02\x1fr\x8f\xf6KRW\xed\x00\
\x00\x00\x00IEND\xaeB`\x82" 

def getDelBitmap():
    return wx.BitmapFromImage(getDelImage())

def getDelImage():
    stream = cStringIO.StringIO(getDelData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getNowData():
    return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xbdIDAT8\x8d}SMN\xdbP\x18\x9c\xf7\xcc\xbaH\xb0Eq\\\x0e\x90\xa6\xea:\
V\xd2\x0b$\xb6\xd8\xa6\xed\x01P\nb\x1b\xc5\x81=\xbd\x00\x95J\x81e\x9c\xe2F\
\xec\x92<[\xb9\x80\xd55\x8d\x02\x1c\xc0\xbe\x80\xa7\x8b\xd4&&1\xb3z?3\xf3~\
\xe6\xfb\x84\x90\x1a6\xc1\xacu\t(\x00\x80R\x0e\x84\xfc(6\xf1\xe4F\xf5\x7f\
\x1c\xd8\x07\xd8\x7f\xbb\xff\x1aei`\xd6\xba,\xebm\x02\x00\x931\xcbz\x9bR\x04\
\x18\xb8\x03\xcc\xe7s4\xea\xa7`\xd2\xcd\xf6?\x7fj\x93\xc9\x98\x00 \x00\x87#\
\xef\x03\x86\xbf\x86\x98L\x13\xd8\xad]t:\x9d\xb5\x93\xc20\xc4\xf9\xb7sD\xd1;\
X\xd6\x0e\xee\xff\xde\xe3\xc7\xe5O\x81\xe5\x1f8\xb4-\x9b\xb3`\xc6Ux\xb7\x7f\
\x18Gq6\x8f\xa3\x98'\xc7'|_=&\xa0(\xa4\x86\xcc\xe0\xe6\xea\x86$\xe98N&PJ\xe5\
\x0c\x95Z0\x8eb\xda\x96M!5d\x06\xb6e\xf3q\xf1\x98\x11S\x93U\xb3\xcc\x84\xe4\
\xc8\x1b\x11p(\xa4\x06\xc9\xa4K\xabea\xaf\xb4\x97\xbd\xb7\xd7\xeb\xa1\xdf\
\xef\x17\xfe|\xa5RA\xa3\x1e\xe4c\xf4\xfd\x87\x1c\xe9\xe8\xeb\x11\xbc[\x0f\
\xd7W\xd7y\xf5\x0b^q\x1d\xf8\n\xd5j\xb5p;\xc5V:0\xcd\xd2\x8a\xd8\xc7\x9b\xed\
m\\|\xbfXW\x98%\xe0\xe1\xe9\xf9\x06B\x9e\tw\xe8\xe2)]\xf4\xfd\xd4\xb1\xf0\
\xd40\x0c1\x99\xd6\x96\x934\xc6\xbb\xdfwYTEHcl\xd4\x1b\xf9\x18\x85\xd4`\x18_\
8\x0bf\xb9\xc2)*\xa4\xb4\x88\x84\xd4 V\xbb\xb1\xac\xb7i\xb7v\xd1l6\xa1\xebz\
\xee\xdaA\x10\xc0\x1d\xba\x18\xb8\x87\xb9\xce\x14/\xdb\x99\xc9\x98\x86q\t\
\xa3\xbc\xc8\xadO\xa65\x08y\xb6\xd6\xd2\xff\x00?\xd0J\xf0\xa9e\xb0\xa6\x00\
\x00\x00\x00IEND\xaeB`\x82" 

def getNowBitmap():
    return wx.BitmapFromImage(getNowImage())

def getNowImage():
    stream = cStringIO.StringIO(getNowData())
    return wx.ImageFromStream(stream)

# defined event for vtInputTextML item changed
wxEVT_VTTIME_TIMEEDIT_CHANGED=wx.NewEventType()
vEVT_VTTIME_TIMEEDIT_CHANGED=wx.PyEventBinder(wxEVT_VTTIME_TIMEEDIT_CHANGED,1)
def EVT_VTTIME_TIMEEDIT_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTTIME_TIMEEDIT_CHANGED,func)
class vtTimeTimeEditChanged(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTTIME_TIMEEDIT_CHANGED(<widget_name>, self.OnChanged)
    """

    def __init__(self,obj,val):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTTIME_TIMEEDIT_CHANGED)
        self.val=val
    def GetValue(self):
        return self.val
    
[wxID_WXTIMEEDIT, wxID_WXTIMEEDITwin, wxID_WXTIMEEDITTGHR00, 
 wxID_WXTIMEEDITTGHR01, wxID_WXTIMEEDITTGHR02, 
 wxID_WXTIMEEDITTGHR03, wxID_WXTIMEEDITTGHR04, 
 wxID_WXTIMEEDITTGHR05, wxID_WXTIMEEDITTGHR06, 
 wxID_WXTIMEEDITTGHR07, wxID_WXTIMEEDITTGHR08, 
 wxID_WXTIMEEDITTGHR09, wxID_WXTIMEEDITTGHR10, 
 wxID_WXTIMEEDITTGHR11, wxID_WXTIMEEDITTGHR12, 
 wxID_WXTIMEEDITTGHR13, wxID_WXTIMEEDITTGHR14, 
 wxID_WXTIMEEDITTGHR15, wxID_WXTIMEEDITTGHR16, 
 wxID_WXTIMEEDITTGHR17, wxID_WXTIMEEDITTGHR18, 
 wxID_WXTIMEEDITTGHR19, wxID_WXTIMEEDITTGHR20, 
 wxID_WXTIMEEDITTGHR21, wxID_WXTIMEEDITTGHR22, 
 wxID_WXTIMEEDITTGHR23, wxID_WXTIMEEDITTGMIN00, 
 wxID_WXTIMEEDITTGMIN05, wxID_WXTIMEEDITTGMIN10, 
 wxID_WXTIMEEDITTGMIN15, wxID_WXTIMEEDITTGMIN20, 
 wxID_WXTIMEEDITTGMIN25, wxID_WXTIMEEDITTGMIN30, 
 wxID_WXTIMEEDITTGMIN35, wxID_WXTIMEEDITTGMIN40, 
 wxID_WXTIMEEDITTGMIN45, wxID_WXTIMEEDITTGMIN50, 
 wxID_WXTIMEEDITTGMIN55, wxID_WXTIMEEDITGCBBCANCEL, 
 wxID_WXTIMEEDITGCBBDEL, wxID_WXTIMEEDITGCBBNOW, 
] = map(lambda _init_ctrls: wx.NewId(), range(41))


class vtTimeTimeEditTransientPopup(wx.Dialog):
    #SIZE_PN_SCROLL=30
    def __init__(self, parent, size,style,name=''):
        self.SIZE_PN_SCROLL=wx.SystemSettings.GetMetric(wx.SYS_VSCROLL_X)
        wx.Dialog.__init__(self, parent,style=wx.RESIZE_BORDER)#wx.BORDER_SIMPLE)#|wx.STAY_ON_TOP)
        id=wx.NewId()
        wx.EVT_SIZE(self,self.OnSize)
        st = wx.StaticText(self, -1,name,pos=(70,4))
        self.cbCancel = wx.BitmapButton(id=-1,
              bitmap=getCancelBitmap(), name=u'cbCancel',
              parent=self, pos=(0,0), size=(30,30), style=wx.BU_AUTODRAW)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              self.cbCancel)
              
        self.cbApply = wx.BitmapButton(id=-1,
              bitmap=getApplyBitmap(), name=u'cbApply',
              parent=self, pos=wx.Point(32, 00), size=wx.Size(30, 30),
              style=wx.BU_AUTODRAW)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              self.cbApply)
        if size[1]<40:
            size=(size[0],40)
        i=0
        iY=40
        iH=size[1]+4
        iSzHeight=0
        #sVal=parent.GetValue()
        #self.dt=wx.DateTime.Now()
        #self.dt.ParseFormat(sVal,'%H%M')
        self.zTime=vtTime()
        self.zTime.Now()
        
        self.tgHr00 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR00, label=u'0',
              name=u'tgHr00', parent=self, pos=wx.Point(0, 40),
              size=wx.Size(30, 20), style=0)
        self.tgHr00.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr00, wxID_WXTIMEEDITTGHR00,
              self.OnTgHrTogglebutton)

        self.tgHr01 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR01, label=u'1',
              name=u'tgHr01', parent=self, pos=(30, 40),
              size=(30, 20), style=0)
        self.tgHr01.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr01, wxID_WXTIMEEDITTGHR01,
              self.OnTgHrTogglebutton)

        self.tgHr02 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR02, label=u'2',
              name=u'tgHr02', parent=self, pos=(60, 40),
              size=(30, 20), style=0)
        self.tgHr02.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr02, wxID_WXTIMEEDITTGHR02,
              self.OnTgHrTogglebutton)

        self.tgHr03 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR03, label=u'3',
              name=u'tgHr03', parent=self, pos=(90, 40),
              size=(30, 20), style=0)
        self.tgHr03.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr03, wxID_WXTIMEEDITTGHR03,
              self.OnTgHrTogglebutton)

        self.tgHr04 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR04, label=u'4',
              name=u'tgHr04', parent=self, pos=(120, 40),
              size=(30, 20), style=0)
        self.tgHr04.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr04, wxID_WXTIMEEDITTGHR04,
              self.OnTgHrTogglebutton)

        self.tgHr05 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR05, label=u'5',
              name=u'tgHr05', parent=self, pos=(150, 40),
              size=(30, 20), style=0)
        self.tgHr05.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr05, wxID_WXTIMEEDITTGHR05,
              self.OnTgHrTogglebutton)

        self.tgHr06 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR06, label=u'6',
              name=u'tgHr06', parent=self, pos=(180, 40),
              size=(30, 20), style=0)
        self.tgHr06.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr06, wxID_WXTIMEEDITTGHR06,
              self.OnTgHrTogglebutton)

        self.tgHr07 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR07, label=u'7',
              name=u'tgHr07', parent=self, pos=(210, 40),
              size=(30, 20), style=0)
        self.tgHr07.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr07, wxID_WXTIMEEDITTGHR07,
              self.OnTgHrTogglebutton)

        self.tgHr08 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR08, label=u'8',
              name=u'tgHr08', parent=self, pos=(240, 40),
              size=(30, 20), style=0)
        self.tgHr08.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr08, wxID_WXTIMEEDITTGHR08,
              self.OnTgHrTogglebutton)

        self.tgHr09 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR09, label=u'9',
              name=u'tgHr09', parent=self, pos=(270, 40),
              size=(30, 20), style=0)
        self.tgHr09.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr09, wxID_WXTIMEEDITTGHR09,
              self.OnTgHrTogglebutton)

        self.tgHr10 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR10, label=u'10',
              name=u'tgHr10', parent=self, pos=(300, 40),
              size=(30, 20), style=0)
        self.tgHr10.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr10, wxID_WXTIMEEDITTGHR10,
              self.OnTgHrTogglebutton)

        self.tgHr11 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR11, label=u'11',
              name=u'tgHr11', parent=self, pos=(330, 40),
              size=(30, 20), style=0)
        self.tgHr11.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr11, wxID_WXTIMEEDITTGHR11,
              self.OnTgHrTogglebutton)

        self.tgHr12 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR12, label=u'12',
              name=u'tgHr12', parent=self, pos=(0, 60),
              size=(30, 20), style=0)
        self.tgHr12.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr12, wxID_WXTIMEEDITTGHR12,
              self.OnTgHrTogglebutton)

        self.tgHr13 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR13, label=u'13',
              name=u'tgHr13', parent=self, pos=(30, 60),
              size=(30, 20), style=0)
        self.tgHr13.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr13, wxID_WXTIMEEDITTGHR13,
              self.OnTgHrTogglebutton)

        self.tgHr14 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR14, label=u'14',
              name=u'tgHr14', parent=self, pos=(60, 60),
              size=(30, 20), style=0)
        self.tgHr14.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr14, wxID_WXTIMEEDITTGHR14,
              self.OnTgHrTogglebutton)

        self.tgHr15 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR15, label=u'15',
              name=u'tgHr15', parent=self, pos=(90, 60),
              size=(30, 20), style=0)
        self.tgHr15.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr15, wxID_WXTIMEEDITTGHR15,
              self.OnTgHrTogglebutton)

        self.tgHr16 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR16, label=u'16',
              name=u'tgHr16', parent=self, pos=(120, 60),
              size=(30, 20), style=0)
        self.tgHr16.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr16, wxID_WXTIMEEDITTGHR16,
              self.OnTgHrTogglebutton)

        self.tgHr17 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR17, label=u'17',
              name=u'tgHr17', parent=self, pos=(150, 60),
              size=(30, 20), style=0)
        self.tgHr17.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr17, wxID_WXTIMEEDITTGHR17,
              self.OnTgHrTogglebutton)

        self.tgHr18 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR18, label=u'18',
              name=u'tgHr18', parent=self, pos=(180, 60),
              size=(30, 20), style=0)
        self.tgHr18.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr18, wxID_WXTIMEEDITTGHR18,
              self.OnTgHrTogglebutton)

        self.tgHr19 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR19, label=u'19',
              name=u'tgHr19', parent=self, pos=(210, 60),
              size=(30, 20), style=0)
        self.tgHr19.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr19, wxID_WXTIMEEDITTGHR19,
              self.OnTgHrTogglebutton)

        self.tgHr20 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR20, label=u'20',
              name=u'tgHr20', parent=self, pos=(240, 60),
              size=(30, 20), style=0)
        self.tgHr20.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr20, wxID_WXTIMEEDITTGHR20,
              self.OnTgHrTogglebutton)

        self.tgHr21 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR21, label=u'21',
              name=u'tgHr21', parent=self, pos=(270, 60),
              size=(30, 20), style=0)
        self.tgHr21.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr21, wxID_WXTIMEEDITTGHR21,
              self.OnTgHrTogglebutton)

        self.tgHr22 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR22, label=u'22',
              name=u'tgHr22', parent=self, pos=(300, 60),
              size=(30, 20), style=0)
        self.tgHr22.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr22, wxID_WXTIMEEDITTGHR22,
              self.OnTgHrTogglebutton)

        self.tgHr23 = wx.ToggleButton(id=wxID_WXTIMEEDITTGHR23, label=u'23',
              name=u'tgHr23', parent=self, pos=(330, 60),
              size=(30, 20), style=0)
        self.tgHr23.SetValue(False)
        wx.EVT_TOGGLEBUTTON(self.tgHr23, wxID_WXTIMEEDITTGHR23,
              self.OnTgHrTogglebutton)

        self.tgMin00 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN00,
              label=u':00', name=u'tgMin00', parent=self, pos=(0,
              80), size=(60, 20), style=0)
        self.tgMin00.SetValue(False)
        self.tgMin00.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin00, wxID_WXTIMEEDITTGMIN00,
              self.OnTgMinTogglebutton)

        self.tgMin05 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN05,
              label=u':05', name=u'tgMin05', parent=self, pos=(60,
              80), size=(60, 20), style=0)
        self.tgMin05.SetValue(False)
        self.tgMin05.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin05, wxID_WXTIMEEDITTGMIN05,
              self.OnTgMinTogglebutton)

        self.tgMin10 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN10,
              label=u':10', name=u'tgMin10', parent=self,
              pos=(120, 80), size=(60, 20), style=0)
        self.tgMin10.SetValue(False)
        self.tgMin10.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin10, wxID_WXTIMEEDITTGMIN10,
              self.OnTgMinTogglebutton)

        self.tgMin15 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN15,
              label=u':15', name=u'tgMin15', parent=self,
              pos=(180, 80), size=(60, 20), style=0)
        self.tgMin15.SetValue(False)
        self.tgMin15.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin15, wxID_WXTIMEEDITTGMIN15,
              self.OnTgMinTogglebutton)

        self.tgMin20 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN20,
              label=u':20', name=u'tgMin20', parent=self,
              pos=(240, 80), size=(60, 20), style=0)
        self.tgMin20.SetValue(False)
        self.tgMin20.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin20, wxID_WXTIMEEDITTGMIN20,
              self.OnTgMinTogglebutton)

        self.tgMin25 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN25,
              label=u':25', name=u'tgMin25', parent=self,
              pos=(300, 80), size=(60, 20), style=0)
        self.tgMin25.SetValue(False)
        self.tgMin25.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin25, wxID_WXTIMEEDITTGMIN25,
              self.OnTgMinTogglebutton)

        self.tgMin30 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN30,
              label=u':30', name=u'tgMin30', parent=self, pos=(0,
              100), size=(60, 20), style=0)
        self.tgMin30.SetValue(False)
        self.tgMin30.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin30, wxID_WXTIMEEDITTGMIN30,
              self.OnTgMinTogglebutton)

        self.tgMin35 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN35,
              label=u':35', name=u'tgMin35', parent=self, pos=(60,
              100), size=(60, 20), style=0)
        self.tgMin35.SetValue(False)
        self.tgMin35.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin35, wxID_WXTIMEEDITTGMIN35,
              self.OnTgMinTogglebutton)

        self.tgMin40 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN40,
              label=u':40', name=u'tgMin40', parent=self,
              pos=(120, 100), size=(60, 20), style=0)
        self.tgMin40.SetValue(False)
        self.tgMin40.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin40, wxID_WXTIMEEDITTGMIN40,
              self.OnTgMinTogglebutton)

        self.tgMin45 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN45,
              label=u':45', name=u'tgMin45', parent=self,
              pos=(180, 100), size=(60, 20), style=0)
        self.tgMin45.SetValue(False)
        self.tgMin45.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin45, wxID_WXTIMEEDITTGMIN45,
              self.OnTgMinTogglebutton)

        self.tgMin50 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN50,
              label=u':50', name=u'tgMin50', parent=self,
              pos=(240, 100), size=(60, 20), style=0)
        self.tgMin50.SetValue(False)
        self.tgMin50.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin50, wxID_WXTIMEEDITTGMIN50,
              self.OnTgMinTogglebutton)

        self.tgMin55 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN55,
              label=u':55', name=u'tgMin55', parent=self,
              pos=(300, 100), size=(60, 20), style=0)
        self.tgMin55.SetValue(False)
        self.tgMin55.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgMin55, wxID_WXTIMEEDITTGMIN55,
              self.OnTgMinTogglebutton)

        
        self.tgHr24 = wx.ToggleButton(id=wxID_WXTIMEEDITTGMIN55,
              label=u'24:00', name=u'tgHr24', parent=self,
              pos=(300, 120), size=(60, 20), style=0)
        self.tgHr24.SetValue(False)
        self.tgHr24.SetBackgroundColour(wx.Colour(128, 128, 255))
        wx.EVT_TOGGLEBUTTON(self.tgHr24, wxID_WXTIMEEDITTGMIN55,
              self.OnTgHr24Togglebutton)
        
        img=getDelBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.gcbbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_WXTIMEEDITGCBBDEL,
              bitmap=img, label=u'Del', name=u'gcbbDel',
              parent=self, pos=wx.Point(288, 10), size=wx.Size(72, 30),
              style=0)
        self.gcbbDel.Bind(wx.EVT_BUTTON, self.OnGcbbDelButton,
              id=wxID_WXTIMEEDITGCBBDEL)
        
        img=getNowBitmap()
        #mask=wx.Mask(img,wx.BLUE)
        #img.SetMask(mask)
        self.cbNow = wx.lib.buttons.GenBitmapTextButton(ID=wxID_WXTIMEEDITGCBBNOW,
              bitmap=img, label=u'Now', name=u'cbNow',
              parent=self, pos=wx.Point(216, 10), size=wx.Size(72, 30),
              style=0)
        self.cbNow.Bind(wx.EVT_BUTTON, self.OnCbNowButton,
              id=wxID_WXTIMEEDITGCBBNOW)
        
        #self.calCalendar.Bind(wx.calendar.EVT_CALENDAR_SEL_CHANGED,
        #      self.OnCalCalendarCalendarSelChanged,
        #      id=wxID_VCALENDARPANELCALCALENDAR)
        self.tgActHr=None
        self.tgActMin=None
        self.tgPrevHr=None
        self.tgPrevMin=None
        self.objTimeCtrl=None

        self.szOrig=(368,148)
        self.SetSize(self.szOrig)
        
    def OnSize(self,evt):
        iW,iH=self.GetSize()
        if iW<self.szOrig[0]:
            iW=self.szOrig[0]
        #if iH<self.szMin[1]:
        #    iH=self.szMin[1]
        if iH<self.szOrig[1]:
            iH=self.szOrig[1]
        self.SetSize((iW,iH))
        #if self.calCalendar is not None:
        #    self.calCalendar.SetSize((iW-16,iH-52))
        evt.Skip()
    def OnCbCancelButton(self,evt):
        self.Show(False)
    def OnCbApplyButton(self,evt):
        #self.Apply()
        try:
            par=self.GetParent()
            #self.GetValue()
            #sVal=dt.Format('%H%M')
            sVal=self.zTime.GetStr(':')
            par.__apply__(sVal)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        self.Show(False)
    def OnGcbbDelButton(self,event):
        if self.tgActHr is not None:
            self.tgActHr.SetValue(False)
        if self.tgActMin is not None:
            self.tgActMin.SetValue(False)
        self.tgActHr=None
        self.tgActMin=None
        self.tgPrevHr=None
        self.tgPrevMin=None
        
        self.zTime.Clear()
        #s='--:--:--'
        #self.SetValue(s)
        #wx.PostEvent(self,vtTimeTimeEditChanged(self,s))
        #self.PopDown()
        self.OnCbApplyButton(None)
    def OnCbNowButton(self,event):
        if self.tgActHr is not None:
            self.tgActHr.SetValue(False)
        if self.tgActMin is not None:
            self.tgActMin.SetValue(False)
        self.tgActHr=None
        self.tgActMin=None
        self.tgPrevHr=None
        self.tgPrevMin=None
        self.zTime.Now()
        self.zTime.Round()
        self.OnCbApplyButton(None)
    def OnTgHrTogglebutton(self, event):
        obj=event.GetEventObject()
        if obj.GetValue()==False:
            obj.SetValue(True)
        else:
            if self.tgHr24.GetValue()==True:
                self.tgHr24.SetValue(False)
            if self.tgActHr!=None:
                self.tgActHr.SetValue(False)
            self.tgActHr=obj
        self.GetValue()

    def OnTgMinTogglebutton(self, event):
        obj=event.GetEventObject()
        if obj.GetValue()==False:
            obj.SetValue(True)
        else:
            if self.tgHr24.GetValue()==True:
                obj.SetValue(False)
                return
            if self.tgActMin!=None:
                self.tgActMin.SetValue(False)
            self.tgActMin=obj
        self.tgPrevHr=self.tgActHr
        self.tgPrevMin=self.tgActMin
        self.GetValue()
        #dt=self.GetValue()
        #s=self.dt.FormatTime()
        #s=self.dt.Format('%H:%M')
        #self.SetValue(s)
        #wx.PostEvent(self,vTimeTimeEditChanged(self,s))
        #self.PopDown()
        self.OnCbApplyButton(None)
    def OnTgHr24Togglebutton(self,event):
        obj=event.GetEventObject()
        if obj.GetValue()==False:
            obj.SetValue(True)
        else:
            if self.tgActHr!=None:
                self.tgActHr.SetValue(False)
            self.tgActHr=None
            if self.tgActMin!=None:
                self.tgActMin.SetValue(False)
            self.tgActMin=None
        self.tgPrevHr=self.tgActHr
        self.tgPrevMin=self.tgActMin
        #dt=self.GetValue()
        #s=self.dt.FormatTime()
        #s='24:00:00'
        
        self.zTime.Set(24,0,0)
        self.OnCbApplyButton(None)
        #self.SetValue(s)
        #wx.PostEvent(self,vTimeTimeEditChanged(self,s))
        #self.PopDown()
    def Show(self,flag):
        try:
            par=self.GetParent()
            if flag:
                sVal=par.GetValue()
                self.SetVal(sVal)
                #self.calCalendar.Refresh()
                #self.calCalendar.Update()
            else:
                #par.cbPopup.SetValue(False)
                par.__setPopupState__(False)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        wx.Dialog.Show(self,flag)
    def GetValue(self):
        min=0
        hr=0
        if self.tgActMin is not None:
            if self.tgActMin == self.tgMin00:
                min=0
            elif self.tgActMin==self.tgMin05:
                min=5
            elif self.tgActMin == self.tgMin10:
                min=10
            elif self.tgActMin == self.tgMin15:
                min=15
            elif self.tgActMin == self.tgMin20:
                min=20
            elif self.tgActMin == self.tgMin25:
                min=25
            elif self.tgActMin == self.tgMin30:
                min=30
            elif self.tgActMin == self.tgMin35:
                min=35
            elif self.tgActMin == self.tgMin40:
                min=40
            elif self.tgActMin == self.tgMin45:
                min=45
            elif self.tgActMin == self.tgMin50:
                min=50
            elif self.tgActMin == self.tgMin55:
                min=55
        if self.tgActHr is not None:
            if self.tgActHr==self.tgHr00:
                hr=0
            elif self.tgActHr==self.tgHr01:
                hr=1
            elif self.tgActHr==self.tgHr02:
                hr=2
            elif self.tgActHr==self.tgHr03:
                hr=3
            elif self.tgActHr==self.tgHr04:
                hr=4
            elif self.tgActHr==self.tgHr05:
                hr=5
            elif self.tgActHr==self.tgHr06:
                hr=6
            elif self.tgActHr==self.tgHr07:
                hr=7
            elif self.tgActHr==self.tgHr08:
                hr=8
            elif self.tgActHr==self.tgHr09:
                hr=9
            elif self.tgActHr==self.tgHr10:
                hr=10
            elif self.tgActHr==self.tgHr11:
                hr=11
            elif self.tgActHr==self.tgHr12:
                hr=12
            elif self.tgActHr==self.tgHr13:
                hr=13
            elif self.tgActHr==self.tgHr14:
                hr=14
            elif self.tgActHr==self.tgHr15:
                hr=15
            elif self.tgActHr==self.tgHr16:
                hr=16
            elif self.tgActHr==self.tgHr17:
                hr=17
            elif self.tgActHr==self.tgHr18:
                hr=18
            elif self.tgActHr==self.tgHr19:
                hr=19
            elif self.tgActHr==self.tgHr20:
                hr=20
            elif self.tgActHr==self.tgHr21:
                hr=21
            elif self.tgActHr==self.tgHr22:
                hr=22
            elif self.tgActHr==self.tgHr23:
                hr=23
        if self.tgHr24.GetValue()==True:
            self.zTime.Set24Hr()
            hr=23
            min=59
        else:
            self.zTime.Set(hr,min)
        #self.dt=wx.DateTime()
        #self.dt.Set(10)
        #self.dt.SetHour(hr)
        #self.dt.SetMinute(min)
        #if self.tgHr24.GetValue()==True:
        #    self.dt.SetSecond(59)
        #return self.dt
    def __setTime(self,hr,min):
        try:
            #vtLog.CallStack('hr:%02d mn:%02d'%(hr,min))
            if self.tgActMin is not None:
                self.tgActMin.SetValue(False)
            if self.tgActHr is not None:
                self.tgActHr.SetValue(False)
            self.tgActHr=None
            self.tgActMin=None
            self.tgHr24.SetValue(False)
            if hr==24 and min==0:
                self.tgHr24.SetValue(True)
                self.tgActMin=None
                self.tgActHr=None
                self.tgPrevHr=self.tgActHr
                self.tgPrevMin=self.tgActMin
                #self.SetValue('24:00:00')
                return
            #min=round(min/5.0)*5
            
            if min==0:
                self.tgActMin=self.tgMin00
            elif min==5:
                self.tgActMin=self.tgMin05
            elif min==10:
                self.tgActMin=self.tgMin10
            elif min==15:
                self.tgActMin=self.tgMin15
            elif min==20:
                self.tgActMin=self.tgMin20
            elif min==25:
                self.tgActMin=self.tgMin25
            elif min==30:
                self.tgActMin=self.tgMin30
            elif min==35:
                self.tgActMin=self.tgMin35
            elif min==40:
                self.tgActMin=self.tgMin40
            elif min==45:
                self.tgActMin=self.tgMin45
            elif min==50:
                self.tgActMin=self.tgMin50
            elif min==55:
                self.tgActMin=self.tgMin55
            if hr==0:
                self.tgActHr=self.tgHr00
            elif hr==1:
                self.tgActHr=self.tgHr01
            elif hr==2:
                self.tgActHr=self.tgHr02
            elif hr==3:
                self.tgActHr=self.tgHr03
            elif hr==4:
                self.tgActHr=self.tgHr04
            elif hr==5:
                self.tgActHr=self.tgHr05
            elif hr==6:
                self.tgActHr=self.tgHr06
            elif hr==7:
                self.tgActHr=self.tgHr07
            elif hr==8:
                self.tgActHr=self.tgHr08
            elif hr==9:
                self.tgActHr=self.tgHr09
            elif hr==10:
                self.tgActHr=self.tgHr10
            elif hr==11:
                self.tgActHr=self.tgHr11
            elif hr==12:
                self.tgActHr=self.tgHr12
            elif hr==13:
                self.tgActHr=self.tgHr13
            elif hr==14:
                self.tgActHr=self.tgHr14
            elif hr==15:
                self.tgActHr=self.tgHr15
            elif hr==16:
                self.tgActHr=self.tgHr16
            elif hr==17:
                self.tgActHr=self.tgHr17
            elif hr==18:
                self.tgActHr=self.tgHr18
            elif hr==19:
                self.tgActHr=self.tgHr19
            elif hr==20:
                self.tgActHr=self.tgHr20
            elif hr==21:
                self.tgActHr=self.tgHr21
            elif hr==22:
                self.tgActHr=self.tgHr22
            elif hr==23:
                self.tgActHr=self.tgHr23
            try:
                self.tgActMin.SetValue(True)
                self.tgActHr.SetValue(True)
            except:
                pass
            self.tgPrevHr=self.tgActHr
            self.tgPrevMin=self.tgActMin
            #dt=self.GetValue()
            #s=self.dt.FormatTime()
            #s=self.dt.Format('%H:%M')
            #self.SetValue(s)
        
        except:
            vtLog.vtLngTB(self.GetName())
    def GetVal(self):
        try:
            #dt=self.calCalendar.GetDate()
            #sVal=dt.Format('%H%M')
            sVal=self.zTime.GetStr(':')
            return sVal
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
            return ''
    def SetVal(self,sVal):
        try:
            par=self.GetParent()
            
            sVal=par.GetValueStr()
            self.zTime.SetStr(sVal,':')
            self.zTime.Round(5)
            #strs=sVal.split(':')
            #nums=map(int,strs)
            #try:
            #    dt.ParseFormat(sVal,'%H:%M')
            #except:
            #    pass
            self.__setTime(self.zTime.GetHour(),self.zTime.GetMinute())
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
class vtTimeTimeEdit(wx.Panel,vtXmlDomConsumer,vtXmlDomConsumerLang):
    def __init__(self,*_args,**_kwargs):
        global _
        _=vtLgBase.assignPluginLang('vtTime')
        vtXmlDomConsumer.__init__(self)
        vtXmlDomConsumerLang.__init__(self)
        try:
            sz=_kwargs['size']
        except:
            sz=wx.Size(84,30)
            _kwargs['size']=sz
        try:
            szCb=_kwargs['size_button']
            del _kwargs['size_button']
        except:
            szCb=wx.Size(24,24)
        try:
            szTxt=_kwargs['size_text']
            del _kwargs['size_text']
        except:
            szTxt=wx.Size(sz[0]-szCb[0]-2,sz[1]-4)
        
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        self.SetAutoLayout(True)
        
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        size=(szTxt[0],szTxt[1])
        pos=(0,(sz[1]-szTxt[1])/2)
        self.txtVal = wx.TextCtrl(id=-1, name='txtVal',
              parent=self, pos=pos, size=size,
              style=0, value='')
        self.txtVal.SetMaxLength(9)
        self.txtVal.SetToolTipString('HH:MM:SS')
        #self.txtVal.SetConstraints(LayoutAnchors(self.txtVal, True, True,
        #      True, False))
        bxs.AddWindow(self.txtVal, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        self.txtVal.Bind(wx.EVT_TEXT, self.OnTextText,self.txtVal)
        self.txtVal.Bind(wx.EVT_TEXT_ENTER, self.OnTextEnter,self.txtVal)
        size=(szCb[0],szCb[1])
        pos=(szTxt[0]+(sz[0]-(szTxt[0]+szCb[0]))/2,(sz[1]-szCb[1])/2)
        id=wx.NewId()
        self.cbPopup = wx.lib.buttons.GenBitmapToggleButton(ID=id,
              bitmap=wx.EmptyBitmap(8, 8), name=u'cbPopup',
              parent=self, pos=pos, size=size, style=wx.BU_AUTODRAW)
        #self.cbPopup.SetConstraints(LayoutAnchors(self.cbPopup, False, True,
        #      True, False))
        bxs.AddWindow(self.cbPopup, 0, border=0, flag=wx.EXPAND)
        self.SetSizer(bxs)
        self.cbPopup.SetBitmapLabel(getDownBitmap())
        self.cbPopup.SetBitmapSelected(getDownBitmap())
        self.cbPopup.Bind(wx.EVT_BUTTON,self.OnPopupButton,self.cbPopup)#id=id)
        self.popWin=None
        self.tagName='time'
        self.bBlock=True
        self.zTime=vtTime()
        self.zTime.Now()
        #self.dt=wx.DateTime().Today()
        
        #self.dftBkgColor=self.txtVal.GetBackgroundColour()
        self.bEnableMark=True
        self.bBusy=False
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
    def Enable(self,flag):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        wx.Panel.Enable(self,flag)
        self.cbPopup.Refresh()
    def SetEnableMark(self,flag):
        self.bEnableMark=flag
    def __markModified__(self,flag=True):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.bEnableMark==False:
            return
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'flag:%s'%(flag),self)
        if flag:
            f=self.txtVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_BOLD)
            self.txtVal.SetFont(f)
        else:
            f=self.txtVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
            self.txtVal.SetFont(f)
        self.txtVal.Refresh()
    def __markFlt__(self,flag=False):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'flag:%s'%(flag),self)
        if flag:
            color=wx.TheColourDatabase.Find('YELLOW')
            self.txtVal.SetBackgroundColour(color)
        else:
            color=wx.SystemSettings_GetColour(wx.SYS_COLOUR_WINDOW)
            self.txtVal.SetBackgroundColour(color)
    def SetTagNames(self,tagName):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'SetTagNames;tagname:%s,%s'%(tagName,tagNameInt),
        #                origin=self.GetName())
        self.tagName=tagName
    def ClearLang(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'ClearLang',
        #                origin=self.GetName())
        #if VERBOSE:
        #    vtLog.CallStack('')
        self.txtVal.SetValue('')
    def UpdateLang(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'UpdateLang',
        #                origin=self.GetName())
        return
        if VERBOSE:
            vtLog.CallStack('')
        try:
            if self.popWin is None:
                sVal=self.doc.getNodeText(self.node,self.tagName)
            else:
                sVal=self.popWin.GetVal(self.doc.GetLang())
            self.bBlock=True
            self.txtVal.SetValue(string.split(sVal,'\n')[0])
            wx.CallAfter(self.__clearBlock__)
            #self.txtVal.SetValue(sVal)
        except:
            vtLog.vtLngTB(self.GetName())

    def Clear(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'Clear',
        #                origin=self.GetName())
        if VERBOSE:
            vtLog.CallStack('')
        self.ClearLang()
        self.node=None
        self.__markModified__(False)
    def __setPopupState__(self,flag):
        self.bBusy=flag
        self.cbPopup.SetValue(flag)
    def IsBusy(self):
        return self.bBusy
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'IsBusy',
        #                origin=self.GetName())
        if self.popWin is not None:
            return self.popWin.IsShown()
        else:
            return False
    def Close(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.__Close__()
    def __Close__(self):
        if self.popWin is not None:
            self.popWin.Show(False)
            self.__setPopupState__(False)
    def Stop(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'Stop',self)
        wx.CallAfter(self.__Close__)
    def SetDoc(self,doc):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'SetDoc',
        #                origin=self.GetName())
        vtXmlDomConsumer.SetDoc(self,doc)
        vtXmlDomConsumerLang.SetDoc(self,doc)
    def SetNode(self,node):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'SetNode;node:%s'%(node),
        #                origin=self.GetName())
        if VERBOSE:
            vtLog.CallStack('')
            print node
        self.Clear()
        self.node=node
        if self.doc is None:
            return
        if self.node is None:
            return
        try:
            sVal=string.split(self.doc.getNodeText(self.node,self.tagName),'\n')[0]
            #sVal=sVal[:4]
            bOk,bMod,sVal=self.__validate__(sVal,True)
            self.bBlock=True
            self.__markModified__(bMod)
            self.__markFlt__(not bOk)
            self.txtVal.SetValue(sVal)
            wx.CallAfter(self.__clearBlock__)
        except:
            pass
            #vtLog.vtLngTB(self.GetName())
        #self.__getSelNode__()
        self.UpdateLang()
    def __validate__(self,sVal,bSet2NowOnFlt=False,bTxt=False):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'val:%s;curVal:%s'%(sVal,self.txtVal.GetValue()),self)
        if VERBOSE:
            vtLog.CallStack(sVal)
            print bSet2NowOnFlt
            print bTxt
        bMod=False
        bOk=False
        try:
            if bTxt:
                #ret=self.dt.ParseFormat(sVal,'%H:%M')
                try:
                    if self.zTime.SetStr(sVal,':')>=0:
                        bOk=True
                    ret=8
                except:
                    self.zTime.Now()
                    ret=-1
                iLen=8
            else:
                #ret=self.dt.ParseFormat(sVal,'%H%M')
                try:
                    if self.zTime.SetStr(sVal)>=0:
                        bOk=True
                    ret=6
                except:
                    self.zTime.Now()
                    ret=-1
                iLen=6
            self.zTime.Round(5)
            if VERBOSE:
                print iLen,len(sVal)
            if iLen!=len(sVal):
                ret=-1
                #sVal=sVal[:iLen]
                #bMod=True
            else:
                if 1==0:
                    if iHr<0:
                        iHr=0
                        bMod=True
                    elif iHr>23:
                        if iHr==24 and iMin>0:
                            iMin=0
                            bMod=True
                    if iMin<0:
                        iMin=0
                        bMod=True
                    elif iMin>59:
                        iMin=59
                        bMod=True
            if ret!=iLen:
                if bSet2NowOnFlt:
                    bMod=True
            #else:
            #    bOk=True
        except:
            vtLog.vtLngTB(self.GetName())
            vtLog.vtLngCurWX(vtLog.WARN,sVal,self)
            if bSet2NowOnFlt:
                bMod=True
        if bMod:
            if bSet2NowOnFlt:
                self.zTime.Now()
                #z=time.time()
                #tm=time.localtime(z)
                #iHr=tm[3]
                #iMin=tm[4]
                #self.dt=self.dt.Now()
        #sVal=self.dt.Format('%H:%M')
        #sVal='%02d:%02d'%(iHr,iMin)
        #vtLog.CallStack('')
        if bTxt:
            sValNew=self.zTime.GetStr(':')
        else:
            sValNew=self.zTime.GetStr('')
        if VERBOSE:
            print sVal,sValNew
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'val:%s;newVal:%s'%(sVal,sValNew),self)
        if sValNew!=sVal:
            bMod=True
        sValNew=self.zTime.GetStr(':')
        return bOk,bMod,sValNew
    def __getDateTime__(self):
        return self.dt
    def __clearBlock__(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'__clearBlock__',
        #                origin=self.GetName())
        self.bBlock=False
    def GetNode(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'GetNode',
        #                origin=self.GetName())
        if VERBOSE:
            vtLog.CallStack('')
        try:
            #val=self.txtVal.GetValue()#self.dt.Format('%H%M00')
            #sVal=val[:2]+val[3:]+'00'
            sVal=self.zTime.GetStr()
            self.doc.setNodeText(self.node,self.tagName,sVal)
            #self.__setNode__()

            self.__markModified__(False)
            self.doc.AlignNode(self.node)
        except:
            pass
            #vtLog.vtLngTB(self.GetName())
            
    def __getDoc__(self):
        return self.doc
    def __getNode__(self):
        return self.node
    def __apply__(self,sVal):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCS(vtLog.DEBUG,'__apply__;val:%s'%(sVal),
                        origin=self.GetName())
        if VERBOSE:
            vtLog.CallStack(sVal)
        try:
            #sVal=self.popWin.GetVal()
            #if self.trSetNode is not None:
            #    sVal=self.trSetNode(self.docTreeTup[0],node)
            #else:
            #    sVal=''
            bOk,bMod,sVal=self.__validate__(sVal,bTxt=True)
            self.__markFlt__(not bOk)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'val:%s;curVal:%s;block:%d'%(sVal,self.txtVal.GetValue(),self.bBlock),self)
            if bMod or self.txtVal.GetValue()!=sVal:
                if self.bBlock==False:
                    self.__markModified__(True)
                    wx.PostEvent(self,vtTimeTimeEditChanged(self,sVal))
        except:
            sVal=_(u'fault')
            vtLog.vtLngTB(self.GetName())
        self.bBlock=True
        self.txtVal.SetValue(string.split(sVal,'\n')[0])
        wx.CallAfter(self.__clearBlock__)
        return sVal
        
    def __createPopup__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCS(vtLog.DEBUG,'__createPopup__',
                        origin=self.GetName())
        if self.popWin is None:
            sz=self.GetSize()
            sz=(sz[0],sz[1]-20)
            try:
                self.popWin=vtTimeTimeEditTransientPopup(self,sz,wx.SIMPLE_BORDER)
            except:
                vtLog.vtLngTB(self.GetName())
            #self.popWin.SetVal(self.txtVal.GetValue(),self.doc.GetLang())
        else:
            pass
    def SetValueStr(self,sVal):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        try:
            sVal=sVal[0:2]+':'+sVal[2:4]+':'+sVal[4:6]
        except:
            pass
        self.bBlock=True
        self.txtVal.SetValue(sVal)
        self.__apply__(sVal)
    def SetValue(self,sVal):
        self.__apply__(sVal)
    def SetTimeStr(self,sVal):
        self.SetValueStr(sVal)
    def GetValueStr(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%self.txtVal.GetValue(),self)
        return self.txtVal.GetValue()
    def GetValue(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #val=self.txtVal.GetValue()#self.dt.Format('%H%M00')
        #sVal=val[:2]+val[3:]+'00'
        self.__validate__(self.txtVal.GetValue(),bTxt=True)
        sVal=self.zTime.GetStr(':')
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%sVal,self)
        return sVal
    def GetTimeStr(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #val=self.txtVal.GetValue()#self.dt.Format('%H%M00')
        #sVal=val[:2]+val[3:]+'00'
        self.__validate__(self.txtVal.GetValue(),bTxt=True)
        sVal=self.zTime.GetStr('')
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%sVal,self)
        return sVal
    def OnTextText(self,evt):
        if self.bBlock:
            return
        sVal=self.txtVal.GetValue()
        #if len(sVal)<8:
        #    evt.Skip()
            #return
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'val:%s'%(sVal),self)

        try:
            bOk,bMod,val=self.__validate__(sVal,False,bTxt=True)
            self.__markFlt__(not bOk)
            #if bMod:
            #    self.txtVal.SetValue(val)
        except:
            vtLog.vtLngTB(self.GetName())
        self.__markModified__()
        wx.PostEvent(self,vtTimeTimeEditChanged(self,sVal))
    def OnTextEnter(self,evt):
        if self.bBlock:
            return
        sVal=self.txtVal.GetValue()
        #if len(sVal)<8:
        #    evt.Skip()
            #return
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'val:%s'%(sVal),self)

        try:
            bOk,bMod,val=self.__validate__(sVal,False,bTxt=True)
            self.__markFlt__(not bOk)
            if bMod:
                self.txtVal.SetValue(val)
        except:
            vtLog.vtLngTB(self.GetName())
        self.__markModified__()
        wx.PostEvent(self,vtTimeTimeEditChanged(self,sVal))
        
    def OnPopupButton(self,evt):
        self.__createPopup__()
        if self.cbPopup.GetValue()==True:
            btn=self
            iX,iY = btn.ClientToScreen( (0,0) )
            iW,iH =  btn.GetSize()
            iPopW,iPopH=self.popWin.GetSize()
            iScreenW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            iScreenH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            if iX+iPopW>iScreenW:
                iX=iScreenW-iPopW-4
                if iX<0:
                    iX=0
            iY+=iH
            if iY+iPopH>iScreenH:
                iY=iScreenH-iPopH
                if iY<0:
                    iY=0
            
            self.popWin.Move((iX,iY))
            #self.popWin.Move((pos[0],pos[1]+sz[1]))
            self.bBusy=True
            self.popWin.Show(True)
        else:
            self.popWin.Show(False)
            self.bBusy=False
        evt.Skip()

