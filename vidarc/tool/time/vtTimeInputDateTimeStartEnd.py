#----------------------------------------------------------------------
# Name:         vtTimeInputDateTimeStartEnd.py
# Purpose:      text control with tree popup window
#
# Author:       Walter Obweger
#
# Created:      20060215
# CVS-ID:       $Id: vtTimeInputDateTimeStartEnd.py,v 1.11 2008/03/26 23:12:09 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import cStringIO
import string

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomConsumer import *
from vidarc.tool.xml.vtXmlDomConsumerLang import *
from vidarc.tool.time.vtTime import vtDateTimeStartEnd
from vidarc.tool.time.vtTimeInputDate import vtTimeInputDate
from vidarc.tool.time.vtTimeInputTimeStartEnd import vtTimeInputTimeStartEnd
    
VERBOSE=0

#----------------------------------------------------------------------
def getCancelData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00oIDAT8\x8d\xad\x93\xc1\x11\xc0 \x08\x04\x0f\xd3E\xfa\xaf\xcd6\xcc+\
\x19\xd1;p\x86\xf0fw\x80S\xb3v\xa1R\xadD\xff*\xe8\x86\xd1\r#\x03\xd6\xbeOp\
\x0f\xd8\xdb\x10\xc1s\xaf\x13d\x12\x06o\x02%Q0\x00\x98\x8aq\x9d\x82\xc1t\x02\
\x06(8\x14\xb0\x15\x8e\x05\xf3\xceY:\x9b\x80\x1d,\x928Atm%q/Q\xc1\x91D\xc6xZ\
\xe5\xcf\xf4\x00\xe0\xc8:\xc8\xd18`E\x00\x00\x00\x00IEND\xaeB`\x82' 

def getCancelBitmap():
    return wx.BitmapFromImage(getCancelImage())

def getCancelImage():
    stream = cStringIO.StringIO(getCancelData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getApplyData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00pIDAT8\x8dcddbf\xa0\x040\x91\xa3Ix\x89\xe0\x7f\xe1%\x82\xff\x19\x18\
\x18\x18XH\xd5\x08c\xbf\x8dy\xcfH\x92\x010\xcd0\x8d0@\x94\x17pi&\xda\x00\\\
\x9a\x892\x00\xd9\xdfd\x19@\x08\xe05\x00\x9f\xdfQ\x0c@\x8eW\x8a\\@\x8eAL0\'\
\xa2;\x93\x18\xe7c\xb8\x00\xa6\x98\x14W`\x04"\xb2\x8d\x84l\xc7j\x00\xa9\x80\
\x91\xd2\xec\x0c\x00y\x1c/\xbdxe+\x9e\x00\x00\x00\x00IEND\xaeB`\x82' 

def getApplyBitmap():
    return wx.BitmapFromImage(getApplyImage())

def getApplyImage():
    stream = cStringIO.StringIO(getApplyData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getDownData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x83IDAT8\x8d\xb5\x92\xd1\r\x84 \x10D\xdf\xaa5l\x05\xd4y\xa2\xdfj\
\x85TA\x03\xde\x17fUH\xee\x02\xcc\x17\t\xcc\xcc\xdb\r"\xc3H\x8d\x86*70\xa5\
\xc3\xba\xf83\x84\xf0\x93IU\xd9\xf6C\x00\xc4\x8e\xb0.\xfe\x04(\x05\xa9*\xc0e\
\xbe\x11X9\xe7\xb0A\xc9\x98\x93<\x97\x98(\x92b\x8c\xb7{\xdb\x0e\r\x96\xf8\n\
\xf8\xcc^r\x0fs\xed}\x08J\x14\xb9\xf6~\x04M\x02\xec\x18%\xfc&\x04\xaf\x8f\
\xf4\xaf\xaa\t\xbe\x1el!\xed\xde\xc2%\x86\x00\x00\x00\x00IEND\xaeB`\x82' 

def getDownBitmap():
    return wx.BitmapFromImage(getDownImage())

def getDownImage():
    stream = cStringIO.StringIO(getDownData())
    return wx.ImageFromStream(stream)

# defined event for vtInputTextML item changed
wxEVT_VTTIME_INPUT_DATETIME_STARTEND_CHANGED=wx.NewEventType()
vEVT_VTTIME_INPUT_DATETIME_STARTEND_CHANGED=wx.PyEventBinder(wxEVT_VTTIME_INPUT_DATETIME_STARTEND_CHANGED,1)
def EVT_VTTIME_INPUT_DATETIME_STARTEND_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTTIME_INPUT_DATETIME_STARTEND_CHANGED,func)
class vtTimeInputDateTimeStartEndChanged(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTTIME_INPUT_DATETIME_STARTEND_CHANGED(<widget_name>, self.OnChanged)
    """

    def __init__(self,obj,val):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTTIME_INPUT_DATETIME_STARTEND_CHANGED)
        self.val=val
    def GetValue(self):
        return self.val

class vtTimeInputDateTimeStartEndTransientPopup(wx.Dialog):
    #SIZE_PN_SCROLL=30
    def __init__(self, parent, size,style,name=''):
        self.SIZE_PN_SCROLL=wx.SystemSettings.GetMetric(wx.SYS_VSCROLL_X)
        wx.Dialog.__init__(self, parent,style=wx.RESIZE_BORDER)#wx.BORDER_SIMPLE)#|wx.STAY_ON_TOP)
        id=wx.NewId()
        wx.EVT_SIZE(self,self.OnSize)
        #st = wx.StaticText(self, -1,name,pos=(70,4))
        #bxsAll=wx.FlexGridSizer(cols=1, hgap=0, rows=2, vgap=0)#wx.BoxSizer(wx.VERTICAL)
        #bxsAll.AddGrowableRow(1)
        #bxsAll.AddGrowableCol(0)
        #bxsBt=wx.BoxSizer(wx.HORIZONTAL)
        self.cbCancel = wx.BitmapButton(id=-1,
              bitmap=getCancelBitmap(), name=u'cbCancel',
              parent=self, pos=(0,0), size=(30,30), 
              style=wx.BU_AUTODRAW)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              self.cbCancel)
        #bxsBt.AddWindow(self.cbCancel,0,border=4,flag=0)
        
        self.cbApply = wx.BitmapButton(id=-1,
              bitmap=getApplyBitmap(), name=u'cbApply',
              parent=self, pos=wx.Point(32, 00), size=wx.Size(30, 30),
              style=wx.BU_AUTODRAW)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              self.cbApply)
        #bxsBt.AddWindow(self.cbApply,0,border=4,flag=wx.LEFT)
        if size[1]<40:
            size=(size[0],40)
        i=0
        iY=40
        iH=size[1]+4
        iSzHeight=0
        sVal=parent.GetValue()
        dt=wx.DateTime.Today()
        dt.ParseFormat(sVal,'%Y-%m-%d')
        
        #bxsInp=wx.FlexGridSizer(cols=2, hgap=0, rows=1, vgap=0)
        #bxsInp.AddGrowableRow(0)
        #bxsInp.AddGrowableCol(1)
        self.calCalendar = vtTimeInputDate(
              id=-1, name=u'calCalendar',draw_grid=False,
              parent=self, pos=wx.Point(4, 40),size=wx.Size(192, 192))
        iDx=200
        #bxsInp.AddWindow(self.calCalendar,1,border=4,flag=wx.EXPAND)
        
        self.vtiTime=vtTimeInputTimeStartEnd(self,
                    id=-1,draw_grid=False,small=False,blks_per_line=12,
                    pos=(iDx,40),size=(270,192),
                    style=0,name='inTime')
        #bxsInp.AddWindow(self.vtiTime,1,border=4,flag=wx.EXPAND|wx.LEFT)
        
        #bxsAll.AddSizer(bxsBt,1,border=4,flag=wx.EXPAND|wx.LEFT|wx.RIGHT|wx.TOP)
        #bxsAll.AddSizer(bxsInp,1,border=4,flag=wx.EXPAND|wx.LEFT|wx.RIGHT|wx.TOP|wx.BOTTOM)
        
        if  0:
            self.SetSizer(bxsAll)
            bxsAll.Layout()
            bxsAll.Fit(self)
            self.bxsAll=bxsAll
            self.szOrig=self.GetSize()
            print self.szOrig
            return
        szCal=(192,192)#self.calCalendar.GetSize()
        iY=52+szCal[1]
        if iY<228:
            iY=228
        self.szOrig=(291+szCal[0],iY)
        self.SetSize(self.szOrig)
        #self.SetSizer(bxsAll)
        #bxsAll.Layout()
        #bxsAll.Fit(self)
        #self.bxsAll=bxsAll
        #print self.GetSize()
        #self.szOrig=self.GetSize()
        
    def OnSize(self,evt):
        iW,iH=self.GetSize()
        if iW<self.szOrig[0]:
            iW=self.szOrig[0]
        #if iH<self.szMin[1]:
        #    iH=self.szMin[1]
        if iH<self.szOrig[1]:
            iH=self.szOrig[1]
        self.SetSize((iW,iH))
        #print self.GetSize()
        #if self.calCalendar is not None:
        #    self.calCalendar.SetSize((iW-16,iH-52))
        evt.Skip()
    def OnCbCancelButton(self,evt):
        self.Show(False)
    def OnCbApplyButton(self,evt):
        #self.Apply()
        try:
            par=self.GetParent()
            sVal=self.GetValue()
            #sVal=dt.Format('%Y-%m-%d %H:%M')
            par.__apply__(sVal)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        self.Show(False)
    def Show(self,flag):
        try:
            par=self.GetParent()
            if flag:
                sVal=par.GetValue()
                self.SetVal(sVal)
                #self.bxsAll.Fit(self)
                #self.calCalendar.Refresh()
                #self.calCalendar.Update()
            else:
                par=self.GetParent()
                #par.cbPopup.SetValue(False)
                par.__setPopupState__(False)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        wx.Dialog.Show(self,flag)
    def GetValue(self):
        par=self.GetParent()
        dtPar=par.__getDateTime__()
        sVal=self.calCalendar.GetValueStr()
        dtPar.SetDateStr(sVal)
        self.vtiTime.Get(dtPar)
        sVal=dtPar.GetDateTimeStartEndStr(sepTm=':')
        return sVal
    def SetVal(self,sVal):
        try:
            par=self.GetParent()
            dtPar=par.__getDateTime__()
            sVal=dtPar.GetDateTimeStartEndStr(sepTm=':')
            try:
                sVal='%04d-%02d-%02d'%(dtPar.GetYear(),dtPar.GetMonth(),dtPar.GetDay())
            except:
                sVal=''
                pass
            self.calCalendar.SetValueStr(sVal)
            self.vtiTime.Set(dtPar.GetHourStart(),dtPar.GetMinuteStart(),dtPar.GetSecondStart(),
                            dtPar.GetHourEnd(),dtPar.GetMinuteEnd(),dtPar.GetSecondEnd())
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
class vtTimeInputDateTimeStartEnd(wx.Panel,vtXmlDomConsumer,vtXmlDomConsumerLang):
    DEFAULT_VAL=u'____-__-__ --:-- --:-- (--:--)'
    def __init__(self,*_args,**_kwargs):
        vtXmlDomConsumer.__init__(self)
        vtXmlDomConsumerLang.__init__(self)
        try:
            sz=_kwargs['size']
        except:
            sz=wx.Size(220,30)
            _kwargs['size']=sz
        try:
            szCb=_kwargs['size_button']
            del _kwargs['size_button']
        except:
            szCb=wx.Size(24,24)
        try:
            szTxt=_kwargs['size_text']
            del _kwargs['size_text']
        except:
            szTxt=wx.Size(sz[0]-szCb[0]-2,sz[1]-4)
        try:
            self.bSingleTag=_kwargs['single_tag']
            del _kwargs['single_tag']
        except:
            self.bSingleTag=True
            
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        self.SetAutoLayout(True)
        
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        size=(szTxt[0],szTxt[1])
        pos=(0,(sz[1]-szTxt[1])/2)
        self.txtVal = wx.TextCtrl(id=-1, name='txtVal',
              parent=self, pos=pos, size=size,
              style=0, value='')
        self.txtVal.SetMaxLength(30)
        self.txtVal.SetToolTipString('YYYY-MM-DD HH:MM HH:MM')
        #self.txtVal.SetConstraints(LayoutAnchors(self.txtVal, True, True,
        #      True, False))
        bxs.AddWindow(self.txtVal, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        self.txtVal.Bind(wx.EVT_TEXT, self.OnTextText,self.txtVal)
        self.txtVal.Bind(wx.EVT_CHAR, self.OnTextChar)
        
        size=(szCb[0],szCb[1])
        pos=(szTxt[0]+(sz[0]-(szTxt[0]+szCb[0]))/2,(sz[1]-szCb[1])/2)
        id=wx.NewId()
        self.cbPopup = wx.lib.buttons.GenBitmapToggleButton(ID=id,
              bitmap=wx.EmptyBitmap(8, 8), name=u'cbPopup',
              parent=self, pos=pos, size=size, style=wx.BU_AUTODRAW)
        #self.cbPopup.SetConstraints(LayoutAnchors(self.cbPopup, False, True,
        #      True, False))
        bxs.AddWindow(self.cbPopup, 0, border=0, flag=wx.EXPAND)
        self.SetSizer(bxs)
        self.cbPopup.SetBitmapLabel(getDownBitmap())
        self.cbPopup.SetBitmapSelected(getDownBitmap())
        self.cbPopup.Bind(wx.EVT_BUTTON,self.OnPopupButton,self.cbPopup)#id=id)
        self.popWin=None
        if self.bSingleTag:
            self.tagName='datestartendtime'
            self.tagNameStart=None
            self.tagNameEnd=None
        else:
            self.tagName='date'
            self.tagNameStart='starttime'
            self.tagNameEnd='endtime'
        self.bBlock=True
        self.dt=vtDateTimeStartEnd(True)
        #self.dt=wx.DateTime.Today()
        #self.dftBkgColor=self.txtVal.GetBackgroundColour()
        sVal=self.DEFAULT_VAL
        self.txtVal.SetValue(sVal)
        wx.CallAfter(self.txtVal.SetInsertionPoint,0)
        wx.CallAfter(self.__clearBlock__)
        self.bEnableMark=True
        self.bBusy=False
    def Enable(self,flag):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        wx.Panel.Enable(self,flag)
        self.cbPopup.Refresh()
    def SetEnableMark(self,flag):
        self.bEnableMark=flag
    def __markModified__(self,flag=True):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.bEnableMark==False:
            return
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'__markModified__;flag:%s'%(flag),
        #                origin=self.GetName())
        if flag:
            f=self.txtVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_BOLD)
            self.txtVal.SetFont(f)
        else:
            f=self.txtVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
            self.txtVal.SetFont(f)
        self.txtVal.Refresh()
    def __markFlt__(self,flag=False):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if flag:
            color=wx.TheColourDatabase.Find('YELLOW')
            self.txtVal.SetBackgroundColour(color)
        else:
            color=wx.SystemSettings_GetColour(wx.SYS_COLOUR_WINDOW)
            self.txtVal.SetBackgroundColour(color)
    def SetTagNames(self,tagName,tagNameStart=None,tagNameEnd=None):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'SetTagNames;tagname:%s,%s'%(tagName,tagNameInt),
        #                origin=self.GetName())
        self.tagName=tagName
        if tagNameStart is None or tagNameEnd is None:
            self.bSingleTag=True
        else:
            self.bSingleTag=False
            self.tagNameStart=tagNameStart
            self.tagNameEnd=tagNameEnd
    def ClearLang(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'ClearLang',
        #                origin=self.GetName())
        #if VERBOSE:
        #    vtLog.CallStack('')
        #self.txtVal.SetValue('')
        self.txtVal.SetValue(self.DEFAULT_VAL)
        self.__markModified__(False)
        self.__markFlt__(False)
    def UpdateLang(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'UpdateLang',
        #                origin=self.GetName())
        return
        if VERBOSE:
            vtLog.CallStack('')
        try:
            if self.popWin is None:
                sVal=self.doc.getNodeText(self.node,self.tagName)
            else:
                sVal=self.popWin.GetVal(self.doc.GetLang())
            self.bBlock=True
            self.txtVal.SetValue(string.split(sVal,'\n')[0])
            wx.CallAfter(self.__clearBlock__)
            #self.txtVal.SetValue(sVal)
        except:
            vtLog.vtLngTB(self.GetName())

    def Clear(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'Clear',
        #                origin=self.GetName())
        #if VERBOSE:
        #    vtLog.CallStack('')
        self.ClearLang()
        self.node=None
        self.__markModified__(False)
    def __setPopupState__(self,flag):
        self.bBusy=flag
        self.cbPopup.SetValue(flag)
    def IsBusy(self):
        return self.bBusy
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'IsBusy',
        #                origin=self.GetName())
        if self.popWin is not None:
            return self.popWin.IsShown()
        else:
            return False
    def __Close__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.popWin is not None:
            self.popWin.Show(False)
            self.__setPopupState__(False)
    def Stop(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'Stop',self)
        wx.CallAfter(self.__Close__)
    def SetDoc(self,doc):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'SetDoc',
        #                origin=self.GetName())
        vtXmlDomConsumer.SetDoc(self,doc)
        vtXmlDomConsumerLang.SetDoc(self,doc)
    def SetNode(self,node):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'SetNode;node:%s'%(node),
        #                origin=self.GetName())
        if VERBOSE:
            vtLog.CallStack('')
            print node
        self.Clear()
        self.node=node
        if self.doc is None:
            return
        if self.node is None:
            return
        try:
            sVal=string.split(self.doc.getNodeText(self.node,self.tagName),'\n')[0]
            if self.bSingleTag==False:
                sValS=string.split(self.doc.getNodeText(self.node,self.tagNameStart),'\n')[0]
                sValE=string.split(self.doc.getNodeText(self.node,self.tagNameEnd),'\n')[0]
                sVal=sVal+' '+sValS+' '+sValE
            #sVal=sVal[:4]
            bOk,bMod,sVal=self.__validate__(sVal,bTxt=self.bSingleTag)
            self.bBlock=True
            self.__markModified__(bMod)
            self.__markFlt__(not bOk)
            self.txtVal.SetValue(sVal)
            wx.CallAfter(self.__clearBlock__)
        except:
            pass
            #vtLog.vtLngTB(self.GetName())
        #self.__getSelNode__()
        self.UpdateLang()
    def __validate__(self,sVal,bSet2NowOnFlt=False,bTxt=False):
        if VERBOSE:
            vtLog.CallStack(sVal)
            print bTxt,self.bSingleTag
        bMod=False
        bOk=False
        try:
            if bTxt:
                ret=self.dt.SetStr(sVal)
                #iLen=5
            else:
                ret=self.dt.SetStr(sVal,sepDt='',sepTm='')
                #iLen=4
            #if iLen!=len(sVal):
            #    ret=-1
                #sVal=sVal[:iLen]
                #bMod=True
            #if ret!=iLen:
            #    if bSet2NowOnFlt:
            #        bMod=True
            #else:
            #    bOk=True
            bOk=True
            sVal=self.dt.GetDateTimeStartEndShortStr()
        except:
            if len(sVal)>0:
                #vtLog.vtLngTB(self.GetName())
                
                vtLog.vtLngCurWX(vtLog.WARN,sVal,self)
                if bSet2NowOnFlt:
                    bMod=True
            if bTxt==False:
                sVal=self.dt.GetDateTimeStartEndShortStr()
        if bMod:
            if bSet2NowOnFlt:
                self.dt.Now()
        if self.bSingleTag:
            sValNew=self.dt.GetDateTimeStartEndStoreStr()
        else:
            sValNew=self.dt.GetDateTimeStartEndStr('','')[:22]
        if VERBOSE:
            print 'new',sValNew
            print 'val',sVal
            print self.bSingleTag
        #val=self.dt.GetDateTimeStartEndShortStr()
        #if sValNew!=sVal:
        #    bMod=True
        return bOk,bMod,sVal
    def __getDateTime__(self):
        return self.dt
    def __clearBlock__(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'__clearBlock__',
        #                origin=self.GetName())
        self.bBlock=False
    def GetNode(self,node=None):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'GetNode',
        #                origin=self.GetName())
        #if VERBOSE:
        #    vtLog.CallStack('')
        if node is None:
            node=self.node
        if node is None:
            return
        try:
            if self.bSingleTag:
                sVal=self.dt.GetDateTimeStartEndStoreStr()
                self.doc.setNodeText(node,self.tagName,sVal)
            else:
                sVal=self.dt.GetDateTimeStartEndStr('','')
                strs=sVal.split(' ')
                self.doc.setNodeText(node,self.tagName,strs[0])
                self.doc.setNodeText(node,self.tagNameStart,strs[1])
                self.doc.setNodeText(node,self.tagNameEnd,strs[2])
                
            #self.__setNode__()

            self.__markModified__(False)
            self.doc.AlignNode(node)
        except:
            #vtLog.vtLngTB(self.GetName())
            pass
            
    def __getDoc__(self):
        return self.doc
    def __getNode__(self,node=None):
        if node is None:
            return self.node
        else:
            return node
    def __apply__(self,sVal):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'__apply__;val:%s'%(sVal),
        #                origin=self.GetName())
        #if VERBOSE:
        #    vtLog.CallStack(sVal)
        try:
            #sVal=self.popWin.GetVal()
            #if self.trSetNode is not None:
            #    sVal=self.trSetNode(self.docTreeTup[0],node)
            #else:
            #    sVal=''
            bOk,bMod,sVal=self.__validate__(sVal,bTxt=True)
            self.__markFlt__(not bOk)
            if bMod or self.txtVal.GetValue()!=sVal:
                if self.bBlock==False:
                    self.__markModified__(True)
                    wx.PostEvent(self,vtTimeInputDateTimeStartEndChanged(self,sVal))
        except:
            sVal=_(u'fault')
            vtLog.vtLngTB(self.GetName())
        self.bBlock=True
        self.txtVal.SetValue(string.split(sVal,'\n')[0])
        wx.CallAfter(self.__clearBlock__)
        return sVal
        
    def __createPopup__(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCS(vtLog.DEBUG,'__createPopup__',
                        origin=self.GetName())
        if self.popWin is None:
            sz=self.GetSize()
            sz=(sz[0],sz[1]-20)
            try:
                self.popWin=vtTimeInputDateTimeStartEndTransientPopup(self,sz,wx.SIMPLE_BORDER)
            except:
                vtLog.vtLngTB(self.GetName())
            #self.popWin.SetVal(self.txtVal.GetValue(),self.doc.GetLang())
        else:
            pass
    def Now(self):
        self.dt.Now()
    def SetValueStore(self,sVal):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        try:
            #if self.bSingleTag:
            #    self.dt.SetStr(sVal)
            #else:
            #    self.dt.SetStr(sVal,'','')
            bOk,bMod,sVal=self.__validate__(sVal,bTxt=self.bSingleTag)
            self.bBlock=True
            self.__markModified__(bMod)
            self.__markFlt__(not bOk)
            self.txtVal.SetValue(sVal)
            wx.CallAfter(self.__clearBlock__)
        except:
            vtLog.vtLngTB(self.GetName())
    def GetValueStore(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'GetValue:%s'%self.txtVal.GetValue(),
        #                origin=self.GetName())
        if self.bSingleTag:
            sValNew=self.dt.GetDateTimeStartEndStoreStr()
            return sValNew
        else:
            sValNew=self.dt.GetDateTimeStartEndStr('','')[:22]
            return sValNew.split(' ')
    def GetValue(self):
        return self.txtVal.GetValue()
    def OnTextText(self,evt):
        if self.bBlock:
            return
        sVal=self.txtVal.GetValue()
        try:
            bOk,bMod,val=self.__validate__(sVal,False,bTxt=True)
            self.__markFlt__(not bOk)
            if bMod:
                self.bBlock=True
                self.txtVal.SetValue(val)
        except:
            vtLog.vtLngTB(self.GetName())
        self.__markModified__()
        wx.CallAfter(self.__clearBlock__)
        wx.PostEvent(self,vtTimeInputDateTimeStartEndChanged(self,sVal))
    def OnTextChar(self,evt):
        if VERBOSE:
            vtLog.CallStack('')
        sVal=self.txtVal.GetValue()
        iPos=self.txtVal.GetInsertionPoint()
        #iKeyCode=evt.GetKeyCode()
        if wx.VERSION >= (2,8):
            iKeyCode=evt.GetKeyCode()
        else:
            iKeyCode = evt.KeyCode()
        if iKeyCode == wx.WXK_BACK:
            iPos-=1
            if iPos<0:
                return
            if iPos in [4,7,10,13,16,19]:
                    iPos-=1
            if iPos<10:
                sC=u'_'
            else:
                sC=u'-'
            sVal=sVal[:iPos]+sC+sVal[iPos+1:]
            self.txtVal.SetValue(sVal)
            self.txtVal.SetInsertionPoint(iPos)
            return
        if iKeyCode == wx.WXK_DELETE or iKeyCode==wx.WXK_NUMPAD_DELETE:
            if iPos>=22:
                return
            if iPos in [4,7,10,13,16,19]:
                    iPos+=1
            if iPos<10:
                sC=u'_'
            else:
                sC=u'-'
            sVal=sVal[:iPos]+sC+sVal[iPos+1:]
            self.txtVal.SetValue(sVal)
            self.txtVal.SetInsertionPoint(iPos+1)
            return
        if iKeyCode>=wx.WXK_START:
            evt.Skip()
            return
        try:
            if iPos in [4,7,10,13,16,19]:
                    iPos+=1
            sKey=unicode(chr(iKeyCode))
            if iPos>=22:
                return
            iNum=int(sKey)
            if iNum>=0 and iNum<=9:
                sVal=sVal[:iPos]+sKey+sVal[iPos+1:]
                #if iPos in [3,6,9,12,13]:
                #    iPos+=1
                self.txtVal.SetValue(sVal)
                self.txtVal.SetInsertionPoint(iPos+1)
        except:
            evt.Skip()
            vtLog.vtLngTB(self.GetName())
    def OnPopupButton(self,evt):
        self.__createPopup__()
        if self.cbPopup.GetValue()==True:
            btn=self
            iX,iY = btn.ClientToScreen( (0,0) )
            iW,iH =  btn.GetSize()
            iPopW,iPopH=self.popWin.GetSize()
            iScreenW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            iScreenH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            if iX+iPopW>iScreenW:
                iX=iScreenW-iPopW-4
                if iX<0:
                    iX=0
            iY+=iH
            if iY+iPopH>iScreenH:
                iY=iScreenH-iPopH
                if iY<0:
                    iY=0
            
            self.popWin.Move((iX,iY))
            #self.popWin.Move((pos[0],pos[1]+sz[1]))
            self.bBusy=True
            self.popWin.Show(True)
        else:
            self.popWin.Show(False)
            self.bBusy=False
        evt.Skip()

