
import wx
import wx.lib.masked.maskededit
import wx.lib.popupctl
import wx.calendar
import wx.lib.buttons

import time
import calendar
import string
from timeconvert import *

# defined event for vgpXmlTree item selected
wxEVT_VTIME_DATETIMEGM_CHANGED=wx.NewEventType()
vEVT_VTIME_DATETIMEGM_CHANGED=wx.PyEventBinder(wxEVT_VTIME_DATETIMEGM_CHANGED,1)
def EVT_VTIME_DATETIMEGM_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTIME_DATETIMEGM_CHANGED,func)
class vTimeDateTimeGMChanged(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTIME_DATETIMEGM_CHANGED(<widget_name>, self.OnChanged)
    """

    def __init__(self,obj,text):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTIME_DATETIMEGM_CHANGED)
        self.text=text
    def GetText(self):
        return self.text
    

class vTimeDateTimeGM(wx.lib.popupctl.PopupControl):
    def __init__(self,*_args,**_kwargs):
        apply(wx.lib.popupctl.PopupControl.__init__,(self,) + _args,_kwargs)

        self.win = wx.Window(self,-1,pos = (2,2),style = 0)
        lbl=wx.StaticText(self.win,-1,'Time',pos=wx.Point(0,0))
        self.txt=wx.lib.masked.textctrl.TextCtrl( self.win, -1, "", pos=wx.Point(45,0),
                            autoformat       = "MILTIMEHHMMSS",
                            demo             = True,
                            name             = "Start")
        bt=wx.Button(self.win,-1,'close',pos=wx.Point(130,0),size=wx.Size(40,-1))
        wx.EVT_BUTTON(bt,bt.GetId(),self.OnClose)
        self.cal = wx.calendar.CalendarCtrl(self.win,-1,pos = (0,30))
        
        bz = self.cal.GetBestSize()
        self.win.SetSize(wx.Size(bz[0],bz[1]+40))

        # This method is needed to set the contents that will be displayed
        # in the popup
        self.SetPopupContent(self.win)

        # Event registration for date selection
        wx.calendar.EVT_CALENDAR_DAY(self.cal,self.cal.GetId(),self.OnCalSelected)

    # Method called when button clicked
    def __set_date_time__(self,t):
        # Format the date that was selected for the text part of the control
        h=time.localtime(time.time())
        t=time.gmtime(time.mktime(t+[0,0,h[8]]))
        s='%04d%02d%02d_%02d%02d%02d' % (t[0],t[1],t[2],t[3],t[4],t[5])
        self.SetValue(s)
        wx.PostEvent(self,vTimeDateTimeGMChanged(self,s))

    def OnClose(self,evt):
        self.PopDown()
        try:
            t=(zHour,zMin,zSec)=map(string.atoi,string.split(self.txt.GetValue(),':'))
        except:
            t=(zHour,zMin,zSec)=(0,0,0)
        date = self.cal.GetDate()
        self.__set_date_time__([date.GetYear(),date.GetMonth()+1,
                                          date.GetDay(),
                                          zHour,zMin,zSec])
        evt.Skip()

    # Method called when a day is selected in the calendar
    def OnCalSelected(self,evt):
        self.PopDown()
        try:
            t=(zHour,zMin,zSec)=map(string.atoi,string.split(self.txt.GetValue(),':'))
        except:
            t=(zHour,zMin,zSec)=(0,0,0)
        date = self.cal.GetDate()

        # Format the date that was selected for the text part of the control
        #self.SetValue('%04d%02d%02d %02d%02d%02d' % (date.GetYear(),date.GetMonth()+1,
        #                                  date.GetDay(),
        #                                  zHour,zMin,zSec
        #                                  ))
        self.__set_date_time__([date.GetYear(),date.GetMonth()+1,
                                          date.GetDay(),
                                          zHour,zMin,zSec])
        evt.Skip()

    # Method overridden from wxPopupControl
    # This method is called just before the popup is displayed
    # Use this method to format any controls in the popup
    def FormatContent(self):
        # I parse the value in the text part to resemble the correct date in
        # the calendar control
        t=convertStrTime2Time(self.GetValue())
        if t is None:
            t=time.localtime(time.time())
        else:
            t=time.localtime(t)
        self.cal.SetDate(wx.DateTimeFromDMY(t[2],t[1]-1,t[0]))
        self.txt.SetValue("%02d:%02d:%02d"%(t[3],t[4],t[5]))
