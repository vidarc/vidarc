from wxPython.lib.maskededit import wxMaskedTextCtrl
from wxPython.lib.popupctl import wxPopupControl
from wxPython.lib.buttons import *
from wxPython.calendar import *
from wxPython.wx import wxNewEventType
from wxPython.wx import wxPyEvent
from wxPython.wx import wxWindow
from wxPython.wx import wxStaticText
from wxPython.wx import wxButton
from wxPython.wx import wxPoint
from wxPython.wx import wxSize
from wxPython.wx import EVT_BUTTON
from wxPython.wx import wxDateTimeFromDMY
from wxPython.wx import wxPostEvent

import time
import calendar
import string

from timeconvert import *

# defined event for vgpXmlTree item selected
wxEVT_VTIME_DATETIMELOCAL_CHANGED=wxNewEventType()
vEVT_VTIME_DATETIMELOCAL_CHANGED=wx.PyEventBinder(wxEVT_VTIME_DATETIMELOCAL_CHANGED,1)
def EVT_VTIME_DATETIMELOCAL_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTIME_DATETIMELOCAL_CHANGED,func)
class vTimeDateTimeLocalChanged(wxPyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTIME_DATETIMELOCAL_CHANGED(<widget_name>, self.OnChanged)
    """

    def __init__(self,obj,text):
        wxPyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTIME_DATETIMELOCAL_CHANGED)
        self.text=text
    def GetText(self):
        return self.text
    

class vTimeDateTimeLocal(wxPopupControl):
    def __init__(self,*_args,**_kwargs):
        apply(wxPopupControl.__init__,(self,) + _args,_kwargs)

        self.win = wxWindow(self,-1,pos = (2,2),style = 0)
        lbl=wxStaticText(self.win,-1,'Time',pos=wxPoint(0,0))
        self.txt=wxMaskedTextCtrl( self.win, -1, "", pos=wxPoint(45,0),
                            autoformat       = "MILTIMEHHMMSS",
                            demo             = True,
                            name             = "Start")
        bt=wxButton(self.win,-1,'close',pos=wxPoint(130,0),size=wxSize(40,-1))
        EVT_BUTTON(bt,bt.GetId(),self.OnClose)
        self.cal = wxCalendarCtrl(self.win,-1,pos = (0,30))
        
        bz = self.cal.GetBestSize()
        self.win.SetSize(wxSize(bz[0],bz[1]+40))

        # This method is needed to set the contents that will be displayed
        # in the popup
        self.SetPopupContent(self.win)

        # Event registration for date selection
        EVT_CALENDAR_DAY(self.cal,self.cal.GetId(),self.OnCalSelected)

    # Method called when button clicked
    def OnClose(self,evt):
        self.PopDown()
        try:
            t=(zHour,zMin,zSec)=map(string.atoi,string.split(self.txt.GetValue(),':'))
        except:
            t=(zHour,zMin,zSec)=(0,0,0)
        date = self.cal.GetDate()
        
        s='%04d/%02d/%02d %02d:%02d:%02d' % (date.GetYear(),date.GetMonth()+1,
                                          date.GetDay(),
                                          zHour,zMin,zSec
                                          )
        # Format the date that was selected for the text part of the control
        self.SetValue(s)
        wxPostEvent(self,vTimeDateTimeLocalChanged(self,s))
        evt.Skip()

    # Method called when a day is selected in the calendar
    def OnCalSelected(self,evt):
        self.PopDown()
        try:
            t=(zHour,zMin,zSec)=map(string.atoi,string.split(self.txt.GetValue(),':'))
        except:
            t=(zHour,zMin,zSec)=(0,0,0)
        date = self.cal.GetDate()
        
        s='%04d/%02d/%02d %02d:%02d:%02d' % (date.GetYear(),date.GetMonth()+1,
                                          date.GetDay(),
                                          zHour,zMin,zSec
                                          )
        # Format the date that was selected for the text part of the control
        self.SetValue(s)
        wxPostEvent(self,vTimeDateTimeLocalChanged(self,s))
        evt.Skip()

    # Method overridden from wxPopupControl
    # This method is called just before the popup is displayed
    # Use this method to format any controls in the popup
    def FormatContent(self):
        # I parse the value in the text part to resemble the correct date in
        # the calendar control
        strs=string.split(self.GetValue(),' ')
        try:
            t=map(string.atoi,strs[0].split('/')+strs[1].split(':'))
            
        except:
            t=time.localtime(time.time())
        
        self.cal.SetDate(wxDateTimeFromDMY(t[2],t[1]-1,t[0]))
        self.txt.SetValue("%02d:%02d:%02d"%(t[3],t[4],t[5]))
        
        if 0==1:
          txtValue = strs[0]#self.GetValue()
          dmy = txtValue.split('/')
          didSet = False
          if len(dmy) == 3:
            date = self.cal.GetDate()
            d = int(dmy[0])
            m = int(dmy[1]) - 1
            y = int(dmy[2])
            if d > 0 and d < 31:
                if m >= 0 and m < 12:
                    if y > 1000:
                        self.cal.SetDate(wxDateTimeFromDMY(d,m,y))
                        didSet = True
          if not didSet:
            self.cal.SetDate(wxDateTime_Today())
