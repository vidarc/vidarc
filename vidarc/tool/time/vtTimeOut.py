#----------------------------------------------------------------------------
# Name:         vtTimeOut.py
# Purpose:      time out object
#               thread safe
#
# Author:       Walter Obweger
#
# Created:      20090508
# CVS-ID:       $Id: vtTimeOut.py,v 1.4 2009/05/27 03:22:32 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.vtThreadCore import getRLock
from vtTime import vtDateTime
import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
if VERBOSE>0:
    import vidarc.tool.log.vtLog as vtLog

class vtTimeOut(vtDateTime):
    def __init__(self,bLocal,fTimeOut,func,*args,**kwargs):
        vtDateTime.__init__(self,bLocal=bLocal)
        self.zCheck=vtDateTime(bLocal=bLocal)
        self.func=func
        self.args=args
        self.kwargs=kwargs
        self.fTimeOut=fTimeOut
        if self.fTimeOut is None:
            self.CheckTimeOut=self.__CheckTimeOutInAct__
        else:
            self.CheckTimeOut=self.__CheckTimeOut__
        if self.func is None:
            self.Notify=self.__NotifyDft__
        else:
            self.Notify=self.__Notify__
        self.semAccess=getRLock()
    def Start(self):
        try:
            if VERBOSE>0:
                vtLog.vtLngCurCls(vtLog.DEBUG,''%(),self)
            self.semAccess.acquire()
            if self.fTimeOut is None:
                self.CheckTimeOut=self.__CheckTimeOutInAct__
            else:
                self.CheckTimeOut=self.__CheckTimeOut__
            self.Now()
        finally:
            self.semAccess.release()
    def Stop(self):
        try:
            if VERBOSE>0:
                vtLog.vtLngCurCls(vtLog.DEBUG,''%(),self)
            self.semAccess.acquire()
            self.CheckTimeOut=self.__CheckTimeOutInAct__
        finally:
            self.semAccess.release()
    def SetTimeOut(self,fTimeOut):
        try:
            self.semAccess.acquire()
            self.fTimeOut=fTimeOut
            if self.fTimeOut is None:
                self.CheckTimeOut=self.__CheckTimeOutInAct__
            else:
                self.CheckTimeOut=self.__CheckTimeOut__
        finally:
            self.semAccess.release()
    def __NotifyDft__(self):
        pass
    def __Notify__(self):
        self.func(*self.args,**self.kwargs)
    def __CheckTimeOutInAct__(self,bRestart=True):
        return False
    def __CheckTimeOut__(self,bRestart=True):
        try:
            self.semAccess.acquire()
            self.zCheck.Now()
            fSec=self.CalcDiffSecFloat(self.zCheck)
            if fSec>self.fTimeOut:
                self.Notify()
                if bRestart:
                    self.Now()
                return True
            return False
        finally:
            self.semAccess.release()
