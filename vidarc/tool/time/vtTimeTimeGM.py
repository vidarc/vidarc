#----------------------------------------------------------------------------
# Name:         vtTimeTimeGM.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtTimeTimeGM.py,v 1.1 2005/12/13 13:19:33 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
from wxPython.lib.maskededit import wxMaskedTextCtrl
from wxPython.lib.popupctl import wxPopupControl
from wxPython.lib.buttons import *
from wxPython.calendar import *

import time
import calendar
import string
from timeconvert import *

# defined event for vgpXmlTree item selected
wxEVT_DATETIMEGM_CHANGED=wxNewEventType()
def EVT_DATETIMEGM_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_DATETIMEGM_CHANGED,func)
class wxDateTimeGMChanged(wxPyEvent):
    """
    Posted Events:
        Text changed event
            EVT_DATETIMEGM_CHANGED(<widget_name>, self.OnChanged)
    """

    def __init__(self,text):
        wxPyEvent.__init__(self)
        self.SetEventType(wxEVT_DATETIMEGM_CHANGED)
        self.text=text
    def GetText(self):
        return self.text
    

class vtTimeDateTimeGM(wxPopupControl):
    def __init__(self,*_args,**_kwargs):
        apply(wxPopupControl.__init__,(self,) + _args,_kwargs)

        self.win = wxWindow(self,-1,pos = (2,2),style = 0)
        lbl=wxStaticText(self.win,-1,'Time',pos=wxPoint(0,0))
        self.txt=wxMaskedTextCtrl( self.win, -1, "", pos=wxPoint(45,0),
                            autoformat       = "MILTIMEHHMMSS",
                            demo             = True,
                            name             = "Start")
        bt=wxButton(self.win,-1,'close',pos=wxPoint(130,0),size=wxSize(40,-1))
        EVT_BUTTON(bt,bt.GetId(),self.OnClose)
        self.cal = wxCalendarCtrl(self.win,-1,pos = (0,30))
        
        bz = self.cal.GetBestSize()
        self.win.SetSize(wxSize(bz[0],bz[1]+40))

        # This method is needed to set the contents that will be displayed
        # in the popup
        self.SetPopupContent(self.win)

        # Event registration for date selection
        EVT_CALENDAR_DAY(self.cal,self.cal.GetId(),self.OnCalSelected)

    # Method called when button clicked
    def __set_date_time__(self,t):
        # Format the date that was selected for the text part of the control
        h=time.localtime(time.time())
        t=time.gmtime(time.mktime(t+[0,0,h[8]]))
        s='%04d%02d%02d_%02d%02d%02d' % (t[0],t[1],t[2],t[3],t[4],t[5])
        self.SetValue(s)
        wxPostEvent(self,wxDateTimeGMChanged(s))

    def OnClose(self,evt):
        self.PopDown()
        try:
            t=(zHour,zMin,zSec)=map(string.atoi,string.split(self.txt.GetValue(),':'))
        except:
            t=(zHour,zMin,zSec)=(0,0,0)
        date = self.cal.GetDate()
        self.__set_date_time__([date.GetYear(),date.GetMonth()+1,
                                          date.GetDay(),
                                          zHour,zMin,zSec])
        evt.Skip()

    # Method called when a day is selected in the calendar
    def OnCalSelected(self,evt):
        self.PopDown()
        try:
            t=(zHour,zMin,zSec)=map(string.atoi,string.split(self.txt.GetValue(),':'))
        except:
            t=(zHour,zMin,zSec)=(0,0,0)
        date = self.cal.GetDate()

        # Format the date that was selected for the text part of the control
        #self.SetValue('%04d%02d%02d %02d%02d%02d' % (date.GetYear(),date.GetMonth()+1,
        #                                  date.GetDay(),
        #                                  zHour,zMin,zSec
        #                                  ))
        self.__set_date_time__([date.GetYear(),date.GetMonth()+1,
                                          date.GetDay(),
                                          zHour,zMin,zSec])
        evt.Skip()

    # Method overridden from wxPopupControl
    # This method is called just before the popup is displayed
    # Use this method to format any controls in the popup
    def FormatContent(self):
        # I parse the value in the text part to resemble the correct date in
        # the calendar control
        t=convertStrTime2Time(self.GetValue())
        if t is None:
            t=time.localtime(time.time())
        else:
            t=time.localtime(t)
        self.cal.SetDate(wxDateTimeFromDMY(t[2],t[1]-1,t[0]))
        self.txt.SetValue("%02d:%02d:%02d"%(t[3],t[4],t[5]))
