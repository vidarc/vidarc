#----------------------------------------------------------------------------
# Name:         vTimeLimit.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vTimeLimit.py,v 1.1 2005/12/13 13:19:33 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
from wx import DateTime
import time
import calendar

dt=DateTime()
def getWeek(val):
    dt.ParseFormat(val,'%Y%m%d')
    return dt.GetWeekOfYear()

def getRange(ranges,year,month,day):
    str='%04d%02d%02d'%(year,month,day)
    for r in ranges:
        if r[0]=='--------':
            if r[1]=='--------':
                return r
            if str<=r[1]:
                return r
        elif r[1]=='--------':
            if str>=r[0]:
                return r
        else:
            if r[0]<=str and str<=r[1]:
                return r
    return ('********','********',[0.0, 0.0, 0.0, 0.0, 0.0, 0.0 ,0.0],100)
def getNormalTimes(sStart,sEnd,ranges):
    """ ranges is a list of normal working times
        [('20050101','20050331',[8.5, 8.5, 8.5, 8.5, 8.5, 0.0 ,0.0],100),
         ('20050331','--------',[2.125, 2.125, 2.125, 2.125, 2.125, 0.0 ,0.0],100)]
"""    
    iStartYear=int(sStart[0:4])
    iStartMon=int(sStart[4:6])
    iStartDay=int(sStart[6:8])

    iEndYear=int(sEnd[0:4])
    iEndMon=int(sEnd[4:6])
    iEndDay=int(sEnd[6:8])

    dYear={}
    for iYear in range(iStartYear,iEndYear+1):
        dMon={}
        dWeek={}
        iWeek=-1
        iOldWeek=-1
        fSumYear=0.0
        fSumWeek=0.0
        for iMon in range(iStartMon,iEndMon+1):
            fSumMon=0.0
            dDay={}
            month=calendar.monthcalendar(iYear,iMon)
            for w in month:
                i=0
                for iDay in w:
                    if iDay==0:
                        i+=1
                        continue
                    str='%04d%02d%02d'%(iYear,iMon,iDay)
                    if sStart<=str and str<=sEnd:
                        # within range
                        r=getRange(ranges,iYear,iMon,iDay)
                        iWeek=getWeek(str)
                        if iWeek!=iOldWeek:
                            if iOldWeek>=0:
                                dWeek[iOldWeek]=fSumWeek
                            iOldWeek=iWeek
                            fSumWeek=0.0
                        fDayTime=r[2][i]
                        try:
                            fDayTime*=float(r[3])/100.0
                        except:
                            pass
                        fSumWeek+=fDayTime
                        dDay[iDay]=fDayTime
                        fSumMon+=fDayTime
                        fSumYear+=fDayTime
                    i+=1
            dMon[iMon]={'sum':fSumMon,'day':dDay}
        dWeek[iWeek]=fSumWeek
        dYear[iYear]={'sum':fSumYear,'month':dMon,'week':dWeek}
    return dYear

if __name__=='__main__':
    ranges=[('20050101','20050331',[8.5, 8.5, 8.5, 8.5, 8.5, 0.0 ,0.0]),
         ('20050331','--------',[2.125, 2.125, 2.125, 2.125, 2.125, 0.0 ,0.0])]
    print 'step 01'
    print getNormalTimes('20050101','20050131',ranges)

    print 'step 02'
    print getNormalTimes('20050201','20050231',ranges)

    print 'step 03'
    print getNormalTimes('20050301','20050331',ranges)

    print 'step 04'
    print getNormalTimes('20050401','20050431',ranges)

    print 'step 12'
    print getNormalTimes('20050101','20050231',ranges)

    print 'step 13'
    print getNormalTimes('20050101','20050331',ranges)

    print 'step 14'
    print getNormalTimes('20050101','20050431',ranges)
    