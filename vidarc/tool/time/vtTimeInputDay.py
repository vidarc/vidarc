#----------------------------------------------------------------------------
# Name:         vtTimeInputDay.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060222
# CVS-ID:       $Id: vtTimeInputDay.py,v 1.3 2008/03/26 23:12:09 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import cStringIO

from vidarc.tool.draw.vtDrawCanvasPanel import *
from vidarc.tool.draw.vtDrawCanvasObjectBase import *
from vidarc.tool.draw.vtDrawCanvasTextSelectable import *

import vidarc.tool.art.vtColor as vtColor
import vidarc.tool.log.vtLog as vtLog
import calendar,types,copy
import vidarc.tool.time.vtTime as vtTime

[wxID_VTDRAWCANVASPANEL, wxID_VTDRAWCANVASPANELSCWDRAWING, 
 wxID_VTDRAWCANVASPANELCHCMONTH, wxID_VTDRAWCANVASPANELSPYEAR, 
 wxID_VTDRAWCANVASPANELSPWEEK, 
] = [wx.NewId() for _init_ctrls in range(5)]

#----------------------------------------------------------------------
def getLeftData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x89IDAT8\x8d\xc5\x93;\x0e\x80 \x10Dw\xc1\x13\xab\xf4j\r\x1cB=\x16\'\
\xd8\x92\x0b`\x05\x01B\x08\x9f\xc2IHX\x92\x99y\xc5\x82\xc88\xcc\x88M\xb9G\
\x03\xae\xf3p\xfe\xbe\x8c\x18\x8d1\xe1\xad) n\xccU\r(\x19\xe3\xf6b@\xad\xadJ\
\xd0b\xcc\xdb\x93\x00kmOq\x10\xe6\x8b\xb4ok\x95\x84\x88\x00\x00\xe0~^L\x08\
\xbc\xa4\xd2\xd8\x1aV$()\x0f"\xa2@\xd0\xb4\x89Ri\x8c\xc9R\x04\xc6\xbb\x8f\
\x10\xc2\x85\xf9\xf7\xdf\xf8\x01-\x971\xe7\xc3]\x80\xb0\x00\x00\x00\x00IEND\
\xaeB`\x82' 

def getLeftBitmap():
    return wx.BitmapFromImage(getLeftImage())

def getLeftImage():
    stream = cStringIO.StringIO(getLeftData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getRightData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x80IDAT8\x8d\xc5\x93\xc1\r\x80 \x0cEi\xf1\xdc\x15\xd9\x01\x18C\x1dB\
\x1d\x8b\tX\x02O\x1a \x95\xb4\xe1`\x93&\xbd\xbc\xd7_\x12\x00\xd0\x9a\x99\xc2\
)\xba\x16\xc4\xe0\xcb\x94\x80\x88L\x0c\xbehE\xec\t\x1aQ# "\xb5h\x91l\xa9%\
\xeb\xb6\xc3g\x02.\x05\'\xab\x85l\x82\x9c\xb3$\x18/H)\r\x81\xe3\xbc\x9a\x13D\
o\xd0C\x9f\x82~\xfb\x08\x1c&\x90\x80o\x01Z\x03h\x8ds\xae<\xb3\xaa\x7f\xff\
\x8d7\x00\x9d+\xcf\xef\xe1r/\x00\x00\x00\x00IEND\xaeB`\x82' 

def getRightBitmap():
    return wx.BitmapFromImage(getRightImage())

def getRightImage():
    stream = cStringIO.StringIO(getRightData())
    return wx.ImageFromStream(stream)


class vtTimeInputDay(vtDrawCanvasPanel):
    DFT_ACT_COLOR=[
        vtColor.Scale(vtColor.MONDAY,       0.8),
        vtColor.Scale(vtColor.TUESDAY,      0.8),
        vtColor.Scale(vtColor.WEDNESDAY,    0.8),
        vtColor.Scale(vtColor.THURSDAY,     0.8),
        vtColor.Scale(vtColor.FRIDAY,       0.8),
        vtColor.Scale(vtColor.SATURDAY,     0.8),
        vtColor.Scale(vtColor.SUNDAY,       0.8),
        
        vtColor.SEAGREEN,           # week layer = 7
        vtColor.SADDLE_BROWN,
        vtColor.WHITE,
        vtColor.BLACK,
        ]
    DFT_COLOR=[
        vtColor.Scale(vtColor.MONDAY,       0.8),
        vtColor.Scale(vtColor.TUESDAY,      0.8),
        vtColor.Scale(vtColor.WEDNESDAY,    0.8),
        vtColor.Scale(vtColor.THURSDAY,     0.8),
        vtColor.Scale(vtColor.FRIDAY,       0.8),
        vtColor.Scale(vtColor.SATURDAY,     0.8),
        vtColor.Scale(vtColor.SUNDAY,       0.8),
        
        vtColor.SEAGREEN,           # week layer = 7
        vtColor.SADDLE_BROWN,
        vtColor.WHITE,
        vtColor.BLACK,
        ]
    DFT_DIS_COLOR=[
        vtColor.Scale(vtColor.MONDAY,0.8),
        vtColor.Scale(vtColor.TUESDAY,0.8),
        vtColor.Scale(vtColor.WEDNESDAY,0.8),
        vtColor.Scale(vtColor.THURSDAY,0.8),
        vtColor.Scale(vtColor.FRIDAY,0.8),
        vtColor.Scale(vtColor.SATURDAY,0.8),
        vtColor.Scale(vtColor.SUNDAY,0.8),
        vtColor.Scale(vtColor.SEAGREEN,0.8),
        vtColor.Scale(vtColor.SADDLE_BROWN,0.8),
        vtColor.Scale(vtColor.WHITE,0.8),
        vtColor.Scale(vtColor.BLACK,0.8),
        ]
    #FOCUS_LAYER=7
    def _init_ctrls(self, prnt,name):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTDRAWCANVASPANEL, name=name, parent=prnt,
              pos=wx.Point(0, 0), size=wx.Size(100, 100),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(100, 100))
        self.SetAutoLayout(True)
        fgs = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)
        fgs.AddGrowableRow(2)
        fgs.AddGrowableCol(0)
        #fgs.AddGrowableCol(1)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        self.spYear = wx.SpinCtrl(id=wxID_VTDRAWCANVASPANELSPYEAR, initial=2000, max=2100,
              min=0, name=u'spYear', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(117, 21), style=wx.SP_ARROW_KEYS)
        self.spYear.Bind(wx.EVT_SPINCTRL, self.OnUpdateDate,self.spYear)
        self.spYear.Bind(wx.EVT_TEXT, self.OnUpdateDate,self.spYear)
        
        id=wx.NewId()
        self.cbWrkDay = wx.lib.buttons.GenBitmapButton(ID=id,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbDec',
              parent=self, style=wx.BU_AUTODRAW)
        self.cbWrkDay.SetBitmapLabel(getLeftBitmap())
        #self.cbWrkDay.Bind(wx.EVT_BUTTON,self.OnWrkDayButton,self.cbWrkDay)
        
        id=wx.NewId()
        self.cbNoWrkDay = wx.lib.buttons.GenBitmapButton(ID=id,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbDec',
              parent=self, style=wx.BU_AUTODRAW)
        self.cbNoWrkDay.SetBitmapLabel(getLeftBitmap())
        #self.cbNoWrkDay.Bind(wx.EVT_BUTTON,self.OnNoWrkDayButton,self.cbNoWrkDay)
        
        bxs.AddWindow(self.spYear, 3, border=0, flag=wx.EXPAND)
        bxs.AddWindow(self.cbWrkDay, 0, border=0, flag=wx.ALIGN_CENTRE)
        bxs.AddWindow(self.cbNoWrkDay, 0, border=0, flag=wx.ALIGN_CENTRE)
        fgs.AddSizer(bxs, 0, border=0, flag=wx.EXPAND|wx.LEFT|wx.RIGHT)
        
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        id=wx.NewId()
        self.cbDec = wx.lib.buttons.GenBitmapButton(ID=id,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbDec',
              parent=self, style=wx.BU_AUTODRAW)
        self.cbDec.SetBitmapLabel(getLeftBitmap())
        #self.cbDec.SetBitmapSelected(getLeftBitmap())
        self.cbDec.Bind(wx.EVT_BUTTON,self.OnDecButton,self.cbDec)
        
        id=wx.NewId()
        self.cbInc = wx.lib.buttons.GenBitmapButton(ID=id,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbInc',
              parent=self, style=wx.BU_AUTODRAW)
        self.cbInc.SetBitmapLabel(getRightBitmap())
        #self.cbInc.SetBitmapSelected(getRightBitmap())
        self.cbInc.Bind(wx.EVT_BUTTON,self.OnIncButton,self.cbInc)
        
        self.chcMonth = wx.Choice(choices=[], id=wxID_VTDRAWCANVASPANELCHCMONTH,
              name=u'chcMonth', parent=self, pos=wx.Point(117, 0),
              size=wx.Size(130, 21), style=0)
        self.chcMonth.Bind(wx.EVT_CHOICE, self.OnUpdateDateChc,self.chcMonth)
        
        #bxs.AddWindow(self.spYear, 3, border=0, flag=wx.EXPAND)
        bxs.AddWindow(self.cbDec, 0, border=0, flag=wx.ALIGN_CENTRE)
        bxs.AddWindow(self.chcMonth, 4, border=0, flag=wx.EXPAND)
        bxs.AddWindow(self.cbInc, 0, border=0, flag=wx.ALIGN_CENTRE)
        
        self.scwDrawing = wx.ScrolledWindow(id=wxID_VTDRAWCANVASPANELSCWDRAWING,
              name=u'scwDrawing', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(100, 100), style=wx.SUNKEN_BORDER | wx.HSCROLL | wx.VSCROLL)
        #self.scwDrawing.SetConstraints(LayoutAnchors(self.scwDrawing, True,
        #      True, True, True))
        
        #fgs.AddSizer(self.spYear, 0, border=0, flag=wx.EXPAND|wx.LEFT|wx.RIGHT)
        fgs.AddSizer(bxs, 0, border=0, flag=wx.EXPAND|wx.LEFT|wx.RIGHT)
        fgs.AddWindow(self.scwDrawing, 0, border=4, flag=wx.EXPAND|wx.TOP)
        self.SetSizer(fgs)
        EVT_VTDRAW_CANVAS_ITEM_ACTIVATED(self,self.OnActivatedElement)
        EVT_VTDRAW_CANVAS_ITEM_DEACTIVATED(self,self.OnDeActivatedElement)
        #EVT_VTDRAW_CANVAS_ITEM_SELECTED(self,self.OnSelectedElement)
        #EVT_VTDRAW_CANVAS_ITEM_UNSELECTED(self,self.OnUnSelectedElement)
    def __init__(self, parent, 
                id=wx.NewId(), pos=wx.DefaultPosition, size=wx.Size(100, 50), 
                style=wx.TAB_TRAVERSAL, name=u'vtTimeInputDay',
                draw_grid=False,is_gui_wx=True,
                grid_x=20,grid_y=20):
        max=(7,7)
        vtDrawCanvasPanel.__init__(self, parent, 
                id=wx.NewId(), pos=pos, size=size, 
                style=style, name=name,
                draw_grid=draw_grid,
                is_gui_wx=is_gui_wx,
                max_x=max[0],max_y=max[1],
                grid_x=grid_x,grid_y=grid_y)
        self.SetActivateOnSelect(True)
        #self.bFlip=True
        
        self.dVal=None
        self.funcVal=None
        self.iHrLimit=8.0#24.0
        self.blkLastAct=None
        
        self.lblBlks=[]
        
        obj=vtDrawCanvasObjectBase()
        self.SetConfig(obj,obj.DRV_WX,max=max,
                    grid=(grid_x,grid_y))
        
        #calendar.setfirstweekday(2)
        
        self.__updateLang__()
        self.dt=vtTime.vtDateTime(True)
        self.dt.SetDay(1)
        self.__initDayLblBlks__()
        self.__showDate__()
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        
    def __initDayLblBlks__(self):
        self.lblBlks=[]
        self.lblBlks=[]
        iFirst=calendar._firstweekday
        for i in range(7):
            sTag=calendar.day_abbr[i]
            j=i-iFirst
            if j<0:
                j+=7
            blk=vtDrawCanvasText(id=-1,name='day_%02d'%i,
                        text='%s'%sTag,
                        x=j,y=0,w=1,h=1,layer=i)
            self.lblBlks.append(blk)
        self.objs=[]
        for i in range(0,31):
            blk=vtDrawCanvasTextSelectable(id=-1,name='day_%02d'%i,
                        text='%02d'%(i+1),
                        x=0,y=0,w=1,h=1,layer=0)
            blk.SetVisible(False)
            self.objs.append(blk)
        
    def __clearInt__(self):
        pass
    def __updateDayLblBlks__(self):
        iDayStart,iDayMax=calendar.monthrange(self.dt.GetYear(),self.dt.GetMonth())
        iMon=self.dt.GetMonth()
        iFirst=calendar._firstweekday
        iDayMax-=1
        self.blkLastAct=None
        
        self.BeginBatch()
        try:
            for i in range(0,31):
                self.ShowObj(self.objs[i],False)
                self.objs[i].SetActive(False)
        except:
            pass
        iDay=iDayStart
        iDay-=iFirst
        if iDay<0:
            iDay+=7
        iWk=0
        for i in range(0,31):
            blk=self.objs[i]
            blk.SetX(iDay)
            blk.SetY(iWk+1)
            blk.SetLayer((iDay+iFirst)%7)
            if i<=iDayMax:
                blk.SetVisible(True)
                try:
                    self.ShowObj(blk,False)
                except:
                    pass
            else:
                blk.SetVisible(False)
            iDay+=1
            if iDay==7:
                iDay=0
                iWk+=1
        self.EndBatch()
        
    def __drawObjects__(self,dc,printing):
        # overload me
        #vtLog.vtLogCallDepth(None,'',verbose=1)
        try:
            self.drvInfo[self.objBase.DRV_WX]['dc']=dc
            self.drvInfo[self.objBase.DRV_WX]['printing']=printing
        except:
            #traceback.print_exc()
            return
        if 1==0:
            if self.thdDraw.IsRunning():
                return
            self.drvInfo[self.objBase.DRV_WX]['act_layer']=-1
            self.thdDraw.Start(self)
        else:
            #self.semDraw.acquire()
            #vtLog.CallStack('')
            self.drvInfo[self.objBase.DRV_WX]['act_layer']=-1
            for blk in self.lblBlks:
                blk.__draw__(self)
            for o in self.objs:
                o.__draw__(self)
            self.drvInfo[self.objBase.DRV_WX]['dc']=None
            #self.semDraw.release()
        #vtDrawCanvasPanel.__drawGrid__(self,dc,printing)
        #vtDrawCanvasPanel.__drawObjects__(self,dc,printing)
        #for blk in self.lblBlks:
        #    blk.__draw__(self)
    def __updateLang__(self):
        self.chcMonth.Clear()
        for i in range(1,13):
            self.chcMonth.Append(calendar.month_name[i])
    def __showDate__(self):
        self.spYear.SetValue(self.dt.GetYear())
        self.chcMonth.SetSelection(self.dt.GetMonth()-1)
        #mrng=calendar.monthrange(self.dt.GetYear(),self.dt.GetMonth())
        self.__updateDayLblBlks__()
    def OnDecButton(self,evt):
        iYear=self.dt.GetYear()
        iMon=self.dt.GetMonth()
        iMon-=1
        if iMon<1:
            iYear-=1
            iMon=12
        self.dt.SetDate(iYear,iMon,1)
        self.__showDate__()
        self.ShowAll()
    def OnIncButton(self,evt):
        iYear=self.dt.GetYear()
        iMon=self.dt.GetMonth()
        iMon+=1
        if iMon>12:
            iYear+=1
            iMon=1
        self.dt.SetDate(iYear,iMon,1)
        self.__showDate__()
        self.ShowAll()
        #evt.Skip()
    def OnUpdateDate(self,evt):
        try:
            iYear=int(self.spYear.GetValue())
            self.dt.SetYear(iYear)
            self.__showDate__()
            self.ShowAll()
        except:
            pass
        evt.Skip()
    def OnUpdateDateChc(self,evt):
        self.dt.SetMonth(self.chcMonth.GetSelection()+1)
        self.__showDate__()
        self.ShowAll()
        evt.Skip()
    def __findFirstDay__(self,blk):
        idx=self.objs.index(self.blkLastAct)
        while blk.GetX()>0:
            idx-=1
            if idx<0:
                return 0
            blk=self.objs[idx]
        return idx
    def OnWrkDayButton(self,evt):
        if self.blkLastAct is None:
            return
        idx=self.__findFirstDay__(self.blkLastAct)
        if idx<0:
            return
        self.BeginBatch()
        try:
            for i in range(5):
                blk=self.objs[i+idx]
                blk.SetActive(True)
                self.ShowObj(blk,True)
        except:
            pass
        self.EndBatch()
    def OnNoWrkDayButton(self,evt):
        if self.blkLastAct is None:
            return
        idx=self.__findFirstDay__(self.blkLastAct)
        if idx<0:
            return
        self.BeginBatch()
        try:
            for i in range(5,7):
                blk=self.objs[i+idx]
                blk.SetActive(True)
                self.ShowObj(blk,True)
        except:
            pass
        self.EndBatch()
    def OnActivatedElement(self,evt):
        self.blkLastAct=evt.GetBlock()
        evt.Skip()
    def OnDeActivatedElement(self,evt):
        self.blkLastAct=evt.GetBlock()
        evt.Skip()
        
        
