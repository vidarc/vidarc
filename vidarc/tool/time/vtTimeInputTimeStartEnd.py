#----------------------------------------------------------------------------
# Name:         vtTimeInputTimeStartEnd.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060214
# CVS-ID:       $Id: vtTimeInputTimeStartEnd.py,v 1.6 2008/03/26 23:12:09 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
#from vidarc.tool.draw.vtDrawCanvasPanel import vtDrawCanvasPanel
from vidarc.tool.time.vtTimeInputTime import *
from vidarc.tool.draw.vtDrawCanvasPanel import EVT_VTDRAW_CANVAS_ITEM_ACTIVATED
from vidarc.tool.draw.vtDrawCanvasPanel import EVT_VTDRAW_CANVAS_ITEM_DEACTIVATED
from vidarc.tool.draw.vtDrawCanvasObjectBase import *
from vidarc.tool.draw.vtDrawCanvasGroup import *
from vidarc.tool.draw.vtDrawCanvasRectangle import *
from vidarc.tool.draw.vtDrawCanvasRectangleFilled import *
from vidarc.tool.draw.vtDrawCanvasText import *
import vidarc.tool.log.vtLog as vtLog

wxEVT_VTTIME_INPUT_TIMESTARTEND_CHANGED=wx.NewEventType()
vEVT_VTTIME_INPUT_TIMESTARTEND_CHANGED=wx.PyEventBinder(wxEVT_VTTIME_INPUT_TIMESTARTEND_CHANGED,1)
def EVT_VTTIME_INPUT_TIMESTARTEND_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTTIME_INPUT_TIMESTARTEND_CHANGED,func)
class vtTimeInputTimeStartEndChanged(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VTTIME_INPUT_TIMESTARTEND_CHANGED(<widget_name>, self.OnItemSel)
    """

    def __init__(self,obj,iHrS,iMnS,iHrE,iMnE):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTTIME_INPUT_TIMESTARTEND_CHANGED)
        self.iHrS=iHrS
        self.iHrE=iHrE
        if iMnS<0:
            iMnS=-1
        if iMnE<0:
            iMnE=-1
        self.iMnS=iMnS
        self.iMnE=iMnE
    def GetCanvas(self):
        return self.obj
    def GetHourStart(self):
        return self.iHrS
    def GetHourEnd(self):
        return self.iHrE
    def GetMinStart(self):
        return self.iMnS
    def GetMinEnd(self):
        return self.iMnE
    def GetTime(self):
        return self.iHrS,self.iMnS,self.iHrE,self.iMnE
    def GetTimeStart(self):
        return self.iHrS,self.iMnS
    def GetTimeEnd(self):
        return self.iHrE,self.iMnE
    def IsValid(self):
        if self.iHrS<0 or self.iHrE<0 or self.iMnS<0 or self.iMnE<0:
            return False
        return True
    
    

class vtTimeHrLL(vtDrawCanvasGroup):
    def __init__(self,id=-1,name='',text='',x=0,y=0,w=0,h=0,layer=0):
        vtDrawCanvasGroup.__init__(self,id,name,x,y,w,h,layer)
        obj=vtDrawCanvasRectangle(id=-1,name=name+'_rct',
                        x=0,y=0,w=w,h=h,layer=layer)
        self.Add(obj)
        obj=vtDrawCanvasRectangleFilled(id=-1,name=name+'_rcf',
                        x=0,y=0,w=w,h=h,layer=layer)
        obj.SetVisible(False)
        self.Add(obj)
        obj=vtDrawCanvasText(id=-1,name=name+'_txt',text=text,
                        x=0,y=0,w=w,h=h,layer=layer)
        self.Add(obj)
    def __draw__(self,canvas,aX=0,aY=0,aW=-1,aH=-1,iDriver=-1):
        if self.bActive:
            self.objs[1].SetVisible(True)
            #self.objs[1].SetLayer(self.GetLayer())
            self.objs[2].SetLayer(-1)
        else:
            self.objs[1].SetVisible(False)
            self.objs[2].SetLayer(self.GetLayer())
            
        vtDrawCanvasGroup.__draw__(self,canvas,aX,aY,aW,aH,iDriver)
        
        #if self.bActive:
        #    self.objs[1].SetFocus(True)
            #self.objs[1].SetLayer(-1)
        #    self.objs[1].__drawSelect__(canvas,aX,aY,aW,aH,iDriver)
        #    self.objs[1].SetFocus(False)
        #else:
        #    self.objs[0].SetActive(True)
        #    self.objs[0].__drawSelect__(canvas,aX,aY,aW,aH,iDriver)
        self.SetSelected(False)
        if self.bActive:
            iLayer=self.GetLayer()
            self.SetLayer(-1)
        self.__drawSelect__(canvas,aX,aY,aW,aH,iDriver)
        if self.bActive:
            self.SetLayer(iLayer)
        #self.txt.__draw__(canvas,aX,aY,aW,aH,iDriver)
    
class vtTimeHrO(vtDrawCanvasRectangle):
    def __init__(self,id=-1,name='',text='',x=0,y=0,w=0,h=0,layer=0):
        vtDrawCanvasRectangle.__init__(self,id,name,x,y,w,h,layer)
        self.txt=vtDrawCanvasText(id=-1,name=name+'_txt',text=text,
                        x=x,y=y,w=w,h=h,layer=layer)
    def __draw__(self,canvas,aX=0,aY=0,aW=-1,aH=-1,iDriver=-1):
        print 'draw'
        vtDrawCanvasRectangle.__draw__(self,canvas,aX,aY,aW,aH,iDriver)
        self.txt.__draw__(canvas,aX,aY,aW,aH,iDriver)
        
class vtTimeInputTimeStartEnd(vtTimeInputTime):
    VERBOSE=0
    DFT_COLOR=[
        (  0, 136,   0),
        (  0, 108,   0),
        (136,   0,   0),
        (108,   0,   0),
        (128, 128, 128),
        ( 31, 156, 127),
        (178, 100, 178)
        ]

    def __init__(self, parent, 
                id=wx.NewId(), pos=wx.DefaultPosition, size=wx.Size(100, 50), 
                style=wx.TAB_TRAVERSAL, name=u'vtTimeInputTime',
                draw_grid=True,small=False,blks_per_line=12,
                start_hour=0, end_hour=24,min_tolerance=5):
        self.iHrStart=start_hour
        self.iHrEnd=end_hour
        self.iHrS=-1
        self.iMnS=-1
        self.iHrE=-1
        self.iMnE=-1
        self.iMinTol=min_tolerance
        self.iMin=60/min_tolerance
        
        self.zTime=vtTime()
        self.zTime.Clear()
        self.zTimeEnd=vtTime()
        self.zTimeEnd.Clear()
        self.zTimeDiff=vtTime()
        self.zTimeDiff.Clear()
        
        self.tagName='startendtime'
        self.tagNameEnd=None
        #vtTimeInputTime.__init__(self,parent,id,pos,size,style,name,
        #            draw_grid,
        #            small=small,
        #            start_hour=start_hour, end_hour=end_hour,min_tolerance=min_tolerance)
        vtDrawCanvasPanel.__init__(self,parent,id,pos,size,style,name,
                    draw_grid,max_x=12,max_y=4)
        
        self.SetActivateOnSelect(True)
        
        obj=vtDrawCanvasObjectBase()
        self.RegisterObject(vtTimeHr())
        x=0
        y=0
        for i in range(self.iHrStart,self.iHrEnd):
            self.Add(vtTimeHr(id=i,name='hr_%02d'%i,text='%02d'%i,
                            x=x,y=y,w=1,h=1,layer=0))
            x+=1
            if x%blks_per_line==0:
                x=0
                y+=1
        if x!=0:
            y+=1
        x=0
        for i in range(0,self.iMin):
            self.Add(vtTimeHr(id=100+i,name='mn_%02d'%i,text=':%02d'%(i*self.iMinTol),
                            x=x,y=y,w=1,h=1,layer=1))
            x+=1
            if x%blks_per_line==0:
                x=0
                y+=1
        if x!=0:
            y+=1
        y+=1
        x=0
        for i in range(self.iHrStart,self.iHrEnd):
            self.Add(vtTimeHr(id=200+i,name='hr_%02d'%i,text='%02d'%i,
                            x=x,y=y,w=1,h=1,layer=2))
            x+=1
            if x%blks_per_line==0:
                x=0
                y+=1
        i=24
        if x==0:
            x=blks_per_line
            y-=1
        self.Add(vtTimeHr(id=200+i,name='hr_%02d'%i,text='%02d'%i,
                            x=x,y=y,w=1,h=1,layer=2))
        x+=1
        if x!=0:
            y+=1
        x=0
        for i in range(0,self.iMin):
            self.Add(vtTimeHr(id=300+i,name='mn_%02d'%i,text=':%02d'%(i*self.iMinTol),
                            x=x,y=y,w=1,h=1,layer=3))
            x+=1
            if x%blks_per_line==0:
                x=0
                y+=1
        if x==0:
            y-=1
            
        if small:
            grid=(15,15)
            i=blks_per_line+1
        else:
            grid=(20,20)
            i=blks_per_line+1
        self.SetConfig(obj,obj.DRV_WX,max=(i,y+1),
                    grid=grid)
        EVT_VTDRAW_CANVAS_ITEM_ACTIVATED(self,self.OnActivated)
        EVT_VTDRAW_CANVAS_ITEM_DEACTIVATED(self,self.OnDeActivated)
    def __showHrMnBlk(self,ofs,id,flag):
        if self.VERBOSE:
            print 'showHrMnBlk',ofs,id,flag
        start=[]
        blk=self.FindByAttr(ofs+id,start,'id')
        blk.SetActive(flag)
        self.ShowObj(blk,False)
    def OnActivated(self,evt):
        blk=evt.GetBlock()
        sName=blk.GetName()
        bMnS=bMnE=False
        if self.VERBOSE:
            vtLog.CallStack('')
            print sName
            print blk.GetId()
            print 'iHrS:',self.iHrS,'iMnS',self.iMnS,'iHrE',self.iHrE,'iMnE',self.iMnE
        self.BeginBatch()
        bHrS=False
        bHrE=False
        bMnS=False
        bMnE=False
        blkId=blk.GetId()
        if blkId<100:
            bHrS=True
        elif blkId<200:
            bMnS=True
        elif blkId<300:
            bHrE=True
        else:
            bMnE=True
        start=[]
        if bHrS:
            iHr=blkId
            if self.iHrS>=0:
                self.__showHrMnBlk(0,self.iHrS,False)
            self.iHrS=iHr
        if bHrE:
            iHr=blkId-200
            if self.iHrE>=0:
                self.__showHrMnBlk(200,self.iHrE,False)
            self.iHrE=iHr
            if self.iHrE==24:
                self.iMnE=0
        if bMnE:
            iMn=blkId-300
            if self.iMnE>=0:
                self.__showHrMnBlk(300,self.iMnE,False)
            self.iMnE=iMn
            if self.iHrE==24:
                self.iMnE=0
            #if self.iMnS==-1:
            #    self.iMnS=self.iMnE
            #    bMnS=True
            #    blkId=iMn+100
        if bMnS:
            iMn=blkId-100
            if self.iMnS>=0:
                self.__showHrMnBlk(100,self.iMnS,False)
            self.iMnS=iMn
        if self.VERBOSE:
            vtLog.CallStack('')
            print self.iHrS,self.iMnS,self.iHrE,self.iMnE
        if self.iHrS==self.iHrE:
            if self.iMnE>=0:
                if self.iMnS>self.iMnE:
                    self.iMnS,self.iMnE=self.iMnE,self.iMnS
                    bMnS=bMnE=True
        elif self.iHrS>self.iHrE:
            if self.iHrE>=0:
                self.iHrS,self.iHrE=self.iHrE,self.iHrS
        for ofs in [0,200]:
            for id in range(self.iHrStart,self.iHrEnd):
                self.__showHrMnBlk(ofs,id,False)
        for ofs in [100,300]:
            for id in range(0,self.iMin):
                self.__showHrMnBlk(ofs,id,False)
                
        if self.iHrS>=0:
            self.__showHrMnBlk(0,self.iHrS,True)
        if self.iMnS>=0:
            self.__showHrMnBlk(100,self.iMnS,True)
        if self.iHrE>=0:
            self.__showHrMnBlk(200,self.iHrE,True)
        if self.iMnE>=0:
            self.__showHrMnBlk(300,self.iMnE,True)
        self.EndBatch()
        if self.iMnE>=0:
            iMinE=(self.iMnE)*self.iMinTol
            if iMinE>=60:
                iHr=self.iHrE#+1
                iMinE=0
            else:
                iHr=self.iHrE
        else:
            iMinE=-1
            iHr=self.iHrE
        self.zTime.Set(self.iHrS,self.iMnS*self.iMinTol)
        self.zTimeEnd.Set(iHr,iMinE)
        self.zTime.CalcDiff(self.zTimeEnd,self.zTimeDiff)
        wx.PostEvent(self,vtTimeInputTimeStartEndChanged(self,
                                self.iHrS,self.iMnS*self.iMinTol,
                                iHr,iMinE))
        return
        if self.iHrS==self.iHrE:
            if self.iMnS>self.iMnE:
                self.iMnS,self.iMnE=self.iMnE,self.iMnS
                bMnS=bMnE=True
            if self.iHrS==self.iHrE:
                bMnS=bMnE=True
        if bMnS:
            start=[]
            for id in range(0,self.iMin):
                blk=self.FindByAttr(100+id,start,'id')
                if blk is not None:
                    if self.iMnS<0:
                        blk.SetActive(False)
                    elif self.iHrS!=self.iHrE:
                        blk.SetActive(id>=self.iMnS)
                    else:
                        blk.SetActive(id>=self.iMnS and id<=self.iMnE)
                    self.ShowObj(blk,False)
        if bMnE:
            start=[]
            for id in range(0,self.iMin):
                blk=self.FindByAttr(200+id,start,'id')
                if blk is not None:
                    if self.iMnE<0:
                        blk.SetActive(False)
                    elif self.iHrS!=self.iHrE:
                        blk.SetActive(id<=self.iMnE)
                    else:
                        blk.SetActive(id<=self.iMnE and id>=self.iMnS)
                    self.ShowObj(blk,False)
        if self.iMnE>=0:
            iMinE=(self.iMnE+1)*self.iMinTol
            if iMinE>=60:
                iHr=self.iHrE+1
                iMinE=0
            else:
                iHr=self.iHrE
        else:
            iMinE=-1
            iHr=self.iHrE
        self.zTime.Set(self.iHrS,self.iMnS*self.iMinTol)
        self.zTimeEnd.Set(iHr,iMinE)
        self.zTime.CalcDiff(self.zTimeEnd,self.zTimeDiff)
        wx.PostEvent(self,vtTimeInputTimeStartEndChanged(self,
                                self.iHrS,self.iMnS*self.iMinTol,
                                iHr,iMinE))
        self.EndBatch()
        evt.Skip()
    def OnDeActivated(self,evt):
        blk=evt.GetBlock()
        bHrS=False
        bHrE=False
        bMnS=False
        bMnE=False
        blkId=blk.GetId()
        if blkId<100:
            bHrS=True
        elif blkId<200:
            bMnS=True
        elif blkId<300:
            bHrE=True
        else:
            bMnE=True
        sName=blk.GetName()
        bMnS=bMnE=False
        if self.VERBOSE:
            vtLog.CallStack('')
            print sName
            print blk.GetId()
            print self.iHrS,self.iMnS,self.iHrE,self.iMnE
        self.BeginBatch()
        if bHrS:
            self.__showHrMnBlk(0,self.iHrS,False)
            self.iHrS=-1
        if bHrE:
            self.__showHrMnBlk(200,self.iHrE,False)
            self.iHrE=-1
        if bMnS:
            self.__showHrMnBlk(100,self.iMnS,False)
            self.iMnS=-1
        if bMnE:
            self.__showHrMnBlk(3000,self.iMnE,False)
            self.iMnE=-1
        if self.VERBOSE:
            #vtLog.CallStack('')
            #print sName
            #print blk.GetId()
            print self.iHrS,self.iMnS,self.iHrE,self.iMnE
        if self.iHrS==self.iHrE:
                if self.iMnS>self.iMnE or self.iMnS==-1:
                    self.iMnS=self.iMnE=-1
                if self.iMnS>self.iMnE:
                    self.iMnS,self.iMnE=self.iMnE,self.iMnS
                #if self.iMnS<self.iMnE:
                #    self.iMnS=self.iMnE
                #if self.iMnE<self.iMnS:
                #    self.iMnE=self.iMnS
                bMnS=bMnE=True
        if self.iMnE>=0:
            iMinE=(self.iMnE)*self.iMinTol
            if iMinE>=60:
                iHr=self.iHrE
                iMinE=0
            else:
                iHr=self.iHrE
        else:
            iMinE=-1
            iHr=self.iHrE
        self.zTime.Set(self.iHrS,self.iMnS*self.iMinTol)
        self.zTimeEnd.Set(iHr,iMinE)
        self.zTime.CalcDiff(self.zTimeEnd,self.zTimeDiff)
        wx.PostEvent(self,vtTimeInputTimeStartEndChanged(self,
                                self.iHrS,self.iMnS*self.iMinTol,
                                iHr,iMinE))
        self.EndBatch()
        evt.Skip()
    def ClearInt(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.zTime.Clear()
        self.zTimeEnd.Clear()
        self.zTimeDiff.Clear()
        self.iHrS,self.iMnS=-1,-1
        self.iHrE,self.iMnE=-1,-1
        self.__clearHrMn__()
    def __clearHrMn__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        start=[]
        self.BeginBatch()
        for id in range(self.iHrStart,self.iHrEnd):
            blk=self.FindByAttr(id,start,'id')
            if blk is not None:
                blk.SetActive(False)
                self.ShowObj(blk,False)
        for id in range(self.iMin):
            blk=self.FindByAttr(100+id,start,'id')
            if blk is not None:
                blk.SetActive(False)
                self.ShowObj(blk,False)
        for id in range(self.iHrStart,self.iHrEnd):
            blk=self.FindByAttr(200+id,start,'id')
            if blk is not None:
                blk.SetActive(False)
                self.ShowObj(blk,False)
        for id in range(self.iMin):
            blk=self.FindByAttr(300+id,start,'id')
            if blk is not None:
                blk.SetActive(False)
                self.ShowObj(blk,False)
        self.EndBatch()
    def __selectHrMn__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.__clearHrMn__()
        self.zTimeDiff.Clear()
        if self.zTime.IsValid():
            self.zTime.Round(self.iMinTol)
            self.iHrS=self.zTime.GetHour()
            self.iMnS=self.zTime.GetMinute()/self.iMinTol
        else:
            self.iHrS,self.iMnS=-1,-1
        if self.zTimeEnd.IsValid():
            self.zTimeEnd.Round(self.iMinTol)
            self.iHrE=self.zTimeEnd.GetHour()
            self.iMnE=self.zTimeEnd.GetMinute()/self.iMinTol
        else:
            self.iHrE,self.iMnE=-1,-1
        if self.VERBOSE:
            vtLog.CallStack('')
            print self.iHrS,self.iMnS,self.iHrE,self.iMnE
        if not self.zTime.IsValid() and not self.zTimeEnd.IsValid():
            return
        if not self.zTimeEnd.IsValid():
            if 1:
                self.iHrE=self.iHrS
                self.iMnE=self.iMnS+1
                #print self.iHrS,self.iMnS,self.iHrE,self.iMnE
                self.zTimeEnd.SetHour(self.iHrE)
                self.zTimeEnd.SetMinute(self.iMnE*self.iMinTol)
            else:
                self.BeginBatch()
                start=[]
                blk=self.FindByAttr(self.iHrS,start,'id')
                if blk is not None:
                    blk.SetActive(True)
                    self.ShowObj(blk,False)
                blk=self.FindByAttr(100+self.iMnS,start,'id')
                if blk is not None:
                    blk.SetActive(True)
                    self.ShowObj(blk,False)
                self.EndBatch()
                return
        if self.iHrS>self.iHrE:
            self.iHrS,self.iHrE=self.iHrE,self.iHrS
            self.iMnS,self.iMnE=self.iMnE,self.iMnS
            self.zTime.SetHour(self.iHrS)
            self.zTimeEnd.SetHour(self.iHrE)
            self.zTime.SetMinute(self.iMnS*self.iMinTol)
            self.zTimeEnd.SetMinute(self.iMnE*self.iMinTol)
        #self.iMnE-=1
        if self.iHrE>=0:
            if self.iMnE<0:
                self.iMnE=self.iMin
                self.iHrE-=1
        if self.iHrS==self.iHrE:
            if self.iMnS>self.iMnE:
                self.iMnS,self.iMnE=self.iMnE,self.iMnS
                self.zTime.SetMinute(self.iMnS*self.iMinTol)
                self.zTimeEnd.SetMinute(self.iMnE*self.iMinTol)
        self.zTime.CalcDiff(self.zTimeEnd,self.zTimeDiff)
        self.BeginBatch()
        start=[]
        blk=self.FindByAttr(self.iHrS,start,'id')
        if blk is not None:
            blk.SetActive(True)
            self.ShowObj(blk,False)
        blk=self.FindByAttr(100+self.iMnS,start,'id')
        if blk is not None:
            blk.SetActive(True)
            self.ShowObj(blk,False)
        blk=self.FindByAttr(200+self.iHrE,start,'id')
        if blk is not None:
            blk.SetActive(True)
            self.ShowObj(blk,False)
        blk=self.FindByAttr(300+self.iMnE,start,'id')
        if blk is not None:
            blk.SetActive(True)
            self.ShowObj(blk,False)
        self.EndBatch()

    def SetTagNames(self,tagName,tagNameEnd=None):
        self.tagName=tagName
        self.tagNameEnd=tagNameEnd
    def SetNode(self,node):
        self.ClearInt()
        self.node=node
        if self.doc is not None:
            sVal=self.doc.getNodeText(self.node,self.tagName)
            if self.tagNameEnd is not None:
                self.zTime.SetStr(sVal)
                self.iHrS=self.zTime.GetHour()
                self.iMnS=self.zTime.GetMinute()
                sVal=self.doc.getNodeText(self.node,self.tagNameEnd)
                self.zTimeEnd.SetStr(sVal)
                self.iHrE=self.zTimeEnd.GetHour()
                self.iMnE=self.zTimeEnd.GetMinute()
            else:
                strs=sVal.split(' ')
                try:
                    self.zTime.SetStr(strs[0])
                    self.zTimeEnd.SetStr(strs[1])
                except:
                    pass
            self.__selectHrMn__()
    def GetNode(self,node=None):
        if node is None:
            node=self.node
        if self.doc is not None:
            if node is not None:
                sVal=self.zTime.GetStr()
                if self.tagNameEnd is not None:
                    self.doc.setNodeText(self.node,self.tagName,sVal)
                    sVal=self.zTimeEnd.GetStr()
                    self.doc.setNodeText(self.node,self.tagNameEnd,sVal)
                else:
                    sValE=self.zTimeEnd.GetStr()
                    self.doc.setNodeText(self.node,self.tagName,sVal+ ' '+sValE)
    def SetValue(self,sVal):
        self.zTime.Clear()
        self.zTimeEnd.Clear()
        try:
            strs=sVal.split(' ')
            self.zTime.SetStr(strs[0],':')
            self.zTimeEnd.SetStr(strs[1],':')
        except:
            vtLog.vtLngCurWX(vtLog.ERROR,'parse error:%s'%sVal,self)
            vtLog.vtLngTB(self.GetName())
        self.iHrS=self.zTime.GetHour()
        self.iMnS=self.zTime.GetMinute()
        self.iHrE=self.zTimeEnd.GetHour()
        self.iMnE=self.zTimeEnd.GetMinute()
        self.__selectHrMn__()
    def GetValue(self):
        return self.zTime.GetStr(':')+' '+self.zTimeEnd.GetStr(':')+' ('+self.zTime.GetDiffStr(self.zTimeEnd,':')+')'
    def Set(self,hourS,minS,secS,hourE,minE,secE):
        self.zTime.Set(hourS,minS,secS)
        self.zTimeEnd.Set(hourE,minE,secE)
        self.__selectHrMn__()
    def SetStart(self,hour,min=0,sec=0):
        self.zTime.Set(hour,min,sec)
        self.__selectHrMn__()
    def SetHourStart(self,hour):
        self.zTime.SetHour(hour)
        self.__selectHrMn__()
    def SetMinuteStart(self,min):
        self.zTime.SetMinute(min)
        self.__selectHrMn__()
    def SetSecondStart(self,sec):
        self.zTime.SetSecond(sec)
        self.__selectHrMn__()
    def SetEnd(self,hour,min=0,sec=0):
        self.zTimeEnd.Set(hour,min,sec)
        self.__selectHrMn__()
    def SetHourEnd(self,hour):
        self.zTimeEnd.SetHour(hour)
        self.__selectHrMn__()
    def SetMinuteEnd(self,min):
        self.zTimeEnd.SetMinute(min)
        self.__selectHrMn__()
    def SetSecondEnd(self,sec):
        self.zTimeEnd.SetSecond(sec)
        self.__selectHrMn__()
    def GetHourEnd(self):
        return self.zTimeEnd.GetHour()
    def GetMinuteEnd(self):
        return self.zTimeEnd.GetMinute()
    def GetSecondEnd(self):
        return self.zTimeEnd.GetSecond()
    def GetHourDiff(self):
        return self.zTimeDiff.GetHour()
    def GetMinuteDiff(self):
        return self.zTimeDiff.GetMinute()
    def GetSecondDiff(self):
        return self.zTimeDiff.GetSecond()
    def Get(self,obj):
        obj.SetTimeStr(self.zTime.GetStr(':'))
        obj.SetHourEnd(self.zTimeEnd.GetHour())
        obj.SetMinuteEnd(self.zTimeEnd.GetMinute())
        obj.SetSecondEnd(self.zTimeEnd.GetSecond())
        

