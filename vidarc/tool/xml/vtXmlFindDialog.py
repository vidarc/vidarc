#Boa:Frame:vtXmlFindDialog

import wx

from vidarc.tool.xml.vtXmlFindPanel import *
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

def create(parent,name):
    return vtXmlFindDialog(parent,name)

[wxID_VTXMLFINDDIALOG] = [wx.NewId() for _init_ctrls in range(1)]

class vtXmlFindDialog(wx.Frame):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VTXMLFINDDIALOG,
              name=u'vtXmlFindDialog', parent=prnt, pos=wx.Point(66, 66),
              size=wx.Size(349, 329), style=wx.DEFAULT_FRAME_STYLE,
              title=_(u'vtXml Find'))
        self.SetClientSize(wx.Size(341, 302))

    def __init__(self, parent,name):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self._init_ctrls(parent)
        
        icon= wx.EmptyIcon()#wx.Icon(16,16)
        icon.CopyFromBitmap(vtArt.getBitmap(vtArt.Search))
        self.SetIcon(icon)
        
        self.pn=vtXmlFindPanel(parent=self,id=-1,name=name+':vtXmlFilterPanel',
                    pos=(0,0),size=(341, 302),style=0)
        EVT_VTXML_FIND_PANEL_FOUND(self.pn,self.OnFound)
        EVT_VTXML_FIND_PANEL_CANCELED(self.pn,self.OnCanceled)
    def OnFound(self,evt):
        wx.PostEvent(self,vtXmlFindPanelFound(self,evt.GetNode()))
        evt.Skip()
    def OnCanceled(self,evt):
        self.Show(False)
    def SetDoc(self,doc,bNet=False):
        self.pn.SetDoc(doc,bNet)
    def UpdateFilter(self):
        self.pn.UpdateFilter()
    def SetDocHum(self,doc,bNet=False):
        self.pn.SetDocHum(doc,bNet)
    def SetNode(self,node):
        self.pn.SetNode(node)
    def GetNode(self,node=None):
        self.pn.GetNode(node)
        
    
