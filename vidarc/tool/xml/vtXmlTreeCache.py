#----------------------------------------------------------------------------
# Name:         vtXmlTreeCache.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtXmlTreeCache.py,v 1.6 2008/04/26 23:09:01 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

VERBOSE=0
if VERBOSE:
    import vidarc.tool.log.vtLog as vtLog

class vtXmlTreeCache:
    def __init__(self,doc,idName='id'):
        self.doc=doc
        self.idName=idName
        self.cache=None
        self.Clear()
    def __del__(self):
        del self.cache
    def Clear(self):
        self.bValid=True
        if self.cache is not None:
            del self.cache
        self.cache={}
        self.tree=None
        self.verbose=0
    def SetDoc(self,doc):
        self.doc=doc
    def __build__(self,ti):
        #vtLog.vtLngCurCls(vtLog.INFO,'',self)
        node=self.tree.GetPyData(ti)
        if node is not None:
            try:
                if self.verbose>0:
                    print '__build',self.idName,self.doc.getAttribute(node,self.idName)
                if self.tree.TREE_DATA_ID:
                    pass
                else:
                    id=long(self.doc.getAttribute(node,self.idName))
                if id<-1:
                    return
                self.cache[id]=ti
                if self.verbose>1:
                    print '__build',len(self.cache.keys())
            except:
                pass
        triChild=self.tree.GetFirstChild(ti)
        while triChild[0].IsOk():
            self.__build__(triChild[0])
            triChild=self.tree.GetNextChild(ti,triChild[1])
    def Build(self,tree=None):
        #vtLog.vtLngCurCls(vtLog.INFO,'',self)
        if tree is not None:
            self.tree=tree
            self.bValid=False
            self.cache={}
        if self.tree is None:
            return
        try:
            self.__build__(ti)
        except:
            pass
        self.bValid=True
        
    def Add(self,ti):
        if self.tree is not None:
            self.__build__(ti)
    def AddID(self,ti,id):
        try:
            #vtLog.vtLngCurCls(vtLog.DEBUG,'id:%08d'%(id),self)
            id=long(id)
            self.cache[id]=ti
        except:
            pass
    def DelID(self,id):
        try:
            #vtLog.vtLngCurCls(vtLog.DEBUG,'id:%08d'%(id),self)
            id=long(id)
            del self.cache[id]
        except:
            pass
    def GetTreeItem(self,node):
        try:
            id=self.doc.getKeyNum(node)
            #vtLog.vtLngCurCls(vtLog.DEBUG,'id:%08d'%(id),self)
            return self.GetTreeItemById(id)
        except:
            return None
    def GetTreeItemById(self,id):
        if self.bValid==False:
            self.Build()
        if self.bValid:
            id=long(id)
            if VERBOSE:
                keys=self.cache.keys()
                keys.sort()
                vtLog.vtLngCurCls(vtLog.DEBUG,'id:%08d;keys:%s'%(id,
                    vtLog.pformat(keys)),self)
            #vtLog.CallStack('')
            #print self.cache.keys()
            if id in self.cache:
                ti=self.cache[id]
                return ti
            else:
                return None
        return None
    def HasId(self,id):
        if self.bValid==False:
            self.Build()
        if self.bValid:
            id=long(id)
            if id in self.cache:
                return 1
        return 0
    def SetInvalid(self):
        #self.bValid=False
        pass
