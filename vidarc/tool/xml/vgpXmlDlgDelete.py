#Boa:Dialog:vgpXmlDlgDelete

from wxPython.wx import *
from wxPython.lib.buttons import *
from wxPython.wx import wxImageFromStream, wxBitmapFromImage
from wxPython.wx import wxEmptyIcon
import cStringIO


def create(parent):
    return vgpXmlDlgDelete(parent)

[wxID_VGPXMLDLGDELETE, wxID_VGPXMLDLGDELETEGCBBCANCEL, 
 wxID_VGPXMLDLGDELETEGCBBOK, wxID_VGPXMLDLGDELETELBLNAME, 
 wxID_VGPXMLDLGDELETETXTNAME, 
] = map(lambda _init_ctrls: wxNewId(), range(5))

#----------------------------------------------------------------------
def getOkData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00nIDATx\x9c\xa5\x93\xc9\r\xc0 \x0c\x04\x17D\x01\xd0^\n\xa4=\xd2\x01yD\
\x96L\x08\xe0c\xdf\xecx\x0c"\x00\xbd\xc3\x91h)\xe5Z\x90k\x01\x00$m\x91r_M\
\x07\xa02\x15)\xa2\x15Ve1`U\x16\x01\xf8\xde&\xc0)[\xc0n\xf7\x01\xc0\xdf\xd5e\
`\x01E\xe0U\xfcjJ\xf4\'\x03:\xac\xb1\x98.\x91O<M\xff\x05h\x13\xbc\xdf\xf9\
\x01\x97y&\xadH\xfc\xe0%\x00\x00\x00\x00IEND\xaeB`\x82' 

def getOkBitmap():
    return wxBitmapFromImage(getOkImage())

def getOkImage():
    stream = cStringIO.StringIO(getOkData())
    return wxImageFromStream(stream)

#----------------------------------------------------------------------
def getCancelData():
    return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00lIDATx\x9c\xad\x93\xc1\r\xc00\x08\x03M\xb7\xe8\xfe\xc3e\x0c\xfa\x8bB\
bC$\xca\x9b;\x01N\x0cpG\xa3\x9e\x0e\xfc\xaf`\xc00`%\xb0\xf7M\xc1\x0b\x9f\r\
\x19\xbc\xf6\x06A%a\xf0!P\x12\x05\x03\x80\xa9\x18\xf7)\x18L'`\x80\x82S\x01[\
\xe1Z\xb0\xee\\\xa5s\x08\xd8\xc12I\x10d\xd7V\x92\xf0\x12\x15\x9cId\x8c\xb7\
\xd5\xfeL\x1f$\x07+\xb8\xd6Q\x0bp\x00\x00\x00\x00IEND\xaeB`\x82" 

def getCancelBitmap():
    return wxBitmapFromImage(getCancelImage())

def getCancelImage():
    stream = cStringIO.StringIO(getCancelData())
    return wxImageFromStream(stream)


class vgpXmlDlgDelete(wxDialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wxDialog.__init__(self, id=wxID_VGPXMLDLGDELETE,
              name=u'vgpXmlDlgDelete', parent=prnt, pos=wxPoint(334, 226),
              size=wxSize(226, 136), style=wxDEFAULT_DIALOG_STYLE,
              title=u'vgp xml-Node Delete')
        self.SetClientSize(wxSize(218, 109))

        self.lblName = wxStaticText(id=wxID_VGPXMLDLGDELETELBLNAME,
              label=u'name', name=u'lblName', parent=self, pos=wxPoint(8, 40),
              size=wxSize(26, 13), style=0)

        self.txtName = wxTextCtrl(id=wxID_VGPXMLDLGDELETETXTNAME,
              name=u'txtName', parent=self, pos=wxPoint(80, 40),
              size=wxSize(100, 21), style=0, value=u'')
        self.txtName.Enable(False)

        self.gcbbOk = wxGenBitmapTextButton(ID=wxID_VGPXMLDLGDELETEGCBBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'gcbbOk', parent=self,
              pos=wxPoint(16, 72), size=wxSize(76, 30), style=0)
        EVT_BUTTON(self.gcbbOk, wxID_VGPXMLDLGDELETEGCBBOK, self.OnGcbbOkButton)

        self.gcbbCancel = wxGenBitmapTextButton(ID=wxID_VGPXMLDLGDELETEGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel', name=u'gcbbCancel',
              parent=self, pos=wxPoint(136, 72), size=wxSize(76, 30), style=0)
        EVT_BUTTON(self.gcbbCancel, wxID_VGPXMLDLGDELETEGCBBCANCEL,
              self.OnGcbbCancelButton)

    def __init__(self, parent):
        self._init_ctrls(parent)

        img=getOkBitmap()
        mask=wxMask(img,wxBLUE)
        img.SetMask(mask)
        self.gcbbOk.SetBitmapLabel(img)
        
        img=getCancelBitmap()
        mask=wxMask(img,wxBLUE)
        img.SetMask(mask)
        self.gcbbCancel.SetBitmapLabel(img)

    def OnGcbbOkButton(self, event):
        self.EndModal(1)
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
    def SetNodeName(self,name):
        self.txtCurName.SetValue(name)
        self.txtNewName.SetValue(name)
    def GetNodeName(self):
        return self.txtNewName.GetValue()
