#----------------------------------------------------------------------------
# Name:         vtXmlThread.py
# Purpose:
# Author:       Walter Obweger
#
# Created:      20060423
# CVS-ID:       $Id: vtXmlThread.py,v 1.5 2007/07/29 07:47:44 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.vtThread import vtThread
import vidarc.tool.log.vtLog as vtLog

class vtXmlThread(vtThread):
    def Clear(self):
        pass
    def Do(self,id,rec,bChilds,func,*args,**kwargs):
        if self.verbose:
            vtLog.vtLngCur(vtLog.DEBUG,'id:%08d rec:%d childs:%d;running:%d keepgoing:%d'%(id,rec,bChilds,self.running,self.keepGoing),
                            self.GetOrigin())
        self._acquire()
        if self.running:
            self.qSched.put((id,rec,bChilds,func,args,kwargs))
            return
        self.qSched.put((id,rec,bChilds,func,args,kwargs))
        self.keepGoing=self.running=True
        self._release()
        self.__runThread__()
    def __procElem__(self,node,*args):
        try:
            self.par.doc.acquire()
            # handle node 
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.doc.release()
    def _doProc(self,t):
        try:
            id,rec,bChilds,func,args,kwargs=t
            if self.verbose:
                vtLog.vtLngCur(vtLog.DEBUG,'id:%08d rec:%d childs:%d;'%(id,rec,bChilds),
                                self.GetOrigin())
            self.par.doc.acquire('dom')
            if self.verbose:
                vtLog.vtLngCur(vtLog.DEBUG,'doc acquired;process started',
                                self.GetOrigin())
            try:
                node=self.par.doc.getNodeByIdNum(id)
                if bChilds:
                    self.par.doc.procChildsKeysRec(rec,node,func,*args,**kwargs)
                else:
                    self.par.doc.procKeysRec(rec,node,func,*args,**kwargs)
            except:
                vtLog.vtLngTB(self.GetOrigin())
            self.par.doc.release('dom')
            if self.verbose:
                vtLog.vtLngCur(vtLog.DEBUG,'doc released;process finished',
                                self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
