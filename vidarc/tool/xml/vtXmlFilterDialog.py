#Boa:Frame:vtXmlFilterDialog

import wx

from vidarc.tool.xml.vtXmlFilterPanel import *
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

def create(parent):
    return vtXmlFilterDialog(parent)

[wxID_VTXMLFILTERDIALOG] = [wx.NewId() for _init_ctrls in range(1)]

class vtXmlFilterDialog(wx.Frame):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VTXMLFILTERDIALOG,
              name=u'vtXmlFilterDialog', parent=prnt, pos=wx.Point(66, 66),
              size=wx.Size(330, 250), style=wx.DEFAULT_FRAME_STYLE,
              title=u'vXml Filter')
        self.SetClientSize(wx.Size(322, 223))

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self._init_ctrls(parent)
        
        icon= wx.EmptyIcon()
        icon.CopyFromBitmap(vtArt.getBitmap(vtArt.Filter))
        self.SetIcon(icon)
        
        self.pn=vtXmlFilterPanel(parent=self,id=-1,name='vtXmlFilterPanel',
                    pos=(0,0),size=(322,223),style=0)
        EVT_VTXML_FILTER_PANEL_APPLIED(self.pn,self.OnApplied)
        EVT_VTXML_FILTER_PANEL_CANCELED(self.pn,self.OnCanceled)
    def OnApplied(self,evt):
        self.Show(False)
    def OnCanceled(self,evt):
        self.Show(False)
    def SetDoc(self,doc,bNet=False):
        self.pn.SetDoc(doc,bNet)
    def SetDocHum(self,doc,bNet=False):
        self.pn.SetDocHum(doc,bNet)
    def SetNode(self,node):
        self.pn.SetNode(node)
    def GetNode(self,node=None):
        self.pn.GetNode(node)
        
    
