#Boa:Dialog:vtXmlNodeBaseAddDialog
#----------------------------------------------------------------------------
# Name:         vtXmlNodeBaseAddDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtXmlNodeBaseAddDialog.py,v 1.7 2012/11/19 05:02:34 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
from vidarc.tool.xml.vtXmlNodeAddDialog import *
import vidarc.tool.lang.vtLgBase as vtLgBase

def create(parent):
    return vtXmlNodeBaseAddDialog(parent)

[wxID_VTXMLNODEBASEADDDIALOG, wxID_VTXMLNODEBASEADDDIALOGCBAPPLY, 
 wxID_VTXMLNODEBASEADDDIALOGCBCANCEL, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vtXmlNodeBaseAddDialog(wx.Dialog,vtXmlNodeAddDialog):
    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsBt, (1, 0), border=4,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER, span=(1, 1))

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=0, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_gbsData_Items(self.gbsData)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt,name,title):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTXMLNODEBASEADDDIALOG,
              name=name, parent=prnt, pos=wx.Point(176,
              176), size=wx.Size(400, 250), style=wx.DEFAULT_DIALOG_STYLE|wx.RESIZE_BORDER,
              title=title)
        self.SetClientSize(wx.Size(392, 223))

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLNODEBASEADDDIALOGCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'), name=u'cbApply',
              parent=self, pos=wx.Point(80, 192), size=wx.Size(96, 30),
              style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VTXMLNODEBASEADDDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLNODEBASEADDDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(216, 192),
              size=wx.Size(96, 30), style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VTXMLNODEBASEADDDIALOGCBCANCEL)

        self._init_sizers()

    def __init__(self, parent,title,pnCls,name='vtXmlNodeBase',
                pos=None,sz=None,
                pnPos=wx.DefaultPosition,pnSz=wx.DefaultSize,pnStyle=0,
                pnName='pn'):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self._init_ctrls(parent,u''.join([name,'AddDialog']),title)
        vtXmlNodeAddDialog.__init__(self)
        try:
            self.gbsData.AddGrowableCol(0)
            self.gbsData.AddGrowableRow(0)
            self.pn=pnCls(self,id=wx.NewId(),pos=pnPos,size=pnSz,
                        style=pnStyle,name=pnName)
            iFlag=wx.BOTTOM | wx.TOP | wx.LEFT | wx.RIGHT | wx.EXPAND
            if hasattr(self.pn,'GetWid'):
                wid=self.pn.GetWid()
            else:
                wid=self.pn
            self.gbsData.AddWindow(wid, (0, 0), border=4,flag=iFlag, span=(1, 1))
        except:
            self.__logTB__()
        if sz is not None:
            self.SetSize(sz)
        else:
            #self.Fit()
            self.gbsData.Layout()
            self.gbsData.Fit(self)
        if pos is not None:
            self.Move(pos)
