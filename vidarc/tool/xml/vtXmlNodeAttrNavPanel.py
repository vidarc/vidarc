#----------------------------------------------------------------------
# Name:         vtXmlNodeAttrNavPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120909
# CVS-ID:       $Id: vtXmlNodeAttrNavPanel.py,v 1.4 2015/10/20 09:11:09 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    from vGuiPanelWX import vGuiPanelWX
    import vidarc.tool.lang.vtLgBase as vtLgBase
    from vidarc.tool.input.vtInputTreeExt import vtInputTreeExt
    from vidarc.tool.xml.vtXmlNodeTagInfoPanel import vtXmlNodeTagInfoPanel
    from vidarc.tool.xml.vtXmlNodeAttrPanel import vtXmlNodeAttrPanel
    
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vtXmlNodeAttrNavPanel(vGuiPanelWX):
    VERBOSE=0
    def __initCtrlWid__(self,*args,**kwargs):
        global _
        _=vtLgBase.assignPluginLang('vidarc.tool')
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        #self.SetVerbose(__name__,iVerbose=-1,kwargs=kwargs)
        lWidExt=[
            #(vtXmlNodeAttrPanel,       1,0,{'name':'pnAttr',
            #                                    },None),
            ]
        #lWid=[
        #    ]
        #lWidget={
        lWid={
            'slwNav':{
                'size':(200,50),
                'lWid':[
                    (vtInputTreeExt,        (0,0),(1,1),{'name':'trAttr','tip':'tree for description',
                                                        'mod':False,#'bSuppressNetNotify':True,
                                                        'lPropNode':1,'lPropAttr':1,'iOrientation':0,
                                                        #'lWid':lWidExt,
                                                        },None),
                ],
                'lGrowRow':[(0,1),],
                'lGrowCol':[(0,1)],
            },
            'pnData':{
                'size':(100,200),
                
                'lWid':[
                    (vtXmlNodeTagInfoPanel, (0,0),(1,1),{'name':'pnTag',
                                                        },None),
                    (vtXmlNodeAttrPanel,    (1,0),(1,1),{'name':'pnAttr','bAutoApply':False,
                                                        },None),
                ],
                'lGrowRow':[(1,1),],
                'lGrowCol':[(0,1)],
            },
            }
        return lWid
        #vtXmlNodeGuiPanel.__initCtrl__(self,lWid=lWid)
        vGuiPanelWX.__initCtrl__(self,lWid=lWid)
        lGrowRow=[(0,2),(2,1),]
        lGrowCol=[(0,1),(1,4),]
        #lWidAdd=kwargs.get('lWid',[])
        #kwargs['lWid']=lWidget
        ##vGuiPanelWX.__init__(self,*args,**kwargs)
    def __initProperties__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            
            self.trAttr=self.pnNav.trAttr
            self.pnTag=self.pnData.pnTag
            self.trAttr.BindEvent('cmd',self.OnTreeCmd)
            #self.pnDesc=self.trDesc.pnDesc
            self.pnAttr=self.pnData.pnAttr
        except:
            self.__logTB__()
    def __initCls__(self,*args,**kwargs):
        self.SetVerbose(__name__,iVerbose=VERBOSE,kwargs=kwargs)
    def OnApply(self,evt):
        try:
            self.__logDebug__(''%())
            self.pnAttr.Apply()
        except:
            self.__logTB__()
    def OnCancel(self,evt):
        try:
            self.__logDebug__(''%())
            self.pnAttr.Cancel()
        except:
            self.__logTB__()
    def OnTreeCmd(self,evt):
        try:
            sCmd=evt.GetCmd()
            sData=evt.GetData()
            self.__logDebug__({'sCmd':sCmd,'sData':sData})
            if sCmd=='sel':
                self.pnTag.SetNodeById(sData)
                self.pnAttr.SetNodeById(sData)
        except:
            self.__logTB__()
    def SetDoc(self,doc,bNet=False):
        try:
            self.__logDebug__('doc is None=%d'%(doc is None))
            self.trAttr.SetDoc(doc,bNet=bNet)
            self.pnTag.SetDoc(doc,bNet=bNet)
            self.pnAttr.SetDoc(doc,bNet=bNet)
        except:
            self.__logTB__()
    def ClearDoc(self):
        try:
            self.__logDebug__(''%())
            self.trAttr.ClearDoc()
            self.pnTag.ClearDoc()
            self.pnAttr.ClearDoc()
        except:
            self.__logTB__()
    def SetNodeByID(self,sID):
        try:
            self.__logDebug__('sID:%r'%(sID))
            node=self.doc.getNodeById(sID)
            self.trAttr.SetNode(node)
        except:
            self.__logTB__()
    def SetNode(self,node,sID):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            self.__logDebug__('sID:%r'%(sID))
            self.trAttr.SetNode(node)
        except:
            self.__logTB__()
    def dummy(self):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            pass
        except:
            self.__logTB__()
    def IsShutDownActive(self):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            if self.trAttr.HasDoc():
                return False
            return False
        except:
            self.__logTB__()
    def IsShutDownFinished(self):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            if self.trAttr.HasDoc():
                return False
            return True
        except:
            self.__logTB__()
    def ShutDown(self):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            #self.ClearDoc()
            #self.trAttr.ClearDoc()
            self.trAttr.SetDoc(None,bNet=True)
            self.pnTag.SetDoc(None,bNet=True)
            self.pnAttr.SetDoc(None,bNet=True)
        except:
            self.__logTB__()
    def DoShutDown(self):
        try:
            if self.IsMainThread()==False:
                return
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            self.Close()
        except:
            self.__logTB__()
