#Boa:Dialog:vtXmlImportOfficeDialog
#----------------------------------------------------------------------------
# Name:         vtImportOfficeDialog.py
# Purpose:      import xml from StarOffice or MS Office
#
# Author:       Walter Obweger
#
# Created:      20060505
# CVS-ID:       $Id: vtXmlImportOfficeDialog.py,v 1.11 2010/03/03 02:16:18 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputCalcPanel
import wx.gizmos
import wx.lib.intctrl
import wx.lib.buttons
import sys,fnmatch

from vidarc.tool.xml.vtXmlDom import vtXmlDom
from vidarc.tool.xml.vtXmlNodePanel import *
from vidarc.tool.xml.vtXmlFindThread import *
from vidarc.tool.xml.vtXmlFilterType import vtXmlFilterCreate
import vidarc.tool.lang.vtLgBase as vtLgBase

import os,string,time,copy,types
import thread,traceback
from vidarc.tool.office.vtOfficeSCalc import *

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt

def create(parent):
    return vtXmlImportOfficeDialog(parent)

[wxID_VTXMLIMPORTOFFICEDIALOG, wxID_VTXMLIMPORTOFFICEDIALOGCBADDINTERNAL, 
 wxID_VTXMLIMPORTOFFICEDIALOGCBADDTMPL, wxID_VTXMLIMPORTOFFICEDIALOGCBCLEAR, 
 wxID_VTXMLIMPORTOFFICEDIALOGCBDEL, wxID_VTXMLIMPORTOFFICEDIALOGCBDELINTERNAL, 
 wxID_VTXMLIMPORTOFFICEDIALOGCBDELTMPL, 
 wxID_VTXMLIMPORTOFFICEDIALOGCBREADTMPL, 
 wxID_VTXMLIMPORTOFFICEDIALOGCBSAVETMPL, wxID_VTXMLIMPORTOFFICEDIALOGCBSET, 
 wxID_VTXMLIMPORTOFFICEDIALOGCHCAUTOOPEN, wxID_VTXMLIMPORTOFFICEDIALOGCHCKEY, 
 wxID_VTXMLIMPORTOFFICEDIALOGCHCMSOFF, wxID_VTXMLIMPORTOFFICEDIALOGCHCSHT, 
 wxID_VTXMLIMPORTOFFICEDIALOGCHCSTAROFF, wxID_VTXMLIMPORTOFFICEDIALOGCHCTMPL, 
 wxID_VTXMLIMPORTOFFICEDIALOGCHCVALUE, 
 wxID_VTXMLIMPORTOFFICEDIALOGGCBBBROWSEFN, 
 wxID_VTXMLIMPORTOFFICEDIALOGGCBBBUILD, 
 wxID_VTXMLIMPORTOFFICEDIALOGGCBBCANCEL, 
 wxID_VTXMLIMPORTOFFICEDIALOGGCBBIMPORT, wxID_VTXMLIMPORTOFFICEDIALOGGCBBOK, 
 wxID_VTXMLIMPORTOFFICEDIALOGGPROC, wxID_VTXMLIMPORTOFFICEDIALOGINTCOLEND, 
 wxID_VTXMLIMPORTOFFICEDIALOGINTCOLSTART, 
 wxID_VTXMLIMPORTOFFICEDIALOGINTROWSTART, 
 wxID_VTXMLIMPORTOFFICEDIALOGINTROWSTOP, wxID_VTXMLIMPORTOFFICEDIALOGLBLCMP, 
 wxID_VTXMLIMPORTOFFICEDIALOGLBLCOL, wxID_VTXMLIMPORTOFFICEDIALOGLBLEST, 
 wxID_VTXMLIMPORTOFFICEDIALOGLBLFN, wxID_VTXMLIMPORTOFFICEDIALOGLBLNODECOUNT, 
 wxID_VTXMLIMPORTOFFICEDIALOGLBLROW, wxID_VTXMLIMPORTOFFICEDIALOGLBLROWBUILD, 
 wxID_VTXMLIMPORTOFFICEDIALOGLBLSHT, wxID_VTXMLIMPORTOFFICEDIALOGLBLTME, 
 wxID_VTXMLIMPORTOFFICEDIALOGLBLTMPL, wxID_VTXMLIMPORTOFFICEDIALOGLBLTMPLNAME, 
 wxID_VTXMLIMPORTOFFICEDIALOGLSTINPRAW, 
 wxID_VTXMLIMPORTOFFICEDIALOGLSTINTERNAL, wxID_VTXMLIMPORTOFFICEDIALOGLSTROWS, 
 wxID_VTXMLIMPORTOFFICEDIALOGNBIMPORT, wxID_VTXMLIMPORTOFFICEDIALOGPNBUILD, 
 wxID_VTXMLIMPORTOFFICEDIALOGPNGEN, wxID_VTXMLIMPORTOFFICEDIALOGPNINPUT, 
 wxID_VTXMLIMPORTOFFICEDIALOGPNMAP, wxID_VTXMLIMPORTOFFICEDIALOGSPROW, 
 wxID_VTXMLIMPORTOFFICEDIALOGTRLSTMAP, wxID_VTXMLIMPORTOFFICEDIALOGTXTCOMP, 
 wxID_VTXMLIMPORTOFFICEDIALOGTXTFN, wxID_VTXMLIMPORTOFFICEDIALOGTXTNAME, 
 wxID_VTXMLIMPORTOFFICEDIALOGTXTNODECOUNT, 
 wxID_VTXMLIMPORTOFFICEDIALOGTXTTMELAPSED, 
 wxID_VTXMLIMPORTOFFICEDIALOGTXTTMEST, 
 wxID_VTXMLIMPORTOFFICEDIALOGTXTTMPLNAME, wxID_VTXMLIMPORTOFFICEDIALOGVICALC, 
] = [wx.NewId() for _init_ctrls in range(56)]

wxEVT_IMPORT_OFFICE_THREAD_FINSHED=wx.NewEventType()
def EVT_IMPORT_OFFICE_THREAD_FINSHED(win,func):
    win.Connect(-1,-1,wxEVT_IMPORT_OFFICE_THREAD_FINSHED,func)
class wxImportOfficeThreadFinshed(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_IMPORT_OFFICE_THREAD_FINSHED(<widget_name>, self.OnItemSel)
    """

    def __init__(self,appl):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_IMPORT_OFFICE_THREAD_FINSHED)
        self.appl=appl
    def GetAppl(self):
        return self.appl

wxEVT_IMPORT_OFFICE_THREAD_FINSHED_READ=wx.NewEventType()
def EVT_IMPORT_OFFICE_THREAD_FINSHED_READ(win,func):
    win.Connect(-1,-1,wxEVT_IMPORT_OFFICE_THREAD_FINSHED_READ,func)
class wxImportOfficeThreadFinshedRead(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_IMPORT_OFFICE_THREAD_FINSHED_READ(<widget_name>, self.OnItemSel)
    """

    def __init__(self,infos):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_IMPORT_OFFICE_THREAD_FINSHED_READ)
        self.infos=infos
    def GetInfos(self):
        return self.infos

wxEVT_IMPORT_OFFICE_PROGRESS=wx.NewEventType()
def EVT_IMPORT_OFFICE_PROGRESS(win,func):
    win.Connect(-1,-1,wxEVT_IMPORT_OFFICE_PROGRESS,func)
class wxImportOfficeProgress(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_IMPORT_OFFICE_PROGRESS(<widget_name>, self.OnItemSel)
    """

    def __init__(self,sElapsed,sEstimate,iVal,iCount):
        wx.PyEvent.__init__(self)
        self.elapsed=sElapsed
        self.estimate=sEstimate
        self.val=iVal
        self.count=iCount
        self.SetEventType(wxEVT_IMPORT_OFFICE_PROGRESS)
    def GetElapsed(self):
        return self.elapsed
    def GetEstimate(self):
        return self.estimate
    def GetValue(self):
        return self.val
    def GetCount(self):
        return self.count

wxEVT_IMPORT_XML_THREAD_FINSHED=wx.NewEventType()
def EVT_IMPORT_XML_THREAD_FINSHED(win,func):
    win.Connect(-1,-1,wxEVT_IMPORT_XML_THREAD_FINSHED,func)
class wxImportXmlThreadFinshed(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_IMPORT_XML_THREAD_FINSHED(<widget_name>, self.OnItemSel)
    """

    def __init__(self,appl):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_IMPORT_XML_THREAD_FINSHED)
        self.appl=appl
    def GetAppl(self):
        return self.appl

wxEVT_IMPORT_XML_THREAD_FINSHED=wx.NewEventType()
def EVT_IMPORT_XML_THREAD_FINSHED(win,func):
    win.Connect(-1,-1,wxEVT_IMPORT_XML_THREAD_FINSHED,func)
class wxImportXmlThreadFinshed(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_IMPORT_XML_THREAD_FINSHED(<widget_name>, self.OnItemSel)
    """

    def __init__(self,infos):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_IMPORT_XML_THREAD_FINSHED)
        self.infos=infos
    def GetInfos(self):
        return self.infos

wxEVT_IMPORT_XML_THREAD_PROGRESS=wx.NewEventType()
def EVT_IMPORT_XML_THREAD_PROGRESS(win,func):
    win.Connect(-1,-1,wxEVT_IMPORT_XML_THREAD_PROGRESS,func)
class wxImportXmlThreadProgress(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_IMPORT_XML_THREAD_PROGRESS(<widget_name>, self.OnItemSel)
    """

    def __init__(self,sElapsed,sEstimate,iVal,iCount):
        wx.PyEvent.__init__(self)
        self.elapsed=sElapsed
        self.estimate=sEstimate
        self.val=iVal
        self.count=iCount
        self.SetEventType(wxEVT_IMPORT_XML_THREAD_PROGRESS)
    def GetElapsed(self):
        return self.elapsed
    def GetEstimate(self):
        return self.estimate
    def GetValue(self):
        return self.val
    def GetCount(self):
        return self.count

class thdOpenFile:
    def __init__(self,par):
        self.parent=par
        self.running=False
    def SetFN(self,appl,fn,bStartImport):
        self.appl=appl
        self.fn=fn
        self.bStartImport=bStartImport
        self.Start()
    def Start(self):
        self.keepGoing = self.running = True
        thread.start_new_thread(self.Run, ())

    def Stop(self):
        self.keepGoing = False

    def IsRunning(self):
        return self.running

    def Run(self):
        self.appl.openFile(self.fn)
        wx.PostEvent(self.parent,wxImportOfficeThreadFinshed(self.appl))
        
class thdReadData:
    def __init__(self,par,verbose=0):
        self.parent=par
        self.running=False
        self.verbose=verbose
    def ReadData(self,fn,sheet,iRowStart,iRowEnd,iColStart,iColEnd):
        self.fn=fn
        self.sheet=sheet
        self.iRowStart=iRowStart
        self.iRowEnd=iRowEnd
        self.iColStart=iColStart
        self.iColEnd=iColEnd
        self.infos=[]
        self.Start()
    def Start(self):
        self.keepGoing = self.running = True
        thread.start_new_thread(self.Run, ())

    def Stop(self):
        self.keepGoing = False

    def IsRunning(self):
        return self.running

    def Run(self):
        try:
            import pythoncom
            pythoncom.CoInitialize()
            self.appl=vtOfficeSCalc()
            #self.appl.setVisible(False)
            self.appl.openFile(self.fn)
            #self.appl.setVisible(False)
            #vtLog.vtLogCallDepth(self,'fn:%s'%self.fn,callstack=False)
            self.appl.selSheet(self.sheet)
            #vtLog.vtLogCallDepth(self,'sheet:%s'%(self.sheet),callstack=False)
            
            start=time.clock()
            self.infos=[]
            self.__startTime__()
            iCount=self.iRowEnd-self.iRowStart
            self.__showTime__(0,iCount)
            
            for i in range(self.iRowStart,self.iRowEnd):
                #vtLog.vtLogCallDepth(self,'row:%4d %d'%(i,self.iRowEnd),callstack=False)
                if self.keepGoing==False:
                    break
                self.infos.append(self.appl.getCellRange(i,self.iColStart,i,self.iColEnd)[0])
                #vtLog.vtLogCallDepth(self,'proc:%4d %d'%(i,iCount),callstack=False)
                sElapsed,sEstimate=self.__showTime__(i-self.iRowStart,iCount)
                #vtLog.vtLogCallDepth(self,'ela:%s %s'%(sElapsed,sEstimate),callstack=False)
                wx.PostEvent(self.parent,
                        wxImportOfficeProgress(sElapsed,sEstimate,i,iCount))
            self.appl.closeFile()
            self.appl.close()
            if self.keepGoing==False:
                self.infos=None
        except:
            self.infos=None
            traceback.print_exc()
        self.keepGoing = self.running = False
        wx.PostEvent(self.parent,wxImportOfficeThreadFinshedRead(self.infos))
    def __startTime__(self):
        self.zStart=time.clock()
    def __showTime__(self,act,count):
        def __calcTime__(zVal):
            zSec=zVal%60
            zMin=zVal/60
            zVal-=zSec
            zHr=zMin/24
            zMin=zMin%24
            return u'%d:%02d:%02d'%(zHr,zMin,zSec)
        zAct=time.clock()
        zDiff=zAct-self.zStart
        sElapsed=__calcTime__(zDiff)
        if act==0:
            sEstimate=''
        else:
            zDiff=zDiff*count/act
            sEstimate=__calcTime__(zDiff)
        return sElapsed,sEstimate

class thdImportData:
    def __init__(self,par,verbose=0):
        self.parent=par
        self.running=False
        self.verbose=verbose
        self.bNet=True
    def SetNetworking(self,flag):
        self.bNet=flag
    def Do(self,node,lRow):
        if self.IsRunning():
            return
        self.node=node
        if self.verbose:
            vtLog.CallStack('')
            print node
        self.lRow=lRow
        self.lLockMissed=[]
        self.lEditFlt=[]
        self.lAddFlt=[]
        self.thdFind=vtXmlFindThread('importFlt',self.node,self.parent.doc)
        self.Start()
    def Start(self):
        self.bWait2FoundFin=False
        self.bWait2Locked=False
        self.keepGoing = self.running = True
        thread.start_new_thread(self.Run, ())
    def Stop(self):
        self.keepGoing = False
    def IsRunning(self):
        return self.running
    def Run(self):
        try:
            start=time.clock()
            self.doc=self.parent.doc
            self.dInt=self.parent.dInt
            self.dMap=self.parent.dMap
            self.dKey=self.parent.dKey
            self.dVal=self.parent.dVal
            self.dFormel=self.parent.dFormel
            self.oCalc=self.parent.oCalc
            self.infos=self.parent.infos
            self.dNodesCreated={}
            self.dNodes2Proc={}
            self.__startTime__()
            iCount=len(self.lRow)
            self.__showTime__(0,iCount)
            keys=self.dInt.keys()
            keys.sort()
            if self.verbose:
                vtLog.CallStack('')
                vtLog.vtLngCurCls(vtLog.DEBUG,'map:%s;key:%s;val:%s'%\
                        (vtLog.pformat(self.dMap),vtLog.pformat(self.dKey),vtLog.pformat(self.dVal)),self)
            iTime2Update=0
            for iRow in self.lRow:
                i=0
                row=self.infos[iRow]
                for key in keys:
                    val=self.dInt[key]
                    sVal=''
                    iLen=len(val)
                    j=0
                    while (j<iLen) and (j>=0):
                        j,k=self.parent.__getColAdr__(val,j)
                        if j!=-1:
                            iCol=int(val[j:k])
                            sVal+=row[iCol]
                            j=k+1
                    self.dVal[key]=sVal
                    i+=1
                for key in keys:
                    try:
                        l=self.dFormel[key]
                        if self.oCalc is not None:
                            sRes=self.oCalc.Calc(self.dVal[key],self.dVal,l)
                            self.dVal[key]=sRes
                    except:
                        pass
                flts={}
                for kk in self.dKey.keys():
                    o=self.doc.GetRegisteredNode(kk)
                    lFltTypes=o.GetAttrFilterTypes()
                    if self.verbose:
                        vtLog.vtLngCurCls(vtLog.DEBUG,'kk:%s;lFltType:%s'%(kk,vtLog.pformat(lFltTypes)),self)
                    ll=[]
                    for kAttr in self.dKey[kk].keys():
                        for tup in lFltTypes:
                            if kAttr==tup[0]:
                                flt=vtXmlFilterCreate(tup[1],flt=self.dVal[self.dKey[kk][kAttr]])
                                if flt is not None:
                                    ll.append((kAttr,flt))
                                    break
                    flts[kk]=ll
                if self.verbose:
                    vtLog.vtLngCurCls(vtLog.DEBUG,'flts:%s'%(vtLog.pformat(flts)),self)
                self.bWait2FoundFin=True
                self.bWait2Locked=False
                self.bWait2Created=False
                self.nodeFound=None
                #print 'wait2Found:%d wait2Locked:%d'%(self.bWait2FoundFin,self.bWait2Locked)
                self.thdFind.Find(flts,self.node,[],self.OnFound)
                #print 'wait2Found:%d wait2Locked:%d'%(self.bWait2FoundFin,self.bWait2Locked)
                if self.verbose:
                    #print 'row:%d'%iRow
                    #vtLog.pprint(self.dVal)
                    vtLog.vtLngCurCls(vtLog.DEBUG,'row:%d;dVal:%s'%(iRow,self.dVal),self)
                while ((self.bWait2FoundFin==True) or (self.bWait2Locked==True) or (self.bWait2Created==True)) and (self.keepGoing==True):
                    #if self.bWait2Locked:
                    #    pass
                    if iTime2Update==0:
                        sElapsed,sEstimate=self.__showTime__(iRow,iCount)
                        wx.PostEvent(self.parent,
                                wxImportXmlThreadProgress(sElapsed,sEstimate,iRow,iCount))
                    iTime2Update+=1
                    if iTime2Update>=10:
                        iTime2Update=0
                    time.sleep(0.1)
                time.sleep(0.1)
                #print 'wait2Found:%d wait2Locked:%d'%(self.bWait2FoundFin,self.bWait2Locked)
                if self.keepGoing==False:
                    break;
        except:
            vtLog.vtLngTB(self.parent.GetName()+'_'+self.__class__.__name__)
        self.keepGoing = self.running = False
        wx.PostEvent(self.parent,wxImportXmlThreadFinshed(self.infos))
    def walkMapVal(self,node,dMap,k,hier=[],bCheckAttr=True,bRecursive=True):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            #print dMap,k,hier
            #s=vtLog.pformat(dMap)
            #print s,type(s)
            #print node
            #print node.content
            #print node.content.encode('utf-8','replace')
            #vtLog.vtLngCur(vtLog.DEBUG,u';'.join([u'node:',node.content,#.encode('ascii','replace'),
            #            'hier:',hier,'tagName:',k,'attrs:',vtLog.pformat(dMap)]),
            #            self.parent.GetName()+'_'+self.__class__.__name__)
            vtLog.vtLngCurCls(vtLog.DEBUG,u'id:%s;hier:%s;tagName:%s;attrs:%s'%(self.doc.getKey(node),hier,k,vtLog.pformat(dMap)),self)
        if bCheckAttr:
            o=self.doc.GetRegisteredNode(k)
            dAttrs=dMap[k]['attr']
            lAttrs=dAttrs.keys()
            if len(lAttrs)>0:
                self.lNodes2Proc.append((self.doc.getKey(node),node,dAttrs,k))
        dSub=dMap[k]['sub']
        vtLog.pprint(dSub)
        for kSub in dSub.keys():
            c=self.doc.getChild(node,kSub)
            if c is None:
                #vtLog.vtLngCur(vtLog.ERROR,'FIXME;create child:%s'%kSub,self.parent.GetName()+'_'+self.__class__.__name__)
                o,c=self.doc.CreateNode(node,kSub,
                        self.setMapValByCreate,map=dSub)
                self.dNodesCreated[self.doc.getKey(c)]=(c,dSub,kSub)
            else:
                if bRecursive:
                    self.walkMapVal(c,dSub,kSub,hier+[kSub],lProc,lSubRes)
    def setMapValAttrs(self,node,dAttrs,k):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurCls(vtLog.DEBUG,'id:%s;attrs:%s;tagName:%s'%\
                        (self.doc.getKey(node),vtLog.pformat(dAttrs),k),self)
        o=self.doc.GetRegisteredNode(k)
        lAttrs=dAttrs.keys()
        for kAttr in lAttrs:
            sVal=self.dVal[dAttrs[kAttr][0]]
            #if self.verbose:
            #    print sVal
            i=kAttr.find('|')
            if i>=0:
                j=kAttr.find('=',i)
                if j>0:
                    o.SetAttrVal(node,kAttr[:i],sVal,kAttr[i+1:j],kAttr[j+1:])
                else:
                    vtLog.vtLngCurCls(vtLog.ERROR,'tag:%s,attr:%s'%(k,kAttr),self)
            else:
                o.Set(node,kAttr,sVal)
    def setMapVal(self,node,dMap,k):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurCls(vtLog.DEBUG,'id:%s;mpa:%s;tagName:%s'%\
                        (self.doc.getKey(node),vtLog.pformat(dMap),k),self)
        if self.verbose:
            vtLog.CallStack('')
            print node
            print 'tag',k
            print 'map'
            vtLog.pprint(dMap)
            print 'val'
            vtLog.pprint(self.dVal)
        o=self.doc.GetRegisteredNode(k)
        if k not in dMap:
            vtLog.vtLngCurCls(vtLog.WARN,'tag:%s not found int map'%(k),self)
            return
        dAttrs=dMap[k]['attr']
        lAttrs=dAttrs.keys()
        if len(lAttrs)>0:
            self.setMapValAttrs(node,dAttrs,k)
        dSub=dMap[k]['sub']
        
        if self.verbose:
            print 'sub'
            vtLog.pprint(dSub)
            print self.dKey
        for kSub in dSub.keys():
            if kSub in self.dKey:
                # 061007 wro dirty hack FIXME
                self.thdFind.nodeFound=None
                self.thdFind.checkNode(node,0)
                c=self.thdFind.nodeFound
            else:
                
                c=self.doc.getChild(node,kSub)
            if c is None:
                o,c=self.doc.CreateNode(node,kSub,
                        self.setMapValByCreate,map=dSub)
            else:
                self.setMapVal(c,dSub,kSub)
        self.doc.AlignNode(node)
    def setMapValByCreate(self,objReg,node,*args,**kwargs):
        try:
            if self.verbose:
                vtLog.CallStack('')
                print node
                print args
                print kwargs
            dMap=kwargs['map']
            k=objReg.GetTagName()
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurCls(vtLog.DEBUG,'tagName:%s;map:%s'%\
                        (k,vtLog.pformat(dMap[k])),self)
            self.setMapValAttrs(node,dMap[k]['attr'],k)
            #self.setMapVal(node,dMap,k)
        except:
            vtLog.vtLngTB(self.parent.GetName()+'_'+self.__class__.__name__)
    def Lock(self,flag):
        if self.verbose:
            vtLog.CallStack('')
            print flag
        try:
            if flag==False:
                # okay got lock, set values now
                sTagName=self.doc.getTagName(self.nodeFound)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurCls(vtLog.DEBUG,'id:%s;lock:%s;nodes2proc:%s;tagName:%s'%\
                            (self.doc.getKey(self.nodeFound),flag,self.lNodes2Proc,sTagName),self)
                tup=self.lNodes2Proc[0]
                self.setMapValAttrs(self.nodeFound,tup[-2],tup[-1])
                self.doc.doEdit(self.nodeFound)
                self.doc.endEdit(self.nodeFound)
                self.lNodes2Proc=self.lNodes2Proc[1:]
                #self.lNodes2Proc=self.lNodes2Proc[i:]
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurCls(vtLog.DEBUG,'id:%s;lock:%s;nodes2proc:%s;tagName:%s'%\
                            (self.doc.getKey(self.nodeFound),flag,self.lNodes2Proc,sTagName),self)
                if len(self.lNodes2Proc)>0:
                    if self.verbose:
                        vtLog.CallStack('')
                        vtLog.pprint(self.lNodes2Proc)
                    self.nodeFound=self.lNodes2Proc[0][1]#node
                    self.doc.startEdit(self.nodeFound)
                    self.bWait2Locked=True
                    return
                else:
                    self.lNodes2Proc=None
                #self.setMapVal(self.nodeFound,self.dMap,sTagName)
                #self.doc.doEdit(self.nodeFound)
                #self.doc.endEdit(self.nodeFound)
            else:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurCls(vtLog.DEBUG,'id:%s'%(self.doc.getKey(self.nodeFound)),self)
                self.lLockMissed.append(self.doc.getKey(self.nodeFound))
        except:
            self.lEditFlt.append(self.doc.getKey(self.nodeFound))
            vtLog.vtLngTB(self.parent.GetName()+'_'+self.__class__.__name__)
        self.bWait2Locked=False
        self.bWait2FoundFin=False
    def OnAddResponse(self,evt):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurCls(vtLog.DEBUG,'id:%s;idNew:%s'%(evt.GetID(),evt.GetNewID()),self)
        if self.verbose:
            vtLog.CallStack('')
            vtLog.pprint(self.dNodesCreated)
        try:
            id=evt.GetID()
            #print id,type(id)
            if self.dNodesCreated.has_key(id):
                node,dMap,k=self.dNodesCreated[id]
                self.walkMapVal(node,dMap,k,[],
                            bCheckAttr=False,bRecursive=False)
                del self.dNodesCreated[id]
            if len(self.dNodesCreated.keys())==0:
                self.bWait2Created=False
        except:
            self.lAddFlt.append(evt.GetID())
            vtLog.vtLngTB(self.parent.GetName()+'_'+self.__class__.__name__)
            self.lNodes2Proc=None
            self.bWait2Created=False
    def OnFound(self,node,*args,**kwargs):
        if self.verbose:
            vtLog.CallStack('')
            print node
            print args,kwargs
        if node is None:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurCls(vtLog.DEBUG,'add new node',self)
                    vtLog.vtLngCurCls(vtLog.DEBUG,'dMap:%s'%(vtLog.pformat(self.dMap)),self)
                # ok add new node
                for k in self.dMap.keys():
                    #FIXME compare
                    if vtLog.vtLngIsLogged(vtLog.INFO):
                        vtLog.vtLngCurCls(vtLog.INFO,'parId:%s;tag2create:%s'%\
                                (self.doc.getKey(self.node),k),self)
                    
                    o,node=self.doc.CreateNode(self.node,k,
                        self.setMapValByCreate,map=self.dMap)
                    self.dNodesCreated[self.doc.getKey(node)]=(node,self.dMap,k)
                    #self.setMapVal(node,self.dMap,k)
                    if self.verbose:
                        print node
                    self.bWait2Created=True
            except:
                self.lAddFlt.append(self.doc.getKey(self.nodeFound))
                vtLog.vtLngTB(self.parent.GetName()+'_'+self.__class__.__name__)
                self.bWait2FoundFin=False
        else:
            # node found, apply changes
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurCls(vtLog.DEBUG,'id:%s node found'%(self.doc.getKey(node)),self)
            if self.bNet:
                # request lock
                sTagName=self.doc.getTagName(node)
                lProc=[]
                lSubRes={}
                self.walkMapVal(node,self.dMap,sTagName,[],lProc,lSubRes)
                if self.verbose:
                    vtLog.CallStack('')
                    vtLog.pprint(lSubRes)
                if len(lSubRes)>0:
                    self.lNodes2Proc=lSubRes
                
                    self.nodeFound=self.lNodes2Proc[0][1]#node
                    self.doc.startEdit(node)
                    self.bWait2Locked=True
                else:
                    self.bWait2Locked=False
            else:
                # do now
                self.doc.startEdit(node)
                sTagName=self.doc.getTagName(node)
                self.setMapVal(node,self.dMap,sTagName)
                self.doc.doEdit(node)
                self.doc.endEdit(node)
        self.bWait2FoundFin=False
    def __startTime__(self):
        self.zStart=time.clock()
    def __showTime__(self,act,count):
        def __calcTime__(zVal):
            zSec=zVal%60
            zMin=zVal/60
            zVal-=zSec
            zHr=zMin/24
            zMin=zMin%24
            return u'%d:%02d:%02d'%(zHr,zMin,zSec)
        zAct=time.clock()
        zDiff=zAct-self.zStart
        sElapsed=__calcTime__(zDiff)
        if act==0:
            sEstimate=''
        else:
            zDiff=zDiff*count/act
            sEstimate=__calcTime__(zDiff)
        return sElapsed,sEstimate

class vtXmlImportOfficeDialog(wx.Dialog,vtXmlNodePanel,vtLog.vtLogOriginWX):
    VERBOSE=0
    def _init_coll_fgsGen_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)
        parent.AddGrowableCol(1)
        parent.AddGrowableCol(2)
        parent.AddGrowableCol(3)
        parent.AddGrowableCol(4)
        parent.AddGrowableCol(5)

    def _init_coll_fgsInpConv_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)
        parent.AddGrowableCol(0)
        parent.AddGrowableCol(1)

    def _init_coll_fgsMapOut_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsKey, 1, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsComp, 1, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddWindow(self.trlstMap, 0, border=4,
              flag=wx.BOTTOM | wx.EXPAND | wx.TOP)

    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_bxsTmplStored_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblTmplName, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.chcTmpl, 3, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbDelTmpl, 1, border=4, flag=wx.EXPAND | wx.LEFT)

    def _init_coll_bxsTmplAdd_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblTmpl, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtTmplName, 3, border=4,
              flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbAddTmpl, 1, border=4, flag=wx.EXPAND | wx.LEFT)

    def _init_coll_bxsMapInpBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbSet, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbDel, 1, border=4, flag=wx.EXPAND | wx.TOP)

    def _init_coll_fgsMap_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_fgsBuild_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)
        parent.AddGrowableCol(0)

    def _init_coll_fgsGen_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsTmplAdd, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsTmplStored, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsTmplChanged, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsFN, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsInput, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)

    def _init_coll_fgsMapOut_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_bxsIntCalc_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.txtName, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.viCalc, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsComp_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblCmp, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtComp, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fgsBuild_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblRowBuild, 1, border=4,
              flag=wx.EXPAND | wx.LEFT | wx.TOP)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.lstRows, 1, border=4,
              flag=wx.BOTTOM | wx.EXPAND | wx.TOP | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsMapGen, 0, border=4,
              flag=wx.RIGHT | wx.TOP | wx.LEFT)

    def _init_coll_fgsMap_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.fgsMapOut, 1, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsMapInpBt, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT)

    def _init_coll_fgsInpConv_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.spRow, 0, border=4,
              flag=wx.EXPAND | wx.TOP | wx.LEFT)
        parent.AddSizer(self.bxsIntCalc, 1, border=4,
              flag=wx.TOP | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbAddInternal, 0, border=4,
              flag=wx.RIGHT | wx.TOP | wx.LEFT)
        parent.AddWindow(self.lstInpRaw, 1, border=4,
              flag=wx.TOP | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lstInternal, 1, border=4,
              flag=wx.TOP | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbDelInternal, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT)

    def _init_coll_bxsTmplChanged_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.chcAutoOpen, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.chcStarOff, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.chcMsOff, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.cbReadTmpl, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbSaveTmpl, 1, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsMapGen_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.gcbbBuild, 0, border=0, flag=0)
        parent.AddWindow(self.cbClear, 0, border=4, flag=wx.TOP)

    def _init_coll_bxsKey_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.chcKey, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.chcValue, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.nbImport, 1, border=4,
              flag=wx.BOTTOM | wx.LEFT | wx.RIGHT | wx.TOP | wx.EXPAND)
        parent.AddWindow(self.gProc, 0, border=8,
              flag=wx.BOTTOM | wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsProc, 0, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.EXPAND | wx.BOTTOM)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.gcbbOk, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(16, 8), border=0, flag=0)
        parent.AddWindow(self.gcbbCancel, 0, border=0, flag=0)

    def _init_coll_bxsFN_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblFN, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtFN, 3, border=4, flag=wx.EXPAND | wx.LEFT)
        parent.AddWindow(self.gcbbBrowseFN, 1, border=3,
              flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsInput_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblSht, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.chcSht, 2, border=4, flag=wx.EXPAND | wx.LEFT)
        parent.AddWindow(self.lblRow, 1, border=4, flag=wx.EXPAND | wx.LEFT)
        parent.AddWindow(self.intRowStart, 1, border=4,
              flag=wx.EXPAND | wx.LEFT)
        parent.AddWindow(self.intRowStop, 1, border=4, flag=wx.EXPAND | wx.LEFT)
        parent.AddWindow(self.lblCol, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.intColStart, 1, border=4,
              flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.intColEnd, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.gcbbImport, 0, border=0, flag=0)

    def _init_coll_bxsProc_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblTme, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtTmElapsed, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.lblEst, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.txtTmEst, 1, border=4, flag=wx.EXPAND | wx.LEFT)
        parent.AddWindow(self.lblNodeCount, 1, border=4,
              flag=wx.EXPAND | wx.LEFT)
        parent.AddWindow(self.txtNodeCount, 0, border=4,
              flag=wx.EXPAND | wx.LEFT)

    def _init_coll_lstInpRaw_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_RIGHT,
              heading=_(u'Col'), width=50)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'Value'), width=120)

    def _init_coll_trlstMap_Columns(self, parent):
        # generated method, don't edit

        parent.AddColumn(text=_(u'Hierarchy'))
        parent.AddColumn(text=_(u'Map'))
        parent.AddColumn(text=_(u'Compare'))
        parent.AddColumn(text=_(u'Key'))

    def _init_coll_nbImport_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.pnGen, select=True,
              text=_(u'General'))
        parent.AddPage(imageId=-1, page=self.pnInput, select=False,
              text=_(u'Input'))
        parent.AddPage(imageId=-1, page=self.pnMap, select=False,
              text=_(u'Mapping'))
        parent.AddPage(imageId=-1, page=self.pnBuild, select=False,
              text=_(u'Build'))

    def _init_coll_lstInternal_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'Name'), width=80)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'Col'), width=40)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'Value'), width=120)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=5, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.fgsGen = wx.FlexGridSizer(cols=1, hgap=0, rows=7, vgap=0)

        self.bxsFN = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsInput = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.fgsMap = wx.FlexGridSizer(cols=3, hgap=0, rows=1, vgap=0)

        self.bxsMapInpBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.fgsMapOut = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self.bxsComp = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsMapGen = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsProc = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.fgsInpConv = wx.FlexGridSizer(cols=3, hgap=0, rows=2, vgap=0)

        self.fgsBuild = wx.FlexGridSizer(cols=3, hgap=0, rows=3, vgap=0)

        self.bxsKey = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsTmplAdd = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsTmplStored = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsTmplChanged = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsIntCalc = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_fgsGen_Items(self.fgsGen)
        self._init_coll_fgsGen_Growables(self.fgsGen)
        self._init_coll_bxsFN_Items(self.bxsFN)
        self._init_coll_bxsInput_Items(self.bxsInput)
        self._init_coll_fgsMap_Items(self.fgsMap)
        self._init_coll_fgsMap_Growables(self.fgsMap)
        self._init_coll_bxsMapInpBt_Items(self.bxsMapInpBt)
        self._init_coll_fgsMapOut_Items(self.fgsMapOut)
        self._init_coll_fgsMapOut_Growables(self.fgsMapOut)
        self._init_coll_bxsComp_Items(self.bxsComp)
        self._init_coll_bxsMapGen_Items(self.bxsMapGen)
        self._init_coll_bxsProc_Items(self.bxsProc)
        self._init_coll_fgsInpConv_Items(self.fgsInpConv)
        self._init_coll_fgsInpConv_Growables(self.fgsInpConv)
        self._init_coll_fgsBuild_Items(self.fgsBuild)
        self._init_coll_fgsBuild_Growables(self.fgsBuild)
        self._init_coll_bxsKey_Items(self.bxsKey)
        self._init_coll_bxsTmplAdd_Items(self.bxsTmplAdd)
        self._init_coll_bxsTmplStored_Items(self.bxsTmplStored)
        self._init_coll_bxsTmplChanged_Items(self.bxsTmplChanged)
        self._init_coll_bxsIntCalc_Items(self.bxsIntCalc)

        self.SetSizer(self.fgsData)
        self.pnMap.SetSizer(self.fgsMap)
        self.pnGen.SetSizer(self.fgsGen)
        self.pnInput.SetSizer(self.fgsInpConv)
        self.pnBuild.SetSizer(self.fgsBuild)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTXMLIMPORTOFFICEDIALOG,
              name=u'vtXmlImportOfficeDialog', parent=prnt, pos=wx.Point(270,
              82), size=wx.Size(610, 440),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE,
              title=u'vtXml Import Office Dialog')
        self.SetClientSize(wx.Size(602, 413))

        self.gcbbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLIMPORTOFFICEDIALOGGCBBOK,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Ok'),
              name=u'gcbbOk', parent=self, pos=wx.Point(217, 379),
              size=wx.Size(76, 30), style=0)
        self.gcbbOk.SetMinSize(wx.Size(-1, -1))
        self.gcbbOk.Bind(wx.EVT_BUTTON, self.OnGcbbOkButton,
              id=wxID_VTXMLIMPORTOFFICEDIALOGGCBBOK)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLIMPORTOFFICEDIALOGGCBBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'gcbbCancel', parent=self, pos=wx.Point(309, 379),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.SetMinSize(wx.Size(-1, -1))
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VTXMLIMPORTOFFICEDIALOGGCBBCANCEL)

        self.gProc = wx.Gauge(id=wxID_VTXMLIMPORTOFFICEDIALOGGPROC,
              name=u'gProc', parent=self, pos=wx.Point(8, 316), range=100,
              size=wx.Size(586, 18), style=wx.GA_HORIZONTAL | wx.GA_SMOOTH)
        self.gProc.SetMinSize(wx.Size(-1, 18))

        self.lblNodeCount = wx.StaticText(id=wxID_VTXMLIMPORTOFFICEDIALOGLBLNODECOUNT,
              label=_(u'Count'), name=u'lblNodeCount', parent=self,
              pos=wx.Point(400, 342), size=wx.Size(94, 21),
              style=wx.ALIGN_RIGHT)
        self.lblNodeCount.SetMinSize(wx.Size(-1, -1))

        self.txtNodeCount = wx.TextCtrl(id=wxID_VTXMLIMPORTOFFICEDIALOGTXTNODECOUNT,
              name=u'txtNodeCount', parent=self, pos=wx.Point(498, 342),
              size=wx.Size(100, 21), style=0, value=u'')
        self.txtNodeCount.SetMinSize(wx.Size(-1, -1))

        self.txtTmElapsed = wx.TextCtrl(id=wxID_VTXMLIMPORTOFFICEDIALOGTXTTMELAPSED,
              name=u'txtTmElapsed', parent=self, pos=wx.Point(102, 342),
              size=wx.Size(98, 21), style=wx.TE_READONLY, value=u'')
        self.txtTmElapsed.SetMinSize(wx.Size(-1, -1))

        self.txtTmEst = wx.TextCtrl(id=wxID_VTXMLIMPORTOFFICEDIALOGTXTTMEST,
              name=u'txtTmEst', parent=self, pos=wx.Point(302, 342),
              size=wx.Size(94, 21), style=wx.TE_READONLY, value=u'')
        self.txtTmEst.SetMinSize(wx.Size(-1, -1))

        self.nbImport = wx.Notebook(id=wxID_VTXMLIMPORTOFFICEDIALOGNBIMPORT,
              name=u'nbImport', parent=self, pos=wx.Point(4, 4),
              size=wx.Size(594, 300), style=0)

        self.pnGen = wx.Panel(id=wxID_VTXMLIMPORTOFFICEDIALOGPNGEN,
              name=u'pnGen', parent=self.nbImport, pos=wx.Point(0, 0),
              size=wx.Size(586, 274), style=wx.TAB_TRAVERSAL)

        self.lblTmplName = wx.StaticText(id=wxID_VTXMLIMPORTOFFICEDIALOGLBLTMPLNAME,
              label=_(u'Name'), name=u'lblTmplName', parent=self.pnGen,
              pos=wx.Point(4, 38), size=wx.Size(115, 30), style=wx.ALIGN_RIGHT)
        self.lblTmplName.SetMinSize(wx.Size(-1, -1))

        self.txtTmplName = wx.TextCtrl(id=wxID_VTXMLIMPORTOFFICEDIALOGTXTTMPLNAME,
              name=u'txtTmplName', parent=self.pnGen, pos=wx.Point(123, 4),
              size=wx.Size(342, 30), style=0, value=u'')
        self.txtTmplName.SetMinSize(wx.Size(-1, -1))

        self.cbAddTmpl = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLIMPORTOFFICEDIALOGCBADDTMPL,
              bitmap=vtArt.getBitmap(vtArt.Add), label=u'Add',
              name=u'cbAddTmpl', parent=self.pnGen, pos=wx.Point(469, 4),
              size=wx.Size(111, 30), style=0)
        self.cbAddTmpl.SetMinSize(wx.Size(-1, -1))
        self.cbAddTmpl.Bind(wx.EVT_BUTTON, self.OnCbAddTmplButton,
              id=wxID_VTXMLIMPORTOFFICEDIALOGCBADDTMPL)

        self.lblTmpl = wx.StaticText(id=wxID_VTXMLIMPORTOFFICEDIALOGLBLTMPL,
              label=_(u'Template'), name=u'lblTmpl', parent=self.pnGen,
              pos=wx.Point(4, 4), size=wx.Size(115, 30), style=wx.ALIGN_RIGHT)
        self.lblTmpl.SetMinSize(wx.Size(-1, -1))

        self.chcTmpl = wx.Choice(choices=[],
              id=wxID_VTXMLIMPORTOFFICEDIALOGCHCTMPL, name=u'chcTmpl',
              parent=self.pnGen, pos=wx.Point(123, 38), size=wx.Size(342, 21),
              style=0)
        self.chcTmpl.SetMinSize(wx.Size(-1, -1))
        self.chcTmpl.Bind(wx.EVT_CHOICE, self.OnChcTmplChoice,
              id=wxID_VTXMLIMPORTOFFICEDIALOGCHCTMPL)

        self.cbDelTmpl = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLIMPORTOFFICEDIALOGCBDELTMPL,
              bitmap=vtArt.getBitmap(vtArt.Del), label=_(u'Del'),
              name=u'cbDelTmpl', parent=self.pnGen, pos=wx.Point(469, 38),
              size=wx.Size(111, 30), style=0)
        self.cbDelTmpl.SetMinSize(wx.Size(-1, -1))
        self.cbDelTmpl.Bind(wx.EVT_BUTTON, self.OnCbDelTmplButton,
              id=wxID_VTXMLIMPORTOFFICEDIALOGCBDELTMPL)

        self.chcAutoOpen = wx.CheckBox(id=wxID_VTXMLIMPORTOFFICEDIALOGCHCAUTOOPEN,
              label=_(u'auto open'), name=u'chcAutoOpen', parent=self.pnGen,
              pos=wx.Point(4, 80), size=wx.Size(115, 30), style=0)
        self.chcAutoOpen.SetValue(False)
        self.chcAutoOpen.SetMinSize(wx.Size(-1, -1))

        self.chcMsOff = wx.CheckBox(id=wxID_VTXMLIMPORTOFFICEDIALOGCHCMSOFF,
              label=u'MS Office', name=u'chcMsOff', parent=self.pnGen,
              pos=wx.Point(234, 80), size=wx.Size(115, 30), style=0)
        self.chcMsOff.SetValue(False)
        self.chcMsOff.SetMinSize(wx.Size(-1, -1))

        self.chcStarOff = wx.CheckBox(id=wxID_VTXMLIMPORTOFFICEDIALOGCHCSTAROFF,
              label=u'Star Office', name=u'chcStarOff', parent=self.pnGen,
              pos=wx.Point(123, 80), size=wx.Size(111, 30), style=0)
        self.chcStarOff.SetValue(False)
        self.chcStarOff.SetMinSize(wx.Size(-1, -1))

        self.cbReadTmpl = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLIMPORTOFFICEDIALOGCBREADTMPL,
              bitmap=vtArt.getBitmap(vtArt.Open), label=_(u'Read'),
              name=u'cbReadTmpl', parent=self.pnGen, pos=wx.Point(353, 80),
              size=wx.Size(111, 30), style=0)
        self.cbReadTmpl.SetMinSize(wx.Size(-1, -1))
        self.cbReadTmpl.Bind(wx.EVT_BUTTON, self.OnCbReadTmplButton,
              id=wxID_VTXMLIMPORTOFFICEDIALOGCBREADTMPL)

        self.cbSaveTmpl = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLIMPORTOFFICEDIALOGCBSAVETMPL,
              bitmap=vtArt.getBitmap(vtArt.Save), label=_(u'Save'),
              name=u'cbSaveTmpl', parent=self.pnGen, pos=wx.Point(468, 80),
              size=wx.Size(111, 30), style=0)
        self.cbSaveTmpl.SetMinSize(wx.Size(-1, -1))
        self.cbSaveTmpl.Bind(wx.EVT_BUTTON, self.OnCbSaveTmplButton,
              id=wxID_VTXMLIMPORTOFFICEDIALOGCBSAVETMPL)

        self.lblFN = wx.StaticText(id=wxID_VTXMLIMPORTOFFICEDIALOGLBLFN,
              label=_(u'Filename'), name=u'lblFN', parent=self.pnGen,
              pos=wx.Point(4, 114), size=wx.Size(115, 30),
              style=wx.ALIGN_RIGHT)
        self.lblFN.SetMinSize(wx.Size(-1, -1))

        self.txtFN = wx.TextCtrl(id=wxID_VTXMLIMPORTOFFICEDIALOGTXTFN,
              name=u'txtFN', parent=self.pnGen, pos=wx.Point(123, 114),
              size=wx.Size(342, 30), style=0, value=u'')
        self.txtFN.SetMinSize(wx.Size(-1, -1))

        self.gcbbBrowseFN = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLIMPORTOFFICEDIALOGGCBBBROWSEFN,
              bitmap=vtArt.getBitmap(vtArt.Browse), label=_(u'Browse'),
              name=u'gcbbBrowseFN', parent=self.pnGen, pos=wx.Point(468, 114),
              size=wx.Size(112, 30), style=0)
        self.gcbbBrowseFN.SetMinSize(wx.Size(-1, -1))
        self.gcbbBrowseFN.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseFNButton,
              id=wxID_VTXMLIMPORTOFFICEDIALOGGCBBBROWSEFN)

        self.lblSht = wx.StaticText(id=wxID_VTXMLIMPORTOFFICEDIALOGLBLSHT,
              label=_(u'Sheet'), name=u'lblSht', parent=self.pnGen,
              pos=wx.Point(4, 148), size=wx.Size(55, 30), style=wx.ALIGN_RIGHT)
        self.lblSht.SetMinSize(wx.Size(-1, -1))

        self.chcSht = wx.Choice(choices=[],
              id=wxID_VTXMLIMPORTOFFICEDIALOGCHCSHT, name=u'chcSht',
              parent=self.pnGen, pos=wx.Point(63, 148), size=wx.Size(107, 21),
              style=0)
        self.chcSht.SetMinSize(wx.Size(-1, -1))
        self.chcSht.Bind(wx.EVT_CHOICE, self.OnChcShtChoice,
              id=wxID_VTXMLIMPORTOFFICEDIALOGCHCSHT)

        self.lblRow = wx.StaticText(id=wxID_VTXMLIMPORTOFFICEDIALOGLBLROW,
              label=_(u'Row'), name=u'lblRow', parent=self.pnGen,
              pos=wx.Point(174, 148), size=wx.Size(51, 30),
              style=wx.ALIGN_RIGHT)
        self.lblRow.SetMinSize(wx.Size(-1, -1))

        self.intRowStart = wx.lib.intctrl.IntCtrl(allow_long=False,
              allow_none=False, default_color=wx.BLACK,
              id=wxID_VTXMLIMPORTOFFICEDIALOGINTROWSTART, limited=False,
              max=None, min=None, name=u'intRowStart', oob_color=wx.RED,
              parent=self.pnGen, pos=wx.Point(229, 148), size=wx.Size(51, 30),
              style=0, value=0)
        self.intRowStart.SetMinSize(wx.Size(-1, -1))

        self.intRowStop = wx.lib.intctrl.IntCtrl(allow_long=False,
              allow_none=False, default_color=wx.BLACK,
              id=wxID_VTXMLIMPORTOFFICEDIALOGINTROWSTOP, limited=False,
              max=None, min=None, name=u'intRowStop', oob_color=wx.RED,
              parent=self.pnGen, pos=wx.Point(284, 148), size=wx.Size(51, 30),
              style=0, value=0)
        self.intRowStop.SetMinSize(wx.Size(-1, -1))

        self.lblCol = wx.StaticText(id=wxID_VTXMLIMPORTOFFICEDIALOGLBLCOL,
              label=_(u'Col'), name=u'lblCol', parent=self.pnGen,
              pos=wx.Point(339, 148), size=wx.Size(51, 30),
              style=wx.ALIGN_RIGHT)
        self.lblCol.SetMinSize(wx.Size(-1, -1))

        self.intColStart = wx.lib.intctrl.IntCtrl(allow_long=False,
              allow_none=False, default_color=wx.BLACK,
              id=wxID_VTXMLIMPORTOFFICEDIALOGINTCOLSTART, limited=False,
              max=None, min=None, name=u'intColStart', oob_color=wx.RED,
              parent=self.pnGen, pos=wx.Point(394, 148), size=wx.Size(51, 30),
              style=0, value=0)
        self.intColStart.SetMinSize(wx.Size(-1, -1))

        self.intColEnd = wx.lib.intctrl.IntCtrl(allow_long=False,
              allow_none=False, default_color=wx.BLACK,
              id=wxID_VTXMLIMPORTOFFICEDIALOGINTCOLEND, limited=False, max=None,
              min=None, name=u'intColEnd', oob_color=wx.RED, parent=self.pnGen,
              pos=wx.Point(449, 148), size=wx.Size(51, 30), style=0, value=0)
        self.intColEnd.SetMinSize(wx.Size(-1, -1))

        self.gcbbImport = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLIMPORTOFFICEDIALOGGCBBIMPORT,
              bitmap=vtArt.getBitmap(vtArt.ExcelImport), label=_(u'Import'),
              name=u'gcbbImport', parent=self.pnGen, pos=wx.Point(500, 148),
              size=wx.Size(76, 30), style=0)
        self.gcbbImport.SetMinSize(wx.Size(-1, -1))
        self.gcbbImport.Bind(wx.EVT_BUTTON, self.OnGcbbImportButton,
              id=wxID_VTXMLIMPORTOFFICEDIALOGGCBBIMPORT)

        self.pnInput = wx.Panel(id=wxID_VTXMLIMPORTOFFICEDIALOGPNINPUT,
              name=u'pnInput', parent=self.nbImport, pos=wx.Point(0, 0),
              size=wx.Size(586, 274), style=wx.TAB_TRAVERSAL)

        self.lblTme = wx.StaticText(id=wxID_VTXMLIMPORTOFFICEDIALOGLBLTME,
              label=_(u'Time'), name=u'lblTme', parent=self, pos=wx.Point(4,
              342), size=wx.Size(98, 21), style=wx.ALIGN_RIGHT)
        self.lblTme.SetMinSize(wx.Size(-1, -1))

        self.lblEst = wx.StaticText(id=wxID_VTXMLIMPORTOFFICEDIALOGLBLEST,
              label=_(u'Estimated'), name=u'lblEst', parent=self,
              pos=wx.Point(204, 342), size=wx.Size(94, 21),
              style=wx.ALIGN_RIGHT)
        self.lblEst.SetMinSize(wx.Size(-1, -1))

        self.spRow = wx.SpinCtrl(id=wxID_VTXMLIMPORTOFFICEDIALOGSPROW,
              initial=0, max=100, min=0, name=u'spRow', parent=self.pnInput,
              pos=wx.Point(4, 4), size=wx.Size(178, 30),
              style=wx.SP_ARROW_KEYS)
        self.spRow.SetMinSize(wx.Size(-1, -1))
        self.spRow.Bind(wx.EVT_SPINCTRL, self.OnSpRowSpinctrl,
              id=wxID_VTXMLIMPORTOFFICEDIALOGSPROW)

        self.lstInpRaw = wx.ListCtrl(id=wxID_VTXMLIMPORTOFFICEDIALOGLSTINPRAW,
              name=u'lstInpRaw', parent=self.pnInput, pos=wx.Point(4, 38),
              size=wx.Size(178, 236), style=wx.LC_REPORT)
        self.lstInpRaw.SetMinSize(wx.Size(-1, -1))
        self._init_coll_lstInpRaw_Columns(self.lstInpRaw)
        self.lstInpRaw.Bind(wx.EVT_LIST_COL_CLICK, self.OnLxtInpRowListColClick,
              id=wxID_VTXMLIMPORTOFFICEDIALOGLSTINPRAW)
        self.lstInpRaw.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLxtInpRowListItemDeselected,
              id=wxID_VTXMLIMPORTOFFICEDIALOGLSTINPRAW)
        self.lstInpRaw.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLxtInpRowListItemSelected,
              id=wxID_VTXMLIMPORTOFFICEDIALOGLSTINPRAW)

        self.txtName = wx.TextCtrl(id=wxID_VTXMLIMPORTOFFICEDIALOGTXTNAME,
              name=u'txtName', parent=self.pnInput, pos=wx.Point(186, 4),
              size=wx.Size(120, 30), style=0, value=u'')
        self.txtName.SetMinSize(wx.Size(-1, -1))

        self.viCalc = vidarc.tool.input.vtInputCalcPanel.vtInputCalcPanel(id=wxID_VTXMLIMPORTOFFICEDIALOGVICALC,
              name=u'viCalc', parent=self.pnInput, pos=wx.Point(310, 4),
              size=wx.Size(236, 30), style=0)

        self.lstInternal = wx.ListCtrl(id=wxID_VTXMLIMPORTOFFICEDIALOGLSTINTERNAL,
              name=u'lstInternal', parent=self.pnInput, pos=wx.Point(186, 38),
              size=wx.Size(361, 236), style=wx.LC_REPORT)
        self.lstInternal.SetMinSize(wx.Size(-1, -1))
        self._init_coll_lstInternal_Columns(self.lstInternal)
        self.lstInternal.Bind(wx.EVT_LIST_COL_CLICK,
              self.OnLstInternalListColClick,
              id=wxID_VTXMLIMPORTOFFICEDIALOGLSTINTERNAL)
        self.lstInternal.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstInternalListItemDeselected,
              id=wxID_VTXMLIMPORTOFFICEDIALOGLSTINTERNAL)
        self.lstInternal.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstInternalListItemSelected,
              id=wxID_VTXMLIMPORTOFFICEDIALOGLSTINTERNAL)

        self.cbAddInternal = wx.lib.buttons.GenBitmapButton(ID=wxID_VTXMLIMPORTOFFICEDIALOGCBADDINTERNAL,
              bitmap=vtArt.getBitmap(vtArt.Add), name=u'cbAddInternal',
              parent=self.pnInput, pos=wx.Point(551, 4), size=wx.Size(31, 30),
              style=0)
        self.cbAddInternal.SetMinSize(wx.Size(-1, -1))
        self.cbAddInternal.Bind(wx.EVT_BUTTON, self.OnCbAddInternalButton,
              id=wxID_VTXMLIMPORTOFFICEDIALOGCBADDINTERNAL)

        self.cbDelInternal = wx.lib.buttons.GenBitmapButton(ID=wxID_VTXMLIMPORTOFFICEDIALOGCBDELINTERNAL,
              bitmap=vtArt.getBitmap(vtArt.Del), name=u'cbDelInternal',
              parent=self.pnInput, pos=wx.Point(551, 38), size=wx.Size(31, 30),
              style=0)
        self.cbDelInternal.SetMinSize(wx.Size(-1, -1))
        self.cbDelInternal.Bind(wx.EVT_BUTTON, self.OnCbDelInternalButton,
              id=wxID_VTXMLIMPORTOFFICEDIALOGCBDELINTERNAL)

        self.pnMap = wx.Panel(id=wxID_VTXMLIMPORTOFFICEDIALOGPNMAP,
              name=u'pnMap', parent=self.nbImport, pos=wx.Point(0, 0),
              size=wx.Size(586, 274), style=wx.TAB_TRAVERSAL)

        self.chcKey = wx.CheckBox(id=wxID_VTXMLIMPORTOFFICEDIALOGCHCKEY,
              label=_(u'key'), name=u'chcKey', parent=self.pnMap,
              pos=wx.Point(4, 8), size=wx.Size(164, 21), style=0)
        self.chcKey.SetValue(False)
        self.chcKey.SetMinSize(wx.Size(-1, -1))

        self.chcValue = wx.Choice(choices=[],
              id=wxID_VTXMLIMPORTOFFICEDIALOGCHCVALUE, name=u'chcValue',
              parent=self.pnMap, pos=wx.Point(172, 8), size=wx.Size(325, 21),
              style=0)
        self.chcValue.SetMinSize(wx.Size(-1, -1))
        self.chcValue.Bind(wx.EVT_CHOICE, self.OnChcValueChoice,
              id=wxID_VTXMLIMPORTOFFICEDIALOGCHCVALUE)

        self.lblCmp = wx.StaticText(id=wxID_VTXMLIMPORTOFFICEDIALOGLBLCMP,
              label=_(u'Compare'), name=u'lblCmp', parent=self.pnMap,
              pos=wx.Point(4, 33), size=wx.Size(164, 21), style=wx.ALIGN_RIGHT)
        self.lblCmp.SetMinSize(wx.Size(-1, -1))

        self.cbSet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLIMPORTOFFICEDIALOGCBSET,
              bitmap=vtArt.getBitmap(vtArt.Add), label=u'', name=u'cbSet',
              parent=self.pnMap, pos=wx.Point(506, 4), size=wx.Size(76, 34),
              style=0)
        self.cbSet.SetMinSize(wx.Size(-1, -1))
        self.cbSet.Bind(wx.EVT_BUTTON, self.OnCbSetButton,
              id=wxID_VTXMLIMPORTOFFICEDIALOGCBSET)

        self.txtComp = wx.TextCtrl(id=wxID_VTXMLIMPORTOFFICEDIALOGTXTCOMP,
              name=u'txtComp', parent=self.pnMap, pos=wx.Point(172, 33),
              size=wx.Size(325, 21), style=0, value=u'')
        self.txtComp.SetToolTipString(u'[?:][AI*,AI?,FI]')
        self.txtComp.SetMinSize(wx.Size(-1, -1))

        self.trlstMap = wx.gizmos.TreeListCtrl(id=wxID_VTXMLIMPORTOFFICEDIALOGTRLSTMAP,
              name=u'trlstMap', parent=self.pnMap, pos=wx.Point(4, 58),
              size=wx.Size(494, 208), style=wx.TR_HAS_BUTTONS)
        self._init_coll_trlstMap_Columns(self.trlstMap)
        self.trlstMap.Bind(wx.EVT_TREE_SEL_CHANGED,
              self.OnTrlstMapTreeSelChanged,
              id=wxID_VTXMLIMPORTOFFICEDIALOGTRLSTMAP)

        self.cbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLIMPORTOFFICEDIALOGCBDEL,
              bitmap=vtArt.getBitmap(vtArt.Del), label=u'', name=u'cbDel',
              parent=self.pnMap, pos=wx.Point(506, 42), size=wx.Size(76, 30),
              style=0)
        self.cbDel.SetMinSize(wx.Size(-1, -1))
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VTXMLIMPORTOFFICEDIALOGCBDEL)

        self.pnBuild = wx.Panel(id=wxID_VTXMLIMPORTOFFICEDIALOGPNBUILD,
              name=u'pnBuild', parent=self.nbImport, pos=wx.Point(0, 0),
              size=wx.Size(586, 274), style=wx.TAB_TRAVERSAL)

        self.gcbbBuild = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLIMPORTOFFICEDIALOGGCBBBUILD,
              bitmap=vtArt.getBitmap(vtArt.Build), label=_(u'Build'),
              name=u'gcbbBuild', parent=self.pnBuild, pos=wx.Point(506, 21),
              size=wx.Size(76, 30), style=0)
        self.gcbbBuild.SetMinSize(wx.Size(-1, -1))
        self.gcbbBuild.Bind(wx.EVT_BUTTON, self.OnGcbbBuildButton,
              id=wxID_VTXMLIMPORTOFFICEDIALOGGCBBBUILD)

        self.cbClear = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLIMPORTOFFICEDIALOGCBCLEAR,
              bitmap=vtArt.getBitmap(vtArt.Del), label=_(u'Clear'),
              name=u'cbClear', parent=self.pnBuild, pos=wx.Point(506, 55),
              size=wx.Size(76, 30), style=0)
        self.cbClear.SetMinSize(wx.Size(-1, -1))
        self.cbClear.Bind(wx.EVT_BUTTON, self.OnCbClearButton,
              id=wxID_VTXMLIMPORTOFFICEDIALOGCBCLEAR)

        self.lstRows = wx.ListCtrl(id=wxID_VTXMLIMPORTOFFICEDIALOGLSTROWS,
              name=u'lstRows', parent=self.pnBuild, pos=wx.Point(4, 21),
              size=wx.Size(490, 249), style=wx.LC_REPORT)
        self.lstRows.SetMinSize(wx.Size(-1, -1))

        self.lblRowBuild = wx.StaticText(id=wxID_VTXMLIMPORTOFFICEDIALOGLBLROWBUILD,
              label=_(u'Rows'), name=u'lblRowBuild', parent=self.pnBuild,
              pos=wx.Point(4, 4), size=wx.Size(490, 13), style=0)
        self.lblRowBuild.SetMinSize(wx.Size(-1, -1))

        self._init_coll_nbImport_Pages(self.nbImport)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self)
        vtLog.vtLogOriginWX.__init__(self)
        self.fgsData.SetMinSize(wx.Size(-1,-1))
        self.bxsBt.SetMinSize(wx.Size(-1,-1))
        self.fgsGen.SetMinSize(wx.Size(-1,-1))
        self.bxsFN.SetMinSize(wx.Size(-1,-1))
        self.bxsInput.SetMinSize(wx.Size(-1,-1))
        self.fgsMap.SetMinSize(wx.Size(-1,-1))
        self.bxsMapInpBt.SetMinSize(wx.Size(-1,-1))
        self.fgsMapOut.SetMinSize(wx.Size(-1,-1))
        self.bxsComp.SetMinSize(wx.Size(-1,-1))
        self.bxsMapGen.SetMinSize(wx.Size(-1,-1))
        self.bxsProc.SetMinSize(wx.Size(-1,-1))
        self.bxsKey.SetMinSize(wx.Size(-1,-1))
        
        vidarc.tool.input.vtInputCalcPanel.EVT_VTINPUT_CALC_CHANGED(self.viCalc,self.OnFiltersChanged)
        
        self.idxMap=-1
        self.selElem=-1
        self.selInt=-1
        self.selElem=-1
        self.selNode=None
        self.appl=None
        self.iTmplNodes=-1
        self.dInt={}
        self.dFormel={}
        self.dVal={}
        self.dMap={}
        self.dKey={}
        self.tiRoot=None
        self.selTi=None
        self.selRegObj=None
        self.selTagName=None
        self.oCalc=None
        
        self.il = wx.ImageList(16, 16)
        self.il.Add(vtArt.getBitmap(vtArt.Navigate))
        self.il.Add(vtArt.getBitmap(vtArt.ImportFree))
        self.il.Add(vtArt.getBitmap(vtArt.Link))
        self.il.Add(vtArt.getBitmap(vtArt.Build))
        self.nbImport.AssignImageList(self.il)
        for i in xrange(0,4):
            self.nbImport.SetPageImage(i,i)
            
        self.thdOpen=thdOpenFile(self)
        EVT_IMPORT_OFFICE_THREAD_FINSHED(self,self.OnOpenFin)
        self.thdRead=thdReadData(self,verbose=1)
        EVT_IMPORT_OFFICE_THREAD_FINSHED_READ(self,self.OnReadFin)
        EVT_IMPORT_OFFICE_PROGRESS(self,self.OnReadProgress)
        #self.Bind(EVT_IMPORT_OFFICE_THREAD_FINSHED_READ,self.OnReadFin)
        #self.Bind(EVT_IMPORT_OFFICE_PROGRESS,self.OnReadProgress)
        
        verbose=self.VERBOSE-1
        if verbose<0:
            verbose=0
        self.thdBuild=thdImportData(self,verbose=verbose)
        EVT_IMPORT_XML_THREAD_FINSHED(self,self.OnImportFin)
        EVT_IMPORT_XML_THREAD_PROGRESS(self,self.OnImportProgress)
        
        self.trlstMap.SetColumnWidth(0,280)
        
        if 0:
            self.trlstMap = vXmlPrjEngTreeList(id=wx.NewId(),
                  name=u'trlstMap', parent=self, pos=wx.Point(298, 108),
                  size=wx.Size(388, 268), style=wx.TR_HAS_BUTTONS,
                  cols=[u'tag',u'value',u'calc',u'comp'],
                  master=True,verbose=0)
            EVT_VTXMLTREE_ITEM_SELECTED(self.trlstMap,self.OnTreeItemSel)
            self.trlstMap.SetColumnWidth(0, 150)
            self.trlstMap.SetColumnWidth(1, 40)
            EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.trlstMap,self.OnAddFinished)
        
        #self.lstElement.InsertColumn(0,u'Name',wx.LIST_FORMAT_LEFT,20)
        #self.lstElement.InsertColumn(1,u'Type',wx.LIST_FORMAT_LEFT,180)
    def Lock(self,flag):
        #self.thdBuild.Lock(flag)
        pass
    def OnLock(self,evt):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                if self.doc.GetLoggedInSecLv()<self.iSecLvEdit:
                    self.Lock(True)
                else:
                    self.Lock(False)
            else:
                self.Lock(True)
        if self.thdBuild.IsRunning():
            node=self.thdBuild.nodeFound
            if self.doc.isSameKey(node,evt.GetID()):
                resp=evt.GetResponse()
                if resp in  ['ok','already locked']:
                    self.thdBuild.Lock(False)
                else:
                    self.thdBuild.Lock(True)
        evt.Skip()
    def OnAddResponse(self,evt):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'id:%s;idNew:%s'%(evt.GetID(),evt.GetNewID()),self)
            vtLog.CallStack('')
            print self.thdBuild.IsRunning()
        if self.thdBuild.IsRunning():
            self.thdBuild.OnAddResponse(evt)
        evt.Skip()
    def Clear(self):
        if self.VERBOSE:
            vtLog.CallStack('')
        vtLog.vtLngCurWX(vtLog.INFO,'',self)
        vtXmlNodePanel.Clear(self)
        self.idxMap=-1
        self.selElem=-1
        self.selInt=-1
        self.selElem=-1
        self.selNode=None
        self.iTmplNodes=-1
        self.dInt={}
        self.dFormel={}
        self.dVal={}
        self.dMap={}
        self.dKey={}
        self.infos=[]
        self.selTi=None
        self.selRegObj=None
        self.selTagName=None
        self.txtFN.SetValue('')
        self.txtTmplName.SetValue('')
        self.lstInpRaw.DeleteAllItems()
        self.lstInternal.DeleteAllItems()
        self.lstRows.ClearAll()
        self.chcValue.Clear()
        self.__clrRegNode__(self.tiRoot)
    def __getTag__(self,base,node,lst):
        childs=self.doc.getChilds(node)
        for c in childs:
            if len(base)>0:
                s=base+'>'+self.doc.getTagName(c)
            else:
                s=self.doc.getTagName(c)
            lst.append((s,c))
            self.__getTag__(s,c,lst)
    def SetLanguage(self,lang):
        self.trlstMap.SetLanguage(lang)
    def SetLanguages(self,languages,languageIds):
        self.trlstMap.SetLanguages(languages,languageIds)
    def __calcCount__(self,node,skip):
        i=1
        for c in self.doc.getChilds(node):
            if self.doc.getTagName(c) in skip:
                continue
            i+=self.__calcCount__(c,skip)
        return i
    def __addRegNode__(self,l,tip,iLv=0,lHier=[],lSkip=[]):
        lst=copy.deepcopy(l)
        lst.sort()
        iSkip=len(lSkip)
        if iSkip==0:
            lS=[]
        for s in lst:
            if s in lHier:
                continue
            o=self.doc.GetRegisteredNode(s)
            if iSkip==0:
                i,j=self.imgDict[o.GetTagName()]
                if o.IsImportExportBase() and iLv==0:
                    tid=wx.TreeItemData(o)
                else:
                    tipd=self.trlstMap.GetPyData(tip)
                    if type(tipd)==types.ListType:
                        lCpy=copy.copy(tipd)
                        lCpy.append(o)
                        tid=wx.TreeItemData(lCpy)
                    else:
                        tid=wx.TreeItemData([tipd,o])
                tic=self.trlstMap.AppendItem(tip,s,i,j,tid)
                lVals=o.GetNodeValues()
                if self.VERBOSE:
                    vtLog.CallStack('')
                    print o.GetTagName(),o
                    print lVals
                if len(lVals)>0:
                    lVals.sort()
                    i,j=self.imgDict['__attrs']
                    tiv=self.trlstMap.AppendItem(tic,'values',i,j)
                    for lVal in lVals:
                        i,j=self.imgDict['__attr']
                        tid=wx.TreeItemData((o,lVal))
                        tivc=self.trlstMap.AppendItem(tiv,lVal,i,j,tid)
            else:
                if s!=lSkip[0]:
                    continue
                lS=lSkip[1:]
                tic=tip
            ll=o.GetValidChild()
            if ll is not None:
                if len(ll)>=0:
                    self.__addRegNode__(ll,tic,iLv+1,lHier+[s],lS)
    def __clrRegNode__(self,ti):
        if ti is None:
            return
        triChild=self.trlstMap.GetFirstChild(ti)
        while triChild[0].IsOk():
            self.trlstMap.SetItemText(triChild[0],'',1)
            self.trlstMap.SetItemText(triChild[0],'',2)
            self.__clrRegNode__(triChild[0])
            triChild=self.trlstMap.GetNextChild(ti,triChild[1])
    def __setRegNode__(self,ti,lBase,sTag,sAttr,sVal,sCmp,sKey):
        if ti is None:
            return
        if self.VERBOSE:
            vtLog.CallStack('')
            print 'tag:%s,attr:%s,val:%s,cmp:%s,key:%s'%(sTag,sAttr,sVal,sCmp,sKey)
            print lBase
        triChild=self.trlstMap.GetFirstChild(ti)
        tipd=self.trlstMap.GetPyData(ti)
        if tipd is None:
            tip=self.trlstMap.GetItemParent(ti)
            #print tip
            try:
                if tip is not None:
                    tipd=self.trlstMap.GetPyData(tip)
            except:
                tipd=None
                #print 'tag:%s,attr:%s,val:%s,cmp:%s,key:%s'%(sTag,sAttr,sVal,sCmp,sKey)
                
        while triChild[0].IsOk():
            tid=self.trlstMap.GetPyData(triChild[0])
            if len(sAttr)>0:
                if type(tid)==types.TupleType:
                    bMatch=True
                    if lBase is not None:
                        if type(tipd)==types.ListType:
                            for o,sTag in zip(tipd,lBase):
                                if o.GetTagName()!=sTag:
                                    bMatch=False
                                    break
                        else:
                            bMatch=False
                    if bMatch:
                        if tid[0].GetTagName()==sTag and tid[1]==sAttr:
                            self.trlstMap.SetItemText(triChild[0],sVal,1)
                            self.trlstMap.SetItemText(triChild[0],sCmp,2)
                            if sKey!='':
                                self.trlstMap.SetItemText(triChild[0],sKey,3)
                            return 1
            else:
                if lBase is None:
                    if (type(tid)!=types.TupleType) and (type(tid)!=types.ListType):
                        if tid[0].GetTagName()==sTag:
                            self.trlstMap.SetItemText(triChild[0],sCmp,2)
                            return 1
                else:
                    if type(tid)==types.ListType:
                        bMatch=True
                        for o,sTagTmp in zip(tid,lBase):
                            if o.GetTagName()!=sTagTmp:
                                bMatch=False
                                break
                        if bMatch:
                            self.trlstMap.SetItemText(triChild[0],sCmp,2)
                            return 1
            iRet=self.__setRegNode__(triChild[0],lBase,sTag,sAttr,sVal,sCmp,sKey)
            if iRet==1:
                return iRet
            triChild=self.trlstMap.GetNextChild(ti,triChild[1])
        return 0
    def SetDoc(self,doc,bNet=False):
        if self.doc is not None:
            EVT_NET_XML_ADD_NODE_RESPONSE(self.doc,self.OnAddResponse)
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        if self.doc is not None:
            EVT_NET_XML_ADD_NODE_RESPONSE(self.doc,self.OnAddResponse)
        self.thdBuild.SetNetworking(bNet)
        #self.trlstMap.SetDoc(doc,False)
    def __buildTree(self,node):
        try:
            lSkip=[]
            if node is not None:
                tmp=node
                while self.doc.IsNodeKeyValid(tmp):
                    sTagName=self.doc.getTagName(tmp)
                    if len(lSkip)>0:
                        if lSkip[-1]==sTagName:
                            tmp=self.doc.getParent(tmp)
                            continue
                    lSkip.append(sTagName)
                    tmp=self.doc.getParent(tmp)
                lSkip.reverse()
            self.oCalc=self.doc.GetRegisteredNode('calc')
            self.viCalc.SetRegObj(self.oCalc)
            self.cbSet.Enable(False)
            self.cbDel.Enable(False)
            self.trlstMap.DeleteAllItems()
            self.tiRoot=None
            self.imgDict={}
            self.imgLst=wx.ImageList(16,16)
            i=self.imgLst.Add(vtArt.getBitmap(vtArt.Attr))
            self.imgDict['__attr']=[i,i]
            i=self.imgLst.Add(vtArt.getBitmap(vtArt.Attrs))
            self.imgDict['__attrs']=[i,i]
            for o in self.doc.lstRegNodes:
                if o.HasGraphics():
                    i=self.imgLst.Add(o.GetBitmap())
                    j=self.imgLst.Add(o.GetSelBitmap())
                    self.imgDict[o.GetTagName()]=[i,j]
            i=self.imgLst.Add(vtArt.getBitmap(vtArt.MES))
            j=self.imgLst.Add(vtArt.getBitmap(vtArt.MES))
            self.trlstMap.SetImageList(self.imgLst)
            self.tiRoot=self.trlstMap.AddRoot(_(u'structure'),i,j)
            l=[]
            for o in self.doc.lstRegNodesRoot:
                l.append(o.GetTagName())
            l.sort()
            self.__addRegNode__(l,self.tiRoot,lSkip=lSkip)
            self.trlstMap.Expand(self.tiRoot)
        except:
            vtLog.vtLngTB(self.GetName())
    def SetNode(self,node,nodeImpBase):
        try:
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
                print node
            #if vtXmlNodePanel.SetNode(self,node)<0:
            if vtXmlNodePanel.SetNode(self,nodeImpBase)<0:
                if self.VERBOSE:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'exit',self)
                    print 'exit'
                return
            #self.nodeImpBase=nodeImpBase
            self.node=nodeImpBase
            self.nodeSettings=node
            self.lstNewNodes=[]
            self.dInt={}
            
            self.chcTmpl.Clear()
            tmplNodes=self.doc.getChilds(node,'io_tmpl')
            for n in tmplNodes:
                sTag=self.doc.getNodeText(n,'name')
                self.chcTmpl.Append(sTag,n)
            self.__buildTree(nodeImpBase)
        except:
            vtLog.vtLngTB(self.GetName())
    def __getTagDict__(self,dMap,lBase,sTag):
        if lBase is None:
            if not dMap.has_key(sTag):
                dMap[sTag]={'attr':{},'sub':{}}
            return dMap[sTag]
        else:
            for s in lBase:
                if not dMap.has_key(s):
                    dMap[s]={'attr':{},'sub':{}}
                dMap=dMap[s]['sub']
            if not dMap.has_key(sTag):
                dMap[sTag]={'attr':{},'sub':{}}
            return dMap[sTag]
    def __getKeyDict__(self,dMap,sTag):
        if not dMap.has_key(sTag):
            dMap[sTag]={}
        return dMap[sTag]
    def __getTmplData__(self,node):
        try:
            sName=self.doc.getNodeText(node,'name')
            sFN=self.doc.getNodeText(node,'fn')
            sRowStart=self.doc.getNodeText(node,'row_start')
            sRowEnd=self.doc.getNodeText(node,'row_stop')
            sColStart=self.doc.getNodeText(node,'col_start')
            sColEnd=self.doc.getNodeText(node,'col_stop')
            self.chcSht.Clear()
            sheetsNode=self.doc.getChild(node,'sheets')
            if sheetsNode is not None:
                for c in self.doc.getChilds(sheetsNode,'sheet'):
                    self.chcSht.Append(self.doc.getText(c))
            sSht=self.doc.getNodeText(node,'sheet')
            
            self.txtTmplName.SetValue(sName)
            self.txtFN.SetValue(sFN)
            self.__openOfficeFile__()
            self.chcSht.SetStringSelection(sSht)
            def __conv2Int__(s):
                try:
                    return int(s)
                except:
                    return 0
            
            self.intRowStart.SetValue(__conv2Int__(sRowStart))
            self.intRowStop.SetValue(__conv2Int__(sRowEnd))
            self.intColStart.SetValue(__conv2Int__(sColStart))
            self.intColEnd.SetValue(__conv2Int__(sColEnd))
            
            self.lstInternal.DeleteAllItems()
            lst=[]
            self.lstNewNodes=[]
            self.dInt={}
            self.dFormel={}
            interNode=self.doc.getChild(node,'internal')
            if interNode is not None:
                self.__getTag__('',interNode,lst)
                for item in lst:
                    #index = self.lstInternal.InsertImageStringItem(sys.maxint, item[0], -1)
                    sVal=self.doc.getText(item[1])
                    self.dInt[item[0]]=sVal
                    #self.lstInternal.SetStringItem(index,1,sVal,-1)
            formelNode=self.doc.getChild(node,'formel')
            if formelNode is not None:
                keys=self.dInt.keys()
                keys.sort()
                oCalc=self.doc.GetRegisteredNode('calc')
                for k in keys:
                    #self.dFormel[k]=[]
                    tmp=self.doc.getChild(formelNode,k)
                    self.dFormel[k]=oCalc.GetFormel(tmp)
            self.__clrRegNode__(self.tiRoot)
            mapNode=self.doc.getChild(node,'map')
            if mapNode is not None:
                for c in self.doc.getChilds(mapNode):
                    def addMapInfo(c,lBase):
                        sTag=self.doc.getTagName(c)
                        if self.doc.hasAttribute(c,'attr'):
                            sCmp=self.doc.getAttribute(c,'cmp')
                            sKey=self.doc.getAttribute(c,'key')
                            sAttr=self.doc.getAttribute(c,'attr')
                        else:
                            sBase=self.doc.getAttribute(c,'base')
                            if sBase=='':
                                lBase=[sTag]
                            else:
                                lBase=sBase.split(',')
                                lBase.append(sTag)
                            for cc in self.doc.getChilds(c):
                                addMapInfo(cc,lBase)
                            return
                        dMap=self.__getTagDict__(self.dMap,lBase,sTag)
                        sVal=self.doc.getText(c)
                        #sVal=self.doc.getNodeText(c,'internal')
                        d=dMap['attr']
                        if sKey!='':
                            #if not self.dKey.has_key(sTag):
                            #    self.dKey[sTag]={'attr':{},'sub':{}}
                            #dK=self.dKey[sTag]['attr']
                            dK=self.__getKeyDict__(self.dKey,sTag)
                            sKey=_('key')
                            dK[sAttr]=sVal
                        if lBase is None:
                            iRet=self.__setRegNode__(self.tiRoot,lBase,sTag,sAttr,sVal,sCmp,sKey)
                        else:
                            iRet=self.__setRegNode__(self.tiRoot,lBase+[sTag],sTag,sAttr,sVal,sCmp,sKey)
                        if iRet>0:
                            d[sAttr]=[sVal,sCmp,sKey]
                    addMapInfo(c,None)
            if self.VERBOSE:
                vtLog.CallStack('')
                print 'map'
                vtLog.pprint(self.dMap)
                print 'key'
                vtLog.pprint(self.dKey)
            self.__showInternal__()
            self.tmplNode=node
        except:
            vtLog.vtLngTB(self.GetName())
    def __showInternal__(self):
        self.lstInternal.DeleteAllItems()
        self.chcValue.Clear()
        keys=self.dInt.keys()
        keys.sort()
        self.dVal={}
        for k in keys:
            index = self.lstInternal.InsertStringItem(sys.maxint, k)
            self.lstInternal.SetStringItem(index,1,self.dInt[k],-1)
            
            index = self.chcValue.Append(k)
    def OnAddFinished(self,evt):
        node=self.tmplNode
        mapNode=self.doc.getChild(node,'map')
        if mapNode is not None:
            for c in self.doc.getChilds(mapNode,'node'):
                id=self.doc.getAttribute(c,'fid')
                res,node=self.trlstMap.__findNodeByID__(None,id)
                if node is None:
                    continue
                for ca in self.doc.getChilds(c):
                    ti=self.trlstMap.FindTreeItem(node)
                    if self.doc.getTagName(ca)=='__node':
                        attrNode=node
                    else:
                        attrNode=self.doc.getChild(node,self.doc.getTagName(ca))
                        ti=self.trlstMap.FindAttr(ti,attrNode)
                    if ti is None:
                        continue
                    sVal=self.doc.getText(ca)
                    sCmp=self.doc.getAttribute(ca,'cmp')
                    self.trlstMap.SetItemText(ti,sVal,2)
                    if len(sCmp)>0:
                        self.trlstMap.SetItemText(ti,sCmp,3)
        evt.Skip()
    def GetNode(self):
        idx=self.chcTmpl.GetSelection()
        sNewName=self.txtTmplName.GetValue()
        bCreate=False
        if idx>=0:
            tmplNode=self.chcTmpl.GetClientData(idx)
            sName=self.doc.getNodeText(tmplNode,'name')
            if sName!=sNewName:
                sName=self.txtTmplName.GetValue()
                bCreate=True
        else:
            bCreate=True
            for c in self.doc.getChilds(self.nodeSettings,'io_tmpl'):
                sName=self.doc.getNodeText(c,'name')
                if sNewName==sName:
                    bCreate=False
                    tmplNode=c
                    break
        if bCreate:
            tmplNode=self.doc.createSubNode(self.nodeSettings,'io_tmpl',False)
        sFN=self.txtFN.GetValue()
        sRowStart=str(self.intRowStart.GetValue())
        sRowEnd=str(self.intRowStop.GetValue())
        sColStart=str(self.intColStart.GetValue())
        sColEnd=str(self.intColEnd.GetValue())
        sSht=self.chcSht.GetStringSelection()
        
        self.doc.setNodeText(tmplNode,'name',sNewName)
        self.doc.setNodeText(tmplNode,'fn',sFN)
        sheetNode=self.doc.getChild(tmplNode,'internal')
        if sheetNode is not None:
            for n in self.doc.getChilds(sheetNode):
                self.doc.deleteNode(n,tmplNode)
        else:
            sheetNode=self.doc.createSubNode(tmplNode,'sheets',False)
        for i in xrange(self.chcSht.GetCount()):
            self.doc.createSubNodeText(sheetNode,'sheet',self.chcSht.GetString(i),False)
        self.doc.setNodeText(tmplNode,'sheet',sSht)
        self.doc.setNodeText(tmplNode,'row_start',sRowStart)
        self.doc.setNodeText(tmplNode,'row_stop',sRowEnd)
        self.doc.setNodeText(tmplNode,'col_start',sColStart)
        self.doc.setNodeText(tmplNode,'col_stop',sColEnd)
        
        interNode=self.doc.getChild(tmplNode,'internal')
        if interNode is not None:
            for n in self.doc.getChilds(interNode):
                self.doc.deleteNode(n,interNode)
        else:
            interNode=self.doc.createSubNode(tmplNode,'internal',False)
        formelNode=self.doc.getChild(tmplNode,'formel')
        if formelNode is not None:
            for n in self.doc.getChilds(formelNode):
                self.doc.deleteNode(n,formelNode)
        else:
            formelNode=self.doc.createSubNode(tmplNode,'formel',False)
        oCalc=self.doc.GetRegisteredNode('calc')
        iCount=self.lstInternal.GetItemCount()
        keys=self.dInt.keys()
        keys.sort()
        for k in keys:
            sVal=self.dInt[k]
            self.doc.createSubNodeText(interNode,k,sVal,False)
            try:
                lFormel=self.dFormel[k]
            except:
                lFormel=[]
            c=self.doc.createSubNode(formelNode,k,False)
            oCalc.SetFormel(c,lFormel)
        mapNode=self.doc.getChild(tmplNode,'map')
        if mapNode is not None:
            for n in self.doc.getChilds(mapNode):
                self.doc.deleteNode(n,mapNode)
        else:
            mapNode=self.doc.createSubNode(tmplNode,'map',False)
        def addAttr(mapNode,dMap):
            if self.VERBOSE:
                vtLog.pprint(dMap)
            keys=dMap.keys()
            keys.sort()
            for k in keys:
                d=dMap[k]['attr']
                kkeys=d.keys()
                kkeys.sort()
                for kk in kkeys:
                    dd=d[kk]
                    c=self.doc.createSubNodeTextAttr(mapNode,k,dd[0],'attr',kk)
                    self.doc.setAttribute(c,'cmp',dd[1])
                    if dd[2]!='':
                        self.doc.setAttribute(c,'key',dd[2])
        def addSub(mapNode,dMap,l):
            if self.VERBOSE:
                vtLog.pprint(dMap)
            keys=dMap.keys()
            keys.sort()
            for k in keys:
                d=dMap[k]['sub']
                kkeys=d.keys()
                if len(kkeys)>0:
                    c=self.doc.createSubNodeAttr(mapNode,k,'base',','.join(l))
                    addAttr(c,d)
                    addSub(c,d,l+[k])
        if self.VERBOSE:
            vtLog.CallStack('')
            vtLog.pprint(self.dMap)
        addAttr(mapNode,self.dMap)
        addSub(mapNode,self.dMap,[])
        if 0:
            lst=self.trlstMap.GetNodesAttrs(check=[2])
            for it in lst:
                n=it[0]
                id=self.doc.getAttribute(n,'id')
                newNode=self.doc.createSubNode(mapNode,'node')
                self.doc.setAttribute(newNode,'fid',id)
                ti=self.trlstMap.FindTreeItem(n)
                if ti is not None:
                    sVal=self.trlstMap.GetItemText(ti,2)
                    sComp=self.trlstMap.GetItemText(ti,3)
                    if len(sVal)>0:
                        if len(sComp)>0:
                            self.doc.createSubNodeTextAttr(newNode,'__node',sVal,
                                    'cmp',sComp,False)
                        else:
                            self.doc.createSubNodeText(newNode,'__node',sVal,
                                    False)
                for attrTup in it[1]:
                    sTag=self.doc.getTagName(attrTup[1])
                    sVal=self.trlstMap.GetItemText(attrTup[0],2)
                    sComp=self.trlstMap.GetItemText(attrTup[0],3)
                    if len(sComp)>0:
                        self.doc.createSubNodeTextAttr(newNode,sTag,sVal,
                                'cmp',sComp,False)
                    else:
                        self.doc.createSubNodeText(newNode,sTag,sVal,
                                False)
        self.doc.AlignNode(self.nodeSettings)
    def OnTreeItemSel(self,event):
        event.Skip()
        if event.GetTreeNodeData() is not None:
            self.selNode=event.GetTreeNodeData()
    def OnGcbbLinkButton(self,event):
        if self.selNode is None:
            return
        ti=self.trlstMap.FindTreeItem(self.selNode)
        if self.selInt>=0:
            sTag=self.lstInterData.GetItem(self.selInt,0).m_text
            sVal=self.lstInterData.GetItem(self.selInt,1).m_text
        else:
            sTag=''
            sVal=''
        sComp=self.txtComp.GetValue()
        if self.VERBOSE:
            vtLog.CallStack('')
            print self.trlstMap.GetPyData(ti)
        self.trlstMap.SetItemText(ti,sVal,2)
        self.trlstMap.SetItemText(ti,sComp,3)
        self.txtComp.SetValue('')
        pass
    def GetNodes(self):
        return self.lstNewNodes
    def OnGcbbOkButton(self, event):
        if self.thdRead.IsRunning():
            self.thdRead.Stop()
        elif self.thdBuild.IsRunning():
            self.thdBuild.Stop()
        else:
            self.EndModal(1)
        event.Skip()
    def OnGcbbCancelButton(self, event):
        if self.thdRead.IsRunning():
            self.thdRead.Stop()
        elif self.thdBuild.IsRunning():
            self.thdBuild.Stop()
        else:
            self.EndModal(0)
        event.Skip()

    def __openOfficeFile__(self,bForce=False,bStartImport=False):
        try:
            if bForce==False:
                if self.chcAutoOpen.GetValue()==0:
                    self.appl=None
                    return
            if self.appl is not None:
                return
            r=self.gProc.GetRange()/2
            self.gProc.SetValue(r)
            filename=self.txtFN.GetValue()
            self.appl=vtOfficeSCalc()
            if 1==0:
                # try threading
                self.thdOpen.SetFN(self.appl,filename,bStartImport)
            else:
                # none threading
                self.__logInfo__('open:%r'%(filename))
                self.appl.openFile(filename)
                shts=self.appl.getSheetNames()
                self.chcSht.Clear()
                for s in shts:
                    self.chcSht.Append(s)
                self.appl.selSheet()
                self.gProc.SetValue(0)
        except:
            self.__logTB__()
    def OnOpenFin(self,evt):
        self.appl=evt.GetAppl()
        shts=self.appl.getSheetNames()
        self.chcSht.Clear()
        for s in shts:
            self.chcSht.Append(s)
        self.appl.selSheet()
        self.gProc.SetValue(0)
        if self.thdOpen.bStartImport:
            self.__startImport__()
            pass
    def OnGcbbBrowseFNButton(self, event):
        dlg = wx.FileDialog(self, _("Open"), ".", "", _("Excel files (*.xls)|*.xls|StarOffice files (*.sxc)|*.sxc|all files (*.*)|*.*"), wx.OPEN)
        try:
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.txtFN.SetValue(filename)
                self.__openOfficeFile__()
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()
    def __showRowInfo__(self):
        self.lstInpRaw.DeleteAllItems()
        try:
            i=0
            for item in self.infos[self.spRow.GetValue()]:
                index = self.lstInpRaw.InsertImageStringItem(sys.maxint, '%d'%i, -1)
                self.lstInpRaw.SetStringItem(index,1,item,-1)
                i=i+1
        except:
            pass
        self.__showIntRowInfo__()
    def __getIntRowInfo__(self,iRow,d={}):
        keys=self.dInt.keys()
        keys.sort()
        row=self.infos[iRow]
        for key in keys:
            val=self.dInt[key]
            sVal=''
            iLen=len(val)
            j=0
            while (j<iLen) and (j>=0):
                j,k=self.__getColAdr__(val,j)
                if j!=-1:
                    iCol=int(val[j:k])
                    sVal+=row[iCol]
                    j=k+1
            d[key]=sVal
        return d
    def __showIntRowInfo__(self):
        keys=self.dInt.keys()
        keys.sort()
        i=0
        row=self.infos[self.spRow.GetValue()]
        for key in keys:
            val=self.dInt[key]
            sVal=''
            iLen=len(val)
            j=0
            while (j<iLen) and (j>=0):
                j,k=self.__getColAdr__(val,j)
                if j!=-1:
                    iCol=int(val[j:k])
                    sVal+=row[iCol]
                    j=k+1
            self.dVal[key]=sVal
            try:
                self.lstInternal.SetStringItem(i,2,sVal)
            except:
                pass
            i+=1
        i=0
        for key in keys:
            try:
                l=self.dFormel[key]
            except:
                l=[]
            if self.oCalc is not None:
                sRes=self.oCalc.Calc(self.dVal[key],self.dVal,l)
                self.lstInternal.SetStringItem(i,2,sRes)
            else:
                self.lstInternal.SetStringItem(i,2,'')
            i+=1
    def __showRowBuild__(self,bClr=True):
        if self.infos is not None:
            dK={}
            keys=self.dKey.keys()
            keys.sort()
            for k in keys:
                dKeys=self.dKey[k]
                kkeys=dKeys.keys()
                kkeys.sort()
                for kk in kkeys:
                    sVal=dKeys[kk]
                    if dK.has_key(sVal)==False:
                        dK[sVal]=1
            keys=dK.keys()
            keys.sort()
            if bClr:
                self.lstRows.ClearAll()
                self.lstRows.InsertColumn(col=0, 
                        format=wx.LIST_FORMAT_RIGHT,
                        heading=_(u'Row'), width=80)
                iCol=1
                for k in keys:
                    self.lstRows.InsertColumn(col=iCol, 
                                format=wx.LIST_FORMAT_LEFT,
                                heading=k, width=80)
                    iCol+=1
            iLen=len(self.infos)
            dVal={}
            for i in xrange(0,iLen):
                if bClr:
                    idx=self.lstRows.InsertStringItem(i,'%03d'%i)
                self.__getIntRowInfo__(i,dVal)
                iCol=1
                for k in keys:
                    if k in self.dFormel:
                        l=self.dFormel[k]
                        if (len(l)>0) and (self.oCalc is not None):
                            sRes=self.oCalc.Calc(dVal[k],dVal,l)
                        else:
                            sRes=dVal[k]
                    else:
                        sRes=dVal[k]
                    self.lstRows.SetStringItem(i,iCol,sRes)
                    iCol+=1
    def OnGcbbImportButton(self, event):
        #self.__openOfficeFile__(True,True)
        self.__startImport__()
    def __startImport__(self):
        self.lstRows.DeleteAllItems()
        self.gcbbOk.Enable(False)
        self.gcbbBrowseFN.Enable(False)
        self.gcbbImport.Enable(False)
        #self.gcbbLink.Enable(False)
        self.gcbbBuild.Enable(False)
        self.cbSet.Enable(False)
        #self.cbAddInt.Enable(False)
        self.cbDel.Enable(False)
        #self.cbDelInt.Enable(False)
        self.cbDelTmpl.Enable(False)
        self.cbClear.Enable(False)
        
        iRowStart=self.intRowStart.GetValue()-1
        iRowEnd=self.intRowStop.GetValue()-1
        iColStart=self.intColStart.GetValue()-1
        iColEnd=self.intColEnd.GetValue()-1
        sSht=self.chcSht.GetStringSelection()
        fn=self.txtFN.GetValue()
        self.thdRead.ReadData(fn,sSht,iRowStart,iRowEnd,iColStart,iColEnd)
    def OnImportFin(self,evt):
        self.gProc.SetValue(0)
        self.txtTmEst.SetValue(u'')
        pass
    def OnImportProgress(self,evt):
        self.txtTmElapsed.SetValue(evt.GetElapsed())
        self.txtTmEst.SetValue(evt.GetEstimate())
        self.gProc.SetRange(evt.GetCount())
        self.gProc.SetValue(evt.GetValue())
    def OnReadFin(self,evt):
        self.gcbbOk.Enable(True)
        self.gcbbBrowseFN.Enable(True)
        self.gcbbImport.Enable(True)
        #self.gcbbLink.Enable(True)
        self.gcbbBuild.Enable(True)
        self.cbSet.Enable(True)
        #self.cbAddInt.Enable(True)
        self.cbDel.Enable(True)
        #self.cbDelInt.Enable(True)
        self.cbDelTmpl.Enable(True)
        self.cbClear.Enable(True)

        self.infos=evt.GetInfos()
        self.txtTmEst.SetValue(u'')
        self.spRow.SetValue(0)
        self.gProc.SetValue(0)
        if self.infos is not None:
            self.__showRowInfo__()
            self.spRow.SetRange(0,len(self.infos))
        else:
            self.spRow.SetRange(0,0)
        self.__showRowBuild__()
        evt.Skip()
    def OnReadProgress(self,evt):
        self.txtTmElapsed.SetValue(evt.GetElapsed())
        self.txtTmEst.SetValue(evt.GetEstimate())
        self.gProc.SetRange(evt.GetCount())
        self.gProc.SetValue(evt.GetValue())
    def __isParent__(self,node,baseNode):
        par=self.doc.getParent(node)
        if par is None:
            return False
        if self.doc.isSame(par,baseNode):
            return True
        else:
            return self.__isParent__(par,baseNode)
    def __getTreeItemValCmp__(self,ti):
        sVal=self.trlstMap.GetItemText(ti,2)
        sComp=self.trlstMap.GetItemText(ti,3)
        strs=string.split(sComp,':')
        if len(strs)==2:
            try:
                iRow=int(strs[0])
            except:
                iRow=0
            return (sVal,strs[1],iRow)
        return (sVal,sComp,0)
    def __startTime__(self):
        self.zStart=time.clock()
    def __showTime__(self,act,count):
        def __calcTime__(zVal):
            zSec=zVal%60
            zMin=zVal/60
            zVal-=zSec
            zHr=zMin/24
            zMin=zMin%24
            return u'%d:%02d:%02d'%(zHr,zMin,zSec)
        zAct=time.clock()
        zDiff=zAct-self.zStart
        self.txtTmElapsed.SetValue(__calcTime__(zDiff))
        if act==0:
            self.txtTmEst.SetValue('')
        else:
            zDiff=zDiff*count/act
            self.txtTmEst.SetValue(__calcTime__(zDiff))
        
    def OnGcbbBuildButton(self, event):
        lRow=[]
        iLen=self.lstRows.GetItemCount()
        for i in xrange(0,iLen):
            if self.lstRows.GetItemState(i,wx.LIST_STATE_SELECTED)!=0:
                lRow.append(i)
        if self.VERBOSE:
            vtLog.CallStack('')
            print 'rows',lRow
            print 'map'
            vtLog.pprint(self.dMap)
        self.gProc.SetValue(0)
        self.gProc.SetRange(len(lRow))
        #self.thdBuild.Do(self.nodeImpBase,lRow)
        self.thdBuild.Do(self.node,lRow)
        return
        baseNode=None
        lst=self.trlstMap.GetNodesAttrs(check=[2])
        lstInst=[]
        def __attr2dict__(attrs):
            d={}
            for a in attrs:
                tag=self.doc.getTagName(a[1])
                d[tag]=self.__getTreeItemValCmp__(a[0])
            return d
        self.gProc.SetValue(0)
        self.gProc.SetRange(len(lst))
        i=0
        for it in lst:
            i+=1
            self.gProc.SetValue(i)
            n=it[0]
            id=self.doc.getAttribute(n,'id')
            if baseNode is None:
                baseNode=n
                baseAttr=__attr2dict__(it[1])
                lstNodes=[]
            else:
                if self.__isParent__(n,baseNode)==True:
                    lstNodes.append((n,it[1]))
                else:
                    ti=self.trlstMap.FindTreeItem(baseNode)
                    tup=self.__getTreeItemValCmp__(ti)
                    d={'node':baseNode,'valCmp':tup,'attr':baseAttr,'lstNodes':lstNodes}
                    lstInst.append(d)
                    baseNode=n
                    baseAttr=__attr2dict__(it[1])
                    lstNodes=[]
        if baseNode is not None:
            ti=self.trlstMap.FindTreeItem(baseNode)
            tup=self.__getTreeItemValCmp__(ti)
            d={'node':baseNode,'valCmp':tup,'attr':baseAttr,'lstNodes':lstNodes}
            lstInst.append(d)
        def __procVal__(sVal,iRow):
            try:
                row=self.infos[iRow]
            except:
                return None
            strs=string.split(sVal,'+')
            lst=[]
            for s in strs:
                if len(s)>2:
                    if (s[0]=='$') and (s[-1]=='$'):
                        v=row[int(s[1:-1])]
                        lst.append(v)
                    else:
                        lst.append(s)
                else:
                    lst.append(s)
            return string.join(lst,'')
        def __isMatch__(val,cmp):
            if len(cmp)>0:
                for s in string.split(cmp,','):
                    if fnmatch.fnmatch(val,s)==True:
                        return True
                return False
            else:
                return True
        iMax=self.spRow.GetMax()
        self.lstNewNodes=[]
        self.lstMatched=[]
        self.lstMissed=[]
        self.__startTime__()
        self.__showTime__(0,iMax)
        self.gProc.SetValue(0)
        self.gProc.SetRange(self.spRow.GetMax())
        for iRow in range(0,iMax):
            self.gProc.SetValue(iRow+1)
            self.__showTime__(iRow,iMax)
            iRowAdd=0
            try:
                row=self.infos[iRow]
            except:
                continue
            bFound=False
            itc=None
            for it in lstInst:
                tupValCmp=it['valCmp']
                val=__procVal__(tupValCmp[0],iRow)
                if val is None:
                    continue
                if __isMatch__(val,tupValCmp[1]):
                    bFound=True
                    #itc=copy.deepcopy(it)
                    itc={}
                    itc['node']=it['node']
                    itc['attr']=copy.deepcopy(it['attr'])
                    itc['lstNodes']=it['lstNodes']
                    self.lstMatched.append(tupValCmp)
                    break
                else:
                    self.lstMissed.append(tupValCmp)
            if itc is not None:
                iRowAdd=0
                itc['val']=val
                d=itc['attr']
                for k in d.keys():
                    val=__procVal__(d[k][0],iRow+iRowAdd)
                    d[k]=val
                lst=[]
                for it in itc['lstNodes']:
                    ti=self.trlstMap.FindTreeItem(it[0])
                    tup=self.__getTreeItemValCmp__(ti)
                    val=__procVal__(tup[0],iRow+tup[2])
                    if val is None:
                        continue
                    if __isMatch__(val,tup[1]):
                        if tup[2]>iRowAdd:
                            iRowAdd=tup[2]
                        d=__attr2dict__(it[1])
                        for k in d.keys():
                            val=__procVal__(d[k][0],iRow+tup[2])
                            if val is not None:
                                if __isMatch__(val,d[k][1]):
                                    d[k]=val
                                else:
                                    del d[k]
                        lst.append((it[0],d))
                itc['nodes']=lst
                self.lstNewNodes.append(itc)
            iRow+=iRowAdd
        self.gProc.SetValue(0)
        self.txtTmEst.SetValue(u'')
        self.txtNodeCount.SetValue(str(len(self.lstNewNodes)))
        event.Skip()
    def OnChcShtChoice(self, event):
        idx=event.GetSelection()
        s=self.chcSht.GetString(idx)
        self.appl.selSheet(s)
        event.Skip()
    def OnFiltersChanged(self,event):
        event.Skip()
        if self.selInt>=0:
            k=self.lstInternal.GetItem(self.selInt).m_text
            self.dFormel[k]=event.GetFormel()
        if self.VERBOSE:
            vtLog.CallStack('')
            vtLog.pprint(self.dVal)
            vtLog.pprint(self.dFormel)
    def OnLstInternalListColClick(self, event):
        event.Skip()
        self.selInt=-1
        self.txtName.SetValue('')
        self.viCalc.Enable(False)
    def OnLstInternalListItemDeselected(self, event):
        event.Skip()
        self.selInt=-1
        self.txtName.SetValue('')
        self.viCalc.Enable(False)
    def OnLstInternalListItemSelected(self, event):
        event.Skip()
        self.selInt=event.GetIndex()
        k=self.lstInternal.GetItem(self.selInt).m_text
        if self.VERBOSE:
            vtLog.CallStack('')
            print k
            vtLog.pprint(self.dVal)
            vtLog.pprint(self.dFormel)
        
        self.txtName.SetValue(k)
        try:
            self.viCalc.SetValue(self.dVal[k],self.dVal)
            try:
                l=self.dFormel[k]
            except:
                l=[]
                self.dFormel[k]=l
            self.viCalc.SetFormel(l)
            self.viCalc.Calc()
            self.viCalc.Enable(True)
        except:
            vtLog.vtLngTB(self.GetName())
            self.viCalc.Enable(False)
    def OnCbAddInternalButton(self, event):
        event.Skip()
        s=self.txtName.GetValue()
        if self.selInpRaw<0:
            #FIXME message
            return
        if len(s)<=0:
            #FIXME message
            return
        if self.dInt.has_key(s):
            self.dInt[s]=self.dInt[s]+'+$%d$'%self.selInpRaw
        else:
            self.dInt[s]='$%d$'%self.selInpRaw
        self.__showInternal__()
        self.__showIntRowInfo__()
    def OnCbDelInternalButton(self, event):
        event.Skip()
        if self.selInt>=0:
            k=self.lstInternal.GetItem(self.selInt,0).m_text
            del self.dInt[k]
            self.__showInternal__()
    def __getColAdr__(self,s,iStart=0):
        i=string.find(s,'$',iStart)
        if i>=0:
            j=string.find(s,'$',i+1)
            if j>0:
                return (i+1,j)
        return (-1,-1)
    def OnCbDelButton(self, event):
        if self.selTi is not None:
            tid=self.trlstMap.GetPyData(self.selTi)
            if tid is not None:
                lBase=None
                if (type(tid)==types.TupleType):
                    #if not self.dKey.has_key(self.selRegObj.GetTagName()):
                    #    self.dKey[self.selRegObj.GetTagName()]={}
                    #d=self.dKey[self.selRegObj.GetTagName()]
                    try:
                        tipd=self.trlstMap.GetPyData(self.trlstMap.GetItemParent(self.trlstMap.GetItemParent(self.selTi)))
                        if type(tipd)==types.ListType:
                            lBase=[]
                            for o in tipd[:-1]:
                                lBase.append(o.GetTagName())
                    except:
                        lBase=None
                    #d=self.__getKeyDict__(self.dKey,self.selRegObj.GetTagName())
                d=self.__getTagDict__(self.dMap,lBase,self.selRegObj.GetTagName())
                #if not self.dMap.has_key(self.selRegObj.GetTagName()):
                #    self.dMap[self.selRegObj.GetTagName()]={}
                del d['attr'][self.selTagName]
                self.trlstMap.SetItemText(self.selTi,'',1)
                self.trlstMap.SetItemText(self.selTi,'',2)
                self.trlstMap.SetItemText(self.selTi,'',3)
                if self.VERBOSE:
                    vtLog.CallStack('')
                    vtLog.pprint(self.dMap)
                    vtLog.pprint(self.dKey)
        event.Skip()
    def OnCbSetButton(self, event):
        event.Skip()
        try:
            if self.VERBOSE:
                vtLog.CallStack('')
                print self.selRegObj
                vtLog.pprint(self.dMap)
                print self.trlstMap.GetPyData(self.selTi)
                print self.chcKey.GetValue()
            self.trlstMap.GetSelection()
            if self.selElem<0:
                return 
            k=self.chcValue.GetStringSelection()
            tid=self.trlstMap.GetPyData(self.selTi)
            sCmp=sKey=''
            lBase=None
            if tid is not None:
                if (type(tid)==types.TupleType):
                    #if not self.dKey.has_key(self.selRegObj.GetTagName()):
                    #    self.dKey[self.selRegObj.GetTagName()]={}
                    #d=self.dKey[self.selRegObj.GetTagName()]
                    try:
                        tipd=self.trlstMap.GetPyData(self.trlstMap.GetItemParent(self.trlstMap.GetItemParent(self.selTi)))
                        if type(tipd)==types.ListType:
                            lBase=[]
                            for o in tipd[:-1]:
                                lBase.append(o.GetTagName())
                    except:
                        lBase=None
                    d=self.__getKeyDict__(self.dKey,self.selRegObj.GetTagName())
                    if self.chcKey.GetValue():
                        sKey=_(u'key')
                        d[self.selTagName]=k
                        self.__showRowBuild__(True)
                    else:
                        try:
                            del d[self.selTagName]
                            self.__showRowBuild__(True)
                        except:
                            pass
                else:
                    sCmp=self.txtComp.GetValue()
            if self.selRegObj is None:
                return
            d=self.__getTagDict__(self.dMap,lBase,self.selRegObj.GetTagName())
            #if not self.dMap.has_key(self.selRegObj.GetTagName()):
            #    self.dMap[self.selRegObj.GetTagName()]={}
            #d=self.dMap[self.selRegObj.GetTagName()]
            if self.selTagName is None:
                d['']=[k,sCmp,sKey]
            else:
                d['attr'][self.selTagName]=[k,sCmp,sKey]
            self.trlstMap.SetItemText(self.selTi,k,1)
            self.trlstMap.SetItemText(self.selTi,sCmp,2)
            self.trlstMap.SetItemText(self.selTi,sKey,3)
            if self.VERBOSE:
                vtLog.pprint(self.dMap)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbDelIntButton(self, event):
        if self.selInt>=0:
            # delete
            self.lstInterData.DeleteItem(self.selInt)
            self.selInt=-1
        event.Skip()

    def OnSpRowSpinctrl(self, event):
        self.__showRowInfo__()
        event.Skip()

    def OnChcTmplChoice(self, event):
        idx=event.GetSelection()
        obj=self.chcTmpl.GetClientData(idx)
        self.__getTmplData__(obj)
        event.Skip()

    def OnCbDelTmplButton(self, event):
        idx=self.chcTmpl.GetSelection()
        if idx<0:
            return
        if self.nodeSettings is None:
            return
        n=self.chcTmpl.GetClientData(idx)
        self.doc.deleteNode(n)
        self.chcTmpl.Delete(idx)
        event.Skip()

    def OnCbClearButton(self, event):
        self.lstNewNodes=[]
        self.txtNodeCount.SetValue('0')
        event.Skip()

    def OnLxtInpRowListColClick(self, event):
        self.selInpRaw=-1
        vtLog.CallStack('')
        event.Skip()

    def OnLxtInpRowListItemDeselected(self, event):
        self.selInpRaw=-1
        event.Skip()

    def OnLxtInpRowListItemSelected(self, event):
        self.selInpRaw=event.GetIndex()
        event.Skip()

    def OnCbAddTmplButton(self, event):
        self.GetNode()
        event.Skip()

    def OnCbReadTmplButton(self, event):
        self.__openOfficeFile__(True,True)
        event.Skip()

    def OnCbSaveTmplButton(self, event):
        self.GetNode()
        event.Skip()

    def OnTrlstMapTreeSelChanged(self, event):
        event.Skip()
        ti=event.GetItem()
        self.selTi=ti
        if ti is None:
            self.selRegObj=None
            self.selTagName=None
        tid=self.trlstMap.GetPyData(ti)
        if tid is not None:
            if type(tid)==types.TupleType:
                self.selRegObj=tid[0]
                self.selTagName=tid[1]
                self.chcKey.Enable(True)
                self.txtComp.Enable(False)
                #self.trlstMap.GetItemText(self.selTi,k,1)
                sVal=self.trlstMap.GetItemText(self.selTi,2)
                if sVal==_(u'key'):
                    self.chcKey.SetValue(True)
                else:
                    self.chcKey.SetValue(False)
            else:
                self.selRegObj=tid
                self.chcKey.Enable(False)
                self.chcKey.SetValue(False)
                self.txtComp.Enable(True)
                sVal=self.trlstMap.GetItemText(self.selTi,2)
                self.txtComp.SetValue(sVal)
            self.cbSet.Enable(True)
            self.cbDel.Enable(True)
        else:
            self.cbSet.Enable(False)
            self.cbDel.Enable(False)
    def OnChcValueChoice(self, event):
        event.Skip()
        self.selElem=self.chcValue.GetSelection()
        if self.selElem>=0:
            k=self.chcValue.GetStringSelection()
            if self.VERBOSE:
                vtLog.CallStack('')
                print self.selElem,k
                vtLog.pprint(self.dVal)
                vtLog.pprint(self.dFormel)
        if self.selTi is not None:
            self.cbSet.Enable(self.selElem>=0)
            self.cbDel.Enable(True)
        else:
            self.cbSet.Enable(False)
            self.cbDel.Enable(False)
