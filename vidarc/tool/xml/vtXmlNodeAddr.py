#----------------------------------------------------------------------------
# Name:         vXmlNodeAddr.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060101
# CVS-ID:       $Id: vtXmlNodeAddr.py,v 1.10 2010/03/03 02:16:18 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlNodeBase import *
from vidarc.tool.loc.vLocCountries import COUNTRIES
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase

try:
    if vcCust.is2Import(__name__):
        from vidarc.tool.xml.vtXmlNodeAddrPanel import vtXmlNodeAddrPanel
        #from vidarc.tool.xml.vtXmlNodeAddrEditDialog import *
        #from vidarc.tool.xml.vtXmlNodeAddrAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vtXmlNodeAddr(vtXmlNodeBase):
    NODE_ATTRS=[
            ('Name',None,'name',None),
            ('Street',None,'street',None),
            ('City',None,'city',None),
            ('Zip',None,'zip',None),
            ('Country',None,'country',None),
            ('Region',None,'region',None),
            ('Distance',None,'distance',None),
            #('Coor',None,'coor',None),
        ]
    NODE_ATTRS_REQ=['street','city','zip']
    def __init__(self,tagName='address'):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'address')
    # ---------------------------------------------------------
    # specific
    def GetCity(self,node):
        return self.Get(node,'city')
    def GetCountry(self,node):
        return self.Get(node,'country')
    def GetDistance(self,node):
        return self.Get(node,'distance')
    def GetName(self,node):
        return self.Get(node,'name')
    def GetRegion(self,node):
        return self.Get(node,'region')
    def GetStreet(self,node):
        return self.Get(node,'street')
    def GetZip(self,node):
        return self.Get(node,'zip')
    def GetCoor(self,node):
        return self.Get(node,'coor')
    
    def SetCity(self,node,val):
        return self.Set(node,'city',val)
    def SetCountry(self,node,val):
        return self.Set(node,'country',val)
    def SetDistance(self,node,val):
        return self.Set(node,'distance',val)
    def SetName(self,node,val):
        return self.Set(node,'name',val)
    def SetRegion(self,node,val):
        return self.Set(node,'region',val)
    def SetStreet(self,node,val):
        return self.Set(node,'street',val)
    def SetZip(self,node,val):
        return self.Set(node,'zip',val)
    def SetCoor(self,node,val):
        return self.Set(node,'coor',val)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        return [('city',vtXmlFilterType.FILTER_TYPE_STRING),
            ('country',vtXmlFilterType.FILTER_TYPE_STRING),
            ('name',vtXmlFilterType.FILTER_TYPE_STRING),
            ('region',vtXmlFilterType.FILTER_TYPE_STRING),
            ('street',vtXmlFilterType.FILTER_TYPE_STRING),
            ('zip',vtXmlFilterType.FILTER_TYPE_STRING),
            ('distance',vtXmlFilterType.FILTER_TYPE_INT),
            ]
    def GetTranslation(self,name):
        _(u'city'),_(u'country'),_(u'name'),_(u'region')
        _(u'street'),_(u'zip'),_(u'distance')
        return _(name)
    def Is2Create(self):
        return False
    def Is2Add(self):
        return True
    def IsSkip(self):
        return False
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x00\xe1IDAT(\x91}\x92Av\xc3@\x08C\xbf\x92\x1e\x8c\x9c\xc4G\x198Y\x9d\x93\
\xa9\x0b\xec\xf14i\xea\x05\x0fd=\x10\x1a\x04d\xe6K|/\'(\xc06\xa8*\x811\x06\
\x08\xfcg\xac\xca\x1bPU\xe01\xba\xd9G6\xb8\x7f\x1f\x13\xfe\xe5-\x132\xb3\xeb\
\x9e\xb32\x1a\x99xk\xbe\x01\'\x83\x17\xf6\xc8<\xa2ti\xceL\xfb\xf2\xc4\xc6v#\
\x99i0ti{\x9d0\x1d\xbc\xf6\x1b\xa7\xa7U\xd3\xbd\xa5k\xf7X\xf3\x04\xdb\xfe\
\xad\xe2k\xe9\x9as\x871\x0c\x1a\xed\x9e=\x0e\xbcl\xb7$ID\x84\xc4\'\xc7Z\x8f$\
\xcew8\xbe\xa9\xea=^g\xd2\xfc}\xdf;\x7f\xd9dE\x80; eDl\xdb&)\xe2{j\xe8\xbc*#\
\x1e\x8dHy\x07"\xa2][\xd9\x93\x17\xf1\x90\x90\xf4|\xc6t\xa9\xef\xef8^P\xcb\
\xadJ\xa8\xf5\xc03\xf3\x07\xcf\xbe\xeaG@2\x15e\x00\x00\x00\x00IEND\xaeB`\x82\
' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            #return vtXmlNodeAddrEditDialog
            return {'name':self.__class__.__name__,
                    'sz':(466, 356),'pnName':'pnLoc',
                }
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            #return vtXmlNodeAddrAddDialog
            return {'name':self.__class__.__name__,
                    'sz':(474, 356),'pnName':'pnLoc',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vtXmlNodeAddrPanel
        else:
            return None

