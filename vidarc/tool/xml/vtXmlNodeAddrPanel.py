#Boa:FramePanel:vtXmlNodeAddrPanel
#----------------------------------------------------------------------------
# Name:         vtXmlNodeAddrPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060207
# CVS-ID:       $Id: vtXmlNodeAddrPanel.py,v 1.19 2010/03/10 22:38:36 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import wx
    from wx.lib.anchors import LayoutAnchors

    from vidarc.tool.xml.vtXmlTree import *
    from vidarc.tool.loc.vLocCountries import COUNTRIES
    from vidarc.tool.xml.vtXmlNodePanel import *

    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)


[wxID_VTXMLNODEADDRPANEL, wxID_VTXMLNODEADDRPANELCHCCOUNTRY, 
 wxID_VTXMLNODEADDRPANELLBLCITY, wxID_VTXMLNODEADDRPANELLBLCOUNTRY, 
 wxID_VTXMLNODEADDRPANELLBLDIST, wxID_VTXMLNODEADDRPANELLBLDISTUNIT, 
 wxID_VTXMLNODEADDRPANELLBLNAME, wxID_VTXMLNODEADDRPANELLBLREGION, 
 wxID_VTXMLNODEADDRPANELLBLSTREET, wxID_VTXMLNODEADDRPANELLBLZIP, 
 wxID_VTXMLNODEADDRPANELSPNDIST, wxID_VTXMLNODEADDRPANELTXTCITY, 
 wxID_VTXMLNODEADDRPANELTXTNAME, wxID_VTXMLNODEADDRPANELTXTREGION, 
 wxID_VTXMLNODEADDRPANELTXTSTREET, wxID_VTXMLNODEADDRPANELTXTZIP, 
] = [wx.NewId() for _init_ctrls in range(16)]

class vtXmlNodeAddrPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_fgsAddr_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblName, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtName, 2, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblStreet, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtStreet, 3, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblZip, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsCity, 3, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblRegion, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsCountry, 3, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblDist, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsDist, 3, border=0, flag=wx.EXPAND)

    def _init_coll_bxsDist_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.spnDist, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.lblDistUnit, 0, border=0, flag=wx.BOTTOM)

    def _init_coll_bxsCity_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.txtZip, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblCity, 1, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.txtCity, 2, border=0, flag=wx.EXPAND)

    def _init_coll_fgsAddr_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)
        parent.AddGrowableCol(0)
        parent.AddGrowableCol(1)

    def _init_coll_bxsCountry_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.txtRegion, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblCountry, 1, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.chcCountry, 2, border=0, flag=wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.bxsCity = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.fgsAddr = wx.FlexGridSizer(cols=2, hgap=8, rows=7, vgap=8)

        self.bxsDist = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsCountry = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_bxsCity_Items(self.bxsCity)
        self._init_coll_fgsAddr_Items(self.fgsAddr)
        self._init_coll_fgsAddr_Growables(self.fgsAddr)
        self._init_coll_bxsDist_Items(self.bxsDist)
        self._init_coll_bxsCountry_Items(self.bxsCountry)

        self.SetSizer(self.fgsAddr)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTXMLNODEADDRPANEL,
              name=u'vtXmlNodeAddrPanel', parent=prnt, pos=wx.Point(221, 315),
              size=wx.Size(426, 219), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(418, 192))
        self.SetAutoLayout(True)

        self.lblName = wx.StaticText(id=wxID_VTXMLNODEADDRPANELLBLNAME,
              label=_(u'Name'), name=u'lblName', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(83, 21), style=wx.ALIGN_RIGHT)
        self.lblName.SetMinSize(wx.Size(-1, -1))

        self.txtName = wx.TextCtrl(id=wxID_VTXMLNODEADDRPANELTXTNAME,
              name=u'txtName', parent=self, pos=wx.Point(91, 0),
              size=wx.Size(327, 21), style=0, value=u'')
        self.txtName.SetMinSize(wx.Size(-1, -1))
        self.txtName.Bind(wx.EVT_TEXT, self.OnTxtNameText,
              id=wxID_VTXMLNODEADDRPANELTXTNAME)

        self.lblStreet = wx.StaticText(id=wxID_VTXMLNODEADDRPANELLBLSTREET,
              label=_(u'Street'), name=u'lblStreet', parent=self, pos=wx.Point(0,
              29), size=wx.Size(83, 76), style=wx.ALIGN_RIGHT)
        self.lblStreet.SetMinSize(wx.Size(-1, -1))

        self.txtStreet = wx.TextCtrl(id=wxID_VTXMLNODEADDRPANELTXTSTREET,
              name=u'txtStreet', parent=self, pos=wx.Point(91, 29),
              size=wx.Size(327, 76), style=wx.TE_MULTILINE, value=u'')
        self.txtStreet.SetMinSize(wx.Size(-1, -1))
        self.txtStreet.Bind(wx.EVT_TEXT, self.OnTxtStreetText,
              id=wxID_VTXMLNODEADDRPANELTXTSTREET)

        self.lblZip = wx.StaticText(id=wxID_VTXMLNODEADDRPANELLBLZIP,
              label=_(u'ZIP / Postal Code'), name=u'lblZip', parent=self,
              pos=wx.Point(0, 113), size=wx.Size(83, 21), style=wx.ALIGN_RIGHT)
        self.lblZip.SetMinSize(wx.Size(-1, -1))

        self.txtZip = wx.TextCtrl(id=wxID_VTXMLNODEADDRPANELTXTZIP,
              name=u'txtZip', parent=self, pos=wx.Point(91, 113),
              size=wx.Size(79, 21), style=0, value=u'')
        self.txtZip.SetMinSize(wx.Size(-1, -1))
        self.txtZip.Bind(wx.EVT_TEXT, self.OnTxtZipText,
              id=wxID_VTXMLNODEADDRPANELTXTZIP)

        self.lblCity = wx.StaticText(id=wxID_VTXMLNODEADDRPANELLBLCITY,
              label=_(u'City'), name=u'lblCity', parent=self, pos=wx.Point(170,
              113), size=wx.Size(79, 21), style=wx.ALIGN_RIGHT)
        self.lblCity.SetMinSize(wx.Size(-1, -1))

        self.txtCity = wx.TextCtrl(id=wxID_VTXMLNODEADDRPANELTXTCITY,
              name=u'txtCity', parent=self, pos=wx.Point(257, 113),
              size=wx.Size(159, 21), style=0, value=u'')
        self.txtCity.SetMinSize(wx.Size(-1, -1))
        self.txtCity.Bind(wx.EVT_TEXT, self.OnTxtCityText,
              id=wxID_VTXMLNODEADDRPANELTXTCITY)

        self.lblRegion = wx.StaticText(id=wxID_VTXMLNODEADDRPANELLBLREGION,
              label=_(u'Region'), name=u'lblRegion', parent=self,
              pos=wx.Point(0, 142), size=wx.Size(83, 21), style=wx.ALIGN_RIGHT)
        self.lblRegion.SetMinSize(wx.Size(-1, -1))

        self.txtRegion = wx.TextCtrl(id=wxID_VTXMLNODEADDRPANELTXTREGION,
              name=u'txtRegion', parent=self, pos=wx.Point(91, 142),
              size=wx.Size(79, 21), style=0, value=u'')
        self.txtRegion.SetMinSize(wx.Size(-1, -1))
        self.txtRegion.Bind(wx.EVT_TEXT, self.OnTxtRegionText,
              id=wxID_VTXMLNODEADDRPANELTXTREGION)

        self.lblCountry = wx.StaticText(id=wxID_VTXMLNODEADDRPANELLBLCOUNTRY,
              label=_(u'Country'), name=u'lblCountry', parent=self,
              pos=wx.Point(170, 142), size=wx.Size(79, 21),
              style=wx.ALIGN_RIGHT)
        self.lblCountry.SetMinSize(wx.Size(-1, -1))

        self.chcCountry = wx.Choice(choices=[],
              id=wxID_VTXMLNODEADDRPANELCHCCOUNTRY, name=u'chcCountry',
              parent=self, pos=wx.Point(257, 142), size=wx.Size(159, 21),
              style=0)
        self.chcCountry.SetMinSize(wx.Size(-1, -1))
        self.chcCountry.Bind(wx.EVT_CHOICE, self.OnChcCountryChoice,
              id=wxID_VTXMLNODEADDRPANELCHCCOUNTRY)

        self.lblDistUnit = wx.StaticText(id=wxID_VTXMLNODEADDRPANELLBLDISTUNIT,
              label=u'km', name=u'lblDistUnit', parent=self, pos=wx.Point(179,
              171), size=wx.Size(14, 21), style=0)
        self.lblDistUnit.SetMinSize(wx.Size(-1, -1))

        self.spnDist = wx.SpinCtrl(id=wxID_VTXMLNODEADDRPANELSPNDIST, initial=0,
              max=40000, min=0, name=u'spnDist', parent=self, pos=wx.Point(91,
              171), size=wx.Size(80, 21), style=wx.SP_ARROW_KEYS)
        self.spnDist.SetMinSize(wx.Size(-1, -1))
        self.spnDist.Bind(wx.EVT_TEXT, self.OnSpnDistText,
              id=wxID_VTXMLNODEADDRPANELSPNDIST)

        self.lblDist = wx.StaticText(id=wxID_VTXMLNODEADDRPANELLBLDIST,
              label=_(u'Distance'), name=u'lblDist', parent=self,
              pos=wx.Point(0, 171), size=wx.Size(83, 21), style=wx.ALIGN_RIGHT)
        self.lblDist.SetMinSize(wx.Size(-1, -1))

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,applName=_(u'vTool Address'),
                lWidgets=[self.txtName,self.txtStreet,self.txtZip,self.txtCity,
                        self.txtRegion,self.spnDist,self.chcCountry]
                )
        self.verbose=VERBOSE
        
        self.lstCountries=[]
        for k in COUNTRIES.keys():
            self.lstCountries.append((COUNTRIES[k],k))
        def cmpFunc(a,b):
            return cmp(a[0],b[0])
        self.lstCountries.sort(cmpFunc)
        
        for it in self.lstCountries:
            self.chcCountry.Append(it[0])
        
        self.objRegNode=None
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
    def SetNetDocs(self,d):
        pass
    def SetRegNode(self,obj):
        self.objRegNode=obj
    def OnChcCountryChoice(self, event):
        self.SetModified(True,self.chcCountry)
        event.Skip()
    def __isModified__Old(self):
        if self.bModified:
            return True
        return False
    def __Clear__(self):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            #self.bBlock=True
            #self.SetModified(False)
            #if self.doc is not None and self.node is not None:
            #    self.doc.endEdit(self.node)
            self.txtName.SetValue('')
            self.txtStreet.SetValue('')
            self.txtCity.SetValue('')
            self.txtZip.SetValue('')
            #self.chcCountry.SetSelection(-1)
            try:
                docGlb=self.doc.GetNetDoc('vGlobals')
                sCountry=docGlb.GetGlobal(['vLoc','location'],'globalsLoc','country')#,bAcquire=False)
                self.chcCountry.SetStringSelection(COUNTRIES[sCountry])
            except:
                pass
            self.txtRegion.SetValue('')
            self.spnDist.SetValue(0)
            #wx.CallAfter(self.__clearBlock__)
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
    def IsBusy(self):
        return False
    def Stop(self):
        pass
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        #if doc is not None:
        #    if bNet==True:
        #        EVT_NET_XML_GOT_CONTENT(doc,self.OnGotContent)
        #        EVT_NET_XML_GET_NODE(doc,self.OnGetNode)
        #        EVT_NET_XML_REMOVE_NODE(doc,self.OnRemoveNode)
        #        EVT_NET_XML_LOCK(doc,self.OnLock)
        #        EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        #        EVT_NET_XML_SET_NODE(doc,self.OnSetNode)
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            self.txtName.SetValue(self.doc.getNodeText(self.node,'name'))
            self.txtStreet.SetValue(self.doc.getNodeText(self.node,'street'))
            self.txtCity.SetValue(self.doc.getNodeText(self.node,'city'))
            self.txtZip.SetValue(self.doc.getNodeText(self.node,'zip'))
            try:
                sCountry=self.doc.getNodeText(self.node,'country')
                self.chcCountry.SetStringSelection(COUNTRIES[sCountry])
            except:
                try:
                    docGlb=self.doc.GetNetDoc('vGlobals')
                    sCountry=docGlb.GetGlobal(['vLoc','location'],'globalsLoc','country')#,bAcquire=False)
                    self.chcCountry.SetStringSelection(COUNTRIES[sCountry])
                except:
                    pass
            self.txtRegion.SetValue(self.doc.getNodeText(self.node,'region'))
            try:
                iDist=long(self.doc.getNodeText(self.node,'distance'))
            except:
                iDist=0
            self.spnDist.SetValue(iDist)
        except:
            self.__logTB__()
        #wx.CallAfter(self.__clearBlock__)
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            self.doc.setNodeText(node,'name',self.txtName.GetValue())
            self.doc.setNodeText(node,'street',self.txtStreet.GetValue())
            self.doc.setNodeText(node,'city',self.txtCity.GetValue())
            self.doc.setNodeText(node,'zip',self.txtZip.GetValue())
            try:
                i=self.chcCountry.GetSelection()
                sCountry=self.lstCountries[i][1]
            except:
                sCountry='ch'
            self.doc.setNodeText(node,'country',sCountry)
            self.doc.setNodeText(node,'region',self.txtRegion.GetValue())
            self.doc.setNodeText(node,'distance',str(self.spnDist.GetValue()))
        except:
            self.__logTB__()
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            self.txtName.Enable(False)
            self.txtStreet.Enable(False)
            self.txtCity.Enable(False)
            self.txtZip.Enable(False)
            self.chcCountry.Enable(False)
            self.txtRegion.Enable(False)
            self.spnDist.Enable(False)
        else:
            self.txtName.Enable(True)
            self.txtStreet.Enable(True)
            self.txtCity.Enable(True)
            self.txtZip.Enable(True)
            self.chcCountry.Enable(True)
            self.txtRegion.Enable(True)
            self.spnDist.Enable(True)

    def OnTxtNameText(self, event):
        if self.bBlock==False:
            self.SetModified(True,self.txtName)
        event.Skip()

    def OnTxtStreetText(self, event):
        if self.bBlock==False:
            self.SetModified(True,self.txtStreet)
        event.Skip()

    def OnTxtZipText(self, event):
        if self.bBlock==False:
            self.SetModified(True,self.txtZip)
        event.Skip()

    def OnTxtCityText(self, event):
        if self.bBlock==False:
            self.SetModified(True,self.txtCity)
        event.Skip()

    def OnTxtRegionText(self, event):
        if self.bBlock==False:
            self.SetModified(True,self.txtRegion)
        event.Skip()

    def OnSpnDistText(self, event):
        if self.bBlock==False:
            self.SetModified(True,self.spnDist)
        event.Skip()
            
