#----------------------------------------------------------------------------
# Name:         vtXmlNodeMessages.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060422
# CVS-ID:       $Id: vtXmlNodeMessages.py,v 1.7 2010/03/03 02:16:18 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeTag import *
try:
    if vcCust.is2Import(__name__):
        from vtXmlNodeMessagesPanel import vtXmlNodeMessagesPanel
        #from vtXmlNodeMessagesEditDialog import *
        #from vtXmlNodeMessagesAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vtXmlNodeMessages(vtXmlNodeTag):
    def __init__(self,tagName='messages'):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'messages')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return None
    def GetAttrFilterTypes(self):
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        return True
    def Is2Add(self):
        return False
    def IsMultiple(self):
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return True
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02\xc0IDAT8\x8d\x95\x93\xdfOSw\x18\xc6?\xdfsJ[ZJ\x8bsk\x19\xad\xd4R]\
\x16e\x0c\x94\xac("Z\xf1F]\xb6\xf8/x\xeb\x8d\x7f\x8f\xd1\xa9\xb7\xc4\x98\xcd\
m7#Y\xd0L\xb4\x8cV9PAZ\xa0\x1d\x19\x15z\xc0\xd3\x1ez\xd6__/\x8cL#^\xf8\\\xbf\
\xcf\'O\xde<\x8f\x10\x8a\xca\xc7\x94\xcb\xe7$Bp \x18\x12\x1f\xbb\x11{\x01\
\x1e..H-\xb7\xc2\x96\xbeI\xad\xd1\xc4\xdf\xd2J_0J,\xf6\xed\x07\xa0\x0f\x00\
\xb7f\x9f\xc9\xf1\xc2:\x85\xec\x02\xc5\xc2\x02\xa2\xb6\x8dk\xcb\xc9\xe1R/?\
\xc4{\xf1\xfb\x1b\xb8\\NN\x0e\x9f\x12\x00\xca\xbb\xe6\xf1\xe7\xf3rRX\xccX\
\x05R\xe6:k\xf5\xa7\x94\x95i\xdc\x01\x93\x83G\xdd\xb4\xb9\x9aP\xad\xa0i\x1a\
\xf9\xdc\xaa\x04\xb0\xbd5\xdfI%\xe5\xbdB\x91\xf9\x8d\x7f\xd9\xd1\x97p\xecd\
\x89\xb8$#\x818\xf1\x9e1\x8ev\x1e\xa6\xb4\x9c\xa0\x81\x8a\xaa\x08\xd2\xe94\
\xbb\x80d6#\xaf\xe7r\xcc\xd6\x05\xdb\xd2A\xc4\xe6c(t\x8cs\x91\xef\xe9\xeb>\
\xce>\x8f\x8bz\xb5\x8e\xb7\xa7\x9b\xa6\xc3F\xc5\xfe\x1f\x9a\xf6\xfc\x7f\xc0\
\xb2\xae\xb3h\x9a\xec(*?zZ\x88\x1f\xec\xa37\xd0\x81\xaf\xad\r\xc3\xf01\x95,\
\xb3Uje\xa8?H\xd7\xe7\nQ\xcad\xb3+\xfc\xfe\xdb\xaf\xd2\x06P0M,\xa1\xa0\xeaM"\
m^\xbe;\xe0\xc3\xebU\x98[\xec\xe0\xcf\'N\xd6\x8a*n\x97\x9dh\xc4\xc2\xbf\xdf\
\x8e\xa24\xf1\xfb\x038\x9d\xado\x128\xac\x1a\xddE\x8bb\xe23\x1e\xc9\x1a\xa3\
\xe1*\xde\xf6\x0ej\x8a\x03\xa3f\xc7\xdfi\xe7H\xb4\x8e\xcf\x03\xcff\x97y<\x95\
\xa4Z\xb58{\xf6\x0c\xb6\xc9\x9b?\xc9\xa9\xe94\xf9W\x15\xaa\x9aJ\x8b\xfe\x0f\
\x1b_C84\xca\xa1P\x051\xda\xc4\xa1V)m\xac\xf0\xf3\xf8,\x13\x13)\x8a\xc5\x97\
\\\xbdz\x99\xfe\x81c\xc2\x96\xbe}\x8bJ\xb0\x13\xd1\xbe\xc9\xa05\xc9\xf9\x17k\
\xbc\xbc\x1feu\xa0\x9f/\xc2\r\x9a\xc6&\x7f<\x9ecb"E\xea\x89\x9dr\xa9\x9d\x8b\
\x17\xfb\x88\xc5\x06\xdf<\xb1\xa3\xb0N\xd7v\x91\xa8\xd8\xa2\x917\xc9\x9aGH\
\xce\x1c\xe7\xc5=\x1dg \xc1\xe4\xdc_\xcc\xcc4\xd8\xc8\xb4\xa0Z>N\x8f\x84\xb8\
r\xe5\x04=\xd1\xb0\x00\x10K7n\xc8r"\x81\x91[%\xb1\xe3\xe6n&\xce\xdc\xe6(\x9e\
\xfd\x12\xa5s\x9c\x92\xe7\x01F6\x80\xbb\xfc%\xc3\x83]\\\xbb6\xc2\xb9\xb1\x81\
\xddJ\xefVY\xcf,I\xbdR\xe7\xd1\xdf\x05~\xb9o0=\xed\xc0\x94\xf3\xa8\xeey\x82\
\xfb\x02\x8c\x9d\xfe\x8aK\x17z\x89\r}\xf3\xde\x1e\xf6\x1cSf)\'5-\x8fal\xa3\
\xa8\r\xc2\xddAN\x0e\x0f\xec\xb9\xc8=\x01\x9f\xa2\xd7n\xe2&\x8e\x98n9\xe1\
\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            #return vtXmlNodeMessagesEditDialog
            return {'name':self.__class__.__name__,
                    'sz':(348, 174),'pnName':'pnNodeMessages',
                }
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            #return vtXmlNodeMessagesAddDialog
            return {'name':self.__class__.__name__,
                    'sz':(500, 420),'pnName':'pnNodeMessages',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vtXmlNodeMessagesPanel
        else:
            return None

