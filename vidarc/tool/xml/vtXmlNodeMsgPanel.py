#Boa:FramePanel:vtXmlNodeMsgPanel
#----------------------------------------------------------------------------
# Name:         vtXmlNodeMsgPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060422
# CVS-ID:       $Id: vtXmlNodeMsgPanel.py,v 1.9 2010/03/03 02:16:18 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputTextML
import vidarc.tool.input.vtInputTextMultiLineML
import vidarc.tool.time.vtTimeInputDateTime
import wx.lib.buttons

import sys

from vidarc.tool.xml.vtXmlNodePanel import *

import vidarc.tool.xml.vtXmlHierarchy as vtXmlHierarchy

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_VTXMLNODEMSGPANEL, wxID_VTXMLNODEMSGPANELCBGOTO, 
 wxID_VTXMLNODEMSGPANELCBPROCESSED, wxID_VTXMLNODEMSGPANELLBLDTTM, 
 wxID_VTXMLNODEMSGPANELLBLINFO, wxID_VTXMLNODEMSGPANELLBLSUBJECT, 
 wxID_VTXMLNODEMSGPANELVIINFO, wxID_VTXMLNODEMSGPANELVISUBJECT, 
 wxID_VTXMLNODEMSGPANELVITIME, 
] = [wx.NewId() for _init_ctrls in range(9)]

wxEVT_VTXML_MSG_PANEL_GOTO=wx.NewEventType()
vEVT_VTXML_MSG_PANEL_GOTO=wx.PyEventBinder(wxEVT_VTXML_MSG_PANEL_GOTO,1)
def EVT_VTMSG_PANEL_GOTO(win,func):
    win.Connect(-1,-1,wxEVT_VTXML_MSG_PANEL_GOTO,func)
def EVT_VTMSG_PANEL_GOTO_DISCONNECT(win,func):
    win.Disconnect(-1,-1,wxEVT_VTXML_MSG_PANEL_GOTO)
class vtXmlMsgPanelGoto(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTMSG_PANEL_GOTO(<widget_name>, self.OnGoto)
    """

    def __init__(self,obj,appl,alias,id):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTXML_MSG_PANEL_GOTO)
        self.obj=obj
        self.appl=appl
        self.alias=alias
        self.idNode=id
    def GetAppl(self):
        return self.appl
    def GetAlias(self):
        return self.alias
    def GetID(self):
        return self.idNode
    def GetInfos(self):
        return self.appl,self.alias,self.idNode
    def GetObject(self):
        return self.obj

wxEVT_VTXML_MSG_PANEL_VIEWED=wx.NewEventType()
vEVT_VTXML_MSG_PANEL_VIEWED=wx.PyEventBinder(wxEVT_VTXML_MSG_PANEL_VIEWED,1)
def EVT_VTMSG_PANEL_VIEWED(win,func):
    win.Connect(-1,-1,wxEVT_VTXML_MSG_PANEL_VIEWED,func)
def EVT_VTMSG_PANEL_VIEWED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,wxEVT_VTXML_MSG_PANEL_VIEWED)
class vtXmlMsgPanelViewed(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTMSG_PANEL_VIEWED(<widget_name>, self.OnGoto)
    """

    def __init__(self,obj,id):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTMSG_PANEL_VIEWED)
        self.obj=obj
        self.idNode=id
    def GetID(self):
        return self.idNode
    def GetObject(self):
        return self.obj

wxEVT_VTXML_MSG_PANEL_PROCESSED=wx.NewEventType()
vEVT_VTXML_MSG_PANEL_PROCESSED=wx.PyEventBinder(wxEVT_VTXML_MSG_PANEL_PROCESSED,1)
def EVT_VTMSG_PANEL_PROCESSED(win,func):
    win.Connect(-1,-1,wxEVT_VTXML_MSG_PANEL_PROCESSED,func)
def EVT_VTMSG_PANEL_PROCESSED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,wxEVT_VTXML_MSG_PANEL_PROCESSED)
class vtXmlMsgPanelProcessed(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTMSG_PANEL_PROCESSED(<widget_name>, self.OnGoto)
    """

    def __init__(self,obj,id):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTMSG_PANEL_PROCESSED)
        self.obj=obj
        self.idNode=id
    def GetID(self):
        return self.idNode
    def GetObject(self):
        return self.obj

class vtXmlNodeMsgPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_fgsMsg_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsSubject, 0, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbGoTo, 0, border=4, flag=wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxTime, 0, border=4,
              flag=wx.TOP | wx.LEFT | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsInfo, 1, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbProcessed, 0, border=4,
              flag=wx.ALIGN_CENTER | wx.RIGHT | wx.LEFT)

    def _init_coll_bxTime_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblDtTm, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.viTime, 3, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsSubject_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblSubject, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.viSubject, 3, border=4, flag=wx.EXPAND | wx.LEFT)

    def _init_coll_bxsInfo_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblInfo, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.viInfo, 3, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fgsMsg_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsMsg = wx.FlexGridSizer(cols=2, hgap=0, rows=4, vgap=0)

        self.bxsSubject = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxTime = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsInfo = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsMsg_Items(self.fgsMsg)
        self._init_coll_fgsMsg_Growables(self.fgsMsg)
        self._init_coll_bxsSubject_Items(self.bxsSubject)
        self._init_coll_bxTime_Items(self.bxTime)
        self._init_coll_bxsInfo_Items(self.bxsInfo)

        self.SetSizer(self.fgsMsg)

    def _init_ctrls(self, prnt ,id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=id, name=u'vtXmlNodeMsgPanel', parent=prnt,
              pos=wx.Point(579, 239), size=wx.Size(324, 205),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(316, 178))

        self.viSubject = vidarc.tool.input.vtInputTextML.vtInputTextML(id=wxID_VTXMLNODEMSGPANELVISUBJECT,
              name=u'viSubject', parent=self, pos=wx.Point(76, 0),
              size=wx.Size(200, 30), style=0)

        self.lblSubject = wx.StaticText(id=wxID_VTXMLNODEMSGPANELLBLSUBJECT,
              label=_(u'Subject'), name=u'lblSubject', parent=self,
              pos=wx.Point(4, 0), size=wx.Size(68, 30), style=wx.ALIGN_RIGHT)
        self.lblSubject.SetMinSize(wx.Size(-1, -1))

        self.lblDtTm = wx.StaticText(id=wxID_VTXMLNODEMSGPANELLBLDTTM,
              label=_(u'Time'), name=u'lblDtTm', parent=self, pos=wx.Point(4,
              34), size=wx.Size(68, 24), style=wx.ALIGN_RIGHT)
        self.lblDtTm.SetMinSize(wx.Size(-1, -1))

        self.viTime = vidarc.tool.time.vtTimeInputDateTime.vtTimeInputDateTime(id=wxID_VTXMLNODEMSGPANELVITIME,
              name=u'viTime', parent=self, pos=wx.Point(76, 34),
              size=wx.Size(200, 24), style=0)

        self.viInfo = vidarc.tool.input.vtInputTextMultiLineML.vtInputTextMultiLineML(id=wxID_VTXMLNODEMSGPANELVIINFO,
              name=u'viInfo', parent=self, pos=wx.Point(76, 62),
              size=wx.Size(200, 112), style=0)

        self.cbGoTo = wx.lib.buttons.GenBitmapButton(ID=wxID_VTXMLNODEMSGPANELCBGOTO,
              bitmap=vtArt.getBitmap(vtArt.Navigate), name=u'cbGoTo',
              parent=self, pos=wx.Point(281, 0), size=wx.Size(31, 30), style=0)
        self.cbGoTo.Bind(wx.EVT_BUTTON, self.OnCbGoToButton,
              id=wxID_VTXMLNODEMSGPANELCBGOTO)

        self.lblInfo = wx.StaticText(id=wxID_VTXMLNODEMSGPANELLBLINFO,
              label=_(u'Info'), name=u'lblInfo', parent=self, pos=wx.Point(4,
              62), size=wx.Size(68, 112), style=wx.ALIGN_RIGHT)
        self.lblInfo.SetMinSize(wx.Size(-1, -1))

        self.cbProcessed = wx.lib.buttons.GenBitmapButton(ID=wxID_VTXMLNODEMSGPANELCBPROCESSED,
              bitmap=vtArt.getBitmap(vtArt.Build), name=u'cbProcessed',
              parent=self, pos=wx.Point(281, 103), size=wx.Size(31, 30),
              style=0)
        self.cbProcessed.Bind(wx.EVT_BUTTON, self.OnCbProcessedButton,
              id=wxID_VTXMLNODEMSGPANELCBPROCESSED)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self._init_ctrls(parent,id)
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.docHum=None
        
        self.viSubject.SetEditable(False)
        self.viInfo.SetEditable(False)
        self.viTime.SetEditable(False)
        self.viSubject.SetTagName('subject')
        self.viTime.SetTagName('datetime')
        self.viInfo.SetTagName('desc')
        
        self.cbGoTo.Show(False)
        
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
    def ClearWid(self):
        try:
            vtXmlNodePanel.ClearWid(self)
            # add code here
            self.viSubject.Clear()
            self.viTime.Clear()
            self.viInfo.Clear()

            #self.cbEdit.Enable(False)
            self.cbProcessed.Enable(False)
            self.cbGoTo.Enable(False)
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def SetNetDocs(self,d):
        if d.has_key('vHum'):
            dd=d['vHum']
            self.docHum=dd['doc']
        # add code here
        
    def SetDoc(self,doc,bNet=False):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        
        # add code here
        self.viSubject.SetDoc(doc)
        self.viTime.SetDoc(doc)
        self.viInfo.SetDoc(doc)
    def SetNodeById(self,fid):
        if self.node is not None:
            self.GetNode()
        if self.doc is None:
            return
        self.SetNode(self.doc.getNodeByIdNum(fid))
        
    def SetNode(self,node):
        try:
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
                print node
            vtXmlNodePanel.SetNode(self,node)
            #self.doc.acquire()
            # add code here
            self.cbGoTo.Enable(node is not None)
            self.cbProcessed.Enable(node is not None)
            try:
                self.fid=long(self.doc.getAttribute(node,'fid'))
                n=self.doc.getNodeByIdNum(self.fid)
            except:
                self.fid=-1
            self.viSubject.SetNode(node)
            self.viTime.SetNode(node)
            self.viInfo.SetNode(node)
            
            self.doc.startEdit(node)
        except:
            vtLog.vtLngTB(self.GetName())
        #self.doc.release()
        
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if node is None:
                return
            #if self.VERBOSE:
            #    vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            #if self.doc is None:
            #    return
            #if node is None:
            #    if self.node is None:
            #        return
            #    node=self.node
            ##self.doc.acquire()
            try:
                # add code here
                pass
            except:
                self.__logTB__()
            ##self.doc.release()
            #self.doc.AlignNode(node,iRec=2)
            #vtXmlNodePanel.GetNode(self,node)
            self.GetNodeFin(node)
        except:
            vtLog.vtLngTB(self.GetName())
    def Close(self):
        # add code here
        pass
    def Lock(self,flag):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if flag:
            # add code here
            #self.cbEdit.Enable(False)
            self.cbProcessed.Enable(False)
        else:
            # add code here
            oMsg=self.doc.GetRegisteredNode('msg')
            oMsg.SetViewed(self.node)  # 090602:wro do not
            oMsg.doEdit(self.node)     # 090602;wro do not
            
            oMsg=self.doc.GetRegisteredNode('msg')
            if not oMsg.IsProcessed(self.node):
                self.cbProcessed.Enable(True)
            else:
                self.cbProcessed.Enable(False)
            #self.cbEdit.Enable(True)
            pass

    def OnCbGoToButton(self, event):
        vtLog.vtLngCurWX(vtLog.INFO,'',self)
        #fid=self.doc.getAttribute(self.node,'fid')
        #self.doc.SelectById(fid)
        event.Skip()
        if self.doc is None:
            return
        if self.node is None:
            return
        par=self.doc.getParent(self.node)
        sAlias=self.doc.getAttribute(par,'alias')
        vtLog.vtLngCurWX(vtLog.DEBUG,'alias:%d'%(sAlias),self)
        appl,alias='',''
        if len(sAlias)>0:
            strs=sAlias.split(':')
            if len(strs)==2:
                appl=strs[0]
                alias=strs[1]
                idNav=self.doc.getAttribute(self.node,'fid')
                vtLog.vtLngCurWX(vtLog.DEBUG,'idNav:%s'%(idNav),self)
                wx.PostEvent(self,vtXmlMsgPanelGoto(self,appl,alias,idNav))

    def OnCbEditButton(self, event):
        try:
            oMsg=self.doc.GetRegisteredNode('msg')
            fid=self.doc.getAttribute(self.node,'fid')
            tmp=self.doc.getNodeByIdNum(fid)
            #oMsg.SetViewed(self.node)
            #oMsg.DoEdit(tmp)
        except:
            pass
        event.Skip()
    def SetViewed(self):
        try:
            if self.node is not None:
                oMsg=self.doc.GetRegisteredNode('msg')
                oMsg.SetViewed(self.node)
                self.doc.doEdit(self.node)
        except:
            vtLog.vtLngTB(self.GetName())
    def SetProcessed(self):
        try:
            if self.node is not None:
                oMsg=self.doc.GetRegisteredNode('msg')
                oMsg.SetProcessed(self.node)
                self.doc.doEdit(self.node)
        except:
            pass
    def OnCbProcessedButton(self, event):
        try:
            self.SetProcessed()
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()
