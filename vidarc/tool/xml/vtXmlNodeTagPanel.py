#Boa:FramePanel:vtXmlNodeTagPanel
#----------------------------------------------------------------------------
# Name:         vtXmlNodeTagPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      
# CVS-ID:       $Id: vtXmlNodeTagPanel.py,v 1.21 2012/04/06 21:57:33 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import wx
    import vidarc.tool.input.vtInputTextMultiLineML
    import vidarc.tool.input.vtInputTextML
    import vidarc.tool.input.vtInputText
    import vidarc.tool.input.vtInputTextPopup
    import wx.lib.buttons
    
    import sys
    
    from vidarc.tool.xml.vtXmlNodePanel import *
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VTXMLNODETAGPANEL, wxID_VTXMLNODETAGPANELLBLDESC, 
 wxID_VTXMLNODETAGPANELLBLID, 
 wxID_VTXMLNODETAGPANELLBLNAME, wxID_VTXMLNODETAGPANELLBLTAG, 
 wxID_VTXMLNODETAGPANELLBLTYPE, wxID_VTXMLNODETAGPANELTXTTYPE, 
 wxID_VTXMLNODETAGPANELVIDESC, wxID_VTXMLNODETAGPANELVINAME, 
 wxID_VTXMLNODETAGPANELVITAG, 
] = [wx.NewId() for _init_ctrls in range(10)]

class vtXmlNodeTagPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblTag, (0, 0), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.viTag, (0, 1), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.lblName, (0, 2), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viName, (0, 3), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.lblDesc, (1, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viDesc, (1, 1), border=0, flag=wx.EXPAND, span=(1,
              3))
        parent.AddWindow(self.lblType, (2, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.lblID, (2, 1), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.txtType, (2, 2), border=0, flag=wx.EXPAND,
              span=(1, 2))

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=4, vgap=4)

        self._init_coll_gbsData_Items(self.gbsData)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=id, name=u'vtXmlNodeTagPanel', parent=prnt,
              pos=wx.Point(0, 0), size=wx.Size(323, 140),
              style=wx.TAB_TRAVERSAL)
        self.SetMinSize(wx.Size(-1, -1))
        self.SetClientSize(wx.Size(315, 113))
        self.SetAutoLayout(True)

        self.lblTag = wx.StaticText(id=wxID_VTXMLNODETAGPANELLBLTAG,
              label=_(u'Tag'), name=u'lblTag', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(53, 32), style=wx.ALIGN_RIGHT)
        self.lblTag.SetMinSize(wx.Size(-1, -1))

        self.viTag = vidarc.tool.input.vtInputTextPopup.vtInputTextPopup(id=wxID_VTXMLNODETAGPANELVITAG,
              name=u'viTag', parent=self, pos=wx.Point(57, 0), size=wx.Size(55,
              32), style=0)
        self.viTag.SetMinSize(wx.Size(-1, -1))

        self.lblName = wx.StaticText(id=wxID_VTXMLNODETAGPANELLBLNAME,
              label=_(u'Name'), name=u'lblName', parent=self, pos=wx.Point(116,
              0), size=wx.Size(32, 32), style=wx.ALIGN_RIGHT)
        self.lblName.SetMinSize(wx.Size(-1, -1))

        self.viName = vidarc.tool.input.vtInputTextML.vtInputTextML(id=wxID_VTXMLNODETAGPANELVINAME,
              name=u'viName', parent=self, pos=wx.Point(152, 0),
              size=wx.Size(134, 32), style=0)

        self.lblDesc = wx.StaticText(id=wxID_VTXMLNODETAGPANELLBLDESC,
              label=_(u'Description'), name=u'lblDesc', parent=self,
              pos=wx.Point(0, 36), size=wx.Size(53, 34), style=wx.ALIGN_RIGHT)
        self.lblDesc.SetMinSize(wx.Size(-1, -1))

        self.viDesc = vidarc.tool.input.vtInputTextMultiLineML.vtInputTextMultiLineML(id=wxID_VTXMLNODETAGPANELVIDESC,
              name=u'viDesc', parent=self, pos=wx.Point(57, 36),
              size=wx.Size(229, 34), style=0)

        self.lblType = wx.StaticText(id=wxID_VTXMLNODETAGPANELLBLTYPE,
              label=_(u'Type'), name=u'lblType', parent=self, pos=wx.Point(0,
              74), size=wx.Size(53, 21), style=wx.ALIGN_RIGHT)
        self.lblType.SetMinSize(wx.Size(-1, -1))

        self.txtType = wx.TextCtrl(id=wxID_VTXMLNODETAGPANELTXTTYPE,
              name=u'txtType', parent=self, pos=wx.Point(57, 74),
              size=wx.Size(91, 21), style=wx.TE_READONLY, value=u'')
        #self.txtType.Enable(False)
        self.txtType.SetMinSize(wx.Size(-1, -1))
        
        self.lblID = wx.TextCtrl(id=wxID_VTXMLNODETAGPANELLBLID, name=u'lblID',
              parent=self, pos=wx.Point(152, 74), size=wx.Size(65, 21),
              style=wx.ALIGN_RIGHT|wx.TE_READONLY, value=u'00000000')
        #self.lblID.Enable(False)
        self.lblID.SetMinSize(wx.Size(-1, -1))

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self._init_ctrls(parent,id)
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.docHum=None
        
        self.viTag.SetTagName('tag')
        self.viName.SetTagName('name')
        self.viDesc.SetTagName('description')
        
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
        #self.gbsData.AddGrowableCol(0,2)
        self.gbsData.AddGrowableCol(1,1)
        #self.gbsData.AddGrowableCol(2,1)
        self.gbsData.AddGrowableCol(3,1)
        self.gbsData.AddGrowableCol(4,1)
        self.gbsData.AddGrowableRow(1,1)
        self.gbsData.Layout()
        #self.gbsData.FitInside(self)
        self.gbsData.Fit(self)
        #self.fgsData.FitInside(self)
        
    def __Clear__(self):
        try:
            # add code here
            self.viTag.Clear()
            self.viName.Clear()
            self.viDesc.Clear()
            self.txtType.SetValue('')
            self.lblID.SetValue(u'')
            #self.lblIID.SetValue(u'')
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        self.__logDebug__(''%())
        #self.doc=doc
        # add code here
        self.viTag.SetDoc(doc)
        self.viName.SetDoc(doc)
        self.viDesc.SetDoc(doc)
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viTag.SetNode(node)
            self.viName.SetNode(node)
            self.viDesc.SetNode(node)
            sTag=self.doc.getTagName(node)
            if len(sTag)>0:
                o=self.doc.GetRegisteredNode(sTag)
                if o is not None:
                    self.txtType.SetValue(o.GetDescription())
                else:
                    self.txtType.SetValue(' '.join([sTag,'?']))
            else:
                self.txtType.SetValue('')
            sID=u''
            
            if node is not None:
                iId=long(self.doc.getKey(node))
                if iId>=0:
                    try:
                        sID=self.doc.ConvertIdNum2Str(iId)
                    except:
                        pass
                if hasattr(self.doc,'getKeyInst'):
                    try:
                        iIID=self.doc.getKeyInst(node)
                        if iIID>0:
                            sIID=self.doc.ConvertIdNum2Str(iIID)
                            sID=''.join([sID,' (',sIID,')'])
                    except:
                        pass
            self.lblID.SetValue(sID)
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viTag.GetNode(node)
            self.viName.GetNode(node)
            self.viDesc.GetNode(node)
            
        except:
            self.__logTB__()
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viTag.Clear()
        self.viName.Clear()
        self.viDesc.Clear()
    def __Close__(self):
        # add code here
        if VERBOSE>0:
            self.__logDebug__(''%())
        self.viTag.Close()
        self.viName.Close()
        self.viDesc.Close()
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            # add code here
            self.Close()
            self.viTag.Enable(False)
            self.viName.Enable(False)
            self.viDesc.Enable(False)
        else:
            # add code here
            self.viTag.Enable(True)
            self.viName.Enable(True)
            self.viDesc.Enable(True)
    def IsModified(self):
        if self.viTag.IsModified():
            return True
        if self.viName.IsModified():
            return True
        if self.viDesc.IsModified():
            return True
        return vtXmlNodePanel.GetModified(self)
