#----------------------------------------------------------------------------
# Name:         vXmlNodeLog.py
# Purpose:      XML-node definition for logging information
#
# Author:       Walter Obweger
#
# Created:      20060203
# CVS-ID:       $Id: vtXmlNodeLog.py,v 1.12 2010/03/03 02:16:18 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)

from vidarc.tool.xml.vtXmlNodeBase import *
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase

try:
    if vcCust.is2Import(__name__):
        from vidarc.tool.xml.vtXmlNodeLogPanel import vtXmlNodeLogPanel
        #from vidarc.tool.xml.vtXmlNodeLogEditDialog import *
        #from vidarc.tool.xml.vtXmlNodeLogAddDialog import *
        GUI=1
        vLogFallBack.logDebug('GUI objects imported',__name__)
    else:
        GUI=0
        vLogFallBack.logDebug('GUI objects import skipped',__name__)
except:
    vLogFallBack.logTB(__name__)
    GUI=0
vLogFallBack.logDebug('import;done',__name__)

class vtXmlNodeLog(vtXmlNodeBase):
    NODE_ATTRS=[
            ('DtTm',None,'datetime',None),
            ('Name',None,'name',None),
            ('Desc',None,'desc',None),
            ('Usr','UsrId','usr','fid'),
        ]
    FUNCS_GET_SET_4_LST=['DtTm','Name','Desc','Usr','UsrId']
    HUM_INFOS=['surname','firstname']
    
    def __init__(self,tagName='log'):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'log')
    # ---------------------------------------------------------
    # specific
    def GetDtTm(self,node):
        return self.Get(node,'datetime')
    def GetName(self,node):
        return self.Get(node,'name')
    def GetDesc(self,node):
        return self.Get(node,'desc')
    def GetUsrID(self,node):
        return self.GetChildForeignKey(node,'usr','fid',appl='vHum')
    def SetDtTm(self,node,val):
        return self.Set(node,'datetime',val)
    def SetName(self,node,val):
        return self.Set(node,'name',val)
    def SetDesc(self,node,val):
        return self.Set(node,'desc',val)
    def SetUsr(self,node,val):
        return self.Set(node,'usr',val)
    def SetUsrID(self,node,val):
        return self.SetChildForeignKey(node,'usr','fid',val,appl='vHum')
    
    def GetUsrInfos(self,node):
        id=self.doc.getChildAttribute(node,'usr','fid')
        if id.find('@')<0:
            id+='@vHum'
        tag,d=self.doc.GetInfos(id,name=self.GetClsName())
        return tag,map(d.__getitem__,self.HUM_INFOS)
    def GetUsrInfosLst(self,node):
        try:
            id=self.doc.getChildAttribute(node,'usr','fid')
            if id.find('@')<0:
                id+='@vHum'
            tag,d=self.doc.GetInfos(id,name=self.GetClsName())
            netDoc,nodeTmp=self.doc.GetNode(id)
            if netDoc.IsNodeKeyValid(nodeTmp)==False:
                d['surname']=self.doc.getNodeText(node,'usr')
            return map(d.__getitem__,self.HUM_INFOS)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
            def conv(a):
                return '???'
            return map(conv,self.HUM_INFOS)
            
    # ---------------------------------------------------------
    # inheritance
    def __create__(self,node,*args,**kwargs):
        self.SetValue(node,*args,**kwargs)
    def SetInfos2Get(self):
        self.doc.SetInfos2Get(self.HUM_INFOS,foreign='vHum',
                name=self.GetClsName())
    def GetAttrFilterTypes(self):
        return [('name',vtXmlFilterType.FILTER_TYPE_STRING),
                ('desc',vtXmlFilterType.FILTER_TYPE_STRING),
                ('usr',vtXmlFilterType.FILTER_TYPE_STRING)]
    def Is2Create(self):
        return False
    def Is2Add(self):
        return False
    def IsSkip(self):
        return True
    def IsSkip2Collect(self):
        "do not display node in tree but collect"
        return True
    def IsId2Add(self):
        return True
    def UseBaseNode(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02%IDAT8\x8d\xa5SMk\x13Q\x14=o\xa6\x84T\xb3\xd0\x85v\x11\xd3\x11%.\x0c\
\x18&!\xe4\x03\xda\x8c\x0b\x15t\xa3(\xe9?\x08"5\xb3\x90VE\xb4\x8d\x0b\x15\
\xea\xc7/\xb0t!b\x166\x91\xb8i\xd0\xa6\xad\x12\xbb\xd14\x8b@1tUK\xa04\x93@\
\x1ef\x12b\xaf\x8b\xd8\xc9\x97\x14\xc1\x03\x17\x1e\x97{\xcf;\xf7\x9e\xf7\x18\
\x13D\xfc\x0f\x06z\x13S\x0f\xeeSo\x8es\x8eg\xcf_\xb0\xbf\x11\xb0=\x05\xe9\
\xc5\x8f\xe4\xf7\xfb\xa1iZ_\xd1\xee\xaf]4\x1bMd\xbed0\xf3t\x06\xd9\\\x8e\xf5\
\x11\xb4\n\x9b\xf4>y\x0f\x95\xb2\x8e\x03\x07\x05\xe8z\x9bD\xd7\x81\xb3\xc1\
\x1b\xd04\r\xe1\xeba\x83DdL\xe8\xb8\x8b\xa6\xc3\xe1\x878:\xe4\x84\xd5\xea\
\xc3\xb0\xcd\x0bI\xf2A\x92|\x18\x10\x9bx\xf5z\x12V\xab\x1d\xa5R\r\xd9\xb5\
\xb5h\x9f\x82\xe0\xe8\x08\xa5\xd3i\xcc\xbf\x9d\x80\xc5b\x81\xae\x03;;\x1c\
\x00`?\x15\x80t\xcc\x8d\xef\x85\x0c\x0e\x1f\xb2\xc3\xe3\xf7\xb6\xc6`\x82\xd8\
\x15\xf4\x07\xd5j\xd5\x88b\xb1H++\xf3t\xe7v\x80\x16?\xc4h=\xbfN.\xd9EL\x10\
\xfb]\x00\x80\xd4B\xd48W*@hl\n\x16\xcb98\x1cA$\xe2\x8fq\\\xf2@\x96eds\xb9n\
\x82\xe0\xe8\x88a\xe1\xf9\x0bS\xe0\x9cccc\x19\xb3/\'\xb0\xb9\x99\x81\xcd\x16\
\xc0\xb0\xcd\r\x00Po\xaa\x98\x9d\x9b\xeb&P\x14\xc5hN-D\xb1\xbd\xdd@\xa1\x90A\
hl\x1a\x97\xaf\xdcE\xa3\xd1\xc0\xbb\xf8\x13\x9c<\xe15z\xfal\xec\x1d\x87s\x8e\
|~\x19\x89\xf8#\xd8\xed\x01\\\xbct\x0b5^\x03\xe7\x1cN\xb7\xcc\x8c\xe5)\x8aB\
\x9d\xa8\xd7\xeb\xb4\xb5\xb5D\xbdK\x8d\xbd\x89\x91\x1aQi\xaf\xcf\x18A\x8d\
\xa8\xa8h\x15\x98\x07\xcd0\x0f\x9aa2\x99\xf0\xedk\n\x9f?-!t\xc4\x03\xd3\x90\
\x04\xf3\x19\'\x03\x80\xd0\xd5km\xa5\x9d\x16\xfa\xbc>R#*\x15\x7f\x14\xa9\\*S\
\xedg\xad\xa5&\x11\xa7\xd2\xea*1AD2\x99\xa4.\xeb\xf7\xfb\x8d\x91\xf1q\x02\
\x00\x97\xec\x82\xe3\xb4\xa3\xfdx:\xb0/\xc1\xbf\xe07\x8e\xd1\x07-\\Y\x1b\x7f\
\x00\x00\x00\x00IEND\xaeB`\x82'  
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            #return vtXmlNodeLogEditDialog
            return {'name':self.__class__.__name__,
                    'sz':(470, 350),'pnName':'pnLoc',
                }
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            #return vtXmlNodeLogAddDialog
            return {'name':self.__class__.__name__,
                    'sz':(470, 350),'pnName':'pnLoc',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vtXmlNodeLogPanel
        else:
            return None

