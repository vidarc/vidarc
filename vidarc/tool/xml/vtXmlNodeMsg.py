#----------------------------------------------------------------------------
# Name:         vtXmlNodeMsg.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060422
# CVS-ID:       $Id: vtXmlNodeMsg.py,v 1.6 2006/08/29 10:06:24 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        from vtXmlNodeMsgPanel import *
        #from vtXmlNodeMsgEditDialog import *
        #from vtXmlNodeMsgAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vtXmlNodeMsg(vtXmlNodeBase):
    STATUS_NAMES=['new','viewed','processed']
    NODE_ATTRS=[
            ('AuthorID','fid','author','fid'),
            ('RecipID','fid','tag','fid'),
            ('DtTm',None,'datetime',None),
            ('Prior',None,'prior',None),
            ('Origin',None,'origin',None),
            ('Status',None,'status',None),
            ('Subject',None,'subject',None),
            ('Desc',None,'desc',None),
        ]
    FUNCS_GET_SET_4_LST=['Prior','Status','Origin','Subject','AuthorID','DtTm',
            'Desc','Key']
    HUM_INFOS=['surname','firstname']
    
    def __init__(self,tagName='msg'):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'message')
    # ---------------------------------------------------------
    # specific
    def GetStatusIdx(self,sStatus):
        try:
            return self.STATUS_NAMES.index(sStatus)
        except:
            return -1
    def GetTag(self,node):
        return self.Get(node,'tag')
    def GetOrigin(self,node,lang=None):
        return self.GetML(node,'origin',lang=lang)
    def GetPrior(self,node):
        return self.Get(node,'prior')
    def GetStatus(self,node):
        return self.Get(node,'status')
    def IsViewed(self,node):
        return self.GetStatus(node)=='viewed'
    def IsProcessed(self,node):
        return self.GetStatus(node)=='processed'
    def GetSubject(self,node):
        return self.GetML(node,'subject')
    def GetRecipID(self,node):
        return self.GetChildForeignKey(node,'recipient','fid',appl='vHum')
    def GetRecip(self,node):
        return self.Get(node,'recipient')
    def GetDesc(self,node):
        return self.GetML(node,'desc')
    def GetFID(self,node):
        return self.GetAttr(node,'fid')
    def GetAuthorID(self,node):
        return self.GetChildForeignKey(node,'author','fid',appl='vHum')
    def GetDtTm(self,node):
        return self.Get(node,'datetime')
    def SetTag(self,node,val):
        self.Set(node,'tag',val)
    def SetOrigin(self,node,val,lang=None):
        self.SetML(node,'origin',val,lang=lang)
    def SetPrior(self,node,val):
        self.Set(node,'prior',val)
    def SetStatus(self,node,val):
        self.Set(node,'status',val)
    def SetViewed(self,node):
        self.SetStatus(node,'viewed')
    def SetProcessed(self,node):
        self.SetStatus(node,'processed')
    def SetSubject(self,node,val):
        return self.SetML(node,'subject',val)
    def SetDesc(self,node,val):
        return self.SetML(node,'desc',val)
    def SetFID(self,node,val):
        return self.SetAttr(node,'fid',val)
    def SetAuthorID(self,node,val):
        try:
            long(val)
        except:
            val=self.doc.GetLoginUsrId()
        return self.SetChildForeignKey(node,'author','fid',val,appl='vHum')
    def SetRecipID(self,node,val):
        try:
            long(val)
        except:
            val='-1'
        return self.SetChildForeignKey(node,'recipient','fid',val,appl='vHum')
    def SetDtTm(self,node,val):
        return self.Set(node,'datetime',val)
        
    def GetRecipientInfos(self,node):
        return self.GetHumInfos(node,'recipient')
    def GetHumInfos(self,node,tag):
        id=self.doc.getChildAttribute(node,tag,'fid')
        netDoc,nodeTmp=self.doc.GetNode(id)
        o=netDoc.GetRegByNode(nodeTmp)
        if o.GetTagName()=='group':
            return 'group',o.AddValuesToLst(nodeTmp)
        tag,d=self.doc.GetInfos(id,name=self.GetClsName())
        return tag,map(d.__getitem__,self.HUM_INFOS)
    def GetAuthorInfos(self,node):
        return self.GetHumInfos(node,'author')
    
    # ---------------------------------------------------------
    # inheritance
    def SetInfos2Get(self):
        if self.doc is None:
            return
        self.doc.SetInfos2Get(self.HUM_INFOS,foreign='vHum',
                name=self.GetClsName())
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        return [('tag',vtXmlFilterType.FILTER_TYPE_STRING),
            ('author',vtXmlFilterType.FILTER_TYPE_STRING),
            ('prior',vtXmlFilterType.FILTER_TYPE_INT),
            ('origin',vtXmlFilterType.FILTER_TYPE_STRING),
            ('status',vtXmlFilterType.FILTER_TYPE_STRING),
            ('subject',vtXmlFilterType.FILTER_TYPE_STRING),
            ('desc',vtXmlFilterType.FILTER_TYPE_STRING),
            ('datetime',vtXmlFilterType.FILTER_TYPE_DATE_TIME),]
    def GetTranslation(self,name):
        _(u'tag'),_(u'author'),_(u'prior'),_(u'origin'),_(u'status')
        _(u'subject'),_(u'desc')
        return _(name)
    def Is2Create(self):
        return False
    def Is2Add(self):
        return False
    def IsMultiple(self):
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return True
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\x98IDAT8\x8d\xed\x92\xbfkSq\x14\xc5?\xdf\x9bk\xf2\xf24E\xa5JM+Z\x1c\
c\x07\xf7\xba8K\xe9\xe0\xe4\xe2\xd4\xd5\xff\xc4\xc1\xc5\xd9Mq\xb5\x82:\x04qh\
;\n:\ti\x8b`\x87b\xd5h\xfa\xf2}\xf7}s\x9fC\xc4\x1f\x83\xa0t\xf5.g\xba\x87s8\
\x9f\x10\xa4\xc1QN\x8e\xf4\xfd\xdf\x00\x00\xed]\xee\xd5\xbd\xa5%\x04p@\x81\
\xf4\x17\xfa\xe8\xc1\xc3\x00\xa0\xab\xd7WX[\xbd\xc9\xa1C\xa7\xd3&?\x9ba\x11\
\x92\'\xc4+J\x80\t01\xacrv769q\xe6\x1c\xc3/\xa3\xfa\xd9\x93\xf5 \t\xe1\xf1\
\xbdu\x82\xd6\xf4\xef?go\xfb\x03I\x13\x95+\xa5NcZ\x80\x88@-\xbc\xdax\xca\xa7\
wo\xb9\xb4xqZAqn\xddY#\xd5\xc6\xca\xed\x1bX\xc3\xf0\xd2\xa8\x18O\xb3N\x1c\
\x1d\x03\x9a\x888\xf9\x95k\xd8\x859\x18\xecL\r\x92@\x14\x10Wv\x87{\x94_K\xac\
\x1c\x91$G\x89,\x9c\xefb-\x07\x03Sgy\xf9*YS\xe8\xf7_|O\xe0\x8e\xc4\x84\xa9\
\xd1\x8a5\x07\xa3\xcf\x1c\x1e\x0c\x89\xd5{\xb4vN\xb6\x9a\xec\xef\x7fdnq\x81\
\xacL\xc4\x1a\x9c\xec\xe7\n\x1e\x84(\t\x89Jl9V\x1a\xc7g;\xc8X\xa1\xd9\xe6\
\xf5\x9b\x01\xf1\x98QT\xa0m\xa7;\xdb\xfdm}\x15w\x9a\x95bn\xcc\xcf\xccp\xbaw\
\n&\x13\xdc\r\x8fJ\x9a\xef"U\xa2pG\x83\xe2$\xb4\xfe\x85\x83\x97[[X\xf3.$\xc5\
C\x02\xc0\x9d\x1f\\\xfcIw\xb6\x07\x00\x84 \r\xf2,\xab\xf3<\xff\'\x02\x8b\xa2\
\xa0\x881|\x03\xcf\xd0\xc1\x9e\x1c\x10\xf8\x8a\x00\x00\x00\x00IEND\xaeB`\x82\
' 
    def getSelImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02\x84IDAT8\x8d}\x93OL\xdcU\x10\xc7?\xbf\xc7c\xd9]\\\xd2\x164\xb8@Z\
\xa2\x96\x16\xcc"\x18\x82\xa9U\x1b5\x1cM\x0fzibz\xe2\xda\x8b\'\xb5Go\xa6\x12\
C\xfc\xd3\x83\xc4\x03\xd1\x9b\x89\x80F\x9bH[b\x10\x9bTT\xd0\x92\x9a-\xb4\xe5\
O\x8b@\x05ay\xfb~og<`\xa4\x8a0\xc9d.3\x9f\xf9\xced\x86\xc8\x94\xb1\x9b\xb7?\
\x91\xd3T*\xa5{\xe5\xecQ\xdc\xa2\xf7n\x7f\xab\x9f\xf7\xbf\xad\x95{A"S\xc6\
\x7f\xad5wX/\x0e|\xc4\xda\xda\n&2\x8c]\xfd\x95\x9e\x0f?\xe3h\xf3c\xf4\xf5\
\xf5G\xf7\xe7\xee\x00\xb4\xb5\x1e\xd1o\x06\xces\xed\x97\xdfHV\xedg\xfc\xe7)\
\x9a\x9a\xday\xfa\xc4\x0b\x80\xa1\xa3\xa3\x8d\xab?LlC\xfe%\xbb\xadE\x97o\x8e\
\xe8\x95\xcb\x9fj\xef;o\xea\x95\xef/\xa9\xaa\xaa.\xff\xaes\x93yUU\xfd\xe0\
\xbd\x1e\xfd\xdf\x11Zs\x87u\xf0\x93^\x06\xbe\x1c\xa5\xab\xebEj\x1b\x9bH\xacz\
\xf23\xd3\xac \x1c\xac\xaef~a\x81\xe6\x8e\'y\xee\xd9c\x8c\xff4\x15\x01D\x8f\
\xe7r\xda\xf9T\'\xdd\xaf\xbe\xc2\x9a\x8bi\xcc\xd6s\xfb\xee,\xb2\xe9\xb8;;K\
\xc3\xa1G\x99\xbes\x87\xeaL\x12Wn\x886\x84\x9bKs\xbcv\xe6\xf5-\xc0\xd97\xcej\
\xf7\xc9Sl\x08d2)\xd2\x0f%\xf1\x0e\x82\x04\x8c\xc4\x14\x01J@\xc9\xe3caf\xf4;\
\x1ex\xf0a\xde\xea;\xcf\xd7_\x0cE&`\x18|\x7f\x88\xc8*\xc3\x1f_`\xfe\xc6\x12\
\xc1\x06b\xb1\x14\xed\xd6\x9e|\x04\x0e\x03j\xf8q\xf4+\xee\xdd\xba\xce#\x8d\
\x87\x00\xb0\x16\xe1\xf4\xb9n\x82z^:\xf32\xbe\xcc#EO\xcc&\x04\xa0$\xd8M\xc0\
\x06\x1cB\xba\xedy\xfc\xc1Z\xc8Oo\x01\x82\x01g\xc0\x88efu\x9e\xe2\x9fE|q\x9d\
`\xd2X\x1c\xf5\rY|\x85\x80\x07o\x85\xe3\xc7\x9f!\x990\x0c\x0f_\xfa[\x81\x08\
\xc6\x05\xbc\xf5T8ey\xfd\x0f6\x96Wq\xf1\x1cV\x85}\x15\t\x16\x17W\xa8m\xac\'Y\
\x0c8\x05!\xf9\xcf\x19X\x89\x0c\xce\x04\x8c\xb3\xb8\n\xc1\x17=\x955\x19\xcc\
\xa6\x85D\x8a\x89\xc9<\xae\xdcS\x88\xc1\xa6\x84lM\x160\xdb\x00#B"\xb6x\xf1\
\xd4UUq\xa0e?\x94J\x88x\xc4YB]\x16\x13\x07\n"\xd8\xc8"\x04\xacn_\xae\x1d\x19\
\x1b\xc3\'\xde\x85`\x91(\x00 \xb2\xd5C\xd8=N\xdf\xc8o\xffB:\x99\xd4t:\xbd\
\xe3\xa9\xf6\xb2B\xa1@\xc1\xb9\xe8/3\xce*\xf9\x0e\xd7\x95\xcd\x00\x00\x00\
\x00IEND\xaeB`\x82' 

    def GetEditDialogClass(self):
        if GUI:
            return vtXmlNodeMsgEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return vtXmlNodeMsgAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vtXmlNodeMsgPanel
        else:
            return None

