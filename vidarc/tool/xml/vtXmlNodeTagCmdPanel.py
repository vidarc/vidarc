#Boa:FramePanel:vtXmlNodeTagCmdPanel
#----------------------------------------------------------------------------
# Name:         vtXmlNodeTagCmdPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060515
# CVS-ID:       $Id: vtXmlNodeTagCmdPanel.py,v 1.19 2012/07/09 15:05:32 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import vidarc.tool.xml.vtXmlNodeTagPanel

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VTXMLNODETAGCMDPANEL, wxID_VTXMLNODETAGCMDPANELCBAPPLY, 
 wxID_VTXMLNODETAGCMDPANELCBCANCEL, wxID_VTXMLNODETAGCMDPANELVITAG, 
] = [wx.NewId() for _init_ctrls in range(4)]

vtEVT_TOOL_XML_TAG_APPLY=wx.NewEventType()
vEVT_TOOL_XML_TAG_APPLY=wx.PyEventBinder(vtEVT_TOOL_XML_TAG_APPLY,1)
def EVT_TOOL_XML_TAG_APPLY(win,func):
    win.Connect(-1,-1,vtEVT_TOOL_XML_TAG_APPLY,func)
def EVT_TOOL_XML_TAG_APPLY_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtEVT_TOOL_XML_TAG_APPLY)
class vtXmlTagApply(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_TOOL_XML_OPEN_START(<widget_name>, xxx)
    """
    def __init__(self,obj,id):
        wx.PyEvent.__init__(self)
        self.obj=obj
        self.id=id
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtEVT_TOOL_XML_TAG_APPLY)
    def GetID(self):
        return self.id
    def GetEventObj(self):
        return self.obj

vtEVT_TOOL_XML_TAG_CANCEL=wx.NewEventType()
vEVT_TOOL_XML_TAG_CANCEL=wx.PyEventBinder(vtEVT_TOOL_XML_TAG_CANCEL,1)
def EVT_TOOL_XML_TAG_CANCEL(win,func):
    win.Connect(-1,-1,vtEVT_TOOL_XML_TAG_CANCEL,func)
def EVT_TOOL_XML_TAG_CANCEL_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtEVT_TOOL_XML_TAG_CANCEL)
class vtXmlTagCancel(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_TOOL_XML_OPEN_CANCEL(<widget_name>, xxx)
    """
    def __init__(self,obj,id):
        wx.PyEvent.__init__(self)
        self.obj=obj
        self.id=id
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtEVT_TOOL_XML_TAG_CANCEL)
    def GetID(self):
        return self.id
    def GetEventObj(self):
        return self.obj

vtEVT_TOOL_XML_TAG_SELECTED=wx.NewEventType()
vEVT_TOOL_XML_TAG_SELECTED=wx.PyEventBinder(vtEVT_TOOL_XML_TAG_SELECTED,1)
def EVT_TOOL_XML_TAG_SELECTED(win,func):
    win.Connect(-1,-1,vtEVT_TOOL_XML_TAG_SELECTED,func)
def EVT_TOOL_XML_TAG_SELECTED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtEVT_TOOL_XML_TAG_SELECTED)
class vtXmlTagSelected(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_TOOL_XML_TAG_SELECTED(<widget_name>, xxx)
    """
    def __init__(self,obj,id,lWid,oReg):
        wx.PyEvent.__init__(self)
        self.obj=obj
        self.id=id
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtEVT_TOOL_XML_TAG_SELECTED)
        self.lWid=lWid
        self.oReg=oReg
    def GetID(self):
        return self.id
    def GetEventObj(self):
        return self.obj
    def GetListWidget(self):
        return self.lWid
    def GetObjReg(self):
        return self.oReg

class vtXmlNodeTagCmdPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 0, border=4, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=4, flag=wx.TOP)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viTag, 1, border=4,
              flag=wx.EXPAND | wx.BOTTOM | wx.RIGHT | wx.TOP | wx.LEFT)
        parent.AddSizer(self.bxsBt, 1, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=2, hgap=0, rows=1, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt,id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=id,
              name=u'vtXmlNodeTagCmdPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(400, 144), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(392, 117))

        self.viTag = vidarc.tool.xml.vtXmlNodeTagPanel.vtXmlNodeTagPanel(id=wxID_VTXMLNODETAGCMDPANELVITAG,
              name=u'viTag', parent=self, pos=wx.Point(4, 4), size=wx.Size(300,
              109), style=0)
        self.viTag.SetMinSize((-1,-1))

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLNODETAGCMDPANELCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'),
              name=u'cbApply', parent=self, pos=wx.Point(312, 4),
              size=wx.Size(76, 30), style=0)
        self.cbApply.SetMinSize((-1,-1))
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VTXMLNODETAGCMDPANELCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLNODETAGCMDPANELCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(312, 38),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.SetMinSize((-1,-1))
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VTXMLNODETAGCMDPANELCBCANCEL)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name,applName='vtXmlNodeRegListBook'):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self.dWidDependent={}
        self.dObjReg={}
        self.sActTag=''
        self._init_ctrls(parent,id)
        vtXmlNodePanel.__init__(self,applName=applName)
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
        self.fgsData.Layout()
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        del self.dWidDependent
        del self.dObjReg
        vtXmlNodePanel.__del__(self)
    def Enable(self,flag):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        vtLog.vtLngCurWX(vtLog.DEBUG,'flag:%d'%(flag),self)
        wx.Panel.Enable(self,flag)
    def AddWidgetDependent(self,wid,obj):
        if obj is None:
            sTag=None
        else:
            sTag=obj.GetTagName()
        if not self.dWidDependent.has_key(sTag):
            self.dWidDependent[sTag]=[]
        self.dWidDependent[sTag].append(wid)
    def GetModified(self,bDependent=False):
        if vtXmlNodePanel.GetModified(self):
            return True
        if self.viTag.IsModified():
            return True
        if bDependent==False:
            return False
        if self.sActTag!='':
            lWid=self.dWidDependent[self.sActTag]
            for wid in lWid:
                try:
                    if wid is not None:
                        if wid.GetModified():
                            return True
                except:
                    vtLog.vtLngCurWX(vtLog.ERROR,'tag:%s;%s'%(self.sActTag,wid),self)
                    vtLog.vtLngTB(self.GetName())
        return False
    def __apply2Dependent__(self,sTag,method,*args,**kwargs):
        if not self.dWidDependent.has_key(sTag):
            if None in self.dWidDependent:
                sTag=None
            else:
                return
        lWid=self.dWidDependent[sTag]
        for wid in lWid:
            try:
                if wid is not None:
                    #sMethod=''.join(['__',method,'__'])
                    #if hasattr(wid,sMethod):
                    #    f=getattr(wid,sMethod)
                    #else:
                    if hasattr(wid,method):
                        f=getattr(wid,method)
                        f(*args,**kwargs)
            except:
                vtLog.vtLngTB(self.GetName())
    def __apply2DependentFB__(self,sTag,method,*args,**kwargs):
        if not self.dWidDependent.has_key(sTag):
            if None in self.dWidDependent:
                sTag=None
            else:
                return
        lWid=self.dWidDependent[sTag]
        for wid in lWid:
            try:
                if wid is not None:
                    sMethod=''.join(['__',method,'__'])
                    if hasattr(wid,sMethod):
                        f=getattr(wid,sMethod)
                        f(*args,**kwargs)
                    else:
                        if hasattr(wid,method):
                            f=getattr(wid,method)
                            f(*args,**kwargs)
                        else:
                            vtLog.vtLngCurWX(vtLog.WARN,'method:%s not found'%(method),self)
            except:
                vtLog.vtLngTB(self.GetName())
    def __showDependent__(self,sTag):
        try:
            if sTag not in self.dWidDependent:
                sTag=None
                if sTag not in self.dWidDependent:
                    if self.sActTag!='':
                        lWid=self.dWidDependent[self.sActTag]
                        for wid in lWid:
                            if wid is not None:
                                wid.Show(False)
                    self.sActTag=''
                    #self.Enable(False)
                    #self.Refresh()
                    wx.PostEvent(self,vtXmlTagSelected(self,self.nodeId,None,sTag))
                    return
            #self.Enable(True)
            if sTag=='':
                if self.sActTag!=sTag:
                    for lWid in self.dWidDependent.values():
                        for wid in lWid:
                            if wid is not None:
                                wid.Show(False)
                self.sActTag=sTag
                wx.PostEvent(self,vtXmlTagSelected(self,self.nodeId,None,sTag))
            else:
                if self.sActTag!=sTag:
                    if self.sActTag!='':
                        lWid=self.dWidDependent[self.sActTag]
                        for wid in lWid:
                            if wid is not None:
                                wid.Show(False)
                    #else:
                    #    self.Enable(True)
                    #    self.Refresh()
                    lWid=self.dWidDependent[sTag]
                    for wid in lWid:
                        if wid is not None:
                            wid.Show(True)
                    self.sActTag=sTag
                    wx.PostEvent(self,vtXmlTagSelected(self,self.nodeId,lWid,sTag))
        except:
            vtLog.vtLngTB(self.GetName())
    def SetRegNode(self,obj,*args,**kwargs):
        sTag=obj.GetTagName()
        self.dObjReg[sTag]=obj
        self.__apply2Dependent__(sTag,'SetRegNode',obj,*args,**kwargs)
        if self.VERBOSE:
            vtLog.CallStack('')
            vtLog.pprint(self.dObjReg)
    def __Clear__(self):
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
        self.viTag.__Clear__()
    def SetNetDocs(self,d):
        self.viTag.SetNetDocs(d)
        for k in self.dObjReg.keys():
            self.__apply2Dependent__(k,'SetNetDocs',d)
    #def SetDoc(self,doc,bNet=False):
    #    vtXmlNodePanel.SetDoc(self,doc,bNet)
    #    self.viTag.SetDoc(doc,bNet)
    #    for k in self.dObjReg.keys():
    #        self.__apply2Dependent__(k,'SetDoc',doc,bNet)
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        self.__logDebug__(''%())
        bDbg=True
        if VERBOSE>0:
            if self.__isLogDebug__():
                self.__logDebug__(''%())
                bDbg=True
        self.viTag.SetDoc(doc,bNet=bNet)
        if bDbg:
            vtLog.vtLngCurWX(vtLog.DEBUG,'dObjReg;%s'%(vtLog.pformat(self.dObjReg)),self)
            vtLog.vtLngCurWX(vtLog.DEBUG,'dWidDependent;%s'%(vtLog.pformat(self.dWidDependent)),self)
        for k,lWid in self.dWidDependent.iteritems():
            for wid in lWid:
                try:
                    if bDbg:
                        vtLog.vtLngCurWX(vtLog.DEBUG,'k:%s;cls:%s'%(k,wid.__class__.__name__),self)
                    if wid is not None:
                        if hasattr(wid,'SetDoc'):
                            f=getattr(wid,'SetDoc')
                            f(doc,bNet=bNet)
                        #else:
                        #    f=getattr(wid,'SetDoc')
                        #    f(doc,bNet=bNet)
                        #    f=getattr(wid,'SetNetDocs')
                        #    f(dDocs)
                except:
                    vtLog.vtLngTB(self.GetName())
    #def __SetNode__(self,node):
    #    pass
    def SetNode(self,node):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if self.node is not None:
            sTagOld=self.doc.getTagName(self.node)
            sTag=self.doc.getTagName(node)
            if sTagOld!=sTag:
                self.__apply2Dependent__(sTagOld,'SetNode',None)
            if len(sTag)==0:
                self.__apply2Dependent__(sTagOld,'Clear')
                vtXmlNodePanel.Clear(self)
                self.__showDependent__('')
                return
        #else:
        #    self.__apply2Dependent__(sTagOld,'Clear')
        #    vtXmlNodePanel.Clear(self)
        #    self.__showDependent__('')
        #    return
        vtXmlNodePanel.SetNode(self,node)
    def __SetNode__(self,node):
        self.__logDebug__(''%())
        if node is not None:
            self.viTag.__SetNode__(node)
            sTag=self.doc.getTagName(node)
            self.__showDependent__(sTag)
            self.__apply2Dependent__(sTag,'SetNode',node)
        else:
            self.__showDependent__('')
            self.viTag.__Clear__()
            #vtXmlNodePanel.Clear(self)
            #for sTag in self.dWidDependent.keys():
            #    self.__apply2Dependent__(sTag,'Clear')
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            self.viTag.__GetNode__(node)
            sTag=self.doc.getTagName(node)
            self.__apply2Dependent__(sTag,'GetNode',node)
        except:
            self.__logTB__()
    def GetNodeOld(self,node=None):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.viTag.GetNode(node)
        tmp=self.GetNodeStart(node)
        if tmp is not None:
            sTag=self.doc.getTagName(tmp)
            self.__apply2Dependent__(sTag,'GetNode',tmp)
        else:
            for k in self.dObjReg.keys():
                self.__apply2Dependent__(k,'Clear')
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.__logDebug__(''%())
        self.viTag.__Close__()
        if self.doc is not None:
            if self.node is not None:
                sTag=self.doc.getTagName(self.node)
                self.__apply2DependentFB__(sTag,'Close')

    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viTag.__Cancel__()
    def Lock(self,flag):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if flag:
            self.cbApply.Enable(False)
            self.cbCancel.Enable(False)
        else:
            self.cbApply.Enable(True)
            self.cbCancel.Enable(True)
    def Apply(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        vtLog.vtLngCurWX(vtLog.INFO,'',self)
        try:
            if self.node is not None:
                self.GetNode()
                id=self.viTag.nodeId
                wx.PostEvent(self,vtXmlTagApply(self,id))
        except:
            vtLog.vtLngTB(self.GetName())
    def Cancel(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        vtLog.vtLngCurWX(vtLog.INFO,'',self)
        try:
            if self.node is not None:
                sTag=self.doc.getTagName(self.node)
                self.__apply2Dependent__(sTag,'Cancel')
            self.SetNode(self.node)
            id=self.viTag.nodeId
            wx.PostEvent(self,vtXmlTagCancel(self,id))
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbApplyButton(self, event):
        event.Skip()
        self.Apply()
    def OnCbCancelButton(self, event):
        event.Skip()
        self.Cancel()
    def CreateRegisteredPanel(self,sReg,doc=None,bNet=True,
                    parent=None,id=-1,pos=wx.DefaultPosition,
                    sz=wx.DefaultSize,style=0,name=None):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        try:
            if doc is None:
                doc=self.doc
            if doc is None:
                vtLog.vtLngCurWX(vtLog.ERROR,'no doc set',self)
            oReg=doc.GetReg(sReg)
            if hasattr(oReg,'GetPanelClassGui'):
                pnCls=oReg.GetPanelClassGui()
            else:
                pnCls=oReg.GetPanelClass()
            if name is None:
                name='pn'+sReg
            if hasattr(pnCls,'GetWid'):
                pnObj=pnCls(parent=parent,id=id,pos=pos,size=sz,style=style,name=name)
                pn=pnObj.GetWid()
                pnObj.SetRegNode(oReg)
                pnObj.SetDoc(doc,bNet)
                self.AddWidgetDependent(pnObj,oReg)
            else:
                pn=pnCls(parent,id,pos=pos,size=sz,style=style,name=name)
                pn.SetRegNode(oReg)
                pn.SetDoc(doc,bNet)
                self.AddWidgetDependent(pn,oReg)
            return pn
        except:
            vtLog.vtLngTB(self.GetName())
        return None
    def GetSelRegName(self):
        return self.sActTag
