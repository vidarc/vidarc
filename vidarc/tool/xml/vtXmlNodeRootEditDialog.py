#Boa:Dialog:vtXmlNodeRootEditDialog
#----------------------------------------------------------------------------
# Name:         vtXmlNodeRootEditDialog.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060928
# CVS-ID:       $Id: vtXmlNodeRootEditDialog.py,v 1.1 2006/10/09 11:18:47 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
from vidarc.tool.xml.vtXmlNodeDialog import *

from vidarc.tool.xml.vtXmlNodeRootPanel import *
import vidarc.tool.lang.vtLgBase as vtLgBase

def create(parent):
    return vtXmlNodeRootEditDialog(parent)

[wxID_VTXMLNODEROOTEDITDIALOG, wxID_VTXMLNODEROOTEDITDIALOGCBAPPLY, 
 wxID_VTXMLNODEROOTEDITDIALOGCBCANCEL, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vtXmlNodeRootEditDialog(wx.Dialog,vtXmlNodeDialog):
    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsBt, (1, 0), border=4,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER, span=(1, 1))

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=0, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_gbsData_Items(self.gbsData)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTXMLNODEROOTEDITDIALOG,
              name=u'vtXmlNodeRootEditDialog', parent=prnt, pos=wx.Point(348,
              174), size=wx.Size(340, 220),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE,
              title=u'vtXmlNodeRoot Edit Dialog')
        self.SetMinSize(wx.Size(-1, -1))
        self.SetClientSize(wx.Size(332, 193))

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLNODEROOTEDITDIALOGCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'),
              name=u'cbApply', parent=self, pos=wx.Point(0, 24),
              size=wx.Size(76, 30), style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VTXMLNODEROOTEDITDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLNODEROOTEDITDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(108, 24),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VTXMLNODEROOTEDITDIALOGCBCANCEL)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self._init_ctrls(parent)
        vtXmlNodeDialog.__init__(self)
        try:
            self.doc=None
            self.node=None
            self.gbsData.AddGrowableCol(0)
            self.gbsData.AddGrowableRow(0)
            self.pn=vtXmlNodeRootPanel(self,id=wx.NewId(),pos=(0,0),size=(240,185),
                            style=0,name=u'pnNodeRoot')
            self.gbsData.AddWindow(self.pn, (0, 0), border=4,
                  flag=wx.BOTTOM | wx.TOP | wx.LEFT | wx.RIGHT | wx.EXPAND, span=(1, 1))
            self.gbsData.Fit(self)
        except:
            vtLog.vtLngTB('')

    def OnCbApplyButton(self,evt):
        evt.Skip()
        try:
            self.pn.Close()
            self.pn.GetNode(None)
            if self.IsModal():
                self.EndModal(1)
            else:
                self.Show(False)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbCancelButton(self,evt):
        evt.Skip()
        try:
            self.pn.Close()
            self.pn.Cancel()
            if self.IsModal():
                self.EndModal(0)
            else:
                self.Show(False)
        except:
            vtLog.vtLngTB(self.GetName())

