#Boa:FramePanel:vgpXmlList

from wxPython.wx import *
from xml.dom.minidom import Element
import string

import images

[wxID_VGPXMLLIST, wxID_VGPXMLLISTLCNODES, 
] = map(lambda _init_ctrls: wxNewId(), range(2))

class vgpXmlList(wxPanel):
    def _init_coll_boxSizer1_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lcNodes, 0, border=5, flag=wxEXPAND | wxALL)

    def _init_coll_lcNodes_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wxLIST_FORMAT_LEFT, heading=u'Type',
              width=40)
        parent.InsertColumn(col=1, format=wxLIST_FORMAT_LEFT, heading=u'Name',
              width=120)
        parent.InsertColumn(col=2, format=wxLIST_FORMAT_LEFT, heading=u'Value',
              width=150)
        parent.InsertColumn(col=3, format=wxLIST_FORMAT_LEFT, heading=u'Parent',
              width=-1)
        parent.InsertColumn(col=4, format=wxLIST_FORMAT_LEFT, heading=u'Attr00',
              width=-1)
        parent.InsertColumn(col=5, format=wxLIST_FORMAT_LEFT, heading=u'Attr01',
              width=-1)
        parent.InsertColumn(col=6, format=wxLIST_FORMAT_LEFT, heading=u'Attr02',
              width=-1)
        parent.InsertColumn(col=7, format=wxLIST_FORMAT_LEFT, heading=u'Attr03',
              width=-1)
        parent.InsertColumn(col=8, format=wxLIST_FORMAT_LEFT, heading=u'Attr04',
              width=-1)

    def _init_sizers(self):
        # generated method, don't edit
        self.boxSizer1 = wxBoxSizer(orient=wxVERTICAL)

        self._init_coll_boxSizer1_Items(self.boxSizer1)

        self.SetSizer(self.boxSizer1)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wxPanel.__init__(self, id=wxID_VGPXMLLIST, name=u'vgpXmlList',
              parent=prnt, pos=wxPoint(275, 164), size=wxSize(450, 199),
              style=wxTAB_TRAVERSAL)
        self.SetClientSize(wxSize(442, 172))
        self.SetToolTipString(u'xml List')

        self.lcNodes = wxListCtrl(id=wxID_VGPXMLLISTLCNODES, name=u'lcNodes',
              parent=self, pos=wxPoint(5, 5), size=wxSize(432, 160),
              style=wxLC_REPORT)
        self.lcNodes.SetToolTipString(u'xml Nodes')
        self.lcNodes.SetLabel(u'')
        self._init_coll_lcNodes_Columns(self.lcNodes)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        
        self.imgLstTyp=wxImageList(16,16)
        img=images.getElementBitmap()
        mask=wxMask(img,wxBLACK)
        img.SetMask(mask)
        self.imgElem=self.imgLstTyp.Add(img)
        self.lcNodes.SetImageList(self.imgLstTyp,wxIMAGE_LIST_SMALL)
        
    def __addItem2List(self,o,par="",level=0):
        iLen=self.lcNodes.GetItemCount()
        if o.nodeType == Element.ELEMENT_NODE:
            it=wxListItem()
            it.m_itemId=iLen
            it.m_mask=wxLIST_MASK_TEXT
            it.m_col=1
            it.m_format=0
            s="  "
            for i in range(level):
                s=s+"  "
            it.m_text=s+o.tagName
            self.lcNodes.InsertImageStringItem(iLen,"",self.imgElem)
            iLen=self.lcNodes.GetItemCount()
            self.lcNodes.SetItem(it)
            
            it.m_col=3
            it.m_text=par
            self.lcNodes.SetItem(it)
            if o.hasAttributes():
                keys=o.attributes.keys()
                iAttrs=len(o.attributes)
                if iAttrs>5:
                    iAttrs=5
                for i in range(0,iAttrs):
                    key=keys[i]
                    val=o.attributes[key]
                    it.m_col=4+i
                    it.m_text=key+'='+val.nodeValue
                    self.lcNodes.SetItem(it)
                    
                pass
        elif o.nodeType == Element.TEXT_NODE:
            #print 'text node'
            s=string.strip(o.nodeValue)
            if len(s)==0:
                return
            it=wxListItem()
            it.m_itemId=iLen-1
            it.m_mask=wxLIST_MASK_TEXT
            it.m_col=2
            it.m_format=0
            try:
                self.lcNodes.GetItem(it)
                it.m_text=it.m_text+s
            except:
                it.m_text=s
            
            self.lcNodes.SetItem(it)
            
            #self.lstNodeProp.InsertStringItem(iLen,s)

    def add2List(self,obj,par="",level=0):
        #print "add2List",par
        self.__addItem2List(obj,par,level)
        if obj.nodeType == Element.ELEMENT_NODE:
            if len(par)>0:
                par=par+":"+obj.tagName
            else:
                par=obj.tagName
        level=level+1
        for o in obj.childNodes:
            if o.nodeType == Element.ELEMENT_NODE:
                self.add2List(o,par,level)
            elif o.nodeType == Element.TEXT_NODE:
                if string.strip(o.nodeValue)==0:
                    level=level-1
                self.add2List(o,par,level)


    def SetNode(self,node):
        self.lcNodes.DeleteAllItems()
        try:
            node.nodeType
        except:
            return
        self.objTreeItemSel=node
        if node.nodeType == Element.ELEMENT_NODE:
            #self.txtElem.SetValue(node.tagName)
            self.add2List(node)
        elif node.nodeType == Element.TEXT_NODE:
            #self.txtElem.SetValue()
            self.add2List(node)

    def __updateList__(self):
        self.lcNodes.DeleteAllItems()
        o=self.objTreeItemSel
        if o.nodeType == Element.ELEMENT_NODE:
            #self.txtElem.SetValue(o.tagName)
            self.add2List(o)
        elif o.nodeType == Element.TEXT_NODE:
            #self.txtElem.SetValue()
            self.add2List(o)
        
