#----------------------------------------------------------------------------
# Name:         vtXmlGrpAdd2List.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtXmlGrpAdd2List.py,v 1.3 2007/07/28 14:43:34 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
from wx import NewEventType as wxNewEventType
from wx import PyEvent as wxPyEvent
from wx import PostEvent as wxPostEvent

import vtXmlDom as vtXmlDom
import vidarc.tool.log.vtLog as vtLog

import thread,types,sys


# defined event for vgpXmlTree item selected
wxEVT_THREAD_GROUP_ADD2LIST_ELEMENTS=wxNewEventType()
def EVT_THREAD_GROUP_ADD2LIST_ELEMENTS(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_GROUP_ADD2LIST_ELEMENTS,func)
class wxThreadGroupAdd2ListElements(wxPyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_GROUP_ADD2LIST_ELEMENTS(<widget_name>, self.OnItemSel)
    """

    def __init__(self,iVal):
        wxPyEvent.__init__(self)
        self.val=iVal
        self.SetEventType(wxEVT_THREAD_GROUP_ADD2LIST_ELEMENTS)
    def GetValue(self):
        return self.val

# defined event for vgpXmlTree item selected
wxEVT_THREAD_GROUP_ADD2LIST_ELEMENTS_FINISHED=wxNewEventType()
def EVT_THREAD_GROUP_ADD2LIST_ELEMENTS_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_GROUP_ADD2LIST_ELEMENTS_FINISHED,func)
class wxThreadGroupAdd2ListElementsFinished(wxPyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_GROUP_ADD2LIST_ELEMENTS_FINISHED(<widget_name>, self.OnItemSel)
    """
    def __init__(self):
        wxPyEvent.__init__(self)
        self.SetEventType(wxEVT_THREAD_GROUP_ADD2LIST_ELEMENTS_FINISHED)

# defined event for vgpXmlTree item selected
wxEVT_THREAD_GROUP_ADD2LIST_ELEMENTS_ABORTED=wxNewEventType()
def EVT_THREAD_GROUP_ADD2LIST_ELEMENTS_ABORTED(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_GROUP_ADD2LIST_ELEMENTS_ABORTED,func)
class wxThreadGroupAdd2ListElementsAborted(wxPyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_GROUP_ADD2LIST_ELEMENTS_ABORTED(<widget_name>, self.OnItemSel)
    """
    def __init__(self):
        wxPyEvent.__init__(self)
        self.SetEventType(wxEVT_THREAD_GROUP_ADD2LIST_ELEMENTS_ABORTED)


class thdGroupAdd2List:
    def __init__(self,doc=None,verbose=0):
        self.verbose=verbose
        self.doc=doc
        self.running=False
    def Add2List(self,grps,sorting,idxSum,lstCtrl,par):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.par=par
        self.sorting=sorting
        self.idxSum=idxSum
        self.lstCtrl=lstCtrl
        self.grps=grps
        self.iAct=0
        if self.par is not None:
            self.updateProcessbar=True
        else:
            self.updateProcessbar=False
        
        self.Start()
    def Clear(self):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.iAct=0
            
    def Show(self):
        self.bFind=False
        if self.running==False:
            self.Start()
    def Start(self):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.keepGoing = self.running = True
        thread.start_new_thread(self.Run, ())

    def Stop(self):
        self.keepGoing = False

    def IsRunning(self):
        return self.running
    
    def __showNodes__(self,lst):
        for node in lst:
            self.iAct+=1
            if self.updateProcessbar:
                wxPostEvent(self.par,wxThreadGroupAdd2ListElements(self.iAct))
            self.doc.acquire()
            tagName,infos=self.doc.getNodeInfos(node)
            try:
                lst=self.doc.__procGroup__(node,tagName,infos)
                self.lst.append(lst)
            except:
                self.Stop()
                self.doc.release()
                return -1
            self.doc.release()
        return 0
    def __procDict__(self,d,level,prefix):
            keys=d.keys()
            keys.sort()
            sumTotal=0.0
            for k in keys:
                v=d[k]
                if types.DictType==type(v):
                    sum=self.__procDict__(v,level+1,prefix+[k])
                    #idx=self.grp[level]
                else:
                    self.lst=[]
                    if self.__showNodes__(v)<0:
                        self.keepGoing = False
                        return
                    sum=self.doc.__outputNodes__(self.lst,prefix)
                sumTotal+=sum
                if self.keepGoing == False:
                    return 0
            return sumTotal
        
    def Run(self):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.Clear()
        
        
        if self.updateProcessbar:
            wxPostEvent(self.par,wxThreadGroupAdd2ListElements(0))
        self.sumTotal=self.__procDict__(self.grps,0,[])
        index = self.lstCtrl.InsertImageStringItem(sys.maxint, '', -1)
        if self.idxSum >= 0:
            self.lstCtrl.SetStringItem(index,self.idxSum,'%6.2f'%self.sumTotal,-1)
        
        self.keepGoing = self.running = False
        if self.updateProcessbar:
            wxPostEvent(self.par,wxThreadGroupAdd2ListElements(0))
            wxPostEvent(self.par,wxThreadGroupAdd2ListElementsFinished())
    

class vtXmlGroupAdd2List(vtXmlDom.vtXmlDom):
    def __init__(self,verbose=0):
        vtXmlDom.vtXmlDom.__init__(self)
        self.verbose=verbose
        self.groups={}
        self.outputs=[]
        self.grouping=[]
        self.groupingIdx=[]
        self.thdGroupAdd2List=thdGroupAdd2List(self,verbose=verbose-1)
    def setShow(self,showing):
        self.groups={}
        self.outputs=[]
        self.showing=showing
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
    def IsRunning(self):
        return self.thdGroupAdd2List.IsRunning()
    def __procGroup__(self,node,tagName,infos):
        lst=[]
        for grp in self.showing:
            k=grp[0]
            if grp[1] is None:
                val=infos[k]
            else:
                val=grp[1](infos[k])
            lst.append(val)
        return lst
    def __outputNodes__(self,lst,prefix):
        def compFunc(a,b):
            for i in self.sorting:
                res=cmp(a[i],b[i])
                if res!=0:
                    return res
            return 0
        lst.sort(compFunc)
        
        tupRes=[]
        for it in self.showing:
            tupRes.append(None)
        if self.idxSum>=0:
            tupRes[self.idxSum]=0.0
        iCount=len(self.showing)
        for tup in lst:
            for i in range(iCount):
                if i==self.idxSum:
                    tupRes[i]+=float(tup[i])
                else:
                    if tupRes[i] is None:
                        tupRes[i]=tup[i]
                    else:
                        if tupRes[i]!=tup[i]:
                            tupRes[i]='*'
            index = self.lstCtrl.InsertImageStringItem(sys.maxint, tup[0], -1)
            i=1
            for t in tup[1:]:
                self.lstCtrl.SetStringItem(index,i,t,-1)
                i+=1
        for i in range(iCount):
            if tupRes[i] is None:
                tupRes[i]=''
        index = self.lstCtrl.InsertImageStringItem(sys.maxint, tupRes[0], -1)
        i=1
        for t in tupRes[1:]:
            if self.idxSum==i:
                self.lstCtrl.SetStringItem(index,i,'%6.2f'%t,-1)
            else:
                self.lstCtrl.SetStringItem(index,i,t,-1)
            i+=1
        if self.idxSum>=0:
            sum=tupRes[self.idxSum]
        else:
            sum=0.0
        self.outputs.append([prefix,sum,lst,tupRes])
        index = self.lstCtrl.InsertImageStringItem(sys.maxint, '', -1)
        return sum

    def Add2List(self,master,groups,sorting,idxSum,lst,par=None):
        if self.IsRunning():
            return
        self.outputs=[]
        self.groups=groups
        self.master=master
        self.sorting=sorting
        self.idxSum=idxSum
        self.lstCtrl=lst
        self.lstCtrl.DeleteAllItems()
        self.thdGroupAdd2List.Add2List(groups,sorting,idxSum,lst,par)
    def GetGrouped(self):
        return self.groups
    def GetShowing(self):
        return self.showing
    def GetSorting(self):
        return self.sorting
    def GetOutputs(self):
        return self.outputs
    def SetGrouping(self,grouping,groupingIdx):
        self.grouping=grouping
        self.groupingIdx=groupingIdx
    def GetGrouping(self):
        return self.grouping
    def GetGroupingIdx(self):
        return self.groupingIdx
