#----------------------------------------------------------------------------
# Name:         vtXmlDom.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtXmlDom.py,v 1.131 2015/10/20 09:12:47 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:

    import sys

    import types,time
    import os,os.path,stat,threading

    import vidarc.config.vcCust as vcCust
    from vidarc.tool.vtThreadCore import vtThreadCore
    from vidarc.tool.misc.vtmThread import vtmThreadInterFace
    #from vidarc.tool.vtThread import vtThread
    import vidarc.tool.InOut.fnUtil as fnUtil
    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    import vidarc.tool.time.vtTimeLoadBound as vtLdBd
    from vidarc.tool.log.vtLogAuditTrail import vtLogAuditTrail
    from vidarc.tool.xml.vtXmlNodeBase import vtXmlNodeBase
    from vidarc.tool.sec.vtSecXmlAclDom import vtSecXmlAclDom
    from vidarc.tool.vtStateFlagSimple import vtStateFlagSimple
    from vtXmlDomCore import vtXmlDomCore

    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vpc.logTB(__name__)
    VERBOSE=0
vpc.logDebug('import;done',__name__)

VERBOSE_IDS=VERBOSE>20

import vidarc.config.vcCust as vcCust

def newSha():
        if sys.version_info >= (2,6,0):
            import hashlib
            return hashlib.sha1()
        else:
            import sha
            return sha.new()

class NodeIds:
    MAP_OFFLINE={'add':0x01,'edit':0x02,'del':0x04,'move':0x08}
    OFFLINE_MASK=0xff
    def __init__(self,origin,attr='id',fp='fp'):
        self.lastId=-1
        self.verbose=VERBOSE_IDS
        self.attr=attr
        self.Ids={}
        self.FPs={}
        self.offlineIds={}
        #self.offlineIds=[]
        #self.offlineContentIds=[]
        self.missingIdNodes=[]
        self.format="%08d"
        self.SetOrigin(origin)
    def __del__(self):
        #vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
        del self.Ids
        del self.FPs
        del self.offlineIds
        #del self.offlineContentIds
        del self.missingIdNodes
        del self.origin
    def SetOrigin(self,origin):
        self.origin=':'.join([origin,'NodeIds'])
    def SetFormat(self,fmt):
        self.format=fmt
    def GetLastId(self):
        return self.lastId
    def GetNextId(self):
        if self.lastId is None:
            self.lastId=1
        else:
            try:
                self.lastId=self.lastId+1
            except:
                return ''
        return self.lastId
    def IsIdUsed(self,id):
        if self.verbose:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,
                    'id:%s,ids:%s'%(id,self.Ids.keys()),
                    origin=self.origin)
        if id in self.Ids:
            o=self.Ids[id]
            return 1
        else:
            return 0
    def RemoveId(self,attrId):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%s'%(attrId),
                    origin=self.origin)
        if self.verbose:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,
                    'id:%s,ids:%s'%(attrId,self.Ids.keys()),
                    origin=self.origin)
        try:
            id=long(attrId)
            self.Ids[id]=None
            del self.Ids[id]
            del self.FPs[id]
            if id in self.offlineIds:
                del self.offlineIds[id]
            #if id in self.offlineIds:
            #    self.offlineIds.remove(id)
            #if id in self.offlineContentIds:
            #    self.offlineContentIds.remove(id)
        except:
            vtLog.vtLngTB(self.origin)
    def ClrOfflineIds(self):
        del self.offlineIds
        self.offlineIds={}
        #self.offlineIds=[]
    #def ClrOfflineContentIds(self):
    #    del self.offlineContentIds
    #    self.offlineContentIds=[]
    def AddOffline(self,id,action):
        i=action
        if id not in self.offlineIds:
            self.offlineIds[id]=i
        else:
            self.offlineIds[id]|=i
        return self.offlineIds[id]
    def AddOfflineStr(self,id,action):
        if action in self.MAP_OFFLINE:
            i=self.MAP_OFFLINE[action]
            if id not in self.offlineIds:
                self.offlineIds[id]=i
            else:
                self.offlineIds[id]|=i
            return self.offlineIds[id]
        else:
            raise LookupError(action)
        #if id not in self.offlineIds:
        #    self.offlineIds.append(id)
    #def AddOfflineContent(self,id):
    #    if id not in self.offlineContentIds:
    #        self.offlineContentIds.append(id)
    def __checkAndSet__(self,node,doc):
        self.CheckId(node,doc)
        self.SetNode(node,doc)
        return 1
    def SetNode(self,node,doc):
        if self.verbose:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,
                    'node:%s,ids:%s'%(node,self.Ids.keys()),
                    origin=self.origin)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,
                    'id:%s'%(doc.getAttribute(node,self.attr)),
                    origin=self.origin)
        #attrId=node.getAttribute('id')
        #if len(attrId)>0:
        #    self.CheckId(c)
        #    self.SetNode(c)
        if doc.__isSkip__(node):
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,
                    'id:%s;skip'%(doc.getAttribute(node,self.attr)),
                    origin=self.origin)
            return
        doc.procChildsKeys(node,self.__checkAndSet__,doc)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,
                    'id:%s;done'%(doc.getAttribute(node,self.attr)),
                    origin=self.origin)
        #childs=self.doc.getChilds(node)
        #for c in childs:
        #    attrId=self.doc.getAttribute(c,self.attr)
        #    if len(attrId)>0:
        #        self.CheckId(c,attrId)
        #        self.SetNode(c)
    def CheckIdDeep(self,node,doc):
        self.CheckId(node,doc)
        self.SetNode(node,doc)
    def CheckId(self,node,doc,attrId=None):
        #vtLog.CallStack('')
        if attrId is None:
            attrId=doc.getAttribute(node,self.attr)
        if self.verbose:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,
                    'id:%s,ids:%s'%(attrId,self.Ids.keys()),
                    origin=self.origin)
            #print node
            #print attrId
        bMissingId=False
        if len(attrId)<=0:
            bMissingId=True
        else:
            try:
                id=long(attrId)
                if self.IsIdUsed(id):
                    bMissingId=True
                else:
                    self.Ids[id]=node
                    #self.FPs[id]=self.doc.getFingerPrint(node)
                    self.__calc__(id,doc)
                    if id>self.lastId:
                        self.lastId=id
                    #    self.offlineContentIds.append(id)
                        #self.offlineIds.append(id)
            except:
                bMissingId=True
                vtLog.vtLngTB(self.origin)
        if bMissingId:
            self.missingIdNodes.append(node)
        return bMissingId
    def __clrOffline__(self,id,action,doc):
        if id in self.Ids:
            node=self.Ids[id]
            if action in self.MAP_OFFLINE:
                i=self.MAP_OFFLINE[action]
                if id in self.offlineIds:
                    iNum=self.offlineIds[id]
                    if (iNum&i)!=0:
                        iNum=iNum&(iNum^i)
                        if iNum!=0:
                            self.offlineIds[id]=iNum
                            doc.setAttribute(node,'offline','0x%02x'%iNum)
                        else:
                            doc.removeAttribute(node,'offline')
                            del self.offlineIds[id]
                else:
                    #raise LookupError(id)
                    pass
            else:
                if action==self.OFFLINE_MASK:
                    if id in self.offlineIds:
                        del self.offlineIds[id]
                    doc.removeAttribute(node,'offline')
                #raise LookupError(action)
                pass
            doc.removeAttribute(node,'offline_content')
        else:
            raise LookupError(id)
    def __calc__(self,id,doc):
        if id in self.Ids:
            node=self.Ids[id]
            #doc.clearFingerPrint(node)
            self.FPs[id]=doc.GetFingerPrintAndStore(node)
            self.offlineIds[id]=0
            sAttrOffline=doc.getAttribute(node,'offline')
            if len(sAttrOffline)>0:
                iAction=int(sAttrOffline,16)
                self.offlineIds[id]=iAction
            elif len(doc.getAttribute(node,'offline_content'))>0:
                self.AddOffline(id,self.MAP_OFFLINE['edit'])
        else:
            raise LookupError(id)
    def GetIdStr(self,attrId):
        if self.verbose:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,
                    'id:%s,ids:%s'%(attrId,self.Ids.keys()),
                    origin=self.origin)
        bMissingId=False
        if len(attrId)<=0:
            bMissingId=True
        else:
            try:
                try:
                    id=long(attrId)
                except:
                    if vtLog.vtLngIsLogged(vtLog.ERROR):
                        vtLog.vtLngCur(vtLog.ERROR,'id:%s'%(attrId),
                                self.origin)
                    return (-1,None)
                node=self.Ids[id]
                #if id>self.lastId:
                #    self.lastId=id
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'id:%s found'%(attrId),
                                self.origin)
                return (1,node)
            except:
                bMissingId=True
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s not found'%(attrId),
                                self.origin)
        return (0,None)
    def GetId(self,id):
        if self.verbose:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,
                    'id:%d,ids:%s'%(id,self.Ids.keys()),
                    self.origin)
        bMissingId=False
        if id in self.Ids:
            node=self.Ids[id]
            if self.verbose:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'id:%d found'%(id),
                                self.origin)
            return (1,node)
        else:
            bMissingId=True
        if self.verbose:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'id:%d not found'%(id),
                                self.origin)
        return (0,None)
    def AddNewNode(self,node,doc):
        if self.verbose:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,
                        'node:%s,ids:%s'%(node,self.Ids.keys()),
                        self.origin)
        self.missingIdNodes.append(node)    # 070202:wro=[node]
        self.ProcessMissing(doc)
    def ClearMissing(self):
        if self.verbose:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'',
                    origin=self.origin)
        del self.missingIdNodes
        self.missingIdNodes=[]
    def AddMissing(self,node):
        if self.verbose:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,
                    'node:%s,ids:%s'%(node,self.Ids.keys()),
                    origin=self.origin)
        self.missingIdNodes.append(node)
    def ProcessMissing(self,doc):
        bIdChanged=False
        #process missing ids
        if self.verbose:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                if len(self.missingIdNodes)>0:
                    vtLog.vtLngCur(vtLog.DEBUG,
                            'process missing:%d'%len(self.missingIdNodes),
                            origin=self.origin)
        for n in self.missingIdNodes:
            lastIdPrev=self.lastId
            id=self.GetNextId()
            sId=self.format%id
            if self.verbose:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,
                            'oldId:%s newId:%s'%(doc.getAttribute(n,self.attr),sId),
                            origin=self.origin)
            sOldId=doc.getAttribute(n,self.attr)
            doc.setAttribute(n,self.attr,sId)
            self.Ids[id]=n
            #self.FPs[id]=self.doc.getFingerPrint(n)
            doc.clearFingerPrint(n)    # 070221:wro force recalc fingerprint
            self.FPs[id]=doc.GetFingerPrintAndStore(n)
            # 070221:wro check old id points to the same node
            try:
                iOldId=long(sOldId)
                nn=self.Ids[iOldId]
                if doc.getAttribute(nn,self.attr)==sId:
                    if self.verbose:
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCur(vtLog.DEBUG,
                                    'oldId:%s points to the new node revert change'%(sOldId),
                                    origin=self.origin)
                    sId=self.format%iOldId
                    #del self.Ids[iOldId]
                    #del self.FPs[iOldId]
                    del self.Ids[id]
                    del self.FPs[id]
                    doc.setAttribute(n,self.attr,sId)
                    doc.clearFingerPrint(n)
                    self.FPs[id]=doc.GetFingerPrintAndStore(n)
                    self.lastId=lastIdPrev  # unwind last free ID
            except:
                pass
            bIdChanged=True
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,
        #        'ids:%s'%(self.Ids.keys()),
        #        origin='%s:NodeIds'%self.doc.appl)
        return bIdChanged
    def ConvertId2Str(self,node,doc):
        id=doc.getAttribute(node,self.attr)
        return self.format%long(id)
    def ConvertIdNum2Str(self,id):
        return self.format%id
    def ConvertIdStr2Str(self,id):
        return self.format%long(id)
    def GetIDs(self):
        #vtLog.vtLngCur(vtLog.ERROR,'','')
        return self.Ids.keys()
    def GetFPs(self):
        return self.FPs
    def GetOfflineIdStr(self,sId):
        try:
            id=long(sId)
        except:
            vtLog.vtLngCur(vtLog.ERROR,'id:%s'%(sId),'%s:NodeIds'%self.origin)
            return 0
        return GetOfflineId(id)
    def GetOfflineId(self,id):
        if id in self.offlineIds:
            return self.offlineIds[id]
        return 0
    def GetOfflineIds(self):
        l=[]
        iMask=self.OFFLINE_MASK
        for id,val in self.offlineIds.iteritems():
            if (val & iMask)!=0:
                l.append(id)
        return l
    def GetOfflineAddIds(self):
        l=[]
        iMask=self.MAP_OFFLINE['add']
        for id,val in self.offlineIds.iteritems():
            if (val & iMask)==iMask:
                l.append(id)
        return l
    def GetOfflineContentIds(self):
        l=[]
        iMask=self.MAP_OFFLINE['edit']
        for id,val in self.offlineIds.iteritems():
            if (val & iMask)==iMask:
                l.append(id)
        return l
    def GetOfflineEditIds(self):
        l=[]
        iMask=self.MAP_OFFLINE['edit']
        for id,val in self.offlineIds.iteritems():
            if (val & iMask)==iMask:
                l.append(id)
        return l
    def GetOfflineDelIds(self):
        l=[]
        iMask=self.MAP_OFFLINE['del']
        for id,val in self.offlineIds.iteritems():
            if (val & iMask)==iMask:
                l.append(id)
        return l
    def GetOfflineMoveIds(self):
        l=[]
        iMask=self.MAP_OFFLINE['move']
        for id,val in self.offlineIds.iteritems():
            if (val & iMask)==iMask:
                l.append(id)
        return l
    #    return self.offlineContentIds
    def IsOfflineAdd(self,id):
        return self.IsOfflineAction(id,'add')
    def IsOfflineEdit(self,id):
        return self.IsOfflineAction(id,'edit')
    def IsOfflineDel(self,id):
        return self.IsOfflineAction(id,'del')
    def IsOfflineMove(self,id):
        return self.IsOfflineAction(id,'move')
    def IsOfflineAction(self,id,sAction):
        iMask=self.MAP_OFFLINE[sAction]
        #print 'IsOfflineAction',id,self.GetOfflineId(id) ,iMask
        return (self.GetOfflineId(id) & iMask)==iMask
    def IsOffline(self,id):
        return (self.GetOfflineId(id) & self.OFFLINE_MASK)!= 0
    def GetGlobalFingerPrint(self):
        s=newSha()#sha.new()
        keys=self.FPs.keys()
        keys.sort()
        for k in keys:
            s.update('%d%s'%(k,self.FPs[k]))
        return s.hexdigest()
class SortedNodeIds:
    def __init__(self):
        self.ids={}
        self.infos={}
        self.lId=[]
        self.keys=[]
        self.faultyIds=[]
        self.node=None
        self.idName=''
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        # del self.doc
        del self.ids
        del self.infos
        del self.lId
        del self.keys
        del self.faultyIds
    #def SetDoc(self,doc):
    #    self.doc=doc
    def SetNode(self,doc,node,tagName,idName,infoTags):
        self.doc=doc
        self.idName=idName
        self.node=node
        del self.ids
        del self.infos
        del self.lId
        self.ids={}
        self.lId=[]
        self.infos={}
        if node is None:
            return
        return self.AddNode(node,tagName,idName,infoTags,doc)
    def AddNodeSingle(self,o,idName,infoTags,doc,bSort=False):
        id=doc.getAttribute(o,idName)
        try:
            id=long(id)
        except:
            #self.faultyIds.append((idInfo,o))
            return
        if id in self.ids:
            idInfo=self.ids[id]
            # id is already stored
            self.faultyIds.append((idInfo,o))
        else:
            sLabel=''
            if infoTags is not None:
                for tag,format in infoTags:
                    tmp=o
                    tags=tag.split(':')
                    if len(tags)>1:
                        for s in tags[:-1]:
                            tmp=doc.getChild(tmp,s)
                    sTag=tags[-1]
                        
                    s=format%doc.getNodeText(tmp,sTag)
                    sLabel=sLabel+s
                if sLabel in self.infos:
                    info=self.infos[sLabel]
                    info.append(id)
                else:
                    self.infos[sLabel]=[id]
            else:
                sLabel=None
            self.keys.append(sLabel)
            self.lId.append(id)
            self.ids[id]=[o,sLabel,id]
            #lst.append((s,id))
            self.lId.sort()
        if bSort:
            self.keys=self.infos.keys()
            self.keys.sort()
            self.lId=self.ids.keys()
            self.lId.sort()
    def Sort(self):
        self.keys=self.infos.keys()
        self.keys.sort()
        self.lId=self.ids.keys()
        self.lId.sort()
    def AddNode(self,node,tagName,idName,infoTags,doc):
        n=doc.getChilds(node,tagName)
        if len(n)>0:
            for o in n:
                self.AddNodeSingle(o,idName,infoTags,doc)
        self.Sort()
        return len(self.faultyIds)
    def GetIdName(self):
        return self.idName
    def GetIds(self):
        return self.ids.keys()
    def GetKeys(self):
        return self.keys
    def GetInfoByIdx(self,idx):
        try:
            k=self.keys[idx]
            return self.ids[self.infos[k][0]]
        except:
            return [None,'','']
    def GetInfoById(self,id):
        try:
            return self.ids[long(id)]
        except:
            return [None,'','']
    def GetIdxById(self,id):
        try:
            i=self.ids[long(id)]
            return self.keys.index(i[1])
        except:
            return -1
    def RemoveId(self,id):
        try:
            id=long(id)
            if id in self.ids:
                del self.ids[id]
            if id in self.lId:
                self.lId.remove(id)
            idx=self.GetIdxById(id)
            if idx>=0:
                del self.keys[idx]
        except:
            pass
    def GetIdByIdx(self,idx):
        try:
            #i=self.ids[id]
            #return self.keys.index(i[1])
            return self.infos[self.keys[idx]]
        except:
            return 0
    def GetIdxByName(self,name):
        try:
            i=self.infos[name]
            return self.GetIdxById(i[0])
        except:
            return 0
    def GetIdByName(self,name):
        try:
            return self.infos[name]
        except:
            return 0
    def GetFaultyIds(self):
        return self.faultyIds
    def GetLastUsedId(self):
        try:
            #print self.keys
            return self.lId[-1]
        except:
            return 0
    def GetFirstFreeId(self):
        try:
            i=self.GetLastUsedId()+1
            return i
        except:
            return 0
class NodeInfo:
    def __init__(self,doc,n):
        self.doc=doc
        self.node=n
        self.res={}
        self.infos=self.__getInfos__(n)
    def __getInfos__(self,n):
        infos={}
        if n is not None:
            childs=self.doc.getChilds(n)
            for c in childs:
                i=self.doc.getText(c)
                infos[c]=i
                if c.children is not None:
                    infos['children']=self.__getInfos__(c)
        return infos
    def __check__(self,infos1,infos2):
        if infos1 is None:
            infos1={}
        else:
            keys1=infos1.keys()
        if infos2 is None:
            infos2={}
        else:
            keys2=infos2.keys()
        res={}
        try:
            keys1.remove('children')
            bNoChilds1=False
        except:
            bNoChilds1=True
        try:
            keys2.remove('children')
            bNoChilds2=False
        except:
            bNoChilds2=True
        for k in keys1:
            try:
                i=infos2[k]
                if i<>infos1[k]:
                    res[k]=(infos1[k],infos2[k])
            except:
                res[k]=(infos1[k],None)
        for k in keys2:
            try:
                i=infos1[k]
            except:
                res[k]=(None,infos2[k])
        if bNoChilds1:
            if bNoChilds2:
                # ok both are single
                pass
            else:
                # only infos2 is parent
                res['children']=self.__check__({},infos2['children'])
        else:
            if bNoChilds1:
                # only infos1 is parent
                res['children']=self.__check__(infos1['children'],{})
                pass
            else:
                # ok both are parents
                resChild=self.__check__(infos1['children'],infos2['children'])
                if len(resChild.keys())>0:
                    res['children']=resChild
                pass
        return res
    def checkDiff(self,n):
        self.res={}
        infos=self.__getInfos__(n)
        self.res=self.__check__(self.infos,infos)
        return self.res
class InfoDiff:
    def __init__(self,doc):
        self.doc=doc
        self.res={}
        self.infos1={}
        self.infos2={}
        
    def __getInfos__(self,n,prefix=[]):
        if n is not None:
            childs=self.doc.getChilds(n)
            for c in childs:
                tag=self.doc.getTagName(c)
                i=self.doc.getText(c).strip()
                sTag=':'.join(prefix+[tag])
                self.infos[sTag]=i
                if c.children is not None:
                    self.__getInfos__(c,prefix+[tag])
    def __getInfos2__(self,n,prefix=[]):
        if n is not None:
            childs=self.doc.getChilds(n)
            for c in childs:
                tag=self.doc.getTagName(c)
                i=self.doc.getText(c)
                sTag=':'.join(prefix+[tag])
                self.infos2[sTag]=i
                if c.children is not None:
                    self.__getInfos2__(c,prefix+[tag])
    def __check__(self,a,b,prefix=[]):
        dA={}
        for c in self.doc.getChilds(a):
            tag=self.doc.getIdent(c)
            sTag=','.join(prefix+[tag])
            dA[sTag]=c
        dB={}
        for c in self.doc.getChilds(b):
            tag=self.doc.getIdent(c)
            sTag=','.join(prefix+[tag])
            dB[sTag]=c
        keys1=dA.keys()
        keys1.sort()
        keys2=dB.keys()
        for k in keys1:
            try:
                sA=self.doc.getText(dA[k]).strip()
                sB=self.doc.getText(dB[k]).strip()
                if sA!=sB:
                    print '     diff',k,sA,sB
                    self.res.append([k,dA[k],dB[k],sA,sB])
                self.__check__(dA[k],dB[k],prefix+[k])
            except:
                self.res.append([k,dA[k],None,sA,''])
                #self.__check__(dA[k],None,prefix+[k])
        for k in keys2:
            try:
                i=dA[k]
            except:
                self.res.append([k,None,dB[k],'',sB])
                #self.__check__(None,dB[k],prefix+[k])
    def checkDiff(self,a,b):
        self.res=[]
        self.__check__(a,b)
        return self.res

class thdSynch:
    def __init__(self,doc=None,verbose=0):
        self.verbose=verbose
        self.doc=doc
        self.running=False
    def Synch(self,tmp,nodes):
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'')
        self.keepGoing = self.running = True
        self.nodes=nodes
        self.tmp=tmp
        self.dNotify={'edit':[],'edit_fault':[],'edit_offline':[],
                'add':[],'add_fault':[],'add_offline':[],
                'move':[],'move_fault':[],
                'del':[],'del_fault':[]}
        thread.start_new_thread(self.Run, ())

    def Stop(self):
        self.keepGoing = False

    def IsRunning(self):
        return self.running
    
    def __getIds__(self,node):
        d={}
        for c in self.doc.getChilds(node):
            key=self.doc.getKey(c)
            if len(key)>0:
                d[key]=c
        return d
    def __clearOffline__(self,node):
        docKeys=self.docIds.GetIDs()
        for k in docKeys:
            c=self.docIds.GetId(k)[1]
            off=self.doc.getAttribute(c,'offline')
            if len(off)>0:
                id=self.doc.getKey(c)
                self.lstOffline[id]=c
                self.doc.removeId(c)
                self.doc.removeAttribute(c,'offline')
                self.doc.removeAttribute(c,'offline_content')
                
            else:
                off=self.doc.getAttribute(c,'offline_content')
                if len(off)>0:
                    id=self.doc.getKey(c)
                    #self.doc.removeId(c)
                    self.lstOffCont[id]=c
                    self.doc.removeAttribute(c,'offline_content')
        
    def __procNode__(self,docNode,tmpNode):
        dTmp=self.__getIds__(docNode)
        dDoc=self.__getIds__(tmpNode)
        kDoc=dDoc.keys()
        kTmp=dTmp.keys()
        kDoc.sort()
        kTmp.sort()
        for k in kDoc:
            
            pass
        pass
    def __proc__(self):
        tmpKeys=self.tmpIds.GetIDs()
        tmpKeys.sort()
        docKeys=self.docIds.GetIDs()
        offKeys=self.lstOffline.keys()
        #map(docKeys.remove,offKeys)
        lstAdd=[]
        self.iCount=len(tmpKeys)
        #self.iCount+=len(self.lstOffline.keys())
        self.iAct=0
        for k in tmpKeys:
            if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'act:%5d key:%7d'%(self.iAct,k))
            self.doc.NotifySynchProc(self.iAct,self.iCount)
            try:
                self.doc.acquire()
                docNode=self.docIds.GetId(k)[1]
                docKeys.remove(k)
                iEdit=self.doc.__synchEdit__(docNode,self.tmpIds.GetId(k)[1])
                self.doc.release()
                if iEdit==1:
                    self.dNotify['edit'].append(k)
                    self.iCount+=1
                elif iEdit==2:
                    self.dNotify['edit_offline'].append(docNode)
                    self.iCount+1
                elif iEdit==0:
                    pass
                else:
                    self.dNotify['edit_fault'].append(k)
                    self.iCount+=1
            except:
                lstAdd.append(k)
                self.doc.release()
                self.iCount+=1
            self.iAct+=1
        
        # delete
        self.doc.acquire()
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'del:%s'%(docKeys))
        for k in docKeys:
            docNode=self.docIds.GetId(k)[1]
            if self.doc.__synchDel__(docNode)>0:
                self.dNotify['del'].append(k)
            else:
                self.dNotify['del_fault'].append(k)
        self.doc.release()
        
        # add
        def getLevel(node):
            nodePar=self.doc.getParent(node)
            if nodePar is not None:
                return getLevel(nodePar)+1
            return 1
        def cmpAdd(a,b):
            aNode=self.tmpIds.GetId(a)[1]
            bNode=self.tmpIds.GetId(b)[1]
            return getLevel(aNode)-getLevel(bNode)
        lstAdd.sort(cmpAdd)
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'add:%s'%(lstAdd))
        for k in lstAdd:
            self.doc.NotifySynchProc(self.iAct,self.iCount)
            docNode=self.docIds.GetId(k)[1]
            if docNode is None:
                self.doc.acquire()
                iAdd=self.doc.__synchAdd__(self.tmpIds.GetId(k)[1])
                self.doc.release()
                if iAdd>0:
                    self.dNotify['add'].append(k)
                else:
                    self.dNotify['add_fault'].append(k)
            self.iAct+=1
        # move
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'move')
        for k in tmpKeys:
            try:
                self.doc.acquire()
                idParTmp=self.doc.getKey(self.doc.getParent(self.tmpIds.GetId(k)[1]))
                idPar=self.doc.getKey(self.doc.getParent(self.docIds.GetId(k)[1]))
                if idPar!=idParTmp:
                    if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'move:%d from %s to %s'%(k,idPar,idParTmp))
                    parNode=self.docIds.GetIdStr(idParTmp)[1]
                    node=self.docIds.GetIdStr(k)[1]
                    if self.doc.__synchMove__(parNode,node)>0:
                        self.dNotify['move'].append(k)
                    else:
                        self.dNotify['move_fault'].append(k)
                self.doc.release()
            except:
                self.dNotify['move_fault'].append(k)
                self.doc.release()
        # add offline added to server
        def cmpAddNode(aNode,bNode):
            return getLevel(aNode)-getLevel(bNode)
        lstAdd=self.lstOffline.values()
        lstAdd.sort(cmpAddNode)
        for k in lstAdd:
            #self.doc.NotifySynchProc(self.iAct,self.iCount)
            #iAdd=self.doc.__synchAdd2Srv__(self.lstOffline[k])
            self.dNotify['add_offline'].append(k)
            #self.iAct+=1
        pass
    def GetAddOffline(self):
        return self.dNotify['add_offline']
    def GetEditOffline(self):
        return self.dNotify['edit_offline']
    def Run(self):
        self.doc.acquire('thread')
        #self.doc.acquire()
        self.lstOffline={}
        self.lstOffCont={}
        #iTmpElemCount=self.tmp.GetElementCount()
        #iTmpAttrCount=self.tmp.GetElementAttrCount()
        
        iDocElemCount=self.doc.GetElementCount()
        iDocAttrCount=self.doc.GetElementAttrCount()
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'doc count elem:%5d count attr:%5d'%(iDocElemCount,iDocAttrCount))
            #vtLog.vtLngCur(vtLog.DEBUG,'tmp count elem:%5d count attr:%5d'%(iTmpElemCount,iTmpAttrCount),callstack=False)
        #if iTmpElemCount<iDocElemCount:
        #    iElemCount=iTmpElemCount
        #else:
        iElemCount=iDocElemCount
        #if iTmpAttrCount<iDocAttrCount:
        #    iAttrCount=iTmpAttrCount
        #else:
        self.iAct=0
        self.iCount=iElemCount
        iAttrCount=iDocAttrCount
        self.doc.NotifySynchStart(iDocAttrCount)
        docNode=self.doc.doc.getRootElement()
        self.docIds=self.doc.ids
        self.__clearOffline__(docNode)
        
        tmpNode=self.tmp.getRootElement()
        if tmpNode is not None:
            self.tmpIds=NodeIds(self.doc,self.doc.attr)
            self.doc.__genIds__(tmpNode,self.tmpIds)
            self.docIds=self.doc.ids
            self.__proc__()
            self.docIds=self.doc.ids
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'notify add:%d'%(len(self.dNotify['add'])))
        #self.doc.release('thread')
        #print '\n\n\n\n'
        bAdd=False
        for k in self.dNotify['add']:
            bAdd=True
            self.doc.NotifySynchProc(self.iAct,self.iCount)
            node=self.docIds.GetId(k)[1]
            try:
                parID=long(self.doc.getKey(self.doc.getParent(node)))
            except:
                parID=-1
            self.doc.NotifyAddNode(parID,long(k),'')
            #time.sleep(10)
            #vtLog.vtLngCallStack(None,vtLog.DEBUG,'synch add:%d %d'%(parID,long(k)),1,callstack=False)
            self.iAct+=1
        self.doc.release('thread')
        if bAdd:
            self.doc.acquire('thread_tree_add')
            # wait until all elements added into the tree
            self.doc.release('thread_tree_add')
            #time.sleep(1.0)
            self.doc.lock('adding')
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'notify edit:%d'%(len(self.dNotify['edit'])))
        for k in self.dNotify['edit']:
            #vtLog.vtLngCallStack(None,vtLog.DEBUG,'synch edit %d'%(long(k)),1,callstack=False)
            self.doc.NotifySynchProc(self.iAct,self.iCount)
            self.doc.NotifyGetNode(k)
            self.iAct+=1
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'notify move:%d'%(len(self.dNotify['move'])))
        for k in self.dNotify['move']:
            #vtLog.vtLngCallStack(None,vtLog.DEBUG,'synch move: %d'%(long(k)),1,callstack=False)
            pass
        #vtLog.vtLngCallStack(None,vtLog.DEBUG,'synch do common',1,callstack=False)
        #self.doc.release()
        #time.sleep(100)
        #self.doc.acquire()
        par=self.doc.getRoot()
        #vtLog.vtLngCallStack(None,vtLog.DEBUG,'synch do common',1,callstack=False)
        try:
            if 1==0:
                for tagName in self.doc.nodes2keep:
                    #print 'keep',tagName
                    n=self.doc.getChild(self.doc.getRoot(),tagName)
                    if n is not None:
                        self.doc.deleteNode(n)
                    for n in self.nodes:
                        #print n
                        self.doc.appendChild(par,n)
                        self.doc.AlignNode(n,iRec=10)
            for tagName in self.doc.nodes2synch:#['config','prjinfos']:
                #print '   ',tagName
                nn=self.doc.getChild(tmpNode,tagName)
                if nn is None:
                    continue
                nn.unlinkNode()
                n=self.doc.getChild(self.doc.getRoot(),tagName)
                if n is not None:
                    self.doc.deleteNode(n)
                self.doc.appendChild(par,nn)
                self.doc.AlignNode(nn,iRec=10)
            #print 'synch finished'
            self.doc.NotifySynchFinished(True)
            #print 'synch finished'
            del self.tmp
        except Exception,list:
            vtLog.vtLngTB('')
        #vtLog.vtLngCallStack(None,vtLog.DEBUG,'synch finished',1,callstack=True)
        
        #self.doc.release()
        self.keepGoing = self.running = False
        
#class vtXmlDom(vtXmlDomCore,vtSecXmlAclDom):
class vtXmlDomBase(vtSecXmlAclDom):
    NODE_ATTRS=[]
    TAGNAME_REFERENCE='tag'
    LOG_ACQUIRE=0 or VERBOSE>10
    LOG_SAVE=0
    USE_FILE_LOCK=0
    SERIALIZE_CODEC='utf-8'
    TAGNAME_ROOT='root'
    def __init__(self,attr='id',skip=[],synch=False,appl='',verbose=0,audit_trail=True):
        #self.sem=threading.Semaphore()
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self.appl=appl
        self.__applAlias=None
        try:
            if hasattr(self,'origin')==False:
                self.origin=appl
        except:
            self.origin=self.__class__.__name__
            pass
        self.oLogin=None
        self.nodes2keep=['settings']
        self.nodes2synch=[]#['config']
        if hasattr(self,'oSF')==False:
            self.oSF=vtStateFlagSimple(
                    {
                    0x000:{'name':'CLOSED','translation':_(u'closed'),
                        'init':True,'is':None,
                        'enter':{'set':['clsd','STLD',],
                                 'clr':['WRKG','RDY','MOD']}},
                    0x001:{'name':'OPEN','translation':_(u'open'),
                        'is':None,
                        'enter':{'set':['WRKG',],
                                 'clr':['STLD','FLT','CLSD','MOD','RDY']}},
                    0x002:{'name':'OPENING','translation':_(u'opening'),
                        'is':None,},
                    0x003:{'name':'GENID','translation':_(u'generate IDs'),
                        'is':None,
                        'enter':{'set':['WRKG']}},
                    0x004:{'name':'OPENED','translation':_(u'opened'),
                        'is':None,
                        'enter':{'set':['OPND','RDY','STLD'],
                                 'clr':['WRKG','FLT','MOD']},
                        '':{'set':['WRKG']}},
                    0x005:{'name':'OPEN_FLT','translation':_(u'open fault'),
                        'is':'IsOpenFault',
                        'enter':{'set':['FLT','STLD'],
                                 'clr':['WRKG','MOD','OPND','RDY',]}},
                    0x006:{'name':'PAUSE','translation':_(u'pause'),
                        'is':None,
                        'enter':{'set':['WRKG'],
                                 'clr':['RDY']}},
                    0x007:{'name':'PAUSING','translation':_(u'pausing'),
                        'is':None,
                        'enter':{'set':['WRKG']}},
                    0x008:{'name':'PAUSED','translation':_(u'paused'),
                        'is':None,
                        'enter':{'set':['RDY'],
                                 'clr':['WRKG']}},
                    0x009:{'name':'RESUME','translation':_(u'resume'),
                        'enter':{'set':['WRKG']}},
                    0x00E:{'name':'NEW','translation':_(u'new'),
                        'enter':{'set':['STLD','MOD','RDY']}},
                    0x00F:{'name':'CLOSING','translation':_(u'closing'),
                        'enter':{'set':['WRKG']}},
                    #0x007:{'name':'paused','translation':_(u'paused'),
                    #    'is':None,
                    #    'leave':{'set':['fin','rdy'],
                    #             'clr':['aly','wrk']},
                    #    'enter':{'set':['wrk']}},
                    },
                    {
                    0x001:{'name':'CLSD', 'translation':_(u'closed'),'is':'IsClosed','order':0},
                    0x002:{'name':'STLD', 'translation':_(u'settled'),'is':'IsSettled','order':1},
                    0x004:{'name':'WRKG',  'translation':_(u'working'),'is':'IsWorking','order':2},
                    0x008:{'name':'RDY',  'translation':_(u'ready'),'is':'IsReady','order':3},
                    0x010:{'name':'MOD',  'translation':_(u'modified'),'is':'IsModified','order':4},
                    0x020:{'name':'FLT',  'translation':_(u'faulty'),'is':'IsFaulty','order':5},
                    0x040:{'name':'OPND', 'translation':_(u'opened'),'is':'IsOpened','order':6},
                    0x080:{'name':'PSD', 'translation':_(u'opened'),'is':'IsPaused','order':7},
                    0x00080000:{'name':'SHUTDOWN', 'translation':_(u'shutdown'),'is':'IsShutDown','keep':True,'order':18},
                    },None,
                    origin=u':'.join([self.origin,u'StateFlag']))
        self.verbose=verbose
        self.semUsr=threading.Lock()#threading.Semaphore()
        #self.semUsr=threading._allocate_lock()
        self.semDict={}
        self.lockDict={}
        if hasattr(self,'thdProc')==False:
            self.thdProc=vtThreadCore(bPost=False,verbose=verbose-1,
                        origin=u':'.join([self.GetOrigin(),'thdProc']))
            #vtmThreadInterFace.__init__(self,self.thdProc)
        #self.thdProc=vtThread(None,bPost=False,verbose=verbose-1,
        #            origin=u':'.join([self.GetOrigin(),'thdProc']))
        #self.thdSynch=thdSynch(self,verbose-1)
        #self.bSynch=synch
        self.sFN=None
        self.dom=None
        self.dNetDoc=None
        self.skip=skip
        self.attr=attr
        self.iElemCount=-1
        self.iElemIDCount=-1
        self.applEncoding='utf-8'
        self.xmlEncoding='ISO-8859-1'
        #self.bModified=False
        #self.bPaused=False
        self.consumers=[]
        self.consumersLang=[]
        self.lang=None
        self.languages=[]
        self.dUsrInfos={}
        self.netMaster=None
        self.__initCmdDict__()
        self.__initLib__()
        #vtXmlDomCore.__init__(self)
        vtSecXmlAclDom.__init__(self)
        self.audit=vtLogAuditTrail(self.appl)
        if audit_trail==False:
            self.audit.setEnable(False)
        self.CreateIds(self.attr)
        self.CreateAcl()
    def __initLib__(self):
        pass
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,''%(),self.GetOrigin())
        #vtLog.vtLngCur(vtLog.DEBUG,'','del')
        #self.Close()
        try:
            del self.consumers
            del self.consumersLang
        except:
            #vtLog.vtLngTB('del')
            pass
        self.consumers=None
        self.consumersLang=None
        try:
            del self.semUsr
            del self.semDict
            del self.lockDict
            del self.ids
            del self.languages
            del self.dUsrInfos
            del self.dNetDoc
            del self.CMD_DICT
        except:
            #vtLog.vtLngTB('del')
            pass
        try:
            del self.oLogin
            del self.audit
        except:
            #vtLog.vtLngTB('del')
            pass
        try:
            vtSecXmlAclDom.__del__(self)
        except:
            pass
        #vtXmlDomCore.__del__(self)
        #print self.__dict__
    def __logInfo__(self,msg=''):
        vtLog.vtLngCur(vtLog.INFO,msg,self.GetOrigin(),level2skip=2)
    def __logError__(self,msg=''):
        vtLog.vtLngCur(vtLog.ERROR,msg,self.GetOrigin(),level2skip=2)
    def __logTB__(self):
        vtLog.vtLngTB(self.GetOrigin())
    def IsLoggedInSelf(self,node,usrId):
        return False
    def IsOpened(self):
        return self.oSF.IsOpened
    def SetApplName(self,appl):
        self.appl=appl
    def GetApplName(self):
        return self.appl
    def GetAppl(self):
        return self.appl
    def GetApplAlias(self):
        return self.__applAlias
    def SetApplAlias(self,applAlias):
        self.__applAlias=applAlias[:]
    def GetOrigin(self):
        return self.origin
    def SetOrigin(self,origin):
        self.origin=origin
        try:
            if hasattr(self,'ids'):
                if self.ids is not None:
                    self.ids.SetOrigin(origin)
            if hasattr(self,'thdProc'):
                self.thdProc.SetOrigin(u':'.join([self.GetOrigin(),u'thdProc']))
            if hasattr(self,'oSF'):
                self.oSF.SetOrigin(u':'.join([self.GetOrigin(),u'StateFlag']))
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def ShutDown(self):
        vtLog.vtLngCur(vtLog.DEBUG,'',self.GetOrigin())
        self.oSF.SetFlag(self.oSF.SHUTDOWN)
        #self.thdProc.Stop()
        #self.thdProc.Set
        self.dNetDoc={}
        self.netMaster=None
        #print hasattr(self,'__ShutDown__')
        #print vtLog.pformat(self.__dict__)
        if hasattr(self,'__ShutDown__'):
            self.__ShutDown__()
    def IsShutDown(self):
        return self.oSF.IsShutDown
    def DoDelayed(self,func,*args,**kwargs):
        self.thdProc.Do(func,*args,**kwargs)
    def _doSafe(self,func,*args,**kwargs):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,''%(),self.GetOrigin())
        self.acquire('dom')
        try:
            func(*args,**kwargs)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.release('dom')
    def DoSafe(self,func,*args,**kwargs):
        self.thdProc.Do(self._doSafe,func,*args,**kwargs)
    def CallBack(self,func,*args,**kwargs):
        try:
            if hasattr(self.thdProc,'CallBack'):
                self.thdProc.CallBack(func,*args,**kwargs)
            else:
                func(*args,**kwargs)
        except:
            vtLog.vtLngTB(self.GetOrigin())
            func(*args,**kwargs)
    def CallBackWX(self,func,*args,**kwargs):
        try:
            if hasattr(self.thdProc,'CallBackWX'):
                self.thdProc.CallBackWX(func,*args,**kwargs)
            else:
                func(*args,**kwargs)
        except:
            vtLog.vtLngTB(self.GetOrigin())
            func(*args,**kwargs)
    def CallBackDelayed(self,zSleep,func,*args,**kwargs):
        try:
            if hasattr(self.thdProc,'CallBackDelayed'):
                self.thdProc.CallBackDelayed(zSleep,func,*args,**kwargs)
            else:
                func(*args,**kwargs)
        except:
            vtLog.vtLngTB(self.GetOrigin())
            func(*args,**kwargs)
    def DoCallBack(self,func,*args,**kwargs):
        try:
            if hasattr(self.thdProc,'DoCallBack'):
                self.thdProc.DoCallBack(func,*args,**kwargs)
            else:
                self.thdProc.Do(func,*args,**kwargs)
        except:
            vtLog.vtLngTB(self.GetOrigin())
            func(*args,**kwargs)
    def DoCallBackWX(self,func,*args,**kwargs):
        try:
            if hasattr(self.thdProc,'DoCallBackWX'):
                self.thdProc.DoCallBackWX(func,*args,**kwargs)
            else:
                self.thdProc.Do(func,*args,**kwargs)
        except:
            vtLog.vtLngTB(self.GetOrigin())
            func(*args,**kwargs)
    def GetNetDocs(self):
        return self.dNetDoc
    def GetNetDoc(self,name):
        try:
            return self.dNetDoc[name]['doc']
        except:
            return None
    def SetNetMaster(self,netMaster):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'netMaster is None==%s'%(netMaster is None),self.GetOrigin())
        self.netMaster=netMaster
    def GetNetMaster(self):
        return self.netMaster
    def SetNetDocs(self,d):
        """netMaster {'vHum':{'doc':hum,'bNet':False}}
        """
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'d:%'%(vtLog.pformat(d)),self.GetOrigin())
        self.dNetDoc=d
        self.SetInfos2Get4RegisteredNodes()
    def GetNetDocDef(self,name):
        try:
            return self.dNetDoc[name]
        except:
            return None        
    def SetLogin(self,o):
        self.oLogin=o
    def GetLogin(self):
        try:
            return self.oLogin
        except:
            return None
    def GetLoginUsrId(self):
        try:
            return self.oLogin.GetUsrId()
        except:
            return -1
    def SetNetDocs(self,d):
        """netMaster {'vHum':{'doc':hum,'bNet':False}}
        """
        self.dNetDoc=d
    def __initCmdDict__(self):
        if hasattr(self,'CMD_DICT')==False:
            self.CMD_DICT={}
        self.CMD_DICT.update({
            'backup':(_('backup'),0x103,None)
            })
    def GetCmdDict(self):
        """ return a command dictionary
                tuple:( translation,
                        local=                          0x001|
                        remote=                         0x002|
                        local and remote=               0x003|
                        build permission=               0x004|
                        add to quick menu position=     0xF00,
                        argument dictionary)
                
                argument dictionary: key:argument name, value argument tuple
                
                argument tuple:(translation,type, settings dictionary to configure widget or similar)
            {
            'Clr':(_('clear'),1,None),
            'Go':(_('go on vacation'),2,{'arg01: ('arg01 translation','arg01 type',{setting}} ),
            'NoGo':(_('do not go on vacation'),3,None),
            }
        """
        return self.CMD_DICT#{}
    def DoCmd(self,srv,action,**kwargs):
        """ return
            1       ... action executed correctly
            0       ... action unknown
           -1       ... fault occured during execution 
           -2       ... permission denied
           -3       ... action execution in process
        """
        try:
            if action=='backup':
                sNow=time.strftime('%Y%m%d_%H%M%S',time.localtime(time.time()))
                sFNOld=self.GetFN()
                if sFNOld is None:
                    sFNBak='.'.join(['_'.join([self.appl,sNow]),'bak'])
                else:
                    sFNBak='.'.join([sFNOld,sNow,'bak'])
                self.Save(sFNBak)
                self.SetFN(sFNOld)
                vtLog.vtLngCur(vtLog.INFO,'action:%s executed;FN:%s'%(action,sFNBak),self.GetOrigin())
                return 1
            return 0
        except:
            vtLog.vtLngCur(vtLog.ERROR,'action:%s;kwargs:%s'%(action,vtLog.pformat(kwargs)),self.GetOrigin())
            vtLog.vtLngTB(self.GetOrigin())
            return -1
    def acquire(self,name=None,blocking=True):
        #print
        #print '\n\n\n\n\n****************************************'
        #vtLog.vtLngCallStack(None,vtLog.DEBUG,'name:%s'%(repr(name)),verbose=1)
        #print '****************************************\n\n\n\n\n'
        if self.LOG_ACQUIRE:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCS(vtLog.DEBUG,'semaphore name:%s appl:%s;acquire;blocking:%d'%(name,self.appl,blocking),self.GetOrigin())
        #else:
        #    if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #        vtLog.vtLngCur(vtLog.DEBUG,'name:%s;blocking:%d'%(name,blocking),self.GetOrigin())
        if name is None:
        #    print 'usr',self.semUsr,self.semUsr.__dict__
            return self.semUsr.acquire(blocking)
        if name in self.semDict:
            sem=self.semDict[name]
        else:
            sem=threading.Lock()#threading.Semaphore()
            #sem=threading._allocate_lock()
            self.semDict[name]=sem
        #print name,sem,sem.__dict__
        return sem.acquire(blocking)
    def release(self,name=None):
        #print 
        #vtLog.vtCallStack(5,level=5,verbose=1)
        #print '\n\n\n\n\n****************************************'
        #vtLog.vtLngCallStack(None,vtLog.DEBUG,'name:%s'%(repr(name)),verbose=1)
        #print '****************************************\n\n\n\n\n'
        if self.LOG_ACQUIRE:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCS(vtLog.DEBUG,'semaphore name:%s appl:%s;release'%(name,self.appl),self.GetOrigin())
        #else:
        #    if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #        vtLog.vtLngCur(vtLog.DEBUG,'name:%s'%(name),self.GetOrigin())
        try:
            if name is None:
            #    print 'usr','released'
                self.semUsr.release()
            if name in self.semDict:
                sem=self.semDict[name]
                sem.release()
        except:
        #    print name,'release exception'
            vtLog.vtLngTB(self.__class__.__name__)
            return
        #print name,'released'
        if self.LOG_ACQUIRE:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCS(vtLog.DEBUG,'semaphore name:%s appl:%s;released'%(name,self.appl),self.GetOrigin())
    def acquired(self,name=None):
        if name is None:
            return self.semUsr.locked()
        try:
            sem=self.semDict[name]
            return sem.locked()
        except:
            return False
    def lock(self,name):
        #print
        #vtLog.vtCallStack(5,level=5,verbose=1)
        try:
            lock=self.lockDict[name]
        except:
            lock=threading.Lock()
            self.lockDict[name]=lock
        lock.acquire()
    def unlock(self,name):
        #print 
        #vtLog.vtCallStack(5,level=5,verbose=1)
        try:
            lock=self.lockDict[name]
        except:
        #    print name,'release exception'
            return
        #print name,'released'
        lock.release()
    def __clear__(self):
        pass
    def __isRunning__(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'proc:%d'%(self.thdProc.IsRunning()),origin=self.GetOrigin())
        if self.IsConsumerBusy():
            return True
        if self.thdProc.IsRunning():
            return True
        if self.thdProc.IsEmpty()==False:
            return True
        #if self.thdSynch.IsRunning():
        #    return True
        vtLog.vtLngCur(vtLog.INFO,'all inactive',self.GetOrigin())
        return False
    #def IsSynchThreadEnabled(self):
    #    return self.bSynch
    def __start__(self):
        self.thdProc.Start()
    def __stop__(self):
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
        self.StopConsumer()
        #vtLog.vtLngCallStack(None,vtLog.DEBUG,'',verbose=1)
        self.thdProc.Stop()
        #self.thdSynch.Stop()
        #self.acquire()
        #self.release()
    def LogConsumerVerbose(self):
        try:
            iTmpVERBOSE=vpc.getVerboseType(__name__)
            bDbg=False
            if iTmpVERBOSE>10:
                bDbg=True
            iTmpVERBOSE=vpc.getVerboseOrigin(self.GetOrigin())
            if iTmpVERBOSE>10:
                bDbg=True
            if bDbg:
                sOrgTmp=self.GetOrigin()
                for i in xrange(len(self.consumers)):
                    try:
                        sOrgCons=self.consumers[i][0].GetOrigin()
                    except:
                        sOrgCons='???'
                    vtLog.vtLngCur(vtLog.DEBUG,'%d;org:%s;consumer:%s'%(i,
                            sOrgCons,self.consumers[i][0]),
                            sOrgTmp)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def AddConsumer(self,consumer,func):
        try:
            iTmpVERBOSE=vpc.getVerboseType(__name__)
            bDbg=False
            if iTmpVERBOSE>10:
                bDbg=True
            iTmpVERBOSE=vpc.getVerboseOrigin(self.GetOrigin())
            if iTmpVERBOSE>10:
                bDbg=True
            self.consumers.append((consumer,func))
            if bDbg:
                sOrgTmp=self.GetOrigin()
                try:
                    sOrgCons=consumer.GetOrigin()
                except:
                    sOrgCons='???'
                vtLog.vtLngCur(vtLog.DEBUG,
                        'len:%d;org:%s;onsumer:%s'%(len(self.consumers),
                        sOrgCons,consumer),sOrgTmp)
            self.LogConsumerVerbose()
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def DelConsumer(self,consumer=None):
        try:
            iTmpVERBOSE=vpc.getVerboseType(__name__)
            bDbg=False
            if iTmpVERBOSE>10:
                bDbg=True
            iTmpVERBOSE=vpc.getVerboseOrigin(self.GetOrigin())
            if iTmpVERBOSE>10:
                bDbg=True
            
            if hasattr(self,'consumers')==False:
                return
            if bDbg:
                sOrgTmp=self.GetOrigin()
                try:
                    sOrgCons=consumer.GetOrigin()
                except:
                    sOrgCons='???'
                vtLog.vtLngCur(vtLog.DEBUG,
                        'len:%d;org:%s;consumer:%s'%(len(self.consumers),
                        sOrgCons,consumer),sOrgTmp)
            if consumer is None:
                vtLog.vtLngCur(vtLog.INFO,'',self.GetOrigin())
                for tup in self.consumers:
                    try:
                        self.CallBackWX(tup[0].Clear)
                        #tup[0].Clear()
                    except:
                        vtLog.vtLngCur(vtLog.CRITICAL,'tup:%s'%(repr(tup[0])),self.GetOrigin())
                    try:
                        self.CallBackWX(tup[0].ClearDoc)
                        #tup[0].ClearDoc()
                    except:
                        vtLog.vtLngTB(self.GetOrigin())
                        vtLog.vtLngCur(vtLog.CRITICAL,'tup:%s'%(repr(tup[0])),self.GetOrigin())
                del self.consumers
                self.consumers=[]
                return
            if bDbg:
                sOrgTmp=self.GetOrigin()
            iTmpVERBOSE=vpc.getVerboseType(__name__)
            bDbg=False
            if iTmpVERBOSE>20:
                bDbg=True
            iTmpVERBOSE=vpc.getVerboseOrigin(self.GetOrigin())
            if iTmpVERBOSE>20:
                bDbg=True
            for i in range(len(self.consumers)):
                try:
                    if bDbg:
                        vtLog.vtLngCur(vtLog.DEBUG,
                                '%d;org:%s;con:%s;==:%d;it:%s'%(i,
                                self.consumers[i][0].GetOrigin(),
                                consumer,self.consumers[i][0]==consumer,
                                self.consumers[i]),sOrgTmp)
                except:
                    pass
                if self.consumers[i][0]==consumer:
                    self.consumers=self.consumers[:i]+self.consumers[i+1:]
                    self.LogConsumerVerbose()
                    return
            #self.consumers.remove(consumer)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def IsConsumerNotRunning(self,bCheckAll=False):
        bBusy=False
        for consumer in self.consumers:
            try:
                if consumer[0].IsRunning():
                    bBusy=True
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'consumer:%s is busy'%(`consumer[0]`),self.GetOrigin())
                    if bCheckAll==False:
                        return bBusy
            except:
                vtLog.vtLngCur(vtLog.ERROR,'consumer:%s'%(consumer[0]),self.GetOrigin())
                vtLog.vtLngTB(self.GetOrigin())
        if bBusy==False:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'all consumers are inactive.',self.GetOrigin())
        else:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'at least one consumer is busy',self.GetOrigin())
        return bBusy
    def WaitConsumerNotRunning(self,zTimeOut=1,zTimeSleep=0.1,bThread=False):
        #if bThread==False:
        #    vtLog.vtLngCur(vtLog.CRITICAL,'FIXME, call only from thread'%(),self.GetOrigin())
        zWait=0.0
        while self.IsConsumerNotRunning(bCheckAll=True):
            time.sleep(zTimeSleep)
            zWait+=zTimeSleep
            if zWait>=zTimeOut:
                vtLog.vtLngCur(vtLog.CRITICAL,'timeout (%5.2f)'%(zWait),self.GetOrigin())
                return True
        return True
    def StopConsumer(self):
        for consumer in self.consumers:
            try:
                #if consumer[0].IsRunning():        # 070718:wro stop anyway
                #consumer[0].Stop()
                self.CallBackWX(consumer[0].Stop)
            except:
                vtLog.vtLngCur(vtLog.ERROR,'consumer:%s'%(consumer[0]),self.GetOrigin())
                vtLog.vtLngTB(self.GetOrigin())
    def StartConsumer(self):
        if VERBOSE>=5:
            vtLog.vtLngCur(vtLog.DEBUG,''%(),self.GetOrigin())
        if self.oSF.IsShutDown:
            return
        try:
            for o in self.__class__.__bases__:
                if hasattr(o,'__start__'):
                    o.__start__(self)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        for consumer in self.consumers:
            try:
                consumer[0].Restart()
            except:
                vtLog.vtLngCur(vtLog.ERROR,'consumer:%s'%(consumer[0]),self.GetOrigin())
                vtLog.vtLngTB(self.GetOrigin())
    def PauseConsumer(self):
        if VERBOSE>=5:
            vtLog.vtLngCur(vtLog.DEBUG,''%(),self.GetOrigin())
        self.thdProc.Pause()
        for consumer in self.consumers:
            try:
                consumer[0].Pause()
            except:
                vtLog.vtLngCur(vtLog.ERROR,'consumer:%s'%(consumer[0]),self.GetOrigin())
                vtLog.vtLngTB(self.GetOrigin())
    def ResumeConsumer(self):
        if VERBOSE>=5:
            vtLog.vtLngCur(vtLog.DEBUG,''%(),self.GetOrigin())
        if self.oSF.IsShutDown:
            self.StopConsumer()
            return
        self.thdProc.Resume()
        for consumer in self.consumers:
            try:
                consumer[0].Resume()
            except:
                vtLog.vtLngCur(vtLog.ERROR,'consumer:%s'%(consumer[0]),self.GetOrigin())
                vtLog.vtLngTB(self.GetOrigin())
    def IsConsumerBusy(self,bCheckAll=False):
        bBusy=self.thdProc.IsBusy()   #False
        for consumer in self.consumers:
            try:
                if consumer[0].IsBusy():
                    bBusy=True
                    if vtLog.vtLngIsLogged(vtLog.INFO):
                        vtLog.vtLngCur(vtLog.INFO,'consumer:%s is busy'%(`consumer[0]`),self.GetOrigin())
                    if bCheckAll==False:
                        return bBusy
            except:
                vtLog.vtLngCur(vtLog.ERROR,'consumer:%s'%(consumer[0]),self.GetOrigin())
                vtLog.vtLngTB(self.GetOrigin())
        if bBusy==False:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'all consumers are inactive.',self.GetOrigin())
        else:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'at least one consumer is busy',self.GetOrigin())
        return bBusy
    def WaitConsumerNotBusy(self,zTimeOut=1,zTimeSleep=0.1,bThread=False):
        if len(self.consumers)==0:
            return True
        #if bThread==False:
        #    vtLog.vtLngCur(vtLog.CRITICAL,'FIXME, call only from thread'%(),self.GetOrigin())
        zWait=0.0
        while self.IsConsumerBusy(bCheckAll=True):
            time.sleep(zTimeSleep)
            zWait+=zTimeSleep
            if zWait>=zTimeOut:
                vtLog.vtLngCur(vtLog.CRITICAL,'timeout (%5.2f)'%(zWait),self.GetOrigin())
                return True
        return True
    def __setPaused__(self,flag,bAcquire=False):
        self.oSF.SetFlag(self.oSF.PSD)
        return
        if bAcquire:
            self.acquire('dom')
        try:
            self.bPaused=flag
        except:
            vtLog.vtLngTB(self.GetOrigin())
        if bAcquire:
            self.release('dom')
    def __isPaused__(self,bAcquire=False):
        return self.oSF.IsPaused
        if bAcquire:
            self.acquire('dom')
        try:
            bRet=self.bPaused
        except:
            vtLog.vtLngTB(self.GetOrigin())
            bRet=False
        if bAcquire:
            self.release('dom')
        return bRet
    def ClearConsumer(self,bThread=False):
        if len(self.consumers)==0:
            return
        #if bThread==False:
        #    vtLog.vtLngCur(vtLog.CRITICAL,'FIXME, call only from thread'%(),self.GetOrigin())
        vtSecXmlAclDom.ClearAcl(self)
        try:
            if VERBOSE>=5:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,
                        'isbusy:%d'%self.IsConsumerBusy(),self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.StopConsumer()
        while self.IsConsumerNotRunning():
            time.sleep(0.5)
            try:
                if VERBOSE>=5:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,
                            'isbusy:%d'%self.IsConsumerBusy(),self.GetOrigin())
            except:
                pass
        for consumer in self.consumers:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,repr(consumer[0]),self.GetOrigin())
            try:
                #consumer[1]()#.Clear()
                self.CallBackWX(consumer[1])
            except:
                vtLog.vtLngCur(vtLog.ERROR,'clear fault:%s'%(consumer[0]),self.GetOrigin())
                vtLog.vtLngTB(self.GetOrigin())
        self.StartConsumer()
    def Check2ClearConsumer(self,node):
        try:
            if self.__isPaused__(bAcquire=False):
                vtLog.vtLngCur(vtLog.CRITICAL,'id:%s'%(self.getKey(node)),self.GetOrigin())
            if VERBOSE>=5:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,
                        'isbusy:%d'%self.IsConsumerBusy(),self.GetOrigin())
        except:
            pass
        #self.StopConsumer()
        #while self.IsConsumerBusy():
        #    time.sleep(0.5)
        #    try:
        #        if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #            vtLog.vtLngCur(vtLog.DEBUG,
        #                'isbusy:%d'%self.IsConsumerBusy(),self.GetOrigin())
        #    except:
        #        pass
        for consumer in self.consumers:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,repr(consumer[0]),self.GetOrigin())
            try:
                consumer[0].Check2Clear(node)
            except:
                vtLog.vtLngCS(vtLog.WARN,'Check2Clear faulty;%s'%consumer[0],
                        origin=self.GetOrigin())
                vtLog.vtLngTB(self.GetOrigin())
    def Check2ClearConsumerByIdNum(self,idNum):
        try:
            if self.__isPaused__(bAcquire=False):
                vtLog.vtLngCur(vtLog.CRITICAL,'id:%08d'%(idNum),self.GetOrigin())
            if VERBOSE>=5:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,
                        'isbusy:%d'%self.IsConsumerBusy(),self.GetOrigin())
        except:
            pass
        for consumer in self.consumers:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,repr(consumer[0]),self.GetOrigin())
            try:
                consumer[0].Check2ClearByIdNum(idNum)
            except:
                vtLog.vtLngCS(vtLog.WARN,'Check2Clear faulty;%s'%consumer[0],
                        origin=self.GetOrigin())
                vtLog.vtLngTB(self.GetOrigin())
    def AddConsumerLang(self,consumer,func):
        self.consumersLang.append((consumer,func))
    def DelConsumerLang(self,consumer=None):
        try:
            if consumer is None:
                vtLog.vtLngCur(vtLog.INFO,'',self.GetOrigin())
                for tup in self.consumers:
                    #try:
                    #    tup[0].Clear()
                    #except:
                    #    vtLog.vtLngCur(vtLog.CRITICAL,'tup:%s'%(repr(tup[0])),self.GetOrigin())
                    try:
                        #tup[0].ClearDoc()
                        self.CallBackWX(tup[0].ClearDoc)
                    except:
                        vtLog.vtLngCur(vtLog.CRITICAL,'tup:%s'%(repr(tup[0])),self.GetOrigin())
                self.consumersLang=[]
                return
            for i in range(len(self.consumersLang)):
                if self.consumersLang[i][0]==consumer:
                    self.consumersLang=self.consumersLang[:i]+self.consumersLang[i+1:]
                    return
            #self.consumers.remove(consumer)
        except:
            pass
    def SetDftLanguages(self):
        self.SetLanguages([
            ('english','en',vtArt.getBitmap(vtArt.LangEn)),
            ('german','de',vtArt.getBitmap(vtArt.LangDe)),
            ])
        self.SetLang('en')
    def GetLanguageIds(self):
        try:
            if len(self.languages)==0:
                return None
            lst=[]
            for it in self.languages:
                lst.append(it[1])
            return lst
        except:
            return None
    def SetLang(self,lang):
        if VERBOSE>0:
            vtLog.vtLngCur(vtLog.INFO,'lang:%s(%s)'%(`lang`,`self.lang`),self.GetOrigin())
        if self.lang!=lang:
            self.lang=lang
            self.BuildLang()
            for consumer in self.consumersLang:
                consumer[1]()
    def GetLang(self):
        return self.lang
    def SetLanguages(self,lst):
        """provide a list of tuples (name,lang,bitmap)"""
        self.languages=lst
    def GetLanguages(self):
        return self.languages
    def GetLanguage(self,val,type=-1):
        """"""
        try:
            if type==-1:
                return self.languages[val]
            else:
                for tup in self.languages:
                    if tup[type]==val:
                        return tup
        except:
            pass
        return None,None,None
    def __setModified__(self,flag=True):
        #self.bModified=flag
        #self.__logDebug__('bFlag:%d'%(flag))
        #vtLog.vtLngCur(vtLog.DEBUG,'bFlag:%d'%(flag),origin=self.GetOrigin())
        self.oSF.SetFlag(self.oSF.MOD,flag)
    def IsModified(self):
        return self.oSF.IsModified
        return self.bModified
    def NotifyOpenProc(self,iAct,iSize):
        pass
    def New(self,revision='1.0',root=None,bConnection=False,bAcquire=True,bThread=False):
        if root is None:
            root=self.TAGNAME_ROOT
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'root:%s;bConnection:%d;bAcquire:%d'%(
                    root,bConnection,bAcquire),origin=self.GetOrigin())
        #acquireLibXml2()
        if bAcquire:
            if self.acquire('dom',blocking=False)==False:
                if vtLog.vtLngIsLogged(vtLog.INFO):
                    vtLog.vtLngCur(vtLog.INFO,'acquire not possible yet',origin=self.GetOrigin())
                return
        self.StopConsumer()
        self.WaitConsumerNotRunning(bThread=bThread)
        self.ClearConsumer(bThread=bThread)
        self.Close(bAcquire=False,bThread=bThread)
        try:
            self.audit.start(None,self.GetAppl())
            self.audit.log(vtLog.INFO,'new')
            self.__NewLib__(revision,root)
            try:
                self.StartConsumer()
                self.__setModified__()
                self.iElemCount=-1
                self.iElemIDCount=0
                self.GetElementCount()
                self.GetElementAttrCount()
                self.genIds(bAcquire=False)
                self.__New__(bConnection=bConnection)
            except:
                vtLog.vtLngTB(self.GetOrigin())
                self.genIds(bAcquire=False)
            self.AlignDoc()
            self.genIds(bAcquire=False)
        except:
            vtLog.vtLngTB(self.GetOrigin())
            self.StartConsumer()
        if bAcquire:
            self.release('dom')
        self.oSF.SetState(self.oSF.NEW)
        #releaseLibXml2()
    def __New__(self,bConnection=False):
        pass
    def SetFN(self,fn):
        try:
            vtLog.vtLngCur(vtLog.INFO,'old:%s;new:%s'%(self.sFN,fn),self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
        fn=self.__ensureDN__(fn)
        if self.sFN!=fn:
            self.audit.checkRename(fn)
        self.sFN=fn
    def GetFN(self):
        return self.sFN
    def __Open__(self,sFN):
        if sFN is None:
            if vtLog.vtLngIsLogged(vtLog.WARN):
                vtLog.vtLngCur(vtLog.WARN,'sFN is None',origin=self.GetOrigin())
            self.oSF.SetState(self.oSF.OPEN_FLT)
            self.NotifyOpenFault(self.sFN,'')
            #self.keepGoing = self.running = False
            return
        self.Close(bAcquire=False,bThread=True) # 080124:wro move up 
        self.sFN=sFN                            # 080124:wro move up 
        self.audit.start(sFN,self.GetAppl())
        self.audit.log(vtLog.INFO,'open')
        if os.path.exists(sFN)==False:
            if vtLog.vtLngIsLogged(vtLog.WARN):
                vtLog.vtLngCur(vtLog.WARN,'file:%s;do not exist'%(sFN),self.GetOrigin())
            #self.keepGoing = self.running = False
            self.oSF.SetState(self.oSF.OPEN_FLT)
            self.NotifyOpenFault(sFN,'')
            return
        #self.acquire('dom')
        # 080124:wro move up self.Close(bAcquire=False,bThread=True)
        # 080124:wro move up self.sFN=sFN
        try:
            #vtLog.vtLngCallStack(None,vtLog.DEBUG,sFN,verbose=1)
            self.oSF.SetState(self.oSF.OPENING)
            self.NotifyOpenStart(sFN)
            self.audit.backup(sFN)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        try:
            if self.open(sFN)<0:
                try:
                    self.oSF.SetState(self.oSF.OPEN_FLT)
                    self.NotifyOpenFault(sFN,list)
                    #doc.oSF.SetFlag(doc.oSF.OPENED)
                    #doc.oSF.SetFlag(doc.oSF.FAULT)
                except:
                    vtLog.vtLngTB(self.GetOrigin())
                return 0
            self.__setModified__(False)
            self.iElemCount=-1
            self.iElemIDCount=0
            #doc.GetElementCount()
            #doc.GetElementAttrCount()
            #doc.acquire()
            self.oSF.SetState(self.oSF.GENID)
            self.genIds(bAcquire=False)
            self.__setModified__(False)
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'analyse done;file:%s'%(sFN),self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
            try:
                self.oSF.SetState(self.oSF.OPEN_FLT)
                self.NotifyOpenFault(sFN,list)
                #doc.oSF.SetFlag(doc.oSF.OPENED)
                #doc.oSF.SetFlag(doc.oSF.FAULT)
            except:
                vtLog.vtLngTB(self.GetOrigin())
            return 0
        #self.doc.release()
        #self.release('dom')
        try:
            #self.keepGoing = self.running = False
            self.oSF.SetState(self.oSF.OPENED)
            self.NotifyOpenOk(sFN)
            #doc.oSF.SetFlag(doc.oSF.OPND)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def __ensureDN__(self,fn):
        try:
            if fn is not None:
                #return fn       # 070711:wro do not ensure directory yet
                dnTmp,fnTmp=os.path.split(fn)
                if (dnTmp is None) or (len(dnTmp)==0):
                    vtLog.vtLngCur(vtLog.WARN,'no directory part included',self.GetOrigin())
                    try:
                        fnTmp=os.path.join(vcCust.USR_LOCAL_DN,fn)
                        vtLog.vtLngCur(vtLog.INFO,'fn:%s;changed to;fn:%s'%(
                                        fn,fnTmp),self.GetOrigin())
                        return fnTmp
                    except:
                        vtLog.vtLngTB(self.GetOrigin())
                return fn
        except:
            vtLog.vtLngTB(self.GetOrigin())
        return fn
    def Open(self,fn,bThread=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'fn:%s;threading:%d;cwd:%s'%(
                    fn,bThread,os.getcwd()),origin=self.GetOrigin())
        def ensureDN(fn):
            return self.__ensureDN__(fn)
        if bThread:
            #try:
            #    self.release('dom')                # 070217:wro what for????
            #except:
            #    vtLog.vtLngCur(vtLog.WARN,''%(),self.GetOrigin())
            #if self.thdOpen.IsRunning():
            #    return -2
            if fn is not None:
                self.sFN=ensureDN(fn)
            #self.thdOpen.Open(self.sFN,self)
            self.DoSafe(self.__Open__,self.sFN)
        else:
            if self.oSF.IsClosed==False:
                self.Close(bAcquire=True,bThread=bThread)
            if fn is not None:
                self.sFN=ensureDN(fn)
            if self.sFN is None:
                if vtLog.vtLngIsLogged(vtLog.WARN):
                    vtLog.vtLngCur(vtLog.WARN,'sFN is None',origin=self.GetOrigin())
                return -1
            #if self.acquire('dom',blocking=False)==False:
            if self.acquire('dom',blocking=True)==False:    #091020:wro FIXME
                if vtLog.vtLngIsLogged(vtLog.INFO):
                    vtLog.vtLngCur(vtLog.INFO,'fn:%s;already acquired'%fn,origin=self.GetOrigin())
                #self.acquire()
                #self.release('dom')
                return 0
            
            self.audit.start(self.sFN,self.GetAppl())
            self.oSF.SetState(self.oSF.OPENING)
            
            self.audit.log(vtLog.INFO,'open')
            if os.path.exists(self.sFN)==False:
                if vtLog.vtLngIsLogged(vtLog.WARN):
                    vtLog.vtLngCur(vtLog.WARN,'file:%s;do not exist'%self.sFN,origin=self.GetOrigin())
                self.release('dom')
                return -1
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'parsing start;file:%s'%self.sFN,origin=self.GetOrigin())
            self.audit.backup(self.sFN)
            if self.open(self.sFN)<0:
                self.oSF.SetState(self.oSF.OPEN_FLT)
                self.release('dom')
                return -2
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'parsing start;file:%s'%self.sFN,origin=self.GetOrigin())
            try:
                self.__setModified__(False)
                self.iElemCount=-1
                self.iElemIDCount=0
                #self.GetElementCount()
                #self.GetElementAttrCount()
                self.oSF.SetState(self.oSF.GENID)
                self.genIds(bAcquire=False)
                self.__setModified__(False)
                #self.oSF.SetFlag(self.oSF.OPENED)
                #self.oSF.SetFlag(self.oSF.FAULT)
            except:
                vtLog.vtLngTB(self.GetOrigin())
            self.oSF.SetState(self.oSF.OPENED)
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'finished;file:%s'%fn,origin=self.GetOrigin())
            self.release('dom')
        return 0
    def CreateReq(self):
        pass
    def Build(self):
        """called by client on settled data.
        """
        pass
    def BuildSrv(self):
        """called by server on alias starting
        """
        pass
    def BuildLang(self):
        pass
    #def NotifyOpenOk(self,fn):
    #    pass
    #def NotifyOpenFault(self,fn,sExp):
    #    pass
    #def NotifySynchStart(self,count):
    #    pass
    #def NotifySynchProc(self,act,count):
    #    pass
    #def NotifySynchFinished(self):
    #    pass
    #def NotifySynchAborted(self):
    #    pass
    def __Close__(self):
        pass
    def Close(self,bAcquire=True,bThread=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'bAcquire:%d'%(
                    bAcquire),origin=self.GetOrigin())
        if bAcquire:
            if self.acquire('dom',blocking=False)==False:
                if vtLog.vtLngIsLogged(vtLog.INFO):
                    vtLog.vtLngCur(vtLog.INFO,'acquire not possible yet',origin=self.GetOrigin())
                return
        self.StopConsumer()
        self.WaitConsumerNotRunning(bThread=bThread)
        self.ClearConsumer(bThread=bThread)
        try:
            self.audit.log(vtLog.INFO,'close')
            self.audit.stop()
            self.sFN=None
            self.close()
            self.__Close__()
        except:
            vtLog.vtLngTB(self.GetOrigin())
        try:
            if self.oSF.IsShutDown==False:
                self.StartConsumer()
                self.__setModified__(False)
                self.iElemCount=-1
                self.iElemIDCount=-1
            else:
                # clean up consumers
                self.DelConsumer()
                self.DelConsumerLang()
                #self.genIds(bAcquire=False)
                #self.CreateIds(self.attr)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        if bAcquire:
            self.release('dom')
    def __Save__(self,sTmpFN,encoding='ISO-8859-1'):
        """ make sure 'dom'- is acquired and sTmpFN is valid.
        """
        try:
            vtLog.vtLngCur(vtLog.INFO,'sTmpFN:%s;FN:%s;enc:%s'%(vtLog.pformat(sTmpFN),
                    vtLog.pformat(self.sFN),encoding),self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
        if self.doc is None:
            return 0
        try:
            try:
                sDir=os.path.split(sTmpFN)
                os.makedirs(sDir[0])
            except:
                pass
        except:
            vtLog.vtLngTB(self.GetOrigin())
        if sTmpFN is None:
            fn=self.sFN
        else:
            fn=sTmpFN
        if sTmpFN is None:
            try:
                vtLog.vtLngCur(vtLog.WARN,'fn:%s;TmpFN:%s;FN:%s;enc:%s'%(vtLog.pformat(fn),
                        vtLog.pformat(fn),vtLog.pformat(self.sFN),encoding),
                        self.GetOrigin())
            except:
                vtLog.vtLngTB(self.GetOrigin())
        
        if self.save(fn)>0:
            self.SetFN(fn)
            try:
                self.__SavePersistant__()
            except:
                vtLog.vtLngTB(self.GetOrigin())
            if self.audit.isEnabled():
                self.audit.checkRename(self.sFN)
            self.oSF.SetFlag(self.oSF.MOD,False)
        return 0
    def __SavePersistant__(self):
        pass
    def SaveDelayed(self,fn=None,encoding='ISO-8859-1'):
        try:
            vtLog.vtLngCur(vtLog.INFO,'fn:%s;FN:%s;enc:%s'%(vtLog.pformat(fn),
                    vtLog.pformat(self.sFN),encoding),self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'',origin=self.GetOrigin())
        if self.root is None:
            return -3
        if self.doc is None:
            return -4
        if fn is None:
            sTmpFN=self.sFN
        else:
            sTmpFN=fn
            #self.sFN=fn        # 080202:wro do this after correct save
        if sTmpFN is None:
            vtLog.vtLngCur(vtLog.WARN,'file name is None',self.GetOrigin())
            return -1
        self.thdProc.DoSafe(self.AlignDoc)
        self.thdProc.DoSafe(self.__Save__,sTmpFN,encoding=encoding,bThread=True)
    def Save(self,fn=None,encoding='ISO-8859-1',bThread=False):
        try:
            vtLog.vtLngCur(vtLog.INFO,'fn:%s;FN:%s;enc:%s;bThread:%d'%(vtLog.pformat(fn),
                    vtLog.pformat(self.sFN),encoding,bThread),self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'',origin=self.GetOrigin())
        if self.root is None:
            return -3
        if self.doc is None:
            return -4
        if fn is None:
            sTmpFN=self.sFN
        else:
            sTmpFN=fn
            #self.sFN=fn        # 080202:wro do this after correct save
        if sTmpFN is None:
            vtLog.vtLngCur(vtLog.WARN,'file name is None',self.GetOrigin())
            return -1
        vtLog.vtLngCur(vtLog.INFO,'fn:%s;cur:%s'%(sTmpFN,self.sFN),self.GetOrigin())
        self.audit.log(vtLog.INFO,'save:%s'%sTmpFN)
        
        #acquireLibXml2()
        if self.acquire('dom',blocking=False)==False:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'acquire not possible yet',origin=self.GetOrigin())
            return -2
        if self.oSF.IsShutDown:
            self.StopConsumer()
            self.WaitConsumerNotRunning(bThread=bThread)
        else:
            self.PauseConsumer()
            self.WaitConsumerNotBusy(bThread=bThread)
        self.__setPaused__(True,bAcquire=False)
        iRet=self.__Save__(sTmpFN,encoding=encoding)
        #releaseLibXml2()
        self.release('dom')
        if self.oSF.IsShutDown:
            pass
        else:
            self.ResumeConsumer()
        self.__setPaused__(False,bAcquire=False)
        return iRet
    def __isAttrSkip4FingerPrint__(self,name):
        #if name in ['fp',self.attr]:
        if name=='fp':
            return True
        return False
    def __isNode2CalcFingerPrint__(self,node):
        return False
    def __getFingerPrintStrFromLst__(self,lst):
        def compFunc(a,b):
            return cmp(a[0],b[0])
        lst.sort(compFunc)
        c=''
        if self.verbose:
            lstNames=[]
        for it in lst:
            if self.verbose:
                lstNames.append(it[0])
            if it[1] is None:
                continue
            val=it[1].strip()
            if len(val)>0:
                c+=it[1]
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            if len(lstNames)>0:
                vtLog.vtLngCur(vtLog.DEBUG,'%s;%s'%(';'.join(lstNames),c),origin='fp'+self.GetOrigin())
        return c
    def __getFingerPrintContent__(self,node,bRec=False):
        lst=[]
        content=u''
        try:
            childs=self.getChildsNoAttr(node,self.attr)
            for c in childs:
                lst.append((self.getTagName(c),self.getText(c),c))
            content=self.__getFingerPrintStrAttr__(node)+self.__getFingerPrintStrFromLst__(lst)
            for tagName,sChilds,c in lst:
                content+=self.__getFingerPrintContent__(c)
            lst=[]
            for c in self.getChildsAttr(node,self.attr):
                if bRec==False:
                    if self.__isNode2CalcFingerPrint__(c)==False:
                        continue
                lst.append((self.getTagName(c),c))
            def compFunc(a,b):
                return cmp(a[0],b[0])
            lst.sort(compFunc)
            for tagName,c in lst:
                content+=self.__getFingerPrintContent__(c,True)
        except:
            vtLog.CallStack('')
            vtLog.vtLngTB('fp'+self.GetOrigin())
            print node
        return content
    def GetFingerPrint(self,node):
        if node is None:
            return ''
        m=newSha()#sha.new()
        if VERBOSE>10:
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(self.getKey(node)),origin=self.GetOrigin())
        try:
            content=self.__getFingerPrintContent__(node)
            if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'id:%s;%s'%(self.getKey(node),content),origin='fp'+self.GetOrigin())
            #content=self.GetNodeXmlContent(node)
            if type(content)==types.UnicodeType:
                try:
                    #m.update(content.encode('ISO-8859-1'))
                    m.update(content.encode('utf-8','ignore'))
                except:
                    vtLog.vtLngCur(vtLog.ERROR,'id:%s'%(self.getKey(node)),origin='fp'+self.GetOrigin())
                    vtLog.vtLngTB('fp'+self.GetOrigin())
                    m.update(content)
            else:
                m.update(content)
        except:
            #print type(content)#,content
            vtLog.vtLngCur(vtLog.ERROR,'id:%s'%(self.getKey(node)),origin='fp'+self.GetOrigin())
            vtLog.vtLngTB('fp'+self.GetOrigin())
        #vtLog.vtLngCur(vtLog.DEBUG,'id:%s\nfp:%s\ncontent:%s'%(self.getKey(node),m.hexdigest(),content))
        self.__HandleInternal__('upd',self.getKey(node))
        return m.hexdigest()
    def GetFingerPrintAndStore(self,node):
        if node is None:
            vtLog.vtLngCur(vtLog.WARN,'node is None'%(),self.GetOrigin())
            return ''
        #vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(self.getKey(node)),self.GetOrigin())
        sFP=self.getAttribute(node,'fp')
        if len(sFP)==0:
            sFP=self.GetFingerPrint(node)
            self.setAttribute(node,'fp',sFP)
            self.__setModified__()
        #vtLog.vtLngCur(vtLog.DEBUG,'id:%s\nfp:%s\nnode:%s'%(self.getKey(node),sFP,node))
        return sFP
    def clearFingerPrint(self,node):
        if VERBOSE>10:
            #vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(self.getKey(node)),self.GetOrigin())
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(self.getKey(node)),origin=self.GetOrigin())
        self.removeAttribute(node,'fp')
        self.__setModified__()
    def clearFingerPrintFull(self,node):
        if VERBOSE>10:
            #vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(self.getKey(node)),self.GetOrigin())
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(self.getKey(node)),origin=self.GetOrigin())
        self.removeAttribute(node,'fp')
        self.__setModified__()
        self.procChildsKeys(node,self.clearFingerPrintFull)
        return 0
        #for c in self.getChildsAttr(node,self.attr):
        #    self.removeAttribute(node,'fp')
    def getFingerPrint(self,node):
        return self.getAttribute(node,'fp')
    def calcFingerPrint(self,node):
        #vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(self.getKey(node)),self.GetOrigin())
        self.removeAttribute(node,'fp')
        sFP=self.GetFingerPrint(node)
        self.setAttribute(node,'fp',sFP)
        self.ids.__calc__(self.getKeyNum(node),self)# FPupd
        self.__setModified__()
    def __calcFingerPrintFull__(self,node):
        sFP=self.GetFingerPrint(node)
        self.setAttribute(node,'fp',sFP)
        self.procChildsKeys(node,self.__calcFingerPrintFull__)
        return 0
    def calcFingerPrintFull(self,node):
        #vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(self.getKey(node)),self.GetOrigin())
        self.clearFingerPrintFull(node)
        #for c in self.getChildsAttr(node,self.attr):
        #    self.calcFingerPrint(c)
        self.calcFingerPrint(node)
        self.procChildsKeys(node,self.__calcFingerPrintFull__)
        self.__setModified__()
    def GetXmlContent(self,node=None):
        return self.getXmlContent(node)
    def GetNodeXmlContent(self,node=None,bFull=False,bIncludeNodes=False):
        if node is None:
            node=self.getRoot()
        return self.getNodeXmlContent(node,bFull=bFull,bIncludeNodes=bIncludeNodes)
    def getAuditOriginByNode(self,node,sLogin4Audit=None):
        if node is None:
            id=-1
        else:
            id=self.getKeyNum(node)
        sTag=self.getTagName(node)
        if sLogin4Audit is None:
            if self.oLogin is None:
                return u''.join([u'%s.%d'%(sTag,id),u';',u'__:-1'])
            else:
                return u''.join([u'%s.%d'%(sTag,id),u';',self.oLogin.GetAuditName()])
        else:
            return u''.join([u'%s.%d'%(sTag,id),u';',sLogin4Audit])
    def SetNodeXmlContent(self,node,s,sLogin4Audit=None):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'%s'%(s),self.GetOrigin())
        if node is None:
            vtLog.vtLngCur(vtLog.CRITICAL,'node is None;%s'%(s),self)
            return None         # 090707:wro do not use root node
            node=self.getRoot()
        if self.audit.isEnabled():
            self.audit.write('SetNodeXmlContent',s,self.GetNodeXmlContent(node),
                            origin=self.getAuditOriginByNode(node,sLogin4Audit))
        node=self.setNodeXmlContent(node,s)
        self.ids.__calc__(self.getKeyNum(node),self)        # FPupd
        self.__HandleInternal__('upd',self.getKey(node))    # FPupd
        self.__setModified__()
        #self.AlignNode(node,iRec=2)   # 090321:wro skip increases performance
        return node
    def SetNodeXmlContentFull(self,node,s,sLogin4Audit=None):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'%s'%(s),self.GetOrigin())
        if node is None:
            vtLog.vtLngCur(vtLog.CRITICAL,'node is None;%s'%(s),self)
            return None         # 090707:wro do not use root node
            node=self.getRoot()
            nodePar=None
        else:
            nodePar=self.getParent(node)
        if self.audit.isEnabled():
            self.audit.write('SetNodeXmlContentFull',s,self.GetNodeXmlContent(node,False,True),
                            origin=self.getAuditOriginByNode(node,sLogin4Audit))
        tmp=self.setNodeXmlContentFull(nodePar,node,s)
        self.__setModified__()
        #self.AlignNode(tmp,iRec=10)   # 090321:wro skip increases performance
        return tmp
    def __addNodeXmlContentExternal__(self,node,nodeBase,expAttr,lValidTagNames,
                                lNewID,lOldID,sLogin4Audit=None):
        bAdd=False
        if lValidTagNames is None:
            bAdd=True
        else:
            if self.getTagName(node) in lValidTagNames:
                bAdd=True
        if bAdd:
            self.__addNodeXmlContentExternalLib__(node,nodeBase,
                    expAttr,lValidTagNames,lNewID,lOldID)
            if self.audit.isEnabled():
                self.audit.write('__addNodeXmlContentExternal__',self.GetNodeXmlContent(node,False,False),
                            origin=self.getAuditOriginByNode(node,sLogin4Audit))
        return 0
    def AddNodeXmlContentExternal(self,node,s,expAttr='eid',lValidTagNames=None):
        if node is None:
            node=self.getRoot()
        idNew,idOld=self.addNodeXmlContentExternal(node,s,expAttr=expAttr,
                    lValidTagNames=lValidTagNames)
        self.__setModified__()
        #self.AlignNode(node,iRec=10)   # 090321:wro skip increases performance
        return idNew,idOld
    def AddNodeXmlContent(self,node,s,sLogin4Audit=None):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'%s'%(s),self.GetOrigin())
        bDbg=False
        if node is None:
            node=self.getRoot()
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            bDbg=True
            iId=self.getKeyNum(node)
            vtLog.vtLngCur(vtLog.DEBUG,'id:%d;start'%(iId),self.GetOrigin())
        if self.audit.isEnabled():
            self.audit.write('AddNodeXmlContent2Node',s,
                            origin=self.getAuditOriginByNode(node,sLogin4Audit))
        idNew,tmp=self.addNodeXmlContent(node,s)
        #if bDbg:
        #    vtLog.vtLngCur(vtLog.DEBUG,'id:%d;auit'%(iId),self)
        if self.audit.isEnabled():
            self.audit.write('AddNodeXmlContent',self.getNodeXmlContent(tmp,False,False),
                            origin=self.getAuditOriginByNode(tmp,sLogin4Audit))
        self.__setModified__()
        #if bDbg:
        #    vtLog.vtLngCur(vtLog.DEBUG,'id:%d;align'%(iId),self)
        #self.AlignNode(node,iRec=10)   # 090321:wro skip increases performance
        if bDbg:
            vtLog.vtLngCur(vtLog.DEBUG,'id:%d;done'%(iId),self.GetOrigin())
        return idNew
    def SetNodeXmlConfig(self,node,s,sLogin4Audit=None):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'%s'%(s),self.GetOrigin())
        if node is None:
            node=self.getRoot()
        tmp=self.setNodeXmlConfig(node,s)
        if tmp:
            self.__setModified__()
            self.AlignNode(node,iRec=10)
            if self.audit.isEnabled():
                self.audit.write('SetNodeXmlConfig',self.getNodeXmlContent(tmp,False,False),
                            origin=self.getAuditOriginByNode(tmp,sLogin4Audit))
        return tmp is not None
    def SetNodeXmlSpecial(self,node,sTagName,s,sLogin4Audit=None):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'%s'%(s),self.GetOrigin())
        if node is None:
            node=self.getRoot()
        tmp=self.setNodeXmlSpecial(node,sTagName,s)
        if tmp:
            self.__setModified__()
            self.AlignNode(node,iRec=10)
            if self.audit.isEnabled():
                self.audit.write('SetNodeXmlSpecial',self.getNodeXmlContent(tmp,False,False),
                            origin=self.getAuditOriginByNode(tmp,sLogin4Audit))
        return tmp is not None
    def CopyNode(self,node,bFull=False,bIncludeNodes=False):
        return self.copyNode(node,bFull=bFull,bIncludeNodes=bIncludeNodes)
    def ParseBuffer(self,s,attr='id'):
        if self.audit.isEnabled():
            self.audit.write('ParseBuffer',s)
        ret=self.parseBuffer(s,attr=attr)
        if ret:
            self.__setModified__()
            if hasattr(self,'NotifySynchStart'):
                self.NotifySynchStart(0)
            if hasattr(self,'NotifySynchFinished'):
                self.NotifySynchFinished(True)
        return ret
    def Synch(self):
        return
        while self.thdOpen.IsRunning():
            #print 'wait to open finished'
            time.sleep(1)
        while self.thdSynch.IsRunning():
            #print 'wait synch to stop'
            time.sleep(1)
        if self.doc is None:
            fn=self.sFN
            self.New()
            self.sFN=fn
        self.thdSynch.Synch()
    def __synchAdd__(self,idPar,tmpNode,bClone=False):
        try:
            #vtLog.vtLngCur(vtLog.INFO,'idPar:%s'%(idPar),self.GetOrigin())
            #idPar=self.getKey(self.getParent(tmpNode))
            #if vtLog.vtLngIsLogged(vtLog.DEBUG):
            #    vtLog.vtLngCur(vtLog.DEBUG,'idPar:%s;node:%s'%(idPar,tmpNode),self.GetOrigin())
            nodePar=self.getNodeById(idPar)
            if bClone:
                node=self.cloneNode(tmpNode,True)
            else:
                node=tmpNode
            #if nodePar is None:
            #    nodePar=self.getBaseNode()
            if nodePar is None:
                return -2
            self.appendChild(nodePar,node)
            self.ids.CheckId(node,self)
            #self.__genIds__(node,node,self.ids)
            self.ids.ProcessMissing(self)
            self.ids.ClearMissing()
            self.__setModified__()
            self.AlignNode(node,iRec=12)
            if self.audit.isEnabled():
                self.audit.write('__synchAdd__',self.GetNodeXmlContent(node,False,False),
                            origin=self.getAuditOriginByNode(node))
            #
            return 1
        except:
            vtLog.vtLngTB(self.GetOrigin())
            return -1
    def __synchAdd2Srv__(self,node):
        try:
            idPar=self.getKey(self.getParent(node))
            if self.IsKeyValid(idPar):
                nodePar=self.ids.GetIdStr(idPar)[1]
            else:
                nodePar=self.getBaseNode()
                idPar=self.getKey(nodePar)
            #if vtLog.vtLngIsLogged(vtLog.DEBUG):
            #        vtLog.vtLngCur(vtLog.DEBUG,'before idPar:%s;%s'%(idPar,node),self.GetOrigin())
            #self.__genIds__(node,self.ids)
            self.AlignNode(node,iRec=12)
            self.removeAttribute(node,'offline')
            self.removeAttribute(node,'offline_content')    # 070831:wro offline attrs shall not be send to srv
            #self.removeIds(node)       # 070201:wro
            #content=self.GetNodeXmlContent(node,bFull=False,bIncludeNodes=False)
            #print content
            self.addNode2Srv(nodePar,node)
            if self.audit.isEnabled():
                self.audit.write('__synchAdd2Srv__',self.GetNodeXmlContent(node,False,False),
                            origin=self.getAuditOriginByNode(node))
            #if vtLog.vtLngIsLogged(vtLog.DEBUG):
            #        vtLog.vtLngCur(vtLog.DEBUG,'after idPar:%s;%s'%(idPar,node),self.GetOrigin())
            #
            return 1
        except:
            vtLog.vtLngTB(self.GetOrigin())
            return -1
    def __synchDel__(self,node):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(self.getKey(node)),self.GetOrigin())
            #idPar=self.getKey(self.getParent(node))
            if self.audit.isEnabled():
                self.audit.write('__synchDel__',self.GetNodeXmlContent(node,False,False),
                            origin=self.getAuditOriginByNode(node))
            self.__deleteNode__(node)
            self.__setModified__()
            return 1
        except:
            vtLog.vtLngTB(self.GetOrigin())
            #if vtLog.vtLngIsLogged(vtLog.ERROR):
            #    vtLog.vtLngCallStack(None,vtLog.ERROR,
            #                '%s'%(traceback.format_exc()),
            #                origin=self.GetOrigin())
            return -1
    def __synchSet2Srv__(self,node):
        try:
            self.removeAttribute(node,'offline')            # 070831:wro offline attrs shall not be send to srv
            self.removeAttribute(node,'offline_content')    # 070831:wro offline attrs shall not be send to srv
            #self.removeIds(node)       # 070201:wro
            #content=self.GetNodeXmlContent(node,bFull=False,bIncludeNodes=False)
            #print content
            self.setNode2Srv(node)
            if self.audit.isEnabled():
                self.audit.write('__synchSet2Srv__',self.GetNodeXmlContent(node,False,False),
                            origin=self.getAuditOriginByNode(node))
            #if vtLog.vtLngIsLogged(vtLog.DEBUG):
            #        vtLog.vtLngCur(vtLog.DEBUG,'after idPar:%s;%s'%(idPar,node),self.GetOrigin())
            #
            return 1
        except:
            vtLog.vtLngTB(self.GetOrigin())
            return -1
    def __synchMove__(self,par,node):
        if node is None:
            return -2
        if par is None:
            return -3
        try:
            try:
                if self.audit.isEnabled():
                    self.audit.write('__synchMove__:%d from:%d to:%d'%\
                            (self.getKeyNum(node),self.getKeyNum(self.getParent(node)),self.getKeyNum(par),),\
                            self.GetNodeXmlContent(node,False,False),
                            origin=self.getAuditOriginByNode(node))
            except:
                vtLog.vtLngTB(self.GetOrigin())
            self.unlinkNode(node,self.getParent(node))  # 080204 wro: provide parent node
            self.appendChild(par,node)
            self.AlignNode(node,iRec=10)
            self.__setModified__()
            return 1
        except:
            vtLog.vtLngTB(self.GetOrigin())
            #vtLog.vtLngCur(vtLog.ERROR,'%s'%(traceback.format_exc()),origin=self.GetOrigin())
            return -1
        
    def __isSkip__(self,node):
        if self.getTagName(node) in self.skip:
            return True
        return False
    def IsSkip(self,node):
        if self.getTagName(node) in self.skip:
            return True
        if self.ids.IsOfflineDel(self.getKeyNum(node)):
            return True
        return False
    def __genIdsOld__(self,node,ids):
        childs=self.getChilds(node)
        for c in childs:
            if c.name in self.skip:
                continue
            id=self.getAttribute(c,self.attr)
            if len(id)>0:
                ids.CheckId(c,id)
            self.__genIds__(c,ids)
    def genIds(self,node=None,bAcquire=True):
        #vtLog.CallStack('')
        if bAcquire:
            self.acquire('dom')
        try:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                    vtLog.vtLngCur(vtLog.INFO,'started',origin=self.GetOrigin())
            self.CreateIds(self.attr)
            if node is None:
                node=self.getBaseNode()
            self.procChildsKeys(node,self.__genIds__,node,self.ids)
            if self.ids.ProcessMissing(self):
                self.__setModified__()
            self.ids.ClearMissing()
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'finished',origin=self.GetOrigin())
            if self.verbose:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(str(self.ids.GetIDs())),origin=self.GetOrigin())
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(vtLog.pformat(self.ids.FPs)),origin=self.GetOrigin())
            self.CreateAcl()
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'finished create acl',origin=self.GetOrigin())
            #self.Build()
            #if vtLog.vtLngIsLogged(vtLog.INFO):
            #    vtLog.vtLngCur(vtLog.INFO,'finished build',origin=self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
        if bAcquire:
            self.release('dom')
    def genIdsRef(self,bAcquire=True,bThreaded=True):
        pass
    def CreateNode(self,parNode,**kwargs):
        return None
    def SetNode(self,node,**kwargs):
        if node is not None:
            for kwName,kwAttr,nodeName,attrName in self.NODE_ATTRS:
                try:
                    sName=kwargs[kwName]
                except:
                    sName=''
                bAddAttr=False
                try:
                    if kwAttr is not None:
                        sId=kwargs[kwAttr]
                        if sId is not None:
                            self.setNodeTextAttr(node,nodeName,sName,attrName,sId)
                            bAddAttr=True
                except:
                    pass
                if bAddAttr==False:
                    self.setNodeText(node,nodeName,sName)
    def GetPossibleAcl_old(self):
        return None
    def GetPossibleFilter_old(self):
        return None
    def CreateAcl_old(self):
        tmpAcl=self.getChild(self.getRoot(),'security_acl')
        try:
            self.objAcl=vtXmlAclList(self,tmpAcl,verbose=0)
        except:
            pass#traceback.print_exc()
    def IsAccessOk_old(self,node,objLogin,iAcl):
        #vtLog.CallStack('')
        try:
            return self.objAcl.IsAccessOk(node,objLogin,iAcl)
        except:
            pass#traceback.print_exc()
    def GetActiveAcl_old(self):
        vtLog.CallStack('')
        node=self.getChild(self.getRoot(),'security_acl')
        for c in self.getChilds(node):
            if self.getAttribute(c,'active')=='1':
                return self.getTagName(c),c
        return None,None
    def GetElementCount(self,node=None):
        if node is None:
            node=self.root
            if self.iElemCount>=0:
                return self.iElemCount
            self.iElemCount=self.__getElementCount__(node)
        return self.__getElementCount__(node)
    def GetElementAttrCount(self,node=None,attr='id'):
        if node is None:
            node=self.root
            if node is None:
                return 0
            if attr==self.attr:
                if self.iElemIDCount>=0:
                    return self.iElemIDCount
                self.iElemIDCount=self.__getElementAttrCount__(node,attr)
                return self.iElemIDCount
            else:
                return self.__getElementAttrCount__(node,attr)
        else:
            return self.__getElementAttrCount__(node,attr)
    def getChildsAttrVal(self,node=None,tagName=None,attr=None,attrVal=None):
        if node is None:
            node=self.getBaseNode()#self.root
        return self.__getChildsAttrVal__(node,tagName,attr,attrVal)
    
    def getChildsAttr(self,node,attr):
        return self.__getChildsAttr__(node,attr)
    def getChildsKeys(self,node):
        return self.__getChildsAttr__(node,self.attr)
    def getChildsNoAttr(self,node,attr):
        return self.__getChildsNoAttr__(node,attr)
    def procChildsKeysRecHierReStart(self,lHier,lv,node,func,*args,**kwargs):
        if node is None:
            return -2,None
        lHierNew=[]
        iRet=self.__procChildsKeysRecHierReStart__(lHier,lHierNew,lv,node,func,*args,**kwargs)
        #print lHierNew
        lHierNew.reverse()
        return iRet,lHierNew
    def setFilters(self,filters,name='tmpFilter'):
        dFilters={}
        #self.dFilterInfos={}
        #self.dFilterRes={}
        infos=[]
        for tagName,filter in filters:
            try:
                filter.Start()
            except:
                pass
            try:
                lst=dFilters[tagName]
                lst.append(filter)
            except:
                lst=[filter]
                dFilters[tagName]=lst
                #self.dFilterRes[tagName]=''
                try:
                    infos.index(tagName)
                except:
                    infos.append(tagName)
        self.setNodeInfos2Get(infos,name,dFilters)
    def delFilters(self,name='tmpFilter'):
        self.delNodeInfos(name)
    def matchFilters(self,node,lang=None,name='tmpFilter'):
        try:
            d=self.dUsrInfos[name]
            dFlt=d['cltData']
        except:
            return True
        self.acquire()
        tagName,dRes=self.getNodeInfos(node,lang,name)
        keys=dFlt.keys()
        for k in keys:
            try:
                sVal=dRes[k]
                for flt in dFlt[k]:
                    if flt.match(sVal)==False:
                        self.release()
                        return False
            except:
                pass
        self.release()
        return True
    def getChildsRec(self,node,tagName):
        lst=[]
        self.__getChildsRec__(node,tagName,lst)
        return lst 
    def getChildByLst(self,node,lst):
        for s in lst:
            node=self.getChild(node,s)
            if node is None:
                return None
        return node
    def getChildByLstForced(self,node,lst):
        for s in lst:
            node=self.getChildForced(node,s)
            if node is None:
                return None
        return node
    def getKey(self,node):
        if self.getTagName(node)==self.getTagName(self.getBaseNode()):
            # 100613:wro check if root node has no valid key
            if self.IsKeyValid(self.getAttribute(node,self.attr))==False:
                return '-1'
        #if node==self.getBaseNode():
        #    return '-1'
        return self.getAttribute(node,self.attr)
    def getKeyNum(self,node):
        try:
            return long(self.getKey(node))
        except:
            return -2
    def IsKeyValid(self,id):
        try:
            if long(id)>=0:
                return True
        except:
            pass
        return False
    def IsNodeKeyValid(self,node):
        return self.IsKeyValid(self.getKey(node))
    def isSameKey(self,node,id):
        try:
            idNode=self.getKeyNum(node)
            return idNode==long(id)
        except:
            return False
    def isSameKeyNum(self,node,id):
        idNode=self.getKeyNum(node)
        return idNode==id
    def hasKey(self,node):
        return self.hasAttribute(node,self.attr)
    def getForeignKey(self,node,attr='fid',appl=None):
        if node is None:
            return '-4'
        if appl is None:
            return self.getAttribute(node,attr)
        else:
            fid=self.getAttribute(node,attr)
            i=fid.find('@')
            if i>0:
                if fid[i+1:]==appl:
                    return fid[:i]
            elif i==-1:
                return fid
            return '-3'
    def getForeignKeyAppl(self,node,attr='fid',appl=None):
        if node is None:
            return '-4',None
        fid=self.getAttribute(node,attr)
        i=fid.find('@')
        if i>0:
            if appl is None:
                return fid[:i],fid[i+1:]
            if fid[i+1:]==appl:
                return fid[:i],fid[i+1:]
            else:
                return '-3',None
        elif i==-1:
            if appl is None:
                return fid,self.GetApplName()
            else:
                return fid,appl
        return '-3',None
    def getForeignKeyNumAppl(self,node,attr='fid',appl=None):
        if node is None:
            return -4,None
        try:
            fid=self.getAttribute(node,attr)
            i=fid.find('@')
            if i>0:
                if appl is None:
                    return long(fid[:i]),fid[i+1:]
                if fid[i+1:]==appl:
                    return long(fid[:i]),fid[i+1:]
                else:
                    return -3,None
            elif i==-1:
                if appl is None:
                    return long(fid),self.GetApplName()
                else:
                    return long(fid),appl
        except:
            pass
        return -3,None
    def convForeign(self,fid,fallback=-2):
        if type(fid) in [types.IntType,types.LongType]:
            return fid,None
        try:
            i=fid.find('@')
            if i>0:
                return long(fid[:i]),fid[i+1:]
            else:
                return long(fid),None
        except:
            return fallback,None
    def setForeignKey(self,node,attr='fid',attrVal='',appl=None):
        try:
            j=attrVal.find('@')
            if j<0:
                i=long(attrVal)
            else:
                i=long(attrVal[:j])
        except:
            i=long(attrVal)
        if i>=0:
            s='%08d'%i
        else:
            s='%d'%i
        if appl is not None:
            self.setAttribute(node,attr,s+'@'+appl)
        else:
            self.setAttribute(node,attr,s)
    def getAttributeTypeNode(self,node):
        if node is None:
            return None
        if self.hasAttribute(node,'ih'):
            nodePar=self.getParent(node)
            sID=self.getAttribute(nodePar,'iid')
            nodeTmp=self.getNodeById(sID)
            if nodeTmp is not None:
                nodeTmpAttr=self.getChild(nodeTmp,self.getTagName(node))
                return self.getAttributeTypeNode(nodeTmpAttr)
            return None
        if self.hasAttribute(node,'fid'):
            sID=self.getAttribute(node,'fid')
            if sID=='1':
                nodePar=self.getParent(node)
            else:
                nodePar=node
            sID=self.getAttribute(nodePar,'fid')
            nodeTmp=self.getNodeById(sID)
            if nodeTmp is not None:
                nodeTmpAttr=self.getChild(nodeTmp,self.getTagName(node))
                return self.getAttributeTypeNode(nodeTmpAttr)
            return None
        c=self.getChild(node,'type')
        if c is not None:
            return node
        #vtLog.vtLngCur(vtLog.CRITICAL,'check me',self.GetOrigin())
        #vtLog.vtLngCur(vtLog.WARN,'CRITICAL possible to fix, check me',self.GetOrigin())
        sInheritance=self.getNodeText(node,'inheritance')
        strs=sInheritance.split(',')
        if 'type' in strs:
            nodePar=self.getParent(node)
            c=self.getChild(nodePar,'inheritance_type')
            if c is not None:
                return c
        c=self.getChild(node,'inheritance_type')
        if c is not None:
            return c
        #vtLog.vtLngCur(vtLog.WARN,'no inheritance type definition found',self.GetOrigin())  # 140615 wro: do not warn the obvious
        return None
    def getAttributeVal(self,node):
        if node is None:
            return ''
        if self.hasAttribute(node,'aid'):
            return self.getText(node)
        if self.hasAttribute(node,'ih'):
            return self.getText(node)
        if self.hasAttribute(node,'fid'):
            return self.getText(node)
        # FIXME
        #if self.doc.getChild(aNode,'__browse') is not None:
        #        Val=vtXmlHierarchy.applyCalcNode(doc,aNode)
        
        c=self.getChild(node,'val')
        if c is not None:
            return self.getText(c)
        return self.getText(node)
    def getNodeAttributeVal(self,node,tagName):
        return self.getAttributeVal(self.getChild(node,tagName))
    def getAttributeValLang(self,node,lang=None):
        if node is None:
            return ''
        if self.hasAttribute(node,'aid'):
            return self.getText(node)
        if self.hasAttribute(node,'ih'):
            return self.getText(node)
        if self.hasAttribute(node,'fid'):
            return self.getText(node)
        # FIXME
        #if self.doc.getChild(aNode,'__browse') is not None:
        #        Val=vtXmlHierarchy.applyCalcNode(doc,aNode)
        c=self.getChild(node,'val')
        if c is not None:
            return self.getText(c)
        return self.getText(node)
    def getNodeAttributeValLang(self,node,tagName,lang=None):
        return self.getAttributeValLang(self.getChildLang(node,tagName,lang),lang)
    def getAttributeValFullLang_old(self,node,lang=None):
        if node is None:
            return ''
        if self.hasAttribute(node,'ih'):
            return self.getText(node)
        if self.hasAttribute(node,'fid'):
            return self.getText(node)
        # FIXME
        #if self.doc.getChild(aNode,'__browse') is not None:
        #        Val=vtXmlHierarchy.applyCalcNode(doc,aNode)
        c=self.getChild(node,'val')
        if c is not None:
            cUnit=self.getChildLang(node,'unit',lang)
            if cUnit is not None:
                return self.getText(c)+' ['+self.getText(cUnit)+']'
            return self.getText(c)
        return self.getText(node)
    def getAttributeValFullLang(self,node,lang=None):
        # FIXME but CHECK first
        #vtLog.vtLngCur(vtLog.WARN,'CRITICAL possible to fix, check me',self.GetOrigin())   # 140615 wro: do not warn the obvious
        sVal=self.getAttributeVal(node)
        nodeType=self.getAttributeTypeNode(node)
        cUnit=self.getChildLang(nodeType,'unit',lang)
        if cUnit is not None:
            return ''.join([sVal,' [',self.getText(cUnit),']'])
        return sVal
    def getNodeAttributeValFullLang(self,node,tagName,lang=None):
        return self.getAttributeValFullLang(self.getChildLang(node,tagName,lang),lang)
    def GetNodeAttributeValFullLang(self,node,tagName,lang=None):
        return self.getAttributeValFullLang(self.getChildLang(node,tagName,lang),lang)
    def getNodeText(self,node,tagName):
        return self.getText(self.getChild(node,tagName))
    def getNodeTextAttr(self,node,tagName,attr,attrVal):
        child=self.getChildAttr(node,tagName,attr,attrVal)
        return self.getText(child)
    def getNodeTextLang(self,node,tagName,lang):
        child=self.getChildLang(node,tagName,lang)
        return self.getText(child)
    def hasNodeInfos2Get(self,name='basic'):
        return name in self.dUsrInfos
    def setNodeInfos2Get(self,infos,name='basic',cltData=None,bSingleLang=True):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'infos:%s;name:%s;cltData:%d;bSingLang:%s'%\
                    (vtLog.pformat(infos),name,cltData is None,bSingleLang),self.GetOrigin())
        if name in self.dUsrInfos:
            vtLog.vtLngCur(vtLog.CRITICAL,
                    'name:%s already present, delete it first;dUsrInfo:%s'%(name,
                    vtLog.pformat(self.dUsrInfos)),self.GetOrigin())
            return
        #self.dInfos={}
        #self.dRes={}
        #vtLog.CallStack('')
        #print infos
        dInfos={}
        dRes={}
        for tag in infos:
            #self.dRes[tag]=''
            #dTmp=self.dInfos
            dRes[tag]=''
            dTmp=dInfos
            strs=tag.split(':')
            if len(strs)>1:
                for i in range(len(strs)-1):
                    try:
                        d=dTmp[strs[i]]
                    except:
                        dTmp[strs[i]]={strs[i+1]:{}}
                    dTmp=dTmp[strs[i]]
            tmpTag=strs[-1]
            strs=tmpTag.split('|')
            sNode=strs[0]
            if len(strs)>1:
                sAttr=strs[1]
            else:
                sAttr=''
            try:
                d=dTmp[sNode]
                d[sAttr]=tmpTag
            except:
                dTmp[sNode]={sAttr:tmpTag}
        #self.iLenRes=len(self.dRes.keys())
        self.dUsrInfos[name]={
                'infos':dInfos,
                'res':dRes,
                'len':len(dRes.keys()),
                'cltData':cltData,
                'singleLang':bSingleLang,
            }
    def delNodeInfos(self,name='basic'):
        try:
            del self.dUsrInfos[name]
            vtLog.vtLngCur(vtLog.DEBUG,'name:%s;dUsrInfos:%s'%(name,
                        vtLog.pformat(self.dUsrInfos)),self.GetOrigin())
        except:
            pass
    def getRoot(self):
        return self.root
    def getBaseNode(self):
        return self.root
    def setNodeTextAttr(self,node,tagName,newValue,attr,attrVal):
        if node is None:
            return
        child=self.getChildAttr(node,tagName,attr,attrVal)
        if child is not None:
            return self.setText(child,newValue)
        else:
            self.createSubNodeTextAttr(node,tagName,newValue,attr,attrVal)
            self.iElemCount+=1
            if attr=='id':
                self.iElemIDCount+=1
            return 1
    def setNodeTextLang(self,node,tagName,newValue,lang):
        if lang is None:
            lang=self.lang
            if lang is None:
                return None
        if node is None:
            return
        child=self.getChildLang(node,tagName,lang)
        if self.getAttribute(child,'language')!=lang:
            child=None
        if child is not None:
            return self.setText(child,newValue)
        else:
            self.createSubNodeTextAttr(node,tagName,newValue,'language',lang)
            #self.iElemCount+=self.iElemCount
            return 1
    def setAttributeVal(self,node,val):
        if node is None:
            return ''
        if self.hasAttribute(node,'ih'):
            return self.setText(node,val)
        if self.hasAttribute(node,'fid'):
            return self.setText(node,val)
        c=self.getChild(node,'val')
        if c is not None:
            return self.setText(c,val)
        return self.setText(node,val)
    def setNodeAttributeVal(self,node,tagName,val):
        return self.setAttributeVal(self.getChild(node,tagName),val)
    def GetValue(self,node,tagName,type,fallback):
        try:
            return type(self.getNodeAttributeVal(node,tagName))
        except:
            self.getChildForced(node,tagName)
            self.setNodeAttributeVal(node,tagName,str(fallback))
            self.__setModified__()
            return fallback
    def SetValue(self,node,tagName,value):
        self.getChildForced(node,tagName)
        self.setNodeAttributeVal(node,tagName,str(value))
        self.__setModified__()
    def prcGetNodeDictAttr(self,node,sAttr,sVal,lAttr):
        lAttr.append((sAttr,sVal))
        return 1
    def prcGetNodeDict(self,node,dDict,bDbg):
        
        sTagName=self.getTagName(node)
        sVal=self.getText(node)
        lAttr=[]
        self.procAttrsExt(node,self.prcGetNodeDictAttr,lAttr)
        iLenAttr=len(lAttr)
        if bDbg:
            vtLog.vtLngCur(vtLog.DEBUG,
                    'tag:%s;val:%s;atr:%s'%(sTagName,sVal,
                    vtLog.pformat(lAttr)),
                    origin=self.GetOrigin())
        if iLenAttr>0:
            sK=tuple(lAttr)
        else:
            sK=None
            #dDict[sTagName]=sVal
        if sTagName in dDict:
            dVal=dDict[sTagName]
            if type(dVal)==types.DictType:
                dVal[sK]=sVal
            else:
                dDict[sTagName]={None:dVal,sK:sVal}
        else:
            if sK is None:
                dVal=None#sVal
                dDict[sTagName]=sVal
            else:
                dVal={}
                dVal[sK]=sVal
                dDict[sTagName]=dVal
        dSub={}
        self.procChildsNoKeys(node,self.prcGetNodeDict,dSub)
        if len(dSub)>0:
            dDict[-2]=dSub
        return 1
    def GetNodeDict(self,node):
        bDbg=True
        dDict={}
        sTagName=self.getTagName(node)
        sKey=self.getKey(node)
        if bDbg:
            vtLog.vtLngCur(vtLog.DEBUG,
                    'tag:%s;key:%s'%(sTagName,sKey),
                    origin=self.GetOrigin())
        self.procChildsNoKeys(node,self.prcGetNodeDict,dDict,bDbg)
        if bDbg:
            vtLog.vtLngCur(vtLog.DEBUG,
                    'tag:%s;key:%s;atr:%s'%(sTagName,sKey,
                    vtLog.pformat(dDict)),
                    origin=self.GetOrigin())
        return dDict
    def createNew(self,root,revision='0.0.1',attr='id'):
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'',origin=self.GetOrigin())
        self.Close()
        self.__createNewLib__(root,revision=revision,attr=attr)
        self.__setModified__()
        self.CreateIds(self.attr)
        self.CreateAcl()
        self.iElemCount=-1
        self.iElemIDCount=-1
        return self.root
    def createSubNode(self,node,tagname,align=False):
        if node is None:
            node=self.getBaseNode()#self.root
        if node is None:
            return None
        n=self.__createSubNodeLib__(node,tagname)
        self.__setModified__()
        self.iElemCount+=1
        if align==True:
            self.AlignNode(n)
        return n
    def createSubNodeAttr(self,node,tagname,attr,attrVal,align=False):
        if node is None:
            node=self.getBaseNode()#self.root
        if node is None:
            return
        n=self.__createSubNodeAttrLib__(node,tagname,attr,attrVal)
        self.__setModified__()
        self.iElemCount+=1
        if attr==self.attr:
            self.iElemIDCount+=1
            # check ids
            self.ids.CheckId(n,self)
            self.__checkIds__(n)
            self.processMissingId()
            self.clearMissingId()
        if align==True:
            self.AlignNode(n)
        return n
    def createMissingSubNodeByLst(self,node,lst,align=False):
        if node is None:
            node=self.getBaseNode()#self.root
        if node is None:
            return None
        if self.__createMissingSubNodeByLstLib__(node,lst):
            if align==True:
                self.AlignNode(node)
        return node
    def createSubNodeText(self,node,tagname,val,align=False):
        if node is None:
            node=self.getBaseNode()#self.root
        if node is None:
            return
        n=self.__createSubNodeTextLib__(node,tagname,val)
        
        self.__setModified__()
        self.iElemCount+=1
        if align==True:
            self.AlignNode(n)
        return n
    def createSubNodeTextAttr(self,node,tagname,val,attr,attrVal,align=False):
        if node is None:
            node=self.getBaseNode()#self.root
        if node is None:
            return
        n=self.__createSubNodeTextAttrLib__(node,tagname,val,attr,attrVal)
        
        self.__setModified__()
        self.iElemCount+=1
        if attr==self.attr:#'id':
            self.iElemIDCount+=1
            # check ids
            self.ids.CheckId(n,self)
            self.__checkIds__(n)
            self.processMissingId()
            self.clearMissingId()
        if align==True:
            self.AlignNode(n)
        return n
    def createSubNodeDict(self,node,dict,align=False):
        if node is None:
            return
        t=dict['tag']
        if dict.has_key('val'):
            v=dict['val']
            if dict.has_key('attr'):
                c=self.createSubNodeTextAttr(node,t,v,dict['attr'][0],dict['attr'][1],align=align)
            else:
                c=self.createSubNodeText(node,t,v,align=align)
            if dict.has_key('lst'):         # 090321:wro add sub childs now, valids FP misscalc
                for i in dict['lst']:
                    self.createSubNodeDict(c,i,align=align)
        else:
            c=self.createSubNode(node,t,align=align)
            if dict.has_key('attr'):         # 090321:wro add sub childs now, valids FP misscalc
                self.setAttribute(c,dict['attr'][0],dict['attr'][1])
                if dict['attr'][0]==self.attr:
                    self.iElemIDCount+=1
                    # check ids
                    self.ids.CheckId(c,self)
                    self.__checkIds__(c)
                    self.processMissingId()
                    self.clearMissingId()
            if dict.has_key('lst'):
                for i in dict['lst']:
                    self.createSubNodeDict(c,i,align=align)
        #if dict.has_key('lst'):    # 090321:wro add sub childs preivous, here leads to FP misscalc
        #    for i in dict['lst']:
        #        self.createSubNodeDict(c,i,align=align)
        return c
    def createChildByLst(self,node,tagname,lst,align=False):
        if node is None:
            return
        n=self.createSubNode(node,tagname)
        for i in lst:
            self.createSubNodeDict(n,i,align=align)
        if align==True:
            self.AlignNode(n)
        return n
    def __procGetId__(self,n,dId):
        dId[self.getKeyNum(n)]=n
        return 0
    def __procChangeIdRefChild__(self,n,dIdChg,bDbg=False):
        try:
            sIdOld=self.getAttribute(n,'fid')
            if len(sIdOld)>0:
                #if bDbg==True:
                #    self.__logDebug__(['sIdOld',sIdOld])
                iFind=sIdOld.find('@')
                if iFind<0:
                    # ok maybe to change
                    try:
                        iIdOld=long(sIdOld)
                        if iIdOld in dIdChg:
                            iIdNew=dIdChg[iIdOld]
                            #if bDbg==True:
                            #    self.__logDebug__('change;sIdOld:%d;iIdNew:%d'%(iIdOld,iIdNew))
                            self.setAttribute(n,'fid',self.ConvertIdNum2Str(iIdNew))
                    except:
                        self.__logTB__()
            
            sVal=self.getText(n)
            
        except:
            self.__logTB__()
        return 0
    def __procChangeIdRef__(self,n,dIdChg,bDbg=False):
        self.procChildsNoKeys(n,self.__procChangeIdRefChild__,dIdChg,bDbg=bDbg)
        return 0
    def __procChangeIdValChild__(self,n,dIdChg,bDbg=False):
        try:
            sVal=self.getText(n)
            bMod=False
            for sO,sN in dIdChg.iteritems():
                try:
                    iFind=sVal.find(sO)
                    if iFind>=0:
                        sVal=sVal.replace(sO,sN)
                        bMod=True
                except:
                    self.__logTB__()
            if bMod==True:
                self.setText(n,sVal)
        except:
            self.__logTB__()
        return 0
    def __procChangeIdVal__(self,n,dIdChg,bDbg=False):
        self.procChildsNoKeys(n,self.__procChangeIdValChild__,dIdChg,bDbg=bDbg)
        return 0
    def addNode(self,par,node,func=None,**kwargs):
        if node is None:
            return
        bDbg=False
        if VERBOSE>5:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'parId:%s'%(self.getKey(par)),self.GetOrigin())
                bDbg=True
        #self.ids.AddNewNode(node)
        
        if self.__isSkip__(node):
            return
        self.__setModified__()
        dIdOld={}
        self.procKeysRec(99,node,self.__procGetId__,dIdOld)
        #if bDbg==True:
        #    self.__logDebug__(['dIdOld',dIdOld])
        self.ids.CheckId(node,self)
        self.__checkIds__(node)
        self.processMissingId()
        self.clearMissingId()
        dIdChg={}
        dIdChgStr={}
        for iIdOld,n in dIdOld.iteritems():
            iIdNew=self.getKeyNum(n)
            if iIdNew!=iIdOld:
                dIdChg[iIdOld]=iIdNew
                sT=''.join(['[',self.ConvertIdNum2Str(iIdOld),']'])
                dIdChgStr[sT]=''.join(['[',self.ConvertIdNum2Str(iIdNew),']'])
        for iIdOld,n in dIdOld.iteritems():
            iIdNew=self.getKeyNum(n)
            if iIdNew!=iIdOld:
                dIdChg[iIdOld]=iIdNew
        if bDbg==True:
            vtLog.vtLngCur(vtLog.DEBUG,
                    "%s"%(vtLog.pformat(['dIdChg',dIdChg])),
                    self.GetOrigin())
        self.procKeysRec(99,node,self.__procChangeIdRef__,dIdChg,bDbg=bDbg)
        self.procKeysRec(99,node,self.__procChangeIdVal__,dIdChgStr,bDbg=bDbg)
        dIdOld=None
        dIdChg=None
        self.updateParentDict(par,node)
        if func is not None:
            func(par,node,**kwargs)
    def moveNode(self,par,node):
        self.unlinkNode(node,self.getParent(node))       # 080204 wro: provide parent node
        self.appendChild(par,node)
        self.__setModified__()
    def __deleteNodeIds__(self,node,*args):
        if self.__isSkip__(node)==False:
            id=self.getKey(node)
            if len(id)>0:
                self.__HandleInternal__('del',id)
                self.ids.RemoveId(id)
                self.removeParentDict(node)
            self.procChildsAttr(node,self.attr,self.__deleteNodeIds__)
        return 0
    def __check2clear__(self,node,*args):
        id=self.getKey(node)
        if len(id)>0:
            try:
                self.Check2ClearConsumerByIdNum(long(id))
            except:
                self.Check2ClearConsumer(node)
        self.procChildsAttr(node,self.attr,self.__check2clear__)
        #for c in self.getChilds(node):
        #    self.__check2clear__(c)
    def delNode(self,node):
        self.deleteNode(node)
    def deleteNode(self,node,nodePar=None):
        if VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                #vtLog.vtLngCur(vtLog.DEBUG,'node:%s'%node,self.GetOrigin())
                #vtLog.vtLngCur(vtLog.DEBUG,'node: not None %d'%(node is not None),self.GetOrigin())
                vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(self.getKey(node)),self.GetOrigin())
        if node is None:
            return
        bPause=False
        try:
            if self.__isPaused__(bAcquire=False):
                #vtLog.vtLngCur(vtLog.CRITICAL,'not paused yet',self.GetOrigin())
                self.PauseConsumer()
                self.WaitConsumerNotBusy()
                bPause=True
                self.__setPaused__(True,bAcquire=False)
            self.__check2clear__(node)
            self.__deleteNode__(node,nodePar=nodePar)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        if bPause:
            self.ResumeConsumer()
            self.__setPaused__(False,bAcquire=False)
        self.__setModified__()
    def isSame(self,a,b,attr='id'):
        if a is None:
            return 0
        if b is None:
            return 0
        try:
            a1=self.getAttribute(a,attr)   # 070612:wro long(self.getAttribute(a,attr))
            b1=self.getAttribute(b,attr)   # 070612:wro long(self.getAttribute(b,attr))
            return a1==b1
        except:
            return 0
    def getSortedNodeIds(self,hierStart,tagName,idName,infoTags):
        node=self.getBaseNode()
        tmp=SortedNodeIds()
        for s in hierStart:
            node=self.getChild(node,s)
            if node is None:
                return tmp
        tmp.SetNode(self,node,tagName,idName,infoTags)
        return tmp
    def __getSortedNodeIdsRec__(self,node,tmp,tagName,idName,infoTags):
        if self.getTagName(node)==tagName:
            tmp.AddNodeSingle(node,idName,infoTags,self)
        return 0
    def getSortedNodeIdsRec(self,hierStart,tagName,idName,infoTags):
        node=self.getBaseNode()
        tmp=SortedNodeIds()
        #tmp.SetDoc(self)
        for s in hierStart:
            node=self.getChild(node,s)
            if node is None:
                return tmp
        self.procChildsKeysRec(1000,node,self.__getSortedNodeIdsRec__,
                                tmp,tagName,idName,infoTags)
        tmp.Sort()
        if self.verbose>1:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'keys:%s;ids:%s'%\
                        (vtLog.pformat(tmp.keys),vtLog.pformat(tmp.ids)),self.GetOrigin())
        return tmp
    def CreateIds(self,attr='id'):
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'',origin=self.GetOrigin())
        #print 'CreateIds'
        #vtLog.vtCallStack(5)
        if self.ids is not None:
            del self.ids
        self.attr=attr
        self.ids=NodeIds(self.GetOrigin(),attr)
    def setIds(self,ids):
        if self.ids is not None:
            del self.ids
        self.ids=ids
    def getIds(self):
        if self.ids is None:
            return NodeIds(self.GetOrigin())
        else:
            return self.ids
    def __HandleInternal__(self,action,Id):
        pass
    def changeIdNum(self,idOld,idNew,sAlias):
        vtLog.vtLngCur(vtLog.INFO,'id:%08d;idNew:%08d'%(idOld,idNew),self.GetOrigin())
        #if hasattr(self,'APPL_REF'):
        #    if sAlias in self.APPL_REF:
        #        self.refs.changeId(idOld,idNew,sAlias)
    def changeId(self,node,id):
        #if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,node,origin=self.GetOrigin())
        
        idOld=self.getAttribute(node,self.attr)
        idNew=self.ids.ConvertIdStr2Str(id)
        vtLog.vtLngCur(vtLog.INFO,'id:%s;idNew:%s'%(idOld,idNew),self.GetOrigin())
        if idOld==idNew:
            return
        #self.ids.RemoveId(idOld)
        #print idNew
        nodeOld=self.getNodeById(idOld)
        self.setAttribute(node,self.attr,idNew)
        #print nodeOld,idOld
        #print node,idNew
        if self.isSame(node,nodeOld):
            self.ids.RemoveId(idOld)
        else:
            vtLog.vtLngCur(vtLog.WARN,'idOld:%s is already used -> may never happen'%(idOld),self.GetOrigin())
        self.__setModified__()
        try:
            if self.ids.IsIdUsed(long(idNew)):
                vtLog.vtLngCur(vtLog.CRITICAL,'idNew:%s is already used -> should never happen'%(idNew),self.GetOrigin())
                vtLog.vtLngCur(vtLog.WARN,'idNew:%s is already used -> should never happen'%(idNew),self.GetOrigin())
        #        self.ids.RemoveId(idNew)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.clearFingerPrint(node)
        self.ids.CheckId(node,self)
        self.ids.ProcessMissing(self)
        self.ids.ClearMissing()
        try:
            if hasattr(self,'changeIdNet'):
                self.changeIdNet(node,idOld)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        #try:
        #    self.netMaster.changeIdNum(long(idOld),long(idNew),self.appl)
        #except:
        #    vtLog.vtLngTB(self.GetOrigin())
    def checkId(self,node):
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'',origin=self.GetOrigin())
        self.ids.CheckId(node,self)
        self.ids.ProcessMissing(self)   # 070309:wro proc
        self.ids.ClearMissing()     # 070309:wro clear
    def removeId(self,node):
        id=self.getKey(node)
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,id,origin=self.GetOrigin())
        self.__HandleInternal__('del',id)
        self.ids.RemoveId(id)
    def __removeId__(self,node):
        if self.__isSkip__(node)==False:
            id=self.getKey(node)
            self.__HandleInternal__('del',id)
            self.ids.RemoveId(id)
            self.procChildsAttrExt(node,self.attr,self.__removeId__)
        return 0
    def removeIds(self,node):
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'',origin=self.GetOrigin())
        self.procAttrExt(node,self.attr,self.__removeId__)
    def processMissingId(self):
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'',origin=self.GetOrigin())
        if self.ids.ProcessMissing(self):
            self.__setModified__()
    def clearMissing(self):
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'',origin=self.GetOrigin())
        self.ids.ClearMissing()
    def clearMissingId(self):
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'',origin=self.GetOrigin())
        self.ids.ClearMissing()
    def removeInternalNodes(self,node,remove=[]):
        for c in self.getChilds(node):
            try:
                if self.getTagName(c) in remove:
                    self.deleteNode(c)
            except:
                pass
            self.removeInternalNodes(c)
            
    def __clearId__(self,node,*args):
        self.setAttribute(node,self.attr,'')
        self.procChildsAttr(node,self.attr,self.__clearId__)
        return 0
    def clearIds(self,node):
        self.procAttr(node,self.attr,self.__clearId__)
    def __recalcId__(self,node,*args):
        self.ids.AddNewNode(node,self)
        self.procChildsAttr(node,self.attr,self.__recalcId__)
        return 0
    def recalcIds(self,node):
        self.procAttr(node,self.attr,self.__recalcId__)
    def getNodeById(self,id):
        if self.ids is None:
            return None
        try:
            if long(id)<0:
                return self.getBaseNode()
        except:
            pass
        tup=self.ids.GetIdStr(id)
        return tup[1]
    def getNodeByIdNum(self,id):
        if self.ids is None:
            return None
        if id<0:
            return self.getBaseNode()
        tup=self.ids.GetId(id)
        return tup[1]
    def GetNodeById(self,id):
        if self.ids is None:
            return None
        try:
            if long(id)<0:
                return self.getBaseNode()
        except:
            pass
        tup=self.ids.GetIdStr(id)
        return tup[1]
    def GetNodeByIdNum(self,id):
        if self.ids is None:
            return None
        if id<0:
            return self.getBaseNode()
        tup=self.ids.GetId(id)
        return tup[1]
    def GetId(self,id):
        if self.ids is None:
            return (0,None)
        if id<0:
            return (1,self.getBaseNode())
        return self.ids.GetId(id)
    def getTmplBase(self,node):
        nodePar=self.getParent(node)
        if self.getChild(nodePar,'__node') is not None:
            return self.getTmplBase(nodePar)
        return node
    def getInstBase(self,node):
        nodePar=self.getParent(node)
        if len(self.getAttribute(nodePar,'iid'))>0:
            return self.getInstBase(nodePar)
        return node
    def GetIdStr(self,id):
        if self.ids is None:
            return None
        try:
            if long(id)<0:
                return (1,self.getBaseNode())
        except:
            pass
        return self.ids.GetIdStr(id)
    def ConvertIdStr2Str(self,id):
        try:
            return '%08d'%(long(id))
        except:
            vtLog.vtLngTB(self.appl)
            return id
    def ConvertIdNum2Str(self,id):
        try:
            return '%08d'%(id)
        except:
            vtLog.vtLngTB(self.appl)
            return id
    def getPossibleNode(self):
        return []
    def GetSettings(self):
        if self.root is None:
            return None
        return self.getChildForced(self.root,'settings')
    def ClearCache(self):
        try:
            n=self.getChild(self.getRoot(),'security_acl')
            if n is not None:
                nC=self.getChild(n,'__cache__')
                if nC is not None:
                    self.unlinkNode(nC,n)
                    #nC.unlinkNode()
                    #nC.freeNode()
        except:
            vtLog.vtLngTB(self.GetOrigin())

class vtXmlDom(vtXmlDomCore,vtXmlDomBase):
    def __init__(self,attr='id',skip=[],synch=False,appl='',verbose=0,audit_trail=True):
        vtXmlDomBase.__init__(self,attr=attr,skip=skip,synch=synch,
                appl=appl,verbose=verbose,audit_trail=audit_trail)
    def __initLib__(self):
        vtXmlDomCore.__init__(self)
        vtLog.vtLngCur(vtLog.DEBUG,'%s'%(self.getLibClassName()),self.GetOrigin())
    def __del__(self):
        vtXmlDomBase.__del__(self)
        vtXmlDomCore.__del__(self)

def test1():
    dom1=vtXmlDom()
    dom1.Open('g:/company/python/boa/tool/xml/test01.xml')
    childs=dom1.getChilds()
    print childs
    
    childs=dom1.getChilds(None,'prjengs')
    print childs
    child=dom1.getChild(None,'prjengs')
    #print child
    print 'count',dom1.GetNodeCount(child)
    childInst=dom1.getChild(child,'instance')
    childs=dom1.getChilds(childInst,'name')
    print childs
    print dom1.getNodeText(childInst,'name')
    child=dom1.getChildAttr(childInst,'name','language','de')
    print child
    print dom1.getText(child)
    dom1.Close()
def test1a():
    dom1=vtXmlDom()
    for i in range(1000):
        dom1.Open('v:/python/vidarc/tool/xml/test/test01a.xml')
        dom1.Close()
def test2():
    dom2=vtXmlDom()
    root=dom2.createNew('engs_doc')
    print root
    eng=dom2.createSubNode(None,'eng',False)
    set=dom2.createSubNode(None,'set',False)
    inf=dom2.createSubNode(None,'info',False)
    dom2.setNodeText(set,'name','test')
    dom2.setAttribute(set,'id','0101')
    dom2.setAttribute(set,'lang','en')
    print '==================== align ===================='
    dom2.AlignDoc(2)
    dom2.Save('v:/python/vidarc/tool/xml/test/test02.xml')
    dom2.removeAttribute(set,'lang')
    dom2.Save('v:/python/vidarc/tool/xml/test/test02b.xml')
def test3():
    print '\n\n----------------test3--------------------'
    from vidarc.tool.xml.vtXmlNodeBase import vtXmlNodeBase
    o1=vtXmlNodeBase('steps')
    o2=vtXmlNodeBase('step')
    dom3=vtXmlDom()
    dom3.RegisterNode(o1)
    dom3.RegisterNode(o2)
    root=dom3.createNew('link')
    root=dom3.getRoot()
    dom3.createSubNode(None,'header')
    o1.Create(root)
    o2.Create(root)
    print dom3.CreateNode(None,'steps')
    dom3.AlignDoc()
    dom3.Save('v:/python/vidarc/tool/xml/test/test03.xml')
    
if __name__=='__main__':
    test1()
    test2()
    test3()
