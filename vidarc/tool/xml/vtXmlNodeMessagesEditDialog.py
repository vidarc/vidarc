#Boa:Dialog:vtXmlNodeMessagesEditDialog
#----------------------------------------------------------------------------
# Name:         vtXmlNodeMessagesEditDialog.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060422
# CVS-ID:       $Id: vtXmlNodeMessagesEditDialog.py,v 1.3 2006/08/29 10:06:24 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
from vidarc.tool.xml.vtXmlNodeDialog import *
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeMessagesPanel import *

def create(parent):
    return vtXmlNodeMessagesEditDialog(parent)

[wxID_VTXMLNODEMESSAGESEDITDIALOG, wxID_VTXMLNODEMESSAGESEDITDIALOGCBCANCEL, 
] = [wx.NewId() for _init_ctrls in range(2)]

class vtXmlNodeMessagesEditDialog(wx.Dialog,vtXmlNodeDialog):
    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsBt, (1, 0), border=4,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER, span=(1, 1))

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=0, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_gbsData_Items(self.gbsData)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTXMLNODEMESSAGESEDITDIALOG,
              name=u'vtXmlNodeMessagesEditDialog', parent=prnt,
              pos=wx.Point(348, 174), size=wx.Size(500, 495),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE,
              title=u'vtXmlNodeMessages Edit Dialog')
        self.SetClientSize(wx.Size(492, 468))

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLNODEMESSAGESEDITDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Close'),
              name=u'cbCancel', parent=self, pos=wx.Point(0, 24),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VTXMLNODEMESSAGESEDITDIALOGCBCANCEL)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self._init_ctrls(parent)
        vtXmlNodeDialog.__init__(self)
        try:
            self.doc=None
            self.node=None
            self.gbsData.AddGrowableCol(0)
            self.gbsData.AddGrowableRow(0)
            self.pn=vtXmlNodeMessagesPanel(self,id=wx.NewId(),pos=(0,0),size=(440,285),
                            style=0,name=u'pnNodeMessages')
            self.gbsData.AddWindow(self.pn, (0, 0), border=4,
                  flag=wx.BOTTOM | wx.TOP | wx.LEFT | wx.RIGHT | wx.EXPAND, span=(1, 1))
        except:
            vtLog.vtLngTB('')
    def OnCbApplyButton(self, event):
        self.pn.Close()
        self.EndModal(1)
        event.Skip()
    def OnCbCancelButton(self, event):
        self.pn.Close()
        self.EndModal(0)
        event.Skip()
