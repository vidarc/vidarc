#Boa:Dialog:vtXmlTreeAddDialog

import wx
import wx.lib.buttons

import sys,traceback
import images_dlg

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase

def create(parent):
    return vtXmlTreeAddDialog(parent)

[wxID_VTXMLTREEADDDIALOG, wxID_VTXMLTREEADDDIALOGCBAPPLY, 
 wxID_VTXMLTREEADDDIALOGCBCANCEL, wxID_VTXMLTREEADDDIALOGLSTNODES, 
] = [wx.NewId() for _init_ctrls in range(4)]

class vtXmlTreeAddDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTXMLTREEADDDIALOG,
              name=u'vtXmlTreeAddDialog', parent=prnt, pos=wx.Point(363, 204),
              size=wx.Size(328, 328), style=wx.DEFAULT_DIALOG_STYLE,
              title=_(u'Add Node Dialog'))
        self.SetClientSize(wx.Size(320, 301))

        self.lstNodes = wx.ListCtrl(id=wxID_VTXMLTREEADDDIALOGLSTNODES,
              name=u'lstNodes', parent=self, pos=wx.Point(8, 8),
              size=wx.Size(304, 252), style=wx.LC_REPORT)
        self.lstNodes.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstNodesListItemDeselected,
              id=wxID_VTXMLTREEADDDIALOGLSTNODES)
        self.lstNodes.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstNodesListItemSelected,
              id=wxID_VTXMLTREEADDDIALOGLSTNODES)
        self.lstNodes.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstNodesListColClick,
              id=wxID_VTXMLTREEADDDIALOGLSTNODES)
        self.lstNodes.Bind(wx.EVT_LEFT_DCLICK, self.OnLstNodesLeftDclick)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLTREEADDDIALOGCBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Apply'), name=u'cbApply',
              parent=self, pos=wx.Point(56, 268), size=wx.Size(76, 30),
              style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VTXMLTREEADDDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLTREEADDDIALOGCBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Cancel'), name=u'cbCancel',
              parent=self, pos=wx.Point(168, 268), size=wx.Size(76, 30),
              style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VTXMLTREEADDDIALOGCBCANCEL)

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self._init_ctrls(parent)
        #self.doc=None
        #self.node=None
        #self.nodePar=None
        self.lstNodes.InsertColumn(0,_(u'tag'),wx.LIST_FORMAT_LEFT,80)
        self.lstNodes.InsertColumn(1,_(u'description'),wx.LIST_FORMAT_LEFT,200)
        self.cbApply.SetBitmapLabel(images_dlg.getApplyBitmap())
        self.cbCancel.SetBitmapLabel(images_dlg.getCancelBitmap())
        self.imgDict={}
        self.imgLstNode=wx.ImageList(16,16)
        self.lstNodes.SetImageList(self.imgLstNode,wx.IMAGE_LIST_NORMAL)
        self.lstNodes.SetImageList(self.imgLstNode,wx.IMAGE_LIST_SMALL)
    def SetPossibleNodes(self,parent,doc,nodePar,lst):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,vtLog.pformat(lst),self)
        self.sTagName=None
        #self.parent=parent
        #self.doc=doc
        #self.node=None
        #self.nodePar=nodePar
        self.lstPosNodes=lst
        self.iSelIdx=-1
        self.lstNodes.DeleteAllItems()
        i=0
        for it in lst:
            img=-1
            try:
                img=self.imgDict[it[0]]
            except:
                try:
                    o=doc.GetRegisteredNode(it[0])
                    if o.IsAclOk(doc.ACL_MSK_ADD)==False:
                        i+=1
                        continue
                    img=self.imgLstNode.Add(o.GetBitmap())
                    self.imgDict[it[0]]=img
                except:
                    img=-1
            idx=self.lstNodes.InsertImageStringItem(sys.maxint, it[0], img)
            self.lstNodes.SetStringItem(idx,1,it[1],-1)
            self.lstNodes.SetItemData(idx,i)
            i+=1
    def GetTagName(self):
        return self.sTagName
    def GetNode(self):
        return self.node
    def OnCbApplyButton(self, event):
        if self.iSelIdx<0:
            self.EndModal(0)
        if self.iSelIdx>=0:
            try:
                # check register node
                i=self.lstNodes.GetItemData(self.iSelIdx)
                self.sTagName,sDesc=self.lstPosNodes[i]
                #o=self.doc.GetRegisteredNode(sTagName)
                #if vtLog.vtLngIsLogged(vtLog.DEBUG):
                #    vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;o found==%d'%(sTagName,o is not None),self)
                #iRes,self.node=o.DoAdd(self.parent,self.nodePar)
                #vtLog.vtLngCurWX(vtLog.DEBUG,'node:%s'%(self.node),self)
                self.EndModal(1)
                #o,n=self.doc.CreateNode(self.nodePar,sTagName)
                #o.DoEdit(self.parent,self.doc,n)
            except:
                vtLog.vtLngTB(self.GetName())
                self.EndModal(-1)
            pass
        event.Skip()

    def OnCbCancelButton(self, event):
        self.sTagName=None
        self.EndModal(0)
        event.Skip()

    def OnLstNodesListItemDeselected(self, event):
        self.iSelIdx=-1
        event.Skip()

    def OnLstNodesListItemSelected(self, event):
        self.iSelIdx=event.GetIndex()
        event.Skip()

    def OnLstNodesListColClick(self, event):
        self.iSelIdx=-1
        event.Skip()

    def OnLstNodesLeftDclick(self, event):
        if self.iSelIdx<0:
            return
        self.OnCbApplyButton(event)
        event.Skip()
