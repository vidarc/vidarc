#Boa:Dialog:vgdXmlDlgAdd
#----------------------------------------------------------------------------
# Name:         vtXmlAddDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtXmlAddDialog.py,v 1.2 2006/01/17 12:11:13 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import cStringIO


def create(parent):
    return vgdXmlDlgAdd(parent)

[wxID_VGDXMLDLGADD, wxID_VGDXMLDLGADDCHCLOC, wxID_VGDXMLDLGADDCHCNODETYPE, 
 wxID_VGDXMLDLGADDGCBBCANCEL, wxID_VGDXMLDLGADDGCBBOK, 
 wxID_VGDXMLDLGADDLBLLOC, wxID_VGDXMLDLGADDLBLNAME, 
 wxID_VGDXMLDLGADDLBLNODETYPE, wxID_VGDXMLDLGADDTXTNAME, 
] = map(lambda _init_ctrls: wx.NewId(), range(9))

#----------------------------------------------------------------------
def getOkData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00nIDATx\x9c\xa5\x93\xc9\r\xc0 \x0c\x04\x17D\x01\xd0^\n\xa4=\xd2\x01yD\
\x96L\x08\xe0c\xdf\xecx\x0c"\x00\xbd\xc3\x91h)\xe5Z\x90k\x01\x00$m\x91r_M\
\x07\xa02\x15)\xa2\x15Ve1`U\x16\x01\xf8\xde&\xc0)[\xc0n\xf7\x01\xc0\xdf\xd5e\
`\x01E\xe0U\xfcjJ\xf4\'\x03:\xac\xb1\x98.\x91O<M\xff\x05h\x13\xbc\xdf\xf9\
\x01\x97y&\xadH\xfc\xe0%\x00\x00\x00\x00IEND\xaeB`\x82' 

def getOkBitmap():
    return wx.BitmapFromImage(getOkImage())

def getOkImage():
    stream = cStringIO.StringIO(getOkData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getCancelData():
    return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00lIDATx\x9c\xad\x93\xc1\r\xc00\x08\x03M\xb7\xe8\xfe\xc3e\x0c\xfa\x8bB\
bC$\xca\x9b;\x01N\x0cpG\xa3\x9e\x0e\xfc\xaf`\xc00`%\xb0\xf7M\xc1\x0b\x9f\r\
\x19\xbc\xf6\x06A%a\xf0!P\x12\x05\x03\x80\xa9\x18\xf7)\x18L'`\x80\x82S\x01[\
\xe1Z\xb0\xee\\\xa5s\x08\xd8\xc12I\x10d\xd7V\x92\xf0\x12\x15\x9cId\x8c\xb7\
\xd5\xfeL\x1f$\x07+\xb8\xd6Q\x0bp\x00\x00\x00\x00IEND\xaeB`\x82" 

def getCancelBitmap():
    return wx.BitmapFromImage(getCancelImage())

def getCancelImage():
    stream = cStringIO.StringIO(getCancelData())
    return wx.ImageFromStream(stream)


class vtXmlAddDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VGDXMLDLGADD, name=u'vgdXmlDlgAdd',
              parent=prnt, pos=wx.Point(334, 226), size=wx.Size(226, 191),
              style=wx.DEFAULT_DIALOG_STYLE, title=u'vgd xml-Node Add')
        self.SetClientSize(wx.Size(218, 164))

        self.lblName = wx.StaticText(id=wxID_VGDXMLDLGADDLBLNAME, label=u'name',
              name=u'lblName', parent=self, pos=wx.Point(22, 8), size=wx.Size(26,
              13), style=0)

        self.txtName = wx.TextCtrl(id=wxID_VGDXMLDLGADDTXTNAME, name=u'txtName',
              parent=self, pos=wx.Point(80, 8), size=wx.Size(128, 21), style=0,
              value=u'')

        self.gcbbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDXMLDLGADDGCBBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'gcbbOk', parent=self,
              pos=wx.Point(16, 120), size=wx.Size(76, 30), style=0)
        wx.EVT_BUTTON(self.gcbbOk, wxID_VGDXMLDLGADDGCBBOK, self.OnGcbbOkButton)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDXMLDLGADDGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel', name=u'gcbbCancel',
              parent=self, pos=wx.Point(128, 120), size=wx.Size(76, 30), style=0)
        wx.EVT_BUTTON(self.gcbbCancel, wxID_VGDXMLDLGADDGCBBCANCEL,
              self.OnGcbbCancelButton)

        self.chcLoc = wx.Choice(choices=['before', 'after', 'append child'],
              id=wxID_VGDXMLDLGADDCHCLOC, name=u'chcLoc', parent=self,
              pos=wx.Point(80, 40), size=wx.Size(128, 21), style=0)
        #self.chcLoc.SetColumns(2)
        self.chcLoc.SetSelection(1)
        self.chcLoc.SetLabel(u'')

        self.lblLoc = wx.StaticText(id=wxID_VGDXMLDLGADDLBLLOC,
              label=u'Add Location', name=u'lblLoc', parent=self, pos=wx.Point(8,
              40), size=wx.Size(63, 13), style=0)

        self.lblNodeType = wx.StaticText(id=wxID_VGDXMLDLGADDLBLNODETYPE,
              label=u'Node Type', name=u'lblNodeType', parent=self,
              pos=wx.Point(16, 72), size=wx.Size(53, 13), style=0)

        self.chcNodeType = wx.Choice(choices=['element', 'text', 'cdata',
              'comment'], id=wxID_VGDXMLDLGADDCHCNODETYPE, name=u'chcNodeType',
              parent=self, pos=wx.Point(80, 72), size=wx.Size(128, 21), style=0)
        self.chcNodeType.SetSelection(0)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.bChildAdded=False
        
        img=getOkBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.gcbbOk.SetBitmapLabel(img)
        
        img=getCancelBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.gcbbCancel.SetBitmapLabel(img)

    def OnGcbbOkButton(self, event):
        self.EndModal(1)
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
    def SetNodeName(self,name):
        #self.txtCurName.SetValue(name)
        self.txtName.SetValue(name)
        self.bChildAdded=False
    def __getParent__(self,node):
        try:
            pn=node.parentNode
            if pn is None:
                return node
            else:
                return pn
        except:
            return node
    def AddNode(self,node,doc):
        iNodeType=self.chcNodeType.GetSelection()
        iLoc=self.chcLoc.GetSelection()
        sName=self.txtName.GetValue()
        if iNodeType==0:
            # add element
            n=doc.createElement(sName)
        elif iNodeType==1:
            # add text
            n=doc.createTextNode(sName)
        elif iNodeType==2:
            # add cdata
            n=doc.createCDATASection(sName)
        elif iNodeType==3:
            # add comment
            n=doc.createComment(sName)
        parNode=self.__getParent__(node)
        if iLoc==0:
            # add before
            parNode.insertBefore(n,node)
            self.bChilAdded=False
        elif iLoc==1:
            # add after
            nextNode=node.nextSibling 
            if nextNode is None:
                parNode.appendChild(n)
            else:
                parNode.insertBefore(n,nextNode)
            self.bChilAdded=False
        elif iLoc==2:
            # append child
            node.appendChild(n) 
            self.bChildAdded=True
        #node.appendChild(n)
    def IsChildAdded(self):
        return self.bChildAdded
    def GetNodeName(self):
        return self.txtName.GetValue()
