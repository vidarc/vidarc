#----------------------------------------------------------------------------
# Name:         vtXmlNodeGuiPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120515
# CVS-ID:       $Id: vtXmlNodeGuiPanel.py,v 1.8 2015/04/27 06:44:56 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    from vGuiPanelWX import vGuiPanelWX
    from vidarc.tool.xml.vtXmlDomConsumer import *
    from vidarc.tool.net.vNetXmlWxGuiEvents import *
    from vidarc.tool.input.vtInputModifyFeedBack import *
    
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vtXmlNodeGuiPanel(vtXmlDomConsumer,vGuiPanelWX):
    VERBOSE=0
    YESNO=0x1
    OK=0x2
    OK=0x4
    EXCLAMATION=0x100
    ERROR=0x200
    INFORMATION=0x300
    QUESTION=0x400
    def __init__(self,*args,**kwargs):
        """
        applName='vtXML',iSecLvView=-1,iSecLvEdit=-1,lSecLvView=None,lWidgets=[],lEvent=[],
                    bEnableMark=True,bEnableMod=True,bAutoApply=False,bSuppressNetNotify=False):
        """
        vtXmlDomConsumer.__init__(self)
        vGuiPanelWX.__init__(self,*args,**kwargs)
        self.lWidgets=[]
        #vtInputModifyFeedBack.__init__(self,lWidgets=lWidgets,
        #            lEvent=lEvent,bEnableMark=bEnableMark,bEnableMod=bEnableMod)
        self.objRegNode=None
        self.bAutoApply=kwargs.get('bAutoApply',True)
        self.bSuppressNetNotify=kwargs.get('bSuppressNetNotify',False)
        self.netMaster=None
        self.applName=kwargs.get('applName','vtXML')
        self.lSecLvView=kwargs.get('lSecLvView',None)
        self.iSecLvView=kwargs.get('iSecLvView',-1)
        self.iSecLvEdit=kwargs.get('iSecLvEdit',-1)
        self._bNet=False
    def __initObj__(self,*args,**kwargs):
        try:
            vGuiPanelWX.__initObj__(self,*args,**kwargs)
            
            bDbg=self.GetVerboseDbg(20)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
            
        except:
            self.__logTB__()
    def __initCtrl__(self,*args,**kwargs):
        self._iVerbose=0
        if self.VERBOSE>0:
            self._iVerbose=self.VERBOSE
        if kwargs.get('VERBOSE',0)>0:
            self._iVerbose=kwargs.get('VERBOSE',0)
        
        vGuiPanelWX.__initCtrl__(self,lWid=kwargs.get('lWid',[]))
        lGrowRow=[]
        lGrowCol=[]
        return lGrowRow,lGrowCol
    def __del__(self):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        vtXmlDomConsumer.__del__(self)
        pass
    def EnableOld(self,flag):
        self.__logDebug__('flag:%d'%(flag))
    def GetApplName(self):
        return self.applName
    def SetApplName(self,applName):
        self.applName=applName
    def SetAutoApply(self,flag):
        self.bAutoApply=flag
    def SetSuppressNetNotify(self,flag):
        self.bSuppressNetNotify=flag
    def ClearDoc(self):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        self.__logDebug__(''%())
        self.netMaster=None
        vtXmlDomConsumer.ClearDoc(self)
    def ClearWid(self):
        self.__logError__('%s;replace with __Clear__'%(self.__class__.__name__))
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        self.Close()
    def __Clear__(self):
        self.__logError__('%s;overwrite me'%(self.__class__.__name__))
    def ClearInt(self):
        self.SetBlock()
        self.SetModified(False)
        try:
            self.__Close__()
            self.__Clear__()
        except:
            self.__logTB__()
            self.__logError__(self.__class__.__name__)
        self.ClrBlockDelayed()
    def Clear(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        if VERBOSE>0:
            self.__logDebug__(''%())
        if self.doc is not None:
            if self.node is not None:
                if self.bSuppressNetNotify==False:
                    self.doc.endEdit(self.node)
        self.ClearInt()
        vtXmlDomConsumer.Clear(self)
        self.SetModified(False)
    def ClearSafe(self):
        if self.doc is None:
            self.Clear()
        else:
            self.doc.DoSafe(self.doc.CallBackWX,self.Clear)
    def IsBusy(self):
        return False
    def Stop(self):
        self.Clear()
    def SetNetMaster(self,netMaster):
        self.netMaster=netMaster
    def SetNetDocs(self,d):
        pass
    def SetRegNode(self,obj):
        self.objRegNode=obj
    #def __SetDoc__(self,doc,bNet=False,dDocs=None):
    #    self.__logError__('%s;overwrite me'%(self.__class__.__name__))
    def SetDoc(self,doc,bNet=False):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        try:
            if VERBOSE>0:
                self.__logDebug__('doc;bNet:%d'%(bNet))
            self.__logDebug__('doc is None:%d;bNet:%d;self.doc is None:%d'%(doc is None,bNet,self.doc is None))
            self.Clear()
            if self.doc is not None:
                if self._bNet:          # FIXME:20150426evt
                    EVT_NET_XML_GOT_CONTENT_DISCONNECT(self.doc,self.OnGotContent)
                    EVT_NET_XML_GET_NODE_DISCONNECT(self.doc,self.OnGetNode)
                    EVT_NET_XML_REMOVE_NODE_DISCONNECT(self.doc,self.OnRemoveNode)
                    EVT_NET_XML_LOCK_DISCONNECT(self.doc,self.OnLock)
                    EVT_NET_XML_UNLOCK_DISCONNECT(self.doc,self.OnUnLock)
                    EVT_NET_XML_SET_NODE_DISCONNECT(self.doc,self.OnSetNode)
            
            self.__logDebug__('doc is None:%d;bNet:%d;self.doc is None:%d'%(doc is None,bNet,self.doc is None))
            vtXmlDomConsumer.SetDoc(self,doc)
            self._bNet=False            # FIXME:20150426evt
            self.__logDebug__('doc is None:%d;bNet:%d;self.doc is None:%d'%(doc is None,bNet,self.doc is None))
            if doc is not None:
                if bNet==True:
                    self._bNet=bNet     # FIXME:20150426evt
                    EVT_NET_XML_GOT_CONTENT(doc,self.OnGotContent)
                    EVT_NET_XML_GET_NODE(doc,self.OnGetNode)
                    EVT_NET_XML_REMOVE_NODE(doc,self.OnRemoveNode)
                    EVT_NET_XML_LOCK(doc,self.OnLock)
                    EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
                    EVT_NET_XML_SET_NODE(doc,self.OnSetNode)
                try:
                    if self.objRegNode is None:
                        self.objRegNode=self.doc.GetRegisteredNode('root')#'log'
                except:
                    pass
        except:
            self.__logError__(self.__class__.__name__)
            self.__logTB__()
        try:
            if doc is not None:
                if hasattr(self,'__SetDoc__'):
                    self.__SetDoc__(doc,bNet=bNet,dDocs=doc.GetNetDocs())
                else:
                    self.__logCritical__('%s;change to __SetDoc__'%(self.__class__.__name__))
                    #self.SetDoc(doc,bNet=bNet)
                    self.SetNetDocs(doc.GetNetDocs())
            else:
                if hasattr(self,'__SetDoc__'):
                    self.__SetDoc__(doc,bNet=bNet)
                else:
                    self.__logCritical__('%s;change to __SetDoc__'%(self.__class__.__name__))
                    #self.SetDoc(doc,bNet=bNet)
        except:
            self.__logTB__()
            self.__logError__(self.__class__.__name__)
    def OnGotContent(self,evt):
        evt.Skip()
        self.__logDebug__(''%())
    def OnGetNode(self,evt):
        evt.Skip()
        try:
            if self.IsMainThread()==False:
                self.__logCritical__('called by thread'%())
            if VERBOSE or self.VERBOSE:
                self.__logDebug__(''%())
            if self.node is None:
                return
            id=self.doc.getKey(self.node)
            if self.doc.isSameKey(self.node,evt.GetID()):
                self.SetNode(self.node)
        except:
            self.__logTB__()
    def OnAddNode(self,evt):
        evt.Skip()
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
    def OnDelNode(self,evt):
        evt.Skip()
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
    def OnRemoveNode(self,evt):
        evt.Skip()
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
    def OnSetNode(self,evt):
        evt.Skip()
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
        #id=self.doc.getKey(self.node)
        #if self.doc.isSameKey(self.node,evt.GetID()):
        #    self.SetNode(self.node)
    def OnLock(self,evt):
        evt.Skip()
        try:
            if self.IsMainThread()==False:
                self.__logCritical__('called by thread'%())
            if self.doc is None:
                self.Clear()
                self.Lock(True)
                return
            if self.node is None:
                return
            id=self.doc.getKey(self.node)
            if VERBOSE or self.VERBOSE:
                self.__logDebug__('id:%s;evtID:%s'%(id,evt.GetID()))
            if self.IsSame(evt.GetID()):
            #if self.doc.isSameKey(self.node,evt.GetID()):
                resp=evt.GetResponse()
                if self.VERBOSE:
                    if self.__isLogDebug__():
                        self.__logDebug__('resp:%s;loggedIdLv:%s;secLv:%d'%(resp,
                            self.doc.GetLoggedInSecLv(),self.iSecLvEdit))
                if resp in  ['ok','already locked']:
                    if self.doc.GetLoggedInSecLv()<self.iSecLvEdit:
                        self.Lock(True)
                    else:
                        self.Lock(False)
                else:
                    self.Lock(True)
                    self.Refresh()
        except:
            self.__logTB__()
    def OnUnLock(self,evt):
        evt.Skip()
        try:
            if self.IsMainThread()==False:
                self.__logCritical__('called by thread'%())
            if self.doc is None:
                self.Clear()
                self.Lock(True)
                return
            if self.node is None:
                return
            id=self.doc.getKey(self.node)
            if VERBOSE or self.VERBOSE:
                self.__logDebug__('id:%s;evtID:%s'%(id,evt.GetID()))
            if self.IsSame(evt.GetID()):
            #if self.doc.isSameKey(self.node,evt.GetID()):
                if self.__isLogDebug__():
                    self.__logDebug__('resp:%s'%(evt.GetResponse()))
                resp=evt.GetResponse()
                if resp in  ['released','ok']:
                    #self.Lock(False)
                    self.Lock(True)
                    self.Refresh()
                elif resp in ['granted']:
                    self.Lock(True)
                    self.Refresh()
        except:
            self.__logTB__()
    def __SetNode__(self,node,*args,**kwargs):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        self.__logError__('%s;overwrite me'%(self.__class__.__name__))
    def SetNode(self,node,*args,**kwargs):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        try:
            if VERBOSE or self.VERBOSE:
                self.__logDebug__(''%())
            iRet=self.__SetNodeCheckPerm__(node)
            if iRet<0:
                return iRet
            #self.doc.DoSafe(self.doc.CallBackWX,
            #                self.__SetNode__,node,*args,**kwargs)
            self.__SetNode__(node,*args,**kwargs)
            return iRet
        except:
            self.__logTB__()
        return -9
    def __SetNodeCheckPerm__(self,node):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        if self.doc is None:
            self.Clear()
            self.Lock(True)
            return -2
        if self.doc.IsShutDown():
            self.Clear()
            self.Lock(True)
            return -9
        sTag=self.doc.getTagName(node)
        id=self.doc.getKey(node)
        if self.node is None:# or len(id)==0:
            bMod=False
        else:
            bMod=self.GetModified()
        if self.__isLogDebug__():
            self.__logDebug__('id:%s;tag:%s;mod:%d;auto:%d'%\
                    (id,sTag,bMod,self.bAutoApply))
        if bMod:
            if self.bAutoApply:
                self.GetNode()
            else:
                # ask
                iRet=self.ShowMsgDlg(self.YESNO|self.QUESTION,
                        _(u'Do you to apply modified data?'),None)
                #dlg=vtmMsgDialog(self,_(u'Do you to apply modified data?') ,
                #            self.applName,
                #            wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                #if dlg.ShowModal()==wx.ID_YES:
                self.__logDebug__('iRet:%d'%(iRet))
                if iRet>0:
                    self.GetNode()
                else:
                    try:
                        self.SetModified(False)
                    except:
                        self.__logTB__()
        self.Check2Clear(node)
        #if self.doc.isSameKey(self.node,node):
        #    self.ClearWid()
        #else:
        #    self.Clear()
        #self.Clear()
        iSec=self.doc.GetLoggedInSecLv()
        #if iSec<self.iSecLvView:
        #    self.Show(False)
        #else:
        #    self.Show(True)
        if self.lSecLvView is not None:
            for i,wid in zip(self.lSecLvView,self.lWidgets):
                wid.Show(i>=iSec)
        self.SetBlock()
        self.ClrBlockDelayed()
        if self.doc.IsKeyValid(id):
            if self.doc.IsNodeAclOk(node,self.doc.ACL_MSK_READ):
                vtXmlDomConsumer.SetNode(self,node)
            else:
                self.__logPrintMsg__(_('id:%s tag:%s read permission denied')%(id,sTag))
                self.Lock(True)             # 070617:wro lock it
                return -1
        else:
            #vtXmlDomConsumer.SetNode(self,node)
            node=None
        #vtLog.CallStack('')
        #print self.doc,node
        if (self.doc is not None):
            if (node is not None):
                if self.doc.IsNodeAclOk(node,self.doc.ACL_MSK_WRITE):
                    if self.bSuppressNetNotify==False:
                        self.doc.startEdit(node)
                        try:
                            if self._bNet==False:
                                if self.doc.GetLoggedInSecLv()<self.iSecLvEdit:
                                    self.Lock(True)
                                else:
                                    self.Lock(False)
                        except:
                            self.__logTB__()
                else:
                    self.Lock(True)
                    self.__logPrintMsg__(_('id:%s tag:%s write permission denied')%(id,sTag))
                return 0
            else:
                if self.objRegNode is not None:
                    if self.doc.IsAclOk(self.objRegNode.GetTagName(),self.doc.ACL_MSK_ADD)==False:
                        self.Lock(True)
                    #if self.doc.IsNodeAclOk(node,self.doc.ACL_MSK_WRITE)==False:
                    #    self.Lock(True)
                    return 0
                else:
                    #self.Lock(True)
                    self.Lock(False)   # 061004 wro
                    return 0
        return -3
    def GetNodeStart(self,node=None):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
        if self.doc is None:
            self.SetModified(False)
            self.__logWarn__('doc is None')
            return None
        if self.doc.IsShutDown():
            self.SetModified(False)
            self.__logWarn__('shutdown active')
            return None
        if node is None:
            node=self.node
        if node is None:
            self.SetModified(False)
            self.__logWarn__('node is None')
            return None
        if self.doc.GetLoggedInSecLv()<self.iSecLvEdit:
            self.SetModified(False)
            self.__logInfo__('sec lv insufficient;%d<%d'%(\
                    self.doc.GetLoggedInSecLv(),self.iSecLvEdit))
            return None
        if self.doc.IsNodeAclOk(node,self.doc.ACL_MSK_WRITE):
            return node
        else:
            self.__logInfo__('no acl write')
            return None
    def GetNodeFin(self,node,idLock=None):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            if self.IsMainThread()==False:
                self.__logCritical__('called by thread'%())
            if node is None:
                return
            if self.doc.IsNodeAclOk(node,self.doc.ACL_MSK_WRITE)==False:
                #self.SetModified(False)
                self.__logInfo__('no acl write')
                self.ClearWid()
                return 
        except:
            self.__logTB__()
        try:
            self.__GetNode__(node)
        except:
            self.__logTB__()
            self.__logError__(self.__class__.__name__)
        try:
            self.doc.AlignNode(node)
            if self.bSuppressNetNotify==False:
                if self.doc.IsNodeKeyValid(node):
                    self.doc.doEdit(node,idLock=idLock)
        except:
            self.__logTB__()
        self.SetModified(False)
    def __GetNode__(self,node):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        self.__logError__('%s;overwrite me'%(self.__class__.__name__))
        #self.GetNodeFin(node)
    def GetNode(self,node=None,func=None,**kwargs):
        #if self.IsMainThread()==False:
        #    self.__logCritical__('called by thread'%())
        try:
            node=self.GetNodeStart(node)
            if VERBOSE or self.VERBOSE:
                self.__logDebug__(''%())
            #if node is None:
            #    return
            #self.__GetNode__(node)
            #self.doc.DoSafe(self.doc.CallBackWX,self.__GetNode__,node)
            idLock=self.doc.getKey(node)
            if self.IsMainThread()==False:
                self.doc.DoSafe(self.doc.CallBackWX,self.GetNodeFin,node,idLock=idLock)
            else:
                self.GetNodeFin(node,idLock=idLock)
            if func is not None:
                self.doc.DoCallBack(func,**kwargs)
            #self.GetNodeFin(node)
        except:
            self.__logTB__()
            self.__logError__(self.__class__.__name__)
    def __Close__(self):
        self.__logError__('%s;overwrite me'%(self.__class__.__name__))
    def Close(self):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        if VERBOSE>0:
            self.__logInfo__(''%())
        try:
            self.__Close__()
        except:
            self.__logTB__()
            self.__logError__(self.__class__.__name__)
    def Apply(self,bDoApply=False):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        if VERBOSE>0:
            self.__logInfo__(''%())
        self.Close()
        return False
    def __Cancel__(self):
        self.__logError__('%s;overwrite me'%(self.__class__.__name__))
    def Cancel(self):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        if VERBOSE>0:
            self.__logInfo__(''%())
        self.SetModified(False)
        self.Close()
        try:
            self.__Cancel__()
        except:
            self.__logTB__()
            self.__logError__(self.__class__.__name__)
        self.SetNode(self.node)
    def __Lock__(self,bFlag):
        self.__logError__('%s;overwrite me'%(self.__class__.__name__))
    def Lock(self,bFlag):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        if VERBOSE>0:
            self.__logDebug__('bFlag:%d'%(bFlag))
        try:
            self.__Lock__(bFlag)
        except:
            self.__logTB__()
            self.__logError__(self.__class__.__name__)
