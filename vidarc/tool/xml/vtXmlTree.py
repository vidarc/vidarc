#Boa:FramePanel:vgpXmlTree
#----------------------------------------------------------------------------
# Name:         vtXmlTree.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtXmlTree.py,v 1.92 2015/04/27 06:43:56 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#from wxPython.wx import *
import wx
import string,thread,threading,time,Queue
import vidarc.tool.xml.vtXmlDom as vtXmlDomTree
from vidarc.tool.xml.vtXmlTreeCache import vtXmlTreeCache
from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer
from vidarc.tool.net.vNetXmlWxGui import *
from vidarc.tool.net.vtNetLockRequestDialog import vtNetLockRequestDialog
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.vtThreadEndless import vtThreadEndless   # FIXME:remove
from vidarc.tool.vtThreadEndless import vtThreadEndlessWX
from vidarc.tool.gui.vtgEvent import vtgEvent

import vidarc.tool.log.vtLog as vtLog
import types,time,array
import vidarc.tool.xml.images as images

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

IMG_LIST={}
IMG_DICT={}
IMG_EXTENDED={}


STORE_EXPAND=1
#VERBOSE=0
#VERBOSE_ADD_NODE=0
#VERBOSE_ADD_NODE_RESP=0
#VERBOSE_SET_NODE=0
#VERBOSE_GET_NODE=0
#VERBOSE_DEL_NODE=0
#VERBOSE_REMOVE_NODE=0
#VERBOSE_LOCK=0
#VERBOSE_UNLOCK=0
VERBOSE_THREAD=1

# defined event for vgpXmlTree item selected
wxEVT_VTXMLTREE_ITEM_SELECTED=wx.NewEventType()
vEVT_VTXMLTREE_ITEM_SELECTED=wx.PyEventBinder(wxEVT_VTXMLTREE_ITEM_SELECTED,1)
def EVT_VTXMLTREE_ITEM_SELECTED(win,func):
    win.Connect(-1,-1,wxEVT_VTXMLTREE_ITEM_SELECTED,func)
def EVT_VTXMLTREE_ITEM_SELECTED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,wxEVT_VTXMLTREE_ITEM_SELECTED,func)
class vtXmlTreeItemSelected(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VTXMLTREE_ITEM_SELECTED(<widget_name>, self.OnItemSel)
    """

    def __init__(self,obj,tn,data):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTXMLTREE_ITEM_SELECTED)
        self.obj=obj
        self.treeNode=tn
        self.treeNodeData=data
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,obj.GetOrigin())
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def GetVgpTree(self):
        return self.obj
    def GetTreeNode(self):
        return self.treeNode
    def GetTreeNodeData(self):
        try:
            if type(self.treeNodeData)==types.DictType:
                return self.treeNodeData
            return self.obj.doc.getNodeByIdNum(self.treeNodeData)
        except:
            return None
        #return self.treeNodeData
    def GetTreeNodeIDNum(self):
        return self.treeNodeData

# defined event for vgpXmlTree item selected
wxEVT_VTXMLTREE_ITEM_EDITED=wx.NewEventType()
vEVT_VTXMLTREE_ITEM_EDITED=wx.PyEventBinder(wxEVT_VTXMLTREE_ITEM_EDITED,1)
def EVT_VTXMLTREE_ITEM_EDITED(win,func):
    win.Connect(-1,-1,wxEVT_VTXMLTREE_ITEM_EDITED,func)
class vtXmlTreeItemEdited(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VTXMLTREE_ITEM_EDITED(<widget_name>, self.OnItemEdit)
    """

    def __init__(self,obj,tn,data):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTXMLTREE_ITEM_EDITED)
        self.obj=obj
        self.treeNode=tn
        self.treeNodeData=data
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,obj.GetOrigin())
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def GetVgpTree(self):
        return self.obj
    def GetTreeNode(self):
        return self.treeNode
    def GetTreeNodeData(self):
        try:
            if type(self.treeNodeData)==types.DictType:
                return self.treeNodeData
            return self.obj.doc.getNodeByIdNum(self.treeNodeData)
        except:
            return None
        #return self.treeNodeData
    def GetTreeNodeIDNum(self):
        return self.treeNodeData


# defined event for vgpXmlTree item selected
wxEVT_VTXMLTREE_ITEM_ADDED=wx.NewEventType()
vEVT_VTXMLTREE_ITEM_ADDED=wx.PyEventBinder(wxEVT_VTXMLTREE_ITEM_ADDED,1)
def EVT_VTXMLTREE_ITEM_ADDED(win,func):
    win.Connect(-1,-1,wxEVT_VTXMLTREE_ITEM_ADDED,func)
class vtXmlTreeItemAdded(wx.PyEvent):
    """
    Posted Events:
        Tree Item added event, reports the parent node of the added one
            EVT_VTXMLTREE_ITEM_ADDED(<widget_name>, self.OnItemSel)
    """

    def __init__(self,obj,tn,data,parID):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTXMLTREE_ITEM_ADDED)
        self.obj=obj
        self.treeNode=tn
        self.treeNodeData=data
        self.parentID=parID
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,obj.GetOrigin())
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def GetVgpTree(self):
        return self.obj
    def GetTreeNode(self):
        return self.treeNode
    def GetTreeNodeData(self):
        try:
            if type(self.treeNodeData)==types.DictType:
                return self.treeNodeData
            return self.obj.doc.getNodeByIdNum(self.treeNodeData)
        except:
            return None
        #return self.treeNodeData
    def GetTreeNodeIDNum(self):
        return self.treeNodeData
    def GetParentID(self):
        return self.parentID
    
# defined event for vgpXmlTree item selected
wxEVT_VTXMLTREE_ITEM_DELETED=wx.NewEventType()
vEVT_VTXMLTREE_ITEM_DELETED=wx.PyEventBinder(wxEVT_VTXMLTREE_ITEM_DELETED,1)
def EVT_VTXMLTREE_ITEM_DELETED(win,func):
    win.Connect(-1,-1,wxEVT_VTXMLTREE_ITEM_DELETED,func)
class vtXmlTreeItemDeleted(wx.PyEvent):
    """
    Posted Events:
        Tree Item added event, reports the parent node of the added one
            EVT_VTXMLTREE_ITEM_DELTED(<widget_name>, self.OnItemDel)
    """

    def __init__(self,obj,tn,data,parID):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTXMLTREE_ITEM_DELETED)
        self.obj=obj
        self.treeNode=tn
        self.treeNodeData=data
        self.parentID=parID
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,obj.GetOrigin())
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def GetVgpTree(self):
        return self.obj
    def GetTreeNode(self):
        return self.treeNode
    def GetTreeNodeData(self):
        try:
            if type(self.treeNodeData)==types.DictType:
                return self.treeNodeData
            return self.obj.doc.getNodeByIdNum(self.treeNodeData)
        except:
            return None
        #return self.treeNodeData
    def GetTreeNodeIDNum(self):
        return self.treeNodeData
    def GetParentID(self):
        return self.parentID    

# defined event for vgpXmlTree item selected
wxEVT_VTXMLTREE_ITEM_EXEC=wx.NewEventType()
vEVT_VTXMLTREE_ITEM_EXEC=wx.PyEventBinder(wxEVT_VTXMLTREE_ITEM_EXEC,1)
def EVT_VTXMLTREE_ITEM_EXEC(win,func):
    win.Connect(-1,-1,wxEVT_VTXMLTREE_ITEM_EXEC,func)
class vtXmlTreeItemExec(wx.PyEvent):
    """
    Posted Events:
        Tree Item added event, reports the parent node of the added one
            EVT_VTXMLTREE_ITEM_Exec(<widget_name>, self.OnItemExec)
    """

    def __init__(self,obj,tn,data,parID):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTXMLTREE_ITEM_EXEC)
        self.obj=obj
        self.treeNode=tn
        self.treeNodeData=data
        self.parentID=parID
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,obj.GetOrigin())
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def GetVgpTree(self):
        return self.obj
    def GetTreeNode(self):
        return self.treeNode
    def GetTreeNodeData(self):
        try:
            if type(self.treeNodeData)==types.DictType:
                return self.treeNodeData
            return self.obj.doc.getNodeByIdNum(self.treeNodeData)
        except:
            return None
        #return self.treeNodeData
    def GetTreeNodeIDNum(self):
        return self.treeNodeData
    def GetParentID(self):
        return self.parentID    

# defined event for vgpXmlTree item selected
wxEVT_VTXMLTREE_ITEM_BUILD=wx.NewEventType()
vEVT_VTXMLTREE_ITEM_BUILD=wx.PyEventBinder(wxEVT_VTXMLTREE_ITEM_BUILD,1)
def EVT_VTXMLTREE_ITEM_BUILD(win,func):
    win.Connect(-1,-1,wxEVT_VTXMLTREE_ITEM_BUILD,func)
class vtXmlTreeItemBuild(wx.PyEvent):
    """
    Posted Events:
        Tree Item added event, reports the parent node of the added one
            EVT_VTXMLTREE_ITEM_BUILD(<widget_name>, self.OnItemBuild)
    """

    def __init__(self,obj,tn,data,parID):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTXMLTREE_ITEM_BUILD)
        self.obj=obj
        self.treeNode=tn
        self.treeNodeData=data
        self.parentID=parID
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,obj.GetOrigin())
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def GetVgpTree(self):
        return self.obj
    def GetTreeNode(self):
        return self.treeNode
    def GetTreeNodeData(self):
        try:
            if type(self.treeNodeData)==types.DictType:
                return self.treeNodeData
            return self.obj.doc.getNodeByIdNum(self.treeNodeData)
        except:
            return None
        #return self.treeNodeData
    def GetTreeNodeIDNum(self):
        return self.treeNodeData
    def GetParentID(self):
        return self.parentID    

# defined event for vgpXmlTree item selected
wxEVT_VTXMLTREE_THREAD_ADD_ELEMENTS=wx.NewEventType()
vEVT_VTXMLTREE_THREAD_ADD_ELEMENTS=wx.PyEventBinder(wxEVT_VTXMLTREE_THREAD_ADD_ELEMENTS,1)
def EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(win,func):
    win.Connect(-1,-1,wxEVT_VTXMLTREE_THREAD_ADD_ELEMENTS,func)
class vtXmlTreeThreadAddElements(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(<widget_name>, self.OnItemSel)
    """

    def __init__(self,obj,iVal,iCount=-1):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.val=iVal
        self.count=iCount
        self.SetEventType(wxEVT_VTXMLTREE_THREAD_ADD_ELEMENTS)
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,obj.GetOrigin())
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def GetValue(self):
        return self.val
    def GetCount(self):
        return self.count
# defined event for vgpXmlTree item selected
wxEVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED=wx.NewEventType()
vEVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED=wx.PyEventBinder(wxEVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED,1)
def EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED,func)
class vtXmlTreeThreadAddElementsFinished(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(<widget_name>, self.OnItemSel)
    """
    def __init__(self,obj,ti=None):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED)
        self.ti=ti
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,obj.GetOrigin())
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def GetTreeItem(self):
        return self.ti
    
# defined event for vgpXmlTree item selected
wxEVT_VTXMLTREE_THREAD_ADD_ELEMENTS_ABORTED=wx.NewEventType()
vEVT_VTXMLTREE_THREAD_ADD_ELEMENTS_ABORTED=wx.PyEventBinder(wxEVT_VTXMLTREE_THREAD_ADD_ELEMENTS_ABORTED,1)
def EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_ABORTED(win,func):
    win.Connect(-1,-1,wxEVT_VTXMLTREE_THREAD_ADD_ELEMENTS_ABORTED,func)
class vtXmlTreeThreadAddElementsAborted(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_ABORTED(<widget_name>, self.OnItemSel)
    """
    def __init__(self,obj,ti=None):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTXMLTREE_THREAD_ADD_ELEMENTS_ABORTED)
        self.ti=ti
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,obj.GetOrigin())
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def GetTreeItem(self):
        return self.ti

# defined event for vgpXmlTree item selected
wxEVT_VTXMLTREE_BUILD=wx.NewEventType()
vEVT_VTXMLTREE_BUILD=wx.PyEventBinder(wxEVT_VTXMLTREE_BUILD,1)
def EVT_VTXMLTREE_BUILD(win,func):
    win.Connect(-1,-1,wxEVT_VTXMLTREE_BUILD,func)
class vtXmlTreeBuild(wx.PyEvent):
    """
    Posted Events:
        Tree Item added event, reports the parent node of the added one
            EVT_VTXMLTREE_BUILD(<widget_name>, self.OnBuild)
    """
    def __init__(self,obj,iAct,iCount=-1):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTXMLTREE_BUILD)
        self.obj=obj
        self.iAct=iAct
        self.iCount=iCount
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,obj.GetOrigin())
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def GetVgpTree(self):
        return self.obj
    def GetAct(self):
        return self.iAct
    def GetCount(self):
        return self.iCount

class vtXmlTreeThread(vtThreadEndlessWX):
    Z_UPDATE_INTERVAL=0.25
    def __init__(self,par,bPost=False,verbose=0,origin=None):
        vtThreadEndlessWX.__init__(self,par,bPost=bPost,
                    verbose=verbose,origin=origin)
        self.tiFirst=None
        self.bNotify=True#False
        self._clearIntData()
        self._clearProcData()
    def _clearProcData(self):
        self._acquire()
        try:
            self.iAct=0
            self.iCount=1
        finally:
            self._release()
    def _clearIntData(self):
        self._acquire()
        try:
            self.idLast=-1
            self.tiLast=None
            self.zUpdate=0
        finally:
            self._release()
    def _is2Update(self):
        zNow=time.clock()
        if zNow>(self.zUpdate+self.Z_UPDATE_INTERVAL):
            self.zUpdate=zNow
            return True
        return False
    def doUpdateProc(self,iAct,bForce=False):
        self._acquire()
        try:
            if self.bNotify:
                self.iAct+=iAct
                if bForce or self._is2Update():
                    self.Post('proc',self.iAct,self.iCount)
        finally:
            self._release()
    def Do(self,*args,**kwargs):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'args:%s;kwargs:%s'%(repr(args),
                        repr(kwargs)),self.GetOrigin())
        self._acquire()
        try:
            self.iCount+=1
        finally:
            self._release()
        vtThreadEndlessWX.Do(self,*args,**kwargs)
    def _proc(self):
        vtThreadEndlessWX._proc(self)
        if self.IsEmpty():
            self._clearProcData()
        self.doUpdateProc(0,True)
    def _is2Notify(self,trWid):
        bNotify=trWid.bMaster
        if trWid.bNotifyAddThread:
            bNotify=True
        return bNotify
    def Check2Notify(self,trWid,iCount):
            bNotify=self.CallBackWX(self._is2Notify,trWid)
            if bNotify:
                self._acquire()
                try:
                    self.bNotify=bNotify
                    self.iAct+=1
                    self.iCount+=iCount
                except:
                    vtLog.vtLngTB(self.GetOrigin())
                self._release()
    def ReBuild(self):
        self.__clrSched__()
        self.DoWX(self.par.SetNode,None)
    def AddChildNodes(self,ti,id,lang,iLevel,iLevelAutoBuild,iLevelLimit,
                        func=None,*args,**kwargs):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'ti:%s;id:%s;lang:%s,lv:%d,%d,%d'%(ti,
                        id,lang,iLevel,iLevelAutoBuild,iLevelLimit),self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self._clearIntData()
        doc=self.par.doc
        trWid=self.par
        if doc is None:
            return
        try:
            node=doc.getNodeByIdNum(id)
            if node is None:
                vtLog.vtLngCur(vtLog.ERROR,'node:%08d;has been deleted'%(id),self.GetOrigin())
                return
            if iLevelLimit>0:
                if iLevel>iLevelLimit:
                    vtLog.vtLngCur(vtLog.WARN,'level:%d>%d;skipped'%(iLevel,iLevelLimit),self.GetOrigin())
                    return
            if type(ti)==types.LongType:
                tup=self.CallBackWX(trWid.__findNodeByID__,None,ti)
                if tup is None:
                    vtLog.vtLngCur(vtLog.WARN,'ti for id %d not found'%(ti),self.origin)
                    return
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'id:%d found:%d'%(ti,tup is not None),self.origin)
                ti=tup[0]
            iCount=doc.GetElementAttrCount()
            self.Check2Notify(trWid,iCount)
            #self.CallBack(trWid.Freeze)
            self.doUpdateProc(0)
            doc.procChildsKeys(node,self.__procElemAdd__,
                            trWid,doc,ti,lang,iLevel,iLevelAutoBuild,iLevelLimit)
            #self.CallBack(trWid.Expand,ti)
            #self.CallBack(trWid.Thaw)
            if self.IsEmpty():
                self._clearProcData()
            #print 'addchilds','empty',self.IsEmpty(),func
            self.doUpdateProc(0,True)
            if func is not None:
                self.CallBack(func,*args,**kwargs)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def AddNode(self,ti,id,lang,iLevel,iLevelAutoBuild,iLevelLimit,
                    func=None,*args,**kwargs):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'ti:%s;id:%s;lang:%s,lv:%d,%d,%d'%(ti,
                        id,lang,iLevel,iLevelAutoBuild,iLevelLimit),self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self._clearIntData()
        doc=self.par.doc
        trWid=self.par
        if doc is None:
            return
        try:
            node=doc.getNodeByIdNum(id)
            if node is None:
                vtLog.vtLngCur(vtLog.ERROR,'node:%08d;has been deleted'%(id),self.GetOrigin())
                return
            if iLevelLimit>0:
                if iLevel>iLevelLimit:
                    vtLog.vtLngCur(vtLog.WARN,'level:%d>%d;skipped'%(iLevel,iLevelLimit),self.GetOrigin())
                    return
            if type(ti)==types.LongType:
                tup=self.CallBackWX(trWid.__findNodeByID__,None,ti)
                if tup is None:
                    vtLog.vtLngCur(vtLog.WARN,'ti for id %d not found'%(ti),self.origin)
                    return
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'id:%d found:%d'%(ti,tup is not None),self.origin)
                ti=tup[0]
            iCount=doc.GetElementAttrCount()
            self.Check2Notify(trWid,iCount)
            self.CallBack(trWid.Freeze)
            self.doUpdateProc(0)
            self.__procElemAdd__(node,trWid,doc,ti,lang,
                            iLevel,iLevelAutoBuild,iLevelLimit)
            #self.CallBack(trWid.EnsureVisible,ti)
            #self.CallBack(trWid.Expand,ti)
            self.CallBack(trWid.Thaw)
            if self.IsEmpty():
                self._clearProcData()
            self.doUpdateProc(0,True)
            tup=trWid.__findNodeByID__(None,ti)
            if tup is not None:
                #return tup[0]
                if self.CallBackWX(trWid.IsExpanded,tup[0]):
                    self.CallBack(trWid.__Sort__,id,True)
            if func is not None:
                self.CallBack(func,*args,**kwargs)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def SetNode(self,id,lang,iLevelAutoBuild,iLevelLimit,
                    func=None,*args,**kwargs):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'id:%s;lang:%s,lv:0,%d,%d'%(id,
                        lang,iLevelAutoBuild,iLevelLimit),self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self._clearIntData()
        doc=self.par.doc
        trWid=self.par
        if doc is None:
            return
        try:
            iCount=doc.GetElementAttrCount()
            self.Check2Notify(trWid,iCount)
            self.CallBackWX(trWid.__Clear__)
            if id==-2:
                node=trWid.GetBaseNode()
            else:
                node=doc.getNodeByIdNum(id)
            if node is None:
                return
            self.CallBack(trWid.Freeze)
            self.doUpdateProc(0)
            ti=self.CallBackWX(trWid.__createRoot__,node)
            doc.procChildsKeys(node,self.__procElemAdd__,
                            trWid,doc,ti,lang,0,iLevelAutoBuild,iLevelLimit)
            self.CallBack(trWid.Expand,ti)
            self.CallBack(trWid.Thaw)
            self.doUpdateProc(0,True)
            if self.Is2Stop():
                self.Post('aborted')
            else:
                self.Post('finished')
                if self.IsEmpty():
                    self._clearProcData()
            if func is not None:
                self.CallBack(func,*args,**kwargs)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def __chkElem__(self,node,trWid,doc):
        if doc.IsSkip(node):
            return 0
        if trWid.__is2block__(node):
            return -2
        if trWid.__is2skip__(node,doc.getTagName(node)):
            return 1
        self.bHasChilds2Add=True
        return -1
    def __procElemAdd__(self,node,trWid,doc,ti,lang,iLevelAct,iLevelAutoBuild,iLevelLimit):
        try:
            bDbg=False
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                if VERBOSE>10:
                    vtLog.vtLngCur(vtLog.DEBUG,'node:%s'%(repr(node)),self.GetOrigin())
                    bDbg=True
            if ti is None:
                vtLog.vtLngCur(vtLog.CRITICAL,'ti is None',self.GetOrigin())
                return -1
            #if self.par.TREE_BUILD_ON_DEMAND:
            #    bWalk=False
            #else:
            #    bWalk=True
            self.doUpdateProc(1)
            
            bWalk=False
            if iLevelAutoBuild>=0:
                if iLevelAct<iLevelAutoBuild:
                    bWalk=True
            else:
                bWalk=True
            if self.Is2Stop():
                vtLog.vtLngCur(vtLog.INFO,'Is2Stop',self.origin)
                return -1
            if iLevelLimit>0:
                if iLevelAct>iLevelLimit:
                    return -1
            bAdd=True
            ret=self.__chkElem__(node,trWid,doc)
            if bDbg:
                vtLog.vtLngCur(vtLog.DEBUG,'chk:%d'%(ret),self.GetOrigin())
            if ret==0:      # skip by doc, exit now!
                return 0
            elif ret==-2:   # blocked by tree
                return 0
            elif ret==1:    # skip by tree, walk down
                bWalk=True
                bAdd=False
                iLevelAct-=1
            else:
                bAdd=True
            if 0:
                if doc.IsSkip(node):
                    bAdd=False
                    #return 0        # 061203:wro
                if trWid.__is2block__(node):
                    return 0
                if trWid.__is2skip__(node,doc.getTagName(node)):
                    bWalk=True
                    bAdd=False
            id=doc.getKeyNum(node)
            tn=ti
            if bAdd:
                if VERBOSE>0:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'id:%s add'%(id),self.origin)
                doc.acquire()
                try:
                    tn=self.__addElement__(trWid,ti,doc,node,lang)
                    if self.tiFirst is None:
                        self.tiFirst=tn
                    self.tiLast=tn
                    self.idLast=id
                except:
                    vtLog.vtLngTB(self.GetOrigin())
                    doc.release()
                    return 0
                doc.release()
            if bWalk:
                doc.procChildsKeys(node,self.__procElemAdd__,
                            trWid,doc,tn,lang,iLevelAct+1,
                            iLevelAutoBuild,iLevelLimit)
            else:
                self.bHasChilds2Add=False
                doc.procChildsKeys(node,self.__chkElem__,
                            trWid,doc)
                if self.bHasChilds2Add:
                    self.CallBack(self._doAddDummyChild,trWid,tn)
                
        except:
            vtLog.vtLngTB(self.GetOrigin())
            return -1
        return 0
    def __addElement__(self,trWid,ti,doc,node,lang):
        tagName,infos=doc.getNodeInfos(node,lang)
        tn=self.CallBackWX(trWid.__addElementByInfos__,
                                ti,node,tagName,infos,True)
        return tn
    def Refresh(self,id,bChild,lang,
                    func=None,*args,**kwargs):
        try:
            if VERBOSE>0:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'id:%s;lang:%s,bChild:%d'%(
                            id,lang,bChild),self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
        doc=self.par.doc
        trWid=self.par
        if doc is None:
            return
        try:
            if id is None:
                node=self.CallBackWX(trWid.GetRootNode)
            else:
                node=doc.getNodeByIdNum(id)
            if node is None:
                vtLog.vtLngCur(vtLog.INFO,'id:%s;node deleted'%(id),self.GetOrigin())
                return
            if bChild:
                iCount=doc.GetElementAttrCount()
            else:
                iCount=1
            self.Check2Notify(trWid,iCount)
            self.CallBack(trWid.Freeze)
            self.doUpdateProc(0)
            try:
                doc.acquire()
                ti=self.__refreshElement__(trWid,doc,id,node,lang)
            except:
                vtLog.vtLngTB(self.origin)
            doc.release()
            if bChild:
                doc.procChildsKeys(node,self.__procElemRefresh__,
                            trWid,doc,lang)
                self.CallBack(trWid.__Sort__,id,True)
            else:
                self.CallBack(trWid.__Sort__,id,True)
            self.CallBack(trWid.Thaw)
            self.doUpdateProc(0,True)
            if self.Is2Stop():
                self.Post('aborted')
            else:
                self.Post('finished')
            if func is not None:
                self.CallBack(func,*args,**kwargs)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def __refreshElement__(self,trWid,doc,id,node,lang):
        tagName,infos=doc.getNodeInfos(node,lang)
        ti=self.CallBackWX(trWid.__refreshLabelByInfos__,
                                    id,node,tagName,infos)
        self.bHasChilds2Add=False
        doc.procChildsKeys(node,self.__chkElem__,
                            trWid,doc)
        if self.bHasChilds2Add:
            self.CallBack(self._doAddDummyChild,trWid,ti)
        return ti
    def __procElemRefresh__(self,node,trWid,doc,lang):
        try:
            self.doUpdateProc(1)
            bAdd=True
            bWalk=True
            ret=self.__chkElem__(node,trWid,doc)
            if ret==0:      # skip by doc, exit now!
                return 0
            elif ret==-2:   # blocked by tree
                return 0
            elif ret==1:    # skip by tree, walk down
                bWalk=True
                bAdd=False
            else:
                bAdd=True
            id=doc.getKey(node)
            if bAdd:
                try:
                    doc.acquire()
                    ti=self.__refreshElement__(trWid,doc,id,node,lang)
                except:
                    vtLog.vtLngTB(self.origin)
                    ti=None
                doc.release()
                if ti is None:
                    return -1
            if bWalk:
                doc.procChildsKeys(node,self.__procElemRefresh__,
                            trWid,doc,lang)
                return 0
            else:
                self.bHasChilds2Add=False
                doc.procChildsKeys(node,self.__chkElem__,
                            trWid,doc)
                if self.bHasChilds2Add:
                    self.CallBack(self._doAddDummyChild,trWid,ti)
        except:
            vtLog.vtLngTB(self.GetOrigin())
            return -1
        return 0
    def _doExpand(self,tn):
        try:
            self.par.Expand(tn)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def _doAddDummyChild(self,trWid,tn):
        try:
            if tn is not None:
                trWid.SetItemHasChildren(tn,True)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def SetLevelLimit(self,level,auto):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'lv:%d;auto:%d'%(level,
                        auto),self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
        doc=self.par.doc
        trWid=self.par
        iAutoOld=trWid.iLevelAutoBuild
        iLimitOld=trWid.iLevelLimit
        
        trWid.iLevelAutoBuild=auto
        trWid.iLevelLimit=level

    def ChangeLevels(self):
        pass

class vtXmlTreeEvent(vtgEvent):
    _MAP_EVENT={
            'selected':EVT_VTXMLTREE_ITEM_SELECTED,
            'edited':EVT_VTXMLTREE_ITEM_EDITED,
            'added':EVT_VTXMLTREE_ITEM_ADDED,
            'deleted':EVT_VTXMLTREE_ITEM_DELETED,
            'exec':EVT_VTXMLTREE_ITEM_EXEC,
            'build':EVT_VTXMLTREE_ITEM_BUILD,
            'thdBuild':EVT_VTXMLTREE_ITEM_BUILD,
            'thdAddElem':EVT_VTXMLTREE_THREAD_ADD_ELEMENTS,
            'thdAddElemFin':EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED,
            'thdAddElemAbort':EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_ABORTED,
            }

class vtXmlTree(wx.TreeCtrl,vtXmlDomConsumer,vtXmlTreeEvent,vtLog.vtLogOriginWX):
    TREE_BUILD_ON_DEMAND=1
    TREE_DATA_ID=1
    MAP_IMG_ALPHA=[1,1,1.5,1.5,2.0,2.0]
    def __init__(self, parent=None, id=None, pos=None, size=None, style=0, name='trXml',
                    master=False,controller=False,verbose=0):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        vtXmlDomConsumer.__init__(self)
        #print style
        if id is None:
            id=wx.NewId()
        if pos is None:
            pos=wx.DefaultPosition
        if size is None:
            size=wx.DefaultSize
        wx.TreeCtrl.__init__(self,id=id, name=name,
              parent=parent, pos=pos, size=size,style=style|wx.TR_HAS_BUTTONS)
        self.SetAutoLayout(True)
        wx.EVT_TREE_SEL_CHANGED(self, id,
              self.OnTcNodeTreeSelChanged)
        wx.EVT_TREE_ITEM_COLLAPSING(self,id,self.OnTcNodeCollapsing)
        #wx.EVT_TREE_SEL_CHANGING(self, id,
        #        self.OnTcNodeTreeSelChanging)
        #EVT_RIGHT_UP(self, self.OnTcNodeRightUp)
        wx.EVT_RIGHT_DOWN(self, self.OnTcNodeRightDown)
        if STORE_EXPAND==1:
            wx.EVT_TREE_ITEM_COLLAPSED(self,id,self.OnTcNodeCollapes)
            wx.EVT_TREE_ITEM_EXPANDED(self,id,self.OnTcNodeExpanded)
            wx.EVT_TREE_ITEM_EXPANDING(self,id,self.OnTcNodeExpanding)
        ##self._init_ctrls(parent, id, pos, size, style, name)
        
        self.__init_properties__()
        self.verbose=verbose
        self.bMaster=master
        self.bNotifyAddThread=False
        self.bController=controller
        self.bNetResponse=True
        self.FORCE_CHILDREN=True
        vtLog.vtLogOriginWX.__init__(self,bExtended=True)
        #self.lstSkip=[]
        #self.lstBlk=[]
        
        #self.thdAddElements=vtXmlTreeAddThread(self,bPost=True,verbose=verbose-1)
        #self.thdProc=vtXmlTreeThread(self,bPost=False)
        self.thdProc=vtXmlTreeThread(self,bPost=True)
        
        #EVT_THREAD_ADD_ELEMENTS_FINISHED(self,self.OnAddElementsFin)
        #EVT_THREAD_ADD_ELEMENTS(self,self.OnAddElementsProgress)
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self,self.OnAddFinished)
        
        if self.bNetResponse:
            self.EnableCache()
        self.SetupImageList()
    def _init_ctrls(self):
        wx.TreeCtrl.__init__(self,id=id, name=name,
              parent=parent, pos=pos, size=size,style=wx.TR_HAS_BUTTONS)
        self.SetAutoLayout(True)
        wx.EVT_TREE_SEL_CHANGED(self, id,
              self.OnTcNodeTreeSelChanged)
        #wx.EVT_TREE_SEL_CHANGING(self, id,
        #        self.OnTcNodeTreeSelChanging)
        #EVT_RIGHT_UP(self, self.OnTcNodeRightUp)
        wx.EVT_RIGHT_DOWN(self, self.OnTcNodeRightDown)
        if STORE_EXPAND==1:
            wx.EVT_TREE_ITEM_COLLAPSED(self,id,self.OnTcNodeCollapes)
            wx.EVT_TREE_ITEM_EXPANDED(self,id,self.OnTcNodeExpanded)
            wx.EVT_TREE_ITEM_EXPANDING(self,id,self.OnTcNodeExpanding)
        else:
            wx.EVT_TREE_ITEM_EXPANDING(self,id,self.OnTcNodeExpandingNoStore)
    def __init_properties__(self):
        vtLog.vtLogOriginWX.__init__(self)
        self.lang=None
        self.iLevelLimit=-1
        self.iLevelAutoBuild=0
        self.tiCache=None
        self.doc=None
        self.label=[]
        self.nodeInfos=[]
        self.lTagNames2Base=None
        self.idRootNode=-1
        self.rootNode=None
        self.objTreeItemSel=None
        self.triSelected=None
        self.tiRoot=None
        self.dlgAdd=None
        self.dlgRename=None
        self.dlgLockReq=None
        self.idManualAdd=''
        self.idManualMove=''
        self.widLogging=vtLog.GetPrintMsgWid(self)
        self.bStoreExpand=False
        #self.sExpandName='__expand'
        self.sExpandName='expand'
        self.bAutoSelectLast=False
        self.bSelectLast=False
        
        self.verbose=False
        self.bMaster=False
        self.bNotifyAddThread=False
        self.bController=False
        self.bNetResponse=True
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        try:
            #if self.doc is not None:
            #    self.doc.DelConsumer(self)
            if self.tiCache is not None:
                del self.tiCache
            del self.thdProc
        except:
            #vtLog.vtLngTB(self.__class__.__name__)
            pass
        self.objTreeItemSel=None
        self.triSelected=None
        self.rootNode=None
        #self.DeleteAllItems()
        self.tiRoot=None
    def BindEvents(self,funcProc=None,funcAborted=None,funcFinished=None):
        self.thdProc.BindEvents(funcProc=funcProc,
                funcAborted=funcAborted,funcFinished=funcFinished)
    def BindEvent_old(self,name,func,par=None):
        try:
            if name in self.__MAP_EVENT:
                if par is None:
                    self.__MAP_EVENT[name](self,func)
                else:
                    self.__MAP_EVENT[name](par,func)
                return 0
            else:
                vtLog.vtLngCurWX(vtLog.CRITICAL,'evt name:%s not resolved'%(name),self)
        except:
            vtLog.vtLngTB(self.GetName())
        return -1
    #def Destroy(self):
    #    vtLog.vtLngCur(vtLog.DEBUG,''%(),self.GetOrigin())
    #    print 'Destroy'
    #    return wx.TreeCtrl.Destroy(self)
    #def Close(self,bForce=False):
    #    vtLog.vtLngCur(vtLog.DEBUG,''%(),self.GetOrigin())
    #    print 'close'
    #    self.thdAddElements.SetPostWid(None)
    #    wx.TreeCtrl.Close(self,bForce)
    def CallBackWX(self,func,*args,**kwargs):
        self.thdProc.CallBackWX(func,*args,**kwargs)
    def DoWX(self,func,*args,**kwargs):
        self.thdProc.DoWX(func,*args,**kwargs)
    def SetNodeInfos(self,lst,key='|id'):
        """['project','task','subtask','loc','person',
                        'desc','date','starttime','endtime']
                        """
        self.nodeInfos=lst
        self.nodeKey=key
        try:
            i=self.nodeInfos.index(self.nodeKey)
        except:
            self.nodeInfos.append(self.nodeKey)
    def SetLabeling(self,lblLst):
        """[('date','YYYY'),('date','MM'),('date','DD'),('person','')]
        [('date','YYYY-MM-DD'),('starttime','HH:MM'),('project',''),('person','')]
        or
        {'user':[('date','YYYY'),('date','MM'),('date','DD'),('person','')]}
        {'user':[('date','YYYY-MM-DD'),('starttime','HH:MM'),('project',''),('person','')]}
        """
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'lbl:%s'%(vtLog.pformat(lblLst)),
                        self)
        except:
            vtLog.vtLngTB('')
        self.label=lblLst
    def SetLanguage(self,lang):
        if lang!=self.lang:
            self.lang=lang
            #self.SetNode(self.rootNode)
            self.RefreshAllLabels()
    def DoDelayed(self,func,*args,**kwargs):
        self.thdProc.Do(func,*args,**kwargs)
    def _doSafe(self,func,*args,**kwargs):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,''%(),self.GetOrigin())
        if self.doc is None:
            return
        self.doc.acquire('dom')
        try:
            func(*args,**kwargs)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.doc.release('dom')
    def DoSafe(self,func,*args,**kwargs):
        self.thdProc.Do(self._doSafe,func,*args,**kwargs)
    def CallBack(self,func,*args,**kwargs):
        self.thdProc.CallBack(func,*args,**kwargs)
    def CallBackDelayed(self,zTm,func,*args,**kwargs):
        self.thdProc.CallBack(wx.FutureCall,zTm,
                        self.thdProc.CallBack,func,*args,**kwargs)
    def DoCallBack(self,func,*args,**kwargs):
        self.thdProc.Do(self.thdProc.CallBack,func,*args,**kwargs)
    DoCB=DoCallBack
    def DoCallBackDelayed(self,zTm,func,*args,**kwargs):
        self.thdProc.Do(self.thdProc.CallBack,wx.FutureCall,zTm,
                        self.thdProc.CallBack,func,*args,**kwargs)
    def HasId(self,id):
        if long(id)==-1:
            return self.rootNode != None
        if self.tiCache is not None:
            return self.tiCache.HasId(id)
        return -1
    def SetBuildOnDemand(self,flag):
        #vtLog.CallStack('')
        #print flag
        if self.tiCache is not None:
            self.TREE_BUILD_ON_DEMAND=flag
        else:
            self.TREE_BUILD_ON_DEMAND=False # FIXME 060912 wro
        return
        self.TREE_BUILD_ON_DEMAND=flag
    def EnableSelectLast(self,flag=True):
        self.bAutoSelectLast=flag
        self.bSelectLast=flag
        
    def SetNotifyAddThread(self,flag=True):
        self.bNotifyAddThread=flag

    def __is2add__(self,o,tagName):
        # check node to skip
        if hasattr(self,'lstValid'):
            for it in self.lstValid:
                if type(it)==types.ListType:
                    if (it[-1]==tagName):
                        # check previous
                        tmp=o
                        r=range(len(it)-1)
                        r.reverse()
                        for i in r:
                            tmp=self.doc.getParent(tmp)
                            if tmp is None:
                                return False
                            if it[i]!=self.doc.getTagName(tmp):
                                return False
                        if bFound==True:
                            return True
                else:
                    if it!=tagName:
                        # forget it
                        return False
        return True
    def SetValidInfo(self,lst):
        """['TagName1',['TagName2','TagName3']]"""
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'lstValid:%s'%(vtLog.pformat(lst)),self)
        self.lstValid=lst

    def __is2skip__(self,o,tagName):
        # check node to skip
        if hasattr(self,'lstSkip'):
            for it in self.lstSkip:
                if type(it)==types.ListType:
                    if (it[-1]==tagName):
                        # check previous
                        tmp=o
                        r=range(len(it)-1)
                        r.reverse()
                        bFound=True
                        for i in r:
                            tmp=self.doc.getParent(tmp)
                            if tmp is None:
                                bFound=False
                                break
                            if it[i]!=self.doc.getTagName(tmp):
                                bFound=False
                                break
                        if bFound==True:
                            return True
                else:
                    if it==tagName:
                        # forget it
                        return True
        return False
    def SetSkipInfo(self,lst):
        """['TagName1',['TagName2','TagName3']]"""
        self.lstSkip=lst
    
    def __is2block__(self,o):
        # check node to skip
        if hasattr(self,'lstBlk'):
            tagName=self.doc.getTagName(o)
            for it in self.lstBlk:
                if type(it)==types.ListType:
                    if (it[-1]==tagName):
                        # check previous
                        tmp=o
                        r=range(len(it)-1)
                        r.reverse()
                        bFound=True
                        for i in r:
                            tmp=self.doc.getParent(tmp)
                            if tmp is None:
                                bFound=False
                                break
                            if it[i]!=self.doc.getTagName(tmp):
                                bFound=False
                                break
                        if bFound==True:
                            return True
                else:
                    if it==tagName:
                        # forget it
                        return True
        return False
    def SetBlockInfo(self,lst):
        """['TagName1',['TagName2','TagName3']]"""
        self.lstBlk=lst
    def IsScheduled(self):
        return False
        return self.thdAddElements.IsScheduled()
    def IsBusy(self):
        if self.thdProc.IsBusy():
            return True
        return False
    def IsRunning(self):
        if self.thdProc.IsRunning():
            return True
        return False
    def IsThreadRunning(self):
        bRunning=False
        if self.thdProc.IsRunning():
            bRunning=True
        return bRunning
    def Resume(self):
        self.thdProc.Resume()
    def Pause(self):
        self.thdProc.Pause()
    def Restart(self):
        self.thdProc.Restart()
    def Stop(self):
        if self.thdProc.IsRunning():
            self.thdProc.Stop()
        #while self.IsThreadRunning():
        #    time.sleep(1.0)
    def EnableCache(self):
        self.tiCache=vtXmlTreeCache(self.doc)
    def SetNetResponse(self,flag):
        self.bNetResponse=flag
    def IsNetResponse(self):
        return self.bNetResponse
    def AddCache(self,ti):
        if self.tiCache is not None:
            self.tiCache.Add(ti)
    def __getTreeLevels__(self,ti):
        try:
            if ti.IsOk():
                tiPar=self.GetItemParent(ti)
                if tiPar is not None:
                    return self.__getTreeLevels__(tiPar)+1
            return -1
        except:
            vtLog.vtLngTB(self.GetOrigin())
        return -1
    def __deleteTreeLevels__(self,ti,level,limit):
        triChild=self.GetFirstChild(ti)
        while triChild[0].IsOk():
            if level>=limit:
                self.DeleteChildren(triChild[0])
            else:
                self.__deleteTreeLevels__(triChild[0],level+1,limit)
            triChild=self.GetNextChild(ti,triChild[1])
    def __addTreeLevels__(self,ti,level,limit):
        vtLog.vtLngCurWX(vtLog.CRITICAL,'FIXME',self)
        if ti is None:
            return
        triChild=self.GetFirstChild(ti)
        while triChild[0].IsOk():
            if self.GetChildrenCount(triChild[0])==0:
                o=self.GetPyData(triChild[0])
                if o is not None:
                    if len(self.doc.getKey(o))>0:
                        self.__addElements__(triChild[0],o)
            else:
                if level>=limit:
                    o=self.GetPyData(triChild[0])
                    if o is not None:
                        if len(self.doc.getKey(o))>0:
                            self.__addElements__(triChild[0],o)
                else:
                    self.__addTreeLevels__(triChild[0],level+1,limit)
            triChild=self.GetNextChild(ti,triChild[1])
    def SetLevelLimit(self,level,auto):
        self.thdProc.Do(self.thdProc.SetLevelLimit,level,auto)
        return
    def __SetLevelLimit__(self,level,auto):
        self.iLevelAutoBuild=auto
        self.iLevelLimit=level
        return
        vtLog.vtLngCurWX(vtLog.CRITICAL,'FIXME',self)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,
                'level:(%d,%d) oldlevel:(%d,%d)'%(level,auto,self.iLevelLimit,self.iLevelAutoBuild),
                self)
        self.iLevelAutoBuild=auto
        if self.iLevelLimit!=level:
            self.iLevelLimit=level
            #self.thdAddElements.ChangeLevels(self.tiRoot,self.rootNode)
        return
        
        
        if self.IsBusy():
            self.iLevelLimit=level
            self.iLevelAutoBuild=auto
            return
        while self.IsBusy():
            time.sleep(0.1)
        if level>0:
            if level<self.iLevelLimit:
                self.__deleteTreeLevels__(self.tiRoot,0,level)
            elif level>self.iLevelLimit:
                self.iLevelLimit=level
                self.__addTreeLevels__(self.tiRoot,0,level)
        self.iLevelLimit=level
        self.iLevelAutoBuild=auto
    def SetStoreExpand(self,state):
        self.bStoreExpand=state
    def SetExpandName(self,name):
        self.sExpandName=name
    def UpdateLang(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'UpdateLang;parent:%s master:%d'%(self.GetParent().GetParent().GetName(),self.bMaster),
                        self)
        self.RefreshAllLabels()
    def ClearDoc(self):
        vtLog.vtLngCur(vtLog.DEBUG,''%(),self.GetOrigin())
        self.thdProc.SetPostWid(None)
        vtXmlDomConsumer.ClearDoc(self)
    def SetDoc(self,doc,bNet=False):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'net:%d'%bNet,self)
        #if doc is None:
        #    return
        try:
            if self.doc is not None:
                self.doc.DelConsumer(self)
                self.doc.DelConsumerLang(self)
            if bNet==True:
                # FIXME:20150426evt
                if self.doc is not None:
                    EVT_NET_XML_OPEN_START_DISCONNECT(self.doc,self.OnOpenStart)
                    EVT_NET_XML_SYNCH_FINISHED_DISCONNECT(self.doc,self.OnSynchFin)
                    EVT_NET_XML_LOGGEDIN_DISCONNECT(self.doc,self.OnLoggedin)
                    EVT_NET_XML_GOT_CONTENT_DISCONNECT(self.doc,self.OnGotContent)
                    EVT_NET_XML_GET_NODE_DISCONNECT(self.doc,self.OnGetNode)
                    EVT_NET_XML_SET_NODE_DISCONNECT(self.doc,self.OnSetNode)
                    EVT_NET_XML_ADD_NODE_DISCONNECT(self.doc,self.OnAddNode)
                    EVT_NET_XML_ADD_NODE_RESPONSE_DISCONNECT(self.doc,self.OnAddNodeResp)
                    EVT_NET_XML_MOVE_NODE_DISCONNECT(self.doc,self.OnMoveNode)
                    EVT_NET_XML_MOVE_NODE_RESPONSE_DISCONNECT(self.doc,self.OnMoveNodeResp)
                    EVT_NET_XML_DEL_NODE_DISCONNECT(self.doc,self.OnDelNode)
                    EVT_NET_XML_REMOVE_NODE_DISCONNECT(self.doc,self.OnRemoveNode)
                    EVT_NET_XML_LOCK_DISCONNECT(self.doc,self.OnLock)
                    EVT_NET_XML_UNLOCK_DISCONNECT(self.doc,self.OnUnLock)
                    EVT_NET_XML_LOCK_REQUEST_DISCONNECT(self.doc,self.OnLockRequest)
                    if self.bMaster:
                        EVT_NET_XML_SELECT_DISCONNECT(self.doc,self.OnSelect)
                if doc is not None:
                    EVT_NET_XML_OPEN_START(doc,self.OnOpenStart)
                    EVT_NET_XML_SYNCH_FINISHED(doc,self.OnSynchFin)
                    EVT_NET_XML_LOGGEDIN(doc,self.OnLoggedin)
                    EVT_NET_XML_GOT_CONTENT(doc,self.OnGotContent)
                    EVT_NET_XML_GET_NODE(doc,self.OnGetNode)
                    EVT_NET_XML_SET_NODE(doc,self.OnSetNode)
                    EVT_NET_XML_ADD_NODE(doc,self.OnAddNode)
                    EVT_NET_XML_ADD_NODE_RESPONSE(doc,self.OnAddNodeResp)
                    EVT_NET_XML_MOVE_NODE(doc,self.OnMoveNode)
                    EVT_NET_XML_MOVE_NODE_RESPONSE(doc,self.OnMoveNodeResp)
                    EVT_NET_XML_DEL_NODE(doc,self.OnDelNode)
                    EVT_NET_XML_REMOVE_NODE(doc,self.OnRemoveNode)
                    EVT_NET_XML_LOCK(doc,self.OnLock)
                    EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
                    EVT_NET_XML_LOCK_REQUEST(doc,self.OnLockRequest)
                    if self.bMaster:
                        EVT_NET_XML_SELECT(doc,self.OnSelect)
            
            self.doc=doc
            if doc is None:
                return
            self.doc.AddConsumer(self,self.Clear)
            self.doc.AddConsumerLang(self,self.UpdateLang)
            if self.bMaster or self.bController:
                self.doc.setNodeInfos2Get(self.nodeInfos,name=self.GetName())
            if self.tiCache is not None:
                self.tiCache.SetDoc(doc)
                self.tiCache.Clear()
                self.tiCache.Build(self)
            self.__extendImageListRegNodes__()
        except:
            vtLog.vtLngTB(self.GetName())
    def SetIdManual(self,par,node,**kwargs):
        if node is not None:
            self.idManualAdd=self.doc.getKey(node)
        else:
            self.idManualAdd=''
    def OnOpenStart(self,evt):
        evt.Skip()
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            self.DoCB(self.Clear)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSynchFin(self,evt):
        evt.Skip()
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            if self.tiRoot is not None:
                self.DoCB(self.Expand,self.tiRoot)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnLoggedin(self,evt):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        #self.Clear()
        evt.Skip()
    def OnGotContent(self,evt):
        evt.Skip()
        try:
            if VERBOSE>0:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'parent:%s master:%d'%(self.GetParent().GetParent().GetName(),self.bMaster),
                                self)
            if self.bNetResponse==False:
                return
            self.SetNode(self.doc.getBaseNode())
        except:
            vtLog.vtLngTB(self.GetName())
    def OnGetNode(self,evt):
        evt.Skip()
        try:
            if VERBOSE>0:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'parent:%s master:%d'%(self.GetParent().GetParent().GetName(),self.bMaster),
                                self)
            if self.bNetResponse==False:
                return
            id=evt.GetID()
            tup=self.__findNodeByID__(None,id)
            if tup is not None:
                self.__refreshLabel__(tup[0])
                #ti=self.GetItemParent(tup[0])
                #self.SortChildren(ti)
            if self.verbose:
                vtLog.CallStack('')
                vtLog.vtSpace()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSetNode(self,evt):
        evt.Skip()
        try:
            if VERBOSE>0:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'parent:%s master:%d'%(self.GetParent().GetParent().GetName(),self.bMaster),
                                self)
            if self.bNetResponse==False:
                return
            idOld=evt.GetID()
            idNew=evt.GetNewID()
            tup=self.__findNodeByID__(None,idOld)
            #if vtLog.vtLngIsLogged(vtLog.DEBUG):
            #    vtLog.vtLngCallStack(None,vtLog.DEBUG,'id:%s %d'%(id,tup is not None),verbose=self.verbose,
            #                origin=self.GetName())
            if tup is not None:
                iIdSel=self.GetSelectedIDNum()
                iIdOld=long(idOld)
                if iIdOld!=long(idNew):
                    self.DoCB(self.Delete,tup[0])
                    if self.tiCache is not None:
                        self.tiCache.DelID(id)
                    
                    node=self.doc.getNodeById(idNew)
                    nodePar=self.doc.getParent(node)
                    if nodePar is None:
                        vtLog.vtLngCur(vtLog.CRITICAL,'no parent node found'%(),self.GetOrigin())
                    else:
                        idPar=self.doc.getKeyNum(nodePar)
                        self.__addElements__(idPar,long(idNew))
                        if iIdSel==iIdOld:
                            self.DoCB(self.SelectByID,idNew)
                else:
                    self.__refreshLabel__(tup[0])
                    if iIdSel==iIdOld:
                        self.DoCB(self.SelectByID,idOld)
                #if self.GetRootItem()!=tup[0]:
                #    ti=self.GetItemParent(tup[0])
                #    self.SortChildren(ti)
            if self.verbose:
                vtLog.CallStack('')
                vtLog.vtSpace()
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnAddNode(self,evt):
        evt.Skip()
        try:
            if VERBOSE>0:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'parent:%s master:%d'%(self.GetParent().GetParent().GetName(),self.bMaster),
                                self)
                #vtLog.vtCallStack(3,VERBOSE_ADD_NODE)
            if self.bNetResponse==False:
                return
            idPar=evt.GetID()
            idNew=evt.GetNewID()
            child=self.doc.getNodeById(idNew)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'id:%s %d'%\
                        (idNew,child is not None),self)
            if child is None:
                return
            if self.doc.IsSkip(child):
                return
            self.__addElements__(long(idPar),long(idNew))
            return
            tup=self.__findNodeByID__(None,idPar)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'idPar:%s %d'%\
                        (idPar,tup is not None),self)
            if (tup is not None):
                self.__addElements__(long(idPar),long(idNew))
                #self.thdAddElements.Do(tup[0],long(idNew),False)
                #self.SortChildren(tup[0])
            else:
                #if TREE_BUILD_ON_DEMAND:
                #self.__addElements__(long(idPar),child)
                
                #self.thdAddElements.Do(long(idPar),long(idNew))
                pass
            if self.verbose:
                vtLog.CallStack('')
                vtLog.vtSpace()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnAddNodeResp(self,evt):
        evt.Skip()
        try:
            if VERBOSE>0:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'parent:%s master:%d'%(self.GetParent().GetParent().GetName(),self.bMaster),self)
                #vtLog.vtCallStack(3,VERBOSE)
            #if self.bMaster: # wro 20060102
            #    evt.Skip()
            #    return
            if self.bNetResponse==False:
                return
            id=evt.GetID()
            idNew=evt.GetNewID()
            resp=evt.GetResponse()
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,
                        'id      :%s;idNew  :%s;idManAdd:%s;reps:%s'%(id,idNew,self.idManualAdd,resp),self)
            if resp!='ok':
                return
            child=self.doc.getNodeById(idNew)
            if child is None:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,
                            'node is None;id      :%s;idNew  :%s;idManAdd:%s;reps:%s'%(id,idNew,self.idManualAdd,resp),self)
                return
            if self.doc.IsSkip(child):
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,
                            'node is to skip;id      :%s;idNew  :%s;idManAdd:%s;reps:%s'%(id,idNew,self.idManualAdd,resp),self)
                return
            tup=self.__findNodeByID__(None,id)
            #vtLog.vtLngCurWX(vtLog.DEBUG,tup,self)
            if tup is not None:
                if vtLog.vtLngIsLogged(vtLog.INFO):
                    vtLog.vtLngCurWX(vtLog.INFO,
                            'node is already in tree;id      :%s;idNew  :%s;idManAdd:%s;reps:%s'%(id,idNew,self.idManualAdd,resp),self)
                if long(id)!=long(idNew):
                    self.Delete(tup[0])
                    if self.tiCache is not None:
                        self.tiCache.DelID(long(id))
                else:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurWX(vtLog.DEBUG,'id:%s'%id,self)
                    return
            idPar=self.doc.getKeyNum(self.doc.getParent(child))
            self.__addElements__(long(idPar),long(idNew))
            if self.idManualAdd==id:
                self.thdProc.DoWX(self.SelectByID,idNew)
                self.idManualAdd=''
        except:
            vtLog.vtLngTB(self.GetName())
    def OnMoveNode(self,evt):
        evt.Skip()
        try:
            idPar=evt.GetParID()
            id=evt.GetID()
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCurWX(vtLog.INFO,'parent:%s master:%d;idPar:%s;id   :%s'%\
                            (self.GetParent().GetParent().GetName(),self.bMaster,idPar,id),self)
                #vtLog.vtCallStack(3,VERBOSE)
            if self.bNetResponse==False:
                return
            
            parNode=self.doc.getNodeById(idPar)
            node=self.doc.getNodeById(id)
            #sParID=self.doc.getKey(parNode)
            #sID=self.doc.getKey(node)
            #self.doc.moveNode(parNode,node)
            tupPar=self.__findNodeByID__(None,idPar)
            tup=self.__findNodeByID__(None,id)
            if VERBOSE>0:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'idPar:%s  found:%d;id   :%s  found:%d'%\
                                (idPar,tupPar is not None,id,tup is not None),self)
            if tupPar is not None:
                tp=tupPar[0]
            else:
                tp=long(idPar)
            #if tup is None:
            #    evt.Skip()
            #    return
            #self.Delete(tup[0])
            #if self.tiCache is not None:
            #    self.tiCache.DelID(sID)
            if tup is not None:
                self.Delete(tup[0])
                if self.tiCache is not None:
                    self.tiCache.DelID(id)
            if node is None:
                vtLog.vtLngCurWX(vtLog.ERROR,'idPar:%s id:%s node not found'%(idPar,id),self)
                return
            if self.bMaster or self.bController:
                #self.doc.AlignNode(tup[1])    # wro 060819
                self.doc.AlignNode(node)
            #self.__addElementAndChilds__(tupPar[0],tup[1],self.skip,False)
            #self.SortChildren(tupPar[0])
            #self.__addElements__(tp,tup[1])    # wro 060819
            #self.__addElements__(tp,node)
            self.__addElements__(long(idPar),long(id))
            if self.verbose:
                vtLog.CallStack('')
                vtLog.vtSpace()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnMoveNodeResp(self,evt):
        evt.Skip()
        try:
            idPar=evt.GetParID()
            id=evt.GetID()
            resp=evt.GetResponse()
            if VERBOSE>0:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'parent:%s master:%d;idPar:%s;id   :%s;resp:%s'%\
                                (self.GetParent().GetParent().GetName(),self.bMaster,idPar,id,resp),self)
                #vtLog.vtCallStack(3,VERBOSE)
            if self.bNetResponse==False:
                return
            parNode=self.doc.getNodeById(idPar)
            node=self.doc.getNodeById(id)
            if resp!='ok':
                vtLog.vtLngCurWX(vtLog.CRITICAL,'FIXME',self)
                return
            return
            #sParID=self.doc.getKey(parNode)
            #sID=self.doc.getKey(node)
            #self.doc.moveNode(parNode,node)
            tupPar=self.__findNodeByID__(None,idPar)
            tup=self.__findNodeByID__(None,id)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'idPar:%s  found:%d;id   :%s  found:%d'%\
                            (idPar,tupPar is not None,id,tup is not None),self)
            if tupPar is not None:
                tp=tupPar[0]
            else:
                tp=long(idPar)
            #if tup is None:
            #    evt.Skip()
            #    return
            #self.Delete(tup[0])
            #if self.tiCache is not None:
            #    self.tiCache.DelID(sID)
            if tup is not None:
                self.Delete(tup[0])
                if self.tiCache is not None:
                    self.tiCache.DelID(id)
            
            if node is None:
                vtLog.vtLngCurWX(vtLog.ERROR,'idPar:%s id:%s node not found'%(idPar,id),self)
                return
            if self.bMaster or self.bController:
                #self.doc.AlignNode(tup[1])    # wro 060819
                self.doc.AlignNode(node)
            #self.__addElementAndChilds__(tupPar[0],tup[1],self.skip,False)
            #self.SortChildren(tupPar[0])
            #self.__addElements__(tp,tup[1])    # wro 060819
            #self.__addElements__(tp,node)
            self.__addElements__(long(idPar),long(id))
        except:
            vtLog.vtLngTB(self.GetName())
    def OnDelNode(self,evt):
        evt.Skip()
        try:
            id=evt.GetID()
            resp=evt.GetResponse()
            if VERBOSE>0:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'parent:%s;master:%d;response:%d;id   :%s;resp:%s'%\
                                (self.GetParent().GetParent().GetName(),self.bMaster,self.bNetResponse,id,resp),self)
                #vtLog.vtCallStack(3,VERBOSE)
            if self.bNetResponse==False:
                return
            tup=self.__findNodeByID__(None,id)
            #if vtLog.vtLngIsLogged(vtLog.DEBUG):
            #    vtLog.vtLngCallStack(None,vtLog.DEBUG,'id:%s %d'%(id,tup is not None),verbose=self.verbose,
            #                origin=self.GetName())
            if tup is not None:
                self.__Delete__(tup[0])
                if self.tiCache is not None:
                    self.tiCache.DelID(id)
                #if self.bMaster or self.bController:
                #    self.doc.deleteNode(tup[1])
            if self.verbose:
                vtLog.CallStack('')
                vtLog.vtSpace()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnRemoveNode(self,evt):
        evt.Skip()
        try:
            id=evt.GetID()
            if VERBOSE>0:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'parent:%s master:%d;id   :%s'%\
                                (self.GetParent().GetParent().GetName(),self.bMaster,id),self)
                #vtLog.vtCallStack(3,VERBOSE)
            if self.bNetResponse==False:
                return
            tup=self.__findNodeByID__(None,id)
            #if vtLog.vtLngIsLogged(vtLog.DEBUG):
            #    vtLog.vtLngCallStack(None,vtLog.DEBUG,'id:%s %d'%(id,tup is not None),verbose=self.verbose,
            #                origin=self.GetName())
            if tup is not None:
                self.__Delete__(tup[0])
                if self.tiCache is not None:
                    self.tiCache.DelID(id)
                if self.bMaster or self.bController:
                    node=self.doc.getNodeById(id)
                    self.doc.deleteNode(node)
            else:
                if self.bMaster or self.bController:
                    node=self.doc.getNodeById(id)
                    self.doc.deleteNode(node)
            if self.verbose:
                vtLog.CallStack('')
                vtLog.vtSpace()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnLock(self,evt):
        evt.Skip()
        try:
            if VERBOSE>0:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'id:%s;parent:%s master:%d'%(evt.GetID(),self.GetParent().GetParent().GetName(),self.bMaster),
                                self)
                #vtLog.vtLngCallStack(None,vtLog.DEBUG,'id:%s'%(evt.GetID()),
                #            origin=self.GetName())
                #vtLog.vtCallStack(3,VERBOSE)
            if self.verbose:
                vtLog.CallStack('')
                vtLog.vtSpace()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnUnLock(self,evt):
        evt.Skip()
        try:
            if VERBOSE>0:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'id:%s;parent:%s master:%d'%(evt.GetID(),self.GetParent().GetParent().GetName(),self.bMaster),
                                self)
                #vtLog.vtLngCallStack(None,vtLog.DEBUG,'',
                #            origin=self.GetName())
                #vtLog.vtCallStack(3,VERBOSE)
            #return
            iIdSel=self.GetSelectedIDNum()
            iIdOld=long(evt.GetID())
            if iIdSel!=iIdOld:
                return
            if self.bMaster:
                nodeSel=self.GetSelected()
                if nodeSel is not None:
                    if self.doc.isSameKey(nodeSel,evt.GetID()):
                        resp=evt.GetResponse()
                        if resp in  ['released']:
                            self.doc.startEdit(nodeSel)
                        elif resp in  ['granted']:
                            self.doc.endEdit(nodeSel)
                    else:
                        if len(self.doc.getAttribute(nodeSel,'iid'))>0:
                            # instanced node selected
                            instNode=self.doc.getInstBase(nodeSel)
                            
                            node=self.doc.getNodeById(evt.GetID())
                            if self.doc.getChild(node,'__node') is not None:
                                nodeTmpl=self.doc.getTmplBase(node)
                                if self.doc.isSameKey(nodeTmpl,self.doc.getAttribute(instNode,'iid')):
                                    self.doc.startEdit(nodeSel)
            if self.verbose:
                vtLog.CallStack('')
                vtLog.vtSpace()
        except:
            vtLog.vtLngTB(self.GetName())
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'exit',self)
    def OnLockRequest(self,evt):
        evt.Skip()
        try:
            if VERBOSE>0:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'parent:%s master:%d'%(self.GetParent().GetParent().GetName(),self.bMaster),
                                self)
            try:
                if self.bMaster:
                    if evt.GetKind()=='grant':
                        pass
                    elif evt.GetKind()=='reject':
                        pass
                    else:
                        if self.dlgLockReq is None:
                            self.dlgLockReq=vtNetLockRequestDialog(self)
                            self.dlgLockReq.Centre()
                        node=self.doc.getNodeByIdNum(long(evt.GetID()))
                        self.dlgLockReq.SetRequest(self.doc,node,evt.GetMsg(),evt.GetRaw())
                        self.dlgLockReq.Show(True)
            except:
                vtLog.vtLngTB(self.GetName())
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSelect(self,evt):
        evt.Skip()
        try:
            self.SelectByID(evt.GetID())
        except:
            vtLog.vtLngTB(self.GetName())
    def OnAddFinished(self,evt):
        if VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,`self.bSelectLast`,self)
        try:
            if self.bSelectLast:
                self.bSelectLast=True
                self.SelectLastAdded()
                self.Refresh()
            else:
                self.Refresh()
                ti=evt.GetTreeItem()
                bSel=False
                if self.TREE_DATA_ID:
                    id=self.GetPyData(ti)
                    node=self.doc.getNodeByIdNum(id)
                else:
                    node=self.GetPyData(ti)
                    id=self.doc.getKeyNum(node)
                if VERBOSE>0:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurWX(vtLog.DEBUG,
                            'id      :%d;idManAdd:%s'%(id,self.idManualAdd),self)
                if long(self.idManualAdd)==id:
                    self.SelectItem(ti)
                    self.EnsureVisible(ti)
                    self.idManualAdd=''
                    if self.TREE_DATA_ID:           # 080426:wro
                        self.objTreeItemSel=id
                    else:
                        self.objTreeItemSel=node
                    self.triSelected=evt.GetTreeItem()
                    wx.PostEvent(self,vtXmlTreeItemSelected(self,
                                self.triSelected,self.objTreeItemSel))
                if long(self.idManualMove)==id:
                    self.SelectItem(ti)
                    self.EnsureVisible(ti)
                    self.idManualMove=''
                    if self.TREE_DATA_ID:           # 080426:wro
                        self.objTreeItemSel=id
                    else:
                        self.objTreeItemSel=node
                    self.triSelected=evt.GetTreeItem()
                    wx.PostEvent(self,vtXmlTreeItemSelected(self,
                                self.triSelected,self.objTreeItemSel))
        except:
            pass
        #self.thdAddElements.StartScheduled()
        evt.Skip()
    def SelectLastAdded(self):
        vtLog.vtLngCurWX(vtLog.CRITICAL,'FIXME',self)
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if self.tiLast is not None:
            self.SelectItem(self.tiLast)
            self.EnsureVisible(self.tiLast)
            try:
                if self.TREE_DATA_ID:
                    id=self.GetPyData(self.tiLast)
                    node=self.doc.getNodeByIdNum(id)
                else:
                    node=self.GetPyData(self.tiLast)
                    id=self.doc.getKeyNum(node)
                vtLog.vtLngCurWX(vtLog.DEBUG,'id:%d'%id,self)
            except:
                node=None
            if self.TREE_DATA_ID:           # 080426:wro
                self.objTreeItemSel=id
            else:
                self.objTreeItemSel=node
            self.triSelected=self.tiLast
            wx.PostEvent(self,vtXmlTreeItemSelected(self,
                            self.triSelected,self.objTreeItemSel))
    def SetNetDoc(self,netDoc,lst=None):
        self.SetDoc(netDoc,True)
        node=self.doc.getRoot()
        if lst is not None:
            node=self.doc.getChildByLst(node,lst)
        self.SetNode(node)
    def SetByLst(self,lst):
        node=self.doc.getRoot()
        if lst is not None:
            node=self.doc.getChildByLst(node,lst)
        self.SetNode(node)
    def ClearDelayed(self):
        #self.thdAddElements.Do(None,None,False)
        self.thdProc.DoWX(self.__Clear__)
    def Clear(self):
        #if self.bMaster:
        #    if self.objTreeItemSel is not None:
        #        self.doc.endEdit(self.objTreeItemSel)
        #self.thdAddElements.Do(None,None,False)
        vtLog.vtLngCurWX(vtLog.DEBUG,'clear',self)
        self.__Clear__()
    def __Clear__(self):
        vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
        self.objTreeItemSel=None
        self.triSelected=None
        if self.tiCache is not None:
            self.tiCache.Clear()
            self.tiCache.Build(self)
        #if self.tiRoot is not None:
        #    self.DeleteChildren(self.tiRoot)
        #return
        #self.rootNode=None
        self.DeleteAllItems()
        self.tiRoot=None
        self.rootNode=None
        self.bSelectLast=self.bAutoSelectLast
        
        #self.__createRoot__(None)
        wx.PostEvent(self,vtXmlTreeItemSelected(self,
                self.triSelected,self.objTreeItemSel))  # forward double click event
    def __applyFormat__(self,val,fmt):
        sPos=fmt.split(',')
        if len(sPos)==2:
            try:
                if len(sPos[0])>0:
                    i=int(sPos[0])
                    if len(sPos[1])>0:
                        j=int(sPos[1])
                        val=val[i:j]
                    else:
                        val=val[i:]
                else:
                    if len(sPos[1])>0:
                        j=int(sPos[1])
                        val=val[:j]
            except:
                pass
            return val
        if fmt[0]=='#':
            i=val.find(fmt[1])
            if i>0:
                return val[:i]
            else:
                return val
        
        if fmt=='YYYY':
            val=val[:4]
        elif fmt=='MM':
            val=val[4:6]
        elif fmt=='DD':
            val=val[6:8]
        elif fmt=='-DD':
            val='-'+val[6:8]
        elif fmt=='YYYY-MM-DD':
            val=val[:4]+'-'+val[4:6]+'-'+val[6:8]
        elif fmt=='MM-DD':
            val=val[4:6]+'-'+val[6:8]
        elif fmt=='HH':
            val=val[:2]
        elif fmt=='MM':
            val=val[2:4]
        elif fmt=='SS':
            val=val[4:6]
        elif fmt=='HHMM':
            val=val[:4]
        elif fmt=='HH:MM':
            val=val[:2]+':'+val[2:4]
        return val
    def __getValFormat__(self,g,val,infos=None):
        #vtLog.CallStack(val)
        #vtLog.pprint(g)
        #vtLog.pprint(infos)
        try:
            if val is None:
                return '---'
            val=val.strip()
            if len(g[1])>0:
                sFormat=g[1]
                val=self.__applyFormat__(val,sFormat)
                try:
                    for gTmp in g[2:]:
                        try:
                            newVal=infos[gTmp[0]]
                        except:
                            newVal=''
                        val=val+self.__getValFormat__(gTmp,newVal,infos)
                except:
                    pass
                return val
            if len(val)<=0:
                val="---"
            return val
        except:
            return '---'
    def __getImgFormat__(self,g,val):
        try:
            imgInst=-1
            imgInstSel=-1
            imgInvalid=-1
            imgInvalidSel=-1
            
            val=val.strip()
            if len(g[1])>0:
                sFormat=g[1]
                if g[0]=='date':
                    if sFormat=='YYYY':
                        #img=self.imgDict['grp']['YYYY'][0]
                        #imgSel=self.imgDict['grp']['YYYY'][0]
                        #return (img,imgSel,imgInst,imgInstSel,imgInvalid,imgInvalidSel)
                        return self.imgDict['grp']['YYYY']
                    elif sFormat=='MM':
                        #img=self.imgDict['grp']['MM'][0]
                        #imgSel=self.imgDict['grp']['MM'][0]
                        #return (img,imgSel,imgInst,imgInstSel,imgInvalid,imgInvalidSel)
                        return self.imgDict['grp']['MM']
                    elif sFormat=='DD':
                        #img=self.imgDict['grp']['DD'][0]
                        #imgSel=self.imgDict['grp']['DD'][0]
                        #return (img,imgSel,imgInst,imgInstSel,imgInvalid,imgInvalidSel)
                        return self.imgDict['grp']['DD']
            if g[0]=='person':
                #if sFormat=='':
                #img=self.imgDict['grp']['person'][0]
                #imgSel=self.imgDict['grp']['person'][0]
                #return (img,imgSel,imgInst,imgInstSel,imgInvalid,imgInvalidSel)
                return self.imgDict['grp']['person']
            tagName=g[0]
            try:
                img=self.imgDict['grp'][tagName][0]
            except:
                img=self.imgDict['grp']['dft'][0]
            try:
                imgSel=self.imgDict['grp'][tagName][1]
            except:
                imgSel=self.imgDict['grp']['dft'][1]
            try:
                imgInst=self.imgDict['grp'][tagName][2]
            except:
                imgInst=self.imgDict['grp']['dft'][2]
            try:
                imgInstSel=self.imgDict['grp'][tagName][3]
            except:
                imgInstSel=self.imgDict['grp']['dft'][3]
            try:
                imgInvalid=self.imgDict['grp'][tagName][4]
            except:
                imgInvalid=self.imgDict['grp']['dft'][4]
            try:
                imgInvalidSel=self.imgDict['grp'][tagName][5]
            except:
                imgInvalidSel=self.imgDict['grp']['dft'][5]
        except:
            img,imgSel=-1,-1
        return (img,imgSel,imgInst,imgInstSel,imgInvalid,imgInvalidSel)
    def __getImagesByInfos__(self,tagName,infos):
        img,imgSel=-1,-1
        imgInst=-1
        imgInstSel=-1
        imgInvalid=-1
        imgInvalidSel=-1

        try:
            try:
                img=self.imgDict['elem'][tagName][0]
            except:
                img=self.imgDict['elem']['dft'][0]
            try:
                imgSel=self.imgDict['elem'][tagName][1]
            except:
                imgSel=self.imgDict['elem']['dft'][1]
            try:
                imgInst=self.imgDict['elem'][tagName][2]
            except:
                imgInst=self.imgDict['elem']['dft'][2]
            try:
                imgInstSel=self.imgDict['elem'][tagName][3]
            except:
                imgInstSel=self.imgDict['elem']['dft'][3]
            try:
                imgInvalid=self.imgDict['elem'][tagName][4]
            except:
                imgInvalid=self.imgDict['elem']['dft'][4]
            try:
                imgInvalidSel=self.imgDict['elem'][tagName][5]
            except:
                imgInvalidSel=self.imgDict['elem']['dft'][5]
            #try:
            #    if infos['|iid']!='':
            #        img=self.imgDict['elem'][tagName][2]
            #except:
            #    pass
        except:
            pass
        return (img,imgSel,imgInst,imgInstSel,imgInvalid,imgInvalidSel)
    def isNodeInstance(self,o,infos):
        try:
            if infos['|iid']!='':
                return True
        except:
            pass
        return False
    def isNodeInvalid(self,o,infos):
        try:
            par=self.GetParent()
            par.IsNodeInvalid(o,infos)
        except:
            pass
        return False
    def __createRoot__(self,node,blocking=False):
        self.__Clear__()
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        tn=None
        if self.doc is not None:
            if self.doc.acquire(blocking=blocking):
                try:
                    if self.doc.hasNodeInfos2Get()==False:      # 070920:wro already set?
                        self.doc.setNodeInfos2Get(self.nodeInfos)
                except:
                    pass
                try:
                    tagName,infos=self.doc.getNodeInfos(node,self.lang)
                    tn=self.__addRootByInfos__(node,tagName,infos)
                    #self.tiRoot=tn
                    #self.rootNode=node
                except:
                    vtLog.vtLngTB(self.GetName())
                #self.rootNode=node
                self.doc.release()
            else:
                vtLog.vtLngCurWX(vtLog.ERROR,'acquire fault;infos:%s'%(vtLog.pformat(self.doc.dUsrInfos)),self)
        else:
            vtLog.vtLngCurWX(vtLog.ERROR,'doc not set',self)
        return tn
    def __addRootByInfos__(self,node,tagName,infos):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'id:%s;tag:%s;infos:%s'%\
                                (self.doc.getKey(node),tagName,vtLog.pformat(infos)),self)
        sTagName='root'
        try:
            if node is not None:
                sTagName=self.doc.getTagName(node)
                oReg=self.doc.GetReg(sTagName)
                if oReg is None:
                    sTagName='root'
        except:
            sTagName='root'
        try:
            imgTup=self.__getImagesByInfos__(sTagName,[])
            img=imgTup[0]
            imgSel=imgTup[1]
        except:
            try:
                img=self.imgDict['elem']['root'][0]
            except:
                img=self.imgDict['elem']['dft'][0]
            try:
                imgSel=self.imgDict['elem']['root'][1]
            except:
                imgSel=self.imgDict['elem']['dft'][1]
        if node is None:
            tid=None
        else:
            tid=wx.TreeItemData()
            if self.TREE_DATA_ID:
                tid.SetData(self.doc.getKeyNum(node))
            else:
                tid.SetData(node)
        if 1:
            try:
                if hasattr(self.doc,'GetReg'):
                    oReg=self.doc.GetReg('root')
                    s=oReg.GetDescription()
                else:
                    s=_(u'root')
            except:
                s=_(u'root')
        else:
            s=_(u'root')
        r=self.AddRoot(s,img,imgSel,tid)
        self.tiRoot=r
        self.rootNode=node
        self.idRootNode=self.doc.getKeyNum(node)
        if tid is not None:
            if self.tiCache is not None:
                self.tiCache.AddID(r,self.doc.getKeyNum(node))
        return r
    def AddElement(self,parent,obj,func=None,*args,**kwargs):
        if obj is None:
            return -2
        id=self.doc.getKeyNum(obj)
        #if parent is None:
        #    parent=self.tiRoot
        #    if parent is None:
        #        self.__createRoot__(None)
        #        parent=self.tiRoot
        #if parent is None:
        #    return -1
        #self.__addElements__(parent,obj,func,*args)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'parent:%s;id:%08d'%(
                        vtLog.pformat(parent),id),self)
        self.thdProc.DoWX(self.__addElements__,parent,id,func,*args,**kwargs)
    def __addElements__(self,idPar,id,func=None,*args,**kwargs):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'idPar:%08d;id:%08d;parent:%s master:%d'%
                        (idPar,id,self.GetParent().GetParent().GetName(),self.bMaster),
                        self)
        if self.HasId(idPar)==0:
            return
        tup=self.__findNodeByID__(None,idPar)
        if tup is not None:
            tn=tup[0]
            iLevel=self.__getTreeLevels__(tn)
            tc=self.GetFirstChild(tn)
            if tc[0].IsOk()==False:
                id=self.GetPyData(tn)
                self.DoSafe(self.thdProc.AddChildNodes,tn,id,self.lang,
                        iLevel,self.iLevelAutoBuild,self.iLevelLimit,
                        func,*args,**kwargs)
            else:
                self.DoSafe(self.thdProc.AddNode,idPar,id,self.lang,
                        iLevel,self.iLevelAutoBuild,self.iLevelLimit,
                        func,*args,**kwargs)
    def __addElementByInfos__(self,parent,o,tagName,infos,bRet=False):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;key:%s'%(tagName,self.doc.getKey(o)),self)
        id=self.doc.getKey(o)
        tup=self.__findNodeByID__(None,id)
        if tup is not None:
            return tup[0]
        try:
            if self.__is2add__(o,tagName)==False:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;key:%s is not to add'%(tagName,self.doc.getKey(o)),self)
                if bRet:
                    return parent
                else:
                    return
            if self.__is2skip__(o,tagName):
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;key:%s is to skip'%(tagName,self.doc.getKey(o)),self)
                if bRet:
                    return parent
                else:
                    return
        except:
            vtLog.vtLngTB(self.GetName())
        tnparent=parent
        imgTup=self.__getImagesByInfos__(tagName,infos)
        sLabel=''
        if type(self.label)==types.DictType:
            try:
                label=self.label[tagName]
            except:
                try:
                    label=self.label[None]
                except:
                    vtLog.vtLngTB(self.GetName())
        else:
            label=self.label
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;label:%s'%(tagName,repr(label)),self)
        if type(label) in [types.FunctionType,types.MethodType]:
            sLabel=label(parent=parent,node=o,tagName=tagName,infos=infos)
        else:
            for v in label:
                if type(v)==types.UnicodeType:
                    sLabel=sLabel+v+' '
                elif v[0] is None:
                    sLabel=sLabel+tagName+' '
                elif v[0] in infos:
                    val=infos[v[0]]
                    val=self.__getValFormat__(v,val)
                    sLabel=sLabel+val+' '
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'sLabel:%s'%(sLabel),self)
        # check project entry type
        tid=wx.TreeItemData()
        if self.TREE_DATA_ID:
            tid.SetData(self.doc.getKeyNum(o))
        else:
            tid.SetData(o)
        tn=self.AppendItem(tnparent,sLabel,-1,-1,tid)
        if self.tiCache is not None:
            self.tiCache.AddID(tn,self.doc.getKeyNum(o))#infos[self.nodeKey])
        iOfs=0
        if self.isNodeInstance(o,infos):
            iOfs=2
            self.SetItemBold(tn)
        if self.isNodeInvalid(o,infos):
            iOfs=4
        try:
            self.SetItemImage(tn,imgTup[iOfs+0],wx.TreeItemIcon_Normal)
            self.SetItemImage(tn,imgTup[iOfs+1],wx.TreeItemIcon_Selected)
        except:
            #vtLog.CallStack('')
            #print tn,tnparent,sLabel,imgTup,bRet,iOfs
            vtLog.vtLngCurWX(vtLog.ERROR,'tag:%s;infos:%s'%(tagName,vtLog.pformat(infos)),self)
            vtLog.vtLngTB(self.GetName())
            #print self.GetItemText(tnparent)
            #print self.GetItemText(tnparent)
        # add grouping sub nodes here
        self.tiLast=tn
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;key:%s added'%(tagName,self.doc.getKey(o)),self)
        if bRet==True:
            return tn
    def SetTagNames2Base(self,lst):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'SetTagNames2Base;lst:%s'%lst,self)
        self.lTagNames2Base=lst
    def GetBaseNode(self):
        try:
            node=self.doc.getBaseNode()
            try:
                if self.lTagNames2Base is not None:
                    node=self.doc.getChildByLst(node,self.lTagNames2Base)
                    if node is None:
                        vtLog.vtLngCurWX(vtLog.INFO,'',self)
            except:
                vtLog.vtLngTB(self.GetName())
            return node
        except:
            vtLog.vtLngTB(self.GetName())
            return None
    def SetNode(self,node):
        if node is None:
            id=-2
        else:
            id=self.doc.getKeyNum(node)
        
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'id:%d'%id,self)
        self.DoSafe(self.thdProc.SetNode,id,self.lang,
                self.iLevelAutoBuild,self.iLevelLimit)
    def GetRootNode(self):
        return self.rootNode
    def SetDlgAdd(self,dlg):
        self.dlgAdd=dlg
    def SetDlgRename(self,dlg):
        self.dlgRename=dlg
    def OnTcNodeTreeSelChanged(self, event):
        event.Skip()
        try:
            if VERBOSE>0:
                self.__logInfo__(''%())
            tn=event.GetItem()
            self.__selectItem__(tn)
        except:
            self.__logTB__()
    def OnTcNodeTreeSelChanging(self, event):
        event.Skip()
        try:
            if VERBOSE>0:
                self.__logInfo__(''%())
            tn=event.GetItem()
            self.__selectItem__(tn)
        except:
            self.__logTB__()
    def __selectItem__(self,tn):
        if VERBOSE>0:
            self.__logDebug__(''%())
        bChanged=True
        if tn is None:
            self.objTreeItemSel=None
            self.triSelected=None
        else:
            try:
                idOld=self.GetSelectedIDNum()
                if self.TREE_DATA_ID:
                    id=self.GetPyData(tn)
                    #node=self.doc.getNodeByIdNum(id)
                    if vtLog.vtLngIsLogged(vtLog.INFO):
                        vtLog.vtLngCurWX(vtLog.INFO,'id:%08d'%(id),self)
                    self.objTreeItemSel=id              # 080426:wro
                else:
                    if self.doc.acquired('dom')==False:
                        vtLog.vtLngCurWX(vtLog.CRITICAL,'FIXME:lock doc',self)
                    node=self.GetPyData(tn)
                    id=self.doc.getKeyNum(node)
                    self.objTreeItemSel=node            # 080426:wro
                    if vtLog.vtLngIsLogged(vtLog.INFO):
                        vtLog.vtLngCurWX(vtLog.INFO,'id:%08d'%(id),self)
                bChanged=idOld!=id
                self.triSelected=tn
                if bChanged==False:
                    if vtLog.vtLngIsLogged(vtLog.INFO):
                        vtLog.vtLngCurWX(vtLog.INFO,
                                'node id:%s not changed;skip post event'%(
                                repr(id)),self)
                    return 0
            except:
                self.objTreeItemSel=None
                self.triSelected=None
        wx.PostEvent(self,vtXmlTreeItemSelected(self,
                self.triSelected,self.objTreeItemSel))  # forward double click event
        return 0
    def OnTcNodeCollapsing(self,evt):
        self.__logDebug__(''%())
        ti=evt.GetItem()
        if ti==self.tiRoot:
            evt.Veto()
        else:
            evt.Skip()
    def OnTcNodeCollapes(self,evt):
        self.__logDebug__(''%())
        if self.bStoreExpand:
            tn=evt.GetItem()
            if self.TREE_DATA_ID:
                node=self.doc.getNodeByIdNum(self.GetPyData(tn))
            else:
                node=self.GetPyData(tn)
            #self.doc.setNodeText(node,self.sExpandName,'0')
            self.doc.setAttribute(node,self.sExpandName,'0')
    def OnTcNodeExpanding(self,evt):
        try:
            tn=evt.GetItem()
            bAdd=False
            tc=self.GetFirstChild(tn)
            if tc[0].IsOk()==False:
                self.__logDebug__('add children'%())
                id=self.GetPyData(tn)
                iLevel=self.__getTreeLevels__(tn)
                self.DoSafe(self.thdProc.AddChildNodes,tn,id,
                        self.lang,iLevel,self.iLevelAutoBuild,self.iLevelLimit)
                self.thdProc.Do(self.thdProc.CallBack,self.SortChildren,tn)
                self.thdProc.Do(self.thdProc.CallBack,self.Expand,tn)
            else:
                self.__logDebug__(''%())
                #self.SortChildren(tn)                          # 10221:wro sort on expanding
                self.thdProc.CallBack(self.SortChildren,tn)     # 10222:wro sort on expanding but delay it
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnTcNodeExpanded(self,evt):
        try:
            tn=evt.GetItem()
            bAdd=False
            if self.bStoreExpand:
                if self.TREE_DATA_ID:
                    id=self.GetPyData(tn)
                    node=self.doc.getNodeByIdNum(id)
                    if node is not None:
                        self.doc.setAttribute(node,self.sExpandName,'1')
                else:
                    node=self.GetPyData(tn)
                #self.doc.setNodeText(node,self.sExpandName,'1')
                    self.doc.setAttribute(node,self.sExpandName,'1')
            if bAdd==False:
                self.SortChildren(tn)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnTcNodeExpandingNoStore(self,evt):
        try:
            tn=evt.GetItem()
            self.SortChildren(tn)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def __updateTree__(self):
        tn=self.triSelected
        #id=self.doc.getKeyNum(self.objTreeItemSel)
        id=self.GetSelectedIDNum()
        self.DeleteChildren(tn)
        #self.__addElements__(self.triSelected,self.objTreeItemSel)
        #self.thdAddElements.Do(self.triSelected,self.doc.getKeyNum(self.objTreeItemSel))
        iLevel=self.__getTreeLevels__(tn)
        self.DoSafe(self.thdProc.AddChildNodes,tn,id,self.lang,
                        iLevel,self.iLevelAutoBuild,self.iLevelLimit)
    def __deleteFromCache__(self,ti,bChildrenOnly=False):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurCls(vtLog.CRITICAL,'called by thread'%(),self)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
        if bChildrenOnly==False:
            if self.TREE_DATA_ID:
                id=self.GetPyData(ti)
            else:
                node=self.GetPyData(ti)
                if node is not None:
                    id=self.doc.getKeyNum(node)
                else:
                    id=None
            if id is not None:
                self.tiCache.DelID(id)
        triChild=self.GetFirstChild(ti)
        while triChild[0].IsOk():
            self.__deleteFromCache__(triChild[0])
            triChild=self.GetNextChild(ti,triChild[1])
    def DeleteByID(self,id):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
        self.thdProc.DoWX(self.__DeleteByID__,id)
    def Delete(self,ti):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
        self.thdProc.DoWX(self.__Delete__,ti)
    def __DeleteByID__(self,id):
        tup=self.__findNodeByID__(None,id)
        if tup is None:
            return None
        self.__Delete__(tup[0])
    def __Delete__(self,ti):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurCls(vtLog.CRITICAL,'called by thread'%(),self)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'start'%(),self)
        if self.TREE_DATA_ID:
            id=self.GetPyData(ti)
        else:
            node=self.GetPyData(ti)
            if node is not None:
                id=self.doc.getKeyNum(node)
            else:
                id=None
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'ti:%s;id:%s'%(ti,repr(id)),self)
        try:
            if self.tiCache is not None:
                self.__deleteFromCache__(ti)
        except:
            vtLog.vtLngTB(self.GetName())
        #self.triSelected=None
        #self.objTreeItemSel=None
        wx.TreeCtrl.Delete(self,ti)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'fin'%(),self)
        #print self.tiCache.cache.keys()
        #print vtLog.pformat(self.tiCache.cache)
    def DeleteChildren(self,ti):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
        self.thdProc.DoWX(self.__DeleteChildren__,ti)
    def __DeleteChildren__(self,ti):
        #vtLog.CallStack('')
        try:
            if self.tiCache is not None:
                self.__deleteFromCache__(ti,bChildrenOnly=True)
        except:
            vtLog.vtLngTB(self.GetName())
        #wx.TreeCtrl.Collapse(self,ti)
        #wx.TreeCtrl.DeleteChildren(self,ti)
        wx.TreeCtrl.CollapseAndReset(self,ti)
    def OnTcNodeRightUp(self, event):
        if self.bMaster==False:
            return
        #print 'right up'
        if not hasattrs(self,popupIdAdd):
            self.popupIdAdd=wxNewId()
            self.popupIdDel=wxNewId()
            self.popupIdRename=wxNewId()
            self.popupIdMove=wxNewId()
            EVT_MENU(self.popupIdAdd,self.OnTreeAdd)
            EVT_MENU(self,self.popupIdRename,self.OnTreeRename)
        menu=wx.Menu()
        menu.AppendItem(self.popupIdAdd,'Add Node')
        menu.AppendItem(self.popupIdDel,'Delete Node')
        menu.AppendItem(self.popupIdRename,'Rename Node')
        menu.AppendItem(self.popupIdMove,'Move Node')
        
        self.PopupMenu(menu,wxPoint(self.x, self.y))
        menu.Destroy()
        event.Skip()
    def __getDocument__(self,node):
        try:
            pn=node.parentNode
            if pn is not None:
                return self.__getDocument__(pn)
            else:
                return node
        except:
            return None
    def OnTreeAdd(self,event):
        vtLog.vtLngCurWX(vtLog.CRITICAL,'FIXME',self)
        if self.dlgAdd is not None:
            ret=self.dlgAdd.ShowModal()
            if ret==1:
                s=self.dlgAdd.GetNodeName()
                nodeSel=self.GetSelected()
                self.dlgAdd.AddNode(nodeSel,
                    self.__getDocument__(nodeSel))
                if self.dlgAdd.IsChildAdded()==False:
                    parNode=self.doc.getParent(nodeSel)
                    triPar=self.GetItemParent(self.triSelected)
                    self.DeleteChildren(triPar)
                    self.__addElements__(triPar,parNode)
                    wx.PostEvent(self,vtXmlTreeItemAdded(self,
                            triPar,parNode,''))  # forward double click event
        
                else:
                    self.DeleteChildren(self.triSelected)
                    #self.__addElements__(self.triSelected,
                    #                self.objTreeItemSel)
                    self.__addElements__(self.triSelected,
                                    self.GetSelected())             # 080426:wro
                    self.Expand(self.triSelected)
                    wx.PostEvent(self,vtXmlTreeItemAdded(self,
                            self.triSelected,self.objTreeItemSel,''))  # forward double click event
        
                    #self.objTreeItemSel.tagName=s
                    #self.SetItemText(self.triSelected,s)
    def OnTreeRename(self,event):
        vtLog.vtLngCurWX(vtLog.CRITICAL,'',self)
        if self.dlgRename is not None:
            self.dlgRename.SetNodeName(s)
            ret=self.dlgRename.ShowModal()
            if ret==1:
                s=self.dlgRename.GetNodeName()
                self.objTreeItemSel.tagName=s
                self.SetItemText(self.triSelected,s)
                wx.PostEvent(self,vtXmlTreeItemEdited(self,
                        self.triSelected,self.objTreeItemSel))  # forward double click event
        
    def OnTcNodeRightDown(self, event):
        self.x=event.GetX()
        self.y=event.GetY()
        #vtLog.CallStack('right')
        #print self.x,self.y
        if not hasattr(self,'popupIdAdd'):
            self.popupIdAdd=wxNewId()
            self.popupIdDel=wxNewId()
            self.popupIdRename=wxNewId()
            self.popupIdMove=wxNewId()
            EVT_MENU(self,self.popupIdAdd,self.OnTreeAdd)
            EVT_MENU(self,self.popupIdRename,self.OnTreeRename)
        menu=wx.Menu()
        menu.Append(self.popupIdAdd,'Add Node')
        menu.Append(self.popupIdDel,'Delete Node')
        menu.Append(self.popupIdRename,'Rename Node')
        menu.Append(self.popupIdMove,'Move Node')
        
        self.PopupMenu(menu,wxPoint(self.x, self.y))
        menu.Destroy()
        event.Skip()
    def __refreshAllLabels__(self,id,node,tagName,infos):
        vtLog.vtLngCur(vtLog.CRITICAL,''%(),self.GetOrigin())
        return
        self.__refreshLabel__(ti)
        ti=self.__refreshLabelByInfos__(id,node,tagName,infos)
        if ti is None:
            return 0
        triChild=self.GetFirstChild(ti)
        while triChild[0].IsOk():
            self.__refreshAllLabels__(triChild[0])
            triChild=self.GetNextChild(ti,triChild[1])
    def __refreshLabel__(self,ti,func=None,*args):
        if ti is None:
            return
        if self.doc is None:
            return
        if self.TREE_DATA_ID:
            id=self.GetPyData(ti)
            if id is None:
                return
        else:
            o=self.GetPyData(ti)
            if o is None:
                return
            id=self.doc.getKeyNum(node)
        self.DoSafe(self.thdProc.Refresh,id,False,self.lang)
    def __refreshLabelByInfos__(self,id,node,tagName,infos):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;infos:%s'%(tagName,vtLog.pformat(infos)),self)
        tup=self.__findNodeByID__(None,id)
        if tup is None:
            return None
        ti=tup[0]
        imgTup=self.__getImagesByInfos__(tagName,infos)
        try:
            imgTup=self.__getImagesByInfos__(tagName,infos)
        except:
            vtLog.vtLngTB(self.GetName())
        sLabel=''
        if type(self.label)==types.DictType:
            try:
                label=self.label[tagName]
            except:
                try:
                    label=self.label[None]
                except:
                    vtLog.vtLngTB(self.GetName())
                    return None
        else:
            label=self.label
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;label:%s'%(tagName,repr(label)),self)
        if type(label) in [types.FunctionType,types.MethodType]:
            #sLabel=label(parent=ti,node=node,tagName=tagName,infos=infos)
            sLabel=label(parent=parent,node=node,tagName=tagName,infos=infos)
        else:
            for v in label:
                if type(v)==types.UnicodeType:
                    sLabel=sLabel+v+' '
                elif v[0] is None:
                    sLabel=sLabel+tagName+' '
                elif v[0] in infos:
                    val=infos[v[0]]
                    val=self.__getValFormat__(v,val)
                    sLabel=sLabel+val+' '
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'sLabel:%s'%(sLabel),self)
        iOfs=0
        if self.isNodeInstance(node,infos):
            iOfs=2
            self.SetItemBold(tn)
        if self.isNodeInvalid(node,infos):
            iOfs=4
        try:
            img=imgTup[iOfs+0]
            imgSel=imgTup[iOfs+1]
        except:
            vtLog.vtLngTB(self.GetName())
        self.SetItemText(ti,sLabel)
        imgOld=self.GetItemImage(ti,wx.TreeItemIcon_Normal)
        if imgOld!=img:
            self.SetItemImage(ti,img,wx.TreeItemIcon_Normal)
        imgOld=self.GetItemImage(ti,wx.TreeItemIcon_Selected)
        if imgOld!=imgSel:
            self.SetItemImage(ti,imgSel,wx.TreeItemIcon_Selected)
        return ti
    def RefreshSelected(self):
        if self.triSelected is None:
            vtLog.vtLngCur(vtLog.WARN,'nothing selected',self.GetOrigin())
        self.__refreshLabel__(self.triSelected)
        wx.PostEvent(self,vtXmlTreeItemEdited(self,
                self.triSelected,self.objTreeItemSel))  # forward double click event
    def RefreshAllLabels(self):
        vtLog.vtLngCur(vtLog.DEBUG,''%(),self.GetOrigin())
        self.DoSafe(self.thdProc.Refresh,None,True,self.lang)
        return
        self.__refreshAllLabels__(self.tiRoot)
    def ReBuild(self):
        try:
            self.thdProc.ReBuild()
        except:
            vtLog.vtLngTB(self.GetName())
    def GetSelectedID(self):
        if self.objTreeItemSel is not None:
            return self.doc.getKey(self.GetSelected())
            if self.TREE_DATA_ID:
                return str(self.objTreeItemSel)
            else:
                return self.doc.getKey(self.objTreeItemSel)
        return None
    def GetSelectedIDNum(self):
        if self.objTreeItemSel is not None:
            #return self.doc.getKeyNum(self.objTreeItemSel)
            if self.TREE_DATA_ID:
                t=type(self.objTreeItemSel)
                if (t==types.LongType) or (t==types.IntType):
                    return self.objTreeItemSel
            else:
                return self.doc.getKeyNum(self.objTreeItemSel)
        return None
    def GetSelected(self):
        if self.TREE_DATA_ID:
            #return self.objTreeItemSel     # 080426: wro return node
            if self.objTreeItemSel is not None:
                t=type(self.objTreeItemSel)
                if (t==types.LongType) or (t==types.IntType):
                    return self.doc.getNodeByIdNum(self.objTreeItemSel)
            return None
        else:
            return self.objTreeItemSel
    def GetSelectedTreeItem(self):
        return self.triSelected
    def SelectByID(self,findID):
        vtLog.vtLngCurWX(vtLog.INFO,'id:%s'%(findID),self)
        if self.doc.IsKeyValid(findID)==False:
            if findID=='-1':
                if self.tiRoot is not None:
                    self.thdProc.DoWX(self.SelectItem,self.tiRoot)
            return
        self.thdProc.DoWX(self.__SelectByID__,findID)
        #self.DoSafe(self.thdProc.DoWX,self.__SelectByID__,findID)
        return
        tup=self.__findNodeByID__(self.tiRoot,findID)
        if tup is not None:
            vtLog.vtLngCurWX(vtLog.DEBUG,'tree item found',self)
            
            #self.triSelected=tup[0]
            #self.objTreeItemSel=tup[1]
            #self.EnsureVisible(tup[0])
            #self.SelectItem(tup[0],True)
        else:
            node=self.doc.getNodeById(findID)
            if node is None:
                vtLog.vtLngCurWX(vtLog.ERROR,'id:%s not present anymore'%(str(findID)),self)
                return
            if self.doc.IsSkip(node):
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'id:%s type:%s'%(str(findID),type(findID)),self)
                self.SelectByID(self.doc.getKey(self.doc.getParent(node)))
            else:
                
                if self.__buildRequiredById__(findID,self.__SelectByID__,findID):
                    vtLog.vtLngCurWX(vtLog.INFO,'id:%s'%(str(findID)),self)
                    return
                else:
                    vtLog.vtLngCurWX(vtLog.INFO,'id:%s'%(str(findID)),self)
                    return
    def _do__SelectByID__(self,findID):
        try:
            tup=self.__findNodeByID__(self.tiRoot,findID)
            self.__selectItem__(tup[0])
            self.SelectItem(tup[0],True)
            self.EnsureVisible(tup[0])
            #self.triSelected=tup[0]
            #self.objTreeItemSel=tup[1]
            #self.SelectItem(tup[0],True)
        except:
            vtLog.vtLngTB(self.GetName())
    def __getItemParentValidNode__(self,ti):
        if ti==self.tiRoot:
            return ti
        tip=self.GetItemParent(ti)
        return tip
    def __Sort__(self,id,bChild=False):
        try:
            """ this call from mainloop"""
            vtLog.vtLngCurWX(vtLog.INFO,'id:%s'%(id),self)
            #if self.doc.getNodeById(id) is None:
            #    return
            tup=self.__findNodeByID__(self.tiRoot,id)
            if tup is not None:
                if bChild:
                    tip=self.__getItemParentValidNode__(tup[0])
                    self.__sortAllChilds__(tip)
                else:
                    self.SortChildren(tup[0])
                    #self.SortChildren(self.GetItemParent(tup[0]))
        except:
            vtLog.vtLngTB(self.GetName())
    def __SelectByIDTolerant__(self,findID):
        """ this call from mainloop"""
        vtLog.vtLngCurWX(vtLog.INFO,'id:%s'%(findID),self)
        if self.doc.getNodeById(findID) is None:
            return
        self.__SelectByID__(findID)
    def __SelectByID__(self,findID):
        """ this call from mainloop"""
        vtLog.vtLngCurWX(vtLog.INFO,'id:%s'%(findID),self)
        tup=self.__findNodeByID__(self.tiRoot,findID)
        if tup is not None:
            self.__selectItem__(tup[0])
            self.SelectItem(tup[0],True)
            self.EnsureVisible(tup[0])
        else:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'id:%s type:%s'%(str(findID),type(findID)),self)
            node=self.doc.getNodeById(findID)
            if node is None:
                vtLog.vtLngCurWX(vtLog.ERROR,'id:%s not present anymore'%(str(findID)),self)
                return
            if self.doc.IsSkip(node):
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'id:%s type:%s'%(str(findID),type(findID)),self)
                self.__SelectByID__(self.doc.getKey(self.doc.getParent(node)))
            else:
                if self.__buildRequiredById__(findID,self.__SelectByID__,findID):
                    vtLog.vtLngCurWX(vtLog.INFO,'id:%s'%(str(findID)),self)
                    return
                else:
                    vtLog.vtLngCurWX(vtLog.INFO,'id:%s'%(str(findID)),self)
                    return
            #node=self.doc.getNodeById(findID)
            #if node is not None:
            #    self.SelectByID(self.doc.getKey(self.doc.getParent(node)))
    def FindTreeItem(self,node2Find):
        return self.__findNode__(self.tiRoot,node2Find)
    def __findNode__(self,ti,node2Find):
        if self.tiCache is None:
            o=self.GetPyData(ti)
            if self.TREE_DATA_ID:
                if self.doc.isSameKeyNum(node2Find,o):
                    return ti
            else:
                if self.doc.isSame(node2Find,o):
                    return ti
            
            triChild=self.GetFirstChild(ti)
            while triChild[0].IsOk():
                retVal=self.__findNode__(triChild[0],node2Find)
                if retVal is not None:
                    return retVal
                triChild=self.GetNextChild(ti,triChild[1])
            return None
        else:
            ti=self.tiCache.GetTreeItem(node2Find)
            return ti
    def __findNodeByID__(self,ti,id2Find):
        if ti is None:
            ti=self.tiRoot
        if ti is None:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'id2Find:%s no starting point'%id2Find,self)
            return None
        try:
            if long(id2Find)<0:
                if VERBOSE>5:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurWX(vtLog.DEBUG,'id2Find:%s return root'%id2Find,self)
                return (self.tiRoot,self.doc.getBaseNode())
        except:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'id2Find:%s return root'%id2Find,self)
            return (self.tiRoot,self.doc.getBaseNode())
        if self.tiCache is None:
            if VERBOSE>5:
                vtLog.vtLngCurWX(vtLog.DEBUG,'no cache',self)
            if self.TREE_DATA_ID:
                o=self.doc.getNodeByIdNum(self.GetPyData(ti))
            else:
                o=self.GetPyData(ti)
            if o is not None:
                try:
                    if self.doc.getKeyNum(o)==long(id2Find):
                        if VERBOSE>5:
                            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                vtLog.vtLngCurWX(vtLog.DEBUG,'id2Find:%s found'%id2Find,self)
                        return (ti,o)
                except:
                    pass
            triChild=self.GetFirstChild(ti)
            while triChild[0].IsOk():
                retVal=self.__findNodeByID__(triChild[0],id2Find)
                if retVal is not None:
                    return retVal
                triChild=self.GetNextChild(ti,triChild[1])
            if VERBOSE>5:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'id2Find:%s not found'%id2Find,self)
            return None
        else:
            ti=self.tiCache.GetTreeItemById(id2Find)
            #vtLog.vtLngCurWX(vtLog.DEBUG,'ti:%s'%ti,self)
            if ti is not None:
                #return (ti,self.doc.getNodeByIdNum(self.GetPyData(ti)))
                if VERBOSE>5:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurWX(vtLog.DEBUG,'id2Find:%s found'%id2Find,self)
                return (ti,self.doc.getNodeByIdNum(long(id2Find)))    # 070205:wro
            #else:
            #    if vtLog.vtLngIsLogged(vtLog.DEBUG):
            #        vtLog.vtLngCurWX(vtLog.DEBUG,'cache:%s'%(vtLog.pformat(self.tiCache.cache.keys())),self)
            #self.__buildRequiredById__(self,id)
            if VERBOSE>5:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'id2Find:%s not found'%id2Find,self)
            return None
    def __buildRequiredById__(self,idReq,func,*args):
        if self.TREE_BUILD_ON_DEMAND:
            try:
                idReq=long(idReq)
                vtLog.vtLngCurWX(vtLog.INFO,'id:%s;idRootNode:%d'%(idReq,self.idRootNode),self)
                id=idReq
                l=[]#[idReq]
                node=self.doc.getNodeByIdNum(id)
                if node is None:
                    vtLog.vtLngCurWX(vtLog.ERROR,'id:%08d not found'%(id),self)
                    return False
                if self.rootNode is None:
                    nodeBase=self.GetBaseNode()
                else:
                    nodeBase=self.rootNode
                if nodeBase is None:
                    idBase=-1
                else:
                    idBase=self.doc.getKeyNum(nodeBase)
                while id>=0:
                    nodePar=self.doc.getParent(node)
                    if nodePar is not None:
                        idPar=self.doc.getKeyNum(nodePar)
                        if id<0:
                            break
                        if self.idRootNode==id:
                            break
                        if idBase==id:
                            break
                        if self.__is2skip__(nodePar,self.doc.getTagName(nodePar))==False:
                            l.append((idPar,id))
                        id=idPar
                        node=nodePar
                        #if self.tiCache.GetTreeItemById(id)==None:
                        #    if self.__is2skip__(nodePar,self.doc.getTagName(nodePar))==False:
                        #        l.append((idPar,id))
                        #    id=idPar
                        #    node=nodePar
                        #else:
                        #    if self.__is2skip__(nodePar,self.doc.getTagName(nodePar))==False:
                        #        l.append((idPar,id))
                        #    break
                        #if self.doc.isSameKeyNum(node,idBase):
                        #    break
                l.reverse()
                # 070117 check outside base node
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'l:%s;idBase:%d;idRoot:%d'%(vtLog.pformat(l),idBase,self.idRootNode),self)
                if len(l)>0:
                    if self.idRootNode!=l[0][0]:
                        vtLog.vtLngCurWX(vtLog.WARN,'root id missmatching;%d,%d'%(self.idRootNode,l[0][0]),self)
                        return False
                    iLevel=1
                    for idPar,id in l[:-1]:
                        #self.__addElements__(id,node)
                        #self.thdAddElements.Do(idPar,idPar,True)
                        #iLevel=self.__getTreeLevels__(tn)
                        if self.__findNodeByID__(None,id) is None:
                            self.DoSafe(self.thdProc.AddChildNodes,idPar,idPar,
                                    self.lang,iLevel,self.iLevelAutoBuild,
                                    self.iLevelLimit)
                        iLevel+=1
                        #self.thdAddElements.Do(idPar,id,True) #070109:wro
                        #self.thdAddElements.Do(idPar,idPar,True,
                        #        self.__expandOnDemandBuild,idPar)
                    idPar,id=l[-1]
                    #self.__addElements__(id,node,func,args)
                    #self.thdAddElements.Do(idPar,idPar,True,func,*args) #061229:wro
                    if self.__findNodeByID__(None,id) is None:
                        self.DoSafe(self.thdProc.AddChildNodes,idPar,idPar,
                                    self.lang,iLevel,self.iLevelAutoBuild,
                                    self.iLevelLimit)
                    self.thdProc.DoWX(self._do__SelectByID__,idReq)
                    #self.thdAddElements.Do(idPar,id,True,func,*args) #070109:wro
                    #self.thdAddElements.Do(idPar,id,True,func,*args)
                return True
            except:
                vtLog.vtLngTB(self.GetName())
        else:
            vtLog.vtLngCur(vtLog.CRITICAL,'build on demand disabled;you are not supposed to be here',self)
        return False
        #return True
    def __expandOnDemandBuild(self,id):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'id:%s'%(str(id)),self)
        tup=self.__findNodeByID__(None,id)
        if tup is not None:
            self.EnsureVisible(tup[0])
        else:
            vtLog.vtLngCurWX(vtLog.ERROR,'id:%s not found, should never happen!'%(str(id)),self)
    def __sortAllChilds__(self,ti):
        if ti is None:
            return
        triChild=self.GetFirstChild(ti)
        #while triChild[0].IsOk():
        #    triChild=self.GetNextChild(triChild[0],triChild[1])
        #    if triChild[0].IsOk():
        #        if self.IsExpanded(triChild[0]):
        #            self.__sortAllChilds__(triChild[0])
        while triChild[0].IsOk():
            if self.IsExpanded(triChild[0]):
                self.__sortAllChilds__(triChild[0])
            triChild=self.GetNextChild(ti,triChild[1])
        self.SortChildren(ti)
    def __sortAllChildsForced__(self,ti):
        if ti is None:
            return
        triChild=self.GetFirstChild(ti)
        while triChild[0].IsOk():
            self.__sortAllChildsForced__(triChild[0])
            triChild=self.GetNextChild(ti,triChild[1])
        self.SortChildren(ti)
    #def EnsureVisible(self,ti):
    #    vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
    #    wx.TreeCtrl.EnsureVisible(self,ti)
    def GetOrigin(self):
        return vtLog.vtLngGetRelevantNamesWX(self)
    #
    #
    #
    def __convImg__(self,imgInst,f):
        ar=array.array('B',imgInst.GetData())
        iLen=ar.buffer_info()[1]
        for i in xrange(0, iLen, 3):
            if ar[i]!=1:
                if ar[i+1]!=0:
                    if ar[i+2]!=0:
                        def chRgb(a):
                            a=int(a*f)
                            if a>254:
                                a=254
                            return a
                        ar[i]=chRgb(ar[i])
                        ar[i+1]=chRgb(ar[i+1]) 
                        ar[i+2]=chRgb(ar[i+2])
        imgInst.SetData(ar.tostring())
        return imgInst.ConvertToBitmap()
    def __mergeImg__(self,imgTmp,imgOverlay,mergeMask=None):
        arTmp=array.array('B',imgTmp.GetData())
        arOvl=array.array('B',imgOverlay.GetData())
        iLen=arTmp.buffer_info()[1]
        if arOvl.buffer_info()[1]!=iLen:
            vtLog.vtLngCur(vtLog.CRITICAL,'bitmap size mismatch',self)
        for i in xrange(0, iLen, 3):
            if arOvl[i]!=1 or arOvl[i+1]!=0 or arOvl[i+2]!=0:
                if mergeMask is not None:
                    if arOvl[i]==mergeMask[0] and arOvl[i+1]==mergeMask[1] and arOvl[i+2]==mergeMask[2]:
                        arTmp[i]=1
                        arTmp[i+1]=0
                        arTmp[i+2]=0
                        continue
                arTmp[i]=arOvl[i]
                arTmp[i+1]=arOvl[i+1] 
                arTmp[i+2]=arOvl[i+2]
        imgTmp.SetData(arTmp.tostring())
    def __buildGrpBmp__(self,bmp,imgOverlay):
        imgTmp=bmp.ConvertToImage()
        self.__mergeImg__(imgTmp,imgOverlay,mergeMask=[255,255,255])
        return imgTmp.ConvertToBitmap()
    def __convImg2Bmp__(self,imgInst):
        return imgInst.ConvertToBitmap()
    def __convImgInvalid__(self,imgInst,f):
        ar=array.array('B',imgInst.GetData())
        iLen=ar.buffer_info()[1]
        for i in xrange(0, iLen, 3):
            if ar[i]!=1:
                if ar[i+1]!=0:
                    if ar[i+2]!=0:
                        def chRgb(a):
                            a=int(a*f)
                            if a>254:
                                a=254
                            return a
                        ar[i]=chRgb(ar[i])
                        ar[i+1]=chRgb(ar[i+1]) 
                        ar[i+2]=chRgb(ar[i+2])
        imgInst.SetData(ar.tostring())
        xCenter=imgInst.GetWidth()/2
        yCenter=imgInst.GetHeight()/2
        if xCenter<yCenter:
            dW=xCenter
        else:
            dW=xCenter
        def set(x,y):
            try:
                imgInst.SetRGB(x,y,180,0,0)
            except:
                pass
        for i in range(dW-1):
            set(xCenter-i,yCenter-i)
            set(xCenter-i-1,yCenter-i)
            #set(xCenter-i+1,yCenter-i)
            
            set(xCenter+i,yCenter-i)
            set(xCenter+i-1,yCenter-i)
            #set(xCenter+i+1,yCenter-i)
            
            set(xCenter-i,yCenter+i)
            set(xCenter-i-1,yCenter+i)
            #set(xCenter-i+1,yCenter+i)
            
            set(xCenter+i,yCenter+i)
            set(xCenter+i-1,yCenter+i)
            #set(xCenter+i+1,yCenter+i)
        j=imgInst.GetHeight()-1
        for i in range(0,imgInst.GetWidth()):
            set(i,0)
            #set(i,1)
            set(i,j)
            #set(i,j-1)
        i=imgInst.GetWidth()-1
        for j in range(0,imgInst.GetHeight()):
            set(0,j)
            #set(1,j)
            set(i,j)
            #set(i-1,j)
        return imgInst.ConvertToBitmap()
    def __addElemImage2ImageList__(self,name,imgTmp,imgTmpSel,bAlpha=True):
        self.__addImage2ImageList__('elem',name,imgTmp,imgTmpSel,bAlpha)
    def __addImage2ImageList__(self,orig,name,imgTmp,imgTmpSel,bAlpha=True):
        if orig not in self.imgDict:
            self.imgDict[orig]={}
        if bAlpha:
            img=self.__convImg__(imgTmp.Copy(),self.MAP_IMG_ALPHA[0])
            imgSel=self.__convImg__(imgTmpSel.Copy(),self.MAP_IMG_ALPHA[1])
            
            self.imgDict[orig][name]=[self.imgLstTyp.Add(img)]
            self.imgDict[orig][name].append(self.imgLstTyp.Add(imgSel))
        
            img=self.__convImg__(imgTmp.Copy(),self.MAP_IMG_ALPHA[2])
            imgSel=self.__convImg__(imgTmpSel.Copy(),self.MAP_IMG_ALPHA[3])
            self.imgDict[orig][name].append(self.imgLstTyp.Add(img))
            self.imgDict[orig][name].append(self.imgLstTyp.Add(imgSel))
        
            img=self.__convImg__(imgTmp.Copy(),self.MAP_IMG_ALPHA[4])
            imgSel=self.__convImg__(imgTmpSel.Copy(),self.MAP_IMG_ALPHA[5])
            self.imgDict[orig][name].append(self.imgLstTyp.Add(img))
            self.imgDict[orig][name].append(self.imgLstTyp.Add(imgSel))
        else:
            img=self.__convImg2Bmp__(imgTmp)
            imgSel=self.__convImg2Bmp__(imgTmpSel)
            #img=imgTmp
            #imgSel=imgTmpSel
            imgIdx=self.imgLstTyp.Add(img)
            imgSelIdx=self.imgLstTyp.Add(imgSel)
            self.imgDict[orig][name]=[imgIdx]
            self.imgDict[orig][name].append(imgSelIdx)
            self.imgDict[orig][name].append(imgIdx)
            self.imgDict[orig][name].append(imgSelIdx)
            self.imgDict[orig][name].append(imgIdx)
            self.imgDict[orig][name].append(imgSelIdx)
    def SetupImageList(self):
        bRet=self.__setupImageList__()
        if bRet:
            #self.__extendImageListRegNodes__()
            self.__processImgList__()
        return bRet
    def GetImageDict(self):
        try:
            return self.imgDict
        except:
            self.__logTB__()
        return {}
    def GetImageList(self):
        try:
            return self.imgLstTyp
        except:
            self.__logTB__()
        return None
    def __setupImageList__(self):
        sImgOrigin=self.__class__.__name__
        global IMG_LIST
        global IMG_DICT
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'class:%s;inLstGrp:%d;IMG_LIST_GRP:%s;IMG_DICT_GRP:%s'%
                    (sImgOrigin,
                    IMG_LIST.has_key(sImgOrigin),
                    vtLog.pformat(IMG_LIST),
                    vtLog.pformat(IMG_DICT)),self)
        if not IMG_LIST.has_key(sImgOrigin):
            IMG_EXTENDED[sImgOrigin]=False
            self.imgDict={'elem':{},'txt':{},'cdata':{},'comm':{}}
            self.imgLstTyp=wx.ImageList(16,16)
            
            self.__addElemImage2ImageList__('dft',
                            images.getElemImage(),
                            images.getElemImage(),True)
            
            self.imgDict['grp']={}
            self.__addImage2ImageList__('grp','dft',
                            images.getElemImage(),
                            images.getElemImage(),True)
            self.__addImage2ImageList__('grp','YYYY',
                            images.getYearImage(),
                            images.getYearImage(),True)
            self.__addImage2ImageList__('grp','MM',
                            images.getMonthImage(),
                            images.getMonthImage(),True)
            self.__addImage2ImageList__('grp','DD',
                            images.getDayImage(),
                            images.getDayImage(),True)
            self.__addImage2ImageList__('grp','usr',
                            images.getUsrImage(),
                            images.getUsrImage(),True)
            self.__addImage2ImageList__('grp','person',
                            images.getUsrImage(),
                            images.getUsrImage(),True)
            
            self.__addImage2ImageList__('txt','dft',
                            images.getTextImage(),
                            images.getTextImage(),True)
            
            IMG_LIST[sImgOrigin]=self.imgLstTyp
            IMG_DICT[sImgOrigin]=self.imgDict
            self.SetImageList(self.imgLstTyp)
            return True
        else:
            self.imgLstTyp=IMG_LIST[sImgOrigin]
            self.imgDict=IMG_DICT[sImgOrigin]
            self.SetImageList(self.imgLstTyp)
            return False
    def __processImgList__(self):
        try:
            for k in self.imgDict.keys():
                d=self.imgDict[k]
                for kk in d.keys():
                    l=d[kk]
                    for i in range(len(l),6):
                        idx=i%2
                        img=self.imgLstTyp.GetBitmap(idx)
                        imgTmp=img.ConvertToImage()
                        bmp=self.__convImg__(imgTmp,self.MAP_IMG_ALPHA[i])
                        self.imgDict[k][kk].append(self.imgLstTyp.Add(bmp))
        except:
            #vtLog.vtLngTB(self.GetName())
            pass
    def __extendImageListRegNodes__(self):
        if self.doc is None:
            return
        global IMG_LIST
        global IMG_DICT
        global IMG_EXTENDED
        sImgOrigin=self.__class__.__name__
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'class:%s;inLstGrp:%d;IMG_LIST_GRP:%s;IMG_DICT_GRP:%s;IMG_EXTENDED_GRP:%s'%
                    (sImgOrigin,
                    IMG_LIST.has_key(sImgOrigin),
                    vtLog.pformat(IMG_LIST),
                    vtLog.pformat(IMG_DICT),
                    vtLog.pformat(IMG_EXTENDED)),self)
        if not IMG_EXTENDED.has_key(sImgOrigin):
            #vtLog.vtLngCurWX(vtLog.DEBUG,'key defined:%s'%(vtLog.pformat(IMG_EXTENDED_GRP[self.__class__.__name__])),self)
            return
        if IMG_EXTENDED[sImgOrigin]==False:
            try:
                regNodes=self.doc.GetRegisteredNodes()
                keys=regNodes.keys()
                vtLog.vtLngCurWX(vtLog.DEBUG,'keys:%s'%(vtLog.pformat(keys)),self)
                for k in keys:
                    o=regNodes[k]
                    try:
                        if o.HasGraphics()==False:
                            continue
                        #if o.IsSkip()==True:
                        #    continue
                        img=o.GetBitmap()
                        imgSel=o.GetSelBitmap()
                        
                        try:
                            iLen=self.imgDict['elem'][k]
                        except:
                            iLen=-1
                        if iLen==-1:
                            self.imgDict['elem'][k]=[self.imgLstTyp.Add(img)]
                        if iLen<=0:
                            self.imgDict['elem'][k].append(self.imgLstTyp.Add(imgSel))
                        if iLen<=1:
                            self.imgDict['elem'][k].append(self.imgLstTyp.Add(o.GetInstBitmap()))
                        if iLen<=2:
                            self.imgDict['elem'][k].append(self.imgLstTyp.Add(o.GetInstSelBitmap()))
                        if iLen<=3:
                            self.imgDict['elem'][k].append(self.imgLstTyp.Add(o.GetInvalidBitmap()))
                        if iLen<=4:
                            self.imgDict['elem'][k].append(self.imgLstTyp.Add(o.GetInvalidSelBitmap()))
                    except:
                        vtLog.vtLngTB(self.GetName())
                IMG_EXTENDED[sImgOrigin]=True
            except:
                pass
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'images:%s'%(vtLog.pformat(self.imgDict)),self)
            try:
                self.__processImgList__()
            except:
                vtLog.vtLngTB(self.GetName())
            self.SetImageList(self.imgLstTyp)
    # +++++++++++++++++++
    # FIXME move me to vtmTreeCtrl
    def GetSelection(self):
        self.__logCritical__('FIXME'%())
        return wx.TreeCtrl.GetSelection(self)
    def GetSelections(self):
        self.__logCritical__('FIXME'%())
        return wx.TreeCtrl.GetSelections(self)
    def __setTreeItemLst__(self,tr,ti,l):
        try:
            self.__logDebug__('l:%s'%(self.__logFmt__(l)))
            if l[0] is not None:
                tr.SetPyData(ti,l[0])
            i=0
            for v in l[1]:
                t=type(v)
                if t==types.TupleType:
                    tr.SetItemText(ti,v[0])
                    tr.SetItemImage(ti,v[1],which=wx.TreeItemIcon_Normal)
                elif t==types.IntType:
                    tr.SetItemImage(ti,v,which=wx.TreeItemIcon_Normal)
                else:
                    tr.SetItemText(ti,v)
                    #tr.SetItemImage(ti,self.IMG_EMPTY,i,which=wx.TreeItemIcon_Normal)
                i+=1
                break
            iLen=len(l)
            if iLen>2:
                self.__setValue__(tr,ti,l[2])
        except:
            self.__logTB__()
            self.__logError__('l:%s;l[0]:%s;l[1]:%s'%(self.__logFmt__(l),
                    self.__logFmt__(l[0]),self.__logFmt__(l[1])))
    def __setTreeItemTup__(self,tr,ti,t):
        pass
    def SetItem(self,ti,i,v):
        if i<0:
            self.SetPyData(ti,v)
            return
        t=type(v)
        if t==types.TupleType:
            self.SetItemText(ti,v[0])
            self.SetItemImage(ti,v[1],which=wx.TreeItemIcon_Normal)
        elif t==types.IntType:
            self.SetItemImage(ti,v,which=wx.TreeItemIcon_Normal)
        else:
            self.SetItemText(ti,v)
    def GetItem(self,ti,i):
        if i<0:
            return self.GetPyData(ti)
        t=(self.GetItemText(ti),self.GetItemImage(ti))
        return t
    def __setTreeItem__(self,tr,ti,val):
        t=type(val)
        self.__logDebug__('val:%s'%(self.__logFmt__(val)))
        if t==types.ListType:
            self.__setTreeItemLst__(tr,ti,val)
        elif t==types.TupleType:
            self.__setTreeItemTup__(tr,ti,val)
        #elif t==types.DictType:
        #    self.__setTreeItemDict__(tr,ti,val)
    def __setValue__(self,tr,tp,l,bRoot=False,bCheck4Reg=True):
        #if tp is not None:
        #    tr.DeleteChildren(tp)
        if bCheck4Reg==True:
            def check(ll):
                t=type(ll)
                if t==types.ListType:
                    try:
                        if type(ll[0])==types.DictType:
                            if '__reg' in ll[0]:
                                return True
                    except:
                        pass
                return False
        else:
            def check(ll):
                return True
        for ll in l:
            self.__logDebug__('ll:%s'%(self.__logFmt__(ll)))
            if check(ll)==False:
                continue
            if tp is None:
                ti=tr.AddRoot('')
            else:
                ti=tr.AppendItem(tp,'')
            if self.FORCE_CHILDREN:
                tr.SetItemHasChildren(ti,True)
            self.__setTreeItem__(tr,ti,ll)
            if tp is None:
                return
    def SetValue(self,l,ti=None,tiKey=None,bExpand=True,bCheck4Reg=True):
        try:
            #if self.IsMainThread()==False:
            #    return -2
            self.__logDebug__(''%())
            w=self
            self.thdProc.DoWX(self.Freeze)
            self.DeleteChildren(ti)
            self.thdProc.DoWX(self.__setValue__,w,ti,l,bCheck4Reg=bCheck4Reg)
            if bExpand==True:
                self.thdProc.DoWX(self.Expand,ti)
            self.thdProc.DoWX(self.Thaw)
            return
            #iCols=w.GetColumnCount()
            if idx==-1:
                w.DeleteAllItems()
                return self.__insert__(w,l,idx=idx)
            else:
                return self.__setValue__(w,l,idx=idx)
        except:
            self.__logTB__()
        return -1

