#----------------------------------------------------------------------------
# Name:         vtXmlWxGuiEvents.py
# Purpose:      
#   derived from vNetXmlWxGui
#
# Author:       Walter Obweger
#
# Created:      20091011
# CVS-ID:       $Id: vtXmlWxGuiEvents.py,v 1.2 2010/02/26 00:41:23 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from wx import NewEventType,PyEvent
import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.gui.vtgEvent import vtgEvent

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

vtnxEVT_NET_XML_OPEN_START=NewEventType()
def EVT_NET_XML_OPEN_START(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_OPEN_START,func)
def EVT_NET_XML_OPEN_START_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_OPEN_START)
class vtnxXmlOpenStart(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_OPEN_START(<widget_name>, xxx)
    """
    def __init__(self,netDoc):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.SetEventType(vtnxEVT_NET_XML_OPEN_START)
    def GetNetDoc(self):
        return self.netDoc
        
vtnxEVT_NET_XML_OPEN_OK=NewEventType()
def EVT_NET_XML_OPEN_OK(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_OPEN_OK,func)
def EVT_NET_XML_OPEN_OK_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_OPEN_OK)
class vtnxXmlOpenOk(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_OPEN_OK(<widget_name>, xxx)
    """
    def __init__(self,netDoc):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.SetEventType(vtnxEVT_NET_XML_OPEN_OK)
    def GetNetDoc(self):
        return self.netDoc

vtnxEVT_NET_XML_OPEN_FAULT=NewEventType()
def EVT_NET_XML_OPEN_FAULT(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_OPEN_FAULT,func)
def EVT_NET_XML_OPEN_FAULT_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_OPEN_FAULT)
class vtnxXmlOpenFault(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_OPEN_FAULT(<widget_name>, xxx)
    """
    def __init__(self,netDoc):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,netDoc.GetOrigin())
            except:
                pass
        self.SetEventType(vtnxEVT_NET_XML_OPEN_FAULT)
    def GetNetDoc(self):
        return self.netDoc

vtnxEVT_NET_XML_SYNCH_START=NewEventType()
def EVT_NET_XML_SYNCH_START(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_SYNCH_START,func)
def EVT_NET_XML_SYNCH_START_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_SYNCH_START)
class vtnxXmlSynchStart(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_SYNCH_START(<widget_name>, xxx)
    """
    def __init__(self,netDoc,count):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.count=count
        self.SetEventType(vtnxEVT_NET_XML_SYNCH_START)
    def GetCount(self):
        return self.count
    def GetNetDoc(self):
        return self.netDoc
        
vtnxEVT_NET_XML_SYNCH_PROC=NewEventType()
def EVT_NET_XML_SYNCH_PROC(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_SYNCH_PROC,func)
def EVT_NET_XML_SYNCH_PROC_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_SYNCH_PROC)
class vtnxXmlSynchProc(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_SYNCH_PROC(<widget_name>, xxx)
    """
    def __init__(self,netDoc,act,count):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        self.act=act
        self.count=count
        self.SetEventType(vtnxEVT_NET_XML_SYNCH_PROC)
    def GetAct(self):
        return self.act
    def GetCount(self):
        return self.count
    def GetNetDoc(self):
        return self.netDoc

vtnxEVT_NET_XML_SYNCH_FINISHED=NewEventType()
def EVT_NET_XML_SYNCH_FINISHED(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_SYNCH_FINISHED,func)
def EVT_NET_XML_SYNCH_FINISHED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_SYNCH_FINISHED)
class vtnxXmlSynchFinished(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_SYNCH_FINISHED(<widget_name>, xxx)
    """
    def __init__(self,netDoc,bChanged):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.bChanged=bChanged
        self.SetEventType(vtnxEVT_NET_XML_SYNCH_FINISHED)
    def GetNetDoc(self):
        return self.netDoc
    def IsChanged(self):
        return self.bChanged

vtnxEVT_NET_XML_SYNCH_ABORTED=NewEventType()
def EVT_NET_XML_SYNCH_ABORTED(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_SYNCH_ABORTED,func)
def EVT_NET_XML_SYNCH_ABORTED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_SYNCH_ABORTED)
class vtnxXmlSynchAborted(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_SYNCH_ABORTED(<widget_name>, xxx)
    """
    def __init__(self,netDoc,bChanged):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.bChanged=bChanged
        self.SetEventType(vtnxEVT_NET_XML_SYNCH_ABORTED)
    def GetNetDoc(self):
        return self.netDoc
    def IsChanged(self):
        return self.bChanged

vtnxEVT_NET_XML_CONNECT=NewEventType()
def EVT_NET_XML_CONNECT(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_CONNECT,func)
def EVT_NET_XML_CONNECT_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_CONNECT)
class vtnxXmlConnect(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_CONNECT(<widget_name>, xxx)
    """
    def __init__(self,netDoc):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.SetEventType(vtnxEVT_NET_XML_CONNECT)
    def GetNetDoc(self):
        return self.netDoc

vtnxEVT_NET_XML_CONNECTED=NewEventType()
def EVT_NET_XML_CONNECTED(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_CONNECTED,func)
def EVT_NET_XML_CONNECTED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_CONNECTED)
class vtnxXmlConnected(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_CONNECTED(<widget_name>, xxx)
    """
    def __init__(self,netDoc):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.SetEventType(vtnxEVT_NET_XML_CONNECTED)
    def GetNetDoc(self):
        return self.netDoc

vtnxEVT_NET_XML_CONNECT_FAULT=NewEventType()
def EVT_NET_XML_CONNECT_FAULT(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_CONNECT_FAULT,func)
def EVT_NET_XML_CONNECT_FAULT_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_CONNECT_FAULT)
class vtnxXmlConnectFault(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_CONNECT_FAULT(<widget_name>, xxx)
    """
    def __init__(self,netDoc):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.SetEventType(vtnxEVT_NET_XML_CONNECT_FAULT)
    def GetNetDoc(self):
        return self.netDoc


vtnxEVT_NET_XML_DISCONNECT=NewEventType()
def EVT_NET_XML_DISCONNECT(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_DISCONNECT,func)
def EVT_NET_XML_DISCONNECT_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_DISCONNECT)
class vtnxXmlDisConnect(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_DISCONNECT(<widget_name>, xxx)
    """
    def __init__(self,netDoc):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.SetEventType(vtnxEVT_NET_XML_DISCONNECT)
    def GetNetDoc(self):
        return self.netDoc

vtnxEVT_NET_XML_CONFIG=NewEventType()
def EVT_NET_XML_CONFIG(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_CONFIG,func)
def EVT_NET_XML_CONFIG_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_CONFIG)
class vtnxXmlConfig(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_CONFIG(<widget_name>, xxx)
    """
    def __init__(self,netDoc):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.SetEventType(vtnxEVT_NET_XML_CONFIG)
    def GetNetDoc(self):
        return self.netDoc


vtnxEVT_NET_XML_ADD_ELEMENT=NewEventType()
def EVT_NET_XML_ADD_ELEMENT(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_ADD_ELEMENT,func)
def EVT_NET_XML_ADD_ELEMENT_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_ADD_ELEMENT)
class vtnxXmlAddElement(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_ADD_ELEMENT(<widget_name>, xxx)
    """
    def __init__(self,netDoc,id):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,'%s;id:%s'%(
                            self.__class__.__name__,`id`),netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.id=id
        self.SetEventType(vtnxEVT_NET_XML_ADD_ELEMENT)
    def GetID(self):
        return self.id
    def GetNetDoc(self):
        return self.netDoc

vtnxEVT_NET_XML_CLOSED=NewEventType()
def EVT_NET_XML_CLOSED(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_CLOSED,func)
def EVT_NET_XML_CLOSED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_CLOSED)
class vtnxXmlClosed(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_CLOSED(<widget_name>, xxx)
    """
    def __init__(self,netDoc):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.SetEventType(vtnxEVT_NET_XML_CLOSED)
    def GetNetDoc(self):
        return self.netDoc

vtnxEVT_NET_XML_LOGIN=NewEventType()
def EVT_NET_XML_LOGIN(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_LOGIN,func)
def EVT_NET_XML_LOGIN_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_LOGIN)
class vtnxXmlLogin(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_LOGIN(<widget_name>, xxx)
    """
    def __init__(self,netDoc):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.SetEventType(vtnxEVT_NET_XML_LOGIN)
    def GetNetDoc(self):
        return self.netDoc

vtnxEVT_NET_XML_LOGGEDIN=NewEventType()
def EVT_NET_XML_LOGGEDIN(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_LOGGEDIN,func)
def EVT_NET_XML_LOGGEDIN_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_LOGGEDIN)
class vtnxXmlLoggedin(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_LOGGEDIN(<widget_name>, xxx)
    """
    def __init__(self,netDoc):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.SetEventType(vtnxEVT_NET_XML_LOGGEDIN)
    def GetNetDoc(self):
        return self.netDoc

vtnxEVT_NET_XML_BROWSE=NewEventType()
def EVT_NET_XML_BROWSE(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_BROWSE,func)
def EVT_NET_XML_BROWSE_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_BROWSE)
class vtnxXmlBrowse(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_BROWSE(<widget_name>, xxx)
    """
    def __init__(self,netDoc,l):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.l=l
        self.SetEventType(vtnxEVT_NET_XML_BROWSE)
    def GetNetDoc(self):
        return self.netDoc
    def GetList(self):
        return self.l

vtnxEVT_NET_XML_CMD=NewEventType()
def EVT_NET_XML_CMD(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_CMD,func)
def EVT_NET_XML_CMD_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_CMD)
class vtnxXmlCmd(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_CMD(<widget_name>, xxx)
    """
    def __init__(self,netDoc,action,res):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.action=action
        self.res=res
        self.SetEventType(vtnxEVT_NET_XML_CMD)
    def GetNetDoc(self):
        return self.netDoc
    def GetAction(self):
        return self.action
    def GetResult(self):
        return self.res

vtnxEVT_NET_XML_GOT_CONTENT=NewEventType()
def EVT_NET_XML_GOT_CONTENT(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_GOT_CONTENT,func)
def EVT_NET_XML_GOT_CONTENT_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_GOT_CONTENT)
class vtnxXmlGotContent(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_GOT_CONTENT(<widget_name>, xxx)
    """
    def __init__(self,netDoc):
        #vtLog.vtLngCurCls(vtLog.INFO,'',self)
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.SetEventType(vtnxEVT_NET_XML_GOT_CONTENT)
    def GetNetDoc(self):
        return self.netDoc

vtnxEVT_NET_XML_BUILD_TREE=NewEventType()
def EVT_NET_XML_BUILD_TREE(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_BUILD_TREE,func)
def EVT_NET_XML_BUILD_TREE_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_BUILD_TREE)
class vtnxXmlBuildTree(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_BUILD_TREE(<widget_name>, xxx)
    """
    def __init__(self,netDoc):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.SetEventType(vtnxEVT_NET_XML_BUILD_TREE)
    def GetNetDoc(self):
        return self.netDoc


vtnxEVT_NET_XML_LOCK=NewEventType()
def EVT_NET_XML_LOCK(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_LOCK,func)
def EVT_NET_XML_LOCK_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_LOCK)
class vtnxXmlLock(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_LOCK(<widget_name>, xxx)
    """
    def __init__(self,netDoc,id,response):
        PyEvent.__init__(self)
        self.SetEventObject(netDoc)
        self.SetId(netDoc.GetWidId())
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,'%s;id:%s;resp:%s'%(
                            self.__class__.__name__,`id`,`response`),netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.id=id
        self.response=response
        self.SetEventType(vtnxEVT_NET_XML_LOCK)
    def GetID(self):
        return self.id
    def GetResponse(self):
        return self.response
    def GetNetDoc(self):
        return self.netDoc

vtnxEVT_NET_XML_LOCK_BLOCK=NewEventType()
def EVT_NET_XML_LOCK_BLOCK(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_LOCK_BLOCK,func)
def EVT_NET_XML_LOCK_BLOCK_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_LOCK_BLOCK)
class vtnxXmlLockBlock(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_LOCK_BLOCK(<widget_name>, xxx)
    """
    def __init__(self,netDoc,id,zDiff):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,'%s;id:%s'%(
                            self.__class__.__name__,`id`),netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.id=id
        self.zDiff=zDiff
        self.SetEventType(vtnxEVT_NET_XML_LOCK_BLOCK)
    def GetID(self):
        return self.id
    def GetDiff(self):
        return self.zDiff
    def GetNetDoc(self):
        return self.netDoc

vtnxEVT_NET_XML_LOCK_REQUEST=NewEventType()
def EVT_NET_XML_LOCK_REQUEST(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_LOCK_REQUEST,func)
def EVT_NET_XML_LOCK_REQUEST_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_LOCK_REQUEST)
class vtnxXmlLockRequest(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_LOCK_REQUEST(<widget_name>, xxx)
    """
    def __init__(self,netDoc,id,sMsg,sKind,raw):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                vtLog.vtLngCS(vtLog.INFO,'%s;id:%s;kind:%s'%(
                        self.__class__.__name__,`id`,`sKind`),netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.id=id
        self.sMsg=sMsg
        self.sKind=sKind
        self.raw=raw
        self.SetEventType(vtnxEVT_NET_XML_LOCK_REQUEST)
    def GetID(self):
        return self.id
    def GetMsg(self):
        return self.sMsg
    def GetKind(self):
        return self.sKind
    def GetRaw(self):
        return self.raw
    def GetNetDoc(self):
        return self.netDoc

vtnxEVT_NET_XML_UNLOCK=NewEventType()
def EVT_NET_XML_UNLOCK(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_UNLOCK,func)
def EVT_NET_XML_UNLOCK_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_UNLOCK)
class vtnxXmlUnLock(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_UNLOCK(<widget_name>, xxx)
    """
    def __init__(self,netDoc,id,response):
        PyEvent.__init__(self)
        self.SetEventObject(netDoc)
        self.SetId(netDoc.GetWidId())
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,'%s;id:%s;resp:%s'%(
                            self.__class__.__name__,`id`,`response`),netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.id=id
        self.response=response
        self.SetEventType(vtnxEVT_NET_XML_UNLOCK)
    def GetID(self):
        return self.id
    def GetResponse(self):
        return self.response
    def GetNetDoc(self):
        return self.netDoc

vtnxEVT_NET_XML_GET_NODE=NewEventType()
def EVT_NET_XML_GET_NODE(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_GET_NODE,func)
def EVT_NET_XML_GET_NODE_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_GET_NODE)
class vtnxXmlGetNode(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_GET_NODE(<widget_name>, xxx)
    """
    def __init__(self,netDoc,id):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,'%s;id:%s'%(
                            self.__class__.__name__,`id`),netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.id=id
        self.SetEventType(vtnxEVT_NET_XML_GET_NODE)
    def GetID(self):
        return self.id
    def GetNetDoc(self):
        return self.netDoc

vtnxEVT_NET_XML_SET_NODE=NewEventType()
def EVT_NET_XML_SET_NODE(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_SET_NODE,func)
def EVT_NET_XML_SET_NODE_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_SET_NODE)
class vtnxXmlSetNode(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_SET_NODE(<widget_name>, xxx)
    """
    def __init__(self,netDoc,id,idNew,resp):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,'%s;id:%s;idNew:%s;resp:%s'%(
                            self.__class__.__name__,`id`,`idNew`,`resp`),netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.id=id
        self.idNew=idNew
        self.resp=resp
        self.SetEventType(vtnxEVT_NET_XML_SET_NODE)
    def GetID(self):
        return self.id
    def GetNewID(self):
        return self.idNew
    def GetResponse(self):
        return self.resp
    def GetNetDoc(self):
        return self.netDoc

vtnxEVT_NET_XML_ADD_NODE=NewEventType()
def EVT_NET_XML_ADD_NODE(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_ADD_NODE,func)
def EVT_NET_XML_ADD_NODE_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_ADD_NODE)
class vtnxXmlAddNode(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_ADD_NODE(<widget_name>, xxx)
    """
    def __init__(self,netDoc,id,idNew,content):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,'%s;id:%s;idNew:%s'%(
                            self.__class__.__name__,`id`,`idNew`),netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.id=id
        self.idNew=idNew
        self.content=content
        self.SetEventType(vtnxEVT_NET_XML_ADD_NODE)
    def GetID(self):
        return self.id
    def GetNewID(self):
        return self.idNew
    def GetContent(self):
        return self.content
    def GetNetDoc(self):
        return self.netDoc

vtnxEVT_NET_XML_ADD_NODE_RESPONSE=NewEventType()
def EVT_NET_XML_ADD_NODE_RESPONSE(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_ADD_NODE_RESPONSE,func)
def EVT_NET_XML_ADD_NODE_RESPONSE_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_ADD_NODE_RESPONSE)
class vtnxXmlAddNodeResponse(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_ADD_NODE_RESPONSE(<widget_name>, xxx)
    """
    def __init__(self,netDoc,id,idNew,resp=''):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,'%s;id:%s;idNew:%s;resp:%s'%(
                            self.__class__.__name__,`id`,`idNew`,`resp`),netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.id=id
        self.idNew=idNew
        self.resp=resp
        self.SetEventType(vtnxEVT_NET_XML_ADD_NODE_RESPONSE)
    def GetID(self):
        return self.id
    def GetNewID(self):
        return self.idNew
    def GetNetDoc(self):
        return self.netDoc
    def GetResponse(self):
        return self.resp

vtnxEVT_NET_XML_DEL_NODE=NewEventType()
def EVT_NET_XML_DEL_NODE(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_DEL_NODE,func)
def EVT_NET_XML_DEL_NODE_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_DEL_NODE)
class vtnxXmlDelNode(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_DEL_NODE(<widget_name>, xxx)
    """
    def __init__(self,netDoc,id,resp=''):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,'%s;id:%s;reps:%s'%(
                            self.__class__.__name__,`id`,`resp`),netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.id=id
        self.resp=resp
        self.SetEventType(vtnxEVT_NET_XML_DEL_NODE)
    def GetID(self):
        return self.id
    def GetNetDoc(self):
        return self.netDoc
    def GetResponse(self):
        return self.resp

vtnxEVT_NET_XML_REMOVE_NODE=NewEventType()
def EVT_NET_XML_REMOVE_NODE(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_REMOVE_NODE,func)
def EVT_NET_XML_REMOVE_NODE_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_REMOVE_NODE)
class vtnxXmlRemoveNode(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_REMOVE_NODE(<widget_name>, xxx)
    """
    def __init__(self,netDoc,id):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,'%s;id:%s'%(
                            self.__class__.__name__,`id`),netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.id=id
        self.SetEventType(vtnxEVT_NET_XML_REMOVE_NODE)
    def GetID(self):
        return self.id
    def GetNetDoc(self):
        return self.netDoc

vtnxEVT_NET_XML_MOVE_NODE=NewEventType()
def EVT_NET_XML_MOVE_NODE(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_MOVE_NODE,func)
def EVT_NET_XML_MOVE_NODE_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_MOVE_NODE)
class vtnxXmlMoveNode(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_MOVE_NODE(<widget_name>, xxx)
    """
    def __init__(self,netDoc,idPar,id):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,'%s;idPar:%s;id:%s'%(
                            self.__class__.__name__,`idPar`,`id`),netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.id=id
        self.idPar=idPar
        self.SetEventType(vtnxEVT_NET_XML_MOVE_NODE)
    def GetID(self):
        return self.id
    def GetParID(self):
        return self.idPar
    def GetNetDoc(self):
        return self.netDoc

vtnxEVT_NET_XML_MOVE_NODE_RESPONSE=NewEventType()
def EVT_NET_XML_MOVE_NODE_RESPONSE(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_MOVE_NODE_RESPONSE,func)
def EVT_NET_XML_MOVE_NODE_RESPONSE_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_MOVE_NODE_RESPONSE)
class vtnxXmlMoveNodeResponse(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_MOVE_NODE_RESPONSE(<widget_name>, xxx)
    """
    def __init__(self,netDoc,idPar,id,resp=''):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,'%s;idPar:%s;id:%s'%(
                            self.__class__.__name__,`idPar`,`id`),netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.id=id
        self.idPar=idPar
        self.resp=resp
        self.SetEventType(vtnxEVT_NET_XML_MOVE_NODE_RESPONSE)
    def GetID(self):
        return self.id
    def GetParID(self):
        return self.idPar
    def GetNetDoc(self):
        return self.netDoc
    def GetResponse(self):
        return self.resp

vtnxEVT_NET_XML_SETUP_CHANGED=NewEventType()
def EVT_NET_XML_SETUP_CHANGED(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_SETUP_CHANGED,func)
def EVT_NET_XML_SETUP_CHANGED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_SETUP_CHANGED)
class vtnxXmlSetupChanged(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_SETUP_CHANGED(<widget_name>, xxx)
    """
    def __init__(self,netDoc):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.SetEventType(vtnxEVT_NET_XML_SETUP_CHANGED)
    def GetNetDoc(self):
        return self.netDoc
    
vtnxEVT_NET_XML_CONTENT_CHANGED=NewEventType()
def EVT_NET_XML_CONTENT_CHANGED(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_CONTENT_CHANGED,func)
def EVT_NET_XML_CONTENT_CHANGED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_CONTENT_CHANGED)
class vtnxXmlContentChanged(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_CONTENT_CHANGED(<widget_name>, xxx)
    """
    def __init__(self,netDoc):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.SetEventType(vtnxEVT_NET_XML_CONTENT_CHANGED)
    def GetNetDoc(self):
        return self.netDoc

vtnxEVT_NET_XML_SELECT=NewEventType()
def EVT_NET_XML_SELECT(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_SELECT,func)
def EVT_NET_XML_SELECT_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_XML_SELECT)
class vtnxXmlSelect(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_SELECT(<widget_name>, xxx)
    """
    def __init__(self,netDoc,id):
        PyEvent.__init__(self)
        self.netDoc=netDoc
        if VERBOSE>5:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,'%s;id:%s'%(
                            self.__class__.__name__,`id`),netDoc.GetOrigin())
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        self.id=id
        self.SetEventType(vtnxEVT_NET_XML_SELECT)
    def GetID(self):
        return self.id
    def GetNetDoc(self):
        return self.netDoc

class vtnxXmlEvent(vtgEvent):
    _MAP_EVENT={
            'openStart':        EVT_NET_XML_OPEN_START,
            'openOk':           EVT_NET_XML_OPEN_OK,
            'openFault':        EVT_NET_XML_OPEN_FAULT,
            'synchStart':       EVT_NET_XML_SYNCH_START,
            'synchProc':        EVT_NET_XML_SYNCH_PROC,
            'synchFin':         EVT_NET_XML_SYNCH_FINISHED,
            'synchAbort':       EVT_NET_XML_SYNCH_ABORTED,
            'connect':          EVT_NET_XML_CONNECT,
            'connected':        EVT_NET_XML_CONNECTED,
            'connectFault':     EVT_NET_XML_CONNECT_FAULT,
            'disconnect':       EVT_NET_XML_DISCONNECT,
            'config':           EVT_NET_XML_CONFIG,
            'addElem':          EVT_NET_XML_ADD_ELEMENT,
            'closed':           EVT_NET_XML_CLOSED,
            'login':            EVT_NET_XML_LOGIN,
            'loggedin':         EVT_NET_XML_LOGGEDIN,
            'browse':           EVT_NET_XML_BROWSE,
            'cmd':              EVT_NET_XML_CMD,
            'gotContent':       EVT_NET_XML_GOT_CONTENT,
            'buildTree':        EVT_NET_XML_BUILD_TREE,
            'lock':             EVT_NET_XML_LOCK,
            'lockBlock':        EVT_NET_XML_LOCK_BLOCK,
            'lockReq':          EVT_NET_XML_LOCK_REQUEST,
            'unlock':           EVT_NET_XML_UNLOCK,
            'getNode':          EVT_NET_XML_GET_NODE,
            'setNode':          EVT_NET_XML_SET_NODE,
            'addNode':          EVT_NET_XML_ADD_NODE,
            'addNodeResp':      EVT_NET_XML_ADD_NODE_RESPONSE,
            'delNode':          EVT_NET_XML_DEL_NODE,
            'removeNode':       EVT_NET_XML_REMOVE_NODE,
            'moveNode':         EVT_NET_XML_MOVE_NODE,
            'moveNodeResp':     EVT_NET_XML_MOVE_NODE_RESPONSE,
            'setupChanged':     EVT_NET_XML_SETUP_CHANGED,
            'contentChanged':   EVT_NET_XML_CONTENT_CHANGED,
            'select':           EVT_NET_XML_SELECT,
            }#,dMapEvent)
