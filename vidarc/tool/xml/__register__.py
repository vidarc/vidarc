#----------------------------------------------------------------------------
# Name:         __register__.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060605
# CVS-ID:       $Id: __register__.py,v 1.1 2006/06/07 10:11:53 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

def Link(doc,oPar,tagName):
    try:
        oReg=doc.GetReg(tagName)
        if oReg is None:
            vtLog.vtLngCur(vtLog.ERROR,'node:%s is not registered.'%tagName,__name__)
        else:
            doc.LinkRegisteredNode(oPar,oReg)
    except:
        vtLog.vtLngTB(__name__)
