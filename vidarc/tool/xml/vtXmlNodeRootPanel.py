#Boa:FramePanel:vtXmlNodeRootPanel
#----------------------------------------------------------------------------
# Name:         vtXmlNodeRootPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060928
# CVS-ID:       $Id: vtXmlNodeRootPanel.py,v 1.1 2006/10/09 11:18:47 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputTextMultiLineML
import vidarc.tool.input.vtInputTextML
import vidarc.tool.input.vtInputText
import vidarc.tool.input.vtInputTextPopup
import wx.lib.buttons

import sys

from vidarc.tool.xml.vtXmlNodePanel import *
import vidarc.tool.lang.vtLgBase as vtLgBase

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt

[wxID_VTXMLNODEROOTPANEL, wxID_VTXMLNODEROOTPANELLBLID, 
 wxID_VTXMLNODEROOTPANELLBLIID, wxID_VTXMLNODEROOTPANELLBLNAME, 
 wxID_VTXMLNODEROOTPANELLBLTAG, wxID_VTXMLNODEROOTPANELLBLTYPE, 
 wxID_VTXMLNODEROOTPANELTXTTYPE, wxID_VTXMLNODEROOTPANELVINAME, 
 wxID_VTXMLNODEROOTPANELVITAG, 
] = [wx.NewId() for _init_ctrls in range(9)]

class vtXmlNodeRootPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)

    def _init_coll_bxsTagName_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblTag, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viTag, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblName, 1, border=4, flag=wx.EXPAND | wx.LEFT)
        parent.AddWindow(self.viName, 2, border=4, flag=wx.EXPAND | wx.LEFT)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsTagName, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsType, 1, border=4, flag=wx.TOP | wx.EXPAND)

    def _init_coll_bxsType_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblType, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtType, 2, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblID, 1, border=0,
              flag=wx.EXPAND | wx.LEFT | wx.RIGHT)
        parent.AddWindow(self.lblIID, 1, border=0,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT)

    def _init_coll_bxsDesc_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(None, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(None, 4, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=2, vgap=0)

        self.bxsTagName = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsType = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsDesc = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsTagName_Items(self.bxsTagName)
        self._init_coll_bxsType_Items(self.bxsType)
        self._init_coll_bxsDesc_Items(self.bxsDesc)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTXMLNODEROOTPANEL,
              name=u'vtXmlNodeRootPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(323, 88), style=wx.TAB_TRAVERSAL)
        self.SetMinSize(wx.Size(-1, -1))
        self.SetClientSize(wx.Size(315, 61))

        self.lblTag = wx.StaticText(id=wxID_VTXMLNODEROOTPANELLBLTAG,
              label=_(u'Tag'), name=u'lblTag', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(63, 30), style=wx.ALIGN_RIGHT)
        self.lblTag.SetMinSize(wx.Size(-1, -1))

        self.viTag = vidarc.tool.input.vtInputTextPopup.vtInputTextPopup(id=wxID_VTXMLNODEROOTPANELVITAG,
              name=u'viTag', parent=self, pos=wx.Point(67, 0), size=wx.Size(59,
              30), style=0)
        self.viTag.SetMinSize(wx.Size(-1, -1))

        self.lblName = wx.StaticText(id=wxID_VTXMLNODEROOTPANELLBLNAME,
              label=_(u'Name'), name=u'lblName', parent=self, pos=wx.Point(130,
              0), size=wx.Size(59, 30), style=wx.ALIGN_RIGHT)
        self.lblName.SetMinSize(wx.Size(-1, -1))

        self.viName = vidarc.tool.input.vtInputTextML.vtInputTextML(id=wxID_VTXMLNODEROOTPANELVINAME,
              name=u'viName', parent=self, pos=wx.Point(193, 0),
              size=wx.Size(122, 30), style=0)

        self.lblType = wx.StaticText(id=wxID_VTXMLNODEROOTPANELLBLTYPE,
              label=_(u'Type'), name=u'lblType', parent=self, pos=wx.Point(0,
              34), size=wx.Size(63, 21), style=wx.ALIGN_RIGHT)
        self.lblType.SetMinSize(wx.Size(-1, -1))

        self.lblID = wx.TextCtrl(id=wxID_VTXMLNODEROOTPANELLBLID, name=u'lblID',
              parent=self, pos=wx.Point(189, 34), size=wx.Size(63, 21),
              style=wx.ALIGN_RIGHT | wx.NO_BORDER, value=u'00000000')
        self.lblID.Enable(False)
        self.lblID.SetMinSize(wx.Size(-1, -1))

        self.lblIID = wx.TextCtrl(id=wxID_VTXMLNODEROOTPANELLBLIID,
              name=u'lblIID', parent=self, pos=wx.Point(252, 34),
              size=wx.Size(63, 21), style=wx.ALIGN_RIGHT | wx.NO_BORDER,
              value=u'00000000')
        self.lblIID.Enable(False)
        self.lblIID.SetMinSize(wx.Size(-1, -1))

        self.txtType = wx.TextCtrl(id=wxID_VTXMLNODEROOTPANELTXTTYPE,
              name=u'txtType', parent=self, pos=wx.Point(67, 34),
              size=wx.Size(122, 21), style=0, value=u'')
        self.txtType.Enable(False)
        self.txtType.SetMinSize(wx.Size(-1, -1))

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self._init_ctrls(parent,id)
        vtXmlNodePanel.__init__(self,lWidgets=[])
        
        self.viTag.SetTagName('tag')
        self.viName.SetTagName('name')
        
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
        self.fgsData.Layout()
        #self.fgsData.FitInside(self)
        
    def Clear(self):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.Clear(self)
        
        # add code here
        self.viTag.Clear()
        self.viName.Clear()
        self.txtType.SetValue('')
        self.lblID.SetValue(u'')
        self.lblIID.SetValue(u'')
        
        self.ClrBlockDelayed()
    def SetNetDocs(self,d):
        # add code here
        pass
        
    def SetDoc(self,doc,bNet=False):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        
        # add code here
        self.viTag.SetDoc(doc)
        self.viName.SetDoc(doc)
        
    def SetNode(self,node):
        try:
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            # add code here
            self.viTag.SetNode(node)
            self.viName.SetNode(node)
            sTag=self.doc.getTagName(node)
            o=self.doc.GetRegisteredNode(sTag)
            if o is not None:
                self.txtType.SetValue(o.GetDescription())
            else:
                self.txtType.SetValue(' '.join([sTag,'?']))
            try:
                iId=long(self.doc.getKey(node))
                if iId>=0:
                    self.lblID.SetValue(self.doc.ConvertIdNum2Str(iId))
                else:
                    self.lblID.SetValue(str(iId))
            except:
                self.lblID.SetValue(u'')
            try:
                self.lblIID.SetValue(self.doc.ConvertIdNum2Str(self.doc.getKeyInst(node)))
            except:
                self.lblIID.SetValue(u'')
        except:
            vtLog.vtLngTB(self.GetName())
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            if node is None:
                return
            try:
                # add code here
                self.viTag.GetNode(node)
                self.viName.GetNode(node)
            except:
                vtLog.vtLngTB(self.GetName())
            self.doc.AlignNode(node)
            self.GetNodeFin(node)
        except:
            vtLog.vtLngTB(self.GetName())
    def Cancel(self):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.Cancel(self)
        # add code here
        self.viTag.Clear()
        self.viName.Clear()
    def Close(self):
        # add code here
        self.viTag.Close()
        self.viName.Close()
    def Lock(self,flag):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if flag:
            # add code here
            self.Close()
            self.viTag.Enable(False)
            self.viName.Enable(False)
        else:
            # add code here
            self.viTag.Enable(True)
            self.viName.Enable(True)
