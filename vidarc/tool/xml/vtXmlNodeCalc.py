#----------------------------------------------------------------------------
# Name:         vtXmlNodeCalc.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060509
# CVS-ID:       $Id: vtXmlNodeCalc.py,v 1.10 2008/02/02 13:44:25 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vtXmlNodeCalc(vtXmlNodeBase):
    NODE_ATTRS=[
            ('formel',None,'formel',None),
        ]
    FUNCS_GET_SET_4_LST=['Formel']
    def __init__(self,tagName='calc'):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'calculation')
    # ---------------------------------------------------------
    # specific
    def GetFormel(self,node):
        l=[]
        for c in self.doc.getChilds(node,'formel'):
            l.append(self.doc.getText(c))
        return l
    def SetFormel(self,node,val):
        for c in self.doc.getChilds(node,'formel'):
            self.doc.deleteNode(c,node)
        for s in val:
            c=self.doc.createSubNodeText(node,'formel',s)
    def Calc(self,val,vals,lFormel,dValMap=None):
        try:
            bMap=False
            if dValMap is not None:
                bMap=True
            if lFormel is None:
                return val
            elif len(lFormel)==0:
                return val
            else:
                lRes=[val]
                iAct=1
                for formel in lFormel:
                    #print lRes
                    #print lFormel
                    i=0
                    while i>=0:
                        i=formel.find('$',i)
                        if i>=0:
                            j=formel.find('$',i+1)
                            if j>=0:
                                tmp=formel[i+1:j]
                                #print 'tmp',tmp,type(tmp)
                                try:
                                    #iLen=len(tmp)
                                    try:
                                        idx=int(tmp)
                                        tmp='u"'+unicode(lRes[idx])+'"'
                                        #tmp=u'"'+lRes[idx]+u'"'
                                    except:
                                        if bMap:
                                            if tmp in dValMap:
                                                tmp='u"'+unicode(vals[dValMap[tmp]])+'"'
                                            else:
                                                tmp=u'u""'
                                        else:
                                            if tmp in vals:
                                                tmp='u"'+unicode(vals[tmp])+'"'
                                                #tmp=u'"'+vals[tmp]+u'"'
                                            else:
                                                tmp=u'u""'
                                    #print 'formel',formel
                                    #print 'formel',formel,type(formel)
                                except:
                                    vtLog.vtLngTB(self.__class__.__name__)
                                    tmp=u'u"*****"'
                                formel=formel[:i]+tmp+formel[j+1:]
                    lRes.append(unicode(eval(formel)))
                    #sRes=eval(formel)
                    #print sRes,type(sRes)
                    #lRes.append(eval(formel))
                return lRes[-1]
        except:
            vtLog.vtLngTB(self.__class__.__name__)
            return u'*****'
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        return True
    def Is2Add(self):
        return True
    def IsMultiple(self):
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return False
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x1cIDAT8\x8dcddbf\xa0\x040Q\xa4{\xd4\x80Q\x03F\r\x18D\x06\x00\x00]b\
\x00&\x87\xd5\x92\xeb\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return vNodeXXXEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return vNodeXXXAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vNodeXXXPanel
        else:
            return None

