#----------------------------------------------------------------------------
# Name:         vtXmlNodeAddDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060420
# CVS-ID:       $Id: vtXmlNodeAddDialog.py,v 1.5 2010/03/03 02:16:18 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.log.vtLog import vtLogOriginWX

class vtXmlNodeAddDialog(vtLogOriginWX):
    def __init__(self):
        vtLogOriginWX.__init__(self)
        if hasattr(self,'pn')==False:
            self.pn=None
    def SetRegNode(self,obj):
        self.pn.SetRegNode(obj)
    def SetDoc(self,doc,bNet=False):
        self.pn.SetDoc(doc,bNet)
    def SetNode(self,objReg,nodePar,node):
        self.objReg=objReg
        self.nodePar=nodePar
        self.pn.SetNode(node)
    def GetNode(self,node=None):
        self.pn.GetNode(node)
    def OnCbApplyButton(self, event):
        event.Skip()
        try:
            self.pn.Close()
            if self.IsModal():
                self.EndModal(1)
            else:
                node=self.objReg.Create(self.nodePar)
                self.pn.GetNode(node)
                self.pn.Clear()
                self.Show(False)
        except:
            self.__logTB__()
    def OnCbCancelButton(self, event):
        event.Skip()
        try:
            self.pn.Close()
            if self.IsModal():
                self.EndModal(0)
            else:
                self.Show(False)
            self.pn.Clear()
        except:
            self.__logTB__()
    def __getattr__(self,name):
        if hasattr(self,'pn'):
            if hasattr(self.pn,name):
                return getattr(self.pn,name)
        raise AttributeError(name)
