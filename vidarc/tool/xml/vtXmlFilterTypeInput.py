#----------------------------------------------------------------------------
# Name:         vtXmlFilterTypeInput.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060223
# CVS-ID:       $Id: vtXmlFilterTypeInput.py,v 1.7 2007/07/28 14:43:34 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

from vidarc.tool.xml.vtXmlFilterType import *
from vidarc.tool.time.vtTimeTimeEdit import vtTimeTimeEdit
from vidarc.tool.time.vtTimeDateGM import vtTimeDateGM
from vidarc.tool.time.vtTimeInputDateTime import vtTimeInputDateTime
import vidarc.tool.log.vtLog as vtLog
import types

wxEVT_VTXML_FILTER_TYPE_CHANGED=wx.NewEventType()
vEVT_VTXML_FILTER_TYPE_CHANGED=wx.PyEventBinder(wxEVT_VTXML_FILTER_TYPE_CHANGED,1)
def EVT_VTXML_FILTER_TYPE_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTXML_FILTER_TYPE_CHANGED,func)
class vtXmlFilterTypeChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTXML_FILTER_TYPE_CHANGED(<widget_name>, self.OnInfoApplied)
    """

    def __init__(self,obj,val):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VTXML_FILTER_TYPE_CHANGED)
        self.obj=obj
        self.val=val
    def GetObject(self):
        return self.obj
    def GetValue(self):
        return self.val

class vtXmlFilterOperator(wx.Panel):
    def __init__(self,*_args,**_kwargs):
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        self.SetAutoLayout(True)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.chcOp=wx.Choice(choices=['!=','==','<','<=','>=','>'], id=-1,
              name='chcOp', parent=self, style=0)
        self.chcOp.SetSelection(1)
        self.chcOp.Bind(wx.EVT_CHOICE, self.OnChoice,self.chcOp)
        bxs.AddWindow(self.chcOp, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        self.SetSizer(bxs)
        self.bBlock=False
        self.bEnableMark=True
    def SetEnableMark(self,flag):
        self.bEnableMark=flag
    def __markModified__(self,flag=True):
        if self.bEnableMark==False:
            return
        if flag:
            f=self.chcOp.GetFont()
            f.SetWeight(wx.FONTWEIGHT_BOLD)
            self.chcOp.SetFont(f)
        else:
            f=self.chcOp.GetFont()
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
            self.chcOp.SetFont(f)
        self.chcOp.Refresh()
    def __clearBlock__(self):
        self.bBlock=False
    def SetValue(self,val):
        self.bBlock=True
        self.chcOp.SetSelection(val)
        wx.CallAfter(self.__clearBlock__)
    def GetValue(self):
        return self.chcOp.GetSelection()
    def OnChoice(self,evt):
        if self.bBlock:
            return
        sVal=self.chcOp.GetSelection()
        self.__markModified__()
        wx.PostEvent(self,vtXmlFilterTypeChanged(self,sVal))

class vtXmlFilterStringInput(wx.Panel):
    def __init__(self,*_args,**_kwargs):
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        self.SetAutoLayout(True)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.txtVal = wx.TextCtrl(id=-1, name='txtVal',
              parent=self, style=0, value='')
        bxs.AddWindow(self.txtVal, 3, border=0, flag=wx.EXPAND|wx.ALL)
        self.txtVal.Bind(wx.EVT_TEXT, self.OnTextText,self.txtVal)
        
        self.chcOp=wx.Choice(choices=['!=','==','<','<=','>=','>'], id=-1,
              name='chcOp', parent=self, style=0)
        self.chcOp.SetSelection(1)
        self.chcOp.Bind(wx.EVT_CHOICE, self.OnChoice,self.chcOp)
        bxs.AddWindow(self.chcOp, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        self.ckcCase=wx.CheckBox(id=-1, label=_('case sensitive'),
              name='ckcCase', parent=self, style=0)
        self.ckcCase.SetValue(True)
        bxs.AddWindow(self.ckcCase, 2, border=0, flag=wx.EXPAND|wx.ALL)
        
        self.SetSizer(bxs)
        self.Layout()
        self.bBlock=False
        self.bEnableMark=True
    def SetEnableMark(self,flag):
        self.bEnableMark=flag
    def __markModified__(self,flag=True):
        if self.bEnableMark==False:
            return
        if flag:
            f=self.txtVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_BOLD)
            self.txtVal.SetFont(f)
            #f=self.chcOp.GetFont()
            #f.SetWeight(wx.FONTWEIGHT_BOLD)
            #self.chcOp.SetFont(f)
        else:
            f=self.txtVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
            self.txtVal.SetFont(f)
            #f=self.chcOp.GetFont()
            #f.SetWeight(wx.FONTWEIGHT_NORMAL)
            #self.chcOp.SetFont(f)
        self.txtVal.Refresh()
        #self.chcOp.Refresh()
    def __markFlt__(self,flag=False):
        if flag:
            color=wx.TheColourDatabase.Find('YELLOW')
            self.txtVal.SetBackgroundColour(color)
        else:
            color=wx.SystemSettings_GetColour(wx.SYS_COLOUR_WINDOW)
            self.txtVal.SetBackgroundColour(color)
        if self.bEnableMark==False:
            self.txtVal.Refresh()
    def __clearBlock__(self):
        self.bBlock=False
    def SetValue(self,val,op=-1):
        self.bBlock=True
        bOk,bMod,val=self.__validate__(val,True)
        self.__markModified__( not bMod)
        self.__markFlt__(not bOk)
        self.txtVal.SetValue(val)
        if op>=0:
            self.chcOp.SetSelection(op)
        wx.CallAfter(self.__clearBlock__)
    def GetValueStr(self):
        return self.txtVal.GetValue()
    def GetValue(self):
        return self.txtVal.GetValue(),self.chcOp.GetSelection()
    def SetFilter(self,flt):
        try:
            self.bBlock=True
            self.txtVal.SetValue(flt.GetValue())
            self.chcOp.SetSelection(flt.GetOp())
            self.ckcCase.SetValue(flt.GetSensitive())
            self.__markModified__(False)
        except:
            vtLog.vtLngTB(self.GetName())
        wx.CallAfter(self.__clearBlock__)
    def GetFilter(self):
        return vtXmlFilterString(self.txtVal.GetValue(),
                self.chcOp.GetSelection(),self.ckcCase.GetValue())
    def __validate__(self,val,bHandleFlt=True):
        return True,False,val
    def OnTextText(self,evt):
        if self.bBlock:
            return
        sVal=self.txtVal.GetValue()
        try:
            bOk,bMod,val=self.__validate__(sVal,False)
            self.__markFlt__(not bOk)
            if bMod:
                self.txtVal.SetValue(val)
        except:
            #vtLog.vtLngTB(self.GetName())
            pass
        self.__markModified__()
        wx.PostEvent(self,vtXmlFilterTypeChanged(self,sVal))
    def OnChoice(self,evt):
        if self.bBlock:
            return
        sVal=self.chcOp.GetSelection()
        sValFlt=self.txtVal.GetValue()
        bWildCard=False
        if sValFlt.find('*')>=0:
            bWildCard=True
        elif sValFlt.find('*')>=0:
            bWildCard=True
        if bWildCard:
            if sVal>1:
                self.chcOp.SetSelection(1)
        #self.__markModified__()

class vtXmlFilterIntegerInput(vtXmlFilterStringInput):
    def __init__(self,*_args,**_kwargs):
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        self.SetAutoLayout(True)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.txtVal = wx.TextCtrl(id=-1, name='txtVal',
              parent=self, style=0, value='')
        bxs.AddWindow(self.txtVal, 3, border=0, flag=wx.EXPAND|wx.ALL)
        self.txtVal.Bind(wx.EVT_TEXT, self.OnTextText,self.txtVal)
        
        self.chcOp=wx.Choice(choices=['!=','==','<','<=','>=','>'], id=-1,
              name='chcOp', parent=self, style=0)
        self.chcOp.SetSelection(1)
        self.chcOp.Bind(wx.EVT_CHOICE, self.OnChoice,self.chcOp)
        bxs.AddWindow(self.chcOp, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        self.SetSizer(bxs)
        self.Layout()
        self.bBlock=False
        self.bEnableMark=True
    def __validate__(self,val,bHandleFlt=True):
        try:
            if type(int(val))!=types.IntType:
                if bHandleFlt:
                    return False,True,'0'
                else:
                    return False,False,str(val)
            return True,False,str(val)
        except:
            if bHandleFlt:
                return False,True,'0'
            else:
                return False,False,str(val)
    def SetFilter(self,flt):
        try:
            self.bBlock=True
            self.txtVal.SetValue(flt.GetValueStr())
            self.chcOp.SetSelection(flt.GetOp())
            self.__markModified__(False)
        except:
            vtLog.vtLngTB(self.GetName())
        wx.CallAfter(self.__clearBlock__)
    def GetFilter(self):
        return vtXmlFilterInt(self.txtVal.GetValue(),self.chcOp.GetSelection())
    
class vtXmlFilterLongInput(vtXmlFilterIntegerInput):
    def __validate__(self,val,bHandleFlt=True):
        try:
            if type(long(val))!=types.LongType:
                if bHandleFlt:
                    return False,True,'0'
                else:
                    return False,False,str(val)
            return True,False,str(val)
        except:
            if bHandleFlt:
                return False,True,'0'
            else:
                return False,False,str(val)
    def GetFilter(self):
        return vtXmlFilterLong(self.txtVal.GetValue(),self.chcOp.GetSelection())

class vtXmlFilterFloatInput(vtXmlFilterIntegerInput):
    def __init__(self,*_args,**_kwargs):
        try:
            self.fmt=_kwargs['format']
            del _kwargs['format']
        except:
            self.fmt='%3.1f'
        vtXmlFilterIntegerInput.__init__(self,*_args,**_kwargs)
    def __validate__(self,val,bHandleFlt=True):
        #print val,self.fmt%float(val),type(float(val))
        try:
            sVal=self.fmt%float(val)
            if type(float(val))!=types.FloatType:
                if bHandleFlt:
                    return False,True,self.fmt%0.0
                else:
                    return False,False,str(val)
            return True,val!=sVal,sVal
        except:
            if bHandleFlt:
                return False,True,self.fmt%0.0
            else:
                return False,False,val
    def SetFilter(self,flt):
        try:
            self.bBlock=True
            self.txtVal.SetValue(self.fmt%flt.GetValue())
            self.chcOp.SetSelection(flt.GetOp())
            self.__markModified__(False)
        except:
            vtLog.vtLngTB(self.GetName())
        wx.CallAfter(self.__clearBlock__)
    def GetFilter(self):
        return vtXmlFilterFloat(self.txtVal.GetValue(),self.chcOp.GetSelection())

class vtXmlFilterTimeInput(wx.Panel):
    def __init__(self,*_args,**_kwargs):
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        self.SetAutoLayout(True)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.viTime = vtTimeTimeEdit(id=-1, name='txtVal',
              parent=self, style=0)
        bxs.AddWindow(self.viTime, 3, border=0, flag=wx.EXPAND|wx.ALL)
        #self.txtVal.Bind(wx.EVT_TEXT, self.OnTextText,self.txtVal)
        
        self.chcOp=wx.Choice(choices=['!=','==','<','<=','>=','>'], id=-1,
              name='chcOp', parent=self, style=0)
        self.chcOp.SetSelection(1)
        #self.chcOp.Bind(wx.EVT_CHOICE, self.OnChoice,self.chcOp)
        bxs.AddWindow(self.chcOp, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        self.SetSizer(bxs)
        self.Layout()
        self.bBlock=False
        self.bEnableMark=True
    def SetEnableMark(self,flag):
        self.bEnableMark=flag
        self.viTime.SetEnableMark(flag)
    def SetValue(self,val,op=-1):
        self.bBlock=True
        self.viTime.SetValue(val)
        if op>=0:
            self.chcOp.SetSelection(op)
    def GetValueStr(self):
        return self.viTime.GetValueStr()
    def SetFilter(self,flt):
        try:
            self.bBlock=True
            self.viTime.SetValueStr(flt.GetValue())
            self.chcOp.SetSelection(flt.GetOp())
            #self.__markModified__(False)
        except:
            vtLog.vtLngTB(self.GetName())
        #wx.CallAfter(self.__clearBlock__)
    def GetFilter(self):
        sVal=self.viTime.GetValue()
        sVal=sVal[0:2]+sVal[3:5]+sVal[6:8]
        return vtXmlFilterTime(sVal,self.chcOp.GetSelection())
    def GetValue(self):
        try:
            sVal=self.viTime.GetValue()
            return sVal[0:2]+sVal[3:5]+sVal[6:8],self.chcOp.GetSelection()
        except:
            return '120000',self.chcOp.GetSelection()
    def OnTextText(self,evt):
        if self.bBlock:
            return
        sVal=self.txtVal.GetValue()
        #if len(sVal)<8:
        #    evt.Skip()
            #return
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'val:%s'%(sVal),self)

        try:
            bOk,bMod,val=self.__validate__(sVal,False,bTxt=True)
            self.__markFlt__(not bOk)
            #if bMod:
            #    self.txtVal.SetValue(val)
        except:
            vtLog.vtLngTB(self.GetName())
        self.__markModified__()
        wx.PostEvent(self,vtXmlFilterTypeChanged(self,sVal))

class vtXmlFilterDateInput(wx.Panel):
    def __init__(self,*_args,**_kwargs):
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        self.SetAutoLayout(True)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.viDate = vtTimeDateGM(id=-1, name='txtVal',
              parent=self, style=0)
        bxs.AddWindow(self.viDate, 3, border=0, flag=wx.EXPAND|wx.ALL)
        #self.txtVal.Bind(wx.EVT_TEXT, self.OnTextText,self.txtVal)
        
        self.chcOp=wx.Choice(choices=['!=','==','<','<=','>=','>'], id=-1,
              name='chcOp', parent=self, style=0)
        self.chcOp.SetSelection(1)
        #self.chcOp.Bind(wx.EVT_CHOICE, self.OnChoice,self.chcOp)
        bxs.AddWindow(self.chcOp, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        self.SetSizer(bxs)
        self.Layout()
        self.bBlock=False
        self.bEnableMark=True
    def SetEnableMark(self,flag):
        self.bEnableMark=flag
        self.viDate.SetEnableMark(flag)
    def SetValue(self,val,op=-1):
        self.bBlock=True
        self.viDate.SetValue(val)
        if op>=0:
            self.chcOp.SetSelection(op)
    def GetValueStr(self):
        return self.viDate.GetValueStr()
    def SetFilter(self,flt):
        try:
            self.bBlock=True
            self.viDate.SetValueStr(flt.GetValue())
            self.chcOp.SetSelection(flt.GetOp())
            #self.__markModified__(False)
        except:
            vtLog.vtLngTB(self.GetName())
        #wx.CallAfter(self.__clearBlock__)
    def GetFilter(self):
        sVal=self.viDate.GetValue()
        return vtXmlFilterDate(sVal,self.chcOp.GetSelection())
    def GetValue(self):
        try:
            sVal=self.viDate.GetValue()
            return sVal,self.chcOp.GetSelection()
        except:
            return '20010101',self.chcOp.GetSelection()
    def OnTextText(self,evt):
        if self.bBlock:
            return
        sVal=self.txtVal.GetValue()
        try:
            bOk,bMod,val=self.__validate__(sVal,False,bTxt=True)
            self.__markFlt__(not bOk)
            if bMod:
                self.txtVal.SetValue(val)
        except:
            vtLog.vtLngTB(self.GetName())
        self.__markModified__()
        wx.PostEvent(self,vtXmlFilterTypeChanged(self,sVal))

class vtXmlFilterDateTimeInput(wx.Panel):
    def __init__(self,*_args,**_kwargs):
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        self.SetAutoLayout(True)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.viDateTime = vtTimeInputDateTime(id=-1, name='txtVal',
              parent=self, style=0)
        bxs.AddWindow(self.viDateTime, 3, border=0, flag=wx.EXPAND|wx.ALL)
        #self.txtVal.Bind(wx.EVT_TEXT, self.OnTextText,self.txtVal)
        
        self.chcOp=wx.Choice(choices=['!=','==','<','<=','>=','>'], id=-1,
              name='chcOp', parent=self, style=0)
        self.chcOp.SetSelection(1)
        #self.chcOp.Bind(wx.EVT_CHOICE, self.OnChoice,self.chcOp)
        bxs.AddWindow(self.chcOp, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        self.SetSizer(bxs)
        self.Layout()
        self.bBlock=False
        self.bEnableMark=True
    def SetEnableMark(self,flag):
        self.bEnableMark=flag
        self.viDateTime.SetEnableMark(flag)
    def SetValue(self,val,op=-1):
        self.bBlock=True
        self.viDateTime.SetValue(val)
        if op>=0:
            self.chcOp.SetSelection(op)
    def GetValueStr(self):
        return self.viDateTime.GetValue()
    def SetFilter(self,flt):
        try:
            self.viDateTime.SetValue(flt.GetValue())
            self.chcOp.SetSelection(flt.GetOp())
        except:
            vtLog.vtLngTB(self.GetName())
    def GetFilter(self):
        sVal=self.viDateTime.GetValue()
        return vtXmlFilterDateTime(sVal,self.chcOp.GetSelection())
    def GetValue(self):
        try:
            sVal=self.viDateTime.GetValue()
            return sVal,self.chcOp.GetSelection()
        except:
            return '2001-01-01 00:00:00',self.chcOp.GetSelection()
    def OnTextText(self,evt):
        if self.bBlock:
            return
        sVal=self.txtVal.GetValue()
        try:
            bOk,bMod,val=self.__validate__(sVal,False,bTxt=True)
            self.__markFlt__(not bOk)
            if bMod:
                self.txtVal.SetValue(val)
        except:
            vtLog.vtLngTB(self.GetName())
        self.__markModified__()
        wx.PostEvent(self,vtXmlFilterTypeChanged(self,sVal))

class vtXmlFilterDateTimeRelInput(wx.Panel):
    def __init__(self,*_args,**_kwargs):
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        self.SetAutoLayout(True)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.spnDays=wx.SpinCtrl(id=-1, initial=0,size=(50,-1),
              max=365, min=0, name='spnDays', parent=self, 
              style=wx.SP_ARROW_KEYS)
        bxs.AddWindow(self.spnDays, 1, border=0, flag=wx.EXPAND|wx.ALL)
        self.spnHrs=wx.SpinCtrl(id=-1, initial=0, size=(50,-1),
              max=23, min=0, name='spnHrs', parent=self, 
              style=wx.SP_ARROW_KEYS)
        bxs.AddWindow(self.spnHrs, 1, border=0, flag=wx.EXPAND|wx.ALL)
        self.spnMns=wx.SpinCtrl(id=-1, initial=0,size=(50,-1),
              max=59, min=0, name='spnHrs', parent=self, 
              style=wx.SP_ARROW_KEYS)
        bxs.AddWindow(self.spnMns, 1, border=0, flag=wx.EXPAND|wx.ALL)
        self.spnSecs=wx.SpinCtrl(id=-1, initial=0,size=(50,-1),
              max=59, min=0, name='spnHrs', parent=self, 
              style=wx.SP_ARROW_KEYS)
        bxs.AddWindow(self.spnSecs, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        self.spnDays.SetValue(1)
        self.spnHrs.SetValue(0)
        self.spnMns.SetValue(0)
        self.spnSecs.SetValue(0)
        #self.spnDays.SetMinSize((-1,-1))
        #self.spnHrs.SetMinSize((-1,-1))
        #self.spnMns.SetMinSize((-1,-1))
        #self.spnSecs.SetMinSize((-1,-1))
        self.spnDays.SetToolTipString(_(u'days'))
        self.spnHrs.SetToolTipString(_(u'hours'))
        self.spnMns.SetToolTipString(_(u'minutes'))
        self.spnSecs.SetToolTipString(_(u'seconds'))
        self.spnDays.Bind(wx.EVT_SPINCTRL, self.OnSpin,self.spnDays)
        self.spnHrs.Bind(wx.EVT_SPINCTRL, self.OnSpin,self.spnHrs)
        self.spnMns.Bind(wx.EVT_SPINCTRL, self.OnSpin,self.spnMns)
        self.spnSecs.Bind(wx.EVT_SPINCTRL, self.OnSpin,self.spnSecs)
        
        self.lWid=[self.spnDays,self.spnHrs,self.spnMns,self.spnSecs]
        
        self.chcOp=wx.Choice(choices=['!=','==','<','<=','>=','>'], id=-1,
              name='chcOp', parent=self, size=(50,-1), style=0)
        self.chcOp.SetMinSize((-1,-1))
        self.chcOp.SetSelection(1)
        self.chcOp.Bind(wx.EVT_CHOICE, self.OnChoice,self.chcOp)
        bxs.AddWindow(self.chcOp, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        self.ckcBefore=wx.CheckBox(id=-1, label=_('before'),
              name='ckcBefore', parent=self, size=(50,-1),style=0)
        self.ckcBefore.SetValue(True)
        self.ckcBefore.SetMinSize((-1,-1))
        bxs.AddWindow(self.ckcBefore, 2, border=0, flag=wx.EXPAND|wx.ALL)
        
        self.SetSizer(bxs)
        bxs.Layout()
        bxs.Fit(self)
        self.bBlock=False
        self.bEnableMark=True
    def SetEnableMark(self,flag):
        self.bEnableMark=flag
    def __markModified__(self,flag=True):
        if self.bEnableMark==False:
            return
        if flag:
            for wid in self.lWid:
                f=wid.GetFont()
                f.SetWeight(wx.FONTWEIGHT_BOLD)
                wid.SetFont(f)
        else:
            for wid in self.lWid:
                f=wid.GetFont()
                f.SetWeight(wx.FONTWEIGHT_NORMAL)
                wid.SetFont(f)
            #f=self.chcOp.GetFont()
            #f.SetWeight(wx.FONTWEIGHT_NORMAL)
            #self.chcOp.SetFont(f)
        for wid in self.lWid:
            wid.Refresh()
        #self.chcOp.Refresh()
    def __markFlt__(self,flag=False):
        if flag:
            for wid in self.lWid:
                color=wx.TheColourDatabase.Find('YELLOW')
                wid.SetBackgroundColour(color)
        else:
            for wid in self.lWid:
                color=wx.SystemSettings_GetColour(wx.SYS_COLOUR_WINDOW)
                wid.SetBackgroundColour(color)
        if self.bEnableMark==False:
            for wid in self.lWid:
                wid.Refresh()
    def __clearBlock__(self):
        self.bBlock=False
    def SetValue(self,val,op=-1):
        self.bBlock=True
        bOk,bMod,val=self.__validate__(val,True)
        self.__markModified__( not bMod)
        self.__markFlt__(not bOk)
        self.__setVal__(val)
        if op>=0:
            self.chcOp.SetSelection(op)
        wx.CallAfter(self.__clearBlock__)
    def GetValueStr(self):
        return self.__getVal__()
    def GetValue(self):
        return self.__getVal__(),self.chcOp.GetSelection()
    def SetFilter(self,flt):
        try:
            self.bBlock=True
            self.__setVal__(flt.GetValue())
            self.chcOp.SetSelection(flt.GetOp())
            self.ckcBefore.SetValue(flt.GetBefore())
            self.__markModified__(False)
        except:
            vtLog.vtLngTB(self.GetName())
        wx.CallAfter(self.__clearBlock__)
    def GetFilter(self):
        return vtXmlFilterDateTimeRel(self.__getVal__(),
                self.chcOp.GetSelection(),self.ckcBefore.GetValue())
    def __validate__(self,val,bHandleFlt=True):
        return True,False,val
    def OnTextText(self,evt):
        if self.bBlock:
            return
        sVal=self.txtVal.GetValue()
        try:
            bOk,bMod,val=self.__validate__(sVal,False)
            self.__markFlt__(not bOk)
            if bMod:
                self.txtVal.SetValue(val)
        except:
            #vtLog.vtLngTB(self.GetName())
            pass
        self.__markModified__()
        wx.PostEvent(self,vtXmlFilterTypeChanged(self,sVal))
    def OnChoice(self,evt):
        if self.bBlock:
            return
        sVal=self.chcOp.GetSelection()
        sValFlt=self.__getVal__()
        #self.__markModified__()
    def __getVal__(self):
        iDys=self.spnDays.GetValue()
        iHrs=self.spnHrs.GetValue()
        iMns=self.spnMns.GetValue()
        iSecs=self.spnSecs.GetValue()
        return '%d.%d.%d'%(iDys,(iHrs*60+iMns)*60+iSecs,0)
    def __setVal__(self,val):
        s=val.split('.')
        iDy=0
        iHr=0
        iMn=0
        iSc=0
        try:
            iDy=long(s[0])
            iSc=long(s[1])
            iMl=long(s[2])
        except:
            pass
        iHr=iSc/(3600)
        iSc-=iHr*3600
        iMn=iSc/60
        iSc-=iMn*60
        self.spnDays.SetValue(iDy)
        self.spnHrs.SetValue(iHr)
        self.spnMns.SetValue(iMn)
        self.spnSecs.SetValue(iSc)
    def OnSpin(self,evt):
        iDys=self.spnDays.GetValue()
        iHrs=self.spnHrs.GetValue()
        iMns=self.spnMns.GetValue()
        iSecs=self.spnSecs.GetValue()

