#----------------------------------------------------------------------------
# Name:         vtXmlAcl.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtXmlAcl.py,v 1.2 2005/12/13 16:46:24 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import fnmatch,traceback
import sys,string

from vidarc.tool.log import vtLog

class vtXmlLoginInfo:
    def __init__(self,usr,usrId,grpIds,iSecLv):
        self.usr=usr
        self.usrId=long(usrId)
        self.grpIds=map(long,grpIds)
        self.iSecLv=iSecLv
    def GetUsr(self):
        return self.usr
    def GetUsrId(self):
        return self.usrId
    def GetGrpIds(self):
        return self.grpIds
    def GetSecLv(self):
        return self.iSecLv
    def __str__(self):
        return 'usr:%08d grp:%s secLv:%3d'%(self.usrId,self.grpIds,self.iSecLv)
class vtXmlAclFilter:
    def __init__(self,doc,tmp,verbose=0):
        self.nr=0
        self.lst=[]
        self.verbose=verbose
        self.__build__(doc,tmp)
    def __build__(self,doc,tmp):
        try:
            self.nr=int(doc.getNodeText(tmp,'nr'))
        except:
            self.nr=0
        for c in doc.getChilds(tmp,'cmp'):
            self.lst.append((doc.getNodeText(c,'value'),
                            doc.getNodeText(c,'filter')))
        if self.verbose>0:
            vtLog.CallStack('compares:%d'%len(self.lst))
            for it in self.lst:
                print '   ',it
    def checkFilter(self,doc,node,sVal,sFilter,iType):
        tmp,s=doc.getNodeInfoVal(node,sVal)
        #print tmp,s,sFilter
        if iType==doc.FILTER_TYPE_STRING:
            if fnmatch.fnmatch(s,sFilter):
                return True
        return False
    def IsFilterOk(self,doc,node):
        #vtLog.CallStack('')
        dPoss=doc.GetPossibleFilter()
        tag=doc.getTagName(node)
        try:
            def getFilterType(sVal):
                lstNode=dPoss[tag]
                for sName,iType in lstNode:
                    if sName==sVal:
                        return iType
                return doc.FILTER_TYPE_STRING
            for sVal,sFilter in self.lst:
                #print sVal,sFilter
                iType=getFilterType(sVal)
                if self.checkFilter(doc,node,sVal,sFilter,iType)==False:
                    return False
        except:
            traceback.print_exc()
            return True
        return True
    def __str__(self):
        return 'nr:%3d cmps:%d'%(self.nr,len(self.lst))
class vtXmlAcl:
    def __init__(self,doc,tmp,verbose=0):
        self.id=-1
        self.name=None
        self.iAcl=0
        self.iSecLv=-1
        self.lstFilter=[]
        self.doc=doc
        self.verbose=verbose
        self.__build__(tmp)
    def __build__(self,tmp):
        #print tmp
        try:
            self.id=long(self.doc.getAttribute(tmp,'fid'))
        except:
            self.id=-1
        self.name=self.doc.getNodeText(tmp,'name')
        try:
            self.iAcl=long(self.doc.getAttribute(tmp,'acl'),16)
        except:
            self.iAcl=0
        try:
            self.iSecLv=long(self.doc.getAttribute(tmp,'sec_level'))
        except:
            self.iSecLv=-1
        for c in self.doc.getChilds(tmp,'filter'):
            self.lstFilter.append(vtXmlAclFilter(self.doc,c,verbose=self.verbose-1))
        def compFunc(a,b):
            return cmp(a.nr,b.nr)
        self.lstFilter.sort(compFunc)
        if self.verbose>0:
            vtLog.CallStack('filters:%d'%len(self.lstFilter))
            for it in self.lstFilter:
                print '   ',it
    def getId(self):
        return self.id
    def getName(self):
        return self.name
    def getAcl(self):
        return iAcl
    def IsAccessOk(self,node,objLogin,iAcl):
        #vtLog.CallStack('')
        for it in self.lstFilter:
            #print it
            if it.IsFilterOk(self.doc,node):
                #print '   filter ok'
                #print '       ',self.iAcl,iAcl
                if self.iAcl&iAcl:
                    #print '    acl ok',self.iAcl,iAcl
                    if self.iSecLv<=objLogin.GetSecLv():
                        #print '   sec level ok',self.iSecLv,objLogin.GetSecLv()
                        return True
        return False
    def __str__(self):
        return 'id:%08d acl:0x%08x sec_lv:%3d filters:%3d'%(self.id,self.iAcl,self.iSecLv,len(self.lstFilter))
class vtXmlAclList:
    def __init__(self,doc,node,verbose=0):
        self.doc=doc
        self.nodeAcl=node
        self.verbose=verbose
        self.__build__()
        
    def __buildAclAccess__(self,tmp):
        d={'user':{},'username':{},'group':{},'groupname':{}}
        dUsr=d['user']
        dUsrName=d['username']
        dGrp=d['group']
        dGrpName=d['groupname']
        for c in self.doc.getChilds(tmp):
            o=vtXmlAcl(self.doc,c,verbose=self.verbose-1)
            tag=self.doc.getTagName(c)
            if tag=='user':
                dUsr[o.getId()]=o
                dUsrName[o.getName()]=o
            elif tag=='group':
                dGrp[o.getId()]=o
                dGrpName[o.getName()]=o
        return d
    def __buildAcl__(self,tmp):
        self.dAcl={}
        for c in self.doc.getChilds(tmp):
            self.dAcl[self.doc.getTagName(c)]=self.__buildAclAccess__(c)
        if self.verbose>0:
            #vtLog.CallStack('')
            for k in self.dAcl.keys():
                #print '   ',k
                d=self.dAcl[k]
                for kk in d.keys():
                    #print '     ',kk
                    dd=d[kk]
                    for kkk in dd.keys():
                        print '       ',dd[kkk]
    def __build__(self):
        self.dAcl=None
        if self.nodeAcl is None:
            return 
        for c in self.doc.getChilds(self.nodeAcl):
            if self.doc.getAttribute(c,'active')=='1':
                self.__buildAcl__(c)
                return True
    def IsAccessOk(self,node,objLogin,iAcl):
        #vtLog.CallStack(iAcl)
        #self.doc.acquire()
        tag=self.doc.getTagName(node)
        try:
            d=self.dAcl[tag]
            try:
                #vtLog.CallStack('')
                #print d
                #print objLogin
                #print d
                if objLogin.GetUsrId()<0:
                    obj=d['username'][objLogin.GetUsr()]
                else:
                    obj=d['user'][objLogin.GetUsrId()]
                #print obj
                if obj.IsAccessOk(node,objLogin,iAcl):
                    #self.doc.release()
                    return True
            except:
                #traceback.print_exc()
                pass
        except:
            #traceback.print_exc()
            #self.doc.release()
            return True
        #self.doc.release()
        return False
        