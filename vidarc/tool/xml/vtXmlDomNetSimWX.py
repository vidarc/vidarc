#----------------------------------------------------------------------------
# Name:         vtXmlDomNetSimWX.py
# Purpose:      mixin class to emulate networking features
#
# Author:       Walter Obweger
#
# Created:      20091011
# CVS-ID:       $Id: vtXmlDomNetSimWX.py,v 1.5 2010/03/03 02:16:17 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.net.vtNetArt as vtNetArt
from vtXmlDomNetSim import vtXmlDomNetSim
from vidarc.tool.gui.vtgButtonStateFlag import vtgButtonStateFlag
from vidarc.tool.vtThread import vtThreadWX
from vidarc.tool.misc.vtmThread import vtmThreadInterFace

from vtXmlWxGuiEvents import *

VTNETXMLWXGUI_DICT=None
VTNETXMLWXGUI_DICTMED=None
VTNETXMLWXGUI_DICTSUB=None
VTNETXMLWXGUI_DICTSUBMED=None

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

class vtXmlDomNetSimWX(vtXmlDomNetSim,vtgButtonStateFlag,vtnxXmlEvent,
                vtmThreadInterFace):
    OFFLINE_ADD     =0x01
    OFFLINE_EDIT    =0x02   # see also __setOffline__ default value
    OFFLINE_DEL     =0x04
    OFFLINE_MOVE    =0x08
    
    CLOSED          =0x000
    DISCONNECTED    =0x010
    PRECONNECT      =0x01F
    CONNECT         =0x020
    CONNECTED       =0x060
    CONNECT_FLT     =0x030
    OPEN_START      =0x002
    OPENED          =0x004
    OPEN_FLT        =0x005
    SYNCH_START     =0x0A0
    SYNCH_PROC      =0x0B0
    SYNCH_FIN       =0x0C0
    SYNCH_ABORT     =0x0D0
    LOGIN           =0x070
    LOGGEDIN        =0x080
    STOPPED         =0x0F0
    SERVED          =0x040
    SERVE_FLT       =0x050
    CONFIG          =0x090
    LOGIN_ATTEMPT   =0x00000020
    DATA_ACCESS     =0x00000100
    BLOCK_NOTIFY    =0x00000200
    #OPENED          =0x00000400
    CFG_VALID       =0x00000800
    SYNCHABLE       =0x00001000
    CONN_ACTIVE     =0x00002000
    CONN_PAUSE      =0x00004000
    SHUTDOWN        =0x00008000
    SYNCH_IDLE      =0x00010000         # synch data when idle no access
    CONN_ATTEMPT    =0x00020000
    DATA_SETTLED    =0x00040000
    STATE_MASK      =0x000000FF
    MASK            =0x00FFFFFF
    
    SINGLE_LOCK     =0
    
    def __init__(self,parent,appl,pos=(0,0),size=(16,16),synch=False,small=True,verbose=0,id=-1):
        if small:
            self.bSmall=True
        else:
            self.bSmall=False
        self.netMaster=None
        vtLog.vtLngCurCls(vtLog.DEBUG,''%(),self)
        vtgButtonStateFlag.__init__(self,id=id,parent=parent,pos=pos,size=size,
                bitmap=vtArt.getBitmap(vtArt.Invisible),name=appl,
                style=0,small=small)
    def __init_ctrl__(self,*args,**kwargs):
        self.__logDebug__(''%())
        self.oSF.AddStateDict(
                    {
                    0x000:{'name':'CLOSED','translation':_(u'closed'),
                        'imgNor':vtNetArt.Stopped,
                        'imgMed':vtNetArt.StoppedMed,
                        'imgSub':vtNetArt.StoppedAlpha,
                        'imgMedSub':vtNetArt.StoppedMedAlpha,
                        'init':True,'is':None,
                        'enter':{'set':['CLSD','STLD',],
                                 'clr':['WRKG','RDY','MOD']}},
                    0x001:{'name':'OPEN','translation':_(u'open'),
                        'imgNor':vtNetArt.Open,
                        'imgMed':vtNetArt.OpenMed,
                        'imgSub':vtNetArt.OpenAlpha,
                        'imgMedSub':vtNetArt.OpenMedAlpha,
                        'is':None,
                        'enter':{'set':['WRKG',],
                                 'clr':['STLD','FLT','CLSD','MOD','RDY']}},
                    0x002:{'name':'OPENING','translation':_(u'opening'),
                        'imgNor':vtNetArt.Open,
                        'imgMed':vtNetArt.OpenMed,
                        'imgSub':vtNetArt.OpenAlpha,
                        'imgMedSub':vtNetArt.OpenMedAlpha,
                        'is':None,},
                    0x003:{'name':'GENID','translation':_(u'generate IDs'),
                        'imgNor':vtNetArt.Open,
                        'imgMed':vtNetArt.OpenMed,
                        'imgSub':vtNetArt.OpenAlpha,
                        'imgMedSub':vtNetArt.OpenMedAlpha,
                        'is':None,
                        'enter':{'set':['WRKG']}},
                    0x004:{'name':'OPENED','translation':_(u'opened'),
                        'imgNor':vtNetArt.Opened,
                        'imgMed':vtNetArt.OpenedMed,
                        'imgSub':vtNetArt.OpenedAlpha,
                        'imgMedSub':vtNetArt.OpenedMedAlpha,
                        'is':'IsOpenOk',
                        'enter':{'set':['OPND',],
                                 'clr':['FLT','MOD']},
                        '':{'set':['WRKG']}},
                    0x005:{'name':'OPEN_FLT','translation':_(u'open fault'),
                        'imgNor':vtNetArt.OpenFault,
                        'imgMed':vtNetArt.OpenFaultMed,
                        'imgSub':vtNetArt.OpenFaultAlpha,
                        'imgMedSub':vtNetArt.OpenFaultMedAlpha,
                        'is':'IsOpenFault',
                        'enter':{'set':['FLT','OPND','MOD',],
                                 'clr':['WRKG','RDY',]}},
                    0x006:{'name':'PAUSE','translation':_(u'pause'),
                        'imgNor':vtNetArt.Stopped,
                        'imgMed':vtNetArt.StoppedMed,
                        'imgSub':vtNetArt.StoppedAlpha,
                        'imgMedSub':vtNetArt.StoppedMedAlpha,
                        'is':None,
                        'enter':{'set':['WRKG'],
                                 'clr':['RDY']}},
                    0x007:{'name':'PAUSING','translation':_(u'pausing'),
                        'imgNor':vtNetArt.Stopped,
                        'imgMed':vtNetArt.Stopped,
                        'imgSub':vtNetArt.StoppedAlpha,
                        'imgMedSub':vtNetArt.StoppedAlpha,
                        'is':None,
                        'enter':{'set':['WRKG']}},
                    0x008:{'name':'PAUSED','translation':_(u'paused'),
                        'imgNor':vtNetArt.Stopped,
                        'imgMed':vtNetArt.StoppedMed,
                        'imgSub':vtNetArt.StoppedAlpha,
                        'imgMedSub':vtNetArt.StoppedMedAlpha,
                        'is':None,
                        'enter':{#'set':['fin',],
                                 'clr':['WRKG','RDY']}},
                    0x009:{'name':'RESUME','translation':_(u'resume'),
                        'enter':{'set':['WRKG']}},
                    0x00E:{'name':'NEW','translation':_(u'new'),
                        'imgNor':vtNetArt.Stopped,
                        'imgMed':vtNetArt.StoppedMed,
                        'imgSub':vtNetArt.StoppedAlpha,
                        'imgMedSub':vtNetArt.StoppedMedAlpha,
                        'enter':{'set':['MOD']}},
                    0x00F:{'name':'CLOSING','translation':_(u'closing'),
                        'imgNor':vtNetArt.Stopping,
                        'imgMed':vtNetArt.StoppingMed,
                        'imgSub':vtNetArt.StoppingAlpha,
                        'imgMedSub':vtNetArt.StoppingMedAlpha,
                        'imgBig':vtNetArt.StoppingBig,
                        'enter':{'set':['WRKG']}},
                    
                    0x010:{'name':'DISCONNECT','translation':_(u'disconnect'),
                        'imgNor':vtNetArt.Stopping,
                        'imgMed':vtNetArt.StoppingMed,
                        'imgSub':vtNetArt.StoppingAlpha,
                        'imgMedSub':vtNetArt.StoppingMedAlpha,
                        'imgBig':vtNetArt.StoppingBig,
                        'is':'IsDisConnect',
                        'enter':{'set':['WRKG',],
                                 'clr':['STLD','RDY','ONLINE','CONN_ATTEMPT']}},
                    0x013:{'name':'DISCONNECTING','translation':_(u'disconnected'),
                        'imgNor':vtNetArt.Stopping,
                        'imgMed':vtNetArt.StoppingMed,
                        'imgSub':vtNetArt.StoppingAlpha,
                        'imgMedSub':vtNetArt.StoppingMedAlpha,
                        'imgBig':vtNetArt.StoppingBig,
                        'is':'IsDisConnecting',
                        'enter':{'set':[],
                                 'clr':['WRKG','RDY']}},
                    0x015:{'name':'DISCONNECTED','translation':_(u'disconnected'),
                        'imgNor':vtNetArt.Stopped,
                        'imgMed':vtNetArt.StoppedMed,
                        'imgSub':vtNetArt.StoppedAlpha,
                        'imgMedSub':vtNetArt.StoppedMedAlpha,
                        'imgBig':vtNetArt.StoppedBig,
                        'is':'IsDisConnected',
                        'enter':{'set':['STLD'],
                                 'clr':['WRKG','RDY','CONN_ACTIVE']}},
                    0x01F:{'name':'PRECONNECT','translation':_(u'pre connect'),
                        'imgNor':vtNetArt.Connect,
                        'imgMed':vtNetArt.ConnectMed,
                        'imgSub':vtNetArt.ConnectAlpha,
                        'imgMedSub':vtNetArt.ConnectMedAlpha,
                        'imgBig':vtNetArt.ConnectBig,
                        'is':None,
                        'enter':{'set':['WRKG','BLOCK_NOTIFY'],
                                 'clr':['STLD','RDY','CONN_ATTEMPT']}},
                    0x020:{'name':'CONNECT','translation':_(u'connect'),
                        'imgNor':vtNetArt.Connect,
                        'imgMed':vtNetArt.ConnectMed,
                        'imgSub':vtNetArt.ConnectAlpha,
                        'imgMedSub':vtNetArt.ConnectMedAlpha,
                        'imgBig':vtNetArt.ConnectBig,
                        'is':None,
                        'enter':{'set':['WRKG','BLOCK_NOTIFY','CONN_ATTEMPT'],
                                 'clr':['STLD','RDY']}},
                    0x030:{'name':'CONNECT_FLT','translation':_(u'connect fault'),
                        'imgNor':vtNetArt.Fault,
                        'imgMed':vtNetArt.FaultMed,
                        'imgSub':vtNetArt.FaultAlpha,
                        'imgMedSub':vtNetArt.FaultMedAlpha,
                        'imgBig':vtNetArt.FaultBig,
                        'is':'IsConnectFault',
                        'enter':{'set':['FLT','STLD'],
                                 'clr':['WRKG','RDY']}},
                    0x040:{'name':'SERVED','translation':_(u'served'),
                        'imgNor':vtNetArt.Connect,
                        'imgMed':vtNetArt.ConnectMed,
                        'imgSub':vtNetArt.ConnectAlpha,
                        'imgMedSub':vtNetArt.ConnectMedAlpha,
                        'imgBig':vtNetArt.ConnectBig,
                        'is':None,
                        'enter':{'set':['WRKG'],
                                 'clr':['STLD','RDY']}},
                    0x050:{'name':'SERVE_FLT','translation':_(u'serve fault'),
                        'imgNor':vtNetArt.ServeFlt,
                        'imgMed':vtNetArt.ServeFltMed,
                        'imgSub':vtNetArt.ServeFltAlpha,
                        'imgMedSub':vtNetArt.ServeFltMedAlpha,
                        'imgBig':vtNetArt.ServeFltBig,
                        'is':None,
                        'enter':{'set':['STLD','FLT'],
                                 'clr':['WRKG','RDY']}},
                    0x060:{'name':'CONNECTED','translation':_(u'connected'),
                        'imgNor':vtNetArt.Connect,
                        'imgMed':vtNetArt.ConnectMed,
                        'imgSub':vtNetArt.ConnectAlpha,
                        'imgMedSub':vtNetArt.ConnectMedAlpha,
                        'imgBig':vtNetArt.ConnectBig,
                        'is':'IsConnected',
                        'enter':{#'set':['STLD'],
                                 'set':['WRKG','LOGIN_FIRST'],
                                 'clr':['WRKG','RDY']}},
                    0x070:{'name':'LOGIN','translation':_(u'login'),
                        'imgNor':vtNetArt.Login,
                        'imgMed':vtNetArt.LoginMed,
                        'imgSub':vtNetArt.LoginAlpha,
                        'imgMedSub':vtNetArt.LoginMedAlpha,
                        'imgBig':vtNetArt.LoginBig,
                        'is':None,
                        'enter':{#'set':['WRKG','LOGIN_FIRST'],
                                 'clr':['STLD','RDY'],
                                 'CB':(self.Login,[],{})}},
                    0x075:{'name':'LOGIN_ATTEMPT','translation':_(u'login attempt'),
                        'imgNor':vtNetArt.Login,
                        'imgMed':vtNetArt.LoginMed,
                        'imgSub':vtNetArt.LoginAlpha,
                        'imgMedSub':vtNetArt.LoginMedAlpha,
                        'imgBig':vtNetArt.LoginBig,
                        'is':None,
                        'enter':{#'set':['WRKG','LOGIN_FIRST'],
                                 'clr':['STLD','RDY'],
                                 #'CB':(self.Login,[],{})
                                }},
                    0x080:{'name':'LOGGEDIN','translation':_(u'loggedin'),
                        'imgNor':vtNetArt.Loggedin,
                        'imgMed':vtNetArt.LoggedinMed,
                        'imgSub':vtNetArt.LoggedinAlpha,
                        'imgMedSub':vtNetArt.LoggedinMedAlpha,
                        'imgBig':vtNetArt.LoggedinBig,
                        'is':None,
                        'enter':{'set':['WRKG','CONNINFOSET'],
                                 'clr':['STLD','RDY','CONNINFOSET'],
                                 'CBs':[
                                        (self.__genAutoFN__,(),{'bStore':True}),
                                        (self.GetLoggedInAcl,(),{}),
                                        (self.GetConfig,(),{}),
                                        ],
                                    },
                                },
                    0x090:{'name':'CONFIG','translation':_(u'configuration'),
                        'imgNor':vtNetArt.Connect,
                        'imgMed':vtNetArt.ConnectMed,
                        'imgSub':vtNetArt.ConnectAlpha,
                        'imgMedSub':vtNetArt.ConnectMedAlpha,
                        'imgBig':vtNetArt.ConnectBig,
                        'is':None,
                        'enter':{'set':['WRKG','CFG_VALID'],
                                 'clr':['STLD','RDY']}},
                    0x0A0:{'name':'SYNCH_START','translation':_(u'synching'),
                        'imgNor':vtNetArt.Synch,
                        'imgMed':vtNetArt.SynchMed,
                        'imgSub':vtNetArt.SynchAlpha,
                        'imgMedSub':vtNetArt.SynchMedAlpha,
                        'imgBig':vtNetArt.SynchBig,
                        'is':None,
                        'enter':{'set':['WRKG'],
                                 'clr':['STLD','RDY']}},
                    0x0B0:{'name':'SYNCH_PROC','translation':_(u'synching'),
                        'imgNor':vtNetArt.Synch,
                        'imgMed':vtNetArt.SynchMed,
                        'imgSub':vtNetArt.SynchAlpha,
                        'imgMedSub':vtNetArt.SynchMedAlpha,
                        'imgBig':vtNetArt.SynchBig,
                        'is':None,
                        'enter':{'set':['WRKG'],
                                 'clr':['STLD','RDY']}},
                    0x0C0:{'name':'SYNCH_FIN','translation':_(u'synch finsihed'),
                        'imgNor':vtNetArt.Running,
                        'imgMed':vtNetArt.RunningMed,
                        'imgSub':vtNetArt.RunningAlpha,
                        'imgMedSub':vtNetArt.RunningMedAlpha,
                        'imgBig':vtNetArt.RunningBig,
                        'is':'IsSynchFinished',
                        'enter':{'set':['STLD','RDY'],
                                 'clr':['WRKG',]}},   #'BLOCK_NOTIFY'
                    0x0D0:{'name':'SYNCH_ABORT','translation':_(u'synch aborted'),
                        'imgNor':vtNetArt.SynchFlt,
                        'imgMed':vtNetArt.SynchFltMed,
                        'imgSub':vtNetArt.SynchFltAlpha,
                        'imgMedSub':vtNetArt.SynchFltMedAlpha,
                        'imgBig':vtNetArt.SynchFltBig,
                        'is':'IsSynchAborted',
                        'enter':{'set':['STLD','RDY'],
                                 'clr':['WRKG',]}},     #'BLOCK_NOTIFY'
                    0x0E0:{'name':'STOPPING','translation':_(u'stopping'),
                        'imgNor':vtNetArt.Stopping,
                        'imgMed':vtNetArt.StoppingMed,
                        'imgSub':vtNetArt.StoppingAlpha,
                        'imgMedSub':vtNetArt.StoppingMedAlpha,
                        'imgBig':vtNetArt.StoppingBig,
                        'is':None,
                        'enter':{'set':['WRKG'],
                                 'clr':['STLD','WRKG','RDY']}},
                    0x0F0:{'name':'STOPPED','translation':_(u'stopped'),
                        'imgNor':vtNetArt.Stopped,
                        'imgMed':vtNetArt.StoppedMed,
                        'imgSub':vtNetArt.StoppedAlpha,
                        'imgMedSub':vtNetArt.StoppedMedAlpha,
                        'imgBig':vtNetArt.StoppedBig,
                        'is':None,
                        'enter':{'set':['STLD'],
                                 'clr':['WRKG','RDY','CONN_ACTIVE','BLOCK_NOTIFY']}},
                    #0x007:{'name':'paused','translation':_(u'paused'),
                    #    'is':None,
                    #    'leave':{'set':['fin','RDY'],
                    #             'clr':['WRKG']},
                    #    'enter':{'set':['WRKG']}},
                    })
        self.oSF.AddFlagDict({
                    0x00000001:{'name':'CLSD', 'translation':_(u'closed'),'is':'IsClosed','order':0},
                    0x00000002:{'name':'STLD', 'translation':_(u'settled'),'is':'IsSettled','order':1},
                    0x00000004:{'name':'WRKG',  'translation':_(u'working'),'is':'IsWorking','order':2},
                    0x00000008:{'name':'RDY',  'translation':_(u'ready'),'is':'IsReady','order':3},
                    0x00000010:{'name':'MOD',  'translation':_(u'MODified'),'is':'IsModified','order':4},
                    0x00000020:{'name':'FLT',  'translation':_(u'faulty'),'is':'IsFaulty','order':5},
                    0x00000040:{'name':'OPND', 'translation':_(u'opened'),'is':'IsOpened','order':6},
                    0x00000080:{'name':'PSD', 'translation':_(u'opened'),'is':'IsPaused','order':7},
                    0x00000100:{'name':'SYNCHABLE', 'translation':_(u'synchable'),'is':'IsSynchable','keep':True},
                    0x00000200:{'name':'SYNCH_IDLE', 'translation':_(u'synch on idle'),'is':'IsSynchIdle'},
                    0x00000400:{'name':'BLOCK_NOTIFY', 'translation':_(u'block events'),'is':'IsBlockNotify','order':10},
                    0x00000800:{'name':'DATA_ACCESS', 'translation':_(u'data access'),'is':'IsDataAccess'},
                    0x00001000:{'name':'CFG_VALID', 'translation':_(u'configuration valid'),'is':'IsCfgValid','order':12},
                    0x00002000:{'name':'SYNCHED', 'translation':_(u'synched'),'is':'IsSynched','order':13},
                    0x00004000:{'name':'CONN_ACTIVE', 'translation':_(u'connection active'),'is':'IsConnActive','order':14},
                    0x00008000:{'name':'CONN_PAUSE', 'translation':_(u'connection paused'),'is':'IsConnPaused','order':15},
                    0x00010000:{'name':'CONN_ATTEMPT', 'translation':_(u'connection attemped'),'is':'IsConnAttempt','order':16},
                    #0x00020000:{'name':'LOGIN_ATTEMPT', 'translation':_(u'login attemped'),'is':'IsLoginAttemped','order':17},
                    0x00080000:{'name':'SHUTDOWN', 'translation':_(u'shutdown'),'is':'IsShutDown','keep':True,'order':18},
                    0x00100000:{'name':'LOGIN_FIRST', 'translation':_(u'login first attemped'),'is':'IsLoginFirst','order':19},
                    0x00200000:{'name':'ONLINE', 'translation':_(u'online'),'is':'IsOnLine','order':20},
                    0x00400000:{'name':'ACTIVE', 'translation':_(u'active'),'is':'IsActive','order':21},
                    
                    0x01000000:{'name':'FIXED', 'translation':_(u'application fixed'),'is':'IsFixed','keep':True},
                    0x02000000:{'name':'CONNINFOSET', 'translation':_(u'connection info set'),'is':'IsConnInfoSet','keep':True},
                    })
        #self.oSF.AddCB(self.oSF.LOGGEDIN,'enter',self.__genAutoFN__,bStore=True)
        #self.oSF.AddCB(self.oSF.LOGGEDIN,'enter',self.getDataFromRemote)
        #self.oSF.SetFlag(self.oSF.SYNCHABLE,synch)
        #self.widLogging=vtLog.GetPrintMsgWid(self)
        self.thdProc=vtThreadWX(None,bPost=False,
                origin=u':'.join([self.GetOrigin(),u'thdProc']))
        vtmThreadInterFace.__init__(self,self.thdProc)
        #vtnxXmlEvent.__init__(self)
        self.__logInfo__(''%())
        
        self.dictDoc={}     #FIXME
        self.sRecentAlias=''
        self.sRecentConn=''
        self.alias=''
        self.host=''
        self.port=-1
        self.usr=''
        self.passwd=''
        self.sUsr=''
        self.iUsrId=-1
        self.iSecLv=-1
        self.bAdmin=False
        #self.__setupImageList__()
        #self.__initCmdRemoteDict__()
    def __del__(self):
        #self.__logDebug__(''%())
        pass
    def SetupAsMainAlias(self):
        pass
    def __setupImageList__(self,bSmall=True):
        global VTNETXMLWXGUI_DICT
        global VTNETXMLWXGUI_DICTMED
        global VTNETXMLWXGUI_DICTSUB
        global VTNETXMLWXGUI_DICTSUBMED
        if VTNETXMLWXGUI_DICT is None:
            if VERBOSE>5:
                self.__logDebug__(''%())
            self.imgDict={}
            for state,sImg in self.oSF.GetStateInfoLst('imgNor'):
                if sImg is None:
                    continue
                self.imgDict[state]=vtNetArt.getBitmap(sImg)
            self.imgDict[None]      =vtNetArt.getBitmap(vtNetArt.Fault)
            VTNETXMLWXGUI_DICT=self.imgDict
            
            sz=(16,12)
            self.imgDictMed={}
            for state,sImg in self.oSF.GetStateInfoLst('imgMed'):
                if sImg is None:
                    continue
                self.imgDictMed[state]=vtNetArt.getBitmap(sImg,sz=sz)
            self.imgDictMed[None]      =vtNetArt.getBitmap(vtNetArt.FaultMed,sz=sz)
            VTNETXMLWXGUI_DICTMED=self.imgDictMed
            
            self.imgDictSub={}
            for state,sImg in self.oSF.GetStateInfoLst('imgSub'):
                if sImg is None:
                    continue
                self.imgDictSub[state]=vtNetArt.getBitmap(sImg,sz=sz)
            self.imgDictSub[None]      =vtNetArt.getBitmap(vtNetArt.FaultAlpha)
            VTNETXMLWXGUI_DICTSUB=self.imgDictSub
            
            self.imgDictSubMed={}
            for state,sImg in self.oSF.GetStateInfoLst('imgMedSub'):
                if sImg is None:
                    continue
                self.imgDictSubMed[state]=vtNetArt.getBitmap(sImg,sz=sz)
            self.imgDictSubMed[None]      =vtNetArt.getBitmap(vtNetArt.FaultMedAlpha,sz=sz)
            VTNETXMLWXGUI_DICTSUBMED=self.imgDictSubMed
            if VERBOSE>5:
                self.__logDebug__('imgDict:%s'%(self.__logFmt__(self.imgDict)))
                self.__logDebug__('imgDictMed:%s'%(self.__logFmt__(self.imgDictMed)))
                self.__logDebug__('imgDictSub:%s'%(self.__logFmt__(self.imgDictSub)))
                self.__logDebug__('imgDictSubMed:%s'%(self.__logFmt__(self.imgDictSubMed)))
        else:
            if VERBOSE>5:
                self.__logDebug__('take cached images'%())
            self.imgDict=VTNETXMLWXGUI_DICT
            self.imgDictMed=VTNETXMLWXGUI_DICTMED
            self.imgDictSub=VTNETXMLWXGUI_DICTSUB
            self.imgDictSubMed=VTNETXMLWXGUI_DICTSUBMED
    def SetupAsMainAlias(self):
        self.__logDebug__(''%())
        #self.oSF.ClrCBs(self.oSF.LOGGEDIN,'enter')
        #self.oSF.ClrCBs(self.oSF.CONFIG,'enter')
        #self.oSF.AddCB(self.oSF.LOGGEDIN,'enter',self.__genAutoFN__,bStore=True)
        #self.oSF.AddCB(self.oSF.LOGGEDIN,'enter',self.GetLoggedInAcl)
        #self.oSF.AddCB(self.oSF.LOGGEDIN,'enter',self.GetConfig)
        #self.oSF.AddCB(self.oSF.CONFIG,'enter',self.getDataFromRemote)
    
    def GetAppls(self):
        return []
    def GetNetDoc(self,appl):
        return None
    
    def ClrCBs(self,state,kind):
        self.__logDebug__(''%())
        self.oSF.ClrCBs(state,kind)
    def AddCB(self,state,kind,func,*args,**kwargs):
        self.__logDebug__(''%())
        self.oSF.AddCB(state,kind,func,*args,**kwargs)
    def DelCB(self,state,kind,func,*args,**kwargs):
        self.__logDebug__(''%())
        self.oSF.DelCB(state,kind,func,*args,**kwargs)
    def GetWidId(self):
        return vtgButtonStateFlag.GetId(self)
    
    def Stop(self,func=None,*args,**kwargs):
        self.__logInfo__(''%())
    def IsStopping(self):
        self.__logDebug__(''%())
    def __ShutDown__(self):
        self.__logInfo__(''%())
        self.oSF.SetFlag(self.oSF.SHUTDOWN)
    def __start__(self):
        pass
    def __stop__(self):
        self.__logInfo__(''%())
    def __isRunning__(self):
        return False
    def Pause(self,flag):
        self.__logInfo__('flag:%d'%(flag))
    
    def IsConnAttempt(self):
        return self.oSF.IsConnAttempt
    def IsCfgValid(self):
        self.__logDebug__(''%())
        return self.oSF.IsCfgValid
    def IsConnActive(self):
        return self.oSF.IsConnActive
    def ConnPause(self,flag):
        pass
    def IsDisConn(self):
        return not self.oSF.IsConnActive
    def IsSettled(self):
        return self.oSF.IsSettled
        #return self.IsFlag(self.DATA_SETTLED)
    def __IsSettled__(self):
        self.__logCritical__(''%())
        return self.oSF.IsSettled
    def IsActive(self):
        return self.oSF.IsActive
    def __setBitmapByStatus__(self,iStatusPrev=None,iStatus=None):
        if self.IsStatusChanged(iStatusPrev,iStatus)==False:
            return
        #self.__calcFlags__()
        if self.GetStatus(iStatus) in self.imgDict:
            self.__setBitMap__(self.imgDict[self.GetStatus(iStatus)])
        else:
            self.__setBitMap__(self.imgDict[None])
    def __getBitmapByStatus__(self,iStatus=None):
        #self.__calcFlags__()
        iStatus=self.GetStatus(iStatus)
        if VERBOSE>10:
            self.__logDebug__('iStatus:%d;in imgDict=%d'%(iStatus,iStatus in self.imgDict))
        if iStatus in self.imgDict:
            return self.imgDict[self.GetStatus(iStatus)]
        else:
            return self.imgDict[None]
    def __getSubBitmapByStatus__(self,iStatus=None):
        #self.__calcFlags__()
        iStatus=self.GetStatus(iStatus)
        if VERBOSE>10:
            self.__logDebug__('iStatus:%d;in imgDict=%d'%(iStatus,iStatus in self.imgDictSub))
        if iStatus in self.imgDictSub:
            return self.imgDictSub[self.GetStatus(iStatus)]
        else:
            return self.imgDictSub[None]
    def __getMedSubBitmapByStatus__(self,iStatus=None):
        #self.__calcFlags__()
        iStatus=self.GetStatus(iStatus)
        if VERBOSE>10:
            self.__logDebug__('iStatus:%d;in imgDict=%d'%(iStatus,iStatus in self.imgDictSubMed))
        if iStatus in self.imgDictSubMed:
            return self.imgDictSubMed[self.GetStatus(iStatus)]
        else:
            return self.imgDictSubMed[None]
    def __getMedBitmapByStatus__(self,iStatus=None):
        iStatus=self.GetStatus(iStatus)
        if VERBOSE>10:
            self.__logDebug__('iStatus:%d;in imgDict=%d'%(iStatus,iStatus in self.imgDictMed))
        if iStatus in self.imgDictMed:
            return self.imgDictMed[iStatus]
        else:
            return self.imgDict[None]
    def GetListImgDict(self,imgLstTyp):
        d={}
        for state,sImg in self.oSF.GetStateInfoLst('imgNor'):
            try:
                if sImg is None:
                    continue
                d[state]=imgLstTyp.Add(vtNetArt.getBitmap(sImg))
            except:
                self.__logTB__()
                self.__logError__('state:%s'%(state))
        return d
    def GetStatusStr(self,iStatus=None):
        if iStatus is None:
            return self.oSF.GetStateNameAct()
        else:
            return self.oSF.GetStateName(iStatus)
    def GetStatusTrans(self,iStatus=None):
        if iStatus is None:
            return self.oSF.GetStateTransAct()
        else:
            return self.oSF.GetStateTrans(iStatus)
    def GetFlagValLst(self):
        return self.oSF.GetFlagLst()
    def __getLogStatusStr__(self):
        return self.oSF.GetStr(u';')
    def GetStatus(self,iStatus=None):
        if iStatus is None:
            return self.oSF.GetState()
        else:
            return iStatus
    def __logStatus__(self):
        if self.__isLogInfo__():
            self.__logInfo__(self.oSF.GetStrFull(u';'))
    def NotifyStateChange(self,*args,**kwargs):
        """ called in case of state change from wx MainLoop
        """
        self.Refresh()
        if self.netMaster is not None:
            self.netMaster.NotifyStateChangeAlias(*args,**kwargs)
    def __getLogStatusStr__(self):
        return ''
    def IsRunning(self):
        return False
    def IsStopping(self):
        return False
    def IsFlag(self,int_flag,iStatus=None):
        if int_flag==self.CONN_ACTIVE:          # 071025:wro little hack
            self.__logWarn__('FIXME'%())
            return self.oSF.IsConnActive
        self.__logCritical__('int_flag:%08x,iStatus:%s'%(int_flag,iStatus))
        return False
    
    def IsConnActive(self):
        return False
    def SetBlockNotify(self,flag):
        if self.__isLogDebug__():
            self.__logDebug__('flag:%d'%(flag))
        bOld=self.oSF.IsBlockNotify
        self.oSF.SetFlag(self.oSF.BLOCK_NOTIFY,flag)
        if bOld==True and self.oSF.IsBlockNotify==False:
                self.PostEvent(self,vtnxXmlGotContent(self))
                self.PostEvent(self,vtnxXmlBuildTree(self))
                self.PostEvent(self,vtnxXmlContentChanged(self))
    def Select(self,node):
        if self.oSF.IsBlockNotify:
            return
        if node is not None:
            self.PostEvent(self,vtnxXmlSelect(self,self.getKey(node)))
    def SelectById(self,iId):
        if self.oSF.IsBlockNotify:
            return
        self.PostEvent(self,vtnxXmlSelect(self,iId))
    def IsBlockNotify(self):
        self.__logWarn__(''%())
        return self.oSF.IsBlockNotify
    def IsUsrInteractNeeded(self,bOffLine):
        """ this is usally called by net-master object to verify, if
        user interaction is required.
        return True -> show connections status dialog
        return False -> very thing is fine
        """
        if bOffLine:
            if self.oSF.IsOpenOk:
                return False
            elif self.oSF.IsDisConnected:
                return False
            else:
                return True
        else:
            if self.oSF.IsSynchFinished:
                return False
            else:
                return True
    def DisConnect(self):
        pass
    def PreConnect(self):
        pass
    def Connect2SrvByAlias(self,objAlias,dn,force=False,offline=False):
        if self.__isLogInfo__():
            self.__logInfo__('force:%d;offline:%d'%(force,offline))
        bDbg=False
        if self.__isLogDebug__():
            bDbg=True
        offline=True
        self.__logStatus__()
        #self.dn=self.docSetup.getNodeText(node,'path')
        self.dn=dn
        
        oldRecentAlias=self.sRecentAlias
        oldRecentConn=self.sRecentConn
        oldHost=self.host
        oldAlias=self.alias
        oldUsr=self.usr
            
        if objAlias is None:
            self.oSF.SetFlag(self.oSF.CONN_ATTEMPT,False)
            self.oSF.SetFlag(self.oSF.STLD,False)
            self.oSF.SetFlag(self.oSF.CONNINFOSET,False)
            fn=self.__genAutoFN__(bStore=True)
            return 0
        try:
            if offline==False:
                self.oSF.SetFlag(self.oSF.CONN_ATTEMPT,True)
            #self.oSF.SetFlag(self.oSF.DATA_SETTLED,False)
            self.oSF.SetState(self.oSF.CONNECT)
            #self.ClearConsumer()   #071023: wro
            self.sRecentConn=objAlias.GetHostConnectionStr()
            self.sRecentAlias=objAlias.GetHostAliasStr()
            if bDbg:
                self.__logDebug__('%s;%s'%(objAlias.GetHostAliasStr(),
                            objAlias.GetStr()))
                self.__logDebug__('oldconn:%s;newconn:%s'%(
                            oldRecentConn,self.sRecentConn))
            self.host=objAlias.GetHost()
            self.port=objAlias.GetPort()
            self.alias=objAlias.GetAlias()
            self.usr=objAlias.GetUsr()
            self.passwd=objAlias.GetPasswd()
            #self.bConnInfoSet=True
            self.oSF.SetFlag(self.oSF.CONNINFOSET,True)
            self.__calcApplAlias__()
            #fn=self.__genAutoFN__()
            sOld=self.GetFN()
            fn=self.GenAutoFNConnAppl(objAlias,self.dn)
            if bDbg:
                self.__logDebug__('oldFN:%s;newFN:%s'%(sOld,fn))
            self.oSF.SetFlag(self.oSF.CONNINFOSET,False)
            # 070422:wro restart audit trail
            #vtLog.CallStack('')
            #print fn
            #print self.sFN
            #self.audit.stop()
            #self.Save()
            #self.audit.start(self.sFN,self.GetAppl())
            
            bOpen=False
            bClose=False
            #bConn=not objAlias.IsOffline() # 070611:wro
            #if offline:                    # 070611:wro
            #    bConn=False                # 070611:wro
            bConn=not offline               # 070611:wro
            self.oSF.SetFlag(self.oSF.ONLINE,bConn)
            
            if bDbg:
                self.__logDebug__('fn:%s'%(fn))
            #if len(oldRecentAlias)>0:
            #    if self.sRecentAlias!=oldRecentAlias:
            #        bOpen=True
            #        bClose=True
            #        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            #            vtLog.vtLngCur(vtLog.DEBUG,'oldconn:%s conn:%s'%(oldRecentAlias,self.sRecentAlias),origin=self.GetOrigin())
            if len(oldRecentConn)>0:
                if self.sRecentConn!=oldRecentConn:
                    bOpen=True
                    bClose=True
                    if bDbg:
                        self.__logDebug__('oldconn:%s;newconn:%s'%(oldRecentConn,self.sRecentConn))
            if self.host!=oldHost:
                bOpen=True
            if self.alias!=oldAlias:
                bOpen=True
                #bClose=True
            if self.usr!=oldUsr:
                bOpen=True
            if bDbg:
                self.__logDebug__('bOpen:%d bClose:%d'%(bOpen,bClose))
                #vtLog.vtLngCur(vtLog.DEBUG,'open',origin=self.GetOrigin())
            #if self.IsFlag(self.OPENED)==False:
            #    bOpen=True
            if self.oSF.IsOpened==False:
                bOpen=True
            if bOpen:
                #self.bDataSettled=False
                #self.bLoginFirst=True
                #self.oSF.SetState(self.oSF.OPEN
                #self.Open(fn)     # 070623:wro set flags first
                self.CallBack(self.Open,fn)
                if bConn:
                    # delay connection attept until file is completly opened
                    self.oSF.SetFlag(self.oSF.LOGIN_FIRST,True)
                    self.oSF.SetFlag(self.oSF.CONN_ACTIVE,True)
                else:
                    self.oSF.SetFlag(self.oSF.CONN_ACTIVE,False)
                #self.Open(fn)      # 070623:wro set flags first
            else:
                if bConn:
                    #self.bLoginFirst=True
                    self.oSF.SetFlag(self.oSF.LOGIN_FIRST,True)
                    self.oSF.SetFlag(self.CONN_ACTIVE,True)
                    #self.Connect2Srv()     # 070623:wro set flags first
                    self.CallBack(self.Connect2Srv)
                #else:
                #    self.oSF.SetFlag(self.oSF.CONN_ACTIVE,False)
            self.__logStatus__()
            if 1==0:
                if bClose:
                    for k in self.dictDoc.keys():
                        if k == self.appls[0]:
                            continue
                        self.dictDoc[k].ReConnect2Srv()
                        
                else:
                    for k in self.dictDoc.keys():
                        if k == self.appls[0]:
                            continue
                        self.dictDoc[k].ReConnect2Srv()
                return 1
        except Exception,list:
            self.__logTB__()
            self.sRecentAlias=''
            self.sRecentConn=''
            return -1
        return
    def Connect2SrvByNodeOld(self,node,lst):
        self.__logInfo__(''%())
        if node is None:
            if self.nodeSetup is not None:
                node=self.nodeSetup
            else:
                node=self.root
        if node is None:
            return -1
        if lst is None:
            lst=self.lstSetupNode
        if self.__isLogDebug__():
            self.__logDebug__('lst:%s'%(self.__logFmt__(lst)))
        node=self.getChildByLst(node,lst)
        if node is None:
            return
        self.SetNode(node)
        self.Connect2Srv()
        return 0
    def Connect2SrvByCfgOld(self,recentNode,recentAliasNode,recentHostNode,force=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        self.nodeSetup=self.docSetup.getRoot()
        node=self.docSetup.getChildByLst(self.nodeSetup,self.lstCfg)
        self.dn=self.docSetup.getNodeText(node,'path')
            
        oldRecentAlias=self.sRecentAlias
        oldRecentConn=self.sRecentConn
        oldHost=self.host
        oldAlias=self.alias
        oldUsr=self.usr
            
        if recentNode is None:
                self.oSF.SetFlag(self.oSF.CONN_ATTEMPT,False)
                #self.bConnInfoSet=False
                self.oSF.SetFlag(self.oSF.CONNINFOSET,False)
                self.__genAutoFN__()
                self.cltSrv=None
                self.cltSock=None
                #self.Setup()
                return 0
        try:
                #self.SetFlag(self.CONN_ATTEMPT,True)
                #self.SetFlag(self.DATA_SETTLED,False)
                self.oSF.SetState(self.oSF.CONNECT)
                self.sRecentAlias=self.docSetup.getNodeText(recentNode,'name')
                self.sRecentConn=self.docSetup.getNodeText(recentNode,'name')
                    
                self.host=self.docSetup.getNodeText(recentHostNode,'host')
                self.port=int(self.docSetup.getNodeText(recentHostNode,'port'))
                self.alias=self.docSetup.getNodeText(recentHostNode,'alias')
                self.usr=self.docSetup.getNodeText(recentHostNode,'usr')
                self.passwd=self.docSetup.getNodeText(recentHostNode,'passwd')
                #self.bConnInfoSet=True
                self.oSF.SetFlag(self.oSF.CONNINFOSET,True)
                self.__calcApplAlias__()
                fn=self.__genAutoFN__(bStore=True)
                self.oSF.SetFlag(self.oSF.CONNINFOSET,False)
                bOpen=False
                bClose=False
                #bConn=False
                #if force==False:
                #    if self.GetStatus() in [self.STOPPED,self.DISCONNECTED]:
                #        bConn=self.__getAutoConnect__(recentAliasNode)
                #else:
                #    bConn=True
                #    bOpen=True
                bConn=True
                #self.bOnline=bConn
                self.oSF.SetFlag(self.oSF.ONLINE,bConn)
                if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'fn:%s'%(fn),origin=self.GetOrigin())
                if len(oldRecentAlias)>0:
                    if self.sRecentAlias!=oldRecentAlias:
                        bOpen=True
                        bClose=True
                        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCur(vtLog.DEBUG,'oldconn:%s conn:%s'%(oldRecentAlias,self.sRecentAlias),origin=self.GetOrigin())
                if len(oldRecentConn)>0:
                    if self.sRecentConn!=oldRecentConn:
                        bOpen=True
                        bClose=True
                        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCur(vtLog.DEBUG,'oldconn:%s conn:%s'%(oldRecentConn,self.sRecentConn),origin=self.GetOrigin())
                if self.host!=oldHost:
                    bOpen=True
                if self.alias!=oldAlias:
                    bOpen=True
                    #bClose=True
                if self.usr!=oldUsr:
                    bOpen=True
                
                if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'bOpen:%d bClose:%d'%(bOpen,bClose),origin=self.GetOrigin())
                    vtLog.vtLngCur(vtLog.DEBUG,'open',origin=self.GetOrigin())
                if self.oSF.IsOpened==False:#Flag(self.OPENED)==False:
                    bOpen=True
                if bOpen:
                    #if bConn:
                    #    self.Open(fn,silent=True)
                    #else:
                    #    self.Open(fn,silent=False)
                    #self.bDataSettled=False
                    #self.bLoginFirst=True
                    self.Open(fn)
                
                    if bConn:
                        # delay connection attept until file is completly opened
                        self.oSF.SetFlag(self.oSF.LOGIN_FIRST,True)
                        self.oSF.SetFlag(self.oSF.CONN_ACTIVE,True)
                else:
                    if bConn:
                        #self.bLoginFirst=True
                        self.oSF.SetFlag(self.oSF.LOGIN_FIRST,True)
                        self.oSF.SetFlag(self.oSF.CONN_ACTIVE,True)
                        self.Connect2Srv()
                #else:
                #    if self.IsFlag(self.OPENED)==False:
                #        self.bDataSettled=False
                #        self.Open(fn,silent=False)
                
                if 1==0:
                    if bClose:
                        for k in self.dictDoc.keys():
                            if k == self.appls[0]:
                                continue
                            self.dictDoc[k].ReConnect2Srv()
                            #if self.verbose:
                            #    vtLog.vtLngCur(vtLog.DEBUG,'close:%s'%(k))
                            #self.dictDoc[k].DisConnect()
                            #self.dictDoc[k].Close()
                            
                            #self.dictDoc[k].AutoConnect2Srv(force=True)
                    else:
                        for k in self.dictDoc.keys():
                            if k == self.appls[0]:
                                continue
                            self.dictDoc[k].ReConnect2Srv()
                    return 1
        except Exception,list:
                vtLog.vtLngTB(self.GetOrigin())
                self.sRecentAlias=''
                self.sRecentConn=''
                #self.Setup()
                return -1
    def Login(self):
        self.__logInfo__(''%())
    def __genAutoFN__(self,bStore=True):
        #if self.bConnInfoSet==True:
        try:
            #fn=os.path.join(self.dn,string.join([self.mainAppl,self.appl,self.alias,self.host,self.usr],'_')+'.xml')
            fn=os.path.join(self.dn,self.host,self.usr,self.alias,self.appl+'.xml')
            if self.__isLogDebug__():
                self.__logDebug__('fn:%s'%(fn))
            if bStore:
                self.SetFN(fn)
            return fn
        except:
            self.__logTB__()
        fn=os.path.join(self.dn,'local','usr','dummy',self.appl+'.xml')
        if self.__isLogDebug__():
            self.__logDebug__('fn:%s'%(fn))
        if bStore:
            self.SetFN(fn)
        return fn
        #return None
    def GenAutoFN(self,cfgNode,dn):
        if self.__isLogDebug__():
            self.__logDebug__('')
        self.acquire('dom')
        try:
            if self.getNodeText(cfgNode,'port')=='offline':
                self.SetFN(self.getNodeText(cfgNode,'fn'))
            else:
                self.SetFN(os.path.join(dn,self.getNodeText(cfgNode,'host'),
                            self.getNodeText(cfgNode,'usr'),
                            self.getNodeText(cfgNode,'alias'),
                            self.appl+'.xml'))
        except:
            self.SetFN(os.path.join(dn,'local','usr','dummy',self.appl+'.xml'))
        self.release('dom')
        return self.sFN
    def GenAutoFNConnAppl(self,connAppl,dn):
        if self.__isLogDebug__():
            self.__logDebug__(connAppl.GetHostConnectionStr())
        try:
            self.SetFN(connAppl.GenAutoFN(dn))
            return self.GetFN()
        except:
            pass
        self.SetFN(os.path.join(dn,'local','usr','dummy',self.appl+'.xml'))
        return self.GetFN
        #return None
    def GetAutoFN(self):
        if self.__isLogDebug__():
            self.__logDebug__('')
        #recentNode=self.__getRecentAlias__()
        #recentAliasNode=self.__getRecentConnection__(recentNode)
        #recentNode=self.__getApplHost__(recentNode)
        #if self.bConnInfoSet==True:
        if self.oSF.IsConnInfoSet:
            self.__genAutoFN__(bStore=True)
        #else:
        #    self.sFN=None
        return self.sFN
    def ClearAutoFN(self):
        if self.__isLogDebug__():
            self.__logDebug__('')
        return 1
        if self.cltSrv is None:
            self.sFN=None
            return 1
        else:
            return 0
    def GetLoggedInSecLv(self):
        if self.oLogin is None:
            return -1
        return self.oLogin.GetSecLv()
    def IsLoggedInSelf(self,node):
        return False
    def New(self,root='root'):
        pass
        #self.__logInfo__(''%())
    def GetFN(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'GetFN',origin=self.GetOrigin())
        return self.sFN
    def OpenApplDocs(self):
        if self.__isLogInfo__():
            self.__logInfo__('')
        for k in self.dictDoc.keys():
            if k == self.appls[0]:
                continue
            self.dictDoc[k].Open()
    def SaveApplDocs(self,encoding='ISO-8859-1'):
        if self.__isLogInfo__():
            self.__logInfo__(''%())
        for k in self.dictDoc.keys():
            if k == self.appls[0]:
                continue
            self.dictDoc[k].Save(encoding=encoding)
    def GetAppl(self):
        return self.appl
    def __calcApplAlias__(self):
        self.SetApplAlias(':'.join([self.appl,self.alias]))
    #def GetApplAlias(self):
    #    return self.applAlias
    def GetAlias(self):
        return self.alias
    def GetHost(self):
        return self.host
    def GetPort(self):
        return self.port
    def GetUsr(self):
        return self.usr
    def Connect2Srv(self):
        pass
    def __setOffline__(self,node,action=0x02,bRec=False):
        self.__logCritical__(''%())
        pass
    def GetConfig(self):
        pass
    def GetLoggedInAcl(self):
        pass
    def GetKeys(self):
        pass
    def GetSynchNode(self,id):
        pass
    
    def startEdit(self,node):
        if self.oSF.IsShutDown:
            return
        if node is None:
            self.__logDebug__('node is None')
            return
        id=self.getKey(node)
        try:
            if self.audit.isEnabled():
                self.audit.write('startEdit',None,self.GetNodeXmlContent(node,False,False),
                            origin=self.getAuditOriginByNode(node))
        except:
            pass
        self.CallBack(self.NotifyLockNode,id,'ok')
    def endEdit(self,node):
        if self.oSF.IsShutDown:
            return
        id=self.getKey(node)
        try:
            if self.audit.isEnabled():
                self.audit.write('endEdit',self.GetNodeXmlContent(node,False,False),
                            origin=self.getAuditOriginByNode(node))
        except:
            pass
        self.CallBack(self.NotifyUnLockNode,id,'released')
    def IsLocked(self,node):
        return True
    def doEdit(self,node,idLock=None):
        if self.oSF.IsShutDown:
            return
        id=self.getKey(node)
        if self.__isLogInfo__():
            self.__logInfo__('idLock:%s;    id:%s'%(idLock,id))
        if node is not None:
            if self.__isLogDebug__():
                self.__logDebug__('fp:%s;%s'%(self.getFingerPrint(node),
                        self.getKey(node)))
        try:
            if self.audit.isEnabled():
                self.audit.write('doEdit',self.GetNodeXmlContent(node,False,False),
                            origin=self.getAuditOriginByNode(node))
        except:
            pass
        self.__HandleInternal__('upd',self.getKey(node))
        self.calcFingerPrint(node)
        self.CallBack(self.NotifySetNode,id,id,'')
    def doEditFull(self,node):
        if self.oSF.IsShutDown:
            return
        id=self.getKey(node)
        if self.__isLogInfo__():
            self.__logInfo__('id:%s'%id)
        if self.__isLogDebug__():
            self.__logDebug__('fp:%s;%s'%(self.getFingerPrint(node),node))
        try:
            if self.audit.isEnabled():
                self.audit.write('doEditFull',self.GetNodeXmlContent(node,False,True),
                            origin=self.getAuditOriginByNode(node))
        except:
            pass
        self.__setOffline__(node,self.OFFLINE_EDIT,bRec=True)
        self.__HandleInternal__('upd',self.getKey(node))
        def handleinternal(node):
            self.__HandleInternal__('upd',self.getKey(node))
            self.procChildsKeys(node,handleinternal)
        self.procChildsKeys(node,handleinternal)
        self.calcFingerPrintFull(node)
        self.CallBack(self.NotifySetNode,id,id,'') #061119 wro delay post
        
    def doUpdate(self,node):
        if self.oSF.IsShutDown:
            return
        id=self.getKey(node)
        if self.__isLogInfo__():
            self.__logInfo__('id:%s'%id)
        if self.__isLogDebug__():
            self.__logDebug__('fp:%s;%s'%(self.getFingerPrint(node),node))
        try:
            if self.audit.isEnabled():
                self.audit.write('doUpdate',self.GetNodeXmlContent(node,False,False),
                            origin=self.getAuditOriginByNode(node))
        except:
            pass
        self.__setOffline__(node,self.OFFLINE_EDIT)
        self.clearFingerPrint(node)
        self.GetFingerPrintAndStore(node)
        self.CallBack(self.NotifySetNode,id,id,'') #061119 wro delay post
    def changeIdNet(self,node,id):
        if node is None:
            return
    
    def addNode(self,par,node,func=None,**kwargs):
        if self.oSF.IsShutDown:#IsFlag(self.SHUTDOWN):
            return
        if node is None:
            return
        if VERBOSE:
            if self.__isLogDebug__():
                self.__logDebug__('kwargs:%s'%(self.__logFmt__(kwargs)))
        if '__idOld__' in kwargs:
            idOld=kwargs['__idOld__']
            del kwargs['__idOld__']
        else:
            idOld=None
        if '__idNew__' in kwargs:
            idNew=kwargs['__idNew__']
            del kwargs['__idNew__']
        else:
            idNew=None
        if func is not None:
            func(par,node,**kwargs)
        idPar=self.getAttribute(par,self.attr)
        id=self.getAttribute(node,self.attr)
        if idOld is None:
            idOld=id
        if idNew is None:
            idNew=id
        if VERBOSE:
            if self.__isLogInfo__():
                self.__logInfo__('id:%s;parId:%s;idOld:%s;idNew:%s'%(id,
                        idPar,idOld,idNew))
        else:
            if self.__isLogInfo__():
                self.__logInfo__('id:%s;parId:%s'%(id,idPar))
        try:
            if self.audit.isEnabled():
                self.audit.write('addNode:%s to:%s'%(id,idPar),self.GetNodeXmlContent(node,False,False),
                            origin=self.getAuditOriginByNode(node))
        except:
            pass
        self.CallBack(self.NotifyAddNodeResponse,id,id,'ok')
        #self.CallBack(self.NotifyAddNodeResponse,idOld,idNew,'ok')
    def delNode(self,node):
        if self.oSF.IsShutDown:#IsFlag(self.SHUTDOWN):
            return
        if node is None:
            return
        id=self.getKey(node)
        if self.__isLogInfo__():
            self.__logInfo__('id:%s'%id)
        try:
            if self.audit.isEnabled():
                self.audit.write('delNode',self.GetNodeXmlContent(node,True,True),
                            origin=self.getAuditOriginByNode(node))
        except:
            pass
        self.CallBack(self.NotifyDelNode,id,'ok')
    def setNode(self,node):
        if self.oSF.IsShutDown:
            return
        if node is None:
            return
        id=self.getKey(node)
        if self.__isLogInfo__():
            self.__logInfo__('id:%s'%id)
        self.clearFingerPrint(node) # FIXME??? wro 060913
        try:
            if self.audit.isEnabled():
                self.audit.write('setNode',self.GetNodeXmlContent(node,False,False),
                            origin=self.getAuditOriginByNode(node))
        except:
            pass
        self.CallBack(self.NotifySetNode,id,id,'')
    def setNodeFull(self,node):
        if self.oSF.IsShutDown:#IsFlag(self.SHUTDOWN):
            return
        if node is None:
            return
        id=self.getKey(node)
        if self.__isLogInfo__():
            self.__logInfo__('id:%s'%id)
        self.clearFingerPrint(node) # FIXME??? wro 060913
        try:
            if self.audit.isEnabled():
                self.audit.write('setNodeFull',self.GetNodeXmlContent(node,False,True),
                            origin=self.getAuditOriginByNode(node))
        except:
            pass
        self.CallBack(self.NotifySetNode,id,id,'')
    #def moveNode2Srv(self,par,node):
    #    return vNetXmlWxGui.moveNode(self,par,node)
    def moveNode(self,par,node):
        if self.oSF.IsShutDown:#IsFlag(self.SHUTDOWN):
            return
        idPar=self.getKey(par)
        id=self.getKey(node)
        if self.__isLogInfo__():
            self.__logInfo__('parId:%s;id:%s'%(idPar,id))
        try:
            if self.audit.isEnabled():
                self.audit.write('moveNode:%d from:%d to:%d'%\
                        (self.getKeyNum(node),self.getKeyNum(self.getParent(node)),self.getKeyNum(par),),\
                        self.GetNodeXmlContent(node,False,False),
                        origin=self.getAuditOriginByNode(node))
        except:
            pass
        self.CallBack(self.NotifyMoveNodeResponse,idPar,id,'ok')
    def NotifyPause(self,flag):
        self.__logInfo__(''%())
        self.oSF.SetFlag(self.oSF.CONN_PAUSE,flag)
    def NotifyOpenStart(self,fn):
        if self.oSF.IsShutDown:
            return
        self.__logInfo__(''%())
        #if self.silent==False:
        #    self.NotifyContent()
        self.sUsr=''
        self.iSecLv=-1
        self.bAdmin=False
        #self.__setStatus__(self.OPEN_START)
        self.oSF.SetState(self.oSF.OPEN)
        #if self.IsBlockNotify()==False:
        if self.oSF.IsBlockNotify==False:
            self.PostEvent(self,vtnxXmlOpenStart(self))
        
    def NotifyOpenOk(self,fn):
        if self.oSF.IsShutDown:
            return
        self.__logInfo__(''%())
        #self.__setStatus__(self.OPEN_OK)
        self.oSF.SetState(self.oSF.OPENED)
        #if self.IsFlag(self.CONN_ACTIVE):
        if self.oSF.IsConnActive:
            # start delay connection attept 
            self.Connect2Srv()
        if self.oSF.IsOnLine==False:
            self.oSF.SetFlag(self.oSF.BLOCK_NOTIFY,False)
        #if self.IsBlockNotify()==False:
        if self.oSF.IsBlockNotify==False:
            self.PostEvent(self,vtnxXmlOpenOk(self))
        if 1:#if self.cltSrv is None:
                # 070422:wro 
                if hasattr(self,'CreateReq'):
                    self.CreateReq()
                self.Build()
                #self.bDataSettled=True
                self.oSF.SetFlag(self.oSF.STLD)
        #if self.bSynch:
        #    self.bDataSettled=True
        #if self.bDataSettled==True:
        #if self.IsFlag(self.SYNCHABLE)==False:
        self.NotifyContent()
        
    def NotifyOpenFault(self,fn,sExp):
        if self.oSF.IsShutDown:
            return
        self.__logInfo__(''%())
        self.oSF.SetState(self.oSF.OPEN_FLT)
        if self.oSF.IsOnLine==False:
            self.oSF.SetFlag(self.oSF.STLD,True)
        self.New(bAcquire=False)
        #if self.IsBlockNotify()==False:
        if self.oSF.IsBlockNotify==False:
            self.PostEvent(self,vtnxXmlOpenFault(self))
        #self.__setStatus__(self.OPEN_FLT)
        #if self.IsFlag(self.CONN_ACTIVE):
        if self.oSF.IsConnActive:
            # start delay connection attept 
            self.Connect2Srv()
        else:
            self.NotifyContent()

    def NotifyContent(self):
        if self.__isLogInfo__():
            self.__logInfo__('setteled:%d;block:%d'%(self.IsSettled(),
                            self.oSF.IsBlockNotify))
        self.__logStatus__()
        #self.__updateConfig__()
        #self.genIds()
        
        #if self.IsFlag(self.SYNCHABLE)==False:
        if self.IsSettled():
            #if hasattr(self,'CreateReq'):
            #    self.CreateReq()
            #self.Build()        # 061006 wro lead to blocking vMESCenter
                                # 061007 wro can't be deactivated!
                                # 070421 wro isn't seems to be risky
            #if self.IsBlockNotify()==False:
            if self.oSF.IsBlockNotify==False:
                self.PostEvent(self,vtnxXmlGotContent(self))
                self.PostEvent(self,vtnxXmlContentChanged(self))
                self.PostEvent(self,vtnxXmlBuildTree(self))
    def NotifyGetNode(self,id):
        if self.__isLogInfo__():
            self.__logInfo__('id:%s'%(id))
        try:
            sID=self.ConvertIdStr2Str(id)
            #if self.IsBlockNotify()==False:
            if self.oSF.IsBlockNotify==False:
                self.PostEvent(self,vtnxXmlGetNode(self,sID))
                self.PostEvent(self,vtnxXmlContentChanged(self))
        except:
            self.__logTB__()
            self.__logError__('notify get node id:%s'%(id))
    def NotifySetNode(self,id,idNew,resp):
        if self.__isLogInfo__():
            self.__logInfo__('id:%s;idNew:%s;resp:%s'%(id,idNew,resp))
        try:
            sID=self.ConvertIdStr2Str(id)
            sNewID=self.ConvertIdStr2Str(idNew)
            if resp=='ok':
                #if long(sID)!=long(sNewID):
                #    if self.SINGLE_LOCK!=0:
                #        self.idLock=sNewID
                #    else:
                #        self.lock.Del(sID)
                #        self.lock.Add(sNewID)
                pass
            #if self.IsBlockNotify()==False:
            if self.oSF.IsBlockNotify==False:
                self.PostEvent(self,vtnxXmlSetNode(self,sID,sNewID,resp))
            #if self.thdSynch.IsRunning():
            #    self.thdSynch.__procSetNodeResponse__(id,idNew,resp)
        except:
            self.__logTB__()
            self.__logError__('notify set node id:%s idNew:%s resp:%s'%(id,idNew,resp))
    def NotifyLockNode(self,id,content):
        if self.__isLogInfo__():
            self.__logInfo__('id:%s'%(id))
        try:
            sID=self.ConvertIdStr2Str(id)
            strs=content.split(',')
            if strs[0] in ['ok','already locked']:
                self.PostEvent(self,vtnxXmlLock(self,sID,content))
            elif strs[0]=='lock block':
                i=content.find(',')
                try:
                    zTime=int(content[i+1:])
                except:
                    zTime=-2
                if self.oSF.IsBlockNotify==False:
                    node=self.getNodeById(sID)
                    sTag=self.getTagName(node)
                    vtLog.PrintMsg('id:%s tag:%s'%(sID,sTag)+' ' + _('lock blocked for')+' '+str(zTime)+'[s]',self.widLogging)
                    self.PostEvent(self,vtnxXmlLockBlock(self,sID,zTime))
                    self.PostEvent(self,vtnxXmlLock(self,sID,'block'))
            elif strs[0]=='rejected':
                if 1:
                    node=self.getNodeById(sID)
                    sTag=self.getTagName(node)
                    if len(strs)>3:
                        sMsg=_(u'lock rejected locked by %s at %s')%(strs[2],strs[4])
                    else:
                        sMsg=' '.join(strs)
                    vtLog.PrintMsg('id:%s tag:%s'%(sID,sTag)+' ' + ' '.join(strs),self.widLogging)
                    self.PostEvent(self,vtnxXmlLock(self,sID,content))
            else:
                    node=self.getNodeById(sID)
                    sTag=self.getTagName(node)
                    vtLog.PrintMsg('id:%s tag:%s'%(sID,sTag)+' ' + ' '.join(strs),self.widLogging)
                    self.PostEvent(self,vtnxXmlLock(self,sID,content))
        except:
            self.__logTB__()
            self.__logError__('notify lock node id:%s content:%s'%(id,content))
    def NotifyUnLockNode(self,id,content):
        if self.__isLogInfo__():
            self.__logInfo__('id:%s'%(id))
        try:
            sID=self.ConvertIdStr2Str(id)
            #if content in ['ok','released','granted']:# wro 060805 content=='released':
            #    if self.SINGLE_LOCK!=0:
            #        if sID==self.idLock:
            #            self.idLock=''
            #    else:
            #        self.lock.Del(sID)
            #if self.IsBlockNotify()==False:
            if self.oSF.IsBlockNotify==False:
                self.PostEvent(self,vtnxXmlUnLock(self,sID,content))
        except:
            self.__logTB__()
            self.__logError__('notify unlock node id:%s content:%s'%(id,content))
    def NotifyLockRequest(self,id,content):
        if self.__isLogInfo__():
            self.__logInfo__('id:%s'%(id))
        if self.__isLogDebug__():
            self.__logDebug__(content)
        try:
            sID=self.ConvertIdStr2Str(id)
            #if self.IsBlockNotify()==False:
            if self.oSF.IsBlockNotify==False:
                sMsg=''
                strs=content.split(',')
                if strs[0]=='ok':
                    sMsg=_(u'lock request wait for %s [s]')%strs[1]
                    vtLog.PrintMsg(sMsg,self.widLogging)
                    return
                elif strs[0]=='request':
                    sMsg=_(u'lock request by %s from %s:%s')%(strs[1],strs[2],strs[3])
                elif strs[0]=='grant':
                    if len(strs)>3:
                        sMsg=_(u'grant lock request (%s) by %s from %s:%s')%(strs[4],strs[1],strs[2],strs[3])
                    else:
                        sMsg=_(u'grant lock request content:%s')%(content)
                    #self.PostEvent(self,vtnxXmlLock(self,sID,'ok'))
                    self.NotifyLockNode(sID,'ok')
                elif strs[0]=='reject':
                    sMsg=_(u'lock request rejected (%s) by %s from %s:%s')%(strs[4],strs[1],strs[2],strs[3])
                else:
                    vtLog.vtLngCur(vtLog.ERROR,'unknown response;%s'%content,self.GetOrigin())
                    sMsg=content
                vtLog.PrintMsg(sMsg,self.widLogging)
                self.PostEvent(self,vtnxXmlLockRequest(self,sID,sMsg,strs[0],content))
        except:
            self.__logTB__()
            self.__logError__('notify unlock node id:%s content:%s'%(id,content))
    def NotifyAddNode(self,idPar,id,content):
        if self.__isLogInfo__():
            self.__logInfo__('idPar:%s id:%s'%(idPar,id))
        if self.__isLogDebug__():
            self.__logDebug__(content)
        try:
            sParID=self.ConvertIdStr2Str(idPar)
            sID=self.ConvertIdStr2Str(id)
            #if content=='ok':
            #    self.idLock=sID
            #if self.IsBlockNotify()==False:
            if self.oSF.IsBlockNotify==False:
                self.PostEvent(self,vtnxXmlAddNode(self,sParID,sID,content))
                self.PostEvent(self,vtnxXmlContentChanged(self))
        except:
            self.__logTB__()
            self.__logError__('notify add node idPar:%s id:%s content:%s'%(idPar,id,content))
    def NotifyAddNodeResponse(self,id,idNew,resp):
        if self.__isLogInfo__():
            self.__logInfo__('id:%s;idNew:%s;resp:%s'%(id,idNew,resp))
        try:
            sID=self.ConvertIdStr2Str(id)
            sIDNew=self.ConvertIdStr2Str(idNew)
            if resp=='ok':
                if self.oSF.IsBlockNotify==False:
                    if self.__isLogDebug__():
                        self.__logDebug__('notify')
                    self.PostEvent(self,vtnxXmlAddNodeResponse(self,sID,sIDNew,resp))
                else:
                    if self.__isLogDebug__():
                        self.__logDebug__('notify blocked')
            else:
                # 061009 wro already done in vtNetXmlClt
                #child=self.getNodeById(id)
                #if child is not None:
                #    self.delNode(child)
                # 070818:wro post delete event
                self.PostEvent(self,vtnxXmlDelNode(self,sID,'ok'))
                try:
                    sTrans={'acl':_(u'ACL'),'locked':_(u'locked'),'fault':_(u'fault')}[resp]
                except:
                    sTrans=_(u'unknown (%s)')%resp
                vtLog.PrintMsg(_(u'appl:%s add id:%s response %s')%(self.appl,sID,sTrans),self.widLogging)
            #if self.thdSynch.IsRunning():
            #    self.thdSynch.__procAddNodeResponse__(sID,sIDNew,resp)
        except:
            self.__logTB__()
            self.__logError__('id:%s;idNew:%s;resp:%s'%(id,idNew,resp))
    def NotifyDelNode(self,id,resp=''):
        if self.__isLogInfo__():
            self.__logInfo__('id:%s;resp:%s'%(id,resp))
        try:
            if len(id)==0:
                return
            sID=self.ConvertIdStr2Str(id)
            if resp=='ok':
                if self.oSF.IsBlockNotify==False:
                    self.PostEvent(self,vtnxXmlDelNode(self,sID,resp))
                    self.PostEvent(self,vtnxXmlContentChanged(self))
            else:
                try:
                    sTrans={'acl':_(u'ACL'),'locked':_(u'locked'),'fault':_(u'fault'),'err':_(u'fault')}[resp]
                except:
                    sTrans=_(u'unknown (%s)')%resp
                vtLog.PrintMsg(_(u'appl:%s del id:%s response %s')%(self.appl,sID,sTrans),self.widLogging)
            #if self.thdSynch.IsRunning():
            #    self.thdSynch.__procDelNodeResponse__(sID,resp)
        except:
            self.__logTB__()
            self.__logError__('notify del node id:%s'%(id))
    def NotifyRemoveNode(self,id):
        if self.__isLogInfo__():
            self.__logInfo__('id:%s'%(id))
        try:
            sID=self.ConvertIdStr2Str(id)
            #if self.IsBlockNotify()==False:
            if self.oSF.IsBlockNotify==False:
                self.PostEvent(self,vtnxXmlRemoveNode(self,sID))
                self.PostEvent(self,vtnxXmlContentChanged(self))
        except:
            self.__logTB__()
            self.__logError__('notify remove node id:%s'%(id))
    def NotifyMoveNode(self,idPar,id):
        if self.__isLogInfo__():
            self.__logInfo__('idPar:%s id:%s'%(idPar,id))
        try:
            sParID=self.ConvertIdStr2Str(idPar)
            sID=self.ConvertIdStr2Str(id)
            #if self.IsBlockNotify()==False:
            if self.oSF.IsBlockNotify==False:
                self.PostEvent(self,vtnxXmlMoveNode(self,sParID,sID))
                self.PostEvent(self,vtnxXmlContentChanged(self))
        except:
            self.__logTB__()
            self.__logError__('notify move node idPar:%s id:%s'%(idPar,id))
    def NotifyMoveNodeResponse(self,idPar,id,resp=''):
        if self.__isLogInfo__():
            self.__logInfo__('id:%s;idPar:%s;resp:%s'%(id,idPar,resp))
        try:
            sID=self.ConvertIdStr2Str(id)
            sParID=self.ConvertIdStr2Str(idPar)
            if resp=='ok':
                #if self.IsBlockNotify()==False:
                if self.oSF.IsBlockNotify==False:
                    self.PostEvent(self,vtnxXmlMoveNodeResponse(self,sParID,sID,resp))
                    self.PostEvent(self,vtnxXmlContentChanged(self))
            else:
                nodePar=self.getNodeById(idPar)
                node=self.getNodeById(id)
                self.moveNode(nodePar,node)
                try:
                    sTrans={'acl':_(u'ACL'),'locked':_(u'locked'),'fault':_(u'fault'),'err':_(u'fault')}[resp]
                except:
                    sTrans=_(u'unknown (%s)')%resp
                vtLog.PrintMsg(_(u'appl:%s move id:%s response %s')%(self.appl,sID,sTrans),self.widLogging)
            #if self.thdSynch.IsRunning():
            #    self.thdSynch.__procMoveNodeResponse__(sID,sParID,resp)
        except:
            self.__logTB__()
            self.__logError__('notify move node response idPar:%s id:%s'%(idPar,id))
    def GetLockID(self):
        self.__logCritical__('you are not supposed to be here'%())
    def NotifyFault(self,origin,fault):
        self.__logError__('origin:%s;fault:%s'%(origin,fault))

