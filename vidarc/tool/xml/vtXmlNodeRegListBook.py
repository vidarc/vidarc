#----------------------------------------------------------------------------
# Name:         vtXmlNodeRegListBook.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060503
# CVS-ID:       $Id: vtXmlNodeRegListBook.py,v 1.21 2010/03/29 08:52:35 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import wx
    
    import types,copy
    
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel
    
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vtXmlNodeRegListBook(wx.Notebook,vtXmlNodePanel):
    VERBOSE=0
    def _init_ctrls(self, prnt,id, pos, size, style, name):
        wx.Notebook.__init__(self, style=style, name=name, parent=prnt, 
                pos=pos, id=id, size=size)
    def __init__(self, parent, id=wx.NewId(), pos=wx.DefaultPosition, 
                    size=wx.Size(100, 50), style=wx.CLIP_CHILDREN, name='',applName='vtXmlNodeRegListBook',verbose=0):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self._init_ctrls(parent,id, pos, size, style, name)
        vtXmlNodePanel.__init__(self,applName=applName)
        
        self.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGED, self.OnPageChanged)
        self.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGING, self.OnPageChanging)
        
        self.bIncludeBase=False
        self.verbose=verbose
        self.lstName=[]
        self.lstPanel=[]
    def __del__(self):
        #for pn in self.lstPanel:
        #    pn.Destroy()
        #del self.lstPanel
        pass
    def GetPanelByName(self,name):
        try:
            i=self.lstName.index(name)
            return self.lstPanel[i]
        except:
            return None
    def __buildPanels__(self):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        if self.objRegNode is None:
            self.__logDebug__('objRegNode is None')
        else:
            self.__logDebug__('tagName:%s'%(self.objRegNode.GetTagName()))
        valid=copy.deepcopy(self.objRegNode.GetValidChild())
        if valid is None:
            valid=[]
        il = wx.ImageList(16, 16)
        iPos2Chk=0
        if self.__isLogDebug__():
            bDbg=True
            self.__logDebug__('valid:%s;include base=%d'%(valid,self.bIncludeBase))
        else:
            bDbg=False
        if self.bIncludeBase:
            l=self.objRegNode.GetTagNames2IncludeBase()
            if self.__isLogDebug__():
                self.__logDebug__('valid:%s;l:%s'%(self.__logFmt__(valid),self.__logFmt__(l)))
            if l is not None:
                i=0
                for s in l:
                    if valid is None:
                        valid=[s]
                    else:
                        valid.insert(i,s)
                    i+=1
                iPos2Chk=i
            else:
                s=self.objRegNode.GetTagName()
                if valid is None:
                    valid=[s]
                else:
                    if s in valid:
                        valid.remove(s)
                    valid.insert(0,s)
                iPos2Chk=1
            if self.__isLogDebug__():
                self.__logDebug__('valid:%s'%(self.__logFmt__(valid)))
        iPos=0
        for v in valid:
            o=self.doc.GetRegisteredNode(v)
            if iPos>=iPos2Chk:
                if o.IsNotContained():
                    iPos+=1
                    continue
                if self.objRegNode.IsNotContainedChild(v):
                    iPos+=1
                    continue
            iPos+=1
            pnCls=o.GetPanelClass()
            if pnCls is None:
                continue
            bmp=o.GetBitmap()
            if bmp is None:
                bmp=vtArt.getBitmap(vtArt.Invisible())
            il.Add(bmp)
            self.lstName.append(v)
        if bDbg:
            self.__logDebug__('lstName:%s'%(self.__logFmt__(self.lstName)))
        self.AssignImageList(il)
        i=0
        iPos=0
        sz= self.GetSize()
        for v in valid:
            o=self.doc.GetRegisteredNode(v)
            if iPos>=iPos2Chk:
                if o.IsNotContained():
                    iPos+=1
                    continue
                if self.objRegNode.IsNotContainedChild(v):
                    iPos+=1
                    continue
            iPos+=1
            pnCls=o.GetPanelClass()
            if pnCls is None:
                continue
            try:
                panel=pnCls(self,wx.NewId(),pos=(-1,-1),size=(-1,-1),
                        style=wx.TAB_TRAVERSAL,name=v+'RegPanel')
                panel.SetRegNode(o)
                panel.SetDoc(self.doc,self.bNet)
                self.AddPage(panel,o.GetDescription(),i==0,i)
                self.lstPanel.append(panel)
            except:
                import traceback
                traceback.print_exc()
                self.__logTB__()
                self.__logCritical__('setting up regNode:%s faulty'%(v))
            i+=1
    def ClearWid(self):
        self.__logError__('%s;replace with __Clear__'%(self.__class__.__name__))
        try:
            for pn in self.lstPanel:
                pn.ClearWid()
            vtXmlNodePanel.ClearWid(self)
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def __Clear__(self):
        for pn in self.lstPanel:
            try:
                if hasattr(pn,'__Clear__'):
                    f=getattr(pn,'__Clear__')
                    f()
                    #pn.ClearWid()
                else:
                    self.__logError__('%s;implement __Clear__'%(pn.__class__.__name__))
            except:
                self.__logTB__()
                self.__logCritical__(''%())
    def ClearInt(self):
        vtXmlNodePanel.ClearInt(self)
        for pn in self.lstPanel:
            pn.ClearInt()
    def Clear(self):
        for pn in self.lstPanel:
            pn.Clear()
        vtXmlNodePanel.Clear(self)
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        #for pn in self.lstPanel:
        #    pn.Lock(flag)
    def SetModified(self,flag):
        for pn in self.lstPanel:
            pn.SetModified(flag)
    def __isModified__(self):
        for pn in self.lstPanel:
            if pn.GetModified():
                return True
        return False
    def GetModified(self):
        return self.__isModified__()
    def SetNetDocs(self,d):
        if VERBOSE>0:
            self.__logDebug__(''%())
        for pn in self.lstPanel:
            try:
                pn.SetNetDocs(d)
            except:
                pass
    def SetNetMaster(self,netMaster):
        if VERBOSE>0:
            self.__logDebug__(''%())
        for pn in self.lstPanel():
            try:
                pn.SetNetMaster(netMaster)
            except:
                pass
    def SetRegNode(self,obj,bIncludeBase=False):
        try:
            if self.__isLogDebug__():
                self.__logDebug__('bIncludeBase:%d'%(bIncludeBase))
            self.bIncludeBase=bIncludeBase
            vtXmlNodePanel.SetRegNode(self,obj)
            self.__buildPanels__()
        except:
            self.__logTB__()
    def SetDoc(self,doc,bNet=False):
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        self.bNet=bNet
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        for w in self.lstPanel:
            if hasattr(w,'__SetDoc__'):
                w.__SetDoc__(doc,bNet=bNet,dDocs=dDocs)
    def SetDocPanels(self,doc,bNet=False):
        for pn in self.lstPanel:
            pn.SetDoc(doc,bNet)
    def __SetNode__(self,node,*args,**kwargs):
        try:
            sTagName=self.doc.getTagName(node)
            id=self.doc.getKey(node)
            self.__logDebug__('id:%s;tagName:%s'%(id,sTagName))
    
            i=0
            if self.bIncludeBase:
                l=self.objRegNode.GetTagNames2IncludeBase()
                if l is not None:
                    for s in l:
                        if self.__isLogDebug__():
                            self.__logDebug__('id:%s;tagName:%s;i:%d;s:%s'%(id,sTagName,i,s))
                        self.lstPanel[i].SetNode(node)
                        i+=1
                else:
                    if self.__isLogDebug__():
                        self.__logDebug__('id:%s;tagName:%s;i:%d'%(id,sTagName,i))
                    self.lstPanel[i].SetNode(node)
                    i+=1
            for s in self.lstName[i:]:
                #if self.bIncludeBase:
                #    if i==0:
                #        i+=1
                #        continue
                if self.__isLogDebug__():
                    self.__logDebug__('id:%s;tagName:%s;i:%d;s:%s'%(id,sTagName,i,s))
                o=self.doc.GetRegisteredNode(s)
                if o.UseBaseNode():
                    self.lstPanel[i].SetNode(node)
                else:
                    tmp=self.doc.getChild(node,s)
                    self.lstPanel[i].SetNode(tmp)
                i+=1
        except:
            self.__logTB__()
    def SetNode(self,node,*args,**kwargs):
        try:
            #if vtXmlNodePanel.SetNode(self,node)<0:
            #    return
            sTagName=self.doc.getTagName(node)
            id=self.doc.getKey(node)
            self.__logDebug__('id:%s;tagName:%s'%(id,sTagName))
            if self.objRegNode.GetTagName()!=sTagName:
                vtXmlNodePanel.Clear(self)    # 060928
                for wid in self.lstPanel:
                    wid.Clear()
                return
                #l=self.objRegNode.GetTagNames2IncludeBase()
            iRet=self.__SetNodeCheckPerm__(node)
            if iRet<0:
                return iRet
            #vtXmlNodePanel.SetNode(self,node)    # 060807
            self.__SetNode__(node,*args,**kwargs)
            return iRet
        except:
            self.__logTB__()
        return -9
    #def GetNode(self,node=None,func=None,**kwargs):
    def __GetNode__(self,node):
        try:
            #node=self.GetNodeStart(node)
            #if node is None:
            #    return
            i=0
            if self.bIncludeBase:
                l=self.objRegNode.GetTagNames2IncludeBase()
                if l is not None:
                    for s in l:
                        self.lstPanel[i].GetNode(node)
                        i+=1
                else:
                    self.lstPanel[i].GetNode(node)
                    i+=1
            for s in self.lstName[i:]:
                #if self.bIncludeBase:
                #    if i==0:
                #        i+=1
                #        continue
                o=self.doc.GetRegisteredNode(s)
                if o.UseBaseNode():
                    self.lstPanel[i].GetNode(node)
                else:
                    tmp=self.doc.getChild(node,s)
                    if tmp is None:
                        # check acl first
                        if self.doc.IsAclOk(s,self.doc.ACL_MSK_ADD)==False:
                            continue
                        obj,tmp=self.doc.CreateNode(node,s)
                    self.lstPanel[i].GetNode(tmp)
                i+=1
            #if func is not None:
            #    self.doc.DoCallBack(func,**kwargs)
        except:
            self.__logTB__()
    def OnPageChanged(self, event):
        old = event.GetOldSelection()
        sel = event.GetSelection()
        if sel>=len(self.lstPanel):
            return
        #try:
        #    self.nodeSet.index(sel)
        #except:
        #    self.lstPanel[sel].SetNode(self.lstName[sel],self.node)
        #    self.nodeSet.append(sel)
        event.Skip()

    def OnPageChanging(self, event):
        sel = event.GetSelection()
        event.Skip()
    def __getPanel__(self,name):
        try:
            idx=self.lstName.index(name)
            panel=self.lstPanel[idx]
            return panel
        except:
            return None
    