#----------------------------------------------------------------------------
# Name:         vtXmlGroupingTreeList.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtXmlGroupTreeList.py,v 1.2 2007/07/28 14:43:34 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import wx
import wx.gizmos

import vtXmlDom as vtXmlDom
import vidarc.tool.log.vtLog as vtLog

import thread,types,sys,string

import images

IMG_LIST=None
IMG_DICT=None

# defined event for vgpXmlTree item selected
wxEVT_THREAD_GROUP_TREELIST_ELEMENTS=wx.NewEventType()
def EVT_THREAD_GROUP_TREELIST_ELEMENTS(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_GROUP_TREELIST_ELEMENTS,func)
class wxThreadGroupTreeListElements(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_GROUP_TREELIST_ELEMENTS(<widget_name>, self.OnItemSel)
    """

    def __init__(self,iVal):
        wx.PyEvent.__init__(self)
        self.val=iVal
        self.SetEventType(wxEVT_THREAD_GROUP_TREELIST_ELEMENTS)
    def GetValue(self):
        return self.val

# defined event for vgpXmlTree item selected
wxEVT_THREAD_GROUP_TREELIST_ELEMENTS_FINISHED=wx.NewEventType()
def EVT_THREAD_GROUP_TREELIST_ELEMENTS_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_GROUP_TREELIST_ELEMENTS_FINISHED,func)
class wxThreadGroupTreeListElementsFinished(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_GROUP_TREELIST_ELEMENTS_FINISHED(<widget_name>, self.OnItemSel)
    """
    def __init__(self):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_THREAD_GROUP_TREELIST_ELEMENTS_FINISHED)

# defined event for vgpXmlTree item selected
wxEVT_THREAD_GROUP_TREELIST_ELEMENTS_ABORTED=wx.NewEventType()
def EVT_THREAD_GROUP_TREELIST_ELEMENTS_ABORTED(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_GROUP_TREELIST_ELEMENTS_ABORTED,func)
class wxThreadGroupTreeListElementsAborted(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_GROUP_TREELIST_ELEMENTS_ABORTED(<widget_name>, self.OnItemSel)
    """
    def __init__(self):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_THREAD_GROUP_TREELIST_ELEMENTS_ABORTED)


class thdGroupTreeList:
    def __init__(self,doc=None,verbose=0):
        self.verbose=verbose
        self.doc=doc
        self.running=False
    def Build(self,grps,sorting,idxSum,idxLim,lstCtrl,par):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.par=par
        self.sorting=sorting
        self.idxSum=idxSum
        self.idxLim=idxLim
        self.lstCtrl=lstCtrl
        self.grps=grps
        self.iAct=0
        if self.par is not None:
            self.updateProcessbar=True
        else:
            self.updateProcessbar=False
        
        self.Start()
    def Clear(self):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.iAct=0
            
    def Show(self):
        self.bFind=False
        if self.running==False:
            self.Start()
    def Start(self):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.keepGoing = self.running = True
        thread.start_new_thread(self.Run, ())

    def Stop(self):
        self.keepGoing = False

    def IsRunning(self):
        return self.running
    
    def __showNodes__(self,lst):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        for node in lst:
            self.iAct+=1
            if self.updateProcessbar:
                wx.PostEvent(self.par,wxThreadGroupTreeListElements(self.iAct))
            self.doc.acquire()
            tagName,infos=self.doc.getNodeInfos(node)
            try:
                lst=self.doc.__procGroup__(node,tagName,infos)
                self.lst.append(lst)
            except Exception,list:
                if self.verbose>0:
                    vtLog.vtLngCallStack(self,vtLog.DEBUG,'exception:%s'%list)
                self.Stop()
                self.doc.release()
                return -1
            self.doc.release()
        return 0
    def __procDict__(self,d,level,prefix,pti):
            if self.verbose>0:
                vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
            keys=d.keys()
            keys.sort()
            sumTotal=0.0
            limTotal=0.0
            for k in keys:
                v=d[k]
                if types.DictType==type(v):
                    if self.verbose>0:
                        vtLog.vtLngCallStack(self,vtLog.DEBUG,'key:%s'%k,callstack=False)
                    ti=self.doc.__addTreeItem__(pti,k,level,prefix,v)
                    sum,lim=self.__procDict__(v,level+1,prefix+[k],ti)
                    self.doc.SetItemText(ti,'%6.2f'%sum,self.doc.mapping[self.idxSum])
                    if len(prefix)>0:
                        lim=self.doc.getLimit(prefix+[k])
                    if self.idxLim>=0:
                        self.doc.SetItemText(ti,'%6.2f'%lim,self.idxLim)
                    #self.doc.SetItemBold(ti,True)
                    #idx=self.grp[level]
                else:
                    if self.verbose>0:
                        vtLog.vtLngCallStack(self,vtLog.DEBUG,'key:%s'%k,callstack=False)
                    self.lst=[]
                    ti=self.doc.__addTreeItem__(pti,k,level+1,prefix,v)
                    if self.__showNodes__(v)<0:
                        #self.keepGoing = False
                        sum=0.0
                        lim=0.0
                    else:
                        sum,lim=self.doc.__outputNodes__(self.lst,k,level+1,prefix+[k],ti)
                sumTotal+=sum
                #lim=self.doc.getLimit(prefix)
                limTotal+=lim
                #if lim>limTotal:
                #    limTotal=lim
                if self.keepGoing == False:
                    return 0.0 , 0.0
            return sumTotal,limTotal
        
    def Run(self):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.Clear()
        
        if self.updateProcessbar:
            wx.PostEvent(self.par,wxThreadGroupTreeListElements(0))
        try:
            self.doc.DeleteAllItems()
            pti=self.doc.AddRoot('Result')
            self.sumTotal,self.limTotal=self.__procDict__(self.grps,0,[],pti)
            self.doc.SetItemText(pti,'%6.2f'%self.sumTotal,self.doc.mapping[self.idxSum])
            if self.idxLim>=0:
                self.doc.SetItemText(pti,'%6.2f'%self.limTotal,self.idxLim)
            self.doc.Expand(pti)
        except Exception,list:
            if self.verbose>0:
                vtLog.vtLngCallStack(self,vtLog.DEBUG,'exception:%s',list)
        
        #index = self.lstCtrl.InsertImageStringItem(sys.maxint, '', -1)
        #if self.idxSum >= 0:
        #    self.lstCtrl.SetStringItem(index,self.idxSum,'%6.2f'%self.sumTotal,-1)
        
        if self.updateProcessbar:
            wx.PostEvent(self.par,wxThreadGroupTreeListElements(0))
            if self.keepGoing:
                wx.PostEvent(self.par,wxThreadGroupTreeListElementsFinished())
            else:
                wx.PostEvent(self.par,wxThreadGroupTreeListElementsAborted())
        self.keepGoing = self.running = False
        

class vtXmlGroupTreeList(wx.gizmos.TreeListCtrl,vtXmlDom.vtXmlDom):
    def __init__(self, parent, id=-1, pos=wx.Point(0, 0), size=wx.Size(100, 50), 
                style=wx.TR_HAS_BUTTONS, name='vtXmlGrpTreeList',verbose=0):
        vtXmlDom.vtXmlDom.__init__(self)
        if id<0:
            id=wx.NewId()
        wx.gizmos.TreeListCtrl.__init__(self,id=id, name=name,
              parent=parent, pos=pos, size=size,style=style)
        self.verbose=verbose
        self.groups={}
        self.outputs=[]
        self.grouping=[]
        self.groupingIdx=[]
        self.mapping={}
        self.limits={}
        self.thdGroupTreeList=thdGroupTreeList(self,verbose=verbose-1)
        self.SetupImageList()
        wx.EVT_TREE_ITEM_COLLAPSED(self,id,self.OnTcNodeCollapsed)
        wx.EVT_TREE_ITEM_EXPANDED(self,id,self.OnTcNodeExpanded)
    def OnTcNodeCollapsed(self,evt):
        tn=evt.GetItem()
        self.SetItemBold(tn,False)
    def OnTcNodeExpanded(self,evt):
        tn=evt.GetItem()
        self.SetItemBold(tn,True)
    def setShow(self,showing,mapping):
        self.groups={}
        self.outputs=[]
        self.showing=showing
        self.mapping=mapping
        self.DeleteAllItems()
        
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
    def IsRunning(self):
        return self.thdGroupTreeList.IsRunning()
    def __addTreeItem__(self,pti,sLabel,level,prefix,d):
        #if self.verbose>0:
        #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'label:%s level:%d prefix:%s'%(label,level,prefix),callstack=False)
        tid=wx.TreeItemData()
        tid.SetData(d)
        img=imgSel=imgSel=-1
        #sLabel=string.join(prefix+[label],' ')
        tn=self.AppendItem(pti,sLabel,-1,-1,tid)
        self.SetItemImage(tn,img,0,wx.TreeItemIcon_Normal)
        self.SetItemImage(tn,imgSel,0,wx.TreeItemIcon_Expanded)
        self.SetItemImage(tn,imgSel,0,wx.TreeItemIcon_Selected)
        return tn
    def __procGroup__(self,node,tagName,infos):
        lst=[]
        for grp in self.showing:
            k=grp[0]
            if grp[1] is None:
                val=infos[k]
            else:
                val=grp[1](infos[k])
            lst.append(val)
        return lst
    def getLimit(self,prefix):
            if self.idxLim>=0:
                dLim=self.limits
                fLimit=0.0
                for k in prefix:
                    try:
                        tup=dLim[k]
                        fLimit=tup[0]
                        dLim=tup[1]
                    except:
                        dLim=None
                        fLimit=-1.0
                return fLimit
            return -1.0
    def __outputNodes__(self,lst,sLabel,level,prefix,pti):
        def compFunc(a,b):
            for i in self.sorting:
                res=cmp(a[i],b[i])
                if res!=0:
                    return res
            return 0
        lst.sort(compFunc)
        
        tupRes=[]
        for it in self.showing:
            tupRes.append(None)
        if self.idxSum>=0:
            tupRes[self.idxSum]=0.0
        iCount=len(self.showing)
        
        for tup in lst:
            for i in range(iCount):
                if i==self.idxSum:
                    tupRes[i]+=float(tup[i])
                else:
                    if tupRes[i] is None:
                        tupRes[i]=tup[i]
                    else:
                        if tupRes[i]!=tup[i]:
                            tupRes[i]='*'
            tin=self.__addTreeItem__(pti,string.join(prefix,' '),level,prefix,tup)
            #index = self.lstCtrl.InsertImageStringItem(sys.maxint, tup[0], -1)
            i=0
            for t in tup:
                self.SetItemText(tin,t,self.mapping[i])
                i+=1
        for i in range(iCount):
            if tupRes[i] is None:
                tupRes[i]=''
        #index = self.lstCtrl.InsertImageStringItem(sys.maxint, tupRes[0], -1)
        i=0
        for t in tupRes:
            if self.idxSum==i:
                self.SetItemText(pti,'%6.2f'%t,self.mapping[i])
            else:
                self.SetItemText(pti,t,self.mapping[i])
            i+=1
        fLimit=self.getLimit(prefix)
        tupRes.append(fLimit)
        if self.idxLim>=0:
            self.SetItemText(pti,'%6.2f'%fLimit,self.idxLim)
        #self.SetItemBold(pti,True)
        if self.idxSum>=0:
            sum=tupRes[self.idxSum]
        else:
            sum=0.0
        self.outputs.append([prefix,sLabel,sum,lst,tupRes])
        #index = self.lstCtrl.InsertImageStringItem(sys.maxint, '', -1)
        return sum,fLimit

    def Build(self,master,groups,limits,sorting,idxSum,idxLim,par=None):
        if self.IsRunning():
            return
        del self.outputs
        self.outputs=[]
        self.groups=groups
        self.master=master
        self.limits=limits
        self.sorting=sorting
        self.idxSum=idxSum
        self.idxLim=idxLim
        self.DeleteAllItems()
        self.thdGroupTreeList.Build(groups,sorting,idxSum,idxLim,self,par)
        
    def SetupImageList(self):
        global IMG_LIST
        global IMG_DICT
        if IMG_LIST is None:
            self.imgDict={}
            self.imgLstTyp=wx.ImageList(16,16)
            img=images.getElementBitmap()
            self.imgDict['dft']=[self.imgLstTyp.Add(img)]
            img=images.getElementSelBitmap()
            self.imgDict['dft'].append(self.imgLstTyp.Add(img))
            IMG_LIST=self.imgLstTyp
            IMG_DICT=self.imgDict
        else:
            self.imgLstTyp=IMG_LIST
            self.imgDict=IMG_DICT
        self.SetImageList(self.imgLstTyp)
    def GetOutputs(self):
        return self.outputs
    def GetGrouped(self):
        return self.groups
    def GetGrouping(self):
        return self.grouping
    def GetGroupingIdx(self):
        return self.groupingIdx
    def SetGrouping(self,grouping,groupingIdx):
        self.grouping=grouping
        self.groupingIdx=groupingIdx
    def GetShowing(self):
        return self.showing
    def GetSorting(self):
        return self.sorting
    def GetSumIdx(self):
        return self.idxSum
    def GetLimIdx(self):
        return self.idxLim
    