#----------------------------------------------------------------------------
# Name:         vtXmlDiff.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtXmlDiff.py,v 1.7 2008/03/11 20:50:16 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog

import string,types

class vtXmlDiff:
    INFO        =   0
    ATTR        =   1
    CHILDS      =   2
    def __init__(self,doc,verbose=0):
        self.verbose=verbose
        self.doc=None
        self.SetDoc(doc)
        self.Clear()
    def SetDoc(self,doc):
        if self.verbose:
            vtLog.CallStack('')
        self.doc=doc
    def __buildInfoDict__(self):
        return {self.INFO:[],self.ATTR:[],self.CHILDS:[]}
    def __getResInfoLst__(self):
        lst=[]
        for i in range(len(self.infos)):
            lst.append(None)
        return lst
    def __buildResDict__(self):
        return {self.INFO:{},self.ATTR:{},self.CHILDS:{}}
    def Clear(self):
        #if self.verbose:
        #    vtLog.CallStack('')
        self.infos=[]
        self.res=self.__buildInfoDict__()
    def __buildAttr__(self,node,bSkipFP=False,doc=None):
        #if self.verbose:
        #    vtLog.CallStack(repr(node))
        lst=[]
        if node is None:
            return lst
        if doc is None:
            doc=self.doc
        lst=doc.getAttrsValues(node)
        if bSkipFP==False:
            pass
        #lst.sort(compFunc)
        return lst
    def __getTagName__(self,node,doc=None,lKeyAttr=None):
        if doc is None:
            doc=self.doc
        if doc is None:
            return ''
        lst=self.__buildAttr__(node,True,doc=doc)
        if len(lst)>0:
            if lKeyAttr is None:
                s=u'|'.join([doc.getTagName(node)]+[u'='.join([k,v]) for k,v in lst])# if k in ['id','language']])
            else:
                s=u'|'.join([doc.getTagName(node)]+[u'='.join([k,v]) for k,v in lst if k in lKeyAttr])
        else:
            s=doc.getTagName(node)
        return s
    def __buildInfo__(self,d,node,bRec=False,doc=None,lKeyAttr=['id','language']):
        if doc is None:
            doc=self.doc
        if doc is None:
            return
        if self.verbose>2:
            #vtLog.CallStack(repr(d)+'\n'+repr(node)+'\n'+repr(bRec))
            vtLog.vtLngCurCls(vtLog.DEBUG,'d;%s;bRec:%d'%(vtLog.pformat(d),bRec),self)
        lst=d[self.INFO]
        childs=doc.getChildsNoAttr(node,doc.attr)
        for c in childs:
            sVal=doc.getText(c)
            if len(sVal.strip())==0:
                sVal=''
            lst.append((self.__getTagName__(c,doc=doc),sVal))
        d[self.ATTR]=self.__buildAttr__(node,doc=doc)
        lst=d[self.CHILDS]
        for c in doc.getChilds(node):
            if len(doc.getKey(c))==0:
                if not doc.hasChilds(c):
                    continue
                    #dC=self.__buildInfoDict__()
                    #self.__buildInfo__(dC,c,bRec)
            else:
                if bRec==False:
                    if doc.__isNode2CalcFingerPrint__(c)==False:
                        continue
            dC=self.__buildInfoDict__()
            lst.append((self.__getTagName__(c,doc=doc,lKeyAttr=lKeyAttr),dC,c))
        def compFunc(a,b):
            return cmp(a[0],b[0])
        lst.sort(compFunc)
        for tagName,dC,c in lst:
            self.__buildInfo__(dC,c,True,doc=doc)
        if self.verbose>2:
            #vtLog.CallStack(repr(d)+'\n'+repr(node)+'\n'+repr(bRec))
            vtLog.vtLngCurCls(vtLog.DEBUG,'d;%s;bRec:%d'%(vtLog.pformat(d),bRec),self)
        
    def AddInfo(self,node,bRec=False,doc=None,lKeyAttr=['id','language']):
        d=self.__buildInfoDict__()
        self.__buildInfo__(d,node,bRec,doc=doc,lKeyAttr=lKeyAttr)
        self.infos.append((d,node))
        if self.verbose:
            #vtLog.CallStack(repr(d))
            #print node
            vtLog.vtLngCurCls(vtLog.DEBUG,'d;%s'%(vtLog.pformat(d)),self)
    def __getUnique__(self,dRes,kind,tag,bForce=False):
        d=dRes[kind]
        if tag in d:
            if bForce:
                tagUnique=''.join([tag,'[%d]'%0])
                if  tagUnique not in d:
                    d[tagUnique]=d[tag]
                    lst=d[tag]
                    d[tag]=None
                    #return tagUnique,lst
                for i in xrange(1,1000):
                    tagUnique=''.join([tag,'[%d]'%i])
                    if tagUnique not in d:
                        lst=self.__getResInfoLst__()
                        d[tagUnique]=lst
                        return tagUnique,lst
            else:
                lst=d[tag]
        else:
            lst=self.__getResInfoLst__()
            d[tag]=lst
        return tag,lst
    def __buildDiff__(self,dRes,idx1,d1,idx2,d2):
        if self.doc is None:
            return False,{}
        if self.verbose>1:
        #    vtLog.CallStack('')
            vtLog.vtLngCurCls(vtLog.DEBUG,'idx1:%d;d1;%s;idx2:%d;d2;%s'%(idx1,vtLog.pformat(d1),
                        idx2,vtLog.pformat(d2)),self)
            
        bDiffFound=False
        for kind in [self.INFO,self.ATTR,self.CHILDS]:
            #if self.verbose:
            #    print '  check diff',kind
            lst1=d1[kind]
            lst2=d2[kind]
            iLen1=len(lst1)
            iLen2=len(lst2)
            if iLen1<iLen2:
                iLen=iLen2
            else:
                iLen=iLen1
            i=0
            j=0
            bGo=True
            while bGo:
                dResChild=None
                try:
                    tagName1=lst1[i][0]
                except:
                    tagName1=None
                try:
                    tagName2=lst2[j][0]
                except:
                    tagName2=None
                bAdd1=False
                bAdd2=False
                if tagName1 is not None:
                    if tagName2 is None:
                        iCmpTag=-1
                    else:
                        iCmpTag=cmp(tagName1,tagName2)
                else:
                    if tagName2 is None:
                        # should never happen
                        break
                    else:
                        iCmpTag=1
                if self.verbose>1:
                    #print '     ',tagName1,tagName2,iCmpTag
                    vtLog.vtLngCurCls(vtLog.DEBUG,'tag1:%s;tag2:%s;cmp:%d'%(tagName1,tagName2,iCmpTag),self)
                if iCmpTag==0:
                    # check
                    val1=lst1[i][1]
                    val2=lst2[j][1]
                    if self.verbose>1:
                        #print '         ','<%s>'%val1
                        #print '         ','<%s>'%val2
                        vtLog.vtLngCurCls(vtLog.DEBUG,'val1:%s;val2:%s'%(vtLog.pformat(val1),vtLog.pformat(val2)),self)
                    bAdd=False
                    if type(val1)==types.DictType:
                        # check recursive before adding
                        #try:
                        #    dResChild=dRes[kind][tagName1]
                        #except:
                        #    dResChild=self.__buildResDict__()
                        dResChild=self.__buildResDict__()
                        iRes,dResChild=self.__buildDiff__(dResChild,idx1,val1,idx2,val2)
                        if iRes:
                            if self.verbose>1:
                                #print '             ','child difference'
                                #self.__showResDict__(dResChild,2)
                                vtLog.vtLngCurCls(vtLog.DEBUG,'dResChild;%s'%(vtLog.pformat(dResChild)),self)
                            bAdd1=True
                            bAdd2=False
                        else:
                            del dResChild
                            dResChild=None
                        pass
                    else:
                        if cmp(val1,val2)!=0:
                            bAdd1=True
                            bAdd2=True
                    bDiffFound=True
                    i+=1
                    j+=1
                elif iCmpTag<0:
                    bAdd1=True
                    bDiffFound=True
                    i+=1
                elif iCmpTag>0:
                    bAdd2=True
                    bDiffFound=True
                    j+=1
                if self.verbose>1:
                    vtLog.vtLngCurCls(vtLog.DEBUG,'kind:%d;bAdd:%d %d;dRes;%s;dResChild;%s'%(kind,
                            bAdd1,bAdd2,vtLog.pformat(dRes),vtLog.pformat(dResChild)),self)
                if bAdd1:
                    sTag,lst=self.__getUnique__(dRes,kind,tagName1,bForce=True)
                    if lst[idx1] is None:
                        if dResChild is None:
                            lst[idx1]=lst1[i-1]
                            if kind==self.CHILDS:
                                lst=self.__buildDiffSingle__(0,lst1[i-1][1])
                            else:
                                lst[idx1]=(lst1[i-1][0],self.__buildDiffSingle__(0,lst1[i-1][1]))
                        else:
                            lst=dResChild
                            #if self.verbose:
                            #    print 'child res'
                            #    print lst[idx1]
                        dRes[kind][sTag]=lst
                    if 0:
                        try:
                            lst=dRes[kind][tagName1]
                            if lst[idx1] is None:
                                if dResChild is None:
                                    lst[idx1]=lst1[i-1]
                                    #lst[idx1]=self.__buildDiffSingle__(lst1[i-1])
                                    lst[idx1]=(lst1[i-1][0],self.__buildDiffSingle__(0,lst1[i-1][1]))
                                else:
                                    lst[idx1]=dResChild
                        except:
                            lst=self.__getResInfoLst__()
                            if dResChild is None:
                                lst[idx1]=lst1[i-1]
                                if kind==self.CHILDS:
                                    lst=self.__buildDiffSingle__(0,lst1[i-1][1])
                                else:
                                    lst[idx1]=(lst1[i-1][0],self.__buildDiffSingle__(0,lst1[i-1][1]))
                            else:
                                lst=dResChild
                                #if self.verbose:
                                #    print 'child res'
                                #    print lst[idx1]
                            dRes[kind][tagName1]=lst
                if self.verbose>1:
                    vtLog.vtLngCurCls(vtLog.DEBUG,'kind:%d;bAdd:%d %d;dRes;%s'%(kind,bAdd1,bAdd2,vtLog.pformat(dRes)),self)
                if bAdd2:
                    if bAdd1==False:
                        sTag,lst=self.__getUnique__(dRes,kind,tagName2,bForce=True)
                    if lst[idx2] is None:
                        if dResChild is None:
                            lst[idx2]=lst2[j-1]
                            if kind==self.CHILDS:
                                lst=self.__buildDiffSingle__(1,lst2[j-1][1])
                            else:
                                lst[idx2]=(lst2[j-1][0],self.__buildDiffSingle__(1,lst2[j-1][1]))
                        else:
                            lst=dResChild
                        dRes[kind][sTag]=lst
                    if 0:
                        try:
                            lst=dRes[kind][tagName2]
                            if lst[idx2] is None:
                                if dResChild is None:
                                    lst[idx2]=lst2[j-1]
                                    #lst[idx2]=self.__buildDiffSingle__(lst2[j-1])
                                    lst[idx2]=(lst2[j-1][0],self.__buildDiffSingle__(1,lst2[j-1][1]))
                                else:
                                    lst[idx2]=dResChild
                                    #if self.verbose:
                                    #    print lst
                        except:
                            lst=self.__getResInfoLst__()
                            if dResChild is None:
                                lst[idx2]=lst2[j-1]
                                if kind==self.CHILDS:
                                    lst=self.__buildDiffSingle__(1,lst2[j-1][1])
                                else:
                                    lst[idx2]=(lst2[j-1][0],self.__buildDiffSingle__(1,lst2[j-1][1]))
                            else:
                                lst=dResChild
                            dRes[kind][tagName2]=lst
                if self.verbose>1:
                    vtLog.vtLngCurCls(vtLog.DEBUG,'kind:%d;bAdd:%d %d;dRes;%s'%(kind,bAdd1,bAdd2,vtLog.pformat(dRes)),self)
                if i>=iLen1:
                    i=iLen1
                if j>=iLen2:
                    j=iLen2
                if i==iLen1 and j==iLen2:
                    bGo=False
            if dRes is not None:
                d=dRes[kind]
                l=[k for k,v in d.iteritems() if v is None]
                for k in l:
                    del d[k]
        bDiffFound=False
        if dRes is not None:
            for kind in [self.INFO,self.ATTR,self.CHILDS]:
                if len(dRes[kind])>0:
                    bDiffFound=True
                    break
        return bDiffFound,dRes
    def __buildDiffSingle__(self,idx,d1):
        if self.verbose>1:
        #    vtLog.CallStack('')
            vtLog.vtLngCurCls(vtLog.DEBUG,'d;%s'%(vtLog.pformat(d1)),self)
        if type(d1)!=types.DictType:
            return d1[:]
        dRes={}
        for kind in [self.INFO,self.ATTR,self.CHILDS]:
            #if self.verbose:
            #    print '  check diff',kind
            lst1=d1[kind]
            lKind={}
            dRes[kind]=lKind
            for t in lst1:
                val1=t[1]
                if type(val1)==types.DictType:
                    dResChild=self.__buildDiffSingle__(idx,val1)
                    if kind==self.CHILDS:
                        lKind[t[0]]=dResChild
                    else:
                        l=[None,None]
                        l[idx]=(t[0],dResChild)
                        #lKind.append(l)
                        lKind[t[0]]=l
                    #lKind.append((t[0],dResChild))
                else:
                    l=[None,None]
                    l[idx]=(t[0],t[1][:])
                    #lKind.append(l)
                    lKind[t[0]]=l
                    #lKind.append((t[0],t[1][:]))
        if self.verbose>1:
        #    vtLog.CallStack('')
            vtLog.vtLngCurCls(vtLog.DEBUG,'dRes;%s'%(vtLog.pformat(dRes)),self)
        return dRes
    def CalcDiff(self):
        #if self.verbose:
        #    vtLog.CallStack('')
        self.res=self.__buildResDict__()
        if self.doc is None:
            return False
        try:
            dRef,refChild=self.infos[0]
            idx=1
            for tup in self.infos[1:]:
                self.__buildDiff__(self.res,0,dRef,idx,tup[0])
                idx+=1
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        if self.verbose:
            #vtLog.CallStack('')
            #self.__showResDict__(self.res)
            vtLog.vtLngCurCls(vtLog.DEBUG,'res;%s'%(vtLog.pformat(self.res)),self)
        return self.IsDiff()
    def IsDiff(self):
        for kind in [self.INFO,self.ATTR,self.CHILDS]:
            d=self.res[kind]
            if len(d.keys())>0:
                return True
        return False
    def GetDiff(self):
        #if self.verbose:
        #    vtLog.CallStack('')
        return self.res
    def PrintRes(self):
        self.__showResDict__(self.GetDiff())
    def __getSpace__(self,lv=0):
        sSpace=''
        for i in range(lv):
            sSpace+='  '
    def __showResDict__(self,dRes,lv=0):
        sSpace=''
        for i in range(lv):
            sSpace+='  '
        try:
            for kind in [self.INFO,self.ATTR,self.CHILDS]:
                d=dRes[kind]
                print sSpace,'kind',kind
                keys=d.keys()
                keys.sort()
                for k in keys:
                    print sSpace,' ','<%s>'%k
                    if type(d[k])==types.DictType:
                        self.__showResDict__(d[k],lv+1)
                    else:
                        for it in d[k]:
                            print sSpace,'    ','<%s>'%it[1]
        except:
            pass
if __name__=='__main__':
    import vtXmlDom
    doc1=vtXmlDom.vtXmlDom()
    doc1.Open('testDiff01.xml')
    root=doc1.getRoot()
    childs1=doc1.getChilds(root)
    iLen=len(childs1)
    
    doc2=vtXmlDom.vtXmlDom()
    doc2.Open('testDiff02.xml')
    root=doc2.getRoot()
    childs2=doc2.getChilds(root)
    
    xmlCmp=vtXmlDiff(doc1,verbose=0)
    for i in [0,1,2,3,4,5]:
    #for i in [4]:
        print
        print 'diff test',i
        c1=childs1[i]
        c2=childs2[i]
        xmlCmp.Clear()
        xmlCmp.AddInfo(c1,True)
        xmlCmp.AddInfo(c2,True)
        xmlCmp.CalcDiff()
        xmlCmp.PrintRes()
    doc1.Close()
    doc2.Close()
