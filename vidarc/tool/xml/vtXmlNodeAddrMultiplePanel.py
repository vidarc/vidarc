#Boa:FramePanel:vtXmlNodeAddrMultiplePanel
#----------------------------------------------------------------------------
# Name:         vtXmlNodeAddrMultiplePanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060619
# CVS-ID:       $Id: vtXmlNodeAddrMultiplePanel.py,v 1.6 2010/03/29 08:52:35 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import wx
    import vidarc.tool.input.vtInputMultipledValues
    from wx.lib.anchors import LayoutAnchors
    
    from vidarc.tool.loc.vLocCountries import COUNTRIES
    from vidarc.tool.xml.vtXmlNodePanel import *
    
    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)


[wxID_VTXMLNODEADDRMULTIPLEPANEL, wxID_VTXMLNODEADDRMULTIPLEPANELCHCCOUNTRY, 
 wxID_VTXMLNODEADDRMULTIPLEPANELLBLCITY, 
 wxID_VTXMLNODEADDRMULTIPLEPANELLBLCOUNTRY, 
 wxID_VTXMLNODEADDRMULTIPLEPANELLBLDIST, 
 wxID_VTXMLNODEADDRMULTIPLEPANELLBLDISTUNIT, 
 wxID_VTXMLNODEADDRMULTIPLEPANELLBLNAME, 
 wxID_VTXMLNODEADDRMULTIPLEPANELLBLREGION, 
 wxID_VTXMLNODEADDRMULTIPLEPANELLBLSTREET, 
 wxID_VTXMLNODEADDRMULTIPLEPANELLBLZIP, 
 wxID_VTXMLNODEADDRMULTIPLEPANELSPNDIST, 
 wxID_VTXMLNODEADDRMULTIPLEPANELTXTCITY, 
 wxID_VTXMLNODEADDRMULTIPLEPANELTXTREGION, 
 wxID_VTXMLNODEADDRMULTIPLEPANELTXTSTREET, 
 wxID_VTXMLNODEADDRMULTIPLEPANELTXTZIP, 
 wxID_VTXMLNODEADDRMULTIPLEPANELVIMULTIPLE, 
] = [wx.NewId() for _init_ctrls in range(16)]

class vtXmlNodeAddrMultiplePanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_fgsAddr_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblName, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viMultiple, 2, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblStreet, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtStreet, 3, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblZip, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsCity, 3, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblRegion, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsCountry, 3, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblDist, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsDist, 3, border=0, flag=wx.EXPAND)

    def _init_coll_bxsDist_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.spnDist, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.lblDistUnit, 0, border=0, flag=wx.BOTTOM)

    def _init_coll_bxsCity_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.txtZip, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblCity, 1, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.txtCity, 2, border=0, flag=wx.EXPAND)

    def _init_coll_fgsAddr_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)
        parent.AddGrowableCol(0)
        parent.AddGrowableCol(1)

    def _init_coll_bxsCountry_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.txtRegion, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblCountry, 1, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.chcCountry, 2, border=0, flag=wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.bxsCity = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.fgsAddr = wx.FlexGridSizer(cols=2, hgap=8, rows=7, vgap=8)

        self.bxsDist = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsCountry = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_bxsCity_Items(self.bxsCity)
        self._init_coll_fgsAddr_Items(self.fgsAddr)
        self._init_coll_fgsAddr_Growables(self.fgsAddr)
        self._init_coll_bxsDist_Items(self.bxsDist)
        self._init_coll_bxsCountry_Items(self.bxsCountry)

        self.SetSizer(self.fgsAddr)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTXMLNODEADDRMULTIPLEPANEL,
              name=u'vtXmlNodeAddrMultiplePanel', parent=prnt, pos=wx.Point(221,
              315), size=wx.Size(426, 219), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(418, 192))
        self.SetAutoLayout(True)

        self.lblName = wx.StaticText(id=wxID_VTXMLNODEADDRMULTIPLEPANELLBLNAME,
              label=_(u'Name'), name=u'lblName', parent=self, pos=wx.Point(0,
              0), size=wx.Size(83, 24), style=wx.ALIGN_RIGHT)
        self.lblName.SetMinSize(wx.Size(-1, -1))

        self.lblStreet = wx.StaticText(id=wxID_VTXMLNODEADDRMULTIPLEPANELLBLSTREET,
              label=_(u'Street'), name=u'lblStreet', parent=self,
              pos=wx.Point(0, 32), size=wx.Size(83, 73), style=wx.ALIGN_RIGHT)
        self.lblStreet.SetMinSize(wx.Size(-1, -1))

        self.viMultiple = vidarc.tool.input.vtInputMultipledValues.vtInputMultipledValues(id=wxID_VTXMLNODEADDRMULTIPLEPANELVIMULTIPLE,
              name=u'viMultiple', parent=self, pos=wx.Point(91, 0),
              size=wx.Size(327, 24), style=0)
        self.viMultiple.Bind(vidarc.tool.input.vtInputMultipledValues.vEVT_VTINPUT_MULTIPLED_VALUES_DELETED,
              self.OnViMultipleVtinputMultipledValuesDeleted,
              id=wxID_VTXMLNODEADDRMULTIPLEPANELVIMULTIPLE)
        self.viMultiple.Bind(vidarc.tool.input.vtInputMultipledValues.vEVT_VTINPUT_MULTIPLED_VALUES_ADDED,
              self.OnViMultipleVtinputMultipledValuesAdded,
              id=wxID_VTXMLNODEADDRMULTIPLEPANELVIMULTIPLE)
        self.viMultiple.Bind(vidarc.tool.input.vtInputMultipledValues.vEVT_VTINPUT_MULTIPLED_VALUES_SELECTED,
              self.OnViMultipleVtinputMultipledValuesSelected,
              id=wxID_VTXMLNODEADDRMULTIPLEPANELVIMULTIPLE)
        self.viMultiple.Bind(vidarc.tool.input.vtInputMultipledValues.vEVT_VTINPUT_MULTIPLED_VALUES_CHANGED,
              self.OnViMultipleVtinputMultipledValuesChanged,
              id=wxID_VTXMLNODEADDRMULTIPLEPANELVIMULTIPLE)

        self.txtStreet = wx.TextCtrl(id=wxID_VTXMLNODEADDRMULTIPLEPANELTXTSTREET,
              name=u'txtStreet', parent=self, pos=wx.Point(91, 32),
              size=wx.Size(327, 73), style=wx.TE_MULTILINE, value=u'')
        self.txtStreet.SetMinSize(wx.Size(-1, -1))
        self.txtStreet.SetMaxSize(wx.Size(-1, 60))
        self.txtStreet.Bind(wx.EVT_TEXT, self.OnTxtStreetText,
              id=wxID_VTXMLNODEADDRMULTIPLEPANELTXTSTREET)

        self.lblZip = wx.StaticText(id=wxID_VTXMLNODEADDRMULTIPLEPANELLBLZIP,
              label=_(u'ZIP / Postal Code'), name=u'lblZip', parent=self,
              pos=wx.Point(0, 113), size=wx.Size(83, 21), style=wx.ALIGN_RIGHT)
        self.lblZip.SetMinSize(wx.Size(-1, -1))

        self.txtZip = wx.TextCtrl(id=wxID_VTXMLNODEADDRMULTIPLEPANELTXTZIP,
              name=u'txtZip', parent=self, pos=wx.Point(91, 113),
              size=wx.Size(79, 21), style=0, value=u'')
        self.txtZip.SetMinSize(wx.Size(-1, -1))
        self.txtZip.Bind(wx.EVT_TEXT, self.OnTxtZipText,
              id=wxID_VTXMLNODEADDRMULTIPLEPANELTXTZIP)

        self.lblCity = wx.StaticText(id=wxID_VTXMLNODEADDRMULTIPLEPANELLBLCITY,
              label=_(u'City'), name=u'lblCity', parent=self, pos=wx.Point(170,
              113), size=wx.Size(79, 21), style=wx.ALIGN_RIGHT)
        self.lblCity.SetMinSize(wx.Size(-1, -1))

        self.txtCity = wx.TextCtrl(id=wxID_VTXMLNODEADDRMULTIPLEPANELTXTCITY,
              name=u'txtCity', parent=self, pos=wx.Point(257, 113),
              size=wx.Size(159, 21), style=0, value=u'')
        self.txtCity.SetMinSize(wx.Size(-1, -1))
        self.txtCity.Bind(wx.EVT_TEXT, self.OnTxtCityText,
              id=wxID_VTXMLNODEADDRMULTIPLEPANELTXTCITY)

        self.lblRegion = wx.StaticText(id=wxID_VTXMLNODEADDRMULTIPLEPANELLBLREGION,
              label=_(u'Region'), name=u'lblRegion', parent=self,
              pos=wx.Point(0, 142), size=wx.Size(83, 21), style=wx.ALIGN_RIGHT)
        self.lblRegion.SetMinSize(wx.Size(-1, -1))

        self.txtRegion = wx.TextCtrl(id=wxID_VTXMLNODEADDRMULTIPLEPANELTXTREGION,
              name=u'txtRegion', parent=self, pos=wx.Point(91, 142),
              size=wx.Size(79, 21), style=0, value=u'')
        self.txtRegion.SetMinSize(wx.Size(-1, -1))
        self.txtRegion.Bind(wx.EVT_TEXT, self.OnTxtRegionText,
              id=wxID_VTXMLNODEADDRMULTIPLEPANELTXTREGION)

        self.lblCountry = wx.StaticText(id=wxID_VTXMLNODEADDRMULTIPLEPANELLBLCOUNTRY,
              label=_(u'Country'), name=u'lblCountry', parent=self,
              pos=wx.Point(170, 142), size=wx.Size(79, 21),
              style=wx.ALIGN_RIGHT)
        self.lblCountry.SetMinSize(wx.Size(-1, -1))

        self.chcCountry = wx.Choice(choices=[],
              id=wxID_VTXMLNODEADDRMULTIPLEPANELCHCCOUNTRY, name=u'chcCountry',
              parent=self, pos=wx.Point(257, 142), size=wx.Size(159, 21),
              style=0)
        self.chcCountry.SetMinSize(wx.Size(-1, -1))
        self.chcCountry.Bind(wx.EVT_CHOICE, self.OnChcCountryChoice,
              id=wxID_VTXMLNODEADDRMULTIPLEPANELCHCCOUNTRY)

        self.lblDistUnit = wx.StaticText(id=wxID_VTXMLNODEADDRMULTIPLEPANELLBLDISTUNIT,
              label=u'km', name=u'lblDistUnit', parent=self, pos=wx.Point(216,
              171), size=wx.Size(13, 13), style=0)
        self.lblDistUnit.SetMinSize(wx.Size(-1, -1))

        self.spnDist = wx.SpinCtrl(id=wxID_VTXMLNODEADDRMULTIPLEPANELSPNDIST,
              initial=0, max=40000, min=0, name=u'spnDist', parent=self,
              pos=wx.Point(91, 171), size=wx.Size(117, 21),
              style=wx.SP_ARROW_KEYS)
        self.spnDist.SetMinSize(wx.Size(-1, -1))
        self.spnDist.Bind(wx.EVT_TEXT, self.OnSpnDistText,
              id=wxID_VTXMLNODEADDRMULTIPLEPANELSPNDIST)

        self.lblDist = wx.StaticText(id=wxID_VTXMLNODEADDRMULTIPLEPANELLBLDIST,
              label=_(u'Distance'), name=u'lblDist', parent=self,
              pos=wx.Point(0, 171), size=wx.Size(83, 21), style=wx.ALIGN_RIGHT)
        self.lblDist.SetMinSize(wx.Size(-1, -1))

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,applName=_(u'vTool Address'),
                lWidgets=[self.txtStreet,self.txtZip,self.txtCity,
                        self.txtRegion,self.spnDist,self.chcCountry]
                )
        self.verbose=VERBOSE

        self.viMultiple.SetCB(self.__showMultiple__)
        
        self.lstCountries=[]
        for k in COUNTRIES.keys():
            self.lstCountries.append((COUNTRIES[k],k))
        def cmpFunc(a,b):
            return cmp(a[0],b[0])
        self.lstCountries.sort(cmpFunc)
        
        for it in self.lstCountries:
            self.chcCountry.Append(it[0])
        
        self.objRegNode=None
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
    def SetRegNode(self,obj):
        self.objRegNode=obj
    def OnChcCountryChoice(self, event):
        self.SetModified(True,self.chcCountry)
        event.Skip()
    def __clearMultiple__(self):
        if self.IsMainThread()==False:
            return
        if VERBOSE>0:
            self.__logDebug__(''%())
        self.txtStreet.SetValue('')
        self.txtZip.SetValue('')
        self.txtCity.SetValue('')
        self.txtRegion.SetValue('')
        try:
            docGlb=self.doc.GetNetDoc('vGlobals')
            sCountry=docGlb.GetGlobal(['vLoc','location'],'globalsLoc','country')
            self.chcCountry.SetStringSelection(COUNTRIES[sCountry])
        except:
            pass
        self.spnDist.SetValue(0)
    def __enableMultiple__(self,flag):
        if self.IsMainThread()==False:
            return
        if VERBOSE>0:
            self.__logDebug__(''%())
        self.txtStreet.Enable(flag)
        self.txtZip.Enable(flag)
        self.txtCity.Enable(flag)
        self.txtRegion.Enable(flag)
        self.chcCountry.Enable(flag)
        self.spnDist.Enable(flag)
    def __isModified__(self):
        if self.bModified:
            return True
        return False
    def __Clear__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        #self.bBlock=True
        #self.SetModified(False)
        #if self.doc is not None and self.node is not None:
        #    self.doc.endEdit(self.node)
        self.viMultiple.Clear()
        #wx.CallAfter(self.__clearBlock__)
    def IsBusy(self):
        return False
    def Stop(self):
        pass
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        self.viMultiple.SetDoc(doc)
    def __showMultiple__(self,node,*args,**kwargs):
        if self.IsMainThread()==False:
            return
        if VERBOSE>0:
            self.__logDebug__(''%())
        self.SetBlock()
        self.SetModified(False)
        if node is None:
            self.__clearMultiple__()
            self.__enableMultiple__(False)
        else:
            self.__enableMultiple__(True)
            self.txtStreet.SetValue(self.objRegNode.GetStreet(node))
            self.txtCity.SetValue(self.objRegNode.GetCity(node))
            self.txtZip.SetValue(self.objRegNode.GetZip(node))
            try:
                sCountry=self.objRegNode.GetCountry(node)
                self.chcCountry.SetStringSelection(COUNTRIES[sCountry])
            except:
                try:
                    docGlb=self.doc.GetNetDoc('vGlobals')
                    sCountry=docGlb.GetGlobal(['vLoc','location'],'globalsLoc','country')
                    self.chcCountry.SetStringSelection(COUNTRIES[sCountry])
                except:
                    pass
            self.txtRegion.SetValue(self.objRegNode.GetRegion(node))
            try:
                iDist=long(self.objRegNode.GetDistance(node))
            except:
                iDist=0
            self.spnDist.SetValue(iDist)
        self.ClrBlockDelayed()
    def __SetNode__(self,node):
        try:
            if VERBOSE>5:
                self.__logDebug__(''%())
            self.viMultiple.SetNode(node)
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            nodeMultiple=self.viMultiple.GetNode(node)
            if nodeMultiple is not None:
                self.objRegNode.SetStreet(nodeMultiple,self.txtStreet.GetValue())
                self.objRegNode.SetCity(nodeMultiple,self.txtCity.GetValue())
                self.objRegNode.SetZip(nodeMultiple,self.txtZip.GetValue())
                try:
                    i=self.chcCountry.GetSelection()
                    sCountry=self.lstCountries[i][1]
                except:
                    sCountry='ch'
                self.objRegNode.SetCountry(nodeMultiple,sCountry)
                self.objRegNode.SetRegion(nodeMultiple,self.txtRegion.GetValue())
                self.objRegNode.SetDistance(nodeMultiple,str(self.spnDist.GetValue()))
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
    def __Lock__(self,flag):
        if flag:
            self.viMultiple.Enable(False)
            self.__enableMultiple__(False)
        else:
            self.viMultiple.Enable(True)
            self.__enableMultiple__(True)

    def OnTxtNameText(self, event):
        if self.bBlock==False:
            self.SetModified(True,self.txtName)
        event.Skip()

    def OnTxtStreetText(self, event):
        if self.bBlock==False:
            self.SetModified(True,self.txtStreet)
        event.Skip()

    def OnTxtZipText(self, event):
        if self.bBlock==False:
            self.SetModified(True,self.txtZip)
        event.Skip()

    def OnTxtCityText(self, event):
        if self.bBlock==False:
            self.SetModified(True,self.txtCity)
        event.Skip()

    def OnTxtRegionText(self, event):
        if self.bBlock==False:
            self.SetModified(True,self.txtRegion)
        event.Skip()

    def OnSpnDistText(self, event):
        if self.bBlock==False:
            self.SetModified(True,self.spnDist)
        event.Skip()

    def OnViMultipleVtinputMultipledValuesDeleted(self, event):
        event.Skip()
        self.SetBlock()
        self.__clearMultiple__()
        self.ClrBlockDelayed()

    def OnViMultipleVtinputMultipledValuesAdded(self, event):
        event.Skip()
        self.SetBlock()
        self.__clearMultiple__()
        self.__enableMultiple__(True)
        self.ClrBlockDelayed()

    def OnViMultipleVtinputMultipledValuesSelected(self, event):
        event.Skip()

    def OnViMultipleVtinputMultipledValuesChanged(self, event):
        event.Skip()
            
