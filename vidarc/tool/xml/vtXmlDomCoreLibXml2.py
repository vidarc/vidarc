#----------------------------------------------------------------------------
# Name:         vtXmlDomCoreLibXml2.py
# Purpose:      very basic xml-dom object, that provide common interface for
#               libxml2-librariy
#
# Author:       Walter Obweger
#
# Created:      20080131
# CVS-ID:       $Id: vtXmlDomCoreLibXml2.py,v 1.21 2015/10/20 09:13:41 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import libxml2
import sys
def newSha():
    if sys.version_info >= (2,6,0):
        import hashlib
        return hashlib.sha1()
    else:
        import sha
        return sha.new()
import types,time
import os,os.path,stat,threading

import vidarc.tool.InOut.fnUtil as fnUtil
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.time.vtTimeLoadBound as vtLdBd
from vidarc.tool.InOut.vtInOutFileLock import vtInOutFileLock

semLibXml2=threading.Lock()#threading.Semaphore()#threading._allocate_lock()
def acquireLibXml2():
    #return
    global semLibXml2
    #vtLog.vtLngCur(vtLog.DEBUG,'','vtXmlDom:libXml2')
    semLibXml2.acquire()
def releaseLibXml2():
    #return
    global semLibXml2
    semLibXml2.release()
    #vtLog.vtLngCur(vtLog.DEBUG,'','vtXmlDom:libXml2')

class vtXmlDomParse(Exception):
    def __init__(self,origin,sFN):
        self.origin=origin
        self.sFN=sFN
    def __str__(self):
        return ''.join(['vtXmlDomParse origin:',repr(self.origin),'fn:',repr(self.sFN)])

class vtXmlDomCoreLibXml2:
    def __init__(self):
        self._ctx=None          # libxml2 parser context
        self.doc=None
        self.root=None
        self.ids=None
    def __del__(self):
        try:
            self.root=None
            if self.doc is not None:
                self.doc.freeDoc()
            #del self.doc
            self.doc=None
            #if self._ctx is not None:
            #    self._ctx.clearParserCtxt()
        except:
            pass
    def getLibClassName(self):
        return 'vtXmlDomCoreLibXml2'#self.__class__.__name__
    def __NewLib__(self,revision,root):
            acquireLibXml2()
            try:
                self.doc=libxml2.newDoc(revision)
                self.root=self.doc.newDocNode(None,root,'')
                self.doc.setRootElement(self.root)
            except:
                vtLog.vtLngTB(self.GetOrigin())
            releaseLibXml2()
    def __parseFile__(self,sFN,sz=1024):
        acquireLibXml2()
        try:
            #if self.doc is not None:
            #    del self.doc
            self.doc=None
            self.root=None
            vtLog.vtLngCur(vtLog.INFO,u'fn:%s;started'%(sFN),self.GetOrigin())
            iAct=0
            st=os.stat(sFN)
            iCount=st[stat.ST_SIZE]
            if iCount==0:
                raise IOError
            self.doc=libxml2.parseFile(self.sFN)
            releaseLibXml2()
            vtLog.vtLngCur(vtLog.INFO,u'fn:%s;finished'%(sFN),self.GetOrigin())
            return self.doc
            
            #print iAct
            f=open(sFN)
            #s=f.read(sz)
            #iSz=len(s)
            #bFin=iSz<sz
            #self._ctx=libxml2.createPushParser(None,s,iSz,sFN)
            bFin=False
            #if self._ctx:
            #    del self._ctx
            #    self._ctx.clearParserCtxt()
            
            self._ctx=libxml2.createPushParser(None,'',0,sFN)
            while bFin==False:
                #print '   ',iAct,iCount,iSz
                #print '   ',s
                s=f.read(sz)
                iSz=len(s)
                bFin=iSz<sz
                iAct+=iSz
                self.NotifyOpenProc(iAct,iCount)
                self._ctx.parseChunk(s,iSz,bFin)
            f.close()
            #self._ctx.parseChunk('',-1,1)
            #ctx.parseChunk('',0,1)
            self.doc=self._ctx.doc()
            vtLog.vtLngCur(vtLog.INFO,u'fn:%s;finished'%(sFN),self.GetOrigin())
        except:
            #try:
            #    self._ctx.clearParserCtxt()
            #except:
            #    pass
            releaseLibXml2()
            vtLog.vtLngTB(self.GetOrigin())
            self.doc=None
            raise vtXmlDomParse(self.GetOrigin(),sFN)
        releaseLibXml2()
        return self.doc
    def open(self,sFN):
        #acquireLibXml2()
        try:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'read;file:%s'%(sFN),self.GetOrigin())
            if self.USE_FILE_LOCK:
                lckFd=vtInOutFileLock(sFN)
                fd=lckFd.open()
                #vtLog.CallStack('')
                #print fd
                if fd is not None:
                    self.doc=libxml2.readFd(fd,'',None,#0)
                            libxml2.XML_PARSE_NOERROR|
                            libxml2.XML_PARSE_NOWARNING)
                else:
                    if vtLog.vtLngIsLogged(vtLog.INFO):
                        vtLog.vtLngCur(vtLog.INFO,'fn:%s;unable to lock file'%sFN,origin=self.GetOrigin())
                    vtLog.vtLngTB(self.GetOrigin())
                    self.doc=None
                    #releaseLibXml2()
                    #self.release('dom')
                    #self.oSF.SetState(self.oSF.OPEN_FLT)
                    #self.NotifyOpenFault(sFN,'')
                    return -2
                lckFd.close()
            else:
                #self.doc.doc=libxml2.parseFile(sFN)
                self.__parseFile__(sFN)
                #sFN
        except Exception,list:
            if self.USE_FILE_LOCK:
                lckFd.close()
            #self.doc.New()  ## 070530:wro FIXME:why deactivated?
            vtLog.vtLngTB(self.GetOrigin())
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'fn:%s;parsing fault'%sFN,origin=self.GetOrigin())
            #print 'exception %s on filename:%s'%(list,sFN)
            #releaseLibXml2()
            return -1
            #self.keepGoing = self.running = False
            #self.release('dom')
            return 0
        #releaseLibXml2()
        try:
            if self is None:
                self.root=None
            else:
                self.root=self.doc.getRootElement()
        except:
            vtLog.vtLngTB(self.GetOrigin())
        return 0
    def close(self):
        if self.doc is not None:
            self.doc.freeDoc()
            #del self.doc
        #if self._ctx is not None:
            #self._ctx.ctxtResetPush()
        #    self._ctx.ctxtReset()
        self.doc=None
        self.root=None
        if self.ids is not None:
            del self.ids
        self.ids=None
    def save(self,sTmpFN,encoding='ISO-8859-1'):
        """ make sure 'dom'- is acquired and sTmpFN is valid.
        """
        if self.doc is None:
            return 0
        try:
            if self.USE_FILE_LOCK:
                f=open(sTmpFN,'w+')#'a+rw')
                lckFile=vtInOutFileLock(sTmpFN)
                fd=lckFile.lock(f)
                if fd is None:
                    vtLog.vtLngCur(vtLog.ERROR,'fn:%s;unable to lock file'%(sTmpFN),self.GetOrigin())
                    f.close()
                    return -5
                fnUtil.shiftFile(sTmpFN,fd=fd)
                f.seek(0)
                f.truncate()
                self.doc.saveTo(f,encoding)
                lckFile.unlock()
                f.close()
            else:
                fnUtil.shiftFile(sTmpFN)
                self.doc.saveFileEnc(sTmpFN,encoding)
            #self.audit.backup(sTmpFN)
            try:
                if self.LOG_SAVE:
                    if vtLog.vtLngIsLogged(vtLog.INFO):
                        m=os.stat(sTmpFN)
                        inf=open(sTmpFN)
                        try:
                            #s=sha.new(inf.read()).hexdigest()
                            s=newSha()
                            s.update(inf.read())
                            s=s.hexdigest()
                        except:
                            s=''
                        inf.close()
                        vtLog.vtLngCur(vtLog.INFO,'fn:%s;size:%d;sha:%s'%(sTmpFN,m[stat.ST_SIZE],s),self.GetOrigin())
            except:
                vtLog.vtLngTB(self.GetOrigin())
            return 1
        except:
            vtLog.vtLngTB(self.GetOrigin())
            try:
                lckFile.unlock()
            except:
                pass
            try:
                f.close()
            except:
                pass
        return -1
    def serializeDocEnc(self,encoding):
        return self.doc.serialize(encoding)
    def getXmlContent(self,node=None):
        if node is None:
            #content=self.doc.c14nMemory()
            content=self.doc.serialize(self.SERIALIZE_CODEC) # 070309:wro ('ISO-8859-1')
            return content
            #tmp=content.decode('ISO-8859-1') #060521
            #return content
            ## 070309:wro return tmp.encode('ISO-8859-1')
        else:
            acquireLibXml2()
            try:
                tmpDoc=libxml2.newDoc('1.0')
                tn=self.cloneNode(node,True)
                tmpDoc.setRootElement(tn)
                #content=tmpDoc.c14nMemory()
                content=tmpDoc.serialize(self.SERIALIZE_CODEC) # 070309:wro ('ISO-8859-1')
                tmpDoc.freeDoc()
            except:
                vtLog.vtLngTB(self.GetOrigin())
            releaseLibXml2()
            ## 070309:wro tmp=content.decode('ISO-8859-1')
            #tn.unlinkNode()
            #del tmpDoc
            return content # 070309:wro  act
            ## 070309:wro return tmp.encode('ISO-8859-1')
    def __buildAttrXmlDoc__(self,tn,node):
        for c in self.getChildsNoAttr(node,self.attr):
            if self.hasChilds(c):
                tc=self.cloneNode(c,False)
                tn.addChild(tc)
                self.__buildAttrXmlDoc__(tc,c)
            else:
                tn.addChild(self.cloneNode(c,True))
    def __buildXmlDoc__(self,tn,node,bIncludeNodes):
        for c in self.getChilds(node):
            if c.name in self.skip:
                continue
            #if c.name[:2]=='__':
            #    continue
            a=c.hasProp(self.attr)
            if a is None:
                #tn.addChild(self.cloneNode(c,True))
                if self.hasChilds(c):
                    tc=self.cloneNode(c,False)
                    tn.addChild(tc)
                    self.__buildAttrXmlDoc__(tc,c)
                else:
                    tn.addChild(self.cloneNode(c,True))
                continue
            if bIncludeNodes:
                tc=self.cloneNode(c,False)
                tn.addChild(tc)
                self.__buildXmlDoc__(tc,c,bIncludeNodes)
    def getNodeXmlContent(self,node=None,bFull=False,bIncludeNodes=False):
        if node is None:
            node=self.getRoot()
        acquireLibXml2()
        try:
            tmpDoc=libxml2.newDoc('1.0')
            if bFull:
                tn=self.cloneNode(node,True)
            else:
                tn=self.cloneNode(node,False)
                self.__buildXmlDoc__(tn,node,bIncludeNodes)
            tmpDoc.setRootElement(tn)
            #content=tmpDoc.c14nMemory()
            #content=tmpDoc.serialize('ISO-8859-1')
            content=tmpDoc.serialize(self.SERIALIZE_CODEC)  # 070309:wro  ('ISO-8859-1')
            tmpDoc.freeDoc()
        except:
            vtLog.vtLngTB(self.GetOrigin())
        releaseLibXml2()
        #tmp=content.decode('ISO-8859-1')
        #del tmpDoc
        return content
        #return tmp.encode('ISO-8859-1')
    def __getFingerPrintStrAttr__(self,node):
        lst=[]
        if node is not None:
            tmpAttr=node.get_properties()
            while tmpAttr is not None:
                if self.__isAttrSkip4FingerPrint__(tmpAttr.name)==False:
                    lst.append((tmpAttr.name,tmpAttr.content))
                tmpAttr=tmpAttr.next
        return self.__getFingerPrintStrFromLst__(lst)
    def setNodeXmlContent(self,node,s):
        
        acquireLibXml2()
        try:
            try:
                tmpDoc=libxml2.parseMemory(s,len(s))
            except:
                vtLog.vtLngCS(vtLog.CRITICAL,'please report to office@vidarc.com;unicode problem;perform decode %s'%s,origin=self.GetOrigin())
                s=s.decode('ISO-8859-1')
                tmpDoc=libxml2.parseMemory(s,len(s))
        except:
            vtLog.vtLngTB(self.GetOrigin())
        releaseLibXml2()
        
        l=[]
        tmpAttr=node.get_properties()
        while tmpAttr is not None:
            l.append(tmpAttr.name)
            tmpAttr=tmpAttr.next
        for sAttr in l:
            node.unsetProp(sAttr)
        for c in self.getChildsNoAttr(node,self.attr):
            self.deleteNode(c)
        tmp=tmpDoc.getRootElement()
        #tmp.unlinkNode()
        tmpAttr=tmp.get_properties()
        while tmpAttr is not None:
            a1=node.newProp(tmpAttr.name,tmpAttr.content)
            tmpAttr=tmpAttr.next
        for c in self.getChildsNoAttr(tmp,self.attr):
            c.unlinkNode()
            node.addChild(c)
        tmpDoc.setRootElement(None)
        tmpDoc.freeDoc()
        #del tmpDoc
        return node
    def setNodeXmlContentFull(self,nodePar,node,s):
        acquireLibXml2()
        try:
            try:
                tmpDoc=libxml2.parseMemory(s,len(s))
            except:
                vtLog.vtLngCS(vtLog.CRITICAL,'please report to office@vidarc.com;unicode problem;perform decode %s'%s,origin=self.GetOrigin())
                s=s.decode('ISO-8859-1')
                tmpDoc=libxml2.parseMemory(s,len(s))
        except:
            releaseLibXml2()
            vtLog.vtLngTB(self.GetOrigin())
            return node
        releaseLibXml2()
        #for c in self.getChilds(node):
        #    if c.name in self.skip:
        #        continue
        #    if c.name[:2]=='__':
        #        continue
        #    a=c.hasProp(self.attr)
        #    if a is None:
        #        print '   delete node'
        #        self.deleteNode(c)
        tmp=tmpDoc.getRootElement()
        tmp.unlinkNode()
        #for c in self.getChilds(tmp):
        #    node.addChild(c)
        if nodePar is not None:
            self.deleteNode(node)
            nodePar.addChild(tmp)
            self.clearFingerPrintFull(node)
            self.ids.CheckIdDeep(tmp,self)
            # FIXME: process missing???
        tmpDoc.setRootElement(None)
        tmpDoc.freeDoc()
        #del tmpDoc
        return tmp
    def getNode2Add(self,s,bWarn=True):
        acquireLibXml2()
        tmpDoc=None
        tmp=None
        try:
            tmpDoc=libxml2.parseMemory(s,len(s))
            tmp=tmpDoc.getRootElement()
            tmp.unlinkNode()
            tmpDoc.setRootElement(None)
        except:
            try:
                vtLog.vtLngCS(vtLog.CRITICAL,'please report to office@vidarc.com;unicode problem;perform decode %s'%s,origin=self.GetOrigin())
                s=s.decode('ISO-8859-1')
                tmpDoc=libxml2.parseMemory(s,len(s))
                tmp=tmpDoc.getRootElement()
                tmp.unlinkNode()
                tmpDoc.setRootElement(None)
            except:
                vtLog.vtLngTB(self.GetOrigin())
        releaseLibXml2()
        if bWarn:
            vtLog.vtLngCur(vtLog.WARN,'FIXME,freeDoc alter using __freeDocRaw__',self.GetOrigin())
        #tmpDoc.freeDoc()
        return tmpDoc,tmp
    def __freeDocRaw__(self,tmpDoc):
        acquireLibXml2()
        try:
            tmpDoc.freeDoc()
        except:
            vtLog.vtLngTB(self.GetOrigin())
        releaseLibXml2()
    def __freeNodeRaw__(self,tmpNode):
        acquireLibXml2()
        try:
            tmpNode.freeNode()
        except:
            vtLog.vtLngTB(self.GetOrigin())
        releaseLibXml2()
    def __addNodeXmlContentExternalLib__(self,node,nodeBase,expAttr,lValidTagNames,lNewID,lOldID):
            idOld=self.getAttribute(node,self.attr)
            self.setAttribute(node,self.attr,'')
            self.setAttribute(node,expAttr,idOld)
            if 1:
                nodeBase.addChild(node)
                self.clearFingerPrintFull(node)
                #self.calcFingerPrintFull(node)
                self.ids.CheckIdDeep(node,self)
                self.ids.ProcessMissing(self)
                self.ids.ClearMissing()
            else:
                self.addnodeBase(nodeBase,node)
            idNew=self.getAttribute(node,self.attr)
            lNewID.append(idNew)
            lOldID.append(idOld)
    def addNodeXmlContentExternal(self,node,s,expAttr='eid',lValidTagNames=None):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'%s'%(s),self.GetOrigin())
        acquireLibXml2()
        try:
            try:
                tmpDoc=libxml2.parseMemory(s,len(s))
            except:
                vtLog.vtLngCS(vtLog.CRITICAL,'please report to office@vidarc.com;unicode problem;perform decode %s'%s,origin=self.GetOrigin())
                s=s.decode('ISO-8859-1')
                tmpDoc=libxml2.parseMemory(s,len(s))
        except:
            releaseLibXml2()
            vtLog.vtLngTB(self.GetOrigin())
            return None,None
        releaseLibXml2()
        tmp=tmpDoc.getRootElement()
        tmp.unlinkNode()
        lNewID=[]
        lOldID=[]
        if lValidTagNames is None:
            self.__addNodeXmlContentExternalLib__(tmp,node,expAttr,lValidTagNames,lNewID,lOldID)
            if 0:
                idOld=self.getAttribute(tmp,self.attr)
                self.setAttribute(tmp,self.attr,'')
                self.setAttribute(tmp,expAttr,idOld)
                if 1:
                    node.addChild(tmp)
                    self.clearFingerPrintFull(tmp)
                    #self.calcFingerPrintFull(tmp)
                    self.ids.CheckIdDeep(tmp,self)
                    self.ids.ProcessMissing(self)
                    self.ids.ClearMissing()
                else:
                    self.addNode(node,tmp)
                idNew=self.getAttribute(tmp,self.attr)
        else:
            self.procKeysRec(100,tmp,self.__addNodeXmlContentExternalLib__,
                        node,expAttr,lValidTagNames,lNewID,lOldID)
        idNew=lNewID
        idOld=lOldID
        tmpDoc.setRootElement(None)
        tmpDoc.freeDoc()
        #del tmpDoc
        return idNew,idOld
    def addNodeXmlContent(self,node,s):
        acquireLibXml2()
        try:
            try:
                tmpDoc=libxml2.parseMemory(s,len(s))
            except:
                vtLog.vtLngCS(vtLog.CRITICAL,'please report to office@vidarc.com;unicode problem;perform decode %s'%s,origin=self.GetOrigin())
                s=s.decode('ISO-8859-1')
                tmpDoc=libxml2.parseMemory(s,len(s))
        except:
            releaseLibXml2()
            vtLog.vtLngTB(self.GetOrigin())
            return None
        releaseLibXml2()
        tmp=tmpDoc.getRootElement()
        tmp.unlinkNode()
        node.addChild(tmp)
        self.clearFingerPrintFull(tmp)
        #self.calcFingerPrintFull(tmp)
        self.ids.CheckIdDeep(tmp,self)
        self.ids.ProcessMissing(self)
        self.ids.ClearMissing()
        tmpDoc.setRootElement(None)
        idNew=self.getAttribute(tmp,self.attr)
        tmpDoc.freeDoc()
        #del tmpDoc
        return idNew,tmp
    def setNodeXmlConfig(self,node,s):
        try:
            acquireLibXml2()
            try:
                tmpDoc=libxml2.parseMemory(s,len(s))
            except:
                vtLog.vtLngCS(vtLog.CRITICAL,'please report to office@vidarc.com;unicode problem;perform decode %s'%s,origin=self.GetOrigin())
                s=s.decode('ISO-8859-1')
                tmpDoc=libxml2.parseMemory(s,len(s))
        except:
            releaseLibXml2()
            vtLog.vtLngTB(self.GetOrigin())
            return None
        try:
            releaseLibXml2()
            tmp=tmpDoc.getRootElement()
            tmp.unlinkNode()
            for c in self.getChilds(node,'config'):
                self.deleteNode(c)
            node.addChild(tmp)
            tmpDoc.setRootElement(None)
            #tmpDoc.freeDoc()
            #del tmpDoc
            return tmp
        except:
            return None
    def setNodeXmlSpecial(self,node,sTagName,s):
        try:
            if len(s)==0:
                return None
            acquireLibXml2()
            try:
                tmpDoc=libxml2.parseMemory(s,len(s))
            except:
                vtLog.vtLngCS(vtLog.CRITICAL,'please report to office@vidarc.com;unicode problem;perform decode %s'%s,origin=self.GetOrigin())
                s=s.decode('ISO-8859-1')
                tmpDoc=libxml2.parseMemory(s,len(s))
        except:
            releaseLibXml2()
            vtLog.vtLngTB(self.GetOrigin())
            return None
        try:
            releaseLibXml2()
            tmp=tmpDoc.getRootElement()
            tmp.unlinkNode()
            for c in self.getChilds(node,sTagName):
                self.deleteNode(c)
            node.addChild(tmp)
            tmpDoc.setRootElement(None)
            #tmpDoc.freeDoc()
            #del tmpDoc
            return tmp
        except:
            return None
    def copyNode(self,node,bFull=False,bIncludeNodes=False):
        s=self.getNodeXmlContent(node,bFull=bFull,bIncludeNodes=bIncludeNodes)
        try:
            tmpDoc=libxml2.parseMemory(s,len(s))
        except:
            vtLog.vtLngCS(vtLog.CRITICAL,'please report to office@vidarc.com;unicode problem;perform decode %s'%s,origin=self.GetOrigin())
            s=s.decode('ISO-8859-1')
            tmpDoc=libxml2.parseMemory(s,len(s))
        #releaseLibXml2()
        tmp=tmpDoc.getRootElement()
        tmp.unlinkNode()
        tmpDoc.setRootElement(None)
        #tmpDoc.freeDoc()  #061212
        #self.validateNode(tmp)
        return tmp
    def __parseBuffer__(self,s):
        acquireLibXml2()
        try:
            try:
                tmpDoc=libxml2.parseMemory(s,len(s))
            except:
                vtLog.vtLngCS(vtLog.CRITICAL,'please report to office@vidarc.com;unicode problem;perform decode %s'%s,origin=self.GetOrigin())
                s=s.decode('ISO-8859-1')
                tmpDoc=libxml2.parseMemory(s,len(s))
            root=tmpDoc.getRootElement()
        except:
            vtLog.vtLngTB(self.GetOrigin())
            tmpDoc,root=None,None
        releaseLibXml2()
        return tmpDoc,root
    def parseBuffer(self,s,attr='id'):
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'synch',origin=self.GetOrigin())
        nodes=[]
        if self.audit.isEnabled():
            self.audit.write('ParseBuffer',s)
        
        if 1==0:#self.bSynch:
            tmp=libxml2.parseMemory(s,len(s))
            while self.thdOpen.IsRunning():
                #print 'wait to open finished'
                time.sleep(1)
            while self.thdSynch.IsRunning():
                time.sleep(1)
            if self.doc is None:
                fn=self.sFN
                self.New()
                self.sFN=fn
            self.thdSynch.Synch(tmp,nodes)
        else:
            try:
                for tagName in self.nodes2keep:
                    n=self.getChild(self.getRoot(),tagName)
                    if n is not None:
                        nodes.append(self.cloneNode(n,True))
                fn=self.sFN
                self.Close()
                self.attr=attr
                self.sFN=fn
                self.doc,self.root=self.__parseBuffer__(s)
                self.iElemCount=-1
                self.iElemIDCount=-1
                #self.GetElementCount()
                #self.GetElementAttrCount()
                self.genIds()
            
                par=self.getRoot()
                for tagName in self.nodes2keep:
                    n=self.getChild(self.getRoot(),tagName)
                    if n is not None:
                        self.deleteNode(n)
                    for n in nodes:
                        self.appendChild(par,n)
            except:
                vtLog.vtLngTB(self.GetOrigin())
    def __diffChilds__(self,childs,tmpChilds):
        def compFunc(a,b):
            return cmp(a.name,b.name)
        childs.sort(compFunc)
        tmpChilds.sort(compFunc)
        iLen=len(childs)
        if iLen!=len(tmpChilds):
            #print 'len diff:',iLen,len(tmpChilds)
            return True
        for i in range(iLen):
            c=childs[i]
            t=tmpChilds[i]
            if c.name!=t.name:
                #print 'tagname:',c.name
                #print '       :',t.name
                return True
            #if len(self.getKey(c))>0:
            #    continue
            if self.getText(c).strip()!=self.getText(t).strip():
                return True
            dcAttr={}
            dtAttr={}
            cAttr=c.get_properties()
            while cAttr is not None:
                dcAttr[cAttr.name]=cAttr.content
                cAttr=cAttr.next
            cAttr=t.get_properties()
            while cAttr is not None:
                dtAttr[cAttr.name]=cAttr.content
                cAttr=cAttr.next
            keys=dcAttr.keys()
            iAttrLen=len(keys)
            if iAttrLen!=len(dtAttr.keys()):
                #print 'attr len diff:%d %d',iAttrLen,len(dtAttr.keys())
                return True
            for k in keys:
                try:
                    if dcAttr[k]!=dtAttr[k]:
                        #print 'attrname:',k,dcAttr[k]
                        #print '        :',k,dtAttr[k]
                        return True
                except:
                    return True
        for i in range(iLen):
            c=childs[i]
            t=tmpChilds[i]
            #if len(self.getKey(c))>0:
            #    continue
            if self.__diffChilds__(self.getChildsNoAttr(c,self.attr),
                                self.getChildsNoAttr(t,self.attr)):
                #print '    childs differ'
                return True
        return False
    def __synchEdit__(self,node,tmpNode,bCheck=False):
        #vtLog.CallStack('')
        if bCheck:
            if self.ids.IsOfflineEdit(self.getKeyNum(node)):
            #off=self.getAttribute(node,'offline_content')
            #if len(off)>0:
                #self.doc.removeAttribute(c,'offline_content')
                return 2
            childs=self.getChildsNoAttr(node,self.attr)
            tmpChilds=self.getChildsNoAttr(tmpNode,self.attr)
            if self.__diffChilds__(childs,tmpChilds)==False:
                return 0
        else:
            childs=self.getChildsNoAttr(node,self.attr)
            tmpChilds=self.getChildsNoAttr(tmpNode,self.attr)
        #if self.verbose:
        #    vtLog.vtLngCur(vtLog.DEBUG,'local:%s'%node)
        #    vtLog.vtLngCur(vtLog.DEBUG,'remote:%s'%tmpNode,callstack=False)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'local:%s'%node,self.GetOrigin())
        #    vtLog.vtLngCur(vtLog.DEBUG,'remote:%s'%tmpNode,self.GetOrigin())
        self.setTagName(node,self.getTagName(tmpNode))
        iEdited=0
        for c in childs:
            if len(self.getKey(c))>0:
                continue
            self.deleteNode(c)
        
        lst=[]
        cAttr=node.get_properties()
        while cAttr is not None:
            lst.append(cAttr.name)
            cAttr=cAttr.next
        for attr in lst:
            node.unsetProp(attr)
            
        for c in tmpChilds:
            if len(self.getKey(c))>0:
                continue
            #tc=self.cloneNode(c,True)
            #node.addChild(tc)
            c.unlinkNode()
            node.addChild(c)
            self.AlignNode(c,iRec=2)
            iEdited=1
        cAttr=tmpNode.get_properties()
        while cAttr is not None:
            if cAttr.name in ['fp']:
                cAttr=cAttr.next
                continue
            self.setAttribute(node,cAttr.name,cAttr.content)
            cAttr=cAttr.next
        self.__setModified__()
        if self.audit.isEnabled():
            self.audit.write('__synchEdit__',self.getNodeXmlContent(node,False,False),
                            origin=self.getAuditOriginByNode(node))
        #self.AlignNode(node,iRec=2)
        return iEdited
    def __showNode__(self,node,s):
        if node.type=='element':
            print s,"ELEMENT",node.name
        #elif node.type=='text':
        #    print s,"TEXT_NODE",node.content
        #elif node.type=='cdata':
        #    print s,"CDATA_SECTION_NODE"
        #elif node.nodeType==Element.ATTRIBUTE_NODE:
        #    print s,"ATTRIBUTE_NODE"
        #elif node.type=='comment':
        #    print s,"COMMENT_NODE"
        #elif node.nodeType==Element.DOCUMENT_NODE:
        #    print s,"DOCUMENT_NODE"
        #elif node.nodeType==Element.ENTITY_NODE:
        #    print s,"ENTITY_NODE"
        #elif node.nodeType==Element.NOTATION_NODE:
        #    print s,"NOTATION_NODE"
    def __genIds__(self,c,node,ids):
        #time.sleep(0.1)
        if c.name in self.skip:
            return 0
        if self.hasAttribute(c,self.attr):      # 061019: wro better solution
            ids.CheckId(c,self)
        self.iElemIDCount+=1
        #id=self.getAttribute(c,self.attr)
        #if len(id)>0:
        #    ids.CheckId(c,id)
        #self.__genIds__(c,ids)
        self.procChildsKeys(c,self.__genIds__,c,ids)
        return 0
    def __getNodeLevel__(self,node):
        try:
            pn=node.parent
            if pn is not None:
                return self.__getNodeLevel__(pn)+1
            else:
                return -1
        except:
            return -1
    def getLevel(self,node):
        nodePar=self.getParent(node)
        if nodePar is not None:
            return self.getLevel(nodePar)+1
        return 1
    def getTagName(self,node):
        if node is None:
            return ''
        return node.name
    def getIdent(self,node):
        if node is None:        # 070201:wro
            return ''
        s=node.name
        tmpAttr=node.get_properties()
        while tmpAttr is not None:
            s+='|'+tmpAttr.name+':'+tmpAttr.content
        return s
    def setTagName(self,node,name):
        #if node is None:        # 070201:wro
        #    return 
        node.setName(name)
    def isElement(self,node):
        if node.type=='element':
            return 1
        else:
            return 0
    def isText(self,node):
        if node.type=='text':
            return 1
        else:
            return 0
    def isComment(self,node):
        if node.type=='comment':
            return 1
        else:
            return 0
    def isCData(self,node):
        if node.type=='cdata':
            return 1
        else:
            return 0
    def hasChilds(self,node):
        if node is None:
            return 0
        tmp=node.children
        while tmp is not None:
            if tmp.type=='element':
                return 1
            tmp=tmp.next
        return 0
    def hasNodes(self,node):
        if node is None:
            return 0
        tmp=node.children
        while tmp is not None:
            if tmp.type=='element':
                if self.hasKey(tmp):
                    return 1
            tmp=tmp.next
        return 0
    def validateNode(self,node):
        pass
    def appendChild(self,node,add):
        #vtLog.CallStack('')
        #print 'appendChild',node.type,node.name
        node.addChild(add)
        self.__setModified__()
        return
        tmp=node.get_last()
        if tmp is None:
            node.addChild(add)
        else:
            #print '    ',tmp.type,tmp.name,tmp.children
            tmp.addChild(add)
    def Align(self,node,iLevel,s,ident,verbose=0,gProcess=None,iAct=0,iSkip=0,iRec=1000):
        if verbose>0:
            vtLog.vtLogCallDepth(None,self.getTagName(node),1,False)
            print 'level:%03d ident:%03d act:%03d skip:%03d rec:%03d'%(iLevel,len(ident),iAct,iSkip,iRec)
            #self.__showNode__(node,s)
        prevNodeType=-1
        sNew=s+ident
        iOldAct=iAct
        
        if node.children is not None:
            o=node.children
            if o.next is None:
                prevNodeType=0
            if o.type!='text':
                # add text node 
                if verbose>1:
                    vtLog.vtLogCallDepth(None,sNew[1:]+' add at begin',1,False)
                nt=self.doc.newDocText(sNew)
                self.__setModified__()
                o.addPrevSibling(nt)
                if gProcess is not None:
                    gProcess.SetRange(gProcess.GetRange()+1)
            while o is not None:
                on=o.next
                if gProcess is not None:
                    gProcess.SetValue(gProcess.GetValue()+1)
                if verbose>0:
                    vtLog.vtLogCallDepth(None,self.getTagName(o)+' '+sNew,1,False)
                    #self.__showNode__(o,sNew)
                if (prevNodeType==-1):
                    if o.type == 'text':
                        if verbose>1:
                            vtLog.vtLogCallDepth(None,'',1,False)
                            print sNew[1:],'first text'
                        if len(o.content.strip())==0:
                            if verbose>1:
                                vtLog.vtLogCallDepth(None,'',1,False)
                                print sNew[1:],'  set'
                            o.setContent(sNew)
                            self.__setModified__()
                if o.type == 'element':
                    if on is not None and on.type!='text':
                        if verbose>1:
                            vtLog.vtLogCallDepth(None,'',1,False)
                            print sNew[1:],'append',o,on.type,on.content
                        nt=self.doc.newDocText(sNew)
                        self.__setModified__()
                        o.addNextSibling(nt)
                        #o.addPrevSibling(nt)
                        if gProcess is not None:
                            gProcess.SetRange(gProcess.GetRange()+1)
                    if iRec>0 or self.hasKey(o)==False:
                        childs=self.getChilds(o)
                        iLen=len(childs)
                        if iLen>0:
                            if verbose>1:
                                vtLog.vtLogCallDepth(None,'',1,False)
                                print o.name,'len',iLen
                        #if o.children is not None:
                            self.Align(o,iLevel+1,sNew,ident,verbose,gProcess,iAct,iRec=iRec-1)
                elif o.type == 'cdata':
                    if on is not None and on.type!='text':
                        nt=self.doc.newDocText(sNew)
                        self.__setModified__()
                        o.addNextSibling(nt)
                        if gProcess is not None:
                            gProcess.SetRange(gProcess.GetRange()+1)
                elif o.type == 'comment':
                    if on is not None and on.nodeType!='text':
                        nt=self.doc.newDocText(sNew)
                        self.__setModified__()
                        o.addNextSibling(nt)
                        if gProcess is not None:
                            gProcess.SetRange(gProcess.GetRange()+1)
                elif o.type == 'text':
                    if prevNodeType=='text':
                        prev.textMerge(o)
                        o=prev
                    if len(o.content.strip())==0:
                        if verbose>1:
                            vtLog.vtLogCallDepth(None,'',1,False)
                            print sNew[1:],'set text'
                            print o
                            print on
                        if on is not None:
                        #if iLen==1:
                        #    o.setContent(u'')
                        #else:
                            o.setContent(sNew)
                            self.__setModified__()
                        if verbose>1:
                            vtLog.vtLogCallDepth(None,'',1,False)
                            print sNew[1:],'set text after'
                            print o
                        
                    pass
                prevNodeType=o.type
                prev=o
                o=o.next
            if prevNodeType!='text':
                # add text node 
                if verbose>1:
                    vtLog.vtLogCallDepth(None,'',1,False)
                    print sNew[1:],'add at end'
                nt=self.doc.newDocText(s)
                self.__setModified__()
                prev.addNextSibling(nt)
                if gProcess is not None:
                    gProcess.SetRange(gProcess.GetRange()+1)
            elif prevNodeType=='text':
                if prev is not None:
                    if len(prev.content.strip())==0:
                        if verbose>1:
                            vtLog.vtLogCallDepth(None,'',1,False)
                            print s[1:],'set text at end'
                            print prev.parent
                        prev.setContent(s)
                        self.__setModified__()
    def AlignNode(self,node,gProcess=None,ident='  ',iRec=0):
        #doc=dom.documentElement
        if node is None:
            vtLog.vtLngCur(vtLog.WARN,'node is None'%(),self.GetOrigin())
            return
        iLevel=self.__getNodeLevel__(node)
        s='\n'
        for i in range(0,iLevel):
            s=s+ident
        parnode=node.parent
        if parnode is None:
            return
        if (parnode.children is not None) and (iLevel>0):
            i=len(ident)
            lc=node.prev
            if lc is None:
                nt=self.doc.newDocText(s)
                #print '>>',nt
                self.__setModified__()
                node.addPrevSibling(nt)
            else:
                if lc.type=='text':
                    if len(lc.getContent())!=len(s):
                        if len(lc.getContent().strip())==0:
                            lc.setContent(s)
                            self.__setModified__()
        else:
            nt=self.doc.newDocText(s)
            self.__setModified__()
            self.appendChild(node,nt)
            i=0
        self.Align(node,iLevel,s,ident,verbose=0,gProcess=gProcess,iRec=iRec)
        bDoAdd=True
        if node.type=='text':
            bDoAdd=False
        lc=node.next
        #print '0',i,len(ident)
        #print lc
        if lc is not None:
            if lc.next is not None:
                if lc.type=='text':
                    if len(lc.content.strip())<=0:
                        if lc.next.type=='element':
                            iLevelNew=self.__getNodeLevel__(lc.next)
                            if iLevelNew==iLevel:
                                sTmp=s
                            else:
                                sTmp=s[:-i]
                            if len(lc.getContent())!=len(sTmp):
                                lc.setContent(sTmp)
                                self.__setModified__()
                            bDoAdd=False
            else:
                bDoAdd=False
            pass
        if bDoAdd and (iLevel>0):
            nt=self.doc.newDocText(s[:-i])
            #nt=self.doc.newDocText(s[:-len(ident)])   #060418
            #nt=self.doc.newDocText(s)
            self.__setModified__()
            node.addNextSibling(nt)
        
    def AlignDoc(self,verbose=0,gProcess=None):
        if self.root is None:
            return
        self.Align(self.root,0,'\n','  ',verbose,gProcess)
    def GetNodeCount(self,node=None):
        i=0
        if node is None:
            node=self.root
        if node is None:
            return i
        tmp=node.children
        while tmp is not None:
            i+=1
            i+=self.GetNodeCount(tmp)
            tmp=tmp.next
        return i
    def __getElementCount__(self,node):
        if node is None:
            return 0
        i=0
        tmp=node.children
        while tmp is not None:
            vtLdBd.check()
            if tmp.type=='element':
                i+=1
            i+=self.__getElementCount__(tmp)
            tmp=tmp.next
        return i
    def __getElementAttrCount__(self,node,attr):
        i=0
        if node is None:
            return 0
        #vtLog.CallStack('')
        tmp=node.children
        while tmp is not None:
            vtLdBd.check()
            if tmp.type=='element':
                try:
                    if tmp.hasProp(attr) is not None:
                        i+=1
                except:
                    pass
            i+=self.__getElementAttrCount__(tmp,attr)
            tmp=tmp.next
        return i
    #def getText(self,node):
    #    if node is None:
    #        return None
    #    if node.children.type=='text':
    #        return unicode(node.children.content,self.applEncoding)
    #    return ''
    def getChildAttr(self,node,tagName,attr,attrVal):
        if node is None:
            node=self.getBaseNode()#self.root
        if node is None:
            return None
        tmp=node.children
        while tmp is not None:
            if tmp.type=='element':
                if tmp.name==tagName:
                    tmpAttr=tmp.get_properties()
                    while tmpAttr is not None:
                        if tmpAttr.name==attr:
                            if tmpAttr.content==attrVal:
                                return tmp
                        tmpAttr=tmpAttr.next
            tmp=tmp.next
        return None
    def getChildLang(self,node,tagName,lang):
        if node is None:
            node=self.getBaseNode()#self.root
        if node is None:
            return None
        if lang is None:
            lang=self.lang
            if lang is None:
                vtLog.vtLngCur(vtLog.WARN,'no langaguage specified;no language defined',self.GetOrigin())
                return None
        oFallback=None
        oFirst=None
        tmp=node.children
        while tmp is not None:
            if tmp.type=='element':
                if tmp.name==tagName:
                    tmpAttr=tmp.get_properties()
                    while tmpAttr is not None:
                        if tmpAttr.name=='language':
                            if tmpAttr.content==lang:
                                return tmp
                        tmpAttr=tmpAttr.next
                    if oFallback is None:
                        oFallback=tmp
                    if oFirst is None:
                        oFirst=tmp
            tmp=tmp.next
        if oFallback is not None:
            return oFallback
        return oFirst
    def getChild(self,node,tagName):
        if node is None:
            node=self.getBaseNode()#self.root
        if node is None:
            return None
        tmp=node.children
        while tmp is not None:
            if tmp.type=='element':
                if tagName is not None:
                    if tmp.name==tagName:
                        return tmp
                else:
                    return tmp
            tmp=tmp.next
        return None
    def getChildForced(self,node,tagName):
        if node is None:
            node=self.getBaseNode()#self.root
        if node is None:
            return None
        n=self.getChild(node,tagName)
        if n is None:
            n=self.createSubNode(node,tagName,True)#False)
            self.__setModified__()
        return n
    def __getChilds__(self,node,tagName=None):
        lst=[]
        if node is None:
            return lst
        tmp=node.children
        while tmp is not None:
            if tmp.type=='element':
                bFound=False
                if tagName is not None:
                    if tmp.name==tagName:
                        bFound=True
                else:
                    bFound=True
                if bFound:
                    lst.append(tmp)
            tmp=tmp.next
        return lst
    def getChilds(self,node=None,tagName=None):
        if node is None:
            node=self.getBaseNode()#self.root
        return self.__getChilds__(node,tagName)
    def getAttrs(self,node,bSkip=True):
        l=[]
        if node is not None:
            tmpAttr=node.get_properties()
            while tmpAttr is not None:
                if bSkip==False or self.__isAttrSkip4FingerPrint__(tmpAttr.name)==False:
                    l.append(tmpAttr.name)
                tmpAttr=tmpAttr.next
            l.sort()
        return l
    def getAttrsValues(self,node,bSkip=True):
        l=[]
        if node is not None:
            tmpAttr=node.get_properties()
            while tmpAttr is not None:
                if bSkip==False or self.__isAttrSkip4FingerPrint__(tmpAttr.name)==False:
                    l.append((tmpAttr.name,tmpAttr.content))
                tmpAttr=tmpAttr.next
            l.sort()
        return l
    def __getChildsAttrVal__(self,node,tagName=None,attr=None,attrVal=None):
        lst=[]
        if node is None:
            return lst
        tmp=node.children
        while tmp is not None:
            if tmp.type=='element':
                bFound=False
                if tagName is not None:
                    if tmp.name==tagName:
                        bFound=True
                else:
                    bFound=True
                if bFound:
                    # check attr
                    if attr is not None:
                        bFound=False
                        tmpAttr=tmp.get_properties()
                        while tmpAttr is not None:
                            if tmpAttr.name==attr:
                                if attrVal is not None:
                                    if attrVal==tmpAttr.content:
                                        bFound=True
                                else:
                                    bFound=True
                                break
                            tmpAttr=tmpAttr.next
                    if bFound:
                        lst.append(tmp)
            tmp=tmp.next
        return lst
    def __getChildsAttr__(self,node,attr):
        lst=[]
        if node is None:
            return lst
        tmp=node.children
        while tmp is not None:
            if tmp.type=='element':
                if tmp.hasProp(attr) is not None:
                    lst.append(tmp)
            tmp=tmp.next
        return lst
    def __getChildsNoAttr__(self,node,attr):
        lst=[]
        if node is None:
            return lst
        tmp=node.children
        while tmp is not None:
            if tmp.type=='element':
                if tmp.hasProp(attr) is None:
                    lst.append(tmp)
            tmp=tmp.next
        return lst
    def getChildren(self,node=None):
        if node is None:
            node=self.getBaseNode()#self.root
        return node.children
    def procChildsAttr(self,node,attr,func,*args):
        if node is None:
            return -2
        tmp=node.children
        while tmp is not None:
            vtLdBd.check()
            if tmp.type=='element':
                if tmp.hasProp(attr) is not None:
                    if func(tmp,args)<0:
                        return -1
            tmp=tmp.next
        return 0
    def procChildsNoAttr(self,node,attr,func,*args):
        if node is None:
            return -2
        tmp=node.children
        while tmp is not None:
            vtLdBd.check()
            if tmp.type=='element':
                if tmp.hasProp(attr) is None:
                    if func(tmp,args)<0:
                        return -1
            tmp=tmp.next
        return 0
    def procChilds(self,node,func,*args):
        if node is None:
            return -2
        tmp=node.children
        while tmp is not None:
            vtLdBd.check()
            if tmp.type=='element':
                if func(tmp,args)<0:
                    return -1
            tmp=tmp.next
        return 0
    def procChildsAttrExt(self,node,attr,func,*args,**kwargs):
        if node is None:
            return -2
        tmp=node.children
        while tmp is not None:
            vtLdBd.check()
            if tmp.type=='element':
                if tmp.hasProp(attr) is not None:
                    if func(tmp,*args,**kwargs)<0:
                        return -1
            tmp=tmp.next
        return 0
    def procChildsNoAttrExt(self,node,attr,func,*args,**kwargs):
        if node is None:
            return -2
        tmp=node.children
        while tmp is not None:
            vtLdBd.check()
            if tmp.type=='element':
                if tmp.hasProp(attr) is None:
                    if func(tmp,*args,**kwargs)<0:
                        return -1
            tmp=tmp.next
        return 0
    def procChildsExt(self,node,func,*args,**kwargs):
        if node is None:
            return -2
        tmp=node.children
        while tmp is not None:
            vtLdBd.check()
            if tmp.type=='element':
                if func(tmp,*args,**kwargs)<0:
                    return -1
            tmp=tmp.next
        return 0
    def procChildsKeys(self,node,func,*args,**kwargs):
        if node is None:
            return -2
        tmp=node.children
        while tmp is not None:
            vtLdBd.check()
            if tmp.type=='element':
                if tmp.hasProp(self.attr) is not None:
                    if func(tmp,*args,**kwargs)<0:
                        return -1
            tmp=tmp.next
        return 0
        #self.procChildsAttr(node,self.attr,func,*args)
    def procChildsNoKeys(self,node,func,*args,**kwargs):
        if node is None:
            return -2
        tmp=node.children
        while tmp is not None:
            vtLdBd.check()
            if tmp.type=='element':
                if tmp.hasProp(self.attr) is None:
                    if func(tmp,*args,**kwargs)<0:
                        return -1
            tmp=tmp.next
        return 0
    def procChildsKeysRec(self,lv,node,func,*args,**kwargs):
        if node is None:
            return
        tmp=node.children
        while tmp is not None:
            vtLdBd.check()
            if tmp.type=='element':
                if tmp.hasProp(self.attr) is not None:
                    if func(tmp,*args,**kwargs)<0:
                        return -1
                    else:
                        if lv>0:
                            if self.procChildsKeysRec(lv-1,tmp,func,*args,**kwargs)<0:
                                return -1
            tmp=tmp.next
        return 0
    def procKeysRec(self,lv,node,func,*args,**kwargs):
        if node is None:
            return -2
        if node.type=='element':
            vtLdBd.check()
            if node.hasProp(self.attr) is not None:
                if func(node,*args,**kwargs)<0:
                    return -1
                else:
                    if lv>0:
                        return self.procChildsKeysRec(lv-1,node,func,*args,**kwargs)
            return 0
        return -3
    def __procChildsKeysRecHierReStart__(self,lHierOld,lHierNew,lv,node,func,*args,**kwargs):
        if lHierOld is None:
            bSkip=False
        else:
            bSkip=True
        tmp=node.children
        while tmp is not None:
            vtLdBd.check()
            if tmp.type=='element':
                if tmp.hasProp(self.attr) is not None:
                    idNum=self.getKeyNum(tmp)
                    if bSkip==True:#lHierOld is not None:
                        if lHierOld[0]==idNum:
                            iLen=len(lHierOld)
                            if iLen>1:
                                #lHierNew.append(idNum)
                                #print 'down',lv,lHierOld,lHierNew
                                iRet=self.__procChildsKeysRecHierReStart__(lHierOld[1:],lHierNew,
                                            lv-1,tmp,func,*args,**kwargs)
                                #print '  up',lv,lHierOld,lHierNew,iRet
                                if iRet<0:
                                    lHierNew.append(idNum)
                                    return -1
                                bSkip=False
                            else:
                                #for i in xrange(iLen):
                                #    del lHierOld[0]
                                bSkip=False
                    else:
                        if func(tmp,*args,**kwargs)<0:
                            lHierNew.append(idNum)
                            #print lHierNew
                            return -1
                        else:
                            if lv>0:
                                iRet=self.__procChildsKeysRecHierReStart__(None,lHierNew,
                                                lv-1,tmp,func,*args,**kwargs)
                                if iRet<0:
                                    lHierNew.append(idNum)
                                    return -1
                                #lHierNew.remove(idNum)
            tmp=tmp.next
        return 0
    def procKeysRecHierReStart(self,lHier,lv,node,func,*args,**kwargs):
        if node is None:
            return -2
        if node.type=='element':
            vtLdBd.check()
            if node.hasProp(self.attr) is not None:
                if lHier is None:
                    if func(node,*args,**kwargs)<0:
                        return -1,[self.getKeyNum(node)]
                else:
                    if len(lHier)==1:
                        if lHier[0]==self.getKeyNum(node):
                            lHier=None
                if lv>0:
                    return self.procChildsKeysRecHierReStart(lHier,lv-1,node,func,*args,**kwargs)
            return 0,None
        return -3,None
    def procChildsKeysRecSkip(self,lv,node,func,*args,**kwargs):
        if node is None:
            return
        tmp=node.children
        while tmp is not None:
            vtLdBd.check()
            if tmp.type=='element':
                if tmp.hasProp(self.attr) is not None:
                    if self.IsSkip(tmp)==False:
                        if func(tmp,*args,**kwargs)<0:
                            return -1
                        else:
                            if lv>0:
                                if self.procChildsKeysRecSkip(lv-1,tmp,func,*args,**kwargs)<0:
                                    return -1
            tmp=tmp.next
        return 0
    def procKeysRecSkip(self,lv,node,func,*args,**kwargs):
        if node is None:
            return -2
        if node.type=='element':
            vtLdBd.check()
            if node.hasProp(self.attr) is not None:
                if self.IsSkip(node)==False:
                    if func(node,*args,**kwargs)<0:
                        return -1
                    else:
                        if lv>0:
                            return self.procChildsKeysRecSkip(lv-1,node,func,*args,**kwargs)
            return 0
        return -3
    def procChildsKeysRecHierarchy(self,par,parId,node,func,*args,**kwargs):
        if node is None:
            return
        tmp=node.children
        nodeId=self.getKey(node)
        while tmp is not None:
            vtLdBd.check()
            if tmp.type=='element':
                if tmp.hasProp(self.attr) is not None:
                    iRet=func(node,nodeId,tmp,*args,**kwargs)
                    if iRet<0:
                        return -1
                    elif iRet>0:
                        if self.procChildsKeysRecHierarchy(node,nodeId,tmp,func,*args,**kwargs)<0:
                                return -1
            tmp=tmp.next
        return 0
    def procAttr(self,node,attr,func,*args):
        if node is None:
            return -2
        if node.type=='element':
            vtLdBd.check()
            if node.hasProp(attr) is not None:
                if func(node,args)<0:
                    return -1
                return 0
        return -3
    def procAttrExt(self,node,attr,func,*args,**kwargs):
        if node is None:
            return -2
        if node.type=='element':
            vtLdBd.check()
            if node.hasProp(attr) is not None:
                if func(node,*args,**kwargs)<0:
                    return -1
                return 0
        return -3
    def procAttrs(self,node,func,*args):
        if node is None:
            return -2
        if node.type=='element':
            tmpAttr=node.get_properties()
            while tmpAttr is not None:
                if func(node,tmpAttr.name,tmpAttr.content,args)<0:
                    return -1
                tmpAttr=tmpAttr.next
            return 0
        return -3
    def procAttrsExt(self,node,func,*args,**kwargs):
        if node is None:
            return -2
        if node.type=='element':
            tmpAttr=node.get_properties()
            while tmpAttr is not None:
                if func(node,tmpAttr.name,tmpAttr.content,*args,**kwargs)<0:
                    return -1
                tmpAttr=tmpAttr.next
            return 0
        return -3
    def findChildsKeys(self,node,hier,func,*args,**kwargs):
        if node is None:
            vtLog.vtLngCur(vtLog.CRITICAL,'possible to FIX',self.appl)
            return 0,hier,None      #061227:wro added
        #vtLog.CallStack('')
        #print hier
        tmp=node.children
        while tmp is not None:
            vtLdBd.check()
            if tmp.type=='element':
                if tmp.hasProp(self.attr) is not None:
                    ret=func(tmp,*args,**kwargs)
                    if ret!=0:
                        return ret,hier,tmp
                    if hier is not None:   # 060806
                        ret,hierRet,tmpRet=self.findChildsKeys(tmp,hier+[tmp],func,*args,**kwargs)
                        if ret!=0:
                            return ret,hierRet,tmpRet
            tmp=tmp.next
        return 0,hier,None
    def match(self,node,tagName,filter):
        if node is None:
            return False
        tmp=node.children
        i=tagName.find('|')
        if i>0:
            sAttr=tagName[i+1:]
            tagName=tagName[:i]
        else:
            sAttr=None
        #bFound=False
        while tmp is not None:
            if tmp.type=='element':
                if tmp.name==tagName:
                    if sAttr is None:
                        val=self.getText(tmp)
                        if filter.match(val):
                            bFound=True
                            return True
                    else:
                        tmpAttr=tmp.get_properties()
                        while tmpAttr is not None:
                            if tmpAttr.name==sAttr:
                                if filter.match(tmpAttr.content):
                                    bFound=True
                                    return True
                                break
                            tmpAttr=tmpAttr.next
            tmp=tmp.next
        return False
    def __getChildsRec__(self,node,tagName,lst):
        if node is None:
            return 
        tmp=node.children
        while tmp is not None:
            if tmp.type=='element':
                bFound=False
                if tagName is not None:
                    if tmp.name==tagName:
                        bFound=True
                else:
                    bFound=True
                if bFound:
                    lst.append(tmp)
            if tmp.children is not None:
                self.__getChildsRec__(tmp,tagName,lst)
            tmp=tmp.next
    def getText(self,node):
        s=u''
        if node is None:
            return s
        tmp=node.children
        while tmp is not None:
            sT=tmp.type
            if sT=='text':
                #print type(tmp.content),tmp.content
                #print type(tmp.getContent()),tmp.getContent()
                #s=s+unicode(tmp.content,self.applEncoding)
                s=s+tmp.content.decode(self.applEncoding)
                #s=s+tmp.content
            elif sT=='cdata':
                s=s+tmp.content.decode(self.applEncoding)
            elif sT=='element':
                break
            tmp=tmp.next
        #return s.encode(self.applEncoding)
        return s
    def hasAttribute(self,node,attr):
        if node is None:
            return -1
        if node.type=='element':
            tmpAttr=node.get_properties()
            if attr is None:
                if tmpAttr is not None:
                    return 1
            while tmpAttr is not None:
                if tmpAttr.name==attr:
                    return 1
                tmpAttr=tmpAttr.next
        return 0
    def getAttribute(self,node,attr):
        if node is None:
            return ''
        if node.type=='element':
            tmpAttr=node.get_properties()
            while tmpAttr is not None:
                if tmpAttr.name==attr:
                    return tmpAttr.content
                tmpAttr=tmpAttr.next
        return ''
    def getChildAttribute(self,node,tagName,attr):
        if node is None:
            node=self.getBaseNode()#self.root
        if node is None:
            return None
        tmp=node.children
        while tmp is not None:
            if tmp.type=='element':
                if tmp.name==tagName:
                    tmpAttr=tmp.get_properties()
                    while tmpAttr is not None:
                        if tmpAttr.name==attr:
                            return tmpAttr.content
                        tmpAttr=tmpAttr.next
            tmp=tmp.next
        return None
    def setChildAttribute(self,node,tagName,attr,attrVal):
        if node is None:
            node=self.getBaseNode()#self.root
        if node is None:
            return None
        tmp=node.children
        while tmp is not None:
            if tmp.type=='element':
                if tmp.name==tagName:
                    tmpAttr=tmp.get_properties()
                    while tmpAttr is not None:
                        if tmpAttr.name==attr:
                            self.__setModified__()
                            tmpAttr.setContent(attrVal)
                            return 0
                        tmpAttr=tmpAttr.next
                    a1=node.newProp(attr,attrVal)
                    self.__setModified__()
                    if attr==self.attr:
                        self.iElemIDCount+=1
                    return 1
            tmp=tmp.next
        return None
    def setAttribute(self,node,attr,attrVal):
        if node is None:
            return -1
        if node.type=='element':
            tmpAttr=node.get_properties()
            while tmpAttr is not None:
                if tmpAttr.name==attr:
                    tmpAttr.setContent(attrVal)
                    self.__setModified__()
                    return 0
                tmpAttr=tmpAttr.next
            a1=node.newProp(attr,attrVal)
            self.__setModified__()
            if attr==self.attr:
                self.iElemIDCount+=1
            return 1
    def removeAttribute(self,node,attr):
        if node is None:
            return -1
        if node.type=='element':
            node.unsetProp(attr)
            self.__setModified__()
            if attr==self.attr:
                self.iElemIDCount-=1
            return 0
    def getNodeInfos(self,node,lang=None,name='basic'):
        #self.sem.acquire()
        #print 'getNodeInfos start'
        try:
            dInfos=self.dUsrInfos[name]['infos']
            dRes=self.dUsrInfos[name]['res']
            iLenRes=self.dUsrInfos[name]['len']
            #if self.dUsrInfos[name]['singleLang']:
            #    if lang is None:
            #        lang=self.lang
        except:
            return '',{}
        #dInfos=self.dInfos
        #dRes=self.dRes
        #iLenRes=self.iLenRes
        if lang is None:
            lang=self.lang
        i=0
        for k in dRes.keys():   # mod
            dRes[k]=''
        if node is None:
            #self.sem.release()
            return '',dRes
        def getInfo(node,d,name):
            if d.has_key(''):
                dRes[name]=self.getText(node)
            keys=d.keys()
            try:
                keys.remove('')
            except:
                pass
            if len(keys)>0:
                tmpAttr=node.get_properties()
                while tmpAttr is not None:
                    if tmpAttr.name in keys:
                        try:
                            idx=keys.index(tmpAttr.name)
                            dRes[d[keys[idx]]]=tmpAttr.content
                        except:
                            pass
                    tmpAttr=tmpAttr.next
                
        if dInfos.has_key(''):
            getInfo(node,dInfos[''],'')
            i+=1
        def getChildsInfos(node,dInfos,lst):
            tmp=node.children
            while tmp is not None:
                if tmp.type=='element':
                    try:
                        if tmp.name in dInfos:
                            d=dInfos[tmp.name]
                            if type(d.values()[0])==types.DictType:
                                #process child
                                getChildsInfos(tmp,d,lst+[tmp.name])
                                if i>=self.iLenRes:
                                    #print 'getNodeInfos end'
                                    #self.sem.release()
                                    return node.name,dRes
                                tmp=tmp.next
                                continue
                        else:
                            tmp=tmp.next
                            continue
                    except:
                        tmp=tmp.next
                        continue
                    bFound=False
                    bFallback=False
                    sCombName=':'.join(lst+[tmp.name])
                    if lang is None:
                        bFound=True
                    else:
                        if self.getAttribute(tmp,'language')==lang:
                            bFound=True
                        else:
                            if len(dRes[sCombName])<=0:
                                bFound=True
                                bFallback=True
                    if bFound:
                        try:
                            getInfo(tmp,d,sCombName)
                            if bFallback==False:
                                i+=1
                                if i>=iLenRes:
                                    #print 'getNodeInfos end'
                                    #self.sem.release()
                                    return node.name,dRes
                        except:
                            pass
                tmp=tmp.next
        getChildsInfos(node,dInfos,[])
        #print 'getNodeInfos end'
        #self.sem.release()
        return node.name,dRes
    def getNodeInfoVal(self,node,sVal,lang=None):
        if lang is None:
            lang=self.lang
        #self.sem.acquire()
        #print 'getNodeInfos start'
        #vtLog.CallStack('')
        #print sVal
        i=0
        if node is None:
            #self.sem.release()
            return ''
        if sVal.find('@')>=0:
            lstAlias=sVal.split('@')
            try:
                docAlias=self.netMaster.GetDocAlias(lstAlias[1])
                if docAlias is None:
                    return ''
                sInfo=self.getNodeInfoVal(node,lstAlias[0],lang)
                #print sInfo
                nodeTmp=docAlias.getNodeById(sInfo)
                #print nodeTmp
                return docAlias.getNodeInfoVal(nodeTmp,lstAlias[2],lang)
            except:
                vtLog.vtLngTB(self.GetOrigin())
                return ''
        def getChildsInfos(node,lst,sAttr):
            tmp=node.children
            while tmp is not None:
                if tmp.type=='element':
                    if tmp.name==lst[0]:
                        #process child
                        if len(lst)>1:
                            return getChildsInfos(tmp,lst[1:])
                        if len(sAttr)>0:
                            tmpAttr=tmp.get_properties()
                            while tmpAttr is not None:
                                if tmpAttr.name==sAttr:
                                    return tmpAttr.content
                                tmpAttr=tmpAttr.next
                            return ''
                        else:
                            return self.getText(tmp)
                tmp=tmp.next
            return ''
        lst=sVal.split(':')
        strs=lst[-1].split('|')
        sAttr=''
        if len(strs)>1:
            lst[-1]=strs[0]
            sAttr=strs[1]
        if len(lst)==1:
            if len(lst[0])==0:
                if len(sAttr)>0:
                    tmpAttr=node.get_properties()
                    while tmpAttr is not None:
                        if tmpAttr.name==sAttr:
                            return tmpAttr.content
                        tmpAttr=tmpAttr.next
                    return ''
        return getChildsInfos(node,lst,sAttr)
    def getParent(self,node):
        if node is None:
            return None
        return node.parent
    def getNodeParent(self,node):
        if node is None:
            return None
        if self.hasKey(node):
            return node
        else:
            return self.getNodeParent(node.parent)
    def updateParentDict(self,par,node):
        pass
    def removeParentDict(self,node):
        pass
    def setText(self,node,newValue):
        if node is None:
            return -1
        tmp=node.children
        while tmp is not None:
            if tmp.type == 'text':
                #tmp.setContent(newValue.encode(self.xmlEncoding))
                tmp.setContent(newValue.encode(self.applEncoding))
                self.__setModified__()
                #tmp.setContent(newValue)
                return 1
            tmp=tmp.next
        #tmp=self.doc.newDocText(newValue.encode(self.xmlEncoding))
        tmp=self.doc.newDocText(newValue.encode(self.applEncoding))
        self.__setModified__()
        #tmp=self.doc.newDocText(newValue)
        node.addChild(tmp)
        return 0
    def setNodeText(self,node,tagName,newValue):
        if node is None:
            return
        nNode=self.getChild(node,tagName)
        if nNode is not None:
            tmp=nNode.children
            while tmp is not None:
                if tmp.type=='text':
                    #tmp.setContent(newValue.encode(self.xmlEncoding))
                    tmp.setContent(newValue.encode(self.applEncoding))
                    self.__setModified__()
                    #tmp.setContent(newValue)
                    return 1
            #tmp=self.doc.newDocText(newValue.encode(self.xmlEncoding))
            tmp=self.doc.newDocText(newValue.encode(self.applEncoding))
            self.__setModified__()
            #tmp=self.doc.newDocText(newValue)
            nNode.addChild(tmp)
            return 3
        else:
            #tmp=self.doc.newDocNode(None,tagName,newValue.encode(self.xmlEncoding))
            nNode=self.doc.newDocNode(None,tagName,None)#newValue.encode(self.applEncoding))
            self.appendChild(node,nNode)
            tmp=self.doc.newDocText(newValue.encode(self.applEncoding))
            nNode.addChild(tmp)
            self.__setModified__()
            #tmp=self.doc.newDocNode(None,tagName,newValue)
            self.iElemCount+=1
            return 0
            #self.setNodeText(node,tagName,newValue)
    def AddValueToDictByDict(self,node,dDef,dVal,bMultiple,*args,**kwargs):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurCls(vtLog.DEBUG,'key:%s;bMulti:%d;dDef:%s;dVal:%s;args:%s;kwargs:%s'%(self.getKey(node),bMultiple,
        #                        vtLog.pformat(dDef),
        #                        vtLog.pformat(dVal),vtLog.pformat(args),vtLog.pformat(kwargs)),self)
        if node is None:
            return -1
        tmp=node.children
        if 'lang' in kwargs:
            lang=kwargs['lang']
        else:
            lang=self.lang
        while tmp is not None:
            if tmp.type=='element':
                sTag=tmp.name
                if sTag in dDef:
                    if sTag not in dVal or bMultiple:
                        #print sTag,dVal
                        if self.IsNodeKeyValid(tmp):
                            if bMultiple==False:
                                dVal[sTag]={}
                        else:
                            if bMultiple:
                                if sTag in dVal:
                                    dVal[sTag].append(self.getAttributeVal(tmp))
                                else:
                                    dVal[sTag]=[self.getAttributeVal(tmp)]
                            else:
                                dVal[sTag]=self.getAttributeVal(tmp)
                    tmpAttr=tmp.get_properties()
                    while tmpAttr is not None:
                        if tmpAttr.name=='language':
                            if lang==tmpAttr.content:
                                if bMultiple:
                                    if tmp.name in dVal:
                                        dVal[tmp.name].append(tmp.content.decode(self.applEncoding))
                                    else:
                                        dVal[tmp.name]=[tmp.content.decode(self.applEncoding)]
                                else:
                                    dVal[tmp.name]=tmp.content.decode(self.applEncoding)
                                break
                        tmpAttr=tmpAttr.next
                    tmpAttr=tmp.get_properties()
            tmp=tmp.next
        tmpAttr=node.get_properties()
        while tmpAttr is not None:
            sTag=''.join([u'|',tmpAttr.name.decode(self.applEncoding)])
            if sTag in dDef:
                dVal[sTag]=tmpAttr.content.decode(self.applEncoding)
            tmpAttr=tmpAttr.next
        return 0
    def GetAttrValueToDict(self,node,*args,**kwargs):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurCls(vtLog.DEBUG,'args:%s;kwargs:%s'%(vtLog.pformat(args),vtLog.pformat(kwargs)),self)
        if node is None:
            return None
        tmp=node.children
        if 'lang' in kwargs:
            lang=kwargs['lang']
        else:
            lang=self.lang
        d={}
        while tmp is not None:
            if tmp.type=='element':
                if not self.IsNodeKeyValid(tmp):
                    sTag=tmp.name
                    sVal=self.getAttributeVal(tmp)
                    sValStrip=sVal.strip()
                    if self.hasChilds(tmp):
                        if len(sVal.strip())!=0:
                            d[sTag]=sVal
                    else:
                        tmpAttr=tmp.get_properties()
                        while tmpAttr is not None:
                            if tmpAttr.name=='language':
                                if lang==tmpAttr.content:
                                    d[sTag]=sVal
                                    break
                            tmpAttr=tmpAttr.next
                        if sTag not in d:
                            d[sTag]=sVal
            tmp=tmp.next
        return d
    def __createNewLib__(self,root,revision='0.0.1',attr='id'):
        self.doc=libxml2.newDoc(revision)
        self.root=self.doc.newDocNode(None,root,None)
        self.doc.addChild(self.root)
        self.attr=attr
    def __createSubNodeLib__(self,node,tagname):
        n=self.doc.newDocNode(None,tagname,None)
        node.addChild(n)
        return n
    def __createSubNodeAttrLib__(self,node,tagname,attr,attrVal):
        n=self.doc.newDocNode(None,tagname,None)
        node.addChild(n)
        n.newProp(attr,attrVal)
        return n
    def __createMissingSubNodeByLstLib__(self,node,lst):
        bAdd=False
        for it in lst:
            n=self.getChild(node,it)
            if n is None:
                n=self.doc.newDocNode(None,it,None)
                node.addChild(n)
                self.__setModified__()
                self.iElemCount+=1
                bAdd=True
            node=n
        return bAdd
    def __createSubNodeTextLib__(self,node,tagname,val):
        n=self.doc.newDocNode(None,tagname,None)#val.encode(self.applEncoding))
        node.addChild(n)
        #self.appendChild(node,nNode)
        tmp=self.doc.newDocText(val.encode(self.applEncoding))
        n.addChild(tmp)
        return n
    def __createSubNodeTextAttrLib__(self,node,tagname,val,attr,attrVal,align=False):
        #n=self.createNode(tagname)
        n=self.doc.newDocNode(None,tagname,None)
        #val.encode(self.applEncoding))
        node.addChild(n)
        tmp=self.doc.newDocText(val.encode(self.applEncoding))
        n.addChild(tmp)
        n.newProp(attr,attrVal)
        return n
    def createNode(self,tagname):
        n=self.doc.newDocNode(None,tagname,None)
        return n
    def __checkIds__(self,node):
        childs=self.getChilds(node)
        for c in childs:
            if c.name in self.skip:
                continue
            if self.__isSkip__(c):
                continue
            id=self.getAttribute(c,self.attr)
            if len(id)>0:
                self.ids.CheckId(c,self,id)
            self.__checkIds__(c)
    def __deleteNode__(self,node,nodePar=None):
        try:
            self.__deleteNodeIds__(node)
            self.iElemCount-=self.GetElementCount(node)
            self.iElemIDCount-=self.GetElementAttrCount(node)
            n=node.next
            if n is not None:
                if n.type=='text':
                    if len(n.content.strip())<=0:
                        n.unlinkNode()
                        n.freeNode()
            node.unlinkNode()
            node.freeNode()
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def cloneNode(self,node,deep):
        if deep:
            tmp=node.copyNode(1)
        else:
            tmp=node.copyNode(2)
            #self.setText(tmp,self.getText(node))    # 091027:wro fix for TestPrg
            s=self.getText(node)
            if len(s.strip())>0:
                tmpText=self.doc.newDocText(s.encode(self.applEncoding))
                tmp.addChild(tmpText)
        return tmp
    def unlinkNode(self,node,nodePar=None):
        node.unlinkNode()

