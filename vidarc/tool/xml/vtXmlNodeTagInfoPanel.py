#----------------------------------------------------------------------
# Name:         vtXmlNodeTagInfoPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120527
# CVS-ID:       $Id: vtXmlNodeTagInfoPanel.py,v 1.3 2015/07/19 08:10:27 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import vidarc.tool.lang.vtLgBase as vtLgBase
    from vidarc.tool.xml.vtXmlNodeGuiPanel import vtXmlNodeGuiPanel
    
    #from vidarc.tool.lang.vtLgSelector import vtLgSelector
    #from vidarc.tool.lang.vtLgSelector import vEVT_VTLANG_SELECTION_CHANGED
    
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vtXmlNodeTagInfoPanel(vtXmlNodeGuiPanel):
    VERBOSE=0
    TAGNAME_TAG='tag'
    TAGNAME_NAME='name'
    def __initCtrl__(self,*args,**kwargs):
        global _
        _=vtLgBase.assignPluginLang('vidarc.')
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        self.SetVerbose(__name__,iVerbose=-1,kwargs=kwargs)
        
        lWid=[
            ('lblRg',               (0,0),(1,1),{'name':'lblHier','label':_(u'Hierarchy')},None),
            ('txtRd',               (0,1),(1,3),{'name':'txtHier','value':u'','mod':False,},None),
            
            ('lblRg',               (1,0),(1,1),{'name':'lblTag','label':_(u'Tag')},None),
            ('txtRd',               (1,1),(1,1),{'name':'txtTag','value':u'','mod':False,},None),
            ('lblRg',               (1,2),(1,1),{'name':'lblName','label':_(u'Name')},None),
            ('txtRd',               (1,3),(1,1),{'name':'txtName','value':u'','mod':False,},None),
            
            ('lblRg',               (2,0),(1,1),{'name':'lblType','label':_(u'Type')},None),
            ('txtRd',               (2,1),(1,1),{'name':'txtType','value':u'','mod':False,},None),
            ('txtRd',               (2,2),(1,2),{'name':'txtID','value':u'','mod':False,},None),
            ]
        
        lWidExt=kwargs.get('lWid',[])
        vtXmlNodeGuiPanel.__initCtrl__(self,lWid=lWid)
        
        lGrowRow=[]
        lGrowCol=[(1,1),(3,1),]
        return lGrowRow,lGrowCol
    def __initCls__(self,*args,**kwargs):
        self.SetVerbose(__name__,iVerbose=VERBOSE,kwargs=kwargs)
    def Apply(self):
        if self.IsMainThread()==False:
            return
        self.__logInfo__(''%())
        try:
            if self.node is not None:
                self.GetNode()
                #id=self.viTag.nodeId
                #wx.PostEvent(self,vtXmlTagApply(self,id))
        except:
            self.__logTB__()
    def Cancel(self):
        if self.IsMainThread()==False:
            return
        self.__logInfo__(''%())
        try:
            if self.node is not None:
                sTag=self.doc.getTagName(self.node)
                self.__apply2Dependent__(sTag,'Cancel')
            self.SetNode(self.node)
            #id=self.viTag.nodeId
            #wx.PostEvent(self,vtXmlTagCancel(self,id))
        except:
            self.__logTB__()
    def OnApply(self,evt):
        try:
            self.__logDebug__(''%())
            self.Apply()
        except:
            self.__logTB__()
    def OnCancel(self,evt):
        try:
            self.__logDebug__(''%())
            self.Cancel()
        except:
            self.__logTB__()
    def procSetNodeById(self,sId):
        try:
            self.__logDebug__('sId:%s'%(sId))
            if self.doc.IsKeyValid(sId)==False:
                self.doc.CallBack(self.Clear)
                node=None
            else:
                node=self.doc.getNodeByIdNum(sId)
            self.doc.CallBack(self.SetNode,node)
        except:
            self.__logTB__()
    def SetNodeById(self,sId):
        try:
            self.__logDebug__('sId:%s'%(sId))
            if self.doc is None:
                self.__logError__('doc is None')
                return
            self.doc.DoSafe(self.procSetNodeById,sId)
        except:
            self.__logTB__()
    def __getNodeVal_(self,sLang):
        try:
            if self.doc is None:
                return u''
            if self.node is None:
                return u''
            return self.doc.getNodeTextLang(self.node,self.TAGNAME,sLang)
        except:
            self.__logTB__()
        return u''
    def __Clear__(self):
        if self.IsMainThread()==False:
            return
        bDbg=self.GetVerbose(iVerbose=5)
        if bDbg:
            self.__logDebug__(''%())
        self.txtHier.SetValue(u'')
        self.txtTag.SetValue(u'')
        self.txtName.SetValue(u'')
        self.txtType.SetValue(u'')
        self.txtID.SetValue(u'')
    def SetRegNode(self,obj):
        bDbg=self.GetVerbose(iVerbose=5)
        if bDbg:
            self.__logDebug__(''%())
        vtXmlNodeGuiPanel.SetRegNode(self,obj)
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            # add code here
        except:
            self.__logTB__()
    def __SetNode__(self,node):
        if self.IsMainThread()==False:
            return
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            # add code here
            sVal=self.doc.GetTagNamesRaw(self.node)
            self.txtHier.SetValue(sVal)
            
            sVal=self.doc.getNodeText(self.node,self.TAGNAME_TAG)
            self.txtTag.SetValue(sVal)
            sVal=self.doc.getNodeTextLang(self.node,self.TAGNAME_NAME,None)
            self.txtName.SetValue(sVal)
            
            sTag=self.doc.getTagName(node)
            if len(sTag)>0:
                o=self.doc.GetRegisteredNode(sTag)
                if o is not None:
                    self.txtType.SetValue(o.GetDescription())
                else:
                    self.txtType.SetValue(' '.join([sTag,'?']))
            else:
                self.txtType.SetValue('')
            sID=u''
            
            if node is not None:
                iId=long(self.doc.getKey(node))
                if iId>=0:
                    try:
                        sID=self.doc.ConvertIdNum2Str(iId)
                    except:
                        pass
                if hasattr(self.doc,'getKeyInst'):
                    try:
                        iIID=self.doc.getKeyInst(node)
                        if iIID>0:
                            sIID=self.doc.ConvertIdNum2Str(iIID)
                            sID=''.join([sID,' (',sIID,')'])
                    except:
                        pass
            self.txtID.SetValue(sID)

            
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        if self.IsMainThread()==False:
            return
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            # add code here
            #sVal=self.txtTag.GetValue()
            #self.doc.setNodeText(self.node,self.TAGNAME_TAG,sVal)
            #sVal=self.txtName.GetValue()
            #self.doc.setNodeTextLang(self.node,self.TAGNAME_NAME,sVal,None)
        except:
            self.__logTB__()
    def __Close__(self):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            # add code here
        except:
            self.__logTB__()
    def __Cancel__(self):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            # add code here
        except:
            self.__logTB__()
    def __Lock__(self,flag):
        bDbg=self.GetVerbose(iVerbose=5)
        if bDbg:
            self.__logDebug__(''%())
        if flag:
            # add code here
            #self.cbApply.Enable(False)
            #self.cbCancel.Enable(False)
            pass
        else:
            # add code here
            #self.cbApply.Enable(True)
            #self.cbCancel.Enable(True)
            pass
