#----------------------------------------------------------------------------
# Name:         vtXmlGrpAttrTreeList.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtXmlGrpAttrTreeList.py,v 1.9 2010/02/21 15:51:36 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import wx
import wx.gizmos

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

import vidarc.tool.xml.vtXmlDom as vtXmlDom

from vidarc.tool.xml.vtXmlGrpTreeList import *

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.tool.art.vtArt as vtArt

import traceback
import fnmatch,time
import thread,threading
from types import TupleType

import images

class vtXmlGrpAttrTreeList(vtXmlGrpTreeList):
    TREE_BUILD_ON_DEMAND=False
    def __init__(self, parent, id, pos, size, style, name,cols,
                    master=False,controller=False,
                    gui_update=False,verbose=0):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        if id<0:
            id=wx.NewId()
        self.attrs=[]
        self.attrTreeItemName=_('__attr__')
        vtXmlGrpTreeList.__init__(self,parent, id, pos, size, style, name,cols,
                    master,controller,gui_update,verbose=verbose)
    def __init_properties__(self):
        self.attrs=[]
        self.attrTreeItemName=_('__attr__')
        self.bIncludeAttr=True
        vtXmlGrpTreeList.__init_properties__(self)
    def SetIncludeAttr(self,bFlag):
        self.bIncludeAttr=bFlag
    def UpdateLang(self):
        self.SetNode(self.rootNode)
    def SetDftGrouping(self):
        self.grouping=[]
        self.label=[[('tag',''),('name','')],[('name','')],[(None,'')]]
        self.attrs=['*']
        self.bGrouping=False
    def SetAttrs(self,attrs):
        self.attrs=attrs
    #def SetupImageList(self):
    #    if vtXmlGrpTree.SetupImageList(self)==False:
    #        return
    def __setupImageList__(self):
        bRet=vtXmlGrpTreeList.__setupImageList__(self)
        if bRet:
            if 'elem' not in self.imgDict:
                self.imgDict['elem']={}
            if 'attr' not in self.imgDict:
                self.imgDict['attr']={}
            if 'txt' not in self.imgDict:
                self.imgDict['txt']={}
            if 'cdata' not in self.imgDict:
                self.imgDict['cdata']={}
            if 'comm' not in self.imgDict:
                self.imgDict['comm']={}
            if 'grp' not in self.imgDict:
                self.imgDict['grp']={}
            #self.imgDict={'elem':{},'attr':{},'txt':{},'cdata':{},'comm':{},'grp':{}}
            #self.imgLstTyp=wx.ImageList(16,16)
            if 'dft' not in self.imgDict['elem']:
                img=images.getElemBitmap()
                self.imgDict['elem']['dft']=[self.imgLstTyp.Add(img)]
                
                img=images.getElemSelBitmap()
                self.imgDict['elem']['dft'].append(self.imgLstTyp.Add(img))
            if 'value' not in self.imgDict['elem']:
                bmp=vtArt.getBitmap(vtArt.Attr)
                img=bmp.ConvertToImage()
                self.__addImage2ImageList__('elem','value',img,img)
            
            if 'dft' not in self.imgDict['attr']:
                img=images.getAttributeBitmap()
                self.imgDict['attr']['dft']=[self.imgLstTyp.Add(img)]
                img=images.getAttributeSelBitmap()
                self.imgDict['attr']['dft'].append(self.imgLstTyp.Add(img))
            
            if 'id' not in self.imgDict['attr']:
                bmp=vtArt.getBitmap(vtArt.ID)
                img=bmp.ConvertToImage()
                self.__addImage2ImageList__('attr','id',img,img)
            
            if 'iid' not in self.imgDict['attr']:
                bmp=vtArt.getBitmap(vtArt.IDInstance)
                img=bmp.ConvertToImage()
                self.__addImage2ImageList__('attr','iid',img,img)
            
            if 'fid' not in self.imgDict['attr']:
                bmp=vtArt.getBitmap(vtArt.IDForeign)
                img=bmp.ConvertToImage()
                self.__addImage2ImageList__('attr','fid',img,img)
            
            if 'ih' not in self.imgDict['attr']:
                bmp=vtArt.getBitmap(vtArt.IDReference)
                img=bmp.ConvertToImage()
                self.__addImage2ImageList__('attr','ih',img,img)
            
            if 'fp' not in self.imgDict['attr']:
                bmp=vtArt.getBitmap(vtArt.Fingerprint)
                img=bmp.ConvertToImage()
                self.__addImage2ImageList__('attr','fp',img,img)
            
            #img=prjtimer_tree_images.getPrjBitmap()
            #self.imgDict['elem']['project']=[self.imgLstTyp.Add(img)]
            #img=prjtimer_tree_images.getPrjSelBitmap()
            #self.imgDict['elem']['project'].append(self.imgLstTyp.Add(img))
            
            #self.SetImageList(self.imgLstTyp)
            #pass
        return bRet
    def __addRootByInfos__(self,o,tagName,infos):
        tn=vtXmlGrpTreeList.__addRootByInfos__(self,o,tagName,infos)
        #tn=self.GetRoot()
        self.__addNodeAttr__(o,tn)
        return tn
    def __addElementByInfos__(self,parent,o,tagName,infos,bRet=False):
        tn=vtXmlGrpTreeList.__addElementByInfos__(self,parent,o,tagName,infos,bRet)
        self.__addNodeAttr__(o,tn)
        # add grouping sub nodes here
        if bRet==True:
            return tn
    def __getImgFormat__(self,g,val):
        val=string.strip(val)
        if g[0]=='attr':
            try:
                img=self.imgDict['attr'][val][0]
                imgSel=self.imgDict['attr'][val][0]
            except:
                img=self.imgDict['attr']['dft'][0]
                imgSel=self.imgDict['attr']['dft'][0]
            return (img,imgSel)
                
        tagName=g[0]
        try:
            img=self.imgDict['elem'][tagName][0]
        except:
            img=self.imgDict['elem']['dft'][0]
        try:
            imgSel=self.imgDict['elem'][tagName][1]
        except:
            imgSel=self.imgDict['elem']['dft'][1]
        return (img,imgSel)

    def __addAttr__(self,node,name,val,*args):
        tAttr=args[0][0]
        tn=self.AppendItem(tAttr,name,-1,-1,None)
        img,imgSel=self.__getImgFormat__(('attr',''),name)
        self.SetItemImage(tn,img,0,wx.TreeItemIcon_Normal)
        self.SetItemImage(tn,imgSel,0,wx.TreeItemIcon_Selected)
        self.SetItemText(tn,val,1)
        return 0
    def __addNodeAttr__(self,node,tip):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'id:%s'%(self.doc.getKey(node)),self)
        if tip is None:
            return
        #img=self.imgDict['attr']['dft'][0]
        #ti=self.AppendItem(tip,'__values__',-1,-1,None)
        #self.SetItemImage(ti,img,0,wx.TreeItemIcon_Normal)
        #self.SetItemImage(ti,img,0,wx.TreeItemIcon_Selected)
        ti=tip
        o=self.GetPyData(ti)
        print o,type(0)
        #sVal=self.doc.getText(c)
        #sVal=sVal.strip()
        #if len(sVal)>0:
        #    self.SetItemText(tn,sVal,1)
        if self.bIncludeAttr==True:
            if self.doc.hasAttribute(node,None):
                img=self.imgDict['attr']['dft'][0]
                tAttr=self.AppendItem(ti,self.attrTreeItemName,-1,-1,None)
                self.SetItemImage(tAttr,img,0,wx.TreeItemIcon_Normal)
                self.SetItemImage(tAttr,img,0,wx.TreeItemIcon_Selected)
                self.doc.procAttrs(node,self.__addAttr__,tAttr)
            for c in self.doc.getChildsNoAttr(node,self.doc.attr):
                tagName=self.doc.getTagName(c)
                imgTup=self.imgDict['elem']['value']#[0]
                tn=self.AppendItem(ti,tagName,-1,-1,None)
                self.SetItemImage(tn,imgTup[0],0,wx.TreeItemIcon_Normal)
                self.SetItemImage(tn,imgTup[1],0,wx.TreeItemIcon_Selected)
                sVal=self.doc.getText(c)
                sVal=sVal.strip()
                if len(sVal)>0:
                    self.SetItemText(tn,sVal,1)
                self.__addNodeAttr__(c,tn)
                if self.doc.hasAttribute(c,None) and 0:
                    img=self.imgDict['attr']['dft'][0]
                    tAttr=self.AppendItem(tn,self.attrTreeItemName,-1,-1,None)
                    self.SetItemImage(tAttr,img,0,wx.TreeItemIcon_Normal)
                    self.SetItemImage(tAttr,img,0,wx.TreeItemIcon_Selected)
                    self.doc.procAttrs(c,self.__addAttr__,tAttr)
        else:
            img=self.imgDict['attr']['dft'][0]
            ti=self.AppendItem(tip,self.attrTreeItemName,-1,-1,None)
            self.SetItemImage(ti,img,0,wx.TreeItemIcon_Normal)
            self.SetItemImage(ti,img,0,wx.TreeItemIcon_Selected)
            for c in self.doc.getChildsNoAttr(node,self.doc.attr):
                tagName=self.doc.getTagName(c)
                sLang=self.doc.getAttribute(c,'language')
                if len(sLang)==0:
                    sVal=self.doc.getAttributeVal(c)
                    tAttr=(o,tagName,None)
                else:
                    if sLang==self.doc.GetLang():
                        sVal=self.doc.getAttributeValLang(c,sLang)
                        tAttr=(o,tagName,sLang)
                    else:
                        continue
                #sVal=sVal.strip()
                
                imgTup=self.imgDict['elem']['value']#[0]
                tn=self.AppendItem(ti,tagName,-1,-1,None)
                self.SetPyData(tn,tAttr)
                self.SetItemImage(tn,imgTup[0],0,wx.TreeItemIcon_Normal)
                self.SetItemImage(tn,imgTup[1],0,wx.TreeItemIcon_Selected)
                
                
                #sVal=self.doc.getText(c)
                #sVal=sVal.strip()
                if len(sVal)>0:
                    self.SetItemText(tn,sVal,1)
            
    def __addNodeAttrOld__(self,node,tip):
        if tip is None:
            return
        tagName=self.doc.getTagName(node)
        try:
            img=self.imgDict['attr'][tagName][0]
        except:
            img=self.imgDict['attr']['dft'][0]
        try:
            imgSel=self.imgDict['attr'][tagName][1]
        except:
            imgSel=self.imgDict['attr']['dft'][1]
        tn=self.AppendItem(tip,self.attrTreeItemName,-1,-1,None)
        self.SetItemImage(tn,img,0,which=wx.TreeItemIcon_Normal)
        self.SetItemImage(tn,imgSel,0,
                                which=wx.TreeItemIcon_Expanded)
        self.SetItemImage(tn,imgSel,0,
                                which=wx.TreeItemIcon_Selected)
        tip=tn
        for c in self.doc.getChilds(node):
            sLabel=self.doc.getTagName(c)
            bFound=False
            for attr in self.attrs:
                if fnmatch.fnmatch(sLabel,attr):
                    bFound=True
                    break
            if bFound==False:
                continue
            if sLabel[:2]=='__':
                continue
            if len(self.doc.getAttribute(c,'id'))==0:
                sLang=self.doc.getAttribute(c,'language')
                if len(sLang)==0:
                    sVal=self.doc.getAttributeVal(c)
                else:
                    if sLang==self.doc.GetLang():
                        sVal=self.doc.getAttributeValLang(c,sLang)
                    else:
                        continue
                tid=wx.TreeItemData()
                tid.SetData(c)
                
                tn=self.AppendItem(tip,sLabel,-1,-1,tid)
                self.SetItemImage(tn,img,0,which=wx.TreeItemIcon_Normal)
                self.SetItemImage(tn,imgSel,0,
                                which=wx.TreeItemIcon_Expanded)
                self.SetItemImage(tn,imgSel,0,
                                which=wx.TreeItemIcon_Selected)
                
                self.SetItemText(tn,sVal,1)
    def __findAttr__(self,ti,node2Find):
        if node2Find is None:
            return
        tag2Find=self.doc.getTagName(node2Find)
        triChild=self.GetFirstChild(ti)
        while triChild[0].IsOk():
            o=self.GetPyData(triChild[0])
            if self.doc.getTagName(o)==tag2Find:
                return triChild[0]
            triChild=self.GetNextChild(ti,triChild[1])
        return None
    def FindAttr(self,ti,node2Find):
        if ti is None:
            return 
        triChild=self.GetFirstChild(ti)
        while triChild[0].IsOk():
            if self.GetItemText(triChild[0])==self.attrTreeItemName:
                self.__findAttr__(triChild[0],node2Find)
            triChild=self.GetNextChild(ti,triChild[1])
        return self.__findAttr__(ti,node2Find)
    def GetNodeAttrsTup(self,node=None,check=None):
        #ti=self.__findAttr__(self.tiRoot,node)
        if node==None:
            ti=self.triSelected
        else:
            ti=self.FindTreeItem(node)
        if ti is None:
            return []
        lst=[]
        triChild=self.GetFirstChild(ti)
        if VERBOSE>0:
            self.__logDebug__('check:%s'%(self.__logFmt__(check)))
        while triChild[0].IsOk():
            if self.GetItemText(triChild[0])==self.attrTreeItemName:
                triChildAttr=self.GetFirstChild(triChild[0])
                while triChildAttr[0].IsOk():
                    o=self.GetPyData(triChildAttr[0])
                    if o is not None:
                        if type(o)==TupleType:
                            iId,sTagName,sLang=o
                            n=self.doc.getNodeByIdNum(iId)
                            if n is not None:
                                if sLang is None:
                                    nAttr=self.doc.getChild(n,sTagName)
                                else:
                                    nAttr=self.doc.getChildLang(n,sTagName,sLang)
                                if check is None:
                                    lst.append((triChildAttr[0],nAttr))
                                
                        elif len(self.doc.getAttribute(o,'id'))==0:
                            if check is None:
                                lst.append((triChildAttr[0],o))
                            else:
                                bFound=False
                                for num in check:
                                    if len(self.GetItemText(triChild[0],num))>0:
                                        bFound=True
                                        break
                                if bFound:
                                    lst.append((triChildAttr[0],o))
                    triChildAttr=self.GetNextChild(triChild[0],triChildAttr[1])
            triChild=self.GetNextChild(ti,triChild[1])
        if VERBOSE>0:
            self.__logDebug__('lst:%s'%(self.__logFmt__(lst)))
        return lst
    def __getNodesAttrs__(self,node,lst,check=None):
        lstAttr=self.GetNodeAttrsTup(node,check)
        if len(lstAttr)>0:
            lst.append((node,lstAttr))
        for c in self.doc.getChilds(node):
            if len(self.doc.getAttribute(c,'id'))>0:
                self.__getNodesAttrs__(c,lst,check)
    def GetNodesAttrs(self,node=None,check=None):
        if node is None:
            node=self.rootNode
        if node is None:
            return []
        lst=[]
        for c in self.doc.getChilds(node):
            if len(self.doc.getAttribute(c,'id'))>0:
                self.__getNodesAttrs__(c,lst,check)
        return lst
    def SetValue(self,col,val,node=None):
        if node is None:
            ti=self.triSelected
        else:
            ti=self.FindTreeItem(node)
        self.SetTreeItemValue(ti,col,val)
    def SetTreeItemValue(self,ti,col,val):
        if ti is None:
            return -1
        try:
            self.SetItemText(ti,val,col)
        except:
            traceback.print_exc()
        return 0
    def SetTreeItemValueAll(self,ti,col,val):
        if ti is None:
            ti=self.tiRoot
        if ti is None:
            return
        self.SetItemText(ti,val,col)
        triChild=self.GetFirstChild(ti)
        while triChild[0].IsOk():
            self.SetItemText(triChild[0],val,col)
            self.SetTreeItemValueAll(triChild[0],col,val)
            triChild=self.GetNextChild(ti,triChild[1])
        return 0
    def GetValue(self,col,node=None):
        if node is None:
            ti=self.triSelected
        else:
            ti=self.FindTreeItem(node)
        return self.GetTreeItemValue(ti,col)
    def GetTreeItemValue(self,ti,col):
        if ti is None:
            return ''
        return self.GetItemText(ti,col)
    def OnCompareItems(self,ti1,ti2):
        tid1=self.GetPyData(ti1)
        tid2=self.GetPyData(ti2)
        if self.TREE_DATA_ID:
            if tid1 is not None:
                n1=self.doc.getNodeByIdNum(tid1)
            else:
                n1=None
            if tid2 is not None:
                n2=self.doc.getNodeByIdNum(tid2)
            else:
                n2=None
        else:
            n1=self.GetPyData(ti1)
            n2=self.GetPyData(ti2)
            tid1=n1
            tid2=n2
        l1=self.GetItemText(ti1)
        l2=self.GetItemText(ti2)
        #print l1,l2,cmpTreeItems(l1,l2)
        #print '  ',tid1,tid2
        if tid1 is None and tid2 is None:
            l1=self.GetItemText(ti1)
            l2=self.GetItemText(ti2)
            return cmpTreeItems(l1,l2)
        elif tid1 is None:
            return -1
        elif tid2 is None:
            return 1
        return vtXmlGrpTreeList.OnCompareItems(self,ti1,ti2)
    def OnTcNodeTreeSelChanged(self, event):
        event.Skip()
        try:
            if VERBOSE>0:
                self.__logInfo__(''%())
            if self.iBlockTreeSel>0:
                #self.iBlockTreeSel=self.iBlockTreeSel-1
                return
            tn=event.GetItem()
            self.__selectItem__(tn,bChange=False)
        except:
            self.__logTB__()
