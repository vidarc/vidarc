#----------------------------------------------------------------------------
# Name:         vtXmlDomElementTree.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080201
# CVS-ID:       $Id: vtXmlDomElementTree.py,v 1.1 2008/02/01 12:11:02 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlDom import vtXmlDomBase
from vidarc.tool.xml.vtXmlDomCoreElementTree import vtXmlDomCoreElementTree

class vtXmlDomElementTree(vtXmlDomCoreElementTree,vtXmlDomBase):
    def __init__(self,attr='id',skip=[],synch=False,appl='',verbose=0,audit_trail=True):
        vtXmlDomBase.__init__(self,attr=attr,skip=skip,synch=synch,
                appl=appl,verbose=verbose,audit_trail=audit_trail)
    def __initLib__(self):
        vtXmlDomCoreElementTree.__init__(self)
    def __del__(self):
        vtXmlDomBase.__del__(self)
        vtXmlDomCoreElementTree.__del__(self)
