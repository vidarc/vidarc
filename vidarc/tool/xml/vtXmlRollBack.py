#----------------------------------------------------------------------------
# Name:         vtXmlRollBack.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtXmlRollBack.py,v 1.1 2005/12/11 23:04:49 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import os,time

import vtXmlDom

class vtXmlRollBack:
    def __init__(self,sDir):
        self.sDir=sDir
        self.dTransID={}
    
    def __getTransID__(self):
        pass
        
    def MakeSnapShot(self,doc):
        pass
    
    def StartTransaction(self,name,doc,node,origin='root'):
        pass
    
    def EndTransaction(self,name,doc,node,origin='root'):
        pass
        
    