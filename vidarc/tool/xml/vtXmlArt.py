#----------------------------------------------------------------------------
# Name:         vtArtState.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080313
# CVS-ID:       $Id: vtXmlArt.py,v 1.1 2010/02/21 00:22:06 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.art.vtArt import vtArtProvider,getIcon,addBitmapFromMod

import vidarc.tool.xml.images as images

for sN in addBitmapFromMod(images):
    exec('%s="%s"'%(sN,sN))

class vtXmlArtProvider(vtArtProvider):
    def CreateBitmap(self, artid, client, size):
        try:
            bmp=getattr(images,''.join(['get',artid,'Bitmap']))()
            return bmp
        except:
            pass
        return self.bmpNull
    def CreateImage(self, artid, client, size):
        try:
            bmp=getattr(images,''.join(['get',artid,'Image']))()
            return bmp
        except:
            pass
        return self.bmpNull

__artProv=None
__bInstalled=False

def Install():
    global __bInstalled
    if __bInstalled:
        return
    __bInstalled=True
    global __artProv
    __artProv=vtXmlArtProvider()
def DeInstall():
    global __bInstalled
    if __bInstalled==False:
        return
    __bInstalled=False
    global __stateProv
    __stateProv=None

def getBitmap(artid,client='VIDARC',sz=(16,16)):
    global __bInstalled
    if __bInstalled:
        pass
    else:
        Install()
    global __artProv
    return __artProv.GetBitmap(artid, client, sz)

getIcon=getIcon

def getImage(artid,client='VIDARC',sz=(16,16)):
    global __bInstalled
    if __bInstalled:
        pass
    else:
        Install()
    global __artProv
    return __artProv.GetImage(artid, client, sz)

#Install()
