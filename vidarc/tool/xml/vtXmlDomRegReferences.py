#----------------------------------------------------------------------------
# Name:         vtXmlDomRegReferences.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060518
# CVS-ID:       $Id: vtXmlDomRegReferences.py,v 1.19 2008/12/11 14:48:58 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import threading,thread,traceback
import time
from vidarc.tool.vtThread import vtThread
from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer
from vidarc.tool.xml.vtXmlDomPersistant import vtXmlDomPersistant
import vidarc.tool.log.vtLog as vtLog

class vtXmlDomRegReferences(vtXmlDomConsumer,vtXmlDomPersistant):
    VERBOSE=0
    def __init__(self,doc,attrRef='fid'):
        self.attrRef=attrRef
        self.running=False
        self.keepGoing=True
        self.origin='_'.join([doc.GetOrigin(),'ref'])
        vtXmlDomConsumer.__init__(self)
        vtXmlDomPersistant.__init__(self,'refs')
        self.thdBuild=vtThread(None,origin='_'.join([self.origin,'thd']))
        self.SetDoc(doc)
        self.alias={'':{}}
        #self.Build()
        self.Clear()
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        vtXmlDomConsumer.__del__(self)
        del self.alias
    def GetOrigin(self):
        return self.origin
    def GetName(self):
        return self.GetOrigin()
    def GetParent(self):
        return None
    def SetDoc(self,doc,bNet=False):
        vtLog.vtLngCur(vtLog.DEBUG,'doc is None=%d'%(doc is None),self.GetOrigin())
        vtXmlDomConsumer.SetDoc(self,doc,bNet)
        try:
            if self.doc is not None:
                self.origin='_'.join([self.doc.GetOrigin(),'refs'])
                self.thdBuild.__origin__('_'.join([self.origin,'thd']))
            vtLog.vtLngCur(vtLog.INFO,'',self.origin)
        except:
            #vtLog.CallStack('')
            #traceback.print_exc()
            vtLog.vtLngTB(self.GetOrigin())
    def Pause(self):
        self.thdBuild.Pause()
    def Resume(self):
        self.thdBuild.Resume()
    def Restart(self):
        self.thdBuild.Start()
    def Stop(self):
        #vtLog.vtLngCur(vtLog.INFO,'',self.origin)
        self.thdBuild.Stop()
        #self.keepGoing=False
    def IsBusy(self):
        #vtLog.vtLngCur(vtLog.INFO,'',self.origin)
        bRet=self.thdBuild.IsBusy()
        if bRet:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,self.thdBuild.__str__(),self.origin)
        return self.thdBuild.IsBusy()
        return self.running
    def IsRunning(self):
        return self.thdBuild.IsRunning()
    def Clear(self):
        #vtLog.vtLngCur(vtLog.INFO,'',self.origin)
        del self.alias
        self.alias={'':{}}
    def Build(self):
        #return      # 080120:wro FIXME
        vtLog.vtLngCur(vtLog.INFO,'',self.origin)
        if self.IsBusy():
            return
        self.thdBuild.Do(self.Clear)
        if self.doc is None:
            return
        node=self.doc.getBaseNode()
        vtXmlDomConsumer.SetNode(self,node)
        self.thdBuild.Do(self.__build__,node)
        #vtLog.vtLngCur(vtLog.INFO,'',self.origin)
        #self.keepGoing = self.running = True
        #thread.start_new_thread(self.__build__,())
    def BuildNoThread(self):
        #return      # 080120:wro FIXME
        vtLog.vtLngCur(vtLog.INFO,'start',self.origin)
        zClk=time.clock()
        if self.IsBusy():
            return
        self.Clear()
        if self.doc is None:
            return
        node=self.doc.getBaseNode()
        vtXmlDomConsumer.SetNode(self,node)
        #self.thdBuild.Do(self.__build__,node)
        #self.doc.procChilds(node,self.__walk__)
        self.doc.procChildsKeys(node,self.__walk__,node,None,bThread=False)
        vtLog.vtLngCur(vtLog.INFO,'fin;clock:%s'%(time.clock()-zClk),self.origin)
    def __walkChild__(self,node):
        pass
    def __walk__(self,node,nodePar,id,bRec=True,bThread=True):
        if bThread:
            if self.thdBuild.Is2Stop():
                return -1
        if self.doc.__isSkip__(node):
            return 0
        try:
            #id=self.doc.getKeyNum(node)
            #if id<0:
            #    id=idPar
            #else:
            #    idPar=id
            if id is None:
                id=self.doc.getKeyNum(node)
            if self.VERBOSE:
                vtLog.vtLngCur(vtLog.DEBUG,'id:%s tag:%s'%(self.doc.getKey(node),self.doc.getTagName(node)),self.origin)
            sFid=self.doc.getAttribute(node,self.attrRef)
            if len(sFid)>0:
                i=sFid.find('@')
                if i>0:
                    sName=sFid[i+1:]
                    fid=long(sFid[:i])
                else:
                    sName=''
                    fid=long(sFid)
                if fid>=0:
                    if sName in self.alias:
                        d=self.alias[sName]
                    else:
                        d={}
                        self.alias[sName]=d
                    if fid in d:
                        l=d[fid]
                    else:
                        #l=[]
                        l={}
                        d[fid]=l
                    def getKeyNode(tmp):
                        if tmp is None:
                            return -2
                        id=self.doc.getKey(tmp)
                        if self.doc.IsKeyValid(id)==False:
                            return getKeyNode(self.doc.getParent(tmp))
                        return long(id)
                    #id=getKeyNode(node)
                    
                    if id>=0:
                        if id not in l:
                            #l.append(id)
                            l[id]=1
                        #try:
                        #    l.index(id)
                        #except:
                        #    vtLog.vtLngCur(vtLog.DEBUG,'add ref alias:%s fid:%08d id:%08d'%(sName,fid,id),self.origin)
                        #    l.append(id)
            self.doc.procChildsNoKeys(node,self.__walk__,node,id,bRec=bRec,bThread=bThread)
            if bRec:
                self.doc.procChildsKeys(node,self.__walk__,node,None,bRec=bRec,bThread=bThread)
            #self.doc.procChildsExt(node,self.__walk__,node,id)
        except:
            vtLog.vtLngCur(vtLog.ERROR,'id:%s tag:%s'%(self.doc.getKey(node),
                    self.doc.getTagName(node)),self.origin)
            vtLog.vtLngTB(self.origin)
        return 0
    def __build__(self,node):
        try:
            self.doc.acquire('dom')
            vtLog.vtLngCur(vtLog.INFO,'started',self.origin)
            self.doc.procChilds(node,self.__walk__,node,self.doc.getKey(node))
            if self.thdBuild.Is2Stop():
                vtLog.vtLngCur(vtLog.WARN,'aborted',self.origin)
                self.Clear()
            if self.VERBOSE:
                vtLog.CallStack(self.origin)
                vtLog.pprint(self.alias)
        except:
            self.Clear()
            vtLog.vtLngTB(self.origin)
        self.doc.release('dom')
        vtLog.vtLngCur(vtLog.INFO,'finished',self.origin)
        #self.keepGoing=self.running=False
    def __changeRefId__(self,node,idOld,idNew,sAlias):#*args):
        #vtLog.CallStack('')
        #print args
        #idOld,idNew,sAlias=args[0]
        try:
            #if self.doc.IsNodeKeyValid(node):
            #    return 0
            sFid=self.doc.getAttribute(node,self.attrRef)
            if len(sFid)>0:
                i=sFid.find('@')
                if i>0:
                    sName=sFid[i+1:]
                    fid=long(sFid[:i])
                else:
                    sName=''
                    fid=long(sFid)
                if self.VERBOSE:
                    vtLog.CallStack('')
                    print node
                    print sFid,fid,sName
                    print idOld,idNew,sAlias
                if fid>=0 and sName==sAlias:
                    if fid==idOld:
                        if self.VERBOSE:
                            print 'change'
                        if len(sAlias)>0:
                            sFid='%08d@%s'%(idNew,sAlias)
                        else:
                            sFid='%08d'%idNew
                        self.doc.setAttribute(node,self.attrRef,sFid)
                        if self.VERBOSE:
                            print 'new',node
                        #vtLog.vtLngCur(vtLog.DEBUG,'changed;node:%s'%node,self.origin)
            self.doc.procChildsNoKeys(node,self.__changeRefId__,idOld,idNew,sAlias)
        except:
            vtLog.vtLngTB(self.origin)
        return 0
    def __changeRef__(self,l,idOld,idNew,sAlias):
        if self.VERBOSE:
            vtLog.CallStack('')
            print l,idOld,idNew,sAlias
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'change;idOld:%08d idNew:%08d alias:%s;%s'%(idOld,idNew,sAlias,l),self.origin)
        for id in l.iterkeys():
            try:
                node=self.doc.getNodeByIdNum(id)
                if node is None:
                    vtLog.vtLngCur(vtLog.WARN,'node id:%08d not found'%id,self.origin)
                else:
                    #vtLog.vtLngCur(vtLog.DEBUG,'change;node:%s'%node,self.origin)
                    self.__changeRefId__(node,idOld,idNew,sAlias)
                    #self.doc.procChildsNoKeys(node,self.__changeRefId__,idOld,idNew,sAlias)
                    #vtLog.vtLngCur(vtLog.DEBUG,'changed;node:%s'%node,self.origin)
            except:
                vtLog.vtLngTB(self.origin)
    def __deleteRefId__(self,node,idNum,nodePar):
        fid,sAlias=self.doc.convForeign(self.doc.getAttribute(node,self.attrRef))
        if fid<0:
            return 0
        if sAlias is None:
            sAlias=''
        if sAlias in self.alias:
            d=self.alias[sAlias]
            if fid in d:
                dd=d[fid]
                if idNum in dd:
                    del dd[idNum]
                    if len(dd)==0:
                        del d[fid]
                        if len(d)==0:
                            del self.alias[sAlias]
        self.doc.procChildsNoKeys(node,self.__deleteRefId__,idNum,nodePar)
        return 0
    def deleteId(self,id):
        idNum=long(id)
        node=self.doc.getNodeByIdNum(idNum)
        self.doc.procChildsNoKeys(node,self.__deleteRefId__,idNum,node)
    def clrId(self,id):
        idNum=long(id)
        lDel=[]
        for sAlias,d in self.alias.iteritems():
            for i,dd in d.iteritems():
                if idNum in dd:
                    del dd[idNum]
                    if len(dd)==0:
                        lDel.append((sAlias,d,i))
        for sAlias,d,idNum in lDel:
            del d[idNum]
            if len(d)==0:
                del self.alias[sAlias]
    def addId(self,id,bRec=False):
        idNum=long(id)
        node=self.doc.getNodeByIdNum(idNum)
        self.__walk__(node,None,idNum,bRec=bRec,bThread=False)
    def changeId(self,idOld,idNew,sAlias=''):
        #return      # 080120:wro FIXME
        if self.VERBOSE:
            vtLog.CallStack(self.origin)
            vtLog.pprint(self.alias)
        try:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'aliasType:%s idOld:%s idNew:%s'%(sAlias,idOld,idNew),self.origin)
            if sAlias=='':
                idOld=long(idOld)
                idNew=long(idNew)
                for s,d in self.alias.iteritems():
                    if s==sAlias:
                        if idOld in d:
                            l=d[idOld]
                            self.__changeRef__(l,idOld,idNew,sAlias)
                            d[idNew]=l
                            del d[idOld]
                    for k,dd in d.iteritems():
                        if idOld in dd:
                            del dd[idOld]
                            dd[idNew]=1
            elif self.alias.has_key(sAlias):
                d=self.alias[sAlias]
                if self.VERBOSE:
                    vtLog.CallStack('')
                    vtLog.pprint(d)
                try:
                    l=d[long(idOld)]
                    if self.VERBOSE:
                        vtLog.CallStack('')
                        print l
                    self.__changeRef__(l,long(idOld),long(idNew),sAlias)
                    d[long(idNew)]=l
                    del d[long(idOld)]
                except:
                    pass
            else:
                vtLog.vtLngCur(vtLog.ERROR,'aliasType:%s not found'%sAlias,self.origin)
            if self.VERBOSE:
                print 'after changed'
                vtLog.pprint(self.alias)
        except:
            vtLog.vtLngTB(self.origin)
    def getRefs(self,sAlias,id=None):
        try:
            if sAlias in self.alias:
                d=self.alias[sAlias]
                if id is None:
                    dd={}
                    for id,dVal in d.iteritems():
                        dd[id]=dVal.keys()
                        dd[id].sort()
                    return dd
                else:
                    if long(id) in d:
                        k=d[long(id)].keys()
                        k.sort()
                        return k
                    else:
                        return None
            else:
                return None
        except:
            return None
    def read(self,fPers):
        self.alias={}
        for s in fPers:
            if s[0]==' ':
                l=[long(it) for it in s.split(',')]
                dd={}
                for i in l[1:]:
                    dd[i]=1
                if l[0] in d:
                    raise LookupError('%08d'%l[0])
                d[l[0]]=dd
            else:
                d={}
                self.alias[s.strip()]=d
    def write(self,fPers):
        #self.fPers.write('%s\n'%self.sFP)
        for sAlias,d in self.alias.iteritems():
            fPers.write(sAlias)
            #fPers.write('\n  ')
            #print d
            for id,dVal in d.iteritems():
                #print id,dVal
                fPers.write('\n  %08d,%s'%(id,','.join(['%08d'%k for k in dVal.iterkeys()])))
                #for k in dVal.iteritems():
                #    fPers.write('%08d,'%k)
            fPers.write('\n')
        pass
    def build(self):
        self.BuildNoThread()
