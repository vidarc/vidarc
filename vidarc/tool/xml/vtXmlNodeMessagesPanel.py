#Boa:FramePanel:vtXmlNodeMessagesPanel
#----------------------------------------------------------------------------
# Name:         vtXmlNodeMessagesPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060422
# CVS-ID:       $Id: vtXmlNodeMessagesPanel.py,v 1.8 2008/03/26 23:12:28 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.art.vtThrobber
import vidarc.tool.xml.vtXmlNodeMsgPanel
import vidarc.tool.input.vtInputTextML
import vidarc.tool.input.vtInputTextMultiLineML
import vidarc.tool.time.vtTimeInputDateTime
import wx.lib.buttons

import sys

from vidarc.tool.xml.vtXmlNodePanel import *
from vidarc.tool.xml.vtXmlNodeMsgPanel import *
from vidarc.tool.xml.vtXmlThread import *
from vidarc.tool.time.vtTime import vtDateTime
import vidarc.tool.lang.vtLgBase as vtLgBase

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt

[wxID_VTXMLNODEMESSAGESPANEL, wxID_VTXMLNODEMESSAGESPANELCBUPDATE, 
 wxID_VTXMLNODEMESSAGESPANELLBLMSG, wxID_VTXMLNODEMESSAGESPANELLNMSG, 
 wxID_VTXMLNODEMESSAGESPANELLSTMSG, wxID_VTXMLNODEMESSAGESPANELPNMSG, 
 wxID_VTXMLNODEMESSAGESPANELTGPROCESSED, wxID_VTXMLNODEMESSAGESPANELTGVIEWED, 
 wxID_VTXMLNODEMESSAGESPANELTHRBUILD, 
] = [wx.NewId() for _init_ctrls in range(9)]

class vtXmlNodeMessagesPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_fgsMsg_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblMsg, 0, border=4,
              flag=wx.TOP | wx.LEFT | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.lstMsg, 0, border=4,
              flag=wx.LEFT | wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsShown, 0, border=4, flag=wx.TOP)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.lnMsg, 0, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 0), border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.pnMsg, 0, border=4,
              flag=wx.EXPAND | wx.LEFT | wx.TOP)

    def _init_coll_fgsMsg_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)
        parent.AddGrowableRow(5)
        parent.AddGrowableCol(0)

    def _init_coll_bxsShown_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.tgViewed, 0, border=4, flag=wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.tgProcessed, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 16), border=0, flag=0)
        parent.AddWindow(self.cbUpdate, 0, border=4, flag=wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.thrBuild, 0, border=12, flag=wx.LEFT)

    def _init_coll_lstMsg_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'Prior'), width=50)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'Origin'), width=100)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'Subject'), width=100)
        parent.InsertColumn(col=3, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'Author'), width=80)
        parent.InsertColumn(col=4, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'Time'), width=160)

    def _init_sizers(self):
        # generated method, don't edit
        self.bxsShown = wx.BoxSizer(orient=wx.VERTICAL)

        self.fgsMsg = wx.FlexGridSizer(cols=2, hgap=0, rows=6, vgap=0)

        self._init_coll_bxsShown_Items(self.bxsShown)
        self._init_coll_fgsMsg_Items(self.fgsMsg)
        self._init_coll_fgsMsg_Growables(self.fgsMsg)

        self.SetSizer(self.fgsMsg)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTXMLNODEMESSAGESPANEL,
              name=u'vtXmlNodeMessagesPanel', parent=prnt, pos=wx.Point(579,
              239), size=wx.Size(480, 429), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(472, 402))

        self.lstMsg = wx.ListCtrl(id=wxID_VTXMLNODEMESSAGESPANELLSTMSG,
              name=u'lstMsg', parent=self, pos=wx.Point(4, 21),
              size=wx.Size(429, 157), style=wx.LC_REPORT)
        self.lstMsg.SetMinSize(wx.Size(-1, -1))
        self._init_coll_lstMsg_Columns(self.lstMsg)
        self.lstMsg.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstMsgListColClick,
              id=wxID_VTXMLNODEMESSAGESPANELLSTMSG)
        self.lstMsg.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstMsgListItemDeselected,
              id=wxID_VTXMLNODEMESSAGESPANELLSTMSG)
        self.lstMsg.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstMsgListItemSelected,
              id=wxID_VTXMLNODEMESSAGESPANELLSTMSG)
        self.lstMsg.Bind(wx.EVT_LEFT_DCLICK, self.OnLstMsgLeftDclick)

        self.lblMsg = wx.StaticText(id=wxID_VTXMLNODEMESSAGESPANELLBLMSG,
              label=_(u'Messages'), name=u'lblMsg', parent=self, pos=wx.Point(4,
              4), size=wx.Size(429, 13), style=0)
        self.lblMsg.SetMinSize(wx.Size(-1, -1))

        self.tgViewed = wx.lib.buttons.GenBitmapToggleButton(ID=wxID_VTXMLNODEMESSAGESPANELTGVIEWED,
              bitmap=vtArt.getBitmap(vtArt.Find), name=u'tgViewed', parent=self,
              pos=wx.Point(437, 21), size=wx.Size(31, 30), style=0)
        self.tgViewed.SetToolTipString(_(u'show viewed'))
        self.tgViewed.Bind(wx.EVT_BUTTON, self.OnTgViewedButton,
              id=wxID_VTXMLNODEMESSAGESPANELTGVIEWED)

        self.tgProcessed = wx.lib.buttons.GenBitmapToggleButton(ID=wxID_VTXMLNODEMESSAGESPANELTGPROCESSED,
              bitmap=vtArt.getBitmap(vtArt.Build), name=u'tgProcessed',
              parent=self, pos=wx.Point(437, 55), size=wx.Size(31, 30),
              style=0)
        self.tgProcessed.SetToolTipString(_(u'show processed'))
        self.tgProcessed.Bind(wx.EVT_BUTTON, self.OnTgProcessedButton,
              id=wxID_VTXMLNODEMESSAGESPANELTGPROCESSED)

        self.cbUpdate = wx.lib.buttons.GenBitmapButton(ID=wxID_VTXMLNODEMESSAGESPANELCBUPDATE,
              bitmap=vtArt.getBitmap(vtArt.Synch), name=u'cbUpdate',
              parent=self, pos=wx.Point(437, 101), size=wx.Size(31, 30),
              style=0)
        self.cbUpdate.Bind(wx.EVT_BUTTON, self.OnCbUpdateButton,
              id=wxID_VTXMLNODEMESSAGESPANELCBUPDATE)

        self.lnMsg = wx.StaticLine(id=wxID_VTXMLNODEMESSAGESPANELLNMSG,
              name=u'lnMsg', parent=self, pos=wx.Point(4, 186),
              size=wx.Size(425, 2), style=0)

        self.pnMsg = vidarc.tool.xml.vtXmlNodeMsgPanel.vtXmlNodeMsgPanel(id=wxID_VTXMLNODEMESSAGESPANELPNMSG,
              name=u'pnMsg', parent=self, pos=wx.Point(4, 200),
              size=wx.Size(429, 202), style=0)

        self.thrBuild = vidarc.tool.art.vtThrobber.vtThrobberDelay(id=wxID_VTXMLNODEMESSAGESPANELTHRBUILD,
              name=u'thrBuild', parent=self, pos=wx.Point(445, 139),
              size=wx.Size(15, 15), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.docHum=None
        self.dt=vtDateTime(True)
        
        self.thdAdd=vtXmlThread(self,bPost=True,verbose=self.VERBOSE)
        EVT_VTXML_THREAD_FINISHED(self,self.OnFinished)
        EVT_VTXML_THREAD_ABORTED(self,self.OnAborted)
        
        self.imgLst=wx.ImageList(16,16)
        self.imgLst.Add(vtArt.getBitmap(vtArt.EMail))
        self.imgLst.Add(vtArt.getBitmap(vtArt.Find))
        self.imgLst.Add(vtArt.getBitmap(vtArt.Build))
        self.lstMsg.SetImageList(self.imgLst,wx.IMAGE_LIST_NORMAL)
        self.lstMsg.SetImageList(self.imgLst,wx.IMAGE_LIST_SMALL)
        
        self.iSort=-1
        self.selIdx=-1
        
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
    def IsBusy(self):
        return self.thdAdd.IsRunning()
    def Restart(self):
        self.thdAdd.Start()
    def Stop(self):
        self.thdAdd.Stop()
    def Clear(self):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.Clear(self)
        
        # add code here
        self.iSort=-1
        self.selIdx=-1
        self.iShown=-1
        self.lstMsg.DeleteAllItems()
        self.pnMsg.Clear()
        
        self.ClrBlockDelayed()
    def SetNetDocs(self,d):
        if d.has_key('vHum'):
            dd=d['vHum']
            self.docHum=dd['doc']
        # add code here
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%vtLog.pformat(d),self)
    def SetDoc(self,doc,bNet=False):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if self.doc is not None:
            if bNet:
                EVT_NET_XML_ADD_NODE_DISCONNECT(self.doc,self.OnAddNode)
                EVT_NET_XML_ADD_NODE_RESPONSE_DISCONNECT(self.doc,self.OnAddNode)
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        if doc is not None:
            if bNet:
                EVT_NET_XML_ADD_NODE(doc,self.OnAddNode)
                EVT_NET_XML_ADD_NODE_RESPONSE(doc,self.OnAddNode)
        # add code here
        self.pnMsg.SetDoc(doc,bNet)
    def OnAddNode(self,evt):
        vtXmlNodePanel.OnAddNode(self,evt)
        oLogin=self.doc.GetLogin()
        if oLogin is None:
            vtLog.vtLngCurWX(vtLog.ERROR,'not loggedin',self)
        oMsg=self.doc.GetRegisteredNode('msg')
        self.thrBuild.Start()
        self.thdAdd.Do(long(evt.GetID()),0,False,self.__checkAndAddMsg__,
                            oLogin,oMsg)
    def OnSetNode(self,evt):
        vtXmlNodePanel.OnSetNode(self,evt)
        if self.IsBusy():
            return
        idx=self.lstMsg.FindItemData(-1,long(evt.GetID()))
        if idx>=0:
            oMsg=self.doc.GetRegisteredNode('msg')
            node=self.doc.getNodeById(evt.GetID())
            if node is not None:
                try:
                    imgId=0
                    if oMsg.IsViewed(node):
                        imgId=1
                    if oMsg.IsProcessed(node):
                        imgId=2
                except:
                    pass
                self.lstMsg.SetItemImage(idx,imgId)
    def __showMsg__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            if self.VERBOSE:
                vtLog.CallStack('')
            if self.iShown==-1:
                self.lstMsg.DeleteAllItems()
                self.selIdx=-1
                self.pnMsg.Clear()
            
            oLink=self.doc.GetRegisteredNode('stateMachineLinker')
            lgDict=oLink.GetLangDict()
            oMsg=self.doc.GetRegisteredNode('msg')
            def cmpFunc(a,b):
                iRet=cmp(b[0],a[0])
                if iRet==0:
                    iRet=cmp(oMsg.GetStatusIdx(a[1]),oMsg.GetStatusIdx(b[1]))
                    if iRet==0:
                        return cmp(a[5],b[5])
                    else:
                        return iRet
                else:
                    return iRet
            bViewed=not self.tgViewed.GetValue()
            bProcessed=not self.tgProcessed.GetValue()
            if self.VERBOSE:
                print bViewed,bProcessed
            def add2Lst(tup,pos):
                imgId=0
                if tup[1]=='viewed':
                    if bViewed:
                        return
                    imgId=1
                if tup[1]=='processed':
                    if bProcessed:
                        return
                    imgId=2
                idx=self.lstMsg.InsertImageStringItem(pos,tup[0],imgId)
                self.lstMsg.SetStringItem(idx,1,tup[2])
                self.lstMsg.SetStringItem(idx,2,tup[3])
                self.lstMsg.SetStringItem(idx,3,tup[9])
                self.dt.SetStr(tup[5])
                self.lstMsg.SetStringItem(idx,4,self.dt.GetDateTimeStr(' '))
                self.lstMsg.SetItemData(idx,tup[7])
            if self.iShown==-1:
                self.lMsg.sort(cmpFunc)
                for tup in self.lMsg:
                    add2Lst(tup,sys.maxint)
                self.iShown=len(self.lMsg)
            else:
                iLen=len(self.lMsg)
                while self.iShown<iLen:
                    add2Lst(self.lMsg[self.iShown],0)
                    self.iShown+=1
        except:
            vtLog.vtLngTB(self.GetName())
    def __checkMsg2Add__(self,oLogin,oMsg,c):
        if self.VERBOSE:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%c,self)
        try:
            idRecip=oMsg.GetRecipID(c)
            if idRecip<0:
                return None
            if oLogin.HasId(idRecip):
                l=oMsg.AddValuesToLst(c)
                tag,lHum=oMsg.GetRecipientInfos(c)
                if tag=='group':
                    l+=['grp',_(u'Group')+lHum[0]]
                else:
                    l+=['usr',', '.join(lHum)]
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%l,self)
                if self.VERBOSE:
                    print 'recId',idRecip,l
                return l
        except:
            vtLog.vtLngTB(self.GetName())
        return None
    def __checkAndAddMsg__(self,node,*args,**kwargs):#oLogin,oMsg):
        try:
            if self.VERBOSE:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'args:%s;kwargs:%s'%(args,kwargs),self)
            l=self.__checkMsg2Add__(args[0],args[1],node)#oLogin,oMsg,node)
            if l is not None:
                self.lMsg.append(l)
            if self.thdAdd.Is2Stop():
                return -1
        except:
            vtLog.vtLngTB(self.GetName())
        return 0
    def OnFinished(self,evt):
        evt.Skip()
        self.__showMsg__()
        self.thrBuild.Stop()

    def OnAborted(self,evt):
        evt.Skip()
        self.__showMsg__()
        self.thrBuild.Stop()
        
    def SetNode(self,node):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
                oLogin=self.doc.GetLogin()
                print oLogin
            vtXmlNodePanel.SetNode(self,node)
            oLogin=self.doc.GetLogin()
            self.iShown=-1
            if oLogin is None:
                vtLog.vtLngCurWX(vtLog.ERROR,'not loggedin',self)
                return
            self.lMsg=[]
            oMsg=self.doc.GetRegisteredNode('msg')
            if node is not None:
                id=long(self.doc.getKey(node))
                self.thrBuild.Start()
                self.thdAdd.Do(id,0,True,self.__checkAndAddMsg__,
                            oLogin,oMsg)
        except:
            vtLog.vtLngTB(self.GetName())
        
    def GetNode(self,node=None):
        try:
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            if self.doc is None:
                return
            if node is None:
                if self.node is None:
                    return
                node=self.node
            try:
                # add code here
                pass
            except:
                vtLog.vtLngTB(self.GetName())
            self.doc.AlignNode(node,iRec=2)
            vtXmlNodePanel.GetNode(self,node)
        except:
            vtLog.vtLngTB(self.GetName())
    def Close(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        # add code here
        self.pnMsg.Clear()
        pass
    def Lock(self,flag):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if flag:
            # add code here
            pass
        else:
            # add code here
            pass

    def OnTgViewedButton(self, event):
        self.__showMsg__()
        event.Skip()

    def OnTgProcessedButton(self, event):
        self.__showMsg__()
        event.Skip()

    def OnCbGoToButton(self, event):
        event.Skip()

    def OnLstMsgListColClick(self, event):
        self.selIdx=-1
        event.Skip()

    def OnLstMsgListItemDeselected(self, event):
        self.selIdx=-1
        event.Skip()

    def OnLstMsgListItemSelected(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.pnMsg.Clear()
        self.selIdx=event.GetIndex()
        id=self.lstMsg.GetItemData(self.selIdx)
        tmp=self.doc.getNodeByIdNum(id)
        self.pnMsg.SetNode(tmp)
        event.Skip()

    def OnLstMsgLeftDclick(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.selIdx>=0:
            id=self.lstMsg.GetItemData(self.selIdx)
            tmp=self.doc.getNodeByIdNum(id)
            fid=self.doc.getAttribute(tmp,'fid')
            self.doc.SelectById(fid)
        event.Skip()

    def OnCbUpdateButton(self, event):
        event.Skip()
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.iShown=-1
        self.selIdx=-1
        self.lstMsg.DeleteAllItems()
        self.lMsg=[]
        if self.node is None:
            return
        oLogin=self.doc.GetLogin()
        if oLogin is None:
            vtLog.vtLngCurWX(vtLog.ERROR,'not loggedin',self)
            return
        self.lMsg=[]
        oMsg=self.doc.GetRegisteredNode('msg')
        id=long(self.doc.getKey(self.node))
        self.thrBuild.Start()
        self.thdAdd.Do(id,0,True,self.__checkAndAddMsg__,
                            oLogin,oMsg)
