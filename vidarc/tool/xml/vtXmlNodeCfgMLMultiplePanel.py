#Boa:FramePanel:vtXmlNodeCfgMLMultiplePanel
#----------------------------------------------------------------------------
# Name:         vtXmlNodeCfgMLMultiplePanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060619
# CVS-ID:       $Id: vtXmlNodeCfgMLMultiplePanel.py,v 1.3 2008/03/26 23:12:28 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputAttrCfgMLPanel
import vidarc.tool.input.vtInputMultipledValues
import wx.lib.buttons

import sys

from vidarc.tool.xml.vtXmlNodePanel import *

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_VTXMLNODECFGMLMULTIPLEPANEL, 
 wxID_VTXMLNODECFGMLMULTIPLEPANELLBLMULTIPLE, 
 wxID_VTXMLNODECFGMLMULTIPLEPANELVIATTR, 
 wxID_VTXMLNODECFGMLMULTIPLEPANELVIMULTIPLE, 
] = [wx.NewId() for _init_ctrls in range(4)]

class vtXmlNodeCfgMLMultiplePanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsMultiple, 0, border=4, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.viAttr, 0, border=4, flag=wx.EXPAND)

    def _init_coll_bxsMultiple_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblMultiple, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.viMultiple, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self.bxsMultiple = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsMultiple_Items(self.bxsMultiple)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTXMLNODECFGMLMULTIPLEPANEL,
              name=u'vtXmlNodeCfgMLMultiplePanel', parent=prnt,
              pos=wx.Point(405, 257), size=wx.Size(539, 276),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(531, 249))

        self.lblMultiple = wx.StaticText(id=wxID_VTXMLNODECFGMLMULTIPLEPANELLBLMULTIPLE,
              label=_(u'name'), name=u'lblMultiple', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(177, 24), style=wx.ALIGN_RIGHT)
        self.lblMultiple.SetMinSize(wx.Size(-1, -1))

        self.viMultiple = vidarc.tool.input.vtInputMultipledValues.vtInputMultipledValues(id=wxID_VTXMLNODECFGMLMULTIPLEPANELVIMULTIPLE,
              name=u'viMultiple', parent=self, pos=wx.Point(181, 0),
              size=wx.Size(350, 24), style=0)
        self.viMultiple.Bind(vidarc.tool.input.vtInputMultipledValues.vEVT_VTINPUT_MULTIPLED_VALUES_DELETED,
              self.OnViMultipleVtinputMultipledValuesDeleted,
              id=wxID_VTXMLNODECFGMLMULTIPLEPANELVIMULTIPLE)
        self.viMultiple.Bind(vidarc.tool.input.vtInputMultipledValues.vEVT_VTINPUT_MULTIPLED_VALUES_ADDED,
              self.OnViMultipleVtinputMultipledValuesAdded,
              id=wxID_VTXMLNODECFGMLMULTIPLEPANELVIMULTIPLE)
        self.viMultiple.Bind(vidarc.tool.input.vtInputMultipledValues.vEVT_VTINPUT_MULTIPLED_VALUES_SELECTED,
              self.OnViMultipleVtinputMultipledValuesSelected,
              id=wxID_VTXMLNODECFGMLMULTIPLEPANELVIMULTIPLE)
        self.viMultiple.Bind(vidarc.tool.input.vtInputMultipledValues.vEVT_VTINPUT_MULTIPLED_VALUES_CHANGED,
              self.OnViMultipleVtinputMultipledValuesChanged,
              id=wxID_VTXMLNODECFGMLMULTIPLEPANELVIMULTIPLE)

        self.viAttr = vidarc.tool.input.vtInputAttrCfgMLPanel.vtInputAttrCfgMLPanel(id=wxID_VTXMLNODECFGMLMULTIPLEPANELVIATTR,
              name=u'viAttr', parent=self, pos=wx.Point(0, 32),
              size=wx.Size(531, 217), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.docHum=None
        
        self.viMultiple.SetCB(self.__showMultiple__)
        self.viAttr.SetSuppressNetNotify(True)
        
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
    def __clearMultiple__(self):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
    def __enableMultiple__(self,flag):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.viAttr.Enable(flag)
    def Clear(self):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.Clear(self)
        
        # add code here
        self.viMultiple.Clear()
        self.viAttr.Clear()
        self.ClrBlockDelayed()
    def SetNetDocs(self,d):
        # add code here
        pass
    def SetRegNode(self,obj):
        vtXmlNodePanel.SetRegNode(self,obj)
        self.viAttr.SetRegNode(obj)
    def SetDoc(self,doc,bNet=False):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        
        # add code here
        self.viMultiple.SetDoc(doc)
        self.viAttr.SetDoc(doc,bNet)
    def __showMultiple__(self,node,*args,**kwargs):
        if self.VERBOSE:
            vtLog.CallStack('')
            vtLog.vtLngCurWX(vtLog.DEBUG,'node:%s'%(node),self)
        if node is None:
            self.__clearMultiple__()
            self.__enableMultiple__(False)
        else:
            self.__enableMultiple__(True)
            self.viAttr.SetNode(node)
    def SetNode(self,node):
        try:
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            # add code here
            self.viMultiple.SetNode(node)
        except:
            vtLog.vtLngTB(self.GetName())
        
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
                print node
            if node is None:
                return
            # add code here
            nodeMultiple=self.viMultiple.GetNode(node)
            if nodeMultiple is not None:
                self.viAttr.GetNode(nodeMultiple)
                self.__enableMultiple__(True)
            self.GetNodeFin(node)
        except:
            vtLog.vtLngTB(self.GetName())
    def Close(self):
        # add code here
        self.viMultiple.Close()
    def Lock(self,flag):
        vtXmlNodePanel.Lock(self,flag)
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if flag:
            # add code here
            self.viMultiple.Enable(False)
            self.viAttr.Enable(False)
        else:
            # add code here
            self.viMultiple.Enable(True)
            self.viAttr.Enable(True)

    def OnViMultipleVtinputMultipledValuesDeleted(self, event):
        event.Skip()
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.viAttr.Enable(False)
        #self.viAttr.Clear()

    def OnViMultipleVtinputMultipledValuesAdded(self, event):
        event.Skip()
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.viAttr.Enable(True)
        self.viAttr.Clear()

    def OnViMultipleVtinputMultipledValuesSelected(self, event):
        event.Skip()

    def OnViMultipleVtinputMultipledValuesChanged(self, event):
        event.Skip()
