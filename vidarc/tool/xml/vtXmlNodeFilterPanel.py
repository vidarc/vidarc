#Boa:FramePanel:vtXmlNodeFilterPanel
#----------------------------------------------------------------------------
# Name:         vtXmlNodeFilterPanel.py
# Purpose:      
#               derived from vtXmlFilterPanel.py
# Author:       Walter Obweger
#
# Created:      20060419
# CVS-ID:       $Id: vtXmlNodeFilterPanel.py,v 1.6 2008/03/26 23:12:28 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.xml.vtXmlFilterTypeInput
from vidarc.tool.xml.vtXmlFilterType import *
import wx.lib.buttons

import sys

from vidarc.tool.input.vtInputModifyFeedBack import vtInputModifyFeedBack
from vidarc.tool.xml.vtXmlDomConsumer import *
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_VTXMLNODEFILTERPANEL, wxID_VTXMLNODEFILTERPANELCBADDFLT, 
 wxID_VTXMLNODEFILTERPANELCBDELFLT, wxID_VTXMLNODEFILTERPANELCHCFILTERATTR, 
 wxID_VTXMLNODEFILTERPANELCHCFILTERPOS, 
 wxID_VTXMLNODEFILTERPANELLBLFILTERATTR, 
 wxID_VTXMLNODEFILTERPANELLBLFILTERATTRDESC, 
 wxID_VTXMLNODEFILTERPANELLBLFILTERPOS, wxID_VTXMLNODEFILTERPANELLBLNODENAME, 
 wxID_VTXMLNODEFILTERPANELLSTFILTER, wxID_VTXMLNODEFILTERPANELVIFLTDATE, 
 wxID_VTXMLNODEFILTERPANELVIFLTDATETIME, 
 wxID_VTXMLNODEFILTERPANELVIFLTDATETIMEREL, 
 wxID_VTXMLNODEFILTERPANELVIFLTFLOAT, wxID_VTXMLNODEFILTERPANELVIFLTINT, 
 wxID_VTXMLNODEFILTERPANELVIFLTLONG, wxID_VTXMLNODEFILTERPANELVIFLTSTR, 
 wxID_VTXMLNODEFILTERPANELVIFLTTIME, 
] = [wx.NewId() for _init_ctrls in range(18)]

wxEVT_VTXML_NODE_FILTER_PANEL_CHANGED=wx.NewEventType()
vEVT_VTXML_NODE_FILTER_PANEL_CHANGED=wx.PyEventBinder(wxEVT_VTXML_NODE_FILTER_PANEL_CHANGED,1)
def EVT_VTXML_NODE_FILTER_PANEL_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTXML_NODE_FILTER_PANEL_CHANGED,func)
class vtXmlNodeFilterPanelChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTXML_NODE_FILTER_PANEL_CHANGED(<widget_name>, self.OnInfoChanged)
    """

    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VTXML_NODE_FILTER_PANEL_CHANGED)
        self.obj=obj
    def GetObject(self):
        return self.obj


class vtXmlNodeFilterPanel(wx.Panel,vtInputModifyFeedBack):
    EMPTY_NODE_NAME='---'
    def _init_coll_bxsFilterAttr_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblFilterAttr, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.chcFilterAttr, 2, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.lblFilterAttrDesc, 2, border=4,
              flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsFilterInput_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viFltStr, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltInt, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltLong, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltFloat, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltDate, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltTime, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltDateTime, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltDateTimeRel, 1, border=0, flag=wx.EXPAND)

    def _init_coll_bxsFilterBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbAddFlt, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.cbDelFlt, 0, border=4, flag=wx.TOP | wx.LEFT)

    def _init_coll_bxsFilterPos_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblFilterPos, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.chcFilterPos, 2, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.lblNodeName, 2, border=4,
              flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fgsDom_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(4)
        parent.AddGrowableCol(0)

    def _init_coll_fgsDom_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsFilterPos, 1, border=8,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddSizer(self.bxsFilterAttr, 1, border=8,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsFilterInput, 1, border=4,
              flag=wx.BOTTOM | wx.EXPAND)
        parent.AddSizer(self.bxsFilterAct, 0, border=0, flag=wx.EXPAND)

    def _init_coll_bxsFilterAct_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstFilter, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsFilterBt, 0, border=0, flag=0)

    def _init_coll_lstFilter_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'tagname'), width=-1)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'attribute'), width=-1)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'value'), width=-1)
        parent.InsertColumn(col=3, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'operator'), width=-1)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsDom = wx.FlexGridSizer(cols=1, hgap=0, rows=5, vgap=0)

        self.bxsFilterPos = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsFilterAct = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsFilterBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsFilterInput = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsFilterAttr = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsDom_Items(self.fgsDom)
        self._init_coll_fgsDom_Growables(self.fgsDom)
        self._init_coll_bxsFilterPos_Items(self.bxsFilterPos)
        self._init_coll_bxsFilterAct_Items(self.bxsFilterAct)
        self._init_coll_bxsFilterBt_Items(self.bxsFilterBt)
        self._init_coll_bxsFilterInput_Items(self.bxsFilterInput)
        self._init_coll_bxsFilterAttr_Items(self.bxsFilterAttr)

        self.SetSizer(self.fgsDom)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTXMLNODEFILTERPANEL,
              name=u'vtXmlNodeFilterPanel', parent=prnt, pos=wx.Point(472, 348),
              size=wx.Size(217, 223), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(209, 196))
        self.SetAutoLayout(True)

        self.lblFilterPos = wx.StaticText(id=wxID_VTXMLNODEFILTERPANELLBLFILTERPOS,
              label=_(u'tagname'), name=u'lblFilterPos', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(36, 24), style=wx.ALIGN_RIGHT)

        self.chcFilterPos = wx.Choice(choices=[],
              id=wxID_VTXMLNODEFILTERPANELCHCFILTERPOS, name=u'chcFilterPos',
              parent=self, pos=wx.Point(40, 0), size=wx.Size(76, 21),
              style=wx.CB_SORT)
        self.chcFilterPos.Bind(wx.EVT_CHOICE, self.OnChcFilterPosChoice,
              id=wxID_VTXMLNODEFILTERPANELCHCFILTERPOS)

        self.chcFilterAttr = wx.Choice(choices=[],
              id=wxID_VTXMLNODEFILTERPANELCHCFILTERATTR, name=u'chcFilterAttr',
              parent=self, pos=wx.Point(40, 24), size=wx.Size(76, 21), style=0)
        self.chcFilterAttr.Bind(wx.EVT_CHOICE, self.OnChcFilterAttrChoice,
              id=wxID_VTXMLNODEFILTERPANELCHCFILTERATTR)

        self.viFltStr = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterStringInput(id=wxID_VTXMLNODEFILTERPANELVIFLTSTR,
              name=u'viFltStr', parent=self, pos=wx.Point(0, 53),
              size=wx.Size(26, 26), style=0)
        self.viFltStr.Enable(False)

        self.viFltInt = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterIntegerInput(id=wxID_VTXMLNODEFILTERPANELVIFLTINT,
              name=u'viFltInt', parent=self, pos=wx.Point(26, 53),
              size=wx.Size(26, 26), style=0)
        self.viFltInt.Show(False)

        self.viFltLong = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterLongInput(id=wxID_VTXMLNODEFILTERPANELVIFLTLONG,
              name=u'viFltLong', parent=self, pos=wx.Point(52, 53),
              size=wx.Size(26, 26), style=0)
        self.viFltLong.Show(False)

        self.viFltFloat = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterFloatInput(id=wxID_VTXMLNODEFILTERPANELVIFLTFLOAT,
              name=u'viFltFloat', parent=self, pos=wx.Point(78, 53),
              size=wx.Size(26, 26), style=0)
        self.viFltFloat.Show(False)

        self.viFltDate = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterDateInput(id=wxID_VTXMLNODEFILTERPANELVIFLTDATE,
              name=u'viFltDate', parent=self, pos=wx.Point(104, 53),
              size=wx.Size(26, 26), style=0)
        self.viFltDate.Show(False)

        self.viFltTime = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterTimeInput(id=wxID_VTXMLNODEFILTERPANELVIFLTTIME,
              name=u'viFltTime', parent=self, pos=wx.Point(130, 53),
              size=wx.Size(26, 26), style=0)
        self.viFltTime.Show(False)

        self.viFltDateTime = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterDateTimeInput(id=wxID_VTXMLNODEFILTERPANELVIFLTDATETIME,
              name=u'viFltDateTime', parent=self, pos=wx.Point(156, 53),
              size=wx.Size(26, 26), style=0)
        self.viFltDateTime.Show(False)

        self.viFltDateTimeRel = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterDateTimeRelInput(id=wxID_VTXMLNODEFILTERPANELVIFLTDATETIMEREL,
              name=u'viFltDateTimeRel', parent=self, pos=wx.Point(182, 53),
              size=wx.Size(26, 26), style=0)
        self.viFltDateTimeRel.Show(False)

        self.cbAddFlt = wx.lib.buttons.GenBitmapButton(ID=wxID_VTXMLNODEFILTERPANELCBADDFLT,
              bitmap=vtArt.getBitmap(vtArt.Add), name=u'cbAddFlt', parent=self,
              pos=wx.Point(178, 83), size=wx.Size(31, 30), style=0)
        self.cbAddFlt.Enable(False)
        self.cbAddFlt.Bind(wx.EVT_BUTTON, self.OnCbAddFltButton,
              id=wxID_VTXMLNODEFILTERPANELCBADDFLT)

        self.lstFilter = wx.ListCtrl(id=wxID_VTXMLNODEFILTERPANELLSTFILTER,
              name=u'lstFilter', parent=self, pos=wx.Point(0, 83),
              size=wx.Size(174, 113), style=wx.LC_REPORT)
        self._init_coll_lstFilter_Columns(self.lstFilter)
        self.lstFilter.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstFilterListItemSelected,
              id=wxID_VTXMLNODEFILTERPANELLSTFILTER)
        self.lstFilter.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstFilterListItemDeselected,
              id=wxID_VTXMLNODEFILTERPANELLSTFILTER)
        self.lstFilter.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstFilterListColClick,
              id=wxID_VTXMLNODEFILTERPANELLSTFILTER)

        self.cbDelFlt = wx.lib.buttons.GenBitmapButton(ID=wxID_VTXMLNODEFILTERPANELCBDELFLT,
              bitmap=vtArt.getBitmap(vtArt.Del), name=u'cbDelFlt', parent=self,
              pos=wx.Point(178, 117), size=wx.Size(31, 30), style=0)
        self.cbDelFlt.Enable(False)
        self.cbDelFlt.Bind(wx.EVT_BUTTON, self.OnCbDelFltButton,
              id=wxID_VTXMLNODEFILTERPANELCBDELFLT)

        self.lblNodeName = wx.StaticText(id=wxID_VTXMLNODEFILTERPANELLBLNODENAME,
              label=u'', name=u'lblNodeName', parent=self, pos=wx.Point(124, 0),
              size=wx.Size(76, 24), style=0)

        self.lblFilterAttr = wx.StaticText(id=wxID_VTXMLNODEFILTERPANELLBLFILTERATTR,
              label=_(u'attribute'), name=u'lblFilterAttr', parent=self,
              pos=wx.Point(0, 24), size=wx.Size(36, 21), style=wx.ALIGN_RIGHT)

        self.lblFilterAttrDesc = wx.StaticText(id=wxID_VTXMLNODEFILTERPANELLBLFILTERATTRDESC,
              label=u'', name=u'lblFilterAttrDesc', parent=self,
              pos=wx.Point(124, 24), size=wx.Size(76, 21), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name,verbose=0):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self._init_ctrls(parent)
        
        vtInputModifyFeedBack.__init__(self,[self.lstFilter])
        
        self.dFilter={}
        self.dFilterTranslate={}
        self.filters={}
        self.idxSel=-1
        
        self.SetName(name)
        self.SetSize(size)
        self.Move(pos)
        self.Clear()
        self.fgsDom.Layout()
        self.fgsDom.Fit(self)
        #self.fgsDom.FitInside(self)
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
    def SetEnableMark(self,flag):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        vtInputModifyFeedBack.SetEnableMark(self,flag)
        self.viFltStr.SetEnableMark(flag)
        self.viFltInt.SetEnableMark(flag)
        self.viFltLong.SetEnableMark(flag)
        self.viFltFloat.SetEnableMark(flag)
        self.viFltDate.SetEnableMark(flag)
        self.viFltTime.SetEnableMark(flag)
        self.viFltDateTime.SetEnableMark(flag)
        self.viFltDateTimeRel.SetEnableMark(flag)
    def SetPossibleFilter(self,filter={},trans={}):
        """ on filter shall lock like:
             {'node':[('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
                      ('attr02',vtXmlFilterType.FILTER_TYPE_INT),],
              None:  [('base01',vtXmlFilterType.FILTER_TYPE_DATETIME),
                      ('base02',vtXmlFilterType.FILTER_TYPE_FLOAT),],} 
            on filter translation shall lock like:
             {'node':{ None    : 'translation node',
                      'attr01' : 'translation attr01',
                      'attr02' : 'translation attr02',} , },
              None:  { None    : 'translation for None',
                      'base01' : 'translation base01',
                      'base02' : 'translation base02',} , } 
        """
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.Clear()
        self.dFilter=filter
        self.dFilterTranslate=trans
        self.chcFilterPos.Clear()
        self.chcFilterAttr.Clear()
        try:
            if self.dFilter is None:
                    return
            keys=self.dFilter.keys()
            keys.sort()
            for k in keys:
                if k==None:
                    self.chcFilterPos.Append(self.EMPTY_NODE_NAME)
                else:
                    self.chcFilterPos.Append(k)
            self.chcFilterPos.SetSelection(0)
            self.OnChcFilterPosChoice(None)
        except:
            vtLog.vtLngTB(self.GetName())
    def SetFilters(self,l):
        """filters look like
        {'nodename':[objFlt01,objFlt02],
         None:[objFlt03],}
        """
        try:
            self.Clear()
            self.filters=l
            self.__showFilters__()
        except:
            vtLog.vtLngTB(self.GetName())
    def GetFilters(self):
        """filters look like
        {'nodename':[objFlt01,objFlt02],
         None:[objFlt03],}
        """
        try:
            return self.filters
        except:
            vtLog.vtLngTB(self.GetName())
        return None
    def OnChcFilterPosChoice(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if event is not None:
            event.Skip()
        idx=self.chcFilterPos.GetSelection()
        sName=self.chcFilterPos.GetStringSelection()
        if sName==self.EMPTY_NODE_NAME:
            sName=None
        try:
            sTrans=self.dFilterTranslate[sName][None]
        except:
            sTrans=''
            vtLog.vtLngCurWX(vtLog.ERROR,'translation for nodename:%s not defined'%sName,self)
        self.lblNodeName.SetLabel(sTrans)
        self.chcFilterAttr.Clear()
        lst=self.dFilter[sName]
        if lst is None:
            return
        for tup in lst:
            self.chcFilterAttr.Append(tup[0])
        self.chcFilterAttr.SetSelection(0)
        self.OnChcFilterAttrChoice(None)
        
    def OnChcFilterAttrChoice(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if event is not None:
            event.Skip()
        sTag=self.chcFilterPos.GetStringSelection()
        if sTag==self.EMPTY_NODE_NAME:
            sTag=None
        sName=self.chcFilterAttr.GetStringSelection()
        try:
            sTrans=self.dFilterTranslate[sTag][sName]
        except:
            sTrans=''
            vtLog.vtLngCurWX(vtLog.ERROR,'translation for nodename:%s attribute:%s not defined'%(sTag,sName),self)
        self.lblFilterAttrDesc.SetLabel(sTrans)
        try:
            idx=self.chcFilterAttr.GetSelection()
            sTag=self.chcFilterPos.GetStringSelection()
            if sTag==self.EMPTY_NODE_NAME:
                sTag=None
            tup=self.dFilter[sTag][idx]
            self.viFltStr.Show(False)
            self.viFltDate.Show(False)
            self.viFltTime.Show(False)
            self.viFltDateTime.Show(False)
            self.viFltDateTimeRel.Show(False)
            self.viFltInt.Show(False)
            self.viFltLong.Show(False)
            self.viFltFloat.Show(False)
            self.viFltStr.Enable(False)
            self.viFltDate.Enable(False)
            self.viFltTime.Enable(False)
            self.viFltInt.Enable(False)
            self.viFltLong.Enable(False)
            self.viFltFloat.Enable(False)
            #self.cbAddFlt.Enable(False)
            if tup[1]==vtXmlFilterType.FILTER_TYPE_STRING:
                self.viFltStr.Show(True)
                self.viFltStr.Enable(True)
                self.cbAddFlt.Enable(True)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_DATE:
                self.viFltDate.Show(True)
                self.viFltDate.Enable(True)
                self.cbAddFlt.Enable(True)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_TIME:
                self.viFltTime.Show(True)
                self.viFltTime.Enable(True)
                self.cbAddFlt.Enable(True)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_DATE_TIME:
                self.viFltDateTime.Show(True)
                self.viFltDateTime.Enable(True)
                self.cbAddFlt.Enable(True)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_DATE_TIME_REL:
                self.viFltDateTimeRel.Show(True)
                self.viFltDateTimeRel.Enable(True)
                self.cbAddFlt.Enable(True)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_INT:
                self.viFltInt.Show(True)
                self.viFltInt.Enable(True)
                self.cbAddFlt.Enable(True)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_LONG:
                self.viFltLong.Show(True)
                self.viFltLong.Enable(True)
                self.cbAddFlt.Enable(True)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_FLOAT:
                self.viFltFloat.Show(True)
                self.viFltFloat.Enable(True)
                self.cbAddFlt.Enable(True)
            else:
                self.viFltStr.Show(True)
                self.viFltStr.Enable(False)
                self.cbAddFlt.Enable(True)
        except:
            self.viFltStr.Show(True)
            self.viFltDate.Show(False)
            self.viFltTime.Show(False)
            self.viFltInt.Show(False)
            self.viFltLong.Show(False)
            self.viFltFloat.Show(False)
            self.viFltStr.Enable(False)
            self.viFltDate.Enable(False)
            self.viFltTime.Enable(False)
            self.viFltDateTime.Enable(False)
            self.viFltDateTimeRel.Enable(False)
            self.viFltInt.Enable(False)
            self.viFltLong.Enable(False)
            self.viFltFloat.Enable(False)
            self.cbAddFlt.Enable(False)
            vtLog.vtLngTB(self.GetName())

        #self.fgsDom.Layout()
        #self.fgsDom.FitInside(self)
        self.bxsFilterInput.Layout()
    def Clear(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #self.chcFilterPos.Clear()
        self.filters=[]
        self.lstFilter.DeleteAllItems()
        self.idxSel=-1
        self.cbAddFlt.Enable(False)
        self.cbDelFlt.Enable(False)
        self.SetModified(False)
        
    def OnCbAddFltButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        event.Skip()
        try:
            sTag=self.chcFilterPos.GetStringSelection()
            if sTag==self.EMPTY_NODE_NAME:
                sTag=None
            idx=self.chcFilterAttr.GetSelection()
            sAttr=self.chcFilterAttr.GetStringSelection()
            tup=self.dFilter[sTag][idx]
            if tup[1]==vtXmlFilterType.FILTER_TYPE_STRING:
                sFlt=self.viFltStr.GetValueStr()
                filter=self.viFltStr.GetFilter()
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_DATE:
                sFlt=self.viFltDate.GetValueStr()
                filter=self.viFltDate.GetFilter()
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_TIME:
                sFlt=self.viFltTime.GetValueStr()
                filter=self.viFltTime.GetFilter()
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_DATE_TIME:
                sFlt=self.viFltDateTime.GetValueStr()
                filter=self.viFltDateTime.GetFilter()
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_DATE_TIME_REL:
                sFlt=self.viFltDateTimeRel.GetValueStr()
                filter=self.viFltDateTimeRel.GetFilter()
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_INT:
                sFlt=self.viFltInt.GetValueStr()
                filter=self.viFltInt.GetFilter()
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_LONG:
                sFlt=self.viFltLong.GetValueStr()
                filter=self.viFltLong.GetFilter()
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_FLOAT:
                sFlt=self.viFltFloat.GetValueStr()
                filter=self.viFltFloat.GetFilter()
            self.filters.append((sTag,sAttr,filter))
            self.__showFilters__()
            self.SetModified(True)
        except:
            vtLog.vtLngTB(self.GetName())

    def OnCbDelFltButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        event.Skip()
        if self.idxSel<0:
            return
        try:
            del self.filters[self.idxSel]
            self.SetModified(True)
            self.__showFilters__()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnLstFilterListItemSelected(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.idxSel=event.GetIndex()
        self.cbDelFlt.Enable(True)
        event.Skip()
        
    def __showFilters__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.cbDelFlt.Enable(False)
        self.idxSel=-1
        self.lstFilter.DeleteAllItems()
        try:
            self.filters.sort()
            for sTag,sAttr,flt in self.filters:
                if sTag is None:
                    s=self.EMPTY_NODE_NAME
                else:
                    s=sTag
                idx=self.lstFilter.InsertStringItem(sys.maxint,s)
                self.lstFilter.SetStringItem(idx,1,sAttr)
                self.lstFilter.SetStringItem(idx,2,flt.GetValueStr())
                self.lstFilter.SetStringItem(idx,3,flt.GetOpStr())
        except:
            vtLog.vtLngTB(self.GetName())
    def OnLstFilterListItemDeselected(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        event.Skip()
        self.idxSel=-1
        self.cbDelFlt.Enable(False)

    def OnLstFilterListColClick(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.idxSel=-1
        self.cbDelFlt.Enable(False)
        event.Skip()
