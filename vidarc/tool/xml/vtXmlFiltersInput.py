#Boa:FramePanel:vtXmlFiltersInput
#----------------------------------------------------------------------------
# Name:         vtXmlFiltersInput.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060419
# CVS-ID:       $Id: vtXmlFiltersInput.py,v 1.2 2006/05/30 10:45:28 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.xml.vtXmlFilterTypeInput

[wxID_VTXMLFILTERSINPUT, wxID_VTXMLFILTERSINPUTVIFLTDT, 
 wxID_VTXMLFILTERSINPUTVIFLTDTTM, wxID_VTXMLFILTERSINPUTVIFLTFLOAT, 
 wxID_VTXMLFILTERSINPUTVIFLTINT, wxID_VTXMLFILTERSINPUTVIFLTSTR, 
 wxID_VTXMLFILTERSINPUTVIFLTTM, wxID_VTXMLFILTERSINPUTVTFLTLONG, 
 wxID_VTXMLFILTERSINPUTVIFLTDTTMREL, 
] = [wx.NewId() for _init_ctrls in range(9)]

class vtXmlFiltersInput(wx.Panel):
    def _init_sizers(self):
        # generated method, don't edit
        self.bxsFilter = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_bxsFilter_Items(self.bxsFilter)

        self.SetSizer(self.bxsFilter)


    def _init_coll_bxsFilter_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viFltStr, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltInt, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.vtFltLong, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltFloat, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltDt, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltTm, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltDtTm, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltDtTmRel, 1, border=0, flag=wx.EXPAND)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTXMLFILTERSINPUT,
              name=u'vtXmlFiltersInput', parent=prnt, pos=wx.Point(376, 247),
              size=wx.Size(200, 51), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(192, 24))

        self.viFltStr = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterStringInput(id=wxID_VTXMLFILTERSINPUTVIFLTSTR,
              name=u'viFltStr', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(27, 24), style=0)

        self.viFltInt = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterIntegerInput(id=wxID_VTXMLFILTERSINPUTVIFLTINT,
              name=u'viFltInt', parent=self, pos=wx.Point(27, 0),
              size=wx.Size(27, 24), style=0)

        self.vtFltLong = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterLongInput(id=wxID_VTXMLFILTERSINPUTVTFLTLONG,
              name=u'vtFltLong', parent=self, pos=wx.Point(54, 0),
              size=wx.Size(27, 24), style=0)

        self.viFltFloat = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterFloatInput(id=wxID_VTXMLFILTERSINPUTVIFLTFLOAT,
              name=u'viFltFloat', parent=self, pos=wx.Point(81, 0),
              size=wx.Size(27, 24), style=0)

        self.viFltDt = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterDateInput(id=wxID_VTXMLFILTERSINPUTVIFLTDT,
              name=u'viFltDt', parent=self, pos=wx.Point(108, 0),
              size=wx.Size(27, 24), style=0)

        self.viFltTm = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterTimeInput(id=wxID_VTXMLFILTERSINPUTVIFLTTM,
              name=u'viFltTm', parent=self, pos=wx.Point(135, 0),
              size=wx.Size(27, 24), style=0)

        self.viFltDtTm = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterDateTimeInput(id=wxID_VTXMLFILTERSINPUTVIFLTDTTM,
              name=u'viFltDtTm', parent=self, pos=wx.Point(162, 0),
              size=wx.Size(27, 24), style=0)

        self.viFltDtTmRel = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterDateTimeRelInput(id=wxID_VTXMLFILTERSINPUTVIFLTDTTMREL,
              name=u'viFltDtTmRel', parent=self, pos=wx.Point(189, 0),
              size=wx.Size(27, 24), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
