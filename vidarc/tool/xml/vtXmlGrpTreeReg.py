#----------------------------------------------------------------------------
# Name:         vtXmlGrpTreeReg.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060517
# CVS-ID:       $Id: vtXmlGrpTreeReg.py,v 1.34 2015/02/27 18:51:05 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import wx

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

from vidarc.tool.xml.vtXmlTree import vtXmlTreeThreadAddElements
from vidarc.tool.xml.vtXmlTree import vtXmlTreeThread
from vidarc.tool.xml.vtXmlGrpTree import *
#from vidarc.tool.xml.vtXmlGrpTree import vtXmlTreeRefreshThread
#from vidarc.tool.xml.vtXmlGrpTree import vtXmlGrpTree
import vidarc.tool.lang.vtLgBase as vtLgBase
import types
import thread,threading,time,Queue

class vtXmlTreeRegThread(vtXmlTreeThread):
    def __addElement__(self,trWid,ti,doc,node,lang):
        tagName,oReg,infos=doc.GetInfos4Tree(node,lang)
        tn=self.CallBackWX(trWid.__addElementByInfos__,
                                ti,node,tagName,oReg,infos,True)
        return tn
    def __refreshElement__(self,trWid,doc,id,node,lang):
        tagName,oReg,infos=doc.GetInfos4Tree(node,lang)
        ti=self.CallBackWX(trWid.__refreshLabelByInfos__,
                                    id,node,tagName,oReg,infos)
        return ti

class vtXmlGrpTreeReg(vtXmlGrpTree):
    VERBOSE=0
    def __init__(self, parent=None, id=None, pos=None, size=None, style=0, name='trReg',
                    master=False,controller=False,verbose=0):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        vtXmlGrpTree.__init__(self,id=id, name=name,master=master,
            controller=controller,verbose=verbose,
            parent=parent, pos=pos, size=size,style=style)
        self.thdProc=vtXmlTreeRegThread(self,bPost=True,verbose=verbose-1)
        #self.SetBuildOnDemand(False)
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        try:
            pass
        except:
            #vtLog.vtLngTB(self.__class__.__name__)
            pass
        vtXmlGrpTree.__del__(self)
    def SetBuildOnDemand(self,flag):
        #vtLog.CallStack('')
        #print flag
        self.TREE_BUILD_ON_DEMAND=False # FIXME 060912 wro
    def SetDftGrouping(self):
        self.grouping={}
        self.label=[]
        self.bGrouping=False
    def SetDftNodeInfos(self):
        self.nodeInfos=[]
        self.nodeKey='|id'
    def SetDoc(self,doc,bNet=False,bClrNameGrpDef=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
        vtXmlGrpTree.SetDoc(self,doc,bNet)
        if self.doc is None:
            return
        dReg=self.doc.GetRegisteredNodes()
        if type(self.label)!=types.DictionaryType:
            self.label={None:[]}
        if type(self.grouping)!=types.DictionaryType:
            self.grouping={}
            self.bGroupingDict=True
            self.bGrpMenu=True
            self.bGrouping=True
        if type(self.grpMenu)!=types.DictionaryType:
            self.grpMenu={}
        if bClrNameGrpDef:
            self.label={None:[]}
            self.grouping={}
            self.grpMenu=[]
        keys=dReg.keys()
        for k in keys:
            try:
                fmt=dReg[k].GetLabel4Tree()
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;fmt:%s'%(k,vtLog.pformat(fmt)),self)
                if fmt is not None:
                    self.label[k]=fmt
            except:
                vtLog.vtLngCurWX(vtLog.ERROR,'tag:%s;fmt:%s'%(k,vtLog.pformat(fmt)),self)
                vtLog.vtLngTB(self.GetName())
            try:
                grp=dReg[k].GetGrouping2Tree()
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;grp:%s'%(k,vtLog.pformat(grp)),self)
                if grp is not None:
                    self.grouping[k]=grp
            except:
                vtLog.vtLngCurWX(vtLog.ERROR,'tag:%s;grp:%s'%(k,vtLog.pformat(grp)),self)
                vtLog.vtLngTB(self.GetName())
            try:
                grpMenu=dReg[k].GRP_MENU_4_TREE
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;grpMenu:%s'%(k,vtLog.pformat(grpMenu)),self)
                if grpMenu is not None:
                    self.grpMenu[k]=grpMenu
            except:
                vtLog.vtLngCurWX(vtLog.ERROR,'tag:%s;grpMenu:%s'%(k,vtLog.pformat(grpMenu)),self)
                vtLog.vtLngTB(self.GetName())
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'lbl:%s;grp:%s;mnu:%s'%\
                    (vtLog.pformat(self.label),vtLog.pformat(self.grouping),vtLog.pformat(self.grpMenu)),self)
    def __createMenuGrpIds__(self):
        if not hasattr(self,'popupGrpId'):
            self.popupGrpId=wx.NewId()
            self.popupGrpIds={}
            self.popupViewIntIds={}
            self.popupViewIds={}
            try:
                for k,grpMenu in self.grpMenu.items():
                    for tup in grpMenu:
                        sGrpId,sGrpName=tup[0],tup[1]
                        id=wx.NewId()
                        sKeyGrpId=':'.join([k,sGrpId])
                        self.popupGrpIds[sKeyGrpId]=id
                        wx.EVT_MENU(self,self.popupGrpIds[sKeyGrpId],self.OnTreeGroup)
                self.__logDebug__(self.lViewInt)
                self.__createMenuExternalIds__(self.lViewInt,self.popupViewIntIds)
                self.__createMenuExternalIds__(self.lView,self.popupViewIds)
            except:
                pass
    def AddMenuGroup(self,menu,obj,iPos=-1):
        try:
            self.__createMenuGrpIds__()
            if menu is None:
                menu=wx.Menu()
                iPos=0
            if iPos<0:
                iPos=menu.GetMenuItemCount()
            self.__logDebug__(self.lViewInt)
            iPosInt=self.__createMenuExternal__(obj,menu,self.lViewInt,
                    self.popupViewIntIds,self.OnTreeViewInt)
            if iPosInt>0:
                menu.AppendSeparator()
                iPos+=iPosInt
                iPos+=1
            iPosExt=self.__createMenuExternal__(obj,menu,self.lView,
                    self.popupViewIds,self.OnTreeView)
            if iPosExt>0:
                menu.AppendSeparator()
                iPos+=iPosExt
                iPos+=1
            menuGrp=wx.Menu()
            for tup in self.grpMenu:
                sGrpId,sGrpName=tup[0],tup[1]
                mnIt=wx.MenuItem(menu,self.popupGrpIds[sGrpId],sGrpName)
                wx.EVT_MENU(obj,self.popupGrpIds[sGrpId],self.OnTreeGroup)
                if sGrpId=='normal':
                    mnIt.SetBitmap(images_treemenu.getNoGrpBitmap())
                else:
                    try:
                        mnIt.SetBitmap(self.imgLstTyp.GetBitmap(self.imgDict['grp'][sGrpId][0]))
                    except:
                        mnIt.SetBitmap(images_treemenu.getGrpBitmap())
                menuGrp.AppendItem(mnIt)
            mnIt=wx.MenuItem(menu,self.popupGrpId,_(u'Grouping'),subMenu=menuGrp)
            mnIt.SetBitmap(self.GetMenuGroupBmp())
            self.lMenuGroupIndication.append(menu)
            menu.InsertItem(iPos,mnIt)
            iPos+=1
        except:
            #vtLog.vtLngTB(self.GetName())
            menuGrp=None
        return menuGrp,iPos
    def __addMenuGroup__(self,menu):
        if self.bGrpMenu:
            node=self.GetSelected()
            if node is None:
                return False
            oReg=self.doc.GetRegByNode(node)
            if oReg is None:
                return False
            sTag=oReg.GetTagName()
            if sTag not in self.grpMenu:
                return False
            menuGrp=wx.Menu()
            for tup in self.grpMenu[sTag]:
                sGrpId,sGrpName=tup[0],tup[1]
                sTagGrpId=':'.join([sTag,sGrpId])
                mnIt=wx.MenuItem(menu,self.popupGrpIds[sTagGrpId],sGrpName)
                #f=getattr(images_lang, 'get%sBitmap' % lang)
                #mnIt.SetBitmap(images_treemenu.getGrpBitmap())
                if sGrpId=='normal':
                    mnIt.SetBitmap(images_treemenu.getNoGrpBitmap())
                else:
                    try:
                        mnIt.SetBitmap(self.imgLstTyp.GetBitmap(self.imgDict['grp'][sTagGrpId][0]))
                    except:
                        mnIt.SetBitmap(images_treemenu.getGrpBitmap())
                menuGrp.AppendItem(mnIt)
            self.popupIdGrp=wx.NewId()
            mnIt=wx.MenuItem(menu,self.popupIdGrp,_(u'Grouping'),subMenu=menuGrp)
            #f=getattr(images_lang, 'get%sBitmap' % self.languages[self.languageIds.index(self.lang)])
            mnIt.SetBitmap(images_treemenu.getGrpBitmap())
            menu.AppendItem(mnIt)
            menu.AppendSeparator()
            return True
        return False
    def OnTreeGroup(self,evt):
        eId=evt.GetId()
        sTagGrp=None
        for k in self.popupGrpIds.keys():
            if self.popupGrpIds[k]==eId:
                sTagGrp=k
                break
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCurWX(vtLog.INFO,'sTagGrp:%s'%(sTagGrp),self)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'popupGrp:%s'%
                    (vtLog.pformat(self.popupGrpIds)),self)
        if sTagGrp is not None:
            try:
                i=sTagGrp.find(':')
                sTag=sTagGrp[:i]
                sGrp=sTagGrp[i+1:]
                oReg=self.doc.GetReg(sTag)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'lbl:%s;grp:%s'%\
                            (vtLog.pformat(self.label),vtLog.pformat(self.grouping)),self)
                if oReg is not None:
                    if oReg.SetGroupingByName(sGrp):
                        sTag=oReg.GetTagName()
                        self.label[sTag]=oReg.FMT_GET_4_TREE
                        self.grouping[sTag]=oReg.GRP_GET_4_TREE
                        #vtLog.vtLngCurWX(vtLog.DEBUG,'lbl:%s;grp:%s'%\
                        #            (vtLog.pformat(self.label),vtLog.pformat(self.grouping)),self)
                        self.SetNode(self.rootNode)
                #if self.SetGroupingByName(sGrp):
                #    self.SetNode(self.rootNode)
            except:
                vtLog.vtLngTB(self.GetName())
        try:
            for mn in self.lMenuGroupIndication:
                mnIt=mn.FindItemById(self.popupIdGrp)
                if mnIt is not None:
                    mnIt.SetBitmap(self.GetMenuGroupBmp())
        except:
            vtLog.vtLngTB(self.GetName())
        evt.Skip()
        
    def __createMenuCmdIds__(self):
        if not hasattr(self,'popupIdCmd'):
            #vtLog.CallStack('')
            vtXmlGrpTree.__createMenuCmdIds__(self)
            if self.doc is not None:
                dDocCmds=self.doc.GetCmdDict()
                #vtLog.pprint(dDocCmds)
                if '__regNodeCmds__' in dDocCmds:
                    dCmdsReg=dDocCmds['__regNodeCmds__']
                    keys=dCmdsReg.keys()
                    keys.sort()
                    for tagName in keys:
                        dCmds=dCmdsReg[tagName]
                        if len(dCmds)>0:
                            lCmds=[]
                            for k,v in dCmds.items():
                                try:
                                    if (v[1] & 0xF00)!=0:
                                        lCmds.append(((v[1] & 0xF00) >> 8,':'.join([tagName,k])))
                                except:
                                    pass
                            if len(lCmds)>0:
                                for it in lCmds:
                                    #tagNameKey=':'.join([tagName,])
                                    self.popupCmdIds[it[1]]=wx.NewId()
                                    wx.EVT_MENU(self,self.popupCmdIds[it[1]],self.OnTreeCmd)
    def __addMenuCmds__(self,menu):
        if self.doc is not None:
                #vtLog.CallStack('')
                self.__createMenuCmdIds__()
                dCmds=self.doc.GetCmdDict()
                if len(dCmds)>0:
                    lCmds=[]
                    for k,v in dCmds.items():
                        try:
                            if (v[1] & 0xF00)!=0:
                                lCmds.append(((v[1] & 0xF00) >> 8,k))
                        except:
                            pass
                    if len(lCmds)>0:
                        lCmds.sort()
                        menuCmds=wx.Menu()
                        mnIt=wx.MenuItem(menuCmds,self.popupIdCmd,_(u'Commands'))
                        menuCmds.AppendItem(mnIt)
                        menuCmds.AppendSeparator()
                        for it in lCmds:
                            k=it[1]
                            v=dCmds[k]
                            mnIt=wx.MenuItem(menuCmds,self.popupCmdIds[k],v[0])
                            menuCmds.AppendItem(mnIt)
                    else:
                        menuCmds=None
                    node=self.GetSelected()
                    if (node is not None) and ('__regNodeCmds__' in dCmds):
                        tagName=self.doc.getTagName(node)
                        lCmds=[]
                        d=dCmds['__regNodeCmds__']
                        #vtLog.pprint(d)
                        if tagName in d:
                            dCmds=d[tagName]
                            for k,v in dCmds.items():
                                try:
                                    if (v[1] & 0xF00)!=0:
                                        lCmds.append(((v[1] & 0xF00) >> 8,k))
                                except:
                                    pass
                            if len(lCmds)>0:
                                lCmds.sort()
                                if menuCmds is None:
                                    menuCmds=wx.Menu()
                                    mnIt=wx.MenuItem(menuCmds,self.popupIdCmd,_(u'Commands'))
                                    menuCmds.AppendItem(mnIt)
                                menuCmds.AppendSeparator()
                                for it in lCmds:
                                    k=':'.join([tagName,it[1]])
                                    v=dCmds[it[1]]
                                    mnIt=wx.MenuItem(menuCmds,self.popupCmdIds[k],v[0])
                                    menuCmds.AppendItem(mnIt)
                    mnIt=wx.MenuItem(menu,self.popupIdCmd,_(u'Commands'),subMenu=menuCmds)
                    #wx.EVT_MENU(obj,self.popupIdCmd,self.OnTreeCmd)
                    mnIt.SetBitmap(vtArt.getBitmap(vtArt.Build))
                    menu.AppendItem(mnIt)
                    return True
        return False
    def OnTreeCmd(self,evt):
        try:
            eId=evt.GetId()
            cmd=None
            k=''
            if eId!=self.popupIdCmd:
                for k,id in self.popupCmdIds.items():
                    if id==eId:
                        cmd=k
                        break
            if self.dlgCmd is None:
                self.dlgCmd=vtXmlDomCmdDialog(self)
                self.dlgCmd.Centre()
                self.dlgCmd.SetDoc(self.doc,True)
            dCmd=self.doc.GetCmdDict()
            if k.find(':')>0:
                strs=k.split(':')
                tagName=strs[0]
                cmd=strs[1]
                dCmd=dCmd['__regNodeCmds__'][tagName]
                #vtLog.pprint(dCmd)
            else:
                tagName=None
            self.dlgCmd.Clear()
            self.dlgCmd.SetCmdDict(dCmd,cmd,tagName)
            self.dlgCmd.SetNode(self.GetSelected())
            #self.dlgCmd.SelectCmd(cmd)
            self.dlgCmd.ShowModal()
        except:
            vtLog.vtLngTB(self.GetName())
    def __createRoot__(self,node,blocking=False):
        self.tiRoot=None
        #self.rootNode=None
        self.__Clear__()
        self.objTreeItemSel=None
        self.triSelected=None
        self.DeleteAllItems()
        if self.tiCache is not None:
            self.tiCache.Clear()
            self.tiCache.Build(self)
        vtLog.vtLngCurWX(vtLog.INFO,'',self)
        tn=None
        if self.doc is not None:
            if self.doc.acquire(blocking=blocking):
                try:
                    if self.doc.hasNodeInfos2Get()==False:          # 070920:wro already set?
                        self.doc.setNodeInfos2Get(self.nodeInfos)
                except:
                    pass
                try:
                    tagName,oReg,infos=self.doc.GetInfos4Tree(node,self.lang)
                    tn=self.__addRootByInfos__(node,tagName,oReg,infos)
                    #self.tiRoot=tn
                    #self.rootNode=node
                except:
                    vtLog.vtLngTB(self.GetName())
                #self.rootNode=node
                self.doc.release()
            else:
                vtLog.vtLngCurWX(vtLog.ERROR,'acquire fault;infos:%s'%(vtLog.pformat(self.doc.dUsrInfos)),self)
        else:
            vtLog.vtLngCurWX(vtLog.ERROR,'doc not set',self)
        return tn
    def __addRootByInfos__(self,node,tagName,objReg,infos):
        if self.VERBOSE:
            vtLog.CallStack('')
            print node,objReg,infos
        if node is None or objReg is None:
            return vtXmlGrpTree.__addRootByInfos__(self,node,tagName,infos)
        vtLog.vtLngCurWX(vtLog.DEBUG,tagName,self)
        tid=None
        try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'id:%s;tag:%s;infos:%s'%\
                                (self.doc.getKey(node),tagName,vtLog.pformat(infos)),self)
                #if self.doc.IsNodeKeyValid(node):
                tid=wx.TreeItemData()
                if self.TREE_DATA_ID:
                    tid.SetData(self.doc.getKeyNum(node))
                else:
                    tid.SetData(node)
        except:
            vtLog.vtLngTB(self.GetName())
            pass
        try:
            r=self.AddRoot(objReg.GetDescription(),data=tid)
        except:
            r=self.AddRoot(_(u'root'),data=tid)
        imgTup=self.__getImagesByInfos__(tagName,infos)
        self.SetItemImage(r,imgTup[0],wx.TreeItemIcon_Normal)
        self.SetItemImage(r,imgTup[1],wx.TreeItemIcon_Expanded)
        if tid is not None:
            if self.tiCache is not None:
                self.tiCache.AddID(r,self.doc.getKeyNum(node))
        self.tiRoot=r
        self.rootNode=node
        self.idRootNode=self.doc.getKeyNum(node)
        return r
    def __addGroupingByInfos__(self,parent,o,tagName,objReg,infos):
        if self.VERBOSE:
            vtLog.CallStack('')
            print o,tagName,objReg,infos
        bDbg=False
        if VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                bDbg=True
        if bDbg:
            vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;oReg:%s;infos:%s;node:%s'%(tagName,objReg,vtLog.pformat(infos),o),self)
        #return parent
        tnparent=parent
        if self.bGrouping:
            bFound=False
            if self.VERBOSE:
                print self.grouping
            if bDbg:
                vtLog.vtLngCurWX(vtLog.DEBUG,'grouping:%s'%(vtLog.pformat(self.grouping)),self)
            if self.bGroupingDict:
                if self.grouping.has_key(tagName):
                    bFound=True
                    grouping=self.grouping[tagName]
            else:
                bFound=True
                grouping=self.grouping
            if bFound:
                if self.TREE_DATA_ID:
                    parId=self.GetPyData(parent)
                    parNode=self.doc.getNodeByIdNum(parId)
                else:
                    parNode=self.GetPyData(parent)
                    parId=self.doc.getKeyNum(parNode)
                try:
                    grpItem=self.groupItems[parId]
                except:
                    grpItem={}
                    self.groupItems[parId]=grpItem
                tnparent=parent
                if self.VERBOSE:
                    print grouping
                if bDbg:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'grp:%s'%(vtLog.pformat(grouping)),self)
                for g in grouping:
                    if self.VERBOSE:
                        print g
                    if bDbg:
                        vtLog.vtLngCurWX(vtLog.DEBUG,'g:%s'%(vtLog.pformat(g)),self)
                    if len(g[0])==0:
                        val=tagName
                    else:
                        val=infos[g[0]]
                    val=self.__getValFormat__(g,val,infos)
                    if val is None:
                        continue
                    imgTuple=self.__getImgFormat__(g,val)
                    try:
                        act=grpItem[val]
                        grpItem=act[1]
                        tnparent=act[0]
                    except:
                        tid=wx.TreeItemData()
                        tid.SetData(None)
                        
                        tn=self.AppendItem(tnparent,val,-1,-1,tid)
                        f=self.GetItemFont(tn)
                        f.SetWeight(wx.FONTWEIGHT_LIGHT)
                        f.SetStyle(wx.FONTSTYLE_SLANT)
                        self.SetItemFont(tn,f)
                        try:
                            self.SetItemImage(tn,imgTuple[0],wx.TreeItemIcon_Normal)
                            self.SetItemImage(tn,imgTuple[1],wx.TreeItemIcon_Selected)
                        except:
                            pass
                        act=(tn,{})
                        grpItem[val]=act
                        grpItem=act[1]
                        tnparent=tn
            else:
                tnparent=parent
        else:
            tnparent=parent
        return tnparent
    def __addElementByInfos__(self,parent,o,tagName,objReg,infos,bRet=False):
        bDbg=False
        if VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                bDbg=True
        if bDbg:
                vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;key:%s'%(tagName,self.doc.getKey(o)),self)
        if self.VERBOSE:
            vtLog.CallStack('')
            #print o,tagName,objReg,infos
            #print self.label
        if bDbg:
            vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;oReg:%s;infos:%s;lbl:%s;node:%s'%(tagName,objReg,vtLog.pformat(infos),vtLog.pformat(self.label),o),self)
        id=self.doc.getKey(o)
        tup=self.__findNodeByID__(None,id)
        if tup is not None:
            return tup[0]
        try:
            if self.__is2add__(o,tagName)==False:
                if bDbg:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;key:%s is not to add'%(tagName,self.doc.getKey(o)),self)
                if bRet:
                    return parent
                else:
                    return
            if self.__is2skip__(o,tagName):
                if bDbg:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;key:%s is to skip'%(tagName,self.doc.getKey(o)),self)
                if bRet:
                    return parent
                else:
                    return
            imgTup=self.__getImagesByInfos__(tagName,infos)
            tnparent=self.__addGroupingByInfos__(parent,o,tagName,objReg,infos)
        except:
            vtLog.vtLngTB(self.GetName())
            tnparent=parent
        sLabel=''
        if type(self.label)==types.DictType:
            try:
                label=self.label[tagName]
            except:
                try:
                    label=self.label[None]
                except:
                    vtLog.vtLngTB(self.GetName())
        else:
            label=self.label
        if self.VERBOSE:
            vtLog.CallStack('')
            print self.label
            print tagName
            print label
        if bDbg:
            vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;label:%s'%(tagName,repr(label)),self)
        if type(label) in [types.FunctionType,types.MethodType]:
            sLabel=label(parent=parent,node=o,tagName=tagName,infos=infos)
        else:
            #for v in label:
            #    val=infos[v[0]]
            #    val=self.__getValFormat__(v,val)
            #    sLabel=sLabel+val+' '
            sLabel=u' '.join([self.__getValFormat__(v,infos[v[0]]) 
                            for v in label if v[0] in infos]+[''])
        if self.VERBOSE:
            #vtLog.CallStack('')
            print sLabel
        if bDbg:
            vtLog.vtLngCurWX(vtLog.DEBUG,'sLabel:%s'%(sLabel),self)
        # check project entry type
        tid=wx.TreeItemData()
        if self.TREE_DATA_ID:
            tid.SetData(self.doc.getKeyNum(o))
        else:
            tid.SetData(o)
        tn=self.AppendItem(tnparent,sLabel,-1,-1,tid)
        if self.tiCache is not None:
            self.tiCache.AddID(tn,self.doc.getKeyNum(o))
        iOfs=0
        if self.isNodeInstance(o,infos):
            iOfs=2
            self.SetItemBold(tn)
        if self.isNodeInvalid(o,infos):
            iOfs=4
        try:
            self.SetItemImage(tn,imgTup[iOfs+0],wx.TreeItemIcon_Normal)
            self.SetItemImage(tn,imgTup[iOfs+1],wx.TreeItemIcon_Selected)
        except:
            vtLog.vtLngTB(self.GetName())
        # add grouping sub nodes here
        self.tiLast=tn
        if bDbg:
            vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;key:%s added'%(tagName,self.doc.getKey(o)),self)
        if bRet==True:
            return tn
    def __refreshLabelByInfos__(self,id,node,tagName,objReg,infos):
        bDbg=False
        if VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                bDbg=True
        tup=self.__findNodeByID__(None,id)
        if tup is None:
            return None
        ti=tup[0]
        if bDbg:
            #vtLog.CallStack('')
            #print tagName,objReg,infos
            vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;oReg:%s;infos:%s'%(tagName,objReg,vtLog.pformat(infos)),self)
        if self.bGrouping:
            bFound=False
            if self.bGroupingDict:
                if self.grouping.has_key(tagName):
                    if bDbg:
                        vtLog.vtLngCurWX(vtLog.DEBUG,'grouping:%s'%(vtLog.pformat(self.grouping)),self)
                    bFound=True
                    grouping=self.grouping[tagName]
            else:
                if bDbg:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'grouping:%s'%(vtLog.pformat(self.grouping)),self)
                bFound=True
                grouping=self.grouping
            if bFound:
                if bDbg:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'groupItems:%s'%
                            (vtLog.pformat(self.groupItems)),self)
                parNode=self.doc.getParent(node)
                parId=self.doc.getKeyNum(parNode)
                try:
                    grpItem=self.groupItems[parId]
                except:
                    grpItem={}
                    self.groupItems[parId]=grpItem
                if bDbg:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'grpItem:%s;parID:%d'%
                            (vtLog.pformat(grpItem),parId),self)
                #tnparent=self.tiRoot
                parent=ti
                parent=self.GetItemParent(parent)
                for i in range(len(self.grouping)):
                    if self.GetPyData(parent) is not None:
                        break
                    parent=self.GetItemParent(parent)
                tnparent=parent
                for g in grouping:
                    val=infos[g[0]]
                    val=self.__getValFormat__(g,val,infos)
                    if val is None:
                        continue
                    imgTuple=self.__getImgFormat__(g,val)
                    if bDbg:
                        vtLog.vtLngCurWX(vtLog.DEBUG,'grpItem:%s;val:%s'%(vtLog.pformat(grpItem),val),self)
                    try:
                        act=grpItem[val]
                        grpItem=act[1]
                        tnparent=act[0]
                    except:
                        vtLog.vtLngTB(self.GetName())
                        tid=wx.TreeItemData()
                        tid.SetData(None)
                        
                        tn=self.AppendItem(tnparent,val,-1,-1,tid)
                        self.SetItemImage(tn,imgTuple[0],wx.TreeItemIcon_Normal)
                        self.SetItemImage(tn,imgTuple[1],wx.TreeItemIcon_Selected)
                            
                        act=(tn,{})
                        grpItem[val]=act
                        grpItem=act[1]
                        tnparent=tn
            else:
                tnparent=self.GetItemParent(ti)
        else:
            tnparent=self.GetItemParent(ti)
        imgTup=self.__getImagesByInfos__(tagName,infos)
        sLabel=''
        if type(self.label)==types.DictType:
            try:
                label=self.label[tagName]
            except:
                try:
                    label=self.label[None]
                except:
                    vtLog.vtLngTB(self.GetName())
                    return
        else:
            label=self.label
        if type(label) in [types.FunctionType,types.MethodType]:
            sLabel=label(parent=ti,node=node,tagName=tagName,infos=infos)
        else:
            #for v in label:
            #    val=infos[v[0]]
            #    val=self.__getValFormat__(v,val)
            #    sLabel=sLabel+val+' '
            sLabel=u' '.join([self.__getValFormat__(v,infos[v[0]]) 
                            for v in label if v[0] in infos]+[''])
            if bDbg:
                vtLog.vtLngCurWX(vtLog.DEBUG,'label:%s;sLabel:%s'%(label,sLabel),self)
        iOfs=0
        if self.isNodeInstance(node,infos):
            iOfs=2
            self.SetItemBold(tn)
        if self.isNodeInvalid(node,infos):
            iOfs=4
        try:
            img=imgTup[iOfs+0]
            imgSel=imgTup[iOfs+1]
        except:
            vtLog.vtLngTB(self.GetName())
        tnOldParent=self.GetItemParent(ti)
        if tnOldParent==tnparent:
            # check project entry type
            self.SetItemText(ti,sLabel)
            imgOld=self.GetItemImage(ti,wx.TreeItemIcon_Normal)
            if imgOld!=img:
                self.SetItemImage(ti,img,wx.TreeItemIcon_Normal)
            imgOld=self.GetItemImage(ti,wx.TreeItemIcon_Selected)
            if imgOld!=imgSel:
                self.SetItemImage(ti,imgSel,wx.TreeItemIcon_Selected)
            tn=ti
        else:
            self.SetPyData(ti,None)
            self.Delete(ti)
            tid=wx.TreeItemData()
            if self.TREE_DATA_ID:
                tid.SetData(self.doc.getKeyNum(node))
            else:
                tid.SetData(node)
            
            tn=self.AppendItem(tnparent,sLabel,-1,-1,tid)
            if self.tiCache is not None:
                #self.tiCache.AddID(tn,self.doc.getKey(o))
                self.tiCache.AddID(tn,self.doc.getKeyNum(node))
            if self.isNodeInstance(node,infos):
                self.SetItemBold(tn)
            self.SetItemImage(tn,img,wx.TreeItemIcon_Normal)
            self.SetItemImage(tn,imgSel,wx.TreeItemIcon_Selected)
            if self.TREE_BUILD_ON_DEMAND:
                if self.doc.hasNodes(node):
                    self.SetItemHasChildren(tn,True)
        return tn
    def OnTreeConfig(self,evt):
        try:
            oCfg=self.doc.GetRegisteredNode('cfg')
            if oCfg is None:
                return
            node=self.doc.getChild(None,oCfg.GetTagName())
            #vtLog.CallStack('')
            #print node
            if node is None:
                oCfg.DoAdd(self,self.doc.getBaseNode())
            else:
                oCfg.DoEdit(self,self.doc,node)
            self.doc.DoSafe(self.doc.Build)     # 070930:wro lock dom implicit
        except:
            vtLog.vtLngTB(self.GetName())
    def OnTreeMessages(self,evt):
        try:
            oMsgs=self.doc.GetRegisteredNode('messages')
            if oMsgs is None:
                return
            nodeMsgs=self.doc.getChild(self.doc.getBaseNode(),oMsgs.GetTagName())
            if nodeMsgs is None:
                nodeMsgs=oMsgs.Create(self.doc.getBaseNode())
                self.doc.AlignNode(nodeMsgs)
            
            oMsgs.DoEdit(self,self.doc,nodeMsgs,False)
        except:
            vtLog.vtLngTB(self.GetName())
    def __is2add__(self,o,tagName):
        if self.doc is not None:
            oReg=self.doc.GetReg(tagName)
            if oReg is not None:
                if oReg.IsDisplay()==False:
                    return False
        return vtXmlGrpTree.__is2add__(self,o,tagName)
    def __is2skip__(self,o,tagName):
        if self.doc is not None:
            oReg=self.doc.GetReg(tagName)
            if oReg is not None:
                if oReg.IsSkip()==True:
                    return True
        # check node to skip
        def chk(it,o,tagName):
            if type(it)==types.MethodType:
                return it(o,tagName)
            else:
                return it==tagName
            
        if hasattr(self,'lstSkip'):
            for it in self.lstSkip:
                if type(it)==types.ListType:
                    if chk(it[-1],o,tagName):
                        # check previous
                        tmp=o
                        r=range(len(it)-1)
                        r.reverse()
                        bFound=True
                        for i in r:
                            tmp=self.doc.getParent(tmp)
                            if tmp is None:
                                bFound=False
                                break
                            if chk(it[i],tmp,self.doc.getTagName(tmp))==False:
                                bFound=False
                                break
                        if bFound==True:
                            return True
                else:
                    if chk(it,o,tagName):
                        # forget it
                        return True
        return False
    def __is2block__(self,o):
        if self.doc is not None:
            oReg=self.doc.GetRegByNode(o)
            if oReg is not None:
                if oReg.IsBlock()==True:
                    return True
        # check node to skip
        def chk(it,o,tagName):
            if type(it)==types.MethodType:
                return it(o,tagName)
            else:
                return it==tagName
            
        if hasattr(self,'lstBlk'):
            tagName=self.doc.getTagName(o)
            for it in self.lstBlk:
                if type(it)==types.ListType:
                    if chk(it[-1],o,tagName):
                        # check previous
                        tmp=o
                        r=range(len(it)-1)
                        r.reverse()
                        bFound=True
                        for i in r:
                            tmp=self.doc.getParent(tmp)
                            if tmp is None:
                                bFound=False
                                break
                            if chk(it[i],tmp,self.doc.getTagName(tmp))==False:
                                bFound=False
                                break
                        if bFound==True:
                            return True
                else:
                    if chk(it,o,tagName):
                        # forget it
                        return True
        return False
