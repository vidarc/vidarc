#Boa:FramePanel:vtXmlFilterNodeAttrSelector
#----------------------------------------------------------------------------
# Name:         vtXmlFilterNodeAttrSelector.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060418
# CVS-ID:       $Id: vtXmlFilterNodeAttrSelector.py,v 1.3 2006/08/29 10:06:24 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.art.vtThrobber
import vidarc.tool.xml.vtXmlFilterTypeInput
from vidarc.tool.xml.vtXmlFilterType import *
from vidarc.tool.xml.vtXmlFindThread import *
import wx.lib.buttons

import sys

from vidarc.tool.xml.vtXmlDomConsumer import *
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_VTXMLFILTERNODEATTRSELECTOR, 
 wxID_VTXMLFILTERNODEATTRSELECTORCHCFILTERATTR, 
 wxID_VTXMLFILTERNODEATTRSELECTORCHCFILTERPOS, 
 wxID_VTXMLFILTERNODEATTRSELECTORLBLFILTERATTR, 
 wxID_VTXMLFILTERNODEATTRSELECTORLBLFILTERATTRDESC, 
 wxID_VTXMLFILTERNODEATTRSELECTORLBLFILTERPOS, 
 wxID_VTXMLFILTERNODEATTRSELECTORLBLNODENAME, 
] = [wx.NewId() for _init_ctrls in range(7)]

wxEVT_VTXML_FILTER_PANEL_APPLIED=wx.NewEventType()
vEVT_VTXML_FILTER_PANEL_APPLIED=wx.PyEventBinder(wxEVT_VTXML_FILTER_PANEL_APPLIED,1)
def EVT_VTXML_FILTER_PANEL_APPLIED(win,func):
    win.Connect(-1,-1,wxEVT_VTXML_FILTER_PANEL_APPLIED,func)
class vtXmlFilterPanelApplied(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTXML_FILTER_PANEL_APPLIED(<widget_name>, self.OnInfoApplied)
    """

    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VTXML_FILTER_PANEL_APPLIED)
        self.obj=obj
    def GetObject(self):
        return self.obj

wxEVT_VTXML_FILTER_PANEL_CANCELED=wx.NewEventType()
vEVT_VTXML_FILTER_PANEL_CANCELED=wx.PyEventBinder(wxEVT_VTXML_FILTER_PANEL_CANCELED,1)
def EVT_VTXML_FILTER_PANEL_CANCELED(win,func):
    win.Connect(-1,-1,wxEVT_VTXML_FILTER_PANEL_CANCELED,func)
class vtXmlFilterPanelCanceled(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTXML_FILTER_PANEL_CANCELED(<widget_name>, self.OnInfoCanceled)
    """

    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VTXML_FILTER_PANEL_CANCELED)
        self.obj=obj
    def GetObject(self):
        return self.obj

class vtXmlFilterNodeAttrSelector(wx.Panel,vtXmlDomConsumer):
    def _init_coll_bxsFilterPos_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblFilterPos, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.chcFilterPos, 2, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.lblNodeName, 2, border=4,
              flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fgsDom_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(4)
        parent.AddGrowableCol(0)

    def _init_coll_bxsFilterAttr_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblFilterAttr, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.chcFilterAttr, 2, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.lblFilterAttrDesc, 2, border=4,
              flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fgsDom_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsFilterPos, 1, border=8,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddSizer(self.bxsFilterAttr, 1, border=8,
              flag=wx.RIGHT | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsDom = wx.FlexGridSizer(cols=1, hgap=0, rows=9, vgap=0)

        self.bxsFilterPos = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsFilterAttr = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsDom_Items(self.fgsDom)
        self._init_coll_fgsDom_Growables(self.fgsDom)
        self._init_coll_bxsFilterPos_Items(self.bxsFilterPos)
        self._init_coll_bxsFilterAttr_Items(self.bxsFilterAttr)

        self.SetSizer(self.fgsDom)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTXMLFILTERNODEATTRSELECTOR,
              name=u'vtXmlFilterNodeAttrSelector', parent=prnt,
              pos=wx.Point(472, 348), size=wx.Size(303, 78),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(295, 51))

        self.lblFilterPos = wx.StaticText(id=wxID_VTXMLFILTERNODEATTRSELECTORLBLFILTERPOS,
              label=_(u'tagname'), name=u'lblFilterPos', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(53, 24), style=wx.ALIGN_RIGHT)

        self.chcFilterPos = wx.Choice(choices=[],
              id=wxID_VTXMLFILTERNODEATTRSELECTORCHCFILTERPOS,
              name=u'chcFilterPos', parent=self, pos=wx.Point(57, 0),
              size=wx.Size(110, 21), style=wx.CB_SORT)
        self.chcFilterPos.Bind(wx.EVT_CHOICE, self.OnChcFilterPosChoice,
              id=wxID_VTXMLFILTERNODEATTRSELECTORCHCFILTERPOS)

        self.chcFilterAttr = wx.Choice(choices=[],
              id=wxID_VTXMLFILTERNODEATTRSELECTORCHCFILTERATTR,
              name=u'chcFilterAttr', parent=self, pos=wx.Point(57, 24),
              size=wx.Size(110, 21), style=0)
        self.chcFilterAttr.Bind(wx.EVT_CHOICE, self.OnChcFilterAttrChoice,
              id=wxID_VTXMLFILTERNODEATTRSELECTORCHCFILTERATTR)

        self.lblNodeName = wx.StaticText(id=wxID_VTXMLFILTERNODEATTRSELECTORLBLNODENAME,
              label=u'', name=u'lblNodeName', parent=self, pos=wx.Point(175, 0),
              size=wx.Size(110, 24), style=0)

        self.lblFilterAttr = wx.StaticText(id=wxID_VTXMLFILTERNODEATTRSELECTORLBLFILTERATTR,
              label=_(u'attribute'), name=u'lblFilterAttr', parent=self,
              pos=wx.Point(0, 24), size=wx.Size(53, 21), style=wx.ALIGN_RIGHT)

        self.lblFilterAttrDesc = wx.StaticText(id=wxID_VTXMLFILTERNODEATTRSELECTORLBLFILTERATTRDESC,
              label=u'', name=u'lblFilterAttrDesc', parent=self,
              pos=wx.Point(175, 24), size=wx.Size(110, 21), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        vtXmlDomConsumer.__init__(self)
        self._init_ctrls(parent)
        
        self.dFilter={}
        
        self.SetSize(size)
        self.Move(pos)
        self.Clear()
    
    def IsBusy(self):
        return False
    def Stop(self):
        pass

    def OnChcFilterPosChoice(self, event):
        if event is not None:
            event.Skip()
        idx=self.chcFilterPos.GetSelection()
        sName=self.chcFilterPos.GetStringSelection()
        self.lblNodeName.SetLabel(self.doc.GetTranslation(sName,None))
        self.chcFilterAttr.Clear()
        lst=self.dFilter[sName]
        if lst is None:
            return
        for tup in lst:
            self.chcFilterAttr.Append(tup[0])
        self.chcFilterAttr.SetSelection(0)
        self.OnChcFilterAttrChoice(None)
        
    def OnChcFilterAttrChoice(self, event):
        if event is not None:
            event.Skip()
        sTag=self.chcFilterPos.GetStringSelection()
        sName=self.chcFilterAttr.GetStringSelection()
        self.lblFilterAttrDesc.SetLabel(self.doc.GetTranslation(sTag,sName))
        try:
            idx=self.chcFilterAttr.GetSelection()
            tup=self.dFilter[self.chcFilterPos.GetStringSelection()][idx]
            #self.cbAddFlt.Enable(False)
            if tup[1]==vtXmlFilterType.FILTER_TYPE_STRING:
                pass
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_DATE:
                pass
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_TIME:
                pass
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_DATE_TIME:
                pass
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_INT:
                pass
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_LONG:
                pass
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_FLOAT:
                pass
            else:
                pass
            
        except:
                pass

        self.bxsFilterInput.Layout()

    def Clear(self):
        vtXmlDomConsumer.Clear(self)
        
    def SetDoc(self,doc,bNet=False):
        vtXmlDomConsumer.SetDoc(self,doc,bNet)
        self.chcFilterPos.Clear()
        self.filters={}
        self.lstFilter.DeleteAllItems()
        self.idxSel=-1
        self.cbAddFlt.Enable(False)
        self.cbDelFlt.Enable(False)
        try:
            if self.doc is not None:
                self.dFilter=self.doc.GetPossibleFilter()
                if self.dFilter is None:
                    return
                keys=self.dFilter.keys()
                for k in keys:
                    self.chcFilterPos.Append(k)
            self.chcFilterPos.SetSelection(0)
            self.OnChcFilterPosChoice(None)
        except:
            vtLog.vtLngTB(self.GetName())
        
    def SetNode(self,node):
        self.Clear()
        
        
    def GetNode(self,node=None):
        if node is None:
            node=self.node
        if node is None:
            return
