#----------------------------------------------------------------------------
# Name:         vtXmlDomCoreMap.py
# Purpose:      very basic xml-dom object, that provide common interface for
#               various xml-libraries
#
# Author:       Walter Obweger
#
# Created:      20080131
# CVS-ID:       $Id: vtXmlDomCore.py,v 1.4 2012/05/10 19:26:52 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.config.vcCust as vcCust

#sName=vcCust.is2Import('vtXmlDom','lib','ElementTree')
sName=vcCust.is2Import('vtXmlDom','lib','LibXml2')
try:
    if sName=='ElementTree':
        from vidarc.tool.xml.vtXmlDomCoreElementTree import vtXmlDomCoreElementTree as vtXmlDomCore
        #vtXmlDomCore=_lxc
    else:
        from vidarc.tool.xml.vtXmlDomCoreLibXml2 import vtXmlDomCoreLibXml2 as vtXmlDomCore
except:
    import traceback
    traceback.print_exc()
    from vtXmlDomCoreLibXml2 import vtXmlDomCoreLibXml2 as vtXmlDomCore

