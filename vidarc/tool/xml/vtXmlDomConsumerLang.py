#----------------------------------------------------------------------
# Name:         vtXmlDomConsumerLang.py
# Purpose:      XML dom-tree language consumer
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vtXmlDomConsumerLang.py,v 1.5 2008/03/02 13:02:25 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

class vtXmlDomConsumerLang:
    def __init__(self):
        self.docLang=None
    def __del__(self):
        try:
            if self.docLang is not None:
                self.docLang.DelConsumerLang(self)
        except:
            pass
        self.docLang=None
    def ClearDoc(self):
        self.docLang=None
    def ClearLang(self):
        pass
    def Start(self):
        """overload this method
        this method is called to allow all xml-dom processing, it may
        continue until method Stop is called."""
        pass
    def Stop(self):
        """overload this method
        this method is called to stop all xml-dom processing, it has to
        be stopped until method Start is called.
        """
        pass
    def IsBusy(self):
        return False
    def UpdateLang(self):
        pass
    def SetDoc(self,doc):
        if self.docLang is not None:
            self.docLang.DelConsumerLang(self)
        self.docLang=doc
        if self.docLang is not None:
            self.docLang.AddConsumerLang(self,self.UpdateLang)
