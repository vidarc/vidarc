#----------------------------------------------------------------------------
# Name:         vtXmlDomCoreElementTree.py
# Purpose:      very basic xml-dom object, that provide common interface for
#               ElementTree-librariy
#
# Author:       Walter Obweger
#
# Created:      20080131
# CVS-ID:       $Id: vtXmlDomCoreElementTree.py,v 1.20 2008/05/29 01:22:15 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import types,time,sha
import os,os.path,stat,threading
import cStringIO
import codecs
from platform import python_version_tuple
if python_version_tuple() >= ['2','5']:
    import xml.etree.cElementTree as ET
else:
    import cElementTree as ET

import vidarc.tool.InOut.fnUtil as fnUtil
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.time.vtTimeLoadBound as vtLdBd
from vidarc.tool.InOut.vtInOutFileLock import vtInOutFileLock

class vtXmlDomParse(Exception):
    def __init__(self,origin,sFN):
        self.origin=origin
        self.sFN=sFN
    def __str__(self):
        return ''.join(['vtXmlDomParse origin:',repr(self.origin),'fn:',repr(self.sFN)])

class vtXmlDomCoreElementTree:
    def __init__(self):
        self.doc=None
        self.root=None
        self.ids=None
        self.dPar={}
    def __del__(self):
        try:
            self.root=None
            del self.doc
        except:
            pass
    def getLibClassName(self):
        return 'vtXmlDomCoreElementTree'#self.__class__.__name__
    def __NewLib__(self,revision,root):
            try:
                self.root=ET.Element(root)
                self.doc=ET.ElementTree(self.root)
            except:
                vtLog.vtLngTB(self.GetOrigin())
    def __parseFile__(self,sFN,sz=1024):
        try:
            #if self.doc is not None:
            #    del self.doc
            self.doc=None
            self.root=None
            self.dPar={}
            vtLog.vtLngCur(vtLog.INFO,u'fn:%s;started'%(sFN),self.GetOrigin())
            iAct=0
            st=os.stat(sFN)
            iCount=st[stat.ST_SIZE]
            if iCount==0:
                raise IOError
            self.doc=ET.parse(self.sFN)
            vtLog.vtLngCur(vtLog.INFO,u'fn:%s;finished'%(sFN),self.GetOrigin())
            return self.doc
        except:
            vtLog.vtLngTB(self.GetOrigin())
            self.doc=None
            self.dPar={}
            raise vtXmlDomParse(self.GetOrigin(),sFN)
        return self.doc
    def open(self,sFN):
        #acquireLibXml2()
        try:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'read;file:%s'%(sFN),self.GetOrigin())
            if self.USE_FILE_LOCK:
                lckFd=vtInOutFileLock(sFN)
                fd=lckFd.open()
                #vtLog.CallStack('')
                #print fd
                if fd is not None:
                    self.doc=libxml2.readFd(fd,'',None,#0)
                            libxml2.XML_PARSE_NOERROR|
                            libxml2.XML_PARSE_NOWARNING)
                else:
                    if vtLog.vtLngIsLogged(vtLog.INFO):
                        vtLog.vtLngCur(vtLog.INFO,'fn:%s;unable to lock file'%sFN,origin=self.GetOrigin())
                    vtLog.vtLngTB(self.GetOrigin())
                    self.doc=None
                    self.dPar={}
                    #releaseLibXml2()
                    #self.release('dom')
                    #self.oSF.SetState(self.oSF.OPEN_FLT)
                    #self.NotifyOpenFault(sFN,'')
                    return -2
                lckFd.close()
            else:
                #self.doc.doc=libxml2.parseFile(sFN)
                self.__parseFile__(sFN)
                #sFN
        except Exception,list:
            if self.USE_FILE_LOCK:
                lckFd.close()
            #self.doc.New()  ## 070530:wro FIXME:why deactivated?
            vtLog.vtLngTB(self.GetOrigin())
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'fn:%s;parsing fault'%sFN,origin=self.GetOrigin())
            #print 'exception %s on filename:%s'%(list,sFN)
            #releaseLibXml2()
            return -1
            #self.keepGoing = self.running = False
            #self.release('dom')
            return 0
        try:
            if self.doc is None:
                self.root=None
            else:
                self.root=self.doc.getroot()
                #self.root.parent=None
        except:
            vtLog.vtLngTB(self.GetOrigin())
        return 0
    def close(self):
        del self.doc
        self.doc=None
        self.root=None
        self.dPar={}
        if self.ids is not None:
            del self.ids
        self.ids=None
    def save(self,sTmpFN,encoding='ISO-8859-1'):
        """ make sure 'dom'- is acquired and sTmpFN is valid.
        """
        if self.doc is None:
            return 0
        try:
            if self.USE_FILE_LOCK:
                f=open(sTmpFN,'w+')#'a+rw')
                lckFile=vtInOutFileLock(sTmpFN)
                fd=lckFile.lock(f)
                if fd is None:
                    vtLog.vtLngCur(vtLog.ERROR,'fn:%s;unable to lock file'%(sTmpFN),self.GetOrigin())
                    f.close()
                    return -5
                fnUtil.shiftFile(sTmpFN,fd=fd)
                f.seek(0)
                f.truncate()
                self.doc.saveTo(f,encoding)
                lckFile.unlock()
                f.close()
            else:
                fnUtil.shiftFile(sTmpFN)
                self.doc.write(sTmpFN,encoding)
            #self.audit.backup(sTmpFN)
            try:
                if self.LOG_SAVE:
                    if vtLog.vtLngIsLogged(vtLog.INFO):
                        m=os.stat(sTmpFN)
                        inf=open(sTmpFN)
                        try:
                            s=sha.new(inf.read()).hexdigest()
                        except:
                            s=''
                        inf.close()
                        vtLog.vtLngCur(vtLog.INFO,'fn:%s;size:%d;sha:%s'%(sTmpFN,m[stat.ST_SIZE],s),self.GetOrigin())
            except:
                vtLog.vtLngTB(self.GetOrigin())
            return 1
        except:
            vtLog.vtLngTB(self.GetOrigin())
            try:
                lckFile.unlock()
            except:
                pass
            try:
                f.close()
            except:
                pass
        return -1
    def _serializeEnc(self,node,encoding):
        f=cStringIO.StringIO()
        t=ET.ElementTree(node)
        t.write(f,encoding)
        return f.getvalue()
    def _serialize(self,node):
        f=cStringIO.StringIO()
        t=ET.ElementTree(node)
        t.write(f)
    def serializeDocEnc(self,encoding):
        f=cStringIO.StringIO()
        self.doc.write(f,encoding)
        return f.getvalue()
    def getXmlContent(self,node=None):
        if node is None:
            #content=self.doc.c14nMemory()
            content=self._serializeEnc(self.doc.getroot(),
                                self.SERIALIZE_CODEC) # 070309:wro ('ISO-8859-1')
            return content
        else:
            try:
                tn=self.cloneNode(node,True)
                content=self._serializeEnc(node,self.SERIALIZE_CODEC) # 070309:wro ('ISO-8859-1')
            except:
                vtLog.vtLngTB(self.GetOrigin())
            return content
    def __buildAttrXmlDoc__(self,tn,node):
        for c in self.getChildsNoAttr(node,self.attr):
            if self.hasChilds(c):
                tc=self.cloneNode(c,False)
                tn.append(tc)
                #tc.parent=tn
                self.__buildAttrXmlDoc__(tc,c)
            else:
                tn.append(self.cloneNode(c,True))
    def __buildXmlDoc__(self,tn,node,bIncludeNodes):
        for c in self.getChilds(node):
            if c.tag in self.skip:
                continue
            #if c.name[:2]=='__':
            #    continue
            if self.attr not in c.attrib:
                #tn.addChild(self.cloneNode(c,True))
                if self.hasChilds(c):
                    tc=self.cloneNode(c,False)
                    tn.append(tc)
                    #tc.parent=tn
                    self.__buildAttrXmlDoc__(tc,c)
                else:
                    tc=self.cloneNode(c,True)
                    tn.append(tc)
                    #tc.parent=tn
                continue
            if bIncludeNodes:
                tc=self.cloneNode(c,False)
                tn.append(tc)
                #tc.parent=tn
                self.__buildXmlDoc__(tc,c,bIncludeNodes)
    def getNodeXmlContent(self,node=None,bFull=False,bIncludeNodes=False):
        if node is None:
            node=self.getRoot()
        try:
            if bFull:
                tn=self.cloneNode(node,True)
            else:
                tn=self.cloneNode(node,False)
                self.__buildXmlDoc__(tn,node,bIncludeNodes)
            content=self._serializeEnc(tn,self.SERIALIZE_CODEC) # 070309:wro ('ISO-8859-1')
        except:
            vtLog.vtLngTB(self.GetOrigin())
        return content
    def __getFingerPrintStrAttr__(self,node):
        lst=[]
        if node is not None:
            for attrName,attrContent in node.attrib.iteritems():
                if self.__isAttrSkip4FingerPrint__(attrName)==False:
                    lst.append((attrName,attrContent))
        return self.__getFingerPrintStrFromLst__(lst)
    def setNodeXmlContent(self,node,s):
        try:
            try:
                tmp=ET.fromstring(s)
            except:
                vtLog.vtLngCS(vtLog.CRITICAL,'please report to office@vidarc.com;unicode problem;perform decode %s'%s,origin=self.GetOrigin())
                s=s.decode('ISO-8859-1')
                tmp=ET.fromstring(s)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        
        l=[]
        for c in self.getChildsNoAttr(node,self.attr):
            node.remove(c)
            #self.deleteNode(c)
        
        node.attrib=tmp.attrib
        for c in self.getChildsNoAttr(tmp,self.attr):
            self.appendChild(node,c)
        return node
    def setNodeXmlContentFull(self,nodePar,node,s):
        try:
            try:
                tmp=ET.fromstring(s)
            except:
                vtLog.vtLngCS(vtLog.CRITICAL,'please report to office@vidarc.com;unicode problem;perform decode %s'%s,origin=self.GetOrigin())
                s=s.decode('ISO-8859-1')
                tmp=ET.fromstring(s)
        except:
            vtLog.vtLngTB(self.GetOrigin())
            return node
        if nodePar is not None:
            self.__deleteNode__(node,nodePar)
            self.appendChild(nodePar,tmp)
            #nodePar.append(tmp)
            #tmp.parent=nodePar
            self.clearFingerPrintFull(node)
            self.ids.CheckIdDeep(tmp,self)
            # FIXME: process missing???
        return tmp
    def getNode2Add(self,s,bWarn=True):
        tmpDoc=None
        tmp=None
        try:
            tmp=ET.fromstring(s)
        except:
            try:
                vtLog.vtLngCS(vtLog.CRITICAL,'please report to office@vidarc.com;unicode problem;perform decode %s'%s,origin=self.GetOrigin())
                s=s.decode('ISO-8859-1')
                tmp=ET.fromstring(s)
            except:
                vtLog.vtLngTB(self.GetOrigin())
        return None,tmp
    def __freeDocRaw__(self,tmpDoc):
        try:
            pass
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def __freeNodeRaw__(self,tmpNode):
        try:
            pass
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def __addNodeXmlContentExternalLib__(self,node,nodeBase,expAttr,lValidTagNames,lNewID,lOldID):
            idOld=self.getAttribute(node,self.attr)
            self.setAttribute(node,self.attr,'')
            self.setAttribute(node,expAttr,idOld)
            if 1:
                self.appendChild(nodeBase,node)
                #nodeBase.append(node)
                #node.parent=nodeBase
                self.clearFingerPrintFull(node)
                #self.calcFingerPrintFull(node)
                self.ids.CheckIdDeep(node,self)
                self.ids.ProcessMissing(self)
                self.ids.ClearMissing()
            else:
                self.addnodeBase(nodeBase,node)
            idNew=self.getAttribute(node,self.attr)
            lNewID.append(idNew)
            lOldID.append(idOld)
    def addNodeXmlContentExternal(self,node,s,expAttr='eid',lValidTagNames=None):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'%s'%(s),self.GetOrigin())
        try:
            try:
                tmp=ET.fromstring(s)
            except:
                vtLog.vtLngCS(vtLog.CRITICAL,'please report to office@vidarc.com;unicode problem;perform decode %s'%s,origin=self.GetOrigin())
                s=s.decode('ISO-8859-1')
                tmp=ET.fromstring(s)
        except:
            vtLog.vtLngTB(self.GetOrigin())
            return None,None
        lNewID=[]
        lOldID=[]
        if lValidTagNames is None:
            self.__addNodeXmlContentExternalLib__(tmp,node,expAttr,lValidTagNames,lNewID,lOldID)
            if 0:
                idOld=self.getAttribute(tmp,self.attr)
                self.setAttribute(tmp,self.attr,'')
                self.setAttribute(tmp,expAttr,idOld)
                if 1:
                    self.appendChild(node,tmp)
                    #node.append(tmp)
                    #tmp.parent=node
                    self.clearFingerPrintFull(tmp)
                    #self.calcFingerPrintFull(tmp)
                    self.ids.CheckIdDeep(tmp,self)
                    self.ids.ProcessMissing(self)
                    self.ids.ClearMissing()
                else:
                    self.addNode(node,tmp)
                idNew=self.getAttribute(tmp,self.attr)
        else:
            self.procKeysRec(100,tmp,self.__addNodeXmlContentExternalLib__,
                        node,expAttr,lValidTagNames,lNewID,lOldID)
        idNew=lNewID
        idOld=lOldID
        self.__setModified__()
        self.AlignNode(node,iRec=10)
        return idNew,idOld
    def addNodeXmlContent(self,node,s):
        try:
            try:
                tmp=ET.fromstring(s)
            except:
                vtLog.vtLngCS(vtLog.CRITICAL,'please report to office@vidarc.com;unicode problem;perform decode %s'%s,origin=self.GetOrigin())
                s=s.decode('ISO-8859-1')
                tmp=ET.fromstring(s)
        except:
            vtLog.vtLngTB(self.GetOrigin())
            return None
        self.appendChild(node,tmp)
        #node.append(tmp)
        #tmp.parent=node
        self.clearFingerPrintFull(tmp)
        #self.calcFingerPrintFull(tmp)
        self.ids.CheckIdDeep(tmp,self)
        self.ids.ProcessMissing(self)
        self.ids.ClearMissing()
        idNew=self.getAttribute(tmp,self.attr)
        return idNew,tmp
    def setNodeXmlConfig(self,node,s):
        try:
            try:
                tmp=ET.fromstring(s)
            except:
                vtLog.vtLngCS(vtLog.CRITICAL,'please report to office@vidarc.com;unicode problem;perform decode %s'%s,origin=self.GetOrigin())
                s=s.decode('ISO-8859-1')
                tmp=ET.fromstring(s)
        except:
            vtLog.vtLngTB(self.GetOrigin())
            return None
        try:
            for c in self.getChilds(node,'config'):
                self.__deleteNode__(c,node)
            self.appendChild(node,tmp)
            #node.append(tmp)
            #tmp.parent=node
            return tmp
        except:
            return None
    def setNodeXmlSpecial(self,node,sTagName,s):
        try:
            if len(s)==0:
                return None
            try:
                tmp=ET.fromstring(s)
            except:
                vtLog.vtLngCS(vtLog.CRITICAL,'please report to office@vidarc.com;unicode problem;perform decode %s'%s,origin=self.GetOrigin())
                s=s.decode('ISO-8859-1')
                tmp=ET.fromstring(s)
        except:
            vtLog.vtLngTB(self.GetOrigin())
            return None
        try:
            for c in self.getChilds(node,sTagName):
                self.__deleteNode__(c,node)
            self.appendChild(node,tmp)
            #node.append(tmp)
            #tmp.parnet=node
            return tmp
        except:
            return None
    def copyNode(self,node,bFull=False,bIncludeNodes=False):
        s=self.getNodeXmlContent(node,bFull=bFull,bIncludeNodes=bIncludeNodes)
        try:
            tmp=ET.fromstring(s)
        except:
            vtLog.vtLngCS(vtLog.CRITICAL,'please report to office@vidarc.com;unicode problem;perform decode %s'%s,origin=self.GetOrigin())
            s=s.decode('ISO-8859-1')
            tmp=ET.fromstring(s)
        return tmp
    def __parseBuffer__(self,s):
        try:
            try:
                root=ET.fromstring(s)
            except:
                vtLog.vtLngCS(vtLog.CRITICAL,'please report to office@vidarc.com;unicode problem;perform decode %s'%s,origin=self.GetOrigin())
                s=s.decode('ISO-8859-1')
                root=ET.fromstring(s)
            doc=ET.ElementTree(root)
        except:
            vtLog.vtLngTB(self.GetOrigin())
            doc,root=None
        return doc,root
    def parseBuffer(self,s,attr='id'):
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'synch',origin=self.GetOrigin())
        nodes=[]
        
        if 1==0:#self.bSynch:
            tmp=ET.fromstring(s)
            while self.thdOpen.IsRunning():
                #print 'wait to open finished'
                time.sleep(1)
            while self.thdSynch.IsRunning():
                time.sleep(1)
            if self.doc is None:
                fn=self.sFN
                self.New()
                self.sFN=fn
            self.thdSynch.Synch(tmp,nodes)
        else:
            try:
                for tagName in self.nodes2keep:
                    n=self.getChild(self.getRoot(),tagName)
                    if n is not None:
                        nodes.append(self.cloneNode(n,True))
                fn=self.sFN
                self.Close()
                self.attr=attr
                self.sFN=fn
                self.doc,self.root=self.__parseBuffer__(s)
                #try:
                #    try:
                #        self.root=ET.fromstring(s)
                #    except:
                #        vtLog.vtLngCS(vtLog.CRITICAL,'please report to office@vidarc.com;unicode problem;perform decode %s'%s,origin=self.GetOrigin())
                #        s=s.decode('ISO-8859-1')
                #        self.root=ET.fromstring(s)
                #except:
                #    vtLog.vtLngTB(self.GetOrigin())
                #self.dPar={}
                #self.doc=ET.ElementTree(self.root)
                self.iElemCount=-1
                self.iElemIDCount=-1
                self.genIds()
            
                par=self.getRoot()
                for tagName in self.nodes2keep:
                    n=self.getChild(self.getRoot(),tagName)
                    if n is not None:
                        self.__deleteNode__(n,self.getRoot())
                    for n in nodes:
                        self.appendChild(par,n)
            except:
                vtLog.vtLngTB(self.GetOrigin())
        return True
    def __diffChilds__(self,childs,tmpChilds):
        def compFunc(a,b):
            return cmp(a.name,b.name)
        childs.sort(compFunc)
        tmpChilds.sort(compFunc)
        iLen=len(childs)
        if iLen!=len(tmpChilds):
            #print 'len diff:',iLen,len(tmpChilds)
            return True
        for i in range(iLen):
            c=childs[i]
            t=tmpChilds[i]
            if c.tag!=t.tag:
                #print 'tagname:',c.name
                #print '       :',t.name
                return True
            #if len(self.getKey(c))>0:
            #    continue
            if self.getText(c).strip()!=self.getText(t).strip():
                return True
            dcAttr={}
            dtAttr={}
            dcAttr=c.attrib.copy()
            dtAttr=t.attrib.copy()
            keys=dcAttr.keys()
            iAttrLen=len(keys)
            if iAttrLen!=len(dtAttr.keys()):
                #print 'attr len diff:%d %d',iAttrLen,len(dtAttr.keys())
                return True
            for k in keys:
                try:
                    if dcAttr[k]!=dtAttr[k]:
                        #print 'attrname:',k,dcAttr[k]
                        #print '        :',k,dtAttr[k]
                        return True
                except:
                    return True
        for i in range(iLen):
            c=childs[i]
            t=tmpChilds[i]
            #if len(self.getKey(c))>0:
            #    continue
            if self.__diffChilds__(self.getChildsNoAttr(c,self.attr),
                                self.getChildsNoAttr(t,self.attr)):
                #print '    childs differ'
                return True
        return False
    def __synchEdit__(self,node,tmpNode,bCheck=False):
        #vtLog.CallStack('')
        if bCheck:
            if self.ids.IsOfflineEdit(self.getKeyNum(node)):
            #off=self.getAttribute(node,'offline_content')
            #if len(off)>0:
                #self.doc.removeAttribute(c,'offline_content')
                return 2
            childs=self.getChildsNoAttr(node,self.attr)
            tmpChilds=self.getChildsNoAttr(tmpNode,self.attr)
            if self.__diffChilds__(childs,tmpChilds)==False:
                return 0
        else:
            childs=self.getChildsNoAttr(node,self.attr)
            tmpChilds=self.getChildsNoAttr(tmpNode,self.attr)
        #if self.verbose:
        #    vtLog.vtLngCur(vtLog.DEBUG,'local:%s'%node)
        #    vtLog.vtLngCur(vtLog.DEBUG,'remote:%s'%tmpNode,callstack=False)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'local:%s'%node,self.GetOrigin())
        #    vtLog.vtLngCur(vtLog.DEBUG,'remote:%s'%tmpNode,self.GetOrigin())
        self.setTagName(node,self.getTagName(tmpNode))
        iEdited=0
        for c in childs:
            if len(self.getKey(c))>0:
                continue
            self.__deleteNode__(c,node)
        
        lst=[]
        for c in tmpChilds:
            if len(self.getKey(c))>0:
                continue
            #tc=self.cloneNode(c,True)
            #node.addChild(tc)
            node.append(c)
            #c.parent=node
            self.AlignNode(c,iRec=2)
            iEdited=1
        d=tmpNode.attrib.copy()
        for k in ['fp']:
            del d[k]
        node.attrib=d
        self.__setModified__()
        if self.audit.isEnabled():
            self.audit.write('__synchEdit__',self.getNodeXmlContent(node,False,False),
                            origin=self.getAuditOriginByNode(node))
        #self.AlignNode(node,iRec=2)
        return iEdited
    def __genIds__(self,c,node,ids):
        #time.sleep(0.1)
        if c.tag in self.skip:
            return 0
        if self.attr in c.attrib:      # 061019: wro better solution
            ids.CheckId(c,self)
            self.updateParentDict(node,c)
        self.iElemIDCount+=1
        #id=self.getAttribute(c,self.attr)
        #if len(id)>0:
        #    ids.CheckId(c,id)
        #self.__genIds__(c,ids)
        self.procChildsKeys(c,self.__genIds__,c,ids)
        return 0
    def __showNode__(self,node,s):
        print s,"ELEMENT",node.tag
        #elif node.type=='text':
        #    print s,"TEXT_NODE",node.content
        #elif node.type=='cdata':
        #    print s,"CDATA_SECTION_NODE"
        #elif node.nodeType==Element.ATTRIBUTE_NODE:
        #    print s,"ATTRIBUTE_NODE"
        #elif node.type=='comment':
        #    print s,"COMMENT_NODE"
        #elif node.nodeType==Element.DOCUMENT_NODE:
        #    print s,"DOCUMENT_NODE"
        #elif node.nodeType==Element.ENTITY_NODE:
        #    print s,"ENTITY_NODE"
        #elif node.nodeType==Element.NOTATION_NODE:
        #    print s,"NOTATION_NODE"
    def __getNodeLevel__(self,node):
        try:
            l=[self.root]
            self.__getParentBreadthSearch__(self.root,node,l)
            #print l
            l.reverse()
            return len(l)-1
        except:
            return -1
    def getLevel(self,node):
        if node in self.dPar:
            par=node
            iLv=1
            while 1:
                if par in self.dPar:
                    par=self.dPar[par]
                    iLv+=1
                else:
                    break
            return iLv
        else:
            iLv=self.__getNodeLevel__(node)
            if iLv>=0:
                return iLv+2
        return -1
    def getTagName(self,node):
        if node is None:
            return ''
        return node.tag
    def getIdent(self,node):
        if node is None:        # 070201:wro
            return ''
        s=node.tag
        for attrName,attrContent in node.attrib.iteritems():
            s+='|'+attrName+':'+attrContent
        return s
    def setTagName(self,node,name):
        #if node is None:        # 070201:wro
        #    return 
        node.tag=name
    def isElement(self,node):
        return 1
        if node.type=='element':
            return 1
        else:
            return 0
    def isText(self,node):
        return 0
        if node.type=='text':
            return 1
        else:
            return 0
    def isComment(self,node):
        return 0
        if node.type=='comment':
            return 1
        else:
            return 0
    def isCData(self,node):
        return 0
        if node.type=='cdata':
            return 1
        else:
            return 0
    def hasChilds(self,node):
        if node is None:
            return 0
        for tmp in node:
                return 1
        return 0
    def hasNodes(self,node):
        if node is None:
            return 0
        for tmp in node:
                if self.hasKey(tmp):
                    return 1
        return 0
    def validateNode(self,node):
        pass
    def appendChild(self,node,add):
        #vtLog.CallStack('')
        #print 'appendChild',node.type,node.name
        node.append(add)
        self.updateParentDict(node,add)
        self.__setModified__()
    def updateParentDict(self,par,node):
        #add.parent=node
        #vtLog.CallStack('')
        #print node,self.getKey(node),par,self.getKey(par)
        #vtLog.vtLngCur(vtLog.DEBUG,'%s;%s'%(self.getKey(par),self.getKey(node)),self)
        #self.logParentDict()
        if self.attr in node.attrib:
            self.dPar[node]=par
            #idNode=self.getKeyNum(node)
            #idPar=self.getKeyNum(par)
            #if idNode<-1 or idPar<-1:
            #    self.dPar[node]=par
            #else:
            #    self.dPar[id]=idPar
        #self.showParentDict()
    def removeParentDict(self,node):
        #vtLog.CallStack('')
        vtLog.vtLngCur(vtLog.DEBUG,'%s'%(self.getKey(node)),self)
        if node in self.dPar:
            del self.dPar[node]
        #self.showParentDict()
    def showParentDict(self):
        print 'dPar',len(self.dPar)
        for n in self.dPar.iterkeys():
            print '   ',self.getKey(n),n
    def logParentDict(self):
        l=[(self.getKey(par),self.getKey(node)) for node,par in self.dPar.iteritems()]
        l.sort()
        vtLog.vtLngCur(vtLog.DEBUG,'%s'%(vtLog.pformat(l)),self)
    def Align(self,node,iLevel,s,ident,verbose=0,gProcess=None,iAct=0,iSkip=0,iRec=1000):
        if verbose>0:
            vtLog.vtLogCallDepth(None,self.getTagName(node),1,False)
            print 'level:%03d ident:%03d act:%03d skip:%03d rec:%03d'%(iLevel,len(ident),iAct,iSkip,iRec)
            #self.__showNode__(node,s)
        prevNodeType=-1
        sNew=s+ident
        iOldAct=iAct
        if node.tail is None or node.tail.strip()=='':
            node.tail=s
        o=None
        for o in node:
            #if o.tail is None or o.tail.strip()=='':
            #    o.tail=sNew
            if iRec>0:
                self.Align(o,iLevel+1,sNew,ident,verbose=verbose,
                        gProcess=gProcess,iAct=iAct+1,iSkip=iSkip,
                        iRec=iRec-1)
                #if gProcess is not None:
                #    gProcess.SetRange(gProcess.GetRange()+1)
                #if gProcess is not None:
                #    gProcess.SetValue(gProcess.GetValue()+1)
        if o is not None:
            if o.tail is None or o.tail.strip()=='':
                o.tail=s
            if node.text is None or node.text.strip()=='':
                node.text=sNew
        
        self.__setModified__()
    def AlignNode(self,node,gProcess=None,ident='  ',iRec=0):
        #doc=dom.documentElement
        if node is None:
            vtLog.vtLngCur(vtLog.WARN,'node is None'%(),self.GetOrigin())
            return
        iLevel=self.__getNodeLevel__(node)
        s='\n'
        for i in xrange(0,iLevel):
            s=s+ident
        self.Align(node,iLevel+1,s,ident,verbose=0,gProcess=gProcess,iRec=iRec)
        parnode=self.getParent(node)
        if parnode is not None:
            o=parnode
            if node==o.getchildren()[0]:
                if o.text is None or o.text.strip()=='':
                    o.text=s
            s='\n'
            for i in xrange(0,iLevel-1):
                s=s+ident
            if node==o.getchildren()[-1]:
                if node.tail is None or node.tail.strip()=='':
                    node.tail=s
                
            if o.tail is None or o.tail.strip()=='':
                o.tail=s
        self.__setModified__()
        
    def AlignDoc(self,verbose=0,gProcess=None):
        if self.root is None:
            return
        self.Align(self.root,0,'\n','  ',verbose,gProcess)
    def GetNodeCount(self,node=None):
        i=0
        if node is None:
            node=self.root
        if node is None:
            return i
        if node==self.root:
            if node.text is not None:
                i+=1
            if node.tail is not None:
                i+=1
        for tmp in node:
            i+=1
            if tmp.text is not None:
                i+=1
            if tmp.tail is not None:
                i+=1
            i+=self.GetNodeCount(tmp)
        return i
    def __getElementCount__(self,node):
        if node is None:
            return 0
        i=0
        for tmp in node:
            vtLdBd.check()
            i+=1
            i+=self.__getElementCount__(tmp)
        return i
    def __getElementAttrCount__(self,node,attr):
        i=0
        if node is None:
            return 0
        #vtLog.CallStack('')
        for tmp in node:
            vtLdBd.check()
            try:
                if attr in tmp.attrib:
                    i+=1
            except:
                pass
            i+=self.__getElementAttrCount__(tmp,attr)
        return i
    #def getText(self,node):
    #    if node is None:
    #        return None
    #    if node.children.type=='text':
    #        return unicode(node.children.content,self.applEncoding)
    #    return ''
    def getChildAttr(self,node,tagName,attr,attrVal):
        if node is None:
            node=self.getBaseNode()#self.root
        if node is None:
            return None
        for tmp in node:
                if tmp.tag==tagName:
                    for attrName,attrContent in tmp.attrib.iteritems():
                        if attrName==attr:
                            if attrContent==attrVal:
                                return tmp
        return None
    def getChildLang(self,node,tagName,lang):
        if node is None:
            node=self.getBaseNode()#self.root
        if node is None:
            return None
        if lang is None:
            lang=self.lang
            if lang is None:
                vtLog.vtLngCur(vtLog.WARN,'no langaguage specified;no language defined',self.GetOrigin())
                return None
        oFallback=None
        oFirst=None
        for tmp in node:
                if tmp.tag==tagName:
                    for attrName,attrContent in tmp.attrib.iteritems():
                        if attrName=='language':
                            if attrContent==lang:
                                return tmp
                    if oFallback is None:
                        oFallback=tmp
                    if oFirst is None:
                        oFirst=tmp
        if oFallback is not None:
            return oFallback
        return oFirst
    def getChild(self,node,tagName):
        if node is None:
            node=self.getBaseNode()#self.root
        if node is None:
            return None
        for tmp in node:
                if tagName is not None:
                    if tmp.tag==tagName:
                        return tmp
                else:
                    return tmp
        return None
    def getChildForced(self,node,tagName):
        if node is None:
            node=self.getBaseNode()#self.root
        if node is None:
            return None
        n=self.getChild(node,tagName)
        if n is None:
            n=self.createSubNode(node,tagName,True)#False)
            self.__setModified__()
        return n
    def __getChilds__(self,node,tagName=None):
        lst=[]
        if node is None:
            return lst
        for tmp in node:
                bFound=False
                if tagName is not None:
                    if tmp.tag==tagName:
                        bFound=True
                else:
                    bFound=True
                if bFound:
                    lst.append(tmp)
        return lst
    def getChilds(self,node=None,tagName=None):
        if node is None:
            node=self.getBaseNode()#self.root
        return self.__getChilds__(node,tagName)
    
    def getAttrs(self,node,bSkip=True):
        l=[]
        if node is not None:
            for attrName in node.attrib.iterkeys():
                if bSkip==False or self.__isAttrSkip4FingerPrint__(attrName)==False:
                    l.append(attrName)
            l.sort()
        return l
    def getAttrsValues(self,node,bSkip=True):
        l=[]
        if node is not None:
            for attrName,attrContent in node.attrib.iteritems():
                if bSkip==False or self.__isAttrSkip4FingerPrint__(attrName)==False:
                    l.append((attrName,attrContent))
            l.sort()
        return l
    
    def __getChildsAttrVal__(self,node,tagName=None,attr=None,attrVal=None):
        lst=[]
        if node is None:
            return lst
        for tmp in node:
                bFound=False
                if tagName is not None:
                    if tmp.tag==tagName:
                        bFound=True
                else:
                    bFound=True
                if bFound:
                    # check attr
                    if attr is not None:
                        bFound=False
                        for attrName,attrContent in tmp.attrib.iteritems():
                            if attrName==attr:
                                if attrVal is not None:
                                    if attrVal==attrContent:
                                        bFound=True
                                else:
                                    bFound=True
                                break
                    if bFound:
                        lst.append(tmp)
        return lst
    def __getChildsAttr__(self,node,attr):
        lst=[]
        if node is None:
            return lst
        for tmp in node:
                if attr in tmp.attrib:
                    lst.append(tmp)
        return lst
    def __getChildsNoAttr__(self,node,attr):
        lst=[]
        if node is None:
            return lst
        for tmp in node:
            if attr not in tmp.attrib:
                lst.append(tmp)
        return lst
    def procChildsAttr(self,node,attr,func,*args):
        if node is None:
            return -2
        for tmp in node:
            vtLdBd.check()
            if attr in tmp.attrib:
                if func(tmp,args)<0:
                    return -1
        return 0
    def procChildsNoAttr(self,node,attr,func,*args):
        if node is None:
            return -2
        for tmp in node:
            vtLdBd.check()
            if attr not in tmp.attrib:
                if func(tmp,args)<0:
                    return -1
        return 0
    def procChilds(self,node,func,*args):
        if node is None:
            return -2
        for tmp in node:
            vtLdBd.check()
            if func(tmp,args)<0:
                return -1
        return 0
    def procChildsAttrExt(self,node,attr,func,*args,**kwargs):
        if node is None:
            return -2
        for tmp in node:
            vtLdBd.check()
            if attr in tmp.attrib:
                if func(tmp,*args,**kwargs)<0:
                    return -1
        return 0
    def procChildsNoAttrExt(self,node,attr,func,*args,**kwargs):
        if node is None:
            return -2
        for tmp in node:
            vtLdBd.check()
            if attr not in tmp.attrib:
                if func(tmp,*args,**kwargs)<0:
                    return -1
        return 0
    def procChildsExt(self,node,func,*args,**kwargs):
        if node is None:
            return -2
        for tmp in node:
            vtLdBd.check()
            if func(tmp,*args,**kwargs)<0:
                return -1
        return 0
    def procChildsKeys(self,node,func,*args,**kwargs):
        if node is None:
            return -2
        for tmp in node:
            vtLdBd.check()
            if self.attr in tmp.attrib:
                if func(tmp,*args,**kwargs)<0:
                    return -1
        return 0
    def procChildsNoKeys(self,node,func,*args,**kwargs):
        if node is None:
            return -2
        for tmp in node:
            vtLdBd.check()
            if self.attr not in tmp.attrib:
                if func(tmp,*args,**kwargs)<0:
                    return -1
        return 0
        #self.procChildsAttr(node,self.attr,func,*args)
    def procChildsKeysRec(self,lv,node,func,*args,**kwargs):
        if node is None:
            return
        for tmp in node:
            vtLdBd.check()
            if self.attr in tmp.attrib:
                if func(tmp,*args,**kwargs)<0:
                    return -1
                else:
                    if lv>0:
                        if self.procChildsKeysRec(lv-1,tmp,func,*args,**kwargs)<0:
                            return -1
        return 0
    def procKeysRec(self,lv,node,func,*args,**kwargs):
        if node is None:
            return -2
        vtLdBd.check()
        if self.attr in node.attrib:
            if func(node,*args,**kwargs)<0:
                return -1
            else:
                if lv>0:
                    return self.procChildsKeysRec(lv-1,node,func,*args,**kwargs)
        return 0
        return -3
    def __procChildsKeysRecHierReStart__(self,lHierOld,lHierNew,lv,node,func,*args,**kwargs):
        if lHierOld is None:
            bSkip=False
        else:
            bSkip=True
        for tmp in node:
            vtLdBd.check()
            if self.attr in tmp.attrib:
                idNum=self.getKeyNum(tmp)
                if bSkip==True:#lHierOld is not None:
                    if lHierOld[0]==idNum:
                        iLen=len(lHierOld)
                        if iLen>1:
                            #lHierNew.append(idNum)
                            #print 'down',lv,lHierOld,lHierNew
                            iRet=self.__procChildsKeysRecHierReStart__(lHierOld[1:],lHierNew,
                                            lv-1,tmp,func,*args,**kwargs)
                            #print '  up',lv,lHierOld,lHierNew,iRet
                            if iRet<0:
                                lHierNew.append(idNum)
                                return -1
                            bSkip=False
                        else:
                            #for i in xrange(iLen):
                            #    del lHierOld[0]
                            bSkip=False
                else:
                    if func(tmp,*args,**kwargs)<0:
                        lHierNew.append(idNum)
                        #print lHierNew
                        return -1
                    else:
                        if lv>0:
                            iRet=self.__procChildsKeysRecHierReStart__(None,lHierNew,
                                                lv-1,tmp,func,*args,**kwargs)
                            if iRet<0:
                                lHierNew.append(idNum)
                                return -1
                            #lHierNew.remove(idNum)
        return 0
    def procKeysRecHierReStart(self,lHier,lv,node,func,*args,**kwargs):
        if node is None:
            return -2
        vtLdBd.check()
        if self.attr in node.attrib:
            if lHier is None:
                if func(node,*args,**kwargs)<0:
                    return -1,[self.getKeyNum(node)]
            else:
                if len(lHier)==1:
                    if lHier[0]==self.getKeyNum(node):
                        lHier=None
            if lv>0:
                return self.procChildsKeysRecHierReStart(lHier,lv-1,node,func,*args,**kwargs)
        return 0,None
        return -3,None
    def procChildsKeysRecSkip(self,lv,node,func,*args,**kwargs):
        if node is None:
            return
        for tmp in node:
            vtLdBd.check()
            if self.attr in tmp.attrib:
                if self.IsSkip(tmp)==False:
                    if func(tmp,*args,**kwargs)<0:
                        return -1
                    else:
                        if lv>0:
                            if self.procChildsKeysRecSkip(lv-1,tmp,func,*args,**kwargs)<0:
                                return -1
        return 0
    def procKeysRecSkip(self,lv,node,func,*args,**kwargs):
        if node is None:
            return -2
        vtLdBd.check()
        if self.attr in node.attrib:
            if self.IsSkip(node)==False:
                if func(node,*args,**kwargs)<0:
                    return -1
                else:
                    if lv>0:
                        return self.procChildsKeysRecSkip(lv-1,node,func,*args,**kwargs)
        return 0
        return -3
    def procChildsKeysRecHierarchy(self,par,parId,node,func,*args,**kwargs):
        if node is None:
            return
        nodeId=self.getKey(node)
        for tmp in node:
            vtLdBd.check()
            if self.attr in tmp.attrib:
                iRet=func(node,nodeId,tmp,*args,**kwargs)
                if iRet<0:
                    return -1
                elif iRet>0:
                    if self.procChildsKeysRecHierarchy(node,nodeId,tmp,func,*args,**kwargs)<0:
                            return -1
        return 0
    def procAttr(self,node,attr,func,*args):
        if node is None:
            return -2
        vtLdBd.check()
        if attr in node.attrib:
            if func(node,args)<0:
                return -1
            return 0
        return -3
    def procAttrExt(self,node,attr,func,*args,**kwargs):
        if node is None:
            return -2
        vtLdBd.check()
        if attr in node.attrib:
            if func(node,*args,**kwargs)<0:
                return -1
            return 0
        return -3
    def procAttrs(self,node,func,*args):
        if node is None:
            return -2
        for name,val in node.attrib.iteritems():
            if func(node,name,val,args)<0:
                return -1
        return 0
        return -3
    def procAttrsExt(self,node,func,*args,**kwargs):
        if node is None:
            return -2
        for name,val in node.attrib.iteritems():
            if func(node,name,val,*args,**kwargs)<0:
                return -1
        return 0
        return -3
    def findChildsKeys(self,node,hier,func,*args,**kwargs):
        if node is None:
            vtLog.vtLngCur(vtLog.CRITICAL,'possible to FIX',self.appl)
            return 0,hier,None      #061227:wro added
        #vtLog.CallStack('')
        #print hier
        for tmp in node:
            vtLdBd.check()
            if self.attr in tmp.attrib:
                ret=func(tmp,*args,**kwargs)
                if ret!=0:
                    return ret,hier,tmp
                if hier is not None:   # 060806
                    ret,hierRet,tmpRet=self.findChildsKeys(tmp,hier+[tmp],func,*args,**kwargs)
                    if ret!=0:
                        return ret,hierRet,tmpRet
        return 0,hier,None
    def match(self,node,tagName,filter):
        if node is None:
            return False
        i=tagName.find('|')
        if i>0:
            sAttr=tagName[i+1:]
            tagName=tagName[:i]
        else:
            sAttr=None
        #bFound=False
        for tmp in node:
            if tmp.tag==tagName:
                if sAttr is None:
                    val=self.getText(tmp)
                    if filter.match(val):
                        bFound=True
                        return True
                else:
                    for attrName,val in tmp.attrib.iteritems():
                        if attrName==sAttr:
                            if filter.match(val):
                                bFound=True
                                return True
                            break
        return False
    def __getChildsRec__(self,node,tagName,lst):
        if node is None:
            return 
        for tmp in node:
            bFound=False
            if tagName is not None:
                if tmp.tag==tagName:
                    bFound=True
            else:
                bFound=True
            if bFound:
                lst.append(tmp)
            if len(tmp):
                self.__getChildsRec__(tmp,tagName,lst)
    def getText(self,node):
        s=u''
        if node is None:
            return s
        #return node.text
        if node.text is None:
            return u''
        s=node.text
        if type(s)==types.UnicodeType:
            return s
        return node.text.decode(self.applEncoding)
    def hasAttribute(self,node,attr):
        if node is None:
            return -1
        if attr in node.attrib:
            return 1
        if attr is None:
            if len(node.attrib)>0:
                return 1
        return 0
    def getAttribute(self,node,attr):
        if node is None:
            return ''
        if attr in node.attrib:
            return node.attrib[attr]
        return ''
    def getChildAttribute(self,node,tagName,attr):
        if node is None:
            node=self.getBaseNode()#self.root
        if node is None:
            return None
        for tmp in node:
            if tmp.tag==tagName:
                if attr in tmp.attrib:
                    return tmp.attrib[attr]
        return None
    def setChildAttribute(self,node,tagName,attr,attrVal):
        if node is None:
            node=self.getBaseNode()#self.root
        if node is None:
            return None
        for tmp in node:
            if tmp.tag==tagName:
                self.__setModified__()
                if attr in tmp.attrib:
                    tmp.attrib[attr]=attrVal
                    return 0
                else:
                    tmp.attrib[attr]=attrVal
                    if attr==self.attr:
                        self.iElemIDCount+=1
                    return 1
        return None
    def setAttribute(self,node,attr,attrVal):
        if node is None:
            return -1
        self.__setModified__()
        if attr in node.attrib:
            node.attrib[attr]=attrVal
            return 0
        else:
            node.attrib[attr]=attrVal
            if attr==self.attr:
                self.iElemIDCount+=1
            return 1
    def removeAttribute(self,node,attr):
        if node is None:
            return -1
        if attr in node.attrib:
            del node.attrib[attr]
            self.__setModified__()
            if attr==self.attr:
                self.iElemIDCount-=1
        return 0
    def getNodeInfos(self,node,lang=None,name='basic'):
        #self.sem.acquire()
        #print 'getNodeInfos start'
        try:
            dInfos=self.dUsrInfos[name]['infos']
            dRes=self.dUsrInfos[name]['res']
            iLenRes=self.dUsrInfos[name]['len']
            #if self.dUsrInfos[name]['singleLang']:
            #    if lang is None:
            #        lang=self.lang
        except:
            return '',{}
        #dInfos=self.dInfos
        #dRes=self.dRes
        #iLenRes=self.iLenRes
        if lang is None:
            lang=self.lang
        i=0
        for k in dRes.keys():   # mod
            dRes[k]=''
        if node is None:
            #self.sem.release()
            return '',dRes
        def getInfo(node,d,name):
            if d.has_key(''):
                dRes[name]=self.getText(node)
            keys=d.keys()
            try:
                keys.remove('')
            except:
                pass
            if len(keys)>0:
                for attrName,attrVal in node.attrib.iteritems():
                    if attrName in keys:
                        try:
                            idx=keys.index(attrName)
                            dRes[d[keys[idx]]]=attrVal
                        except:
                            pass
                
        if dInfos.has_key(''):
            getInfo(node,dInfos[''],'')
            i+=1
        def getChildsInfos(node,dInfos,lst):
            for tmp in node:
                    try:
                        if tmp.tag in dInfos:
                            d=dInfos[tmp.tag]
                            if type(d.values()[0])==types.DictType:
                                #process child
                                getChildsInfos(tmp,d,lst+[tmp.tag])
                                if i>=self.iLenRes:
                                    #print 'getNodeInfos end'
                                    #self.sem.release()
                                    return node.tag,dRes
                                continue
                        else:
                            continue
                    except:
                        continue
                    bFound=False
                    bFallback=False
                    sCombName=':'.join(lst+[tmp.tag])
                    if lang is None:
                        bFound=True
                    else:
                        if self.getAttribute(tmp,'language')==lang:
                            bFound=True
                        else:
                            if len(dRes[sCombName])<=0:
                                bFound=True
                                bFallback=True
                    if bFound:
                        try:
                            getInfo(tmp,d,sCombName)
                            if bFallback==False:
                                i+=1
                                if i>=iLenRes:
                                    #print 'getNodeInfos end'
                                    #self.sem.release()
                                    return node.tag,dRes
                        except:
                            pass
        getChildsInfos(node,dInfos,[])
        #print 'getNodeInfos end'
        #self.sem.release()
        return node.tag,dRes
    def getNodeInfoVal(self,node,sVal,lang=None):
        if lang is None:
            lang=self.lang
        #self.sem.acquire()
        #print 'getNodeInfos start'
        #vtLog.CallStack('')
        #print sVal
        i=0
        if node is None:
            #self.sem.release()
            return ''
        if sVal.find('@')>=0:
            lstAlias=sVal.split('@')
            try:
                docAlias=self.netMaster.GetDocAlias(lstAlias[1])
                if docAlias is None:
                    return ''
                sInfo=self.getNodeInfoVal(node,lstAlias[0],lang)
                #print sInfo
                nodeTmp=docAlias.getNodeById(sInfo)
                #print nodeTmp
                return docAlias.getNodeInfoVal(nodeTmp,lstAlias[2],lang)
            except:
                vtLog.vtLngTB(self.GetOrigin())
                return ''
        def getChildsInfos(node,lst,sAttr):
            for tmp in node:
                if tmp.tag==lst[0]:
                    #process child
                    if len(lst)>1:
                        return getChildsInfos(tmp,lst[1:])
                    if len(sAttr)>0:
                        for attrName,attrVal in tmp.attrib.iteritems():
                            if attrName==sAttr:
                                return attrVal
                        return ''
                    else:
                        return self.getText(tmp)
            return ''
        lst=sVal.split(':')
        strs=lst[-1].split('|')
        sAttr=''
        if len(strs)>1:
            lst[-1]=strs[0]
            sAttr=strs[1]
        if len(lst)==1:
            if len(lst[0])==0:
                if len(sAttr)>0:
                    for attrName,attrVal in node.attrib.iteritems():
                        if attrName==sAttr:
                            return attrVal
                    return ''
        return getChildsInfos(node,lst,sAttr)
    def __getParentDepthSearch__(self,node,find,l):
        for n in node:
            if n==find:
                return True
            else:
                l.append(n)
                if self.__getParentDepthSearch__(n,find,l):
                    return True
                l.pop()
        return False
    def __getParentBreadthSearch__(self,node,find,l):
        if node==find:
            #l.append(node)
            return True
        for n in node:
            if n==find:
                l.append(n)
                return True
        for n in node:
                if self.__getParentBreadthSearch__(n,find,l):
                    l.append(node)
                    return True
        return False
    def getParent(self,node):
        #self.logParentDict()
        if node is None:
            return None
        if node in self.dPar:
            return self.dPar[node]
        #if self.attr in node.attrib:
        #    id=self.getKeyNum(node)
        #    if id in self.dPar:
        #        return self.getNodeByIdNum(self.dPar[id])
        l=[self.root]
        if self.__getParentDepthSearch__(self.root,node,l):
            n=l.pop()
            return n#l[-1]
        return None
    def getNodeParent(self,node):
        if node is None:
            return None
        if self.hasKey(node):
            return node
        else:
            l=[self.root]
            if self.__getParentDepthSearch__(self.root,node,l):
                l.reverse()
                for n in l:
                    if self.hasKey(n):
                        return n
        return None
    def setText(self,node,newValue):
        if node is None:
            return -1
        self.__setModified__()
        if node.text is None:
            iRet=0
        else:
            if len(node.text)>0:
                iRet=1
            else:
                iRet=0
        if type(newValue)==types.StringType:
            node.text=newValue.encode(self.applEncoding)
        else:
            node.text=newValue
        #print type(newValue),newValue
        #node.text=newValue
        #node.text=newValue.encode(self.applEncoding)
        return iRet
    def setNodeText(self,node,tagName,newValue):
        if node is None:
            return
        nNode=self.getChild(node,tagName)
        if nNode is not None:
            iRet=self.setText(nNode,newValue)
            if iRet==1:
                return 1
            elif iRet==0:
                return 3
            return iRet
        else:
            nNode=ET.SubElement(node,tagName)
            self.setText(nNode,newValue)
            self.__setModified__()
            self.iElemCount+=1
            return 0
            #self.setNodeText(node,tagName,newValue)
    def AddValueToDictByDict(self,node,dDef,dVal,bMultiple,*args,**kwargs):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurCls(vtLog.DEBUG,'key:%s;bMulti:%d;dDef:%s;dVal:%s;args:%s;kwargs:%s'%(self.getKey(node),bMultiple,
        #                        vtLog.pformat(dDef),
        #                        vtLog.pformat(dVal),vtLog.pformat(args),vtLog.pformat(kwargs)),self)
        if node is None:
            return -1
        if 'lang' in kwargs:
            lang=kwargs['lang']
        else:
            lang=self.lang
        for tmp in node:
                sTag=tmp.tag
                if sTag in dDef:
                    if sTag not in dVal or bMultiple:
                        #print sTag,dVal
                        if self.IsNodeKeyValid(tmp):
                            if bMultiple==False:
                                dVal[sTag]={}
                        else:
                            if bMultiple:
                                if sTag in dVal:
                                    dVal[sTag].append(self.getAttributeVal(tmp))
                                else:
                                    dVal[sTag]=[self.getAttributeVal(tmp)]
                            else:
                                dVal[sTag]=self.getAttributeVal(tmp)
                    for attrName,attrVal in tmp.attrib.iteritems():
                        if attrName=='language':
                            if lang==attrVal:
                                if bMultiple:
                                    if tmp.tag in dVal:
                                        dVal[tmp.tag].append(tmp.text)
                                    else:
                                        dVal[tmp.tag]=[tmp.text]
                                else:
                                    dVal[tmp.tag]=tmp.text
                                break
        for attrName,attrVal in node.attrib.iteritems():
            sTag=''.join(['|',attrName])
            if sTag in dDef:
                dVal[sTag]=attrVal
        return 0
    def GetAttrValueToDict(self,node,*args,**kwargs):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurCls(vtLog.DEBUG,'args:%s;kwargs:%s'%(vtLog.pformat(args),vtLog.pformat(kwargs)),self)
        if node is None:
            return None
        if 'lang' in kwargs:
            lang=kwargs['lang']
        else:
            lang=self.lang
        d={}
        for tmp in node:
                if not self.IsNodeKeyValid(tmp):
                    sTag=tmp.tag
                    sVal=self.getAttributeVal(tmp)
                    sValStrip=sVal.strip()
                    if self.hasChilds(tmp):
                        if len(sVal.strip())!=0:
                            d[sTag]=sVal
                    else:
                        for attrName,attrVal in tmp.attrib.iteritems():
                            if attrName=='language':
                                if lang==attrVal:
                                    d[sTag]=sVal
                                    break
                        if sTag not in d:
                            d[sTag]=sVal
        return d
    def __createNewLib__(self,root,revision='0.0.1',attr='id'):
        self.root = ET.Element(root)
        self.dPar={}
        self.doc = ET.ElementTree(self.root)
        self.attr=attr
    def __createSubNodeLib__(self,node,tagname):
        n=ET.SubElement(node,tagname)
        self.updateParentDict(node,n)
        return n
    def __createSubNodeAttrLib__(self,node,tagname,attr,attrVal):
        n=ET.SubElement(node,tagname)
        n.attrib[attr]=attrVal
        self.updateParentDict(node,n)
        return n
    def __createMissingSubNodeByLstLib__(self,node,lst):
        bAdd=False
        for it in lst:
            n=self.getChild(node,it)
            if n is None:
                n=ET.SubElement(node,it)
                self.updateParentDict(node,n)
                self.__setModified__()
                self.iElemCount+=1
                bAdd=True
            node=n
        return node
    def __createSubNodeTextLib__(self,node,tagname,val):
        n=ET.SubElement(node,tagname)
        n.text=val.encode(self.applEncoding)
        self.updateParentDict(node,n)
        return n
    def __createSubNodeTextAttrLib__(self,node,tagname,val,attr,attrVal,align=False):
        n=ET.SubElement(node,tagname)
        n.attrib[attr]=attrVal
        n.text=val.encode(self.applEncoding)
        self.updateParentDict(node,n)
        return n
    def createNode(self,tagname):
        n=ET.Element(tagname)
        return n
    def __checkIds__(self,node):
        childs=self.getChilds(node)
        for c in childs:
            if c.tag in self.skip:
                continue
            if self.__isSkip__(c):
                continue
            id=self.getAttribute(c,self.attr)
            if len(id)>0:
                self.ids.CheckId(c,self,id)
            self.__checkIds__(c)
    def __deleteNode__(self,node,nodePar=None):
        try:
            if nodePar is None:
                vtLog.vtLngCur(vtLog.WARN,'nodePar is None',self.GetOrigin())
                if node in self.dPar:
                    nodePar=self.dPar[node]
                else:
                    vtLog.vtLngCur(vtLog.FATAL,'nodePar is None',self.GetOrigin())
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        lId=[[self.getKey(nn),self.getKey(n)] for nn,n in self.dPar.iteritems()]
                        vtLog.vtLngCur(vtLog.DEBUG,'lId:%s'%(vtLog.pformat(lId)),self.GetOrigin())
            self.__deleteNodeIds__(node)
            self.iElemCount-=self.GetElementCount(node)
            self.iElemIDCount-=self.GetElementAttrCount(node)
            self.unlinkNode(node,nodePar=nodePar)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def cloneNode(self,node,deep):
        if deep:
            tmp=self.cloneNode(node,False)
            for c in node:
                tmp.append(self.cloneNode(c,True))
        else:
            tmp=node.makeelement(node.tag,node.attrib)
            if node.text:
                tmp.text=node.text[:]
            if node.tail:
                tmp.tail=node.tail[:]
        return tmp
    def unlinkNode(self,node,nodePar=None):
        if nodePar is None:
            vtLog.vtLngCur(vtLog.WARN,'nodePar is None',self.GetOrigin())
            #print node,self.getKey(node)
            #self.showParentDict()
            if node in self.dPar:
                nodePar=self.dPar[node]
                del self.dPar[node]
            else:
                vtLog.vtLngCur(vtLog.FATAL,'nodePar is None',self.GetOrigin())
                nodePar=self.getParent(node)
            if node not in nodePar:
                vtLog.vtLngCur(vtLog.FATAL,'node is not child of nodePar',self.GetOrigin())
                nodePar=self.getParent(node)
            nodePar.remove(node)
        else:
            if node not in nodePar:
                vtLog.vtLngCur(vtLog.FATAL,'node is not child of nodePar',self.GetOrigin())
                nodePar=self.getParent(node)
            if node in self.dPar:
                del self.dPar[node]
            nodePar.remove(node)

