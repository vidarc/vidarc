#----------------------------------------------------------------------------
# Name:         vtXmlNodeContactMultiple.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      
# CVS-ID:       $Id: vtXmlNodeContactMultiple.py,v 1.3 2010/03/03 02:16:18 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeMultipledValues import *
from vidarc.tool.xml.vtXmlNodeContact import *
try:
    if vcCust.is2Import(__name__):
        from vidarc.tool.xml.vtXmlNodeCfgMLMultiplePanel import vtXmlNodeCfgMLMultiplePanel
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vtXmlNodeContactMultiple(vtXmlNodeMultipledValues,vtXmlNodeContact):
    NODE_ATTRS=[
        ]
    FUNCS_GET_SET_4_LST=[]
    def __init__(self,tagName='contacts',cfgBase=['cfg']):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        vtXmlNodeMultipledValues.__init__(self,tagName)
        vtXmlNodeContact.__init__(self,tagName,cfgBase=cfgBase)
    def GetDescription(self):
        return _(u'contacts')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def Is2Create(self):
        return False
    def Is2Add(self):
        return True
    def IsMultiple(self):
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return False
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01UIDAT8\x8d\xa5\x93\xefM\xc30\x10\xc5\x7fN\xfb=\xd9\xa0+d\x816\xe7v\
\x01F\xe8\x08^\xa0\x8d\x81\x05\xb2A;B\x16\x00\x0eX\xa0#x\x04w\x000\x1f\xaa\
\x98\x04Z\t\x89\'Y\xf2\xe9|\xef\xde\xfd\xb11\xc5\x8c\xff`>6\xda\xfd.]{$"\xd8\
\xf5\xc6\\\xf3\x99AA\xbb\xdf%\xe7\x1c1\xc6\xec\\,\x16\x00Xk\x11\x11\x00\xee\
\x1f\x1e\'D\xc5-iUUM\x14\x0c\xe7\'n\x12\xc4\x189\x9f\xcf\xd9VUT\x95v\xbfK\
\xcdj\x99K\x9d\x10\x94e\x89\xaaf\xe9\xa7\xd3iB\xda\xb6-\x00\xafo\xef\xb9\x8c\
9@\xb3Z&\xf5\x9e\x18#1FB\x08\xf4}\x8fs\xee\x92\xd9\xfb\x89\x92f\xb5L\xde{\
\xeczc\xe6\x00\xa2\x8a\x8a\x10B\xa0\xaa*B\x08\x99\xac\xef{\xee\x80\x16\x8f\
\xf1\x1eU\xc5\xae7\xc6\xae7\xdf%\xa8\x08\x02\x1c\x8fG\x00\x0e\x87\x03\xce9\
\xea\xba\xa6\xeb:\x14\xb0h\xee\xc3\x18\x931n\xb7\xdbI\xf7\x01\xba\xae\x03.\
\x93\x18\x82\xc7{Q\x0c\xc1\xce\xb9_\xc1eY\xe6{\xd34\x99\xc0\x8b\xf0\xf2\xfc\
\x94r\x13\xc7\x99~b\xc8l\x8dA\xbc\x07\xd5\x89\x82\xef\x12>?\x92\xdeX\x16UETa\
\xd8\xc6\xd1\x181\xc5\x0cS\xccp\xce%/\x92\x06{|T5\xb9\xba\xbe\xee\xff\xefo\
\xbc\xb9\xca\x7f\xc5\x175\x98\x9c\xda%\x00\xc9\xf1\x00\x00\x00\x00IEND\xaeB`\
\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        return None
    def GetAddDialogClass(self):
        return None
    def GetPanelClass(self):
        if GUI:
            return vtXmlNodeCfgMLMultiplePanel
        else:
            return None
