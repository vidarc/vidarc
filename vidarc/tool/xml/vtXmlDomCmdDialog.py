#Boa:Dialog:vtXmlDomCmdDialog
#----------------------------------------------------------------------------
# Name:         veRoleDialog.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060418
# CVS-ID:       $Id: vtXmlDomCmdDialog.py,v 1.2 2007/01/22 16:57:16 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputValue
import vidarc.tool.xml.vtXmlFiltersInput
import wx.lib.buttons

import sys,copy

from vidarc.tool.input.vtInputModifyFeedBack import *
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

def create(parent):
    return vtXmlDomCmdDialog(parent)

[wxID_VTXMLDOMCMDDIALOG, wxID_VTXMLDOMCMDDIALOGCBCANCEL, 
 wxID_VTXMLDOMCMDDIALOGCBDEL, wxID_VTXMLDOMCMDDIALOGCBEXEC, 
 wxID_VTXMLDOMCMDDIALOGCBSETARG, wxID_VTXMLDOMCMDDIALOGCHCARG, 
 wxID_VTXMLDOMCMDDIALOGCHCCMD, wxID_VTXMLDOMCMDDIALOGLBLARGDESC, 
 wxID_VTXMLDOMCMDDIALOGLBLARGUMENT, wxID_VTXMLDOMCMDDIALOGLBLCMD, 
 wxID_VTXMLDOMCMDDIALOGLBLCMDDESC, wxID_VTXMLDOMCMDDIALOGLBLCMDS, 
 wxID_VTXMLDOMCMDDIALOGLBLEXEC, wxID_VTXMLDOMCMDDIALOGLBLRES, 
 wxID_VTXMLDOMCMDDIALOGLSTCMD, wxID_VTXMLDOMCMDDIALOGTGLOCAL, 
 wxID_VTXMLDOMCMDDIALOGTGREMOTE, wxID_VTXMLDOMCMDDIALOGTXTRES, 
 wxID_VTXMLDOMCMDDIALOGVIVALUE, 
] = [wx.NewId() for _init_ctrls in range(19)]

class vtXmlDomCmdDialog(wx.Dialog,vtInputModifyFeedBack):
    VERBOSE=0
    HUM_INFOS=['name','surname','firstname']
    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblCmd, (0, 0), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.chcCmd, (0, 1), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.lblCmdDesc, (0, 2), border=0, flag=wx.EXPAND,
              span=(1, 2))
        parent.AddWindow(self.cbExec, (8, 4), border=0, flag=0, span=(1, 1))
        parent.AddWindow(self.lblArgument, (1, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.chcArg, (1, 1), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.lblArgDesc, (1, 2), border=0, flag=wx.EXPAND,
              span=(1, 2))
        parent.AddWindow(self.viValue, (2, 0), border=0, flag=wx.EXPAND,
              span=(1, 4))
        parent.AddWindow(self.cbSetArg, (2, 4), border=0, flag=0, span=(1, 1))
        parent.AddWindow(self.lblCmds, (3, 0), border=0, flag=wx.EXPAND,
              span=(1, 4))
        parent.AddWindow(self.lstCmd, (4, 0), border=0, flag=wx.EXPAND, span=(2,
              4))
        parent.AddWindow(self.cbDel, (1, 4), border=0, flag=0, span=(1, 1))
        parent.AddWindow(self.lblRes, (6, 0), border=0, flag=wx.EXPAND, span=(1,
              4))
        parent.AddWindow(self.txtRes, (7, 0), border=0, flag=wx.EXPAND, span=(1,
              4))
        parent.AddSpacer(wx.Size(8, 8), (9, 0), border=0, flag=0, span=(1, 1))
        parent.AddWindow(self.cbCancel, (10, 0), border=0, flag=wx.ALIGN_CENTER,
              span=(1, 4))
        parent.AddWindow(self.lblExec, (8, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddSizer(self.bxsLocRmt, (8, 1), border=0, flag=0, span=(1, 3))

    def _init_coll_bxsLocRmt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.tgLocal, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.tgRemote, 0, border=0, flag=0)

    def _init_coll_lstCmd_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'argument'), width=80)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'value'), width=120)

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=4, vgap=4)

        self.bxsLocRmt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_gbsData_Items(self.gbsData)
        self._init_coll_bxsLocRmt_Items(self.bxsLocRmt)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTXMLDOMCMDDIALOG,
              name=u'vtXmlDomCmdDialog', parent=prnt, pos=wx.Point(401, 197),
              size=wx.Size(337, 368),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE,
              title=u'vtXmlDom Command Dialog')
        self.SetClientSize(wx.Size(329, 341))

        self.lblCmd = wx.StaticText(id=wxID_VTXMLDOMCMDDIALOGLBLCMD,
              label=_(u'command'), name=u'lblCmd', parent=self, pos=wx.Point(0,
              0), size=wx.Size(46, 21), style=wx.ALIGN_RIGHT)
        self.lblCmd.SetMinSize(wx.Size(-1, -1))

        self.chcCmd = wx.Choice(choices=[], id=wxID_VTXMLDOMCMDDIALOGCHCCMD,
              name=u'chcCmd', parent=self, pos=wx.Point(50, 0),
              size=wx.Size(130, 21), style=0)
        self.chcCmd.SetMinSize(wx.Size(-1, -1))
        self.chcCmd.Bind(wx.EVT_CHOICE, self.OnChcCmdChoice,
              id=wxID_VTXMLDOMCMDDIALOGCHCCMD)

        self.cbExec = wx.lib.buttons.GenBitmapButton(ID=wxID_VTXMLDOMCMDDIALOGCBEXEC,
              bitmap=vtArt.getBitmap(vtArt.Build), name=u'cbExec', parent=self,
              pos=wx.Point(298, 254), size=wx.Size(31, 30), style=0)
        self.cbExec.Bind(wx.EVT_BUTTON, self.OnCbExecButton,
              id=wxID_VTXMLDOMCMDDIALOGCBEXEC)

        self.lblArgument = wx.StaticText(id=wxID_VTXMLDOMCMDDIALOGLBLARGUMENT,
              label=_(u'argument'), name=u'lblArgument', parent=self,
              pos=wx.Point(0, 25), size=wx.Size(46, 30), style=wx.ALIGN_RIGHT)
        self.lblArgument.SetMinSize(wx.Size(-1, -1))

        self.chcArg = wx.Choice(choices=[], id=wxID_VTXMLDOMCMDDIALOGCHCARG,
              name=u'chcArg', parent=self, pos=wx.Point(50, 25),
              size=wx.Size(130, 21), style=0)
        self.chcArg.SetMinSize(wx.Size(-1, -1))
        self.chcArg.Bind(wx.EVT_CHOICE, self.OnChcArgChoice,
              id=wxID_VTXMLDOMCMDDIALOGCHCARG)

        self.cbSetArg = wx.lib.buttons.GenBitmapButton(ID=wxID_VTXMLDOMCMDDIALOGCBSETARG,
              bitmap=vtArt.getBitmap(vtArt.Edit), name=u'cbSetArg', parent=self,
              pos=wx.Point(298, 59), size=wx.Size(31, 30), style=0)
        self.cbSetArg.Bind(wx.EVT_BUTTON, self.OnCbSetArgButton,
              id=wxID_VTXMLDOMCMDDIALOGCBSETARG)

        self.cbDel = wx.lib.buttons.GenBitmapButton(ID=wxID_VTXMLDOMCMDDIALOGCBDEL,
              bitmap=vtArt.getBitmap(vtArt.Del), name=u'cbDel', parent=self,
              pos=wx.Point(298, 25), size=wx.Size(31, 30), style=0)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VTXMLDOMCMDDIALOGCBDEL)

        self.viValue = vidarc.tool.input.vtInputValue.vtInputValue(id=wxID_VTXMLDOMCMDDIALOGVIVALUE,
              name=u'viValue', parent=self, pos=wx.Point(0, 59),
              size=wx.Size(294, 30), style=0)

        self.lblCmds = wx.StaticText(id=wxID_VTXMLDOMCMDDIALOGLBLCMDS,
              label=_(u'arguments'), name=u'lblCmds', parent=self,
              pos=wx.Point(0, 93), size=wx.Size(294, 20), style=0)
        self.lblCmds.SetMinSize(wx.Size(-1, -1))

        self.lstCmd = wx.ListCtrl(id=wxID_VTXMLDOMCMDDIALOGLSTCMD,
              name=u'lstCmd', parent=self, pos=wx.Point(0, 117),
              size=wx.Size(294, 84), style=wx.LC_REPORT)
        self.lstCmd.SetMinSize(wx.Size(-1, -1))
        self._init_coll_lstCmd_Columns(self.lstCmd)

        self.lblCmdDesc = wx.TextCtrl(id=wxID_VTXMLDOMCMDDIALOGLBLCMDDESC,
              name=u'lblCmdDesc', parent=self, pos=wx.Point(184, 0),
              size=wx.Size(110, 21), style=wx.TE_CENTER | wx.NO_BORDER,
              value=u'')
        self.lblCmdDesc.Enable(False)
        self.lblCmdDesc.SetMinSize(wx.Size(-1, -1))

        self.lblArgDesc = wx.TextCtrl(id=wxID_VTXMLDOMCMDDIALOGLBLARGDESC,
              name=u'lblArgDesc', parent=self, pos=wx.Point(184, 25),
              size=wx.Size(110, 30), style=wx.TE_CENTER | wx.NO_BORDER,
              value=u'')
        self.lblArgDesc.Enable(False)
        self.lblArgDesc.SetMinSize(wx.Size(-1, -1))

        self.lblRes = wx.StaticText(id=wxID_VTXMLDOMCMDDIALOGLBLRES,
              label=_(u'result'), name=u'lblRes', parent=self, pos=wx.Point(0,
              205), size=wx.Size(294, 20), style=0)
        self.lblRes.SetMinSize(wx.Size(-1, -1))

        self.txtRes = wx.TextCtrl(id=wxID_VTXMLDOMCMDDIALOGTXTRES,
              name=u'txtRes', parent=self, pos=wx.Point(0, 229),
              size=wx.Size(294, 21), style=wx.TE_CENTER | wx.NO_BORDER,
              value=u'')
        self.txtRes.Enable(False)
        self.txtRes.SetMinSize(wx.Size(-1, -1))

        self.lblExec = wx.StaticText(id=wxID_VTXMLDOMCMDDIALOGLBLEXEC,
              label=_(u'execute'), name=u'lblExec', parent=self, pos=wx.Point(0,
              254), size=wx.Size(46, 30), style=0)
        self.lblExec.SetMinSize(wx.Size(-1, -1))

        self.tgLocal = wx.lib.buttons.GenBitmapTextToggleButton(ID=wxID_VTXMLDOMCMDDIALOGTGLOCAL,
              bitmap=vtArt.getBitmap(vtArt.Desk), label=_(u'local'),
              name=u'tgLocal', parent=self, pos=wx.Point(50, 254),
              size=wx.Size(76, 30), style=0)
        self.tgLocal.SetToggle(False)
        self.tgLocal.SetMinSize(wx.Size(-1, -1))

        self.tgRemote = wx.lib.buttons.GenBitmapTextToggleButton(ID=wxID_VTXMLDOMCMDDIALOGTGREMOTE,
              bitmap=vtArt.getBitmap(vtArt.Globe), label=_(u'remote'),
              name=u'tgRemote', parent=self, pos=wx.Point(134, 254),
              size=wx.Size(76, 30), style=0)
        self.tgRemote.SetToggle(False)
        self.tgRemote.SetMinSize(wx.Size(-1, -1))

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLDOMCMDDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(109, 312),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VTXMLDOMCMDDIALOGCBCANCEL)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self._init_ctrls(parent)
        vtInputModifyFeedBack.__init__(self,lWidgets=[])
        #self.viValue.SetEnableMark(False)
        self.doc=None
        self.dCmd={}
        self.lCmd=[]
        self.Clear()
        self.gbsData.AddGrowableCol(1)
        self.gbsData.AddGrowableCol(3)
        self.gbsData.AddGrowableRow(5)
        
        self.gbsData.Layout()
        #self.gbsData.FitInside(self)
        self.Layout()
        
    def Clear(self):
        vtInputModifyFeedBack.SetBlock(self)
        vtInputModifyFeedBack.SetModified(self,False)
        vtInputModifyFeedBack.ClrBlockDelayed(self)
        #self.chcCmd.Clear()
        self.lstCmd.DeleteAllItems()
        self.selIdx=-1
        self.dCmdAct={}
        self.tagName=None
        self.node=None
    def SelectCmd(self,cmd):
        try:
            self.chcCmb.SetStringSelection(cmd)
        except:
            pass
    def SetTagName(self,tagName):
        self.tagName=tagName
    def SetCmdDict(self,dCmd,cmd=None,tagName=None):
        """ dCmd = { 
            'cmd01': ('cmd01 translation', {'arg01: ('arg01 translation','arg01 type'} ),
            'cmd02': ('cmd02 translation', None),
            'cmd03': ('cmd03 translation', {'arg01: ('arg01 translation','arg01 type'}            }
        """
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'dCmd:%s'%(vtLog.pformat(dCmd)),self)
        self.tagName=tagName
        self.dCmd=dCmd
        self.chcCmd.Clear()
        self.lCmd=[]
        for k in self.dCmd.keys():
            tup=self.dCmd[k]
            self.lCmd.append((k,tup[0],{}))
        self.lCmd.sort()
        i=0
        iPos=0
        for it in self.lCmd:
            self.chcCmd.Append(it[0],i)
            if cmd is not None:
                if it[0]==cmd:
                    iPos=i
            i+=1
        try:
            self.chcCmd.SetSelection(iPos)
        except:
            self.lblCmdDesc.SetValue('')
        wx.CallAfter(self.OnChcCmdChoice,None)
    def __showArg__(self,sCmd):
        if self.VERBOSE:
            vtLog.CallStack('')
            print sCmd
        self.chcArg.Clear()
        if sCmd is None:
            return
        try:
            dArgs=self.dCmd[sCmd][2]
            keys=dArgs.keys()
            keys.sort()
            for k in keys:
                self.chcArg.Append(k)
            self.chcArg.SetSelection(0)
        except:
            pass
        wx.CallAfter(self.OnChcArgChoice,None)
    def __showArgs__(self):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'dCmdAct:%s'%(vtLog.pformat(self.dCmdAct)),self)
            self.lstCmd.DeleteAllItems()
            d=self.dCmdAct
            argKeys=d.keys()
            argKeys.sort()
            for argKey in argKeys:
                idx=self.lstCmd.InsertStringItem(sys.maxint,argKey)
                self.lstCmd.SetStringItem(idx,1,str(d[argKey]))
        except:
            vtLog.vtLngTB(self.__class__.__name__)
            vtInputModifyFeedBack.SetModified(self,True)
    def SetDoc(self,doc,bNet):
        self.doc=doc
    def SetNode(self,node):
        self.node=node
    def OnLstRoleListColClick(self, event):
        self.selIdx=-1
        event.Skip()
    def OnLstRoleListItemDeselected(self, event):
        self.selIdx=-1
        event.Skip()
    def OnLstRoleListItemSelected(self, event):
        self.selIdx=event.GetIndex()
        event.Skip()
    def OnCbDelButton(self, event):
        event.Skip()
        try:
            sArg=str(self.chcArg.GetStringSelection())
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
                print sArg
                print 'arg'
                vtLog.pprint(self.dCmd)
                print 'cmd act'
                vtLog.pprint(self.dCmdAct)
            try:
                del self.dCmdAct[sArg]
            except:
                pass
            self.SetModified(True)
            self.__showArgs__()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbExecButton(self, event):
        event.Skip()
        try:
            sCmd=str(self.chcCmd.GetStringSelection())
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
                print sCmd
                print 'cmd'
                vtLog.pprint(self.dCmd)
                print 'cmd act'
                vtLog.pprint(self.dCmdAct)
            #self.SetModified(True)
            #self.__showArgs__()
            if self.tgLocal.GetValue():
                iRet=self.doc.DoCmd(None,sCmd,**self.dCmdAct)
                if iRet==1:
                    self.txtRes.SetValue(_(u'action executed correctly'))
                elif iRet==0:
                    self.txtRes.SetValue(_(u'action unknown'))
                elif iRet==-1:
                    self.txtRes.SetValue(_(u'fault occured during execution'))
                elif iRet==-2:
                    self.txtRes.SetValue(_(u'permission denied'))
                elif iRet==-3:
                    self.txtRes.SetValue(_(u'action execution in process'))
                #else:
                #    vtLog.vtLngCurWX(vtLog.ERROR,'human id:%08d not found;shall never happen'%id,self)
            if self.tgRemote.GetValue():
                try:
                    if self.tagName is None:
                        sCmdRmt=sCmd
                    else:
                        sCmdRmt=':'.join([self.tagName,sCmd])
                    self.doc.DoCmdRemote(sCmdRmt,**self.dCmdAct)
                except:
                    vtLog.vtLngTB(self.GetName())
        except:
            vtLog.vtLngTB(self.GetName())

    def OnCbApplyButton(self, event):
        self.EndModal(1)
        event.Skip()

    def OnCbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()

    def OnCbSetArgButton(self, event):
        event.Skip()
        try:
            sArg=str(self.chcArg.GetStringSelection())
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
                print sArg
                print 'cmd'
                vtLog.pprint(self.dCmd)
                print 'cmd act'
                vtLog.pprint(self.dCmdAct)
            sVal=self.viValue.GetValueStr()
            iLen=len(sVal)
            sVal=sVal.replace(';','')
            sVal=sVal.replace(',','')
            sVal=sVal.replace(':','')
            self.dCmdAct[sArg]=sVal
            if len(sVal)!=iLen:
                self.viValue.SetValueStr(sVal)
            self.SetModified(True)
            self.__showArgs__()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnChcCmdChoice(self, event):
        if event is not None:
            event.Skip()
        self.dCmdAct={}
        s=self.chcCmd.GetStringSelection()
        if s<0:
            self.lblCmdDesc.SetLabel('')
            self.__showArg__(None)
            return
        try:
            tup=self.dCmd[s]
            self.lblCmdDesc.SetLabel(tup[0])
            iKind=tup[1]
            if iKind&0x01:
                self.tgLocal.SetValue(True)
                self.tgLocal.Enable(True)
            else:
                self.tgLocal.SetValue(False)
                self.tgLocal.Enable(False)
            if iKind&0x02:
                self.tgRemote.SetValue(True)
                self.tgRemote.Enable(True)
            else:
                self.tgRemote.SetValue(False)
                self.tgRemote.Enable(False)
            if self.node is not None:
                dArgs=tup[2]
                for k,v in dArgs.items():
                    self.dCmdAct[k]=self.doc.getNodeInfoVal(self.node,k,None)
                vtLog.CallStack('')
                vtLog.pprint(self.dCmdAct)
                self.__showArgs__()
            self.__showArg__(s)
        except:
            self.lblCmdDesc.SetLabel('****')
            self.__showArg__(None)
    def OnChcArgChoice(self, event):
        if event is not None:
            event.Skip()
        sCmd=self.chcCmd.GetStringSelection()
        sArg=self.chcArg.GetStringSelection()
        if self.VERBOSE:
            vtLog.CallStack('')
            print sCmd
            print sArg
        if len(sArg)<=0:
            self.lblArgDesc.SetValue('')
            self.viValue.Enable(False)
            return
        try:
            dArgs=self.dCmd[sCmd][2]
            self.lblArgDesc.SetValue(dArgs[sArg][0])
            if sArg in self.dCmdAct:
                val=self.dCmdAct[sArg]
            else:
                val=''
            self.viValue.Set(dArgs[sArg][1])
            self.viValue.SetValue(val)
            dArg=dArgs[sArg]
            vtLog.pprint(dArg)
            if 'readonly' in dArg[2]:
                self.viValue.Enable(not dArg[2]['readonly'])
            else:
                self.viValue.Enable(True)
        except:
            self.lblArgDesc.SetValue('')
            self.viValue.Enable(False)

