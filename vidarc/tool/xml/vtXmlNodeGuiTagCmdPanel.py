#----------------------------------------------------------------------------
# Name:         vtXmlNodeGuiTagCmdPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20121231
# CVS-ID:       $Id: vtXmlNodeGuiTagCmdPanel.py,v 1.2 2015/01/21 12:55:53 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import vLang
    import vidarc.tool.art.vtArt as vtArt
    from vidarc.tool.xml.vtXmlNodeGuiPanel import vtXmlNodeGuiPanel
    from vidarc.tool.input.vtInputTextMultiLineML import vtInputTextMultiLineML
    from vidarc.tool.input.vtInputTextML import vtInputTextML
    from vidarc.tool.input.vtInputTextPopup import vtInputTextPopup
    
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vtXmlNodeGuiTagCmdPanel(vtXmlNodeGuiPanel):
    def __initCtrl__(self,*args,**kwargs):
        global _
        _=vLang.assignPluginLang('vtXml')
        bNet=True
        self.SetVerbose(__name__,iVerbose=-1,kwargs=kwargs)
        
        lWid=[]
        iRow=0
        lWid.append(('lblRg',(iRow,0),(1,1),{'name':'lblTag',
                'label':_(u'tag'),
                },None))
        lWid.append((vtInputTextPopup,(iRow,1),(1,1),{'name':'viTag',
                #'tip':_(u''),
                },{
                #'ent':self.OnTagEnter,
                #'txt':self.OnTagText,
                }))
        
        lWid.append(('lblRg',(iRow,2),(1,1),{'name':'lblName',
                'label':_(u'name'),
                },None))
        lWid.append((vtInputTextML,(iRow,3),(1,1),{'name':'viName',
                #'tip':_(u''),
                },{
                #'ent':self.OnNameEnter,
                #'txt':self.OnNameText,
                }))
        lWid.append(('cbBmpLbl',(iRow,4),(1,1),{'name':'cbApply',
                'tip':_(u'apply'),'label':_('Apply'),
                'bitmap':vtArt.getBitmap(vtArt.Apply)},{
                'btn':self.OnApplyButton,
                }))
        
        iRow+=1
        lWid.append(('lblRg',(iRow,0),(1,1),{'name':'lblDesc',
                'label':_(u'description'),
                },None))
        lWid.append((vtInputTextMultiLineML,(iRow,1),(2,3),{'name':'viDesc',
                #'tip':_(u'description'),
                },{
                #'ent':self.OnDescEnter,
                #'txt':self.OnDescText,
                }))
        lWid.append(('cbBmpLbl',(iRow,4),(1,1),{'name':'cbCancel',
                'tip':_(u'Cancel'),'label':_('Cancel'),
                'bitmap':vtArt.getBitmap(vtArt.Cancel)},{
                'btn':self.OnCancelButton,
                }))
        
        iRow+=2
        lWid.append(('lbl',(iRow,0),(1,1),{'name':'lblID',
                'label':_(u'ID'),
                },None))
        lWid.append(('txtRdRg',(iRow,1),(1,1),{'name':'txtID',
                'tip':_(u'ID'),
                'value':''},{
                #'ent':self.OnIDEnter,
                #'txt':self.OnIDText,
                }))
        
        lWid.append(('lbl',(iRow,2),(1,1),{'name':'lblType',
                'label':_(u'type'),
                },None))
        lWid.append(('txtRd',(iRow,3),(1,1),{'name':'txtType',
                'tip':_(u'type'),
                'value':''},{
                #'ent':self.OnTypeEnter,
                #'txt':self.OnTypeText,
                }))
        
        vtXmlNodeGuiPanel.__initCtrl__(self,lWid=lWid)
        
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            self.viTag.SetTagName('tag')
            self.viName.SetTagName('name')
            self.viDesc.SetTagName('description')
            
        except:
            self.__logTB__()
        self.dWidDependent={}
        self.dObjReg={}
        self.sActTag=''
        
        self.__clr__()
        return [(2,1),],[(1,1),(3,3),]
    def __clr__(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            self.sTagCur=None
            self.sTagOld=None
        except:
            self.__logTB__()
    def __Clear__(self):
        try:
            # add code here
            self.viTag.Clear()
            self.viName.Clear()
            self.viDesc.Clear()
            self.txtType.SetValue('')
            self.txtID.SetValue(u'')
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def SetNetDocs(self,d):
        #self.viTag.SetNetDocs(d)
        for k in self.dObjReg.keys():
            self.__apply2Dependent__(k,'SetNetDocs',d)
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            self.viTag.SetDoc(doc)
            self.viName.SetDoc(doc)
            self.viDesc.SetDoc(doc)
            
            for k,lWid in self.dWidDependent.iteritems():
                for wid in lWid:
                    try:
                        if bDbg:
                            self.__logDebug__('k:%s;cls:%s'%(k,wid.__class__.__name__))
                        if wid is not None:
                            if hasattr(wid,'SetDoc'):
                                f=getattr(wid,'SetDoc')
                                f(doc,bNet=bNet)
                            #else:
                            #    f=getattr(wid,'SetDoc')
                            #    f(doc,bNet=bNet)
                            #    f=getattr(wid,'SetNetDocs')
                            #    f(dDocs)
                    except:
                        self.__logTB__()
        except:
            self.__logTB__()
    def SetNodeOld(self,node):
        try:
            
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if self.IsMainThread():
                pass
            if self.node is not None:
                sTagOld=self.doc.getTagName(self.node)
                sTag=self.doc.getTagName(node)
                if sTagOld!=sTag:
                    self.__apply2Dependent__(sTagOld,'SetNode',None)
                if len(sTag)==0:
                    self.__apply2Dependent__(sTagOld,'Clear')
                    vtXmlNodeGuiPanel.Clear(self)
                    self.__showDependent__('')
                    return
            self.__SetNode__(node)
        except:
            self.__logTB__()
    def __SetNode__(self,node):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__(''%())
            # add code here
            if node is None:
                self.__showDependent__('')
                #self.viTag.__Clear__()
                return
            self.viTag.SetNode(node)
            self.viName.SetNode(node)
            self.viDesc.SetNode(node)
            sTag=self.doc.getTagName(node)
            if len(sTag)>0:
                self.sTagCur=sTag[:]
                o=self.doc.GetRegisteredNode(sTag)
                if o is not None:
                    self.txtType.SetValue(o.GetDescription())
                else:
                    self.txtType.SetValue(' '.join([sTag[:],'?']))
            else:
                self.txtType.SetValue('')
                self.sTagCur=None
            
            sID=u''
            iId=long(self.doc.getKey(node))
            if iId>=0:
                try:
                    sID=self.doc.ConvertIdNum2Str(iId)
                except:
                    pass
            if hasattr(self.doc,'getKeyInst'):
                try:
                    iIID=self.doc.getKeyInst(node)
                    if iIID>0:
                        sIID=self.doc.ConvertIdNum2Str(iIID)
                        sID=''.join([sID,' (',sIID,')'])
                except:
                    pass
            self.txtID.SetValue(sID)
            
            self.__showDependent__(sTag)
            self.__apply2Dependent__(sTag,'SetNode',node)
            
            if self.sTagOld is not None:
                #sTagOld=self.doc.getTagName(self.node)
                #sTag=self.doc.getTagName(node)
                if self.sTagOld!=self.sTagCur:
                    self.__apply2Dependent__(self.sTagOld,'SetNode',None)
                if len(sTag)==0:
                    self.__apply2Dependent__(self.sTagOld,'Clear')
                    vtXmlNodeGuiPanel.Clear(self)
                    self.__showDependent__('')
                    return
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__(''%())
            # add code here
            self.viTag.GetNode(node)
            self.viName.GetNode(node)
            self.viDesc.GetNode(node)
            if bDbg:
                self.__logDebug__(''%())
            
            if self.nodeId>=0:
                self.__apply2DependentFB__(self.sTagCur,'__GetNode__',node)
            if bDbg:
                self.__logDebug__('done')
            
        except:
            self.__logTB__()
    def __Cancel__(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            # add code here
            self.viTag.Clear()
            self.viName.Clear()
            self.viDesc.Clear()
            if self.nodeId>=0:
                self.__apply2DependentFB__(self.sTagCur,'__Cancel__')
        except:
            self.__logTB__()
    def __Close__(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            # add code here
            self.viTag.Close()
            self.viName.Close()
            self.viDesc.Close()
            if self.nodeId>=0:
                self.__apply2DependentFB__(self.sTagCur,'__Close__')
        except:
            self.__logTB__()
    def __Lock__(self,flag):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if flag:
                # add code here
                self.Close()
                self.viTag.Enable(False)
                self.viName.Enable(False)
                self.viDesc.Enable(False)
            else:
                # add code here
                self.viTag.Enable(True)
                self.viName.Enable(True)
                self.viDesc.Enable(True)
            if self.nodeId>=0:
                self.__apply2DependentFB__(self.sTagCur,'__Lock__',flag)
        except:
            self.__logTB__()
    def AddWidgetDependent(self,wid,obj):
        if obj is None:
            sTag=None
        else:
            sTag=obj.GetTagName()
        if not self.dWidDependent.has_key(sTag):
            self.dWidDependent[sTag]=[]
        self.dWidDependent[sTag].append(wid)
    def GetModified(self,bDependent=False):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__({'bDependent':bDependent,
                        'sActTag':self.sActTag,
                        'sTagCur':self.sTagCur,
                        'sTagOld':self.sTagOld})
            
            if vtXmlNodeGuiPanel.GetModified(self):
                return True
            if self.viTag.IsModified():
                return True
            if self.viName.IsModified():
                return True
            if self.viDesc.IsModified():
                return True
            
            if bDependent==False:
                return False
            if self.sActTag!='':
                lWid=self.dWidDependent[self.sActTag]
                for wid in lWid:
                    try:
                        if wid is not None:
                            if wid.GetModified():
                                return True
                    except:
                        self.__logError__('tag:%s;%s'%(self.sActTag,wid))
                        self.__logTB__()
        except:
            self.__logTB__()
        return False
    def __apply2Dependent__(self,sTag,method,*args,**kwargs):
        if not self.dWidDependent.has_key(sTag):
            if None in self.dWidDependent:
                sTag=None
            else:
                return
        lWid=self.dWidDependent[sTag]
        for wid in lWid:
            try:
                if wid is not None:
                    #sMethod=''.join(['__',method,'__'])
                    #if hasattr(wid,sMethod):
                    #    f=getattr(wid,sMethod)
                    #else:
                    if hasattr(wid,method):
                        f=getattr(wid,method)
                        f(*args,**kwargs)
            except:
                self.__logTB__()
    def __apply2DependentFB__(self,sTag,method,*args,**kwargs):
        if not self.dWidDependent.has_key(sTag):
            if None in self.dWidDependent:
                sTag=None
            else:
                return
        lWid=self.dWidDependent[sTag]
        for wid in lWid:
            try:
                if wid is not None:
                    sMethod=''.join(['__',method,'__'])
                    if hasattr(wid,sMethod):
                        f=getattr(wid,sMethod)
                        f(*args,**kwargs)
                    else:
                        if hasattr(wid,method):
                            f=getattr(wid,method)
                            f(*args,**kwargs)
                        else:
                            self.__logWarn__('method:%s not found'%(method))
            except:
                self.__logTB__()
    def __showDependent__(self,sTag):
        try:
            if sTag not in self.dWidDependent:
                sTag=None
                if sTag not in self.dWidDependent:
                    if self.sActTag!='':
                        lWid=self.dWidDependent[self.sActTag]
                        for wid in lWid:
                            if wid is not None:
                                wid.Show(False)
                    self.sActTag=''
                    #self.Enable(False)
                    #self.Refresh()
                    self.Post('sel',{'iId':self.nodeId,'sTag':sTag})
                    return
            #self.Enable(True)
            if sTag=='':
                if self.sActTag!=sTag:
                    for lWid in self.dWidDependent.values():
                        for wid in lWid:
                            if wid is not None:
                                wid.Show(False)
                self.sActTag=sTag
                self.Post('sel',{'iId':self.nodeId,'sTag':sTag})
            else:
                if self.sActTag!=sTag:
                    if self.sActTag!='':
                        lWid=self.dWidDependent[self.sActTag]
                        for wid in lWid:
                            if wid is not None:
                                wid.Show(False)
                    #else:
                    #    self.Enable(True)
                    #    self.Refresh()
                    lWid=self.dWidDependent[sTag]
                    for wid in lWid:
                        if wid is not None:
                            wid.Show(True)
                    self.sActTag=sTag
                    self.Post('sel',{'iId':self.nodeId,'sTag':sTag,'lWid':lWid})
        except:
            self.__logTB__()
    def SetRegNode(self,obj,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            sTag=obj.GetTagName()
            self.dObjReg[sTag]=obj
            self.__apply2Dependent__(sTag,'SetRegNode',obj,*args,**kwargs)
            if bDbg:
                self.__logDebugAdd__('done',self.dObjReg)
            
        except:
            self.__logTB__()
    def Apply(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if self.IsMainThread()==False:
                return
            if self.nodeId>=0:
                self.GetNode()
                #self.__apply2Dependent__(self.sTagCur,'Cancel')
                self.Post('apply',{'iId':self.nodeId})
        except:
            self.__logTB__()
    def Cancel(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if self.IsMainThread()==False:
                return
            if self.nodeId>=0:
                self.SetNode(self.node)
                self.__apply2Dependent__(self.sTagCur,'Cancel')
                self.Post('cancel',{'iId':self.nodeId})
            
        except:
            self.__logTB__()
    def OnApplyButton(self, event):
        event.Skip()
        self.Apply()
    def OnCancelButton(self, event):
        event.Skip()
        self.Cancel()
    def CreateRegisteredPanel(self,sReg,doc=None,bNet=True,parent=None,**kwargs):
        """            
            parent=None,id=-1,pos=(-1,-1),
            sz=(-1,-1),style=0,name=None
        """
        try:
            bDbg=self.GetVerboseDbg(5)
            if bDbg:
                self.__logDebug__('')
            if self.IsMainThread()==False:
                return
            if doc is None:
                doc=self.doc
            if doc is None:
                self.__logError__('no doc set')
            oReg=doc.GetReg(sReg)
            if hasattr(oReg,'GetPanelClassGui'):
                pnCls=oReg.GetPanelClassGui()
            else:
                pnCls=oReg.GetPanelClass()
            if hasattr(parent,'GetWid'):
                par=parent.GetWid()
            else:
                par=parent
            if hasattr(pnCls,'GetWid'):
                if bDbg:
                    self.__logDebug__('')
                _args,_kwargs=self.GetGuiArgs({'id':-1,'parent':par,
                    'name':sReg,'size':(100,100),
                    'style':0},
                    ['id','name','parent','pos','size','style',],{
                    'name':u'pnData',
                    'pos':None,'size':None,'style':None
                    },kwargs)
                pnObj=pnCls(*_args,**_kwargs)
                pn=pnObj.GetWid()
                pnObj.SetRegNode(oReg)
                pnObj.SetDoc(doc,bNet)
                self.AddWidgetDependent(pnObj,oReg)
                if kwargs.get('sizerSkipAdd',False)==False:
                    bzSizer=par.GetSizer()
                    parent.AddWid(pn,**kwargs)
                    pn.Show(False)
            else:
                if bDbg:
                    self.__logDebug__('')
                _args,_kwargs=self.GetGuiArgs({'id':-1,'parent':par,
                    'name':sReg,'size':(100,100),
                    'style':0},
                    ['id','name','parent','pos','size','style',],{
                    'name':u'pnData',
                    'pos':None,'size':None,'style':None
                    },kwargs)
                pn=pnObj=pnCls(*_args,**_kwargs)
                #pn=pnCls(par,id,pos=pos,size=sz,style=style,name=name)
                pn.SetRegNode(oReg)
                pn.SetDoc(doc,bNet)
                self.AddWidgetDependent(pn,oReg)
                if kwargs.get('sizerSkipAdd',False)==False:
                    bzSizer=par.GetSizer()
                    parent.AddWid(pn,**kwargs)
                    pn.Show(False)
                    
                    #par.Add(pn,proportion=kwargs.get('sizerProportion',1),
                    
            return pnObj
        except:
            self.__logTB__()
        return None
    def GetSelRegName(self):
        return self.sActTag
