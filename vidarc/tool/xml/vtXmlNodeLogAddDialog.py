#Boa:Dialog:vtXmlNodeLogAddDialog
#----------------------------------------------------------------------------
# Name:         vtXmlNodeLogAddDialog.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20060203
# CVS-ID:       $Id: vtXmlNodeLogAddDialog.py,v 1.7 2006/08/29 10:06:24 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

from vtXmlNodeLogPanel import *

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
from vidarc.tool.xml.vtXmlNodeAddDialog import *
import vidarc.tool.lang.vtLgBase as vtLgBase

def create(parent):
    return vtXmlNodeLogAddDialog(parent)

[wxID_VTXMLNODELOGADDDIALOG, wxID_VTXMLNODELOGADDDIALOGCBAPPLY, 
 wxID_VTXMLNODELOGADDDIALOGCBCANCEL, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vtXmlNodeLogAddDialog(wx.Dialog,vtXmlNodeAddDialog):
    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsBt, (1, 0), border=4,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER, span=(1, 1))

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=0, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_gbsData_Items(self.gbsData)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTXMLNODELOGADDDIALOG,
              name=u'vtXmlNodeLogAddDialog', parent=prnt, pos=wx.Point(373, 97),
              size=wx.Size(470, 350), style=wx.DEFAULT_DIALOG_STYLE,
              title=_(u'vtXmlLog Add Dialog'))
        self.SetClientSize(wx.Size(462, 323))

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLNODELOGADDDIALOGCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'),
              name=u'cbApply', parent=self, pos=wx.Point(0, 24),
              size=wx.Size(76, 30), style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VTXMLNODELOGADDDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLNODELOGADDDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(108, 24),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VTXMLNODELOGADDDIALOGCBCANCEL)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self._init_ctrls(parent)
        self.doc=None
        self.node=None
        vtXmlNodeAddDialog.__init__(self)
        try:
            self.gbsData.AddGrowableCol(0)
            self.gbsData.AddGrowableRow(0)
        
            self.pn=vtXmlNodeLogPanel(self,id=wx.NewId(),pos=(0,0),size=(440,285),
                        style=0,name=u'pnLoc')
            self.gbsData.AddWindow(self.pn, (0, 0), border=4,
                  flag=wx.BOTTOM | wx.TOP | wx.LEFT | wx.RIGHT | wx.EXPAND, span=(1, 1))
        except:
            vtLog.vtLngTB('')
    def Close(self):
        self.pn.Close()
    
