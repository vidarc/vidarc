#----------------------------------------------------------------------------
# Name:         vtXmlNodeDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060419
# CVS-ID:       $Id: vtXmlNodeDialog.py,v 1.9 2010/05/18 22:08:16 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.log.vtLog import vtLogOriginWX

class vtXmlNodeDialog(vtLogOriginWX):
    def __init__(self):
        vtLogOriginWX.__init__(self)
        if hasattr(self,'pn')==False:
            self.pn=None
        if hasattr(self,'pnAdd')==False:
            self.pnAdd=None
    def Clear(self):
        ## FIXME delay widget clear becase __GetNode__ may be called later becase delay 
        ## cased by thread and/or lock
        try:
            if hasattr(self.pn,'ClearSafe'):
                self.pn.ClearSafe()
            else:
                self.pn.Clear()
            if self.pnAdd is not None:
                if hasattr(self.pnAdd,'ClearSafe'):
                    self.pnAdd.ClearSafe()
                else:
                    self.pnAdd.Clear()
        #self.pn.Clear()
        #if self.pnAdd is not None:
        #    self.pnAdd.Clear()
        except:
            self.__logTB__()
    def SetRegNode(self,obj):
        self.pn.SetRegNode(obj)
        if self.pnAdd is not None:
            self.pnAdd.SetRegNode(obj)
    def SetDoc(self,doc,bNet=False):
        self.pn.SetDoc(doc,bNet)
        if self.pnAdd is not None:
            self.pnAdd.SetDoc(doc,bNet)
    def SetNode(self,node):
        self.pn.SetNode(node)
        if self.pnAdd is not None:
            self.pnAdd.SetNode(node)
    def SetNodeExt(self,objReg,nodePar,node):
        self.objReg=objReg
        self.nodePar=nodePar
        self.pn.SetNode(node)
        if self.pnAdd is not None:
            self.pnAdd.SetNode(node)
    def GetNode(self,node=None):
        self.pn.GetNode(node)
        if self.pnAdd is not None:
            self.pnAdd.GetNode(node)
    def OnCbApplyButton(self,evt):
        try:
            self.__logDebug__(''%())
            if hasattr(self.pn,'__Close__'):
                self.pn.__Close__()
            else:
                self.pn.Close()
            self.pn.GetNode(None)
            if self.pnAdd is not None:
                if hasattr(self.pnAdd,'__Close__'):
                    self.pnAdd.__Close__()
                else:
                    self.pnAdd.Close()
                self.pnAdd.GetNode(None)
            if self.IsModal():
                self.EndModal(1)
            else:
                self.Show(False)
            self.Clear()
        except:
            self.__logTB__()
    def OnCbCancelButton(self,evt):
        try:
            self.__logDebug__(''%())
            if hasattr(self.pn,'__Close__'):
                self.pn.__Close__()
            else:
                self.pn.Close()
            if self.pnAdd is not None:
                if hasattr(self.pnAdd,'__Close__'):
                    self.pnAdd.__Close__()
                else:
                    self.pnAdd.Close()
            if self.IsModal():
                self.EndModal(0)
            else:
                self.Show(False)
            self.Clear()
        except:
            self.__logTB__()
    def __getattr__(self,name):
        if hasattr(self,'pn'):
            if hasattr(self.pn,name):
                return getattr(self.pn,name)
        raise AttributeError(name)
