#----------------------------------------------------------------------------
# Name:         vtXmlDomNetSim.py
# Purpose:      mixin class to emulate networking features
#
# Author:       Walter Obweger
#
# Created:      20080202
# CVS-ID:       $Id: vtXmlDomNetSim.py,v 1.9 2011/01/16 22:18:48 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
#from vidarc.tool.vtThread import vtThreadWX

class vtXmlDomNetSim:
    def __init__(self,appl='',pos=(0,0),size=(16,16),synch=False,small=True,verbose=0):
    #def __init__(self):
        vtLog.vtLngCurCls(vtLog.DEBUG,''%(),self)
        self.oLogin=None
        #self.thdProc=vtThreadWX(None,bPost=False,
        #        origin=u':'.join([self.GetOrigin(),u'thdProc']))
    def __del__(self):
        pass
    def getNodeAuditSec(self,node):
        if node is None:
            id=-1
        else:
            id=self.getKeyNum(node)
        if self.oLogin is not None:
            return u''.join([unicode(id),u'.',self.oLogin.GetUsr(),
                    ':',unicode(self.oLogin.GetUsrId())])
        else:
            return u''.join([unicode(id),u'.',u'__',':',unicode(-1)])
    def startEdit(self,node):
        try:
            if self.audit.isEnabled():
                self.audit.write('startEdit',None,self.GetNodeXmlContent(node,False,False),
                        origin=self.getNodeAuditSec(node))
        except:
            self.__logTB__()
    def doEdit(self,node,idLock=None):
        try:
            if self.audit.isEnabled():
                self.audit.write('doEdit',self.GetNodeXmlContent(node,False,False),
                        origin=self.getNodeAuditSec(node))
        except:
            self.__logTB__()
    def endEdit(self,node):
        try:
            if self.audit.isEnabled():
                self.audit.write('endEdit',self.GetNodeXmlContent(node,False,False),
                        origin=self.getNodeAuditSec(node))
        except:
            self.__logTB__()
    def __procAddNode2auditTrail__(self,node,par):
        idPar=self.getKey(par)
        id=self.getKey(node)
        self.audit.write('addNode:%s to:%s'%(id,idPar),self.GetNodeXmlContent(node,False,False),
                        origin=self.getNodeAuditSec(node))
        return 0
    def addNode(self,par,node,func=None,**kwargs):
        if node is None:
            return
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'parId:%s'%(self.getKey(par)),self.GetOrigin())
        #self.ids.AddNewNode(node)
        
        if self.__isSkip__(node):
            return
        self.__setModified__()
        self.ids.CheckId(node,self)
        self.__checkIds__(node)
        self.processMissingId()
        self.clearMissingId()
        self.updateParentDict(par,node)
        if func is not None:
            func(par,node,**kwargs)
        try:
            if self.audit.isEnabled():
                idPar=self.getKey(par)
                id=self.getKey(node)
                self.audit.write('addNode:%s to:%s'%(id,idPar),self.GetNodeXmlContent(node,False,False),
                        origin=self.getNodeAuditSec(node))
                self.procChildsKeysRec(10,node,self.__procAddNode2auditTrail__,node)
        except:
            self.__logTB__()
    def delNode(self,node):
        try:
            if self.audit.isEnabled():
                self.audit.write('delNode',self.GetNodeXmlContent(node,False,False),
                        origin=self.getNodeAuditSec(node))
        except:
            self.__logTB__()
        self.deleteNode(node)
    def moveNode(self,par,node):
        try:
            if self.audit.isEnabled():
                self.audit.write('moveNode:%d from:%d to:%d'%\
                        (self.getKeyNum(node),self.getKeyNum(self.getParent(node)),self.getKeyNum(par),),\
                        self.GetNodeXmlContent(node,False,False),
                        origin=self.getNodeAuditSec(node))
        except:
            self.__logTB__()
        self.unlinkNode(node,self.getParent(node))       # 080204 wro: provide parent node
        self.appendChild(par,node)
        self.__setModified__()
    def IsLocked(self,node):
        return True
    def GetLoggedInSecLv(self):
        if self.oLogin is None:
            return -1
        return self.oLogin.GetSecLv()
    def IsLoggedInSelf(self,node):
        return False
    def NotifyOpenStart(self,fn):
        pass
    def NotifyOpenOk(self,fn):
        pass
    def NotifyOpenFault(self,fn,sExp):
        pass
    def ClearAutoFN(self):
        return 1
    def SetAppl(self,appl,fixed=True):
        self.appl=appl
