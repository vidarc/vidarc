#----------------------------------------------------------------------------
# Name:         vtXmlGrpTreeList.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtXmlGrpTreeList.py,v 1.19 2010/02/13 22:45:08 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import wx
import wx.gizmos

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

import vidarc.tool.xml.vtXmlDom as vtXmlDom

from vidarc.tool.xml.vtXmlGrpTree import *
from vidarc.tool.xml.vtXmlTree import vtXmlTreeThread

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.tool.art.vtArt as vtArt

import types
import traceback
import fnmatch,time
import thread,threading

import images

class vtXmlGrpTreeListThread(vtXmlTreeThread):
    def __chkElem__(self,node,trWid,doc):
        if doc.IsSkip(node):
            return 0
        if trWid.__is2block__(node):
            return 0
        if trWid.__is2skip__(node,doc.getTagName(node)):
            return 0
        self.bHasChilds2Add=True
        return -1

class vtXmlGrpTreeList(wx.gizmos.TreeListCtrl,vtXmlGrpTree):
    REL_SIZE_MAIN_COLUMN=0.3
    def __init__(self, parent, id, pos, size, style, name,cols,
                    master=False,controller=False,
                    gui_update=False,verbose=0):
        if id<0:
            id=wx.NewId()
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        vtXmlDomConsumer.__init__(self)
        wx.gizmos.TreeListCtrl.__init__(self,id=id, name=name,
              parent=parent, pos=pos, size=size,style=style|wx.TR_HAS_BUTTONS)
        self.__init_properties__()
        #self.SetAutoLayout(True)
        wx.EVT_TREE_SEL_CHANGED(self, id,
              self.OnTcNodeTreeSelChanged)
        #EVT_RIGHT_DOWN(self, id, self.OnTcNodeRightDown)
        self.GetMainWindow().Bind(wx.EVT_RIGHT_DOWN,self.OnTcNodeRightDown)
        #if STORE_EXPAND==1:
        #    EVT_TREE_ITEM_COLLAPSED(self,id,self.OnTcNodeCollapes)
        #    EVT_TREE_ITEM_EXPANDED(self,id,self.OnTcNodeExpanded)
        wx.EVT_TREE_ITEM_COLLAPSED(self,id,self.OnTcNodeCollapes)
        wx.EVT_TREE_ITEM_EXPANDED(self,id,self.OnTcNodeExpanded)
        self.__init_properties__()
        self.doc=None
        self.rootNode=None
        self.objTreeItemSel=None
        self.triSelected=None
        self.dlgAdd=None
        self.dlgRename=None
        
        self.tiCache=None
        #self.prjAttr=ATTRS
        self.rootNode=None
        self.label=[]
        self.nodeInfos=['tag','name','|id','|iid','|expand']
        self.nodeKey='|id'
        self.groupItems={}
        self.bGrouping=False
        
        self.lang=None
        self.language=None
        self.languages=[]
        self.langIds=[]
        self.dlgElem=None
        self.dlgElemEdit=None
        self.dlgViewElementsTable=None
        self.dlgInstance=None
        self.dlgVsas=None
        self.dlgInstShow=None
        self.dlgFilter=None
        self.nodeCut=None
        self.triCut=None
        self.nodeCopy=None
        self.tiRoot=None
        self.iBlockTreeSel=0
        self.bMaster=master
        self.bNotifyAddThread=False
        self.bController=controller
        self.bNetResponse=True
        self.iLevelLimit=-1
        self.iLevelAutoBuild=-1
        self.verbose=verbose
        
        #vtLog.CallStack(verbose)
        #self.thdAddElements=thdAddElementsTreeList(self,verbose=verbose-1)
        #self.thdAddElements=
        self.thdProc=vtXmlGrpTreeListThread(self,bPost=True,verbose=verbose-1)
        #self.thdAddElements=thdAddElementsInst(self)
        self.bGuiUpdate=gui_update
        self.bAddElement=False
        
        self.SetDftGrouping()
        
        self.bStoreExpand=False
        self.sExpandName='expand'
        
        self.lColumn=[]
        self.lColumnItem=None
        self.cols=cols
        iLen=len(self.cols)-1
        
        for it in cols:
            #wx.gizmos.TreeListCtrl.AddColumn(self,it)
            if type(it)==types.TupleType:
                self.lColumn.append(it)
                self.AddColumn(it[0])
            else:
                self.AddColumn(it)
                self.lColumn.append((it,-(1.0-self.REL_SIZE_MAIN_COLUMN)/iLen))
        self.SetMainColumn(0)
        self.SetColumnWidth(0,130)
        self.iMainColumnSize=None#130
        self.lColumn[0]=(self.lColumn[0][0],130)
        del self.lColumn[0]
        if self.bNetResponse:
            self.EnableCache()
        self.SetupImageList()
        self.Bind(wx.EVT_SIZE, self.OnCmpSize)
    def __init_properties__(self):
        vtXmlGrpTree.__init_properties__(self)
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        try:
            del self.languages
            del self.langIds
            #del self.lMenuLangIndication
            del self.grouping
            del self.label
            del self.nodeInfos
            del self.groupItems
            #del self.grpMenu
            #del self.lMenuGroupIndication
            #del self.lAnalyse
            #del self.thdProc
        except:
            #vtLog.vtLngTB('del')
            pass
        #vtXmlGrpTree.__del__(self)
    def Clear(self):
        wx.gizmos.TreeListCtrl.DeleteAllItems(self)
    def AddColumn(self,text=''):
        wx.gizmos.TreeListCtrl.AddColumn(self,text)
    def SetColumnAlignment(self,iCol,iAlign):
        if wx.VERSION < (2,8):
            if iAlign==wx.ALIGN_LEFT:
                iAlign=wx.LIST_FORMAT_LEFT
            elif iAlign==wx.ALIGN_CENTRE:
                #iAlign=wx.LIST_FORMAT_CENTRE
                iAlign=wx.LIST_FORMAT_RIGHT
            elif iAlign==wx.ALIGN_RIGHT:
                iAlign=wx.LIST_FORMAT_RIGHT
            else:
                self.__logError__('known format')
                return
            wx.gizmos.TreeListCtrl.SetColumnAlignment(self,iCol,iAlign)
        else:
            wx.gizmos.TreeListCtrl.SetColumnAlignment(self,iCol,iAlign)
    def SetColumnWidtho(self,iCol,iW):
        try:
            if iCol>0:
                iCol-=1
                self.lColumn[iCol]=(self.lColumn[iCol][0],iW)
                wx.gizmos.TreeListCtrl.SetColumnWidth(self,iCol,iW)
            else:
                wx.gizmos.TreeListCtrl.SetColumnWidth(self,iCol,iW)
        except:
            vtLog.vtLngTB(self.GetName())
    def SetColumnAlign(self,iCol,iAlign):
        if iAlign==0:
            self.SetColumnAlignment(iCol,wx.ALIGN_LEFT)
        elif iAlign==1:
            self.SetColumnAlignment(iCol,wx.ALIGN_CENTRE)
        elif iAlign==2:
            self.SetColumnAlignment(iCol,wx.ALIGN_RIGHT)
        else:
            return
    def SetDftGrouping(self):
        self.grouping=[]
        self.label=[[('tag','')],[('name','')],[(None,'')]]
        self.attrs=['*']
        self.bGrouping=False
    def SetAttrs(self,attrs):
        self.attrs=attrs
    def SetLevelAutoBuild(self,iLevelAutoBuild):
        self.iLevelAutoBuild=iLevelAutoBuild
    def __setupImageList__(self):
        bRet=vtXmlGrpTree.__setupImageList__(self)
        if bRet:
            if 'attr' not in self.imgDict:
                self.imgDict['attr']={}
            if 'dft' not in self.imgDict['attr']:
                img=images.getAttributeBitmap()
                self.imgDict['attr']['dft']=[self.imgLstTyp.Add(img)]
                img=images.getAttributeSelBitmap()
                self.imgDict['attr']['dft'].append(self.imgLstTyp.Add(img))
                
                img=images.getAttributeImage()
                img=self.__convImg__(img,self.MAP_IMG_ALPHA[2])
                self.imgDict['attr']['dft'].append(self.imgLstTyp.Add(img))
                img=images.getAttributeSelImage()
                img=self.__convImg__(img,self.MAP_IMG_ALPHA[3])
                self.imgDict['attr']['dft'].append(self.imgLstTyp.Add(img))
                
                img=images.getAttributeImage()
                img=self.__convImg__(img,self.MAP_IMG_ALPHA[4])
                self.imgDict['attr']['dft'].append(self.imgLstTyp.Add(img))
                img=images.getAttributeSelImage()
                img=self.__convImg__(img,self.MAP_IMG_ALPHA[5])
                self.imgDict['attr']['dft'].append(self.imgLstTyp.Add(img))
        return bRet
    def __addRootByInfos__(self,o,tagName,infos):
        try:
            img=self.imgDict['elem'][tagName][0]
        except:
            img=self.imgDict['elem']['dft'][0]
        try:
            imgSel=self.imgDict['elem'][tagName][1]
        except:
            imgSel=self.imgDict['elem']['dft'][1]
        i=0
        for lbls in self.label:
            sLabel=''
            for v in lbls:
                if v[0] is None:
                    val=tagName
                else:
                    val=infos[v[0]]
                val=self.__getValFormat__(v,val)
                sLabel=sLabel+val+' '
            if i==0:
                # check project entry type
                #tid=wx.TreeItemData()
                #tid.SetData(o)
                tid=wx.TreeItemData()
                if self.TREE_DATA_ID:
                    tid.SetData(self.doc.getKeyNum(o))
                else:
                    tid.SetData(o)
                tn=self.AddRoot(sLabel,-1,-1,tid)
                if self.tiCache is not None:
                    self.tiCache.AddID(tn,infos['|id'])
                try:
                    if infos['|iid']!='':
                        self.SetItemBold(tn)
                        img=self.imgDict['elem'][tagName][2]
                except:
                    pass
            #else:
            #    self.SetItemText(tn,sLabel,i)
            #    pass
            i+=1
        try:
            self.SetItemImage(tn,img,which=wx.TreeItemIcon_Normal)
        except:
            print traceback.print_exc()
            print 'parent tree item',tnparent
            print 'tagname:%s infos:%s'%(tagName,infos)
            print 'tree item',tn
        self.tiRoot=tn
        return tn
    def __addElementByInfos__(self,parent,o,tagName,infos,bRet=False):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;key:%s'%(tagName,self.doc.getKey(o)),self)
        try:
            if self.__is2add__(o,tagName)==False:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;key:%s is not to add'%(tagName,self.doc.getKey(o)),self)
                if bRet:
                    return parent
                else:
                    return
            if self.__is2skip__(o,tagName):
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;key:%s is to skip'%(tagName,self.doc.getKey(o)),self)
                if bRet:
                    return parent
                else:
                    return
            imgTup=self.__getImagesByInfos__(tagName,infos)
            tnparent=self.__addGroupingByInfos__(parent,o,tagName,infos)
        except:
            vtLog.vtLngTB(self.GetName())
            tnparent=parent
        sLabel=''
        if type(self.label)==types.DictType:
            try:
                label=self.label[tagName]
            except:
                try:
                    label=self.label[None]
                except:
                    vtLog.vtLngTB(self.GetName())
        else:
            label=self.label
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;label:%s'%(tagName,repr(label)),self)
        #if type(label) in [types.FunctionType,types.MethodType]:
        #    sLabel=label(parent=parent,node=o,tagName=tagName,infos=infos)
        #else:
        #    for v in label:
        #        if v[0] in infos:
        #            val=infos[v[0]]
        #            val=self.__getValFormat__(v,val)
        #            sLabel=sLabel+val+' '
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'sLabel:%s'%(sLabel),self)
        i=0
        iOfs=0
        for lbls in label:
            sLabel=''
            def getLblVal(v,infos):
                if type(v)==types.UnicodeType:
                    return v
                elif v[0] is None:
                    return tagName
                elif v[0] in infos:
                    return self.__getValFormat__(v,infos[v[0]])
                return ''
            sLabel=u' '.join([getLblVal(v,infos) for v in lbls]+[''])
            if i==0:
                # check project entry type
                tid=wx.TreeItemData()
                #tid.SetData(o)
                if self.TREE_DATA_ID:
                    tid.SetData(self.doc.getKeyNum(o))
                else:
                    tid.SetData(o)
                tn=self.AppendItem(tnparent,sLabel,-1,-1,tid)
                if self.tiCache is not None:
                    self.tiCache.AddID(tn,infos['|id'])
                try:
                    if self.isNodeInstance(o,infos):
                        iOfs=2
                        self.SetItemBold(tn)
                    if self.isNodeInvalid(o,infos):
                        iOfs=4
                    self.SetItemImage(tn,imgTup[iOfs+0],which=wx.TreeItemIcon_Normal)
                except:
                    pass
            else:
                self.SetItemText(tn,sLabel,i)
                pass
            i+=1
        # check project entry type
        # add grouping sub nodes here
        self.tiLast=tn
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;key:%s added'%(tagName,self.doc.getKey(o)),self)
        if bRet==True:
            return tn
    def __refreshLabelByInfos__(self,id,node,tagName,infos):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;infos:%s'%(tagName,vtLog.pformat(infos)),self)
        tup=self.__findNodeByID__(None,id)
        if tup is None:
            return None
        ti=tup[0]
        parent=self.GetItemParent(ti)
        if self.TREE_DATA_ID and (parent.IsOk()==True):
            id=self.GetPyData(parent)
            if id is None:      # 070328:wro grouping node
                while 1:
                    par=self.GetItemParent(parent)
                    if par.IsOk()==False:
                        break
                    parent=par
                    id=self.GetPyData(parent)
                    if id is not None:
                        break
        imgTup=self.__getImagesByInfos__(tagName,infos)
        try:
            imgTup=self.__getImagesByInfos__(tagName,infos)
            tnparent=self.__addGroupingByInfos__(parent,node,tagName,infos)
        except:
            vtLog.vtLngTB(self.GetName())
            tnparent=parent
        if self.bGrouping and 0:
            parNode=self.doc.getParent(node)
            parId=self.doc.getAttribute(parNode,self.doc.attr)
            try:
                grpItem=self.groupItems[parId]
            except:
                grpItem={}
                self.groupItems[parId]=grpItem
            #tnparent=self.tiRoot
            parent=ti
            for i in range(len(self.grouping)):
                parent=self.GetItemParent(parent)
            tnparent=self.GetItemParent(parent)
            for g in self.grouping:
                val=infos[g[0]]
                val=self.__getValFormat__(g,val,infos)
                imgTuple=self.__getImgFormat__(g,val)
                try:
                    act=grpItem[val]
                    grpItem=act[1]
                    tnparent=act[0]
                except:
                    tid=wx.TreeItemData()
                    tid.SetData(None)
                    
                    tn=self.AppendItem(tnparent,val,-1,-1,tid)
                    self.SetItemImage(tn,imgTuple[0],which=wx.TreeItemIcon_Normal)
                    
                    act=(tn,{})
                    grpItem[val]=act
                    grpItem=act[1]
                    tnparent=tn
            imgTup=self.__getImagesByInfos__(tagName,infos)
        else:
            #tnparent=self.GetItemParent(ti)
            pass
        if type(self.label)==types.DictType:
            try:
                label=self.label[tagName]
            except:
                try:
                    label=self.label[None]
                except:
                    vtLog.vtLngTB(self.GetName())
        else:
            label=self.label
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;label:%s'%(tagName,repr(label)),self)
        i=0
        iOfs=0
        for lbls in label:
            sLabel=''
            #for v in lbls:
            #    if type(v)==types.UnicodeType:
            #        sLabel=sLabel+v+' '
            #    elif v[0] is None:
            #        sLabel=sLabel+tagName+' '
            #    elif v[0] in infos:
            #        val=infos[v[0]]
            #        val=self.__getValFormat__(v,val)
            #        sLabel=sLabel+val+' '
            def getLblVal(v,infos):
                if type(v)==types.UnicodeType:
                    return v
                elif v[0] is None:
                    return tagName
                elif v[0] in infos:
                    return self.__getValFormat__(v,infos[v[0]])
                return ''
            sLabel=u' '.join([getLblVal(v,infos) for v in lbls]+[''])
            if i==0:
                # check project entry type
                #tid=wx.TreeItemData()
                #tid.SetData(o)
                try:
                    img=imgTup[iOfs+0]
                    imgSel=imgTup[iOfs+1]
                except:
                    vtLog.vtLngTB(self.GetName())
                tnOldParent=self.GetItemParent(ti)
                if tnOldParent==tnparent:
                    # check project entry type
                    self.SetItemText(ti,sLabel)
                    imgOld=self.GetItemImage(ti,wx.TreeItemIcon_Normal)
                    if imgOld!=img:
                        self.SetItemImage(ti,img,wx.TreeItemIcon_Normal)
                    imgOld=self.GetItemImage(ti,wx.TreeItemIcon_Selected)
                    if imgOld!=imgSel:
                        self.SetItemImage(ti,imgSel,wx.TreeItemIcon_Selected)
                    tn=ti
                else:
                    tid=wx.TreeItemData()
                    if self.TREE_DATA_ID:
                        tid.SetData(self.doc.getKeyNum(node))
                    else:
                        tid.SetData(node)
                    tn=self.AppendItem(tnparent,sLabel,-1,-1,tid)
                    if self.tiCache is not None:
                        self.tiCache.AddID(tn,infos['|id'])
                try:
                    #if infos['|iid']!='':
                    #    self.SetItemBold(tn)
                    #    img=self.imgDict['elem'][tagName][2]
                    if self.isNodeInstance(node,infos):
                        iOfs=2
                        self.SetItemBold(tn)
                    if self.isNodeInvalid(node,infos):
                        iOfs=4
                    self.SetItemImage(tn,imgTup[iOfs+0],which=wx.TreeItemIcon_Normal)
                except:
                    pass
            else:
                self.SetItemText(tn,sLabel,i)
                pass
            i+=1
        #try:
        #    self.SetItemImage(tn,img,which=wx.TreeItemIcon_Normal)
        #except:
        #    print traceback.print_exc()
        #    print 'parent tree item',tnparent
        #    print 'tagname:%s infos:%s'%(tagName,infos)
        #    print 'tree item',tn
        return tn

    def FindTreeItem(self,node2find):
        if len(self.doc.getAttribute(node2find,'id'))==0:
            parNode=self.doc.getParent(node2find)
            ti=vtXmlGrpTree.FindTreeItem(self,parNode)
            ti=self.FindAttr(ti,node2find)
        else:
            ti=vtXmlGrpTree.FindTreeItem(self,node2find)
        return ti
    def SetNode(self,node):
        vtXmlGrpTree.SetNode(self,node)
        #self.__refreshLabel__(self.tiRoot)
        
    def SetValue(self,col,val,node=None):
        if node is None:
            ti=self.triSelected
        else:
            ti=self.FindTreeItem(node)
        self.SetTreeItemValue(ti,col,val)
    def SetTreeItemValue(self,ti,col,val):
        if ti is None:
            return -1
        try:
            self.SetItemText(ti,val,col)
        except:
            traceback.print_exc()
        return 0
    def SetTreeItemValueAll(self,ti,col,val):
        if ti is None:
            ti=self.tiRoot
        if ti is None:
            return
        self.SetItemText(ti,val,col)
        triChild=self.GetFirstChild(ti)
        while triChild[0].IsOk():
            self.SetItemText(triChild[0],val,col)
            self.SetTreeItemValueAll(triChild[0],col,val)
            triChild=self.GetNextChild(ti,triChild[1])
        return 0
    def GetValue(self,col,node=None):
        if node is None:
            ti=self.triSelected
        else:
            ti=self.FindTreeItem(node)
        return self.GetTreeItemValue(ti,col)
    def GetTreeItemValue(self,ti,col):
        if ti is None:
            return ''
        return self.GetItemText(ti,col)
    def IsBusy(self):
        if vtXmlTree.IsBusy(self):
            return True
        return False
    def Resume(self):
        vtXmlTree.Resume(self)
    def Pause(self):
        vtXmlTree.Pause(self)
    def IsRunning(self):
        if vtXmlTree.IsRunning(self):
            return True
        return False
    def IsThreadRunning(self):
        if vtXmlTree.IsThreadRunning(self):
            return True
        return False
    def Restart(self):
        vtXmlTree.Restart(self)
    def Stop(self):
        vtXmlTree.Stop(self)
    def ClearDoc(self):
        vtLog.vtLngCur(vtLog.DEBUG,''%(),self.GetOrigin())
        vtXmlTree.ClearDoc(self)
    def __calcColumnSizes__(self):
        sz=self.GetClientSize()
        try:
            iWidth=sz[0]
            iFree=iWidth
            iColumn=1
            self.Freeze()
            if self.lColumn is not None:
                for t in self.lColumn:
                    j=t[1]
                    if j<0:
                        j=-int(iWidth*j)
                    iFree-=j
                    self.SetColumnWidth(iColumn,j)
                    iColumn+=1
            if self.lColumnItem is not None:
                for t in self.lColumnItem:
                    j=t[1]
                    if j<0:
                        j=-int(iWidth*j)
                    iFree-=j
                    self.SetColumnWidth(iColumn,j)
                    iColumn+=1
            iFree-=wx.SystemSettings.GetMetric(wx.SYS_VSCROLL_X)+5*iColumn
            if self.iMainColumnSize is None:
                # apply remaining size
                j=max(iFree,int(iWidth*self.REL_SIZE_MAIN_COLUMN))
                self.SetColumnWidth(0,j)
            else:
                j=self.iMainColumnSize
                if j<0:
                    j=-int(iWidth*j)
                else:
                    j=self.iMainColumnSize
                self.SetColumnWidth(0,j)
        except:
            vtLog.vtLngTB(self.GetName())
        self.Thaw()
    def OnCmpSize(self,event):
        event.Skip()
        self.__calcColumnSizes__()
    def _HitTest(self,pos):
        tn,flags,col= self.HitTest(pos)
        return tn,flags
