#Boa:Dialog:vtXmlNodeBaseEditDialog
#----------------------------------------------------------------------------
# Name:         vtXmlNodeBaseEditDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtXmlNodeBaseEditDialog.py,v 1.9 2012/11/19 05:02:17 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
from vidarc.tool.xml.vtXmlNodeDialog import *
import vidarc.tool.lang.vtLgBase as vtLgBase

def create(parent):
    return vtXmlNodeBaseEditDialog(parent)

[wxID_VTXMLNODEBASEEDITDIALOG, wxID_VTXMLNODEBASEEDITDIALOGCBAPPLY, 
 wxID_VTXMLNODEBASEEDITDIALOGCBCANCEL, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vtXmlNodeBaseEditDialog(wx.Dialog,vtXmlNodeDialog):
    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_gbsData_Items(self, parent,row):
        # generated method, don't edit

        parent.AddSizer(self.bxsBt, (row, 0), border=4,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER, span=(1, 1))

    def _init_sizers(self,row):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=0, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_gbsData_Items(self.gbsData,row)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt,name,title,row):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTXMLNODEBASEEDITDIALOG,
              name=name, parent=prnt, pos=wx.Point(176,
              176), size=wx.Size(400, 250), style=wx.DEFAULT_DIALOG_STYLE|wx.RESIZE_BORDER,
              title=title)
        self.SetClientSize(wx.Size(392, 223))

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLNODEBASEEDITDIALOGCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'), name=u'cbApply',
              parent=self, pos=wx.Point(80, 192), size=wx.Size(96, 30),
              style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VTXMLNODEBASEEDITDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLNODEBASEEDITDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(216, 192),
              size=wx.Size(96, 30), style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VTXMLNODEBASEEDITDIALOGCBCANCEL)
        
        self._init_sizers(row)

    def __init__(self, parent,title,pnCls,name='vtXmlNodeBase',
                pos=None,sz=None,bSplitter=False,iSplitterSashPos=0,
                pnPos=wx.DefaultPosition,pnSz=wx.DefaultSize,pnStyle=0,
                pnName='pn',
                pnClsAdd=None,pnPosAdd=wx.DefaultPosition,pnSzAdd=wx.DefaultSize,pnStyleAdd=0,
                pnNameAdd=None):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        if pnClsAdd is not None:
            row=2
        else:
            row=1
        self._init_ctrls(parent,u''.join([name,'EditDialog']),title,
                row)
        
        vtXmlNodeDialog.__init__(self)
        try:
            if (bSplitter==True) and (pnClsAdd is not None):
                par=wx.SplitterWindow(self,id=wx.NewId(),
                        pos=wx.DefaultPosition,size=wx.DefaultSize,
                        style = wx.SP_LIVE_UPDATE|wx.SP_3DSASH)
                bSplit=True
                self.gbsData.AddWindow(par, (0, 0), border=4,
                    flag=wx.BOTTOM | wx.TOP | wx.LEFT | wx.RIGHT | wx.EXPAND, span=(1, 1))
            else:
                par=self
                bSplit=False
            self.gbsData.AddGrowableCol(0)
            self.gbsData.AddGrowableRow(0)
            self.pn=pnCls(par,id=wx.NewId(),pos=pnPos,size=pnSz,
                        style=pnStyle,name=pnName)
            if bSplit==False:
                iFlag=wx.BOTTOM | wx.TOP | wx.LEFT | wx.RIGHT | wx.EXPAND
                self.gbsData.AddWindow(self.pn, (0, 0), border=4,flag=iFlag, span=(1, 1))
            if pnClsAdd is not None:
                #self.gbsData.Detach(0)
                #self.gbsData.SetItemPosition(0,(2,0))
                self.pnAdd=pnClsAdd(par,id=wx.NewId(),pos=pnPosAdd,size=pnSzAdd,
                        style=pnStyleAdd,name=pnNameAdd)
                if hasattr(self.pnAdd,'GetWid'):
                    widAdd=self.pnAdd.GetWid()
                else:
                    widAdd=self.pnAdd
                if bSplit==False:
                    self.gbsData.AddGrowableRow(1)
                    iFlag=wx.BOTTOM | wx.TOP | wx.LEFT | wx.RIGHT | wx.EXPAND
                    self.gbsData.AddWindow(widAdd, (1, 0),border=4,flag=iFlag, span=(1, 1))
                else:
                    par.SplitHorizontally(self.pn,widAdd)
                    par.SetSashGravity(0)
                    par.SetSashPosition(iSplitterSashPos)
            else:
                self.pnAdd=None
        except:
            self.__logTB__()
        wx.EVT_SIZE(self,self.OnSize)
        if sz is not None:
            self.SetSize(sz)
        else:
            #self.Fit()
            self.gbsData.Layout()
            self.gbsData.Fit(self)
        if pos is not None:
            self.Move(pos)
    def OnSize(self,evt):
        evt.Skip()
        self.gbsData.Layout()
        self.Refresh()
