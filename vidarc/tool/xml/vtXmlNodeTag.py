#----------------------------------------------------------------------------
# Name:         vtXmlNodeTag.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      
# CVS-ID:       $Id: vtXmlNodeTag.py,v 1.10 2010/03/03 02:16:19 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        from vtXmlNodeTagPanel import vtXmlNodeTagPanel
        from vtXmlNodeTagEditDialog import vtXmlNodeTagEditDialog
        from vtXmlNodeTagAddDialog import vtXmlNodeTagAddDialog
        GUI=1
        vLogFallBack.logDebug('GUI objects imported',__name__)
    else:
        GUI=0
        vLogFallBack.logDebug('GUI objects import skipped',__name__)
except:
    vLogFallBack.logTB(__name__)
    GUI=0
vLogFallBack.logDebug('import;done',__name__)

class vtXmlNodeTag(vtXmlNodeBase):
    NODE_ATTRS=[
            ('Tag',None,'tag',None),
            ('Name',None,'name',None),
            ('Desc',None,'description',None),
        ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    FUNCS_GET_4_TREE=['Tag','Name']
    FMT_GET_4_TREE=[('Tag',''),('Name','')]
    def __init__(self,tagName='Tag'):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'Tag')
    # ---------------------------------------------------------
    # specific
    def GetTag(self,node):
        return self.Get(node,'tag')
    def GetName(self,node,lang=None):
        return self.GetML(node,'name',lang)
    def GetDesc(self,node,lang=None):
        return self.GetML(node,'description',lang)
    def SetTag(self,node,val):
        self.Set(node,'tag',val)
    def SetName(self,node,val,lang=None):
        self.SetML(node,'name',val,lang)
    def SetDesc(self,node,val,lang=None):
        self.SetML(node,'description',val,lang)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        return [('tag',vtXmlFilterType.FILTER_TYPE_STRING),
            ('name',vtXmlFilterType.FILTER_TYPE_STRING),
            ('description',vtXmlFilterType.FILTER_TYPE_STRING),
            ]
    def GetTranslation(self,name):
        _(u'tag'),_(u'name'),_(u'description')
        return _(name)
    def Is2Create(self):
        return True
    def Is2Add(self):
        return True
    def IsMultiple(self):
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return False
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\x0bIDAT8\x8dcddbf\xa0\x040Q\xa4\x9b\x81\x81\x81\x05\xc6\xa8\xaf\xab\
\xfd\x8fMAcS3#\xba\\cS3#\x8c\xcd\xc8\xc8\xc4\xccP_W\xfb\x1fY\x10\x9b\xa10y\
\x98\x18\x8c\xcfB\x8c&t\x8d\xf5u\xb5\xff\xe1\x962213444\xfcG\xa6\x19\x99\x98\
\x19Dm\xd5\xff#c\x988\xbaZ\x0c\x170000\x88X\xab\xfc\x97\xf4\xd2gPVTb\xb8s\
\xff.\xc3\x8fg\x1f\x18\x18\x18\x18\xfe\xbf9z\x87\x11=L\xe0\xb1\x80\x1e\x0e\
\xca\x8aJ\x0c\x01j\xb6\x0c\xec"\xbc\x0c\xff\xfe\xfc\xc3\x1a\xb8(\x06\xa0\x83\
\xcbG\xce2L:\xb9\x9a\xe1\xcb\xed\x97\x0c\x0c\xff0#\x08\xe6\n&\x98\xcd\xe8N\
\xfbx\xf1\x11\xc3\x87\x0b\x8f\x18~\xbe\xfd\xc2\xf0\xf7\xe7o\x14\x8d\xc8.E\t\
\x03dC\xa6\xee]\xce\xf0\xe9\xdaS\xb8\xdc\x9b\xa3w\x18\xb1E7F b\x8b\xef\xfa\
\xba\xda\xff\x0c\xce\x0cX\xd3\n<!!kF6\x00\x17\x80\xa9e\x84e&|\xc9\x15\x1f`\
\x1c\xf0\xdc\x08\x00*\xe8\x94\x8a?e\xed\xc1\x00\x00\x00\x00IEND\xaeB`\x82' 
  
    def getSelImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\x19IDAT8\x8d\xc5S\xbdN\xc30\x10\xfe\xce\x0e0!1\x84\x81\x05\xa9\x10\
\x95\x11\xf6(\x13\x1bSW\x9e\x81g\x80\xe6\xe7\rx\r\x1e\x82\xa9\x03\x8c\x0cL\
\xa9\xca\x02\x03d@H\x91\x90H|\x0c\xe8\x8c\xddZEj\x07>\xe9$\x9f\xefl\xdf}\xdf\
\x99Hi\xac\x03\xb5\xd6i\x00\x91,\xc6W\x97\x1cJ(\xca\x8a\xe6cEY\x91uHi\xe4y\
\xce\xa44\xc4v\xb3#\x0e\x99\xc4\xdd\xfch\xfe\xc58Mx\xef\xec\xd8\xdbc0>_\xde\
\x01\x80\x9bIMRqQV\xa4\xfe:\xecb\xe7d\x1fq\x9a\xb0\xdb\xc2B\x05\x82\xc3\xc1\
\x81]\xd7\xb3\xa9\x17s9Q\xa1M\xc1h\x98a4\xcc\xb0\x15o\xc3t\x06\xa636&U\xac,\
\xa3<\x18\t\x19\xc0\x0f\x07\xedS\x03\xda\xd0\xc0\x00\xb8\xbe\xbb\x01\x00\xb4\
\xb37\xc0\xfc\x16\xe8J\x1b\xe4\x80\xbfz<\xdc\xde[\xdft=\xb87\xa1T\xbf\x85fR\
\xd3\xc7\xe33\xd4fx\xbc\xdb\xe9+DF\x01\x91\xd2\x1e\x81EYQ\x9c&\xc1\xa9\xbc8=\
\xf7\xda\xb0\x17\x00\x8b*x\xe3\xba\x04\xf4\xef\xbf\xf1\x1ba\xearS\xd3?\xf5\
\x0b\x00\x00\x00\x00IEND\xaeB`\x82' 

    def GetEditDialogClass(self):
        if GUI:
            #return vtXmlNodeTagEditDialog
            return {'name':self.__class__.__name__,
                    'pnName':'pnNodeTag',
                }
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            #return vtXmlNodeTagAddDialog
            return {'name':self.__class__.__name__,
                    'pnName':'pnNodeTag',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vtXmlNodeTagPanel
        else:
            return None

