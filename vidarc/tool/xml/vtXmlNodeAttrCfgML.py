#----------------------------------------------------------------------------
# Name:         vtXmlNodeAttrCfgML.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      
# CVS-ID:       $Id: vtXmlNodeAttrCfgML.py,v 1.8 2012/01/29 13:03:18 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
from vidarc.tool.xml.vtXmlNodeBase import *
try:
    from vidarc.tool.input.vtInputAttrCfgMLPanel import vtInputAttrCfgMLPanel
    GUI=1
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    GUI=0
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vtXmlNodeAttrCfgML(vtXmlNodeBase):
    NODE_ATTRS=[
        #('tag',None,'tag',None),
        #('name',None,'name',None),
        #('image',None,'img',None),
        ]
    FUNCS_GET_SET_4_LST=[]#'Tag','Name']
    MAP_FILTER={
        'string':vtXmlFilterType.FILTER_TYPE_STRING,
        'choice':vtXmlFilterType.FILTER_TYPE_STRING,
        'choiceML':vtXmlFilterType.FILTER_TYPE_STRING,
        'creditcard':vtXmlFilterType.FILTER_TYPE_STRING,
        'email':vtXmlFilterType.FILTER_TYPE_STRING,
        'IP':vtXmlFilterType.FILTER_TYPE_STRING,
        'date':vtXmlFilterType.FILTER_TYPE_DATE,
        'time':vtXmlFilterType.FILTER_TYPE_TIME,
        'datetime':vtXmlFilterType.FILTER_TYPE_DATE_TIME,
        'int':vtXmlFilterType.FILTER_TYPE_INT,
        'lang':vtXmlFilterType.FILTER_TYPE_LONG,
        'float':vtXmlFilterType.FILTER_TYPE_FLOAT,
        'double':vtXmlFilterType.FILTER_TYPE_FLOAT,
        'datetimerel':vtXmlFilterType.FILTER_TYPE_DATE_TIME_REL,
        }
    def __init__(self,tagName='attrML'):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'attrML')
    # ---------------------------------------------------------
    # specific
    def GetTag(self,node):
        return self.Get(node,'tag')
    def GetName(self,node):
        return self.GetML(node,'name')
    def SetTag(self,node,val):
        self.Set(node,'tag',val)
    def SetName(self,node,val):
        self.SetML(node,'name',val)
    def GetTypeDef(self,node,lang=None):
        try:
            sTag=self.GetTag(node)
            nodeTmp=self.doc.getChild(node,sTag)
            if nodeTmp is None:
                return None
            dType={}
            dTypeFallBack={}
            dTypeFull={}
            if lang is None:
                lang=self.doc.GetLang()
            for cc in self.doc.getChilds(nodeTmp):
                sTagTmp=self.doc.getTagName(cc)
                sLg=self.doc.getAttribute(cc,'language')
                sVal=self.doc.getText(cc)
                if dTypeFull.has_key(sTagTmp)==False:
                    dTypeFull[sTagTmp]={sLg:sVal}
                else:
                    dTypeFull[sTagTmp][sLg]=sVal
                if len(sLg)==0:
                    pass
                elif lang==sLg:
                    pass
                else:
                    continue
                dType[sTagTmp]=sVal
            for k in dTypeFallBack.keys():
                if dType.has_key(k)==False:
                    dType[k]=dTypeFallBack[k]
            return dType
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def GetCfgDict(self,nodeCfg,lang=None):
        try:
            if lang is None:
                lang=self.doc.GetLang()
            dCfg={}
            for c in self.doc.getChilds(nodeCfg,'attrML'):
                try:
                    iID=long(self.doc.getKey(c))
                except:
                    iID=-2
                    continue
                sTag=self.doc.getNodeText(c,'tag')
                sName=self.doc.getNodeTextLang(c,'name',lang)
                nodeTmp=self.doc.getChild(c,sTag)
                dType={}
                dTypeFallBack={}
                dTypeFull={}
                for cc in self.doc.getChilds(nodeTmp):
                    sTagTmp=self.doc.getTagName(cc)
                    sLg=self.doc.getAttribute(cc,'language')
                    sVal=self.doc.getText(cc)
                    if dTypeFull.has_key(sTagTmp)==False:
                        dTypeFull[sTagTmp]={sLg:sVal}
                    else:
                        dTypeFull[sTagTmp][sLg]=sVal
                    if len(sLg)==0:
                        pass
                    elif lang==sLg:
                        pass
                    else:
                        continue
                    dType[sTagTmp]=sVal
                for k in dTypeFallBack.keys():
                    if dType.has_key(k)==False:
                        dType[k]=dTypeFallBack[k]
                dCfg[sTag]=[sName,sTag,iID,dType,dTypeFull]
            if VERBOSE>0:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'dCfg:%s'%(vtLog.pformat(dCfg)),self.__class__.__name__)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return dCfg
    # ---------------------------------------------------------
    # inheritance
    def Build(self):
        """ this method is called automatically after the XML-file is read.
        """
        pass
    def BuildLang(self):
        """ this method is called automatically when language is changed.
        """
        pass
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        return False
    def Is2Add(self):
        return False
    def IsMultiple(self):
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return True
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x1cIDAT8\x8dcddbf\xa0\x040Q\xa4{\xd4\x80Q\x03F\r\x18D\x06\x00\x00]b\
\x00&\x87\xd5\x92\xeb\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        return None
    def GetAddDialogClass(self):
        return None
    def GetPanelClass(self):
        if GUI:
            return vtInputAttrCfgMLPanel
        else:
            return None

