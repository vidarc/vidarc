#----------------------------------------------------------------------------
# Name:         vtXmlGrpTree.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtXmlGrpTree.py,v 1.102 2015/04/27 06:43:18 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

from vidarc.tool.xml.vtXmlTree import vtXmlTree
from vidarc.tool.xml.vtXmlTree import EVT_VTXMLTREE_ITEM_SELECTED
from vidarc.tool.xml.vtXmlTree import EVT_VTXMLTREE_THREAD_ADD_ELEMENTS
from vidarc.tool.xml.vtXmlTree import EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED
from vidarc.tool.xml.vtXmlTree import EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_ABORTED
from vidarc.tool.xml.vtXmlTree import vtXmlTreeItemSelected
from vidarc.tool.xml.vtXmlTree import vtXmlTreeItemEdited
from vidarc.tool.xml.vtXmlTree import vtXmlTreeItemBuild
from vidarc.tool.xml.vtXmlTree import vtXmlTreeItemExec
from vidarc.tool.xml.vtXmlTree import vtXmlTreeThreadAddElements
from vidarc.tool.xml.vtXmlTree import vtXmlTreeThreadAddElementsAborted
from vidarc.tool.xml.vtXmlTree import vtXmlTreeThreadAddElementsFinished
import vidarc.tool.xml.vtXmlDom as vtXmlDom
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.xml.vtXmlArt as vtXmlArt

from vidarc.tool.log.vtLogAuditTrailFileViewerFrame import vtLogAuditTrailFileViewerFrame
#from vidarc.vApps.vPrj.vgdXmlDlgEditPrj import *
#from wxPython.wx import wx.PostEvent
import string,thread,traceback,time,types

from vidarc.tool.xml.vtXmlTreeAddDialog import vtXmlTreeAddDialog
from vidarc.tool.xml.vtXmlDomCmdDialog import vtXmlDomCmdDialog
from vidarc.tool.xml.vtXmlFindDialog import *
from vidarc.tool.xml.vtXmlExportNodeDialog import vtXmlExportNodeDialog
from vidarc.tool.xml.vtXmlImportOfficeDialog import vtXmlImportOfficeDialog
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.misc.vtmMsgDialog import vtmMsgDialog

import vidarc.tool.xml.images_lang as images_lang           #FIXME delete
import vidarc.tool.xml.images_treemenu as images_treemenu   #FIXME delete
import vidarc.tool.xml.images as images                     #FIXME delete
import types

try:
    from __init__ import SORT_CASESENSITVE
    if SORT_CASESENSITVE:
        def cmpTreeItems(l1,l2):
            return cmp(l1,l2)
    else:
        def cmpTreeItems(l1,l2):
            return cmp(l1.lower(),l2.lower())
except:
    vtLog.vtLngCur(vtLog.WARN,'tree sorting not defined'%(),'customize')
    def cmpTreeItems(l1,l2):
        return cmp(l1,l2)
        return cmp(l1.lower(),l2.lower())
    
# defined event for vgpXmlTree grouping changed
wxEVT_VTXMLGRPTREE_GROUPING_CHANGED=wx.NewEventType()
vEVT_VTXMLGRPTREE_GROUPING_CHANGED=wx.PyEventBinder(wxEVT_VTXMLGRPTREE_GROUPING_CHANGED,1)
def EVT_VTXMLGRPTREE_GROUPING_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTXMLGRPTREE_GROUPING_CHANGED,func)
class vtXmlGrpTreeGroupingChanged(wx.PyEvent):
    """
    Posted Events:
        Tree Item added event, reports the parent node of the added one
            EVT_VTXMLGRPTREE_GROUPING_CHANGED(<widget_name>, self.OnItemSel)
    """

    def __init__(self,obj,iGrp,sGrp):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTXMLGRPTREE_GROUPING_CHANGED)
        self.obj=obj
        self.iGrp=iGrp
        self.sGrp=sGrp
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCS(vtLog.DEBUG,self.__class__.__name__,obj.GetOrigin())
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def GetObject(self):
        return self.obj
    def GetGroupingNum(self):
        return self.iGrp
    def GetGroupingName(self):
        return self.sGrp


class vtXmlGrpTree(vtXmlTree):
    def __init__(self, parent=None, id=None, pos=None, size=None, style=0, name='trXmlGrp',
                    master=False,controller=False,verbose=0):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        vtXmlTree.__init__(self,id=id, name=name,master=master,
              controller=controller,verbose=verbose,
              parent=parent, pos=pos, size=size,style=style)
        #self.__init_properties__()
        
        self.SetDftGrouping()
        self.SetDftNodeInfos()
        #self.grouping=[('date','YYYY'),('date','MM'),('person','')]
        #self.label=[('date',''),('starttime',''),('project',''),('person','')]
        self.grpMenu=None
        self.tiRoot=None
        
        self.dlgAdd=None
        self.dlgCmd=None
        self.frmDesc=None
        self.frmAttr=None
        
        self.iBlockTreeSel=0
        
        self.tbMain=None
        self.dToolBar=None
    def __init_properties__(self):
        vtXmlTree.__init_properties__(self)
        self.lang=None
        self.languages=[]
        self.languageIds=[]
        self.lMenuLangIndication=[]
        self.rootNode=None
        self.nodeMoveTarget=None
        self.idCut=-1
        self.idCopy=-1
        self.idMoveTarget=-1
        self.grouping=[]
        self.label=[]
        self.nodeInfos=[]
        self.groupItems={}
        self.grpMenu=[]
        self.lMenuGroupIndication=[]
        self.__logDebug__(''%())
        self.lViewInt=[
                ('frmAttrEditRoot', _(u'attribute editor root'),  vtArt.getBitmap(vtArt.Attrs),
                (self.ShowAttrEdit,(),{'iBase':0})),
                ('frmAttrEditSel',  _(u'attribute editor sel'),  vtArt.getBitmap(vtArt.Attrs),
                (self.ShowAttrEdit,(),{'iBase':1})),
                
                ('frmDescEditRoot', _(u'description editor root'),  vtArt.getBitmap(vtArt.Book),
                (self.ShowDescEdit,(),{'iBase':0})),
                ('frmDescEditSel',  _(u'description editor sel'),  vtArt.getBitmap(vtArt.Book),
                (self.ShowDescEdit,(),{'iBase':1})),
                ]
        # FIXME:20150426evt
        self.dViewFrm={}            # 20150426 wro:frm need to be cached till event cleanup works
        self.lView=None
        self.lAnalyse=None
        self.popupAnalyseIds=None
        self.bGrouping=False
        self.bGroupingDict=False
        self.bGrpMenu=False
        self.bLangMenu=False
        self.bImportMenu=False
        self.bExportMenu=False
        self.bEditMenu=False
        self.bClipMenu=False
        self.bMoveMenu=False
        self.bMiscMenu=True
        self.bSortTagName=True
        self.iGrp=-1
        
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        try:
            del self.languages
            del self.languageIds
            del self.lMenuLangIndication
            del self.grouping
            del self.label
            del self.nodeInfos
            del self.groupItems
            del self.grpMenu
            del self.lMenuGroupIndication
            del self.lAnalyse
        except:
            #vtLog.vtLngTB(self.__class__.__name__)
            pass
        vtXmlTree.__del__(self)
        #global IMG_LIST_GRP
        #global IMG_DICT_GRP
        #if self.__class__.__name__ in IMG_LIST_GRP:
        #    del IMG_LIST_GRP[self.__class__.__name__]
        #if self.__class__.__name__ in IMG_DICT_GRP:
        #    d=IMG_DICT_GRP[self.__class__.__name__]
        #    del IMG_DICT_GRP[self.__class__.__name__]
        #vtLog.vtLngCurCls(vtLog.DEBUG,'class:%s'%(self.__class__.__name__),self)
    def Destroy(self):
        return vtXmlTree.Destroy(self)
    def Close(self,bForce=False):
        vtXmlTree.Close(self,bForce)
    def EnableMenu(self,name,flag=True):
        try:
            #if hasattr(self,'b%sMenu'%name):
            setattr(self,'b%sMenu'%name,flag)
        except:
            vtLog.vtLngTB(self.GetName())
    def SetDftGrouping(self):
        self.grouping=[]
        self.label=[]
        self.bGrouping=False
    def SetDftNodeInfos(self):
        self.nodeInfos=[]
        self.nodeKey='|key'
    def SetPossibleGrouping(self,grps):
        self.grpMenu=grps
        self.bGrpMenu=len(grps)>0
        self.iGrp=-1
        self.SetGroupingByNum(0)
    def __postGrouping__(self,iGrp,sGrp):
        wx.PostEvent(self,vtXmlGrpTreeGroupingChanged(self,iGrp,sGrp))
    def GetGroupingName(self):
        try:
            if self.iGrp>=0:
                tup=self.grpMenu[self.iGrp]
                return tup[0]
        except:
            pass
        return None
    def SetGroupingByName(self,sGrp):
        if self.grpMenu is None:
            return False
        i=0
        try:
            for tup in self.grpMenu:
                if tup[0]==sGrp:
                    if self.iGrp==i:
                        return False
                    self.iGrp=i
                    self.SetGrouping(tup[2],tup[3])
                    self.__postGrouping__(self.iGrp,tup[0])
                    return True
                i+=1
            return False
        except:
            return False
    def SetGroupingByNum(self,iGrp):
        if self.iGrp==iGrp:
            return False
        if self.grpMenu is None:
            return False
        if iGrp<0:
            return False
        if len(self.grpMenu)+1<iGrp:
            return False
        try:
            tup=self.grpMenu[iGrp]
            self.iGrp=iGrp
            self.SetGrouping(tup[2],tup[3])
            self.__postGrouping__(self.iGrp,tup[0])
            return True
        except:
            return False
                
    
    def SetNodeInfos(self,lst,key='|id'):
        """['project','task','subtask','loc','person',
                        'desc','date','starttime','endtime']
                        """
        self.nodeInfos=lst
        self.nodeKey=key
        try:
            i=self.nodeInfos.index(self.nodeKey)
        except:
            self.nodeInfos.append(self.nodeKey)
        
    def SetGrouping(self,grpLst,lblLst):
        """[('date','YYYY'),('date','MM'),('date','DD'),('person','')]
        [('date','YYYY-MM-DD'),('starttime','HH:MM'),('project',''),('person','')]
        or
        {'user':[('date','YYYY'),('date','MM'),('date','DD'),('person','')]}
        {'user':[('date','YYYY-MM-DD'),('starttime','HH:MM'),('project',''),('person','')]}
        """
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'grp:%s;lbl:%s'%(vtLog.pformat(grpLst),vtLog.pformat(lblLst)),
                        self)
        except:
            vtLog.vtLngTB('')
        self.bGrouping=False
        self.bGroupingDict=False
        self.grouping=grpLst
        self.label=lblLst
        if type(self.grouping)==types.DictionaryType:
            if len(self.grouping.keys())>0:
                self.bGrouping=True
                #self.bGrpMenu=True
                self.bGroupingDict=True
        else:
            if len(self.grouping)>0:
                self.bGrouping=True
                #self.bGrpMenu=True
    def OnCompareItems(self,ti1,ti2):
        if self.TREE_DATA_ID:
            n1=self.doc.getNodeByIdNum(self.GetPyData(ti1))
            n2=self.doc.getNodeByIdNum(self.GetPyData(ti2))
        else:
            n1=self.GetPyData(ti1)
            n2=self.GetPyData(ti2)
            #vtLog.CallStack('')
            #print self.TREE_BUILD_ON_DEMAND
            if type(n1)==types.LongType:
                return 0
            if type(n2)==types.LongType:
                return 0
        if (n1 is not None and n2 is not None):
            s1=self.doc.getTagName(n1)
            s2=self.doc.getTagName(n2)
            if s1=='cfg':
                return 1
            if s2=='cfg':
                return -1
            if self.bSortTagName:
                i=cmp(s1,s2)
                if i!=0:
                    return i
            l1=self.GetItemText(ti1)
            l2=self.GetItemText(ti2)
            return cmpTreeItems(l1,l2)
            return cmp(l1,l2)
            return cmp(l1.lower(),l2.lower())
            lst1=[]
            lst2=[]
            for l in self.label:
                try:
                    s1=self.doc.getNodeText(n1,l[0])
                except:
                    s1=''
                try:
                    s2=self.doc.getNodeText(n2,l[0])
                except:
                    s2=''
                lst1.append(s1)
                lst2.append(s2)
            for s1,s2 in zip(lst1,lst2):
                i=cmp(s1,s2)
                if i!=0:
                    return i
        else:
            #if n1 is None:
            #    return 1
            #else:
                l1=self.GetItemText(ti1)
                l2=self.GetItemText(ti2)
                return cmpTreeItems(l1,l2)
                return cmp(l1,l2)
                return cmp(l1.lower(),l2.lower())
        return 0
    def SetLanguages(self,languages,languageIds):
        self.languages=languages
        self.languageIds=languageIds
    def SetView(self,lView=None):
        """ [ (view0 , _('view0'), bmp0 , (func , args ,kwargs)) ,
              (view1 , _('view1'), bmp1 , (func , args ,kwargs)) ,
              (view2 , _('view2'), bmp2 , [
                (view21 , _('view21'), bmp21 , (func , args ,kwargs)) , 
                (view22 , _('view22'), bmp22 , (func , args ,kwargs)) ,
                ]
        ]
        """
        self.lView=lView
    def SetAnalyse(self,lAnalyse=None):
        """ [ (analyse0 , _('analyse0'), bmp0 , (func , args ,kwargs)) ,
              (analyse1 , _('analyse1'), bmp1 , (func , args ,kwargs)) ,
              (analyse2 , _('analyse2'), bmp2 , [
                (analyse21 , _('analyse21'), bmp21 , (func , args ,kwargs)) , 
                (analyse22 , _('analyse22'), bmp22 , (func , args ,kwargs)) ,
                ]
        ]
        """
        self.lAnalyse=lAnalyse
    def EnableLanguageMenu(self,flag=True):
        self.bLangMenu=flag
    def EnableImportMenu(self,flag=True):
        self.bImportMenu=flag
    def EnableExportMenu(self,flag=True):
        self.bExportMenu=flag
    def EnableClipMenu(self,flag=True):
        self.bClipMenu=flag
    def EnableMoveMenu(self,flag=True):
        self.bMoveMenu=flag

    def SetupImageList(self):
        bRet=self.__setupImageList__()
        if bRet:
            #self.__extendImageListRegNodes__()
            self.__processImgList__()
        return bRet
    def __setupImageList__(self):
        if vtXmlTree.__setupImageList__(self):
            return True
        else:
            return False
    def getGroupingOverlayImages(self):
        return vtXmlArt.getImage('GrpOvl'),vtXmlArt.getImage('GrpOvl')

    def __processImgList__(self):
        try:
            vtXmlTree.__processImgList__(self)
            keysElem=self.imgDict['elem'].keys()
            keysGrp=self.imgDict['grp'].keys()
            for k in keysElem:
                try:
                    keysGrp.remove(k)
                except:
                    keysGrp.append(k)
            for k in keysGrp:
                try:
                    imgTupleElem=self.imgDict['elem'][k]
                    if len(imgTupleElem)>=4:
                        iOfs=2
                    else:
                        iOfs=0
                except:
                    continue
                try:
                    imgTupleGrp=self.imgDict['grp'][k]
                except:
                    imgTupleGrp=[]
                imgGrp=self.getGroupingOverlayImages()
                for i in range(len(imgTupleGrp),2):
                    bmp=self.imgLstTyp.GetBitmap(imgTupleElem[i+iOfs])
                    img=self.__buildGrpBmp__(bmp,imgGrp[i])
                    imgTupleGrp.append(self.imgLstTyp.Add(img))
                self.imgDict['grp'][k]=imgTupleGrp
            #print self.imgDict['grp'][k]
        except:
            #vtLog.vtLngTB(self.GetName())
            pass
    def __addElement__(self,parent,o,bRet=False):
        self.doc.acquire()
        if 1==0:
            infos={}
            for tagName in self.nodeInfos:
                tags=string.split(tagName,':')
                nodeTmpl=o
                for tag in tags[:-1]:
                    #nodeTmpl=vtXmlDomTree.getNode(nodeTmpl,tag)
                    nodeTmpl=self.doc.getChildLang(nodeTmpl,tag,self.lang)
                sub=string.split(tags[-1],'|')
                if len(sub)==2:
                    if len(sub[0])>0:
                        #nodeTmpl=vtXmlDomTree.getNode(nodeTmpl,sub[0])
                        nodeTmpl=self.doc.getChildLang(nodeTmpl,sub[0],self.lang)
                    infos[tagName]=self.doc.getAttribute(nodeTmpl,sub[1])
                else:
                    #infos[tagName]=vtXmlDomTree.getNodeText(nodeTmpl,tags[-1])
                    infos[tagName]=self.doc.getNodeTextLang(nodeTmpl,tags[-1],self.lang)
            tagName=self.doc.getTagName(o)
        else:
            tagName,infos=self.doc.getNodeInfos(o,self.lang)
        ret=self.__addElementByInfos__(parent,o,tagName,infos,bRet)
        self.doc.release()
        if bRet:
            return ret

    def __addRootByInfos__(self,node,tagName,infos):
        if node is not None and self.doc.IsNodeKeyValid(node):
            try:
                if VERBOSE>5:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurWX(vtLog.DEBUG,'id:%s;tag:%s;infos:%s'%\
                                    (self.doc.getKey(node),tagName,vtLog.pformat(infos)),self)
                #if self.doc.acquire(blocking=False):
                try:
                    imgTup=self.__getImagesByInfos__(tagName,infos)
                    vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                    tid=wx.TreeItemData()
                    if self.TREE_DATA_ID:
                        tid.SetData(self.doc.getKeyNum(node))
                    else:
                        tid.SetData(node)
                    sLabel=''
                    if type(self.label)==types.DictType:
                        try:
                            label=self.label[tagName]
                        except:
                            try:
                                label=self.label[None]
                            except:
                                vtLog.vtLngTB(self.GetName())
                    else:
                        label=self.label
                    if VERBOSE>5:
                        vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%(repr(imgTup)),self)
                    if type(label) in [types.FunctionType,types.MethodType]:
                        sLabel=label(parent=None,node=node,tagName=tagName,infos=infos)
                    else:
                        for v in label:
                            if v[0] in infos:
                                val=infos[v[0]]
                                val=self.__getValFormat__(v,val)
                                sLabel=sLabel+val+' '
                    if len(sLabel)==0:
                        sLabel=_('root')
                    vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                    r=self.AddRoot(sLabel,imgTup[0],imgTup[1],tid)
                    vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                    #self.SetItemImage(r,imgTup[0],wx.TreeItemIcon_Normal)
                    #self.SetItemImage(r,imgTup[1],wx.TreeItemIcon_Expanded)
                    if tid is not None:
                        if self.tiCache is not None:
                            self.tiCache.AddID(r,self.doc.getKeyNum(node))
                    self.tiRoot=r
                    self.rootNode=node
                    self.idRootNode=self.doc.getKeyNum(node)
                    vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                    #self.doc.release()
                    return r
                except:
                    vtLog.vtLngTB(self.GetName())
                #self.doc.release()
            except:
                vtLog.vtLngTB(self.GetName())
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        return vtXmlTree.__addRootByInfos__(self,node,tagName,infos)
    def __addGroupingByInfos__(self,parent,o,tagName,infos):
        if VERBOSE>5:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;key:%s'%(tagName,self.doc.getKey(o)),self)
        if self.bGrouping:
            bFound=False
            if self.bGroupingDict:
                if self.grouping.has_key(tagName):
                    bFound=True
                    grouping=self.grouping[tagName]
            else:
                bFound=True
                grouping=self.grouping
            if bFound:
                if self.TREE_DATA_ID:
                    parId=self.GetPyData(parent)
                    parNode=self.doc.getNodeByIdNum(parId)
                else:
                    parNode=self.GetPyData(parent)
                    parId=self.doc.getKeyNum(parNode)
                try:
                    grpItem=self.groupItems[parId]
                except:
                    grpItem={}
                    self.groupItems[parId]=grpItem
                tnparent=parent
                for g in grouping:
                    if len(g[0])==0:
                        val=tagName
                    else:
                        val=infos[g[0]]
                    val=self.__getValFormat__(g,val,infos)
                    if val is None:
                        continue
                    imgTuple=self.__getImgFormat__(g,val)
                    #try:
                    if val in grpItem:
                        act=grpItem[val]
                        grpItem=act[1]
                        tnparent=act[0]
                    #except:
                    else:
                        tid=wx.TreeItemData()
                        tid.SetData(None)
                        
                        tn=self.AppendItem(tnparent,val,-1,-1,tid)
                        f=self.GetItemFont(tn)
                        f.SetWeight(wx.FONTWEIGHT_LIGHT)
                        f.SetStyle(wx.FONTSTYLE_SLANT)
                        self.SetItemFont(tn,f)
                        try:
                            self.SetItemImage(tn,imgTuple[0],wx.TreeItemIcon_Normal)
                            self.SetItemImage(tn,imgTuple[1],wx.TreeItemIcon_Selected)
                        except:
                            pass
                        act=(tn,{})
                        grpItem[val]=act
                        grpItem=act[1]
                        tnparent=tn
            else:
                tnparent=parent
        else:
            tnparent=parent
        return tnparent
    def __addElementByInfos__(self,parent,o,tagName,infos,bRet=False):
        if VERBOSE>5:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;key:%s'%(tagName,self.doc.getKey(o)),self)
        id=self.doc.getKey(o)
        tup=self.__findNodeByID__(None,id)
        if tup is not None:
            return tup[0]
        try:
            if self.__is2add__(o,tagName)==False:
                if VERBOSE>5:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;key:%s is not to add'%(tagName,self.doc.getKey(o)),self)
                if bRet:
                    return parent
                else:
                    return
            if self.__is2skip__(o,tagName):
                if VERBOSE>5:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;key:%s is to skip'%(tagName,self.doc.getKey(o)),self)
                if bRet:
                    return parent
                else:
                    return
            imgTup=self.__getImagesByInfos__(tagName,infos)
            tnparent=self.__addGroupingByInfos__(parent,o,tagName,infos)
        except:
            vtLog.vtLngTB(self.GetName())
            tnparent=parent
        sLabel=''
        if type(self.label)==types.DictType:
            try:
                label=self.label[tagName]
            except:
                try:
                    label=self.label[None]
                except:
                    vtLog.vtLngTB(self.GetName())
        else:
            label=self.label
        if VERBOSE>5:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;label:%s'%(tagName,repr(label)),self)
        if type(label) in [types.FunctionType,types.MethodType]:
            sLabel=label(parent=parent,node=o,tagName=tagName,infos=infos)
        else:
            ##for v in label:
            ##    if v[0] in infos:
            ##        val=infos[v[0]]
            ##        val=self.__getValFormat__(v,val)
            ##        sLabel=sLabel+val+' '
            #sLabel=u' '.join([self.__getValFormat__(v,infos[v[0]]) 
            #                for v in label if v[0] in infos]+[''])
            def getLblVal(v,infos):
                if type(v)==types.UnicodeType:
                    return v
                elif v[0] is None:
                    return tagName
                elif v[0] in infos:
                    return self.__getValFormat__(v,infos[v[0]])
                return ''
            sLabel=u' '.join([getLblVal(v,infos) for v in label]+[''])
        if VERBOSE>5:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'sLabel:%s'%(sLabel),self)
        # check project entry type
        tid=wx.TreeItemData()
        if self.TREE_DATA_ID:
            tid.SetData(self.doc.getKeyNum(o))
        else:
            tid.SetData(o)
        tn=self.AppendItem(tnparent,sLabel,-1,-1,tid)
        if self.tiCache is not None:
            self.tiCache.AddID(tn,self.doc.getKeyNum(o))#infos[self.nodeKey])
        iOfs=0
        if self.isNodeInstance(o,infos):
            iOfs=2
            self.SetItemBold(tn)
        if self.isNodeInvalid(o,infos):
            iOfs=4
        try:
            self.SetItemImage(tn,imgTup[iOfs+0],wx.TreeItemIcon_Normal)
            self.SetItemImage(tn,imgTup[iOfs+1],wx.TreeItemIcon_Selected)
        except:
            #vtLog.CallStack('')
            #print tn,tnparent,sLabel,imgTup,bRet,iOfs
            vtLog.vtLngCurWX(vtLog.ERROR,'tag:%s;infos:%s'%(tagName,vtLog.pformat(infos)),self)
            vtLog.vtLngTB(self.GetName())
            #print self.GetItemText(tnparent)
            #print self.GetItemText(tnparent)
        # add grouping sub nodes here
        self.tiLast=tn
        if VERBOSE>5:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;key:%s added'%(tagName,self.doc.getKey(o)),self)
        if bRet==True:
            return tn
    def __deleteElement__(self,ti,node):
        self.Delete(ti)
        id=self.doc.getKey(node)
        if self.tiCache is not None:
            self.tiCache.DelID(id)
        if id==self.idCut:
            self.idCut=-1
        if id==self.idCopy:
            self.idCopy=-1
        if id==self.idMoveTarget:
            self.idMoveTarget=-1
    def __sortAllGrpItems__(self,grpItem):
        #vtLog.CallStack('%s'%repr(grpItem))
        for g in grpItem.values():
            self.SortChildren(g[0])
            self.__sortAllGrpItems__(g[1])
    def Clear(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'clear',
                        origin=self.GetName())
        #if self.thdAddElements.IsRunning():
        #    while self.thdAddElements.IsRunning():
        #        time.sleep(1)
        vtXmlTree.Clear(self)
        try:
            if hasattr(self,'dToolBar'):
                if self.dToolBar is not None:
                    self.EnableToolBarItems()
        except:
            vtLog.vtLngTB(self.GetName())
    def __Clear__(self):
        self.groupItems={}
        self.idCut=-1
        self.idCopy=-1
        self.idMoveTarget=-1
        vtXmlTree.__Clear__(self)
    def IsBusy(self):
        if vtXmlTree.IsBusy(self):
            return True
        return False
    def Resume(self):
        vtXmlTree.Resume(self)
    def Pause(self):
        vtXmlTree.Pause(self)
    def IsRunning(self):
        if vtXmlTree.IsRunning(self):
            return True
        return False
    def IsThreadRunning(self):
        if vtXmlTree.IsThreadRunning(self):
            return True
        return False
    def Restart(self):
        vtXmlTree.Restart(self)
    def Stop(self):
        vtXmlTree.Stop(self)
    def ClearDoc(self):
        try:
            vtLog.vtLngCur(vtLog.DEBUG,''%(),self.GetOrigin())
            vtXmlTree.ClearDoc(self)
            
            if self.frmDesc is not None:
                for w in self.frmDesc:
                    #w.ClearDoc()
                    w.DoShutDown()
                    #w.Destroy()
                self.frmDesc=None
            if self.frmAttr is not None:
                for w in self.frmAttr:
                    w.DoShutDown()
                self.frmAttr=None
        except:
            self.__logTB__()
    def SetDoc(self,doc,bNet=False):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'net:%d'%bNet,self)
        vtXmlTree.SetDoc(self,doc,bNet)
        if doc is None:
            if self.frmDesc is not None:
                for w in self.frmDesc:
                    #w.ClearDoc()
                    w.Close()
                    #w.Destroy()
                self.frmDesc=None
            if self.frmAttr is not None:
                for w in self.frmAttr:
                    w.Close()
                self.frmAttr=None
        try:
            if hasattr(self,'dlgFind'):
                self.dlgFind.SetDoc(self.doc)
            if hasattr(self,'dlgImpNode'):
                self.dlgImpNode.SetDoc(self.doc,bNet)
            if hasattr(self,'dlgExpNode'):
                self.dlgExpNode.SetDoc(self.doc,bNet)
        except:
            vtLog.vtLngTB(self.GetName())
        try:
            if hasattr(self,'dToolBar'):
                if self.dToolBar is not None:
                    self.EnableToolBarItems()
        except:
            vtLog.vtLngTB(self.GetName())
    def __createRoot__(self,node,blocking=False):
        self.__Clear__()
        self.groupItems={}
        return vtXmlTree.__createRoot__(self,node,blocking=blocking)
    def SetNode(self,node):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(None,vtLog.DEBUG,'SetNode',
                        origin=self.GetName())
        #if self.IsBusy():
        #    vtLog.vtLngCurWX(vtLog.CRITICAL,'isBusy:%d'%(self.IsBusy()),self)
        #    return
        #if self.thdAddElements.IsRunning():
        #    if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #        vtLog.vtLngCallStack(None,vtLog.DEBUG,'thread is running, break.',
        #                origin=self.GetName())
        #    self.thdAddElements.Stop()
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        #    while self.thdAddElements.IsRunning():
        #        time.sleep(0.3)
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlTree.SetNode(self,node)
        try:
            if hasattr(self,'dToolBar'):
                if self.dToolBar is not None:
                    self.DoCallBack(self.EnableToolBarItems)        # 100221:wro delay after tree is build
        except:
            vtLog.vtLngTB(self.GetName())
    def OnAddFinished(self,evt):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        #idPar=self.doc.getKey(self.rootNode)
        idPar=self.doc.getAttribute(self.rootNode,self.doc.attr)
        if self.groupItems.has_key(idPar):
            self.__sortAllGrpItems__(self.groupItems[idPar])
            #self.__sortAllGrpItems__(self.groupItems)
        self.SortChildren(self.tiRoot)
        
        self.Expand(self.tiRoot)
        vtXmlTree.OnAddFinished(self,evt)
        evt.Skip()
    def __createMenuIds__(self):
        if not hasattr(self,'popupIdSort'):
            self.__createMenuEditIds__()
            self.popupIdFind=wx.NewId()
            self.popupIdSort=wx.NewId()
            wx.EVT_MENU(self,self.popupIdFind,self.OnTreeFind)
            wx.EVT_MENU(self,self.popupIdSort,self.OnTreeSort)
            if not hasattr(self,'popupIdAlign'):
                self.popupIdAlign=wx.NewId()
                self.popupIdReBuild=wx.NewId()
                self.popupIdConfig=wx.NewId()
                #self.popupIdMessages=wx.NewId()
                wx.EVT_MENU(self,self.popupIdAlign,self.OnTreeAlign)
                wx.EVT_MENU(self,self.popupIdReBuild,self.OnTreeReBuild)
                wx.EVT_MENU(self,self.popupIdConfig,self.OnTreeConfig)
                #wx.EVT_MENU(self,self.popupIdMessages,self.OnTreeMessages)
            self.__createMenuCmdIds__()
            if self.bLangMenu:
                self.__createMenuLangIds__()
            if self.bGrpMenu:
                self.__createMenuGrpIds__()
            if self.bImportMenu:
                self.__createMenuImportIds__()
            if self.bExportMenu:
                self.__createMenuExportIds__()
    def __createMenuToolsIds__(self):
        if not hasattr(self,'popupIdAlign'):
            self.popupIdAlign=wx.NewId()
            self.popupIdReBuild=wx.NewId()
            self.popupIdConfig=wx.NewId()
            #self.popupIdCmd=wx.NewId()
            #self.popupIdMessages=wx.NewId()
            #wx.EVT_MENU(self,self.popupIdAlign,self.OnTreeAlign)
        self.__createMenuCmdIds__()
        self.__createMenuExportIds__()
        self.__createMenuImportIds__()
    def __createMenuCmdIds__(self):
        if not hasattr(self,'popupIdCmd'):
            self.popupIdCmd=wx.NewId()
            wx.EVT_MENU(self,self.popupIdCmd,self.OnTreeCmd)
            self.popupCmdIds={}
            if self.doc is not None:
                dCmds=self.doc.GetCmdDict()
                if len(dCmds)>0:
                    lCmds=[]
                    for k,v in dCmds.items():
                        try:
                            if (v[1] & 0xF00)!=0:
                                lCmds.append(((v[1] & 0xF00) >> 8,k))
                        except:
                            pass
                    if len(lCmds)>0:
                        for it in lCmds:
                            self.popupCmdIds[it[1]]=wx.NewId()
                            wx.EVT_MENU(self,self.popupCmdIds[it[1]],self.OnTreeCmd)
    def __createMenuLangIds__(self):
        if not hasattr(self,'popupIdLang'):
            if len(self.languages)>0:
                self.popupIdLang=wx.NewId()
                self.popupLangIds={}
                for lang in self.languages:
                    self.popupLangIds[lang]=wx.NewId()
                    wx.EVT_MENU(self,self.popupLangIds[lang],self.OnTreeLang)
    def __createMenuGrpIds__(self):
        if not hasattr(self,'popupGrpId'):
            self.popupGrpId=wx.NewId()
            self.popupGrpIds={}
            self.popupViewIntIds={}
            self.popupViewIds={}
            try:
                for tup in self.grpMenu:
                    sGrpId,sGrpName=tup[0],tup[1]
                    id=wx.NewId()
                    self.popupGrpIds[sGrpId]=id
                    wx.EVT_MENU(self,self.popupGrpIds[sGrpId],self.OnTreeGroup)
                self.__logDebug__(self.lViewInt)
                self.__createMenuExternalIds__(self.lViewInt,self.popupViewIntIds)
                self.__createMenuExternalIds__(self.lView,self.popupViewIds)
            except:
                pass
    def __createMenuImportIds__(self):
        if not hasattr(self,'popupIdImport'):
            self.popupIdImport=wx.NewId()
            self.popupIdImportXml=wx.NewId()
            self.popupIdImportCsv=wx.NewId()
            self.popupIdImportExcel=wx.NewId()
            wx.EVT_MENU(self,self.popupIdImportXml,self.OnTreeImpXml)
            wx.EVT_MENU(self,self.popupIdImportCsv,self.OnTreeImpCsv)
            wx.EVT_MENU(self,self.popupIdImportExcel,self.OnTreeImpExcel)
    def __addMenuImport__(self,menu):
        if self.bImportMenu:
            menu.AppendSeparator()
            menuImport=wx.Menu()
            mnIt=wx.MenuItem(menuImport,self.popupIdImportXml,'XML')
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Import))
            menuImport.AppendItem(mnIt)
            mnIt=wx.MenuItem(menuImport,self.popupIdImportExcel,'Excel')
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.ExcelImport))
            menuImport.AppendItem(mnIt)
            mnIt=wx.MenuItem(menuImport,self.popupIdImportCsv,'CSV')
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.ImportFree))
            menuImport.AppendItem(mnIt)
            mnIt=wx.MenuItem(menu,self.popupIdImport,_('import'),subMenu=menuImport)
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.ImportFree))
            menu.AppendItem(mnIt)
            return True
        return False
    def __createMenuExportIds__(self):
        if not hasattr(self,'popupIdExport'):
            self.popupIdExport=wx.NewId()
            self.popupIdExportXml=wx.NewId()
            self.popupIdExportCsv=wx.NewId()
            self.popupIdExportExcel=wx.NewId()
            wx.EVT_MENU(self,self.popupIdExportXml,self.OnTreeExpXml)
            wx.EVT_MENU(self,self.popupIdExportCsv,self.OnTreeExpCsv)
            wx.EVT_MENU(self,self.popupIdExportExcel,self.OnTreeExpExcel)
    def __addMenuExport__(self,menu):
        if self.bExportMenu:
            menu.AppendSeparator()
            menuExport=wx.Menu()
            mnIt=wx.MenuItem(menuExport,self.popupIdExportXml,'XML')
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Export))
            menuExport.AppendItem(mnIt)
            mnIt=wx.MenuItem(menuExport,self.popupIdExportExcel,'Excel')
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.ExcelExport))
            menuExport.AppendItem(mnIt)
            mnIt=wx.MenuItem(menuExport,self.popupIdExportCsv,'CSV')
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.ExportFree))
            menuExport.AppendItem(mnIt)
            mnIt=wx.MenuItem(menu,self.popupIdExport,_('export'),subMenu=menuExport)
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.ExportFree))
            menu.AppendItem(mnIt)
            return True
        return False
    def AddMenuExport(self,menu,obj,iPos=-1):
        mnExpIt=None
        try:
            if iPos<0:
                iPos=menuTools.GetMenuItemCount()
            self.__createMenuExportIds__()
            mnExp=wx.Menu()
            mnIt=wx.MenuItem(mnExp,self.popupIdExportXml,'XML')
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Export))
            mnExp.AppendItem(mnIt)
            mnIt=wx.MenuItem(mnExp,self.popupIdExportExcel,'Excel')
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.ExcelExport))
            mnExp.AppendItem(mnIt)
            mnIt=wx.MenuItem(mnExp,self.popupIdExportCsv,'CSV')
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.ExportFree))
            mnExp.AppendItem(mnIt)
            wx.EVT_MENU(obj,self.popupIdExportXml,self.OnTreeExpXml)
            wx.EVT_MENU(obj,self.popupIdExportCsv,self.OnTreeExpCsv)
            wx.EVT_MENU(obj,self.popupIdExportExcel,self.OnTreeExpExcel)
            
            mnExpIt=wx.MenuItem(menu,self.popupIdExport,_(u'Export'),subMenu=mnExp)
            mnExpIt.SetBitmap(vtArt.getBitmap(vtArt.ExportFree))
            menu.InsertItem(iPos,mnExpIt)
            iPos+=1
        except:
            import traceback
            traceback.print_exc()
            vtLog.vtLngTB(self.GetName())
            mnExp=None
        return mnExpIt,iPos
    def AddMenuImport(self,menu,obj,iPos=-1):
        mmImpIt=None
        try:
            if iPos<0:
                iPos=menuTools.GetMenuItemCount()
            self.__createMenuImportIds__()
            menuImport=wx.Menu()
            mnIt=wx.MenuItem(menuImport,self.popupIdImportXml,'XML')
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Import))
            menuImport.AppendItem(mnIt)
            mnIt=wx.MenuItem(menuImport,self.popupIdImportExcel,'Excel')
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.ExcelImport))
            menuImport.AppendItem(mnIt)
            mnIt=wx.MenuItem(menuImport,self.popupIdImportCsv,'CSV')
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.ImportFree))
            menuImport.AppendItem(mnIt)
            wx.EVT_MENU(obj,self.popupIdImportXml,self.OnTreeImpXml)
            wx.EVT_MENU(obj,self.popupIdImportCsv,self.OnTreeImpCsv)
            wx.EVT_MENU(obj,self.popupIdImportExcel,self.OnTreeImpExcel)
            
            mmImpIt=wx.MenuItem(menu,self.popupIdImport,_(u'Import'),subMenu=menuImport)
            mmImpIt.SetBitmap(vtArt.getBitmap(vtArt.ImportFree))
            menu.InsertItem(iPos,mmImpIt)
            iPos+=1
        except:
            import traceback
            traceback.print_exc()
            vtLog.vtLngTB(self.GetName())
            menuImport=None
        return mmImpIt,iPos
    def AddMenuTool(self,menuTools,obj,iPos=-1):
        try:
            self.__createMenuToolsIds__()
            if menuTools is None:
                menuTools=wx.Menu()
                iPos=0
            if iPos<0:
                iPos=menuTools.GetMenuItemCount()
            mnIt,iPos=self.AddMenuImport(menuTools,obj,iPos=iPos)
            mnIt,iPos=self.AddMenuExport(menuTools,obj,iPos=iPos)
            mnIt=wx.MenuItem(menuTools,wx.ID_SEPARATOR)
            menuTools.InsertItem(iPos,mnIt)
            iPos+=1
            mnIt=wx.MenuItem(menuTools,self.popupIdAlign,_(u'Align XML'))
            wx.EVT_MENU(obj,self.popupIdAlign,self.OnTreeAlign)
            #mnIt.SetBitmap(vtArt.getBitmap(vtArt.ImportFree))
            menuTools.InsertItem(iPos,mnIt)
            iPos+=1
            mnIt=wx.MenuItem(menuTools,self.popupIdReBuild,_(u'Rebuild'))
            wx.EVT_MENU(obj,self.popupIdReBuild,self.OnTreeReBuild)
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Refresh))
            menuTools.InsertItem(iPos,mnIt)
            iPos+=1
            mnIt=wx.MenuItem(menuTools,self.popupIdConfig,_(u'Configuration'))
            wx.EVT_MENU(obj,self.popupIdConfig,self.OnTreeConfig)
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Config))
            menuTools.InsertItem(iPos,mnIt)
            iPos+=1
            
            if self.doc is not None:
                if self.__addMenuCmds__(menuTools):
                    iPos+=1
            #mnIt=wx.MenuItem(menuTools,self.popupIdMessages,_(u'Messages'))
            #wx.EVT_MENU(obj,self.popupIdMessages,self.OnTreeMessages)
            #mnIt.SetBitmap(vtArt.getBitmap(vtArt.EMail))
            #menuTools.InsertItem(iPos,mnIt)
            #iPos+=1
        except:
            import traceback
            traceback.print_exc()
            vtLog.vtLngTB(self.GetName())
            menuTools=None
        return menuTools,iPos
    def __addMenuImportExport__(self,menu):
        if self.bExportMenu or self.bImportMenu:
            menu.AppendSeparator()
            menuExpImp=wx.Menu()
        else:
            return False
        if self.bExportMenu:
            mnIt=wx.MenuItem(menuExpImp,self.popupIdExportXml,_(u'export XML'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Export))
            menuExpImp.AppendItem(mnIt)
            mnIt=wx.MenuItem(menuExpImp,self.popupIdExportExcel,_(u'export Excel'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.ExcelExport))
            menuExpImp.AppendItem(mnIt)
            mnIt=wx.MenuItem(menuExpImp,self.popupIdExportCsv,_(u'export CSV'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.ExportFree))
            menuExpImp.AppendItem(mnIt)
        if self.bImportMenu:
            if self.bExportMenu:
                menuExpImp.AppendSeparator()
            mnIt=wx.MenuItem(menuExpImp,self.popupIdImportXml,_(u'import XML'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Import))
            menuExpImp.AppendItem(mnIt)
            mnIt=wx.MenuItem(menuExpImp,self.popupIdImportExcel,_(u'import Excel'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.ExcelImport))
            menuExpImp.AppendItem(mnIt)
            mnIt=wx.MenuItem(menuExpImp,self.popupIdImportCsv,_(u'import CSV'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.ImportFree))
            menuExpImp.AppendItem(mnIt)
            
        mnIt=wx.MenuItem(menu,self.popupIdExport,_('export/import'),subMenu=menuExpImp)
        mnIt.SetBitmap(vtArt.getBitmap(vtArt.ImportExport))
        menu.AppendItem(mnIt)
        return True
    def __addMenuCmds__(self,menu):
        if self.doc is not None:
                self.__createMenuCmdIds__()
                dCmds=self.doc.GetCmdDict()
                if len(dCmds)>0:
                    lCmds=[]
                    for k,v in dCmds.items():
                        if k.find(':')<0:
                            continue
                        try:
                            if (v[1] & 0xF00)!=0:
                                lCmds.append(((v[1] & 0xF00) >> 8,k))
                        except:
                            pass
                    if len(lCmds)>0:
                        lCmds.sort()
                        menuCmds=wx.Menu()
                        mnIt=wx.MenuItem(menuCmds,self.popupIdCmd,_(u'Commands'))
                        menuCmds.AppendItem(mnIt)
                        menuCmds.AppendSeparator()
                        for it in lCmds:
                            k=it[1]
                            v=dCmds[k]
                            mnIt=wx.MenuItem(menuCmds,self.popupCmdIds[k],v[0])
                            menuCmds.AppendItem(mnIt)
                    else:
                        menuCmds=None
                    mnIt=wx.MenuItem(menu,self.popupIdCmd,_(u'Commands'),subMenu=menuCmds)
                    #wx.EVT_MENU(obj,self.popupIdCmd,self.OnTreeCmd)
                    mnIt.SetBitmap(vtArt.getBitmap(vtArt.Build))
                    menu.AppendItem(mnIt)
                    return True
        return False
    def __createMenuEditIds__(self):
        if not hasattr(self,'popupIdAdd'):
            self.popupIdAdd=wx.NewId()
            self.popupIdEdit=wx.NewId()
            self.popupIdEditReq=wx.NewId()
            self.popupIdEditRel=wx.NewId()
            self.popupIdDup=wx.NewId()
            self.popupIdDel=wx.NewId()
            self.popupIdCut=wx.NewId()
            self.popupIdCopy=wx.NewId()
            self.popupIdPaste=wx.NewId()
            self.popupIdMove=wx.NewId()
            self.popupIdMoveTar=wx.NewId()
            wx.EVT_MENU(self,self.popupIdAdd,self.OnTreeAdd)
            wx.EVT_MENU(self,self.popupIdEdit,self.OnTreeEdit)
            wx.EVT_MENU(self,self.popupIdEditReq,self.OnTreeEditReq)
            wx.EVT_MENU(self,self.popupIdEditRel,self.OnTreeEditRel)
            wx.EVT_MENU(self,self.popupIdDup,self.OnTreeDuplicate)
            wx.EVT_MENU(self,self.popupIdDel,self.OnTreeDel)
            if self.bClipMenu:
                wx.EVT_MENU(self,self.popupIdCut,self.OnTreeCut)
                wx.EVT_MENU(self,self.popupIdCopy,self.OnTreeCopy)
                wx.EVT_MENU(self,self.popupIdPaste,self.OnTreePaste)
            if self.bMoveMenu:
                wx.EVT_MENU(self,self.popupIdMove,self.OnTreeMove)
                wx.EVT_MENU(self,self.popupIdMoveTar,self.OnTreeMoveTarget)
    def PopulateToolBar(self,parent,tbMain,dToolBar,iStyle=0x370D):
        self.__logInfo__('iStyle:0x%04x'%(iStyle))
        if (iStyle&0x0001)!=0:
            id=wx.NewId()
            dToolBar['add']=id
            tbMain.AddSimpleTool(id, vtArt.getBitmap(vtArt.Add), _(u"Add"), 
                            _(u"add"))
            parent.Bind(wx.EVT_TOOL, self.OnTreeAdd, id=id)
        
        if (iStyle&0x0002)!=0:
            id=wx.NewId()
            dToolBar['edit']=id
            tbMain.AddSimpleTool(id, vtArt.getBitmap(vtArt.Edit), _(u"Edit"), 
                            _(u"edit"))
            parent.Bind(wx.EVT_TOOL, self.OnTreeEdit, id=id)
        
        if (iStyle&0x0004)!=0:
            id=wx.NewId()
            dToolBar['dup']=id
            tbMain.AddSimpleTool(id, vtArt.getBitmap(vtArt.Duplicate), _(u"Duplicate"), 
                            _(u"duplicate"))
            parent.Bind(wx.EVT_TOOL, self.OnTreeDuplicate, id=id)
        
        if (iStyle&0x0008)!=0:
            id=wx.NewId()
            dToolBar['del']=id
            tbMain.AddSimpleTool(id, vtArt.getBitmap(vtArt.Del), _(u"Delete"), 
                            _(u"delete"))
            parent.Bind(wx.EVT_TOOL, self.OnTreeDel, id=id)
        
        if (iStyle&0x000f)!=0:
            tbMain.AddSeparator()
        
        if (iStyle&0x0010)!=0:
            id=wx.NewId()
            dToolBar['exec']=id
            tbMain.AddSimpleTool(id, vtArt.getBitmap(vtArt.Build), _(u"Execute"), 
                            _(u"execute"))
            parent.Bind(wx.EVT_TOOL, self.OnTreeExec, id=id)
        if (iStyle&0x0020)!=0:
            id=wx.NewId()
            dToolBar['build']=id
            tbMain.AddSimpleTool(id, vtArt.getBitmap(vtArt.Report), _(u"Build"), 
                            _(u"build"))
            parent.Bind(wx.EVT_TOOL, self.OnTreeBuild, id=id)
        if (iStyle&0x00f0)!=0:
            tbMain.AddSeparator()
        
        if (iStyle&0x0100)!=0:
            id=wx.NewId()
            dToolBar['cut']=id
            tbMain.AddSimpleTool(id, vtArt.getBitmap(vtArt.Cut), _(u"Cut"), 
                            _(u"cut"))
            parent.Bind(wx.EVT_TOOL, self.OnTreeCut, id=id)
        if (iStyle&0x0200)!=0:
            id=wx.NewId()
            dToolBar['copy']=id
            tbMain.AddSimpleTool(id, vtArt.getBitmap(vtArt.Copy), _(u"Copy"), 
                            _(u"copy"))
            parent.Bind(wx.EVT_TOOL, self.OnTreeCopy, id=id)
        if (iStyle&0x0400)!=0:
            id=wx.NewId()
            dToolBar['paste']=id
            tbMain.AddSimpleTool(id, vtArt.getBitmap(vtArt.Paste), _(u"Paste"), 
                            _(u"paste"))
            parent.Bind(wx.EVT_TOOL, self.OnTreePaste, id=id)
        if (iStyle&0x0f00)!=0:
            tbMain.AddSeparator()
        
        if (iStyle&0x1000)!=0:
            id=wx.NewId()
            dToolBar['move']=id
            tbMain.AddSimpleTool(id, vtArt.getBitmap(vtArt.Move), _(u"Move Node"), 
                            _(u"Move Node"))
            parent.Bind(wx.EVT_TOOL, self.OnTreeMove, id=id)
        if (iStyle&0x2000)!=0:
            id=wx.NewId()
            dToolBar['moveTarget']=id
            tbMain.AddSimpleTool(id, vtArt.getBitmap(vtArt.ImportFree), _(u"Move ..."), 
                            _(u"Move ..."))
            parent.Bind(wx.EVT_TOOL, self.OnTreeMoveTarget, id=id)
        
        if (iStyle&0x2004)!=0:
            id=wx.NewId()
            dToolBar['dupTo']=id
            tbMain.AddSimpleTool(id, vtArt.getBitmap(vtArt.Duplicate), _(u"Duplicate to"), 
                            _(u"Move ..."))
            parent.Bind(wx.EVT_TOOL, self.OnTreeDuplicateTo, id=id)
        
        self.tbMain=tbMain
        self.dToolBar=dToolBar
    def __getNodeCapablitiesACL(self,node):
        try:
            nodePar=None
            o=None
            oPar=None
            bRegular=True
            #if self.doc.IsNodeKeyValid(node)==False:
            #    node=None
            bFlagAdd=False
            bFlagEdit=False
            bFlagDup=False
            bFlagDel=False
            bFlagExec=False
            bFlagBuild=False
            bFlagCut=False
            bFlagCopy=False
            bFlagPaste=False
            bFlagMove=False
            if node is not None:
                bFlagAdd=True
                bFlagEdit=True
                bFlagDup=True
                bFlagDel=True
                bFlagExec=True
                bFlagBuild=True
                bFlagCut=True
                bFlagCopy=True
                bFlagPaste=True
                #bFlagMove=True
                if self.doc is not None:
                    nodePar=None
                    o=None
                    if type(node)==types.DictType:
                        if '__reg' in node:
                            bFlagAdd=False
                            o=self.doc.GetReg(node['__reg'])
                            bRegular=False
                    else:
                        o=self.doc.GetRegByNode(node)
                        nodePar=self.doc.getParent(node)
                    
                        try:
                            if not self.doc.hasPossibleNode(node):
                                bFlagAdd=False
                        except:
                            bFlagAdd=False
                    if nodePar is not None:
                        try:
                            if self.doc.IsNodeKeyValid(nodePar):
                                oPar=self.doc.GetRegByNode(nodePar)
                            else:
                                # 070108:wro
                                # parent node is above base node, do not check
                                oPar=None
                        except:
                            oPar=None
                            vtLog.vtLngCurWX(vtLog.ERROR,'tag:%s;reg not found'%(self.doc.getTagName(nodePar)),self)
                    if o is not None:
                        try:
                            if o.IsMultiple()==False:
                                bFlagDup=False
                            if o.IsAclOk(self.doc.ACL_MSK_ADD)==False:
                                bFlagDup=False
                                bFlagPaste=False
                            if o.IsAclOk(self.doc.ACL_MSK_DEL)==False:
                                bFlagDel=False
                                bFlagCut=False
                            if o.IsAclOk(self.doc.ACL_MSK_MOVE)==False:
                                bFlagMove=False
                            if o.IsAclOk(self.doc.ACL_MSK_WRITE)==False:
                                bFlagEdit=False
                            if o.GetEditDialogClass() is None:
                                bFlagEdit=False
                            if o.IsAclOk(self.doc.ACL_MSK_EXEC)==False:
                                bFlagExec=False
                            if o.IsAclOk(self.doc.ACL_MSK_BUILD)==False:
                                bFlagBuild=False
                        except:
                            pass
                            vtLog.vtLngTB(self.GetName())
                    else:
                        bFlagAdd=False
                        bFlagEdit=False
                        bFlagDup=False
                        bFlagDel=False
                        bFlagExec=False
                        bFlagBuild=False
                        bFlagCut=False
                        bFlagCopy=False
                        bFlagPaste=False
                        bFlagMove=False
                    
                    if oPar is not None:
                        try:
                            if oPar.IsAclOk(self.doc.ACL_MSK_ADD)==False:
                                bFlagDup=False
                                
                        except:
                            pass
                    if (self.idCut<0) and (self.idCopy<0):
                        bFlagPaste=False
                    if self.idMoveTarget<0:
                        bFlagMove=False
                        #self.tbMain.SetToolShortHelp(self.dToolBar['moveTarget'],_('Move ...'))
                    else:
                        nodeMoveTarget=self.doc.getNodeByIdNum(self.idMoveTarget)
                        try:
                            if self.doc.isTagNamePossible2AddAtNode(o.GetTagName(),nodeMoveTarget)==False:
                                bFlagMove=False
                                vtLog.vtLngCurWX(vtLog.INFO,'id:%08d is invalid target for tagname:%s'%(self.idMoveTarget,o.GetTagName()),self)
                            else:
                                bFlagMove=True
                        except:
                            vtLog.vtLngTB(self.GetName())
                        if nodeMoveTarget is not None:
                            try:
                                oMv=self.doc.GetRegByNode(nodeMoveTarget)
                                l=oMv.AddValuesToLstByLst(nodeMoveTarget,None,oMv.FUNCS_GET_4_TREE)
                                s=' '.join(l)
                            except:
                                vtLog.vtLngTB(self.GetName())
                                s=self.doc.getNodeText(nodeMoveTarget,'tag')
                        else:
                            s='???'
                        #self.tbMain.SetToolShortHelp(self.dToolBar['moveTarget'],_('Move <%s>')%s)
                #else:
                #    self.tbMain.SetToolShortHelp(self.dToolBar['moveTarget'],_('Move ...'))
                if bRegular==True:
                    if self.doc.IsNodeKeyValid(node)==False:
                        # this node is either the root or a grouping node
                        bFlagDup=False
                        bFlagEdit=False
                        bFlagDel=False
                        bFlagExec=False
                        bFlagBuild=False
                        bFlagCut=False
                        bFlagCopy=False
                        bFlagPaste=False
                        bFlagMove=False
        except:
            vtLog.vtLngTB(self.GetName())
        d={   'add':bFlagAdd,
                    'dup':bFlagDup,
                    'edit':bFlagEdit,
                    'del':bFlagDel,
                    'exec':bFlagExec,
                    'build':bFlagBuild,
                    'cut':bFlagCut,
                    'copy':bFlagCopy,
                    'paste':bFlagPaste,
                    'move':bFlagMove}
        if self.doc is not None:
            if VERBOSE>5:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'id:%08d;acl:%s'%
                        (self.doc.getKeyNum(node),vtLog.pformat(d)),self)
        return d
    def EnableToolBarItems(self):
        try:
            if VERBOSE>5:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'selId:%s'%(self.GetSelectedID()),self)
            if self.doc.IsShutDown():
                return
            self.doc.DoSafe(self.thdProc.CallBack,self.__EnableToolBarItems__)
        except:
            vtLog.vtLngTB(self.GetName())
    def __EnableToolBarItems__(self):
        try:
            if VERBOSE>5:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'selId:%s'%(self.GetSelectedID()),self)
            node=self.GetSelected()
            if node is None:
                if self.triSelected is not None:
                    td=self.GetPyData(self.triSelected)
                    if type(td)==types.DictType:
                        if '__reg' in td:
                            node=td
            if node is None:
                #nodePar=self.doc.getBaseNode()
                node=self.rootNode
            #print node
            dAclCur=self.__getNodeCapablitiesACL(node)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'dAclCur:%r'%(dAclCur),self)
            if node is not None:
                if self.doc is not None:
                    if self.idMoveTarget<0:
                        bFlagMove=False
                        self.tbMain.SetToolShortHelp(self.dToolBar['moveTarget'],_('Move ...'))
                    else:
                        nodeMoveTarget=self.doc.getNodeByIdNum(self.idMoveTarget)
                        o=self.doc.GetRegByNode(node)
                        try:
                            if self.doc.isTagNamePossible2AddAtNode(o.GetTagName(),nodeMoveTarget)==False:
                                vtLog.vtLngCurWX(vtLog.INFO,'id:%08d is invalid target for tagname:%s'%(self.idMoveTarget,o.GetTagName()),self)
                        except:
                            vtLog.vtLngTB(self.GetName())
                        if nodeMoveTarget is not None:
                            try:
                                oMv=self.doc.GetRegByNode(nodeMoveTarget)
                                l=oMv.AddValuesToLstByLst(nodeMoveTarget,None,oMv.FUNCS_GET_4_TREE)
                                s=' '.join(l)
                            except:
                                vtLog.vtLngTB(self.GetName())
                                s=self.doc.getNodeText(nodeMoveTarget,'tag')
                        else:
                            s='???'
                        self.tbMain.SetToolShortHelp(self.dToolBar['moveTarget'],_('Move <%s>')%s)
                else:
                    self.tbMain.SetToolShortHelp(self.dToolBar['moveTarget'],_('Move ...'))
            else:
                self.tbMain.SetToolShortHelp(self.dToolBar['moveTarget'],_('Move ...'))
            for k,v in dAclCur.iteritems():
                if k in self.dToolBar:
                    self.tbMain.EnableTool(self.dToolBar[k],v)
            #self.tbMain.EnableTool(self.dToolBar['add'],bFlagAdd)
            #self.tbMain.EnableTool(self.dToolBar['dup'],bFlagDup)
            #self.tbMain.EnableTool(self.dToolBar['del'],bFlagDel)
            #self.tbMain.EnableTool(self.dToolBar['cut'],bFlagCut)
            #self.tbMain.EnableTool(self.dToolBar['copy'],bFlagCopy)
            #self.tbMain.EnableTool(self.dToolBar['paste'],bFlagPaste)
            #self.tbMain.EnableTool(self.dToolBar['move'],bFlagMove)
        except:
            vtLog.vtLngTB(self.GetName())
    def DisableTool(self):
        try:
            if VERBOSE>5:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'selId:%s'%(self.GetSelectedID()),self)
            dAclCur=self.__getNodeCapablitiesACL(None)
            for k,v in dAclCur.iteritems():
                if k in self.dToolBar:
                    self.tbMain.EnableTool(self.dToolBar[k],v)
        except:
            vtLog.vtLngTB(self.GetName())
    def GetMoveTargetID(self):
        return self.idMoveTarget
    def DoNodeCut(self,iId):
        try:
            self.idCut=iId
            self.idCopy=-1
        except:
            vtLog.vtLngTB(self.GetName())
    def DoNodeCopy(self,iId):
        try:
            self.idCut=-1
            self.idCopy=iId
        except:
            vtLog.vtLngTB(self.GetName())
    def DoNodePaste(self,iId):
        try:
            idTarget=iId
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,
                        'selId:%s;idTarget:%d;idCut:%d;idCopy:%d'%(self.GetSelectedID(),idTarget,self.idCut,self.idCopy),self)
            if self.idCut>=0:
                self._moveTreeNodeById(self.idCut,idTarget)
            if self.idCopy>=0:
                self._addTreeNodeById(self.idCopy,idTarget)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnTreeCut(self,evt):
        if self.objTreeItemSel is None:
            self.idCut=-1
            self.idCopy=-1
        else:
            #nodeTarget=self.GetSelected()
            #self.idCut=self.doc.getKeyNum(nodeTarget)
            self.idCut=self.GetSelectedIDNum()          # 080426:wro
            self.idCopy=-1
    def OnTreeCopy(self,evt):
        if self.objTreeItemSel is None:
            self.idCut=-1
            self.idCopy=-1
        else:
            self.idCut=-1
            #nodeTarget=self.GetSelected()
            #self.idCopy=self.doc.getKeyNum(nodeTarget)
            self.idCopy=self.GetSelectedIDNum()          # 080426:wro
    def OnTreePaste(self,evt):
        if self.objTreeItemSel is not None:
            #nodeTarget=self.GetSelected()
            #idTarget=self.doc.getKeyNum(nodeTarget)
            idTarget=self.GetSelectedIDNum()          # 080426:wro
            if self.idCut>=0:
                self._moveTreeNodeById(self.idCut,idTarget)
            if self.idCopy>=0:
                self._addTreeNodeById(self.idCopy,idTarget)
            pass
        else:
            pass
    def _isMovable(self,nodeSrc,nodeTarget):
        if nodeSrc is None:
            vtLog.vtLngCurWX(vtLog.ERROR,'source node is None',self)
            return False
        if nodeTarget is None:
            vtLog.vtLngCurWX(vtLog.ERROR,'target node is None',self)
            return False
        return True
    def __postAddElementsFin__(self,childID,parID):
        #vtLog.vtLngCurWX(vtLog.CRITICAL,'FIXME',self)
        
        #ti=self.__findNode__(self.tiRoot,child)
        tup=self.__findNodeByID__(None,childID)
        if tup is None:
            self.objTreeItemSel=None
            self.triSelected=None
            #vtLog.vtLngCurWX(vtLog.ERROR,'id:%08d;parID:%08d;should never happen'%(childID,parID),self)
        else:
            ti=tup[0]
            #self.objTreeItemSel=child
            if self.TREE_DATA_ID:
                self.objTreeItemSel=childID
            else:
                self.objTreeItemSel=self.doc.getNodeByIdNum(childID)
            self.triSelected=ti
            self.EnsureVisible(ti)
            self.SelectItem(ti)
        wx.PostEvent(self,vtXmlTreeItemSelected(self,
                        self.triSelected,self.objTreeItemSel))
        try:
            if hasattr(self,'dToolBar'):
                if self.dToolBar is not None:
                    self.EnableToolBarItems()
        except:
            vtLog.vtLngTB(self.GetName())
    def _delTreeNode(self,node):
        id=self.doc.getKeyNum(node)
        vtLog.vtLngCurWX(vtLog.INFO,'id:%08d'%(id),self)
        triTup=self.__findNodeByID__(None,id)
        if self.tiCache is not None:
            self.tiCache.DelID(id)
        self.doc.procChildsKeysRec(1000,node,self._delTreeNode)
        if triTup is not None:
            self.Delete(triTup[0])
        return 1
    def _addTreeNodeById(self,idSrc,idTarget):
        vtLog.vtLngCurWX(vtLog.INFO,'idSrc:%08d;idTarget:%08d'%(idSrc,idTarget),self)
        try:
            nodeTarget=self.doc.getNodeByIdNum(idTarget)
            try:
                oTarget=self.doc.GetRegByNode(nodeTarget)
                if oTarget.IsAclOk(self.doc.ACL_MSK_ADD)==False:
                    vtLog.PrintMsg(_('id:%08d tag:%s add permission denied')%(idTarget,oTarget.GetTagName()),self.widLogging)
                    return
            except:
                pass
            nodeSrc=self.doc.getNodeByIdNum(idSrc)
            try:
                oSrc=self.doc.GetRegByNode(nodeSrc)
                if oSrc.IsAclOk(self.doc.ACL_MSK_ADD)==False:
                    vtLog.PrintMsg(_('id:%08d tag:%s add permission denied')%(idSrc,oSrc.GetTagName()),self.widLogging)
                    return
                if self.doc.isTagNamePossible2AddAtNode(oSrc.GetTagName(),nodeTarget)==False:
                    vtLog.PrintMsg(_('id:%s tag:%s can not be added to id:%08d tag:%s')%(idTarget,oTarget.GetTagName(),idSrc,oSrc.GetTagName()),self.widLogging)
                    return
            except:
                pass
            sTagRefNew=self.getCopyRefTag(nodeTarget,nodeSrc)
            tmp=self.doc.cloneNode(nodeSrc,True)
            try:
                sTagRef=self.doc.getNodeText(tmp,self.doc.TAGNAME_REFERENCE)
                #self.doc.setNodeText(tmp,self.doc.TAGNAME_REFERENCE,
                #            ' '.join([_('Copy of'),sTagRef]))
                self.doc.setNodeText(tmp,self.doc.TAGNAME_REFERENCE,sTagRefNew)
            except:
                pass
            self.doc.appendChild(nodeTarget,tmp)
            self.doc.addNode(nodeTarget,tmp)
        except:
            vtLog.vtLngTB(self.GetName())
    def _moveTreeNodeById(self,idSrc,idTarget,bAddElement=True):
        vtLog.vtLngCurWX(vtLog.INFO,'idSrc:%08d;idTarget:%08d'%(idSrc,idTarget),self)
        try:
            triTargetTup=self.__findNodeByID__(None,idTarget)
            if triTargetTup is None:
                return
            triTarget=triTargetTup[0]
            nodeTarget=self.doc.getNodeByIdNum(idTarget)
            try:
                oTarget=self.doc.GetRegByNode(nodeTarget)
                if oTarget.IsAclOk(self.doc.ACL_MSK_ADD)==False:
                    vtLog.PrintMsg(_('id:%08d tag:%s add permission denied')%(idTarget,oTarget.GetTagName()),self.widLogging)
                    return
            except:
                pass
            nodeSrc=self.doc.getNodeByIdNum(idSrc)
            try:
                oSrc=self.doc.GetRegByNode(nodeSrc)
                if oSrc.IsAclOk(self.doc.ACL_MSK_MOVE)==False:
                    vtLog.PrintMsg(_('id:%08d tag:%s move permission denied')%(idSrc,oSrc.GetTagName()),self.widLogging)
                    return
                if self.doc.isTagNamePossible2AddAtNode(oSrc.GetTagName(),nodeTarget)==False:
                    vtLog.PrintMsg(_('id:%s tag:%s can not be added to id:%08d tag:%s')%(idTarget,oTarget.GetTagName(),idSrc,oSrc.GetTagName()),self.widLogging)
                    return
            except:
                pass
            if self._isMovable(nodeSrc,nodeTarget):
                self._delTreeNode(nodeSrc)
                self.doc.moveNode(nodeTarget,nodeSrc)
                self.doc.AlignNode(nodeSrc)
                ti=self.GetItemParent(triTarget)
                if bAddElement:
                    self.bAddElement=bAddElement
                    self.__addElements__(idTarget,idSrc,#nodeSrc,
                            self.__postAddElementsFin__,idSrc,idTarget)
                else:
                    self.bAddElement=False
                    self.__addElements__(idTarget,idSrc)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnTreeMove(self,evt):
        if self.idMoveTarget>=0:
            idSrc=self.GetSelectedIDNum()   # 080426:wro
            if idSrc is None:
                return
            self._moveTreeNodeById(idSrc,self.idMoveTarget,False)
        pass
    def OnTreeMoveTarget(self,evt):
        if self.idMoveTarget<0:
            if self.objTreeItemSel is None:
                pass
            else:
                self.idMoveTarget=self.GetSelectedIDNum()   #080426:wro 
                #self.doc.getKeyNum(self.objTreeItemSel)
        else:
            self.idMoveTarget=-1
        try:
            if hasattr(self,'dToolBar'):
                if self.dToolBar is not None:
                    self.EnableToolBarItems()
        except:
            vtLog.vtLngTB(self.GetName())
    def __getBaseNode__(self,node):
        par=self.doc.getParent(node)
        if par is None:
            return None
        try:
            o=self.doc.GetRegByNode(par)
            if o.IsBase():
                return par
            else:
                return self.__getBaseNode__(par)
        except:
            return None
    def GetMenuLangBmp(self):
        try:
            tup=self.doc.GetLanguage(self.doc.GetLang(),1)
            return tup[2]
        except:
            vtArt.getBitmap(vtArt.Invisible)
    def AddMenuNav(self,menu,obj,iPos=-1):
        try:
            if menu is None:
                menu=wx.Menu()
                iPos=0
            if iPos<0:
                iPos=menu.GetMenuItemCount()
            menuNav=wx.Menu()
            if not hasattr(self,'popupIdNav'):
                self.popupIdNav=wx.NewId()
                self.popupNavIds={}
                bAddNavId=True
            else:
                bAddNavId=False
            iNum=0
            def add(mn,d,num,name,iId=None,w=None,bmp=None):
                if num in d:
                    id=d[num][0]
                else:
                    id=wx.NewId()
                    d[num]=(id,iId,w)
                wx.EVT_MENU(obj,id,self.OnTreeNav)
                mnIt=wx.MenuItem(mn,id,name+u'\tAlt+%s'%(str(num)))
                if bmp is not None:
                    mnIt.SetBitmap(bmp)
                mn.AppendItem(mnIt)
                
            add(menuNav,self.popupNavIds,0,_(u'Tree'))
            try:
                pn=obj.pn
                if hasattr(pn,'dToolBar'):
                    d=getattr(pn,'dToolBar')
                    if 'quickFind' in d:
                        add(menuNav,self.popupNavIds,1,_(u'Quick Search'),d['quickFind'])
                wCmd=None
                if hasattr(pn,'slwTop'):
                    w=getattr(pn,'slwTop').GetChildren()[0]
                    iId=w.GetId()
                    add(menuNav,self.popupNavIds,2,_(u'Top'),iId,None)
                    if hasattr(w,'OnCbApplyButton'):
                        wCmd=w
                if hasattr(pn,'pnData'):
                    w=getattr(pn,'pnData')
                    lChildWid=w.GetChildren()
                    if lChildWid is not None and len(lChildWid)>0:
                        w=lChildWid[0]
                    iId=w.GetId()
                    add(menuNav,self.popupNavIds,3,_(u'Data'),iId,None)
                if wCmd is not None:
                    if hasattr(wCmd,'Apply'):
                        add(menuNav,self.popupNavIds,'Return',_(u'Apply'),
                                None,wCmd)
                    if hasattr(wCmd,'Cancel'):
                        add(menuNav,self.popupNavIds,'Delete',_(u'Cancel'),
                                None,wCmd)
            except:
                vtLog.vtLngTB(self.GetName())
            
            mnIt=wx.MenuItem(menu,self.popupIdLang,_(u'Navigation'),subMenu=menuNav)
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Navigate))
            menu.InsertItem(iPos,mnIt)
            iPos+=1
        except:
            import traceback
            traceback.print_exc()
            vtLog.vtLngTB(self.GetName())
            menuNav=None
        return menuNav,iPos
    def AddMenuLang(self,menu,obj,iPos=-1):
        try:
            if len(self.languages)==0:
                languages=self.doc.GetLanguages()
                if len(languages)>0:
                    self.bLangDoc=True
                    self.languages=languages
                    self.lang=self.doc.GetLang()
                    self.languageIds=self.doc.GetLanguageIds()
            if len(self.languages)>0:
                if menu is None:
                    menu=wx.Menu()
                    iPos=0
                if iPos<0:
                    iPos=menu.GetMenuItemCount()
                menuLang=wx.Menu()
                if not hasattr(self,'popupIdLang'):
                    self.popupIdLang=wx.NewId()
                    self.popupLangIds={}
                    bAddLangId=True
                else:
                    bAddLangId=False
                bmpSel=None
                try:
                    if len(self.lang)==0:
                        self.lang=self.languages[0][1]
                except:
                    self.lang=self.languages[0][1]
                for name,lang,bmp in self.languages:
                    if self.lang==lang:
                        bmpSel=bmp
                    if bAddLangId:
                        self.popupLangIds[lang]=wx.NewId()
                    wx.EVT_MENU(obj,self.popupLangIds[lang],self.OnTreeLangDoc)
                    mnIt=wx.MenuItem(menu,self.popupLangIds[lang],name)
                    if bmp is not None:
                        mnIt.SetBitmap(bmp)
                    menuLang.AppendItem(mnIt)
                mnIt=wx.MenuItem(menu,self.popupIdLang,_(u'Languages'),subMenu=menuLang)
                mnIt.SetBitmap(self.GetMenuLangBmp())
                self.lMenuLangIndication.append(menu)
                menu.InsertItem(iPos,mnIt)
                iPos+=1
        except:
            import traceback
            traceback.print_exc()
            vtLog.vtLngTB(self.GetName())
            menuLang=None
        return menuLang,iPos
    def __addMenuLang__(self,menu):
        if self.bLangMenu:
            #if len(self.languages)==0:
            #    self.languages=self.doc.GetLanguages()
            #    self.lang=self.doc.GetLang()
            if len(self.languages)>0 and not hasattr(self,'bLangDoc'):
                menuLang=wx.Menu()
                for lang in self.languages:
                    mnIt=wx.MenuItem(menu,self.popupLangIds[lang],lang)
                    f=vtXmlArt.getBitmap(land)  #FIXME move to vtLangArt change key to en, de usw #getattr(images_lang, 'get%sBitmap' % lang)
                    mnIt.SetBitmap(f())
                    menuLang.AppendItem(mnIt)
                mnIt=wx.MenuItem(menu,self.popupIdLang,_(u'Language'),subMenu=menuLang)
                #f=getattr(images_lang, 'get%sBitmap' % self.languages[self.languageIds.index(self.lang)])
                f=vtXmlArt.getBitmap(self.languages[self.languageIds.index(self.lang)])
                mnIt.SetBitmap(f())
                menu.AppendItem(mnIt)
                menu.AppendSeparator()
                return True
            elif self.doc is not None:
                if len(self.languages)==0:
                    languages=self.doc.GetLanguages()
                    if len(languages)>0:
                        self.bLangDoc=True
                        self.languages=languages
                        self.lang=self.doc.GetLang()
                        self.languageIds=self.doc.GetLanguageIds()
                if len(self.languages)>0:
                    menuLang=wx.Menu()
                    if not hasattr(self,'popupIdLang'):
                        self.popupIdLang=wx.NewId()
                        self.popupLangIds={}
                        bAddLangId=True
                    else:
                        bAddLangId=False
                    bmpSel=None
                    try:
                        if len(self.lang)==0:
                            self.lang=self.languages[0][1]
                    except:
                        self.lang=self.languages[0][1]
                    for name,lang,bmp in self.languages:
                        if self.lang==lang:
                            bmpSel=bmp
                        #if not hasattr(self,'popupLangIds'):
                        #    self.popupLangIds=[]
                        #try:
                        #    self.popupLangIds.index(lang)
                        #except:
                        #    self.popupLangIds.append(wx.NewId())
                        #    wx.EVT_MENU(self,self.popupLangIds[i],self.OnTreeLangDoc)
                        if bAddLangId:
                            self.popupLangIds[lang]=wx.NewId()
                            wx.EVT_MENU(self,self.popupLangIds[lang],self.OnTreeLangDoc)
                        mnIt=wx.MenuItem(menu,self.popupLangIds[lang],name)
                        if bmp is not None:
                            mnIt.SetBitmap(bmp)
                        menuLang.AppendItem(mnIt)
                    mnIt=wx.MenuItem(menu,self.popupIdLang,_(u'Language'),subMenu=menuLang)
                    #try:
                    #    f=getattr(vtArt, 'Lang%s' % self.languages[self.languageIds.index(self.lang)].capitalize())
                    #except:
                    #    f=vtArt.LangEn
                    mnIt.SetBitmap(bmpSel)
                    menu.AppendItem(mnIt)
                    menu.AppendSeparator()
                    return True
        return False
    def GetMenuGroupBmp(self):
        try:
            if sGrpId=='normal':
                return vtXmlArt.getBitmap('NoGrp')
            else:
                return self.imgDict['grp'][sGrpId][0]
        except:
            return vtArt.getBitmap(vtArt.Invisible)
    def __createMenuExternalIds__(self,lExternal,d):
        if lExternal is None:
            return
        for tup in lExternal:
            if type(tup[3])==types.ListType:
                dd={}
                d[tup[0]]=(wx.NewId(),dd)
                self.__createMenuExternalIds__(tup[3],dd)
            else:
                id=wx.NewId()
                d[tup[0]]=id
    def __createMenuExternal__(self,obj,mn,lExternal,d,func):
        i=0
        if lExternal is None:
            return 0
        for tup in lExternal:
            if type(tup[3])==types.ListType:
                mnSub=wx.Menu()
                mnIt=wx.MenuItem(mn,d[tup[0]][0],tup[1],subMenu=mnSub)
                if tup[2] is not None:
                    mnIt.SetBitmap(tup[2])
                mn.AppendItem(mnIt)
                self.__createMenuExternal__(obj,mnSub,tup[3],d[tup[0]][1],func)
            else:
                if VERBOSE>20:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'key:%s;%s'%(tup[0],vtLog.pformat(d[tup[0]])),self)
                mnIt=wx.MenuItem(mn,d[tup[0]],tup[1])
                wx.EVT_MENU(obj,d[tup[0]],func)
                if tup[2] is not None:
                    mnIt.SetBitmap(tup[2])
                mn.AppendItem(mnIt)
            i+=1
        return i
    def __createMenuAnalyseIds__(self):
        if not hasattr(self,'popupAnalyseId'):
            self.popupAnalyseId=wx.NewId()
            self.popupAnalyseIds={}
            def createAnalyseIds(lAnalyse,d):
                if lAnalyse is None:
                    return
                for tup in lAnalyse:
                    if type(tup[3])==types.ListType:
                        dd={}
                        d[tup[0]]=(wx.NewId(),dd)
                        createAnalyseIds(tup[3],dd)
                    else:
                        id=wx.NewId()
                        d[tup[0]]=id
            #createAnalyseIds(self.lAnalyse,self.popupAnalyseIds)
            self.__createMenuExternalIds__(self.lAnalyse,self.popupAnalyseIds)
            self.popupAnalyseIdAuditTrail=wx.NewId()
    def AddMenuAnalyse(self,menuAnalyse,obj,iPos=-1):
        try:
            self.__createMenuAnalyseIds__()
            if menuAnalyse is None:
                menuAnalyse=wx.Menu()
                iPos=0
            if iPos<0:
                iPos=menuAnalyse.GetMenuItemCount()
            def createAnalyse(mn,lAnalyse,d):
                i=0
                if lAnalyse is None:
                    return 0
                for tup in lAnalyse:
                    if type(tup[3])==types.ListType:
                        
                        mnSub=wx.Menu()
                        mnIt=wx.MenuItem(mn,d[tup[0]][0],tup[1],subMenu=mnSub)
                        if tup[2] is not None:
                            mnIt.SetBitmap(tup[2])
                        mn.AppendItem(mnIt)
                        createAnalyse(mnSub,tup[3],d[tup[0]][1])
                    else:
                        mnIt=wx.MenuItem(mn,d[tup[0]],tup[1])
                        wx.EVT_MENU(obj,d[tup[0]],self.OnTreeAnalyse)
                        if tup[2] is not None:
                            mnIt.SetBitmap(tup[2])
                        mn.AppendItem(mnIt)
                    i+=1
                return i
            #iPos+=createAnalyse(menuAnalyse,self.lAnalyse,self.popupAnalyseIds)
            iPos+=self.__createMenuExternal__(obj,menuAnalyse,self.lAnalyse,
                    self.popupAnalyseIds,self.OnTreeAnalyse)
            if iPos>0:
                menuAnalyse.AppendSeparator()
                iPos+=1
            mnIt=wx.MenuItem(menuAnalyse,self.popupAnalyseIdAuditTrail,_(u'Audit Trail'))
            #f=getattr(images_lang, 'get%sBitmap' % self.languages[self.languageIds.index(self.lang)])
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.AuditTrail))
            wx.EVT_MENU(obj,self.popupAnalyseIdAuditTrail,self.OnTreeAuditTrail)
            menuAnalyse.AppendItem(mnIt)
            iPos+=1
            #menuAnalyse=wx.Menu()
        except:
            vtLog.vtLngTB(self.GetName())
            try:
                vtLog.vtLngCurWX(vtLog.ERROR,'lAnalyse:%r'%(self.lAnalyse),self)
            except:
                vtLog.vtLngTB(self.GetName())
            menuAnalyse=None
            vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%(vtLog.pformat(self.lAnalyse)),self)
        return menuAnalyse,iPos
    def AddMenuGroup(self,menu,obj,iPos=-1):
        try:
            self.__createMenuGrpIds__()
            if menu is None:
                menu=wx.Menu()
                iPos=0
            if iPos<0:
                iPos=menu.GetMenuItemCount()
            self.__logDebug__(self.lViewInt)
            iPosInt=self.__createMenuExternal__(obj,menu,self.lViewInt,
                    self.popupViewIntIds,self.OnTreeViewInt)
            if iPosInt>0:
                menu.AppendSeparator()
                iPos+=iPosInt
                iPos+=1
            iPosExt=self.__createMenuExternal__(obj,menu,self.lView,
                    self.popupViewIds,self.OnTreeView)
            if iPosExt>0:
                menu.AppendSeparator()
                iPos+=iPosExt
                iPos+=1
            menuGrp=wx.Menu()
            for tup in self.grpMenu:
                sGrpId,sGrpName=tup[0],tup[1]
                mnIt=wx.MenuItem(menu,self.popupGrpIds[sGrpId],sGrpName)
                wx.EVT_MENU(obj,self.popupGrpIds[sGrpId],self.OnTreeGroup)
                if sGrpId=='normal':
                    mnIt.SetBitmap(vtXmlArt.getBitmap('NoGrp'))
                else:
                    try:
                        mnIt.SetBitmap(self.imgLstTyp.GetBitmap(self.imgDict['grp'][sGrpId][0]))
                    except:
                        mnIt.SetBitmap(vtXmlArt.getBitmap('Grp'))
                menuGrp.AppendItem(mnIt)
            mnIt=wx.MenuItem(menu,self.popupGrpId,_(u'Grouping'),subMenu=menuGrp)
            mnIt.SetBitmap(self.GetMenuGroupBmp())
            self.lMenuGroupIndication.append(menu)
            menu.InsertItem(iPos,mnIt)
            iPos+=1
        except:
            #vtLog.vtLngTB(self.GetName())
            menuGrp=None
        return menuGrp,iPos
    def __addMenuGroup__(self,menu):
        if self.bGrpMenu:
            menuGrp=wx.Menu()
            for tup in self.grpMenu:
                sGrpId,sGrpName=tup[0],tup[1]
                mnIt=wx.MenuItem(menu,self.popupGrpIds[sGrpId],sGrpName)
                #f=getattr(images_lang, 'get%sBitmap' % lang)
                #mnIt.SetBitmap(images_treemenu.getGrpBitmap())
                if sGrpId=='normal':
                    mnIt.SetBitmap(vtXmlArt.getBitmap('NoGrp'))
                else:
                    try:
                        mnIt.SetBitmap(self.imgLstTyp.GetBitmap(self.imgDict['grp'][sGrpId][0]))
                    except:
                        mnIt.SetBitmap(vtXmlArt.getBitmap('Grp'))
                menuGrp.AppendItem(mnIt)
            self.popupIdGrp=wx.NewId()
            mnIt=wx.MenuItem(menu,self.popupIdGrp,_(u'Grouping'),subMenu=menuGrp)
            #f=getattr(images_lang, 'get%sBitmap' % self.languages[self.languageIds.index(self.lang)])
            mnIt.SetBitmap(vtXmlArt.getBitmap('Grp'))
            menu.AppendItem(mnIt)
            menu.AppendSeparator()
            return True
        return False
    def __addMenuEdit__(self,menu):
        if self.bMaster or self.bEditMenu:
            #node=self.objTreeItemSel
            node=self.GetSelected()         # 080426:wro
            if node is None:
                node=self.rootNode          #100221:wro fallback
            dAclCur=self.__getNodeCapablitiesACL(node)
            mnIt=wx.MenuItem(menu,self.popupIdAdd,_(u'Add'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Add))
            menu.AppendItem(mnIt)
            mnIt.Enable(dAclCur['add'])
            mnIt=wx.MenuItem(menu,self.popupIdDup,_(u'Duplicate'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Duplicate))
            menu.AppendItem(mnIt)
            mnIt.Enable(dAclCur['dup'])
            mnIt=wx.MenuItem(menu,self.popupIdEdit,_(u'Edit'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Edit))
            menu.AppendItem(mnIt)
            mnIt.Enable(dAclCur['edit'])
            mnIt=wx.MenuItem(menu,self.popupIdDel,_(u'Delete'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Del))
            menu.AppendItem(mnIt)
            mnIt.Enable(dAclCur['del'])
            
            menu.AppendSeparator()
            if self.bClipMenu:
                mnItCut=wx.MenuItem(menu,self.popupIdCut,_('Cut'))
                mnItCut.SetBitmap(vtArt.getBitmap(vtArt.Cut))
                menu.AppendItem(mnItCut)
                mnItCut.Enable(dAclCur['cut'])
                mnItCopy=wx.MenuItem(menu,self.popupIdCopy,_('Copy'))
                mnItCopy.SetBitmap(vtArt.getBitmap(vtArt.Copy))
                menu.AppendItem(mnItCopy)
                mnItCopy.Enable(dAclCur['copy'])
                mnItPaste=wx.MenuItem(menu,self.popupIdPaste,_('Paste'))
                mnItPaste.SetBitmap(vtArt.getBitmap(vtArt.Paste))
                menu.AppendItem(mnItPaste)
                mnItPaste.Enable(dAclCur['paste'])
                menu.AppendSeparator()
            if self.bMoveMenu:
                mnItMv=wx.MenuItem(menu,self.popupIdMove,_('Move Node'))
                mnItMv.SetBitmap(vtArt.getBitmap(vtArt.Move))
                menu.AppendItem(mnItMv)
                mnItMv.Enable(dAclCur['move'])
                if self.idMoveTarget<0:
                    mnItMv.Enable(False)
                    mnItMvTarget=wx.MenuItem(menu,self.popupIdMoveTar,_('Move ...'))
                    mnItMvTarget.SetBitmap(vtArt.getBitmap(vtArt.ImportFree))
                    menu.AppendItem(mnItMvTarget)
                    mnItMv.Enable(False)
                else:
                    nodeMoveTarget=self.doc.getNodeByIdNum(self.idMoveTarget)
                    try:
                        if self.doc.isTagNamePossible2AddAtNode(o.GetTagName(),nodeMoveTarget)==False:
                            mnItMv.Enable(False)
                            vtLog.vtLngCurWX(vtLog.INFO,'id:%08d is invalid target for tagname:%s'%(self.idMoveTarget,o.GetTagName()),self)
                    except:
                        vtLog.vtLngTB(self.GetName())
                    if nodeMoveTarget is not None:
                        try:
                            oMv=self.doc.GetRegByNode(nodeMoveTarget)
                            l=oMv.AddValuesToLstByLst(nodeMoveTarget,None,oMv.FUNCS_GET_4_TREE)
                            s=' '.join(l)
                        except:
                            vtLog.vtLngTB(self.GetName())
                            s=self.doc.getNodeText(nodeMoveTarget,'tag')
                    else:
                        s='???'
                    mnItMvTarget=wx.MenuItem(menu,self.popupIdMoveTar,_('Move <%s>')%s)
                    mnItMvTarget.SetBitmap(vtArt.getBitmap(vtArt.ImportFree))
                    menu.AppendItem(mnItMvTarget)
                menu.AppendSeparator()
            
            try:
                nodeTmp=self.GetSelected()
                if nodeTmp is not None:
                    if self.doc.IsLocked(nodeTmp):
                        mnIt=wx.MenuItem(menu,self.popupIdEditRel,_(u'Release'))
                        mnIt.SetBitmap(vtArt.getBitmap(vtArt.SecUnlocked))
                        menu.AppendItem(mnIt)
                    else:
                        mnIt=wx.MenuItem(menu,self.popupIdEditReq,_(u'Request'))
                        mnIt.SetBitmap(vtArt.getBitmap(vtArt.SecLocked))
                        menu.AppendItem(mnIt)
                    menu.AppendSeparator()
            except:
                vtLog.vtLngTB(self.GetName())
            return True
        return False
    def __addMenuMisc__(self,menu):
        if self.bMiscMenu:
            mnIt=wx.MenuItem(menu,self.popupIdFind,_(u'Find'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Find))
            menu.AppendItem(mnIt)
            menu.AppendSeparator()
            
            mnIt=wx.MenuItem(menu,self.popupIdSort,_(u'Sort'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Sort))
            menu.AppendItem(mnIt)
            #mnIt=wx.MenuItem(menu,self.popupIdAlign,_(u'Align'))
            #menu.AppendItem(mnIt)
            mnIt=wx.MenuItem(menu,self.popupIdReBuild,_(u'Rebuild'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Synch))
            menu.AppendItem(mnIt)
            mnIt=wx.MenuItem(menu,self.popupIdConfig,_(u'Configuration'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Config))
            menu.AppendItem(mnIt)
            if self.doc is not None:
                self.__addMenuCmds__(menu)
            return True
        return False
    def OnTcNodeRightDown(self, event):
        event.Skip()
        #self.x=event.GetX()
        #self.y=event.GetY()
        wx.CallAfter(self.__doShowMenu,event.GetX(),event.GetY())
        #self.__doShowMenu(event.GetX(),event.GetY())
    def _HitTest(self,pos):
        return self.HitTest(pos)
    def __doShowMenu(self,x,y):
        #tn,i=self.HitTest((x,y))
        tn,i=self._HitTest((x,y))
        if tn.IsOk():
            if tn!=self.triSelected:
                vtLog.PrintMsg(_('selected node divers from context menu node, context menu suppressed.'),
                                    self.widLogging)
                return
            #tn=self.triSelected
            id=self.GetPyData(tn)
            if id is None:      # 070328:wro grouping node
                vtLog.PrintMsg(_('selecting grouping node is not allowed, context menu suppressed.'),
                                    self.widLogging)
                return
        self.__createMenuIds__()
        menu=wx.Menu()
        bMenu=False
        bMenu|=self.__addMenuLang__(menu)
        bMenu|=self.__addMenuGroup__(menu)
        bMenu|=self.__addMenuEdit__(menu)
        bMenu|=self.__addMenuMisc__(menu)
        bMenu|=self.__addMenuImportExport__(menu)
        #bMenu|=self.__addMenuExport__(menu)
        
        if bMenu:
            self.PopupMenu(menu,(x,y))
        menu.Destroy()
        
    def AddNode(self,node):
        tri=self.__addElement__(self.tiRoot,node,bRet=True)
        self.triSelected=tri
        if self.TREE_DATA_ID:
            self.objTreeItemSel=self.doc.getKeyNum(node)
        else:
            self.objTreeItemSel=node
        #self.SortChildren(self.GetItemParent(self.triSelected))
        self.SelectItem(tri)
        wx.PostEvent(self,vtXmlTreeItemEdited(self,
                self.triSelected,self.objTreeItemSel))  # forward double click event
        try:
            if hasattr(self,'dToolBar'):
                if self.dToolBar is not None:
                    self.EnableToolBarItems()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnTreeSort(self,event):
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
            self.SortChildren(self.GetItemParent(self.triSelected))
        except:
            pass
        pass
    def OnTreeAlign(self,evt):
        try:
            vtLog.PrintMsg(_(u'alignment ...'),self.widLogging)
            self.doc.AlignDoc()
            vtLog.PrintMsg(_(u'alignment finished.'),self.widLogging)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnTreeReBuild(self,evt):
        try:
            if self.rootNode is not None:
                self.ReBuild()
                #vtLog.PrintMsg(_(u'refrsh finished.'),self.widLogging)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnTreeAuditTrail(self,evt):
        try:
            widPar=vtLog.GetPrintMsgWid(self)
            frm=vtLogAuditTrailFileViewerFrame(widPar)
            frm.SetDoc(self.doc)
            frm.SetTreeMain(self)
            frm.Centre()
            frm.Show()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnTreeConfig(self,evt):
        try:
            oCfg=self.doc.GetRegisteredNode('cfg')
            if oCfg is None:
                return
            node=self.doc.getChild(None,oCfg.GetTagName())
            #vtLog.CallStack('')
            #print node
            if node is None:
                oCfg.DoAdd(self,self.doc.getBaseNode())
            else:
                oCfg.DoEdit(self,self.doc,node)
            self.doc.DoSafe(self.doc.Build)     # 070930:wro lock dom implicit
        except:
            vtLog.vtLngTB(self.GetName())
    def OnTreeCmd(self,evt):
        try:
            eId=evt.GetId()
            cmd=None
            if eId!=self.popupIdCmd:
                for k,id in self.popupCmdIds.items():
                    if id==eId:
                        cmd=k
                        break
            if self.dlgCmd is None:
                self.dlgCmd=vtXmlDomCmdDialog(self)
                self.dlgCmd.Centre()
                self.dlgCmd.SetDoc(self.doc,True)
                dCmd=self.doc.GetCmdDict()
                self.dlgCmd.SetCmdDict(dCmd)
            self.dlgCmd.Clear()
            self.dlgCmd.SelectCmd(cmd)
            self.dlgCmd.SetNode(self.GetSelected())
            self.dlgCmd.ShowModal()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnTreeMessages(self,evt):
        try:
            oMsgs=self.doc.GetRegisteredNode('messages')
            if oMsgs is None:
                return
            nodeMsgs=self.doc.getChild(self.doc.getBaseNode(),oMsgs.GetTagName())
            if nodeMsgs is None:
                nodeMsgs=oMsgs.Create(self.doc.getBaseNode())
                self.doc.AlignNode(nodeMsgs)
            
            oMsgs.DoEdit(self,self.doc,nodeMsgs,False)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnTreeAdd(self,evt):
        if evt is not None:
            evt.Skip()
        try:
            if self.dlgAdd is None:
                self.dlgAdd=vtXmlTreeAddDialog(self.GetParent())
                self.dlgAdd.Centre()
            lst=[]
            #vtLog.CallStack('')
            nodePar=self.GetSelected()      # 080426:wro
            #nodePar=objTreeItemSel
            if nodePar is None:
                #nodePar=self.doc.getBaseNode()
                nodePar=self.rootNode
            #print nodePar
            #print self.doc.getKeyNum(self.rootNode)
            if self.doc is not None:
                lst=self.doc.getPossibleNode(nodePar)
            self.dlgAdd.SetPossibleNodes(self.GetParent(),self.doc,nodePar,lst)
            if self.dlgAdd.ShowModal()>0:
                #node=self.dlgAdd.GetNode()
                sTagName=self.dlgAdd.GetTagName()
                if sTagName is not None:
                    o=self.doc.GetRegisteredNode(sTagName)
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;o found==%d'%(sTagName,o is not None),self)
                    iRes,node=o.DoAdd(self.GetParent(),nodePar)
                
                    #vtLog.vtLngCurWX(vtLog.DEBUG,'node:%s'%(node),self)
                    self.idManualAdd=self.doc.getKey(node)
                    vtLog.vtLngCurWX(vtLog.INFO,'idManAdd:%s'%(self.idManualAdd),self)
                    #self.CallBackDelayed(1000,self.__SelectByIDTolerant__,self.idManualAdd)
                    self.DoCallBackDelayed(1000,self.__SelectByIDTolerant__,self.idManualAdd)
                else:
                    vtLog.vtLngCurWX(vtLog.ERROR,'you are not supposed to be here;tagname is None'%(),self)
        except:
            vtLog.vtLngTB(self.GetName())
    def procCheckCopyOfs(self,node,sTagRef,l):
        sTag=self.doc.getNodeText(node,self.doc.TAGNAME_REFERENCE)
        if sTag.startswith(sTagRef):
            l.append(sTag)
        return 1
    def getCopyRefTag(self,par,node):
        sTagRef=self.doc.getNodeText(node,self.doc.TAGNAME_REFERENCE)
        iE=sTagRef.rfind('.')
        if iE>0:
            sTagMatch=sTagRef[:iE]
        else:
            sTagMatch=sTagRef[:]
        l=[]
        self.doc.procChildsKeys(par,self.procCheckCopyOfs,sTagMatch,l=l)
        iL=len(l)
        if iL>0:
            lNum=[]
            for sT in l:
                iE=sT.rfind('.')
                if iE>0:
                    try:
                        sTL=sT[iE+1:]
                        iNum=int(sTL.strip())
                        lNum.append(iNum)
                    except:
                        pass
            if len(lNum)>0:
                lNum.sort()
                iO=lNum[-1]+1
            else:
                iO=0
            sTag=u'.'.join([sTagMatch,unicode(iO)])
            return sTag
        else:
            iO=0
        return sTagRef
    def OnTreeDuplicate(self,evt):
        vtLog.vtLngCurWX(vtLog.INFO,'',self)
        if self.doc is not None:
            node=self.GetSelected()
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.INFO,'id:%s'%self.doc.getKey(node),self)
            if 0 and node is not None:
                dlg=vtmMsgDialog(self,_(u'Duplicate selected node?') + \
                            u'\n\n' + _(u'label:%s')%self.GetItemText(self.triSelected) ,
                            'vtXmlGrpTree',
                            wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                if dlg.ShowModal()==wx.ID_NO:
                    dlg.Destroy()
                    return
            if node is not None:
                par=self.doc.getParent(node)
                sTagRefNew=self.getCopyRefTag(par,node)
                tmp=self.doc.cloneNode(node,True)
                self.doc.appendChild(par,tmp)
                #sTagName=self.doc.getTagName(tmp)
                try:
                    o=self.doc.GetRegByNode(tmp)
                    iRet=o.DoEdit(self.GetParent(),self.doc,tmp,bModal=True)
                    if iRet<=0:
                        o=None
                except:
                    o=None
                if o is None:
                    try:
                        sTagRef=self.doc.getNodeText(tmp,self.doc.TAGNAME_REFERENCE)
                        #self.doc.setNodeText(tmp,self.doc.TAGNAME_REFERENCE,
                        #            ' '.join([_('Copy of'),sTagRef]))
                        self.doc.setNodeText(tmp,self.doc.TAGNAME_REFERENCE,sTagRefNew)
                    except:
                        pass
                self.doc.addNode(par,tmp)
    def OnTreeDuplicateTo(self,evt):
        if self.idMoveTarget<0:
            return
        vtLog.vtLngCurWX(vtLog.INFO,'',self)
        if self.doc is not None:
            node=self.GetSelected()
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.INFO,'id:%s'%self.doc.getKey(node),self)
            if node is not None:
                par=self.doc.getNodeByIdNum(self.idMoveTarget)
                sTagRefNew=self.getCopyRefTag(par,node)
                tmp=self.doc.cloneNode(node,True)
                self.doc.appendChild(par,tmp)
                #sTagName=self.doc.getTagName(tmp)
                try:
                    o=self.doc.GetRegByNode(tmp)
                    iRet=o.DoEdit(self.GetParent(),self.doc,tmp,bModal=True)
                    if iRet<=0:
                        o=None
                except:
                    o=None
                if o is None:
                    try:
                        sTagRef=self.doc.getNodeText(tmp,self.doc.TAGNAME_REFERENCE)
                        #self.doc.setNodeText(tmp,self.doc.TAGNAME_REFERENCE,
                        #            ' '.join([_('Copy of'),sTagRef]))
                        self.doc.setNodeText(tmp,self.doc.TAGNAME_REFERENCE,sTagRefNew)
                    except:
                        pass
                self.doc.addNode(par,tmp)
        pass

    def OnTreeDel(self,evt):
        vtLog.vtLngCurWX(vtLog.INFO,'',self)
        if self.doc is not None:
            node=self.GetSelected()
            if self.doc.IsNodeKeyValid(node)==False:
                dlg=vtmMsgDialog(self,_(u'This node can not be deleted!')  + \
                            u'\n\n' + _(u'label:%s')%self.GetItemText(self.triSelected) ,
                            'vtXmlGrpTree',
                            wx.OK|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg.Destroy()
                return
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.INFO,'id:%s'%self.doc.getKey(node),self)
            if node is not None:
                dlg=vtmMsgDialog(self,_(u'Delete selected node?')   + \
                            u'\n\n' + _(u'label:%s')%self.GetItemText(self.triSelected) ,
                            'vtXmlGrpTree',
                            wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                if dlg.ShowModal()==wx.ID_NO:
                    dlg.Destroy()
                    return
                dlg.Destroy()
                self.DisableTool()
                self.doc.endEdit(node)
                self.doc.delNode(node)
                return
                if self.bNetResponse==False and 0:
                    self.Delete(self.triSelected)
                    # get selected
                    tn=self.GetSelection()
                    try:
                        if self.TREE_DATA_ID:
                            id=self.GetPyData(tn)
                            self.objTreeItemSel=id              # 080426:wro
                            node=self.doc.getNodeByIdNum(id)
                        else:
                            node=self.GetPyData(tn)
                            self.objTreeItemSel=node            # 080426:wro
                        self.triSelected=tn
                    except:
                        self.objTreeItemSel=None
                        self.triSelected=None
                else:
                    #self.Delete(self.triSelected)    # wro 060810
                    self.triSelected=None
                    self.objTreeItemSel=None
                wx.PostEvent(self,vtXmlTreeItemSelected(self,
                        self.triSelected,self.objTreeItemSel))  # forward double click event
        try:
            if hasattr(self,'dToolBar'):
                if self.dToolBar is not None:
                    self.EnableToolBarItems()
        except:
            vtLog.vtLngTB(self.GetName())
        #evt.Skip()
    def OnTreeExec(self,evt):
        try:
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
            if self.doc is not None:
                node=self.GetSelected()
                if node is None:
                    #vtLog.vtLngCur(vtLog.CRITICAL,'you are not supposed to be here.'%(),self.GetOrigin())
                    wx.PostEvent(self,vtXmlTreeItemExec(self,
                                self.triSelected,
                                self.GetPyData(self.triSelected),None))
                    return
                o=self.doc.GetRegByNode(node)
                if node is None:
                    vtLog.vtLngCur(vtLog.CRITICAL,'you are not supposed to be here.'%(),self.GetOrigin())
                    return
                nodePar=None
                nodePar=self.doc.getParent(node)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.INFO,'id:%s'%self.doc.getKey(node),self)
                o.DoExec(self,node,nodePar)
                if nodePar is not None:
                    idPar=self.doc.getKeyNum(nodePar)
                else:
                    idPar=None
                id=self.doc.getKeyNum(node)
                wx.PostEvent(self,vtXmlTreeItemExec(self,
                            self.triSelected,id,idPar))
        except:
            vtLog.vtLngTB(self.GetName())
    def OnTreeBuild(self,evt):
        try:
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
            if self.doc is not None:
                node=self.GetSelected()
                if node is None:
                    #vtLog.vtLngCur(vtLog.CRITICAL,'you are not supposed to be here.'%(),self.GetOrigin())
                    wx.PostEvent(self,vtXmlTreeItemBuild(self,
                                self.triSelected,
                                self.GetPyData(self.triSelected),None))
                    return
                o=self.doc.GetRegByNode(node)
                if node is None:
                    vtLog.vtLngCur(vtLog.CRITICAL,'you are not supposed to be here.'%(),self.GetOrigin())
                    return
                nodePar=None
                nodePar=self.doc.getParent(node)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.INFO,'id:%s'%self.doc.getKey(node),self)
                o.DoBuild(self,node,nodePar)
                if nodePar is not None:
                    idPar=self.doc.getKeyNum(nodePar)
                else:
                    idPar=None
                id=self.doc.getKeyNum(node)
                wx.PostEvent(self,vtXmlTreeItemBuild(self,
                            self.triSelected,id,idPar))
        except:
            vtLog.vtLngTB(self.GetName())
    def OnTreeFind(self,evt):
        if not hasattr(self,'dlgFind'):
            self.dlgFind=vtXmlFindDialog(self,self.GetName())
            self.dlgFind.SetDoc(self.doc)
            #self.dlg.Bind(vEVT_VTXML_FIND_PANEL_FOUND,OnFound,self.dlg)
            EVT_VTXML_FIND_PANEL_FOUND(self.dlgFind,self.OnFound)
            self.dlgFind.Centre()
        else:
            self.dlgFind.UpdateFilter()
        self.dlgFind.Show(True)
        #evt.Skip()
    def OnFound(self,evt):
        evt.Skip()
        node=evt.GetNode()
        node=self.doc.GetSelectableNode(node)
        if node is not None:
            self.SelectByID(self.doc.getKey(node))
    def OnTreeLang(self,evt):
        evt.Skip()
        eId=evt.GetId()
        sLang=None
        for k in self.popupLangIds.keys():
            if self.popupLangIds[k]==eId:
                sLang=k
                break
        if sLang is not None:
            try:
                idx=self.languages.index(sLang)
                if self.lang!=self.languageIds[idx]:
                    self.lang=self.languageIds[idx]
                    self.UpdateLang()
                if self.bMaster:
                    self.doc.SetLang(self.lang)
            except:
                pass
        else:
            vtLog.vtLngCurWX(vtLog.ERROR,'language not resolved',self)
    def OnTreeNav(self,evt):
        try:
            vtLog.vtLngCurWX(vtLog.INFO,''%(),self)
            evt.Skip()
            eId=evt.GetId()
            for k in self.popupNavIds.iterkeys():
                if self.popupNavIds[k][0]==eId:
                    if k==0:
                        self.SetFocus()
                    else:
                        par=vtLog.GetPrintMsgWid(self)
                        iId=self.popupNavIds[k][1]
                        if iId is not None:
                            w=par.FindWindowById(iId)
                            if w is not None:
                                w.SetFocus()
                            else:
                                vtLog.vtLngCurWX(vtLog.ERROR,
                                        'widget;id:%s not found'%(iId),self)
                        w=self.popupNavIds[k][2]
                        if w is not None:
                            if k=='Return':
                                if hasattr(w,'Apply'):
                                    getattr(w,'Apply')()
                                else:
                                    vtLog.vtLngCurWX(vtLog.ERROR,
                                            'method Apply not found'%(),self)
                            elif k=='Delete':
                                if hasattr(w,'Cancel'):
                                    getattr(w,'Cancel')()
                                else:
                                    vtLog.vtLngCurWX(vtLog.ERROR,
                                            'method Cancel not found'%(),self)
                    break
        except:
            vtLog.vtLngTB(self.GetName())
    def OnTreeLangDoc(self,evt):
        evt.Skip()
        eId=evt.GetId()
        sLang=None
        for k in self.popupLangIds.keys():
            if self.popupLangIds[k]==eId:
                sLang=k
                break
        if sLang is not None:
            if self.lang!=sLang:
                self.lang=sLang
                self.UpdateLang()
            if self.bMaster:
                self.doc.SetLang(self.lang)
        else:
            vtLog.vtLngCurWX(vtLog.ERROR,'language not resolved',self)
        try:
            for mn in self.lMenuLangIndication:
                mnIt=mn.FindItemById(self.popupIdLang)
                if mnIt is not None:
                    mnIt.SetBitmap(self.GetMenuLangBmp())
        except:
            vtLog.vtLngTB(self.GetName())
    def __findExternalById__(self,l,d,eId):
            for tup in l:
                if type(tup[3])==types.ListType:
                    tupRet=findAnalyse(tup[3],d[tup[0]][1],eId)
                    if tupRet is not None:
                        return tupRet
                else:
                    if d[tup[0]]==eId:
                        return tup
            return None
    def __doExternal__(self,eId,tupRet):
        if tupRet is not None:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'id:%d;%s'%(eId,vtLog.pformat(tupRet)),self)
                tupCall=tupRet[3]
                iLen=len(tupCall)
                node=self.GetSelected()
                if iLen==1:
                    tupCall[0](self.doc,node)
                elif iLen==2:
                    tupCall[0](self.doc,node,*tupCall[1])
                elif iLen==3:
                    tupCall[0](self.doc,node,*tupCall[1],**tupCall[2])
            except:
                vtLog.vtLngTB(self.GetName())
    def OnTreeAnalyse(self,evt):
        eId=evt.GetId()
        def findAnalyse(l,d,eId):
            for tup in l:
                if type(tup[3])==types.ListType:
                    tupRet=findAnalyse(tup[3],d[tup[0]][1],eId)
                    if tupRet is not None:
                        return tupRet
                else:
                    if d[tup[0]]==eId:
                        return tup
            return None
        tupRet=self.__findExternalById__(self.lAnalyse,self.popupAnalyseIds,eId)
        vtLog.vtLngCurWX(vtLog.DEBUG,'id:%d;%s'%(eId,vtLog.pformat(tupRet)),self)
        self.__doExternal__(eId,tupRet)
        evt.Skip()
        return
        if tupRet is not None:
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'id:%d;%s'%(eId,vtLog.pformat(tupRet)),self)
                tupCall=tupRet[3]
                iLen=len(tupCall)
                node=self.GetSelected()
                if iLen==1:
                    tupCall[0](self.doc,node)
                elif iLen==2:
                    tupCall[0](self.doc,node,*tupCall[1])
                elif iLen==3:
                    tupCall[0](self.doc,node,*tupCall[1],**tupCall[2])
            except:
                vtLog.vtLngTB(self.GetName())
        evt.Skip()
    def OnTreeViewInt(self,evt):
        eId=evt.GetId()
        tupRet=self.__findExternalById__(self.lViewInt,self.popupViewIntIds,eId)
        self.__doExternal__(eId,tupRet)
        evt.Skip()
    def OnTreeView(self,evt):
        eId=evt.GetId()
        tupRet=self.__findExternalById__(self.lView,self.popupViewIds,eId)
        self.__doExternal__(eId,tupRet)
        evt.Skip()
    def OnTreeGroup(self,evt):
        eId=evt.GetId()
        sGrp=None
        for k in self.popupGrpIds.keys():
            if self.popupGrpIds[k]==eId:
                sGrp=k
                break
        if sGrp is not None:
            try:
                if self.SetGroupingByName(sGrp):
                    self.SetNode(self.rootNode)
            except:
                vtLog.vtLngTB(self.GetName())
        try:
            for mn in self.lMenuGroupIndication:
                mnIt=mn.FindItemById(self.popupIdGrp)
                if mnIt is not None:
                    mnIt.SetBitmap(self.GetMenuGroupBmp())
        except:
            vtLog.vtLngTB(self.GetName())
        evt.Skip()
    def OnTreeEdit(self,event):
        try:
            node=self.GetSelected()
            if node is None:
                vtLog.vtLngCurWX(vtLog.ERROR,'nothing selected',self)
                return
            sTag=self.doc.getTagName(node)
            o=self.doc.GetRegisteredNode(sTag)
            o.DoEdit(self,self.doc,node)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnTreeEditReq(self,event):
        try:
            node=self.GetSelected()
            self.doc.reqLock(node)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnTreeEditRel(self,event):
        try:
            node=self.GetSelected()
            self.doc.relLock(node,'release','')
        except:
            vtLog.vtLngTB(self.GetName())
    def GetExportNodeDlg(self):
        try:
            if self.dlgExpNode is None:
                self.dlgExpNode=vtXmlExportNodeDialog(self)
                self.dlgExpNode.SetDoc(self.doc,self.bNetResponse)
                self.dlgExpNode.Centre()
            return self.dlgExpNode
        except:
            self.dlgExpNode=vtXmlExportNodeDialog(self)
            self.dlgExpNode.SetDoc(self.doc,self.bNetResponse)
            self.dlgExpNode.Centre()
            return self.dlgExpNode
    def GetImportNodeDlg(self):
        try:
            if self.dlgImpNode is None:
                self.dlgImpNode=vtXmlImportNodeDialog(self)
                self.dlgImpNode.SetDoc(self.doc,self.bNetResponse)
                self.dlgImpNode.Centre()
            return self.dlgImpNode
        except:
            self.dlgImpNode=vtXmlImportNodeDialog(self)
            self.dlgImpNode.SetDoc(self.doc,self.bNetResponse)
            self.dlgImpNode.Centre()
            return self.dlgImpNode
    def GetImportOfficeDlg(self):
        try:
            if self.dlgImpOffice is None:
                self.dlgImpOffice=vtXmlImportOfficeDialog(self)
                self.dlgImpOffice.SetDoc(self.doc)
                self.dlgImpOffice.Centre()
            return self.dlgImpOffice
        except:
            self.dlgImpOffice=vtXmlImportOfficeDialog(self)
            self.dlgImpOffice.SetDoc(self.doc)
            self.dlgImpOffice.Centre()
            return self.dlgImpOffice
    def OnTreeImpXml(self,evt):
        try:
            tmp=self.GetSelected()
            if tmp is None:
                tmp=self.doc.getBaseNode()
            if tmp is None:
                dlg=vtmMsgDialog(self,_(u'Please select a node to import at.') ,
                            'vtXmlGrpTree',
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg.Destroy()
                return
            dlg=self.GetImportNodeDlg()
            if dlg is not None:
                #dlg.SetDoc(self.doc)
                nodeSettings=self.doc.getChildForced(self.doc.getRoot(),'settings')
                dlg.SetNode(tmp,nodeSettings)
                dlg.ShowModal()
                return
            dlg=vtmMsgDialog(self,_(u'Sorry, this feature is not released yet!') ,
                            'vtXmlGrpTree',
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnTreeImpExcel(self,evt):
        try:
            tmp=self.GetSelected()
            if tmp is None:
                tmp=self.doc.getBaseNode()
            if tmp is None:
                dlg=vtmMsgDialog(self,_(u'Please select a node to import at.') ,
                            'vtXmlGrpTree',
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg.Destroy()
                return
            dlg=self.GetImportOfficeDlg()
            if dlg is not None:
                nodeSettings=self.doc.getChildForced(self.doc.getRoot(),'settings')
                dlg.SetNode(nodeSettings,tmp)
                iRet=dlg.ShowModal()
                if iRet>0:
                    self.RefreshAllLabels()
                return
            dlg=vtmMsgDialog(self,_(u'Sorry, this feature is not released yet!') ,
                        'vtXmlGrpTree',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnTreeImpCsv(self,evt):
        try:
            if 0:
                pass
            else:
                dlg=vtmMsgDialog(self,_(u'Sorry, this feature is not released yet!') ,
                            'vtXmlGrpTree',
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg.Destroy()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnTreeExpXml(self,evt):
        try:
            tmp=self.GetSelected()
            if tmp is None:
                tmp=self.doc.getBaseNode()
            if tmp is None:
                dlg=vtmMsgDialog(self,_(u'Please select a node to export.') ,
                            'vtXmlGrpTree',
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg.Destroy()
                return
            dlg=self.GetExportNodeDlg()
            if dlg is not None:
                dlg.SetDoc(self.doc)
                nodeSettings=self.doc.getChildForced(self.doc.getRoot(),'settings')
                dlg.SetNode(tmp,nodeSettings)
                dlg.ShowModal()
                return
            dlg=vtmMsgDialog(self,_(u'Sorry, this feature is not released yet!') ,
                            'vtXmlGrpTree',
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnTreeExpExcel(self,evt):
        try:
            if 0:
                pass
            else:
                dlg=vtmMsgDialog(self,_(u'Sorry, this feature is not released yet!') ,
                            'vtXmlGrpTree',
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg.Destroy()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnTreeExpCsv(self,evt):
        try:
            if 0:
                pass
            else:
                dlg=vtmMsgDialog(self,_(u'Sorry, this feature is not released yet!') ,
                            'vtXmlGrpTree',
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg.Destroy()
        except:
            vtLog.vtLngTB(self.GetName())
    def __cleanUpXmlNodes__(self,ti):
        if self.GetChildrenCount(ti)>0:
            cid=self.GetFirstChild(ti)
            bFound=cid[0].IsOk()
            while bFound:
                self.__cleanUpXmlNodes__(cid[0])
                cid=self.GetNextChild(ti,cid[1])
                bFound=cid[0].IsOk()
            #self.__cleanUpXmlNodes__(self.triSelected)
        else:
            td=self.GetItemData(ti)
            if self.TREE_DATA_ID:
                o=self.doc.getNodeByIdNum(td.GetData())
            else:
                o=td.GetData()
            self.doc.delNode(o)
            #if o is not None:
            #    o.parentNode.removeChild(o)
    def DeleteNode(self):
        if self.triSelected is not None:
            #self.__cleanUpXmlNodes__(self.triSelected)
            self.Delete(self.triSelected)
    def __getChildNodes__(self,trRoot,nodes):
        ti=self.GetFirstChild(trRoot)
        while ti[0].IsOk():
            tid=self.GetItemData(ti[0])
            if tid is not None:
                if self.TREE_DATA_ID:
                    n=self.doc.getNodeByIdNum(tid.GetData())
                else:
                    n=tid.GetData()
                if n is not None:
                    nodes.append(n)
            if self.ItemHasChildren(ti[0])==True:
                self.__getChildNodes__(ti[0],nodes)
            #ti=self.GetNextChild(ti[0],ti[1])
            ti=self.GetNextChild(trRoot,ti[1])
    def GetAllChildNodes(self,step_back=0):
        trRoot=self.triSelected
        for i in range(0,step_back):
            trRoot=self.GetItemParent(trRoot)
        nodes=[]
        self.__getChildNodes__(trRoot,nodes)
        return nodes
    def __getItemParentValidNode__(self,ti):
        if ti==self.tiRoot:
            return
        tip=self.GetItemParent(ti)
        o=self.GetPyData(tip)
        if o is None:
            return self.__getItemParentValidNode__(tip)
        return tip
    def __findTreeItemByNode__(self,trRoot,node):
        ti=self.GetFirstChild(trRoot)
        while ti[0].IsOk():
            tid=self.GetItemData(ti[0])
            if tid is not None:
                n=tid.GetData()
                if n==node:
                    return ti[0]
            if self.ItemHasChildren(ti[0])==True:
                nf=self.__findTreeItemByNode__(ti[0],node)
                if nf is not None:
                    return nf
            #ti=self.GetNextChild(ti[0],ti[1])
            ti=self.GetNextChild(trRoot,ti[1])
        return None
    def FindTreeItemByNode(self,node):
        trRoot=self.GetRootItem()
        if self.TREE_DATA_ID:
            id=self.doc.getKeyNum(node)
            return self.__findTreeItemByNode__(trRoot,id)
        else:
            return self.__findTreeItemByNode__(trRoot,node)
    def Select(self,node):
        self.iBlockTreeSel=1
        ti=self.FindTreeItemByNode(node)
        self.__selectItem__(ti)
        self.SelectItem(ti)
        self.EnsureVisible(ti)
        
        #self.Unselect()
        #self.objTreeItemSel=node
        #self.triSelected=self.FindTreeItemByNode(node)
        #self.SelectItem(self.triSelected)
        self.iBlockTreeSel=0
        try:
            if hasattr(self,'dToolBar'):
                if self.dToolBar is not None:
                    self.EnableToolBarItems()
        except:
            vtLog.vtLngTB(self.GetName())
        
        #Self.SelectItem(tri)
    def __deleteFromGrpCache__(self,ti):
        #vtLog.CallStack('')
        if self.TREE_DATA_ID:
            id=self.GetPyData(ti)
        else:
            node=self.GetPyData(ti)
            if node is not None:
                id=self.doc.getKeyNum(node)
            else:
                id=None
        #print self.groupItems
        if id is not None:
            if type(id)==types.DictType:
                return
            if id in self.groupItems:
                del self.groupItems[id]
        #print self.GetItemText(ti),id
        triChild=self.GetFirstChild(ti)
        while triChild[0].IsOk():
            self.__deleteFromGrpCache__(triChild[0])
            triChild=self.GetNextChild(ti,triChild[1])
    def Delete(self,ti):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
        #vtLog.CallStack('')
        try:
            self.__deleteFromGrpCache__(ti)
        except:
            vtLog.vtLngTB(self.GetName())
        vtXmlTree.Delete(self,ti)
        #print self.tiCache.cache.keys()
        #print vtLog.pformat(self.tiCache.cache)
    def OnTcNodeTreeSelChanged(self, event):
        event.Skip()
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            if self.iBlockTreeSel>0:
                #self.iBlockTreeSel=self.iBlockTreeSel-1
                return
            tn=event.GetItem()
            self.__selectItem__(tn)
        except:
            self.__logTB__()
    def OnTcNodeTreeSelChanging(self, event):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            if self.iBlockTreeSel>0:
                #self.iBlockTreeSel=self.iBlockTreeSel-1
                event.Veto()
                return
            tn=event.GetItem()
        except:
            self.__logTB__()
        try:
            if self.__selectItem__(tn)<0:
                event.Veto()
            else:
                event.Skip()
        except:
            self.__logTB__()
            event.Skip()
    def __selectItem__(self,tn,bChange=False):
        if VERBOSE>0:
            self.__logDebug__(''%())
        bChanged=True
        if tn is None:
            self.objTreeItemSel=None
            self.triSelected=None
        else:
            try:
                idOld=self.GetSelectedIDNum()
                if self.TREE_DATA_ID:
                    id=self.GetPyData(tn)
                    if id is None:      # 070328:wro grouping node
                        if bChange:
                            self.Expand(tn)
                            tp=tn
                            while 1:
                                tp=self.GetItemParent(tp)
                                id=self.GetPyData(tp)
                                if id is not None:
                                    break
                            #self.SelectItem(tp)
                            #tn=tp
                            node=None
                            vtLog.PrintMsg(_('selecting grouping node is not allowed, changing to valid parent node ...'),
                                    self.widLogging)
                            return -1
                        else:
                            vtLog.PrintMsg(_('selecting grouping node is not allowed, changing is prevented.'),
                                    self.widLogging)
                            return -1
                    self.objTreeItemSel=id              # 080426:wro
                    ##node=self.doc.getNodeByIdNum(id)
                else:
                    if self.doc.acquired('dom')==False:
                        vtLog.vtLngCurWX(vtLog.CRITICAL,'FIXME:lock doc',self)
                    node=self.GetPyData(tn)
                    id=self.doc.getKeyNum(node)
                    self.objTreeItemSel=node            # 080426:wro
                bChanged=idOld!=id
                self.triSelected=tn
                if bChanged==False:   # 070723:wro deactivate this
                    if vtLog.vtLngIsLogged(vtLog.INFO):
                        vtLog.vtLngCurWX(vtLog.INFO,
                                'node id:%s not changed;skip post event'%(
                                repr(id)),self)
                    return 0
            except:
                self.objTreeItemSel=None
                self.triSelected=None
        wx.PostEvent(self,vtXmlTreeItemSelected(self,
                self.triSelected,self.objTreeItemSel))  # forward double click event
        try:
            if hasattr(self,'dToolBar'):
                if self.dToolBar is not None:
                    self.EnableToolBarItems()
        except:
            vtLog.vtLngTB(self.GetName())
        return 0
    def __refreshLabelByInfos__(self,id,node,tagName,infos):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;infos:%s'%(tagName,vtLog.pformat(infos)),self)
        tup=self.__findNodeByID__(None,id)
        if tup is None:
            return None
        ti=tup[0]
        parent=self.GetItemParent(ti)
        if self.TREE_DATA_ID and (parent.IsOk()==True):
            id=self.GetPyData(parent)
            if id is None:      # 070328:wro grouping node
                while 1:
                    par=self.GetItemParent(parent)
                    if par.IsOk()==False:
                        break
                    parent=par
                    id=self.GetPyData(parent)
                    if id is not None:
                        break
        imgTup=self.__getImagesByInfos__(tagName,infos)
        try:
            imgTup=self.__getImagesByInfos__(tagName,infos)
            tnparent=self.__addGroupingByInfos__(parent,node,tagName,infos)
        except:
            vtLog.vtLngTB(self.GetName())
            tnparent=parent
        #imgTup=self.__getImagesByInfos__(tagName,infos)
        sLabel=''
        if type(self.label)==types.DictType:
            try:
                label=self.label[tagName]
            except:
                try:
                    label=self.label[None]
                except:
                    vtLog.vtLngTB(self.GetName())
                    return
        else:
            label=self.label
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;label:%s'%(tagName,repr(label)),self)
        if type(label) in [types.FunctionType,types.MethodType]:
            #sLabel=label(parent=ti,node=node,tagName=tagName,infos=infos)
            sLabel=label(parent=parent,node=node,tagName=tagName,infos=infos)
        else:
            ##for v in label:
            ##    val=infos[v[0]]
            ##    val=self.__getValFormat__(v,val)
            ##    sLabel=sLabel+val+' '
            #sLabel=u' '.join([self.__getValFormat__(v,infos[v[0]]) 
            #                for v in label if v[0] in infos]+[''])
            def getLblVal(v,infos):
                if type(v)==types.UnicodeType:
                    return v
                elif v[0] is None:
                    return tagName
                elif v[0] in infos:
                    return self.__getValFormat__(v,infos[v[0]])
                return ''
            sLabel=u' '.join([getLblVal(v,infos) for v in label]+[''])
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'sLabel:%s'%(sLabel),self)
        iOfs=0
        if self.isNodeInstance(node,infos):
            iOfs=2
            self.SetItemBold(tn)
        if self.isNodeInvalid(node,infos):
            iOfs=4
        try:
            img=imgTup[iOfs+0]
            imgSel=imgTup[iOfs+1]
        except:
            vtLog.vtLngTB(self.GetName())
        tnOldParent=self.GetItemParent(ti)
        if tnOldParent==tnparent:
            # check project entry type
            self.SetItemText(ti,sLabel)
            imgOld=self.GetItemImage(ti,wx.TreeItemIcon_Normal)
            if imgOld!=img:
                self.SetItemImage(ti,img,wx.TreeItemIcon_Normal)
            imgOld=self.GetItemImage(ti,wx.TreeItemIcon_Selected)
            if imgOld!=imgSel:
                self.SetItemImage(ti,imgSel,wx.TreeItemIcon_Selected)
            tn=ti
        else:
                #self.Freeze()
                #self.Delete(ti)
                tid=wx.TreeItemData()
                if self.TREE_DATA_ID:
                    tid.SetData(self.doc.getKeyNum(node))
                else:
                    tid.SetData(node)
                
                tn=self.AppendItem(tnparent,sLabel,-1,-1,tid)
                id=self.doc.getKeyNum(node)
                self.__deleteFromGrpCache__(ti)
                if self.tiCache is not None:
                    #self.tiCache.AddID(tn,self.doc.getKey(o))
                    #self.tiCache.AddID(tn,infos['|id'])
                    self.tiCache.AddID(tn,id)
                if self.isNodeInstance(node,infos):
                    self.SetItemBold(tn)
                self.SetItemImage(tn,img,wx.TreeItemIcon_Normal)
                self.SetItemImage(tn,imgSel,wx.TreeItemIcon_Selected)
                #self.EnsureVisible(tn)
                #if self.triSelected==ti:
                #    self.SelectItem(tn)
                #    self.triSelected=tn
                self.SetPyData(ti,None)
                #vtLog.CallStack('')
                #print tn
                self.Delete(ti)
                #self.Refresh()
                if self.TREE_BUILD_ON_DEMAND:
                    #if self.doc.hasNodes(node):
                    #    self.SetItemHasChildren(tn,True)
                        #tid=wx.TreeItemData()
                        #tid.SetData(-2)
                        #tc=self.AppendItem(tn,'---',-1,-1,tid)
                    pass
                else:
                    wx.CallAfter(self.thdAddElements.Do,tn,id,True)
        return tn
    def ShowDescEdit(self,*args,**kwargs):
        try:
            self.__logDebug__(''%())
            #from vGuiFrmWX import vGuiFrmWX
            #from vidarc.tool.xml.vtXmlNodeDescNavPanel import vtXmlNodeDescNavPanel
            #frmDesc=vGuiFrmWX(name='frmDesc',kind='frmCls',
            #        lWid=vtXmlNodeDescNavPanel
            #        )
            netMaster=self.doc.GetNetMaster()
            
            sTitle=''.join([self.doc.GetApplAlias(),' ('])
            iBase=kwargs.get('iBase',0)
            if iBase>0:
                sID=self.GetSelectedID()
                node=self.GetSelected()
            else:
                sID=-2
                node=None
            sViewFrm='frmDesc%02d'%(iBase)
            # FIXME:20150426evt
            if sViewFrm in self.dViewFrm:
                frmDesc=self.dViewFrm[sViewFrm]
                pn=frmDesc.GetPanel()
            else:
                from vidarc.tool.xml.vtXmlNodeDescNavFrm import vtXmlNodeDescNavFrm
                frmDesc=vtXmlNodeDescNavFrm(name='frmDesc',kind='frmCls',
                        parent=self,bAnchor=True,bmp=vtArt.getBitmap(vtArt.Book),
                        iArrange=4,widArrange=self.GetParent().GetParent(),title=sTitle,
                        size=(600,600))
                frmDesc.GetWidDescPanel().BindEvent('cmd',self.OnFrmDescCmd)
                frmDesc.frm.Bind(wx.EVT_CLOSE, self.OnFrameCloseDesc)
                self.dViewFrm[sViewFrm]=frmDesc
                
                pn=frmDesc.GetPanel()
                pn.SetDoc(self.doc,bNet=True)
                if self.frmDesc is None:
                    self.frmDesc=[]
                self.frmDesc.append(frmDesc)
            pn.SetNode(node,sID)
            frmDesc.SetDocNode(self.doc,node,sID)
            frmDesc.Show(bFlag=True)
        except:
            self.__logTB__()
    def OnFrmDescCmd(self,evt):
        evt.Skip()
        try:
            self.__logDebug__(''%())
            sCmd=evt.GetCmd()
            sData=evt.GetData()
            if sCmd=='nav':
                self.SelectByID(sData)
        except:
            self.__logTB__()
    def OnFrameCloseDesc(self,evt):
        evt.Skip()
        try:
            o=evt.GetEventObject()
            for it in self.frmDesc:
                if it.frm==o:
                    self.frmDesc.remove(it)
                    return
        except:
            self.__logTB__()
    def ShowAttrEdit(self,*args,**kwargs):
        try:
            self.__logDebug__(''%())
            #from vGuiFrmWX import vGuiFrmWX
            #from vidarc.tool.xml.vtXmlNodeDescNavPanel import vtXmlNodeDescNavPanel
            #frmDesc=vGuiFrmWX(name='frmDesc',kind='frmCls',
            #        lWid=vtXmlNodeDescNavPanel
            #        )
            netMaster=self.doc.GetNetMaster()
            
            sTitle=''.join([self.doc.GetApplAlias(),' ('])
            iBase=kwargs.get('iBase',0)
            if iBase>0:
                sID=self.GetSelectedID()
                node=self.GetSelected()
            else:
                sID=-2
                node=None
            sViewFrm='frmAttr%02d'%(iBase)
            # FIXME:20150426evt
            if sViewFrm in self.dViewFrm:
                frmAttr=self.dViewFrm[sViewFrm]
                pn=frmAttr.GetPanel()
            else:
                from vidarc.tool.xml.vtXmlNodeAttrNavFrm import vtXmlNodeAttrNavFrm
                frmAttr=vtXmlNodeAttrNavFrm(name='frmAttr',kind='frmCls',
                        parent=self,bAnchor=True,bmp=vtArt.getBitmap(vtArt.Attrs),
                        iArrange=4,widArrange=self.GetParent().GetParent(),title=sTitle,
                        size=(600,600))
                frmAttr.GetWidAttrPanel().BindEvent('cmd',self.OnFrmAttrCmd)
                frmAttr.frm.Bind(wx.EVT_CLOSE, self.OnFrameCloseAttr)
                self.dViewFrm[sViewFrm]=frmAttr
                
                pn=frmAttr.GetPanel()
                pn.SetDoc(self.doc,bNet=True)
                if self.frmAttr is None:
                    self.frmAttr=[]
                self.frmAttr.append(frmAttr)
            pn.SetNode(node,sID)
            frmAttr.SetDocNode(self.doc,node,sID)
            frmAttr.Show(bFlag=True)
        except:
            self.__logTB__()
    def OnFrmAttrCmd(self,evt):
        evt.Skip()
        try:
            self.__logDebug__(''%())
            sCmd=evt.GetCmd()
            sData=evt.GetData()
            if sCmd=='nav':
                self.SelectByID(sData)
        except:
            self.__logTB__()
    def OnFrameCloseAttr(self,evt):
        evt.Skip()
        try:
            o=evt.GetEventObject()
            for it in self.frmAttr:
                if it.frm==o:
                    self.frmAttr.remove(it)
                    return
        except:
            self.__logTB__()
from vidarc.tool.xml.vtXmlImportNodeDialog import vtXmlImportNodeDialog
