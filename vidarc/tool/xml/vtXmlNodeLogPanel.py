#Boa:FramePanel:vtXmlNodeLogPanel
#----------------------------------------------------------------------------
# Name:         vtXmlNodeLogPanel.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vtXmlNodeLogPanel.py,v 1.23 2011/01/11 23:48:44 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import wx
    from wx.lib.anchors import LayoutAnchors
    import wx.lib.buttons
    import  wx.lib.anchors as anchors
    from vidarc.tool.net.vNetXmlWxGui import *
    import vidarc.tool.xml.vtXmlDom as vtXmlDom
    
    from vidarc.tool.xml.vtXmlNodePanel import *
    import vidarc.tool.lang.vtLgBase as vtLgBase
    import vidarc.tool.log.vtLog as vtLog
    
    import vidarc.vApps.vHum.vXmlHumInputTreeUsr
    from vidarc.tool.time.vtTime import vtDateTime
    import vidarc.tool.art.vtArt as vtArt
    
    import sys
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VTXMLNODELOGPANEL, wxID_VTXMLNODELOGPANELCBADD, 
 wxID_VTXMLNODELOGPANELCBSET, wxID_VTXMLNODELOGPANELLBLLOG, 
 wxID_VTXMLNODELOGPANELLBLTITLE, wxID_VTXMLNODELOGPANELLBLUSR, 
 wxID_VTXMLNODELOGPANELLSTLOG, wxID_VTXMLNODELOGPANELTXTLOGDESC, 
 wxID_VTXMLNODELOGPANELTXTLOGNAME, wxID_VTXMLNODELOGPANELVIHUM, 
] = [wx.NewId() for _init_ctrls in range(10)]

class vtXmlNodeLogPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_bxsLbl_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblLog, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblTitle, 1, border=0, flag=wx.EXPAND)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbAdd, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbSet, 0, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.EXPAND)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsBt, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblUsr, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viHum, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsLbl, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtLogName, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lstLog, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtLogDesc, 1, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(3)
        parent.AddGrowableCol(0)
        parent.AddGrowableCol(1)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=2, hgap=0, rows=4, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsLbl = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsLbl_Items(self.bxsLbl)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTXMLNODELOGPANEL,
              name=u'vtXmlNodeLogPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(312, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 180))
        self.SetAutoLayout(True)

        self.lstLog = wx.ListView(id=wxID_VTXMLNODELOGPANELLSTLOG,
              name=u'lstLog', parent=self, pos=wx.Point(0, 75),
              size=wx.Size(120, 105), style=wx.LC_REPORT)
        self.lstLog.SetMinSize(wx.Size(-1, -1))
        self.lstLog.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstLogListItemSelected, id=wxID_VTXMLNODELOGPANELLSTLOG)
        self.lstLog.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstLogListColClick,
              id=wxID_VTXMLNODELOGPANELLSTLOG)

        self.txtLogName = wx.TextCtrl(id=wxID_VTXMLNODELOGPANELTXTLOGNAME,
              name=u'txtLogName', parent=self, pos=wx.Point(124, 54),
              size=wx.Size(180, 21), style=0, value=u'')
        self.txtLogName.SetMinSize(wx.Size(-1, -1))

        self.txtLogDesc = wx.TextCtrl(id=wxID_VTXMLNODELOGPANELTXTLOGDESC,
              name=u'txtLogDesc', parent=self, pos=wx.Point(124, 75),
              size=wx.Size(180, 105), style=wx.TE_MULTILINE, value=u'')
        self.txtLogDesc.SetMinSize(wx.Size(-1, -1))

        self.cbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLNODELOGPANELCBADD,
              bitmap=vtArt.getBitmap(vtArt.Add), label=_(u'Add'), name=u'cbAdd',
              parent=self, pos=wx.Point(124, 0), size=wx.Size(76, 30), style=0)
        self.cbAdd.SetMinSize(wx.Size(-1, -1))
        self.cbAdd.Bind(wx.EVT_BUTTON, self.OnCbAddButton,
              id=wxID_VTXMLNODELOGPANELCBADD)

        self.cbSet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLNODELOGPANELCBSET,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Set'),
              name=u'cbSet', parent=self, pos=wx.Point(204, 0), size=wx.Size(76,
              30), style=0)
        self.cbSet.SetMinSize(wx.Size(-1, -1))
        self.cbSet.Bind(wx.EVT_BUTTON, self.OnCbSetButton,
              id=wxID_VTXMLNODELOGPANELCBSET)

        self.lblUsr = wx.StaticText(id=wxID_VTXMLNODELOGPANELLBLUSR,
              label=_(u'User'), name=u'lblUsr', parent=self, pos=wx.Point(0,
              30), size=wx.Size(120, 24), style=wx.ALIGN_RIGHT)
        self.lblUsr.SetMinSize(wx.Size(-1, -1))

        self.lblTitle = wx.StaticText(id=wxID_VTXMLNODELOGPANELLBLTITLE,
              label=_(u'Subject'), name=u'lblTitle', parent=self, pos=wx.Point(60,
              54), size=wx.Size(60, 21), style=wx.ALIGN_RIGHT)
        self.lblTitle.SetMinSize(wx.Size(-1, -1))

        self.lblLog = wx.StaticText(id=wxID_VTXMLNODELOGPANELLBLLOG,
              label=_(u'Logs'), name=u'lblLog', parent=self, pos=wx.Point(0,
              54), size=wx.Size(60, 21), style=0)
        self.lblLog.SetMinSize(wx.Size(-1, -1))

        self.viHum = vidarc.vApps.vHum.vXmlHumInputTreeUsr.vXmlHumInputTreeUsr(id=wxID_VTXMLNODELOGPANELVIHUM,
              name=u'viHum', parent=self, pos=wx.Point(124, 30),
              size=wx.Size(180, 24), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,lWidgets=[self.txtLogName,self.txtLogDesc],
                lEvent=[
                        (self.txtLogName       , wx.EVT_TEXT),
                        (self.txtLogDesc       , wx.EVT_TEXT),
                ])
        self.selIdx=-1
        self.dt=vtDateTime(bLocal=True)
        
        
        self.viHum.SetToolTipString(_(u'human selector'))
        
        self.bSortAsc=False
        self.iSortCol=0
        
        self.lstLog.InsertColumn(0,_(u'Date'),wx.LIST_FORMAT_LEFT,120)
        self.lstLog.InsertColumn(1,_(u'Subject'),wx.LIST_FORMAT_LEFT,130)
        self.lstLog.InsertColumn(2,_(u'User'),wx.LIST_FORMAT_LEFT,70)
        
        self.bModified=False
        self.bAutoApply=False
        #self.SetSuppressNetNotify(True)
        self.bBlock=False
        self.objRegNode=None
        
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
    def SetNetDocs(self,d):
        if 'vHum' in d:
            dd=d['vHum']
            #self.netDocHuman=dd['doc']
            self.viHum.SetDocTree(dd['doc'],dd['bNet'])
    def SetRegNode(self,obj):
        if self.__isLogDebug__():
            self.__logDebug__('class:%s'%(obj.__class__))
        from vidarc.tool.xml.vtXmlNodeLog import vtXmlNodeLog
        if issubclass(obj.__class__,vtXmlNodeLog):
            self.objRegNode=obj
    def __Lock__(self,flag):
        if flag:
            self.lstLog.Enable(False)
            self.viHum.Enable(False)
            self.txtLogName.Enable(False)
            self.txtLogDesc.Enable(False)
            self.cbAdd.Enable(False)
            self.cbSet.Enable(False)
        else:
            self.lstLog.Enable(True)
            if self.doc.GetLoggedInSecLv()>=24:
                self.viHum.Enable(True)
            else:
                self.viHum.Enable(False)
            self.txtLogName.Enable(True)
            self.txtLogDesc.Enable(True)
            if self.objRegNode.IsAclOk(self.doc.ACL_MSK_ADD):
                self.cbAdd.Enable(True)
            else:
                self.cbAdd.Enable(False)
            if self.objRegNode.IsAclOk(self.doc.ACL_MSK_WRITE):
                self.cbSet.Enable(True)
            else:
                self.cbSet.Enable(False)
    def SetAutoApply(self,flag):
        self.bAutoApply=flag
    def __clearBlock__(self):
        self.bBlock=False
    def SetModifiedOld(self,state,obj=None):
        if state:
            if obj is None:
                for obj in self.lstMarkObjs:
                    f=obj.GetFont()
                    f.SetWeight(wx.FONTWEIGHT_BOLD)
                    obj.SetFont(f)
                    obj.Refresh()
            else:
                f=obj.GetFont()
                f.SetWeight(wx.FONTWEIGHT_BOLD)
                obj.SetFont(f)
                obj.Refresh()
        else:
            if obj is None:
                for obj in self.lstMarkObjs:
                    f=obj.GetFont()
                    f.SetWeight(wx.FONTWEIGHT_NORMAL)
                    obj.SetFont(f)
                    obj.Refresh()
                self.viHum.__markModified__(False)
            else:
                f=obj.GetFont()
                f.SetWeight(wx.FONTWEIGHT_NORMAL)
                obj.SetFont(f)
                obj.Refresh()
        self.bModified=state    
    def GetModifiedOld(self):
        return self.bModified
    def __isModified__(self):
        if self.bModified:
            return True
        return False
    def __Clear__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        self.txtLogName.SetValue('')
        self.txtLogDesc.SetValue('')
        self.__unlock__()
        self.selIdx=-1
        self.lstLog.DeleteAllItems()
    def SetNetDocHumanOld(self,netDoc,bNet=False):
        self.netDocHuman=netDoc
        if bNet:
            EVT_NET_XML_CONTENT_CHANGED(self.netDocHuman,self.OnHumanChanged)
        else:
            self.OnHumanChanged(None)
        #EVT_NET_XML_GET_NODE(self.netDocHuman,self.OnHumanChanged)
        #EVT_NET_XML_ADD_NODE(self.netDocHuman,self.OnHumanChanged)
        #EVT_NET_XML_DEL_NODE(self.netDocHuman,self.OnHumanChanged)
        #EVT_NET_XML_REMOVE_NODE(self.netDocHuman,self.OnHumanChanged)
        
    def OnHumanChangedOld(self,evt):
        self.viHum.SetDocTree(self.netDocHuman)
        node=self.netDocHuman.getChild(None,'users')
        self.viHum.SetNodeTree(node)
        if evt is not None:
            evt.Skip()
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        self.viHum.SetDoc(self.doc)
        if dDocs is not None:
            if 'vHum' in dDocs:
                dd=dDocs['vHum']
                #self.netDocHuman=dd['doc']
                self.viHum.SetDocTree(dd['doc'],dd['bNet'])
    def __SetNode__(self,node):
        #if self.bModified==True:
        #    if self.bAutoApply:
        #        self.GetNode(self.node)
        #    else:
                # ask
        #        dlg=wx.MessageDialog(self,u'Do you to apply modified data?' ,
        #                    _(u'vLog Info'),
        #                    wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
        #        if dlg.ShowModal()==wx.ID_YES:
        #            self.GetNode(self.node)
        self.__unlock__()
        self.selIdx=-1
        self.bBlock=True
        self.SetModified(False)
        #vtXmlNodePanel.SetNode(self,node)
        
        if self.node is None:
            return
        if self.doc is None:
            return
        self.__logDebug__('class:%s'%(self.objRegNode.__class__))
        if self.objRegNode.IsAclOk(self.doc.ACL_MSK_READ):
            childs=self.doc.getChilds(self.node,'log')
            for c in childs:
                self.__addInfo__(c)
        #wx.CallAfter(self.__clearBlock__)
    def __GetNode__(self,node=None):
        #vtLog.vtLngCurWX(vtLog.ERROR,'needed?',self)
        #self.SetModified(False)
        #vtXmlNodePanel.GetNode(self,node)
        pass
    def __Clear__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        self.txtLogName.SetValue('')
        self.txtLogDesc.SetValue('')
        self.viHum.Clear()
        
        self.logs={}
        self.lstLog.DeleteAllItems()
        self.selIdx=-1
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        self.viHum.Close()
    def Apply(self,bDoApply=False):
        #self.SetModified(False)
        return False
    def Cancel(self):
        self.SetModified(False)
    def OnCbAddButton(self, event):
        try:
            if self.doc is None:
                return
            #if self.node is None:
            #    return
            
            if self.objRegNode.IsAclOk(self.doc.ACL_MSK_ADD)==False:
                return
            tmp=self.viHum.GetSelectedNode()
            sUsr=self.viHum.GetValue()
            #dt=wx.DateTime.Now()
            self.dt.Now()
            if tmp is not None:
                usrID=self.viHum.GetForeignID()#self.netDocHuman.getKey(tmp)+'@vHum'
            else:
                usrID='-2@vHum'
            if self.node is None:
                vtLog.vtLngCurWX(vtLog.ERROR,'no logs on root',self)
                return
            o,n=self.doc.CreateNode(self.node,'log',
                        DtTm=self.dt.GetDateTimeStr(),
                        Name=self.txtLogName.GetValue(),
                        Desc=self.txtLogDesc.GetValue(),
                        Usr=sUsr,
                        UsrId=usrID)
            if self.bSortAsc:
                idx=self.lstLog.GetItemCount()
            else:
                idx=0
            self.__unlock__()
            self.lstLog.SetItemState(idx,0,wx.LIST_STATE_SELECTED)
            self.__addInfo__(n)
            if self.bSortAsc:
                idx=self.lstLog.GetItemCount()
            else:
                idx=0
            self.selIdx=-1
            self.txtLogName.SetValue('')
            self.txtLogDesc.SetValue('')
            self.viHum.SetModified(False)
            self.SetModified(False)
            
            #wx.CallAfter(self.lstLog.SetItemState,idx,wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
            self.doc.AlignNode(n,iRec=2)
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()
    def __addInfo__(self,node):
        try:
            l=self.objRegNode.AddValuesToLst(node)
            sDateTime=l[0]
            sName=l[1]
            sUsr=', '.join(self.objRegNode.GetUsrInfosLst(node))
            if self.bSortAsc:
                idx=sys.maxint
            else:
                idx=0
            try:
                self.dt.SetStr(sDateTime)
            except:
                if self.objRegNode.IsAclOk(self.doc.ACL_MSK_DEL)==False:
                    return
                dlg=wx.MessageDialog(self,_(u'Delete faulty log entry?') +'\n\nLog:'+' '.join(l),
                            'vtXmlNodeLogPanel',
                            wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                if dlg.ShowModal()==wx.ID_YES:
                    self.doc.delNode(node)
                dlg.Destroy()
                return
            sDateTime=self.dt.GetDateTimeStr(' ')
            self.logs[sDateTime+' '+sName]=node
            index = self.lstLog.InsertImageStringItem(idx, sDateTime, -1)
            self.lstLog.SetStringItem(index,1,sName,-1)
            self.lstLog.SetStringItem(index,2,sUsr,-1)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbSetButton(self, event):
        try:
            if self.doc is None:
                return
            if self.objRegNode.IsAclOk(self.doc.ACL_MSK_WRITE)==False:
                return
            tmp=self.viHum.GetSelectedNode()
            sUsr=self.viHum.GetValue()
            dt=wx.DateTime.Now()
            if tmp is not None:
                usrID=self.viHum.GetForeignID()#self.netDocHuman.getKey(tmp)+'@vHum'
            else:
                usrID='-1@vHum'
            sName=self.txtLogName.GetValue()
            sDesc=self.txtLogDesc.GetValue()
            
            #if self.chcUsr.GetSelection()<1:
            #    return
            #sUsr=self.chcUsr.GetStringSelection()
            it=self.lstLog.GetItem(self.selIdx,0)
            sDate=it.m_text
            it=self.lstLog.GetItem(self.selIdx,1)
            k=sDate+' '+it.m_text
            n=self.logs[k]
            self.logs[k]=None
            
            k=sDate+' '+sName
            
            self.objRegNode.SetName(n,self.txtLogName.GetValue())
            self.objRegNode.SetDesc(n,self.txtLogDesc.GetValue())
            self.objRegNode.SetUsr(n,sUsr)
            self.objRegNode.SetUsrID(n,usrID)
            self.logs[k]=n
            
            self.lstLog.SetStringItem(self.selIdx,1,sName,-1)
            self.lstLog.SetStringItem(self.selIdx,2,sUsr,-1)
            #FIXME: do network update
            #self.doc.setNode(n)
            self.doc.doEdit(n)
            self.SetModified(False)
            self.viHum.SetModified(False)
            #wx.PostEvent(self,vgpRevisionChanged(self,self.node))
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()
    def __unlock__(self):
        try:
            if self.selIdx!=-1:
                it=self.lstLog.GetItem(self.selIdx,0)
                sDate=it.m_text
                it=self.lstLog.GetItem(self.selIdx,1)
                k=sDate+' '+it.m_text
                self.doc.endEdit(self.logs[k])
        except:
            vtLog.vtLngTB(self.GetName())
            
    def OnLstLogListItemSelected(self, event):
        try:
            self.bBlock=True
            self.SetModified(False)
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            self.__unlock__()
            idx=event.GetIndex()
            it=self.lstLog.GetItem(idx,0)
            sDate=it.m_text
            it=self.lstLog.GetItem(idx,1)
            self.txtLogName.SetValue(it.m_text)
            k=sDate+' '+it.m_text
            self.txtLogName.SetValue(it.m_text)
            try:
                self.txtLogDesc.SetValue(self.doc.getNodeText(self.logs[k],'desc'))
            except:
                self.txtLogDesc.SetValue('')
            #print self.logs[k]
            self.viHum.SetValueID('',self.objRegNode.GetUsrID(self.logs[k]))
            #self.viHum.SetNode(self.logs[k])
            #id=self.doc.getKey(self.logs[k])
            self.doc.startEdit(self.logs[k])
            self.selIdx=idx
        except:
            vtLog.vtLngTB(self.GetName())
        wx.CallAfter(self.__clearBlock__)
        event.Skip()

    def OnLstLogListColClick(self, event):
        self.__unlock__()
        self.selIdx=-1
        if event.GetColumn()==0:
            self.bSortAsc=not self.bSortAsc
        childs=self.doc.getChilds(self.node,self.name)
        self.lstLog.DeleteAllItems()
        self.logs={}
        for c in childs:
            self.__addInfo__(c)
        
        event.Skip()

    def OnTxtLogNameText(self, event):
        if self.bBlock==False:
            self.SetModified(True,self.txtLogName)
        event.Skip()

    def OnTxtLogDescText(self, event):
        if self.bBlock==False:
            self.SetModified(True,self.txtLogDesc)
        event.Skip()
        
