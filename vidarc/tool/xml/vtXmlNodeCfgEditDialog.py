#Boa:Dialog:vtXmlNodeCfgEditDialog
#----------------------------------------------------------------------------
# Name:         vtXmlNodeCfgEditDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060216
# CVS-ID:       $Id: vtXmlNodeCfgEditDialog.py,v 1.10 2010/03/03 02:16:18 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.xml.vtXmlNodeCfgPanel
import wx.lib.buttons

from vidarc.tool.xml.vtXmlGrpTree import EVT_VTXMLTREE_ITEM_SELECTED
from vidarc.tool.xml.vtXmlNodeCfgPanel import *

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
from vidarc.tool.xml.vtXmlNodeDialog import *
import vidarc.tool.lang.vtLgBase as vtLgBase

def create(parent):
    return vtXmlNodeCfgEditDialog(parent)

[wxID_VTXMLNODECFGEDITDIALOG, wxID_VTXMLNODECFGEDITDIALOGCBADD, 
 wxID_VTXMLNODECFGEDITDIALOGCBAPPLY, wxID_VTXMLNODECFGEDITDIALOGCBCANCEL, 
 wxID_VTXMLNODECFGEDITDIALOGCBEDIT, wxID_VTXMLNODECFGEDITDIALOGCBEXPORT, 
 wxID_VTXMLNODECFGEDITDIALOGCBIMPORT, wxID_VTXMLNODECFGEDITDIALOGPN, 
] = [wx.NewId() for _init_ctrls in range(8)]


class vtXmlNodeCfgEditDialog(wx.Dialog,vtXmlNodeDialog):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_bxsQuickBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbAdd, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.cbEdit, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbExport, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.cbImport, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsCfg, 1, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.BOTTOM | wx.ALIGN_CENTER)

    def _init_coll_bxsCfg_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.pn, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.LEFT)
        parent.AddSizer(self.bxsQuickBt, 0, border=0, flag=0)

    def _init_sizers(self):
        # generated method, don't edit
        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self.bxsCfg = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsQuickBt = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsCfg_Items(self.bxsCfg)
        self._init_coll_bxsQuickBt_Items(self.bxsQuickBt)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTXMLNODECFGEDITDIALOG,
              name=u'vtXmlNodeCfgEditDialog', parent=prnt, pos=wx.Point(348,
              174), size=wx.Size(360, 300),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE,
              title=u'vTool Config Edit Dialog')
        self.SetClientSize(wx.Size(352, 273))

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLNODECFGEDITDIALOGCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'),
              name=u'cbApply', parent=self, pos=wx.Point(84, 239),
              size=wx.Size(76, 30), style=0)
        self.cbApply.SetMinSize((-1,-1))
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VTXMLNODECFGEDITDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLNODECFGEDITDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(192, 239),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.SetMinSize((-1,-1))
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VTXMLNODECFGEDITDIALOGCBCANCEL)

        self.pn = vidarc.tool.xml.vtXmlNodeCfgPanel.vtXmlNodeCfgPanel(id=wxID_VTXMLNODECFGEDITDIALOGPN,
              name=u'pnCfg', parent=self, pos=wx.Point(4, 4), size=wx.Size(264,
              227), style=0)

        self.cbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLNODECFGEDITDIALOGCBADD,
              bitmap=vtArt.getBitmap(vtArt.Add), label=_(u'Add'), name=u'cbAdd',
              parent=self, pos=wx.Point(272, 4), size=wx.Size(76, 30), style=0)
        self.cbAdd.SetMinSize((-1,-1))
        self.cbAdd.Bind(wx.EVT_BUTTON, self.OnCbAddButton,
              id=wxID_VTXMLNODECFGEDITDIALOGCBADD)

        self.cbEdit = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLNODECFGEDITDIALOGCBEDIT,
              bitmap=vtArt.getBitmap(vtArt.Edit), label=_(u'Edit'),
              name=u'cbEdit', parent=self, pos=wx.Point(272, 38),
              size=wx.Size(76, 30), style=0)
        self.cbEdit.SetMinSize((-1,-1))
        self.cbEdit.Enable(False)
        self.cbEdit.Bind(wx.EVT_BUTTON, self.OnCbEditButton,
              id=wxID_VTXMLNODECFGEDITDIALOGCBEDIT)

        self.cbExport = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLNODECFGEDITDIALOGCBEXPORT,
              bitmap=vtArt.getBitmap(vtArt.Export), label=_(u'Export'), name=u'cbExport',
              parent=self, pos=wx.Point(272, 80), size=wx.Size(76, 30),
              style=0)
        self.cbExport.SetMinSize((-1,-1))
        self.cbExport.Bind(wx.EVT_BUTTON, self.OnCbExportButton,
              id=wxID_VTXMLNODECFGEDITDIALOGCBEXPORT)

        self.cbImport = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLNODECFGEDITDIALOGCBIMPORT,
              bitmap=vtArt.getBitmap(vtArt.Import), label=_(u'Import'), name=u'cbImport',
              parent=self, pos=wx.Point(272, 114), size=wx.Size(76, 30),
              style=0)
        self.cbImport.SetMinSize((-1,-1))
        self.cbImport.Bind(wx.EVT_BUTTON, self.OnCbImportButton,
              id=wxID_VTXMLNODECFGEDITDIALOGCBIMPORT)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self._init_ctrls(parent)
        vtXmlNodeDialog.__init__(self)
        icon = vtArt.getIcon(vtArt.getBitmap(vtArt.Config))
        self.SetIcon(icon)
        
        EVT_VTXMLTREE_ITEM_SELECTED(self.pn.trCfg,self.OnTreeItemSel)
        
        #self.fgsData.Layout()
        #self.fgsData.Fit(self)
    def OnTreeItemSel(self,evt):
        if evt is not None:
            node=evt.GetTreeNodeData()
        else:
            node=self.pn.trCfg.GetSelected()
        if self.pn.doc is not None:
            o=self.pn.doc.GetRegByNode(node)
        else:
            o=None
        self.cbEdit.Enable(o is not None)
    
    def OnCbEditButton(self, event):
        event.Skip()
        node=self.pn.trCfg.GetSelected()
        o=self.pn.doc.GetRegByNode(node)
        if o is not None:
            o.DoEdit(self.pn,self.pn.doc,node)

    def OnCbAddButton(self, event):
        event.Skip()
        self.pn.trCfg.OnTreeAdd(None)

    def OnCbExportButton(self, event):
        event.Skip()
        self.pn.trCfg.OnTreeExpXml(None)

    def OnCbImportButton(self, event):
        event.Skip()
        self.pn.trCfg.OnTreeImpXml(None)
        
