#----------------------------------------------------------------------
# Name:         vtXmlDomConsumer.py
# Purpose:      XML dom-tree consumer
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vtXmlDomConsumer.py,v 1.17 2012/05/27 23:20:29 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from wx import Thread_IsMain
import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

class vtXmlDomConsumer:
    def __init__(self):
        self.doc=None
        self.node=None
        self.nodeId=-2
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        try:
            if self.doc is not None:
                self.doc.DelConsumer(self)
        except:
            pass
        self.doc=None
        self.node=None
    def ClearDoc(self):
        self.doc=None
    def ClearInt(self):
        pass
    def Clear(self):
        self.node=None
        self.nodeId=-2
    def Restart(self):
        """overload this method
        this method is called to allow all xml-dom processing, it may
        continue until method Stop is called."""
        pass
    def Stop(self):
        """overload this method
        this method is called to stop all xml-dom processing, it has to
        be stopped until method Restart is called.
        """
        pass
    def Resume(self):
        """overload this method
        this method is called to allow all xml-dom processing, it may
        continue until method Pause is called."""
        pass
    def Pause(self):
        """overload this method
        this method is called to stop all xml-dom processing, it has to
        be stopped until method Reume is called.
        """
        pass
    def IsRunning(self):
        return False
    def IsBusy(self):
        return False
    def IsSame(self,id):
        if self.nodeId>=0:
            if self.nodeId==long(id):
                return True
        return False
    def IsSameNum(self,id):
        if self.nodeId>=0:
            if self.nodeId==id:
                return True
        return False
    def SetDoc(self,doc,bNet=False):
        if self.doc is not None:
            self.doc.DelConsumer(self)
        self.doc=doc
        if self.doc is not None:
            self.doc.AddConsumer(self,self.Clear)
    def HasDoc(self):
        if self.doc is not None:
            return True
        else:
            return False
    def SetNode(self,node):
        self.node=node
        try:
            self.nodeId=long(self.doc.getKey(node))
        except:
            self.nodeId=-2
    def Check2Clear(self,node):
        if VERBOSE>0:
            vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
        if node is None:
            return
        if Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.doc is not None:
            sId=self.doc.getKey(node)
            if len(sId)>0:
                iId=long(sId)
                if VERBOSE>0:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'act:%d;new:%d'%(self.nodeId,iId),self)
                if self.nodeId==iId:
                    self.ClearInt()
                else:
                    self.Clear()
        else:
            self.Clear()
    def Check2ClearByIdNum(self,id):
        if self.nodeId<-1:
            return
        if Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.doc is not None:
            if self.nodeId==id:
                self.Clear()
