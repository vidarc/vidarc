#----------------------------------------------------------------------
# Name:         vtXmlNodeDescPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120527
# CVS-ID:       $Id: vtXmlNodeDescPanel.py,v 1.7 2015/01/21 12:55:53 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import vidarc.tool.lang.vtLgBase as vtLgBase
    from vGuiFrmWX import vGuiFrmWX
    import imgCtr
    from vidarc.tool.xml.vtXmlNodeGuiPanel import vtXmlNodeGuiPanel
    
    from vidarc.tool.lang.vtLgSelector import vtLgSelector
    from vidarc.tool.lang.vtLgSelector import vEVT_VTLANG_SELECTION_CHANGED
    
    from vidarc.tool.input.vtInputTreeAttr import vtInputTreeAttr
    
    from vidarc.ext.report.veRepBrowseDialog import veRepBrowseDialog
    import vidarc.ext.report.images as imgRep
    import vidarc.tool.art.vtArt as vtArt
    
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vtXmlNodeDescPanel(vtXmlNodeGuiPanel):
    VERBOSE=0
    TAGNAME='description'
    FLEX_SIZER=(3,2)
    def __initCtrl__(self,*args,**kwargs):
        global _
        _=vtLgBase.assignPluginLang('vidarc.')
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        self.SetVerbose(__name__,iVerbose=-1,kwargs=kwargs)
        
        lWid=[
            ('szBoxHor',           1,4,{'name':'boxRef',},[
                ('txtPopup',       1,4,{'name':'txtAttr','mark':False,'mod':False,},None),
                ('szBoxHorLf',           0,4,{'name':'boxShort',},[
                ('tgPopup',        0,4,{'name':'tgLaTex','size':(24,24),'bmp':imgRep.getDocTmplTeXBitmap()},None),
                ('tgPopup',        0,4,{'name':'tgSpecial','size':(24,24),'bmp':vtArt.getBitmap(vtArt.Book)},None),
                ('tgPopup',        0,4,{'name':'tgReference','size':(24,24),'bmp':vtArt.getBitmap(vtArt.Measure)},None),
                ]),
            ]),
            (None,          (0,0),16,None,None),
            #('szBoxHor',           1,4,{'name':'boxRef',},[
            #    ('txtPopup',       1,4,{'name':'txtLaTex'},None),
            #    ('txtPopup',       1,4,{'name':'txtSpecial'},None),
            #    ('txtPopup',       1,4,{'name':'txtRef'},None),
            #]),
            #(None,          (0,0),16,None,None),
            ('szBoxHor',           1,4,{'name':'boxRef',},[
                ('txtStyled',      1,4,{'name':'txtRef',},None),
                ('szBoxVert',       0,0,        {'name':'boxRef',},[
                    (vtLgSelector,  0,20,       {'name':'viLangRef',},None),
                    ('cbBmp',       0,4,        {'name':'cbNavRef','size':(24,24),'bitmap':'Navigate',},
                                                        {'btn':self.OnNavigateRef}),
                    ]),
            ]),
                    ('szBoxVert',           0,4,{'name':'boxCmd',},[
                        ('cbBmp',           0,4,        {'name':'cbApply','size':(32,32),'bitmap':'Apply',},
                                                        {'btn':self.OnApply}),
                        ('cbBmp',           0,4,        {'name':'cbCancel','size':(32,32),'bitmap':'Cancel',},
                                                        {'btn':self.OnCancel}),
                        ]
                    ),
            ('szBoxHor',            1,4,        {'name':'boxRef',},[
                ('txtStyled',       1,4,        {'name':'txtAlt',},None),
                ('szBoxVert',       0,0,        {'name':'boxRef',},[
                    (vtLgSelector,  0,20,       {'name':'viLangAlt',},None),
                    ('cbBmp',       0,4,        {'name':'cbNavAlt','size':(24,24),'bitmap':'Navigate',},
                                                        {'btn':self.OnNavigateAlt}),
                    ]),
            ]),
            
            ]
        
        lWidExt=kwargs.get('lWid',[])
        self.objRegDoc=None
        self.dlgBrowse=None
        vtXmlNodeGuiPanel.__initCtrl__(self,lWid=lWid)
        
        self.viLangRef.Bind(vEVT_VTLANG_SELECTION_CHANGED,self.OnViLangRefChanged)
        self.viLangAlt.Bind(vEVT_VTLANG_SELECTION_CHANGED,self.OnViLangAltChanged)
        self.txtRef.BindEvent('cmd',self.OnCmdRef)
        self.txtAlt.BindEvent('cmd',self.OnCmdAlt)
        
        self.__setUpShortCut__()
        
        lGrowRow=[(1,1),(2,1),]
        lGrowCol=[(0,1),]
        return lGrowRow,lGrowCol
    def __setUpShortCut__(self):
        lWidTop=[
                        ('szBoxHor',            (0,0),(1,1),{'name':'boxCmd',},[
                        ('cbBmp'    ,0,4,{'name':'cbAddRef','bitmap':imgCtr.getSrc1Bitmap()},
                                {'btn':self.OnLatexAddRef}),
                        ('cbBmp'    ,0,4,{'name':'cbAddAlt','bitmap':imgCtr.getSrc2Bitmap()},
                                {'btn':self.OnLatexAddAlt}),
                        ]),
                        ]
        p=vGuiFrmWX(name='popLatex',parent=self.tgLaTex.GetWid(),kind='popup',
                iArrange=0,widArrange=self.tgLaTex.GetWid(),bAnchor=True,
                lGrowRow=[(0,1),(1,1)],
                lGrowCol=[(0,1),(1,1)],
                kwTopCmd={'name':'pnTopCmd','bFlexGridSizer':-1,'lWid':lWidTop},
                lWid=[
                        ('lstCtrl', (0,0),(2,2),{'name':'lstData','size':(300,200),
                                'multiple_sel':False,
                                'cols':[
                                    #['',-3,20,0],
                                    [_(u'tag'),-1,80,1],
                                    [_(u'name'),-1,-1,1],
                                    ],
                                'map':[('tag',1),('name',2),],
                                'tip':_(u'latex short cut'),
                                },None),
                        ])
        p.BindEvent('ok',self.OnLatexPopupOk)
        p.GetName()
        self.tgLaTex.SetWidPopup(p)
        #self.txtLaTex.SetWidPopup(p)

        lWidTop=[
                        ('szBoxHor',            (0,0),(1,1),{'name':'boxCmd',},[
                        ('cbBmp'    ,0,4,{'name':'cbAddRef','bitmap':imgCtr.getSrc1Bitmap()},
                                {'btn':self.OnSpecialAddRef}),
                        ('cbBmp'    ,0,4,{'name':'cbAddAlt','bitmap':imgCtr.getSrc2Bitmap()},
                                {'btn':self.OnSpecialAddAlt}),
                        ]),
                        ]
        p=vGuiFrmWX(name='popSpecial',parent=self.tgSpecial.GetWid(),kind='popup',
                iArrange=0,widArrange=self.tgSpecial.GetWid(),bAnchor=True,
                lGrowRow=[(0,1),(1,1)],
                lGrowCol=[(0,1),(1,1)],
                kwTopCmd={'name':'pnTopCmd','bFlexGridSizer':-1,'lWid':lWidTop},
                lWid=[
                        ('lstCtrl', (0,0),(2,2),{'name':'lstData','size':(300,200),
                                'multiple_sel':False,
                                'cols':[
                                    #['',-3,20,0],
                                    [_(u'tag'),-1,80,1],
                                    [_(u'name'),-1,-1,1],
                                    ],
                                'map':[('tag',1),('name',2),],
                                'tip':_(u'latex short cut'),
                                },None),
                        ])
        p.BindEvent('ok',self.OnSpecialPopupOk)
        p.GetName()
        self.tgSpecial.SetWidPopup(p)
        
        lWidTop=[
                        ('szBoxHor',            (0,0),(1,1),{'name':'boxCmd',},[
                        ('cbBmp'    ,0,4,{'name':'cbAddRef','bitmap':imgCtr.getSrc1Bitmap()},
                                {'btn':self.OnReferenceAddRef}),
                        ('cbBmp'    ,0,4,{'name':'cbAddAlt','bitmap':imgCtr.getSrc2Bitmap()},
                                {'btn':self.OnReferenceAddAlt}),
                        ]),
                        ]
        p=vGuiFrmWX(name='popReference',parent=self.tgReference.GetWid(),kind='popup',
                iArrange=0,widArrange=self.tgReference.GetWid(),bAnchor=True,
                lGrowRow=[(0,1),(1,1)],
                lGrowCol=[(0,1),(1,1)],
                kwTopCmd={'name':'pnTopCmd','bFlexGridSizer':-1,'lWid':lWidTop},
                lWid=[
                        ('lstCtrl', (0,0),(2,2),{'name':'lstData','size':(300,200),
                                'multiple_sel':False,
                                'cols':[
                                    #['',-3,20,0],
                                    [_(u'tag'),-1,80,1],
                                    [_(u'name'),-1,-1,1],
                                    ],
                                'map':[('tag',1),('name',2),],
                                'tip':_(u'latex short cut'),
                                },None),
                        ])
        p.BindEvent('ok',self.OnReferencePopupOk)
        p.GetName()
        self.tgReference.SetWidPopup(p)
        
        lWidTop=[
                        ('szBoxHor',            (0,0),(1,1),{'name':'boxCmd',},[
                        ('cbBmp'    ,0,4,{'name':'cbAddRef','bitmap':imgCtr.getSrc1Bitmap()},
                                {'btn':self.OnAttrAddRef}),
                        ('cbBmp'    ,0,4,{'name':'cbAddAlt','bitmap':imgCtr.getSrc2Bitmap()},
                                {'btn':self.OnAttrAddAlt}),
                        ]),
                        ]
        p=vGuiFrmWX(name='popAttr',parent=self.txtAttr.GetWid(),kind='popup',
                iArrange=0,widArrange=self.txtAttr.GetWid(),bAnchor=True,
                size=(400,400),
                lGrowRow=[(0,1),(1,1)],
                lGrowCol=[(0,1),(1,1)],
                kwTopCmd={'name':'pnTopCmd','bFlexGridSizer':-1,'lWid':lWidTop},
                lWid=[
                        (vtInputTreeAttr, (0,0),(2,2),{'name':'viData','size':(300,200),
                                'multiple_sel':False,
                                'lPropNode':1,'lPropAttr':1,'iOrientation':0,
                                'tip':_(u'node attribute'),
                                },None),
                        ])
        p.BindEvent('ok',self.OnAttrPopupOk)
        p.GetName()
        self.txtAttr.SetWidPopup(p)
    def __populateShortCut__(self):
        if self.IsMainThread()==False:
            return
        try:
            
            if self.doc is None:
                return
            if hasattr(self.doc,'GetReg')==False:
                self.__logError__(''%())
                self.__logWarn__('GetReg method not providev by this doc'%())
                return
            self.objRegDoc=self.doc.GetReg('Doc')
            l=self.objRegDoc.GetLaTex4Choice()
            #p=self.txtLaTex.GetWidPopup()
            p=self.tgLaTex.GetWidPopup()
            ll=[[i,[sTag,sName,i]] for sTag,sName,i in l]
            p.lstData.SetValue(ll)
            
            l=self.objRegDoc.GetCmd4Choice()
            p=self.tgSpecial.GetWidPopup()
            ll=[[i,[sTag,sName,i]] for sTag,sName,i in l]
            p.lstData.SetValue(ll)
            
            l=self.objRegDoc.GetRef4Choice()
            p=self.tgReference.GetWidPopup()
            ll=[[i,[sTag,sName,i]] for sTag,sName,i in l]
            p.lstData.SetValue(ll)
            
            
            if self.dlgBrowse is None:
                trCls=self.objRegDoc.GetTreeClass()
                if trCls is not None:
                    self.dlgBrowse=veRepBrowseDialog(self.GetWid(),trCls)
                    #self.dlgBrowse.Centre()
                else:
                    self.__logWarn__('reg:%s has no tree class definied;'\
                            'browsing will not be possible.'%(self.objRegNode.GetTagName()))
        except:
            self.__logTB__()
    def GetBrowseDlg(self):
        return self.dlgBrowse
    def getLaTexPopupSel(self):
            #p=self.txtLaTex.GetWidPopup()
            p=self.tgLaTex.GetWidPopup()
            l=p.lstData.GetSelected([-2,-1,0,1])
            #print l
            if len(l)>0:
                self.iLaTexIdx=l[0][1]
                sTag=l[0][2]
                #self.txtLaTex.SetValue(sTag)
                #self.txtLaTex.__Close__()
                self.tgLaTex.__Close__()
    def addLaTexSel(self,iWhere):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            if iWhere==0:
                w=self.txtRef
            elif iWhere==1:
                w=self.txtAlt
            else:
                self.__logError__('dst %d unknown'%(iWhere))
                return -1
            
            s=self.objRegDoc.GetLaTex(self.iLaTexIdx)
            w.AddValue(s)
        except:
            self.__logTB__()
    def OnLatexPopupOk(self,evt):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            self.getLaTexPopupSel()
        except:
            self.__logTB__()
    def OnLatexAddRef(self,evt):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            self.getLaTexPopupSel()
            self.addLaTexSel(0)
        except:
            self.__logTB__()
    def OnLatexAddAlt(self,evt):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            self.getLaTexPopupSel()
            self.addLaTexSel(1)
        except:
            self.__logTB__()
    def Apply(self):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        self.__logInfo__(''%())
        try:
            if self.node is not None:
                self.GetNode()
                #id=self.viTag.nodeId
                #wx.PostEvent(self,vtXmlTagApply(self,id))
        except:
            self.__logTB__()
    def Cancel(self):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        self.__logInfo__(''%())
        try:
            #if self.node is not None:
            #    sTag=self.doc.getTagName(self.node)
            #    self.__apply2Dependent__(sTag,'Cancel')
            self.SetNode(self.node)
            #id=self.viTag.nodeId
            #wx.PostEvent(self,vtXmlTagCancel(self,id))
        except:
            self.__logTB__()
    def OnApply(self,evt):
        try:
            self.__logDebug__(''%())
            self.Apply()
        except:
            self.__logTB__()
    def OnCancel(self,evt):
        try:
            self.__logDebug__(''%())
            self.Cancel()
        except:
            self.__logTB__()
    def getAttrPopupSel(self):
            p=self.txtAttr.GetWidPopup()
            #l=p.viData.GetSelected([-2,-1,0,1])
            sID,lV=p.viData.GetValue()
            node=self.doc.getNodeById(sID)
            sVal=self.doc.GetTagNamesRaw(node)
            sAttr=u'tag'
            if lV is None:
                sAttr='tag'
            else:
                if len(lV)>0:
                    sAttr=lV[0]
            self.txtAttr.Close()
            self.txtAttr.SetValue('.'.join([sVal,sAttr]))
            self.iAttrIdx=(sID,sAttr)
    def addAttrSel(self,iWhere):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            if iWhere==0:
                w=self.txtRef
            elif iWhere==1:
                w=self.txtAlt
            else:
                self.__logError__('dst %d unknown'%(iWhere))
                return -1
            
            sID,sAttr=self.iAttrIdx#self.viData.GetValue()
            node=self.doc.getNodeById(sID)
            rootNode=self.doc.GetBaseNode(self.node)
            relNode=self.doc.GetRelBaseNode(self.node)
            
            sTag=self.doc.GetTagNames(node,rootNode,relNode)
            s=''.join(['\\insert_attr','[',sID,']','{',sTag,'}','(',sAttr,')','\n'])
            w.AddValue(s)
        except:
            self.__logTB__()
    def OnAttrPopupOk(self,evt):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            self.getAttrPopupSel()
        except:
            self.__logTB__()
    def OnAttrAddRef(self,evt):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            self.getAttrPopupSel()
            self.addAttrSel(0)
        except:
            self.__logTB__()
    def OnAttrAddAlt(self,evt):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            self.getAttrPopupSel()
            self.addAttrSel(1)
        except:
            self.__logTB__()
            
    def getSpecialPopupSel(self):
            #p=self.txtLaTex.GetWidPopup()
            p=self.tgSpecial.GetWidPopup()
            l=p.lstData.GetSelected([-2,-1,0,1])
            #print l
            if len(l)>0:
                self.iSpecialIdx=l[0][1]
                sTag=l[0][2]
                #self.txtLaTex.SetValue(sTag)
                #self.txtLaTex.__Close__()
                self.tgSpecial.__Close__()
    def addSpecialSel(self,iWhere):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            if iWhere==0:
                w=self.txtRef
            elif iWhere==1:
                w=self.txtAlt
            else:
                self.__logError__('dst %d unknown'%(iWhere))
                return -1
            
            s=self.objRegDoc.GetCmd(self.iSpecialIdx,self)
            #print self.iSpecialIdx,s
            #s=self.objRegDoc.GetLaTex(self.iLaTexIdx)
            w.AddValue(s)
        except:
            self.__logTB__()
    def OnSpecialPopupOk(self,evt):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            self.getSpecialPopupSel()
        except:
            self.__logTB__()
    def OnSpecialAddRef(self,evt):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            self.getSpecialPopupSel()
            self.addSpecialSel(0)
        except:
            self.__logTB__()
    def OnSpecialAddAlt(self,evt):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            self.getLaTexPopupSel()
            self.addLaTexSel(1)
        except:
            self.__logTB__()
            
    def OnReferencePopupOk(self,evt):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            self.getLaTexPopupSel()
        except:
            self.__logTB__()
    def OnReferenceAddRef(self,evt):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            self.getLaTexPopupSel()
            self.addLaTexSel(0)
        except:
            self.__logTB__()
    def OnReferenceAddAlt(self,evt):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            self.getLaTexPopupSel()
            self.addLaTexSel(1)
        except:
            self.__logTB__()
    def procSetNodeById(self,sId):
        try:
            self.__logDebug__('sId:%s'%(sId))
            if self.doc.IsKeyValid(sId)==False:
                self.doc.CallBack(self.Clear)
                node=None
            else:
                node=self.doc.getNodeByIdNum(sId)
            self.doc.CallBack(self.SetNode,node)
            return
            if self.node is None:
                self.doc.CallBack(self.Clear)
            else:
                self.doc.CallBack(self.SetNode,node)
        except:
            self.__logTB__()
    def SetNodeById(self,sId):
        try:
            self.__logDebug__('sId:%s'%(sId))
            if self.doc is None:
                self.__logError__('doc is None')
                return
            self.doc.DoSafe(self.procSetNodeById,sId)
        except:
            self.__logTB__()
    def __getNodeVal_(self,sLang):
        try:
            if self.doc is None:
                return u''
            if self.node is None:
                return u''
            return self.doc.getNodeTextLang(self.node,self.TAGNAME,sLang)
        except:
            self.__logTB__()
        return u''
    def __Clear__(self):
        bDbg=self.GetVerbose(iVerbose=5)
        if bDbg:
            self.__logDebug__(''%())
        self.txtAttr.SetValue(u'')
        self.txtRef.Clear()
        self.txtAlt.Clear()
        #self.txtRef.SetValue(u'')
        #self.txtAlt.SetValue(u'')
        #self.txtLaTex.__Close__()
    def SetRegNode(self,obj):
        bDbg=self.GetVerbose(iVerbose=5)
        if bDbg:
            self.__logDebug__(''%())
        vtXmlNodeGuiPanel.SetRegNode(self,obj)
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            # add code here
            self.viLangRef.SetDoc(doc)
            self.viLangAlt.SetDoc(doc)
            if doc is not None:
                langs=doc.GetLanguages()
                iCount=len(langs)
                if (iCount>1):
                    self.viLangAlt.SetValue(langs[1][1])
            self.OnViLangRefChanged(None)
            self.OnViLangAltChanged(None)
            if hasattr(self.doc,'GetReg'):
                self.__populateShortCut__()
            
            p=self.txtAttr.GetWidPopup()
            p.viData.SetDoc(doc,bNet)
            p.viData.SetNode(None)
            
            if self.dlgBrowse is not None:
                if bDbg:
                    self.__logDebug__(''%())
                self.dlgBrowse.SetDoc(doc,bNet)
                self.dlgBrowse.SetNode(None)
        except:
            self.__logTB__()
    def __SetNode__(self,node):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            # add code here
            self.txtRef.SetValue(self.__getNodeVal_(self.sLangRef))
            self.txtAlt.SetValue(self.__getNodeVal_(self.sLangAlt))
            self.txtRef.UpdateLinks(self.objRegDoc,self.doc,self.node,self,silent=False)
            self.txtAlt.UpdateLinks(self.objRegDoc,self.doc,self.node,self,silent=False)
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            # add code here
            sVal=self.txtRef.GetValue()
            self.doc.setNodeTextLang(self.node,self.TAGNAME,sVal,self.sLangRef)
            sVal=self.txtAlt.GetValue()
            self.doc.setNodeTextLang(self.node,self.TAGNAME,sVal,self.sLangAlt)
        except:
            self.__logTB__()
    def __Close__(self):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            # add code here
            self.txtRef.Close()
            self.txtAlt.Close()
            self.viLangRef.Close()
            self.viLangAlt.Close()
            self.txtAttr.__Close__()
            self.tgLaTex.__Close__()
            self.tgSpecial.__Close__()
            self.tgReference.__Close__()
        except:
            self.__logTB__()
    def __Cancel__(self):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            # add code here
            self.txtLaTex.__Close__()
        except:
            self.__logTB__()
    def __Lock__(self,flag):
        bDbg=self.GetVerbose(iVerbose=5)
        if bDbg:
            self.__logDebug__(''%())
        if flag:
            # add code here
            self.cbApply.Enable(False)
            self.cbCancel.Enable(False)
        else:
            # add code here
            self.cbApply.Enable(True)
            self.cbCancel.Enable(True)
    
    def OnViLangRefChanged(self,evt):
        try:
            self.sLangRef=self.viLangRef.GetValue()
            #self.txtRef.SetValue(self.dVal.get(self.sLangRef,''))
            self.txtRef.SetValue(self.__getNodeVal_(self.sLangRef))
        except:
            self.__logTB__()
    def OnViLangAltChanged(self,evt):
        try:
            self.sLangAlt=self.viLangAlt.GetValue()
            #self.txtAlt.SetValue(self.dVal.get(self.sLangAlt,''))
            self.txtAlt.SetValue(self.__getNodeVal_(self.sLangAlt))
        except:
            self.__logTB__()
    def doBrowse(self,iBrowseable,tup,txt,sLang):
        try:
            if self.IsMainThread()==False:
                return
            
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            dlg=self.GetBrowseDlg()
            if dlg is None:
                sMsg=_(u'No browsing dialog defined!\nThis feature can not be used now.')
                self.ShowMsgDlg(self.OK,sMsg,None)
                return
            nodeRel=self.doc.GetRelBaseNode(self.node)
            dlg.SetActNode(self.node)
            dlg.SetRelNode(nodeRel)
            dlg.SetLang(sLang)
            if bDbg:
                self.__logDebug__('iPos:%d;iBrowseable:%d;tup:%r'%
                    (iPos,iBrowseable,tup))
            iRet,tupInfos=self.objRegDoc.DoBrowse(iBrowseable,tup,self)
            if bDbg:
                self.__logDebug__('iRet:%d;iBrowseable:%d;tupInfos:%e'%
                    (iPos,iBrowseable,tupInfos))
            if bDbg:
                vpc.CallStack(deep=3)
                print iRet,tupInfos
            txt.ChgDocElementInfos(-1,tup,iRet,tupInfos)
        except:
            self.__logTB__()
    def OnCmdRef(self,evt):
        try:
            sCmd=evt.GetCmd()
            sData=evt.GetData()
            self.__logDebug__({'cmd':sCmd,'data':sData})
            iBrowseable=self.objRegDoc.IsBrowseable(sData)
            if iBrowseable>0:
                t=self.txtRef.GetDocElementInfos(-1)
                self.doBrowse(iBrowseable,t,self.txtRef,self.sLangRef)
        except:
            self.__logTB__()
    def OnCmdAlt(self,evt):
        try:
            sCmd=evt.GetCmd()
            sData=evt.GetData()
            self.__logDebug__({'cmd':sCmd,'data':sData})
            iBrowseable=self.objRegDoc.IsBrowseable(sData)
            if iBrowseable>0:
                t=self.txtAlt.GetDocElementInfos(-1)
                self.doBrowse(iBrowseable,t,self.txtAlt,self.sLangRef)
        except:
            self.__logTB__()
    def OnNavigateRef(self,evt):
        try:
            t=self.txtRef.GetDocElementInfos(-1)
            self.__logDebug__(t)
            if len(t[0])>0:
                self.Post('nav',t[0])
        except:
            self.__logTB__()
    def OnNavigateAlt(self,evt):
        try:
            t=self.txtAlt.GetDocElementInfos(-1)
            self.__logDebug__(t)
            if len(t[0])>0:
                self.Post('nav',t[0])
        except:
            self.__logTB__()
