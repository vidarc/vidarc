#----------------------------------------------------------------------------
# Name:         vtXmlNodeDatedValues.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060526
# CVS-ID:       $Id: vtXmlNodeDatedValues.py,v 1.6 2010/03/03 02:16:18 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.time.vtTime import vtDateTime

from vidarc.tool.xml.vtXmlNodeBase import *
try:
    GUI=1
except:
    GUI=0

class vtXmlNodeDatedValues(vtXmlNodeBase):
    NODE_ATTRS=[
            #('tag',None,'tag',None),
        ]
    FUNCS_GET_SET_4_LST=[]
    def __init__(self,tagName='dated',tagNameDated='dated'):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        vtXmlNodeBase.__init__(self,tagName)
        self.tagNameDated=tagNameDated
        self.dt=vtDateTime()
        self.sDtSel=None
    def GetDescription(self):
        return _(u'dated values')
    # ---------------------------------------------------------
    # specific
    def __addDatedLst__(self,node,l):
        sTag=self.doc.getTagName(node)
        if sTag==self.tagNameDated:
            sVal=self.doc.getAttribute(node,'validBy')
            try:
                self.dt.SetDateStr(sVal)
                sVal=self.dt.GetDateStr()
                l.append([sVal,node])
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        return 0
    def __addDatedDict__(self,node,d):
        sTag=self.doc.getTagName(node)
        if sTag==self.tagNameDated:
            sVal=self.doc.getAttribute(node,'validBy')
            try:
                self.dt.SetDateStr(sVal)
                sVal=self.dt.GetDateStr()
                d[sVal]=node
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        return 0
    def GetDatedLst(self,node):
        l=[]
        self.doc.procChildsExt(node,self.__addDatedLst__,l)
        return l
    def GetDatedDict(self,node):
        d={}
        self.doc.procChildsExt(node,self.__addDatedDict__,d)
        return d
    def SelectDated(self,sDt=None):
        if sDt is None:
            self.dt.Now()
            sDt=self.dt.GetDateStr()
        self.sDtSelected=sDt
    def GetDatedNode(self,node,sDt=None,bForced=True):
        if node is None:
            return None
        d=self.GetDatedDict(node)
        if sDt is None:
            keys=d.keys()
            if len(keys)==0:
                self.dt.Now()
                sDt=self.dt.GetDateStr()
            else:
                keys.sort()
                sDt=keys[-1]
        try:
            return d[sDt]
        except:
            if bForced:
                c=self.doc.createSubNode(node,self.tagNameDated)
                self.doc.setAttribute(c,'validBy',sDt)
                return c
        return None
    def GetDatedNodeValidAt(self,node,sDt):
        if node is None:
            return None
        d=self.GetDatedDict(node)
        try:
            if sDt is None:
                self.dt.Now()
                sDt=self.dt.GetDateStr()
            else:
                self.dt.SetDateStr(sDt)
        except:
            vtLog.vtLngCurCls(vtLog.ERROR,'date:%s format not valid'%sDt,self.doc.appl+'_'+self.__class__.__name__)
            return None
        keys=d.keys()
        keys.sort()
        nodeValid=None
        for k in keys:
            if k<=sDt:
                nodeValid=d[k]
            else:
                break
        return nodeValid
    def IsDatedNode(self,node):
        if node is None:
            return False
        sTag=self.doc.getTagName(node)
        if sTag==self.tagNameDated:
            return True
        else:
            return False
    def GetDatedValidAt(self,node):
        if node is not None:
            sVal=self.doc.getAttribute(node,'validBy')
            try:
                self.dt.SetDateStr(sVal)
                sVal=self.dt.GetDateStr()
                return sVal
            except:
                pass
        self.dt.Now()
        sVal=self.dt.GetDateStr()
        return sVal
        

    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_ATTR
    def GetAttrFilterTypes(self):
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        return True
    def Is2Add(self):
        return True
    def IsMultiple(self):
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return False
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x1cIDAT8\x8dcddbf\xa0\x040Q\xa4{\xd4\x80Q\x03F\r\x18D\x06\x00\x00]b\
\x00&\x87\xd5\x92\xeb\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        return None
    def GetAddDialogClass(self):
        return None
    def GetPanelClass(self):
        return None
