#----------------------------------------------------------------------------
# Name:         vtXmlTreeListFull.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060718
# CVS-ID:       $Id: vtXmlTreeListFull.py,v 1.2 2007/07/28 14:43:35 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import wx
import wx.gizmos
import vidarc.tool.xml.vtXmlDom as vtXmlDom

from vidarc.tool.xml.vtXmlGrpTreeList import *

import vidarc.tool.log.vtLog as vtLog

import traceback
import fnmatch,time
import thread,threading

import vidarc.tool.art.vtArt as vtArt

class thdAddElementsFull:
    def __init__(self,par,verbose=0):
        self.tree=par
        self.schedule=[]
        self.running = False
        self.verbose=verbose
        self.timer=None
    def AddElements(self,ti,node,func=None,*args):
        if node is None:
            return
        #tupTree=self.tree.__findNodeByID__(None,node)
        #if tupTree is not None:
        #    return
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'func:%s args:%s'%(repr(func),repr(args)),
                        origin=self.tree.GetName())
        if self.running:
            vtLog.vtLngCallStack(None,vtLog.DEBUG,'thread is still running %d %s'%(self.running,repr(self.schedule)),1,
                        origin=self.tree.GetName())
            vtLog.vtLngCallStack(None,vtLog.DEBUG,'tagname:%s id:%s'%(self.tree.doc.getTagName(node),self.tree.doc.getKey(node)),1,
                        origin=self.tree.GetName())
            self.schedule.append([ti,node,func,args])
            vtLog.vtLngCallStack(None,vtLog.DEBUG,'  scheduled %s'%(repr(self.schedule)),1,
                        origin=self.tree.GetName())
            if self.timer is None:
                self.timer=threading.Timer(1.0,self.StartScheduled)
                self.timer.start()
            return
        self.ti=ti
        self.tiFirst=None
        self.node=node
        try:
            self.lang=self.tree.lang
        except:
            self.lang=None
        self.lastId=''
        self.lastNode=None
        self.lastTreeItem=None
        self.zStart=time.clock()
        self.func=func
        self.args=args
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'',
                        origin=self.tree.GetName())
        self.Start()
    def Start(self):
        self.keepGoing = self.running = True
        self.updateProcessbar=self.tree.bMaster
        #if self.tree.bNotifyAddThread:
        #    self.updateProcessbar=True
        self.updateProcessbar=False
        #if self.verbose:
        #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'parent:%s master:%d'%(self.tree.GetParent().GetParent().GetName(),self.tree.bMaster))
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'',
                        origin=self.tree.GetName())
        thread.start_new_thread(self.Run, ())
    def Stop(self):
        self.keepGoing = False

    def IsRunning(self):
        return self.running
    def __procElem__(self,node,parent):
        self.iAct+=1
        if self.verbose:
            try:
                vtLog.vtLngCallStack(self,vtLog.DEBUG,'ti:%s;args:%s'%(self.tree.GetItemText(parent),args),
                        origin=self.tree.GetName())
            except:
                pass
            
        #if self.verbose:
        #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'parent:%s master:%d'%(self.tree.GetParent().GetParent().GetName(),self.tree.bMaster))
        if self.updateProcessbar:
            wx.PostEvent(self.tree,vtXmlTreeThreadAddElements(self.tree,self.iAct))
        tup=None
        if tup is None:
            self.tree.doc.acquire()
            try:
                #vtLog.vtLngCur(vtLog.DEBUG,'',self.tree.GetName())
                tn=self.tree.__addElementByInfos__(parent,node,None,None,True)
            except:
                #vtLog.vtLngCur(vtLog.DEBUG,'tag:%s;infos:%s'%(tagName,vtLog.pformat(infos)),self.tree.GetName())
                vtLog.vtLngTB(self.tree.GetName())
                self.tree.doc.release()
                self.Stop()
                return -1
            #vtLog.vtLngCur(vtLog.DEBUG,'',self.tree.GetName())
            if self.verbose>0:
                vtLog.vtLngCallStack(self,vtLog.DEBUG,'act:%d'%(self.iAct),
                            origin=self.tree.GetName())
            if self.tiFirst is None:
                self.tiFirst=tn
            #vtLog.vtLngCur(vtLog.DEBUG,'',self.tree.GetName())
            #vtLog.vtLngCur(vtLog.DEBUG,'',self.tree.GetName())
            self.tree.doc.release()
        else:
            tn=tup[0]
            bExpand=False
        #vtLog.vtLngCur(vtLog.DEBUG,'',self.tree.GetName())
        self.tree.doc.procChildsExt(node,self.__procElem__,tn)
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'act:%d'%(self.iAct),
                        origin=self.tree.GetName())
        return 0
    def Run(self):
      try:
        self.tree.iBlockTreeSel=1
        vtLog.vtLngCur(vtLog.DEBUG,'parent:%s master:%d'%(self.tree.GetParent().GetParent().GetName(),self.tree.bMaster),
                        origin=self.tree.GetName())
        self.tree.doc.acquire('thread_tree_add')
        self.tree.doc.acquire('thread')
        self.tree.doc.release('thread_tree_add')
        self.sExpandInfo='|'+self.tree.sExpandName
        #if self.verbose:
        #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'node:%s ti:%s'%(self.node,self.ti))
        self.iAct=0
        vtLog.vtLngCur(vtLog.DEBUG,'act:%d'%(self.iAct),
                        origin=self.tree.GetName())
        if self.updateProcessbar:
            wx.PostEvent(self.tree,vtXmlTreeThreadAddElements(self.tree,self.iAct,self.tree.doc.GetElementAttrCount()))
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'ti:%s;root:%s'%(self.ti,self.tree.tiRoot),
                        origin=self.tree.GetName())
            try:
                vtLog.vtLngCallStack(self,vtLog.DEBUG,'ti:%s'%(self.tree.GetItemText(self.ti)),
                        origin=self.tree.GetName())
            except:
                pass
            try:
                vtLog.vtLngCallStack(self,vtLog.DEBUG,'tiRoot:%s'%(self.tree.GetItemText(self.tree.tiRoot)),
                        origin=self.tree.GetName())
            except:
                pass
            
        bBuildCache=False
        if self.ti==self.tree.tiRoot:
            if self.ti is None:
                # build root
                self.tree.doc.acquire()
                tn=self.tree.__addRootByInfos__(self.node,None,None)
                self.tree.doc.release()
                self.ti=tn
                #self.tree.tiRoot=tn
            self.tiRoot=self.ti
            bBuildCache=True
        else:
            self.tiRoot=None
        try:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'tiRoot:%s'%(self.tree.GetItemText(self.tree.tiRoot)),
                    origin=self.tree.GetName())
        except:
            pass
        if (self.node is not None) and (self.ti is not None):
            self.tiFirst=self.ti
            self.tree.doc.procChildsExt(self.node,self.__procElem__,self.ti)
        if self.tiRoot is not None:
            self.tree.Expand(self.tiRoot)
        if self.updateProcessbar:
            wx.PostEvent(self.tree,vtXmlTreeThreadAddElements(self.tree,0))
        self.tree.doc.release('thread')
        self.running = False
        if self.keepGoing:
            if self.func is not None:
                self.func(*self.args[0])
            if self.updateProcessbar:
                wx.PostEvent(self.tree,vtXmlTreeThreadAddElementsFinished(self.tree,self.tiFirst))
        else:
            if self.updateProcessbar:
                wx.PostEvent(self.tree,vtXmlTreeThreadAddElementsAborted(self.tree,self.tiFirst))
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'kepp:%d running:%d'%(self.keepGoing , self.running),
                        origin=self.tree.GetName())
      except:
          vtLog.vtLngTB(self.tree.GetName())
      self.keepGoing = False
      self.StartScheduled()
      if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'',
                        origin=self.tree.GetName())
      self.tree.iBlockTreeSel=0
    def StartScheduled(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'kepp:%d running:%d'%(self.keepGoing , self.running),1,callstack=True)
        #vtLog.vtLngCallStack(None,vtLog.DEBUG,'scheduled %d'%(len(self.schedule)),1,callstack=False)
        if self.running:
            self.timer=threading.Timer(1.0,self.StartScheduled)
            self.timer.start()
            return
        if len(self.schedule)>0:
            #vtLog.vtLngCallStack(None,vtLog.DEBUG,'scheduled',1,callstack=False)
            tup=self.schedule[0]
            #vtLog.vtLngCallStack(None,vtLog.DEBUG,'tup:%s'%(repr(tup)),1,callstack=False)
            if self.verbose>0:
                vtLog.vtLngCallStack(self,vtLog.DEBUG,'start schedule:%s'%(repr(tup)),
                        origin=self.tree.GetName())
            self.schedule=self.schedule[1:]
            tupTree=self.tree.__findNodeByID__(None,tup[2])
            if tupTree is None:
                self.AddElements(tup[0],tup[1],tup[2],*tup[3])
            else:
                self.StartScheduled()
                return
            #vtLog.vtLngCallStack(None,vtLog.DEBUG,'scheduled started:%s'%(tup),1,callstack=False)
            self.timer=threading.Timer(1.0,self.StartScheduled)
            self.timer.start()
        else:
            try:
                self.tree.doc.unlock('adding')
            except:
                pass
            self.timer=None
    def IsScheduled(self):
        if len(self.schedule)>0:
            return True
        else:
            return False


class vtXmlTreeListFull(vtXmlGrpTreeList):
    def __init__(self, parent, id, pos, size, style, name,cols,
                    master=False,controller=False,
                    gui_update=False,verbose=0):
        if id<0:
            id=wx.NewId()
        self.attrs=[]
        self.attrTreeItemName=_('__attr__')
        vtXmlGrpTreeList.__init__(self,parent, id, pos, size, style, name,cols,
                    master,controller,gui_update,verbose=0)
        self.thdAddElements=thdAddElementsFull(self,verbose=verbose-1)
    def SetNode(self,node):
        vtXmlGrpTreeList.SetNode(self,node)
        img=self.imgDict['elem']['dft'][0]
        imgSel=self.imgDict['elem']['dft'][1]
        tn=self.tiRoot
        self.SetItemImage(tn,img,0,which=wx.TreeItemIcon_Normal)
        self.SetItemImage(tn,imgSel,0,
                                which=wx.TreeItemIcon_Expanded)
        self.SetItemImage(tn,imgSel,0,
                                which=wx.TreeItemIcon_Selected)

    def SetDftGrouping(self):
        self.grouping=[]
        self.label=[[('tag',''),('name','')],[('name','')],[(None,'')]]
        self.attrs=['*']
        self.bGrouping=False
    def SetAttrs(self,attrs):
        self.attrs=attrs
    def SetupImageList(self):
        self.imgDict={'elem':{},'attr':{},'txt':{},'cdata':{},'comm':{},'grp':{}}
        self.imgLstTyp=wx.ImageList(16,16)
        img=vtArt.getBitmap(vtArt.Element)
        self.imgDict['elem']['dft']=[self.imgLstTyp.Add(img)]
        
        img=vtArt.getBitmap(vtArt.ElementSel)
        self.imgDict['elem']['dft'].append(self.imgLstTyp.Add(img))
        
        img=vtArt.getBitmap(vtArt.Attrs)
        self.imgDict['attr']['dft']=[self.imgLstTyp.Add(img)]
        #img=images.getAttributeSelBitmap()
        img=vtArt.getBitmap(vtArt.Attrs)
        self.imgDict['attr']['dft'].append(self.imgLstTyp.Add(img))
        
        img=vtArt.getBitmap(vtArt.ID)
        i=self.imgLstTyp.Add(img)
        self.imgDict['attr']['id']=[i,i]
        
        img=vtArt.getBitmap(vtArt.IDInstance)
        i=self.imgLstTyp.Add(img)
        self.imgDict['attr']['iid']=[i,i]
        
        img=vtArt.getBitmap(vtArt.IDForeign)
        i=self.imgLstTyp.Add(img)
        self.imgDict['attr']['fid']=[i,i]
        
        img=vtArt.getBitmap(vtArt.IDReference)
        i=self.imgLstTyp.Add(img)
        self.imgDict['attr']['ih']=[i,i]
        
        #img=img_synch_tree.getFingerprintBitmap()
        #i=self.imgLstTyp.Add(img)
        #self.imgDict['attr']['fp']=[i,i]
        
        #img=img_synch_tree.getExpandBitmap()
        #i=self.imgLstTyp.Add(img)
        #self.imgDict['attr']['expand']=[i,i]
        
        #img=prjtimer_tree_images.getPrjBitmap()
        #self.imgDict['elem']['project']=[self.imgLstTyp.Add(img)]
        #img=prjtimer_tree_images.getPrjSelBitmap()
        #self.imgDict['elem']['project'].append(self.imgLstTyp.Add(img))
        
        self.SetImageList(self.imgLstTyp)
        pass
    def __addRootByInfos__(self,o,tagName,infos):
        tagName=self.doc.getTagName(o)
        tid=wx.TreeItemData()
        tid.SetData(o)
        tn=self.AddRoot(tagName,self.imgDict['elem']['dft'][0],self.imgDict['elem']['dft'][1],tid)
        self.tiRoot=tn
        #tn=self.GetRoot()
        self.__addNodeAttr__(o,tn)
        return tn
    def __addElementByInfos__(self,parent,o,tagName,infos,bRet=False):
        tagName=self.doc.getTagName(o)
        tid=wx.TreeItemData()
        tid.SetData(o)
        tn=self.AppendItem(parent,tagName,-1,-1,tid)
        sVal=self.doc.getText(o).strip()
        
        self.SetItemText(tn,sVal,1)
        self.__addNodeAttrs__(o,tn)
        # add grouping sub nodes here
        if bRet==True:
            return tn
    def __addNodeAttr__(self,node,attr,sVal,tip):
        try:
            img=self.imgDict['attr'][attr][0]
        except:
            img=-1
        try:
            imgSel=self.imgDict['attr'][attr][1]
        except:
            imgSel=img
        tn=self.AppendItem(tip,attr,img,imgSel)
        self.SetItemText(tn,sVal,1)
        return 0
    def __addNodeAttrs__(self,node,tip):
        if tip is None:
            return
        tagName=self.doc.getTagName(node)
        try:
            img=self.imgDict['attr'][tagName][0]
        except:
            img=self.imgDict['attr']['dft'][0]
        try:
            imgSel=self.imgDict['attr'][tagName][1]
        except:
            imgSel=self.imgDict['attr']['dft'][1]
        tn=self.AppendItem(tip,self.attrTreeItemName,-1,-1,None)
        self.SetItemImage(tn,img,0,which=wx.TreeItemIcon_Normal)
        self.SetItemImage(tn,imgSel,0,
                                which=wx.TreeItemIcon_Expanded)
        self.SetItemImage(tn,imgSel,0,
                                which=wx.TreeItemIcon_Selected)
        tip=tn
        self.doc.procAttrsExt(node,self.__addNodeAttr__,tip)
    def __findAttr__(self,ti,node2Find):
        if node2Find is None:
            return
        tag2Find=self.doc.getTagName(node2Find)
        triChild=self.GetFirstChild(ti)
        while triChild[0].IsOk():
            o=self.GetPyData(triChild[0])
            if self.doc.getTagName(o)==tag2Find:
                return triChild[0]
            triChild=self.GetNextChild(ti,triChild[1])
        return None
    def FindAttr(self,ti,node2Find):
        if ti is None:
            return 
        triChild=self.GetFirstChild(ti)
        while triChild[0].IsOk():
            if self.GetItemText(triChild[0])==self.attrTreeItemName:
                self.__findAttr__(triChild[0],node2Find)
            triChild=self.GetNextChild(ti,triChild[1])
        return self.__findAttr__(ti,node2Find)
    def GetNodeAttrsTup(self,node=None,check=None):
        #ti=self.__findAttr__(self.tiRoot,node)
        if node==None:
            ti=self.triSelected
        else:
            ti=self.FindTreeItem(node)
        if ti is None:
            return []
        lst=[]
        triChild=self.GetFirstChild(ti)
        while triChild[0].IsOk():
            if self.GetItemText(triChild[0])==self.attrTreeItemName:
                triChildAttr=self.GetFirstChild(triChild[0])
                while triChildAttr[0].IsOk():
                    o=self.GetPyData(triChildAttr[0])
                    if o is None:
                        continue
                    if len(self.doc.getAttribute(o,'id'))==0:
                        if check is None:
                            lst.append((triChildAttr[0],o))
                        else:
                            bFound=False
                            for num in check:
                                if len(self.GetItemText(triChild[0],num))>0:
                                    bFound=True
                                    break
                            if bFound:
                                lst.append((triChildAttr[0],o))
                    triChildAttr=self.GetNextChild(triChild[0],triChildAttr[1])
            triChild=self.GetNextChild(ti,triChild[1])
        return lst
    def __getNodesAttrs__(self,node,lst,check=None):
        lstAttr=self.GetNodeAttrsTup(node,check)
        if len(lstAttr)>0:
            lst.append((node,lstAttr))
        for c in self.doc.getChilds(node):
            if len(self.doc.getAttribute(c,'id'))>0:
                self.__getNodesAttrs__(c,lst,check)
    def GetNodesAttrs(self,node=None,check=None):
        if node is None:
            node=self.rootNode
        if node is None:
            return []
        lst=[]
        for c in self.doc.getChilds(node):
            if len(self.doc.getAttribute(c,'id'))>0:
                self.__getNodesAttrs__(c,lst,check)
        return lst
    def SetValue(self,col,val,node=None):
        if node is None:
            ti=self.triSelected
        else:
            ti=self.FindTreeItem(node)
        self.SetTreeItemValue(ti,col,val)
    def SetTreeItemValue(self,ti,col,val):
        if ti is None:
            return -1
        try:
            self.SetItemText(ti,val,col)
        except:
            traceback.print_exc()
        return 0
    def SetTreeItemValueAll(self,ti,col,val):
        if ti is None:
            ti=self.tiRoot
        if ti is None:
            return
        self.SetItemText(ti,val,col)
        triChild=self.GetFirstChild(ti)
        while triChild[0].IsOk():
            self.SetItemText(triChild[0],val,col)
            self.SetTreeItemValueAll(triChild[0],col,val)
            triChild=self.GetNextChild(ti,triChild[1])
        return 0
    def GetValue(self,col,node=None):
        if node is None:
            ti=self.triSelected
        else:
            ti=self.FindTreeItem(node)
        return self.GetTreeItemValue(ti,col)
    def GetTreeItemValue(self,ti,col):
        if ti is None:
            return ''
        return self.GetItemText(ti,col)
    
