#Boa:Dialog:vtXmlExportNodeDialog
#----------------------------------------------------------------------------
# Name:         vtXmlExportNodeDialog.py
# Purpose:      export a selected node to a xml-export file
#               derived from vExportNodeDialog
# Author:       Walter Obweger
#
# Created:      20060505
# CVS-ID:       $Id: vtXmlExportNodeDialog.py,v 1.4 2006/08/29 10:06:24 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.filebrowsebutton
import wx.lib.buttons

import os.path
import vidarc.tool.InOut.fnUtil as fnUtil
from vidarc.tool.xml.vtXmlDomConsumer import * 
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDom import vtXmlDom
from vidarc.tool.time.vtTime import vtDateTime
import vidarc.tool.lang.vtLgBase as vtLgBase

def create(parent):
    return vtXmlExportNodeDialog(parent)

[wxID_VTXMLEXPORTNODEDIALOG, wxID_VTXMLEXPORTNODEDIALOGCBCANCEL, 
 wxID_VTXMLEXPORTNODEDIALOGCBGENERATE, wxID_VTXMLEXPORTNODEDIALOGCHCINCLUDE, 
 wxID_VTXMLEXPORTNODEDIALOGFBBFN, wxID_VTXMLEXPORTNODEDIALOGPBEXP, 
] = [wx.NewId() for _init_ctrls in range(6)]

class vtXmlExportNodeDialog(wx.Dialog,vtXmlDomConsumer):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbGenerate, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(16, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.fbbFN, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.chcInclude, 0, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.pbExp, 0, border=8,
              flag=wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.TOP | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=5, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTXMLEXPORTNODEDIALOG,
              name=u'vtXmlExportNodeDialog', parent=prnt, pos=wx.Point(349,
              136), size=wx.Size(311, 154),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE,
              title=u'vtXml Export Node Dialog')
        self.SetClientSize(wx.Size(303, 127))

        self.fbbFN = wx.lib.filebrowsebutton.FileBrowseButton(buttonText='...',
              dialogTitle=_(u'Choose a file'), fileMask=u'*.xml',
              id=wxID_VTXMLEXPORTNODEDIALOGFBBFN, labelText=_(u'Export File:'),
              parent=self, pos=wx.Point(4, 4), size=wx.Size(295, 29),
              startDirectory='.', style=0)

        self.chcInclude = wx.CheckBox(id=wxID_VTXMLEXPORTNODEDIALOGCHCINCLUDE,
              label=_(u'include children'), name=u'chcInclude', parent=self,
              pos=wx.Point(4, 37), size=wx.Size(295, 13), style=0)
        self.chcInclude.SetValue(True)
        self.chcInclude.SetMinSize(wx.Size(-1, -1))

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLEXPORTNODEDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(159, 91),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.SetMinSize(wx.Size(-1, -1))
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VTXMLEXPORTNODEDIALOGCBCANCEL)

        self.cbGenerate = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLEXPORTNODEDIALOGCBGENERATE,
              bitmap=vtArt.getBitmap(vtArt.Build), label=_(u'Export'),
              name=u'cbGenerate', parent=self, pos=wx.Point(67, 91),
              size=wx.Size(76, 30), style=0)
        self.cbGenerate.SetMinSize(wx.Size(-1, -1))
        self.cbGenerate.Bind(wx.EVT_BUTTON, self.OnGcbbGenerateButton,
              id=wxID_VTXMLEXPORTNODEDIALOGCBGENERATE)

        self.pbExp = wx.Gauge(id=wxID_VTXMLEXPORTNODEDIALOGPBEXP, name=u'pbExp',
              parent=self, pos=wx.Point(8, 58), range=100, size=wx.Size(287,
              13), style=wx.GA_SMOOTH | wx.GA_HORIZONTAL)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self._init_ctrls(parent)
        vtXmlDomConsumer.__init__(self)
        icon = vtArt.getIcon(vtArt.getBitmap(vtArt.Export))
        self.SetIcon(icon)
        self.nodeSettings=None
        self.sExpDN=os.path.expanduser('~')
        self.dt=vtDateTime(True)
    def Clear(self):
        vtXmlDomConsumer.Clear(self)
        self.nodeSettings=None
    def Stop(self):
        if self.IsShown():
            self.EndModal(0)
    def IsBusy(self):
        return self.IsShown()
    def SetDoc(self,doc,bNet=False):
        vtXmlDomConsumer.SetDoc(self,doc)
    def SetNode(self,node,nodeSettings):
        try:
            vtXmlDomConsumer.SetNode(self,node)
            self.dt.Now()
            
            self.nodeSettings=nodeSettings
            self.sExpDN=''
            if self.nodeSettings is not None:
                self.sExpDN=self.doc.getNodeText(nodeSettings,'exportdn')
            if len(self.sExpDN)<=0:
                self.sExpDN=os.path.expanduser('~')
            appl=self.doc.appl
            sTagName=self.doc.getTagName(self.node)
            sRef=self.doc.getNodeText(self.node,self.doc.TAGNAME_REFERENCE)
            sFN='_'.join([appl,sTagName,sRef,self.dt.GetDateSmallStr(),self.dt.GetTimeSmallStr()])
            sFN+='.xml'
            sFN=fnUtil.replaceSuspectChars(sFN)
            self.fbbFN.SetValue(os.path.join(self.sExpDN,sFN))
        except:
            vtLog.vtLngTB(self.GetName())
    def GetSelNode(self):
        return self.selNode
    
    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
        
    def OnGcbbGenerateButton(self, event):
        try:
            if self.node is None:
                vtLog.vtLngCurWX(vtLog.ERROR,'no node selected',self)
                dlg=wx.MessageDialog(self,_(u'Please select a node to export.') ,
                            'vtXml Export Node Dialog',
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg.Destroy()
                return
            sFullFN=self.fbbFN.GetValue()
            
            if len(sFullFN)<=0:
                vtLog.vtLngCurWX(vtLog.ERROR,'no file selected',self)
                dlg=wx.MessageDialog(self,_(u'Please select a filename.') ,
                            'vtXml Export Node Dialog',
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg.Destroy()
                return
            sFN=os.path.split(sFullFN)[1]
            sTmp=fnUtil.replaceSuspectChars(sFN)
            if sTmp!=sFN:
                #self.txtPrjTaskFN.SetStyle(0, len(sPrjTaskFN), wx.TextAttr("RED", "YELLOW"))
                vtLog.vtLngCurWX(vtLog.ERROR,'faulty filename;rawFN:%s;convFN:%s'%(sFN,sTmp),self)
                dlg=wx.MessageDialog(self,_(u'Please select a valid filename.') ,
                            'vtXml Export Node Dialog',
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg.Destroy()
                return
            
            # build export file
            docExp=vtXmlDom()
            docExp.New(root="export_node")
            rootNode=docExp.getRoot()
            expNode=docExp.createSubNode(rootNode,'export',False)
            lst=[]
            lst.append({'tag':'date','val':self.dt.GetDateStr()})
            lst.append({'tag':'time','val':self.dt.GetTimeStr()})
            lst.append({'tag':'datetime','val':self.dt.GetDateTimeStr()})
            docExp.createChildByLst(expNode,'create',lst,False)
            c=docExp.createSubNode(expNode,'data',False)
            node=self.doc.cloneNode(self.node,self.chcInclude.GetValue())
            docExp.removeInternalNodes(node)
            docExp.appendChild(c,node)
            
            docExp.AlignDoc()
            docExp.Save(sFullFN)
            docExp.Close()
            del docExp
            dn=os.path.split(sFullFN)[0]
            if self.nodeSettings is not None:
                self.doc.setNodeText(self.nodeSettings,'exportdn',dn)
                self.doc.AlignNode(self.nodeSettings)
            self.EndModal(1)
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()
