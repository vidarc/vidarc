#----------------------------------------------------------------------------
# Name:         vtXmlNodeBase.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtXmlNodeBase.py,v 1.47 2013/11/10 20:19:01 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
import types

from vidarc.tool.xml.vtXmlFilterType import *
from vidarc.tool.sec.vtSecXmlAclDom import vtSecXmlAclDom
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase

import vidarc.config.vcCust as vcCust
try:
    if vcCust.is2Import(__name__):
        #print dir().index('wx')
        import cStringIO,array
        from wx import BitmapFromImage as wxBitmapFromImage
        from wx import ImageFromStream as wxImageFromStream
        from vidarc.tool.xml.vtXmlNodeBaseEditDialog import *
        from vidarc.tool.xml.vtXmlNodeBaseAddDialog import *
        GUI=1
        vLogFallBack.logDebug('GUI objects imported',__name__)
    else:
        GUI=0
        vLogFallBack.logDebug('GUI objects import skipped',__name__)
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
    GUI=0
vLogFallBack.logDebug('import;done',__name__)

class vtXmlNodeBase(vtLog.vtLogOrigin,vtLog.vtLogMixIn):
    NODE_ATTRS=[]           #[('tag',None,'tag',None)]
    FUNCS_GET_SET_4_LST=[]  #['Tag','Name']
    FUNCS_GET_4_TREE=[]     #['Tag','Name']
    FMT_GET_4_TREE=None     #[('Tag',''),('Name','')]
    GRP_GET_4_TREE=None     #[('Tag',''),('Name','')]
    GRP_MENU_4_TREE=None    #[('normal'   , _(u'normal'), 
                             #       [('Closed','clsd'),('CltShort','')] , 
                             #       [('name','')],),
                             #]
    ATTR_HIDDEN=[]
    #NODE_ATTRS_REQ=[]       #['tag','name']
    #DICT_ATTRS_REQ={}
    def __init__(self,tagName='node'):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self.tagName=tagName
        self.lstChilds=[]
        self.dDlgEdit={}
        self.dDlgAdd={}
        self.dTreeInfo={}
        self.doc=None
        self.lValid=None
        self.lInvalid=None
        self.lNotCont=None
        self.__SetOrigin__()
        #if self.GRP_MENU_4_TREE is None:
        #    self.SetGroupingByName(self.GRP_MENU_4_TREE[0][0])
    def __SetOrigin__(self):
        if self.doc is None:
            vtLog.vtLogOrigin.__init__(self,None,
                    u'.'.join(['nodoc',self.tagName]))
        else:
            vtLog.vtLogOrigin.__init__(self,None,
                    u'.'.join([self.doc.GetOrigin(),self.tagName]))
    def __del__Old(self):
        vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
        try:
            del self.lstChilds
            del self.dDlgEdit
            del self.dDlgAdd
            del self.dTreeInfo
            del self.lValid
            del self.lInvalid
            del self.lNotCont
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def SetGroupingByName(self,sGrp):
        if self.GRP_MENU_4_TREE is None:
            return False
        for tup in self.GRP_MENU_4_TREE:
            if tup[0]==sGrp:
                self.FUNCS_GET_4_TREE=[k for k,fmt in tup[3]]
                for k,fmt in tup[2]:
                    if k not in self.FUNCS_GET_4_TREE:
                        self.FUNCS_GET_4_TREE.append(k)
                self.GRP_GET_4_TREE=tup[2]
                self.FMT_GET_4_TREE=tup[3]
                
                try:
                    if VERBOSE>0:
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCurCls(vtLog.DEBUG,'grp:%s;func:%s;fmt:%s'%
                                    (vtLog.pformat(self.GRP_GET_4_TREE),
                                    vtLog.pformat(self.FUNCS_GET_4_TREE),
                                    vtLog.pformat(self.FMT_GET_4_TREE)),self)
                except:
                    vtLog.vtLngTB(self.__class__.__name__)
                #vtLog.CallStack('')
                #print 'grp',self.GRP_GET_4_TREE
                #print 'func',self.FUNCS_GET_4_TREE
                #print 'fmt',self.FMT_GET_4_TREE
                return True
        return False
    def AddValidChild(self,sTagName):
        if self.lValid is None:
            self.lValid=[]
        if sTagName not in self.lValid:
            self.lValid.append(sTagName)
    def AddInvalidChild(self,sTagName):
        if self.lInvalid is None:
            self.lInvalid=[]
        if sTagName not in self.lInvalid:
            self.lInvalid.append(sTagName)
    def AddNotContainedChild(self,sTagName):
        if self.lNotCont is None:
            self.lNotCont=[]
        if sTagName not in self.lNotCont:
            self.lNotCont.append(sTagName)
    def GetTagName(self):
        return self.tagName
    def GetDescription(self):
        return _(u'node')
    def _acl_normal(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def _acl_all(self):
        return vtSecXmlAclDom.ACL_MSK_ALL
    def _acl_read(self):
        return vtSecXmlAclDom.ACL_MSK_READ
    def _acl_write(self):
        return vtSecXmlAclDom.ACL_MSK_WRITE
    def _acl_exec(self):
        return vtSecXmlAclDom.ACL_MSK_EXEC
    def _acl_build(self):
        return vtSecXmlAclDom.ACL_MSK_BUILD
    def _acl_add(self):
        return vtSecXmlAclDom.ACL_MSK_ADD
    def _acl_del(self):
        return vtSecXmlAclDom.ACL_MSK_DEL
    def _acl_move(self):
        return vtSecXmlAclDom.ACL_MSK_MOVE
    def GetPossibleAcl(self):
        return self._acl_normal()
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return None
    def GetTranslation(self,name):
        return name
    def Build(self):
        """ this method is called automatically after the XML-file is read.
        """
        pass
    def BuildLang(self):
        """ this method is called automatically when language is changed.
        """
        pass
    def GetNodeValues(self):
        l=[]
        for tup in self.NODE_ATTRS:
            l.append(tup[2])
        return l
    def GetTagNames2IncludeBase(self):
        "return a list of tagnames to be included in GUI-node display widgets"
        return None
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsRequired(self):
        "node has to be present"
        return False
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultipled(self):
        "serveral child instances"
        return False
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsSkip2Collect(self):
        "do not display node in tree but collect"
        return False
    def IsBlock(self):
        "do not display node in tree and skip children"
        return False
    def IsDisplay(self):
        "display node in tree"
        return True
    def IsFrozen(self):
        "information below is frozen, do not change"
        return False
    def IsNotContained(self):
        "do not add to listbook"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def IsBase(self):
        "is base node"
        return False
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return False
    def IsImportExportBase(self):
        "is base for import or export"
        return True
    def UseBaseNode(self):
        "GUI object use base node"
        return False
    def HasGraphics(self):
        "images are defined"
        return True
    def GetMapVCX(self):
        iMap=0
        for i,s in [
                    (0x40000000, 'Is2Create'),
                    (0x20000000, 'Is2Add'),
                    (0x10000000, 'IsRequired'),
                    (0x08000000, 'IsMultiple'),
                    (0x04000000, 'IsMultipled'),
                    (0x00800000, 'IsSkip'),
                    (0x00400000, 'IsSkip2Collect'),
                    (0x00200000, 'IsBlock'),
                    (0x00100000, 'IsDisplay'),
                    (0x00080000, 'IsFrozen'),
                    (0x00040000, 'IsNotContained'),
                    (0x00020000, 'IsImportExportBase'),
                    (0x00001000, 'IsId2Add'),
                    (0x00000800, 'IsBase'),
                    (0x00000400, 'IsRelBase'),
                    (0x00000080, 'UseBaseNode'),
                    (0x00000020, 'HasGraphics'),
                    ]:
            if getattr(self,s)():
                iMap|=i
        return iMap
    def SetInfos2Get(self):
        #self.doc.SetInfos2Get(['info'],foreign='foreign name',name='result name',cltData=None,bSingleLang=True)
        pass
    def GetValidChild(self):
        return self.lValid
    def IsValidChild(self,sTagName):
        if self.lValid is None:
            return False
        if sTagName in self.lValid:
            return True
        return False
    def IsInvalidChild(self,sTagName):
        if self.lInvalid is None:
            return False
        if sTagName in self.lInvalid:
            return True
        return False
    def IsNotContainedChild(self,sTagName):
        if self.lNotCont is None:
            return False
        if sTagName in self.lNotCont:
            return True
        return False
    def IsAttrHidden(self,sTag):
        if sTag in self.ATTR_HIDDEN:
            return True
        return False
    def IsAclOk(self,iAcl):
        if (self.GetPossibleAcl()&iAcl)!=iAcl:      # 100213:wro check ACL capability first
            return False
        if self.doc is None:
            return False
        return self.doc.IsAclOk(self.GetTagName(),iAcl)
    def SetDoc(self,doc):
        if VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurCls(vtLog.DEBUG,'tag:%s;doc is None=%d'%(
                        self.GetTagName(),doc is None),self)
        self.doc=doc
        self.__SetOrigin__()
    def __create__(self,node,*args,**kwargs):
        for sFuncPart,sAttrName,sTag,attrName in self.NODE_ATTRS:
            try:
                f=getattr(self,'Set%s'%sFuncPart)
                f(node,'')
            except:
                vtLog.vtLngTB(self.__class__.__name__)
    def __createPost__(self,node,*args,**kwargs):
        pass
    def Create(self,nodePar=None,func=None,*args,**kwargs):
        if self.doc is None:
            return None
        #if self.doc.
        #if self.IsAclOk(self.doc.ACL_MSK_ADD)==False:      # 070820:wro check acl
        #    return None
        #print 'create'
        #self.doc.acquire('dom')
        try:
        #if 1:
            c=self.doc.createNode(self.GetTagName())
            if nodePar is not None:
                self.doc.appendChild(nodePar,c)
                self.__create__(c,*args,**kwargs)
                if func is not None:
                    func(self,c,*args,**kwargs)
                #if self.IsId2Add():
                #    self.doc.setAttribute(c,self.doc.attr,'')
                #self.__createPost__(c,*args,**kwargs)   # 081222 wro 
                if self.IsId2Add():             # only add id if needed
                    #print c
                    self.doc.setAttribute(c,self.doc.attr,'')
                    self.doc.addNode(nodePar,c)
                    #print c
                self.__createPost__(c,*args,**kwargs)
            else:
                self.__create__(c)
                if func is not None:
                    func(self,c,*args,**kwargs)
                # 070309:wro should n't that be added?
                #if self.IsId2Add():             # only add id if needed
                #    self.doc.setAttribute(c,self.doc.attr,'')
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        #self.doc.release('dom')
        return c
    def CreateSafe(self,nodePar=None,func=None,*args,**kwargs):
        if self.doc is None:
            return None
        #if self.doc.
        #if self.IsAclOk(self.doc.ACL_MSK_ADD)==False:      # 070820:wro check acl
        #    return None
        #print 'create'
        self.doc.acquire('dom')
        try:
        #if 1:
            c=self.doc.createNode(self.GetTagName())
            if nodePar is not None:
                self.doc.appendChild(nodePar,c)
                self.__create__(c,*args,**kwargs)
                if func is not None:
                    func(c,*args,**kwargs)
                #if self.IsId2Add():
                #    self.doc.setAttribute(c,self.doc.attr,'')
                if self.IsId2Add():             # only add id if needed
                    #print c
                    self.doc.setAttribute(c,self.doc.attr,'')
                    self.doc.addNode(nodePar,c)
                    #print c
                self.__createPost__(c,*args,**kwargs)
            else:
                self.__create__(c)
                if func is not None:
                    func(c,*args,**kwargs)
                # 070309:wro should n't that be added?
                #if self.IsId2Add():             # only add id if needed
                #    self.doc.setAttribute(c,self.doc.attr,'')
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        self.doc.release('dom')
        return c
    def CloneSafe(self,nodeSrc,nodePar=None,func=None,*args,**kwargs):
        if self.doc is None:
            return None
        #if self.doc.
        #if self.IsAclOk(self.doc.ACL_MSK_ADD)==False:      # 070820:wro check acl
        #    return None
        #print 'create'
        self.doc.acquire('dom')
        try:
        #if 1:
            if nodeSrc is None:
                return None
            c=self.doc.cloneNode(nodeSrc,True)
            #c=self.doc.createNode(self.GetTagName())
            if nodePar is not None:
                self.doc.appendChild(nodePar,c)
                #self.__create__(c,*args,**kwargs)
                if func is not None:
                    func(c,*args,**kwargs)
                #if self.IsId2Add():
                #    self.doc.setAttribute(c,self.doc.attr,'')
                if self.IsId2Add():             # only add id if needed
                    #print c
                    self.doc.setAttribute(c,self.doc.attr,'')
                    self.doc.addNode(nodePar,c)
                    #print c
                self.__createPost__(c,*args,**kwargs)
            else:
                #self.__create__(c)
                if func is not None:
                    func(c,*args,**kwargs)
                # 070309:wro should n't that be added?
                #if self.IsId2Add():             # only add id if needed
                #    self.doc.setAttribute(c,self.doc.attr,'')
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        self.doc.release('dom')
        return c
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xebIDAT8\x8d\xa5\x93MO\x13Q\x14\x86\x9f{g:\xdcRZ\xc7\xd2\xea\x184H\
\xda\xa8\x11\x8c\tKc\xd0\x7f\xe0\xda\x9f\xe6\x9e?\xc0\x86\x05!1\xf1+\xba@BH\
\x10\xd0\x1a\x04\xb4\x14\x8aL\x0b\xd6qz?\\hJ\r\xa0&\xbc\xab{r\xee\xfb\xe4\
\xe4\xbd\xf7\x08!=\xce#y.\xf7\xbf\x00B\x18\xe7y\xce\xfd\xed\x8e\xdf_x\xc2\
\xb8\x9c\xe73r9O\xa9\x1009Z&T\x1e/\xd7w\xdd\xd3w\r\x8c\x13\xe2L\xc0\xed\xa8\
\xe0\x1e\xdd\xab0>z\x95N\xb7\xc3\xdcB\x8d\xb1\xe8\x12-\x93\x12\x954\x0f\xab\
\x92\xc4\xa4\xee\xf5\xa7}\x8c9\x06\xf5\x00\x17\n!{q\xc2\xe0\x84%nK\xa2r\x91\
\xe5\x9d\xaf\xac\xac5x\xfc\xe0&\x83\xe3\xa3\xb4\xe2&\xf7+%f\xd6v\xdc\xeaf[\
\xfc\x010ZcM\x86\xe9\xb95\xc20\xcb\xd4\xe4u\xa2(Gm\xe2\x1a\xf3/V\x99\xbaS\
\xa1\xd9JY\xdel\x93l\xb8^z=@\xea\xa5(O\x13\xe5\x15\xe5\xa2b\xacz\x05\xa5|n\
\x18E|\xeb\x80\xe9\x99W\xd4\x0f5?\x8c$\xdf\x17]\xef\xa4\x9c\x04O\xa3\x83\x00\
\x95\x914\xb6v\x99}\xb3\xce\xd2\xfa\x17\\j\xc8\x0c\x05\x04\x89\xe4\xbb\xb4\
\x90\x1c\x87\xd8{\xc6nVa\x85\x84D"=I\x1e\xc9\xb3\xb7\xef9H\r)\x1ae\x05\xbe\
\xaf\xb1.\xe508:\t\xd8\xaa\xc7\x80$6\x1d|@\x0b\x8b\xfc\xdd\xd5X\xb4\xf1AX\
\xa4\x07:s\xca\x04\xbb\xcd\xb6\x98]\xfcH\xab\x99\xa0\xb1``X)\xf2\xd2G#\x7f\
\x99\xb3\x96\xe1\x01\xc8)\xff$\x00`s/\x11\xcf?|\xc6j\t>\x14\xc2a\x8a\xa1\xcf\
\x88\xbaH0 A\x06l\xb7R\x1a\xad\xf4t\x00@\'q\xe2\xc9\xfc\x12\xb6+\x19\xcaYF29\
\x12\xabY\xdc\xd8\xa7\xb6}D\xd2\x15\xc2\xd8\xe3\x8f$\xce\xda\xc6\xbb\xd5\x92\
+\x86\x01\x0b+u\xbe%\x96~S\xbf\xce\x04\xfc\xaf~\x02\x18D\xc6\xd6\'Q/%\x00\
\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetImage(self):
        stream = cStringIO.StringIO(self.getImageData())
        return wxImageFromStream(stream)
    def GetSelImage(self):
        stream = cStringIO.StringIO(self.getSelImageData())
        return wxImageFromStream(stream)
    def __convImg__(self,imgInst,f):
        ar=array.array('B',imgInst.GetData())
        iLen=ar.buffer_info()[1]
        for i in xrange(0, iLen, 3):
            if ar[i]!=1:
                if ar[i+1]!=0:
                    if ar[i+2]!=0:
                        def chRgb(a):
                            a=int(a*f)
                            if a>254:
                                a=254
                            return a
                        ar[i]=chRgb(ar[i])
                        ar[i+1]=chRgb(ar[i+1]) 
                        ar[i+2]=chRgb(ar[i+2])
        imgInst.SetData(ar.tostring())
        return imgInst#.ConvertToBitmap()
    def __convImgInvalid__(self,imgInst,f):
        ar=array.array('B',imgInst.GetData())
        iLen=ar.buffer_info()[1]
        for i in xrange(0, iLen, 3):
            if ar[i]!=1:
                if ar[i+1]!=0:
                    if ar[i+2]!=0:
                        def chRgb(a):
                            a=int(a*f)
                            if a>254:
                                a=254
                            return a
                        ar[i]=chRgb(ar[i])
                        ar[i+1]=chRgb(ar[i+1]) 
                        ar[i+2]=chRgb(ar[i+2])
        imgInst.SetData(ar.tostring())
        xCenter=imgInst.GetWidth()/2
        yCenter=imgInst.GetHeight()/2
        if xCenter<yCenter:
            dW=xCenter
        else:
            dW=xCenter
        def set(x,y):
            try:
                imgInst.SetRGB(x,y,180,0,0)
            except:
                pass
        for i in range(dW-1):
            set(xCenter-i,yCenter-i)
            set(xCenter-i-1,yCenter-i)
            #set(xCenter-i+1,yCenter-i)
            
            set(xCenter+i,yCenter-i)
            set(xCenter+i-1,yCenter-i)
            #set(xCenter+i+1,yCenter-i)
            
            set(xCenter-i,yCenter+i)
            set(xCenter-i-1,yCenter+i)
            #set(xCenter-i+1,yCenter+i)
            
            set(xCenter+i,yCenter+i)
            set(xCenter+i-1,yCenter+i)
            #set(xCenter+i+1,yCenter+i)
        j=imgInst.GetHeight()-1
        for i in range(0,imgInst.GetWidth()):
            set(i,0)
            #set(i,1)
            set(i,j)
            #set(i,j-1)
        i=imgInst.GetWidth()-1
        for j in range(0,imgInst.GetHeight()):
            set(0,j)
            #set(1,j)
            set(i,j)
            #set(i-1,j)
        return imgInst
    def GetInstImage(self):
        return self.__convImg__(self.GetImage(),1.5)
    def GetInstSelImage(self):
        return self.__convImg__(self.GetSelImage(),1.5)
    def GetInvalidImage(self):
        return self.__convImgInvalid__(self.GetImage(),2.0)
    def GetInvalidSelImage(self):
        return self.__convImgInvalid__(self.GetSelImage(),2.0)
    def GetBitmap(self):
        if GUI:
            return wxBitmapFromImage(self.GetImage())
        else:
            return None
    def GetSelBitmap(self):
        if GUI:
            return wxBitmapFromImage(self.GetSelImage())
        else:
            return None
    def GetInstBitmap(self):
        if GUI:
            return wxBitmapFromImage(self.GetInstImage())
        else:
            return None
    def GetInstSelBitmap(self):
        if GUI:
            return wxBitmapFromImage(self.GetInstSelImage())
        else:
            return None
    def GetInvalidBitmap(self):
        if GUI:
            return wxBitmapFromImage(self.GetInvalidImage())
        else:
            return None
    def GetInvalidSelBitmap(self):
        if GUI:
            return wxBitmapFromImage(self.GetInvalidSelImage())
        else:
            return None
    def GetEditDialog(self,parent,sTitle):
        t=self.GetEditDialogClass()
        if t is None:
            return None
        tt=type(t)
        if tt==types.DictType:
            cls=vtXmlNodeBase.GetEditDialogClass(self)
            if cls is None:
                return None
            if 'pnCls' not in t:
                t['pnCls']=self.GetPanelClass()
            dlg=cls(parent,sTitle,**t)
        elif tt==types.TupleType:
            dlg=t[0](parent,self.GetPanelClass(),**t[1])
        else:
            dlg=t(parent)
            dlg.SetTitle(sTitle)
        return dlg
    def GetEditDialogClass(self):
        if GUI:
            return vtXmlNodeBaseEditDialog
        else:
            return None
    def GetAddDialog(self,parent,sTitle):
        t=self.GetAddDialogClass()
        if t is None:
            return None
        tt=type(t)
        if tt==types.DictType:
            cls=vtXmlNodeBase.GetAddDialogClass(self)
            if cls is None:
                return None
            if 'pnCls' not in t:
                t['pnCls']=self.GetPanelClass()
            dlg=cls(parent,sTitle,**t)
        elif tt==types.TupleType:
            dlg=t[0](parent,self.GetPanelClass(),**t[1])
        else:
            dlg=t(parent)
            dlg.SetTitle(sTitle)
        return dlg
    def GetAddDialogClass(self):
        if GUI:
            return vtXmlNodeBaseAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vtXmlNodeBasePanel
        else:
            return None
    def DoEdit(self,parent,doc,node,bModal=True):
        if GUI==0:
            return -2
        if self.doc is None:
            return -3
        if parent in self.dDlgEdit:
            dlgEdit=self.dDlgEdit[parent]
            if dlgEdit is None:
                return 0
        else:
            sTitle=u''.join([_(u'Edit Node Dialog'),u':',
                    self.GetDescription(),u' ',
                    u'(',self.GetTagName(),u'/',self.GetClsName(),u')'])
            dlgEdit=self.GetEditDialog(parent,sTitle)
            self.dDlgEdit[parent]=dlgEdit
            if dlgEdit is None:
                return 0
            #if self.GetEditDialogClass() is None:
            #    return 0
            #dlgEdit=self.GetEditDialogClass()(parent)
            #dlgEdit.SetTitle(u''.join([_(u'Edit Node Dialog'),u':',
            #        self.GetDescription(),u' ',
            #        u'(',self.GetTagName(),u'/',self.GetClsName(),u')']))
            dlgEdit.Centre()
            dlgEdit.SetRegNode(self)
            dlgEdit.SetDoc(doc,True)
        dlgEdit.SetNode(node)
        if bModal:
            iRet=dlgEdit.ShowModal()
            if self.lValid is not None:
                for sTag in self.lValid:
                    o=self.doc.GetRegisteredNode(sTag)
                    if o is not None:
                        if o.IsSkip():
                            lR=[]
                            childs=self.doc.getChilds(node,sTag)
                            for c in childs:
                                iR=o.DoEdit(parent,doc,c,bModal)
                                lR.append((iR,doc.getTagName(c)))
            return iRet
        else:
            if dlgEdit.IsShown():
                return -1
            dlgEdit.Show()
        return 0
    def DoAdd(self,parent,nodePar,bModal=True,func=None,*args,**kwargs):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCurCls(vtLog.INFO,'tag:%s'%(self.GetTagName()),self)
        if GUI==0:
            vtLog.vtLngCurCls(vtLog.WARN,'no gui'%(),self)
            return -1,None
        if self.doc is None:
            vtLog.vtLngCurCls(vtLog.CRITICAL,'doc is None'%(),self)
            return -1,None
        if parent in self.dDlgAdd:
            dlgAdd=self.dDlgAdd[parent]
            if dlgAdd is None:
                c=self.Create(nodePar)
                self.doc.AlignNode(nodePar,iRec=2)
                return 1,c
        else:
            try:
                sTitle=u''.join([_(u'Add Node Dialog'),u':',
                            self.GetDescription(),u' ',
                            u'(',self.GetTagName(),u'/',self.GetClsName(),u')'])
                dlgAdd=self.GetAddDialog(parent,sTitle)
                self.dDlgAdd[parent]=dlgAdd
                if dlgAdd is None:
                    c=self.Create(nodePar)
                    self.doc.AlignNode(nodePar,iRec=2)
                    return 1,c
                #cls=self.GetAddDialogClass()
                
                #dlgAdd=cls(parent)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,''%(),dlgAdd)
                try:
                    #dlgAdd.SetTitle(u''.join([_(u'Add Node Dialog'),u':',
                    #        self.GetDescription(),u' ',
                    #        u'(',self.GetTagName(),u'/',self.GetClsName(),u')']))
                    #self.dDlgAdd[parent]=dlgAdd
                    dlgAdd.Centre()
                    dlgAdd.SetRegNode(self)
                    dlgAdd.SetDoc(self.doc)
                except:
                    try:
                        vtLog.vtLngTB(self.GetTagName())
                    except:
                        vtLog.vtLngTB(self.__class__.__name__)
            except:
                vtLog.vtLngTB(self.__class__.__name__)
                c=self.Create(nodePar)
                self.doc.AlignNode(nodePar,iRec=2)
                return 1,c
        dlgAdd.SetNode(self,nodePar,None)
        if bModal:
            if dlgAdd.ShowModal()>0:
                c=self.Create(None,func,*args,**kwargs)
                if c is not None:
                    if nodePar is not None:
                        self.doc.appendChild(nodePar,c)
                        self.doc.checkId(c)                 # 070325 wro ensure id to avoid problems during createPost
                        #self.doc.addNode(nodePar,c)        # 061003 wro
                    dlgAdd.GetNode(c)
                    self.__createPost__(c,*args,**kwargs)
                    if nodePar is not None:               # 061003 wro
                        self.doc.addNode(nodePar,c)        # 061003 wro
                    self.doc.AlignNode(c)
                        
                    if self.lValid is not None:
                        for sTag in self.lValid:
                            o=self.doc.GetRegisteredNode(sTag)
                            if o is not None:
                                if o.Is2Create():
                                    o.DoAdd(parent,c,bModal)
                    return 1,c
        else:
            if dlgAdd.IsShown():
                return -1,None
            dlgAdd.Show()
        return 0,None
    def doEdit(self,node):
        if self.doc is None:
            return -1
        try:
            self.doc.doEdit(node)
        except:
            pass
    def DoExec(self,parent,node,nodePar,**kwargs):
        """ this method is called when node is executed (e.g. toolbar button).
        """
        pass
    def DoBuild(self,parent,node,nodePar,**kwargs):
        """ this method is called when node is build (e.g. toolbar button).
        """
        pass
    def SetValue(self,node,**kwargs):
        if node is not None:
            for kwName,kwAttr,nodeName,attrName in self.NODE_ATTRS:
                try:
                    sName=kwargs[kwName]
                except:
                    sName=''
                    #if self.doc is not None:
                    #    self.doc.setText(node,)
                    continue
                bAddAttr=False
                try:
                    if kwAttr is not None:
                        sId=kwargs[kwAttr]
                        if sId is not None:
                            self.doc.setNodeTextAttr(node,nodeName,sName,attrName,sId)
                            bAddAttr=True
                except:
                    pass
                if bAddAttr==False:
                    self.doc.setNodeText(node,nodeName,sName)
    def SetValueUniqueAttr(self,node,**kwargs):
        if node is not None:
            for kwName,kwAttr,nodeName,attrName in self.NODE_ATTRS:
                try:
                    sName=kwargs[kwName]
                except:
                    sName=''
                    #if self.doc is not None:
                    #    self.doc.setText(node,)
                    continue
                bAddAttr=False
                try:
                    if kwAttr is not None:
                        sId=kwargs[kwAttr]
                        if sId is not None:
                            tmp=self.doc.getChild(node,nodeName)
                            if tmp is None:
                                self.doc.setNodeTextAttr(node,nodeName,sName,attrName,sId)
                            else:
                                self.doc.setText(tmp,sName)
                                self.doc.setAttribute(tmp,attrName,sId)
                            bAddAttr=True
                except:
                    pass
                if bAddAttr==False:
                    self.doc.setNodeText(node,nodeName,sName)
    def DeleteChilds(self,node,tag):
        for c in self.doc.getChilds(node,tag):
            self.doc.deleteNode(c,node)
    def GetValue(self,node,**kwargs):
        lst=[]
        if node is not None:
            for kwName,kwAttr,nodeName,attrName in self.NODE_ATTRS:
                try:
                    sName=kwargs[kwName]
                except:
                    sName=''
                    continue
                    if self.doc is not None:
                        return self.doc.getText(node)
                    return ''
                bAddAttr=False
                try:
                    if kwAttr is not None:
                        sId=kwargs[kwAttr]
                        if sId is not None:
                            return self.doc.getNodeTextAttr(node,nodeName,attrName,sId)
                            bAddAttr=True
                except:
                    pass
                if bAddAttr==False:
                    return self.doc.getNodeText(node,nodeName)
                    kwargs[kwName]=s
    def GetClsName(self):
        return self.__class__.__name__
    
    def Get(self,node,tagName):
        if self.doc is None:
            return u''
        return self.doc.getNodeText(node,tagName)
    def GetML(self,node,tagName,lang=None):
        if self.doc is None:
            return u''
        return self.doc.getNodeTextLang(node,tagName,lang)
    def GetVal(self,node,tagName,type,fallback=None):
        try:
            return type(self.doc.getNodeText(node,tagName))
        except:
            return fallback
    def GetAttrVal(self,node,tagName,attr,attrVal):
        return self.doc.getNodeTextAttr(node,tagName,attr,attrVal)
    def GetKey(self,node):
        try:
            return long(self.doc.getKey(node))
        except:
            return -2
    def GetKeyStr(self,node):
        return self.doc.ConvertIdNum2Str(node)
    def GetForeignKey(self,node,attr,appl):
        try:
            return long(self.doc.getForeignKey(node,attr,appl))
        except:
            return -2
    def GetForeignKeyStr(self,node,attr,appl):
        return self.doc.getForeignKey(node,attr,appl)
    def GetAttr(self,node,attr):
        try:
            return long(self.doc.getAttribute(node,attr))
        except:
            return -2
    def GetAttrStr(self,node,attr):
        return self.doc.getAttribute(node,attr)
    def GetChild(self,node,tag):
        return self.doc.getChild(node,tag)
    def GetChildForced(self,node,tag):
        return self.doc.getChildForced(node,tag)
    def GetChildAttr(self,node,tag,attr):
        tmp=self.doc.getChild(node,tag)
        return self.GetAttr(tmp,attr)
    def GetChildAttrStr(self,node,tag,attr):
        tmp=self.doc.getChild(node,tag)
        return self.GetAttrStr(tmp,attr)
    def GetChildForeignKey(self,node,tag,attr,appl):
        tmp=self.doc.getChild(node,tag)
        return self.GetForeignKey(tmp,attr,appl)
    def GetChildForeignKeyStr(self,node,tag,attr,appl):
        tmp=self.doc.getChild(node,tag)
        return self.GetForeignKeyStr(tmp,attr,appl)
    def Set(self,node,tagName,val):
        self.doc.setNodeText(node,tagName,val)
    def SetML(self,node,tagName,val,lang=None):
        self.doc.setNodeTextLang(node,tagName,val,lang)
    def SetVal(self,node,tagName,value):
        self.doc.setNodeText(node,tagName,unicode(value))
    def SetAttrVal(self,node,tagName,val,attr,attrVal):
        self.doc.setNodeTextAttr(node,tagName,val,attr,attrVal)
    def SetAttr(self,node,attr,val,bConv=True):
        if bConv:
            self.doc.setAttribute(node,attr,self.doc.ConvertIdNum2Str(val))
        else:
            self.doc.setAttribute(node,attr,str(val))
    def SetAttrStr(self,node,attr,val):
        self.doc.setAttribute(node,attr,val)
    def SetForeignKey(self,node,attr,val,appl):
        self.doc.setForeignKey(node,attr,val,appl)
    def SetForeignKeyStr(self,node,attr,val,appl):
        self.doc.setForeignKey(node,attr,val,appl)
    def SetChildAttr(self,node,tag,attr,id):
        tmp=self.doc.getChildForced(node,tag)
        return self.SetAttr(tmp,attr,id)
    def SetChildAttrStr(self,node,tag,attr,id):
        tmp=self.doc.getChildForced(node,tag)
        return self.SetAttrStr(tmp,attr,id)
    def SetChildForeignKey(self,node,tag,attr,id,appl):
        tmp=self.doc.getChildForced(node,tag)
        return self.SetForeignKey(tmp,attr,id,appl)
    def SetChildForeignKeyStr(self,node,tag,attr,id,appl):
        tmp=self.doc.getChildForced(node,tag)
        return self.SetForeignKeyStr(tmp,attr,id,appl)
    def GetValueByAttr(self,node,attr):
        try:
            f=getattr(self,'Get%s'%attr)
            return f(node)
        except:
            vtLog.vtLngCurCls(vtLog.ERROR,'attr:%s'%(attr),self)
            return ''
    def GetValueByAttrML(self,node,attr,lang=None):
        try:
            f=getattr(self,'Get%s'%attr)
            return f(node,lang)
        except:
            vtLog.vtLngCurCls(vtLog.ERROR,'attr:%s'%(attr),self)
            return ''
    def AddValuesToLst(self,node,l=None):
        if l is None:
            l=[]
        for sFuncPart in self.FUNCS_GET_SET_4_LST:
            try:
                f=getattr(self,'Get%s'%sFuncPart)
                l.append(f(node))
            except:
                l.append('')
        return l
    def SetValuesFromLst(self,node,l=None,iStart=0):
        if l is None:
            return
        for sFuncPart in self.FUNCS_GET_SET_4_LST:
            try:
                f=getattr(self,'Set%s'%sFuncPart)
                f(node,l[iStart])
            except:
                vtLog.vtLngTB(self.__class__.__name__)
            iStart+=1
    def AddValuesToLstByLst(self,node,l=None,atts=[]):
        if l is None:
            l=[]
        for attr in atts:
            try:
                f=getattr(self,'Get%s'%attr)
                l.append(f(node))
            except:
                l.append('')
        return l
    def SetValuesFromLstByLst(self,node,l=None,iStart=0,attrs=[]):
        if l is None:
            return
        for sFuncPart in attrs:
            try:
                f=getattr(self,'Set%s'%sFuncPart)
                f(node,l[iStart])
            except:
                vtLog.vtLngTB(self.__class__.__name__)
            iStart+=1
    def GetInfos4Tree(self,node):
        for v in self.dTreeInfo.values():
            v=''
        for sFuncPart in self.FUNCS_GET_4_TREE:
            try:
                f=getattr(self,'Get%s'%sFuncPart)
                self.dTreeInfo[sFuncPart]=f(node)
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        return self.dTreeInfo
    def GetTranslation4List(self):
        l=[]
        for s in self.FUNCS_GET_SET_4_LST:
            sTag=None
            for t in self.NODE_ATTRS:
                if t[0]==s:
                    sTag=t[2]
                    break
            if sTag is not None:
                l.append(self.GetTranslation(sTag))
        return l
    def GetLabel4Tree(self):
        return self.FMT_GET_4_TREE
    def GetData(self,node,dImg={},thd=None,**kwargs):
        """ lHdr = [lColDef0,lColDef1,...]
        lColDefx =[
        name,       ... column name
        align,      ... column alignment 
                                -1=left
                                -2=center
                                -3=right
                                else unchanged
        width,      ... column width
        sort,       ... column sortable 
                                0=not sortable
                                1=sortable initially ascending 
                               -1=sortable initially descending
        ]
        """
        self.__logWarn__('tagname:%s;cls:%s;overwrite me'%(self.GetTagName(),self.__class__.__name__))
        return (None,None,None)
    def __getImgFromDict__(self,dImg,sBase,sN,iIdx=0):
        try:
            if sBase in dImg:
                if sN in dImg[sBase]:
                    if iIdx<0:
                        return dImg[sBase][sN]
                    return dImg[sBase][sN][iIdx]
            return -1
        except:
            self.__logTB__()
        return -1
    def GetGrouping2Tree(self):
        return self.GRP_GET_4_TREE
    def GetCmdDict(self):
        """ return a command dictionary
                tuple:( translation,
                        local=                          0x001|
                        remote=                         0x002|
                        local and remote=               0x003|
                        build permission=               0x004|
                        add to quick menu position=     0xF00,
                        argument dictionary)
                
                argument dictionary: key:argument name, value argument tuple
                
                argument tuple:(translation,type, settings dictionary to configure widget or similar)
            {
            'Clr':(_('clear'),1,None),
            'Go':(_('go on vacation'),2,{'arg01: ('arg01 translation','arg01 type',{setting}} ),
            'NoGo':(_('do not go on vacation'),3,None),
            }
        """
        return None
    def DoCmd(self,srv,action,**kwargs):
        return 0
    ACL_MSK_ALL=vtSecXmlAclDom.ACL_MSK_ALL
    ACL_MSK_NORMAL=vtSecXmlAclDom.ACL_MSK_NORMAL
    ACL_MSK_ATTR=vtSecXmlAclDom.ACL_MSK_ATTR
    ACL_MSK_READ=vtSecXmlAclDom.ACL_MSK_READ
    ACL_MSK_WRITE=vtSecXmlAclDom.ACL_MSK_WRITE
    ACL_MSK_EXEC=vtSecXmlAclDom.ACL_MSK_EXEC
    ACL_MSK_BUILD=vtSecXmlAclDom.ACL_MSK_BUILD
    ACL_MSK_ADD=vtSecXmlAclDom.ACL_MSK_ADD
    ACL_MSK_DEL=vtSecXmlAclDom.ACL_MSK_DEL
    ACL_MSK_MOVE=vtSecXmlAclDom.ACL_MSK_MOVE
    FTY_STR=vtXmlFilterType.FILTER_TYPE_STRING
    FTY_DATE=vtXmlFilterType.FILTER_TYPE_DATE
    FTY_TIME=vtXmlFilterType.FILTER_TYPE_TIME
    FTY_DATE_TIME=vtXmlFilterType.FILTER_TYPE_DATE_TIME
    FTY_INT=vtXmlFilterType.FILTER_TYPE_INT
    FTY_LONG=vtXmlFilterType.FILTER_TYPE_LONG
    FTY_FLOAT=vtXmlFilterType.FILTER_TYPE_FLOAT
    FTY_DATE_TIME_REL=vtXmlFilterType.FILTER_TYPE_DATE_TIME_REL
