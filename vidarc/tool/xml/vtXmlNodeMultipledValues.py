#----------------------------------------------------------------------------
# Name:         vtXmlNodeMultipledValues.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060619
# CVS-ID:       $Id: vtXmlNodeMultipledValues.py,v 1.3 2010/03/03 02:16:19 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeBase import *
try:
    GUI=1
except:
    GUI=0

class vtXmlNodeMultipledValues(vtXmlNodeBase):
    NODE_ATTRS=[
            #('tag',None,'tag',None),
        ]
    FUNCS_GET_SET_4_LST=[]
    def __init__(self,tagName='multiple',tagNameMultipled='multipled'):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        vtXmlNodeBase.__init__(self,tagName)
        self.tagNameMultipled=tagNameMultipled
        self.sMultipleSel=None
    def GetDescription(self):
        return _(u'multiple values')
    # ---------------------------------------------------------
    # specific
    def GetTagNameMultiled(self):
        return self.tagNameMultipled
    def __addMultipledLst__(self,node,*args):
        l=args[0][0]
        sTag=self.doc.getTagName(node)
        if sTag==self.tagNameMultipled:
            sVal=self.doc.getAttribute(node,'multiple')
            try:
                self.dt.SetDateStr(sVal)
                sVal=self.dt.GetDateStr()
                l.append([sVal,node])
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        return 0
    def __addMultipledDict__(self,node,*args):
        d=args[0][0]
        sTag=self.doc.getTagName(node)
        if sTag==self.tagNameMultipled:
            sVal=self.doc.getAttribute(node,'multiple')
            try:
                d[sVal]=node
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        return 0
    def GetMultipledLst(self,node):
        l=[]
        self.doc.procChilds(node,self.__addMultipledLst__,l)
        return l
    def GetMultipledDict(self,node):
        d={}
        self.doc.procChilds(node,self.__addMultipledDict__,d)
        return d
    def SelectMultipled(self,sMultiple=None):
        self.sMultipleSelected=sDt
    def GetMultipledNode(self,node,sMultiple=None,bForced=True):
        if node is None:
            return None
        d=self.GetMultipledDict(node)
        if sMultiple is None:
            keys=d.keys()
            if len(keys)>0:
                keys.sort()
                sMultiple=keys[0]
        if sMultiple in d:
            return d[sMultiple]
        else:
            if bForced:
                c=self.doc.createSubNode(node,self.tagNameMultipled)
                self.doc.setAttribute(c,'multiple',sMultiple)
                return c
        return None
    def IsMultipledNode(self,node):
        if node is None:
            return False
        sTag=self.doc.getTagName(node)
        if sTag==self.tagNameMultipled:
            return True
        else:
            return False
    def GetValuesFromParent(self,node,sMultiple,lst=None,lang=None):
        if node is None:
            return None
        if self.doc is None:
            return None
        if lst is None:
            lst=self.NODE_ATTRS_REQ
        tmp=self.GetMultipledNode(self.doc.getChild(node,self.tagName),
                    sMultiple=sMultiple,bForced=False)
        if tmp is None:
            return None
        return [self.GetML(tmp,attr,lang) for attr in lst]
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        return True
    def Is2Add(self):
        return True
    def IsMultiple(self):
        return True
    def IsMultipled(self):
        "serveral child instances"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return False
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x1cIDAT8\x8dcddbf\xa0\x040Q\xa4{\xd4\x80Q\x03F\r\x18D\x06\x00\x00]b\
\x00&\x87\xd5\x92\xeb\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        return None
    def GetAddDialogClass(self):
        return None
    def GetPanelClass(self):
        return None
