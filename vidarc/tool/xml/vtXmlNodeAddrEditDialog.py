#Boa:Dialog:vtXmlNodeAddrEditDialog
#----------------------------------------------------------------------------
# Name:         vtXmlNodeAddrEditDialog.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060207
# CVS-ID:       $Id: vtXmlNodeAddrEditDialog.py,v 1.7 2007/08/20 14:21:54 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

from vtXmlNodeAddrPanel import *

import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.xml.vtXmlNodeDialog import vtXmlNodeDialog

def create(parent):
    return vtXmlNodeAddrEditDialog(parent)

[wxID_VTXMLNODEADDREDITDIALOG, wxID_VTXMLNODEADDREDITDIALOGCBAPPLY, 
 wxID_VTXMLNODEADDREDITDIALOGCBCANCEL, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vtXmlNodeAddrEditDialog(wx.Dialog,vtXmlNodeDialog):
    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(16, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsBt, (1, 0), border=4,
              flag=wx.BOTTOM | wx.TOP | wx.RIGHT | wx.LEFT | wx.ALIGN_CENTER,
              span=(1, 1))

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=0, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_gbsData_Items(self.gbsData)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTXMLNODEADDREDITDIALOG,
              name=u'vtXmlNodeAddrEditDialog', parent=prnt, pos=wx.Point(348,
              174), size=wx.Size(466, 356), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vLoc Customer Edit Dialog')
        self.SetClientSize(wx.Size(458, 329))

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLNODEADDREDITDIALOGCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'),
              name=u'cbApply', parent=self, pos=wx.Point(4, 24),
              size=wx.Size(76, 30), style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VTXMLNODEADDREDITDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLNODEADDREDITDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(96, 24),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VTXMLNODEADDREDITDIALOGCBCANCEL)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self._init_ctrls(parent)
        vtXmlNodeDialog.__init__(self)
        self.doc=None
        self.node=None
        try:
            self.gbsData.AddGrowableCol(0)
            self.gbsData.AddGrowableRow(0)
        
            self.pn=vtXmlNodeAddrPanel(self,id=wx.NewId(),pos=(0,0),size=(440,285),
                        style=0,name=u'pnLoc')
            self.gbsData.AddWindow(self.pn, (0, 0), border=4,
                  flag=wx.BOTTOM | wx.TOP | wx.LEFT | wx.RIGHT | wx.EXPAND, span=(1, 1))
            self.gbsData.Layout()
            self.gbsData.Fit(self)
        except:
            vtLog.vtLngTB('')

    #def SetRegNode(self,obj):
    #    self.pnLoc.SetRegNode(obj)
    #def SetDoc(self,doc,bNet=False):
    #    self.pnLoc.SetDoc(doc,bNet)
    #def SetNode(self,node):
    #    self.pnLoc.SetNode(node)
    #def GetNode(self):
    #    self.pnLoc.GetNode()
    #def OnCbApplyButton(self, event):
    #    self.GetNode()
    #    self.EndModal(1)
    #    event.Skip()
    #def OnCbCancelButton(self, event):
    #    self.pnLoc.Cancel()
    #    self.EndModal(0)
    #    event.Skip()
    def OnCbApplyButton(self,evt):
        evt.Skip()
        try:
            self.pn.Close()
            self.pn.GetNode(None)
            if self.IsModal():
                self.EndModal(1)
            else:
                self.Show(False)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbCancelButton(self,evt):
        evt.Skip()
        try:
            self.pn.Close()
            self.pn.Cancel()
            if self.IsModal():
                self.EndModal(0)
            else:
                self.Show(False)
        except:
            vtLog.vtLngTB(self.GetName())

