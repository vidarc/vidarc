#----------------------------------------------------------------------------
# Name:         vXmlNodeAddrSingle.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060503
# CVS-ID:       $Id: vtXmlNodeAddrSingle.py,v 1.2 2006/05/16 11:08:53 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlNodeAddr import *

class vtXmlNodeAddrSingle(vtXmlNodeAddr):
    def Is2Create(self):
        return False
    def Is2Add(self):
        return False
    def IsMultiple(self):
        return False
    def IsImportExportBase(self):
        return False
    def IsSkip(self):
        return True

