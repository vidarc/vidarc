#----------------------------------------------------------------------------
# Name:         vtXmlFindThread.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060223
# CVS-ID:       $Id: vtXmlFindThread.py,v 1.7 2007/07/29 09:56:50 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import threading,thread

import vidarc.tool.log.vtLog as vtLog

class vtXmlFindThread:
    def __init__(self,name='tmpFlt',par=None,doc=None,bFindAll=False,verbose=0):
        self.name=name
        self.par=par
        self.doc=doc
        self.running=False
        self.keepGoing = False
        self.hier=None
        self.func=None
        self.bFindAll=bFindAll
        self.verbose=verbose
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        try:
            if self.doc is not None:
                for nodeName in self.filters.keys():
                    self.doc.delFilters(self.name+'_'+nodeName)
            del self.hier
            
        except:
            pass
    def Find(self,filters,node,hier=[],func=None,*args,**kwargs):
        if self.verbose:
            vtLog.CallStack('')
            print filters
        vtLog.vtLngCur(vtLog.DEBUG,'','thdFind')
        self.keepGoing = self.running=True
        self.filters=filters
        self.hier=hier
        self.bSkip=len(self.hier)!=0
        self.func=func
        self.args=args
        self.kwargs=kwargs
        self.node=node
        if self.bFindAll:
            self.nodeFound=[]
        else:
            self.nodeFound=None
        if self.doc is None:
            return
        for nodeName in self.filters.keys():
            self.doc.setFilters(self.filters[nodeName],self.name+'_'+nodeName)
        thread.start_new_thread(self.Run, ())
    def FindNext(self):
        self.bSkip=len(self.hier)!=0
        self.nodeFound=None
        self.keepGoing = self.running=True
        thread.start_new_thread(self.Run, ())
    def Stop(self):
        self.keepGoing=False
    def IsRunning(self):
        return self.running
    
    def __checkNode__(self,c,node,level):
        if not self.keepGoing and self.running:
            return -1
        try:
            if self.bSkip:
                if self.doc.isSameKey(c,self.hier[level]):
                    if len(self.hier)==level+1:
                        self.bSkip=False
                        self.hier.pop()
                    else:
                        self.doc.procChildsKeys(c,self.__checkNode__,c,level+1)
                        if not self.bFindAll:
                            if self.nodeFound is not None:
                                return -2
                            else:
                                self.hier.pop()
                        else:
                            self.hier.pop()
                return 1
            try:
                sTagName=self.doc.getTagName(c)
                if not self.filters.has_key(sTagName):
                    if self.filters.has_key('*'):
                        sTagName='*'
                filters=self.filters[sTagName]
            except:
                self.hier.append(self.doc.getKey(c))
                self.doc.procChildsKeys(c,self.__checkNode__,c,level+1)
                if not self.bFindAll:
                    if self.nodeFound is None:
                        self.hier.pop()
                    else:
                        return -2
                else:
                    self.hier.pop()
                return 0
            
            bFound=self.doc.matchFilters(c,None,self.name+'_'+sTagName)
            if 1==0:
                bFound=True
                for tagname,filter in filters:
                    bFondAttr=self.doc.match(c,tagname,filter)
                    #bFoundAttr=False
                    #for cAttr in self.doc.getChilds(c,tagname):
                    #    if filter.match(self.doc.getText(cAttr))!=0:
                    #        bFoundAttr=True
                    #        break
                    if not bFoundAttr:
                        bFound=False
                        break
                    
                    #if filter.match(self.doc.getNodeText(c,tagname))==0:
                    #    bFound=False
                    #    break
            if bFound:
                if self.bFindAll:
                    self.nodeFound.append(c)
                    #self.hier.append(self.doc.getKey(c))
                    #return 1
                else:
                    self.nodeFound=c
                    self.hier.append(self.doc.getKey(c))
                    return -2
            else:
                self.hier.append(self.doc.getKey(c))
                self.doc.procChildsKeys(c,self.__checkNode__,c,level+1)
                if not self.bFindAll:
                    if self.nodeFound is None:
                        self.hier.pop()
                    else:
                        return -2
                else:
                    self.hier.pop()
        except:
            vtLog.vtLngTB('thdFind')
        return 1
    def checkNode(self,node,level):
        self.doc.procChildsKeys(node,self.__checkNode__,node,level)
    def Run(self):
        try:
            self.checkNode(self.node,0)
            if self.func is not None:
                self.func(self.nodeFound,*self.args,**self.kwargs)
        except:
            vtLog.vtLngTB('thdFind')
        self.keepGoing = self.running = False
