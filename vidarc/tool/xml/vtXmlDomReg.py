#----------------------------------------------------------------------------
# Name:         vtXmlDomReg.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051228
# CVS-ID:       $Id: vtXmlDomReg.py,v 1.59 2015/10/20 09:14:50 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlDom import vtXmlDom
from vidarc.tool.xml.vtXmlDomRegReferences import vtXmlDomRegReferences
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.time.vtTimeLoadBound as vtLdBd
from vidarc.tool.xml.vtXmlNodeCalc import vtXmlNodeCalc
import time


class vtXmlDomReg(vtXmlDom):
    def __init__(self,*kw,**kwargs):
        vtXmlDom.__init__(self,*kw,**kwargs)
        self.regNodes={}
        self.lstRegNodes=[]
        self.lstRegNodesRoot=[]
        self.dAcl=None          # cache
        self.lAcl=None          # cache
        self.dFilter=None       # cache
        #self.netMaster=None
        #self.dNetDoc={}
        self.refs=vtXmlDomRegReferences(self)
        
        oCalc=vtXmlNodeCalc()
        self.RegisterNode(oCalc,False)
        
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        try:
            del self.refs
            del self.regNodes
            del self.lstRegNodes
            del self.lstRegNodesRoot
            del self.dAcl
            del self.lAcl
            del self.dFilter
        except:
            pass
        vtXmlDom.__del__(self)
    def SetFN(self,fn):
        vtXmlDom.SetFN(self,fn)
        self.refs.SetFN(fn)
    def __Close__(self):
        self.refs.close()
    def __SavePersistant__(self):
        sFP=self.getIds().GetGlobalFingerPrint()
        vtLog.vtLngCur(vtLog.INFO,'global FP:%s'%(sFP),self.GetOrigin())
        self.refs.SetFP(sFP)
        self.refs.save()
    def genIds(self,node=None,bAcquire=True):
        if bAcquire:
            self.acquire('dom')
        try:
            zClk=time.clock()
            if vtLog.vtLngIsLogged(vtLog.INFO):
                    vtLog.vtLngCur(vtLog.INFO,'started',origin=self.GetOrigin())
            self.CreateIds(self.attr)
            if node is None:
                node=self.getBaseNode()
            #if self.IsId2Add(node)==True:  # 100613:wro stupid
            #    if self.IsKeyValid(id):
            #        ids.CheckId(c,id)
            #    else:
            #        ids.CheckId(c,None)
            if self.hasKey(node)>0:
                self.__genIds__(node,None,self.ids)
            else:
                self.procChildsKeys(node,self.__genIds__,node,self.ids)
            if self.ids.ProcessMissing(self):
                self.__setModified__()
            self.ids.ClearMissing()
            if self.verbose:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(str(self.ids.GetIDs())),origin=self.GetOrigin())
            if vtLog.vtLngIsLogged(vtLog.INFO):
                    vtLog.vtLngCur(vtLog.INFO,'finished;%s'%(time.clock()-zClk),origin=self.GetOrigin())
            self.CreateAcl()
            #self.Build()
            #self.refs.Build()
            if bAcquire==False:
                self.refs.SetFN(self.GetFN())
                sGlbFP=self.getIds().GetGlobalFingerPrint()
                self.refs.SetFP(sGlbFP)
                sSavFP=self.refs.open()
                if vtLog.vtLngIsLogged(vtLog.DEBUG):     # 081207:wro logging
                    vtLog.vtLngCur(vtLog.DEBUG,'actFP:%s;savFP:%s'%(repr(sGlbFP),repr(sSavFP)),
                            self.GetOrigin())
                #self.refs.BuildNoThread()
        except:
            vtLog.vtLngTB(self.GetOrigin())
        if bAcquire:
            self.release('dom')
    def DelConsumer(self,consumer=None):
        if consumer is None:
            for o in self.lstRegNodes:
                #o.ClearDoc()
                o.SetDoc(None)
        vtXmlDom.DelConsumer(self,consumer=consumer)
    def genIdsRef(self,bAcquire=True,bThreaded=True):
        if bAcquire:
            self.acquire('dom')
        try:
            if bThreaded:
                self.refs.Build()
            else:
                self.refs.BuildNoThread()
        except:
            vtLog.vtLngTB(self.GetOrigin())
        if bAcquire:
            self.release('dom')
    def __isRunning__(self):
        if self.refs.IsBusy():
            return True
        return vtXmlDom.__isRunning__(self)
    def __stop__(self):
        self.refs.Stop()
        vtXmlDom.__stop__(self)
    def __HandleInternal__(self,action,id):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'action:%s;id:%s'%(action,id),self.GetOrigin())
        if action=='del':
            self.refs.deleteId(id)
        elif action=='upd':
            self.refs.clrId(id)
            self.refs.addId(id)
        else:
            vtLog.vtLngCur(vtLog.CRITICAL,'action:%s;id:%s;not handled'%(action,id),self.GetOrigin())
    def changeIdNum(self,idOld,idNew,sAlias):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'aliasType:%s;id:%08d;idNew:%08d'%(sAlias,idOld,idNew),self.GetOrigin())
        if hasattr(self,'APPL_REF'):
            if sAlias in self.APPL_REF:
                self.refs.changeId(idOld,idNew,sAlias)
    def changeId(self,node,id):
        idOld=self.getAttribute(node,self.attr)
        idNew=self.ids.ConvertIdStr2Str(id)
        vtLog.vtLngCur(vtLog.INFO,'id:%s;idNew:%s'%(idOld,idNew),self.GetOrigin())
        if idOld==idNew:
            return
        vtXmlDom.changeId(self,node,id)
        self.refs.changeId(idOld,idNew)
        if self.netMaster is not None:
            try:
                if len(self.dNetDoc.keys())==0:
                    self.netMaster.changeIdNum(long(idOld),long(idNew),self.appl)
            except:
                self.netMaster.changeIdNum(long(idOld),long(idNew),self.appl)
    def getPossibleReg(self,o):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurCls(vtLog.DEBUG,'tag:%s'%(o.GetTagName()),self)
        lst=[]
        for obj in self.lstRegNodes:
            if obj.Is2Add()==False:
                continue
            if o.IsValidChild(obj.GetTagName()):
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurCls(vtLog.DEBUG,'par tagname:%s;valid child tagname:%s;lValid:%s'%(o.GetTagName(),
                            obj.GetTagName(),vtLog.pformat(obj.lValid)),self)
                lst.append(obj)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurCls(vtLog.DEBUG,'tag:%s;lst:%s'%(o.GetTagName(),vtLog.pformat(lst)),self)
        return lst
    def getPossibleNode(self,par=None):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurCls(vtLog.DEBUG,u'par:%s'%(self.getKey(par)),self)
        lst=[]
        if par is None:
            for obj in self.lstRegNodesRoot:
                if obj.Is2Add()==False:
                    continue
                if obj.IsMultiple()==False:
                    if self.getChild(None,obj.GetTagName()) is not None:
                        continue
                lst.append((obj.GetTagName(),obj.GetDescription()))
        else:
            o=self.GetRegisteredNode(self.getTagName(par))
            if o is None:
            #if self.IsNodeKeyValid(par)==False:
                return self.getPossibleNode(None)
            o=self.GetRegisteredNode(self.getTagName(par),bLogFlt=False)
            if o is None:
                return self.getPossibleNode(None)
            for obj in self.lstRegNodes:
                if obj.Is2Add()==False:
                    continue
                if o.IsValidChild(obj.GetTagName()):
                    if obj.IsMultiple()==False:
                        if self.getChild(par,obj.GetTagName()):
                            continue
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurCls(vtLog.DEBUG,'par tagname:%s;valid child tagname:%s;lValid:%s'%(o.GetTagName(),
                                obj.GetTagName(),vtLog.pformat(obj.lValid)),self)
                    lst.append((obj.GetTagName(),obj.GetDescription()))
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurCls(vtLog.DEBUG,'par:%s;lst:%s'%(par,vtLog.pformat(lst)),self)
        return lst
    def hasPossibleNode(self,par=None):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurCls(vtLog.DEBUG,u'par:%s'%(self.getKey(par)),self)
        if par is None:
            for obj in self.lstRegNodesRoot:
                if obj.Is2Add()==False:
                    continue
                if obj.IsMultiple()==False:
                    if self.getChild(None,obj.GetTagName()) is not None:
                        continue
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurCls(vtLog.DEBUG,'valid tagname:%s'%(obj.GetTagName()),self)
                return True
        else:
            o=self.GetRegisteredNode(self.getTagName(par))
            if o is None:
                return self.hasPossibleNode(None)
            for obj in self.lstRegNodes:
                if obj.Is2Add()==False:
                    continue
                if o.IsValidChild(obj.GetTagName()):
                    if obj.IsMultiple()==False:
                        if self.getChild(par,obj.GetTagName()):
                            continue
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurCls(vtLog.DEBUG,'par tagname:%s;valid child tagname:%s'%(o.GetTagName(),obj.GetTagName()),self)
                    return True
        return False
    def isTagNamePossible2AddAtNode(self,sTagName,par=None):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurCls(vtLog.DEBUG,'par:%s'%(self.getKey(par)),self)
        if par is None:
            for obj in self.lstRegNodesRoot:
                if obj.GetTagName()!=sTagName:
                    continue
                if obj.Is2Add()==False:
                    continue
                if obj.IsMultiple()==False:
                    if self.getChild(None,obj.GetTagName()) is not None:
                        continue
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurCls(vtLog.DEBUG,'valid tagname:%s'%(obj.GetTagName()),self)
                return True
        else:
            o=self.GetRegisteredNode(self.getTagName(par))
            if o is None:
                return self.hasPossibleNode(None)
            for obj in self.lstRegNodes:
                if obj.GetTagName()!=sTagName:
                    continue
                if obj.Is2Add()==False:
                    continue
                if o.IsValidChild(obj.GetTagName()):
                    if obj.IsMultiple()==False:
                        if self.getChild(par,obj.GetTagName()):
                            continue
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurCls(vtLog.DEBUG,'par tagname:%s;valid child tagname:%s'%(o.GetTagName(),obj.GetTagName()),self)
                    return True
        return False
    def __checkReq__(self,oPar,par):
        for obj in self.lstRegNodes:
            if obj.Is2Add()==False:
                continue
            if oPar.IsValidChild(obj.GetTagName()):
                if obj.IsRequired():
                    c=obj.Create(par)
                    self.__checkReq__(obj,c)
    def __checkReqBase__(self):
        try:
            par=self.getBaseNode()
            for obj in self.lstRegNodesRoot:
                if obj.IsRequired():
                    c=obj.Create(par)
                    self.__checkReq__(obj,c)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def GetRegisteredNodes(self):
        return self.regNodes
    def GetRegisteredNode(self,tagName,bLogFlt=True):
        try:
            o=self.regNodes[tagName]
            return o
        except:
            if bLogFlt:
                vtLog.vtLngCur(vtLog.ERROR,'tag:%s not registerd'%(tagName),self.GetOrigin())
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'regNodes:%s'%(vtLog.pformat(self.regNodes)),self.GetOrigin())
            return None
    def GetReg(self,tagName,bLogFlt=True):
        return self.GetRegisteredNode(tagName,bLogFlt=bLogFlt)
    def GetRegByNode(self,node,bLogFlt=True):
        if node is None:
            return None
        return self.GetRegisteredNode(self.getTagName(node),bLogFlt=bLogFlt)
    def RegisterNode(self,obj,bRoot):
        try:
            if self.verbose>1:
                if vtLog.vtLngIsLogged(vtLog.INFO):
                    vtLog.vtLngCur(vtLog.INFO,'tagname:%s;obj:%s;bRoot:%d'%(obj.GetTagName(),repr(obj),bRoot),self.GetOrigin())
            if obj.GetTagName() not in self.regNodes:
                self.regNodes[obj.GetTagName()]=obj
                if bRoot:
                    self.lstRegNodesRoot.append(obj)
                    #if 'root' not in self.regNodes:
                    #    self.regNodes['root']=obj
                self.lstRegNodes.append(obj)
                if self.verbose>1:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'regNodes:%s;lstRegNodesRoot:%s;lstRegNodes:%s'%\
                                (vtLog.pformat(self.regNodes),vtLog.pformat(self.lstRegNodesRoot),vtLog.pformat(self.lstRegNodes)),self.GetOrigin())
            else:
                vtLog.vtLngCur(vtLog.WARN,'tagname:%s already registered;obj:%s'%(obj.GetTagName(),repr(obj)),self.GetOrigin())
            obj.SetDoc(self)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def LinkRegisteredNode(self,obj,objChild):
        try:
            if self.verbose>1:
                if vtLog.vtLngIsLogged(vtLog.INFO):
                    vtLog.vtLngCur(vtLog.INFO,'tagname:%s;obj:%s;child tagname:%s;obj:%s'%\
                            (obj.GetTagName(),repr(obj),objChild.GetTagName(),repr(objChild)),self.GetOrigin())
            if objChild.GetTagName() in self.regNodes:
                self.regNodes[obj.GetTagName()].AddValidChild(objChild.GetTagName())
            else:
                vtLog.vtLngCur(vtLog.WARN,'tagname:%s already linked;obj:%s'%(obj.GetTagName(),repr(obj)),self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def SetInfos2Get4RegisteredNodes(self):
        try:
            for o in self.regNodes.values():
                o.SetInfos2Get()
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def GetPossibleAcl(self):
        if self.dAcl is None:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'regNodes:%s'%(vtLog.pformat(self.regNodes)),self.GetOrigin())
            self.dAcl={}
            self.lAcl=[]
            try:
                for o in self.regNodes.values():
                    lst=o.GetPossibleAcl()
                    if lst is not None:
                        sTagName=o.GetTagName()
                        if sTagName not in self.dAcl:
                            self.lAcl.append(sTagName)
                            self.dAcl[sTagName]=lst
                    else:
                        if vtLog.vtLngIsLogged(vtLog.WARN):
                            vtLog.vtLngCur(vtLog.WARN,'tag:%s no ACL definied'%(o.GetTagName()),self.GetOrigin()) 
            except:
                vtLog.vtLngTB(self.GetOrigin())
            if len(self.lAcl)==0:
                vtLog.vtLngCur(vtLog.WARN,'no ACLs definied;add default',self.GetOrigin()) 
            self.lAcl.append('all')
            self.dAcl['all']=self.ACL_MSK_ALL
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'dAcl:%s'%(vtLog.pformat(self.dAcl)),self.GetOrigin())
        return self.dAcl,self.lAcl
    def UpdateCmdDict(self):
        try:
            dDocCmds=vtXmlDom.GetCmdDict(self)
            if '__regNodeCmds__' not in dDocCmds:
                # extend command dict
                dCmds={}
                try:
                    for o in self.regNodes.values():
                        dNodeCmd=o.GetCmdDict()
                        if dNodeCmd is not None:
                            dCmds[o.GetTagName()]=dNodeCmd
                        else:
                            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                vtLog.vtLngCur(vtLog.DEBUG,'tag:%s no command definied'%(o.GetTagName()),self.GetOrigin()) 
                except:
                    vtLog.vtLngTB(self.GetOrigin())
                if len(dCmds)>0:
                    dDocCmds['__regNodeCmds__']=dCmds
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def GetPossibleFilter(self):
        if self.dFilter is None:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
            self.dFilter={}
            for o in self.regNodes.values():
                lst=o.GetAttrFilterTypes()
                if lst is not None:
                    self.dFilter[o.GetTagName()]=lst
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,u'dFilter:%s'%
                        (vtLog.pformat(self.dFilter)),self.GetOrigin())
        return self.dFilter
    def GetTranslation(self,kind,name):
        o=self.GetRegisteredNode(kind)
        if o is not None:
            if name is None:
                return o.GetDescription()
            return o.GetTranslation(name)
        return name
    def GetSelectableNode(self,node):
        sTagName=self.getTagName(node)
        try:
            if sTagName in self.regNodes:
                o=self.regNodes[sTagName]
                if o.IsSkip():
                    par=self.getParent(node)
                    if par is not None:
                        return self.GetSelectableNode(par)
                    return node
        except:
            pass
        return node
    def __matchTagName__(self,node,sTag):
        if self.getTagName(node)==sTag:
            return 1
        return 0
    def CreateNode(self,nodePar,tagName,func=None,*args,**kwargs):
        if nodePar is None:
            #nodePar=self.getRoot()
            nodePar=self.getBaseNode()
        if nodePar is None:
            return None,None
        try:
            o=self.regNodes[tagName]
            #print func
            #print args,kwargs
            c=o.Create(nodePar,func,*args,**kwargs)
            return o,c
        except:
            vtLog.vtLngCur(vtLog.DEBUG,'tag:%s create fault'%(tagName),self.GetOrigin())
            vtLog.vtLngTB(self.GetOrigin())
            return None,None
    def GetCreateNodeByLst(self,node,lst):
        n=node
        for sTag in lst:
            tup=self.findChildsKeys(n,None,self.__matchTagName__,sTag)
            if tup[2] is None:
                oReg,c=self.CreateNode(n,sTag)
                if oReg is None:
                    vtLog.vtLngCur(vtLog.ERROR,'tag:%s not registered'%(sTag),self.GetOrigin())
                    c=self.createSubNode(n,sTag)
                    self.addNode(n,c)
                n=c
            else:
                n=tup[2]
        return n
    def GetNodeById(self,id):
        node=vtXmlDom.GetNodeById(self,id)
        try:
            o=self.regNodes[self.getTagName(node)]
            return o,node
        except:
            return None,node
    def GetNodeByIdNum(self,id):
        node=vtXmlDom.GetNodeByIdNum(self,id)
        try:
            o=self.regNodes[self.getTagName(node)]
            return o,node
        except:
            return None,node
    def __isSkip__(self,node):
        if vtXmlDom.__isSkip__(self,node):
            return True
        o=self.GetRegisteredNode(self.getTagName(node),False)
        if o is None:
            return False
        if hasattr(o,'IsId2Add'):
            if o.IsId2Add()==False:
                return True
        return False
    def IsSkip(self,node):
        if vtXmlDom.IsSkip(self,node):
            return True
        #return False    # 070325:wro FIXME
        o=self.GetRegisteredNode(self.getTagName(node),bLogFlt=False)
        try:
            if o.IsSkip():
                return True
        except:
            #vtLog.vtLngCS(vtLog.ERROR,'node:%s'%repr(node),origin=self.GetOrigin())
            #vtLog.vtLngTB(self.GetOrigin())
            pass
        return False
    def IsId2Add(self,node):
        vtLog.vtLngCur(vtLog.CRITICAL,'id:%s'%(self.getKey(node)),self.GetOrigin())
        #if vtXmlDom.IsId2Add(self,node):
        #    return True
        o=self.GetRegisteredNode(self.getTagName(node),bLogFlt=False)
        try:
            if o.IsId2Add():
                return True
            else:
                return False
        except:
            #vtLog.vtLngCS(vtLog.ERROR,'node:%s'%repr(node),origin=self.GetOrigin())
            #vtLog.vtLngTB(self.GetOrigin())
            return False
        return False
    def __genIds__(self,c,node,ids):
        if self.verbose>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(self.getKey(c)),self.GetOrigin())
        #print self.getTagName(c)
        #o=self.GetRegisteredNode(self.getTagName(c))
        #print self.getTagName(c),o
        if self.__isSkip__(c):
            #print '  skip'#,o.GetTagName()
            for attr in [self.attr,'offline_content','offline','fp']:
                if self.hasAttribute(c,attr):
                    self.removeAttribute(c,attr)
            return 0
        # 061122:wro cases bug due free configurable attributes
        # ++++++
        #if self.IsId2Add(c)==False:
            #print '  isadd2skip false'#,o.GetTagName()
        #    for attr in [self.attr,'offline_content','offline','fp']:
        #        if self.hasAttribute(c,attr):
        #            self.removeAttribute(c,attr)
        #    return 0
        # ------
        #print '    add id'#,o.GetTagName()
        if self.hasAttribute(c,self.attr):      # 061019: wro better solution
            ids.CheckId(c,self)
            self.updateParentDict(node,c)
        self.iElemIDCount+=1
        #id=self.getAttribute(c,self.attr)
        #if self.IsKeyValid(id):
        #    ids.CheckId(c,id)
        #else:
        #    ids.CheckId(c,None)
        #o=self.GetRegisteredNode(self.getTagName(c))
        #if o is not None:
        #    if o.IsMultipled():
        #        self.procChildsExt(c,self.__genIdsMulti__,c,ids,o.GetTagNameMultiled()) 
        self.procChildsKeys(c,self.__genIds__,c,ids)
        return 0
    def __genIdsMulti__(self,c,node,ids,tagName):
        vtLog.vtLngCur(vtLog.DEBUG,'tag:%s;chk:%s'%(self.getTagName(c),tagName),self.GetOrigin())
        if self.getTagName(c)==tagName:
            id=self.getAttribute(c,self.attr)
            if self.IsKeyValid(c):
                ids.CheckId(c,self,id)
            else:
                ids.CheckId(c,self,None)
        return 0
    def procChildsKeysOld(self,node,func,*args,**kwargs):
        if node is None:
            return
        tmp=node.children
        while tmp is not None:
            vtLdBd.check()
            if tmp.type=='element':
                if tmp.hasProp(self.attr) is not None:
                # 061122:wro cases bug due free configurable attributes
                # ++++++
                #if tmp.hasProp(self.attr) is not None or self.IsId2Add(tmp):
                # ------
                    if func(tmp,*args,**kwargs)<0:
                        return
            tmp=tmp.next
    def SetNetMaster(self,netMaster):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'netMaster is None==%s'%(netMaster is None),self.GetOrigin())
        self.netMaster=netMaster
    def GetNetMaster(self):
        return self.netMaster
    def SetNetDocs(self,d):
        """netMaster {'vHum':{'doc':hum,'bNet':False}}
        """
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'d:%s'%(vtLog.pformat(d)),self.GetOrigin())
        self.dNetDoc=d
        self.SetInfos2Get4RegisteredNodes()
    def GetNetDocs(self):
        if self.dNetDoc is None:
            if self.netMaster is not None:
                return self.netMaster.GetNetDocs()
        return self.dNetDoc
    def GetNetDoc(self,name):
        if name in self.dNetDoc:
            return self.dNetDoc[name]['doc']
        else:
            return None
    def GetNetDocDef(self,name):
        if name in self.dNetDoc:
            return self.dNetDoc[name]
        else:
            return None        
    def __getNodeByForeignId__(self,id):
        try:
            i=id.find('@')
            if i>=0:
                netDoc=self.GetNetDoc(id[i+1:])
            else:
                netDoc=self
                i=len(id)
                if i==0:
                    return None,None
            if netDoc is None:
                #vtLog.vtLngCur(vtLog.ERROR,'id:%s not found'%id,self.GetOrigin())
                return None,None
            node=netDoc.getNodeById(id[:i])
            return netDoc,node
        except:
            vtLog.vtLngTB(self.GetOrigin())
        return None,None
    def SetInfos2Get(self,infos,foreign=None,name='basic',cltData=None,bSingleLang=True):
        if foreign is None:
            netDoc=self
        else:
            netDoc=self.GetNetDoc(foreign)
        if netDoc is None:
            vtLog.vtLngCur(vtLog.WARN,'foreign:%s not found'%foreign,self.GetOrigin())
            return 
        if netDoc.hasNodeInfos2Get(name)==False:        # 070920:wro already set?
            netDoc.setNodeInfos2Get(infos,name=name,cltData=cltData,bSingleLang=bSingleLang)
        else:
            vtLog.vtLngCur(vtLog.WARN,'name:%s already set'%(name),self.GetOrigin())
    def GetInfos(self,id,lang=None,name='basic'):
        netDoc,node=self.__getNodeByForeignId__(id)
        if netDoc is None or node is None:
            vtLog.vtLngCur(vtLog.ERROR,'id:%s not found'%id,self.GetOrigin())
            return None,None
        #netDoc.acquire()
        sName,d=netDoc.getNodeInfos(node,lang=lang,name=name)
        #netDoc.release()
        return sName,d
    def GetNode(self,id):
        netDoc,node=self.__getNodeByForeignId__(id)
        if netDoc is None:
            vtLog.vtLngCur(vtLog.WARN,'id:%s doc not found'%id,self.GetOrigin())
            return None,None
        if node is None:
            vtLog.vtLngCur(vtLog.ERROR,'id:%s not found'%id,self.GetOrigin())
            return None,None
        return netDoc,node
    #def GetForeignKeyNumAppl(self,id):
    #    return self.getApplID(id)
    def GetBaseNode(self,node):
        if node is None:
            return
        sTag=self.getTagName(node)
        o=self.GetRegisteredNode(sTag,False)
        if o is None:
            par=self.getParent(node)
            if self.IsNodeKeyValid(par):
                return self.GetBaseNode(par)
            return node
        if o.IsBase():
            return node
        else:
            par=self.getParent(node)
            if self.IsNodeKeyValid(par):
                return self.GetBaseNode(par)
            return node
    def GetRelBaseNode(self,node):
        if node is None:
            return
        sTag=self.getTagName(node)
        o=self.GetRegisteredNode(sTag,False)
        if o is None:
            par=self.getParent(node)
            if self.IsNodeKeyValid(par):
                return self.GetRelBaseNode(par)
            return node
        if o.IsRelBase():
            return node
        #elif o.IsBase():
        #    return node
        else:
            par=self.getParent(node)
            if self.IsNodeKeyValid(par):
                return self.GetRelBaseNode(par)
            return node
    def __getTagNamesRaw__(self,baseNode,n,l):
        if baseNode is None:
            return
        if n is None:
            return
        if self.IsNodeKeyValid(n)==False:
            l.append(self.getNodeText(n,'tag'))
            return
        if not(self.isSame(baseNode,n)):
            l.append(self.getNodeText(n,'tag'))
            self.__getTagNamesRaw__(baseNode,self.getParent(n),l)
            return
        else:
            l.append(self.getNodeText(n,'tag'))
            return
    def GetTagNamesRaw(self,node,nodeBase=None):
        if nodeBase is None:
            nodeBase=self.getBaseNode()
        l=[]
        self.__getTagNamesRaw__(nodeBase,node,l)
        l.reverse()
        return u'.'.join(l)
    def __getTagNames__(self,baseNode,relativeNode,n,l):
        if baseNode is None:
            return
        if n is None:
            return
        if self.IsNodeKeyValid(n)==False:
            if len(l)==0:
                l.append(u'>>')
            return
        if not(self.isSame(baseNode,n)):
            if self.isSame(n,relativeNode):
                l.append(u'_')
                return
            else:
                l.append(self.getNodeText(n,'tag'))
                self.__getTagNames__(baseNode,relativeNode,self.getParent(n),l)
                return
        else:
            if self.isSame(n,relativeNode):
                l.append(u'_')
            else:
                l.append(u'>')
            return
    def __getTagNamesWithBase__(self,baseNode,relativeNode,n,l):
        if baseNode is None:
            return
        if n is None:
            return
        if self.IsNodeKeyValid(n)==False:
            if len(l)==0:
                l.append(u'>>')
            return
        if not(self.isSame(baseNode,n)):
            l.append(self.getNodeText(n,'tag'))
            self.__getTagNamesWithBase__(baseNode,relativeNode,self.getParent(n),l)
            return
        else:
            l.append(u'>')
            return
    def GetTagNames(self,node,nodeBase=None,nodeRel=None):
        if nodeBase is None:
            nodeBase=self.GetBaseNode(node)
        if nodeRel is None:
            nodeRel=self.GetRelBaseNode(node)
        l=[]
        self.__getTagNames__(nodeBase,nodeRel,node,l)
        l.reverse()
        return u'.'.join(l)
    def GetTagNamesWithRel(self,node,nodeBase=None,nodeRel=None):
        if nodeBase is None:
            nodeBase=self.GetBaseNode(node)
        if nodeRel is None:
            nodeRel=self.GetRelBaseNode(node)
        l=[]
        self.__getTagNames__(nodeBase,nodeRel,node,l)
        l.reverse()
        if len(l)>0:
            if l[0] in [u'_']:
                if self.IsNodeKeyValid(nodeRel):
                    l[0]=self.getNodeText(nodeRel,'tag')
        return u'.'.join(l)
    def GetTagNamesWithBase(self,node,nodeBase=None,nodeRel=None):
        if nodeBase is None:
            nodeBase=self.GetBaseNode(node)
        if nodeRel is None:
            nodeRel=self.GetRelBaseNode(node)
        l=[]
        self.__getTagNamesWithBase__(nodeBase,nodeRel,node,l)
        l.reverse()
        if len(l)>0:
            if l[0] in [u'>']:
                if self.IsNodeKeyValid(nodeBase):
                    l[0]=self.getNodeText(nodeBase,'tag')
        return u'.'.join(l)
    def GetInfos4Tree(self,node,lang=None,name='tree'):
        sName=self.getTagName(node)
        try:
            o=self.GetRegisteredNode(sName)
            if o is None:
                return sName,None,None
            d=o.GetInfos4Tree(node)#netDoc.getNodeInfos(node,lang=lang,name=name)
            return sName,o,d
        except:
            return sName,None,None
    def GetInfos4Lst(self,node,lang=None,name='lst'):
        sName=self.getTagName(node)
        try:
            o=self.GetRegisteredNode(sName)
            if o is None:
                return sName,None,None
            l=o.AddValuesToLst(node)
            return sName,o,l
        except:
            return sName,None,None
    def GetRegData(self,oReg,node,dImg={},thd=None,func=None,args=(),kwargs={},**kw):
        try:
            t=oReg.GetData(node,dImg=dImg,thd=thd,**kw)
            if func is not None:
                tt=t+args
                self.CallBack(func,*tt,**kwargs)
            return t
        except:
            self.__logTB__()
        return None
    def GetData(self,node,dImg={},thd=None,func=None,args=(),kwargs={},**kw):
        """ lHdr = [lColDef0,lColDef1,...]
        lColDefx =[
        name,       ... column name
        align,      ... column alignment 
                                -1=left
                                -2=center
                                -3=right
                                else unchanged
        width,      ... column width
        sort,       ... column sortable 
                                0=not sortable
                                1=sortable initially ascending 
                               -1=sortable initially descending
        ]
        """
        try:
            self.__logDebug__(''%())
            #lData,lHdr,dSortDef
            sName=self.getTagName(node)
            oReg=self.GetRegisteredNode(sName)
            if oReg is None:
                self.__logError__('sName:%s not registered'%(sName))
                return
            t=self.GetRegData(oReg,node,dImg=dImg,thd=thd,
                    func=func,args=args,kwargs=kwargs,**kw)
            return t
        except:
            self.__logTB__()
        return None
    #def BuildReq(self):
    def CreateReq(self):
        try:
            vtLog.vtLngCur(vtLog.INFO,'',self.GetOrigin())
            lIds=[]
            if self.root is not None:
                for obj in self.lstRegNodesRoot:
                    if obj.IsRequired():
                        nodeTmp=self.getChild(None,obj.GetTagName())
                        if nodeTmp is None:
                            #if self.IsAclOk(obj.GetTagName(),self.ACL_MSK_ADD): # 061004 wro import!
                                nodeTmp=obj.Create(self.getBaseNode())
                                self.AlignNode(nodeTmp)
                for obj in self.lstRegNodes:
                    if obj.IsRequired()==False:
                        continue
                    #print obj.GetTagName(),obj.IsRequired(),hasattr(obj,'GetCfgBase')
                    #print '   ',obj.IsBase()
                    node=self.getBaseNode()
                    if hasattr(obj,'GetCfgBase'):
                        lTagNames=obj.GetCfgBase()
                        for sTagName in lTagNames:
                            nodeTmp=self.getChild(node,sTagName)
                            if nodeTmp is None:
                                o=self.GetReg(sTagName,False)
                                nodeTmp=None
                                if o is not None:
                                    #if o.IsAclOk(self.ACL_MSK_ADD)==False:  #070818:wro check acl
                                    #    break
                                    nodeTmp=obj.Create(node)
                                    if nodeTmp is not None:
                                        self.AlignNode(nodeTmp)
                                        node=nodeTmp
                                        lIds.append(self.getKeyNum(node))
                                if nodeTmp is None:
                                    nodeTmp=self.createSubNodeAttr(node,sTagName,
                                            self.attr,'',align=True)
                                    if nodeTmp is not None:
                                        node=nodeTmp
                                        lIds.append(self.getKeyNum(node))
                                    else:
                                        vtLog.vtLngCur(vtLog.ERROR,'lTagNames:%s;sTagName:%s could not be created.'%
                                                (lTagNames,sTagName),self.GetOrigin())
                                        break
                            else:
                                node=nodeTmp
                    #nodeTmp=obj.Create(node)
                    #print nodeTmp
                    #self.AlignNode(nodeTmp)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        #print lIds
        return lIds
    def GetCfgByNode(self,node):
        try:
            bDbg=False
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                bDbg=True
            oReg=self.GetRegByNode(node)
            if oReg is None:
                return {}
            lang=self.GetLang()
            sTag=oReg.GetTagName()
            if hasattr(oReg,'GetCfgDict'):
                dCfg=oReg.GetCfgDict(None,lang)
            else:
                dCfg={}
            if bDbg:
                vtLog.vtLngCur(vtLog.DEBUG,'sTagName:%s;dCfg:%s'%(\
                        sTag,vtLog.pformat(dCfg)),
                        self.GetOrigin())
            lTagNames2Base=['cfg',sTag]
            nodeCfg=self.getChildByLst(self.getBaseNode(),lTagNames2Base)
            if nodeCfg is None:
                vtLog.vtLngCur(vtLog.WARN,'nodeCfg is None;'
                        'sTagName:%s;l:%s'%(\
                        sTag,vtLog.pformat(lTagNames2Base)),
                        self.GetOrigin())
                return dCfg
            obj=self.GetReg('attrML')
            if obj is None:
                vtLog.vtLngCur(vtLog.ERROR,
                        'reg node %s not found'%('attrML'),
                        self.GetOrigin())
                return
            dNode=obj.GetCfgDict(nodeCfg,lang)
            if bDbg:
                vtLog.vtLngCur(vtLog.DEBUG,'sTagName:%s;dCfg:%s'%(\
                        sTag,vtLog.pformat(dNode)),
                        self.GetOrigin())
            dCfg.update(dNode)
            if bDbg:
                vtLog.vtLngCur(vtLog.DEBUG,'sTagName:%s;dCfg:%s'%(\
                        sTag,vtLog.pformat(dCfg)),
                        self.GetOrigin())
            return dCfg
        except:
            vtLog.vtLngTB(self.GetOrigin())
            return {}
    def Build(self):
        #self.BuildReq()    # 061006 wro
        #self.CreateReq()
        vtLog.vtLngCur(vtLog.INFO,'',self.GetOrigin())
        for obj in self.lstRegNodes:
            obj.Build()
        self.BuildLang()
    def BuildLang(self):
        vtLog.vtLngCur(vtLog.INFO,'',self.GetOrigin())
        for obj in self.lstRegNodes:
            obj.BuildLang()
        #del self.dFilter
        self.dFilter=None
        self.GetPossibleFilter()
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'dFilter:%s'%(vtLog.pformat(self.dFilter)),self.GetOrigin())
    def AddValueToDictByDict(self,node,dDef,dVal,bMultiple,*args,**kwargs):
        obj=self.GetRegByNode(node,bLogFlt=False)
        if obj is None:
            return vtXmlDom.AddValueToDictByDict(self,node,dDef,dVal,bMultiple,*args,**kwargs)
        elif hasattr(obj,'AddValueToDictByDict'):
            return obj.AddValueToDictByDict(node,dDef,dVal,bMultiple,*args,**kwargs)
        else:
            return vtXmlDom.AddValueToDictByDict(self,node,dDef,dVal,bMultiple,*args,**kwargs)
    def __AddValueToDictByDict__(self,node,dDef,dVal,bMultiple,*args,**kwargs):
            return vtXmlDom.AddValueToDictByDict(self,node,dDef,dVal,bMultiple,*args,**kwargs)
    def GetNodeAttribute(self,node,attr,*args,**kwargs):
        try:
            o=self.GetRegByNode(node,bLogFlt=False)
            if o is None:
                return ''
            if hasattr(o,attr):
                return getattr(o,attr)(node,*args,**kwargs)
            else:
                return ''
        except:
            vtLog.vtLngTB(self.GetOrigin())
        return ''
    def GetAttrValueToDict(self,node,*args,**kwargs):
        dVal=vtXmlDom.GetAttrValueToDict(self,node,*args,**kwargs)
        oReg=self.GetRegByNode(node,bLogFlt=False)
        if oReg is not None:
            if hasattr(oReg,'GetAttrValueToDict'):
                return oReg.GetAttrValueToDict(dVal,node,*args,**kwargs)
        return dVal
    def getNodeAttributeValFullLang(self,node,tagName,lang=None):
        #vtLog.CallStack('')
        vtLog.vtLngCur(vtLog.DEBUG,'tag:%s;chk:%s'%(self.getTagName(node),tagName),self.GetOrigin())
        #print tagName
        if tagName.find(':')>0:
            oReg=self.GetRegByNode(node,bLogFlt=False)
            if oReg is not None:
                if hasattr(oReg,'GetAttributeValFullLang'):
                    return oReg.GetAttributeValFullLang(node,tagName,lang=lang)
        elif tagName.startswith('__link_'):
            vtLog.vtLngCur(vtLog.DEBUG,'tag:%s;chk:%s;try follow link'%(self.getTagName(node),tagName),self.GetOrigin())
            c=self.getChild(node,tagName)
            iFID,sFAppl=self.getForeignKeyNumAppl(c)
            if iFID>=0:
                n=self.getNodeByIdNum(iFID)
                if n is not None:
                    sLnkAttr=self.getText(c)
                    lLnkAttr=sLnkAttr.split(':')
                    sLnk=lLnkAttr[-1]
                    vtLog.vtLngCur(vtLog.DEBUG,'tag:%s;chk:%s;iFID:%s;sLnkAttr:%s;sLnk:%s;follow link'%(self.getTagName(node),
                            tagName,iFID,sLnkAttr,sLnk),self.GetOrigin())
                    return self.getNodeAttributeValFullLang(n,sLnk,lang=lang)
        return vtXmlDom.getNodeAttributeValFullLang(self,node,tagName,lang=lang)
    def validateNode(self,node):
        oReg=self.GetRegByNode(node,bLogFlt=True)
        if oReg is not None:
            if hasattr(oReg,'ValidateAttr'):
                oReg.ValidateAttr(node)
    def __New__Old(self,bConnection=False):
        lRoot=self.getPossibleNode(None)
        rootNode=self.getRoot()
        print lRoot
        for sTag,sDesc in lRoot:
            print sTag,sDesc 
            elem=self.createSubNode(rootNode,sTag)

if __name__=='__main__':
    from vidarc.tool.xml.vtXmlNodeBase import vtXmlNodeBase
    class vtxnTest(vtXmlNodeBase):
        NODE_ATTRS=[
            ('name','lang','nameX','language'),
            ('desc','lang','description','language'),
            ('value',None,'value',None),
            ]
        def GetTagName(self):
            return 'test'
        
    doc=vtXmlDomReg('ID',verbose=1)
    doc.RegisterNode(vtXmlNodeBase())
    doc.RegisterNode(vtxnTest())
    
    doc.New()
    obj,node=doc.CreateNode(None,'test')
    obj.SetValue(node,name='test default')
    obj.SetValue(node,name='test english',lang='en')
    obj.SetValue(node,name='test deutsch',lang='de',desc='Beschreibung')
    obj.SetValue(node,value='val0')
    print " 0",obj.GetValue(node)
    print " 1",obj.GetValue(node,name='test')
    print " 2",obj.GetValue(node,name='test',lang='en')
    print " 3",obj.GetValue(node,name='test',lang='de')
    
    print " 4",obj.GetValue(node,value='')
    print " 5",obj.GetValue(node,value='',lang='de')
    sRes=obj.GetValue(node,value='')
    print " 6",sRes
    
    tup=doc.GetNodeById('0')
    print " 7",tup,doc.getTagName(tup[1])
    tup=doc.GetNodeByIdNum(0)
    print " 8",tup
    
    doc.AlignDoc()
    doc.Save('test/domReg01.xml')
    