#----------------------------------------------------------------------
# Name:         vtXmlNodeDescPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120527
# CVS-ID:       $Id: vtXmlNodeDescNavPanel.py,v 1.7 2015/04/27 06:42:45 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    from vGuiPanelWX import vGuiPanelWX
    import vidarc.tool.lang.vtLgBase as vtLgBase
    from vidarc.tool.input.vtInputTreeExt import vtInputTreeExt
    from vidarc.tool.xml.vtXmlNodeTagInfoPanel import vtXmlNodeTagInfoPanel
    from vidarc.tool.xml.vtXmlNodeDescPanel import vtXmlNodeDescPanel
    
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vtXmlNodeDescNavPanel(vGuiPanelWX):
    VERBOSE=0
    def __init__(self,*args,**kwargs):
        global _
        _=vtLgBase.assignPluginLang('vidarc.tool')
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        #self.SetVerbose(__name__,iVerbose=-1,kwargs=kwargs)
        lWidExt=[
            (vtXmlNodeDescPanel,    1,0,{'name':'pnDesc',
                                                },None),
            ]
        lWid=[
            ]
        lWidget={
            'slwNav':{
                'size':(200,10),
                'lWid':[
                    (vtInputTreeExt,        (0,0),(1,2),{'name':'trDesc','tip':'tree for description',
                                                        'mod':False,#'bSuppressNetNotify':True,
                                                        'lPropNode':1,'lPropAttr':1,'iOrientation':0,
                                                        #'lWid':lWidExt,
                                                        },None),
                ],
                'lGrowRow':[(0,1),],
                'lGrowCol':[(0,1)],
            },
            'pnData':{
                'size':(100,200),
                
                'lWid':[
                    (vtXmlNodeTagInfoPanel, (0,0),(1,1),{'name':'pnTag',
                                                        },None),
                    (vtXmlNodeDescPanel,    (1,0),(1,1),{'name':'pnDesc','bAutoApply':False,
                                                        },None),
                ],
                'lGrowRow':[(1,1),],
                'lGrowCol':[(0,1)],
            },
            }
        lGrowRow=[(0,2),(2,1),]
        lGrowCol=[(0,1),(1,4),]
        lWidAdd=kwargs.get('lWid',[])
        kwargs['lWid']=lWidget
        vGuiPanelWX.__init__(self,*args,**kwargs)
        self.trDesc=self.pnNav.trDesc
        self.pnTag=self.pnData.pnTag
        self.trDesc.BindEvent('cmd',self.OnTreeCmd)
        #self.pnDesc=self.trDesc.pnDesc
        self.pnDesc=self.pnData.pnDesc
        #return lGrowRow,lGrowCol
    def __initCls__(self,*args,**kwargs):
        self.SetVerbose(__name__,iVerbose=VERBOSE,kwargs=kwargs)
    def OnApply(self,evt):
        try:
            self.__logDebug__(''%())
            self.pnDesc.Apply()
        except:
            self.__logTB__()
    def OnCancel(self,evt):
        try:
            self.__logDebug__(''%())
            self.pnDesc.Cancel()
        except:
            self.__logTB__()
    def OnTreeCmd(self,evt):
        try:
            sCmd=evt.GetCmd()
            sData=evt.GetData()
            self.__logDebug__({'sCmd':sCmd,'sData':sData})
            if sCmd=='sel':
                self.pnTag.SetNodeById(sData)
                self.pnDesc.SetNodeById(sData)
        except:
            self.__logTB__()
    def SetDoc(self,doc,bNet=False):
        try:
            self.__logDebug__('doc is None=%d'%(doc is None))
            self.trDesc.SetDoc(doc,bNet=bNet)
            self.pnTag.SetDoc(doc,bNet=bNet)
            self.pnDesc.SetDoc(doc,bNet=bNet)
        except:
            self.__logTB__()
    def ClearDoc(self):
        try:
            self.__logDebug__(''%())
            self.trDesc.ClearDoc()
            self.pnTag.ClearDoc()
            self.pnDesc.ClearDoc()
        except:
            self.__logTB__()
    def SetNodeByID(self,sID):
        try:
            self.__logDebug__('sID:%r'%(sID))
            node=self.doc.getNodeById(sID)
            self.trDesc.SetNode(node)
        except:
            self.__logTB__()
    def SetNode(self,node,sID):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            self.__logDebug__('sID:%r'%(sID))
            self.trDesc.SetNode(node)
        except:
            self.__logTB__()
    def dummy(self):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            pass
        except:
            self.__logTB__()
    def IsShutDownActive(self):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            if self.trDesc.HasDoc():
                return False
            return False
        except:
            self.__logTB__()
    def IsShutDownFinished(self):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            if self.trDesc.HasDoc():
                return False
            return True
        except:
            self.__logTB__()
    def ShutDown(self):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            #self.trDesc.ClearDoc()
            self.trDesc.SetDoc(None,bNet=True)
            self.pnDesc.SetDoc(None,bNet=True)
        except:
            self.__logTB__()
    def DoShutDown(self):
        try:
            if self.IsMainThread()==False:
                return
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            self.Close()
        except:
            self.__logTB__()
