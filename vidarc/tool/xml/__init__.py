#----------------------------------------------------------------------------
# Name:         __init__.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: __init__.py,v 1.5 2007/09/02 14:56:40 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

PLUGIN_TRANS='vtXml'

import sys
if hasattr(sys, 'importers'):
    try:
        SORT_CASESENSITVE=0
        from build_options import *
    except:
        SORT_CASESENSITVE=0
else:
    try:
        from build_options import *
    except:
        SORT_CASESENSITVE=1
