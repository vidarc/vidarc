#----------------------------------------------------------------------------
# Name:         vtXmlTreeCache.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtXmlGrp.py,v 1.1 2005/12/11 23:04:49 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
from wx import NewEventType as wxNewEventType
from wx import PyEvent as wxPyEvent
from wx import PostEvent as wxPostEvent

import vtXmlDom as vtXmlDom
import vidarc.tool.log.vtLog as vtLog

import thread,fnmatch


# defined event for vgpXmlTree item selected
wxEVT_THREAD_GROUP_ELEMENTS=wxNewEventType()
def EVT_THREAD_GROUP_ELEMENTS(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_GROUP_ELEMENTS,func)
class wxThreadGroupElements(wxPyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_GROUP_ELEMENTS(<widget_name>, self.OnItemSel)
    """

    def __init__(self,iVal):
        wxPyEvent.__init__(self)
        self.val=iVal
        self.SetEventType(wxEVT_THREAD_GROUP_ELEMENTS)
    def GetValue(self):
        return self.val

# defined event for vgpXmlTree item selected
wxEVT_THREAD_GROUP_ELEMENTS_FINISHED=wxNewEventType()
def EVT_THREAD_GROUP_ELEMENTS_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_GROUP_ELEMENTS_FINISHED,func)
class wxThreadGroupElementsFinished(wxPyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_GROUP_ELEMENTS_FINISHED(<widget_name>, self.OnItemSel)
    """
    def __init__(self):
        wxPyEvent.__init__(self)
        self.SetEventType(wxEVT_THREAD_GROUP_ELEMENTS_FINISHED)

# defined event for vgpXmlTree item selected
wxEVT_THREAD_GROUP_ELEMENTS_ABORTED=wxNewEventType()
def EVT_THREAD_GROUP_ELEMENTS_ABORTED(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_GROUP_ELEMENTS_ABORTED,func)
class wxThreadGroupElementsAborted(wxPyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_GROUP_ELEMENTS_ABORTED(<widget_name>, self.OnItemSel)
    """
    def __init__(self):
        wxPyEvent.__init__(self)
        self.SetEventType(wxEVT_THREAD_GROUP_ELEMENTS_ABORTED)


class thdGroup:
    def __init__(self,doc=None,verbose=0):
        self.verbose=verbose
        self.doc=doc
        self.running=False
    def Group(self,lst,par):
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'')
        self.par=par
        self.lst=lst
        self.iAct=0
        if self.par is not None:
            self.updateProcessbar=True
        else:
            self.updateProcessbar=False
        
        self.Start()
    def Clear(self):
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'')
        self.iAct=0
            
    def Show(self):
        self.bFind=False
        if self.running==False:
            self.Start()
    def Start(self):
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'')
        self.keepGoing = self.running = True
        thread.start_new_thread(self.Run, ())

    def Stop(self):
        self.keepGoing = False

    def IsRunning(self):
        return self.running
    
    def Run(self):
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'')
        self.Clear()
        if self.updateProcessbar:
            wxPostEvent(self.par,wxThreadGroupElements(0))
        for node in self.lst:
            self.iAct+=1
            if self.updateProcessbar:
                wxPostEvent(self.par,wxThreadGroupElements(self.iAct))
            self.doc.acquire()
            tagName,infos=self.doc.getNodeInfos(node)
            try:
                bFound=self.doc.__procGroup__(node,tagName,infos)
            except:
                self.Stop()
                self.doc.release()
                break
            self.doc.release()
        
        self.keepGoing = self.running = False
        if self.updateProcessbar:
            wxPostEvent(self.par,wxThreadGroupElements(0))
            wxPostEvent(self.par,wxThreadGroupElementsFinished())
    

class vtXmlGroup(vtXmlDom.vtXmlDom):
    def __init__(self,verbose=0):
        vtXmlDom.vtXmlDom.__init__(self)
        self.verbose=verbose
        self.groups={}
        self.grouping=[]
        self.groupingIdx=[]
        self.thdGroup=thdGroup(self,verbose=verbose-1)
    def IsRunning(self):
        return self.thdGroup.IsRunning()
    def setGroup(self,grouping,idxs):
        self.groups={}
        self.grouping=grouping
        self.groupingIdx=idxs
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
    def __procGroup__(self,node,tagName,infos):
        dTmp=self.groups
        for grp in self.grouping[:-1]:
            k=grp[0]
            if grp[1] is None:
                val=infos[k]
            else:
                val=grp[1](infos[k])
            try:
                d=dTmp[val]
            except:
                d={}
                dTmp[val]=d
            dTmp=d
        grp=self.grouping[-1]
        k=grp[0]
        if grp[1] is None:
            val=infos[k]
        else:
            val=grp[1](infos[k])
        try:
            lst=dTmp[val]
        except:
            lst=[]
            dTmp[val]=lst
        lst.append(node)
        return True
    def Group(self,lst,par=None):
        self.groups={}
        self.thdGroup.Group(lst,par)
    def GetGrouping(self):
        return self.grouping
    def GetGroupingIdx(self):
        return self.groupingIdx
    def GetGrouped(self):
        return self.groups
    