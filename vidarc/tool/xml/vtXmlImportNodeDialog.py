#Boa:Dialog:vtXmlImportNodeDialog
#----------------------------------------------------------------------------
# Name:         vDataPanel.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vtXmlImportNodeDialog.py,v 1.10 2015/02/27 20:24:09 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputTree
import vidarc.tool.input.vtInputTreeInternal
import wx.lib.filebrowsebutton
import wx.lib.buttons

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.tool.vtThread as vtThread

from vidarc.tool.xml.vtXmlDomConsumer import * 
import vidarc.tool.xml.vtXmlDom as vtXmlDom
#import vidarc.tool.xml.vtXmlThread as vtXmlThread
from vidarc.tool.net.vNetXmlWxGuiEvents import EVT_NET_XML_ADD_NODE_RESPONSE
from vidarc.tool.net.vNetXmlWxGuiEvents import EVT_NET_XML_ADD_NODE_RESPONSE_DISCONNECT
from vidarc.tool.net.vNetXmlWxGuiEvents import EVT_NET_XML_LOCK
from vidarc.tool.net.vNetXmlWxGuiEvents import EVT_NET_XML_LOCK_DISCONNECT

import os.path,time,stat,threading

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

def create(parent):
    return vtXmlImportNodeDialog(parent)

[wxID_VTXMLIMPORTNODEDIALOG, wxID_VTXMLIMPORTNODEDIALOGFBBFN, 
 wxID_VTXMLIMPORTNODEDIALOGGCBBCANCEL, wxID_VTXMLIMPORTNODEDIALOGGCBBGENERATE, 
 wxID_VTXMLIMPORTNODEDIALOGLBLIMP, wxID_VTXMLIMPORTNODEDIALOGLBLTIME, 
 wxID_VTXMLIMPORTNODEDIALOGPBIMP, wxID_VTXMLIMPORTNODEDIALOGTXTDATE, 
 wxID_VTXMLIMPORTNODEDIALOGTXTTIME, wxID_VTXMLIMPORTNODEDIALOGVINODE2IMP, 
] = [wx.NewId() for _init_ctrls in range(10)]

class vtXmlImportNodeDialog(wx.Dialog,vtXmlDomConsumer):
    VERBOSE=0
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.gcbbGenerate, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(16, 8), border=0, flag=0)
        parent.AddWindow(self.gcbbCancel, 0, border=0, flag=0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.fbbFN, 0, border=4, flag=wx.EXPAND | wx.ALL)
        parent.AddSizer(self.bxsTm, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsSrc, 0, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT | wx.TOP)
        parent.AddWindow(self.pbImp, 0, border=4, flag=wx.EXPAND | wx.ALL)
        parent.AddSizer(self.bxsBt, 0, border=4, flag=wx.ALIGN_CENTER | wx.ALL)

    def _init_coll_bxsSrc_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblImp, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viNode2Imp, 3, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsTm_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblTime, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtDate, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.txtTime, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=6, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsTm = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsSrc = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsTm_Items(self.bxsTm)
        self._init_coll_bxsSrc_Items(self.bxsSrc)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTXMLIMPORTNODEDIALOG,
              name=u'vtXmlImportNodeDialog', parent=prnt, pos=wx.Point(181,
              101), size=wx.Size(357, 181), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vtXml Import Node Dialog')
        self.SetClientSize(wx.Size(349, 154))

        self.fbbFN = wx.lib.filebrowsebutton.FileBrowseButton(buttonText='...',
              dialogTitle=_(u'Choose a file'), fileMask=u'*.xml',
              id=wxID_VTXMLIMPORTNODEDIALOGFBBFN, labelText=_(u'File Entry:'),
              parent=self, pos=wx.Point(4, 4), size=wx.Size(341, 29),
              startDirectory='.', style=0)

        self.lblTime = wx.StaticText(id=wxID_VTXMLIMPORTNODEDIALOGLBLTIME,
              label=_(u'time'), name=u'lblTime', parent=self, pos=wx.Point(4,
              41), size=wx.Size(85, 21), style=wx.ALIGN_RIGHT)
        self.lblTime.SetMinSize((-1,-1))

        self.txtDate = wx.TextCtrl(id=wxID_VTXMLIMPORTNODEDIALOGTXTDATE,
              name=u'txtDate', parent=self, pos=wx.Point(93, 41),
              size=wx.Size(81, 21), style=0, value=u'')
        self.txtDate.SetMinSize((-1,-1))
        self.txtDate.Enable(False)

        self.txtTime = wx.TextCtrl(id=wxID_VTXMLIMPORTNODEDIALOGTXTTIME,
              name=u'txtTime', parent=self, pos=wx.Point(178, 41),
              size=wx.Size(166, 21), style=0, value=u'')
        self.txtTime.SetMinSize((-1,-1))
        self.txtTime.Enable(False)

        self.lblImp = wx.StaticText(id=wxID_VTXMLIMPORTNODEDIALOGLBLIMP,
              label=u'node to import', name=u'lblImp', parent=self,
              pos=wx.Point(4, 66), size=wx.Size(85, 24), style=wx.ALIGN_RIGHT)
        self.lblImp.SetMinSize((-1,-1))

        self.viNode2Imp = vidarc.tool.input.vtInputTreeInternal.vtInputTreeInternal(id=wxID_VTXMLIMPORTNODEDIALOGVINODE2IMP,
              name=u'viNode2Imp', parent=self, pos=wx.Point(93, 66),
              size=wx.Size(251, 24), style=0)

        self.pbImp = wx.Gauge(id=wxID_VTXMLIMPORTNODEDIALOGPBIMP, name=u'pbImp',
              parent=self, pos=wx.Point(4, 94), range=1000, size=wx.Size(341,
              13), style=wx.GA_SMOOTH | wx.GA_HORIZONTAL)

        self.gcbbGenerate = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLIMPORTNODEDIALOGGCBBGENERATE,
              bitmap=vtArt.getBitmap(vtArt.Import), label=_(u'Import'),
              name=u'gcbbGenerate', parent=self, pos=wx.Point(90, 115),
              size=wx.Size(76, 30), style=0)
        self.gcbbGenerate.SetMinSize((-1,-1))
        self.gcbbGenerate.Enable(False)
        self.gcbbGenerate.Bind(wx.EVT_BUTTON, self.OnGcbbGenerateButton,
              id=wxID_VTXMLIMPORTNODEDIALOGGCBBGENERATE)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLIMPORTNODEDIALOGGCBBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'gcbbCancel', parent=self, pos=wx.Point(182, 115),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.SetMinSize((-1,-1))
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VTXMLIMPORTNODEDIALOGGCBBCANCEL)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self._init_ctrls(parent)
        vtXmlDomConsumer.__init__(self)
        
        icon = vtArt.getIcon(vtArt.getBitmap(vtArt.Import))
        self.SetIcon(icon)
        
        self.viNode2Imp.AddTreeCall('init','SetNodeInfos',['tag','name','|id'])
        self.viNode2Imp.AddTreeCall('init','SetGrouping',[],[('tag',''),('name','')])
        self.viNode2Imp.SetTagNames('tag','tag')
        #self.viNode2Imp.SetTagNames2Base(['export','data'])
        
        self.thdOpenFile=vtThread.vtThread(self)
        self.thdImport=vtThread.vtThread(self,bPost=True)
        self.thdImport.BindEvents(self.OnProcImport)
        self.semSend=threading.Lock()#threading.Semaphore()
        
        self.bNetResponse=False
        self.nodeSettings=None
        self.selNode=None
        self.domImp=None
        self.impNode=None
        self.impData=None
        self.node2inst=None
        self.fbbFN.changeCallback=self.OnGcbbBrowseButton
        self.fgsData.Layout()
        self.fgsData.Fit(self)
    def Clear(self):
        vtXmlDomConsumer.Clear(self)
        self.nodeSettings=None
    def Stop(self):
        if self.IsShown():
            self.EndModal(0)
    def IsBusy(self):
        return self.IsShown()
    def SetDoc(self,doc,bNet=False):
        if self.doc is not None:
            if self.bNetResponse:
                EVT_NET_XML_ADD_NODE_RESPONSE_DISCONNECT(self.doc,self.OnAddNodeResp)
                EVT_NET_XML_LOCK_DISCONNECT(self.doc,self.OnLock)
        vtXmlDomConsumer.SetDoc(self,doc)
        if self.doc is not None:
            if bNet:
                EVT_NET_XML_ADD_NODE_RESPONSE(self.doc,self.OnAddNodeResp)
                EVT_NET_XML_LOCK(self.doc,self.OnLock)
        vtLog.CallStack('')
        self.bNetResponse=bNet
        #print trClass
        #self.viNode2Imp.SetTreeFunc(trClass,None)
        
        #self.vgpTree.SetDoc(doc)
    def SetNode(self,node,nodeSettings):
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,'node:%s;nodeSettings:%s'%(\
                    self.doc.getKey(node),
                    self.doc.getTagName(nodeSettings)),self)
            vtXmlDomConsumer.SetNode(self,node)
            
            self.nodeSettings=nodeSettings
            self.sImpDN=''
            if self.nodeSettings is not None:
                self.sImpDN=self.doc.getNodeText(nodeSettings,'importdn')
            if len(self.sImpDN)<=0:
                self.sImpDN=os.path.expanduser('~')
            self.fbbFN.SetValue(self.sImpDN)
        except:
            vtLog.vtLngTB(self.GetName())
        return
        #self.node2inst=node2instance
        self.vgpTree.SetNode(node)
        self.node=node
        if self.node is not None:
            node=self.doc.getChild(self.doc.getParent(self.node),'prjinfo')
        else:
            node=None
        if node is None:
            self.prjId=u''
            self.sShort=u''
            self.sClient=u''
        else:
            self.prjId=self.doc.getAttribute(node,'fid')
            self.sShort=self.doc.getNodeText(node,'name')
            self.sClient=self.doc.getNodeText(node,'clientshort')
        
        self.nodeSettings=nodeSettings
        if self.nodeSettings is not None:
            self.sImpFN=self.doc.getNodeText(nodeSettings,'importfn')
        else:
            self.sImpFN=''
        
        self.domImp=None
        self.impNode=None
        self.impData=None
        self.node2inst=None
        self.txtDate.SetValue(u'')
        self.txtTime.SetValue(u'')
        self.txtClient.SetValue(u'')
        self.txtPrj.SetValue(u'')
        self.txtPrjTaskFN.SetValue(self.sImpFN)
        self.vgpImpTree.SetNode(None)
        pass
    def GetSelNode(self):
        return self.selNode
    
    def SetXml(self,node):
        self.panel.SetXml(node)
    def GetXml(self,node):
        self.panel.GetXml(node)
        
    def OnGcbbCancelButton(self, event):
        event.Skip()
        if self.thdOpenFile.IsRunning():
            return
        if self.thdImport.IsRunning():
            dlg=wx.MessageDialog(self,_('Do you really want to abort import?'),
                                'vtXml Import Node Dialog',
                                wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            if dlg.ShowModal()==wx.ID_YES:
                self.thdImport.Stop()
            dlg.Destroy()
            return
        self.EndModal(0)
    def __checkPossible__(self,node):
        if self.node is None:
            return -1
        if node is None:
            return -2
        sTypeNode2Inst=self.doc.getTagName(self.node)
        sType=self.doc.getTagName(node)
        try:
            oReg=self.doc.GetReg(sTypeNode2Inst)
            if oReg.IsValidChild(sType):
                return 1
            else:
                return 0
        except:
            return -5
        return 0
    def __clearImport__(self):
        self.dId={}
        self.dIdAdd={}
        self.dEdit={}
        self.iCount=1
        self.iAct=self.iSend=self.iRecv=0
        try:
            if self.node is not None:
                self.doc.AlignNode(self.node,iRec=10)
        except:
            vtLog.vtLngTB(self.GetName())
        self.pbImp.SetValue(0)
        self.gcbbGenerate.Enable(True)
        
    def __clearAll__(self):
        if self.domImp is not None:
            self.domImp.Close()
            del self.domImp
        self.domImp=None
        self.impNode=None
        self.impData=None
        self.node2inst=None
        self.__clearImport__()
        
        self.txtDate.SetValue(u'')
        self.txtTime.SetValue(u'')
        self.Clear()
    def __cntNode__(self,node):
        try:
            self.iCount+=1
        except:
            vtLog.vtLngTB(self.GetName())
        return 1
    def __addNode__(self,node,nodeTarget):
        try:
            if self.thdImport.Is2Stop():
                return -1
            vtLog.vtLngCurWX(vtLog.DEBUG,self.domImp.getKey(node),self)
            child=self.domImp.CopyNode(node,False,False)
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'orig:%s;copy:%s'%(node,child),self)
                sCont=self.domImp.GetNodeXmlContent(node,False,False)
                print sCont,type(sCont)
            parNodeImp=self.domImp.getParent(node)
            parIDimp=self.domImp.getKey(parNodeImp)
            #if self.domImp.IsKeyValid(parIDimp)==False:
            #    parNode=self.node
            #elif self.domImp.isSameKey(self.node2instpar,parIDimp):
            #    parNode=self.node
            #else:
            if 0:
                parNode=None
                parIDimp=long(parIDimp)
                while parNode is None:
                    try:
                        idAdd=self.dId[parIDimp]
                        idNew=self.dIdAdd[idAdd]
                        if self.VERBOSE:
                            vtLog.vtLngCurWX(vtLog.DEBUG,'parIDimp:%08d newID:%08d'%(parIDimp,idNew),self)
                        parNode=self.doc.getNodeByIdNum(idNew)
                        if parNode is not None:
                            break
                        else:
                            vtLog.vtLngCurWX(vtLog.ERROR,'id:%08d not found;this shall never happen'%idNew,self)
                    except:
                        if self.VERBOSE:
                            vtLog.vtLngCurWX(vtLog.DEBUG,'parIDimp:%08d not found yet'%(parIDimp),self)
                        pass
                    time.sleep(1)
                    if self.thdImport.Is2Stop():
                        return -1
            #self.semSend.acquire()
            self.doc.validateNode(child)
            #self.doc.appendChild(parNode,child)
            #self.doc.addNode(parNode,child)
            self.doc.appendChild(nodeTarget,child)
            self.doc.addNode(nodeTarget,child,self._addId,nodeImp=node)
            idAdd=long(self.doc.getKey(child))
            #if self.VERBOSE:
            #    vtLog.vtLngCurWX(vtLog.DEBUG,'nodePar:%s;node:%s'%(parNode,child),self)
            self.iSend+=1
            #  id=long(self.domImp.getKey(node))
            #  self.dId[id]=idAdd
            #self.dIdAdd[idAdd]=idAdd
            #  self.dIdAdd[idAdd]=id
            #self.semSend.release()
            if self.VERBOSE:
                print self.domImp.getKey(node)
                print child
        except:
            vtLog.vtLngTB(self.GetName())
        return 1
    def _addId(self,par,node,nodeImp=None):
        idAdd=long(self.doc.getKey(node))
        id=long(self.domImp.getKey(nodeImp))
        self.dId[id]=idAdd
        self.dIdAdd[idAdd]=id
        #self.dIdAddResp[idAdd]=id
    def __addImportNodes__(self,node):
        self.gcbbGenerate.Enable(False)
        #self.iCount=self.domImp.GetElementAttrCount()
        self.iCount=0
        self.domImp.procKeysRec(20,node,self.__cntNode__)
        self.iAct=0
        self.iSend=0
        self.iRecv=0
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'bNet:%d;count:%d'%(self.bNetResponse,self.iCount),self)
        self.thdImport.Post('proc',0,self.iCount<<1)
        #071015:wro
        #wx.MutexGuiEnter()
        #self.pbImp.SetValue(0)
        #self.pbImp.SetRange(self.iCount<<1)
        #wx.MutexGuiLeave()
        
        self.dId={}
        self.dIdAdd={}
        #self.domImp.procKeysRec(10,node,self.__addNode__) # 061212:wro
        self.domImp.procKeysRec(0,node,self.__addNode__,self.node)
        if self.thdImport.Is2Stop():
            self.__clearImport__()
            return -1
        zTimeOut=100  # time out 10s
        zTime=0.0
        if self.VERBOSE:
            print self.iSend,self.iRecv
            print self.iAct,self.iCount
        #while self.iSend!=self.iRecv:
        while self.iAct<self.iCount:
            if self.VERBOSE:
                print self.iSend,self.iRecv
                print self.iAct,self.iCount
            time.sleep(0.1)
            zTime+=0.1
            if self.thdImport.Is2Stop():
                self.__clearImport__()
                return -1
            if zTime>zTimeOut:
                vtLog.vtLngCurWX(vtLog.ERROR,'time out on adding nodes',self)
                break
        if self.VERBOSE:
            print self.iSend,self.iRecv
        if self.iSend==self.iRecv:# and 0:  # 061212:wro not not edit any more
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'check fid to change;dId:%s;dIdAdd:%s'%(vtLog.pformat(self.dId),vtLog.pformat(self.dIdAdd)),self)
            self.dEdit={}
            self.domImp.procKeysRec(10,node,self.__checkNode__)
            ##self.doc.procKeysRec(10,node,self.__checkNode__)
            if self.thdImport.Is2Stop():
                self.__clearImport__()
                return -1
            for id in self.dEdit.keys():
                try:
                    tmp=self.doc.getNodeByIdNum(id)
                    self.doc.startEdit(tmp)
                except:
                    pass
        self.__clearImport__()
    def __checkNode__(self,node):
        try:
            if self.thdImport.Is2Stop():
                return -1
            ##idNew=self.doc.getKeyNum(node)
            ##self.doc.procChilds(node,self.__checkFid__,idNew)
            ##return 1
            idAdd=self.dId[long(self.doc.getKey(node))]
            #idNew=self.dIdAdd[idAdd]
            idNew=idAdd
            if self.VERBOSE:            # 20150110 wro:verbose mode
                print 'checkNode'
                print node
                print idAdd,idNew
            tmp=self.doc.getNodeByIdNum(idNew)
            if self.VERBOSE:
                print tmp
            self.doc.procChilds(tmp,self.__checkFid__,idNew)
            vtLog.vtLngCurWX(vtLog.DEBUG,'edit ids:%s'%(vtLog.pformat(self.dEdit.keys())),self)
            
            #self.doc.procChilds(node,self.__checkFid__)
        except:
            vtLog.vtLngTB(self.GetName())
        return 1
    def __checkFid__(self,node,*args):
        try:
            if self.thdImport.Is2Stop():
                return -1
            if self.VERBOSE:
                print node
            #if self.domImp.IsKeyValid(self.domImp.getKey(node)):
            #    return 0
            #sFid=self.domImp.getAttribute(node,'fid')
            if self.doc.IsKeyValid(self.doc.getKey(node)):
                return 0
            sFid=self.doc.getAttribute(node,'fid')
            if len(sFid)>0:
                if VERBOSE>0:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'sFid:%s'%(sFid),self)
                if self.VERBOSE:
                    print sFid
                iFind=sFid.find('@')
                if iFind<0:
                    # ok maybe to change
                    try:
                        idAdd=self.dId[long(sFid)]
                        idNew=self.dIdAdd[idAdd]
                        if self.VERBOSE:
                            print idAdd,idNew
                        if VERBOSE>0:
                            vtLog.vtLngCurWX(vtLog.DEBUG,'sFid:%s;idAdd:%d;idNew:%d'%(sFid,
                                    idAdd,idNew),self)
                        #self.doc.setAttribute(node,'fid',self.doc.ConvertIdNum2Str(idNew))
                        self.doc.setAttribute(node,'fid',self.doc.ConvertIdNum2Str(idAdd))
                        if self.VERBOSE:            # 20150110 wro:verbose mode
                            print idAdd,self.doc.ConvertIdNum2Str(idAdd)
                            print args[0][0],node
                            print 
                        self.dEdit[args[0][0]]=True
                    except:
                        vtLog.vtLngTB(self.GetName())
            self.domImp.procChilds(node,self.__checkFid__,args[0][0])
        except:
            vtLog.vtLngTB(self.GetName())
        return 1
    def OnLock(self,evt):
        evt.Skip()
        try:
            id=long(evt.GetID())
            try:
                self.iAct+=1
                bTmp=self.dEdit[id]
                node=self.doc.getNodeByIdNum(id)
                self.doc.doEdit(node)
                self.doc.endEdit(node)
                del self.dEdit[id]
                self.pbImp.SetValue(self.iAct)
            except:
                pass
        except:
            vtLog.vtLngTB(self.GetName())
    def OnAddNodeResp(self,evt):
        evt.Skip()
        try:
            id=evt.GetID()
            idNew=evt.GetNewID()
            #while self.semSend.acquire(False)==False:
            #    time.sleep(0.1)
            vtLog.vtLngCurWX(vtLog.DEBUG,'id:%s idNew:%s'%(id,idNew),self)
            if self.dIdAdd.has_key(long(id)):
                zTime=0.0
                self.iRecv+=1
                idImp=self.dIdAdd[long(id)]
                #self.dIdAdd[long(id)]=long(idNew)
                #self.dIdAdd[long(id)]=long(idNew)
                #self.dIdAddResp[idImp]=long(idNew)
                self.iAct+=1
                #wx.MutexGuiEnter()
                self.thdImport.Post('proc',self.iAct,0)       #071016:wro self.pbImp.SetValue(self.iAct)
                #wx.MutexGuiLeave()
                #self.dIdAdd
                #self.dId[id]
                #print id,idNew
                self.dId[long(idImp)]=long(idNew)
                #print '  ',idImp
                node=self.domImp.getNodeByIdNum(idImp)
                nodeTarget=self.doc.getNodeById(idNew)
                self.domImp.procChildsKeysRecSkip(0,node,self.__addNode__,nodeTarget)
                #if self.iAct==self.iCount:
                    # last added
                #    vtLog.vtLngCurWX(vtLog.DEBUG,vtLog.pformat(self.dId),self)
            #self.semSend.release()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnGcbbGenerateButton(self, event):
        try:
            self.node2inst=self.viNode2Imp.GetSelectedNode()
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'node2imp:%s;node:%s'%(self.node2inst,self.node),self)
                vtLog.CallStack('')
                print self.node2inst
                print self.viNode2Imp.GetID()
            if self.impData is None:
                return
            
            ret=self.__checkPossible__(self.node2inst)
            if ret==1:
                self.node2instpar=self.domImp.getParent(self.node2inst)
                self.thdImport.Start()
                self.thdImport.Do(self.__addImportNodes__,self.node2inst)
                event.Skip()
                return
            elif ret==0:
                # adding not possible
                sMsg=_(u'Import at this position not allowed.')
            elif ret==-1:
                # no node to instance
                sMsg=_(u'No node to instance!')
            elif ret==-2:
                # no node selected
                sMsg=_(u'No node to import selected.')
            elif ret==-5:
                # error oocurred
                sMsg=_(u'A serious error has occurred.')
            dlg=wx.MessageDialog(self,sMsg ,
                                'vtXml Import Node Dialog',
                                wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()
    def OnProcImport(self,evt):
        self.pbImp.SetValue(evt.GetAct())    #071015:wro
        iCount=evt.GetCount()
        if iCount>0:
            self.pbImp.SetRange(iCount)
    def _doReleaseWidLock(self):
        self.txtDate.SetValue("")
        self.txtTime.SetValue("")
        self.gcbbGenerate.Enable(False)
        self.fbbFN.Enable(True)
    def _clrImportGenWidgetInfo(self):
        self.gcbbGenerate.Enable(False)
        self.fbbFN.Enable(True)
    def _setImportWidgetInfo(self,sDt,sTm,node2inst):
        try:
                self.txtDate.SetValue(sDt)
                self.txtTime.SetValue(sTm)
                self.viNode2Imp.SetDoc(self.domImp,False)
                #self.viNode2Imp.SetNodeTree(self.impData)   # 061210:wro
                #self.viNode2Imp.SetNode(self.impData)
                self.node2inst=node2inst
                if node2inst is not None:
                    #self.node2inst=childs[0]
                    self.viNode2Imp.SetNodeTree(self.node2inst)
                    #self.viNode2Imp.SetNode(self.node2inst)
                    fid=self.domImp.getKey(self.node2inst)
                    #print self.domImp.getNodeByIdNum(long(fid))
                    sTag=self.domImp.getNodeText(self.node2inst,'tag')
                    self.viNode2Imp.SetValueID(sTag,long(fid))
                #    self.vgpImpTree.SelectByID(self.domImp.getAttribute(self.node2inst,'id'))
                #self.txtEditor.SaveFile(filename)
                self.gcbbGenerate.Enable(True)
                self.fbbFN.Enable(True)
        except:
            self._clrImportGenWidgetInfo()
            vtLog.vtLngTB(self.GetName())
    def __openFile__(self,fn):
        try:
                self.fn=fn
                docClass=None
                tBases=self.doc.__class__.__bases__
                if len(tBases)>0:
                    for cls in tBases:
                        if issubclass(cls,vtXmlDom.vtXmlDom):
                            docClass=cls.__name__
                            docMod=cls.__module__
                            break
                if docClass is  None:
                    docClass=self.doc.__class__.__name__
                    docMod=self.doc.__class__.__module__
                vtLog.PrintMsg(_(u'open import file %s ...')%(fn),self)
                try:
                    self.domImp=eval('%s.%s(audit_trail=False)'%(docMod,docClass))#vtXmlDom.vtXmlDom()
                except:
                    vtLog.vtLngCurWX(vtLog.WARN,'kw audit_trail not available'%(),self)
                    self.domImp=eval('%s.%s()'%(docMod,docClass))#vtXmlDom.vtXmlDom()
                self.domImp.Open(fn)
                vtLog.PrintMsg(_(u'opened import file %s.')%(fn),self)
                # fill out import data infos
                self.impNode=self.domImp.getChild(self.domImp.getRoot(),'export')
                createNode=self.domImp.getChild(self.impNode,'create')
        except:
            vtLog.vtLngTB(self.GetName())
        try:
            if self.impNode is None:
                self.thdImport.CallBack(self._doReleaseWidLock)
                return
            self.impData=self.domImp.getChild(self.impNode,'data')
            self.domImp.genIds(self.impData)
            #self.impData=self.domImp.getChild(self.impData)
            #print self.impData
            self.domImp.removeInternalNodes(self.impData)
            #if self.VERBOSE:
            #    vtLog.vtLngCurWX(vtLog.DEBUG,self.impData,self)
            #    vtLog.CallStack('')
            #    print self.impData
        except:
            vtLog.vtLngTB(self.GetName())
        try:
            childs=self.domImp.getChilds(self.impData)
            node2inst=None
            if len(childs)>0:
                node2inst=childs[0]
            self.thdImport.CallBack(self._setImportWidgetInfo,
                    self.domImp.getNodeText(createNode,'date'),
                    self.domImp.getNodeText(createNode,'time'),
                    node2inst)
        except:
            vtLog.vtLngTB(self.GetName())
            self.thdImport.CallBack(self._clrImportGenWidgetInfo)
        
    def OnGcbbBrowseButton(self, event):
        #vtLog.CallStack('')
        try:
                fn=self.fbbFN.GetValue()
                try:
                    self.sImpDN,s=os.path.split(fn)
                except:
                    self.sImpDN='.'
                st=os.stat(fn)
                if stat.S_ISREG(st[stat.ST_MODE]):
                    if self.thdOpenFile.IsRunning():
                        vtLog.vtLngCurWX(vtLog.ERROR,'thread open still running',self)
                        return
                    self.fbbFN.Enable(False)
                    self.domImp=None
                    self.impNode=None
                    self.impData=None
                    self.node2inst=None
                    self.gcbbGenerate.Enable(False)
                    self.thdOpenFile.Do(self.__openFile__,fn)
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()

    def OnTrImpVtxmltreeItemSelected(self, event):
        event.Skip()

    def OnTrImpVtxmltreeThreadAddElementsAborted(self, event):
        event.Skip()

    def OnTrImpVtxmltreeThreadAddElementsFinished(self, event):
        event.Skip()

    def OnTrImpVtxmltreeThreadAddElements(self, event):
        event.Skip()

    def OnImpTreeItemSel(self,event):
        if event.GetTreeNodeData() is not None:
            self.node2inst=event.GetTreeNodeData()
    def OnTreeItemSel(self,event):
        if event.GetTreeNodeData() is not None:
            self.selNode=event.GetTreeNodeData()
    def SetLanguage(self,lang):
        self.vgpImpTree.SetLanguage(lang)
        self.vgpTree.SetLanguage(lang)
    def SetMainTree(self,vgpTree):
        self.vgpMainTree=vgpTree
    
