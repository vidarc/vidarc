#----------------------------------------------------------------------------
# Name:         vtXmlNodeFilter.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060419
# CVS-ID:       $Id: vtXmlNodeFilter.py,v 1.4 2008/02/02 13:44:25 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlNodeBase import *
from vidarc.tool.xml.vtXmlFilterType import *
import vidarc.tool.lang.vtLgBase as vtLgBase

class vtXmlNodeFilter(vtXmlNodeBase):
    NODE_ATTRS=[
        ]
    FUNCS_GET_SET_4_LST=['Filters']
    def __init__(self,tagName='filters',filter={}):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        vtXmlNodeBase.__init__(self,tagName)
        self.fltMapper=vtXmlFilterMapper()
        self.dFlt=filter
    def GetDescription(self):
        return _(u'filters')
    def Is2Create(self):
        return False
    def Is2Add(self):
        return False
    def IsMultiple(self):
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return False
    def IsId2Add(self):
        return False
    def HasGraphics(self):
        return False
    def getImageData(self):
        return None
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        return None
    def GetAddDialogClass(self):
        return None
    def GetImage(self):
        return None
    def GetSelImage(self):
        return None
    def GetBitmap(self):
        return None
    def GetSelBitmap(self):
        return None
    # specific
    def GetFilters(self,node):
        lFlt=[]
        try:
            for c in self.doc.getChilds(node,'filter'):
                sName=self.doc.getAttribute(c,'node')
                try:
                    if len(sName)==0:
                        sName=None
                    l=self.dFlt[sName]
                    sAttr,flt=self.fltMapper.SetNode(l,self.doc,c)
                    if flt is not None:
                        lFlt.append((sName,sAttr,flt))
                except:
                    pass
        except:
            vtLog.vtLngTB('')
        return lFlt
    def SetFilters(self,node,l):
        for c in self.doc.getChilds(node,'filter'):
            self.doc.deleteNode(c,node)
        for sName,sAttr,flt in l:
            if sName is None:
                s=''
            else:
                s=sName
            c=self.doc.createSubNodeAttr(node,'filter','node',s)
            self.doc.setNodeText(c,'tag',sAttr)
            flt.GetNode(self.doc,c)
    
