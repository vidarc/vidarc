#Boa:FramePanel:vtXmlNodeLogSimplePanel
#----------------------------------------------------------------------------
# Name:         vtXmlNodeLogPanel.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vtXmlNodeLogSimplePanel.py,v 1.7 2008/03/16 22:20:22 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons
import  wx.lib.anchors as anchors
from vidarc.tool.net.vNetXmlWxGui import *
import vidarc.tool.xml.vtXmlDom as vtXmlDom
#from vidarc.vApps.vPrjEng.evtDataChanged import *
import vidarc.tool.xml.vtXmlDomConsumer as vtXmlDomConsumer
from vidarc.tool.time.vtTime import vtDateTime
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

import sys

[wxID_VTXMLNODELOGSIMPLEPANEL, wxID_VTXMLNODELOGSIMPLEPANELCBADD, 
 wxID_VTXMLNODELOGSIMPLEPANELCBSET, wxID_VTXMLNODELOGSIMPLEPANELLBLLOG, 
 wxID_VTXMLNODELOGSIMPLEPANELLBLTITLE, wxID_VTXMLNODELOGSIMPLEPANELLBLUSR, 
 wxID_VTXMLNODELOGSIMPLEPANELLSTLOG, wxID_VTXMLNODELOGSIMPLEPANELTXTLOGDESC, 
 wxID_VTXMLNODELOGSIMPLEPANELTXTLOGNAME, wxID_VTXMLNODELOGSIMPLEPANELTXTUSR, 
] = [wx.NewId() for _init_ctrls in range(10)]

class vtXmlNodeLogSimplePanel(wx.Panel,vtXmlDomConsumer.vtXmlDomConsumer):
    def _init_coll_bxsLbl_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblLog, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblTitle, 1, border=0, flag=wx.EXPAND)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbAdd, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbSet, 0, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.EXPAND)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsBt, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblUsr, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtUsr, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsLbl, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtLogName, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lstLog, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtLogDesc, 1, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(3)
        parent.AddGrowableCol(0)
        parent.AddGrowableCol(1)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=2, hgap=0, rows=4, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsLbl = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsLbl_Items(self.bxsLbl)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTXMLNODELOGSIMPLEPANEL,
              name=u'vtXmlNodeLogSimplePanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(312, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 180))
        self.SetAutoLayout(True)

        self.lstLog = wx.ListView(id=wxID_VTXMLNODELOGSIMPLEPANELLSTLOG,
              name=u'lstLog', parent=self, pos=wx.Point(0, 72),
              size=wx.Size(120, 108), style=wx.LC_REPORT)
        self.lstLog.SetMinSize(wx.Size(-1, -1))
        self.lstLog.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstLogListItemSelected,
              id=wxID_VTXMLNODELOGSIMPLEPANELLSTLOG)
        self.lstLog.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstLogListColClick,
              id=wxID_VTXMLNODELOGSIMPLEPANELLSTLOG)

        self.txtUsr = wx.TextCtrl(id=wxID_VTXMLNODELOGSIMPLEPANELTXTUSR,
              name=u'txtUsr', parent=self, pos=wx.Point(124, 30),
              size=wx.Size(180, 21), style=0, value=u'')
        self.txtUsr.SetMinSize(wx.Size(-1, -1))
        self.txtUsr.Bind(wx.EVT_TEXT, self.OnTxtUsrText,
              id=wxID_VTXMLNODELOGSIMPLEPANELTXTUSR)

        self.txtLogName = wx.TextCtrl(id=wxID_VTXMLNODELOGSIMPLEPANELTXTLOGNAME,
              name=u'txtLogName', parent=self, pos=wx.Point(124, 51),
              size=wx.Size(180, 21), style=0, value=u'')
        self.txtLogName.SetMinSize(wx.Size(-1, -1))
        self.txtLogName.Bind(wx.EVT_TEXT, self.OnTxtLogNameText,
              id=wxID_VTXMLNODELOGSIMPLEPANELTXTLOGNAME)

        self.txtLogDesc = wx.TextCtrl(id=wxID_VTXMLNODELOGSIMPLEPANELTXTLOGDESC,
              name=u'txtLogDesc', parent=self, pos=wx.Point(124, 72),
              size=wx.Size(180, 108), style=wx.TE_MULTILINE, value=u'')
        self.txtLogDesc.SetMinSize(wx.Size(-1, -1))
        self.txtLogDesc.Bind(wx.EVT_TEXT, self.OnTxtLogDescText,
              id=wxID_VTXMLNODELOGSIMPLEPANELTXTLOGDESC)

        self.cbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLNODELOGSIMPLEPANELCBADD,
              bitmap=vtArt.getBitmap(vtArt.Add), label=_(u'Add'), name=u'cbAdd',
              parent=self, pos=wx.Point(124, 0), size=wx.Size(76, 30), style=0)
        self.cbAdd.SetMinSize(wx.Size(-1, -1))
        self.cbAdd.Bind(wx.EVT_BUTTON, self.OnCbAddButton,
              id=wxID_VTXMLNODELOGSIMPLEPANELCBADD)

        self.cbSet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLNODELOGSIMPLEPANELCBSET,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Set'),
              name=u'cbSet', parent=self, pos=wx.Point(204, 0), size=wx.Size(76,
              30), style=0)
        self.cbSet.SetMinSize(wx.Size(-1, -1))
        self.cbSet.Bind(wx.EVT_BUTTON, self.OnCbSetButton,
              id=wxID_VTXMLNODELOGSIMPLEPANELCBSET)

        self.lblUsr = wx.StaticText(id=wxID_VTXMLNODELOGSIMPLEPANELLBLUSR,
              label=_(u'User'), name=u'lblUsr', parent=self, pos=wx.Point(0,
              30), size=wx.Size(120, 21), style=wx.ALIGN_RIGHT)
        self.lblUsr.SetMinSize(wx.Size(-1, -1))

        self.lblTitle = wx.StaticText(id=wxID_VTXMLNODELOGSIMPLEPANELLBLTITLE,
              label=_(u'Subject'), name=u'lblTitle', parent=self, pos=wx.Point(60,
              51), size=wx.Size(60, 21), style=wx.ALIGN_RIGHT)
        self.lblTitle.SetMinSize(wx.Size(-1, -1))

        self.lblLog = wx.StaticText(id=wxID_VTXMLNODELOGSIMPLEPANELLBLLOG,
              label=_(u'Logs'), name=u'lblLog', parent=self, pos=wx.Point(0,
              51), size=wx.Size(60, 21), style=0)
        self.lblLog.SetMinSize(wx.Size(-1, -1))

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self._init_ctrls(parent)
        self.selIdx=-1
        self.dt=vtDateTime(bLocal=True)
        
        vtXmlDomConsumer.vtXmlDomConsumer.__init__(self)
        
        #self.viHum = vidarc.vApps.vHum.vXmlHumInputTree.vXmlHumInputTree(id=-1,
        #      name=u'vitrHum', parent=self, pos=wx.Point(64, 0),
        #      size=wx.Size(133, 30), size_button=wx.Size(31, 30),
        #      size_text=wx.Size(100, 22), style=0)
        #self.vitrHum.Bind(vidarc.tool.input.vtInputTree.vEVT_VTINPUT_TREE_CHANGED,
        #      self.OnVitrPrjVtinputTreeChanged)
        #self.gbsLog.AddWindow(self.vitrHum, (1, 2), border=0, flag=wx.SHRINK | wx.EXPAND, span=(1, 2))
        
        #self.gbsLog.AddGrowableRow(3)
        #self.gbsLog.AddGrowableCol(0)
        #self.gbsLog.AddGrowableCol(2)
        #self.gbsLog.Layout()
        
        self.bSortAsc=False
        self.iSortCol=0
        
        self.lstLog.InsertColumn(0,_(u'Date'),wx.LIST_FORMAT_LEFT,120)
        self.lstLog.InsertColumn(1,_(u'Name'),wx.LIST_FORMAT_LEFT,130)
        self.lstLog.InsertColumn(2,_(u'User'),wx.LIST_FORMAT_LEFT,70)
        
        self.bModified=False
        self.bAutoApply=False
        self.bBlock=False
        self.objRegNode=None
        
        self.lstMarkObjs=[self.txtUsr,self.txtLogName,self.txtLogDesc]
        
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
    def SetNetDocs(self,d):
        pass
    def SetRegNode(self,obj):
        self.objRegNode=obj
    def Lock(self,flag):
        if flag:
            self.lstLog.Enable(False)
            self.txtUsr.Enable(False)
            self.txtLogName.Enable(False)
            self.txtLogDesc.Enable(False)
            self.cbAdd.Enable(False)
            self.cbSet.Enable(False)
        else:
            self.lstLog.Enable(True)
            self.txtUsr.Enable(True)
            self.txtLogName.Enable(True)
            self.txtLogDesc.Enable(True)
            self.cbAdd.Enable(True)
            self.cbSet.Enable(True)
    def SetAutoApply(self,flag):
        self.bAutoApply=flag
    def __clearBlock__(self):
        self.bBlock=False
    def SetModified(self,state,obj=None):
        if state:
            if obj is None:
                for obj in self.lstMarkObjs:
                    f=obj.GetFont()
                    f.SetWeight(wx.FONTWEIGHT_BOLD)
                    obj.SetFont(f)
                    obj.Refresh()
            else:
                f=obj.GetFont()
                f.SetWeight(wx.FONTWEIGHT_BOLD)
                obj.SetFont(f)
                obj.Refresh()
        else:
            if obj is None:
                for obj in self.lstMarkObjs:
                    f=obj.GetFont()
                    f.SetWeight(wx.FONTWEIGHT_NORMAL)
                    obj.SetFont(f)
                    obj.Refresh()
            else:
                f=obj.GetFont()
                f.SetWeight(wx.FONTWEIGHT_NORMAL)
                obj.SetFont(f)
                obj.Refresh()
        self.bModified=state    
    def GetModified(self):
        return self.bModified
    def __isModified__(self):
        if self.bModified:
            return True
        return False
    def Clear(self):
        self.bBlock=True
        self.SetModified(False)
        vtXmlDomConsumer.vtXmlDomConsumer.Clear(self)
        self.txtUsr.SetValue('')
        self.txtLogName.SetValue('')
        self.txtLogDesc.SetValue('')
        self.__unlock__()
        self.selIdx=-1
        self.lstLog.DeleteAllItems()
        wx.CallAfter(self.__clearBlock__)
    def SetDoc(self,doc,bNet=False):
        vtXmlDomConsumer.vtXmlDomConsumer.SetDoc(self,doc)
    def SetNode(self,node):
        try:
            self.__unlock__()
            self.selIdx=-1
            self.bBlock=True
            self.SetModified(False)
            self.logs={}
            vtXmlDomConsumer.vtXmlDomConsumer.SetNode(self,node)
            self.txtUsr.SetValue('')
            self.txtLogName.SetValue('')
            self.txtLogDesc.SetValue('')
            self.lstLog.DeleteAllItems()
            self.logs={}
            if self.doc is None:
                return
            childs=self.doc.getChilds(self.node,'log')
            for c in childs:
                self.__addInfo__(c)
        except:
            vtLog.vtLngTB(self.GetName())
        wx.CallAfter(self.__clearBlock__)
        
    def GetNode(self):
        self.SetModified(False)
        pass
    def Apply(self,bDoApply=False):
        #self.SetModified(False)
        return False
    def Cancel(self):
        self.SetModified(False)
    def OnCbAddButton(self, event):
        try:
            sUsr=self.txtUsr.GetValue()
            self.dt.Now()
            
            n=self.doc.createChildByLst(self.node,'log',[
                    {'tag':'datetime','val':self.dt.GetDateTimeStr()},
                    {'tag':'usr','val':sUsr},
                    {'tag':'name','val':self.txtLogName.GetValue()},
                    {'tag':'desc','val':self.txtLogDesc.GetValue()},]
                )
            #self.doc.addNode(self.node,n)
            if self.bSortAsc:
                idx=self.lstLog.GetItemCount()
            else:
                idx=0
            self.__unlock__()
            self.lstLog.SetItemState(idx,0,wx.LIST_STATE_SELECTED)
            self.__addInfo__(n)
            if self.bSortAsc:
                idx=self.lstLog.GetItemCount()
            else:
                idx=0
            self.selIdx=-1
            self.txtUsr.SetValue('')
            self.txtLogName.SetValue('')
            self.txtLogDesc.SetValue('')
            self.SetModified(False)
            
            #wx.CallAfter(self.lstLog.SetItemState,idx,wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
            self.doc.AlignNode(n,iRec=2)
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()
    def __addInfo__(self,node):
        sDateTime=self.doc.getNodeText(node,'datetime')
        sName=self.doc.getNodeText(node,'name')
        sUsr=self.doc.getNodeText(node,'usr')
            
        if self.bSortAsc:
            idx=sys.maxint
        else:
            idx=0
        self.dt.SetStr(sDateTime)
        sDateTime=self.dt.GetDateTimeStr(' ')
        self.logs[sDateTime+' '+sName]=node
        index = self.lstLog.InsertImageStringItem(idx, sDateTime, -1)
        self.lstLog.SetStringItem(index,1,sName,-1)
        self.lstLog.SetStringItem(index,2,sUsr,-1)
    def OnCbSetButton(self, event):
        try:
            sUsr=self.txtUsr.GetValue()
            dt=wx.DateTime.Now()
            sName=self.txtLogName.GetValue()
            sDesc=self.txtLogDesc.GetValue()
            
            #if self.chcUsr.GetSelection()<1:
            #    return
            #sUsr=self.chcUsr.GetStringSelection()
            it=self.lstLog.GetItem(self.selIdx,0)
            sDate=it.m_text
            it=self.lstLog.GetItem(self.selIdx,1)
            k=sDate+' '+it.m_text
            n=self.logs[k]
            self.logs[k]=None
            
            k=sDate+' '+sName
            
            self.doc.setNodeText(n,'name',self.txtLogName.GetValue())
            self.doc.setNodeText(n,'desc',self.txtLogDesc.GetValue())
            self.doc.setNodeText(n,'usr',sUsr)
            
            self.logs[k]=n
            
            self.lstLog.SetStringItem(self.selIdx,1,sName,-1)
            self.lstLog.SetStringItem(self.selIdx,2,sUsr,-1)
            #FIXME: do network update
            #self.doc.setNode(n)
            #self.doc.doEdit(n)
            self.SetModified(False)
            #wx.PostEvent(self,vgpRevisionChanged(self,self.node))
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()
    def __unlock__(self):
        try:
            if self.selIdx!=-1:
                it=self.lstLog.GetItem(self.selIdx,0)
                sDate=it.m_text
                it=self.lstLog.GetItem(self.selIdx,1)
                k=sDate+' '+it.m_text
                #self.doc.endEdit(self.logs[k])
        except:
            vtLog.vtLngTB(self.GetName())
            
    def OnLstLogListItemSelected(self, event):
        try:
            self.bBlock=True
            self.SetModified(False)
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            self.__unlock__()
            idx=event.GetIndex()
            it=self.lstLog.GetItem(idx,0)
            sDate=it.m_text
            it=self.lstLog.GetItem(idx,1)
            self.txtLogName.SetValue(it.m_text)
            k=sDate+' '+it.m_text
            self.txtLogName.SetValue(it.m_text)
            try:
                self.txtLogDesc.SetValue(self.doc.getNodeText(self.logs[k],'desc'))
            except:
                self.txtLogDesc.SetValue('')
            self.txtUsr.SetValue(self.doc.getNodeText(self.logs[k],'usr'))
            #id=self.doc.getKey(self.logs[k])
            #self.doc.startEdit(self.logs[k])
            self.selIdx=idx
        except:
            vtLog.vtLngTB(self.GetName())
        wx.CallAfter(self.__clearBlock__)
        event.Skip()

    def OnLstLogListColClick(self, event):
        self.__unlock__()
        self.selIdx=-1
        if event.GetColumn()==0:
            self.bSortAsc=not self.bSortAsc
        childs=self.doc.getChilds(self.node,self.name)
        self.lstLog.DeleteAllItems()
        self.logs={}
        for c in childs:
            self.__addInfo__(c)
        
        event.Skip()

    def OnTxtLogNameText(self, event):
        if self.bBlock==False:
            self.SetModified(True,self.txtLogName)
        event.Skip()

    def OnTxtLogDescText(self, event):
        if self.bBlock==False:
            self.SetModified(True,self.txtLogDesc)
        event.Skip()

    def OnTxtUsrText(self, event):
        if self.bBlock==False:
            self.SetModified(True,self.txtUsr)
        event.Skip()
        
