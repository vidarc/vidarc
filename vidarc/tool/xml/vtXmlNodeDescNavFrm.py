#----------------------------------------------------------------------
# Name:         vtXmlNodeDescNavFrm.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120527
# CVS-ID:       $Id: vtXmlNodeDescNavFrm.py,v 1.6 2015/04/27 06:42:45 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    from vGuiFrmWX import vGuiFrmWX
    from vidarc.tool.xml.vtXmlNodeDescNavPanel import vtXmlNodeDescNavPanel
    
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vtXmlNodeDescNavFrm(vGuiFrmWX):
    def __init__(self, *args,**kwargs):
        kwargs['lWid']=vtXmlNodeDescNavPanel
        self.sTitle=kwargs.get('title','')
        vGuiFrmWX.__init__(self,*args,**kwargs)
    def GetWidTag(self):
        return self.pn.pnData.pnTag
    def GetWidDescTree(self):
        return self.pn.pnData.trDesc
    def GetWidDescPanel(self):
        return self.pn.pnData.pnDesc
    def OnCbCloseButton(self, event):
        vGuiFrmWX.OnCbCloseButton(self, event)
        # FIXME:20150426evt
        #self.Close()
        #self.frm.Destroy()
        #self.OnFrameMainClose(evt)
    def OnFrameClose(self,evt):
        self.OnFrameMainClose(evt)
    def __makeTitle__(self):
        pass
    def SetDocNode(self,doc,node,sID):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            self.__logDebug__('sID:%r'%(sID))
            #nBase=doc.GetBaseNode(node)
            #nRel=doc.GetRelBaseNode(node)
            #sT=doc.GetTagNamesWithBase(node,nBase,nBase)
            sT=doc.GetTagNamesRaw(node)
            s=''.join([self.sTitle,sT,')'])
            self.frm.SetTitle(s)
        except:
            self.__logTB__()
    def IsShutDownActive(self):
        try:
            self.__logDebug__('')
            pn=self.GetPanel()
            return pn.IsShutDownActive()
            return False
        except:
            self.__logTB__()
    def IsShutDownFinished(self):
        try:
            self.__logDebug__('')
            pn=self.GetPanel()
            return pn.IsShutDownFinished()
            return True
        except:
            self.__logTB__()
    def ShutDown(self):
        try:
            self.__logDebug__('')
            pn=self.GetPanel()
            pn.ShutDown()
        except:
            self.__logTB__()
    def DoShutDown(self):
        try:
            if self.IsMainThread()==False:
                return
            self.__logDebug__('')
            self.Close()
            self.frm.Destroy()
        except:
            self.__logTB__()
    def CloseOld(self):
        try:
            self.__logDebug__('')
            self.frm.Close()
            self.ShutDown()
            #self.frm.Destroy()
        except:
            self.__logTB__()
