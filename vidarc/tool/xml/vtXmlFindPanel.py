#Boa:FramePanel:vtXmlFindPanel
#----------------------------------------------------------------------------
# Name:         vtXmlFindPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20050205
# CVS-ID:       $Id: vtXmlFindPanel.py,v 1.10 2008/03/26 23:12:28 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.art.vtThrobber
import vidarc.tool.xml.vtXmlFilterTypeInput
from vidarc.tool.xml.vtXmlFilterType import *
from vidarc.tool.xml.vtXmlFindThread import *
import wx.lib.buttons

import sys

from vidarc.tool.xml.vtXmlDomConsumer import *
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_VTXMLFINDPANEL, wxID_VTXMLFINDPANELCBADDFLT, 
 wxID_VTXMLFINDPANELCBCANCEL, wxID_VTXMLFINDPANELCBDELFLT, 
 wxID_VTXMLFINDPANELCBFIND, wxID_VTXMLFINDPANELCBFINDNEXT, 
 wxID_VTXMLFINDPANELCBSTOP, wxID_VTXMLFINDPANELCHCFILTERATTR, 
 wxID_VTXMLFINDPANELCHCFILTERPOS, wxID_VTXMLFINDPANELLBBFOUND, 
 wxID_VTXMLFINDPANELLBLFILTERATTR, wxID_VTXMLFINDPANELLBLFILTERATTRDESC, 
 wxID_VTXMLFINDPANELLBLFILTERPOS, wxID_VTXMLFINDPANELLBLNODENAME, 
 wxID_VTXMLFINDPANELLSTFILTER, wxID_VTXMLFINDPANELTHRPROC, 
 wxID_VTXMLFINDPANELVIFLTDATE, wxID_VTXMLFINDPANELVIFLTDATETIME, 
 wxID_VTXMLFINDPANELVIFLTFLOAT, wxID_VTXMLFINDPANELVIFLTINT, 
 wxID_VTXMLFINDPANELVIFLTLONG, wxID_VTXMLFINDPANELVIFLTSTR, 
 wxID_VTXMLFINDPANELVIFLTTIME, wxID_VTXMLFINDPANELVIFLTDATETIMEREL,
] = [wx.NewId() for _init_ctrls in range(24)]

wxEVT_VTXML_FIND_PANEL_FOUND=wx.NewEventType()
vEVT_VTXML_FIND_PANEL_FOUND=wx.PyEventBinder(wxEVT_VTXML_FIND_PANEL_FOUND,1)
def EVT_VTXML_FIND_PANEL_FOUND(win,func):
    win.Connect(-1,-1,wxEVT_VTXML_FIND_PANEL_FOUND,func)
class vtXmlFindPanelFound(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTXML_FIND_PANEL_FOUND(<widget_name>, self.OnInfoApplied)
    """

    def __init__(self,obj,node):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VTXML_FIND_PANEL_FOUND)
        self.obj=obj
        self.node=node
    def GetObject(self):
        return self.obj
    def GetNode(self):
        return self.node

wxEVT_VTXML_FIND_PANEL_CANCELED=wx.NewEventType()
vEVT_VTXML_FIND_PANEL_CANCELED=wx.PyEventBinder(wxEVT_VTXML_FIND_PANEL_CANCELED,1)
def EVT_VTXML_FIND_PANEL_CANCELED(win,func):
    win.Connect(-1,-1,wxEVT_VTXML_FIND_PANEL_CANCELED,func)
class vtXmlFindPanelCanceled(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTXML_FIND_PANEL_CANCELED(<widget_name>, self.OnInfoCanceled)
    """

    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VTXML_FIND_PANEL_CANCELED)
        self.obj=obj
    def GetObject(self):
        return self.obj

class vtXmlFindPanel(wx.Panel,vtXmlDomConsumer):
    def _init_coll_bxsFilterAttr_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblFilterAttr, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.chcFilterAttr, 2, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.lblFilterAttrDesc, 2, border=4,
              flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbCancel, 1, border=8, flag=wx.RIGHT | wx.LEFT)

    def _init_coll_bxsFilterInput_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viFltStr, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltInt, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltLong, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltFloat, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltDate, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltTime, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltDateTime, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltDateTimeRel, 1, border=0, flag=wx.EXPAND)

    def _init_coll_bxsFilterBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbAddFlt, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.cbDelFlt, 0, border=4, flag=wx.TOP | wx.LEFT)

    def _init_coll_fgsDom_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsFilterPos, 1, border=8,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddSizer(self.bxsFilterAttr, 1, border=8,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsFilterInput, 1, border=4,
              flag=wx.BOTTOM | wx.EXPAND)
        parent.AddSizer(self.bxsFilterAct, 0, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsProc, 1, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER)

    def _init_coll_bxsFilterPos_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblFilterPos, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.chcFilterPos, 2, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.lblNodeName, 2, border=4,
              flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fgsDom_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(4)
        parent.AddGrowableCol(0)

    def _init_coll_bxsProc_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.thrProc, 0, border=0, flag=wx.ALIGN_CENTER)
        parent.AddSpacer(wx.Size(16, 8), border=0, flag=0)
        parent.AddWindow(self.lbbFound, 0, border=8,
              flag=wx.ALIGN_CENTER | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.cbFind, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbFindNext, 1, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbStop, 0, border=0, flag=wx.ALIGN_RIGHT)

    def _init_coll_bxsFilterAct_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstFilter, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsFilterBt, 0, border=0, flag=0)

    def _init_coll_lstFilter_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'tagname'), width=-1)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'attribute'), width=-1)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'value'), width=-1)
        parent.InsertColumn(col=3, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'operator'), width=-1)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsDom = wx.FlexGridSizer(cols=1, hgap=0, rows=9, vgap=0)

        self.bxsFilterPos = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsFilterAct = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsFilterBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsProc = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsFilterInput = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsFilterAttr = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsDom_Items(self.fgsDom)
        self._init_coll_fgsDom_Growables(self.fgsDom)
        self._init_coll_bxsFilterPos_Items(self.bxsFilterPos)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsFilterAct_Items(self.bxsFilterAct)
        self._init_coll_bxsFilterBt_Items(self.bxsFilterBt)
        self._init_coll_bxsProc_Items(self.bxsProc)
        self._init_coll_bxsFilterInput_Items(self.bxsFilterInput)
        self._init_coll_bxsFilterAttr_Items(self.bxsFilterAttr)

        self.SetSizer(self.fgsDom)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTXMLFINDPANEL, name=u'vtXmlFindPanel',
              parent=prnt, pos=wx.Point(472, 348), size=wx.Size(246, 259),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(238, 232))

        self.lblFilterPos = wx.StaticText(id=wxID_VTXMLFINDPANELLBLFILTERPOS,
              label=_(u'tagname'), name=u'lblFilterPos', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(42, 21), style=wx.ALIGN_RIGHT)
        self.lblFilterPos.SetMinSize(wx.Size(-1, -1))

        self.chcFilterPos = wx.Choice(choices=[],
              id=wxID_VTXMLFINDPANELCHCFILTERPOS, name=u'chcFilterPos',
              parent=self, pos=wx.Point(46, 0), size=wx.Size(88, 21),
              style=wx.CB_SORT)
        self.chcFilterPos.SetMinSize(wx.Size(-1, -1))
        self.chcFilterPos.Bind(wx.EVT_CHOICE, self.OnChcFilterPosChoice,
              id=wxID_VTXMLFINDPANELCHCFILTERPOS)

        self.chcFilterAttr = wx.ComboBox(choices=[],
              id=wxID_VTXMLFINDPANELCHCFILTERATTR, name=u'chcFilterAttr',
              parent=self, pos=wx.Point(46, 21), size=wx.Size(88, 21), style=0)
        self.chcFilterAttr.SetMinSize(wx.Size(-1, -1))
        self.chcFilterAttr.Bind(wx.EVT_TEXT_ENTER, self.OnChcFilterAttrChoiceEnter,
              id=wxID_VTXMLFINDPANELCHCFILTERATTR)
        self.chcFilterAttr.Bind(wx.EVT_COMBOBOX, self.OnChcFilterAttrChoice,
              id=wxID_VTXMLFINDPANELCHCFILTERATTR)

        self.viFltStr = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterStringInput(id=wxID_VTXMLFINDPANELVIFLTSTR,
              name=u'viFltStr', parent=self, pos=wx.Point(0, 50),
              size=wx.Size(34, 26), style=0)
        self.viFltStr.Enable(False)

        self.viFltInt = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterIntegerInput(id=wxID_VTXMLFINDPANELVIFLTINT,
              name=u'viFltInt', parent=self, pos=wx.Point(34, 50),
              size=wx.Size(34, 26), style=0)
        self.viFltInt.Show(False)

        self.viFltLong = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterLongInput(id=wxID_VTXMLFINDPANELVIFLTLONG,
              name=u'viFltLong', parent=self, pos=wx.Point(68, 50),
              size=wx.Size(34, 26), style=0)
        self.viFltLong.Show(False)

        self.viFltFloat = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterFloatInput(id=wxID_VTXMLFINDPANELVIFLTFLOAT,
              name=u'viFltFloat', parent=self, pos=wx.Point(102, 50),
              size=wx.Size(34, 26), style=0)
        self.viFltFloat.Show(False)

        self.viFltDate = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterDateInput(id=wxID_VTXMLFINDPANELVIFLTDATE,
              name=u'viFltDate', parent=self, pos=wx.Point(136, 50),
              size=wx.Size(34, 26), style=0)
        self.viFltDate.Show(False)

        self.viFltTime = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterTimeInput(id=wxID_VTXMLFINDPANELVIFLTTIME,
              name=u'viFltTime', parent=self, pos=wx.Point(170, 50),
              size=wx.Size(34, 26), style=0)
        self.viFltTime.Show(False)
        self.viFltTime.Show(False)

        self.viFltDateTime = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterDateTimeInput(id=wxID_VTXMLFINDPANELVIFLTDATETIME,
              name=u'viFltDateTime', parent=self, pos=wx.Point(204, 50),
              size=wx.Size(34, 26), style=0)

        self.viFltDateTimeRel = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterDateTimeRelInput(id=wxID_VTXMLFINDPANELVIFLTDATETIMEREL,
              name=u'viFltDateTimeRel', parent=self, pos=wx.Point(238, 50),
              size=wx.Size(34, 26), style=0)
        
        self.cbAddFlt = wx.lib.buttons.GenBitmapButton(ID=wxID_VTXMLFINDPANELCBADDFLT,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbAddFlt', parent=self,
              pos=wx.Point(207, 80), size=wx.Size(31, 30), style=0)
        self.cbAddFlt.Enable(False)
        self.cbAddFlt.Bind(wx.EVT_BUTTON, self.OnCbAddFltButton,
              id=wxID_VTXMLFINDPANELCBADDFLT)

        self.lstFilter = wx.ListCtrl(id=wxID_VTXMLFINDPANELLSTFILTER,
              name=u'lstFilter', parent=self, pos=wx.Point(0, 80),
              size=wx.Size(203, 69), style=wx.LC_REPORT)
        self._init_coll_lstFilter_Columns(self.lstFilter)
        self.lstFilter.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstFilterListItemSelected,
              id=wxID_VTXMLFINDPANELLSTFILTER)
        self.lstFilter.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstFilterListItemDeselected,
              id=wxID_VTXMLFINDPANELLSTFILTER)
        self.lstFilter.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstFilterListColClick,
              id=wxID_VTXMLFINDPANELLSTFILTER)

        self.cbDelFlt = wx.lib.buttons.GenBitmapButton(ID=wxID_VTXMLFINDPANELCBDELFLT,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbDelFlt', parent=self,
              pos=wx.Point(207, 114), size=wx.Size(31, 30), style=0)
        self.cbDelFlt.Enable(False)
        self.cbDelFlt.Bind(wx.EVT_BUTTON, self.OnCbDelFltButton,
              id=wxID_VTXMLFINDPANELCBDELFLT)

        self.thrProc = vidarc.tool.art.vtThrobber.vtThrobberDelay(id=wxID_VTXMLFINDPANELTHRPROC,
              name=u'thrProc', parent=self, pos=wx.Point(0, 164),
              size=wx.Size(15, 15), style=0)

        self.lblNodeName = wx.StaticText(id=wxID_VTXMLFINDPANELLBLNODENAME,
              label=u'', name=u'lblNodeName', parent=self, pos=wx.Point(142, 0),
              size=wx.Size(88, 21), style=0)
        self.lblNodeName.SetMinSize(wx.Size(-1, -1))

        self.lblFilterAttr = wx.StaticText(id=wxID_VTXMLFINDPANELLBLFILTERATTR,
              label=_(u'attribute'), name=u'lblFilterAttr', parent=self,
              pos=wx.Point(0, 21), size=wx.Size(42, 21), style=wx.ALIGN_RIGHT)
        self.lblFilterAttr.SetMinSize(wx.Size(-1, -1))

        self.lblFilterAttrDesc = wx.StaticText(id=wxID_VTXMLFINDPANELLBLFILTERATTRDESC,
              label=u'', name=u'lblFilterAttrDesc', parent=self,
              pos=wx.Point(142, 21), size=wx.Size(88, 21), style=0)
        self.lblFilterAttrDesc.SetMinSize(wx.Size(-1, -1))

        self.cbFind = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLFINDPANELCBFIND,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Find', name=u'cbFind',
              parent=self, pos=wx.Point(63, 157), size=wx.Size(72, 30),
              style=0)
        self.cbFind.SetMinSize(wx.Size(-1, -1))
        self.cbFind.Bind(wx.EVT_BUTTON, self.OnCbFindButton,
              id=wxID_VTXMLFINDPANELCBFIND)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLFINDPANELCBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=_('Close'), name=u'cbCancel',
              parent=self, pos=wx.Point(81, 199), size=wx.Size(76, 30),
              style=0)
        self.cbCancel.SetMinSize(wx.Size(-1, -1))
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VTXMLFINDPANELCBCANCEL)

        self.cbStop = wx.lib.buttons.GenBitmapButton(ID=wxID_VTXMLFINDPANELCBSTOP,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbStop', parent=self,
              pos=wx.Point(207, 157), size=wx.Size(31, 30), style=0)

        self.cbFindNext = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLFINDPANELCBFINDNEXT,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Next', name=u'cbFindNext',
              parent=self, pos=wx.Point(139, 157), size=wx.Size(64, 30),
              style=0)
        self.cbFindNext.SetMinSize(wx.Size(-1, -1))
        self.cbFindNext.Bind(wx.EVT_BUTTON, self.OnCbFindNextButton,
              id=wxID_VTXMLFINDPANELCBFINDNEXT)

        self.lbbFound = wx.StaticBitmap(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VTXMLFINDPANELLBBFOUND, name=u'lbbFound', parent=self,
              pos=wx.Point(39, 164), size=wx.Size(16, 16), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        vtXmlDomConsumer.__init__(self)
        self._init_ctrls(parent)
        
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.cbCancel.SetBitmapLabel(vtArt.getBitmap(vtArt.Cancel))
        self.cbAddFlt.SetBitmapLabel(vtArt.getBitmap(vtArt.Add))
        self.cbDelFlt.SetBitmapLabel(vtArt.getBitmap(vtArt.Del))
        self.cbStop.SetBitmapLabel(vtArt.getBitmap(vtArt.Stop))
        self.cbFind.SetBitmapLabel(vtArt.getBitmap(vtArt.Search))
        self.cbFindNext.SetBitmapLabel(vtArt.getBitmap(vtArt.SearchNext))
        
        self.lbbFound.SetBitmap(vtArt.getBitmap(vtArt.Invisible))
        #self.gbsFilter.AddGrowableCol(0)
        #self.viFltStr.Disable()
        #self.viFltInt.Disable()
        #self.viFltLong.Disable()
        #self.viFltFloat.Disable()
        #self.viFltDate.Disable()
        #self.viFltTime.Disable()
        #self.gbsFilter.Hide(self.viFltInt)
        #self.gbsFilter.Hide(self.viFltLong)
        #self.gbsFilter.Hide(self.viFltFloat)
        #self.gbsFilter.Hide(self.viFltDate)
        #self.gbsFilter.Hide(self.viFltTime)
        
        #self.gbsFilter.Layout()
        self.operator=vtXmlFilterOperator()
        self.thdFind=None
        self.filters={}
        self.idxSel=-1
        
        self.SetSize(size)
        self.Move(pos)
        self.Clear()
    
    def IsBusy(self):
        if self.thdFind is None:
            return False
        return self.thdFind.IsRunning()
    def Restart(self):
        if self.thdFind is not None:
            self.thdFind.Start()
    def Stop(self):
        if self.thdFind is not None:
            self.thdFind.Stop()

    def OnCbCancelButton(self, event):
        event.Skip()
        try:
            if self.IsBusy():
                return
        except:
            vtLog.vtLngTB(self.GetName())
        wx.PostEvent(self,vtXmlFindPanelCanceled(self))
        
    def OnChcFilterPosChoice(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if event is not None:
            event.Skip()
        idx=self.chcFilterPos.GetSelection()
        sName=self.chcFilterPos.GetStringSelection()
        self.lblNodeName.SetLabel(self.doc.GetTranslation(sName,None))
        self.chcFilterAttr.Clear()
        lst=self.dFilter[sName]
        if lst is None:
            return
        for tup in lst:
            self.chcFilterAttr.Append(tup[0])
        self.chcFilterAttr.SetSelection(0)
        self.OnChcFilterAttrChoice(None)
    def OnChcFilterAttrChoiceEnter(self,event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if event is not None:
            event.Skip()
        sTag=self.chcFilterPos.GetStringSelection()
        sName=self.chcFilterAttr.GetValue()
        try:
            tupLst=self.dFilter[sTag]
            for tup in tupLst:
                if tup[0]==sName:
                    return
            dlg = wx.SingleChoiceDialog(
                    self, _(u'Filter'), u'vtXmlFind '+_(u'Select Filter'),
                    [_(u'String'), _(u'Integer'), _(u'Long'), _(u'Float'),
                     _(u'Date'), _(u'Time'), _(u'Date Time')], 
                    wx.CHOICEDLG_STYLE
                    )

            if dlg.ShowModal() == wx.ID_OK:
                iSel=dlg.GetSelection()
                iVal=[vtXmlFilterType.FILTER_TYPE_STRING,
                    vtXmlFilterType.FILTER_TYPE_INT,
                    vtXmlFilterType.FILTER_TYPE_LONG,
                    vtXmlFilterType.FILTER_TYPE_FLOAT,
                    vtXmlFilterType.FILTER_TYPE_DATE,
                    vtXmlFilterType.FILTER_TYPE_TIME,
                    vtXmlFilterType.FILTER_TYPE_DATE_TIME,
                    ][iSel]
            else:
                return
            self.dFilter[sTag].append((sName,iVal))
            self.chcFilterAttr.Append(sName)
            self.chcFilterAttr.SetStringSelection(sName)
            self.OnChcFilterAttrChoice(None)
        except:
            pass
    def OnChcFilterAttrChoice(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if event is not None:
            event.Skip()
        sTag=self.chcFilterPos.GetStringSelection()
        sName=self.chcFilterAttr.GetStringSelection()
        self.lblFilterAttrDesc.SetLabel(self.doc.GetTranslation(sTag,sName))
        try:
            idx=self.chcFilterAttr.GetSelection()
            tup=self.dFilter[self.chcFilterPos.GetStringSelection()][idx]
            self.viFltStr.Show(False)
            self.viFltDate.Show(False)
            self.viFltTime.Show(False)
            self.viFltDateTime.Show(False)
            self.viFltDateTimeRel.Show(False)
            self.viFltInt.Show(False)
            self.viFltLong.Show(False)
            self.viFltFloat.Show(False)
            self.viFltStr.Enable(False)
            self.viFltDate.Enable(False)
            self.viFltTime.Enable(False)
            self.viFltInt.Enable(False)
            self.viFltLong.Enable(False)
            self.viFltFloat.Enable(False)
            #self.cbAddFlt.Enable(False)
            if tup[1]==vtXmlFilterType.FILTER_TYPE_STRING:
                self.viFltStr.Show(True)
                self.viFltStr.Enable(True)
                self.cbAddFlt.Enable(True)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_DATE:
                self.viFltDate.Show(True)
                self.viFltDate.Enable(True)
                self.cbAddFlt.Enable(True)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_TIME:
                self.viFltTime.Show(True)
                self.viFltTime.Enable(True)
                self.cbAddFlt.Enable(True)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_DATE_TIME:
                self.viFltDateTime.Show(True)
                self.viFltDateTime.Enable(True)
                self.cbAddFlt.Enable(True)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_DATE_TIME_REL:
                self.viFltDateTimeRel.Show(True)
                self.viFltDateTimeRel.Enable(True)
                self.cbAddFlt.Enable(True)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_INT:
                self.viFltInt.Show(True)
                self.viFltInt.Enable(True)
                self.cbAddFlt.Enable(True)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_LONG:
                self.viFltLong.Show(True)
                self.viFltLong.Enable(True)
                self.cbAddFlt.Enable(True)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_FLOAT:
                self.viFltFloat.Show(True)
                self.viFltFloat.Enable(True)
                self.cbAddFlt.Enable(True)
            else:
                self.viFltStr.Show(True)
                self.viFltStr.Enable(False)
                self.cbAddFlt.Enable(True)
            
        except:
            self.viFltStr.Show(True)
            self.viFltDate.Show(False)
            self.viFltTime.Show(False)
            self.viFltInt.Show(False)
            self.viFltLong.Show(False)
            self.viFltFloat.Show(False)
            self.viFltStr.Enable(False)
            self.viFltDate.Enable(False)
            self.viFltTime.Enable(False)
            self.viFltDateTime.Enable(False)
            self.viFltDateTimeRel.Enable(False)
            self.viFltInt.Enable(False)
            self.viFltLong.Enable(False)
            self.viFltFloat.Enable(False)
            self.cbAddFlt.Enable(False)

        self.bxsFilterInput.Layout()

    def Clear(self):
        vtXmlDomConsumer.Clear(self)
        
    def SetDoc(self,doc,bNet=False):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        vtXmlDomConsumer.SetDoc(self,doc,bNet)
        self.thdFind=None
        self.lstFilter.DeleteAllItems()
        self.idxSel=-1
        self.cbAddFlt.Enable(False)
        self.cbDelFlt.Enable(False)
        self.cbStop.Enable(False)
        self.cbFind.Enable(True)
        self.cbFindNext.Enable(False)
        self.UpdateFilter()
    def UpdateFilter(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.thdFind is not None:
            if self.thdFind.IsRunning():
                return
        self.chcFilterPos.Clear()
        self.filters={}
        try:
            if self.doc is not None:
                self.dFilter=self.doc.GetPossibleFilter()
                if self.dFilter is None:
                    return
                keys=self.dFilter.keys()
                for k in keys:
                    self.chcFilterPos.Append(k)
            self.chcFilterPos.SetSelection(0)
            self.OnChcFilterPosChoice(None)
            self.thdFind=vtXmlFindThread(self.GetName(),self,self.doc,verbose=0)
        except:
            vtLog.vtLngTB(self.GetName())
        
    def SetNode(self,node):
        self.Clear()
        
    def GetNode(self,node=None):
        if node is None:
            node=self.node
        if node is None:
            return
        
    def OnFound(self,node):
        #print node
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.thrProc.Rest()
        self.cbStop.Enable(False)
        self.cbFind.Enable(True)
        self.cbFindNext.Enable(True)
        if node is None:
            self.lbbFound.SetBitmap(vtArt.getBitmap(vtArt.Cancel))
        else:
            self.lbbFound.SetBitmap(vtArt.getBitmap(vtArt.Apply))
        wx.PostEvent(self,vtXmlFindPanelFound(self,node))
        
    def OnCbFindButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        event.Skip()
        if self.IsBusy():
            return
        if self.thdFind is None:
            return
        d={}
        for k in self.filters.keys():
            l=self.filters[k]
            ll=[]
            for tup in l:
                ll.append((tup[0][0],tup[2]))
            d[k]=ll
        self.lbbFound.SetBitmap(vtArt.getBitmap(vtArt.Search))
        self.thdFind.Find(d,self.doc.getBaseNode(),[],
                self.OnFound)
        self.cbStop.Enable(True)
        self.cbFind.Enable(False)
        self.cbFindNext.Enable(False)

    def OnCbFindNextButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.IsBusy():
            return
        if self.thdFind is None:
            return
        self.lbbFound.SetBitmap(vtArt.getBitmap(vtArt.Search))
        self.thdFind.FindNext()
        self.cbStop.Enable(True)
        event.Skip()

    def OnCbAddFltButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        event.Skip()
        try:
            sName=self.chcFilterPos.GetStringSelection()
            lstAttr=self.dFilter[sName]
            
            idx=self.chcFilterAttr.GetSelection()
            tup=self.dFilter[self.chcFilterPos.GetStringSelection()][idx]
            try:
                lst=self.filters[sName]
            except:
                lst=[]
                self.filters[sName]=lst
            if tup[1]==vtXmlFilterType.FILTER_TYPE_STRING:
                sFlt=self.viFltStr.GetValueStr()
                filter=self.viFltStr.GetFilter()
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_DATE:
                sFlt=self.viFltDate.GetValueStr()
                filter=self.viFltDate.GetFilter()
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_TIME:
                sFlt=self.viFltTime.GetValueStr()
                filter=self.viFltTime.GetFilter()
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_DATE_TIME:
                sFlt=self.viFltDateTime.GetValueStr()
                filter=self.viFltDateTime.GetFilter()
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_DATE_TIME_REL:
                sFlt=self.viFltDateTimeRel.GetValueStr()
                filter=self.viFltDateTimeRel.GetFilter()
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_INT:
                sFlt=self.viFltInt.GetValueStr()
                filter=self.viFltInt.GetFilter()
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_LONG:
                sFlt=self.viFltLong.GetValueStr()
                filter=self.viFltLong.GetFilter()
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_FLOAT:
                sFlt=self.viFltFloat.GetValueStr()
                filter=self.viFltFloat.GetFilter()
            lst.append((tup,sFlt,filter))
            def cmpFunc(a,b):
                return lstAttr.index(a[0])-lstAttr.index(b[0])
            lst.sort(cmpFunc)
            self.__showFilters__()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnCbDelFltButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        event.Skip()
        if self.idxSel<0:
            return
        try:
            it=self.lstFilter.GetItem(self.idxSel,0)
            sTag=it.m_text
            lst=self.filters[sTag]
            i=self.lstFilter.GetItemData(self.idxSel)
            lst=lst[:i]+lst[i+1:]
            if len(lst)<=0:
                del self.filters[sTag]
            else:
                self.filters[sTag]=lst
            self.filters
            self.__showFilters__()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnLstFilterListItemSelected(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.idxSel=event.GetIndex()
        self.cbDelFlt.Enable(True)
        event.Skip()
        
    def __showFilters__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.cbDelFlt.Enable(False)
        self.idxSel=-1
        self.lstFilter.DeleteAllItems()
        keys=self.filters.keys()
        keys.sort()
        for k in keys:
            lFlt=self.filters[k]
            i=0
            for tup in lFlt:
                idx=self.lstFilter.InsertStringItem(sys.maxint,k)
                self.lstFilter.SetStringItem(idx,1,tup[0][0])
                self.lstFilter.SetStringItem(idx,2,tup[1])
                #self.operator.SetValue(tup[3])
                self.lstFilter.SetStringItem(idx,3,tup[2].GetOpStr())
                self.lstFilter.SetItemData(idx,i)
                i+=1
    def OnLstFilterListItemDeselected(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        event.Skip()
        self.idxSel=-1
        self.cbDelFlt.Enable(False)

    def OnLstFilterListColClick(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.idxSel=-1
        self.cbDelFlt.Enable(False)
        event.Skip()

    
