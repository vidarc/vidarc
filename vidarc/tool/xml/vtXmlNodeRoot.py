#----------------------------------------------------------------------------
# Name:         vXmlNodeRoot.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060607
# CVS-ID:       $Id: vtXmlNodeRoot.py,v 1.5 2010/03/03 02:16:19 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)

import vidarc.tool.log.vtLog as vtLog       #FIXME: 091015:wro remove
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeBase import vtXmlNodeBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vLogFallBack.logDebug('GUI objects imported',__name__)
    else:
        GUI=0
        vLogFallBack.logDebug('GUI objects import skipped',__name__)
except:
    vLogFallBack.logTB(__name__)
    GUI=0
vLogFallBack.logDebug('import;done',__name__)

class vtXmlNodeRoot(vtXmlNodeBase):
    FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='root'):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        vtXmlNodeBase.__init__(self,tagName)
        self.FMT_GET_4_TREE=self.GetName
    def GetDescription(self):
        return _(u'application root')
    def GetName(self,parent,node,tagName,infos):
        return self.GetDescription()
    # ---------------------------------------------------------
    # specific
    def GetTag(self,node):
        return self.tagName
    def GetName(self,node):
        return self.GetDescription()
    def SetTag(self,node,val):
        pass
    def SetName(self,node,val):
        pass
    # ---------------------------------------------------------
    # inheritance
    def Is2Create(self):
        return False
    def Is2Add(self):
        return False
    def IsMultiple(self):
        return False
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return False
    def IsId2Add(self):
        return False
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00pIDAT8\x8dcddbf@\x07\xba\x1d!\xff1\x04\x19\x18\x18.W\xacaD\x17c"V!61\
\x06\x06\x06\x06F\x98\x0bp\xd9\x8a\x0b\xc0\x0c\xc4\xea\x02R\x00\xe5\x06\x90\
\xeatd\xa0\xdb\x11\xf2\x9fQ\xaf+\x9cl\x03\x18\x18\x06E\x18P\xc5\x00\\\x89\
\x04\x1f\x80\xa7\x03r4#\x1b2\xf0a\xc0\x88-720`\xe6\r\\^\xc5\xea\x02l\xa9\x13\
W\x8a\x05\x00b\xb7"-7\xce\xbc\x10\x00\x00\x00\x00IEND\xaeB`\x82' 

    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        return None
    def GetAddDialogClass(self):
        return None
    def GetPanelClass(self):
        return None

