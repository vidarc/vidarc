#----------------------------------------------------------------------------
# Name:         vtXmlFindQuickTrend.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20070108
# CVS-ID:       $Id: vtXmlFindQuickThread.py,v 1.3 2007/07/18 06:49:31 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.vtThreadEndless import vtThreadEndless
from vidarc.tool.vtThread import vtThreadProc
import vidarc.tool.log.vtLog as vtLog

from wx import NewEventType
from wx import PyEvent
from wx import PostEvent
import time,types,fnmatch

vtxEVT_FIND_QUICK_FOUND=NewEventType()
def EVT_VTXML_FIND_QUICK_FOUND(win,func):
    win.Connect(-1,-1,vtxEVT_FIND_QUICK_FOUND,func)
def EVT_VTXML_FIND_QUICK_FOUNDD_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtxEVT_FIND_QUICK_FOUND)
class vtxFindQuickFound(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VTXML_FIND_QUICK_FOUND(<widget_name>, xxx)
    """
    def __init__(self,obj,id):
        PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.idNode=id
        self.SetEventType(vtxEVT_FIND_QUICK_FOUND)
    def GetID(self):
        return self.idNode

class vtXmlFindQuickThread(vtThreadEndless):
    Z_UPDATE_INTERVAL=0.1
    def __init__(self,par,bPost=False,lFindTagName=None,verbose=0,origin=None):
        vtThreadEndless.__init__(self,par,bPost=bPost,verbose=verbose,origin=origin)
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self.widLogging=vtLog.GetPrintMsgWid(par)
        self.lFindTagName=lFindTagName
        self.bFindSpecific=False
        self.bFindAll=False
        if lFindTagName is None:
            self.bFindAll=True
        else:
            if type(lFindTagName)==types.DictionaryType:
                self.bFindSpecific=True
                self.dSpecific={}
                for k,l in lFindTagName.iteritems():
                    d={}
                    for v in l:
                        d[v]=False
                    self.dSpecific[k]=d
            else:
                self.bFindSpecific=False
            self.bFindAll=False
        self.lHierLast=None
        self._clearIntData()
    def _clearIntData(self):
        self.iAct=0
        self.iCount=1
        self.zUpdate=0
        self.idLast=-1
        self.bMatch=False
        self.bFound=False
    def _clearSpecificDict(self,d):
        for k in d.iterkeys():
            d[k]=False
    def _isSpecificDictInspected(self,d):
        for v in d.itervalues():
            if v==False:
                return False
        return True
    def _is2Update(self):
        zNow=time.clock()
        if zNow>(self.zUpdate+self.Z_UPDATE_INTERVAL):
            self.zUpdate=zNow
            return True
        return False
    def Do(self,id,idLast,sPattern,lv,func=None,*args,**kwargs):
        self.qSched.put((id,idLast,sPattern.lower(),lv,func,args,kwargs))
        self._ensureRun()
    def _doProc(self,t):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'proc'%(),self.origin)
        id,idLast,sPattern,lv,func,args,kwargs=t
        doc=self.par.doc
        self._clearIntData()
        sPattern=sPattern.lower()
        if sPattern.find(u'?')>=0:
            self.bMatch=True
        if sPattern.find(u'*')>=0:
            self.bMatch=True
        
        self.iCount=doc.GetElementAttrCount()
        doc.acquire('dom')
        self.updateProcessbar=self.par.bMaster
        try:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'id:%08d;idLast:%08d'%(id,idLast),self.origin)
            if self.verbose>0:
                vtLog.vtLngCur(vtLog.DEBUG,'t:%s'%(repr(t)),self.origin)
            if doc.IsKeyValid(id):
                node=doc.getNodeByIdNum(id)
                if node is None:
                    vtLog.vtLngCur(vtLog.WARN,'node for id:%08d not found'%(id),self.origin)
                    vtLog.PrintMsg(_(u'node for id:%08d not found')%(id),self.widLogging)
                    doc.release('dom')
                    return
            else:
                node=doc.getBaseNode()
            self.idLast=-1
            lHier=None
            if doc.IsKeyValid(idLast):
                ##self.idLast=idLast
                lHier=self.lHierLast
            lFound=[]
            if doc.IsKeyValid(id):
                vtLog.PrintMsg(_(u'searching at id:%08d ...')%(id),
                        self.widLogging,bForce=True)
            else:
                vtLog.PrintMsg(_(u'searching at root ...'),
                        self.widLogging,bForce=True)
            if doc.IsNodeKeyValid(node):
                #doc.procKeysRec(lv,node,self.__procElem__,doc,sPattern,lFound)
                iRet,self.lHierLast=doc.procKeysRecHierReStart(lHier,lv,node,self.__procElem__,doc,sPattern,lFound)
            else:
                #vtLog.CallStack('')
                #print 'start',lHier
                #doc.procChildsKeysRec(lv,node,self.__procElem__,doc,sPattern,lFound)
                iRet,self.lHierLast=doc.procChildsKeysRecHierReStart(lHier,lv,node,self.__procElem__,doc,sPattern,lFound)
                #print 'result',iRet,self.lHierLast
            vtLog.SetProcBarVal(0,self.iCount,self.widLogging)
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'lFound:%s'%(vtLog.pformat(lFound)),self.origin)
            if self.bPost:
                if len(lFound)>0:
                    if doc.IsKeyValid(id):
                        vtLog.PrintMsg(_(u'searching at id:%08d finished (success).')%(id),self.widLogging)
                    else:
                        vtLog.PrintMsg(_(u'searching at root finished (success).'),self.widLogging)
                    PostEvent(self.par,vtxFindQuickFound(self.par,lFound[0]))
                else:
                    if doc.IsKeyValid(id):
                        vtLog.PrintMsg(_(u'searching at id:%08d finished (failed).')%(id),self.widLogging)
                    else:
                        vtLog.PrintMsg(_(u'searching at root finished (failed).'),self.widLogging)
                    PostEvent(self.par,vtxFindQuickFound(self.par,-1))
        except:
            vtLog.vtLngTB(self.origin)
        doc.release('dom')
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'proc done'%(),self.origin)
    def __procElem__(self,node,doc,sPattern,lFound):
        if self.qSched.empty()==False:
            return -2
        self.iAct+=1
        if self._is2Update():
            PostEvent(self.par,vtThreadProc(self.iAct,self.iCount))
            vtLog.SetProcBarVal(self.iAct,self.iCount,self.widLogging)
        idAct=doc.getKeyNum(node)
        #print '__procElem__',idAct
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'id:%08d'%(idAct),self.origin)
        if self.idLast>=0:
            if idAct==self.idLast:
                self.idLast=-1
            return 0
        #if len(lIdLast)>0:
        #    if idAct==lIdLast[-1]:
                #lIdLast=[]
        #        vtLog.vtLngCur(vtLog.DEBUG,'id:%08d;lIdlast:%s'%(idAct,vtLog.pformat(lIdLast)),self.origin)
        #        while len(lIdLast)>0:
        #            del lIdLast[0]
        #    return 0
        if doc.IsSkip(node):
            return 0
        if self.par.__is2block__(node):
            return 0
        if self.par.__is2skip__(node,doc.getTagName(node)):
            return 0
        if self.bFindSpecific:
            sTagName=doc.getTagName(node)
            dSpecific=None
            if sTagName in self.dSpecific:
                dSpecific=self.dSpecific[sTagName]
            elif None in self.dSpecific:
                dSpecific=self.dSpecific[None]
            else:
                vtLog.vtLngCur(vtLog.WARN,'tagName:%s not found in;lFindTagName:%s;dSpecific:%s'%
                        (sTagName,vtLog.pformat(self.lFindTagName),
                        vtLog.pformat(self.dSpecific)),self.origin)
            if dSpecific is not None:
                self._clearSpecificDict(dSpecific)
                #iRet=self.__specificMatch__(node,doc,sPattern,idAct,dSpecific,lFound)
                iRet=doc.procChildsExt(node,self.__specificMatch__,doc,sPattern,idAct,dSpecific,lFound)
            else:
                iRet=0
        else:
            iRet=doc.procChildsExt(node,self.__procMatch__,doc,sPattern,idAct,lFound)
        
        #if iRet<0:
        if self.bFound==True:
            lFound.append(idAct)
            return -1
        return 0
    def _match(self,node,doc,sPattern):
        sVal=doc.getAttributeVal(node)
        try:
            sVal=sVal.lower()
            if self.bMatch:
                if fnmatch.fnmatch(sVal,sPattern)==True:
                    self.bFound=True
                    return -1
            else:
                if sVal.startswith(sPattern):
                    self.bFound=True
                    return -1
        except:
            pass
        return 0
    def __specificMatch__(self,node,doc,sPattern,idAct,dSpecific,lFound):
        if self.qSched.empty()==False:
            return -2
        sTagName=doc.getTagName(node)
        if sTagName in dSpecific:
            dSpecific[sTagName]=True
            iRet=self._match(node,doc,sPattern)
            if iRet==0:
                if self._isSpecificDictInspected(dSpecific):
                    return -3
            else:
                return iRet
        return 0
    def __procMatch__(self,node,doc,sPattern,idAct,lFound):
        if self.qSched.empty()==False:
            return -2
        if doc.IsNodeKeyValid(node)==False:
            if self.bFindAll==False:
                sTagName=doc.getTagName(node)
                if sTagName not in self.lFindTagName:
                    return 0
            return self._match(node,doc,sPattern)
        return 0

