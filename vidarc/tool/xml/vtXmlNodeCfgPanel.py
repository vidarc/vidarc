#Boa:FramePanel:vtXmlNodeCfgPanel
#----------------------------------------------------------------------------
# Name:         vtXmlNodeCfgPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060216
# CVS-ID:       $Id: vtXmlNodeCfgPanel.py,v 1.16 2008/03/26 23:12:28 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.xml.vtXmlGrpTreeReg
import vidarc.tool.xml.vtXmlTree
import vidarc.tool.input.vtInputTextML

import vidarc.tool.xml.vtXmlDomConsumer as vtXmlDomConsumer
from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_GOT_CONTENT
from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_GET_NODE
from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_REMOVE_NODE
from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_LOCK
from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_UNLOCK
from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_SET_NODE
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase


[wxID_VTXMLNODECFGPANEL, wxID_VTXMLNODECFGPANELLBLNAME, 
 wxID_VTXMLNODECFGPANELLBLTAG, wxID_VTXMLNODECFGPANELTRCFG, 
 wxID_VTXMLNODECFGPANELTXTMLNAME, wxID_VTXMLNODECFGPANELTXTTAG, 
] = [wx.NewId() for _init_ctrls in range(6)]

class vtXmlNodeCfgPanel(wx.Panel,vtXmlDomConsumer.vtXmlDomConsumer):
    VERBOSE=0
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(3)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsTag, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsName, 1, border=3, flag=wx.EXPAND | wx.TOP)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.trCfg, 1, border=0, flag=wx.EXPAND)

    def _init_coll_bxsTag_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblTag, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtTag, 2, border=4, flag=wx.EXPAND)

    def _init_coll_bxsName_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblName, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtmlName, 2, border=0, flag=wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=4, vgap=0)

        self.bxsTag = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsName = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsTag_Items(self.bxsTag)
        self._init_coll_bxsName_Items(self.bxsName)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTXMLNODECFGPANEL,
              name=u'vtXmlNodeCfgPanel', parent=prnt, pos=wx.Point(286, 253),
              size=wx.Size(223, 291), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(215, 264))

        self.lblTag = wx.StaticText(id=wxID_VTXMLNODECFGPANELLBLTAG,
              label=u'tag', name=u'lblTag', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(71, 21), style=wx.ALIGN_RIGHT)
        self.lblTag.SetMinSize(wx.Size(-1, -1))

        self.txtTag = wx.TextCtrl(id=wxID_VTXMLNODECFGPANELTXTTAG,
              name=u'txtTag', parent=self, pos=wx.Point(71, 0),
              size=wx.Size(143, 21), style=0, value=u'')
        self.txtTag.SetMinSize(wx.Size(-1, -1))

        self.txtmlName = vidarc.tool.input.vtInputTextML.vtInputTextML(id=wxID_VTXMLNODECFGPANELTXTMLNAME,
              name=u'txtmlName', parent=self, pos=wx.Point(71, 24),
              size=wx.Size(143, 30), size_button=wx.Size(31, 30),
              size_text=wx.Size(100, 22), style=0)
        self.txtmlName.SetMinSize(wx.Size(-1, -1))

        self.lblName = wx.StaticText(id=wxID_VTXMLNODECFGPANELLBLNAME,
              label=u'name', name=u'lblName', parent=self, pos=wx.Point(0, 24),
              size=wx.Size(67, 30), style=wx.ALIGN_RIGHT)
        self.lblName.SetMinSize(wx.Size(-1, -1))

        self.trCfg = vidarc.tool.xml.vtXmlGrpTreeReg.vtXmlGrpTreeReg(controller=False,
              id=wxID_VTXMLNODECFGPANELTRCFG, master=False, name=u'trCfg',
              parent=self, pos=wx.Point(0, 62), size=wx.Size(215, 202),
              style=0)
        self.trCfg.SetMinSize(wx.Size(-1, -1))

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self._init_ctrls(parent)
        vtXmlDomConsumer.vtXmlDomConsumer.__init__(self)
        self.bModified=False
        
        self.trCfg.EnableMenu('Edit',True)
        self.trCfg.EnableMenu('Misc',False)
        self.trCfg.EnableMenu('Import',True)
        self.trCfg.EnableMenu('Export',True)
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
        self.objRegNode=None
        self.fgsData.Layout()
        self.fgsData.Fit(self)
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        
    def SetNetDocs(self,d):
        pass
    def SetRegNode(self,obj):
        self.objRegNode=obj
    def SetModified(self,state):
        self.bModified=state    
    def GetModified(self):
        return self.bModified
    def __isModified__(self):
        if self.bModified:
            return True
        return False
    def Clear(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.VERBOSE:
            vtLog.CallStack('')
        self.SetModified(False)
        if self.doc is not None:
            self.doc.endEdit(self.node)
        vtXmlDomConsumer.vtXmlDomConsumer.Clear(self)
        self.txtTag.SetValue('')
        self.txtmlName.Clear()
    def SetDoc(self,doc,bNet=False):
        try:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCurWX(vtLog.INFO,'bNet:%d'%(bNet),self)
            vtXmlDomConsumer.vtXmlDomConsumer.SetDoc(self,doc)
            if doc is not None:
                if bNet==True:
                    EVT_NET_XML_GOT_CONTENT(doc,self.OnGotContent)
                    EVT_NET_XML_GET_NODE(doc,self.OnGetNode)
                    EVT_NET_XML_REMOVE_NODE(doc,self.OnRemoveNode)
                    EVT_NET_XML_LOCK(doc,self.OnLock)
                    EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
                    EVT_NET_XML_SET_NODE(doc,self.OnSetNode)
            self.txtmlName.SetDoc(doc)
            self.trCfg.SetDoc(doc,bNet)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnGotContent(self,evt):
        evt.Skip()
    def OnGetNode(self,evt):
        evt.Skip()
        if self.node is None:
            return
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            self.SetNode(self.node)
    def OnAddNode(self,evt):
        evt.Skip()
    def OnDelNode(self,evt):
        evt.Skip()
    def OnRemoveNode(self,evt):
        evt.Skip()
    def OnSetNode(self,evt):
        evt.Skip()
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            self.SetNode(self.node)
    def OnLock(self,evt):
        evt.Skip()
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                self.Lock(False)
            else:
                self.Lock(True)
    def OnUnLock(self,evt):
        evt.Skip()
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['released']:
                self.Lock(False)

    def SetNode(self,node):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        vtLog.vtLngCurWX(vtLog.INFO,'id:%s'%(self.doc.getKey(node)),self)
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,node,self)
        self.Clear()
        self.node=node
        self.doc.startEdit(node)
        vtXmlDomConsumer.vtXmlDomConsumer.SetNode(self,node)
        if self.VERBOSE:
            vtLog.CallStack('')
            print self.node
        self.txtTag.SetValue(self.doc.getNodeText(self.node,'tag'))
        self.txtmlName.SetNode(self.node,'name')
        self.trCfg.SetNode(self.node)
    def GetNode(self,node=None):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.doc is None:
            return
        if node is None:
            if self.node is None:
                return
            node=self.node
        self.doc.setNodeText(node,'tag',self.txtTag.GetValue())
        self.txtmlName.GetNode(node)
        self.doc.AlignNode(node,iRec=2)
        self.doc.doEdit(node)
        self.doc.endEdit(node)
    def Lock(self,flag):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if flag:
            self.txtTag.Enable(False)
            self.txtmlName.Enable(False)
        else:
            self.txtTag.Enable(True)
            self.txtmlName.Enable(True)
            

