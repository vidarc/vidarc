#----------------------------------------------------------------------------
# Name:         vtXmlFilter.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtXmlFilter.py,v 1.3 2007/07/28 14:43:34 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from wx import NewEventType as wxNewEventType
from wx import PyEvent as wxPyEvent
from wx import PostEvent as wxPostEvent

import vtXmlDom as vtXmlDom
import vidarc.tool.log.vtLog as vtLog

import thread,fnmatch


# defined event for vgpXmlTree item selected
wxEVT_THREAD_FILTER_ELEMENTS=wxNewEventType()
def EVT_THREAD_FILTER_ELEMENTS(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_FILTER_ELEMENTS,func)
class wxThreadFilterElements(wxPyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_FILTER_ELEMENTS(<widget_name>, self.OnItemSel)
    """

    def __init__(self,iVal):
        wxPyEvent.__init__(self)
        self.val=iVal
        self.SetEventType(wxEVT_THREAD_FILTER_ELEMENTS)
    def GetValue(self):
        return self.val

# defined event for vgpXmlTree item selected
wxEVT_THREAD_FILTER_ELEMENTS_FINISHED=wxNewEventType()
def EVT_THREAD_FILTER_ELEMENTS_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_FILTER_ELEMENTS_FINISHED,func)
class wxThreadFilterElementsFinished(wxPyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_FILTER_ELEMENTS_FINISHED(<widget_name>, self.OnItemSel)
    """
    def __init__(self):
        wxPyEvent.__init__(self)
        self.SetEventType(wxEVT_THREAD_FILTER_ELEMENTS_FINISHED)

# defined event for vgpXmlTree item selected
wxEVT_THREAD_FILTER_ELEMENTS_ABORTED=wxNewEventType()
def EVT_THREAD_FILTER_ELEMENTS_ABORTED(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_FILTER_ELEMENTS_ABORTED,func)
class wxThreadFilterElementsAborted(wxPyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_FILTER_ELEMENTS_ABORTED(<widget_name>, self.OnItemSel)
    """
    def __init__(self):
        wxPyEvent.__init__(self)
        self.SetEventType(wxEVT_THREAD_FILTER_ELEMENTS_ABORTED)


class thdFilter:
    def __init__(self,doc=None,verbose=0):
        self.verbose=verbose
        self.doc=doc
        self.lstSort=None
        self.running=False
    def Find(self,node,par):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.par=par
        self.node=node
        self.bFind=True
        self.lstFound=[]
        self.iAct=0
        if self.par is not None:
            self.updateProcessbar=True
        else:
            self.updateProcessbar=False
        
        self.Start()
        self.lstSort=None
    def Clear(self):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.iFound=0
            
        if self.lstSort is not None:
            del self.lstSort
        self.lstSort=[]
        self.iAct=0
            
    def Show(self):
        self.bFind=False
        if self.running==False:
            self.Start()
    def Start(self):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.keepGoing = self.running = True
        thread.start_new_thread(self.Run, ())

    def Stop(self):
        self.keepGoing = False

    def IsRunning(self):
        return self.running
    def __procNode__(self,node,*args):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        if self.keepGoing==False:
            return
        bFound=True
        sTag=None
        sHier=None
        sTagName=None
        sName=None
        self.iAct+=1
        if self.updateProcessbar:
            wxPostEvent(self.par,wxThreadFilterElements(self.iAct))
        self.doc.acquire()
        tagName,infos=self.doc.getNodeInfos(node)
        try:
            bFound=self.doc.__procFilter__(node,tagName,infos)
            if bFound:
                self.iFound+=1
                self.lstFound.append(node)
        except:
            self.Stop()
            self.doc.release()
            return -1
        self.doc.release()
        self.doc.procChildsAttr(node,'id',self.__procNode__,None)
        return 0
        
    def Run(self):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        if self.bFind:
            self.Clear()
            self.doc.procChildsAttr(self.node,'id',
                    self.__procNode__,None)

        #if self.keepGoing:
        #    self.__showNodes__()
        #if self.keepGoing==False:
        #    self.tree.lstRes.DeleteAllItems()
        self.keepGoing = self.running = False
        if self.updateProcessbar:
            wxPostEvent(self.par,wxThreadFilterElements(0))
            wxPostEvent(self.par,wxThreadFilterElementsFinished())
    def GetFoundCount(self):
        return self.iFound
    def GetFound(self):
        return self.lstFound
    def GetNode(self,idx):
        if self.lstSort is None:
            return None
        try:
            return self.lstSort[idx][1][3]
        except:
            return None


class vtXmlFilter(vtXmlDom.vtXmlDom):
    def __init__(self,verbose=0):
        vtXmlDom.vtXmlDom.__init__(self)
        self.verbose=verbose
        self.filters=[]
        self.cmpDict={}
        self.thdFilter=thdFilter(self,verbose=verbose-1)
    def IsRunning(self):
        return self.thdFilter.IsRunning()
    def setFilter(self,dict,cmpDict={}):
        #self.setNodeInfos2Get(infolst)
        self.filters=dict
        self.cmpDict=cmpDict
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
            keys=self.filters.keys()
            keys.sort()
            for k in keys:
                vtLog.vtLngCallStack(self,vtLog.DEBUG,'keys:%s val:%s'%(k,repr(self.filters[k])),callstack=False)
    def __match__(self,info,filters):
        for filter in filters:
            if fnmatch.fnmatch(info,filter):
                return True
        return False
    def __procFilter__(self,node,tagName,infos):
        bMatch=True
        for k in self.filters.keys():
            if self.cmpDict.has_key(k):
                if self.cmpDict[k](infos,self.filters,k)==False:
                    return False
            else:
                if self.__match__(infos[k],self.filters[k])==False:
                    return False
        return True
        
        zStart=self.tree.doc.getNodeText(node,'starttime')
        if zStart=='--:--:--':
            bFound=False
        #print node
        if self.prjId>=0:
            n=self.tree.doc.getChild(node,'project')
            if self.tree.doc.getAttribute(n,'fid')!=self.prjId:
                bFound=False
        if bFound and (self.usrId>=0):
            n=self.tree.doc.getChild(node,'person')
            if self.tree.doc.getAttribute(n,'fid')!=self.usrId:
                bFound=False
        if bFound and (self.locId>=0):
            n=self.tree.doc.getChild(node,'loc')
            if self.tree.doc.getAttribute(n,'fid')!=self.locId:
                bFound=False
        if bFound and self.bTask:
            n=self.tree.doc.getChild(node,'task')
            tId=self.tree.doc.getAttribute(n,'fid')
            try:
                self.lstTask.index(long(tId))
            except:
                bFound=False
        if bFound:
            sDate=self.tree.doc.getNodeText(node,'date')
            if self.zStartDate<=sDate and sDate<=self.zEndDate:
                pass
            else:
                bFound=False            
        if bFound:
            self.iFound+=1
            self.lstFound.append(node)
        #self.iCount+=1
    
        
        return False
    def Search(self,node,par=None):
        self.thdFilter.Find(node,par)
    def GetFound(self):
        return self.thdFilter.GetFound()
    def GetFoundCount(self):
        return self.thdFilter.GetFoundCount()
    