#----------------------------------------------------------------------------
# Name:         vtXmlMsgList.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060706
# CVS-ID:       $Id: vtXmlMsgList.py,v 1.6 2007/10/28 22:46:16 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import sys

from vidarc.tool.time.vtTime import vtDateTime
import vidarc.tool.lang.vtLgBase as vtLgBase

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.time.vtTimeLoadBound as vtLdBd
from vidarc.tool.vtThread import vtThread
from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer
from vidarc.tool.net.vNetXmlWxGuiEvents import *

wxEVT_XML_MSG_LIST_VIEW=wx.NewEventType()
vEVT_XML_MSG_LIST_VIEW=wx.PyEventBinder(wxEVT_XML_MSG_LIST_VIEW,1)
def EVT_XML_MSG_LIST_VIEW(win,func):
    win.Connect(-1,-1,wxEVT_XML_MSG_LIST_VIEW,func)
def EVT_XML_MSG_LIST_VIEW_DISCONNECT(win,func):
    win.Disconnect(-1,-1,wxEVT_XML_MSG_LIST_VIEW)
class vtXmlMsgListView(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_XML_MSG_LIST_VIEW(<widget_name>, self.OnMsgView)
    """
    def __init__(self,obj,appl,alias,msgid):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_XML_MSG_LIST_VIEW)
        self.appl=appl
        self.alias=alias
        self.msgid=msgid
    def GetAppl(self):
        return self.appl
    def GetAlias(self):
        return self.alias
    def GetMsgID(self):
        return self.msgid
    def GetInfos(self):
        return self.appl,self.alias,self.msgid

wxEVT_XML_MSG_LIST_GOTO=wx.NewEventType()
vEVT_XML_MSG_LIST_GOTO=wx.PyEventBinder(wxEVT_XML_MSG_LIST_GOTO,1)
def EVT_XML_MSG_LIST_GOTO(win,func):
    win.Connect(-1,-1,wxEVT_XML_MSG_LIST_GOTO,func)
def EVT_XML_MSG_LIST_GOTO_DISCONNECT(win,func):
    win.Disconnect(-1,-1,wxEVT_XML_MSG_LIST_GOTO)
class vtXmlMsgListGoto(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_XML_MSG_LIST_GOTO(<widget_name>, self.OnMsgGoTo)
    """
    def __init__(self,obj,appl,alias,fid):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_XML_MSG_LIST_GOTO)
        self.appl=appl
        self.alias=alias
        self.fid=fid
    def GetAppl(self):
        return self.appl
    def GetAlias(self):
        return self.alias
    def GetFID(self):
        return self.fid
    def GetInfos(self):
        return self.appl,self.alias,self.fid

wxEVT_XML_MSG_LIST_FINISHED=wx.NewEventType()
vEVT_XML_MSG_LIST_FINISHED=wx.PyEventBinder(wxEVT_XML_MSG_LIST_FINISHED,1)
def EVT_XML_MSG_LIST_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_XML_MSG_LIST_FINISHED,func)
def EVT_XML_MSG_LIST_FINISHED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,wxEVT_XML_MSG_LIST_FINISHED)
class vtXmlMsgListFinished(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_XML_MSG_LIST_FINISHED(<widget_name>, self.OnMsgFin)
    """
    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_XML_MSG_LIST_FINISHED)


class vtXmlMsgConsumer(vtXmlDomConsumer):
    def __init__(self,par):
        vtXmlDomConsumer.__init__(self)
        #vtXmlDomConsumerLang.__init__(self)
        self.par=par
    def IsBusy(self):
        return self.par.IsBusy()
    def Restart(self):
        self.par.Restart()
    def Stop(self):
        self.par.Stop()

class vtXmlMsgList(wx.ListCtrl):
    VERBOSE=0
    def __init__(self,id=-1,parent=None,pos=(0,0),size=(400,100),name='lstMsg',style=0):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        wx.ListCtrl.__init__(self,id=id, name=name,parent=parent, pos=pos, size=size,
              style=wx.LC_REPORT)
        self.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT, heading=_(u'Prior'),width=60)
        self.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT, heading=_(u'Origin'), width=160)
        self.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT, heading=_(u'Subject'), width=400)
        self.InsertColumn(col=3, format=wx.LIST_FORMAT_LEFT, heading=_(u'Author'), width=80)
        self.InsertColumn(col=4, format=wx.LIST_FORMAT_LEFT, heading=_(u'Recipient'), width=80)
        self.InsertColumn(col=5, format=wx.LIST_FORMAT_LEFT, heading=_(u'Time'), width=140)
        self.InsertColumn(col=6, format=wx.LIST_FORMAT_LEFT, heading=_(u'Appl'),width=70)
        self.InsertColumn(col=7, format=wx.LIST_FORMAT_LEFT, heading=_(u'Alias'),width=70)
        self.imgLst=wx.ImageList(16,16)
        self.imgLst.Add(vtArt.getBitmap(vtArt.EMail))
        self.imgLst.Add(vtArt.getBitmap(vtArt.Find))
        self.imgLst.Add(vtArt.getBitmap(vtArt.Build))
        self.SetImageList(self.imgLst,wx.IMAGE_LIST_NORMAL)
        self.SetImageList(self.imgLst,wx.IMAGE_LIST_SMALL)
        self.selIdx=-1
        self.lPos=[]
        self.dPos={}
        
        self.bViewed=False
        self.bProcessed=False
        self.dt=vtDateTime(True)
        self.dNet={}
        self.lNet=[]
        self.thdBuild=vtThread(self)
        
        self.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstMsgListColClick,self)
        self.Bind(wx.EVT_LIST_ITEM_DESELECTED,self.OnLstMsgListItemDeselected,self)
        self.Bind(wx.EVT_LIST_ITEM_SELECTED,self.OnLstMsgListItemSelected,self)
        self.Bind(wx.EVT_LEFT_DCLICK, self.OnLstMsgLeftDclick,self)
    def Destroy(self):
        for d in self.dNet.values():
            if 'consumer' in d:
                d['consumer'].SetDoc(None)
        del self.dNet
        del self.lNet
        wx.ListCtrl.Destroy(self)
    def SetShowViewed(self,flag):
        self.bViewed=flag
    def SetShowProcessed(self,flag):
        self.bProcessed=flag
    def OnLstMsgListColClick(self, event):
        self.selIdx=-1
        event.Skip()
    def OnLstMsgListItemDeselected(self, event):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            self.selIdx=-1
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()
    def GetNetDoc(self,appl,alias):
        if (appl,alias) in self.dNet:
            return self.dNet[(appl,alias)]['doc']
    def OnLstMsgListItemSelected(self, event):
        try:
            self.selIdx=event.GetIndex()
            id=self.GetItemData(self.selIdx)
            appl=self.GetItem(self.selIdx,6).m_text
            alias=self.GetItem(self.selIdx,7).m_text
            #netDoc=self.dNet[(appl,alias)]
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'idx:%d,appl:%s;alias:%s;id:%s'%\
                            (self.selIdx,appl,alias,id),self)
            #tmp=self.doc.getNodeByIdNum(id)
            #self.pnMsg.SetNode(tmp)
            wx.PostEvent(self,vtXmlMsgListView(self,appl,alias,id))
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()
    def OnLstMsgLeftDclick(self, event):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'idx:%d'%(self.selIdx),self)
            if self.selIdx>=0:
                id=self.GetItemData(self.selIdx)
                appl=self.GetItem(self.selIdx,6).m_text
                alias=self.GetItem(self.selIdx,7).m_text
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'idx:%d,appl:%s;alias:%s;id:%s'%\
                                (self.selIdx,appl,alias,id),self)
                dNetDoc=self.dNet[(appl,alias)]
                netDoc=dNetDoc['doc']
                tmp=netDoc.getNodeByIdNum(id)
                fid=netDoc.getAttribute(tmp,'fid')
                
                par=netDoc.getParent(tmp)
                sAlias=netDoc.getAttribute(par,'alias')
                appl,alias='',''
                if len(sAlias)>0:
                    strs=sAlias.split(':')
                    if len(strs)==2:
                        appl=strs[0]
                        alias=strs[1]
                        #idNav=netDoc.getAttribute(self.node,'fid')
                        wx.PostEvent(self,vtXmlMsgListGoto(self,appl,alias,fid))
                else:
                    vtLog.vtLngCurWX(vtLog.ERROR,'no alias defined',self)
                #tmp=self.doc.getNodeByIdNum(id)
                #fid=self.doc.getAttribute(tmp,'fid')
                #self.doc.SelectById(fid)
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()
    def Clear(self):
        self.lPos=[]
        self.dPos={}
        self.DeleteAllItems()
    def IsBusy(self):
        return self.thdBuild.IsRunning()
    def Restart(self):
        self.thdBuild.Start()
    def Stop(self):
        self.thdBuild.Stop()
    def OnAddNode(self,evt):
        evt.Skip()
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
        try:
            netDoc=evt.GetNetDoc()
            appl,alias=netDoc.GetAppl(),netDoc.GetAlias()
            oLogin=netDoc.GetLogin()
            oMsg=netDoc.GetReg('msg')
            id=evt.GetID()
            id=evt.GetNewID()
            node=netDoc.getNodeById(id)
            self.VERBOSE=1
            self.__checkAndAddMsg__(node,netDoc,appl,alias,
                            oLogin,oMsg,bMutexWX=False)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSetNode(self,evt):
        evt.Skip()
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
        try:
            netDoc=evt.GetNetDoc()
            id=evt.GetNewID()
            idNew=evt.GetNewID()
            self.__refreshMsg__(netDoc,id,idNew)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnGetNode(self,evt):
        evt.Skip()
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
        try:
            netDoc=evt.GetNetDoc()
            id=evt.GetID()
            self.__refreshMsg__(netDoc,id)
        except:
            vtLog.vtLngTB(self.GetName())
    def __refreshMsg__(self,netDoc,id,idNew=None):
        try:
            node=netDoc.getNodeById(id)
            appl,alias=netDoc.GetAppl(),netDoc.GetAlias()
            oLogin=netDoc.GetLogin()
            oMsg=netDoc.GetReg('msg')
            node=netDoc.getNodeById(id)
            l=self.__checkMsg2Add__(netDoc,appl,alias,oLogin,oMsg,node)
            idx=self.FindItemData(-1,l[7])
            if idx<0:
                return
            f=self.GetItemFont(idx)
            if l[1]=='new':
                f.SetWeight(wx.FONTWEIGHT_BOLD)
            else:
                f.SetWeight(wx.FONTWEIGHT_NORMAL)
            self.SetItemFont(idx,f)
            imgId=0
            if l[1]=='viewed':
                imgId=1
            if l[1]=='processed':
                imgId=2
            self.SetItemImage(idx,imgId)
            if idNew is not None:
                try:
                    id=long(idNew)
                    self.SetItemData(idx,id)
                except:
                    pass
        except:
            vtLog.vtLngTB(self.GetName())
    def __checkMsg2Add__(self,netDoc,appl,alias,oLogin,oMsg,c):
        if self.VERBOSE:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'node:%s'%netDoc.getKey(c),self)
        try:
            #print c
            #print oLogin
            #print oMsg
            netDoc.acquire('dom')
            idRecip=oMsg.GetRecipID(c)
            if idRecip<0:
                netDoc.release('dom')
                return None
            if oLogin.HasId(idRecip):
                l=oMsg.AddValuesToLst(c)
                tag,lHum=oMsg.GetAuthorInfos(c)
                if tag=='group':
                    l+=['grp',_(u'Group')+lHum[0]]
                else:
                    l+=['usr',', '.join(lHum)]
                tag,lHum=oMsg.GetRecipientInfos(c)
                if tag=='group':
                    l+=['grp',_(u'Group')+':'+', '.join(lHum)]
                else:
                    l+=['usr',', '.join(lHum)]
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%l,self)
                netDoc.release('dom')
                return l
        except:
            vtLog.vtLngTB(self.GetName())
        netDoc.release('dom')
        return None
    def __checkAndAddMsg__(self,node,netDoc,appl,alias,oLogin,oMsg,bMutexWX=True):
        try:
            vtLdBd.check()
            if self.VERBOSE:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'appl:%s;alias:%s'%(appl,alias),self)
            l=self.__checkMsg2Add__(netDoc,appl,alias,oLogin,oMsg,node)
            if l is not None:
                #lMsg.append(l)
                imgId=0
                if l[1]=='viewed':
                    if not self.bViewed:
                        return 0
                    imgId=1
                if l[1]=='processed':
                    if not self.bProcessed:
                        return 0
                    imgId=2
                iPrior=long(l[0])
                if iPrior in self.dPos:
                    dPrior=self.dPos[iPrior]
                else:
                    dPrior={}
                    for sStatName in oMsg.STATUS_NAMES:
                        dPrior[sStatName]=0
                    self.dPos[iPrior]=dPrior
                    self.lPos=self.dPos.keys()
                    self.lPos.sort()
                    self.lPos.reverse()
                iPos=0
                for iPr in self.lPos:
                    if iPr>iPrior:
                        iPos+=reduce(lambda x,y:x+y,self.dPos[iPr].values())
                if l[1] in oMsg.STATUS_NAMES:
                    idx=oMsg.STATUS_NAMES.index(l[1])
                    for s in oMsg.STATUS_NAMES[:idx]:
                        iPos+=dPrior[s]
                    dPrior[oMsg.STATUS_NAMES[idx]]+=1
                if self.VERBOSE:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurWX(vtLog.DEBUG,'iPos:%d;iPrior:%d;stat:%s;lPos:%s;dPos:%s'%\
                                (iPos,iPrior,l[1],vtLog.pformat(self.lPos),vtLog.pformat(self.dPos)),self)
                self.thdBuild.CallBack(self._doAdd,appl,alias,iPos,imgId,l)
            if self.thdBuild.Is2Stop():
                return -1
        except:
            vtLog.vtLngTB(self.GetName())
        return 0
    def _doAdd(self,appl,alias,iPos,imgId,l):
        try:
            idx=self.InsertImageStringItem(iPos,l[0],imgId)
            self.SetStringItem(idx,1,l[2])
            self.SetStringItem(idx,2,l[3])
            self.SetStringItem(idx,3,l[9])
            self.SetStringItem(idx,4,l[11])
            self.dt.SetStr(l[5])
            self.SetStringItem(idx,5,self.dt.GetDateTimeStr(' '))
            self.SetItemData(idx,l[7])
            self.SetStringItem(idx,6,appl)
            self.SetStringItem(idx,7,alias)
            if l[1]=='new':
                f=self.GetItemFont(idx)
                f.SetWeight(wx.FONTWEIGHT_BOLD)
                self.SetItemFont(idx,f)
        except:
            vtLog.vtLngTB(self.GetName())
    def __procMsg__(self,node,netDoc,appl,alias,oLogin,oMsg):
        netDoc.procChildsKeys(node,self.__checkAndAddMsg__,netDoc,appl,alias,
                    oLogin,oMsg)
        return 0
    def __procMsgs__(self,node,netDoc,appl,alias,oLogin,oMsg):
        netDoc.procChildsKeys(node,self.__procMsg__,netDoc,appl,alias,
                    oLogin,oMsg)
        wx.PostEvent(self,vtXmlMsgListFinished(self))
        return 0
    def AddMsg(self,appl,alias,netDoc,bNet):
        vtLog.vtLngCurWX(vtLog.INFO,'appl:%s;alias:%s'%(appl,alias),self)
        tAppl=(appl,alias)
        if tAppl not in self.dNet:
            consumer=vtXmlMsgConsumer(self)
            consumer.SetDoc(netDoc,bNet)
            
            self.dNet[tAppl]={'doc':netDoc,'bNet':bNet,'consumer':consumer}
            if bNet:
                EVT_NET_XML_ADD_NODE(netDoc,self.OnAddNode)
                EVT_NET_XML_ADD_NODE_RESPONSE(netDoc,self.OnAddNode)
                EVT_NET_XML_SET_NODE(netDoc,self.OnSetNode)
                EVT_NET_XML_GET_NODE(netDoc,self.OnGetNode)
            self.lNet.append(tAppl)
        netDoc.SetLang(vtLgBase.getPluginLang())
        oMsg=netDoc.GetReg('msg')
        if oMsg is None:
            vtLog.vtLngCurWX(vtLog.ERROR,'node:msg not registered',self)
        oLogin=netDoc.GetLogin()
        if oLogin is None:
            return
        self.thdBuild.Do(self.__procMsgs__,netDoc.getBaseNode(),netDoc,appl,alias,oLogin,oMsg)
    def Update(self):
        self.lPos=[]
        self.dPos={}
        self.DeleteAllItems()
        for tAppl in self.lNet:
            d=self.dNet[tAppl]
            netDoc=d['doc']
            oMsg=netDoc.GetReg('msg')
            oLogin=netDoc.GetLogin()
            if oLogin is not None:
                self.thdBuild.Do(self.__procMsgs__,netDoc.getBaseNode(),netDoc,tAppl[0],tAppl[1],oLogin,oMsg)
