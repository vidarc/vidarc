#----------------------------------------------------------------------------
# Name:         vtXmlDomPersistant.py
# Purpose:      base object to keep pesistant xml dom data
#
# Author:       Walter Obweger
#
# Created:      20080209
# CVS-ID:       $Id: vtXmlDomPersistant.py,v 1.4 2008/12/11 14:50:12 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import codecs
import os

class vtXmlDomPersistant:
    def __init__(self,sPersistant):
        self.fPers=None
        self.sFN=None
        self.sFP=None
        self.sPersistant=sPersistant
        self.enable=True
    def setEnable(self,flag):
        self.enable=flag
    def isEnabled(self):
        return self.enable
    def SetFN(self,fn):
        if self.enable==False:
            return
        if fn is None:
            return
        try:
            sFN='.'.join([fn,self.sPersistant])
            if sFN!=self.sFN:
                self.sFN=sFN
                self.sFP=''
        except:
            pass
    def SetFP(self,fp):
        self.sFP=fp
    def open(self,*args,**kwargs):
        if self.enable==False:
            return
        try:
            self.close()
            if self.enable==False:
                return
            self.fPers=codecs.open(self.sFN, 'r', 'UTF-8')
            s=self.fPers.readline().strip()
            if s==self.sFP:
                self.read(self.fPers,*args,**kwargs)
                self.close()
                return s            # 081207:wro return for debugging
            else:
                self.close()
            #return s                # 081207:wro return for debugging
        except:
            self.close()
        self.build(*args,**kwargs)
        self.save(*args,**kwargs)
    def close(self):
        try:
            if self.fPers is not None:
                self.fPers.close()
        except:
            pass
        self.fPers=None
    def save(self,*args,**kwargs):
        if self.sFN is None:
            return
        try:
            sDNtmp,sFNtmp=os.path.split(self.sFN)
            if len(sDNtmp)>0:
                os.makedirs(sDNtmp)
        except:
            pass
        self.fPers=codecs.open(self.sFN, 'w', 'UTF-8')
        self.fPers.write('%s\n'%self.sFP)
        self.write(self.fPers,*args,**kwargs)
        self.close()
    # +++++++++++++++++++++++++++++++++++++
    # overide this methods
    def read(self,fPers,*args,**kwargs):
        pass
    def write(self,fPers,*args,**kwargs):
        pass
    def build(self,*args,**kwargs):
        pass
