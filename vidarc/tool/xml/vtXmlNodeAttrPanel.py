#----------------------------------------------------------------------
# Name:         vtXmlNodeAttrPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120909
# CVS-ID:       $Id: vtXmlNodeAttrPanel.py,v 1.4 2015/10/20 09:09:14 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import vidarc.tool.lang.vtLgBase as vtLgBase
    from vGuiFrmWX import vGuiFrmWX
    import imgCtr
    from vidarc.tool.xml.vtXmlNodeGuiPanel import vtXmlNodeGuiPanel
    
    from vidarc.tool.lang.vtLgSelector import vtLgSelector
    from vidarc.tool.lang.vtLgSelector import vEVT_VTLANG_SELECTION_CHANGED
    
    from vidarc.tool.input.vtInputTreeAttr import vtInputTreeAttr
    
    from vidarc.ext.report.veRepBrowseDialog import veRepBrowseDialog
    import vidarc.ext.report.images as imgRep
    import vidarc.tool.art.vtArt as vtArt
    
    VERBOSE=vpc.getVerboseOrigin(__name__)
    
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vtXmlNodeAttrPanel(vtXmlNodeGuiPanel):
    VERBOSE=0
    TAGNAME='description'
    FLEX_SIZER=(3,2)
    ATTR_SEP='_'
    def __initCtrl__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            
        except:
            self.__logTB__()
        global _
        _=vtLgBase.assignPluginLang('vidarc.')
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        #self.SetVerbose(__name__,iVerbose=-1,kwargs=kwargs)
        
        lWid=[
            ('szBoxHor',           1,4,{'name':'boxRef',},[
                ('lstCtrl',        1,4,{'name':'lstAttr','cols':[
                        [_(u''),-3,20,0],
                        [_(u'attribute'),-1,80,1],
                        [_(u'value'),-1,-1,1],
                    ]},{
                    'sel':self.OnLstNodeAttrSel,
                    }),
                ('szBoxVert',       0,0,        {'name':'boxRef',},[
                    (vtLgSelector,  0,20,       {'name':'viLang',},None),
                    ('cbBmp',       0,4,        {'name':'cbNav','size':(24,24),
                                                'bitmap':'Navigate',
                                                },{
                                                'btn':self.OnNavigate}),
                    ('cbBmp',       0,4,        {'name':'cbBws','size':(24,24),
                                                'bitmap':'cmd.FdrClsd',
                                                },{
                                                'btn':self.OnBrowse}),
                    ('cbBmp',       0,4,        {'name':'cbOpn','size':(24,24),
                                                'bitmap':'Open',
                                                },{
                                                'btn':self.OnOpn}),
                    ]),
            ]),
                    ('szBoxVert',           0,4,{'name':'boxCmd',},[
                        ('cbBmp',           0,4,        {'name':'cbApply','size':(32,32),
                                                        'bitmap':'Apply',},
                                                        {'btn':self.OnApply}),
                        ('cbBmp',           0,4,        {'name':'cbCancel','size':(32,32),
                                                        'bitmap':'Cancel',},
                                                        {'btn':self.OnCancel}),
                        ]
                    ),
            ('szBoxHor',            1,4,        {'name':'boxAttr',},[
                ('chcCtrl',         1,4,        {'name':'txtAttrMain',
                                                'map':[('name',0),('attr',1),],
                                                'cols':[(u'name',-1,-1,1),(u'attr',-2,60,0)],
                                                'mark':False,
                                                'mod':False,
                                                },{
                                                'ok':self.OnAttrSel,
                                                }),
                ('txt',             0,4,        {'name':'txtAttrTxt','mark':False,
                                                'mod':False,},None),
                ('txt',             0,4,        {'name':'txtAttrSub','mark':False,
                                                'mod':False,},None),
                ('cbBmp',           0,4,        {'name':'cbAttrClr','size':(24,24),
                                                'bitmap':'cmd.Clr',
                                                },{
                                                'btn':self.OnAttrClr}),
                ('szBoxVert',       0,0,        {'name':'boxRef',},[
                    ('cbBmp',       0,4,        {'name':'cbCfg','size':(24,24),
                                                'bitmap':'cmd.Config',
                                                },{
                                                'btn':self.OnAttrCfg}),
                    ]),
            ]),
            (None,          (0,0),16,None,None),
            ('szBoxHor',            1,4,        {'name':'boxDat',},[
                ('txt',             2,4,        {'name':'txtVal','mark':False,
                                                'mod':False,},None),
                ('szBoxVert',       0,0,        {'name':'boxRef',},[
                    ('cbBmp',       0,4,        {'name':'cbApplyVal','size':(24,24),
                                                'bitmap':'cmd.Add',
                                                },{
                                                'btn':self.OnApplyVal}),
                    ]),
            ]),
            (None,          (0,0),16,None,None),
            
            ]
        
        lWidExt=kwargs.get('lWid',[])
        
        vtXmlNodeGuiPanel.__initCtrl__(self,lWid=lWid)
        
        self.viLang.Bind(vEVT_VTLANG_SELECTION_CHANGED,self.OnViLangChanged)
        #self.viLangAlt.Bind(vEVT_VTLANG_SELECTION_CHANGED,self.OnViLangAltChanged)
        #self.txtRef.BindEvent('cmd',self.OnCmdRef)
        #self.txtAlt.BindEvent('cmd',self.OnCmdAlt)
        
        #self.__setUpShortCut__()
        
        lGrowRow=[(0,1),]
        lGrowCol=[(0,1),]
        return lGrowRow,lGrowCol
    def __initObj__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            
            self.objRegDoc=None
            self.dlgBrowse=None
            self.dCfgAttr={}
        except:
            self.__logTB__()
        
    def __initCls__(self,*args,**kwargs):
        self.SetVerbose(__name__,iVerbose=VERBOSE,kwargs=kwargs)
    def __setUpShortCut__(self):
        lWidTop=[
                        ('szBoxHor',            (0,0),(1,1),{'name':'boxCmd',},[
                        ('cbBmp'    ,0,4,{'name':'cbAddRef','bitmap':imgCtr.getSrc1Bitmap()},
                                {'btn':self.OnLatexAddRef}),
                        ('cbBmp'    ,0,4,{'name':'cbAddAlt','bitmap':imgCtr.getSrc2Bitmap()},
                                {'btn':self.OnLatexAddAlt}),
                        ]),
                        ]
        p=vGuiFrmWX(name='popLatex',parent=self.tgLaTex.GetWid(),kind='popup',
                iArrange=0,widArrange=self.tgLaTex.GetWid(),bAnchor=True,
                lGrowRow=[(0,1),(1,1)],
                lGrowCol=[(0,1),(1,1)],
                kwTopCmd={'name':'pnTopCmd','bFlexGridSizer':-1,'lWid':lWidTop},
                lWid=[
                        ('lstCtrl', (0,0),(2,2),{'name':'lstData','size':(300,200),
                                'multiple_sel':False,
                                'cols':[
                                    #['',-3,20,0],
                                    [_(u'tag'),-1,80,1],
                                    [_(u'name'),-1,-1,1],
                                    ],
                                'map':[('tag',1),('name',2),],
                                'tip':_(u'latex short cut'),
                                },None),
                        ])
        p.BindEvent('ok',self.OnLatexPopupOk)
        p.GetName()
        self.tgLaTex.SetWidPopup(p)
        #self.txtLaTex.SetWidPopup(p)

    def Apply(self):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        self.__logInfo__(''%())
        try:
            if self.node is not None:
                self.GetNode()
                #id=self.viTag.nodeId
                #wx.PostEvent(self,vtXmlTagApply(self,id))
        except:
            self.__logTB__()
    def Cancel(self):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        self.__logInfo__(''%())
        try:
            #if self.node is not None:
            #    sTag=self.doc.getTagName(self.node)
            #    self.__apply2Dependent__(sTag,'Cancel')
            self.SetNode(self.node)
            #id=self.viTag.nodeId
            #wx.PostEvent(self,vtXmlTagCancel(self,id))
        except:
            self.__logTB__()
    def OnApply(self,evt):
        try:
            self.__logDebug__(''%())
            self.Apply()
        except:
            self.__logTB__()
    def OnCancel(self,evt):
        try:
            self.__logDebug__(''%())
            self.Cancel()
        except:
            self.__logTB__()
    def __Clear__(self):
        bDbg=self.GetVerbose(iVerbose=5)
        if bDbg:
            self.__logDebug__(''%())
        self.dCfgAttr={}
        self.txtAttrMain.SetChoice(self.dCfgAttr)
        self.txtAttrMain.SetValue(u'')
        self.txtAttrSub.Clear()
        self.txtVal.Clear()
        
    def SetRegNode(self,obj):
        bDbg=self.GetVerbose(iVerbose=5)
        if bDbg:
            self.__logDebug__(''%())
        vtXmlNodeGuiPanel.SetRegNode(self,obj)
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            # add code here
            self.viLang.SetDoc(doc)
            self.OnViLangChanged(None)
        except:
            self.__logTB__()
    def __SetNode__(self,node):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            # add code here
            lD=[]
            self.dCfgAttr={}
            if node is not None:
                dVal=self.doc.GetAttrValueToDict(node)#,lang=self.lang)
                self.__logDebug__(dVal)
                if dVal is None:
                    l=[]
                else:
                    l=dVal.keys()
                l.sort()
                for sK in l:
                    lD.append([sK,['',sK,dVal[sK]]])
                oReg=self.doc.GetRegByNode(node)
                dCfg=self.doc.GetCfgByNode(node)
                self.__logDebugAdd__('config',dCfg)
                self.dCfgAttr=dCfg
                self.__logDebugAdd__('val',dVal)
                dValue=self.doc.GetNodeDict(node)
                self.__logDebugAdd__('val',dValue)
            lCfg=[]
            for sK,lIt in self.dCfgAttr.iteritems():
                lCfg.append({-1:sK,'name':lIt[0],'attr':sK})
            
            self.lstAttr.SetValue(lD)
            self.txtAttrMain.SetChoice(lCfg)
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            # add code here
            #self.lstAttr.SetValue(lD)
            #sVal=self.txtRef.GetValue()
            #self.doc.setNodeTextLang(self.node,self.TAGNAME,sVal,self.sLangRef)
            #sVal=self.txtAlt.GetValue()
            #self.doc.setNodeTextLang(self.node,self.TAGNAME,sVal,self.sLangAlt)
        except:
            self.__logTB__()
    def __Close__(self):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            # add code here
            #self.txtRef.Close()
            self.viLang.Close()
            #self.txtAttr.__Close__()
        except:
            self.__logTB__()
    def __Cancel__(self):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            # add code here
            #self.txtLaTex.__Close__()
        except:
            self.__logTB__()
    def __Lock__(self,flag):
        bDbg=self.GetVerbose(iVerbose=5)
        if bDbg:
            self.__logDebug__(''%())
        if flag:
            # add code here
            self.cbApply.Enable(False)
            self.cbCancel.Enable(False)
        else:
            # add code here
            self.cbApply.Enable(True)
            self.cbCancel.Enable(True)
    
    def procSetNodeById(self,sId):
        try:
            self.__logDebug__('sId:%s'%(sId))
            if self.doc.IsKeyValid(sId)==False:
                self.doc.CallBack(self.Clear)
                node=None
            else:
                node=self.doc.getNodeByIdNum(sId)
            self.doc.CallBack(self.SetNode,node)
            return
            if self.node is None:
                self.doc.CallBack(self.Clear)
            else:
                self.doc.CallBack(self.SetNode,node)
        except:
            self.__logTB__()
    def SetNodeById(self,sId):
        try:
            self.__logDebug__('sId:%s'%(sId))
            if self.doc is None:
                self.__logError__('doc is None')
                return
            self.doc.DoSafe(self.procSetNodeById,sId)
        except:
            self.__logTB__()
    def OnViLangChanged(self,evt):
        try:
            self.sLang=self.viLang.GetValue()
        except:
            self.__logTB__()
    def OnNavigate(self,evt):
        try:
            if self.doc is None:
                return
            if self.node is None:
                return
            iID=self.doc.getKey(self.node)
            self.__logDebug__({'iID':iID})
            self.Post('nav',iID)
        except:
            self.__logTB__()
    def OnBrowse(self,evt):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            
        except:
            self.__logTB__()
    def OnOpn(self,evt):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            
        except:
            self.__logTB__()
    
    def OnAttrClr(self,evt):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            self.txtAttrTxt.SetValue('')
            self.txtAttrSub.SetValue('')
            #self.txtVal.SetValue('')
        except:
            self.__logTB__()
    def OnLstNodeAttrSel(self,evt):
        try:
            lSel=self.lstAttr.GetSelected([-2,-1,0,1,2])
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebugAdd__('lSel',lSel)
            sMain=''
            sSub=''
            sVal=''
            if len(lSel)>0:
                sK=lSel[0][3]
                sVal=lSel[0][4]
                lK=sK.split(self.ATTR_SEP)
                if len(lK)>0:
                    sMain=lK[0]
                if len(lK)>1:
                    sSub=lK[1]
            self.txtAttrTxt.SetValue(sMain)
            self.txtAttrSub.SetValue(sSub)
            self.txtVal.SetValue(sVal)
        except:
            self.__logTB__()
    def OnAttrSel(self,evt):
        try:
            self.__logDebug__('')
            sRes=evt.GetResult()
            dDat=evt.GetData()
            self.__logDebug__({'sRes':sRes,'dDat':dDat})
            self.txtAttrTxt.SetValue(dDat[0])
        except:
            self.__logTB__()
    def OnAttrCfg(self,evt):
        try:
            self.__logDebug__('')
        except:
            self.__logTB__()
    def OnApplyVal(self,evt):
        try:
            t=self.lstAttr.GetSelected([-1])
            self.__logDebug__(t)
            lAttrMainRaw=self.txtAttrMain.GetValue()
            sAttrMainRaw=self.txtAttrTxt.GetValue()
            sAttrSubRaw=self.txtAttrSub.GetValue()
            self.__logDebug__({
                    'lAttrMainRaw':lAttrMainRaw,
                    'sAttrMainRaw':sAttrMainRaw,
                    'sAttrSubRaw':sAttrSubRaw,
                    })
            #sAttrMainRaw=lAttrMainRaw[0].encode('ascii')
            sAttrMainRaw=sAttrMainRaw.encode('ascii')
            sAttrSubRaw=sAttrSubRaw.encode('ascii')
            self.__logDebug__({
                    'sAttrMainRaw':sAttrMainRaw,
                    'sAttrSubRaw':sAttrSubRaw,
                    })
            sAttrMain=vpc.vcOsGetXml(sAttrMainRaw,1)
            sAttrSub=vpc.vcOsGetXml(sAttrSubRaw,1)
            if len(sAttrSub)>0:
                sK=self.ATTR_SEP.join([sAttrMain,sAttrSub])
            else:
                sK=sAttrMain
            sVal=self.txtVal.GetValue()
            self.__logDebug__({
                    'sAttrMain':sAttrMain,
                    'sAttrSub':sAttrSub,
                    'sAttrMainRaw':sAttrMainRaw,
                    'sAttrSubRaw':sAttrSubRaw,
                    'sK':sK,
                    'sVal':sVal,
                    })
            lT=self.lstAttr.GetValue([-3,-2,-1,0],0)
            print lT
            lFind=self.lstAttr.FindValue([sK],lMapChk=[-1],lMapGet=[-3,-2,-1,0,])
            self.__logDebug__({'lFind':lFind})
            if len(lFind)>0:
                self.lstAttr.SetValue([[sK,['',sK,sVal]]],lFind[0][1])
            else:
                self.lstAttr.Insert([[sK,['',sK,sVal]]])
        except:
            self.__logTB__()
