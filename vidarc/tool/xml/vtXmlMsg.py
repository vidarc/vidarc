#----------------------------------------------------------------------------
# Name:         vtXmlMsg.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060703
# CVS-ID:       $Id: vtXmlMsg.py,v 1.10 2007/11/18 13:35:57 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog

from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vidarc.tool.xml.vtXmlNodeMsgRoot import vtXmlNodeMsgRoot
from vidarc.tool.xml.vtXmlNodeMessagesAlias import vtXmlNodeMessagesAlias
from vidarc.tool.xml.vtXmlNodeMsg import vtXmlNodeMsg

class vtXmlMsg(vtXmlDomReg):
    TAGNAME_REFERENCE='tag'
    TAGNAME_ROOT='messagesRoot'
    APPL_REF=['vHum']
    def __init__(self,attr='id',skip=[],synch=False,appl='vMsg',verbose=0,
                    audit_trail=True):
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        vtXmlDomReg.__init__(self,attr=attr,skip=skip,synch=synch,
                        appl=appl,verbose=verbose,audit_trail=audit_trail)
        try:
            oRoot=vtXmlNodeMsgRoot(tagName='root')
            self.RegisterNode(oRoot,False)
            oRoot=vtXmlNodeMsgRoot()
            oMsgs=vtXmlNodeMessagesAlias()
            oMsg=vtXmlNodeMsg()
            self.RegisterNode(oRoot,True)
            self.RegisterNode(oMsgs,False)
            self.RegisterNode(oMsg,False)
            self.LinkRegisteredNode(oMsgs,oMsg)
        except:
            vtLog.vtLngTB(self.appl)
        self.SetDftLanguages()
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'MsgsRoot')
    #def New(self,revision='1.0',root='messagesRoot',bConnection=False):
    #    if vtLog.vtLngIsLogged(vtLog.INFO):
    #        vtLog.vtLngCallStack(None,vtLog.INFO,'New',origin=self.appl)
    #    vtXmlDomReg.New(self,revision,root)
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elem=self.createSubNode(self.getRoot(),'MsgsRoot')
        if bConnection==False:
            self.CreateReq()
        #self.createSubNodeTextAttr(elem,'cfg','','id','')
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        #elem=self.createSubNode(self.getRoot(),'security_acl')
        #self.AlignDoc()
    def __procAlias__(self,node,applAlias,nodeApplAlias):
        if self.getTagName(node)=='messages':
            sAlias=self.getAttribute(node,'alias')
            if sAlias==applAlias:
                nodeApplAlias.append(node)
                return -1
        return 0
    def __setApplAlias__(self,oMsgs,c,applAlias):
        oMsgs.SetAlias(c,applAlias)
    def GetMessagesApplAlias(self,applAlias,node=None):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'alias:%s'%(applAlias),self.appl)
        nodeApplAlias=[]
        if node is None:
            node=self.getBaseNode()
        self.procChildsExt(node,self.__procAlias__,applAlias,nodeApplAlias)
        if len(nodeApplAlias)>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'alias:%s found'%(applAlias),self.appl)
            return nodeApplAlias[0]
        else:
            n=self.getBaseNode()
            oMsgs=self.GetReg('messages')
            c=oMsgs.Create(n,self.__setApplAlias__,applAlias)
            self.AlignNode(c)
            #if vtLog.vtLngIsLogged(vtLog.DEBUG):
            #    vtLog.vtLngCur(vtLog.DEBUG,'alias:%s created;node:%s'%(applAlias,c),self.appl)
            #c=self.createSubNodeAttr(n,'messages','alias',applAlias)
            ##return oMsgs   
            return c        # 070325:wro node is expected
        return None
    def __procAliasLst__(self,node,lApplAlias):
        if self.getTagName(node)=='messages':
            sAlias=self.getAttribute(node,'alias')
            lApplAlias.append(sAlias)
        return 0
    def GetMessagesApplAliasLst(self,node=None):
        lApplAlias=[]
        if node is None:
            node=self.getBaseNode()
        self.procChildsExt(node,self.__procAliasLst__,lApplAlias)
        return lApplAlias
    def IsLoggedInSelf(self,node,usrId):
        try:
            oReg=self.GetRegByNode(node)
            if oReg.GetTagName()=='msg':
                if oReg.GetRecipID(node)==usrId:
                    return True
        except:
            vtLog.vtLngTB(self.GetOrigin())
        return False
