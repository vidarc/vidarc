#Boa:FramePanel:vtXmlNodeRegSelector
#----------------------------------------------------------------------------
# Name:         vtXmlNodeRegSelector.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060516
# CVS-ID:       $Id: vtXmlNodeRegSelector.py,v 1.19 2015/07/26 17:31:43 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel
    
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VTXMLNODEREGSELECTOR, wxID_VTXMLNODEREGSELECTORCBAPPLY, 
 wxID_VTXMLNODEREGSELECTORCBCANCEL, wxID_VTXMLNODEREGSELECTORPNDUMMY, 
] = [wx.NewId() for _init_ctrls in range(4)]

vtEVT_TOOL_XML_REG_SELECTOR_APPLY=wx.NewEventType()
vEVT_TOOL_XML_REG_SELECTOR_APPLY=wx.PyEventBinder(vtEVT_TOOL_XML_REG_SELECTOR_APPLY,1)
def EVT_TOOL_XML_REG_SELECTOR_APPLY(win,func):
    win.Connect(-1,-1,vtEVT_TOOL_XML_REG_SELECTOR_APPLY,func)
def EVT_TOOL_XML_REG_SELECTOR_APPLY_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtEVT_TOOL_XML_REG_SELECTOR_APPLY)
class vtXmlRegSelectorApply(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_TOOL_XML_REG_SELECTOR_START(<widget_name>, xxx)
    """
    def __init__(self,obj,id):
        wx.PyEvent.__init__(self)
        self.obj=obj
        self.id=id
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtEVT_TOOL_XML_REG_SELECTOR_APPLY)
    def GetID(self):
        return self.id
    def GetEventObj(self):
        return self.obj

vtEVT_TOOL_XML_REG_SELECTOR_CANCEL=wx.NewEventType()
vEVT_TOOL_XML_REG_SELECTOR_CANCEL=wx.PyEventBinder(vtEVT_TOOL_XML_REG_SELECTOR_CANCEL,1)
def EVT_TOOL_XML_REG_SELECTOR_CANCEL(win,func):
    win.Connect(-1,-1,vtEVT_TOOL_XML_REG_SELECTOR_CANCEL,func)
def EVT_TOOL_XML_REG_SELECTOR_CANCEL_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtEVT_TOOL_XML_REG_SELECTOR_CANCEL)
class vtXmlRegSelectorCancel(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_TOOL_XML_REG_SELECTOR_CANCEL(<widget_name>, xxx)
    """
    def __init__(self,obj,id):
        wx.PyEvent.__init__(self)
        self.obj=obj
        self.id=id
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtEVT_TOOL_XML_REG_SELECTOR_CANCEL)
    def GetID(self):
        return self.id
    def GetEventObj(self):
        return self.obj

vtEVT_TOOL_XML_REG_SELECTOR_SELECTED=wx.NewEventType()
vEVT_TOOL_XML_REG_SELECTOR_SELECTED=wx.PyEventBinder(vtEVT_TOOL_XML_REG_SELECTOR_SELECTED,1)
def EVT_TOOL_XML_REG_SELECTOR_SELECTED(win,func):
    win.Connect(-1,-1,vtEVT_TOOL_XML_REG_SELECTOR_SELECTED,func)
def EVT_TOOL_XML_REG_SELECTOR_SELECTED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtEVT_TOOL_XML_REG_SELECTOR_SELECTED)
class vtXmlRegSelectorSelected(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_TOOL_XML_REG_SELECTOR_SELECTED(<widget_name>, xxx)
    """
    def __init__(self,obj,id,lWid,oReg):
        wx.PyEvent.__init__(self)
        self.obj=obj
        self.id=id
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtEVT_TOOL_XML_REG_SELECTOR_SELECTED)
        self.lWid=lWid
        self.oReg=oReg
    def GetID(self):
        return self.id
    def GetEventObj(self):
        return self.obj
    def GetListWidget(self):
        return self.lWid
    def GetObjReg(self):
        return self.oReg

class vtXmlNodeRegSelector(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_fgsInfo_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsInfo, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddSizer(self.bxsBt, 0, border=4, flag=wx.LEFT)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 0, border=4, flag=wx.BOTTOM)
        parent.AddWindow(self.cbCancel, 0, border=4, flag=wx.TOP)

    def _init_coll_bxsInfo_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.pnDummy, 1, border=0, flag=wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)
        
        self.bxsInfo = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        self.fgsInfo = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsInfo_Items(self.bxsInfo)
        self._init_coll_fgsInfo_Items(self.fgsInfo)

        self.SetSizer(self.fgsInfo)

    def _init_ctrls(self, prnt,id,style):
        # generated method, don't edit
        wx.Panel.__init__(self, id=id, name=u'vtXmlNodeRegSelector',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(259, 103),
              style=style)
        self.SetClientSize(wx.Size(251, 76))
        self.SetAutoLayout(True)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLNODEREGSELECTORCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'),
              name=u'cbApply', parent=self, pos=wx.Point(175, 0),
              size=wx.Size(76, 30), style=0)
        self.cbApply.SetMinSize((-1,-1))
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VTXMLNODEREGSELECTORCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLNODEREGSELECTORCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(175, 38),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.SetMinSize((-1,-1))
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VTXMLNODEREGSELECTORCBCANCEL)

        self.pnDummy = wx.Panel(id=wxID_VTXMLNODEREGSELECTORPNDUMMY,
              name=u'pnDummy', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(167, 76), style=wx.TAB_TRAVERSAL)
        self.pnDummy.SetMinSize(wx.Size(-1, -1))

        self._init_sizers()

    def __init__(self, parent, id=-1, pos=None, size=None, style=0, name='pnRegSel'):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self.dWidDependent={}
        self.dObjReg={}
        #self.sActTag=None#''
        self.sActTag=''
        #if hasattr(parent,'GetWid'):
        #    w=parent.GetWid()
        #else:
        #    w=parent
        #self._init_ctrls(w,id,style)
        self._init_ctrls(parent,id,style)
        vtXmlNodePanel.__init__(self)
        self.SetName(name)
        if pos is not None:
            self.Move(pos)
        if size is not None:
            self.SetSize(size)
        self.fgsInfo.Layout()
        #self.fgsInfo.FitInside(self)
        #self.Layout()
        #self.Refresh()
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
    def IsDependentAdded(self,obj):
        sTag=obj.GetTagName()
        return self.dWidDependent.has_key(sTag)
    def AddWidgetDependent(self,wid,obj):
        try:
            if self.__isLogDebug__():
                self.__logDebug__('wid:%s;obj:%s'%(wid,obj))
            if self.IsMainThread()==False:
                self.__logCritical__('called by thread'%())
            if obj is None:
                sTag=None
            else:
                sTag=obj.GetTagName()
            #sTag=obj.GetTagName()
            if not self.dWidDependent.has_key(sTag):
                self.dWidDependent[sTag]=[]
            self.dWidDependent[sTag].append(wid)
            #if self.sActTag is None:
            if self.sActTag=='':
                self.pnDummy.Show(False)
                self.sActTag=sTag
                wid.Show(True)
                pass
            else:
                wid.Show(False)
            if hasattr(wid,'GetWid'):
                widTmp=wid.GetWid()
            else:
                widTmp=wid
            self.bxsInfo.AddWindow(widTmp, 1, border=4, flag=wx.EXPAND)
            self.bxsInfo.Layout()
            self.fgsInfo.Layout()
        except:
            self.__logTB__()
    def GetModified(self):
        if vtXmlNodePanel.GetModified(self):
            return True
        if self.sActTag!='':
            lWid=self.dWidDependent[self.sActTag]
            for wid in lWid:
                try:
                    if wid is not None:
                        if wid.GetModified():
                            return True
                except:
                    self.__logTB__()
                    self.__logError__('tag:%s;%s'%(self.sActTag,wid))
        return False
    def __apply2Dependent__(self,sTag,method,*args,**kwargs):
        if not self.dWidDependent.has_key(sTag):
            if None in self.dWidDependent:
                sTag=None
            else:
                return
            #return
        lWid=self.dWidDependent[sTag]
        for wid in lWid:
            try:
                if wid is not None:
                    f=getattr(wid,method)
                    f(*args,**kwargs)
            except:
                self.__logTB__()
                self.__logError__('tag:%s;%s'%(sTag,wid))
    def __chk2apply2Dependent__(self,sTag,method,*args,**kwargs):
        if not self.dWidDependent.has_key(sTag):
            if None in self.dWidDependent:
                sTag=None
            else:
                return
            #return
        lWid=self.dWidDependent[sTag]
        for wid in lWid:
            try:
                if wid is not None:
                    f=getattr(wid,method)
                    f(*args,**kwargs)
            except:
                self.__logTB__()
    def __showDependent__(self,sTag):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        try:
            if self.__isLogDebug__():
                self.__logDebug__('tag:%s;tagAct:%s'%
                            (sTag,self.sActTag))
            #vtLog.CallStack('')
            #print 'tag',sTag,'tagAct',self.sActTag,self.pnDummy.IsShown()
            if sTag not in self.dWidDependent:
                sTag=None
                self.pnDummy.Show(False)
                if sTag not in self.dWidDependent:
                    if self.sActTag!='':
                        lWid=self.dWidDependent[self.sActTag]
                        for wid in lWid:
                            if wid is not None:
                                wid.Show(False)
                    self.sActTag=''
                    self.fgsInfo.Layout()
                    #self.Enable(False)
                    #self.Refresh()
                    wx.PostEvent(self,vtXmlRegSelectorSelected(self,id,None,sTag))
                    #return
            if sTag=='':
                if self.sActTag!=sTag:
                    for lWid in self.dWidDependent.values():
                        for wid in lWid:
                            if wid is not None:
                                wid.Show(False)
                    self.pnDummy.Show(True)
                    self.fgsInfo.Layout()
                    wx.PostEvent(self,vtXmlRegSelectorSelected(self,id,None,sTag))
                self.sActTag=sTag
            else:
                if self.sActTag!=sTag:
                    self.pnDummy.Show(False)
                    if self.sActTag!='':
                        lWid=self.dWidDependent[self.sActTag]
                        for wid in lWid:
                            if wid is not None:
                                wid.Show(False)
                        wx.PostEvent(self,vtXmlRegSelectorSelected(self,id,None,sTag))
                    #else:
                    #    self.Enable(True)
                    #    self.Refresh()
                    try:
                        lWid=self.dWidDependent[sTag]
                        for wid in lWid:
                            if wid is not None:
                                wid.Show(True)
                        wx.PostEvent(self,vtXmlRegSelectorSelected(self,id,lWid,sTag))
                    except:
                        sTag=''
                        self.pnDummy.Show(True)
                        self.sActTag=sTag
                    self.sActTag=sTag
            self.bxsInfo.Layout()
            self.fgsInfo.Layout()
            #self.FitInside()
            #self.fgsInfo.FitInside(self)
            #print 'tag',sTag,'tagAct',self.sActTag,self.pnDummy.IsShown()
            #print self.GetSize()
        except:
            self.__logTB__()
    def SetRegNode(self,obj,**kwargs):#bIncludeBase=True):
        sTag=obj.GetTagName()
        self.dObjReg[sTag]=obj
        self.__apply2Dependent__(sTag,'SetRegNode',obj,**kwargs)#bIncludeBase=bIncludeBase)
        if VERBOSE>0:
            self.__logDebug__('%s'%(self.__logFmt__(self.dObjReg)))
    def GetDependentWid(self,sTag):
        try:
            self.__logDebug__({'sTag':sTag,'dWidDependent':self.dWidDependent})
            if sTag in self.dWidDependent:
                return self.dWidDependent[sTag]
            return None
        except:
            self.__logTB__()
    def GetDependentReg(self,sTag):
        try:
            if sTag not in self.dObjReg:
                return self.dObjReg[sTag]
            return None
        except:
            self.__logTB__()
    def SetNetDocs(self,d):
        for sTag in self.dObjReg.keys():
            #self.__apply2Dependent__(k,'SetNetDocs',d)
            lWid=self.dWidDependent[sTag]
            for wid in lWid:
                try:
                    if wid is not None:
                        if hasattr(wid,'__SetDoc__'):
                            pass
                        else:
                            self.__logCritical__('%s;change to __SetDoc__'%(wid.__class__.__name__))
                            f=getattr(wid,'SetNetDocs')
                            f(d)
                except:
                    self.__logTB__()
                    self.__logError__('tag:%s;%s'%(sTag,wid))
    def SetNetMaster(self,netMaster):
        for k in self.dObjReg.keys():
            self.__apply2Dependent__(k,'SetNetMaster',netMaster)
    def SetDoc(self,doc,bNet=False):
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        for k in self.dObjReg.keys():
            self.__apply2Dependent__(k,'SetDoc',doc,bNet)
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        for sTag in self.dObjReg.keys():
            #self.__apply2Dependent__(k,'__SetDoc__',doc,bNet,dDocs)
            lWid=self.dWidDependent[sTag]
            for wid in lWid:
                try:
                    if wid is not None:
                        if hasattr(wid,'__SetDoc__'):
                            f=getattr(wid,'__SetDoc__')
                            f(doc,bNet=bNet,dDocs=dDocs)
                        else:
                            self.__logCritical__('%s;change to __SetDoc__'%(wid.__class__.__name__))
                            f=getattr(wid,'SetDoc')
                            f(doc,bNet=bNet,dDocs=dDocs)
                except:
                    self.__logTB__()
                    self.__logError__('tag:%s;%s'%(sTag,wid))
    def __Clear__(self):
        return
        keys=self.dWidDependent.keys()
        for k in keys:
            self.__apply2Dependent__(k,'__Clear__')
        return
        if self.sActTag!='':
            lWid=self.dWidDependent[self.sActTag]
            for wid in lWid:
                try:
                    if wid is not None:
                        wid.__Clear__()
                except:
                    self.__logTB__()
                    self.__logError__('tag:%s;%s'%(self.sActTag,wid))
    def __Close__(self):
        pass
        #keys=self.dWidDependent.keys()
        #for k in keys:
        #    self.__apply2Dependent__(k,'__Close__')
    #def __Lock__(self,bFlag):
    #    keys=self.dWidDependent.keys()
    #    for k in keys:
    #        self.__apply2Dependent__(k,'__Lock__',bFlag)
    def __SetNode__(self,node,*args,**kwargs):
        try:
            if node is not None:
                sTag=self.doc.getTagName(node)
                self.__showDependent__(sTag)
                self.__apply2Dependent__(sTag,'SetNode',node)
            else:
                self.__showDependent__('')
        except:
            self.__logTB__()
        #if wx.Thread_IsMain()==False:
        #    vtLog.vtLngCurCls(vtLog.CRITICAL,'called by thread'%(),self)
    def SetModified(self,bFlag):
        try:
            self.__logDebug__('bFlag:%d'%(bFlag))
            vtXmlNodePanel.SetModified(self,bFlag)
            self.__logDebug__(self.dWidDependent.keys())
            for k in self.dWidDependent.keys():
                self.__logDebug__('k:%s;bFlag:%d'%(k,bFlag))
                self.__apply2Dependent__(k,'SetModified',bFlag)
        except:
            self.__logTB__()
    def SetNode(self,node,*args,**kwargs):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        try:
            if VERBOSE or self.VERBOSE:
                self.__logDebug__(''%())
            if self.node is not None:
                sTagOld=self.doc.getTagName(self.node)
                sTag=self.doc.getTagName(node)
                if sTagOld!=sTag:
                    self.__apply2Dependent__(sTagOld,'SetNode',None)
            iRet=self.__SetNodeCheckPerm__(node)
            self.__logDebug__('iRet:%d'%(iRet))
            if iRet<0:
                return iRet
            self.__SetNode__(node,*args,**kwargs)
            return iRet
        except:
            self.__logTB__()
        return -9
                
        if self.doc is None:
            self.Clear()
            self.Lock(True)
            return
        if self.node is not None:
            if self.__isLogDebug__():
                self.__logDebug__('old id:%s'%(self.doc.getKey(self.node)))
        else:
            if self.__isLogDebug__():
                self.__logDebug__('node==None')
        if self.node is not None:
            sTagOld=self.doc.getTagName(self.node)
            sTag=self.doc.getTagName(node)
            if sTagOld!=sTag:
                self.__apply2Dependent__(sTagOld,'SetNode',None)
        if node is not None:
            if self.__isLogDebug__():
                self.__logDebug__('id:%s'%(self.doc.getKey(node)))
        else:
            if self.__isLogDebug__():
                self.__logDebug__('node==None')
        vtXmlNodePanel.SetNode(self,node)
        #if node is not None:
        #    sTag=self.doc.getTagName(node)
        #    self.__showDependent__(sTag)
        #    self.__apply2Dependent__(sTag,'SetNode',node)
        #else:
        #    self.__showDependent__('')
    def GetNode(self,node=None):
        if node is not None:
            if self.__isLogDebug__():
                self.__logDebug__('id:%s'%(self.doc.getKey(node)))
        else:
            if self.__isLogDebug__():
                self.__logDebug__('node==None')
        tmp=self.GetNodeStart(node)
        if tmp is not None:
            if self.__isLogDebug__():
                self.__logDebug__('id:%s'%(self.doc.getKey(tmp)))
        else:
            if self.__isLogDebug__():
                self.__logDebug__('node==None')
        if tmp is not None:
            sTag=self.doc.getTagName(tmp)
            self.__apply2Dependent__(sTag,'GetNode',tmp)
        else:
            for k in self.dObjReg.keys():
                self.__apply2Dependent__(k,'Clear')
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            self.cbApply.Enable(False)
            #self.cbCancel.Enable(False)
        else:
            self.cbApply.Enable(True)
            #self.cbCancel.Enable(True)
    def OnCbApplyButton(self, event):
        self.__logInfo__(''%())
        self.GetNode()
        id=self.nodeId
        wx.PostEvent(self,vtXmlRegSelectorApply(self,id))
        event.Skip()
    def OnCbCancelButton(self, event):
        self.__logInfo__(''%())
        if self.node is not None:
            sTag=self.doc.getTagName(self.node)
            self.__apply2Dependent__(sTag,'Cancel')
        self.SetNode(self.node)
        id=self.nodeId
        wx.PostEvent(self,vtXmlRegSelectorCancel(self,id))
        event.Skip()
    def CreateRegisteredPanel(self,sReg,doc=None,bNet=True,
                    id=-1,pos=wx.DefaultPosition,
                    sz=wx.DefaultSize,style=0,name=None):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        try:
            self.__logInfo__('sReg:%s'%(sReg))
            if doc is None:
                doc=self.doc
            if doc is None:
                self.__logError__('no doc set')
            oReg=doc.GetReg(sReg)
            pnCls=oReg.GetPanelClass()
            if name is None:
                name='pn'+sReg
            pn=pnCls(parent=self,id=id,pos=pos,size=sz,style=style,name=name)
            pn.Show(False)
            pn.SetRegNode(oReg)
            pn.SetDoc(doc,bNet)
            self.AddWidgetDependent(pn,oReg)
            return pn
        except:
            self.__logTB__()
        return None
