#----------------------------------------------------------------------------
# Name:         vtXmlFilterType.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060223
# CVS-ID:       $Id: vtXmlFilterType.py,v 1.11 2007/04/18 15:58:21 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import fnmatch,datetime,time
from vidarc.tool.time.vtTime import vtDateTime
import vidarc.tool.log.vtLog as vtLog

class vtXmlFilterType:
    FILTER_TYPE_STRING      =   0
    FILTER_TYPE_DATE        =   1
    FILTER_TYPE_TIME        =   2
    FILTER_TYPE_DATE_TIME   =   3
    FILTER_TYPE_INT         =   4
    FILTER_TYPE_LONG        =   5
    FILTER_TYPE_FLOAT       =   6
    FILTER_TYPE_DATE_TIME_REL   =   7
    FILTER_TYPES=[FILTER_TYPE_STRING,FILTER_TYPE_INT,FILTER_TYPE_FLOAT,
            FILTER_TYPE_DATE,FILTER_TYPE_TIME,FILTER_TYPE_DATE_TIME,
            FILTER_TYPE_DATE_TIME_REL]
    def __init__(self):
        pass

class vtXmlFilterOperator:
    OP_NOT_EQUAL        = 0
    OP_EQUAL            = 1 
    OP_LESS             = 2
    OP_LESS_EQUAL       = 3
    OP_GREATER_EQUAL    = 4
    OP_GREATER          = 5
    def __init__(self,op=OP_EQUAL):
        self.op=op
    def Start(self):
        pass
    def SetValueStr(self,val):
        if val=='!=':
            self.op=self.OP_NOT_EQUAL
        elif val=='==':
            self.op=self.OP_EQUAL
        elif val=='<':
            self.op=self.OP_LESS
        elif val=='<=':
            self.op=self.OP_LESS_EQUAL
        elif val=='>=':
            self.op=self.OP_GREATER_EQUAL
        elif val=='!=':
            self.op=self.OP_GREATER
        else:
            self.op=self.OP_EQUAL
    def SetValue(self,val):
        if val<0 or val>5:
            return
        self.op=val
    def GetValue(self):
        return self.op
    def GetValueStr(self):
        return self.__repr__()
    def GetStrXml(self):
        if self.op==self.OP_NOT_EQUAL:
            return 'not_equal'
        if self.op==self.OP_EQUAL:
            return 'equal'
        if self.op==self.OP_LESS:
            return 'less'
        if self.op==self.OP_GREATER_EQUAL:
            return 'greater_equal'
        if self.op==self.OP_LESS_EQUAL:
            return 'less_equal'
        if self.op==self.OP_GREATER:
            return 'greater'
        return ''
    def __str__(self):
        if self.op==self.OP_NOT_EQUAL:
            return '!='
        if self.op==self.OP_EQUAL:
            return '=='
        if self.op==self.OP_LESS:
            return '<'
        if self.op==self.OP_GREATER_EQUAL:
            return '>='
        if self.op==self.OP_LESS_EQUAL:
            return '<='
        if self.op==self.OP_GREATER:
            return '>'
        return '??'
    def __repr__(self):
        if self.op==self.OP_NOT_EQUAL:
            return '!='
        if self.op==self.OP_EQUAL:
            return '=='
        if self.op==self.OP_LESS:
            return '<'
        if self.op==self.OP_LESS_EQUAL:
            return '<='
        if self.op==self.OP_GREATER_EQUAL:
            return '>='
        if self.op==self.OP_GREATER:
            return '>'
        return '??'

class vtXmlFilterString(vtXmlFilterOperator):
    def __init__(self,flt='*',op=vtXmlFilterOperator.OP_EQUAL,bCaseSensitive=True):
        vtXmlFilterOperator.__init__(self,op)
        self.bCaseSensitive=bCaseSensitive
        self.SetValue(flt)
    def SetValue(self,flt):
        if self.bCaseSensitive==False:
            self.flt=flt.lower()
        else:
            self.flt=flt
        #self.op=op
        self.bWildCard=False
        if flt.find('*')>=0:
            self.bWildCard=True
        elif flt.find('?')>=0:
            self.bWildCard=True
    def GetValueStr(self):
        return self.flt
    def GetValue(self):
        return self.flt
    def GetOp(self):
        return self.op
    def GetOpStr(self):
        return vtXmlFilterOperator.__repr__(self)
    def GetSensitive(self):
        return self.bCaseSensitive
    def GetSensitiveStr(self):
        if self.bCaseSensitive:
            return '1'
        else:
            return '0'
    def GetNode(self,doc,node):
        doc.setNodeText(node,'cmp',self.GetValue())
        doc.setNodeText(node,'op',vtXmlFilterOperator.GetStrXml(self))
        doc.setNodeText(node,'sensitive',self.GetSensitiveStr())
    def match(self,val):
        if self.bCaseSensitive==False:
            val=val.lower()
        if self.bWildCard:
            if self.bCaseSensitive:
                bRet=fnmatch.fnmatchcase(val,self.flt)
            else:
                bRet=fnmatch.fnmatch(val,self.flt)
            if self.op==vtXmlFilterOperator.OP_NOT_EQUAL:
                return not bRet
            elif self.op==vtXmlFilterOperator.OP_EQUAL:
                return bRet
            return False
        if self.op==vtXmlFilterOperator.OP_NOT_EQUAL:
            return self.flt!=val
        elif self.op==vtXmlFilterOperator.OP_EQUAL:
            return self.flt==val
        elif self.op==vtXmlFilterOperator.OP_LESS:
            return val<self.flt
        elif self.op==vtXmlFilterOperator.OP_LESS_EQUAL:
            return val<=self.flt
        elif self.op==vtXmlFilterOperator.OP_GREATER:
            return val>self.flt
        elif self.op==vtXmlFilterOperator.OP_GREATER_EQUAL:
            return val>=self.flt
        return False
    def __str__(self):
        return 'filter:%s op:%s case:%d'%(self.flt,self.GetOpStr(),self.bCaseSensitive)
    def __repr__(self):
        return 'filter:%s op:%s case:%d'%(self.flt,self.GetOpStr(),self.bCaseSensitive)

class vtXmlFilterInt(vtXmlFilterOperator):
    def __init__(self,flt=0,op=vtXmlFilterOperator.OP_EQUAL):
        self.SetValue(flt)
        #self.op=op
        vtXmlFilterOperator.__init__(self,op)
    def SetValue(self,flt):
        self.flt=int(flt)
    def GetValueStr(self):
        return str(self.flt)
    def GetValue(self):
        return self.flt
    def GetOp(self):
        return self.op
    def GetOpStr(self):
        return vtXmlFilterOperator.__repr__(self)
    def GetNode(self,doc,node):
        doc.setNodeText(node,'cmp',self.GetValueStr())
        doc.setNodeText(node,'op',vtXmlFilterOperator.GetStrXml(self))
    def match(self,val):
        return self.__match__(int(val))
    def __match__(self,val):
        if self.op==vtXmlFilterOperator.OP_NOT_EQUAL:
            return self.flt!=val
        elif self.op==vtXmlFilterOperator.OP_EQUAL:
            return self.flt==val
        elif self.op==vtXmlFilterOperator.OP_LESS:
            return val<self.flt
        elif self.op==vtXmlFilterOperator.OP_LESS_EQUAL:
            return val<=self.flt
        elif self.op==vtXmlFilterOperator.OP_GREATER:
            return val>self.flt
        elif self.op==vtXmlFilterOperator.OP_GREATER_EQUAL:
            return val>=self.flt
        return False
    def __str__(self):
        return 'filter:%d op:%s'%(self.flt,self.GetOpStr())
    def __repr__(self):
        return 'filter:%d op:%s'%(self.flt,self.GetOpStr())

class vtXmlFilterLong(vtXmlFilterInt):
    def __init__(self,flt=0,op=vtXmlFilterOperator.OP_EQUAL):
        self.SetValue(flt)
        #self.op=op
        vtXmlFilterOperator.__init__(self,op)
    def SetValue(self,flt):
        self.flt=long(flt)
    def match(self,val):
        return self.__match__(long(val))
    def __str__(self):
        return 'filter:%d op:%s'%(self.flt,self.GetOpStr())
    def __repr__(self):
        return 'filter:%d op:%s'%(self.flt,self.GetOpStr())

class vtXmlFilterFloat(vtXmlFilterInt):
    def __init__(self,flt=0,op=vtXmlFilterOperator.OP_EQUAL):
        self.SetValue(flt)
        #self.op=op
        vtXmlFilterOperator.__init__(self,op)
    def SetValue(self,flt):
        self.flt=float(flt)
    def GetValueStr(self,fmt='%4.1f'):
        return fmt%self.flt
    def match(self,val):
        return self.__match__(float(val))
    def __str__(self):
        return 'filter:%f op:%s'%(self.flt,self.GetOpStr())
    def __repr__(self):
        return 'filter:%f op:%s'%(self.flt,self.GetOpStr())

class vtXmlFilterDate(vtXmlFilterString):
    def __init__(self,flt='20060101',op=vtXmlFilterOperator.OP_EQUAL):
        self.SetValue(flt)
        #self.op=op
        vtXmlFilterOperator.__init__(self,op)
    def SetValue(self,flt):
        self.flt=flt
    def GetValueStr(self):
        return self.flt[:4]+'-'+self.flt[4:6]+'-'+self.flt[6:8]
    def GetNode(self,doc,node):
        doc.setNodeText(node,'cmp',self.GetValue())
        doc.setNodeText(node,'op',vtXmlFilterOperator.GetStrXml(self))
    def match(self,val):
        if self.op==vtXmlFilterOperator.OP_NOT_EQUAL:
            return self.flt!=val
        elif self.op==vtXmlFilterOperator.OP_EQUAL:
            return self.flt==val
        elif self.op==vtXmlFilterOperator.OP_LESS:
            return val<self.flt
        elif self.op==vtXmlFilterOperator.OP_LESS_EQUAL:
            return val<=self.flt
        elif self.op==vtXmlFilterOperator.OP_GREATER:
            return val>self.flt
        elif self.op==vtXmlFilterOperator.OP_GREATER_EQUAL:
            return val>=self.flt
        return False
    def __str__(self):
        return 'filter:%s op:%s'%(self.flt,self.GetOpStr())
    def __repr__(self):
        return 'filter:%s op:%s'%(self.flt,self.GetOpStr())

class vtXmlFilterTime(vtXmlFilterDate):
    def __init__(self,flt='120000',op=vtXmlFilterOperator.OP_EQUAL):
        vtXmlFilterDate.__init__(self,flt,op)
    def GetValueStr(self):
        return self.flt[:2]+':'+self.flt[2:4]+':'+self.flt[4:6]
    def matchL(self,val):
        bRet=vtXmlFilterDate.match(self,val)
        print self.flt,val,self.op,bRet
        return bRet
    def __str__(self):
        return 'filter:%s op:%s'%(self.flt,self.GetOpStr())
    def __repr__(self):
        return 'filter:%s op:%s'%(self.flt,self.GetOpStr())
        

class vtXmlFilterDateTime(vtXmlFilterOperator):
    def __init__(self,flt='2006-01-01T12:00:00',op=vtXmlFilterOperator.OP_EQUAL):
        try:
            self.fltDt=vtDateTime()
            self.fltDt.SetStr(flt)
        except:
            self.fltDt=vtDateTime()
        self.flt=time.mktime(self.fltDt.GetUtcTimeTuple())
        self.valDt=vtDateTime()
        #self.op=op
        vtXmlFilterOperator.__init__(self,op)
    def SetValue(self,flt):
        try:
            self.fltDt.SetStr(flt)
        except:
            self.fltDt.Now()
        self.flt=time.mktime(self.fltDt.GetUtcTimeTuple())
    def GetValue(self):
        return self.fltDt.GetDateTimeStr(sep=' ')
    def GetValueStr(self):
        return self.fltDt.GetDateTimeStr(sep=' ')
    #def GetValue(self):
    #    return self.flt
    def GetOp(self):
        return self.op
    def GetOpStr(self):
        return vtXmlFilterOperator.__repr__(self)
    def GetNode(self,doc,node):
        doc.setNodeText(node,'cmp',self.fltDt.GetDateTimeStr())
        doc.setNodeText(node,'op',vtXmlFilterOperator.GetStrXml(self))
    def match(self,val):
        try:
            self.valDt.SetStr(val)
            diff=time.mktime(self.valDt.GetUtcTimeTuple())-self.flt
            if self.op==vtXmlFilterOperator.OP_NOT_EQUAL:
                return diff!=0
            elif self.op==vtXmlFilterOperator.OP_EQUAL:
                return diff==0
            elif self.op==vtXmlFilterOperator.OP_LESS:
                return diff<0
            elif self.op==vtXmlFilterOperator.OP_LESS_EQUAL:
                return diff<=0
            elif self.op==vtXmlFilterOperator.OP_GREATER:
                return diff>0
            elif self.op==vtXmlFilterOperator.OP_GREATER_EQUAL:
                return diff>=0
        except:
            pass
        return False
    def __str__(self):
        return 'filter:%s op:%s'%(self.fltDt.GetDateTimeGMStr(),self.GetOpStr())
    def __repr__(self):
        return 'filter:%s op:%s'%(self.fltDt.GetDateTimeGMStr(),self.GetOpStr())

class vtXmlFilterDateTimeRel(vtXmlFilterOperator):
    def __init__(self,flt='12.00.00',op=vtXmlFilterOperator.OP_EQUAL,bBefore=True):
        self.bBefore=bBefore
        self.actDt=vtDateTime()
        self.valDt=vtDateTime()
        self.SetValue(flt)
        #self.op=op
        vtXmlFilterOperator.__init__(self,op)
    def SetValue(self,flt):
        self.flt=flt
        s=flt.split('.')
        self.iFltDays=0
        self.iFltSeconds=0
        self.iFltMillSeconds=0
        try:
            self.iFltDays=long(s[0])
            self.iFltSeconds=long(s[1])
            self.iFltMillSeconds=long(s[2])
        except:
            pass
    def Start(self):
        self.actDt.Now()
    def GetValue(self):
        return self.flt
    def GetValueStr(self):
        return self.flt
    #def GetValue(self):
    #    return self.flt
    def GetOp(self):
        return self.op
    def GetOpStr(self):
        return vtXmlFilterOperator.__repr__(self)
    def GetBefore(self):
        return self.bBefore
    def GetBeforeStr(self):
        if self.bBefore:
            return '1'
        else:
            return '0'
    def GetNode(self,doc,node):
        doc.setNodeText(node,'cmp',self.GetValue())
        doc.setNodeText(node,'op',vtXmlFilterOperator.GetStrXml(self))
        doc.setNodeText(node,'before',self.GetBeforeStr())
    def match(self,val):
        try:
            try:
                self.valDt.SetStr(val)
            except:
                self.valDt.Now()
                self.valDt.SetDateStr(val)
            if self.bBefore:
                diff=self.actDt.dt-self.valDt.dt
            else:
                diff=self.valDt.dt-self.actDt.dt
            if diff.days<0:
                return False
            if diff.seconds<0:
                return False
            if diff.microseconds<0:
                return False
            
            if self.op==vtXmlFilterOperator.OP_NOT_EQUAL:
                if diff.days!=self.iFltDays:
                    return True
                elif diff.seconds!=self.iFltSeconds:
                    return True
                elif diff.microseconds!=self.iFltMillSeconds:
                    return True
                return False
            elif self.op==vtXmlFilterOperator.OP_EQUAL:
                if diff.days==self.iFltDays:
                    if diff.seconds==self.iFltSeconds:
                        if diff.microseconds==self.iFltMillSeconds:
                            return True
                return False
            elif self.op==vtXmlFilterOperator.OP_LESS:
                if diff.days<self.iFltDays:
                    return True
                elif diff.days==self.iFltDays:
                    if diff.seconds<self.iFltSeconds:
                        return True
                    elif diff.seconds==self.iFltSeconds:
                        if diff.microseconds<self.iFltMillSeconds:
                            return True
                        return False
                return False
            elif self.op==vtXmlFilterOperator.OP_LESS_EQUAL:
                if diff.days>self.iFltDays:
                    return False
                elif diff.days==self.iFltDays:
                    if diff.seconds>self.iFltSeconds:
                        return False
                    elif diff.seconds==self.iFltSeconds:
                        if diff.microseconds>self.iFltMillSeconds:
                            return False
                return True
            elif self.op==vtXmlFilterOperator.OP_GREATER:
                if diff.days>self.iFltDays:
                    return True
                elif diff.days==self.iFltDays:
                    if diff.seconds>self.iFltSeconds:
                        return True
                    elif diff.seconds==self.iFltSeconds:
                        if diff.microseconds>self.iFltMillSeconds:
                            return True
                        return False
                return False
            elif self.op==vtXmlFilterOperator.OP_GREATER_EQUAL:
                if diff.days<self.iFltDays:
                    return False
                elif diff.days==self.iFltDays:
                    if diff.seconds<self.iFltSeconds:
                        return False
                    elif diff.seconds==self.iFltSeconds:
                        if diff.microseconds<self.iFltMillSeconds:
                            return False
                return True
        except:
            if len(val)>0:
                vtLog.vtLngCurCls(vtLog.DEBUG,'val:%s'%(val),self)
                vtLog.vtLngTB(self.__class__.__name__)
            else:
                vtLog.vtLngCurCls(vtLog.WARN,'val:%s'%(val),self)
        return False
    def __str__(self):
        return 'filter:%s op:%s before:%d'%(self.flt,self.GetOpStr(),self.GetBefore())
    def __repr__(self):
        return 'filter:%s op:%s before:%d'%(self.flt,self.GetOpStr(),self.GetBefore())

def vtXmlFilterCreate(iType,**kwargs):
    if iType==vtXmlFilterType.FILTER_TYPE_STRING:
        return vtXmlFilterString(**kwargs)
    elif iType==vtXmlFilterType.FILTER_TYPE_DATE:
        return vtXmlFilterDate(**kwargs)
    elif iType==vtXmlFilterType.FILTER_TYPE_TIME:
        return vtXmlFilterTime(**kwargs)
    elif iType==vtXmlFilterType.FILTER_TYPE_DATE_TIME:
        return vtXmlFilterDateTime(**kwargs)
    elif iType==vtXmlFilterType.FILTER_TYPE_INT:
        return vtXmlFilterInt(**kwargs)
    elif iType==vtXmlFilterType.FILTER_TYPE_LONG:
        return vtXmlFilterLong(**kwargs)
    elif iType==vtXmlFilterType.FILTER_TYPE_FLOAT:
        return vtXmlFilterFloat(**kwargs)
    elif iType==vtXmlFilterType.FILTER_TYPE_DATE_TIME_REL:
        return vtXmlFilterDateTimeRel(**kwargs)
    else:
        return None
    
class vtXmlFilterMapper(vtXmlFilterType):
    def convNothing(s):
        return s
    def convOp(s):
        if s=='equal':
            return vtXmlFilterOperator.OP_EQUAL
        elif s=='not_equal':
            return vtXmlFilterOperator.OP_NOT_EQUAL
        elif s=='less':
            return vtXmlFilterOperator.OP_LESS
        elif s=='less_equal':
            return vtXmlFilterOperator.OP_LESS_EQUAL
        elif s=='greater_equal':
            return vtXmlFilterOperator.OP_GREATER_EQUAL
        elif s=='greater':
            return vtXmlFilterOperator.OP_GREATER
        else:
            return vtXmlFilterOperator.OP_EQUAL
    def convBool(s):
        if s=='1':
            return True
        else:
            return False
    MAP2FLT={   vtXmlFilterType.FILTER_TYPE_STRING          : (vtXmlFilterString        ,[('cmp',convNothing),('op',convOp),('sensitive',convBool)]),
                vtXmlFilterType.FILTER_TYPE_DATE            : (vtXmlFilterDate          ,[('cmp',convNothing),('op',convOp)]) ,
                vtXmlFilterType.FILTER_TYPE_TIME            : (vtXmlFilterTime          ,[('cmp',convNothing),('op',convOp)]),
                vtXmlFilterType.FILTER_TYPE_DATE_TIME       : (vtXmlFilterDateTime      ,[('cmp',convNothing),('op',convOp)]),
                vtXmlFilterType.FILTER_TYPE_INT             : (vtXmlFilterInt           ,[('cmp',int),('op',convOp)]),
                vtXmlFilterType.FILTER_TYPE_LONG            : (vtXmlFilterLong          ,[('cmp',long),('op',convOp)]),
                vtXmlFilterType.FILTER_TYPE_FLOAT           : (vtXmlFilterFloat         ,[('cmp',float),('op',convOp)]),
                vtXmlFilterType.FILTER_TYPE_DATE_TIME_REL   : (vtXmlFilterDateTimeRel   ,[('cmp',convNothing),('op',convOp),('before',convBool)]),
                }
    def __init__(self):
        pass
    def SetNode(self,lFlt,doc,node):
        if doc.getTagName(node) not in ['filter','match']:
            #print 'filter',node
            return None
        sName=doc.getNodeText(node,'tag')
        try:
            for tup in lFlt:
                if tup[0]==sName:
                    tupFlt=self.MAP2FLT[tup[1]]
                    l=[]
                    for tag,convFunc in tupFlt[1]:
                        l.append(convFunc(doc.getNodeText(node,tag)))
                    return (sName,tupFlt[0](*l))
        except:
            vtLog.vtLngTB('filtermapp')
            return None
