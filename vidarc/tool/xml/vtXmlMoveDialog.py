#Boa:Dialog:vXmlRenameDialog

from wxPython.wx import *
from wxPython.lib.buttons import *
from wxPython.wx import wxImageFromStream, wxBitmapFromImage
from wxPython.wx import wxEmptyIcon
import cStringIO


def create(parent):
    return vXmlRenameDialog(parent)

[wxID_VGPXMLDLGRENAME, wxID_VGPXMLDLGRENAMEGCBBCANCEL, 
 wxID_VGPXMLDLGRENAMEGCBBOK, wxID_VGPXMLDLGRENAMELBLCURNAME, 
 wxID_VGPXMLDLGRENAMELBLNEWNAME, wxID_VGPXMLDLGRENAMETXTCURNAME, 
 wxID_VGPXMLDLGRENAMETXTNEWNAME, 
] = map(lambda _init_ctrls: wxNewId(), range(7))

#----------------------------------------------------------------------
def getOkData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00nIDATx\x9c\xa5\x93\xc9\r\xc0 \x0c\x04\x17D\x01\xd0^\n\xa4=\xd2\x01yD\
\x96L\x08\xe0c\xdf\xecx\x0c"\x00\xbd\xc3\x91h)\xe5Z\x90k\x01\x00$m\x91r_M\
\x07\xa02\x15)\xa2\x15Ve1`U\x16\x01\xf8\xde&\xc0)[\xc0n\xf7\x01\xc0\xdf\xd5e\
`\x01E\xe0U\xfcjJ\xf4\'\x03:\xac\xb1\x98.\x91O<M\xff\x05h\x13\xbc\xdf\xf9\
\x01\x97y&\xadH\xfc\xe0%\x00\x00\x00\x00IEND\xaeB`\x82' 

def getOkBitmap():
    return wxBitmapFromImage(getOkImage())

def getOkImage():
    stream = cStringIO.StringIO(getOkData())
    return wxImageFromStream(stream)

#----------------------------------------------------------------------
def getCancelData():
    return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00lIDATx\x9c\xad\x93\xc1\r\xc00\x08\x03M\xb7\xe8\xfe\xc3e\x0c\xfa\x8bB\
bC$\xca\x9b;\x01N\x0cpG\xa3\x9e\x0e\xfc\xaf`\xc00`%\xb0\xf7M\xc1\x0b\x9f\r\
\x19\xbc\xf6\x06A%a\xf0!P\x12\x05\x03\x80\xa9\x18\xf7)\x18L'`\x80\x82S\x01[\
\xe1Z\xb0\xee\\\xa5s\x08\xd8\xc12I\x10d\xd7V\x92\xf0\x12\x15\x9cId\x8c\xb7\
\xd5\xfeL\x1f$\x07+\xb8\xd6Q\x0bp\x00\x00\x00\x00IEND\xaeB`\x82" 

def getCancelBitmap():
    return wxBitmapFromImage(getCancelImage())

def getCancelImage():
    stream = cStringIO.StringIO(getCancelData())
    return wxImageFromStream(stream)


[wxID_VXMLRENAMEDIALOG, wxID_VXMLRENAMEDIALOGGCBBCANCEL, 
 wxID_VXMLRENAMEDIALOGGCBBOK, wxID_VXMLRENAMEDIALOGLBLCURNAME, 
 wxID_VXMLRENAMEDIALOGLBLNEWNAME, wxID_VXMLRENAMEDIALOGTXTCURNAME, 
 wxID_VXMLRENAMEDIALOGTXTNEWNAME, 
] = [wx.NewId() for _init_ctrls in range(7)]

class vXmlRenameDialog(wxDialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wxDialog.__init__(self, id=wxID_VXMLRENAMEDIALOG,
              name=u'vXmlRenameDialog', parent=prnt, pos=wx.Point(334, 226),
              size=wx.Size(226, 136), style=wxDEFAULT_DIALOG_STYLE,
              title=u'vgp xml-Node Rename')
        self.SetClientSize(wx.Size(218, 109))

        self.lblCurName = wxStaticText(id=wxID_VXMLRENAMEDIALOGLBLCURNAME,
              label=u'current name', name=u'lblCurName', parent=self,
              pos=wx.Point(8, 8), size=wx.Size(62, 13), style=0)

        self.txtCurName = wxTextCtrl(id=wxID_VXMLRENAMEDIALOGTXTCURNAME,
              name=u'txtCurName', parent=self, pos=wx.Point(80, 8),
              size=wx.Size(100, 21), style=0, value=u'')
        self.txtCurName.Enable(False)

        self.lblNewName = wxStaticText(id=wxID_VXMLRENAMEDIALOGLBLNEWNAME,
              label=u'new name', name=u'lblNewName', parent=self,
              pos=wx.Point(22, 40), size=wx.Size(49, 13), style=0)

        self.txtNewName = wxTextCtrl(id=wxID_VXMLRENAMEDIALOGTXTNEWNAME,
              name=u'txtNewName', parent=self, pos=wx.Point(80, 40),
              size=wx.Size(100, 21), style=0, value=u'')

        self.gcbbOk = wxGenBitmapTextButton(ID=wxID_VXMLRENAMEDIALOGGCBBOK,
              bitmap=wxBitmap(u'G:/company/python/boa/apps/xmlBase/ok.png',
              wxBITMAP_TYPE_PNG), label=u'Ok', name=u'gcbbOk', parent=self,
              pos=wx.Point(16, 72), size=wx.Size(76, 30), style=0)

        self.gcbbCancel = wxGenBitmapTextButton(ID=wxID_VXMLRENAMEDIALOGGCBBCANCEL,
              bitmap=wxBitmap(u'G:/company/python/boa/apps/xmlBase/ok.png',
              wxBITMAP_TYPE_PNG), label=u'Cancel', name=u'gcbbCancel',
              parent=self, pos=wx.Point(136, 72), size=wx.Size(76, 30),
              style=0)

    def __init__(self, parent):
        self._init_ctrls(parent)

        img=getOkBitmap()
        mask=wxMaskColour(img,wxBLUE)
        img.SetMask(mask)
        self.gcbbOk.SetBitmapLabel(img)
        
        img=getCancelBitmap()
        mask=wxMaskColour(img,wxBLUE)
        img.SetMask(mask)
        self.gcbbCancel.SetBitmapLabel(img)

    def OnGcbbOkButton(self, event):
        self.EndModal(1)
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
    def SetNodeName(self,name):
        self.txtCurName.SetValue(name)
        self.txtNewName.SetValue(name)
    def GetNodeName(self):
        return self.txtNewName.GetValue()
