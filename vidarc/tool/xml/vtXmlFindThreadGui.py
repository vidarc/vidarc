

wxEVT_VTXML_FILTER_TYPE_CHANGED=wx.NewEventType()
vEVT_VTXML_FILTER_TYPE_CHANGED=wx.PyEventBinder(wxEVT_VTXML_FILTER_TYPE_CHANGED,1)
def EVT_VTXML_FILTER_TYPE_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTXML_FILTER_TYPE_CHANGED,func)
class vtXmlFilterTypeChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTXML_FILTER_TYPE_CHANGED(<widget_name>, self.OnInfoApplied)
    """

    def __init__(self,obj,val):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VTXML_FILTER_TYPE_CHANGED)
        self.obj=obj
        self.val=val
    def GetObject(self):
        return self.obj
    def GetValue(self):
        return self.val
