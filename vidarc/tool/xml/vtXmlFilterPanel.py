#Boa:FramePanel:vtXmlFilterPanel
#----------------------------------------------------------------------------
# Name:         vtXmlFilterPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060418
# CVS-ID:       $Id: vtXmlFilterPanel.py,v 1.7 2008/03/26 23:12:28 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.art.vtThrobber
import vidarc.tool.xml.vtXmlFilterTypeInput
from vidarc.tool.xml.vtXmlFilterType import *
from vidarc.tool.xml.vtXmlFindThread import *
import wx.lib.buttons

import sys

from vidarc.tool.xml.vtXmlDomConsumer import *
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_VTXMLFILTERPANEL, wxID_VTXMLFILTERPANELCBADDFLT, 
 wxID_VTXMLFILTERPANELCBAPPLY, wxID_VTXMLFILTERPANELCBCANCEL, 
 wxID_VTXMLFILTERPANELCBDELFLT, wxID_VTXMLFILTERPANELCHCFILTERATTR, 
 wxID_VTXMLFILTERPANELCHCFILTERPOS, wxID_VTXMLFILTERPANELLBLFILTERATTR, 
 wxID_VTXMLFILTERPANELLBLFILTERATTRDESC, wxID_VTXMLFILTERPANELLBLFILTERPOS, 
 wxID_VTXMLFILTERPANELLBLNODENAME, wxID_VTXMLFILTERPANELLSTFILTER, 
 wxID_VTXMLFILTERPANELVIFLTDATE, wxID_VTXMLFILTERPANELVIFLTDATETIME, 
 wxID_VTXMLFILTERPANELVIFLTFLOAT, wxID_VTXMLFILTERPANELVIFLTINT, 
 wxID_VTXMLFILTERPANELVIFLTLONG, wxID_VTXMLFILTERPANELVIFLTSTR, 
 wxID_VTXMLFILTERPANELVIFLTTIME, wxID_VTXMLFILTERPANELVIFLTDATETIMEREL, 
] = [wx.NewId() for _init_ctrls in range(20)]

wxEVT_VTXML_FILTER_PANEL_APPLIED=wx.NewEventType()
vEVT_VTXML_FILTER_PANEL_APPLIED=wx.PyEventBinder(wxEVT_VTXML_FILTER_PANEL_APPLIED,1)
def EVT_VTXML_FILTER_PANEL_APPLIED(win,func):
    win.Connect(-1,-1,wxEVT_VTXML_FILTER_PANEL_APPLIED,func)
class vtXmlFilterPanelApplied(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTXML_FILTER_PANEL_APPLIED(<widget_name>, self.OnInfoApplied)
    """

    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VTXML_FILTER_PANEL_APPLIED)
        self.obj=obj
    def GetObject(self):
        return self.obj

wxEVT_VTXML_FILTER_PANEL_CANCELED=wx.NewEventType()
vEVT_VTXML_FILTER_PANEL_CANCELED=wx.PyEventBinder(wxEVT_VTXML_FILTER_PANEL_CANCELED,1)
def EVT_VTXML_FILTER_PANEL_CANCELED(win,func):
    win.Connect(-1,-1,wxEVT_VTXML_FILTER_PANEL_CANCELED,func)
class vtXmlFilterPanelCanceled(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTXML_FILTER_PANEL_CANCELED(<widget_name>, self.OnInfoCanceled)
    """

    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VTXML_FILTER_PANEL_CANCELED)
        self.obj=obj
    def GetObject(self):
        return self.obj

class vtXmlFilterPanel(wx.Panel,vtXmlDomConsumer):
    def _init_coll_bxsFilterAttr_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblFilterAttr, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.chcFilterAttr, 2, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.lblFilterAttrDesc, 2, border=4,
              flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddSpacer(wx.Size(8, 8), border=32, flag=wx.LEFT)
        parent.AddWindow(self.cbApply, 1, border=8,
              flag=wx.LEFT | wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.cbCancel, 1, border=8,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=32, flag=wx.RIGHT)

    def _init_coll_bxsFilterInput_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viFltStr, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltInt, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltLong, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltFloat, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltDate, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltTime, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltDateTime, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltDateTimeRel, 1, border=0, flag=wx.EXPAND)

    def _init_coll_bxsFilterBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbAddFlt, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.cbDelFlt, 0, border=4, flag=wx.TOP | wx.LEFT)

    def _init_coll_bxsFilterPos_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblFilterPos, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.chcFilterPos, 2, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.lblNodeName, 2, border=4,
              flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fgsDom_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(4)
        parent.AddGrowableCol(0)

    def _init_coll_fgsDom_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsFilterPos, 1, border=8,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddSizer(self.bxsFilterAttr, 1, border=8,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsFilterInput, 1, border=4,
              flag=wx.BOTTOM | wx.EXPAND)
        parent.AddSizer(self.bxsFilterAct, 0, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER)

    def _init_coll_bxsFilterAct_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstFilter, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsFilterBt, 0, border=0, flag=0)

    def _init_coll_lstFilter_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'tagname'), width=-1)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'attribute'), width=-1)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'value'), width=-1)
        parent.InsertColumn(col=3, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'operator'), width=-1)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsDom = wx.FlexGridSizer(cols=1, hgap=0, rows=9, vgap=0)

        self.bxsFilterPos = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsFilterAct = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsFilterBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsFilterInput = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsFilterAttr = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsDom_Items(self.fgsDom)
        self._init_coll_fgsDom_Growables(self.fgsDom)
        self._init_coll_bxsFilterPos_Items(self.bxsFilterPos)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsFilterAct_Items(self.bxsFilterAct)
        self._init_coll_bxsFilterBt_Items(self.bxsFilterBt)
        self._init_coll_bxsFilterInput_Items(self.bxsFilterInput)
        self._init_coll_bxsFilterAttr_Items(self.bxsFilterAttr)

        self.SetSizer(self.fgsDom)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTXMLFILTERPANEL,
              name=u'vtXmlFilterPanel', parent=prnt, pos=wx.Point(472, 348),
              size=wx.Size(246, 223), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(238, 196))

        self.lblFilterPos = wx.StaticText(id=wxID_VTXMLFILTERPANELLBLFILTERPOS,
              label=_(u'tagname'), name=u'lblFilterPos', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(42, 24), style=wx.ALIGN_RIGHT)

        self.chcFilterPos = wx.Choice(choices=[],
              id=wxID_VTXMLFILTERPANELCHCFILTERPOS, name=u'chcFilterPos',
              parent=self, pos=wx.Point(46, 0), size=wx.Size(88, 21),
              style=wx.CB_SORT)
        self.chcFilterPos.Bind(wx.EVT_CHOICE, self.OnChcFilterPosChoice,
              id=wxID_VTXMLFILTERPANELCHCFILTERPOS)

        self.chcFilterAttr = wx.Choice(choices=[],
              id=wxID_VTXMLFILTERPANELCHCFILTERATTR, name=u'chcFilterAttr',
              parent=self, pos=wx.Point(46, 24), size=wx.Size(88, 21), style=0)
        self.chcFilterAttr.Bind(wx.EVT_CHOICE, self.OnChcFilterAttrChoice,
              id=wxID_VTXMLFILTERPANELCHCFILTERATTR)

        self.viFltStr = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterStringInput(id=wxID_VTXMLFILTERPANELVIFLTSTR,
              name=u'viFltStr', parent=self, pos=wx.Point(0, 53),
              size=wx.Size(34, 26), style=0)
        self.viFltStr.Enable(False)

        self.viFltInt = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterIntegerInput(id=wxID_VTXMLFILTERPANELVIFLTINT,
              name=u'viFltInt', parent=self, pos=wx.Point(34, 53),
              size=wx.Size(34, 26), style=0)
        self.viFltInt.Show(False)

        self.viFltLong = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterLongInput(id=wxID_VTXMLFILTERPANELVIFLTLONG,
              name=u'viFltLong', parent=self, pos=wx.Point(68, 53),
              size=wx.Size(34, 26), style=0)
        self.viFltLong.Show(False)

        self.viFltFloat = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterFloatInput(id=wxID_VTXMLFILTERPANELVIFLTFLOAT,
              name=u'viFltFloat', parent=self, pos=wx.Point(102, 53),
              size=wx.Size(34, 26), style=0)
        self.viFltFloat.Show(False)

        self.viFltDate = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterDateInput(id=wxID_VTXMLFILTERPANELVIFLTDATE,
              name=u'viFltDate', parent=self, pos=wx.Point(136, 53),
              size=wx.Size(34, 26), style=0)
        self.viFltDate.Show(False)

        self.viFltTime = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterTimeInput(id=wxID_VTXMLFILTERPANELVIFLTTIME,
              name=u'viFltTime', parent=self, pos=wx.Point(170, 53),
              size=wx.Size(34, 26), style=0)
        self.viFltTime.Show(False)

        self.viFltDateTime = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterDateTimeInput(id=wxID_VTXMLFILTERPANELVIFLTDATETIME,
              name=u'viFltDateTime', parent=self, pos=wx.Point(204, 53),
              size=wx.Size(34, 26), style=0)
        self.viFltDateTime.Show(False)

        self.viFltDateTimeRel = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterDateTimeRelInput(id=wxID_VTXMLFILTERPANELVIFLTDATETIMEREL,
              name=u'viFltDateTimeRel', parent=self, pos=wx.Point(238, 53),
              size=wx.Size(34, 26), style=0)
        self.viFltDateTime.Show(False)

        self.cbAddFlt = wx.lib.buttons.GenBitmapButton(ID=wxID_VTXMLFILTERPANELCBADDFLT,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbAddFlt', parent=self,
              pos=wx.Point(207, 83), size=wx.Size(31, 30), style=0)
        self.cbAddFlt.Enable(False)
        self.cbAddFlt.Bind(wx.EVT_BUTTON, self.OnCbAddFltButton,
              id=wxID_VTXMLFILTERPANELCBADDFLT)

        self.lstFilter = wx.ListCtrl(id=wxID_VTXMLFILTERPANELLSTFILTER,
              name=u'lstFilter', parent=self, pos=wx.Point(0, 83),
              size=wx.Size(203, 77), style=wx.LC_REPORT)
        self._init_coll_lstFilter_Columns(self.lstFilter)
        self.lstFilter.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstFilterListItemSelected,
              id=wxID_VTXMLFILTERPANELLSTFILTER)
        self.lstFilter.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstFilterListItemDeselected,
              id=wxID_VTXMLFILTERPANELLSTFILTER)
        self.lstFilter.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstFilterListColClick,
              id=wxID_VTXMLFILTERPANELLSTFILTER)

        self.cbDelFlt = wx.lib.buttons.GenBitmapButton(ID=wxID_VTXMLFILTERPANELCBDELFLT,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbDelFlt', parent=self,
              pos=wx.Point(207, 117), size=wx.Size(31, 30), style=0)
        self.cbDelFlt.Enable(False)
        self.cbDelFlt.Bind(wx.EVT_BUTTON, self.OnCbDelFltButton,
              id=wxID_VTXMLFILTERPANELCBDELFLT)

        self.lblNodeName = wx.StaticText(id=wxID_VTXMLFILTERPANELLBLNODENAME,
              label=u'', name=u'lblNodeName', parent=self, pos=wx.Point(142, 0),
              size=wx.Size(88, 24), style=0)

        self.lblFilterAttr = wx.StaticText(id=wxID_VTXMLFILTERPANELLBLFILTERATTR,
              label=_(u'attribute'), name=u'lblFilterAttr', parent=self,
              pos=wx.Point(0, 24), size=wx.Size(42, 21), style=wx.ALIGN_RIGHT)

        self.lblFilterAttrDesc = wx.StaticText(id=wxID_VTXMLFILTERPANELLBLFILTERATTRDESC,
              label=u'', name=u'lblFilterAttrDesc', parent=self,
              pos=wx.Point(142, 24), size=wx.Size(88, 21), style=0)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLFILTERPANELCBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=_('Close'), name=u'cbCancel',
              parent=self, pos=wx.Point(127, 167), size=wx.Size(108, 30),
              style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VTXMLFILTERPANELCBCANCEL)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLFILTERPANELCBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Apply'), name=u'cbApply',
              parent=self, pos=wx.Point(3, 167), size=wx.Size(108, 30),
              style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VTXMLFILTERPANELCBAPPLY)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        vtXmlDomConsumer.__init__(self)
        self._init_ctrls(parent)
        
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.cbApply.SetBitmapLabel(vtArt.getBitmap(vtArt.Apply))
        self.cbCancel.SetBitmapLabel(vtArt.getBitmap(vtArt.Cancel))
        self.cbAddFlt.SetBitmapLabel(vtArt.getBitmap(vtArt.Add))
        self.cbDelFlt.SetBitmapLabel(vtArt.getBitmap(vtArt.Del))
        
        self.filters={}
        self.idxSel=-1
        
        self.SetSize(size)
        self.Move(pos)
        self.Clear()
    
    def IsBusy(self):
        return False
    def Stop(self):
        pass

    def OnCbApplyButton(self, event):
        event.Skip()
        wx.PostEvent(self,vtXmlFilterPanelApplied(self))

    def OnCbCancelButton(self, event):
        event.Skip()
        if self.IsBusy():
            return
        wx.PostEvent(self,vtXmlFilterPanelCanceled(self))
        
    def OnChcFilterPosChoice(self, event):
        if event is not None:
            event.Skip()
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        idx=self.chcFilterPos.GetSelection()
        sName=self.chcFilterPos.GetStringSelection()
        self.lblNodeName.SetLabel(self.doc.GetTranslation(sName,None))
        self.chcFilterAttr.Clear()
        lst=self.dFilter[sName]
        if lst is None:
            return
        for tup in lst:
            self.chcFilterAttr.Append(tup[0])
        self.chcFilterAttr.SetSelection(0)
        self.OnChcFilterAttrChoice(None)
        
    def OnChcFilterAttrChoice(self, event):
        if event is not None:
            event.Skip()
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        sTag=self.chcFilterPos.GetStringSelection()
        sName=self.chcFilterAttr.GetStringSelection()
        self.lblFilterAttrDesc.SetLabel(self.doc.GetTranslation(sTag,sName))
        try:
            idx=self.chcFilterAttr.GetSelection()
            tup=self.dFilter[self.chcFilterPos.GetStringSelection()][idx]
            self.viFltStr.Show(False)
            self.viFltDate.Show(False)
            self.viFltTime.Show(False)
            self.viFltDateTime.Show(False)
            self.viFltDateTimeRel.Show(False)
            self.viFltInt.Show(False)
            self.viFltLong.Show(False)
            self.viFltFloat.Show(False)
            self.viFltStr.Enable(False)
            self.viFltDate.Enable(False)
            self.viFltTime.Enable(False)
            self.viFltInt.Enable(False)
            self.viFltLong.Enable(False)
            self.viFltFloat.Enable(False)
            #self.cbAddFlt.Enable(False)
            if tup[1]==vtXmlFilterType.FILTER_TYPE_STRING:
                self.viFltStr.Show(True)
                self.viFltStr.Enable(True)
                self.cbAddFlt.Enable(True)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_DATE:
                self.viFltDate.Show(True)
                self.viFltDate.Enable(True)
                self.cbAddFlt.Enable(True)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_TIME:
                self.viFltTime.Show(True)
                self.viFltTime.Enable(True)
                self.cbAddFlt.Enable(True)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_DATE_TIME:
                self.viFltDateTime.Show(True)
                self.viFltDateTime.Enable(True)
                self.cbAddFlt.Enable(True)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_DATE_TIME_REL:
                self.viFltDateTimeRel.Show(True)
                self.viFltDateTimeRel.Enable(True)
                self.cbAddFlt.Enable(True)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_INT:
                self.viFltInt.Show(True)
                self.viFltInt.Enable(True)
                self.cbAddFlt.Enable(True)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_LONG:
                self.viFltLong.Show(True)
                self.viFltLong.Enable(True)
                self.cbAddFlt.Enable(True)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_FLOAT:
                self.viFltFloat.Show(True)
                self.viFltFloat.Enable(True)
                self.cbAddFlt.Enable(True)
            else:
                self.viFltStr.Show(True)
                self.viFltStr.Enable(False)
                self.cbAddFlt.Enable(True)
            
        except:
            self.viFltStr.Show(True)
            self.viFltDate.Show(False)
            self.viFltTime.Show(False)
            self.viFltInt.Show(False)
            self.viFltLong.Show(False)
            self.viFltFloat.Show(False)
            self.viFltStr.Enable(False)
            self.viFltDate.Enable(False)
            self.viFltTime.Enable(False)
            self.viFltDateTime.Enable(False)
            self.viFltDateTimeRel.Enable(False)
            self.viFltInt.Enable(False)
            self.viFltLong.Enable(False)
            self.viFltFloat.Enable(False)
            self.cbAddFlt.Enable(False)

        self.bxsFilterInput.Layout()

    def Clear(self):
        vtXmlDomConsumer.Clear(self)
        
    def SetDoc(self,doc,bNet=False):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        vtXmlDomConsumer.SetDoc(self,doc,bNet)
        self.chcFilterPos.Clear()
        self.filters={}
        self.lstFilter.DeleteAllItems()
        self.idxSel=-1
        self.cbAddFlt.Enable(False)
        self.cbDelFlt.Enable(False)
        try:
            if self.doc is not None:
                self.dFilter=self.doc.GetPossibleFilter()
                if self.dFilter is None:
                    return
                keys=self.dFilter.keys()
                for k in keys:
                    self.chcFilterPos.Append(k)
            self.chcFilterPos.SetSelection(0)
            self.OnChcFilterPosChoice(None)
        except:
            vtLog.vtLngTB(self.GetName())
        
    def SetNode(self,node):
        self.Clear()
        
        
    def GetNode(self,node=None):
        if node is None:
            node=self.node
        if node is None:
            return
    
    def OnCbAddFltButton(self, event):
        event.Skip()
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        try:
            sName=self.chcFilterPos.GetStringSelection()
            lstAttr=self.dFilter[sName]
            
            idx=self.chcFilterAttr.GetSelection()
            tup=self.dFilter[self.chcFilterPos.GetStringSelection()][idx]
            try:
                lst=self.filters[sName]
            except:
                lst=[]
                self.filters[sName]=lst
            if tup[1]==vtXmlFilterType.FILTER_TYPE_STRING:
                sFlt=self.viFltStr.GetValueStr()
                filter=self.viFltStr.GetFilter()
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_DATE:
                sFlt=self.viFltDate.GetValueStr()
                filter=self.viFltDate.GetFilter()
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_TIME:
                sFlt=self.viFltTime.GetValueStr()
                filter=self.viFltTime.GetFilter()
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_DATE_TIME:
                sFlt=self.viFltDateTime.GetValueStr()
                filter=self.viFltDateTime.GetFilter()
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_DATE_TIME_REL:
                sFlt=self.viFltDateTimeRel.GetValueStr()
                filter=self.viFltDateTimeRel.GetFilter()
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_INT:
                sFlt=self.viFltInt.GetValueStr()
                filter=self.viFltInt.GetFilter()
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_LONG:
                sFlt=self.viFltLong.GetValueStr()
                filter=self.viFltLong.GetFilter()
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_FLOAT:
                sFlt=self.viFltFloat.GetValueStr()
                filter=self.viFltFloat.GetFilter()
            lst.append((tup,sFlt,filter))
            def cmpFunc(a,b):
                return lstAttr.index(a[0])-lstAttr.index(b[0])
            lst.sort(cmpFunc)
            self.__showFilters__()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnCbDelFltButton(self, event):
        event.Skip()
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.idxSel<0:
            return
        try:
            it=self.lstFilter.GetItem(self.idxSel,0)
            sTag=it.m_text
            lst=self.filters[sTag]
            i=self.lstFilter.GetItemData(self.idxSel)
            lst=lst[:i]+lst[i+1:]
            if len(lst)<=0:
                del self.filters[sTag]
            else:
                self.filters[sTag]=lst
            self.filters
            self.__showFilters__()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnLstFilterListItemSelected(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.idxSel=event.GetIndex()
        self.cbDelFlt.Enable(True)
        event.Skip()
        
    def __showFilters__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.cbDelFlt.Enable(False)
        self.idxSel=-1
        self.lstFilter.DeleteAllItems()
        keys=self.filters.keys()
        keys.sort()
        for k in keys:
            lFlt=self.filters[k]
            i=0
            for tup in lFlt:
                idx=self.lstFilter.InsertStringItem(sys.maxint,k)
                self.lstFilter.SetStringItem(idx,1,tup[0][0])
                self.lstFilter.SetStringItem(idx,2,tup[1])
                self.lstFilter.SetStringItem(idx,3,tup[2].GetOpStr())
                self.lstFilter.SetItemData(idx,i)
                i+=1
    def OnLstFilterListItemDeselected(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        event.Skip()
        self.idxSel=-1
        self.cbDelFlt.Enable(False)

    def OnLstFilterListColClick(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.idxSel=-1
        self.cbDelFlt.Enable(False)
        event.Skip()

    
