#!/usr/bin/env python
#----------------------------------------------------------------------
# Name:         enocde_bitmaps.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: encode_bitmaps.py,v 1.10 2010/02/21 00:21:15 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

command_lines = [
    
    "-u -i -n Element img/Box01_16.png images.py",
    "-a -u -n ElementSel img/Box02_16.png images.py",
    "-a -u -n Elem img/Box03_16.png images.py",
    "-a -u -n ElemSel img/Box04_16.png images.py",
    "-a -i -n NoIcon  img/noicon.png  images.py",
    "-a -u -n Attribute img/Attr01_16.png images.py",
    "-a -u -n AttributeSel img/Attr01_16.png images.py",
    "-a -u -n Text img/Note01_16.png images.py",
    "-a -u -n TextSel img/Note01_16.png images.py",
    "-a -u -n Year img/Year03_16.png images.py",
    "-a -u -n YearSel img/Year03_16.png images.py",
    "-a -u -n Month img/Month03_16.png images.py",
    "-a -u -n MonthSel img/Month03_16.png images.py",
    "-a -u -n Day img/Day03_16.png images.py",
    "-a -u -n DaySel img/Day03_16.png images.py",
    "-a -u -n Usr img/Usr02_16.png images.py",
    "-a -u -n UsrSel img/Usr02_16.png images.py",
    "-a -u -n Grouping img/TreeGrouping07b_16.png images.py",
    "-a -u -n Messages img/Messages01_16.png images.py",
    "-a -u -n MailNew img/Mail01_16.png images.py",
    "-a -u -n MailRead img/Mail02_16.png images.py",
    
    "-a -u -n Root img/VidMod_tmpl06_hollow_16.png images.py",
    "-a -u -n Grp               img/TreeGrp01_16.png            images.py",
    "-a -u -n NoGrp             img/TreeGrpNo01_16.png          images.py",
    "-a -u -n GrpOvl            img/TreeGrpOvl01_16.png         images.py",

    "-a -u -n LangEn            img/Lang_en_16.png              images.py",
    "-a -u -n LangDe            img/Lang_de_16.png              images.py",
    "-a -u -n English           img/Lang_en_16.png              images.py",
    "-a -u -n Deutsch           img/Lang_de_16.png              images.py",

    "-u -i -n Sort img/SortTree_01_16.png images_treemenu.py",
    "-a -u -n Move img/Move01_16.png images_treemenu.py",
    "-a -u -n Add img/Add01_16.png images_treemenu.py",
    "-a -u -n Del img/Del01_16.png images_treemenu.py",
    "-a -u -n Edit img/Edit01_16.png images_treemenu.py",
    "-a -u -n Find img/Search02_16.png images_treemenu.py",
    "-a -u -n Grp img/TreeGrouping05_16.png images_treemenu.py",
    "-a -u -n NoGrp img/TreeGrouping06_16.png images_treemenu.py",
    
    "-u -i -n English img/Lang_en_16.png images_lang.py",
    "-a -u -n Deutsch img/Lang_de_16.png images_lang.py",
    
    "-u -i -n Apply img/ok.png images_dlg.py",
    "-a -u -n Cancel img/abort.png images_dlg.py",
    "-a -u -n Add img/Add01_16.png images_dlg.py",
    "-a -u -n Del img/Del01_16.png images_dlg.py",
        ]

