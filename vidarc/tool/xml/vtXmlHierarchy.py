#----------------------------------------------------------------------------
# Name:         vtXmlHierarchy.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtXmlHierarchy.py,v 1.4 2008/03/09 19:43:15 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.xml.vtXmlDom as vtXmlDom

import string
        
def getTagNamesWithRel(doc,baseNode,relativeNode,n):
    if baseNode is None:
        return ''
    if n is None:
        return ''
    if not(doc.isSame(baseNode,n)):
        if doc.isSame(n,relativeNode):
            return doc.getNodeText(n,'tag')
        else:
            s='.'+doc.getNodeText(n,'tag')
            if not(doc.isSame(baseNode,doc.getParent(n))):
                return getTagNamesWithRel(doc,baseNode,relativeNode,doc.getParent(n))+s
            else:
                return doc.getNodeText(n,'tag')
    else:
        return ''
def getTagNames(doc,baseNode,relativeNode,n):
    if baseNode is None:
        return ''
    if n is None:
        return ''
    if not(doc.isSame(baseNode,n)):
        if doc.isSame(n,relativeNode):
            return '_'
        else:
            s='.'+doc.getNodeText(n,'tag')
            if not(doc.isSame(baseNode,doc.getParent(n))):
                return getTagNames(doc,baseNode,relativeNode,doc.getParent(n))+s
            else:
                return doc.getNodeText(n,'tag')
    else:
        return ''
def getTagNamesWithBase(doc,baseNode,relativeNode,n):
    if baseNode is None:
        return ''
    if n is None:
        return ''
    if not(doc.isSame(baseNode,n)):
        if doc.isSame(n,relativeNode):
            return '_'
        else:
            s='.'+doc.getNodeText(n,'tag')
            if not(doc.isSame(baseNode,n)):
                return getTagNamesWithBase(doc,baseNode,relativeNode,doc.getParent(n))+s#n.tagName
            else:
                return doc.getNodeText(n,'tag')
    else:
        return doc.getNodeText(n,'tag')

dictHierSkip=None
dictRelBase=None
dictRoot=None

def setup():
    global dictHierSkip
    global dictRelBase
    global dictRoot
    if dictHierSkip is None:
        dictHierSkip={}
        dictRelBase={}
        dictRoot={}
        #for e in ELEMENTS:
        #    if e[1].has_key('__properties'):
        #        for prop in e[1]['__properties']:
        #            if prop=='isSkipHier':
        #                dictHierSkip[e[0]]=True
        #            elif prop=='isRelBase':
        #                dictRelBase[e[0]]=True
        #            elif prop=='isRoot':
        #                dictRoot[e[0]]=True
    else:
        pass

def getRelBaseNode(doc,node):
    global dictRelBase
    setup()
    if node is None:
        return None
    try:
        if dictRelBase.has_key(doc.getTagName(node)):
            return node
        else:
            return getRelBaseNode(doc,doc.getParent(node))
    except:
        return None

def getRootNode(doc,node):
    global dictRoot
    setup()
    if node is None:
        return None
    try:
        if dictRoot.has_key(doc.getTagName(node)):
            return node
        else:
            return getRootNode(doc,doc.getParent(node))
    except:
        return None
def applyCalcNode(doc,node):
    try:
        return applyCalc(doc,doc.getNodeText(node,'__fid'),
                doc.getNodeText(node,'attr'),
                doc.getNodeText(node,'formular'))
    except:
        return 'fault'
def applyCalcDict(doc,dict):
    try:
        return applyCalc(doc,dict['__fid'],dict['attr'],
                dict['formular'])
    except:
        return 'fault'
def applyCalc(doc,id,attr,sProc):
    try:
        if doc is None:
            return 'fault'
        nodeElem=doc.getNodeById(id)
        if nodeElem is None:
            return 'fault'
        node=doc.getChild(nodeElem,attr)
        if node is None:
            return 'fault'
        sVal=doc.getAttributeVal(node)
        
        #if attr in ['tag','name','descrition']:
        #    sVal=doc.getText(node)
        #elif len(doc.getAttribute(node,'ih'))>0:
        #    sVal=doc.getText(node)
        #elif len(doc.getAttribute(node,'ih'))>0:
        #    sVal=doc.getText(node)
        #else:
        #    sVal=doc.getNodeText(node,'val')
        strs=string.split(sProc,',')
        lRes=[unicode(sVal)]
        iAct=1
        for s in strs:
            i=0
            while i>=0:
                i=string.find(s,'$',i)
                if i>=0:
                    j=string.find(s,'$',i+1)
                    if j>=0:
                        tmp=s[i+1:j]
                        try:
                            iLen=len(tmp)
                            try:
                                idx=int(tmp)
                                tmp=unicode(lRes[idx])
                            except:
                                if tmp==u'relative':
                                    root=getRootNode(doc,nodeElem)
                                    tmp=getTagNamesWithBase(doc,
                                            root,
                                            getRelBaseNode(doc,nodeElem),
                                            nodeElem)+'.'+attr
                                elif tmp==u'hierarchy_relative':
                                    root=getRootNode(doc,nodeElem)
                                    tmp=getTagNamesWithBase(doc,root,None,
                                            getRelBaseNode(doc,nodeElem))
                                elif tmp==u'absolute':
                                    root=getRootNode(doc,nodeElem)
                                    tmp=getTagNames(doc,
                                            root,
                                            None,nodeElem)+'.'+attr
                                elif tmp==u'absolute_base':
                                    root=getRootNode(doc,nodeElem)
                                    tmp=getTagNamesWithBase(doc,
                                            root,
                                            None,nodeElem)+'.'+attr
                            s=s[:i]+tmp+s[j+1:]
                        except:
                            return 'fault'
            lRes.append(str(eval(s)))
        return str(lRes[-1])
    except:
        return 'fault'
