#----------------------------------------------------------------------------
# Name:         vtBuffer.py
# Purpose:
# Author:       Walter Obweger
#
# Created:      20070309
# CVS-ID:       $Id: vtBuffer.py,v 1.9 2009/05/07 10:10:07 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import cStringIO
import threading,types

VERBOSE=0
if VERBOSE:
    import traceback

class vtBufferSizeException(Exception):
    def __init__(self,iLen,buf):
        self.iLen=iLen
        self.buf=buf
    def __str__(self):
        return 'vtBufferSizeException:can not add data len:%d buffer info:%s'%(self.iLen,self.buf)
    def getBuf(self):
        return self.buf
class vtBufferPushException(Exception):
    def __init__(self,val,buf):
        self.val=val
        self.buf=buf
    def __str__(self):
        return self.val
    def getBuf(self):
        return self.buf
class vtBufferPopException(Exception):
    def __init__(self,iLen,buf):
        self.buf=buf
        self.iLen=iLen
    def __str__(self):
        return 'vtBufferPopException:can not add data len:%d buffer info:%s'%(self.iLen,self.buf)
    def getBuf(self):
        return self.buf

class vtBuffer:
    def __init__(self,max=1048576,reorganise=65535):
        self.buf=cStringIO.StringIO()
        self.tmp=cStringIO.StringIO()
        self.iAct=0
        self.iFill=0
        self.iMax=max
        self.iReorganise=reorganise
        self.semAccess=threading.Lock()#threading.Semaphore()
    def _acquire(self,blocking=True):
        if self.semAccess.acquire(blocking):
            return True
        return False
    def _release(self):
        try:
            self.semAccess.release()
        except:
            pass
    def push(self,data,blocking=True):
        if self.semAccess.acquire(blocking)==False:
            return -1
        try:
            try:
                if type(data)==types.UnicodeType:
                    data=data.encode('utf-8')
                iLen=len(data)
            except:
                if VERBOSE:
                    traceback.print_exc()
                #self.semAccess.release()
                raise vtBufferPushException(data,self)
            self.buf.seek(self.iFill)
            if (self.iFill+iLen)>self.iMax:
                self.__reorganise__()
                if (self.iFill+iLen)>self.iMax:
                    #self.semAccess.release()
                    raise vtBufferSizeException(iLen,self)#self.iMax)
            try:
                self.buf.write(data)
                self.iFill=self.buf.tell()
            except:
                if VERBOSE:
                    traceback.print_exc()
                #self.semAccess.release()
                raise vtBufferPushException(data,self)
        finally:
            self.semAccess.release()
        return 0
    def __getDataLen__(self):
        return self.iFill-self.iAct
    def getDataLen(self,blocking=True):
        if self.semAccess.acquire(blocking)==False:
            return None
        try:
            return self.__getDataLen__()
        finally:
            self.semAccess.release()
        return -1
    def pop(self,iCount=-1,blocking=True):
        sVal=None
        if self.semAccess.acquire(blocking)==False:
            return sVal
        try:
            if iCount<0:
                iCount=self.__getDataLen__()
            if iCount>self.__getDataLen__():
                raise
            self.buf.seek(self.iAct)
            sVal=self.buf.read(iCount)
            self.iAct+=iCount
            if self.iAct==self.iFill:
                self.buf.seek(0)
                self.buf.truncate()
                self.iAct=0
                self.iFill=0
                #else:
                #    self.__reorganise__()
        except:
            if VERBOSE:
                traceback.print_exc()
            self.semAccess.release()
            raise vtBufferPopException(iCount,self)
        self.semAccess.release()
        return sVal
    def __clear__(self):
        self.buf.seek(0)
        self.buf.truncate()
        self.iAct=0
        self.iFill=0
    def clear(self,blocking=True):
        if self.semAccess.acquire(blocking)==False:
            return
        try:
            self.__clear__()
        except:
            pass
        self.semAccess.release()
    def getvalue(self,blocking=True):
        sVal=None
        if self.semAccess.acquire(blocking)==False:
            return sVal
        sVal=self.buf.getvalue()[self.iAct:]
        self.semAccess.release()
        return sVal
    def get(self,blocking=True):
        sVal=None
        if self.semAccess.acquire(blocking)==False:
            return sVal
        sVal=self.buf.getvalue()[self.iAct:]
        self.semAccess.release()
        return sVal.decode('utf-8'),len(sVal)
    def find(self,sub,start=-1,end=-1,blocking=True):
        if self.semAccess.acquire(blocking)==False:
            return -1
        try:
            #sVal=self.buf.getvalue()[self.iAct:]
            #return sVal.find(sub)
            if type(sub)==types.UnicodeType:
                sub=sub.encode('utf-8')
            iLen=len(sub)
            iAct=self.iAct
            iFill=self.iFill
            if start>0:
                iAct+=start
            if end>0:
                iEnd=iAct+end
            else:
                iEnd=iFill
            iStop=iEnd-iLen
            self.buf.seek(iAct)
            i=0
            for j in xrange(iAct,iEnd):
                c=self.buf.read(1)
                if c==sub[i]:
                    i+=1
                    if i==iLen:
                        return j-iLen+1-self.iAct#k-self.iAct
                else:
                    i=0
                    if c==sub[i]:
                        i+=1
                        if i==iLen:
                            return j-iLen+1-self.iAct#k-self.iAct
            return -1
        finally:
            self.semAccess.release()
    def __reorganise__(self):
        try:
            if (self.iMax-self.iFill)<self.iReorganise:
            #if (self.iAct-self.iFill)<self.iReorganise:
                if VERBOSE:
                    print 'reorganise1',self
                iLen=self.__getDataLen__()#iFill-self.iAct
                #if iLen>self.iReorganise:
                #    return
                #self.buf.seek(-self.iReorganise,2)
                #self.tmp.write(self.buf.read(self.iReorganise))
                self.buf.seek(self.iAct)
                self.tmp.write(self.buf.read(iLen))
                self.buf.seek(0)
                self.buf.truncate()
                #iLen=self.tmp.tell()
                self.tmp.seek(0)
                self.buf.write(self.tmp.read(iLen))
                #self.buf.seek(0,2)
                #self.iFill=self.buf.tell()
                self.iFill=iLen
                self.tmp.seek(0)
                self.tmp.truncate()
                self.iAct=0#self.iFill-self.iAct
                #if self.iAct<0:
                    # unprocessed data lost
                #    self.iAct=0
                if VERBOSE:
                    print 'reorganise2',self
        except:
            if VERBOSE:
                traceback.print_exc()
            pass
    def __str__(self):
        return 'iAct:%d,iFill:%d,iSize:%d'%(self.iAct,self.iFill,self.iMax)
    def __len__(self):
        try:
            self.semAccess.acquire()
            return self.iFill
        finally:
            self.semAccess.release()
    def __getitem__(self,i):
        try:
            #print i,type(i)
            self.semAccess.acquire()
            iPos=self.buf.tell()
            if type(i)==types.SliceType:
                k=i.start
                l=i.stop
                if i.step is None:
                    if k<0:
                        return ''
                    else:
                        m=l-k
                        #print k,l,l-k,i.step
                        if m<0:
                            return ''
                        self.buf.seek(self.iAct+min(self.iFill,k),0)
                        return self.buf.read(min(self.iFill,m))
                else:
                    if k is None:
                        k=0
                    if l is None:
                        l=self.iFill
                    if k<0:
                        k+=self.iFill
                    if l<0:
                        l+=self.iFill
                    m=l-k
                    n=i.step
                    #print k,l,l-k,n
                    if n>0:
                        self.buf.seek(self.iAct+min(self.iFill,k),0)
                        v=[]
                        #print k,l,n
                        while k<l:
                            #print k,l,n,v
                            v.append(self.buf.read(1))
                            self.buf.seek(n-1,1)
                            k+=n
                        return ''.join(v)
                    elif n<0:
                        #raise ValueError('')
                        n=-n
                        self.buf.seek(self.iAct+min(self.iFill,m),2)
                        v=[]
                        #print k,l,n
                        while k<l:
                            #print k,l,n,v
                            v.append(self.buf.read(1))
                            self.buf.seek(n-1,1)
                            k+=n
                        v.reverse()
                        return ''.join(v)
                    else:
                        raise ValueError('')
            else:
                if i<0:
                    self.buf.seek(max(-i-1),2)
                    return self.read(1)
                else:
                    self.buf.seek(self.iAct+min(self.iFill,i),0)
                    return self.buf.read(1)
        finally:
            self.buf.seek(iPos)
            self.semAccess.release()
