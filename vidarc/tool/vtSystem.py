#----------------------------------------------------------------------------
# Name:         vtSystem.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060306
# CVS-ID:       $Id: vtSystem.py,v 1.1 2006/04/13 14:24:34 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------


import wx

def LimitWindowToScreen(iX,iY,iWidth,iHeight):
    try:
        iMaxW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
        iMaxH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
        iMaxW-=wx.SystemSettings.GetMetric(wx.SYS_MENU_Y)
        iMaxH-=wx.SystemSettings.GetMetric(wx.SYS_MENU_Y)
        #iMaxW-=wx.SystemSettings.GetMetric(wx.SYS_WINDOWMIN_X)
        #iMaxH-=wx.SystemSettings.GetMetric(wx.SYS_WINDOWMIN_Y)
        if iX>iMaxW:
            iX=iMaxW-iWidth
        if iY>iMaxH:
            iY=iMaxH-iHeight
        if iX<0:
            iX=0
        if iY<0:
            iY=0
        if iX+iWidth>iMaxW:
            iX=iMaxW-iWidth
        if iY+iHeight>iMaxH:
            iY=iMaxH-iHeight
        if iX<0:
            iX=0
        if iY<0:
            iY=0
        if iX+iWidth>iMaxW:
            iWidth=iMaxW-iX
        if iY+iHeight>iMaxH:
            iHeight=iMaxH-iY
    except:
        iX=iY=0
        iWidth=100
        iHeight=100
    return iX,iY,iWidth,iHeight

