#----------------------------------------------------------------------------
# Name:         vtSecAes.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20100301
# CVS-ID:       $Id: vtSecAes.py,v 1.2 2010/03/03 02:11:30 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from Crypto.Cipher import AES
import binascii

class vtSecAes:
    def __init__(self,phrase,IV=None):
        iL=len(phrase)
        if iL>32:
            phrase=phrase[-32:]
        elif iL==32:
            pass
        elif iL>24:
            phrase=self._extend(phrase,32)
        elif iL>16:
            phrase=self._extend(phrase,24)
        elif iL<16:
            phrase=self._extend(phrase,16)
        if IV is None:
            self._oAes=AES.new(phrase,AES.MODE_CFB,'0Z\xae\xcfj\xe5\xac\x8a_\x07/6\xce\x06\xd3v')
        elif len(IV)==0:
            self._oAes=AES.new(phrase,AES.MODE_ECB,'0Z\xae\xcfj\xe5\xac\x8a_\x07/6\xce\x06\xd3v')
        else:
            self._oAes=AES.new(phrase,AES.MODE_CFB,IV)
    def _extend(self,s,sz,fill='\x00'):
        iL=len(s)
        #i=iL-((iL/sz)*sz)
        i=sz-(iL%sz)
        if i>0:
            return s+fill*i
        return s
    def Encrypt(self,s):
        return self._oAes.encrypt(self._extend(s,16))
    def EncryptHex(self,s):
        return binascii.hexlify(self._oAes.encrypt(self._extend(s,16)))
    def Decrypt(self,s):
        p=self._oAes.decrypt(s)
        iL=len(p)
        for i in xrange(iL-1,iL-17,-1):
            if p[i]!='\x00':
                return p[:i+1]
        return p[:-16]
    def DecryptHex(self,s):
        return self.Decrypt(binascii.unhexlify(s))
