#Boa:FramePanel:vtSecAclFileTree
#----------------------------------------------------------------------------
# Name:         vtSecAclDomPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060409
# CVS-ID:       $Id: vtSecAclFileTree.py,v 1.11 2008/02/02 16:11:48 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.sec.vtSecAclFile
import vidarc.tool.xml.vtXmlFilterTypeInput
import vidarc.vApps.vHum.vXmlHumInputTreeGrp
import vidarc.vApps.vHum.vXmlHumInputTree
import wx.gizmos
import wx.lib.buttons

from vidarc.tool.sec.vtSecFileAcl import *
from vidarc.tool.xml.vtXmlDomConsumer import *
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase

import types

VERBOSE=0

[wxID_VTSECACLFILETREE, wxID_VTSECACLFILETREECBAPPLYACL, 
 wxID_VTSECACLFILETREECBUSRADD, wxID_VTSECACLFILETREECBUSRCLR, 
 wxID_VTSECACLFILETREECBUSRDEL, wxID_VTSECACLFILETREELBLACL, 
 wxID_VTSECACLFILETREELBLUSR, wxID_VTSECACLFILETREETRLSTACL, 
 wxID_VTSECACLFILETREEVIUSR, wxID_VTSECACLFILETREEVTSACL, 
] = [wx.NewId() for _init_ctrls in range(10)]

class vtSecAclFileTree(wx.Panel,vtXmlDomConsumer):
    def _init_coll_bxsUsrBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbUsrClr, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.cbUsrAdd, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.cbUsrDel, 0, border=4, flag=wx.LEFT)

    def _init_coll_bxsAclSecLbl_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblAcl, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.vtsAcl, 4, border=8, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.cbApplyAcl, 0, border=4, flag=wx.LEFT)

    def _init_coll_bxsUsr_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblUsr, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viUsr, 2, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddSizer(self.bxsUsrBt, 2, border=0, flag=wx.EXPAND)

    def _init_coll_fgsDom_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_fgsDom_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.trlstAcl, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsAclSecLbl, 1, border=8,
              flag=wx.EXPAND | wx.ALIGN_CENTER | wx.TOP)
        parent.AddSizer(self.bxsUsr, 1, border=4, flag=wx.TOP | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsDom = wx.FlexGridSizer(cols=1, hgap=0, rows=10, vgap=0)

        self.bxsUsr = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsUsrBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsAclSecLbl = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsDom_Items(self.fgsDom)
        self._init_coll_fgsDom_Growables(self.fgsDom)
        self._init_coll_bxsUsr_Items(self.bxsUsr)
        self._init_coll_bxsUsrBt_Items(self.bxsUsrBt)
        self._init_coll_bxsAclSecLbl_Items(self.bxsAclSecLbl)

        self.SetSizer(self.fgsDom)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTSECACLFILETREE,
              name=u'vtSecAclFileTree', parent=prnt, pos=wx.Point(441, 246),
              size=wx.Size(366, 307), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(358, 280))

        self.trlstAcl = wx.gizmos.TreeListCtrl(id=wxID_VTSECACLFILETREETRLSTACL,
              name=u'trlstAcl', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(358, 208), style=wx.TR_HAS_BUTTONS)
        self.trlstAcl.Bind(wx.EVT_TREE_SEL_CHANGED,
              self.OnTrlstAclTreeSelChanged, id=wxID_VTSECACLFILETREETRLSTACL)

        self.lblUsr = wx.StaticText(id=wxID_VTSECACLFILETREELBLUSR,
              label=u'user', name=u'lblUsr', parent=self, pos=wx.Point(0, 250),
              size=wx.Size(67, 30), style=wx.ALIGN_RIGHT)

        self.cbUsrClr = wx.lib.buttons.GenBitmapButton(ID=wxID_VTSECACLFILETREECBUSRCLR,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbUsrClr', parent=self,
              pos=wx.Point(218, 250), size=wx.Size(31, 30), style=0)
        self.cbUsrClr.Bind(wx.EVT_BUTTON, self.OnCbUsrClrButton,
              id=wxID_VTSECACLFILETREECBUSRCLR)

        self.viUsr = vidarc.vApps.vHum.vXmlHumInputTree.vXmlHumInputTree(id=wxID_VTSECACLFILETREEVIUSR,
              name=u'viUsr', parent=self, pos=wx.Point(71, 250),
              size=wx.Size(139, 30), style=0)

        self.cbUsrAdd = wx.lib.buttons.GenBitmapButton(ID=wxID_VTSECACLFILETREECBUSRADD,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbUsrAdd', parent=self,
              pos=wx.Point(253, 250), size=wx.Size(31, 30), style=0)
        self.cbUsrAdd.Bind(wx.EVT_BUTTON, self.OnCbAddFltButton,
              id=wxID_VTSECACLFILETREECBUSRADD)

        self.cbUsrDel = wx.lib.buttons.GenBitmapButton(ID=wxID_VTSECACLFILETREECBUSRDEL,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbUsrDel', parent=self,
              pos=wx.Point(288, 250), size=wx.Size(31, 30), style=0)
        self.cbUsrDel.Bind(wx.EVT_BUTTON, self.OnCbDelFltButton,
              id=wxID_VTSECACLFILETREECBUSRDEL)

        self.lblAcl = wx.StaticText(id=wxID_VTSECACLFILETREELBLACL,
              label=u'permission', name=u'lblAcl', parent=self, pos=wx.Point(0,
              216), size=wx.Size(60, 30), style=wx.ALIGN_RIGHT)

        self.cbApplyAcl = wx.lib.buttons.GenBitmapButton(ID=wxID_VTSECACLFILETREECBAPPLYACL,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbApplyAcl', parent=self,
              pos=wx.Point(326, 216), size=wx.Size(31, 30), style=0)
        self.cbApplyAcl.Bind(wx.EVT_BUTTON, self.OnCbApplyAclButton,
              id=wxID_VTSECACLFILETREECBAPPLYACL)

        self.vtsAcl = vidarc.tool.sec.vtSecAclFile.vtSecAclFile(id=wxID_VTSECACLFILETREEVTSACL,
              name=u'vtsAcl', parent=self, pos=wx.Point(64, 216),
              size=wx.Size(250, 30), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vtSec')
        vtXmlDomConsumer.__init__(self)
        self._init_ctrls(parent)
        
        self.docHum=vtXmlDomConsumer()
        self.cbApplyAcl.SetBitmapLabel(vtArt.getBitmap(vtArt.Apply))
        self.cbUsrAdd.SetBitmapLabel(vtArt.getBitmap(vtArt.Add))
        self.cbUsrDel.SetBitmapLabel(vtArt.getBitmap(vtArt.Del))
        self.cbUsrClr.SetBitmapLabel(vtArt.getBitmap(vtArt.Erase))
        
        self.lObjHum=[self.viUsr, self.cbUsrClr,self.cbUsrAdd]
        
        self.SetSize(size)
        self.Move(pos)
        
        self.tiRoot=None
        self.tiGrps=None
        self.tiUsrs=None
        self.lGrps=[]
        self.dTreeItems={}
        self.dAcl={}
        self.Clear()
        i=0
        for sCol,iWidth in [(_(u'tag'),180),
                    (_(u'R'),20),(_(u'W'),20),(_(u'X'),20),
                    (_(u'D'),20),(_(u'B'),20),(_(u'C'),20),(_(u'S'),20),
                    ]:
            self.trlstAcl.AddColumn(sCol)
            self.trlstAcl.SetColumnWidth(i,iWidth)
            i+=1
        
        self.trlstAcl.SetMainColumn(0)
        self.SetupImageList()
        
        r=self.trlstAcl.AddRoot(_(u'ACL File'))
        self.__setTIImages__('elem','root',r)
        self.tiGrps=self.trlstAcl.AppendItem(r,'groups',-1,-1,None)
        self.__setTIImages__('grp','group',self.tiGrps)
        self.tiUsrs=self.trlstAcl.AppendItem(r,'users',-1,-1,None)
        self.__setTIImages__('grp','user',self.tiUsrs)
        self.trlstAcl.Expand(r)
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        try:
            del self.lGrps
            del self.dTreeItems
            del self.dAcl
        except:
            #vtLog.vtLngTB('del')
            pass
    def SetupImageList(self):
        self.dImg={'elem':{},'attr':{},'grp':{}}
        self.lImg=wx.ImageList(16,16)
        
        imgNor=vtArt.getBitmap(vtArt.Invisible)
        imgSel=vtArt.getBitmap(vtArt.Invisible)
        self.dImg['dft']=[self.lImg.Add(imgNor),self.lImg.Add(imgSel)]
        
        imgNor=vtArt.getBitmap(vtArt.Acl)
        imgSel=vtArt.getBitmap(vtArt.Acl)
        self.dImg['elem']['root']=[self.lImg.Add(imgNor),self.lImg.Add(imgSel)]
        
        imgNor=vtArt.getBitmap(vtArt.Group)
        imgSel=vtArt.getBitmap(vtArt.Group)
        self.dImg['grp']['group']=[self.lImg.Add(imgNor),self.lImg.Add(imgSel)]
        
        imgNor=vtArt.getBitmap(vtArt.User)
        imgSel=vtArt.getBitmap(vtArt.User)
        self.dImg['grp']['user']=[self.lImg.Add(imgNor),self.lImg.Add(imgSel)]
        
        imgNor=vtArt.getBitmap(vtArt.Filter)
        imgSel=vtArt.getBitmap(vtArt.Filter)
        self.dImg['flt']=[self.lImg.Add(imgNor),self.lImg.Add(imgSel)]
        
        img=vtArt.getBitmap(vtArt.Build)
        self.dImg['attr'][ACL_FILE_MSK_EXEC]=self.lImg.Add(img)
        img=vtArt.getBitmap(vtArt.Browse)
        self.dImg['attr'][ACL_FILE_MSK_READ]=self.lImg.Add(img)
        img=vtArt.getBitmap(vtArt.Save)
        self.dImg['attr'][ACL_FILE_MSK_WRITE]=self.lImg.Add(img)
        img=vtArt.getBitmap(vtArt.Report)#vtArt.Add)
        self.dImg['attr'][ACL_FILE_MSK_CREATE]=self.lImg.Add(img)
        img=vtArt.getBitmap(vtArt.Del)
        self.dImg['attr'][ACL_FILE_MSK_DEL]=self.lImg.Add(img)
        img=vtArt.getBitmap(vtArt.Zip)
        self.dImg['attr'][ACL_FILE_MSK_ARCHIVE]=self.lImg.Add(img)
        img=vtArt.getBitmap(vtArt.EMail)
        self.dImg['attr'][ACL_FILE_MSK_DISTRIBUTE]=self.lImg.Add(img)
        img=vtArt.getBitmap(vtArt.Invisible)
        self.dImg['attr'][-1]=self.lImg.Add(img)
        
        self.trlstAcl.SetImageList(self.lImg)
    def __markModified__(self,obj,flag=True):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'__markModified__;flag:%s'%(flag),
        #                origin=self.GetName())
        if flag:
            f=obj.GetFont()
            f.SetWeight(wx.FONTWEIGHT_BOLD)
            obj.SetFont(f)
        else:
            f=obj.GetFont()
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
            obj.SetFont(f)
        obj.Refresh()
    
    def Clear(self):
        vtXmlDomConsumer.Clear(self)
        self.lHum=[]
        self.dAcl={}
        self.iSelKind=-1
        self.selData=None
        self.selTi=None
        self.__clearTreeAclInfo__()
    def __clearTreeAclInfo__(self):
        #vtLog.CallStack('')
        for ti in [self.tiGrps,self.tiUsrs]:
            if ti is not None:
                self.trlstAcl.DeleteChildren(ti)
    def SetDoc(self,doc,bNet=False):
        self.tiRoot=None
        vtXmlDomConsumer.SetDoc(self,doc,bNet)
        self.viUsr.SetDoc(doc,bNet)
        self.__buildTree__()
        
        self.iSelKind=-1
        self.selData=None
        self.selTi=None
        
        #self.cbUsrAdd.Enable(False)
        #self.cbUsrDel.Enable(False)
        
    def __getImages__(self,kind,name):
        try:
            return self.dImg[kind][name]
        except:
            return self.dImg['dft']
    def __setTIImages__(self,kind,name,ti):
        img,imgSel=self.__getImages__(kind,name)
        self.trlstAcl.SetItemImage(ti,img,which = wx.TreeItemIcon_Normal)
        self.trlstAcl.SetItemImage(ti,imgSel,which = wx.TreeItemIcon_Expanded)
    def __setTIAttrImages__(self,iAttr,ti):
        if iAttr==ACL_FILE_MSK_READ:
            iCol=1
        elif iAttr==ACL_FILE_MSK_WRITE:
            iCol=2
        elif iAttr==ACL_FILE_MSK_EXEC:
            iCol=3
        elif iAttr==ACL_FILE_MSK_DEL:
            iCol=4
        elif iAttr==ACL_FILE_MSK_CREATE:
            iCol=5
        elif iAttr==ACL_FILE_MSK_ARCHIVE:
            iCol=6
        elif iAttr==ACL_FILE_MSK_DISTRIBUTE:
            iCol=7
        else:
            return     
        img=self.__getImages__('attr',iAttr)
        self.trlstAcl.SetItemImage(ti,img,iCol)
    def __clearTIAttrImages__(self,iAttr,ti):
        if iAttr==ACL_FILE_MSK_READ:
            iCol=1
        elif iAttr==ACL_FILE_MSK_WRITE:
            iCol=2
        elif iAttr==ACL_FILE_MSK_EXEC:
            iCol=3
        elif iAttr==ACL_FILE_MSK_DEL:
            iCol=4
        elif iAttr==ACL_FILE_MSK_CREATE:
            iCol=5
        elif iAttr==ACL_FILE_MSK_ARCHIVE:
            iCol=6
        elif iAttr==ACL_FILE_MSK_DISTRIBUTE:
            iCol=7
        else:
            return     
        img=self.__getImages__('attr',-1)
        self.trlstAcl.SetItemImage(ti,img,iCol)
    def __addAcl2Tree__(self,ti,k,sFid,sName,iAcl):
        #vtLog.CallStack('')
        #print kAcl,tup
        try:
            bmp=self.dImg['grp'][k]
        except:
            bmp=self.dImg['dft']
        
        tid=wx.TreeItemData()
        tid.SetData(sFid)
        tic=self.trlstAcl.AppendItem(ti,sName,-1,-1,tid)
        self.trlstAcl.SetItemImage(tic,bmp[0],which = wx.TreeItemIcon_Normal)
        self.trlstAcl.SetItemImage(tic,bmp[1],which = wx.TreeItemIcon_Expanded)
        self.__setTIAttr__(iAcl,tic)
        return tic
    def __setTIAttr__(self,iAcl,tic):
        for acl in ACL_FILE_LIST:
            iMskAcl=1<<acl
            if (iAcl& (iMskAcl))!=0:
                self.__setTIAttrImages__(iMskAcl,tic)
            else:
                self.__clearTIAttrImages__(iMskAcl,tic)
    def __buildTree__(self):
        self.__enableObjLst__(self.lObjHum,True)
        self.cbApplyAcl.Enable(False)
        self.cbUsrAdd.Enable(True)
        self.cbUsrDel.Enable(False)
        self.vtsAcl.SetValue(0)
        
        self.iSelKind=-1
        self.selData=None
        self.selTi=None
        
        self.lGrps=[]
        self.dTreeItems={}
        self.dAcl={}
        if self.docHum.doc is None:
            return
        try:
            childs=self.doc.getChilds(self.node,'group')
            ti=self.tiGrps
            for c in childs:
                sFid=self.doc.getForeignKey(c,'fid',appl='vHum')
                nodeHum=self.docHum.doc.getNodeById(sFid)
                if nodeHum is not None:
                    iFid=long(sFid)
                    sName=self.docHum.doc.getNodeText(nodeHum,'name')
                    try:
                        iAcl=int(self.doc.getAttribute(c,'acl_file'),16)
                    except:
                        iAcl=0
                    tic=self.__addAcl2Tree__(ti,'group',iFid,sName,iAcl)
                    self.dAcl[iFid]=iAcl
                    self.dTreeItems[iFid]=tic
                    self.lGrps.append(iFid)
                else:
                    vtLog.vtLngCurWX(vtLog.WARN,'group id:%s not found'%sFid,self)
            childs=self.doc.getChilds(self.node,'user')
            ti=self.tiUsrs
            for c in childs:
                sFid=self.doc.getForeignKey(c,'fid',appl='vHum')
                nodeHum=self.docHum.doc.getNodeById(sFid)
                if nodeHum is not None:
                    iFid=long(sFid)
                    sName=self.docHum.doc.getNodeText(nodeHum,'name')
                    try:
                        iAcl=int(self.doc.getAttribute(c,'acl_file'),16)
                    except:
                        iAcl=0
                    tic=self.__addAcl2Tree__(ti,'user',iFid,sName,iAcl)
                    self.dAcl[iFid]=iAcl
                    self.dTreeItems[iFid]=tic
                else:
                    vtLog.vtLngCurWX(vtLog.WARN,'user id:%s not found'%sFid,self)
        except:
            vtLog.vtLngTB(self.GetName())
    def SetDocHum(self,doc,bNet=False):
        self.docHum.SetDoc(doc,bNet)
        self.viUsr.SetDocTree(doc,bNet)
    def SetNode(self,node):
        self.Clear()
        if self.doc is None:
            return
        if node is None:
            self.Enable(False)
            return
        vtXmlDomConsumer.SetNode(self,node)
        self.__buildTree__()
    def GetNode(self,node=None):
        if node is None:
            node=self.node
        if node is None:
            return
        childs=self.doc.getChilds(node,'group')
        for c in childs:
            self.doc.deleteNode(c,node)   #delNode
        childs=self.doc.getChilds(node,'user')
        for c in childs:
            self.doc.deleteNode(c,node)   #delNode
        l=self.dAcl.keys()
        l.sort()
        self.lGrps.sort()
        for id in self.lGrps:
            try:
                e=self.doc.createSubNode(node,'group')
                self.doc.setForeignKey(e,'fid',id,'vHum')
                self.doc.setAttribute(e,'acl_file','0x%04x'%self.dAcl[id])
                l.remove(id)
            except:
                vtLog.vtLngTB(self.GetName())
        for id in l:
            try:
                e=self.doc.createSubNode(node,'user')
                self.doc.setForeignKey(e,'fid',id,'vHum')
                self.doc.setAttribute(e,'acl_file','0x%04x'%self.dAcl[id])
            except:
                vtLog.vtLngTB(self.GetName())
        self.doc.AlignNode(node,iRec=2)
    def OnCbApplyAclButton(self, event):
        event.Skip()
        try:
            if self.iSelKind==0:            # acl entry
                iAcl=self.vtsAcl.GetValue()
                tic=self.selTi
                self.dAcl[self.selData]=iAcl
                self.__setTIAttr__(iAcl,tic)
        except:
            vtLog.vtLngTB(self.GetName())

    def OnCbAddFltButton(self, event):
        event.Skip()
        try:
            if self.iSelKind==-1:          # tag
                sHum=self.viUsr.GetValue()
                try:
                    idHum=long(self.viUsr.GetID())
                except:
                    return
                iAcl=self.vtsAcl.GetValue()
                try:
                    tic=self.dTreeItems[idHum]
                    self.dAcl[idHum]=iAcl
                    self.__setTIAttr__(iAcl,tic)
                except:
                    #vtLog.vtLngTB(self.GetName())
                    k=self.viUsr.GetSelectedNodeTagName()
                    if k=='group':
                        ti=self.tiGrps
                    elif k=='user':
                        ti=self.tiUsrs
                    tic=self.__addAcl2Tree__(ti,'group',idHum,sHum,iAcl)
                    self.dAcl[idHum]=iAcl
                    self.dTreeItems[idHum]=tic
                    if k=='group':
                        self.lGrps.append(idHum)
                self.trlstAcl.SelectItem(tic)
                self.trlstAcl.EnsureVisible(tic)
        except:
            vtLog.vtLngTB(self.GetName())
        
    def __getNodeFromTI__(self,ti):
        tid=self.trlstAcl.GetPyData(ti)
        if  type(tid)==types.IntType:
            return self.trlstAcl.GetItemText(ti)
        else:
            tp=self.trlstAcl.GetItemParent(ti)
            if tp is None:
                return None
            else:
                return self.__getNodeFromTI__(tp)
    def __getNodeAclFromTI__(self,ti):
        tid=self.trlstAcl.GetPyData(ti)
        if  type(tid)==types.IntType:
            return tid
        else:
            tp=self.trlstAcl.GetItemParent(ti)
            if tp is None:
                return None
            else:
                return self.__getNodeAclFromTI__(tp)
    def OnCbDelFltButton(self, event):
        try:
            if self.iSelKind==0:            # acl entry
                self.trlstAcl.Delete(self.selTi)
                del self.dAcl[self.selData]
                del self.dTreeItems[self.selData]
                try:
                    self.lGrps.remove(self.selData)
                except:
                    pass
                #self.__buildTree__()
        except:
            vtLog.vtLngTB(self.GetName())
        self.selTi=None
        self.iSelKind=-1
        self.selData=None
        self.cbUsrDel.Enable(False)
        event.Skip()
    def __enableObjLst__(self,lst,flag):
        for o in lst:
            o.Enable(flag)
            o.Refresh()
    def OnTrlstAclTreeSelChanged(self, event):
        self.viUsr.Clear()
        ti=event.GetItem()
        iFid=self.trlstAcl.GetPyData(ti)
        if iFid is not None:
            iAcl=self.dAcl[iFid]
            self.vtsAcl.SetValue(iAcl)
            self.vtsAcl.Enable(True)
            self.__enableObjLst__(self.lObjHum,False)
            self.cbApplyAcl.Enable(True)
            #self.cbUsrAdd.Enable(True)
            self.cbUsrDel.Enable(True)
            self.iSelKind=0
            self.selData=iFid
            self.selTi=ti
        else:
            self.vtsAcl.SetValue(0)
            self.vtsAcl.Enable(True)
            self.__enableObjLst__(self.lObjHum,True)
            self.cbApplyAcl.Enable(False)
            #self.cbUsrAdd.Enable(False)
            self.cbUsrDel.Enable(False)
            self.iSelKind=-1
            self.selData=None
            self.selTi=ti
        event.Skip()

    def OnCbUsrClrButton(self, event):
        self.viUsr.Clear()
        event.Skip()

    
