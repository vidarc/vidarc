#----------------------------------------------------------------------------
# Name:         vtSecFileAcl.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060703
# CVS-ID:       $Id: vtSecFileAcl.py,v 1.2 2006/08/29 10:06:24 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

ACL_FILE_READ           =   0
ACL_FILE_WRITE          =   1
ACL_FILE_EXEC           =   2
ACL_FILE_DEL            =   3
ACL_FILE_CREATE         =   4
ACL_FILE_ARCHIVE        =   5
ACL_FILE_DISTRIBUTE     =   6

ACL_FILE_MSK_READ           =   0x0001
ACL_FILE_MSK_WRITE          =   0x0002
ACL_FILE_MSK_EXEC           =   0x0004
ACL_FILE_MSK_DEL            =   0x0008
ACL_FILE_MSK_CREATE         =   0x0010
ACL_FILE_MSK_ARCHIVE        =   0x0020
ACL_FILE_MSK_DISTRIBUTE     =   0x0040

ACL_FILE_LIST= [0,1,2,3,4,5,6]

from vidarc.tool.xml.vtXmlNodeBase import *
import vidarc.tool.log.vtLog as vtLog

class vtSecFileAcl(vtXmlNodeBase):
    def __init__(self,tagName='fileAcl'):
        vtXmlNodeBase.__init__(self,tagName)
        self.dAcl={'group':{},'user':{}}
        self.bBusy=False
    def GetDescription(self):
        return 'file ACLs'
    # ---------------------------------------------------------
    # specific
    def __getAcl__(self,node):
        try:
            #if vtLog.vtLngIsLogged(vtLog.DEBUG):
            #    vtLog.vtLngCurCls(vtLog.DEBUG,'node:%s'%(node),self)
            sTag=self.doc.getTagName(node)
            sAcl=self.doc.getAttribute(node,'acl_file')
            #print '    ',sTag,sAcl
            if len(sAcl)>0:
                sFid=self.doc.getForeignKey(node,'fid',appl='vHum')
                #print '    ',self.doc.getTagName(self.doc.getParent(node)),self.doc.getKey(self.doc.getParent(node)),sTag,sAcl
                if sTag in self.dAcl:
                    d=self.dAcl[sTag]
                else:
                    d={}
                    self.dAcl[sTag]=d
                
                try:
                    d[long(sFid)]=long(sAcl,16)
                except:
                    pass
                #print self.dAcl
        except:
            vtLog.vtLngTB(self.GetName())
        return 0
        
    def BuildAcl(self,node):
        self.bBusy=True
        self.dAcl={'group':{},'user':{}}
        self.doc.procChildsExt(node,self.__getAcl__)
        self.bBusy=False
    def GetAclDict(self):
        return self.dAcl
    def IsAccessOk(self,objLogin,iAcl,iSecLv):
        return self.IsAccessOkDict(self.dAcl,objLogin,iAcl,iSecLv)
    def IsAccessOkDict(self,dAcl,objLogin,iAcl,iSecLv):
        try:
            d=dAcl['user']
            if objLogin.GetUsrId() in d:
                if d[objLogin.GetUsrId()]&iAcl==0:
                    return False
                if iSecLv>=0:
                    if iSecLv>objLogin.GetSecLv():
                        return False
                return True
            d=dAcl['group']
            for grpId in objLogin.GetGrpIds():
                if d[grpId]&iAcl!=0:
                    if iSecLv>=0:
                        if iSecLv<objLogin.GetSecLv():
                            return True
                    else:
                        return True
            return False
        except:
            pass
        return False
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return None
    def GetAttrFilterTypes(self):
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return False
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return False
    def HasGraphics(self):
        return False
    def GetEditDialogClass(self):
        return None
    def GetAddDialogClass(self):
        return None
    def GetPanelClass(self):
        return None
