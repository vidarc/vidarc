��    A      $  Y   ,      �     �     �     �     �  	   �     �     �       %        =     K     Y     g     k     q     x     ~     �     �     �     �     �  $   �     �  
   �     �               '     ?     K     b     q  
   w     �     �     �     �     �     �     �  	   �     �               #     *     /  
   5     @     M     T  ,   q     �     �  �   �     	     �	     �	     �	     �	     �	     �	     �	  �  �	     �     �     �     �  	   �     �                     :     H     W     f     j  	   s     }     �     �     �     �     �      �  +   �     �  
   �  &        (     6  !   F     h     y     �     �  
   �     �     �     �     �       	        #     7     ?     F     \     u     }     �  
   �     �     �     �  .   �     �  
      �        �          0     9     =     F     O     c            A             !      <   ;   )             +              =   '   0       "          7   %   ?   (       $                       3      #   4   
   @             *   >   8       ,   	                     &                 1                        .   9                6   -   :       5                2           /      Create vtSecPwd ...   Open Security ...   import %s ...   import library ...  (undef*) --config <filename> --file <filename> --help --lang <language id according ISO639> --log <level> -c <filename> -f <filename> Add Apply Cancel Clear Day Delete End End Date From Internal structure is faulty. Internal structure not set properly. Login New	Ctrl+N New passwords do not match. Open	Ctrl+O Password Change Passwords do not match. Save	Ctrl+S Save Config	Ctrl+Alt+S Security Level Start Start Date To Unexcepted return value=%d. VIDARC Security Password Wrong password entered. access category confirm password firstname group group by login group by surname groups help login navigation new password normal open the <filename> at start open the <filename> to configure application password security security password module.

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
 security password root show this screen surname type user users valid flags: vtSecPwd Password Change Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2008-03-16 19:46+0100
PO-Revision-Date: 2005-09-20 14:49+0100
Last-Translator: Walter Obweger <walter.obweger@vidarc.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=iso-8859-1
Content-Transfer-Encoding: 8bit
X-Poedit-Language: German
X-Poedit-Country: GERMANY
X-Poedit-SourceCharset: utf-8
   Erzeuge vtSecPwd ...   �ffne Sicherheit ...   importiere %s ...   importiere Bibliothek ...  (undef*) --config <Dateiname> --file <Dateiname> --help --lang <Sprache id nach ISO639> --log <Ebene> -c <Dateiname> -f <Dateiname> Add Anwenden Abbrechen S�ubern Tag L�schen Ende Enddatum von Interne Struktur ist fehlerhaft. Interne Struktur nicht vollst�ndig gesetzt. Login Neu	Ctrl+N Neue Passw�rter stimmen nicht �berein. �ffnen	Ctrl+O Passwort �ndern Passw�rter stimmen nicht �berein. Speichern	Ctrl+S Speichern Konfig	Ctrl+Alt+S Sicherheitsebene Start Startdatum bis Unerwarteter R�ckgabewert�%d. VIDARC Sicherheit Passwort Falsches Password eingegeben. Zugang Kategorie Passwort best�tigen Vorname Gruppe gruppieren nach Login gruppieren nach Nachname Gruppen Hilfe Login Navigation neues Passwort normal �ffnet <Dateiname> beim Start �ffnet <Dateiname> als Anwendungskonfiguration Passwort Sicherheit Sicherheit Passwort Modul.

Entworfen von VIDARC Automation GmbH, Walter Obweger.

Bitte besuchen Sie unsere web site http://www.vidarc.com.
Bitte senden Sie Fragen und R�ckmeldungen an office@vidarc.com. 
Wir freuen uns von Ihnen zu h�ren.
 Sicherheit Passwort Wurzel zeigt diesen Bildschirm Nachname Typ Benutzer Benutzer g�ltige Kennzeichen vtsecPwd Passwort �ndern 