#----------------------------------------------------------------------------
# Name:         vtSecPwdXml.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080305
# CVS-ID:       $Id: vtSecPwdXml.py,v 1.5 2008/03/18 16:13:30 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import sha

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vtSecPwdNodeRoot import vtSecPwdNodeRoot
from vtSecPwdNodeUsers import vtSecPwdNodeUsers
from vtSecPwdNodeUser import vtSecPwdNodeUser
from vtSecPwdNodeGroups import vtSecPwdNodeGroups
from vtSecPwdNodeGroup import vtSecPwdNodeGroup
from vtSecPwdNodeAccess import vtSecPwdNodeAccess
from vtSecPwdNodeSecurity import vtSecPwdNodeSecurity

from vidarc.tool.xml.vtXmlNodeLog import vtXmlNodeLog
from vidarc.tool.xml.vtXmlNodeCfg import vtXmlNodeCfg
from vidarc.tool.xml.vtXmlNodeAttrCfgML import vtXmlNodeAttrCfgML
from vidarc.tool.xml.vtXmlDomNetSim import vtXmlDomNetSim

from vidarc.tool.sec.vtSecXmlLogin import vtSecXmlLogin
from vidarc.tool.sec.vtSecCryptoXmlDomRegMachine import vtSecCryptoXmlDomRegMachine

class vtSecPwdXml(vtXmlDomNetSim,vtSecCryptoXmlDomRegMachine):
    VERBOSE=1
    TAGNAME_REFERENCE='tag'
    TAGNAME_ROOT='vtSecPwd'
    APPL_REF=[]
    def __init__(self,*kw,**kwargs):
        if 'appl' in kwargs:
            if kwargs['appl']=='':
                kwargs['appl']='vtSecPwd'
        else:
            kwargs['appl']='vtSecPwd'
        if 'skip' in kwargs:
            skip=kwargs['skip']
            for s in ['config']:
                try:
                    idx=skip.index(s)
                except:
                    skip.append(s)
        try:
            vtSecCryptoXmlDomRegMachine.__init__(self,*kw,**kwargs)
            vtXmlDomNetSim.__init__(self)
            self.verbose=10
            oRoot=vtSecPwdNodeRoot('root')
            self.RegisterNode(oRoot,False)
            oRoot=vtSecPwdNodeRoot()
            self.RegisterNode(oRoot,False)
            oUsrs=vtSecPwdNodeUsers()
            oUsr=vtSecPwdNodeUser()
            oGrps=vtSecPwdNodeGroups()
            oGrp=vtSecPwdNodeGroup()
            oAccess=vtSecPwdNodeAccess()
            oSec=vtSecPwdNodeSecurity()
            
            oLog=vtXmlNodeLog()
            oCfg=vtXmlNodeCfg()
            self.RegisterNode(oCfg,True)
            self.RegisterNode(vtXmlNodeAttrCfgML(),False)
            self.RegisterNode(oGrps,True)
            self.RegisterNode(oGrp,False)
            self.RegisterNode(oUsrs,True)
            self.RegisterNode(oUsr,False)
            self.RegisterNode(oLog,False)
            self.RegisterNode(oAccess,False)
            self.RegisterNode(oSec,False)
            
            self.LinkRegisteredNode(oRoot,oGrps)
            self.LinkRegisteredNode(oRoot,oUsrs)
            self.LinkRegisteredNode(oGrps,oGrp)
            self.LinkRegisteredNode(oGrp,oLog)
            self.LinkRegisteredNode(oUsrs,oUsr)
            self.LinkRegisteredNode(oUsr,oSec)
            self.LinkRegisteredNode(oUsr,oAccess)
            #self.LinkRegisteredNode(oUsr,oLog)
        except:
            vtLog.vtLngTB(self.appl)
        self.SetDftLanguages()
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'humans')
    def setUserInfo(self,oUsr,node,sLogin):
        try:
            oUsr.SetTag(node,sLogin)
            oUsr.SetSurName(node,sLogin)
            oUsr.SetFirstName(node,u'')
        except:
            vtLog.vtLngTB(self.appl)
    def setSecurityInfo(self,oSec,node,sLogin,iSecLevel):
        try:
            oSec.SetSecLevel(node,iSecLevel)
            oSec.SetPasswd(node,sLogin)
        except:
            vtLog.vtLngTB(self.appl)
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elem=self.createSubNode(self.getRoot(),'humans')
        self.CreateIds(self.attr)
        if bConnection==False:
            self.CreateReq()
        try:
            nodeBase=self.getBaseNode()
            nodeUsrs=self.getChild(nodeBase,'users')
            oUsr=self.GetReg('user')
            oAccess=self.GetReg('access')
            oSec=self.GetReg('security')
            for sLogin,iSecLevel in [('admin',32),('maintenance',16),('operator',8),('guest',0)]:
                n=oUsr.Create(nodeUsrs,self.setUserInfo,sLogin)
                oAccess.Create(n)
                oSec.Create(n,self.setSecurityInfo,sLogin,iSecLevel)
        except:
            vtLog.vtLngTB(self.appl)
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        elem=self.createSubNode(self.getRoot(),'security_acl')
        nAcl=self.createSubNodeAttr(elem,'___','active','1')
        n=self.createSubNodeAttr(nAcl,'all','sec_level','0')
        self.setAttribute(n,'acl','0x00000001')
        for tag in ['humans','cfg','groups','users','calc','attrML','log']:
            n=self.createSubNodeAttr(nAcl,tag,'sec_level','24')
            self.setAttribute(n,'acl','0x00000033')
        for tag in ['group','user','access','security']:
            n=self.createSubNodeAttr(nAcl,tag,'sec_level','24')
            self.setAttribute(n,'acl','0x00000033')
    def IsLoggedInSelf(self,node):
        if self.oLogin is None:
            return False
        else:
            if self.getTagName(node)=='user':
                tmp=node
            else:
                tmp=self.getParent(node)
            if self.getTagName(tmp)=='user':
                if self.getKeyNum(tmp)==self.oLogin.GetUsrId():
                    return True
        return False
    def Login(self,sLogin,sPwd):
        if len(sPwd)>0:
            m=sha.new()
            m.update(sPwd)
            sPwdEnc=m.hexdigest()
        else:
            sPwdEnc=''
        return self.LoginEncoded(sLogin,sPwdEnc)
    def LoginChange(self,sLogin,sPwd,sPwdNew,sPwdConf,bStore=False):
        if len(sPwdNew)>0:
            m=sha.new()
            m.update(sPwdNew)
            sPwdNewEnc=m.hexdigest()
        else:
            sPwdNewEnc=''
        if len(sPwdConf)>0:
            m=sha.new()
            m.update(sPwdConf)
            sPwdConfEnc=m.hexdigest()
        else:
            sPwdConfEnc=''
        if len(sPwd)>0:
            m=sha.new()
            m.update(sPwd)
            sPwdEnc=m.hexdigest()
        else:
            sPwdEnc=''
        return self.LoginEncodedChange(sLogin,sPwdEnc,sPwdNewEnc,sPwdConfEnc,
                    bStore=bStore)
    def __checkLogin__(self,node,oUsr,oSec,sLogin,sPwd):
        sTag=self.getTagName(node)
        if sTag=='user':
            if oUsr.GetLogin(node)==sLogin:
                n=self.getChild(node,'security')
                if n is not None:
                    if oSec.GetPasswd(n)==sPwd:
                        if oUsr.IsAccessOk(node):
                            self.oLogin=vtSecXmlLogin(sLogin,self.getKey(node),
                                    oUsr.GetGrpIds(node),
                                    oUsr.GetGrps(node),
                                    oSec.GetSecLevel(n),
                                    oSec.GetSecLevel(n)>=24
                                    )
                    return -1
        return 0
    def LoginEncoded(self,sLogin,sPwd):
        self.oLogin=None
        node=self.getBaseNode()
        if node is None:
            return -1
        nodeUsers=self.getChild(node,'users')
        if nodeUsers is None:
            return -1
        oUsr=self.GetReg('user')
        oSec=self.GetReg('security')
        self.procChildsKeys(nodeUsers,self.__checkLogin__,oUsr,oSec,
                    sLogin,sPwd)
        self.UpdateActiveAcl()
        return self.oLogin is not None
    def __checkLoginChange__(self,node,oUsr,oSec,sLogin,sPwd,sPwdNew,sPwdConf,l):
        sTag=self.getTagName(node)
        if sTag=='user':
            if oUsr.GetLogin(node)==sLogin:
                n=self.getChild(node,'security')
                if n is not None:
                    if oSec.GetPasswd(n)==sPwd:
                        if sPwdNew==sPwdConf:
                            oSec.SetPasswdEnocded(n,sPwdNew)
                        if l is None:
                            self.oLogin=vtSecXmlLogin(sLogin,self.getKey(node),
                                    oUsr.GetGrpIds(node),
                                    oUsr.GetGrps(node),
                                    oSec.GetSecLevel(n),
                                    oSec.GetSecLevel(n)>=24
                                    )
                        else:
                            if sPwdNew==sPwdConf:
                                l.append(2)
                            else:
                                l.append(1)
                    
                return -1
        return 0
    def LoginEncodedChange(self,sLogin,sPwd,sPwdNew,sPwdConf,bStore=False):
        self.oLogin=None
        node=self.getBaseNode()
        if node is None:
            return -1
        nodeUsers=self.getChild(node,'users')
        if nodeUsers is None:
            return -1
        if bStore:
            l=None
        else:
            l=[]
        oUsr=self.GetReg('user')
        oSec=self.GetReg('security')
        self.procChildsKeys(nodeUsers,self.__checkLoginChange__,oUsr,oSec,
                    sLogin,sPwd,sPwdNew,sPwdConf,l)
        if l is None:
            self.UpdateActiveAcl()
            if self.oLogin is None:
                return 0
            else:
                return 1
        else:
            if len(l)>0:
                return l[0]
            return 0
    def CreateAcl(self):
        vtSecCryptoXmlDomRegMachine.CreateAcl(self)
        self.UpdateActiveAcl()
    def UpdateActiveAcl(self):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,''%(),self)
            if self.objAcl is None:
                vtLog.vtLngCur(vtLog.WARN,'objAcl is None'%(),self.GetOrigin())
                return
            self.objAcl.dAcl={}
            if self.oLogin is None:
                vtLog.vtLngCur(vtLog.WARN,'oLogin is None'%(),self.GetOrigin())
                return
            self.objAcl.dAcl=self.GetAcl4Login(self.oLogin)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'dAcl:%s'%(vtLog.pformat(self.objAcl.dAcl)),self)
            return
            dAclFull=self.objAcl.dAcls['___']['acl']
            dAcl={}
            def getAcl(oLogin,d):
                iAcl=0
                idLogin=oLogin.GetUsrId()
                iSecLevel=oLogin.GetSecLv()
                if d['__sec_level']>=0:
                    iSecLv=d['__sec_level']
                    if iSecLv<=iSecLevel or iSecLv==-1:
                        iAcl|=d['__acl']
                dUsr=d['user']
                if idLogin in dUsr:
                    iSecLv=dUsr[idLogin].getSecLv()
                    if iSecLv<=iSecLevel or iSecLv==-1:
                        iAcl|=dUsr[idLogin].getAcl()
                return iAcl
            iAclAll=getAcl(self.oLogin,dAclFull['all'])
            for k,d in dAclFull.iteritems():
                if k=='all':
                    continue
                iAcl=getAcl(self.oLogin,d)|iAclAll
                if iAcl>0:
                    dAcl[k]=iAcl
            self.objAcl.dAcl=dAcl
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'dAcl:%s'%(vtLog.pformat(dAcl)),self)
        except:
            vtLog.vtLngTB(self.GetOrigin())
