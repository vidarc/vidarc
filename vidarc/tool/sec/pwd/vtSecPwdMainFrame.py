#Boa:Frame:vtSecPwdMainFrame
#----------------------------------------------------------------------------
# Name:         vtSecPwdMainFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080306
# CVS-ID:       $Id: vtSecPwdMainFrame.py,v 1.3 2008/03/20 19:47:44 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

import vidarc.tool.log.vtLog as vtLog
try:
    import vidarc.vApps.common.vSystem as vSystem
    from vidarc.vApps.common.vMDIFrame import vMDIFrame
    from vidarc.vApps.common.vCfg import vCfg
    import vidarc.config.vcCust as vcCust
    import vidarc.tool.art.vtArt as vtArt
    
    import vidarc.tool.sec.pwd.images as imgSecPwd
    from vidarc.tool.sec.pwd.vtSecPwdMainPanel import vtSecPwdMainPanel
except:
    vtLog.vtLngTB('import')

def getPluginImage():
    return imgSecPwd.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(imgSecPwd.getApplicationBitmap())
    return icon

def create(parent, id=-1, pos=wx.DefaultPosition, size=wx.DefaultSize,style=0, name='vSecPwdMain',
        fn=None,dn=None):
    return vtSecPwdMainFrame(parent,id,pos,size,style,name,fn,dn)

[wxID_VTSECPWDMAINFRAME] = [wx.NewId() for _init_ctrls in range(1)]

class vtSecPwdMainFrame(wx.Frame,vMDIFrame,vCfg):
    STATUS_CLK_POS=4
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VTSECPWDMAINFRAME,
              name=u'vtSecPwdMainFrame', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(400, 250), style=wx.DEFAULT_FRAME_STYLE,
              title=u'vtSecPwd')
        self.SetClientSize(wx.Size(392, 223))

    def __init__(self, parent, id, pos, size,style,name,fn,dn):
        self._init_ctrls(parent)
        icon = getApplicationIcon()
        self.SetIcon(icon)
        
        vMDIFrame.__init__(self,u'vtSecPwd',iNotifyTime=250)
        vCfg.__init__(self)
        self.dCfg.update({'x':10,'y':10,'width':800,'height':500})
        
        try:
            self.pn=vtSecPwdMainPanel(self,-1,wx.DefaultPosition,wx.DefaultSize,wx.TAB_TRAVERSAL,'vtSecPwdMainPanel',
                        fn=fn,dn=dn)
            self.pn.OpenCfgFile('vtSecPwdCfg.xml')
            
            self.netMaster=self.pn.GetNetMaster()
            self.netDocMain=self.pn.GetDocMain()
            self.trMain=self.pn.GetTreeMain()
            
            self.trMain.BindEvent('thdAddElemFin',self.OnAddElementsFin)
            self.trMain.BindEvent('thdAddElem',self.OnAddElementsProgress)
            
            self.AddMenus(self , self.trMain , self.netMaster,
                self.mnFile , self.mnView , self.mnTools)
            self.AddMenuGeneral(self.mnFile,None,iPos=0)
            self.pn.PopulateToolBar(self)
        except:
            vtLog.vtLngTB(self.GetName())
        self.SetName(name)
        self.dCfg.update(self._getCfgData(['vtSecPwdGui']))
        self.pn.SetCfgData(self._setCfgData,['vtSecPwdGui'],self.dCfg)
        self.__setCfg__()
    def Notify(self):
        vtLog.vtLngNumTrend()
        vMDIFrame.Notify(self)
    def __makeTitle__(self):
        sOldTitle=self.GetTitle()
        s="VIDARC "+self.appl
        fn=self.netDocMain.GetFN()
        if fn is None:
            s=s+_(" (undef*)")
        else:
            try:
                if fn.startswith(vcCust.USR_LOCAL_DN):
                    i=len(vcCust.USR_LOCAL_DN)
                    fn=u''.join([u'...',fn[i:]])
            except:
                pass
            s=s+" ("+fn
            if self.netDocMain.IsModified():
            #if self.bModified:
                s=s+"*)"
            else:
                s=s+")"
        if sOldTitle!=s:
            self.SetTitle(s)
    def OpenFile(self,fn):
        self.pn.OpenFile(fn)
    def OpenCfgFile(self,fn=None):
        self.pn.OpenCfgFile(fn)
        self.dCfg.update(self._getCfgData(['vtSecPwdGui']))
        self.pn.SetCfgData(self._setCfgData,['vtSecPwdGui'],self.dCfg)
        self.__setCfg__()
    def __setCfg__(self):
        try:
            iX=int(self.dCfg['x'])
            iY=int(self.dCfg['y'])
            iWidth=int(self.dCfg['width'])
            iHeight=int(self.dCfg['height'])
            iX,iY,iWidth,iHeight=vSystem.LimitWindowToScreen(iX,iY,iWidth,iHeight)
            self.Move((iX,iY))
            self.SetSize((iWidth,iHeight))
        except:
            pass
    def _getCfgData(self,l):
        return vCfg.getCfgData(self,self.pn.xdCfg,l)
    def _setCfgData(self,l,d):
        return vCfg.setCfgData(self,self.pn.xdCfg,l,d)
    def __getPluginImage__(self):
        return imgSecPwd.getPluginImage()
    def __getPluginBitmap__(self):
        return imgSecPwd.getPluginBitmap()
    def OnMainClose(self,event):
        if self.pn.xdCfg is not None:
            self._setCfgData(['vtSecPwdGui'],self.dCfg)
        vMDIFrame.OnMainClose(self,event)
    def Login(self,sLogin,sPwd):
        return self.netDocMain.Login(sLogin,sPwd)
    def LoginEncoded(self,sLogin,sPwd):
        return self.netDocMain.LoginEncoded(sLogin,sPwd)
    def __createMenuIds__(self):
        if not hasattr(self,'popupIdNew'):
            self.popupIdNew=wx.NewId()
            self.popupIdOpen=wx.NewId()
            self.popupIdSave=wx.NewId()
            self.popupIdSaveCfg=wx.NewId()
            wx.EVT_MENU(self,self.popupIdNew,self.OnNew)
            wx.EVT_MENU(self,self.popupIdOpen,self.OnOpen)
            wx.EVT_MENU(self,self.popupIdSave,self.OnSave)
            wx.EVT_MENU(self,self.popupIdSaveCfg,self.OnSaveCfg)
    def AddMenuGeneral(self,menu,obj,iPos=-1):
        try:
            self.__createMenuIds__()
            self.__addMenuGeneral__(menu,obj,iPos)
        except:
            vtLog.vtLngTB(self.GetName())
    def __addMenuGeneral__(self,menu,obj=None,iPos=-1):
        try:
            if menu is None:
                menu.wx.Menu()
                iPos=0
            if iPos<0:
                iPos=menu.GetMenuItemCount()
            mnIt=wx.MenuItem(menu,self.popupIdNew,_(u'New\tCtrl+N'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.New))
            menu.InsertItem(iPos,mnIt)
            iPos+=1
            mnIt=wx.MenuItem(menu,self.popupIdOpen,_(u'Open\tCtrl+O'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Browse))
            menu.InsertItem(iPos,mnIt)
            iPos+=1
            mnIt=wx.MenuItem(menu,self.popupIdSave,_(u'Save\tCtrl+S'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Save))
            menu.InsertItem(iPos,mnIt)
            iPos+=1
            mnIt=wx.MenuItem(menu,self.popupIdSaveCfg,_(u'Save Config\tCtrl+Alt+S'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Save))
            menu.InsertItem(iPos,mnIt)
            iPos+=1
            if obj is not None:
                wx.EVT_MENU(obj,self.popupIdNew,self.OnNew)
                wx.EVT_MENU(obj,self.popupIdOpen,self.OnOpen)
                wx.EVT_MENU(obj,self.popupIdSave,self.OnSave)
                wx.EVT_MENU(obj,self.popupIdSaveCfg,self.OnSaveCfg)
            iPos+=1
        except:
            vtLog.vtLngTB(self.GetName())
        return True
    def OnNew(self,evt):
        try:
            self.trMain.Clear()
            sFN=self.netDocMain.GetFN()
            self.netDocMain.New()
            self.netDocMain.SetFN(sFN)
            self.trMain.SetNode(None)
            
        except:
            vtLog.vtLngTB(self.GetName())
    def OnOpen(self,evt):
        try:
            self.pn.OpenFile()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSave(self,evt):
        try:
            self.pn.SaveFile()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSaveCfg(self,evt):
        try:
            self.pn.SaveCfgFile()
        except:
            vtLog.vtLngTB(self.GetName())
