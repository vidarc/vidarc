#Boa:Dialog:vtSecPwdPasswdChangeDialog
#----------------------------------------------------------------------------
# Name:         vtSecPwdPasswdChangeDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080316
# CVS-ID:       $Id: vtSecPwdPasswdChangeDialog.py,v 1.1 2008/03/16 16:06:43 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import vidarc.tool.art.vtArt as vtArt
from vidarc.tool.misc.vtmMsgDialog import vtmMsgDialog

def create(parent):
    return vtSecPwdPasswdChangeDialog(parent)

[wxID_VTSECPWDPASSWDCHANGEDIALOG, wxID_VTSECPWDPASSWDCHANGEDIALOGCBAPPLY, 
 wxID_VTSECPWDPASSWDCHANGEDIALOGCBCANCEL, 
 wxID_VTSECPWDPASSWDCHANGEDIALOGLBLLOGIN, 
 wxID_VTSECPWDPASSWDCHANGEDIALOGLBLPASSWDCONF, 
 wxID_VTSECPWDPASSWDCHANGEDIALOGLBLPASSWDNEW, 
 wxID_VTSECPWDPASSWDCHANGEDIALOGLBLPASSWDOLD, 
 wxID_VTSECPWDPASSWDCHANGEDIALOGTXTLOGIN, 
 wxID_VTSECPWDPASSWDCHANGEDIALOGTXTPASSWD, 
 wxID_VTSECPWDPASSWDCHANGEDIALOGTXTPASSWDCONF, 
 wxID_VTSECPWDPASSWDCHANGEDIALOGTXTPASSWDNEW, 
] = [wx.NewId() for _init_ctrls in range(11)]

class vtSecPwdPasswdChangeDialog(wx.Dialog):
    def _init_coll_fgsInfo_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblLogin, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtLogin, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblPasswdOld, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtPasswd, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblPasswdNew, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtPasswdNew, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblPasswdConf, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtPasswdConf, 0, border=0, flag=wx.EXPAND)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 1, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(16, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 1, border=0, flag=wx.EXPAND)

    def _init_coll_fgsMain_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.fgsInfo, 0, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsBt, 0, border=0, flag=wx.ALIGN_CENTER)

    def _init_coll_fgsInfo_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)
        parent.AddGrowableCol(1)

    def _init_coll_fgsMain_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsMain = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self.fgsInfo = wx.FlexGridSizer(cols=2, hgap=4, rows=3, vgap=4)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsMain_Items(self.fgsMain)
        self._init_coll_fgsMain_Growables(self.fgsMain)
        self._init_coll_fgsInfo_Items(self.fgsInfo)
        self._init_coll_fgsInfo_Growables(self.fgsInfo)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.fgsMain)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTSECPWDPASSWDCHANGEDIALOG,
              name=u'vtSecPwdPasswdChangeDialog', parent=prnt, pos=wx.Point(220,
              220), size=wx.Size(282, 180), style=wx.DEFAULT_DIALOG_STYLE,
              title=_(u'vtSecPwd Password Change'))
        self.SetClientSize(wx.Size(274, 153))

        self.lblLogin = wx.StaticText(id=wxID_VTSECPWDPASSWDCHANGEDIALOGLBLLOGIN,
              label=_(u'Login'), name=u'lblLogin', parent=self, pos=wx.Point(0,
              0), size=wx.Size(112, 21), style=wx.ALIGN_RIGHT)

        self.txtLogin = wx.TextCtrl(id=wxID_VTSECPWDPASSWDCHANGEDIALOGTXTLOGIN,
              name=u'txtLogin', parent=self, pos=wx.Point(116, 0),
              size=wx.Size(158, 21),
              style=wx.TE_PROCESS_ENTER | wx.TE_PROCESS_TAB | wx.TE_READONLY,
              value=u'')
        self.txtLogin.Enable(False)

        self.lblPasswdOld = wx.StaticText(id=wxID_VTSECPWDPASSWDCHANGEDIALOGLBLPASSWDOLD,
              label=_(u'password'), name=u'lblPasswdOld', parent=self,
              pos=wx.Point(0, 25), size=wx.Size(112, 21), style=wx.ALIGN_RIGHT)

        self.txtPasswd = wx.TextCtrl(id=wxID_VTSECPWDPASSWDCHANGEDIALOGTXTPASSWD,
              name=u'txtPasswd', parent=self, pos=wx.Point(116, 25),
              size=wx.Size(158, 21), style=wx.TE_PROCESS_ENTER | wx.TE_PASSWORD,
              value=u'')
        self.txtPasswd.Bind(wx.EVT_TEXT_ENTER, self.OnTxtPasswdTextEnter,
              id=wxID_VTSECPWDPASSWDCHANGEDIALOGTXTPASSWD)

        self.lblPasswdNew = wx.StaticText(id=wxID_VTSECPWDPASSWDCHANGEDIALOGLBLPASSWDNEW,
              label=_(u'new password'), name=u'lblPasswdNew', parent=self,
              pos=wx.Point(0, 50), size=wx.Size(112, 21), style=wx.ALIGN_RIGHT)

        self.txtPasswdNew = wx.TextCtrl(id=wxID_VTSECPWDPASSWDCHANGEDIALOGTXTPASSWDNEW,
              name=u'txtPasswdNew', parent=self, pos=wx.Point(116, 50),
              size=wx.Size(158, 21), style=wx.TE_PROCESS_ENTER | wx.TE_PASSWORD,
              value=u'')
        self.txtPasswdNew.Bind(wx.EVT_TEXT_ENTER, self.OnTxtPasswdNewTextEnter,
              id=wxID_VTSECPWDPASSWDCHANGEDIALOGTXTPASSWDNEW)

        self.lblPasswdConf = wx.StaticText(id=wxID_VTSECPWDPASSWDCHANGEDIALOGLBLPASSWDCONF,
              label=_(u'confirm password'), name=u'lblPasswdConf', parent=self,
              pos=wx.Point(0, 75), size=wx.Size(112, 21), style=wx.ALIGN_RIGHT)

        self.txtPasswdConf = wx.TextCtrl(id=wxID_VTSECPWDPASSWDCHANGEDIALOGTXTPASSWDCONF,
              name=u'txtPasswdConf', parent=self, pos=wx.Point(116, 75),
              size=wx.Size(158, 21), style=wx.TE_PROCESS_ENTER | wx.TE_PASSWORD,
              value=u'')
        self.txtPasswdConf.Bind(wx.EVT_TEXT_ENTER,
              self.OnTxtPasswdConfTextEnter,
              id=wxID_VTSECPWDPASSWDCHANGEDIALOGTXTPASSWDCONF)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Apply),
              id=wxID_VTSECPWDPASSWDCHANGEDIALOGCBAPPLY, label=_(u'Apply'),
              name=u'cbApply', parent=self, pos=wx.Point(25, 104),
              size=wx.Size(104, 30), style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VTSECPWDPASSWDCHANGEDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Cancel),
              id=wxID_VTSECPWDPASSWDCHANGEDIALOGCBCANCEL, label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(145, 104),
              size=wx.Size(104, 30), style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VTSECPWDPASSWDCHANGEDIALOGCBCANCEL)

        self._init_sizers()

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.doc=None
        self.sLogin=None
        self.iRet=-1
    def SetInfo(self,doc,sLogin):
        self.doc=doc
        self.sLogin=sLogin
        self.txtLogin.SetValue(sLogin)
        self.iRet=-1
    def Clear(self):
        self.doc=None
        self.sLogin=None
    def OnCbApplyButton(self, event):
        if self.doc is None:
            iRet=-100
        else:
            iRet=self.doc.LoginChange(self.sLogin,
                        self.txtPasswd.GetValue(),
                        self.txtPasswdNew.GetValue(),
                        self.txtPasswdConf.GetValue(),bStore=False)
        if iRet==0:
            sMsg=_(u'Wrong password entered.')
        elif iRet==1:
            sMsg=_(u'New passwords do not match.')
        elif iRet==2:
            sMsg=None
        elif iRet==-2:
            sMsg=_(u'Internal structure is faulty.')
        elif iRet==-100:
            sMsg=_(u'Internal structure not set properly.')
        else:
            sMsg=_(u'Unexcepted return value=%d.')%(iRet)
        if sMsg is not None:
            dlg=vtmMsgDialog(self,sMsg ,
                        u'vtSecPwd '+_(u'Password Change'),
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            self.txtPasswdConf.SetValue('')
            self.txtPasswdNew.SetValue('')
            self.txtPasswd.SetValue('')
            self.txtPasswd.SetFocus()
        else:
            iRet=self.doc.LoginChange(self.sLogin,
                        self.txtPasswd.GetValue(),
                        self.txtPasswdNew.GetValue(),
                        self.txtPasswdConf.GetValue(),bStore=True)
            self.doc.Save()
            self.Clear()
            self.iRet=1
            if self.IsModal():
                self.EndModal(1)
            else:
                self.Show(False)
    def OnCbCancelButton(self, event):
        event.Skip()
        self.iRet=0
        self.Clear()
        if self.IsModal():
            self.EndModal(0)
        else:
            self.Show(False)

    def OnTxtPasswdTextEnter(self, event):
        event.Skip()
        self.txtPasswdNew.SetFocus()
    def OnTxtPasswdNewTextEnter(self, event):
        event.Skip()
        self.txtPasswdConf.SetFocus()
    def OnTxtPasswdConfTextEnter(self, event):
        event.Skip()
        self.OnCbApplyButton(None)
