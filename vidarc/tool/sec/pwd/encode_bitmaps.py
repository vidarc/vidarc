#!/usr/bin/env python
#----------------------------------------------------------------------
#----------------------------------------------------------------------------
# Name:         encode_bitmaps.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080306
# CVS-ID:       $Id: encode_bitmaps.py,v 1.1 2008/03/08 00:46:34 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    "-u -i -n Application           img/Safe01_16.ico           images.py",
    "-a -u -n Plugin                img/Safe01_16.png           images.py",
    
    "-a -u -n Login                 img/ID01_16.png             images.py",
    "-a -u -n SurName               img/Usr02_16.png            images.py",
    
    "-u -i -n Splash                img/splashSecPwd01.png      images_splash.py",
    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)

