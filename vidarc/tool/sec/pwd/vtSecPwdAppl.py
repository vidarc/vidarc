#!/usr/bin/env python
#Boa:App:BoaApp
#----------------------------------------------------------------------------
# Name:         vtSecPwdAppl.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080316
# CVS-ID:       $Id: vtSecPwdAppl.py,v 1.1 2008/03/16 22:14:24 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os,sys,getopt,traceback
import wx

from __init__ import *

import vSplashFrame
import vtLogDef
import vtLgBase

import images_splash
modules ={u'vtSecPwdAppl': [1,
                            'Main frame of Application',
                            u'vtSecPwdAppl.py']}

OPTSHORT,OPTLONG='l:f:c:h',['lang=','file=','config=','log=','help']

class BoaApp(wx.App):
    def __init__(self,num,sAppl,cfgFN,sSecPwdFN,iLogLv,iSockPort):
        self.sSecPwdFN=sSecPwdFN
        self.cfgFN=cfgFN
        self.iLogLv=iLogLv
        self.iSockPort=iSockPort
        self.sAppl=sAppl
        #if VIDARC_IMPORT:
        #    self.iOfs=1
        #else:
        #    self.iOfs=0
        wx.App.__init__(self,num)
    def OnInit(self):
        vtLgBase.initAppl(OPTSHORT,OPTLONG,'vtSecPwd')
        wx.InitAllImageHandlers()
        actionList=[]
        actionListPost=[]
        if VIDARC_IMPORT:
            d={'label':u'  import library ...',
                'eval':'__import__("vidImp")'}
            actionList.append(d)
        else:
            d={'label':_(_(u'  import library ...')),
                'eval':'0'}
            actionList.append(d)
        d={'label':_(u'  import %s ...')%'vidarc',
            'eval':'__import__("vidarc",globals(),locals(),["vidarc"])'}
        actionList.append(d)
        d={'label':_(u'  import %s ...')%'vidarc.tool',
            'eval':'__import__("vidarc.tool",globals(),locals(),["vidarc"])'}
        actionList.append(d)
        d={'label':_(u'  import %s ...')%'vidarc.config',
            'eval':'__import__("vidarc.config",globals(),locals(),["vidarc"])'}
        actionList.append(d)
        d={'label':_(u'  import %s ...')%'vidarc.tool.lang',
            'eval':'__import__("vidarc.tool.lang",globals(),locals(),["vidarc"])'}
        actionList.append(d)
        d={'label':_(u'  import %s ...')%'vidarc.tool.InOut',
            'eval':'__import__("vidarc.tool.InOut",globals(),locals(),["vidarc"])'}
        actionList.append(d)
        d={'label':_(u'  import %s ...')%'vidarc.tool.art',
            'eval':'__import__("vidarc.tool.art",globals(),locals(),["vidarc"])'}
        actionList.append(d)
        d={'label':_(u'  import %s ...')%'vidarc.tool.time',
            'eval':'__import__("vidarc.tool.time",globals(),locals(),["vidarc"])'}
        actionList.append(d)
        d={'label':_(u'  import %s ...')%'vidarc.tool.xml',
            'eval':'__import__("vidarc.tool.xml",globals(),locals(),["vidarc"])'}
        actionList.append(d)
        d={'label':_(u'  import %s ...')%'vidarc.tool.input',
            'eval':'__import__("vidarc.tool.input",globals(),locals(),["vidarc"])'}
        actionList.append(d)
        d={'label':_(u'  import %s ...')%'vidarc.tool.log',
            'eval':'__import__("vidarc.tool.log",globals(),locals(),["vidarc"])'}
        actionList.append(d)
        d={'label':_(u'  import %s ...')%'vidarc.tool.sec',
            'eval':'__import__("vidarc.tool.sec",globals(),locals(),["vidarc"])'}
        actionList.append(d)
        d={'label':_(u'  import %s ...')%'vidarc.tool.sec.pwd',
            'eval':'__import__("vidarc.tool.sec.pwd",globals(),locals(),["vidarc"])'}
        actionList.append(d)
        d={'label':_(u'  Open Security ...'),
            'eval':'self.OpenSecPwd("%s")'%(self.sSecPwdFN)}
        actionList.append(d)
        d={'label':u'  import vtSecPwd ...',
            'eval':'__import__("vidarc.tool.sec.pwd.vtSecPwdMainFrame",globals(),locals(),["vidarc"])'}
        actionList.append(d)
        d={'label':_(u'  Create vtSecPwd ...'),
            'eval':'self.res[-1].create'}
        actionList.append(d)
        d={'label':_(u'  Create vtSecPwd ...'),
            'eval':'self.res[-1](None)'}
        actionList.append(d)

        d={'label':u'  Finished.'}
        actionList.append(d)
        
        self.iOfs=len(actionList)-2
        
        d={'label':u'  Open Config ("%s")...'%self.cfgFN,
            'eval':'self.res[%d].OpenCfgFile("%s")'%(self.iOfs,self.cfgFN)}
        actionListPost.append(d)
        d={'label':u'  Open File ("%s")...'%self.sSecPwdFN,
            'eval':'self.res[%d].OpenFile("%s")'%(self.iOfs,self.sSecPwdFN)}
        actionListPost.append(d)
        
        self.splash = vSplashFrame.create(None,'Security Password','Management',
            images_splash.getSplashBitmap(),
            actionList,
            actionListPost,
            self.sAppl,
            self.iLogLv,
            self.iSockPort)
        vSplashFrame.EVT_SPLASH_ACTION(self.splash,self.OnSplashAction)
        vSplashFrame.EVT_SPLASH_PROCESSED(self.splash,self.OnSplashProcessed)
        vSplashFrame.EVT_SPLASH_FINISHED(self.splash,self.OnSplashFinished)
        vSplashFrame.EVT_SPLASH_ABORTED(self.splash,self.OnSplashAborted)
        self.splash.Show()
        return True
    def OnFatalExecption(self):
        try:
            vtLog.vtLngTB('application')
        except:
            traceback.print_exc()
    def OnExceptionInMainLoop(self):
        try:
            vtLog.vtLngTB('application')
        except:
            traceback.print_exc()
    def OnUnhandledException(self):
        try:
            vtLog.vtLngTB('application')
        except:
            traceback.print_exc()
    def OpenCfgFile(self,fn):
        self.cfgFN=fn
    def OnSplashAction(self,evt):
        self.splash.DoProcess()
        evt.Skip()
    def OnSplashProcessed(self,evt):
        #if evt.GetIdx()==4:
        #    self.main=self.splash.res[3]
        #    self.SetTopWindow(self.main)
        #    if self.fn is not None:
        #        self.main.OpenFile(self.fn)
        self.splash.DoAction()
        evt.Skip()
    def OnSplashFinished(self,evt):
        self.main=self.splash.res[self.iOfs]
        self.splash.Show(False)
        sLogin,sPasswd,docSecPwd=self.splash.GetLoginInfo()
        #self.SetTopWindow(self.main)
        #self.main.Show()
        try:
            import vidarc.tool.log.vtLog as vtLog
            vtLog.vtLngSetLevel(self.splash.GetLogLv())
            vtLogDef.LEVEL=self.splash.GetLogLv()
        except:
            pass
        try:
            wx.CallAfter(self.SetTopWindow,self.main)
        except:
            traceback.print_exc()
        try:
            wx.CallAfter(vtLog.InitMsgWid)
        except:
            traceback.print_exc()
        try:
            wx.CallAfter(self.main.Show)
        except:
            traceback.print_exc()
        try:
            self.main.LoginEncoded(sLogin,sPasswd)
        except:
            traceback.print_exc()
        wx.CallAfter(self.splash.Destroy)
        self.splash=None
        evt.Skip()
    def OnSplashAborted(self,evt):
        try:
            self.main=self.splash.res[self.iOfs]
            self.main.Destroy()
        except:
            pass
        self.splash.Destroy()
        self.splash=None
        evt.Skip()

def showHelp():
    print _('help')
    print sys.version
    print "  ",_("valid flags:")
    print "  ",_("-c <filename>")
    print "  ",_("--config <filename>")
    print "        ",_("open the <filename> to configure application")
    print "  ",_("-f <filename>")
    print "  ",_("--file <filename>")
    print "        ",_("open the <filename> at start")
    print "  ","-h"
    print "  ",_("--help")
    print "        ",_("show this screen")
    print "  ","-l"
    print "  ",_("--lang <language id according ISO639>")
    print "  ",_("--log <level>")
    print "       ",_("0 = debug")
    print "       ",_("1 = information")
    print "       ",_("2 = waring")
    print "       ",_("3 = error")
    print "       ",_("4 = critical")
    print "       ",_("5 = fatal")
    
def main(optshort,optlong):
    fn=None
    libFN=None
    cfgFN='vtSecPwdCfg.xml'
    sSecPwdFN=os.getenv('vtSecPwdFN',None)
    
    iLogLv=vtLogDef.ERROR
    global OPTSHORT
    global OPTLONG
    OPTSHORT,OPTLONG=optshort,optlong
    vtLgBase.initAppl(OPTSHORT,OPTLONG,'vtSecPwd')
    try:
        optlist , args = getopt.getopt(sys.argv[1:],optshort,optlong)
        for o in optlist:
            if o[0] in ['-h','--help']:
                vtLgBase.initAppl(OPTSHORT,OPTLONG,'vtSecPwd')
                showHelp()
                return
            if o[0] in ['--file','-f']:
                sSecPwdFN=o[1]
            if o[0] in ['--config','-c']:
                cfgFN=o[1]
            if o[0] in ['--log']:
                if o[1]=='0':
                    iLogLv=vtLogDef.DEBUG
                elif o[1]=='1':
                    iLogLv=vtLogDef.INFO
                elif o[1]=='2':
                    iLogLv=vtLogDef.WARN
                elif o[1]=='3':
                    iLogLv=vtLogDef.ERROR
                elif o[1]=='4':
                    iLogLv=vtLogDef.CRITICAL
                elif o[1]=='5':
                    iLogLv=vtLogDef.FATAL
    except:
        vtLgBase.initAppl(OPTSHORT,OPTLONG,'vtSecPwd')
        showHelp()
        traceback.print_exc()
    if sSecPwdFN is not None:
        sSecPwdFN=sSecPwdFN.replace('\\','\\\\')
    application = BoaApp(0,'vtSecPwd',cfgFN,sSecPwdFN,
                        iLogLv,60090)
    application.MainLoop()

if __name__ == '__main__':
    import vAppl
    vAppl.main('vtSecPwd','f:c:',['file=','config='],main)
