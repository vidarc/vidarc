#----------------------------------------------------------------------------
# Name:         vtSecPwdNodeAccess.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080305
# CVS-ID:       $Id: vtSecPwdNodeAccess.py,v 1.3 2008/03/09 19:41:45 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.time.vtTime import vtDateTime

from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        from vtSecPwdNodeAccessPanel import vtSecPwdNodeAccessPanel
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vtSecPwdNodeAccess(vtXmlNodeBase):
    NODE_ATTRS=[
            ('StartDateTime',None,'startdatetime',None),
            ('EndDateTime',None,'enddatetime',None),
            ('Range',None,'range',None),
        ]
    #FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='access'):
        global _
        _=vtLgBase.assignPluginLang('vtSecPwd')
        vtXmlNodeBase.__init__(self,tagName)
        self.zDt=vtDateTime()
        self.zTmp=vtDateTime()
    def GetDescription(self):
        return _(u'access')
    # ---------------------------------------------------------
    # specific
    def GetStartDateTime(self,node):
        return self.Get(node,'startdatetime')
    def GetEndDateTime(self,node):
        return self.Get(node,'enddatetime')
    def GetRange(self,node):
        return self.Get(node,'range')
    def SetStartDateTime(self,node,val):
        self.Set(node,'startdatetime',val)
    def SetEndDateTime(self,node,val):
        self.Set(node,'enddatetime',val)
    def SetRange(self,node,val):
        self.Set(node,'range',val)
    def __isAccessOk__(self,node,zTmp,zDt,iDay,l):
        sTagName=self.doc.getTagName(node)
        if sTagName!='range':
            return 0
        try:
            s=self.doc.getNodeText(node,'day')
            if len(s)==0:
                return 0
            l[0]=True
            if int(s)!=iDay:
                return 0
            s=self.doc.getNodeText(node,'startdate')
            zTmp.SetDateSmallStr(s)
            s=self.doc.getNodeText(node,'starttime')
            zTmp.SetTimeSmallStr(s)
            fDiff=zTmp.CalcDiffSecFloat(zDt)
            if fDiff<0:
                return 0
            s=self.doc.getNodeText(node,'enddate')
            zTmp.SetDateSmallStr(s)
            s=self.doc.getNodeText(node,'endtime')
            if s.startswith('2400'):
                zTmp.SetTimeStr('23:59:59.9999999')
            else:
                zTmp.SetTimeSmallStr(s)
            fDiff=zTmp.CalcDiffSecFloat(zDt)
            if fDiff<0:
                l[1]=True
                return -1
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return 0
    def IsAccessOk(self,node,zDt=None):
        if zDt is None:
            zDt=vtDateTime()
            #zDt=self.zDt.Now()
        zTmp=vtDateTime()
        l=[False,False]
        iDay=zDt.GetCalendarISO()[2]-1
        self.doc.procChildsExt(node,self.__isAccessOk__,zTmp,zDt,iDay,l)
        if l[0]==True:
            return l[-1]
        return True
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return self.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        return True
    def Is2Add(self):
        return True
    def IsMultiple(self):
        return False
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return True
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\x8dIDAT8\x8d\xa5\x93?K[Q\x18\xc6\x7f\xe7\xe6\xa2vpp\x88$\xd3\t7\r\
\x194\x14\xea\x90(\x85\xe2\xd0\x8b\xa4\x1f\xa0S?@\xed\xa0m)8\x14:t\x13\xa1\r\
\x82_\xc0%\xd9B\xc0x\xe9\x122\x08\xeaR\x84^\n%\xf7\x9a;\x88E\xdbBM\x86\x0c\
\xc2\xeb\x10\x12*x\x93H\x1e8px\xcfy\xfe\xbc\xef\xe1(eD\x18\x07\xc6Xl\xc0\x1c\
\xf5\xa2\xe3T\x05*\x00t:\x9ad2O&\xf3H\xa9A-8NUVV\xf2\xcaq>\x8b\xbdx\x00\xbf\
\xce\xa1\xdd\x82X\x1c\xf7o\x14x\x8fjx\r\xf1</D\xa2\x82\xe7\xcfa?;!yu\xdc\xaf\
\x16J\x1dr\xf3&\xee\x9f\'\x98\xde\xc9\'\xec\xc5\xcb\xf0\xec\x99Sh\xb7o\x91\
\xdfl\xfeT\x8eS\x95\xd6\xf7\xf5n\x82\xa3\x9d<\x97\x17\xe1"k\xcf\xaf!\xfd\x10\
\x80#\xf7\x9a\x7f\xb3[p^\xe7\xc7\xf1.F*\x95V\xd9\xd5*\xdfT\x14\x16>2\xb1\xf4\
\x8a\xaf\xbf\xc1\x9d\xcaQ9\x8b\x92{\xbdOa\xcf\x84V7Ev\xce\xc4\x8en`OnC\xea\
\x1d(#2t\x95\x8a%\xf9\xf2rF\xa48-\xb2\x17\x97\xc3\x0f\x0f\xe4\xed\x8b\xc7\
\xa2\x8c\x08\x03_\xa1^\xab\xc9\xd3\xe5eU\xaf\xd5\x04\xa0\\.wS\xe4\xb2\xc4cq\
\xb4\xd6\xa8\xa0\x19H\x10\x04\xe1C\xbc\x03Zkz\x1c\xe3\xbed\x80\x84e)\xad5@\
\xb7\x85^\xc4anw\x9d\xf5g\xd0\xf4}IX\x96\xea\xed\xffw\x0b3\xb8%0\x0cM\xdf\
\xef\xcf\xaa\x17?aY\x83\xff\xc2(\xb8\x01\x05\xca\x97v\n\x18\xb7\xb7\x00\x00\
\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        return None
    def GetAddDialogClass(self):
        return None
    def GetPanelClass(self):
        if GUI:
            return vtSecPwdNodeAccessPanel
        else:
            return None

