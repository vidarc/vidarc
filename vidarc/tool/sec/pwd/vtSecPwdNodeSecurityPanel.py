#Boa:FramePanel:vtSecPwdNodeSecurityPanel
#----------------------------------------------------------------------------
# Name:         vtSecPwdNodeSecurityPanel.py
# Purpose:      
#               derived from vUserInfoSecurityPanel.py
#
# Author:       Walter Obweger
#
# Created:      20080305
# CVS-ID:       $Id: vtSecPwdNodeSecurityPanel.py,v 1.4 2008/03/20 19:47:44 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors
import wx.grid
import wx.lib.buttons
import sha
#import boa.apps.xmlBase.vtXmlDomTree as vtXmlDomTree

import __config__
from vidarc.tool.xml.vtXmlNodePanel import *
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.misc.vtmMsgDialog import vtmMsgDialog

[wxID_VTSECPWDNODESECURITYPANEL, wxID_VTSECPWDNODESECURITYPANELCBAPPLY, 
 wxID_VTSECPWDNODESECURITYPANELCHKLSTGRP, 
 wxID_VTSECPWDNODESECURITYPANELLBLPASSWD, 
 wxID_VTSECPWDNODESECURITYPANELLBLSECLV, 
 wxID_VTSECPWDNODESECURITYPANELSPNSECLEVEL, 
 wxID_VTSECPWDNODESECURITYPANELTGBPASSWD, 
 wxID_VTSECPWDNODESECURITYPANELTXTPASSWD, 
 wxID_VTSECPWDNODESECURITYPANELTXTPWDCONF, 
] = [wx.NewId() for _init_ctrls in range(9)]

class vtSecPwdNodeSecurityPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.fgsPwd, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.chklstGrp, 1, border=4, flag=wx.TOP | wx.EXPAND)

    def _init_coll_fgsPwd_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblSecLv, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.spnSecLevel, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.lblPasswd, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtPasswd, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtPwdConf, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.tgbPasswd, 0, border=4, flag=wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.cbApply, 0, border=4, flag=wx.LEFT)

    def _init_coll_fgsPwd_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(1)
        parent.AddGrowableCol(2)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=2, vgap=0)

        self.fgsPwd = wx.FlexGridSizer(cols=5, hgap=4, rows=2, vgap=4)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_fgsPwd_Items(self.fgsPwd)
        self._init_coll_fgsPwd_Growables(self.fgsPwd)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTSECPWDNODESECURITYPANEL,
              name=u'vtSecPwdNodeSecurityPanel', parent=prnt, pos=wx.Point(0,
              0), size=wx.Size(385, 317), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(377, 290))
        self.SetAutoLayout(True)

        self.lblSecLv = wx.StaticText(id=wxID_VTSECPWDNODESECURITYPANELLBLSECLV,
              label=_(u'Security Level'), name=u'lblSecLv', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(67, 21), style=wx.ALIGN_RIGHT)
        self.lblSecLv.SetMinSize(wx.Size(-1, -1))

        self.spnSecLevel = wx.SpinCtrl(id=wxID_VTSECPWDNODESECURITYPANELSPNSECLEVEL,
              initial=-1, max=32, min=-1, name=u'spnSecLevel', parent=self,
              pos=wx.Point(71, 0), size=wx.Size(117, 21),
              style=wx.SP_ARROW_KEYS)
        self.spnSecLevel.SetMinSize(wx.Size(-1, -1))

        self.lblPasswd = wx.StaticText(id=wxID_VTSECPWDNODESECURITYPANELLBLPASSWD,
              label=_(u'password'), name=u'lblPasswd', parent=self,
              pos=wx.Point(0, 25), size=wx.Size(67, 30), style=wx.ALIGN_RIGHT)
        self.lblPasswd.SetMinSize(wx.Size(-1, -1))

        self.txtPasswd = wx.TextCtrl(id=wxID_VTSECPWDNODESECURITYPANELTXTPASSWD,
              name=u'txtPasswd', parent=self, pos=wx.Point(71, 25),
              size=wx.Size(119, 30), style=wx.TE_PROCESS_ENTER | wx.TE_PASSWORD,
              value=u'')
        self.txtPasswd.SetMinSize(wx.Size(-1, -1))
        self.txtPasswd.Bind(wx.EVT_TEXT, self.OnTxtPasswdText,
              id=wxID_VTSECPWDNODESECURITYPANELTXTPASSWD)
        self.txtPasswd.Bind(wx.EVT_TEXT_ENTER, self.OnTxtPasswdTextEnter,
              id=wxID_VTSECPWDNODESECURITYPANELTXTPASSWD)

        self.txtPwdConf = wx.TextCtrl(id=wxID_VTSECPWDNODESECURITYPANELTXTPWDCONF,
              name=u'txtPwdConf', parent=self, pos=wx.Point(194, 25),
              size=wx.Size(102, 30), style=wx.TE_PROCESS_ENTER | wx.TE_PASSWORD,
              value=u'')
        self.txtPwdConf.Bind(wx.EVT_TEXT_ENTER, self.OnTxtPwdConfTextEnter,
              id=wxID_VTSECPWDNODESECURITYPANELTXTPWDCONF)
        self.txtPwdConf.Bind(wx.EVT_TEXT, self.OnTxtPwdConfText,
              id=wxID_VTSECPWDNODESECURITYPANELTXTPWDCONF)

        self.chklstGrp = wx.CheckListBox(choices=[],
              id=wxID_VTSECPWDNODESECURITYPANELCHKLSTGRP, name=u'chklstGrp',
              parent=self, pos=wx.Point(0, 59), size=wx.Size(377, 231),
              style=0)
        self.chklstGrp.SetMinSize(wx.Size(-1, -1))

        self.tgbPasswd = wx.lib.buttons.GenBitmapToggleButton(bitmap=wx.EmptyBitmap(16,
              16), id=wxID_VTSECPWDNODESECURITYPANELTGBPASSWD,
              name=u'tgbPasswd', parent=self, pos=wx.Point(304, 25),
              size=wx.Size(30, 30), style=0)
        self.tgbPasswd.Bind(wx.EVT_BUTTON, self.OnTgbPasswdButton,
              id=wxID_VTSECPWDNODESECURITYPANELTGBPASSWD)

        self.cbApply = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Apply),
              id=wxID_VTSECPWDNODESECURITYPANELCBAPPLY, name=u'cbApply',
              parent=self, pos=wx.Point(346, 25), size=wx.Size(31, 30),
              style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VTSECPWDNODESECURITYPANELCBAPPLY)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vtSecPwd')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self)
        self.doc=None
        self.node=None
        self.bModified=False
        self.passwd=None
        self.grps=[]
        
        self.tgbPasswd.SetBitmapLabel(vtArt.getBitmap(vtArt.SecUnlocked))
        self.tgbPasswd.SetBitmapSelected(vtArt.getBitmap(vtArt.SecLocked))
        #lc = wx.LayoutConstraints()
        #lc.top.SameAs       (self.dteStart, wx.Top, 0)
        #lc.left.SameAs      (self.dteStart, wx.Right, 10)
        #lc.bottom.AsIs      ()
        #lc.right.AsIs       ()
        #self.lstDays.SetConstraints(lc)
        self.__showPasswd__()
    def __showPasswd__(self):
        flag=False
        flagAdmin=False
        try:
            if self.VERBOSE:
                vtLog.CallStack('')
                print 'loggedin',self.doc.GetLoggedInSecLv(),'limit',__config__.SEC_LEVEL_LIMIT_ADMIN
            if self.node is not None:
                try:
                    if self.doc.GetLoggedInSecLv()>=__config__.SEC_LEVEL_LIMIT_ADMIN:
                        flag=True
                        flagAdmin=True
                    else:
                        if self.doc.IsLoggedInSelf(self.node):
                            vtLog.vtLngCurWX(vtLog.DEBUG,'IsLoggedInSel',self)
                            flag=True
                except:
                    vtLog.vtLngTB(self.GetName())
        except:
            vtLog.vtLngTB(self.GetName())
        self.tgbPasswd.Enable(flag)
        self.txtPasswd.Enable(flag)
        self.txtPwdConf.Enable(flag)
        self.cbApply.Enable(flag)
        self.spnSecLevel.Enable(flagAdmin)
        self.chklstGrp.Enable(flagAdmin)
        
    def SetModified(self,state):
        self.bModified=state
    def GetModified(self):
        return self.bModified
    
    def Clear(self):
        vtXmlNodePanel.Clear(self)
        self.SetModified(False)
        self.passwd=None
        self.grps=[]
        self.txtPasswd.SetValue('')
        self.txtPwdConf.SetValue('')
        self.tgbPasswd.SetToggle(False)
        self.chklstGrp.Clear()
        self.spnSecLevel.SetValue(-1)
        #self.lstRanges.DeleteAllItems()
        self.selectedIdx=-1
        self.__showPasswd__()
    def SetDoc(self,doc,bNet=False):
        vtXmlNodePanel.SetDoc(self,doc,bNet)

    def SetNode(self,node):
        try:
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
                print node
            if vtXmlNodePanel.SetNode(self,node)<0:
                if self.VERBOSE:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'exit',self)
                    print 'exit'
                return
            self.__showPasswd__()
            self.grps=[]
            grpsNode=self.doc.getChild(self.doc.getBaseNode(),'groups')
            self.chklstGrp.Clear()
            if grpsNode is not None:
                for c in self.doc.getChilds(grpsNode,'group'):
                    id=self.doc.getKey(c)
                    sName=self.doc.getNodeText(c,'name')
                    self.grps.append((sName,id))
            def compFunc(a,b):
                return cmp(a[0],b[0])
            self.grps.sort(compFunc)
            dGrps={}
            i=0
            for sName,id in self.grps:
                try:
                    dGrps[long(id)]=i
                    self.chklstGrp.Append(sName)
                    i+=1
                except:
                    pass
            try:
                i=int(self.doc.getNodeText(node,'sec_level'))
            except:
                i=-1
            self.spnSecLevel.SetValue(i)
            
            self.passwd=passwd=self.doc.getNodeText(node,'passwd')
            if self.VERBOSE:
                print 'passwd',self.passwd
            if len(self.passwd)==0:
                self.passwd=None
                self.tgbPasswd.SetToggle(False)
            else:
                self.txtPasswd.SetValue(u'*'*8)
                self.txtPwdConf.SetValue(u'*'*8)
                self.tgbPasswd.SetToggle(True)
                self.passwd=passwd
            # setup attributes
            for c in self.doc.getChilds(node,'grp'):
                try:
                    i=dGrps[long(self.doc.getAttribute(c,'fid'))]
                    self.chklstGrp.Check(i,True)
                except:
                    pass
            if self.VERBOSE:
                print 'passwd',self.passwd
        except:
            vtLog.vtLngTB(self.GetName())
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
                print node
                print 'passwd',self.passwd
            if node is None:
                return
            bModSec=True
            bModPasswd=True
            if self.doc.GetLoggedInSecLv()<__config__.SEC_LEVEL_LIMIT_ADMIN:
                #self.SetModified(False)
                #return
                bModSec=False
                bModPasswd=False
            if self.doc.IsLoggedInSelf(node):
                bModPasswd=True
            if bModPasswd:
                if self.passwd is None:
                    if self.tgbPasswd.GetToggle():
                        if self.txtPasswd.GetValue()!=self.txtPwdConf.GetValue():
                            #self.doc.setNodeText(node,'passwd','')
                            pass
                        else:
                            passwd=self.txtPasswd.GetValue()
                            print passwd,self.doc.getNodeText(node,'passwd')
                            if len(passwd)>0:
                                m=sha.new()
                                m.update(passwd)
                                self.doc.setNodeText(node,'passwd',m.hexdigest())
                            else:
                                self.doc.setNodeText(node,'passwd','')
                            print passwd,self.doc.getNodeText(node,'passwd')
                    else:
                        self.doc.setNodeText(node,'passwd','')
                else:
                    self.doc.setNodeText(node,'passwd',self.passwd)
            if bModSec:
                self.doc.setNodeText(node,'sec_level','%d'%self.spnSecLevel.GetValue())
                childs=self.doc.getChilds(node,'grp')
                iChilds=len(childs)
                grps=[]
                for i in range(self.chklstGrp.GetCount()):
                    if self.chklstGrp.IsChecked(i):
                        grps.append(i)
                iLen=len(grps)
                for i in range(iLen):
                    try:
                        child=childs[i]
                    except:
                        child=self.doc.createSubNode(node,'grp')
                    self.doc.setAttribute(child,'fid',self.grps[grps[i]][1])
                    #self.doc.setNodeText(child,'name',self.grps[grps[i]][0])
                for i in range(iLen,iChilds):
                    self.doc.deleteNode(childs[i],node)
            self.GetNodeFin(node)
        except:
            vtLog.vtLngTB(self.GetName())
    def GetGroupLst(self):
        grps=[]
        for i in range(self.chklstGrp.GetCount()):
            if self.chklstGrp.IsChecked(i):
                grps.append(self.grps[i][1])
        return grps
    def Lock(self,flag,locker=''):
        if flag:
            self.spnSecLevel.Enable(False)
            self.chklstGrp.Enable(False)
            self.tgbPasswd.Enable(False)
            self.txtPasswd.Enable(False)
            self.txtPwdConf.Enable(False)
            self.cbApply.Enable(False)
        else:
            self.__showPasswd__()
            #self.spnSecLevel.Enable(True)
            #self.chklstGrp.Enable(True)

    def OnTxtPasswdText(self, event):
        event.Skip()
        self.passwd=None
        if len(self.txtPasswd.GetValue())>0:
            self.tgbPasswd.SetToggle(False)
    def OnTxtPwdConfText(self, event):
        event.Skip()
        self.passwd=None
        if len(self.txtPwdConf.GetValue())>0:
            self.tgbPasswd.SetToggle(False)
    def OnTgbPasswdButton(self, event):
        if self.tgbPasswd.GetToggle()==False:
            self.passwd=None
            self.txtPasswd.SetValue(u'')
            self.txtPwdConf.SetValue(u'')
            self.txtPasswd.SetFocus()
        else:
            if self.txtPasswd.GetValue()=='':
                self.tgbPasswd.SetValue(False)
            elif self.txtPasswd.GetValue()!=self.txtPwdConf.GetValue():
                dlg=vtmMsgDialog(self,_(u'Passwords do not match.'),
                        u'vtSecPwd' + u' '+_(u'password'),
                        wx.OK|wx.ICON_EXCLAMATION)
                dlg.ShowModal()
                self.tgbPasswd.SetValue(False)
                self.txtPasswd.SetValue(u'')
                self.txtPwdConf.SetValue(u'')
                self.txtPasswd.SetFocus()
            else:
                self.passwd=None
        event.Skip()

    def OnTxtPasswdTextEnter(self, event):
        event.Skip()
        self.txtPwdConf.SetFocus()
    def OnTxtPwdConfTextEnter(self, event):
        event.Skip()
        self.tgbPasswd.SetFocus()

    def OnCbApplyButton(self, event):
        event.Skip()
        try:
            node=self.node
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
                print node
                print 'passwd',self.passwd
            if node is None:
                return
            bModSec=True
            bModPasswd=True
            if self.doc.GetLoggedInSecLv()<__config__.SEC_LEVEL_LIMIT_ADMIN:
                #self.SetModified(False)
                #return
                bModSec=False
                bModPasswd=False
            if self.doc.IsLoggedInSelf(node):
                bModPasswd=True
            if bModPasswd:
                if self.passwd is None:
                    if self.tgbPasswd.GetToggle():
                        if self.txtPasswd.GetValue()!=self.txtPwdConf.GetValue():
                            #self.doc.setNodeText(node,'passwd','')
                            pass
                        else:
                            passwd=self.txtPasswd.GetValue()
                            print passwd,self.doc.getNodeText(node,'passwd')
                            if len(passwd)>0:
                                m=sha.new()
                                m.update(passwd)
                                self.doc.setNodeText(node,'passwd',m.hexdigest())
                            else:
                                self.doc.setNodeText(node,'passwd','')
                            print passwd,self.doc.getNodeText(node,'passwd')
                    else:
                        self.doc.setNodeText(node,'passwd','')
                else:
                    self.doc.setNodeText(node,'passwd',self.passwd)
        except:
            vtLog.vtLngTB(self.GetName())
