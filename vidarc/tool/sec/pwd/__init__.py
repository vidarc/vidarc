#----------------------------------------------------------------------------
# Name:         __init__.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20080305
# CVS-ID:       $Id: __init__.py,v 1.2 2008/03/26 23:16:38 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

PYTHON_LOGGIN_SOCKET=0

try:
    if hasattr(sys, 'importers'):
        try:
            from build_options import *
        except:
            VIDARC_IMPORT=1
    else:
        try:
            from build_options import *
        except:
            VIDARC_IMPORT=0
except:
    VIDARC_IMPORT=1
