#Boa:FramePanel:vtSecPwdNodeAccessPanel
#----------------------------------------------------------------------------
# Name:         vtSecPwdNodeAccessPanel.py
# Purpose:      
#               derived from vUserInfoAccessPanel.py
#
# Author:       Walter Obweger
#
# Created:      20080305
# CVS-ID:       $Id: vtSecPwdNodeAccessPanel.py,v 1.4 2010/03/03 02:17:11 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.time.vtTimeTimeEdit
import vidarc.tool.time.vtTimeDateGM
import wx.lib.buttons

from vidarc.tool.xml.vtXmlNodePanel import *
from vidarc.tool.time.vTimeDateGM import *
from vidarc.tool.time.vTimeTimeEdit import *

import sys,calendar

import __config__
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.tool.art.vtArt as vtArt


[wxID_VTSECPWDNODEACCESSPANEL, wxID_VTSECPWDNODEACCESSPANELCBADD, 
 wxID_VTSECPWDNODEACCESSPANELCBCLR, wxID_VTSECPWDNODEACCESSPANELCBDEL, 
 wxID_VTSECPWDNODEACCESSPANELDTEEND, wxID_VTSECPWDNODEACCESSPANELDTESTART, 
 wxID_VTSECPWDNODEACCESSPANELLBLENDDATE, wxID_VTSECPWDNODEACCESSPANELLBLFROM, 
 wxID_VTSECPWDNODEACCESSPANELLBLSTARTDATE, wxID_VTSECPWDNODEACCESSPANELLBLTO, 
 wxID_VTSECPWDNODEACCESSPANELLSTDAYS, wxID_VTSECPWDNODEACCESSPANELLSTRANGES, 
 wxID_VTSECPWDNODEACCESSPANELTMEEND, wxID_VTSECPWDNODEACCESSPANELTMESTART, 
] = [wx.NewId() for _init_ctrls in range(14)]

class vtSecPwdNodeAccessPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    TAGS=['startdate','enddate','day','starttime','endtime']
    def _init_coll_fgsRg_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_fgsRg_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstRanges, 1, border=4, flag=wx.EXPAND)
        parent.AddSizer(self.bxsBt, 1, border=4, flag=wx.RIGHT | wx.LEFT)

    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.fgsDtDisp, 0, border=4,
              flag=wx.BOTTOM | wx.RIGHT | wx.TOP | wx.LEFT | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.fgsRg, 1, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)

    def _init_coll_fgsDtDisp_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)
        parent.AddGrowableCol(1)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbDel, 0, border=4, flag=wx.TOP)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbClr, 0, border=0, flag=0)

    def _init_coll_fgsDtDisp_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.fgsDt, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lstDays, 0, border=4,
              flag=wx.LEFT | wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.cbAdd, 0, border=4, flag=wx.RIGHT | wx.LEFT)

    def _init_coll_fgsDt_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)
        parent.AddGrowableCol(1)

    def _init_coll_fgsDt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblStartDate, 0, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.dteStart, 0, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblEndDate, 0, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddWindow(self.dteEnd, 0, border=4,
              flag=wx.TOP | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblFrom, 0, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddWindow(self.tmeStart, 0, border=4,
              flag=wx.TOP | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblTo, 0, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddWindow(self.tmeEnd, 0, border=4,
              flag=wx.TOP | wx.LEFT | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self.fgsDt = wx.FlexGridSizer(cols=2, hgap=0, rows=4, vgap=0)

        self.fgsDtDisp = wx.FlexGridSizer(cols=3, hgap=0, rows=1, vgap=0)

        self.fgsRg = wx.FlexGridSizer(cols=2, hgap=0, rows=1, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_fgsDt_Items(self.fgsDt)
        self._init_coll_fgsDt_Growables(self.fgsDt)
        self._init_coll_fgsDtDisp_Items(self.fgsDtDisp)
        self._init_coll_fgsDtDisp_Growables(self.fgsDtDisp)
        self._init_coll_fgsRg_Growables(self.fgsRg)
        self._init_coll_fgsRg_Items(self.fgsRg)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTSECPWDNODEACCESSPANEL,
              name=u'vtSecPwdNodeAccessPanel', parent=prnt, pos=wx.Point(2, 1),
              size=wx.Size(469, 280), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(461, 253))
        self.SetAutoLayout(True)

        self.lblStartDate = wx.StaticText(id=wxID_VTSECPWDNODEACCESSPANELLBLSTARTDATE,
              label=_(u'Start Date'), name=u'lblStartDate', parent=self,
              pos=wx.Point(4, 4), size=wx.Size(52, 24), style=wx.ALIGN_RIGHT)
        self.lblStartDate.SetMinSize(wx.Size(-1, -1))

        self.dteStart = vidarc.tool.time.vtTimeDateGM.vtTimeDateGM(id=wxID_VTSECPWDNODEACCESSPANELDTESTART,
              name=u'dteStart', parent=self, pos=wx.Point(60, 4),
              size=wx.Size(165, 24), style=0)

        self.lblEndDate = wx.StaticText(id=wxID_VTSECPWDNODEACCESSPANELLBLENDDATE,
              label=_(u'End Date'), name=u'lblEndDate', parent=self,
              pos=wx.Point(4, 32), size=wx.Size(52, 24), style=wx.ALIGN_RIGHT)
        self.lblEndDate.SetMinSize(wx.Size(-1, -1))

        self.dteEnd = vidarc.tool.time.vtTimeDateGM.vtTimeDateGM(id=wxID_VTSECPWDNODEACCESSPANELDTEEND,
              name=u'dteEnd', parent=self, pos=wx.Point(60, 32),
              size=wx.Size(165, 24), style=0)

        self.lblFrom = wx.StaticText(id=wxID_VTSECPWDNODEACCESSPANELLBLFROM,
              label=_(u'From'), name=u'lblFrom', parent=self, pos=wx.Point(4,
              60), size=wx.Size(52, 24), style=wx.ALIGN_RIGHT)
        self.lblFrom.SetMinSize(wx.Size(-1, -1))

        self.tmeStart = vidarc.tool.time.vtTimeTimeEdit.vtTimeTimeEdit(id=wxID_VTSECPWDNODEACCESSPANELTMESTART,
              name=u'tmeStart', parent=self, pos=wx.Point(60, 60),
              size=wx.Size(165, 24), style=0)

        self.lblTo = wx.StaticText(id=wxID_VTSECPWDNODEACCESSPANELLBLTO,
              label=_(u'To'), name=u'lblTo', parent=self, pos=wx.Point(4, 88),
              size=wx.Size(52, 24), style=wx.ALIGN_RIGHT)
        self.lblTo.SetMinSize(wx.Size(-1, -1))

        self.tmeEnd = vidarc.tool.time.vtTimeTimeEdit.vtTimeTimeEdit(id=wxID_VTSECPWDNODEACCESSPANELTMEEND,
              name=u'tmeEnd', parent=self, pos=wx.Point(60, 88),
              size=wx.Size(165, 24), style=0)

        self.lstDays = wx.CheckListBox(choices=[],
              id=wxID_VTSECPWDNODEACCESSPANELLSTDAYS, name=u'lstDays',
              parent=self, pos=wx.Point(229, 4), size=wx.Size(144, 108),
              style=0)
        self.lstDays.Bind(wx.EVT_CHECKLISTBOX, self.OnLstDaysChecklistbox,
              id=wxID_VTSECPWDNODEACCESSPANELLSTDAYS)

        self.cbAdd = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Add),
              id=wxID_VTSECPWDNODEACCESSPANELCBADD, label=_(u'Add'),
              name=u'cbAdd', parent=self, pos=wx.Point(381, 4), size=wx.Size(72,
              30), style=0)
        self.cbAdd.Bind(wx.EVT_BUTTON, self.OnCbAddButton,
              id=wxID_VTSECPWDNODEACCESSPANELCBADD)

        self.lstRanges = wx.ListCtrl(id=wxID_VTSECPWDNODEACCESSPANELLSTRANGES,
              name=u'lstRanges', parent=self, pos=wx.Point(4, 128),
              size=wx.Size(373, 121), style=wx.LC_REPORT)
        self.lstRanges.SetMinSize(wx.Size(-1, -1))
        self.lstRanges.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstRangesListItemSelected,
              id=wxID_VTSECPWDNODEACCESSPANELLSTRANGES)
        self.lstRanges.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstRangesListItemDeselected,
              id=wxID_VTSECPWDNODEACCESSPANELLSTRANGES)

        self.cbDel = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Cancel),
              id=wxID_VTSECPWDNODEACCESSPANELCBDEL, label=_(u'Delete'),
              name=u'cbDel', parent=self, pos=wx.Point(381, 132),
              size=wx.Size(72, 30), style=0)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VTSECPWDNODEACCESSPANELCBDEL)

        self.cbClr = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Del),
              id=wxID_VTSECPWDNODEACCESSPANELCBCLR, label=_(u'Clear'),
              name=u'cbClr', parent=self, pos=wx.Point(381, 170),
              size=wx.Size(72, 30), style=0)
        self.cbClr.Bind(wx.EVT_BUTTON, self.OnCbClrButton,
              id=wxID_VTSECPWDNODEACCESSPANELCBCLR)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vHum')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,lWidgets=[self.lstRanges])
        self.ranges=[]
        self.dt=wx.DateTime()
        self.dteStart.SetEnableMark(False)
        self.dteEnd.SetEnableMark(False)
        self.tmeStart.SetEnableMark(False)
        self.tmeEnd.SetEnableMark(False)
        
        self.__showDays__()
        
        # setup list
        self.lstRanges.InsertColumn(0,_(u'Start'),wx.LIST_FORMAT_LEFT,70)
        self.lstRanges.InsertColumn(1,_(u'End'),wx.LIST_FORMAT_LEFT,70)
        self.lstRanges.InsertColumn(2,_(u'Day'),wx.LIST_FORMAT_LEFT,40)
        self.lstRanges.InsertColumn(3,_(u'From'),wx.LIST_FORMAT_LEFT,70)
        self.lstRanges.InsertColumn(4,_(u'To'),wx.LIST_FORMAT_LEFT,70)
        self.SetupImageList()
    def SetupImageList(self):
        self.imgDict={}
        self.imgLstTyp=wx.ImageList(16,16)
        #img=images_hum_tree.getTelBitmap()
        #self.imgDict['tel']=self.imgLstTyp.Add(img)
        #self.lstRanges.SetImageList(self.imgLstTyp,wx.IMAGE_LIST_SMALL)
    
    def ClearWid(self):
        vtXmlNodePanel.ClearWid(self)
        #self.SetModified(False)
        self.lstRanges.DeleteAllItems()
        self.selectedIdx=-1
        self.ranges=[]
    def __showDays__(self):
        self.lstDays.Clear()
        for i in range(7):
            self.lstDays.Append(calendar.day_name[i])
        
    def SetDoc(self,doc,bNet=False):
        vtXmlNodePanel.SetDoc(self,doc,bNet)

    def SetNode(self,node):
        try:
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
                print node
            if vtXmlNodePanel.SetNode(self,node)<0:
                if self.VERBOSE:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'exit',self)
                    print 'exit'
                return
            #self.Clear()
            sStartDT=self.doc.getNodeText(node,'startdatetime')
            sEndDT=self.doc.getNodeText(node,'enddatetime')
            
            # setup attributes
            for rangeNode in self.doc.getChilds(node,'range'):
                range={}
                for k in self.TAGS:
                    val=self.doc.getNodeText(rangeNode,k)
                    range[k]=val
                self.ranges.append(range)
            self.__showRanges__()
        except:
            vtLog.vtLngTB(self.GetName())
    def __showRanges__(self):
        self.lstRanges.DeleteAllItems()
        self.selectedIdx=-1
        def compFunc(a,b):
            for i in [0,2,3]:
                res=cmp(a[self.TAGS[i]],b[self.TAGS[i]])
                if res!=0:
                    return res
            return 0
        self.ranges.sort(compFunc)
        for rng in self.ranges:
            try:
                if rng['startdate']==u'':
                    continue
                self.dt.ParseFormat(rng['startdate'],'%Y%m%d')
                val=self.dt.FormatISODate()
                index = self.lstRanges.InsertImageStringItem(sys.maxint, val, -1)
                i=1
                for k in self.TAGS[1:]:
                    val=rng[k]
                    if k in ['startdate','enddate']:
                        self.dt.ParseFormat(val,'%Y%m%d')
                        val=self.dt.FormatISODate()
                    elif k in ['starttime','endtime']:
                        self.dt.ParseFormat(val,'%H%M%S')
                        val=val[0:2]+':'+val[2:4]+':'+val[4:6]
                    elif k=='day':
                        try:
                            val=calendar.day_abbr[int(val)]
                        except:
                            pass
                    self.lstRanges.SetStringItem(index, i, val)
                    i+=1
            except:
                pass
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
                print node
            if node is None:
                return
            self.SetModified(False)
            if self.doc.GetLoggedInSecLv()<__config__.SEC_LEVEL_LIMIT_ADMIN:
                return
            #sStartDT=self.zdtlStartGM.GetValue()
            #sEndDT=self.zdtlEndGM.GetValue()
            
            #self.doc.setNodeText(node,'startdatetime',sStartDT)
            #self.doc.setNodeText(node,'enddatetime',sEndDT)
            def validate(rng):
                for v in rng.itervalues():
                    if len(v)==0:
                        return False
                return True
            self.ranges=[rng for rng in self.ranges if validate(rng)]
            def cmpFunc(rngA,rngB):
                for k in self.TAGS:
                    i=cmp(rngA[k],rngB[k])
                    if i!=0:
                        return i
                return 0
            self.ranges.sort()
            childs=self.doc.getChilds(node,'range')
            iChilds=len(childs)
            iLen=len(self.ranges)
            for i in range(iLen):
                try:
                    child=childs[i]
                except:
                    child=self.doc.createSubNode(node,'range')
                for k in self.TAGS:
                    print k
                    print self.ranges[i][k]
                    self.doc.setNodeText(child,k,self.ranges[i][k])
            for i in range(iLen,iChilds):
                self.doc.deleteNode(childs[i],node)
            self.doc.AlignNode(node,iRec=5)
            self.GetNodeFin(node)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbAddButton(self,event):
        if self.selectedIdx<0:
            for i in range(self.lstDays.GetCount()):
                if self.lstDays.IsChecked(i):
                    self.ranges.append({'startdate':self.dteStart.GetValue(),
                        'enddate':self.dteEnd.GetValue(),
                        'starttime':self.tmeStart.GetTimeStr(),
                        'endtime':self.tmeEnd.GetTimeStr(),
                        'day':str(i)})
        else:
            rng=self.ranges[self.selectedIdx]
            rng['startdate']=self.dteStart.GetValue()
            rng['enddate']=self.dteEnd.GetValue()
            rng['starttime']=self.tmeStart.GetTimeStr()
            rng['endtime']=self.tmeEnd.GetTimeStr()
            #range['day']=i
        self.__showRanges__()
        self.SetModified(True,self.lstRanges)
        event.Skip()
        
    def OnCbClrButton(self, event):
        self.lstRanges.DeleteAllItems()
        self.selectedIdx=-1
        self.ranges=[]
        self.SetModified(True,self.lstRanges)
        event.Skip()
    def OnCbDelButton(self, event):
        if self.selectedIdx>=0:
            self.ranges=self.ranges[:self.selectedIdx]+self.ranges[self.selectedIdx+1:]
            self.__showRanges__()
        self.SetModified(True,self.lstRanges)
        event.Skip()
    def OnLstRangesListItemSelected(self, event):
        idx=event.GetIndex()
        self.selectedIdx=idx
        rng=self.ranges[idx]
        self.dteStart.SetValue(rng['startdate'])
        self.dteEnd.SetValue(rng['enddate'])
        self.tmeStart.SetTimeStr(rng['starttime'])
        self.tmeEnd.SetTimeStr(rng['endtime'])
        try:
            iDay=int(rng['day'])
        except:
            iDay=-1
            pass
        
        for i in range(self.lstDays.GetCount()):
            self.lstDays.Check(i,i==iDay)
        
    def OnLstRangesListItemDeselected(self, event):
        self.selectedIdx=-1
        self.lstDays.Enable(True)
        
    def Lock(self,flag,locker=''):
        try:
            if self.doc.GetLoggedInSecLv()<__config__.SEC_LEVEL_LIMIT_ADMIN:
                flag=True
        except:
            flag=True
        if flag:
            self.dteStart.Enable(False)
            self.dteEnd.Enable(False)
            self.tmeStart.Enable(False)
            self.tmeEnd.Enable(False)
            self.lstDays.Enable(False)
            self.lstRanges.Enable(False)
            self.cbAdd.Enable(False)
            self.cbDel.Enable(False)
            self.cbClr.Enable(False)
        else:
            self.dteStart.Enable(True)
            self.dteEnd.Enable(True)
            self.tmeStart.Enable(True)
            self.tmeEnd.Enable(True)
            self.lstDays.Enable(True)
            self.lstRanges.Enable(True)
            self.cbAdd.Enable(True)
            self.cbDel.Enable(True)
            self.cbClr.Enable(True)

    def OnLstDaysChecklistbox(self, event):
        if self.selectedIdx>=0:
            rng=self.ranges[self.selectedIdx]
            try:
                iDay=int(rng['day'])
            except:
                iDay=event.GetSelection()
                pass
            for i in range(self.lstDays.GetCount()):
                self.lstDays.Check(i,i==iDay)
        event.Skip()

