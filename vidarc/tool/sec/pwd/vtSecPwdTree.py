#----------------------------------------------------------------------------
# Name:         vtSecPwdTree.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080307
# CVS-ID:       $Id: vtSecPwdTree.py,v 1.2 2008/03/08 00:46:34 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlGrpTreeReg import vtXmlGrpTreeReg

import vidarc.tool.sec.pwd.images as imgSecPwd

class vtSecPwdTree(vtXmlGrpTreeReg):
    def __init__(self, parent, id, pos, size, style, name,
                    master=False,controller=False,verbose=0):
        vtXmlGrpTreeReg.__init__(self,id=id, name=name,master=master,
              controller=controller,verbose=verbose,
              parent=parent, pos=pos, size=size,style=style)
        self.bLangMenu=True
        self.bGrpMenu=True
        self.bGrouping=True
        self.bGroupingDict=True
        return
        self.SetPossibleGrouping([
                ('normal'   , _(u'normal'), [] , 
                            {None:[('tag','',),('name','')],
                            #'user':[('name',''),('surname',''),('firstname','')],
                            #'group':[('name','')]
                            }),
                ('category'   , _(u'category'), [('category',''),] , 
                            {None:[('tag','',),('name','')],
                            }),
                ])
    def SetDftNodeInfosOld(self):
        self.SetNodeInfos(['tag','name','|id'])
    #def getGroupingOverlayImages(self):
    #    return imgSecPwd.getGroupingImage(),imgSecPwd.getGroupingImage()
    def SetupImageList(self):
        if vtXmlGrpTreeReg.SetupImageList(self):
            self.__addElemImage2ImageList__('root',
                            imgSecPwd.getPluginImage(),
                            imgSecPwd.getPluginImage(),True)
            self.__addElemImage2ImageList__('humans',
                            imgSecPwd.getPluginImage(),
                            imgSecPwd.getPluginImage(),True)
            imgGrp=self.getGroupingOverlayImages()
            imgNor=imgSecPwd.getLoginImage()
            imgSel=imgSecPwd.getLoginImage()
            self.__mergeImg__(imgNor,imgGrp[0],mergeMask=[255,255,255])
            self.__mergeImg__(imgSel,imgGrp[1],mergeMask=[255,255,255])
            self.__addImage2ImageList__('grp','Login',
                            imgNor,imgSel,False)
            imgNor=imgSecPwd.getSurNameImage()
            imgSel=imgSecPwd.getSurNameImage()
            self.__mergeImg__(imgNor,imgGrp[0],mergeMask=[255,255,255])
            self.__mergeImg__(imgSel,imgGrp[1],mergeMask=[255,255,255])
            self.__addImage2ImageList__('grp','SurName',
                            imgNor,imgSel,True)
            #self.__processImgList__()
            self.SetImageList(self.imgLstTyp)
