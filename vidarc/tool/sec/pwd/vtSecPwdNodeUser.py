#----------------------------------------------------------------------------
# Name:         vtSecPwdNodeUser.py
# Purpose:      user xml data object
#
# Author:       Walter Obweger
#
# Created:      20080305
# CVS-ID:       $Id: vtSecPwdNodeUser.py,v 1.3 2008/03/09 19:41:45 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeBase import vtXmlNodeBase
try:
    if vcCust.is2Import(__name__):
        from vtSecPwdNodeUserPanel import vtSecPwdNodeUserPanel
        from vtSecPwdNodeUserEditDialog import vtSecPwdNodeUserEditDialog
        from vtSecPwdNodeUserAddDialog import vtSecPwdNodeUserAddDialog
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vtSecPwdNodeUser(vtXmlNodeBase):
    NODE_ATTRS=[
            ('Login',None,'login',None),
            ('SurName',None,'surname',None),
            ('FirstName',None,'firstname',None),
        ]
    FUNCS_GET_SET_4_LST=['SurName','FirstName','Login']
    FUNCS_GET_4_TREE=['Login','SurName','FirstName']
    FMT_GET_4_TREE=[('Login',''),('SurName',''),('FirstName','')]
    #GRP_GET_4_TREE=[('Login',''),('SurName','')]
    def __init__(self,tagName='user'):
        global _
        _=vtLgBase.assignPluginLang('vtSecPwd')
        vtXmlNodeBase.__init__(self,tagName)
        self.GRP_MENU_4_TREE=[
                ('normal',  _(u'normal'),
                    [],
                    [('Login',''),('SurName',''),('FirstName','')],
                ),
                ('logingrp',  _(u'group by login'),
                    [('Login',',3')],
                    [('Login',''),('SurName',''),('FirstName','')],
                ),
                ('surname',  _(u'surname'),
                    [],
                    [('SurName',''),('FirstName',''),('Login','')],
                ),
                ('surnamegrp',  _(u'group by surname'),
                    [('SurName',',6')],
                    [('SurName',''),('FirstName',''),('Login','')],
                ),
                ]
    def GetDescription(self):
        return _(u'user')
    # ---------------------------------------------------------
    # specific
    def GetTag(self,node):
        return self.GetLogin(node)
    def GetLogin(self,node):
        return self.Get(node,'login')
    def GetSurName(self,node):
        return self.Get(node,'surname')
    def GetFirstName(self,node):
        return self.Get(node,'firstname')
    def GetInActive(self,node):
        sVal=self.Get(node,'inactive')
        return sVal in ['1','True']
    def GetGrpIds(self,node):
        sVal=self.Get(node,'grps')
        l=[]
        for s in sVal.split(','):
            try:
                l.append(long(s))
            except:
                pass
        return l
    def GetGrps(self,node):
        try:
            oGrp=self.doc.GetReg('group')
            lIDs=self.GetGrpIds(node)
            l=[]
            for id in lIDs:
                n=self.doc.getNodeByIdNum(id)
                if n is not None:
                    if self.doc.getTagName(n)=='group':
                        l.append(oGrp.GetTag(n))
            return l
        except:
            vtLog.vtLngTB(self.__class__.__name__)
            return []
    def SetTag(self,node,val):
        self.SetLogin(node,val)
    def SetLogin(self,node,val):
        self.Set(node,'login',val)
    def SetSurName(self,node,val):
        self.Set(node,'surname',val)
    def SetFirstName(self,node,val):
        self.Set(node,'firstname',val)
    def SetInActive(self,node,val):
        if val:
            sVal='1'
        else:
            sVal='0'
        self.Set(node,'inactive',sVal)
    def IsAccessOk(self,node,zDt=None):
        if node is None:
            return False
        c=self.doc.getChild(node,'access')
        if c is None:
            return True
        oAccess=self.doc.GetReg('access')
        return oAccess.IsAccessOk(c,zDt)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return self.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01%IDAT8\x8d\xa5\x93=K\xc3P\x14\x86\x9f\xa4\x9a.\xb6D\xa9 ]\xa4C\xaa7\
\xa3uU3\x15\x07A\xc4\xbf\xe2\xac\xd9\xec\xd2\xdf\xa1\xa3\x83? U\xdc\xac\x9d4\
E\x11\xcc Q\x9a\xad]\xcbq\xa8\x15\x9a/\xac\x1e8p?\x0e\xcf}\xcf{\xef\xd54\xbd\
\xc0\x7fB\xcf\xda(\x1a\x8bb\x9ae\xf9\x13`\xbf\xd9\x94\xe0-\xa0u\xde\xa2RY\
\xc9\x85\xa4\x02,\xcb\xc2(\x1a\xd4\xd6k\x98\xcbf\xae\x82\x85\xb4\xc5\xf0\xf5\
\x82v\xbbL\xf8\x19\x12\r\xa2\\\x00\x9a^\x98I\xf7\xc4\x11\xe9\xba\xe26\x10\
\xb7\x81x\x07\x88\xb3\x8d\xc4\xeb\xa6\x99T0\xf2@\xf78\xbbv'\xf3\x07\x17\xe7\
\x14:\xf3\xb4\x00\xc0\xc77\xe0*\xbf\x83\xcck\xfcm$\x00\xde}\xb2h4\xcc!\xa4\
\x19#=dx\x89H\x0fy\xd9\xcd60\xdd\xc4\xe9\xa9}\xe0\x1d\xd6\xaa\xb0\xa9\xea\
\x02\xd0\xf7\x9f\xb5x]\xb6\x89\xc0R\t(M\xc6\xcaV([\x89\xff\xe4\xcf\x80\xe66Q\
\xd9\x8a\xa3\xe3\xc3\x9f\xe7\x9d\xaa\xa0\xd3\x85\x8d\x0c@\\\x81\x96\xf5\x9d\
\xf7\xb6\xc6\xe2T\xe11Xe\\\xdf!\x1aD\xdc\xde\xdc%<\xf8\x02;\x99Y\xc7\xf9\xc9\
\x06\xfb\x00\x00\x00\x00IEND\xaeB`\x82"
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return vtSecPwdNodeUserEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return vtSecPwdNodeUserAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vtSecPwdNodeUserPanel
        else:
            return None
