#Boa:FramePanel:vtSecPwdMainPanel
#----------------------------------------------------------------------------
# Name:         vtSecPwdMainPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080306
# CVS-ID:       $Id: vtSecPwdMainPanel.py,v 1.4 2008/03/20 19:47:44 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.xml.vtXmlNodeRegListBook
from wx.lib.anchors import LayoutAnchors
import vidarc.tool.xml.vtXmlNodeRegSelector
import time

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase

try:
    import vidarc.vApps.common.vSystem as vSystem
    import vidarc.config.vcCust as vcCust
    
    from vidarc.tool.sec.pwd.vtSecPwdXml import vtSecPwdXml
    from vidarc.tool.sec.pwd.vtSecPwdTree import vtSecPwdTree
    
    from vidarc.tool.xml.vtXmlDom import vtXmlDom
    from vidarc.tool.xml.vtXmlFindQuickThread import vtXmlFindQuickThread
    from vidarc.tool.xml.vtXmlFindQuickThread import EVT_VTXML_FIND_QUICK_FOUND
    
    import vidarc.tool.sec.pwd.images as imgSecPwd
except:
    vtLog.vtLngTB('import')

def create(parent):
    return vtSecPwdMainPanel(parent)

[wxID_VTSECPWDMAINPANEL, wxID_VTSECPWDMAINPANELPNDATA, 
 wxID_VTSECPWDMAINPANELSLWNAV, wxID_VTSECPWDMAINPANELVIREGSEL, 
] = [wx.NewId() for _init_ctrls in range(4)]

def getPluginImage():
    return imgSecPwd.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(imgSecPwd.getPluginBitmap())
    return icon

class vtSecPwdMainPanel(wx.Panel):
    VERBOSE=0
    def _init_coll_bxsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viRegSel, 1, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.bxsData = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_bxsData_Items(self.bxsData)

        self.pnData.SetSizer(self.bxsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTSECPWDMAINPANEL,
              name=u'vtSecPwdPanel', parent=prnt, pos=wx.Point(325, 29),
              size=wx.Size(650, 400), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(642, 373))
        self.Bind(wx.EVT_SIZE, self.OnSize)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VTSECPWDMAINPANELSLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 0), size=wx.Size(200,
              376), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetAutoLayout(True)
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetLabel(_(u'navigation'))
        self.slwNav.SetToolTipString(_(u'navigation'))
        self.slwNav.SetDefaultSize(wx.Size(200, 100))
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VTSECPWDMAINPANELSLWNAV)

        self.pnData = wx.Panel(id=wxID_VTSECPWDMAINPANELPNDATA, name=u'pnData',
              parent=self, pos=wx.Point(208, 8), size=wx.Size(424, 352),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)

        self.viRegSel = vidarc.tool.xml.vtXmlNodeRegSelector.vtXmlNodeRegSelector(id=wxID_VTSECPWDMAINPANELVIREGSEL,
              name=u'viRegSel', parent=self.pnData, pos=wx.Point(4, 4),
              size=wx.Size(412, 340), style=wx.TAB_TRAVERSAL)
        self.viRegSel.Bind(vidarc.tool.xml.vtXmlNodeRegSelector.vEVT_TOOL_XML_REG_SELECTOR_APPLY,
              self.OnViRegSelToolXmlRegSelectorApply,
              id=wxID_VTSECPWDMAINPANELVIREGSEL)
        self.viRegSel.Bind(vidarc.tool.xml.vtXmlNodeRegSelector.vEVT_TOOL_XML_REG_SELECTOR_CANCEL,
              self.OnViRegSelToolXmlRegSelectorCancel,
              id=wxID_VTSECPWDMAINPANELVIREGSEL)
        self.viRegSel.Bind(vidarc.tool.xml.vtXmlNodeRegSelector.vEVT_TOOL_XML_REG_SELECTOR_SELECTED,
              self.OnViRegSelToolXmlRegSelectorSelected,
              id=wxID_VTSECPWDMAINPANELVIREGSEL)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name,fn,dn):
        global _
        _=vtLgBase.assignPluginLang('vtSecPwd')
        self._init_ctrls(parent)
        self.SetName(name)
        self.bActivated=False
        self.dCfg={}
        self.cfgFunc=None
        
        self.xdCfg=vtXmlDom(appl='vtSecPwdCfg',audit_trail=False)
        
        rect = parent.sbStatus.GetFieldRect(0)
        sFN=fn or 'vtSecPwd.priv'
        sDN=dn or vcCust.USR_LOCAL_DN
        self.netSecPwd=vtSecPwdXml(appl='vtSecPwd',synch=False,verbose=0,
                fn=sFN,dn=sDN,keySize=512)
        
        self.vgpTree=vtSecPwdTree(self.slwNav,wx.NewId(),
                pos=(0,0),size=(198,529),style=0,name="trSecPwd",
                master=True,verbose=0)
        self.vgpTree.BindEvent('selected',self.OnTreeItemSel)
        self.vgpTree.SetDoc(self.netSecPwd,False)
        
        # ++++++++++++++
        # add stuff here
        self.viRegSel.SetDoc(self.netSecPwd,False)
        #self.viRegSel.CreateRegisteredPanel('user',self.netSecPwd,bNet=False,
        #                    name='pnUsr')
        self.viRegSel.CreateRegisteredPanel('group',self.netSecPwd,bNet=False,
                            name='pnGrp')
        
        self.viRegNB = vidarc.tool.xml.vtXmlNodeRegListBook.vtXmlNodeRegListBook(applName='vtXmlNodeTagCmdPanel',
              id=wx.NewId(), name=u'viRegNB',
              parent=self.viRegSel, pos=wx.Point(0, 0), size=wx.Size(420, 248),
              style=0)
        
        self.viRegNB.SetDoc(self.netSecPwd,False)
        oUsr=self.netSecPwd.GetReg('user')
        self.viRegNB.SetRegNode(oUsr,bIncludeBase=True)
        self.viRegNB.SetDocPanels(self.netSecPwd,False)
        #self.viRegNB.Show(False)
        
        self.viRegSel.AddWidgetDependent(self.viRegNB,oUsr)
        #self.viRegSel.SetRegNode(oUsr)
        # add stuff here
        # --------------
        self.thdFindQuick=vtXmlFindQuickThread(self.vgpTree,bPost=True,
                origin='vtSecPwd:thdFindQuick')
        EVT_VTXML_FIND_QUICK_FOUND(self.vgpTree,self.OnFindQuickFound)
        self.idQuickFoundLast=-1
        
        self.Move(pos)
        self.SetSize(size)
    def GetDocMain(self):
        return self.netSecPwd
    def GetNetMaster(self):
        return None
    def GetTreeMain(self):
        return self.vgpTree
    def PopulateToolBar(self,parent):
        id=wx.NewId()
        sz=self.GetClientSize()
        self.tbMain = wx.ToolBar(id=id,
                name=u'tbSecPwd', parent=parent, pos=wx.Point(0,0),
                size=wx.DefaultSize, style=wx.TB_HORIZONTAL | wx.NO_BORDER)
        self.tbMain.SetToolBitmapSize(wx.Size(20,20))
        self.dToolBar={}
        self.vgpTree.PopulateToolBar(parent,self.tbMain,self.dToolBar)
        #self.tbMain.AddSimpleTool(id, vtArt.getBitmap(vtArt.Duplicate), _(u"Duplicate"), 
        #                _(u"duplicate"))
        #self.Bind(wx.EVT_TOOL, self.OnToolDupRecipeClick, id=id)
        self.tbMain.AddSeparator()
        id=wx.NewId()
        self.dToolBar['quickFind']=id
        txtCtrl=wx.TextCtrl(self.tbMain, id, "", size=(150, -1))
        self.tbMain.AddControl(txtCtrl)
        self.tbMain.Bind(wx.EVT_TEXT, self.OnQuickFind, id=id)
        self.tbMain.Bind(wx.EVT_TEXT_ENTER, self.OnQuickFindAgain, id=id)
        self.txtQuickFind=txtCtrl
        
        parent.SetToolBar(self.tbMain)
        self.tbMain.Realize()
        return self.tbMain
    def OnQuickFind(self,event):
        sVal=self.txtQuickFind.GetValue()
        self.idQuickFoundLast=-1
    def OnQuickFindAgain(self,event):
        sVal=self.txtQuickFind.GetValue()
        id=self.vgpTree.GetMoveTargetID()
        self.thdFindQuick.Do(id,self.idQuickFoundLast,sVal,100)
    def OnFindQuickFound(self,evt):
        evt.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.INFO,'id:%08d'%(evt.GetID()),self)
            self.idQuickFoundLast=evt.GetID()
            self.vgpTree.SelectByID('%08d'%self.idQuickFoundLast)
        except:
            vtLog.vtLngTB(self.GetName())
            vtLog.vtLngCurWX(vtLog.ERROR,'id:%08d'%(evt.GetID()),self)
    def OnTreeItemSel(self,event):
        event.Skip()
        node=event.GetTreeNodeData()
        self.viRegSel.SetNode(node)
        self.viRegNB.SetNode(node)
    def OpenFile(self,fn):
        if self.netSecPwd.Open(fn)<0:
            self.netSecPwd.New()
            self.netSecPwd.Save(fn)
        self.vgpTree.SetNode(None)
    def SaveFile(self,fn=None):
        self.netSecPwd.Save(fn)
    def OpenCfgFile(self,fn=None):
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                self.__setCfg__()
                return
        self.xdCfg.New(root='config')
        if fn is not None:
            self.xdCfg.SetFN(fn)
        self.__setCfg__()
    def SaveCfgFile(self,fn=None):
        self.GetCfgData()
        self.xdCfg.Save(fn)
    def __setCfg__(self):
        try:
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSlwNavSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iWidth=event.GetDragRect().width
            #if iWidth>80:
            #    iWidth=80
            self.dCfg['nav_sash']=str(iWidth)
            
            self.slwNav.SetDefaultSize((iWidth, 1000))
            
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            self.GetCfgData()
            #event.Skip()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSlwTopSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iHeight=event.GetDragRect().height
            #if iHeight>80:
            #    iHeight=80
            self.dCfg['nav_top']=str(iHeight)
            
            self.slwTop.SetDefaultSize((1000, iHeight))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            self.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnSize(self, event):
        event.Skip()
        try:
            bMod=False
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            
            self.pnData.Refresh()
            self.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())

    def SetCfgData(self,func,l,d):
        self.cfgFunc=func
        self.cfgOrigin=l
        self.dCfg=d
        try:
            i=int(self.dCfg['nav_sash'])
            self.slwNav.SetDefaultSize((i, 1000))
        except:
            pass
        try:
            i=int(self.dCfg['nav_top'])
            self.slwTop.SetDefaultSize((1000, i))
        except:
            pass
        try:
            pos=map(int,self.dCfg['pos'].split(','))
            self.Move(pos)
        except:
            pass
        try:
            sz=map(int,self.dCfg['size'].split(','))
            self.SetSize(sz)
        except:
            pass
    def GetCfgData(self):
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,self.dCfg)
    def OnViRegSelToolXmlRegSelectorApply(self, event):
        event.Skip()
        self.vgpTree.RefreshSelected()
    def OnViRegSelToolXmlRegSelectorCancel(self, event):
        event.Skip()
    def OnViRegSelToolXmlRegSelectorSelected(self, event):
        event.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            #lWid=event.GetListWidget()
            #for wid in lWid:
            #    wid.Layout()
            #self.bxsData.Layout()
            #wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            
            #self.pnData.Refresh()
            #self.bxsData.FitInside(self.pnData)
            #wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        except:
            vtLog.vtLngTB(self.GetName())
    def GetAboutData(self):
        import __config__
        version=u' %d.%d.%d.%d'%(__config__.VER_MAJOR,__config__.VER_MINOR,
                    __config__.VER_RELEASE,__config__.VER_SUBREL)
        desc=_(u"""security password module.

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
""")
        return _(u'VIDARC Security Password'),desc,version
