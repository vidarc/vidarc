#----------------------------------------------------------------------------
# Name:         setup__inno.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080316
# CVS-ID:       $Id: setup_inno.py,v 1.1 2008/03/16 22:14:24 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

# A setup script showing how to extend py2exe.
#
# In this case, the py2exe command is subclassed to create an installation
# script for InnoSetup, which can be compiled with the InnoSetup compiler
# to a single file windows installer.
#
# By default, the installer will be created as dist\Output\setup.exe.

from distutils.core import setup
from distutils.dist import Distribution
import py2exe
import sys,os,getopt,traceback
import glob
#from EasySWAXS.appversion import APPNAME, VERSION, STRVERSION, CONFFILE
#myCfgFiles=['wbpFileTools.ini','wbpFileTools_test.ini']


# A program using wxPython

# The manifest will be inserted as resource into test_wx.exe.  This
# gives the controls the Windows XP appearance (if run on XP ;-)
#
# Another option would be to store if in a file named
# test_wx.exe.manifest, and probably copy it with the data_files
# option.
#
manifest_template = '''
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<assembly xmlns="urn:schemas-microsoft-com:asm.v1" manifestVersion="1.0">
<assemblyIdentity
    version="5.0.0.0"
    processorArchitecture="x86"
    name="%(prog)s"
    type="win32"
/>
<description>%(prog)s Program</description>
<dependency>
    <dependentAssembly>
        <assemblyIdentity
            type="win32"
            name="Microsoft.Windows.Common-Controls"
            version="6.0.0.0"
            processorArchitecture="X86"
            publicKeyToken="6595b64144ccf1df"
            language="*"
        />
    </dependentAssembly>
</dependency>
</assembly>
'''

RT_MANIFEST = 24
VIDARC_IMPORT=1

Distribution.global_options.append(('trusted','t','trusted build'))
for o in sys.argv[1:]:
    try:
        if o in ['--internal','--trusted']:
            print 'set by cmdline'
            VIDARC_IMPORT=0
    except:
        pass
################################################################
# arguments for the setup() call
sDN=os.getcwd()
if os.path.exists("build_options.py"):
    os.remove("build_options.py")
sys.setup_is_main =  __name__ == "__main__"  # an icky hack!
from vidarc.config import config
config.build_options(os.getcwd(),VIDARC_IMPORT=VIDARC_IMPORT)
#from vidarc.install.config import *
#build_options(VIDARC_IMPORT=VIDARC_IMPORT)

if VIDARC_IMPORT:
    includes=['vtSecPwdMainFrame','vidImp',
        'atexit',
        'binhex','bisect',
        'calendar','chunk','cmd',
        'code','codecs','codeop','colorsys','commands','compileall',
        'ConfigParser','copy','copy_reg',
        'decimal','difflib','dircache','dis',
        'filecmp','fileinput','formatter','fpformat','fnmatch',
        'getopt','getpass','gettext','glob','gzip','heapq',
        'ihooks','imaplib','imghdr','imputil','inspect',
        'keyword','linecache','locale',
        'logging','logging.*',
        'modulefinder','mutex',
        'netrc','new','ntpath','nturl2path',
        'numpy','numpy.core','numpy.lib','numpy.random','numpy.linalg',
        'numpy.fft','numpy.testing','numpy.f2py',
        'opcode','optparse','os',
        'pdb','pickle','pickletools','pipes','pkgutil','platform',
        'popen2','poplib','posixfile','posixpath','pprint','profile',
        'pstats','pty','py_compile','pyclbr','pydoc','pyodbc','pylab',
        'Queue','quopri',
        'random','re','reconvert','regex_syntax','regsub','repr',
        'rexec','rfc822','rlcompleter','robotparser',
        'sched','sets','sgmllib','shelve','shlex','shutil',
        'smtpd','smtplib',
        'sndhdr','socket','sre','sre_compile','sre_constants',
        'sre_parse','stat','statcache','statvfs','string','StringIO','cStringIO',
        'stringprep','subprocess','symbol','symtable',
        'tabnanny','tarfile','tempfile','textwrap','this',
        'thread','threading','timeit','token','tokenize','trace',
        'traceback','tty','types','tzparse',
        'urllib','urllib2','urlparse','user','UserDict','UserList','UserString',
        'uu',
        'warnings','wave','weakref','webbrowser','whichdb','whrandom',
        'xdrlib','xmllib','xmlrpclib','zipfile',
        'xml',
        
        #'adodb',
        'libxmlmods.*',
        #'numarray','Numeric',
        'PIL',
        'libxml2',
        'cPickle',
        'email',
        
        'win32com.*',#'win32comext.*',
        'win32file.*',
        'win32con.*',
        
        'OpenSSL',
        'Crypto',
        'Crypto.Cipher.*',
        'Crypto.Hash.*',
        'Crypto.Protocol.*',
        'Crypto.PublicKey.*',
        'Crypto.Util.*',
        'wx.calendar',
        'wx.lib',
        'wx.lib.anchors',
        'wx.lib.buttons',
        'wx.lib.colourdb',
        'wx.lib.filebrowsebutton',
        'wx.lib.intctrl',
        'wx.lib.throbber',
        'wx.stc',
        'wx.gizmos',
        'wx.grid',
        'wx.html',
        'wx.lib.masked',
        'wx.lib.scrolledpanel',
        'wx.lib.popupctl',
        'wxPython.tools.img2py',
        'wxPython',
        'wxPython.lib',
        'wxPython.lib.maskededit',
        'wxPython.lib.popupctl',
        'wxPython.lib.buttons',
        'wxPython.calendar',
        ]
    excludes=["vidarc","vidarc.tool","vidarc.ext"]
    
    from platform import python_version_tuple
    if python_version_tuple() >= ['2','5']:
        includes.extend(['xml.etree.*'])
    else:
        includes.extend(['elementtree.*','cElementTree'])
else:
    includes="vtSecPwdMainFrame,vidImp"
    #excludes=["vidarc","vidarc.tool","vidarc.ext"]
    excludes=["vidarc","vidarc.tool","vidarc.ext"]
    excludes=[]

INST_PATH="vidarc/sec"
GROUP_BASE="VIDARC"

es_app = dict(
    script = "vtSecPwdAppl.py",
    icon_resources = [(1, "img/Safe01_16.ico")],
    #icon = "vidarcPrjTimer_16.ico",
    other_resources = [(RT_MANIFEST, 1, manifest_template % dict(prog='vtSecPwd'))],
    #)
    dest_base = r"vtSecPwd")

#zipfile = r"shardlib.zip"
zipfile = None

options = {"py2exe": {"compressed": 1,
                      "includes" : includes,
                      "excludes" : excludes,    
                      "packages": ["encodings"],
                      "optimize": 2,
                      #"bundle_files":2,
                      "dll_excludes": ["MSVCR71.dll","OLEAUT32.dll",'USER32.dll','shell32.dll',
                            'ole32.dll','winmm.dll','wsock32.dll','comctl32.dll','advapi32.dll',
                            'msvcrt.dll','ws2_32.dll','winspool.drv','gdi32.dll','version.dll',
                            'kernel32.dll','comdlg32.dll','rpcrt4.dll'],
                      }}

################################################################
import os

class InnoScript:
    def __init__(self,
                 name,
                 lib_dir,
                 dist_dir,
                 windows_exe_files = [],
                 lib_files = [],
                 conf_files = [],
                 version = '0.0',
                 languages=[]):
        self.lib_dir = lib_dir
        self.dist_dir = dist_dir
        if not self.dist_dir[-1] in "\\/":
            self.dist_dir += "\\"
        self.name = name
        self.conf_files = conf_files
        self.version = version
        self.languages=languages
        self.windows_exe_files = [self.chop(p) for p in windows_exe_files]
        self.lib_files = [self.chop(p) for p in lib_files]

    def chop(self, pathname):
        assert pathname.startswith(self.dist_dir)
        return pathname[len(self.dist_dir):]
    
    def create(self, pathname=None):
        if pathname is None:
            pathname = "dist\\%s.iss" % self.name
        self.pathname = pathname
        ofi = self.file = open(pathname, "w")
        print >> ofi, "; WARNING: This script has been created by py2exe. Changes to this script"
        print >> ofi, "; will be overwritten the next time py2exe is run!"
        #print >> ofi, """
#[Languages]
#Name: English; MessagesFile: "compiler:Default.isl"
#Name: Deutsch; MessagesFile: "compiler:Languages\German.isl"
#        """
        print >> ofi, r"[Setup]"
        print >> ofi, r"AppName=%s" % self.name
        print >> ofi, r"AppVerName=%s %s" % (self.name, self.version)
        print >> ofi, r"DefaultDirName={pf}\%s\%s" % (INST_PATH,self.name)
        print >> ofi, r"OutputBaseFileName=%s-%s-%s" % (self.name, self.version,'setup')
        print >> ofi, r"DefaultGroupName=%s\%s" % (GROUP_BASE,self.name)
        #print >> ofi, r"Compression=lzma/max"
        print >> ofi, r"Compression=bzip"
        print >> ofi

        print >> ofi, r"[Files]"
        for path in self.windows_exe_files + self.lib_files:
            #print >> ofi, r'Source: "%s"; DestDir: "{app}\%s"; Flags: ignoreversion' % (path, os.path.dirname(path))
            print >> ofi, r'Source: "%s"; DestDir: "{app}\%s"; ' % (path, os.path.dirname(path))
        print >> ofi
        if self.conf_files:
            print >> ofi, r"[Registry]"
            for cff in self.conf_files:
                print >> ofi, r'Root: HKCU; Subkey: "Software\%s"; Flags: dontcreatekey uninsdeletekey' % cff
            print >> ofi
        else:
            print >> ofi, r"[Registry]"
            print >> ofi, r'Root: HKCR; Subkey: ".vidarc"; ValueType: string; ValueName: ""; ValueData: "VIDARC_PlugIn"; Flags: uninsdeletevalue'
            print >> ofi, r'Root: HKCR; Subkey: "VIDARC_PlugIn"; ValueType: string; ValueName: ""; ValueData: "VIDARC PlugIn"; Flags: uninsdeletekey'
            print >> ofi, r'Root: HKCR; Subkey: "VIDARC_PlugIn\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\%s.exe,0"'% self.name
            print >> ofi
            
        print >> ofi, r"[Icons]"
        for path in self.windows_exe_files:
            #print >> ofi, r'Name: "\%s"; Filename: "{app}\%s"' % \
            #      (self.name, path)
            print >> ofi, r'Name: "{group}\%s"; Filename: "{app}\%s"' % \
                  (self.name, path)
            for lang in self.languages:
                print >> ofi, r'Name: "{group}\%s %s"; Filename: "{app}\%s" ; Parameters: "--lang=%s"' % \
                      (self.name,lang, path,lang)
        #print >> ofi, 'Name: "{group}\Uninstall %s"; Filename: "{uninstallexe}"' % self.name

    def compile(self):
        try:
            import ctypes
        except ImportError:
            try:
                import win32api
            except ImportError:
                import os
                os.startfile(self.pathname)
            else:
                print "Ok, using win32api."
                win32api.ShellExecute(0, "compile",
                                                self.pathname,
                                                None,
                                                None,
                                                0)
        else:
            print "Cool, you have ctypes installed."
            res = ctypes.windll.shell32.ShellExecuteA(0, "compile",
                                                      self.pathname,
                                                      None,
                                                      None,
                                                      0)
            if res < 32:
                raise RuntimeError, "ShellExecute failed, error %d" % res


################################################################

from py2exe.build_exe import py2exe
from __config__ import *

class build_installer(py2exe):
    # This class first builds the exe file(s), then creates a Windows installer.
    # You need InnoSetup for it.
    def run(self):
        # First, let py2exe do it's work.
        py2exe.run(self)

        lib_dir = self.lib_dir
        dist_dir = self.dist_dir
        # create the Installer, using the files py2exe has created.
        script = InnoScript('vtSecPwd',
                            lib_dir,
                            dist_dir,
                            self.windows_exe_files,
                            self.lib_files,
                            #conf_files=myCfgFiles,
                            version='%d.%d.%d'%(VER_MAJOR,VER_MINOR,VER_RELEASE),
                            languages=['en','de']
                            )
        print "*** creating the inno setup script***"
        script.create()
        print "*** compiling the inno setup script***"
        script.compile()
        # Note: By default the final setup.exe will be in an Output subdirectory.

################################################################

setup(
    options = options,
    # The lib directory contains everything except the executables and the python dll.
    zipfile = zipfile,
    windows = [es_app],
    #file_type = [('vidarc', "img/Vid_MES02_16.ico")],
    data_files=[
            ('locale/en/LC_MESSAGES', ['./locale/en/LC_MESSAGES/vtSecPwd.mo']), 
            ('locale/de/LC_MESSAGES', ['./locale/de/LC_MESSAGES/vtSecPwd.mo']), 
            ('locale/fr/LC_MESSAGES', ['./locale/fr/LC_MESSAGES/vtSecPwd.mo']), 
            ],
    #data_files=[('', ['floderclsd.png', 'changelog.txt']), 
    #    ('folder1/sub01', glob.glob('folder1/sub01/*.*')),
    #    ('folder2/sub02/sub031', glob.glob('folder2/sub02/sub031/*'))
    #    ],
    # use out build_installer class as extended py2exe build command
    cmdclass = {"py2exe": build_installer},
    )

if os.path.exists("build_options.py"):
    os.remove("build_options.py")

# vim:sts=4:ts=8:sw=4:et
