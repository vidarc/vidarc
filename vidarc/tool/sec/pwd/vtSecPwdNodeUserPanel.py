#Boa:FramePanel:vtSecPwdNodeUserPanel
#----------------------------------------------------------------------------
# Name:         vtSecPwdNodeUserPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20080305
# CVS-ID:       $Id: vtSecPwdNodeUserPanel.py,v 1.2 2008/03/08 00:46:34 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputText

import sys

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_VTSECPWDNODEUSERPANEL, wxID_VTSECPWDNODEUSERPANELLBLFIRSTNAME, 
 wxID_VTSECPWDNODEUSERPANELLBLLOGIN, wxID_VTSECPWDNODEUSERPANELLBLSURNAME, 
 wxID_VTSECPWDNODEUSERPANELLBLTYPE, wxID_VTSECPWDNODEUSERPANELTXTID, 
 wxID_VTSECPWDNODEUSERPANELTXTTYPE, wxID_VTSECPWDNODEUSERPANELVIFIRSTNAME, 
 wxID_VTSECPWDNODEUSERPANELVILOGIN, wxID_VTSECPWDNODEUSERPANELVISURNAME, 
] = [wx.NewId() for _init_ctrls in range(10)]

class vtSecPwdNodeUserPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblLogin, (0, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viLogin, (0, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.lblSurName, (1, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viSurName, (1, 1), border=0, flag=wx.EXPAND,
              span=(1, 3))
        parent.AddWindow(self.lblFirstName, (2, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viFirstName, (2, 1), border=0, flag=wx.EXPAND,
              span=(1, 3))
        parent.AddWindow(self.lblType, (3, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.txtType, (3, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.txtID, (3, 2), border=0, flag=wx.EXPAND, span=(1,
              2))

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=4, vgap=4)

        self._init_coll_gbsData_Items(self.gbsData)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=id, name=u'vtSecPwdNodeUserPanel',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(312, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 180))
        self.SetAutoLayout(True)

        self.lblLogin = wx.StaticText(id=wxID_VTSECPWDNODEUSERPANELLBLLOGIN,
              label=_(u'login'), name=u'lblLogin', parent=self, pos=wx.Point(0,
              0), size=wx.Size(56, 21), style=wx.ALIGN_RIGHT)

        self.viLogin = vidarc.tool.input.vtInputText.vtInputText(id=wxID_VTSECPWDNODEUSERPANELVILOGIN,
              name=u'viLogin', parent=self, pos=wx.Point(60, 0),
              size=wx.Size(100, 21), style=0)

        self.lblSurName = wx.StaticText(id=wxID_VTSECPWDNODEUSERPANELLBLSURNAME,
              label=_(u'surname'), name=u'lblSurName', parent=self,
              pos=wx.Point(0, 25), size=wx.Size(56, 21), style=wx.ALIGN_RIGHT)

        self.viSurName = vidarc.tool.input.vtInputText.vtInputText(id=wxID_VTSECPWDNODEUSERPANELVISURNAME,
              name=u'viSurName', parent=self, pos=wx.Point(60, 25),
              size=wx.Size(208, 21), style=0)

        self.lblFirstName = wx.StaticText(id=wxID_VTSECPWDNODEUSERPANELLBLFIRSTNAME,
              label=_(u'firstname'), name=u'lblFirstName', parent=self,
              pos=wx.Point(0, 50), size=wx.Size(56, 21), style=wx.ALIGN_RIGHT)

        self.viFirstName = vidarc.tool.input.vtInputText.vtInputText(id=wxID_VTSECPWDNODEUSERPANELVIFIRSTNAME,
              name=u'viFirstName', parent=self, pos=wx.Point(60, 50),
              size=wx.Size(208, 21), style=0)

        self.lblType = wx.StaticText(id=wxID_VTSECPWDNODEUSERPANELLBLTYPE,
              label=_(u'type'), name=u'lblType', parent=self, pos=wx.Point(0,
              75), size=wx.Size(56, 21), style=wx.ALIGN_RIGHT)

        self.txtType = wx.TextCtrl(id=wxID_VTSECPWDNODEUSERPANELTXTTYPE,
              name=u'txtType', parent=self, pos=wx.Point(60, 75),
              size=wx.Size(100, 21), style=0, value=u'')
        self.txtType.Enable(False)
        self.txtType.SetMinSize(wx.Size(-1, -1))

        self.txtID = wx.TextCtrl(id=wxID_VTSECPWDNODEUSERPANELTXTID,
              name=u'txtID', parent=self, pos=wx.Point(164, 75),
              size=wx.Size(104, 21), style=0, value=u'00000000')
        self.txtID.Enable(False)
        self.txtID.SetMinSize(wx.Size(-1, -1))

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vtSecPwd')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        
        self.viLogin.SetTagName('login')
        self.viSurName.SetTagName('surname')
        self.viFirstName.SetTagName('firstname')
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
        
        self.gbsData.AddGrowableCol(1,1)
        self.gbsData.AddGrowableCol(3,1)
        self.gbsData.AddGrowableCol(4,1)
        self.gbsData.Layout()
        self.gbsData.Fit(self)
        
    def Clear(self):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.Clear(self)
        
        # add code here
        self.viLogin.Clear()
        self.viSurName.Clear()
        self.viFirstName.Clear()
        self.txtType.SetValue('')
        self.txtID.SetValue(u'')
        
        self.ClrBlockDelayed()
    def SetNetDocs(self,d):
        #if d.has_key('vHum'):
        #    dd=d['vHum']
        # add code here
        pass
    def SetDoc(self,doc,bNet=False):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        
        # add code here
        self.viLogin.SetDoc(doc)
        self.viSurName.SetDoc(doc)
        self.viFirstName.SetDoc(doc)
        
    def __SetNode__(self,node):
        try:
            # add code here
            self.viLogin.SetNode(node)
            self.viSurName.SetNode(node)
            self.viFirstName.SetNode(node)
            sTag=self.doc.getTagName(node)
            if len(sTag)>0:
                o=self.doc.GetRegisteredNode(sTag)
                if o is not None:
                    self.txtType.SetValue(o.GetDescription())
                else:
                    self.txtType.SetValue(' '.join([sTag,'?']))
            else:
                self.txtType.SetValue('')
            sID=u''
            
            if node is not None:
                iId=long(self.doc.getKey(node))
                if iId>=0:
                    try:
                        sID=self.doc.ConvertIdNum2Str(iId)
                    except:
                        pass
                if hasattr(self.doc,'getKeyInst'):
                    try:
                        iIID=self.doc.getKeyInst(node)
                        if iIID>0:
                            sIID=self.doc.ConvertIdNum2Str(iIID)
                            sID=''.join([sID,' (',sIID,')'])
                    except:
                        pass
            self.txtID.SetValue(sID)
        except:
            vtLog.vtLngTB(self.GetName())
    def SetNode(self,node):
        try:
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            # add code here
            self.__SetNode__(node)
        except:
            vtLog.vtLngTB(self.GetName())
    def __GetNode__(self,node):
        try:
            # add code here
            self.viLogin.GetNode(node)
            self.viSurName.GetNode(node)
            self.viFirstName.GetNode(node)
        except:
            vtLog.vtLngTB(self.GetName())
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
                print node
            if node is None:
                return
            # add code here
            self.__GetNode__(node)
            self.doc.AlignNode(node)
            self.GetNodeFin(node)
        except:
            vtLog.vtLngTB(self.GetName())
    def Close(self):
        vtXmlNodePanel.Close(self)
        # add code here
        self.viLogin.Close()
        self.viSurName.Close()
        self.viFirstName.Close()
    def Cancel(self):
        vtXmlNodePanel.Cancel(self)
        # add code here
        self.viLogin.Clear()
        self.viSurName.Clear()
        self.viFirstName.Clear()
    def Lock(self,flag):
        try:
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                print flag
            if flag:
                # add code here
                self.viLogin.Enable(False)
                self.viSurName.Enable(False)
                self.viFirstName.Enable(False)
            else:
                # add code here
                self.viLogin.Enable(True)
                self.viSurName.Enable(True)
                self.viFirstName.Enable(True)
        except:
            vtLog.vtLngTB(self.GetName())
