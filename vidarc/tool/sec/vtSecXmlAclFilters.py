#----------------------------------------------------------------------------
# Name:         vtSecXmlAclFilters.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060302
# CVS-ID:       $Id: vtSecXmlAclFilters.py,v 1.7 2008/02/04 15:38:16 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlFilterType import *

class vtSecXmlAclFilters:
    def __init__(self,lFlt,doc,tmp,verbose=0):
        self.nr=0
        self.lst=[]
        self.verbose=verbose
        self.fltMapper=vtXmlFilterMapper()
        self.__build__(lFlt,doc,tmp)
    def __del__(self):
        del self.lst
        del self.fltMapper
    def AppendFilter(self,flt):
        self.lst.append(flt)
        if self.verbose>0:
            print self.lst
    def UpdateFilter(self,idx,tup):
        self.lst=self.lst[:idx]+[tup]+self.lst[idx+1:]
    def MoveFilter(self,idx,iDiff):
        if iDiff<0:
            self.lst=self.lst[:idx-1]+[self.lst[idx],self.lst[idx-1]]+self.lst[idx+1:]
        elif iDiff>0:
            if self.verbose>0:
                print self.lst
            self.lst=self.lst[:idx]+[self.lst[idx+1],self.lst[idx]]+self.lst[idx+2:]
            if self.verbose>0:
                print self.lst
    def GetFilters(self):
        return self.lst
    def GetFilter(self,idx):
        try:
            return self.lst[idx]
        except:
            return None
    def DelFilter(self,idx):
        if self.verbose>0:
            vtLog.CallStack('')
            print idx
        try:
            self.lst=self.lst[:idx]+self.lst[idx+1:]
        except:
            pass
        if self.verbose>0:
            print self.lst
    def HasFilters(self):
        return len(self.lst)>0
    def __build__(self,lFlt,doc,tmp):
        #vtLog.CallStack('')
        del self.lst
        self.lst=[]
        if lFlt is not None:
            for c in doc.getChilds(tmp,'filter'):
                flt=self.fltMapper.SetNode(lFlt,doc,c)
                if flt is None:
                    continue
                self.lst.append(flt)
        if self.verbose>0:
            vtLog.CallStack('compares:%d'%len(self.lst))
            for it in self.lst:
                print '   ',it
    def GetNode(self,doc,tmp):
        for it in self.lst:
            c=doc.createSubNode(tmp,'filter')
            doc.setNodeText(c,'tag',it[0])
            it[1].GetNode(doc,c)
    def IsFilterOk(self,doc,node):
        #vtLog.CallStack('')
        for sName,flt in self.lst:
            #print node,sName,flt
            if doc.match(node,sName,flt)==False:
                #print 'false'
                return False
        return True
    def __str__(self):
        return 'cmps:%d'%(len(self.lst))

