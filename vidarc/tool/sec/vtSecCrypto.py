#----------------------------------------------------------------------------
# Name:         vtSecCrypto.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060701
# CVS-ID:       $Id: vtSecCrypto.py,v 1.6 2007/08/07 16:59:20 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os.path,os
#import ezPyCrypto
import vidCrypto

import vidarc.tool.log.vtLog as vtLog
import threading

semCrypto=threading.Lock()#threading.Semaphore()
def acquireCrypto():
    global semCrypto
    semCrypto.acquire()
def releaseCrypto():
    global semCrypto
    semCrypto.release()

class vtSecCrypto:
    PREFIX='<StartPycryptoMessage>\n'
    SUFFIX='<EndPycryptoMessage>\n'
    def __init__(self,phrase,fn=None,dn='~',keySize=2048,algoSes=None):
        if algoSes is None:
            self.algoSes='AES'
        else:
            self.algoSes=algoSes
        self.sDN=None
        self.sFullFN=None
        self.key=None
        #self.key=ezPyCrypto.key(keysize=keySize,passphrase=phrase,algoSess=algoSes)
        self.bValid=False
        self.SetDN(dn)
        if fn is None:
            self.sFullFN=None
        else:
            self.sFullFN=os.path.join(self.sDN,fn)
        self.phrase=phrase
        if self.sFullFN is not None:
            iRet=self.ImportPrivKey()
            if iRet==-2:
                acquireCrypto()
                try:
                    self.key=vidCrypto.VidCryptoKey(keySize,algCipher=self.algoSes,phrase=phrase)
                except:
                    vtLog.vtLngTB(self.__class__.__name__)
                releaseCrypto()
                #self.key=ezPyCrypto.key(keySize,algoSess=self.algoSes,passphrase=phrase)
                self.ExportPrivKey()
                self.bValid=True
            elif iRet==0:
                self.bValid=True
        else:
            acquireCrypto()
            try:
                self.key=vidCrypto.VidCryptoKey(keySize,algCipher=self.algoSes,phrase=phrase)
            except:
                vtLog.vtLngTB(self.__class__.__name__)
            releaseCrypto()
            #self.key=ezPyCrypto.key(keysize=keySize,passphrase=phrase,algoSess=algoSes)
        #print self.key.algoPub,self.key.algoPname,self.key.algoSes,self.key.algoSname
    def CreateKey(self,phrase,keySize=2048):
        try:
            acquireCrypto()
            self.key=vidCrypto.VidCryptoKey(keySize,algCipher=self.algoSes,phrase=phrase)
            #self.key=ezPyCrypto.key(keySize,passphrase=phrase)
            releaseCrypto()
            #print self.key.algoPub,self.key.algoPname,self.key.algoSes,self.key.algoSname
            return True
        except:
            releaseCrypto()
            vtLog.vtLngTB(self.__class__.__name__)
        return False
    def IsValid(self):
        return self.bValid
    def SetDN(self,dn):
        if dn is not None:
            self.sDN=os.path.expanduser(dn)
    def SetFN(self,fn):
        if fn is not None:
            try:
                self.sFullFN=os.path.join(self.sDN,fn)
            except:
                self.sFullFN=None
    def ImportKeyStr(self,sKey,phrase=None):
        try:
            acquireCrypto()
            self.key=vidCrypto.VidCryptoKey(sKey,phrase=phrase,algCipher=self.algoSes)
            #self.key=ezPyCrypto.key(sKey)
            #self.key.algoSes=self.key._algosSes.get(self.algoSes, None)
            #self.key.algoSname=self.algoSes
            releaseCrypto()
            #self.key.importKey(sKey)
            #print self.key.algoPub,self.key.algoPname,self.key.algoSes,self.key.algoSname
            self.bValid=True
            return 0
        except:
            releaseCrypto()
            vtLog.vtLngTB(self.__class__.__name__)
            return -3
    def ImportKey(self,fn=None,dn=None,phrase=None):
        if dn is not None:
            self.SetDN(dn)
        if fn is not None:
            self.SetFN(fn)
        if self.sFullFN is None:
            vtLog.vtLngCurCls(vtLog.ERROR,'sFullFN is None',self)
            return -1
        #self.key=None
        self.bValid=False
        try:
            if os.path.exists(self.sFullFN):
                f=open(self.sFullFN,'rs')
                sKey=f.read()
                f.close()
                return self.ImportKeyStr(sKey,phrase)
            else:
                vtLog.vtLngCurCls(vtLog.WARN,'sFullFN:%s does not exit'%(self.sFullFN),self)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return -2
    def ImportPrivKeyStr(self,sKey,phrase=None):
        try:
            acquireCrypto()
            self.key=vidCrypto.VidCryptoKey(sKey,phrase=phrase,algCipher=self.algoSes)
            #self.key=ezPyCrypto.key(sKey,passphrase=phrase)
            #print self.key.algoPub,self.key.algoPname,self.key.algoSes,self.key.algoSname
            #self.key.algoSes=self.key._algosSes.get(self.algoSes, None)
            #self.key.algoSname=self.algoSes
            #print self.key.algoPub,self.key.algoPname,self.key.algoSes,self.key.algoSname
            #self.key.randpool = ezPyCrypto.RandomPool()
            #self.key.randfunc = self.key.randpool.get_bytes
            #self.key.importKey(sKey,passphrase=phrase)
            releaseCrypto()
            self.bValid=True
            return 0
        except:
            releaseCrypto()
            vtLog.vtLngTB(self.__class__.__name__)
            return -3
    def ImportPrivKey(self,fn=None,dn=None,phrase=None):
        if dn is not None:
            self.SetDN(dn)
        if fn is not None:
            self.SetFN(fn)
        if self.sFullFN is None:
            vtLog.vtLngCurCls(vtLog.ERROR,'sFullFN is None',self)
            return -1
        self.bValid=False
        #self.key=None
        try:
            if os.path.exists(self.sFullFN):
                if phrase is None:
                    phrase=self.phrase
                if phrase is None:
                    phrase=''
                f=open(self.sFullFN,'r')
                sKey=f.read()
                f.close()
                return self.ImportPrivKeyStr(sKey,phrase)
            else:
                vtLog.vtLngCurCls(vtLog.WARN,'sFullFN:%s does not exit'%(self.sFullFN),self)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return -2
    def ExportKey(self,fn=None,dn=None,phrase=None):
        if dn is not None:
            self.SetDN(dn)
        if fn is not None:
            self.SetFN(fn)
        if self.sFullFN is None:
            return False
        try:
            sDN,sFN=os.path.split(self.sFullFN)
            try:
                os.makedirs(sDN)
            except:
                pass
            f=open(self.sFullFN,'w')
            f.write(self.key.exportKey())
            f.close()
            return True
        except:
            vtLog.vtLngTB(self.__class__.__name__)
            return False
    def ExportPrivKey(self,fn=None,dn=None,phrase=None):
        if dn is not None:
            self.SetDN(dn)
        if fn is not None:
            self.SetFN(fn)
        if self.sFullFN is None:
            return False
        try:
            acquireCrypto()
            sDN,sFN=os.path.split(self.sFullFN)
            try:
                os.makedirs(sDN)
            except:
                pass#return False
            if phrase is None:
                phrase=self.phrase
            if phrase is None:
                phrase=''
            f=open(self.sFullFN,'w')
            f.write(self.key.exportKeyPrivate(phrase=phrase))
            #f.write(self.key.exportKeyPrivate(passphrase=phrase))
            f.close()
            releaseCrypto()
            return True
        except:
            releaseCrypto()
            vtLog.vtLngTB(self.__class__.__name__)
            return False    
