#----------------------------------------------------------------------------
# Name:         vtSecCryptoXmlDomRegMachine.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080305
# CVS-ID:       $Id: vtSecCryptoXmlDomRegMachine.py,v 1.1 2008/03/05 14:18:01 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.sec.vtSecCryptoXmlDomReg import vtSecCryptoXmlDomReg

import identify as vIdentify
gMID=vIdentify.getMachineID()

class vtSecCryptoXmlDomRegMachine(vtSecCryptoXmlDomReg):
    def __init__(self,attr='id',skip=[],synch=False,appl='',verbose=0,
                audit_trail=True,
                fn=None,dn='~',keySize=2048):
        global gMID
        vtSecCryptoXmlDomReg.__init__(self,attr=attr,skip=skip,synch=synch,appl=appl,
                verbose=verbose,audit_trail=False,
                phrase=gMID,fn=fn,dn=dn,keySize=keySize)
    
