#----------------------------------------------------------------------------
# Name:         vtSecAclXmlNodeFilter.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060223
# CVS-ID:       $Id: vtSecXmlAclNodeFilter.py,v 1.2 2007/01/30 15:45:51 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

class vtSecAclXmlNodeFilter:
    def __init__(self,doc,tmp,verbose=0):
        self.nr=0
        self.lst=[]
        self.verbose=verbose
        self.__build__(doc,tmp)
    def __del__(self):
        del self.lst
    def __build__(self,doc,tmp):
        try:
            self.nr=int(doc.getNodeText(tmp,'nr'))
        except:
            self.nr=0
        for c in doc.getChilds(tmp,'cmp'):
            self.lst.append((doc.getNodeText(c,'value'),
                            doc.getNodeText(c,'filter')))
        if self.verbose>0:
            vtLog.CallStack('compares:%d'%len(self.lst))
            for it in self.lst:
                print '   ',it
    def checkFilter(self,doc,node,sVal,sFilter,iType):
        tmp,s=doc.getNodeInfoVal(node,sVal)
        #print tmp,s,sFilter
        if iType==doc.FILTER_TYPE_STRING:
            if fnmatch.fnmatch(s,sFilter):
                return True
        return False
    def IsFilterOk(self,doc,node):
        #vtLog.CallStack('')
        dPoss=doc.GetPossibleFilter()
        tag=doc.getTagName(node)
        try:
            def getFilterType(sVal):
                lstNode=dPoss[tag]
                for sName,iType in lstNode:
                    if sName==sVal:
                        return iType
                return doc.FILTER_TYPE_STRING
            for sVal,sFilter in self.lst:
                #print sVal,sFilter
                iType=getFilterType(sVal)
                if self.checkFilter(doc,node,sVal,sFilter,iType)==False:
                    return False
        except:
            traceback.print_exc()
            return True
        return True
    def __str__(self):
        return 'nr:%3d cmps:%d'%(self.nr,len(self.lst))
