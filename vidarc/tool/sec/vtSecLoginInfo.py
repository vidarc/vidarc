#----------------------------------------------------------------------------
# Name:         vtSecLogin.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060223
# CVS-ID:       $Id: vtSecLoginInfo.py,v 1.5 2007/08/21 18:09:20 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

class vtSecLoginInfo:
    def __init__(self,usr='',usrId='-1',grpIds=[],grps=[],iSecLv=0,bAdmin=False):
        self.usr=usr
        try:
            self.usrId=long(usrId)
        except:
            self.usrId=-1
        try:
            self.grpIds=map(long,grpIds)
        except:
            self.grpIds=[]
        self.grps=grps
        try:
            self.iSecLv=int(iSecLv)
        except:
            self.iSecLv=0
        try:
            self.bAdmin=int(bAdmin)
        except:
            self.bAdmin=False
    def __del__(self):
        del self.grpIds
    def Clear(self,bMaster=False):
        self.usr=''
        self.usrId=-1
        self.grpIds=[]
        self.grps=[]
        if bMaster:
            self.iSecLv=32
            self.bAdmin=True
        else:
            self.iSecLv=0
            self.bAdmin=False
    def GetUsr(self):
        return self.usr
    def GetUsrId(self):
        return self.usrId
    def GetGrpIds(self):
        return self.grpIds
    def GetGrps(self):
        return self.grps
    def GetSecLv(self):
        return self.iSecLv
    def SetUsr(self,usr):
        self.Clear()
        self.usr=usr
    def IsAdmin(self):
        return self.bAdmin
    def GetStr(self):
        return '%s,%d,%s,%s,%d,%d'%(self.usr,self.usrId,';'.join(map(str,self.grpIds)),';'.join(self.grps),self.iSecLv,self.bAdmin)
    def SetStr(self,s,bMaster=False):
        strs=s.join(',')
        self.Clear(bMaster)
        try:
            self.usr=strs[0]
            try:
                self.usrId=long(strs[1])
            except:
                pass
            try:
                self.grpIds=map(long,strs[2].split(';'))
            except:
                pass
            self.grps=strs[3].split(';')
            try:
                self.iSecLv=int(strs[4])
            except:
                pass
            try:
                self.bAdmin=int(strs[5])
            except:
                pass
        except:
            pass
    def Set(self,o):
        self.usr=o.usr
        self.usrId=o.usrId
        self.grpIds=o.grpIds
        self.grps=o.grps
        self.iSecLv=o.iSecLv
        self.bAdmin=o.bAdmin
    def __str__(self):
        return 'usr:%08d grp:%s secLv:%3d'%(self.usrId,self.grpIds,self.iSecLv)
