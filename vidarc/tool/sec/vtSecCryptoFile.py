#----------------------------------------------------------------------------
# Name:         vtSecCryptoFile.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060701
# CVS-ID:       $Id: vtSecCryptoFile.py,v 1.6 2008/02/02 13:43:56 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

MM=0
if MM:
    import mmap
else:
    import os
    import tempfile
import traceback
from vidarc.tool.sec.vtSecCrypto import vtSecCrypto
import cStringIO
#import vidarc.tool.log.vtLog as vtLog

class vtSecCryptoFile(vtSecCrypto):
    BUF_SIZE=1024 #16384
    def __init__(self,phrase,fn=None,dn='~',keySize=2048,algoSes=None):
        vtSecCrypto.__init__(self,phrase,fn=fn,dn=dn,keySize=keySize,algoSes=algoSes)
        self.mm=None
        self.iSize=0
    def EncryptStr(self,s,outFN):
        try:
            outf=open(outFN,'wb')
        except:
            return -3
        try:
            inf=cStringIO.StringIO()
            inf.write(s)
            inf.seek(0)
            self.key.enc(inf,outf)
            del inf
            outf.close()
            return 0
        except:
            del inf
            try:
                outf.close()
            except:
                pass
            return -3
    def EncryptStrFile(self,s,outf):
        try:
            inf=cStringIO.StringIO()
            inf.write(s)
            inf.seek(0)
            self.key.enc(inf,outf)
            del inf
            return 0
        except:
            return -3
        return False
    def Encrypt(self,inFN,outFN):
        if self.key is None:
            return -1
        try:
            inf=open(inFN,'rb')
        except:
            traceback.print_exc()
            return -2
        try:
            outf=open(outFN,'wb')
        except:
            inf.close()
            traceback.print_exc()
            return -3
        iRet=self.EncryptFile(inf,outf)
        outf.close()
        inf.close()
        return iRet
    def EncryptFile(self,inf,outf):
        try:
            self.key.enc(inf,outf)
            outf.flush()
            return 0
            #vtLog.vtLngCurCls(vtLog.DEBUG,'end :%d rd:%d'%(outf.tell(),iRd),self)
        except:
            traceback.print_exc()
            return -3
    def Decrypt(self,inFN,outFN):
        if self.key is None:
            return -1
        try:
            inf=open(inFN,'rb')
        except:
            return -2
        try:
            outf=open(outFN,'wb')
        except:
            inf.close()
            return -3
        iRet=self.DecryptFile(inf,outf)
        outf.close()
        inf.close()
        return iRet
    def DecryptFile(self,inf,outf):
        try:
            self.iSize=0
            self.key.dec(inf,outf)
            outf.flush()
            return 0
        except:
            traceback.print_exc()
        return -3
    def DecryptStr(self,fn):
        if self.key is None:
            return ''
        s=''
        try:
            inf=open(fn,'rb')
            outf=cStringIO.StringIO()
            self.key.dec(inf,outf)
            outf.seek(0)
            s=outf.read()
            inf.close()
            del outf
            return s
        except:
            pass
        try:
            inf.close()
        except:
            pass
        return None
    def DecryptMM(self,fn):
        if self.key is None:
            return -1
        try:
            self.inf=open(fn,'rb')
            if MM:
                self.mm=mmap.mmap(self.inf.fileno(),0,access=mmap.ACCESS_COPY)
            else:
                self.mm=tempfile.TemporaryFile(prefix='vid')
            self.iSize=0
            self.key.dec(self.inf,self.mm)
            self.mm.flush()
            self.iSize=self.mm.tell()
            #vtLog.vtLngCurCls(vtLog.DEBUG,'end :%d rd:%d enc:%s'%(self.iSize,iRd,iEnc),self)
            self.inf.close()
            self.inf=None
            return True
        except:
            self.mm=None
            pass
        try:
            self.inf.close()
            self.inf=None
        except:
            pass
        return False
    def size(self):
        return self.iSize
    def close(self):
        self.mm.close()
    def flush(self):
        self.mm.flush()
    def fileno(self):
        return self.mm.fileno()
    def isatty(self):
        return self.mm.isatty()
    def next(self):
        return self.mm.next()
    def read(self,size=-1):
        if MM:
            if size<0:
                size=self.iSize
            pos=self.mm.tell()
            if pos+size>self.iSize:
                size=self.iSize-pos
            return self.mm.read(size)
        else:
            return self.mm.read(size)
    def seek(self,pos,whence=0):
        if MM:
            if whence==0:
                pass
            elif whence==1:
                pos+=self.mm.tell()
            elif whence==2:
                pos=self.iSize+pos
            self.mm.seek(pos,0)
        else:
            self.mm.seek(pos,whence)
    def tell(self):
        return self.mm.tell()
    def truncate(self,size=-1):
        return self.mm.truncate(size)
