#Boa:FramePanel:vtSecAclFile
#----------------------------------------------------------------------------
# Name:         vtSecAclFile.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060409
# CVS-ID:       $Id: vtSecAclFile.py,v 1.2 2006/07/17 10:54:24 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

from vidarc.tool.xml.vtXmlDomConsumer import *
import vidarc.tool.art.vtArt as vtArt

#import vidarc.tool.sec.vtSecFileAcl as vtSecFileAcl

[wxID_VTSECACLFILE, wxID_VTSECACLFILETGDEL, wxID_VTSECACLFILETGDIST, 
 wxID_VTSECACLFILETGEX, wxID_VTSECACLFILETGGEN, wxID_VTSECACLFILETGRD, 
 wxID_VTSECACLFILETGWR, wxID_VTSECACLFILETGZIP, 
] = [wx.NewId() for _init_ctrls in range(8)]

class vtSecAclFile(wx.Panel,vtXmlDomConsumer):
    def _init_coll_bxsAclFile_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.tgRd, 0, border=4, flag=0)
        parent.AddWindow(self.tgWr, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.tgEx, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.tgDel, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.tgGen, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.tgZip, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.tgDist, 0, border=4, flag=wx.LEFT)

    def _init_sizers(self):
        # generated method, don't edit
        self.bxsAclFile = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_bxsAclFile_Items(self.bxsAclFile)

        self.SetSizer(self.bxsAclFile)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTSECACLFILE, name=u'vtSecAclFile',
              parent=prnt, pos=wx.Point(408, 144), size=wx.Size(254, 62),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(246, 35))

        self.tgRd = wx.lib.buttons.GenBitmapToggleButton(ID=wxID_VTSECACLFILETGRD,
              bitmap=wx.EmptyBitmap(16, 16), name=u'tgRd', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(31, 30), style=0)
        self.tgRd.SetToolTipString(_(u'read'))

        self.tgWr = wx.lib.buttons.GenBitmapToggleButton(ID=wxID_VTSECACLFILETGWR,
              bitmap=wx.EmptyBitmap(16, 16), name=u'tgWr', parent=self,
              pos=wx.Point(35, 0), size=wx.Size(31, 30), style=0)
        self.tgWr.SetToolTipString(_(u'write'))

        self.tgEx = wx.lib.buttons.GenBitmapToggleButton(ID=wxID_VTSECACLFILETGEX,
              bitmap=wx.EmptyBitmap(16, 16), name=u'tgEx', parent=self,
              pos=wx.Point(70, 0), size=wx.Size(31, 30), style=0)
        self.tgEx.SetToolTipString(_(u'execute'))

        self.tgDel = wx.lib.buttons.GenBitmapToggleButton(ID=wxID_VTSECACLFILETGDEL,
              bitmap=wx.EmptyBitmap(16, 16), name=u'tgDel', parent=self,
              pos=wx.Point(105, 0), size=wx.Size(31, 30), style=0)
        self.tgDel.SetToolTipString(_(u'delete'))

        self.tgGen = wx.lib.buttons.GenBitmapToggleButton(ID=wxID_VTSECACLFILETGGEN,
              bitmap=wx.EmptyBitmap(16, 16), name=u'tgGen', parent=self,
              pos=wx.Point(140, 0), size=wx.Size(31, 30), style=0)
        self.tgGen.SetToolTipString(_(u'generate'))

        self.tgZip = wx.lib.buttons.GenBitmapToggleButton(ID=wxID_VTSECACLFILETGZIP,
              bitmap=wx.EmptyBitmap(16, 16), name=u'tgZip', parent=self,
              pos=wx.Point(175, 0), size=wx.Size(31, 30), style=0)
        self.tgZip.SetToolTipString(_(u'archive'))

        self.tgDist = wx.lib.buttons.GenBitmapToggleButton(ID=wxID_VTSECACLFILETGDIST,
              bitmap=wx.EmptyBitmap(16, 16), name=u'tgDist', parent=self,
              pos=wx.Point(210, 0), size=wx.Size(31, 30), style=0)
        self.tgDist.SetToolTipString(_(u'distribute'))

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        vtXmlDomConsumer.__init__(self)
        self._init_ctrls(parent)
        
        self.tgRd.SetBitmapLabel(vtArt.getBitmap(vtArt.Browse))
        self.tgRd.SetBitmapSelected(vtArt.getBitmap(vtArt.Browse))
        
        self.tgWr.SetBitmapLabel(vtArt.getBitmap(vtArt.Save))
        self.tgWr.SetBitmapSelected(vtArt.getBitmap(vtArt.Save))
        
        self.tgEx.SetBitmapLabel(vtArt.getBitmap(vtArt.Build))
        self.tgEx.SetBitmapSelected(vtArt.getBitmap(vtArt.Build))
        
        self.tgDel.SetBitmapLabel(vtArt.getBitmap(vtArt.Del))
        self.tgDel.SetBitmapSelected(vtArt.getBitmap(vtArt.Del))
        
        self.tgGen.SetBitmapLabel(vtArt.getBitmap(vtArt.Report))
        self.tgGen.SetBitmapSelected(vtArt.getBitmap(vtArt.Report))
        
        self.tgZip.SetBitmapLabel(vtArt.getBitmap(vtArt.Zip))
        self.tgZip.SetBitmapSelected(vtArt.getBitmap(vtArt.Zip))
        
        self.tgDist.SetBitmapLabel(vtArt.getBitmap(vtArt.EMail))
        self.tgDist.SetBitmapSelected(vtArt.getBitmap(vtArt.EMail))
        
        self.lMap=[self.tgRd,self.tgWr,self.tgEx,self.tgDel,self.tgGen,self.tgZip,self.tgDist]
        self.sTagName='acl_file'
    def SetTagName(self,sTag):
        self.sTagName=sTag
    def Clear(self):
        vtXmlDomConsumer.Clear(self)
        for tgObj in self.lMap:
            tgObj.SetValue(False)
    def SetNode(self,node):
        self.Clear()
        if self.doc is None:
            return
        self.node=node
        sVal=self.doc.getNodeText(self.node,self.sTagName)
        try:
            iVal=int(sVal,16)
        except:
            iVal=0
        self.SetValue(iVal)
    def GetNode(self,node=None):
        if node is None:
            node=self.node
        if node is None:
            return
        if self.doc is None:
            return
        iAcl=self.GetValue()
        self.doc.setNodeText(node,self.sTagName,'0x%04x'%iAcl)
    def GetValue(self):
        iAcl=0
        iNum=1
        for tgObj in self.lMap:
            if tgObj.GetValue():
                iAcl|=iNum
            iNum<<=1
        return iAcl
    def SetValue(self,iAcl):
        iNum=1
        for tgObj in self.lMap:
            tgObj.SetValue((iAcl&iNum)!=0)
            iNum<<=1
        pass
