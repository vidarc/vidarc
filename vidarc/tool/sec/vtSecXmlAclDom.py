#----------------------------------------------------------------------------
# Name:         vtSecAclXmlDom.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060302
# CVS-ID:       $Id: vtSecXmlAclDom.py,v 1.23 2012/05/09 21:25:07 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.tool.xml.vtXmlFilterType import vtXmlFilterType
    from vidarc.tool.sec.vtSecXmlAclList import vtSecXmlAclList
    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.lang.vtLgBase as vtLgBase

    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
    ACCESS=vcLog.is2Log(__name__,'ACCESS')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vtSecXmlAclDom(vtXmlFilterType):
    ACL_READ                =   0
    ACL_WRITE               =   1
    ACL_EXEC                =   2
    ACL_BUILD               =   3
    ACL_ADD                 =   4
    ACL_DEL                 =   5
    ACL_MOVE                =   6
    
    ACL_MSK_READ            =   2**0
    ACL_MSK_WRITE           =   2**1
    ACL_MSK_EXEC            =   2**2
    ACL_MSK_BUILD           =   2**3
    ACL_MSK_ADD             =   2**4
    ACL_MSK_DEL             =   2**5
    ACL_MSK_MOVE            =   2**6
    
    ACL_LIST                = [ACL_READ  , ACL_WRITE , ACL_EXEC , 
                               ACL_BUILD , ACL_ADD   , ACL_DEL  , 
                               ACL_MOVE]
    
    ACL_MSK_ALL             =  ACL_MSK_READ  | ACL_MSK_WRITE | ACL_MSK_EXEC | ACL_MSK_BUILD | ACL_MSK_ADD   | ACL_MSK_DEL  | ACL_MSK_MOVE
    ACL_MSK_NORMAL          =  ACL_MSK_READ  | ACL_MSK_WRITE | ACL_MSK_ADD  | ACL_MSK_DEL
    ACL_MSK_ATTR            =  ACL_MSK_READ  | ACL_MSK_WRITE 
    
    
    ACL_READ_STR            =   'read'
    ACL_WRITE_STR           =   'write'
    ACL_EXEC_STR            =   'exec'
    ACL_BUILD_STR           =   'build'
    ACL_ADD_STR             =   'add'
    ACL_DEL_STR             =   'del'
    ACL_MOVE_STR            =   'move'
    ACL_MAP2STR = {ACL_READ:ACL_READ_STR,ACL_WRITE:ACL_WRITE_STR,
            ACL_EXEC:ACL_EXEC_STR,ACL_BUILD:ACL_BUILD_STR,
            ACL_ADD:ACL_ADD_STR,ACL_DEL:ACL_DEL_STR,
            ACL_MOVE:ACL_MOVE_STR}
    ACL_MAP2NUM = {ACL_READ_STR:ACL_READ , ACL_WRITE_STR:ACL_WRITE,
            ACL_EXEC_STR:ACL_EXEC , ACL_BUILD_STR:ACL_BUILD,
            ACL_ADD_STR:ACL_ADD , ACL_DEL_STR:ACL_DEL,
            ACL_MOVE_STR:ACL_MOVE}
    def __init__(self):
        global _
        _=vtLgBase.assignPluginLang('vtSec')
        vtXmlFilterType.__init__(self)
        self.bAggressive=False
        self.objAcl=None
        self.ClearAcl()
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        try:
            del self.objAcl
        except:
            #vtLog.vtLngTB(self.__class__.__name__)
            pass
    def IsAclActive(self,mask,acl):
        return (mask & 2**acl) != 0
    def GetTranslation(self,kind,name):
        if name is not None:
            return name
        return kind
    def ClearAcl(self):
        try:
            if VERBOSE>=5:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'',self.GetOrigin())#appl)
        except:
            pass
        del self.objAcl
        self.objAcl=None
    def CreateAcl(self):
        #vtLog.CallStack('')
        try:
            if VERBOSE>=5:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'',self.GetOrigin())#appl)
        except:
            pass
        self.ClearAcl()
        tmpAcl=self.getChild(self.getRoot(),'acl')
        if tmpAcl is None:
            tmpAcl=self.getChild(self.getRoot(),'security_acl')
            if tmpAcl is None:
                return
        try:
            self.objAcl=vtSecXmlAclList(self,tmpAcl,
                    bAggressive=self.bAggressive,verbose=self.verbose-2)
        except:
            self.objAcl=None
            vtLog.vtLngTB(self.GetOrigin())#appl)
            #traceback.print_exc()
    def GetAcl(self,tag):
        try:
            d,l=self.GetPossibleAcl()
            if d.has_key(tag):
                return d[tag]
            return d['all']
        except:
            vtLog.vtLngTB(self.GetOrigin())
            return None
    def GetPossibleAcl(self):
        return None,None
    def GetPossibleFilter(self):
        return None
    def GetPossibleFilterTagName(self,tagName):
        try:
            return self.GetPossibleFilter()[tagName]
        except:
            return None
    def GetSelectableNode(self,node):
        return node
    def GetAcl4Login(self,objLogin):
        if VERBOSE>=5:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'',self.GetOrigin())#appl)
        try:
            dAcl={}
            if self.objAcl is None:
                return dAcl
            d,l=self.GetPossibleAcl()
            for sTag in l:
                if sTag=='all':
                    continue
                bRet,iAcl,iSecLv=self.objAcl.GetAccess(self,sTag,objLogin)
                if bRet:
                    dAcl[sTag]=iAcl
            if VERBOSE>=5:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'acl:%s'%(vtLog.pformat(dAcl)),self.GetOrigin())#appl)
        except:
            return {}
        return dAcl
    def IsAclOk(self,sTag,iAcl2Chk):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'',self.appl)
        try:
            if self.objAcl is None:
                bOk=True
            else:
                bOk=self.objAcl.IsAclOk(sTag,iAcl2Chk)
        except:
            vtLog.vtLngTB(self.GetOrigin())#appl)
            bOk=self.bAggressive
        if VERBOSE>5:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'tag:%s acl:0x%08x ok==%d'%(
                            sTag,iAcl2Chk,bOk),self.GetOrigin())
        return bOk
    def IsNodeAclOk(self,node,iAcl2Chk):
        if node is None:
            return False
        tag=self.getTagName(node)
        return self.IsAclOk(tag,iAcl2Chk)
    def IsAccessOk(self,node,objLogin,iAcl2Chk):
        #vtLog.CallStack('')
        #print self.objAcl
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'',self.appl)
        #vtLog.CallStack('')
        if self.objAcl is None:
            try:
                if VERBOSE>5:
                    if vtLog.vtLngIsLogged(vtLog.INFO):
                        vtLog.vtLngCur(vtLog.INFO,'tag:%s id:%08d acl:0x%08x ok==%d;no acl obj'%(
                            self.getTagName(node),self.getKeyNum(node),iAcl2Chk,True),self.GetOrigin())
            except:
                vtLog.vtLngTB(self.GetOrigin())
            return True
            #return self.bAggressive
        if objLogin is None:
            try:
                if ACCESS>0 & vtLog.vtLngIsLogged(vtLog.INFO):
                    vtLog.vtLngCur(vtLog.INFO,'tag:%s id:%08d acl:0x%08x ok==%d;no login'%(
                            self.getTagName(node),self.getKeyNum(node),iAcl2Chk,True),self.GetOrigin())
            except:
                vtLog.vtLngTB(self.GetOrigin())
            return True
        try:
            tag=self.getTagName(node)
            iAcl=self.GetAcl(tag)
            #print tag,iAcl
            #print '0x%08x 0x%08x'%(iAcl,iAcl2Chk)
            if iAcl is None:
                if ACCESS>0 & vtLog.vtLngIsLogged(vtLog.INFO):
                    vtLog.vtLngCur(vtLog.INFO,
                            'tag:%s id:%08d acl:0x%08x ok==%d;acl cfg not found'%(
                            tag,self.getKeyNum(node),iAcl2Chk,True),self.GetOrigin())
                return True#self.bAggressive
            iAcl&=iAcl2Chk
            #print '0x%08x 0x%08x'%(iAcl,iAcl2Chk)
            
            if iAcl==0:
                if ACCESS>0 & vtLog.vtLngIsLogged(vtLog.INFO):
                    vtLog.vtLngCur(vtLog.INFO,
                            'tag:%s id:%08d acl:0x%08x ok==%d;acl not possible'%(
                            tag,self.getKeyNum(node),iAcl2Chk,False),self.GetOrigin())
                return False
            bOk=self.objAcl.IsAccessOk(self,node,objLogin,iAcl)
            if ACCESS>0 & vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'tag:%s id:%08d acl:0x%08x ok==%d'%(
                        tag,self.getKeyNum(node),iAcl,bOk),self.GetOrigin())
            return bOk
        except:
            vtLog.vtLngTB(self.GetOrigin())#appl)
        if ACCESS>0 & vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'aggressive:%s'%(self.bAggressive),self.GetOrigin())
        return self.bAggressive
    def GetActiveAcl(self):
        #vtLog.CallStack('')
        if VERBOSE>5:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'',self.GetOrigin())#appl)
        node=self.getChild(self.getRoot(),'security_acl')
        for c in self.getChilds(node):
            if self.getAttribute(c,'active')=='1':
                return self.getTagName(c),c
        vtLog.vtLngCur(vtLog.ERROR,'no active acl',self.GetOrigin())#appl)
        return None,None
    def GetPossibleGrouping(self,iFilter):
        if iFilter==self.FILTER_TYPE_DATE:
            return [_(u'year'),_(u'month'),_(u'day'),_(u'week')]
    def GetLoginPossible(self,dUsed=None,dataInst=None):
        try:
            if dUsed is None:
                dUsed={}
            lAct=self.objAcl.GetAclActive()
            for sAclName,dAclAct in lAct:
                if VERBOSE>5:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'dAcl:%s'%(vtLog.pformat(dAclAct)),self.GetOrigin())#appl)
                humAlias=':'.join(['vHum',sAclName])
                docHum=dataInst.GetServedXmlDom(humAlias)
                if docHum is not None:
                    bCalcLogin=True
                else:
                    bCalcLogin=False
                kAcl=dAclAct.keys()
                if sAclName in dUsed:
                    dUsedAcl=dUsed[sAclName]
                else:
                    dUsedAcl={}
                    dUsed[sAclName]=dUsedAcl
                for kAcl,d in dAclAct.items():
                    dUsr=d['user']
                    for idUsr in dUsr.keys():
                        if idUsr>=0:
                            entryAcl=dUsr[idUsr]
                            if idUsr not in dUsedAcl:
                                dUsedAcl[idUsr]=entryAcl.getName()
                            #sLogin=entryAcl.getName()
                            #if sLogin not in dUsedAcl:
                            #    dUsedAcl[sLogin]=idUsr
                            if 0:
                                if bCalcLogin:
                                    nodeUsr=docHum.getNodeByIdNum(idUsr)
                                    if nodeUsr is not None:
                                        oReg=docHum.GetRegByNode(nodeUsr)
                                        sLogin=oReg.GetName(nodeUsr)
                                        if sLogin in dUsedAcl:
                                            dUsedAcl[sLogin]=idUsr
                                else:
                                    dUsedAcl['%08d'%idUsr]=idUsr
                    dGrp=d['group']
                    for idGrp in dGrp.keys():
                        if idGrp>=0:
                            if bCalcLogin:
                                lUsr=docHum.GetGroupMemberLst(idGrp)
                                if VERBOSE>5:
                                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                        vtLog.vtLngCur(vtLog.DEBUG,'idGrp:%08d,lUsr:%s'%(idGrp,vtLog.pformat(lUsr)),self.GetOrigin())#appl)
                                for sIdUsr in lUsr:
                                    idUsr=long(sIdUsr)
                                    if idUsr not in dUsedAcl:
                                        nodeUsr=docHum.getNodeByIdNum(idUsr)
                                        if nodeUsr is not None:
                                            oReg=docHum.GetRegByNode(nodeUsr)
                                            sLogin=oReg.GetName(nodeUsr)
                                            dUsedAcl[idUsr]=sLogin
                                        #if sLogin not in dUsedAcl:
                                        #    dUsedAcl[sLogin]=idUsr
        except:
            vtLog.vtLngTB(self.GetOrigin())#appl)
        
