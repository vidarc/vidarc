#----------------------------------------------------------------------------
# Name:         vtSecXmlAclEntry.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060302
# CVS-ID:       $Id: vtSecXmlAclEntry.py,v 1.14 2008/03/09 19:41:44 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.sec.vtSecXmlAclFilters import *
import vidarc.tool.log.vtLog as vtLog

class vtSecXmlAclEntry:
    def __init__(self,verbose=0):
        self.id=-1
        self.name=None
        self.iAcl=0
        self.iSecLv=-1
        self.flts=None
        self.verbose=verbose
        #self.__build__(doc,tmp)
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        try:
            del self.flts
        except:
            #vtLog.vtLngTB('del')
            pass
    def __str__(self):
        return 'id:%08d name:%s acl:0x%08x sec_lv:%3d filters:%s'%(self.id,
                self.name,self.iAcl,self.iSecLv,self.flts.__str__())
    def __repr__(self):
        return ''.join(['id:',repr(self.id),',name:',self.name,',acl:',
                '0x%08x'%self.iAcl,',secLv:',repr(self.iSecLv)])
    def SetNode(self,lFlt,doc,tmp):
        #print tmp
        try:
            self.id=long(doc.getAttribute(tmp,'fid'))
        except:
            self.id=-1
        self.name=doc.getNodeText(tmp,'name')
        try:
            self.iAcl=long(doc.getAttribute(tmp,'acl'),16)
        except:
            self.iAcl=0
        try:
            self.iSecLv=long(doc.getAttribute(tmp,'sec_level'))
        except:
            self.iSecLv=-1
        try:
            if self.verbose>0:
                vtLog.CallStack('')
                print self
                print 'possible',lFlt
            self.flts=vtSecXmlAclFilters(lFlt,doc,tmp,verbose=self.verbose-1)
            if self.flts.HasFilters()==False:
                self.flts=None
        except:
            vtLog.vtLngTB('')
    def GetNode(self,doc,tmp):
        if len(self.name)==0:
            doc.setAttribute(tmp,'fid',unicode(self.id))
        else:
            if self.id>=0:
                doc.setAttribute(tmp,'fid',doc.ConvertIdNum2Str(self.id))
            doc.setNodeText(tmp,'name',self.name)
        doc.setAttribute(tmp,'acl','0x%08x'%self.iAcl)
        doc.setAttribute(tmp,'sec_level',str(self.iSecLv))
        if self.flts is not None:
            self.flts.GetNode(doc,tmp)
    def getId(self):
        return self.id
    def isIdValid(self):
        if self.id<0:
            if self.id <= -100:
                return True
            return False
        return True
    def getName(self):
        return self.name
    def getNameFull(self):
        if len(self.name)==0:
            return u'__SecLv__%02d'%(-(self.id+100))
        return self.name
    def getNameSecLv(self):
        if len(self.name)==0:
            return u'__SecLv_%02d__'%(self.iSecLv)
        return self.name
    def getAcl(self):
        return self.iAcl
    def getSecLv(self):
        return self.iSecLv
    def setSecLv(self,val):
        self.iSecLv=val
    def getFilter(self,bForced=False):
        if bForced:
            #vtLog.CallStack('')
            if self.flts is None:
                self.flts=vtSecXmlAclFilters(None,None,None,verbose=self.verbose-1)
        return self.flts
    def setAcl(self,iAcl):
        self.iAcl=iAcl
    def setSecLv(self,iSecLv):
        self.iSecLv=iSecLv
    def Set(self,id,name,acl,secLv,flts=None):
        self.id=id
        self.name=name
        self.iAcl=acl
        self.iSecLv=secLv
        self.flts=flts
    def IsAccessOk(self,doc,node,objLogin,iAcl,iSecLv):
        #vtLog.CallStack(doc.appl)
        #print '0x%08x 0x%08x'%(self.iAcl,iAcl)
        #print self.iSecLv,iSecLv,objLogin.GetSecLv()
        if self.iAcl&iAcl==0:
            return False
        if self.iSecLv>=0:
            if self.iSecLv>objLogin.GetSecLv():
                return False
        else:
            if iSecLv>objLogin.GetSecLv():
                return False
        if self.flts is not None:
            if self.flts.IsFilterOk(doc,node):
                return True
            else:
                return False
        else:
            return True
        return False

