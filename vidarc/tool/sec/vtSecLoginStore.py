#----------------------------------------------------------------------------
# Name:         vtSecLoginStore.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060701
# CVS-ID:       $Id: vtSecLoginStore.py,v 1.7 2007/07/28 14:43:33 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.sec.vtSecCryptoXmlDom import vtSecCryptoXmlDom
import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.time.vtTime import vtDateTime

class vtSecLoginStore(vtSecCryptoXmlDom):
    def __init__(self,attr='id',skip=[],synch=False,appl='',verbose=0,
                phrase='',fn=None,dn='~',keySize=2048):
        vtSecCryptoXmlDom.__init__(self,attr=attr,skip=skip,synch=synch,appl=appl,verbose=verbose,
                phrase=phrase,fn=fn,dn=dn,keySize=keySize)
        self.dt=vtDateTime(False)
        self.dtTemp=vtDateTime(False)
        self.sLoginFB=None
        self.sPwdFB=None
    def SetFallback(self,sLogin,sPwd):
        self.sLoginFB=sLogin
        self.sPwdFB=sPwd
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'logins')
    def New(self,revision='1.0',root='loginStore',bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'New',origin=self.appl)
        vtSecCryptoXmlDom.New(self,revision,root)
        if bConnection==False:
            elem=self.createSubNode(self.getRoot(),'logins')
            self.setAttribute(elem,self.attr,'')
            self.checkId(elem)
            self.processMissingId()
            self.clearMissing()
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        self.AlignDoc()
    
    def __procConnAlias__(self,node,alias,usr,l):
        sTag=self.getTagName(node)
        if sTag=='alias':
            #print alias,usr
            sName=self.getNodeText(node,'name')
            #print sName
            i=sName.find('@')
            if i>0:
                if sName[i+1:]==alias:
                    sLogin=self.getNodeText(node,'login')
                    if sLogin==usr:
                        j=sName.find(':')
                        k=sName.find('//')
                        if j>0 and k>0:
                            try:
                                iPort=int(sName[j+1:k])
                                l[0]=sName[:j]
                                l[1]=sName[j+1:k]
                                #self.setNodeText(nodeAlias,'last_connect')
                                return -1
                            except:
                                pass
                #lLogin[1]=self.getNodeText(node,'passwd')
                #lLogin[3]=node
                #return -1
        return 0
    def __procConnection__(self,node,appl,alias,usr,l):
        sTag=self.getTagName(node)
        if sTag=='appl':
            if self.getNodeText(node,'name')==appl:
                self.procChildsExt(node,
                            self.__procConnAlias__,alias,usr,l)
                return -1
        return 0
    def FindConnection(self,appl,alias,usr):
        vtLog.vtLngCurCls(vtLog.DEBUG,'enter',self)
        if self.acquire(blocking=False)==False:
            vtLog.vtLngCurCls(vtLog.DEBUG,'exit 1',self)
            return None
        l=[None,None,None]
        self.procChildsExt(self.getBaseNode(),self.__procConnection__,
                                    appl,alias,usr,l)
        if l[0]!=None:
            val=''.join([l[0],':',l[1],'//',usr,'@',alias])
        else:
            val=None
        self.release()
        vtLog.vtLngCurCls(vtLog.DEBUG,'exit',self)
        return val
    def __procRecentAlias__(self,node,dd):
        sTag=self.getTagName(node)
        if sTag=='alias':
            sLast=self.getNodeText(node,'last_connect')
            if len(sLast)>0:
                sName=self.getNodeText(node,'name')
                dd[sLast]=sName
            return 0
        return 0
    def __procRecent__(self,node,d):
        sTag=self.getTagName(node)
        if sTag=='appl':
            sAppl=self.getNodeText(node,'name')
            if sAppl in d:
                dd=d[sAppl]
            else:
                dd={}
                d[sAppl]=dd
            self.procChildsExt(node,self.__procRecentAlias__,dd)
        return 0
    def GetRecentDict(self,iCount):
        if self.acquire(blocking=False)==False:
            return {}
        try:
            vtLog.vtLngCurCls(vtLog.DEBUG,'enter',self)
            d={}
            self.procChildsExt(self.getBaseNode(),self.__procRecent__,d)
        except:
            pass
        dRecent={}
        for k,dd in d.items():
            l=dd.items()
            l.sort()
            iLen=len(l)
            if iLen==0:
                continue
            if iLen<iCount:
                iCnt=iLen
            else:
                iCnt=iCount
            
            lRecent=[]
            for i in xrange(1,iCnt+1):
                lRecent.append(l[-i][1])
            dRecent[k]=lRecent
        self.release()
        vtLog.vtLngCurCls(vtLog.DEBUG,'exit',self)
        return dRecent
    def __procLoginAlias__(self,node,alias,lLogin):
        sTag=self.getTagName(node)
        if sTag=='alias':
            if self.getNodeText(node,'name')==alias:
                lLogin[0]=self.getNodeText(node,'login')
                lLogin[1]=self.getNodeText(node,'passwd')
                lLogin[3]=node
                return -1
        return 0
    def __procLoginAppl__(self,node,appl,alias,lLogin):
        sTag=self.getTagName(node)
        if sTag=='appl':
            if self.getNodeText(node,'name')==appl:
                lLogin[2]=node
                self.procChildsExt(node,
                            self.__procLoginAlias__,alias,lLogin)
                return -1
        return 0
    def SetLogin(self,appl,host,port,alias,usr,passwd):
        vtLog.vtLngCurCls(vtLog.INFO,'appl:%s;host:%s;port:%s;alias:%s;usr:%s'%(appl,host,port,alias,usr),self)
        if self.acquire(blocking=False)==False:
            return -1
        self.dt.Now()
        try:
            vtLog.vtLngCurCls(vtLog.DEBUG,'enter',self)
            l=[None,None,None,None]
            sAlias=''.join([host,':',str(port),'//',usr,'@',alias])
            self.procChildsExt(self.getBaseNode(),self.__procLoginAppl__,appl,sAlias,l)
        except:
            pass
        if l[2] is None:
            nodeAppl=self.createSubNodeAttr(self.getBaseNode(),'appl',self.attr,'')
            if nodeAppl is None:
                self.release()
                vtLog.vtLngCur(vtLog.ERROR,'failed to add appl:%s'%appl,self.appl)
                return -1
            self.addNode(self.getBaseNode(),nodeAppl)
            self.setNodeText(nodeAppl,'name',appl)
            self.AlignNode(nodeAppl)
        else:
            nodeAppl=l[2]
        
        if l[3] is None:
            nodeAlias=self.createSubNodeAttr(nodeAppl,'alias',self.attr,'')
            if nodeAlias is None:
                self.release()
                vtLog.vtLngCur(vtLog.ERROR,'failed to add alias:%s'%sAlias,self.appl)
                return -1
            self.addNode(nodeAppl,nodeAlias)
            self.setNodeText(nodeAlias,'name',sAlias)
            self.setNodeText(nodeAlias,'login',usr)
            self.setNodeText(nodeAlias,'passwd',passwd)
            self.setNodeText(nodeAlias,'last_connect',self.dt.GetDateTimeStr())
            self.AlignNode(nodeAlias)
        else:
            nodeAlias=l[3]
            self.setNodeText(nodeAlias,'login',usr)
            self.setNodeText(nodeAlias,'passwd',passwd)
        self.release()
        vtLog.vtLngCurCls(vtLog.DEBUG,'exit',self)
    def SetLastOnline(self):
        try:
            if self.acquire(blocking=False)==False:
                return
            vtLog.vtLngCurCls(vtLog.DEBUG,'enter',self)
            node=self.getChildForced(self.getRoot(),'settings')
            self.dt.Now()
            self.setNodeText(node,'last_connect',self.dt.GetDateTimeStr())
            #self.release()
        except:
            pass
        self.release()
        vtLog.vtLngCurCls(vtLog.DEBUG,'exit',self)
    def GetLastOnline(self):
        try:
            if self.acquire(blocking=False)==False:
                return ''
            vtLog.vtLngCurCls(vtLog.DEBUG,'enter',self)
            node=self.getChild(self.getRoot(),'settings')
            if node is None:
                self.release()
                vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
                return None
            s=self.getNodeText(node,'last_connect')
            self.release()
            vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
            return s
        except:
            self.release()
        vtLog.vtLngCurCls(vtLog.DEBUG,'exit',self)
        return ''
    def SetLoginSpecial(self,sSpecial,sHost,sPort,sUser,sPasswd):
        try:
            if self.acquire(blocking=False)==False:
                return
            vtLog.vtLngCurCls(vtLog.DEBUG,'enter',self)
            node=self.getChildForced(self.getRoot(),'settings')
            if node is None:
                self.release()
                vtLog.vtLngCurCls(vtLog.DEBUG,'exit 1',self)
                return
            nodeSpecial=self.getChildForced(node,sSpecial)
            if nodeSpecial is None:
                self.release()
                vtLog.vtLngCurCls(vtLog.DEBUG,'exit 2',self)
                return
            self.setNodeText(nodeSpecial,'host',sHost)
            self.setNodeText(nodeSpecial,'port',sPort)
            self.setNodeText(nodeSpecial,'user',sUser)
            self.setNodeText(nodeSpecial,'passwd',sPasswd)
            self.release()
        except:
            self.release()
        vtLog.vtLngCurCls(vtLog.DEBUG,'exit',self)
    def SetPlugInConnection(self,sHost,sPort,sUser,sPasswd):
        self.SetLoginSpecial('plugin',sHost,sPort,sUser,sPasswd)
    def GetLoginSpecial(self,sSpecial):
        try:
            if self.acquire(blocking=False)==False:
                return '','','',''
            vtLog.vtLngCurCls(vtLog.DEBUG,'enter',self)
            node=self.getChild(self.getRoot(),'settings')
            if node is None:
                vtLog.vtLngCurCls(vtLog.DEBUG,'exit 1',self)
                self.release()
                return '','','',''
            nodeSpecial=self.getChildForced(node,sSpecial)
            if nodeSpecial is None:
                vtLog.vtLngCurCls(vtLog.DEBUG,'exit 2',self)
                self.release()
                return '','','',''
            sHost=self.getNodeText(nodeSpecial,'host')
            sPort=self.getNodeText(nodeSpecial,'port')
            sUser=self.getNodeText(nodeSpecial,'user')
            sPasswd=self.getNodeText(nodeSpecial,'passwd')
            self.release()
            vtLog.vtLngCurCls(vtLog.DEBUG,'exit',self)
            return sHost,sPort,sUser,sPasswd
        except:
            self.release()
        vtLog.vtLngCurCls(vtLog.DEBUG,'exit',self)
        return '','','',''
    def GetPlugInConnection(self):
        return self.GetLoginSpecial('plugin')
    def SetLease(self,val):
        try:
            if self.acquire(blocking=False)==False:
                return
            vtLog.vtLngCurCls(vtLog.DEBUG,'enter',self)
            node=self.getChildForced(self.getRoot(),'settings')
            self.dt.Now()
            self.setNodeText(node,'lease',str(val))
            self.AlignNode(node)
            #self.release()
            #vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
        except:
            #vtLog.vtLngTB(self.__class__.__name__)
            pass
        self.release()
        vtLog.vtLngCurCls(vtLog.DEBUG,'exit',self)
    def GetLease2Expire(self):
        try:
            if self.acquire(blocking=False)==False:
                return -1
            vtLog.vtLngCurCls(vtLog.DEBUG,'enter',self)
            node=self.getChild(self.getRoot(),'settings')
            if node is None:
                self.release()
                vtLog.vtLngCurCls(vtLog.DEBUG,'exit',self)
                return -1
            sDtLastConn=self.getNodeText(node,'last_connect')
            sLease=self.getNodeText(node,'lease')
            iLease=int(sLease)
            self.dtTemp.SetStr(sDtLastConn)
            self.dt.Now()
            diff=self.dtTemp.dt-self.dt.dt
            self.release()
            vtLog.vtLngCurCls(vtLog.DEBUG,'exit',self)
            return diff.days+iLease
        except:
            self.release()
            #vtLog.vtLngTB(self.__class__.__name__)
        vtLog.vtLngCurCls(vtLog.DEBUG,'exit',self)
        return -1
    def GetLogin(self,appl,host,port,alias,usr):
        try:
            if self.acquire(blocking=False)==False:
                return None,None
            vtLog.vtLngCurCls(vtLog.DEBUG,'enter',self)
            vtLog.vtLngCurCls(vtLog.INFO,'appl:%s;host:%s;port:%s;alias:%s;usr:%s'%(appl,host,port,alias,usr),self)
            self.dt.Now()
            l=[None,None,None,None]
            sAlias=''.join([host,':',str(port),'//',usr,'@',alias])
            self.procChildsExt(self.getBaseNode(),self.__procLoginAppl__,
                    appl,sAlias,l)
            if l[3] is not None:
                #self.release()
                self.setNodeText(l[3],'last_connect',self.dt.GetDateTimeStr())
                self.AlignNode(l[3])
            #self.release()
        except:
            pass
        s0,s1=l[0],l[1]
        if l[0]==None:
            s0=self.sLoginFB
            #s1=self.sPwdFB
        if l[1]==None:
            #s0=self.sLoginFB
            s1=self.sPwdFB
        vtLog.vtLngCurCls(vtLog.DEBUG,'exit',self)
        self.release()
        return s0,s1
    def DelLogin(self,appl,host,port,alias,usr):
        vtLog.vtLngCurCls(vtLog.INFO,'appl:%s;host:%s;port:%s;alias:%s;usr:%s'%(appl,host,port,alias,usr),self)
        try:
            if self.acquire(blocking=False)==False:
                return
            vtLog.vtLngCurCls(vtLog.DEBUG,'enter',self)
            sAlias=''.join([host,':',str(port),'//',usr,'@',alias])
            self.DelLoginConn(appl,sAlias)
        except:
            pass
        self.release()
        vtLog.vtLngCurCls(vtLog.DEBUG,'exit',self)
    def DelLoginConn(self,appl,sAlias):
        vtLog.vtLngCurCls(vtLog.INFO,'appl:%s;conn:%s'%(appl,sAlias),self)
        try:
            if self.acquire(blocking=False)==False:
                return
            vtLog.vtLngCurCls(vtLog.DEBUG,'enter',self)
            l=[None,None,None,None]
            self.procChildsExt(self.getBaseNode(),self.__procLoginAppl__,
                    appl,sAlias,l)
            if l[3] is not None:
                self.deleteNode(l[3])
            self.release()
        except:
            self.release()
        vtLog.vtLngCurCls(vtLog.DEBUG,'exit',self)
