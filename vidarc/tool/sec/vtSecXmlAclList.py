#----------------------------------------------------------------------------
# Name:         vtSecXmlAclList.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060302
# CVS-ID:       $Id: vtSecXmlAclList.py,v 1.25 2011/01/12 00:20:40 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.tool.sec.vtSecXmlAclEntry import *
    import vidarc.tool.log.vtLog as vtLog

    import types
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
    ACCESS=vcLog.is2Log(__name__,'ACCESS')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vtSecXmlAclList:
    VERBOSE=0
    def __init__(self,doc,node,bAggressive=True,verbose=0):
        #self.doc=doc
        self.verbose=verbose
        if self.VERBOSE:
            self.verbose=self.VERBOSE
        self.bAggressive=bAggressive
        #self.__build__(doc,node)
        self.dAcl={}
        self.dAcls=None
        self.lAclAct=[]
        self.dAcls={}
        sTag=doc.getTagName(node)
        if sTag=='acl':
            self.SetNodeAcl(node,doc)
        elif sTag=='security_acl':
            self.SetNode(node,doc)
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        try:
            del self.dAcl
            del self.dAcls
            del self.lAclAct
        except:
            #vtLog.vtLngTB(self.__class__.__name__)
            pass
    def __buildAclAccess__(self,doc,tmp):
        try:
            iSecLv=int(doc.getAttribute(tmp,'sec_level'))
        except:
            iSecLv=-1
        d={'user':{},'group':{},'__sec_level':iSecLv}
        try:
            iAcl=int(doc.getAttribute(tmp,'acl'),16)
            sTag=doc.getTagName(tmp)
            dd,ll=doc.GetPossibleAcl()
            try:
                d['__acl']=iAcl&dd[sTag]
            except:
                pass
        except:
            sTag=doc.getTagName(tmp)
            dd,ll=doc.GetPossibleAcl()
            try:
                d['__acl']=dd[sTag]
            except:
                pass
        dUsr=d['user']
        dGrp=d['group']
        sTagName=doc.getTagName(tmp)
        lFlt=doc.GetPossibleFilterTagName(sTagName)
        for c in doc.getChilds(tmp):
            o=vtSecXmlAclEntry(verbose=self.verbose-1)
            o.SetNode(lFlt,doc,c)
            tag=doc.getTagName(c)
            #tag=o.getNameSecLv()
            self.__addAclAccess__(d,tag,o)
        #print sTagName,d
        return d#(d,iSecLv)
    def __getAclAccessTag__(self,d,entry):
        iIdCmp=entry.getId()
        sNameCmp=entry.getName()
        if iIdCmp<0:
            try:
                for name in d[-1]:
                    if name==sNameCmp:
                        return True
            except:
                return False
        else:
            if d.has_key(id):
                return True
            else:
                return False
            #for id,name in d.keys():
            #    if id==iIdCmp:
            #        return True
        return False
    def __addAclAccess__(self,dAcl,tag,entry):
        try:
            dIds=dAcl[tag]
        except:
            dIds={}
            dAcl[tag]=dIds
        if self.__getAclAccessTag__(dIds,entry)==False:
            #if entry.getId()<0:
            if entry.isIdValid()==False:
                if dIds.has_key(-1):
                    dIds[-1][entry.getName()]=entry
                else:
                    dIds[-1]={entry.getName():entry}
            else:
                dIds[entry.getId()]=entry
            #tup=(entry.getId(),entry.getName())
            #dIds[tup]=entry
        pass
        
    def __buildAcl__(self,doc,tmp):
        dAcl={}
        for c in doc.getChilds(tmp):
            d=self.__buildAclAccess__(doc,c)
            try:
                dUsr=d['user']
                bBuild=False
                keys=dUsr.keys()
                if dUsr.has_key(-1):
                    if len(keys)>1:
                        bBuild=True
                else:
                    if len(keys)>0:
                        bBuild=True
                if bBuild:
                    dUsr[-2]={}
                    for k in keys:
                        if k<0:
                            continue
                        e=dUsr[k]
                        dUsr[-2][e.getName()]=e
                #vtLog.CallStack('')
                #print dUsr
            except:
                vtLog.vtLngTB(doc.appl)
            dAcl[doc.getTagName(c)]=d
        dPos,lPos=doc.GetPossibleAcl()
        if self.VERBOSE:
            vtLog.vtLngCur(vtLog.DEBUG,'acl:%s;possible:%s'%(vtLog.pformat(dAcl),vtLog.pformat(dPos)),doc.appl)
        # check node to add
        keys=dPos.keys()
        for k in keys:
            if not dAcl.has_key(k):
                if self.VERBOSE:
                    vtLog.vtLngCur(vtLog.DEBUG,'add:%s'%(k),doc.appl)
                c=doc.getChildForced(tmp,k)
                d=self.__buildAclAccess__(doc,c)
                dAcl[k]=d
        # check node to delete
        keys=dAcl.keys()
        for k in keys:
            if not dPos.has_key(k):
                c=doc.getChild(tmp,k)
                del dAcl[k]
                doc.deleteNode(c,tmp)
        #if self.verbose>0:
        #    vtLog.CallStack('')
        #    for k in dAcl.keys():
                #print '   ',k
        #        d=dAcl[k]
        #        print d
                #for kk in d.keys():
                    #print '     ',kk
                #    dd=d[kk]
                #    for kkk in dd.keys():
                #        print '       ',dd[kkk]
        return dAcl
    def SetNode(self,node,doc):
        if self.verbose>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'',doc.appl)
        try:
            del self.dAcls
            del self.lAclAct
            self.dAcls=None
            self.lAclAct=[]
            self.dAcls={}
            if node is None:
                return 
            
            for c in doc.getChilds(node):
                sTag=doc.getTagName(c)
                if sTag=='__cache__':
                    continue
                if doc.getAttribute(c,'active')=='1':
                    bAct=True
                else:
                    bAct=False
                d=self.__buildAcl__(doc,c)
                self.dAcls[sTag]={'acl':d,'active':bAct}
                if bAct:
                    self.lAclAct.append([sTag,d])
        except:
            vtLog.vtLngTB(doc.appl)
        if self.verbose>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,vtLog.pformat(self.dAcls),doc.appl)
    def IsAclOk(self,tag,iAcl):
        if self.verbose>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurCls(vtLog.DEBUG,'tag:%s;iAcl:0x%08x;dAcl:%s'%(tag,iAcl,vtLog.pformat(self.dAcl)),self)
        try:
            if tag in self.dAcl:
                if (self.dAcl[tag]&iAcl)==iAcl:
                    if self.verbose>0:
                        if vtLog.vtLngIsLogged(vtLog.INFO):
                            vtLog.vtLngCurCls(vtLog.DEBUG,'==True;tag:%s;iAcl:0x%08x'%(tag,iAcl),self)
                    return True
            if self.verbose>0:
                if vtLog.vtLngIsLogged(vtLog.INFO):
                    vtLog.vtLngCurCls(vtLog.DEBUG,'==%s;tag:%s;iAcl:0x%08x'%(self.bAggressive,tag,iAcl),self)
            return self.bAggressive
        except:
            if vtLog.vtLngIsLogged(vtLog.ERROR):
                vtLog.vtLngCurCls(vtLog.ERROR,'==%s;tag:%s;iAcl:0x%08x'%(self.bAggressive,tag,iAcl),self)
            return self.bAggressive
    def SetNodeAcl(self,node,doc):
        if self.verbose>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'',doc.appl)
        try:
            del self.dAcl
            self.dAcl={}
            if node is None:
                return 
            for c in doc.getChilds(node):
                sTag=doc.getTagName(c)
                if sTag=='__cache__':
                    continue
                sAcl=doc.getAttribute(c,'acl')
                iAcl=long(sAcl,16)
                self.dAcl[sTag]=iAcl
        except:
            vtLog.vtLngTB(doc.appl)
        if self.verbose>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,vtLog.pformat(self.dAcl),doc.appl)
    def SetActive(self,name,flag):
        try:
            d=self.dAcls[name]
            if d['active']!=flag:
                d['active']=flag
                self.UpdateActive()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def UpdateActive(self):
        try:
            keys=self.dAcls.keys()
            keys.sort()
            self.lAclAct=[]
            for k in keys:
                d=self.dAcls[k]
                if d['active']:
                    self.lAclAct.append([k,d])
        except:
            vtLog.vtLngTB(doc.appl)
    def GetNode(self,node,doc):
        try:
            if self.verbose>0:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,vtLog.pformat(self.dAcls),doc.appl)
            if doc is None:
                return
            if node is None:
                return
            for c in doc.getChilds(node):
                doc.deleteNode(c,node)
            keys=self.dAcls.keys()
            keys.sort()
            for k in keys:
                c=doc.createSubNode(node,k)
                d=self.dAcls[k]
                if d['active']:
                    doc.setAttribute(c,'active','1')
                else:
                    doc.setAttribute(c,'active','0')
                dd=d['acl']
                #cc=doc.createSubNode(c,kk)
                kkeys=dd.keys()
                #print dd
                for kk in kkeys:
                    if kk[:2]=='__':
                        doc.setAttribute(c,kk[2:],str(dd[kk]))
                        continue
                    #print '    ',kk
                    cc=doc.createSubNode(c,kk)
                    ddd=dd[kk]
                    kkkeys=ddd.keys()
                    for kkk in kkkeys:
                        #print ddd
                        if kkk=='__acl':
                            doc.setAttribute(cc,'acl','0x%08x'%(ddd[kkk]))
                            continue
                        if kkk[:2]=='__':
                            doc.setAttribute(cc,kkk[2:],str(ddd[kkk]))
                            continue
                        dddd=ddd[kkk]
                        kkkkeys=dddd.keys()
                        kkkkeys.sort()
                        for kkkk in kkkkeys:
                            #doc.setAttribute(ccc,'fid',doc.ConvertIdNum2Str(kkkk[0]))
                            #doc.setNodeText(ccc,'name',kkkk[1])
                            if kkkk==-2:
                                continue
                            entry=dddd[kkkk]
                            if type(entry)==types.DictionaryType:
                                for kE in entry.keys():
                                    ccc=doc.createSubNode(cc,kkk)
                                    entry[kE].GetNode(doc,ccc)
                            else:
                                ccc=doc.createSubNode(cc,kkk)
                                entry.GetNode(doc,ccc)
            doc.AlignNode(node,iRec=4)
        except:
            vtLog.vtLngTB(doc.appl)
            
    def GetAclActiveNames(self):
        l=[]
        for sName,sAcl in self.lAclAct:
            l.append(sName)
        return l
    def GetAclActive(self):
        return self.lAclAct
    def GetAcl(self,name):
        try:
            d=self.dAcls[name]
            return d['acl'],d['active']
        except:
            return None,False
    def GetAclTagName(self,name,tagName):
        dAcl,bAct=self.GetAcl(name)
        try:
            d=dAcl[tagName]
            return d
        except:
            return None
    def AddAclTagNameHum(self,name,tagName,sKind,fid,sName,iAcl,iSecLv):
        #d=self.GetAclTagName(name,tagName)
        #if d is not None:
        #if self.VERBOSE:
        #    vtLog.CallStack('')
        dAcl,bAct=self.GetAcl(name)
        dAcl=self.GetAclTagName(name,tagName)
        #if self.VERBOSE:
        #    print 'dAcl not found for ',name,tagName
        if dAcl is None:
            return None
        entry=vtSecXmlAclEntry(verbose=self.verbose-1)
        entry.Set(fid,sName,iAcl,iSecLv,vtSecXmlAclFilters(None,None,None))
        self.__addAclAccess__(dAcl,sKind,entry)
        return entry
    def DelAclTagName(self,name,tagName):
        dAcl,bAct=self.GetAcl(name)
        try:
            vtLog.CallStack('')
            print dAcl[tagName].keys()
        except:
            pass
    def DelAclTagNameKind(self,name,tagName,kind):
        dAcl,bAct=self.GetAcl(name)
        try:
            dAcl[tagName][kind]={}
        except:
            pass
    def DelAclTagNameKindEntry(self,name,tagName,kind,entry):
        dAcl,bAct=self.GetAcl(name)
        try:
            id=entry.getId()
            acl=dAcl[tagName][kind]
            if id>=0:
                del acl[id]
                return
            else:
                #FIXME
                pass
        except:
            pass
    def __isAccessOkFallBack__(self,doc,objLogin,iSecLv,iAclFB,iAcl):
        try:
            if iSecLv>=0:
                if objLogin.GetSecLv()>=iSecLv:
                    if self.verbose>0:
                        vtLog.vtLngCur(vtLog.DEBUG,
                                'login secLv:%d,secLv:%d;ok'%
                                (objLogin.GetSecLv(),iSecLv),doc.appl)
                    if (iAclFB&iAcl)==iAcl:
                        return True
        except:
            vtLog.vtLngTB(doc.appl)
        if self.verbose>0:
            vtLog.vtLngCur(vtLog.DEBUG,'aggressive:ok==%d;secLv:%d'%
                    (self.bAggressive,iSecLv),doc.appl)
        if iSecLv==-2:      # 070622:wro do not check without iAcl
            #if doc.IsLoggedInSelf(obj)==True:
            #    return True
            if (iAclFB&iAcl)==iAcl:
                return True
        #if iSecLv==-3:
        #    return True
        return self.bAggressive
    def __isAccessOk__(self,doc,node,objLogin,iAcl,d):
        try:
            if self.verbose>0:
                vtLog.vtLngCur(vtLog.DEBUG,'tagName:%s;login:%s;acl:0x%08x;d:%s'%(doc.getTagName(node),
                        objLogin,iAcl,vtLog.pformat(d)),doc.appl)
            #print objLogin
            #print d
            bUsr=False
            iSecLv=d['__sec_level']
            iAclFB=d['__acl']
            try:
                dUsr=d['user']
                obj=None
                if objLogin.GetUsrId()<0:
                    if -1 in dUsr:
                        if objLogin.GetUsr() in dUsr[-1]:
                            obj=dUsr[-1][objLogin.GetUsr()]
                    if obj is None:
                        if -2 in dUsr:
                            if objLogin.GetUsr() in dUsr[-2]:
                                obj=dUsr[-2][objLogin.GetUsr()]
                    if obj is None:
                        return self.__isAccessOkFallBack__(doc,objLogin,
                                iSecLv,iAclFB,iAcl),True
                    #try:
                    #    obj=dUsr[-1][objLogin.GetUsr()]
                    #except:
                    #    try:
                    #        obj=dUsr[-2][objLogin.GetUsr()]
                    #    except:
                    #        return self.__isAccessOkFallBack__(doc,objLogin,iSecLv)
                else:
                    if objLogin.GetUsrId() in dUsr:
                        obj=dUsr[objLogin.GetUsrId()]
                        if self.verbose>0:
                            vtLog.vtLngCur(vtLog.DEBUG,'obj:%s'%(obj),doc.appl)
                if obj is not None:
                    bUsr=True
                    if obj.IsAccessOk(doc,node,objLogin,iAcl,iSecLv):
                        return True,False
                for k,obj in dUsr.iteritems():
                    if k>-100:
                        continue
                    if obj.IsAccessOk(doc,node,objLogin,iAcl,iSecLv):
                        return True,False
                    #else:
                    #    
                    #    return False
                #else:
                #    bUsr=False
            except:
                vtLog.vtLngTB(doc.appl)
            try:
                # check group
                dGrp=d['group']
                grpIds=objLogin.GetGrpIds()
                if self.verbose>0:
                    vtLog.vtLngCur(vtLog.DEBUG,'grpIds:%s;len:%d'%(grpIds,len(grpIds)),doc.appl)
                #if len(grpIds)==0:  # 060916 dact
                #    if bUsr:
                #        return False
                #    return self.__isAccessOkFallBack__(doc,objLogin,iSecLv)#self.bAggressive
                for grpId in grpIds:
                    if grpId in dGrp:
                        obj=dGrp[grpId]
                        if self.verbose>0:
                            vtLog.vtLngCur(vtLog.DEBUG,'grpId:%d;obj:%s'%(grpId,obj),doc.appl)
                        if obj.IsAccessOk(doc,node,objLogin,iAcl,iSecLv):
                            return True,False
                #if bUsr:  # 060916 dact
                #    return False
                if iSecLv==-3:
                    if doc.IsLoggedInSelf(node,objLogin.GetUsrId())==True:
                        if (iAclFB&iAcl)==iAcl:
                            return True,True
                    return False,True    # 070928:wro stop search
                if iSecLv==-2:      # 070622:wro valid for all user
                    if (iAclFB&iAcl)==iAcl:
                        return True,True
                    return False,True    # 070928:wro stop search
                return self.__isAccessOkFallBack__(doc,objLogin,iSecLv,
                        iAclFB,iAcl),True
            except:
                vtLog.vtLngTB(doc.appl)
        except:
            vtLog.vtLngTB(doc.appl)
        return self.__isAccessOkFallBack__(doc,objLogin,iSecLv,iAclFB,iAcl),True
    def IsAccessOk(self,doc,node,objLogin,iAcl):
        #vtLog.CallStack('')
        #vtLog.CallStack(iAcl)
        #self.doc.acquire()
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'aggressive:%d;acl:0x%08x;node:%s;objLogin:%s'%(self.bAggressive,
        #                    iAcl,doc.getTagName(node),objLogin),doc.appl)
        tag=doc.getTagName(node)
        try:
            for sName,dAcl in self.lAclAct:
                if self.verbose>0:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'tag:%s;name:%s;dAcl:%s'%(tag,sName,
                                    vtLog.pformat(dAcl)),doc.appl)
                #vtLog.CallStack('')
                #print sName,dAcl,tag
                if tag in dAcl:
                    d=dAcl[tag]
                    bRes,bChkAll=self.__isAccessOk__(doc,node,objLogin,iAcl,d)
                    if bRes:
                        if ACCESS>0 & vtLog.vtLngIsLogged(vtLog.INFO):
                            vtLog.vtLngCur(vtLog.INFO,'id:%s;%s;result:ok==%d'%
                                (doc.getKey(node),objLogin,bRes),doc.appl)
                        return True
                    if bChkAll==False:
                        return False
                    #else:
                    #    return False
                if 'all' in dAcl:
                    d=dAcl['all']
                    bRes,bChkAll=self.__isAccessOk__(doc,node,objLogin,iAcl,d)
                    if ACCESS>0 & vtLog.vtLngIsLogged(vtLog.INFO):
                        vtLog.vtLngCur(vtLog.INFO,'id:%s;%s;result:ok==%d'%
                                (doc.getKey(node),objLogin,bRes),doc.appl)
                    return bRes
        except:
            vtLog.vtLngTB(doc.appl)
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'id:%s;%s;result:ok==%d'%
                        (doc.getKey(node),objLogin,self.bAggressive),doc.appl)
            return self.bAggressive
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%s;%s;result:ok==%d'%
                    (doc.getKey(node),objLogin,True),doc.appl)
        return True
    def __getAccess__(self,doc,sTag,objLogin,d):
        try:
            if self.verbose>0:
                vtLog.vtLngCur(vtLog.DEBUG,'tagName:%s;login:%s;d:%s'%(sTag,
                        objLogin,vtLog.pformat(d)),doc.appl)
            bUsr=False
            bGrp=False
            iAcl=0
            iSecLv=0
            try:
                dUsr=d['user']
                obj=None
                if objLogin.GetUsrId()<0:
                    if -1 in dUsr:
                        if objLogin.GetUsr() in dUsr[-1]:
                            obj=dUsr[-1][objLogin.GetUsr()]
                    if obj is None:
                        if -2 in dUsr:
                            if objLogin.GetUsr() in dUsr[-2]:
                                obj=dUsr[-2][objLogin.GetUsr()]
                else:
                    if objLogin.GetUsrId() in dUsr:
                        obj=dUsr[objLogin.GetUsrId()]
                        if self.verbose>0:
                            vtLog.vtLngCur(vtLog.DEBUG,'obj:%s'%(obj),doc.appl)
                if obj is not None:
                    bUsr=True
                    if obj.getSecLv()<=objLogin.GetSecLv():
                        if self.verbose>0:
                            vtLog.vtLngCur(vtLog.DEBUG,'iAcl:0x%08x;secLv:%d'%(iAcl,iSecLv),doc.appl)
                        iAcl|=obj.getAcl()
                        if iSecLv<obj.getSecLv():
                            iSecLv=obj.getSecLv()
                        if self.verbose>0:
                            vtLog.vtLngCur(vtLog.DEBUG,'iAcl:0x%08x;secLv:%d'%(iAcl,iSecLv),doc.appl)
                iSecLvAct=-1
                for k,obj in dUsr.iteritems():
                    if k>-100:
                        continue
                    if iSecLv<obj.getSecLv():
                        iSecLvAct=max(iSecLvAct,obj.getSecLv())
                        if obj.getSecLv()<=objLogin.GetSecLv():
                            bUsr=True
                            if self.verbose>0:
                                vtLog.vtLngCur(vtLog.DEBUG,'iAcl:0x%08x;secLv:%d'%(iAcl,iSecLv),doc.appl)
                            iAcl|=obj.getAcl()
                            if self.verbose>0:
                                vtLog.vtLngCur(vtLog.DEBUG,'iAcl:0x%08x;secLv:%d'%(iAcl,iSecLv),doc.appl)
                iSecLv=max(iSecLvAct,iSecLv)
            except:
                vtLog.vtLngTB(doc.appl)
            try:
                # check group
                dGrp=d['group']
                grpIds=objLogin.GetGrpIds()
                if self.verbose>0:
                    vtLog.vtLngCur(vtLog.DEBUG,'grpIds:%s;len:%d'%(grpIds,len(grpIds)),doc.appl)
                for grpId in grpIds:
                    if grpId in dGrp:
                        obj=dGrp[grpId]
                        if self.verbose>0:
                            vtLog.vtLngCur(vtLog.DEBUG,'grpId:%d;obj:%s'%(grpId,obj),doc.appl)
                        if obj.getSecLv()<=objLogin.GetSecLv():
                            bGrp=True
                            if self.verbose>0:
                                vtLog.vtLngCur(vtLog.DEBUG,'iAcl:0x%08x;secLv:%d'%(iAcl,iSecLv),doc.appl)
                            iAcl|=obj.getAcl()
                            if iSecLv<obj.getSecLv():
                                iSecLv=obj.getSecLv()
                            if self.verbose>0:
                                vtLog.vtLngCur(vtLog.DEBUG,'iAcl:0x%08x;secLv:%d'%(iAcl,iSecLv),doc.appl)
            except:
                vtLog.vtLngTB(doc.appl)
            if bUsr or bGrp:
                return True,iAcl,iSecLv
            else:
                #print 'fb'
                #for sName,dAcl in self.GetAclActive():
                #    if sTag in dAcl:
                #        d=dAcl[sTag]
                        #print 'fb'
                        if '__sec_level' in d:
                            iSecLv=d['__sec_level']
                            if iSecLv<-1:
                                if '__acl' in d:
                                    return True,d['__acl'],0
                            elif iSecLv==-1:
                                pass#return True,0,0
                            elif iSecLv<=objLogin.GetSecLv():
                                if '__acl' in d:
                                    return True,d['__acl'],iSecLv
            return self.bAggressive,0xFFFFFF,0
        except:
            vtLog.vtLngTB(doc.appl)
        return False,0,0
    def GetAccess(self,doc,tag,objLogin):
        try:
            for sName,dAcl in self.lAclAct:
                if tag in dAcl:
                    d=dAcl[tag]
                    bRet,iAcl,iSecLv=self.__getAccess__(doc,tag,objLogin,d)
                if 'all' in dAcl:
                    d=dAcl['all']
                    bRetAll,iAclAll,iSecLvAll=self.__getAccess__(doc,'all',objLogin,d)
                if bRet and bRetAll:
                    if iSecLvAll>=0:
                        if iSecLvAll<iSecLv:
                            iSecLv=iSecLvAll
                    if ACCESS>0 & vtLog.vtLngIsLogged(vtLog.INFO):
                        vtLog.vtLngCur(vtLog.INFO,
                                'tag:%s;bRet:%d;iAcl:0x%08x;iSecLv:%d'%
                                (tag,True,iAcl|iAclAll,iSecLv),doc.appl)
                    return True,iAcl|iAclAll,iSecLv
                if bRet:
                    if ACCESS>0 & vtLog.vtLngIsLogged(vtLog.INFO):
                        vtLog.vtLngCur(vtLog.INFO,
                                'tag:%s;bRet:%d;iAcl:0x%08x;iSecLv:%d'%
                                (tag,bRet,iAcl,iSecLv),doc.appl)
                    return bRet,iAcl,iSecLv
                if bRetAll:
                    if ACCESS>0 & vtLog.vtLngIsLogged(vtLog.INFO):
                        vtLog.vtLngCur(vtLog.INFO,
                                'tag:%s;bRetAll:%d;iAclAll:0x%08x;iSecLvAll:%d'%
                                (tag,bRetAll,iAclAll,iSecLvAll),doc.appl)
                    return bRetAll,iAclAll,iSecLvAll
        except:
            vtLog.vtLngTB(doc.appl)
        return False,0,-1
