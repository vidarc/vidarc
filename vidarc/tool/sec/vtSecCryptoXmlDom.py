#----------------------------------------------------------------------------
# Name:         vtSecCryptoXmlDom.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060701
# CVS-ID:       $Id: vtSecCryptoXmlDom.py,v 1.7 2008/03/05 14:18:22 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.InOut.fnUtil as fnUtil

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDom import vtXmlDom
from vidarc.tool.sec.vtSecCryptoFile import vtSecCryptoFile

class vtSecCryptoXmlDom(vtXmlDom,vtSecCryptoFile):
    def __init__(self,attr='id',skip=[],synch=False,appl='',verbose=0,
                audit_trail=True,
                phrase='',fn=None,dn='~',keySize=2048):
        vtXmlDom.__init__(self,attr=attr,skip=skip,synch=synch,appl=appl,
                verbose=verbose,audit_trail=False)
        vtSecCryptoFile.__init__(self,phrase,fn=fn,dn=dn,keySize=keySize)
    def NotifyOpenStart(self,fn):
        pass
    def NotifyOpenFault(self,fn,lst):
        pass
    def NotifyOpenOk(self,fn):
        pass
    def open(self,sFN):
        try:
            s=vtSecCryptoFile.DecryptStr(self,sFN)
            self.doc,self.root=self.__parseBuffer__(s)
            return 0
        except:
            vtLog.vtLngTB(self.GetOrigin())
            return -1
    def save(self,sTmpFN,encoding='ISO-8859-1'):
        try:
            fnUtil.shiftFile(sTmpFN)
            vtSecCryptoFile.EncryptStr(self,self.GetXmlContent(),sTmpFN)
            return 0
        except:
            vtLog.vtLngTB(self.GetOrigin())
        return -1
