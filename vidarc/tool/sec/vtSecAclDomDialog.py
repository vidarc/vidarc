#Boa:Frame:vtSecAclDomDialog
#----------------------------------------------------------------------------
# Name:         vtSecAclDomDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060409
# CVS-ID:       $Id: vtSecAclDomDialog.py,v 1.7 2007/06/29 18:23:32 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

from vidarc.tool.sec.vtSecAclDomPanel import *
import vidarc.tool.art.vtArt as vtArt

def create(parent):
    return vtSecAclDomDialog(parent)

[wxID_VTSECACLDOMDIALOG] = [wx.NewId() for _init_ctrls in range(1)]

class vtSecAclDomDialog(wx.Frame):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VTSECACLDOMDIALOG,
              name=u'vtSecAclDomDialog', parent=prnt, pos=wx.Point(66, 66),
              size=wx.Size(532, 474), style=wx.RESIZE_BORDER | wx.DEFAULT_FRAME_STYLE | wx.STAY_ON_TOP,
              title=u'vtSec ACL XML-DOM')
        self.SetClientSize(wx.Size(524, 447))
        self.Bind(wx.EVT_CLOSE, self.OnVtSecAclDomDialogClose)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        icon= wx.EmptyIcon()
        icon.CopyFromBitmap(vtArt.getBitmap(vtArt.Safe))
        self.SetIcon(icon)
        
        self.pn=vtSecAclDomPanel(parent=self,id=-1,name='vtSecAclDomPanel',
                    pos=(0,0),size=(524, 447),style=0)
        EVT_VTSEC_ACLDOM_PANEL_APPLIED(self.pn,self.OnApplied)
        EVT_VTSEC_ACLDOM_PANEL_CANCELED(self.pn,self.OnCanceled)
    def OnApplied(self,evt):
        wx.PostEvent(self,vtSecAclDomPanelApplied(self))
        self.Show(False)
    def OnCanceled(self,evt):
        wx.PostEvent(self,vtSecAclDomPanelCanceled(self))
        self.Show(False)
    def SetDoc(self,doc,bNet=False):
        self.pn.SetDoc(doc,bNet)
    def SetDocHums(self,docs,bNet=False):
        self.pn.SetDocHums(docs,bNet)
    def SetDocHum(self,doc,bNet=False):
        self.pn.SetDocHum(doc,bNet)
    def SetNode(self,node):
        self.pn.SetNode(node)
    def GetNode(self,node=None):
        self.pn.GetNode(node)

    def OnVtSecAclDomDialogClose(self, event):
        #event.Skip()
        wx.PostEvent(self,vtSecAclDomPanelCanceled(self))
        self.Show(False)
        
    
