#Boa:FramePanel:vtSecAclDomPanel
#----------------------------------------------------------------------------
# Name:         vtSecAclDomPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060409
# CVS-ID:       $Id: vtSecAclDomPanel.py,v 1.24 2008/03/26 23:14:30 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.xml.vtXmlFilterTypeInput
import vidarc.vApps.vHum.vXmlHumInputTreeGrp
import vidarc.vApps.vHum.vXmlHumInputTree
import wx.gizmos
import wx.lib.buttons

from vidarc.tool.xml.vtXmlFilterType import vtXmlFilterType
from vidarc.tool.sec.vtSecXmlAclDom import vtSecXmlAclDom
from vidarc.tool.sec.vtSecXmlAclList import vtSecXmlAclList
from vidarc.tool.sec.vtSecXmlAclEntry import vtSecXmlAclEntry
from vidarc.tool.sec.vtSecXmlAclFilters import vtSecXmlAclFilters
from vidarc.tool.xml.vtXmlDomConsumer import *
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog

import types,copy

VERBOSE=0

[wxID_VTSECACLDOMPANEL, wxID_VTSECACLDOMPANELCBADDFLT, 
 wxID_VTSECACLDOMPANELCBAPPLY, wxID_VTSECACLDOMPANELCBAPPLYACL, 
 wxID_VTSECACLDOMPANELCBAPPLYFLT, wxID_VTSECACLDOMPANELCBCANCEL, 
 wxID_VTSECACLDOMPANELCBDELFLT, wxID_VTSECACLDOMPANELCBFLTDN, 
 wxID_VTSECACLDOMPANELCBFLTUP, wxID_VTSECACLDOMPANELCBUSRCLR, 
 wxID_VTSECACLDOMPANELCHCFILTERATTR, wxID_VTSECACLDOMPANELCHCFILTERPOS, 
 wxID_VTSECACLDOMPANELCHCHUM, wxID_VTSECACLDOMPANELCKCACT, 
 wxID_VTSECACLDOMPANELLBLACL, wxID_VTSECACLDOMPANELLBLACLDUMMY, 
 wxID_VTSECACLDOMPANELLBLATTR, wxID_VTSECACLDOMPANELLBLFILTER, 
 wxID_VTSECACLDOMPANELLBLFILTERATTRDESC, wxID_VTSECACLDOMPANELLBLHUMAN, 
 wxID_VTSECACLDOMPANELLBLNODENAME, wxID_VTSECACLDOMPANELLBLSECLV, 
 wxID_VTSECACLDOMPANELLBLTAG, wxID_VTSECACLDOMPANELLBLUSR, 
 wxID_VTSECACLDOMPANELSPSECLV, wxID_VTSECACLDOMPANELTGACLADD, 
 wxID_VTSECACLDOMPANELTGACLBUILD, wxID_VTSECACLDOMPANELTGACLDEL, 
 wxID_VTSECACLDOMPANELTGACLEXEC, wxID_VTSECACLDOMPANELTGACLMOVE, 
 wxID_VTSECACLDOMPANELTGACLREAD, wxID_VTSECACLDOMPANELTGACLWRITE, 
 wxID_VTSECACLDOMPANELTRLSTACL, wxID_VTSECACLDOMPANELVIFLTDATE, 
 wxID_VTSECACLDOMPANELVIFLTDATETIME, wxID_VTSECACLDOMPANELVIFLTFLOAT, 
 wxID_VTSECACLDOMPANELVIFLTINT, wxID_VTSECACLDOMPANELVIFLTLONG, 
 wxID_VTSECACLDOMPANELVIFLTSTR, wxID_VTSECACLDOMPANELVIFLTTIME, 
 wxID_VTSECACLDOMPANELVIUSR, 
] = [wx.NewId() for _init_ctrls in range(41)]

wxEVT_VTSEC_ACLDOM_PANEL_APPLIED=wx.NewEventType()
vEVT_VTSEC_ACLDOM_PANEL_APPLIED=wx.PyEventBinder(wxEVT_VTSEC_ACLDOM_PANEL_APPLIED,1)
def EVT_VTSEC_ACLDOM_PANEL_APPLIED(win,func):
    win.Connect(-1,-1,wxEVT_VTSEC_ACLDOM_PANEL_APPLIED,func)
class vtSecAclDomPanelApplied(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTSEC_ACLDOM_PANEL_APPLIED(<widget_name>, self.OnInfoApplied)
    """

    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VTSEC_ACLDOM_PANEL_APPLIED)
        self.obj=obj
    def GetObject(self):
        return self.obj

wxEVT_VTSEC_ACLDOM_PANEL_CANCELED=wx.NewEventType()
vEVT_VTSEC_ACLDOM_PANEL_CANCELED=wx.PyEventBinder(wxEVT_VTSEC_ACLDOM_PANEL_CANCELED,1)
def EVT_VTSEC_ACLDOM_PANEL_CANCELED(win,func):
    win.Connect(-1,-1,wxEVT_VTSEC_ACLDOM_PANEL_CANCELED,func)
class vtSecAclDomPanelCanceled(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTSEC_ACLDOM_PANEL_CANCELED(<widget_name>, self.OnInfoCanceled)
    """

    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VTSEC_ACLDOM_PANEL_CANCELED)
        self.obj=obj
    def GetObject(self):
        return self.obj

class vtSecAclDomPanel(wx.Panel,vtXmlDomConsumer):
    VERBOSE=0
    FILTER_COL_START=9
    def _init_coll_bxsUsrBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbUsrClr, 0, border=0, flag=0)

    def _init_coll_bxsAclSecLbl_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblAcl, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddSizer(self.bxsAclSec, 4, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbApplyAcl, 0, border=4, flag=wx.LEFT)

    def _init_coll_bxsAttr_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblAttr, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.chcFilterAttr, 2, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.lblFilterAttrDesc, 2, border=4,
              flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsSecLv_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblSecLv, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.spSecLv, 1, border=4, flag=wx.RIGHT | wx.EXPAND)

    def _init_coll_bxsAcl_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.tgAclRead, 0, border=4, flag=0)
        parent.AddWindow(self.tgAclWrite, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.tgAclAdd, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.tgAclDel, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.tgAclMove, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.tgAclExec, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.tgAclBuild, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.lblAclDummy, 1, border=0, flag=wx.EXPAND)

    def _init_coll_bxsFlt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viFltStr, 3, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltInt, 3, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltLong, 3, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltFloat, 3, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltDate, 3, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltTime, 3, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFltDateTime, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbFltUp, 0, border=4, flag=wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.cbFltDn, 0, border=4, flag=0)

    def _init_coll_bxsInfoBtInfo_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsInfo, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsBtInfo, 0, border=4, flag=wx.LEFT | wx.TOP)

    def _init_coll_bxsHuman_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblHuman, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.chcHum, 2, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.ckcAct, 1, border=0, flag=wx.EXPAND)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 1, border=8, flag=wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.cbCancel, 1, border=8, flag=wx.RIGHT | wx.LEFT)

    def _init_coll_bxsBtInfo_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbAddFlt, 0, border=4, flag=0)
        parent.AddWindow(self.cbDelFlt, 0, border=4, flag=wx.TOP)

    def _init_coll_bxsUsr_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblUsr, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viUsr, 2, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddSizer(self.bxsUsrBt, 2, border=0, flag=wx.EXPAND)

    def _init_coll_fgsDom_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_bxsInfo_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsUsr, 1, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsTag, 1, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsAttr, 1, border=4, flag=wx.TOP | wx.EXPAND)

    def _init_coll_bxsFltLbl_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblFilter, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddSizer(self.bxsFlt, 4, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbApplyFlt, 0, border=6, flag=wx.LEFT)

    def _init_coll_bxsAclSec_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsAcl, 2, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddSizer(self.bxsSecLv, 1, border=4, flag=wx.EXPAND)

    def _init_coll_fgsDom_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsHuman, 1, border=8, flag=wx.RIGHT | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.trlstAcl, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsAclSecLbl, 1, border=8,
              flag=wx.EXPAND | wx.ALIGN_CENTER | wx.TOP)
        parent.AddSizer(self.bxsInfoBtInfo, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsFltLbl, 1, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER)

    def _init_coll_bxsTag_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblTag, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.chcFilterPos, 2, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.lblNodeName, 2, border=4,
              flag=wx.LEFT | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsDom = wx.FlexGridSizer(cols=1, hgap=0, rows=10, vgap=0)

        self.bxsHuman = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsUsr = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsFlt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsTag = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsAttr = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsInfo = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsBtInfo = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsInfoBtInfo = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsAcl = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsSecLv = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsAclSec = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsUsrBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsFltLbl = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsAclSecLbl = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsDom_Items(self.fgsDom)
        self._init_coll_fgsDom_Growables(self.fgsDom)
        self._init_coll_bxsHuman_Items(self.bxsHuman)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsUsr_Items(self.bxsUsr)
        self._init_coll_bxsFlt_Items(self.bxsFlt)
        self._init_coll_bxsTag_Items(self.bxsTag)
        self._init_coll_bxsAttr_Items(self.bxsAttr)
        self._init_coll_bxsInfo_Items(self.bxsInfo)
        self._init_coll_bxsBtInfo_Items(self.bxsBtInfo)
        self._init_coll_bxsInfoBtInfo_Items(self.bxsInfoBtInfo)
        self._init_coll_bxsAcl_Items(self.bxsAcl)
        self._init_coll_bxsSecLv_Items(self.bxsSecLv)
        self._init_coll_bxsAclSec_Items(self.bxsAclSec)
        self._init_coll_bxsUsrBt_Items(self.bxsUsrBt)
        self._init_coll_bxsFltLbl_Items(self.bxsFltLbl)
        self._init_coll_bxsAclSecLbl_Items(self.bxsAclSecLbl)

        self.SetSizer(self.fgsDom)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTSECACLDOMPANEL,
              name=u'vtSecAclDomPanel', parent=prnt, pos=wx.Point(768, 248),
              size=wx.Size(405, 357), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(397, 330))

        self.lblHuman = wx.StaticText(id=wxID_VTSECACLDOMPANELLBLHUMAN,
              label=u'human', name=u'lblHuman', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(93, 21), style=wx.ALIGN_RIGHT)
        self.lblHuman.SetMinSize(wx.Size(-1, -1))

        self.chcHum = wx.Choice(choices=[], id=wxID_VTSECACLDOMPANELCHCHUM,
              name=u'chcHum', parent=self, pos=wx.Point(97, 0),
              size=wx.Size(190, 21), style=wx.CB_SORT)
        self.chcHum.SetMinSize(wx.Size(-1, -1))
        self.chcHum.Bind(wx.EVT_CHOICE, self.OnChcHumChoice,
              id=wxID_VTSECACLDOMPANELCHCHUM)

        self.ckcAct = wx.CheckBox(id=wxID_VTSECACLDOMPANELCKCACT,
              label=u'active', name=u'ckcAct', parent=self, pos=wx.Point(291,
              0), size=wx.Size(97, 21), style=0)
        self.ckcAct.SetValue(False)
        self.ckcAct.SetMinSize(wx.Size(-1, -1))
        self.ckcAct.Bind(wx.EVT_CHECKBOX, self.OnCkcActCheckbox,
              id=wxID_VTSECACLDOMPANELCKCACT)

        self.trlstAcl = wx.gizmos.TreeListCtrl(id=wxID_VTSECACLDOMPANELTRLSTACL,
              name=u'trlstAcl', parent=self, pos=wx.Point(0, 29),
              size=wx.Size(397, 81), style=wx.TR_HAS_BUTTONS)
        self.trlstAcl.Bind(wx.EVT_TREE_SEL_CHANGED,
              self.OnTrlstAclTreeSelChanged, id=wxID_VTSECACLDOMPANELTRLSTACL)

        self.tgAclRead = wx.lib.buttons.GenBitmapToggleButton(ID=wxID_VTSECACLDOMPANELTGACLREAD,
              bitmap=wx.EmptyBitmap(16, 16), name=u'tgAclRead', parent=self,
              pos=wx.Point(72, 118), size=wx.Size(30, 30), style=0)
        self.tgAclRead.SetToolTipString(u'read')

        self.tgAclWrite = wx.lib.buttons.GenBitmapToggleButton(ID=wxID_VTSECACLDOMPANELTGACLWRITE,
              bitmap=wx.EmptyBitmap(16, 16), name=u'tgAclWrite', parent=self,
              pos=wx.Point(106, 118), size=wx.Size(30, 30), style=0)
        self.tgAclWrite.SetToolTipString(u'write')

        self.tgAclAdd = wx.lib.buttons.GenBitmapToggleButton(ID=wxID_VTSECACLDOMPANELTGACLADD,
              bitmap=wx.EmptyBitmap(16, 16), name=u'tgAclAdd', parent=self,
              pos=wx.Point(140, 118), size=wx.Size(30, 30), style=0)
        self.tgAclAdd.SetToolTipString(u'add')

        self.tgAclDel = wx.lib.buttons.GenBitmapToggleButton(ID=wxID_VTSECACLDOMPANELTGACLDEL,
              bitmap=wx.EmptyBitmap(16, 16), name=u'tgAclDel', parent=self,
              pos=wx.Point(174, 118), size=wx.Size(30, 30), style=0)
        self.tgAclDel.SetToolTipString(u'del')

        self.tgAclMove = wx.lib.buttons.GenBitmapToggleButton(ID=wxID_VTSECACLDOMPANELTGACLMOVE,
              bitmap=wx.EmptyBitmap(16, 16), name=u'tgAclMove', parent=self,
              pos=wx.Point(208, 118), size=wx.Size(30, 30), style=0)
        self.tgAclMove.SetToolTipString(u'move')

        self.tgAclExec = wx.lib.buttons.GenBitmapToggleButton(ID=wxID_VTSECACLDOMPANELTGACLEXEC,
              bitmap=wx.EmptyBitmap(16, 16), name=u'tgAclExec', parent=self,
              pos=wx.Point(242, 118), size=wx.Size(30, 30), style=0)
        self.tgAclExec.SetToolTipString(u'exec')

        self.tgAclBuild = wx.lib.buttons.GenBitmapToggleButton(ID=wxID_VTSECACLDOMPANELTGACLBUILD,
              bitmap=wx.EmptyBitmap(16, 16), name=u'tgAclBuild', parent=self,
              pos=wx.Point(276, 118), size=wx.Size(30, 30), style=0)
        self.tgAclBuild.SetToolTipString(u'build')

        self.spSecLv = wx.SpinCtrl(id=wxID_VTSECACLDOMPANELSPSECLV, initial=0,
              max=100, min=-3, name=u'spSecLv', parent=self, pos=wx.Point(312,
              118), size=wx.Size(44, 30), style=wx.SP_ARROW_KEYS)
        self.spSecLv.SetMinSize(wx.Size(-1, -1))

        self.lblUsr = wx.StaticText(id=wxID_VTSECACLDOMPANELLBLUSR,
              label=u'user', name=u'lblUsr', parent=self, pos=wx.Point(0, 152),
              size=wx.Size(68, 30), style=wx.ALIGN_RIGHT)
        self.lblUsr.SetMinSize(wx.Size(-1, -1))

        self.cbUsrClr = wx.lib.buttons.GenBitmapButton(ID=wxID_VTSECACLDOMPANELCBUSRCLR,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbUsrClr', parent=self,
              pos=wx.Point(216, 152), size=wx.Size(31, 30), style=0)
        self.cbUsrClr.Bind(wx.EVT_BUTTON, self.OnCbUsrClrButton,
              id=wxID_VTSECACLDOMPANELCBUSRCLR)

        self.viUsr = vidarc.vApps.vHum.vXmlHumInputTree.vXmlHumInputTree(id=wxID_VTSECACLDOMPANELVIUSR,
              name=u'viUsr', parent=self, pos=wx.Point(72, 152),
              size=wx.Size(140, 30), style=0)

        self.chcFilterPos = wx.Choice(choices=[],
              id=wxID_VTSECACLDOMPANELCHCFILTERPOS, name=u'chcFilterPos',
              parent=self, pos=wx.Point(72, 186), size=wx.Size(140, 21),
              style=0)
        self.chcFilterPos.SetMinSize(wx.Size(-1, -1))
        self.chcFilterPos.Bind(wx.EVT_CHOICE, self.OnChcFilterPosChoice,
              id=wxID_VTSECACLDOMPANELCHCFILTERPOS)

        self.chcFilterAttr = wx.Choice(choices=[],
              id=wxID_VTSECACLDOMPANELCHCFILTERATTR, name=u'chcFilterAttr',
              parent=self, pos=wx.Point(72, 220), size=wx.Size(140, 21),
              style=0)
        self.chcFilterAttr.SetMinSize(wx.Size(-1, -1))
        self.chcFilterAttr.Bind(wx.EVT_CHOICE, self.OnChcFilterAttrChoice,
              id=wxID_VTSECACLDOMPANELCHCFILTERATTR)

        self.viFltStr = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterStringInput(id=wxID_VTSECACLDOMPANELVIFLTSTR,
              name=u'viFltStr', parent=self, pos=wx.Point(72, 254),
              size=wx.Size(34, 30), style=0)

        self.viFltInt = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterIntegerInput(id=wxID_VTSECACLDOMPANELVIFLTINT,
              name=u'viFltInt', parent=self, pos=wx.Point(106, 254),
              size=wx.Size(34, 30), style=0)
        self.viFltInt.Show(False)

        self.viFltLong = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterLongInput(id=wxID_VTSECACLDOMPANELVIFLTLONG,
              name=u'viFltLong', parent=self, pos=wx.Point(140, 254),
              size=wx.Size(34, 30), style=0)
        self.viFltLong.Show(False)

        self.viFltFloat = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterFloatInput(id=wxID_VTSECACLDOMPANELVIFLTFLOAT,
              name=u'viFltFloat', parent=self, pos=wx.Point(174, 254),
              size=wx.Size(34, 30), style=0)
        self.viFltFloat.Show(False)

        self.viFltDate = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterDateInput(id=wxID_VTSECACLDOMPANELVIFLTDATE,
              name=u'viFltDate', parent=self, pos=wx.Point(208, 254),
              size=wx.Size(34, 30), style=0)
        self.viFltDate.Show(False)

        self.viFltTime = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterTimeInput(id=wxID_VTSECACLDOMPANELVIFLTTIME,
              name=u'viFltTime', parent=self, pos=wx.Point(242, 254),
              size=wx.Size(34, 30), style=0)
        self.viFltTime.Show(False)

        self.viFltDateTime = vidarc.tool.xml.vtXmlFilterTypeInput.vtXmlFilterDateTimeInput(id=wxID_VTSECACLDOMPANELVIFLTDATETIME,
              name=u'viFltDateTime', parent=self, pos=wx.Point(276, 254),
              size=wx.Size(11, 30), style=0)
        self.viFltDateTime.Show(False)

        self.cbFltUp = wx.lib.buttons.GenBitmapButton(ID=wxID_VTSECACLDOMPANELCBFLTUP,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbFltUp', parent=self,
              pos=wx.Point(291, 254), size=wx.Size(31, 30), style=0)
        self.cbFltUp.Bind(wx.EVT_BUTTON, self.OnCbFltUpButton,
              id=wxID_VTSECACLDOMPANELCBFLTUP)

        self.cbFltDn = wx.lib.buttons.GenBitmapButton(ID=wxID_VTSECACLDOMPANELCBFLTDN,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbFltDn', parent=self,
              pos=wx.Point(326, 254), size=wx.Size(31, 30), style=0)
        self.cbFltDn.Bind(wx.EVT_BUTTON, self.OnCbFltDnButton,
              id=wxID_VTSECACLDOMPANELCBFLTDN)

        self.lblTag = wx.StaticText(id=wxID_VTSECACLDOMPANELLBLTAG,
              label=u'tagname', name=u'lblTag', parent=self, pos=wx.Point(0,
              186), size=wx.Size(68, 30), style=wx.ALIGN_RIGHT)
        self.lblTag.SetMinSize(wx.Size(-1, -1))

        self.lblNodeName = wx.StaticText(id=wxID_VTSECACLDOMPANELLBLNODENAME,
              label=u'', name=u'lblNodeName', parent=self, pos=wx.Point(220,
              186), size=wx.Size(140, 30), style=0)
        self.lblNodeName.SetMinSize(wx.Size(-1, -1))

        self.lblAttr = wx.StaticText(id=wxID_VTSECACLDOMPANELLBLATTR,
              label=u'attribute', name=u'lblAttr', parent=self, pos=wx.Point(0,
              220), size=wx.Size(68, 30), style=wx.ALIGN_RIGHT)
        self.lblAttr.SetMinSize(wx.Size(-1, -1))

        self.lblFilterAttrDesc = wx.StaticText(id=wxID_VTSECACLDOMPANELLBLFILTERATTRDESC,
              label=u'', name=u'lblFilterAttrDesc', parent=self,
              pos=wx.Point(220, 220), size=wx.Size(140, 30), style=0)
        self.lblFilterAttrDesc.SetMinSize(wx.Size(-1, -1))

        self.cbApplyFlt = wx.lib.buttons.GenBitmapButton(ID=wxID_VTSECACLDOMPANELCBAPPLYFLT,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbApplyFlt', parent=self,
              pos=wx.Point(366, 254), size=wx.Size(30, 30), style=0)
        self.cbApplyFlt.Bind(wx.EVT_BUTTON, self.OnCbApplyFltButton,
              id=wxID_VTSECACLDOMPANELCBAPPLYFLT)

        self.cbAddFlt = wx.lib.buttons.GenBitmapButton(ID=wxID_VTSECACLDOMPANELCBADDFLT,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbAddFlt', parent=self,
              pos=wx.Point(366, 152), size=wx.Size(31, 30), style=0)
        self.cbAddFlt.Bind(wx.EVT_BUTTON, self.OnCbAddFltButton,
              id=wxID_VTSECACLDOMPANELCBADDFLT)

        self.cbDelFlt = wx.lib.buttons.GenBitmapButton(ID=wxID_VTSECACLDOMPANELCBDELFLT,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbDelFlt', parent=self,
              pos=wx.Point(366, 186), size=wx.Size(31, 30), style=0)
        self.cbDelFlt.Bind(wx.EVT_BUTTON, self.OnCbDelFltButton,
              id=wxID_VTSECACLDOMPANELCBDELFLT)

        self.lblSecLv = wx.StaticText(id=wxID_VTSECACLDOMPANELLBLSECLV,
              label=u'security level', name=u'lblSecLv', parent=self,
              pos=wx.Point(264, 118), size=wx.Size(44, 30),
              style=wx.ALIGN_RIGHT)
        self.lblSecLv.SetMinSize(wx.Size(-1, -1))

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTSECACLDOMPANELCBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=_('Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(206, 296),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.SetMinSize(wx.Size(-1, -1))
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VTSECACLDOMPANELCBCANCEL)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTSECACLDOMPANELCBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=_('Apply'), name=u'cbApply',
              parent=self, pos=wx.Point(114, 296), size=wx.Size(76, 30),
              style=0)
        self.cbApply.SetMinSize(wx.Size(-1, -1))
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VTSECACLDOMPANELCBAPPLY)

        self.lblFilter = wx.StaticText(id=wxID_VTSECACLDOMPANELLBLFILTER,
              label=u'filter', name=u'lblFilter', parent=self, pos=wx.Point(0,
              254), size=wx.Size(68, 30), style=wx.ALIGN_RIGHT)
        self.lblFilter.SetMinSize(wx.Size(-1, -1))

        self.lblAcl = wx.StaticText(id=wxID_VTSECACLDOMPANELLBLACL,
              label=u'permission', name=u'lblAcl', parent=self, pos=wx.Point(0,
              118), size=wx.Size(68, 30), style=wx.ALIGN_RIGHT)
        self.lblAcl.SetMinSize(wx.Size(-1, -1))

        self.lblAclDummy = wx.StaticText(id=wxID_VTSECACLDOMPANELLBLACLDUMMY,
              label=u'', name=u'lblAclDummy', parent=self, pos=wx.Point(306,
              118), size=wx.Size(0, 30), style=0)

        self.cbApplyAcl = wx.lib.buttons.GenBitmapButton(ID=wxID_VTSECACLDOMPANELCBAPPLYACL,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbApplyAcl', parent=self,
              pos=wx.Point(365, 118), size=wx.Size(31, 30), style=0)
        self.cbApplyAcl.Bind(wx.EVT_BUTTON, self.OnCbApplyAclButton,
              id=wxID_VTSECACLDOMPANELCBAPPLYACL)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        vtXmlDomConsumer.__init__(self)
        self._init_ctrls(parent)
        
        self.docHum=vtXmlDomConsumer()
        self.cbApply.SetBitmapLabel(vtArt.getBitmap(vtArt.Apply))
        self.cbApplyAcl.SetBitmapLabel(vtArt.getBitmap(vtArt.Apply))
        self.cbApplyFlt.SetBitmapLabel(vtArt.getBitmap(vtArt.Apply))
        self.cbCancel.SetBitmapLabel(vtArt.getBitmap(vtArt.Cancel))
        self.cbAddFlt.SetBitmapLabel(vtArt.getBitmap(vtArt.Add))
        self.cbDelFlt.SetBitmapLabel(vtArt.getBitmap(vtArt.Del))
        self.cbUsrClr.SetBitmapLabel(vtArt.getBitmap(vtArt.Erase))
        self.cbFltUp.SetBitmapLabel(vtArt.getBitmap(vtArt.Up))
        self.cbFltDn.SetBitmapLabel(vtArt.getBitmap(vtArt.Down))
        
        self.lObjFilter=[self.chcFilterPos, self.chcFilterAttr, self.viFltStr,
                self.viFltInt, self.viFltLong, self.viFltFloat, self.viFltDate,
                self.viFltTime, self.viFltDateTime]
        self.lObjHum=[self.viUsr, self.cbUsrClr]
        
        self.SetSize(size)
        self.Move(pos)
        self.aclNode=vtSecXmlAclDom()
        self.tiRoot=None
        self.lTreeItems=[]
        self.dTreeItems={}
        self.dFilter={}
        self.humDocs=[]
        self.Clear()
        i=0
        for sCol,iWidth in [(_(u'tag'),180),(_(u'description'),80),
                    (_(u'R'),20),(_(u'W'),20),(_(u'A'),20),
                    (_(u'D'),20),(_(u'M'),20),(_(u'X'),20),(_(u'B'),20),
                    (_(u'sec'),30),(_(u'filter'),60),(_(u'op'),30)]:
            self.trlstAcl.AddColumn(sCol)
            self.trlstAcl.SetColumnWidth(i,iWidth)
            i+=1
        
        self.trlstAcl.SetMainColumn(0)
        self.SetupImageList()
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        try:
            del self.lTreeItems
            del self.dTreeItems
            del self.dFilter
            del self.humDocs
        except:
            #vtLog.vtLngTB(self.__class__.__name__)
            pass
    def SetupImageList(self):
        self.dImg={'elem':{},'attr':{},'grp':{}}
        self.lImg=wx.ImageList(16,16)
        
        imgNor=vtArt.getBitmap(vtArt.Invisible)
        imgSel=vtArt.getBitmap(vtArt.Invisible)
        self.dImg['dft']=[self.lImg.Add(imgNor),self.lImg.Add(imgSel)]
        
        imgNor=vtArt.getBitmap(vtArt.Safe)
        imgSel=vtArt.getBitmap(vtArt.Safe)
        self.dImg['elem']['root']=[self.lImg.Add(imgNor),self.lImg.Add(imgSel)]
        
        imgNor=vtArt.getBitmap(vtArt.Group)
        imgSel=vtArt.getBitmap(vtArt.Group)
        self.dImg['grp']['group']=[self.lImg.Add(imgNor),self.lImg.Add(imgSel)]
        
        imgNor=vtArt.getBitmap(vtArt.User)
        imgSel=vtArt.getBitmap(vtArt.User)
        self.dImg['grp']['user']=[self.lImg.Add(imgNor),self.lImg.Add(imgSel)]
        
        imgNor=vtArt.getBitmap(vtArt.Filter)
        imgSel=vtArt.getBitmap(vtArt.Filter)
        self.dImg['flt']=[self.lImg.Add(imgNor),self.lImg.Add(imgSel)]
        
        img=vtArt.getBitmap(vtArt.Report)
        self.tgAclBuild.SetBitmapLabel(img)
        self.tgAclBuild.SetBitmapSelected(img)
        self.dImg['attr'][vtSecXmlAclDom.ACL_BUILD]=self.lImg.Add(img)
        img=vtArt.getBitmap(vtArt.Build)
        self.tgAclExec.SetBitmapLabel(img)
        self.tgAclExec.SetBitmapSelected(img)
        self.dImg['attr'][vtSecXmlAclDom.ACL_EXEC]=self.lImg.Add(img)
        img=vtArt.getBitmap(vtArt.Browse)
        self.tgAclRead.SetBitmapLabel(img)
        self.tgAclRead.SetBitmapSelected(img)
        self.dImg['attr'][vtSecXmlAclDom.ACL_READ]=self.lImg.Add(img)
        img=vtArt.getBitmap(vtArt.Save)
        self.tgAclWrite.SetBitmapLabel(img)
        self.tgAclWrite.SetBitmapSelected(img)
        self.dImg['attr'][vtSecXmlAclDom.ACL_WRITE]=self.lImg.Add(img)
        img=vtArt.getBitmap(vtArt.Add)
        self.tgAclAdd.SetBitmapLabel(img)
        self.tgAclAdd.SetBitmapSelected(img)
        self.dImg['attr'][vtSecXmlAclDom.ACL_ADD]=self.lImg.Add(img)
        img=vtArt.getBitmap(vtArt.Del)
        self.tgAclDel.SetBitmapLabel(img)
        self.tgAclDel.SetBitmapSelected(img)
        self.dImg['attr'][vtSecXmlAclDom.ACL_DEL]=self.lImg.Add(img)
        img=vtArt.getBitmap(vtArt.Move)
        self.tgAclMove.SetBitmapLabel(img)
        self.tgAclMove.SetBitmapSelected(img)
        self.dImg['attr'][vtSecXmlAclDom.ACL_MOVE]=self.lImg.Add(img)
        img=vtArt.getBitmap(vtArt.Invisible)
        self.dImg['attr'][-1]=self.lImg.Add(img)
        
        imgNor=vtArt.getBitmap(vtArt.ID)
        imgSel=vtArt.getBitmap(vtArt.ID)
        self.dImg['elem']['secLv']=[self.lImg.Add(imgNor),self.lImg.Add(imgSel)]
        
        self.trlstAcl.SetImageList(self.lImg)
    def __markModified__(self,obj,flag=True):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'__markModified__;flag:%s'%(flag),
        #                origin=self.GetName())
        if flag:
            f=obj.GetFont()
            f.SetWeight(wx.FONTWEIGHT_BOLD)
            obj.SetFont(f)
        else:
            f=obj.GetFont()
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
            obj.SetFont(f)
        obj.Refresh()
    
    def OnCbApplyButton(self, event):
        try:
            if self.doc is not None:
                self.doc.acquire()
                self.GetNode()
                self.doc.CreateAcl()
                wx.PostEvent(self,vtSecAclDomPanelApplied(self))
        except:
            vtLog.vtLngTB(self.GetName())
        if self.doc is not None:
            self.doc.release()
        event.Skip()

    def OnCbCancelButton(self, event):
        self.SetNode(self.node)
        wx.PostEvent(self,vtSecAclDomPanelCanceled(self))
        event.Skip()
        
    def OnChcHumChoice(self, event):
        event.Skip()
        idx=self.chcHum.GetSelection()
        if idx<0:
            return
        n=self.chcHum.GetClientData(idx)
        try:
            i=self.lHum[idx][1]
        except:
            i=False
        self.ckcAct.SetValue(i)
        self.__setDocHumSelected__()
        self.__buildTreeAclInfo__(self.chcHum.GetStringSelection())
        
    def OnCkcActCheckbox(self, event):
        i=self.ckcAct.GetValue()
        idx=self.chcHum.GetSelection()
        if idx<0:
            return
        try:
            self.lHum[idx][1]=i
        except:
            pass
        self.aclLst.SetActive(self.chcHum.GetStringSelection(),i)
        self.__markModified__(self.chcHum,True)
        event.Skip()

    def Clear(self):
        vtXmlDomConsumer.Clear(self)
        self.chcHum.Clear()
        self.ckcAct.SetValue(False)
        self.lHum=[]
        self.dAcl={}
        self.iSelKind=-1
        self.selData=None
        self.selTi=None
        self.iUsrIdSecLv=-100
        self.__clearTreeAclInfo__()
    def __clearTreeAclInfo__(self):
        #vtLog.CallStack('')
        if self.dTreeItems is None:
            return
        try:
            for k in self.dTreeItems.keys():
                val=self.dTreeItems[k]
                #print val
                if type(val)==types.DictionaryType:
                    for kk in val.keys():
                        ti=val[kk]
                        self.trlstAcl.DeleteChildren(ti)
                else:
                    self.trlstAcl.DeleteChildren(val)
        except:
            vtLog.vtLngTB(self.GetName())
    def SetDoc(self,doc,bNet=False):
        self.tiRoot=None
        vtXmlDomConsumer.SetDoc(self,doc,bNet)
        self.viUsr.SetDoc(doc,bNet)
        self.__buildTree__()
        
        self.iSelKind=-1
        self.selData=None
        self.selTi=None
        
        self.dFilter={}
        self.cbAddFlt.Enable(False)
        self.cbDelFlt.Enable(False)
        try:
            if self.doc is not None:
                self.dFilter=self.doc.GetPossibleFilter()
                if self.dFilter is None:
                    return
                keys=self.dFilter.keys()
                for k in keys:
                    self.chcFilterPos.Append(k)
            self.chcFilterPos.SetSelection(0)
            self.OnChcFilterPosChoice(None)
        except:
            vtLog.vtLngTB(self.GetName())

    def __getImages__(self,kind,name):
        try:
            return self.dImg[kind][name]
        except:
            return self.dImg['dft']
    def __setTIImages__(self,kind,name,ti):
        img,imgSel=self.__getImages__(kind,name)
        self.trlstAcl.SetItemImage(ti,img,which = wx.TreeItemIcon_Normal)
        self.trlstAcl.SetItemImage(ti,imgSel,which = wx.TreeItemIcon_Expanded)
    def __setTIAttrImages__(self,iAttr,ti):
        if iAttr==vtSecXmlAclDom.ACL_READ:
            iCol=2
        elif iAttr==vtSecXmlAclDom.ACL_WRITE:
            iCol=3
        elif iAttr==vtSecXmlAclDom.ACL_ADD:
            iCol=4
        elif iAttr==vtSecXmlAclDom.ACL_DEL:
            iCol=5
        elif iAttr==vtSecXmlAclDom.ACL_MOVE:
            iCol=6
        elif iAttr==vtSecXmlAclDom.ACL_EXEC:
            iCol=7
        elif iAttr==vtSecXmlAclDom.ACL_BUILD:
            iCol=8
        else:
            return     
        img=self.__getImages__('attr',iAttr)
        self.trlstAcl.SetItemImage(ti,img,iCol)
    def __clearTIAttrImages__(self,iAttr,ti):
        if iAttr==vtSecXmlAclDom.ACL_READ:
            iCol=2
        elif iAttr==vtSecXmlAclDom.ACL_WRITE:
            iCol=3
        elif iAttr==vtSecXmlAclDom.ACL_ADD:
            iCol=4
        elif iAttr==vtSecXmlAclDom.ACL_DEL:
            iCol=5
        elif iAttr==vtSecXmlAclDom.ACL_MOVE:
            iCol=6
        elif iAttr==vtSecXmlAclDom.ACL_EXEC:
            iCol=7
        elif iAttr==vtSecXmlAclDom.ACL_BUILD:
            iCol=8
        else:
            return     
        img=self.__getImages__('attr',-1)
        self.trlstAcl.SetItemImage(ti,img,iCol)
    def __addFilter2Tree__(self,sFltName,sflt,flt,kAcl,iNumFlt,tic,tib=None):
        tid=wx.TreeItemData()
        tid.SetData((flt,kAcl,iNumFlt))
        if tib is None:
            tif=self.trlstAcl.AppendItem(tic,_(u'filter'),-1,-1,tid)
        else:
            if type(tib)==types.IntType:
                tif=self.trlstAcl.InsertItemBefore(tic,tib,_(u'filter'),-1,-1,tid)
            else:
                tif=self.trlstAcl.InsertItem(tic,tib,_(u'filter'),-1,-1,tid)
        bmpFlt=self.dImg['flt']
        self.trlstAcl.SetItemImage(tif,bmpFlt[0],which = wx.TreeItemIcon_Normal)
        self.trlstAcl.SetItemImage(tif,bmpFlt[1],which = wx.TreeItemIcon_Expanded)
        self.trlstAcl.SetItemText(tif,sFltName,1)
        self.trlstAcl.SetItemText(tif,sflt.GetValueStr(),self.FILTER_COL_START+1)
        self.trlstAcl.SetItemText(tif,sflt.GetOpStr(),self.FILTER_COL_START+2)
        return tif
    def __addEntryTup2Tree__(self,ti,kAcl,tup):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'kAcl:%s;tup:%s'%(kAcl,vtLog.pformat(tup)),self)
        if tup[1]<0:
            try:
                bmp=self.dImg['elem']['secLv']
            except:
                bmp=self.dImg['dft']
            sName=tup[2].getNameSecLv()
        else:
            try:
                bmp=self.dImg['grp'][kAcl]
            except:
                bmp=self.dImg['dft']
            sName=tup[0]
        
        tid=wx.TreeItemData()
        tid.SetData(tup[2])
        tic=self.trlstAcl.AppendItem(ti,sName,-1,-1,tid)
        self.trlstAcl.SetItemImage(tic,bmp[0],which = wx.TreeItemIcon_Normal)
        self.trlstAcl.SetItemImage(tic,bmp[1],which = wx.TreeItemIcon_Expanded)
        entry=tup[2]
        iAcl=entry.getAcl()
        for acl in self.doc.ACL_LIST:
            if self.doc.IsAclActive(iAcl,acl):
                self.__setTIAttrImages__(acl,tic)
        i=self.FILTER_COL_START
        self.trlstAcl.SetItemText(tic,str(entry.getSecLv()),i)
        flt=entry.getFilter()
        if flt is not None:
            iNumFlt=0
            for sFltName,sflt in flt.GetFilters():
                self.__addFilter2Tree__(sFltName,sflt,
                                    flt,kAcl,iNumFlt,tic)
                iNumFlt+=1
        return tic
    def __buildTree__(self):
        vtLog.vtLngCurWX(vtLog.INFO,'start',self)
        self.__enableObjLst__(self.lObjHum,False)
        self.__enableObjLst__(self.lObjFilter,False)
        self.cbApplyAcl.Enable(False)
        self.spSecLv.Enable(False)
        self.cbApplyFlt.Enable(False)
        self.cbAddFlt.Enable(False)
        self.cbDelFlt.Enable(False)
        self.cbFltUp.Enable(False)
        self.cbFltDn.Enable(False)
        self.__showAclNum__(-1)
        self.__enableAclNum__(0)
        
        self.iSelKind=-1
        self.selData=None
        self.selTi=None
        
        sHumName=self.chcHum.GetStringSelection()
        try:
            acl,bAct=self.aclLst.GetAcl(sHumName)
        except:
            acl,bAct={},False
        vtLog.vtLngCurWX(vtLog.DEBUG,'vHuman:%s;acl:%s;bAct:%d'%\
                (sHumName,vtLog.pformat(acl),bAct),self)
        self.lTreeItems=[]
        self.dTreeItems={}
        self.trlstAcl.DeleteAllItems()
        r=self.trlstAcl.AddRoot(_(u'ACL'))
        self.__setTIImages__('elem','root',r)
        if 1==0:
            self.__setTIAttrImages__(vtSecXmlAclDom.ACL_EXEC,r)
            self.__setTIAttrImages__(vtSecXmlAclDom.ACL_READ,r)
            self.__setTIAttrImages__(vtSecXmlAclDom.ACL_WRITE,r)
            self.__setTIAttrImages__(vtSecXmlAclDom.ACL_ADD,r)
            self.__setTIAttrImages__(vtSecXmlAclDom.ACL_DEL,r)
            self.__setTIAttrImages__(vtSecXmlAclDom.ACL_MOVE,r)
        try:
            tup=self.doc.GetPossibleAcl()
            if tup is None:
                vtLog.vtLngCurWX(vtLog.WARN,'no possible ACL',self)
                return
            dAcl,lst=tup
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'lst:%s;dAcl:%s;acl:%s'%\
                    (vtLog.pformat(lst),vtLog.pformat(dAcl),vtLog.pformat(acl)),self)
            self.dTreeItems=copy.deepcopy(dAcl)
            lst.sort()
            for k in lst:
                def addTreeItem(tp,sVal,bmp,iAcl):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'%s;%d'%(sVal,iAcl),self)
                    tid=wx.TreeItemData()
                    tid.SetData(iAcl)
                    ti=self.trlstAcl.AppendItem(tp,sVal,-1,-1,tid)
                    if bmp is not None:
                        self.trlstAcl.SetItemImage(ti,bmp[0],which = wx.TreeItemIcon_Normal)
                        self.trlstAcl.SetItemImage(ti,bmp[1],which = wx.TreeItemIcon_Expanded)
                    for acl in self.doc.ACL_LIST:
                        if self.doc.IsAclActive(iAcl,acl):
                            self.__setTIAttrImages__(acl,ti)
                    return ti
                def addTreeItemAcl(tp,aclNode):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                    if tp is None:
                        return
                    bAddDftGrpUsr=False
                    try:
                        keysAcl=aclNode.keys()
                        iSecLv=aclNode['__sec_level']
                        keysAcl.remove('__sec_level')
                        if '__acl' in aclNode:
                            keysAcl.remove('__acl')
                        self.trlstAcl.SetItemText(tp,str(iSecLv),self.FILTER_COL_START)
                        if len(keysAcl)==0:
                            bAddDftGrpUsr=True
                    except:
                        bAddDftGrpUsr=True
                    if bAddDftGrpUsr==True:
                        for kAcl in ['group','user']:
                            ti=self.trlstAcl.AppendItem(tp,kAcl,-1,-1,None)
                            try:
                                bmp=self.dImg['grp'][kAcl]
                            except:
                                bmp=self.dImg['dft']
                            self.trlstAcl.SetItemImage(ti,bmp[0],which = wx.TreeItemIcon_Normal)
                            self.trlstAcl.SetItemImage(ti,bmp[1],which = wx.TreeItemIcon_Expanded)
                        return
                    keysAcl.sort()
                    for kAcl in keysAcl:
                        d=aclNode[kAcl]
                        vtLog.vtLngCurWX(vtLog.DEBUG,'acl;%s;%s'%(kAcl,vtLog.pformat(d)),self)
                        ti=self.trlstAcl.AppendItem(tp,kAcl,-1,-1,None)
                        try:
                            bmp=self.dImg['grp'][kAcl]
                        except:
                            bmp=self.dImg['dft']
                        self.trlstAcl.SetItemImage(ti,bmp[0],which = wx.TreeItemIcon_Normal)
                        self.trlstAcl.SetItemImage(ti,bmp[1],which = wx.TreeItemIcon_Expanded)
                        lEntries=[]
                        #print d
                        for kEntry in d.keys():
                            if kEntry<0 and kEntry>-100:
                                vtLog.vtLngCurWX(vtLog.WARN,'FIXME',self)
                                continue
                            #id=kEntry[0]
                            if self.docHum.doc is None:
                                return
                            if kEntry>=0:
                                nodeHum=self.docHum.doc.getNodeByIdNum(kEntry)
                                #print nodeHum
                                if nodeHum is None:
                                    d[kEntry]=None
                                    continue
                                sHumUsr=self.docHum.doc.getNodeText(nodeHum,'name')
                            else:
                                sHumUsr=d[kEntry].getNameSecLv()
                            lEntries.append((sHumUsr,kEntry,d[kEntry]))
                        if self.VERBOSE:
                            vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%(vtLog.pformat(lEntries)),self)
                        #print lEntries
                        lEntries.sort()
                        for tup in lEntries:
                            self.__addEntryTup2Tree__(ti,kAcl,tup)
                if type(dAcl[k])==types.ListType:
                    lAcl=dAcl[k]
                    bmp=self.dImg['dft']
                    sVal,iAcl=lAcl[0]
                    ti=addTreeItem(r,k,bmp,iAcl)
                    try:
                        #print sHumName,k
                        aclNode=self.aclLst.GetAclTagName(sHumName,k)
                        addTreeItemAcl(ti,aclNode)
                    except:
                        vtLog.vtLngTB(self.GetName())
                    self.dTreeItems[k]={None:ti}
                    for sVal,iAcl in lAcl[1:]:
                        bmp=self.dImg['dft']
                        tic=addTreeItem(ti,sVal,bmp,iAcl)
                        self.dTreeItems[k][sVal]=tic
                else:
                    bmp=self.dImg['dft']
                    iAcl=dAcl[k]
                    try:
                        if bAct==True:
                            if k in acl:
                                dAclTmp=acl[k]
                                if '__acl' in dAclTmp:
                                    iAcl=dAclTmp['__acl']
                    except:
                        pass
                    tic=addTreeItem(r,k,bmp,iAcl)
                    self.dTreeItems[k]=tic
                    try:
                        aclNode=self.aclLst.GetAclTagName(sHumName,k)
                        try:
                            addTreeItemAcl(tic,aclNode)
                        except:
                            vtLog.vtLngTB(self.GetName())
                    except:
                        pass
        except:
            vtLog.vtLngTB(self.GetName())
        self.trlstAcl.Expand(r)
    def SetDocHums(self,humDocs,bNet=False):
        self.humDocs=(humDocs,bNet)
    def SetDocHum(self,doc,bNet=False):
        self.docHum.SetDoc(doc,bNet)
        self.viUsr.SetDocTree(doc,bNet)
    def SetNode(self,node):
        self.Clear()
        if self.doc is None:
            return
        if node is None:
            return
        if self.doc.getTagName(node)!='security_acl':
            vtLog.vtLngCurWX(vtLog.WARN,'node is none',self)
            return
        #self.chcHum.Enable(self.humDocs is None)
        #self.ckcAct.Enable(self.humDocs is None)
        vtXmlDomConsumer.SetNode(self,node)
        self.aclLst=vtSecXmlAclList(self.doc,node,verbose=VERBOSE)
        #vtLog.CallStack('')
        for c in self.doc.getChilds(node):
            sTag=self.doc.getTagName(c)
            if sTag=='__cache__':
                continue
            self.chcHum.Append(sTag,c)
            bFound=False
            if self.doc.getAttribute(c,'active')=='1':
                bFound=True
            self.lHum.append([sTag,bFound,c])
                
        self.lHum.sort()
        i=0
        for tup in self.lHum:
            if tup[1]:
                self.chcHum.SetSelection(i)
                self.ckcAct.SetValue(True)
                self.__setDocHumSelected__()
            i+=1
        self.__buildTree__()
        #self.__buildTreeAclInfo__(self.chcHum.GetStringSelection())
    def __setDocHumSelected__(self):
        sHum=self.chcHum.GetStringSelection()
        #vtLog.CallStack('')
        for tup in self.humDocs[0]:
            if tup[0]==sHum:
                self.SetDocHum(tup[1],tup[3])#self.humDocs[1])
                return
        self.SetDocHum(None,False)
    def __buildTreeAclInfo__(self,sHumName):
        #vtLog.CallStack('')
        self.__buildTree__()
        acl,bAct=self.aclLst.GetAcl(sHumName)
        pass
    def GetNode(self,node=None):
        if node is None:
            node=self.node
        if node is None:
            return
        #self.doc.acquire()
        try:
            c=self.doc.getChild(node,'__cache__')
            if c is not None:
                self.doc.deleteNode(c,node)
                #c.unlinkNode()
                #c.freeNode()
        except:
            vtLog.vtLngTB(self.GetName())
        #self.doc.release()
        self.aclLst.GetNode(node,self.doc)
        return
        iLen=self.chcHum.GetCount()
        for i in range(iLen):
            tup=self.lHum[i]
            n=tup[-1]
            if tup[1]:
                sVal='1'
            else:
                sVal='0'
            self.doc.setAttribute(n,'active',sVal)

    def OnChcFilterPosChoice(self, event):
        if event is not None:
            event.Skip()
        idx=self.chcFilterPos.GetSelection()
        sName=self.chcFilterPos.GetStringSelection()
        sTrans=self.doc.GetTranslation(sName,None)
        self.chcFilterAttr.Clear()
        if sTrans is None:
            return
        self.lblNodeName.SetLabel(sTrans)
        lst=self.dFilter[sName]
        if lst is None:
            return
        for tup in lst:
            self.chcFilterAttr.Append(tup[0])
        self.chcFilterAttr.SetSelection(0)
        self.OnChcFilterAttrChoice(None)
    def __setFilterValues__(self,flt):
        try:
            idx=self.chcFilterAttr.GetSelection()
            tup=self.dFilter[self.chcFilterPos.GetStringSelection()][idx]
            if tup[1]==vtXmlFilterType.FILTER_TYPE_STRING:
                self.viFltStr.SetFilter(flt)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_DATE:
                self.viFltDate.SetFilter(flt)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_TIME:
                self.viFltTime.SetFilter(flt)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_DATE_TIME:
                self.viFltDateTime.SetFilter(flt)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_INT:
                self.viFltInt.SetFilter(flt)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_LONG:
                self.viFltLong.SetFilter(flt)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_FLOAT:
                self.viFltFloat.SetFilter(flt)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnChcFilterAttrChoice(self, event):
        if event is not None:
            event.Skip()
        sTag=self.chcFilterPos.GetStringSelection()
        sName=self.chcFilterAttr.GetStringSelection()
        self.lblFilterAttrDesc.SetLabel(self.doc.GetTranslation(sTag,sName))
        try:
            idx=self.chcFilterAttr.GetSelection()
            tup=self.dFilter[self.chcFilterPos.GetStringSelection()][idx]
            self.viFltStr.Show(False)
            self.viFltDate.Show(False)
            self.viFltTime.Show(False)
            self.viFltDateTime.Show(False)
            self.viFltInt.Show(False)
            self.viFltLong.Show(False)
            self.viFltFloat.Show(False)
            self.viFltStr.Enable(False)
            self.viFltDate.Enable(False)
            self.viFltTime.Enable(False)
            self.viFltInt.Enable(False)
            self.viFltLong.Enable(False)
            self.viFltFloat.Enable(False)
            #self.cbAddFlt.Enable(False)
            if tup[1]==vtXmlFilterType.FILTER_TYPE_STRING:
                self.viFltStr.Show(True)
                self.viFltStr.Enable(True)
                self.cbAddFlt.Enable(True)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_DATE:
                self.viFltDate.Show(True)
                self.viFltDate.Enable(True)
                self.cbAddFlt.Enable(True)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_TIME:
                self.viFltTime.Show(True)
                self.viFltTime.Enable(True)
                self.cbAddFlt.Enable(True)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_DATE_TIME:
                self.viFltDateTime.Show(True)
                self.viFltDateTime.Enable(True)
                self.cbAddFlt.Enable(True)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_INT:
                self.viFltInt.Show(True)
                self.viFltInt.Enable(True)
                self.cbAddFlt.Enable(True)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_LONG:
                self.viFltLong.Show(True)
                self.viFltLong.Enable(True)
                self.cbAddFlt.Enable(True)
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_FLOAT:
                self.viFltFloat.Show(True)
                self.viFltFloat.Enable(True)
                self.cbAddFlt.Enable(True)
            else:
                self.viFltStr.Show(True)
                self.viFltStr.Enable(False)
                self.cbAddFlt.Enable(True)
            
        except:
            self.viFltStr.Show(True)
            self.viFltDate.Show(False)
            self.viFltTime.Show(False)
            self.viFltInt.Show(False)
            self.viFltLong.Show(False)
            self.viFltFloat.Show(False)
            self.viFltStr.Enable(False)
            self.viFltDate.Enable(False)
            self.viFltTime.Enable(False)
            self.viFltDateTime.Enable(False)
            self.viFltInt.Enable(False)
            self.viFltLong.Enable(False)
            self.viFltFloat.Enable(False)
            self.cbAddFlt.Enable(False)
            vtLog.vtLngTB(self.GetName())
        self.bxsFlt.Layout()

    def OnCbApplyAclButton(self, event):
        event.Skip()
        try:
            if self.iSelKind==0:            # acl entry
                iAcl=self.__getAclNum__()
                iSecLv=self.spSecLv.GetValue()
                self.selData.setAcl(iAcl)
                self.selData.setSecLv(iSecLv)
                oAcl=self.trlstAcl.GetPyData(self.selTi)
                if oAcl.getId()<=-100:
                    oAcl.setSecLv(iSecLv)
                    sName=oAcl.getNameSecLv()
                    self.trlstAcl.SetItemText(self.selTi,sName,0)
                self.trlstAcl.SetItemText(self.selTi,str(iSecLv),self.FILTER_COL_START)
                for acl in self.doc.ACL_LIST:
                    if self.doc.IsAclActive(iAcl,acl):
                        self.__setTIAttrImages__(acl,self.selTi)
                    else:
                        self.__clearTIAttrImages__(acl,self.selTi)
            elif self.iSelKind==2:          # tag
                sHumName=self.chcHum.GetStringSelection()
                kTag=self.__getNodeFromTI__(self.selTi)
                aclNode=self.aclLst.GetAclTagName(sHumName,kTag)
                iSecLv=self.spSecLv.GetValue()
                iAcl=self.__getAclNum__()
                aclNode['__sec_level']=iSecLv
                aclNode['__acl']=iAcl
                self.trlstAcl.SetItemText(self.selTi,str(iSecLv),self.FILTER_COL_START)
                for acl in self.doc.ACL_LIST:
                    if self.doc.IsAclActive(iAcl,acl):
                        self.__setTIAttrImages__(acl,self.selTi)
                    else:
                        self.__clearTIAttrImages__(acl,self.selTi)
                iAcl=self.__getAclNum__()
                self.trlstAcl.SetPyData(self.selTi,iAcl)
        except:
            vtLog.vtLngTB(self.GetName())

    def OnCbApplyFltButton(self, event):
        try:
            if self.iSelKind==0:            # acl entry
                iAcl=self.__getAclNum__()
                iSecLv=self.spSecLv.GetValue()
                self.selData.setAcl(iAcl)
                self.selData.setSecLv(iSecLv)
                self.trlstAcl.SetItemText(self.selTi,str(iSecLv),self.FILTER_COL_START)
                for acl in self.doc.ACL_LIST:
                    if self.doc.IsAclActive(iAcl,acl):
                        self.__setTIAttrImages__(acl,self.selTi)
                    else:
                        self.__clearTIAttrImages__(acl,self.selTi)
            elif self.iSelKind==1:          # filter
                sHumName=self.chcHum.GetStringSelection()
                sNode=self.__getNodeFromTI__(self.selTi)
                try:
                    acl,bAct=self.aclLst.GetAcl(sHumName)
                except:
                    acl,bAct={},False
                dAcl=acl[sNode]
            
                tp=self.trlstAcl.GetItemParent(self.selTi)
                entry=self.trlstAcl.GetPyData(tp)
                tpp=self.trlstAcl.GetItemParent(tp)
                sKind=self.trlstAcl.GetItemText(tpp)
                filter=entry.getFilter()
                filters=filter.GetFilters()
                idx=self.chcFilterAttr.GetSelection()
                sFltNode=self.chcFilterPos.GetStringSelection()
                sFltName=self.chcFilterAttr.GetStringSelection()
                tup=self.dFilter[sFltNode][idx]
                if tup[1]==vtXmlFilterType.FILTER_TYPE_STRING:
                    sflt=self.viFltStr.GetFilter()
                elif tup[1]==vtXmlFilterType.FILTER_TYPE_DATE:
                    sflt=self.viFltDate.GetFilter()
                elif tup[1]==vtXmlFilterType.FILTER_TYPE_TIME:
                    sflt=self.viFltTime.GetFilter()
                elif tup[1]==vtXmlFilterType.FILTER_TYPE_DATE_TIME:
                    sflt=self.viFltDateTime.GetFilter()
                elif tup[1]==vtXmlFilterType.FILTER_TYPE_INT:
                    sflt=self.viFltInt.GetFilter()
                elif tup[1]==vtXmlFilterType.FILTER_TYPE_LONG:
                    sflt=self.viFltLong.GetFilter()
                elif tup[1]==vtXmlFilterType.FILTER_TYPE_FLOAT:
                    sflt=self.viFltFloat.GetFilter()
                else:
                    sflt=None
                if sflt is not None:
                    iNumFlt=self.selData[2]
                    filter.UpdateFilter(iNumFlt,(sFltName,sflt))
                    tiNew=self.__addFilter2Tree__(sFltName,sflt,
                                filter,sKind,iNumFlt,tp,iNumFlt)
                    self.trlstAcl.Delete(self.selTi)
                    self.trlstAcl.SelectItem(tiNew)
                    self.trlstAcl.EnsureVisible(tiNew)
            elif self.iSelKind==2:          # tag
                vtLog.vtLngCurWX(vtLog.WARN,'you are not supposed to be here.',self)
                sHumName=self.chcHum.GetStringSelection()
                kTag=self.__getNodeFromTI__(self.selTi)
                aclNode=self.aclLst.GetAclTagName(sHumName,kTag)
                iSecLv=self.spSecLv.GetValue()
                aclNode['__sec_level']=iSecLv
                self.trlstAcl.SetItemText(self.selTi,str(iSecLv),self.FILTER_COL_START)
                iAcl=self.__getAclNum__()
                self.trlstAcl.SetPyData(iAcl)
                pass
        except:
            vtLog.vtLngTB(self.GetName())
        #self.selTi=None
        #self.iSelKind=-1
        #self.selData=None
        event.Skip()
    def __getTreeItemChildKeyAcl__(self,ti,kAcl):
        try:
            triChild=self.trlstAcl.GetFirstChild(ti)
            while triChild[0].IsOk():
                if self.trlstAcl.GetItemText(triChild[0])==kAcl:
                    return triChild[0]
                triChild=self.trlstAcl.GetNextChild(ti,triChild[1])
        except:
            vtLog.vtLngTB(self.GetName())
        return None
    def OnCbAddFltButton(self, event):
        event.Skip()
        if self.VERBOSE:
            vtLog.CallStack('')
        try:
            sHumName=self.chcHum.GetStringSelection()
            sNode=self.__getNodeFromTI__(self.selTi)
            if self.VERBOSE:
                print 'human:',sHumName
                print 'node:',sNode
            if self.iSelKind==2:          # tag
                sHum=self.viUsr.GetValue()
                idHum=self.viUsr.GetID()
                iAcl=self.selData
                iAcl=self.__getAclNum__()
                iSec=self.spSecLv.GetValue()
                if len(idHum)==0:
                    idHum=-1
                    idHum=self.iUsrIdSecLv
                    if iSec<0:
                        return
                    self.iUsrIdSecLv-=1
                    kAcl='user'
                else:
                    kAcl=self.viUsr.GetSelectedNodeTagName()
                entry=self.aclLst.AddAclTagNameHum(sHumName,sNode,kAcl,
                    long(idHum),sHum,iAcl,iSec)
                ti=self.__getTreeItemChildKeyAcl__(self.selTi,kAcl)
                tic=self.__addEntryTup2Tree__(ti,kAcl,(sHum,idHum,entry))
                self.trlstAcl.SelectItem(tic)
                self.trlstAcl.EnsureVisible(tic)
                return
            try:
                acl,bAct=self.aclLst.GetAcl(sHumName)
            except:
                acl,bAct={},False
            dAcl=acl[sNode]
            if self.iSelKind==0:            # acl entry
                tp=self.selTi
                entry=self.selData
                pass
            elif self.iSelKind==1:          # filter
                tp=self.trlstAcl.GetItemParent(self.selTi)
                entry=self.trlstAcl.GetPyData(tp)
            tpp=self.trlstAcl.GetItemParent(tp)
            sKind=self.trlstAcl.GetItemText(tpp)
            filter=entry.getFilter(True)
            filters=filter.GetFilters()
            idx=self.chcFilterAttr.GetSelection()
            sFltNode=self.chcFilterPos.GetStringSelection()
            sFltName=self.chcFilterAttr.GetStringSelection()
            tup=self.dFilter[sFltNode][idx]
            if tup[1]==vtXmlFilterType.FILTER_TYPE_STRING:
                sflt=self.viFltStr.GetFilter()
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_DATE:
                sflt=self.viFltDate.GetFilter()
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_TIME:
                sflt=self.viFltTime.GetFilter()
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_DATE_TIME:
                sflt=self.viFltDateTime.GetFilter()
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_INT:
                sflt=self.viFltInt.GetFilter()
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_LONG:
                sflt=self.viFltLong.GetFilter()
            elif tup[1]==vtXmlFilterType.FILTER_TYPE_FLOAT:
                sflt=self.viFltFloat.GetFilter()
            else:
                sflt=None
            if sflt is not None:
                filter.AppendFilter((sFltName,sflt))
                iNumFlt=len(filters)-1
                tiNew=self.__addFilter2Tree__(sFltName,sflt,
                            filter,sKind,iNumFlt,tp,None)
                self.trlstAcl.SelectItem(tiNew)
                self.trlstAcl.EnsureVisible(tiNew)
        except:
            vtLog.vtLngTB(self.GetName())
        
    def __getNodeFromTI__(self,ti):
        tid=self.trlstAcl.GetPyData(ti)
        if  type(tid)==types.IntType:
            return self.trlstAcl.GetItemText(ti)
        else:
            tp=self.trlstAcl.GetItemParent(ti)
            if tp is None:
                return None
            else:
                return self.__getNodeFromTI__(tp)
    def __getNodeAclFromTI__(self,ti):
        tid=self.trlstAcl.GetPyData(ti)
        if  type(tid)==types.IntType:
            return tid
        else:
            tp=self.trlstAcl.GetItemParent(ti)
            if tp is None:
                return None
            else:
                return self.__getNodeAclFromTI__(tp)
    def OnCbFltUpButton(self, event):
        event.Skip()
        if self.iSelKind!=1:          # not filter
            return 
        try:
            tp=self.trlstAcl.GetItemParent(self.selTi)
            triChild=self.trlstAcl.GetFirstChild(tp)
            tiBefore=None
            bMod=False
            while triChild[0].IsOk():
                if bMod:
                    tup=self.trlstAcl.GetPyData(triChild[0])
                    #self.trlstAcl.SetPyData(triChild[0],(tup[0],tup[1],tup[2]-1))
                else:
                    if triChild[0]==self.selTi:
                        bMod=True
                    else:
                        tiBefore=triChild[0]
                        
                triChild=self.trlstAcl.GetNextChild(tp,triChild[1])
            if tiBefore is None:
                return
            tup=self.trlstAcl.GetPyData(tiBefore)
            self.trlstAcl.SetPyData(tiBefore,(tup[0],tup[1],tup[2]+1))
            #flt=entry.getFilter()
            #if flt is not None:
            #print flt.GetFilters()[self.selData[2]]
            sFltName,sflt=self.selData[0].GetFilters()[self.selData[2]]
            #sFltName,sflt=flt.GetFilters()[self.selData[2]]
            self.selData[0].MoveFilter(self.selData[2],-1)
            iNumFlt=self.selData[2]-1
            tiNew=self.__addFilter2Tree__(sFltName,sflt,
                        self.selData[0],self.selData[1],iNumFlt,tp,iNumFlt)
            self.trlstAcl.Delete(self.selTi)
            self.trlstAcl.SelectItem(tiNew)
            self.trlstAcl.EnsureVisible(tiNew)
            
            if self.VERBOSE>1:
                vtLog.CallStack('')
                print 'filter'
                triChild=self.trlstAcl.GetFirstChild(tp)
                while triChild[0].IsOk():
                    print self.trlstAcl.GetPyData(triChild[0])
                    triChild=self.trlstAcl.GetNextChild(tp,triChild[1])
            
        except:
            vtLog.vtLngTB(self.GetName())

    def OnCbFltDnButton(self, event):
        event.Skip()
        if self.iSelKind!=1:          # not filter
            return 
        try:
            tp=self.trlstAcl.GetItemParent(self.selTi)
            triChild=self.trlstAcl.GetFirstChild(tp)
            tiAfter=None
            bMod=False
            while triChild[0].IsOk():
                if bMod:
                    tiAfter=triChild[0]
                    break
                else:
                    if triChild[0]==self.selTi:
                        bMod=True
                triChild=self.trlstAcl.GetNextChild(tp,triChild[1])
            if tiAfter is None:
                return
            tup=self.trlstAcl.GetPyData(tiAfter)
            self.trlstAcl.SetPyData(tiAfter,(tup[0],tup[1],tup[2]-1))
            #flt=entry.getFilter()
            #if flt is not None:
            #print flt.GetFilters()[self.selData[2]]
            sFltName,sflt=self.selData[0].GetFilters()[self.selData[2]]
            #sFltName,sflt=flt.GetFilters()[self.selData[2]]
            self.selData[0].MoveFilter(self.selData[2],+1)
            iNumFlt=self.selData[2]+1
            tiNew=self.__addFilter2Tree__(sFltName,sflt,
                        self.selData[0],self.selData[1],iNumFlt,tp,tiAfter)
            self.trlstAcl.Delete(self.selTi)
            self.trlstAcl.SelectItem(tiNew)
            self.trlstAcl.EnsureVisible(tiNew)
            
            if self.VERBOSE>1:
                vtLog.CallStack('')
                print 'filter'
                triChild=self.trlstAcl.GetFirstChild(tp)
                while triChild[0].IsOk():
                    print self.trlstAcl.GetPyData(triChild[0])
                    triChild=self.trlstAcl.GetNextChild(tp,triChild[1])
            
        except:
            vtLog.vtLngTB(self.GetName())

    def OnCbDelFltButton(self, event):
        try:
            if self.iSelKind==0:            # acl entry
                sHumName=self.chcHum.GetStringSelection()
                try:
                    acl,bAct=self.aclLst.GetAcl(sHumName)
                except:
                    acl,bAct={},False
                    
                tp=self.trlstAcl.GetItemParent(self.selTi)
                kind=self.trlstAcl.GetItemText(tp)
                kTag=self.__getNodeFromTI__(self.selTi)
                self.aclLst.DelAclTagNameKindEntry(sHumName,kTag,kind,self.selData)
                self.trlstAcl.Delete(self.selTi)
                #self.__buildTree__()
                pass
            elif self.iSelKind==1:          # filter
                self.selData[0].DelFilter(self.selData[2])
                bMod=False
                tp=self.trlstAcl.GetItemParent(self.selTi)
                triChild=self.trlstAcl.GetFirstChild(tp)
                while triChild[0].IsOk():
                    if bMod:
                        tup=self.trlstAcl.GetPyData(triChild[0])
                        self.trlstAcl.SetPyData(triChild[0],(tup[0],tup[1],tup[2]-1))
                    else:
                        if triChild[0]==self.selTi:
                            bMod=True
                    triChild=self.trlstAcl.GetNextChild(tp,triChild[1])
                self.trlstAcl.Delete(self.selTi)
                #self.__buildTree__()
            elif self.iSelKind==2:          # tag
                kTag=self.__getNodeFromTI__(self.selTi)
                aclNode=self.aclLst.GetAclTagName(sHumName,kTag)
        except:
            vtLog.vtLngTB(self.GetName())
        self.selTi=None
        self.iSelKind=-1
        self.selData=None
        self.cbDelFlt.Enable(False)
        event.Skip()
    def __getAclNum__(self):
        iAcl=0
        if self.tgAclRead.GetValue()==True:
            iAcl|=vtSecXmlAclDom.ACL_MSK_READ
        if self.tgAclWrite.GetValue()==True:
            iAcl|=vtSecXmlAclDom.ACL_MSK_WRITE
        if self.tgAclAdd.GetValue()==True:
            iAcl|=vtSecXmlAclDom.ACL_MSK_ADD
        if self.tgAclDel.GetValue()==True:
            iAcl|=vtSecXmlAclDom.ACL_MSK_DEL
        if self.tgAclMove.GetValue()==True:
            iAcl|=vtSecXmlAclDom.ACL_MSK_MOVE
        if self.tgAclExec.GetValue()==True:
            iAcl|=vtSecXmlAclDom.ACL_MSK_EXEC
        if self.tgAclBuild.GetValue()==True:
            iAcl|=vtSecXmlAclDom.ACL_MSK_BUILD
        return iAcl
    def __showAclNum__(self,iAcl):
        self.tgAclRead.SetValue(False)
        self.tgAclWrite.SetValue(False)
        self.tgAclAdd.SetValue(False)
        self.tgAclDel.SetValue(False)
        self.tgAclMove.SetValue(False)
        self.tgAclExec.SetValue(False)
        self.tgAclBuild.SetValue(False)
        if iAcl>0:
            if (iAcl & vtSecXmlAclDom.ACL_MSK_READ) !=0:
                self.tgAclRead.SetValue(True)
            if (iAcl & vtSecXmlAclDom.ACL_MSK_WRITE)!=0:
                self.tgAclWrite.SetValue(True)
            if (iAcl & vtSecXmlAclDom.ACL_MSK_ADD)  !=0:
                self.tgAclAdd.SetValue(True)
            if (iAcl & vtSecXmlAclDom.ACL_MSK_DEL)  !=0:
                self.tgAclDel.SetValue(True)
            if (iAcl & vtSecXmlAclDom.ACL_MSK_MOVE) !=0:
                self.tgAclMove.SetValue(True)
            if (iAcl & vtSecXmlAclDom.ACL_MSK_EXEC) !=0:
                self.tgAclExec.SetValue(True)
            if (iAcl & vtSecXmlAclDom.ACL_MSK_BUILD) !=0:
                self.tgAclBuild.SetValue(True)
    def __enableAclNum__(self,iAcl):
            if (iAcl & vtSecXmlAclDom.ACL_MSK_READ) !=0:
                self.tgAclRead.Enable(True)
            else:
                self.tgAclRead.Enable(False)
            if (iAcl & vtSecXmlAclDom.ACL_MSK_WRITE)!=0:
                self.tgAclWrite.Enable(True)
            else:
                self.tgAclWrite.Enable(False)
            if (iAcl & vtSecXmlAclDom.ACL_MSK_ADD)  !=0:
                self.tgAclAdd.Enable(True)
            else:
                self.tgAclAdd.Enable(False)
            if (iAcl & vtSecXmlAclDom.ACL_MSK_DEL)  !=0:
                self.tgAclDel.Enable(True)
            else:
                self.tgAclDel.Enable(False)
            if (iAcl & vtSecXmlAclDom.ACL_MSK_MOVE) !=0:
                self.tgAclMove.Enable(True)
            else:
                self.tgAclMove.Enable(False)
            if (iAcl & vtSecXmlAclDom.ACL_MSK_EXEC) !=0:
                self.tgAclExec.Enable(True)
            else:
                self.tgAclExec.Enable(False)
            if (iAcl & vtSecXmlAclDom.ACL_MSK_BUILD) !=0:
                self.tgAclBuild.Enable(True)
            else:
                self.tgAclBuild.Enable(False)
    def __showAclEntry__(self,entry):
        if entry is None:
            self.__showAclNum__(-1)
            return
        try:
            id=entry.getId()
            sName=entry.getName()
            self.spSecLv.SetValue(entry.getSecLv())
            iAcl=entry.getAcl()
            self.__showAclNum__(iAcl)
            self.viUsr.SetValueID(sName,id)
        except:
            self.tgAclRead.SetValue(False)
            self.tgAclWrite.SetValue(False)
            self.tgAclAdd.SetValue(False)
            self.tgAclDel.SetValue(False)
            self.tgAclMove.SetValue(False)
            self.tgAclExec.SetValue(False)
            self.tgAclBuild.SetValue(False)
            vtLog.vtLngTB(self.GetName())
    def __enableObjLst__(self,lst,flag):
        for o in lst:
            o.Enable(flag)
            o.Refresh()
    def OnTrlstAclTreeSelChanged(self, event):
        self.viUsr.Clear()
        ti=event.GetItem()
        tid=self.trlstAcl.GetPyData(ti)
        t=type(tid)
        if self.VERBOSE:
            vtLog.CallStack('')
            print t,tid
        if isinstance(tid,vtSecXmlAclEntry):
            self.__enableObjLst__(self.lObjFilter,True)
            self.__enableObjLst__(self.lObjHum,True)
            self.__showAclEntry__(tid)
            self.cbApplyAcl.Enable(True)
            self.spSecLv.Enable(True)
            self.cbApplyFlt.Enable(False)
            self.cbAddFlt.Enable(True)
            self.cbDelFlt.Enable(True)
            self.cbFltUp.Enable(False)
            self.cbFltDn.Enable(False)
            iAclPos=self.__getNodeAclFromTI__(ti)
            self.__enableAclNum__(iAclPos)
            self.iSelKind=0
            self.selData=tid
            self.selTi=ti
        elif t==types.TupleType:
            self.__enableObjLst__(self.lObjFilter,True)
            self.__enableObjLst__(self.lObjHum,False)
            self.__showAclNum__(0)
            self.__enableAclNum__(0)
            sFltName,flt=tid[0].GetFilter(tid[2])
            #self.chcFilterPos.SetStringSelection(tid[1])
            #self.OnChcFilterPosChoice(None)
            
            self.chcFilterAttr.SetStringSelection(sFltName)
            self.OnChcFilterAttrChoice(None)
            iAclPos=self.__getNodeAclFromTI__(ti)
            #self.__enableAclNum__(iAclPos)
            self.__setFilterValues__(flt)
            self.cbApplyAcl.Enable(False)
            self.spSecLv.Enable(False)
            self.cbApplyFlt.Enable(True)
            self.cbAddFlt.Enable(True)
            self.cbDelFlt.Enable(True)
            self.cbFltUp.Enable(True)
            self.cbFltDn.Enable(True)
            self.iSelKind=1
            self.selData=tid
            self.selTi=ti
        elif t==types.IntType:
            sTag=self.trlstAcl.GetItemText(ti)
            dAcl,lAcl=self.doc.GetPossibleAcl()
            self.__enableObjLst__(self.lObjHum,True)
            self.__enableObjLst__(self.lObjFilter,False)
            sHumName=self.chcHum.GetStringSelection()
            kTag=self.__getNodeFromTI__(ti)
            aclNode=self.aclLst.GetAclTagName(sHumName,kTag)
            try:
                iSecLv=aclNode['__sec_level']
            except:
                iSecLv=-1
            self.spSecLv.SetValue(iSecLv)
            self.__showAclNum__(tid)
            try:
                iAcl=dAcl[sTag]
                self.__enableAclNum__(iAcl)
            except:
                self.__enableAclNum__(0)
            lFlt=self.doc.GetPossibleFilterTagName(kTag)
            if lFlt is not None:
                wx.CallAfter(self.chcFilterPos.SetStringSelection,kTag)
            self.cbApplyAcl.Enable(True)
            self.spSecLv.Enable(True)
            self.cbApplyFlt.Enable(False)
            self.cbAddFlt.Enable(True)
            self.cbDelFlt.Enable(False)
            self.cbFltUp.Enable(False)
            self.cbFltDn.Enable(False)
            self.iSelKind=2
            self.selData=tid
            self.selTi=ti
        else:
            self.__enableObjLst__(self.lObjHum,False)
            self.__enableObjLst__(self.lObjFilter,False)
            self.cbApplyAcl.Enable(False)
            self.spSecLv.Enable(False)
            self.cbApplyFlt.Enable(False)
            self.cbAddFlt.Enable(False)
            self.cbDelFlt.Enable(False)
            self.cbFltUp.Enable(False)
            self.cbFltDn.Enable(False)
            self.__showAclNum__(-1)
            try:
                iAclPos=self.__getNodeAclFromTI__(ti)
            except:
                iAclPos=0
            self.__showAclNum__(iAclPos)
            self.__enableAclNum__(0)
            self.iSelKind=-1
            self.selData=None
            self.selTi=ti
        event.Skip()

    def OnCbUsrClrButton(self, event):
        self.viUsr.Clear()
        event.Skip()

    
