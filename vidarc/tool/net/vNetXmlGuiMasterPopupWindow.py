#Boa:PopupWindow:vNetXmlGuiMasterPopupWindow
#----------------------------------------------------------------------------
# Name:         vNetXmlGuiMasterPopupWindow.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vNetXmlGuiMasterPopupWindow.py,v 1.3 2007/08/06 22:54:45 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import wx.lib.scrolledpanel

from vidarc.tool.net.vNetXmlGuiMasterCloseDialog import vNetXmlGuiMasterCloseDialog
from vidarc.tool.net.vNetCfgDialog import vNetCfgDialog
from vidarc.tool.net.vNetXmlWxGui import *
from vidarc.tool.xml.vtXmlDom import *

import sys,traceback,threading

import img_gui
import images

def create(parent):
    return vNetXmlGuiMasterPopupWindow(parent)

[wxID_VNETXMLGUIMASTERPOPUPWINDOW, wxID_VNETXMLGUIMASTERPOPUPWINDOWCBCFG, 
 wxID_VNETXMLGUIMASTERPOPUPWINDOWCBCLOSE, 
 wxID_VNETXMLGUIMASTERPOPUPWINDOWCBCONN, 
 wxID_VNETXMLGUIMASTERPOPUPWINDOWCBOPEN, 
 wxID_VNETXMLGUIMASTERPOPUPWINDOWLSTAPPLCONN, 
 wxID_VNETXMLGUIMASTERPOPUPWINDOWLSTLOG, 
] = [wx.NewId() for _init_ctrls in range(7)]

import vidarc.tool.log.vtLog as vtLog

class vNetXmlGuiMasterPopupWindow(wx.PopupWindow):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.PopupWindow.__init__(self, flags=wx.SIMPLE_BORDER, parent=prnt)
        self.SetSize(wx.Size(450, 270))
        self.Move(wx.Point(348, 104))
        self.SetName(u'vNetXmlGuiMasterPopupWindow')

        self.lstApplConn = wx.ListView(id=wxID_VNETXMLGUIMASTERPOPUPWINDOWLSTAPPLCONN,
              name=u'lstApplConn', parent=self, pos=wx.Point(8, 8),
              size=wx.Size(350, 100), style=wx.LC_REPORT)
        self.lstApplConn.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstApplConnListItemSelected,
              id=wxID_VNETXMLGUIMASTERPOPUPWINDOWLSTAPPLCONN)
        self.lstApplConn.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstApplConnListItemDeselected,
              id=wxID_VNETXMLGUIMASTERPOPUPWINDOWLSTAPPLCONN)
        self.lstApplConn.Bind(wx.EVT_LEFT_DCLICK, self.OnLstApplConnLeftDclick)

        self.cbCfg = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VNETXMLGUIMASTERPOPUPWINDOWCBCFG,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Config', name=u'cbCfg',
              parent=self, pos=wx.Point(364, 8), size=wx.Size(76, 30), style=0)
        self.cbCfg.Bind(wx.EVT_BUTTON, self.OnCbCfgButton,
              id=wxID_VNETXMLGUIMASTERPOPUPWINDOWCBCFG)

        self.cbClose = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VNETXMLGUIMASTERPOPUPWINDOWCBCLOSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Close', name=u'cbClose',
              parent=self, pos=wx.Point(152, 232), size=wx.Size(76, 30),
              style=0)
        self.cbClose.Bind(wx.EVT_BUTTON, self.OnCbCloseButton,
              id=wxID_VNETXMLGUIMASTERPOPUPWINDOWCBCLOSE)

        self.cbConn = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VNETXMLGUIMASTERPOPUPWINDOWCBCONN,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Connect', name=u'cbConn',
              parent=self, pos=wx.Point(364, 40), size=wx.Size(76, 30),
              style=0)
        self.cbConn.Bind(wx.EVT_BUTTON, self.OnCbConnButton,
              id=wxID_VNETXMLGUIMASTERPOPUPWINDOWCBCONN)

        self.lstLog = wx.ListView(id=wxID_VNETXMLGUIMASTERPOPUPWINDOWLSTLOG,
              name=u'lstLog', parent=self, pos=wx.Point(8, 112),
              size=wx.Size(264, 112), style=wx.LC_REPORT)

        self.cbOpen = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VNETXMLGUIMASTERPOPUPWINDOWCBOPEN,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Open', name=u'cbOpen',
              parent=self, pos=wx.Point(364, 72), size=wx.Size(76, 30),
              style=0)
        self.cbOpen.Bind(wx.EVT_BUTTON, self.OnCbOpenButton,
              id=wxID_VNETXMLGUIMASTERPOPUPWINDOWCBOPEN)

    def __init__(self, parent,verbose=0):
        self._init_ctrls(parent)
        self.iFill=0
        self.pos=[0,0]
        self.verbose=verbose
        self.bBlockNotify=False
        
        self.idxSel=-1
        self.dt=wx.DateTime()
        
        self.dlgCfgSetup=None
        self.nodeSetup=None
        self.docSetup=None
        self.lstCfg=None
        #self.cfgNode=None
        self.appls=[]
        self.mainAppl=''
        self.dn=''
        self.sRecentAlias=''
        self.sRecentConn=''
        self.alias=''
        self.host=''
        self.port=-1
        self.usr=''
        self.passwd=''
        
        self.netMaster=parent
        self.bConnectAll=False
        
        self.pnGui=wx.lib.scrolledpanel.ScrolledPanel(self,id=wx.NewId(),
                pos=(280,112),size=(160,112),
                style = wx.TAB_TRAVERSAL|wx.SUNKEN_BORDER)
        #self.pnGuiSizer = wx.BoxSizer(wx.VERTICAL)
        self.pnGuiSizer = wx.FlexGridSizer(cols=3, vgap=4, hgap=4)
        self.pnGui.SetSizer(self.pnGuiSizer)
        self.pnGui.SetAutoLayout(1)

        #self.scwConn.Show(False)
        self.semNotify=threading.Lock()#threading.Semaphore()
        self.lstScheduled=[]
        
        self.netDocMaster=None
        self.lstNetAppl=[]
        self.dictNet={}
        self.dictLbl={}
        self.dictProcess={}
        self.dlgCfgSetup=None
        self.lstApplConn.InsertColumn(0,u'state',wx.LIST_FORMAT_LEFT,40)
        self.lstApplConn.InsertColumn(1,u'appl',wx.LIST_FORMAT_LEFT,60)
        self.lstApplConn.InsertColumn(2,u'alias',wx.LIST_FORMAT_LEFT,60)
        self.lstApplConn.InsertColumn(3,u'host',wx.LIST_FORMAT_LEFT,60)
        self.lstApplConn.InsertColumn(4,u'usr',wx.LIST_FORMAT_LEFT,60)
        self.lstApplConn.InsertColumn(5,u'fn',wx.LIST_FORMAT_LEFT,160)
        self.lstApplConn.InsertColumn(6,u'port',wx.LIST_FORMAT_RIGHT,60)
        self.SetupImageList()
        
        self.lstLog.InsertColumn(0,u'appl',wx.LIST_FORMAT_LEFT,80)
        self.lstLog.InsertColumn(1,u'msg',wx.LIST_FORMAT_LEFT,100)
        self.lstLog.InsertColumn(2,u'time',wx.LIST_FORMAT_LEFT,60)
        self.lstLog.InsertColumn(3,u'status',wx.LIST_FORMAT_LEFT,50)
        
        self.cbCfg.SetBitmapLabel(images.getSettingBitmap())
        self.cbClose.SetBitmapLabel(images.getCloseBitmap())
        self.cbOpen.SetBitmapLabel(images.getBrowseBitmap())
        self.cbConn.SetBitmapLabel(img_gui.getConnectBitmap())

    def SetupImageList(self):
        self.imgDict={}
        self.imgLstTyp=wx.ImageList(16,16)
        d={}
        d[vNetXmlWxGui.STOPPED]         =self.imgLstTyp.Add(img_gui.getStoppedBitmap())
        d[vNetXmlWxGui.CONNECT]         =self.imgLstTyp.Add(img_gui.getConnectBitmap())
        d[vNetXmlWxGui.CONNECTED]       =self.imgLstTyp.Add(img_gui.getConnectBitmap())
        d[vNetXmlWxGui.CONNECT_FLT]     =self.imgLstTyp.Add(img_gui.getFaultBitmap())
        d[vNetXmlWxGui.OPEN_START]      =self.imgLstTyp.Add(img_gui.getOpenBitmap())
        d[vNetXmlWxGui.OPEN_FLT]        =self.imgLstTyp.Add(img_gui.getFaultBitmap())
        d[vNetXmlWxGui.SYNCH_START]     =self.imgLstTyp.Add(img_gui.getSynchBitmap())
        d[vNetXmlWxGui.SYNCH_PROC]      =self.imgLstTyp.Add(img_gui.getSynchBitmap())
        d[vNetXmlWxGui.SYNCH_FIN]       =self.imgLstTyp.Add(img_gui.getRunningBitmap())
        d[vNetXmlWxGui.SYNCH_ABORT]     =self.imgLstTyp.Add(img_gui.getFaultBitmap())
        d[vNetXmlWxGui.LOGIN]           =self.imgLstTyp.Add(img_gui.getLoginBitmap())
        d[vNetXmlWxGui.LOGGEDIN]        =self.imgLstTyp.Add(img_gui.getRunningBitmap())
        d[vNetXmlWxGui.SERVED]          =self.imgLstTyp.Add(img_gui.getConnectBitmap())
        d[vNetXmlWxGui.SERVE_FLT]       =self.imgLstTyp.Add(img_gui.getFaultBitmap())
        d[vNetXmlWxGui.DISCONNECTED]    =self.imgLstTyp.Add(img_gui.getStoppedBitmap())
        d[-1]                           =self.imgLstTyp.Add(img_gui.getStoppedBitmap())  # FIXME
        
        self.imgDict['dft']=d
        
        self.lstApplConn.SetImageList(self.imgLstTyp,wx.IMAGE_LIST_SMALL)
    
    def SetCfg(self,doc,lst,appls,mainAppl):
        self.docSetup=doc
        self.lstCfg=lst
        self.dictDoc={}
        self.appls=appls
        self.mainAppl=mainAppl
        self.sRecentAlias=''
        self.sRecentConn=''
        if self.dlgCfgSetup is None:
            self.dlgCfgSetup=vNetCfgDialog(self)
            self.SetCfgNode(None)
        for k in self.dictNet.keys():
            self.dictNet[k].SetCfg(self.docSetup,self.lstCfg,self.appls,k,
                    self.mainAppl,self.dlgCfgSetup)
        #self.pnGui.SetSize((-1,self.pos[1]+32))
        self.pnGui.SetupScrolling()
    def __getCfgNode__(self):
        self.nodeSetup=self.docSetup.getRoot()
        return self.docSetup.getChildByLst(self.nodeSetup,self.lstCfg)
        
    def SetCfgNode(self,bConnect=True):
        idx=0
        self.nodeSetup=self.docSetup.getRoot()
        cfgNode=self.__getCfgNode__()
        dn=self.docSetup.getNodeText(cfgNode,'path')
        for name in self.lstNetAppl:
            netDoc=self.dictNet[name]
            recentNode=self.netMaster.__getRecentAlias__()
            recentAliasNode=self.netMaster.__getRecentConnection__(recentNode)
            recentHostNode=self.netMaster.__getApplHost__(recentAliasNode,name)
            if recentHostNode is not None:
                netDoc.GenAutoFN(recentHostNode,dn)
                self.lstApplConn.SetStringItem(idx,2,self.docSetup.getNodeText(recentHostNode,'alias'),-1)
                self.lstApplConn.SetStringItem(idx,3,self.docSetup.getNodeText(recentHostNode,'host'),-1)
                self.lstApplConn.SetStringItem(idx,4,self.docSetup.getNodeText(recentHostNode,'usr'),-1)
                self.lstApplConn.SetStringItem(idx,5,netDoc.GetFN(),-1)
                self.lstApplConn.SetStringItem(idx,6,self.docSetup.getNodeText(recentHostNode,'port'),-1)
            idx+=1
    def SetCfgDlg(self,dlgCfgSetup):
        self.dlgCfgSetup=dlgCfgSetup
    
    def GetCfgDlg(self,dlgCfgSetup):
        return self.dlgCfgSetup
    
    def GetNetControlParent(self):
        return self.scwConn
    def __getImgIndex__(self,name,status):
        try:
            return self.imgDict[name][status]
        except:
            try:
                return self.imgDict['dft'][status]
            except:
                return self.imgDict['dft'][-1]
    def __addLog__(self,name,msg,iStatus):
        try:
            sTime=self.dt.Now().FormatTime()
            idx=self.lstLog.InsertImageStringItem(0, name,-1)
            self.lstLog.SetStringItem(idx,1,msg,-1)
            self.lstLog.SetStringItem(idx,2,sTime,-1)
            self.lstLog.SetStringItem(idx,3,'0x%04x'%iStatus,-1)
            
            idx=self.lstNetAppl.index(name)
            netDoc=self.dictNet[name]
            self.lstApplConn.SetStringItem(idx,0,'',self.__getImgIndex__(name,netDoc.GetStatus(iStatus)))
        except:
            traceback.print_exc()
    def AddNetControl(self,ctrlClass,name,*args,**kwargs):
        if self.dictNet.has_key(name):
            return None
        #if kwargs.has_key('pos')==False:
        kwargs['pos']=self.pos
        if kwargs.has_key('size')==False:
            kwargs['size']=wx.Size(16,16)
        
        ctrl=ctrlClass(self.pnGui,name,*args,**kwargs)
        self.pnGuiSizer.Add(ctrl, flag=wx.ALIGN_LEFT | wx.ALIGN_CENTER_VERTICAL)
        ctrl.SetNetMaster(self.netMaster)
        lbl=wx.StaticText(id=wx.NewId(),
              label=name, name=u'lbl'+name, parent=self.pnGui, pos=wx.Point(20,
              self.pos[1]), size=wx.Size(60, 13), style=0)
        self.pnGuiSizer.Add(lbl, flag=wx.ALIGN_LEFT | wx.ALIGN_CENTER_VERTICAL)
        pb=wx.Gauge(self.pnGui, -1, 50, (90, self.pos[1]), 
                    (80, 16),wx.GA_HORIZONTAL|wx.GA_SMOOTH)
        self.pnGuiSizer.Add(pb, flag=wx.ALIGN_LEFT | wx.ALIGN_CENTER_VERTICAL)
        
        self.iFill+=1
        self.pos[1]+=20#size[1]+4
        self.lstNetAppl.append(name)
        self.dictNet[name]=ctrl
        self.dictLbl[name]=lbl
        self.dictProcess[name]=pb
        if self.iFill==1:
            self.netDocMaster=ctrl
        d=ctrl.GetListImgDict(self.imgLstTyp)
        if d is not None:
            self.imgDict[name]=d
        idx=self.lstApplConn.InsertImageStringItem(sys.maxint, '', 
                            self.__getImgIndex__(name,ctrl.STOPPED))
        self.lstApplConn.SetStringItem(idx,1,name,-1)
        
        recentNode=self.netMaster.__getRecentAlias__()
        recentAliasNode=self.netMaster.__getRecentConnection__(recentNode)
        recentHostNode=self.netMaster.__getApplHost__(recentAliasNode,name)
        if recentHostNode is not None:
            self.lstApplConn.SetStringItem(idx,2,self.docSetup.getNodeText(recentHostNode,'alias'),-1)
            self.lstApplConn.SetStringItem(idx,3,self.docSetup.getNodeText(recentHostNode,'host'),-1)
            self.lstApplConn.SetStringItem(idx,4,self.docSetup.getNodeText(recentHostNode,'usr'),-1)
            self.lstApplConn.SetStringItem(idx,6,self.docSetup.getNodeText(recentHostNode,'port'),-1)
        
        EVT_NET_XML_OPEN_START      (ctrl,self.OnOpenStart)
        EVT_NET_XML_OPEN_OK         (ctrl,self.OnOpenOk)
        EVT_NET_XML_OPEN_FAULT      (ctrl,self.OnOpenFault)
        EVT_NET_XML_SYNCH_START     (ctrl,self.OnSynchStart)
        EVT_NET_XML_SYNCH_PROC      (ctrl,self.OnSynchProc)
        EVT_NET_XML_SYNCH_FINISHED  (ctrl,self.OnSynchFin)
        EVT_NET_XML_SYNCH_ABORTED   (ctrl,self.OnSynchAbort)
        EVT_NET_XML_CONNECT         (ctrl,self.OnConnect)
        EVT_NET_XML_CONNECTED       (ctrl,self.OnConnected)
        EVT_NET_XML_CONNECT_FAULT   (ctrl,self.OnConnectFault)
        EVT_NET_XML_DISCONNECT      (ctrl,self.OnDisConnect)
        EVT_NET_XML_LOGIN           (ctrl,self.OnLogin)
        EVT_NET_XML_LOGGEDIN        (ctrl,self.OnLoggedin)
        
        #if len(self.lstNetAppl)==1:
        #    self.lstApplConn.SetSelection(0)
        return ctrl
    
    def __getNetDocIdxName__(self,netDoc):
        i=0
        for name in self.lstNetAppl:
            if self.dictNet[name]==netDoc:
                return i,name
            i+=1
        return -1,''
    def __updateStatusByNetDoc__(self,name,netDoc,iStatusPrev,iStatus,*args,**kwargs):
        #idx,name=self.__getNetDocIdxName__(netDoc)
        if self.semNotify.acquire(False):
            #vtLog.vtLogCallDepth(None,'scheduled:%s %04x %04x args:%s kwargs:%s'%(name,iStatusPrev,iStatus,repr(args),repr(kwargs)),verbose=1)
            self.lstScheduled.append((name,netDoc,iStatusPrev,iStatus,args,kwargs))
            return
            pass
        for tup in self.lstScheduled:
            self.__notify__(tup[0],tup[1],tup[2],tup[3],*tup[4],**tup[5])
        self.lstScheduled=[]
        self.__notify__(name,netDoc,iStatusPrev,iStatus,*args,**kwargs)
        self.semNotify.release()
        if 1==0:
            if idx<0:
                return
            if netDoc.GetStatus() in [netDoc.SYNCH_PROC]:
                return
            self.lstApplConn.SetStringItem(idx,0,'',self.__getImgIndex__(name,netDoc.GetStatus(iStatus)))
            if idx==0:
                if netDoc.IsStatusChanged():
                    self.netMaster.SetBitmap(netDoc.__getBitmapByStatus__(iStatus))
    def __notify__(self,name,netDoc,iStatusPrev,iStatus,*args,**kwargs):
        try:
            idx=self.lstNetAppl.index(name)
        except:
            print name,self.lstNetAppl
            print netDoc
            traceback.print_exc()
            return
        try:
            
            if idx==0:
                if netDoc.IsStatusChanged(iStatusPrev,iStatus):
                    self.netMaster.SetBitmap(netDoc.__getBitmapByStatus__(iStatus))
            if netDoc.GetStatus() not in [netDoc.SYNCH_PROC]:
                #self.__addLog__(name,netDoc.GetStatusStr(iStatus),'0x%04x'%netDoc.iStatus)
                wx.CallAfter(self.__addLog__,name,netDoc.GetStatusStr(iStatus),iStatus)
                
            #self.semNotify.release()
            
            pb=self.dictProcess[name]
            if netDoc.GetStatus(iStatus)==netDoc.CONNECT:
                try:
                    fn=netDoc.GetFN()
                    if fn is None:
                        fn='???'
                except:
                    fn='???'
                #self.lstApplConn.SetStringItem(idx,1,netDoc.appl,-1)
                self.lstApplConn.SetStringItem(idx,2,netDoc.alias,-1)
                self.lstApplConn.SetStringItem(idx,3,netDoc.host,-1)
                self.lstApplConn.SetStringItem(idx,4,netDoc.usr,-1)
                self.lstApplConn.SetStringItem(idx,5,fn,-1)
                self.lstApplConn.SetStringItem(idx,6,repr(netDoc.port),-1)
            elif netDoc.GetStatus(iStatus)==netDoc.SYNCH_START:
                pb.SetRange(100)
            elif netDoc.GetStatus(iStatus)==netDoc.SYNCH_PROC:
                try:
                    pb.SetRange(args[1])
                    pb.SetValue(args[0])
                except:
                    vtLog.vtLogCallDepth(None,'',verbose=1,callstack=True)
            elif netDoc.GetStatus(iStatus)==netDoc.SYNCH_FIN:
                pb.SetValue(0)
            if idx==0:
                try:
                    #if netDoc.GetStatus()!=netDoc.SYNCH_PROC:
                    #    print netDoc.GetStatusStr()
                    
                    if netDoc.GetStatus(iStatus)==netDoc.LOGGEDIN:
                        netDoc.GetConfig()
                    if netDoc.GetStatus(iStatus)==netDoc.CONFIG:
                        self.netMaster.__updateConfig__()
                except:
                    traceback.print_exc()
                #if netDoc.GetStatus()==netDoc.OPEN_OK:
                #    self.netMaster.__updateConfig__()
                
                #if netDoc.GetStatus()==netDoc.SYNCH_START:
                #    if netDoc.IsFlag(netDoc.OPENED):
                #        netDoc.SetFlag(netDoc.CFG_VALID,True)
                #    self.netMaster.AutoConnect2Srv()
                if self.bConnectAll:
                    if self.netDocMaster.IsActive():
                        if netDoc.IsFlag(netDoc.CFG_VALID):
                            self.netMaster.AutoConnect2Srv(-2)
                pass
            
            self.__check__()
        except:
            traceback.print_exc()

    def Notify(self,name,netDoc,iStatusPrev,iStatus,*args,**kwargs):
        if self.bBlockNotify:
            return
        if self.semNotify.acquire(False):
                #vtLog.vtLogCallDepth(None,'scheduled:%s %04x %04x args:%s kwargs:%s'%(name,iStatusPrev,iStatus,repr(args),repr(kwargs)),verbose=1)
                self.lstScheduled.append((name,netDoc,iStatusPrev,iStatus,args,kwargs))
                return
                pass
        if 1==1:
            for tup in self.lstScheduled:
                self.__notify__(tup[0],tup[1],tup[2],tup[3],*tup[4],**tup[5])
            self.lstScheduled=[]
            for tup in self.lstScheduled:
                self.__notify__(tup[0],tup[1],tup[2],tup[3],*tup[4],**tup[5])
        else:
            if len(self.lstScheduled)>0:
                tup=self.lstScheduled[0]
                #vtLog.vtLogCallDepth(None,'scheduled start:%s %04x %04x'%(tup[0],tup[2],tup[3]),verbose=1)
                self.lstScheduled=self.lstScheduled[1:]
                self.semNotify.release()
                self.Notify(tup[0],tup[1],tup[2],tup[3],*tup[4],**tup[5])
                return
        self.__notify__(name,netDoc,iStatusPrev,iStatus,*args,**kwargs)
        self.semNotify.release()
        
    def IsAllSettled(self):
        for name in self.lstNetAppl:
            netDoc=self.dictNet[name]
            if netDoc.IsSettled()==False:
                return False
        return True
    def IsDataAccessed(self):
        for name in self.lstNetAppl:
            netDoc=self.dictNet[name]
            if netDoc.IsDataAccessed()==True:
                return True
        return False
    def __check__(self):
        return
        bAllSettled=True
        bReleaseBlock=False
        i=0
        for name in self.lstNetAppl:
            netDoc=self.dictNet[name]
            vtLog.vtLogCallDepth(None,'full:0x%04x 0x%04x state:%s '%(netDoc.iStatusPrev,netDoc.iStatus,netDoc.GetStatusStr()),callstack=False,verbose=1)
            if netDoc.IsSettled()==False:
                bAllSettled=False
            else:
                if i==0:
                    bReleaseBlock=True
            if bReleaseBlock and i>0:
                #if self.verbose:
                #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'release block',callstack=False)
                netDoc.SetBlockNotify(False)
            i+=1
        if bAllSettled:
            self.bConnectAll=False
            #if self.verbose:
            #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'all settled',callstack=False)
            for name in self.lstNetAppl:
                netDoc=self.dictNet[name]
                if self.verbose:
                    vtLog.vtLngCallStack(self,vtLog.DEBUG,'state:%s full:'%(netDoc.GetStatusStr(),netDoc.iStatus),callstack=False)
                netDoc.SetBlockNotify(False)
            
    def OnOpenStart(self,evt):
        netDoc=evt.GetNetDoc()
        self.__updateStatusByNetDoc__(netDoc.appl,netDoc,
                    netDoc.iStatusPrev,netDoc.iStatus)
        evt.Skip()
        
    def OnOpenOk(self,evt):
        netDoc=evt.GetNetDoc()
        self.__updateStatusByNetDoc__(netDoc.appl,netDoc,
                    netDoc.iStatusPrev,netDoc.iStatus)
        evt.Skip()
        
    def OnSynchAbort(self,evt):
        netDoc=evt.GetNetDoc()
        self.__updateStatusByNetDoc__(netDoc.appl,netDoc,
                    netDoc.iStatusPrev,netDoc.iStatus)
        evt.Skip()
    
    def OnOpenFault(self,evt):
        netDoc=evt.GetNetDoc()
        self.__updateStatusByNetDoc__(netDoc.appl,netDoc,
                    netDoc.iStatusPrev,netDoc.iStatus)
        evt.Skip()
    
    def OnSynchStart(self,evt):
        netDoc=evt.GetNetDoc()
        self.__updateStatusByNetDoc__(netDoc.appl,netDoc,
                    netDoc.iStatusPrev,netDoc.iStatus,
                    evt.GetCount())
        #idx,name=self.__getNetDocIdxName__(netDoc)
        evt.Skip()
    
    def OnSynchProc(self,evt):
        netDoc=evt.GetNetDoc()
        self.__updateStatusByNetDoc__(netDoc.appl,netDoc,
                    netDoc.iStatusPrev,netDoc.iStatus,
                    evt.GetAct(),evt.GetCount())
        #idx,name=self.__getNetDocIdxName__(netDoc)
        evt.Skip()
    
    def OnSynchFin(self,evt):
        netDoc=evt.GetNetDoc()
        self.__updateStatusByNetDoc__(netDoc.appl,netDoc,
                    netDoc.iStatusPrev,netDoc.iStatus)
        #idx,name=self.__getNetDocIdxName__(netDoc)
        evt.Skip()
        
    def OnConnect(self,evt):
        netDoc=evt.GetNetDoc()
        self.__updateStatusByNetDoc__(netDoc.appl,netDoc,netDoc.iStatusPrev,netDoc.iStatus)
        #idx,name=self.__getNetDocIdxName__(netDoc)
        #if idx>=0:
        #    self.lstApplConn.SetStringItem(idx,1,netDoc.appl,-1)
        #    self.lstApplConn.SetStringItem(idx,1,netDoc.alias,-1)
            
        evt.Skip()
        
    def OnSynchAbort(self,evt):
        netDoc=evt.GetNetDoc()
        self.__updateStatusByNetDoc__(netDoc.appl,netDoc,netDoc.iStatusPrev,netDoc.iStatus)
        #idx,name=self.__getNetDocIdxName__(netDoc)
        evt.Skip()
    
    def OnConnected(self,evt):
        netDoc=evt.GetNetDoc()
        self.__updateStatusByNetDoc__(netDoc.appl,netDoc,netDoc.iStatusPrev,netDoc.iStatus)
        #idx,name=self.__getNetDocIdxName__(netDoc)
        evt.Skip()
    
    def OnConnectFault(self,evt):
        netDoc=evt.GetNetDoc()
        self.__updateStatusByNetDoc__(netDoc.appl,netDoc,netDoc.iStatusPrev,netDoc.iStatus)
        #idx,name=self.__getNetDocIdxName__(netDoc)
        evt.Skip()
    
    def OnDisConnect(self,evt):
        netDoc=evt.GetNetDoc()
        self.__updateStatusByNetDoc__(netDoc.appl,netDoc,netDoc.iStatusPrev,netDoc.iStatus)
        #idx,name=self.__getNetDocIdxName__(netDoc)
        evt.Skip()
    
    def OnLogin(self,evt):
        netDoc=evt.GetNetDoc()
        self.__updateStatusByNetDoc__(netDoc.appl,netDoc,netDoc.iStatusPrev,netDoc.iStatus)
        #idx,name=self.__getNetDocIdxName__(netDoc)
        evt.Skip()
    
    def OnLoggedin(self,evt):
        netDoc=evt.GetNetDoc()
        self.__updateStatusByNetDoc__(netDoc.appl,netDoc,netDoc.iStatusPrev,netDoc.iStatus)
        #idx,name=self.__getNetDocIdxName__(netDoc)
        evt.Skip()
    
    def GetDictNet(self):
        return self.dictNet
    
    def GetNetNames(self):
        return self.lstNetAppl
    
    def OnLstApplConnListItemSelected(self, event):
        self.idxSel=event.GetIndex()
        netDoc=self.dictNet[self.lstNetAppl[self.idxSel]]
        if netDoc.IsActive():
            self.cbConn.SetBitmapLabel(img_gui.getStoppedBitmap())
        else:
            self.cbConn.SetBitmapLabel(img_gui.getConnectBitmap())
        self.cbConn.Refresh()
        event.Skip()

    def OnLstApplConnListItemDeselected(self, event):
        self.idxSel=-1
        if self.IsAllDisconnected():
            self.cbConn.SetBitmapLabel(img_gui.getConnectBitmap())
        else:
            self.cbConn.SetBitmapLabel(img_gui.getStoppedBitmap())
        self.cbConn.Refresh()
        event.Skip()

    def OnCbCfgButton(self, event):
        if self.idxSel<0:
            return
        if self.dlgCfgSetup is None:
            return
        appl=self.lstNetAppl[self.idxSel]
        self.dlgCfgSetup.SelAppl(appl)
        self.dlgCfgSetup.SetNode(self.docSetup,self.lstCfg,self.appls,
                        appl,self.__getCfgNode__())
        self.dlgCfgSetup.Centre()                
        self.Show(False)
        ret=self.dlgCfgSetup.ShowModal()
        if ret>0:
            #self.netMaster.AutoConnect2Srv()
            #self.OnCbConnButton(None)
            self.docSetup.Save()
            self.Show(True)
            pass
        event.Skip()

    def OnCbCloseButton(self, event):
        self.Show(False)
        event.Skip()

    def IsAllDisconnected(self):
        for netDoc in self.dictNet.values():
            if netDoc.GetStatus() not in [netDoc.STOPPED,netDoc.DISCONNECTED]:
                return False
        return True
    def OnCbConnButton(self, event):
        #if self.bConnectAll:
        #    return
        if self.idxSel==-1:
            if self.IsAllSettled()==False:
                return
            if self.IsDataAccessed()==True:
                return
            if self.IsAllDisconnected():
                netDoc=self.dictNet[self.lstNetAppl[0]]
                netDoc.SetFlag(netDoc.CFG_VALID,False)
                #for netDoc in self.dictNet.values():
                    #if netDoc.IsFlag(netDoc.CONN_ACTIVE)==False:
                    #if netDoc.GetStatus() in [netDoc.STOPPED,netDoc.DISCONNECTED]:
                    #    netDoc.SetBlockNotify(True)
                self.bConnectAll=True
                self.netMaster.AutoConnect2Srv(self.idxSel)
                self.cbConn.SetBitmapLabel(img_gui.getStoppedBitmap())
            else:
                for netDoc in self.dictNet.values():
                    netDoc.DisConnect()
                self.cbConn.SetBitmapLabel(img_gui.getConnectBitmap())
                self.bConnectAll=False
            return
        else:
            if self.bConnectAll:
                return
            self.bConnectAll=False
            netDoc=self.dictNet[self.lstNetAppl[self.idxSel]]
            if netDoc.IsFlag(netDoc.CONN_ACTIVE)==False:
            #if netDoc.GetStatus() in [netDoc.STOPPED,netDoc.DISCONNECTED]:
            #    netDoc.SetBlockNotify(True)
                self.netMaster.AutoConnect2Srv(self.idxSel)
                self.cbConn.SetBitmapLabel(img_gui.getStoppedBitmap())
            else:
                netDoc.DisConnect()
                self.cbConn.SetBitmapLabel(img_gui.getConnectBitmap())
        #if self.IsAllDisconnected():
        #    self.cbConn.SetBitmapLabel(img_gui.getConnectBitmap())
        #else:
        #    self.cbConn.SetBitmapLabel(img_gui.getStoppedBitmap())
        if event is not None:
            event.Skip()

    def OnCbOpenButton(self, event):
        if self.idxSel!=-1:
            netDoc=self.dictNet[self.lstNetAppl[self.idxSel]]
            if netDoc.IsFlag(netDoc.CONN_ACTIVE)==False:
            #if (netDoc.GetStatus() in [netDoc.STOPPED,netDoc.DISCONNECTED]) and (self.IsDataAccessed()==False):
                #'open'
                self.Show(False)
                dlg = wx.FileDialog(self, "Open", ".", "", "XML files (*.xml)|*.xml|all files (*.*)|*.*", wx.OPEN)
                try:
                    cfgNode=self.__getCfgNode__()
                    dn=self.docSetup.getNodeText(cfgNode,'path')
                    recentNode=self.netMaster.__getRecentAlias__()
                    recentAliasNode=self.netMaster.__getRecentConnection__(recentNode)
                    recentHostNode=self.netMaster.__getApplHost__(recentAliasNode,netDoc.appl)
                    netDoc.GenAutoFN(recentHostNode,dn)
                    fn=netDoc.GetFN()
                    if fn is not None:
                        dlg.SetPath(fn)
                    if dlg.ShowModal() == wx.ID_OK:
                        filename = dlg.GetPath()
                        netDoc.Open(filename)
                        vtLog.vtLogCallDepth(None,filename,verbose=1)
                        self.lstApplConn.SetStringItem(self.idxSel,5,netDoc.GetFN(),-1)
                        
                finally:
                    dlg.Destroy()
                self.Show(True)
            else:
                sMsg=u'Connection still active! \n\nClose connection to use this feautre.'
                dlg=wx.MessageDialog(self,sMsg ,
                            u'vgaPrjDoc',
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg.Destroy()
                return
        
        event.Skip()

    def OnLstApplConnLeftDclick(self, event):
        self.OnCbConnButton(None)
        event.Skip()
    def Stop(self):
        self.Show(False)
        self.bBlockNotify=True
        dlg=vNetXmlGuiMasterCloseDialog(self)
        dlg.Centre()
        dlg.SetDictNet(self.dictNet)
        dlg.ShowModal()
        return
        for netDoc in self.dictNet.values():
            #netDoc.Stop()
            netDoc.DisConnect()
        while 1==1:
            for netDoc in self.dictNet.values():
                if netDoc.IsRunning():
                    continue
                if netDoc.IsConnActive():
                    continue
            break
        time.sleep(2)