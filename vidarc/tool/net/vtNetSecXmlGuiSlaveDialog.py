#Boa:Dialog:vtNetSecXmlGuiSlaveDialog
#----------------------------------------------------------------------------
# Name:         vtNetSecXmlGuiSlaveDialog.py
# Purpose:      
#               
# Author:       Walter Obweger
#
# Created:      20060705
# CVS-ID:       $Id: vtNetSecXmlGuiSlaveDialog.py,v 1.6 2007/07/29 09:56:50 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import vidarc.tool.net.vtNetSecXmlGuiSlavePanel

import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase

def create(parent):
    return vtNetSecXmlGuiSlaveDialog(parent)

[wxID_VTNETSECXMLGUISLAVEDIALOG, wxID_VTNETSECXMLGUISLAVEDIALOGCBCLOSE, 
 wxID_VTNETSECXMLGUISLAVEDIALOGPNSTATUS, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vtNetSecXmlGuiSlaveDialog(wx.Dialog):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.pnStatus, 1, border=4,
              flag=wx.BOTTOM | wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbClose, 0, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.RIGHT | wx.LEFT | wx.ALIGN_CENTER)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTNETSECXMLGUISLAVEDIALOG,
              name=u'vtNetSecXmlGuiSlaveDialog', parent=prnt, pos=wx.Point(176,
              176), size=wx.Size(400, 285),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE,
              title=u'vtNetSecXmlGuiSlave Dialog')
        self.SetClientSize(wx.Size(392, 258))

        self.pnStatus = vidarc.tool.net.vtNetSecXmlGuiSlavePanel.vtNetSecXmlGuiSlavePanel(id=wxID_VTNETSECXMLGUISLAVEDIALOGPNSTATUS,
              name=u'pnStatus', parent=self, pos=wx.Point(4, 4),
              size=wx.Size(384, 208), style=0)

        self.cbClose = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTNETSECXMLGUISLAVEDIALOGCBCLOSE,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Close'), name=u'cbClose',
              parent=self, pos=wx.Point(158, 226), size=wx.Size(76, 30),
              style=0)
        self.cbClose.SetMinSize(wx.Size(-1, -1))
        self.cbClose.Bind(wx.EVT_BUTTON, self.OnCbCloseButton,
              id=wxID_VTNETSECXMLGUISLAVEDIALOGCBCLOSE)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        self._init_ctrls(parent)
        
        self.cbClose.SetBitmapLabel(vtArt.getBitmap(vtArt.Cancel))
        self.fgsData.Fit(self)
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        pass
    def OnCbCloseButton(self, event):
        self.Show(False)
        event.Skip()
    def SetNetMaster(self,netMaster):
        self.pnStatus.SetNetMaster(netMaster)
    def AddStatus(self,name,iStatus):
        self.pnStatus.AddStatus(name,iStatus)
    def RegNetControl(self,ctrlClass,name,*args,**kwargs):
        return self.pnStatus.RegNetControl(ctrlClass,name,*args,**kwargs)
    def AddNetControl(self,name,alias):
        return self.pnStatus.AddNetControl(name,alias)
    def DelNetControl(self,name,alias):
        self.pnStatus.DelNetControl(name,alias)
    def __check__(self,appl,alias):
        self.pnStatus.__check__(appl,alias)
    def RegNetControl(self,ctrlClass,name,*args,**kwargs):
        self.pnStatus.RegNetControl(ctrlClass,name,*args,**kwargs)
    def GetRegNetControl(self):
        return self.pnStatus.GetRegNetControl()
    def GetSlaves(self,appl,alias):
        return self.pnStatus.GetSlaves(appl,alias)
    def IsModified(self):
        return self.pnStatus.IsModified()
    def Notify(self,appl,alias,netDoc,iStatusPrev,iStatus,*args,**kwargs):
        self.pnStatus.Notify(appl,alias,netDoc,iStatusPrev,iStatus,*args,**kwargs)
    def Save(self,bModified=False):
        self.pnStatus.Save(bModified)
    def ShutDown(self):
        self.pnStatus.ShutDown()
    def IsShutDown(self):
        return self.pnStatus.IsShutDown()
    def SetProcess(self,name,iStatusPrev,iStatus,iValue,iRange):
        self.pnStatus.SetProcess(name,iStatusPrev,iStatus,iValue,iRange)
    def IsModified(self):
        return self.pnStatus.IsModified()
    def Save(self,bModified=False):
        self.pnStatus.Save(bModified)
    def GetDictNet(self):
        return self.pnStatus.GetDictNet()
    def GetNetNames(self):
        return self.pnStatus.GetNetNames()
    def GetNetDoc(self,name,alias):
        return self.pnStatus.GetNetDoc(name,alias)
    def GetNetDocBySlaves(self,name,alias,d={}):
        return self.pnStatus.GetNetDocBySlaves(self,name,alias,d)
    def GetNetControlParent(self):
        return self.pnStatus.GetNetControlParent()
    #def GetNetDocMaster(self):
    #    return self.pnStatus.GetNetDocMaster()
    def Show(self,flag=True):
        #self.pnStatus.UpdateConnState()
        wx.Dialog.Show(self,flag)
    def ShutDown(self):
        self.pnStatus.ShutDown()

