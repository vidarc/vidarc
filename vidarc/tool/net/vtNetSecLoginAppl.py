#----------------------------------------------------------------------------
# Name:         vNetSecLoginAppl.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060413
# CVS-ID:       $Id: vtNetSecLoginAppl.py,v 1.3 2006/09/26 05:05:59 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.net.vtNetSecLoginHost import *
from vidarc.tool.time.vtTime import vtDateTime

class vtNetSecLoginAppl:
    def __init__(self,name,auto,recent='0',dt='',store=0,ident=1):
        self.aliases={}
        self.name=name
        self.auto=auto
        self.store=store
        self.ident=ident
        try:
            self.recent=int(recent)
        except:
            self.recent=0
        self.dt=vtDateTime()
        self.dtLocal=vtDateTime(True)
        try:
            if len(dt)!=0:
                self.dt.SetStr(dt)
        except:
            self.dt.Now()
    def GetName(self):
        return self.name
    def GetAuto(self):
        return self.auto
    def GetRecent(self):
        return self.recent
    def GetStoreStr(self):
        return str(int(self.store))
    def GetIdentStr(self):
        return str(int(self.ident))
    def IsStore(self):
        return self.store
    def IsIdent(self):
        return self.ident
    def SetStore(self,flag):
        self.store=flag
    def SetIdent(self,flag):
        self.ident=flag
    def SetRecent(self,recent):
        try:
            self.recent=int(recent)
        except:
            self.recent=0
    def GetRecentStr(self,fmt='%d'):
        return fmt%self.recent
    def GetDateTime(self):
        self.dtLocal.SetStr(self.dt.GetDateTimeStr())
        return self.dtLocal.GetDateTimeStr(' ')
    def GetDateTimeGM(self):
        return self.dt.GetDateTimeStr()
    def SetDateTime2Now(self):
        self.dt.Now()
    def GetAliases(self):
        return self.aliases
    def GetHostAliasStr(self,appl):
        #try:
            return self.aliases[appl].GetHostAliasStr()
        #except:
        #    return getHostAliasStr('',50000,'')
    def GetHostConnectionStr(self,appl):
        #try:
            return self.aliases[appl].GetHostConnectionStr()
        #except:
        #    return getHostAliasStr('',50000,'')
    def ClearAll(self,appls):
        for k in appls:
            self.Clear(k)
    def Clear(self,sAppl):
        self.aliases={}
        o=vtNetSecLoginHost('','',-1,[],'0','','50000',sAppl,'')
        self.aliases[sAppl]=o
    def AddAlias(self,k,alias):
        self.aliases[k]=alias
    def DelAlias(self,k):
        try:
            del self.aliases[k]
        except:
            pass
    def GetAlias(self,k):
        try:
            return self.aliases[k]
        except:
            return None
    def IsAppl2Cfg(self,k):
        o=self.GetAlias(k)
        return o.Is2Cfg()
