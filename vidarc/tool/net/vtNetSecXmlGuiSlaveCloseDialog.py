#Boa:Dialog:vtNetSecXmlGuiSlaveCloseDialog
#----------------------------------------------------------------------------
# Name:         vtNetSecXmlGuiSlaveCloseDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060705
# CVS-ID:       $Id: vtNetSecXmlGuiSlaveCloseDialog.py,v 1.10 2008/02/02 13:43:38 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

import time,types
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.vtThread import vtThread

def create(parent):
    return vtNetSecXmlGuiSlaveCloseDialog(parent)

[wxID_VTNETSECXMLGUISLAVECLOSEDIALOG, 
 wxID_VTNETSECXMLGUISLAVECLOSEDIALOGLBLMSG, 
 wxID_VTNETSECXMLGUISLAVECLOSEDIALOGLISTBOX1, 
 wxID_VTNETSECXMLGUISLAVECLOSEDIALOGPBPROCESS, 
] = [wx.NewId() for _init_ctrls in range(4)]

class thdStopMasterDialog(vtThread):
    #def __init__(self,dlg,verbose=0):
    #    self.verbose=verbose
    #    self.dlg=dlg
    #    self.running=False
    #    self.origin=vtLog.vtLngGetRelevantNamesWX(self.dlg)+':thdStopMasterDialog'
    def DoStop(self,dictNet,func,*args,**kwargs):
        if self.IsRunning():
            vtLog.vtLngCur(vtLog.CRITICAL,
                    'thread is already engaged',self.GetOrigin())
            return
        for d in dictNet.values():
            dAlias=d['alias']
            for t in dAlias.values():
                netDoc=t[0]
                try:
                    #netDoc.Stop()
                    netDoc.DisConnect()
                except:
                    vtLog.vtLngTB(self.GetOrigin())
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        vtLog.vtLngCur(vtLog.INFO,'starting ...',self.GetOrigin())
        self.Do(self.Run,dictNet,func,*args,**kwargs)
    def Run(self,dictNet,func,*args,**kwargs):
        vtLog.vtLngCur(vtLog.INFO,'',self.GetOrigin())
        try:
            lstPrev=[]
            lstAct=[]
            for d in dictNet.values():
                dAlias=d['alias']
                for t in dAlias.values():
                    netDoc=t[0]
                    tup=[netDoc.IsRunning(),netDoc.IsConnActive()]
                    lstPrev.append(tup)
            bSomeActive=True
            iTime=0.1
            while bSomeActive:
                bSomeActive=False
                i=0
                for d in dictNet.values():
                    dAlias=d['alias']
                    for t in dAlias.values():
                        netDoc=t[0]
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCur(vtLog.DEBUG,
                                    'status:%s'%(netDoc.GetStatusStr()),
                                    self.GetOrigin())
                        tup=[netDoc.IsRunning(),netDoc.IsConnActive()]
                        if tup[0]==False:
                            if netDoc.IsStopping():
                                bSomeActive=True
                            else:
                                if lstPrev[i][0]:
                                    self.Post('proc',netDoc.appl,0)
                                    # 071016:wro 
                                    #wx.MutexGuiEnter()
                                    try:
                                        netDoc.DelConsumer()
                                    except:
                                        pass
                                    try:
                                        netDoc.DelConsumerLang()
                                    except:
                                        pass
                                    #try:
                                    #    self.par.ThreadStopped(netDoc)
                                        #netDoc.Close()
                                    #except:
                                    #    pass
                                    #wx.MutexGuiLeave()
                                    lstPrev[i][0]=False
                        else:
                            bSomeActive=True
                        if tup[1]==False:
                            if lstPrev[i][1]:
                                vtLog.vtLngCur(vtLog.INFO,
                                        'appl:%s connection closed'%(netDoc.GetAppl()),
                                        self.GetOrigin())
                                self.Post('proc',netDoc.appl,1)
                                # 071016:wro 
                                #wx.MutexGuiEnter()
                                #try:
                                #    self.par.ConnectionClosed(netDoc)
                                #except:
                                #    pass
                                #wx.MutexGuiLeave()
                                lstPrev[i][1]=False
                        else:
                            bSomeActive=True
                        
                        i+=1
                time.sleep(0.1)
                iTime+=0.1
                if iTime>60.0:
                    break
                self.Post('proc',iTime,60.0)
                # 071016:wro 
                #wx.MutexGuiEnter()
                #try:
                #    self.par.Process(iTime)
                #except:
                #    pass
                #wx.MutexGuiLeave()
            vtLog.vtLngCur(vtLog.INFO,'clean up',self.GetOrigin())
            #time.sleep(1)
            self.Post('proc',-1,-1)
            # 071016:wro  wx.MutexGuiEnter()
            #try:
            #    for d in dictNet.values():
            #        dAlias=d['alias']
            #        for t in dAlias.values():
            #            netDoc=t[0]
            #            try:
            #                netDoc.ClearConsumer()
            #                netDoc.DelConsumer()
            #                netDoc.DelConsumerLang()
            #                netDoc.Close()
            #            except:
            #                vtLog.vtLngTB(self.GetOrigin())
            #except:
            #    pass
            # 071016:wro 
            #try:
            #    if func is not None:
            #        func(*args,**kwargs)
            #except:
            #    pass
            #wx.MutexGuiLeave()
        except:
            vtLog.vtLngTB(self.GetOrigin())

class vtNetSecXmlGuiSlaveCloseDialog(wx.Dialog):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblMsg, 1, border=4,
              flag=wx.RIGHT | wx.TOP | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.listBox1, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.pbProcess, 1, border=4,
              flag=wx.ALIGN_CENTER | wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.TOP)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTNETSECXMLGUISLAVECLOSEDIALOG,
              name=u'vtNetSecXmlGuiSlaveCloseDialog', parent=prnt,
              pos=wx.Point(304, 74), size=wx.Size(359, 174),
              style=wx.CAPTION,#wx.DEFAULT_DIALOG_STYLE,
              title=u'vNet Sec XML Slave Close Dialog')
        self.SetClientSize(wx.Size(351, 147))

        self.lblMsg = wx.StaticText(id=wxID_VTNETSECXMLGUISLAVECLOSEDIALOGLBLMSG,
              label=_(u'stopping thread and closing connections please wait ...'),
              name=u'lblMsg', parent=self, pos=wx.Point(4, 4), size=wx.Size(343,
              13), style=0)
        self.lblMsg.SetMinSize(wx.Size(-1, -1))

        self.listBox1 = wx.ListBox(choices=[],
              id=wxID_VTNETSECXMLGUISLAVECLOSEDIALOGLISTBOX1, name='listBox1',
              parent=self, pos=wx.Point(4, 21), size=wx.Size(343, 105),
              style=0)
        self.listBox1.SetMinSize(wx.Size(-1, -1))

        self.pbProcess = wx.Gauge(id=wxID_VTNETSECXMLGUISLAVECLOSEDIALOGPBPROCESS,
              name=u'pbProcess', parent=self, pos=wx.Point(49, 130), range=60,
              size=wx.Size(252, 13), style=wx.GA_HORIZONTAL | wx.GA_SMOOTH)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        self._init_ctrls(parent)
        self.dictNet={}
        self.bModal=True
        self.thdStop=thdStopMasterDialog(self,bPost=True)
        self.thdStop.BindEvents(funcProc=self.OnProc)
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        pass
    def SetDictNet(self,dict):
        self.dictNet=dict
    def ShowModal(self):
        vtLog.vtLngCurWX(vtLog.INFO,''%(),self)
        self.bModal=True
        self.thdStop.DoStop(self.dictNet,self.OnFin)
        wx.Dialog.ShowModal(self)
    def Show(self,bFlag):
        vtLog.vtLngCurWX(vtLog.INFO,''%(),self)
        self.bModal=False
        self.thdStop.DoStop(self.dictNet,self.OnFin)
        wx.Dialog.Show(self,bFlag)
    def OnProc(self,evt):
        #evt.Skip()
        if type(evt.GetAct())==types.FloatType:
            iVal=int((evt.GetAct()/evt.GetCount())*1000)
            self.Process(iVal)
        else:
            if evt.GetCount()==0:
                msg=evt.GetAct()+u' '+_(u'thread stopped')
            elif evt.GetCount()==1:
                msg=evt.GetAct()+u' '+_(u'connection closed')
            elif evt.GetCount()==-1 and evt.GetAct()==-1:
                self.thdStop.SetPostWid(None)
                self.Close()
                msg=None
            else:
                msg=None
            if msg is not None:
                self.listBox1.Insert(msg,0)
    def ThreadStopped(self,netDoc):
        msg=netDoc.appl+u' '+_(u'thread stopped')
        self.listBox1.Insert(msg,0)
        pass
    def ConnectionClosed(self,netDoc):
        msg=netDoc.appl+u' '+_(u'connection closed')
        self.listBox1.Insert(msg,0)
        pass
    def Process(self,iTime):
        self.pbProcess.SetValue(int(iTime))
    def OnFin(self):
        if self.bModal:
            wx.CallAfter(self.EndModal,1)
        else:
            #wx.Dialog.Show(self,False)
            wx.CallAfter(self.Close)
            #from vidarc.tool.net.vtNetSecXmlGuiSlaveMultiple import vtnxSecXmlSlaveMultipleShutdown
            #wx.PostEvent(self.GetParent(),vtnxSecXmlSlaveMultipleShutdown(self.GetParent()))
