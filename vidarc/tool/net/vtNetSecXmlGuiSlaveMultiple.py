#----------------------------------------------------------------------------
# Name:         vtNetSecXmlGuiSlaveMultiple.py
# Purpose:      
#               
# Author:       Walter Obweger
#
# Created:      20060705
# CVS-ID:       $Id: vtNetSecXmlGuiSlaveMultiple.py,v 1.19 2008/02/02 13:43:38 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.buttons import GenBitmapButton

from vidarc.tool.net.vtNetSecXmlGuiSlaveDialog import vtNetSecXmlGuiSlaveDialog
from vidarc.tool.net.vtNetSecXmlGuiSlaveBrowseDialog import vtNetSecXmlGuiSlaveBrowseDialog
from vidarc.tool.net.vtNetSecXmlGuiSlaveCloseDialog import vtNetSecXmlGuiSlaveCloseDialog
from vidarc.tool.net.vtNetXmlSynchResolveDialog import vtNetXmlSynchResolveDialog
from vidarc.tool.net.vNetLoginDialog import vNetLoginDialog
from vidarc.tool.net.vNetLoginDialog import EVT_VTNET_LOGIN_DLG_FINISHED
from vidarc.tool.net.vtNetSecLoginHost import vtNetSecLoginHost
from vidarc.tool.net.vtNetSecLoginHost import getApplHostConnectionStr

from vidarc.tool.net.vNetXmlWxGui import *

from vidarc.tool.time.vtTime import vtDateTime
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.config.vcCust as vcCust
import vidarc.tool.gui.vtgCore as vtgCore
from vidarc.tool.vtThread import vtThread
from vidarc.tool.vtStateFlag import vtStateFlag
from vidarc.tool.vtThread import vtThreadWX
import vidarc.tool.net.vtNetArt as vtNetArt

import string,os.path,sys,Queue,threading,time

#import vidarc.tool.net.img_gui as img_gui
import vidarc.tool.net.images as img_img

vtnxEVT_NET_SEC_XML_SLAVE_MULTIPLE_START=wx.NewEventType()
def EVT_NET_SEC_XML_SLAVE_MULTIPLE_START(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_SEC_XML_SLAVE_MULTIPLE_START,func)
def EVT_NET_SEC_XML_SLAVE_MULTIPLE_START_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_SEC_XML_SLAVE_MULTIPLE_START,func)
class vtnxSecXmlSlaveMultipleStart(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_SEC_XML_SLAVE_MULTIPLE_START(<widget_name>, xxx)
    """
    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtnxEVT_NET_SEC_XML_SLAVE_MULTIPLE_START)

vtnxEVT_NET_SEC_XML_SLAVE_MULTIPLE_FINISHED=wx.NewEventType()
def EVT_NET_SEC_XML_SLAVE_MULTIPLE_FINISHED(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_SEC_XML_SLAVE_MULTIPLE_FINISHED,func)
def EVT_NET_SEC_XML_SLAVE_MULTIPLE_FINISHED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_SEC_XML_SLAVE_MULTIPLE_FINISHED,func)
class vtnxSecXmlSlaveMultipleFinished(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_SEC_XML_SLAVE_MULTIPLE_FINISHED(<widget_name>, xxx)
    """
    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtnxEVT_NET_SEC_XML_SLAVE_MULTIPLE_FINISHED)

vtnxEVT_NET_SEC_XML_SLAVE_MULTIPLE_SHUTDOWN=wx.NewEventType()
def EVT_NET_SEC_XML_SLAVE_MULTIPLE_SHUTDOWN(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_SEC_XML_SLAVE_MULTIPLE_SHUTDOWN,func)
def EVT_NET_SEC_XML_SLAVE_MULTIPLE_SHUTDOWN_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_SEC_XML_SLAVE_MULTIPLE_SHUTDOWN,func)
class vtnxSecXmlSlaveMultipleShutdown(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_SEC_XML_SLAVE_MULTIPLE_SHUTDOWN(<widget_name>, xxx)
    """
    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtnxEVT_NET_SEC_XML_SLAVE_MULTIPLE_SHUTDOWN)

vtnxEVT_NET_SEC_XML_SLAVE_MULTIPLE_ALIAS_CHANGED=wx.NewEventType()
def EVT_NET_SEC_XML_SLAVE_MULTIPLE_ALIAS_CHANGED(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_SEC_XML_SLAVE_MULTIPLE_ALIAS_CHANGED,func)
def EVT_NET_SEC_XML_SLAVE_MULTIPLE_ALIAS_CHANGED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_SEC_XML_SLAVE_MULTIPLE_ALIAS_CHANGED)
class vtnxSecXmlSlaveMultipleAliasChanged(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_SEC_XML_SLAVE_MULTIPLE_ALIAS_CHANGED(<widget_name>, xxx)
    """
    def __init__(self,obj,appl,alias,host):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtnxEVT_NET_SEC_XML_SLAVE_MULTIPLE_ALIAS_CHANGED)
        self.alias=alias
        self.host=host
        self.appl=appl
    def GetAppl(self):
        return self.appl
    def GetAlias(self):
        return self.alias
    def GetHost(self):
        return self.host

vtnxEVT_NET_SEC_XML_SLAVE_MULTIPLE_NOTIFY=wx.NewEventType()
def EVT_NET_SEC_XML_SLAVE_MULTIPLE_NOTIFY(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_SEC_XML_SLAVE_MULTIPLE_NOTIFY,func)
def EVT_NET_SEC_XML_SLAVE_MULTIPLE_NOTIFY_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_SEC_XML_SLAVE_MULTIPLE_NOTIFY)
class vtnxSecXmlSlaveMultipleNotify(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_SEC_XML_SLAVE_MULTIPLE_NOTIFY(<widget_name>, xxx)
    """
    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtnxEVT_NET_SEC_XML_SLAVE_MULTIPLE_NOTIFY)

class thdNetXmlStart(vtThread):
    def Start(self):
        #thread.start_new_thread(self.Run, ())
        self.Do(self.Run)
    def Run(self):
        try:
            speed=2
            iCount=len(self.par.lNetAppl)
            for idx in range(0,iCount):
                name=self.par.lNetAppl[idx]
                netDoc=self.par.dNetDoc[name]
                iTime=0.0
                
                while 1==1:
                    if speed==0:
                        if netDoc.GetStatus() in [netDoc.SYNCH_FIN,netDoc.SYNCH_ABORT]:
                            break
                    elif speed==1:
                        #if self.par.netMaster.IsFlag(netDoc.CFG_VALID):
                        if netDoc.IsFlag(netDoc.OPENED):
                            break
                    else:
                        #if self.par.netMaster.IsFlag(self.par.netMaster.OPENED):
                            break
                    time.sleep(0.1)
                    iTime+=0.1
                    if iTime>10:
                        if netDoc.GetStatus() in [netDoc.DISCONNECTED,netDoc.STOPPED,netDoc.CONNECT_FLT]:
                            break
                self.par.AutoConnect2Srv(idx+1)
        except:
            vtLog.vtLngTB(self.GetOrigin())
class thdNetXmlStop(vtThread):
    def Start(self):
        #thread.start_new_thread(self.Run, ())
        self.Do(self.Run)
    def Run(self):
        speed=1
        #self.par.thdLog.Stop()
        #while self.par.thdLog.IsRunning():
        #    time.sleep(1.0)
        iCount=len(self.par.lstNetAppl)
        for idx in range(0,iCount):
            name=self.par.lNetAppl[idx]
            netDoc=self.par.dNetDoc[name]
            netDoc.DisConnect()
            if speed==0:
                while netDoc.GetStatus() != netDoc.STOPPED:
                    time.sleep(0.1)
            elif speed==1:
                time.sleep(1.0)
            else:
                pass

#class vtNetSecXmlGuiSlaveMultiple(wx.BitmapButton):
class vtNetSecXmlGuiSlaveMultiple(GenBitmapButton):
    UPDATE_TIME = 500
    VERBOSE=0
    RECENT_SUB_MENU=16
    RECENT=8
    VERBOSE=1
    SEM_BLOCKING=True
    USE_TIMER4QUEUE_CHECK=False
    
    DISCONNECTED    =0x00000000
    #CONNECT         =0x00000001
    SHUTDOWN        =0x00008000
    SHUTDOWN_FIN    =0x00080000
    STATE_MASK      =0x000000FF
    MASK            =0x00FFFFFF
    
    def __init__(self,id=-1,parent=None,appl='',pos=(0,0),size=(16,12),name='netSlaveMultiple',style=0,
                    verbose=0):
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        #wx.BitmapButton.__init__(self,id=id,parent=parent,pos=pos,size=size,
        #        bitmap=wx.EmptyBitmap(16, 12),name='vtNetSecXmlGuiSlaveMultiple',
        #        style=wx.SUNKEN_BORDER | wx.SIMPLE_BORDER | wx.NO_3D | wx.NO_BORDER)
        self.imgDft=vtNetArt.getBitmap(vtNetArt.StoppedMed,sz=(16,12))
        GenBitmapButton.__init__(self,parent=parent,pos=pos,size=size,
                bitmap=self.imgDft,name='vtNetSecXmlGuiSlaveMultiple',
                style=0)
        self.semLogin=threading.Lock()#threading.Semaphore()
        
        widDlgPar=vtgCore.vtGuiCoreGetBaseWidget(self)
        self.widLogging=vtLog.GetPrintMsgWid(self)
        try:
            sOrigin=vtLog.vtLngGetRelevantNamesWX(self,appl)
            self.SetOrigin(sOrigin)
        except:
            vtLog.vtLngTB(self.GetName())
            self.SetOrigin(appl)
        
        self.thdProc=vtThreadWX(widDlgPar,bPost=False,
                origin=u':'.join([self.GetOrigin(),u'thdProc']))
        self.oSF=vtStateFlag(
                    {
                    0x000:{'name':'CLOSED','translation':_(u'closed'),
                        'init':True,'is':None,
                        'enter':{'set':['CLSD','STLD',],
                                 'clr':['WRKG','FLT','RDY','MOD']}},
                    0x001:{'name':'OPEN','translation':_(u'open'),
                        'is':None,
                        'enter':{'set':['WRKG','BLOCK_NOTIFY'],
                                 'clr':['STLD','FLT','CLSD','MOD','RDY','ONLINE']}},
                    0x003:{'name':'GENID','translation':_(u'generate IDs'),
                        'is':None,
                        'enter':{'set':['WRKG']}},
                    0x004:{'name':'OPENED','translation':_(u'opened'),
                        'is':'IsOpenOk',
                        'enter':{'set':['OPND',],
                                 'clr':['FLT','MOD','BLOCK_NOTIFY']},
                        '':{'set':['WRKG']}},
                    0x005:{'name':'OPEN_FLT','translation':_(u'open fault'),
                        'is':'IsOpenFault',
                        'enter':{'set':['FLT','STLD','OPND','MOD',],
                                 'clr':['WRKG','RDY','BLOCK_NOTIFY',]}},
                    0x006:{'name':'PAUSE','translation':_(u'pause'),
                        'is':None,
                        'enter':{'set':['WRKG'],
                                 'clr':['RDY']}},
                    0x007:{'name':'PAUSING','translation':_(u'pausing'),
                        'is':None,
                        'enter':{'set':['WRKG']}},
                    0x008:{'name':'PAUSED','translation':_(u'paused'),
                        'is':None,
                        'enter':{#'set':['fin',],
                                 'clr':['WRKG','RDY']}},
                    0x009:{'name':'RESUME','translation':_(u'resume'),
                        'enter':{'set':['WRKG']}},
                    0x00E:{'name':'NEW','translation':_(u'new'),
                        'enter':{'set':['MOD']}},
                    0x00F:{'name':'CLOSING','translation':_(u'closing'),
                        'enter':{'set':['WRKG']}},
                    0x010:{'name':'DISCONNECT','translation':_(u'disconnect'),
                        'is':'IsDisConnect',
                        'enter':{'set':['WRKG',],
                                 'clr':['STLD','RDY','ONLINE','CONN_ATTEMPT']}},
                    0x015:{'name':'DISCONNECTED','translation':_(u'disconnected'),
                        'is':'IsDisConnected',
                        'enter':{'set':['STLD'],
                                 'clr':['WRKG','RDY']}},
                    0x01F:{'name':'PRECONNECT','translation':_(u'pre connect'),
                        'is':None,
                        'enter':{'set':['WRKG','BLOCK_NOTIFY'],
                                 'clr':['STLD','RDY','CONN_ATTEMPT']}},
                    0x020:{'name':'CONNECT','translation':_(u'connect all'),
                        'is':'IsConnect',
                        'enter':{'set':['WRKG','BLOCK_NOTIFY','CONN_ATTEMPT','ONLINE'],
                                 'clr':['STLD','RDY']}},
                    0x021:{'name':'CONNECT_MAIN','translation':_(u'main alias connect'),
                        'is':'IsConnectMain',
                        'enter':{'set':['WRKG','BLOCK_NOTIFY','CONN_ATTEMPT'],
                                 'clr':['STLD','RDY']}},
                    0x022:{'name':'CONNECT_SUB','translation':_(u'sub alias connect'),
                        'is':'IsConnectSub',
                        'enter':{'set':['WRKG','BLOCK_NOTIFY','CONN_ATTEMPT','CONN_All'],
                                 'clr':['STLD','RDY']}},
                    0x030:{'name':'CONNECT_FLT','translation':_(u'at least one connect fault'),
                        'is':'IsConnectFault',
                        'enter':{'set':['FLT','STLD'],
                                 'clr':['WRKG','RDY']}},
                    0x031:{'name':'CONNECT_FLT_MAIN','translation':_(u'main alias connect fault'),
                        'is':'IsConnectFaultMain',
                        'enter':{'set':['FLT','STLD'],
                                 'clr':['WRKG','RDY']}},
                    0x032:{'name':'CONNECT_FLT_SUB','translation':_(u'sub alias connect fault'),
                        'is':'IsConnectFaultSub',
                        'enter':{'set':['FLT','STLD'],
                                 'clr':['WRKG','RDY']}},
                    0x060:{'name':'CONNECTED','translation':_(u'all connected'),
                        'is':'IsConnected',
                        'enter':{'set':['RDY','STLD'],
                                 'clr':['WRKG','BLOCK_NOTIFY']}},
                    0x061:{'name':'CONNECTED_MAIN','translation':_(u'main alias connected'),
                        'is':'IsConnectedMain',
                        'enter':{'set':['STLD'],
                                 'clr':['WRKG','RDY','BLOCK_NOTIFY']}},
                    0x062:{'name':'CONNECTED_SUB','translation':_(u'sub aliases connected'),
                        'is':'IsConnectedSub',
                        'enter':{'set':['STLD'],
                                 'clr':['WRKG','RDY','BLOCK_NOTIFY']}},
                    0x0F0:{'name':'SHUTTINGDOWN','translation':_(u'shutting down'),
                        'is':'IsStateShuttingDown',
                        'enter':{'set':['WRKG','SHTGDN',],
                                 'clr':['RDY','STLD']}},
                    0x0FF:{'name':'SHUTDOWN','translation':_(u'shut down'),
                        'is':'IsStateShutDown',
                        'enter':{'set':['CLSD','SHTDN',],
                                 'clr':['WRKG','RDY','STLD']}},
                    },
                    {
                    0x00000001:{'name':'CLSD', 'translation':_(u'closed'),'is':'IsClsd','order':0},
                    0x00000002:{'name':'STLD', 'translation':_(u'settled'),'is':'IsSettled','order':1},
                    0x00000004:{'name':'WRKG',  'translation':_(u'working'),'is':'IsWorking','order':2},
                    0x00000008:{'name':'RDY',  'translation':_(u'ready'),'is':'IsReady','order':3},
                    0x00000010:{'name':'MOD',  'translation':_(u'modified'),'is':'IsModified','order':4},
                    0x00000020:{'name':'FLT',  'translation':_(u'faulty'),'is':'IsFaulty','order':5},
                    0x00000040:{'name':'OPND', 'translation':_(u'opened'),'is':'IsOpened','order':6},
                    0x00000200:{'name':'LOGIN_EXT', 'translation':_(u'extended login'),'is':'IsLoginExt','order':9},
                    0x00000400:{'name':'BLOCK_NOTIFY', 'translation':_(u'block events'),'is':'IsBlockNotify','order':10},
                    0x00001000:{'name':'CFG_VALID', 'translation':_(u'configuration valid'),'is':'IsCfgValid','order':11},
                    0x00002000:{'name':'SYNCHED', 'translation':_(u'synched'),'is':'IsSynched','order':12},
                    0x00004000:{'name':'CONN_All', 'translation':_(u'connect all'),'is':'IsConnActive','order':13},
                    0x00010000:{'name':'CONN_ATTEMPT', 'translation':_(u'connection attemped'),'is':'IsConnAttempt','order':16},
                    0x00040000:{'name':'SHTGDN', 'translation':_(u'shutting down'),'is':'IsShuttingDown','keep':True,'order':17},
                    0x00080000:{'name':'SHTDN', 'translation':_(u'shutdown'),'is':'IsShutDown','keep':True,'order':18},
                    0x00200000:{'name':'ONLINE', 'translation':_(u'online'),'is':'IsOnline','order':20},
                    0x00400000:{'name':'ACTIVE', 'translation':_(u'active'),'is':'IsActive','order':21},
                    },
                    verbose=0,
                    ident=self,
                    funcNotifyCB=self.NotifyStateChangeSlave,
                    origin=u':'.join([self.origin,u'StateFlag']))
        
        self.dlgBrowse=vtNetSecXmlGuiSlaveBrowseDialog(widDlgPar)
        self.dlgBrowse.SetNetSlave(self)
        self.dlgStatus=None
        self.dlgClose=None
        
        self.dlgLogin=vNetLoginDialog(widDlgPar)
        self.dlgLogin.EnableHostCfg(True)
        EVT_VTNET_LOGIN_DLG_FINISHED(self.dlgLogin,self.OnLoginDlgFin)
        
        self.dlgEditResolve=vtNetXmlSynchResolveDialog(widDlgPar,
                    _(u'vtNet Slave Synch Resolve Dialog'))
        
        self.oLoginStorage=None
        widStatus=None
        self.dAppl={}
        self.appls=[]
        self.lCheck=[]
        self.lSettled=[]
        self.dn=vcCust.USR_LOCAL_DN     # 070204:wro os.path.expanduser('~')
        self.docCfg=None
        self.node=None
        self.iStatus=self.DISCONNECTED
        self.iStatusPrev=self.DISCONNECTED
        
        # handle messages send by threads
        self.qMsg=Queue.Queue()
        self.semNotify=threading.Lock()#threading.Semaphore()
        self.semChange=threading.Lock()#threading.Semaphore()
        self.dt=vtDateTime(True)
        if self.USE_TIMER4QUEUE_CHECK:
            #self.timer=wx.PyTimer(self.CheckQueue)
            self.timer=wx.Timer(self)
            self.Bind(wx.EVT_TIMER,self.OnCheckQueue)
            self.timer.Start(self.UPDATE_TIME)
        else:
            EVT_NET_SEC_XML_SLAVE_MULTIPLE_NOTIFY(self,self.OnCheckQueue)
        #EVT_NET_SEC_XML_SLAVE_MULTIPLE_SHUTDOWN(self,self.OnShutDown)
        
        #self.thdStart=thdNetXmlStart(self)
        #self.thdStop=thdNetXmlStop(self)
        
        #img=img_gui.getStoppedMedBitmap()
        #self.SetBitmap(img)
        
        wx.EVT_LEFT_DOWN(self, self.OnLeftDown)
        wx.EVT_RIGHT_DOWN(self, self.OnRightDown)
    def InitWidgetStatus(self,widStatus=None):
        if widStatus is None:
            #self.widStatus=vtNetSecXmlGuiSlaveDialog(self)
            #self.widStatus.SetNetMaster(self)
            self.dlgStatus=vtNetSecXmlGuiSlaveDialog(self)
            self.widStatus=self.dlgStatus.pnStatus
            self.widStatus.SetNetMaster(self)
        else:
            self.widStatus=widStatus
    def SetLocalDN(self,dn):
        self.dn=dn
    def SetCfg(self,doc,lChild,appls):
        self.docCfg=doc
        self.lChild=lChild
        self.appls=appls
        if len(appls)>0:
            self.dlgBrowse.SetAppl(appls[0])
    def SetCfgNode(self,bConnect=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
        self.dn=vcCust.USR_LOCAL_DN     # 070204:wro os.path.expanduser('~')
        if self.docCfg is not None:
            self.node=self.docCfg.getChildByLstForced(self.docCfg.getRoot(),self.lChild)
            if self.node is not None:
                self.dn=self.docCfg.getNodeText(self.node,'path')
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'dn:%s'%(self.dn),self)
                try:
                    os.path.exists(self.dn)
                except:
                    vtLog.vtLngTB(self.GetName())
                    self.dn=vcCust.USR_LOCAL_DN     # 070204:wro os.path.expanduser('~')
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'dn:%s'%(self.dn),self)
        if len(self.appls)>0:
            lConn=self.GetConnections(self.appls[0],None)
            self.dlgBrowse.UpdateCfg(self.dn,lConn)
    def StoreCfg(self,dn,lConn):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'dn:%s;lConn:%s'%(dn,vtLog.pformat(lConn)),self)
        if self.docCfg is not None:
            self.node=self.docCfg.getChildByLstForced(self.docCfg.getRoot(),self.lChild)
            if self.node is not None:
                self.dn=dn
                self.docCfg.setNodeText(self.node,'path',dn)
                if len(self.appls)>0:
                    self.SetConnections(self.appls[0],lConn)
    def Open(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
        if len(self.appls)>0:
            lConn=self.GetConnections(self.appls[0],None)
            lAlias=[':'.join([t[0],t[1]]) for t in lConn]
            #self.AddAliasLst(lAlias,True)
            self.AddAliasLst(lConn,True)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'lConn:%s'%(vtLog.pformat(lConn)),self)
    def DoAutoConnect(self,sName,bOffline=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCurWX(vtLog.INFO,'%s offline:%d'%(sName,bOffline),self)
        self.DoConnect(-1)
    def DoConnect(self,selIdx):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
        if len(self.appls)>0:
            lConn=self.GetConnections(self.appls[0],None)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'lConn:%s'%(
                                vtLog.pformat(lConn)),self)
            lAlias=[':'.join([t[0],t[1]]) for t in lConn]
            self.lCheck=[(t[0],t[1]) for t in lConn]   # 070904:wro FIXME, full connection
            #self.AddAliasLst(lAlias,False)
            try:
                lConnAlias=[getApplHostConnectionStr(t[2],t[3],t[4],t[1],t[0]) for t in lConn]
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'lConnAlias:%s'%(
                                vtLog.pformat(lConnAlias)),self)
            except:
                vtLog.vtLngTB(self.GetName())
            self.AddAliasLst(lConn,False)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'lConn:%s'%(vtLog.pformat(lConn)),self)
        
    def DoDisConnect(self,selIdx):
        try:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCurWX(vtLog.INFO,'',self)
            if len(self.appls)>0:
                lConn=self.GetConnections(self.appls[0],None)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'lConn:%s'%(vtLog.pformat(lConn)),self)
        except:
            vtLog.vtLngTB(self.GetName())
    def GetOrigin(self):
        return self.origin
    def SetOrigin(self,origin):
        self.origin=origin
    def BindEvents(self,funcStart=None,funcFinish=None,funcShutDown=None):
        if funcStart is not None:
            EVT_NET_SEC_XML_SLAVE_MULTIPLE_START(self,funcStart)
        if funcFinish is not None:
            EVT_NET_SEC_XML_SLAVE_MULTIPLE_FINISHED(self,funcFinish)
        if funcShutDown is not None:
            EVT_NET_SEC_XML_SLAVE_MULTIPLE_SHUTDOWN(self,funcShutDown)
    #def SetBitmap(self,img):
    #    wx.BitmapButton.SetBitmapLabel(self,img)
    #    self.Refresh()
    def SetBitmap(self,img):
        self.__setBitMap__(img)
    def __setBitMap__(self,img):
        #if self.IsFlag(self.SHUTDOWN):
        #    return
        vtLog.vtLngCur(vtLog.DEBUG,''%(),self.GetOrigin())
        self.Refresh()
    def DrawBezel(self, dc, x1, y1, x2, y2):
        if self.hasFocus and self.useFocusInd:
            return
        # draw the upper left sides
        if self.up:
            #dc.SetPen(self.highlightPen)
            dc.SetPen(self.shadowPen)
        else:
            dc.SetPen(self.shadowPen)
        for i in range(self.bezelWidth):
            dc.DrawLine(x1+i, y1, x1+i, y2-i)
            dc.DrawLine(x1, y1+i, x2-i, y1+i)
        # draw the lower right sides
        if self.up:
            dc.SetPen(self.shadowPen)
        else:
            #dc.SetPen(self.highlightPen)
            dc.SetPen(self.shadowPen)
        for i in range(self.bezelWidth):
            dc.DrawLine(x1+i, y2-i, x2+1, y2-i)
            dc.DrawLine(x2-i, y1+i, x2-i, y2)
    def DrawFocusIndicator(self, dc, w, h):
        bw = self.bezelWidth
        self.focusIndPen.SetColour(self.focusClr)
        dc.SetLogicalFunction(wx.INVERT)
        dc.SetPen(self.focusIndPen)
        dc.SetBrush(wx.TRANSPARENT_BRUSH)
        dc.DrawRectangle(bw,bw,  w-bw*2, h-bw*2)
        dc.SetLogicalFunction(wx.COPY)
    def DrawLabel(self, dc, width, height, dw=0, dy=0):
        try:
            x,y=0,0
            def getXY(bmp):
                bw,bh = bmp.GetWidth(), bmp.GetHeight()
                x,y=(width-bw)/2+dw, (height-bh)/2+dy
                return x,y
            iStatus=-1
            iStatusSub=-1
            iPrior=0
            #vtLog.CallStack('')
            #print self.lCheck
            #print self.appls
            #print self.dAppl
            #for sAppl,sAlias in self.lCheck:
            #if self.widStatus.__check__(sAppl,sAlias):
            #    vtLog.vtLngCurWX(vtLog.DEBUG,'true',self)
            #    dictNet=self.widStatus.GetDictNet()
            lNetAppl=self.widStatus.GetNetNames()
            dNetDoc=self.widStatus.GetDictNet()
            #print lNetAppl
            #print dNetDoc
            if len(lNetAppl)>0:
                #netDocMaster=dNetDoc[lNetAppl[0]]
                if hasattr(self,'DICT_PRIOR_OVERLAY')==False:
                    netDocMaster=self.widStatus.__getDocByIdx__(0)
                    if netDocMaster is not None:
                        netDoc=netDocMaster
                        self.DICT_PRIOR_OVERLAY={netDoc.SERVE_FLT:255,netDoc.CONNECT_FLT:254,netDoc.SYNCH_ABORT:253,
                                netDoc.OPEN_FLT:252,netDoc.STOPPED:100,
                                netDoc.SYNCH_PROC:1}
                if hasattr(self,'DICT_PRIOR_OVERLAY'):
                    #print self.DICT_PRIOR_OVERLAY
                    for name in lNetAppl:
                        #netDoc=dNetDoc[name]
                        dAlias=dNetDoc[name]['alias']
                        for l in dAlias.itervalues():
                            idx=l[1]
                            netDoc=l[0]
                            i=netDoc.GetStatus()
                            if idx==0:
                                iStatus=i
                                netDocMaster=netDoc
                            else:
                                if i in self.DICT_PRIOR_OVERLAY:
                                    
                                    j=self.DICT_PRIOR_OVERLAY[i]
                                    if j>iPrior:
                                        iStatusSub=i
                                        iPrior=j
                                        netDocSub=netDoc
                    #vtLog.CallStack('')
                    #print iStatus
                    #print iStatusSub
                    #brush = self.GetBackgroundBrush(dc)
                    #if brush is not None:
                    #    dc.SetBackground(brush)
                    #    dc.Clear()
                    bmp=netDocMaster.__getMedBitmapByStatus__(iStatus)
                    hasMask = bmp.GetMask() != None
                    x,y=getXY(bmp)
                    dc.DrawBitmap(bmp,x,y,hasMask)
                    if iStatusSub>=0:
                        bmp=netDocSub.__getMedSubBitmapByStatus__(iStatusSub)
                        hasMask = bmp.GetMask() != None
                        x,y=getXY(bmp)
                        dc.DrawBitmap(bmp,x,y,hasMask)
                    return
        except:
            vtLog.vtLngTB(self.GetOrigin())
        try:
            bmp=self.imgDft
            hasMask = bmp.GetMask() != None
            x,y=getXY(bmp)
            dc.DrawBitmap(bmp,x,y,hasMask)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnPaint(self, event):
        (width, height) = self.GetClientSizeTuple()
        x1 = y1 = 0
        x2 = width-1
        y2 = height-1
        dc = wx.PaintDC(self)
        brush = self.GetBackgroundBrush(dc)
        if brush is not None:
            dc.SetBackground(brush)
            dc.Clear()
        self.DrawLabel(dc, width, height)
        self.DrawBezel(dc, x1, y1, x2, y2)
        if self.hasFocus and self.useFocusInd:
            self.DrawFocusIndicator(dc, width, height)
    def IsStatusChanged(self,iStatusPrev=None,iStatus=None):
        vtLog.vtLngCur(vtLog.CRITICAL,''%(),self.GetOrigin())
    def SetFlag(self,int_flag,flag):
        vtLog.vtLngCur(vtLog.CRITICAL,''%(),self.GetOrigin())
    def IsFlag(self,int_flag,iStatus=None):
        vtLog.vtLngCur(vtLog.CRITICAL,''%(),self.GetOrigin())
    def SetLoginDlg(self,dlg):
        self.dlgLogin=dlg
    def GetLoginDlg(self):
        if self.dlgLogin is None:
            return None
        if self.dlgLogin.IsShown():
            return None
        return self.dlgLogin
    def GetEditResolveDlg(self):
        return self.dlgEditResolve
    def AcquireLogin(self):
        self.semLogin.acquire()
    def ReleaseLogin(self):
        self.semLogin.release()
    def SetLoginStorage(self,oLoginStorage):
        self.oLoginStorage=oLoginStorage
    def CheckLogin(self,appl,host,port,alias,usr):
        vtLog.vtLngCurWX(vtLog.INFO,'appl:%s;host:%s;port:%s;alias:%s;usr:%s'%(appl,host,port,alias,usr),self)
        if self.oLoginStorage is None:
            return None
        try:
            if self.oLoginStorage.acquire('check',blocking=self.SEM_BLOCKING):          # 060911
                try:
                    sLogin,sPasswd=self.oLoginStorage.GetLogin(appl,host,port,alias,usr)
                    
                    if sLogin is None:
                        self.oLoginStorage.release('check')
                        return None
                    else:
                        self.UpdateLogin(appl,usr,sPasswd)
                        self.oLoginStorage.release('check')
                        return sPasswd
                except:
                    self.oLoginStorage.release('check')
                return None
            vtLog.vtLngCurWX(vtLog.DEBUG,'oLoginStorage acquire not possible',self)
        except:
            vtLog.vtLngTB(self.GetName())
        return None
    def UpdateLogin(self,applSrc,usr,passwd):
        pass
    def UpdateLoginInfo(self,appl,oLogin):
        pass
    def SetLogin(self,appl,host,port,alias,usr,passwd):
        vtLog.vtLngCurWX(vtLog.INFO,'appl:%s;host:%s;port:%s;alias:%s;usr:%s'%(appl,host,port,alias,usr),self)
        if self.oLoginStorage is None:
            return None
        if self.oLoginStorage.acquire('check',blocking=self.SEM_BLOCKING):          # 060911
            try:
                self.oLoginStorage.SetLogin(appl,host,port,alias,usr,passwd)
                self.oLoginStorage.Save()
                #self.oLoginStorage.release('check')
            except:
                pass
            self.oLoginStorage.release('check')
            if appl not in self.appls:
                self.__setSlaveConnAlias__(appl,alias,host,str(port),usr,'')
        vtLog.vtLngCurWX(vtLog.DEBUG,'oLoginStorage acquire not possible',self)
    def OnLoginDlgFin(self,evt):
        evt.Skip()
        try:
            sAppl=evt.GetAppl()
            sAlias=evt.GetAlias()
            sUsr=evt.GetUsr()
            if sUsr is None:
                vtLog.vtLngCurWX(vtLog.INFO,'dialog canceled or closed',self)
                self.ReleaseLogin()
                self.__doLoginDlgAbort__(sAppl,sAlias)
                return
            sPasswd=evt.GetPasswd()
            sHost=evt.GetHost()
            sPort=evt.GetPort()
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCurWX(vtLog.INFO,'appl:%s;host:%s;port:%s;alias:%s;usr:%s'%(sAppl,
                    sHost,sPort,sAlias,sUsr),self)
            self.UpdateLogin(sAppl,sUsr,sPasswd)
            self.SetLogin(sAppl,sHost,sPort,
                                        sAlias,sUsr,sPasswd)
            self.ReleaseLogin()
            netDoc=self.GetNetDoc(sAppl,sAlias)
            if netDoc is None:
                return
            if netDoc.LoginFromGui(sUsr,sPasswd)==False:
                self.__doLoginDlgAbort__(sAppl,sAlias)
                #if sAppl==self.lNetAppl[0]:
                    # go ahead with sub connections
                #    netDoc.SetFlag(netDoc.CFG_VALID,True)
                #    netDoc.__setStatus__(netDoc.CONFIG)
        except:
            vtLog.vtLngTB(self.GetName())
    def __doLoginDlgAbort__(self,sAppl,sAlias):
        try:
            netDoc=self.GetNetDoc(sAppl,sAlias)
            if netDoc is None:
                return
            netDoc.DisConnect()
            return
            if sAppl==self.lNetAppl[0]:
                # go ahead with sub connections
                netDoc.SetFlag(netDoc.CFG_VALID,True)
                #netDoc.__setStatus__(netDoc.CONFIG)
            netDoc.DisConnect()
            if sAppl==self.lNetAppl[0]:
                iCount=len(self.lNetAppl)
                #vtLog.vtLngCur(vtLog.DEBUG,'lNetAppl:%s'%(vtLog.pformat(self.lNetAppl)),self.mainAppl)
                for idx in range(1,iCount):
                    name=self.lNetAppl[idx]
                    netDoc=self.dNetDoc[name]
                    vtLog.vtLngCur(vtLog.INFO,'appl:%s;idx:%d;connect_attempt:%s'%\
                            (name,idx,netDoc.IsFlag(netDoc.CONN_ATTEMPT)),self.mainAppl)
                    if netDoc.IsFlag(netDoc.CONN_ATTEMPT)==False:
                            self.AutoConnect2Srv(idx)
                            #self.thdStart.Start()
                self.bConnectAll=False
        except:
            vtLog.vtLngTB(self.GetName())
    def AddNetControl(self,ctrlClass,name,*args,**kwargs):
        if ctrlClass is not None:
            iRet=self.widStatus.RegNetControl(ctrlClass,name,*args,**kwargs)
        else:
            self.dAppl=self.widStatus.GetRegNetControl()
            iRet=1
        return iRet
    def SetCB(self,appl,func,*args,**kwargs):
        if self.widStatus is None:
            return
        dictNet=self.widStatus.GetDictNet()
        if appl in dictNet:
            dictNet[appl]['callback']=(func,args,kwargs)
    def GetConnections(self,sAppl,sAlias):
        l=[]
        nodeConn=self.docCfg.getChildForced(self.node,'connection')
        self.docCfg.procChildsExt(nodeConn,self.__procGetConnAlias__,sAppl,sAlias,l)
        return l
    def SetConnections(self,sAppl,lConn):
        nodeConn=self.docCfg.getChild(self.node,'connection')
        if nodeConn is not None:
            self.docCfg.deleteNode(nodeConn,self.node)
        nodeConn=self.docCfg.getChildForced(self.node,'connection')
        for l in lConn:
            nodeAlias=self.docCfg.createChildByLst(nodeConn,'alias',[
                {'tag':sAppl,'lst':[
                    {'tag':'name','val':l[2]},
                    {'tag':'port','val':l[3]},
                    {'tag':'appl','val':sAppl},
                    {'tag':'alias','val':l[1]},
                    {'tag':'usr','val':l[4]},
                    {'tag':'passwd','val':''},
                    ]
                }
                ])
            self.docCfg.AlignNode(nodeAlias)
        self.docCfg.Save()
    def __procGetAlias__(self,node,sAppl,sAlias,l):
        sTag=self.docCfg.getTagName(node)
        if sTag!=sAppl:
            return 0
        if sAlias is not None:
            if self.docCfg.getNodeText(node,'alias')==sAlias:
                lA=self.__getAliasData__(node)
                l.append([sAppl,sAlias]+lA)
        else:
            s=self.docCfg.getNodeText(node,'alias')
            lA=self.__getAliasData__(node)
            l.append([sAppl,s]+lA)
        return 0
    def __procGetConnAlias__(self,node,sAppl,sAlias,l):
        sTag=self.docCfg.getTagName(node)
        if sTag!='alias':
            return 0
        self.docCfg.procChildsExt(node,self.__procGetAlias__,sAppl,sAlias,l)
        return 0
    def __findLogin__(self,sAppl,sAlias):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if self.docCfg.acquire(blocking=self.SEM_BLOCKING):
            l=[None]
            if self.node is None:
                self.docCfg.release()
                return [None,None,None,None]
            nodeConn=self.docCfg.getChild(self.node,'connection')
            self.docCfg.procChildsExt(nodeConn,self.__procConnAlias__,sAppl,sAlias,l)
            if l[0]!=None:
                retVal=self.__getAliasData__(l[0])
                self.docCfg.release()
                return retVal
            l=[None]
            nodeSlave=self.docCfg.getChildForced(self.node,'slave')
            self.docCfg.procChildsExt(nodeSlave,self.__procConnAlias__,sAppl,sAlias,l)
            if l[0]!=None:
                retVal=self.__getAliasData__(l[0])
                self.docCfg.release()
                return retVal
            self.docCfg.release()
        return [None,None,None,None]
    def __getAliasData__(self,node):
        sHost=self.docCfg.getNodeText(node,'name')
        sPort=self.docCfg.getNodeText(node,'port')
        sUsr=self.docCfg.getNodeText(node,'usr')
        sPasswd=self.docCfg.getNodeText(node,'passwd')
        if len(sPasswd)==0:
            sPasswd=None
        l=[sHost,sPort,sUsr,sPasswd]
        return l
    def __setAliasData__(self,node,sHost,sPort,sUsr,sPasswd):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if self.docCfg.acquire(blocking=self.SEM_BLOCKING):
            self.docCfg.setNodeText(node,'name',sHost)
            self.docCfg.setNodeText(node,'port',sPort)
            self.docCfg.setNodeText(node,'usr',sUsr)
            self.docCfg.setNodeText(node,'passwd',sPasswd)
            self.docCfg.release()
        else:
            vtLog.vtLngCurWX(vtLog.INFO,'acquire fault',self)
    def __procAlias__(self,node,sAppl,sAlias,l):
        sTag=self.docCfg.getTagName(node)
        if sTag!=sAppl:
            return 0
        if self.docCfg.getNodeText(node,'alias')==sAlias:
            l[0]=node
            return -1
        return 0
    def __procConnAlias__(self,node,sAppl,sAlias,l):
        sTag=self.docCfg.getTagName(node)
        if sTag!='alias':
            return 0
        self.docCfg.procChildsExt(node,self.__procAlias__,sAppl,sAlias,l)
        return 0
    def __setSlaveConnAlias__(self,sAppl,sAlias,sHost,sPort,sUsr,sPasswd):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if self.docCfg.acquire(blocking=self.SEM_BLOCKING)==False:
            vtLog.vtLngCurWX(vtLog.INFO,'acquire not possible',self)
            return
        try:
            nodeSlave=self.docCfg.getChildForced(self.node,'slave')
            if nodeSlave is None:
                self.docCfg.release()
                return
            l=[None]
            self.docCfg.procChildsExt(nodeSlave,self.__procConnAlias__,sAppl,sAlias,l)
            if l[0] is not None:
                node=l[0]
            else:
                nodeAlias=self.docCfg.getChildForced(nodeSlave,'alias')
                if nodeAlias is None:
                    self.docCfg.release()
                    return
                node=self.docCfg.getChildForced(nodeAlias,sAppl)
                if node is None:
                    self.docCfg.release()
                    return
                self.docCfg.setNodeText(node,'alias',sAlias)
            self.docCfg.release()
            self.__setAliasData__(node,sHost,sPort,sUsr,sPasswd)
            return
        except:
            pass
        self.docCfg.release()
        #self.docCfg.Save()
    def AddAliasLst(self,lAlias,bOffline=False):
        try:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCurWX(vtLog.INFO,'lAlias:%s;bOffline:%d'%(vtLog.pformat(lAlias),bOffline),self)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'lAlias:%s;bOffline:%d;dn:%s'%(vtLog.pformat(lAlias),bOffline,self.dn),self)
            dlg=self.GetLoginDlg()
            if dlg is None:
                if vtLog.vtLngIsLogged(vtLog.WARN):
                    vtLog.vtLngCurWX(vtLog.WARN,'no login dialog defined, or temporary not available',self)
            #for sApplAlias in lAlias:
            #    strs=sApplAlias.split(':')
            for strs in lAlias:
                if len(strs)<5:
                    vtLog.vtLngCurWX(vtLog.ERROR,'invalid format;%s'%(vtLog.pformat(strs)),self)
                    continue
                # add slave connection
                sAppl=strs[0]
                sAlias=strs[1]
                sHost=strs[2]
                sPort=strs[3]
                sUsr=strs[4]
                sPasswdStored=''
                
                ctrl=self.widStatus.AddNetControl(sAppl,sAlias)
                if sAppl==self.appls[0]:
                    ctrl.SetupAsMainAlias()
                    #ctrl.ClrCBs(ctrl.CONFIG,'enter')
                    #ctrl.AddCB(ctrl.oSF.LOGGEDIN,'enter',self.GetConfig)
                    ctrl.AddCB(ctrl.CONFIG,'enter',self.__addConfig__,ctrl)
                if 0:
                    l=self.__findLogin__(sAppl,sAlias)
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurWX(vtLog.DEBUG,vtLog.pformat(l),self)
                    sHost=l[0]
                    sPort=l[1]
                    if l[2] is None:
                        sUsr=l[2]
                        sPasswdStored=l[3]
                    else:
                        self.__logConnections__()
                        sUsr=''
                        sPasswdStored=''
                if sHost is None:
                    if dlg is None:
                        continue
                    if dlg.IsShown():
                        continue
                    dlg.Centre()
                    
                    sTmpHost,iTmpPort=vcCust.getDefaultHostPortPerNetAppl(sAppl)
                    
                    dlg.SetValues(sAppl,sTmpHost,iTmpPort,sAlias,'')
                    if dlg.ShowModal()>0:
                        sHost=dlg.GetHost()
                        sPort=dlg.GetPort()
                        sUsr=dlg.GetUsr()
                        sPasswdStored=dlg.GetPasswd()
                        self.SetLogin(sAppl,sHost,sPort,sAlias,sUsr,sPasswdStored)
                        
                    else:
                        continue
                else:
                    if self.oLoginStorage is not None:
                        sPasswd=self.CheckLogin(sAppl,sAlias,sHost,sPort,sUsr)
                    else:
                        sPasswd=None
                    if sPasswd is not None:
                        sPasswdStored=sPasswd
                    if sUsr is None:
                        sUsr=''
                    if sPasswdStored is None:
                        sPasswdStored=''
                self.__setSlaveConnAlias__(sAppl,sAlias,sHost,sPort,sUsr,'')
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'appl:%s;alias:%s;host:%s;port:%s;usr:%s;passwd:%s'%(sAppl,sAlias,sHost,sPort,sUsr,sPasswdStored),self)
                
                oAlias=vtNetSecLoginHost(passwd=sPasswdStored,usr=sUsr,
                                host=sHost,port=sPort,
                                appl=sAppl,alias=sAlias)
                ctrl.Connect2SrvByAlias(oAlias,self.dn,offline=bOffline)
        except:
            vtLog.vtLngTB(self.GetName())
    def __getLogStatusStr__(self):
        return ''
    def __logStatus__(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,self.__getLogStatusStr__(),
                        origin=self.GetOrigin())
    def __logConnections__(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'appls:%s;dAppl:%s;lCheck:%s;lSettled:%s'%(
                        vtLog.pformat(self.appls),
                        vtLog.pformat(self.dAppl),
                        vtLog.pformat(self.lCheck),
                        vtLog.pformat(self.lSettled)),
                        origin=self.GetOrigin())
    def NotifyStateChangeSlave(self,netMaster,iStatusPrev,iStatus,zStamp,*args,**kwargs):
        try:
            self.Refresh()
            #self.dlgMaster.Notify(msgTup[1],netDoc,iStatusPrev,iStatus,*msgTup[4],**msgTup[5])
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def NotifyStateChangeAlias(self,netDoc,iStatusPrev,iStatus,zStamp,*args,**kwargs):
        try:
            name=netDoc.GetAppl()
            alias=netDoc.GetAlias()
            #self.dlgLog.AddLog(zStamp,name,netDoc.oSF.GetStateName(iStatus),iStatusPrev,iStatus)
            self.widStatus.Notify(name,alias,netDoc,iStatusPrev,iStatus,*args,**kwargs)
            self.Refresh()
            self.__check__()
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def Notify(self,name,alias,netDoc,iStatusPrev,iStatus,*args,**kwargs):
        vtLog.vtLngCur(vtLog.CRITICAL,''%(),self.GetOrigin())
        if self.VERBOSE:
            vtLog.vtLngCur(vtLog.DEBUG,'appl:%s;prev:0x%04x;act :0x%04x'%(name,iStatusPrev,iStatus),self.GetName())
        self.semNotify.acquire()
        try:
            self.dt.Now()
            sTm=self.dt.GetDateTimeStr(' ')
            self.qMsg.put((sTm,name,alias,iStatusPrev,iStatus,args,kwargs))
        except:
            vtLog.vtLngTB(self.GetName())
        self.semNotify.release()
        if self.USE_TIMER4QUEUE_CHECK==False:
            wx.PostEvent(self,vtnxSecXmlSlaveMultipleNotify(self))
    def OnCheckQueue(self,evt):
        vtLog.vtLngCur(vtLog.CRITICAL,''%(),self.GetOrigin())
        self.CheckQueue()
    def CheckQueue(self):
        vtLog.vtLngCur(vtLog.CRITICAL,''%(),self.GetOrigin())
        try:
            #print 'CheckQueue',self.dlgLogin.IsShown()
            #if self.dlgLogin.IsShown():
            #    return
            while self.qMsg.empty()==False:
                msgTup=self.qMsg.get(0)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,vtLog.pformat(msgTup),self)
                #print msgTup
                try:
                    sAppl=msgTup[1]
                    sAlias=msgTup[2]
                    
                    netDoc=self.widStatus.GetNetDoc(sAppl,sAlias)
                    sMsg=netDoc.GetStatusStr(msgTup[4])
                    iStatusPrev,iStatus=msgTup[3],msgTup[4]
                    if netDoc.IsStatusChanged(iStatusPrev,iStatus):
                        #self.dlgLog.AddLog(msgTup[0],msgTup[1],sMsg,msgTup[2],msgTup[3])
                        self.widStatus.Notify(sAppl,sAlias,netDoc,iStatusPrev,iStatus,*msgTup[5],**msgTup[6])
                        netDoc.__setBitmapByStatus__()
                    #if netDoc.GetStatus(iStatus)==netDoc.SYNCH_START:
                    #    self.dlgStatus.SetProcess(msgTup[1],iStatusPrev,iStatus,0,100)
                    #elif netDoc.GetStatus(iStatus)==netDoc.SYNCH_PROC:
                    #    self.dlgStatus.SetProcess(msgTup[1],iStatusPrev,iStatus,msgTup[4][0],msgTup[4][1])
                    #elif netDoc.GetStatus(iStatus)==netDoc.SYNCH_FIN:
                    #    self.dlgStatus.SetProcess(msgTup[1],iStatusPrev,iStatus,0,100)
                    if sAppl==self.appls[0]:
                        if netDoc.IsStatusChanged():
                            self.SetBitmap(netDoc.__getMedBitmapByStatus__(netDoc.GetStatus()))
                        if netDoc.GetStatus(iStatus)==netDoc.LOGIN:
                            netDoc.Login()
                        if netDoc.GetStatus(iStatus)==netDoc.LOGGEDIN:
                            netDoc.GetConfig()
                        if netDoc.GetStatus(iStatus)==netDoc.CONFIG:
                            # add this also to slave
                            self.__addConfig__(netDoc)
                            netDoc.getDataFromRemote()
                    else:
                        if netDoc.IsStatusChanged():
                            self.SetBitmap(netDoc.__getMedBitmapByStatus__(netDoc.GetStatus()))
                        if netDoc.GetStatus(iStatus)==netDoc.LOGIN:
                            netDoc.Login()
                        if netDoc.GetStatus(iStatus)==netDoc.LOGGEDIN:
                            netDoc.getDataFromRemote()
                except:
                    vtLog.vtLngTB(self.GetName())
                    netDoc=None
                    sMsg=''
        except:
            #vtLog.vtLngTB(self.GetName())
            pass
        self.__check__()
    def __check__(self):
        self.__logConnections__()
        for sAppl,sAlias in self.lCheck:
            if self.widStatus.__check__(sAppl,sAlias):
                vtLog.vtLngCurWX(vtLog.DEBUG,'true',self)
                dictNet=self.widStatus.GetDictNet()
                dNet=dictNet[sAppl]
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'dNet:%s'%(vtLog.pformat(dNet)),self.GetOrigin())
                if 'callback' in dNet:
                    t=dNet['callback']
                    args=t[1]
                    kwargs=t[2]
                    dAlias=dNet['alias']
                    netDoc=self.GetNetDoc(sAppl,sAlias)
                    dDocs={}
                    d=self.widStatus.GetNetDocBySlaves(sAppl,sAlias,dDocs)
                    netDoc.SetNetDocs(dDocs)
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'d:%s;dDocs:%s'%(vtLog.pformat(d),vtLog.pformat(dDocs)),self.GetOrigin())
                    t[0](sAppl,sAlias,*args,**kwargs)
                    self.lCheck.remove((sAppl,sAlias))
                    self.lSettled.append((sAppl,sAlias,t[0],t[1],t[2]))
                    try:
                        netDoc.SetBlockNotify(False)
                    except:
                        vtLog.vtLngTB(self.GetOrigin())
            else:
                vtLog.vtLngCurWX(vtLog.DEBUG,'false',self)
        self.__logConnections__()
    def __addConfig__(self,netDoc):
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
        nodeCfg=netDoc.getChild(netDoc.getRoot(),'config')
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'nodeCfg:%s'%(nodeCfg),self)
            oLogin=netDoc.GetLogin()
            if oLogin is None:
                vtLog.vtLngCurWX(vtLog.ERROR,
                            'no login info available'%(),self)
            sUsr=oLogin.GetUsr()
            if len(sUsr)==0:
                vtLog.vtLngCurWX(vtLog.ERROR,
                            'login info incomplete;%s'%(oLogin),self)
        except:
            vtLog.vtLngTB(self.GetName())
        lAlias=[]
        nodeSlave=self.docCfg.getChildForced(self.node,'slave')
        for c in netDoc.getChilds(nodeCfg):
            sAppl=netDoc.getTagName(c)
            nodeHost=netDoc.getChild(c,'host')
            sHost=netDoc.getNodeText(nodeHost,'name')
            sPort=netDoc.getNodeText(nodeHost,'port')
            sAlias=netDoc.getNodeText(nodeHost,'alias')
            sSlave=':'.join([sAppl,sAlias])
            if self.widStatus is not None:
                lSlaves=self.widStatus.GetSlaves(netDoc.GetAppl(),netDoc.GetAlias())
                if sSlave not in lSlaves:
                    lSlaves.append(sSlave)
                    #lAlias.append(sSlave)
                    lAlias.append([sAppl,sAlias,sHost,sPort,sUsr])
            else:
                continue
            # ensure at slave connections
            l=[None]
            self.docCfg.procChildsExt(nodeSlave,self.__procConnAlias__,sAppl,sAlias,l)
            if l[0]!=None:
                node=l[0]
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'nodeCfg:%s'%(nodeCfg),self)
                sSlaveHost=self.docCfg.getNodeText(node,'name')
                sSlavePort=self.docCfg.getNodeText(node,'port')
                if sSlaveHost!=sHost:
                    if vtLog.vtLngIsLogged(vtLog.INFO):
                        vtLog.vtLngCurWX(vtLog.INFO,'node:%s;host:%s updated'%(node,sHost),self)
                    self.docCfg.setNodeText(node,'name',sHost)
                if sSlavePort!=sPort:
                    if vtLog.vtLngIsLogged(vtLog.INFO):
                        vtLog.vtLngCurWX(vtLog.INFO,'node:%s;port:%s updated'%(node,sPort),self)
                    self.docCfg.setNodeText(node,'port',sPort)
            else:
                if vtLog.vtLngIsLogged(vtLog.INFO):
                    vtLog.vtLngCurWX(vtLog.INFO,'node added',self)
                nodeAlias=self.docCfg.createChildByLst(nodeSlave,'alias',[
                    {'tag':sAppl,'lst':[
                        {'tag':'name','val':sHost},
                        {'tag':'port','val':sPort},
                        {'tag':'appl','val':sAppl},
                        {'tag':'alias','val':sAlias},
                        {'tag':'usr','val':sUsr},
                        {'tag':'passwd','val':''},
                        ]
                    }
                    ])
                self.docCfg.AlignNode(nodeAlias)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'dictNet:%s'%(vtLog.pformat(self.widStatus.GetDictNet())),self)
        self.AddAliasLst(lAlias,False)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'dictNet:%s'%(vtLog.pformat(self.widStatus.GetDictNet())),self)
    def OnLeftDown(self,evt):
        self.dlgBrowse.Centre()
        self.dlgBrowse.Show(True)
    def OnRightDown(self,evt):
        if self.dlgStatus is not None:
            self.dlgStatus.Centre()
            self.dlgStatus.Show(True)
    def Stop(self,bModal=True):
        vtLog.vtLngCurWX(vtLog.INFO,''%(),self)
        self.bBlockNotify=True
        self.dlgBrowse.Show(False)
        if self.dlgStatus is not None:
            self.dlgStatus.Show(False)
        if self.widStatus is not None:
            if self.dlgClose is not None:
                self.dlgClose.SetFocus()
                return
            self.dlgClose=vtNetSecXmlGuiSlaveCloseDialog(self)
            self.dlgClose.Centre()
            self.dlgClose.SetDictNet(self.widStatus.GetDictNet())
            wx.EVT_CLOSE(self.dlgClose,self.OnShutDown)
            if bModal:
                self.dlgClose.ShowModal()
            else:
                self.dlgClose.Show(True)
            #time.sleep(2)
    def ShutDown(self,bModal=False):
        vtLog.vtLngCurWX(vtLog.INFO,''%(),self)
        self.oSF.SetState(self.oSF.SHUTTINGDOWN)
        #self.dlgMaster.ShutDown()
        #if self.USE_TIMER4QUEUE_CHECK:
        #    self.timer.Stop()
        if self.widStatus is not None:
            self.widStatus.ShutDown()
        self.Stop(bModal=bModal)
    def IsModified(self):
        if self.widStatus is not None:
            return self.widStatus.IsModified()
        else:
            return False
    def Save(self,bModified=False):
        if self.widStatus is not None:
            self.widStatus.Save(bModified)
    def Close(self):
        if self.widStatus is not None:
            self.widStatus.Close()
        #vtLog.CallStack('')
        #print self.lSettled
    def ShowPopup(self):
        btn=self
        pos = btn.ClientToScreen( (0,0) )
        sz =  btn.GetSize()
        self.dlgBrowse.Centre()
        self.dlgBrowse.Show(True)
    def GetNetDoc(self,name,alias):
        if self.widStatus is None:
            return None
        return self.widStatus.GetNetDoc(name,alias)
    def GetSettled(self):
        return self.lSettled
    def IsAllSettled(self):
        if len(self.lCheck)==0:
            return True
        return False
    def IsShutDownActive(self):
        return self.oSF.IsShuttingDown or self.oSF.IsShutDown
        return self.IsFlag(self.SHUTDOWN)
    def IsShutDown(self):
        if self.widStatus is None:
            return True
        return self.widStatus.IsShutDown()
    def OnShutDown(self,evt):
        evt.Skip()
        vtLog.vtLngCurWX(vtLog.INFO,''%(),self)
        #self.Close()
        self.oSF.SetState(self.oSF.SHUTDOWN)
        #self.SetFlag(self.SHUTDOWN_FIN,True)
        wx.PostEvent(self,vtnxSecXmlSlaveMultipleShutdown(self))
    def IsShutDownFinished(self):
        return self.oSF.IsShutDown
        return self.IsFlag(self.SHUTDOWN_FIN)
    def changeIdNum(self,idOld,idNew,sAlias):
        vtLog.vtLngCur(vtLog.INFO,'idOld:%08d idNew:%08d alias:%s'%(idOld,idNew,sAlias))
        
