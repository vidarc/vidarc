#----------------------------------------------------------------------------
# Name:         vNetXmlSynchProc.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vNetXmlSynchProc.py,v 1.13 2007/10/25 11:01:18 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import wx
import os,codecs,string,time

from vidarc.tool.xml.vtXmlDom import vtXmlDom
from vidarc.tool.InOut.vtInOutCleanUp import vtInOutCleanUpByTime

import vidarc.tool.log.vtLog as vtLog

VERBOSE=0

class vNetXmlSynchProc(vtXmlDom):
    def __init__(self,netDoc2Synch,PSEUDO_XML=False):
        if VERBOSE:
            vtLog.CallStack('')
        self.PSEUDO_XML=PSEUDO_XML
        if self.PSEUDO_XML:
            self.sid=0
            self.outf=None
            self.sFN=None
        else:
            vtXmlDom.__init__(self,attr='sid',appl='vNetXmlSynchProc',audit_trail=False)
        self.netDoc=netDoc2Synch
        self.nodeData=None
        self.nodeLastProcResp=None
        self.nodeLastProcDel=None
        self.New(netDoc2Synch)
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        #vtLog.CallStack('')
        self.netDoc=None
        self.nodeData=None
        self.nodeLastProcResp=None
        self.nodeLastProcDel=None
    def New(self,netDoc2Synch,bThread=False):
        if self.PSEUDO_XML:
            self.sid=0
        else:
            vtXmlDom.New(self,revision='1.0',root="synch_proc",bConnection=False)
        if netDoc2Synch is not None:
            dt=wx.DateTime.Now()
            
            dn,fn=os.path.split(netDoc2Synch.GetFN())
            vtInOutCleanUpByTime(dn,fn+'_*',keep=2)
                
            self.sFN=netDoc2Synch.GetFN()+'_'+dt.Format('%Y%m%d')+'_'+dt.Format('%H%M%S')+'.%03d'%(int((time.clock()%1)*1000))+'.xml'
            #self.sFN=netDoc2Synch.GetFN()+'_'+dt.Format('%Y%m%d')+'_'+dt.Format('%H%M%S')+'.xml'
            if VERBOSE:
                vtLog.CallStack(self.sFN)
            if self.PSEUDO_XML:
                try:
                    sDN,sFN=os.path.split(self.sFN)
                    os.makedirs(sDN)
                except:
                    pass
                self.outf=codecs.open(self.sFN,'w','ISO-8859-1')
                self.outf.write("""<?xml version="1.0" encoding="ISO-8859-1"?>
<synch_proc>
  <create>
    <date>%s</date>
    <time>%s</time>
    <appl>%s</appl>
  </create>
  <data>
"""%(dt.FormatISODate(),dt.FormatISOTime(),netDoc2Synch.appl))
                try:
                    self.iSkip=string.find(self.netDoc.GetNodeXmlContent(self.netDoc.getBaseNode()),'>')
                    self.iSkip+=1
                except:
                    self.iSkip=0
            else:
                rootNode=self.getRoot()
                lst=[]
                lst.append({'tag':'date','val':dt.FormatISODate()})
                lst.append({'tag':'time','val':dt.FormatISOTime()})
                lst.append({'tag':'appl','val':netDoc2Synch.appl})
                #lst.append({'tag':'client','val':self.sClient})
                self.createChildByLst(rootNode,'create',lst,False)
                c=self.createSubNode(rootNode,'data',False)
                self.nodeData=c
                #node=self.doc.cloneNode(self.selNode,True)
                #docExp.removeInternalNodes(node)
                #self..appendChild(c,node)
                    
                self.AlignDoc()
                #self.Save()
                #self.Close()
                #del docExp
    def Save(self,bThread=False):
        if self.PSEUDO_XML:
            pass
        else:
            vtXmlDom.Save(self)
    def Close(self,bAcquire=True,bThread=False):
        if self.PSEUDO_XML:
            if self.outf is not None:
                self.outf.write("""    </data>
</synch_proc>""")
                self.outf.close()
                self.outf=None
        else:
            vtXmlDom.Close(self,bAcquire=bAcquire)
    def getBaseNode(self):
        if self.PSEUDO_XML:
            return None
        else:
            if self.root is None:
                return None
            return self.getChild(self.root,'data')
    def __clone__(self,node,bIncludeNodes=False):
        bIncludeNodes=False
        tn=self.netDoc.cloneNode(node,False)
        self.netDoc.__buildXmlDoc__(tn,node,bIncludeNodes)
        return tn
    def AddProcIdentical(self,iAct,id,sAction,iRes,idPar=-2):
        try:
            sID=self.netDoc.ConvertIdNum2Str(long(id))
        except:
            sID='-1'
        if self.PSEUDO_XML:
            self.outf.write("""    <synch sid="%08d">
      <action>%s</action>
      <act>%s</act>
      <name>%s</name>
      <result>%d</result>
"""%(self.sid,sAction,str(iAct),sID,iRes))
            self.outf.write("""    </synch>\n""")
            self.sid+=1
        else:
            c=self.createSubNode(self.nodeData,'synch')
            self.checkId(c)
            self.processMissingId()
            self.createSubNodeText(c,'action','response')
            self.createSubNodeText(c,'act',str(iAct))
            self.createSubNodeText(c,'name',sID)
            self.nodeLastProcResp=c
        
    def AddProcResponse(self,iAct,localNode,remoteNode,sAction,iRes,idPar=-2):
        if localNode is not None:
            id=self.netDoc.getKey(localNode)
        elif remoteNode is not None:
            id=self.netDoc.getKey(remoteNode)
        else:
            id=-1
        try:
            sID=self.netDoc.ConvertIdNum2Str(long(id))
        except:
            sID='-1'
        if self.PSEUDO_XML:
            self.outf.write("""    <synch sid="%08d">
      <action>%s</action>
      <act>%s</act>
      <name>%s</name>
      <result>%d</result>
"""%(self.sid,sAction,str(iAct),sID,iRes))
            if idPar>-2:
                self.outf.write("""      <idPar>%s</idPar>\n"""%str(idPar))
                self.outf.write("""      <id>%s</id>\n"""%str(id))
            self.sid+=1
            if localNode is not None:
                self.outf.write(u"""      <local>"""+self.netDoc.GetNodeXmlContent(localNode)[self.iSkip:].decode('ISO-8859-1')+u"""</local>\n""")
            else:
                self.outf.write("""      <local/>\n""")
            if remoteNode is not None:
                self.outf.write("""      <remote>%s</remote>\n"""%self.netDoc.GetNodeXmlContent(remoteNode)[self.iSkip:].decode('ISO-8859-1'))
            else:
                self.outf.write("""      <remote/>\n""")
            self.outf.write("""    </synch>\n""")
            self.sid+=1
        else:
            c=self.createSubNode(self.nodeData,'synch')
            self.checkId(c)
            self.processMissingId()
            self.createSubNodeText(c,'action','response')
            self.createSubNodeText(c,'act',str(iAct))
            self.createSubNodeText(c,'name',sID)
            if idPar>-2:
                self.createSubNodeText(c,'idPar',str(idPar))
                self.createSubNodeText(c,'id',str(id))
            l=self.createSubNode(c,'local')
            r=self.createSubNode(c,'remote')
            if localNode is not None:
                l.addChild(self.__clone__(localNode,True))
            if remoteNode is not None:
                r.addChild(self.__clone__(remoteNode,True))
            self.nodeLastProcResp=c
    def SetProcResponseEditResult(self,iRes):
        if self.nodeLastProcResp is not None:
            self.setNodeText(self.nodeLastProcResp,'action','edit')
            self.setNodeText(self.nodeLastProcResp,'edit_result',str(iRes))
    def SetProcResponseAdd(self,iRes):
        if self.nodeLastProcResp is not None:
            #self.setNodeText(self.nodeLastProcResp,'action','edit')
            self.setNodeText(self.nodeLastProcResp,'add','1')
    def SetProcResponseIdentical(self,iRes):
        if self.nodeLastProcResp is not None:
            self.setNodeText(self.nodeLastProcResp,'action','identical')
            self.setNodeText(self.nodeLastProcResp,'identical','1')
    def SetProcResponseEditOffline(self,iRes):
        if self.nodeLastProcResp is not None:
            self.setNodeText(self.nodeLastProcResp,'action','edit_offline')
            #self.setNodeText(self.nodeLastProcResp,'identical','1')
    def DelProc(self,iAct,localNode):
        c=self.createSubNode(self.nodeData,'synch')
        self.checkId(c)
        self.processMissingId()
        self.createSubNodeText(c,'action','del')
        self.createSubNodeText(c,'act',str(iAct))
        if localNode is not None:
            id=self.netDoc.getKey(localNode)
        else:
            id=-1
        self.createSubNodeText(c,'name',self.netDoc.ConvertIdNum2Str(long(id)))
        l=self.createSubNode(c,'local')
        if localNode is not None:
            l.addChild(self.__clone__(localNode,True))
        self.nodeLastProcDel=c
    def SetProcDelResult(self,iRes):
        if self.nodeLastProcDel is not None:
            self.setNodeText(self.nodeLastProcDel,'del_result',str(iRes))
    def AddProc(self,iAct,idPar,localNode,iRes):
        c=self.createSubNode(self.nodeData,'synch')
        self.checkId(c)
        self.processMissingId()
        self.createSubNodeText(c,'action','add')
        self.createSubNodeText(c,'act',str(iAct))
        if localNode is not None:
            id=self.netDoc.getKey(localNode)
        else:
            id=-1
        self.createSubNodeText(c,'name',self.netDoc.ConvertIdNum2Str(long(id)))
        self.createSubNodeText(c,'idPar',str(idPar))
        l=self.createSubNode(c,'remote')
        if localNode is not None:
            l.addChild(self.__clone__(localNode,True))
        self.setNodeText(c,'add_result',str(iRes))
    def AddOfflineProc(self,iAct,localNode,iRes):
        c=self.createSubNode(self.nodeData,'synch')
        self.checkId(c)
        self.processMissingId()
        self.createSubNodeText(c,'action','add_offline')
        self.createSubNodeText(c,'act',str(iAct))
        if localNode is not None:
            id=self.netDoc.getKey(localNode)
        else:
            id=-1
        self.createSubNodeText(c,'name',self.netDoc.ConvertIdNum2Str(long(id)))
        l=self.createSubNode(c,'local')
        if localNode is not None:
            l.addChild(self.__clone__(localNode,True))
        self.setNodeText(c,'add_offline_result',str(iRes))
    def MoveProc(self,iAct,idPar,id,localNode,iRes):
        c=self.createSubNode(self.nodeData,'synch')
        self.checkId(c)
        self.processMissingId()
        self.createSubNodeText(c,'action','move')
        self.createSubNodeText(c,'act',str(iAct))
        if localNode is not None:
            id=self.netDoc.getKey(localNode)
        else:
            id=-1
        self.createSubNodeText(c,'name',self.netDoc.ConvertIdNum2Str(long(id)))
        self.createSubNodeText(c,'idPar',str(idPar))
        self.createSubNodeText(c,'id',str(id))
        l=self.createSubNode(c,'local')
        if localNode is not None:
            l.addChild(self.__clone__(localNode,True))
        self.setNodeText(c,'move_result',str(iRes))
    
