#----------------------------------------------------------------------------
# Name:         vtNetTransactionProcThread.py
# Purpose:
# Author:       Walter Obweger
#
# Created:      20061130
# CVS-ID:       $Id: vtNetTransactionProcThread.py,v 1.15 2010/03/28 20:27:42 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.vtThread import vtThread
from vidarc.tool.vtThreadEndless import vtThreadEndless
#from vidarc.tool.vtSlotKeyBuffer import vtSlotKeyBuffer
from vidarc.tool.net.vtNetTransaction import vtNetTransaction
from vidarc.tool.net.vNetXmlWxGuiEvents import *
from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer
import thread,time,types

VERBOSE=0

wxEVT_VT_NET_TRANSACTION_PROC_THREAD_PROC=wx.NewEventType()
vEVT_VT_NET_TRANSACTION_PROC_THREAD_PROC=wx.PyEventBinder(wxEVT_VT_NET_TRANSACTION_PROC_THREAD_PROC,1)
def EVT_VT_NET_TRANSACTION_PROC_THREAD_PROC(win,func):
    win.Connect(-1,-1,wxEVT_VT_NET_TRANSACTION_PROC_THREAD_PROC,func)
def EVT_VT_NET_TRANSACTION_PROC_THREAD_PROC_DISCONNECT(win,func):
    win.Disconnect(-1,-1,wxEVT_VT_NET_TRANSACTION_PROC_THREAD_PROC,func)
class vtNetTransactionProcThreadProc(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VT_NET_TRANSACTION_PROC_THREAD_PROC(<widget_name>, self.OnProc)
    """
    def __init__(self,iVal,iCount=-1):
        wx.PyEvent.__init__(self)
        self.val=iVal
        self.count=iCount
        self.SetEventType(wxEVT_VT_NET_TRANSACTION_PROC_THREAD_PROC)
    def GetValue(self):
        return self.val
    def GetCount(self):
        return self.count

wxEVT_VT_NET_TRANSACTION_PROC_THREAD_FINISHED=wx.NewEventType()
vEVT_VT_NET_TRANSACTION_PROC_THREAD_FINISHED=wx.PyEventBinder(wxEVT_VT_NET_TRANSACTION_PROC_THREAD_FINISHED,1)
def EVT_VT_NET_TRANSACTION_PROC_THREAD_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_VT_NET_TRANSACTION_PROC_THREAD_FINISHED,func)
def EVT_VT_NET_TRANSACTION_PROC_THREAD_FINISHED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,wxEVT_VT_NET_TRANSACTION_PROC_THREAD_FINISHED,func)
class vtNetTransactionProcThreadFinished(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VT_NET_TRANSACTION_PROC_THREAD_FINISHED(<widget_name>, self.OnFinished)
    """
    def __init__(self):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VT_NET_TRANSACTION_PROC_THREAD_FINISHED)

wxEVT_VT_NET_TRANSACTION_PROC_THREAD_ABORTED=wx.NewEventType()
vEVT_VT_NET_TRANSACTION_PROC_THREAD_ABORTED=wx.PyEventBinder(wxEVT_VT_NET_TRANSACTION_PROC_THREAD_ABORTED,1)
def EVT_VT_NET_TRANSACTION_PROC_THREAD_ABORTED(win,func):
    win.Connect(-1,-1,wxEVT_VT_NET_TRANSACTION_PROC_THREAD_ABORTED,func)
def EVT_VT_NET_TRANSACTION_PROC_THREAD_ABORTED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,wxEVT_VT_NET_TRANSACTION_PROC_THREAD_ABORTED,func)
class vtNetTransactionProcThreadAborted(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VT_NET_TRANSACTION_PROC_THREAD_ABORTED(<widget_name>, self.OnAborted)
    """
    def __init__(self):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VT_NET_TRANSACTION_PROC_THREAD_ABORTED)

class vtNetTransactionProcThread(vtThread,vtXmlDomConsumer):
    def __init__(self,par,bPost=False,zTimeOut=100,szBuffer=10,zSleep=0.3,
            verbose=0,origin=None,name='vtNetTransactionProcThread'):
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        vtThread.__init__(self,par,bPost=bPost,verbose=verbose,origin=origin)
        vtXmlDomConsumer.__init__(self)
        self.thdExec=vtThreadEndless(None,bPost=False,verbose=verbose-1,
                origin=':'.join([self.GetOrigin(),'thdExec']))
        self.zTimeOut=zTimeOut
        self.zSleep=zSleep
        self.bTimeOut=False
        self.iAct=0
        self.iCount=0
        self.name=name
        try:
            self.widLogging=vtLog.GetPrintMsgWid(self.par)
        except:
            self.widLogging=None
        ##self.iTransIDAct=0
        ##self.iTransID=0
        self.sbTrans=vtNetTransaction(szBuffer)#vtSlotKeyBuffer(szBuffer)
    def __del__(self):
        #self.SetDoc(None)
        pass
    def Clear(self):
        self.iAct=0
        self.iCount=0
    def SetDoc(self,doc):
        if self.doc is not None:
            EVT_NET_XML_ADD_NODE_RESPONSE_DISCONNECT(self.doc,self.OnAddResponse)
            EVT_NET_XML_LOCK_DISCONNECT(self.doc,self.OnLock)
            EVT_NET_XML_SET_NODE_DISCONNECT(self.doc,self.OnSetNodeResponse)
        vtXmlDomConsumer.SetDoc(self,doc)
        if self.doc is not None:
            EVT_NET_XML_ADD_NODE_RESPONSE(self.doc,self.OnAddResponse)
            EVT_NET_XML_LOCK(self.doc,self.OnLock)
            EVT_NET_XML_SET_NODE(self.doc,self.OnSetNodeResponse)
    def DoProc(self,dRes,bChild=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,self.__str__(),self.GetOrigin())
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'dRes:%s'%(vtLog.pformat(dRes)),self.GetOrigin())
        self.Do(self._procFind,dRes,bChild)
    def _procCheck(self,node,dType,dData):
        if self.verbose:
            
            
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'dData:%s'%(vtLog.pformat(dData)),self.origin)
        try:
            sTagName=self.doc.getTagName(node)
            if sTagName in dType:
                lFilters=dType[sTagName]
                # 070310:wro  check filters
                if lFilters is None:
                    return 0
                if len(lFilters)==0:
                    return 0
                # update filter
                
                #sFltName=':'.join([self.origin,kFlt])
                bProc=False
                self.doc.acquire()
                try:
                    def conv(node,attr,func):
                        #if type(attr)==types.TupleType:
                        #    pass
                        #else:
                        val=self.doc.getNodeInfoVal(node,attr)
                        if func is None:
                            return val.strip()
                        return func(val)
                    k=(sTagName,tuple([conv(node,attr,func) for attr,func in lFilters]))
                    #print k
                    if k in dData:
                        #print '  gotta'
                        d=dData[k]
                        if '__' in d:
                            dd=d['__']
                            bProc=True
                            if '__' in dd:
                                if dd['__']==False:
                                    bProc=False
                            if bProc:
                                id=self.doc.getKeyNum(node)
                                self.__Do__(self.DoEdit,id,d,dType)
                            #else:
                                #self._addDict(id,dData[k])
                                dd['__']=id
                                #for kk in d.keys():
                                    #if kk!='__':
                                #self.doc.release()
                                #self.doc.procChildsKeysRec(0,node,self._procCheck,dType,d)
                                del dData[k]
                    #else:
                    #    self.doc.release()
                    #self.doc.matchFilters(node,sFltName)
                except:
                    vtLog.vtLngTB(self.origin)
                self.doc.release()
        except:
            vtLog.vtLngTB(self.origin)
            return -1
        #self.doc.procChildsKeysRec(0,node,self._procCheck,dType,dData)
        return 0
    def _procFind(self,dRes,bChild):
        id=dRes['id']
        if self.verbose:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'dRes:%s'%(vtLog.pformat(dRes)),self.origin)
        self.doc.acquire('dom')
        try:
            dType=dRes['type']
            #for kFlt,dFilters in dType.items():
            #    sFltName=':'.join([self.origin,kFlt])
            #    self.doc.setFilter(dFilters,sFltName)
            node=self.doc.getNodeByIdNum(id)
            if node is None:
                vtLog.vtLngCur(vtLog.ERROR,'id:%08d'%(id),self.origin)
                return
            dData=dRes['data']
            #calc count
            if self.bPost:
                vtLog.PrintMsg(_('%s:calculate count ...')%self.name,self.widLogging)
            self.iCount=0
            self.iAct=0
            def walkDict(d):
                for k in d.keys():
                    if k=='__':
                        continue
                    else:
                        self.iCount+=1
                        walkDict(d[k])
            walkDict(dData)
            if self.bPost:
                vtLog.PrintMsg(_('%s:calculate count finished.')%self.name,self.widLogging)
                wx.PostEvent(self.par,vtNetTransactionProcThreadProc(0,self.iCount))
            if bChild:
                self.doc.procChildsKeysRec(0,node,
                        self._procCheck,dType,dData)
            else:
                k=dData.keys()[0]
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'k:%s;keys:%s'%(k,
                                vtLog.pformat(dData.keys())),self)
                d=dData[k]
                if '__' in d:
                    dd=d['__']
                    self.__Do__(self.DoEdit,id,d,dType)
                dData=dData[k]
                self.doc.procChildsKeysRec(0,node,
                        self._procCheck,dType,dData)
                if self.verbose:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'dData:%s'%(vtLog.pformat(dData)),self.origin)
            #100327 self._addDict(id,dData)     # 090429:wro needed to add
                #self.doc.procKeysRec(100,node,
                #        self._procCheck,dType,dData)
        except:
            vtLog.vtLngTB(self.origin)
        self.doc.release('dom')
        if self.verbose:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'dData:%s'%(vtLog.pformat(dRes)),self.origin)
    def _addDict(self,id,dData):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'id:%08d;dData;%s'%(id,vtLog.fmtLimit(dData)),self.origin)
        keys=dData.keys()
        keys.sort()
        #keys.remove('__')      # 070310:wro may cause exception
        for k in keys:
            if k=='__':
                continue        # 070310:wro skip processing
            d=dData[k]
            if '__' in d:
                dd=d['__']
                if '__' in dd:
                    if dd['__']==True:
                        ##dd['__']=self.iTransIDAct
                        ##if hasattr(self.doc,'GetReg'):
                        ##    oReg=self.doc.GetReg(k[0])
                        ##else:
                        ##    oReg=None
                        ##self.__Do__(self.DoAdd,id,d,oReg)
                        self.__Do__(self.DoAdd,id,k[0],d)
                        #self.thdExec.Do(self.DoAdd,id,d,oReg)
                    #else:
                    #    self._addDict(d['__']['__'],d)
                    #    pass
    def DoEdit(self,id,d,dType):
        self.iAct+=1
        if self.bPost:
            vtLog.PrintMsg(_('%s:process (%05d - %05d) edit ...')%(self.name,self.iAct,self.iCount),self.widLogging)
            wx.PostEvent(self.par,vtNetTransactionProcThreadProc(self.iAct,self.iCount))
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'id:%08d d;%s'%(id,vtLog.fmtLimit(d)),self.origin)
        vtLog.vtLngCur(vtLog.INFO,'id:%08d;edit'%(id),self.origin)
        self.doc.acquire('dom')
        try:
            dd=None
            node=self.doc.getNodeByIdNum(id)
            if node is not None:
                if '__' in d:
                    dd=d['__']
                self.doc.procChildsKeysRec(0,node,self._procCheck,dType,d)
                self._addDict(id,d)
            else:
                vtLog.vtLngCur(vtLog.INFO,'id:%08d;node not found for edit'%(id),self.origin)
        except:
            vtLog.vtLngTB(self.origin)
        self.doc.release('dom')
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'id:%08d;proc'%(id),self.origin)
        try:
            if dd is not None:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'id:%08d;push'%(id),self.origin)
                    iPos=self.sbTrans.put(id,dd,cmd='set',blocking=True)
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'id:%08d d:%s;iPos:%d'%(id,
                                vtLog.pformat(d),iPos),self.origin)
                    if iPos>=0:
                        self.doc.startEdit(node)
                    else:
                        vtLog.vtLngCur(vtLog.CRITICAL,'id:%08d d:%s;iPos:%d'%(id,
                                vtLog.pformat(d),iPos),self.origin)
        except:
            vtLog.vtLngTB(self.origin)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'id:%08d;done'%(id),self.origin)
    def _doEdit(self,id,d):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'id:%08d d:%s'%(id,vtLog.fmtLimit(d)),self.origin)
        self.doc.acquire('dom')
        try:
            node=self.doc.getNodeByIdNum(id)
            #if '__' in d:
            #    dd=d['__']
            dd=d
            for k in dd.keys():
                if k!='__':
                    if type(k)==types.TupleType:
                        self.doc.setNodeTextAttr(node,k[0],dd[k],k[1],k[2])
                    else:
                        self.doc.setNodeText(node,k,dd[k])
        except:
            vtLog.vtLngTB(self.origin)
        self.doc.release('dom')
        self.doc.doEdit(node)
        self.doc.endEdit(node)
    ##def DoAdd(self,id,d,oReg):
    def DoAdd(self,id,sTagName,d):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'id:%08d d:%s'%(id,vtLog.fmtLimit(d)),self.origin)
        self.iAct+=1
        if self.bPost:
            vtLog.PrintMsg(_('%s:process (%05d - %05d) add ...')%(self.name,self.iAct,self.iCount),self.widLogging)
            wx.PostEvent(self.par,vtNetTransactionProcThreadProc(self.iAct,self.iCount))
        try:
            node=self.doc.getNodeByIdNum(id)
            if hasattr(self.doc,'GetReg'):
                oReg=self.doc.GetReg(sTagName)
            else:
                oReg=None
            if oReg is not None:
                c=oReg.Create(None)#,self._doSet,sName,sType,obj)
            else:
                return
            if '__' in d:
                dd=d['__']
                #dd=d
                bLogDbg=vtLog.vtLngIsLogged(vtLog.DEBUG)
                for k in dd.keys():
                    if k=='__':
                        continue
                    if bLogDbg:
                        vtLog.vtLngCur(vtLog.DEBUG,'k:%s;dd:%s'%(k,
                                vtLog.pformat(dd[k])),self.origin)
                    if type(k)==types.TupleType:
                        self.doc.setNodeTextAttr(c,k[0],dd[k],k[1],k[2])
                    else:
                        self.doc.setNodeText(c,k,dd[k])
            #return self.doc.getKeyNum(c),node,c,self._doAddResp,(ti,),{}
            self.doc.appendChild(node,c)
            self.doc.AlignNode(c)
            self.doc.addNode(node,c,self._addTrans,
                            tup=(self._addDict,(d,),{}))
        except:
            vtLog.vtLngTB(self.origin)
    def Do(self,func,*args,**kwargs):
        if self.verbose:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'status:%s;func;%s,args:%s;kwargs:%s'%(self.__str__(),func,`args`,`kwargs`),
                            self.origin)
        ##self.qSched.put((self.iTransIDAct,func,args,kwargs))
        ##self.iTransIDAct+=1
        vtThread.Do(self,func,*args,**kwargs)
    def __Do__(self,func,*args,**kwargs):
        #self.Do(func,*args,**kwargs)
        self.thdExec.Do(func,*args,**kwargs)
    def Pause(self):
        vtThread.Pause(self)
        self.thdExec.Pause()
    def Resume(self):
        vtThread.Resume(self)
        self.thdExec.Resume()
    def Restart(self):
        vtThread.Start(self)
        self.thdExec.Start()
    def Start(self):
        vtThread.Start(self)
        self.thdExec.Start()
    def Stop(self):
        vtThread.Stop(self)
        self.thdExec.Stop()
    def IsPause(self):
        if vtThread.IsPause(self):
            return True
        if self.thdExec.Pause():
            return True
        return False
    def IsPaused(self):
        if vtThread.IsPaused(self):
            return self.thdExec.IsPaused()
        return False
    def Is2Stop(self):
        if self.keepGoing==False:
            return True
        if self.thdExec.Is2Stop():
            return True
        return False
    def IsRunning(self):
        if self.running:
            return True
        if self.thdExec.IsRunning():
            return True
        return False
    def _addTrans(self,par,node,tup=None):
        #iTransID,f,a,kw=tup
        func,args,kwargs=tup
        d=args[0]
        #print args,kwargs
        idChild=self.doc.getKeyNum(node)
        iPos=self.sbTrans.put(idChild,(func,args,kwargs),blocking=True)
        if '__' in d:
            dd=d['__']
        else:
            dd={}
        dd['__']=self.sbTrans.getTransID(iPos)
    def __str__(self):
        s=vtThread.__str__(self)
        self._acquire()
        try:
            s=','.join([s,'isFill:%d'%self.sbTrans.isFree(),
                    'thdExec:IsEmpty:%d'%self.thdExec.IsEmpty(),';',
                    self.thdExec.__str__()])
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self._release()
        return s
    #def Run(self):
    def __run__(self):
        vtLog.vtLngCur(vtLog.INFO,self.__str__(),self.GetOrigin())
        self._acquire()
        self.running=True
        self.bTimeOut=False
        self._release()
        try:
            self.__runStart__()
            if self.verbose:
                vtLog.vtLngCur(vtLog.DEBUG,self.__str__(),self.origin)
            self._acquire()
            self.zTime=0.0
            zNotify=0.0
            self._release()
            #while self.keepGoing:
            while self.Is2Stop()==False:
                if self.verbose:
                    vtLog.vtLngCur(vtLog.DEBUG,self.__str__(),self.origin)
                while (self.qSched.empty()==False) and self.sbTrans.isFree():
                    if self.verbose:
                        vtLog.vtLngCur(vtLog.DEBUG,'empty:%d,free:%d'%(self.qSched.empty(),self.sbTrans.isFree()),self.GetOrigin())
                    tup=self.qSched.get(0)
                    ##iTransID,func,args,kwargs=tup
                    func,args,kwargs=tup
                    try:
                        func(*args,**kwargs)
                    except:
                        vtLog.vtLngTB(self.origin)
                    #time.sleep(0.05)
                if self.qSched.empty():
                    if self.sbTrans.isFill()==False:
                        if self.thdExec.IsEmpty()==True:
                            break
                #self.sbTrans.findTimeOut(self.zTimeOut)
                self._cleanUpTimeOut()
                ##self.zTime+=self.zSleep
                ##zNotify+=self.zSleep
                ##if self.zTime>self.zTimeOut:
                    #self.keepGoing=False
                    #self.bAbort=True
                ##    vtLog.vtLngCur(vtLog.ERROR,'time out %3.1f %3.1f [s];%s'%\
                ##                    (self.zTime,self.zTimeOut,self.sbTrans),self.origin)
                    #if self.bUpdate:
                    #    vtLog.PrintMsg(_(u'alias:%s id:%s wait for lock time out %3.1f [s]')%(self.doc.appl,id,self.zTime),self.widLogging)
                    #self.doc.endEdit(node)
                    #self.__next__()
                ##    self.zTime=0.0
                ##    zNotify=0.0
                ##if zNotify>=2.0:
                    #node=self.__getInfo__()
                    #id=self.doc.getKey(node)
                    #vtLog.vtLngCur(vtLog.DEBUG,'id:%s wait for lock time %3.1f %3.1f [1s]'%\
                    #                (id,self.zTime,self.zTimeOut),self.doc.GetOrigin())
                ##    zNotify=0.0
                time.sleep(self.zSleep)
            self._acquire()
            self.running=False
            if self.bPost:
                vtLog.PrintMsg(_(u'alignment ...'),self.widLogging)
            ##self.doc.acquire('dom')
            ##self.doc.AlignDoc()
            ##self.doc.release('dom')
            if self.bPost:
                vtLog.PrintMsg(_(u'alignment finished.'),self.widLogging)
            
            if self.keepGoing==False:
                while self.qSched.empty()==False:
                    t=self.qSched.get(0)
        except:
            self._acquire()
            #self.keepGoing=
            vtLog.vtLngTB(self.origin)
        self.__runEnd__()
        self.running=False
        if self.bPost:
            if self.keepGoing and self.bTimeOut==False:
                wx.PostEvent(self.par,vtNetTransactionProcThreadFinished())
                vtLog.PrintMsg(_('%s finished.')%self.name,self.widLogging)
            else:
                wx.PostEvent(self.par,vtNetTransactionProcThreadAborted())
                vtLog.PrintMsg(_('%s aborted.')%self.name,self.widLogging)
        self.keepGoing=self.running=False
        self._release()
        vtLog.vtLngCur(vtLog.DEBUG,self.__str__(),self.origin)
    def OnLock(self,evt):
        evt.Skip()
        if self.verbose:
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(evt.GetID()),self.origin)
        try:
            id=long(evt.GetID())
            iPos=self.sbTrans.find(id)
            if iPos>=0:
                # 070310:wro
                resp=evt.GetResponse()
                if vtLog.vtLngIsLogged(vtLog.INFO):
                    vtLog.vtLngCur(vtLog.INFO,'id:%08d;resp:%s'%(id,resp),self.origin)
                if self.sbTrans.next(id,resp):
                    tup=self.sbTrans.get(iPos)
                    try:
                        #self.__Do__(self._doEdit,id,tup)
                        #self._doEdit(id,tup)
                        self.doc.thdProcNet.Do(self._doEdit,id,tup)
                    except:
                        vtLog.vtLngTB(self.doc.GetOrigin())
                else:
                    tup=self.sbTrans.pop(iPos)
            if 0:     #070310:wro
                tup=self.sbTrans.get(iPos)
                self.zTime=0.0
                
                resp=evt.GetResponse()
                if resp in  ['ok','already locked']:
                    try:
                        #iTransID,cmd,id,func,args,kwargs=tup
                        self.__Do__(self._doEdit,id,tup)
                        #func(id,*args,**kwargs)
                        #self.thdExec.Do(func,id,*args,**kwargs)
                    except:
                        vtLog.vtLngTB(self.doc.GetOrigin())
                else:
                    # not locked
                    tup=self.sbTrans.pop(iPos)
                #self.__next__()
        except:
            vtLog.vtLngTB(self.origin)
        self._cleanUpTimeOut()
    def OnSetNodeResponse(self,evt):
        evt.Skip()
        if self.verbose:
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(evt.GetID()),self.origin)
        try:
            id=long(evt.GetID())
            iPos=self.sbTrans.find(id)
            if iPos>=0:
                # 070310:wro
                resp=evt.GetResponse()
                if vtLog.vtLngIsLogged(vtLog.INFO):
                    vtLog.vtLngCur(vtLog.INFO,'id:%08d;resp:%s'%(id,resp),self.origin)
                if self.sbTrans.next(id,resp):
                    pass
                else:
                    tup=self.sbTrans.pop(iPos)
            if 0:     #070310:wro
                tup=self.sbTrans.get(iPos)
                resp=evt.GetResponse()
                if vtLog.vtLngIsLogged(vtLog.INFO):
                    vtLog.vtLngCur(vtLog.INFO,'id:%08d;resp:%s'%(id,resp),self.origin)
                tup=self.sbTrans.pop(iPos)
        except:
            vtLog.vtLngTB(self.origin)
        self._cleanUpTimeOut()
    def OnAddResponse(self,evt):
        evt.Skip()
        if self.verbose:
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(evt.GetID()),self.origin)
        try:
            id=long(evt.GetID())
            iPos=self.sbTrans.find(id)
            if iPos>=0:
                # 070310:wro
                resp=evt.GetResponse()
                if vtLog.vtLngIsLogged(vtLog.INFO):
                    vtLog.vtLngCur(vtLog.INFO,'id:%08d;resp:%s'%(id,resp),self.origin)
                if self.sbTrans.next(id,resp):
                    pass
                tup=self.sbTrans.get(iPos)
                #self.zTime=0.0
                if resp in  ['ok']:
                    try:
                        func,args,kwargs=tup
                        #print args,kwargs
                        idNew=long(evt.GetNewID())
                        func(idNew,*args,**kwargs)
                        #self.thdExec.Do(funcid,idNew,*args,**kwargs)
                    except:
                        vtLog.vtLngTB(self.origin)
                tup=self.sbTrans.pop(iPos)
        except:
            vtLog.vtLngTB(self.origin)
        self._cleanUpTimeOut()
    def _cleanUpTimeOut(self):
        l=self.sbTrans.clearTimeOut(self.zTimeOut)
        if l is not None:
            for d in l:
                vtLog.vtLngCur(vtLog.INFO,
                        'time out;id:%08d;zDT:%s;transID:%d;info:%s'%\
                        (d['key'],d['time'],d['transID'],
                        vtLog.pformat(d['info'])),
                        self.origin)
                vtLog.PrintMsg(_('id:%08d time out.')%d['key'],self.widLogging)
            self.bTimeOut=True
