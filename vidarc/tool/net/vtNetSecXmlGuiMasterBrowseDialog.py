#Boa:Dialog:vtNetSecXmlGuiMasterBrowseDialog
#----------------------------------------------------------------------------
# Name:         vtNetSecXmlGuiMasterBrowseDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060416
# CVS-ID:       $Id: vtNetSecXmlGuiMasterBrowseDialog.py,v 1.10 2007/07/29 09:56:50 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import vidarc.tool.net.vtNetSecXmlGuiMasterBrowsePanel

from vidarc.tool.net.vtNetSecXmlGuiMasterBrowsePanel import *

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

import vidarc.tool.net.img_gui as img_gui
import vidarc.tool.net.images as img_img

def create(parent):
    return vtNetSecXmlGuiMasterBrowseDialog(parent)

wxEVT_VTSEC_XML_MASTER_BROWSE_DIALOG_CFG=wx.NewEventType()
vEVT_VTSEC_XML_MASTER_BROWSE_DIALOG_CFG=wx.PyEventBinder(wxEVT_VTSEC_XML_MASTER_BROWSE_DIALOG_CFG,1)
def EVT_VTSEC_XML_MASTER_BROWSE_DIALOG_CFG(win,func):
    win.Connect(-1,-1,wxEVT_VTSEC_XML_MASTER_BROWSE_DIALOG_CFG,func)
class vtSecXmlMasterBrowseDialogCfg(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTSEC_XML_MASTER_BROWSE_DIALOG_CFG(<widget_name>, self.OnCfg)
    """

    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VTSEC_XML_MASTER_BROWSE_DIALOG_CFG)
        self.obj=obj
    def GetObject(self):
        return self.obj

wxEVT_VTSEC_XML_MASTER_BROWSE_DIALOG_MASTER=wx.NewEventType()
vEVT_VTSEC_XML_MASTER_BROWSE_DIALOG_MASTER=wx.PyEventBinder(wxEVT_VTSEC_XML_MASTER_BROWSE_DIALOG_MASTER,1)
def EVT_VTSEC_XML_MASTER_BROWSE_DIALOG_MASTER(win,func):
    win.Connect(-1,-1,wxEVT_VTSEC_XML_MASTER_BROWSE_DIALOG_MASTER,func)
class vtSecXmlMasterBrowseDialogMaster(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTSEC_XML_MASTER_BROWSE_DIALOG_MASTER(<widget_name>, self.OnMaster)
    """

    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VTSEC_XML_MASTER_BROWSE_DIALOG_MASTER)
        self.obj=obj
    def GetObject(self):
        return self.obj

wxEVT_VTSEC_XML_MASTER_BROWSE_DIALOG_LOG=wx.NewEventType()
vEVT_VTSEC_XML_MASTER_BROWSE_DIALOG_LOG=wx.PyEventBinder(wxEVT_VTSEC_XML_MASTER_BROWSE_DIALOG_LOG,1)
def EVT_VTSEC_XML_MASTER_BROWSE_DIALOG_LOG(win,func):
    win.Connect(-1,-1,wxEVT_VTSEC_XML_MASTER_BROWSE_DIALOG_LOG,func)
class vtSecXmlMasterBrowseDialogLog(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTSEC_XML_MASTER_BROWSE_DIALOG_LOG(<widget_name>, self.OnLog)
    """

    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VTSEC_XML_MASTER_BROWSE_DIALOG_LOG)
        self.obj=obj
    def GetObject(self):
        return self.obj

wxEVT_VTSEC_XML_MASTER_BROWSE_DIALOG_STATUS=wx.NewEventType()
vEVT_VTSEC_XML_MASTER_BROWSE_DIALOG_STATUS=wx.PyEventBinder(wxEVT_VTSEC_XML_MASTER_BROWSE_DIALOG_STATUS,1)
def EVT_VTSEC_XML_MASTER_BROWSE_DIALOG_STATUS(win,func):
    win.Connect(-1,-1,wxEVT_VTSEC_XML_MASTER_BROWSE_DIALOG_STATUS,func)
class vtSecXmlMasterBrowseDialogStatus(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTSEC_XML_MASTER_BROWSE_DIALOG_STATUS(<widget_name>, self.OnStatus)
    """

    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VTSEC_XML_MASTER_BROWSE_DIALOG_STATUS)
        self.obj=obj
    def GetObject(self):
        return self.obj

[wxID_VTNETSECXMLGUIMASTERBROWSEDIALOG, 
 wxID_VTNETSECXMLGUIMASTERBROWSEDIALOGCBCFG, 
 wxID_VTNETSECXMLGUIMASTERBROWSEDIALOGCBLOG, 
 wxID_VTNETSECXMLGUIMASTERBROWSEDIALOGCBMASTER, 
 wxID_VTNETSECXMLGUIMASTERBROWSEDIALOGCBSTATUS, 
 wxID_VTNETSECXMLGUIMASTERBROWSEDIALOGLNSEP, 
 wxID_VTNETSECXMLGUIMASTERBROWSEDIALOGPNBROWSE, 
] = [wx.NewId() for _init_ctrls in range(7)]

class vtNetSecXmlGuiMasterBrowseDialog(wx.Dialog):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.pnBrowse, 0, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT | wx.TOP)
        parent.AddWindow(self.lnSep, 1, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.TOP | wx.BOTTOM | wx.EXPAND)
        parent.AddSizer(self.bxsDlgBt, 0, border=4,
              flag=wx.ALIGN_CENTER | wx.BOTTOM | wx.TOP | wx.RIGHT | wx.LEFT)

    def _init_coll_bxsDlgBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbCfg, 0, border=4, flag=wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbLog, 0, border=4, flag=wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.cbStatus, 0, border=4, flag=wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbMaster, 0, border=4, flag=wx.RIGHT | wx.LEFT)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self.bxsDlgBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsDlgBt_Items(self.bxsDlgBt)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTNETSECXMLGUIMASTERBROWSEDIALOG,
              name=u'vtNetSecXmlGuiMasterBrowseDialog', parent=prnt,
              pos=wx.Point(301, 265), size=wx.Size(400, 346),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE,
              title=u'vtNetSecXmlBrowse Dialog')
        self.SetClientSize(wx.Size(392, 319))

        self.pnBrowse = vidarc.tool.net.vtNetSecXmlGuiMasterBrowsePanel.vtNetSecXmlGuiMasterBrowsePanel(id=wxID_VTNETSECXMLGUIMASTERBROWSEDIALOGPNBROWSE,
              name=u'pnBrowse', parent=self, pos=wx.Point(4, 4),
              size=wx.Size(384, 267), style=0)

        self.cbCfg = wx.lib.buttons.GenBitmapButton(id=wxID_VTNETSECXMLGUIMASTERBROWSEDIALOGCBCFG,
              bitmap=vtArt.getBitmap(vtArt.Settings), name=u'cbCfg',
              parent=self, pos=wx.Point(114, 285), size=wx.Size(31, 30),
              style=0)
        self.cbCfg.SetToolTipString(_(u'config'))
        self.cbCfg.Bind(wx.EVT_BUTTON, self.OnCbCfgButton,
              id=wxID_VTNETSECXMLGUIMASTERBROWSEDIALOGCBCFG)

        self.cbLog = wx.lib.buttons.GenBitmapButton(id=wxID_VTNETSECXMLGUIMASTERBROWSEDIALOGCBLOG,
              bitmap=vtArt.getBitmap(vtArt.Log), name=u'cbLog', parent=self,
              pos=wx.Point(161, 285), size=wx.Size(31, 30), style=0)
        self.cbLog.SetToolTipString(_(u'log'))
        self.cbLog.Bind(wx.EVT_BUTTON, self.OnCbLogButton,
              id=wxID_VTNETSECXMLGUIMASTERBROWSEDIALOGCBLOG)

        self.cbMaster = wx.lib.buttons.GenBitmapButton(id=wxID_VTNETSECXMLGUIMASTERBROWSEDIALOGCBMASTER,
              bitmap=vtArt.getBitmap(vtArt.Synch), name=u'cbMaster',
              parent=self, pos=wx.Point(247, 285), size=wx.Size(31, 30),
              style=0)
        self.cbMaster.SetToolTipString(_(u'master'))
        self.cbMaster.Bind(wx.EVT_BUTTON, self.OnCbMasterButton,
              id=wxID_VTNETSECXMLGUIMASTERBROWSEDIALOGCBMASTER)

        self.cbStatus = wx.lib.buttons.GenBitmapButton(id=wxID_VTNETSECXMLGUIMASTERBROWSEDIALOGCBSTATUS,
              bitmap=vtArt.getBitmap(vtArt.Link), name=u'cbStatus', parent=self,
              pos=wx.Point(200, 285), size=wx.Size(31, 30), style=0)
        self.cbStatus.SetToolTipString(_(u'status'))
        self.cbStatus.Bind(wx.EVT_BUTTON, self.OnCbStatusButton,
              id=wxID_VTNETSECXMLGUIMASTERBROWSEDIALOGCBSTATUS)

        self.lnSep = wx.StaticLine(id=wxID_VTNETSECXMLGUIMASTERBROWSEDIALOGLNSEP,
              name=u'lnSep', parent=self, pos=wx.Point(4, 275),
              size=wx.Size(384, 2), style=0)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        self._init_ctrls(parent)
        try:
            icon = wx.EmptyIcon()
            icon.CopyFromBitmap(vtArt.getBitmap(vtArt.Browse))
            self.SetIcon(icon)
        except:
            vtLog.vtLngTB(self.sOrigin)
        EVT_VTSEC_XML_MASTER_BROWSE_PANEL_OPEN(self.pnBrowse,self.OnOpen)
        EVT_VTSEC_XML_MASTER_BROWSE_PANEL_CONNECT(self.pnBrowse,self.OnConnect)
        EVT_VTSEC_XML_MASTER_BROWSE_PANEL_STOP(self.pnBrowse,self.OnStop)
        EVT_VTSEC_XML_MASTER_BROWSE_PANEL_CANCELED(self.pnBrowse,self.OnCanceled)
        EVT_VTSEC_XML_MASTER_BROWSE_PANEL_ADD(self.pnBrowse,self.OnAdd)
        EVT_VTSEC_XML_MASTER_BROWSE_PANEL_DEL(self.pnBrowse,self.OnDel)
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        pass
    def OnOpen(self,evt):
        wx.PostEvent(self,vtSecXmlMasterBrowsePanelOpen(self,evt.GetConnectionName()))
    def OnConnect(self,evt):
        wx.PostEvent(self,vtSecXmlMasterBrowsePanelConnect(self,evt.GetConnectionName()))
        #self.Show(False)
    def OnStop(self,evt):
        wx.PostEvent(self,vtSecXmlMasterBrowsePanelStop(self,evt.GetConnectionName()))
    def OnCanceled(self,evt):
        wx.PostEvent(self,vtSecXmlMasterBrowsePanelCanceled(self))
        self.Show(False)
    def OnAdd(self,evt):
        wx.PostEvent(self,vtSecXmlMasterBrowsePanelAdd(self))
        self.Show(False)
    def OnDel(self,evt):
        wx.PostEvent(self,vtSecXmlMasterBrowsePanelDel(self,evt.GetConnection()))
        #self.Show(False)

    def SetAppl(self,appl):
        self.pnBrowse.SetAppl(appl)
    def SetLocalDN(self,dn):
        self.pnBrowse.SetLocalDN(dn)
    def UpdateConnections(self,conns):
        self.pnBrowse.UpdateConnections(conns)

    def OnCbCfgButton(self, event):
        wx.PostEvent(self,vtSecXmlMasterBrowseDialogCfg(self))
        event.Skip()

    def OnCbLogButton(self, event):
        wx.PostEvent(self,vtSecXmlMasterBrowseDialogLog(self))
        event.Skip()

    def OnCbMasterButton(self, event):
        wx.PostEvent(self,vtSecXmlMasterBrowseDialogMaster(self))
        event.Skip()

    def OnCbStatusButton(self, event):
        wx.PostEvent(self,vtSecXmlMasterBrowseDialogStatus(self))
        event.Skip()
