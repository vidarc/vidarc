#Boa:Dialog:vNetXmlGuiMasterDialog
#----------------------------------------------------------------------------
# Name:         vNetXmlGuiMasterDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vNetXmlGuiMasterDialog.py,v 1.9 2007/08/06 22:54:45 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import wx.lib.scrolledpanel

from vidarc.tool.net.vNetXmlGuiMasterCloseDialog import vNetXmlGuiMasterCloseDialog
from vidarc.tool.net.vNetCfgDialog import vNetCfgDialog
from vidarc.tool.net.vNetXmlSynchProc import vNetXmlSynchProc
from vidarc.tool.net.vNetXmlSynchTreeList import *#vNetXmlSynchTreeList
from vidarc.tool.net.vNetXmlWxGui import *
from vidarc.tool.xml.vtXmlDom import *

import vidarc.tool.log.vtLog as vtLog
import sys,traceback,threading,time,os
import asynchat

import vidarc.tool.net.img_gui as img_gui
import vidarc.tool.net.images as images

def create(parent):
    return vNetXmlGuiMasterDialog(parent)

[wxID_VNETXMLGUIMASTERDIALOG, wxID_VNETXMLGUIMASTERDIALOGCBCFG, 
 wxID_VNETXMLGUIMASTERDIALOGCBCLOSE, wxID_VNETXMLGUIMASTERDIALOGCBCONN, 
 wxID_VNETXMLGUIMASTERDIALOGCBDISCONN, wxID_VNETXMLGUIMASTERDIALOGCBOPEN, 
 wxID_VNETXMLGUIMASTERDIALOGCBSAVE, wxID_VNETXMLGUIMASTERDIALOGCBSTOP, 
 wxID_VNETXMLGUIMASTERDIALOGCBUPDATESYNCH, wxID_VNETXMLGUIMASTERDIALOGCHCCONN, 
 wxID_VNETXMLGUIMASTERDIALOGCHCCONNSYNCH, 
 wxID_VNETXMLGUIMASTERDIALOGLSTAPPLCONN, 
 wxID_VNETXMLGUIMASTERDIALOGLSTCONNSTATE, wxID_VNETXMLGUIMASTERDIALOGLSTLOG, 
 wxID_VNETXMLGUIMASTERDIALOGNBMAIN, wxID_VNETXMLGUIMASTERDIALOGPNCONN, 
 wxID_VNETXMLGUIMASTERDIALOGPNLOG, wxID_VNETXMLGUIMASTERDIALOGPNOVERVIEW, 
 wxID_VNETXMLGUIMASTERDIALOGPNSYNCH, wxID_VNETXMLGUIMASTERDIALOGSLCONN, 
 wxID_VNETXMLGUIMASTERDIALOGSLCONNSYNCH, 
] = [wx.NewId() for _init_ctrls in range(21)]

class thdNetXmlStart:
    def __init__(self,par,verbose=0):
        self.verbose=verbose
        self.par=par
    def Start(self):
        thread.start_new_thread(self.Run, ())
    def Run(self):
        try:
            speed=2
            iCount=len(self.par.lstNetAppl)
            for idx in range(0,iCount):
                name=self.par.lstNetAppl[idx]
                netDoc=self.par.dictNet[name]
                iTime=0.0
                
                while 1==1:
                    if speed==0:
                        if netDoc.GetStatus() in [netDoc.SYNCH_FIN,netDoc.SYNCH_ABORT]:
                            break
                    elif speed==1:
                        #if self.par.netMaster.IsFlag(netDoc.CFG_VALID):
                        if netDoc.IsFlag(netDoc.OPENED):
                            break
                    else:
                        #if self.par.netMaster.IsFlag(self.par.netMaster.OPENED):
                            break
                    time.sleep(0.1)
                    iTime+=0.1
                    if iTime>10:
                        if netDoc.GetStatus() in [netDoc.DISCONNECTED,netDoc.STOPPED,netDoc.CONNECT_FLT]:
                            break
                self.par.netMaster.AutoConnect2Srv(idx+1)
        except:
            traceback.print_exc()
class thdNetXmlStop:
    def __init__(self,par,verbose=0):
        self.verbose=verbose
        self.par=par
    def Start(self):
        thread.start_new_thread(self.Run, ())
    def Run(self):
        speed=1
        #self.par.thdLog.Stop()
        #while self.par.thdLog.IsRunning():
        #    time.sleep(1.0)
        iCount=len(self.par.lstNetAppl)
        for idx in range(0,iCount):
            name=self.par.lstNetAppl[idx]
            netDoc=self.par.dictNet[name]
            netDoc.DisConnect()
            if speed==0:
                while netDoc.GetStatus() != netDoc.STOPPED:
                    time.sleep(0.1)
            elif speed==1:
                time.sleep(1.0)
            else:
                pass
class thdNetXmlLog:
    def __init__(self,par,verbose=0):
        self.verbose=verbose
        self.par=par
        self.running=False
    def Start(self):
        thread.start_new_thread(self.Run, ())
    def Stop(self):
        self.keepGoing=False
    def IsRunning(self):
        return self.running
    def Run(self):
        self.keepGoing=self.running=True
        try:
            while self.keepGoing:
                self.par.__checkScheduled__()
                time.sleep(0.5)
        except:
            traceback.print_exc()
        self.running=False
class vNetXmlGuiMasterDialog(wx.Dialog):
    UPDATE_TIME = 500
    STATE_MAP=[
        []  ,  # state
        [_(u'no access')    ,_(u'access')]  ,      # configuation   DATA_ACCESS
        [_('notify')        ,_(u'blocked')]  ,     # notify         BLOCK_NOTIFY
        [_(u'closed')       ,_(u'opened')]  ,      # open           OPENED
        [_(u'unknown')      ,_(u'valid')]  ,       # config         CONFIG_VALID
        [_(u'copy')         ,_(u'synchable')]  ,   # synch          SYNCHABLE
        [_(u'disconnected') ,_(u'established')]  , # connection     CONN_ACTIVE
        [_(u'running')      ,_(u'paused')]  ,      # connection     CONN_ACTIVE
        
        ]
    def _init_coll_nbMain_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.pnOverview, select=False,
              text=_(u'Connection'))
        parent.AddPage(imageId=-1, page=self.pnConn, select=True,
              text=_(u'Settings'))
        parent.AddPage(imageId=-1, page=self.pnLog, select=False,
              text=_(u'Logs'))
        parent.AddPage(imageId=-1, page=self.pnSynch, select=False,
              text=_(u'Synch'))

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VNETXMLGUIMASTERDIALOG,
              name=u'vNetXmlGuiMasterDialog', parent=prnt, pos=wx.Point(244,
              152), size=wx.Size(521, 324), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vNetGuiMaster Dialog')
        self.SetClientSize(wx.Size(513, 297))

        self.cbClose = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VNETXMLGUIMASTERDIALOGCBCLOSE,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Close'), name=u'cbClose',
              parent=self, pos=wx.Point(200, 264), size=wx.Size(76, 30),
              style=0)
        self.cbClose.Bind(wx.EVT_BUTTON, self.OnCbCloseButton,
              id=wxID_VNETXMLGUIMASTERDIALOGCBCLOSE)

        self.nbMain = wx.Notebook(id=wxID_VNETXMLGUIMASTERDIALOGNBMAIN,
              name=u'nbMain', parent=self, pos=wx.Point(8, 8), size=wx.Size(408,
              248), style=0)
        self.nbMain.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGING,
              self.OnNbMainNotebookPageChanging,
              id=wxID_VNETXMLGUIMASTERDIALOGNBMAIN)
        self.nbMain.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGED,
              self.OnNbMainNotebookPageChanged,
              id=wxID_VNETXMLGUIMASTERDIALOGNBMAIN)

        self.pnOverview = wx.Panel(id=wxID_VNETXMLGUIMASTERDIALOGPNOVERVIEW,
              name=u'pnOverview', parent=self.nbMain, pos=wx.Point(0, 0),
              size=wx.Size(400, 222), style=wx.TAB_TRAVERSAL)

        self.pnConn = wx.Panel(id=wxID_VNETXMLGUIMASTERDIALOGPNCONN,
              name=u'pnConn', parent=self.nbMain, pos=wx.Point(0, 0),
              size=wx.Size(400, 222), style=wx.TAB_TRAVERSAL)

        self.pnLog = wx.Panel(id=wxID_VNETXMLGUIMASTERDIALOGPNLOG,
              name=u'pnLog', parent=self.nbMain, pos=wx.Point(0, 0),
              size=wx.Size(400, 222), style=wx.TAB_TRAVERSAL)

        self.lstLog = wx.ListView(id=wxID_VNETXMLGUIMASTERDIALOGLSTLOG,
              name=u'lstLog', parent=self.pnLog, pos=wx.Point(8, 8),
              size=wx.Size(384, 208), style=wx.LC_REPORT)

        self.lstApplConn = wx.ListView(id=wxID_VNETXMLGUIMASTERDIALOGLSTAPPLCONN,
              name=u'lstApplConn', parent=self.pnConn, pos=wx.Point(8, 8),
              size=wx.Size(384, 208), style=wx.LC_REPORT)
        self.lstApplConn.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstApplConnListItemSelected,
              id=wxID_VNETXMLGUIMASTERDIALOGLSTAPPLCONN)
        self.lstApplConn.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstApplConnListItemDeselected,
              id=wxID_VNETXMLGUIMASTERDIALOGLSTAPPLCONN)
        self.lstApplConn.Bind(wx.EVT_LEFT_DCLICK, self.OnLstApplConnLeftDclick)
        self.lstApplConn.Bind(wx.EVT_LIST_COL_CLICK,
              self.OnLstApplConnListColClick,
              id=wxID_VNETXMLGUIMASTERDIALOGLSTAPPLCONN)

        self.cbCfg = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VNETXMLGUIMASTERDIALOGCBCFG,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Config'), name=u'cbCfg',
              parent=self, pos=wx.Point(428, 16), size=wx.Size(76, 30),
              style=0)
        self.cbCfg.Bind(wx.EVT_BUTTON, self.OnCbCfgButton,
              id=wxID_VNETXMLGUIMASTERDIALOGCBCFG)

        self.cbConn = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VNETXMLGUIMASTERDIALOGCBCONN,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Connect'),
              name=u'cbConn', parent=self, pos=wx.Point(428, 64),
              size=wx.Size(76, 30), style=0)
        self.cbConn.Bind(wx.EVT_BUTTON, self.OnCbConnButton,
              id=wxID_VNETXMLGUIMASTERDIALOGCBCONN)

        self.cbDisConn = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VNETXMLGUIMASTERDIALOGCBDISCONN,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Discon'),
              name=u'cbDisConn', parent=self, pos=wx.Point(428, 96),
              size=wx.Size(76, 30), style=0)
        self.cbDisConn.Bind(wx.EVT_BUTTON, self.OnCbDisConnButton,
              id=wxID_VNETXMLGUIMASTERDIALOGCBDISCONN)

        self.cbStop = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VNETXMLGUIMASTERDIALOGCBSTOP,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Stop'), name=u'cbStop',
              parent=self, pos=wx.Point(428, 144), size=wx.Size(76, 30),
              style=0)
        self.cbStop.Bind(wx.EVT_BUTTON, self.OnCbStopButton,
              id=wxID_VNETXMLGUIMASTERDIALOGCBSTOP)

        self.cbOpen = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VNETXMLGUIMASTERDIALOGCBOPEN,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Open'), name=u'cbOpen',
              parent=self, pos=wx.Point(428, 192), size=wx.Size(76, 30),
              style=0)
        self.cbOpen.Bind(wx.EVT_BUTTON, self.OnCbOpenButton,
              id=wxID_VNETXMLGUIMASTERDIALOGCBOPEN)

        self.cbSave = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VNETXMLGUIMASTERDIALOGCBSAVE,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Save'), name=u'cbSave',
              parent=self, pos=wx.Point(428, 226), size=wx.Size(76, 30),
              style=0)
        self.cbSave.Bind(wx.EVT_BUTTON, self.OnCbSaveButton,
              id=wxID_VNETXMLGUIMASTERDIALOGCBSAVE)

        self.chcConn = wx.Choice(choices=[],
              id=wxID_VNETXMLGUIMASTERDIALOGCHCCONN, name=u'chcConn',
              parent=self.pnOverview, pos=wx.Point(224, 40), size=wx.Size(130,
              21), style=0)
        self.chcConn.Bind(wx.EVT_CHOICE, self.OnChcConnChoice,
              id=wxID_VNETXMLGUIMASTERDIALOGCHCCONN)

        self.slConn = wx.Slider(id=wxID_VNETXMLGUIMASTERDIALOGSLCONN,
              maxValue=0, minValue=0, name=u'slConn', parent=self.pnOverview,
              point=wx.Point(224, 8), size=wx.DefaultSize,
              style=wx.SL_HORIZONTAL, value=0)
        self.slConn.Bind(wx.EVT_SCROLL, self.OnSlConnScroll)

        self.lstConnState = wx.ListView(id=wxID_VNETXMLGUIMASTERDIALOGLSTCONNSTATE,
              name=u'lstConnState', parent=self.pnOverview, pos=wx.Point(204,
              74), size=wx.Size(188, 142), style=wx.LC_REPORT)

        self.pnSynch = wx.Panel(id=wxID_VNETXMLGUIMASTERDIALOGPNSYNCH,
              name=u'pnSynch', parent=self.nbMain, pos=wx.Point(0, 0),
              size=wx.Size(400, 222), style=wx.TAB_TRAVERSAL)

        self.chcConnSynch = wx.Choice(choices=[],
              id=wxID_VNETXMLGUIMASTERDIALOGCHCCONNSYNCH, name=u'chcConnSynch',
              parent=self.pnSynch, pos=wx.Point(116, 8), size=wx.Size(130, 21),
              style=0)
        self.chcConnSynch.Bind(wx.EVT_CHOICE, self.OnChcConnSynchChoice,
              id=wxID_VNETXMLGUIMASTERDIALOGCHCCONNSYNCH)

        self.slConnSynch = wx.Slider(id=wxID_VNETXMLGUIMASTERDIALOGSLCONNSYNCH,
              maxValue=0, minValue=0, name=u'slConnSynch', parent=self.pnSynch,
              point=wx.Point(4, 8), size=wx.Size(100, 26),
              style=wx.SL_HORIZONTAL, value=0)
        self.slConnSynch.Bind(wx.EVT_SCROLL, self.OnSlConnSynchScroll)

        self.cbUpdateSynch = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VNETXMLGUIMASTERDIALOGCBUPDATESYNCH,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Update',
              name=u'cbUpdateSynch', parent=self.pnSynch, pos=wx.Point(320, 4),
              size=wx.Size(76, 30), style=0)
        self.cbUpdateSynch.Bind(wx.EVT_BUTTON, self.OnCbUpdateSynchButton,
              id=wxID_VNETXMLGUIMASTERDIALOGCBUPDATESYNCH)

        self._init_coll_nbMain_Pages(self.nbMain)

    def __init__(self, parent,verbose=0):
        self._init_ctrls(parent)
        
        self.iFill=0
        self.pos=[0,0]
        self.verbose=verbose
        self.bBlockNotify=False
        
        self.idxSel=-1
        self.dt=wx.DateTime()
        
        self.dlgCfgSetup=None
        self.nodeSetup=None
        self.docSetup=None
        self.lstCfg=None
        #self.cfgNode=None
        self.appls=[]
        self.mainAppl=''
        self.dn=''
        self.sRecentAlias=''
        self.sRecentConn=''
        self.alias=''
        self.host=''
        self.port=-1
        self.usr=''
        self.passwd=''
        
        self.netMaster=parent
        self.bConnectAll=False
        self.bOpenAll=False
        
        self.thdStart=thdNetXmlStart(self)
        self.thdStop=thdNetXmlStop(self)
        self.thdLog=thdNetXmlLog(self)
        
        self.pnGui=wx.lib.scrolledpanel.ScrolledPanel(self.pnOverview,id=wx.NewId(),
                pos=(8,8),size=(188, 200),
                style = wx.TAB_TRAVERSAL|wx.SUNKEN_BORDER)
        #self.pnGuiSizer = wx.BoxSizer(wx.VERTICAL)
        self.pnGuiSizer = wx.FlexGridSizer(cols=3, vgap=4, hgap=4)
        self.pnGui.SetSizer(self.pnGuiSizer)
        self.pnGui.SetAutoLayout(1)

        #self.scwConn.Show(False)
        self.semNotify=threading.Lock()#threading.Semaphore()
        self.lstScheduled=asynchat.fifo()
        
        self.netDocMaster=None
        self.lstNetAppl=[]
        self.dictNet={}
        self.dictLbl={}
        self.dictProcess={}
        self.dlgCfgSetup=None
        self.lstApplConn.InsertColumn(0,_(u'state'),wx.LIST_FORMAT_LEFT,40)
        self.lstApplConn.InsertColumn(1,_(u'appl'),wx.LIST_FORMAT_LEFT,60)
        self.lstApplConn.InsertColumn(2,_(u'alias'),wx.LIST_FORMAT_LEFT,60)
        self.lstApplConn.InsertColumn(3,_(u'host'),wx.LIST_FORMAT_LEFT,60)
        self.lstApplConn.InsertColumn(4,_(u'usr'),wx.LIST_FORMAT_LEFT,60)
        self.lstApplConn.InsertColumn(5,_(u'fn'),wx.LIST_FORMAT_LEFT,160)
        self.lstApplConn.InsertColumn(6,_(u'port'),wx.LIST_FORMAT_RIGHT,60)
        self.SetupImageList()
        
        self.lstLog.InsertColumn(0,_(u'appl'),wx.LIST_FORMAT_LEFT,80)
        self.lstLog.InsertColumn(1,_(u'msg'),wx.LIST_FORMAT_LEFT,100)
        self.lstLog.InsertColumn(2,_(u'time'),wx.LIST_FORMAT_LEFT,60)
        self.lstLog.InsertColumn(3,_(u'status'),wx.LIST_FORMAT_LEFT,50)
        
        self.cbCfg.SetBitmapLabel(images.getSettingBitmap())
        self.cbClose.SetBitmapLabel(images.getCloseBitmap())
        self.cbStop.SetBitmapLabel(images.getStopBitmap())
        self.cbOpen.SetBitmapLabel(img_gui.getOpenBitmap())
        self.cbSave.SetBitmapLabel(img_gui.getSaveBitmap())
        self.cbConn.SetBitmapLabel(img_gui.getConnectBitmap())
        self.cbDisConn.SetBitmapLabel(img_gui.getStoppedBitmap())
        self.cbUpdateSynch.SetBitmapLabel(img_gui.getSynchBitmap())
        #self.timer = wx.PyTimer(self.__checkScheduled__)
        #self.timer.Start(self.UPDATE_TIME)
        
        self.lstConnState.InsertColumn(0,_(u'name'),wx.LIST_FORMAT_LEFT,70)
        self.lstConnState.InsertColumn(1,_(u'value'),wx.LIST_FORMAT_LEFT,90)
        self.lstConnState.InsertImageStringItem(sys.maxint,_(u'state'),-1)
        self.lstConnState.InsertImageStringItem(sys.maxint,_(u'access'),-1)
        self.lstConnState.InsertImageStringItem(sys.maxint,_(u'notify'),-1)
        self.lstConnState.InsertImageStringItem(sys.maxint,_(u'open'),-1)
        self.lstConnState.InsertImageStringItem(sys.maxint,_(u'configuation'),-1)
        self.lstConnState.InsertImageStringItem(sys.maxint,_(u'synch'),-1)
        self.lstConnState.InsertImageStringItem(sys.maxint,_(u'connection'),-1)
        self.lstConnState.InsertImageStringItem(sys.maxint,_(u'paused'),-1)
        
        self.docSynch=vNetXmlSynchProc(None)
        self.trlstSynch=vNetXmlSynchTreeList(parent=self.pnSynch,
                id=wx.NewId(), name=u'trlstSynch', 
                pos=wx.Point(4, 44), size=wx.Size(392, 172), style=wx.TR_HAS_BUTTONS,
                master=False,verbose=1)
        EVT_VTXMLTREE_ITEM_SELECTED(self.trlstSynch,self.OnSynchTreeItemSel)
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.trlstSynch,self.OnSynchAddElement)
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.trlstSynch,self.OnSynchAddFinished)
        self.trlstSynch.SetDoc(self.docSynch)
        
        self.thdLog.Start()
    def SetupImageList(self):
        self.imgDict={}
        self.imgLstTyp=wx.ImageList(16,16)
        d={}
        d[vNetXmlWxGui.STOPPED]         =self.imgLstTyp.Add(img_gui.getStoppedBitmap())
        d[vNetXmlWxGui.CONNECT]         =self.imgLstTyp.Add(img_gui.getConnectBitmap())
        d[vNetXmlWxGui.CONNECTED]       =self.imgLstTyp.Add(img_gui.getConnectBitmap())
        d[vNetXmlWxGui.CONNECT_FLT]     =self.imgLstTyp.Add(img_gui.getFaultBitmap())
        d[vNetXmlWxGui.OPEN_START]      =self.imgLstTyp.Add(img_gui.getOpenBitmap())
        d[vNetXmlWxGui.OPEN_FLT]        =self.imgLstTyp.Add(img_gui.getFaultBitmap())
        d[vNetXmlWxGui.SYNCH_START]     =self.imgLstTyp.Add(img_gui.getSynchBitmap())
        d[vNetXmlWxGui.SYNCH_PROC]      =self.imgLstTyp.Add(img_gui.getSynchBitmap())
        d[vNetXmlWxGui.SYNCH_FIN]       =self.imgLstTyp.Add(img_gui.getRunningBitmap())
        d[vNetXmlWxGui.SYNCH_ABORT]     =self.imgLstTyp.Add(img_gui.getFaultBitmap())
        d[vNetXmlWxGui.LOGIN]           =self.imgLstTyp.Add(img_gui.getLoginBitmap())
        d[vNetXmlWxGui.LOGGEDIN]        =self.imgLstTyp.Add(img_gui.getRunningBitmap())
        d[vNetXmlWxGui.SERVED]          =self.imgLstTyp.Add(img_gui.getConnectBitmap())
        d[vNetXmlWxGui.SERVE_FLT]       =self.imgLstTyp.Add(img_gui.getFaultBitmap())
        d[vNetXmlWxGui.DISCONNECTED]    =self.imgLstTyp.Add(img_gui.getStoppedBitmap())
        d[-1]                           =self.imgLstTyp.Add(img_gui.getStoppedBitmap())  # FIXME
        
        self.imgDict['dft']=d
        
        self.lstApplConn.SetImageList(self.imgLstTyp,wx.IMAGE_LIST_SMALL)
        
        self.imgNB=wx.ImageList(16,16)
        self.imgNB.Add(images.getNetConnectionsBitmap())
        self.imgNB.Add(images.getNetConfigBitmap())
        self.imgNB.Add(images.getNetLogBitmap())
        self.imgNB.Add(images.getNetSynchBitmap())
        self.nbMain.SetImageList(self.imgNB)
        for i in range(4):
            self.nbMain.SetPageImage(i,i)
            
        self.imgConnState=wx.ImageList(16,16)
        self.imgConnState.Add(images.getNetStateClrBitmap())
        self.imgConnState.Add(images.getNetStateSetBitmap())
        self.lstConnState.SetImageList(self.imgConnState,wx.IMAGE_LIST_SMALL)
    
    def SetCfg(self,doc,lst,appls,mainAppl):
        self.docSetup=doc
        self.lstCfg=lst
        self.dictDoc={}
        self.appls=appls
        self.mainAppl=mainAppl
        self.sRecentAlias=''
        self.sRecentConn=''
        if self.dlgCfgSetup is None:
            self.dlgCfgSetup=vNetCfgDialog(self)
            self.SetCfgNode(None)
        for k in self.dictNet.keys():
            self.dictNet[k].SetCfg(self.docSetup,self.lstCfg,self.appls,k,
                    self.mainAppl,self.dlgCfgSetup)
        #self.pnGui.SetSize((-1,self.pos[1]+32))
        self.pnGui.SetupScrolling()
    def __getCfgNode__(self):
        self.nodeSetup=self.docSetup.getRoot()
        return self.docSetup.getChildByLst(self.nodeSetup,self.lstCfg)
        
    def SetCfgNode(self,bConnect=True):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.__updateConnState__()
        idx=0
        self.nodeSetup=self.docSetup.getRoot()
        cfgNode=self.__getCfgNode__()
        dn=self.docSetup.getNodeText(cfgNode,'path')
        if len(dn)<=0:
            dn=os.getcwd()
            self.docSetup.setNodeText(cfgNode,'path',dn)
        for name in self.lstNetAppl:
            netDoc=self.dictNet[name]
            recentNode=self.netMaster.__getRecentAlias__()
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'recent:%s'%recentNode,self)
            recentAliasNode=self.netMaster.__getRecentConnection__(recentNode)
            recentHostNode=self.netMaster.__getApplHost__(recentAliasNode,name)
            if recentHostNode is not None:
                netDoc.GenAutoFN(recentHostNode,dn)
                self.lstApplConn.SetStringItem(idx,2,self.docSetup.getNodeText(recentHostNode,'alias'),-1)
                self.lstApplConn.SetStringItem(idx,3,self.docSetup.getNodeText(recentHostNode,'host'),-1)
                self.lstApplConn.SetStringItem(idx,4,self.docSetup.getNodeText(recentHostNode,'usr'),-1)
                try:
                    self.lstApplConn.SetStringItem(idx,5,netDoc.GetFN(),-1)
                except:
                    pass
                self.lstApplConn.SetStringItem(idx,6,self.docSetup.getNodeText(recentHostNode,'port'),-1)
            idx+=1
    def SetCfgDlg(self,dlgCfgSetup):
        self.dlgCfgSetup=dlgCfgSetup
    
    def GetCfgDlg(self,dlgCfgSetup):
        return self.dlgCfgSetup
    
    def GetNetControlParent(self):
        return self.scwConn
    def __getImgIndex__(self,name,status):
        try:
            return self.imgDict[name][status]
        except:
            try:
                return self.imgDict['dft'][status]
            except:
                return self.imgDict['dft'][-1]
    def __addLog__(self,name,msg,iStatus):
        try:
            sTime=self.dt.Now().FormatTime()
            idx=self.lstLog.InsertImageStringItem(0, name,-1)
            self.lstLog.SetStringItem(idx,1,msg,-1)
            self.lstLog.SetStringItem(idx,2,sTime,-1)
            self.lstLog.SetStringItem(idx,3,'0x%04x'%iStatus,-1)
            
            idx=self.lstNetAppl.index(name)
            netDoc=self.dictNet[name]
            self.lstApplConn.SetStringItem(idx,0,'',self.__getImgIndex__(name,netDoc.GetStatus(iStatus)))
        except:
            traceback.print_exc()
    def AddNetControl(self,ctrlClass,name,*args,**kwargs):
        if self.dictNet.has_key(name):
            return None
        #if kwargs.has_key('pos')==False:
        kwargs['pos']=self.pos
        if kwargs.has_key('size')==False:
            kwargs['size']=wx.Size(16,16)
        
        ctrl=ctrlClass(self.pnGui,name,*args,**kwargs)
        self.pnGuiSizer.Add(ctrl, flag=wx.ALIGN_LEFT | wx.ALIGN_CENTER_VERTICAL)
        ctrl.SetNetMaster(self.netMaster)
        lbl=wx.StaticText(id=wx.NewId(),
              label=name, name=u'lbl'+name, parent=self.pnGui, pos=wx.Point(20,
              self.pos[1]), size=wx.Size(60, 13), style=0)
        self.pnGuiSizer.Add(lbl, flag=wx.ALIGN_LEFT | wx.ALIGN_CENTER_VERTICAL)
        pb=wx.Gauge(self.pnGui, -1, 50, (90, self.pos[1]), 
                    (80, 16),wx.GA_HORIZONTAL|wx.GA_SMOOTH)
        self.pnGuiSizer.Add(pb, flag=wx.ALIGN_LEFT | wx.ALIGN_CENTER_VERTICAL)
        
        self.iFill+=1
        self.pos[1]+=20#size[1]+4
        self.lstNetAppl.append(name)
        self.dictNet[name]=ctrl
        self.dictLbl[name]=lbl
        self.dictProcess[name]=pb
        if self.iFill==1:
            self.netDocMaster=ctrl
        d=ctrl.GetListImgDict(self.imgLstTyp)
        if d is not None:
            self.imgDict[name]=d
        idx=self.lstApplConn.InsertImageStringItem(sys.maxint, '', 
                            self.__getImgIndex__(name,ctrl.STOPPED))
        self.lstApplConn.SetStringItem(idx,1,name,-1)
        
        recentNode=self.netMaster.__getRecentAlias__()
        recentAliasNode=self.netMaster.__getRecentConnection__(recentNode)
        recentHostNode=self.netMaster.__getApplHost__(recentAliasNode,name)
        if recentHostNode is not None:
            self.lstApplConn.SetStringItem(idx,2,self.docSetup.getNodeText(recentHostNode,'alias'),-1)
            self.lstApplConn.SetStringItem(idx,3,self.docSetup.getNodeText(recentHostNode,'host'),-1)
            self.lstApplConn.SetStringItem(idx,4,self.docSetup.getNodeText(recentHostNode,'usr'),-1)
            self.lstApplConn.SetStringItem(idx,6,self.docSetup.getNodeText(recentHostNode,'port'),-1)
        
        self.chcConn.Append(name)
        self.slConn.SetRange(0,len(self.lstNetAppl)-1)
        self.chcConn.SetSelection(0)
        
        self.chcConnSynch.Append(name)
        self.slConnSynch.SetRange(0,len(self.lstNetAppl)-1)
        self.chcConnSynch.SetSelection(0)
        
        return ctrl
    def IsModified(self):
        for netDoc in self.dictNet.values():
            if netDoc.IsModified():
                return True
        return False
    def Save(self,bModified=False):
        for name in self.lstNetAppl:
            netDoc=self.dictNet[name]
            if bModified:
                if netDoc.IsModified():
                    netDoc.Save()
            else:
                netDoc.Save()
        
    def __getNetDocIdxName__(self,netDoc):
        i=0
        for name in self.lstNetAppl:
            if self.dictNet[name]==netDoc:
                return i,name
            i+=1
        return -1,''
    def __updateStatusByNetDoc__(self,name,netDoc,iStatusPrev,iStatus,*args,**kwargs):
        #idx,name=self.__getNetDocIdxName__(netDoc)
        if self.semNotify.acquire(False):
            #vtLog.vtLngCallStack(None,vtLog.DEBUG,'scheduled:%s %04x %04x args:%s kwargs:%s'%(name,iStatusPrev,iStatus,repr(args),repr(kwargs)),verbose=1)
            self.lstScheduled.push((name,netDoc,iStatusPrev,iStatus,args,kwargs))
            return
            pass
        #for tup in self.lstScheduled:
        self.__checkScheduled__()
        #while self.lstScheduled.is_empty()==False:
        #    tup=self.lstScheduled.pop()
        #    self.__notify__(tup[0],tup[1],tup[2],tup[3],*tup[4],**tup[5])
            #self.lstScheduled=[]
        self.__notify__(name,netDoc,iStatusPrev,iStatus,*args,**kwargs)
        #for tup in self.lstScheduled:
        #    self.__notify__(tup[0],tup[1],tup[2],tup[3],*tup[4],**tup[5])
        #    self.lstScheduled=[]
        self.semNotify.release()
        if 1==0:
            if idx<0:
                return
            if netDoc.GetStatus() in [netDoc.SYNCH_PROC]:
                return
            self.lstApplConn.SetStringItem(idx,0,'',self.__getImgIndex__(name,netDoc.GetStatus(iStatus)))
            if idx==0:
                if netDoc.IsStatusChanged():
                    self.netMaster.SetBitmap(netDoc.__getBitmapByStatus__(iStatus))
    
    def __notify__(self,name,alias,netDoc,iStatusPrev,iStatus,*args,**kwargs):
        try:
            idx=self.lstNetAppl.index(name)
        except:
            traceback.print_exc()
            vtLog.vtLngCallStack(None,vtLog.ERROR,
                    'name:%s;lstNetAppl:%s;netDoc:%s;%s'%(name,repr(self.lstNetAppl),repr(netDoc),traceback.format_exc()),origin='vNetMasterDlg')
            return
        try:
            #vtLog.vtLngCallStack(None,vtLog.DEBUG,'name:%s status:0x%04x prev:0x%04x'%(name,iStatus,iStatusPrev),verbose=1,origin='vNetMasterDlg')
            if netDoc.IsStatusChanged(iStatusPrev,iStatus):
                if idx==0:
                    self.netMaster.SetBitmap(netDoc.__getBitmapByStatus__(iStatus))
                self.__addLog__(name,netDoc.GetStatusStr(iStatus),iStatus)
            pb=self.dictProcess[name]
            if netDoc.GetStatus(iStatus)==netDoc.CONNECT:
                try:
                    fn=netDoc.GetFN()
                    if fn is None:
                        fn='???'
                except:
                    fn='???'
                self.lstApplConn.SetStringItem(idx,2,netDoc.alias,-1)
                self.lstApplConn.SetStringItem(idx,3,netDoc.host,-1)
                self.lstApplConn.SetStringItem(idx,4,netDoc.usr,-1)
                self.lstApplConn.SetStringItem(idx,5,fn,-1)
                self.lstApplConn.SetStringItem(idx,6,repr(netDoc.port),-1)
                #self.__updateApplButtons__()
            elif netDoc.GetStatus(iStatus)==netDoc.SYNCH_START:
                pb.SetRange(100)
            elif netDoc.GetStatus(iStatus)==netDoc.SYNCH_PROC:
                try:
                    pb.SetRange(args[1])
                    pb.SetValue(args[0])
                except:
                    vtLog.vtLngCallStack(None,vtLog.DEBUG,'',verbose=1,origin='vNetMasterDlg')
            #elif netDoc.GetStatus(iStatus) in [netDoc.STOPPED,netDoc.DISCONNECTED]:
            #    self.__updateApplButtons__()
            elif netDoc.GetStatus(iStatus)==netDoc.SYNCH_FIN:
                pb.SetValue(0)
                #netDoc.DoEditResolve()
            if idx==0:
                try:
                    if netDoc.GetStatus(iStatus)==netDoc.LOGGEDIN:
                        netDoc.GetConfig()
                    if netDoc.GetStatus(iStatus)==netDoc.CONFIG:
                        if netDoc.IsFlag(netDoc.CFG_VALID):
                            self.netMaster.__updateConfig__()
                            #if self.bConnectAll:
                            #    self.netMaster.AutoConnect2Srv(-2)
                        netDoc.getDataFromRemote()
                    pass
                except:
                    traceback.print_exc()
                #if netDoc.GetStatus()==netDoc.OPEN_OK:
                #    self.netMaster.__updateConfig__()
                
                #if netDoc.GetStatus()==netDoc.SYNCH_START:
                #    if netDoc.IsFlag(netDoc.OPENED):
                #        netDoc.SetFlag(netDoc.CFG_VALID,True)
                #    self.netMaster.AutoConnect2Srv()
                if self.bConnectAll:
                    if self.netDocMaster.IsActive():
                        if self.netDocMaster.IsFlag(netDoc.CFG_VALID):
                            self.thdStart.Start()
                            self.bConnectAll=False
                            #self.netMaster.AutoConnect2Srv(idx+1)
            else:
                if netDoc.GetStatus(iStatus)==netDoc.LOGGEDIN:
                    netDoc.getDataFromRemote()
            
            #if netDoc.GetStatus(iStatus) in [netDoc.SYNCH_FIN,netDoc.SYNCH_ABORT]:
            #if netDoc.GetStatus(iStatus) in [netDoc.OPEN_OK,netDoc.OPEN_FLT]:
            #    if self.bConnectAll:
            #        if self.netDocMaster.IsActive():
            #            if self.netDocMaster.IsFlag(netDoc.CFG_VALID):
            #                if idx<len(self.lstNetAppl):
            #                    self.netMaster.AutoConnect2Srv(idx+1)
                                #self.bConnectAll=False
            
            self.__check__()
            #self.__updateApplButtons__()
        except:
            traceback.print_exc()
            vtLog.vtLngCallStack(None,vtLog.ERROR,
                    traceback.format_exc(),origin='vNetMasterDlg')

    def __checkScheduled__(self):
        if self.semNotify.acquire(False):
            return
        while self.lstScheduled.is_empty()==False:
            bFound,tup=self.lstScheduled.pop()
            self.__notify__(tup[0],tup[1],tup[2],tup[3],*tup[4],**tup[5])
        self.semNotify.release()
        
    def Notify(self,name,netDoc,iStatusPrev,iStatus,*args,**kwargs):
        vtLog.vtLngCallStack(None,vtLog.DEBUG,'acquire',verbose=1,origin='vNetMasterDlg')
        #self.semNotify.acquire()
        if self.nbMain.GetSelection()==0:
            self.__updateConnState__()
        try:
            self.lstScheduled.push((name,netDoc,iStatusPrev,iStatus,args,kwargs))
        except:
            vtLog.vtLngCallStack(None,vtLog.ERROR,'name:%s status:0x%04x'%(name,iStatus),verbose=1,origin='vNetMasterDlg')
            traceback.print_exc()
            vtLog.vtLngCallStack(None,vtLog.ERROR,
                    traceback.format_exc(),origin='vNetMasterDlg')
        #self.semNotify.release()
        vtLog.vtLngCallStack(None,vtLog.DEBUG,'release',verbose=1,origin='vNetMasterDlg')
        
    def IsAllSettled(self):
        for name in self.lstNetAppl:
            netDoc=self.dictNet[name]
            if netDoc.IsSettled()==False:
                return False
        return True
    def IsDataAccessed(self):
        for name in self.lstNetAppl:
            netDoc=self.dictNet[name]
            if netDoc.IsDataAccessed()==True:
                return True
        return False
    def __check__(self):
        bAllSettled=True
        bAllOpened=True
        bReleaseBlock=False
        i=0
        for name in self.lstNetAppl:
            netDoc=self.dictNet[name]
            #vtLog.vtLngCallStack(None,vtLog.DEBUG,'full:0x%04x 0x%04x state:%s '%(netDoc.iStatusPrev,netDoc.iStatus,netDoc.GetStatusStr()),verbose=1)
            if netDoc.IsSettled()==False:
                bAllSettled=False
                #vtLog.vtLngCallStack(None,vtLog.DEBUG,'not settled:%s'%name,verbose=1,origin='vNetMasterDlg')
            else:
                if i==0:
                    bReleaseBlock=True
            if netDoc.IsFlag(netDoc.OPENED)==False:
                bAllOpened=False
            if bReleaseBlock and i>0:
                #if self.verbose:
                #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'release block')
                netDoc.SetBlockNotify(False)
            i+=1
        if bAllSettled:
            self.bConnectAll=False
            vtLog.vtLngCallStack(None,vtLog.DEBUG,'all settled',verbose=1,origin='vNetMasterDlg')
            for name in self.lstNetAppl:
                netDoc=self.dictNet[name]
                if self.verbose:
                    vtLog.vtLngCallStack(self,vtLog.DEBUG,'state:%s full:'%(netDoc.GetStatusStr(),netDoc.iStatus),origin='vNetMasterDlg')
                netDoc.SetBlockNotify(False)
        if bAllOpened:
            self.bOpenAll=False
            vtLog.vtLngCallStack(None,vtLog.DEBUG,'all opened',verbose=1,origin='vNetMasterDlg')
    def IsAllSettled(self):
        for name in self.lstNetAppl:
            netDoc=self.dictNet[name]
            if netDoc.IsSettled()==False:
                return False
        return True
    def GetDictNet(self):
        return self.dictNet
    
    def GetNetNames(self):
        return self.lstNetAppl
    
    def GetNetDoc(self,name):
        try:
            return self.dictNet[name]
        except:
            return None
            
    def __updateApplButtons__(self):
        if self.idxSel<0:
            #if self.IsAllDisconnected():
            #    self.cbConn.SetBitmapLabel(img_gui.getConnectBitmap())
            #else:
            #    self.cbConn.SetBitmapLabel(img_gui.getStoppedBitmap())
            self.cbConn.Enable(self.IsAllSettled())
            
            #if self.IsAllClosed():
            #    self.cbOpen.SetLabel(_(u'Open'))
            #    self.cbOpen.SetBitmapLabel(img_gui.getOpenBitmap())
            #else:
            #    self.cbOpen.SetLabel(_(u'Save'))
            #    self.cbOpen.SetBitmapLabel(img_gui.getSaveBitmap())
            self.cbOpen.Enable(self.IsAllSettled())
        else:
            netDoc=self.dictNet[self.lstNetAppl[self.idxSel]]
            #if netDoc.IsActive():
            #    self.cbConn.SetLabel(_(u'Discon.'))
            #    self.cbConn.SetBitmapLabel(img_gui.getStoppedBitmap())
            #else:
            #    self.cbConn.SetLabel(_(u'Connect'))
            #    self.cbConn.SetBitmapLabel(img_gui.getConnectBitmap())
            self.cbConn.Enable(netDoc.IsSettled())
            
            #if netDoc.IsFlag(netDoc.OPENED)==False:
            #    self.cbOpen.SetLabel(_(u'Open'))
            #    self.cbOpen.SetBitmapLabel(images.getBrowseBitmap())
            #else:
            #    self.cbOpen.SetLabel(_(u'Save'))
            #    self.cbOpen.SetBitmapLabel(img_gui.getSaveBitmap())
            self.cbOpen.Enable(netDoc.IsSettled())
        self.cbConn.Refresh()
        self.cbOpen.Refresh()
                    
    def OnLstApplConnListItemSelected(self, event):
        self.idxSel=event.GetIndex()
        #self.__updateApplButtons__()
        event.Skip()

    def OnLstApplConnListItemDeselected(self, event):
        self.idxSel=-1
        #self.__updateApplButtons__()
        event.Skip()

    def OnLstApplConnListColClick(self, event):
        if self.idxSel!=-1:
            self.lstApplConn.SetItemState(self.idxSel,0,wx.LIST_STATE_SELECTED)
                    
        self.idxSel=-1
        if self.IsAllDisconnected():
            self.cbConn.SetBitmapLabel(img_gui.getConnectBitmap())
        else:
            self.cbConn.SetBitmapLabel(img_gui.getStoppedBitmap())
        self.cbConn.Refresh()
        event.Skip()

    def OnCbCfgButton(self, event):
        if self.idxSel==-1:
            if self.netDocMaster.IsActive():
                return
        if self.dlgCfgSetup is None:
            return
        appl=self.lstNetAppl[self.idxSel]
        self.dlgCfgSetup.SetNode(self.docSetup,self.lstCfg,self.appls,
                        appl,self.__getCfgNode__())
        if self.idxSel>=0:
            self.dlgCfgSetup.SelAppl(appl)
        self.dlgCfgSetup.Centre()                
        self.Show(False)
        ret=self.dlgCfgSetup.ShowModal()
        if ret>0:
            #self.netMaster.AutoConnect2Srv()
            #self.OnCbConnButton(None)
            self.docSetup.Save()
            self.__updateListCfg__()
            self.Show(True)
            
            pass
        self.__updateListCfg__()
        event.Skip()

    def OnCbCloseButton(self, event):
        self.Show(False)
        event.Skip()

    def IsAllDisconnected(self):
        for netDoc in self.dictNet.values():
            if netDoc.GetStatus() not in [netDoc.STOPPED,netDoc.DISCONNECTED,
                                            netDoc.OPEN_OK,netDoc.OPEN_FLT]:
                return False
        return True
    def IsAllClosed(self):
        for netDoc in self.dictNet.values():
            if netDoc.IsFlag(netDoc.OPENED)==True:
                return False
        return True
    def OnCbConnButton(self, event):
        if self.idxSel==-1:
            if self.IsAllSettled()==False:
                return
            if self.IsDataAccessed()==True:
                return
            if self.bOpenAll:
                return
            if self.IsAllDisconnected():
                netDoc=self.dictNet[self.lstNetAppl[0]]
                netDoc.SetFlag(netDoc.CFG_VALID,False)
                for netDoc in self.dictNet.values():
                    netDoc.SetFlag(netDoc.OPENED,False)
                self.bConnectAll=True
                self.netMaster.AutoConnect2Srv(self.idxSel)
            #self.__updateApplButtons__()
            return
        else:
            netDoc=self.dictNet[self.lstNetAppl[self.idxSel]]
            if netDoc.GetStatus()==netDoc.LOGIN:
                netDoc.Login()
                return
            if self.bConnectAll:
                return
            if self.bOpenAll:
                return
            self.bConnectAll=False
            if netDoc.IsFlag(netDoc.CONN_ACTIVE)==False:
                self.netMaster.AutoConnect2Srv(self.idxSel)
            #self.__updateApplButtons__()
        if event is not None:
            event.Skip()
    
    def OnCbDisConnButton(self, event):
        if self.idxSel==-1:
            #if self.IsAllSettled()==False:
            #    return
            if self.IsDataAccessed()==True:
                return
            self.bConnectAll=False
            self.thdStop.Start()
            #self.__updateApplButtons__()
            return
        else:
            if self.bConnectAll:
                return
            if self.bOpenAll:
                return
            self.bConnectAll=False
            netDoc=self.dictNet[self.lstNetAppl[self.idxSel]]
            netDoc.DisConnect()
        if event is not None:
            event.Skip()
    
    def __updateListCfg__(self,idx=-1):
        cfgNode=self.__getCfgNode__()
        dn=self.docSetup.getNodeText(cfgNode,'path')
        recentNode=self.netMaster.__getRecentAlias__()
        recentAliasNode=self.netMaster.__getRecentConnection__(recentNode)
            
        for i in range(len(self.lstNetAppl)):
            if idx>=0:
                if idx!=i:
                    continue
            netDoc=self.dictNet[self.lstNetAppl[i]]
            recentHostNode=self.netMaster.__getApplHost__(recentAliasNode,netDoc.appl)
            netDoc.GenAutoFN(recentHostNode,dn)
            try:
                fn=netDoc.GetFN()
                if fn is None:
                    fn=u'???'
            except:
                fn=u'???'
            if recentHostNode is not None:
                self.lstApplConn.SetStringItem(i,2,self.docSetup.getNodeText(recentHostNode,'alias'),-1)
                self.lstApplConn.SetStringItem(i,3,self.docSetup.getNodeText(recentHostNode,'host'),-1)
                self.lstApplConn.SetStringItem(i,4,self.docSetup.getNodeText(recentHostNode,'usr'),-1)
                self.lstApplConn.SetStringItem(i,5,fn,-1)
                self.lstApplConn.SetStringItem(i,6,self.docSetup.getNodeText(recentHostNode,'port'),-1)
            else:
                self.lstApplConn.SetStringItem(i,2,'',-1)
                self.lstApplConn.SetStringItem(i,3,'',-1)
                self.lstApplConn.SetStringItem(i,4,'',-1)
                self.lstApplConn.SetStringItem(i,5,'',-1)
                self.lstApplConn.SetStringItem(i,6,'',-1)
    def OnCbOpenButton(self, event):
        if self.idxSel==-1:
            if self.IsAllSettled()==False:
                return
            if self.IsDataAccessed()==True:
                return
            if self.bOpenAll:
                return
            if self.IsAllClosed():
                netDoc=self.dictNet[self.lstNetAppl[0]]
                netDoc.SetFlag(netDoc.CFG_VALID,False)
                for netDoc in self.dictNet.values():
                    netDoc.SetFlag(netDoc.OPENED,False)
                    netDoc.Open()
                self.bOpenAll=True
            #self.__updateApplButtons__()
        else:
            netDoc=self.dictNet[self.lstNetAppl[self.idxSel]]
            if netDoc.IsRunning():
                return
            if netDoc.IsFlag(netDoc.CONN_ACTIVE)==False:
                self.Show(False)
                dlg = wx.FileDialog(self, "Open", ".", "", "XML files (*.xml)|*.xml|all files (*.*)|*.*", wx.OPEN)
                try:
                    self.__updateListCfg__(self.idxSel)
                    cfgNode=self.__getCfgNode__()
                    dn=self.docSetup.getNodeText(cfgNode,'path')
                    recentNode=self.netMaster.__getRecentAlias__()
                    if recentNode is None:
                        recentNode=self.netMaster.__createRecentAlias__('offline')
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurWX(vtLog.DEBUG,'recent:%s'%recentNode,self)
                    
                    recentAliasNode=self.netMaster.__getRecentConnection__(recentNode)
                    if recentAliasNode is None:
                        recentAliasNode=self.netMaster.__createRecentConnection__(recentNode,'offline')
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurWX(vtLog.DEBUG,'connection:%s'%recentAliasNode,self)
                    
                    recentHostNode=self.netMaster.__getApplHost__(recentAliasNode,netDoc.appl)
                    netDoc.GenAutoFN(recentHostNode,dn)
                    fn=netDoc.GetFN()
                    if fn is not None:
                        dlg.SetPath(fn)
                    if dlg.ShowModal() == wx.ID_OK:
                        filename = dlg.GetPath()
                        netDoc.Open(filename)
                        if recentHostNode is None:
                            recentHostNode=self.netMaster.__createRecentHost__(recentAliasNode,netDoc.appl,filename)
                            self.docSetup.Save()
                        vtLog.vtLngCallStack(None,vtLog.DEBUG,filename,verbose=1)
                        self.lstApplConn.SetStringItem(self.idxSel,5,netDoc.GetFN(),-1)
                finally:
                    dlg.Destroy()
                self.Show(True)
            else:
                sMsg=u'Connection still active! \n\nClose connection to use this feautre.'
                dlg=wx.MessageDialog(self,sMsg ,
                            u'vNetMasterDlg',
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg.Destroy()
                return
        event.Skip()

    def OnCbSaveButton(self, event):
        if self.idxSel==-1:
            if self.IsAllSettled()==False:
                return
            if self.IsDataAccessed()==True:
                return
            netDoc=self.dictNet[self.lstNetAppl[0]]
            netDoc.SetFlag(netDoc.CFG_VALID,False)
            for netDoc in self.dictNet.values():
                netDoc.Pause(True)
                if netDoc.WaitNotRunning(5)==False:
                    vtLog.vtLngCallStack(None,vtLog.ERROR,
                            'name:%s;netDoc:%s'%(self.lstNetAppl[self.idxSel],repr(netDoc)),origin='vNetMasterDlg')
                    sMsg=_(u'Timeout on pausing')+' %s.'%self.lstNetAppl[self.idxSel]
                    dlg=wx.MessageDialog(self,sMsg ,
                                u'vNetMasterDlg',
                                wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                    dlg.ShowModal()
                    dlg.Destroy()
                    continue
                if netDoc.IsFlag(netDoc.OPENED):
                    #netDoc.AlignDoc()
                    netDoc.Save()
                netDoc.Pause(False)
        else:
            netDoc=self.dictNet[self.lstNetAppl[self.idxSel]]
            netDoc.Pause(True)
            if netDoc.WaitNotRunning(5)==False:
            #if netDoc.IsRunning():
                vtLog.vtLngCallStack(None,vtLog.ERROR,
                        'name:%s;netDoc:%s'%(self.lstNetAppl[self.idxSel],repr(netDoc)),origin='vNetMasterDlg')
                sMsg=_(u'Timeout on pausing')+' %s.'%self.lstNetAppl[self.idxSel]
                dlg=wx.MessageDialog(self,sMsg ,
                            u'vNetMasterDlg',
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg.Destroy()
                return
            if netDoc.IsFlag(netDoc.OPENED):
                #netDoc.AlignDoc()
                netDoc.Save()
            netDoc.Pause(False)
        event.Skip()

    def OnLstApplConnLeftDclick(self, event):
        self.OnCbConnButton(None)
        event.Skip()
    def Stop(self):
        self.Show(False)
        self.bBlockNotify=True
        dlg=vNetXmlGuiMasterCloseDialog(self)
        dlg.Centre()
        dlg.SetDictNet(self.dictNet)
        dlg.ShowModal()
        time.sleep(2)
    def ShutDown(self):
        for k in self.dictNet.keys():
            netDoc=self.dictNet[k]
            netDoc.SetFlag(netDoc.SHUTDOWN,True)
        self.thdLog.Stop()
        while self.thdLog.IsRunning():
            time.sleep(1)
        self.Stop()

    def OnCbStopButton(self, event):
        event.Skip()

    def __updateConnState__(self):
        iSel=self.chcConn.GetSelection()
        netDoc=self.dictNet[self.lstNetAppl[iSel]]
        
        self.lstConnState.SetStringItem(0,1,netDoc.GetStatusStr(),-1)
        
        val=netDoc.IsFlag(netDoc.DATA_ACCESS)
        self.lstConnState.SetItemImage(1,val)
        self.lstConnState.SetStringItem(1,1,self.STATE_MAP[1][val],-1)
        
        val=netDoc.IsFlag(netDoc.BLOCK_NOTIFY)
        self.lstConnState.SetItemImage(2,val)
        self.lstConnState.SetStringItem(2,1,self.STATE_MAP[2][val],-1)
        
        val=netDoc.IsFlag(netDoc.OPENED)
        self.lstConnState.SetItemImage(3,val)
        self.lstConnState.SetStringItem(3,1,self.STATE_MAP[3][val],-1)
        
        val=netDoc.IsFlag(netDoc.CFG_VALID)
        self.lstConnState.SetItemImage(4,val)
        self.lstConnState.SetStringItem(4,1,self.STATE_MAP[4][val],-1)
        
        val=netDoc.IsFlag(netDoc.SYNCHABLE)
        self.lstConnState.SetItemImage(5,val)
        self.lstConnState.SetStringItem(5,1,self.STATE_MAP[5][val],-1)
        
        val=netDoc.IsFlag(netDoc.CONN_ACTIVE)
        self.lstConnState.SetItemImage(6,val)
        self.lstConnState.SetStringItem(6,1,self.STATE_MAP[6][val],-1)
        
        val=netDoc.IsFlag(netDoc.CONN_PAUSE)
        self.lstConnState.SetItemImage(7,val)
        self.lstConnState.SetStringItem(7,1,self.STATE_MAP[7][val],-1)
    def __getNodeInfoBySynch__(self,netDoc,k,synchInfo):
        sID='-1'
        sName=''
        node=None
        if k in ['edit','edit_fault','add','add_fault','move','move_fault','ident']:
            sID=synchInfo
            node=netDoc.getNodeById(sID)
            sName=netDoc.ConvertIdNum2Str(long(sID))
        elif k in ['edit_offline','add_offline']:
            node=k
            sName=netDoc.getKey(k)
        elif k in ['del','del_fault']:
            sName=synchInfo
        return long(sID),sName,node
    def __updateConnSynch__(self):
        iSel=self.chcConnSynch.GetSelection()
        netDoc=self.dictNet[self.lstNetAppl[iSel]]
        sSynchFN=netDoc.GetSynchFN()
        self.docSynch.Open(sSynchFN)
        self.trlstSynch.SetNode(None)
        
    def OnChcConnChoice(self, event):
        iSel=self.chcConn.GetSelection()
        self.slConn.SetValue(iSel)
        self.__updateConnState__()
        event.Skip()

    def OnSlConnScroll(self, event):
        iSel=self.slConn.GetValue()
        self.chcConn.SetSelection(iSel)
        self.__updateConnState__()
        event.Skip()

    def OnChcConnSynchChoice(self, event):
        iSel=self.chcConnSynch.GetSelection()
        self.slConnSynch.SetValue(iSel)
        event.Skip()

    def OnSlConnSynchScroll(self, event):
        iSel=self.slConnSynch.GetValue()
        self.chcConnSynch.SetSelection(iSel)
        event.Skip()

    def OnNbMainNotebookPageChanging(self, event):
        event.Skip()

    def OnNbMainNotebookPageChanged(self, event):
        old = event.GetOldSelection()
        sel = event.GetSelection()
        #if sel>=len(self.lstPanel):
        #    return
        try:
            if sel==0:
                self.__updateConnState__()
        except:
            pass
        event.Skip()

    def OnCbUpdateSynchButton(self, event):
        self.__updateConnSynch__()
        event.Skip()

    def OnSynchTreeItemSel(self,evt):
        pass
    def OnSynchAddElement(self,evt):
        pass
    def OnSynchAddFinished(self,evt):
        pass
