#!/usr/bin/env python
#Boa:App:BoaApp
#----------------------------------------------------------------------------
# Name:         vNetXmlSynchAppl.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vNetXmlSynchAppl.py,v 1.2 2007/07/28 14:43:30 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import os,sys,getopt

import vNetXmlSynchFrame
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.tool.log.vtLog as vtLog

modules ={u'vNetXmlSynchFrame': [1,
                        'Main frame of Application',
                        u'vNetXmlSynchFrame.py']}

class BoaApp(wx.App):
    def OnInit(self):
        wx.InitAllImageHandlers()
        self.main = vNetXmlSynchFrame.create(None)
        self.main.Show()
        self.SetTopWindow(self.main)
        return True

def main():
    optshort,optlong='l:f:c:h',['lang=','file=','help']
    vtLgBase.initAppl(optshort,optlong,'vNetXmlSynch')
    vtLog.vtLngInit('vNetXmlSynchAppl','vNetXmlSynchAppl.log',vtLog.DEBUG)

    application = BoaApp(0)
    application.MainLoop()

if __name__ == '__main__':
    main()
