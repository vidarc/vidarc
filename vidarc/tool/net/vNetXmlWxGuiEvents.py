#----------------------------------------------------------------------------
# Name:         vNetXmlWxGuiEvents.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060418
# CVS-ID:       $Id: vNetXmlWxGuiEvents.py,v 1.4 2009/10/11 21:06:35 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_OPEN_START
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_OPEN_OK
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_OPEN_FAULT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_SYNCH_START
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_SYNCH_PROC
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_SYNCH_FINISHED
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_SYNCH_ABORTED
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_CONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_CONNECTED
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_CONNECT_FAULT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_CONFIG
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_ADD_ELEMENT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_CLOSED
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_LOGIN
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_LOGGEDIN
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_BROWSE
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_CMD
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_BUILD_TREE
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_GOT_CONTENT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_GET_NODE
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_REMOVE_NODE
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_LOCK
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_LOCK_BLOCK
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_LOCK_REQUEST
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_UNLOCK
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_SET_NODE
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_ADD_NODE
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_ADD_NODE_RESPONSE
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_DEL_NODE
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_MOVE_NODE
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_MOVE_NODE_RESPONSE
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_SETUP_CHANGED
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_CONTENT_CHANGED
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_SELECT

from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_OPEN_START_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_OPEN_OK_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_OPEN_FAULT_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_SYNCH_START_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_SYNCH_PROC_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_SYNCH_FINISHED_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_SYNCH_ABORTED_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_CONNECT_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_CONNECTED_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_CONNECT_FAULT_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_DISCONNECT_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_CONFIG_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_ADD_ELEMENT_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_CLOSED_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_LOGIN_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_LOGGEDIN_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_BROWSE_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_CMD_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_BUILD_TREE_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_GOT_CONTENT_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_GOT_CONTENT_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_GET_NODE_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_REMOVE_NODE_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_LOCK_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_LOCK_BLOCK_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_LOCK_REQUEST_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_UNLOCK_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_SET_NODE_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_ADD_NODE_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_ADD_NODE_RESPONSE_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_DEL_NODE_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_MOVE_NODE_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_MOVE_NODE_RESPONSE_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_SETUP_CHANGED_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_CONTENT_CHANGED_DISCONNECT
from vidarc.tool.xml.vtXmlWxGuiEvents import EVT_NET_XML_SELECT_DISCONNECT
