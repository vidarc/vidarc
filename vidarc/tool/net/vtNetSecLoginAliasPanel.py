#Boa:FramePanel:vtNetSecLoginAliasPanel
#----------------------------------------------------------------------------
# Name:         vtNetSecLoginAliasPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060416
# CVS-ID:       $Id: vtNetSecLoginAliasPanel.py,v 1.21 2007/10/13 21:18:03 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import sys

from vidarc.tool.net.vtNetSecLoginDialog import vtNetSecLoginDialog
from vidarc.tool.net.vtNetSecLoginAliases import vtNetSecLoginAliases
#from vidarc.tool.net.vtNetSecLoginAppl import vtNetSecLoginAppl
from vidarc.tool.net.vtNetSecLoginHost import *
from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.config.vcCust as vcCust

[wxID_VTNETSECLOGINALIASPANEL, wxID_VTNETSECLOGINALIASPANELCBADD, 
 wxID_VTNETSECLOGINALIASPANELCBDEL, wxID_VTNETSECLOGINALIASPANELCBEDIT, 
 wxID_VTNETSECLOGINALIASPANELCHCAPPL, wxID_VTNETSECLOGINALIASPANELCHCIDENT, 
 wxID_VTNETSECLOGINALIASPANELCHCSTORE, wxID_VTNETSECLOGINALIASPANELLSTHOST, 
] = [wx.NewId() for _init_ctrls in range(8)]

wxEVT_VTSEC_LOGIN_ALIAS_CHANGED=wx.NewEventType()
vEVT_VTSEC_LOGIN_ALIAS_CHANGED=wx.PyEventBinder(wxEVT_VTSEC_LOGIN_ALIAS_CHANGED,1)
def EVT_VTSEC_LOGIN_ALIAS_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTSEC_LOGIN_ALIAS_CHANGED,func)
class vtSecLoginAliasChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTSEC_LOGIN_ALIAS_CHANGED(<widget_name>, self.OnMaster)
    """

    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VTSEC_LOGIN_ALIAS_CHANGED)
        self.obj=obj
    def GetObject(self):
        return self.obj


class vtNetSecLoginAliasPanel(wx.Panel,vtXmlDomConsumer):
    VERBOSE=0
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(3)
        parent.AddGrowableCol(0)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbEdit, 0, border=4, flag=wx.BOTTOM)
        parent.AddWindow(self.cbDel, 0, border=4, flag=wx.BOTTOM)
        parent.AddWindow(self.cbAdd, 0, border=4, flag=0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.chcAppl, 1, border=4,
              flag=wx.RIGHT | wx.TOP | wx.LEFT | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsCfg, 1, border=4,
              flag=wx.EXPAND | wx.BOTTOM | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.lstHost, 1, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.BOTTOM | wx.TOP | wx.LEFT)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.RIGHT | wx.LEFT)

    def _init_coll_bxsCfg_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.chcIdent, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.chcStore, 1, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_lstHost_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT, heading=_(u'Appl'),
              width=80)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT, heading=_(u'Alias'),
              width=80)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT, heading=_(u'Host'),
              width=80)
        parent.InsertColumn(col=3, format=wx.LIST_FORMAT_LEFT, heading=_(u'Login'),
              width=60)
        parent.InsertColumn(col=4, format=wx.LIST_FORMAT_LEFT, heading=_(u'Lv'),
              width=30)
        parent.InsertColumn(col=5, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'Groups'), width=-1)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=2, hgap=0, rows=4, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsCfg = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsCfg_Items(self.bxsCfg)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTNETSECLOGINALIASPANEL,
              name=u'vtNetSecLoginAliasPanel', parent=prnt, pos=wx.Point(414,
              248), size=wx.Size(269, 188), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(261, 161))

        self.chcAppl = wx.Choice(choices=[],
              id=wxID_VTNETSECLOGINALIASPANELCHCAPPL, name=u'chcAppl',
              parent=self, pos=wx.Point(4, 4), size=wx.Size(214, 21), style=0)
        self.chcAppl.SetMinSize(wx.Size(-1, -1))
        self.chcAppl.Bind(wx.EVT_CHOICE, self.OnChcApplChoice,
              id=wxID_VTNETSECLOGINALIASPANELCHCAPPL)

        self.chcIdent = wx.CheckBox(id=wxID_VTNETSECLOGINALIASPANELCHCIDENT,
              label=_(u'identical login'), name=u'chcIdent', parent=self,
              pos=wx.Point(4, 37), size=wx.Size(107, 13), style=0)
        self.chcIdent.SetValue(False)
        self.chcIdent.SetMinSize(wx.Size(-1, -1))
        self.chcIdent.Bind(wx.EVT_CHECKBOX, self.OnChcIdentCheckbox,
              id=wxID_VTNETSECLOGINALIASPANELCHCIDENT)

        self.chcStore = wx.CheckBox(id=wxID_VTNETSECLOGINALIASPANELCHCSTORE,
              label=_(u'store password'), name=u'chcStore', parent=self,
              pos=wx.Point(115, 37), size=wx.Size(103, 13), style=0)
        self.chcStore.SetValue(False)
        self.chcStore.SetMinSize(wx.Size(-1, -1))
        self.chcStore.Bind(wx.EVT_CHECKBOX, self.OnChcStoreCheckbox,
              id=wxID_VTNETSECLOGINALIASPANELCHCSTORE)

        self.lstHost = wx.ListCtrl(id=wxID_VTNETSECLOGINALIASPANELLSTHOST,
              name=u'lstHost', parent=self, pos=wx.Point(4, 58),
              size=wx.Size(214, 99), style=wx.LC_REPORT)
        self.lstHost.SetMinSize(wx.Size(-1, -1))
        self._init_coll_lstHost_Columns(self.lstHost)
        self.lstHost.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstHostListColClick,
              id=wxID_VTNETSECLOGINALIASPANELLSTHOST)
        self.lstHost.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstHostListItemDeselected,
              id=wxID_VTNETSECLOGINALIASPANELLSTHOST)
        self.lstHost.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstHostListItemSelected,
              id=wxID_VTNETSECLOGINALIASPANELLSTHOST)

        self.cbEdit = wx.lib.buttons.GenBitmapButton(ID=wxID_VTNETSECLOGINALIASPANELCBEDIT,
              bitmap=vtArt.getBitmap(vtArt.Edit), name=u'cbEdit', parent=self,
              pos=wx.Point(226, 58), size=wx.Size(31, 30), style=0)
        self.cbEdit.Bind(wx.EVT_BUTTON, self.OnCbEditButton,
              id=wxID_VTNETSECLOGINALIASPANELCBEDIT)

        self.cbDel = wx.lib.buttons.GenBitmapButton(ID=wxID_VTNETSECLOGINALIASPANELCBDEL,
              bitmap=vtArt.getBitmap(vtArt.Del), name=u'cbDel', parent=self,
              pos=wx.Point(226, 92), size=wx.Size(31, 30), style=0)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VTNETSECLOGINALIASPANELCBDEL)

        self.cbAdd = wx.lib.buttons.GenBitmapButton(ID=wxID_VTNETSECLOGINALIASPANELCBADD,
              bitmap=vtArt.getBitmap(vtArt.Add), name=u'cbAdd', parent=self,
              pos=wx.Point(226, 126), size=wx.Size(31, 30), style=0)
        self.cbAdd.Bind(wx.EVT_BUTTON, self.OnCbAddButton,
              id=wxID_VTNETSECLOGINALIASPANELCBADD)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        vtXmlDomConsumer.__init__(self)
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        self._init_ctrls(parent)
        self.selIdx=-1
        self.oLogin=None
        self.dlgLogin=None
        self.iMode=-1       #0:edit,1:add
        
        self.__clrPreDefs__()
        
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
        self.fgsData.Fit(self)
    def __clrPreDefs__(self):
        self.sPreDefHost=''
        self.sPreDefPort=''
        self.sPreDefAlias=''
        self.sPreDefUsr=''
        self.sPreDefPwd=''
    def OnLstHostListColClick(self, event):
        self.selIdx=-1
        event.Skip()

    def OnLstHostListItemDeselected(self, event):
        self.selIdx=-1
        event.Skip()

    def OnLstHostListItemSelected(self, event):
        self.selIdx=event.GetIndex()
        event.Skip()
    def OnLoginDlgApply(self,evt):
        if evt is not None:
            evt.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.INFO,'add connection',self)
            sHost=self.dlgLogin.GetHost()
            sPort=self.dlgLogin.GetPort()
            sPasswd=self.dlgLogin.GetPasswd()
            sUsr=self.dlgLogin.GetUsr()
            sUsrId=self.dlgLogin.GetUsrId()
            sGrpIds=self.dlgLogin.GetGrpIds()
            sGrps=self.dlgLogin.GetGrps()
            sSecLv=self.dlgLogin.GetSecLv()
            bAdmin=self.dlgLogin.IsAdmin()
            sAppl=self.dlgLogin.GetAppl()
            sAlias=self.dlgLogin.GetAlias()
            
            if self.iMode==1:
                pass
            elif self.iMode==0:
                idx,bSkip=self.__prepareEdit__(self.selIdx,False)
                if self.VERBOSE:
                    vtLog.CallStack('')
                    print idx,bSkip
                if idx<0:
                    return
            else:
                return
            o=vtNetSecLoginHost(passwd=sPasswd,usr=sUsr,usrId=long(sUsrId),
                        grpIds=sGrpIds,grps=sGrps,iSecLv=sSecLv,bAdmin=bAdmin,
                        host=sHost,port=sPort,appl=sAppl,alias=sAlias)
            if self.iMode==1:
                self.oLogin.AddApplAlias(o,0)
                self.__clrPreDefs__()
                self.__showAppls__()
            elif self.iMode==0:
                self.oLogin.SetAlias(idx,o.GetAppl(),o)
                self.__showLogin__()
            wx.PostEvent(self,vtSecLoginAliasChanged(self))
            vtLog.vtLngCurWX(vtLog.INFO,'done',self)
            self.iMode=-1
        except:
            vtLog.vtLngTB(self.GetName())
    def OnLoginDlgCancel(self,evt):
        if evt is not None:
            evt.Skip()
        self.__clrPreDefs__()
        vtLog.vtLngCurWX(vtLog.INFO,'done',self)
        self.iMode=-1
    def OnCbAddButton(self, event):
        if event is not None:
            event.Skip()
        vtLog.vtLngCurWX(vtLog.INFO,'',self)
        if self.dlgLogin is None:
            self.dlgLogin=vtNetSecLoginDialog(self)
            self.dlgLogin.BindEvents(self.OnLoginDlgApply,self.OnLoginDlgCancel)
            self.dlgLogin.Centre()
        if len(self.sPreDefHost)==0:
            try:
                sHost=vcCust.NET_DFT_XML_SRV_HOST
            except:
                sHost='vidarc'
        else:
            sHost=self.sPreDefHost
        if len(self.sPreDefPort)==0:
            try:
                iPort=vcCust.NET_DFT_XML_SRV_PORT
            except:
                iPort=50000
        else:
            try:
                iPort=int(self.sPreDefPort)
            except:
                iPort=50000
        self.dlgLogin.SetValues(sHost,iPort,
                self.oLogin.GetAppl()[0],
                self.sPreDefAlias,
                self.sPreDefUsr,
                self.sPreDefPwd)
        self.dlgLogin.SetAliasUsed(self.oLogin.GetConnections())
        self.iMode=1        #add
        self.dlgLogin.Show(True)
        return
        iRet=self.dlgLogin.ShowModal()
        vtLog.vtLngCurWX(vtLog.INFO,'add connection',self)
        if iRet>0:
            sHost=self.dlgLogin.GetHost()
            sPort=self.dlgLogin.GetPort()
            sPasswd=self.dlgLogin.GetPasswd()
            sUsr=self.dlgLogin.GetUsr()
            sUsrId=self.dlgLogin.GetUsrId()
            sGrpIds=self.dlgLogin.GetGrpIds()
            sGrps=self.dlgLogin.GetGrps()
            sSecLv=self.dlgLogin.GetSecLv()
            bAdmin=self.dlgLogin.IsAdmin()
            sAppl=self.dlgLogin.GetAppl()
            sAlias=self.dlgLogin.GetAlias()
            o=vtNetSecLoginHost(passwd=sPasswd,usr=sUsr,usrId=long(sUsrId),
                    grpIds=sGrpIds,grps=sGrps,iSecLv=sSecLv,bAdmin=bAdmin,
                    host=sHost,port=sPort,appl=sAppl,alias=sAlias)
            self.oLogin.AddApplAlias(o,0)
            self.__showAppls__()
            wx.PostEvent(self,vtSecLoginAliasChanged(self))
        # clear perdefs
        self.sPreDefHost=''
        self.sPreDefPort=''
        self.sPreDefAlias=''
        self.sPreDefUsr=''
        self.sPreDefPwd=''
        vtLog.vtLngCurWX(vtLog.INFO,'done',self)
    def GetHostAlias(self,appl):
        vtLog.vtLngCurWX(vtLog.INFO,appl,self)
        l=[]
        try:
            node=self.doc.getChildByLst(self.doc.getRoot(),['config',appl])
            #if self.VERBOSE:
            #    vtLog.CallStack('')
            if node is not None:
                for c in self.doc.getChilds(node,'host'):
                    sName=self.doc.getNodeText(c,'name')
                    sPort=self.doc.getNodeText(c,'port')
                    sAlias=self.doc.getNodeText(c,'alias')
                    s=getHostAliasStr(sName,sPort,sAlias)
                    #s=sName+u':'+sPort+u'//'+sAlias
                    vtLog.vtLngCurWX(vtLog.DEBUG,s,self)
                    l.append(s)
        except:
            pass
        return l
    def __prepareEdit__(self,idxAppl,bTry):
        try:
            if self.dlgLogin is None:
                self.dlgLogin=vtNetSecLoginDialog(self)
                self.dlgLogin.BindEvents(self.OnLoginDlgApply,self.OnLoginDlgCancel)
                self.dlgLogin.Centre()
            idx=self.chcAppl.GetSelection()
            lAppls=self.oLogin.GetApplLst(idx)
            o=lAppls[idxAppl]
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'idx:%d;idxAppl:%d;name:%s;lApps:%s;%s;;%s'%(
                            idx,idxAppl,o.GetHostAliasStr(),lAppls,
                            ';'.join([oLog.__str__() for oLog in lAppls]),o.__str__()),self)
            oMain=lAppls[0]
            o.Set2Cfg(False)
            if o.IsOffline() and oMain.IsOffline():
                dlg = wx.FileDialog(self, _(u"Open"), ".", "", _("XML files (*.xml)|*.xml|all files (*.*)|*.*"), wx.OPEN)
                fn=o.GetFN()
                if fn is not None:
                    if len(fn)==0:
                        fn=None
                if fn is not None:
                    dlg.SetPath(fn)
                else:
                    try:
                        par=self.GetParent()
                        dn=par.GetDN()
                        if dn is not None:
                            dlg.SetPath(dn+os.sep)
                    except:
                        vtLog.vtLngTB(self.GetName())
                if dlg.ShowModal() == wx.ID_OK:
                    filename = dlg.GetPath()
                    o.SetFN(filename)
                    self.__showLogin__()
                return -1,False
            if self.VERBOSE:
                vtLog.CallStack('')
                print o.GetUsr(),'applIdx',idxAppl,'try',bTry,lAppls
            try:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'%s;host:%s;port:%s;appl:%s;alias:%s;usr:%s'%(o.__str__(),
                                o.GetHost(),o.GetPort(),o.GetAppl(),o.GetAlias(),o.GetUsr()),self)
            except:
                vtLog.vtLngTB(self.GetName())
            self.dlgLogin.SetValues(o.GetHost(),o.GetPort(),o.GetAppl(),o.GetAlias(),
                    o.GetUsr(),o.GetPasswd(),False,idxAppl==0)
            if idxAppl!=0:
                l=self.GetHostAlias(o.GetAppl())
                sSubAlias=o.GetHostAliasStr()
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'configured aliases;actual:%s;l:%s'%(sSubAlias,
                                    vtLog.pformat(l)),self)
                sMaster=None
                if len(o.GetAlias())==0:
                    sMaster=lAppls[0].GetHostAliasStr()
                    i=sMaster.find('//')
                    if i>0:
                        sMaster=sMaster[:i]
                    else:
                        sMaster=None
                sFound=None
                for s in l:
                    if s==sSubAlias:
                        sFound=s
                        break
                if sFound is None:
                    sFound=self.dlgLogin.SetHostAlias(l,sMaster)
                if self.VERBOSE:
                    print '   ',l,sMaster
                    print 'found',sFound
                if sFound is not None:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'found:%s'%(sFound),self)
                    i=sFound.find('//')
                    #j=sFound.find(':')
                    if self.VERBOSE:
                        print i
                    if i>0:
                        #print sFound[j+1:i]
                        #o.SetPort(sFound[j+1:i])
                        #o.SetAlias(sFound[i+2:])
                        o.SetHostAliasStr(sFound)
                        # 070821:wro TRYME:remove move to provoce fault in 
                        # vtNetSecXmlGuiMaster.py:
                        #       OnCfgSetupApply:(1342) or 
                        #       __updateConfig__(1532)
                        o.SetLoginInfo(oMain)           # 070821:wro enforce same user ; 
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%(o.__str__()),self)
                    if bTry:
                        return idx,True
                else:
                    vtLog.vtLngCurWX(vtLog.ERROR,'appl:%s not found'%(o.GetAppl()),self)
        except:
            vtLog.vtLngTB(self.GetName())
        return idx,False
    def OnCbEditButton(self, event):
        vtLog.vtLngCurWX(vtLog.INFO,self.chcAppl.GetStringSelection(),self)
        if event is not None:
            event.Skip()
        if self.selIdx<0:
            return
        idx,bSkip=self.__prepareEdit__(self.selIdx,False)
        if self.VERBOSE:
            vtLog.CallStack('')
            print idx,bSkip
        if idx<0:
            return
        self.iMode=0        #edit
        if bSkip:
            iRet=1
            self.OnLoginDlgApply(None)
        else:
            self.dlgLogin.Show(True)
            #iRet=self.dlgLogin.ShowModal()
        return
        vtLog.vtLngCurWX(vtLog.INFO,'edit connection',self)
        if iRet>0:
            sHost=self.dlgLogin.GetHost()
            sPort=self.dlgLogin.GetPort()
            sPasswd=self.dlgLogin.GetPasswd()
            sUsr=self.dlgLogin.GetUsr()
            sUsrId=self.dlgLogin.GetUsrId()
            grpIds=self.dlgLogin.GetGrpIds()
            grps=self.dlgLogin.GetGrps()
            sSecLv=self.dlgLogin.GetSecLv()
            bAdmin=self.dlgLogin.IsAdmin()
            sAppl=self.dlgLogin.GetAppl()
            sAlias=self.dlgLogin.GetAlias()
            o=vtNetSecLoginHost(sPasswd,usr=sUsr,usrId=long(sUsrId),
                    grpIds=grpIds,grps=grps,iSecLv=sSecLv,bAdmin=bAdmin,
                    host=sHost,port=sPort,appl=sAppl,alias=sAlias)
            self.oLogin.SetAlias(idx,o.GetAppl(),o)
            self.__showLogin__()
            wx.PostEvent(self,vtSecLoginAliasChanged(self))
        vtLog.vtLngCurWX(vtLog.INFO,'done',self)
    def OnCbDelButton(self, event):
        if event is not None:
            event.Skip()
        idx=self.chcAppl.GetSelection()
        if self.selIdx<0:
            self.oLogin.DelAppl(idx)
            self.__showAppls__()
            wx.PostEvent(self,vtSecLoginAliasChanged(self))
            return
        self.oLogin.DelAlias(idx,self.selIdx)
        self.__showLogin__()
        wx.PostEvent(self,vtSecLoginAliasChanged(self))
    def UpdateData(self):
        self.__showAppls__()
    def __showAppls__(self):
        self.chcAppl.Clear()
        l=self.oLogin.GetAppls()
        a=self.oLogin.GetAppl()
        idx=0
        for o in l:
            sName=o.GetName()
            sName=o.GetHostAliasStr(a[0])
            self.chcAppl.Append(sName)
            try:
                lAppls=self.oLogin.GetApplLst(idx)
                idx+=1
                vtLog.vtLngCurWX(vtLog.DEBUG,'idx:%d;appl:%s;%s'%(
                            idx,sName,
                            ';'.join([oLog.__str__() for oLog in lAppls])),self)
            except:
                vtLog.vtLngTB(self.GetName())
        self.chcAppl.SetSelection(0)
        self.__showLogin__()
    def __showLogin__(self):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        idx=self.chcAppl.GetSelection()
        lAppls=self.oLogin.GetApplLst(idx)
        self.lstHost.DeleteAllItems()
        try:
            a=self.oLogin.GetApplLogin(idx)
            if a is None:
                self.chcStore.SetValue(0)
                self.chcIdent.SetValue(1)
            else:
                self.chcStore.SetValue(a.IsStore())
                self.chcIdent.SetValue(a.IsIdent())
            
            #docHum=self.oLogin.GetHumanDoc()
            for o in lAppls:
                sAppl=o.GetAppl()
                sAlias=o.GetAlias()
                idx=self.lstHost.InsertStringItem(sys.maxint,sAppl)
                sUsr=o.GetUsr()
                sUsrId=o.GetUsrId()
                    
                #if o.GetUsrId()>=0:
                #    tmp=docHum.getNodeByIdNum(o.GetUsrId())
                #    if tmp is not None:
                #        sUsr=docHum.getNodeText(tmp,'name')
                if o.IsOffline():
                    self.lstHost.SetStringItem(idx,1,o.GetFN())
                    continue
                else:
                    self.lstHost.SetStringItem(idx,1,sAlias)
                    self.lstHost.SetStringItem(idx,2,o.GetHost())
                    self.lstHost.SetStringItem(idx,3,sUsr)
                    self.lstHost.SetStringItem(idx,4,'%02d'%o.GetSecLv())
                lGrps=[]
                try:
                    lGrps=o.GetGrps()
                except:
                    pass
                #l=o.GetGrpIds()
                #for i in l:
                #    try:
                #        tmp=docHum.getNodeByIdNum(i)
                #        if tmp is not None:
                #            lGrps.append(docHum.getNodeText(tmp,'name'))
                #        else:
                #            lGrps.append(str(i))
                #    except:
                #        pass
                lGrps.sort()
                self.lstHost.SetStringItem(idx,5,','.join(lGrps))
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'idx:%d;appl:%s;%s'%(
                            idx,self.chcAppl.GetStringSelection(),
                            ';'.join([oLog.__str__() for oLog in lAppls])),self)
        except:
            vtLog.vtLngTB(self.GetName())
    def SetNetSecLogin(self,o=None):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if o is None:
            o=vtNetSecLoginAliases([])
        self.oLogin=o
        self.__showAppls__()
    def GetNetSecLogin(self):
        return self.oLogin
    
    def SetDoc(self,doc,bNet=False):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if self.VERBOSE:
            vtLog.CallStack('')
        vtXmlDomConsumer.SetDoc(self,doc)
        if doc is not None:
            self.doc.acquire('dom')
            try:
                nodeTmp=self.doc.getChild(self.doc.getRoot(),'config')
                conn=self.oLogin.GetConnection()
                idx=0
                l=self.oLogin.GetAppls()
                lAppls=self.oLogin.GetApplLst(0)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'conn:%s;appls:%s'%(conn,l),self)
                    vtLog.vtLngCurWX(vtLog.DEBUG,'idx:%d;appl:%s;%s'%(
                                idx,self.chcAppl.GetStringSelection(),
                                ';'.join([oLog.__str__() for oLog in lAppls])),self)
                try:
                    bOffline=lAppls[0].IsOffline()
                    for a in lAppls[1:]:
                        a.SetOffline(bOffline)
                        if bOffline==False:
                            a.Set2Cfg(True)
                        #a.SetUsr('')
                except:
                    vtLog.vtLngTB(self.GetName())
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'idx:%d;appl:%s;%s'%(
                                idx,self.chcAppl.GetStringSelection(),
                                ';'.join([oLog.__str__() for oLog in lAppls])),self)
            finally:
                self.doc.release('dom')
    def SetLoginDoc(self,doc,bNet=False):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        try:
            self.oLogin.SetDoc(doc,bNet)
        except:
            vtLog.vtLngTB(self.GetName())
    def SetLoginNode(self,node):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        try:
            self.oLogin.SetNode(node)
            self.__showAppls__()
        except:
            vtLog.vtLngTB(self.GetName())
    def GetLoginNode(self,node=None):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        try:
            self.oLogin.GetNode(node)
        except:
            vtLog.vtLngTB(self.GetName())
    def GetNode(self,node=None):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        try:
            self.oLogin.GetNode(node)
        except:
            vtLog.vtLngTB(self.GetName())
    def AddHost(self,obj):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.oLogin.AddHost(obj)
        self.__showLogin__()

    def OnChcApplChoice(self, event):
        self.__showLogin__()
        event.Skip()
    def GetObjConnections(self):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        try:
            return self.oLogin.GetAppls()
        except:
            vtLog.vtLngTB(self.GetName())
            return []
    def GetConnections(self):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        try:
            return self.oLogin.GetConnections()
        except:
            vtLog.vtLngTB(self.GetName())
            return []
    def SelectConnection(self,sName):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'name:%s'%(sName),self)
        try:
            iRet=self.oLogin.SelectConnection(sName)
            if iRet==-2:
                try:
                    
                    self.sPreDefHost,self.sPreDefPort,self.sPreDefUsr,self.sPreDefAlias=setHostConnectionStr(sName)
                    self.sPreDefPwd=''
                    #self.OnCbAddButton(None)
                    # 070207:wro skip dialog popup
                    if len(self.sPreDefHost)==0:
                        try:
                            sHost=vcCust.NET_DFT_XML_SRV_HOST
                        except:
                            sHost='vidarc'
                    else:
                        sHost=self.sPreDefHost
                    if len(self.sPreDefPort)==0:
                        try:
                            iPort=vcCust.NET_DFT_XML_SRV_PORT
                        except:
                            iPort=50000
                    else:
                        try:
                            iPort=int(self.sPreDefPort)
                        except:
                            iPort=50000
                    o=vtNetSecLoginHost(passwd=self.sPreDefPwd,
                            usr=self.sPreDefUsr,#usrId=long(sUsrId),
                            #grpIds=sGrpIds,grps=sGrps,iSecLv=sSecLv,bAdmin=bAdmin,
                            host=sHost,port=str(iPort),
                            appl=self.oLogin.GetAppl()[0],
                            alias=self.sPreDefAlias)
                    sNameNew=o.GetHostConnectionStr()
                    if sName!=sNameNew:
                        if vtLog.vtLngIsLogged(vtLog.ERROR):
                            vtLog.vtLngCurWX(vtLog.ERROR,'name:%s changed to %s'%(sName,sNameNew),self)
                    self.oLogin.AddApplAlias(o,0)
                    iRet=self.oLogin.SelectConnection(sNameNew)
                except:
                    vtLog.vtLngTB(self.GetName())
                    return -2
            return iRet
        except:
            vtLog.vtLngTB(self.GetName())
            return -1
    def SelectSelectedConnection(self):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        try:
            sName=self.chcAppl.GetStringSelection()
            self.oLogin.SelectConnection(sName)
        except:
            vtLog.vtLngTB(self.GetName())
    def GetConnection(self,sName=None):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        try:
            return self.oLogin.GetConnection(sName)
        except:
            vtLog.vtLngTB(self.GetName())
            return None
    def GetConnectionAppl2Cfg(self):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        try:
            return self.oLogin.GetConnectionAppl2Cfg()
        except:
            vtLog.vtLngTB(self.GetName())
            return None
    def ConfigAppl(self,appl):
        vtLog.vtLngCurWX(vtLog.INFO,'appl:%s'%(appl),self)
        if self.selIdx>=0:
            self.lstHost.SetItemState(self.selIdx,0,wx.LIST_STATE_SELECTED)
            pass
        self.selIdx=self.oLogin.GetAppl().index(appl)
        idx,bSkip=self.__prepareEdit__(self.selIdx,True)
        self.lstHost.SetItemState(self.selIdx,wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
        if bSkip:
            return True
        else:
            wx.CallAfter(self.OnCbEditButton,None)
            return False
    def ConfigAdd(self):
        vtLog.vtLngCurWX(vtLog.INFO,'',self)
        wx.CallAfter(self.OnCbAddButton,None)
    def ConfigDel(self,sConn):
        vtLog.vtLngCurWX(vtLog.INFO,'conn:%s'%(sConn),self)
        try:
            self.chcAppl.SetStringSelection(sConn)
            wx.CallAfter(self.OnCbDelButton,None)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnChcStoreCheckbox(self, event):
        idx=self.chcAppl.GetSelection()
        o=self.oLogin.GetApplLogin(idx)
        if o is not None:
            o.SetStore(self.chcStore.GetValue())
        event.Skip()
    def OnChcIdentCheckbox(self, event):
        idx=self.chcAppl.GetSelection()
        o=self.oLogin.GetApplLogin(idx)
        if o is not None:
            o.SetIdent(self.chcIdent.GetValue())
        event.Skip()
        
