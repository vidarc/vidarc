#----------------------------------------------------------------------------
# Name:         vtNetArt.py
# Purpose:      
#               
# Author:       Walter Obweger
#
# Created:      20070617
# CVS-ID:       $Id: vtNetArt.py,v 1.4 2010/02/17 19:53:19 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.net.img_gui as images
import fnmatch
#import numpy

__j=0
lNames=[]
lIds=[]
#def makeByteArray(shape):
#    return numpy.empty(shape, numpy.uint8)

def addBitmaps(name):
    global __j
    global lNames
    if fnmatch.fnmatch(name,'get*Bitmap'):
        __j+=1
        lNames.append(name)
        #s='%s=%d'%(name[3:-6],__j)
        s='%s="%s"'%(name[3:-6],name)
        #print s
        lIds.append(s)
        __j+=1
        s='%sAlpha="get%sImageAlpha"'%(name[3:-6],name[3:-6])#name)
        lIds.append(s)
        #exec(s)
    return __j

__i=0
def add():
    global __i
    __i+=1
    return __i

map(addBitmaps,dir(images))
#print lNames
#print lIds
for it in lIds:
    exec(it)

def getAlpha(img):
    if img.HasMask():
        maskColor = (img.GetMaskRed(), img.GetMaskGreen(), img.GetMaskBlue())
    else:
        maskColor = None
    img.InitAlpha()
    w,h=img.GetWidth(),img.GetHeight()
    #arr=makeByteArray((w,h,1))
    #print arr
    #arr[:,:,0]=0
    #img.SetAlphaData(arr)
    data = map(ord, list(img.GetData()))
    #print len(data),w*h
    x,y=0,0
    for i in range(0, len(data), 3):
        pixel = (data[i], data[i+1], data[i+2])
        #pixel = makeAlpha(pixel, maskColor)
        #print x,y,pixel,maskColor
        if pixel != maskColor:
            #arr[x,y,0]=128
            img.SetAlpha(x,y,128)
        #else:
        #    img.SetAlpha(x,y,0)
        #    arr[x,y,0]=255
        x+=1
        if x>=w:
            x=0
            y+=1
        #for x in range(3):
        #    data[i+x] = pixel[x]
    #print ''.join(map(chr, data))
    #print arr
    #img.SetAlphaData(arr)
    #print img.GetAlpha(4,4)
    bmp=img.ConvertToBitmap()
    img.Destroy()
    return bmp

class vtNetArtProvider:#(wx.ArtProvider):
    def __init__(self):
        #wx.ArtProvider.__init__(self)
        #wx.ArtProvider.Push(self)
        self.bmpNull=wx.NullBitmap
        #self.Push(self)
        self.dCache={}
    def GetBitmap(self,artid,client='VIDARC',sz=(16,16)):
        if artid in self.dCache:
            return self.dCache[artid]
        else:
            img=self.CreateBitmap(artid,client,sz)
            self.dCache[artid]=img
            return img
    def CreateBitmap(self, artid, client, size):
        try:
            if artid.endswith('Alpha'):
                bmp=getAlpha(getattr(images,artid[:-5])())
            else:
                bmp=getattr(images,artid)()
            return bmp
        except:
            pass
        return self.bmpNull

__netProv=None
__bInstalled=False

def Install():
    #import traceback
    #traceback.print_stack()
    global __bInstalled
    #print 'Install',__bInstalled
    if __bInstalled:
        return
    __bInstalled=True
    global __netProv
    __netProv=vtNetArtProvider()
    return
    if wx.VERSION >= (2.8):
        wx.ArtProvider.Push(__netProv)
    else:
        wx.ArtProvider_PushProvider(__netProv)
    #if wx.VERSION >= (2.8):
    #    wx.ArtProvider.PushProvider(__netProv)
    #else:
    #    wx.ArtProvider_PushProvider(__netProv)
def DeInstall():
    global __bInstalled
    if __bInstalled==False:
        return
    __bInstalled=False
    global __netProv
    __netProv=None
    return
    if wx.VERSION >= (2.8):
        wx.ArtProvider.Pop()
    else:
        wx.ArtProvider_PopProvider()
    #if wx.VERSION >= (2.8):
    #    wx.ArtProvider.Delete(__netProv)
    #wx.ArtProvider_PopProvider()

def getBitmap(artid,client='VIDARC',sz=(16,16)):
    global __bInstalled
    if __bInstalled:
        #return wx.ArtProvider_GetBitmap(artid, client, sz)
        pass
    else:
        Install()
        #return wx.ArtProvider_GetBitmap(artid, client, sz)
    global __netProv
    return __netProv.GetBitmap(artid, client, sz)
    #return images.getNoIconBitmap()

def getIcon(bmp):
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(bmp)
    return icon

#Install()
