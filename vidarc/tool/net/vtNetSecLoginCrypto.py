#----------------------------------------------------------------------------
# Name:         vNetSecLoginCrypto.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060413
# CVS-ID:       $Id: vtNetSecLoginCrypto.py,v 1.1 2006/04/19 11:52:17 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import ezPyCrypto,os.path,os
import libxml2

from vidarc.tool.net.vtNetSecLogin import vtNetSecLogin
from vidarc.tool.net.vtNetSecLoginHost import vtNetSecLoginHost
from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer

import vidarc.tool.log.vtLog as vtLog

class vtNetSecLoginCrypto(vtNetSecLogin):
    PREFIX='<StartPycryptoMessage>\n'
    SUFFIX='<EndPycryptoMessage>\n'
    def __init__(self,phrase,fn=None,dn='~',keySize=2048):
        vtNetSecLogin.__init__(self)
        self.SetDN(dn)
        self.SetFN(fn)
        self.phrase=phrase
        self.key=ezPyCrypto.key(keySize,passphrase=phrase)
    def SetDN(self,dn):
        self.sDN=os.path.expanduser(dn)
    def SetFN(self,fn):
        if fn is not None:
            try:
                sFN=os.path.join(self.sDN,fn)
                self.sFN=fn
                self.sFullFN=sFN
            except:
                self.sFullFN=None
    def ImportKey(self,fn=None,dn='~',phrase=None):
        self.SetDN(dn)
        self.SetFN(fn)
        if fn is None:
            return False
        try:
            if os.path.exists(os.path.join(dn,fn)):
                f=open(self.sFullFN,'rs')
                sKey=f.read()
                f.close()
                return self.key.importKey(sKey)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
            return False
    def ImportPrivKey(self,fn=None,dn='~',phrase=None):
        self.SetDN(dn)
        self.SetFN(fn)
        if fn is None:
            return False
        try:
            if os.path.exists(self.sFullFN):
                if phrase is None:
                    phrase=self.phrase
                if phrase is None:
                    phrase=''
                f=open(self.sFullFN,'r')
                sKey=f.read()
                f.close()
                return self.key.importKey(sKey,passphrase=phrase)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
            return False
    def ExportKey(self,fn=None,dn='~',phrase=None):
        self.SetDN(dn)
        self.SetFN(fn)
        try:
            sDN,sFN=os.path.split(self.sFullFN)
            try:
                os.makedirs(sDN)
            except:
                return False
            f=open(self.sFullFN,'w')
            f.write(self.key.exportKey())
            f.close()
            return True
        except:
            vtLog.vtLngTB(self.__class__.__name__)
            return False
    def ExportPrivKey(self,fn=None,dn='~',phrase=None):
        self.SetDN(dn)
        self.SetFN(fn)
        try:
            sDN,sFN=os.path.split(self.sFullFN)
            try:
                os.makedirs(sDN)
            except:
                pass#return False
            if phrase is None:
                phrase=self.phrase
            if phrase is None:
                phrase=''
            f=open(self.sFullFN,'w')
            f.write(self.key.exportKeyPrivate(passphrase=phrase))
            f.close()
            return True
        except:
            vtLog.vtLngTB(self.__class__.__name__)
            return False
    def SetNode(self,node,bNet=False):
        self.Clear()
        vtXmlDomConsumer.SetNode(self,node)
        if self.doc is None:
            return
        sData=self.doc.getText(self.node)
        sDataEnc=self.PREFIX+sData+self.SUFFIX
        try:
            sDataDec=self.key.decStringFromAscii(sDataEnc)
            tmpDoc=libxml2.parseMemory(sDataDec,len(sDataDec))
            tmp=tmpDoc.getRootElement()
            vtNetSecLogin.SetNode(self,tmp)
            vtXmlDomConsumer.SetNode(self,node)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def GetNode(self,node=None):
        if node is None:
            node=self.node
        if node is None:
            return
        docTmp=libxml2.newDoc('1.0')
        root=docTmp.newDocNode(None,'input','')
        docTmp.addChild(root)
        vtNetSecLogin.GetNode(self,root)
        s=docTmp.serialize('ISO-8859-1')
        sEnc=self.key.encStringToAscii(s)
        self.doc.setText(node,sEnc[len(self.PREFIX):-len(self.SUFFIX)])
    
