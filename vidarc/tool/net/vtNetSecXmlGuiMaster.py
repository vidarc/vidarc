#----------------------------------------------------------------------------
# Name:         vtNetSecXmlGuiMaster.py
# Purpose:      
#               dervied from vNetXmlGuiMaster
# Author:       Walter Obweger
#
# Created:      20060415
# CVS-ID:       $Id: vtNetSecXmlGuiMaster.py,v 1.49 2008/03/24 11:07:26 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.buttons import GenBitmapButton

#from vidarc.tool.net.vtNetSecXmlGuiMasterPopupWindow import *
from vidarc.tool.net.vtNetSecXmlGuiMasterDialog import *
#from vidarc.tool.net.vNetCfgDialog import vNetCfgDialog
from vidarc.tool.net.vtNetSecCfgDialog import vtNetSecCfgDialog
from vidarc.tool.net.vNetLoginDialog import vNetLoginDialog
from vidarc.tool.net.vNetLoginDialog import EVT_VTNET_LOGIN_DLG_FINISHED
from vidarc.tool.net.vtNetSecXmlGuiMasterLogDialog import vtNetSecXmlGuiMasterLogDialog
from vidarc.tool.net.vtNetSecXmlGuiMasterStatusDialog import vtNetSecXmlGuiMasterStatusDialog
from vidarc.tool.net.vtNetSecXmlGuiMasterBrowseDialog import *
from vidarc.tool.net.vtNetSecXmlGuiMasterBrowsePanel import EVT_VTSEC_XML_MASTER_BROWSE_PANEL_OPEN
from vidarc.tool.net.vtNetSecXmlGuiMasterBrowsePanel import EVT_VTSEC_XML_MASTER_BROWSE_PANEL_CONNECT
from vidarc.tool.net.vtNetSecXmlGuiMasterBrowsePanel import EVT_VTSEC_XML_MASTER_BROWSE_PANEL_STOP
from vidarc.tool.net.vtNetSecXmlGuiMasterBrowsePanel import EVT_VTSEC_XML_MASTER_BROWSE_PANEL_ADD
from vidarc.tool.net.vtNetSecXmlGuiMasterBrowsePanel import EVT_VTSEC_XML_MASTER_BROWSE_PANEL_DEL
#from vidarc.tool.net.vNetXmlSynchEditResolveDialog import vNetXmlSynchEditResolveDialog
from vidarc.tool.net.vtNetXmlSynchResolveDialog import vtNetXmlSynchResolveDialog
from vidarc.tool.net.vNetXmlGuiMasterCloseDialog import vNetXmlGuiMasterCloseDialog
from vidarc.tool.net.vtNetSecLoginAliasPanel import EVT_VTSEC_LOGIN_ALIAS_CHANGED
from vidarc.tool.net.vNetXmlWxGui import *
from vidarc.tool.xml.vtXmlDom import *
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.tool.gui.vtgCore as vtgCore
from vidarc.tool.misc.vtmMsgDialog import vtmMsgDialog
from vidarc.tool.vtStateFlag import vtStateFlag
from vidarc.tool.vtThread import vtThreadWX

import string,os.path,sys,threading,time
import vidarc.tool.net.img_gui as img_gui
import vidarc.tool.net.images as img_img

vtnxEVT_NET_SEC_XML_MASTER_START=wx.NewEventType()
vtnEVT_NET_SEC_XML_MASTER_START=wx.PyEventBinder(vtnxEVT_NET_SEC_XML_MASTER_START,1)
def EVT_NET_SEC_XML_MASTER_START(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_SEC_XML_MASTER_START,func)
def EVT_NET_SEC_XML_MASTER_START_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_SEC_XML_MASTER_START,func)
class vtnxSecXmlMasterStart(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_SEC_XML_MASTER_START(<widget_name>, xxx)
    """
    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtnxEVT_NET_SEC_XML_MASTER_START)

vtnxEVT_NET_SEC_XML_MASTER_FINISHED=wx.NewEventType()
vtnEVT_NET_SEC_XML_MASTER_FINISHED=wx.PyEventBinder(vtnxEVT_NET_SEC_XML_MASTER_FINISHED,1)
def EVT_NET_SEC_XML_MASTER_FINISHED(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_SEC_XML_MASTER_FINISHED,func)
def EVT_NET_SEC_XML_MASTER_FINISHED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_SEC_XML_MASTER_FINISHED,func)
class vtnxSecXmlMasterFinished(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_SEC_XML_MASTER_FINISHED(<widget_name>, xxx)
    """
    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtnxEVT_NET_SEC_XML_MASTER_FINISHED)

vtnxEVT_NET_SEC_XML_MASTER_SHUTDOWN=wx.NewEventType()
vtnEVT_NET_SEC_XML_MASTER_SHUTDOWN=wx.PyEventBinder(vtnxEVT_NET_SEC_XML_MASTER_SHUTDOWN,1)
def EVT_NET_SEC_XML_MASTER_SHUTDOWN(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_SEC_XML_MASTER_SHUTDOWN,func)
def EVT_NET_SEC_XML_MASTER_SHUTDOWN_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_SEC_XML_MASTER_SHUTDOWN,func)
class vtnxSecXmlMasterShutdown(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_SEC_XML_MASTER_SHUTDOWN(<widget_name>, xxx)
    """
    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtnxEVT_NET_SEC_XML_MASTER_SHUTDOWN)

vtnxEVT_NET_SEC_XML_MASTER_ALIAS_CHANGED=wx.NewEventType()
vtnEVT_NET_SEC_XML_MASTER_ALIAS_CHANGED=wx.PyEventBinder(vtnxEVT_NET_SEC_XML_MASTER_ALIAS_CHANGED,1)
def EVT_NET_SEC_XML_MASTER_ALIAS_CHANGED(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_SEC_XML_MASTER_ALIAS_CHANGED,func)
def EVT_NET_SEC_XML_MASTER_ALIAS_CHANGED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_SEC_XML_MASTER_ALIAS_CHANGED)
class vtnxSecXmlMasterAliasChanged(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_SEC_XML_MASTER_ALIAS_CHANGED(<widget_name>, xxx)
    """
    def __init__(self,obj,appl,alias,host):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtnxEVT_NET_SEC_XML_MASTER_ALIAS_CHANGED)
        self.alias=alias
        self.host=host
        self.appl=appl
    def GetAppl(self):
        return self.appl
    def GetAlias(self):
        return self.alias
    def GetHost(self):
        return self.host

vtnxEVT_NET_SEC_XML_MASTER_NOTIFY=wx.NewEventType()
vtnEVT_NET_SEC_XML_MASTER_NOTIFY=wx.PyEventBinder(vtnxEVT_NET_SEC_XML_MASTER_NOTIFY,1)
def EVT_NET_SEC_XML_MASTER_NOTIFY(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_SEC_XML_MASTER_NOTIFY,func)
def EVT_NET_SEC_XML_MASTER_NOTIFY_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_SEC_XML_MASTER_NOTIFY)
class vtnxSecXmlMasterNotify(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_SEC_XML_MASTER_NOTIFY(<widget_name>, xxx)
    """
    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtnxEVT_NET_SEC_XML_MASTER_NOTIFY)

class thdNetXmlStart:
    def __init__(self,par,verbose=0):
        self.verbose=verbose
        self.par=par
    def Start(self):
        thread.start_new_thread(self.Run, ())
    def Run(self):
        try:
            speed=2
            iCount=len(self.par.lNetAppl)
            for idx in range(0,iCount):
                name=self.par.lNetAppl[idx]
                netDoc=self.par.dNetDoc[name]
                iTime=0.0
                
                while 1==1:
                    if speed==0:
                        if netDoc.GetStatus() in [netDoc.SYNCH_FIN,netDoc.SYNCH_ABORT]:
                            break
                    elif speed==1:
                        #if self.par.netMaster.IsFlag(netDoc.CFG_VALID):
                        if netDoc.IsFlag(netDoc.OPENED):
                            break
                    else:
                        #if self.par.netMaster.IsFlag(self.par.netMaster.OPENED):
                            break
                    time.sleep(0.1)
                    iTime+=0.1
                    if iTime>10:
                        if netDoc.GetStatus() in [netDoc.DISCONNECTED,netDoc.STOPPED,netDoc.CONNECT_FLT]:
                            break
                vtLog.vtLngCurWX(vtLog.CRITICAL,'FIXME',self.par)
                self.par.AutoConnect2Srv(idx+1)
        except:
            traceback.print_exc()
class thdNetXmlStop:
    def __init__(self,par,verbose=0):
        self.verbose=verbose
        self.par=par
    def Start(self):
        thread.start_new_thread(self.Run, ())
    def Run(self):
        speed=1
        #self.par.thdLog.Stop()
        #while self.par.thdLog.IsRunning():
        #    time.sleep(1.0)
        iCount=len(self.par.lstNetAppl)
        for idx in range(0,iCount):
            name=self.par.lNetAppl[idx]
            netDoc=self.par.dNetDoc[name]
            netDoc.DisConnect()
            if speed==0:
                while netDoc.GetStatus() != netDoc.STOPPED:
                    time.sleep(0.1)
            elif speed==1:
                time.sleep(1.0)
            else:
                pass

##class vtNetSecXmlGuiMaster(StaticBitmap):
#class vtNetSecXmlGuiMaster(wx.BitmapButton):
class vtNetSecXmlGuiMaster(GenBitmapButton):
    UPDATE_TIME = 500 # FIXME 100
    VERBOSE=0
    RECENT_SUB_MENU=16
    RECENT=8
    SEM_LOGIN_BLOCKING=True
    USE_TIMER4QUEUE_CHECK=False
    
    DISCONNECTED    =0x010
    #CONNECT         =0x00000001
    SHUTDOWN        =0x00008000
    SHUTDOWN_FIN    =0x00080000
    STATE_MASK      =0x000000FF
    MASK            =0x00FFFFFF
    
    def __init__(self,parent,appl,id=-1,pos=(0,0),size=(16,12),name='vtNetSecXmlGuiMaster',verbose=0):
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        ##StaticBitmap.__init__(self,parent=parent,pos=pos,size=size,
        ##        bitmap=EmptyBitmap(16, 12),name='vtNetSecXmlGuiMaster')
        #wx.BitmapButton.__init__(self,parent=parent,pos=pos,size=size,
        #        bitmap=wx.EmptyBitmap(16, 12),name='vtNetSecXmlGuiMaster',
        #        style=wx.SUNKEN_BORDER | wx.SIMPLE_BORDER | wx.NO_3D)
        GenBitmapButton.__init__(self,parent=parent,id=id,pos=pos,size=size,
                bitmap=wx.EmptyBitmap(16, 12),name=name,
                style=0)
        #self.Bind(wx.EVT_PAINT, self.OnPaint)
        self.semLogin=threading.Lock()#threading.Semaphore()
        
        widDlgPar=vtgCore.vtGuiCoreGetBaseWidget(self)
        self.widLogging=vtLog.GetPrintMsgWid(self)
        try:
            sOrigin=vtLog.vtLngGetRelevantNamesWX(self,appl)
            self.SetOrigin(sOrigin)
        except:
            vtLog.vtLngTB(self.GetName())
            self.SetOrigin(appl)
        self.thdProc=vtThreadWX(widDlgPar,bPost=False,
                origin=u':'.join([self.GetOrigin(),u'thdProc']))
        #self.pwMaster=vtNetSecXmlGuiMasterPopupWindow(self,verbose=0)
        self.dlgBrowse=vtNetSecXmlGuiMasterBrowseDialog(widDlgPar)
        EVT_VTSEC_XML_MASTER_BROWSE_PANEL_OPEN(self.dlgBrowse,self.OnBrowseOpen)
        EVT_VTSEC_XML_MASTER_BROWSE_PANEL_CONNECT(self.dlgBrowse,self.OnBrowseConnect)
        EVT_VTSEC_XML_MASTER_BROWSE_PANEL_STOP(self.dlgBrowse,self.OnBrowseStop)
        EVT_VTSEC_XML_MASTER_BROWSE_PANEL_ADD(self.dlgBrowse,self.OnBrowseAdd)
        EVT_VTSEC_XML_MASTER_BROWSE_PANEL_DEL(self.dlgBrowse,self.OnBrowseDel)
        EVT_VTSEC_XML_MASTER_BROWSE_DIALOG_CFG(self.dlgBrowse,self.OnBrowseCfg)
        EVT_VTSEC_XML_MASTER_BROWSE_DIALOG_LOG(self.dlgBrowse,self.OnBrowseLog)
        EVT_VTSEC_XML_MASTER_BROWSE_DIALOG_STATUS(self.dlgBrowse,self.OnBrowseStatus)
        EVT_VTSEC_XML_MASTER_BROWSE_DIALOG_MASTER(self.dlgBrowse,self.OnBrowseMaster)
        
        self.oSF=vtStateFlag(
                    {
                    0x000:{'name':'CLOSED','translation':_(u'closed'),
                        'init':True,'is':None,
                        'enter':{'set':['CLSD','STLD',],
                                 'clr':['WRKG','FLT','RDY','MOD']}},
                    0x001:{'name':'OPEN','translation':_(u'open'),
                        'is':None,
                        'enter':{'set':['WRKG','BLOCK_NOTIFY'],
                                 'clr':['STLD','FLT','CLSD','MOD','RDY','ONLINE']}},
                    0x003:{'name':'GENID','translation':_(u'generate IDs'),
                        'is':None,
                        'enter':{'set':['WRKG']}},
                    0x004:{'name':'OPENED','translation':_(u'opened'),
                        'is':'IsOpenOk',
                        'enter':{'set':['OPND',],
                                 'clr':['FLT','MOD','BLOCK_NOTIFY']},
                        '':{'set':['WRKG']}},
                    0x005:{'name':'OPEN_FLT','translation':_(u'open fault'),
                        'is':'IsOpenFault',
                        'enter':{'set':['FLT','STLD','OPND','MOD',],
                                 'clr':['WRKG','RDY','BLOCK_NOTIFY',]}},
                    0x006:{'name':'PAUSE','translation':_(u'pause'),
                        'is':None,
                        'enter':{'set':['WRKG'],
                                 'clr':['RDY']}},
                    0x007:{'name':'PAUSING','translation':_(u'pausing'),
                        'is':None,
                        'enter':{'set':['WRKG']}},
                    0x008:{'name':'PAUSED','translation':_(u'paused'),
                        'is':None,
                        'enter':{#'set':['fin',],
                                 'clr':['WRKG','RDY']}},
                    0x009:{'name':'RESUME','translation':_(u'resume'),
                        'enter':{'set':['WRKG']}},
                    0x00E:{'name':'NEW','translation':_(u'new'),
                        'enter':{'set':['MOD']}},
                    0x00F:{'name':'CLOSING','translation':_(u'closing'),
                        'enter':{'set':['WRKG']}},
                    0x010:{'name':'DISCONNECT','translation':_(u'disconnect'),
                        'is':'IsDisConnect',
                        'enter':{'set':['WRKG',],
                                 'clr':['STLD','RDY','ONLINE','CONN_ATTEMPT']}},
                    0x015:{'name':'DISCONNECTED','translation':_(u'disconnected'),
                        'is':'IsDisConnected',
                        'enter':{'set':['STLD'],
                                 'clr':['WRKG','RDY']}},
                    0x01F:{'name':'PRECONNECT','translation':_(u'pre connect'),
                        'is':None,
                        'enter':{'set':['WRKG','BLOCK_NOTIFY'],
                                 'clr':['STLD','RDY','CONN_ATTEMPT']}},
                    0x020:{'name':'CONNECT','translation':_(u'connect all'),
                        'is':'IsConnect',
                        'enter':{'set':['WRKG','BLOCK_NOTIFY','CONN_ATTEMPT','ONLINE'],
                                 'clr':['STLD','RDY']}},
                    0x021:{'name':'CONNECT_MAIN','translation':_(u'main alias connect'),
                        'is':'IsConnectMain',
                        'enter':{'set':['WRKG','BLOCK_NOTIFY','CONN_ATTEMPT'],
                                 'clr':['STLD','RDY']}},
                    0x022:{'name':'CONNECT_SUB','translation':_(u'sub alias connect'),
                        'is':'IsConnectSub',
                        'enter':{'set':['WRKG','BLOCK_NOTIFY','CONN_ATTEMPT','CONN_All'],
                                 'clr':['STLD','RDY']}},
                    0x030:{'name':'CONNECT_FLT','translation':_(u'at least one connect fault'),
                        'is':'IsConnectFault',
                        'enter':{'set':['FLT','STLD'],
                                 'clr':['WRKG','RDY']}},
                    0x031:{'name':'CONNECT_FLT_MAIN','translation':_(u'main alias connect fault'),
                        'is':'IsConnectFaultMain',
                        'enter':{'set':['FLT','STLD'],
                                 'clr':['WRKG','RDY']}},
                    0x032:{'name':'CONNECT_FLT_SUB','translation':_(u'sub alias connect fault'),
                        'is':'IsConnectFaultSub',
                        'enter':{'set':['FLT','STLD'],
                                 'clr':['WRKG','RDY']}},
                    0x060:{'name':'CONNECTED','translation':_(u'all connected'),
                        'is':'IsConnected',
                        'enter':{'set':['RDY','STLD'],
                                 'clr':['WRKG','BLOCK_NOTIFY']}},
                    0x061:{'name':'CONNECTED_MAIN','translation':_(u'main alias connected'),
                        'is':'IsConnectedMain',
                        'enter':{'set':['STLD'],
                                 'clr':['WRKG','RDY','BLOCK_NOTIFY']}},
                    0x062:{'name':'CONNECTED_SUB','translation':_(u'sub aliases connected'),
                        'is':'IsConnectedSub',
                        'enter':{'set':['STLD'],
                                 'clr':['WRKG','RDY','BLOCK_NOTIFY']}},
                    0x0F0:{'name':'SHUTTINGDOWN','translation':_(u'shutting down'),
                        'is':'IsStateShuttingDown',
                        'enter':{'set':['WRKG','SHTGDN',],
                                 'clr':['RDY','STLD']}},
                    0x0FF:{'name':'SHUTDOWN','translation':_(u'shut down'),
                        'is':'IsStateShutDown',
                        'enter':{'set':['CLSD','SHTDN',],
                                 'clr':['WRKG','RDY','STLD']}},
                    },
                    {
                    0x00000001:{'name':'CLSD', 'translation':_(u'closed'),'is':'IsClsd','order':0},
                    0x00000002:{'name':'STLD', 'translation':_(u'settled'),'is':'IsSettled','order':1},
                    0x00000004:{'name':'WRKG',  'translation':_(u'working'),'is':'IsWorking','order':2},
                    0x00000008:{'name':'RDY',  'translation':_(u'ready'),'is':'IsReady','order':3},
                    0x00000010:{'name':'MOD',  'translation':_(u'modified'),'is':'IsModified','order':4},
                    0x00000020:{'name':'FLT',  'translation':_(u'faulty'),'is':'IsFaulty','order':5},
                    0x00000040:{'name':'OPND', 'translation':_(u'opened'),'is':'IsOpened','order':6},
                    0x00000200:{'name':'LOGIN_EXT', 'translation':_(u'extended login'),'is':'IsLoginExt','order':9},
                    0x00000400:{'name':'BLOCK_NOTIFY', 'translation':_(u'block events'),'is':'IsBlockNotify','order':10},
                    0x00001000:{'name':'CFG_VALID', 'translation':_(u'configuration valid'),'is':'IsCfgValid','order':11},
                    0x00002000:{'name':'SYNCHED', 'translation':_(u'synched'),'is':'IsSynched','order':12},
                    0x00004000:{'name':'CONN_All', 'translation':_(u'connect all'),'is':'IsConnActive','order':13},
                    0x00010000:{'name':'CONN_ATTEMPT', 'translation':_(u'connection attemped'),'is':'IsConnAttempt','order':16},
                    0x00040000:{'name':'SHTGDN', 'translation':_(u'shutting down'),'is':'IsShuttingDown','keep':True,'order':17},
                    0x00080000:{'name':'SHTDN', 'translation':_(u'shutdown'),'is':'IsShutDown','keep':True,'order':18},
                    0x00200000:{'name':'ONLINE', 'translation':_(u'online'),'is':'IsOnline','order':20},
                    0x00400000:{'name':'ACTIVE', 'translation':_(u'active'),'is':'IsActive','order':21},
                    },
                    verbose=0,
                    ident=self,
                    funcNotifyCB=self.NotifyStateChangeMaster,
                    origin=u':'.join([self.origin,u'StateFlag']))

        self.dlgMaster=vtNetSecXmlGuiMasterDialog(widDlgPar,verbose=0)
        self.dlgLogin=vNetLoginDialog(widDlgPar)
        #self.dlgEditResolve=vNetXmlSynchEditResolveDialog(widDlgPar)
        self.dlgEditResolve=vtNetXmlSynchResolveDialog(widDlgPar,
                    _(u'vtNet Synch Resolve Dialog'))
        self.dlgCfgSetup=vtNetSecCfgDialog(widDlgPar)
        self.dlgCfgSetup.BindEvents(self.OnCfgSetupApply)
        EVT_VTSEC_LOGIN_ALIAS_CHANGED(self.dlgCfgSetup.pnAlias,self.OnCfgChanged)
        EVT_VTNET_LOGIN_DLG_FINISHED(self.dlgLogin,self.OnLoginDlgFin)
        
        self.dlgLog=vtNetSecXmlGuiMasterLogDialog(widDlgPar)
        self.dlgStatus=vtNetSecXmlGuiMasterStatusDialog(widDlgPar)
        self.dlgStatus.SetNetMaster(self)
        
        self.dlgClose=None
        #EVT_NET_SEC_XML_MASTER_SHUTDOWN(self,self.OnShutDown)
        
        self.netDocMaster=None
        self.nodeSetup=None
        self.docSetup=None
        self.lstSetupNode=[]
        self.lstCfg=None
        self.dictDoc={}
        self.lNetAppl=[]
        self.dNetDoc={}
        self.appls=[]
        self.mainAppl=''
        self.dn=''
        self.sRecentAlias=''
        self.sRecentConn=''
        self.alias=''
        self.host=''
        self.port=-1
        self.usr=''
        self.passwd=''
        self.oLoginStorage=None
        self.verbose=verbose
        
        # handle messages send by threads
        self.semChange=threading.Lock()#threading.Semaphore()
        
        #wx.EVT_LEFT_DOWN(self, self.OnLeftDown)
        wx.EVT_LEFT_UP(self, self.OnLeftDown)
        wx.EVT_RIGHT_DOWN(self, self.OnRightDown)
    def __del__(self):
        try:
            vtLog.vtLngCur(vtLog.DEBUG,'',origin=self.origin)
        except:
            pass
        try:
            #self.dlgMaster.Destroy()
            #self.dlgLogin.Destroy()
            #self.dlgEditResolve.Destroy()
            #self.dlgCfgSetup.Destroy()
            #self.dlgLog.Destroy()
            #self.dlgStatus.Destroy()
            pass
            del self.appls
            del self.lstSetupNode
            del self.dictDoc
            del self.lNetAppl
            del self.dNetDoc
            self.dNetDoc=None
        except:
            #vtLog.vtLngTB('del')
            pass
        #self.dlgMaster=None
        #self.dlgLogin=None
        #self.dlgEditResolve=None
        #self.dlgCfgSetup=None
        #self.dlgLog=None
        #self.dlgStatus=None
    def GetOrigin(self):
        return self.origin
    def SetOrigin(self,origin):
        self.origin=origin
    def BindEvents(self,funcStart=None,funcFinish=None,funcShutDown=None):
        if funcStart is not None:
            EVT_NET_SEC_XML_MASTER_START(self,funcStart)
        if funcFinish is not None:
            EVT_NET_SEC_XML_MASTER_FINISHED(self,funcFinish)
        if funcShutDown is not None:
            EVT_NET_SEC_XML_MASTER_SHUTDOWN(self,funcShutDown)
    def SetBitmap(self,img):
        vtLog.vtLngCur(vtLog.DEBUG,''%(),self.GetOrigin())
        #StaticBitmap.SetBitmap(self,img)
        #wx.BitmapButton.SetBitmapLabel(self,img)
        self.Refresh()
        #self.Update()
    #def OnPaint(self,evt):
    #    dc = wx.PaintDC(self)
    def DrawBezel(self, dc, x1, y1, x2, y2):
        if self.hasFocus and self.useFocusInd:
            return
        # draw the upper left sides
        if self.up:
            #dc.SetPen(self.highlightPen)
            dc.SetPen(self.shadowPen)
        else:
            dc.SetPen(self.shadowPen)
        for i in range(self.bezelWidth):
            dc.DrawLine(x1+i, y1, x1+i, y2-i)
            dc.DrawLine(x1, y1+i, x2-i, y1+i)
        # draw the lower right sides
        if self.up:
            dc.SetPen(self.shadowPen)
        else:
            #dc.SetPen(self.highlightPen)
            dc.SetPen(self.shadowPen)
        for i in range(self.bezelWidth):
            dc.DrawLine(x1+i, y2-i, x2+1, y2-i)
            dc.DrawLine(x2-i, y1+i, x2-i, y2)
    def DrawFocusIndicator(self, dc, w, h):
        bw = self.bezelWidth
        self.focusIndPen.SetColour(self.focusClr)
        dc.SetLogicalFunction(wx.INVERT)
        dc.SetPen(self.focusIndPen)
        dc.SetBrush(wx.TRANSPARENT_BRUSH)
        dc.DrawRectangle(bw,bw,  w-bw*2, h-bw*2)
        dc.SetLogicalFunction(wx.COPY)
    def DrawLabel(self, dc, width, height, dw=0, dy=0):
        try:
            x,y=0,0
            def getXY(bmp):
                bw,bh = bmp.GetWidth(), bmp.GetHeight()
                x,y=(width-bw)/2+dw, (height-bh)/2+dy
                return x,y
            #print 'OnPaint'
            iStatus=-1
            iStatusSub=-1
            iPrior=0
            #print self.netDocMaster
            #print self.lNetAppl
            #print self.dNetDoc
            if hasattr(self,'DICT_PRIOR_OVERLAY')==False:
                if self.netDocMaster is not None:
                    netDoc=self.netDocMaster
                    netDoc=self.dNetDoc[self.lNetAppl[0]]
                    self.DICT_PRIOR_OVERLAY={netDoc.SERVE_FLT:255,netDoc.CONNECT_FLT:254,netDoc.SYNCH_ABORT:253,
                            netDoc.OPEN_FLT:252,netDoc.STOPPED:100,
                            netDoc.SYNCH_PROC:1}
            if hasattr(self,'DICT_PRIOR_OVERLAY'):
                #print self.DICT_PRIOR_OVERLAY
                for name in self.lNetAppl:
                    netDoc=self.dNetDoc[name]
                    i=netDoc.GetStatus()
                    if iStatus==-1:
                        iStatus=i
                    else:
                        if i in self.DICT_PRIOR_OVERLAY:
                            
                            j=self.DICT_PRIOR_OVERLAY[i]
                            if j>iPrior:
                                iStatusSub=i
                                iPrior=j
                #vtLog.CallStack('')
                #print iStatus
                #print iStatusSub
                netDoc=self.netDocMaster
                if netDoc is not None:
                    #brush = self.GetBackgroundBrush(dc)
                    #if brush is not None:
                    #    dc.SetBackground(brush)
                    #    dc.Clear()
                    bmp=netDoc.__getMedBitmapByStatus__(iStatus)
                    hasMask = bmp.GetMask() != None
                    x,y=getXY(bmp)
                    dc.DrawBitmap(bmp,x,y,hasMask)
                    if iStatusSub>=0:
                        bmp=netDoc.__getMedSubBitmapByStatus__(iStatusSub)
                        hasMask = bmp.GetMask() != None
                        x,y=getXY(bmp)
                        dc.DrawBitmap(bmp,x,y,hasMask)
                #x+=3
                #y+=3
                #dc.DrawBitmap(self.redBmp,    50,  50, True)
        except:
            #import traceback
            #traceback.print_exc()
            vtLog.vtLngTB(self.GetOrigin())
    def OnPaint(self, event):
        (width, height) = self.GetClientSizeTuple()
        x1 = y1 = 0
        x2 = width-1
        y2 = height-1
        dc = wx.PaintDC(self)
        brush = self.GetBackgroundBrush(dc)
        if brush is not None:
            dc.SetBackground(brush)
            dc.Clear()
        self.DrawLabel(dc, width, height)
        self.DrawBezel(dc, x1, y1, x2, y2)
        if self.hasFocus and self.useFocusInd:
            self.DrawFocusIndicator(dc, width, height)
    def IsStatusChanged(self,iStatusPrev=None,iStatus=None):
        vtLog.vtLngCur(vtLog.CRITICAL,''%(),self.GetOrigin())
    def SetFlag(self,int_flag,flag):
        vtLog.vtLngCur(vtLog.CRITICAL,''%(),self.GetOrigin())
    def IsFlag(self,int_flag,iStatus=None):
        vtLog.vtLngCur(vtLog.CRITICAL,''%(),self.GetOrigin())
        return
        if iStatus is None:
            iStatus=self.iStatus
        if iStatus & int_flag:
            return True
        else:
            return False
    def SetLocalDN(self,dn):
        self.dlgBrowse.SetLocalDN(dn)
        self.dlgCfgSetup.SetLocalDN(dn)
    def SetLang(self,lang):
        try:
            for sAppl,doc in self.dNetDoc.iteritems():
                try:
                    doc.SetLang(lang)
                except:
                    vtLog.vtLngTB(self.GetOrigin())#self.mainAppl)
        except:
            vtLog.vtLngTB(self.GetOrigin())#self.mainAppl)
    def SetCfg(self,doc,lst,appls,mainAppl):
        self.docSetup=doc
        self.lstCfg=lst
        self.dictDoc={}
        self.sRecentAlias=''
        self.sRecentConn=''
        self.appls=appls
        self.mainAppl=mainAppl
        
        self.dlgMaster.SetCfg(self,appls,mainAppl)
        self.dlgMaster.SetCfgDlg(self.dlgCfgSetup)
        self.dlgMaster.SetLogDlg(self.dlgLog)
        self.dlgMaster.SetStatusDlg(self.dlgStatus)
        self.dlgCfgSetup.SetCfg(doc,lst,appls)
        try:
            self.dlgBrowse.SetAppl(self.appls[0])
            self.dlgBrowse.SetLocalDN(self.dlgCfgSetup.GetDN())
        except:
            vtLog.vtLngCur(vtLog.ERROR,'no master application defined;%appl:%s'%','.join(self.appls),#self.mainAppl)
                            self.GetOrigin())
        conns=self.dlgCfgSetup.GetObjConnections()
        self.dlgBrowse.UpdateConnections(conns)
    def SetCfgNode(self,bConnect=True):
        #self.pwMaster.SetCfgNode(bConnect)
        #self.dlgMaster.SetCfgNode(bConnect)
        #vtLog.vtLngCur(vtLog.FATAL,'',self.mainAppl)
        self.dlgMaster.SetCfg(self,self.appls,self.mainAppl)
        self.dlgCfgSetup.SetCfg(self.docSetup,self.lstCfg,self.appls)
        self.dlgBrowse.SetLocalDN(self.dlgCfgSetup.GetDN())
        self.dlgBrowse.UpdateConnections(self.dlgCfgSetup.GetObjConnections())
    def SetLoginDlg(self,dlg):
        self.dlgLogin=dlg
    def GetLoginDlg(self):
        return self.dlgLogin#vNetLoginDialog(self)
    def GetMasterDlg(self):
        return self.dlgMaster
    def GetSetupDlg(self):
        return self.dlgCfgSetup
    def GetLogDlg(self):
        return self.dlgLog
    def GetStatusDlg(self):
        return self.dlgStatus
    def GetEditResolveDlg(self):
        return self.dlgEditResolve
    def AcquireLogin(self,bBlocking=False):
        vtLog.vtLngCur(vtLog.INFO,'',self.GetOrigin())#self.mainAppl)
        return self.semLogin.acquire(bBlocking)
    def ReleaseLogin(self):
        self.semLogin.release()
        vtLog.vtLngCur(vtLog.INFO,'',self.GetOrigin())#self.mainAppl)
    def SetLoginStorage(self,oLoginStorage):
        vtLog.vtLngCur(vtLog.INFO,'',self.GetOrigin())#self.mainAppl)
        self.oLoginStorage=oLoginStorage
    def SetLoginExtended(self,bFlag):
        vtLog.vtLngCur(vtLog.INFO,'flag:%d'%bFlag,self.GetOrigin())#self.mainAppl)
        #self.bLoginExtended=bFlag
        self.oSF.SetFlag(self.oSF.LOGIN_EXT,True)
    def CheckLogin(self,appl,host,port,alias,usr):
        vtLog.vtLngCur(vtLog.INFO,'appl:%s;host:%s;port:%s;alias:%s;usr:%s'%(appl,host,port,alias,usr),self.GetOrigin())#self.mainAppl)
        if self.oLoginStorage is None:
            return None
        if self.oLoginStorage.acquire('check',blocking=self.SEM_LOGIN_BLOCKING):          # 060911
            try:
                sLogin,sPasswd=self.oLoginStorage.GetLogin(appl,host,port,alias,usr)
                if sLogin is None:
                    self.oLoginStorage.release('check')
                    return None
                else:
                    self.UpdateLogin(appl,usr,sPasswd)
                    self.oLoginStorage.release('check')
                    return sPasswd
            except:
                self.oLoginStorage.release('check')
                vtLog.vtLngTB(self.GetOrigin())
                return
        vtLog.vtLngCur(vtLog.DEBUG,'oLoginStorage acquire not possible',self.GetOrigin())#self.mainAppl)
        return None
    def SetLogin(self,appl,host,port,alias,usr,passwd):
        vtLog.vtLngCur(vtLog.INFO,'appl:%s;host:%s;port:%s;alias:%s;usr:%s'%(appl,host,port,alias,usr),self.GetOrigin())#self.mainAppl)
        if self.oLoginStorage is None:
            return None
        if self.oLoginStorage.acquire('check',blocking=self.SEM_LOGIN_BLOCKING):          # 060911
            try:
                self.oLoginStorage.SetLogin(appl,host,port,alias,usr,passwd)
                self.oLoginStorage.Save()
                self.oLoginStorage.release('check')
                return
            except:
                self.oLoginStorage.release('check')
                vtLog.vtLngTB(self.GetOrigin())
                return
        vtLog.vtLngCur(vtLog.DEBUG,'oLoginStorage acquire not possible',self.GetOrigin())#self.mainAppl)
    def AddNetControl(self,ctrlClass,name,*args,**kwargs):
        vtLog.vtLngCur(vtLog.INFO,'name:%s;cls:%s'%(name,ctrlClass),self.GetOrigin())#self.mainAppl)
        #return self.pwMaster.AddNetControl(ctrlClass,name,*args,**kwargs)
        if ctrlClass is not None:
            iRet=self.dlgStatus.AddNetControl(ctrlClass,name,*args,**kwargs)
        else:
            self.lNetAppl=self.dlgStatus.GetNetNames()
            self.dNetDoc=self.dlgStatus.GetDictNet()
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'dNet:%s'%(vtLog.pformat(self.dNetDoc)),self.GetOrigin())#self.mainAppl)
            self.netDocMaster=self.dlgStatus.GetNetDocMaster()
            docMain=self.dNetDoc[self.lNetAppl[0]]
            d={}
            for k,doc in zip(self.dNetDoc.keys(),self.dNetDoc.values()):
                d[k]={'doc':doc,'bNet':True}
            #self.netDocMaster.SetNetDocs(self.dNetDoc)
            docMain.SetNetDocs(d)
            # setup main doc
            docMain.SetupAsMainAlias()
            docMain.AddCB(docMain.CONFIG,'enter',self.__updateConfig__)
            docMain.AddCB(docMain.CONFIG,'enter',self._doConnectSubAlias)
            docMain.AddCB(docMain.CONNECT_FLT,'enter',self.__updateConfig__)
            docMain.AddCB(docMain.CONNECT_FLT,'enter',self._doConnectSubAlias)
            iRet=1
        return iRet
    def SetDictDoc(self,d):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'dict:%s'%(d))
        if self.docSetup is None:
            return
        self.dictDoc=d
        for k in self.dictDoc.keys():
            if k == self.appls[0]:
                continue
            doc=self.dictDoc[k]
            doc.SetCfg(self.docSetup,self.lstCfg,self.appls,k,
                    self.mainAppl,self.dlgCfgSetup)
    def changeIdNum(self,idOld,idNew,sAlias):
        vtLog.vtLngCur(vtLog.INFO,'idOld:%08d idNew:%08d alias:%s'%(idOld,idNew,sAlias))
        if self.semChange.acquire(False):
            for k in self.dNetDoc.keys():
                if k == sAlias:
                    continue
                doc=self.dNetDoc[k]
                try:
                    doc.changeIdNum(idOld,idNew,sAlias)
                except:
                    vtLog.vtLngTB(self.GetOrigin())
            self.semChange.release()
        else:
            pass
    def __getRecentAlias__(self):
        self.dlgCfgSetup.GetConnections()
        if self.VERBOSE:
            vtLog.vtLngCur(vtLog.DEBUG,'',self.GetOrigin())#self.mainAppl)
        #if self.verbose:
        #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        if self.docSetup is None:
            return None
        else:
            self.nodeSetup=self.docSetup.getRoot()
            node=self.docSetup.getChildByLst(self.nodeSetup,self.lstCfg)
            self.dn=self.docSetup.getNodeText(node,'path')
            #node=self.docSetup.getChild(node,self.appl)
            if (node is None) or (len(self.dn)<=0):
                self.fn=None
                return None
                #self.Setup()
                #return
            recentNode=None
            iRecent=1000
            for c in self.docSetup.getChilds(node,'alias'):
                s=self.docSetup.getNodeText(c,'recent')
                if len(s)>0:
                    try:
                        i=int(s)
                        if i<iRecent:
                            recentNode=c
                            iRecent=i
                    except:
                        pass
            return recentNode
    def __cmpRecentFunc__(self,a,b):
            try:
                iA=int(self.docSetup.getNodeText(a,'recent'))
            except:
                iA=0
            try:
                iB=int(self.docSetup.getNodeText(b,'recent'))
            except:
                iB=0
            return cmp(iA,iB)
        
    def __createRecentAlias__(self,sName):
        if self.docSetup is None:
            return None
        self.nodeSetup=self.docSetup.getRoot()
        node=self.docSetup.getChildByLst(self.nodeSetup,self.lstCfg)
        self.dn=self.docSetup.getNodeText(node,'path')
        #node=self.docSetup.getChild(node,self.appl)
        if (node is None) or (len(self.dn)<=0):
            self.fn=None
            return None
            #self.Setup()
            #return
        nodeAliases=self.docSetup.getChilds(node,'alias')
        n=self.docSetup.createChildByLst(node,'alias',[
                        {'tag':'name','val':sName},
                        {'tag':'recent','val':'0'},
                        ])
        nodeAliases.sort(self.__cmpRecentFunc__)
        i=1
        for c in nodeAliases:
            self.docSetup.setNodeText(a,'recent',str(i))
            i+=1
        self.docSetup.AlignNode(n,iRec=2)
        return n
    def __createRecentConnection__(self,recentNode,sName):
        if self.docSetup is None:
            return None
        if recentNode is None:
            return None
        nodeConnections=self.docSetup.getChilds(recentNode,'connection')
        nodeConnections.sort(self.__cmpRecentFunc__)
        n=self.docSetup.createChildByLst(recentNode,'connection',[
                            {'tag':'name','val':sName},
                            {'tag':'recent','val':'0'},
                            ])
        i=1
        for c in nodeConnections:
            self.docSetup.setNodeText(a,'recent',str(i))
            i+=1
        self.docSetup.AlignNode(n,iRec=2)
        return n
    def __createRecentHost__(self,recentConn,sAppl,sFN):
        if self.docSetup is None:
            return None
        if recentConn is None:
            return None
        #nodeHosts=self.docSetup.getChilds(recentNode,'host')
        #nodeHosts.sort(self.__cmpRecentFunc__)
        applNode=self.docSetup.getChildForced(recentConn,sAppl)
        n=self.docSetup.createChildByLst(applNode,'host',
                    [{'tag':'name','val':'main'},
                     {'tag':'recent','val':'0'},
                     {'tag':'appl','val':sAppl},
                     {'tag':'alias','val':''},
                     {'tag':'host','val':''},
                     {'tag':'port','val':'offline'},
                     {'tag':'usr','val':''},
                     {'tag':'passwd','val':''},
                     {'tag':'fn','val':sFN},
                    ])
        #i=1
        #for c in nodeHosts:
        #    self.docSetup.setNodeText(a,'recent',str(i))
        #    i+=1
        self.docSetup.AlignNode(applNode,iRec=2)
        
        return n
    def __getRecentConnection__(self,recentNode):
        #if self.verbose:
        #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
            
        if recentNode is None:
            return None
        
        recentConnNode=None
        iRecent=1000
        for c in self.docSetup.getChilds(recentNode,'connection'):
            s=self.docSetup.getNodeText(c,'recent')
            if len(s)>0:
                try:
                    i=int(s)
                    if i<iRecent:
                        recentConnNode=c
                        iRecent=i
                except:
                    pass
        return recentConnNode
    def __getApplHost__(self,recentNode,appl=None):
        if recentNode is None:
            return None
        if appl is None:
            appl=self.appl
        applNode=self.docSetup.getChildForced(recentNode,appl)
        recentNode=None
        iRecent=1000
        for c in self.docSetup.getChilds(applNode,'host'):
            s=self.docSetup.getNodeText(c,'recent')
            if len(s)>0:
                try:
                    i=int(s)
                    if i<iRecent:
                        recentNode=c
                        iRecent=i
                except:
                    pass
        return recentNode
    def __getAutoConnect__(self,recentNode):
        applNode=self.docSetup.getChild(recentNode,self.appl)
        sName=self.docSetup.getNodeText(applNode,'autoconnect')
        if sName=='1':
            return True
        elif sName=='0':
            return False
        else:
            self.docSetup.setNodeText(applNode,'autoconnect','1')
            return True
    def ReConnect2Srv(self,doc=None):
        #if self.verbose:
        #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'doc:%d docSetup:%d'%(doc is not None,self.docSetup is not None))
        #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'appl:%s alias:%s host:%s port:%s usr:%s passwd:%s'%
        #            (self.appl,self.alias,self.host,self.port,self.usr,self.passwd))
        #dictNet=self.pwMaster.GetDictNet()
        #lstNetAppl=self.pwMaster.GetNetNames()
        dictNet=self.dlgStatus.GetDictNet()
        lstNetAppl=self.dlgStatus.GetNetNames()
        dn=self.dlgCfgSetup.GetDN()
        i=0
        for appl in lstNetAppl:
            netDoc=dictNet[appl]
            
            #self.DisConnect()
            if self.verbose:
                vtLog.vtLngCallStack(self,vtLog.DEBUG,'recent alias:%s recent conn:%s'%(oldRecentAlias,oldRecentConn),origin=appl)
                vtLog.vtLngCallStack(self,vtLog.DEBUG,'host:%s alias:%s usr:%s'%(oldHost,oldAlias,oldUsr),origin=appl)
            
            recentNode=self.__getRecentAlias__()
            recentAliasNode=self.__getRecentConnection__(recentNode)
            recentHostNode=self.__getApplHost__(recentAliasNode,appl)
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'appl:%s recent:%s'%(appl,recentNode),origin=appl)
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'appl:%s recent:%s'%(appl,recentAliasNode),origin=appl)
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'appl:%s recent:%s'%(appl,recentHostNode),origin=appl)
            
            netDoc.Connect2SrvByCfg(recentNode,recentAliasNode,recentHostNode)
            
            if i==0:
                if netDoc.GetStatus()!=netDoc.SYNCH_FIN:
                    break
            i+=1
            pass
    def AutoConnect2Srv(self,idx=-1,force=False):
        #if self.verbose:
        #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'idx:%d docSetup:%d'%(idx,self.docSetup is not None))
        #dictNet=self.pwMaster.GetDictNet()
        #lstNetAppl=self.pwMaster.GetNetNames()
        vtLog.vtLngCur(vtLog.DEBUG,'idx:%d;force:%d'%(idx,force),self.GetOrigin())#self.mainAppl)
        lConns=self.dlgCfgSetup.GetConnections()
        conn=self.dlgCfgSetup.GetConnection()
        dn=self.dlgCfgSetup.GetDN()
        if self.VERBOSE:
            vtLog.CallStack('')
            print lConns
            print conn
        
        #dictNet=self.dlgStatus.GetDictNet()
        #lstNetAppl=self.dlgStatus.GetNetNames()
        i=0
        #for appl in lstNetAppl:
        # FIXME
        netDoc=self.dNetDoc[self.lNetAppl[0]]
        if netDoc.IsCfgValid():
            bCfgValid=True
        else:
            bCfgValid=False
        for appl in self.lNetAppl:
            #netDoc=dictNet[appl]
            netDoc=self.dNetDoc[appl]
            if idx>=0:
                if i!=idx:
                    i+=1
                    continue
            #self.DisConnect()
            if idx==-2:
                if i==0:
                    i+=1
                    continue
            #if netDoc.GetStatus() not in [netDoc.DISCONNECTED,netDoc.STOPPED]:
            #    i+=1
            #    continue
            #if netDoc.IsFlag(netDoc.CONN_ACTIVE):
            if netDoc.IsConnActive():
                vtLog.vtLngCur(vtLog.DEBUG,'skip due connection active %s'%appl,self.GetOrigin())#self.mainAppl)
                if idx>=0:
                    return
                i+=1
                continue
            netDoc.PreConnect()
            if self.verbose:
                vtLog.vtLngCur(vtLog.DEBUG,'idx:%d docSetup:%d'%(idx,self.docSetup is not None),self.GetOrigin())#self.mainAppl)
            objAlias=conn.GetAlias(appl)
            if objAlias.Is2Cfg():
                vtLog.vtLngCur(vtLog.DEBUG,'skip due unknown configuration %s'%appl,self.GetOrigin())#self.mainAppl)
                continue
            if self.VERBOSE:
                print appl,objAlias
                print appl,objAlias.GetStr()
            #netDoc.SetFlag(netDoc.CONN_ACTIVE,True)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'appl %s;obj:%s'%(appl,repr(objAlias)),self.GetOrigin())#self.mainAppl)
                try:
                    vtLog.vtLngCur(vtLog.DEBUG,'connect to %s;fn:%s'%(objAlias.GetHostAliasStr(),objAlias.GetFN()),self.GetOrigin())#self.mainAppl)
                except:
                    pass
            if i==0:
                #if idx==-1:
                #    wx.PostEvent(self,vtnxSecXmlMasterStart())
                
                #if netDoc.GetStatus()!=netDoc.SYNCH_FIN:
                #    break
                #if netDoc.IsFlag(netDoc.OPENED)==False:
                #    break
                #if netDoc.IsFlag(netDoc.CFG_VALID)==False:
                #    break
                
                netDoc.Connect2SrvByAlias(objAlias,dn,True)
                if objAlias is not None:
                    wx.PostEvent(self,vtnxSecXmlMasterAliasChanged(self,
                                appl,objAlias.GetAlias(),objAlias.GetHost()))
                else:
                    vtLog.vtLngCur(vtLog.WARN,'connection for appl %s not found'%(appl),self.GetOrigin())#self.mainAppl)
            else:
                if bCfgValid:
                    netDoc.Connect2SrvByAlias(objAlias,dn,True)
                    if objAlias is not None:
                        wx.PostEvent(self,vtnxSecXmlMasterAliasChanged(self,
                                    appl,objAlias.GetAlias(),objAlias.GetHost()))
                    else:
                        vtLog.vtLngCur(vtLog.WARN,'connection for appl %s not found'%(appl),self.GetOrigin())#self.mainAppl)
            i+=1
            pass
    def SignalMasterStart(self):
        wx.PostEvent(self,vtnxSecXmlMasterStart(self))
    def SignalMasterFin(self):
        wx.PostEvent(self,vtnxSecXmlMasterFinished(self))
    
    def OpenApplDocs(self):
        for k in self.dictDoc.keys():
            if k == self.appls[0]:
                continue
            self.dictDoc[k].Open()
    def SaveApplDocs(self,encoding='ISO-8859-1'):
        for k in self.dictDoc.keys():
            if k == self.appls[0]:
                continue
            self.dictDoc[k].Save(encoding=encoding)
    def OnLeftDown(self, event):
        event.Skip()
        wx.CallAfter(self.__doShowLeftMenu,event.GetX(),event.GetY())
        #self.__doShowMenu(event.GetX(),event.GetY())
    def __doShowLeftMenu(self,x,y):
        self.x=x
        self.y=y
        self.__createMenuIds__()
        bMenu=True
        menu=wx.Menu()
        mnIt=wx.MenuItem(menu,self.popupIdConnect,_(u'Browse'))
        mnIt.SetBitmap(vtArt.getBitmap(vtArt.Browse))
        menu.AppendItem(mnIt)
        menu.AppendSeparator()
        if self.IsAllDisconnected():
            bMenu|=self.__addMenuRecentConn__(menu)
        else:
            mnIt=wx.MenuItem(menu,self.popupIdDisconnect,_(u'Disconnect'))
            mnIt.SetBitmap(img_gui.getStoppedBitmap())
            menu.AppendItem(mnIt)
       
        if bMenu:
            self.PopupMenu(menu,(self.x,self.y))
        menu.Destroy()
    def ShowPopup(self):
        btn=self
        pos = btn.ClientToScreen( (0,0) )
        sz =  btn.GetSize()
        #self.pwMaster.Position(pos, (0, sz[1]))
        #self.pwMaster.Show(True)
        if 1:
            self.dlgBrowse.SetLocalDN(self.dlgCfgSetup.GetDN())
            self.dlgBrowse.UpdateConnections(self.dlgCfgSetup.GetObjConnections())
            self.dlgBrowse.Centre()
            self.dlgBrowse.Show(True)
        else:
            self.dlgMaster.Centre()
            self.dlgMaster.Show(True)
    def __createMenuIds__(self):
        if not hasattr(self,'popupIdConnectMn'):
            self.popupIdConnectMn=wx.NewId()
            self.popupIdConnect=wx.NewId()
            self.lPopupIdConnnect=[]
            for i in range(self.RECENT_SUB_MENU):
                id=wx.NewId()
                self.lPopupIdConnnect.append(id)
                wx.EVT_MENU(self,id,self.OnConnectQuick)
            
            self.popupIdDisconnect=wx.NewId()
            self.popupIdNew=wx.NewId()
            self.popupIdOpen=wx.NewId()
            self.popupIdSave=wx.NewId()
            self.popupIdSaveAs=wx.NewId()
            self.popupIdSaveCfg=wx.NewId()
            self.popupIdMasterSynch=wx.NewId()
            self.popupIdSetting=wx.NewId()
            self.popupIdLog=wx.NewId()
            self.popupIdStatus=wx.NewId()
            wx.EVT_MENU(self,self.popupIdConnect,self.OnConnect)
            wx.EVT_MENU(self,self.popupIdDisconnect,self.OnDisConnect)
            wx.EVT_MENU(self,self.popupIdNew,self.OnNew)
            wx.EVT_MENU(self,self.popupIdOpen,self.OnOpen)
            wx.EVT_MENU(self,self.popupIdSave,self.OnSave)
            wx.EVT_MENU(self,self.popupIdSaveAs,self.OnSaveAs)
            wx.EVT_MENU(self,self.popupIdSaveCfg,self.OnSaveCfg)
            wx.EVT_MENU(self,self.popupIdMasterSynch,self.OnMasterSynch)
            wx.EVT_MENU(self,self.popupIdSetting,self.OnSetting)
            wx.EVT_MENU(self,self.popupIdLog,self.OnLog)
            wx.EVT_MENU(self,self.popupIdStatus,self.OnStatus)
    def __addMenuRecentConn__(self,menu):
        l=self.dlgCfgSetup.GetConnections()
        if len(l)>0:
            i=0
            for s in l:
                mnIt=wx.MenuItem(menu,self.lPopupIdConnnect[i],s)
                mnIt.SetBitmap(img_gui.getConnectBitmap())
                menu.AppendItem(mnIt)
                i+=1
                if i>self.RECENT_SUB_MENU:
                    break
            return True
        return False
    def AddMenuGeneral(self,menu,obj,iPos=-1):
        try:
            self.__createMenuIds__()
            self.__addMenuGeneral__(menu,obj,iPos)
        except:
            import traceback
            traceback.print_exc()
            vtLog.vtLngTB(self.GetOrigin())
    def __addMenuGeneral__(self,menu,obj=None,iPos=-1):
        try:
            if menu is None:
                menu.wx.Menu()
                iPos=0
            if iPos<0:
                iPos=menu.GetMenuItemCount()
            if obj is None:
                menuConn=wx.Menu()
                mnIt=wx.MenuItem(menuConn,self.popupIdConnect,_(u'&Browse\tAlt+B'))
                mnIt.SetBitmap(vtArt.getBitmap(vtArt.Browse))
                menuConn.AppendItem(mnIt)
                #menuConn.AppendItem(iPos,mnIt)
                #iPos+=1
                menuConn.AppendSeparator()
                l=self.dlgCfgSetup.GetConnections()
                if len(l)>0:
                    i=0
                    for s in l:
                        if i<self.RECENT:
                            mnIt=wx.MenuItem(menu,self.lPopupIdConnnect[i],s)
                            mnIt.SetBitmap(img_gui.getConnectBitmap())
                            menu.InsertItem(iPos,mnIt)
                            iPos+=1
                        mnIt=wx.MenuItem(menuConn,self.lPopupIdConnnect[i],s)
                        mnIt.SetBitmap(img_gui.getConnectBitmap())
                        menuConn.AppendItem(mnIt)
                        i+=1
                        if i>self.RECENT_SUB_MENU:
                            break
                mnIt=wx.MenuItem(menu,self.popupIdConnectMn,_(u'Connect\tAlt+C'),subMenu=menuConn)
                mnIt.SetBitmap(img_gui.getConnectBitmap())
                menu.InsertItem(iPos,mnIt)
                iPos+=1
            else:
                mnIt=wx.MenuItem(menu,self.popupIdConnect,_(u'&Browse\tAlt+B'))
                mnIt.SetBitmap(vtArt.getBitmap(vtArt.Browse))
                menu.InsertItem(iPos,mnIt)
                iPos+=1
            mnIt=wx.MenuItem(menu,self.popupIdDisconnect,_(u'Disconnect\tAlt+D'))
            mnIt.SetBitmap(img_gui.getStoppedBitmap())
            menu.InsertItem(iPos,mnIt)
            iPos+=1
            mnIt=wx.MenuItem(menu,wx.ID_SEPARATOR)
            menu.InsertItem(iPos,mnIt)
            iPos+=1
            mnIt=wx.MenuItem(menu,self.popupIdNew,_(u'New\tCtrl+N'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.New))
            menu.InsertItem(iPos,mnIt)
            iPos+=1
            mnIt=wx.MenuItem(menu,self.popupIdOpen,_(u'Open\tCtrl+O'))
            mnIt.SetBitmap(img_gui.getOpenBitmap())
            menu.InsertItem(iPos,mnIt)
            iPos+=1
            mnIt=wx.MenuItem(menu,self.popupIdSave,_(u'Save\tCtrl+S'))
            mnIt.SetBitmap(img_gui.getSaveBitmap())
            menu.InsertItem(iPos,mnIt)
            iPos+=1
            mnIt=wx.MenuItem(menu,self.popupIdSaveAs,_(u'Save As\tCtrl+SHIFT+S'))
            mnIt.SetBitmap(img_gui.getSaveBitmap())
            menu.InsertItem(iPos,mnIt)
            iPos+=1
            mnIt=wx.MenuItem(menu,self.popupIdSaveCfg,_(u'Save Config\tCtrl+Alt+S'))
            mnIt.SetBitmap(img_gui.getSaveBitmap())
            menu.InsertItem(iPos,mnIt)
            iPos+=1
            mnIt=wx.MenuItem(menu,wx.ID_SEPARATOR)
            menu.InsertItem(iPos,mnIt)
            iPos+=1
            mnIt=wx.MenuItem(menu,self.popupIdMasterSynch,_(u'Master\tAlt+M'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Synch))
            menu.InsertItem(iPos,mnIt)
            iPos+=1
            mnIt=wx.MenuItem(menu,self.popupIdSetting,_(u'Setting\tAlt+S'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Settings))
            menu.InsertItem(iPos,mnIt)
            iPos+=1
            mnIt=wx.MenuItem(menu,self.popupIdLog,_(u'Log\tAlt+L'))
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Log))
            menu.InsertItem(iPos,mnIt)
            iPos+=1
            mnIt=wx.MenuItem(menu,self.popupIdStatus,_(u'Status\tAlt+T'))
            mnIt.SetBitmap(img_img.getNetConnectionsBitmap())
            menu.InsertItem(iPos,mnIt)
            if obj is not None:
                wx.EVT_MENU(obj,self.popupIdConnect,self.OnConnect)
                wx.EVT_MENU(obj,self.popupIdDisconnect,self.OnDisConnect)
                wx.EVT_MENU(obj,self.popupIdNew,self.OnNew)
                wx.EVT_MENU(obj,self.popupIdOpen,self.OnOpen)
                wx.EVT_MENU(obj,self.popupIdSave,self.OnSave)
                wx.EVT_MENU(obj,self.popupIdSaveAs,self.OnSaveAs)
                wx.EVT_MENU(obj,self.popupIdSaveCfg,self.OnSaveCfg)
                wx.EVT_MENU(obj,self.popupIdMasterSynch,self.OnMasterSynch)
                wx.EVT_MENU(obj,self.popupIdSetting,self.OnSetting)
                wx.EVT_MENU(obj,self.popupIdLog,self.OnLog)
                wx.EVT_MENU(obj,self.popupIdStatus,self.OnStatus)
            iPos+=1
        except:
            vtLog.vtLngTB(self.GetOrigin())
        return True
    def OnSetting(self,evt):
        self.dlgCfgSetup.Centre()
        self.dlgCfgSetup.Show(True)
        self.dlgCfgSetup.SetFocus()
    def OnSettingOld(self,evt):
        self.dlgMaster.Centre()
        self.dlgMaster.Show(True)
        
    def OnRightDown(self, event):
        event.Skip()
        wx.CallAfter(self.__doShowMenu,event.GetX(),event.GetY())
    def __doShowMenu(self,x,y):
        self.x=x
        self.y=y
        self.__createMenuIds__()
        menu=wx.Menu()
        bMenu=False
        bMenu|=self.__addMenuGeneral__(menu)
        if bMenu:
            self.PopupMenu(menu,(self.x,self.y))
        menu.Destroy()
    def OnSetup(self,evt):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'appl:%s'%(self.mainAppl))
        if self.docSetup is not None:
            if self.appl==self.appls[0]:
                oldRecentAlias=self.sRecentAlias
                oldRecentConn=self.sRecentConn
                oldHost=self.host
                oldAlias=self.alias
                oldUsr=self.usr
                if self.verbose:
                    vtLog.vtLngCallStack(self,vtLog.DEBUG,'recent alias:%s recent conn:%s'%(oldRecentAlias,oldRecentConn))
                    vtLog.vtLngCallStack(self,vtLog.DEBUG,'host:%s alias:%s usr:%s'%(oldHost,oldAlias,oldUsr))
            
        self.Setup()
    def __getCfgNode__(self):
        self.nodeSetup=self.docSetup.getRoot()
        return self.docSetup.getChildByLst(self.nodeSetup,self.lstCfg)
    
    def SaveCfg(self,fn=None):
        try:
            self.docSetup.DoSafe(self.docSetup.AlignDoc)
            self.thdProc.DoCallBackMultiBranchWX(
                        (self.docSetup.__Save__,(fn,),{}),#{'bThread':True}),
                        {   None:(self.__SaveCfgAsShowDlg__,[],{}),
                            0:None,
                        })
            return
            if hasattr(self.widLogging,'DoCallBackBranchWX'):
                self.docSetup.DoSafe(self.docSetup.AlignDoc)
                self.widLogging.DoCallBackMultiBranchWX(
                        (self.docSetup.__Save__,(fn,),{}),#{'bThread':True}),
                        {   None:(self.__SaveCfgAsShowDlg__,[],{}),
                            0:None,
                        })
            else:
                self.SaveCfgRisky(fn=fn)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def __SaveCfgAsShowDlg__(self):
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
        dlg = wx.FileDialog(self, _(u"Save Config File As"), ".", "", _(u"XML files (*.xml)|*.xml|all files (*.*)|*.*"), wx.SAVE)
        try:
            sFN=self.docSetup.GetFN()
            if sFN is not None:
                dlg.SetPath(sFN)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.SaveCfg(filename)
        finally:
            dlg.Destroy()
    def SaveCfgRisky(self,fn=None):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'appl:%s'%(self.mainAppl))
        if self.docSetup.acquire('dom',blocking=False)==False:
            vtLog.vtLngCur(vtLog.ERROR,'lock can not be acquired, abort...',self.GetOrigin())
        try:
            self.docSetup.AlignDoc()
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.docSetup.release('dom')
        if self.docSetup.Save(fn)<0:
            dlg = wx.FileDialog(self, _(u"Save Config File As"), ".", "", _(u"XML files (*.xml)|*.xml|all files (*.*)|*.*"), wx.SAVE)
            try:
                if dlg.ShowModal() == wx.ID_OK:
                    filename = dlg.GetPath()
                    #self.txtEditor.SaveFile(filename)
                    self.SaveCfg(filename)
            finally:
                dlg.Destroy()
        return 0
    def OnNew(self,evt):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'appl:%s'%(self.mainAppl))
        dictNet=self.dlgStatus.GetDictNet()
        conn=self.dlgCfgSetup.GetConnection()
        for appl in self.appls:
            dictNet[appl].Close()
            dictNet[appl].New()
    def OnOpen(self,evt):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'appl:%s'%(self.mainAppl))
        #self.AutoConnect2Srv(force=True)
        dictNet=self.dlgStatus.GetDictNet()
        conn=self.dlgCfgSetup.GetConnection()
        for appl in self.appls:
            objAlias=conn.GetAlias(appl)
            dictNet[appl].Open(objAlias.GetFN())
            wx.PostEvent(self,vtnxSecXmlMasterAliasChanged(self,
                        appl,objAlias.GetAlias(),objAlias.GetHost()))
    def OnSave(self,evt):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'appl:%s'%(self.mainAppl))
        self.Save()
    def OnSaveAs(self,evt):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'appl:%s'%(self.mainAppl))
        self.SaveAs()
    def OnSaveCfg(self,evt):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'appl:%s'%(self.mainAppl))
        self.SaveCfg()
    def OnConnect(self,evt):
        self.dlgBrowse.Centre()
        self.dlgBrowse.SetLocalDN(self.dlgCfgSetup.GetDN())
        self.dlgBrowse.Show()
        self.dlgBrowse.SetFocus()
        return
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'appl:%s'%(self.mainAppl))
        if self.VERBOSE:
            vtLog.CallStack('')
            print evt.GetEventObject().GetLabel(evt.GetId())
        self.SignalMasterStart()
        self.AutoConnect2Srv(force=True)
    def UpdateLogin(self,applSrc,usr,passwd):
        try:
            vtLog.vtLngCurWX(vtLog.INFO,'usr:%s'%(usr),self)
            conn=self.dlgCfgSetup.GetConnection()
            if conn.IsStore():
                o=conn.GetAlias(applSrc)
                o.SetLogin(usr,passwd)
                self.dlgCfgSetup.SaveSelectedConnection()
            if conn.IsIdent():
                for appl in self.appls:
                    if appl!=applSrc:
                        o=conn.GetAlias(appl)
                        o.SetLogin(usr,passwd)
            if conn.IsStore():
                self.dlgCfgSetup.SaveSelectedConnection()
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def UpdateLoginInfo(self,appl,oLogin):
        try:
            vtLog.vtLngCurWX(vtLog.INFO,'appl:%s;oLogin:%s'%(appl,oLogin),self)
            conn=self.dlgCfgSetup.GetConnection()
            if appl in self.appls:
                o=conn.GetAlias(appl)
                if o is None:
                    vtLog.vtLngCur(vtLog.WARN,'alias is None',self.GetOrigin())
                else:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'oldLogin:%s;newLogin:%s'%(o,oLogin),self.GetOrigin())
                    o.SetLoginInfo(oLogin)
            else:
                vtLog.vtLngCur(vtLog.ERROR,'appl:%s not found;appls:%s'%(
                        appl,self.appls),self.GetOrigin())
            #if conn.IsStore():
            #    self.dlgCfgSetup.SaveSelectedConnection()
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def DoAutoConnect(self,sName,bOffline=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCurWX(vtLog.INFO,'%s offline:%d'%(sName,bOffline),self)
        self.__connectionQuick__(sName,bOffline=bOffline)
    def __WaitDisConnected__(self,zTimeOut):
                #zTimeOut=200
                zTime=0
                self.__logStatus__()
                while self.IsAllDisconnected()==False:
                    time.sleep(0.1)
                    zTime+=0.1
                    if zTime>zTimeOut:
                        vtLog.vtLngCur(vtLog.CRITICAL,'timeout closing connections'%(),self.GetOrigin())
                        dlg = vtmMsgDialog(self,
                                _(u'Timeout closing connections!\nAt least one open connection!\n\nManual interaction required.'),
                                u'vtNetSecXmlGuiMaster' + u' '+ _(u'Connect'),
                                wx.OK | wx.YES_DEFAULT |  wx.ICON_ERROR
                                )
                        dlg.ShowModal()
                        dlg.Destroy()
                        return -1
                return 0
    def __showDisConnectFault__(self):
        vtLog.vtLngCur(vtLog.CRITICAL,'timeout closing connections'%(),self.GetOrigin())
        dlg = vtmMsgDialog(self,
                                _(u'Timeout closing connections!\nAt least one open connection!\n\nManual interaction required.'),
                                u'vtNetSecXmlGuiMaster' + u' '+ _(u'Connect'),
                                wx.OK | wx.YES_DEFAULT |  wx.ICON_ERROR
                                )
        dlg.ShowModal()
        dlg.Destroy()
        self.dlgMaster.Centre()
        self.dlgMaster.Show(True)
    def __connectionQuick__(self,sName,bOffline=False):
        try:
            self.__logStatus__()
            if self.oSF.IsShutDown or self.oSF.IsShuttingDown:
                vtLog.vtLngCur(vtLog.INFO,'skipped due active shutdown'%(),self.GetOrigin())
                return
            vtLog.vtLngCur(vtLog.DEBUG,'%s offline:%d'%(sName,bOffline),self.GetOrigin())
            if self.IsAllDisconnected()==False:
                #return         # 071014:wro handle new connect
                dlg = vtmMsgDialog(self,
                        _(u'At least one open connection!\n\nDo you want to close all connetions?'),
                        u'vtNetSecXmlGuiMaster' + u' '+ _(u'Connect'),
                        wx.YES_NO | wx.YES_DEFAULT |  wx.ICON_QUESTION
                        #wx.YES_NO | wx.NO_DEFAULT | wx.CANCEL | wx.ICON_INFORMATION
                        )
                if dlg.ShowModal()==wx.ID_NO:
                    dlg.Destroy()
                    return
                vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
                self.__browseStop__()
                self.thdProc.DoCallBackTimeOutWX(0.1,200,
                            (self.IsAllDisconnected,(),{}),
                            (self.__connectionQuick__,(sName,),{'bOffline':bOffline}),
                            (self.__showDisConnectFault__,(),{})
                            )
                #else:
                #    vtLog.vtLngCur(vtLog.CRITICAL,''%(),self.GetOrigin())
                #    self.__WaitDisConnected__(200)
                return
                #self.OnBrowseStop(None)
                #self.ShutDown()
                self.__logStatus__()
            if self.verbose:
                vtLog.vtLngCur(vtLog.DEBUG,'%s offline:%d'%(sName,bOffline),self.GetOrigin())
            if bOffline==False:
                iRet=self.dlgCfgSetup.SelectConnection(sName)
                iRet=0
                #self.dlgBrowse.UpdateConnections(self.dlgCfgSetup.GetObjConnections())
            else:
                iRet=self.dlgCfgSetup.SelectConnection(sName)
                
            if self.dlgCfgSetup.SelectConnection(sName)<0:
                # show browse dialog
                if self.dlgBrowse.IsShown()==False:
                    self.dlgBrowse.SetLocalDN(self.dlgCfgSetup.GetDN())
                    self.dlgBrowse.Show()
                return
            try:
                for name in self.lNetAppl:
                    netDoc=self.dNetDoc[name]
                    #netDoc.SetFlag(netDoc.DATA_SETTLED,False)
                    netDoc.PreConnect()
            except:
                vtLog.vtLngTB(self.GetOrigin())
            self.dlgMaster.SetCfgNode()
            dn=self.dlgCfgSetup.GetDN()
            conn=self.dlgCfgSetup.GetConnection()
            vtLog.vtLngCur(vtLog.INFO,conn.GetHostAliasStr(self.appls[0]),self.GetOrigin())#self.mainAppl)
            conn.SetDateTime2Now()
            self.dlgCfgSetup.GetLoginNode()
            if conn.GetAlias(self.appls[0]).IsOffline() or bOffline:
                self.oSF.SetState(self.oSF.OPEN)
                dictNet=self.dlgStatus.GetDictNet()
                conn=self.dlgCfgSetup.GetConnection()
                #for appl in self.appls:
                #    dictNet[appl].SetBlockNotify(True) # 060521
                for appl in self.appls:
                    #dictNet[appl].Open(conn.GetAlias(appl).GetFN())
                    dictNet[appl].Connect2SrvByAlias(conn.GetAlias(appl),dn,offline=True)
                    objAlias=conn.GetAlias(appl)
                    wx.PostEvent(self,vtnxSecXmlMasterAliasChanged(self,
                            appl,objAlias.GetAlias(),objAlias.GetHost()))
            else:
                self.SignalMasterStart()
                self.oSF.SetState(self.oSF.CONNECT)
                #self.bConnectAll=True
                #for appl in self.appls:
                #    self.dNetDoc[appl].SetBlockNotify(True) # 060521
                self.AutoConnect2Srv(force=True)
            #self.dlgStatus.Centre()
            #self.dlgStatus.Show(True)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnBrowseOpen(self,evt):
        try:
            sName=evt.GetConnectionName()
            if self.verbose:
                vtLog.vtLngCur(vtLog.DEBUG,'appl:%s;connection:%s'%(self.mainAppl,sName),self.GetOrigin())#self.mainAppl)
            self.__connectionQuick__(sName,True)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnBrowseConnect(self,evt):
        try:
            sName=evt.GetConnectionName()
            if self.verbose:
                vtLog.vtLngCur(vtLog.DEBUG,'appl:%s;connection:%s'%(self.mainAppl,sName),self.GetOrigin())#self.mainAppl)
            self.__connectionQuick__(sName)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def __browseStop__(self):
            self.oSF.SetState(self.oSF.DISCONNECT)
            for appl in self.appls:
                try:
                    self.dNetDoc[appl].DisConnect()
                except:
                    pass
    def OnBrowseStop(self,evt):
        try:
            vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
            #self.bConnectAll=False
            self.__browseStop__()
            self.thdProc.DoCallBackTimeOutWX(0.1,200,
                            (self.IsAllDisconnected,(),{}),
                            None,
                            (self.__showDisConnectFault__,(),{})
                            )
            #else:
            #    vtLog.vtLngCur(vtLog.CRITICAL,''%(),self.GetOrigin())
            #    self.__WaitDisConnected__(200)
            #wx.FutureCall(100,self.__checkDisConnected__,100,200)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def __checkDisConnected__(zDly,zTimeOut):
        try:
            self.__logStatus__()
            if self.IsAllDisconnected()==False:
                if zTimeOut<=0:
                    vtLog.vtLngCur(vtLog.CRITICAL,'timeout closing connections'%(),self.GetOrigin())
                    self.__logStatus__()
                    dlg = vtmMsgDialog(self,
                                _(u'Timeout closing connections!\nAt least one open connection!\n\nManual interaction required.'),
                                u'vtNetSecXmlGuiMaster' + u' '+ _(u'Connect'),
                                wx.OK | wx.YES_DEFAULT |  wx.ICON_ERROR
                                )
                    dlg.Show()
            else:
                wx.FutureCall(100,self.__checkDisConnected__,zDly,zTimeOut-1)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnCfgSetupApply(self,evt):
        vtLog.vtLngCurWX(vtLog.INFO,'bAdd:%d;appl:%s'%(evt.IsAdd(),evt.GetAppl()),self)
        evt.Skip()
        if evt.IsAdd():
            self.dlgBrowse.SetLocalDN(self.dlgCfgSetup.GetDN())
            self.dlgBrowse.UpdateConnections(self.dlgCfgSetup.GetObjConnections())
            self.dlgBrowse.Show()
            self.dlgBrowse.SetFocus()
        else:
            appl=evt.GetAppl()
            if appl is not None:
                try:
                    idx=self.lNetAppl.index(appl)
                    netDoc=self.dNetDoc[appl]
                    netDoc.__logStatus__()
                    if netDoc.IsFlag(netDoc.CONN_ATTEMPT)==False:
                        self.AutoConnect2Srv(idx)
                except:
                    vtLog.vtLngTB(self.GetOrigin())
            iMax=128
            while iMax>0:
                appl=self.dlgCfgSetup.GetConnectionAppl2Cfg()
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'appl:%s'%(appl),self.GetOrigin())#self.mainAppl)
                if appl is None:
                    break
                bRet=self.dlgCfgSetup.ConfigAppl(appl)
                if bRet:
                    iRet=1
                    netDoc=self.dNetDoc[appl]
                else:
                    self.dlgCfgSetup.Show(False,True,appl)
                    return
                    #iRet=self.dlgCfgSetup.ShowModal()
                try:
                    idx=self.lNetAppl.index(appl)
                    netDoc.__logStatus__()
                    if netDoc.IsFlag(netDoc.CONN_ATTEMPT)==False:
                        self.AutoConnect2Srv(idx)
                except:
                    vtLog.vtLngTB(self.GetOrigin())
                iMax-=1
            if iMax==0:
                vtLog.vtLngCur(vtLog.CRITICAL,'emergency stop',self.GetOrigin())
            
                #if iRet>0:
                #    applNew=self.dlgCfgSetup.GetConnectionAppl2Cfg()
                #    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                #        vtLog.vtLngCur(vtLog.DEBUG,'applNew:%s'%(applNew),self.GetOrigin())#self.mainAppl)
                #    if appl!=applNew:
                #        if appl in self.lNetAppl:
                #            idx=self.lNetAppl.index(appl)
                            #self.AutoConnect2Srv(idx+1)  # 061006 wro block automatic connect of related aliases
                #        else:
                #            vtLog.vtLngCur(vtLog.ERROR,'appl:%s not found'%(appl),self.GetOrigin())#self.mainAppl)
        self.dlgCfgSetup.SaveSelectedConnection()
        self.SaveCfg()
    def OnBrowseAdd(self,evt):
        try:
            self.dlgCfgSetup.Centre()
            self.dlgCfgSetup.ConfigAdd()
            self.dlgCfgSetup.Show(True,True)
            self.dlgCfgSetup.SetFocus()
            return
            #self.dlgCfgSetup.Show()
            #return
            iRet=self.dlgCfgSetup.ShowModal()
            self.dlgBrowse.SetLocalDN(self.dlgCfgSetup.GetDN())
            self.dlgBrowse.UpdateConnections(self.dlgCfgSetup.GetObjConnections())
            self.dlgBrowse.Show()
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnBrowseDel(self,evt):
        try:
            sConn=evt.GetConnection()
            self.dlgCfgSetup.ConfigDel(sConn)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnBrowseCfg(self,evt):
        try:
            #self.dlgCfgSetup.ConfigAdd()
            self.dlgCfgSetup.Show(False,True)
            #self.dlgBrowse.UpdateConnections(self.dlgCfgSetup.GetObjConnections())
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnCfgChanged(self,evt):
        try:
            self.dlgBrowse.UpdateConnections(self.dlgCfgSetup.GetObjConnections())
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnBrowseMaster(self,evt):
        try:
            self.dlgMaster.Show()
            self.dlgMaster.SetFocus()
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnBrowseLog(self,evt):
        try:
            self.dlgLog.Show()
            self.dlgLog.SetFocus()
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnBrowseStatus(self,evt):
        try:
            self.dlgStatus.Show()
            self.dlgStatus.SetFocus()
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnConnectQuick(self,evt):
        try:
            sName=evt.GetEventObject().GetLabel(evt.GetId())
            if self.verbose:
                vtLog.vtLngCur(vtLog.DEBUG,'appl:%s;connection:%s'%(self.mainAppl,sName),self.GetOrigin())#self.mainAppl)
            if self.VERBOSE:
                vtLog.CallStack('')
                print sName
            self.__connectionQuick__(sName)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnDisConnect(self,evt):
        self.oSF.SetState(self.oSF.DISCONNECT)
        #self.bConnectAll=False
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'appl:%s'%(self.mainAppl))
        for appl in self.appls:
            try:
                self.dNetDoc[appl].DisConnect()
            except:
                pass
    def OnMasterSynch(self,evt):
        if self.verbose:
            vtLog.vtLngCur(vtLog.DEBUG,'appl:%s'%(self.mainAppl),self.GetOrigin())#self.mainAppl)
        try:
            self.dlgMaster.Centre()
            self.dlgMaster.Show()
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnLog(self,evt):
        if self.verbose:
            vtLog.vtLngCur(vtLog.DEBUG,'appl:%s'%(self.mainAppl),self.GetOrigin())#self.mainAppl)
        try:
            self.dlgLog.Centre()
            self.dlgLog.Show()
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnStatus(self,evt):
        vtLog.vtLngCur(vtLog.INFO,'appl:%s'%(self.mainAppl),self.GetOrigin())#self.mainAppl)
        if self.verbose:
            vtLog.vtLngCur(vtLog.DEBUG,'appl:%s'%(self.mainAppl),self.GetOrigin())#self.mainAppl)
        try:
            self.dlgStatus.Centre()
            self.dlgStatus.Show()
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnLoginDlgFin(self,evt):
        evt.Skip()
        try:
            sAppl=evt.GetAppl()
            sUsr=evt.GetUsr()
            if sUsr is None:
                vtLog.vtLngCur(vtLog.INFO,'dialog canceled or closed',self.GetOrigin())#self.mainAppl)
                self.ReleaseLogin()
                self.__doLoginDlgAbort__(sAppl)
                return
            sPasswd=evt.GetPasswd()
            sHost=evt.GetHost()
            sPort=evt.GetPort()
            sAlias=evt.GetAlias()
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'appl:%s;host:%s;port:%s;alias:%s;usr:%s'%(sAppl,
                    sHost,sPort,sAlias,sUsr),self.GetOrigin())#self.mainAppl)
            self.UpdateLogin(sAppl,sUsr,sPasswd)
            self.SetLogin(sAppl,sHost,sPort,
                                        sAlias,sUsr,sPasswd)
            self.ReleaseLogin()
            netDoc=self.dNetDoc[sAppl]
            if netDoc.LoginFromGui(sUsr,sPasswd)==False:
                self.__doLoginDlgAbort__(sAppl)
                if sAppl==self.lNetAppl[0]:
                    # go ahead with sub connections
                    netDoc.SetFlag(netDoc.CFG_VALID,True)
                    netDoc.__setStatus__(netDoc.CONFIG)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def __doLoginDlgAbort__(self,sAppl):
        try:
            netDoc=self.dNetDoc[sAppl]
            if sAppl==self.lNetAppl[0]:
                # go ahead with sub connections
                netDoc.SetFlag(netDoc.CFG_VALID,True)
                #netDoc.__setStatus__(netDoc.CONFIG)
            netDoc.DisConnect()
            if sAppl==self.lNetAppl[0]:
                iCount=len(self.lNetAppl)
                #vtLog.vtLngCur(vtLog.DEBUG,'lNetAppl:%s'%(vtLog.pformat(self.lNetAppl)),self.GetOrigin())#self.mainAppl)
                for idx in range(1,iCount):
                    name=self.lNetAppl[idx]
                    netDoc=self.dNetDoc[name]
                    vtLog.vtLngCur(vtLog.INFO,'appl:%s;idx:%d;connect_attempt:%s'%\
                            (name,idx,netDoc.IsFlag(netDoc.CONN_ATTEMPT)),self.GetOrigin())#self.mainAppl)
                    if netDoc.IsFlag(netDoc.CONN_ATTEMPT)==False:
                            self.AutoConnect2Srv(idx)
                            #self.thdStart.Start()
                #self.bConnectAll=False
                self.oSF.SetState(self.oSF.CONNECT_FLT)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def __getHostAliasName__(self,doc,n):
        sName=doc.getNodeText(n,'name')+':'+doc.getNodeText(n,'port')+'//'+doc.getNodeText(n,'alias')
        return sName
    def __getConnHostAliasName__(self,doc,n):
        sName=doc.getNodeText(n,'host')+':'+doc.getNodeText(n,'port')+'//'+doc.getNodeText(n,'alias')
        return sName
    def __updateConfig__(self):
        vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())#self.mainAppl)
        #dictNet=self.dlgStatus.GetDictNet()
        #lstNetAppl=self.dlgStatus.GetNetNames()
        doc=self.dNetDoc[self.lNetAppl[0]]
        #doc.SetFlag(doc.CFG_VALID,True)
        self.dlgCfgSetup.SetDoc(doc)
        iMax=128
        while iMax>0:
            appl=self.dlgCfgSetup.GetConnectionAppl2Cfg()
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'appl:%s'%(appl),self.GetOrigin())#self.mainAppl)
            if appl is None:
                break
            bRet=self.dlgCfgSetup.ConfigAppl(appl)
            if bRet:
                iRet=1
            else:
                self.dlgCfgSetup.Show(False,True,appl=appl)
                return
                iRet=self.dlgCfgSetup.ShowModal()
            if iRet>0:
                applNew=self.dlgCfgSetup.GetConnectionAppl2Cfg()
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'applNew:%s'%(applNew),self.GetOrigin())#self.mainAppl)
                if appl!=applNew:
                    if appl in self.lNetAppl:
                        idx=self.lNetAppl.index(appl)
                        #self.AutoConnect2Srv(idx+1)  # 061006 wro block automatic connect of related aliases
                    else:
                        vtLog.vtLngCur(vtLog.ERROR,'appl:%s not found'%(appl),self.GetOrigin())#self.mainAppl)
                else:
                    break       # 070821:wro break endless loop
            else:
                break
            iMax-=1
        if iMax==0:
            vtLog.vtLngCur(vtLog.CRITICAL,'emergency stop',self.GetOrigin())
        self.dlgCfgSetup.SaveSelectedConnection()
        self.SaveCfg()
    #def Notify(self,name,alias,netDoc,iStatusPrev,iStatus,*args,**kwargs):
    def NotifyStateChangeMaster(self,netMaster,iStatusPrev,iStatus,zStamp,*args,**kwargs):
        try:
            self.Refresh()
            #self.dlgMaster.Notify(msgTup[1],netDoc,iStatusPrev,iStatus,*msgTup[4],**msgTup[5])
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def NotifyStateChangeAlias(self,netDoc,iStatusPrev,iStatus,zStamp,*args,**kwargs):
        try:
            name=netDoc.GetAppl()
            alias=netDoc.GetAlias()
            self.dlgLog.AddLog(zStamp,name,netDoc.oSF.GetStateName(iStatus),iStatusPrev,iStatus)
            self.dlgMaster.Notify(name,netDoc,iStatusPrev,iStatus,*args,**kwargs)
            self.Refresh()
            self.__check__()
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def _doConnectSubAlias(self):
        netDoc=self.netDocMaster
        netDoc.__logStatus__()
        try:
            self.oSF.SetState(self.oSF.CONNECTED_MAIN)
            iCount=len(self.lNetAppl)
            for idx in range(1,iCount):
                name=self.lNetAppl[idx]
                netDoc=self.dNetDoc[name]
                vtLog.vtLngCur(vtLog.INFO,'appl:%s;idx:%d;connect_attempt:%s'%\
                                (name,idx,netDoc.IsConnAttempt()),self.GetOrigin())
                if netDoc.IsConnAttempt()==False:
                    self.AutoConnect2Srv(idx)
            self.oSF.SetState(self.oSF.CONNECT_SUB)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def _doConnectSubAliasOnFlt(self):
        netDoc=self.netDocMaster
        netDoc.__logStatus__()
        try:
            self.oSF.SetState(self.oSF.CONNECT_FLT_MAIN)
            vtLog.vtLngCur(vtLog.INFO,'',self.GetOrigin())
            self.__updateConfig__()
            iCount=len(self.lNetAppl)
            for idx in range(1,iCount):
                name=self.lNetAppl[idx]
                netDoc=self.dNetDoc[name]
                vtLog.vtLngCur(vtLog.INFO,'appl:%s;idx:%d;connect_attempt:%s'%\
                                (name,idx,netDoc.IsFlag(netDoc.CONN_ATTEMPT)),self.GetOrigin())
                if netDoc.IsConnAttempt()==False:
                    self.AutoConnect2Srv(idx)
            self.oSF.SetState(self.oSF.CONNECT_SUB)
        except:
            vtLog.vtLngTB(self.GetOrigin())#self.mainAppl)
    def IsAllSettled(self):
        for name in self.lNetAppl:
            netDoc=self.dNetDoc[name]
            if netDoc.IsSettled()==False:
                return False
        return True
    def IsDataAccessed(self):
        for name in self.lNetAppl:
            netDoc=self.dNetDoc[name]
            if netDoc.IsDataAccessed()==True:
                return True
        return False
    def IsAllDisconnected(self):
        for netDoc in self.dNetDoc.values():
            #if netDoc.GetStatus() not in [netDoc.STOPPED,netDoc.DISCONNECTED,
            #                                netDoc.OPEN_OK,netDoc.OPEN_FLT]:
            #if netDoc.IsDisConn():
            if netDoc.IsConnActive():
                netDoc.__logStatus__()
                vtLog.vtLngCur(vtLog.INFO,'appl:%s not disconnected'%(netDoc.GetAppl()),self.GetOrigin())
                return False
        vtLog.vtLngCur(vtLog.INFO,'all disconnected',self.GetOrigin())
        return True
    def IsAllClosed(self):
        for netDoc in self.dNetDoc.values():
            if netDoc.IsFlag(netDoc.OPENED)==True:
                return False
        return True
    def IsAllStopped(self):
        for netDoc in self.dNetDoc.values():
            if netDoc.IsFlag(netDoc.STOPPED)==True:
                return False
        return True
    def __getLogStatusStr__(self):
        l=['%-12s:%s'%('master',self.oSF.GetStr(u' '))]
        for name in self.lNetAppl:
            netDoc=self.dNetDoc[name]
            l.append('%-12s:%s'%(name,netDoc.__getLogStatusStr__().replace(';',' ')))
        return ';'.join(l)
    def __logStatus__(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,u';'.join([self.__getLogStatusStr__(),'',
                        self.oSF.GetStrFull(u';')]),
                        origin=self.GetOrigin())#self.mainAppl)
    def __check__(self):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,''%(),self.GetOrigin())
            self.__logStatus__()
            if self.oSF.IsShuttingDown:
                return
            if self.oSF.IsShutDown:
                return
            if self.oSF.IsReady:
                return
            if self.oSF.IsSettled:
                return
            if self.IsAllSettled()==False:
                return
            vtLog.vtLngCur(vtLog.INFO,'all settled',self.GetOrigin())
            bOpenMaster=False
            bOffLine=not self.oSF.IsOnline
            for name in self.lNetAppl:
                netDoc=self.dNetDoc[name]
                netDoc.SetBlockNotify(False)
                netDoc.__logStatus__()
                if netDoc.IsUsrInteractNeeded(bOffLine):
                    netDoc.__logStatus__()
                    bOpenMaster=True
            if self.oSF.IsOnline:
                self.oSF.SetState(self.oSF.CONNECTED)
            else:
                self.oSF.SetState(self.oSF.OPENED)
            self.SignalMasterFin()
            if bOpenMaster:
                if vtLog.vtLngIsLogged(vtLog.WARN):
                    vtLog.vtLngCur(vtLog.WARN,
                                        'offline:%d;%s'%(bOffLine,self.__getLogStatusStr__()),
                                        origin=self.GetOrigin())#self.mainAppl)
                try:
                    self.dlgMaster.Centre()
                    self.dlgMaster.Show(True)
                except:
                    vtLog.vtLngTB(self.GetOrigin())#self.mainAppl)
            self.__logStatus__()
            #vtLog.vtLngCur(vtLog.INFO,'blocked:%d;all conn:%d'%(self.dNetDoc[self.appls[0]].IsBlockNotify(),self.bConnectAll),self.GetOrigin())#self.mainAppl)
            netDocMaster=self.dNetDoc[self.appls[0]]
            if 0 and netDocMaster.IsBlockNotify():
                #if netDocMaster.IsFlag(netDocMaster.CONN_ATTEMPT)==False:
                if netDocMaster.IsConnAttempt()==False:
                    if netDocMaster.GetStatus() in [netDocMaster.OPEN_OK]:
                        bOffLine=True
                    else:
                        bOffLine=False
                else:
                    bOffLine=False   #070530:wro
                bOpenMaster=False
                if self.IsAllSettled():
                        vtLog.vtLngCur(vtLog.INFO,'all settled',self.GetOrigin())#self.mainAppl)
                        #try:
                        #    for name in self.lNetAppl:
                        #        netDoc=self.dNetDoc[name]
                        #        netDoc.Build()
                        #except:
                        #    vtLog.vtLngTB(self.GetOrigin())#self.mainAppl)
                        for name in self.lNetAppl:
                            netDoc=self.dNetDoc[name]
                            netDoc.SetBlockNotify(False)
                            netDoc.__logStatus__()
                            if netDoc.IsUsrInteractNeeded(bOffLine):
                                if bOffLine:
                            #    if netDoc.GetStatus() not in [netDoc.OPEN_OK]:
                                    vtLog.vtLngCur(vtLog.INFO,
                                        'offline:%d;appl:%s;status:%s'%(bOffLine,name,netDoc.GetStatusStr()),self.GetOrigin())#self.mainAppl)
                                    bOpenMaster=True
                                else:
                            #    if netDoc.GetStatus() not in [netDoc.SYNCH_FIN]:
                                    vtLog.vtLngCur(vtLog.INFO,
                                        'offline:%d;appl:%s;status:%s'%(bOffLine,name,netDoc.GetStatusStr()),self.GetOrigin())#self.mainAppl)
                                    bOpenMaster=True
                        self.SignalMasterFin()
                        #netDocMaster.genIdsRef()  # 070728:wro process this at synch
                        if bOpenMaster:
                            if vtLog.vtLngIsLogged(vtLog.WARN):
                                vtLog.vtLngCur(vtLog.WARN,
                                        'offline:%d;%s'%(bOffLine,self.__getLogStatusStr__()),
                                        origin=self.GetOrigin())#self.mainAppl)
                            try:
                                self.dlgMaster.Centre()
                                self.dlgMaster.Show(True)
                            except:
                                vtLog.vtLngTB(self.GetOrigin())#self.mainAppl)
                self.__logStatus__()
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                self.__logStatus__()
                s=[]
                iMasterStatus=0
                for name in self.lNetAppl:
                    netDoc=self.dNetDoc[name]
                    #bConnAct=netDoc.IsFlag(netDoc.CONN_ACTIVE)
                    #bConnAtp=netDoc.IsFlag(netDoc.CONN_ATTEMPT)
                    bConnAct=netDoc.IsConnActive()
                    bConnAtp=netDoc.IsConnAttempt()
                    netDoc.__logStatus__()
                    iStatus=netDoc.GetStatus()
                    if iStatus>iMasterStatus:
                        iMasterStatus=iStatus
                    sStatus=netDoc.GetStatusStr()
                    s.append('name:%-12s connect:%d/%d status:0x%02x %s'%(name,bConnAtp,bConnAct,iStatus,sStatus)) 
                vtLog.vtLngCur(vtLog.DEBUG,'%s'%(';'.join(s)),self.GetOrigin())#self.mainAppl)
                #self.SetBitmap(netDoc.__getMedBitmapByStatus__(iMasterStatus))
        except:
            vtLog.vtLngTB(self.GetOrigin())#self.mainAppl)
        return
        bAllSettled=True
        bAllOpened=True
        bReleaseBlock=False
        i=0
        for name in self.lNetAppl:
            netDoc=self.dNetDoc[name]
            #vtLog.vtLngCur(vtLog.DEBUG,'full:0x%04x 0x%04x state:%s '%(netDoc.iStatusPrev,netDoc.iStatus,netDoc.GetStatusStr()),verbose=1)
            if netDoc.IsSettled()==False:
                bAllSettled=False
                #vtLog.vtLngCur(vtLog.DEBUG,'not settled:%s'%name,self.GetOrigin())#self.mainAppl)
            else:
                if i==0:
                    bReleaseBlock=True
            if netDoc.IsFlag(netDoc.OPENED)==False:
                bAllOpened=False
            if bReleaseBlock and i>0:
                #if self.verbose:
                #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'release block')
                netDoc.SetBlockNotify(False)
            i+=1
        if bAllSettled:
            self.bConnectAll=False
            vtLog.vtLngCur(vtLog.DEBUG,'all settled',self.GetOrigin())#self.mainAppl)
            for name in self.lNetAppl:
                netDoc=self.dNetDoc[name]
                if self.verbose:
                    vtLog.vtLngCur(vtLog.DEBUG,'state:%s full:%d'%(netDoc.GetStatusStr(),netDoc.iStatus),self.GetOrigin())#self.mainAppl)
                netDoc.SetBlockNotify(False)
        if bAllOpened:
            self.bOpenAll=False
            vtLog.vtLngCur(vtLog.DEBUG,'all opened',self.GetOrigin())#self.mainAppl)

    def Stop(self):
        vtLog.vtLngCur(vtLog.DEBUG,'started',self.GetOrigin())#self.mainAppl)
        self.bBlockNotify=True
        if self.dlgClose is not None:
            #self.dlgClose.Show(True)
            self.dlgClose.SetFocus()
            return
        self.dlgClose=vNetXmlGuiMasterCloseDialog(self)
        self.dlgClose.Centre()
        self.dlgClose.SetDictNet(self.dNetDoc)
        wx.EVT_CLOSE(self.dlgClose,self.OnShutDown)
        #dlg.ShowModal()
        self.dlgClose.Show(True)
        #vtLog.vtLngCur(vtLog.DEBUG,'finished',self.GetOrigin())#self.mainAppl)
        #time.sleep(2)
    def ShutDown(self):
        vtLog.vtLngCur(vtLog.INFO,'',self.GetOrigin())#self.mainAppl)
        #self.dlgMaster.ShutDown()
        self.oSF.SetState(self.oSF.SHUTTINGDOWN)
        #self.SetFlag(self.SHUTDOWN,True)
        try:
            self.dlgStatus.ShutDown()
            self.dlgMaster.ShutDown()
            self.dlgCfgSetup.ShutDown()
            #docMain=self.dNetDoc[self.lNetAppl[0]]
            #docMain.SetNetDocs({})
            for netDoc in self.dNetDoc.itervalues():
                netDoc.ShutDown()
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.dlgBrowse.Show(False)
        self.SaveCfg()
        if self.USE_TIMER4QUEUE_CHECK:
            self.timer.Stop()
        #for k in self.dNetDoc.keys():
        #    netDoc=self.dNetDoc[k]
        #    netDoc.SetFlag(netDoc.SHUTDOWN,True)
        self.Stop()
        #wx.PostEvent(self,vtnxSecXmlMasterShutdown(self))
    def IsShutDownActive(self):
        return self.oSF.IsShuttingDown or self.oSF.IsShutDown
        #return self.IsFlag(self.SHUTDOWN)
    def OnShutDown(self,evt):
        evt.Skip()
        self.dNetDoc={}
        self.lNetAppl=[]
        self.oSF.SetState(self.oSF.SHUTDOWN)
        #self.SetFlag(self.SHUTDOWN_FIN,True)
        wx.PostEvent(self,vtnxSecXmlMasterShutdown(self))
    def IsShutDownFinished(self):
        return self.oSF.IsShutDown
        #return self.IsFlag(self.SHUTDOWN_FIN)
    def IsModified(self):
        return self.dlgStatus.IsModified()
    def Save(self,bModified=False):
        self.dlgStatus.Save(bModified)
    def SaveAs(self):
        try:
            docMain=self.GetMainDoc()
            if docMain.IsActive():
                sMsg=_(u'Connection still active!')+' \n\n'+_('Close connection to use this feautre.')
                dlg=wx.MessageDialog(self,sMsg ,
                            self.GetName(),
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg.Destroy()
                return
            dlg = wx.FileDialog(self, _("Save File As"), ".", "", _("XML files (*.xml)|*.xml|all files (*.*)|*.*"), wx.SAVE)
            try:
                fn=docMain.GetFN()
                if fn is not None:
                    dlg.SetPath(fn)
                if dlg.ShowModal() == wx.ID_OK:
                    filename = dlg.GetPath()
                    #self.txtEditor.SaveFile(filename)
                    docMain.Save(filename)
            finally:
                dlg.Destroy()
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def Open(self,k=None):
        dictNet=self.dlgStatus.GetDictNet()
        conn=self.dlgCfgSetup.GetConnection()
        for appl in self.appls:
            if k is not None:
                if k!=appl:
                    continue
            dictNet[appl].Open(conn.GetAlias(appl).GetFN())
    def GetDN(self):
        return self.dn
    def GetMainAppl(self):
        return self.appls[0]
    def GetAppls(self):
        return self.appls
    def GetMainDoc(self):
        try:
            return self.dNetDoc[self.appls[0]]
        except:
            return None
    def GetNetDoc(self,name,fallback=False):
        try:
            return self.dNetDoc[name]
        except:
            return self.GetMainDoc()
        #return self.dlgStatus.GetNetDoc(name)
    def GetNetDocs(self):
        try:
            return self.GetMainDoc().GetNetDocs()
        except:
            return None
    def GetNetDocNodeInfos(self,name,id,lst,lang=None):
        netDoc=self.GetNetDoc(name)
        if netDoc is None:
            return None
        node=netDoc.getNodeById(id)
        if node is None:
            return None
        netDoc.acquire()
        try:
            d={}
            for it in lst:
                val=netDoc.getNodeInfoVal(node,it,lang=lang)
                d[it]=val
        except:
            vtLog.vtLngTB(self.GetOrigin())
        netDoc.release()
        return d
    def GetNetDocNodeInfosById(self,name,id,lst,lang=None):
        netDoc=self.GetNetDoc(name)
        if netDoc is None:
            return None
        node=netDoc.getNodeById(id)
        if node is None:
            return None
        netDoc.acquire()
        try:
            d={}
            for it in lst:
                val=netDoc.getNodeInfoVal(node,it,lang=lang)
                d[it]=val
        except:
            vtLog.vtLngTB(self.GetOrigin())
        netDoc.release()
        return d
    def GetNetDocNodeInfosByIdNum(self,name,id,lst,lang=None):
        netDoc=self.GetNetDoc(name)
        if netDoc is None:
            return None
        node=netDoc.getNodeByIdNum(id)
        if node is None:
            return None
        netDoc.acquire()
        try:
            d={}
            for it in lst:
                val=netDoc.getNodeInfoVal(node,it,lang=lang)
                d[it]=val
        except:
            vtLog.vtLngTB(self.GetOrigin())
        netDoc.release()
        return d
    def __getNodeByForeignId__(self,id):
        try:
            i=id.find('@')
            if i>=0:
                netDoc=self.GetNetDoc(id[i+1:])
            else:
                netDoc=self.GetMainDoc()
            if netDoc is None:
                vtLog.vtLngCurWX(vtLog.ERROR,'id:%s not found'%id,self)
                return None,None
            node=netDoc.getNodeById(id[:i])
            return netDoc,node
        except:
            vtLog.vtLngTB(self.GetOrigin())
        return None,None
    def SetInfos2Get(self,infos,foreign=None,name='basic',cltData=None,bSingleLang=True):
        if foreign is None:
            netDoc=self.GetMainDoc()
        else:
            netDoc=self.GetNetDoc(foreign)
        if netDoc is None:
            vtLog.vtLngCurWX(vtLog.ERROR,'id:%s not found'%id,self)
        netDoc.setNodeInfos2Get(infos,name=name,cltData=cltData,bSingleLang=bSingleLang)
    def GetInfos(self,id,lang=None,name='basic'):
        netDoc,node=self.__getNodeByForeignId__(id)
        if netDoc is None:
            vtLog.vtLngCurWX(vtLog.ERROR,'id:%s not found'%id,self)
            return None,None
        netDoc.acquire()
        sName,d=netDoc.getNodeInfos(node,lang=lang,name=name)
        netDoc.release()
        return sName,d
    
    def GetDocAlias(self,name):
        return self.dlgStatus.GetNetDoc(name)
    def GetDocAliasNodeInfos(self,name,id,lst,lang=None):
        netDoc=self.GetDocAlias(name)
        if netDoc is None:
            return None
        node=netDoc.getNodeById(id)
        if node is None:
            return None
        netDoc.acquire()
        try:
            d={}
            for it in lst:
                val=netDoc.getNodeInfoVal(node,it,lang=lang)
                d[it]=val
        except:
            vtLog.vtLngTB(self.GetOrigin())
        netDoc.release()
        return d
    def GetDocAliasNodeInfosById(self,name,id,lst,lang=None):
        netDoc=self.GetDocAlias(name)
        if netDoc is None:
            return None
        node=netDoc.getNodeById(id)
        if node is None:
            return None
        netDoc.acquire()
        try:
            d={}
            for it in lst:
                val=netDoc.getNodeInfoVal(node,it,lang=lang)
                d[it]=val
        except:
            vtLog.vtLngTB(self.GetOrigin())
        netDoc.release()
        return d
    def GetDocAliasNodeInfosByIdNum(self,name,id,lst,lang=None):
        netDoc=self.GetDocAlias(name)
        if netDoc is None:
            return None
        node=netDoc.getNodeByIdNum(id)
        if node is None:
            return None
        netDoc.acquire()
        try:
            d={}
            for it in lst:
                val=netDoc.getNodeInfoVal(node,it,lang=lang)
                d[it]=val
        except:
            vtLog.vtLngTB(self.GetOrigin())
        netDoc.release()
        return d
