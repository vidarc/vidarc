#Boa:FramePanel:vModifyPanel
#----------------------------------------------------------------------------
# Name:         vNetModifyThread.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vNetModifyThread.py,v 1.3 2007/08/21 18:08:59 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from wx import NewEventType as wxNewEventType
from wx import PyEvent as wxPyEvent
from wx import PostEvent as wxPostEvent
import vidarc.tool.log.vtLog as vtLog

import thread,time


VERBOSE=0

# defined event for vgpXmlTree item selected
wxEVT_NET_MODIFY_THREAD_ELEMENTS=wxNewEventType()
def EVT_NET_MODIFY_THREAD_ELEMENTS(win,func):
    win.Connect(-1,-1,wxEVT_NET_MODIFY_THREAD_ELEMENTS,func)
class wxNetThreadModifyElements(wxPyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_MODIFY_THREAD_ELEMENTS(<widget_name>, self.OnItemSel)
    """

    def __init__(self,iVal,iCount=-1):
        wxPyEvent.__init__(self)
        self.val=iVal
        self.count=iCount
        self.SetEventType(wxEVT_NET_MODIFY_THREAD_ELEMENTS)
    def GetValue(self):
        return self.val
    def GetCount(self):
        return self.count
# defined event for vgpXmlTree item selected
wxEVT_NET_MODIFY_THREAD_ELEMENTS_FINISHED=wxNewEventType()
def EVT_NET_MODIFY_THREAD_ELEMENTS_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_NET_MODIFY_THREAD_ELEMENTS_FINISHED,func)
class wxNetThreadModifyElementsFinished(wxPyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_MODIFY_THREAD_ELEMENTS_FINISHED(<widget_name>, self.OnItemSel)
    """
    def __init__(self):
        wxPyEvent.__init__(self)
        self.SetEventType(wxEVT_NET_MODIFY_THREAD_ELEMENTS_FINISHED)

# defined event for vgpXmlTree item selected
wxEVT_NET_MODIFY_THREAD_ELEMENTS_ABORTED=wxNewEventType()
def EVT_NET_MODIFY_THREAD_ELEMENTS_ABORTED(win,func):
    win.Connect(-1,-1,wxEVT_NET_MODIFY_THREAD_ELEMENTS_ABORTED,func)
class wxNetThreadModifyElementsAborted(wxPyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_MODIFY_THREAD_ELEMENTS_ABORTED(<widget_name>, self.OnItemSel)
    """
    def __init__(self):
        wxPyEvent.__init__(self)
        self.SetEventType(wxEVT_NET_MODIFY_THREAD_ELEMENTS_ABORTED)
    

class thdNetModifyThread:
    def __init__(self,par,zTimeOut=100,verbose=0):
        self.zTimeOut=zTimeOut
        self.verbose=verbose
        self.par=par
        self.lstFound=[]
        self.dApply=None
        self.running=False
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
    def SetParent(self,par=None):
        self.par=par
    def Apply(self,doc,d,update_gui=False):
        self.doc=doc
        self.dApply=d
        self.bUpdate=update_gui
        self.StartApply()
    def Clear(self):
        self.iCount=0
        self.iFound=0
        self.lstFound=[]
        self.dElem={}
        self.iAct=0
            
    def StartApply(self):
        self.keepGoing = self.running = True
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        thread.start_new_thread(self.RunApply, ())

    def Stop(self):
        self.keepGoing = False

    def IsRunning(self):
        return self.running

    def RunApply(self):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        iCount=self.dApply['iCount']
        i=0
        if self.bUpdate:
            wxPostEvent(self.par,wxNetThreadModifyElements(self.dApply['iAct'],iCount))
        node=self.__getApplyNode__()
        self.zTime=0
        self.bAbort=False
        self.doc.startEdit(node)
        while self.keepGoing:
            if self.bUpdate:
                wxPostEvent(self.par,wxNetThreadModifyElements(self.dApply['iAct'],iCount))
            self.zTime+=1
            if self.zTime>self.zTimeOut:
                self.keepGoing=False
                self.bAbort=True
            time.sleep(1)
        if self.dApply['iAct']==self.dApply['iCount']:
            self.dApply['iAct']-=1
            node=self.__getApplyNode__()
            if node is not None:
                self.doc.endEdit(node)
        self.dApply=None
        self.keepGoing = self.running = False
        if self.bUpdate:
            wxPostEvent(self.par,wxNetThreadModifyElements(0))
            if self.bAbort:
                wxPostEvent(self.par,wxNetThreadModifyElementsAborted())
            else:
                wxPostEvent(self.par,wxNetThreadModifyElementsFinished())
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'finished:%d'%self.bAbort)
        
    def __getApplyNode__(self):
        # overload this method
        return None
    def __apply__(self):
        # overload this method
        node=self.__getApplyNode__()        
        self.doc.doEdit(node)
        pass
    def __next__(self):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'iact:%4d'%self.dApply['iAct'])
        self.dApply['iAct']+=1
        self.zTime=0
        if self.dApply['iAct']<self.dApply['iCount']:
            node=self.__getApplyNode__()
            if node is not None:
                self.doc.startEdit(node)
            if self.bUpdate:
                wxPostEvent(self.par,wxNetThreadModifyElements(self.dApply['iAct']))
        else:
            self.keepGoing=False
    def OnLock(self,evt):
        evt.Skip()
        node=self.__getApplyNode__()
        if node is None:
            return
        id=self.doc.getKey(node)
        if self.doc.isSameKey(node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                self.__apply__()
            self.__next__()
    
    
