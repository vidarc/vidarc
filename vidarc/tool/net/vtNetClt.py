#----------------------------------------------------------------------------
# Name:         vNetClt.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtNetClt.py,v 1.32 2009/08/30 10:02:34 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import thread
import time
import socket,select
from Queue import Queue
import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.vtThread import vtThread
#from vidarc.tool.net import vtNetSock
#from vidarc.tool.net.vtNetSock import *
from vidarc.tool.sock.vtSock import vtSock
from vidarc.tool.vtStateFlagSimple import vtStateFlagSimple

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

# Echo client program
import os
import sys,traceback
import __init__
if __init__.SSL:
    from OpenSSL import SSL
    from OpenSSL import tsafe
    VERBOSE_CERT=0
    def verify_cb(conn, cert, errnum, depth, ok):
        #traceback.print_stack()
        # This obviously has to be updated
        #vtLog.vtLngCurCls(vtLog.INFO,'Got certificate: %s' % cert.get_subject(),self)
        #vtLog.vtLngCurCls(vtLog.ERROR,'errnum:%s;depth:%s;ok:%s'%(`errnum`,`depth`,`ok`),self)
        self=conn.get_app_data()
        #print 'Got certificate: %s' % cert.get_subject()
        #print 'errnum:%s;depth:%s;ok:%s'%(`errnum`,`depth`,`ok`)
        #if ok:
        #    self.iVerify=1
        #else:
        #    self.iVerify=-1
        self.DoVerify(conn,cert,errnum,depth,ok)
        #self.SetServed(ok)
        return ok

class vtEchoClt(vtSock):
    #def __init__(self,*args):
    #    vtSock.__init__(self,args)
    def HandleData(self):
        print self.data
        self.data=''
        self.len=0
    def ParseData(self):
        while 1:
            data=self.conn.recv(1024)
            if not data:
                self.serving=False
                return
            print data
            #print
class vtSilentEchoClt(vtSock):
    def __init__(self,server,conn,adr):
        vtSock.__init__(self,server,conn,adr)
        self.receivedCount=0
    def HandleData(self):
        self.receivedCount+=len(self.data)
        self.data=''
        self.len=0
    def ParseData(self):
        while 1:
            data=self.conn.recv(1024)
            if not data:
                self.serving=False
                return
            self.receivedCount+=len(data)
class vtNetClt(vtThread):
    #SSL=True
    SSL=__init__.SSL
    ZWAIT_SELECT=0.1
    ZWAIT_SELECT_ACTIVE=0.5
    SELECT_FD_SIZE=500
    def __init__(self,par,host,port,socketClass,verbose=0,origin=None,*args,**kwargs):
        self.host=host or 'localhost'
        self.port=port
        if origin is None:
            origin=''.join(['vtNetClt:',repr(self.host),repr(self.port)])
        vtThread.__init__(self,par,bPost=False,verbose=verbose-1,origin=origin)
        #self.par=par
        vtLog.vtLngCur(vtLog.INFO,'host:%s;port:%s'%(repr(self.host),repr(self.port)),self.GetOrigin())
        #self.origin=''.join(['vtNetClt:',repr(self.host),repr(self.port)])
        #self.SetOrigin(''.join(['vtNetClt:',repr(self.host),repr(self.port)]))
        self.serving=False
        self.connecting=False
        #self.stopping=False
        self.connected=False
        #self.pausing=False
        self.paused=False
        self._iConnectingCount=0
        self._iConnectedCount=0
        #self.dataSettled=False
        self.oSF=vtStateFlagSimple(
                    {
                    0x00:{'name':'CLOSED','is':'IsCLosed','init':True,
                        'enter':{#'func':(self.SocketClosed,(),{}),
                                 'clr':['ACTIVE''PAUSING','PAUSED','SERVING','STARTING']}},
                    0x01:{'name':'CONNECTING','is':'IsConnecting'},
                    0x02:{'name':'CONNECTION','is':'IsConnectionOk'},
                    0x04:{'name':'CONNECTED','is':'IsConnected',
                        #'enter':{#'func':(self.NotifyConnected,(),{}),
                        #         'set':['ACTIVE','SERVING'],
                        #         'clr':['PAUSING','PAUSED']}
                                     },
                    0x10:{'name':'CONNECTION_OK','is':'IsConnectionOk',
                        #'enter':{'func':(self.NotifyConnectionOk,(),{})}
                            },
                    0x11:{'name':'CONNECTION_FLT','is':'IsConnectionFlt',
                        #'enter':{'func':(self.NotifyConnectionFault,(),{})}
                            },
                    0xff:{'name':'ABORTED','is':'IsAbortd','init':True,
                        'enter':{#'func':(self.SocketClosed,(),{}),
                                 'clr':['ACTIVE''PAUSING','PAUSED','SERVING','STARTING']}},
                    
                    },
                    {
                    0x0001:{'name':'ACTIVE','is':'IsActive','order':0},
                    0x0002:{'name':'PAUSING','is':'IsPausing','order':1},
                    0x0004:{'name':'PAUSED','is':'IsPaused','order':2},
                    0x0008:{'name':'SERVING','is':'IsServing','order':3},
                    0x0020:{'name':'STARTING','is':'IsStarting','order':4},
                    0x0010:{'name':'NOTITY_CONNECT_IMMEDIATLY','is':'IsNotifyConnectImmediatly'},
                    0x0040:{'name':'STOPPING','is':'IsStopping','order':5},
                    
                    })
        self.sock=None
        self.socketClass=socketClass
        self.socketArgs=args
        self.socketKwArgs=kwargs
        
        self.connections={}
        self.lRd=[]
        self.dConn={}
        self.lWr=[]
        self.qConn=Queue()
        
        self.verbose=verbose
        try:
            if self.SSL:
                self.iVerify=0
                #self.ctx=self.conn.get_context()
                #self.conn.set_app_data(self)
        except:
            traceback.print_exc()
            vtLog.vtLngTB(self.GetOrigin())
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        try:
            del self.sock
        except:
            #vtLog.vtLngTB('del')
            pass
    def __initSSL__(self,privkey=None,cert=None,verify=None,DN=None):
        vtLog.vtLngCur(vtLog.CRITICAL,''%(),self.GetOrigin())
        try:
            #traceback.print_stack()
            if self.SSL:
                if DN is None:
                    DN=os.getcwd()
                if privkey is None:
                    privkey='vMESClt.pkey'
                if cert is None:
                    cert='vMESClt.cert'
                if verify is None:
                    verify='CA.cert'
                try:
                    DNcert=os.path.split(sys.argv[0])[0]
                except:
                    DNcert=DN
                if VERBOSE_CERT:
                    sys.stderr.write('\n%s'%(''.join(traceback.format_stack())))
                    sys.stderr.write('\nappl:%s\ndnCert:%s\ndn:%s %d\n'%(sys.argv[0],DNcert,os.path.join(DN, privkey),os.path.exists(os.path.join(DN, privkey))))
                    sys.stderr.write('       nfn:%s %d\n'%(os.path.join(DN, privkey),os.path.exists(os.path.join(DN, privkey))))
                    sys.stderr.write('       nfn:%s %d\n'%(os.path.join(DN, cert),os.path.exists(os.path.join(DN, cert))))
                    sys.stderr.write('       nfn:%s %d\n'%(os.path.join(DNcert, verify),os.path.exists(os.path.join(DNcert, verify))))
                
                self.ctx = SSL.Context(SSL.SSLv23_METHOD)
                self.ctx.set_options(SSL.OP_NO_SSLv2)
                self.ctx.set_verify(SSL.VERIFY_PEER|SSL.VERIFY_FAIL_IF_NO_PEER_CERT, verify_cb) # Demand a certificate
                self.ctx.use_privatekey_file (os.path.join(DN, privkey))
                self.ctx.use_certificate_file(os.path.join(DN, cert))
                self.ctx.load_verify_locations(os.path.join(DNcert, verify))
            else:
                self.ctx=None
        except:
            traceback.print_exc()
            vtLog.vtLngTB(self.GetOrigin())
            self.ctx=None
    def DoVerify(self,conn, cert, errnum, depth, ok):
        #sys.stderr.write('\n%s'%(''.join(traceback.format_stack())))
        if ok:
            self.SetServing(True)
        else:
            self.SetServing(False)
    def SetHostPort(self,host,port):
        self.host=host or 'localhost'
        self.port=port
    def GetSocketInstance(self):
        return self.sock
    def Start(self):
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
        try:
            if self.IsServing():
                vtLog.vtLngCur(vtLog.WARN,'already served',self.GetOrigin())
                return
            if self.oSF.IsStarting:
                vtLog.vtLngCur(vtLog.WARN,'already starting',self.GetOrigin())
                return
            if self.oSF.IsStopping:
                vtLog.vtLngCur(vtLog.WARN,'stopping active',self.GetOrigin())
                return
            vtThread.Start(self)
            self.oSF.SetFlag(self.oSF.STARTING,True)
            #self.SetServing(True)
            #self.stopping=False
            self.Do(self.Run)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        #thread.start_new_thread(self.Run, ())
    def Stop(self):
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
        vtThread.Stop(self)
        self.oSF.SetFlag(self.oSF.STOPPING,True)
        #print 'netclt','stop'
        #print '   conn:%d stop:%d serv:%d'%(self.connecting,self.stopping,self.serving)
        #print type(self.sock),self.sock
        #self.stopping=True
        while self.connecting:
            time.sleep(1)
        if self.sock is not None:
            self.sock.Stop()
        return
        self._acquire()
        try:
            l=self.dConn.values()
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self._release()
        for sock in l:
            try:
                sock.SocketClose()
                #sock.Stop()
            except:
                vtLog.vtLngTB(self.GetOrigin())
        #self.serving=False
    def Pause(self,flag):
        try:
            self.oSF.SetFlag(self.oSF.PAUSING,flag)
            if self.sock is not None:
                self.sock.Pause(flag)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        for sock in self.dConn.itervalues():
            try:
                sock.Pause(Flag)
            except:
                vtLog.vtLngTB(self.GetOrigin())
        #self._acquire()
        #self.pausing=flag
        #self._release()
        #if self.sock is not None:
        #    self.sock.Pause(flag)
    def IsConnecting(self):
        self._acquire()
        bRet=self._iConnectingCount>0
        self._release()
        return bRet
        
        if self.sock is not None:
            if self.sock.IsConnecting():
                return True
        for sock in self.dConn.itervalues():
            try:
                if sock.IsConnecting():
                    return True
            except:
                vtLog.vtLngTB(self.GetOrigin())
        return self.oSF.IsConnecting
    def SetConnecting(self,flag):
        vtLog.vtLngCur(vtLog.CRITICAL,''%(),self.GetOrigin())
        #self._acquire()
        #self.connecting=flag
        #self._release()
    def IsConnected(self):
        if self.IsConnecting():
            return False
        self._acquire()
        try:
            if self.sock is not None:
                if self.sock.IsConnected():
                    return True
            for sock in self.dConn.itervalues():
                if sock.IsConnected():
                    return True
        finally:
            self._release()
        return False
    def SetConnected(self,flag):
        vtLog.vtLngCur(vtLog.CRITICAL,''%(),self.GetOrigin())
        #self._acquire()
        #self.connected=flag
        #self._release()
    def SetServing(self,flag):
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
        self.oSF.SetFlag(self.oSF.SERVING,flag)
    def IsServing(self):
        return self.oSF.IsServing
        if self.IsConnecting():
            return True
        #bRet=False
        self._acquire()
        try:
            if self.sock is not None:
                if self.sock.IsRunning():
                    if self.sock.IsPaused():
                        #return False
                        pass
                    else:
                        #bRet=True
                        return True
                #if self.sock.IsThreadRunning():
                #    return True
            #else:
            #    return False
            for sock in self.dConn.itervalues():
                if sock.IsRunning():
                    if sock.IsPaused():
                        #return False
                        pass
                    else:
                        return True
            #bRet=self.serving
        finally:
            self._release()
        return self.oSF.IsServing
        return bRet
    def __removeSock__(self,cli):
        self._acquire()
        try:
            vtLog.vtLngCur(vtLog.INFO,'remove:%d'%(cli),self.GetOrigin())
            for lRd in self.lRd:
                if cli in lRd:
                    vtLog.vtLngCur(vtLog.INFO,'remove from lRd'%(),self.GetOrigin())
                    lRd.remove(cli)
                    break
            self.lRd=[lRd for lRd in self.lRd if len(lRd)>0]
            # compress lists
            iL=len(self.lRd)
            for i in xrange(iL-1,0,-1):
                iL1=len(self.lRd[i])
                iL2=len(self.lRd[i-1])
                if (iL1+iL2)<self.SELECT_FD_SIZE:
                    self.lRd[i-1]=self.lRd[i-1]+self.lRd[i]
                    del self.lRd[i]
        except:
            vtLog.vtLngTB(self.GetOrigin())
        try:
            if cli in self.dConn:
                del self.dConn[cli]
        except:
            vtLog.vtLngTB(self.GetOrigin())
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'lRd:%s;dConn:%s'%(vtLog.pformat(self.lRd),vtLog.pformat(self.dConn)),self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self._release()
        self.__calcConnected__()
    def RemoveSock(self,sock,cli):
        vtLog.vtLngCur(vtLog.INFO,'close connection'%(),self.GetOrigin())
        self._acquire()
        #self.__removeSock__(cli)
        self.qConn.put((self.__removeSock__,(cli,),{}))
        try:
            sock.SocketClose()
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self._release()
    def __calcConnected__(self):
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
        self._acquire()
        try:
            self._iConnectedCount=0
            if len(self.lRd)>0:
                self._iConnectedCount=reduce(lambda x,y:x+y,[len(lRd) for lRd in self.lRd])
            if VERBOSE>10:
                vtLog.vtLngCur(vtLog.INFO,'lRd;%s;dConn;%s'%(vtLog.pformat(self.lRd),vtLog.pformat(self.dConn)),self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
            self._iConnectedCount=0
        try:
            if VERBOSE>5:
                vtLog.vtLngCur(vtLog.INFO,'connected:%d'%(self._iConnectedCount),self.GetOrigin())
            if self.sock is not None:
                self._iConnectedCount+=1
            if VERBOSE>5:
                vtLog.vtLngCur(vtLog.INFO,'connected:%d'%(self._iConnectedCount),self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.oSF.SetFlag(self.oSF.ACTIVE,self._iConnectedCount!=0)
        self._release()
        if self.IsRunning()==False:
            self.oSF.SetFlag(self.oSF.SERVING,self._iConnectedCount!=0)
    def StopSocket(self,adr,sockInst):
        try:
            #vtLog.vtLngCallStack(self,#vtLog.DEBUG,'StopSocket by %s'%str(adr))
            #vtLog.vtLngCur(vtLog.CRITICAL,''%(),self.GetOrigin())
            #sockInst=self.connections[adr]
            #cli=sockInst.GetSock()
            cli=sockInst.GetSockFD()
            if self.IsRunning():
                self.qConn.put((self.__removeSock__,(cli,),{}))
            else:
                self.__removeSock__(cli)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        try:
            lst=self.connections[adr]
            lst.remove(sockInst)
        except:
            pass
        self._acquire()
        try:
            if self.sock==sockInst:
                self.sock=None
                self.socket=None
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self._release()
        self.__calcConnected__()
        ##self.SetPostWid(None)
        #self.socketClass=None
        #self.socketArgs=None
        #self.socketKwArgs=None
        #vtLog.CallStack('')
        #print self.__dict__
        #print vtLog.pformat(self.__dict__)
    def Connect(self):
        try:
            vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
            #self.dataSettled=False
            self._acquire()
            self._iConnectingCount+=1
            self._release()
            self.oSF.SetFlag(self.oSF.SERVING,True)
            self.sock=self.socketClass(self,None,None,verbose=self.verbose,
                        *self.socketArgs,**self.socketKwArgs)
            self.sock.SetParent(self.par)
            self.sock.Connect(self.host, self.port)
            vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
            #self.Do(self.RunConnect)
            #thread.start_new_thread(self.RunConnect, ())
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def Connect4Select(self,host,port,socketClass,funcNotified,*args,**kwargs):
        try:
            vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
            #self.dataSettled=False
            self._acquire()
            self._iConnectingCount+=1
            self._release()
            sock=socketClass(self,None,None,*args,**kwargs)
            #if 'sockpar' in kwargs:
            #    sock.SetParent(kwargs['sockpar'])
            #else:
            #    sock.SetParent(self.par)
            sock.SetParent(self.par)
            sock.Connect(host, port,funcNotified=funcNotified,bUseSelect=True)
            vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
            #self.Do(self.RunConnect)
            #thread.start_new_thread(self.RunConnect, ())
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def GetConnectingCount(self):
        try:
            self._acquire()
            return self._iConnectingCount
        finally:
            self._release()
    def __addSock__(self,sock):
        try:
            cli=sock.GetSockFD()
            vtLog.vtLngCur(vtLog.INFO,'fd:%d'%(cli),self.GetOrigin())
            l=None
            for lRd in self.lRd:
                if len(lRd)<self.SELECT_FD_SIZE:
                    l=lRd
                    break
            if l is None:
                l=[cli]
                self.lRd.append(l)
            else:
                l.append(cli)
            self.dConn[cli]=sock
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'lRd:%s;dConn:%s'%(vtLog.pformat(self.lRd),vtLog.pformat(self.dConn)),self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.__calcConnected__()
    def NotifyConnected(self,sock):
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
        self._acquire()
        try:
            self._iConnectingCount-=1
            self._iConnectedCount+=1
            self.oSF.SetFlag(self.oSF.ACTIVE,self._iConnectedCount!=0)
            if self._iConnectingCount<0:
                vtLog.vtLngCur(vtLog.CRITICAL,'connecting count undeeflow;%d'%(self._iConnectingCount),self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self._release()
        if self.IsRunning():
            self.qConn.put((self.__addSock__,(sock,),{}))
        else:
            self.__addSock__(sock)
        #self.SetConnecting(False)
        #self.SetConnected(True)
        if 'bNotifyConnectImmediatly' in self.socketKwArgs and 0:
            if self.socketKwArgs['bNotifyConnectImmediatly']:
                if self.par is not None:
                    self.par.NotifyConnected()
    def NotifyConnectionFault(self):
        self._acquire()
        try:
            self._iConnectingCount-=1
            if self._iConnectingCount<0:
                vtLog.vtLngCur(vtLog.CRITICAL,'connecting count underflow;%d'%(self._iConnectingCount),self.GetOrigin())
        except:
            pass
        self._release()
    def RunConnect(self):
        vtLog.vtLngCur(vtLog.CRITICAL,''%(),self.GetOrigin())
        vtLog.vtLngCur(vtLog.INFO,'host:%s;port:%s'%(repr(self.host),repr(self.port)),self.GetOrigin())
        vtLog.vtLngCS(vtLog.DEBUG,'Connect to host %s at port %s'%(
                self.host,self.port),self.GetOrigin())
        self.SetConnected(False)
        self.socket = None
        try:
            for res in socket.getaddrinfo(self.host, self.port, socket.AF_UNSPEC, socket.SOCK_STREAM):
                af, socktype, proto, canonname, sa = res
                try:
                    if self.SSL:
                        self.__initSSL__()
                        self.socket = tsafe.Connection(self.ctx,socket.socket(af, socktype, proto))
                        #self.socket.set_app_data(self)
                    else:
                        self.socket = socket.socket(af, socktype, proto)
                except socket.error, msg:
                    #print msg
                    #traceback.print_exc()
                    self.socket = None
                    continue
                try:
                    self.socket.connect(sa)
                    #if self.SSL:
                    #    self.socket.do_handshake()
                    #    self.socket=socket.ssl(self.socket,None,None)
                    #    print self.socket
                except socket.error, msg:
                    #traceback.print_exc()
                    self.socket.close()
                    self.socket = None
                    continue
                break
        except:
            # address fault
            vtLog.vtLngTB(self.GetOrigin())
            pass
        if self.socket is None:
            #vtLog.vtLngCallStack(self,#vtLog.DEBUG,'could not open socket')
            try:
                self.par.NotifyConnectionFault()
            except:
                pass
            self.SetConnecting(False)
            #self.dataSettled=True
            #self.serving=False
            return
        try:
            self.sock=self.socketClass(self,self.socket,None,verbose=self.verbose,
                    *self.socketArgs,**self.socketKwArgs)
            #if self.par.bSynch:
            #    self.dataSettled=True
            #while self.sock.iVerify==0:
            #    time.sleep(0.5)
            #    break
            #print self.socket.state_string()
            #if self.SSL:
                #self.socket.do_handshake()
                #while self.sock.IsRunning()==False:
                #    time.sleep(0.2)
                #time.sleep(8)
                #print self.socket.state_string()
            
            if 'bNotifyConnectImmediatly' in self.socketKwArgs:
                if self.socketKwArgs['bNotifyConnectImmediatly']:
                    if self.par is not None:
                        self.par.NotifyConnected()
            #self.sock.Start()
        except:
            vtLog.vtLngTB(self.GetOrigin())
            traceback.print_exc()
            
        self.SetConnected(True)
        self.SetConnecting(False)
        self.oSF.SetFlag(self.oSF.STOPPING,False)
        #return 0
    def Run(self):
        self.oSF.SetFlag(self.oSF.STARTING,False)
        self.oSF.SetFlag(self.oSF.SERVING,True)
        lEvt=[]
        lWr=[]
        while self.oSF.IsServing:
            while self.qConn.empty()==False:
                try:
                    f,args,kwargs=self.qConn.get()
                    vtLog.vtLngCur(vtLog.INFO,'f:%s;args:%s;kwargs:%s'%(repr(f),
                            repr(args),repr(kwargs)),self.GetOrigin())
                    f(*args,**kwargs)
                except:
                    vtLog.vtLngTB(self.GetOrigin())
            if self.Is2Stop():
                self.oSF.SetFlag(self.oSF.SERVING,False)
                break
            try:
                bDbg=False
                if VERBOSE>10:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'%s;;lRd:%s;lWr;%s;dConn:%s'%(self.oSF.GetStrFull(';'),
                                vtLog.pformat(self.lRd),vtLog.pformat(lWr),
                                vtLog.pformat(self.dConn)),self.GetOrigin())
                        bDbg=True
            except:
                vtLog.vtLngTB(self.GetOrigin())
            if self.oSF.IsActive==False:
                time.sleep(self.ZWAIT_SELECT_ACTIVE)
                continue
            try:
                for lRd in self.lRd:
                    try:
                        #print self.lRd, self.lWr , lEvt
                        #if len(self.lRd)>0:
                        #print self.oSF.IsActive
                        #lRdRdy,lWrRdy , lEvtRdy = select.select(lRd, lWr , lEvt, self.ZWAIT_SELECT)
                        #lRdRdy,lWrRdy , lEvtRdy = select.select(lRd, lRd , lEvt, self.ZWAIT_SELECT)
                        lRdRdy,lWrRdy , lEvtRdy = select.select(lRd, [] , [], self.ZWAIT_SELECT)
                    except socket.timeout,msg:
                        #print 'timeout'
                        #self.CheckPause()
                        continue
                    except:
                        vtLog.vtLngTB(self.GetOrigin())
                        continue
                        #break
                    if bDbg:
                        vtLog.vtLngCur(vtLog.DEBUG,'rd:%d;%s'%(len(lRdRdy),vtLog.pformat(lRdRdy)),self.GetOrigin())
                    for cli in lRdRdy:
                        #self._acquire()
                        #try:
                        if cli in self.dConn:
                            sock=self.dConn[cli]
                            if bDbg:
                                vtLog.vtLngCur(vtLog.DEBUG,'cli:%d;%s'%(cli,sock.__str__()),self.GetOrigin())
                            if sock.doRecv()<0:
                                vtLog.vtLngCur(vtLog.INFO,'close connection'%(),self.GetOrigin())
                                try:
                                    vtLog.vtLngCur(vtLog.INFO,'fd:%d;doRecv returned fault'%(cli),self.GetOrigin())
                                    vtLog.vtLngCur(vtLog.INFO,'fd:%d;lRd:%s;dConn:%s'%(cli,
                                            vtLog.pformat(lRd),
                                            vtLog.pformat(self.dConn)),self.GetOrigin())
                                except:
                                    vtLog.vtLngTB(self.GetOrigin())
                                #self._acquire()
                                #try:
                                #    self.lRd.remove(cli)
                                #except:
                                #    vtLog.vtLngTB(self.GetOrigin())
                                #try:
                                #    del self.dConn[cli]
                                #except:
                                #    vtLog.vtLngTB(self.GetOrigin())
                                try:
                                    sock.SocketClose()
                                except:
                                    vtLog.vtLngTB(self.GetOrigin())
                                #self._release()
                        #except:
                        #    vtLog.vtLngTB(self.GetOrigin())
                        else:
                            vtLog.vtLngCur(vtLog.ERROR,'fd:%d;lRd:%s;dConn:%s'%(cli,
                                        vtLog.pformat(lRd),
                                        vtLog.pformat(self.dConn)),self.GetOrigin())
                            
                        #self._release()
            except:
                vtLog.vtLngTB(self.GetOrigin())
            try:
                lWr=[]
                iCount=0
                def selectWr(lWr):
                    lRdRdy,lWrRdy , lEvtRdy = select.select([], lWr , [], 0)#self.ZWAIT_SELECT)
                    if VERBOSE>10:
                        vtLog.vtLngCur(vtLog.DEBUG,'wr:%d;%s'%(len(lWrRdy),vtLog.pformat(lWrRdy)),self.GetOrigin())
                    for cli in lWrRdy:
                        if cli in self.dConn:
                            sock=self.dConn[cli]
                            #print 'recv',cli,sock,sock.IsServing()
                            if sock.doSend()<0:
                                #self.RemoveSock(sock,cli)
                                pass
                        else:
                            vtLog.vtLngCur(vtLog.ERROR,'cli:%d;lRd:%s;dConn:%s'%(cli,
                                            vtLog.pformat(lRd),
                                            vtLog.pformat(self.dConn)),self.GetOrigin())
                for fdCli,sock in self.dConn.iteritems():
                    if sock.Is2Snd():
                        lWr.append(fdCli)
                        iCount+=1
                    if iCount>32:
                        selectWr(lWr)
                        iCount=0
                        lWr=[]
                if iCount>0:
                    selectWr(lWr)
            except:
                vtLog.vtLngTB(self.GetOrigin())
            try:
                for fdCli,sock in self.dConn.iteritems():
                    if sock.Is2Stop():
                        sock.SocketClose()
                        #sock.Close()
            except:
                vtLog.vtLngTB(self.GetOrigin())
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
        self._acquire()
        try:
            l=self.dConn.values()
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self._release()
        try:
            for sock in l:
                try:
                    #cli.shutdown()
                    sock.SocketClose()
                except:
                    vtLog.vtLngTB(self.GetOrigin())
            #self.lRd=[]
            #self.dConn={}
        except:
            vtLog.vtLngTB(self.GetOrigin())
        zTm=0
        while zTm<60:
            if self.oSF.IsActive==False:
                break
            zTm+=0.1
            time.sleep(0.25)
            while self.qConn.empty()==False:
                try:
                    f,args,kwargs=self.qConn.get()
                    vtLog.vtLngCur(vtLog.INFO,'f:%s;args:%s;kwargs:%s'%(repr(f),
                            repr(args),repr(kwargs)),self.GetOrigin())
                    f(*args,**kwargs)
                except:
                    vtLog.vtLngTB(self.GetOrigin())
            self.__calcConnected__()
        vtLog.vtLngCur(vtLog.INFO,'zTm:%d'%(zTm),self.GetOrigin())
        self.oSF.SetFlag(self.oSF.STOPPING,False)
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
    def Send(self,str):
        #if self.IsConnected():
        #    self.sock.Send(str)
        self.sock.Send(str)
    def Close(self):
        #self.par.NotifyDisConnected()
        #vtLog.CallStack('')
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
        #self.socketArgs=()          # 080313:wro do not delete 070805:wro possible to FIX
        #self.socketKwArgs={}        # 080313:wro do not delete 070805:wro possible to FIX
        if self.sock is not None:
            self.sock.Close()
        #self.oSF.SetFlag(self.oSF.SERVING,False)
        
def testEcho():
    zStart=time.clock()
    clt1=vtNetClt(None,'',50007,vtEchoClt,1)
    clt1.Connect()
    for i in range(10):
        print i
        clt1.Send(u'Hello, world')
    time.sleep(5)
    time.sleep(1)
    lstClt=[]
    for i in range(10):
        clt=vtNetClt(None,'',50007,vtSilentEchoClt,1)
        clt.Connect()
        lstClt.append(clt)
    data=''
    for i in range(1000):
        for j in range(80):
            data+=str((i+j)%10)
        data+=os.linesep
    for i in range(100):
        sCount=''
        for clt in lstClt:
            sCount+='%05d '%(clt.sock.receivedCount/1024)
        print 'send pulk %3d %s [kB]'%(i,sCount)
        for clt in lstClt:
            clt.Send(data)
        #time.sleep(0.001)
    print 'time consumed',time.clock()-zStart
    print 'wait to close'
    for i in range(10):
        sCount=''
        for clt in lstClt:
            sCount+='%05d '%(clt.sock.receivedCount/1024)
        print sCount,'[kB]'
        time.sleep(1)
    #print 'received 0'
    #lstClt[0].GetSocketInstance().Show(10)
    clt1.Close()
    
    for clt in lstClt:
        clt.Close()
    print 'time consumed',time.clock()-zStart
if __name__=='__main__':
    testEcho()
    
