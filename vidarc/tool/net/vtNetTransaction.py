#----------------------------------------------------------------------------
# Name:         vtNetTransaction.py
# Purpose:
# Author:       Walter Obweger
#
# Created:      20061119
# CVS-ID:       $Id: vtNetTransaction.py,v 1.3 2007/04/18 15:56:34 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.time.vtTime import vtDateTime
from vidarc.tool.vtSlotKeyBuffer import vtSlotKeyBuffer

class vtNetTransactionSwitchException(Exception):
    def __init__(self,k,pos,cmd,state):
        self.key=k
        self.pos=pos
        self.cmd=cmd
        self.state=state
    def __str__(self):
        return 'cmd:%s;state:%s;key:%s;pos:%d'%(self.cmd,self.state,self.key,self.pos)

class vtNetTransaction(vtSlotKeyBuffer):
    CMD_UNDEF               = 0x000000
    CMD_ADD                 = 0x010000
    CMD_SET                 = 0x020000
    CMD_MASK                = 0xff0000
    STATE_INIT              = 0x000001
    STATE_LOCKED            = 0x000010
    STATE_LOCKED_ALREADY    = 0x000020
    STATE_FIN               = 0x004000
    STATE_ABORT             = 0x008000
    STATE_ABORT_ACL         = 0x00A000
    STATE_ABORT_LOCKED      = 0x00B000
    STATE_ABORT_LOCK        = 0x00C000
    STATE_MASK              = 0x00ffff
    STATE_MASK_STAGE_0      = 0x00000f
    STATE_MASK_STAGE_1      = 0x0000f0
    STATE_MASK_STAGE_2      = 0x000f00
    STATE_MASK_STAGE_3      = 0x00f000
    MAP_STR_2_CMD     = {'add':CMD_ADD,'set':CMD_SET}
    STATE_MACHINE={
        CMD_ADD:{
            STATE_INIT:{
                        'ok':               STATE_FIN,
                        'fault':            STATE_ABORT,
                        'acl':              STATE_ABORT_ACL,
                        'locked':           STATE_ABORT_LOCKED,
                        },
            },
        CMD_SET:{
            STATE_INIT:{
                        'ok':               STATE_LOCKED,
                        'already locked':   STATE_LOCKED_ALREADY,
                        'rejected':         STATE_ABORT_LOCK,
                        'fault':            STATE_ABORT,
                        'lock block':       STATE_INIT,
                        },
            STATE_LOCKED:{
                        'ok':               STATE_FIN,
                        'fault':            STATE_ABORT,
                        'acl':              STATE_ABORT_ACL,
                        'locked':           STATE_ABORT_LOCKED,
                        },
            STATE_LOCKED_ALREADY:{
                        'ok':               STATE_FIN,
                        'fault':            STATE_ABORT,
                        'acl':              STATE_ABORT_ACL,
                        'locked':           STATE_ABORT_LOCKED,
                        },
            },
        }
    def __init__(self,size):
        vtSlotKeyBuffer.__init__(self,size)
        self.zNow=vtDateTime()
        self.zPrev=vtDateTime()
        #self.zDiff=vtDateTime()
        self.lCmd=[self.CMD_UNDEF]*self.iSize
        self.lTransID=[-1L]*self.iSize
        self.lTime=[None]*self.iSize
        #self.lTimeSort=[None]*self.iSize
        self.iTransIDAct=0L
        self.MAP_CMD_2_STR={}
        for k,v in self.MAP_STR_2_CMD.iteritems():
            self.MAP_CMD_2_STR[v]=k
    def put(self,k,t,cmd='',blocking=False):
        if self.find(k)>=0:
            return -2
        if self.semSpace.acquire(blocking)==False:
            return -1
        self.semAccess.acquire()
        if self.iFill<self.iSize:
            iAct=0
            while self.lSlot[iAct] is not None:
                iAct+=1
                if iAct>=self.iSize:
                    self.semAccess.release()
                    return -1
            self.lSlot[iAct]=t
            self.lKey[iAct]=k
            if cmd in self.MAP_STR_2_CMD:
                self.lCmd[iAct]=self.MAP_STR_2_CMD[cmd]|self.STATE_INIT
            else:
                self.lCmd[iAct]=self.CMD_UNDEF
            self.lTransID[iAct]=self.iTransIDAct
            self.zNow.Now()
            self.lTime[iAct]=self.zNow.GetDateTimeGMStr()
            #self.lTimeSort[-1]=(self.zNow.GetDateTimeGMStr(),iAct)
            #self.lTimeSort.sort()
            #print self.lTimeSort
            self.iFill+=1
            self.iTransIDAct+=1L
            self.semAccess.release()
            return iAct
        else:
            self.semAccess.release()
            return -1
    def pop(self,iPos):
        self.semAccess.acquire()
        if self.iFill>0:
            t=self.lSlot[iPos]
            self.lSlot[iPos]=None
            self.lKey[iPos]=None
            self.lCmd[iPos]=self.CMD_UNDEF
            self.lTransID[iPos]=-1L
            self.lTime[iPos]=None
            self.iFill-=1
            self.semAccess.release()
            self.semSpace.release()
            return t
        else:
            self.semAccess.release()
            return None
    def getTransIDAct(self):
        self.semAccess.acquire()
        tID=self.iTransIDAct
        self.semAccess.release()
        return tID
    def getTransID(self,iAct):
        self.semAccess.acquire()
        try:
            tID=self.lTransID[iAct]
        except:
            tID=-1L
        self.semAccess.release()
        return tID
    def getTime(self,iAct):
        self.semAccess.acquire()
        try:
            zDT=self.lTime[iAct]
        except:
            zDT=None
        self.semAccess.release()
        return zDT
    def next(self,k,response):
        self.semAccess.acquire()
        iAct=self.__find__(k)
        sCmd=''
        sState=''
        iCmdState=self.STATE_ABORT
        try:
            iCmdState=self.lCmd[iAct]
            iCmd=iCmdState & self.CMD_MASK
            iState=iCmdState & self.STATE_MASK_STAGE_2
            if iState==0:
                iState=iCmdState & self.STATE_MASK_STAGE_1
                if iState==0:
                    iState=iCmdState&self.STATE_MASK_STAGE_0
            iStateNext=self.STATE_ABORT
            if iCmd in self.STATE_MACHINE:
                d=self.STATE_MACHINE[iCmd]
                if iState in d:
                    dd=d[iState]
                    if response in dd:
                        iStateNext=dd[response]
                        self.zNow.Now()
                        self.lTime[iAct]=self.zNow.GetDateTimeGMStr()
            iCmdState|=iStateNext
            self.lCmd[iAct]=iCmdState
        except:
            e=vtNetTransactionSwitchException(k,iAct,sCmd,sState)
            self.semAccess.release()
            raise e
        self.semAccess.release()
        return (iCmdState & self.STATE_MASK_STAGE_3)==0
        
    def findTimeOut(self,fSecTimeOut):
        self.semAccess.acquire()
        if self.iFill>0:
            self.zNow.Now()
            iFirst=-1
            fFirst=-1
            for iAct in xrange(self.iSize):
                if self.lTime[iAct] is not None:
                    self.zPrev.SetStr(self.lTime[iAct])
                    fSec=self.zPrev.CalcDiffSecFloat(self.zNow)
                    if fSec>fSecTimeOut:
                        #self.semAccess.release()
                        #return iAct
                        if fFirst<fSec:
                            iFirst=iAct
                            fFirst=fSec
            self.semAccess.release()
            return iFirst
        else:
            self.semAccess.release()
            return -1
    def clearTimeOut(self,fSecTimeOut):
        self.semAccess.acquire()
        if self.iFill>0:
            self.zNow.Now()
            iFirst=-1
            fFirst=-1
            l=None
            for iAct in xrange(self.iSize):
                if self.lTime[iAct] is not None:
                    self.zPrev.SetStr(self.lTime[iAct])
                    fSec=self.zPrev.CalcDiffSecFloat(self.zNow)
                    if fSec>fSecTimeOut:
                        #self.semAccess.release()
                        #return iAct
                        if l is None:
                            l=[]
                        l.append((self.lTime[iAct],{
                            'key':self.lKey[iAct],
                            'time':self.lTime[iAct],
                            'info':self.lSlot[iAct],
                            'transID':self.lTransID[iAct],
                            'cmd':self.lCmd[iAct] & self.CMD_MASK,
                            'state':self.lCmd[iAct] & self.STATE_MASK,
                            }))
                        self.lSlot[iAct]=None
                        self.lKey[iAct]=None
                        self.lCmd[iAct]=self.CMD_UNDEF
                        self.lTransID[iAct]=-1L
                        self.lTime[iAct]=None
                        self.iFill-=1
                        self.semSpace.release()
            if l is not None:
                l.sort()
                l=[v for z,v in l]
            self.semAccess.release()
            return l
        else:
            self.semAccess.release()
            return None
    def __str__(self):
        return 'fill:%d,size:%d'%(self.iFill,self.iSize)
