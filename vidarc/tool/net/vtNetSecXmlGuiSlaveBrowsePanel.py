#Boa:FramePanel:vtNetSecXmlGuiSlaveBrowsePanel
#----------------------------------------------------------------------------
# Name:         vtNetSecXmlGuiSlaveBrowsePanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060705
# CVS-ID:       $Id: vtNetSecXmlGuiSlaveBrowsePanel.py,v 1.6 2007/10/29 21:17:27 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.filebrowsebutton
import wx.lib.buttons

import sys,os.path

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.config.vcCust as vcCust

import vidarc.tool.net.img_gui as img_gui


[wxID_VTNETSECXMLGUISLAVEBROWSEPANEL, 
 wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBADD, 
 wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBAPPLY, 
 wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBCANCEL, 
 wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBCONNECT, 
 wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBDEL, 
 wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBDOWN, 
 wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBOPEN, 
 wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBSORT, 
 wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBSTOP, 
 wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBUP, 
 wxID_VTNETSECXMLGUISLAVEBROWSEPANELDBBDN, 
 wxID_VTNETSECXMLGUISLAVEBROWSEPANELLBLAPPL, 
 wxID_VTNETSECXMLGUISLAVEBROWSEPANELLSTRECENT, 
 wxID_VTNETSECXMLGUISLAVEBROWSEPANELTXTAPPL, 
] = [wx.NewId() for _init_ctrls in range(15)]

class vtNetSecXmlGuiSlaveBrowsePanel(wx.Panel):
    VERBOSE=0
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbDel, 0, border=4, flag=wx.BOTTOM)
        parent.AddWindow(self.cbUp, 0, border=4, flag=wx.TOP)
        parent.AddWindow(self.cbDown, 0, border=4, flag=wx.BOTTOM | wx.TOP)
        parent.AddWindow(self.cbSort, 0, border=4, flag=wx.TOP)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.dbbDN, 0, border=4, flag=wx.BOTTOM | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsAppl, 1, border=4,
              flag=wx.BOTTOM | wx.EXPAND | wx.TOP | wx.LEFT)
        parent.AddWindow(self.cbAdd, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.lstRecent, 0, border=4,
              flag=wx.BOTTOM | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsBt, 0, border=4, flag=wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.btBtCmd, 0, border=4,
              flag=wx.ALIGN_CENTER | wx.BOTTOM | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.cbStop, 0, border=4, flag=wx.TOP | wx.LEFT)

    def _init_coll_bxsAppl_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblAppl, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtAppl, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_btBtCmd_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbOpen, 0, border=4, flag=wx.RIGHT)
        parent.AddWindow(self.cbConnect, 0, border=4, flag=wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbApply, 0, border=4, flag=wx.RIGHT)
        parent.AddWindow(self.cbCancel, 0, border=4, flag=wx.LEFT)

    def _init_coll_lstRecent_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'Alias'), width=80)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'Host'), width=80)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'Port'), width=60)
        parent.InsertColumn(col=3, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'User'), width=60)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=2, hgap=0, rows=4, vgap=0)

        self.bxsAppl = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.btBtCmd = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsAppl_Items(self.bxsAppl)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_btBtCmd_Items(self.btBtCmd)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTNETSECXMLGUISLAVEBROWSEPANEL,
              name=u'vtNetSecXmlGuiSlaveBrowsePanel', parent=prnt,
              pos=wx.Point(366, 326), size=wx.Size(384, 276),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(376, 249))

        self.dbbDN = wx.lib.filebrowsebutton.DirBrowseButton(buttonText='...',
              dialogTitle=_(u'Choose local data directory'),
              id=wxID_VTNETSECXMLGUISLAVEBROWSEPANELDBBDN,
              labelText=_(u'local data:'), parent=self, pos=wx.Point(0,
              0), size=wx.Size(340, 29), startDirectory='.', style=0)
        self.dbbDN.SetMinSize(wx.Size(-1, -1))

        self.lblAppl = wx.StaticText(id=wxID_VTNETSECXMLGUISLAVEBROWSEPANELLBLAPPL,
              label=_(u'Application'), name=u'lblAppl', parent=self,
              pos=wx.Point(4, 37), size=wx.Size(112, 22), style=0)
        self.lblAppl.SetMinSize(wx.Size(-1, -1))

        self.txtAppl = wx.TextCtrl(id=wxID_VTNETSECXMLGUISLAVEBROWSEPANELTXTAPPL,
              name=u'txtAppl', parent=self, pos=wx.Point(120, 37),
              size=wx.Size(220, 22), style=0, value=u'')
        self.txtAppl.Enable(False)
        self.txtAppl.SetMinSize(wx.Size(-1, -1))

        self.cbAdd = wx.lib.buttons.GenBitmapButton(ID=wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBADD,
              bitmap=vtArt.getBitmap(vtArt.Add), name=u'cbAdd', parent=self,
              pos=wx.Point(344, 33), size=wx.Size(31, 30), style=0)
        self.cbAdd.Bind(wx.EVT_BUTTON, self.OnCbAddButton,
              id=wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBADD)

        self.lstRecent = wx.ListCtrl(id=wxID_VTNETSECXMLGUISLAVEBROWSEPANELLSTRECENT,
              name=u'lstRecent', parent=self, pos=wx.Point(4, 63),
              size=wx.Size(336, 144), style=wx.LC_REPORT)
        self._init_coll_lstRecent_Columns(self.lstRecent)
        self.lstRecent.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstRecentListColClick,
              id=wxID_VTNETSECXMLGUISLAVEBROWSEPANELLSTRECENT)
        self.lstRecent.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstRecentListItemDeselected,
              id=wxID_VTNETSECXMLGUISLAVEBROWSEPANELLSTRECENT)
        self.lstRecent.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstRecentListItemSelected,
              id=wxID_VTNETSECXMLGUISLAVEBROWSEPANELLSTRECENT)
        self.lstRecent.Bind(wx.EVT_LEFT_DCLICK, self.OnLstRecentLeftDclick)

        self.cbDel = wx.lib.buttons.GenBitmapButton(ID=wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBDEL,
              bitmap=vtArt.getBitmap(vtArt.Del), name=u'cbDel', parent=self,
              pos=wx.Point(344, 63), size=wx.Size(31, 30), style=0)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBDEL)

        self.cbUp = wx.lib.buttons.GenBitmapButton(ID=wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBUP,
              bitmap=vtArt.getBitmap(vtArt.Up), name=u'cbUp', parent=self,
              pos=wx.Point(344, 101), size=wx.Size(31, 30), style=0)
        self.cbUp.Bind(wx.EVT_BUTTON, self.OnCbUpButton,
              id=wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBUP)

        self.cbDown = wx.lib.buttons.GenBitmapButton(ID=wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBDOWN,
              bitmap=vtArt.getBitmap(vtArt.Down), name=u'cbDown', parent=self,
              pos=wx.Point(344, 135), size=wx.Size(31, 30), style=0)
        self.cbDown.Bind(wx.EVT_BUTTON, self.OnCbDownButton,
              id=wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBDOWN)

        self.cbSort = wx.lib.buttons.GenBitmapButton(ID=wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBSORT,
              bitmap=vtArt.getBitmap(vtArt.Sort), name=u'cbSort', parent=self,
              pos=wx.Point(344, 173), size=wx.Size(31, 30), style=0)
        self.cbSort.Bind(wx.EVT_BUTTON, self.OnCbSortButton,
              id=wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBSORT)

        self.cbOpen = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBOPEN,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Open'), name=u'cbOpen',
              parent=self, pos=wx.Point(4, 215), size=wx.Size(76, 30), style=0)
        self.cbOpen.SetMinSize(wx.Size(-1, -1))
        self.cbOpen.Bind(wx.EVT_BUTTON, self.OnCbOpenButton,
              id=wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBOPEN)

        self.cbConnect = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBCONNECT,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Connect'),
              name=u'cbConnect', parent=self, pos=wx.Point(88, 215),
              size=wx.Size(76, 30), style=0)
        self.cbConnect.SetMinSize(wx.Size(-1, -1))
        self.cbConnect.Bind(wx.EVT_BUTTON, self.OnCbConnectButton,
              id=wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBCONNECT)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'),
              name=u'cbApply', parent=self, pos=wx.Point(176, 215),
              size=wx.Size(76, 30), style=0)
        self.cbApply.SetMinSize(wx.Size(-1, -1))
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(260, 215),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.SetMinSize(wx.Size(-1, -1))
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBCANCEL)

        self.cbStop = wx.lib.buttons.GenBitmapButton(ID=wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBSTOP,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbStop', parent=self,
              pos=wx.Point(344, 215), size=wx.Size(31, 30), style=0)
        self.cbStop.SetToolTipString(_(u'stop'))
        self.cbStop.Bind(wx.EVT_BUTTON, self.OnCbStopButton,
              id=wxID_VTNETSECXMLGUISLAVEBROWSEPANELCBSTOP)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        self._init_ctrls(parent)
        #self.dn=os.path.expanduser('~')
        self.dn=vcCust.USR_LOCAL_DN
        self.lConn=[]
        
        self.dbbDN.SetValue(self.dn)
        self.cbOpen.SetBitmapLabel(img_gui.getOpenBitmap())
        self.cbConnect.SetBitmapLabel(img_gui.getConnectBitmap())
        self.cbStop.SetBitmapLabel(img_gui.getStoppedBitmap())
        self.netSlave=None
        
        self.fgsData.Fit(self)
        self.fgsData.Layout()
        
        self.selIdx=-1
    def SetNetSlave(self,netSlave):
        self.netSlave=netSlave
    def UpdateCfg(self,dn,lConn):
        if len(dn)==0:
            #dn=os.path.expanduser('~')
            dn=vcCust.USR_LOCAL_DN
        self.dbbDN.SetValue(dn)
        self.dn=dn
        self.lConn=lConn
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'dn:%s;lConn:%s'%(dn,vtLog.pformat(lConn)),self)
        self.__showConnections__()
        self.cbOpen.Enable(True)
        self.cbConnect.Enable(True)
    def OnLstRecentListColClick(self, event):
        event.Skip()

    def OnLstRecentListItemDeselected(self, event):
        self.selIdx=-1
        event.Skip()

    def OnLstRecentListItemSelected(self, event):
        self.selIdx=event.GetIndex()
        event.Skip()
    def __updateConnLst__(self,iStart,iEnd):
        for idx in range(iStart,iEnd):
            tup=self.lConn[idx]
            for i in range(4):
                self.lstRecent.SetStringItem(idx,i,tup[i+1])
    def OnCbUpButton(self, event):
        event.Skip()
        if self.selIdx<1:
            return
        idx=self.selIdx
        if self.VERBOSE:
            vtLog.CallStack('')
            vtLog.pprint(self.lConn)
        self.lConn=self.lConn[:idx-1]+[self.lConn[idx],self.lConn[idx-1]]+self.lConn[idx+1:]
        if self.VERBOSE:
            vtLog.CallStack('')
            vtLog.pprint(self.lConn)
        self.lstRecent.SetItemState(idx,0,wx.LIST_STATE_SELECTED)
        self.__updateConnLst__(idx-1,idx+1)
        idx-=1
        self.lstRecent.SetItemState(idx,wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
        self.lstRecent.EnsureVisible(idx)
        self.cbOpen.Enable(False)
        self.cbConnect.Enable(False)
        #self.__updateRecent__()
    def OnCbDownButton(self, event):
        event.Skip()
        if self.selIdx<0 or (self.selIdx+1)>=len(self.lConn):
            return
        if self.VERBOSE:
            vtLog.CallStack('')
            vtLog.pprint(self.lConn)
        idx=self.selIdx
        self.lstRecent.SetItemState(idx,0,wx.LIST_STATE_SELECTED)
        self.lConn=self.lConn[:idx]+[self.lConn[idx+1],self.lConn[idx]]+self.lConn[idx+2:]
        if self.VERBOSE:
            vtLog.CallStack('')
            vtLog.pprint(self.lConn)
        self.__updateConnLst__(idx,idx+2)
        idx+=1
        self.lstRecent.SetItemState(idx,wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
        self.lstRecent.EnsureVisible(idx)
        self.cbOpen.Enable(False)
        self.cbConnect.Enable(False)
        #self.__updateRecent__()
    def OnCbSortButton(self, event):
        self.lConn.sort()
        self.__updateConnLst__(0,len(self.lConn))
        self.cbOpen.Enable(False)
        self.cbConnect.Enable(False)
        event.Skip()
    def SetAppl(self,appl):
        self.txtAppl.SetValue(appl)
    def __showConnections__(self):
        if self.VERBOSE:
            vtLog.CallStack('')
        self.selIdx=-1
        self.lstRecent.DeleteAllItems()
        for tup in self.lConn:
            idx=self.lstRecent.InsertStringItem(sys.maxint,tup[1])
            for i in range(1,4):
                self.lstRecent.SetStringItem(idx,i,tup[i+1])

    def OnCbOpenButton(self, event):
        appl=self.txtAppl.GetValue()
        if self.VERBOSE:
            vtLog.CallStack('')
            print self.selIdx,self.lConn
        self.netSlave.Open()
        if event is not None:
            event.Skip()
        
    def OnCbStopButton(self, event):
        appl=self.txtAppl.GetValue()
        if self.VERBOSE:
            vtLog.CallStack('')
            print self.selIdx,self.lConn
        self.netSlave.DoDisConnect(self.selIdx)
        if event is not None:
            event.Skip()
    
    def OnCbConnectButton(self, event):
        appl=self.txtAppl.GetValue()
        if self.VERBOSE:
            vtLog.CallStack('')
            print self.selIdx,self.lConn
        self.netSlave.DoConnect(self.selIdx)
        if event is not None:
            event.Skip()

    def OnCbCancelButton(self, event):
        if self.cbOpen.IsEnabled():
            self.GetParent().Show(False)
        else:
            self.netSlave.SetCfgNode(bConnect=False)
        event.Skip()

    def OnLstRecentLeftDclick(self, event):
        event.Skip()
        if self.selIdx<0:
            return
        self.OnCbConnectButton(None)

    def OnCbAddButton(self, event):
        event.Skip()
        try:
            dlg=self.netSlave.GetLoginDlg()
            if dlg is not None:
                if dlg.IsShown():
                    return
                #dlg.Show(True)
                sAppl=self.txtAppl.GetValue()
                dlg.Centre()
                sTmpHost,iTmpPort=vcCust.getDefaultHostPortPerNetAppl(sAppl)
                dlg.SetValues(sAppl,sTmpHost,iTmpPort,'','')
                if dlg.ShowModal()>0:
                    sHost=dlg.GetHost()
                    sPort=dlg.GetPort()
                    sAlias=dlg.GetAlias()
                    sUsr=dlg.GetUsr()
                    sPasswd=dlg.GetPasswd()
                    self.lConn.append([sAppl,sAlias,sHost,sPort,sUsr,sPasswd])
                    self.__showConnections__()
                    self.cbOpen.Enable(False)
                    self.cbConnect.Enable(False)
        except:
            vtLog.vtLngTB(self.GetName())

    def OnCbApplyButton(self, event):
        event.Skip()
        self.dn=self.dbbDN.GetValue()
        self.netSlave.StoreCfg(self.dn,self.lConn)
        self.cbOpen.Enable(True)
        self.cbConnect.Enable(True)

    def OnCbDelButton(self, event):
        event.Skip()
        if self.selIdx<0:
            return
        self.lConn=self.lConn[:self.selIdx]+self.lConn[self.selIdx+1:]
        self.lstRecent.DeleteItem(self.selIdx)
        self.selIdx=-1
        self.cbOpen.Enable(False)
        self.cbConnect.Enable(False)

