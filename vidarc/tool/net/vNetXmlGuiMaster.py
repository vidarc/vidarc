#----------------------------------------------------------------------------
# Name:         vNetXmlGuiMaster.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vNetXmlGuiMaster.py,v 1.8 2008/02/02 13:43:38 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

#from vidarc.tool.net.vNetXmlGuiMasterPopupWindow import *
from vidarc.tool.net.vNetXmlGuiMasterDialog import *
from vidarc.tool.net.vNetCfgDialog import vNetCfgDialog
from vidarc.tool.net.vNetLoginDialog import vNetLoginDialog
#from vidarc.tool.net.vNetSynchEditResolveDialog import *
from vidarc.tool.net.vNetXmlSynchEditResolveDialog import vNetXmlSynchEditResolveDialog
from vidarc.tool.net.vNetXmlWxGui import *
from vidarc.tool.xml.vtXmlDom import *

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt

import string,os.path,sys
import img_gui

vtnxEVT_NET_XML_MASTER_START=NewEventType()
def EVT_NET_XML_MASTER_START(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_MASTER_START,func)
class vtnxXmlMasterStart(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_MASTER_START(<widget_name>, xxx)
    """
    def __init__(self):
        PyEvent.__init__(self)
        self.SetEventType(vtnxEVT_NET_XML_MASTER_START)

vtnxEVT_NET_XML_MASTER_FINISHED=NewEventType()
def EVT_NET_XML_MASTER_FINISHED(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_XML_MASTER_FINISHED,func)
class vtnxXmlMasterFinished(PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_XML_MASTER_FINISHED(<widget_name>, xxx)
    """
    def __init__(self):
        PyEvent.__init__(self)
        self.SetEventType(vtnxEVT_NET_XML_MASTER_FINISHED)

class vNetXmlGuiMaster(StaticBitmap):
    def __init__(self,parent,appl,pos=(0,0),size=(16,12),verbose=0):
        StaticBitmap.__init__(self,parent=parent,pos=pos,size=size,
                bitmap=EmptyBitmap(16, 12))
        self.semLogin=threading.Semaphore()
        
        #self.pwMaster=vNetXmlGuiMasterPopupWindow(self,verbose=0)
        self.dlgMaster=vNetXmlGuiMasterDialog(self,verbose=0)
        self.dlgLogin=vNetLoginDialog(self)
        self.dlgEditResolve=vNetXmlSynchEditResolveDialog(self)
        self.dlgSetup=None
        self.dlgCfgSetup=None
        self.nodeSetup=None
        self.docSetup=None
        self.lstSetupNode=[]
        self.lstCfg=None
        self.dictDoc={}
        self.appls=[]
        self.mainAppl=''
        self.dn=''
        self.sRecentAlias=''
        self.sRecentConn=''
        self.alias=''
        self.host=''
        self.port=-1
        self.usr=''
        self.passwd=''
        self.verbose=verbose

        img=img_gui.getStoppedBitmap()
        self.SetBitmap(img)
        
        #EVT_LEFT_DOWN(self, self.OnLeftDown)
        EVT_RIGHT_DOWN(self, self.OnRightDown)
    def SetBitmap(self,img):
        StaticBitmap.SetBitmap(self,img)
        self.Refresh()
    
    def SetCfg(self,doc,lst,appls,mainAppl):
        vtLog.CallStack('')
        self.docSetup=doc
        self.lstCfg=lst
        self.dictDoc={}
        self.sRecentAlias=''
        self.sRecentConn=''
        self.appls=appls
        self.mainAppl=mainAppl
        
        #self.pwMaster.SetCfg(doc,lst,appls,mainAppl)
        self.dlgMaster.SetCfg(doc,lst,appls,mainAppl)
        
    def SetCfgNode(self,bConnect=True):
        #self.pwMaster.SetCfgNode(bConnect)
        self.dlgMaster.SetCfgNode(bConnect)
    
    def SetLoginDlg(self,dlg):
        self.dlgLogin=dlg
    def GetLoginDlg(self):
        return self.dlgLogin#vNetLoginDialog(self)
    def GetEditResolveDlg(self):
        return self.dlgEditResolve
    def AcquireLogin(self):
        self.semLogin.acquire()
    def ReleaseLogin(self):
        self.semLogin.release()
    
    def AddNetControl(self,ctrlClass,name,*args,**kwargs):
        #return self.pwMaster.AddNetControl(ctrlClass,name,*args,**kwargs)
        return self.dlgMaster.AddNetControl(ctrlClass,name,*args,**kwargs)
        
    def SetDictDoc(self,d):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'dict:%s'%(d))
        if self.docSetup is None:
            return
        self.dictDoc=d
        for k in self.dictDoc.keys():
            if k == self.appls[0]:
                continue
            doc=self.dictDoc[k]
            doc.SetCfg(self.docSetup,self.lstCfg,self.appls,k,
                    self.mainAppl,self.dlgCfgSetup)
    
    def __getRecentAlias__(self):
        #if self.verbose:
        #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        if self.docSetup is None:
            return None
        else:
            self.nodeSetup=self.docSetup.getRoot()
            node=self.docSetup.getChildByLst(self.nodeSetup,self.lstCfg)
            self.dn=self.docSetup.getNodeText(node,'path')
            #node=self.docSetup.getChild(node,self.appl)
            if (node is None) or (len(self.dn)<=0):
                self.fn=None
                return None
                #self.Setup()
                #return
            recentNode=None
            iRecent=1000
            for c in self.docSetup.getChilds(node,'alias'):
                s=self.docSetup.getNodeText(c,'recent')
                if len(s)>0:
                    try:
                        i=int(s)
                        if i<iRecent:
                            recentNode=c
                            iRecent=i
                    except:
                        pass
            return recentNode
    def __cmpRecentFunc__(self,a,b):
            try:
                iA=int(self.docSetup.getNodeText(a,'recent'))
            except:
                iA=0
            try:
                iB=int(self.docSetup.getNodeText(b,'recent'))
            except:
                iB=0
            return cmp(iA,iB)
        
    def __createRecentAlias__(self,sName):
        if self.docSetup is None:
            return None
        self.nodeSetup=self.docSetup.getRoot()
        node=self.docSetup.getChildByLst(self.nodeSetup,self.lstCfg)
        self.dn=self.docSetup.getNodeText(node,'path')
        #node=self.docSetup.getChild(node,self.appl)
        if (node is None) or (len(self.dn)<=0):
            self.fn=None
            return None
            #self.Setup()
            #return
        nodeAliases=self.docSetup.getChilds(node,'alias')
        n=self.docSetup.createChildByLst(node,'alias',[
                        {'tag':'name','val':sName},
                        {'tag':'recent','val':'0'},
                        ])
        nodeAliases.sort(self.__cmpRecentFunc__)
        i=1
        for c in nodeAliases:
            self.docSetup.setNodeText(a,'recent',str(i))
            i+=1
        self.docSetup.AlignNode(n,iRec=2)
        return n
    def __createRecentConnection__(self,recentNode,sName):
        if self.docSetup is None:
            return None
        if recentNode is None:
            return None
        nodeConnections=self.docSetup.getChilds(recentNode,'connection')
        nodeConnections.sort(self.__cmpRecentFunc__)
        n=self.docSetup.createChildByLst(recentNode,'connection',[
                            {'tag':'name','val':sName},
                            {'tag':'recent','val':'0'},
                            ])
        i=1
        for c in nodeConnections:
            self.docSetup.setNodeText(a,'recent',str(i))
            i+=1
        self.docSetup.AlignNode(n,iRec=2)
        return n
    def __createRecentHost__(self,recentConn,sAppl,sFN):
        if self.docSetup is None:
            return None
        if recentConn is None:
            return None
        #nodeHosts=self.docSetup.getChilds(recentNode,'host')
        #nodeHosts.sort(self.__cmpRecentFunc__)
        applNode=self.docSetup.getChildForced(recentConn,sAppl)
        n=self.docSetup.createChildByLst(applNode,'host',
                    [{'tag':'name','val':'main'},
                     {'tag':'recent','val':'0'},
                     {'tag':'appl','val':sAppl},
                     {'tag':'alias','val':''},
                     {'tag':'host','val':''},
                     {'tag':'port','val':'offline'},
                     {'tag':'usr','val':''},
                     {'tag':'passwd','val':''},
                     {'tag':'fn','val':sFN},
                    ])
        #i=1
        #for c in nodeHosts:
        #    self.docSetup.setNodeText(a,'recent',str(i))
        #    i+=1
        self.docSetup.AlignNode(applNode,iRec=2)
        
        return n
    def __getRecentConnection__(self,recentNode):
        #if self.verbose:
        #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
            
        if recentNode is None:
            return None
        
        recentConnNode=None
        iRecent=1000
        for c in self.docSetup.getChilds(recentNode,'connection'):
            s=self.docSetup.getNodeText(c,'recent')
            if len(s)>0:
                try:
                    i=int(s)
                    if i<iRecent:
                        recentConnNode=c
                        iRecent=i
                except:
                    pass
        return recentConnNode
    def __getApplHost__(self,recentNode,appl=None):
        if recentNode is None:
            return None
        if appl is None:
            appl=self.appl
        applNode=self.docSetup.getChildForced(recentNode,appl)
        recentNode=None
        iRecent=1000
        for c in self.docSetup.getChilds(applNode,'host'):
            s=self.docSetup.getNodeText(c,'recent')
            if len(s)>0:
                try:
                    i=int(s)
                    if i<iRecent:
                        recentNode=c
                        iRecent=i
                except:
                    pass
        return recentNode
    def __getAutoConnect__(self,recentNode):
        applNode=self.docSetup.getChild(recentNode,self.appl)
        sName=self.docSetup.getNodeText(applNode,'autoconnect')
        if sName=='1':
            return True
        elif sName=='0':
            return False
        else:
            self.docSetup.setNodeText(applNode,'autoconnect','1')
            return True
    def ReConnect2Srv(self,doc=None):
        #if self.verbose:
        #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'doc:%d docSetup:%d'%(doc is not None,self.docSetup is not None))
        #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'appl:%s alias:%s host:%s port:%s usr:%s passwd:%s'%
        #            (self.appl,self.alias,self.host,self.port,self.usr,self.passwd))
        #dictNet=self.pwMaster.GetDictNet()
        #lstNetAppl=self.pwMaster.GetNetNames()
        dictNet=self.dlgMaster.GetDictNet()
        lstNetAppl=self.dlgMaster.GetNetNames()
        i=0
        for appl in lstNetAppl:
            netDoc=dictNet[appl]
            
            #self.DisConnect()
            if self.verbose:
                vtLog.vtLngCallStack(self,vtLog.DEBUG,'recent alias:%s recent conn:%s'%(oldRecentAlias,oldRecentConn),origin=appl)
                vtLog.vtLngCallStack(self,vtLog.DEBUG,'host:%s alias:%s usr:%s'%(oldHost,oldAlias,oldUsr),origin=appl)
            
            recentNode=self.__getRecentAlias__()
            recentAliasNode=self.__getRecentConnection__(recentNode)
            recentHostNode=self.__getApplHost__(recentAliasNode,appl)
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'appl:%s recent:%s'%(appl,recentNode),origin=appl)
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'appl:%s recent:%s'%(appl,recentAliasNode),origin=appl)
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'appl:%s recent:%s'%(appl,recentHostNode),origin=appl)
            
            netDoc.Connect2SrvByCfg(recentNode,recentAliasNode,recentHostNode)
            
            if i==0:
                if netDoc.GetStatus()!=netDoc.SYNCH_FIN:
                    break
            i+=1
            pass
    def AutoConnect2Srv(self,idx=-1,force=False):
        #if self.verbose:
        #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'idx:%d docSetup:%d'%(idx,self.docSetup is not None))
        #dictNet=self.pwMaster.GetDictNet()
        #lstNetAppl=self.pwMaster.GetNetNames()
        dictNet=self.dlgMaster.GetDictNet()
        lstNetAppl=self.dlgMaster.GetNetNames()
        i=0
        for appl in lstNetAppl:
            netDoc=dictNet[appl]
            if idx>=0:
                if i!=idx:
                    i+=1
                    continue
            #self.DisConnect()
            if idx==-2:
                if i==0:
                    i+=1
                    continue
            #if netDoc.GetStatus() not in [netDoc.DISCONNECTED,netDoc.STOPPED]:
            #    i+=1
            #    continue
            if netDoc.IsFlag(netDoc.CONN_ACTIVE):
                vtLog.vtLngCallStack(None,vtLog.DEBUG,'skip due connection active %s'%appl,origin=appl)
                i+=1
                continue
            if self.verbose:
                vtLog.vtLngCallStack(self,vtLog.DEBUG,'idx:%d docSetup:%d'%(idx,self.docSetup is not None),origin='vNetMaster')
            recentNode=self.__getRecentAlias__()
            recentAliasNode=self.__getRecentConnection__(recentNode)
            recentHostNode=self.__getApplHost__(recentAliasNode,appl)
            if recentHostNode is None:
                vtLog.vtLngCallStack(self,vtLog.ERROR,'recent host node not found.',origin=appl)
                continue
            #vtLog.vtLngCallStack(self,vtLog.DEBUG,'appl:%s recent:%s'%(appl,recentNode))
            #vtLog.vtLngCallStack(self,vtLog.DEBUG,'appl:%s recent:%s'%(appl,recentAliasNode))
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'recent:%s'%(repr(recentHostNode.content)),origin=appl)
            
            #netDoc.SetFlag(netDoc.CONN_ACTIVE,True)
            netDoc.Connect2SrvByCfg(recentNode,recentAliasNode,recentHostNode,True)
            
            if i==0:
                #wx.PostEvent(self,vtnxXmlMasterStart())
                
                #if netDoc.GetStatus()!=netDoc.SYNCH_FIN:
                #    break
                #if netDoc.IsFlag(netDoc.OPENED)==False:
                #    break
                if netDoc.IsFlag(netDoc.CFG_VALID)==False:
                    break
            i+=1
            pass
    def SignalMasterStart(self):
        wx.PostEvent(self,vtnxXmlMasterStart())
    
    def OpenApplDocs(self):
        for k in self.dictDoc.keys():
            if k == self.appls[0]:
                continue
            self.dictDoc[k].Open()
    def SaveApplDocs(self,encoding='ISO-8859-1'):
        for k in self.dictDoc.keys():
            if k == self.appls[0]:
                continue
            self.dictDoc[k].Save(encoding=encoding)
    def OnLeftDown(self, event):
        btn = event.GetEventObject()
        pos = btn.ClientToScreen( (0,0) )
        sz =  btn.GetSize()
        #self.pwMaster.Position(pos, (0, sz[1]))
        #self.pwMaster.Show(True)
        self.dlgMaster.Centre()
        self.dlgMaster.Show(True)
        return
    def ShowPopup(self):
        btn=self
        pos = btn.ClientToScreen( (0,0) )
        sz =  btn.GetSize()
        #self.pwMaster.Position(pos, (0, sz[1]))
        #self.pwMaster.Show(True)
        self.dlgMaster.Centre()
        self.dlgMaster.Show(True)
    def OnRightDown(self, event):
        btn = event.GetEventObject()
        pos = btn.ClientToScreen( (0,0) )
        sz =  btn.GetSize()
        #self.pwMaster.Position(pos, (0, sz[1]))
        #self.pwMaster.Show(True)
        self.dlgMaster.Centre()
        self.dlgMaster.Show(True)
        return
        
        if not hasattr(self,'popupIdConnect'):
            self.popupIdConnect=NewId()
            self.popupIdDisConnect=NewId()
            self.popupIdSetup=NewId()
            EVT_MENU(self,self.popupIdConnect,self.OnConnect)
            EVT_MENU(self,self.popupIdDisConnect,self.OnDisConnect)
            EVT_MENU(self,self.popupIdSetup,self.OnSetup)
        
        menu=Menu()
        mnIt=MenuItem(menu,self.popupIdConnect,'master')
        #mnIt.SetBitmap(images.getGotoTmplBitmap())
        menu.AppendItem(mnIt)
        #mnIt.Enable(False)
        menu.AppendSeparator()
        if self.cltSock is None:
            mnIt=MenuItem(menu,self.popupIdConnect,'Connect')
            #mnIt.SetBitmap(images.getGotoTmplBitmap())
            menu.AppendItem(mnIt)
        else:
            mnIt=MenuItem(menu,self.popupIdDisConnect,'Disconnect')
            #mnIt.SetBitmap(images.getGotoTmplBitmap())
            menu.AppendItem(mnIt)
        menu.AppendSeparator()
        mnIt=MenuItem(menu,self.popupIdSetup,'Setup')
        #mnIt.SetBitmap(images.getGotoTmplBitmap())
        menu.AppendItem(mnIt)
        self.PopupMenu(menu,Point(event.GetX(), event.GetY()))
        menu.Destroy()
    def OnSetup(self,evt):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'appl:%s'%(self.mainAppl))
        if self.docSetup is not None:
            if self.appl==self.appls[0]:
                oldRecentAlias=self.sRecentAlias
                oldRecentConn=self.sRecentConn
                oldHost=self.host
                oldAlias=self.alias
                oldUsr=self.usr
                if self.verbose:
                    vtLog.vtLngCallStack(self,vtLog.DEBUG,'recent alias:%s recent conn:%s'%(oldRecentAlias,oldRecentConn))
                    vtLog.vtLngCallStack(self,vtLog.DEBUG,'host:%s alias:%s usr:%s'%(oldHost,oldAlias,oldUsr))
            
        self.Setup()
    def __getCfgNode__(self):
        self.nodeSetup=self.docSetup.getRoot()
        return self.docSetup.getChildByLst(self.nodeSetup,self.lstCfg)
    
    def Setup(self,node=None):
        return
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'appl:%s'%(self.appls[0]))
        if self.lstCfg is None:
            if node is None:
                node=self.nodeSetup
                if node is None:
                    node=self.docSetup.getRoot()
            if self.dlgSetup is None:
                self.dlgSetup=vNetPlaceDialog(self)
            self.dlgSetup.Centre()
            n=self.getChildByLst(node,self.lstSetupNode)
            if n is None:
                n=self.docSetup.createMissingSubNodeByLst(node,self.lstSetupNode)
            self.dlgSetup.SetNode(self.docSetup,n,self.appl)
            ret=self.dlgSetup.ShowModal()
            if ret>0:
                self.dlgSetup.GetNode()
                self.docSetup.AlignNode(n,iRec=2)
                self.Connect2SrvByNode(None,None)
                PostEvent(self,vtnxXmlSetupChanged())
                pass
        else:
            if self.dlgCfgSetup is None:
                return
                #self.dlgCfgSetup=vNetCfgDialog(self)
            self.dlgCfgSetup.Centre()
            node=self.docSetup.getRoot()
            #recentOld=self.__getRecentConnection__()
            #infos=InfoDiff(self.docSetup)
            self.dlgCfgSetup.SelAppl(self.appls[0])
            self.dlgCfgSetup.SetNode(self.docSetup,self.lstCfg,self.appls,
                                self.appls[0],self.__getCfgNode__())
            ret=self.dlgCfgSetup.ShowModal()
            if ret>0:
                #recentNew=self.__getRecentConnection__()
                #res=infos.checkDiff(recentOld,recentNew)
                self.AutoConnect2Srv()
                #self.ReConnect2Srv()
                self.SaveCfg()
                #PostEvent(self,vtnxXmlSetupChanged())
            #del infos
    def SaveCfg(self,fn=None):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'appl:%s'%(self.mainAppl))
        self.docSetup.AlignDoc()
        if self.docSetup.Save(fn)<0:
            dlg = FileDialog(self, "Save File As", ".", "", "XML files (*.xml)|*.xml|all files (*.*)|*.*", SAVE)
            try:
                if dlg.ShowModal() == ID_OK:
                    filename = dlg.GetPath()
                    #self.txtEditor.SaveFile(filename)
                    self.SaveCfg(filename)
            finally:
                dlg.Destroy()
        return 0
    def OnConnect(self,evt):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'appl:%s'%(self.mainAppl))
        self.AutoConnect2Srv(force=True)
    def OnDisConnect(self,evt):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'appl:%s'%(self.mainAppl))
        #self.endEdit(None)
        if self.cltSrv is not None:
            self.cltSrv.Stop()
        #self.__connectionClosed__()
        #PostEvent(self,vtnxXmlDisConnect())
    def __getHostAliasName__(self,doc,n):
        sName=doc.getNodeText(n,'name')+':'+doc.getNodeText(n,'port')+'//'+doc.getNodeText(n,'alias')
        return sName
    def __getConnHostAliasName__(self,doc,n):
        sName=doc.getNodeText(n,'host')+':'+doc.getNodeText(n,'port')+'//'+doc.getNodeText(n,'alias')
        return sName
    def __updateConfig__(self):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'update config',origin='vNetMaster')
        #dictNet=self.pwMaster.GetDictNet()
        #lstNetAppl=self.pwMaster.GetNetNames()
        dictNet=self.dlgMaster.GetDictNet()
        lstNetAppl=self.dlgMaster.GetNetNames()
        doc=dictNet[lstNetAppl[0]]
        doc.SetFlag(doc.CFG_VALID,True)
        if self.docSetup is None:
            return
        recentNode=self.__getRecentAlias__()
        if recentNode is None:
            return
        recentConnNode=self.__getRecentConnection__(recentNode)
        if recentConnNode is None:
            return
        cfgNode=doc.getChild(doc.getRoot(),'config')
        if cfgNode is not None:
            for a in lstNetAppl[1:]:
                bAdd=False
                n=doc.getChild(cfgNode,a)
                if n is None:
                    continue
                childs=doc.getChilds(recentNode,a)
                for c in childs:
                    doc.deleteNode(c,recentNode)
                nn=doc.cloneNode(n,True)
                doc.appendChild(recentNode,nn)
                d={}
                for c in doc.getChilds(nn,'host'):
                    d[self.__getHostAliasName__(doc,c)]=c
                bAdd=False
                child=doc.getChildForced(recentConnNode,a)
                bAutoConnect=''
                childs=doc.getChilds(child,'host')
                connHosts={}
                for c in childs:
                    connHosts[self.__getConnHostAliasName__(doc,c)]=c
                iRecent=len(connHosts.keys())
                for k in d.keys():
                    if connHosts.has_key(k):
                        pass
                    else:
                        tmp=d[k]
                        sName=doc.getNodeText(tmp,'name')
                        sPort=doc.getNodeText(tmp,'port')
                        sAlias=doc.getNodeText(tmp,'alias')
                        n=doc.createChildByLst(child,'host',
                                        [{'tag':'name','val':sName},
                                         {'tag':'recent','val':str(iRecent)},
                                         {'tag':'alias','val':sAlias},
                                         {'tag':'host','val':sName},
                                         {'tag':'port','val':sPort},
                                         {'tag':'usr','val':self.usr},
                                         {'tag':'passwd','val':self.passwd}
                                        ])
                        iRecent+=1
                for k in connHosts.keys():
                    if d.has_key(k):
                        pass
                    else:
                        doc.deleteNode(connHosts[k],recentNode)
            doc.AlignNode(recentNode,iRec=3)
            #self.pwMaster.SetCfgNode()
            self.dlgMaster.SetCfgNode()
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'finished',origin='vNetMaster')
        
    def Notify(self,netDoc,status,*args,**kwargs):
        #self.pwMaster.Notify(netDoc,status,*args,**kwargs)
        self.dlgMaster.Notify(netDoc,status,*args,**kwargs)
    def Stop(self):
        self.dlgMaster.Stop()
    def ShutDown(self):
        self.dlgMaster.ShutDown()
    def IsModified(self):
        return self.dlgMaster.IsModified()
    def Save(self,bModified=False):
        self.dlgMaster.Save(bModified)
    def GetNetDoc(self,name):
        return self.dlgMaster.GetNetDoc(name)
    def GetNetDocNodeInfos(self,name,id,lst,lang=None):
        netDoc=self.GetNetDoc(name)
        if netDoc is None:
            return None
        node=netDoc.getNodeById(id)
        if node is None:
            return None
        netDoc.acquire()
        try:
            d={}
            for it in lst:
                val=netDoc.getNodeInfoVal(node,it,lang=lang)
                d[it]=val
        except:
            vtLog.vtLngTB(self.GetName())
        netDoc.release()
        return d
    def GetNetDocNodeInfosById(self,name,id,lst,lang=None):
        netDoc=self.GetNetDoc(name)
        if netDoc is None:
            return None
        node=netDoc.getNodeById(id)
        if node is None:
            return None
        netDoc.acquire()
        try:
            d={}
            for it in lst:
                val=netDoc.getNodeInfoVal(node,it,lang=lang)
                d[it]=val
        except:
            vtLog.vtLngTB(self.GetName())
        netDoc.release()
        return d
    def GetNetDocNodeInfosByIdNum(self,name,id,lst,lang=None):
        netDoc=self.GetNetDoc(name)
        if netDoc is None:
            return None
        node=netDoc.getNodeByIdNum(id)
        if node is None:
            return None
        netDoc.acquire()
        try:
            d={}
            for it in lst:
                val=netDoc.getNodeInfoVal(node,it,lang=lang)
                d[it]=val
        except:
            vtLog.vtLngTB(self.GetName())
        netDoc.release()
        return d
        
    def GetDocAlias(self,name):
        return self.dlgMaster.GetNetDoc(name)
    def GetDocAliasNodeInfos(self,name,id,lst,lang=None):
        netDoc=self.GetDocAlias(name)
        if netDoc is None:
            return None
        node=netDoc.getNodeById(id)
        if node is None:
            return None
        netDoc.acquire()
        try:
            d={}
            for it in lst:
                val=netDoc.getNodeInfoVal(node,it,lang=lang)
                d[it]=val
        except:
            vtLog.vtLngTB(self.GetName())
        netDoc.release()
        return d
    def GetDocAliasNodeInfosById(self,name,id,lst,lang=None):
        netDoc=self.GetDocAlias(name)
        if netDoc is None:
            return None
        node=netDoc.getNodeById(id)
        if node is None:
            return None
        netDoc.acquire()
        try:
            d={}
            for it in lst:
                val=netDoc.getNodeInfoVal(node,it,lang=lang)
                d[it]=val
        except:
            vtLog.vtLngTB(self.GetName())
        netDoc.release()
        return d
    def GetDocAliasNodeInfosByIdNum(self,name,id,lst,lang=None):
        netDoc=self.GetDocAlias(name)
        if netDoc is None:
            return None
        node=netDoc.getNodeByIdNum(id)
        if node is None:
            return None
        netDoc.acquire()
        try:
            d={}
            for it in lst:
                val=netDoc.getNodeInfoVal(node,it,lang=lang)
                d[it]=val
        except:
            vtLog.vtLngTB(self.GetName())
        netDoc.release()
        return d
