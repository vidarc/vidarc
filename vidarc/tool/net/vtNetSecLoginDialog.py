#Boa:Dialog:vtNetSecLoginDialog
#----------------------------------------------------------------------------
# Name:         vNetLoginDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtNetSecLoginDialog.py,v 1.25 2008/03/23 15:17:48 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import sha,Queue
from vidarc.tool.net.vtNetClt import vtNetClt
from vidarc.tool.net.vtNetXmlClt import vtNetXmlCltSock
import vidarc.tool.net.vtNetSecLoginHost as vtNetSecLoginHost
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.vtThread import CallBack

import vidarc.tool.net.vtNetArt as vtNetArt

def create(parent):
    return vtNetSecLoginDialog(parent)

[wxID_VTNETSECLOGINDIALOG, wxID_VTNETSECLOGINDIALOGCBAPPLY, 
 wxID_VTNETSECLOGINDIALOGCBBROWSE, wxID_VTNETSECLOGINDIALOGCBCANCEL, 
 wxID_VTNETSECLOGINDIALOGCHCALIAS, wxID_VTNETSECLOGINDIALOGCHCALL, 
 wxID_VTNETSECLOGINDIALOGCHCHOSTPORT, wxID_VTNETSECLOGINDIALOGLBLALIAS, 
 wxID_VTNETSECLOGINDIALOGLBLAPPL, wxID_VTNETSECLOGINDIALOGLBLHOST, 
 wxID_VTNETSECLOGINDIALOGLBLPASSWD, wxID_VTNETSECLOGINDIALOGLBLRESULT, 
 wxID_VTNETSECLOGINDIALOGLBLUSR, wxID_VTNETSECLOGINDIALOGSTBRESULT, 
 wxID_VTNETSECLOGINDIALOGTXTALIAS, wxID_VTNETSECLOGINDIALOGTXTAPPL, 
 wxID_VTNETSECLOGINDIALOGTXTHOST, wxID_VTNETSECLOGINDIALOGTXTPASSWD, 
 wxID_VTNETSECLOGINDIALOGTXTPORT, wxID_VTNETSECLOGINDIALOGTXTUSR, 
] = [wx.NewId() for _init_ctrls in range(20)]

wxEVT_VTNETSEC_LOGIN_DIALOG_APPLY=wx.NewEventType()
vEVT_VTNETSEC_LOGIN_DIALOG_APPLY=wx.PyEventBinder(wxEVT_VTNETSEC_LOGIN_DIALOG_APPLY,1)
def EVT_VTNETSEC_LOGIN_DIALOG_APPLY(win,func):
    win.Connect(-1,-1,wxEVT_VTNETSEC_LOGIN_DIALOG_APPLY,func)
class vtNetSecLoginDialogApply(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTNETSEC_LOGIN_DIALOG_APPLY(<widget_name>, self.OnApply)
    """
    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTNETSEC_LOGIN_DIALOG_APPLY)
        self.obj=obj
    def GetObject(self):
        return self.obj

wxEVT_VTNETSEC_LOGIN_DIALOG_CANCEL=wx.NewEventType()
vEVT_VTNETSEC_LOGIN_DIALOG_CANCEL=wx.PyEventBinder(wxEVT_VTNETSEC_LOGIN_DIALOG_CANCEL,1)
def EVT_VTNETSEC_LOGIN_DIALOG_CANCEL(win,func):
    win.Connect(-1,-1,wxEVT_VTNETSEC_LOGIN_DIALOG_CANCEL,func)
class vtNetSecLoginDialogCancel(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTNETSEC_LOGIN_DIALOG_CANCEL(<widget_name>, self.OnCancel)
    """
    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTNETSEC_LOGIN_DIALOG_CANCEL)
        self.obj=obj
    def GetObject(self):
        return self.obj

class vtNetSecLoginDialog(wx.Dialog):
    def _init_coll_bxsHost_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblHost, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtHost, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.txtPort, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.chcHostPort, 2, border=4,
              flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(9)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsHost, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsAppl, 1, border=4,
              flag=wx.TOP | wx.EXPAND | wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsAlias, 1, border=4,
              flag=wx.TOP | wx.EXPAND | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.cbBrowse, 0, border=4, flag=wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsAlias2, 0, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.BOTTOM | wx.TOP | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsUser, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsPasswd, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.stbResult, 0, border=4,
              flag=wx.ALIGN_LEFT | wx.TOP)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.lblResult, 0, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER)

    def _init_coll_bxsPasswd_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPasswd, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtPasswd, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsAppl_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblAppl, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtAppl, 2, border=4, flag=wx.EXPAND | wx.LEFT)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_bxsUser_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblUsr, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtUsr, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsAlias2_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.chcAll, 1, border=8, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.chcAlias, 2, border=4, flag=wx.EXPAND | wx.LEFT)

    def _init_coll_bxsAlias_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblAlias, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtAlias, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=2, hgap=0, rows=12, vgap=0)
        self.fgsData.SetMinSize(wx.Size(-1, -1))

        self.bxsHost = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.bxsHost.SetMinSize(wx.Size(-1, -1))

        self.bxsPort = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.bxsPort.SetMinSize(wx.Size(-1, -1))

        self.bxsUser = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.bxsUser.SetMinSize(wx.Size(-1, -1))

        self.bxsPasswd = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.bxsPasswd.SetMinSize(wx.Size(-1, -1))

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.bxsBt.SetMinSize(wx.Size(-1, -1))

        self.bxsAppl = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.bxsAppl.SetMinSize(wx.Size(-1, -1))

        self.bxsAlias = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.bxsAlias.SetMinSize(wx.Size(-1, -1))

        self.bxsAlias2 = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.bxsAlias2.SetMinSize(wx.Size(-1, -1))

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsHost_Items(self.bxsHost)
        self._init_coll_bxsUser_Items(self.bxsUser)
        self._init_coll_bxsPasswd_Items(self.bxsPasswd)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsAppl_Items(self.bxsAppl)
        self._init_coll_bxsAlias_Items(self.bxsAlias)
        self._init_coll_bxsAlias2_Items(self.bxsAlias2)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTNETSECLOGINDIALOG,
              name=u'vtNetSecLoginDialog', parent=prnt, pos=wx.Point(440, 215),
              size=wx.Size(401, 309),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE | wx.STAY_ON_TOP,
              title=u'vtNetSec Login')
        self.SetClientSize(wx.Size(393, 282))

        self.lblHost = wx.StaticText(id=wxID_VTNETSECLOGINDIALOGLBLHOST,
              label=_(u'Host'), name=u'lblHost', parent=self, pos=wx.Point(4,
              4), size=wx.Size(49, 21), style=wx.ALIGN_RIGHT)
        self.lblHost.SetMinSize(wx.Size(-1, -1))

        self.lblUsr = wx.StaticText(id=wxID_VTNETSECLOGINDIALOGLBLUSR,
              label=_(u'User'), name=u'lblUsr', parent=self, pos=wx.Point(4,
              129), size=wx.Size(82, 21), style=wx.ALIGN_RIGHT)
        self.lblUsr.SetMinSize(wx.Size(-1, -1))

        self.lblPasswd = wx.StaticText(id=wxID_VTNETSECLOGINDIALOGLBLPASSWD,
              label=_(u'Password'), name=u'lblPasswd', parent=self,
              pos=wx.Point(4, 154), size=wx.Size(82, 21), style=wx.ALIGN_RIGHT)
        self.lblPasswd.SetMinSize(wx.Size(-1, -1))

        self.txtHost = wx.TextCtrl(id=wxID_VTNETSECLOGINDIALOGTXTHOST,
              name=u'txtHost', parent=self, pos=wx.Point(57, 4),
              size=wx.Size(45, 21), style=0, value=u'')
        self.txtHost.Enable(False)
        self.txtHost.SetMinSize(wx.Size(-1, -1))

        self.txtPort = wx.TextCtrl(id=wxID_VTNETSECLOGINDIALOGTXTPORT,
              name=u'txtPort', parent=self, pos=wx.Point(106, 4),
              size=wx.Size(45, 21), style=0, value=u'')
        self.txtPort.Enable(False)
        self.txtPort.SetMinSize(wx.Size(-1, -1))

        self.chcHostPort = wx.Choice(choices=[],
              id=wxID_VTNETSECLOGINDIALOGCHCHOSTPORT, name=u'chcHostPort',
              parent=self, pos=wx.Point(155, 4), size=wx.Size(94, 21), style=0)
        self.chcHostPort.Show(False)
        self.chcHostPort.SetMinSize(wx.Size(-1, -1))
        self.chcHostPort.Bind(wx.EVT_CHOICE, self.OnChcHostPortChoice,
              id=wxID_VTNETSECLOGINDIALOGCHCHOSTPORT)

        self.lblAppl = wx.StaticText(id=wxID_VTNETSECLOGINDIALOGLBLAPPL,
              label=_(u'Application'), name=u'lblAppl', parent=self,
              pos=wx.Point(4, 37), size=wx.Size(82, 21), style=wx.ALIGN_RIGHT)
        self.lblAppl.SetMinSize(wx.Size(-1, -1))

        self.txtAppl = wx.TextCtrl(id=wxID_VTNETSECLOGINDIALOGTXTAPPL,
              name=u'txtAppl', parent=self, pos=wx.Point(90, 37),
              size=wx.Size(160, 21), style=0, value=u'')
        self.txtAppl.SetMinSize(wx.Size(-1, -1))

        self.lblAlias = wx.StaticText(id=wxID_VTNETSECLOGINDIALOGLBLALIAS,
              label=_(u'Alias'), name=u'lblAlias', parent=self, pos=wx.Point(4,
              62), size=wx.Size(82, 26), style=wx.ALIGN_RIGHT)
        self.lblAlias.SetMinSize(wx.Size(-1, -1))

        self.txtAlias = wx.TextCtrl(id=wxID_VTNETSECLOGINDIALOGTXTALIAS,
              name=u'txtAlias', parent=self, pos=wx.Point(90, 62),
              size=wx.Size(160, 26), style=0, value=u'')
        self.txtAlias.SetMinSize(wx.Size(-1, -1))

        self.cbBrowse = wx.lib.buttons.GenBitmapButton(ID=wxID_VTNETSECLOGINDIALOGCBBROWSE,
              bitmap=vtArt.getBitmap(vtArt.Browse), name=u'cbBrowse', parent=self,
              pos=wx.Point(258, 58), size=wx.Size(31, 30), style=0)
        self.cbBrowse.Bind(wx.EVT_BUTTON, self.OnCbBrowseButton,
              id=wxID_VTNETSECLOGINDIALOGCBBROWSE)

        self.chcAll = wx.CheckBox(id=wxID_VTNETSECLOGINDIALOGCHCALL,
              label=_(u'all'), name=u'chcAll', parent=self, pos=wx.Point(12, 92),
              size=wx.Size(74, 21), style=0)
        self.chcAll.SetValue(False)
        self.chcAll.SetMinSize(wx.Size(-1, -1))

        self.chcAlias = wx.Choice(choices=[],
              id=wxID_VTNETSECLOGINDIALOGCHCALIAS, name=u'chcAlias',
              parent=self, pos=wx.Point(90, 92), size=wx.Size(160, 21),
              style=0)
        self.chcAlias.SetMinSize(wx.Size(-1, -1))
        self.chcAlias.Bind(wx.EVT_CHOICE, self.OnChcAliasChoice,
              id=wxID_VTNETSECLOGINDIALOGCHCALIAS)

        self.txtUsr = wx.TextCtrl(id=wxID_VTNETSECLOGINDIALOGTXTUSR,
              name=u'txtUsr', parent=self, pos=wx.Point(90, 129),
              size=wx.Size(160, 21), style= wx.TE_PROCESS_ENTER, value=u'')
        self.txtUsr.SetMinSize(wx.Size(-1, -1))
        self.txtUsr.Bind(wx.EVT_TEXT_ENTER, self.OnTxtUsrTextEnter,
              id=wxID_VTNETSECLOGINDIALOGTXTUSR)

        self.txtPasswd = wx.TextCtrl(id=wxID_VTNETSECLOGINDIALOGTXTPASSWD,
              name=u'txtPasswd', parent=self, pos=wx.Point(90, 154),
              size=wx.Size(160, 21), style=wx.TE_PASSWORD | wx.TE_PROCESS_ENTER, value=u'')
        self.txtPasswd.SetMinSize(wx.Size(-1, -1))
        self.txtPasswd.Bind(wx.EVT_TEXT_ENTER, self.OnTxtPasswdTextEnter,
              id=wxID_VTNETSECLOGINDIALOGTXTPASSWD)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTNETSECLOGINDIALOGCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'), name=u'cbApply',
              parent=self, pos=wx.Point(35, 248), size=wx.Size(76, 30),
              style=0)
        self.cbApply.SetMinSize(wx.Size(-1, -1))
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VTNETSECLOGINDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTNETSECLOGINDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(143, 248),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.SetMinSize(wx.Size(-1, -1))
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VTNETSECLOGINDIALOGCBCANCEL)

        self.stbResult = wx.StaticBitmap(bitmap=vtArt.getBitmap(vtArt.Invisible),
              id=wxID_VTNETSECLOGINDIALOGSTBRESULT, name=u'stbResult',
              parent=self, pos=wx.Point(0, 179), size=wx.Size(16, 16), style=0)

        self.lblResult = wx.StaticText(id=wxID_VTNETSECLOGINDIALOGLBLRESULT,
              label=u'', name=u'lblResult', parent=self, pos=wx.Point(4, 195),
              size=wx.Size(246, 41), style=0)
        self.lblResult.SetMinSize(wx.Size(-1, -1))

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        self._init_ctrls(parent)
        
        try:
            self.sOrigin=vtLog.vtLngGetRelevantNamesWX(self)
        except:
            vtLog.vtLngTB(self.GetName())
            self.sOrigin=self.__class__.__name__
        try:
            icon = wx.EmptyIcon()
            icon.CopyFromBitmap(vtNetArt.getBitmap(vtNetArt.Login))
            self.SetIcon(icon)
        except:
            vtLog.vtLngTB(self.sOrigin)
        self.usrId=-1
        self.grpIds=[]
        self.grps=[]
        self.bAdmin=False
        self.bBrowse=False
        self.qMsg=Queue.Queue()
        self.qBrowse=Queue.Queue()
        self.cltServ=None
        self.cltSock=None
        self.timer=None
        self.bActive=False
        self.bCheckOk=False
        self.lAliasUsed=[]
        self.fgsData.Fit(self)
        self.dBmp={}
    def __del__old(self):
        vtLog.CallStack('')
        print self.cltServ
        print self.cltSock
        print self.bActive
        print self.bCheckOk
        #print self.IsShown(),self.IsModal()
        del self.dBmp
    def GetOrigin(self):
        return self.sOrigin
    def SetAliasUsed(self,lst):
        self.lAliasUsed=lst
    def BindEvents(self,funcApply=None,funcCancel=None):
        if funcApply is not None:
            EVT_VTNETSEC_LOGIN_DIALOG_APPLY(self,funcApply)
        if funcCancel is not None:
            EVT_VTNETSEC_LOGIN_DIALOG_CANCEL(self,funcCancel)
    def GetHostAliasUsed(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'FIXME'%(),self)
            wx.MutexGuiEnter()
            try:
                host=self.txtHost.GetValue()
            except:
                pass
            wx.MutexGuiLeave()
        else:
            host=self.txtHost.GetValue()
        iLen=len(host)
        l=[]
        for s in self.lAliasUsed:
            try:
                if s[:iLen]==host:
                    i=s.find('//')
                    j=s.find('@',i)
                    if j>=0:
                        i=j+1
                    else:
                        i+=1
                    if i>0:
                        l.append(s[i:])
            except:
                pass
        return l
    def SetHostAlias(self,l,sFind):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'l:%s;sFind:%s'%(vtLog.pformat(l),sFind),self)
        iFind=-1
        try:
            self.chcHostPort.Clear()
            i=0
            if sFind is not None:
                iLen=len(sFind)
                iPort=sFind.find(':')       # 070819:wro dont care about port
                if iPort>0:
                    iLen=iPort
            else:
                iLen=-1
            for s in l:
                if iFind<0 and iLen>0:
                    try:
                        if s[:iLen]==sFind[:iLen]:
                            iFind=i
                    except:
                        pass
                self.chcHostPort.Append(s)
                i+=1
        except:
            pass
        if iFind<0:
            i=0
            if len(l)>0:            # 070819:wro use first connection
                iFind=0
        else:
            i=iFind
        self.chcHostPort.SetSelection(i)
        self.OnChcHostPortChoice(None)
        if iFind>=0:
            return l[iFind]
        return None
    def SetValues(self,host,port,appl,alias,usr,passwd,bAdd=True,bMaster=True):
        self.txtHost.SetValue(host)
        self.txtPort.SetValue(str(port))
        self.txtAppl.SetValue(appl)
        try:
            self.txtAlias.SetValue(alias)
        except:
            try:
                s=vtNetSecLoginHost.setHostAliasStr(host,port,alias)
                self.chcHostPort.SetStringSelection(s)
            except:
                self.chcHostPort.SetSelection(0)
            self.OnChcHostPortChoice(None)
        self.txtHost.Enable(bAdd)
        self.txtPort.Enable(bAdd)
        self.txtAppl.Enable(bMaster and len(appl)==0)
        self.txtAlias.Enable(bMaster)
        self.txtHost.Show(bMaster)
        self.txtPort.Show(bMaster)
        self.chcHostPort.Show(not bMaster)
        
        if bAdd:
            self.stbResult.SetBitmap(vtArt.getBitmap(vtArt.Question))
        else:
            if len(passwd)==0:
                self.stbResult.SetBitmap(vtArt.getBitmap(vtArt.SecUnlocked))
            else:
                self.stbResult.SetBitmap(vtArt.getBitmap(vtArt.SecLocked))
        self.txtUsr.SetValue(usr)
        self.passwd=passwd
        self.iSecLv=0
        self.grpIds=[]
        self.grps=[]
        self.bAdmin=False
        self.usrId=-3
    
    def GetHost(self):
        return self.txtHost.GetValue()
    def GetPort(self):
        return self.txtPort.GetValue()
    def GetAppl(self):
        return self.txtAppl.GetValue()
    def GetAlias(self):
        return self.txtAlias.GetValue()
    def GetUsr(self):
        return self.txtUsr.GetValue()
    def GetSecLv(self):
        return str(self.iSecLv)
    def GetUsrId(self):
        return '%08d'%self.usrId#str(self.usrId)
    def GetGrpIds(self):
        return self.grpIds
    def GetGrps(self):
        return self.grps
    def IsAdmin(self):
        return self.bAdmin
    def GetPasswd(self):
        passwd=self.txtPasswd.GetValue()
        if len(passwd)>0:
            m=sha.new()
            m.update(passwd)
            return m.hexdigest()
        else:
            return ''
    def __performCheck__(self,bBrowse=False):
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            self.bActive=True
            self.bCheckOk=False
            self.bBrowse=bBrowse
            self.bBrowseAll=self.chcAll.GetValue()
            
            self.cltServ=vtNetClt(self,self.GetHost(),
                int(self.GetPort()),vtNetXmlCltSock,bNotifyConnectImmediatly=False)
            self.cltServ.Connect()
            self.timer = wx.PyTimer(self.__CheckQueue__)
            self.timer.Start(250)
            self.cbApply.Enable(False)
            #self.Notify()
        except:
            vtLog.vtLngTB(self.GetName())
        
    def NotifyConnect(self):
        self.qMsg.put('conn')
    def NotifyConnectionFault(self):
        self.qMsg.put('connFlt')
    def NotifyFault(self):
        self.qMsg.put('fault')
    def NotifyConnected(self):
        self.qMsg.put('connOk')
    def __setConnectionInfo__(self):
        self.cltSock=self.cltServ.GetSocketInstance()
        if self.cltSock is None:
            return
        if self.bBrowse:
            self.cltSock.SetParent(self)
            self.cltSock.Browse(self.GetAppl())
        else:
            appl=self.GetAppl()
            alias=self.GetAlias()
            usr=self.GetUsr()
            passwd=self.GetPasswd()
            self.cltSock.SetParent(self)
            self.cltSock.LoginEncoded(usr,passwd)
            self.cltSock.Alias(appl,alias)
    def NotifyServed(self,flag):
        if flag==False:
            #self.__close__()
            self.qMsg.put('servedFlt')
    def NotifyLogin(self):
        self.qMsg.put('login')
    def NotifyLoggedIn(self,o):
        self.usr=o.GetUsr()
        self.usrId=o.GetUsrId()
        self.iSecLv=o.GetSecLv()
        self.bAdmin=o.IsAdmin()
        self.grpIds=o.GetGrpIds()
        self.grps=o.GetGrps()
        self.qMsg.put('loggedIn')
    def NotifyBrowse(self,lst):
        lst.sort()
        CallBack(self.doBrowseShow,lst)
    def doBrowseShow(self,lst):
        l=self.GetHostAliasUsed()
        for s in lst:
            if self.bBrowseAll==False:
                try:
                    l.index(s)
                    continue
                except:
                    pass
            self.qBrowse.put(s)
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'FIXME'%(),self)
            wx.MutexGuiEnter()
            try:
                self.__close__()
            except:
                pass
            wx.MutexGuiLeave()
        else:
            self.__close__()
    def __delConnectionObjs__(self):
        if self.cltSock is not None:
            self.cltSock.SetParent(None)
            del self.cltSock
        self.cltSock=None
        if self.cltServ is not None:
            del self.cltServ
        self.cltServ=None
        self.host=None
        self.port=None
    def __connectionClosed__(self,bEstablished=False):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        #import pdb
        #pdb.set_trace()
        #del self.cltServ
        #self.__delConnectionObjs__()
        self.qMsg.put('closed')
        #self.bActive=False
        #self.timer.Stop()
        #while self.__CheckQueue__():
        #    pass
    def __close__(self,bExit=False):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if self.cltSock is not None:
            self.cltSock.Close()
            #self.cltSock=None
        else:
            self.bActive=False
        if self.cltServ is not None:
            self.cltServ.Stop()
        else:
            self.bActive=False
        #self.cltServ=None
        #self.bActive=False
        if bExit:
            try:
                if self.timer is not None:
                    self.timer.Stop()
            except:
                pass
            #if self.bCheckOk:
            #    self.EndModal(1)
            try:
                while 1:
                    s=self.qBrowse.get(0)
                    bBrowse=True
            except:
                pass
            self.chcAlias.Clear()
            try:
                while self.qMsg.get(0):
                    pass
            except:
                pass
    def __endDialog__(self,bApply):
        if bApply:
            wx.PostEvent(self,vtNetSecLoginDialogApply(self))
        else:
            wx.PostEvent(self,vtNetSecLoginDialogCancel(self))
        if self.IsModal():
            if bApply:
                self.EndModal(1)
            else:
                self.EndModal(0)
        else:
            self.Show(False)
    def __getBmp__(self,sMsg):
        #if name in self.dBmp:
        if sMsg in self.dBmp:
            return self.dBmp[sMsg]
        else:
            if sMsg=='conn':
                img=vtNetArt.getBitmap(vtNetArt.Connect)
            elif sMsg=='connFlt':
                img=vtNetArt.getBitmap(vtNetArt.Fault)
            elif sMsg=='connOk':
                img=vtNetArt.getBitmap(vtNetArt.Login)
            elif sMsg=='fault':
                img=vtNetArt.getBitmap(vtNetArt.Fault)
            elif sMsg=='serveFlt':
                img=vtNetArt.getBitmap(vtNetArt.Stopped)
            elif sMsg=='login':
                img=vtNetArt.getBitmap(vtNetArt.Login)
            elif sMsg=='loggedIn':
                img=vtNetArt.getBitmap(vtNetArt.Running)
            elif sMsg=='closed':
                img=vtNetArt.getBitmap(vtNetArt.Stopped)
            elif sMsg=='ok':
                img=vtArt.getBitmap(vtArt.Apply)
            else:
                img=vtArt.getBitmap(vtArt.Error)
            self.dBmp[sMsg]=img
            return img
    def __CheckQueue__(self):
        try:
            while self.qMsg.empty()==False:
                sMsg=self.qMsg.get(False)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'sMsg:%s'%(sMsg),self)
                if sMsg=='conn':
                    img=self.__getBmp__(sMsg)
                    self.lblResult.SetLabel(_('connecting'))
                elif sMsg=='connFlt':
                    img=self.__getBmp__(sMsg)
                    self.lblResult.SetLabel(_('connection fault'))
                    
                    self.cbApply.Enable(True)
                elif sMsg=='connOk':
                    img=self.__getBmp__(sMsg)
                    self.lblResult.SetLabel(_('connection established'))
                    self.__setConnectionInfo__()
                elif sMsg=='fault':
                    img=self.__getBmp__(sMsg)
                    self.lblResult.SetLabel(_('fault'))
                    self.cbApply.Enable(True)
                elif sMsg=='serveFlt':
                    img=self.__getBmp__(sMsg)
                    self.lblResult.SetLabel(_('serve fault'))
                    self.__close__()
                    self.cbApply.Enable(True)
                elif sMsg=='login':
                    img=self.__getBmp__(sMsg)
                    self.lblResult.SetLabel(_('login'))
                elif sMsg=='loggedIn':
                    img=self.__getBmp__(sMsg)
                    self.lblResult.SetLabel(_('logged in'))
                    self.bCheckOk=True
                    self.__close__()
                elif sMsg=='closed':
                    img=self.__getBmp__(sMsg)
                    self.lblResult.SetLabel(_('connection closed'))
                    bBrowse=False
                    try:
                        while 1:
                            s=self.qBrowse.get(0)
                            bBrowse=True
                            self.chcAlias.Append('%s'%s)
                    except:
                        pass
                    if bBrowse:
                        self.chcAlias.SetSelection(0)
                    self.bActive=False
                    if self.timer is not None:
                        self.timer.Stop()
                        self.timer=None
                    try:
                        while self.qMsg.get(0):
                            pass
                    except:
                        pass
                    try:
                        self.__delConnectionObjs__()
                    except:
                        pass
                    if self.bCheckOk:
                        self.chcAlias.Clear()
                        #wx.CallAfter(self.EndModal,1)
                        wx.CallAfter(self.__endDialog__,True)
                    self.cbApply.Enable(True)
                else:
                    img=self.__getBmp__(sMsg)#vtArt.getBitmap(vtArt.Error)
                    self.lblResult.SetLabel(_('unknown'))
                if self.bCheckOk:
                    img=self.__getBmp__('ok')#vtArt.getBitmap(vtArt.Apply)
                self.stbResult.SetBitmap(img)
                self.stbResult.Refresh()
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'exit normally',self)
            return True
        except:
            vtLog.vtLngTB(self.GetName())
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'exit by exception',self)
            return False
    def OnCbApplyButton(self, event):
        if event is not None:
            event.Skip()
        # perform check
        #self.EndModal(1)
        self.__performCheck__()
    def OnCbCancelButton(self, event):
        event.Skip()
        self.__close__(bExit=True)
        if self.bActive:
            return
        #self.EndModal(0)
        wx.CallAfter(self.__endDialog__,False)

    def OnChcHostPortChoice(self, event):
        s=self.chcHostPort.GetStringSelection()
        sHost,sPort,sAlias=vtNetSecLoginHost.setHostAliasStr(s)
        
        self.txtHost.SetValue(sHost)
        self.txtPort.SetValue(sPort)
        self.txtAlias.SetValue(sAlias)
        if event is not None:
            event.Skip()

    def OnCbBrowseButton(self, event):
        self.chcAlias.Clear()
        self.__performCheck__(True)
        event.Skip()

    def OnChcAliasChoice(self, event):
        self.txtAlias.SetValue(self.chcAlias.GetStringSelection())
        event.Skip()
    def OnTxtUsrTextEnter(self, event):
        event.Skip()
        self.txtPasswd.SetFocus()
    def OnTxtPasswdTextEnter(self, event):
        event.Skip()
        self.OnCbApplyButton(None)
    def Show(self,flag=True):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
        wx.Dialog.Show(self,flag)
    def ShowModal(self):
        if vtLog.vtLngIsLogged(vtLog.ERROR):
            vtLog.vtLngCurWX(vtLog.ERROR,''%(),self)
        ret=wx.Dialog.ShowModal(self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'ret:%d'%(ret),self)
        return ret
