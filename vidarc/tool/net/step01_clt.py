#----------------------------------------------------------------------------
# Name:         setp01_clt.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: step01_clt.py,v 1.1 2005/12/11 23:04:49 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import thread
import xmlrpclib
import time
def genRequest(s,max):
    zStart=time.clock()
    srv=xmlrpclib.ServerProxy("http://localhost:8000")
    i=0
    while i<max:
        zRound=time.clock()
        print srv.getServerStatus()
        print srv.replyMsg(s+' %04d '%i+'- %04d'%max),time.clock()-zRound
        i+=1
    print s,time.clock()-zStart
def genRequest2(s,max):
    zStart=time.clock()
    srv=xmlrpclib.ServerProxy("http://localhost:8000")
    i=0
    while i<max:
        zRound=time.clock()
        srv.replyMsg(s+' %04d '%i+'- %04d'%max)
        srv.replyMsg(s+' %04d '%i+'- %04d'%max)
        print time.clock()-zRound
        i+=1
    print s,time.clock()-zStart
def genRequest3(s,max):
    zStart=time.clock()
    srv=xmlrpclib.ServerProxy("http://localhost:8000")
    i=0
    while i<max:
        zRound=time.clock()
        print srv.replyMsg(s+' %04d '%i+'- %04d'%max),time.clock()-zRound
        i+=1
    print s,time.clock()-zStart
def genRequest4(s,max):
    zStart=time.clock()
    srv=xmlrpclib.ServerProxy("http://localhost:8000")
    i=0
    while i<max:
        zRound=time.clock()
        res=srv.replyMsg(s+' %04d '%i+'- %04d'%max)
        zRoundS=time.clock()
        print res,zRoundS-zRound
        time.sleep(1)
        i+=1
    print s,time.clock()-zStart
    
thread.start_new_thread(genRequest, ('clt01a',10))
thread.start_new_thread(genRequest, ('clt01b',10))
thread.start_new_thread(genRequest2, ('clt01c',20))
thread.start_new_thread(genRequest2, ('clt01d',100))
thread.start_new_thread(genRequest3, ('clt01e',30))
thread.start_new_thread(genRequest4, ('clt01f',30))

