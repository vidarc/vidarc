#Boa:Dialog:vNetXmlGuiMasterCloseDialog
#----------------------------------------------------------------------------
# Name:         vNetXmlGuiMasterCloseDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vNetXmlGuiMasterCloseDialog.py,v 1.15 2008/03/24 11:07:26 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

import time,types
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.vtThread import vtThread

from vidarc.tool.net.vNetXmlWxGui import vNetXmlWxGui

def create(parent):
    return vNetXmlGuiMasterCloseDialog(parent)

[wxID_VNETXMLGUIMASTERCLOSEDIALOG, wxID_VNETXMLGUIMASTERCLOSEDIALOGLBLMSG, 
 wxID_VNETXMLGUIMASTERCLOSEDIALOGLISTBOX1, 
 wxID_VNETXMLGUIMASTERCLOSEDIALOGPBPROCESS, 
] = [wx.NewId() for _init_ctrls in range(4)]

class thdStopMasterDialog(vtThread):
    def DoStop(self,dictNet,func,*args,**kwargs):
        if self.IsRunning():
            vtLog.vtLngCur(vtLog.CRITICAL,
                    'thread is already engaged',self.GetOrigin())
            return
        for netDoc in dictNet.values():
            try:
                netDoc.DisConnect()
            except:
                vtLog.vtLngTB(self.GetOrigin())
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        vtLog.vtLngCur(vtLog.INFO,'starting ...',self.GetOrigin())
        #vtLog.vtLngCur(vtLog.ERROR,'starting ...',self.GetOrigin())
        self.Do(self.Run,dictNet,func,*args,**kwargs)
    def Run(self,dictNet,func,*args,**kwargs):
        vtLog.vtLngCur(vtLog.INFO,'',self.GetOrigin())
        try:
            lstPrev=[]
            lstAct=[]
            for netDoc in dictNet.values():
                tup=[True,True]#[netDoc.IsRunning(),netDoc.IsConnActive()]
                lstPrev.append(tup)
            bSomeActive=True
            iTime=0.0
            while bSomeActive:
                bSomeActive=False
                i=0
                for netDoc in dictNet.values():
                    netDoc.__logStatus__()
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,
                                'appl:%s check'%(netDoc.GetAppl()),
                                self.GetOrigin())
                    bRunning=netDoc.IsRunning()
                    if netDoc.IsStopping():
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCur(vtLog.DEBUG,'appl:%s stopping bRunning:%d'%(
                                            netDoc.GetAppl(),bRunning),self.GetOrigin())
                        bRunning=True
                    if bRunning==False:
                        if lstPrev[i][0]:
                            vtLog.vtLngCur(vtLog.INFO,
                                    'appl:%s stopped'%(netDoc.GetAppl()),
                                    self.GetOrigin())
                            self.Post('proc',netDoc.appl,0)
                            #wx.MutexGuiEnter()
                            try:
                                netDoc.DelConsumer()
                            except:
                                pass
                            try:
                                netDoc.DelConsumerLang()
                            except:
                                pass
                            #wx.MutexGuiLeave()
                            lstPrev[i][0]=False
                    else:
                        bSomeActive=True
                    bConnAct=netDoc.IsConnActive()
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'appl:%s bConnAct:%d'%(
                                        netDoc.GetAppl(),bConnAct),
                                        self.GetOrigin())
                    if bConnAct==False:
                        if lstPrev[i][1]:
                            vtLog.vtLngCur(vtLog.INFO,
                                    'appl:%s connection closed'%(netDoc.GetAppl()),
                                    self.GetOrigin())
                            self.Post('proc',netDoc.appl,1)
                            #wx.MutexGuiEnter()
                            #try:
                            #    self.par.ConnectionClosed(netDoc)
                            #except:
                            #    pass
                            #wx.MutexGuiLeave()
                            vtLog.vtLngCur(vtLog.INFO,
                                    'appl:%s connection closed'%(netDoc.GetAppl()),
                                    self.GetOrigin())
                            lstPrev[i][1]=False
                    else:
                        bSomeActive=True
                    i+=1
                time.sleep(0.25)
                iTime+=0.25
                if iTime>60.0:
                    vtLog.vtLngCur(vtLog.CRITICAL,'time out',self.GetOrigin())
                    try:
                        for netDoc in dictNet.values():
                            vtLog.vtLngCur(vtLog.ERROR,'%d;%d;%d'%(netDoc.__isRunning__(),
                                    vNetXmlWxGui.__isRunning__(netDoc),
                                    netDoc.IsRunning()),netDoc.GetOrigin())
                            vtLog.vtLngCur(vtLog.ERROR,netDoc.oSF.GetStrFull(u';'),origin=self.GetOrigin())
                            if netDoc.thdSynch.IsRunning():
                                vtLog.vtLngCur(vtLog.ERROR,'thdSynch running',self.GetOrigin())
                            if netDoc.cltSrv is not None:
                                if netDoc.cltSrv.IsServing():
                                    vtLog.vtLngCur(vtLog.ERROR,'cltSrv serving',self.GetOrigin())
                            if netDoc.IsConnActive():
                                if netDoc.oSF.IsDisConnecting==False:
                                    vtLog.vtLngCur(vtLog.ERROR,'connection is still active',self.GetOrigin())
                    except:
                        vtLog.vtLngTB(self.GetOrigin())
                    break
                self.Post('proc',iTime,60.0)
                #wx.MutexGuiEnter()
                #try:
                #    self.par.Process(iTime)
                #except:
                #    pass
                #wx.MutexGuiLeave()
            
            vtLog.vtLngCur(vtLog.INFO,'clean up',self.GetOrigin())
            vtLog.vtLngCur(vtLog.INFO,'cleaned up',self.GetOrigin())
            self.Post('proc',-1,-1)
            #wx.MutexGuiEnter()
            #try:
            #    if func is not None:
            #        func(*args,**kwargs)
            #except:
            #    vtLog.vtLngTB(self.GetOrigin())
            #wx.MutexGuiLeave()
        except:
            vtLog.vtLngTB(self.GetOrigin())

class vNetXmlGuiMasterCloseDialog(wx.Dialog):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblMsg, 1, border=4,
              flag=wx.RIGHT | wx.TOP | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.listBox1, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.pbProcess, 1, border=4,
              flag=wx.ALIGN_CENTER | wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.TOP)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VNETXMLGUIMASTERCLOSEDIALOG,
              name=u'vNetXmlGuiMasterCloseDialog', parent=prnt,
              pos=wx.Point(304, 74), size=wx.Size(359, 174),
              style=wx.CAPTION,
              title=_(u'vNet XML Master Close Dialog'))
        self.SetClientSize(wx.Size(351, 147))

        self.lblMsg = wx.StaticText(id=wxID_VNETXMLGUIMASTERCLOSEDIALOGLBLMSG,
              label=_(u'stopping thread and closing connections please wait ...'),
              name=u'lblMsg', parent=self, pos=wx.Point(4, 4), size=wx.Size(343,
              13), style=0)
        self.lblMsg.SetMinSize(wx.Size(-1, -1))

        self.listBox1 = wx.ListBox(choices=[],
              id=wxID_VNETXMLGUIMASTERCLOSEDIALOGLISTBOX1, name='listBox1',
              parent=self, pos=wx.Point(4, 21), size=wx.Size(343, 105),
              style=0)
        self.listBox1.SetMinSize(wx.Size(-1, -1))

        self.pbProcess = wx.Gauge(id=wxID_VNETXMLGUIMASTERCLOSEDIALOGPBPROCESS,
              name=u'pbProcess', parent=self, pos=wx.Point(49, 130), range=1000,
              size=wx.Size(252, 13), style=wx.GA_HORIZONTAL | wx.GA_SMOOTH)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        self._init_ctrls(parent)
        self.dictNet={}
        self.thdStop=thdStopMasterDialog(self,bPost=True)
        self.thdStop.BindEvents(funcProc=self.OnProc)
    def SetDictNet(self,dict):
        self.dictNet=dict
    def ShowModal(self):
        self.thdStop.DoStop(self.dictNet,self.OnFin)
        wx.Dialog.ShowModal(self)
    def Show(self,bFlag):
        if bFlag:
            self.thdStop.DoStop(self.dictNet,self.OnFin)
        wx.Dialog.Show(self,bFlag)
    def OnProc(self,evt):
        #evt.Skip()
        if type(evt.GetAct())==types.FloatType:
            iVal=int((evt.GetAct()/evt.GetCount())*1000)
            self.Process(iVal)
        else:
            if evt.GetCount()==0:
                msg=evt.GetAct()+u' '+_(u'thread stopped')
            elif evt.GetCount()==1:
                msg=evt.GetAct()+u' '+_(u'connection closed')
            elif evt.GetCount()==-1 and evt.GetAct()==-1:
                self.thdStop.SetPostWid(None)
                self.Close()
                msg=None
            else:
                msg=None
            if msg is not None:
                self.listBox1.Insert(msg,0)
    def ThreadStopped(self,netDoc):
        msg=netDoc.appl+u' '+_(u'thread stopped')
        self.listBox1.Insert(msg,0)
        pass
    def ConnectionClosed(self,netDoc):
        msg=netDoc.appl+u' '+_(u'connection closed')
        self.listBox1.Insert(msg,0)
        pass
    def Process(self,iTime):
        self.pbProcess.SetValue(int(iTime))
    def OnFin(self):
        wx.CallAfter(self.Close)
        #from vidarc.tool.net.vtNetSecXmlGuiMaster import vtnxSecXmlMasterShutdown
        #wx.PostEvent(self.GetParent(),vtnxSecXmlMasterShutdown(self.GetParent()))
