#----------------------------------------------------------------------------
# Name:         vNetXmlWxGuiReg.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060505
# CVS-ID:       $Id: vNetXmlWxGuiReg.py,v 1.1 2006/07/17 11:05:22 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.net.vNetXmlWxGui import *

import vidarc.tool.log.vtLog as vtLog

class vNetXmlWxGuiReg(vNetXmlWxGui):
    VERBOSE=1
    def __init__(self,parent,appl,pos=(0,0),size=(16,16),synch=False,small=True,verbose=0):
        vNetXmlWxGui.__init__(self,parent=parent,appl=appl,pos=pos,size=size,
                synch=synch,small=small,verbose=verbose)
        self.lCfgIds=[]
    def __setupNodes2Monitor__(self):
        if self.VERBOSE:
            vtLog.CallStack('')
        vtLog.vtLngCur(vtLog.DEBUG,'',self.appl)
        pass
