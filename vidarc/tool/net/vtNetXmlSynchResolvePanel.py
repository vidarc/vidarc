#----------------------------------------------------------------------------
# Name:         vtNetXmlSynchResolvePanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20070831
# CVS-ID:       $Id: vtNetXmlSynchResolvePanel.py,v 1.1 2007/10/02 03:55:16 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

from vidarc.tool.misc.vtmCompareResults import vtmCompareResults
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

class vtNetXmlSynchResolvePanel(vtmCompareResults):
    def __init__New(self, parent, id, pos, size, style, name):
        vtmCompareResults.__init__(self, parent, id, pos, size, style, name)
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        self.bxsBt.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        self.cbSynch = wx.lib.buttons.GenBitmapButton(ID=-1,
              bitmap=vtArt.getBitmap(vtArt.Synch), name=u'cbSynch',
              parent=self, pos=wx.Point(259, 112), size=wx.Size(31, 30),
              style=0)
        self.cbSynch.SetMinSize(wx.Size(-1, -1))
        self.cbSynch.Bind(wx.EVT_BUTTON, self.OnCbSynchButton)
        self.bxsBt.AddWindow(self.cbSynch, 0, border=4, flag=wx.TOP | wx.EXPAND)
    def UpdateResult(self):
        vtmCompareResults.UpdateResult(self)
        for ti in self.dTi.itervalues():
            self.trlstCmp.Expand(ti)

    def __setAction__(self,iAction):
        self.trlstCmp.Freeze()
        try:
            dRes=self.oCmp.GetResultDict()
            lTi=self.trlstCmp.GetSelections()
            for ti in lTi:
                sKey=self.trlstCmp.GetItemText(ti)
                t=dRes[sKey]
                if len(t)>2:
                    t[2]=iAction
                iMain,iImgNr,l=self.oCmp.ConvertResultItem4Disp(sKey,t)
                if l is not None:
                    i=1
                    for s in l:
                        self.trlstCmp.SetItemText(ti,s,i)
                        i+=1
                d=self.imgDict['resultsItems']
                try:
                    img=d[iAction]
                except:
                    img=self.imgDict['empty']
                self.trlstCmp.SetItemImage(ti,img,column=1,
                        which = wx.TreeItemIcon_Normal)
        except:
            vtLog.vtLngTB(self.GetName())
        self.trlstCmp.Thaw()
    def OnCbSynchButton(self,evt):
        evt.Skip()
        vtLog.vtLngCurWX(vtLog.INFO,''%(),self)
        vtLog.CallStack('')
        self.Synch()
    def Synch(self):
        vtLog.vtLngCurWX(vtLog.INFO,''%(),self)
        vtLog.CallStack('')
        
