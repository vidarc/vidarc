#----------------------------------------------------------------------------
# Name:         vNetFileSrv.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060627
# CVS-ID:       $Id: vtNetFileSrv.py,v 1.25 2012/04/08 10:40:19 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os,os.path,stat
#import thread,threading
import time,random
import socket,sha,binascii
import fnmatch,traceback
import sys,string,re
from cStringIO import StringIO
import tempfile

from vidarc.tool.InOut.listFilesXml import DirEntryXml
import vidarc.tool.InOut.fnUtil as fnUtil
from vidarc.tool.InOut.vtInOutFileTimes import setFileTimes
#from vidarc.tool.vtThread import vtThread
from vidarc.tool.vtThreadCore import vtThreadCore
from vidarc.tool.vtStateFlagSimple import vtStateFlagSimple
from vidarc.tool.log import vtLog
#from vidarc.tool.net import vtNetSock
from vidarc.tool.sock.vtSock import vtSock
from vidarc.tool.net.vtNetSrv import *
from vidarc.tool.xml.vtXmlDom import vtXmlDom
from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer
from vidarc.tool.sec.vtSecXmlLogin import vtSecXmlLogin
from vidarc.tool.net.vtNetXmlLock import *
from vidarc.tool.sec.vtSecCryptoFile import vtSecCryptoFile
import vidarc.tool.sec.vtSecFileAcl as vtSecFileAcl

import identify as vIdentify
mid=vIdentify.getMachineID()

BUFFER_MAX=40*1024*1024
from wx import PyTimer

import __init__
if __init__.SSL:
    from OpenSSL import SSL
    from OpenSSL import tsafe
    def verify_cb(conn, cert, errnum, depth, ok):
        try:
            self=conn.get_app_data()
            self.DoVerify(conn,cert,errnum,depth,ok)
        except:
            return False
        return ok

class vtNetFileSock(vtSock):
    FILE_INFOS=['NAME','SIZE','LAST_ACCESS','MOD_ACCESS','STAT_ACCESS','MD5']
    #def __init__(self,server,conn,adr,sID=''):
    #    vtSock.__init__(self,server,conn,adr,sID,BUFFER_MAX)
    def __initCtrl__(self,*args,**kwargs):
        vtSock.__initCtrl__(self,*args,**kwargs)
        self.loggedin=False
        self.objLogin=None
        self.verbose=0
        self.iStart=-1
        self.iAct=0
        self.iEnd=-1
        self.phrase=None
        self.pubkey=None
        self.oCryptoSrv=None
        self.oCryptoClt=None
        self.lCmds=[
            #('lock_node',self.__lock_node__),
            #('unlock_node',self.__unlock_node__),
            ('getFile',self.__getFile__),
            ('setFileStart',self.__setFileStart__),
            ('setFileProc',self.__setFileProc__),
            ('setFileFin',self.__setFileFin__),
            ('setFileAbort',self.__setFileAbort__),
            ('listContent',self.__listContent__),
            ('alias',self.__alias__),
            ('login',self.__login__),
            ('loggedin',self.__loggedin__),
            ('authenticate',self.__authenticate__),
            ('browse',self.__browse__),
            (u'sessionID',self.__sessionID__),
            (u'shutdown',self.__shutdown__)]
        self.appl_alias=''
        self.usr=None
        self.passwd=None
        self.doc=None
        self.docDoc=None
        self.dFileSet={}
        #self.SendTelegram('login')
    def __initSSL__(self,privkey=None,cert=None,verify=None,DN=None):
        #vtLog.CallStack('')
        #self.ctx=None
        return
    def DoVerify(self,conn, cert, errnum, depth, ok):
        vtLog.vtLngCur(vtLog.INFO,'ok:%d'%(ok),self.appl_alias)
        if ok:
            self.serving=True
            vtLog.vtLngCur(vtLog.INFO,'SSL got certificate: %s'%(cert.get_subject()),self.appl_alias)
        else:
            self.serving=False
            #vtLog.vtLngCur(vtLog.ERROR,'errnum:%d'%(errnum),self.appl_alias)
            vtLog.vtLngCurCls(vtLog.ERROR,'errnum:%s;depth:%s;ok:%s'%(`errnum`,`depth`,`ok`),self.appl_alias)
        if self.serving==False:
            vtLog.vtLngCur(vtLog.INFO,'SSL verify fault',self.appl_alias)
        #self.SendSessionID()
    def SocketClosed(self):
        vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        self.doc=None
        self.docDoc=None
        self.objLogin=None
        #del self.cmds
        vtSock.SocketClosed(self)
    def HandleDataOld(self):
        try:
            #iVerbose=vtLog.vtLngCallStack(self,vtLog.DEBUG,'HandleData data len:%d'%(self.len))
            if self.iStart==-1:
                self.iStart=self.data.find(self.startMarker,self.iAct)
                self.iAct=self.iStart+self.iStartMarker
            #vtLog.vtLog(None,'start:%d'%(iStart),level=1,verbose=iVerbose)
            if self.iStart>=0:
                # start marker found
                self.iEnd=self.data.find(self.endMarker,self.iAct)
                #vtLog.vtLog(None,'end  :%d'%(iEnd),level=1,verbose=iVerbose)
                if self.verbose>0:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCallStack(self,vtLog.DEBUG,
                                    'HandleData telegram found; len:%8d %08d; act:%8d;start:%8d; end:%8d'%(len(self.data),self.len,self.iAct,self.iStart,self.iEnd),
                                    origin=self.appl_alias)
                if self.iEnd>0:
                    # end marker found, telegram is complete
                    self.iStart+=self.iStartMarker
                    val=self.data[self.iStart:self.iEnd]
                    #vtLog.vtLngCallStack(self,vtLog.DEBUG,'  '+val)
                    #vtLog.vtLngCur(vtLog.DEBUG,'val:%s'%(val[:100]),self.appl_alias)
                    for cmd in self.cmds:
                        k=cmd[0]
                        iLen=len(k)
                        if val[:iLen]==k:
                            cmd[1](val[iLen:])
                            break
                    self.iEnd+=self.iEndMarker
                    #self.data=self.data[iEnd:]
                    #self.len-=iEnd
                    self.iAct=self.iEnd
                    self.iStart=self.iEnd=-1
                    if self.iAct>=self.len:
                        self.data=''
                        self.len=0
                        self.iAct=0
                        if self.verbose>0:
                            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                vtLog.vtLngCallStack(self,vtLog.DEBUG,
                                    'HandleData reduce buffer; len:%8d %8d; act:%8d;start:%8d; end:%8d'%(len(self.data),self.len,self.iAct,self.iStart,self.iEnd),
                                    origin=self.appl_alias)
                        return 0
                        #vtLog.vtLog(None,'remaining data len:%d'%(self.len),level=1,verbose=iVerbose)
                    if self.verbose>0:
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCallStack(self,vtLog.DEBUG,
                                    'HandleData; len:%8d %8d; act:%8d;start:%8d; end:%8d'%(len(self.data),self.len,self.iAct,self.iStart,self.iEnd),
                                    origin=self.appl_alias)
                    return 1
                else:
                    self.data=self.data[self.iStart:]
                    self.iAct=0
                    self.len-=self.iStart
                    self.iStart=-1
                    if self.verbose>0:
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCallStack(self,vtLog.DEBUG,
                                    'HandleData reduce buffer; len:%8d %8d; act:%8d;start:%8d; end:%8d'%(len(self.data),self.len,self.iAct,self.iStart,self.iEnd),
                                    origin=self.appl_alias)
        except:
            vtLog.vtLngTB(self.appl_alias)
        #vtLog.vtLog(None,'exit handling data',level=1,verbose=iVerbose)
        return 0
    def __checkLoginOld__(self,applAlias):
        try:
                self.objLogin,iRes=self.srv.CheckLogin(self.adr[0],applAlias,self.sessionID,self.usr,self.passwd)
                vtLog.CallStack('')
                print 'addr',self.adr
                print self.objLogin,iRes
                if iRes>0:
                    sInfo=''
                    try:
                        sInfo=self.objLogin.GetStr()
                    except:
                        sInfo=',,,,,'
                        vtLog.vtLngTB('')
                    #self.Send(self.startMarker+'loggedin|'+sInfo+self.endMarker)
                    self.SendTelegram('loggedin|'+sInfo)
                    self.loggedin=True
                    self.usr=val[1:i]
                    self.srv.release()
                    return
                #self.Send(self.startMarker+'login'+self.endMarker)
                self.SendTelegram('login')
        except:
            vtLog.vtLngCS(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
        self.srv.release()
        
        if self.loggedin:
            return 1
        else:
            #self.Send(self.startMarker+'login'+self.endMarker)
            self.SendTelegram('login')
            return 0
    def __getFile__(self,val):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'val:%s'%val[:100],origin=self.appl_alias)
        try:
            iFlt=0
            i=val.find('|')
            if i>=0:
                iFlt=1
                j=val.find(',',i+1)
                if j>0:
                    appl_alias=val[i+1:j]
                    sFN=val[j+1:]
                    self.srv.acquire()
                    iRetDoc=self.srv.IsServedDocRel(appl_alias)
                    if iRetDoc==1:
                        i=sFN.find('_')
                        sKey=None
                        if i>0:
                            j=sFN.find('_',i+1)
                            if j>0:
                                sKey=sFN[i+1:j]
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCur(vtLog.DEBUG,'fn:%s;key:%s;type:%s'%(sFN,sKey,type(sKey)),self.appl_alias)
                        if sKey is not None:
                            if sKey in self.dKey:
                                fid=self.dKey[sKey]
                                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                    vtLog.vtLngCur(vtLog.DEBUG,'key:%s;type:%s'%(fid,type(fid)),self.appl_alias)
                                #print fid,type(fid)
                                if self.doc.IsAccessOkDocGrp(fid,self.objLogin,vtSecFileAcl.ACL_FILE_MSK_READ,-1)==False:
                                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                        vtLog.vtLngCur(vtLog.DEBUG,'fid:%s;vPrjDoc false'%(fid),self.appl_alias)
                                    if self.docDoc.IsAccessOkDocGrp(fid,self.objLogin,vtSecFileAcl.ACL_FILE_MSK_READ,-1)==False:
                                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                            vtLog.vtLngCur(vtLog.DEBUG,'fid:%s;vDoc false'%(fid),self.appl_alias)
                                        self.SendTelegram('getFileStart|'+','.join([sFN]))
                                        self.SendTelegram('getFileAbort|'+','.join([sFN,'restricted']))
                                        self.srv.release()
                                        return 0
                        try:
                            d=self.srv.GetServedDN(appl_alias)
                            sFullFN=os.path.join(d['dir'],sFN)
                            f=open(sFullFN,'rb')
                            s=os.stat(sFullFN)
                        except:
                            self.SendTelegram('getFileStart|'+','.join([sFN]))
                            self.SendTelegram('getFileAbort|'+','.join([sFN,'restricted']))
                            vtLog.vtLngTB(self.appl_alias)
                            self.srv.release()
                            return 0
                        self.srv.release()
                        try:
                            iSize=s[stat.ST_SIZE]
                            iAct=0
                            m=sha.new()
                            self.SendTelegram('getFileStart|'+','.join([sFN]))
                            while iAct!=iSize:
                                blk=f.read(4096)
                                iAct=f.tell()
                                m.update(blk)
                                #self.Send(self.startMarker+'getFileProc|'+','.join([sFN,str(iAct),str(iSize),'']))
                                #self.Send(blk,True)
                                #self.Send(self.endMarker)
                                #self.SendTelegram('getFileProc|'+','.join([sFN,str(iAct),str(iSize),blk]))
                                self.SendTelegram('getFileProc|'+','.join([sFN,str(iAct),str(iSize),blk]))
                            dig=m.hexdigest()
                            self.SendTelegram('getFileFin|'+','.join([sFN,dig]))
                            f.close()
                        except:
                            vtLog.vtLngCS(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
                            f.close()
                            self.SendTelegram('getFileAbort|'+','.join([sFN,'processing fault']))
                        return
                    elif iRetDoc==0:
                        objLogin,iRes,oAcl=self.srv.CheckLogin(self.adr[0],
                                    appl_alias,self.sessionID,self.usr,
                                    self.passwd)
                        if oAcl is not None:
                            bOk=oAcl.IsAccessOk(objLogin,vtSecFileAcl.ACL_FILE_MSK_READ,0)
                        else:
                            bOk=True
                        if bOk==False:
                            self.SendTelegram('getFileStart|'+','.join([sFN]))
                            self.SendTelegram('getFileAbort|'+','.join([sFN,'restricted']))
                            self.srv.release()
                            return
                        try:
                            d=self.srv.GetServedDN(appl_alias)
                            lFN=self.srv.GetServedContent(appl_alias)
                        except:
                            vtLog.vtLngTB(self.appl_alias)
                        if sFN not in lFN:
                            vtLog.vtLngCur(vtLog.ERROR,'fn:%s not available in;alias:%s'%(sFN,appl_alias),self.appl_alias)
                            self.SendTelegram('getFileStart|'+','.join([sFN]))
                            self.SendTelegram('getFileAbort|'+','.join([sFN,'inavailable']))
                            self.srv.release()
                            return
                        try:
                            sFullFN=os.path.join(d['dir'],sFN)
                            self.srv.release()
                            if self.phrase is not None:
                                #f=os.tmpfile()
                                f=tempfile.TemporaryFile(prefix='vn')
                                self.oCryptoSrv.DecryptMM(sFullFN)
                                self.oCryptoSrv.seek(0)
                                if 0:
                                    try:
                                        fDe=open(sFullFN+'.dec.zip','wb')
                                        fDe.write(self.oCryptoSrv.read())
                                        fDe.close()
                                        self.oCryptoSrv.seek(0)
                                        import zipfile
                                        fZip=zipfile.ZipFile(self.oCryptoSrv,'r')
                                        vtLog.vtLngCur(vtLog.DEBUG,'files:%s'%(vtLog.pformat(fZip.namelist())),self.appl_alias)
                                        fZip.close()
                                        self.oCryptoSrv.seek(0)
                                    except:
                                        vtLog.vtLngTB(self.appl_alias)
                                self.oCryptoClt.EncryptFile(self.oCryptoSrv,f)
                                self.oCryptoSrv.close()
                                iSize=f.tell()
                                f.seek(0)
                                if 0:
                                    try:
                                        fClt=open(sFullFN+'.clt.enc','wb')
                                        fClt.write(f.read())
                                    except:
                                        pass
                                    f.seek(0)
                                if 0:
                                    fDeClt=open(sFullFN+'.clt.dec.zip','wb')
                                    self.oCryptoClt.DecryptFile(f,fDeClt)
                                    fDeClt.close()
                                    f.seek(0)
                            else:
                                f=open(sFullFN,'rb')
                                s=os.stat(sFullFN)
                                iSize=s[stat.ST_SIZE]
                            iAct=0
                            m=sha.new()
                            self.SendTelegram('getFileStart|'+','.join([sFN]))
                            while iAct!=iSize:
                                blk=f.read(4096)
                                iAct=f.tell()
                                m.update(blk)
                                #vtLog.vtLngCur(vtLog.DEBUG,'fn:%s; act:%08d;size:%08d;sha:%s'%(sFN,iAct,iSize,m.hexdigest()),self.appl_alias)
                                #self.Send(self.startMarker+'getFileProc|'+','.join([sFN,str(iAct),str(iSize),'']))
                                #self.Send(blk,True)
                                #self.Send(self.endMarker)
                                self.SendTelegram('getFileProc|'+','.join([sFN,str(iAct),str(iSize),blk]))
                            dig=m.hexdigest()
                            vtLog.vtLngCur(vtLog.DEBUG,'fn:%s; size:%08d;sha:%s'%(sFN,iSize,dig),self.appl_alias)
                            self.SendTelegram('getFileFin|'+','.join([sFN,dig]))
                            f.close()
                        except:
                            vtLog.vtLngCS(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
                            f.close()
                            self.SendTelegram('getFileAbort|'+','.join([sFN,'processing fault']))
                        return
                    else:
                        self.SendTelegram('getFileStart|'+','.join([sFN]))
                        self.SendTelegram('getFileAbort|'+','.join([sFN,'restricted']))
                        vtLog.vtLngTB(self.appl_alias)
                        self.srv.release()
                        return 0
            vtLog.vtLngCur(vtLog.WARN,'fault:%d'%iFlt,
                    origin=self.appl_alias)
        except:
            vtLog.vtLngCS(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
    def __setFileStart__(self,val):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'val;%s'%val,origin=self.appl_alias)
        try:
            i=val.find('|')
            iFlt=0
            if i>=0:
                iFlt=1
                j=val.find(',',i+1)
                iFlt=0
                if i>=0:
                    appl_alias=val[i+1:j]
                    sFN=val[j+1:]
                    self.srv.acquire()
                    try:
                        iRetDoc=self.srv.IsServedDocRel(appl_alias)
                        if iRetDoc==1:
                            strs=sFN.split('/')
                            sTmpFN=strs[-1]
                            #i=sFN.find('_')
                            #sKey=None
                            #if i>0:
                            #    j=sFN.find('_',i+1)
                            #    if j>0:
                            #        sKey=sFN[i+1:j]
                            i=sTmpFN.find('_')
                            sKey=None
                            if i>0:
                                j=sTmpFN.find('_',i+1)
                                if j>0:
                                    sKey=sTmpFN[i+1:j]
                            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                vtLog.vtLngCur(vtLog.DEBUG,'fn:%s;key:%s;type:%s'%(sFN,sKey,type(sKey)),self.appl_alias)
                            if sKey is not None:
                                if sKey in self.dKey:
                                    fid=self.dKey[sKey]
                                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                        vtLog.vtLngCur(vtLog.DEBUG,'key:%s;type:%s'%(fid,type(fid)),self.appl_alias)
                                    #print fid,type(fid)
                                    if self.doc.IsAccessOkDocGrp(fid,self.objLogin,vtSecFileAcl.ACL_FILE_MSK_READ,-1)==False:
                                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                            vtLog.vtLngCur(vtLog.DEBUG,'fid:%s;vPrjDoc false'%(fid),self.appl_alias)
                                        if self.docDoc.IsAccessOkDocGrp(fid,self.objLogin,vtSecFileAcl.ACL_FILE_MSK_READ,-1)==False:
                                            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                                vtLog.vtLngCur(vtLog.DEBUG,'fid:%s;vDoc false'%(fid),self.appl_alias)
                                            self.SendTelegram('setFileAbort|'+','.join([sFN,'restricted']))
                                            return 0
                            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                vtLog.vtLngCur(vtLog.DEBUG,'fn:%s;key:%s;type:%s;start'%(sFN,sKey,type(sKey)),self.appl_alias)
                            try:
                                d=self.srv.GetServedDN(appl_alias)
                                sFullFN=os.path.join(d['dir'],sFN)
                                sTmpFN=sFullFN+'.net'
                                dn,fn=os.path.split(sTmpFN)
                                try:
                                    os.makedirs(dn)
                                except:
                                    pass
                                f=open(sTmpFN,'wb')
                            except:
                                self.SendTelegram('setFileAbort|'+','.join([sFN,'fault']))
                                vtLog.vtLngTB(self.appl_alias)
                                return 0
                            self.SendTelegram('setFileStart|'+','.join([sFN]))
                            self.dFileSet[sFN]={'file':f,'sha':sha.new(),'FN':sFullFN,'tmpFN':sTmpFN}
                        else:
                            self.SendTelegram('setFileAbort|'+','.join([sFN,'implemented']))
                    finally:
                        self.srv.release()
                return
            vtLog.vtLngCur(vtLog.WARN,'fault:%d'%iFlt,
                    origin=self.appl_alias)
        except:
            vtLog.vtLngTB(self.appl_alias)
    def __setFileProc__(self,val):
        try:
            #vtLog.vtLngCur(vtLog.INFO,'',origin=self.appl_alias)
            i=val.find('|')
            iFlt=0
            if i>=0:
                iFlt=1
                j=val.find(',',i+1)
                if j>0:
                    iFlt=2
                    k=val.find(',',j+1)
                    if k>0:
                        iFlt=3
                        l=val.find(',',k+1)
                        
                        sFN=val[i+1:j]
                        iAct=long(val[j+1:k])
                        iSize=long(val[k+1:l])
                        blk=val[l+1:]
                        d=self.dFileSet[sFN]
                        d['file'].write(blk)
                        d['sha'].update(blk)
                        self.SendTelegram('setFileProc|'+','.join([sFN,str(iAct),str(iSize)]))
                        #self.par.NotifyFileGetProc(sFN,iAct,iSize)
                return
            vtLog.vtLngCur(vtLog.WARN,'fault:%d'%iFlt,
                    origin=self.appl_alias)
        except:
            vtLog.vtLngTB(self.appl_alias)
    def __setFileFin__(self,val):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'',origin=self.appl_alias)
        try:
            i=val.find('|')
            iFlt=0
            if i>=0:
                iFlt=1
                j=val.find(',',i+1)
                if j>0:
                    sFN=val[i+1:j]
                    k=val.find(',',j+1)
                    if k==-1:
                        dig=val[j+1:]
                        times=None
                    else:
                        dig=val[j+1:k]
                        try:
                            times=[float(s) for s in val[k+1:].split(',')]
                        except:
                            vtLog.vtLngCur(vtLog.ERROR,'fn:%s faulty file times:%s'%(sFN,val[k+1:]),self.appl_alias)
                            times=None
                    d=self.dFileSet[sFN]
                    d['file'].close()
                    if d['sha'].hexdigest()!=dig:
                        #os.remove(d['tmpFN'])
                        vtLog.vtLngCur(vtLog.ERROR,'fn:%s content not identical'%(sFN),self.appl_alias)
                        del self.dFileSet[sFN]
                        self.SendTelegram('setFileAbort|'+','.join([sFN,'content']))
                        return
                        #self.par.NotifyFileGetAbort(sFN)
                    else:
                        try:
                            fnUtil.shiftFile(d['FN'])
                        except:
                            vtLog.vtLngCur(vtLog.ERROR,'fn:%s received correctly;but os can not shift files FN:%s'%(sFN,d['FN']),self.appl_alias)
                            #vtLog.PrintMsg(_('os can not rename received file to %s')%(d['FN']))
                            del self.dFileSet[sFN]
                            self.SendTelegram('setFileAbort|'+','.join([sFN,'rename']))
                        try:
                            os.rename(d['tmpFN'],d['FN'])
                        except:
                            vtLog.vtLngCur(vtLog.ERROR,'fn:%s received correctly;but os can not rename file from;tmpFN:%s;FN:%s'%(sFN,d['tmpFN'],d['FN']),self.appl_alias)
                            #vtLog.PrintMsg(_('os can not rename received file to %s')%(d['FN']))
                            del self.dFileSet[sFN]
                            self.SendTelegram('setFileAbort|'+','.join([sFN,'rename']))
                            return
                        vtLog.vtLngCur(vtLog.INFO,'fn:%s received correctly'%(sFN),self.appl_alias)
                        try:
                            if times is not None:
                                setFileTimes(d['FN'],times[0],times[1],times[2])
                        except:
                            vtLog.vtLngCur(vtLog.ERROR,'fn:%s faulty file times:%s'%(sFN,vtLog.pformat(times)),self.appl_alias)
                        self.srv.acquire()
                        try:
                            self.srv.RefreshFile(self.appl_alias,d['FN'])
                        finally:
                            self.srv.release()
                        #self.par.NotifyFileGetFin(sFN)
                    del self.dFileSet[sFN]
                    self.SendTelegram('setFileFin|'+','.join([sFN,'ok']))
                return
            vtLog.vtLngCur(vtLog.WARN,'fault:%d'%iFlt,
                    origin=self.appl_alias)
        except:
            vtLog.vtLngTB(self.appl_alias)
    def __setFileAbort__(self,val):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'',origin=self.appl_alias)
        try:
            i=val.find('|')
            iFlt=0
            if i>=0:
                sFN=val[i+1:]
                d=self.dFileSet[sFN]
                d['file'].close()
                #os.remove(d['tmpFN'])
                del self.dFileSet[sFN]
                #self.par.NotifyFileGetAbort(sFN)
                return
            vtLog.vtLngCur(vtLog.WARN,'fault:%d'%iFlt,
                    origin=self.appl_alias)
        except:
            vtLog.vtLngTB(self.appl_alias)
    def __listContent__(self,val):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'alias;%s'%val,origin=self.appl_alias)
        try:
            self.srv.acquire()
            iFlt=0
            i=string.find(val,'|')
            if i>=0:
                appl_alias=val[i+1:]
                # check is to login?
                iRetDoc=self.srv.IsServedDocRel(appl_alias)
                if iRetDoc==1:
                    # doc releated
                    #vtLog.CallStack('')
                    if self.objLogin is None:
                        ret=2
                        #self.objLogin,ret,self.doc=self.srv.CheckLogin(self.adr[0],
                        #                appl_alias,self.sessionID,self.usr,self.passwd)
                    else:
                        ret=3
                    #print self.objLogin,ret,self.doc,self.usr,self.passwd
                elif iRetDoc==0:
                    objLogin,ret,oAcl=self.srv.CheckLogin(self.adr[0],
                                    appl_alias,self.sessionID,self.usr,
                                    self.passwd)
                    #print objLogin,ret,oAcl,self.usr
                    if oAcl is not None:
                        bOk=oAcl.IsAccessOk(objLogin,vtSecFileAcl.ACL_FILE_MSK_READ,0)
                    else:
                        bOk=True
                    if bOk==False:
                        ret=0
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'appl_alias:%s;bOk:%d;'\
                                'usr:%s;oAcl:%d;ret:%d'%(appl_alias,bOk,self.usr,
                                oAcl is not None,ret),self.appl_alias)
                else:
                    # not served
                    ret=2
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'iRetDoc:%d;ret:%d'%(iRetDoc,ret),self.appl_alias)
                if ret==0:
                    vtLog.vtLngCur(vtLog.DEBUG,'login needed',self.appl_alias)
                    if self.loggedin==False:
                        self.SendTelegram('login')
                    self.srv.release()
                    return
                elif ret==1:
                    # logged in
                    vtLog.vtLngCur(vtLog.DEBUG,'loggedin',self.appl_alias)
                    if self.loggedin==False:
                        sInfo=''
                        try:
                            sInfo=objLogin.GetStr()
                        except:
                            sInfo=',,,,,'
                            vtLog.vtLngTB('')
                        #self.Send(self.startMarker+'loggedin|'+sInfo+self.endMarker)
                        self.SendTelegram('loggedin|'+sInfo)
                    self.loggedin=True
                    pass
                elif ret==2:
                    vtLog.vtLngCur(vtLog.DEBUG,'send empty content',self.appl_alias)
                    self.SendTelegram('listContent|'+appl_alias+',')
                    self.srv.release()
                    return
                elif ret==3:
                    pass
                else:
                    # forget it NO ACCESS
                    vtLog.vtLngCur(vtLog.DEBUG,'connection rejected',self.appl_alias)
                    #self.SendTelegram('rejected')
                    self.SendTelegram('login')
                    self.srv.release()
                    return
                try:
                    if iRetDoc==0:
                        l=self.srv.GetServedContent(appl_alias)
                        l.sort()
                        lFN=[]
                        if len(l)>0:
                            lFN=[l[0]]
                            sP=''.join([l[0],'.\d+'])#None
                            for sA in l[1:]:
                                if re.match(sP,sA) is not None:
                                    continue
                                sP=''.join([sA,'.\d+'])
                                lFN.append(sA)
                        #self.Send(self.startMarker+'listContent|'+appl_alias+','+';'.join(lFN)+self.endMarker)
                        self.SendTelegram('listContent|'+appl_alias+','+';'.join(lFN))
                    elif iRetDoc==1:
                        self.__listContentDoc__()
                except:
                    vtLog.vtLngTB(self.appl_alias)
        except:
            vtLog.vtLngCS(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
        self.srv.release()
    def __buildDirContent__(self,node,baseDN,iSkipDN,parDN,lDir):
        sTag=self.doc.getTagName(node)
        if sTag=='DIR':
            sDN=self.doc.getNodeText(node,'BASE')
            lDir.append((sDN[iSkipDN:],node))
            self.doc.procChildsExt(node,self.__buildDirContent__,baseDN,iSkipDN,sDN[iSkipDN:],lDir)
        return 0
    def __buildFileContent__(self,node,ioContent,parDN,dKey):
        sTag=self.doc.getTagName(node)
        if sTag=='FILE':
            sFN=self.doc.getNodeText(node,self.FILE_INFOS[0])
            # filter backup files (xxx.y.z) where z is a number
            i=sFN.rfind('.')
            if i>0:
                r=re.match('.\d+',sFN[i:])
                if r is not None:
                    if r.end()==(len(sFN)-i):
                        return 0
            i=sFN.find('_')
            sKey=None
            if i>0:
                j=sFN.find('_',i+1)
                if j>0:
                    sKey=sFN[i+1:j]
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'fn:%s;key:%s;type:%s'%(sFN,sKey,type(sKey)),self.appl_alias)
            if sKey is not None:
                if sKey in dKey:
                    fid=dKey[sKey]
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'key:%s;type:%s'%(fid,type(fid)),self.appl_alias)
                    #print fid,type(fid)
                    if self.doc.IsAccessOkDocGrp(fid,self.objLogin,vtSecFileAcl.ACL_FILE_MSK_READ,-1)==False:
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCur(vtLog.DEBUG,'fid:%s;vPrjDoc false'%(fid),self.appl_alias)
                        if self.docDoc.IsAccessOkDocGrp(fid,self.objLogin,vtSecFileAcl.ACL_FILE_MSK_READ,-1)==False:
                            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                vtLog.vtLngCur(vtLog.DEBUG,'fid:%s;vDoc false'%(fid),self.appl_alias)
                            return 0
            ioContent.write(''.join(['.',parDN,',']))
            lInfos=[':'.join([sInfo,self.doc.getNodeText(node,sInfo)]) for sInfo in self.FILE_INFOS]
            ioContent.write(','.join(lInfos))
            ioContent.write(';')
        return 0
    def __listContentDoc__(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.appl_alias)
        try:
            #bErr=False
            if self.docDoc is None:
                vtLog.vtLngCur(vtLog.ERROR,'no vDoc reference set',self.appl_alias)
                return
            dFID={}
            dKey={}
            try:
                self.docDoc.acquire()
                oDocGrp=self.docDoc.GetReg('docgroup')
                idObj=self.docDoc.getIds()
                ids=idObj.GetIDs()
                for id in ids:
                    node=self.docDoc.getNodeByIdNum(id)
                    if node is None:
                        continue
                    if self.docDoc.getTagName(node)!='docgroup':
                        continue
                    sDN=oDocGrp.GetPath(node)
                    sKey=oDocGrp.GetKey(node)
                    dKey[sKey]=id
                    dFID[id]=sDN
            except:
                self.docDoc.release()
                vtLog.vtLngCS(vtLog.ERROR,'%s'%(traceback.format_exc()),origin=self.appl_alias)
                return
            self.docDoc.release()
            self.dKey=dKey
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'dKeys:%s;dFID:%s'%(vtLog.pformat(dKey),vtLog.pformat(dFID)),origin=self.appl_alias)
            ioContent=StringIO()
            try:
                self.doc.acquire()
                nodeContent=self.doc.getChildForced(self.doc.getRoot(),'contense')
                nodeBase=self.doc.getChild(nodeContent,'DIR')
                baseDN=self.doc.getNodeText(nodeBase,'BASE')
                lDir=[]
                self.doc.procChildsExt(nodeBase,self.__buildDirContent__,baseDN,len(baseDN),'',lDir)
                #print lDir
                self.doc.procChildsExt(nodeBase,self.__buildFileContent__,ioContent,'',dKey)
                lDir.sort()
                #vtLog.pprint(lDir)
                for tup in lDir:
                    self.doc.procChildsExt(tup[1],self.__buildFileContent__,ioContent,tup[0],dKey)
                #bOk=self.doc.IsAccessOk(node,self.objLogin,doc.ACL_MSK_READ)
                #print bOk
                #print bOk
                #if vtLog.vtLngIsLogged(vtLog.DEBUG):
                #        vtLog.vtLngCur(vtLog.DEBUG,'access:%d'%bOk,self.appl_alias)
            except:
                self.doc.release()
                vtLog.vtLngCS(vtLog.ERROR,'%s'%(traceback.format_exc()),origin=self.appl_alias)
                return
            self.doc.release()
            content=ioContent.getvalue()
            #print content
            m=sha.new()
            m.update(content)
            m.update(self.sessionID)
            fp=m.hexdigest()
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'keys:%s'%content,self.appl_alias)
            self.SendTelegram('listContent|'+fp+','+content)
        except:
            vtLog.vtLngCS(vtLog.ERROR,'%s'%(traceback.format_exc()),origin=self.appl_alias)
    def __alias__(self,val):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCS(vtLog.INFO,'alias;%s'%val,origin=self.appl_alias)
        #if self.__checkLogin__():
        try:
            self.srv.acquire()
            self.doc=self.srv.GetServedDN(val[1:])
            if self.doc is None:
                self.appl_alias=''
                self.SendTelegram('alias|not served')
            else:
                self.SendTelegram('alias|served')
                self.appl_alias=val[1:]
                # check is to login?
                ret=self.srv.IsLogginNeeded(self.adr[0],val[1:])
                if ret>0:
                    vtLog.vtLngCur(vtLog.DEBUG,'login needed',self.appl_alias)
                    self.loggedin=False
                    self.SendTelegram('login')
                elif ret==0:
                    #no login required
                    vtLog.vtLngCur(vtLog.DEBUG,'no login needed',self.appl_alias)
                    self.loggedin=True
                    self.srv.EnsurseContense(self.appl_alias)
                    pass
                else:
                    # forget it NO ACCESS
                    vtLog.vtLngCur(vtLog.DEBUG,'connection rejected',self.appl_alias)
                    self.SendTelegram('rejected')
        except:
            vtLog.vtLngCS(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
        self.srv.release()
    def __sessionID__(self,val):
        pass
    def __authenticate__(self,val):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'%s'%val,origin=self.appl_alias)
        try:
            self.phrase=None
            self.pubKey=None
            i=val.find(',')
            if i>0:
                self.phrase=val[1:i]
                self.pubKey=val[i+1:]
                if len(self.phrase)==0:
                    self.phrase=None
                    self.pubKey=None
                    return
                if self.oCryptoSrv==None:
                    sDN=vIdentify.getCfg('baseDN')
                    sPrivFN = "license.priv"
                    global mid
                    self.oCryptoSrv=vtSecCryptoFile(mid,fn=sPrivFN,dn=sDN,keySize=512,algoSes='Blowfish')
                if self.oCryptoClt==None:
                    self.oCryptoClt=vtSecCryptoFile('',fn=None,dn=None,keySize=512,algoSes='Blowfish')
                self.oCryptoClt.ImportKeyStr(self.pubKey,self.phrase)
        except:
            self.SendTelegram('authenticate|failure')
            vtLog.vtLngTB(self.appl_alias)
    def __login__(self,val):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCS(vtLog.INFO,'login;%s'%val,origin=self.appl_alias)
        try:
            self.usr=None
            self.passwd=None
            i=val.find(',')
            if i>0:
                self.srv.EnsurseContense(self.appl_alias)
                iRetDoc=self.srv.IsServedDocRel(self.appl_alias)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'iRetDoc:%d'%(iRetDoc),self.appl_alias)
                if iRetDoc==1:
                    self.objLogin,iRes,self.doc=self.srv.CheckLogin(self.adr[0],self.appl_alias,
                            self.sessionID,val[1:i],binascii.a2b_uu(val[i+1:]))
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'%s;iRet:%d;adr:%s'%(self.objLogin,iRes,self.adr),self.appl_alias)
                    #vtLog.CallStack('')
                    #print 'addr',self.adr
                    #print self.objLogin
                    if iRes>0:
                        self.docDoc=self.srv.GetServedRefDoc(self.appl_alias)
                        sInfo=''
                        try:
                            sInfo=self.objLogin.GetStr()
                        except:
                            sInfo=',,,,,'
                            vtLog.vtLngTB('')
                        #self.Send(self.startMarker+'loggedin|'+sInfo+self.endMarker)
                        self.SendTelegram('loggedin|'+sInfo)
                        #self.srv.EnsurseContense(self.appl_alias)
                        self.loggedin=True
                        if 1==0:
                            if val[1:i]=='wal':
                                self.objLogin=vtSecXmlLogin(1,15,16)
                            else:
                                self.objLogin=vtSecXmlLogin(0,15,16)
                            #print self.objLogin
                        self.usr=val[1:i]
                        return
                    else:
                        self.SendTelegram('rejected')
                    #self.Send(self.startMarker+'login'+self.endMarker)
                    self.SendTelegram('login')
                else:
                    self.usr=val[1:i]
                    self.passwd=binascii.a2b_uu(val[i+1:])
                    #self.Send(self.startMarker+'loggedin|'+self.endMarker)
                    self.SendTelegram('loggedin|')
        except:
            vtLog.vtLngCS(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
                
    def __loggedin__(self,val):
        pass
    def __shutdown__(self,val):
        pass
    def __browse__(self,val):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCS(vtLog.INFO,'browse;%s'%val,origin=self.appl_alias)
        #if self.__checkLogin__()==0:
        #    return
        try:
            self.srv.acquire()
            l=[]
            if len(val)>1:
                appl=val[1:]
                l=self.srv.GetServedDNs(appl)
                #self.Send(self.startMarker+'browse|'+';'.join(l)+self.endMarker)
                self.SendTelegram('browse|'+';'.join(l))
                self.srv.release()
                return
        except:
            vtLog.vtLngCS(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
        #self.Send(self.startMarker+'browse|'+self.endMarker)
        self.SendTelegram('browse|')
        self.srv.release()
class vtRefConsumer(vtXmlDomConsumer):
    def __init__(self,par,sTag):
        vtXmlDomConsumer.__init__(self)
        self.srvFile=par
        self.sTag=sTag
    def SetParent(self,par):
        self.srvFile=par
    def Stop(self):
        #vtLog.CallStack('')
        #print self.sTag
        if self.srvFile is None:
            return
        vtLog.vtLngCurCls(vtLog.DEBUG,'tag:%s'%(self.sTag),self)
        self.srvFile.StopServed(self.sTag)
    def IsBusy(self):
        if self.srvFile is None:
            return False
        #vtLog.CallStack('')
        #print self.sTag
        vtLog.vtLngCurCls(vtLog.DEBUG,'tag:%s'%(self.sTag),self)
        if self.srvFile.IsServedFile(self.sTag):
            return True
        else:
            return False
class vtSrvFileSock(vtSrvSock):
    VIDARC_ALIAS='vPlugInFile:vidarc'
    VIDARC_ALIAS_LOCALE='vPlugInFile:vidarcLocale'
    SERVE_RUNNING              = 0x0001
    SERVE_PAUSED               = 0x0002
    SERVE_STOPPED              = 0x0004
    def __init__(self,server,port,socketClass,verbose=0):
        vtSrvSock.__init__(self,server,port,socketClass,verbose=verbose)
        self.docSettings=None
        self.hosts={}
        self.dDN={}
        self.dLogin={}
        self.docHum=None
        self.appl='vtSrvFileSock'
        #self.thdContense=vtThread(None)
        self.thdContense=vtThreadCore(None)
        #self.semSrv=threading.Lock()#threading.Semaphore()
        self.semSrv=self.thdContense.getLock()
    def __getStatus__(self,sTag):
        o=vtStateFlagSimple({
                0:  {'name':'UNDEF','is':'IsUndef','init':True,
                     'enter':{'func':(self.NotifyUndefDN,(sTag,),{})}},
                1:  {'name':'RUN','is':'IsRun',
                     'enter':{'func':(self.NotifyRunningDN,(sTag,),{}),
                               'set':['rng']}},
                2:  {'name':'PAUSE','is':'IsPause',
                     'enter':{'func':(self.NotifyPauseDN,(sTag,),{})}},
                3:  {'name':'STOP','is':'IsStop',
                     'enter':{'func':(self.NotifyStopDN,(sTag,),{}),
                              'clr':['rng','cont']}},
                4:  {'name':'CONTENSE','is':'IsContense',
                     'enter':{'func':(self.NotifyContenseDN,(sTag,),{}),
                              'set':['cont']}},
                },
                {
                0x0001:{'name': 'cont', 'order':1,  'is':'IsContense'},
                0x0002:{'name': 'rng',  'order':2,  'is':'IsRunning'},
                }
                )
        return o
    def NotifyUndefDN(self,sTag):
        pass
    def NotifyRunningDN(self,sTag):
        pass
    def NotifyPauseDN(self,sTag):
        pass
    def NotifyStopDN(self,sTag):
        pass
    def NotifyContenseDN(self,sTag):
        try:
            d=self.dDN[sTag]
            if 'applRef' in d:
                if d['doc'] is None:
                    srvXml=d['srvXml']
                    applAliasRef=d['applRef']
                    doc=srvXml.GetServedXmlDom(applAliasRef)
                    domConsumer=vtRefConsumer(self,sTag)
                    domConsumer.SetDoc(doc)
                    doc.acquire()
                    try:
                        cfgNode=doc.getChildByLst(doc.getRoot(),['config','vDoc'])
                        if cfgNode is None:
                            vtLog.vtLngCur(vtLog.ERROR,'config node is None',self.appl)
                        else:
                            docDoc=None
                            for c in doc.getChilds(cfgNode,'host'):
                                sName=doc.getNodeText(c,'name')
                                sPort=doc.getNodeText(c,'port')
                                sAlias=doc.getNodeText(c,'alias')
                                applAliasRefDoc=':'.join(['vDoc',sAlias])
                                vtLog.vtLngCur(vtLog.INFO,'tag:%s;aliasRef:%s;aliasRefDoc:%s'%(sTag,
                                        applAliasRef,applAliasRefDoc),self.appl)
                                docDoc=srvXml.GetServedXmlDom(applAliasRefDoc)
                                if docDoc is not None:
                                    break
                                else:
                                    vtLog.vtLngCur(vtLog.WARN,'alias:%s not served'%(
                                                applAliasRefDoc),self.appl)
                                #s=getHostAliasStr(sName,sPort,sAlias)
                                #s=sName+u':'+sPort+u'//'+sAlias
                                #vtLog.vtLngCurWX(vtLog.DEBUG,s,self)
                                #l.append(s)
                            if docDoc is None:
                                vtLog.vtLngCur(vtLog.ERROR,'no vDoc-doc defined',self.appl)
                            else:
                                doc.SetNetDocs({'vDoc':{'doc':docDoc,'bNet':False}})
                    except:
                        vtLog.vtLngTB(self.appl)
                    doc.release()
                    d.update({
                                'domConsumer':domConsumer,
                                'doc':doc,
                                'vDoc':docDoc,
                            })
            self.__getContense__(sTag)
        except:
            vtLog.vtLngTB(self.appl)
    def acquire(self,blocking=True):
        return self.semSrv.acquire(blocking)
    def release(self):
        return self.semSrv.release()
    def SetAppl(self,appl):
        self.appl=appl
    def SetDocHum(self,doc):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'doc is None==%d'%(doc is None),self.appl)
        self.docHum=doc
        del self.dLogin
        self.dLogin={}
    def ShutDown(self):
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
        self.docHum=None
        self.dDN={}
        self.dLogin={}
        self.docSettings=None
        vtSrvSock.ShutDown(self)
        #vtLog.CallStack('')
        #print self.dDN
    def __getContense__(self,sTag):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'alias:%s'%(sTag),self.appl)
        #vtLog.CallStack('')
        if sTag not in self.dDN:
            return
        d=self.dDN[sTag]
        #vtLog.pprint(d)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'alias:%s;d:%s'%(sTag,vtLog.pformat(d)),self.appl)
        doc=d['doc']
        doc.acquire()
        try:
            nodeContense=doc.getChildForced(doc.getRoot(),'contense')
            dirEntries=DirEntryXml(doc,nodeContense,bRead=True)
            dirEntries.process(d['dir'],20,style=0)
            d['dirEntries']=dirEntries
            #for root, dirs, files in os.walk(d['dir']):
            #    print root, "consumes",
            #    print sum(os.path.getsize(os.path.join(root, name)) for name in files),
            #    print "bytes in", len(files), "non-directory files"
            
            #vtLog.CallStack('')
            #vtLog.pprint(nodeContense)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        d['doc'].release()
    def RefreshFile(self,sTag,sFN):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'alias:%s;FN:%s'%(sTag,sFN),self.appl)
        try:
            if sTag not in self.dDN:
                return
            d=self.dDN[sTag]
            if 'dirEntries' not in d:
                return
            if 'doc' not in d:
                return
            dE=d['dirEntries']
            doc=d['doc']
            doc.acquire()
            try:
                dE.refresh(sFN)
                #dirEntries.refresh(d['dir'],sFN,style=0)
            except:
                vtLog.vtLngTB(self.__class__.__name__)
            d['doc'].release()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def AddServedDN(self,sTag,sDN,srvXml=None,applAliasRef=None):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'alias:%s;DN:%s'%(sTag,sDN),self.appl)
        
        #if sTag.find(self.VIDARC_ALIAS_LOCALE)==0:
        #    sLocDN=os.path.join(vIdentify.getCfg('baseDN'),'locale')
        #    sDN=sLocDN+sDN[len(sLocDN):]
        #    if vtLog.vtLngIsLogged(vtLog.INFO):
        #        vtLog.vtLngCur(vtLog.INFO,'tag:%s;dir changed to DN:%s'%(sTag,sDN),self.appl)
        #if sTag==self.VIDARC_ALIAS:
        #    sDN=vIdentify.getCfg('baseDN')
        #    if vtLog.vtLngIsLogged(vtLog.INFO):
        #        vtLog.vtLngCur(vtLog.INFO,'tag:%s;dir changed to DN:%s'%(sTag,sDN),self.appl)
        #elif sTag==self.VIDARC_ALIAS_LOCALE:
        #    sDN=os.path.join(vIdentify.getCfg('baseDN'),'locale')
        #    if vtLog.vtLngIsLogged(vtLog.INFO):
        #        vtLog.vtLngCur(vtLog.INFO,'tag:%s;dir changed to DN:%s'%(sTag,sDN),self.appl)
        if srvXml is None:
            try:
                lDir=os.listdir(sDN)
                lFN=[]
                for f in lDir:
                    sFN=os.path.join(sDN,f)
                    s=os.stat(sFN)
                    if stat.S_ISREG(s[stat.ST_MODE]):
                        lFN.append(f)
                oStatus=self.__getStatus__(sTag)
                oStatus.SetState(oStatus.RUN)
                self.dDN[sTag]={   'dir':sDN,
                                    'files':lFN,
                                    'connections':[],
                                    'status':oStatus,
                                }
            except:
                vtLog.vtLngTB(self.appl)
        else:
            vtLog.vtLngCur(vtLog.INFO,'tag:%s;aliasRef:%s'%(sTag,applAliasRef),self.appl)
            #lDir=os.listdir(sDN)
            #lFN=[]
            #for f in lDir:
            #    sFN=os.path.join(sDN,f)
            #    s=os.stat(sFN)
            #    if stat.S_ISREG(s[stat.ST_MODE]):
            #        lFN.append(f)
            lFN=None
            oStatus=self.__getStatus__(sTag)
            oStatus.SetState(oStatus.RUN)
            self.dDN[sTag]={   'dir':sDN,
                                'files':lFN,
                                #'contense':{},
                                'dirEntries':None,
                                'connections':[],
                                'status':oStatus,
                                'srvXml':srvXml,
                                'applRef':applAliasRef,
                                'domConsumer':None,#domConsumer,
                                'doc':None,#doc,
                                'vDoc':None,#docDoc,
                            }
            if os.path.exists(sDN)==False:
                vtLog.vtLngCur(vtLog.ERROR,'dn:%s does not exist'%(sDN),self.appl)
    def IsServedStatus(self,alias):
        if alias in self.dDN:
            if self.dDN[alias]['status'].IsRunning:
                return 1
            else:
                return 0
        else:
            return -1
    def IsServed(self,sTag):
        return sTag in self.dDN
    def IsServedFile(self,sTag):
        if self.IsServed(sTag):
            if self.IsServedStatus(sTag):
                return True
            else:
                return False
        return False
    def IsServedDocRel(self,sTag):
        if len(sTag)==0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'alias:%s;no doc'%(sTag),self.appl)
            return 0
        if sTag in self.dDN:
            if 'doc' in self.dDN[sTag]:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'alias:%s;doc;%s'%(sTag,vtLog.pformat(self.dDN[sTag])),self.appl)
                return 1
            else:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'alias:%s;no doc;:%s'%(sTag,vtLog.pformat(self.dDN[sTag])),self.appl)
                return 0
        else:
            return -1
    def GetServedRefDoc(self,sTag):
        if sTag in self.dDN:
            d=self.dDN[sTag]
            if 'vDoc' in d:
                return d['vDoc']
        return None
    def GetServedDN(self,sTag):
        if sTag in self.dDN:
            return self.dDN[sTag]
        return None
    def GetServedDNs(self,prefix=None):
        if prefix is None:
            return self.dDN.keys()
        else:
            l=[]
            keys=self.dDN.keys()
            keys.sort()
            for k in keys:
                if k.find(prefix)==0:
                    if len(prefix)<len(k):
                        l.append(k)
            return l
    def UpdateServedDN(self,sTag):
        d=self.GetServedDN(sTag)
        if d is None:
            return
        sDN=d['dir']
        lFN=[]
        try:
            lDir=os.listdir(sDN)
            for f in lDir:
                sFN=os.path.join(sDN,f)
                s=os.stat(sFN)
                if stat.S_ISREG(s[stat.ST_MODE]):
                    lFN.append(f)
        except:
            vtLog.vtLngTB(self.appl)
        d['files']=lFN
    def GetServedContent(self,sTag):
        d=self.GetServedDN(sTag)
        if d is None:
            return []
        self.UpdateServedDN(sTag)
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'alias:%s;l:%s'%(sTag,vtLog.pformat(d)),self.appl)
        return d['files']
    def StopSocket(self,adr,sockInst):
        #vtLog.CallStack('')
        try:
            alias=sockInst.appl_alias
            #print alias
            #print self.dDN
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'alias:%s'%(alias),self.appl)
            if alias not in self.dDN:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'alias:%s;%s,closing...'%(alias,repr(adr)),self.appl)
                sockInst.Close()
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'alias:%s;%s,closed'%(alias,repr(adr)),self.appl)
                return
            tup=self.dDN[alias]
            i=0
            bFound=False
            for cadr,sock in tup['connections']:
                if cadr==adr:
                    bFound=True
                    break
                i+=1
            #if self.verbose:
            #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'connection:%d remove'%i)
            if bFound:
                tup['connections']=tup['connections'][:i]+tup['connections'][i+1:]
        except:
            vtLog.vtLngCS(vtLog.ERROR,'%s'%(traceback.format_exc()),origin=self.appl)
        #vtSrvSock.StopSocket(self,adr,sockInst)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'%s,closing...'%repr(adr),sockInst.appl_alias)
        sockInst.Close()
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'%s,closed'%repr(adr),sockInst.appl_alias)
    def ClearServedDN(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',self.appl)
        for k in self.dDN.keys():
            self.DelServedDN(k)
        pass
    def DelServedDN(self,alias,bWaitConnectionsClosed=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'alias:%s'%(alias),origin=self.appl)
        if self.thdContense.IsRunning():
            if vtLog.vtLngIsLogged(vtLog.ERROR):
                vtLog.vtLngCur(vtLog.ERROR,'alias:%s;is not posssible now contense thread is running'%(alias),origin=self.appl)
            return False
        try:
            if alias in self.dDN:
                tup=self.dDN[alias]
                #.append((adr,sockInst))
                if self.acquire(False):
                    bRelease=True
                else:
                    bRelease=False
                    vtLog.vtLngCur(vtLog.CRITICAL,''%(),alias)
                for conn in tup['connections']:
                    #self.StopSocket(conn[0],conn[1])
                    try:
                        conn[1].Close()
                    except:
                        vtLog.vtLngTB(alias)
                if bRelease:
                    self.release()
                if bWaitConnectionsClosed:
                    zTime=0
                    zWait=0.1
                    while len(tup['connections'])!=0:
                        time.sleep(zWait)
                        zTime+=zWait
                        if zTime<60.0:
                            vtLog.vtLngCur(vtLog.CRITICAL,'time out'%(),alias)
                            break
                if 'domConsumer' in self.dDN[alias]:
                    domCons=self.dDN[alias]['domConsumer']
                    if domCons is not None:
                        domCons.SetParent(None)
                        domCons.ClearDoc()
                    #domCons.SetDoc(None)
                if 'doc' in self.dDN[alias]:
                    self.dDN[alias]['doc']=None
                if 'vDoc' in self.dDN[alias]:
                    self.dDN[alias]['vDoc']=None
                if 'dirEntries' in self.dDN[alias]:
                    if self.dDN[alias]['dirEntries'] is not None:
                        self.dDN[alias]['dirEntries'].ClearDoc()
                        self.dDN[alias]['dirEntries']=None
                #vtLog.CallStack('')
                #print self.dDN[alias]
                del self.dDN[alias]
                vtLog.vtLngCur(vtLog.INFO,'deleted',alias)
                return True
            else:
                vtLog.vtLngCur(vtLog.INFO,'not found',alias)
                return False
        except:
            vtLog.vtLngTB(alias)
        return False
    def SetServedHosts(self,alias,hosts,oAcl):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCS(vtLog.INFO,'alias:%s;host:%s'%(alias,hosts),origin=self.appl)
        try:
            lstHosts=self.hosts[alias]
            # check current connections
        except:
            pass
        lst=[]
        for tup in hosts:
            i=2
            if tup[1]=='__vHum__':
                i=0
            elif tup[1]=='__srv__':
                i=1
            lst.append((i,tup))
        def compFunc(a,b):
            iRes=cmp(a[0],b[0])
            return iRes
        lst.sort(compFunc)
        tmp=[]
        for tup in lst:
            tmp.append(tup[1])
        self.hosts[alias]=(tmp,oAcl)
    def IsLogginNeeded(self,adr,applAlias):
        try:
            if self.verbose:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'addr:%s;alias:%s;hosts:%s'%(adr,applAlias,self.hosts),self.appl)
            i=applAlias.find(':')
            appl=applAlias[:i]
            alias=applAlias[i+1:]
            try:
                lstHosts,oAcl=self.hosts[applAlias]
            except:
                return 0
            for host in lstHosts:
                if self.__checkAdr__(host[0],adr):
                    if host[1]=='__vHum__':
                        return 1
                    elif host[1]=='__srv__':
                        return 1
                    else:
                        if len(host[2])==0:
                            if len(host[1])==0:
                                return 0
                            else:
                                return 1
                        else:
                            return 1
        except:
            vtLog.vtLngTB(self.appl)
        return -1
    def __checkAdr__(self,adr1,adr2):
        try:
            strs1=adr1.split('.')
            strs2=adr2.split('.')
            for s1,s2 in zip(strs1,strs2):
                if fnmatch.fnmatch(s2,s1)==False:
                    return False
            return True
        except:
            vtLog.vtLngTB(self.appl)
    def CheckLogin(self,adr,applAlias,sessionID,usr,passwd):
        try:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,
                        'adr:%s;alias:%s;usr:%s'%
                        (adr,applAlias,usr),self.appl)
            i=applAlias.find(':')
            appl=applAlias[:i]
            alias=applAlias[i+1:]
            try:
                lstHosts,oAcl=self.hosts[applAlias]
            except:
                return None,-1,None
            doc=None
            if applAlias in self.dDN:
                if 'doc' in self.dDN[applAlias]:
                    doc=self.dDN[applAlias]['doc']
            for host in lstHosts:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,
                            'host:%s'%(vtLog.pformat(host)),self.appl)
                if self.__checkAdr__(host[0],adr):
                    if vtLog.vtLngIsLogged(vtLog.DEBUG): 
                        vtLog.vtLngCur(vtLog.DEBUG,
                                'check host:%s'%(vtLog.pformat(host)),
                                self.appl)
                    if host[1]=='__vHum__':
                        if doc is None:
                            if self.docHum is None:
                                continue
                                return None,-1,None
                            if usr in self.dLogin:
                                obj=self.dLogin[usr]
                                iRes=1
                            else:
                                obj,iRes=self.CheckLoginHum(self.docHum,appl,sessionID,usr,passwd)
                            if obj is not None:
                                if vtLog.vtLngIsLogged(vtLog.INFO):
                                    vtLog.vtLngCur(vtLog.INFO,'login ok;obj:%s'%(repr(obj)),self.appl)
                                return obj,1,doc #oAcl
                        else:
                            if self.docHum is None:
                                continue
                                return None,-1,None
                            if usr in self.dLogin:
                                obj=self.dLogin[usr]
                                iRes=1
                            else:
                                obj,iRes=self.CheckLoginHum(self.docHum,appl,sessionID,usr,passwd)
                            if obj is not None:
                                if vtLog.vtLngIsLogged(vtLog.INFO):
                                    vtLog.vtLngCur(vtLog.INFO,'login ok;obj:%s'%(repr(obj)),self.appl)
                                return obj,1,doc
                    elif host[1]=='__srv__':
                        obj,iRes=self.CheckLoginInSettings(appl,sessionID,usr,passwd)
                        if obj is not None:
                            if vtLog.vtLngIsLogged(vtLog.INFO):
                                vtLog.vtLngCur(vtLog.INFO,'login ok;obj:%s'%(repr(obj)),self.appl)
                            return obj,1,doc#None
                    else:
                        if len(host[2])==0:
                            if len(host[1])==0:
                                return None,0,doc#None
                            else:
                                if host[1]==usr:
                                    obj=vtSecXmlLogin(usr,-1,[],[],32,True)
                                    if vtLog.vtLngIsLogged(vtLog.INFO):
                                        vtLog.vtLngCur(vtLog.INFO,'login ok;obj:%s'%(repr(obj)),self.appl)
                                    return obj,1,doc#None
                        else:
                            if host[1]==usr:
                                m=sha.new()
                                m.update(host[2])
                                m.update(sessionID)
                                if m.digest()==passwd:
                                    obj=vtSecXmlLogin(usr,-1,[],[],32,True)
                                    if vtLog.vtLngIsLogged(vtLog.INFO):
                                        vtLog.vtLngCur(vtLog.INFO,'login ok;obj:%s'%(repr(obj)),self.appl)
                                    return obj,1,doc#None
        except:
            vtLog.vtLngTB(self.appl)
        return None,-1,None
    def CheckLoginHum(self,docHum,appl,sessionID,usr,passwd):
        if docHum is None:
            return None,0
        obj=None
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,
                        'appl:%s;usr:%s'%
                        (appl,usr),self.appl)
        docHum.acquire()
        try:
            #vtLog.CallStack('')
            nodeUsrs=docHum.getChildByLst(docHum.getBaseNode(),['users'])
            for nodeUsr in docHum.getChilds(nodeUsrs,'user'):
                if usr==docHum.getNodeText(nodeUsr,'name'):
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'user:%s found'%(usr),self.appl)
                    nodeTmp=docHum.getChild(nodeUsr,'security')
                    sPasswd=docHum.getNodeText(nodeTmp,'passwd')
                    try:
                        #vtLog.CallStack('')
                        #print docHum.getKey(nodeUsr)
                        iId=long(docHum.getKey(nodeUsr))
                    except:
                        iId=-1
                    
                    #m=sha.new()
                    #m.update(sPasswd)
                    #m.update(sessionID)
                    #if m.digest()==passwd:
                        # password is ok
                    #    vtLog.vtLngCur(vtLog.INFO,'user:%s passwd matched'%(usr),self.appl)
                    #    pass
                    #else:
                    #    vtLog.vtLngCur(vtLog.WARN,'user:%s passwd mismatched'%(usr),self.appl)
                    #    break
                    bAdmin=False
                    iGrpSecLv=-1
                    grpIds=[]
                    grps=[]
                    for nodeGrp in docHum.getChilds(nodeTmp,'grp'):
                        fid=docHum.getAttribute(nodeGrp,'fid')
                        try:
                            grpIds.append(long(fid))
                            nodeGrp=docHum.getNodeByIdNum(long(fid))
                            if nodeGrp is not None:
                                grps.append(docHum.getNodeText(nodeGrp,'name'))
                                nodeGrpSec=docHum.getChild(nodeGrp,'security')
                                if nodeGrpSec is not None:
                                    try:
                                        iGrpSecLvAct=long(docHum.getNodeText(nodeGrpSec,'sec_level'))
                                        if iGrpSecLvAct>iGrpSecLv:
                                            iGrpSecLv=iGrpSecLvAct
                                    except:
                                        pass
                                    try:
                                        bAdminAct=long(docHum.getNodeText(nodeGrpSec,'admin'))
                                        if bAdminAct>0:
                                            bAdmin=True
                                    except:
                                        pass
                            else:
                                grps.append('')
                        except:
                            pass
                    try:
                        iLevel=int(docHum.getNodeText(nodeTmp,'sec_level'))
                    except:
                        iLevel=0
                    if iLevel<0:
                        iLevel=iGrpSecLv
                    if len(sPasswd)==0:
                        try:
                            obj=vtSecXmlLogin(usr,iId,grpIds,grps,iLevel,bAdmin)
                        except:
                            vtLog.vtLngTB(self.appl)
                        break
                    m=sha.new()
                    m.update(sPasswd)
                    m.update(sessionID)
                    if m.digest()==passwd:
                        # password is ok
                        vtLog.vtLngCur(vtLog.INFO,'user:%s passwd matched'%(usr),self.appl)
                        try:
                            obj=vtSecXmlLogin(usr,iId,grpIds,grps,iLevel,bAdmin)
                        except:
                            vtLog.vtLngTB(self.appl)
                        break
                    else:
                        vtLog.vtLngCur(vtLog.WARN,'user:%s passwd mismatched'%(usr),self.appl)
                        #break
        except:
            vtLog.vtLngTB(self.appl)
        docHum.release()
        return obj,obj is not None
    def __checkLoginInSettings__(self,node,usr):
        for c in self.docSettings.getChilds(node,'login'):
            sLogin=self.docSettings.getNodeText(c,'login')
            if sLogin==usr:
                return c
        return None
    def CheckLoginInSettings(self,appl,sessionID,usr,passwd):
        try:
            nodeAll=None
            nodeAppl=None
            if self.docSettings is None:
                return None,0
            tmp=self.docSettings.getChild(self.docSettings.getRoot(),'access')
            if tmp is not None:
                for c in self.docSettings.getChilds(tmp):
                    tagName=self.docSettings.getTagName(c)
                    if tagName==appl:
                        nodeAppl=self.__checkLoginInSettings__(c,usr)
                    elif tagName=='all':
                        nodeAll=self.__checkLoginInSettings__(c,usr)
                nodeTmp=None
                if nodeAppl is not None:
                    nodeTmp=nodeAppl
                elif nodeAll is not None:
                    nodeTmp=nodeAll
                if nodeTmp is not None:
                    sPasswd=self.docSettings.getNodeText(nodeTmp,'passwd')
                    try:
                        iLevel=int(self.docSettings.getNodeText(nodeTmp,'sec_level'))
                    except:
                        iLevel=0
                        
                    m=sha.new()
                    m.update(sPasswd)
                    m.update(sessionID)
                    if m.digest()==passwd:
                        # password is ok
                        try:
                            obj=vtSecXmlLogin(usr,-1,[],[],iLevel,iLevel>=32)
                        except:
                            traceback.print_exc()
                        return obj,1
        except:
            vtLog.vtLngTB(self.appl)
        return None,0
    def StartServed(self,alias):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'alias:%s'%(alias),alias)
        if alias in self.dDN:
            tup=self.dDN[alias]
            oStatus=tup['status']
            oStatus.SetState(oStatus.RUN)
    def StopServed(self,alias):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'alias:%s'%(alias),alias)
        if alias in self.dDN:
            tup=self.dDN[alias]
            oStatus=tup['status']
            oStatus.SetState(oStatus.STOP)
            for conn in tup['connections']:
                self.StopSocket(conn[0],conn[1])
    def EnsurseContense(self,sTag):
        try:
            self.acquire()
            if sTag in self.dDN:
                tup=self.dDN[sTag]
                oStatus=tup['status']
                if oStatus.IsContense==False:
                    oStatus.SetState(oStatus.CONTENSE)
        except:
            vtLog.vtLngTB(self.appl)
        self.release()
        
    def Stop(self):
        vtLog.vtLngCurCls(vtLog.INFO,'',self)
        #self.stopping=True
        vtSrvSock.Stop(self)
    def RunOld(self):
        self._acquire()
        self.serving=True
        self.connections={}
        self._release()
        HOST=None#'localhost'
        vtLog.vtLngCur(vtLog.INFO,'Run on port %d'%self.port,self.GetOrigin())
        for res in socket.getaddrinfo(HOST, self.port, socket.AF_UNSPEC, socket.SOCK_STREAM, 0, socket.AI_PASSIVE):
            #vtLog.vtLngCurCls(vtLog.INFO,repr(res),self)
            af, socktype, proto, canonname, sa = res
            try:
                self.socket = socket.socket(af, socktype, proto)
            except socket.error, msg:
                self.socket = None
                vtLog.vtLngTB(self.__class__.__name__)
                continue
            try:
                self.socket.bind(sa)
                self.socket.listen(1)
            except socket.error, msg:
                self.socket.close()
                self.socket = None
                vtLog.vtLngTB(self.GetOrigin())
                continue
            break
            #if s is None:
            #    print 'could not open socket'
                #sys.exit(1)
            #print s
        if self.socket is None:
            self.serving=False
            vtLog.vtLngCur(vtLog.INFO,'socket is None',self.GetOrigin())
            return
        self.socket.settimeout(1.0)
        while self.serving:
            if self.Is2Stop():
            #if self.stopping:
                break
            #vtLog.vtLngCurCls(vtLog.INFO,'wait for connection',self)
            try:
                conn, addr = self.socket.accept()
                vtLog.vtLngCurCls(vtLog.INFO,'incomming connection',self)
                #print s,'accept'
                vtLog.vtLngCurCls(vtLog.INFO,'Connected by %s'%str(addr),self)
                sID=self.GenerateSessionID()
                sock=self.socketClass(self,conn,addr,sID)
                #sock.SendSessionID()
                #self.connections[addr]=sock
            except socket.timeout:
                pass
            except:
                vtLog.vtLngTB(self.GetOrigin())
        #print 'flisrv',self.port
        self._acquire()
        try:
            self.serving=False
            self.socket.close()
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self._release()
        vtLog.vtLngCur(vtLog.INFO,'stopped on %d'%self.port,self.GetOrigin())
    def RunSSLOld(self):
        self._acquire()
        self.serving=True
        self.connections={}
        self._release()
        HOST=None#'localhost'
        vtLog.vtLngCur(vtLog.INFO,'Run on port %d'%self.port,self.GetOrigin())
        for res in socket.getaddrinfo(HOST, self.port, socket.AF_UNSPEC, socket.SOCK_STREAM, 0, socket.AI_PASSIVE):
            #vtLog.vtLngCurCls(vtLog.INFO,repr(res),self)
            af, socktype, proto, canonname, sa = res
            try:
                self.socket = tsafe.Connection(self.ctx, socket.socket(af, socktype, proto))
                self.socket.set_app_data(self)
            except socket.error, msg:
                self.socket = None
                continue
            try:
                self.socket.bind(sa)
                self.socket.listen(3)
                self.socket.settimeout(1)
            except socket.error, msg:
                self.socket.close()
                self.socket = None
                continue
            break
        if self.socket is None:
            self.serving=False
            return
        self.socket.settimeout(0.1)
        while self.serving:
            if self.Is2Stop():
            #if self.stopping:
                break
            try:
                sock, addr = self.socket.accept()
                conn = tsafe.Connection(connection=sock)
                vtLog.vtLngCur(vtLog.INFO,'incomming connection;class:%s'%(
                            self.socketClass),self.GetOrigin())
                vtLog.vtLngCur(vtLog.INFO,'Connected by %s'%str(addr),
                            self.GetOrigin())
                sID=self.GenerateSessionID()
                sock=self.socketClass(self,conn,addr,sID)
                #sock.SendSessionID()
                #self.connections[addr]=sock
            except socket.timeout:
                pass
            except (SSL.WantReadError, SSL.WantWriteError, SSL.WantX509LookupError):
                pass
            except SSL.ZeroReturnError:
                #print 'SSL.ZeroReturnError'
                vtLog.vtLngTB(self.GetOrigin())
                #self.conn.shutdown()
                #self.serving=False
            except SSL.Error, errors:
                #print 'SSL.Error',errors
                vtLog.vtLngTB(self.GetOrigin())
                #traceback.print_exc()
                #self.serving=False
            except:
                vtLog.vtLngTB(self.GetOrigin())
        self._acquire()
        try:
            self.serving=False
            self.socket.close()
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self._release()
        vtLog.vtLngCur(vtLog.INFO,'stopped on %d'%self.port,self.GetOrigin())
