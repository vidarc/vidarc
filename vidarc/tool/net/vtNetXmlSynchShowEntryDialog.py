#Boa:Dialog:vtNetXmlSynchShowEntryDialog
#----------------------------------------------------------------------------
# Name:         vtNetXmlSynchShowEntryDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20070831
# CVS-ID:       $Id: vtNetXmlSynchShowEntryDialog.py,v 1.1 2007/10/02 03:55:16 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.xml.vtXmlGrpAttrTreeList
import vidarc.tool.xml.vtXmlTree

def create(parent):
    return vtNetXmlSynchShowEntryDialog(parent)

[wxID_VTNETXMLSYNCHSHOWENTRYDIALOG, wxID_VTNETXMLSYNCHSHOWENTRYDIALOGTRENTRY, 
] = [wx.NewId() for _init_ctrls in range(2)]

class vtNetXmlSynchShowEntryDialog(wx.Dialog):
    def _init_sizers(self):
        # generated method, don't edit
        self.fgsShow = wx.FlexGridSizer(cols=1, hgap=4, rows=3, vgap=4)

        self._init_coll_fgsShow_Items(self.fgsShow)
        self._init_coll_fgsShow_Growables(self.fgsShow)

        self.SetSizer(self.fgsShow)


    def _init_coll_fgsShow_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_fgsShow_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.trEntry, 0, border=0, flag=wx.EXPAND)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTNETXMLSYNCHSHOWENTRYDIALOG,
              name=u'vtNetXmlSynchShowEntryDialog', parent=prnt,
              pos=wx.Point(110, 110), size=wx.Size(400, 250),
              style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vtNet XML Synch Show Entry Dialog')
        self.SetClientSize(wx.Size(392, 223))

        self.trEntry = vidarc.tool.xml.vtXmlGrpAttrTreeList.vtXmlGrpAttrTreeList(cols=['tag',
              'value'], controller=False,
              id=wxID_VTNETXMLSYNCHSHOWENTRYDIALOGTRENTRY, master=True,
              name=u'trEntry', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(392, 223), style=0)

        self._init_sizers()

    def __init__(self, parent):
        self._init_ctrls(parent)
