#----------------------------------------------------------------------------
# Name:         vNetXml.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtNetXml.py,v 1.59 2009/10/03 22:35:46 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import thread,threading
import time,random
import socket,sha,binascii
import fnmatch,traceback
import sys,string,os,os.path
from cStringIO import StringIO

from vidarc.tool.log import vtLog
#from vidarc.tool.net import vtNetSock
from vidarc.tool.sock.vtSock import vtSock
from vidarc.tool.net.vtNetSrv import *
from vidarc.tool.xml.vtXmlDom import vtXmlDom
#from vidarc.tool.xml.vtXmlAcl import vtSecXmlLogin
from vidarc.tool.sec.vtSecXmlLogin import vtSecXmlLogin
from vidarc.tool.net.vtNetXmlLock import *
from vidarc.tool.vtThread import vtThread

from wx import PyTimer

import __init__
if __init__.SSL:
    from OpenSSL import SSL
    from OpenSSL import tsafe
    def verify_cb(conn, cert, errnum, depth, ok):
        traceback.print_stack()
        # This obviously has to be updated
        print 'errnum:%s;depth:%s;ok:%s'%(`errnum`,`depth`,`ok`)
        self=conn.get_app_data()
        vtLog.vtLngCurCls(vtLog.INFO,'Got certificate: %s' % cert.get_subject(),self)
        vtLog.vtLngCurCls(vtLog.ERROR,'errnum:%s;depth:%s;ok:%s'%(`errnum`,`depth`,`ok`),self)
        if ok:
            pass
            self.iVerify=1
        else:
            self.iVerify=-1
        #self.semVerify.release()
        return ok

BUFFER_MAX=40*1024*1024

class vtNetXmlSock(vtLog.vtLogMixIn,vtSock):
    def __initCtrl__(self,*args,**kwargs):
        vtSock.__initCtrl__(self,*args,**kwargs)
        self.lCmds=[
            ('lock_node',self.__lock_node__),
            ('unlock_node',self.__unlock_node__),
            ('get_node',self.__get_node__),
            ('get_synch_node',self.__get_synch_node__),
            ('set_node',self.__set_node__),
            ('set_full_node',self.__set_node_full__),
            ('add_node',self.__add_node__),
            ('del_node',self.__del_node__),
            ('move_node',self.__move_node__),
            ('get_content',self.__get_content__),
            ('count_key',self.__count_key__),
            ('count_element',self.__count_element__),
            ('count_node',self.__count_node__),
            ('get_config',self.__get_config__),
            ('get_special',self.__get_special__),
            ('get_acl',self.__get_acl__),
            ('get_keys',self.__get_keys__),
            ('alias',self.__alias__),
            ('login',self.__login__),
            ('loggedin',self.__loggedin__),
            ('browse',self.__browse__),
            ('hypbrowse',self.__hyper_browse__),
            ('cmd',self.__cmd__),
            ('lock_request',self.__lock_request__),
            ('lock_release',self.__lock_release__),
            ('lockfull_request',self.__lock_full_request__),
            ('lockfull_release',self.__lock_full_release__),
            (u'sessionID',self.__sessionID__),
            (u'shutdown',self.__shutdown__)]
        self.loggedin=False
        self.loginreq=True          # 080202 wro does alias require login
        self.objLogin=None
        self.sLogin4Audit=u'__:-1'
        self.doc=None
        self.appl_alias=''
        self.usr=''
        self.dLockBlock={}
    def DoVerifyOld(self,conn, cert, errnum, depth, ok):
        vtLog.vtLngCur(vtLog.INFO,'ok:%d'%(ok),self.appl_alias)
        if ok:
            self.serving=True
            vtLog.vtLngCur(vtLog.INFO,'SSL got certificate: %s'%(cert.get_subject()),self.appl_alias)
        else:
            self.serving=False
            #vtLog.vtLngCur(vtLog.ERROR,'errnum:%d'%(errnum),self.appl_alias)
            vtLog.vtLngCurCls(vtLog.ERROR,'errnum:%s;depth:%s;ok:%s'%(`errnum`,`depth`,`ok`),self.appl_alias)
        if self.serving==False:
            vtLog.vtLngCur(vtLog.INFO,'SSL verify fault',self.appl_alias)
        #self.SendSessionID()
    def verify_cb_old(self,conn, cert, errnum, depth, ok):
        # This obviously has to be updated
        vtLog.CallStack('')
        vtLog.vtLngCurCls(vtLog.INFO,'Got certificate: %s' % cert.get_subject(),self)
        vtLog.vtLngCurCls(vtLog.ERROR,'errnum:%s;depth:%s;ok:%s'%(`errnum`,`depth`,`ok`),self)
        return ok
    def LockBlock(self,id):
        self.dLockBlock[id]=time.time()
    def IsLockBlocked(self,id,zDelay):
        try:
            z=self.dLockBlock[id]
            if (z+zDelay) < time.time():
                self.RemoveLockBlock(zDelay)
                return False
            self.RemoveLockBlock(zDelay)
            return True
        except:
            self.RemoveLockBlock(zDelay)
            return False
    def RemoveLockBlock(self,zDelay):
        zNow=time.time()
        for k in self.dLockBlock.keys():
            if (self.dLockBlock[k]+zDelay) < zNow:
                del self.dLockBlock[k]
    def GetLockBlockDiff(self,id,zDelay):
        try:
            z=self.dLockBlock[id]
            zDiff=(z+zDelay)-time.time()
            if zDiff<0:
                zDiff=1
            return zDiff
        except:
            return -1
    def addNode(self,par,node):
        if self.doc is None:
            return
        self.doc.acquire()
        try:
            self.doc.addNode(self,node)
            idPar=self.doc.getAttribute(par,self.doc.attr)
            id=self.doc.getAttribute(node,self.doc.attr)
            content=self.doc.GetNodeXmlContent(node)
        except:
            vtLog.vtLngTB(self.appl_alias)
        self.doc.release()
        m=sha.new()
        m.update(content)
        m.update(self.sessionID)
        fp=m.hexdigest()
        self.SendTelegram('add_node|'+fp+','+idPar+','+id+','+content)
    def delNode(self,node):
        if self.doc is None:
            return
        id=self.doc.getAttribute(node,self.doc.attr)
        self.SendTelegram('del_node|'+id)
    def GetUsr(self):
        return self.usr
    def IsLoggedIn(self):
        return self.loggedin
    def CheckPause(self):
        try:
            if self.paused!=self.pausing:
                self.paused=self.pausing
                if vtLog.vtLngIsLogged(vtLog.INFO):
                    vtLog.vtLngCS(vtLog.INFO,
                                'pause=%s'%self.paused,origin=self.appl_alias)
        except:
            traceback.print_exc()
            vtLog.vtLngCS(vtLog.ERROR,
                        traceback.format_exc())
    def SocketClosed(self):
        vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        try:
            if self.srv is not None:
                self.srv.StopSocket(self.adr,self)
        except:
            try:
                vtLog.vtLngTB(self.appl_alias)
            except:
                vtLog.vtLngCS(vtLog.ERROR,
                        traceback.format_exc())
        self.doc=None
        self.objLogin=None
        del self.lCmds
        vtSock.SocketClosed(self)
    def __checkLogin__(self):
        if self.loggedin:
            return 1
        else:
            self.SendTelegram('login')
            return 0
    def __alias__(self,val):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCS(vtLog.INFO,'alias;%s'%val,origin=self.appl_alias)
        #if self.__checkLogin__():
        try:
            self.srv.acquire()
            self.doc=self.srv.GetServedXml(val[1:],self.adr,self,bThread=True)
            if self.doc is None:
                self.appl_alias=''
                self.SendTelegram('alias|not served')
            else:
                #self.SendTelegram('alias|served')  # 080202:wro makes no sense
                self.appl_alias=val[1:]
                # check is to login?
                ret=self.srv.IsLogginNeeded(self.adr[0],val[1:])
                if vtLog.vtLngIsLogged(vtLog.INFO):
                    vtLog.vtLngCur(vtLog.INFO,'isLogginNeeded:%d'%ret,self.appl_alias)
                if ret>0:
                    self.loginreq=True  # 080202:wro makes no sense
                    self.loggedin=False  # 080202:wro makes no sense
                    if self.loggedin:  # 080202:wro makes no sense
                        self.SendTelegram('alias|served')
                    else:
                        self.SendTelegram('alias|served')
                        #self.SendTelegram('alias|login')
                        self.SendTelegram('login')
                elif ret==0:
                    #no login required
                    self.loginreq=False
                    self.loggedin=True
                    self.SendTelegram('alias|served')
                    pass
                else:
                    # forget it NO ACCESS
                    self.SendTelegram('alias|rejected')
                    #self.SendTelegram('rejected')
        except:
            vtLog.vtLngCS(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
        self.srv.release()
    def __login__(self,val):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'%s'%val,self.appl_alias)
        try:
            self.srv.acquire()
            i=string.find(val,',')
            if i>0:
                self.objLogin,iRes=self.srv.CheckLogin(self.adr[0],
                        self.appl_alias,self.sessionID,val[1:i],
                        binascii.a2b_uu(val[i+1:]))
                if self.objLogin is None:
                    self.sLogin4Audit=u'__:-1'
                else:
                    self.sLogin4Audit=self.objLogin.GetAuditName()
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'%s;iRet:%d;adr:%s'%(self.objLogin,iRes,self.adr),self.appl_alias)
                #vtLog.CallStack('')
                #print 'addr',self.adr
                #print self.objLogin
                if iRes>0:
                    sInfo=''
                    try:
                        sInfo=self.objLogin.GetStr()
                    except:
                        sInfo=',,,,,'
                        vtLog.vtLngTB('')
                    try:
                        self.doc.audit.sec(sInfo)
                    except:
                        vtLog.vtLngTB('')
                    self.SendTelegram('loggedin|'+sInfo)
                    #self.SendTelegram('alias|served')       # 080202
                    self.loggedin=True
                    if 1==0:
                        if val[1:i]=='wal':
                            self.objLogin=vtSecXmlLogin(1,15,16)
                        else:
                            self.objLogin=vtSecXmlLogin(0,15,16)
                        #print self.objLogin
                    self.usr=val[1:i]
                    self.srv.release()
                    return
                else:
                    self.SendTelegram('rejected')
            self.SendTelegram('login')
        except:
            vtLog.vtLngCS(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
        self.srv.release()
    def __sessionID__(self,val):
        pass
    def __loggedin__(self,val):
        pass
    def __browse__(self,val):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCS(vtLog.INFO,'browse;%s'%val,origin=self.appl_alias)
        try:
            l=[]
            if len(val)>1:
                appl=val[1:]
                self.srv.acquire()
                try:
                    l=self.srv.GetServedXmlLst(appl)
                except:
                    vtLog.vtLngCS(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
                self.srv.release()
                self.SendTelegram('browse|'+';'.join(l))
                return
        except:
            vtLog.vtLngCS(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
        self.SendTelegram('browse|')
    def __hyper_browse__(self,val):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            #vtLog.vtLngCS(vtLog.INFO,'browse;%s'%val,origin=self.appl_alias)
            vtLog.vtLngCurCls(vtLog.INFO,'%s'%(val),self)
        try:
            l=[]
            i=string.find(val,',')
            if i>0:
                usr=val[1:i]
                appl=val[i+1:]
                self.srv.acquire()
                try:
                    l=self.srv.GetServedXml4UsrLst(usr,appl)
                except:
                    vtLog.vtLngCS(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
                self.srv.release()
                if l is not None:
                    self.SendTelegram('hypbrowse|'+';'.join(l))
                    return
        except:
            vtLog.vtLngCS(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
        self.SendTelegram('hypbrowse|')
    def __cmd__(self,val):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'%s'%val,self.appl_alias)
        try:
            i=string.find(val,'|')
            if i>=0:
                iFlt=1
                j=string.find(val,',',i)
                if j>0:
                    iFlt=2
                    fp=val[i+1:j]
                    sData=val[j+1:]
                    m=sha.new()
                    m.update(sData)
                    m.update(self.sessionID)
                    if m.hexdigest()==val[i+1:j]:
                        kwargs={}
                        strs=sData.split(',')
                        sAction=strs[0]
                        try:
                            if len(strs)>1:
                                for s in strs[1].split(';'):
                                    k=s.find(':')
                                    key=s[:k]
                                    v=s[k+1:]
                                    kwargs[key]=v
                        except:
                            pass
                        #self.srv.acquire()
                        try:
                            node=None
                            if '|id' in kwargs:
                                try:
                                    node=self.doc.getNodeByIdNum(long(kwargs['|id']))
                                except:
                                    pass
                            if node is None:
                                node=self.doc.getBaseNode()
                            bOkExec=self.doc.IsAccessOk(node,self.objLogin,self.doc.ACL_MSK_EXEC)
                            bOkBuild=self.doc.IsAccessOk(node,self.objLogin,self.doc.ACL_MSK_BUILD)
                            try:
                                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                    vtLog.vtLngCur(vtLog.DEBUG,'cmd:%s;kwargs:%s'\
                                            %(strs[0],vtLog.pformat(kwargs)),self.appl_alias)
                                ret=self.srv.DoCmd(bOkExec,bOkBuild,self.appl_alias,sAction,**kwargs)
                            except:
                                ret=-1
                                vtLog.vtLngTB(self.appl_alias)
                        except:
                            ret=-1
                            vtLog.vtLngTB(self.appl_alias)
                        #self.srv.release()
                        s=';'.join([sAction,str(ret)])
                        m=sha.new()
                        m.update(s)
                        m.update(self.sessionID)
                        self.SendTelegram(''.join(['cmd|',','.join([m.hexdigest(),s])]))
                        #self.__get_node__('|%s'%self.doc.getKey(node))
        except:
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),self.appl_alias)
        self.SendTelegram('browse|')
    def __shutdown__(self,val):
        pass
    def __count_key__(self,val):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCS(vtLog.INFO,'__count_key__',origin=self.appl_alias)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCS(vtLog.DEBUG,val,origin=self.appl_alias)
        if self.doc is None:
            return
        try:
            self.doc.acquire()
            iLen=len(self.doc.getIds().getIDs())
            self.doc.release()
            self.SendTelegram('count_key|'+str(iLen))
        except:
            vtLog.vtLngCS(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
            self.doc.release()
    def __count_element__(self,val):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCS(vtLog.INFO,'__count_element__',origin=self.appl_alias)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCS(vtLog.DEBUG,val,origin=self.appl_alias)
        if self.doc is None:
            return
        try:
            self.doc.acquire()
            iLen=self.doc.GetElementCount()
            self.doc.release()
            self.SendTelegram('count_element|'+str(iLen))
        except:
            vtLog.vtLngCS(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
            self.doc.release()
    def __count_node__(self,val):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCS(vtLog.INFO,'__count_node__',origin=self.appl_alias)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCS(vtLog.DEBUG,val,origin=self.appl_alias)
        if self.doc is None:
            return
        try:
            self.doc.acquire()
            iLen=self.doc.GetElementAttrCount()
            self.doc.release()
            self.SendTelegram('count_node|'+str(iLen))
        except:
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),self.appl_alias)
            self.doc.release()
    def __get_content__(self,val):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCS(vtLog.INFO,'__get_content__',origin=self.appl_alias)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCS(vtLog.DEBUG,val,origin=self.appl_alias)
        if self.doc is None:
            return
        try:
            self.doc.acquire()
            content=self.doc.GetXmlContent()#.encode('ISO-8859-1')
            self.doc.release()
            #print content[:300]
            m=sha.new()
            m.update(content)
            m.update(self.sessionID)
            fp=m.hexdigest()
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'send data:%d'%len(content),self.appl_alias)
            #self.SendTelegram('get_content|'+fp+','+content)
            self.SendTelegram(''.join(['get_content|',fp,',',content]))
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'send data:%d done'%len(content),self.appl_alias)
        except:
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),self.appl_alias)
            self.doc.release()
    def __get_config__(self,val):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCS(vtLog.INFO,'__get_config__',origin=self.appl_alias)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,val,self.appl_alias)
        if self.doc is None:
            return
        try:
            if len(val)==1:
                self.doc.acquire()
                cfgNode=self.doc.getChild(self.doc.getRoot(),'config')
                if cfgNode is not None:
                    content=self.doc.GetNodeXmlContent(cfgNode,True)
                else:
                    content=''
                self.doc.release()
                m=sha.new()
                m.update(content)
                m.update(self.sessionID)
                fp=m.hexdigest()
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'',self.appl_alias)
                    vtLog.vtLngCur(vtLog.DEBUG,'sessionID:%s;dig:%s;len:%d'%(
                            self.sessionID,fp,len(content)),self.appl_alias)
                #self.SendTelegram('get_config|'+fp+','+content)
                self.SendTelegram(''.join(['get_config|',fp,',',content]))
                return
            if vtLog.vtLngIsLogged(vtLog.WARN):
                vtLog.vtLngCur(vtLog.WARN,'fault',self.appl_alias)
        except:
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),self.appl_alias)
            self.doc.release()
    def __get_special__(self,val):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCS(vtLog.INFO,'__get_special__',origin=self.appl_alias)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCS(vtLog.DEBUG,val,origin=self.appl_alias)
        if self.doc is None:
            return
        try:
            if len(val)>1:
                sTag=val[1:]
                self.doc.acquire()
                cfgNode=self.doc.getChild(self.doc.getRoot(),sTag)
                if cfgNode is not None:
                    content=self.doc.GetNodeXmlContent(cfgNode,False,True)
                else:
                    content=''
                self.doc.release()
                m=sha.new()
                m.update(content)
                m.update(self.sessionID)
                fp=m.hexdigest()
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'',self.appl_alias)
                #self.SendTelegram('get_special|'+fp+','+sTag+','+content)
                self.SendTelegram(''.join(['get_special|',fp,',',sTag,',',content]))
                return
            if vtLog.vtLngIsLogged(vtLog.WARN):
                vtLog.vtLngCur(vtLog.WARN,'fault',self.appl_alias)
        except:
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),self.appl_alias)
            self.doc.release()
    def __get_acl__(self,val):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.appl_alias)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,val,origin=self.appl_alias)
        if self.doc is None:
            return
        try:
            self.doc.acquire()
            d=self.doc.GetAcl4Login(self.objLogin)
            ioContent=StringIO()
            ioContent.write('<?xml version="1.0" encoding="ISO-8859-1"?>')
            ioContent.write('<acl>')
            keys=d.keys()
            keys.sort()
            for k in keys:
                sAcl='0x%08x'%d[k]
                ioContent.write('<')
                ioContent.write(k)
                ioContent.write(' acl="')
                ioContent.write(sAcl)
                ioContent.write('"/>')
            ioContent.write('</acl>')
            self.doc.release()
            content=ioContent.getvalue()
            m=sha.new()
            m.update(content)
            m.update(self.sessionID)
            fp=m.hexdigest()
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'%s'%content,self.appl_alias)
                vtLog.vtLngCur(vtLog.DEBUG,'sessionID:%s;dig:%s;len:%d'%(
                        self.sessionID,fp,len(content)),self.appl_alias)
            self.SendTelegram(''.join(['get_acl|',fp,',',content]))
        except:
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),self.appl_alias)
            self.doc.release()
    def __proc_get_keys__(self,par,parId,node,ioContent,ioCache):
        bOk=self.doc.IsAccessOk(node,self.objLogin,self.doc.ACL_MSK_READ)
        if bOk:
            id=self.doc.getKeyNum(node)
            fp=self.doc.GetFingerPrintAndStore(node)
            sID=str(id)
            sTag=self.doc.getTagName(node)
            ioContent.write(''.join([',',sID,';',fp,';',sTag,';',parId]))
            ioCache.write(''.join([sID,';',sTag,';',parId,',']))
            #ioCache.write(''.join([sID,';',sTag,',']))
            return 1
        else:
            return 0
    def __clearCache__(self):
        try:
            n=self.doc.getChild(self.doc.getRoot(),'security_acl')
            if n is not None:
                nC=self.doc.getChild(n,'__cache__')
                if nC is not None:
                    self.doc.__deleteNode__(nC,n)
                    #nC.unlinkNode()
                    #nC.freeNode()
        except:
            vtLog.vtLngTB(self.appl_alias)
    def __get_keys__(self,val):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCS(vtLog.INFO,'__get_keys__',origin=self.appl_alias)
        if self.doc is None:
            return
        try:
            if len(val)==1:
                #keys=map(str,[-1]+self.doc.getIds().GetIDs())
                #content=string.join(keys,',')
                #print self.doc.getIds().GetFPs().keys()
                #if vtLog.vtLngIsLogged(vtLog.DEBUG):
                #    vtLog.vtLngCur(vtLog.DEBUG,vtLog.pformat(self.doc.getIds().GetFPs().keys()),self.appl_alias)
                self.doc.acquire()
                zStart=time.clock()
                content=None
                nodeCache=None
                ioContent=StringIO()
                ioCache=StringIO()
                n=self.doc.getChild(self.doc.getRoot(),'security_acl')
                if n is not None:
                    nC=self.doc.getChildForced(n,'__cache__')
                    idUsr=self.objLogin.GetUsrId()
                    if idUsr>=0:
                        sKind='usr'
                        nodeCache=self.doc.getChildForced(nC,'%s%08d'%(sKind,self.objLogin.GetUsrId()))
                        sVal=self.doc.getText(nodeCache)
                        if sVal!='':
                            try:
                                fps=self.doc.getIds().GetFPs()
                                lCacheID=sVal.split(',')#[long(t[0])]
                                #print len(lCacheID),self.doc.iElemIDCount
                                ioContent.write(';'.join([',-1',self.doc.getFingerPrint(self.doc.getBaseNode()),
                                            self.doc.getTagName(self.doc.getBaseNode())]))
                                for s in lCacheID:
                                    i=s.find(';')
                                    if i<1:
                                        continue
                                    sID=s[:i]
                                    id=long(sID)
                                    n=self.doc.getNodeByIdNum(id)
                                    p=self.doc.getParent(n)
                                    if id in fps:
                                        ioContent.write(''.join([',',sID,';',fps[id],s[i:]]))
                                        #ioContent.write(''.join([',',sID,';',fps[id],s[i:],';',str(self.doc.getKeyNum(p))]))
                                content=ioContent.getvalue()
                            except:
                                vtLog.vtLngTB(self.appl_alias)
                                content=None
                                ioContent=StringIO()
                if content is None:
                    ioContent.write(';'.join([',-1',self.doc.getFingerPrint(self.doc.getBaseNode()),
                            self.doc.getTagName(self.doc.getBaseNode())]))
                    if 0:
                        for k,v in self.doc.getIds().GetFPs().items():
                            node=self.doc.getNodeByIdNum(k)
                            if node is None:
                                continue
                            bOk=self.doc.IsAccessOk(node,self.objLogin,self.doc.ACL_MSK_READ)
                            if bOk:
                                ioContent.write(''.join([',',str(k),';',v,';',self.doc.getTagName(node)]))
                    else:
                        self.doc.procChildsKeysRecHierarchy(None,'-1',self.doc.getBaseNode(),
                                self.__proc_get_keys__,ioContent,ioCache)
                    content=ioContent.getvalue()
                    if nodeCache is not None:
                        self.doc.setText(nodeCache,ioCache.getvalue())
                if vtLog.vtLngIsLogged(vtLog.INFO):
                    vtLog.vtLngCur(vtLog.INFO,'calculation:%6.3f'%(time.clock()-zStart),self.appl_alias)
                self.doc.release()
                m=sha.new()
                m.update(content)
                m.update(self.sessionID)
                fp=m.hexdigest()
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'keys:%s'%content,self.appl_alias)
                #self.SendTelegram('get_keys|'+fp+content)
                self.SendTelegram(''.join(['get_keys|',fp,content]))
                return
            if vtLog.vtLngIsLogged(vtLog.WARN):
                vtLog.vtLngCur(vtLog.WARN,'fault',self.appl_alias)
        except:
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),self.appl_alias)
            self.doc.release()
    def __get_node__(self,val):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,val,origin=self.appl_alias)
        if self.doc is None:
            vtLog.vtLngCur(vtLog.ERROR,'doc is None',self.appl_alias)
            return
        try:
            if len(val)>1:
                id=val[1:]
                self.doc.acquire()
                node=self.doc.getNodeByIdNum(long(id))
                bOk=self.doc.IsAccessOk(node,self.objLogin,self.doc.ACL_MSK_READ)
                try:
                    self.doc.audit.sec(self.sLogin4Audit,'read',str(bOk))
                except:
                    vtLog.vtLngTB(self.appl_alias)
                #print bOk
                if vtLog.vtLngIsLogged(vtLog.INFO):
                    vtLog.vtLngCur(vtLog.INFO,'id:%s;access:%d'%(id,bOk),self.appl_alias)
                if bOk:
                    content=self.doc.GetNodeXmlContent(node)
                else:
                    content=''
                try:
                    self.doc.audit.write('get',content,origin=self.getNodeAuditSec(node))
                except:
                    vtLog.vtLngTB(self.appl_alias)
                
                self.doc.release()
                m=sha.new()
                m.update(content)
                m.update(self.sessionID)
                fp=m.hexdigest()
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'get node id:%s'%(id),self.appl_alias)
                #self.SendTelegram('get_node|'+fp+','+id+','+content)
                self.SendTelegram(''.join(['get_node|',fp,',',id,',',content]))
                return
            if vtLog.vtLngIsLogged(vtLog.WARN):
                vtLog.vtLngCur(vtLog.WARN,'fault',self.appl_alias)
        except:
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),self.appl_alias)
            self.doc.release()
    def __get_synch_node__(self,val):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,val,origin=self.appl_alias)
        if self.doc is None:
            vtLog.vtLngCur(vtLog.ERROR,'doc is None',self.appl_alias)
            return
        #if len(val)>1:
        try:
            self.doc.acquire()
            i=string.find(val,'|')
            if i>=0:
                iFlt=1
                j=string.find(val,',',i)
                if j>0:
                    id=val[i+1:j]
                    k=val.find(',',j+1)
                    if k>0:
                        fingerprint=val[j+1:k]
                        tagname=val[k+1:]
                    else:
                        fingerprint=val[j+1:]
                        tagname=None
                    iID=long(id)
                    if iID<0:
                        node=self.doc.getBaseNode()
                    else:
                        node=self.doc.getNodeByIdNum(iID)
                    #print self.objLogin
                    bOk=self.doc.IsAccessOk(node,self.objLogin,self.doc.ACL_MSK_READ)
                    try:
                        self.doc.audit.sec(self.sLogin4Audit,'read',str(bOk))
                    except:
                        vtLog.vtLngTB(self.appl_alias)
                    #print bOk
                    if vtLog.vtLngIsLogged(vtLog.INFO):
                        vtLog.vtLngCur(vtLog.INFO,'id:%s;access:%d'%(id,bOk),self.appl_alias)
                    if bOk:
                        bFP=self.doc.hasAttribute(node,'fp')
                        sFP=self.doc.GetFingerPrintAndStore(node)
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCur(vtLog.DEBUG,'id:%s;lfp:%s;rfp:%s'%(id,sFP,fingerprint),self.appl_alias)
                        if bFP:
                            if sFP!=fingerprint:
                                # 070311:wro FIXME recalculate fingerprint here???
                                # recalculate FP to be sure
                                self.doc.clearFingerPrint(node)
                                sFP=self.doc.GetFingerPrintAndStore(node)
                        if tagname is not None:
                            if self.doc.getTagName(node)!=tagname:
                                if vtLog.vtLngIsLogged(vtLog.INFO):
                                    vtLog.vtLngCur(vtLog.INFO,'id:%s;local tagname:%s;remote tagname:%s'%(id,
                                        self.doc.getTagName(node),tagname),self.appl_alias)
                                fingerprint=None
                        if sFP!=fingerprint:
                            content=self.doc.GetNodeXmlContent(node)
                            m=sha.new()
                            m.update(content)
                            m.update(self.sessionID)
                            fp=m.hexdigest()
                            level=self.doc.getLevel(node)
                            parID=self.doc.getKey(self.doc.getParent(node))
                            #self.SendTelegram('get_synch_node|'+fp+','+parID+','+id+','+str(level)+','+content)
                            self.SendTelegram(''.join(['get_synch_node|',fp,',',parID,',',id,',',str(level),',',content]))
                        else:
                            parID=self.doc.getKey(self.doc.getParent(node))
                            #self.SendTelegram('get_synch_node|'+','+parID+','+id+','+',')
                            self.SendTelegram(''.join(['get_synch_node|',',',parID,',',id,',',',']))
                    else:
                        # acl fault, access denied
                        #self.SendTelegram('get_synch_node|'+',-2,'+id+',,')
                        self.SendTelegram(''.join(['get_synch_node|',',-2,',id,',,']))
                    self.doc.release()
                    return
            if vtLog.vtLngIsLogged(vtLog.WARN):
                vtLog.vtLngCur(vtLog.WARN,'get synch node',self.appl_alias)
            self.doc.release()
        except:
            vtLog.vtLngCS(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
        self.doc.release()
        #self.SendTelegram('get_synch_node|'+id+',fault')
    def __set_node__(self,val):
        #if vtLog.vtLngIsLogged(vtLog.INFO):
        #    vtLog.vtLngCS(vtLog.INFO,'__set_node__',origin=self.appl_alias)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,val,origin=self.appl_alias)
        if self.doc is None:
            vtLog.vtLngCur(vtLog.ERROR,'doc is None',self.appl_alias)
            return
        id='-2'
        try:
            iFlt=0
            i=string.find(val,'|')
            if i>=0:
                iFlt=1
                j=string.find(val,',',i)
                if j>0:
                    iFlt=2
                    k=string.find(val,',',j+1)
                    if k>0:
                        iFlt=3
                        l=string.find(val,',',k+1)
                        if l>0:
                            iFlt=4
                            fp=val[i+1:j]
                            m=sha.new()
                            m.update(val[l+1:])
                            m.update(self.sessionID)
                            if m.hexdigest()==val[i+1:j]:
                                idLock=val[j+1:k]
                                id=val[k+1:l]
                                if vtLog.vtLngIsLogged(vtLog.INFO):
                                    vtLog.vtLngCur(vtLog.INFO,
                                        'idLock:%s id:%s'%(idLock,id),
                                        origin=self.appl_alias)
                                if 1==1:
                                    node=self.doc.getNodeByIdNum(long(idLock))
                                    if node is None:
                                        vtLog.vtLngCur(vtLog.ERROR,'idLock:%s'%(idLock),self.appl_alias)
                                        #self.SendTelegram('set_node|'+idLock+','+id+',fault')
                                        self.SendTelegram(''.join(['set_node|',idLock,',',id,',fault']))
                                        return
                                    self.doc.acquire()
                                    try:
                                        bOk=self.doc.IsAccessOk(node,self.objLogin,self.doc.ACL_MSK_WRITE)
                                        #print bOk
                                        try:
                                            self.doc.audit.sec(self.sLogin4Audit,'write',str(bOk))
                                        except:
                                            vtLog.vtLngTB('')
                                        if vtLog.vtLngIsLogged(vtLog.INFO):
                                            vtLog.vtLngCur(vtLog.INFO,
                                                'idLock:%s;access:%d'%(idLock,bOk),self.appl_alias)
                                        if bOk:
                                            self.doc.clearFingerPrint(node)
                                            if idLock!=id:
                                                if vtLog.vtLngIsLogged(vtLog.INFO):
                                                    vtLog.vtLngCur(vtLog.INFO,
                                                        'change id from %s to %s'%(idLock,id),
                                                        origin=self.appl_alias)
                                                self.doc.changeId(node,id)
                                                idNew=self.doc.getKey(node)
                                            else:
                                                idNew=id
                                            tmpNode=self.doc.SetNodeXmlContent(node,val[l+1:],self.sLogin4Audit)
                                            idNew=self.doc.getKey(tmpNode)
                                            ids=self.doc.getIds()
                                            ids.__calc__(long(idNew),self.doc)
                                            self.doc.clearFingerPrint(node)
                                            self.doc.GetFingerPrintAndStore(node)
                                            #print '*****'
                                            #print val[l+1:]
                                            valUpd=self.doc.GetNodeXmlContent(node,False,False)     #081222:wro get current xml
                                            #print valUpd
                                            #print len(val[l+1:]),len(valUpd)
                                            #print '------'
                                            #FIXME
                                            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                                vtLog.vtLngCur(vtLog.DEBUG,'id:%s idNew:%s'%(idLock,idNew),self.appl_alias)
                                            iRet=self.srv.Modify(self.appl_alias,self.adr,long(idLock),long(idNew),valUpd)
                                            #self.SendTelegram('set_node|'+idLock+','+idNew+',ok')
                                            self.SendTelegram(''.join(['set_node|',idLock,',',idNew,',ok']))
                                        else:
                                            #self.SendTelegram('set_node|'+idLock+','+idNew+',acl')
                                            self.SendTelegram(''.join(['set_node|',idLock,',',idNew,',acl']))
                                        self.doc.release()
                                    except:
                                        vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
                                        #self.SendTelegram('set_node|'+id+',fault')
                                        self.SendTelegram(''.join(['set_node|',id,',fault']))
                                        self.doc.release()
                                else:
                                    #self.SendTelegram('set_node|'+id+','+idNew+',locked')
                                    self.SendTelegram(''.join(['set_node|',id,',',idNew,',locked']))
                                return
                                self.SendTelegram('set_node|'+id+',fault')
            if vtLog.vtLngIsLogged(vtLog.WARN):
                vtLog.vtLngCur(vtLog.WARN,'fault;%s'%val,origin=self.appl_alias)
        except:
            vtLog.vtLngCS(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
        #self.SendTelegram('set_node|'+id+',fault')
        self.SendTelegram(''.join(['set_node|',id,',fault']))
    def __set_node_full__(self,val):
        #if vtLog.vtLngIsLogged(vtLog.INFO):
        #    vtLog.vtLngCS(vtLog.INFO,'__set_node_full__',origin=self.appl_alias)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,val,origin=self.appl_alias)
        if self.doc is None:
            vtLog.vtLngCur(vtLog.ERROR,'doc is None',self.appl_alias)
            return
        id='-2'
        try:
            iFlt=0
            i=string.find(val,'|')
            if i>=0:
                iFlt=1
                j=string.find(val,',',i)
                if j>0:
                    iFlt=2
                    k=string.find(val,',',j+1)
                    if k>0:
                        iFlt=3
                        l=string.find(val,',',k+1)
                        if l>0:
                            iFlt=4
                            fp=val[i+1:j]
                            m=sha.new()
                            m.update(val[l+1:])
                            m.update(self.sessionID)
                            if m.hexdigest()==val[i+1:j]:
                                idLock=val[j+1:k]
                                id=val[k+1:l]
                                if vtLog.vtLngIsLogged(vtLog.INFO):
                                    vtLog.vtLngCur(vtLog.INFO,
                                        'idLock:%s id:%s;%s'%(idLock,id,val[l+1:]),
                                        origin=self.appl_alias)
                                if 1==1:
                                    node=self.doc.getNodeByIdNum(long(idLock))
                                    if node is None:
                                        #self.SendTelegram('set_full_node|'+idLock+','+id+',fault')
                                        self.SendTelegram(''.join(['set_full_node|',idLock,',',id+',fault']))
                                        return
                                    self.doc.acquire()
                                    try:
                                        bOk=self.doc.IsAccessOk(node,self.objLogin,self.doc.ACL_MSK_WRITE)
                                        #print bOk
                                        try:
                                            self.doc.audit.sec(self.sLogin4Audit,'write',str(bOk))
                                        except:
                                            vtLog.vtLngTB('')
                                        if vtLog.vtLngIsLogged(vtLog.INFO):
                                            vtLog.vtLngCur(vtLog.INFO,
                                                'idLock:%s;access:%d'%(idLock,bOk),self.appl_alias)
                                        if bOk:
                                            idOld=self.doc.getKey(node)
                                            self.doc.clearFingerPrintFull(node)
                                            node=self.doc.SetNodeXmlContentFull(node,val[l+1:],self.sLogin4Audit)#.encode('ISO-8859-1'))
                                            #node=self.doc.SetNodeXmlContentFull(node,val[l+1:].decode('ISO-8859-1'))
                                            idNew=self.doc.getKey(node)
                                            if idOld!=idNew:
                                                if vtLog.vtLngIsLogged(vtLog.INFO):
                                                    vtLog.vtLngCur(vtLog.INFO,
                                                        'change id from %s to %s'%(idOld,id),
                                                        origin=self.appl_alias)
                                                self.doc.changeId(node,id)
                                                idNew=self.doc.getKey(node)
                                                pass
                                            self.doc.clearFingerPrintFull(node)
                                            self.doc.calcFingerPrintFull(node)
                                            
                                            idNew=self.doc.getKey(node)
                                            ids=self.doc.getIds()
                                            ids.__calc__(long(idNew),self.doc)
                                            def __calc__(n,ids,doc):
                                                idNew=doc.getKey(n)
                                                ids.__calc__(long(idNew),doc)
                                                self.doc.procChildsKeys(n,ids,doc)
                                                return 0
                                            self.doc.procChildsKeys(node,__calc__,ids,doc)
                                            
                                            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                                vtLog.vtLngCur(vtLog.DEBUG,
                                                    'idOld:%s;idNew:%s'%(idOld,idNew),
                                                    origin=self.appl_alias)
                                            iRet=self.srv.ModifyFull(self.appl_alias,self.adr,long(idOld),long(idNew),val[l+1:])
                                            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                                vtLog.vtLngCur(vtLog.DEBUG,'set node full id:%s idNew:%s'%(id,idNew),self.appl_alias)
                                        else:
                                            #self.SendTelegram('set_node|'+id+','+id+',acl')
                                            self.SendTelegram(''.join(['set_node|',id,',',id,',acl']))
                                        self.doc.release()
                                    except:
                                        vtLog.vtLngTB(origin=self.appl_alias)
                                        #self.SendTelegram('set_resp_full|'+id+','+id+',fault')
                                        self.SendTelegram(''.join(['set_resp_full|',id,',',id,',fault']))
                                        self.doc.release()
                                    #self.SendTelegram('set_resp_full|'+id+','+idNew+',ok')
                                #else:
                                #    self.SendTelegram('set_resp_full|'+id+','+idNew+',locked')
                                return
                                self.SendTelegram('set_full_node|'+id+',fault')
            if vtLog.vtLngIsLogged(vtLog.WARN):
                vtLog.vtLngCS(vtLog.WARN,'set node full fault;%s'%val,origin=self.appl_alias)
        except:
            vtLog.vtLngTB(origin=self.appl_alias)
        #self.SendTelegram('set_resp_full|'+id+','+id+',fault')
        self.SendTelegram(''.join(['set_resp_full|',id,',',id,',fault']))
    def __lock_node__(self,val):
        #if vtLog.vtLngIsLogged(vtLog.INFO):
        #    vtLog.vtLngCS(vtLog.INFO,'__lock_node__',origin=self.appl_alias)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,val,origin=self.appl_alias)
        if self.doc is None:
            vtLog.vtLngCur(vtLog.ERROR,'doc is None',self.appl_alias)
            return
        id='-2'
        try:
            if len(val)>1:
                id=val[1:]
                try:
                    #node=self.doc.getNodeByIdNum(long(id))
                    #vtLog.CallStack('')
                    #print self.adr,type(self.adr[0]),type(self.adr[1])
                    #print self.objLogin
                    ##FIXME self.objLogin==None???
                    self.doc.acquire()
                    self.srv.acquire()
                    bOk=False
                    try:
                        node=self.doc.getNodeByIdNum(long(id))
                        bOk=self.doc.IsAccessOk(node,self.objLogin,self.doc.ACL_MSK_WRITE)
                        if bOk:
                            iRet=self.srv.Lock(self.appl_alias,self.adr,long(id),self.objLogin)
                        else:
                            iRet=4
                    except:
                        vtLog.vtLngTB(self.appl_alias)
                        iRet=4
                    #self.srv.release()         # 091003:wro do not release lock yet
                    self.doc.release()
                    if vtLog.vtLngIsLogged(vtLog.INFO):
                        vtLog.vtLngCur(vtLog.INFO,'id:%s;access:%d;iRet:%d;usr:%s'%(id,bOk,iRet,self.objLogin.GetUsr()),origin=self.appl_alias)
                    #print iRet,id
                    self.NotifyLock(id,iRet)
                    self.srv.release()         # 091003:wro release lock yet
                    #self.doc.release()
                    return
                except:
                    self.srv.release()         # 091003:wro release lock yet
                    vtLog.vtLngCS(vtLog.ERROR,'lock node fault;%s'%traceback.format_exc(),origin=self.appl_alias)
                #self.SendTelegram('lock_node|'+id+',fault')
                self.SendTelegram(''.join(['lock_node|',id,',fault']))
            if vtLog.vtLngIsLogged(vtLog.WARN):
                vtLog.vtLngCS(vtLog.WARN,'lock node fault;%s'%val,origin=self.appl_alias)
        except:
            vtLog.vtLngCS(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
    def __unlock_node__(self,val):
        #if vtLog.vtLngIsLogged(vtLog.INFO):
        #    vtLog.vtLngCS(vtLog.INFO,'__unlock_node__',origin=self.appl_alias)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,val,origin=self.appl_alias)
        if self.doc is None:
            vtLog.vtLngCur(vtLog.ERROR,'doc is None',self.appl_alias)
            return
        id='-2'
        try:
            if len(val)>1:
                id=val[1:]
                #node=self.doc.getNodeByIdNum(long(id))
                #vtLog.CallStack('')
                self.doc.acquire()
                self.srv.acquire()
                try:
                    iRet=self.srv.UnLock(self.appl_alias,self.adr,long(id))
                    if vtLog.vtLngIsLogged(vtLog.INFO):
                        vtLog.vtLngCur(vtLog.INFO,'id:%s;iRet:%d;usr:%s'%(id,iRet,self.objLogin.GetUsr()),origin=self.appl_alias)
                except:
                    vtLog.vtLngTB(self.appl_alias)
                self.srv.release()
                self.doc.release()
                #print iRet,id
                if iRet==1:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'unlock node id:%s ok'%(id),self.appl_alias)
                    #self.SendTelegram('unlock_node|'+id+',ok')
                    self.SendTelegram(''.join(['unlock_node|',id,',ok']))
                    return
                elif iRet==0:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'unlock node id:%s rejected'%(id),self.appl_alias)
                    #self.SendTelegram('unlock_node|'+id+',rejected')
                    self.SendTelegram(''.join(['unlock_node|',id,',rejected']))
                    return
                else:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'unlock node id:%s fault'%(id),self.appl_alias)
                    #self.SendTelegram('unlock_node|'+id+',fault')
                    self.SendTelegram(''.join(['unlock_node|',id,',fault']))
                    return
                #self.SendTelegram('unlock_node|'+id+',fault')
                self.SendTelegram(''.join(['unlock_node|',id,',fault']))
            if vtLog.vtLngIsLogged(vtLog.WARN):
                vtLog.vtLngCS(vtLog.WARN,'unlock node fault;%s'%val,origin=self.appl_alias)
        except:
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
    def __lock_request__(self,val):
        iLog=vtLog.vtLngCur(vtLog.DEBUG,'val:%s'%(val),self.appl_alias)
        if self.doc is None:
            vtLog.vtLngCur(vtLog.ERROR,'doc is None',self.appl_alias)
            return
        try:
            iFlt=0
            if len(val)>1:
                id=val[1:]
                self.doc.acquire()
                self.srv.acquire()
                try:
                    iRet,zDiff=self.srv.ReqLock(self.appl_alias,self.adr,long(id),self.objLogin)
                except:
                    iRet=-1
                    vtLog.vtLngTB(self.appl_alias)
                self.srv.release()
                self.doc.release()
                if iRet==1:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'id:%s ok'%(id),self.appl_alias)
                    #self.SendTelegram('lock_request|'+id+',ok,%d'%zDiff)
                    return
                elif iRet==2:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'id:%s ok'%(id),self.appl_alias)
                    #self.SendTelegram('lock_request|'+id+',ok,%d'%zDiff)
                    self.SendTelegram(''.join(['lock_request|',id,',ok,%d'%zDiff]))
                    return
                elif iRet==3:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'id:%s grant'%(id),self.appl_alias)
                    #self.SendTelegram('lock_request|'+id+',grant')
                    self.SendTelegram(''.join(['lock_request|',id,',grant']))
                    return
                elif iRet==4:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'id:%s lock not present'%(id),self.appl_alias)
                    #self.SendTelegram('lock_request|'+id+',lock not present')
                    self.SendTelegram(''.join(['lock_request|',id,',lock not present']))
                    return
                #self.SendTelegram('lock_request|'+id+',fault')
                self.SendTelegram(''.join(['lock_request|',id,',fault']))
                
            #self.SendTelegram('lock_request|'+id+',fault')
        except:
            vtLog.vtLngCS(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
    def __lock_release__(self,val):
        iLog=vtLog.vtLngCur(vtLog.DEBUG,'val:%s'%(val),self.appl_alias)
        if self.doc is None:
            vtLog.vtLngCur(vtLog.ERROR,'doc is None',self.appl_alias)
            return
        try:
            iFlt=0
            i=string.find(val,'|')
            if i>=0:
                iFlt=1
                j=string.find(val,',',i)
                if j>0:
                    iFlt=2
                    k=string.find(val,',',j+1)
                    if k>0:
                        iFlt=3
                        id=val[i+1:j]
                        kind=val[j+1:k]
                        msg=val[k+1:]
                        self.doc.acquire()
                        self.srv.acquire()
                        try:
                            iRet=self.srv.ReleaseLock(self.appl_alias,self.adr,long(id),kind,msg,self.objLogin)
                        except:
                            iRet=-1
                            vtLog.vtLngTB(self.appl_alias)
                        #self.srv.release()         # 091003:wro do not release lock yet
                        self.doc.release()
                        if iRet==1:
                            usr,adr=self.objLogin.GetUsr(),self.adr
                            sock,iConnIdx=self.srv.GetServedXmlDomConnection(self.appl_alias,adr)
                            if vtLog.vtLngIsLogged(vtLog.INFO):
                                vtLog.vtLngCur(vtLog.INFO,'lock node id:%s rejected'%(id),self.appl_alias)
                            #self.SendTelegram('lock_node|'+id+',rejected locked by %s at %s'%(usr,adr))
                            if iConnIdx>=0:
                                self.SendTelegram(''.join(['unlock_node|',id,',granted']))
                                self.SendTelegram(''.join(['lock_node|',id,',rejected,lock released by %s at %s (%d)'%(usr,adr[0],iConnIdx)]))
                            else:
                                self.SendTelegram(''.join(['unlock_node|',id,',granted']))
                                self.SendTelegram(''.join(['lock_node|',id,',rejected,lock released by %s at %s'%(usr,adr)]))
                        self.srv.release()         # 091003:wro release lock yet
                        return
            #self.SendTelegram('lock_release|'+id+',fault')
        except:
            vtLog.vtLngCS(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
    def __lock_full_request__(self,val):
        iLog=vtLog.vtLngCur(vtLog.DEBUG,'val:%s'%(val),self.appl_alias)
        if self.doc is None:
            vtLog.vtLngCur(vtLog.ERROR,'doc is None',self.appl_alias)
            return
        try:
            iFlt=0
            if len(val)>1:
                id=val[1:]
                if long(id)!=-2:
                    #self.SendTelegram('lockfull_request|'+id+',fault')
                    self.SendTelegram(''.join(['lockfull_request|',id,',fault']))
                    return
                self.doc.acquire()
                self.srv.acquire()
                try:
                    iRet,zDiff=self.srv.ReqLockFull(self.appl_alias,self.adr,long(id),self.objLogin)
                except:
                    iRet=-1
                self.srv.release()
                self.doc.release()
                if iRet==1:
                    if vtLog.vtLngIsLogged(vtLog.INFO):
                        vtLog.vtLngCur(vtLog.INFO,'id:%s ok'%(id),self.appl_alias)
                    #self.SendTelegram('lock_request|'+id+',ok,%d'%zDiff)
                    return
                elif iRet==2:
                    if vtLog.vtLngIsLogged(vtLog.INFO):
                        vtLog.vtLngCur(vtLog.INFO,'id:%s ok'%(id),self.appl_alias)
                    #self.SendTelegram('lockfull_request|'+id+',ok,%d'%zDiff)
                    self.SendTelegram(''.join(['lockfull_request|',id,',ok,%d'%zDiff]))
                    return
                elif iRet==3:
                    if vtLog.vtLngIsLogged(vtLog.INFO):
                        vtLog.vtLngCur(vtLog.INFO,'id:%s grant'%(id),self.appl_alias)
                    #self.SendTelegram('lockfull_request|'+id+',grant')
                    self.SendTelegram(''.join(['lockfull_request|',id,',grant']))
                    return
                elif iRet==4:
                    if vtLog.vtLngIsLogged(vtLog.INFO):
                        vtLog.vtLngCur(vtLog.INFO,'id:%s lock not present'%(id),self.appl_alias)
                    #self.SendTelegram('lockfull_request|'+id+',lock not present')
                    self.SendTelegram(''.join(['lockfull_request|',id,',lock not present']))
                    return
                #self.SendTelegram('lockfull_request|'+id+',fault')
                self.SendTelegram(''.join(['lockfull_request|',id,',fault']))
        except:
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),self.appl_alias)
    def __lock_full_release__(self,val):
        iLog=vtLog.vtLngCur(vtLog.DEBUG,'val:%s'%(val),self.appl_alias)
        if self.doc is None:
            vtLog.vtLngCur(vtLog.ERROR,'doc is None',self.appl_alias)
            return
        try:
            iFlt=0
            i=string.find(val,'|')
            if i>=0:
                iFlt=1
                j=string.find(val,',',i)
                if j>0:
                    iFlt=2
                    k=string.find(val,',',j+1)
                    if k>0:
                        iFlt=3
                        id=val[i+1:j]
                        kind=val[j+1:k]
                        msg=val[k+1:]
                        self.doc.acquire()
                        self.srv.acquire()
                        try:
                            iRet=self.srv.ReleaseLockFull(self.appl_alias,self.adr,long(id),kind,msg,self.objLogin)
                        except:
                            iRet=-1
                        self.srv.release()
                        self.doc.release()
                        if iRet==1:
                            usr,adr=self.objLogin.GetUsr(),self.adr
                            if vtLog.vtLngIsLogged(vtLog.INFO):
                                vtLog.vtLngCur(vtLog.INFO,'id:%s rejected'%(id),self.appl_alias)
                            #self.SendTelegram('lockfull_release|'+id+',rejected locked by %s at %s'%(usr,adr))
                        return
            #self.SendTelegram('lock_release|'+id+',fault')
        except:
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),self.appl_alias)
    def __add_node__(self,val):
        #if vtLog.vtLngIsLogged(vtLog.INFO):
        #    vtLog.vtLngCS(vtLog.INFO,'__add_node__',origin=self.appl_alias)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,val,origin=self.appl_alias)
        if self.doc is None:
            vtLog.vtLngCur(vtLog.ERROR,'doc is None',self.appl_alias)
            return
        id='-2'
        try:
            iFlt=0
            i=string.find(val,'|')
            if i>=0:
                iFlt=1
                j=string.find(val,',',i)
                if j>0:
                    iFlt=2
                    k=string.find(val,',',j+1)
                    if k>0:
                        iFlt=3
                        l=string.find(val,',',k+1)
                        if l>0:
                            iFlt=4
                            fp=val[i+1:j]
                            idPar=val[j+1:k]
                            id=val[k+1:l]
                            if vtLog.vtLngIsLogged(vtLog.INFO):
                                vtLog.vtLngCur(vtLog.INFO,
                                        u'id:%s;idPar:%s'%(id,idPar),
                                        origin=self.appl_alias)
                            #if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            #    vtLog.vtLngCur(vtLog.DEBUG,
                            #            u'%s'%(val[l+1:]),
                            #            origin=self.appl_alias)
                            m=sha.new()
                            m.update(val[l+1:])
                            m.update(self.sessionID)
                            if m.hexdigest()==fp:
                                if 1==1:
                                    if len(idPar)>0:
                                        idPar=long(idPar)
                                    else:
                                        idPar=-1
                                    if idPar>-1:
                                        nodePar=self.doc.getNodeByIdNum(idPar)
                                    else:
                                        #nodePar=self.doc.getRoot()
                                        nodePar=self.doc.getBaseNode()
                                    docTmp,node=self.doc.getNode2Add(val[l+1:],bWarn=False)
                                    bOk=self.doc.IsAccessOk(node,self.objLogin,self.doc.ACL_MSK_ADD)
                                    try:
                                        self.doc.audit.sec(self.sLogin4Audit,'add',str(bOk))
                                    except:
                                        vtLog.vtLngTB(self.appl_alias)
                                    if vtLog.vtLngIsLogged(vtLog.INFO):
                                        vtLog.vtLngCur(vtLog.INFO,
                                                'idLock:%s;access:%d'%(self.doc.getKey(node),bOk),self.appl_alias)
                                    #node.freeNode()
                                    self.doc.__freeNodeRaw__(node)
                                    self.doc.__freeDocRaw__(docTmp)
                                    if bOk:
                                        self.doc.acquire()
                                        try:
                                            self.__clearCache__()
                                            idNew=self.doc.AddNodeXmlContent(nodePar,val[l+1:])
                                            #idNew=self.dec.getAttribute(node,self.doc.attr)
                                            #if idOld!=idNew:
                                            #    pass
                                            self.srv.acquire()
                                            try:
                                                iRet=self.srv.AddNode(self.appl_alias,self.adr,idPar,long(id),val[l+1:])
                                            except:
                                                iRet=-1
                                                vtLog.vtLngTB(self.appl_alias)
                                            self.srv.release()
                                            if vtLog.vtLngIsLogged(vtLog.INFO):
                                                vtLog.vtLngCur(vtLog.INFO,'add response id:%s idNew:%s'%(id,idNew),self.appl_alias)
                                            #self.SendTelegram('add_resp|'+id+','+idNew+',ok')
                                            self.SendTelegram(''.join(['add_resp|',id,',',idNew,',ok']))
                                        except:
                                            vtLog.vtLngTB(self.appl_alias)
                                            vtLog.vtLngCur(vtLog.ERROR,'add response id:%s idNew:%s'%(id,idNew),self.appl_alias)
                                            #self.SendTelegram('add_resp|'+id+','+id+',acl')
                                            self.SendTelegram(''.join(['add_resp|',id,',',id,',acl']))
                                        self.doc.release()
                                    else:
                                        if vtLog.vtLngIsLogged(vtLog.INFO):
                                            vtLog.vtLngCur(vtLog.INFO,'add response id:%s idNew:%s acl'%(id,id),self.appl_alias)
                                        #self.SendTelegram('add_resp|'+id+','+id+',acl')
                                        self.SendTelegram(''.join(['add_resp|',id,',',id,',acl']))
                                else:
                                    if vtLog.vtLngIsLogged(vtLog.INFO):
                                        vtLog.vtLngCur(vtLog.INFO,'add response id:%s idNew:%s locked'%(id,idNew),self.appl_alias)
                                    #self.SendTelegram('add_resp|'+id+','+idNew+',locked')
                                    self.SendTelegram(''.join(['add_resp|',id,',',idNew,',locked']))
                                return
                            #self.SendTelegram('add_resp|'+id+','+id+',fault')
                            self.SendTelegram(''.join(['add_resp|',id,',',id,',fault']))
            if vtLog.vtLngIsLogged(vtLog.WARN):
                vtLog.vtLngCur(vtLog.WARN,'add node fault;%s'%val,origin=self.appl_alias)
        except:
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
    def __del_node__(self,val):
        #if vtLog.vtLngIsLogged(vtLog.INFO):
        #    vtLog.vtLngCS(vtLog.INFO,'__del_node__',origin=self.appl_alias)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,val,origin=self.appl_alias)
        if self.doc is None:
            vtLog.vtLngCur(vtLog.ERROR,'doc is None',self.appl_alias)
            return
        id='-2'
        try:
            iFlt=0
            i=string.find(val,'|')
            if i>=0:
                id=val[i+1:]
                iId=long(id)
                self.doc.acquire()
                try:
                    node=self.doc.getNodeByIdNum(iId)
                    if node is None:
                        bOk=True
                        if vtLog.vtLngIsLogged(vtLog.INFO):
                            vtLog.vtLngCS(vtLog.INFO,'id:%s;already deleted;access:%d'%(id,bOk),origin=self.appl_alias)
                    else:
                        bOk=self.doc.IsAccessOk(node,self.objLogin,self.doc.ACL_MSK_DEL)
                        try:
                            self.doc.audit.sec(self.sLogin4Audit,'del',str(bOk))
                        except:
                            vtLog.vtLngTB(self.appl_alias)
                        if bOk:
                            nodePar=self.doc.getParent(node)
                            try:
                                id=self.doc.getKey(node)
                                idPar=self.doc.getKey(nodePar)
                                self.doc.audit.write('delNode:%s from:%s'%(id,idPar),self.doc.GetNodeXmlContent(node,True,True),
                                            origin=self.doc.getAuditOriginByNode(node,self.sLogin4Audit))
                            except:
                                vtLog.vtLngTB(self.appl_alias)
                            self.doc.__deleteNode__(node,nodePar)
                            self.__clearCache__()
                            self.srv.acquire()
                            try:
                                self.srv.DelNode(self.appl_alias,self.adr,iId)
                            except:
                                vtLog.vtLngTB(self.appl_alias)
                            self.srv.release()
                        if vtLog.vtLngIsLogged(vtLog.INFO):
                            vtLog.vtLngCS(vtLog.INFO,'id:%s;access:%d'%(id,bOk),origin=self.appl_alias)
                except:
                    bOk=False
                    vtLog.vtLngCS(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
                self.doc.release()
                if bOk:
                    #self.SendTelegram('del_node|'+id+',ok')
                    self.SendTelegram(''.join(['del_node|',str(iId),',ok']))
                else:
                    #self.SendTelegram('del_node|'+id+',acl')
                    self.SendTelegram(''.join(['del_node|',str(iId),',acl']))
                return
            if vtLog.vtLngIsLogged(vtLog.WARN):
                vtLog.vtLngCur(vtLog.WARN,'fault;%s'%val,origin=self.appl_alias)
        except:
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
    def __move_node__(self,val):
        #if vtLog.vtLngIsLogged(vtLog.INFO):
        #    vtLog.vtLngCS(vtLog.INFO,'__move_node__',origin=self.appl_alias)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,val,origin=self.appl_alias)
        if self.doc is None:
            vtLog.vtLngCur(vtLog.ERROR,'doc is None',self.appl_alias)
            return
        try:
            iFlt=0
            i=string.find(val,'|')
            if i>=0:
                iFlt=1
                j=string.find(val,',',i)
                if j>0:
                    iFlt=2
                    idPar=val[i+1:j]
                    id=val[j+1:]
                    self.doc.acquire()
                    try:
                        parNode=self.doc.getNodeByIdNum(long(idPar))
                        node=self.doc.getNodeByIdNum(long(id))
                        if parNode is None:
                            if node is None:
                                self.doc.release()
                                vtLog.vtLngCur(vtLog.ERROR,'idPar:%s;id:%s;node not found'%(idPar,id),self.appl_alias)
                                return
                            else:
                                parNode=self.doc.getParent(node)
                                idPar=self.doc.getKey(parNode)
                                #self.SendTelegram('move_resp|'+idPar+','+id+',err')
                                self.SendTelegram(''.join(['move_resp|',idPar,',',id,',err']))
                                self.doc.release()
                                vtLog.vtLngCur(vtLog.ERROR,'idPar:%s;id:%s;parent node not found'%(idPar,id),self.appl_alias)
                                return
                        bOkPar=self.doc.IsAccessOk(parNode,self.objLogin,self.doc.ACL_MSK_ADD)
                        try:
                            self.doc.audit.sec(self.sLogin4Audit,'add',str(bOkPar))
                        except:
                            vtLog.vtLngTB(self.appl_alias)
                        if bOkPar:
                            bOk=self.doc.IsAccessOk(node,self.objLogin,self.doc.ACL_MSK_MOVE)
                        else:
                            bOk=False
                        try:
                            self.doc.audit.sec(self.sLogin4Audit,'move',str(bOk))
                        except:
                            vtLog.vtLngTB(self.appl_alias)
                        if bOk:
                            parNodeOld=self.doc.getParent(node)
                            try:
                                self.doc.audit.write('moveNode:%d from:%d to:%d'%\
                                        (self.doc.getKeyNum(node),self.doc.getKeyNum(parNodeOld),self.doc.getKeyNum(parNode),),\
                                        self.doc.GetNodeXmlContent(node,False,False),
                                        origin=self.doc.getAuditOriginByNode(node,self.sLogin4Audit))
                            except:
                                vtLog.vtLngTB(self.appl_alias)
                            self.doc.moveNode(parNode,node)
                            self.__clearCache__()
                            if parNodeOld is not None:
                                self.doc.AlignNode(parNodeOld)
                            self.srv.acquire()
                            try:
                                self.srv.MoveNode(self.appl_alias,self.adr,long(idPar),long(id))
                            except:
                                vtLog.vtLngTB(self.appl_alias)
                            self.srv.release()
                        if vtLog.vtLngIsLogged(vtLog.INFO):
                            vtLog.vtLngCur(vtLog.INFO,'id:%s;access:%d;par access:%d'%(id,bOk,bOkPar),origin=self.appl_alias)
                    except:
                        bOk=False
                        vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
                    self.doc.release()
                    if bOk:
                        #self.SendTelegram('move_resp|'+idPar+','+id+',ok')
                        self.SendTelegram(''.join(['move_resp|',idPar,',',id,',ok']))
                    else:
                        parNode=self.doc.getParent(node)
                        idPar=self.doc.getKey(parNode)
                        #self.SendTelegram('move_resp|'+idPar+','+id+',acl')
                        self.SendTelegram(''.join(['move_resp|',idPar,',',id,',acl']))
                    return
            if vtLog.vtLngIsLogged(vtLog.WARN):
                vtLog.vtLngCur(vtLog.WARN,'fault;%s'%val,origin=self.appl_alias)
        except:
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),self.appl_alias)
    def NotifyLockReleased(self,id,usr,adr):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%d'%(id),origin=self.appl_alias)
        try:
            self.SendTelegram('lock_node|%d,rejected locked by %s at %s'%(id,usr,adr),origin=self.appl_alias)
        except:
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
    def NotifyLock(self,id,iRet):
        try:
                    if iRet==1:
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCur(vtLog.DEBUG,'lock node id:%s ok'%(id),self.appl_alias)
                        #self.SendTelegram('lock_node|'+id+',ok')
                        self.SendTelegram(''.join(['lock_node|',id,',ok']))
                    elif iRet==2:
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCur(vtLog.DEBUG,'lock node id:%s already locked'%(id),self.appl_alias)
                        #self.SendTelegram('lock_node|'+id+',already locked')
                        self.SendTelegram(''.join(['lock_node|',id,',already locked']))
                    elif iRet==0:
                        usr,adr=self.srv.GetLock(self.appl_alias,long(id))
                        sock,iConnIdx=self.srv.GetServedXmlDomConnection(self.appl_alias,adr)
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCur(vtLog.DEBUG,'lock node id:%s rejected'%(id),self.appl_alias)
                        ##self.SendTelegram('lock_node|'+id+',rejected locked by %s at %s'%(usr,adr))
                        #self.SendTelegram('lock_node|'+id+',rejected,locked by,%s,at,%s'%(usr,adr))
                        if iConnIdx>=0:
                            self.SendTelegram(''.join(['lock_node|',id,',rejected,locked by,%s,at,%s (%d)'%(usr,adr[0],iConnIdx)]))
                        else:
                            self.SendTelegram(''.join(['lock_node|',id,',rejected,locked by,%s,at,%s'%(usr,adr)]))
                    elif iRet==3:
                        zLockBlock=self.srv.GetLockBlock()
                        zDiff=self.GetLockBlockDiff(long(id),zLockBlock)
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCur(vtLog.DEBUG,'lock block node id:%s time:%d'%(id,zDiff),self.appl_alias)
                        #self.SendTelegram('lock_node|'+id+',lock block,%d'%(zDiff))
                        self.SendTelegram(''.join(['lock_node|',id,',lock block,%d'%(zDiff)]))
                    else:
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCur(vtLog.DEBUG,'lock node id:%s fault'%(id),self.appl_alias)
                        #self.SendTelegram('lock_node|'+id+',fault')
                        self.SendTelegram(''.join(['lock_node|',id,',fault']))
        except:
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
            self.SendTelegram(''.join(['lock_node|',id,',except']))
    def NotifyUnLock(self,id):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%d'%(id),origin=self.appl_alias)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'unlock_node|%d released'%id,self.appl_alias)
        if self.doc is None:
            vtLog.vtLngCur(vtLog.ERROR,'doc is None',self.appl_alias)
            return
        try:
            #self.SendTelegram('unlock_node|'+str(id)+',released')
            self.SendTelegram(''.join(['unlock_node|',str(id),',released']))
        except:
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
    def NotifyReqLock(self,id,adr,usr,iConnIdx=-1):
        #if vtLog.vtLngIsLogged(vtLog.INFO):
        #    vtLog.vtLngCur(vtLog.INFO,'',self.appl_alias)
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%d;usr:%s,adr:%s:%d;iConnIdx:%d'%(id,usr,adr[0],adr[1],iConnIdx),self.appl_alias)
        if self.doc is None:
            vtLog.vtLngCur(vtLog.ERROR,'doc is None',self.appl_alias)
            return
        try:
            if adr is None:
                self.SendTelegram('lock_request|%d,request,%s,%s,%d'%(id,usr,self.adr[0],self.adr[1]))
            else:
                if iConnIdx>=0:
                    self.SendTelegram('lock_request|%d,request,%s,%s,%d,%d'%(id,usr,adr[0],iConnIdx,adr[1]))
                else:
                    self.SendTelegram('lock_request|%d,request,%s,%s,%d'%(id,usr,adr[0],adr[1]))
        except:
            vtLog.vtLngCur(vtLog.ERROR,'%d;%s'%(id,traceback.format_exc()),origin=self.appl_alias)
    def NotifyReqLockGrant(self,id,adr,usr,msg,iConnIdx=-1):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'id:%d;usr:%s;adr:%s:%d,msg:%s;iConnIdx:%d'%(id,usr,adr[0],adr[1],msg,iConnIdx),self.appl_alias)
        elif vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%d;usr:%s;adr:%s:%d;iConnIdx:%d'%(id,usr,adr[0],adr[1],iConnIdx),self.appl_alias)
        if self.doc is None:
            vtLog.vtLngCur(vtLog.ERROR,'doc is None',self.appl_alias)
            return
        try:
            if iConnIdx>=0:
                self.SendTelegram('lock_request|%d,grant,%s,%s,%d,%s'%(id,usr,adr[0],iConnIdx,msg))
            else:
                self.SendTelegram('lock_request|%d,grant,%s,%s,%d,%s'%(id,usr,adr[0],adr[1],msg))
        except:
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
    def NotifyReqLockReject(self,id,adr,usr,msg,iConnIdx=-1):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'id:%d;usr:%s;adr:%s:%d;msg:%s;iConnIdx:%d'%(id,usr,adr[0],adr[1],msg,iConnIdx),self.appl_alias)
        elif vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.DEBUG,'id:%d;usr:%s;adr:%s:%d;iConnIdx:%d'%(id,usr,adr[0],adr[1],iConnIdx),self.appl_alias)
        if self.doc is None:
            vtLog.vtLngCur(vtLog.ERROR,'doc is None',self.appl_alias)
            return
        try:
            if iConnIdx>=0:
                self.SendTelegram('lock_request|%d,reject,%s,%s,%d,%s'%(id,usr,adr[0],iConnIdx,msg))
            else:
                self.SendTelegram('lock_request|%d,reject,%s,%s,%d,%s'%(id,usr,adr[0],adr[1],msg))
        except:
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
    def NotifySetNode(self,id,content):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%d'%id,origin=self.appl_alias)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'%s content:%s'%(id,content),self.appl_alias)
        if self.doc is None:
            vtLog.vtLngCur(vtLog.ERROR,'doc is None',self.appl_alias)
            return
        #node=self.doc.getNodeByIdNum(long(id))
        #content=self.doc.GetNodeXmlContent(node)
        try:
            m=sha.new()
            m.update(content)
            m.update(self.sessionID)
            fp=m.hexdigest()
            #self.SendTelegram('get_node|'+fp+','+str(id)+','+content)
            self.SendTelegram(''.join(['get_node|',fp,',',str(id),',',content]))
        except:
            vtLog.vtLngCS(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
    def NotifySetNodeFull(self,id,content):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCS(vtLog.INFO,'id:%d'%id,origin=self.appl_alias)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'%d content:%s'%(id,content),self.appl_alias)
        if self.doc is None:
            vtLog.vtLngCur(vtLog.ERROR,'doc is None',self.appl_alias)
            return
        try:
            #node=self.doc.getNodeByIdNum(long(id))
            #content=self.doc.GetNodeXmlContent(node)
            m=sha.new()
            m.update(content)
            m.update(self.sessionID)
            fp=m.hexdigest()
            #self.SendTelegram('get_full_node|'+fp+','+str(id)+','+content)
            self.SendTelegram(''.join(['get_full_node|',fp,',',str(id),',',content]))
        except:
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
    def NotifyAddNode(self,idPar,id,content):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCS(vtLog.INFO,'id:%d;idPar:%d'%(id,idPar),origin=self.appl_alias)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'idPar:%s id:%s %s'%(idPar,id,content),self.appl_alias)
        if self.doc is None:
            vtLog.vtLngCur(vtLog.ERROR,'doc is None',self.appl_alias)
            return
        try:
            m=sha.new()
            m.update(content)
            m.update(self.sessionID)
            fp=m.hexdigest()
            #self.SendTelegram('add_node|'+fp+','+str(idPar)+','+str(id)+','+content)
            self.SendTelegram(''.join(['add_node|',fp,',',str(idPar),',',str(id),',',content]))
        except:
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
    def NotifyDelNode(self,id):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCS(vtLog.INFO,'id:%d'%id,origin=self.appl_alias)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,id,self.appl_alias)
        if self.doc is None:
            vtLog.vtLngCur(vtLog.ERROR,'doc is None',self.appl_alias)
            return
        try:
            #self.SendTelegram('remove_node|'+str(id))
            self.SendTelegram(''.join(['remove_node|',str(id)]))
        except:
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
    def NotifyMoveNode(self,idPar,id):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCS(vtLog.INFO,'id:%d;idPar:%d'%(id,idPar),origin=self.appl_alias)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'idPar:%s id:%s'%(idPar,id),self.appl_alias)
        if self.doc is None:
            vtLog.vtLngCur(vtLog.ERROR,'doc is None',self.appl_alias)
            return
        try:
            #self.SendTelegram('move_node|'+str(idPar)+','+str(id))
            self.SendTelegram(''.join(['move_node|',str(idPar),',',str(id)]))
        except:
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.appl_alias)
    def NotifyAcl(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',self.appl_alias)
        self.__get_acl__('')
class vtSrvXmlSock(vtSrvSock):
    LOCK_BLOCK=300  # block relock for to this time
    LOCK_REQ=60     # block lock request for to this time
    LOCK_REQ_RELEASE=300 # release lock request for this time
    SERVE_RUNNING              = 0x0001
    SERVE_PAUSED               = 0x0002
    SERVE_STOPPED              = 0x0004
    #SERVE_RUNNING              = 0x0001

    def __init__(self,server,port,socketClass,verbose=0):
        vtSrvSock.__init__(self,server,port,socketClass,verbose=verbose)
        self.docs={}
        self.locks={}
        self.locksReq={}
        self.locksReqFull={}
        #self.hosts=[]
        self.hosts={}
        self.hostUsr=[]
        self.zAutoSave=None
        self.docSettings=None
        #self.SetSaveDelay(5)
        self.zLockBlock=self.LOCK_BLOCK
        self.zLockReq=self.LOCK_REQ
        self.zLockReqRel=self.LOCK_REQ_RELEASE
        self.thdDoCmd=vtThread(None,origin='vtSrvXml:thdDoCmd')
        
        self.zAutoClose=None
        self.SetCloseDelay(-1)
        self.semSrv=threading.Lock()#threading.Semaphore()
        self.__initCmdRemoteDict__()
        
        self.dLoginCache=None
    def acquire(self,blocking=True):
        vtLog.vtLngCurCls(vtLog.INFO,'blocking:%d'%(blocking),self)
        return self.semSrv.acquire(blocking)
    def release(self):
        vtLog.vtLngCurCls(vtLog.INFO,'',self)
        return self.semSrv.release()
    def SetSettings(self,doc):
        vtLog.CallStack('')
        self.docSettings=doc
    def SetSaveDelay(self,iMin):
        self.zSave=iMin
        self.zDiffSave=iMin*60
        if self.zAutoSave is None:
            self.zAutoSave=PyTimer(self.AutoSave) # 5 min
        else:
            self.zAutoSave.Stop()
        if self.zSave>0:
            self.zAutoSave.Start(self.zSave*60000)
    def SetLockBlock(self,iMin):
        try:
            self.zLockBlock=iMin*60
            if self.zLockBlock<0:
                self.zLockBlock=0
        except:
            self.zLockBlock=self.LOCK_BLOCK
    def GetLockBlock(self):
        return self.zLockBlock
    def SetLockReq(self,iSec):
        try:
            self.zLockReq=iSec
            if self.zLockReq<0:
                self.zLockReq=0
        except:
            self.zLockReq=self.LOCK_REQ
    def GetLockReq(self):
        return self.zLockReq
    def SetLockReqRelease(self,iSec):
        try:
            self.zLockReqRelease=iSec
            if self.zLockReqRelease<self.zLockReq:
                self.zLockReqRelease=self.zLockReq
        except:
            self.zLockReqRel=self.LOCK_REQ_RELEASE
    def GetLockReqRelease(self):
        return self.zLockReqRel
    def SetCloseDelay(self,iMin):
        self.zClose=iMin
        self.zDiffClose=iMin*60
        if self.zAutoClose is None:
            self.zAutoClose=PyTimer(self.AutoClose) # 5 min
        else:
            self.zAutoClose.Stop()
        self.zClose=-1
        if self.zClose>0:
            self.zAutoClose.Start(self.zClose*60000)
    #def SetHosts(self,hosts):
    #    self.hosts=hosts
    def IsLogginNeeded(self,adr,applAlias):
        if self.verbose:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'IsLogginNeeded '+adr+' '+applAlias,applAlias)
        i=string.find(applAlias,':')
        appl=applAlias[:i]
        alias=applAlias[i+1:]
        try:
            lstHosts=self.hosts[applAlias]
        except:
            return -1
        if self.IsServedStatus(applAlias,self.SERVE_RUNNING)<1:
            return -10
        for host in lstHosts:
            if self.__checkAdr__(host[0],adr):
                if len(host[2])==0:
                    if len(host[1])==0:
                        return 0
                    else:
                        return 1
                else:
                    return 1
        return -1
    def __checkAdr__(self,adr1,adr2):
        strs1=string.split(adr1,'.')
        strs2=string.split(adr2,'.')
        for s1,s2 in zip(strs1,strs2):
            if fnmatch.fnmatch(s2,s1)==False:
                return False
        return True
    def CheckLogin(self,adr,applAlias,sessionID,usr,passwd):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'check login adr:%s'%adr,applAlias)
        i=string.find(applAlias,':')
        appl=applAlias[:i]
        alias=applAlias[i+1:]
        try:
            lstHosts=self.hosts[applAlias]
        except:
            return None,-1
        if self.IsServedStatus(applAlias,self.SERVE_RUNNING)<1:
            return None,-10
        for host in lstHosts:
            if self.__checkAdr__(host[0],adr):
                if host[1]=='__vHum__':
                    doc=self.GetServedXmlDom(applAlias)
                    sHumAlias,nodeAcl=doc.GetActiveAcl()
                    if sHumAlias is None:
                        continue
                    docHum=self.GetServedXmlDom('vHum:'+sHumAlias)
                    if docHum is None:
                        return None,0
                    obj,iRes=self.CheckLoginHum(docHum,appl,sessionID,usr,passwd)
                    if obj is not None:
                        tHostUsr=(adr,usr)
                        if tHostUsr not in self.hostUsr:
                            if len(self.hostUsr)<15:
                                self.hostUsr.append(tHostUsr)
                                if len(self.hostUsr)>10:
                                    iRet=2
                            else:
                                return None,-3
                        return obj,iRes
                    #obj=vtSecXmlLogin(None,1,[],0)
                    #return obj,1
                elif host[1]=='__srv__':
                    obj,iRes=self.CheckLoginInSettings(appl,sessionID,usr,passwd)
                    if obj is not None:
                        tHostUsr=(adr,usr)
                        if tHostUsr not in self.hostUsr:
                            if len(self.hostUsr)<15:
                                self.hostUsr.append(tHostUsr)
                                if len(self.hostUsr)>10:
                                    iRet=2
                            else:
                                return None,-3
                        return obj,iRes
                else:
                    if len(host[2])==0:
                        if len(host[1])==0:
                            return None,0
                        else:
                            if host[1]==usr:
                                obj=vtSecXmlLogin(usr,-1,[],[],32,True)
                                return obj,1
                    else:
                        if host[1]==usr:
                            m=sha.new()
                            m.update(host[2])
                            m.update(sessionID)
                            if m.digest()==passwd:
                                obj=vtSecXmlLogin(usr,-1,[],[],32,True)
                                tHostUsr=(adr,usr)
                                if tHostUsr not in self.hostUsr:
                                    if len(self.hostUsr)<15:
                                        self.hostUsr.append(tHostUsr)
                                        if len(self.hostUsr)>10:
                                            iRet=2
                                    else:
                                        return None,-3
                                return obj,1
        return None,-1
    def CheckLoginHum(self,docHum,appl,sessionID,usr,passwd):
        if docHum is None:
            return None,0
        obj=None
        docHum.acquire()
        try:
            #vtLog.CallStack('')
            nodeUsrs=docHum.getChildByLst(docHum.getBaseNode(),['users'])
            for nodeUsr in docHum.getChilds(nodeUsrs,'user'):
                if usr==docHum.getNodeText(nodeUsr,'name'):
                    nodeTmp=docHum.getChild(nodeUsr,'security')
                    sPasswd=docHum.getNodeText(nodeTmp,'passwd')
                    try:
                        #vtLog.CallStack('')
                        #print docHum.getKey(nodeUsr)
                        iId=long(docHum.getKey(nodeUsr))
                    except:
                        iId=-1
                    bAdmin=False
                    iGrpSecLv=-1
                    grpIds=[]
                    grps=[]
                    for nodeGrp in docHum.getChilds(nodeTmp,'grp'):
                        fid=docHum.getAttribute(nodeGrp,'fid')
                        try:
                            grpIds.append(long(fid))
                            nodeGrp=docHum.getNodeByIdNum(long(fid))
                            if nodeGrp is not None:
                                grps.append(docHum.getNodeText(nodeGrp,'name'))
                                nodeGrpSec=docHum.getChild(nodeGrp,'security')
                                if nodeGrpSec is not None:
                                    try:
                                        iGrpSecLvAct=long(docHum.getNodeText(nodeGrpSec,'sec_level'))
                                        if iGrpSecLvAct>iGrpSecLv:
                                            iGrpSecLv=iGrpSecLvAct
                                    except:
                                        pass
                                    try:
                                        bAdminAct=long(docHum.getNodeText(nodeGrpSec,'admin'))
                                        if bAdminAct>0:
                                            bAdmin=True
                                    except:
                                        pass
                            else:
                                grps.append('')
                        except:
                            pass
                    try:
                        iLevel=int(docHum.getNodeText(nodeTmp,'sec_level'))
                    except:
                        iLevel=0
                    if iLevel<0:
                        iLevel=iGrpSecLv
                    if len(sPasswd)==0:
                        try:
                            obj=vtSecXmlLogin(usr,iId,grpIds,grps,iLevel,bAdmin)
                        except:
                            vtLog.vtLngTB('')
                        break
                    m=sha.new()
                    m.update(sPasswd)
                    m.update(sessionID)
                    if m.digest()==passwd:
                        # password is ok
                        vtLog.vtLngCur(vtLog.INFO,'user:%s passwd matched'%(usr),appl)
                        try:
                            obj=vtSecXmlLogin(usr,iId,grpIds,grps,iLevel,bAdmin)
                        except:
                            vtLog.vtLngTB('')
                        break
                        #return obj,1
                    else:
                        vtLog.vtLngCur(vtLog.WARN,'user:%s passwd mismatched'%(usr),appl)
        except:
            vtLog.vtLngTB('')
        docHum.release()
        return obj,obj is not None
    def __checkLoginInSettings__(self,node,usr):
        for c in self.docSettings.getChilds(node,'login'):
            sLogin=self.docSettings.getNodeText(c,'login')
            if sLogin==usr:
                return c
        return None
            
    def CheckLoginInSettings(self,appl,sessionID,usr,passwd):
        nodeAll=None
        nodeAppl=None
        if self.docSettings is None:
            return None,0
        tmp=self.docSettings.getChild(self.docSettings.getRoot(),'access')
        if tmp is not None:
            for c in self.docSettings.getChilds(tmp):
                tagName=self.docSettings.getTagName(c)
                if tagName==appl:
                    nodeAppl=self.__checkLoginInSettings__(c,usr)
                elif tagName=='all':
                    nodeAll=self.__checkLoginInSettings__(c,usr)
            nodeTmp=None
            if nodeAppl is not None:
                nodeTmp=nodeAppl
            elif nodeAll is not None:
                nodeTmp=nodeAll
            if nodeTmp is not None:
                sPasswd=self.docSettings.getNodeText(nodeTmp,'passwd')
                try:
                    iLevel=int(self.docSettings.getNodeText(nodeTmp,'sec_level'))
                except:
                    iLevel=0
                    
                m=sha.new()
                m.update(sPasswd)
                m.update(sessionID)
                if m.digest()==passwd:
                    # password is ok
                    try:
                        obj=vtSecXmlLogin(usr,-1,[],[],iLevel,iLevel>=32)
                    except:
                        traceback.print_exc()
                    return obj,1
        return None,0
    def __findHostUsr__(self,host,usr):
        try:
            for dAlias in self.docs.values():
                lConn=dAlias['connections']
                for conn in lConn:
                    if conn[0][0]==host:
                        if conn[1].objLogin is not None:
                            if conn[1].objLogin.GetUsr()==usr:
                                return True
                        #else:
                        #    return F
            return False
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return True
    def StopSocket(self,adr,sockInst):
        try:
            if self.verbose:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,repr(adr),sockInst.appl_alias)
        except:
            pass
        try:
            alias=sockInst.appl_alias
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'',alias)
            if alias not in self.docs:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'%s,closing...'%repr(adr),sockInst.appl_alias)
                sockInst.Close()
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'%s,closed'%repr(adr),sockInst.appl_alias)
                return
            tup=self.docs[alias]
            i=0
            bFound=False
            for cadr,sock in tup['connections']:
                if cadr==adr:
                    bFound=True
                    break
                i+=1
            #if self.verbose:
            #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'connection:%d remove'%i)
            if bFound:
                tup['connections']=tup['connections'][:i]+tup['connections'][i+1:]
            #if self.verbose:
            #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'locks remove')
            lst=self.locks[alias]
            lstNew=[]
            #vtLog.vtLog(None,repr(lst),level=1,verbose=self.verbose)
            for it in lst:
                if it.IsAddrPort(adr)==False:
                    lstNew.append(it)
                else:
                    # notify other applications
                    self.NotifyUnLock(alias,adr,it.GetID())
                    pass
            self.locks[alias]=lstNew
            
            lst=self.locksReq[alias]
            lstNew=[]
            #vtLog.vtLog(None,repr(lst),level=1,verbose=self.verbose)
            for it in lst:
                if it[1]!=adr:
                    lstNew.append(it)
            self.locksReq[alias]=lstNew
            #if self.verbose:
            #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'locks removed')
            #    vtLog.vtLog(None,repr(lstNew),level=1,verbose=self.verbose)
            if len(tup['connections'])==0:
                tup['last_use']=time.time()
            try:
                sUsr=sockInst.objLogin.GetUsr()
            except:
                sUsr=''
                pass
            #vtLog.CallStack('')
            #print vtLog.pformat(self.hostUsr)
            hostUsr=(adr[0],sUsr)
            if self.__findHostUsr__(adr[0],sUsr)==False:
                if hostUsr in self.hostUsr:
                    self.hostUsr.remove(hostUsr)
        except:
            vtLog.vtLngCS(vtLog.ERROR,'%s'%(traceback.format_exc()),origin=alias)
        #vtSrvSock.StopSocket(self,adr,sockInst)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'%s,closing...'%repr(adr),sockInst.appl_alias)
        sockInst.Close()
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'%s,closed'%repr(adr),sockInst.appl_alias)
    def ClearServedXml(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'ClearServedXml')
        for k in self.docs.keys():
            self.DelServedXml(k)
        pass
    def IsServedStatus(self,alias,status):
        if alias in self.docs:
            if (self.docs[alias]['status']&status)==status:
                return 1
            else:
                return 0
        else:
            return -1
    def ShutDown(self):
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
        #vtLog.CallStack('')
        #print self.docs
        try:
            if self.zAutoSave is not None:
                self.zAutoSave.Stop()
                del self.zAutoSave
        except:
            vtLog.vtLngTB(self.GetOrigin())
        try:
            if self.zAutoClose is not None:
                self.zAutoClose.Stop()
                del self.zAutoClose
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.zAutoClose=None
        self.zAutoSave=None
        self.hum2SecAcl=None
        vtSrvSock.ShutDown(self)
    def StartServedXml(self,alias):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'alias:%s'%(alias),origin=alias)
        if alias in self.docs:
            tup=self.docs[alias]
            doc=tup['doc']
            #try:
            #    doc.SetApplAlias(alias)
            #except:
            #    vtLog.vtLngTB(alias)
            #doc.Open(tup['fn'])
            iRet=doc.Open(tup['fn'])
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'open return:%d'%(iRet),alias)
            if iRet==-1:
                doc.New()
                doc.Save(tup['fn'])
            else:
                doc.acquire('dom')
                try:
                    doc.CreateReq()
                    doc.BuildSrv()
                except:
                    vtLog.vtLngTB(alias)
                doc.release('dom')
            tup['last_access']=time.time()
            tup['status']=self.SERVE_RUNNING
            
    def StopServedXml(self,alias):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'alias:%s'%(alias),origin=alias)
        if alias in self.docs:
            self.SaveXml(alias)
            tup=self.docs[alias]
            doc=tup['doc']
            tup['status']=self.SERVE_STOPPED
            for conn in tup['connections']:
                self.StopSocket(conn[0],conn[1])
            
    def AddServedXml(self,alias,fn,doc,skip,id=-2):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'alias:%s;fn:%s'%(alias,fn),origin=alias)
        if self.docs.has_key(alias):
            return -1
        if alias[-1]==':':
            return 0
        try:
            doc.SetApplAlias(alias)
        except:
            vtLog.vtLngTB(alias)
        self.docs[alias]={'fn':fn,'doc':doc,'id':id,
                'last_access':None,'last_use':None,
                'last_modify':None,'last_save':time.time(),
                'skip':skip,'connections':[],'status':self.SERVE_RUNNING}
        #if self.verbose:
        #    keys=self.docs.keys()
        #    keys.sort()
        #    for k in keys:
        #        vtLog.vtLngCallStack(self,vtLog.DEBUG,'served:%s %s'%(k,self.docs[k]),callstack=False)
        self.locks[alias]=[]
        self.locksReq[alias]=[]
        return 1
    def __doCmd__(self,alias,method,*args,**kwargs):
        try:
            vtLog.vtLngCur(vtLog.DEBUG,'method:%s;args:%s;kwargs:%s'%\
                        (method,vtLog.pformat(args),vtLog.pformat(kwargs)),alias)
            f=getattr(self,method)
            try:
                self.acquire()
                f(*args,**kwargs)
                self.release()
            except:
                pass
        except:
            vtLog.vtLngTB(alias)
    def DoCmdThreadSafe(self,alias,method,*args,**kwargs):
        try:
            self.thdDoCmd.Do(self.__doCmd__,alias,method,*args,**kwargs)
        except:
            vtLog.vtLngTB(alias)
    def __initCmdRemoteDict__(self):
        if hasattr(self,'CMD_DICT')==False:
            self.CMD_DICT={}
        self.CMD_DICT.update({
                '__cloneAlias__':(_('clone alias'),0x06,{
                        'dest':(_(u'destination'),'string',None),
                        'destFN':(_(u'destination filename'),'string',None),
                        'source':(_(u'source'),'string',None),
                        }),
                '__start__':(_('clone alias'),0x02,{
                        'source':(_(u'source'),'string',None),
                        }),
                '__stop__':(_('clone alias'),0x02,{
                        'source':(_(u'source'),'string',None),
                        }),
                })
    def __cloneAlias__(self,appl,**kwargs):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'appl:%s;kwargs:%s'%(appl,vtLog.pformat(kwargs)),'')
            sAliasSrc=':'.join([appl,kwargs['source']])
            sAliasDst=':'.join([appl,kwargs['dest']])
            doc=self.GetServedXmlDom(sAliasSrc)
            if doc is None:
                return -1
            tup=self.docs[sAliasSrc]
            if 'id' in tup:
                id=tup['id']
            else:
                id=-2
            if 'destFN' not in kwargs:
                kwargs['destFN']=''
            if len(kwargs['destFN'])==0:
                kwargs['destFN']='_'.join([appl,kwargs['dest']])
            if kwargs['destFN'].endswith('.xml')==False:
                kwargs['destFN']=kwargs['destFN']+'.xml'
            sFNold=doc.GetFN()
            sDN,s=os.path.split(sFNold)
            ss=kwargs['destFN']
            ssDN,sTmp=os.path.split(ss)
            if len(ssDN)==0:
                ssDN=sDN
                sFNnew=os.path.join(sDN,ss)
            else:
                if ssDN.startswith('./'):
                    sFNnew=os.path.join(sDN,ssDN[2:],sTmp)
                else:
                    sFNnew=ss
            vtLog.vtLngCur(vtLog.INFO,'dest:%s;srcFN:%s;dstFN:%s'%(sAliasDst,sFNold,sFNnew),sAliasSrc)
            try:
                if os.path.exists(sFNnew):
                    vtLog.vtLngCur(vtLog.CRITICAL,'dest:%s;srcFN:%s;dstFN:%s destination already exists'%(sAliasDst,sFNold,sFNnew),sAliasSrc)
                    return -1
                doc.Save(sFNnew)
                if self.par is not None:
                    self.par.NotifyClone(sAliasSrc,sAliasDst,sFNnew,id)
            except:
                doc.SetFN(sFNold)
                return -1
            doc.SetFN(sFNold)
            return 1
        except:
            vtLog.vtLngTB('')
        return -1
    def __start__(self,appl,**kwargs):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'appl:%s;kwargs:%s'%(appl,vtLog.pformat(kwargs)),'')
            sAliasSrc=':'.join([appl,kwargs['source']])
            doc=self.GetServedXmlDom(sAliasSrc)
            if doc is None:
                return -1
            tup=self.docs[sAliasSrc]
            if 'id' in tup:
                id=tup['id']
            else:
                id=-2
            if tup['status']==self.SERVE_STOPPED:
                self.StartServedXml(sAliasSrc)
                if self.par is not None:
                    self.par.NotifyStart(sAliasSrc,id)
                return 1
            return 0
        except:
            pass
        return -1
    def __stop__(self,appl,**kwargs):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'appl:%s;kwargs:%s'%(appl,vtLog.pformat(kwargs)),'')
            sAliasSrc=':'.join([appl,kwargs['source']])
            doc=self.GetServedXmlDom(sAliasSrc)
            if doc is None:
                return -1
            tup=self.docs[sAliasSrc]
            if 'id' in tup:
                id=tup['id']
            else:
                id=-2
            if tup['status']==self.SERVE_RUNNING:
                self.StopServedXml(sAliasSrc)
                if self.par is not None:
                    self.par.NotifyStop(sAliasSrc,id)
                return 1
            return 0
        except:
            pass
        return -1
    def __convKWArgs4DoCmd__(self,dArgs,kwargs):
        for k in kwargs.keys():
            try:
                if k in dArgs:
                    tup=dArgs[k]
                    sType=tup[1]
                    if sType=='int':
                        v=int(kwargs[k])
                    elif sType=='long':
                        v=long(kwargs[k])
                    elif sType=='float':
                        v=float(kwargs[k])
                    elif sType=='double':
                        v=float(kwargs[k])
                    elif sType=='pseudofloat':
                        v=long(kwargs[k])
                    else:
                        v=kwargs[k]
                    kwargs[k]=v
            except:
                pass
    def DoCmd(self,bOkExec,bOkBuild,applAlias,action,**kwargs):
        """ return
            1       ... action executed correctly
            0       ... action unknown
           -1       ... fault occured during execution 
           -2       ... permission denied
           -3       ... action execution in process
           -4       ... remote execution not possible
        """
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'action:%s;kwargs:%s'%(action,vtLog.pformat(kwargs)),applAlias)
            if action.startswith('__'):
                # server related actions
                if action in self.CMD_DICT:
                    appl,alias=applAlias.split(':')
                    if 'source' not in kwargs:
                        kwargs['source']=alias
                    tup=self.CMD_DICT[action]
                    if (tup[1]&0x04)!=0:
                        if bOkBuild==False:
                            return -2
                    else:
                        if bOkExec==False:
                            return -2
                    dArgs=tup[2]
                    self.__convKWArgs4DoCmd__(dArgs,kwargs)
                    f=getattr(self,action)
                    return f(appl,**kwargs)
                    #return doc.DoCmd(action,**kwargs)
            else:
                doc=self.GetServedXmlDom(applAlias)
                if doc is None:
                    return -4
                dCmd=doc.GetCmdDict()
                if action.find(':')>0:
                    tagName,action=action.split(':')
                    if vtLog.vtLngIsLogged(vtLog.INFO):
                        vtLog.vtLngCur(vtLog.INFO,'tagName:%s;action:%s;kwargs:%s'%\
                                (tagName,action,vtLog.pformat(kwargs)),applAlias)
                    oReg=doc.GetReg(tagName)
                    dCmd=dCmd['__regNodeCmds__'][tagName]
                    if action in dCmd:
                        tup=dCmd[action]
                        if (tup[1]&0x04)!=0:
                            if bOkBuild==False:
                                return -2
                        else:
                            if bOkExec==False:
                                return -2
                        dArgs=tup[2]
                        self.__convKWArgs4DoCmd__(dArgs,kwargs)
                        try:
                            node=doc.getNodeByIdNum(kwargs['|id'])
                        except:
                            node=doc.getBaseNode()
                        return oReg.DoCmd(node,self,action,**kwargs)
                else:
                    if action in dCmd:
                        tup=dCmd[action]
                        if (tup[1]&0x04)!=0:
                            if bOkBuild==False:
                                return -2
                        else:
                            if bOkExec==False:
                                return -2
                        dArgs=tup[2]
                        self.__convKWArgs4DoCmd__(dArgs,kwargs)
                        return doc.DoCmd(self,action,**kwargs)
            #if self.IsServedStatus(applAlias,self.SERVE_RUNNING)<1:
            #    return None,-10
        except:
            vtLog.vtLngTB(applAlias)
        return -4
    def SetServedHosts(self,alias,hosts):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCS(vtLog.INFO,'host:%s'%(hosts),origin=alias)
        try:
            lstHosts=self.hosts[alias]
            # check current connections
        except:
            pass
        lst=[]
        for tup in hosts:
            i=2
            if tup[1]=='__vHum__':
                i=0
            elif tup[1]=='__srv__':
                i=1
            lst.append((i,tup))
        def compFunc(a,b):
            iRes=cmp(a[0],b[0])
            return iRes
        lst.sort(compFunc)
        tmp=[]
        for tup in lst:
            tmp.append(tup[1])
        self.hosts[alias]=tmp
    def GetServedXml(self,alias,adr,sockInst,bThread=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCS(vtLog.INFO,'adr:%s'%(repr(adr)),origin=alias)
        if self.docs.has_key(alias):
            tup=self.docs[alias]
            tup['connections'].append((adr,sockInst))
            doc=tup['doc']
            if tup['last_access'] is None:
                #doc.Open(tup['fn'])
                vtLog.vtLngCur(vtLog.INFO,'alias:%s;opening'%alias,alias)
                vtLog.PrintMsg(_('%s:opening')%alias)
                iRet=doc.Open(tup['fn'],bThread=False)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'open return:%d'%(iRet),alias)
                if iRet==-1:
                    doc.New()
                    doc.Save(tup['fn'],bThread=bThread)
                else:
                    doc.acquire('dom')
                    try:
                        doc.CreateReq()
                        doc.BuildSrv()
                        pass
                    except:
                        vtLog.vtLngTB(alias)
                    doc.release('dom')
                self.__ensureHumDoc4SecAcl__(doc)
                vtLog.vtLngCur(vtLog.INFO,'alias:%s;opened'%alias,alias)
                vtLog.PrintMsg(_('%s:opened')%alias)
                tup['last_access']=time.time()
            return doc
        else:
            return None
    def GetServedXmlDom(self,alias):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'alias:%s'%alias,alias)
        if self.docs.has_key(alias):
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'alias:%s found'%alias,alias)
            tup=self.docs[alias]
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'alias:%s;%s'%(alias,vtLog.pformat(tup)),alias)
            doc=tup['doc']
            if tup['last_access'] is None:
                vtLog.vtLngCur(vtLog.INFO,'alias:%s;opening'%alias,alias)
                vtLog.PrintMsg(_('%s:opening')%alias)
                iRet=doc.Open(tup['fn'])
                if iRet==-1:
                    doc.New()
                    doc.Save(tup['fn'])
                else:
                    doc.acquire('dom')
                    try:
                        doc.CreateReq()
                        doc.BuildSrv()
                    except:
                        vtLog.vtLngTB(alias)
                    doc.release('dom')
                self.__ensureHumDoc4SecAcl__(doc)
                vtLog.vtLngCur(vtLog.INFO,'alias:%s;opened'%alias,alias)
                vtLog.PrintMsg(_('%s:opened')%alias)
                tup['last_access']=time.time()
            return doc
        else:
            if hasattr(self,'hum2SecAcl'):
                return self.hum2SecAcl
            return None
    def __ensureHumDoc4SecAcl__(self,doc):
        lHumAliases=self.GetServedXmlLst('vHum')
        node=doc.getChildForced(doc.getRoot(),'security_acl')
        if node is None:
            vtLog.vtLngCur(vtLog.ERROR,'security_acl node not found','vXmlSrv')
        if hasattr(self,'hum2SecAcl'):
            lHumAliases.append('___')
        if len(lHumAliases)>0:
            for sHum in lHumAliases:
                nodeHum=doc.getChild(node,sHum)
                if nodeHum is None:
                    vtLog.vtLngCur(vtLog.INFO,'add human alias %s'%sHum,'vXmlSrv')
                    #print 'add',sHum
                    nodeHum=doc.createChildByLst(node,sHum,[
                                    {'tag':'all','val':'','attr':['sec_level','-1']},
                                    ])
                    doc.setAttribute(nodeHum,'active','0')
    def SetHumDoc4SecAcl(self,doc):
        self.hum2SecAcl=doc
    def GetHumDoc4SecAclLst(self):
        l=[]
        lHumAliases=self.GetServedXmlLst('vHum')
        if hasattr(self,'hum2SecAcl'):
            lHumAliases.append('___')
        for sName in lHumAliases:
            sApplAlias=':'.join(['vHum',sName])
            docHum=self.GetServedXmlDom(sApplAlias)
            bNet=hasattr(docHum,'NotifyConnected')
            l.append((sName,docHum,None,bNet))
        return l
    def CleanUpHumDocSecAcl(self,doc):
        lHumAliases=self.GetServedXmlLst('vHum')
        #vtLog.CallStack('')
        #print lHumAliases
        node=doc.getChildForced(doc.getRoot(),'security_acl')
        if node is None:
            vtLog.vtLngCur(vtLog.ERROR,'security_acl node not found','vXmlSrv')
        d=[]
        for c in doc.getChilds(node):
            d[doc.getTagName(c)]=c
        for k in d.keys():
            try:
                i=lHumAliases.index(k)
            except:
                doc.delNode(d[k])
                vtLog.vtLngCur(vtLog.DEBUG,'delete vHum alias%s'%k,'vXmlSrv')
    def GetServedXmlLst(self,appl=None):
        try:
            if appl is None:
                keys=self.docs.keys()
                keys.sort()
                return keys
            l=[]
            appl=appl+':'
            keys=self.docs.keys()
            iLen=len(appl)
            for k in keys:
                if k[:iLen]==appl:
                    l.append(k[iLen:])
            l.sort()
            return l
        except:
            return l
    def SetLoginCache(self,dLogin):
        vtLog.vtLngCur(vtLog.INFO,'','vXmlSrv')
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'dLogin:%s'%(vtLog.pformat(dLogin)),'vXmlSrv')
        self.dLoginCache=dLogin
    def GetServedXml4UsrLst(self,usr,appl):
        try:
            if self.dLoginCache is None:
                vtLog.vtLngCur(vtLog.WARN,'login chache not present','vXmlSrv')
                return None
            if usr in self.dLoginCache:
                ll=self.dLoginCache[usr]
                if len(appl)>0:
                    l=[]
                    for it in l:
                        if it.startswith(appl):
                            l.append(it)
                    l.sort()
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'usr:%s;appl:%s;l:%s'%(
                                    usr,appl,vtLog.pformat(l)),'vXmlSrv')
                    else:
                        vtLog.vtLngCur(vtLog.INFO,'usr:%s;appl:%s'%(usr,appl),'vXmlSrv')
                    return l
                ll.sort()
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'usr:%s;l:%s'%(
                                usr,vtLog.pformat(ll)),'vXmlSrv')
                else:
                    vtLog.vtLngCur(vtLog.INFO,'usr:%s'%(usr),'vXmlSrv')
                return ll
        except:
            vtLog.vtLngTB('vXmlSrv')
        return None
    def IsServedXml(self,alias):
        if self.docs.has_key(alias):
            bOk=True
        else:
            bOk=False
        if self.verbose:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'is served:%d'%bOk,alias)
        return bOk
    def Stop(self):
        vtSrvSock.Stop(self)
        for alias,tup in self.docs.iteritems():
            vtLog.vtLngCur(vtLog.INFO,''%(),alias)
            for conn in tup['connections']:
                try:
                    self.StopSocket(conn[0],conn[1])
                except:
                    vtLog.vtLngTB(alias)
    def IsServing(self):
        if vtSrvSock.IsServing(self):
            return True
        bRun=False
        for alias,tup in self.docs.iteritems():
            lConn=tup['connections']
            if len(lConn)>0:
                vtLog.vtLngCur(vtLog.INFO,'count:%sconnections:%s'%(
                            len(lConn),vtLog.pformat(lConn)),alias)
                bRun=True
        return bRun
    def IsServing_old(self):
        if vtSrvSock.IsServing(self)==False:
            return False
        if len(self.docs.keys())>0:
            return True
        else:
            return False
    def DelServedXml(self,alias,bWaitConnectionsClosed=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=alias)
        if self.docs.has_key(alias):
            tup=self.docs[alias]
            #.append((adr,sockInst))
            if self.acquire(False):
                bRelease=True
            else:
                bRelease=False
                vtLog.vtLngCur(vtLog.CRITICAL,''%(),alias)
            for conn in tup['connections']:
                #self.StopSocket(conn[0],conn[1])
                try:
                    conn[1].Close()
                except:
                    vtLog.vtLngTB(alias)
            if bRelease:
                self.release()
            if bWaitConnectionsClosed:
                zTime=0
                zWait=0.1
                while len(tup['connections'])!=0:
                    time.sleep(zWait)
                    zTime+=zWait
                    if zTime<60.0:
                        vtLog.vtLngCur(vtLog.CRITICAL,'time out'%(),alias)
                        break
            doc=tup['doc']
            doc.AlignDoc()
            doc.Save()
            doc.ShutDown()
            doc.Close()
            doc.WaitConsumerNotRunning()
            doc.DelConsumer()
            doc.ShutDown()
            self.docs[alias]=None
            del self.docs[alias]
            #vtLog.CallStack('')
            #print vtLog.pformat(self.__dict__)
            return True
        else:
            #vtLog.CallStack('')
            #print vtLog.pformat(self.__dict__)
            return False
    def GetSock(self,alias,adr):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCS(vtLog.INFO,'adr:%s'%(vtLog.pformat(adr)),origin=alias)
        if self.docs.has_key(alias):
            tup=self.docs[alias]
            for it in tup['connections']:
                if it[0]==adr:
                    return it[1]
        else:
            return None
    def GetServedXmlInfo(self,alias,what):
        if alias in self.docs:
            t=self.docs[alias]
            if what in t:
                return t[what]
        return None
    def GetServedXmlConnections(self,alias):
        if alias in self.docs:
            t=self.docs[alias]
            l=[]
            i=0
            for conn in t['connections']:
                oLogin=conn[1].objLogin
                sUsr=''
                iSecLv=-1
                if oLogin is not None:
                    sUsr=oLogin.GetUsr()
                    iSecLv=oLogin.GetSecLv()
                l.append((i,conn[0][0],conn[0][1],sUsr,iSecLv))
                i+=1
            return l
        return None
    def CloseConnection(self,alias,sHost,iPort):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCS(vtLog.INFO,'host:%s port:%d'%(sHost,iPort),origin=alias)
        if alias in self.docs:
            t=self.docs[alias]
            tCmp=(sHost,iPort)
            for conn in t['connections']:
                if conn[0]==tCmp:
                    conn[1].Close()
    def AccessServedXml(self,alias):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCS(vtLog.INFO,'access served xml',origin=alias)
        if self.docs.has_key(alias):
            self.docs[alias]['last_access']=time.time()
    def ModifyServedXml(self,alias):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCS(vtLog.INFO,'modify served xml',origin=alias)
        if self.docs.has_key(alias):
            self.docs[alias]['last_modify']=time.time()
    def ModifyServedXmlAcl(self,alias):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',alias)
        self.ModifyServedXml(alias)
        self.NotifyAcl(alias)
    def Modify(self,alias,adr,idLock,id,content):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'idLock:%s id:%s by adr:%s'%(idLock,id,adr),origin=alias)
        if self.docs.has_key(alias):
            self.docs[alias]['last_modify']=time.time()
            #if self.verbose:
            #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'idlock:%s id:%s'%(idLock,id))
            self.NotifySetNode(alias,adr,idLock,id,content)
    def ModifyFull(self,alias,adr,idLock,id,content):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'idLock:%s id:%s by adr:%s'%(idLock,id,adr),origin=alias)
        if self.docs.has_key(alias):
            self.docs[alias]['last_modify']=time.time()
            #if self.verbose:
            #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'idlock:%s id:%s'%(idLock,id))
            self.NotifySetNodeFull(alias,adr,idLock,id,content)
    def Update(self,alias,node):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=alias)
        if self.docs.has_key(alias):
            tup=self.docs[alias]
            tup['last_modify']=time.time()
            doc=tup['doc']
            id=doc.getKeyNum(node)
            doc.clearFingerPrint(node)
            doc.GetFingerPrintAndStore(node)
            doc.AlignNode(node)
            content=doc.GetNodeXmlContent(node)
            self.Modify(alias,'',id,id,content)
    def AddNode(self,alias,adr,idPar,id,content):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'idPar:%s id:%s by adr:%s'%(idPar,id,adr),origin=alias)
        #if self.verbose:
        #    if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #        vtLog.vtLngCur(vtLog.DEBUG,'add node content:%s'%(content),alias)
        if self.docs.has_key(alias):
            self.docs[alias]['last_modify']=time.time()
            #if self.verbose:
            #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'idlock:%s id:%s'%(idLock,id))
            self.NotifyAddNode(alias,adr,idPar,id,content)
    def DelNode(self,alias,adr,id):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%s by adr:%s'%(id,adr),origin=alias)
        if self.docs.has_key(alias):
            self.docs[alias]['last_modify']=time.time()
            #if self.verbose:
            #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'idlock:%s id:%s'%(idLock,id))
            self.NotifyDelNode(alias,adr,id)
    def MoveNode(self,alias,adr,idPar,id):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'idPar:%s id:%s by adr:%s'%(idPar,id,adr),origin=alias)
        if self.docs.has_key(alias):
            self.docs[alias]['last_modify']=time.time()
            #if self.verbose:
            #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'idlock:%s id:%s'%(idLock,id))
            self.NotifyMoveNode(alias,adr,idPar,id)
    def AutoSave(self):
        #iVerbose=vtLog.vtLngCallStack(self,vtLog.DEBUG,'',origin=alias)
        keys=self.docs.keys()
        for k in keys:
            tup=self.docs[k]
            if tup['last_modify'] is not None:
                zSave=tup['last_save']
                if (time.time()-zSave)>self.zDiffSave:
                    if vtLog.vtLngIsLogged(vtLog.INFO):
                        vtLog.vtLngCS(vtLog.INFO,'save last_save:%s fn:%s'%(time.asctime(time.localtime(tup['last_save'])),tup['fn']),origin=k)
                    tup['doc'].AlignDoc()
                    tup['doc'].Save()
                    tup['last_save']=time.time()
                    tup['last_modify']=None
                    #if self.verbose:
                    #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'save %s at %s'%(k,time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))))
    def SaveXml(self,alias):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',alias)
        if alias in self.docs:
            tup=self.docs[alias]
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'%s'%(vtLog.pformat(tup)),alias)
            bSkip=False
            try:
                if tup['last_modify'] is None and tup['last_access'] is None:
                    bSkip=True
            except:
                pass
            if bSkip:
                if vtLog.vtLngIsLogged(vtLog.INFO):
                    vtLog.vtLngCur(vtLog.INFO,'no need to save',alias)
                return 
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'save last_save:%s;fn:%s'%(time.asctime(time.localtime(tup['last_save'])),tup['fn']),alias)
            tup['doc'].AlignDoc()
            tup['doc'].Save()
            tup['last_save']=time.time()
            tup['last_modify']=None
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'aliases:%s'%(vtLog.pformat(self.docs.keys())),alias)
        else:
            vtLog.vtLngCur(vtLog.WARN,'alias not served;aliases:%s'%(vtLog.pformat(self.docs.keys())),alias)
    def AutoClose(self):
        #iVerbose=vtLog.vtLngCallStack(self,vtLog.DEBUG,'',origin=alias)
        keys=self.docs.keys()
        for k in keys:
            tup=self.docs[k]
            if tup['last_use'] is not None:
                zUse=tup['last_use']
                if (time.time()-zUse)>self.zDiffClose:
                    if vtLog.vtLngIsLogged(vtLog.INFO):
                        vtLog.vtLngCS(vtLog.INFO,'close last_use:%s fn:%s'%(time.asctime(time.localtime(tup['last_use'])),tup['fn']),origin=k)
                    tup['doc'].AlignDoc()
                    tup['doc'].Save()
                    tup['doc'].Close()
                    #tup['last_save']=time.time()
                    tup['last_access']=None
                    tup['last_use']=None
                    #if self.verbose:
                    #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'save %s at %s'%(k,time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))))
    def Lock(self,alias,adr,id,objLogin):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCS(vtLog.DEBUG,'lock id:%s by adr:%s'%(id,adr),origin=alias)
        try:
            iRet=self.IsLock(alias,adr,id)
            if iRet==0:
                #self.locks[alias].append((id,adr))
                sock,iConnIdx=self.GetServedXmlDomConnection(alias,adr)
                if sock.IsLockBlocked(id,self.zLockBlock):
                    return 3  # lock is blocked
                self.locks[alias].append(vtNetXmlLock(id,adr[0],adr[1],objLogin))
                return 1    # lock added
            elif iRet==1:
                return 0    # already locked by another
            else:
                return 2    # already locked by this address
        except:
            vtLog.vtLngTB(alias)
            return -1   # fault
    def UnLock(self,alias,adr,id,silent=False):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'unlock id:%s by adr:%s'%(id,adr),alias)
        try:
            lst=self.locks[alias]
            i=0
            for tup in lst:
                if tup.IsIDAddrPort(id,adr):
                    del lst[i]
                    if silent==False:
                        self.NotifyUnLock(alias,adr,id)
                    return 1
                i+=1
            return 0
            try:
                lst.remove((id,adr))
                # notify other applications
                self.NotifyUnLock(alias,adr,id)
                return 1
            except:
                return 0
        except:
            vtLog.vtLngTB(alias)
            return -1
    def DelReqLock(self,alias):
        try:
            lst=self.locksReq[alias]
            for tup in lst:
                if (tup[2]+self.zLockReqRel) < time.time():
                    lst.remove(tup)
        except:
            vtLog.vtLngTB(alias)
    def DelReqLockFull(self,alias):
        try:
            lst=self.locksReqFull[alias]
            for tup in lst:
                if (tup[2]+self.zLockReqRel) < time.time():
                    lst.remove(tup)
        except:
            vtLog.vtLngTB(alias)
    def GetReqLock(self,alias,id):
        try:
            lst=self.locksReq[alias]
            for tup in lst:
                if tup[0]==id:
                    return tup
            return None
        except:
            vtLog.vtLngTB(alias)
        return None
    def ReqLock(self,alias,adr,id,objLogin):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s by adr:%s'%(id,adr),alias)
        try:
            self.DelReqLock(alias)
            lst=self.locksReq[alias]
            if 0:
                vtLog.CallStack('')
                print alias,adr,id
            for tup in lst:
                if tup[0]==id:
                    if tup[1]==adr:
                        if (tup[2]+self.zLockReq) < time.time():
                            # release current lock
                            lstLock=self.locks[alias]
                            i=0
                            for tupLock in lstLock:
                                if tupLock.IsID(id):
                                    self.locks[alias].append(vtNetXmlLock(id,adr[0],adr[1],objLogin))
                                    del lstLock[i]
                                    sock,iConnIdx=self.GetServedXmlDomConnection(alias,tupLock.GetAddrPort())
                                    sock.LockBlock(long(id))
                                    self.NotifyUnLock(alias,tupLock.GetAddrPort(),id)
                                    lst.remove(tup)
                                    return 3,0 # lock assigned
                                i+=1
                            return 4,-1 # lock not present any more
                        else:
                            return 2,(tup[2]+self.zLockReq) - time.time()
                    else:
                        # request already present
                        return 0,0
            lst.append([long(id),adr,time.time()])
            # notify locking connection
            lstLock=self.locks[alias]
            i=0
            for tupLock in lstLock:
                if tupLock.IsID(id):
                    conn,iConnIdx=self.GetServedXmlDomConnection(alias,tupLock.GetAddrPort())
                    if conn is not None:
                        conn.NotifyReqLock(id,adr,objLogin.GetUsr(),iConnIdx)
            return 1,self.zLockReq
        except:
            vtLog.vtLngTB(alias)
            return -1,0
    def ReqLockFull(self,alias,adr,id,objLogin):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s by adr:%s'%(id,adr),alias)
        try:
            self.DelReqLockFull(alias)
            lst=self.locksReqFull[alias]
            for tup in lst:
                if tup[0]==id:
                    if tup[1]==adr:
                        if (tup[2]+self.zLockReq) < time.time():
                            # release current lock
                            lstLock=self.locks[alias]
                            i=0
                            for tupLock in lstLock:
                                if tupLock.IsID(id):
                                    #self.locks[alias].append(vtNetXmlLock(id,adr[0],adr[1],objLogin))
                                    #del lstLock[i]
                                    sock,iConnIdx=self.GetServedXmlDomConnection(alias,tupLock.GetAddrPort())
                                    #sock.LockBlock(long(id))
                                    self.NotifyUnLock(alias,tupLock.GetAddrPort(),id)
                                    #lst.remove(tup)
                                    #return 3,0 # lock assigned
                                i+=1
                            self.locks[alias]=[]
                            return 3,0 # lock assigned
                            return 4,-1 # lock not present any more
                        else:
                            return 2,(tup[2]+self.zLockReq) - time.time()
                    else:
                        # request already present
                        return 0,0
            lst.append([long(id),adr,time.time()])
            # notify locking connection
            lstLock=self.locks[alias]
            i=0
            for tupLock in lstLock:
                if tupLock.IsID(id):
                    conn,iConnIdx=self.GetServedXmlDomConnection(alias,tupLock.GetAddrPort())
                    if conn is not None:
                        conn.NotifyReqLock(id,adr,objLogin.GetUsr(),iConnIdx)
            return 1,self.zLockReq
        except:
            vtLog.vtLngTB(alias)
            return -1,0
    def doReleaseLock(self,n,doc,alias,adr,adrReq,zReq,kind,msg,objLogin,sock):
        try:
            iId=doc.getKeyNum(n)
            if self.IsLock(alias,adr,iId):
                self.locksReq[alias].append([iId,adrReq,zReq])
                self.ReleaseLock(alias,adr,iId,kind,msg,objLogin)
                sock.NotifyUnLock(iId)
        except:
            vtLog.vtLngTB(alias)
        return 0
    def ReleaseLock(self,alias,adr,id,kind,msg,objLogin):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'id:%r by adr:%s'%(id,adr),alias)
        try:
            tupReq=self.GetReqLock(alias,id)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'id:%r by adr:%s;tupReq:%s'%(id,adr,vtLog.pformat(tupReq)),alias)
            lst=self.locks[alias]
            if 0:
                vtLog.CallStack('')
                print alias,adr,id,kind,msg
                print objLogin.GetStr()
                print 'locks',lst
                print 'requests',self.locksReq[alias]
                print 'req',tupReq
            if tupReq is None:
                i=0
                for tup in lst:
                    if tup.IsID(id):
                        sock,iConnIdx=self.GetServedXmlDomConnection(alias,adr)
                        sock.LockBlock(long(id))
                        self.UnLock(alias,adr,id)
                        #self.NotifyUnLock(alias,adr,id)
                        return 1
                    i+=1
            #    vtLog.vtLngCur(vtLog.ERROR,'id:%s'%id,alias)
                return 0
            #id=long(id)
            i=0
            for tup in lst:
                if tup.IsID(id):
                    if adr is None:
                        del lst[i]
                        sock,iConnIdx=self.GetServedXmlDomConnection(alias,tup.GetAddrPort())
                        sock.LockBlock(long(id))
                        self.NotifyUnLock(alias,adr,id)
                        self.DelReqLock(alias)
                        return 1
                    elif tup.IsAddrPort(adr):
                        if kind in ['ok','release']:          # released by user
                            #del lst[i]
                            adrReq=tupReq[1]
                            sock,iConnIdx=self.GetServedXmlDomConnection(alias,tup.GetAddrPort())
                            if kind in ['release']:       # released by timeout
                                sock.LockBlock(long(id))
                            
                            self.UnLock(alias,adr,id,silent=True)
                            
                            if adrReq is None:
                                pass
                            else:
                                sockReq,iConnIdx=self.GetServedXmlDomConnection(alias,adrReq)
                                iRet=self.Lock(alias,adrReq,id,sockReq.objLogin)
                                if iRet==1:
                                    sockReq.NotifyReqLockGrant(id,adr,objLogin.GetUsr(),msg,iConnIdx)
                                    #self.NotifyUnLock(alias,adr,id)
                                else:
                                    vtLog.vtLngCur(vtLog.CRITICAL,'id:%s;lock returned %d'%(id,iRet),alias)
                                    sockReq.NotifyLock(str(id),iRet)
                                    sock.NotifyUnLock(id)
                            #self.NotifyUnLock(alias,adr,id)
                            self.DelReqLock(alias)
                            self.locksReq[alias].remove(tupReq)
                            # do recursive node
                            if alias in self.docs:
                                tup=self.docs[alias]
                                doc=tup['doc']
                                n=doc.getNodeByIdNum(id)
                                if n is not None:
                                    doc.procChildsKeys(n,self.doReleaseLock,
                                            doc,alias,adr,adrReq,tupReq[2],kind,msg,objLogin,sock)
                                else:
                                    vtLog.vtLngCur(vtLog.ERROR,'id:%s;node not found'%(id),alias)
                            return 1
                        elif kind=='reject':
                            addr=tupReq[1]
                            if addr is None:
                                pass
                            else:
                                sock,iConnIdx=self.GetServedXmlDomConnection(alias,addr)
                                sock.NotifyReqLockReject(id,adr,objLogin.GetUsr(),msg,iConnIdx)
                                self.DelReqLock(alias)
                            self.locksReq[alias].remove(tupReq)
                        return 0
                    else:
                        vtLog.vtLngCur(vtLog.ERROR,'id:%s;locked now by another connection;%s'%(id,'%s:%d'%tup.GetAddrPort()),alias)
                        break
                i+=1
            self.DelReqLock(alias)
            self.locksReq[alias].remove(tupReq)
            return 0
        except:
            vtLog.vtLngTB(alias)
            return -1
    def ReleaseLockFull(self,alias,adr,id,kind,msg,objLogin):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s by adr:%s'%(id,adr),alias)
        try:
            pass
        except:
            vtLog.vtLngTB(alias)
            return -1
    def RequestLock(self,alias,adr,id):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s by adr:%s'%(id,adr),alias)
        try:
            lst=self.locks[alias]
            i=0
            for tup in lst:
                if tup.IsID(id):
                    if adr is None:
                        conn,iConnIdx=self.GetServedXmlDomConnection(alias,tup.GetAddrPort())
                        if conn is not None:
                            conn.NotifyReqLock(long(id),adr,'vXmlSrv',iConnIdx)
                        self.locksReq[alias].append([long(id),adr,time.time()])
                        #sock.NotifyUnLock(alias,adr,id)
                        return 1
                    else:
                        vtLog.vtLngCur(vtLog.ERROR,'id:%s;you are not supposed to be here'%(id),alias)
                        break
                i+=1
            return 0
        except:
            vtLog.vtLngTB(alias)
            return -1    
    def ForceLock(self,alias,adr,id):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s by adr:%s'%(id,adr),alias)
        try:
            lst=self.locks[alias]
            i=0
            for tup in lst:
                if tup.IsID(id):
                    if adr is None:
                        del lst[i]
                        sock,iConnIdx=self.GetServedXmlDomConnection(alias,tup.GetAddrPort())
                        sock.LockBlock(long(id))
                        sock.NotifyUnLock(long(id))
                        self.NotifyUnLock(alias,adr,id)
                        return 1
                    elif tup.IsAddrPort(adr):
                        break
                    else:
                        break
                i+=1
            return 0
        except:
            vtLog.vtLngTB(alias)
            return -1
    def IsLock(self,alias,adr,id):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'is lock:%s by adr:%s'%(id,adr),alias)
        try:
            lst=self.locks[alias]
            doc=self.docs[alias]['doc']
            for tup in lst:
                #vtLog.vtLngCallStack(self,vtLog.DEBUG,'%s == %s'%(id,tup[0]))
                if tup.IsID(id):
                    if tup.IsAddrPort(adr):
                        return 2
                    else:
                        return 1
                continue
                # 060805 FIXME forget this old stuff
                #if self.verbose:
                #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'check %s %d'%(alias,self.docs.has_key(alias)))
                node=doc.getNodeByIdNum(id)
                node=doc.getInstBase(node)
                idBase=doc.getAttribute(node,'id')
                node=doc.getNodeByIdNum(tup.GetID())
                node=doc.getTmplBase(node)
                #vtLog.vtLngCallStack(self,vtLog.DEBUG,repr(node),level=1)
                childs=doc.getChilds(node,'__node')
                #vtLog.vtLngCallStack(self,vtLog.DEBUG,repr(childs),level=1)
                if len(childs)>0:
                    for c in childs:
                        iid=doc.getAttribute(c,'iid')
                        #if self.verbose:
                        #    vtLog.vtLngCallStack(self,vtLog.DEBUG,iid,level=2)
                        #    vtLog.vtLngCallStack(self,vtLog.DEBUG,idBase,level=2)
                        #    vtLog.vtLngCallStack(self,vtLog.DEBUG,tup[1],level=2)
                        #    vtLog.vtLngCallStack(self,vtLog.DEBUG,adr,level=2)
                        if iid==idBase:
                            if tup.IsAddrPort(adr):
                                return 2
                            else:
                                return 1
            node=doc.getNodeByIdNum(id)
            if doc.getChild(node,'__node') is not None:
                # this a tmpl-node
                # force all instance-locks to be released
                #vtLog.vtLngCallStack(self,vtLog.DEBUG,'this is a tmpl-node',level=1)
                node=doc.getTmplBase(node)
                idTmpl=doc.getAttribute(node,'id')
                sock=self.GetSock(alias,adr)
                usr=sock.GetUsr()
                lstLockDel=[]
                for tup in lst:
                    #if self.verbose:
                    #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'id:%s id:%s'%(idTmpl,tup[0]),level=2)
                    node=doc.getNodeByIdNum(tup[0])
                    if len(doc.getAttribute(node,'iid'))>0:
                        node=doc.getInstBase(node)
                        iid=doc.getAttribute(node,'iid')
                        #if self.verbose:
                        #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'id:%s iid:%s'%(idTmpl,iid),level=2)
                        if iid==idTmpl:
                            #if self.verbose:
                            #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'adr:%s usr:%s at %s'%(tup[1],usr,adr),level=2)
                            sock=self.GetSock(alias,tup.GetAddrPort())
                            sock.NotifyLockReleased(tup.GetID(),usr,adr)
                            lstLockDel.append(tup)
                for tup in lstLockDel:
                    lst.remove(tup)
            return 0
        except:
            vtLog.vtLngTB(alias)
            return -1
    def GetLockLst(self,alias,adr=None):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'',alias)
        try:
            l=[]
            lst=self.locks[alias]
            if adr is None:
                return lst
            for tup in lst:
                if not tup.IsAddrPort(adr):
                    l.append(tup)
            return l
        except:
            vtLog.vtLngTB(alias)
    def GetLock(self,alias,id):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCS(vtLog.DEBUG,'get lock id:%s'%id,origin=alias)
        try:
            lst=self.locks[alias]
            doc=self.docs[alias]['doc']
            for tup in lst:
                if tup.IsID(id):
                    addr=tup.GetAddrPort()
                    sock=self.GetSock(alias,addr)
                    usr=sock.GetUsr()
                    return (usr,addr)
                continue
                # 060805 FIXME forget this old stuff
                node=doc.getNodeByIdNum(id)
                node=doc.getInstBase(node)
                idBase=doc.getAttribute(node,'id')
                node=doc.getNodeByIdNum(tup.GetID())
                node=doc.getNodeByIdNum(idBase)
                node=doc.getTmplBase(node)
                childs=doc.getChilds(node,'__node')
                if len(childs)>0:
                    for c in childs:
                        iid=doc.getAttribute(c,'iid')
                        if iid==idBase:
                            adr=tup.GetAddrPort()
                            sock=self.GetSock(alias,adr)
                            usr=sock.GetUsr()
                            return (usr,adr)
        except:
            vtLog.vtLngTB(alias)
            return None
        vtLog.vtLngCur(vtLog.WARN,'locks:%s'%(vtLog.pformat(self.locks[alias])),alias)
        return None
    def GetServedXmlDomConnection(self,alias,adr):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'adr:%s'%(repr(adr)),alias)
        try:
            iConnIdx=0
            tup=self.docs[alias]
            conn=tup['connections']
            for it in conn:
                if it[0]==adr:
                    return it[1],iConnIdx
                iConnIdx+=1
        except:
            pass
        return None,-1
    def NotifyUnLock(self,alias,adr,id):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'notify unlock id:%s by adr:%s'%(id,adr),alias)
        try:
            tup=self.docs[alias]
            conn=tup['connections']
            for it in conn:
                if it[0]!=adr:
                    it[1].NotifyUnLock(id)
        except:
            pass
    def NotifySetNode(self,alias,adr,idLock,id,content):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'notify setnode idLock:%s id:%s by adr:%s'%(idLock,id,adr),alias)
        #if self.verbose:
        #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'alias:%s idlock:%s id:%s'%(alias,idLock,id))
        try:
            tup=self.docs[alias]
            conn=tup['connections']
            for it in conn:
                if it[0]!=adr:
                    if idLock!=id:
                        it[1].NotifyDelNode(idLock)
                        node=tup['doc'].getNodeByIdNum(long(id))
                        nodePar=tup['doc'].getParent(node)
                        idPar=tup['doc'].getKey(nodePar)
                        it[1].NotifyAddNode(idPar,id,content)
                    else:
                        it[1].NotifySetNode(id,content)
        except:
            pass
    def NotifySetNodeFull(self,alias,adr,idLock,id,content):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'notify setnodefull idLock:%s id:%s by adr:%s'%(idLock,id,adr),alias)
        #if self.verbose:
        #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'alias:%s idlock:%s id:%s'%(alias,idLock,id))
        try:
            tup=self.docs[alias]
            conn=tup['connections']
            for it in conn:
                if it[0]!=adr:
                    if idLock!=id:
                        it[1].NotifyDelNode(idLock)
                    else:
                        it[1].NotifySetNodeFull(id,content)
        except:
            pass
    def NotifyAddNode(self,alias,adr,idPar,id,content):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'notify addnode idLock:%s id:%s by adr:%s'%(idPar,id,adr),alias)
        #if self.verbose:
        #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'alias:%s idpar:%s id:%s'%(alias,idPar,id))
        try:
            tup=self.docs[alias]
            conn=tup['connections']
            for it in conn:
                if it[0]!=adr:
                    it[1].NotifyAddNode(idPar,id,content)
        except:
            pass
    def NotifyDelNode(self,alias,adr,id):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'notify delnode id:%s by adr:%s'%(id,adr),alias)
        #if self.verbose:
        #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'alias:%s id:%s'%(alias,id))
        try:
            tup=self.docs[alias]
            conn=tup['connections']
            for it in conn:
                if it[0]!=adr:
                    it[1].NotifyDelNode(id)
        except:
            pass
    def NotifyMoveNode(self,alias,adr,idPar,id):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'notify movenode idLock:%s id:%s by adr:%s'%(idPar,id,adr),alias)
        #if self.verbose:
        #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'alias:%s idlock:%s id:%s'%(alias,idLock,id))
        try:
            tup=self.docs[alias]
            conn=tup['connections']
            for it in conn:
                if it[0]!=adr:
                    it[1].NotifyMoveNode(idPar,id)
        except:
            pass
    def NotifyAcl(self,alias):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'',alias)
        try:
            tup=self.docs[alias]
            conn=tup['connections']
            for it in conn:
                it[1].NotifyAcl()
        except:
            pass
