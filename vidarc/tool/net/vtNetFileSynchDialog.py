#Boa:Dialog:vtNetFileSynchDialog
#----------------------------------------------------------------------------
# Name:         vCheckDirPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060604
# CVS-ID:       $Id: vtNetFileSynchDialog.py,v 1.6 2009/01/24 22:02:14 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import wx.lib.filebrowsebutton
import vidarc.tool.misc.vtmListCtrl
import time,os,os.path,stat,types,threading
import Queue,sys,sha,re
from vidarc.tool.misc.vtmMsgDialog import vtmMsgDialog

import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.vApps.common.vSystem as vSystem
from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer 

from vidarc.tool.InOut.fnUtil import *
import vidarc.tool.InOut.listFiles as listFiles
from vidarc.tool.InOut.listFilesXml import *
from vidarc.tool.InOut.vtInOutFileTimes import setFileTimes
from vidarc.tool.InOut.vtInOutComp import vtInOutCompDirEntry
from vidarc.tool.InOut.vtInOutReadThread import vtInOutReadThread
from vidarc.tool.InOut.vtInOutCompFileEntryDialog import vtInOutCompFileEntryDialog

import vidarc.tool.net.vtNetArt as vtNetArt
from vidarc.tool.net.vtNetClt import vtNetClt
from vidarc.tool.net.vtNetFileClt import *
from vidarc.tool.net.vtNetSecLoginHost import getHostAliasStr,setHostAliasStr
import vidarc.config.vcCust as vcCust
from vidarc.tool.vtProcess import vtProcess

VERBOSE=0

def create(parent):
    return vtNetFileSynchDialog(parent)

[wxID_VTNETFILESYNCHDIALOG, wxID_VTNETFILESYNCHDIALOGCBCLOSE, 
 wxID_VTNETFILESYNCHDIALOGCBCONNECT, wxID_VTNETFILESYNCHDIALOGCBDELDIR, 
 wxID_VTNETFILESYNCHDIALOGCBDOWNLOAD, wxID_VTNETFILESYNCHDIALOGCBEXEC, 
 wxID_VTNETFILESYNCHDIALOGCBOPENLOCAL, wxID_VTNETFILESYNCHDIALOGCBOPENREMOTE, 
 wxID_VTNETFILESYNCHDIALOGCBPOPUP, wxID_VTNETFILESYNCHDIALOGCBREADDIR, 
 wxID_VTNETFILESYNCHDIALOGCBREFRESHDIR, wxID_VTNETFILESYNCHDIALOGCBUPLOAD, 
 wxID_VTNETFILESYNCHDIALOGCHCREMOTE, wxID_VTNETFILESYNCHDIALOGCHCSHOWEQUAL, 
 wxID_VTNETFILESYNCHDIALOGDBBDIR, wxID_VTNETFILESYNCHDIALOGLBLREMOVE, 
 wxID_VTNETFILESYNCHDIALOGLBLRESULT, wxID_VTNETFILESYNCHDIALOGLSTRES, 
 wxID_VTNETFILESYNCHDIALOGPBFILESDNLD, wxID_VTNETFILESYNCHDIALOGPBFILESUPLD, 
 wxID_VTNETFILESYNCHDIALOGPBPROCDNLD, wxID_VTNETFILESYNCHDIALOGPBPROCUPLD, 
 wxID_VTNETFILESYNCHDIALOGPNDATA, wxID_VTNETFILESYNCHDIALOGSTBRESULT, 
] = [wx.NewId() for _init_ctrls in range(24)]

def getPluginImage():
    return vCheckDirimgNet.getPluginImage()

def getApplicationIcon():
    icon = EmptyIcon()
    icon.CopyFromBitmap(vCheckDirimgNet.getPluginBitmap())
    return icon

class vtNetFileSynchDialog(wx.Dialog,vtXmlDomConsumer):
    def _init_coll_bxsRes_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.stbResult, 0, border=4, flag=wx.RIGHT)
        parent.AddWindow(self.lblResult, 1, border=0, flag=wx.EXPAND)

    def _init_coll_bxsRemote_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblRemove, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.chcRemote, 2, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbConnect, 0, border=4, flag=wx.LEFT)

    def _init_coll_bxsProcUpLd_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.pbFilesUpLd, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.pbProcUpLd, 3, border=0, flag=wx.EXPAND)

    def _init_coll_bxsBtDlg_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbExec, 0, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.cbClose, 0, border=0, flag=wx.EXPAND)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsRemote, 0, border=4, flag=wx.BOTTOM | wx.EXPAND)
        parent.AddSizer(self.bxsRes, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsDir, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsBt, 0, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsProcUpLd, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsResLstBt, 0, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsProcDnLd, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsBtDlg, 0, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER)

    def _init_coll_bxsDir_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.dbbDir, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbReadDir, 0, border=4, flag=wx.LEFT)

    def _init_coll_bxsResBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbOpenLocal, 0, border=0, flag=0)
        parent.AddWindow(self.cbOpenRemote, 0, border=4, flag=wx.TOP)
        parent.AddWindow(self.cbPopup, 0, border=4, flag=wx.TOP)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbRefreshDir, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.chcShowEqual, 1, border=4,
              flag=wx.EXPAND | wx.LEFT)
        parent.AddWindow(self.cbUpload, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.cbDownload, 0, border=4, flag=wx.LEFT)
        parent.AddSpacer(wx.Size(16, 8), border=0, flag=0)
        parent.AddWindow(self.cbDelDir, 0, border=4, flag=wx.LEFT)

    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(6)
        parent.AddGrowableCol(0)

    def _init_coll_bxsResLstBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstRes, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsResBt, 0, border=4, flag=wx.LEFT)

    def _init_coll_bxsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.pnData, 1, border=4,
              flag=wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.TOP | wx.EXPAND)

    def _init_coll_bxsProcDnLd_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.pbFilesDnLd, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.pbProcDnLd, 3, border=0, flag=wx.EXPAND)

    def _init_coll_lstRes_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'action'), width=80)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'suggestion'), width=80)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'name'), width=340)
        parent.InsertColumn(col=3, format=wx.LIST_FORMAT_LEFT, heading=u'S',
              width=30)
        parent.InsertColumn(col=4, format=wx.LIST_FORMAT_LEFT, heading='M',
              width=30)
        parent.InsertColumn(col=5, format=wx.LIST_FORMAT_LEFT, heading='tM',
              width=30)
        parent.InsertColumn(col=6, format=wx.LIST_FORMAT_LEFT, heading='tC',
              width=30)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=9, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsRemote = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsRes = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsProcDnLd = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsData = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsProcUpLd = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsDir = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsResLstBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsResBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsBtDlg = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsRemote_Items(self.bxsRemote)
        self._init_coll_bxsRes_Items(self.bxsRes)
        self._init_coll_bxsProcDnLd_Items(self.bxsProcDnLd)
        self._init_coll_bxsData_Items(self.bxsData)
        self._init_coll_bxsProcUpLd_Items(self.bxsProcUpLd)
        self._init_coll_bxsDir_Items(self.bxsDir)
        self._init_coll_bxsResLstBt_Items(self.bxsResLstBt)
        self._init_coll_bxsResBt_Items(self.bxsResBt)
        self._init_coll_bxsBtDlg_Items(self.bxsBtDlg)

        self.pnData.SetSizer(self.fgsData)
        self.SetSizer(self.bxsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTNETFILESYNCHDIALOG,
              name=u'vtNetFileSynchDialog', parent=prnt, pos=wx.Point(334, 156),
              size=wx.Size(617, 417),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE,
              title=u'vtNetFileSynch Dialog')
        self.SetClientSize(wx.Size(609, 390))
        self.Bind(wx.EVT_SIZE, self.OnSize)
        self.Bind(wx.EVT_MOVE, self.OnMove)

        self.pnData = wx.Panel(id=wxID_VTNETFILESYNCHDIALOGPNDATA,
              name=u'pnData', parent=self, pos=wx.Point(4, 4), size=wx.Size(601,
              382), style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)

        self.dbbDir = wx.lib.filebrowsebutton.DirBrowseButton(buttonText='...',
              dialogTitle=_('Choose a directory'),
              id=wxID_VTNETFILESYNCHDIALOGDBBDIR,
              labelText=_('Select a directory:'), parent=self.pnData,
              pos=wx.Point(0, 55), size=wx.Size(517, 30), startDirectory='.',
              style=0)
        self.dbbDir.SetMinSize(wx.Size(-1, -1))

        self.chcRemote = wx.Choice(choices=[],
              id=wxID_VTNETFILESYNCHDIALOGCHCREMOTE, name=u'chcRemote',
              parent=self.pnData, pos=wx.Point(172, 0), size=wx.Size(344, 21),
              style=0)
        self.chcRemote.SetMinSize(wx.Size(-1, -1))

        self.cbConnect = wx.lib.buttons.GenBitmapTextToggleButton(bitmap=wx.EmptyBitmap(16,
              16), id=wxID_VTNETFILESYNCHDIALOGCBCONNECT, label=_(u'Connect'),
              name=u'cbConnect', parent=self.pnData, pos=wx.Point(520, 0),
              size=wx.Size(76, 30), style=0)
        self.cbConnect.SetToggle(False)
        self.cbConnect.SetMinSize(wx.Size(-1, -1))
        self.cbConnect.Bind(wx.EVT_BUTTON, self.OnCbConnectButton,
              id=wxID_VTNETFILESYNCHDIALOGCBCONNECT)

        self.cbReadDir = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.DirOpen),
              id=wxID_VTNETFILESYNCHDIALOGCBREADDIR, label=_(u'Read'),
              name=u'cbReadDir', parent=self.pnData, pos=wx.Point(521, 55),
              size=wx.Size(76, 30), style=0)
        self.cbReadDir.SetMinSize(wx.Size(-1, -1))
        self.cbReadDir.Bind(wx.EVT_BUTTON, self.OnCbReadButton,
              id=wxID_VTNETFILESYNCHDIALOGCBREADDIR)

        self.chcShowEqual = wx.CheckBox(id=wxID_VTNETFILESYNCHDIALOGCHCSHOWEQUAL,
              label=_(u'show equal'), name=u'chcShowEqual', parent=self.pnData,
              pos=wx.Point(84, 89), size=wx.Size(209, 30), style=0)
        self.chcShowEqual.SetValue(False)
        self.chcShowEqual.SetMinSize(wx.Size(-1, -1))

        self.cbRefreshDir = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Synch),
              id=wxID_VTNETFILESYNCHDIALOGCBREFRESHDIR, label=_(u'Refresh'),
              name=u'cbRefreshDir', parent=self.pnData, pos=wx.Point(4, 89),
              size=wx.Size(76, 30), style=0)
        self.cbRefreshDir.SetMinSize(wx.Size(-1, -1))
        self.cbRefreshDir.Bind(wx.EVT_BUTTON, self.OnCbRefreshDirButton,
              id=wxID_VTNETFILESYNCHDIALOGCBREFRESHDIR)

        self.cbUpload = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.UpLoad),
              id=wxID_VTNETFILESYNCHDIALOGCBUPLOAD, label=_(u'Upload'),
              name=u'cbUpload', parent=self.pnData, pos=wx.Point(297, 89),
              size=wx.Size(99, 30), style=0)
        self.cbUpload.SetMinSize(wx.Size(-1, -1))
        self.cbUpload.Bind(wx.EVT_BUTTON, self.OnCbUpLoadButton,
              id=wxID_VTNETFILESYNCHDIALOGCBUPLOAD)

        self.cbDownload = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.DownLoad),
              id=wxID_VTNETFILESYNCHDIALOGCBDOWNLOAD, label=_(u'Download'),
              name=u'cbDownload', parent=self.pnData, pos=wx.Point(400, 89),
              size=wx.Size(101, 30), style=0)
        self.cbDownload.SetMinSize(wx.Size(-1, -1))
        self.cbDownload.Bind(wx.EVT_BUTTON, self.OnCbDownloadButton,
              id=wxID_VTNETFILESYNCHDIALOGCBDOWNLOAD)

        self.cbDelDir = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Del),
              id=wxID_VTNETFILESYNCHDIALOGCBDELDIR, label=_(u'Delete'),
              name=u'cbDelDir', parent=self.pnData, pos=wx.Point(521, 89),
              size=wx.Size(76, 30), style=0)
        self.cbDelDir.SetMinSize(wx.Size(-1, -1))
        self.cbDelDir.Bind(wx.EVT_BUTTON, self.OnCbDelDirButton,
              id=wxID_VTNETFILESYNCHDIALOGCBDELDIR)

        self.lstRes = vidarc.tool.misc.vtmListCtrl.vtmListCtrl(id=wxID_VTNETFILESYNCHDIALOGLSTRES,
              name=u'lstRes', parent=self.pnData, pos=wx.Point(0, 144),
              size=wx.Size(562, 179), style=wx.LC_REPORT)
        self.lstRes.SetMinSize((-1 , -1))
        self._init_coll_lstRes_Columns(self.lstRes)
        self.lstRes.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK,
              self.OnLstResListItemRightClick,
              id=wxID_VTNETFILESYNCHDIALOGLSTRES)
        self.lstRes.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstResListItemSelected,
              id=wxID_VTNETFILESYNCHDIALOGLSTRES)

        self.lblRemove = wx.StaticText(id=wxID_VTNETFILESYNCHDIALOGLBLREMOVE,
              label=_(u'remote'), name=u'lblRemove', parent=self.pnData,
              pos=wx.Point(0, 0), size=wx.Size(168, 30), style=wx.ALIGN_RIGHT)
        self.lblRemove.SetMinSize(wx.Size(-1, -1))

        self.stbResult = wx.StaticBitmap(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VTNETFILESYNCHDIALOGSTBRESULT, name=u'stbResult',
              parent=self.pnData, pos=wx.Point(0, 34), size=wx.Size(16, 16),
              style=0)

        self.lblResult = wx.TextCtrl(id=wxID_VTNETFILESYNCHDIALOGLBLRESULT,
              name=u'lblResult', parent=self.pnData, pos=wx.Point(20, 34),
              size=wx.Size(577, 21),
              style=wx.NO_3D | wx.NO_BORDER | wx.TE_READONLY, value=u'')
        self.lblResult.Enable(False)
        self.lblResult.SetMinSize(wx.Size(-1, -1))

        self.pbFilesDnLd = wx.Gauge(id=wxID_VTNETFILESYNCHDIALOGPBFILESDNLD,
              name=u'pbFilesDnLd', parent=self.pnData, pos=wx.Point(0, 327),
              range=1000, size=wx.Size(145, 13),
              style=wx.GA_SMOOTH | wx.GA_HORIZONTAL)
        self.pbFilesDnLd.SetMinSize(wx.Size(-1, 13))
        self.pbFilesDnLd.SetMaxSize(wx.Size(-1, 13))

        self.pbFilesUpLd = wx.Gauge(id=wxID_VTNETFILESYNCHDIALOGPBFILESUPLD,
              name=u'pbFilesUpLd', parent=self.pnData, pos=wx.Point(0, 127),
              range=1000, size=wx.Size(145, 13),
              style=wx.GA_SMOOTH | wx.GA_HORIZONTAL)
        self.pbFilesUpLd.SetMinSize(wx.Size(-1, 13))
        self.pbFilesUpLd.SetMaxSize(wx.Size(-1, 13))

        self.pbProcDnLd = wx.Gauge(id=wxID_VTNETFILESYNCHDIALOGPBPROCDNLD,
              name=u'pbProcDnLd', parent=self.pnData, pos=wx.Point(149, 327),
              range=1000, size=wx.Size(447, 13),
              style=wx.GA_SMOOTH | wx.GA_HORIZONTAL)
        self.pbProcDnLd.SetMinSize(wx.Size(-1, 13))
        self.pbProcDnLd.SetMaxSize(wx.Size(-1, 13))

        self.pbProcUpLd = wx.Gauge(id=wxID_VTNETFILESYNCHDIALOGPBPROCUPLD,
              name=u'pbProcUpLd', parent=self.pnData, pos=wx.Point(149, 127),
              range=1000, size=wx.Size(447, 13),
              style=wx.GA_SMOOTH | wx.GA_HORIZONTAL)
        self.pbProcUpLd.SetMinSize(wx.Size(-1, 13))
        self.pbProcUpLd.SetMaxSize(wx.Size(-1, 13))

        self.cbOpenLocal = wx.lib.buttons.GenBitmapButton(bitmap=vtNetArt.getBitmap(vtNetArt.Local),
              id=wxID_VTNETFILESYNCHDIALOGCBOPENLOCAL, name=u'cbOpenLocal',
              parent=self.pnData, pos=wx.Point(566, 144), size=wx.Size(31, 30),
              style=0)
        self.cbOpenLocal.Bind(wx.EVT_BUTTON, self.OnCbOpenLocalButton,
              id=wxID_VTNETFILESYNCHDIALOGCBOPENLOCAL)

        self.cbOpenRemote = wx.lib.buttons.GenBitmapButton(bitmap=vtNetArt.getBitmap(vtNetArt.Remote),
              id=wxID_VTNETFILESYNCHDIALOGCBOPENREMOTE, name=u'cbOpenRemote',
              parent=self.pnData, pos=wx.Point(566, 178), size=wx.Size(31, 30),
              style=0)
        self.cbOpenRemote.Bind(wx.EVT_BUTTON, self.OnCbOpenRemoteButton,
              id=wxID_VTNETFILESYNCHDIALOGCBOPENREMOTE)

        self.cbPopup = wx.lib.buttons.GenBitmapToggleButton(bitmap=vtArt.getBitmap(vtArt.Down),
              id=wxID_VTNETFILESYNCHDIALOGCBPOPUP, name=u'cbPopup',
              parent=self.pnData, pos=wx.Point(566, 212), size=wx.Size(31, 30),
              style=0)
        self.cbPopup.Bind(wx.EVT_BUTTON, self.OnCbPopupButton,
              id=wxID_VTNETFILESYNCHDIALOGCBPOPUP)

        self.cbExec = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Build),
              id=wxID_VTNETFILESYNCHDIALOGCBEXEC, label=_(u'execute'),
              name=u'cbExec', parent=self.pnData, pos=wx.Point(176, 344),
              size=wx.Size(136, 30), style=0)
        self.cbExec.Bind(wx.EVT_BUTTON, self.OnCbExecButton,
              id=wxID_VTNETFILESYNCHDIALOGCBEXEC)

        self.cbClose = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Close),
              id=wxID_VTNETFILESYNCHDIALOGCBCLOSE, label=_(u'Close'),
              name=u'cbClose', parent=self.pnData, pos=wx.Point(344, 344),
              size=wx.Size(76, 30), style=0)
        self.cbClose.SetMinSize(wx.Size(-1, -1))
        self.cbClose.Bind(wx.EVT_BUTTON, self.OnCbCloseButton,
              id=wxID_VTNETFILESYNCHDIALOGCBCLOSE)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        self._init_ctrls(parent)
        vtXmlDomConsumer.__init__(self)
        icon = vtArt.getIcon(vtArt.getBitmap(vtArt.Synch))
        self.SetIcon(icon)
        self.nodeSettings=None
        self.cfgFunc=None
        self.dCfg={}
        self.lDN=[]
        #self.baseDN=os.path.expanduser('~')
        self.baseDN=vcCust.USR_LOCAL_DN
        self.tiSel=None
        self.dirEntrySrc1=None
        self.dirEntrySrc2=None
        self.alias=None
        self.thdRead=vtInOutReadThread(self,True)
        self.thdRead.BindEvents(self.OnInOutReadProc,
                            self.OnInOutReadFin,
                            self.OnInOutReadAbort)
        #EVT_VTINOUT_READ_THREAD_ELEMENTS(self,self.OnInOutReadProc)
        #EVT_VTINOUT_READ_THREAD_FINISHED(self,self.OnInOutReadFin)
        #EVT_VTINOUT_READ_THREAD_ABORTED(self,self.OnInOutReadAbort)
        
        self.cbConnect.SetBitmapLabel(vtNetArt.getBitmap(vtNetArt.Connect))
        self.cbConnect.SetBitmapSelected(vtNetArt.getBitmap(vtNetArt.ShutDown))
        self.stbResult.SetBitmap(vtNetArt.getBitmap(vtNetArt.Stopped))
        
        self.sockFile=None
        self.cltSock=None
        self.cltServ=None
        self.timer=None
        self.bLogin=True
        self.bLoggedIn=False
        self.qMsg=Queue.Queue()
        self.qContent=Queue.Queue()
        self.qFilesUpLd=Queue.Queue()
        self.qFilesUpLdProc=Queue.Queue()
        self.qFilesDnLd=Queue.Queue()
        self.qFilesDnLdProc=Queue.Queue()
        self.iCountUpLd=0
        self.iCountDnLd=0
        
        #self.thdSynch=vtThread(self)
        #self.thdAdd2Lst=vCheckAdd2LstThread(self)
        #self.thdComp2Lst=vCheckAddComp2LstThread(self)
        
        self.dImg={}
        self.imgLst=wx.ImageList(16,16)
        self.dImg['empty']=self.imgLst.Add(vtArt.getBitmap(vtArt.Invisible))
        self.dImg['ok']=self.imgLst.Add(vtArt.getBitmap(vtArt.Apply))
        self.dImg['err']=self.imgLst.Add(vtArt.getBitmap(vtArt.Cancel))
        self.dImg['dnld']=self.imgLst.Add(vtArt.getBitmap(vtArt.DownLoad))
        self.dImg['upld']=self.imgLst.Add(vtArt.getBitmap(vtArt.UpLoad))
        self.lstRes.SetImageList(self.imgLst,wx.IMAGE_LIST_SMALL)
        self.lstRes.SetStretchLst([(2,-1)])

        self.bxsData.Layout()
        self.origin=self.GetName()[:]
        self.cbExec.Enable(False)
        self.MAP_DIR={'dnld':_(u'download'),'upld':_(u'upload'),'empty':''}
        
        self.dImgNet={
            'conn':         vtNetArt.getBitmap(vtNetArt.Connect),
            'connFlt':      vtNetArt.getBitmap(vtNetArt.Fault),
            'connOk':       vtNetArt.getBitmap(vtNetArt.Login),
            'login':        vtNetArt.getBitmap(vtNetArt.Login),
            'loggedIn':     vtNetArt.getBitmap(vtNetArt.Running),
            'closed':       vtNetArt.getBitmap(vtNetArt.Stopped),
            'FileGetStart': vtNetArt.getBitmap(vtNetArt.Running),
            'FileGetProc':  vtNetArt.getBitmap(vtNetArt.Running),
            'FileGetFin':   vtNetArt.getBitmap(vtNetArt.Running),
            'FileGetFinTmp':vtNetArt.getBitmap(vtNetArt.Running),
            'FileGetAbort': vtNetArt.getBitmap(vtNetArt.Error),
            'FileSetStart': vtNetArt.getBitmap(vtNetArt.Running),
            'FileSetProc':  vtNetArt.getBitmap(vtNetArt.Running),
            'FileSetFin':   vtNetArt.getBitmap(vtNetArt.Running),
            'FileSetAbort': vtNetArt.getBitmap(vtNetArt.Error),
            'Served':       vtNetArt.getBitmap(vtNetArt.Synch),
            'ServedOk':     vtNetArt.getBitmap(vtNetArt.Synch),
            'ServedFlt':    vtNetArt.getBitmap(vtNetArt.ServeFlt),
            'dft':          vtNetArt.getBitmap(vtNetArt.Error),
            }
        self.dProcAct={'dnld':{},'upld':{}}
        self.dRes={}
        self.lTmp=[]
        
        self.popWin=vtInOutCompFileEntryDialog(self)
        self.bBusy=False
        self.widLogging=vtLog.GetPrintMsgWid(self)
        self.enableResWid(self.lstRes,-1,-1)
        #self.Layout()
        #parent.Bind(wx.EVT_CLOSE, self.OnFrmClose)
    def GetOrigin(self):
        return self.origin
    def __showHostAlias__(self):
        vtLog.vtLngCurWX(vtLog.INFO,'',self)
        l=[]
        try:
            node=self.doc.getChildByLst(self.doc.getRoot(),['config','vFileSrv'])
            
            for c in self.doc.getChilds(node,'host'):
                sName=self.doc.getNodeText(c,'name')
                sPort=self.doc.getNodeText(c,'port')
                sAlias=self.doc.getNodeText(c,'alias')
                s=getHostAliasStr(sName,sPort,sAlias)
                #s=sName+u':'+sPort+u'//'+sAlias
                vtLog.vtLngCurWX(vtLog.DEBUG,s,self)
                l.append(s)
        except:
            pass
        self.chcRemote.Clear()
        for s in l:
            self.chcRemote.Append(s)
        try:
            self.chcRemote.SetSelection(0)
        except:
            pass
        return l
    def Clear(self):
        vtXmlDomConsumer.Clear(self)
        self.__clrQueues__()
    def __clrQueues__(self):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        while self.qContent.empty()==False:
            self.qContent.get()
        while self.qContent.empty()==False:
            self.qContent.get()
        while self.qFilesUpLdProc.empty()==False:
            self.qFilesUpLdProc.get()
        while self.qFilesDnLdProc.empty()==False:
            self.qFilesDnLdProc.get()
        while self.qFilesUpLd.empty()==False:
            self.qFilesUpLd.get()
        while self.qFilesDnLd.empty()==False:
            self.qFilesDnLd.get()
        self.__updateBar__()
        self.iCountUpLd=0
        self.iCountDnLd=0
        #self.pbFilesUpLd.SetRange(0)
        #self.pbFilesDnLd.SetRange(0)
        self.pbProcUpLd.SetValue(0)
        self.pbProcDnLd.SetValue(0)
    def Stop(self):
        if self.IsShown():
            #self.EndModal(0)
            self.Show(False)
    def IsBusy(self):
        return self.IsShown()
    def SetDoc(self,doc,bNet=False):
        vtXmlDomConsumer.SetDoc(self,doc)
        self.netMaster=self.doc.GetNetMaster()
    def SetNode(self,node):
        try:
            vtXmlDomConsumer.SetNode(self,node)
            self.__showHostAlias__()
            self.baseDN=self.doc.GetPrjDN()
            if self.baseDN is None:
                self.baseDN=''
                if self.node is not None:
                    self.baseDN=self.doc.getNodeText(node,'projectdn')
                if len(self.baseDN)<=0:
                    #self.baseDN=os.path.expanduser('~')
                    self.baseDN=vcCust.USR_LOCAL_DN
            self.dbbDir.SetValue(self.baseDN)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnInOutReadProc(self,evt):
        self.pbProcDnLd.SetValue(evt.GetNormalized())
        return
        iSize=evt.GetCount()
        iVal=evt.GetValue()
        try:
            fVal=int(iVal/float(iSize)*1000.0)
        except:
            fVal=0
        self.pbProcDnLd.SetValue(fVal)
    def OnInOutReadFin(self,evt):
        self.pbProcDnLd.SetValue(0)
    def OnInOutReadAbort(self,evt):
        self.pbProcDnLd.SetValue(0)
    def OnFrmClose(self,evt):
        self.GetCfgData()
        evt.Skip()
    def OnSize(self, event):
        event.Skip()
        try:
            #vtLog.vtLngCurWX(vtLog.DEBUG,vtLog.pformat(self.dCfg),self)
            bMod=False
            #sz=self.GetParent().GetSize()
            sz=self.GetSize()
            self.dCfg['size']='%d,%d'%(sz[0],sz[1])
            #wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            #self.pnData.Refresh()
            #self.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnMove(self, event):
        event.Skip()
        try:
            #vtLog.vtLngCurWX(vtLog.DEBUG,vtLog.pformat(self.dCfg),self)
            #pos=self.GetParent().GetPosition()
            #self.dCfg['pos']='%d,%d'%(pos[0],pos[1])
            pass
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbReadButton(self, event):
        if event is not None:
            event.Skip()
        try:
            #if self.thdAdd2Lst.IsRunning():
            #    return
            #if self.thdComp2Lst.IsRunning():
            #    return
            if self.thdRead.IsRunning():
                return
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            bMod=False
            self.baseDN=self.doc.GetPrjDN()
            if self.baseDN is None:
                if self.node is not None:
                    self.baseDN=self.doc.getNodeText(self.node,'projectdn')
                if len(self.baseDN)<=0:
                    #self.baseDN=os.path.expanduser('~')
                    self.baseDN=vcCust.USR_LOCAL_DN
                    if self.alias is not None:
                        self.baseDN=os.path.join(self.baseDN,self.alias)
                    bMod=True
            if self.baseDN!=self.dbbDir.GetValue():
                bMod=True
            self.baseDN=self.dbbDir.GetValue()
            try:
                os.path.exists(self.baseDN)
            except:
                #self.baseDN=os.path.expanduser('~')
                self.baseDN=vcCust.USR_LOCAL_DN
                if self.alias is not None:
                    self.baseDN=os.path.join(self.baseDN,self.alias)
                self.dbbDN.SetValue(self.baseDN)
                bMod=True
            try:
                os.makedirs(self.baseDN)
            except:
                pass
            #if bMod:
            #    if self.node is not None:
            #        self.doc.setNodeText(self.node,'projectdn',self.baseDN)
            self.dirEntrySrc1=None
            self.thdRead.Read(self.baseDN,None,10,0,0,self.refreshSel)
            #self.thdRead.Do(self.baseDN,None,10,0,0,self.refreshSel)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbDelDirButton(self, event):
        event.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            iCount=self.lstRes.GetItemCount()
            for idx in xrange(iCount-1,-1,-1):
                if (self.lstRes.GetItemState(idx,wx.LIST_STATE_SELECTED)&wx.LIST_STATE_SELECTED)!=0:
                    self.lstRes.DeleteItem(idx)
        except:
            vtLog.vtLngTB(self.GetName())

    def refreshSel(self,dn,dirEntry,*args,**kwargs):
        vtLog.vtLngCurWX(vtLog.DEBUG,dn,self)
        #if self.thdAdd2Lst.IsRunning():
        #    return
        #if self.thdComp2Lst.IsRunning():
        #    return
        #self.lstRes.DeleteAllItems()
        self.dirEntrySrc1=dirEntry
        #self.thdAdd2Lst.Do(dirEntry,len(dn)+1,1,self.__add2Lst__,1,kk='ll')
        self.thdRead.DoCallBack(self.OnCbRefreshDirButton,None)
    def __add2Lst__(self,s,*args,**kwargs):
        strs=s.split(',')
        idx=self.lstRes.InsertStringItem(sys.maxint,_('local'))
        self.lstRes.SetStringItem(idx,1,'')
        self.lstRes.SetStringItem(idx,2,strs[0])
        self.lstRes.SetStringItem(idx,3,strs[2])
        self.lstRes.SetStringItem(idx,4,strs[3])
        self.lstRes.SetStringItem(idx,5,strs[4])
        self.lstRes.SetStringItem(idx,6,strs[5])
        self.lstRes.SetStringItem(idx,7,strs[6])
        
    def refreshComp(self,dn,dirCmp,*args,**kwargs):
        vtLog.vtLngCurWX(vtLog.DEBUG,dn,self)
        #if self.thdAdd2Lst.IsRunning():
        #    return
        #if self.thdComp2Lst.IsRunning():
        #    return
        #self.lstRes.DeleteAllItems()
        self.dirComp=dirCmp
        self.thdRead.DoCallBack(self.showCmp,dirCmp,0,1,None,self.__addComp2Lst__)
        #self.thdComp2Lst.Do(dirEntry,0,1,self.__addComp2Lst__)
    def __addComp2Lst__Old(self,iRes,sRes,s1,s2,*args,**kwargs):
        for src,s in [(_('local'),s1),(_('remote'),s2)]:
            #print s
            if self.chcShowEqual.GetValue()==False:
                if iRes==0x1:
                    continue
            else:
                if iRes==0x1:
                    if src==_('remote'):
                        continue
            strs=s.split(',')
            if len(strs)<6:
                continue
            sRes=sRes.replace('source 1',_('local'))
            sRes=sRes.replace('source 2',_('remote'))
            idx=self.lstRes.InsertStringItem(sys.maxint,src)
            self.lstRes.SetStringItem(idx,1,sRes)
            self.lstRes.SetStringItem(idx,2,strs[0])
            self.lstRes.SetStringItem(idx,3,strs[2])
            self.lstRes.SetStringItem(idx,4,strs[3])
            self.lstRes.SetStringItem(idx,5,strs[4])
            self.lstRes.SetStringItem(idx,6,strs[5])
            self.lstRes.SetStringItem(idx,7,strs[6])
            self.lstRes.SetItemData(idx,iKey)
    def __addComp2Lst__(self,sSug,sName,sRes,iKey,bSz,bCont,bzM,bzC):
        try:
            sAct=self.MAP_DIR[sSug]
            idx=self.lstRes.InsertImageStringItem(sys.maxint,sAct,self.dImg[sSug])
            self.lstRes.SetStringItem(idx,1,sAct,self.dImg[sSug])
            self.lstRes.SetStringItem(idx,2,sName)
            if bSz:
                self.lstRes.SetStringItem(idx,3,'X')
            if bCont:
                self.lstRes.SetStringItem(idx,4,'X')
            if bzM:
                self.lstRes.SetStringItem(idx,5,'X')
            if bzC:
                self.lstRes.SetStringItem(idx,6,'X')
            self.lstRes.SetItemData(idx,iKey)
        except:
            vtLog.vtLngTB(self.GetName())
    def showCmp(self,dirEntry,skip,style,func,*args,**kwargs):
        try:
            bEqual=self.chcShowEqual.GetValue()
            self.dRes={}
            self.iRes=0
            self._showCmp(dirEntry,skip,style,bEqual,func,*args,**kwargs)
        except:
            vtLog.vtLngTB(self.GetName())
    def _showCmp(self,dirEntry,skip,style,bEqual,func,*args,**kwargs):
        try:
            for d in dirEntry.dir_results:
                self._showCmp(d,skip,style,bEqual,func,*args,**kwargs)
            sDN=dirEntry.base
            for f in dirEntry.file_results:
                if f.isEqual():
                    if bEqual==False:
                        continue
                    bSz=False
                    bCont=False
                    bzM=False
                    bzC=False
                else:
                    bSz=True
                    bCont=True
                    bzM=True
                    bzC=True
                    
                sName=f.getFullFN(sDN,skip)
                # filter backup files (xxx.y.z) where z is a number
                i=sName.rfind('.')
                if i>0:
                    r=re.match('.\d+',sName[i:])
                    if r is not None:
                        if r.end()==(len(sName)-i):
                            continue
                if f.hasSrc1():
                    if f.hasSrc2():
                        sSug='empty'
                        bSz=f.isDiffSize()
                        bCont=f.isDiffContent()
                        bzM=f.isDiffModify()
                        bzC=f.isDiffCreate()
                        iM=f.getDiffModify()
                        if iM<0:
                            sSug='upld'
                        elif iM>0:
                            sSug='dnld'
                    else:
                        sSug='upld'
                else:
                    if f.hasSrc2():
                        sSug='dnld'
                self.dRes[self.iRes]=f
                sName=f.getFullFN(sDN,skip)
                self.thdRead.CallBack(self.__addComp2Lst__,sSug,sName,
                                '',self.iRes,bSz,bCont,bzM,bzC)
                self.iRes+=1
        except:
            vtLog.vtLngTB(self.GetName())
    def __getSelFromLst__(self,iDir,bAll=False):
        try:
            #while self.qFiles.empty()==False:
            #    self.qFiles.get()
            if iDir==1:
                qFiles=self.qFilesDnLd
                #src=_('remote')
                src=self.MAP_DIR['dnld']
            elif iDir==2:
                qFiles=self.qFilesUpLd
                #src=_('local')
                src=self.MAP_DIR['upld']
            else:
                return
            iCount=self.lstRes.GetItemCount()
            #iAct=self.pbFiles.GetRange()
            for idx in xrange(iCount):
                if bAll or self.lstRes.GetItemState(idx,wx.LIST_STATE_SELECTED)!=0:
                    if self.lstRes.GetItem(idx,0).m_text==src:
                        sFN=self.lstRes.GetItem(idx,2).m_text
                        iNum=self.lstRes.GetItemData(idx)
                        qFiles.put((iDir,sFN,idx,iNum))
                        if iDir==1:
                            self.iCountDnLd+=1
                        elif iDir==2:
                            self.iCountUpLd+=1
        except:
            vtLog.vtLngTB(self.GetName())
    def __setResult__(self,iDir,sFN,sRes,sResImg):
        #vtLog.vtLngCurWX(vtLog.DEBUG,'fn:%s:res:%s,%s'%(sFN,sRes,sResImg),self)
        if iDir==1:
            #src=_('remote')
            src=self.MAP_DIR['dnld']
        elif iDir==2:
            #src=_('local')
            src=self.MAP_DIR['upld']
        iCount=self.lstRes.GetItemCount()
        for idx in xrange(iCount):
            if self.lstRes.GetItem(idx,2).m_text==sFN:
                if self.lstRes.GetItem(idx,0).m_text==src:
                    try:
                        self.lstRes.SetStringItem(idx,0,sRes,self.dImg[sResImg])
                        #self.lstRes.EnsureVisible(idx)
                    except:
                        pass
                    return
    def __setResultIdx__(self,idx,sRes,sResImg):
        try:
            self.lstRes.SetStringItem(idx,0,sRes,self.dImg[sResImg])
            #self.lstRes.EnsureVisible(idx)
        except:
            pass
    def enableResWid(self,lst,idx,iNum):
        try:
            self.iSelIdx=idx
            if iNum in self.dRes:
                f=self.dRes[iNum]
                self.iSelNum=iNum
            else:
                f=None
                self.iSelNum=-1
            if f is None:
                self.cbOpenLocal.Enable(False)
                self.cbOpenRemote.Enable(False)
                self.popWin.SetSrc1(None,'','')
                self.popWin.SetSrc2(None,'','')
            else:
                s=lst.GetItem(idx,2).m_text
                sRelDN,sTmp=os.path.split(s)
                if f.hasSrc1():
                    self.cbOpenLocal.Enable(True)
                    self.popWin.SetSrc1(f.file1,self.dirEntrySrc1.base,sRelDN)
                else:
                    self.cbOpenLocal.Enable(False)
                    self.popWin.SetSrc1(None,'','')
                if f.hasSrc2() and self.cltSock is not None:
                    if self.dirEntrySrc1.base==self.dirEntrySrc2.base:
                        self.cbOpenRemote.Enable(False)
                        self.popWin.SetSrc2(None,'','')
                    else:
                        self.cbOpenRemote.Enable(True)
                    self.popWin.SetSrc2(f.file2,self.dirEntrySrc2.base,sRelDN)
                else:
                    self.cbOpenRemote.Enable(False)
                    self.popWin.SetSrc2(None,'','')
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbDownloadButton(self, event):
        event.Skip()
        try:
            iCount=self.lstRes.GetItemCount()
            sAct=self.MAP_DIR['dnld']
            imgIdx=self.dImg['dnld']
            for idx in xrange(iCount):
                if (self.lstRes.GetItemState(idx,wx.LIST_STATE_SELECTED)&wx.LIST_STATE_SELECTED)!=0:
                    #self.lstRes.DeleteItem(idx)
                    self.lstRes.SetStringItem(idx,0,sAct,imgIdx)
                    self.lstRes.SetItemState(idx,0,wx.LIST_STATE_SELECTED)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbUpLoadButton(self, event):
        event.Skip()
        try:
            iCount=self.lstRes.GetItemCount()
            sAct=self.MAP_DIR['upld']
            imgIdx=self.dImg['upld']
            for idx in xrange(iCount):
                if (self.lstRes.GetItemState(idx,wx.LIST_STATE_SELECTED)&wx.LIST_STATE_SELECTED)!=0:
                    #self.lstRes.DeleteItem(idx)
                    self.lstRes.SetStringItem(idx,0,sAct,imgIdx)
                    self.lstRes.SetItemState(idx,0,wx.LIST_STATE_SELECTED)
            return
            self.__getSelFromLst__(2)
            self.lblResult.SetLabel(_('start upload ...'))
            if self.qFilesUpLdProc.empty():
                self.qMsg.put(('FileSetFin',None,'start'))
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbExecButton(self, event):
        event.Skip()
        try:
            self.dProcAct={'dnld':{},'upld':{}}
            bAll=self.lstRes.GetSelectedItemCount()==0
            #self.lstRes.Enable(False)
            self.cbExec.Enable(False)
            self.__getSelFromLst__(1,bAll=bAll)
            self.lblResult.SetLabel(_('start download ...'))
            if self.qFilesDnLdProc.empty():
                self.qMsg.put(('FileGetFin',None,'start'))
            
            self.__getSelFromLst__(2,bAll=bAll)
            self.lblResult.SetLabel(_('start upload ...'))
            if self.qFilesUpLdProc.empty():
                self.qMsg.put(('FileSetFin',None,'start'))
            self.thdRead.CallBack(self.__ProcessQueue__)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbRefreshDirButton(self, event):
        if event is not None:
            event.Skip()
        try:
            self.lstRes.DeleteAllItems()
            if self.dirEntrySrc1 is None:
                return
            if self.dirEntrySrc2 is None:
                return
            self.dirComp=vtInOutCompDirEntry()
            self.dirComp.compareHier(self.dirEntrySrc1,self.dirEntrySrc2,0x06)
            #self.dirComp.show()
            self.refreshComp(self.dirEntrySrc1.base,self.dirComp)
            #self.thdComp2Lst.Do(self.dirComp,0,1,self.__addComp2Lst__)
        except:
            vtLog.vtLngTB(self.GetName())
    def __openFile__(self,sDN,sFN):
        sExts=sFN.split('.')
        sExt=sExts[-1]
        fileType = wx.TheMimeTypesManager.GetFileTypeFromExtension(sExt)
        filename=os.path.join(sDN,sFN)
        try:
            mime = fileType.GetMimeType()
            if sys.platform=='win32':
                filename=filename.replace('/','\\')
            cmd = fileType.GetOpenCommand(filename, mime)
            wx.Execute(cmd)
            vtLog.PrintMsg(_(u'opening %s ...')%filename,self.widLogging)
            try:
                vtLog.vtLngCurWX(vtLog.DEBUG,'cmd:%s'%(cmd),self)
            except:
                pass
        except:
            sMsg=_(u"Cann't open %s!")%(filename)
            dlg=vtmMsgDialog(self,sMsg ,
                                u'vtFileSynchDialog',
                                wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
    def OnCbOpenLocalButton(self, event):
        event.Skip()
        try:
            if self.iSelNum in self.dRes:
                f=self.dRes[self.iSelNum]
                if f.hasSrc1()==False:
                    return
                s=self.lstRes.GetItem(self.iSelIdx,2).m_text
                sRelDN,sTmp=os.path.split(s)
                sFN=f.file1.name
                sDN=os.path.join(self.dirEntrySrc1.base,sRelDN)
                self.__openFile__(sDN,sFN)
            else:
                f=None
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbOpenRemoteButton(self, event):
        event.Skip()
        try:
            if self.iSelNum in self.dRes:
                f=self.dRes[self.iSelNum]
                if f.hasSrc2()==False:
                    return
                sFN=self.lstRes.GetItem(self.iSelIdx,2).m_text
                #sRelDN,sTmp=os.path.split(s)
                #sFN=f.file1.name
                #sDN=os.path.join(self.dirEntrySrc1.base,sRelDN)
                try:
                    self.cltSock.GetFile(self.sApplAlias,self.baseDN,sFN,bTmp=True)
                except:
                    vtLog.vtLngTB(self.GetName())
                    sMsg=u"Cann't download\n%s\nas temporary file!"%(sFN)
                    dlg=vtmMsgDialog(self,sMsg ,
                                        u'vtFileSynchDialog',
                                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                    dlg.ShowModal()
                    dlg.Destroy()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbPopupButton(self, event):
        event.Skip()
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.cbPopup.GetValue()==True:
            btn=self.cbPopup
            iX,iY = btn.ClientToScreen( (0,0) )
            iW,iH =  btn.GetSize()
            iPopW,iPopH=self.popWin.GetSize()
            iScreenW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            iScreenH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            if iX+iPopW>iScreenW:
                iX=iScreenW-iPopW-4
                if iX<0:
                    iX=0
            iY+=iH
            if iY+iPopH>iScreenH:
                iY=iScreenH-iPopH
                if iY<0:
                    iY=0
            
            self.popWin.Move((iX,iY))
            self.bBusy=True
            #self.popWin.Move((pos[0],pos[1]+sz[1]))
            self.popWin.Show(True)
        else:
            self.popWin.Show(False)
            self.bBusy=False
    def OnLstResListItemRightClick(self, event):
        event.Skip()

    def OnLstResListItemSelected(self, event):
        event.Skip()
        try:
            idx=event.GetIndex()
            iNum=event.GetData()
            self.enableResWid(self.lstRes,idx,iNum)
        except:
            vtLog.vtLngTB(self.GetName())
    def __connect__(self):
        try:
            sConn=self.chcRemote.GetStringSelection()
            tup=setHostAliasStr(sConn)
            self.host,self.port,self.alias=tup[0],tup[1],tup[2]
            self.appl=self.doc.GetAppl()
            sAliasDoc=self.doc.GetAlias()
            self.sApplAlias=':'.join([self.appl,self.alias])
            self.sApplAliasRef=':'.join([self.appl,sAliasDoc])
            self.usr=self.doc.GetUsr()
            self.passwd=self.doc.GetPasswd()
            vtLog.vtLngCurWX(vtLog.INFO,'host:%s;port:%s;alias:%s;appl:%s;aliasDoc:%s'%\
                        (self.host,self.port,self.alias,self.appl,sAliasDoc),self)
            vtLog.vtLngCurWX(vtLog.DEBUG,'usr:%s;passwd:%s;'%\
                        (self.usr,self.passwd),self)
            self.cbClose.Enable(False)
            self.cbExec.Enable(True)
            self.bLogin=True
            self.bLoginFirst=True
            self.bLoggedIn=False
            self.bBrowse=False
            self.dRemoteLocaleFN={}
            self.cltServ=vtNetClt(self,self.host,int(self.port),vtNetFileCltSock)
            #self.timer = wx.PyTimer(self.__CheckQueue__)
            #self.timer.Start(50)
            self.cltServ.Connect()
        except:
            traceback.print_exc()
    def __connectionClosed__(self,bEstablished=False):
        if self.cltSock is not None:
            del self.cltSock
        self.cltSock=None
        if self.cltServ is not None:
            del self.cltServ
        self.cltServ=None
        self.qMsg.put('closed')
        self.thdRead.CallBack(self.__ProcessQueue__)
    def __close__(self):
        if self.cltSock is not None:
            self.cltSock.Close()
        else:
            self.bActive=False
        if self.cltServ is not None:
            self.cltServ.Stop()
        else:
            self.bActive=False
        self.bLogin=False
    def __CheckQueue__(self):
        while self.__ProcessQueue__():
            pass
        #self.__ProcessQueue__()
    def __checkGet2Proc__(self):
        if self.qFilesDnLd.empty()==False:
            t=self.qFilesDnLd.get()
            sFN=t[1]
            self.qFilesDnLdProc.put(t)
            self.dProcAct['dnld'][sFN]=t
            self.cltSock.GetFile(self.sApplAlias,self.baseDN,sFN)
        else:
            self.lblResult.SetLabel(_('all files downloaded'))
            self.iCountDnLd=0
            #self.pbFiles.SetValue(0)
            while self.qFilesDnLdProc.empty()==False:
                self.qFilesDnLdProc.get()
        self.__updateBar__()
        self.thdRead.CallBack(self.__ProcessQueue__)
    def __checkSet2Proc__(self):
        if self.qFilesUpLd.empty()==False:
            t=self.qFilesUpLd.get()
            sFN=t[1]
            zTimes=None
            try:
                if t[3] in self.dRes:
                    f1=self.dRes[t[3]].file1
                    zTimes=[f1.stat_access,f1.last_access,f1.mod_access]
            except:
                vtLog.vtLngTB(self.GetName())
                zTimes=None
            self.qFilesUpLdProc.put(t)
            self.dProcAct['upld'][sFN]=t
            self.cltSock.SetFile(self.sApplAlias,self.baseDN,sFN,
                    zTimes)
        else:
            self.lblResult.SetLabel(_('all files uploaded'))
            self.iCountUpLd=0
            #self.pbFiles.SetValue(0)
            while self.qFilesUpLdProc.empty()==False:
                self.qFilesUpLdProc.get()
        self.__updateBar__()
        self.thdRead.CallBack(self.__ProcessQueue__)
        if 0:
                if self.qFilesUpLd.empty()==False:
                    t=self.qFilesUpLd.get()
                    sFN=t[1]
                    self.cltSock.SetFile(self.sApplAlias,self.baseDN,sFN)
                    self.qFilesUpLdProc.put(t)
                    self.__updateBar__()
    def __check2Close__(self):
        # auto close connection and dialog
        if self.qFilesDnLd.empty() and self.qFilesUpLd.empty():
            self.lblResult.SetLabel(_('all files downloaded/uploaded'))
            self.cbConnect.SetValue(False)
            self.__close__()
    def __ProcessQueue__(self):
        try:
            sMsg=self.qMsg.get(False)
            #vtLog.vtLngCurWX(vtLog.INFO,'msg:%s'%(vtLog.pformat(sMsg)),self)
            if type(sMsg)==types.TupleType:
                tMsg=sMsg
                sMsg=tMsg[0]
            #vtLog.vtLngCurWX(vtLog.INFO,'msg:%s'%(sMsg),self)
            if sMsg in self.dImgNet:
                img=self.dImgNet[sMsg]
            else:
                img=self.dImgNet['dft']
            if sMsg in ['FileGetAbort','FileGetFin','FileSetAbort','FileSetFin']:
                if self.qFilesDnLd.empty() and self.qFilesUpLd.empty():
                    #self.lstRes.Enable(True)
                    self.cbExec.Enable(True)
            
            if sMsg=='conn':
                self.lblResult.SetLabel(_('connecting'))
            elif sMsg=='connFlt':
                self.lblResult.SetLabel(_('connection fault'))
                self.cbConnect.SetValue(False)
            elif sMsg=='connOk':
                self.lblResult.SetLabel(_('connection established'))
                self.cltSock.Alias(self.appl,self.alias)
            elif sMsg=='login':
                self.lblResult.SetLabel(_('login'))
                self.Login()
                if 0:
                    if self.bLogin:
                        self.bLogin=False
                        try:
                            self.cltSock.__doLogin__()
                        except:
                            self.__close__()
                            pass
                    else:
                        self.bLogin=False
                        dlg=netMaster.GetLoginDlg()
                        
                        self.__close__
            elif sMsg=='loggedIn':
                self.lblResult.SetLabel(_('get remote content'))
                self.bCheckOk=True
                self.cltSock.ListContent(self.sApplAlias)
            elif sMsg=='closed':
                self.lblResult.SetLabel(_('connection closed'))
                self.bActive=False
                #if self.timer is not None:
                #    self.timer.Stop()
                #    self.timer=None
                self.dirEntrySrc1=None
                self.dirEntrySrc2=None
                self.dirComp=None
                self.__clrQueues__()
                if 0:
                    while self.qMsg.empty()==False:
                        self.qMsg.get()
                    while self.qFilesProc.empty()==False:
                        self.qFilesProc.get()
                    while self.qFilesDnLd.empty()==False:
                        self.qFilesDnLd.get()
                    while self.qFilesUpLd.empty()==False:
                        self.qFilesUpLd.get()
                    self.pbFiles.SetValue(0)
                    self.pbFiles.SetRange(0)
                    self.pbProc.SetValue(0)
                    self.pbProc.SetRange(0)
                #self.cbConnect.SetValue(False)
                self.cbConnect.SetToggle(False)
                self.cbClose.Enable(True)
                self.cbExec.Enable(False)
                self.cbOpenRemote.Enable(False)
                self.popWin.SetSrc2(None,'','')
                #self.lstRes.Enable(True)
                self.lRemoteFN=[]
                self.dRemoteLocaleFN={}
            elif sMsg=='FileGetStart':
                self.lblResult.SetLabel(_('receiving file:')+tMsg[1])
                self.__updateBar__()
            elif sMsg=='FileGetProc':
                self.pbProcDnLd.SetValue(self.__convVal2BarPromil__(tMsg[1],tMsg[2]))
                self.__updateBar__()
            elif sMsg=='FileGetFin':
                self.pbProcDnLd.SetValue(0)
                self.__checkGet2Proc__()
                if tMsg[1] is not None:
                    if len(tMsg)==3:
                        self.__setResult__(1,tMsg[1],tMsg[2],'ok')
                    else:
                        self.__setResultIdx__(tMsg[3],tMsg[2],'ok')
            elif sMsg=='FileGetFinTmp':
                sFN=tMsg[1]
                sTmpFN=tMsg[2]
                #f=tMsg[2]
                sExts=sFN.split('.')
                sExt=sExts[-1]
                fileType = wx.TheMimeTypesManager.GetFileTypeFromExtension(sExt)
                #filename=f.name#os.path.join(sDN,sFN)
                filename=sTmpFN
                #self.lTmp.append((sFN,f))
                #self.lTmp.append((sFN,sTmpFN))
                try:
                    mime = fileType.GetMimeType()
                    if sys.platform=='win32':
                        filename=filename.replace('/','\\')
                    cmd = fileType.GetOpenCommand(filename, mime)
                    p=vtProcess(cmd,shell=False)
                    self.lTmp.append((sFN,sTmpFN,p))
                    #wx.Execute(cmd)
                    vtLog.PrintMsg(_(u'opening %s ...')%filename,self.widLogging)
                    try:
                        vtLog.vtLngCurWX(vtLog.DEBUG,'cmd:%s'%(cmd),self)
                    except:
                        pass
                except:
                    sMsg=u'\n'.join([_(u"Cann't open temporary file!"),
                                u'(%s)'%(filename),'',
                                _(u"Shall it be deleted now?")])
                    dlg=vtmMsgDialog(self,sMsg ,
                                        u'vtFileSynchDialog',
                                        wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                    if dlg.ShowModal()==wx.ID_YES:
                        dlg.Destroy()
                        try:
                            os.remove(filename)
                        except:
                            vtLog.vtLngTB(self.GetName())
                            sMsg=u'\n'.join([_(u"Cann't delete temporary file!"),
                                        u'(%s)'%(filename),'',
                                        _(u"Please delete it yourself.")])
                            dlg=vtmMsgDialog(self,sMsg ,
                                                u'vtFileSynchDialog',
                                                wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                            dlg.ShowModal()
                            dlg.Destroy()
                    else:
                        dlg.Destroy()
                self.__updateBar__()
                self.pbProcDnLd.SetValue(0)
            elif sMsg=='FileGetAbort':
                self.pbProcDnLd.SetValue(0)
                #t=self.qFilesDnLd.get()
                self.lblResult.SetLabel(_('receive aborted file:')+tMsg[1])
                self.__checkGet2Proc__()
                if len(tMsg)==3:
                    self.__setResult__(1,tMsg[1],tMsg[2],'err')
                else:
                    self.__setResultIdx__(tMsg[3],tMsg[2],'err')
                if 0:
                    if self.qFilesDnLd.empty()==False:
                        t=self.qFilesDnLd.get()
                        sFN=t[1]
                        self.cltSock.GetFile(self.sApplAlias,self.baseDN,sFN)
                        self.qFilesProcDnLd.put(t)
                        self.__updateBar__()
            elif sMsg=='FileSetStart':
                self.lblResult.SetLabel(_('sending file:')+tMsg[1])
                self.__updateBar__()
            elif sMsg=='FileSetProc':
                self.pbProcUpLd.SetValue(self.__convVal2BarPromil__(tMsg[1],tMsg[2]))
                self.__updateBar__()
            elif sMsg=='FileSetFin':
                self.pbProcUpLd.SetValue(0)
                self.__checkSet2Proc__()
                if tMsg[1] is not None:
                    #self.__setResult__(2,tMsg[1],tMsg[2],'ok')
                    self.__setResultIdx__(tMsg[3],tMsg[2],'ok')
            elif sMsg=='FileSetAbort':
                self.pbProcUpLd.SetValue(0)
                self.lblResult.SetLabel(_('sending aborted file:')+tMsg[1])
                self.__checkSet2Proc__()
                #self.__setResult__(2,tMsg[1],tMsg[2],'err')
                self.__setResultIdx__(tMsg[3],tMsg[2],'err')
            elif sMsg=='Served':
                if tMsg[1]==False:
                    img=self.dImgNet['ServedFlt']
                    self.lblResult.SetLabel(_('not served'))
                else:
                    self.lblResult.SetLabel(_('served:')+str(tMsg[1]))
            else:
                self.lblResult.SetLabel(_('unknown')+' '+repr(sMsg))
            #if self.bCheckOk:
            #    img=imgNet.getApplyBitmap()
            self.stbResult.SetBitmap(img)
            self.stbResult.Refresh()
            return True
        except Queue.Empty:
            return False
        except:
            traceback.print_exc()
            return False
    def NotifyConnect(self):
        self.qMsg.put('conn')
        self.thdRead.CallBack(self.__ProcessQueue__)
    def NotifyConnectionFault(self):
        self.qMsg.put('connFlt')
        self.thdRead.CallBack(self.__ProcessQueue__)
    def NotifyConnected(self):
        try:
            self.cltSock=self.cltServ.GetSocketInstance()
            usr=self.usr
            passwd=self.passwd
            if len(passwd)>0:
                m=sha.new()
                m.update(passwd)
                passwd=m.hexdigest()
            else:
                passwd=''
            self.cltSock.SetParent(self)
            self.cltSock.LoginEncoded(usr,passwd)
            #self.cltSock.Alias(self.VIDARC_ALIAS)
        except:
            traceback.print_exc()
        self.qMsg.put('connOk')
        self.thdRead.CallBack(self.__ProcessQueue__)
    def NotifyLogin(self):
        self.qMsg.put('login')
        self.thdRead.CallBack(self.__ProcessQueue__)
    def Login(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCurWX(vtLog.INFO,'',self.appl)
        dlg=None
        #self.netMaster.AcquireLogin()
        bInteractive=True
        if self.bLoginFirst:
            self.bLoginFirst=False
            sPasswd=self.netMaster.CheckLogin(self.appl,self.host,self.port,
                                self.alias,self.usr)
            if sPasswd is not None:
                bInteractive=False
                self.passwd=sPasswd
        if bInteractive:
            dlg=self.netMaster.GetLoginDlg()
            if dlg.IsShown():
                return
            dlg.Centre()
            dlg.SetValues(self.appl,self.host,int(self.port),self.alias,self.usr)
            if dlg.ShowModal()>0:
                self.usr=dlg.GetUsr()
                self.passwd=dlg.GetPasswd()
                self.netMaster.UpdateLogin(self.appl,self.usr,self.passwd)
                self.netMaster.SetLogin(self.appl,self.host,self.port,
                                self.alias,self.usr,self.passwd)
                pass
            else:
                return
        #self.netMaster.ReleaseLogin()
        if self.cltSock is not None:
            self.cltSock.LoginEncoded(self.usr,self.passwd)
            self.cltSock.__doLogin__()
    def NotifyLoggedIn(self,flag):
        self.bLoggedIn=flag
        self.qMsg.put('loggedIn')
        self.thdRead.CallBack(self.__ProcessQueue__)
    def NotifyFileGetStart(self,sFN):
        self.qMsg.put(('FileGetStart',sFN))
        self.thdRead.CallBack(self.__ProcessQueue__)
    def NotifyFileGetProc(self,sFN,iAct,iSize):
        self.qMsg.put(('FileGetProc',iAct,iSize))
        self.thdRead.CallBack(self.__ProcessQueue__)
    def NotifyFileGetFin(self,sFN,sMsg):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'fn:%s;dnld:%s'%(sFN,
                        vtLog.pformat(self.dProcAct['dnld'])),self)
        if sFN in self.dProcAct['dnld']:
            t=self.dProcAct['dnld'][sFN]
            del self.dProcAct['dnld'][sFN]
            self.qMsg.put(('FileGetFin',sFN,sMsg,t[2]))
            sFullFN=os.path.join(self.dirEntrySrc1.base,sFN)
            try:
                f=self.dRes[t[3]]
                f2=f.file2
                vtLog.vtLngCurWX(vtLog.INFO,'tA:%f;tLA:%f;tM:%f'%(f2.stat_access,
                        f2.last_access,f2.mod_access),self)
                setFileTimes(sFullFN,f2.stat_access,f2.last_access,f2.mod_access)
            except:
                vtLog.vtLngTB(self.GetName())
        else:
            self.qMsg.put(('FileGetFin',sFN,sMsg))
        self.thdRead.CallBack(self.__ProcessQueue__)
    def NotifyFileGetFinTmp(self,sFN,f):
        self.qMsg.put(('FileGetFinTmp',sFN,f))
        self.thdRead.CallBack(self.__ProcessQueue__)
    def NotifyFileGetAbort(self,sFN,sMsg):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'fn:%s;dnld:%s'%(sFN,
                        vtLog.pformat(self.dProcAct['dnld'])),self)
        if sFN in self.dProcAct['dnld']:
            t=self.dProcAct['dnld'][sFN]
            del self.dProcAct['dnld'][sFN]
            self.qMsg.put(('FileGetAbort',sFN,sMsg,t[2]))
        else:
            self.qMsg.put(('FileGetAbort',sFN,sMsg))
        vtLog.vtLngCurWX(vtLog.ERROR,'FileGetAbort fn:%s\n'%sFN,self)
        self.thdRead.CallBack(self.__ProcessQueue__)
    def NotifyFileSetProc(self,sFN,iAct,iSize):
        self.qMsg.put(('FileSetProc',iAct,iSize))
        self.thdRead.CallBack(self.__ProcessQueue__)
    def NotifyFileSetFin(self,sFN,sMsg):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'fn:%s;upld:%s'%(sFN,
                        vtLog.pformat(self.dProcAct['upld'])),self)
        if sFN in self.dProcAct['upld']:
            t=self.dProcAct['upld'][sFN]
            del self.dProcAct['upld'][sFN]
            self.qMsg.put(('FileSetFin',sFN,sMsg,t[2]))
        else:
            self.qMsg.put(('FileSetFin',sFN,sMsg))
        self.thdRead.CallBack(self.__ProcessQueue__)
    def NotifyFileSetAbort(self,sFN,sMsg):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'fn:%s;upld:%s'%(sFN,
                        vtLog.pformat(self.dProcAct['upld'])),self)
        if sFN in self.dProcAct['upld']:
            t=self.dProcAct['upld'][sFN]
            del self.dProcAct['upld'][sFN]
            self.qMsg.put(('FileSetAbort',sFN,sMsg,t[2]))
        else:
            self.qMsg.put(('FileSetAbort',sFN,sMsg))
        vtLog.vtLngCurWX(vtLog.ERROR,'FileSetAbort fn:%s\n'%sFN,self)
        self.thdRead.CallBack(self.__ProcessQueue__)
    def NotifyBrowse(self,l):
        if self.bBrowse==False:
            self.bBrowse=True
            self.qMsg.put('Browse')
        for sIt in l:
            #self.qBrowse.put(sIt)
            self.cltSock.ListContent(sIt)
        self.thdRead.CallBack(self.__ProcessQueue__)
    def __logRemoteEntry__(self,dEntry):
        vtLog.vtLngCurWX(vtLog.DEBUG,'base:%s;files:%s'%(dEntry.base,vtLog.pformat(dEntry.files)),self)
        for d in dEntry.dirs:
            self.__logRemoteEntry__(d)
    def __addRemoteContent__(self,l,dirEntry):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        try:
            for s in l:
                if len(s)==0:
                    continue
                strs=s.split(',')
                #if vtLog.vtLngIsLogged(vtLog.DEBUG):
                #    vtLog.vtLngCurWX(vtLog.DEBUG,'strs:%s'%(vtLog.pformat(strs)),self)
                if len(strs)<6:
                    continue
                dirEntry.AddEntryByLst(strs[0],strs[1:])
            #entries=[s.split(',') for s in l]
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                self.__logRemoteEntry__(dirEntry)
            #self.refreshSel('.',dirEntry)
            #return
            self.dirEntrySrc2=dirEntry
            self.OnCbRefreshDirButton(None)
            return
            self.dirComp=vtInOutCompDirEntry()
            self.dirComp.compareHier(self.dirEntrySrc1,self.dirEntrySrc2,0x07)
            self.dirComp.show()
            #self.refreshComp(self.dirEntrySrc1.base,self.dirComp)
            self.lstRes.DeleteAllItems()
            self.thdComp2Lst.Do(self.dirComp,0,1,self.__addComp2Lst__)
        except:
            vtLog.vtLngTB(self.GetName())
        
    def NotifyListContent(self,applAlias,l):
        vtLog.vtLngCurWX(vtLog.INFO,'alias:%s'%(applAlias),self)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'l:%s'%(vtLog.pformat(l)),self)
        try:
            dirEntry=DirEntryXml()
            dirEntry.base='.'
            self.thdRead.Do(self.__addRemoteContent__,l,dirEntry)
        except:
            vtLog.vtLngTB(self.GetName())
        return
        self.qMsg.put('ListContent')
        self.thdRead.CallBack(self.__ProcessQueue__)
    def NotifyServed(self,served):
        self.qMsg.put(('Served',served))
        self.thdRead.CallBack(self.__ProcessQueue__)
    def OnCbConnectButton(self, event):
        event.Skip()
        if self.cbConnect.GetValue():
            self.__connect__()
        else:
            self.__close__()
        if self.dirEntrySrc1 is None:
            self.OnCbReadButton(None)
    def __convVal2BarPromil__(self,val,count):
        if count==0:
            return 0
        try:
            iPro=int(val/float(count)*1000.0)
        except:
            iPro=0
        return iPro
    def __updateBar__(self):
        self.pbFilesUpLd.SetValue(self.__convVal2BarPromil__(self.qFilesUpLdProc.qsize(),self.iCountUpLd))
        self.pbFilesDnLd.SetValue(self.__convVal2BarPromil__(self.qFilesDnLdProc.qsize(),self.iCountDnLd))
        #self.pbFilesUpLd.SetRange(self.qFilesUpLd.qsize())
        #self.pbFilesDnLd.SetRange(self.qFilesDnLd.qsize())

    def OnCbCloseButton(self, event):
        if len(self.lTmp)>0:
            bRunning=False
            lRunning=[]
            for t in self.lTmp:
                try:
                    p=t[2]
                    for i in xrange(100):
                        if p.communicate()==False:
                            break
                    if t[2].checkRunning():
                        bRunning=True
                        lRunning.append(t)
                    else:
                        try:
                            os.remove(t[1])
                        except:
                            sMsg=u'\n'.join([_(u"Cann't open temporary file!"),
                                        u'(%s)'%(t[1]),'',
                                        _(u"Please close launched application for this file.")])
                            dlg=vtmMsgDialog(self,sMsg ,
                                                u'vtFileSynchDialog',
                                                wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                            dlg.ShowModal()
                            dlg.Destroy()
                            bRunning=True
                            lRunning.append(t)
                            vtLog.vtLngTB(self.GetName())
                except:
                    vtLog.vtLngTB(self.GetName())
            self.lTmp=lRunning
        iLen=len(self.lTmp)
        if iLen>0:
            if iLen==1:
                sMsg=_(u"Cann't close dialog due opened temporary file!")
            else:
                sMsg=_(u"Cann't close dialog due %d opened temporary files!")%(iLen)
            dlg=vtmMsgDialog(self,sMsg ,
                                u'vtFileSynchDialog',
                                wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            return
        event.Skip()
        self.Show(False)
        if self.bBusy:
            self.popWin.Show(False)
            self.cbPopup.SetValue(False)


_('upld'),_('local'),_('dnld'),_('remote')
