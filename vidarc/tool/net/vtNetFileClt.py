#----------------------------------------------------------------------------
# Name:         vtNetFileClt.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060627
# CVS-ID:       $Id: vtNetFileClt.py,v 1.13 2008/05/10 09:30:56 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import thread,traceback
import time,random
import socket,sha,binascii
import tempfile

import sys,os,os.path,stat
import vidarc.tool.InOut.fnUtil as fnUtil
from vidarc.tool.log import vtLog
#from vidarc.tool.net import vtNetSock
#from vidarc.tool.net.vtNetSock import *
from vidarc.tool.sock.vtSock import vtSock
from vidarc.tool.sec.vtSecXmlLogin import vtSecXmlLogin
from vidarc.tool.vtThread import vtThread

BUFFER_MAX=40*1024*1024

class vtNetFileCltSock(vtLog.vtLogMixIn,vtSock):
    def __initCtrl__(self,*args,**kwargs):
        self.bEstabilshed=False
        self.loggedin=False
        #self.bLoginSend=False
        self.oLogin=vtSecXmlLogin()
        self.iStart=-1
        self.iAct=0
        self.iEnd=-1
        self.encoded_passwd=False
        self.lCmds=[
            #('lock_node',self.__lock_node__),
            #('unlock_node',self.__unlock_node__),
            #('getFile',self.__getFile__),
            ('getFileStart',self.__getFileStart__),
            ('getFileProc',self.__getFileProc__),
            ('getFileFin',self.__getFileFin__),
            ('getFileAbort',self.__getFileAbort__),
            ('setFileStart',self.__setFileStart__),
            ('setFileProc',self.__setFileProc__),
            ('setFileFin',self.__setFileFin__),
            ('setFileAbort',self.__setFileAbort__),
            ('listContent',self.__listContent__),
            ('alias',self.__alias__),
            ('login',self.__login__),
            ('loggedin',self.__loggedin__),
            ('authenticate',self.__authenticate__),
            ('browse',self.__browse__),
            (u'sessionID',self.__sessionID__),
            (u'shutdown',self.__shutdown__)]
        self.par=None
        self.appl_alias=''
        self.appl=kwargs.get('appl','')
        self.widLogging=None
        self.SetParent(kwargs.get('sockpar',None))
        self.thdSend=vtThread(None)
        self.dFileGet={}
        self.dFileSet={}
        vtSock.__initCtrl__(self,*args,**kwargs)
    def __logStack__(self):
        vtLog.vtLngCur(vtLog.DEBUG,''%(),self.GetOrigin(),level2skip=2)
    def CheckPause(self):
        self._acquire()
        try:
            if self.paused!=self.pausing:
                self.paused=self.pausing
                if self.par is not None:
                    vtLog.vtLngCS(vtLog.INFO,
                                'pause=%s'%self.paused,origin=self.appl)
                    self.par.NotifyPause(self.paused)
                else:
                    vtLog.vtLngCS(vtLog.INFO,
                            'pause=%s'%self.paused,origin=self.appl)
        except:
            vtLog.vtLngTB(self.appl)
            vtLog.vtLngCS(vtLog.ERROR,
                        traceback.format_exc(),origin=self.appl)
        self._release()
    def IsLoggedIn(self):
        return self.loggedin
    def HandleDataOld(self):
        try:
            #vtLog.vtLngCallStack(self,vtLog.DEBUG,'HandleData')
            if self.verbose>0:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCallStack(self,vtLog.DEBUG,
                                'HandleData; len:%8d %8d; act:%8d;start:%8d; end:%8d'%(len(self.data),self.len,self.iAct,self.iStart,self.iEnd),
                                origin=self.appl)
            if self.iStart==-1:
                self.iStart=self.data.find(self.startMarker,self.iAct)
                #self.iAct=self.iStart   # 060225
                #self.iStart+=self.iStartMarker
                self.iAct=self.iStart+self.iStartMarker
            if self.iStart>=0:
                # start marker found
                try:
                    self.iEnd=self.data.find(self.endMarker,self.iAct)
                except:
                    pass
                #self.iAct=self.len
                if self.verbose>0:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCallStack(self,vtLog.DEBUG,
                                    'HandleData telegram found; len:%8d %08d; act:%8d;start:%8d; end:%8d'%(len(self.data),self.len,self.iAct,self.iStart,self.iEnd),
                                    origin=self.appl)
                if self.iEnd>0:
                    # end marker found, telegram is complete
                    self.iStart+=self.iStartMarker
                    val=self.data[self.iStart:self.iEnd]
                    #vtLog.vtLngCallStack(self,vtLog.DEBUG,'  '+val[:60])
                    for cmd in self.cmds:
                        k=cmd[0]
                        iLen=len(k)
                        if val[:iLen]==k:
                            #cmd[1](val[iLen:].encode('ISO-8859-1'))
                            cmd[1](val[iLen:])
                            break
                    self.iEnd+=self.iEndMarker
                    #self.data=self.data[self.iEnd:]   # 060225
                    #self.len-=self.iEnd               # 060225 
                    #self.iStart=self.iEnd=self.iAct=-1  # 060225
                    self.iAct=self.iEnd
                    self.iStart=self.iEnd=-1
                    if self.iAct>=self.len:
                        self.data=''
                        self.len=0
                        self.iAct=0
                        if self.verbose>0:
                            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                vtLog.vtLngCallStack(self,vtLog.DEBUG,
                                    'HandleData reduce buffer; len:%8d %8d; act:%8d;start:%8d; end:%8d'%(len(self.data),self.len,self.iAct,self.iStart,self.iEnd),
                                    origin=self.appl)
                        return 0
                    if self.verbose>0:
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCallStack(self,vtLog.DEBUG,
                                    'HandleData; len:%8d %8d; act:%8d;start:%8d; end:%8d'%(len(self.data),self.len,self.iAct,self.iStart,self.iEnd),
                                    origin=self.appl)    
                    return 1
                else:
                    self.data=self.data[self.iStart:]
                    self.iAct=0
                    self.len-=self.iStart
                    self.iStart=-1
                    if self.verbose>0:
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCallStack(self,vtLog.DEBUG,
                                    'HandleData reduce buffer; len:%8d %8d; act:%8d;start:%8d; end:%8d'%(len(self.data),self.len,self.iAct,self.iStart,self.iEnd),
                                    origin=self.appl)
        except:
            vtLog.vtLngTB(self.appl)
        return 0
    def SetParent(self,par):
        self.par=par
        self.SetOrigin()
        if self.par is None:
            self.widLogging=None
            return
        try:
            self.widLogging=vtLog.GetPrintMsgWid(self.par)
        except:
            self.widLogging=None
    def SetOrigin(self):
        try:
            if self.par is not None:
                self.origin=''.join([self.par.GetOrigin(),'.',self.appl_alias])
            else:
                self.origin=self.appl_alias
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        self.origin=self.__class__.__name__
    def GetOrigin(self):
        return self.origin
    def SocketClosed(self):
        vtLog.vtLngCur(vtLog.INFO,'',
                origin=self.appl)
        #if self.doc is not None:
            #if self.doc.IsRunning():
            #    self.doc.Stop()
                #time.sleep(1)
                #vtLog.vtLogCallDepth(None,'wait',1)
        #self.doc=None
        if self.par is not None:
            self.par.__connectionClosed__(self.bEstabilshed)
        self.SetParent(None)
        #self.par=None
        #del self.cmds
        self.oLogin=None
        vtSock.SocketClosed(self)
    def Stop(self):
        vtLog.vtLngCS(vtLog.INFO,'Stop',
                origin=self.appl)
        vtSock.Stop(self)
    def IsRunning(self):
        return vtSock.IsRunning(self)
    def Login(self,usr,passwd):
        self.usr=usr
        self.passwd=passwd
        self.encoded_passwd=False
        self.bLoginSend=False
    def LoginEncoded(self,usr,passwd):
        self.usr=usr
        self.passwd=passwd
        self.encoded_passwd=True
        self.bLoginSend=False
    def __sessionID__(self,val):
        vtLog.vtLngCur(vtLog.INFO,'%s'%(val[1:]),
                origin=self.GetOrigin())
        #vtNetXmlSock.__sessionID__(self,val)
        self.sessionID=val[1:]
        if self.par is not None:
            self.par.NotifyConnected()
        #self.__doLogin__()
    def __doLogin__(self):
        vtLog.vtLngCS(vtLog.INFO,'__doLogin__',
                origin=self.appl)
        if len(self.passwd)>0:
            if self.encoded_passwd:
                sPasswd=self.passwd
            else:
                m=sha.new()
                m.update(self.passwd)
                sPasswd=m.hexdigest()
            m=sha.new()
            m.update(sPasswd)
            m.update(self.sessionID)
            s=binascii.b2a_uu(m.digest())
        else:
            s=''
        self.SendTelegram('login|'+self.usr+','+s)
        self.bLoginSend=True
    def __login__(self,val):
        vtLog.vtLngCS(vtLog.INFO,'__login__',
                origin=self.appl)
        if self.bLoginSend==False:
            self.__doLogin__()
        else:
            self.par.NotifyLogin()
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'',
                origin=self.appl)
    def __loggedin__(self,val):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCS(vtLog.INFO,'__loggedin__',
                    origin=self.appl)
        self.loggedin=True
        self.oLogin.SetStr(val[1:])
        try:
            self.par.NotifyLoggedIn(self.oLogin)
        except:
            vtLog.vtLngTB(self.appl)
            vtLog.vtLngCur(vtLog.ERROR,'class:%s'%self.par.__class__.__name__,origin=self.appl)
    def __authenticate__(self,val):
        try:
            #self.par.NotifyLoggedIn(self.oLogin)
            pass
        except:
            vtLog.vtLngTB(self.appl)
            vtLog.vtLngCur(vtLog.ERROR,'class:%s'%self.par.__class__.__name__,origin=self.appl)
        
    def __browse__(self,val):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'',origin=self.appl)
        self.par.NotifyBrowse(val[1:].split(';'))
    def ShutDown(self):
        pass
    def Browse(self,appl):
        self.SendTelegram('browse|'+appl)
    def Authenticate(self,id,pubKey):
        self.SendTelegram('authenticate|'+','.join([id,pubKey]))
    def __getFileStart__(self,val):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'val;%s'%val,origin=self.appl)
        try:
            i=val.find('|')
            iFlt=0
            if i>=0:
                sFN=val[i+1:]
                self.par.NotifyFileGetStart(sFN)
                return
            vtLog.vtLngCur(vtLog.WARN,'fault:%d'%iFlt,
                    origin=self.appl_alias)
        except:
            vtLog.vtLngTB(self.appl_alias)
    def __getFileProc__(self,val):
        #vtLog.vtLngCur(vtLog.INFO,'val;%s'%val,origin=self.appl)
        try:
            i=val.find('|')
            iFlt=0
            if i>=0:
                iFlt=1
                j=val.find(',',i+1)
                if j>0:
                    iFlt=2
                    k=val.find(',',j+1)
                    if k>0:
                        iFlt=3
                        l=val.find(',',k+1)
                        
                        sFN=val[i+1:j]
                        iAct=long(val[j+1:k])
                        iSize=long(val[k+1:l])
                        blk=val[l+1:]
                        d=self.dFileGet[sFN]
                        d['file'].write(blk)
                        d['sha'].update(blk)
                        self.par.NotifyFileGetProc(sFN,iAct,iSize)
                return
            vtLog.vtLngCur(vtLog.WARN,'fault:%d'%iFlt,
                    origin=self.appl_alias)
        except:
            vtLog.vtLngTB(self.appl_alias)
    def __getFileFin__(self,val):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'val;%s'%val,origin=self.appl)
        try:
            i=val.find('|')
            iFlt=0
            if i>=0:
                iFlt=1
                j=val.find(',',i+1)
                if j>0:
                    sFN=val[i+1:j]
                    dig=val[j+1:]
                    d=self.dFileGet[sFN]
                    if d.get('bTmp',False)==False:
                        d['file'].close()
                        if d['sha'].hexdigest()!=dig:
                            os.remove(d['tmpFN'])
                            vtLog.vtLngCur(vtLog.ERROR,'fn:%s content not identical'%(sFN),self.appl_alias)
                            self.par.NotifyFileGetAbort(sFN,'content')
                        else:
                            try:
                                fnUtil.shiftFile(d['FN'])
                                os.rename(d['tmpFN'],d['FN'])
                                self.par.NotifyFileGetFin(sFN,'ok')
                            except:
                                vtLog.vtLngCur(vtLog.ERROR,'fn:%s received correctly;but os can not rename file from;tmpFN:%s;FN:%s'%(sFN,d['tmpFN'],d['FN']),self.appl)
                                #vtLog.PrintMsg(_('os can not rename received file to %s')%(d['FN']),self.widLogging)
                                self.par.NotifyFileGetFin(sFN,'rename')
                            vtLog.vtLngCur(vtLog.INFO,'fn:%s received correctly'%(sFN),self.appl_alias)
                    else:
                        if d['sha'].hexdigest()!=dig:
                            vtLog.vtLngCur(vtLog.ERROR,'fn:%s content not identical'%(sFN),self.appl_alias)
                            d['file'].close()
                            self.par.NotifyFileGetAbort(sFN,'content')
                        else:
                            #sTmpFN=d['file'].name
                            try:
                                d['file'].close()
                                vtLog.vtLngCur(vtLog.INFO,'fn:%s received correctly as temp;sTmpFN:%s'%(sFN,d['tmpFN']),self.appl_alias)
                                self.par.NotifyFileGetFinTmp(sFN,d['tmpFN'])#d['file'])
                            except:
                                vtLog.vtLngTB(self.appl_alias)
                                self.par.NotifyFileGetAbort(sFN,'access')
                        print 
                        pass
                    del self.dFileGet[sFN]
                return
            vtLog.vtLngCur(vtLog.WARN,'fault:%d'%iFlt,
                    origin=self.appl_alias)
        except:
            vtLog.vtLngTB(self.appl_alias)
    def __getFileAbort__(self,val):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'val;%s'%val,origin=self.appl_alias)
        try:
            i=val.find('|')
            iFlt=0
            if i>=0:
                iFlt=1
                j=val.find(',',i+1)
                if j>0:
                    iFlt=2
                    sFN=val[i+1:j]
                    sMsg=val[j+1:]
                    d=self.dFileGet[sFN]
                    d['file'].close()
                    if d.get('bTmp',False):
                        os.remove(d['tmpFN'])
                    
                    del self.dFileGet[sFN]
                    self.par.NotifyFileGetAbort(sFN,sMsg)
                    return
            vtLog.vtLngCur(vtLog.WARN,'fault:%d'%iFlt,
                    origin=self.appl_alias)
        except:
            vtLog.vtLngTB(self.appl_alias)
    def __listContent__(self,val):
        vtLog.vtLngCur(vtLog.INFO,'val;%s'%val,origin=self.appl)
        i=val.find(',',1)
        if i>0:
            appl_alias=val[1:i]
            self.par.NotifyListContent(appl_alias,val[i+1:].split(';'))
    def GetFile(self,appl,sDN,sFN,bTmp=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'fn:%s;dn:%s'%(sFN,sDN),self.appl_alias)
        if sFN in self.dFileGet:
            vtLog.vtLngCur(vtLog.ERROR,'file is already in process:%s:dFileGet:%s'%\
                                (sFN,vtLog.pformat(self.dFileGet)),
                                origin=self.appl_alias)
            return -1
        if bTmp:
            #f=tempfile.TemporaryFile('wb')
            #print f.__dict__
            #sTmpFN=None
            sTmpFN=os.path.join(tempfile.gettempdir(),sFN.replace('/','-'))
            f=open(sTmpFN,'wb')
        else:
            sTmpFN=os.path.join(sDN,sFN)+'.net'
            dn,fn=os.path.split(sTmpFN)
            try:
                os.makedirs(dn)
            except:
                pass
            f=open(sTmpFN,'wb')
        self.dFileGet[sFN]={'file':f,'sha':sha.new(),'FN':os.path.join(sDN,sFN),
                'tmpFN':sTmpFN,'bTmp':bTmp}
        self.SendTelegram('getFile|'+','.join([appl,sFN]))
    def __setFile__(self,sFN,f,iSize,zTimes=None):
        try:
            iAct=0
            m=sha.new()
            #self.SendTelegram('setFileStart|'+','.join([appl,sFN]))
            while iAct!=iSize:
                blk=f.read(4096)
                iAct=f.tell()
                m.update(blk)
                self.Send(self.MARKER_START+'setFileProc|'+','.join([sFN,str(iAct),str(iSize),'']))
                self.Send(blk,True)
                self.Send(self.MARKER_END)
                #self.SendTelegram('setFileProc|'+','.join([sFN,str(iAct),str(iSize),blk]))
            dig=m.hexdigest()
            if zTimes is not None:
                self.SendTelegram('setFileFin|'+','.join([sFN,dig]+[str(z) for z in zTimes]))
            else:
                self.SendTelegram('setFileFin|'+','.join([sFN,dig]))
        except:
            self.SendTelegram('setFileAbort|'+','.join([sFN,'processing fault']))
            vtLog.vtLngTB(self.appl_alias)
        f.close()
    def SetFile(self,appl,sDN,sFN,zTimes=None):
        sFullFN=os.path.join(sDN,sFN)
        if sFN in self.dFileSet:
            vtLog.vtLngCur(vtLog.ERROR,'file is already in process:%s:dFileGet:%s'%\
                                (sFN,vtLog.pformat(self.dFileSet)),
                                origin=self.appl_alias)
            return -1
        try:
            sCltFN=os.path.join(sDN,sFN)
            f=open(sCltFN,'rb')
            s=os.stat(sFullFN)
            iSize=s[stat.ST_SIZE]
        except:
            vtLog.vtLngTB(self.appl_alias)
            return
        self.dFileSet[sFN]={'file':f,'FN':sFN,'iSize':iSize,'zTimes':zTimes}
        self.SendTelegram('setFileStart|'+','.join([appl,sFN]))
    def __setFileStart__(self,val):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'val;%s'%val,origin=self.appl_alias)
        try:
            i=val.find('|')
            iFlt=0
            if i>=0:
                sFN=val[i+1:]
                if sFN in self.dFileSet:
                    d=self.dFileSet[sFN]
                    self.thdSend.Do(self.__setFile__,d['FN'],d['file'],d['iSize'],d.get('zTimes'))
        except:
            vtLog.vtLngTB(self.appl_alias)
    def __setFileProc__(self,val):
        #vtLog.vtLngCur(vtLog.INFO,'val;%s'%val,origin=self.appl)
        try:
            i=val.find('|')
            iFlt=0
            if i>=0:
                iFlt=1
                j=val.find(',',i+1)
                if j>0:
                    iFlt=2
                    k=val.find(',',j+1)
                    if k>0:
                        iFlt=3
                        
                        sFN=val[i+1:j]
                        iAct=long(val[j+1:k])
                        iSize=long(val[k+1:])
                        #d=self.dFileGet[sFN]
                        self.par.NotifyFileSetProc(sFN,iAct,iSize)
                return
            vtLog.vtLngCur(vtLog.WARN,'fault:%d'%iFlt,
                    origin=self.appl_alias)
        except:
            vtLog.vtLngTB(self.appl_alias)
    def __setFileFin__(self,val):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'val;%s;dFileSet:%s'%(val,vtLog.pformat(self.dFileSet)),origin=self.appl_alias)
        try:
            i=val.find('|')
            iFlt=0
            if i>=0:
                iFlt=1
                j=val.find(',',i+1)
                if j>0:
                    iFlt=2
                    sFN=val[i+1:j]
                    sMsg=val[j+1:]
                    d=self.dFileSet[sFN]
                    d['file'].close()
                    self.par.NotifyFileSetFin(sFN,sMsg)
                    del self.dFileSet[sFN]
                    return
            vtLog.vtLngCur(vtLog.WARN,'fault:%d'%iFlt,
                    origin=self.appl_alias)
        except:
            vtLog.vtLngTB(self.appl_alias)
    def __setFileAbort__(self,val):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'val;%s;dFileSet:%s'%(val,vtLog.pformat(self.dFileSet)),origin=self.appl_alias)
        try:
            i=val.find('|')
            iFlt=0
            if i>=0:
                iFlt=1
                j=val.find(',',i+1)
                if j>0:
                    iFlt=2
                    sFN=val[i+1:j]
                    sMsg=val[j+1:]
                    d=self.dFileSet[sFN]
                    d['file'].close()
                    self.par.NotifyFileSetAbort(sFN,sMsg)
                    del self.dFileSet[sFN]
                    return
            vtLog.vtLngCur(vtLog.WARN,'fault:%d'%iFlt,
                    origin=self.appl_alias)
        except:
            vtLog.vtLngTB(self.appl_alias)
    def ListContent(self,appl):
        self.SendTelegram('listContent|'+appl)
    def __alias__(self,val):
        vtLog.vtLngCur(vtLog.DEBUG,val,
                origin=self.appl)
        i=val.find('|')
        if i>=0:
            try:
                if val[i+1:]=='served':
                    # notify served
                    if self.par is not None:
                        self.par.NotifyServed(True)
                else:
                    if self.par is not None:
                        self.par.NotifyServed(False)
            except:
                pass
    def Alias(self,appl,alias):
        self.appl=appl
        self.appl_alias=':'.join([appl,alias])
        vtLog.vtLngCur(vtLog.DEBUG,'appl:%s;alias:%s;applAlias:%s'%(appl,alias,self.appl_alias),
                origin=self.appl)
        self.SendTelegram('alias|'+self.appl_alias)
    def __shutdown__(self,val):
        vtLog.vtLngCS(vtLog.INFO,'__shutdown__',
                origin=self.appl)
        vtLog.vtLngCallStack(self,vtLog.DEBUG,val,
                origin=self.appl)
        pass

