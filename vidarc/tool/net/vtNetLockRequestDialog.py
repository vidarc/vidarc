#Boa:Dialog:vtNetLockRequestDialog
#----------------------------------------------------------------------------
# Name:         vtNetLockRequestDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060423
# CVS-ID:       $Id: vtNetLockRequestDialog.py,v 1.6 2009/10/03 22:35:12 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

def create(parent):
    return vtNetLockRequestDialog(parent)

[wxID_VTNETLOCKREQUESTDIALOG, wxID_VTNETLOCKREQUESTDIALOGCBAGREE, 
 wxID_VTNETLOCKREQUESTDIALOGCBREVOKE, wxID_VTNETLOCKREQUESTDIALOGLBLMSG, 
 wxID_VTNETLOCKREQUESTDIALOGLBLNODE, wxID_VTNETLOCKREQUESTDIALOGLBLREQ, 
 wxID_VTNETLOCKREQUESTDIALOGPBTIMEOUT, wxID_VTNETLOCKREQUESTDIALOGTXTID, 
 wxID_VTNETLOCKREQUESTDIALOGTXTMSG, wxID_VTNETLOCKREQUESTDIALOGTXTREQ, 
 wxID_VTNETLOCKREQUESTDIALOGTXTTAG, 
] = [wx.NewId() for _init_ctrls in range(11)]

class vtNetLockRequestDialog(wx.Dialog):
    def _init_coll_bxsMsg_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblMsg, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtMsg, 3, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbAgree, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.cbRevoke, 0, border=0, flag=0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsNode, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsReq, 1, border=4,
              flag=wx.TOP | wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsMsg, 1, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.pbTimeOut, 0, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.TOP | wx.BOTTOM | wx.ALIGN_CENTER)

    def _init_coll_bxsNode_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblNode, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtTag, 2, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.txtId, 1, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsReq_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblReq, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtReq, 3, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=5, vgap=0)

        self.bxsNode = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsReq = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsMsg = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_bxsNode_Items(self.bxsNode)
        self._init_coll_bxsReq_Items(self.bxsReq)
        self._init_coll_bxsMsg_Items(self.bxsMsg)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTNETLOCKREQUESTDIALOG,
              name=u'vtNetLockRequestDialog', parent=prnt, pos=wx.Point(425,
              205), size=wx.Size(304, 175),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE,
              title=u'vtNetLock Request Dialog')
        self.SetClientSize(wx.Size(296, 148))

        self.lblMsg = wx.StaticText(id=wxID_VTNETLOCKREQUESTDIALOGLBLMSG,
              label=_(u'Message'), name=u'lblMsg', parent=self, pos=wx.Point(4,
              58), size=wx.Size(72, 21), style=0)
        self.lblMsg.SetMinSize(wx.Size(-1, -1))

        self.txtTag = wx.TextCtrl(id=wxID_VTNETLOCKREQUESTDIALOGTXTTAG,
              name=u'txtTag', parent=self, pos=wx.Point(80, 4),
              size=wx.Size(140, 21), style=0, value=u'')
        self.txtTag.Enable(False)
        self.txtTag.SetMinSize(wx.Size(-1, -1))

        self.txtId = wx.TextCtrl(id=wxID_VTNETLOCKREQUESTDIALOGTXTID,
              name=u'txtId', parent=self, pos=wx.Point(224, 4), size=wx.Size(68,
              21), style=0, value=u'')
        self.txtId.Enable(False)
        self.txtId.SetMinSize(wx.Size(-1, -1))

        self.lblReq = wx.StaticText(id=wxID_VTNETLOCKREQUESTDIALOGLBLREQ,
              label=_(u'Request'), name=u'lblReq', parent=self, pos=wx.Point(4,
              29), size=wx.Size(72, 21), style=0)
        self.lblReq.SetMinSize(wx.Size(-1, -1))

        self.txtMsg = wx.TextCtrl(id=wxID_VTNETLOCKREQUESTDIALOGTXTMSG,
              name=u'txtMsg', parent=self, pos=wx.Point(80, 58),
              size=wx.Size(212, 21), style=wx.TE_PROCESS_ENTER, value=u'')
        self.txtMsg.SetMinSize(wx.Size(-1, -1))
        self.txtMsg.Bind(wx.EVT_TEXT_ENTER, self.OnTxtMsgTextEnter,
              id=wxID_VTNETLOCKREQUESTDIALOGTXTMSG)
        self.txtMsg.Bind(wx.EVT_TEXT, self.OnTxtMsgText,
              id=wxID_VTNETLOCKREQUESTDIALOGTXTMSG)

        self.txtReq = wx.TextCtrl(id=wxID_VTNETLOCKREQUESTDIALOGTXTREQ,
              name=u'txtReq', parent=self, pos=wx.Point(80, 29),
              size=wx.Size(212, 21), style=0, value=u'')
        self.txtReq.Enable(False)
        self.txtReq.SetMinSize(wx.Size(-1, -1))

        self.lblNode = wx.StaticText(id=wxID_VTNETLOCKREQUESTDIALOGLBLNODE,
              label=_(u'Node'), name=u'lblNode', parent=self, pos=wx.Point(4,
              4), size=wx.Size(72, 21), style=0)
        self.lblNode.SetMinSize(wx.Size(-1, -1))

        self.cbAgree = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Apply),
              id=wxID_VTNETLOCKREQUESTDIALOGCBAGREE, label=_(u'Agree'),
              name=u'cbAgree', parent=self, pos=wx.Point(56, 108),
              size=wx.Size(76, 30), style=0)
        self.cbAgree.SetMinSize(wx.Size(-1, -1))
        self.cbAgree.Bind(wx.EVT_BUTTON, self.OnCbAgreeButton,
              id=wxID_VTNETLOCKREQUESTDIALOGCBAGREE)

        self.cbRevoke = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Cancel),
              id=wxID_VTNETLOCKREQUESTDIALOGCBREVOKE, label=_(u'Reject'),
              name=u'cbRevoke', parent=self, pos=wx.Point(164, 108),
              size=wx.Size(76, 30), style=0)
        self.cbRevoke.SetMinSize(wx.Size(-1, -1))
        self.cbRevoke.Bind(wx.EVT_BUTTON, self.OnCbRevokeButton,
              id=wxID_VTNETLOCKREQUESTDIALOGCBREVOKE)

        self.pbTimeOut = wx.Gauge(id=wxID_VTNETLOCKREQUESTDIALOGPBTIMEOUT,
              name=u'pbTimeOut', parent=self, pos=wx.Point(4, 87), range=20,
              size=wx.Size(288, 13), style=wx.GA_SMOOTH | wx.GA_HORIZONTAL)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        self._init_ctrls(parent)
        self.timer=wx.PyTimer(self.Notify)
        self.doc=None
        self.node=None
        self.sRaw=''
    def Show(self,bFlag):
        if bFlag==False:
            try:
                self.sRaw=''
                self.timer.Stop()
            except:
                pass
        else:
            self.txtMsg.SetFocus()
        wx.Dialog.Show(self,bFlag)
    def Notify(self):
        iVal=self.pbTimeOut.GetValue()+1
        if iVal==self.pbTimeOut.GetRange():
            self.timer.Stop()
            self.Show(False)
            try:
                self.doc.relLock(None,'release',self.txtMsg.GetValue(),self.nodeId)
                #self.doc.relLock(None,','.join(['release',self.sRaw]),self.txtMsg.GetValue(),self.nodeId)
            except:
                vtLog.vtLngTB(self.GetName())
            return
        self.pbTimeOut.SetValue(iVal)
    def SetRequest(self,doc,node,sReq,sRaw):
        vtLog.vtLngCurWX(vtLog.INFO,sReq,self)
        self.doc=doc
        self.node=node
        self.nodeId=''
        self.sRaw=sRaw[:]
        self.txtTag.SetValue('')
        self.txtId.SetValue('')
        self.txtReq.SetValue('')
        self.txtMsg.SetValue('')
        self.pbTimeOut.SetValue(0)
        try:
            self.txtTag.SetValue(doc.getNodeText(node,doc.TAGNAME_REFERENCE))
            self.txtId.SetValue(doc.getKey(node))
            self.nodeId=doc.getKey(node)
            self.txtReq.SetValue(sReq)
        except:
            vtLog.vtLngTB(self.GetName())
        self.timer.Start(1000)

    def OnCbAgreeButton(self, event):
        if event is not None:
            event.Skip()
        try:
            self.doc.relLock(None,'ok',self.txtMsg.GetValue(),self.nodeId)
            #self.doc.relLock(None,','.join(['ok',self.sRaw]),self.txtMsg.GetValue(),self.nodeId)
        except:
            vtLog.vtLngTB(self.GetName())
        self.Show(False)
        
    def OnCbRevokeButton(self, event):
        event.Skip()
        try:
            self.doc.relLock(None,'reject',self.txtMsg.GetValue(),self.nodeId)
            #self.doc.relLock(None,','.join(['reject',self.sRaw]),self.txtMsg.GetValue(),self.nodeId)
        except:
            vtLog.vtLngTB(self.GetName())
        self.Show(False)

    def OnTxtMsgTextEnter(self, event):
        event.Skip()
        self.OnCbAgreeButton(None)
    def OnTxtMsgText(self, event):
        event.Skip()
        self.pbTimeOut.SetValue(0)
        
