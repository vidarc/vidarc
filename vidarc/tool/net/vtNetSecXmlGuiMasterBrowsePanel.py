#Boa:FramePanel:vtNetSecXmlGuiMasterBrowsePanel
#----------------------------------------------------------------------------
# Name:         vtNetSecXmlGuiMasterBrowsePanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060416
# CVS-ID:       $Id: vtNetSecXmlGuiMasterBrowsePanel.py,v 1.8 2007/10/13 19:25:40 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.filebrowsebutton
import wx.lib.buttons

import sys

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

import vidarc.tool.net.img_gui as img_gui


[wxID_VTNETSECXMLGUIMASTERBROWSEPANEL, 
 wxID_VTNETSECXMLGUIMASTERBROWSEPANELCBADD, 
 wxID_VTNETSECXMLGUIMASTERBROWSEPANELCBCANCEL, 
 wxID_VTNETSECXMLGUIMASTERBROWSEPANELCBCONNECT, 
 wxID_VTNETSECXMLGUIMASTERBROWSEPANELCBDEL, 
 wxID_VTNETSECXMLGUIMASTERBROWSEPANELCBDOWN, 
 wxID_VTNETSECXMLGUIMASTERBROWSEPANELCBOPEN, 
 wxID_VTNETSECXMLGUIMASTERBROWSEPANELCBSORT, 
 wxID_VTNETSECXMLGUIMASTERBROWSEPANELCBSTOP, 
 wxID_VTNETSECXMLGUIMASTERBROWSEPANELCBUP, 
 wxID_VTNETSECXMLGUIMASTERBROWSEPANELDBBLOCAL, 
 wxID_VTNETSECXMLGUIMASTERBROWSEPANELLBLAPPL, 
 wxID_VTNETSECXMLGUIMASTERBROWSEPANELLSTRECENT, 
 wxID_VTNETSECXMLGUIMASTERBROWSEPANELTXTAPPL, 
] = [wx.NewId() for _init_ctrls in range(14)]

wxEVT_VTSEC_XML_MASTER_BROWSE_PANEL_CONNECT=wx.NewEventType()
vEVT_VTSEC_XML_MASTER_BROWSE_PANEL_CONNECT=wx.PyEventBinder(wxEVT_VTSEC_XML_MASTER_BROWSE_PANEL_CONNECT,1)
def EVT_VTSEC_XML_MASTER_BROWSE_PANEL_CONNECT(win,func):
    win.Connect(-1,-1,wxEVT_VTSEC_XML_MASTER_BROWSE_PANEL_CONNECT,func)
class vtSecXmlMasterBrowsePanelConnect(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTSEC_XML_MASTER_BROWSE_PANEL_CONNECT(<widget_name>, self.OnInfoConnect)
    """

    def __init__(self,obj,name):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VTSEC_XML_MASTER_BROWSE_PANEL_CONNECT)
        self.obj=obj
        self.name=name
    def GetConnectionName(self):
        return self.name
    def GetObject(self):
        return self.obj

wxEVT_VTSEC_XML_MASTER_BROWSE_PANEL_OPEN=wx.NewEventType()
vEVT_VTSEC_XML_MASTER_BROWSE_PANEL_OPEN=wx.PyEventBinder(wxEVT_VTSEC_XML_MASTER_BROWSE_PANEL_OPEN,1)
def EVT_VTSEC_XML_MASTER_BROWSE_PANEL_OPEN(win,func):
    win.Connect(-1,-1,wxEVT_VTSEC_XML_MASTER_BROWSE_PANEL_OPEN,func)
class vtSecXmlMasterBrowsePanelOpen(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTSEC_XML_MASTER_BROWSE_PANEL_OPEN(<widget_name>, self.OnInfoOpen)
    """

    def __init__(self,obj,name):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VTSEC_XML_MASTER_BROWSE_PANEL_OPEN)
        self.obj=obj
        self.name=name
    def GetConnectionName(self):
        return self.name
    def GetObject(self):
        return self.obj

wxEVT_VTSEC_XML_MASTER_BROWSE_PANEL_STOP=wx.NewEventType()
vEVT_VTSEC_XML_MASTER_BROWSE_PANEL_STOP=wx.PyEventBinder(wxEVT_VTSEC_XML_MASTER_BROWSE_PANEL_STOP,1)
def EVT_VTSEC_XML_MASTER_BROWSE_PANEL_STOP(win,func):
    win.Connect(-1,-1,wxEVT_VTSEC_XML_MASTER_BROWSE_PANEL_STOP,func)
class vtSecXmlMasterBrowsePanelStop(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTSEC_XML_MASTER_BROWSE_PANEL_STOP(<widget_name>, self.OnInfoStop)
    """

    def __init__(self,obj,name):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VTSEC_XML_MASTER_BROWSE_PANEL_STOP)
        self.obj=obj
        self.name=name
    def GetConnectionName(self):
        return self.name
    def GetObject(self):
        return self.obj

wxEVT_VTSEC_XML_MASTER_BROWSE_PANEL_CANCELED=wx.NewEventType()
vEVT_VTSEC_XML_MASTER_BROWSE_PANEL_CANCELED=wx.PyEventBinder(wxEVT_VTSEC_XML_MASTER_BROWSE_PANEL_CANCELED,1)
def EVT_VTSEC_XML_MASTER_BROWSE_PANEL_CANCELED(win,func):
    win.Connect(-1,-1,wxEVT_VTSEC_XML_MASTER_BROWSE_PANEL_CANCELED,func)
class vtSecXmlMasterBrowsePanelCanceled(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTSEC_XML_MASTER_BROWSE_PANEL_CANCELED(<widget_name>, self.OnInfoCanceled)
    """

    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VTSEC_XML_MASTER_BROWSE_PANEL_CANCELED)
        self.obj=obj
    def GetObject(self):
        return self.obj

wxEVT_VTSEC_XML_MASTER_BROWSE_PANEL_ADD=wx.NewEventType()
vEVT_VTSEC_XML_MASTER_BROWSE_PANEL_ADD=wx.PyEventBinder(wxEVT_VTSEC_XML_MASTER_BROWSE_PANEL_ADD,1)
def EVT_VTSEC_XML_MASTER_BROWSE_PANEL_ADD(win,func):
    win.Connect(-1,-1,wxEVT_VTSEC_XML_MASTER_BROWSE_PANEL_ADD,func)
class vtSecXmlMasterBrowsePanelAdd(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTSEC_XML_MASTER_BROWSE_PANEL_ADD(<widget_name>, self.OnInfoCanceled)
    """

    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VTSEC_XML_MASTER_BROWSE_PANEL_ADD)
        self.obj=obj
    def GetObject(self):
        return self.obj

wxEVT_VTSEC_XML_MASTER_BROWSE_PANEL_DEL=wx.NewEventType()
vEVT_VTSEC_XML_MASTER_BROWSE_PANEL_DEL=wx.PyEventBinder(wxEVT_VTSEC_XML_MASTER_BROWSE_PANEL_DEL,1)
def EVT_VTSEC_XML_MASTER_BROWSE_PANEL_DEL(win,func):
    win.Connect(-1,-1,wxEVT_VTSEC_XML_MASTER_BROWSE_PANEL_DEL,func)
class vtSecXmlMasterBrowsePanelDel(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTSEC_XML_MASTER_BROWSE_PANEL_DEL(<widget_name>, self.OnInfoCanceled)
    """

    def __init__(self,obj,sConn):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VTSEC_XML_MASTER_BROWSE_PANEL_DEL)
        self.obj=obj
        self.sConn=sConn
    def GetObject(self):
        return self.obj
    def GetConnection(self):
        return self.sConn

class vtNetSecXmlGuiMasterBrowsePanel(wx.Panel):
    VERBOSE=0
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbDel, 0, border=4, flag=wx.BOTTOM)
        parent.AddWindow(self.cbUp, 0, border=4, flag=wx.TOP)
        parent.AddWindow(self.cbDown, 0, border=4, flag=wx.BOTTOM | wx.TOP)
        parent.AddWindow(self.cbSort, 0, border=4, flag=wx.TOP)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.dbbLocal, 0, border=4, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsAppl, 1, border=4,
              flag=wx.BOTTOM | wx.EXPAND | wx.TOP | wx.LEFT)
        parent.AddWindow(self.cbAdd, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.lstRecent, 0, border=4,
              flag=wx.BOTTOM | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsBt, 0, border=4, flag=wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.btBtCmd, 0, border=4,
              flag=wx.ALIGN_CENTER | wx.BOTTOM | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.cbStop, 0, border=4, flag=wx.TOP | wx.LEFT)

    def _init_coll_bxsAppl_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblAppl, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtAppl, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_btBtCmd_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbOpen, 0, border=4, flag=wx.RIGHT)
        parent.AddWindow(self.cbConnect, 0, border=4, flag=wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_lstRecent_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'Alias'), width=80)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'Host'), width=80)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'Port'), width=60)
        parent.InsertColumn(col=3, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'User'), width=60)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=2, hgap=0, rows=4, vgap=0)

        self.bxsAppl = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.btBtCmd = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsAppl_Items(self.bxsAppl)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_btBtCmd_Items(self.btBtCmd)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTNETSECXMLGUIMASTERBROWSEPANEL,
              name=u'vtNetSecXmlGuiMasterBrowsePanel', parent=prnt,
              pos=wx.Point(368, 326), size=wx.Size(382, 286),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(374, 259))

        self.dbbLocal = wx.lib.filebrowsebutton.DirBrowseButton(buttonText='...',
              dialogTitle=_(u'Choose local data directory'),
              id=wxID_VTNETSECXMLGUIMASTERBROWSEPANELDBBLOCAL,
              labelText=_('local data:'), parent=self, pos=wx.Point(0, 0),
              size=wx.Size(337, 29), startDirectory='.', style=0)
        self.dbbLocal.SetMinSize(wx.Size(-1, -1))

        self.lblAppl = wx.StaticText(id=wxID_VTNETSECXMLGUIMASTERBROWSEPANELLBLAPPL,
              label=_(u'Application'), name=u'lblAppl', parent=self,
              pos=wx.Point(4, 33), size=wx.Size(111, 22), style=0)
        self.lblAppl.SetMinSize(wx.Size(-1, -1))

        self.txtAppl = wx.TextCtrl(id=wxID_VTNETSECXMLGUIMASTERBROWSEPANELTXTAPPL,
              name=u'txtAppl', parent=self, pos=wx.Point(119, 33),
              size=wx.Size(218, 22), style=0, value=u'')
        self.txtAppl.Enable(False)
        self.txtAppl.SetMinSize(wx.Size(-1, -1))

        self.lstRecent = wx.ListCtrl(id=wxID_VTNETSECXMLGUIMASTERBROWSEPANELLSTRECENT,
              name=u'lstRecent', parent=self, pos=wx.Point(4, 59),
              size=wx.Size(333, 158), style=wx.LC_REPORT)
        self._init_coll_lstRecent_Columns(self.lstRecent)
        self.lstRecent.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstRecentListColClick,
              id=wxID_VTNETSECXMLGUIMASTERBROWSEPANELLSTRECENT)
        self.lstRecent.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstRecentListItemDeselected,
              id=wxID_VTNETSECXMLGUIMASTERBROWSEPANELLSTRECENT)
        self.lstRecent.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstRecentListItemSelected,
              id=wxID_VTNETSECXMLGUIMASTERBROWSEPANELLSTRECENT)
        self.lstRecent.Bind(wx.EVT_LEFT_DCLICK, self.OnLstRecentLeftDclick)

        self.cbAdd = wx.lib.buttons.GenBitmapButton(ID=wxID_VTNETSECXMLGUIMASTERBROWSEPANELCBADD,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbAdd', parent=self,
              pos=wx.Point(341, 29), size=wx.Size(31, 30), style=0)
        self.cbAdd.Bind(wx.EVT_BUTTON, self.OnCbAddButton,
              id=wxID_VTNETSECXMLGUIMASTERBROWSEPANELCBADD)

        self.cbDel = wx.lib.buttons.GenBitmapButton(ID=wxID_VTNETSECXMLGUIMASTERBROWSEPANELCBDEL,
              bitmap=vtArt.getBitmap(vtArt.Del), name=u'cbDel', parent=self,
              pos=wx.Point(341, 59), size=wx.Size(31, 30), style=0)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VTNETSECXMLGUIMASTERBROWSEPANELCBDEL)

        self.cbUp = wx.lib.buttons.GenBitmapButton(ID=wxID_VTNETSECXMLGUIMASTERBROWSEPANELCBUP,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbUp', parent=self,
              pos=wx.Point(341, 97), size=wx.Size(31, 30), style=0)
        self.cbUp.Bind(wx.EVT_BUTTON, self.OnCbUpButton,
              id=wxID_VTNETSECXMLGUIMASTERBROWSEPANELCBUP)

        self.cbDown = wx.lib.buttons.GenBitmapButton(ID=wxID_VTNETSECXMLGUIMASTERBROWSEPANELCBDOWN,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbDown', parent=self,
              pos=wx.Point(341, 131), size=wx.Size(31, 30), style=0)
        self.cbDown.Bind(wx.EVT_BUTTON, self.OnCbDownButton,
              id=wxID_VTNETSECXMLGUIMASTERBROWSEPANELCBDOWN)

        self.cbSort = wx.lib.buttons.GenBitmapButton(ID=wxID_VTNETSECXMLGUIMASTERBROWSEPANELCBSORT,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbSort', parent=self,
              pos=wx.Point(341, 169), size=wx.Size(31, 30), style=0)
        self.cbSort.Bind(wx.EVT_BUTTON, self.OnCbSortButton,
              id=wxID_VTNETSECXMLGUIMASTERBROWSEPANELCBSORT)

        self.cbOpen = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTNETSECXMLGUIMASTERBROWSEPANELCBOPEN,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Open'), name=u'cbOpen',
              parent=self, pos=wx.Point(44, 225), size=wx.Size(76, 30),
              style=0)
        self.cbOpen.SetMinSize(wx.Size(-1, -1))
        self.cbOpen.Bind(wx.EVT_BUTTON, self.OnCbOpenButton,
              id=wxID_VTNETSECXMLGUIMASTERBROWSEPANELCBOPEN)

        self.cbConnect = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTNETSECXMLGUIMASTERBROWSEPANELCBCONNECT,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Connect'),
              name=u'cbConnect', parent=self, pos=wx.Point(128, 225),
              size=wx.Size(76, 30), style=0)
        self.cbConnect.SetMinSize(wx.Size(-1, -1))
        self.cbConnect.Bind(wx.EVT_BUTTON, self.OnCbConnectButton,
              id=wxID_VTNETSECXMLGUIMASTERBROWSEPANELCBCONNECT)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTNETSECXMLGUIMASTERBROWSEPANELCBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(216, 225),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.SetMinSize(wx.Size(-1, -1))
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VTNETSECXMLGUIMASTERBROWSEPANELCBCANCEL)

        self.cbStop = wx.lib.buttons.GenBitmapButton(ID=wxID_VTNETSECXMLGUIMASTERBROWSEPANELCBSTOP,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbStop', parent=self,
              pos=wx.Point(341, 225), size=wx.Size(31, 30), style=0)
        self.cbStop.SetToolTipString(_(u'stop'))
        self.cbStop.Bind(wx.EVT_BUTTON, self.OnCbStopButton,
              id=wxID_VTNETSECXMLGUIMASTERBROWSEPANELCBSTOP)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        self._init_ctrls(parent)
        
        self.cbAdd.SetBitmapLabel(vtArt.getBitmap(vtArt.Add))
        self.cbUp.SetBitmapLabel(vtArt.getBitmap(vtArt.Up))
        self.cbDown.SetBitmapLabel(vtArt.getBitmap(vtArt.Down))
        self.cbSort.SetBitmapLabel(vtArt.getBitmap(vtArt.Sort))
        self.cbCancel.SetBitmapLabel(vtArt.getBitmap(vtArt.Cancel))
        self.cbOpen.SetBitmapLabel(img_gui.getOpenBitmap())
        self.cbOpen.Enable(False)
        self.cbConnect.SetBitmapLabel(img_gui.getConnectBitmap())
        self.cbConnect.Enable(False)
        self.cbStop.SetBitmapLabel(img_gui.getStoppedBitmap())
        
        self.selIdx=-1

    def OnLstRecentListColClick(self, event):
        self.selIdx=-1
        self.cbOpen.Enable(False)
        self.cbConnect.Enable(False)
        event.Skip()

    def OnLstRecentListItemDeselected(self, event):
        self.selIdx=-1
        self.cbOpen.Enable(False)
        self.cbConnect.Enable(False)
        event.Skip()

    def OnLstRecentListItemSelected(self, event):
        self.selIdx=event.GetIndex()
        self.cbOpen.Enable(True)
        self.cbConnect.Enable(True)
        event.Skip()
    def __updateConnLst__(self,iStart,iEnd):
        for idx in range(iStart,iEnd):
            tup=self.lConn[idx]
            for i in range(4):
                self.lstRecent.SetStringItem(idx,i,tup[i+1])
    def __updateRecent__(self):
        i=0
        for tup in self.lConn:
            conn=tup[-2]
            conn.SetRecent(i)
            i+=1
    def OnCbUpButton(self, event):
        event.Skip()
        if self.selIdx<1:
            return
        idx=self.selIdx
        if self.VERBOSE:
            vtLog.CallStack('')
            vtLog.pprint(self.lConn)
        self.lConn=self.lConn[:idx-1]+[self.lConn[idx],self.lConn[idx-1]]+self.lConn[idx+1:]
        if self.VERBOSE:
            vtLog.CallStack('')
            vtLog.pprint(self.lConn)
        self.lstRecent.SetItemState(idx,0,wx.LIST_STATE_SELECTED)
        self.__updateConnLst__(idx-1,idx+1)
        idx-=1
        self.lstRecent.SetItemState(idx,wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
        self.lstRecent.EnsureVisible(idx)
        self.__updateRecent__()
    def OnCbDownButton(self, event):
        event.Skip()
        if self.selIdx<0 or (self.selIdx+1)>=len(self.lConn):
            return
        if self.VERBOSE:
            vtLog.CallStack('')
            vtLog.pprint(self.lConn)
        idx=self.selIdx
        self.lstRecent.SetItemState(idx,0,wx.LIST_STATE_SELECTED)
        self.lConn=self.lConn[:idx]+[self.lConn[idx+1],self.lConn[idx]]+self.lConn[idx+2:]
        if self.VERBOSE:
            vtLog.CallStack('')
            vtLog.pprint(self.lConn)
        self.__updateConnLst__(idx,idx+2)
        idx+=1
        self.lstRecent.SetItemState(idx,wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
        self.lstRecent.EnsureVisible(idx)
        self.__updateRecent__()
    def OnCbSortButton(self, event):
        def cmpFunc(a,b):
            return cmp(b[-2].GetDateTime(),a[-2].GetDateTime())
        self.lConn.sort(cmpFunc)
        self.__updateConnLst__(0,len(self.lConn))
        self.__updateRecent__()
        event.Skip()
    def SetAppl(self,appl):
        self.txtAppl.SetValue(appl)
    def SetLocalDN(self,dn):
        self.dbbLocal.SetValue(dn)
    def UpdateConnections(self,conns):
        if self.VERBOSE:
            vtLog.CallStack('')
        l=[]
        try:
            appl=self.txtAppl.GetValue()
            for conn in conns:
                oAppl=conn.GetAlias(appl)
                l.append((conn.GetRecent(),oAppl.GetAlias(),oAppl.GetHost(),str(oAppl.GetPort()),oAppl.GetUsr(),conn,oAppl))
            l.sort()
        except:
            vtLog.vtLngTB(self.GetName())
        self.lConn=l
        self.lstRecent.DeleteAllItems()
        for tup in l:
            idx=self.lstRecent.InsertStringItem(sys.maxint,tup[1])
            for i in range(1,4):
                self.lstRecent.SetStringItem(idx,i,tup[i+1])

    def OnCbOpenButton(self, event):
        appl=self.txtAppl.GetValue()
        if self.VERBOSE:
            vtLog.CallStack('')
            print self.lConn[self.selIdx][-2].GetHostConnectionStr(appl)
        wx.PostEvent(self,vtSecXmlMasterBrowsePanelOpen(self,
                self.lConn[self.selIdx][-2].GetHostConnectionStr(appl)))
        if event is not None:
            event.Skip()
        
    def OnCbStopButton(self, event):
        appl=self.txtAppl.GetValue()
        if self.VERBOSE:
            vtLog.CallStack('')
            print self.lConn[self.selIdx][-2].GetHostConnectionStr(appl)
        wx.PostEvent(self,vtSecXmlMasterBrowsePanelStop(self,
                self.lConn[self.selIdx][-2].GetHostConnectionStr(appl)))
        if event is not None:
            event.Skip()
    
    def OnCbConnectButton(self, event):
        appl=self.txtAppl.GetValue()
        if self.VERBOSE:
            vtLog.CallStack('')
            print self.lConn[self.selIdx][-2].GetHostConnectionStr(appl)
        wx.PostEvent(self,vtSecXmlMasterBrowsePanelConnect(self,
                self.lConn[self.selIdx][-2].GetHostConnectionStr(appl)))
        if event is not None:
            event.Skip()

    def OnCbCancelButton(self, event):
        wx.PostEvent(self,vtSecXmlMasterBrowsePanelCanceled(self))
        event.Skip()

    def OnLstRecentLeftDclick(self, event):
        event.Skip()
        if self.selIdx<0:
            return
        self.OnCbConnectButton(None)

    def OnCbAddButton(self, event):
        wx.PostEvent(self,vtSecXmlMasterBrowsePanelAdd(self))
        event.Skip()

    def OnCbDelButton(self, event):
        event.Skip()
        if self.selIdx<0:
            return
        appl=self.txtAppl.GetValue()
        if self.VERBOSE:
            vtLog.CallStack('')
            print self.lConn[self.selIdx][-2].GetHostConnectionStr(appl)
        wx.PostEvent(self,vtSecXmlMasterBrowsePanelDel(self,
                self.lConn[self.selIdx][-2].GetHostConnectionStr(appl)))
        self.selIdx=-1
        self.cbOpen.Enable(False)
        self.cbConnect.Enable(False)

