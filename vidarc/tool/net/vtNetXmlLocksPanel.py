#Boa:FramePanel:vtNetXmlLocksPanel
#----------------------------------------------------------------------------
# Name:         vtNetXmlLocksPanel.py
# Purpose:      
#               
# Author:       Walter Obweger
#
# Created:      20060417
# CVS-ID:       $Id: vtNetXmlLocksPanel.py,v 1.4 2006/08/29 10:06:24 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import sys

from vidarc.tool.time.vtTime import vtDateTime

import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_VTNETXMLLOCKSPANEL, wxID_VTNETXMLLOCKSPANELCBFORCE, 
 wxID_VTNETXMLLOCKSPANELCBREFRESH, wxID_VTNETXMLLOCKSPANELCBREQUEST, 
 wxID_VTNETXMLLOCKSPANELLSTLOCKS, 
] = [wx.NewId() for _init_ctrls in range(5)]

wxEVT_VTNET_XML_LOCKS_REQUEST=wx.NewEventType()
vEVT_VTNET_XML_LOCKS_REQUEST=wx.PyEventBinder(wxEVT_VTNET_XML_LOCKS_REQUEST,1)
def EVT_VTNET_XML_LOCKS_REQUEST(win,func):
    win.Connect(-1,-1,wxEVT_VTNET_XML_LOCKS_REQUEST,func)
class vtNetXmlLocksRequest(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTNET_XML_LOCKS_REQUEST(<widget_name>, self.OnRequest)
    """

    def __init__(self,obj,sApplAlias,id):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTNET_XML_LOCKS_REQUEST)
        self.obj=obj
        self.id=id
        self.applAlias=sApplAlias
    def GetApplAlias(self):
        return self.applAlias
    def GetID(self):
        return self.id
    def GetObject(self):
        return self.obj

wxEVT_VTNET_XML_LOCKS_FORCE=wx.NewEventType()
vEVT_VTNET_XML_LOCKS_FORCE=wx.PyEventBinder(wxEVT_VTNET_XML_LOCKS_FORCE,1)
def EVT_VTNET_XML_LOCKS_FORCE(win,func):
    win.Connect(-1,-1,wxEVT_VTNET_XML_LOCKS_FORCE,func)
class vtNetXmlLocksForce(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTNET_XML_LOCKS_FORCE(<widget_name>, self.OnForce)
    """

    def __init__(self,obj,sApplAlias,id):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTNET_XML_LOCKS_FORCE)
        self.obj=obj
        self.id=id
        self.applAlias=sApplAlias
    def GetApplAlias(self):
        return self.applAlias
    def GetID(self):
        return self.id
    def GetObject(self):
        return self.obj

wxEVT_VTNET_XML_LOCKS_UPDATE=wx.NewEventType()
vEVT_VTNET_XML_LOCKS_UPDATE=wx.PyEventBinder(wxEVT_VTNET_XML_LOCKS_UPDATE,1)
def EVT_VTNET_XML_LOCKS_UPDATE(win,func):
    win.Connect(-1,-1,wxEVT_VTNET_XML_LOCKS_UPDATE,func)
class vtNetXmlLocksUpdate(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTNET_XML_LOCKS_UPDATE(<widget_name>, self.OnForce)
    """

    def __init__(self,obj,sApplAlias):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTNET_XML_LOCKS_UPDATE)
        self.obj=obj
        self.applAlias=sApplAlias
    def GetApplAlias(self):
        return self.applAlias
    def GetObject(self):
        return self.obj

class vtNetXmlLocksPanel(wx.Panel):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbRequest, 0, border=0, flag=0)
        parent.AddWindow(self.cbForce, 0, border=4, flag=wx.TOP)
        parent.AddWindow(self.cbRefresh, 0, border=4, flag=wx.TOP)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstLocks, 0, border=4,
              flag=wx.BOTTOM | wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.TOP)

    def _init_coll_lstLocks_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_RIGHT,
              heading=_(u'id'), width=80)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'IP-adr'), width=80)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_RIGHT,
              heading=_(u'port'), width=60)
        parent.InsertColumn(col=3, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'usr'), width=60)
        parent.InsertColumn(col=4, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'time'), width=100)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=2, hgap=0, rows=1, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTNETXMLLOCKSPANEL,
              name=u'vtNetXmlLocksPanel', parent=prnt, pos=wx.Point(255, 196),
              size=wx.Size(304, 146), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(296, 119))

        self.lstLocks = wx.ListCtrl(id=wxID_VTNETXMLLOCKSPANELLSTLOCKS,
              name=u'lstLocks', parent=self, pos=wx.Point(4, 4),
              size=wx.Size(204, 111),
              style=wx.LC_SORT_ASCENDING | wx.LC_REPORT)
        self.lstLocks.SetMinSize(wx.Size(-1, -1))
        self._init_coll_lstLocks_Columns(self.lstLocks)
        self.lstLocks.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstLocksListColClick,
              id=wxID_VTNETXMLLOCKSPANELLSTLOCKS)
        self.lstLocks.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstLocksListItemDeselected,
              id=wxID_VTNETXMLLOCKSPANELLSTLOCKS)
        self.lstLocks.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstLocksListItemSelected,
              id=wxID_VTNETXMLLOCKSPANELLSTLOCKS)

        self.cbRequest = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTNETXMLLOCKSPANELCBREQUEST,
              bitmap=vtArt.getBitmap(vtArt.Question), label=_(u'Request'),
              name=u'cbRequest', parent=self, pos=wx.Point(216, 4),
              size=wx.Size(76, 30), style=0)
        self.cbRequest.SetMinSize(wx.Size(-1, -1))
        self.cbRequest.Bind(wx.EVT_BUTTON, self.OnCbRequestButton,
              id=wxID_VTNETXMLLOCKSPANELCBREQUEST)

        self.cbForce = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTNETXMLLOCKSPANELCBFORCE,
              bitmap=vtArt.getBitmap(vtArt.Pin), label=_(u'Force'),
              name=u'cbForce', parent=self, pos=wx.Point(216, 38),
              size=wx.Size(76, 30), style=0)
        self.cbForce.SetMinSize(wx.Size(-1, -1))
        self.cbForce.Bind(wx.EVT_BUTTON, self.OnCbForceButton,
              id=wxID_VTNETXMLLOCKSPANELCBFORCE)

        self.cbRefresh = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTNETXMLLOCKSPANELCBREFRESH,
              bitmap=vtArt.getBitmap(vtArt.Synch), label=_(u'Refresh'), name=u'cbRefresh',
              parent=self, pos=wx.Point(216, 72), size=wx.Size(76, 30),
              style=0)
        self.cbRefresh.Bind(wx.EVT_BUTTON, self.OnCbRefreshButton,
              id=wxID_VTNETXMLLOCKSPANELCBREFRESH)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        self._init_ctrls(parent)
        self.selIdx=-1
        self.dt=vtDateTime(True)
    
    def SetLocks(self,sApplAlias,lst):
        self.applAlias=sApplAlias
        self.selIdx=-1
        self.lstLocks.DeleteAllItems()
        for l in lst:
            self.dt.SetStr(l.GetDateTimeStr())
            sDt=self.dt.GetDateTimeStr(' ')
            idx=self.lstLocks.InsertStringItem(sys.maxint,'%08d'%l.GetID())
            self.lstLocks.SetStringItem(idx,1,l.GetAddr())
            self.lstLocks.SetStringItem(idx,2,str(l.GetPort()))
            self.lstLocks.SetStringItem(idx,3,l.GetUsr())
            self.lstLocks.SetStringItem(idx,4,sDt)
    def OnLstLocksListColClick(self, event):
        self.selIdx=-1
        event.Skip()

    def OnLstLocksListItemDeselected(self, event):
        self.selIdx=-1
        event.Skip()

    def OnLstLocksListItemSelected(self, event):
        self.selIdx=event.GetIndex()
        
        event.Skip()

    def OnCbRequestButton(self, event):
        if self.selIdx>=0:
            it=self.lstLocks.GetItem(self.selIdx,0)
            sId=it.m_text
            wx.PostEvent(self,vtNetXmlLocksRequest(self,self.applAlias,sId))
        event.Skip()

    def OnCbForceButton(self, event):
        iCount=self.lstLocks.GetItemCount()
        for i in xrange(iCount):
        #if self.selIdx>=0:
            #it=self.lstLocks.GetItem(self.selIdx,0)
            if self.lstLocks.GetItemState(i,wx.LIST_STATE_SELECTED)==0:
                continue
            it=self.lstLocks.GetItem(i,0)
            sId=it.m_text
            wx.PostEvent(self,vtNetXmlLocksForce(self,self.applAlias,sId))
        event.Skip()

    def OnCbRefreshButton(self, event):
        event.Skip()
        wx.PostEvent(self,vtNetXmlLocksUpdate(self,self.applAlias))
