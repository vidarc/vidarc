#Boa:Dialog:vNetXmlSynchEditResolveDialog

import wx
import wx.lib.buttons

import sys
import img_synch_tree

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

def create(parent):
    return vNetXmlSynchEditResolveDialog(parent)

[wxID_VNETXMLSYNCHEDITRESOLVEDIALOG, 
 wxID_VNETXMLSYNCHEDITRESOLVEDIALOGCBAPPLY, 
 wxID_VNETXMLSYNCHEDITRESOLVEDIALOGCBLOCAL, 
 wxID_VNETXMLSYNCHEDITRESOLVEDIALOGCBREMOTE, 
 wxID_VNETXMLSYNCHEDITRESOLVEDIALOGLSTRES, 
 wxID_VNETXMLSYNCHEDITRESOLVEDIALOGPNDATA, 
] = [wx.NewId() for _init_ctrls in range(6)]

class vNetXmlSynchEditResolveDialog(wx.Dialog):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbLocal, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.cbRemote, 0, border=0, flag=0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstRes, 1, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT | wx.TOP)
        parent.AddSizer(self.bxsBt, 0, border=4, flag=wx.TOP | wx.ALIGN_CENTER)
        parent.AddSpacer(wx.Size(16, 8), border=0, flag=0)
        parent.AddWindow(self.cbApply, 0, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER)

    def _init_coll_lstRes_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT, heading=u'',
              width=32)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'Local'), width=-1)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'Remote'), width=-1)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=4, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.pnData.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VNETXMLSYNCHEDITRESOLVEDIALOG,
              name=u'vNetXmlSynchEditResolveDialog', parent=prnt,
              pos=wx.Point(395, 190), size=wx.Size(400, 250),
              style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vNetXmlSynch Edit Resolve')
        self.SetClientSize(wx.Size(392, 223))

        self.pnData = wx.Panel(id=wxID_VNETXMLSYNCHEDITRESOLVEDIALOGPNDATA,
              name=u'pnData', parent=self, pos=wx.Point(0, 0), size=wx.Size(392,
              223), style=wx.TAB_TRAVERSAL)

        self.lstRes = wx.ListCtrl(id=wxID_VNETXMLSYNCHEDITRESOLVEDIALOGLSTRES,
              name=u'lstRes', parent=self.pnData, pos=wx.Point(4, 4),
              size=wx.Size(384, 147), style=wx.LC_REPORT)
        self._init_coll_lstRes_Columns(self.lstRes)
        self.lstRes.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstResListColClick,
              id=wxID_VNETXMLSYNCHEDITRESOLVEDIALOGLSTRES)
        self.lstRes.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstResListItemDeselected,
              id=wxID_VNETXMLSYNCHEDITRESOLVEDIALOGLSTRES)
        self.lstRes.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstResListItemSelected,
              id=wxID_VNETXMLSYNCHEDITRESOLVEDIALOGLSTRES)
        self.lstRes.Bind(wx.EVT_LEFT_DCLICK, self.OnLstResLeftDclick)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VNETXMLSYNCHEDITRESOLVEDIALOGCBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Apply'), name=u'cbApply',
              parent=self.pnData, pos=wx.Point(144, 193), size=wx.Size(104, 30),
              style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VNETXMLSYNCHEDITRESOLVEDIALOGCBAPPLY)

        self.cbLocal = wx.lib.buttons.GenBitmapButton(ID=wxID_VNETXMLSYNCHEDITRESOLVEDIALOGCBLOCAL,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbLocal',
              parent=self.pnData, pos=wx.Point(149, 155), size=wx.Size(31, 30),
              style=0)
        self.cbLocal.Bind(wx.EVT_BUTTON, self.OnCbLocalButton,
              id=wxID_VNETXMLSYNCHEDITRESOLVEDIALOGCBLOCAL)

        self.cbRemote = wx.lib.buttons.GenBitmapButton(ID=wxID_VNETXMLSYNCHEDITRESOLVEDIALOGCBREMOTE,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbRemote',
              parent=self.pnData, pos=wx.Point(212, 155), size=wx.Size(31, 30),
              style=0)
        self.cbRemote.Bind(wx.EVT_BUTTON, self.OnCbRemoteButton,
              id=wxID_VNETXMLSYNCHEDITRESOLVEDIALOGCBREMOTE)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        self._init_ctrls(parent)
        self.doc=None
        self.lst=[]
        self.idxSel=-1
        
        self.cbApply.SetBitmapLabel(vtArt.getBitmap(vtArt.Apply))
        
        self.imgLst=wx.ImageList(16,16)
        img=img_synch_tree.getEmptyBitmap()
        self.imgLst.Add(img)
        img=img_synch_tree.getLocalBitmap()
        self.cbLocal.SetBitmapLabel(img)
        self.imgLst.Add(img)
        img=img_synch_tree.getRemoteBitmap()
        self.cbRemote.SetBitmapLabel(img)
        self.imgLst.Add(img)
        self.lstRes.SetImageList(self.imgLst,wx.IMAGE_LIST_NORMAL)
        self.lstRes.SetImageList(self.imgLst,wx.IMAGE_LIST_SMALL)
        
    def SetEdit2Resolve(self,doc,lst):
        self.idxSel=-1
        self.lstRes.DeleteAllItems()
        self.lst=lst
        l=[]
        self.doc=doc
        i=0
        for tup in lst:
            idx=self.lstRes.InsertStringItem(sys.maxint,'')
            sUnique=self.doc.getNodeText(tup['local'],self.doc.TAGNAME_REFERENCE)
            self.lstRes.SetStringItem(idx,1,sUnique)
            sUnique=self.doc.getNodeText(tup['remote'],self.doc.TAGNAME_REFERENCE)
            self.lstRes.SetStringItem(idx,2,sUnique)
            self.lstRes.SetItemData(idx,i)
            tup['valid']=0
            i+=1
            
    def OnLstResListColClick(self, event):
        self.idxSel=-1
        event.Skip()

    def OnLstResListItemDeselected(self, event):
        self.idxSel=-1
        event.Skip()

    def OnLstResListItemSelected(self, event):
        self.idxSel=event.GetIndex()
        event.Skip()

    def OnLstResLeftDclick(self, event):
        event.Skip()
        if self.idxSel<0:
            return
        iNum=self.lstRes.GetItemData(self.idxSel)
        i=self.lst[iNum]['valid']
        if i==0:
            self.lst[iNum]['valid']=1
        elif i==1:
            self.lst[iNum]['valid']=2
        else:
            self.lst[iNum]['valid']=1
        self.lstRes.SetStringItem(self.idxSel,0,'',self.lst[iNum]['valid'])
        

    def OnCbApplyButton(self, event):
        event.Skip()
        for tup in self.lst:
            if tup['valid']==0:
                return
        self.Show(False)

    def OnCbLocalButton(self, event):
        event.Skip()
        if self.idxSel<0:
            return
        iNum=self.lstRes.GetItemData(self.idxSel)
        self.lst[iNum]['valid']=1
        self.lstRes.SetStringItem(self.idxSel,0,'',self.lst[iNum]['valid'])

    def OnCbRemoteButton(self, event):
        event.Skip()
        if self.idxSel<0:
            return
        iNum=self.lstRes.GetItemData(self.idxSel)
        self.lst[iNum]['valid']=2
        self.lstRes.SetStringItem(self.idxSel,0,'',self.lst[iNum]['valid'])
