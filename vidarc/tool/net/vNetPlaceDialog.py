#Boa:Dialog:vNetPlaceDialog
#----------------------------------------------------------------------------
# Name:         vNetPlaceDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vNetPlaceDialog.py,v 1.2 2009/03/30 19:33:37 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.net.vNetPlacePanel import vNetPlacePanel
from vidarc.tool.net.vtNetXmlClt import vtNetXmlCltSock
from vidarc.tool.net.vtNetClt import vtNetClt

import images

def create(parent):
    return vNetPlaceDialog(parent)

[wxID_VNETPLACEDIALOG, wxID_VNETPLACEDIALOGCBAPPLY, 
 wxID_VNETPLACEDIALOGCBCANCEL, wxID_VNETPLACEDIALOGCBTEST, 
 wxID_VNETPLACEDIALOGLBSTATE, 
] = [wx.NewId() for _init_ctrls in range(5)]

class vNetPlaceDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VNETPLACEDIALOG,
              name=u'vNetPlaceDialog', parent=prnt, pos=wx.Point(342, 87),
              size=wx.Size(309, 196), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vNet Place Dialog')
        self.SetClientSize(wx.Size(301, 162))

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VNETPLACEDIALOGCBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Apply', name=u'cbApply',
              parent=self, pos=wx.Point(80, 128), size=wx.Size(76, 30),
              style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VNETPLACEDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VNETPLACEDIALOGCBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel', name=u'cbCancel',
              parent=self, pos=wx.Point(168, 128), size=wx.Size(76, 30),
              style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VNETPLACEDIALOGCBCANCEL)

        self.cbTest = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VNETPLACEDIALOGCBTEST,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Test', name=u'cbTest',
              parent=self, pos=wx.Point(216, 88), size=wx.Size(76, 30),
              style=0)
        self.cbTest.Bind(wx.EVT_BUTTON, self.OnCbTestButton,
              id=wxID_VNETPLACEDIALOGCBTEST)

        self.lbState = wx.StaticBitmap(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VNETPLACEDIALOGLBSTATE, name=u'lbState', parent=self,
              pos=wx.Point(192, 96), size=wx.Size(16, 16), style=0)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        self.widLogging=vtLog.GetPrintMsgWid(self)
        try:
            self.sOrigin=vtLog.vtLngGetRelevantNamesWX(self,'netPlace')
            #self.SetOrigin(sOrigin)
        except:
            vtLog.vtLngTB(self.GetName())
            #self.SetOrigin(appl)
        
        self.panel=vNetPlacePanel(self , wx.NewId(),
              wx.Point(0, 0), wx.Size(290, 90), 
              wx.TAB_TRAVERSAL, u'vgpNetPlace')
        img=images.getStoppedBitmap()
        self.lbState.SetBitmap(img)
        
        img=images.getCancelBitmap()
        self.cbCancel.SetBitmapLabel(img)
        
        img=images.getApplyBitmap()
        self.cbApply.SetBitmapLabel(img)
        
        images.getConnectBitmap()
        self.cbTest.SetBitmapLabel(img)
        
        self.cltServ=None
        self.cltSock=None
        self.bConnectionTested=False
    def GetOrigin(self):
        return self.sOrigin
    def Set(self,appl,alias,host,port,usr,passwd):
        self.panel.Set(appl,alias,host,port,usr,passwd)
    def SetNode(self,doc,node,appl):
        self.bConnectionTested=False
        self.panel.SetNode(doc,node,appl)
    def GetNode(self):
        self.panel.GetNode()
    def __close__(self):
        if self.cltSock is not None:
            self.cltSock.Close()
            self.cltSock=None
        if self.cltServ is not None:
            self.cltServ.Stop()
            del self.cltServ
        self.cltServ=None
    def ShowModal(self):
        self.bConnectionTested=False
        try:
            return wx.Dialog.ShowModal(self)
        finally:
            pass
    def OnCbApplyButton(self, event):
        self.__close__()
        if self.bConnectionTested:
            self.EndModal(2)
        else:
            self.EndModal(1)
        event.Skip()
    def OnCbCancelButton(self, event):
        self.__close__()
        self.EndModal(0)
        event.Skip()
    def OnCbTestButton(self, event):
        appl=self.panel.GetAppl()
        alias=self.panel.GetAlias()
        host=self.panel.GetHost()
        port=self.panel.GetPort()
        usr=self.panel.GetUsr()
        passwd=self.panel.GetPasswd()
        try:
            iPort=int(port)
        except:
            return
        self.cltServ=vtNetClt(self,host,iPort,vtNetXmlCltSock)
        self.cltServ.Connect()
        self.cltSock=self.cltServ.GetSocketInstance()
        if self.cltSock is None:
            return
        self.cltSock.SetParent(self)
        img=images.getLoginBitmap()
        self.lbState.SetBitmap(img)
        self.cltSock.LoginEncoded(usr,passwd)
        self.cltSock.Alias(appl,alias)
        event.Skip()
    def NotifyLogin(self):
        img=images.getLoginBitmap()
        self.lbState.SetBitmap(img)
        pass
    def NotifyLoggedIn(self,oLogin):
        img=images.getRunningBitmap()
        self.lbState.SetBitmap(img)
        self.bConnectionTested=True
        pass
    def NotifyServed(self,bFlag):
        pass
    def NotifyConnected(self):
        pass
    def __connectionClosed__(self,bEstabilshed=True):
        #del self.cltServ
        self.cltServ=None
        self.cltSock=None
        self.host=None
        self.port=None
        img=images.getStoppedBitmap()
        self.lbState.SetBitmap(img)
    
