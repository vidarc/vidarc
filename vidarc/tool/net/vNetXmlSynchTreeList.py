#----------------------------------------------------------------------------
# Name:         vNetXmlSynchTreeList.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vNetXmlSynchTreeList.py,v 1.7 2007/07/28 14:43:30 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import wx
#import wx.gizmos

from vidarc.tool.xml.vtXmlGrpAttrTreeList import *#vtXmlTreeListGrpAttr
from vidarc.tool.xml.vtXmlDiff import *

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase

import traceback
import string,types
#import fnmatch,time
#import thread,threading

import img_synch_tree

class vNetXmlSynchTreeList(vtXmlGrpAttrTreeList):
    KIND_LOCAL  = 1
    KIND_REMOTE = 2
    def __init__(self, parent, id, pos, size, style, name,
                    cols=None,
                    master=False,controller=False,
                    gui_update=False,verbose=0):
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        bSetDfts=False
        if cols==None:
            bSetDfts=True
            cols=[_(u'tag'),_(u'local'),_(u'remote')]
        vtXmlGrpAttrTreeList.__init__(self, parent, id, pos, size, style, name,cols,
                    master,controller,gui_update,verbose)
        if bSetDfts:
            self.SetColumnWidth(0, 200)
            self.SetColumnWidth(1, 80)
            self.SetColumnWidth(2, 80)
        
    def OnTcNodeTreeSelChanged(self,evt):
        vtXmlGrpAttrTreeList.OnTcNodeTreeSelChanged(self,evt)
        tn=evt.GetItem()
        if tn is not None:
            if self.ItemHasChildren(tn)==False:
                data=self.GetPyData(tn)
                if type(data)==types.TupleType:
                    self.__addSynchDiffNode__(data[0],data[1],tn)
                else:
                    node=data
                    tagName=self.doc.getTagName(node)
                    if tagName=='local':
                        self.__addSynchNode__(node,self.KIND_LOCAL,tn)
                    elif tagName=='remote':
                        self.__addSynchNode__(node,self.KIND_REMOTE,tn)
                    
    def SetDftGrouping(self):
        self.grouping=[('action','')]
        #self.label=[[('name','')],[('action','')],[(None,'')]]
        self.label=[[('name','')]]
        self.attrs=['*']
        self.bGrouping=True
        self.SetNodeInfos(['name','action','|sid'],'|sid')
    def SetAttrs(self,attrs):
        self.attrs=attrs
    def __getImgFormat__(self,g,val):
        val=string.strip(val)
        if len(g[1])>0:
            sFormat=g[1]
            if g[0]=='date':
                if sFormat=='YYYY':
                    img=self.imgDict['grp']['YYYY'][0]
                    imgSel=self.imgDict['grp']['YYYY'][0]
                    return (img,imgSel)
                elif sFormat=='MM':
                    img=self.imgDict['grp']['MM'][0]
                    imgSel=self.imgDict['grp']['MM'][0]
                    return (img,imgSel)
                elif sFormat=='DD':
                    img=self.imgDict['grp']['DD'][0]
                    imgSel=self.imgDict['grp']['DD'][0]
                    return (img,imgSel)
        if g[0]=='action':
            #if sFormat=='':
            try:
                img=self.imgDict['grp'][val][0]
                imgSel=self.imgDict['grp'][val][0]
                return (img,imgSel)
            except:
                img=self.imgDict['grp']['dft'][0]
                imgSel=self.imgDict['grp']['dft'][0]
                return (img,imgSel)
        elif g[0]=='attr':
            try:
                img=self.imgDict['attr'][val][0]
                imgSel=self.imgDict['attr'][val][0]
            except:
                img=self.imgDict['attr']['dft'][0]
                imgSel=self.imgDict['attr']['dft'][0]
            return (img,imgSel)
                
        tagName=g[0]
        try:
            img=self.imgDict['elem'][tagName][0]
        except:
            img=self.imgDict['elem']['dft'][0]
        try:
            imgSel=self.imgDict['elem'][tagName][1]
        except:
            imgSel=self.imgDict['elem']['dft'][1]
        return (img,imgSel)
    def SetupImageList(self):
        self.imgDict={'elem':{},'attr':{},'txt':{},'cdata':{},'comm':{},'grp':{}}
        self.imgLstTyp=wx.ImageList(16,16)
        img=img_synch_tree.getEmptyBitmap()
        self.imgDict['elem']['dft']=[self.imgLstTyp.Add(img)]
        
        img=img_synch_tree.getEmptyBitmap()
        self.imgDict['elem']['dft'].append(self.imgLstTyp.Add(img))
        
        img=img_synch_tree.getSrvSynchBitmap()
        i=self.imgLstTyp.Add(img)
        self.imgDict['elem']['data']=[i,i]
        
        img=img_synch_tree.getSynchBitmap()
        i=self.imgLstTyp.Add(img)
        self.imgDict['elem']['synch']=[i,i]
        
        img=img_synch_tree.getNotEqualBitmap()
        i=self.imgLstTyp.Add(img)
        self.imgDict['elem']['diff']=[i,i]
        
        img=img_synch_tree.getLocalBitmap()
        i=self.imgLstTyp.Add(img)
        self.imgDict['elem']['local']=[i,i]
        
        img=img_synch_tree.getRemoteBitmap()
        i=self.imgLstTyp.Add(img)
        self.imgDict['elem']['remote']=[i,i]
        
        img=img_synch_tree.getSynchConfBitmap()
        i=self.imgLstTyp.Add(img)
        self.imgDict['elem']['conflict']=[i,i]
        
        img=img_synch_tree.getSynchToSrvBitmap()
        i=self.imgLstTyp.Add(img)
        self.imgDict['elem']['editSrv']=[i,i]
        
        img=img_synch_tree.getSynchToCltBitmap()
        i=self.imgLstTyp.Add(img)
        self.imgDict['elem']['editClt']=[i,i]
        
        img=img_synch_tree.getAttributeBitmap()
        self.imgDict['elem']['attr']=[self.imgLstTyp.Add(img)]
        img=img_synch_tree.getAttributeSelBitmap()
        self.imgDict['elem']['attr'].append(self.imgLstTyp.Add(img))
        
        img=img_synch_tree.getUndefBitmap()
        i=self.imgLstTyp.Add(img)
        self.imgDict['grp']['response']=[i,i]
        
        img=img_synch_tree.getUndefBitmap()
        i=self.imgLstTyp.Add(img)
        self.imgDict['grp']['dft']=[i,i]
        
        img=img_synch_tree.getEqualBitmap()
        i=self.imgLstTyp.Add(img)
        self.imgDict['grp']['identical']=[i,i]
        
        img=img_synch_tree.getEditBitmap()
        i=self.imgLstTyp.Add(img)
        self.imgDict['grp']['edit']=[i,i]
        
        img=img_synch_tree.getDelBitmap()
        i=self.imgLstTyp.Add(img)
        self.imgDict['grp']['del']=[i,i]
        
        img=img_synch_tree.getAddBitmap()
        i=self.imgLstTyp.Add(img)
        self.imgDict['grp']['add']=[i,i]
        
        img=img_synch_tree.getAddBitmap()
        i=self.imgLstTyp.Add(img)
        self.imgDict['grp']['add_offline']=[i,i]
        
        img=img_synch_tree.getSynchToSrvBitmap()
        i=self.imgLstTyp.Add(img)
        self.imgDict['grp']['edit_offline']=[i,i]
        
        img=img_synch_tree.getLocalBitmap()
        i=self.imgLstTyp.Add(img)
        self.imgDict['grp']['local']=[i,i]
        
        img=img_synch_tree.getRemoteBitmap()
        i=self.imgLstTyp.Add(img)
        self.imgDict['grp']['remote']=[i,i]
        
        img=img_synch_tree.getEmptyBitmap()
        i=self.imgLstTyp.Add(img)
        self.imgDict['attr']['dft']=[i,i]
        
        img=img_synch_tree.getIdBitmap()
        i=self.imgLstTyp.Add(img)
        self.imgDict['attr']['id']=[i,i]
        
        img=img_synch_tree.getIdInstanceBitmap()
        i=self.imgLstTyp.Add(img)
        self.imgDict['attr']['iid']=[i,i]
        
        img=img_synch_tree.getIdForeignBitmap()
        i=self.imgLstTyp.Add(img)
        self.imgDict['attr']['fid']=[i,i]
        
        img=img_synch_tree.getIdReferenceBitmap()
        i=self.imgLstTyp.Add(img)
        self.imgDict['attr']['ih']=[i,i]
        
        img=img_synch_tree.getFingerprintBitmap()
        i=self.imgLstTyp.Add(img)
        self.imgDict['attr']['fp']=[i,i]
        
        img=img_synch_tree.getExpandBitmap()
        i=self.imgLstTyp.Add(img)
        self.imgDict['attr']['expand']=[i,i]
        
        #img=prjtimer_tree_images.getPrjBitmap()
        #self.imgDict['elem']['project']=[self.imgLstTyp.Add(img)]
        #img=prjtimer_tree_images.getPrjSelBitmap()
        #self.imgDict['elem']['project'].append(self.imgLstTyp.Add(img))
        
        self.SetImageList(self.imgLstTyp)
        pass
    
    def __addSynchDiffAttr__(self,lst,tip):
        img=self.imgDict['elem']['attr'][0]
        tAttr=self.AppendItem(tip,self.attrTreeItemName,-1,-1,None)
        self.SetItemImage(tAttr,img,0,wx.TreeItemIcon_Normal)
        self.SetItemImage(tAttr,img,0,wx.TreeItemIcon_Selected)
        for attrVal in lst:
            strs=string.split(attrVal,'=')
            name=strs[0]
            if len(strs)>1:
                val=strs[1]
            else:
                val=''
            tn=self.AppendItem(tAttr,name,-1,-1,None)
            img,imgSel=self.__getImgFormat__(('attr',''),name)
            self.SetItemImage(tn,img,0,wx.TreeItemIcon_Normal)
            self.SetItemImage(tn,imgSel,0,wx.TreeItemIcon_Selected)
            self.SetItemText(tn,val,1)
            self.SetItemText(tn,val,2)
        
    def __addSynchDiff__(self,xmlDiff,dRes,tip):
        #vtLog.CallStack('')
        # add different attributes to tree
        d=dRes[xmlDiff.ATTR]
        keys=d.keys()
        keys.sort()
        if len(keys)>0:
            bFound=False
            triChild=self.GetFirstChild(tip)
            while triChild[0].IsOk():
                if self.attrTreeItemName==self.GetItemText(triChild[0]):
                    bFound=True
                    tAttr=triChild[0]
                    break
                triChild=self.GetNextChild(tip,triChild[1])
            if bFound==False:
                img=self.imgDict['elem']['attr'][0]
                tAttr=self.AppendItem(tip,self.attrTreeItemName,-1,-1,None)
                self.SetItemImage(tAttr,img,0,wx.TreeItemIcon_Normal)
                self.SetItemImage(tAttr,img,0,wx.TreeItemIcon_Selected)
        for k in keys:
            tn=self.AppendItem(tAttr,k,-1,-1,None)
            img,imgSel=self.__getImgFormat__(('attr',''),k)
            self.SetItemImage(tn,img,0,wx.TreeItemIcon_Normal)
            self.SetItemImage(tn,imgSel,0,wx.TreeItemIcon_Selected)
            self.SetItemText(tn,d[k][0][1],1)
            self.SetItemText(tn,d[k][1][1],2)
        # add different infos to tree
        d=dRes[xmlDiff.INFO]
        keys=d.keys()
        keys.sort()
        for k in keys:
            strs=string.split(k,'|')
            tn=self.AppendItem(tip,strs[0],-1,-1,None)
            if len(strs)>1:
                self.__addSynchDiffAttr__(strs[1:],tn)
            self.SetItemText(tn,d[k][0][1],1)
            self.SetItemText(tn,d[k][1][1],2)
        # add different childs to tree
        d=dRes[xmlDiff.CHILDS]
        keys=d.keys()
        keys.sort()
        for k in keys:
            strs=string.split(k,'|')
            tn=self.AppendItem(tip,strs[0],-1,-1,None)
            if len(strs)>1:
                self.__addSynchDiffAttr__(strs[1:],tn)
            self.__addSynchDiff__(xmlDiff,d[k],tn)
    def __addSynchDiffNode__(self,localNode,remoteNode,tip):
        #vtLog.CallStack('')
        try:
            xmlDiff=vtXmlDiff(self.doc)
            xmlDiff.AddInfo(localNode)
            xmlDiff.AddInfo(remoteNode)
            xmlDiff.CalcDiff()
            dRes=xmlDiff.GetDiff()
            self.__addSynchDiff__(xmlDiff,dRes,tip)
        except:
            vtLog.vtLngTB('')
            vtLog.vtLngCS(vtLog.ERROR,dRes)
    def __addSynchNode__(self,node,kind,tip):
        for c in self.doc.getChilds(node):
            tagName=self.doc.getTagName(c)
            img=self.imgDict['elem']['dft'][0]
            tn=self.AppendItem(tip,tagName,-1,-1,None)
            self.SetItemImage(tn,img,0,wx.TreeItemIcon_Normal)
            self.SetItemImage(tn,img,0,wx.TreeItemIcon_Selected)
            sVal=self.doc.getText(c)
            if len(string.strip(sVal))>0:
                self.SetItemText(tn,sVal,kind)
            #self.SetItemText(tn,tagName,2)
            if self.doc.hasAttribute(c,None):
                img=self.imgDict['elem']['attr'][0]
                tAttr=self.AppendItem(tn,self.attrTreeItemName,-1,-1,None)
                self.SetItemImage(tAttr,img,0,wx.TreeItemIcon_Normal)
                self.SetItemImage(tAttr,img,0,wx.TreeItemIcon_Selected)
                self.doc.procAttrs(c,self.__addAttr__,tAttr,kind)
            self.__addSynchNode__(c,kind,tn)
    def __addAttr__(self,node,name,val,*args):
        tip=args[0][0]
        kind=args[0][1]
        tn=self.AppendItem(tip,name,-1,-1,None)
        img,imgSel=self.__getImgFormat__(('attr',''),name)
        self.SetItemImage(tn,img,0,wx.TreeItemIcon_Normal)
        self.SetItemImage(tn,imgSel,0,wx.TreeItemIcon_Selected)
        self.SetItemText(tn,val,kind)
        return 0
    def __addNodeAttr__(self,node,tip):
        if tip is None:
            return
        
        localNode=self.doc.getChild(node,'local')
        remoteNode=self.doc.getChild(node,'remote')
        bLocal=self.doc.hasChilds(localNode)
        bRemote=self.doc.hasChilds(remoteNode)
        
        if bLocal and bRemote:
            img,imgSel=self.__getImgFormat__(('diff',''),'diff')
            tid=wx.TreeItemData()
            tmpLocal=self.doc.getChilds(localNode)[0]
            tmpRemote=self.doc.getChilds(remoteNode)[0]
            tid.SetData((tmpLocal,tmpRemote))
            tTmp=self.AppendItem(tip,_(u'difference'),-1,-1,tid)
            self.SetItemImage(tTmp,img,0,wx.TreeItemIcon_Normal)
            self.SetItemImage(tTmp,imgSel,0,wx.TreeItemIcon_Expanded)
            self.SetItemImage(tTmp,imgSel,0,wx.TreeItemIcon_Selected)
        
        if bLocal:
            img,imgSel=self.__getImgFormat__(('local',''),'local')
            tid=wx.TreeItemData()
            tid.SetData(localNode)
            tTmp=self.AppendItem(tip,_(u'local'),-1,-1,tid)
            self.SetItemImage(tTmp,img,0,wx.TreeItemIcon_Normal)
            self.SetItemImage(tTmp,imgSel,0,wx.TreeItemIcon_Expanded)
            self.SetItemImage(tTmp,imgSel,0,wx.TreeItemIcon_Selected)
                
        if bRemote:
            img,imgSel=self.__getImgFormat__(('remote',''),'remote')
            tid=wx.TreeItemData()
            tid.SetData(remoteNode)
            tTmp=self.AppendItem(tip,_(u'remote'),-1,-1,tid)
            self.SetItemImage(tTmp,img,0,wx.TreeItemIcon_Normal)
            self.SetItemImage(tTmp,imgSel,0,wx.TreeItemIcon_Expanded)
            self.SetItemImage(tTmp,imgSel,0,wx.TreeItemIcon_Selected)
        
