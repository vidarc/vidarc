#Boa:FramePanel:vtNetXmlSynchShowEntryPanel

import wx
import vidarc.tool.xml.vtXmlGrpAttrTreeList
import vidarc.tool.xml.vtXmlTree

[wxID_VTNETXMLSYNCHSHOWENTRYPANEL, wxID_VTNETXMLSYNCHSHOWENTRYPANELTRENTRY, 
] = [wx.NewId() for _init_ctrls in range(2)]

class vtNetXmlSynchShowEntryPanel(wx.Panel):
    def _init_coll_fgsShow_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_fgsShow_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.trEntry, 0, border=0, flag=wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsShow = wx.FlexGridSizer(cols=1, hgap=0, rows=1, vgap=0)

        self._init_coll_fgsShow_Items(self.fgsShow)
        self._init_coll_fgsShow_Growables(self.fgsShow)

        self.SetSizer(self.fgsShow)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTNETXMLSYNCHSHOWENTRYPANEL,
              name=u'vtNetXmlSynchShowEntryPanel', parent=prnt,
              pos=wx.Point(176, 176), size=wx.Size(431, 267),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(423, 240))

        self.trEntry = vidarc.tool.xml.vtXmlGrpAttrTreeList.vtXmlGrpAttrTreeList(cols=[('tag',-0.4),
              ('value',-0.6)], controller=False,
              id=wxID_VTNETXMLSYNCHSHOWENTRYPANELTRENTRY, master=False,
              name=u'trEntry', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(423, 240), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        self.trEntry.SetLevelAutoBuild(0)
    def ShowNode(self,doc,node):
        #print node
        self.trEntry.SetDoc(doc)
        self.trEntry.SetNode(node)
