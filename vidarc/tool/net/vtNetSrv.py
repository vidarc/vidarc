#----------------------------------------------------------------------------
# Name:         vNetSrv.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtNetSrv.py,v 1.20 2010/03/03 02:17:11 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import thread,threading
import time,random
import socket,sha,binascii
import select
import traceback
from Queue import Queue

import sys,string,os
from vidarc.tool.log import vtLog
#from vidarc.tool.net import vtNetSock
from vidarc.tool.vtThreadCore import vtThreadCore
from vidarc.tool.sock.vtSock import vtSock,hasSSL,isSSL
from vidarc.tool.xml.vtXmlAcl import vtXmlLoginInfo
import vSystem

if hasSSL():
    from OpenSSL import SSL
    from OpenSSL import tsafe
    from OpenSSL import crypto
    import certgen 
    #import select
    def verify_cb(conn, cert, errnum, depth, ok):
        try:
            #vtLog.CallStack('')
            # This obviously has to be updated
            #traceback.print_stack()
            #print 'errnum:%s;depth:%s;ok:%s'%(`errnum`,`depth`,`ok`)
            #self=conn.get_app_data()
            ctx=conn.get_context()
            self=ctx.get_app_data()
            
            #vtLog.vtLngCurCls(vtLog.INFO,'Got certificate: %s' % cert.get_subject(),self)
            #vtLog.vtLngCurCls(vtLog.ERROR,'errnum:%s;depth:%s;ok:%s'%(`errnum`,`depth`,`ok`),self)
            #if ok:
            #    self.iVerify=1
            #else:
            #    self.iVerify=-1
            #self.semVerify.release()
            self.DoVerify(conn,cert,errnum,depth,ok)
        except:
            return False
        return ok

FORCE_SHUTDOWN=20

SESSION_ID_CHARS=string.ascii_letters+string.digits
SESSION_ID_CHARS_LEN=len(SESSION_ID_CHARS)

class vtServiceSock(vtSock):
    def __init__(self,server,conn,adr,sID=''):
        vtSock.__init__(self,server,conn,adr,sID)
        self.loggedin=False
        self.objLogin=None
        self.par=None
        self.cmds=[('login',self.__login__),('loggedin',self.__loggedin__),
            ('connections',self.__conn__),
            (u'sessionID',self.__sessionID__),
            (u'data_connections',self.__datConn__),
            (u'shutdown',self.__shutdown__)]
    def IsLoggedIn(self):
        return self.loggedin
    def SocketClosed(self):
        self.SetParent(None)
        del self.cmds
        self.objLogin=None
        vtSock.SocketClosed(self)
    def HandleData(self):
        #vtLog.vtLog(self,'HandleDate')
        iStart=string.find(self.data,self.startMarker)
        if iStart>=0:
            # start marker found
            iEnd=string.find(self.data,self.endMarker,iStart)
            if iEnd>0:
                # end marker found, telegram is complete
                iStart+=self.iStartMarker
                val=self.data[iStart:iEnd]
                #vtLog.vtLog(self,'  found',val)
                for cmd in self.cmds:
                    k=cmd[0]
                    iLen=len(k)
                    if val[:iLen]==k:
                        cmd[1](val[iLen:])
                        break
                #if val=='connections':
                #    self.__conn__(val)
                #if val=='data_connections':
                #    self.__datConn__(val)
                #else:
                #    pass
                iEnd+=self.iEndMarker
                self.data=self.data[iEnd:]
                self.len-=iEnd
                return 1
        return 0
    def __checkLogin__(self):
        if self.srv.passwd is not None:
            if self.loggedin:
                return 1
            else:
                self.conn.send(self.startMarker+'login'+self.endMarker)
                return 0
        else:
            self.loggedin=True
            return 1
    def __login__(self,val):
        if self.srv.passwd is None:
            self.conn.send(self.startMarker+'loggedin'+self.endMarker)
            self.loggedin=True
        else:
            passwd=binascii.a2b_uu(val[1:])
            m5=sha.new()
            m5.update(self.srv.passwd)
            m5.update(self.sessionID)
            if m5.digest()==passwd:
                self.conn.send(self.startMarker+'loggedin'+self.endMarker)
                self.loggedin=True
            else:
                self.conn.send(self.startMarker+'login'+self.endMarker)
    def __sessionID__(self,val):
        pass
    def __conn__(self,val):
        if self.__checkLogin__():
            sVal=self.srv.GetConnectionsStr()
            if len(sVal)>0:
                answer='connections|'+sVal
                self.conn.send(self.startMarker+answer+self.endMarker)
    def __datConn__(self,val):
        if self.__checkLogin__():
            sVal=self.srv.srv.srvData.GetConnectionsStr()
            if len(sVal)>0:
                answer='data_connections|'+sVal
                self.conn.send(self.startMarker+answer+self.endMarker)
            else:
                self.conn.send(self.startMarker+'data_connections'+self.endMarker)
    def __loggedin__(self,val):
        pass
    def __shutdown__(self,val):
        if self.__checkLogin__():
            self.conn.send(self.startMarker+'shutdown'+self.endMarker)
            time.sleep(5)
            self.srv.srv.Stop()
class vtServiceCltSock(vtServiceSock):
    def Login(self):
        print 'login'
        print 'enter passwd'
        s=raw_input()
        if len(s)>0:
            m=sha.new()
            m.update(s)
            m.update(self.sessionID)
            s=binascii.b2a_uu(m.digest())
            self.Send(self.startMarker+s+self.endMarker)
        else:
            s=''
        self.Send(self.startMarker+'login|'+s+self.endMarker)
    def GetConnections(self):
        self.Send(self.startMarker+'connections'+self.endMarker)
    def GetDataConnections(self):
        self.Send(self.startMarker+'data_connections'+self.endMarker)
    def ShutDown(self):
        self.Send(self.startMarker+'shutdown'+self.endMarker)
    def __sessionID__(self,val):
        self.sessionID=val[1:]
    def __login__(self,val):
        print 'login'
        print 'enter passwd'
        s=raw_input()
        if len(s)>0:
            sHex=sha.new()
            sHex.update(s)
            m=sha.new()
            m.update(sHex.hexdigest())
            m.update(self.sessionID)
            s=binascii.b2a_uu(m.digest())
        else:
            s=''
        #self.Send(self.startMarker+m.digest()+self.endMarker)
        self.Send(self.startMarker+'login|'+s+self.endMarker)
    def __loggedin__(self,val):
        #print 'logged in'
        self.loggedin=True
    def __conn__(self,val):
        i=string.find(val,'|')
        if i>=0:
            lst=string.split(val[i+1:],',')
            for s in lst:
                print s
    def __datConn__(self,val):
        i=string.find(val,'|')
        if i>=0:
            lst=string.split(val[i+1:],',')
            for s in lst:
                print s
    def __shutdown__(self,val):
        #print 'server is shutting down'
        pass

class vtSrvSock(vtThreadCore):
    #SSL=True
    ZWAIT_SELECT=0.1
    ZWAIT_SELECT_ACTIVE=0.5
    SELECT_FD_SIZE=500
    def __init__(self,server,port,socketClass,verbose=0,origin=None,SSL=None):
        vtThreadCore.__init__(self,verbose=verbose)
        self.srv=server
        self.port=port
        self.serving=False
        self.socketClass=socketClass
        self.par=None
        self.passwd=None
        self.connections={}
        self.lRd=[]
        self.dConn={}
        self.lWr=[]
        self.qConn=Queue()
        
        self.iVerify=0
        if SSL is None:
            self.SSL=isSSL()
        else:
            self.SSL=SSL
        self.__initSSL__()
    def __initSSL__(self,privkey=None,cert=None,verify=None,DN=None):
        #vtLog.CallStack('')
        try:
            if self.SSL:
                #try:
                #    self.ctx=__init__.CTX
                #    return self.ctx
                #except:
                #    pass
                #DN='v:/python/vMESSrv/'
                DN=vSystem.getUsrCfgDN('VIDARC')
                if DN is None:
                    DN=os.getcwd()
                if privkey is None:
                    privkey='vMESSrv.pkey'
                if cert is None:
                    cert='vMESSrv.cert'
                if verify is None:
                    verify='CA.cert'
                try:
                    DNcert=os.path.split(sys.argv[0])[0]
                except:
                    DNcert=DN
                DNcert=DN
                vtLog.vtLngCurCls(vtLog.INFO,'dn:%s;priv:%s;cert:%s;verify:%s'%(DN,privkey,cert,verify),self)
                if os.path.exists(os.path.join(DN, privkey))==False:
                    certgen.mkClientCert(DN,vSystem.getHostFull(),
                            [('vMESSrv','Server')])
                    if 0:
                        try:
                            os.makedirs(DN)
                        except:
                            pass
                        try:
                            sHost=platform.uname()[1]
                        except:
                            sHost=''
                        cakey = certgen.createKeyPair(certgen.TYPE_RSA, 1024)
                        #careq = certgen.createCertRequest(cakey, CN='Certificate Authority')
                        careq = certgen.createCertRequest(cakey, CN='VIDARC MES')
                        cacert = certgen.createCertificate(careq, (careq, cakey), 0, (0, 60*60*24*365*5)) # five years
                        open(os.path.join(DN,'CA.pkey'), 'w').write(crypto.dump_privatekey(crypto.FILETYPE_PEM, cakey))
                        open(os.path.join(DN,'CA.cert'), 'w').write(crypto.dump_certificate(crypto.FILETYPE_PEM, cacert))
                        for (fname, cname) in [('vMESSrv', 'VIDARC MES Server (%s)'%(sHost))]:
                            pkey = certgen.createKeyPair(certgen.TYPE_RSA, 1024)
                            req = certgen.createCertRequest(pkey, CN=cname)
                            certGen = certgen.createCertificate(req, (cacert, cakey), 1, (0, 60*60*24*365*5)) # five years
                            open('%s.pkey' % (os.path.join(DN,fname),), 'w').write(crypto.dump_privatekey(crypto.FILETYPE_PEM, pkey))
                            open('%s.cert' % (os.path.join(DN,fname),), 'w').write(crypto.dump_certificate(crypto.FILETYPE_PEM, certGen))
                self.ctx = SSL.Context(SSL.SSLv23_METHOD)
                self.ctx.set_options(SSL.OP_NO_SSLv2)
                #self.ctx.set_app_data(self)
                self.ctx.set_verify(SSL.VERIFY_PEER|SSL.VERIFY_FAIL_IF_NO_PEER_CERT, verify_cb) # Demand a certificate
                self.ctx.use_privatekey_file (os.path.join(DN, privkey))
                self.ctx.use_certificate_file(os.path.join(DN, cert))
                self.ctx.load_verify_locations(os.path.join(DNcert, verify))
                #__init__.CTX=self.ctx
            else:
                self.ctx=None
        except:
            self.ctx=None
            vtLog.vtLngTB(self.__class__.__name__)
    def DoVerify(self,conn, cert, errnum, depth, ok):
        vtLog.vtLngCurCls(vtLog.INFO,'ok:%d'%(ok),self)
        if ok:
            self.serving=True
            vtLog.vtLngCurCls(vtLog.INFO,'SSL got certificate: %s'%(cert.get_subject()),self)
        else:
            self.serving=False
            vtLog.vtLngCurCls(vtLog.ERROR,'errnum:%s;depth:%s;ok:%s'%(`errnum`,`depth`,`ok`),self)
        if self.serving==False:
            vtLog.vtLngCurCls(vtLog.INFO,'SSL verify fault',self)
    def verify_cb_old(self,conn, cert, errnum, depth, ok):
        # This obviously has to be updated
        vtLog.vtLngCurCls(vtLog.INFO,'Got certificate: %s' % cert.get_subject(),self)
        vtLog.vtLngCurCls(vtLog.ERROR,'errnum:%s;depth:%s;ok:%s'%(`errnum`,`depth`,`ok`),self)
        if ok:
            pass
            self.iVerify=1
        else:
            self.iVerify=-1
        #self.semVerify.release()
        return ok

        #vtLog.vtLngCurCls(vtLog.INFO,'instance on %s created'%((str(port))),self)
    def SetPar(self,par):
        self.par=par
    def GenerateSessionID(self):
        sessionID=''
        for k in xrange(32):
            for l in [0,1,2,3]:
                f=random.random()
                s=repr(f)[2:]
                iLen=len(s)
                if iLen<16:
                    s+='4539869453023104356456983145'[:16-iLen]
                i=0
                while i<16:
                    j=int(s[i:i+2],16)%SESSION_ID_CHARS_LEN
                    sessionID+=SESSION_ID_CHARS[j]
                    i+=2
            if len(sessionID)==32:
                break
            else:
                sessionID=''
        return sessionID
    def SetPasswd(self,passwd):
        if self.passwd!=passwd:
            self.Close()
        self.passwd=passwd
    def Start(self,bUseSelect=False):
        vtLog.vtLngCurCls(vtLog.INFO,'',self)
        try:
            if self.IsServing():
                vtLog.vtLngCur(vtLog.ERROR,'already served',self.GetOrigin())
            #if self.serving:
                return
            #if self.stopping:
            #    return
            if self.Is2Stop():
                vtLog.vtLngCur(vtLog.ERROR,'is2Stop active',self.GetOrigin())
                return
            #self.serving=True
            #self.connections={}
            #self.stopping=False
            if self.SSL:
                #self.thdID=thread.start_new_thread(self.RunSSL, ())
                if bUseSelect and 0:
                    self.Do(self.RunSelectSSL)
                else:
                    self.Do(self.RunSSL)
            else:
                #self.thdID=thread.start_new_thread(self.Run, ())
                if bUseSelect:
                    self.Do(self.RunSelect)
                else:
                    self.Do(self.Run)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def Stop(self):
        vtLog.vtLngCurCls(vtLog.INFO,'',self)
        #self.stopping=True
        vtThreadCore.Stop(self)
        try:
            for v in self.connections.values():
                try:
                    v.Stop()
                except:
                    vtLog.vtLngTB(self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
        try:
            pass
            #self.stopping=True
            #self.socket.close()
            #while self.serving:
            #    time.sleep(1)
            #    print self.serving,self.stopping
            #self.socket.shutdown(socket.SHUT_RDWR)
            #thread.exit_thread(self.thdID)
            #vtLog.vtLngCallStack(self,#vtLog.INFO,'Stop',origin='vtSrvSock')
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def Close(self):
        vtLog.vtLngCurCls(vtLog.INFO,'',self)
        try:
            for v in self.connections.values():
                try:
                    v.Close()
                except:
                    vtLog.vtLngTB(self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.connections={}
        self.serving=False
    def IsServing(self):
        self._acquire()
        try:
            return self.serving
        finally:
            self._release()
        return self.IsRunning()
        return self.serving
    def IsBusy(self):
        return False
    def __removeSock__(self,cli):
    #def __removeSock__(self,client):
        try:
            vtLog.vtLngCur(vtLog.INFO,'remove:%d'%(cli),self.GetOrigin())
            #cli=client.fileno()
            for lRd in self.lRd:
                if cli in lRd:
                    vtLog.vtLngCur(vtLog.INFO,'remove from lRd'%(),self.GetOrigin())
                    lRd.remove(cli)
                    break
            self.lRd=[lRd for lRd in self.lRd if len(lRd)>0]
            # compress lists
            iL=len(self.lRd)
            for i in xrange(iL-1,0,-1):
                iL1=len(self.lRd[i])
                iL2=len(self.lRd[i-1])
                if (iL1+iL2)<self.SELECT_FD_SIZE:
                    self.lRd[i-1]=self.lRd[i-1]+self.lRd[i]
                    del self.lRd[i]
        except:
            vtLog.vtLngTB(self.GetOrigin())
        try:
            if cli in self.dConn:
                del self.dConn[cli]
        except:
            vtLog.vtLngTB(self.GetOrigin())
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'lRd:%s;dConn:%s'%(vtLog.pformat(self.lRd),vtLog.pformat(self.dConn)),self.GetOrigin())
    def removeSock(self,sock,cli):
        vtLog.vtLngCur(vtLog.INFO,'close connection'%(),self.GetOrigin())
        try:
            sock.SocketClose()
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def RemoveSock(self,sock,cli):
        vtLog.vtLngCur(vtLog.INFO,'close connection'%(),self.GetOrigin())
        self._acquire()
        #cli=client.fileno()
        #self.__removeSock__(cli)
        #self.__removeSock__(cli)
        self.qConn.put((self.__removeSock__,(cli,),{}))
        self.qConn.put((self.removeSock,(sock,cli,),{}))
        self._release()
    def StopSocket(self,adr,sockInst):
        try:
            vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
            #vtLog.vtLngCallStack(self,#vtLog.INFO,'StopSocket by %s'%str(adr),origin='vtSrvSock')
            sockInst=self.connections[adr]
            client=sockInst.GetSock()
            cli=sockInst.GetSockFD()
            self.qConn.put((self.__removeSock__,(cli,),{}))
            #self.qConn.put(self.RemoveSock,client,cli)
            #self.__removeSock__(cli)
            #try:
            #    self.lRd.remove(cli)
            #except:
                #vtLog.vtLngTB(self.GetOrigin())
            #    pass
            try:
                del self.dConn[cli]
            except:
                #vtLog.vtLngTB(self.GetOrigin())
                pass
            if adr in self.connections:
                del self.connections[adr]
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def ShutDown(self):
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
        self.srv=None
    def __accept__(self):
        try:
            conn, addr = self.socket.accept()
            vtLog.vtLngCur(vtLog.INFO,'incomming connection;class:%s'%(
                            self.socketClass),self.GetOrigin())
            vtLog.vtLngCur(vtLog.INFO,'Connected by %s'%str(addr),
                            self.GetOrigin())
            sID=self.GenerateSessionID()
            vtLog.vtLngCur(vtLog.INFO,'sessionID:%s'%(sID),self.GetOrigin())
            vtLog.vtLngCur(vtLog.INFO,'sock inst:%s'%(self.socketClass),self.GetOrigin())
            sock=self.socketClass(self,conn,addr,sID,
                            bUseSelect4Comm=True)
            #self.dConn[conn]=sock
            fd=conn.fileno()
            self.dConn[fd]=sock
            vtLog.vtLngCur(vtLog.INFO,'fd:%d'%(fd),self.GetOrigin())
            #print 'conn',conn,sock
            #self.lRd.append(conn)
            l=None
            for lRdTmp in self.lRd:
                if len(lRdTmp)<self.SELECT_FD_SIZE:
                    l=lRdTmp
                    break
            if l is None:
                #l=[conn]
                l=[fd]
                self.lRd.append(l)
            else:
                #l.append(conn)
                l.append(fd)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'lRd:%s;dConn:%s'%(vtLog.pformat(self.lRd),vtLog.pformat(self.dConn)),self.GetOrigin())
            #vtLog.vtLngCur(vtLog.INFO,'sock inst:%s'%(self.socketClass),self.GetOrigin())
        except socket.timeout:
            pass
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def RunSelect(self):
        vtLog.vtLngCur(vtLog.DEBUG,'',self.GetOrigin())
        self._acquire()
        self.serving=True
        self.connections={}
        self._release()
        HOST=None#'localhost'
        vtLog.vtLngCur(vtLog.INFO,'Run on port %d'%self.port,self.GetOrigin())
        for res in socket.getaddrinfo(HOST, self.port, socket.AF_UNSPEC, socket.SOCK_STREAM, 0, socket.AI_PASSIVE):
            #vtLog.vtLngCurCls(vtLog.INFO,repr(res),self)
            af, socktype, proto, canonname, sa = res
            try:
                self.socket = socket.socket(af, socktype, proto)
            except socket.error, msg:
                self.socket = None
                continue
            try:
                self.socket.bind(sa)
                self.socket.listen(3)
            except socket.error, msg:
                self.socket.close()
                self.socket = None
                continue
            break
            #if s is None:
            #    print 'could not open socket'
                #sys.exit(1)
            #print s
        if self.socket is None:
            self.serving=False
            vtLog.vtLngCur(vtLog.INFO,'socket is None',self.GetOrigin())
            return
        #self.socket.setblocking(2.0)
        #self.socket.setblocking(0.0)
        self.socket.settimeout(2.0)
        self._acquire()
        fdSrv=self.socket.fileno()
        self.lRd=[[fdSrv]]
        self.dConn={} #{fdSrv:self}
        srv=self.socket
        self.lWr=[]
        self._release()
        lEvt=[]
        lWr=[]
        while self.IsServing():
            while self.qConn.empty()==False:
                try:
                    f,args,kwargs=self.qConn.get()
                    #print f,args,kwargs
                    f(*args,**kwargs)
                except:
                    vtLog.vtLngTB(self.GetOrigin())
            if self.Is2Stop():
                break
            try:
                for lRd in self.lRd:
                    try:
                        #lRdRdy,lWrRdy , lEvtRdy = select.select(lRd, lWr , lEvt, self.ZWAIT_SELECT)
                        lRdRdy,lWrRdy , lEvtRdy = select.select(lRd, [] , lEvt, self.ZWAIT_SELECT)
                    except socket.timeout,msg:
                        #print 'timeout'
                        #self.CheckPause()
                        continue
                    except:
                        vtLog.vtLngTB(self.GetOrigin())
                        #break
                    #vtLog.vtLngCur(vtLog.DEBUG,'rd:%d;wr:%d'%(len(lRdRdy),len(lWrRdy)),self.GetOrigin())
                    for cli in lRdRdy:
                        #if cli == srv:
                        if cli == fdSrv:
                            self.qConn.put((self.__accept__,(),{}))
                        else:
                            if cli in self.dConn:
                                sock=self.dConn[cli]
                                #print 'recv',cli,sock,sock.IsServing()
                                if sock.doRecv()<0:
                                    self.RemoveSock(sock,cli)
                            else:
                                vtLog.vtLngCur(vtLog.ERROR,'fd:%d;lRd:%s;dConn:%s'%(cli,
                                            vtLog.pformat(lRd),
                                            vtLog.pformat(self.dConn)),self.GetOrigin())
            except:
                vtLog.vtLngTB(self.GetOrigin())
            try:
                lWr=[]
                iCount=0
                def selectWr(lWr):
                    lRdRdy,lWrRdy , lEvtRdy = select.select([], lWr , [], 0)#self.ZWAIT_SELECT)
                    for cli in lWrRdy:
                        if cli in self.dConn:
                            sock=self.dConn[cli]
                            #print 'recv',cli,sock,sock.IsServing()
                            if sock.doSend()<0:
                                self.RemoveSock(sock,cli)
                        else:
                            vtLog.vtLngCur(vtLog.ERROR,'cli:%d;lRd:%s;dConn:%s'%(cli,
                                            vtLog.pformat(lRd),
                                            vtLog.pformat(self.dConn)),self.GetOrigin())
                for fdCli,sock in self.dConn.iteritems():
                    if sock.Is2Snd():
                        lWr.append(fdCli)
                        iCount+=1
                    if iCount>32:
                        selectWr(lWr)
                        iCount=0
                        lWr=[]
                if iCount>0:
                    selectWr(lWr)
            except:
                vtLog.vtLngTB(self.GetOrigin())
        self._acquire()
        try:
            #del self.dConn[srv]
            #del self.dConn[fdSrv]
            l=self.dConn.values()
        except:
            vtLog.vtLngTB(self.GetOrigin())
            l=None
        self._release()
        try:
            for sock in l:
            #for cli,sock in self.dConn.iteritems():
                try:
                    #cli.shutdown()
                    sock.SocketClose()
                except:
                    vtLog.vtLngTB(self.GetOrigin())
            self.lRd=[]
            self.dConn={}
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self._acquire()
        try:
            self.serving=False
            self.socket.close()
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self._release()
        vtLog.vtLngCur(vtLog.INFO,'stopped on %d'%self.port,self.GetOrigin())
    def Run(self):
        self._acquire()
        self.serving=True
        self.connections={}
        self._release()
        HOST=None#'localhost'
        vtLog.vtLngCur(vtLog.INFO,'Run on port %d'%self.port,self.GetOrigin())
        for res in socket.getaddrinfo(HOST, self.port, socket.AF_UNSPEC, socket.SOCK_STREAM, 0, socket.AI_PASSIVE):
            #vtLog.vtLngCurCls(vtLog.INFO,repr(res),self)
            af, socktype, proto, canonname, sa = res
            try:
                self.socket = socket.socket(af, socktype, proto)
            except socket.error, msg:
                self.socket = None
                continue
            try:
                self.socket.bind(sa)
                self.socket.listen(3)
            except socket.error, msg:
                self.socket.close()
                self.socket = None
                continue
            break
            #if s is None:
            #    print 'could not open socket'
                #sys.exit(1)
            #print s
        if self.socket is None:
            self.serving=False
            vtLog.vtLngCur(vtLog.INFO,'socket is None',self.GetOrigin())
            return
        self.socket.settimeout(2.0)
        #self.socket.setblocking(0)
        while self.IsServing():
            if self.Is2Stop():
                break
            #if self.stopping:
            #    break
            #vtLog.vtLngCurCls(vtLog.INFO,'wait for connection',self)
            try:
                conn, addr = self.socket.accept()
                vtLog.vtLngCur(vtLog.INFO,'incomming connection;class:%s'%(
                            self.socketClass),self.GetOrigin())
                #print s,'accept'
                vtLog.vtLngCur(vtLog.INFO,'Connected by %s'%str(addr),
                            self.GetOrigin())
                sID=self.GenerateSessionID()
                #if self.SSL:
                #    import os
                #    print os.getcwd()
                #    conn=socket.ssl(conn,'key.pem','cert.pem')
                #    print conn
                vtLog.vtLngCur(vtLog.INFO,'sessionID:%s'%(sID),self.GetOrigin())
                vtLog.vtLngCur(vtLog.INFO,'sock inst:%s'%(self.socketClass),self.GetOrigin())
                sock=self.socketClass(self,conn,addr,sID)
                vtLog.vtLngCur(vtLog.INFO,'sock inst:%s'%(self.socketClass),self.GetOrigin())
                #sock.SendSessionID()
                #vtLog.vtLngCur(vtLog.INFO,'sessionID:%s;send'%(sID),self.GetOrigin())
                #self.connections[addr]=sock
            except socket.timeout:
                pass
            except:
                vtLog.vtLngTB(self.GetOrigin())
        #print self.port
        self._acquire()
        try:
            self.serving=False
            self.socket.close()
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self._release()
        vtLog.vtLngCur(vtLog.INFO,'stopped on %d'%self.port,self.GetOrigin())
    def __acceptSSL__(self):
        try:
            
            sock, addr = self.socket.accept()
            #conn = tsafe.Connection(connection=sock)
            conn = sock
            vtLog.vtLngCur(vtLog.INFO,'incomming connection;class:%s'%(
                        self.socketClass),self.GetOrigin())
            vtLog.vtLngCur(vtLog.INFO,'Connected by %s'%str(addr),
                        self.GetOrigin())
            sID=self.GenerateSessionID()
            vtLog.vtLngCur(vtLog.INFO,'sessionID:%s'%(sID),self.GetOrigin())
            vtLog.vtLngCur(vtLog.INFO,'sock inst:%s'%(self.socketClass),self.GetOrigin())
            sock=self.socketClass(self,conn,addr,sID,
                                bUseSelect4Comm=True)
            fd=conn.fileno()
            self.dConn[fd]=sock
            l=None
            for lRdTmp in self.lRd:
                if len(lRdTmp)<self.SELECT_FD_SIZE:
                    l=lRdTmp
                    break
            if l is None:
                l=[fd]
                self.lRd.append(l)
            else:
                l.append(fd)
        except socket.timeout:
            pass
        except (SSL.WantReadError, SSL.WantWriteError, SSL.WantX509LookupError):
            vtLog.vtLngTB(self.GetOrigin())
            pass
        except SSL.ZeroReturnError:
            vtLog.vtLngTB(self.GetOrigin())
        except SSL.Error, errors:
            vtLog.vtLngTB(self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def RunSelectSSL(self):
        vtLog.vtLngCur(vtLog.DEBUG,'',self.GetOrigin())
        self._acquire()
        self.serving=True
        self.connections={}
        self._release()
        HOST=None#'localhost'
        vtLog.vtLngCur(vtLog.INFO,'Run on port %d'%self.port,self.GetOrigin())
        for res in socket.getaddrinfo(HOST, self.port, socket.AF_UNSPEC, socket.SOCK_STREAM, 0, socket.AI_PASSIVE):
            #vtLog.vtLngCurCls(vtLog.INFO,repr(res),self)
            af, socktype, proto, canonname, sa = res
            try:
                self.socket = tsafe.Connection(self.ctx, socket.socket(af, socktype, proto))
            except socket.error, msg:
                self.socket = None
                continue
            try:
                self.socket.bind(sa)
                self.socket.listen(3)
            except socket.error, msg:
                self.socket.close()
                self.socket = None
                continue
            break
            #if s is None:
            #    print 'could not open socket'
                #sys.exit(1)
            #print s
        if self.socket is None:
            self.serving=False
            vtLog.vtLngCur(vtLog.INFO,'socket is None',self.GetOrigin())
            return
        #self.socket.setblocking(2.0)
        #self.socket.setblocking(0.0)
        self.socket.settimeout(0.5)
        self._acquire()
        fdSrv=self.socket.fileno()
        self.lRd=[[fdSrv]]
        self.dConn={} #{fdSrv:self}
        srv=self.socket
        self.lWr=[]
        self._release()
        lEvt=[]
        lWr=[]
        while self.IsServing():
            while self.qConn.empty()==False:
                try:
                    f,args,kwargs=self.qConn.get()
                    f(*args,**kwargs)
                except:
                    vtLog.vtLngTB(self.GetOrigin())
            if self.Is2Stop():
                break
            try:
                for lRd in self.lRd:
                    try:
                        #lRdRdy,lWrRdy , lEvtRdy = select.select(lRd, lWr , lEvt, self.ZWAIT_SELECT)
                        lRdRdy,lWrRdy , lEvtRdy = select.select(lRd, [] , lEvt, self.ZWAIT_SELECT)
                    except socket.timeout,msg:
                        vtLog.vtLngCur(vtLog.DEBUG,''%(),self.GetOrigin())
                        #print 'timeout'
                        #self.CheckPause()
                        continue
                    except:
                        vtLog.vtLngTB(self.GetOrigin())
                        #break
                    #vtLog.vtLngCur(vtLog.DEBUG,'rd:%d;wr:%d'%(len(lRdRdy),len(lWrRdy)),self.GetOrigin())
                    for cli in lRdRdy:
                        #if cli == srv:
                        if cli == fdSrv:
                            self.qConn.put((self.__acceptSSL__,(),{}))
                        else:
                            sock=self.dConn[cli]
                            #print 'recv',cli,sock,sock.IsServing()
                            if sock.doRecv()<0:
                                self.RemoveSock(sock,cli)
            except:
                vtLog.vtLngTB(self.GetOrigin())
            try:
                lWr=[]
                iCount=0
                def selectWr(lWr):
                    lRdRdy,lWrRdy , lEvtRdy = select.select([], lWr , [], self.ZWAIT_SELECT)
                    for cli in lWrRdy:
                        sock=self.dConn[cli]
                        #print 'recv',cli,sock,sock.IsServing()
                        if sock.doSend()<0:
                            self.RemoveSock(sock,cli)
                for fdCli,sock in self.dConn.iteritems():
                    if sock.Is2Snd():
                        lWr.append(fdCli)
                        iCount+=1
                    if iCount>32:
                        selectWr(lWr)
                        iCount=0
                        lWr=[]
                if iCount>0:
                    selectWr(lWr)
            except:
                vtLog.vtLngTB(self.GetOrigin())
        self._acquire()
        try:
            #del self.dConn[srv]
            #del self.dConn[fdSrv]
            l=self.dConn.values()
        except:
            vtLog.vtLngTB(self.GetOrigin())
            l=None
        self._release()
        try:
            for sock in l:
            #for cli,sock in self.dConn.iteritems():
                try:
                    #cli.shutdown()
                    sock.SocketClose()
                except:
                    vtLog.vtLngTB(self.GetOrigin())
            self.lRd=[]
            self.dConn={}
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self._acquire()
        try:
            self.serving=False
            self.socket.close()
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self._release()
        vtLog.vtLngCur(vtLog.INFO,'stopped on %d'%self.port,self.GetOrigin())
    def RunSSL(self):
        self._acquire()
        self.serving=True
        self.connections={}
        self._release()
        HOST=None#'localhost'
        vtLog.vtLngCur(vtLog.INFO,'Run on port %d'%self.port,self.GetOrigin())
        for res in socket.getaddrinfo(HOST, self.port, socket.AF_UNSPEC, socket.SOCK_STREAM, 0, socket.AI_PASSIVE):
            #vtLog.vtLngCurCls(vtLog.INFO,repr(res),self)
            af, socktype, proto, canonname, sa = res
            try:
                #self.socket = socket.socket(af, socktype, proto)
                self.socket = tsafe.Connection(self.ctx, socket.socket(af, socktype, proto))
                #self.socket.set_app_data(self)
                #self.socket.bind(('', self.port))
                #self.socket.listen(3) 
                #self.socket.settimeout(1)
                #self.socket.setblocking(0)
                #self.socket.setblocking(1)
            
            except socket.error, msg:
                self.socket = None
                continue
            try:
                self.socket.bind(sa)
                self.socket.listen(3)
                self.socket.settimeout(1)
                #self.socket.setblocking(1)
            except socket.error, msg:
                self.socket.close()
                self.socket = None
                continue
            break
            #if s is None:
            #    print 'could not open socket'
                #sys.exit(1)
            #print s
        if self.socket is None:
            self.serving=False
            return
        self.socket.settimeout(2.0)
        #self.socket.setblocking(1)
        while self.IsServing():
            if self.Is2Stop():
                break
            #if self.stopping:
            #    break
            #vtLog.vtLngCurCls(vtLog.INFO,'wait for connection',self)
            try:
                #tup=select.select([self.socket],[],[])
                #print tup
                #self.socket.setblocking(1)
                #conn, addr = self.socket.accept()
                sock, addr = self.socket.accept()
                conn = tsafe.Connection(connection=sock)
                #print sock
                #conn = tsafe.Connection(self.ctx, sock)
                #conn=sock
                #conn.set_app_data(self)
                #print conn
                vtLog.vtLngCur(vtLog.INFO,'incomming connection;class:%s'%(
                        self.socketClass),self.GetOrigin())
                #print s,'accept'
                vtLog.vtLngCur(vtLog.INFO,'Connected by %s'%str(addr),
                        self.GetOrigin())
                #if self.SSL:
                #    import os
                #    print os.getcwd()
                #    conn=socket.ssl(conn,'key.pem','cert.pem')
                #    print conn
                #self.semVerify.acquire()
                #print 'runSSL','ver',1
                sID=self.GenerateSessionID()
                vtLog.vtLngCur(vtLog.INFO,'sessionID:%s'%(sID),self.GetOrigin())
                vtLog.vtLngCur(vtLog.INFO,'sock inst:%s'%(self.socketClass),self.GetOrigin())
                sock=self.socketClass(self,conn,addr,sID)
                #sock=self.socketClass(self,None,addr,sID)
                #ctx=conn.get_context()
                #ctx.set_app_data(sock)
                #time.sleep(5.5)
                
                #conn.set_app_data(self)
                #self.semVerify.acquire()
                #while sock.iVerify==0:
                    #time.sleep(0.1)
                    #time.sleep(1)
                #    print sock.iVerify
                #    print conn.state_string()
                    
                #print conn.state_string()
                #print 'runSSL','ver',2
                #self.semVerify.release()
                #print 'runSSL','ver',3
                #vtLog.vtLngCur(vtLog.INFO,'sock inst:%s'%(self.socketClass),self.GetOrigin())
                #sock.SendSessionID()
                #vtLog.vtLngCur(vtLog.INFO,'sessionID:%s;send'%(sID),self.GetOrigin())
                #self.connections[addr]=sock
            except socket.timeout:
                pass
            except (SSL.WantReadError, SSL.WantWriteError, SSL.WantX509LookupError):
                vtLog.vtLngTB(self.GetOrigin())
                pass
            except SSL.ZeroReturnError:
                #dropClient(cli)
                #print 'SSL.ZeroReturnError'
                vtLog.vtLngTB(self.GetOrigin())
                #self.conn.shutdown()
                #self.serving=False
            except SSL.Error, errors:
                #dropClient(cli, errors)
                #print 'SSL.Error',errors
                vtLog.vtLngTB(self.GetOrigin())
                #traceback.print_exc()
                #self.serving=False
            except:
                vtLog.vtLngTB(self.GetOrigin())
        #print self.port
        self._acquire()
        try:
            self.serving=False
            self.socket.close()
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self._release()
        vtLog.vtLngCur(vtLog.INFO,'stopped on %d'%self.port,self.GetOrigin())
    def NotifyConnected(self,sock):
        sock.SendSessionID()
        addr=sock.adr
        sID=sock.sessionID
        vtLog.vtLngCur(vtLog.INFO,'sessionID:%s;send'%(sID),self.GetOrigin())
        self.connections[addr]=sock
        
    def GetConnectionsStr(self):
        s=''
        keys=self.connections.keys()
        def compFunc(a,b):
            i=cmp(a[0],b[0])
            if i==0:
                return a[1]-b[1]
            return i
        keys.sort(compFunc)
        lst=[]
        for k in keys:
            sock=self.connections[k]
            lst.append(k[0]+';'+str(k[1])+';'+str(sock.received)+';'+str(sock.send)+';'+sock.zConn)
        return string.join(lst,',')
class vtNetSrv:
    def __init__(self,dataSrvClass,dataPort,dataClass,serviceSrvClass,serviceProt,serviceClass,verboseData=0,verboseService=0):
        random.seed()
        self.dataPort=dataPort
        self.servicePort=serviceProt
        self.serving=False
        self.dataClass=dataClass
        self.serviceClass=serviceClass
        self.srvData=dataSrvClass(self,self.dataPort,self.dataClass,verboseData)
        self.srvService=serviceSrvClass(self,self.servicePort,self.serviceClass,verboseService)
    def GetDataInstance(self):
        return self.srvData
    def SetServicePasswd(self,passwd):
        self.srvService.SetPasswd(passwd)
    def Serve(self,bUseSelect=True):
        self.serving=True
        self.srvService.Start()
        self.srvData.Start(bUseSelect=bUseSelect)
    def IsServing(self):
        if self.serving:
            if self.srvData.IsServing():
                return True
            if self.srvService.IsServing():
                return True
        return False
    def Stop(self,bWait=True):
        try:
            try:
                vtLog.vtLngCur(vtLog.INFO,'data port:%s;service port:%s'%(
                        repr(self.dataPort),repr(self.servicePort)),origin=self.__class__.__name__)
            except:
                vtLog.vtLngTB(self.__class__.__name__)
            self.srvData.Stop()
            self.srvService.Stop()
            if bWait==False:
                return
            zTimeOut=0.0
            zDly=0.1
            while zTimeOut<FORCE_SHUTDOWN:
                if self.IsServing()==False:
                    break
                time.sleep(zDly)
                zTimeOut+=zDly
            #for i in range(FORCE_SHUTDOWN):
            #    if self.IsServing()==False:
            #        break
            #    time.sleep(1)
            #print 'close'
            self.srvData.Close()
            #print '  close'
            self.srvService.Close()
            self.serving=False
            #if self.srvData.IsServing()==False:
            #    self.srvService.Stop()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def ShutDown(self):
        try:
            self.srvService.ShutDown()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            self.srvData.ShutDown()
        except:
            vtLog.vtLngTB(self.__class__.__name__)

if __name__=='__main__':
    srv=vtNetSrv(vtSrvSock,50007,vtSock,vtSrvSock,50008,vtServiceSock,1,1)
    print 'enter service password'
    s=raw_input()
    if len(s)>0:
        m=sha.new()
        m.update(s)
        srv.SetServicePasswd(m.hexdigest())
        #srv.SetServicePasswd(s)
    srv.Serve()
    while srv.IsServing():
        time.sleep(10)
        
