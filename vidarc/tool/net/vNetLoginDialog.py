#Boa:Dialog:vNetLoginDialog
#----------------------------------------------------------------------------
# Name:         vNetLoginDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vNetLoginDialog.py,v 1.16 2013/12/09 08:06:54 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import sys
if sys.version_info >= (2,6,0):
    import hashlib
    def newSha():
        return hashlib.sha1()
else:
    import sha
    def newSha():
        return sha.new()
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt

#import images
import vidarc.tool.net.vtNetArt as vtNetArt


# defined event for vtNetLoginDlgFinished item changed
wxEVT_VTNET_LOGIN_DLG_FINISHED=wx.NewEventType()
vEVT_VTNET_LOGIN_DLG_FINISHED=wx.PyEventBinder(wxEVT_VTNET_LOGIN_DLG_FINISHED,1)
def EVT_VTNET_LOGIN_DLG_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_VTNET_LOGIN_DLG_FINISHED,func)
def EVT_VTNET_LOGIN_DLG_FINISHED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,wxEVT_VTNET_LOGIN_DLG_FINISHED)
class vtNetLoginDlgFinished(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTNET_LOGIN_DLG_FINISHED(<widget_name>, self.OnLoginApply)
    """

    def __init__(self,obj,appl,host,port,alias,usr,passwd):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTNET_LOGIN_DLG_FINISHED)
        self.appl=appl
        self.host=host
        self.port=port
        self.alias=alias
        self.usr=usr
        self.passwd=passwd
    def GetAppl(self):
        return self.appl
    def GetHost(self):
        return self.host
    def GetPort(self):
        return self.port
    def GetAlias(self):
        return self.alias
    def GetUsr(self):
        return self.usr
    def GetPasswd(self):
        return self.passwd

def create(parent):
    return vNetLoginDialog(parent)

[wxID_VNETLOGINDIALOG, wxID_VNETLOGINDIALOGCBAPPLY, 
 wxID_VNETLOGINDIALOGCBCANCEL, wxID_VNETLOGINDIALOGLBLALIAS, 
 wxID_VNETLOGINDIALOGLBLAPPL, wxID_VNETLOGINDIALOGLBLHOST, 
 wxID_VNETLOGINDIALOGLBLPASSWD, wxID_VNETLOGINDIALOGLBLPORT, 
 wxID_VNETLOGINDIALOGLBLUSR, wxID_VNETLOGINDIALOGTXTALIAS, 
 wxID_VNETLOGINDIALOGTXTAPPL, wxID_VNETLOGINDIALOGTXTHOST, 
 wxID_VNETLOGINDIALOGTXTPASSWD, wxID_VNETLOGINDIALOGTXTPORT, 
 wxID_VNETLOGINDIALOGTXTUSR, 
] = [wx.NewId() for _init_ctrls in range(15)]

class vNetLoginDialog(wx.Dialog):
    def _init_coll_bxsPort_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPort, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtPort, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsHost_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblHost, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtHost, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsAppl, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsHost, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsPort, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsAlias, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsUser, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsPasswd, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsBt, 0, border=0, flag=wx.ALIGN_CENTER)

    def _init_coll_bxsPasswd_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPasswd, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtPasswd, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsAppl_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblAppl, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtAppl, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_bxsUser_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblUsr, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtUsr, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsAlias_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblAlias, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtAlias, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=8, vgap=0)

        self.bxsHost = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsPort = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsAlias = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsUser = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsPasswd = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsAppl = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsHost_Items(self.bxsHost)
        self._init_coll_bxsPort_Items(self.bxsPort)
        self._init_coll_bxsAlias_Items(self.bxsAlias)
        self._init_coll_bxsUser_Items(self.bxsUser)
        self._init_coll_bxsPasswd_Items(self.bxsPasswd)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsAppl_Items(self.bxsAppl)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VNETLOGINDIALOG,
              name=u'vNetLoginDialog', parent=prnt, pos=wx.Point(177, 90),
              size=wx.Size(223, 219),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE | wx.STAY_ON_TOP,
              title=u'vNetLogin Dialog')
        self.SetClientSize(wx.Size(215, 192))
        self.Bind(wx.EVT_CLOSE, self.OnVNetLoginDialogClose)

        self.lblAppl = wx.StaticText(id=wxID_VNETLOGINDIALOGLBLAPPL,
              label=u'application', name=u'lblAppl', parent=self,
              pos=wx.Point(4, 4), size=wx.Size(69, 21), style=wx.ALIGN_RIGHT)
        self.lblAppl.SetMinSize(wx.Size(-1, -1))

        self.lblAlias = wx.StaticText(id=wxID_VNETLOGINDIALOGLBLALIAS,
              label=_(u'Alias'), name=u'lblAlias', parent=self, pos=wx.Point(4,
              79), size=wx.Size(69, 21), style=wx.ALIGN_RIGHT)
        self.lblAlias.SetMinSize(wx.Size(-1, -1))

        self.lblHost = wx.StaticText(id=wxID_VNETLOGINDIALOGLBLHOST,
              label=_(u'Host'), name=u'lblHost', parent=self, pos=wx.Point(4,
              29), size=wx.Size(69, 21), style=wx.ALIGN_RIGHT)
        self.lblHost.SetMinSize(wx.Size(-1, -1))

        self.lblPort = wx.StaticText(id=wxID_VNETLOGINDIALOGLBLPORT,
              label=_(u'Port'), name=u'lblPort', parent=self, pos=wx.Point(4,
              54), size=wx.Size(69, 21), style=wx.ALIGN_RIGHT)
        self.lblPort.SetMinSize(wx.Size(-1, -1))

        self.lblUsr = wx.StaticText(id=wxID_VNETLOGINDIALOGLBLUSR,
              label=_(u'User'), name=u'lblUsr', parent=self, pos=wx.Point(4,
              104), size=wx.Size(69, 21), style=wx.ALIGN_RIGHT)
        self.lblUsr.SetMinSize(wx.Size(-1, -1))

        self.lblPasswd = wx.StaticText(id=wxID_VNETLOGINDIALOGLBLPASSWD,
              label=_(u'Password'), name=u'lblPasswd', parent=self,
              pos=wx.Point(4, 129), size=wx.Size(69, 21), style=wx.ALIGN_RIGHT)
        self.lblPasswd.SetMinSize(wx.Size(-1, -1))

        self.txtAppl = wx.TextCtrl(id=wxID_VNETLOGINDIALOGTXTAPPL,
              name=u'txtAppl', parent=self, pos=wx.Point(77, 4),
              size=wx.Size(134, 21), style=0, value=u'')
        self.txtAppl.Enable(False)
        self.txtAppl.SetMinSize(wx.Size(-1, -1))

        self.txtHost = wx.TextCtrl(id=wxID_VNETLOGINDIALOGTXTHOST,
              name=u'txtHost', parent=self, pos=wx.Point(77, 29),
              size=wx.Size(134, 21), style=0, value=u'')
        self.txtHost.Enable(False)
        self.txtHost.SetMinSize(wx.Size(-1, -1))

        self.txtPort = wx.TextCtrl(id=wxID_VNETLOGINDIALOGTXTPORT,
              name=u'txtPort', parent=self, pos=wx.Point(77, 54),
              size=wx.Size(134, 21), style=0, value=u'')
        self.txtPort.Enable(False)
        self.txtPort.SetMinSize(wx.Size(-1, -1))

        self.txtAlias = wx.TextCtrl(id=wxID_VNETLOGINDIALOGTXTALIAS,
              name=u'txtAlias', parent=self, pos=wx.Point(77, 79),
              size=wx.Size(134, 21), style=wx.TE_PROCESS_ENTER, value=u'')
        self.txtAlias.Enable(False)
        self.txtAlias.SetMinSize(wx.Size(-1, -1))
        self.txtAlias.Bind(wx.EVT_TEXT_ENTER, self.OnTxtAliasTextEnter,
              id=wxID_VNETLOGINDIALOGTXTALIAS)

        self.txtUsr = wx.TextCtrl(id=wxID_VNETLOGINDIALOGTXTUSR, name=u'txtUsr',
              parent=self, pos=wx.Point(77, 104), size=wx.Size(134, 21),
              style=wx.TE_PROCESS_ENTER, value=u'')
        self.txtUsr.SetMinSize(wx.Size(-1, -1))
        self.txtUsr.Bind(wx.EVT_TEXT_ENTER, self.OnTxtUsrTextEnter,
              id=wxID_VNETLOGINDIALOGTXTUSR)

        self.txtPasswd = wx.TextCtrl(id=wxID_VNETLOGINDIALOGTXTPASSWD,
              name=u'txtPasswd', parent=self, pos=wx.Point(77, 129),
              size=wx.Size(134, 21), style=wx.TE_PASSWORD | wx.TE_PROCESS_ENTER, value=u'')
        self.txtPasswd.SetMinSize(wx.Size(-1, -1))
        self.txtPasswd.Bind(wx.EVT_TEXT_ENTER, self.OnTxtPasswdTextEnter,
              id=wxID_VNETLOGINDIALOGTXTPASSWD)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(id=wxID_VNETLOGINDIALOGCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'), name=u'cbApply',
              parent=self, pos=wx.Point(15, 158), size=wx.Size(76, 30),
              style=0)
        self.cbApply.SetMinSize(wx.Size(-1, -1))
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VNETLOGINDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(id=wxID_VNETLOGINDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(123, 158),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.SetMinSize(wx.Size(-1, -1))
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VNETLOGINDIALOGCBCANCEL)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        self._init_ctrls(parent)
        try:
            icon = wx.EmptyIcon()
            icon.CopyFromBitmap(vtNetArt.getBitmap(vtNetArt.Login))
            self.SetIcon(icon)
        except:
            vtLog.vtLngTB(self.sOrigin)
        
        #self.cbCancel.SetBitmapLabel(images.getCancelBitmap())
        #self.cbApply.SetBitmapLabel(images.getApplyBitmap())
        self.fgsData.Fit(self)
        self.fgsData.Layout()
        self.bModal=False
    def SetValues(self,appl,host,port,alias,usr=''):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'appl:%s;host:%s;port:%s;alias:%s;usr:%s'%(
                    appl,host,repr(port),alias,usr),self)
        self.txtAppl.SetValue(appl)
        self.txtHost.SetValue(host)
        self.txtPort.SetValue(repr(port))
        if len(alias)>0:
            self.txtAlias.Enable(False)
        else:
            self.txtAlias.Enable(True)
        self.txtAlias.SetValue(alias)
        self.txtUsr.SetValue(usr)
        self.txtPasswd.SetValue('')
        if len(usr)>0:
            self.txtUsr.Enable(False)
            self.txtPasswd.SetFocus()
        else:
            self.txtUsr.Enable(True)
            if self.txtAlias.IsEnabled():
                if len(alias)==0:
                    self.txtAlias.SetFocus()
                    return
            if self.txtHost.IsEnabled():
                if len(host)==0:
                    self.txtHost.SetFocus()
                    return
            self.txtUsr.SetFocus()
    def EnableHostCfg(self,flag):
        self.txtHost.Enable(flag)
        self.txtPort.Enable(flag)
        
    def GetAppl(self):
        return self.txtAppl.GetValue()
    def GetHost(self):
        return self.txtHost.GetValue()
    def GetPort(self):
        return self.txtPort.GetValue()
    def GetAlias(self):
        return self.txtAlias.GetValue()
    def GetUsr(self):
        return self.txtUsr.GetValue()
    def GetPasswd(self):
        passwd=self.txtPasswd.GetValue()
        if len(passwd)>0:
            m=newSha()#sha.new()
            m.update(passwd)
            return m.hexdigest()
        else:
            return ''
    def ShowModal(self):
        try:
            vtLog.vtLngCurWX(vtLog.INFO,''%(),self)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'appl:%s;host:%s;port:%s;alias:%s;usr:%s'%(
                        self.GetAppl(),self.GetHost(),
                        self.GetPort(),self.GetAlias(),
                        self.GetUsr()),self)
        except:
            vtLog.vtLngTB(self.GetName())
        self.bModal=True
        return wx.Dialog.ShowModal(self)
    def Show(self,flag):
        try:
            vtLog.vtLngCurWX(vtLog.INFO,''%(),self)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'appl:%s;host:%s;port:%s;alias:%s;usr:%s'%(
                        self.GetAppl(),self.GetHost(),
                        self.GetPort(),self.GetAlias(),
                        self.GetUsr()),self)
        except:
            vtLog.vtLngTB(self.GetName())
        self.bModal=False
        wx.Dialog.Show(self,flag)
    def OnCbApplyButton(self, event):
        if event is not None:
            event.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.INFO,''%(),self)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'appl:%s;host:%s;port:%s;alias:%s;usr:%s'%(
                        self.GetAppl(),self.GetHost(),
                        self.GetPort(),self.GetAlias(),
                        self.GetUsr()),self)
        except:
            vtLog.vtLngTB(self.GetName())
        if self.bModal:
            self.EndModal(1)
        else:
            try:
                wx.PostEvent(self,vtNetLoginDlgFinished(self,
                        self.txtAppl.GetValue(),
                        self.GetHost(),self.GetPort(),
                        self.GetAlias(),
                        self.GetUsr(),self.GetPasswd()))
            except:
                vtLog.vtLngTB(self.GetName())
            self.Show(False)
    def OnCbCancelButton(self, event):
        event.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.INFO,''%(),self)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'appl:%s;host:%s;port:%s;alias:%s;usr:%s'%(
                        self.GetAppl(),self.GetHost(),
                        self.GetPort(),self.GetAlias(),
                        self.GetUsr()),self)
        except:
            vtLog.vtLngTB(self.GetName())
        if self.bModal:
            self.EndModal(0)
        else:
            try:
                wx.PostEvent(self,vtNetLoginDlgFinished(self,
                        self.txtAppl.GetValue(),
                        self.GetHost(),self.GetPort(),
                        self.GetAlias(),
                        None,None))
            except:
                vtLog.vtLngTB(self.GetName())
            self.Show(False)
    def OnTxtAliasTextEnter(self, event):
        event.Skip()
        self.txtUsr.SetFocus()
    def OnTxtUsrTextEnter(self, event):
        event.Skip()
        self.txtPasswd.SetFocus()
    def OnTxtPasswdTextEnter(self, event):
        event.Skip()
        self.OnCbApplyButton(None)

    def OnVNetLoginDialogClose(self, event):
        event.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.INFO,''%(),self)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'appl:%s;host:%s;port:%s;alias:%s;usr:%s'%(
                        self.GetAppl(),self.GetHost(),
                        self.GetPort(),self.GetAlias(),
                        self.GetUsr()),self)
        except:
            vtLog.vtLngTB(self.GetName())
        try:
            wx.PostEvent(self,vtNetLoginDlgFinished(self,
                    self.txtAppl.GetValue(),
                    self.GetHost(),self.GetPort(),
                    self.GetAlias(),
                    None,None))
        except:
            vtLog.vtLngTB(self.GetName())
