#Boa:Frame:vgfMain
#----------------------------------------------------------------------------
# Name:         vgfServiceMain.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vgfServiceMain.py,v 1.1 2005/12/11 23:04:49 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
from wx.lib.anchors import LayoutAnchors
import time
import images
from vidarc.tool.xml.vtXmlDom import *
from vidarc.tool.net.vtNetSrv import vtServiceCltSock
from vidarc.tool.net.vtNetClt import vtNetClt
import string,os,os.path,sys,md5,binascii
import tool.InOut.fnUtil as fnUtil

#from vidarc.vApps.vLunch.vgpInfo import *

def create(parent):
    return vgfMain(parent)

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon
    
[wxID_VGFMAIN, wxID_VGFMAINCBCLOSE, wxID_VGFMAINCBSHUTDOWN, 
 wxID_VGFMAINCBUPDATE, wxID_VGFMAINCHCAUTO, wxID_VGFMAINLBLCONNECTIONS, 
 wxID_VGFMAINLBLCONNSTATUS, wxID_VGFMAINLBLHOST, wxID_VGFMAINLBLPASS, 
 wxID_VGFMAINLBLPORT, wxID_VGFMAINLBLTIME, wxID_VGFMAINLBLUPDATE, 
 wxID_VGFMAINLSTCONNECTIONS, wxID_VGFMAINPNINFO, wxID_VGFMAINSPUPDATE, 
 wxID_VGFMAINTGCONNECT, wxID_VGFMAINTXTHOST, wxID_VGFMAINTXTPASSWD, 
 wxID_VGFMAINTXTPORT, 
] = [wx.NewId() for _init_ctrls in range(19)]

[wxID_VGFMAINTBMAINTOOL_LEFT_BOTTOM, wxID_VGFMAINTBMAINTOOL_LEFT_TOP, 
 wxID_VGFMAINTBMAINTOOL_RIGHT_BOTTOM, wxID_VGFMAINTBMAINTOOL_RIGHT_TOP, 
] = [wx.NewId() for _init_coll_tbMain_Tools in range(4)]

dictLunchApps={
    u'vPrjTimer':[u'vPrjTimer/vgaPrjTimer.py',u'vPrjTimer/vProjectTimer.exe'],
    u'vPrjEng':[u'vPrjEng/vgaPrjEng.py',u'vPrjEng/vPrjEng.exe'],
    u'vPrj':[u'vPrj/vgaPrj.py',u'vProject/vProject.exe'],
    u'vPrjDoc':[u'vPrjDoc/vgaPrjDoc.py',u'vPrjDoc/vPrjDoc.exe'],
    u'vHum':[u'vHum/vgaHum.py',u'vHum/vHuman.exe'],
    u'vDoc':[u'vDoc/vgaDoc.py',u'vDoc/vDoc.exe'],
    u'vSec':[],
    u'Net':[],
    u'Tools':[],
    u'vVSAS':[u'vVSAS/vgaVSAS.py',u'vVSAS/vVSAS.exe']
                }
class vgfMain(wx.Frame):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VGFMAIN, name=u'vgfMain', parent=prnt,
              pos=wx.Point(295, 139), size=wx.Size(556, 360),
              style=wx.MINIMIZE_BOX | wx.CLOSE_BOX | wx.SYSTEM_MENU | wx.CAPTION,# | wx.RESIZE_BORDER,
              title=u'vService')
        self.SetClientSize(wx.Size(548, 333))
        self.Bind(wx.EVT_CLOSE, self.OnVgfMainClose)
        self.Bind(wx.EVT_PAINT, self.OnVgfMainPaint)
        self.Bind(wx.EVT_SIZE, self.OnVgfMainSize)

        self.pnInfo = wx.Panel(id=wxID_VGFMAINPNINFO, name=u'pnInfo',
              parent=self, pos=wx.Point(0, 0), size=wx.Size(548, 333),
              style=wx.TAB_TRAVERSAL)
        self.pnInfo.SetAutoLayout(True)

        self.lblTime = wx.StaticText(id=wxID_VGFMAINLBLTIME, label=u'',
              name=u'lblTime', parent=self.pnInfo, pos=wx.Point(392, 316),
              size=wx.Size(150, 13), style=0)
        self.lblTime.SetConstraints(LayoutAnchors(self.lblTime, False, False,
              True, True))

        self.lblHost = wx.StaticText(id=wxID_VGFMAINLBLHOST, label=u'Host',
              name=u'lblHost', parent=self.pnInfo, pos=wx.Point(8, 8),
              size=wx.Size(22, 13), style=0)

        self.txtHost = wx.TextCtrl(id=wxID_VGFMAINTXTHOST, name=u'txtHost',
              parent=self.pnInfo, pos=wx.Point(40, 4), size=wx.Size(120, 21),
              style=0, value=u'localhost')

        self.lblPort = wx.StaticText(id=wxID_VGFMAINLBLPORT, label=u'Port',
              name=u'lblPort', parent=self.pnInfo, pos=wx.Point(168, 8),
              size=wx.Size(19, 13), style=0)

        self.txtPort = wx.TextCtrl(id=wxID_VGFMAINTXTPORT, name=u'txtPort',
              parent=self.pnInfo, pos=wx.Point(192, 4), size=wx.Size(80, 21),
              style=0, value=u'')

        self.txtPasswd = wx.TextCtrl(id=wxID_VGFMAINTXTPASSWD,
              name=u'txtPasswd', parent=self.pnInfo, pos=wx.Point(336, 4),
              size=wx.Size(100, 21), style=wx.TE_PASSWORD, value=u'')
        self.txtPasswd.SetToolTipString(u'password')

        self.lblPass = wx.StaticText(id=wxID_VGFMAINLBLPASS, label=u'Password',
              name=u'lblPass', parent=self.pnInfo, pos=wx.Point(280, 8),
              size=wx.Size(46, 13), style=0)

        self.cbClose = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGFMAINCBCLOSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Close', name=u'cbClose',
              parent=self.pnInfo, pos=wx.Point(344, 36), size=wx.Size(84, 30),
              style=0)
        self.cbClose.Bind(wx.EVT_BUTTON, self.OnCbCloseButton,
              id=wxID_VGFMAINCBCLOSE)

        self.lstConnections = wx.ListCtrl(id=wxID_VGFMAINLSTCONNECTIONS,
              name=u'lstConnections', parent=self.pnInfo, pos=wx.Point(8, 88),
              size=wx.Size(536, 216), style=wx.LC_REPORT)

        self.lblConnections = wx.StaticText(id=wxID_VGFMAINLBLCONNECTIONS,
              label=u'Connections', name=u'lblConnections', parent=self.pnInfo,
              pos=wx.Point(8, 72), size=wx.Size(59, 13), style=0)

        self.cbShutDown = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGFMAINCBSHUTDOWN,
              bitmap=wx.EmptyBitmap(16, 16), label=u'ShutDown',
              name=u'cbShutDown', parent=self.pnInfo, pos=wx.Point(464, 36),
              size=wx.Size(84, 30), style=0)
        self.cbShutDown.Bind(wx.EVT_BUTTON, self.OnCbShutDownButton,
              id=wxID_VGFMAINCBSHUTDOWN)

        self.lblConnStatus = wx.StaticBitmap(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VGFMAINLBLCONNSTATUS, name=u'lblConnStatus',
              parent=self.pnInfo, pos=wx.Point(444, 8), size=wx.Size(16, 16),
              style=0)

        self.lblUpdate = wx.StaticText(id=wxID_VGFMAINLBLUPDATE,
              label=u'Update', name=u'lblUpdate', parent=self.pnInfo,
              pos=wx.Point(8, 44), size=wx.Size(35, 13), style=0)

        self.spUpdate = wx.SpinCtrl(id=wxID_VGFMAINSPUPDATE, initial=1, max=600,
              min=1, name=u'spUpdate', parent=self.pnInfo, pos=wx.Point(48, 40),
              size=wx.Size(60, 21), style=wx.SP_ARROW_KEYS)

        self.chcAuto = wx.CheckBox(id=wxID_VGFMAINCHCAUTO, label=u'Auto',
              name=u'chcAuto', parent=self.pnInfo, pos=wx.Point(112, 44),
              size=wx.Size(50, 13), style=0)
        self.chcAuto.SetValue(False)
        self.chcAuto.Bind(wx.EVT_CHECKBOX, self.OnChcAutoCheckbox,
              id=wxID_VGFMAINCHCAUTO)

        self.cbUpdate = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGFMAINCBUPDATE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Update', name=u'cbUpdate',
              parent=self.pnInfo, pos=wx.Point(176, 36), size=wx.Size(76, 30),
              style=0)
        self.cbUpdate.Bind(wx.EVT_BUTTON, self.OnCbUpdateButton,
              id=wxID_VGFMAINCBUPDATE)

        self.tgConnect = wx.lib.buttons.GenBitmapTextToggleButton(ID=wxID_VGFMAINTGCONNECT,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Connect',
              name=u'tgConnect', parent=self.pnInfo, pos=wx.Point(464, 2),
              size=wx.Size(84, 30), style=0)
        self.tgConnect.SetToggle(False)
        self.tgConnect.Bind(wx.EVT_BUTTON, self.OnTgConnectButton,
              id=wxID_VGFMAINTGCONNECT)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.dom=None
        self.host=None
        self.port=None
        self.srvSocket=None
        #img=images.getRunningBitmap()
        #self.cbLT.SetBitmapLabel(img)
        
        img=images.getStoppedBitmap()
        self.lblConnStatus.SetBitmap(img)
        
        img=images.getCloseBitmap()
        self.cbClose.SetBitmapLabel(img)
        
        img=images.getShutDownBitmap()
        self.cbShutDown.SetBitmapLabel(img)
        
        img=images.getUpdateBitmap()
        self.cbUpdate.SetBitmapLabel(img)
        
        self.tgConnect.SetBitmapLabel(images.getConnectBitmap())
        self.tgConnect.SetBitmapSelected(images.getConnectBitmap())
        
        self.lstConnections.InsertColumn(0,u'Nr',wx.LIST_FORMAT_RIGHT,40)
        self.lstConnections.InsertColumn(1,u'Host',wx.LIST_FORMAT_LEFT,100)
        self.lstConnections.InsertColumn(2,u'Port',wx.LIST_FORMAT_LEFT,60)
        self.lstConnections.InsertColumn(3,u'Received',wx.LIST_FORMAT_RIGHT,90)
        self.lstConnections.InsertColumn(4,u'Send',wx.LIST_FORMAT_RIGHT,90)
        self.lstConnections.InsertColumn(5,u'Connected at',wx.LIST_FORMAT_LEFT,120)
        
        self.spUpdate.SetValue(1)
        self.zAuto=wx.PyTimer(self.__updateConn__)
        
        self.cbClose.Enable(False)
        
        icon = getApplicationIcon()
        self.SetIcon(icon)
        if wx.Platform == '__WXMSW__o':
            self.TBMENU_RESTORE=wx.NewId()
            self.TBMENU_CLOSE=wx.NewId()
            # setup a taskbar icon, and catch some events from it
            self.tbicon = wx.TaskBarIcon()
            self.tbicon.SetIcon(icon, "VIDARC Luncher")
            wx.EVT_TASKBAR_LEFT_DCLICK(self.tbicon, self.OnTaskBarActivate)
            wx.EVT_TASKBAR_RIGHT_UP(self.tbicon, self.OnTaskBarMenu)
            wx.EVT_MENU(self.tbicon, self.TBMENU_RESTORE, self.OnTaskBarActivate)
            wx.EVT_MENU(self.tbicon, self.TBMENU_CLOSE, self.OnTaskBarClose)

        # setup statusbar
        self.timer = wx.PyTimer(self.Notify)
        self.timer.Start(1000)
        self.Notify()
        self.iPos=0
    def Notify(self):
        t = time.localtime(time.time())
        st = time.strftime("%d-%b-%Y   %H:%M:%S wk:%U", t)
        self.lblTime.SetLabel(st)
        pass
    def OnTaskBarActivate(self, evt):
        if self.IsIconized():
            self.Iconize(False)
        if not self.IsShown():
            self.Show(True)
        self.Raise()

    def OnTaskBarMenu(self, evt):
        menu = wx.Menu()
        #menu.Append(self.TBMENU_RESTORE, "Restore VIDARC Luncher")
        it=wx.MenuItem(menu,help='', id=self.TBMENU_RESTORE,
                kind=wx.ITEM_NORMAL, text=u"Restore VIDARC Luncher")
        it.SetBitmap(images.getOpenBitmap())
        menu.AppendItem(it)
        menu.AppendSeparator()
        #menu.Append(self.TBMENU_CLOSE,   "Close")
        it=wx.MenuItem(menu,help='', id=self.TBMENU_CLOSE,
                kind=wx.ITEM_NORMAL, text=u"Close")
        it.SetBitmap(images.getCancelBitmap())
        menu.AppendItem(it)
        self.tbicon.PopupMenu(menu)
        menu.Destroy()

    #---------------------------------------------
    def OnTaskBarClose(self, evt):
        self.Close()
        #self.Destroy()

    def OnVgfMainClose(self, event):
        if hasattr(self, "tbicon"):
            #del self.tbicon
            self.tbicon.Destroy()
            #del self.tbicon
        self.Destroy()
        
    def OnVgfMainPaint(self, event):
        #self.__update__()
        event.Skip()

    def OnVgfMainSize(self, event):
        size=event.GetSize()
        #size=event.GetClientSize()
        #dw=wsize.GetWidth()-size.GetWidth()
        #dy=wsize.GetWidth()-size.GetHeight()
        w=size.GetWidth()
        h=size[1]
        bMod=False
        if w<400:
            w=300
            bMod=True
        if h<200:
            h=200
            bMod=True
        dispSize=wx.ClientDisplayRect()
        if w>dispSize[2]:
            w=dispSize[2]
            bMod=True
        if h>dispSize[3]:
            h=dispSize[3]
            bMod=True
        if  bMod==True:
            self.SetSize(wx.Size(w,h))
            #self.__update__()
        event.Skip()
    def OnCbCloseButton(self, event):
        
        event.Skip()

    def OnCbShutDownButton(self, event):
        if self.srvSock is not None:
            self.srvSock.ShutDown()
        event.Skip()

    def OnCbUpdateButton(self, event):
        self.__updateConn__()
        event.Skip()
    def NotifyLogin(self):
        img=images.getLoginBitmap()
        self.lblConnStatus.SetBitmap(img)
        self.txtPasswd.SetFocus()
        pass
    def NotifyLoggedIn(self):
        img=images.getRunningBitmap()
        self.lblConnStatus.SetBitmap(img)
        pass
    def __updateConn__(self):
        if self.srvSock is not None:
            self.srvSock.GetDataConnections()
    def NotifyDatConn(self,lst):
        #self.lstConnections.DeleteAllItems()
        iLen=self.lstConnections.GetItemCount()
        i=0
        for it in lst:
            strs=string.split(it,';')
            if i<iLen:
                index=i
                #self.lstConnections.SetStringItem(index,0,strs[0],-1)
            else:
                index = self.lstConnections.InsertImageStringItem(sys.maxint, str(i+1), -1)
            for j in range(0,5):
                try:
                    sVal=strs[j]
                    if j in [2,3]:
                        iVal=long(sVal)
                        fVal=iVal/1024.0
                        if fVal>1024.0:
                            fVal=fVal/1024.0
                            sVal='%8.2f MB'%fVal
                        else:
                            sVal='%8.2f kB'%fVal
                    self.lstConnections.SetStringItem(index,j+1,sVal,-1)
                except:
                    pass
            i+=1
        for j in range(i,iLen):
            self.lstConnections.DeleteItem(i)
    def OnChcAutoCheckbox(self, event):
        if self.chcAuto.GetValue():
            i=self.spUpdate.GetValue()*1000
            self.zAuto.Start(i)
            self.__updateConn__()
            self.spUpdate.Enable(False)
        else:
            self.zAuto.Stop()
            self.spUpdate.Enable(True)
        event.Skip()

    def OnTgConnectButton(self, event):
        if self.tgConnect.GetValue():
            bConnect=False
            bFlt=False
            if self.srvSocket is None:
                bConnect=True
            try:
                host=self.txtHost.GetValue()
                port=int(self.txtPort.GetValue())
            except:
                bFlt=True
            if self.host is None:
                bConnect=True
                self.host=host
                self.port=port
            else:
                if bFlt:
                    return
                if host!=self.host:
                    self.host=host
                    bConnect=True
                if port!=self.port:
                    self.port=port
                    bConnect=True
            if bConnect:
                self.cltServ=vtNetClt(None,self.host,self.port,vtServiceGuiCltSock)
                self.cltServ.Connect()
                self.srvSock=self.cltServ.GetSocketInstance()
                if self.srvSock is None:
                    self.tgConnect.SetValue(0)
                    return
                self.srvSock.SetParent(self)
            img=images.getLoginBitmap()
            self.lblConnStatus.SetBitmap(img)
            self.srvSock.Login(self.txtPasswd.GetValue())
        else:
            
            #img=images.getStoppedBitmap()
            #self.lblConnStatus.SetBitmap(img)
            self.cltServ.Close()
            #self.__connectionClosed__()
            #del self.cltServ
            #self.cltServ=None
            #self.srvSock=None
            #self.host=None
            #self.port=None
        event.Skip()
    def __connectionClosed__(self):
        del self.cltServ
        self.cltServ=None
        self.srvSock=None
        self.host=None
        self.port=None
        self.tgConnect.SetValue(0)
        img=images.getStoppedBitmap()
        self.lblConnStatus.SetBitmap(img)
        self.zAuto.Stop()
        self.spUpdate.Enable(True)
class vtServiceGuiCltSock(vtServiceCltSock):
    def SetParent(self,par):
        self.par=par
    def SocketClosed(self):
        self.par.__connectionClosed__()
    def Login(self,passwd):
        self.passwd=passwd
    def __sessionID__(self,val):
        vtServiceCltSock.__sessionID__(self,val)
        self.__doLogin__()
    def __doLogin__(self):
        if len(self.passwd)>0:
            m=md5.new()
            m.update(self.passwd)
            m.update(self.sessionID)
            s=binascii.b2a_uu(m.digest())
        self.Send(self.startMarker+'login|'+s+self.endMarker)
    def __login__(self,val):
        self.par.NotifyLogin()
    def __loggedin__(self,val):
        self.loggedin=True
        self.par.NotifyLoggedIn()
    def __datConn__(self,val):
        i=string.find(val,'|')
        if i>=0:
            lst=string.split(val[i+1:],',')
        else:
            lst=[]
        self.par.NotifyDatConn(lst)
    
