#Boa:Dialog:vtNetSecXmlGuiMasterLogDialog
#----------------------------------------------------------------------------
# Name:         vtNetSecXmlGuiMasterLogDialog.py
# Purpose:      
#               
# Author:       Walter Obweger
#
# Created:      20060415
# CVS-ID:       $Id: vtNetSecXmlGuiMasterLogDialog.py,v 1.8 2007/07/25 06:07:04 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------


import wx
import wx.lib.buttons
import vidarc.tool.net.vtNetSecXmlGuiMasterLogPanel
import vidarc.tool.lang.vtLgBase as vtLgBase

import vidarc.tool.art.vtArt as vtArt

def create(parent):
    return vtNetSecXmlGuiMasterLogDialog(parent)

[wxID_VTNETSECXMLGUIMASTERLOGDIALOG, 
 wxID_VTNETSECXMLGUIMASTERLOGDIALOGCBCLOSE, 
 wxID_VTNETSECXMLGUIMASTERLOGDIALOGPNLOG, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vtNetSecXmlGuiMasterLogDialog(wx.Dialog):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.pnLog, 0, border=4,
              flag=wx.BOTTOM | wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbClose, 0, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.RIGHT | wx.ALIGN_CENTER | wx.LEFT)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTNETSECXMLGUIMASTERLOGDIALOG,
              name=u'vtNetSecXmlGuiMasterLogDialog', parent=prnt,
              pos=wx.Point(132, 132), size=wx.Size(450, 237),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE | wx.STAY_ON_TOP,
              title=u'vtNetSecXmlMasterLog Dialog')
        self.SetClientSize(wx.Size(442, 210))

        self.pnLog = vidarc.tool.net.vtNetSecXmlGuiMasterLogPanel.vtNetSecXmlGuiMasterLogPanel(id=wxID_VTNETSECXMLGUIMASTERLOGDIALOGPNLOG,
              name=u'pnLog', parent=self, pos=wx.Point(4, 4), size=wx.Size(434,
              156), style=0)

        self.cbClose = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Cancel),
              id=wxID_VTNETSECXMLGUIMASTERLOGDIALOGCBCLOSE, label=_(u'Close'),
              name=u'cbClose', parent=self, pos=wx.Point(183, 176),
              size=wx.Size(76, 30), style=0)
        self.cbClose.SetMinSize(wx.Size(-1, -1))
        self.cbClose.Bind(wx.EVT_BUTTON, self.OnCbCloseButton,
              id=wxID_VTNETSECXMLGUIMASTERLOGDIALOGCBCLOSE)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        self._init_ctrls(parent)
        try:
            icon = wx.EmptyIcon()
            icon.CopyFromBitmap(vtArt.getBitmap(vtArt.Log))
            self.SetIcon(icon)
        except:
            vtLog.vtLngTB(self.sOrigin)
        #self.cbClose.SetBitmapLabel(vtArt.getBitmap(vtArt.Cancel))
    def __del__Old(self):
        #vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
        pass
    def OnCbCloseButton(self, event):
        self.Show(False)
        event.Skip()
    def AddLog(self,sTime,name,msg,iStatusPrev,iStatus):
        self.pnLog.AddLog(sTime,name,msg,iStatusPrev,iStatus)
