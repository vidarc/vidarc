#Boa:Dialog:vNetCfgDialog
#----------------------------------------------------------------------------
# Name:         vNetCfgDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vNetCfgDialog.py,v 1.6 2013/12/09 08:06:54 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import sys
if sys.version_info >= (2,6,0):
    import hashlib
    def newSha():
        return hashlib.sha1()
else:
    import sha
    def newSha():
        return sha.new()

from vidarc.tool.net.vNetPlacePanel import vNetPlacePanel
from vidarc.tool.net.vtNetXmlClt import vtNetXmlCltSock
from vidarc.tool.net.vtNetClt import vtNetClt

import  vidarc.tool.log.vtLog as vtLog

import images

VERBOSE=0

def create(parent):
    return vNetCfgDialog(parent)

[wxID_VNETCFGDIALOG, wxID_VNETCFGDIALOGCBADD, wxID_VNETCFGDIALOGCBAPPLY, 
 wxID_VNETCFGDIALOGCBBROWSE, wxID_VNETCFGDIALOGCBCANCEL, 
 wxID_VNETCFGDIALOGCBDEL, wxID_VNETCFGDIALOGCBDELALIAS, 
 wxID_VNETCFGDIALOGCBDELHOST, wxID_VNETCFGDIALOGCBTEST, 
 wxID_VNETCFGDIALOGCHCAPPL, wxID_VNETCFGDIALOGCHCAUTOCONN, 
 wxID_VNETCFGDIALOGCHCCONN, wxID_VNETCFGDIALOGCHCHOST, 
 wxID_VNETCFGDIALOGCHCIDENTICALLOGIN, wxID_VNETCFGDIALOGCHCMAINALIAS, 
 wxID_VNETCFGDIALOGCHCSTORELOGIN, wxID_VNETCFGDIALOGLBLALIAS, 
 wxID_VNETCFGDIALOGLBLAPPL, wxID_VNETCFGDIALOGLBLCONN, 
 wxID_VNETCFGDIALOGLBLHOST, wxID_VNETCFGDIALOGLBLHST, 
 wxID_VNETCFGDIALOGLBLMAINALIAS, wxID_VNETCFGDIALOGLBLNAME, 
 wxID_VNETCFGDIALOGLBLPASSWD, wxID_VNETCFGDIALOGLBLPATH, 
 wxID_VNETCFGDIALOGLBLUSR, wxID_VNETCFGDIALOGLBSTATE, 
 wxID_VNETCFGDIALOGTXTALIAS, wxID_VNETCFGDIALOGTXTHOST, 
 wxID_VNETCFGDIALOGTXTNAME, wxID_VNETCFGDIALOGTXTPASSWD, 
 wxID_VNETCFGDIALOGTXTPATH, wxID_VNETCFGDIALOGTXTPORT, 
 wxID_VNETCFGDIALOGTXTUSR, 
] = [wx.NewId() for _init_ctrls in range(34)]

class vNetCfgDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VNETCFGDIALOG, name=u'vNetCfgDialog',
              parent=prnt, pos=wx.Point(320, 167), size=wx.Size(448, 381),
              style=wx.DEFAULT_DIALOG_STYLE, title=u'vNet Config Dialog')
        self.SetClientSize(wx.Size(440, 354))

        self.lblPath = wx.StaticText(id=wxID_VNETCFGDIALOGLBLPATH,
              label=u'Path', name=u'lblPath', parent=self, pos=wx.Point(8, 16),
              size=wx.Size(22, 13), style=wx.TE_READONLY)

        self.txtPath = wx.TextCtrl(id=wxID_VNETCFGDIALOGTXTPATH,
              name=u'txtPath', parent=self, pos=wx.Point(40, 12),
              size=wx.Size(312, 21), style=0, value=u'')

        self.cbBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VNETCFGDIALOGCBBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse', name=u'cbBrowse',
              parent=self, pos=wx.Point(360, 8), size=wx.Size(76, 30), style=0)
        self.cbBrowse.Bind(wx.EVT_BUTTON, self.OnCbBrowseButton,
              id=wxID_VNETCFGDIALOGCBBROWSE)

        self.lblMainAlias = wx.StaticText(id=wxID_VNETCFGDIALOGLBLMAINALIAS,
              label=u'Alias', name=u'lblMainAlias', parent=self,
              pos=wx.Point(40, 56), size=wx.Size(22, 13), style=0)

        self.chcMainAlias = wx.Choice(choices=[],
              id=wxID_VNETCFGDIALOGCHCMAINALIAS, name=u'chcMainAlias',
              parent=self, pos=wx.Point(72, 52), size=wx.Size(176, 21),
              style=0)
        self.chcMainAlias.Bind(wx.EVT_CHOICE, self.OnChcMainAliasChoice,
              id=wxID_VNETCFGDIALOGCHCMAINALIAS)

        self.cbDelAlias = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VNETCFGDIALOGCBDELALIAS,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Del', name=u'cbDelAlias',
              parent=self, pos=wx.Point(360, 48), size=wx.Size(76, 30),
              style=0)
        self.cbDelAlias.Bind(wx.EVT_BUTTON, self.OnCbDelAliasButton,
              id=wxID_VNETCFGDIALOGCBDELALIAS)

        self.chcAppl = wx.Choice(choices=[], id=wxID_VNETCFGDIALOGCHCAPPL,
              name=u'chcAppl', parent=self, pos=wx.Point(72, 116),
              size=wx.Size(128, 21), style=wx.CB_SORT)
        self.chcAppl.Bind(wx.EVT_CHOICE, self.OnChcApplChoice,
              id=wxID_VNETCFGDIALOGCHCAPPL)

        self.chcAutoConn = wx.CheckBox(id=wxID_VNETCFGDIALOGCHCAUTOCONN,
              label=u'auto connect', name=u'chcAutoConn', parent=self,
              pos=wx.Point(208, 120), size=wx.Size(96, 13), style=0)
        self.chcAutoConn.SetValue(False)
        self.chcAutoConn.Bind(wx.EVT_CHECKBOX, self.OnChcAutoConnCheckbox,
              id=wxID_VNETCFGDIALOGCHCAUTOCONN)

        self.chcConn = wx.Choice(choices=[], id=wxID_VNETCFGDIALOGCHCCONN,
              name=u'chcConn', parent=self, pos=wx.Point(72, 88),
              size=wx.Size(176, 21), style=0)
        self.chcConn.Bind(wx.EVT_CHOICE, self.OnChcConnChoice,
              id=wxID_VNETCFGDIALOGCHCCONN)

        self.cbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VNETCFGDIALOGCBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Del', name=u'cbDel',
              parent=self, pos=wx.Point(360, 84), size=wx.Size(76, 30),
              style=0)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VNETCFGDIALOGCBDEL)

        self.lblName = wx.StaticText(id=wxID_VNETCFGDIALOGLBLNAME,
              label=u'Name', name=u'lblName', parent=self, pos=wx.Point(32,
              184), size=wx.Size(28, 13), style=0)

        self.txtName = wx.TextCtrl(id=wxID_VNETCFGDIALOGTXTNAME,
              name=u'txtName', parent=self, pos=wx.Point(72, 180),
              size=wx.Size(160, 21), style=0, value=u'')

        self.cbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VNETCFGDIALOGCBADD,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'cbAdd',
              parent=self, pos=wx.Point(360, 176), size=wx.Size(76, 30),
              style=0)
        self.cbAdd.Bind(wx.EVT_BUTTON, self.OnCbAddButton,
              id=wxID_VNETCFGDIALOGCBADD)

        self.lbState = wx.StaticBitmap(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VNETCFGDIALOGLBSTATE, name=u'lbState', parent=self,
              pos=wx.Point(192, 284), size=wx.Size(16, 16), style=0)

        self.lblAppl = wx.StaticText(id=wxID_VNETCFGDIALOGLBLAPPL,
              label=u'Application', name=u'lblAppl', parent=self,
              pos=wx.Point(8, 120), size=wx.Size(52, 15), style=0)

        self.lblConn = wx.StaticText(id=wxID_VNETCFGDIALOGLBLCONN,
              label=u'Connection', name=u'lblConn', parent=self, pos=wx.Point(8,
              92), size=wx.Size(54, 13), style=0)

        self.lblHst = wx.StaticText(id=wxID_VNETCFGDIALOGLBLHST, label=u'Host',
              name=u'lblHst', parent=self, pos=wx.Point(32, 152),
              size=wx.Size(22, 13), style=0)

        self.chcHost = wx.Choice(choices=[], id=wxID_VNETCFGDIALOGCHCHOST,
              name=u'chcHost', parent=self, pos=wx.Point(72, 148),
              size=wx.Size(280, 21), style=0)
        self.chcHost.Bind(wx.EVT_CHOICE, self.OnChcHostChoice,
              id=wxID_VNETCFGDIALOGCHCHOST)

        self.cbDelHost = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VNETCFGDIALOGCBDELHOST,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Del', name=u'cbDelHost',
              parent=self, pos=wx.Point(360, 144), size=wx.Size(76, 30),
              style=0)
        self.cbDelHost.Bind(wx.EVT_BUTTON, self.OnCbDelHostButton,
              id=wxID_VNETCFGDIALOGCBDELHOST)

        self.lblHost = wx.StaticText(id=wxID_VNETCFGDIALOGLBLHOST,
              label=u'Host', name=u'lblHost', parent=self, pos=wx.Point(40,
              212), size=wx.Size(22, 13), style=0)

        self.txtHost = wx.TextCtrl(id=wxID_VNETCFGDIALOGTXTHOST,
              name=u'txtHost', parent=self, pos=wx.Point(72, 208),
              size=wx.Size(160, 21), style=0, value=u'')

        self.txtPort = wx.TextCtrl(id=wxID_VNETCFGDIALOGTXTPORT,
              name=u'txtPort', parent=self, pos=wx.Point(240, 208),
              size=wx.Size(84, 21), style=0, value=u'')

        self.lblAlias = wx.StaticText(id=wxID_VNETCFGDIALOGLBLALIAS,
              label=u'Alias', name=u'lblAlias', parent=self, pos=wx.Point(40,
              236), size=wx.Size(22, 13), style=0)

        self.txtAlias = wx.TextCtrl(id=wxID_VNETCFGDIALOGTXTALIAS,
              name=u'txtAlias', parent=self, pos=wx.Point(72, 232),
              size=wx.Size(160, 21), style=0, value=u'')

        self.txtUsr = wx.TextCtrl(id=wxID_VNETCFGDIALOGTXTUSR, name=u'txtUsr',
              parent=self, pos=wx.Point(72, 256), size=wx.Size(112, 21),
              style=0, value=u'')

        self.lblUsr = wx.StaticText(id=wxID_VNETCFGDIALOGLBLUSR, label=u'User',
              name=u'lblUsr', parent=self, pos=wx.Point(40, 260),
              size=wx.Size(22, 13), style=0)

        self.txtPasswd = wx.TextCtrl(id=wxID_VNETCFGDIALOGTXTPASSWD,
              name=u'txtPasswd', parent=self, pos=wx.Point(72, 280),
              size=wx.Size(112, 21), style=wx.TE_PASSWORD, value=u'')
        self.txtPasswd.Bind(wx.EVT_TEXT, self.OnTxtPasswdText,
              id=wxID_VNETCFGDIALOGTXTPASSWD)

        self.lblPasswd = wx.StaticText(id=wxID_VNETCFGDIALOGLBLPASSWD,
              label=u'Password', name=u'lblPasswd', parent=self,
              pos=wx.Point(16, 284), size=wx.Size(46, 13), style=0)

        self.cbTest = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VNETCFGDIALOGCBTEST,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Test', name=u'cbTest',
              parent=self, pos=wx.Point(216, 276), size=wx.Size(76, 30),
              style=0)
        self.cbTest.Bind(wx.EVT_BUTTON, self.OnCbTestButton,
              id=wxID_VNETCFGDIALOGCBTEST)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VNETCFGDIALOGCBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Apply', name=u'cbApply',
              parent=self, pos=wx.Point(80, 320), size=wx.Size(76, 30),
              style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VNETCFGDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VNETCFGDIALOGCBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel', name=u'cbCancel',
              parent=self, pos=wx.Point(168, 320), size=wx.Size(76, 30),
              style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VNETCFGDIALOGCBCANCEL)

        self.chcIdenticalLogin = wx.CheckBox(id=wxID_VNETCFGDIALOGCHCIDENTICALLOGIN,
              label=u'use identical', name=u'chcIdenticalLogin', parent=self,
              pos=wx.Point(328, 256), size=wx.Size(96, 13), style=0)
        self.chcIdenticalLogin.SetValue(False)
        self.chcIdenticalLogin.Bind(wx.EVT_CHECKBOX,
              self.OnChcIdenticalLoginCheckbox,
              id=wxID_VNETCFGDIALOGCHCIDENTICALLOGIN)

        self.chcStoreLogin = wx.CheckBox(id=wxID_VNETCFGDIALOGCHCSTORELOGIN,
              label=u'store', name=u'chcStoreLogin', parent=self,
              pos=wx.Point(328, 280), size=wx.Size(73, 13), style=0)
        self.chcStoreLogin.SetValue(False)
        self.chcStoreLogin.Bind(wx.EVT_CHECKBOX, self.OnChcStoreLoginCheckbox,
              id=wxID_VNETCFGDIALOGCHCSTORELOGIN)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        self.verbose=VERBOSE
        
        img=images.getStoppedBitmap()
        self.lbState.SetBitmap(img)
        
        img=images.getAddBitmap()
        self.cbAdd.SetBitmapLabel(img)
        
        img=images.getBrowseBitmap()
        self.cbBrowse.SetBitmapLabel(img)
        
        img=images.getDelBitmap()
        self.cbDel.SetBitmapLabel(img)
        self.cbDelHost.SetBitmapLabel(img)
        self.cbDelAlias.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.cbCancel.SetBitmapLabel(img)
        
        img=images.getApplyBitmap()
        self.cbApply.SetBitmapLabel(img)
        
        images.getConnectBitmap()
        self.cbTest.SetBitmapLabel(img)
        
        self.cltServ=None
        self.cltSock=None
        self.hosts=None
        self.bConnectionTested=False
    def __clear__(self):
        if self.verbose>0:
            iVerbose=vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.txtAppl.SetValue('')
        self.txtAlias.SetValue('')
        self.txtHost.SetValue('')
        self.txtPort.SetValue('')
        self.txtUsr.SetValue('')
        self.txtPasswd.SetValue('')
        self.chcHost.Clear()
        self.chcAlias.Clear()
    def Set(self,appl,alias,host,port,usr,passwd):
        if self.verbose>0:
            iVerbose=vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        if self.txtHost.IsEditable():
            self.txtHost.SetValue(host)
        if self.txtAlias.IsEditable():
            self.txtAlias.SetValue(alias)
        if self.txtPort.IsEditable():
            self.txtPort.SetValue(port)
        if self.txtUsr.IsEditable():
            self.txtUsr.SetValue(usr)
        if self.txtPasswd.IsEditable():
            self.txtPasswd.SetValue('')
        if len(passwd)>0:
            self.passwd=passwd
        else:
            self.passwd=None
    def __showTxt__(self,flag):
        if self.verbose>0:
            iVerbose=vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        if flag:
            self.chcAlias.Show(False)
            self.chcHost.Show(False)
            self.txtHost.Show(True)
            self.txtPort.Show(True)
            self.txtAlias.Show(True)
            self.hosts=None
        else:
            self.chcAlias.Show(True)
            self.chcHost.Show(True)
            self.txtHost.Show(False)
            self.txtPort.Show(False)
            self.txtAlias.Show(False)
            self.hosts=[]
    def __fillChoices__(self):
        if self.verbose>0:
            iVerbose=vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        appl=self.chcAppl.GetStringSelection()
        node=self.doc.getChild(self.cfgNode,appl)
        if node is None:
            self.__showTxt__(True)
        self.hosts=[]
        for c in self.doc.getChilds(node,'host'):
            host=self.doc.getNodeText(c,'name')
            port=self.doc.getNodeText(c,'port')
            lst=[]
            for a in self.doc.getChilds(c,'alias'):
                lst.append(self.doc.getText(a))
            self.hosts.append([host,port,lst])
        def compFunc(a,b):
            return cmp(a[0],b[0])
        self.hosts.sort(compFunc)
        self.chcHost.Clear()
        for h in self.hosts:
            self.chcHost.Append(h[0])
        self.chcHost.SetSelection(0)
        wx.CallAfter(self.OnChcHostChoice,None)
    def __updateAlias__(self):
        if self.verbose>0:
            iVerbose=vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        i=self.chcHost.GetSelection()
        self.chcAlias.Clear()
        if i<0:
            return
        if self.hosts is not None:
            for a in self.hosts[i][2]:
                self.chcAlias.Append(a)
    def __enableHostWidget__(self,flag):
        if self.verbose>0:
            iVerbose=vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.txtHost.Enable(flag)
        self.txtHost.SetEditable(flag)
        self.txtPort.Enable(flag)
        self.txtPort.SetEditable(flag)
        self.txtAlias.Enable(flag)
        self.txtAlias.SetEditable(flag)
        self.txtUsr.Enable(flag)
        self.txtUsr.SetEditable(flag)
        self.txtPasswd.Enable(flag)
        self.txtPasswd.SetEditable(flag)
        if flag==False:
            self.txtHost.SetValue('')
            self.txtPort.SetValue('')
            self.txtAlias.SetValue('')
            self.txtUsr.SetValue('')
            self.txtPasswd.SetValue('')
            self.passwd=''
        
    def __setHostNode__(self,n,silent=False):
        if self.verbose>0:
            iVerbose=vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        bAdd=False
        if n is None:
            return bAdd
        s=self.doc.getNodeText(n,'host')
        if len(s)<=0:
            if silent==False:
                self.txtHost.SetValue(u'')
                self.txtHost.Enable(True)
                self.txtHost.SetEditable(True)
            bAdd=True
        else:
            if silent==False:
                self.txtHost.SetValue(s)
                self.txtHost.Enable(False)
                self.txtHost.SetEditable(False)
        s=self.doc.getNodeText(n,'port')
        if len(s)<=0:
            if silent==False:
                self.txtPort.SetValue(u'')
                self.txtPort.Enable(True)
                self.txtPort.SetEditable(True)
            bAdd=True
        else:
            if silent==False:
                self.txtPort.SetValue(s)
                self.txtPort.Enable(False)
                self.txtPort.SetEditable(False)
        s=self.doc.getNodeText(n,'alias')
        if len(s)<=0:
            if silent==False:
                self.txtAlias.SetValue(u'')
                self.txtAlias.Enable(True)
                self.txtAlias.SetEditable(True)
            bAdd=True
        else:
            if silent==False:
                self.txtAlias.SetValue(s)
                self.txtAlias.Enable(False)
                self.txtAlias.SetEditable(False)
        s=self.doc.getNodeText(n,'usr')
        if len(s)<=0:
            if silent==False:
                self.txtUsr.SetValue(u'')
                self.txtUsr.Enable(True)
                self.txtUsr.SetEditable(True)
            bAdd=True
        else:
            if silent==False:
                self.txtUsr.SetValue(s)
                self.txtUsr.Enable(True)
                self.txtUsr.SetEditable(True)
        s=self.doc.getNodeText(n,'passwd')
        if len(s)<=0:
            if silent==False:
                self.txtPasswd.SetValue(u'')
                self.txtPasswd.Enable(True)
                self.txtPasswd.SetEditable(True)
                self.passwd=s
            bAdd=True
        else:
            if silent==False:
                self.txtPasswd.SetValue(u'')
                self.txtPasswd.Enable(True)
                self.txtPasswd.SetEditable(True)
                self.passwd=s
            #bAdd=True
        if self.mainAppl==self.GetAppl():
            self.txtHost.Enable(True)
            self.txtHost.SetEditable(True)
            self.txtPort.Enable(True)
            self.txtPort.SetEditable(True)
            self.txtAlias.Enable(True)
            self.txtAlias.SetEditable(True)
            self.txtUsr.Enable(True)
            self.txtUsr.SetEditable(True)
            self.txtPasswd.Enable(True)
            self.txtPasswd.SetEditable(True)
                
        return bAdd
    def UpdateConfig(self,doc,lst,appls,appl,cfgNode):
        if self.nodeBase is None:
            return
        if self.connNode is None:
            return
        if self.cfgNode is not None:
            for a in appls[1:]:
                bAdd=False
                n=self.doc.getChild(cfgNode,a)
                childs=self.doc.getChilds(self.nodeBase,a)
                bAutoConnect=''
                for c in childs:
                    bAutoConnect=self.doc.getNodeText(c,'autoconnect')
                    self.doc.deleteNode(c,self.nodeBase)
                nn=self.doc.cloneNode(n,True)
                self.doc.appendChild(self.connNode,nn)
                self.doc.setNodeText(nn,'autoconnect',bAutoConnect)
    def SetNode(self,doc,lst,appls,appl,cfgNode):
        if self.verbose>0:
            iVerbose=vtLog.vtLngCallStack(self,vtLog.DEBUG,'appl=%s lst=%s'%(appl,lst))
            #vtLog.vtLngCallStack(self,vtLog.DEBUG,'cfgNode=%s'%(cfgNode))
        self.hosts=None
        self.doc=doc
        self.nodeBase=None
        self.node=None
        self.nodeBase=self.doc.getChildByLstForced(self.doc.getRoot(),lst)
        self.node=self.nodeBase
        self.cfgNode=cfgNode
        self.aliasNode=None
        self.connNode=None
        self.applNode=None
        self.hostNode=None
        self.mainAppl=appls[0]
        
        sPath=self.doc.getNodeText(self.nodeBase,'path')
        self.txtPath.SetValue(sPath)
        
        #self.chcMainAlias.Clear()
        #if self.node is None:
        #    return
        
        self.appl=appl
        self.chcAppl.Clear()
        #self.chcAppl.Append('+++')
        #self.chcAppl.Append('---')
        self.chcAppl.Append(self.mainAppl)
        for a in appls[1:]:
            self.chcAppl.Append(a)
        if self.cfgNode is not None:
                #for host in self.doc.getChilds(n,'host'):
                #    if self.__setHostNode__(host,True):
                #        bAdd=True
                #if bAdd:
                #    self.chcAppl.Append(a)
            pass
        for it in lst:
            node=self.doc.getChildForced(self.nodeBase,it)
            self.doc.AlignNode(node)
        self.Set(appl,'','','50000','','')
        #self.chcAppl.SetStringSelection('---')
        #wx.CallAfter(self.OnChcConnChoice,None)
        self.__showMainAlias__()
        return
        
        #node=self.doc.getChildForced(node,self.mainAppl)
        self.doc.AlignNode(self.nodeBase)
        self.__showMainAlias__()
        
        
    def SelAppl(self,appl):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'appl=%s conn:%s'%(appl,self.connNode))
        try:
            self.appl=appl
            self.chcAppl.SetStringSelection(appl)
            if self.verbose>0:
                vtLog.vtLngCallStack(self,vtLog.DEBUG,'application set')
            
            wx.CallAfter(self.OnChcApplChoice,None)
        except Exception,list:
            if self.verbose>0:
                vtLog.vtLngCallStack(self,vtLog.DEBUG,'exception:%s',list)
            pass
    def __showMainAlias__(self):
        if self.verbose>0:
            iVerbose=vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.aliasNode=None
        self.chcMainAlias.Clear()
        self.chcMainAlias.Append('---')
        childs=self.doc.getChilds(self.node,'alias')
        self.lstAlias=[]
        for c in childs:
            sName=self.doc.getNodeText(c,'name')
            sVal=self.doc.getNodeText(c,'recent')
            self.lstAlias.append([sName,sVal,c])
        def compFunc(a,b):
            i=cmp(a[1],b[1])
            if i==0:
                return cmp(a[0],b[0])
            else:
                return i
        self.lstAlias.sort(compFunc)
        i=0
        for it in self.lstAlias:
            self.chcMainAlias.Append(it[0])
            self.chcMainAlias.SetClientData(i,it)
            i+=1
        if len(self.lstAlias)>0:
            self.chcMainAlias.SetSelection(1)
            self.aliasNode=self.lstAlias[0][-1]
            self.chcConn.Enable(True)
        else:
            self.chcMainAlias.SetSelection(0)
            self.aliasNode=None
        #wx.CallAfter(self.OnChcMainAliasChoice,None)
        self.__showConn__()
        if 1==0:
            try:
                self.chcMainAlias.SetSelection(0)
                #self.chcAppl.SetStringSelection(self.appl)
                wx.CallAfter(self.OnChcMainAliasChoice,None)
            except:
                self.chcAppl.SetStringSelection('+++')
                pass
            if i==0:
                self.chcAppl.SetStringSelection('+++')
                wx.CallAfter(self.OnChcApplChoice,None)
        
        #self.bConnectionTested=False
        #self.panel.SetNode(doc,node,appl)
    def __showConn__(self):
        if self.verbose>0:
            iVerbose=vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.connNode=None
        self.hostNode=None
        self.chcConn.Clear()
        self.chcConn.Append('---')
        sIdentical=self.doc.getNodeText(self.aliasNode,'identical_login')
        bFlag=False
        if sIdentical=='1':
            bFlag=True
        elif sIdentical=='0':
            bFlag=False
        else:
            self.doc.setNodeText(self.aliasNode,'identical_login','1')
            bFlag=True
        self.chcIdenticalLogin.SetValue(bFlag)
        
        sStore=self.doc.getNodeText(self.aliasNode,'store_login')
        bFlag=False
        if sStore=='1':
            bFlag=True
        elif sStore=='0':
            bFlag=False
        else:
            self.doc.setNodeText(self.aliasNode,'store_login','1')
            bFlag=True
        self.chcStoreLogin.SetValue(bFlag)
        
        childs=self.doc.getChilds(self.aliasNode,'connection')
        self.lstConn=[]
        for c in childs:
            sName=self.doc.getNodeText(c,'name')
            sVal=self.doc.getNodeText(c,'recent')
            self.lstConn.append([sName,sVal,c])
        def compFunc(a,b):
            i=cmp(a[1],b[1])
            if i==0:
                return cmp(a[0],b[0])
            else:
                return i
        self.lstConn.sort(compFunc)
        i=0
        for it in self.lstConn:
            self.chcConn.Append(it[0])
            self.chcConn.SetClientData(i,it)
            i+=1
        if len(self.lstConn)>0:
            self.chcConn.SetSelection(1)
            self.connNode=self.lstConn[0][-1]
            self.chcAppl.Enable(True)
            #self.__enableHostWidget__(True)
        else:
            self.chcConn.SetSelection(0)
            self.chcAppl.Enable(False)
            self.connNode=None
        self.__showAppl__()
        #wx.CallAfter(self.OnChcConnChoice,None)
        if 1==0:
            try:
                self.chcConn.SetSelection(0)
                #self.chcAppl.SetStringSelection(self.appl)
                wx.CallAfter(self.OnChcConnChoice,None)
            except:
                self.chcAppl.SetStringSelection('---')
                pass
            if i==0:
                self.chcAppl.SetStringSelection('---')
                wx.CallAfter(self.OnChcApplChoice,None)
        
        #self.bConnectionTested=False
        #self.panel.SetNode(doc,node,appl)
    def __showAppl__(self):
        if self.connNode is None:
            self.applNode=None
            self.chcAppl.SetStringSelection(self.mainAppl)
            self.__showHost__(None)
            self.__enableHostWidget__(False)
            self.txtName.Enable(True)
            self.chcHost.Enable(False)
            self.cbAdd.Enable(True)
            self.cbDelHost.Enable(False)
            return
        a=self.chcAppl.GetStringSelection()
        self.applNode=self.doc.getChild(self.connNode,a)
        if self.applNode is None:
            a=self.mainAppl
        else:
            h=self.doc.getChild(self.applNode,'host')
            if h is None:
                a=self.mainAppl
        
        self.chcAppl.SetStringSelection(a)
        
        self.applNode=None
        if a==self.mainAppl:
            bAdd=False#True
            self.__enableHostWidget__(True)
            self.applNode=self.doc.getChildForced(self.connNode,a)
            #n=self.doc.getChildForced(self.connNode,self.applNode)
            h=self.doc.getChild(self.applNode,'host')  
            if h is None:
                # needed for initial config file
                self.txtName.SetValue('main')
                self.OnCbAddButton(None)
            self.__showHost__(self.applNode)
            self.txtName.Enable(False)
            self.chcHost.Enable(False)
            self.cbDelHost.Enable(False)
            self.cbAdd.Enable(False)
        else:
            bAdd=False
            self.txtName.Enable(False)
            self.chcHost.Enable(True)
            self.cbDelHost.Enable(False)
            self.cbAdd.Enable(False)
            if self.connNode is not None:
                self.applNode=self.doc.getChildForced(self.connNode,a)
                self.__showHost__(self.applNode)
                #self.__setHostNode__(host,True)
        self.__close__()
    def getAliasName(self,host,port,alias):
        return host+':'+port+'//'+alias
    def __showHost__(self,n):
        if self.verbose>0:
            iVerbose=vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.hostNode=None
        self.chcHost.Clear()
        if n is None:
            return
        sAuto=self.doc.getNodeText(n,'autoconnect')
        bConn=False
        if sAuto=='1':
            bConn=True
        elif sAuto=='0':
            bConn=False
        else:
            self.doc.setNodeText(n,'autoconnect','1')
            bConn=True
        self.chcAutoConn.SetValue(bConn)
        
        childs=self.doc.getChilds(n,'host')
        self.lstHost=[]
        for c in childs:
            sName=self.doc.getNodeText(c,'name')
            sVal=self.doc.getNodeText(c,'recent')
            sPort=self.doc.getNodeText(c,'port')
            sAlias=self.doc.getNodeText(c,'alias')
            sUsr=self.doc.getNodeText(c,'usr')
            sPasswd=self.doc.getNodeText(c,'passwd')
            self.lstHost.append([sName,sVal,sPort,sAlias,sUsr,sPasswd,c])
        def compFunc(a,b):
            i=cmp(a[1],b[1])
            if i==0:
                return cmp(a[0],b[0])
            else:
                return i
        self.lstHost.sort(compFunc)
        i=0
        for it in self.lstHost:
            self.chcHost.Append(self.getAliasName(it[0],it[2],it[3]))
            self.chcHost.SetClientData(i,it)
            i+=1
        try:
            self.chcHost.SetSelection(0)
            #self.chcAppl.SetStringSelection(self.appl)
            wx.CallAfter(self.OnChcHostChoice,None)
        except:
            #self.chcAppl.SetStringSelection('---')
            pass

    def __close__(self):
        if self.verbose>0:
            iVerbose=vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        if self.cltSock is not None:
            self.cltSock.Close()
            self.cltSock=None
        if self.cltServ is not None:
            self.cltServ.Stop()
            del self.cltServ
        self.cltServ=None
    def OnCbApplyButton(self, event):
        if self.verbose>0:
            iVerbose=vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.__close__()
        idx=self.chcMainAlias.GetSelection()
        if idx>1:
            idx-=1
            iRec=1
            for i in range(len(self.lstAlias)):
                if idx==i:
                    it=self.lstAlias[i]
                    c=it[2]
                    self.doc.setNodeText(c,'recent',str(0))
                    if self.chcIdenticalLogin.GetValue():
                        self.doc.setNodeText(c,'identical_login','1')
                    else:
                        self.doc.setNodeText(c,'identical_login','0')
                    if self.chcStoreLogin.GetValue():
                        self.doc.setNodeText(c,'store_login','1')
                    else:
                        self.doc.setNodeText(c,'store_login','0')
                else:
                    it=self.lstAlias[i]
                    c=it[2]
                    self.doc.setNodeText(c,'recent',str(iRec))
                    iRec+=1
        #else:
        #    c=self.aliasNode
        #    if self.chcIdenticalLogin.GetValue():
        #        self.doc.setNodeText(c,'identical_login','1')
        #    else:
        #        self.doc.setNodeText(c,'identical_login','0')
        #    if self.chcStoreLogin.GetValue():
        #        self.doc.setNodeText(c,'store_login','1')
        #    else:
        #        self.doc.setNodeText(c,'store_login','0')
        #    self.doc.AlignNode(c,iRec=3)
        idx=self.chcConn.GetSelection()
        if idx>1:
            idx-=1
            iRec=1
            for i in range(len(self.lstConn)):
                if idx==i:
                    it=self.lstConn[i]
                    c=it[2]
                    self.doc.setNodeText(c,'recent',str(0))
                else:
                    it=self.lstConn[i]
                    c=it[2]
                    self.doc.setNodeText(c,'recent',str(iRec))
                    iRec+=1
        a=self.chcAppl.GetStringSelection()
        if self.mainAppl!=a:
            idx=self.chcHost.GetSelection()
            if idx>=0:
                #idx-=1
                iRec=1
                for i in range(len(self.lstHost)):
                    if idx==i:
                        it=self.lstHost[i]
                        c=it[-1]
                        self.doc.setNodeText(c,'recent',str(0))
                    else:
                        it=self.lstHost[i]
                        c=it[-1]
                        self.doc.setNodeText(c,'recent',str(iRec))
                        iRec+=1
        for c in self.doc.getChilds(self.connNode):
            if self.chcStoreLogin.GetValue()==False:
                    lstHost=[]
                    for h in self.doc.getChilds(c,'host'):
                        sVal=self.doc.getNodeText(h,'recent')
                        lstHost.append([sVal,h])
                    def compFunc(a,b):
                        i=cmp(a[0],b[0])
                        return i
                    lstHost.sort(compFunc)
                    try:
                        hostNode=lstHost[0][1]
                        self.doc.delNode(self.doc.getChild(hostNode,'usr'))
                        self.doc.delNode(self.doc.getChild(hostNode,'passwd'))
                    except:
                        pass
        
        if self.bConnectionTested:
            self.EndModal(2)
        else:
            self.EndModal(1)
        event.Skip()
    def OnCbCancelButton(self, event):
        if self.verbose>0:
            iVerbose=vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.__close__()
        self.EndModal(0)
        event.Skip()
    def OnCbTestButton(self, event):
        if self.verbose>0:
            iVerbose=vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        appl=self.GetAppl()
        alias=self.GetAlias()
        host=self.GetHost()
        port=self.GetPort()
        usr=self.GetUsr()
        passwd=self.GetPasswd()
        if self.txtHost.IsEditable():
            self.doc.setNodeText(self.hostNode,'recent','0')
            self.doc.setNodeText(self.hostNode,'name',host)
            self.doc.setNodeText(self.hostNode,'host',host)
        if self.txtPort.IsEditable():
            self.doc.setNodeText(self.hostNode,'port',port)
        if self.txtAlias.IsEditable():
            self.doc.setNodeText(self.hostNode,'alias',alias)
        if self.hostNode is not None:
            if self.chcIdenticalLogin.GetValue():
                for c in self.doc.getChilds(self.connNode):
                    lstHost=[]
                    for h in self.doc.getChilds(c,'host'):
                        sVal=self.doc.getNodeText(h,'recent')
                        lstHost.append([sVal,h])
                    def compFunc(a,b):
                        i=cmp(a[0],b[0])
                        return i
                    lstHost.sort(compFunc)
                    try:
                        hostNode=lstHost[0][1]
                        if self.chcStoreLogin.GetValue():
                            self.doc.setNodeText(hostNode,'usr',usr)
                            self.doc.setNodeText(hostNode,'passwd',passwd)
                        else:
                            self.doc.delNode(self.doc.getChild(hostNode,'usr'))
                            self.doc.delNode(self.doc.getChild(hostNode,'passwd'))
                            
                    except:
                        pass
            else:
                if self.chcStoreLogin.GetValue():
                    self.doc.setNodeText(self.hostNode,'usr',usr)
                    self.doc.setNodeText(self.hostNode,'passwd',passwd)
                else:
                    self.doc.delNode(self.doc.getChild(self.hostNode,'host'))
                    self.doc.delNode(self.doc.getChild(self.hostNode,'passwd'))
                    
        if self.verbose>0:
            vtLog.vtLog(self,'appl:%s alias:%s host:%s port:%s'%(appl,alias,host,port))
            vtLog.vtLog(self,'usr:%s passwd:%s'%(usr,passwd))
        try:
            iPort=int(port)
        except:
            return
        self.cltServ=vtNetClt(self,host,iPort,vtNetXmlCltSock)
        self.cltServ.Connect()
        event.Skip()
    def NotifyConnect(self):
        img=images.getConnectBitmap()
        self.lbState.SetBitmap(img)
    def NotifyConnectionFault(self):
        img=images.getFaultBitmap()
        self.lbState.SetBitmap(img)
    
    def NotifyConnected(self):
        self.cltSock=self.cltServ.GetSocketInstance()
        if self.cltSock is None:
            return
        appl=self.GetAppl()
        alias=self.GetAlias()
        usr=self.GetUsr()
        passwd=self.GetPasswd()
        self.cltSock.SetParent(self)
        img=images.getLoginBitmap()
        self.lbState.SetBitmap(img)
        self.cltSock.LoginEncoded(usr,passwd)
        self.cltSock.Alias(appl,alias)
    def NotifyLogin(self):
        img=images.getLoginBitmap()
        self.lbState.SetBitmap(img)
    def NotifyLoggedIn(self,oLogin):
        img=images.getRunningBitmap()
        self.lbState.SetBitmap(img)
        self.bConnectionTested=True
    def __connectionClosed__(self,bEstablished=False):
        #del self.cltServ
        self.cltServ=None
        self.cltSock=None
        self.host=None
        self.port=None
        img=images.getStoppedBitmap()
        self.lbState.SetBitmap(img)

    def OnCbAddButton(self, event):
        if self.verbose>0:
            iVerbose=vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        sName=self.txtName.GetValue()
        
        if self.aliasNode is None:
            n=self.doc.createChildByLst(self.node,'alias',
                    [{'tag':'name','val':sName},
                     {'tag':'recent','val':'0'},
                ])
            i=1
            for it in self.lstAlias:
                it[1]=i
                c=it[2]
                self.doc.setNodeText(c,'recent',str(i))
                i+=1
            self.doc.AlignNode(n,iRec=3)
            self.__showMainAlias__()
            return
        if self.connNode is None:
            n=self.doc.createChildByLst(self.aliasNode,'connection',
                    [{'tag':'name','val':sName},
                     {'tag':'recent','val':'0'},
                ])
            applNode=self.doc.getChildForced(n,self.mainAppl)
            h=self.doc.createChildByLst(applNode,'host',
                    [{'tag':'name','val':'main'},
                     {'tag':'recent','val':'0'},
                     {'tag':'appl','val':self.mainAppl},
                     {'tag':'alias','val':''},
                     {'tag':'host','val':''},
                     {'tag':'port','val':'50000'},
                     {'tag':'usr','val':''},
                     {'tag':'passwd','val':''}
                    ])
            
            i=1
            for it in self.lstConn:
                it[1]=i
                c=it[2]
                self.doc.setNodeText(c,'recent',str(i))
                i+=1
            self.doc.AlignNode(n,iRec=3)
            self.__showConn__()
            return
        return
        appl=self.GetAppl()
        if 1==0:
            if self.connNode is None:
                return
            if self.mainAppl==self.appl:
                return
            
            hostNode=self.doc.getChildForced(self.connNode,appl)
            
            alias=self.GetAlias()
            host=self.GetHost()
            port=self.GetPort()
            usr=self.GetUsr()
            passwd=self.GetPasswd()
            if len(sName)<=0:
                return
            n=self.doc.createChildByLst(hostNode,'host',
                    [{'tag':'name','val':sName},
                     {'tag':'recent','val':'0'},
                     {'tag':'appl','val':appl},
                     {'tag':'alias','val':alias},
                     {'tag':'host','val':host},
                     {'tag':'port','val':port},
                     {'tag':'usr','val':usr},
                     {'tag':'passwd','val':passwd}
                    ])
            self.doc.AlignNode(hostNode,iRec=2)
            self.__showHost__(hostNode)
        self.txtName.SetValue(u'')
        if event is not None:
            event.Skip()

    def OnCbDelButton(self, event):
        if self.verbose>0:
            iVerbose=vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        #appl=self.GetAppl()
        appl=self.chcConn.GetStringSelection()
        if appl!='---':
            idx=self.chcConn.GetSelection()
            if idx<0:
                return
            try:
                c=self.lstConn[idx-1][2]
                self.doc.deleteNode(c)
            except:
                pass
            self.__showConn__()
        event.Skip()

    def OnChcMainAliasChoice(self, event):
        if self.verbose>0:
            iVerbose=vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        idx=self.chcMainAlias.GetSelection()
        if idx<1:
            self.aliasNode=None
            self.__showConn__()
            self.__enableHostWidget__(False)
            self.txtName.Enable(True)
            #self.txtAlias.Enable(True)
            self.chcConn.Enable(False)
            self.chcAppl.Enable(False)
            self.chcHost.Enable(False)
            self.cbDelHost.Enable(False)
            self.cbAdd.Enable(True)
            return
        try:
            c=self.lstAlias[idx-1][-1]
        except:
            self.aliasNode=None
            return
        self.aliasNode=c
        self.__close__()
        self.__showConn__()
        
        if 1==0:
            self.chcConn.Clear()
            if self.node is None:
                return
            sPath=self.doc.getNodeText(self.nodeBase,'path')
            self.txtPath.SetValue(sPath)
            #node=self.doc.getChildForced(node,self.mainAppl)
            self.doc.AlignNode(self.nodeBase)
            self.__showConn__()
            #wx.CallAfter(self.OnChcApplChoice,None)
        if event is not None:
            event.Skip()


    def OnChcConnChoice(self, event):
        if self.verbose>0:
            iVerbose=vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        idx=self.chcConn.GetSelection()
        if idx<1:
            self.connNode=None
            self.__showHost__(None)
            self.__enableHostWidget__(False)
            self.txtName.Enable(True)
            self.chcHost.Enable(False)
            self.cbDelHost.Enable(False)
            self.cbAdd.Enable(True)
            return
        try:
            c=self.lstConn[idx-1][-1]
        except:
            self.connNode=None
            return
        self.connNode=c
        self.__close__()
        self.txtName.SetValue('')
        self.chcAppl.SetStringSelection(self.appl)
        wx.CallAfter(self.OnChcApplChoice,None)
        #appl=self.chcAppl.GetStringSelection()
        
        #alias=self.doc.getNodeText(c,'alias')
        #host=self.doc.getNodeText(c,'host')
        #port=self.doc.getNodeText(c,'port')
        #usr=self.doc.getNodeText(c,'usr')
        #passwd=self.doc.getNodeText(c,'passwd')
        #self.Set(appl,alias,host,port,usr,passwd)
        if event is not None:
            event.Skip()

    def OnCbBrowseButton(self, event):
        dlg = wx.DirDialog(self, "Choose the cache directory:",
                          style=wx.DD_DEFAULT_STYLE|wx.DD_NEW_DIR_BUTTON)
        try:
            if self.verbose>0:
                vtLog.vtLngCallStack(self,vtLog.DEBUG,'appl=%s %s'%(self.appl,self.nodeBase))
            
            tmplDN=self.doc.getNodeText(self.nodeBase,'path')
            if len(tmplDN)>0:
                dlg.SetPath(tmplDN)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.doc.setNodeText(self.nodeBase,'path',filename)
                self.doc.AlignNode(self.doc.getChild(self.nodeBase,'path'))
                self.txtPath.SetValue(filename)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()

    def OnChcApplChoice(self, event):
        if self.verbose>0:
            iVerbose=vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.__showAppl__()
        return
        if self.aliasNode is not None:
            if self.connNode is not None:
                a=self.chcAppl.GetStringSelection()
                if a=='---':
                    pass
                elif a=='+++':
                    pass
                else:
                    applNode=self.doc.getChild(self.connNode,a)
                    if applNode is None:
                        a=self.mainAppl
                    else:
                        h=self.doc.getChild(applNode,'host')
                        if h is None:
                            a=self.mainAppl
            else:
                a='---'
        else:
            a='+++'
        self.chcAppl.SetStringSelection(a)
        
        
        self.hostNode=None
        self.applNode=None
        if a==self.mainAppl:
            bAdd=False#True
            self.__enableHostWidget__(True)
            self.applNode=self.doc.getChildForced(self.connNode,a)
            #n=self.doc.getChildForced(self.connNode,self.applNode)
            h=self.doc.getChild(self.applNode,'host')  
            if h is None:
                # needed for initial config file
                self.txtName.SetValue('main')
                self.OnCbAddButton(None)
            self.__showHost__(self.applNode)
            self.txtName.Enable(False)
            self.chcHost.Enable(False)
            self.cbDelHost.Enable(False)
            self.cbAdd.Enable(False)
        elif a=='+++':
            bAdd=False
            self.__showConn__()
            self.txtName.Enable(False)
            self.txtAlias.Enable(True)
            self.chcHost.Enable(False)
            self.cbDelHost.Enable(False)
            self.cbAdd.Enable(True)
            self.__enableHostWidget__(False)
            pass
        elif a=='---':
            bAdd=False
            self.__showHost__(None)
            self.txtName.Enable(True)
            self.chcHost.Enable(False)
            self.cbDelHost.Enable(False)
            self.cbAdd.Enable(True)
            self.__enableHostWidget__(False)
            pass
        else:
            bAdd=False
            self.txtName.Enable(False)
            self.chcHost.Enable(True)
            self.cbDelHost.Enable(False)
            self.cbAdd.Enable(False)
            if self.connNode is not None:
                self.applNode=self.doc.getChildForced(self.connNode,a)
                self.__showHost__(self.applNode)
                #self.__setHostNode__(host,True)
                
        self.__close__()
        if bAdd:
            node=self.doc.getChildForced(self.nodeBase,a)
            self.doc.AlignNode(node)
            self.node=node
            self.__showConn__()
        if event is not None:
            event.Skip()

    def GetAppl(self):
        return self.chcAppl.GetStringSelection()
    def GetAlias(self):
        if self.hosts is None:
            return self.txtAlias.GetValue()
        else:
            return self.chcAlias.GetStringSelection()
    def GetHost(self):
        if self.hosts is None:
            return self.txtHost.GetValue()
        else:
            return self.chcHost.GetStringSelection()
    def GetPort(self):
        if self.hosts is None:
            return self.txtPort.GetValue()
        else:
            return self.hosts[self.chcHost.GetSelection()][1]
    def GetUsr(self):
        return self.txtUsr.GetValue()
    def GetPasswd(self):
        if self.passwd is None:
            passwd=self.txtPasswd.GetValue()
            if len(passwd)>0:
                m=newSha()#sha.new()
                m.update(passwd)
                return m.hexdigest()
            else:
                return ''
        else:
            return self.passwd
    def OnTxtPasswdText(self, event):
        self.passwd=None
        event.Skip()

    def OnChcHostChoice(self, event):
        if self.verbose>0:
            iVerbose=vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        idx=self.chcHost.GetSelection()
        if idx<0:
            self.hostNode=None
            self.applNode=None
            return
        try:
            c=self.lstHost[idx][-1]
        except:
            self.hostNode=None
            self.applNode=None
            return
        if idx>0:
            iRec=1
            for i in range(len(self.lstHost)):
                if idx==i:
                    it=self.lstHost[i]
                    c=it[-1]
                    self.doc.setNodeText(c,'recent',str(0))
                else:
                    it=self.lstHost[i]
                    c=it[-1]
                    self.doc.setNodeText(c,'recent',str(iRec))
                    iRec+=1
        self.hostNode=c
        self.__setHostNode__(self.hostNode)
        
        #alias=self.doc.getNodeText(c,'alias')
        #host=self.doc.getNodeText(c,'host')
        #port=self.doc.getNodeText(c,'port')
        #usr=self.doc.getNodeText(c,'usr')
        #passwd=self.doc.getNodeText(c,'passwd')
        #self.Set(appl,alias,host,port,usr,passwd)
        
        #self.__updateAlias__()
        #self.chcAlias.SetSelection(0)
        if event is not None:
            event.Skip()

    def OnCbDelHostButton(self, event):
        if self.verbose>0:
            iVerbose=vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        event.Skip()

    def OnChcAutoConnCheckbox(self, event):
        if self.verbose>0:
            iVerbose=vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        if self.applNode is not None:
            if self.chcAutoConn.GetValue():
                self.doc.setNodeText(self.applNode,'autoconnect','1')
            else:
                self.doc.setNodeText(self.applNode,'autoconnect','0')
        event.Skip()

    def OnChcIdenticalLoginCheckbox(self, event):
        event.Skip()

    def OnChcStoreLoginCheckbox(self, event):
        event.Skip()

    def OnCbDelAliasButton(self, event):
        alias=self.chcMainAlias.GetStringSelection()
        if alias!='---':
            idx=self.chcMainAlias.GetSelection()
            c=self.lstAlias[idx-1][2]
            self.doc.deleteNode(c)
            self.__showMainAlias__()
        event.Skip()

    
