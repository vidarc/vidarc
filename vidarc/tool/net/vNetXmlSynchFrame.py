#Boa:Frame:vNetXmlSynchFrame
#----------------------------------------------------------------------------
# Name:         vNetXmlSynchFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vNetXmlSynchFrame.py,v 1.1 2005/12/11 23:04:49 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

from vidarc.tool.net.vNetXmlSynchProc import vNetXmlSynchProc
from vidarc.tool.net.vNetXmlSynchTreeList import *

def create(parent):
    return vNetXmlSynchFrame(parent)

[wxID_VNETXMLSYNCHFRAME, wxID_VNETXMLSYNCHFRAMECBOPEN, 
 wxID_VNETXMLSYNCHFRAMEPNMAIN, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vNetXmlSynchFrame(wx.Frame):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VNETXMLSYNCHFRAME,
              name=u'vNetXmlSynchFrame', parent=prnt, pos=wx.Point(205, 55),
              size=wx.Size(658, 400), style=wx.DEFAULT_FRAME_STYLE,
              title=u'vNetXmlSynch Frame')
        self.SetClientSize(wx.Size(650, 373))

        self.pnMain = wx.Panel(id=wxID_VNETXMLSYNCHFRAMEPNMAIN, name=u'pnMain',
              parent=self, pos=wx.Point(0, 0), size=wx.Size(648, 368),
              style=wx.TAB_TRAVERSAL)

        self.cbOpen = wx.Button(id=wxID_VNETXMLSYNCHFRAMECBOPEN, label=u'Open',
              name=u'cbOpen', parent=self.pnMain, pos=wx.Point(8, 8),
              size=wx.Size(75, 23), style=0)
        self.cbOpen.Bind(wx.EVT_BUTTON, self.OnCbOpenButton,
              id=wxID_VNETXMLSYNCHFRAMECBOPEN)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        self.docSynch=vNetXmlSynchProc(None)
        self.trlstSynch=vNetXmlSynchTreeList(parent=self.pnMain,
                id=wx.NewId(), name=u'trlstSynch', 
                pos=wx.Point(4, 40), size=wx.Size(600, 310), style=wx.TR_HAS_BUTTONS,
                master=False,verbose=1)
        EVT_VGTRXMLTREE_ITEM_SELECTED(self.trlstSynch,self.OnSynchTreeItemSel)
        EVT_THREAD_ADD_ELEMENTS(self.trlstSynch,self.OnSynchAddElement)
        EVT_THREAD_ADD_ELEMENTS_FINISHED(self.trlstSynch,self.OnSynchAddFinished)
        self.trlstSynch.SetDoc(self.docSynch)
    def OnSynchTreeItemSel(self,evt):
        pass
    def OnSynchAddElement(self,evt):
        pass
    def OnSynchAddFinished(self,evt):
        pass
    def OnCbOpenButton(self, event):
        dlg = wx.FileDialog(self, _("Open"), ".", "", _("XML files (*.xml)|*.xml|all files (*.*)|*.*"), wx.OPEN)
        try:
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.docSynch.Open(filename)
                self.trlstSynch.SetNode(None)
        finally:
            dlg.Destroy()

        event.Skip()
