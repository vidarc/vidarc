#Boa:FramePanel:vModifyPanel
#----------------------------------------------------------------------------
# Name:         vNetEditThread.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vNetEditThread.py,v 1.9 2007/08/06 22:53:13 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from wx import NewEventType as wxNewEventType
from wx import PyEvent as wxPyEvent
from wx import PostEvent as wxPostEvent
import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.net.vNetXmlWxGui import *

import thread,time


VERBOSE=0

# defined event for vgpXmlTree item selected
wxEVT_NET_EDIT_THREAD_ELEMENTS=wxNewEventType()
def EVT_NET_EDIT_THREAD_ELEMENTS(win,func):
    win.Connect(-1,-1,wxEVT_NET_EDIT_THREAD_ELEMENTS,func)
class wxNetThreadEditElements(wxPyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_EDIT_THREAD_ELEMENTS(<widget_name>, self.OnItemSel)
    """

    def __init__(self,iVal,iCount=-1):
        wxPyEvent.__init__(self)
        self.val=iVal
        self.count=iCount
        self.SetEventType(wxEVT_NET_EDIT_THREAD_ELEMENTS)
    def GetValue(self):
        return self.val
    def GetCount(self):
        return self.count
# defined event for vgpXmlTree item selected
wxEVT_NET_EDIT_THREAD_ELEMENTS_FINISHED=wxNewEventType()
def EVT_NET_EDIT_THREAD_ELEMENTS_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_NET_EDIT_THREAD_ELEMENTS_FINISHED,func)
class wxNetThreadEditElementsFinished(wxPyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_EDIT_THREAD_ELEMENTS_FINISHED(<widget_name>, self.OnItemSel)
    """
    def __init__(self):
        wxPyEvent.__init__(self)
        self.SetEventType(wxEVT_NET_EDIT_THREAD_ELEMENTS_FINISHED)

# defined event for vgpXmlTree item selected
wxEVT_NET_EDIT_THREAD_ELEMENTS_ABORTED=wxNewEventType()
def EVT_NET_EDIT_THREAD_ELEMENTS_ABORTED(win,func):
    win.Connect(-1,-1,wxEVT_NET_EDIT_THREAD_ELEMENTS_ABORTED,func)
class wxNetThreadEditElementsAborted(wxPyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_EDIT_THREAD_ELEMENTS_ABORTED(<widget_name>, self.OnItemSel)
    """
    def __init__(self):
        wxPyEvent.__init__(self)
        self.SetEventType(wxEVT_NET_EDIT_THREAD_ELEMENTS_ABORTED)
    

class thdNetEditThread:
    def __init__(self,par,zTimeOut=100,verbose=0):
        self.zTimeOut=zTimeOut
        self.verbose=verbose
        self.par=par
        self.doc=None
        try:
            self.widLogging=vtLog.GetPrintMsgWid(self.par)
        except:
            self.widLogging=None
        self.func=None
        self.args=()
        self.kwargs={}
        self.lLockErr=[]
        self.Clear()
        self.running=False
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        self.Clear()
        self.func=None
        self.args=()
        self.kwargs={}
    def SetFunc(self,func,*args,**kwargs):
        self.func=func
        self.args=args
        self.kwargs=kwargs
    def ScheduleEdit(self,node):
        if self.running:
            return -1
        self.lst.append(node)
        self.iCount+=1
        return 0
    def SetParent(self,par=None):
        self.par=par
    def SetDoc(self,doc):
        #if self.doc is not None:
        #    EVT_NET_XML_LOCK_DISCONNECT(self.doc,self.OnLock)
        self.doc=doc
        if self.doc is not None:
            EVT_NET_XML_LOCK(self.doc,self.OnLock)
    def Apply(self,update_gui=False):
        self.bUpdate=update_gui
        if self.iCount==0:
            if self.bUpdate:
                wxPostEvent(self.par,wxNetThreadEditElementsFinished())
            return
        self.keepGoing = self.running = True
        self.StartApply()
    def Clear(self):
        self.iCount=0
        self.iAct=0
        self.lst=[]
    def ClearLockError(self):
        self.lLockErr=[]
    def GetLockError(self):
        return self.lLockErr
    def StartApply(self):
        self.keepGoing = self.running = True
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        thread.start_new_thread(self.RunApply, ())

    def Stop(self):
        self.keepGoing = False

    def IsRunning(self):
        return self.running

    def RunApply(self):
        try:
            if self.verbose:
                vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
            iCount=len(self.lst)
            self.iAct=0
            i=0
            if self.bUpdate:
                wxPostEvent(self.par,wxNetThreadEditElements(self.iAct,iCount))
            #node=self.__apply__()
            self.zTime=0.0
            self.bAbort=False
            node=self.__getInfo__()
            self.doc.startEdit(node)
            zNotify=0.0
            while self.keepGoing:
                if self.bUpdate:
                    wxPostEvent(self.par,wxNetThreadEditElements(self.iAct,iCount))
                self.zTime+=0.1
                zNotify+=0.1
                if self.zTime>self.zTimeOut:
                    #self.keepGoing=False
                    #self.bAbort=True
                    node=self.__getInfo__()
                    self.lLockErr.append(self.doc.getKeyNum(node))
                    id=self.doc.getKey(node)
                    vtLog.vtLngCur(vtLog.INFO,'id:%s time out %3.1f %3.1f [s]'%\
                                    (id,self.zTime,self.zTimeOut),self.doc.GetOrigin())
                    if self.bUpdate:
                        vtLog.PrintMsg(_(u'alias:%s id:%s wait for lock time out %3.1f [s]')%(self.doc.appl,id,self.zTime),self.widLogging)
                    self.doc.endEdit(node)
                    self.__next__()
                    self.zTime=0.0
                    zNotify=0.0
                if zNotify>=2.0:
                    node=self.__getInfo__()
                    id=self.doc.getKey(node)
                    vtLog.vtLngCur(vtLog.DEBUG,'id:%s wait for lock time %3.1f %3.1f [1s]'%\
                                    (id,self.zTime,self.zTimeOut),self.doc.GetOrigin())
                    zNotify=0.0
                time.sleep(0.1)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        #if self.iAct==self.iCount:
        #    self.iAct-=1
        #    self.__apply__()
        # 070309:wro send abort?
        if len(self.lLockErr)>0:
            self.bAbort=True
        self.keepGoing = self.running = False
        if self.bUpdate:
            wxPostEvent(self.par,wxNetThreadEditElements(0))
            if self.bAbort:
                wxPostEvent(self.par,wxNetThreadEditElementsAborted())
            else:
                wxPostEvent(self.par,wxNetThreadEditElementsFinished())
        self.Clear()
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'finished:%d'%self.bAbort)
        
    def __getInfo__(self):
        # overload this method
        try:
            return self.lst[self.iAct]
        except:
            return None
    def __apply__(self):
        # overload this method
        node=self.__getInfo__()
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'iact:%4d icount:%4d node:%s'%(self.iAct,self.iCount,node))
        if self.func is not None:
            self.func(node,*self.args,**self.kwargs)
        self.doc.doEdit(node)
        self.doc.endEdit(node)
        pass
    def __next__(self):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'iact:%4d icount:%4d'%(self.iAct,self.iCount))
        self.iAct+=1
        self.zTime=0.0
        if self.iAct<self.iCount:
            node=self.__getInfo__()
            self.doc.startEdit(node)
            if self.bUpdate:
                wxPostEvent(self.par,wxNetThreadEditElements(self.iAct))
        else:
            self.keepGoing=False
    def OnLock(self,evt):
        evt.Skip()
        vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(evt.GetID()),self.doc.GetOrigin())
        try:
            node=self.__getInfo__()
            if node is None:
                return
            id=self.doc.getKey(node)
            if self.doc.isSameKey(node,evt.GetID()):
                resp=evt.GetResponse()
                if resp in  ['ok','already locked']:
                    try:
                        self.__apply__()
                    except:
                        vtLog.vtLngTB(self.doc.GetOrigin())
                self.__next__()
        except:
            vtLog.vtLngTB(self.doc.GetOrigin())
        #self.__apply__()
        #self.__next__()
    
    
