#Boa:Dialog:vtNetSecXmlGuiMasterStatusDialog
#----------------------------------------------------------------------------
# Name:         vtNetSecXmlGuiMasterStatusDialog.py
# Purpose:      
#               derived from vNetXmlGuiMasterDialog.py
# Author:       Walter Obweger
#
# Created:      20060415
# CVS-ID:       $Id: vtNetSecXmlGuiMasterStatusDialog.py,v 1.12 2007/08/01 15:42:28 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import vidarc.tool.net.vtNetSecXmlGuiMasterStatusPanel

import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase

def create(parent):
    return vtNetSecXmlGuiMasterStatusDialog(parent)

[wxID_VTNETSECXMLGUIMASTERSTATUSDIALOG, 
 wxID_VTNETSECXMLGUIMASTERSTATUSDIALOGCBCLOSE, 
 wxID_VTNETSECXMLGUIMASTERSTATUSDIALOGPNSTATUS, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vtNetSecXmlGuiMasterStatusDialog(wx.Dialog):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.pnStatus, 1, border=4,
              flag=wx.BOTTOM | wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbClose, 0, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.RIGHT | wx.LEFT | wx.ALIGN_CENTER)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTNETSECXMLGUIMASTERSTATUSDIALOG,
              name=u'vtNetSecXmlGuiMasterStatusDialog', parent=prnt,
              pos=wx.Point(176, 176), size=wx.Size(400, 285),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE | wx.STAY_ON_TOP,
              title=u'vtNetSecXmlGuiMaster Status Dialog')
        self.SetClientSize(wx.Size(392, 258))

        self.pnStatus = vidarc.tool.net.vtNetSecXmlGuiMasterStatusPanel.vtNetSecXmlGuiMasterStatusPanel(id=wxID_VTNETSECXMLGUIMASTERSTATUSDIALOGPNSTATUS,
              name=u'pnStatus', parent=self, pos=wx.Point(4, 4),
              size=wx.Size(384, 208), style=0)

        self.cbClose = wx.lib.buttons.GenBitmapTextButton(id=wxID_VTNETSECXMLGUIMASTERSTATUSDIALOGCBCLOSE,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Close'), name=u'cbClose',
              parent=self, pos=wx.Point(158, 226), size=wx.Size(76, 30),
              style=0)
        self.cbClose.SetMinSize(wx.Size(-1, -1))
        self.cbClose.Bind(wx.EVT_BUTTON, self.OnCbCloseButton,
              id=wxID_VTNETSECXMLGUIMASTERSTATUSDIALOGCBCLOSE)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        self._init_ctrls(parent)
        try:
            icon = wx.EmptyIcon()
            icon.CopyFromBitmap(vtArt.getBitmap(vtArt.Link))
            self.SetIcon(icon)
        except:
            vtLog.vtLngTB(self.sOrigin)
        #self.cbClose.SetBitmapLabel(vtArt.getBitmap(vtArt.Cancel))
        self.fgsData.Fit(self)
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        pass
    def OnCbCloseButton(self, event):
        self.Show(False)
        event.Skip()
    def ShutDown(self):
        self.pnStatus.ShutDown()
    def SetNetMaster(self,netMaster):
        self.pnStatus.SetNetMaster(netMaster)
    def AddStatus(self,name,iStatus):
        self.pnStatus.AddStatus(name,iStatus)
    def AddNetControl(self,ctrlClass,name,*args,**kwargs):
        return self.pnStatus.AddNetControl(ctrlClass,name,*args,**kwargs)
    def SetProcess(self,name,iStatusPrev,iStatus,iValue,iRange):
        self.pnStatus.SetProcess(name,iStatusPrev,iStatus,iValue,iRange)
    def IsModified(self):
        return self.pnStatus.IsModified()
    def Save(self,bModified=False):
        self.pnStatus.Save(bModified)
    def GetDictNet(self):
        return self.pnStatus.GetDictNet()
    def GetNetNames(self):
        return self.pnStatus.GetNetNames()
    def GetNetDoc(self,name):
        return self.pnStatus.GetNetDoc(name)
    def GetNetDocMaster(self):
        return self.pnStatus.GetNetDocMaster()
    def Show(self,flag=True):
        self.pnStatus.UpdateConnState()
        try:
            netMaster=self.pnStatus.GetNetMaster()
            netMaster.__logStatus__()
        except:
            vtLog.vtLngTB(self.GetName())
        wx.Dialog.Show(self,flag)


