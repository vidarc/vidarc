#Boa:FramePanel:vtNetSecXmlGuiMasterStatusPanel
#----------------------------------------------------------------------------
# Name:         vtNetSecXmlGuiMasterStatusPanel.py
# Purpose:      
#               derived from vNetXmlGuiMasterDialog
# Author:       Walter Obweger
#
# Created:      20060425
# CVS-ID:       $Id: vtNetSecXmlGuiMasterStatusPanel.py,v 1.18 2008/02/25 16:19:28 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import wx.lib.scrolledpanel

from vidarc.tool.net.vNetXmlWxGui import *

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
import sys,os

import vidarc.tool.net.vtNetArt as vtNetArt
import vidarc.tool.net.images as images

def create(parent):
    return vtNetSecXmlGuiMasterStatusPanel(parent)

[wxID_VTNETSECXMLGUIMASTERSTATUSPANEL, 
 wxID_VTNETSECXMLGUIMASTERSTATUSPANELCHCCONN, 
 wxID_VTNETSECXMLGUIMASTERSTATUSPANELLSTCONNSTATE, 
 wxID_VTNETSECXMLGUIMASTERSTATUSPANELSLCONN, 
] = [wx.NewId() for _init_ctrls in range(4)]

class vtNetSecXmlGuiMasterStatusPanel(wx.Panel):
    UPDATE_TIME = 500
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)
        parent.AddGrowableCol(1)

    def _init_coll_fgsInfo_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.slConn, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.chcConn, 1, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddWindow(self.lstConnState, 1, border=4,
              flag=wx.TOP | wx.EXPAND)

    def _init_coll_fgsInfo_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.fgsInfo, 1, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.EXPAND | wx.RIGHT | wx.LEFT)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=2, hgap=0, rows=1, vgap=0)

        self.fgsInfo = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_fgsInfo_Items(self.fgsInfo)
        self._init_coll_fgsInfo_Growables(self.fgsInfo)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTNETSECXMLGUIMASTERSTATUSPANEL,
              name=u'vtNetSecXmlGuiMasterStatusPanel', parent=prnt,
              pos=wx.Point(244, 152), size=wx.Size(139, 175),
              style=wx.DEFAULT_DIALOG_STYLE)
        self.SetClientSize(wx.Size(131, 148))

        self.chcConn = wx.Choice(choices=[],
              id=wxID_VTNETSECXMLGUIMASTERSTATUSPANELCHCCONN, name=u'chcConn',
              parent=self, pos=wx.Point(4, 32), size=wx.Size(123, 21), style=0)
        self.chcConn.SetMinSize(wx.Size(-1, -1))
        self.chcConn.Bind(wx.EVT_CHOICE, self.OnChcConnChoice,
              id=wxID_VTNETSECXMLGUIMASTERSTATUSPANELCHCCONN)

        self.slConn = wx.Slider(id=wxID_VTNETSECXMLGUIMASTERSTATUSPANELSLCONN,
              maxValue=0, minValue=0, name=u'slConn', parent=self,
              point=wx.Point(4, 4), size=wx.DefaultSize, style=wx.SL_HORIZONTAL,
              value=0)
        self.slConn.Bind(wx.EVT_SCROLL, self.OnSlConnScroll)

        self.lstConnState = wx.ListView(id=wxID_VTNETSECXMLGUIMASTERSTATUSPANELLSTCONNSTATE,
              name=u'lstConnState', parent=self, pos=wx.Point(4, 57),
              size=wx.Size(123, 87), style=wx.LC_REPORT)
        self.lstConnState.SetMinSize(wx.Size(-1, -1))

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name,verbose=0):
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        self._init_ctrls(parent)
        
        self.iFill=0
        self.pos=[0,0]
        self.lstNetAppl=[]
        self.dictNet={}
        self.dictLbl={}
        self.dictProcess={}
        self.netDocMaster=None
        self.netMaster=None
        
        self.pnGui=wx.lib.scrolledpanel.ScrolledPanel(self,id=wx.NewId(),
                pos=(8,8),size=(188, 200),
                style = wx.TAB_TRAVERSAL|wx.SUNKEN_BORDER)
        #self.pnGuiSizer = wx.BoxSizer(wx.VERTICAL)
        self.pnGuiSizer = wx.FlexGridSizer(cols=3, vgap=4, hgap=4)
        self.pnGui.SetSizer(self.pnGuiSizer)
        self.pnGui.SetAutoLayout(1)
        self.fgsData.AddWindow(self.pnGui, 1, border=4, 
                flag=wx.BOTTOM | wx.TOP | wx.EXPAND | wx.RIGHT | wx.LEFT)
        
        self.SetupImageList()
        
        self.lstConnState.InsertColumn(0,_(u'name'),wx.LIST_FORMAT_LEFT,140)
        self.lstConnState.InsertColumn(1,_(u'value'),wx.LIST_FORMAT_LEFT,90)
        self.lstConnState.InsertColumn(2,_(u'description'),wx.LIST_FORMAT_LEFT,90)
        
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
        self.fgsData.Fit(self)
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        #del self.dictLbl
        #del self.dictProcess
        #del self.dictNet
        pass
    def ShutDown(self):
        self.dictNet={}
        self.netDocMaster=None
        self.lstNetAppl=[]
    def SetupImageList(self):
        self.imgDict={}
        self.imgLstTyp=wx.ImageList(16,16)
        self.imgDict['dft']={-1:self.imgLstTyp.Add(vtNetArt.getBitmap(vtNetArt.Error))}
        
        self.imgConnState=wx.ImageList(16,16)
        self.imgConnState.Add(images.getNetStateClrBitmap())
        self.imgConnState.Add(images.getNetStateSetBitmap())
        self.lstConnState.SetImageList(self.imgConnState,wx.IMAGE_LIST_SMALL)
        
    def SetNetMaster(self,netMaster):
        self.netMaster=netMaster
    def GetNetMaster(self):
        return self.netMaster
    def GetDictNet(self):
        return self.dictNet
    def GetNetNames(self):
        return self.lstNetAppl
    def GetNetDoc(self,name):
        try:
            return self.dictNet[name]
        except:
            return None
    def GetNetDocMaster(self):
        return self.netDocMaster
    def SetCfg(self,appls,mainAppl):
        self.appls=appls
        self.mainAppl=mainAppl
        self.pnGui.SetupScrolling()
    def SetCfgNode(self,bConnect=True):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if self.netMaster is None:
            return
        self.__updateConnState__()
        lstNetAppl=self.netMaster.GetNetAppls()
        dictNet=self.netMaster.GetNetDict()
        idx=0
        for name in lstNetAppl:
            netDoc=dictNet[name]
            idx+=1
    def GetNetControlParent(self):
        return self.scwConn
    def __getImgIndex__(self,name,status):
        try:
            return self.imgDict[name][status]
        except:
            try:
                return self.imgDict['dft'][status]
            except:
                return self.imgDict['dft'][-1]
    def AddStatus(self,name,iStatus):
        try:
            idx=self.lstNetAppl.index(name)
            netDoc=self.dictNet[name]
            self.lstApplConn.SetStringItem(idx,0,'',self.__getImgIndex__(name,netDoc.GetStatus(iStatus)))
            self.__updateConnState__()
        except:
            traceback.print_exc()
    def AddNetControl(self,ctrlClass,name,*args,**kwargs):
        if self.dictNet.has_key(name):
            return None
        try:
            #if kwargs.has_key('pos')==False:
            kwargs['pos']=self.pos
            if kwargs.has_key('size')==False:
                kwargs['size']=wx.Size(16,16)
            #par=vtLog.GetPrintMsgWid(self)
            #appl=name
            #if par is not None:
            #    appl='.'.join([par.GetName(),appl])
            
            ctrl=ctrlClass(self.pnGui,name,*args,**kwargs)
            ctrl.SetAppl(name)
            EVT_NET_XML_SYNCH_START(ctrl,self.OnSynchStart)
            EVT_NET_XML_SYNCH_PROC(ctrl,self.OnSynchProc)
            EVT_NET_XML_SYNCH_FINISHED(ctrl,self.OnSynchFinished)
            EVT_NET_XML_SYNCH_ABORTED(ctrl,self.OnSynchAborted)
            #ctrl=ctrlClass(self.pnGui,appl,*args,**kwargs)
            #ctrl.SetAppl(name)
            self.pnGuiSizer.Add(ctrl, flag=wx.ALIGN_LEFT | wx.ALIGN_CENTER_VERTICAL)
            ctrl.SetNetMaster(self.netMaster)
            lbl=wx.StaticText(id=wx.NewId(),
                  label=name, name=u'lbl'+name, parent=self.pnGui, pos=wx.Point(20,
                  self.pos[1]), size=wx.Size(60, 13), style=0)
            self.pnGuiSizer.Add(lbl, flag=wx.ALIGN_LEFT | wx.ALIGN_CENTER_VERTICAL)
            pb=wx.Gauge(self.pnGui, -1, 50, (90, self.pos[1]), 
                        (80, 16),wx.GA_HORIZONTAL|wx.GA_SMOOTH)
            pb.SetMaxSize((-1,16))
            pb.SetRange(1000)
            self.pnGuiSizer.Add(pb, flag=wx.ALIGN_LEFT | wx.ALIGN_CENTER_VERTICAL)
            
            self.iFill+=1
            self.pos[1]+=20#size[1]+4
            self.lstNetAppl.append(name)
            self.dictNet[name]=ctrl
            self.dictLbl[name]=lbl
            self.dictProcess[name]=pb
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'name:%s;dNet:%s;lName:%s'%\
                            (name,vtLog.pformat(self.dictNet),vtLog.pformat(self.lstNetAppl)),self)
            if self.iFill==1:
                self.netDocMaster=ctrl
            d=ctrl.GetListImgDict(self.imgLstTyp)
            if d is not None:
                self.imgDict[name]=d
            
            self.chcConn.Append(name)
            self.slConn.SetRange(0,len(self.lstNetAppl)-1)
            if self.iFill==1:
                self.chcConn.SetSelection(0)
                self.SetupConnState()
            return ctrl
        except:
            vtLog.vtLngTB(self.GetName())
        return None
    def OnSynchStart(self,evt):
        evt.Skip()
        try:
            netDoc=evt.GetNetDoc()
            name=netDoc.GetAppl()
            if name in self.dictProcess:
                pb=self.dictProcess[name]
                pb.SetValue(0)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSynchProc(self,evt):
        evt.Skip()
        try:
            netDoc=evt.GetNetDoc()
            name=netDoc.GetAppl()
            if name in self.dictProcess:
                pb=self.dictProcess[name]
                iValue=float(evt.GetAct())
                iCount=float(evt.GetCount())
                #pb.SetRange(iRange)
                pb.SetValue(int(iValue/iCount*1000))
            
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSynchFinished(self,evt):
        evt.Skip()
        try:
            netDoc=evt.GetNetDoc()
            name=netDoc.GetAppl()
            if name in self.dictProcess:
                pb=self.dictProcess[name]
                pb.SetValue(0)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSynchAborted(self,evt):
        evt.Skip()
        try:
            netDoc=evt.GetNetDoc()
            name=netDoc.GetAppl()
            if name in self.dictProcess:
                pb=self.dictProcess[name]
                pb.SetValue(0)
        except:
            vtLog.vtLngTB(self.GetName())
    def SetProcess(self,name,iStatusPrev,iStatus,iValue,iRange):
        try:
            pb=self.dictProcess[name]
            netDoc=self.dictNet[name]
            if netDoc.GetStatus(iStatus)==netDoc.SYNCH_START:
                pb.SetRange(100)
            elif netDoc.GetStatus(iStatus)==netDoc.SYNCH_PROC:
                try:
                    pb.SetRange(iRange)
                    pb.SetValue(iValue)
                except:
                    vtLog.vtLngCallStack(None,vtLog.DEBUG,'',verbose=1,origin='vNetMasterDlg')
            #elif netDoc.GetStatus(iStatus) in [netDoc.STOPPED,netDoc.DISCONNECTED]:
            #    self.__updateApplButtons__()
            elif netDoc.GetStatus(iStatus)==netDoc.SYNCH_FIN:
                pb.SetValue(0)
            if name==self.chcConn.GetStringSelection():
                if netDoc.IsStatusChanged(iStatusPrev,iStatus):
                    self.__updateConnState__()
        except:
            vtLog.vtLngTB(self.GetName())
    def IsModified(self):
        for netDoc in self.dictNet.values():
            if netDoc.IsModified():
                return True
        return False
    def Save(self,bModified=False):
        for name in self.lstNetAppl:
            netDoc=self.dictNet[name]
            if bModified:
                if netDoc.IsModified():
                    netDoc.Save()
            else:
                netDoc.Save()
    def __getNetDocIdxName__(self,netDoc):
        i=0
        for name in self.lstNetAppl:
            if self.dictNet[name]==netDoc:
                return i,name
            i+=1
        return -1,''
    def UpdateConnState(self):
        self.__updateConnState__()
    def SetupConnState(self):
        try:
            iSel=self.chcConn.GetSelection()
            netDoc=self.dictNet[self.lstNetAppl[iSel]]
            
            lName=netDoc.GetFlagNameLst()
            lTrans=netDoc.GetFlagTransLst()
            self.lstConnState.DeleteAllItems()
            
            self.lstConnState.InsertImageStringItem(sys.maxint,_(u'state'),-1)
            
            for i in xrange(len(lName)):
                idx=self.lstConnState.InsertImageStringItem(sys.maxint,lName[i],-1)
                self.lstConnState.SetStringItem(idx,2,lTrans[i],-1)
        except:
            vtLog.vtLngTB(self.GetName())
    def __updateConnState__(self):
        iSel=self.chcConn.GetSelection()
        netDoc=self.dictNet[self.lstNetAppl[iSel]]
        
        self.lstConnState.SetStringItem(0,0,netDoc.GetStatusStr(),-1)
        self.lstConnState.SetStringItem(0,1,netDoc.GetStatusTrans(),-1)
        
        lVal=netDoc.GetFlagValLst()
        for i in xrange(len(lVal)):
            t=lVal[i]
            if t[1]==True:
                self.lstConnState.SetItemImage(i+1,1)
                self.lstConnState.SetStringItem(i+1,1,'1',-1)
            else:
                self.lstConnState.SetItemImage(i+1,0)
                self.lstConnState.SetStringItem(i+1,1,'0',-1)
        self.lstConnState.Refresh()
        
    def OnChcConnChoice(self, event):
        iSel=self.chcConn.GetSelection()
        self.slConn.SetValue(iSel)
        self.SetupConnState()
        self.__updateConnState__()
        event.Skip()

    def OnSlConnScroll(self, event):
        iSel=self.slConn.GetValue()
        self.chcConn.SetSelection(iSel)
        self.SetupConnState()
        self.__updateConnState__()
        event.Skip()

