#----------------------------------------------------------------------------
# Name:         vNetAddThread.py
# Purpose:
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vNetAddThread.py,v 1.4 2007/08/06 22:53:10 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from wx import NewEventType as wxNewEventType
from wx import PyEvent as wxPyEvent
from wx import PostEvent as wxPostEvent
import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.net.vNetXmlWxGui import *

import thread,time


VERBOSE=0

# defined event for vgpXmlTree item selected
wxEVT_NET_ADD_THREAD_ELEMENTS=wxNewEventType()
def EVT_NET_ADD_THREAD_ELEMENTS(win,func):
    win.Connect(-1,-1,wxEVT_NET_ADD_THREAD_ELEMENTS,func)
class wxNetThreadAddElements(wxPyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_Add_THREAD_ELEMENTS(<widget_name>, self.OnItemSel)
    """

    def __init__(self,iVal,iCount=-1):
        wxPyEvent.__init__(self)
        self.val=iVal
        self.count=iCount
        self.SetEventType(wxEVT_NET_ADD_THREAD_ELEMENTS)
    def GetValue(self):
        return self.val
    def GetCount(self):
        return self.count
# defined event for vgpXmlTree item selected
wxEVT_NET_ADD_THREAD_ELEMENTS_FINISHED=wxNewEventType()
def EVT_NET_ADD_THREAD_ELEMENTS_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_NET_ADD_THREAD_ELEMENTS_FINISHED,func)
class wxNetThreadAddElementsFinished(wxPyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_ADD_THREAD_ELEMENTS_FINISHED(<widget_name>, self.OnItemSel)
    """
    def __init__(self):
        wxPyEvent.__init__(self)
        self.SetEventType(wxEVT_NET_ADD_THREAD_ELEMENTS_FINISHED)

# defined event for vgpXmlTree item selected
wxEVT_NET_ADD_THREAD_ELEMENTS_ABORTED=wxNewEventType()
def EVT_NET_ADD_THREAD_ELEMENTS_ABORTED(win,func):
    win.Connect(-1,-1,wxEVT_NET_ADD_THREAD_ELEMENTS_ABORTED,func)
class wxNetThreadAddElementsAborted(wxPyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_ADD_THREAD_ELEMENTS_ABORTED(<widget_name>, self.OnItemSel)
    """
    def __init__(self):
        wxPyEvent.__init__(self)
        self.SetEventType(wxEVT_NET_ADD_THREAD_ELEMENTS_ABORTED)
    

class thdNetAddThread:
    def __init__(self,par,zTimeOut=100,verbose=0):
        self.zTimeOut=zTimeOut
        self.verbose=verbose
        self.par=par
        self.Clear()
        self.running=False
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
    def ScheduleAdd(self,par,node):
        if self.running:
            return -1
        self.lst.append((par,node))
        self.iCount+=1
        return 0
    def SetParent(self,par=None):
        self.par=par
    def SetDoc(self,doc):
        self.doc=doc
        if self.doc is not None:
            EVT_NET_XML_ADD_NODE_RESPONSE(self.doc,self.OnAddResponse)
    def Apply(self,update_gui=False):
        self.bUpdate=update_gui
        if self.iCount==0:
            if self.bUpdate:
                wxPostEvent(self.par,wxNetThreadAddElementsFinished())
            return
        self.StartApply()
    def Clear(self):
        self.iCount=0
        self.iAct=0
        self.lst=[]
        
    def StartApply(self):
        self.keepGoing = self.running = True
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        thread.start_new_thread(self.RunApply, ())

    def Stop(self):
        self.keepGoing = False

    def IsRunning(self):
        return self.running

    def RunApply(self):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        iCount=len(self.lst)
        iAct=0
        i=0
        if self.bUpdate:
            wxPostEvent(self.par,wxNetThreadAddElements(self.iAct,iCount))
        node=self.__apply__()
        self.zTime=0
        self.bAbort=False
        #self.doc.startEdit(node)
        while self.keepGoing:
            if self.bUpdate:
                wxPostEvent(self.par,wxNetThreadAddElements(self.iAct,iCount))
            self.zTime+=1
            if self.zTime>self.zTimeOut:
                self.keepGoing=False
                self.bAbort=True
            time.sleep(1)
        #if self.iAct==self.iCount:
        #    self.iAct-=1
        #    self.__apply__()
        self.keepGoing = self.running = False
        if self.bUpdate:
            wxPostEvent(self.par,wxNetThreadAddElements(0))
            if self.bAbort:
                wxPostEvent(self.par,wxNetThreadAddElementsAborted())
            else:
                wxPostEvent(self.par,wxNetThreadAddElementsFinished())
        self.Clear()
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'finished:%d'%self.bAbort)
        
    def __getInfo__(self):
        # overload this method
        try:
            return self.lst[self.iAct]
        except:
            return None,None
    def __apply__(self):
        # overload this method
        par,node=self.__getInfo__()
        if par is None and node is None:
            return
        self.doc.addNode(par,node)
        pass
    def __next__(self):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'iact:%4d icount:%4d'%(self.iAct,self.iCount))
        self.iAct+=1
        self.zTime=0
        if self.iAct<self.iCount:
            self.__apply__()
            if self.bUpdate:
                wxPostEvent(self.par,wxNetThreadAddElements(self.iAct))
        else:
            self.keepGoing=False
    def OnAddResponse(self,evt):
        self.__next__()
        evt.Skip()
    
    
