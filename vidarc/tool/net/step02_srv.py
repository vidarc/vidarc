#----------------------------------------------------------------------------
# Name:         step02_srv.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: step02_srv.py,v 1.1 2005/12/11 23:04:49 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import thread
import time
import socket

# Echo server program
import sys

HOST = ''                 # Symbolic name meaning the local host
PORT = 50007              # Arbitrary non-privileged port
s = None
for res in socket.getaddrinfo(HOST, PORT, socket.AF_UNSPEC, socket.SOCK_STREAM, 0, socket.AI_PASSIVE):
    af, socktype, proto, canonname, sa = res
    try:
    	s = socket.socket(af, socktype, proto)
    except socket.error, msg:
    	s = None
    	continue
    try:
    	s.bind(sa)
    	s.listen(1)
    except socket.error, msg:
    	s.close()
    	s = None
    	continue
    break
if s is None:
    print 'could not open socket'
    sys.exit(1)
conn, addr = s.accept()
print 'Connected by', addr
while 1:
    data = conn.recv(1024)
    if not data: break
    conn.send(data)
conn.close()