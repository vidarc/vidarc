#----------------------------------------------------------------------------
# Name:         vNetXmlWxGui.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vNetXmlWxGui.py,v 1.78 2010/03/03 02:17:10 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.buttons import GenBitmapButton

from vidarc.tool.vtThreadCore import vtThreadCore
#from vidarc.tool.xml.vtXmlDom import InfoDiff
#from vidarc.tool.xml.vtXmlDom import NodeInfo
from vidarc.tool.net.vtNetXmlClt import vtNetXmlCltSock
from vidarc.tool.net.vtNetClt import vtNetClt
from vidarc.tool.net.vNetPlaceDialog import vNetPlaceDialog
from vidarc.tool.net.vNetCfgDialog import vNetCfgDialog
from vidarc.tool.net.vNetLoginDialog import vNetLoginDialog
#from vidarc.tool.net.vNetXmlSynch import *
from vidarc.tool.net.vtNetXmlSynch import vtNetXmlSynch
from vidarc.tool.net.vtNetLock import vtNetLock
import vidarc.tool.net.vtNetArt as vtNetArt
from vidarc.tool.vtStateFlag import vtStateFlag
from vidarc.tool.vtThread import CallBack
from vidarc.tool.vtThread import vtThreadWX

import vidarc.tool.log.vtLog as vtLog
import string,os.path,sys,threading,time
from cStringIO import StringIO

from vidarc.tool.xml.vtXmlWxGuiEvents import *


class thdStop(vtThreadCore):
    def __init__(self,doc=None,verbose=0):
        if doc is not None:
            sOrigin=''.join([doc.GetOrigin(),':',self.__class__.__name__])
        else:
            sOrigin=None
        vtThreadCore.__init__(self,bPost=False,verbose=verbose,origin=sOrigin)
    def DoStop(self,doc,func,*args,**kwargs):
    #    if self.IsRunning():
    #        vtLog.vtLngCur(vtLog.CRITICAL,'stop thread already running'%(),self.GetOrigin())
    #        return
        self.Do(self.Run,doc,func,*args,**kwargs)
    def Run(self,doc,func,*args,**kwargs):
        try:
            vtLog.vtLngCur(vtLog.INFO,'stopping;appl:%s'%(doc.GetApplAlias()),self.GetOrigin())
            doc.__stop__()
            while doc.IsRunning():
                time.sleep(0.5)
                vtLog.vtLngCur(vtLog.INFO,'stopping;appl:%s'%(doc.GetApplAlias()),self.GetOrigin())
                #self.doc.__stop__()
            #time.sleep(0.5)
            vtLog.vtLngCur(vtLog.INFO,'stopped;appl:%s'%(doc.GetApplAlias()),self.GetOrigin())
            #self.keepGoing = self.running = False
            #time.sleep(0.5)
            if func is not None:
                func(*args,**kwargs)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        #self.keepGoing = self.running = False

VTNETXMLWXGUI_DICT=None
VTNETXMLWXGUI_DICTMED=None
VTNETXMLWXGUI_DICTSUB=None
VTNETXMLWXGUI_DICTSUBMED=None

#class vNetXmlWxGui(StaticBitmap):
#class vNetXmlWxGui(wx.BitmapButton):
#class vNetXmlWxGui(wx.lib.buttons.GenBitmapButton):
class vNetXmlWxGui(GenBitmapButton,vtnxXmlEvent):
    OFFLINE_ADD     =0x01
    OFFLINE_EDIT    =0x02   # see also __setOffline__ default value
    OFFLINE_DEL     =0x04
    OFFLINE_MOVE    =0x08
    
    CLOSED          =0x000
    DISCONNECTED    =0x010
    PRECONNECT      =0x01F
    CONNECT         =0x020
    CONNECTED       =0x060
    CONNECT_FLT     =0x030
    OPEN_START      =0x002
    OPENED          =0x004
    OPEN_FLT        =0x005
    SYNCH_START     =0x0A0
    SYNCH_PROC      =0x0B0
    SYNCH_FIN       =0x0C0
    SYNCH_ABORT     =0x0D0
    LOGIN           =0x070
    LOGGEDIN        =0x080
    STOPPED         =0x0F0
    SERVED          =0x040
    SERVE_FLT       =0x050
    CONFIG          =0x090
    LOGIN_ATTEMPT   =0x00000020
    DATA_ACCESS     =0x00000100
    BLOCK_NOTIFY    =0x00000200
    #OPENED          =0x00000400
    CFG_VALID       =0x00000800
    SYNCHABLE       =0x00001000
    CONN_ACTIVE     =0x00002000
    CONN_PAUSE      =0x00004000
    SHUTDOWN        =0x00008000
    SYNCH_IDLE      =0x00010000         # synch data when idle no access
    CONN_ATTEMPT    =0x00020000
    DATA_SETTLED    =0x00040000
    STATE_MASK      =0x000000FF
    MASK            =0x00FFFFFF
    
    SINGLE_LOCK     =0
    
    def __init__(self,parent,appl,pos=(0,0),size=(16,16),synch=False,small=True,verbose=0):
        if small:
            img=wx.EmptyBitmap(16, 16)
            size=(16,16)
            self.bSmall=True
        else:
            img=wx.EmptyBitmap(32, 32)
            size=(32,32)
            self.bSmall=False
        #StaticBitmap.__init__(self,parent=parent,pos=pos,size=size,
        #        bitmap=img,name='vNetXmlWxGui')
        #wx.BitmapButton.__init__(self,parent=parent,pos=pos,size=size,
        #        bitmap=img,name='vNetXmlWxGui',
        #        style=wx.SUNKEN_BORDER | wx.SIMPLE_BORDER | wx.NO_3D)
        GenBitmapButton.__init__(self,parent=parent,pos=pos,size=size,
                bitmap=img,name='vNetXmlWxGui',
                style=0)
        wx.EVT_LEFT_UP(self, self.OnLeftDown)
        wx.EVT_RIGHT_DOWN(self, self.OnRightDown)
        #wx.lib.buttons.GenBitmapButton.__init__(self,parent=parent,pos=pos,size=size,
        #        bitmap=img,name='vNetXmlWxGui',
        #        style=wx.SUNKEN_BORDER,id=-1)
        self.widLogging=vtLog.GetPrintMsgWid(self)
        #self.iStatus=self.DISCONNECTED
        #self.iStatusPrev=self.DISCONNECTED
        self.netMaster=None
        #vtnxXmlEvent.__init__(self)
        
        try:
            sOrigin=vtLog.vtLngGetRelevantNamesWX(self,appl)
            l=sOrigin.split('.')
            sOrigin='.'.join(l[:-3]+[l[-1]])
            self.SetOrigin(sOrigin)
            #self.thdSynch=thdNetXmlSynch(self,':'.join([sOrigin,'thdSynch']),verbose-1)
            self.thdSynch=vtNetXmlSynch(self,verbose=verbose-1,origin=':'.join([sOrigin,'thdSynch']))
            #lName=[appl]
            #par=parent
            #while par is not None:
            #    lName.append(par.GetName())
            #    par=par.GetParent()
            #if len(lName)>3:
            #    del lName[1]
            #    del lName[1]
            #    del lName[1]
            #lName.reverse()
            #if len(lName)>2:
            #    del lName[2]
            #    del lName[0]
            #self.thdSynch=thdNetXmlSynch(self,'.'.join(lName),verbose-1)
            #try:
            #    self.SetOrigin('.'.join(lName))
            #except:
            #    print 'fault'
        except:
            vtLog.vtLngTB(self.GetName())
            #self.thdSynch=thdNetXmlSynch(self,appl,verbose-1)
            self.thdSynch=vtNetXmlSynch(self,verbose=verbose-1)
        self.oSF=vtStateFlag(
                    {
                    0x000:{'name':'CLOSED','translation':_(u'closed'),
                        'imgNor':vtNetArt.Stopped,
                        'imgMed':vtNetArt.StoppedMed,
                        'imgSub':vtNetArt.StoppedAlpha,
                        'imgMedSub':vtNetArt.StoppedMedAlpha,
                        'init':True,'is':None,
                        'enter':{'set':['CLSD','STLD',],
                                 'clr':['WRKG','RDY','MOD']}},
                    0x001:{'name':'OPEN','translation':_(u'open'),
                        'imgNor':vtNetArt.Open,
                        'imgMed':vtNetArt.OpenMed,
                        'imgSub':vtNetArt.OpenAlpha,
                        'imgMedSub':vtNetArt.OpenMedAlpha,
                        'is':None,
                        'enter':{'set':['WRKG',],
                                 'clr':['STLD','FLT','CLSD','MOD','RDY']}},
                    0x002:{'name':'OPENING','translation':_(u'opening'),
                        'imgNor':vtNetArt.Open,
                        'imgMed':vtNetArt.OpenMed,
                        'imgSub':vtNetArt.OpenAlpha,
                        'imgMedSub':vtNetArt.OpenMedAlpha,
                        'is':None,},
                    0x003:{'name':'GENID','translation':_(u'generate IDs'),
                        'imgNor':vtNetArt.Open,
                        'imgMed':vtNetArt.OpenMed,
                        'imgSub':vtNetArt.OpenAlpha,
                        'imgMedSub':vtNetArt.OpenMedAlpha,
                        'is':None,
                        'enter':{'set':['WRKG']}},
                    0x004:{'name':'OPENED','translation':_(u'opened'),
                        'imgNor':vtNetArt.Opened,
                        'imgMed':vtNetArt.OpenedMed,
                        'imgSub':vtNetArt.OpenedAlpha,
                        'imgMedSub':vtNetArt.OpenedMedAlpha,
                        'is':'IsOpenOk',
                        'enter':{'set':['OPND',],
                                 'clr':['FLT','MOD']},
                        '':{'set':['WRKG']}},
                    0x005:{'name':'OPEN_FLT','translation':_(u'open fault'),
                        'imgNor':vtNetArt.OpenFault,
                        'imgMed':vtNetArt.OpenFaultMed,
                        'imgSub':vtNetArt.OpenFaultAlpha,
                        'imgMedSub':vtNetArt.OpenFaultMedAlpha,
                        'is':'IsOpenFault',
                        'enter':{'set':['FLT','OPND','MOD',],
                                 'clr':['WRKG','RDY',]}},
                    0x006:{'name':'PAUSE','translation':_(u'pause'),
                        'imgNor':vtNetArt.Stopped,
                        'imgMed':vtNetArt.StoppedMed,
                        'imgSub':vtNetArt.StoppedAlpha,
                        'imgMedSub':vtNetArt.StoppedMedAlpha,
                        'is':None,
                        'enter':{'set':['WRKG'],
                                 'clr':['RDY']}},
                    0x007:{'name':'PAUSING','translation':_(u'pausing'),
                        'imgNor':vtNetArt.Stopped,
                        'imgMed':vtNetArt.Stopped,
                        'imgSub':vtNetArt.StoppedAlpha,
                        'imgMedSub':vtNetArt.StoppedAlpha,
                        'is':None,
                        'enter':{'set':['WRKG']}},
                    0x008:{'name':'PAUSED','translation':_(u'paused'),
                        'imgNor':vtNetArt.Stopped,
                        'imgMed':vtNetArt.StoppedMed,
                        'imgSub':vtNetArt.StoppedAlpha,
                        'imgMedSub':vtNetArt.StoppedMedAlpha,
                        'is':None,
                        'enter':{#'set':['fin',],
                                 'clr':['WRKG','RDY']}},
                    0x009:{'name':'RESUME','translation':_(u'resume'),
                        'enter':{'set':['WRKG']}},
                    0x00E:{'name':'NEW','translation':_(u'new'),
                        'imgNor':vtNetArt.Stopped,
                        'imgMed':vtNetArt.StoppedMed,
                        'imgSub':vtNetArt.StoppedAlpha,
                        'imgMedSub':vtNetArt.StoppedMedAlpha,
                        'enter':{'set':['MOD']}},
                    0x00F:{'name':'CLOSING','translation':_(u'closing'),
                        'imgNor':vtNetArt.Stopping,
                        'imgMed':vtNetArt.StoppingMed,
                        'imgSub':vtNetArt.StoppingAlpha,
                        'imgMedSub':vtNetArt.StoppingMedAlpha,
                        'imgBig':vtNetArt.StoppingBig,
                        'enter':{'set':['WRKG']}},
                    
                    0x010:{'name':'DISCONNECT','translation':_(u'disconnect'),
                        'imgNor':vtNetArt.Stopping,
                        'imgMed':vtNetArt.StoppingMed,
                        'imgSub':vtNetArt.StoppingAlpha,
                        'imgMedSub':vtNetArt.StoppingMedAlpha,
                        'imgBig':vtNetArt.StoppingBig,
                        'is':'IsDisConnect',
                        'enter':{'set':['WRKG',],
                                 'clr':['STLD','RDY','ONLINE','CONN_ATTEMPT']}},
                    0x013:{'name':'DISCONNECTING','translation':_(u'disconnected'),
                        'imgNor':vtNetArt.Stopping,
                        'imgMed':vtNetArt.StoppingMed,
                        'imgSub':vtNetArt.StoppingAlpha,
                        'imgMedSub':vtNetArt.StoppingMedAlpha,
                        'imgBig':vtNetArt.StoppingBig,
                        'is':'IsDisConnecting',
                        'enter':{'set':[],
                                 'clr':['WRKG','RDY']}},
                    0x015:{'name':'DISCONNECTED','translation':_(u'disconnected'),
                        'imgNor':vtNetArt.Stopped,
                        'imgMed':vtNetArt.StoppedMed,
                        'imgSub':vtNetArt.StoppedAlpha,
                        'imgMedSub':vtNetArt.StoppedMedAlpha,
                        'imgBig':vtNetArt.StoppedBig,
                        'is':'IsDisConnected',
                        'enter':{'set':['STLD'],
                                 'clr':['WRKG','RDY','CONN_ACTIVE']}},
                    0x01F:{'name':'PRECONNECT','translation':_(u'pre connect'),
                        'imgNor':vtNetArt.Connect,
                        'imgMed':vtNetArt.ConnectMed,
                        'imgSub':vtNetArt.ConnectAlpha,
                        'imgMedSub':vtNetArt.ConnectMedAlpha,
                        'imgBig':vtNetArt.ConnectBig,
                        'is':None,
                        'enter':{'set':['WRKG','BLOCK_NOTIFY'],
                                 'clr':['STLD','RDY','CONN_ATTEMPT']}},
                    0x020:{'name':'CONNECT','translation':_(u'connect'),
                        'imgNor':vtNetArt.Connect,
                        'imgMed':vtNetArt.ConnectMed,
                        'imgSub':vtNetArt.ConnectAlpha,
                        'imgMedSub':vtNetArt.ConnectMedAlpha,
                        'imgBig':vtNetArt.ConnectBig,
                        'is':None,
                        'enter':{'set':['WRKG','BLOCK_NOTIFY','CONN_ATTEMPT'],
                                 'clr':['STLD','RDY']}},
                    0x030:{'name':'CONNECT_FLT','translation':_(u'connect fault'),
                        'imgNor':vtNetArt.Fault,
                        'imgMed':vtNetArt.FaultMed,
                        'imgSub':vtNetArt.FaultAlpha,
                        'imgMedSub':vtNetArt.FaultMedAlpha,
                        'imgBig':vtNetArt.FaultBig,
                        'is':'IsConnectFault',
                        'enter':{'set':['FLT','STLD'],
                                 'clr':['WRKG','RDY']}},
                    0x040:{'name':'SERVED','translation':_(u'served'),
                        'imgNor':vtNetArt.Connect,
                        'imgMed':vtNetArt.ConnectMed,
                        'imgSub':vtNetArt.ConnectAlpha,
                        'imgMedSub':vtNetArt.ConnectMedAlpha,
                        'imgBig':vtNetArt.ConnectBig,
                        'is':None,
                        'enter':{'set':['WRKG'],
                                 'clr':['STLD','RDY']}},
                    0x050:{'name':'SERVE_FLT','translation':_(u'serve fault'),
                        'imgNor':vtNetArt.ServeFlt,
                        'imgMed':vtNetArt.ServeFltMed,
                        'imgSub':vtNetArt.ServeFltAlpha,
                        'imgMedSub':vtNetArt.ServeFltMedAlpha,
                        'imgBig':vtNetArt.ServeFltBig,
                        'is':None,
                        'enter':{'set':['STLD','FLT'],
                                 'clr':['WRKG','RDY']}},
                    0x060:{'name':'CONNECTED','translation':_(u'connected'),
                        'imgNor':vtNetArt.Connect,
                        'imgMed':vtNetArt.ConnectMed,
                        'imgSub':vtNetArt.ConnectAlpha,
                        'imgMedSub':vtNetArt.ConnectMedAlpha,
                        'imgBig':vtNetArt.ConnectBig,
                        'is':'IsConnected',
                        'enter':{#'set':['STLD'],
                                 'set':['WRKG','LOGIN_FIRST'],
                                 'clr':['WRKG','RDY']}},
                    0x070:{'name':'LOGIN','translation':_(u'login'),
                        'imgNor':vtNetArt.Login,
                        'imgMed':vtNetArt.LoginMed,
                        'imgSub':vtNetArt.LoginAlpha,
                        'imgMedSub':vtNetArt.LoginMedAlpha,
                        'imgBig':vtNetArt.LoginBig,
                        'is':None,
                        'enter':{#'set':['WRKG','LOGIN_FIRST'],
                                 'clr':['STLD','RDY'],
                                 'CB':(self.Login,[],{})}},
                    0x075:{'name':'LOGIN_ATTEMPT','translation':_(u'login attempt'),
                        'imgNor':vtNetArt.Login,
                        'imgMed':vtNetArt.LoginMed,
                        'imgSub':vtNetArt.LoginAlpha,
                        'imgMedSub':vtNetArt.LoginMedAlpha,
                        'imgBig':vtNetArt.LoginBig,
                        'is':None,
                        'enter':{#'set':['WRKG','LOGIN_FIRST'],
                                 'clr':['STLD','RDY'],
                                 #'CB':(self.Login,[],{})
                                }},
                    0x080:{'name':'LOGGEDIN','translation':_(u'loggedin'),
                        'imgNor':vtNetArt.Loggedin,
                        'imgMed':vtNetArt.LoggedinMed,
                        'imgSub':vtNetArt.LoggedinAlpha,
                        'imgMedSub':vtNetArt.LoggedinMedAlpha,
                        'imgBig':vtNetArt.LoggedinBig,
                        'is':None,
                        'enter':{'set':['WRKG','CONNINFOSET'],
                                 'clr':['STLD','RDY','CONNINFOSET'],
                                 'CBs':[
                                        (self.__genAutoFN__,(),{'bStore':True}),
                                        (self.GetLoggedInAcl,(),{}),
                                        (self.GetConfig,(),{}),
                                        ],
                                    },
                                },
                    0x090:{'name':'CONFIG','translation':_(u'configuration'),
                        'imgNor':vtNetArt.Connect,
                        'imgMed':vtNetArt.ConnectMed,
                        'imgSub':vtNetArt.ConnectAlpha,
                        'imgMedSub':vtNetArt.ConnectMedAlpha,
                        'imgBig':vtNetArt.ConnectBig,
                        'is':None,
                        'enter':{'set':['WRKG','CFG_VALID'],
                                 'clr':['STLD','RDY']}},
                    0x0A0:{'name':'SYNCH_START','translation':_(u'synching'),
                        'imgNor':vtNetArt.Synch,
                        'imgMed':vtNetArt.SynchMed,
                        'imgSub':vtNetArt.SynchAlpha,
                        'imgMedSub':vtNetArt.SynchMedAlpha,
                        'imgBig':vtNetArt.SynchBig,
                        'is':None,
                        'enter':{'set':['WRKG'],
                                 'clr':['STLD','RDY']}},
                    0x0B0:{'name':'SYNCH_PROC','translation':_(u'synching'),
                        'imgNor':vtNetArt.Synch,
                        'imgMed':vtNetArt.SynchMed,
                        'imgSub':vtNetArt.SynchAlpha,
                        'imgMedSub':vtNetArt.SynchMedAlpha,
                        'imgBig':vtNetArt.SynchBig,
                        'is':None,
                        'enter':{'set':['WRKG'],
                                 'clr':['STLD','RDY']}},
                    0x0C0:{'name':'SYNCH_FIN','translation':_(u'synch finsihed'),
                        'imgNor':vtNetArt.Running,
                        'imgMed':vtNetArt.RunningMed,
                        'imgSub':vtNetArt.RunningAlpha,
                        'imgMedSub':vtNetArt.RunningMedAlpha,
                        'imgBig':vtNetArt.RunningBig,
                        'is':'IsSynchFinished',
                        'enter':{'set':['STLD','RDY'],
                                 'clr':['WRKG',]}},   #'BLOCK_NOTIFY'
                    0x0D0:{'name':'SYNCH_ABORT','translation':_(u'synch aborted'),
                        'imgNor':vtNetArt.SynchFlt,
                        'imgMed':vtNetArt.SynchFltMed,
                        'imgSub':vtNetArt.SynchFltAlpha,
                        'imgMedSub':vtNetArt.SynchFltMedAlpha,
                        'imgBig':vtNetArt.SynchFltBig,
                        'is':'IsSynchAborted',
                        'enter':{'set':['STLD','RDY'],
                                 'clr':['WRKG',]}},     #'BLOCK_NOTIFY'
                    0x0E0:{'name':'STOPPING','translation':_(u'stopping'),
                        'imgNor':vtNetArt.Stopping,
                        'imgMed':vtNetArt.StoppingMed,
                        'imgSub':vtNetArt.StoppingAlpha,
                        'imgMedSub':vtNetArt.StoppingMedAlpha,
                        'imgBig':vtNetArt.StoppingBig,
                        'is':None,
                        'enter':{'set':['WRKG'],
                                 'clr':['STLD','WRKG','RDY']}},
                    0x0F0:{'name':'STOPPED','translation':_(u'stopped'),
                        'imgNor':vtNetArt.Stopped,
                        'imgMed':vtNetArt.StoppedMed,
                        'imgSub':vtNetArt.StoppedAlpha,
                        'imgMedSub':vtNetArt.StoppedMedAlpha,
                        'imgBig':vtNetArt.StoppedBig,
                        'is':None,
                        'enter':{'set':['STLD'],
                                 'clr':['WRKG','RDY','CONN_ACTIVE','BLOCK_NOTIFY']}},
                    #0x007:{'name':'paused','translation':_(u'paused'),
                    #    'is':None,
                    #    'leave':{'set':['fin','RDY'],
                    #             'clr':['WRKG']},
                    #    'enter':{'set':['WRKG']}},
                    },
                    {
                    0x00000001:{'name':'CLSD', 'translation':_(u'closed'),'is':'IsClosed','order':0},
                    0x00000002:{'name':'STLD', 'translation':_(u'settled'),'is':'IsSettled','order':1},
                    0x00000004:{'name':'WRKG',  'translation':_(u'working'),'is':'IsWorking','order':2},
                    0x00000008:{'name':'RDY',  'translation':_(u'ready'),'is':'IsReady','order':3},
                    0x00000010:{'name':'MOD',  'translation':_(u'MODified'),'is':'IsModified','order':4},
                    0x00000020:{'name':'FLT',  'translation':_(u'faulty'),'is':'IsFaulty','order':5},
                    0x00000040:{'name':'OPND', 'translation':_(u'opened'),'is':'IsOpened','order':6},
                    0x00000080:{'name':'PSD', 'translation':_(u'opened'),'is':'IsPaused','order':7},
                    0x00000100:{'name':'SYNCHABLE', 'translation':_(u'synchable'),'is':'IsSynchable','keep':True},
                    0x00000200:{'name':'SYNCH_IDLE', 'translation':_(u'synch on idle'),'is':'IsSynchIdle'},
                    0x00000400:{'name':'BLOCK_NOTIFY', 'translation':_(u'block events'),'is':'IsBlockNotify','order':10},
                    0x00000800:{'name':'DATA_ACCESS', 'translation':_(u'data access'),'is':'IsDataAccess'},
                    0x00001000:{'name':'CFG_VALID', 'translation':_(u'configuration valid'),'is':'IsCfgValid','order':12},
                    0x00002000:{'name':'SYNCHED', 'translation':_(u'synched'),'is':'IsSynched','order':13},
                    0x00004000:{'name':'CONN_ACTIVE', 'translation':_(u'connection active'),'is':'IsConnActive','order':14},
                    0x00008000:{'name':'CONN_PAUSE', 'translation':_(u'connection paused'),'is':'IsConnPaused','order':15},
                    0x00010000:{'name':'CONN_ATTEMPT', 'translation':_(u'connection attemped'),'is':'IsConnAttempt','order':16},
                    #0x00020000:{'name':'LOGIN_ATTEMPT', 'translation':_(u'login attemped'),'is':'IsLoginAttemped','order':17},
                    0x00080000:{'name':'SHUTDOWN', 'translation':_(u'shutdown'),'is':'IsShutDown','keep':True,'order':18},
                    0x00100000:{'name':'LOGIN_FIRST', 'translation':_(u'login first attemped'),'is':'IsLoginFirst','order':19},
                    0x00200000:{'name':'ONLINE', 'translation':_(u'online'),'is':'IsOnLine','order':20},
                    0x00400000:{'name':'ACTIVE', 'translation':_(u'active'),'is':'IsActive','order':21},
                    
                    0x01000000:{'name':'FIXED', 'translation':_(u'application fixed'),'is':'IsFixed','keep':True},
                    0x02000000:{'name':'CONNINFOSET', 'translation':_(u'connection info set'),'is':'IsConnInfoSet','keep':True},
                    },
                    verbose=0,
                    ident=self,
                    funcNotifyCB=self.NotifyStateChange,
                    origin=u':'.join([self.origin,u'StateFlag']))
        self.oSF.AddCB(self.oSF.LOGGEDIN,'enter',self.__genAutoFN__,bStore=True)
        self.oSF.AddCB(self.oSF.LOGGEDIN,'enter',self.getDataFromRemote)
        self.oSF.SetFlag(self.oSF.SYNCHABLE,synch)
        #self.SetFlag(self.SYNCHABLE,synch)
        #self.thdStop=thdStop(None,verbose-1)
        self.thdProc=vtThreadWX(None,bPost=False,
                origin=u':'.join([self.GetOrigin(),u'thdProc']))
        
        self.thdProcNet=vtThreadWX(None,bPost=False,
                origin=u':'.join([self.GetOrigin(),u'thdProcNet']))
        
        self.args=None
        self.kwargs=None
        self.lastStatus=-1
        
        self.dlgSetup=None
        self.dlgCfgSetup=None
        self.dlgSynchEditResolve=None
        self.nodeSetup=None
        self.docSetup=None
        self.lstSetupNode=[]
        self.lstCfg=None
        self.dictDoc={}
        self.appls=[]
        self.mainAppl=''
        self.dn=''
        #self.bConnectionTried=False
        
        self.appl=appl
        if len(appl)>0:
            #self.bFixed=True   # 071022 change state flag processing
            self.oSF.SetFlag(self.oSF.FIXED,True)
        else:
            #self.bFixed=False   # 071022 change state flag processing
            self.oSF.SetFlag(self.oSF.FIXED,False)
        self.sFN=None
        self.sRecentAlias=''
        self.sRecentConn=''
        self.alias=''
        self.host=''
        self.port=-1
        self.usr=''
        self.passwd=''
        self.sUsr=''
        self.iUsrId=-1
        self.iSecLv=-1
        self.bAdmin=False
        self.__calcApplAlias__()
        #self.bConnInfoSet=False
        self.verbose=verbose
        #self.bDataSettled=False
        #self.bLoginFirst=False
        #self.bOnline=False
        #self.doc=vtXmlDom()
        #self.bDoAutoConnectAfterDisConn=False
        self.cltSrv=None
        self.cltSock=None
        
        if self.SINGLE_LOCK!=0:
            self.idLock=''
        else:   
            self.lock=vtNetLock(self.GetOrigin(),self)
        
        self.__setupImageList__()
        #self.__setBitmapByStatus__()
        #img=img_gui.getStoppedBitmap()
        #self.SetBitmap(img)
        
        self.__initCmdRemoteDict__()
        
        #wx.EVT_RIGHT_DOWN(self, self.OnRightDown)
    def __del__(self):
        #vtLog.CallStack('')
        try:
            vtLog.vtLngCur(vtLog.DEBUG,'',origin=self.origin)
        except:
            pass
        if self.SINGLE_LOCK==0:
            del self.lock
        try:
            #del self.thdAdd
            #del self.thdEdit
            del self.thdSynch
        except:
            #vtLog.vtLngTB(self.__class__.__name__)
            pass
    def __setupImageList__(self):
        global VTNETXMLWXGUI_DICT
        global VTNETXMLWXGUI_DICTMED
        global VTNETXMLWXGUI_DICTSUB
        global VTNETXMLWXGUI_DICTSUBMED
        if VTNETXMLWXGUI_DICT is None:
            self.imgDict={}
            for state,sImg in self.oSF.GetStateInfoLst('imgNor'):
                if sImg is None:
                    continue
                self.imgDict[state]=vtNetArt.getBitmap(sImg)
            self.imgDict[None]      =vtNetArt.getBitmap(vtNetArt.Fault)
            VTNETXMLWXGUI_DICT=self.imgDict
            
            sz=(16,12)
            self.imgDictMed={}
            for state,sImg in self.oSF.GetStateInfoLst('imgMed'):
                if sImg is None:
                    continue
                self.imgDictMed[state]=vtNetArt.getBitmap(sImg,sz=sz)
            self.imgDictMed[None]      =vtNetArt.getBitmap(vtNetArt.FaultMed,sz=sz)
            VTNETXMLWXGUI_DICTMED=self.imgDictMed
            
            self.imgDictSub={}
            for state,sImg in self.oSF.GetStateInfoLst('imgSub'):
                if sImg is None:
                    continue
                self.imgDictSub[state]=vtNetArt.getBitmap(sImg,sz=sz)
            self.imgDictSub[None]      =vtNetArt.getBitmap(vtNetArt.FaultAlpha)
            VTNETXMLWXGUI_DICTSUB=self.imgDictSub
            
            self.imgDictSubMed={}
            for state,sImg in self.oSF.GetStateInfoLst('imgMedSub'):
                if sImg is None:
                    continue
                self.imgDictSubMed[state]=vtNetArt.getBitmap(sImg,sz=sz)
            self.imgDictSubMed[None]      =vtNetArt.getBitmap(vtNetArt.FaultMedAlpha,sz=sz)
            VTNETXMLWXGUI_DICTSUBMED=self.imgDictSubMed
        else:
            self.imgDict=VTNETXMLWXGUI_DICT
            self.imgDictMed=VTNETXMLWXGUI_DICTMED
            self.imgDictSub=VTNETXMLWXGUI_DICTSUB
            self.imgDictSubMed=VTNETXMLWXGUI_DICTSUBMED
    def SetupAsMainAlias(self):
        vtLog.vtLngCur(vtLog.DEBUG,''%(),self.GetOrigin())
        self.oSF.ClrCBs(self.oSF.LOGGEDIN,'enter')
        self.oSF.ClrCBs(self.oSF.CONFIG,'enter')
        self.oSF.AddCB(self.oSF.LOGGEDIN,'enter',self.__genAutoFN__,bStore=True)
        self.oSF.AddCB(self.oSF.LOGGEDIN,'enter',self.GetLoggedInAcl)
        self.oSF.AddCB(self.oSF.LOGGEDIN,'enter',self.GetConfig)
        self.oSF.AddCB(self.oSF.CONFIG,'enter',self.getDataFromRemote)
        
    def ClrCBs(self,state,kind):
        vtLog.vtLngCur(vtLog.DEBUG,''%(),self.GetOrigin())
        self.oSF.ClrCBs(state,kind)
    def AddCB(self,state,kind,func,*args,**kwargs):
        vtLog.vtLngCur(vtLog.DEBUG,''%(),self.GetOrigin())
        self.oSF.AddCB(state,kind,func,*args,**kwargs)
    def DelCB(self,state,kind,func,*args,**kwargs):
        vtLog.vtLngCur(vtLog.DEBUG,''%(),self.GetOrigin())
        self.oSF.DelCB(state,kind,func,*args,**kwargs)
    def GetWidId(self):
        return GenBitmapButton.GetId(self)
        #return wx.BitmapButton.GetId(self)
        ##return StaticBitmap.GetId(self)
    def SetNetMaster(self,netMaster):
        self.netMaster=netMaster
    def SetBitmap(self,img):
        self.__setBitMap__(img)
    def __setBitMap__(self,img):
        if self.oSF.IsShutDown:#IsFlag(self.SHUTDOWN):
            return
        vtLog.vtLngCur(vtLog.DEBUG,''%(),self.GetOrigin())
        #StaticBitmap.SetBitmap(self,img)
        #wx.BitmapButton.SetBitmapLabel(self,img)
        self.Refresh()
    def DrawBezel(self, dc, x1, y1, x2, y2):
        if self.hasFocus and self.useFocusInd:
            return
        # draw the upper left sides
        if self.up:
            #dc.SetPen(self.highlightPen)
            dc.SetPen(self.shadowPen)
        else:
            dc.SetPen(self.shadowPen)
        for i in range(self.bezelWidth):
            dc.DrawLine(x1+i, y1, x1+i, y2-i)
            dc.DrawLine(x1, y1+i, x2-i, y1+i)
        # draw the lower right sides
        if self.up:
            dc.SetPen(self.shadowPen)
        else:
            #dc.SetPen(self.highlightPen)
            dc.SetPen(self.shadowPen)
        for i in range(self.bezelWidth):
            dc.DrawLine(x1+i, y2-i, x2+1, y2-i)
            dc.DrawLine(x2-i, y1+i, x2-i, y2)
    def DrawFocusIndicator(self, dc, w, h):
        bw = self.bezelWidth
        self.focusIndPen.SetColour(self.focusClr)
        dc.SetLogicalFunction(wx.INVERT)
        dc.SetPen(self.focusIndPen)
        dc.SetBrush(wx.TRANSPARENT_BRUSH)
        dc.DrawRectangle(bw,bw,  w-bw*2, h-bw*2)
        dc.SetLogicalFunction(wx.COPY)
    def DrawLabel(self, dc, width, height, dw=0, dy=0):
        try:
            x,y=0,0
            def getXY(bmp):
                bw,bh = bmp.GetWidth(), bmp.GetHeight()
                x,y=(width-bw)/2+dw, (height-bh)/2+dy
                return x,y
            bmp=self.__getMedBitmapByStatus__()
            hasMask = bmp.GetMask() != None
            x,y=getXY(bmp)
            dc.DrawBitmap(bmp,x,y,hasMask)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnPaint(self, event):
        (width, height) = self.GetClientSizeTuple()
        x1 = y1 = 0
        x2 = width-1
        y2 = height-1
        dc = wx.PaintDC(self)
        brush = self.GetBackgroundBrush(dc)
        if brush is not None:
            dc.SetBackground(brush)
            dc.Clear()
        self.DrawLabel(dc, width, height)
        self.DrawBezel(dc, x1, y1, x2, y2)
        if self.hasFocus and self.useFocusInd:
            self.DrawFocusIndicator(dc, width, height)
    def IsDisConn(self):
        return not self.oSF.IsConnActive
    def IsSettled(self):
        return self.oSF.IsSettled
        #return self.IsFlag(self.DATA_SETTLED)
    def __IsSettled__(self):
        vtLog.vtLngCur(vtLog.CRITICAL,''%(),self.GetOrigin())
        return self.oSF.IsSettled
        #if self.__isRunning__():        # 060928 wro
        #    return False
        if self.GetStatus() in [self.SYNCH_FIN,self.SYNCH_ABORT]:
            return True
        elif self.GetStatus() in [self.DISCONNECTED,self.STOPPED]:
            if self.IsFlag(self.CONN_ATTEMPT)==True:
                return False
            return False    # 070606:wro problem when offline
            return True
        elif self.GetStatus() in [self.CONNECT_FLT,self.SERVE_FLT]:
            return True
        elif (self.IsFlag(self.CONN_ACTIVE)==False) and \
                    (self.GetStatus() in [self.OPEN_OK,self.OPEN_FLT]):
            return True
        return False
    def IsActive(self):
        return self.oSF.IsActive
        if self.IsFlag(self.CONN_ACTIVE)==False:
            return False
        if self.GetStatus() in [self.DISCONNECTED,self.STOPPED,self.SERVE_FLT]:
            return False
        else:
            return True
    def __calcFlags__(self):
        if self.IsStatusChanged()==False:
            return
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,''%(),self.GetOrigin())
        self.__logStatus__()
        iStatus=self.GetStatus()
        if iStatus == self.CONNECT:
            self.SetFlag(self.CONN_ACTIVE,True)
            self.SetFlag(self.CFG_VALID,False)
        elif iStatus == self.CONNECTED:
            self.SetFlag(self.CONN_ATTEMPT,False)
        elif iStatus == self.CONNECT_FLT:
            self.SetFlag(self.CONN_ATTEMPT,False)
            self.SetFlag(self.CONN_ACTIVE,False)
            self.SetFlag(self.DATA_SETTLED,True)
        elif iStatus == self.CONFIG:
            self.SetFlag(self.CFG_VALID,True)
        elif iStatus == self.OPEN_START:
            self.SetFlag(self.OPENED,False)
        elif iStatus == self.OPEN_OK:
            self.SetFlag(self.OPENED,True)
            if (self.IsFlag(self.CONN_ACTIVE) or self.IsFlag(self.CONN_ATTEMPT))==False:
                self.SetFlag(self.DATA_SETTLED,True)
        elif iStatus == self.OPEN_FLT:
            self.SetFlag(self.OPENED,True)
            if (self.IsFlag(self.CONN_ACTIVE) or self.IsFlag(self.CONN_ATTEMPT))==False:
                self.SetFlag(self.DATA_SETTLED,True)
        elif iStatus == self.SYNCH_START:
            pass
        elif iStatus == self.SYNCH_PROC:
            pass
        elif iStatus == self.SYNCH_FIN:
            self.SetFlag(self.CONN_ATTEMPT,False)
            self.SetFlag(self.DATA_SETTLED,True)
        elif iStatus == self.SYNCH_ABORT:
            self.SetFlag(self.CONN_ATTEMPT,False)
            self.SetFlag(self.DATA_SETTLED,True)
        elif iStatus == self.LOGIN:
            pass
        elif iStatus == self.LOGIN_ATTEMPT:
            pass
        elif iStatus == self.LOGGEDIN:
            pass
        elif iStatus == self.SERVED:
            pass
        elif iStatus == self.SERVE_FLT:
            self.SetFlag(self.CONN_ACTIVE,False)
            self.SetFlag(self.DATA_SETTLED,True)
        elif iStatus == self.STOPPED:
            self.SetFlag(self.CONN_ACTIVE,False)
            self.SetFlag(self.DATA_SETTLED,True)
        elif iStatus == self.DISCONNECTED:
            self.SetFlag(self.CONN_ACTIVE,False)
            self.SetFlag(self.DATA_SETTLED,True)
        else:
            pass
        self.__logStatus__()
        
    def __setBitmapByStatus__(self,iStatusPrev=None,iStatus=None):
        if self.IsStatusChanged(iStatusPrev,iStatus)==False:
            return
        #self.__calcFlags__()
        if self.GetStatus(iStatus) in self.imgDict:
            self.__setBitMap__(self.imgDict[self.GetStatus(iStatus)])
        else:
            self.__setBitMap__(self.imgDict[None])
    def __getBitmapByStatus__(self,iStatus=None):
        #self.__calcFlags__()
        if self.GetStatus(iStatus) in self.imgDict:
            return self.imgDict[self.GetStatus(iStatus)]
        else:
            return self.imgDict[None]
    def __getSubBitmapByStatus__(self,iStatus=None):
        #self.__calcFlags__()
        if self.GetStatus(iStatus) in self.imgDict:
            return self.imgDictSub[self.GetStatus(iStatus)]
        else:
            return self.imgDictSub[None]
    def __getMedSubBitmapByStatus__(self,iStatus=None):
        #self.__calcFlags__()
        if self.GetStatus(iStatus) in self.imgDict:
            return self.imgDictSubMed[self.GetStatus(iStatus)]
        else:
            return self.imgDictSubMed[None]
    def __getMedBitmapByStatus__(self,iStatus=None):
        iStatus=self.GetStatus(iStatus)
        if iStatus in self.imgDictMed:
            return self.imgDictMed[iStatus]
        else:
            return self.imgDict[None]
    def GetListImgDict(self,imgLstTyp):
        d={}
        for state,sImg in self.oSF.GetStateInfoLst('imgNor'):
            try:
                if sImg is None:
                    continue
                d[state]=imgLstTyp.Add(vtNetArt.getBitmap(sImg))
            except:
                vtLog.vtLngTB(self.GetOrigin())
                vtLog.vtLngCur(vtLog.ERROR,'state:%s'%(state),self.GetOrigin())
        return d
    def GetStatusStr(self,iStatus=None):
        if iStatus is None:
            return self.oSF.GetStateNameAct()
        else:
            return self.oSF.GetStateName(iStatus)
    def GetStatusTrans(self,iStatus=None):
        if iStatus is None:
            return self.oSF.GetStateTransAct()
        else:
            return self.oSF.GetStateTrans(iStatus)
    def __getLogStatusStr__(self):
        return self.oSF.GetStr(u';')
        return '0x%08x;0x%08x;blocked:%d;activ:%d;settled:%d;%s'%(
                    self.oSF.GetStateAct(),self.oSF.GetStatePrev(),
                    self.oSF.IsBlockNotify,
                    self.oSF.IsActive,self.oSF.IsSettled,
                    self.oSF.GetStateNameAct())
    def __logStatus__(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,self.oSF.GetStrFull(u';'),
                        origin=self.GetOrigin())
    def NotifyStateChange(self,*args,**kwargs):
        """ called in case of state change from wx MainLoop
        """
        self.Refresh()
        if self.netMaster is not None:
            self.netMaster.NotifyStateChangeAlias(*args,**kwargs)
    def IsStatusChanged(self,iStatusPrev=None,iStatus=None):
        vtLog.vtLngCur(vtLog.CRITICAL,''%(),self.GetOrigin())
        return False
    def SetDataAccess(self,flag):
        vtLog.vtLngCur(vtLog.CRITICAL,''%(),self.GetOrigin())
        return
    def IsDataAccessed(self):
        vtLog.vtLngCur(vtLog.CRITICAL,''%(),self.GetOrigin())
        return False
    def SetFlag(self,int_flag,flag):
        vtLog.vtLngCur(vtLog.CRITICAL,''%(),self.GetOrigin())
    def IsFlag(self,int_flag,iStatus=None):
        if int_flag==self.CONN_ACTIVE:          # 071025:wro little hack
            vtLog.vtLngCur(vtLog.WARN,'FIXME'%(),self.GetOrigin())
            return self.oSF.IsConnActive
        vtLog.vtLngCur(vtLog.CRITICAL,'int_flag:%08x,iStatus:%s'%(int_flag,iStatus),self.GetOrigin())
        return False
    def GetFlagNameLst(self):
        return self.oSF.GetFlagNameLst()
    def GetFlagTransLst(self):
        return self.oSF.GetFlagTransLst()
    def GetFlagValLst(self):
        return self.oSF.GetFlagLst()
    def SetBlockNotify(self,flag):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'flag:%d'%(flag),self.GetOrigin())
        bOld=self.oSF.IsBlockNotify
        self.oSF.SetFlag(self.oSF.BLOCK_NOTIFY,flag)
        if bOld==True and self.oSF.IsBlockNotify==False:
                wx.PostEvent(self,vtnxXmlGotContent(self))
                wx.PostEvent(self,vtnxXmlBuildTree(self))
                wx.PostEvent(self,vtnxXmlContentChanged(self))
    def Select(self,node):
        #if self.IsBlockNotify():
        if self.oSF.IsBlockNotify:
            return
        if node is not None:
            wx.PostEvent(self,vtnxXmlSelect(self,self.getKey(node)))
    def SelectById(self,id):
        #if self.IsBlockNotify():
        if self.oSF.IsBlockNotify:
            return
        wx.PostEvent(self,vtnxXmlSelect(self,id))
    def IsBlockNotify(self):
        vtLog.vtLngCur(vtLog.WARN,'',self.GetOrigin())
        return self.oSF.IsBlockNotify
    def IsUsrInteractNeeded(self,bOffLine):
        """ this is usally called by net-master object to verify, if
        user interaction is required.
        return True -> show connections status dialog
        return False -> very thing is fine
        """
        if bOffLine:
            if self.oSF.IsOpenOk:
                return False
            elif self.oSF.IsDisConnected:
                return False
            else:
                #vtLog.vtLngCur(vtLog.CRITICAL,''%(),self.GetOrigin())
                return True
        else:
            if self.oSF.IsSynchFinished:
                return False
            else:
                #vtLog.vtLngCur(vtLog.CRITICAL,''%(),self.GetOrigin())
                return True
    def GetStatus(self,iStatus=None):
        if iStatus is None:
            return self.oSF.GetState()
        else:
            return iStatus
    def DoDelayedNet(self,func,*args,**kwargs):
        self.thdProcNet.Do(func,*args,**kwargs)
    def DoSafeNet(self,func,*args,**kwargs):
        self.thdProcNet.Do(self._doSafe,func,*args,**kwargs)
    def CallBackNet(self,func,*args,**kwargs):
        self.thdProcNet.CallBack(self._doSafe,func,*args,**kwargs)
    def CallBackDelayedNet(self,zSleep,func,*args,**kwargs):
        self.thdProcNet.CallBackDelayed(zSleep,func,*args,**kwargs)
    def DoCallBackNet(self,func,*args,**kwargs):
        self.thdProcNet.DoCallBack(func,*args,**kwargs)
    def New(self,root='root'):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        #if self.GetStatus() not in [self.CONNECT]:
        #    self.__connectionClosed__()
        pass
    def SetAppl(self,appl,fixed=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        self.appl=appl
        self.__calcApplAlias__()
        #self.bFixed=fixed
        self.oSF.SetFlag(self.oSF.FIXED,fixed)
    def __doStop__(self):
        try:
            self.__stop__()
            while self.IsRunning():
                time.sleep(0.5)
                #vtLog.vtLngCur(vtLog.INFO,'stopping;appl:%s'%(doc.GetApplAlias()),self.GetOrigin())
                #self.doc.__stop__()
            #time.sleep(0.5)
            vtLog.vtLngCur(vtLog.INFO,'stopped;appl:%s'%(self.GetApplAlias()),self.GetOrigin())
            if self.oSF.IsShutDown==False:
                self.StartConsumer()
                #while self.IsRunning()==False:
                #    time.sleep(0.5)
                vtLog.vtLngCur(vtLog.INFO,'restarted;appl:%s'%(self.GetApplAlias()),self.GetOrigin())
            else:
                self.thdSynch.SetPostWid(None)          # 080323:wro clear doc when shutdown completed
                
        except:
            vtLog.vtLngTB(self.__class__.__name__)
            vtLog.vtLngTB(self.GetOrigin())
    def Stop(self,func=None,*args,**kwargs):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        #vtLog.vtLngCur(vtLog.ERROR,'',origin=self.GetOrigin())
        #self.thdStop.DoStop(self,func,*args,**kwargs)
        self.thdProcNet.Do(self.__doStop__)
        if func is not None:
            self.thdProcNet.DoCallBack(func,*args,**kwargs)
    def IsStopping(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'stop:%d'%\
                        (self.thdProcNet.IsRunning()),
                        origin=self.GetOrigin())
        return self.thdProcNet.IsRunning()
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'stop:%d'%\
                        (self.thdStop.IsRunning()),
                        origin=self.GetOrigin())
        return self.thdStop.IsRunning()
    #def SetShutDown(self):
    #    self.oSF.SetFlag(self.oSF.SHUTDOWN)
    def __ShutDown__(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        self.oSF.SetFlag(self.oSF.SHUTDOWN)
        #self.thdSynch.SetPostWid(None)          # 080323:wro clear doc when shutdown completed
        self.docSetup=None
        self.netMaster=None
        try:
            if self.SINGLE_LOCK==0:
                self.lock.ShutDown()
        except:
            pass
    def __start__(self):
        self.thdSynch.Start()
        #if self.cltSrv is not None:
        #    self.cltSrv.Stop()
    def __stop__(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        self.thdSynch.Stop()
        if self.cltSrv is not None:
            self.cltSrv.Stop()
    def __isRunning__(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'synch:%d'%\
                        (self.thdSynch.IsRunning()),
                        origin=self.GetOrigin())
        if self.thdSynch.IsRunning():
            vtLog.vtLngCur(vtLog.INFO,'thdSynch running',self.GetOrigin())
            return True
        #if self.thdStop.IsRunning():
        #    vtLog.vtLngCur(vtLog.INFO,'thdStop running',self.GetOrigin())
        #    return True
        if self.cltSrv is not None:
            if self.cltSrv.IsServing():
                vtLog.vtLngCur(vtLog.INFO,'cltSrv serving',self.GetOrigin())
                return True
        if self.IsConnActive():
            if self.oSF.IsDisConnecting==False:
                vtLog.vtLngCur(vtLog.INFO,'connection is still active',self.GetOrigin())
                return True
        #if self.GetStatus() != self.STOPPED:
        #    return True
        vtLog.vtLngCur(vtLog.INFO,'all inactive',self.GetOrigin())
        return False
    def Pause(self,flag):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        if self.cltSrv is not None:
            self.cltSrv.Pause(flag)
        else:
            self.oSF.SetFlag(self.oSF.CONN_PAUSE,flag)
    def NotifyPause(self,flag):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        self.oSF.SetFlag(self.oSF.CONN_PAUSE,flag)
    def WaitNotRunning(self,timeout=-1,interval=0.1):
        vtLog.vtLngCur(vtLog.CRITICAL,''%(),self.GetOrigin())
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        iTime=0
        while self.IsRunning():
            time.sleep(interval)
            iTime+=interval
            if timeout>0:
                if iTime>timeout:
                    return False   # timeout
        return True
    def SetInfo(self,appl,alias,host,port,usr,passwd):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        #if self.bFixed==False:
        if self.oSF.IsFixed==False:
            self.appl=appl
        self.alias=alias
        self.__calcApplAlias__()
        self.host=host
        try:
            self.port=int(port)
        except:
            self.port=-1
        self.usr=usr
        self.passwd=passwd
    def SetNode(self,node):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        self.SetInfo(
                self.getNodeText(node,'appl'),
                self.getNodeText(node,'alias'),
                self.getNodeText(node,'host'),
                self.getNodeText(node,'port'),
                self.getNodeText(node,'usr'),
                self.getNodeText(node,'passwd'))
    def GetUsr(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        return self.usr
    def GetPasswd(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        return self.passwd
    def Connect2SrvByNode(self,node,lst):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        if node is None:
            if self.nodeSetup is not None:
                node=self.nodeSetup
            else:
                node=self.root
        if node is None:
            return -1
        if lst is None:
            lst=self.lstSetupNode
        node=self.getChildByLst(node,lst)
        if node is None:
            return
        self.SetNode(node)
        self.Connect2Srv()
        return 0
    def SetCfg(self,doc,lst,appls,appl,mainAppl,dlgCfgSetup=None):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        self.docSetup=doc
        self.lstCfg=lst
        self.dictDoc={}
        self.sRecentAlias=''
        self.sRecentConn=''
        self.appls=appls
        self.mainAppl=mainAppl
        self.SetAppl(appl,True)
        if dlgCfgSetup is None:
            if self.appls[0]==appl:
                if self.dlgCfgSetup is None:
                    self.dlgCfgSetup=vNetCfgDialog(self)
                    #cfgNode=self.getChild(None,'config')
                    self.SetCfgNode(None)
            else:
                self.dlgCfgSetup=None
        else:
            self.dlgCfgSetup=dlgCfgSetup
    def SetDictDoc(self,d):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'dict:%s'%(d),origin=self.GetOrigin())
        if self.docSetup is None:
            return
        self.dictDoc=d
        for k in self.dictDoc.keys():
            if k == self.appls[0]:
                continue
            doc=self.dictDoc[k]
            doc.SetCfg(self.docSetup,self.lstCfg,self.appls,k,
                    self.mainAppl,self.dlgCfgSetup)
    def __getCfgNode__(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        self.nodeSetup=self.docSetup.getRoot()
        return self.docSetup.getChildByLst(self.nodeSetup,self.lstCfg)
    
    def SetCfgNode(self,bConnect=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        if self.docSetup:
            self.dlgCfgSetup.SetNode(self.docSetup,self.lstCfg,
                            self.appls,self.appl,self.__getCfgNode__())
            #if bConnect:
            #    for k in self.dictDoc.keys():
            #        self.dictDoc[k].AutoConnect2Srv()
        return 1
    def SetSetupNode(self,node,lst,doc=None):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        self.nodeSetup=node
        self.lstSetupNode=lst
        if doc is None:
            self.docSetup=self
        else:
            self.docSetup=doc
    def __getRecentAlias__(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'__getRecentAlias__',origin=self.GetOrigin())
        if self.docSetup is None:
            return None
        else:
            self.nodeSetup=self.docSetup.getRoot()
            node=self.docSetup.getChildByLst(self.nodeSetup,self.lstCfg)
            self.dn=self.docSetup.getNodeText(node,'path')
            #node=self.docSetup.getChild(node,self.appl)
            if (node is None) or (len(self.dn)<=0):
                self.sFN=None
                return None
                #self.Setup()
                #return
            recentNode=None
            iRecent=1000
            for c in self.docSetup.getChilds(node,'alias'):
                s=self.docSetup.getNodeText(c,'recent')
                if len(s)>0:
                    try:
                        i=int(s)
                        if i<iRecent:
                            recentNode=c
                            iRecent=i
                    except:
                        pass
            return recentNode
    def __getRecentConnection__(self,recentNode):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'__getRecentConnection__',origin=self.GetOrigin())
            
        if recentNode is None:
            return None
        
        recentConnNode=None
        iRecent=1000
        for c in self.docSetup.getChilds(recentNode,'connection'):
            s=self.docSetup.getNodeText(c,'recent')
            if len(s)>0:
                try:
                    i=int(s)
                    if i<iRecent:
                        recentConnNode=c
                        iRecent=i
                except:
                    pass
        return recentConnNode
    def __getApplHost__(self,recentNode,appl=None):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'',origin=self.GetOrigin())
        if recentNode is None:
            return None
        if appl is None:
            appl=self.appl
        applNode=self.docSetup.getChild(recentNode,appl)
        recentNode=None
        iRecent=1000
        for c in self.docSetup.getChilds(applNode,'host'):
            s=self.docSetup.getNodeText(c,'recent')
            if len(s)>0:
                try:
                    i=int(s)
                    if i<iRecent:
                        recentNode=c
                        iRecent=i
                except:
                    pass
        return recentNode
    def __getAutoConnect__(self,recentNode):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'',origin=self.GetOrigin())
        applNode=self.docSetup.getChild(recentNode,self.appl)
        sName=self.docSetup.getNodeText(applNode,'autoconnect')
        if sName=='1':
            return True
        elif sName=='0':
            return False
        else:
            self.docSetup.setNodeText(applNode,'autoconnect','1')
            return True
    def __genAutoFN__(self,bStore=True):
        #if self.bConnInfoSet==True:
        try:
            #fn=os.path.join(self.dn,string.join([self.mainAppl,self.appl,self.alias,self.host,self.usr],'_')+'.xml')
            fn=os.path.join(self.dn,self.host,self.usr,self.alias,self.appl+'.xml')
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'fn:%s'%(fn),origin=self.GetOrigin())
            if bStore:
                self.SetFN(fn)
            return fn
        except:
            vtLog.vtLngTB(self.GetOrigin())
            pass
        fn=os.path.join(self.dn,'local','usr','dummy',self.appl+'.xml')
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'fn:%s'%(fn),origin=self.GetOrigin())
        if bStore:
            self.SetFN(fn)
        return fn
        #return None
    def GenAutoFN(self,cfgNode,dn):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'',origin=self.GetOrigin())
        self.acquire('dom')
        try:
            if self.getNodeText(cfgNode,'port')=='offline':
                self.SetFN(self.getNodeText(cfgNode,'fn'))
            else:
                self.SetFN(os.path.join(dn,self.getNodeText(cfgNode,'host'),
                            self.getNodeText(cfgNode,'usr'),
                            self.getNodeText(cfgNode,'alias'),
                            self.appl+'.xml'))
        except:
            self.SetFN(os.path.join(dn,'local','usr','dummy',self.appl+'.xml'))
        self.release('dom')
        return self.sFN
    def GenAutoFNConnAppl(self,connAppl,dn):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,connAppl.GetHostConnectionStr(),origin=self.GetOrigin())
        try:
            self.SetFN(connAppl.GenAutoFN(dn))
            return self.GetFN()
        except:
            pass
        self.SetFN(os.path.join(dn,'local','usr','dummy',self.appl+'.xml'))
        return self.GetFN
        #return None
    def GetAutoFN(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'',origin=self.GetOrigin())
        #recentNode=self.__getRecentAlias__()
        #recentAliasNode=self.__getRecentConnection__(recentNode)
        #recentNode=self.__getApplHost__(recentNode)
        #if self.bConnInfoSet==True:
        if self.oSF.IsConnInfoSet:
            self.__genAutoFN__(bStore=True)
        #else:
        #    self.sFN=None
        return self.sFN
    def ClearAutoFN(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'',origin=self.GetOrigin())
        #vtLog.vtLngCur(vtLog.DEBUG,'\n\n\n\n',verbose=1)
        return 0
        if self.cltSrv is None:
            self.sFN=None
            return 1
        else:
            return 0
    def GetFN(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'GetFN',origin=self.GetOrigin())
        return self.sFN
    def OpenApplDocs(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        for k in self.dictDoc.keys():
            if k == self.appls[0]:
                continue
            self.dictDoc[k].Open()
    def SaveApplDocs(self,encoding='ISO-8859-1'):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        for k in self.dictDoc.keys():
            if k == self.appls[0]:
                continue
            self.dictDoc[k].Save(encoding=encoding)
    def GetAppl(self):
        return self.appl
    def __calcApplAlias__(self):
        self.SetApplAlias(':'.join([self.appl,self.alias]))
    #def GetApplAlias(self):
    #    return self.applAlias
    def GetAlias(self):
        return self.alias
    def GetHost(self):
        return self.host
    def GetPort(self):
        return self.port
    def GetUsr(self):
        return self.usr
    def Connect2Srv(self):
        try:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'alias:%s;host:%s;port:%s;'\
                    'usr:%s;'%(self.alias,self.host,self.port,
                    self.usr),origin=self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.sUsr=''
        self.iSecLv=-1
        self.bAdmin=False
        self.__genAutoFN__(bStore=True)
        #if self.bOnline==False:
        if self.oSF.IsOnLine==False:
            vtLog.vtLngCur(vtLog.DEBUG,'online:%d;fn:%s'%(self.oSF.IsOnLine,
                            self.GetFN()),origin=self.GetOrigin())
            return
        try:
            #wx.PostEvent(self,vtnxXmlConnect())
            #self.__connectionClosed__()
            #self.bDataSettled=False
            #self.bLoginFirst=True
            self.cltSrv=vtNetClt(self,self.host,self.port,
                                vtNetXmlCltSock,verbose=self.verbose-1,
                                sockpar=self,appl=self.GetAppl())
            #self.__setStatus__(self.CONNECT)
            self.oSF.SetState(self.oSF.CONNECT)
            self.cltSrv.Connect()
        except Exception,list:
            vtLog.vtLngTB(self.GetOrigin())
            self.__connectionClosed__(False)
            #self.Open()
            if self.cltSrv is not None:
                self.cltSrv.Stop()
            del self.cltSrv
            self.cltSrv=None
            self.cltSock=None
    def OnLeftDown(self, event):
        event.Skip()
    def OnRightDown(self, event):
        if not hasattr(self,'popupIdConnect'):
            self.popupIdConnect=wx.NewId()
            self.popupIdDisConnect=wx.NewId()
            self.popupIdSetup=wx.NewId()
            wx.EVT_MENU(self,self.popupIdConnect,self.OnConnect)
            wx.EVT_MENU(self,self.popupIdDisConnect,self.OnDisConnect)
            wx.EVT_MENU(self,self.popupIdSetup,self.OnSetup)
        
        menu=wx.Menu()
        mnIt=wx.MenuItem(menu,self.popupIdConnect,self.appl)
        #mnIt.SetBitmap(images.getGotoTmplBitmap())
        menu.AppendItem(mnIt)
        #mnIt.Enable(False)
        menu.AppendSeparator()
        if self.cltSock is None:
            mnIt=wx.MenuItem(menu,self.popupIdConnect,'Connect')
            #mnIt.SetBitmap(images.getGotoTmplBitmap())
            menu.AppendItem(mnIt)
        else:
            mnIt=wx.MenuItem(menu,self.popupIdDisConnect,'Disconnect')
            #mnIt.SetBitmap(images.getGotoTmplBitmap())
            menu.AppendItem(mnIt)
        menu.AppendSeparator()
        mnIt=wx.MenuItem(menu,self.popupIdSetup,'Setup')
        #mnIt.SetBitmap(images.getGotoTmplBitmap())
        menu.AppendItem(mnIt)
        self.PopupMenu(menu,wx.Point(event.GetX(), event.GetY()))
        menu.Destroy()
    def OnConnect(self,evt):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'OnConnect',origin=self.GetOrigin())
        #self.AutoConnect2Srv(force=True)    # 070623:wro replace old stuff
        #self.bLoginFirst=True
        self.oSF.SetFlag(self.oSF.CONN_ACTIVE,True)
        self.oSF.SetFlag(self.oSF.CONN_ATTEMPT,True)
        wx.CallAfter(self.Connect2Srv)
    def OnDisConnect(self,evt):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'OnDisConnect',origin=self.GetOrigin())
        #self.endEdit(None)
        self.DisConnect()
        return
        if self.cltSrv is not None:
            self.cltSrv.Stop()
        #self.__connectionClosed__()
        #wx.PostEvent(self,vtnxXmlDisConnect())
    def DisConnect(self):
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'DisConnect',origin=self.GetOrigin())
        #vtLog.vtLngCur(vtLog.DEBUG,'appl:%s'%(self.appl),1)
        #self.endEdit(None)
        #self.host=''
        self.port=-1
        #self.alias=''
        #self.usr=''
        #self.passwd=''
        self.sUsr=''
        self.iSecLv=-1
        self.bAdmin=False
        #self.SetFlag(self.CFG_VALID,False)
        #self.SetFlag(self.CONN_ACTIVE,False)
        #self.SetFlag(self.CONN_ATTEMPT,False)
        #self.SetFlag(self.DATA_SETTLED,True)
        #self.oSF.SetState(self.oSF.STOPPING)
        self.oSF.SetState(self.oSF.DISCONNECT)
        self.SetBlockNotify(False)
        
        # 070817: wro remove lock immediatly
        if self.SINGLE_LOCK!=0:
            self.idLock=''
        else:
            try:
                if hasattr(self,'lock'):
                    del self.lock
            except:
                vtLog.vtLngTB(self.GetOrigin())
                pass
            self.lock=vtNetLock(self.GetOrigin(),self)
        if self.cltSrv is None:
            self.oSF.SetState(self.oSF.DISCONNECTING)
        #if self.IsFlag(self.SHUTDOWN)==False:
        self.Stop(self.OnThreadStoppedDisconnect)
    def OnThreadStoppedDisconnect(self):
        try:
            vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
            #vtLog.vtLngCur(vtLog.ERROR,''%(),self.GetOrigin())
            #if self.IsFlag(self.OPENED):
            #self.oSF.SetState(self.oSF.STOPPED)
            if self.oSF.IsShutDown==False:
                self.StartConsumer()
            self.oSF.SetState(self.oSF.DISCONNECTED)
            if self.oSF.IsOpened:
                #self.__setStatus__(self.OPEN_OK)
                self.oSF.SetState(self.oSF.OPENED)
            else:
                #self.__setStatus__(self.STOPPED)
                self.oSF.SetState(self.oSF.STOPPED)
            #self.SetFlag(self.CONN_ACTIVE,False)
            #self.SetFlag(self.DATA_SETTLED,True)
            
            self.sUsr=''
            self.iSecLv=-1
            self.bAdmin=False
            #self.bDoAutoConnectAfterDisConn=False
            if self.cltSrv is not None:
                #self.cltSrv.Stop()
                if self.cltSock is not None:
                    self.cltSock.Close()
        finally:
            pass
        #if self.IsFlag(self.SHUTDOWN)==False:
        if self.oSF.IsShutDown==False:
            wx.PostEvent(self,vtnxXmlDisConnect(self))
    def IsConnAttempt(self):
        return self.oSF.IsConnAttempt
    def IsCfgValid(self):
        return self.oSF.IsCfgValid
    def IsConnActive(self):
        return self.oSF.IsConnActive
        if self.cltSrv is not None:
            #if self.GetStatus() != self.STOPPED:
            #    return True
            #else:
            #    return False
            #return self.cltSrv.IsServing()
            vtLog.vtLngCur(vtLog.DEBUG,'active',self.GetOrigin())
            return True
        else:
            vtLog.vtLngCur(vtLog.DEBUG,'inactive',self.GetOrigin())
            return False
    def ConnPause(self,flag):
        if self.cltSrv is not None:
            self.cltSrv.Pause(flag)
    def OnSetup(self,evt):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        if self.docSetup is not None:
            if self.appl==self.appls[0]:
                oldRecentAlias=self.sRecentAlias
                oldRecentConn=self.sRecentConn
                oldHost=self.host
                oldAlias=self.alias
                oldUsr=self.usr
                if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'recent alias:%s recent conn:%s'%(oldRecentAlias,oldRecentConn),origin=self.GetOrigin())
                    vtLog.vtLngCur(vtLog.DEBUG,'host:%s alias:%s usr:%s'%(oldHost,oldAlias,oldUsr),origin=self.GetOrigin())
            
        self.Setup()
    def Setup(self,node=None):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        if self.lstCfg is None:
            if node is None:
                node=self.nodeSetup
                if node is None:
                    node=self.docSetup.getRoot()
            if self.dlgSetup is None:
                self.dlgSetup=vNetPlaceDialog(self)
            self.dlgSetup.Centre()
            n=self.getChildByLst(node,self.lstSetupNode)
            if n is None:
                n=self.docSetup.createMissingSubNodeByLst(node,self.lstSetupNode)
            self.dlgSetup.SetNode(self.docSetup,n,self.appl)
            ret=self.dlgSetup.ShowModal()
            if ret>0:
                self.dlgSetup.GetNode()
                self.docSetup.AlignNode(n,iRec=2)
                self.Connect2SrvByNode(None,None)
                wx.PostEvent(self,vtnxXmlSetupChanged(self))
                pass
        else:
            if self.dlgCfgSetup is None:
                return
                #self.dlgCfgSetup=vNetCfgDialog(self)
            self.dlgCfgSetup.Centre()
            node=self.docSetup.getRoot()
            #recentOld=self.__getRecentConnection__()
            #infos=InfoDiff(self.docSetup)
            self.dlgCfgSetup.SelAppl(self.appl)
            self.dlgCfgSetup.SetNode(self.docSetup,self.lstCfg,self.appls,
                                self.appl,self.__getCfgNode__())
            ret=self.dlgCfgSetup.ShowModal()
            if ret>0:
                #recentNew=self.__getRecentConnection__()
                #res=infos.checkDiff(recentOld,recentNew)
                self.AutoConnect2Srv()
                #self.ReConnect2Srv()
                self.SaveCfg()
                #wx.PostEvent(self,vtnxXmlSetupChanged())
            #del infos
    def SaveCfg(self,fn=None):
        try:
            if hasattr(self.widLogging,'DoCallBackBranchWX'):
                self.docSetup.DoSafe(self.docSetup.AlignDoc)
                self.widLogging.DoCallBackMultiBranchWX(
                        (self.docSetup.__Save__,(fn,),{}),#{'bThread':True}),
                        {   None:(self.__SaveCfgAsShowDlg__,[],{}),
                            0:None,
                        })
            else:
                self.SaveCfgRisky(fn=fn)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def __SaveCfgAsShowDlg__(self):
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
        dlg = wx.FileDialog(self, _(u"Save Config File As"), ".", "", _(u"XML files (*.xml)|*.xml|all files (*.*)|*.*"), wx.SAVE)
        try:
            sFN=self.docSetup.GetFN()
            if sFN is not None:
                dlg.SetPath(sFN)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.SaveCfg(filename)
        finally:
            dlg.Destroy()
    def SaveCfgRiskt(self,fn=None):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'fn:%s'%fn,origin=self.GetOrigin())
        sekf.widLogging.DoCallBackBranchWX((self.docSetup.Save,(fn,),{}),
                    None,
                    (self.__SaveCfg__,(),{}))
        return
        self.docSetup.AlignDoc()
        if self.docSetup.Save(fn)<0:
            dlg = wx.FileDialog(self, "Save File As", ".", "", "XML files (*.xml)|*.xml|all files (*.*)|*.*", wx.SAVE)
            try:
                if dlg.ShowModal() == wx.ID_OK:
                    filename = dlg.GetPath()
                    #self.txtEditor.SaveFile(filename)
                    self.SaveCfg(filename)
            finally:
                dlg.Destroy()
        return 0
    def __SaveCfg__(self):
            dlg = wx.FileDialog(self, "Save File As", ".", "", "XML files (*.xml)|*.xml|all files (*.*)|*.*", wx.SAVE)
            try:
                if dlg.ShowModal() == wx.ID_OK:
                    filename = dlg.GetPath()
                    #self.txtEditor.SaveFile(filename)
                    self.SaveCfg(filename)
            finally:
                dlg.Destroy()

    def __setOffline__Old(self,node,bId=False,bContent=True,bRec=False):
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            #print
            #print
            #print
            #print
            vtLog.vtLngCur(vtLog.DEBUG,'',origin=self.GetOrigin())
        #if bContent or bId:
        #    if self.hasAttribute(node,'fingerprint')==False:
        #        self.setAttribute(node,'fingerprint',self.GetFingerPrint(node))
        if bContent:
            self.setAttribute(node,'offline_content','1')
            try:
                self.ids.AddOffline(long(self.getKey(node)))
            except:
                pass
            if self.__isSkip__(node):
                bRec=True
        if bId:
            self.setAttribute(node,'offline','1')
            try:
                self.ids.AddOffline(long(self.getKey(node)))
            except:
                pass
            bRec=True
        if bRec:
            for c in self.getChildsAttr(node,self.attr):
                self.__setOffline__(c,bId,bContent,bRec)
    def __setOffline__(self,node,action=0x02,bRec=False):
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'',origin=self.GetOrigin())
        id=self.getKeyNum(node)
        iRet=self.ids.AddOffline(id,action)
        self.setAttribute(node,'offline','0x%02x'%iRet)
        if bRec:
            for c in self.getChildsAttr(node,self.attr):
                self.__setOffline__(c,action,bRec)
    def GetConfig(self):
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
        if self.cltSock is None:
            return
        self.cltSock.GetConfig()
    def GetLoggedInAcl(self):
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
        if self.cltSock is None:
            return
        self.cltSock.GetLoggedInAcl()
    def GetKeys(self):
        if self.cltSock is None:
            return
        self.cltSock.GetKeys()
    def GetSynchNode(self,id):
        if long(id)<0:
            vtLog.vtLngCur(vtLog.WARN,'id:%s'%(id),origin=self.GetOrigin())
            node=self.getBaseNode()
        else:
            node=self.getNodeById(id)
        if node is not None:
            sFP=self.GetFingerPrintAndStore(node)
        else:
            sFP=''
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s fingerprint:%s'%(id,sFP),origin=self.GetOrigin())
        self.cltSock.GetSynchNode(id,sFP,self.getTagName(node))
    def GetSynchNodes(self,ids):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'ids:%s'%(vtLog.pformat(ids)),origin=self.GetOrigin())
        zStart=time.clock()
        #s=''
        ioContent=StringIO()
        for id in ids:
            if long(id)<0:
                node=self.getBaseNode()
            else:
                node=self.getNodeById(id)
            if node is not None:
                sFP=self.GetFingerPrintAndStore(node)
            else:
                sFP=''
            #s+=self.cltSock.GetSynchNodeStr(id,sFP,self.getTagName(node))
            ioContent.write(self.cltSock.GetSynchNodeStr(id,sFP,self.getTagName(node)))
        zFP=time.clock()
        #self.cltSock.Send(s)
        self.cltSock.Send(ioContent.getvalue())
        zEnd=time.clock()
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'dur:%4.1f calc fp:%4.1f'%(zEnd-zStart,zFP-zStart),origin=self.GetOrigin())
    def startEdit(self,node):
        if self.oSF.IsShutDown:#Flag(self.SHUTDOWN):
            return
        if node is None:
            vtLog.vtLngCur(vtLog.DEBUG,'node is None',origin=self.GetOrigin())
            return
        id=self.getKey(node)
        if self.SINGLE_LOCK!=0:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'id:%s;idLock:%s'%(id,self.idLock),origin=self.GetOrigin())
        else:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'id:%s;idLock;%s'%(id,self.lock),origin=self.GetOrigin())
        try:
            if self.audit.isEnabled():
                self.audit.write('startEdit',None,self.GetNodeXmlContent(node,False,False),
                            origin=self.getAuditOriginByNode(node))
        except:
            pass
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'%s'%node,origin=self.GetOrigin())
        if self.cltSock is None:
            # loop back
            if self.IsNodeAclOk(node,self.ACL_MSK_WRITE):
                wx.CallAfter(self.NotifyLockNode,id,'ok') #061119 wro delay post
            else:
                wx.CallAfter(self.NotifyLockNode,id,'rejected locked by permission') #061119 wro delay post
            return
        if self.SINGLE_LOCK!=0:
            if len(self.idLock)>0:
                if id!=self.idLock:
                    self.cltSock.unlock_node(self.idLock)
            #self.idLock=''
        else:
            if self.lock.HasLock(id):
                return
        self.cltSock.lock_node(id)
        pass
    def endEdit(self,node):
        if self.oSF.IsShutDown:#Flag(self.SHUTDOWN):
            return
        if node is None:
            vtLog.vtLngCur(vtLog.DEBUG,'node is None;unlock all',origin=self.GetOrigin())
            if self.SINGLE_LOCK!=0:
                if self.cltSock is None:
                    wx.CallAfter(self.NotifyUnLockNode,self.idLock,'released') #061119 wro delay post
                else:
                    self.cltSock.unlock_node(self.idLock)
            else:
                for id in self.lock.GetLockIds():
                    idLock=self.lock.GetLockId(id)
                    if self.cltSock is None:
                        wx.CallAfter(self.NotifyUnLockNode,idLock,'released') #061119 wro delay post
                    else:
                        self.cltSock.unlock_node(str(idLock))
            return
        id=self.getKey(node)
        if self.SINGLE_LOCK!=0:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'id:%s;idLock:%s'%(id,self.idLock),origin=self.GetOrigin())
        else:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'id:%s;idLock;%s'%(id,self.lock),origin=self.GetOrigin())
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'%s'%node,origin=self.GetOrigin())
        try:
            if self.audit.isEnabled():
                self.audit.write('endEdit',self.GetNodeXmlContent(node,False,False),
                            origin=self.getAuditOriginByNode(node))
        except:
            pass
        if self.SINGLE_LOCK!=0:
            if len(self.idLock)<=0:
                return
            idLock=self.idLock
        else:
            if self.lock.HasLock(id)==False:
                return
            idLock=str(self.lock.GetLockId(id))
        if self.cltSock is None:
            # loop back
            if self.SINGLE_LOCK!=0:
                wx.CallAfter(self.NotifyUnLockNode,self.idLock,'released') #061119 wro delay post
            else:
                wx.CallAfter(self.NotifyUnLockNode,id,'released') #061119 wro delay post
            return
        #id=self.getAttribute(node,self.attr)
        #if id==self.idLock:
            # ok
        self.cltSock.unlock_node(idLock)
        #self.cltSock.unlock_node(id)
        pass
    def reqLockFull(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%s'%id,origin=self.GetOrigin())
        self.cltSock.reqlockfull_node(-2)
    def relLockFull(self,kind,msg,id=''):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%s'%id,origin=self.GetOrigin())
        self.cltSock.rellockfull_node(id,kind,msg)
    def reqLock(self,node):
        if node is None:
            return
        id=self.getKey(node)
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%s'%id,origin=self.GetOrigin())
        self.cltSock.reqlock_node(id)
    def relLock(self,node,kind,msg,id=''):
        if node is None:
            if len(id)<=0:
                return
        else:
            id=self.getKey(node)
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%s'%id,origin=self.GetOrigin())
        self.cltSock.rellock_node(id,kind,msg)
    def IsLocked(self,node):
        id=self.getKey(node)
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%s'%id,origin=self.GetOrigin())
        if self.SINGLE_LOCK!=0:
            if self.idLock==id:
                return True
            else:
                return False
        else:
            return self.lock.HasLock(id)
    def doEdit(self,node,idLock=None):
        if self.oSF.IsShutDown:#IsFlag(self.SHUTDOWN):
            return
        id=self.getKey(node)
        if idLock is None:
            idLock=id
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'idLock:%s;    id:%s'%(idLock,id),origin=self.GetOrigin())
        if node is not None:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,
                    'fp:%s;%s'%(self.getFingerPrint(node),self.getKey(node)),origin=self.GetOrigin())
        try:
            if self.audit.isEnabled():
                self.audit.write('doEdit',self.GetNodeXmlContent(node,False,False),
                            origin=self.getAuditOriginByNode(node))
        except:
            pass
        if self.cltSock is None:
            #self.__setOffline__(node,bContent=True)
            self.__setOffline__(node,self.OFFLINE_EDIT)
            self.__HandleInternal__('upd',self.getKey(node))
            # loop back
            wx.CallAfter(self.NotifySetNode,id,id,'') #061119 wro delay post
            return
        if self.SINGLE_LOCK!=0:
            if len(self.idLock)<=0:
                return
            self.clearFingerPrint(node)
            self.cltSock.set_node(self.idLock,id)
        else:
            if self.lock.HasLock(idLock)==False:
                return
            idLock=self.lock.GetLockId(idLock)
            self.clearFingerPrint(node)
            self.cltSock.set_node(idLock,id)
        self.calcFingerPrint(node)
    def doEditFull(self,node):
        if self.oSF.IsShutDown:#Flag(self.SHUTDOWN):
            return
        id=self.getKey(node)
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%s'%id,origin=self.GetOrigin())
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,
                'fp:%s;%s'%(self.getFingerPrint(node),node),origin=self.GetOrigin())
        try:
            if self.audit.isEnabled():
                self.audit.write('doEditFull',self.GetNodeXmlContent(node,False,True),
                            origin=self.getAuditOriginByNode(node))
        except:
            pass
        if self.cltSock is None:
            #self.__setOffline__(node,bContent=True,bRec=True)
            self.__setOffline__(node,self.OFFLINE_EDIT,bRec=True)
            self.__HandleInternal__('upd',self.getKey(node))
            def handleinternal(node):
                self.__HandleInternal__('upd',self.getKey(node))
                self.procChildsKeys(node,handleinternal)
            self.procChildsKeys(node,handleinternal)
            # loop back
            wx.CallAfter(self.NotifySetNode,id,id,'') #061119 wro delay post
            return
        if self.SINGLE_LOCK!=0:
            if len(self.idLock)<=0:
                return
            self.clearFingerPrintFull(node)
            self.cltSock.set_node_full(self.idLock,id)
        else:
            if self.lock.HasLock(id)==False:
                return
            idLock=self.lock.GetLockId(id)
            self.clearFingerPrintFull(node)
            self.cltSock.set_node_full(idLock,id)
        self.calcFingerPrintFull(node)
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,
                'do edit full fp:%s'%self.getFingerPrint(node),origin=self.GetOrigin())
        
    def doUpdate(self,node):
        if self.oSF.IsShutDown:#IsFlag(self.SHUTDOWN):
            return
        id=self.getKey(node)
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%s'%id,origin=self.GetOrigin())
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,
                'fp:%s;%s'%(self.getFingerPrint(node),node),origin=self.GetOrigin())
        try:
            if self.audit.isEnabled():
                self.audit.write('doUpdate',self.GetNodeXmlContent(node,False,False),
                            origin=self.getAuditOriginByNode(node))
        except:
            pass
        if self.cltSock is None:
            #self.__setOffline__(node,bContent=True)
            self.__setOffline__(node,self.OFFLINE_EDIT)
            # loop back
            wx.CallAfter(self.NotifySetNode,id,id,'') #061119 wro delay post
            return
        #if len(self.idLock)<=0:
        #    return
        self.clearFingerPrint(node)
        self.cltSock.set_node(id,id)
        self.GetFingerPrintAndStore(node)
    def changeIdNet(self,node,id):
        if node is None:
            return
        if self.thdSynch.IsRunning():
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'id:%s;idNew:%s;skipped dur thdSynch running'%(id,self.getKey(node)),origin=self.GetOrigin())
            return
        else:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'id:%s;idNew:%s'%(id,self.getKey(node)),origin=self.GetOrigin())
        if self.SINGLE_LOCK==0:
            self.lock.ChangeId(id,self.getKey(node))
    def addNode2Srv(self,par,node,func=None,**kwargs):
        if self.oSF.IsShutDown:#IsFlag(self.SHUTDOWN):
            vtLog.vtLngCur(vtLog.CRITICAL,'shutdown active'%(),self.GetOrigin())
            return
        if node is None:
            return
        idPar=self.getAttribute(par,self.attr)
        id=self.getAttribute(node,self.attr)
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%s;parId:%s'%(id,idPar),origin=self.GetOrigin())
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'%s'%node,origin=self.GetOrigin())
        if func is not None:
            func(par,node,**kwargs)
        try:
            if self.audit.isEnabled():
                self.audit.write('addNode2Srv:%s to:%s'%(id,idPar),self.GetNodeXmlContent(node,False,False),
                            origin=self.getAuditOriginByNode(node))
        except:
            pass
        if self.cltSock is None:
            vtLog.vtLngCur(vtLog.CRITICAL,'is offline'%(),self.GetOrigin())
            return
        self.cltSock.addNode(idPar,id)
        self.GetFingerPrintAndStore(node)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'%s'%node,origin=self.GetOrigin())
        #return vNetXmlWxGui.addNode(self,par,node,func=None,**kwargs)
    def addNode(self,par,node,func=None,**kwargs):
        if self.oSF.IsShutDown:#IsFlag(self.SHUTDOWN):
            return
        if node is None:
            return
        idPar=self.getAttribute(par,self.attr)
        id=self.getAttribute(node,self.attr)
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%s;parId:%s'%(id,idPar),origin=self.GetOrigin())
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'%s'%node,origin=self.GetOrigin())
        if func is not None:
            func(par,node,**kwargs)
        try:
            if self.audit.isEnabled():
                self.audit.write('addNode:%s to:%s'%(id,idPar),self.GetNodeXmlContent(node,False,False),
                            origin=self.getAuditOriginByNode(node))
        except:
            pass
        if self.cltSock is None:
            #self.__setOffline__(node,self.OFFLINE_ADD)
            self.procKeysRec(100,node,self.__addNodeOffLine__)
            # loop back
            wx.CallAfter(self.NotifyAddNodeResponse,id,id,'ok') #061119 wro delay post
            return
        self.procKeysRec(100,node,self.__addNodeCltSock__)
        #self.cltSock.addNode(idPar,id)
        #self.GetFingerPrintAndStore(node)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'%s'%node,origin=self.GetOrigin())
    def __addNodeOffLine__(self,node):
        self.__setOffline__(node,self.OFFLINE_ADD)
        return 0
    def __addNodeCltSock__(self,node):
        par=self.getParent(node)
        idPar=self.getAttribute(par,self.attr)
        id=self.getAttribute(node,self.attr)
        self.thdProc.DoCallBack(self.cltSock.addNode,idPar,id)  # 081222:wro delay this call
        #self.cltSock.addNode(idPar,id)
        #self.GetFingerPrintAndStore(node)
        return 0
    def delNode2Srv(self,node):
        return vNetXmlWxGui.delNode(self,node)
    def delNode(self,node):
        if self.oSF.IsShutDown:#IsFlag(self.SHUTDOWN):
            return
        if node is None:
            return
        id=self.getKey(node)
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%s'%id,origin=self.GetOrigin())
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'%s'%node,origin=self.GetOrigin())
        try:
            if self.audit.isEnabled():
                self.audit.write('delNode',self.GetNodeXmlContent(node,True,True),
                            origin=self.getAuditOriginByNode(node))
        except:
            pass
        if self.cltSock is None:
            #self.deleteNode(node)
            self.__setOffline__(node,self.OFFLINE_DEL,bRec=True)
            # loop back
            wx.CallAfter(self.NotifyDelNode,id,'ok') #061119 wro delay post
            return
        self.cltSock.delNode(id)
    def setNode2Srv(self,node):
        return vNetXmlWxGui.setNode(self,node)
    def setNode(self,node):
        if self.oSF.IsShutDown:#IsFlag(self.SHUTDOWN):
            return
        if node is None:
            return
        id=self.getKey(node)
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%s'%id,origin=self.GetOrigin())
        self.clearFingerPrint(node) # FIXME??? wro 060913
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'%s'%node,origin=self.GetOrigin())
        try:
            if self.audit.isEnabled():
                self.audit.write('setNode',self.GetNodeXmlContent(node,False,False),
                            origin=self.getAuditOriginByNode(node))
        except:
            pass
        if self.cltSock is None:
            #self.__setOffline__(node,bContent=True)
            self.__setOffline__(node,self.OFFLINE_EDIT)
            # loop back
            wx.CallAfter(self.NotifySetNode,id,id,'') #061119 wro delay post
            return
        self.cltSock.set_node(id,id)
        self.GetFingerPrintAndStore(node)
    def setNodeFull(self,node):
        if self.oSF.IsShutDown:#IsFlag(self.SHUTDOWN):
            return
        if node is None:
            return
        id=self.getKey(node)
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%s'%id,origin=self.GetOrigin())
        self.clearFingerPrint(node) # FIXME??? wro 060913
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'%s'%node,origin=self.GetOrigin())
        try:
            if self.audit.isEnabled():
                self.audit.write('setNodeFull',self.GetNodeXmlContent(node,False,True),
                            origin=self.getAuditOriginByNode(node))
        except:
            pass
        if self.cltSock is None:
            #self.__setOffline__(node,bContent=True)
            self.__setOffline__(node,self.OFFLINE_EDIT,bRec=True)
            # loop back
            #wx.CallAfter(self.NotifyGetNode,id) #061119 wro delay post
            wx.CallAfter(self.NotifySetNode,id,id,'') #070622 wro delay post
            return
        self.cltSock.set_node_full(id,id)
        self.GetFingerPrintAndStore(node)
    def moveNode2Srv(self,par,node):
        return vNetXmlWxGui.moveNode(self,par,node)
    def moveNode(self,par,node):
        if self.oSF.IsShutDown:#IsFlag(self.SHUTDOWN):
            return
        idPar=self.getKey(par)
        id=self.getKey(node)
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'parId:%s;id:%s'%(idPar,id),origin=self.GetOrigin())
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'%s'%node,origin=self.GetOrigin())
        try:
            if self.audit.isEnabled():
                self.audit.write('moveNode:%d from:%d to:%d'%\
                        (self.getKeyNum(node),self.getKeyNum(self.getParent(node)),self.getKeyNum(par),),\
                        self.GetNodeXmlContent(node,False,False),
                        origin=self.getAuditOriginByNode(node))
        except:
            pass
        if self.cltSock is None:
            # loop back
            #self.NotifyMoveNode(idPar,id)
            self.__setOffline__(node,self.OFFLINE_MOVE,bRec=True)
            wx.CallAfter(self.NotifyMoveNodeResponse,idPar,id,'ok') #061119 wro delay post
            return
        self.cltSock.moveNode(idPar,id)
    def synchNode(self,parID,id,level,s):
        if self.oSF.IsShutDown:#Flag(self.SHUTDOWN):
            return
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'parid:%s id:%s lv:%s'%(parID,id,level),origin=self.GetOrigin())
        #if len(parID)==0:
        if len(s)==0:
            # fingerprint identical
            self.thdSynch.__procResponse__(parID,id,level,None)
        else:
            tmpDoc,tmp=self.getNode2Add(s)
            self.thdSynch.__procResponse__(parID,id,level,tmp)
            #tmpDoc.freeDoc()
            self.__freeDocRaw__(tmpDoc)
            return
            tmpDoc=libxml2.parseMemory(s,len(s))
            tmp=tmpDoc.getRootElement()
            tmp.unlinkNode()
            tmpDoc.setRootElement(None)
            self.thdSynch.__procResponse__(parID,id,level,tmp)
            #del tmpDoc         # 061008 wro
            tmpDoc.freeDoc()   # 061008 wro
    def __initCmdRemoteDict__(self):
        if hasattr(self,'CMD_DICT')==False:
            self.CMD_DICT={}
        self.CMD_DICT.update({
                '__cloneAlias__':(_('clone alias'),0x06,{
                        'dest':(_(u'destination'),'string',None),
                        'destFN':(_(u'destination filename'),'string',None),
                        'source':(_(u'source'),'string',None),
                        }),
                '__start__':(_('clone alias'),0x02,{
                        'source':(_(u'source'),'string',None),
                        }),
                '__stop__':(_('clone alias'),0x02,{
                        'source':(_(u'source'),'string',None),
                        }),
                })
    def DoCmdRemote(self,action,**kwargs):
        """ return
            1       ... action executed correctly
            0       ... action unknown
           -1       ... fault occured during execution 
           -2       ... permission denied
           -3       ... action execution in process
           -4       ... remote execution not possible
        """
        if self.cltSock is None:
            vtLog.PrintMsg(_('remote execution not possible'),self.widLogging)
            return -4
        self.cltSock.Cmd(action,**kwargs)
    def __connectionClosed__(self,established=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        #vtLog.vtLngCur(vtLog.ERROR,''%(),self.GetOrigin())
        #vtLog.vtLngCur(vtLog.DEBUG,'appl:%s'%(self.appl),1)
        #if established==False:
        #    if self.fn is not None:
        #        self.Open()
        #        self.NotifyContent()
        #self.SetFlag(self.CONN_ACTIVE,False)
        #self.SetFlag(self.CFG_VALID,False)
        #self.SetFlag(self.CONN_ATTEMPT,False)
        #self.SetFlag(self.DATA_SETTLED,True)
        #self.__setStatus__(self.STOPPED)
        #self.oSF.SetState(self.oSF.STOPPED)
        self.oSF.SetState(self.oSF.DISCONNECTING)
        if self.oSF.IsDisConnect==False:
            self.thdProcNet.DoCallBack(self.OnThreadStoppedDisconnect)
        #if self.oSF.IsOpened:
        #    self.oSF.SetState(self.oSF.OPENED)
        #else:
        #    self.oSF.SetState(self.oSF.STOPPED)
        #self.host=''
        self.port=-1
        #self.alias=''
        #self.usr=''
        #self.passwd=''
        if self.SINGLE_LOCK!=0:
            self.idLock=''
        else:
            try:
                if hasattr(self,'lock'):
                    del self.lock
            except:
                vtLog.vtLngTB(self.GetOrigin())
                pass
            self.lock=vtNetLock(self.GetOrigin(),self)
        
        self.sUsr=''
        self.iSecLv=-1
        self.bAdmin=False
        #self.bDataSettled=True
        #self.bLoginFirst=False
        #if self.cltSrv is not None:
        #    del self.cltSrv
        self.cltSrv=None
        self.cltSock=None
        #if self.IsBlockNotify()==False:
        if self.oSF.IsBlockNotify==False:
            wx.PostEvent(self,vtnxXmlClosed(self))
        #if self.bDoAutoConnectAfterDisConn:
        #    self.bDoAutoConnectAfterDisConn=False
        #    self.AutoConnect2Srv(force=True)
    def NotifyOpenStart(self,fn):
        if self.oSF.IsShutDown:
            return
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        #if self.silent==False:
        #    self.NotifyContent()
        self.sUsr=''
        self.iSecLv=-1
        self.bAdmin=False
        #self.__setStatus__(self.OPEN_START)
        self.oSF.SetState(self.oSF.OPEN)
        #if self.IsBlockNotify()==False:
        if self.oSF.IsBlockNotify==False:
            wx.PostEvent(self,vtnxXmlOpenStart(self))
        
    def NotifyOpenOk(self,fn):
        if self.oSF.IsShutDown:
            return
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        #self.__setStatus__(self.OPEN_OK)
        self.oSF.SetState(self.oSF.OPENED)
        #if self.IsFlag(self.CONN_ACTIVE):
        if self.oSF.IsConnActive:
            # start delay connection attept 
            self.Connect2Srv()
        if self.oSF.IsOnLine==False:
            self.oSF.SetFlag(self.oSF.BLOCK_NOTIFY,False)
        #if self.IsBlockNotify()==False:
        if self.oSF.IsBlockNotify==False:
            wx.PostEvent(self,vtnxXmlOpenOk(self))
        if self.cltSrv is None:
                # 070422:wro 
                if hasattr(self,'CreateReq'):
                    self.CreateReq()
                self.Build()
                #self.bDataSettled=True
                self.oSF.SetFlag(self.oSF.STLD)
        #if self.bSynch:
        #    self.bDataSettled=True
        #if self.bDataSettled==True:
        #if self.IsFlag(self.SYNCHABLE)==False:
        self.NotifyContent()
        
    def NotifyOpenFault(self,fn,sExp):
        if self.oSF.IsShutDown:
            return
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        self.oSF.SetState(self.oSF.OPEN_FLT)
        if self.oSF.IsOnLine==False:
            self.oSF.SetFlag(self.oSF.STLD,True)
        self.New(bAcquire=False)
        #if self.IsBlockNotify()==False:
        if self.oSF.IsBlockNotify==False:
            wx.PostEvent(self,vtnxXmlOpenFault(self))
        #self.__setStatus__(self.OPEN_FLT)
        #if self.IsFlag(self.CONN_ACTIVE):
        if self.oSF.IsConnActive:
            # start delay connection attept 
            self.Connect2Srv()
        else:
            self.NotifyContent()
    def NotifySynchStart(self,count):
        if self.oSF.IsShutDown:
            return
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        #self.__setStatus__(self.SYNCH_START,count)
        self.oSF.SetState(self.oSF.SYNCH_START)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'synch start count:%7d'%count,origin=self.GetOrigin())
        wx.PostEvent(self,vtnxXmlSynchStart(self,count))
        #if self.IsBlockNotify()==False:
        #if self.oSF.IsBlockNotify==False:
        #    wx.PostEvent(self,vtnxXmlSynchStart(self,count))
    def NotifySynchProc(self,act,count):
        if self.oSF.IsShutDown:
            return
        try:
            if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'act:%7d - %7d'%(act,count),origin=self.GetOrigin())
            #self.__setStatus__(self.SYNCH_PROC,act,count)
            self.oSF.SetState(self.oSF.SYNCH_PROC)
            wx.PostEvent(self,vtnxXmlSynchProc(self,act,count))
        except:
            vtLog.vtLngTB(self.GetOrigin())
        #if self.IsBlockNotify()==False:
        #if self.oSF.IsBlockNotify==False:
        #    wx.PostEvent(self,vtnxXmlSynchProc(self,act,count))
    def NotifySynchFinished(self,bChanged):
        if self.oSF.IsShutDown:
            return
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        #self.DoEditResolve()
        #self.__updateConfig__()
        #self.__setStatus__(self.SYNCH_FIN)
        self.oSF.SetState(self.oSF.SYNCH_FIN)
        #self.SetFlag(self.BLOCK_NOTIFY,False) # 060521
        #self.bDataSettled=True
        #vtLog.vtLngCur(vtLog.DEBUG,'%04x'%(self.iStatus),verbose=1)
        self.NotifyContent()
        wx.PostEvent(self,vtnxXmlSynchFinished(self,bChanged))
        #if self.IsBlockNotify()==False:
        #if self.oSF.IsBlockNotify==False:
        #    wx.PostEvent(self,vtnxXmlSynchFinished(self,bChanged))
        
    def NotifySynchAborted(self,bChanged):
        if self.oSF.IsShutDown:
            return
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        #self.__setStatus__(self.SYNCH_ABORT)
        #self.SetFlag(self.BLOCK_NOTIFY,False)
        #self.bDataSettled=True
        self.oSF.SetState(self.oSF.SYNCH_ABORT)
        self.NotifyContent()
        wx.PostEvent(self,vtnxXmlSynchAborted(self,bChanged))
        #if self.IsBlockNotify()==False:
        #if self.oSF.IsBlockNotify==False:
        #    wx.PostEvent(self,vtnxXmlSynchAborted(self,bChanged))
    
    def NotifyConnected(self):
        if self.oSF.IsShutDown:
            return
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        try:
            self.cltSock=self.cltSrv.GetSocketInstance()
            if self.cltSock is None:
                self.__connectionClosed__(False)
                return
            #self.cltSock.SetNodes2Keep(self.nodes2keep)
            self.cltSock.SetParent(self)
            self.cltSock.LoginEncoded(self.usr,self.passwd)
            self.cltSock.Alias(self.appl,self.alias)
            #if self.verbose:
            #    vtLog.vtLngCur(vtLog.DEBUG,'alias set')
            #self.__setStatus__(self.CONNECTED)
            self.oSF.SetState(self.oSF.CONNECTED)
            self.GetAutoFN()
            #if self.IsBlockNotify()==False:
            if self.oSF.IsBlockNotify==False:
                wx.PostEvent(self,vtnxXmlConnected(self))
        except Exception,list:
            if vtLog.vtLngIsLogged(vtLog.ERROR):
                vtLog.vtLngCur(vtLog.ERROR,'',origin=self.GetOrigin())
                vtLog.vtLngCur(vtLog.DEBUG,'exception:%s'%(repr(list)),self.GetOrigin())
            self.NotifyConnectionFault()
    def NotifyConnectionFault(self):
        if self.oSF.IsShutDown:
            return
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        self.__connectionClosed__(False)
        #self.Open()
        if self.cltSrv is not None:
            self.cltSrv.Stop()
        del self.cltSrv
        self.cltSrv=None
        self.cltSock=None
        self.sUsr=''
        self.iSecLv=-1
        self.bAdmin=False
        #self.bDataSettled=True
        #self.__setStatus__(self.CONNECT_FLT)
        self.oSF.SetState(self.oSF.CONNECT_FLT)
        #if self.IsBlockNotify()==False:
        if self.oSF.IsBlockNotify==False:
            wx.PostEvent(self,vtnxXmlConnectFault(self))
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'',self.GetOrigin())
        
    def NotifyLogin(self):
        if self.oSF.IsShutDown:
            return
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        #self.__setStatus__(self.LOGIN)
        self.oSF.SetState(self.oSF.LOGIN)
        pass
    def Login(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        dlg=None
        if self.netMaster.AcquireLogin()==False:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'unable to acquire login lock',origin=self.GetOrigin())
            return
        bInteractive=True
        try:
            #if self.bLoginFirst:
            if self.oSF.IsLoginFirst:
                #self.bLoginFirst=False
                self.oSF.SetFlag(self.oSF.LOGIN_FIRST,False)
                sPasswd=self.netMaster.CheckLogin(self.appl,self.host,self.port,
                                    self.alias,self.usr)
                if sPasswd is not None:
                    #self.netMaster.SetLogin(self.appl,self.host,self.port,
                    #                self.alias,self.usr,self.passwd)
                    bInteractive=False
                    self.passwd=sPasswd
            if bInteractive:
                if isinstance(threading.currentThread(),threading._MainThread)==False:
                    vtLog.vtLngCur(vtLog.CRITICAL,'',origin=self.GetOrigin())
                    self.netMaster.ReleaseLogin()
                    return
                vtLog.PrintMsg(_(u'login to %s:%s ...')%(self.appl,self.alias),self)
                dlg=self.netMaster.GetLoginDlg()
                #while dlg.IsShown():  # 070120 wro do not block main loop
                #    time.sleep(1)
                #if dlg.IsShown():
                #    return
                dlg.Centre()
                dlg.SetValues(self.appl,self.host,self.port,self.alias,self.usr)
                dlg.Show(True)
                if 0:
                    if dlg.ShowModal()>0:
                        self.usr=dlg.GetUsr()
                        self.passwd=dlg.GetPasswd()
                        self.netMaster.UpdateLogin(self.appl,self.usr,self.passwd)
                        self.netMaster.SetLogin(self.appl,self.host,self.port,
                                        self.alias,self.usr,self.passwd)
                        pass
                    else:
                        #self.netMaster.ReleaseLogin()
                        return
                else:
                    #self.netMaster.ReleaseLogin()
                    return
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.netMaster.ReleaseLogin()
        if self.cltSock is not None:
            #self.__setStatus__(self.LOGIN_ATTEMPT)
            self.oSF.SetState(self.oSF.LOGIN_ATTEMPT)
            #self.oSF.SetFlag(self.oSF.LOGIN_ATTEMPT)
            self.cltSock.LoginEncoded(self.usr,self.passwd)
            self.cltSock.__doLogin__()
        else:
            vtLog.vtLngCur(vtLog.ERROR,'lost socket',self.GetOrigin())
            self.DisConnect()
    def LoginFromGui(self,usr,passwd):
        try:
            self.usr=usr
            self.passwd=passwd
            if self.cltSock is not None:
                vtLog.PrintMsg(_(u'send login for %s:%s to %s:%s...')%(self.appl,self.alias,self.host,self.port),self)
                #self.__setStatus__(self.LOGIN_ATTEMPT)
                self.oSF.SetState(self.oSF.LOGIN_ATTEMPT)
                self.cltSock.LoginEncoded(self.usr,self.passwd)
                self.cltSock.__doLogin__()
                return True
            else:
                vtLog.vtLngCur(vtLog.ERROR,'lost socket',self.GetOrigin())
                self.DisConnect()
        except:
            vtLog.vtLngTB(self.appl)
        return False
    def NotifyLoggedIn(self,o):
        if self.oSF.IsShutDown:
            return
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'loggedin synch:%d'%self.oSF.IsSynchable,origin=self.GetOrigin())
        self.sUsr=o.GetUsr()
        self.iUsrId=o.GetUsrId()
        self.iSecLv=o.GetSecLv()
        self.bAdmin=o.IsAdmin()
        self.SetLogin(o)
        try:
            self.netMaster.SetLogin(self.appl,self.host,self.port,
                                    self.alias,self.usr,self.passwd)
            self.netMaster.UpdateLoginInfo(self.appl,o)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'usr:%s;secLv:%d;admin:%d'%(self.sUsr,self.iSecLv,self.bAdmin),origin=self.GetOrigin())
        #self.__setStatus__(self.LOGGEDIN)
        self.oSF.SetState(self.oSF.LOGGEDIN)
        #if self.IsBlockNotify()==False:
        if self.oSF.IsBlockNotify==False:
            wx.PostEvent(self,vtnxXmlLoggedin(self))
        #self.cltSock.SendTelegram(u'get_config')
        #self.getDataFromRemote()
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'notify loggedin fin',origin=self.GetOrigin())
    def NotifyCmd(self,action,iRet):
        if self.oSF.IsShutDown:
            return
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'action:%s;result:%d'%(action,iRet),origin=self.GetOrigin())
        if iRet==1:
            sMsg=_(u'executed correctly')
        elif iRet==0:
            sMsg=_(u'unknown')
        elif iRet==-1:
            sMsg=_(u'fault occured during execution')
        elif iRet==-2:
            sMsg=_(u'permission denied')
        elif iRet==-3:
            sMsg=_(u'execution in process')
        else:
            sMsg=_(u'unknown result %d')%iRet
        vtLog.PrintMsg(_(u'action %s ')%action+sMsg,self.widLogging)
        wx.PostEvent(self,vtnxXmlCmd(self,action,iRet))
    def NotifyBrowse(self,l):
        if self.oSF.IsShutDown:
            return
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        wx.PostEvent(self,vtnxXmlBrowse(self,l))
    def GetLoggedInSecLv(self):
        return self.iSecLv
        if self.cltSock is not None:
            return self.iSecLv
        else:
            return -1
    def GetLoggedInUsr(self):
        if self.cltSock is not None:
            return self.sUsr
        else:
            return -1
    def GetLoggedInUsrId(self):
        if self.cltSock is not None:
            return self.iUsrId
        else:
            return -1
    def IsLoggedInAdmin(self):
        if self.cltSock is not None:
            return self.iSecLv==32
        return False
    #def IsLoggedInSelf(self,node):
    #    return False
    def getDataFromRemote(self):
        vtLog.vtLngCur(vtLog.INFO,'synchable:%d'%(self.oSF.IsSynchable),self.GetOrigin())
        self.__logStatus__()
        #if self.IsFlag(self.SYNCHABLE)==False:
        if self.oSF.IsSynchable==False:
            #time.sleep(200)
            self.cltSock.SendTelegram(u'get_content')
            pass
        else:
            self.thdSynch.Start()
            self.thdSynch.Synch()
    def GetSynchNotify(self):
        return self.thdSynch.GetNotify()
    def GetSynchFN(self):
        return self.thdSynch.GetSynchFN()
    def GetEditResolveDlg(self):
        #vtLog.CallStack(self.GetOrigin())
        if self.netMaster is not None:
            return self.netMaster.GetEditResolveDlg()
        return None
    def NotifyCountKey(self,count):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'',origin=self.GetOrigin())
        pass
    def NotifyCountElement(self,count):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'',origin=self.GetOrigin())
        pass
    def NotifyCountNode(self,count):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'',origin=self.GetOrigin())
        pass
    def NotifyConfig(self,content):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'',origin=self.GetOrigin())
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,content,origin=self.GetOrigin())
        #self.__setStatus__(self.CONFIG)
        self.oSF.SetState(self.oSF.CONFIG)
        #if self.IsBlockNotify()==False:
        if self.oSF.IsBlockNotify==False:
            wx.PostEvent(self,vtnxXmlConfig(self))
        pass
    def NotifyAcl(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'',origin=self.GetOrigin())
    def NotifyKeys(self,content):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'',origin=self.GetOrigin())
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,content,origin=self.GetOrigin())
        #self.__setStatus__(self.CONFIG)
        self.thdSynch.SetKeys(content)
        #if self.IsBlockNotify()==False:
        #if self.oSF.IsBlockNotify==False:
        #    wx.PostEvent(self,vtnxXmlConfig(self))
        pass
    
    def __getHostAliasName__(self,n):
        sName=self.getNodeText(n,'name')+':'+self.getNodeText(n,'port')+'//'+self.getNodeText(n,'alias')
        return sName
    def __getConnHostAliasName__(self,n):
        sName=self.getNodeText(n,'host')+':'+self.getNodeText(n,'port')+'//'+self.getNodeText(n,'alias')
        return sName
    def __updateConfig__(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'update config',origin=self.GetOrigin())
        if self.docSetup is None:
            return
        if self.appls[0]!=self.appl:
            return
        #vtLog.vtLngCur(vtLog.DEBUG,'appl:%s'%(self.GetOrigin()),1)
        recentNode=self.__getRecentAlias__()
        if recentNode is None:
            return
        #if self.verbose:
        #    vtLog.vtLngCur(vtLog.DEBUG,'alias:%s'%recentNode)
        recentConnNode=self.__getRecentConnection__(recentNode)
        if recentConnNode is None:
            return
        #self.netMaster.AcquireLogin()
        #if self.verbose:
        #    vtLog.vtLngCur(vtLog.DEBUG,'conn:%s'%recentConnNode)
        cfgNode=self.getChild(self.getRoot(),'config')
        #if cfgNode is None:
        #    return
        if cfgNode is not None:
            for a in self.appls[1:]:
                bAdd=False
                n=self.getChild(cfgNode,a)
                if n is None:
                    continue
                childs=self.getChilds(recentNode,a)
                for c in childs:
                    self.deleteNode(c,recentNode)
                nn=self.cloneNode(n,True)
                self.appendChild(recentNode,nn)
                
                d={}
                for c in self.getChilds(nn,'host'):
                    d[self.__getHostAliasName__(c)]=c
                bAdd=False
                child=self.getChildForced(recentConnNode,a)
                bAutoConnect=''
                childs=self.getChilds(child,'host')
                connHosts={}
                for c in childs:
                    connHosts[self.__getConnHostAliasName__(c)]=c
                    #bAutoConnect=self.getNodeText(c,'autoconnect')
                iRecent=len(connHosts.keys())
                for k in d.keys():
                    if connHosts.has_key(k):
                        pass
                    else:
                        tmp=d[k]
                        sName=self.getNodeText(tmp,'name')
                        sPort=self.getNodeText(tmp,'port')
                        sAlias=self.getNodeText(tmp,'alias')
                        n=self.createChildByLst(child,'host',
                                        [{'tag':'name','val':sName},
                                         {'tag':'recent','val':str(iRecent)},
                                         {'tag':'alias','val':sAlias},
                                         {'tag':'host','val':sName},
                                         {'tag':'port','val':sPort},
                                         {'tag':'usr','val':self.usr},
                                         {'tag':'passwd','val':self.passwd}
                                        ])
                for k in connHosts.keys():
                    if d.has_key(k):
                        pass
                    else:
                        self.deleteNode(connHosts[k],child)
                #self.setNodeText(nn,'autoconnect',bAutoConnect)
            self.AlignNode(recentNode,iRec=3)
        self.SetCfgNode(cfgNode)
        for k in self.dictDoc.keys():
            if k == self.appls[0]:
                continue
            self.dictDoc[k].ReConnect2Srv()
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'finished',origin=self.GetOrigin())
        #self.netMaster.ReleaseLogin()
        #vtLog.vtLngCur(vtLog.DEBUG,'appl:%s finished'%(self.GetOrigin()),1)
        #if self.verbose:
        #    vtLog.vtLngCur(vtLog.DEBUG,'recent:%s'%recentNode)
    def NotifyServed(self,served):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'served:%d'%served,origin=self.GetOrigin())
        if served==False:
            if 1==1:
                self.__connectionClosed__(False)
                if self.cltSrv is not None:
                    self.cltSrv.Stop()
                del self.cltSrv
                self.cltSrv=None
                self.cltSock=None
            #self.__setStatus__(self.SERVE_FLT)
            self.oSF.SetState(self.oSF.SERVE_FLT)
        else:
            #self.__setStatus__(self.SERVED)
            self.oSF.SetState(self.oSF.SERVED)
    def NotifyContent(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'setteled:%d;block:%d'%(self.IsSettled(),
                            self.oSF.IsBlockNotify),origin=self.GetOrigin())
        self.__logStatus__()
        #self.__updateConfig__()
        #self.genIds()
        
        #if self.IsFlag(self.SYNCHABLE)==False:
        if self.IsSettled():
            #if hasattr(self,'CreateReq'):
            #    self.CreateReq()
            #self.Build()        # 061006 wro lead to blocking vMESCenter
                                # 061007 wro can't be deactivated!
                                # 070421 wro isn't seems to be risky
            #if self.IsBlockNotify()==False:
            if self.oSF.IsBlockNotify==False:
                wx.PostEvent(self,vtnxXmlGotContent(self))
                wx.PostEvent(self,vtnxXmlContentChanged(self))
                wx.PostEvent(self,vtnxXmlBuildTree(self))
    def NotifyGetNode(self,id):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%s'%(id),origin=self.GetOrigin())
        try:
            sID=self.ConvertIdStr2Str(id)
            #if self.IsBlockNotify()==False:
            if self.oSF.IsBlockNotify==False:
                wx.PostEvent(self,vtnxXmlGetNode(self,sID))
                wx.PostEvent(self,vtnxXmlContentChanged(self))
        except:
            vtLog.vtLngTB(self.GetOrigin())
            if vtLog.vtLngIsLogged(vtLog.ERROR):
                vtLog.vtLngCur(vtLog.ERROR,'notify get node id:%s'%(id),origin=self.GetOrigin())
    def NotifySetNode(self,id,idNew,resp):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%s;idNew:%s;resp:%s'%(id,idNew,resp),origin=self.GetOrigin())
        try:
            sID=self.ConvertIdStr2Str(id)
            sNewID=self.ConvertIdStr2Str(idNew)
            if resp=='ok':
                if long(sID)!=long(sNewID):
                    if self.SINGLE_LOCK!=0:
                        self.idLock=sNewID
                    else:
                        self.lock.Del(sID)
                        self.lock.Add(sNewID)
            #if self.IsBlockNotify()==False:
            if self.oSF.IsBlockNotify==False:
                wx.PostEvent(self,vtnxXmlSetNode(self,sID,sNewID,resp))
            if self.thdSynch.IsRunning():
                self.thdSynch.__procSetNodeResponse__(id,idNew,resp)
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'notify set node id:%s idNew:%s resp:%s'%(id,idNew,resp),origin=self.GetOrigin())
    def NotifyLockNode(self,id,content):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%s'%(id),origin=self.GetOrigin())
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,content,origin=self.GetOrigin())
        try:
            sID=self.ConvertIdStr2Str(id)
            strs=content.split(',')
            if strs[0] in ['ok','already locked']:
                if self.SINGLE_LOCK!=0:
                    self.idLock=sID
                else:
                    self.lock.Add(sID)
                #if self.IsBlockNotify()==False:
                #if self.oSF.IsBlockNotify==False:
                #    wx.PostEvent(self,vtnxXmlLock(self,sID,content))
                wx.PostEvent(self,vtnxXmlLock(self,sID,content))
            elif strs[0]=='lock block':
                i=content.find(',')
                try:
                    zTime=int(content[i+1:])
                except:
                    zTime=-2
                #if self.IsBlockNotify()==False:
                if self.oSF.IsBlockNotify==False:
                    node=self.getNodeById(sID)
                    sTag=self.getTagName(node)
                    vtLog.PrintMsg('id:%s tag:%s'%(sID,sTag)+' ' + _('lock blocked for')+' '+str(zTime)+'[s]',self.widLogging)
                    wx.PostEvent(self,vtnxXmlLockBlock(self,sID,zTime))
                    wx.PostEvent(self,vtnxXmlLock(self,sID,'block'))
            elif strs[0]=='rejected':
                #if self.IsBlockNotify()==False:
                if 1:
                    node=self.getNodeById(sID)
                    sTag=self.getTagName(node)
                    if len(strs)>3:
                        sMsg=_(u'lock rejected locked by %s at %s')%(strs[2],strs[4])
                    else:
                        sMsg=' '.join(strs)
                    vtLog.PrintMsg('id:%s tag:%s'%(sID,sTag)+' ' + ' '.join(strs),self.widLogging)
                    wx.PostEvent(self,vtnxXmlLock(self,sID,content))
                #else:
                #    wx.PostEvent(self,vtnxXmlLock(self,sID,content))
            else:
                #if self.IsBlockNotify()==False:
                    node=self.getNodeById(sID)
                    sTag=self.getTagName(node)
                    vtLog.PrintMsg('id:%s tag:%s'%(sID,sTag)+' ' + ' '.join(strs),self.widLogging)
                    wx.PostEvent(self,vtnxXmlLock(self,sID,content))
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'notify lock node id:%s content:%s'%(id,content),origin=self.GetOrigin())
    def NotifyUnLockNode(self,id,content):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%s'%(id),origin=self.GetOrigin())
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,content,origin=self.GetOrigin())
        try:
            sID=self.ConvertIdStr2Str(id)
            if content in ['ok','released','granted']:# wro 060805 content=='released':
                if self.SINGLE_LOCK!=0:
                    if sID==self.idLock:
                        self.idLock=''
                else:
                    self.lock.Del(sID)
            #if self.IsBlockNotify()==False:
            if self.oSF.IsBlockNotify==False:
                wx.PostEvent(self,vtnxXmlUnLock(self,sID,content))
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'notify unlock node id:%s content:%s'%(id,content),origin=self.GetOrigin())
    def NotifyLockRequest(self,id,content):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%s'%(id),origin=self.GetOrigin())
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,content,origin=self.GetOrigin())
        try:
            sID=self.ConvertIdStr2Str(id)
            #if self.IsBlockNotify()==False:
            if self.oSF.IsBlockNotify==False:
                sMsg=''
                strs=content.split(',')
                if strs[0]=='ok':
                    sMsg=_(u'lock request wait for %s [s]')%strs[1]
                    vtLog.PrintMsg(sMsg,self.widLogging)
                    return
                elif strs[0]=='request':
                    sMsg=_(u'lock request by %s from %s:%s')%(strs[1],strs[2],strs[3])
                elif strs[0]=='grant':
                    if len(strs)>3:
                        sMsg=_(u'grant lock request (%s) by %s from %s:%s')%(strs[4],strs[1],strs[2],strs[3])
                    else:
                        sMsg=_(u'grant lock request content:%s')%(content)
                    #wx.PostEvent(self,vtnxXmlLock(self,sID,'ok'))
                    self.NotifyLockNode(sID,'ok')
                elif strs[0]=='reject':
                    sMsg=_(u'lock request rejected (%s) by %s from %s:%s')%(strs[4],strs[1],strs[2],strs[3])
                else:
                    vtLog.vtLngCur(vtLog.ERROR,'unknown response;%s'%content,self.GetOrigin())
                    sMsg=content
                vtLog.PrintMsg(sMsg,self.widLogging)
                wx.PostEvent(self,vtnxXmlLockRequest(self,sID,sMsg,strs[0],content))
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'notify unlock node id:%s content:%s'%(id,content),origin=self.GetOrigin())
    def NotifyAddNode(self,idPar,id,content):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'idPar:%s id:%s'%(idPar,id),origin=self.GetOrigin())
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,content,origin=self.GetOrigin())
        try:
            sParID=self.ConvertIdStr2Str(idPar)
            sID=self.ConvertIdStr2Str(id)
            #if content=='ok':
            #    self.idLock=sID
            #if self.IsBlockNotify()==False:
            if self.oSF.IsBlockNotify==False:
                wx.PostEvent(self,vtnxXmlAddNode(self,sParID,sID,content))
                wx.PostEvent(self,vtnxXmlContentChanged(self))
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'notify add node idPar:%s id:%s content:%s'%(idPar,id,content),origin=self.GetOrigin())
    def NotifyAddNodeResponse(self,id,idNew,resp):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%s;idNew:%s;resp:%s'%(id,idNew,resp),origin=self.GetOrigin())
        try:
            sID=self.ConvertIdStr2Str(id)
            sIDNew=self.ConvertIdStr2Str(idNew)
            if resp=='ok':
            #    self.idLock=sID
                #if self.IsBlockNotify()==False:
                if self.oSF.IsBlockNotify==False:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'notify',origin=self.GetOrigin())
                    wx.PostEvent(self,vtnxXmlAddNodeResponse(self,sID,sIDNew,resp))
                else:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'notify blocked',origin=self.GetOrigin())
            else:
                # 061009 wro already done in vtNetXmlClt
                #child=self.getNodeById(id)
                #if child is not None:
                #    self.delNode(child)
                # 070818:wro post delete event
                wx.PostEvent(self,vtnxXmlDelNode(self,sID,'ok'))
                try:
                    sTrans={'acl':_(u'ACL'),'locked':_(u'locked'),'fault':_(u'fault')}[resp]
                except:
                    sTrans=_(u'unknown (%s)')%resp
                vtLog.PrintMsg(_(u'appl:%s add id:%s response %s')%(self.appl,sID,sTrans),self.widLogging)
            if self.thdSynch.IsRunning():
                self.thdSynch.__procAddNodeResponse__(sID,sIDNew,resp)
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,
                    'id:%s;idNew:%s;resp:%s'%(id,idNew,resp),origin=self.GetOrigin())
    def NotifyDelNode(self,id,resp=''):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%s;resp:%s'%(id,resp),origin=self.GetOrigin())
        try:
            if len(id)==0:
                return
            sID=self.ConvertIdStr2Str(id)
            if resp=='ok':
                #if self.IsBlockNotify()==False:
                if self.oSF.IsBlockNotify==False:
                    wx.PostEvent(self,vtnxXmlDelNode(self,sID,resp))
                    wx.PostEvent(self,vtnxXmlContentChanged(self))
            else:
                #self.doc.moveNode(parNode,node)
                try:
                    sTrans={'acl':_(u'ACL'),'locked':_(u'locked'),'fault':_(u'fault'),'err':_(u'fault')}[resp]
                except:
                    sTrans=_(u'unknown (%s)')%resp
                vtLog.PrintMsg(_(u'appl:%s del id:%s response %s')%(self.appl,sID,sTrans),self.widLogging)
            if self.thdSynch.IsRunning():
                self.thdSynch.__procDelNodeResponse__(sID,resp)
        except:
            vtLog.vtLngCur(vtLog.ERROR,'notify del node id:%s'%(id),origin=self.GetOrigin())
            vtLog.vtLngTB(self.GetOrigin())
    def NotifyRemoveNode(self,id):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%s'%(id),origin=self.GetOrigin())
        try:
            sID=self.ConvertIdStr2Str(id)
            #if self.IsBlockNotify()==False:
            if self.oSF.IsBlockNotify==False:
                wx.PostEvent(self,vtnxXmlRemoveNode(self,sID))
                wx.PostEvent(self,vtnxXmlContentChanged(self))
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'notify remove node id:%s'%(id),origin=self.GetOrigin())
    def NotifyMoveNode(self,idPar,id):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'idPar:%s id:%s'%(idPar,id),origin=self.GetOrigin())
        try:
            sParID=self.ConvertIdStr2Str(idPar)
            sID=self.ConvertIdStr2Str(id)
            #if self.IsBlockNotify()==False:
            if self.oSF.IsBlockNotify==False:
                wx.PostEvent(self,vtnxXmlMoveNode(self,sParID,sID))
                wx.PostEvent(self,vtnxXmlContentChanged(self))
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'notify move node idPar:%s id:%s'%(idPar,id),origin=self.GetOrigin())
    def NotifyMoveNodeResponse(self,idPar,id,resp=''):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%s;idPar:%s;resp:%s'%(id,idPar,resp),origin=self.GetOrigin())
        try:
            sID=self.ConvertIdStr2Str(id)
            sParID=self.ConvertIdStr2Str(idPar)
            if resp=='ok':
                #if self.IsBlockNotify()==False:
                if self.oSF.IsBlockNotify==False:
                    wx.PostEvent(self,vtnxXmlMoveNodeResponse(self,sParID,sID,resp))
                    wx.PostEvent(self,vtnxXmlContentChanged(self))
            else:
                nodePar=self.getNodeById(idPar)
                node=self.getNodeById(id)
                self.moveNode(nodePar,node)
                try:
                    sTrans={'acl':_(u'ACL'),'locked':_(u'locked'),'fault':_(u'fault'),'err':_(u'fault')}[resp]
                except:
                    sTrans=_(u'unknown (%s)')%resp
                vtLog.PrintMsg(_(u'appl:%s move id:%s response %s')%(self.appl,sID,sTrans),self.widLogging)
            if self.thdSynch.IsRunning():
                self.thdSynch.__procMoveNodeResponse__(sID,sParID,resp)
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'notify move node response idPar:%s id:%s'%(idPar,id),origin=self.GetOrigin())
    def GetLockID(self):
        if self.SINGLE_LOCK!=0:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'lockId:%s'%self.idLock,origin=self.GetOrigin())
            return self.idLock
        else:
            vtLog.vtLngCur(vtLog.FATAL,'',self.GetOrigin())
            pass
    def NotifyFault(self,origin,fault):
        vtLog.vtLngCur(vtLog.ERROR,'origin:%s;fault:%s'%(origin,fault),
                origin=self.GetOrigin())
    def Connect2SrvByCfg(self,recentNode,recentAliasNode,recentHostNode,force=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        self.nodeSetup=self.docSetup.getRoot()
        node=self.docSetup.getChildByLst(self.nodeSetup,self.lstCfg)
        self.dn=self.docSetup.getNodeText(node,'path')
            
        oldRecentAlias=self.sRecentAlias
        oldRecentConn=self.sRecentConn
        oldHost=self.host
        oldAlias=self.alias
        oldUsr=self.usr
            
        if recentNode is None:
                self.oSF.SetFlag(self.oSF.CONN_ATTEMPT,False)
                #self.bConnInfoSet=False
                self.oSF.SetFlag(self.oSF.CONNINFOSET,False)
                self.__genAutoFN__()
                self.cltSrv=None
                self.cltSock=None
                #self.Setup()
                return 0
        try:
                #self.SetFlag(self.CONN_ATTEMPT,True)
                #self.SetFlag(self.DATA_SETTLED,False)
                self.oSF.SetState(self.oSF.CONNECT)
                self.sRecentAlias=self.docSetup.getNodeText(recentNode,'name')
                self.sRecentConn=self.docSetup.getNodeText(recentNode,'name')
                    
                self.host=self.docSetup.getNodeText(recentHostNode,'host')
                self.port=int(self.docSetup.getNodeText(recentHostNode,'port'))
                self.alias=self.docSetup.getNodeText(recentHostNode,'alias')
                self.usr=self.docSetup.getNodeText(recentHostNode,'usr')
                self.passwd=self.docSetup.getNodeText(recentHostNode,'passwd')
                #self.bConnInfoSet=True
                self.oSF.SetFlag(self.oSF.CONNINFOSET,True)
                self.__calcApplAlias__()
                fn=self.__genAutoFN__(bStore=True)
                self.oSF.SetFlag(self.oSF.CONNINFOSET,False)
                bOpen=False
                bClose=False
                #bConn=False
                #if force==False:
                #    if self.GetStatus() in [self.STOPPED,self.DISCONNECTED]:
                #        bConn=self.__getAutoConnect__(recentAliasNode)
                #else:
                #    bConn=True
                #    bOpen=True
                bConn=True
                #self.bOnline=bConn
                self.oSF.SetFlag(self.oSF.ONLINE,bConn)
                if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'fn:%s'%(fn),origin=self.GetOrigin())
                if len(oldRecentAlias)>0:
                    if self.sRecentAlias!=oldRecentAlias:
                        bOpen=True
                        bClose=True
                        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCur(vtLog.DEBUG,'oldconn:%s conn:%s'%(oldRecentAlias,self.sRecentAlias),origin=self.GetOrigin())
                if len(oldRecentConn)>0:
                    if self.sRecentConn!=oldRecentConn:
                        bOpen=True
                        bClose=True
                        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCur(vtLog.DEBUG,'oldconn:%s conn:%s'%(oldRecentConn,self.sRecentConn),origin=self.GetOrigin())
                if self.host!=oldHost:
                    bOpen=True
                if self.alias!=oldAlias:
                    bOpen=True
                    #bClose=True
                if self.usr!=oldUsr:
                    bOpen=True
                
                if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'bOpen:%d bClose:%d'%(bOpen,bClose),origin=self.GetOrigin())
                    vtLog.vtLngCur(vtLog.DEBUG,'open',origin=self.GetOrigin())
                if self.oSF.IsOpened==False:#Flag(self.OPENED)==False:
                    bOpen=True
                if bOpen:
                    #if bConn:
                    #    self.Open(fn,silent=True)
                    #else:
                    #    self.Open(fn,silent=False)
                    #self.bDataSettled=False
                    #self.bLoginFirst=True
                    self.Open(fn)
                
                    if bConn:
                        # delay connection attept until file is completly opened
                        self.oSF.SetFlag(self.oSF.LOGIN_FIRST,True)
                        self.oSF.SetFlag(self.oSF.CONN_ACTIVE,True)
                else:
                    if bConn:
                        #self.bLoginFirst=True
                        self.oSF.SetFlag(self.oSF.LOGIN_FIRST,True)
                        self.oSF.SetFlag(self.oSF.CONN_ACTIVE,True)
                        self.Connect2Srv()
                #else:
                #    if self.IsFlag(self.OPENED)==False:
                #        self.bDataSettled=False
                #        self.Open(fn,silent=False)
                
                if 1==0:
                    if bClose:
                        for k in self.dictDoc.keys():
                            if k == self.appls[0]:
                                continue
                            self.dictDoc[k].ReConnect2Srv()
                            #if self.verbose:
                            #    vtLog.vtLngCur(vtLog.DEBUG,'close:%s'%(k))
                            #self.dictDoc[k].DisConnect()
                            #self.dictDoc[k].Close()
                            
                            #self.dictDoc[k].AutoConnect2Srv(force=True)
                    else:
                        for k in self.dictDoc.keys():
                            if k == self.appls[0]:
                                continue
                            self.dictDoc[k].ReConnect2Srv()
                    return 1
        except Exception,list:
                vtLog.vtLngTB(self.GetOrigin())
                self.sRecentAlias=''
                self.sRecentConn=''
                #self.Setup()
                return -1
    def PreConnect(self):
        self.oSF.SetState(self.oSF.PRECONNECT)
    def Connect2SrvByAlias(self,objAlias,dn,force=False,offline=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'force:%d;offline:%d'%(force,offline),origin=self.GetOrigin())
        self.__logStatus__()
        #self.dn=self.docSetup.getNodeText(node,'path')
        self.dn=dn
        
        oldRecentAlias=self.sRecentAlias
        oldRecentConn=self.sRecentConn
        oldHost=self.host
        oldAlias=self.alias
        oldUsr=self.usr
            
        if objAlias is None:
            self.oSF.SetFlag(self.oSF.CONN_ATTEMPT,False)
            self.oSF.SetFlag(self.oSF.STLD,False)
            #self.bConnInfoSet=False
            self.oSF.SetFlag(self.oSF.CONNINFOSET,False)
            fn=self.__genAutoFN__(bStore=True)
            # 070422:wro restart audit trail
            #vtLog.CallStack('')
            #print fn
            #print self.sFN
            #self.audit.stop()
            #self.Save()
            #self.audit.start(self.sFN,self.GetAppl())
            
            self.cltSrv=None
            self.cltSock=None
            #self.Setup()
            return 0
        try:
            if offline==False:
                self.oSF.SetFlag(self.oSF.CONN_ATTEMPT,True)
            #self.oSF.SetFlag(self.oSF.DATA_SETTLED,False)
            self.oSF.SetState(self.oSF.CONNECT)
            #self.ClearConsumer()   #071023: wro
            self.sRecentConn=objAlias.GetHostConnectionStr()
            self.sRecentAlias=objAlias.GetHostAliasStr()
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'%s;%s'%(objAlias.GetHostAliasStr(),
                            objAlias.GetStr()),origin=self.GetOrigin())
                vtLog.vtLngCur(vtLog.DEBUG,'oldconn:%s;newconn:%s'%(
                            oldRecentConn,self.sRecentConn),origin=self.GetOrigin())
            self.host=objAlias.GetHost()
            self.port=objAlias.GetPort()
            self.alias=objAlias.GetAlias()
            self.usr=objAlias.GetUsr()
            self.passwd=objAlias.GetPasswd()
            #self.bConnInfoSet=True
            self.oSF.SetFlag(self.oSF.CONNINFOSET,True)
            self.__calcApplAlias__()
            #fn=self.__genAutoFN__()
            sOld=self.GetFN()
            fn=self.GenAutoFNConnAppl(objAlias,self.dn)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'oldFN:%s;newFN:%s'%(sOld,fn),self.GetOrigin())
            self.oSF.SetFlag(self.oSF.CONNINFOSET,False)
            # 070422:wro restart audit trail
            #vtLog.CallStack('')
            #print fn
            #print self.sFN
            #self.audit.stop()
            #self.Save()
            #self.audit.start(self.sFN,self.GetAppl())
            
            bOpen=False
            bClose=False
            #bConn=not objAlias.IsOffline() # 070611:wro
            #if offline:                    # 070611:wro
            #    bConn=False                # 070611:wro
            bConn=not offline               # 070611:wro
            if self.verbose>0:
                vtLog.CallStack('offline:%d connect:%d'%(offline,bConn))
            #self.bOnline=bConn
            self.oSF.SetFlag(self.oSF.ONLINE,bConn)
            if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'fn:%s'%(fn),origin=self.GetOrigin())
            #if len(oldRecentAlias)>0:
            #    if self.sRecentAlias!=oldRecentAlias:
            #        bOpen=True
            #        bClose=True
            #        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            #            vtLog.vtLngCur(vtLog.DEBUG,'oldconn:%s conn:%s'%(oldRecentAlias,self.sRecentAlias),origin=self.GetOrigin())
            if len(oldRecentConn)>0:
                if self.sRecentConn!=oldRecentConn:
                    bOpen=True
                    bClose=True
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'oldconn:%s;newconn:%s'%(oldRecentConn,self.sRecentConn),origin=self.GetOrigin())
            if self.host!=oldHost:
                bOpen=True
            if self.alias!=oldAlias:
                bOpen=True
                #bClose=True
            if self.usr!=oldUsr:
                bOpen=True
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'bOpen:%d bClose:%d'%(bOpen,bClose),origin=self.GetOrigin())
                #vtLog.vtLngCur(vtLog.DEBUG,'open',origin=self.GetOrigin())
            #if self.IsFlag(self.OPENED)==False:
            #    bOpen=True
            if self.oSF.IsOpened==False:
                bOpen=True
            if bOpen:
                #self.bDataSettled=False
                #self.bLoginFirst=True
                #self.oSF.SetState(self.oSF.OPEN
                #self.Open(fn)     # 070623:wro set flags first
                wx.CallAfter(self.Open,fn)
                if bConn:
                    # delay connection attept until file is completly opened
                    self.oSF.SetFlag(self.oSF.LOGIN_FIRST,True)
                    self.oSF.SetFlag(self.oSF.CONN_ACTIVE,True)
                else:
                    self.oSF.SetFlag(self.oSF.CONN_ACTIVE,False)
                #self.Open(fn)      # 070623:wro set flags first
            else:
                if bConn:
                    #self.bLoginFirst=True
                    self.oSF.SetFlag(self.oSF.LOGIN_FIRST,True)
                    self.oSF.SetFlag(self.CONN_ACTIVE,True)
                    #self.Connect2Srv()     # 070623:wro set flags first
                    wx.CallAfter(self.Connect2Srv)
                #else:
                #    self.oSF.SetFlag(self.oSF.CONN_ACTIVE,False)
            self.__logStatus__()
            if 1==0:
                if bClose:
                    for k in self.dictDoc.keys():
                        if k == self.appls[0]:
                            continue
                        self.dictDoc[k].ReConnect2Srv()
                        
                else:
                    for k in self.dictDoc.keys():
                        if k == self.appls[0]:
                            continue
                        self.dictDoc[k].ReConnect2Srv()
                return 1
        except Exception,list:
            vtLog.vtLngTB(self.GetOrigin())
            self.sRecentAlias=''
            self.sRecentConn=''
            return -1
            
