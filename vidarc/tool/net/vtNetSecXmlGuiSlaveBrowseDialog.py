#Boa:Dialog:vtNetSecXmlGuiSlaveBrowseDialog
#----------------------------------------------------------------------------
# Name:         vtNetSecXmlGuiSlaveBrowseDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060705
# CVS-ID:       $Id: vtNetSecXmlGuiSlaveBrowseDialog.py,v 1.6 2007/07/29 09:56:50 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.net.vtNetSecXmlGuiMasterBrowsePanel
import wx.lib.buttons
import vidarc.tool.net.vtNetSecXmlGuiSlaveBrowsePanel

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

import vidarc.tool.net.img_gui as img_gui
import vidarc.tool.net.images as img_img

def create(parent):
    return vtNetSecXmlGuiSlaveBrowseDialog(parent)

[wxID_VTNETSECXMLGUISLAVEBROWSEDIALOG, 
 wxID_VTNETSECXMLGUISLAVEBROWSEDIALOGPNBROWSE, 
] = [wx.NewId() for _init_ctrls in range(2)]

class vtNetSecXmlGuiSlaveBrowseDialog(wx.Dialog):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.pnBrowse, 0, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT | wx.TOP)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=1, vgap=0)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTNETSECXMLGUISLAVEBROWSEDIALOG,
              name=u'vtNetSecXmlGuiSlaveBrowseDialog', parent=prnt,
              pos=wx.Point(301, 265), size=wx.Size(400, 275),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE,
              title=u'vtNetSecXml Slave Browse Dialog')
        self.SetClientSize(wx.Size(392, 248))

        self.pnBrowse = vidarc.tool.net.vtNetSecXmlGuiSlaveBrowsePanel.vtNetSecXmlGuiSlaveBrowsePanel(id=wxID_VTNETSECXMLGUISLAVEBROWSEDIALOGPNBROWSE,
              name=u'pnBrowse', parent=self, pos=wx.Point(4, 4),
              size=wx.Size(384, 244), style=0)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        self._init_ctrls(parent)
        self.fgsData.Fit(self)
        self.fgsData.Layout()
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        pass
    def SetNetSlave(self,netSlave):
        self.pnBrowse.SetNetSlave(netSlave)
    def UpdateCfg(self,dn,lApplConn):
        self.pnBrowse.UpdateCfg(dn,lApplConn)
    def SetAppl(self,appl):
        self.pnBrowse.SetAppl(appl)
    def UpdateConnections(self,conns):
        self.pnBrowse.UpdateConnections(conns)
