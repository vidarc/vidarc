#----------------------------------------------------------------------------
# Name:         vNetFileWxGui.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060627
# CVS-ID:       $Id: vtNetFileWxGui.py,v 1.4 2007/08/06 22:54:53 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

import vidarc.tool.log.vtLog as vtLog
import string,os.path,sys,thread,threading,time,traceback
import vidarc.tool.net.img_gui as imgNetGui
from vidarc.tool.net.vtNetFileClt import vtNetFileCltSock
from vidarc.tool.net.vtNetClt import vtNetClt

vtnxEVT_NET_FILE_GET_START=wx.NewEventType()
def EVT_NET_FILE_GET_START(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_FILE_GET_START,func)
def EVT_NET_FILE_GET_START_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_FILE_GET_START)
class vtnxFileGetStart(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_FILE_GET_START(<widget_name>, xxx)
    """
    def __init__(self,obj,fn):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtnxEVT_NET_FILE_GET_START)
        self.fn=fn
    def GetFN(self):
        return self.fn

vtnxEVT_NET_FILE_GET_PROC=wx.NewEventType()
def EVT_NET_FILE_GET_PROC(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_FILE_GET_PROC,func)
def EVT_NET_FILE_GET_PROC_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_FILE_GET_PROC)
class vtnxFileGetProc(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_FILE_GET_PROC(<widget_name>, xxx)
    """
    def __init__(self,obj,fn,iAct,iSize):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtnxEVT_NET_FILE_GET_PROC)
        self.fn=fn
        self.iAct=iAct
        self.iSize=iSize
    def GetAct(self):
        return self.iAct
    def GetSize(self):
        return self.iSize
    def GetFN(self):
        return self.fn

vtnxEVT_NET_FILE_GET_FIN=wx.NewEventType()
def EVT_NET_FILE_GET_FIN(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_FILE_GET_FIN,func)
def EVT_NET_FILE_GET_FIN_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_FILE_GET_FIN)
class vtnxFileGetFin(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_FILE_GET_FIN(<widget_name>, xxx)
    """
    def __init__(self,obj,fn):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtnxEVT_NET_FILE_GET_FIN)
        self.fn=fn
    def GetFN(self):
        return self.fn

vtnxEVT_NET_FILE_GET_ABORT=wx.NewEventType()
def EVT_NET_FILE_GET_ABORT(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_FILE_GET_ABORT,func)
def EVT_NET_FILE_GET_ABORT_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_FILE_GET_ABORT)
class vtnxFileGetAbort(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_FILE_GET_ABORT(<widget_name>, xxx)
    """
    def __init__(self,obj,fn):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtnxEVT_NET_FILE_GET_ABORT)
        self.fn=fn
    def GetFN(self):
        return self.fn

vtnxEVT_NET_FILE_CLOSED=wx.NewEventType()
def EVT_NET_FILE_CLOSED(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_FILE_CLOSED,func)
def EVT_NET_FILE_CLOSED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_FILE_CLOSED)
class vtnxFileClosed(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_FILE_CLOSED(<widget_name>, xxx)
    """
    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtnxEVT_NET_FILE_CLOSED)

vtnxEVT_NET_FILE_LOGIN=wx.NewEventType()
def EVT_NET_FILE_LOGIN(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_FILE_LOGIN,func)
def EVT_NET_FILE_LOGIN_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_FILE_LOGIN)
class vtnxFileLogin(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_FILE_LOGIN(<widget_name>, xxx)
    """
    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtnxEVT_NET_FILE_LOGIN)

vtnxEVT_NET_FILE_LOGGEDIN=wx.NewEventType()
def EVT_NET_FILE_LOGGEDIN(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_FILE_LOGGEDIN,func)
def EVT_NET_FILE_LOGGEDIN_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_FILE_LOGGEDIN)
class vtnxFileLoggedin(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_FILE_LOGGEDIN(<widget_name>, xxx)
    """
    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtnxEVT_NET_FILE_LOGGEDIN)

vtnxEVT_NET_FILE_BROWSE=wx.NewEventType()
def EVT_NET_FILE_BROWSE(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_FILE_BROWSE,func)
def EVT_NET_FILE_BROWSE_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_FILE_BROWSE)
class vtnxFileBrowse(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_FILE_BROWSE(<widget_name>, xxx)
    """
    def __init__(self,obj,l):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.l=l
        self.SetEventType(vtnxEVT_NET_FILE_BROWSE)
    def GetList(self):
        return self.l

vtnxEVT_NET_FILE_LIST_CONTENT=wx.NewEventType()
def EVT_NET_FILE_LIST_CONTENT(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_FILE_LIST_CONTENT,func)
def EVT_NET_FILE_LIST_CONTENT_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_FILE_LIST_CONTENT)
class vtnxFileListContent(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_FILE_LIST_CONTENT(<widget_name>, xxx)
    """
    def __init__(self,obj,applAlias,l):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.applAlias=applAlias
        self.l=l
        self.SetEventType(vtnxEVT_NET_FILE_LIST_CONTENT)
    def GetApplAlias(self):
        return self.applAlias
    def GetList(self):
        return self.l

vtnxEVT_NET_FILE_CONNECTED=wx.NewEventType()
def EVT_NET_FILE_CONNECTED(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_FILE_CONNECTED,func)
def EVT_NET_FILE_CONNECTED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_FILE_CONNECTED)
class vtnxFileConnected(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_FILE_CONNECTED(<widget_name>, xxx)
    """
    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtnxEVT_NET_FILE_CONNECTED)

vtnxEVT_NET_FILE_CONNECT_FAULT=wx.NewEventType()
def EVT_NET_FILE_CONNECT_FAULT(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_FILE_CONNECT_FAULT,func)
def EVT_NET_FILE_CONNECT_FAULT_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_FILE_CONNECT_FAULT)
class vtnxFileConnectFault(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_FILE_CONNECT_FAULT(<widget_name>, xxx)
    """
    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtnxEVT_NET_FILE_CONNECT_FAULT)

vtnxEVT_NET_FILE_DISCONNECT=wx.NewEventType()
def EVT_NET_FILE_DISCONNECT(win,func):
    win.Connect(-1,-1,vtnxEVT_NET_FILE_DISCONNECT,func)
def EVT_NET_FILE_DISCONNECT_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtnxEVT_NET_FILE_DISCONNECT)
class vtnxFileDisConnect(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_NET_FILE_DISCONNECT(<widget_name>, xxx)
    """
    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtnxEVT_NET_FILE_DISCONNECT)

class thdStop:
    def __init__(self,doc=None,verbose=0):
        self.verbose=verbose
        self.doc=doc
        self.running=False
    def DoStop(self,func,*args,**kwargs):
        self.func=func
        self.args=args
        self.kwargs=kwargs
        self.keepGoing = self.running = True
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'',origin='appl:%s'%(self.doc.appl))
        thread.start_new_thread(self.Run, ())

    def Stop(self):
        self.keepGoing = False

    def IsRunning(self):
        return self.running
    
    def Run(self):
        try:
            self.doc.__stop__()
            while self.doc.IsRunning():
                time.sleep(1)
                #self.doc.__stop__()
            if self.func is not None:
                self.func(*self.args,**self.kwargs)
        except:
            vtLog.vtLngTB(self.doc.appl+'_stop')
        self.keepGoing = self.running = False


class vtNetFileWxGui(wx.StaticBitmap):
    DISCONNECTED    =0x00000000
    CONNECT         =0x00000001
    CONNECTED       =0x00000002
    CONNECT_FLT     =0x00000003
    OPEN_START      =0x00000004
    OPEN_OK         =0x00000005
    OPEN_FLT        =0x00000006
    SYNCH_START     =0x00000007
    SYNCH_PROC      =0x00000008
    SYNCH_FIN       =0x00000009
    SYNCH_ABORT     =0x0000000A
    LOGIN           =0x0000000B
    LOGGEDIN        =0x0000000C
    STOPPED         =0x0000000D
    SERVED          =0x0000000E
    SERVE_FLT       =0x0000000F
    CONFIG          =0x00000010
    DATA_ACCESS     =0x00000100
    BLOCK_NOTIFY    =0x00000200
    OPENED          =0x00000400
    CFG_VALID       =0x00000800
    SYNCHABLE       =0x00001000
    CONN_ACTIVE     =0x00002000
    CONN_PAUSE      =0x00004000
    SHUTDOWN        =0x00008000
    CONN_ATTEMPT    =0x00020000
    STATE_MASK      =0x000000FF
    MASK            =0x00FFFFFF
    def __init__(self,parent,appl,pos=(0,0),size=(16,16),synch=False,small=True,verbose=0):
        if small:
            img=wx.EmptyBitmap(16, 16)
            size=(16,16)
            self.bSmall=True
        else:
            img=wx.EmptyBitmap(32, 32)
            size=(32,32)
            self.bSmall=False
        wx.StaticBitmap.__init__(self,parent=parent,pos=pos,size=size,
                bitmap=img)
        self.iStatus=self.DISCONNECTED
        self.iStatusPrev=self.DISCONNECTED
        self.netMaster=None
        
        self.thdStop=thdStop(self,verbose-1)
        
        self.semNotify=threading.Lock()#threading.Semaphore()
        self.args=None
        self.kwargs=None
        self.lastStatus=-1

        self.appl=appl
        if len(appl)>0:
            self.bFixed=True
        else:
            self.bFixed=False
        self.sRecentAlias=''
        self.sRecentConn=''
        self.alias=''
        self.host=''
        self.port=-1
        self.usr=''
        self.passwd=''
        self.sUsr=''
        self.iSecLv=-1
        self.bAdmin=False
        self.bConnInfoSet=False
        self.verbose=verbose
        self.cltSrv=None
        self.cltSock=None
        img=imgNetGui.getStoppedBitmap()
        self.SetBitmap(img)
        self.oLogin=None
        #EVT_RIGHT_DOWN(self, self.OnRightDown)
    def SetNetMaster(self,netMaster):
        self.netMaster=netMaster
        
    def __setBitMap__(self,img):
        if self.IsFlag(self.SHUTDOWN):
            return
        wx.StaticBitmap.SetBitmap(self,img)
        self.Refresh()
    def __setBitmapByStatus__(self,iStatusPrev=None,iStatus=None):
        if self.IsStatusChanged(iStatusPrev,iStatus)==False:
            return
        if self.GetStatus(iStatus) == self.CONNECT:
            self.SetFlag(self.CONN_ACTIVE,True)
            self.SetFlag(self.CFG_VALID,False)
            self.__setBitMap__(imgNetGui.getConnectBitmap())
        elif self.GetStatus(iStatus) == self.CONNECTED:
            self.__setBitMap__(imgNetGui.getConnectBitmap())
        elif self.GetStatus(iStatus) == self.CONNECT_FLT:
            self.SetFlag(self.CONN_ACTIVE,False)
            self.__setBitMap__(imgNetGui.getFaultBitmap())
        elif self.GetStatus(iStatus) == self.LOGIN:
            self.__setBitMap__(imgNetGui.getLoginBitmap())
        elif self.GetStatus(iStatus) == self.LOGGEDIN:
            self.__setBitMap__(imgNetGui.getLoggedinBitmap())
        elif self.GetStatus(iStatus) == self.SERVED:
            self.__setBitMap__(imgNetGui.getConnectBitmap())  # FIXME
        elif self.GetStatus(iStatus) == self.SERVE_FLT:
            self.SetFlag(self.CONN_ACTIVE,False)
            self.__setBitMap__(imgNetGui.getFaultBitmap())  # FIXME
        elif self.GetStatus(iStatus) == self.STOPPED:
            self.SetFlag(self.CONN_ACTIVE,False)
            self.__setBitMap__(imgNetGui.getStoppedBitmap())
        elif self.GetStatus(iStatus) == self.DISCONNECTED:
            self.SetFlag(self.CONN_ACTIVE,False)
            self.__setBitMap__(imgNetGui.getStoppedBitmap())
        else:
            self.__setBitMap__(imgNetGui.getStoppedBitmap())
    def __getBitmapByStatus__(self,iStatus=None):
        if self.GetStatus(iStatus) == self.CONNECT:
            self.SetFlag(self.CONN_ACTIVE,True)
            self.SetFlag(self.CFG_VALID,False)
            return imgNetGui.getConnectBitmap()
        elif self.GetStatus(iStatus) == self.CONNECTED:
            return imgNetGui.getConnectBitmap()
        elif self.GetStatus(iStatus) == self.CONNECT_FLT:
            self.SetFlag(self.CONN_ACTIVE,False)
            return imgNetGui.getFaultBitmap()
        elif self.GetStatus(iStatus) == self.LOGIN:
            return imgNetGui.getLoginBitmap()
        elif self.GetStatus(iStatus) == self.LOGGEDIN:
            return imgNetGui.getLoggedinBitmap()
        elif self.GetStatus(iStatus) == self.SERVED:
            return imgNetGui.getConnectBitmap()  # FIXME
        elif self.GetStatus(iStatus) == self.SERVE_FLT:
            self.SetFlag(self.CONN_ACTIVE,False)
            return imgNetGui.getServeFltBitmap()  # FIXME
        elif self.GetStatus(iStatus) == self.STOPPED:
            self.SetFlag(self.CONN_ACTIVE,False)
            return imgNetGui.getStoppedBitmap()
        elif self.GetStatus(iStatus) == self.DISCONNECTED:
            self.SetFlag(self.CONN_ACTIVE,False)
            return imgNetGui.getStoppedBitmap()
        else:
            return imgNetGui.getStoppedBitmap()
    def __getMedBitmapByStatus__(self,iStatus=None):
        if self.GetStatus(iStatus) == self.CONNECT:
            self.SetFlag(self.CONN_ACTIVE,True)
            self.SetFlag(self.CFG_VALID,False)
            return imgNetGui.getConnectMedBitmap()
        elif self.GetStatus(iStatus) == self.CONNECTED:
            return imgNetGui.getConnectMedBitmap()
        elif self.GetStatus(iStatus) == self.CONNECT_FLT:
            self.SetFlag(self.CONN_ACTIVE,False)
            return imgNetGui.getFaultMedBitmap()
        elif self.GetStatus(iStatus) == self.LOGIN:
            return imgNetGui.getLoginMedBitmap()
        elif self.GetStatus(iStatus) == self.LOGGEDIN:
            return imgNetGui.getLoggedinMedBitmap()
        elif self.GetStatus(iStatus) == self.SERVED:
            return imgNetGui.getConnectMedBitmap()  # FIXME
        elif self.GetStatus(iStatus) == self.SERVE_FLT:
            self.SetFlag(self.CONN_ACTIVE,False)
            return imgNetGui.getServeFltMedBitmap()  # FIXME
        elif self.GetStatus(iStatus) == self.STOPPED:
            self.SetFlag(self.CONN_ACTIVE,False)
            return imgNetGui.getStoppedMedBitmap()
        elif self.GetStatus(iStatus) == self.DISCONNECTED:
            self.SetFlag(self.CONN_ACTIVE,False)
            return imgNetGui.getStoppedMedBitmap()
        else:
            return imgNetGui.getStoppedMedBitmap()
    def GetListImgDict(self,imgLstTyp):
        d={}
        d[self.STOPPED]         =imgLstTyp.Add(imgNetGui.getStoppedBitmap())
        d[self.CONNECT]         =imgLstTyp.Add(imgNetGui.getConnectBitmap())
        d[self.CONNECTED]       =imgLstTyp.Add(imgNetGui.getConnectBitmap())
        d[self.LOGIN]           =imgLstTyp.Add(imgNetGui.getLoginBitmap())
        d[self.LOGGEDIN]        =imgLstTyp.Add(imgNetGui.getLoggedinBitmap())
        d[self.SERVED]          =imgLstTyp.Add(imgNetGui.getConnectBitmap())
        d[self.SERVE_FLT]       =imgLstTyp.Add(imgNetGui.getFaultBitmap())
        d[self.DISCONNECTED]    =imgLstTyp.Add(imgNetGui.getStoppedBitmap())
        return d
    def GetStatusStr(self,iStatus=None):
        if self.GetStatus(iStatus) == self.CONNECT:
            return 'connect'
        elif self.GetStatus(iStatus) == self.CONNECTED:
            return 'connected'
        elif self.GetStatus(iStatus) == self.CONNECT_FLT:
            return 'connect fault'
        elif self.GetStatus(iStatus) == self.LOGIN:
            return 'login'
        elif self.GetStatus(iStatus) == self.LOGGEDIN:
            return 'loggedin'
        elif self.GetStatus(iStatus) == self.SERVED:
            return 'served'
        elif self.GetStatus(iStatus) == self.SERVE_FLT:
            return 'serve fault'
        elif self.GetStatus(iStatus) == self.STOPPED:
            return 'stopped'
        elif self.GetStatus(iStatus) == self.DISCONNECTED:
            return 'disconnected'
        else:
            return 'undefined'
    def __setStatus__(self,status,*args,**kwargs):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'status:0x%08x args:%s kwargs:%s'%(status,args,kwargs),origin=self.appl)
        if self.IsFlag(self.SHUTDOWN):
            return
        self.iStatusPrev=self.iStatus
        self.iStatus&=self.MASK^self.STATE_MASK
        self.iStatus|=status&self.STATE_MASK
        if self.IsStatusChanged(self.iStatusPrev,self.iStatus):
            if self.GetStatus(self.iStatus) == self.CONNECT:
                self.SetFlag(self.CONN_ACTIVE,True)
                self.SetFlag(self.CFG_VALID,False)
            elif self.GetStatus(self.iStatus) == self.CONNECTED:
                pass
            elif self.GetStatus(self.iStatus) == self.CONNECT_FLT:
                self.SetFlag(self.CONN_ACTIVE,False)
            elif self.GetStatus(self.iStatus) == self.LOGIN:
                pass
            elif self.GetStatus(self.iStatus) == self.LOGGEDIN:
                pass
            elif self.GetStatus(self.iStatus) == self.SERVED:
                pass
            elif self.GetStatus(self.iStatus) == self.SERVE_FLT:
                self.SetFlag(self.CONN_ACTIVE,False)
            elif self.GetStatus(self.iStatus) == self.STOPPED:
                self.SetFlag(self.CONN_ACTIVE,False)
                if self.IsFlag(self.OPENED):
                    self.__setStatus__(self.OPEN_OK)
            elif self.GetStatus(self.iStatus) == self.DISCONNECTED:
                self.SetFlag(self.CONN_ACTIVE,False)
                if self.IsFlag(self.OPENED):
                    self.__setStatus__(self.OPEN_OK)
        if self.netMaster is not None:
            self.netMaster.Notify(self.appl,self.alias,self,self.iStatusPrev,self.iStatus,*args,**kwargs)
    def IsStatusChanged(self,iStatusPrev=None,iStatus=None):
        if iStatusPrev is None:
            iStatusPrev=self.iStatusPrev
        if iStatus is None:
            iStatus=self.iStatus
        if (iStatus & self.STATE_MASK) != (iStatusPrev & self.STATE_MASK):
            return True
        return False
    def SetDataAccess(self,flag):
        if flag:
            self.iStatusPrev=self.iStatus
            self.iStatus|=self.DATA_ACCESS
        else:
            self.iStatusPrev=self.iStatus
            self.iStatus&=self.MASK^self.DATA_ACCESS
    def IsDataAccessed(self):
        if self.iStatus & self.DATA_ACCESS:
            return True
        else:
            return False
    def SetFlag(self,int_flag,flag):
        #vtLog.vtLngCur(vtLog.DEBUG,'0x%04x %d'%(int_flag,flag),verbose=1)
        if flag:
            #self.iStatusPrev=self.iStatus
            self.iStatus|=int_flag
        else:
            #self.iStatusPrev=self.iStatus
            self.iStatus&=self.MASK^int_flag
    def IsFlag(self,int_flag):
        if self.iStatus & int_flag:
            return True
        else:
            return False
    def IsModified(self):
        return False
    def IsRunning(self):
        return False
    def IsSettled(self):
        return True
    def IsActive(self):
        if self.IsFlag(self.CONN_ACTIVE)==False:
            return False
        if self.GetStatus() in [self.DISCONNECTED,self.STOPPED,self.SERVE_FLT]:
            return False
        else:
            return True
    def SetBlockNotify(self,flag):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'status:0x%04x 0x%04x flag:%d'%(self.iStatus,self.iStatusPrev,flag),origin=self.appl)
        if flag:
            #self.iStatusPrev=self.iStatus
            self.iStatus|=self.BLOCK_NOTIFY
        else:
            iStatusPrev=self.iStatus
            self.iStatus&=self.MASK^self.BLOCK_NOTIFY
            if iStatusPrev & self.BLOCK_NOTIFY:
                #wx.PostEvent(self,vtnxXmlGotContent(self))
                #wx.PostEvent(self,vtnxXmlBuildTree(self))
                #wx.PostEvent(self,vtnxXmlContentChanged(self))
                pass
    def IsBlockNotify(self):
        if self.iStatus & self.BLOCK_NOTIFY:
            return True
        else:
            return False
    def SetAppl(self,appl,fixed=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.appl)
        self.appl=appl
        self.bFixed=fixed
    def Stop(self,func=None,*args,**kwargs):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.appl)
        self.thdStop.DoStop(func,*args,**kwargs)
    def __stop__(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.appl)
    def __isRunning__(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.appl)
        return False
    def Pause(self,flag):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.appl)
        if self.cltSrv is not None:
            self.cltSrv.Pause(flag)
        else:
            self.SetFlag(self.CONN_PAUSE,flag)
    def NotifyPause(self,flag):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.appl)
        self.SetFlag(self.CONN_PAUSE,flag)
    def WaitNotRunning(self,timeout=-1,interval=0.1):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.appl)
        iTime=0
        while self.IsRunning():
            time.sleep(interval)
            iTime+=interval
            if timeout>0:
                if iTime>timeout:
                    return False   # timeout
        return True
    def GetAlias(self):
        return self.alias
    def GetHost(self):
        return self.host
    def GetPort(self):
        return self.port
    def GetUsr(self):
        return self.usr
    def DisConnect(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'DisConnect',origin=self.appl)
        #vtLog.vtLngCallStack(None,vtLog.DEBUG,'appl:%s'%(self.appl),1)
        #self.endEdit(None)
        #self.host=''
        self.port=-1
        #self.alias=''
        #self.usr=''
        #self.passwd=''
        self.sUsr=''
        self.iSecLv=-1
        self.bAdmin=False
        self.SetFlag(self.CFG_VALID,False)
                
        self.Stop(self.OnThreadStoppedDisconnect)
    def OnThreadStoppedDisconnect(self):
        if self.IsFlag(self.OPENED):
            self.__setStatus__(self.OPEN_OK)
        else:
            self.__setStatus__(self.STOPPED)
        self.SetFlag(self.CONN_ACTIVE,False)
        self.sUsr=''
        self.iSecLv=-1
        self.bAdmin=False
        self.bDoAutoConnectAfterDisConn=False
        if self.cltSrv is not None:
            #self.cltSrv.Stop()
            if self.cltSock is not None:
                self.cltSock.Close()
        wx.PostEvent(self,vtnxFileDisConnect(self))
    def IsConnActive(self):
        if self.cltSrv is not None:
            return self.cltSrv.IsServing()
        else:
            return False
    def ConnPause(self,flag):
        if self.cltSrv is not None:
            self.cltSrv.Pause(flag)
    def __connectionClosed__(self,established=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.appl)
        self.SetFlag(self.CONN_ACTIVE,False)
        self.SetFlag(self.CFG_VALID,False)
        self.__setStatus__(self.STOPPED)
        self.port=-1
        self.sUsr=''
        self.iSecLv=-1
        self.bAdmin=False
        self.bDataSettled=True
        self.cltSrv=None
        self.cltSock=None
        if self.IsBlockNotify()==False:
            wx.PostEvent(self,vtnxFileClosed(self))
    def NotifyConnected(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.appl)
        try:
            self.cltSock=self.cltSrv.GetSocketInstance()
            if self.cltSock is None:
                self.__connectionClosed__(False)
                return
            #self.cltSock.SetNodes2Keep(self.nodes2keep)
            self.cltSock.SetParent(self)
            self.cltSock.LoginEncoded(self.usr,self.passwd)
            self.cltSock.Alias(self.appl,self.alias)
            #if self.verbose:
            #    vtLog.vtLngCur(vtLog.DEBUG,'alias set')
            self.__setStatus__(self.CONNECTED)
            self.GetAutoFN()
            if self.IsBlockNotify()==False:
                wx.PostEvent(self,vtnxFileConnected(self))
        except Exception,list:
            if vtLog.vtLngIsLogged(vtLog.ERROR):
                vtLog.vtLngCur(vtLog.ERROR,'exception:%s'%(repr(list)),self.appl)
            vtLog.vtLngTB(self.appl)
            self.NotifyConnectionFault()
            
    def NotifyConnectionFault(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.appl)
        self.__connectionClosed__(False)
        #self.Open()
        if self.cltSrv is not None:
            self.cltSrv.Stop()
        del self.cltSrv
        self.cltSrv=None
        self.cltSock=None
        self.sUsr=''
        self.iSecLv=-1
        self.bAdmin=False
        self.bDataSettled=True
        self.__setStatus__(self.CONNECT_FLT)
        if self.IsBlockNotify()==False:
            wx.PostEvent(self,vtnxFileConnectFault(self))
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'')
    def NotifyLogin(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.appl)
        self.__setStatus__(self.LOGIN)
    def Login(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.appl)
        dlg=None
        #self.netMaster.AcquireLogin()
        dlg=self.netMaster.GetLoginDlg()
        if dlg.IsShown():
            return
        dlg.Centre()
        dlg.SetValues(self.host,self.port,self.alias,self.usr)
        if dlg.ShowModal()>0:
            self.usr=dlg.GetUsr()
            self.passwd=dlg.GetPasswd()
            self.netMaster.UpdateLogin(self.appl,self.usr,self.passwd)
            pass
        else:
            return
        #self.netMaster.ReleaseLogin()
        if self.cltSock is not None:
            self.cltSock.LoginEncoded(self.usr,self.passwd)
            self.cltSock.__doLogin__()
    def NotifyLoggedIn(self,o):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.appl)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(None,vtLog.DEBUG,'loggedin synch:%d'%self.IsFlag(self.SYNCHABLE),origin=self.appl)
        self.sUsr=o.GetUsr()
        self.iSecLv=o.GetSecLv()
        self.bAdmin=o.IsAdmin()
        self.SetLogin(o)
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'usr:%s;secLv:%d;admin:%d'%(self.sUsr,self.iSecLv,self.bAdmin),origin=self.appl)
        self.__setStatus__(self.LOGGEDIN)
        if self.IsBlockNotify()==False:
            wx.PostEvent(self,vtnxFileLoggedin(self))
        #self.cltSock.SendTelegram(u'get_config')
        #self.getDataFromRemote()
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'notify loggedin fin',origin=self.appl)
    def SetLogin(self,o):
        self.oLogin=o
    def NotifyOpenStart(self,fn):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.appl)
        #if self.silent==False:
        #    self.NotifyContent()
        self.sUsr=''
        self.iSecLv=-1
        self.bAdmin=False
        self.__setStatus__(self.OPEN_START)
        if self.IsBlockNotify()==False:
            wx.PostEvent(self,vtnxXmlOpenStart(self))
    def NotifyFileGetStart(self,sFN):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.appl)
        wx.PostEvent(self,vtnxFileGetStart(self,sFN))
    def NotifyFileGetProc(self,sFN,iAct,iSize):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.appl)
        wx.PostEvent(self,vtnxFileGetProc(self,sFN,iAct,iSize))
    def NotifyFileGetFin(self,sFN):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.appl)
        wx.PostEvent(self,vtnxFileGetFin(self,sFN))
    def NotifyFileGetAbort(self,sFN):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.appl)
        wx.PostEvent(self,vtnxFileGetAbort(self,sFN))
    def NotifyBrowse(self,l):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,origin=self.appl)
        wx.PostEvent(self,vtnxFileBrowse(self,l))
    def NotifyListContent(self,applAlias,l):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'alias:%s'%applAlas,origin=self.appl)
        wx.PostEvent(self,vtnxFileListContent(self,applAlias,l))
    def NotifyServed(self,served):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'served:%d'%served,origin=self.appl)
        if served==False:
            if 1==0:
                self.__connectionClosed__(False)
                if self.cltSrv is not None:
                    self.cltSrv.Stop()
                del self.cltSrv
                self.cltSrv=None
                self.cltSock=None
            self.__setStatus__(self.SERVE_FLT)
        else:
            self.__setStatus__(self.SERVED)
    def GetStatus(self,iStatus=None):
        if iStatus is not None:
            return iStatus&self.STATE_MASK
        return self.iStatus&self.STATE_MASK
    def New(self,root='PlugInSrvRoot'):
        pass
    def GetFN(self):
        pass
    def Open(self,fn=None,silent=False,bThread=True):
        pass
    def Save(self,fn=None,encoding='ISO-8859-1'):
        pass
    def Connect2Srv(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.appl)
        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'alias:%s host:%s port:%s usr:%s passwd:%s'%
                    (self.alias,self.host,self.port,self.usr,self.passwd),origin=self.appl)
        self.sUsr=''
        self.iSecLv=-1
        self.bAdmin=False
        if self.bOnline==False:
            vtLog.vtLngCur(vtLog.DEBUG,'offline;online:%d;fn:%s'%(self.bOnline,self.fn),origin=self.appl)
            return
        try:
            #PostEvent(self,vtnxXmlConnect())
            #self.__connectionClosed__()
            self.bDataSettled=False
            self.cltSrv=vtNetClt(self,self.host,self.port,
                                vtNetFileCltSock,verbose=self.verbose-1)
            self.__setStatus__(self.CONNECT)
            self.cltSrv.Connect()
        except Exception,list:
            self.__connectionClosed__(False)
            #self.Open()
            if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.ERROR):
                vtLog.vtLngCur(vtLog.ERROR,'exception:%s'%(repr(list)),origin=self.appl)
            if self.cltSrv is not None:
                self.cltSrv.Stop()
            del self.cltSrv
            self.cltSrv=None
            self.cltSock=None
    def Connect2SrvByAlias(self,objAlias,dn,force=False,offline=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.appl)
        #self.dn=self.docSetup.getNodeText(node,'path')
        self.dn=dn
        
        oldRecentAlias=self.sRecentAlias
        oldRecentConn=self.sRecentConn
        oldHost=self.host
        oldAlias=self.alias
        oldUsr=self.usr
            
        if objAlias is None:
            self.bConnInfoSet=False
            #self.fn=self.__genAutoFN__()
            self.cltSrv=None
            self.cltSock=None
            #self.Setup()
            return 0
        try:
            self.sRecentConn=objAlias.GetHostConnectionStr()
            self.sRecentAlias=objAlias.GetHostAliasStr()
            vtLog.vtLngCur(vtLog.DEBUG,'%s;%s'%(objAlias.GetHostAliasStr(),objAlias.GetStr()),origin=self.appl)
            
            self.host=objAlias.GetHost()
            self.port=objAlias.GetPort()
            self.alias=objAlias.GetAlias()
            self.usr=objAlias.GetUsr()
            self.passwd=objAlias.GetPasswd()
            self.bConnInfoSet=True
            self.fn=self.__genAutoFN__()
            #self.fn=self.GenAutoFNConnAppl(objAlias,self.dn)
            bOpen=False
            bClose=False
            bConn=True#not objAlias.IsOffline()
            #if offline:
            #    bConn=False
            if self.verbose>0:
                vtLog.CallStack('offline:%d connect:%d'%(offline,bConn))
            self.bOnline=bConn
            #if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            #    vtLog.vtLngCur(vtLog.DEBUG,'fn:%s'%(self.fn),origin=self.appl)
            #if len(oldRecentAlias)>0:
            #    if self.sRecentAlias!=oldRecentAlias:
            #        bOpen=True
            #        bClose=True
            #        if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
            #            vtLog.vtLngCur(vtLog.DEBUG,'oldconn:%s conn:%s'%(oldRecentAlias,self.sRecentAlias),origin=self.appl)
            if len(oldRecentConn)>0:
                if self.sRecentConn!=oldRecentConn:
                    #bOpen=True
                    bClose=True
                    if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'oldconn:%s conn:%s'%(oldRecentConn,self.sRecentConn),origin=self.appl)
            #if self.host!=oldHost:
            #    bOpen=True
            #if self.alias!=oldAlias:
            #    bOpen=True
                #bClose=True
            #if self.usr!=oldUsr:
            #    bOpen=True
                
            if self.verbose>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'bOpen:%d bClose:%d'%(bOpen,bClose),origin=self.appl)
                #vtLog.vtLngCur(vtLog.DEBUG,'open',origin=self.appl)
            #if self.IsFlag(self.OPENED)==False:
            #    bOpen=True
            if bOpen:
                self.bDataSettled=False
                if bConn:
                    # delay connection attept until file is completly opened
                    self.SetFlag(self.CONN_ACTIVE,True)
            else:
                if bConn:
                    self.SetFlag(self.CONN_ACTIVE,True)
                    self.Connect2Srv()
        except Exception,list:
            vtLog.vtLngTB(self.appl)
            self.sRecentAlias=''
            self.sRecentConn=''
            return -1
    def GetAutoFN(self):
        return '---'
    def __genAutoFN__(self):
        return '---'
    def Build(self):
        pass
    def BuildLang(self):
        pass
    def getDataFromRemote(self):
        return
        if self.IsFlag(self.SYNCHABLE)==False:
            #time.sleep(200)
            self.cltSock.SendTelegram(u'get_content')
