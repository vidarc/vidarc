#Boa:Dialog:vtNetSecXmlGuiMasterDialog
#----------------------------------------------------------------------------
# Name:         vNetXmlGuiMasterDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtNetSecXmlGuiMasterDialog.py,v 1.24 2007/10/29 21:17:27 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import wx.lib.scrolledpanel

from vidarc.tool.net.vNetXmlGuiMasterCloseDialog import vNetXmlGuiMasterCloseDialog
from vidarc.tool.net.vNetXmlSynchProc import vNetXmlSynchProc
from vidarc.tool.net.vNetXmlSynchTreeList import *#vNetXmlSynchTreeList
from vidarc.tool.net.vNetXmlWxGui import *
from vidarc.tool.xml.vtXmlDom import *

from vidarc.tool.xml.vtXmlTree import EVT_VTXMLTREE_THREAD_ADD_ELEMENTS
from vidarc.tool.xml.vtXmlTree import EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED
from vidarc.tool.xml.vtXmlTree import EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_ABORTED

import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
import sys,os

import vidarc.tool.net.vtNetArt as vtNetArt

import vidarc.tool.net.images as images

def create(parent):
    return vtNetSecXmlGuiMasterDialog(parent)

[wxID_VTNETSECXMLGUIMASTERDIALOG, wxID_VTNETSECXMLGUIMASTERDIALOGCBCFG, 
 wxID_VTNETSECXMLGUIMASTERDIALOGCBCLOSE, 
 wxID_VTNETSECXMLGUIMASTERDIALOGCBCONN, 
 wxID_VTNETSECXMLGUIMASTERDIALOGCBDISCONN, 
 wxID_VTNETSECXMLGUIMASTERDIALOGCBLOG, wxID_VTNETSECXMLGUIMASTERDIALOGCBOPEN, 
 wxID_VTNETSECXMLGUIMASTERDIALOGCBSAVE, 
 wxID_VTNETSECXMLGUIMASTERDIALOGCBSTATUS, 
 wxID_VTNETSECXMLGUIMASTERDIALOGCBSTOP, 
 wxID_VTNETSECXMLGUIMASTERDIALOGCBUPDATESYNCH, 
 wxID_VTNETSECXMLGUIMASTERDIALOGCHCCONNSYNCH, 
 wxID_VTNETSECXMLGUIMASTERDIALOGLSTAPPLCONN, 
 wxID_VTNETSECXMLGUIMASTERDIALOGNBMAIN, wxID_VTNETSECXMLGUIMASTERDIALOGPNCONN, 
 wxID_VTNETSECXMLGUIMASTERDIALOGPNSYNCH, 
 wxID_VTNETSECXMLGUIMASTERDIALOGSLCONNSYNCH, 
] = [wx.NewId() for _init_ctrls in range(17)]

class vtNetSecXmlGuiMasterDialog(wx.Dialog):
    UPDATE_TIME = 500
    def _init_coll_fgsSynch_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsSynch, 0, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)

    def _init_coll_bxsSynch_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.slConnSynch, 1, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.chcConnSynch, 1, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbUpdateSynch, 0, border=4,
              flag=wx.RIGHT | wx.LEFT)

    def _init_coll_fgsSynch_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.fgsInfo, 1, border=4,
              flag=wx.EXPAND | wx.BOTTOM | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbClose, 0, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER)

    def _init_coll_fgsConn_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbCfg, 0, border=4, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbConn, 0, border=4, flag=0)
        parent.AddWindow(self.cbDisConn, 0, border=4, flag=wx.TOP)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbStop, 0, border=4, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbOpen, 0, border=4, flag=0)
        parent.AddWindow(self.cbSave, 0, border=4, flag=wx.TOP)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbLog, 0, border=4, flag=0)
        parent.AddWindow(self.cbStatus, 0, border=4, flag=wx.TOP)

    def _init_coll_fgsConn_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstApplConn, 1, border=4,
              flag=wx.TOP | wx.BOTTOM | wx.EXPAND | wx.RIGHT | wx.LEFT)

    def _init_coll_fgsInfo_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.nbMain, 1, border=4, flag=wx.EXPAND)
        parent.AddSizer(self.bxsBt, 0, border=4, flag=wx.EXPAND | wx.LEFT)

    def _init_coll_fgsInfo_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_nbMain_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.pnConn, select=True,
              text=_(u'Settings'))
        parent.AddPage(imageId=-1, page=self.pnSynch, select=False,
              text=_(u'Synch'))

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self.fgsInfo = wx.FlexGridSizer(cols=2, hgap=0, rows=1, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.fgsConn = wx.FlexGridSizer(cols=1, hgap=0, rows=1, vgap=0)

        self.fgsSynch = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self.bxsSynch = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_fgsInfo_Items(self.fgsInfo)
        self._init_coll_fgsInfo_Growables(self.fgsInfo)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_fgsConn_Items(self.fgsConn)
        self._init_coll_fgsConn_Growables(self.fgsConn)
        self._init_coll_fgsSynch_Items(self.fgsSynch)
        self._init_coll_fgsSynch_Growables(self.fgsSynch)
        self._init_coll_bxsSynch_Items(self.bxsSynch)

        self.SetSizer(self.fgsData)
        self.pnConn.SetSizer(self.fgsConn)
        self.pnSynch.SetSizer(self.fgsSynch)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTNETSECXMLGUIMASTERDIALOG,
              name=u'vtNetSecXmlGuiMasterDialog', parent=prnt, pos=wx.Point(244,
              152), size=wx.Size(514, 366),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE | wx.STAY_ON_TOP,
              title=u'vtNetSecGuiMaster Dialog')
        self.SetClientSize(wx.Size(506, 339))

        self.cbClose = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTNETSECXMLGUIMASTERDIALOGCBCLOSE,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Close'), name=u'cbClose',
              parent=self, pos=wx.Point(215, 305), size=wx.Size(76, 30),
              style=0)
        self.cbClose.SetMinSize(wx.Size(-1, -1))
        self.cbClose.Bind(wx.EVT_BUTTON, self.OnCbCloseButton,
              id=wxID_VTNETSECXMLGUIMASTERDIALOGCBCLOSE)

        self.nbMain = wx.Notebook(id=wxID_VTNETSECXMLGUIMASTERDIALOGNBMAIN,
              name=u'nbMain', parent=self, pos=wx.Point(4, 4), size=wx.Size(418,
              285), style=0)
        self.nbMain.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGING,
              self.OnNbMainNotebookPageChanging,
              id=wxID_VTNETSECXMLGUIMASTERDIALOGNBMAIN)
        self.nbMain.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGED,
              self.OnNbMainNotebookPageChanged,
              id=wxID_VTNETSECXMLGUIMASTERDIALOGNBMAIN)

        self.pnConn = wx.Panel(id=wxID_VTNETSECXMLGUIMASTERDIALOGPNCONN,
              name=u'pnConn', parent=self.nbMain, pos=wx.Point(0, 0),
              size=wx.Size(410, 259), style=wx.TAB_TRAVERSAL)

        self.lstApplConn = wx.ListView(id=wxID_VTNETSECXMLGUIMASTERDIALOGLSTAPPLCONN,
              name=u'lstApplConn', parent=self.pnConn, pos=wx.Point(4, 4),
              size=wx.Size(402, 251), style=wx.LC_REPORT)
        self.lstApplConn.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstApplConnListItemSelected,
              id=wxID_VTNETSECXMLGUIMASTERDIALOGLSTAPPLCONN)
        self.lstApplConn.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstApplConnListItemDeselected,
              id=wxID_VTNETSECXMLGUIMASTERDIALOGLSTAPPLCONN)
        self.lstApplConn.Bind(wx.EVT_LEFT_DCLICK, self.OnLstApplConnLeftDclick)
        self.lstApplConn.Bind(wx.EVT_LIST_COL_CLICK,
              self.OnLstApplConnListColClick,
              id=wxID_VTNETSECXMLGUIMASTERDIALOGLSTAPPLCONN)

        self.cbCfg = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTNETSECXMLGUIMASTERDIALOGCBCFG,
              bitmap=vtArt.getBitmap(vtArt.Settings), label=_(u'Config'), name=u'cbCfg',
              parent=self, pos=wx.Point(426, 4), size=wx.Size(76, 30), style=0)
        self.cbCfg.SetMinSize(wx.Size(-1, -1))
        self.cbCfg.Bind(wx.EVT_BUTTON, self.OnCbCfgButton,
              id=wxID_VTNETSECXMLGUIMASTERDIALOGCBCFG)

        self.cbConn = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTNETSECXMLGUIMASTERDIALOGCBCONN,
              bitmap=vtNetArt.getBitmap(vtNetArt.Connect), label=_(u'Connect'),
              name=u'cbConn', parent=self, pos=wx.Point(426, 42),
              size=wx.Size(76, 30), style=0)
        self.cbConn.SetMinSize(wx.Size(-1, -1))
        self.cbConn.Bind(wx.EVT_BUTTON, self.OnCbConnButton,
              id=wxID_VTNETSECXMLGUIMASTERDIALOGCBCONN)

        self.cbDisConn = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTNETSECXMLGUIMASTERDIALOGCBDISCONN,
              bitmap=vtNetArt.getBitmap(vtNetArt.Stopped), label=_(u'Discon'),
              name=u'cbDisConn', parent=self, pos=wx.Point(426, 76),
              size=wx.Size(76, 30), style=0)
        self.cbDisConn.SetMinSize(wx.Size(-1, -1))
        self.cbDisConn.Bind(wx.EVT_BUTTON, self.OnCbDisConnButton,
              id=wxID_VTNETSECXMLGUIMASTERDIALOGCBDISCONN)

        self.cbStop = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTNETSECXMLGUIMASTERDIALOGCBSTOP,
              bitmap=vtArt.getBitmap(vtArt.Stop), label=_(u'Stop'), name=u'cbStop',
              parent=self, pos=wx.Point(426, 114), size=wx.Size(76, 30),
              style=0)
        self.cbStop.SetMinSize(wx.Size(-1, -1))
        self.cbStop.Bind(wx.EVT_BUTTON, self.OnCbStopButton,
              id=wxID_VTNETSECXMLGUIMASTERDIALOGCBSTOP)

        self.cbOpen = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTNETSECXMLGUIMASTERDIALOGCBOPEN,
              bitmap=vtNetArt.getBitmap(vtNetArt.Open), label=_(u'Open'), name=u'cbOpen',
              parent=self, pos=wx.Point(426, 152), size=wx.Size(76, 30),
              style=0)
        self.cbOpen.SetMinSize(wx.Size(-1, -1))
        self.cbOpen.Bind(wx.EVT_BUTTON, self.OnCbOpenButton,
              id=wxID_VTNETSECXMLGUIMASTERDIALOGCBOPEN)

        self.cbSave = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTNETSECXMLGUIMASTERDIALOGCBSAVE,
              bitmap=vtNetArt.getBitmap(vtNetArt.Save), label=_(u'Save'), name=u'cbSave',
              parent=self, pos=wx.Point(426, 186), size=wx.Size(76, 30),
              style=0)
        self.cbSave.SetMinSize(wx.Size(-1, -1))
        self.cbSave.Bind(wx.EVT_BUTTON, self.OnCbSaveButton,
              id=wxID_VTNETSECXMLGUIMASTERDIALOGCBSAVE)

        self.pnSynch = wx.Panel(id=wxID_VTNETSECXMLGUIMASTERDIALOGPNSYNCH,
              name=u'pnSynch', parent=self.nbMain, pos=wx.Point(0, 0),
              size=wx.Size(410, 259), style=wx.TAB_TRAVERSAL)

        self.chcConnSynch = wx.Choice(choices=[],
              id=wxID_VTNETSECXMLGUIMASTERDIALOGCHCCONNSYNCH,
              name=u'chcConnSynch', parent=self.pnSynch, pos=wx.Point(155, 4),
              size=wx.Size(143, 21), style=0)
        self.chcConnSynch.Bind(wx.EVT_CHOICE, self.OnChcConnSynchChoice,
              id=wxID_VTNETSECXMLGUIMASTERDIALOGCHCCONNSYNCH)

        self.slConnSynch = wx.Slider(id=wxID_VTNETSECXMLGUIMASTERDIALOGSLCONNSYNCH,
              maxValue=0, minValue=0, name=u'slConnSynch', parent=self.pnSynch,
              point=wx.Point(4, 8), size=wx.Size(100, 26),
              style=wx.SL_HORIZONTAL, value=0)
        self.slConnSynch.Bind(wx.EVT_SCROLL, self.OnSlConnSynchScroll)

        self.cbUpdateSynch = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTNETSECXMLGUIMASTERDIALOGCBUPDATESYNCH,
              bitmap=vtNetArt.getBitmap(vtNetArt.Synch), label=_(u'Update'),
              name=u'cbUpdateSynch', parent=self.pnSynch, pos=wx.Point(306, 4),
              size=wx.Size(100, 30), style=0)
        self.cbUpdateSynch.Bind(wx.EVT_BUTTON, self.OnCbUpdateSynchButton,
              id=wxID_VTNETSECXMLGUIMASTERDIALOGCBUPDATESYNCH)

        self.cbLog = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTNETSECXMLGUIMASTERDIALOGCBLOG,
              bitmap=vtArt.getBitmap(vtArt.Log), label=_(u'Log'), name=u'cbLog',
              parent=self, pos=wx.Point(426, 224), size=wx.Size(76, 30),
              style=0)
        self.cbLog.SetMinSize(wx.Size(-1, -1))
        self.cbLog.Bind(wx.EVT_BUTTON, self.OnCbLogButton,
              id=wxID_VTNETSECXMLGUIMASTERDIALOGCBLOG)

        self.cbStatus = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTNETSECXMLGUIMASTERDIALOGCBSTATUS,
              bitmap=vtArt.getBitmap(vtArt.Link), label=_(u'Status'),
              name=u'cbStatus', parent=self, pos=wx.Point(426, 258),
              size=wx.Size(76, 30), style=0)
        self.cbStatus.SetMinSize(wx.Size(-1, -1))
        self.cbStatus.Bind(wx.EVT_BUTTON, self.OnCbStatusButton,
              id=wxID_VTNETSECXMLGUIMASTERDIALOGCBSTATUS)

        self._init_coll_nbMain_Pages(self.nbMain)

        self._init_sizers()

    def __init__(self, parent,verbose=0):
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        self.STATE_MAP=[
            []  ,  # state
            [_(u'no access')    ,_(u'access')]  ,      # configuation   DATA_ACCESS
            [_('notify')        ,_(u'blocked')]  ,     # notify         BLOCK_NOTIFY
            [_(u'closed')       ,_(u'opened')]  ,      # open           OPENED
            [_(u'unknown')      ,_(u'valid')]  ,       # config         CONFIG_VALID
            [_(u'copy')         ,_(u'synchable')]  ,   # synch          SYNCHABLE
            [_(u'disconnected') ,_(u'established')]  , # connection     CONN_ACTIVE
            [_(u'running')      ,_(u'paused')]  ,      # connection     CONN_ACTIVE
            
            ]
        self._init_ctrls(parent)
        try:
            sOrigin=vtLog.vtLngGetRelevantNamesWX(self)
            l=sOrigin.split('.')
            sOrigin='.'.join(l[:-3]+[l[-1]])
            self.origin=sOrigin
        except:
            self.origin='vtNetSecXmlGuiMasterDialog'
        try:
            icon = wx.EmptyIcon()
            icon.CopyFromBitmap(vtNetArt.getBitmap(vtNetArt.Synch))
            self.SetIcon(icon)
        except:
            vtLog.vtLngTB(self.sOrigin)
        
        self.verbose=verbose
        self.bBlockNotify=False
        
        self.idxSel=-1
        
        self.appls=[]
        self.mainAppl=''
        self.dn=''
        self.alias=''
        self.host=''
        self.port=-1
        self.usr=''
        self.passwd=''
        
        self.netMaster=parent
        self.bConnectAll=False
        self.bOpenAll=False
        
        self.dlgCfg=None
        self.dlgLog=None
        self.dlgStatus=None
        self.lstApplConn.InsertColumn(0,_(u'state'),wx.LIST_FORMAT_LEFT,40)
        self.lstApplConn.InsertColumn(1,_(u'appl'),wx.LIST_FORMAT_LEFT,60)
        self.lstApplConn.InsertColumn(2,_(u'alias'),wx.LIST_FORMAT_LEFT,60)
        self.lstApplConn.InsertColumn(3,_(u'host'),wx.LIST_FORMAT_LEFT,60)
        self.lstApplConn.InsertColumn(4,_(u'usr'),wx.LIST_FORMAT_LEFT,60)
        self.lstApplConn.InsertColumn(5,_(u'fn'),wx.LIST_FORMAT_LEFT,160)
        self.lstApplConn.InsertColumn(6,_(u'port'),wx.LIST_FORMAT_RIGHT,60)
        self.SetupImageList()
        
        self.docSynch=vNetXmlSynchProc(None)
        self.trlstSynch=vNetXmlSynchTreeList(parent=self.pnSynch,
                id=wx.NewId(), name=u'trlstSynch', 
                pos=wx.Point(4, 44), size=wx.Size(392, 172), style=wx.TR_HAS_BUTTONS,
                master=False,verbose=1)
        EVT_VTXMLTREE_ITEM_SELECTED(self.trlstSynch,self.OnSynchTreeItemSel)
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.trlstSynch,self.OnSynchAddElement)
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.trlstSynch,self.OnSynchAddFinished)
        self.trlstSynch.SetDoc(self.docSynch)
        self.fgsSynch.AddWindow(self.trlstSynch, 1, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.BOTTOM | wx.TOP | wx.EXPAND)
        #self.fgsSynch.Layout()
        self.pnSynch.Refresh()
        self.pnSynch.SetSizer(self.fgsSynch)
        self.fgsData.Fit(self)
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        pass
    def Show(self,show=True):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'show:%d'%(show),self)
        try:
            self.netMaster.__logStatus__()
        except:
            vtLog.vtLngTB(self.GetName())
        wx.Dialog.Show(self,show)
    def SetupImageList(self):
        self.imgDict={}
        self.imgLstTyp=wx.ImageList(16,16)
        self.imgDict['dft']={-1:self.imgLstTyp.Add(vtNetArt.getBitmap(vtNetArt.Error))}
        
        self.lstApplConn.SetImageList(self.imgLstTyp,wx.IMAGE_LIST_SMALL)
        
        self.imgNB=wx.ImageList(16,16)
        self.imgNB.Add(images.getNetConfigBitmap())
        self.imgNB.Add(images.getNetSynchBitmap())
        self.nbMain.SetImageList(self.imgNB)
        for i in range(2):
            self.nbMain.SetPageImage(i,i)
    def SetCfg(self,netMaster,appls,mainAppl):
        self.netMaster=netMaster
        self.appls=appls
        self.mainAppl=mainAppl
        self.lstApplConn.DeleteAllItems()
        self.chcConnSynch.Clear()
        self.slConnSynch.SetRange(0,len(appls)-1)
        
        for appl in appls:
            netDoc=self.netMaster.dNetDoc[appl]
            try:
                if appl not in self.imgDict:
                    d=netDoc.GetListImgDict(self.imgLstTyp)
                    if d is not None:
                        self.imgDict[appl]=d
                    else:
                        self.imgDict[appl]=self.imgDict[-1]
            except:
                vtLog.vtLngTB(self.GetName())
                self.imgDict[appl]=self.imgDict[-1]
            idx=self.lstApplConn.InsertImageStringItem(sys.maxint, '', 
                            self.__getImgIndex__(appl,netDoc.GetStatus()))
            self.lstApplConn.SetStringItem(idx,1,appl,-1)
            self.chcConnSynch.Append(appl)
        self.chcConnSynch.SetSelection(0)
        
    def SetCfgNode(self,bConnect=True):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        idx=0
        dn=self.dlgCfg.GetDN()
        conn=self.dlgCfg.GetConnection()
        for name in self.netMaster.lNetAppl:
            netDoc=self.netMaster.dNetDoc[name]
            oAlias=conn.GetAlias(name)
            if oAlias is None:
                idx+=1
                continue
            sFN=oAlias.GenAutoFN(dn)
            if sFN is None:
                sFN=''
            self.lstApplConn.SetStringItem(idx,2,oAlias.GetAlias(),-1)
            self.lstApplConn.SetStringItem(idx,3,oAlias.GetHost(),-1)
            self.lstApplConn.SetStringItem(idx,4,oAlias.GetUsr(),-1)
            self.lstApplConn.SetStringItem(idx,5,sFN,-1)
            self.lstApplConn.SetStringItem(idx,6,str(oAlias.GetPort()),-1)
            idx+=1
    def SetCfgDlg(self,dlg):
        self.dlgCfg=dlg
    def SetStatusDlg(self,dlg):
        self.dlgStatus=dlg
    def SetLogDlg(self,dlg):
        self.dlgLog=dlg
    
    def __getImgIndex__(self,name,status):
        try:
            return self.imgDict[name][status]
        except:
            try:
                return self.imgDict['dft'][status]
            except:
                return self.imgDict['dft'][-1]
    def IsModified(self):
        for netDoc in self.dictNet.values():
            if netDoc.IsModified():
                return True
        return False
    def Save(self,bModified=False):
        for name in self.netMaster.lNetAppl:
            netDoc=self.netMaster.dNetDoc[name]
            if bModified:
                if netDoc.IsModified():
                    netDoc.Save()
            else:
                netDoc.Save()
        
    def __getNetDocIdxName__(self,netDoc):
        i=0
        for name in self.netMaster.lNetAppl:
            if self.netMaster.dNetDoc[name]==netDoc:
                return i,name
            i+=1
        return -1,''
    
    def Notify(self,name,netDoc,iStatusPrev,iStatus,*args,**kwargs):
        try:
            idx=self.netMaster.lNetAppl.index(name)
        except:
            traceback.print_exc()
            vtLog.vtLngCur(vtLog.ERROR,
                    'name:%s;lstNetAppl:%s;netDoc:%s;%s'%(name,
                    repr(self.lstNetAppl),repr(netDoc),traceback.format_exc()),
                    origin=self.origin)
            return
        try:
            vtLog.vtLngCur(vtLog.DEBUG,'name:%s;prev:0x%04x;status:0x%04x'%(name,
                    iStatusPrev,iStatus),origin=self.origin)
            self.lstApplConn.SetStringItem(idx,0,'',
                            self.__getImgIndex__(name,netDoc.GetStatus(iStatus)))
            try:
                fn=netDoc.GetFN()
                if fn is None:
                    fn='???'
            except:
                fn='???'
            self.lstApplConn.SetStringItem(idx,2,netDoc.alias,-1)
            self.lstApplConn.SetStringItem(idx,3,netDoc.host,-1)
            self.lstApplConn.SetStringItem(idx,4,netDoc.usr,-1)
            self.lstApplConn.SetStringItem(idx,5,fn,-1)
            self.lstApplConn.SetStringItem(idx,6,repr(netDoc.port),-1)
            #self.__updateApplButtons__()
        except:
            vtLog.vtLngTB(self.GetName())

    def __updateApplButtons__(self):
        if self.idxSel<0:
            #if self.IsAllDisconnected():
            #    self.cbConn.SetBitmapLabel(img_gui.getConnectBitmap())
            #else:
            #    self.cbConn.SetBitmapLabel(img_gui.getStoppedBitmap())
            self.cbConn.Enable(self.IsAllSettled())
            
            #if self.IsAllClosed():
            #    self.cbOpen.SetLabel(_(u'Open'))
            #    self.cbOpen.SetBitmapLabel(img_gui.getOpenBitmap())
            #else:
            #    self.cbOpen.SetLabel(_(u'Save'))
            #    self.cbOpen.SetBitmapLabel(img_gui.getSaveBitmap())
            self.cbOpen.Enable(self.IsAllSettled())
        else:
            netDoc=self.netMaster.dNetDoc[self.netMaster.lNetAppl[self.idxSel]]
            #if netDoc.IsActive():
            #    self.cbConn.SetLabel(_(u'Discon.'))
            #    self.cbConn.SetBitmapLabel(img_gui.getStoppedBitmap())
            #else:
            #    self.cbConn.SetLabel(_(u'Connect'))
            #    self.cbConn.SetBitmapLabel(img_gui.getConnectBitmap())
            self.cbConn.Enable(netDoc.IsSettled())
            
            #if netDoc.IsFlag(netDoc.OPENED)==False:
            #    self.cbOpen.SetLabel(_(u'Open'))
            #    self.cbOpen.SetBitmapLabel(images.getBrowseBitmap())
            #else:
            #    self.cbOpen.SetLabel(_(u'Save'))
            #    self.cbOpen.SetBitmapLabel(img_gui.getSaveBitmap())
            self.cbOpen.Enable(netDoc.IsSettled())
        self.cbConn.Refresh()
        self.cbOpen.Refresh()
                    
    def OnLstApplConnListItemSelected(self, event):
        self.idxSel=event.GetIndex()
        #self.__updateApplButtons__()
        event.Skip()

    def OnLstApplConnListItemDeselected(self, event):
        self.idxSel=-1
        #self.__updateApplButtons__()
        event.Skip()

    def OnLstApplConnListColClick(self, event):
        if self.idxSel!=-1:
            self.lstApplConn.SetItemState(self.idxSel,0,wx.LIST_STATE_SELECTED)
                    
        self.idxSel=-1
        if self.IsAllDisconnected():
            self.cbConn.SetBitmapLabel(vtNetArt.getBitmap(vtNetArt.Connect))
        else:
            self.cbConn.SetBitmapLabel(vtNetArt.getBitmap(vtNetArt.Stopped))
        self.cbConn.Refresh()
        event.Skip()

    def OnCbCfgButton(self, event):
        if self.idxSel==-1:
            if self.netMaster.netDocMaster.IsActive():
                return
        if self.dlgCfg is None:
            return
        self.dlgCfg.Show(False,True)
        self.dlgCfg.SetFocus()
        return
        self.dlgCfg.ShowModal()
        return
        appl=self.lstNetAppl[self.idxSel]
        self.dlgCfg.SetNode(self.docSetup,self.lstCfg,self.appls,
                        appl,self.__getCfgNode__())
        if self.idxSel>=0:
            self.dlgCfg.SelAppl(appl)
        self.dlgCfg.Centre()                
        self.Show(False)
        ret=self.dlgCfg.ShowModal()
        if ret>0:
            #self.netMaster.AutoConnect2Srv()
            #self.OnCbConnButton(None)
            self.docSetup.Save()
            self.__updateListCfg__()
            self.Show(True)
            
            pass
        self.__updateListCfg__()
        event.Skip()

    def OnCbCloseButton(self, event):
        self.Show(False)
        event.Skip()

    def OnCbConnButton(self, event):
        if self.idxSel==-1:
            if self.netMaster.IsAllSettled()==False:
                return
            if self.netMaster.IsDataAccessed()==True:
                return
            if self.bOpenAll:
                return
            if self.netMaster.IsAllDisconnected():
                netDoc=self.netMaster.dNetDoc[self.netMaster.lNetAppl[0]]
                netDoc.SetFlag(netDoc.CFG_VALID,False)
                for netDoc in self.netMaster.dNetDoc.values():
                    netDoc.SetFlag(netDoc.OPENED,False)
                self.bConnectAll=True
                self.netMaster.AutoConnect2Srv(self.idxSel)
            #self.__updateApplButtons__()
            return
        else:
            netDoc=self.netMaster.dNetDoc[self.netMaster.lNetAppl[self.idxSel]]
            if netDoc.GetStatus()==netDoc.LOGIN:
                netDoc.Login()
                return
            if self.bConnectAll:
                return
            if self.bOpenAll:
                return
            self.bConnectAll=False
            if netDoc.IsFlag(netDoc.CONN_ACTIVE)==False:
                self.netMaster.AutoConnect2Srv(self.idxSel)
            #self.__updateApplButtons__()
        if event is not None:
            event.Skip()
    
    def OnCbDisConnButton(self, event):
        if self.idxSel==-1:
            #if self.IsAllSettled()==False:
            #    return
            if self.netMaster.IsDataAccessed()==True:
                return
            self.bConnectAll=False
            #self.thdStop.Start()
            #self.__updateApplButtons__()
            return
        else:
            if self.bConnectAll:
                return
            if self.bOpenAll:
                return
            self.bConnectAll=False
            netDoc=self.netMaster.dNetDoc[self.netMaster.lNetAppl[self.idxSel]]
            netDoc.DisConnect()
        if event is not None:
            event.Skip()
    
    def __updateListCfg__(self,idx=-1):
        try:
            dn=self.dlgCfg.GetDN()
            
            conn=self.dlgCfg.GetConnection()
            for i in range(len(self.netMaster.lNetAppl)):
                if idx>=0:
                    if idx!=i:
                        continue
                
                sAppl=self.netMaster.lNetAppl[i]
                oAlias=conn.GetAlias(sAppl)
                sFN=oAlias.GenAutoFN(dn)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'appl:%s;oAlias:%s'%(sAppl,`oAlias`),self)
                netDoc=self.netMaster.dNetDoc[sAppl]
                #netDoc.GenAutoFN(conn.GetAlias(sAppl),dn)
                netDoc.GenAutoFNConnAppl(oAlias,dn)
                if oAlias is not None:
                    if sFN is None:
                        sFN=''
                    try:
                        fn=netDoc.GetFN()
                        if fn is None:
                            fn=u'???'
                    except:
                        fn=u'???'
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurWX(vtLog.DEBUG,'fn:%s;sFN:%s'%(fn,sFN),self)
                    self.lstApplConn.SetStringItem(i,2,oAlias.GetAlias(),-1)
                    self.lstApplConn.SetStringItem(i,3,oAlias.GetHost(),-1)
                    self.lstApplConn.SetStringItem(i,4,oAlias.GetUsr(),-1)
                    self.lstApplConn.SetStringItem(i,5,fn,-1)
                    self.lstApplConn.SetStringItem(i,6,str(oAlias.GetPort()),-1)
                else:
                    self.lstApplConn.SetStringItem(i,2,'',-1)
                    self.lstApplConn.SetStringItem(i,3,'',-1)
                    self.lstApplConn.SetStringItem(i,4,'',-1)
                    self.lstApplConn.SetStringItem(i,5,'',-1)
                    self.lstApplConn.SetStringItem(i,6,'',-1)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbOpenButton(self, event):
        if self.idxSel==-1:
            if self.netMaster.IsAllSettled()==False:
                return
            if self.netMaster.IsDataAccessed()==True:
                return
            if self.bOpenAll:
                return
            if self.netMaster.IsAllClosed():
                netDoc=self.netMaster.dNetDoc[self.netMaster.lNetAppl[0]]
                netDoc.SetFlag(netDoc.CFG_VALID,False)
                for netDoc in self.netMaster.dNetDoc.values():
                    netDoc.SetFlag(netDoc.OPENED,False)
                    netDoc.Open()
                self.bOpenAll=True
            #self.__updateApplButtons__()
        else:
            netDoc=self.netMaster.dNetDoc[self.netMaster.lNetAppl[self.idxSel]]
            if netDoc.IsRunning():
                return
            if netDoc.IsFlag(netDoc.CONN_ACTIVE)==False:
                self.Show(False)
                dlg = wx.FileDialog(self, _(u"Open"), ".", "", _(u"XML files (*.xml)|*.xml|all files (*.*)|*.*"), wx.OPEN)
                try:
                    self.__updateListCfg__(self.idxSel)
                    conn=self.dlgCfg.GetConnection()
                    fn=netDoc.GetFN()
                    if fn is not None:
                        dlg.SetPath(fn)
                    if dlg.ShowModal() == wx.ID_OK:
                        filename = dlg.GetPath()
                        netDoc.Open(filename)
                        self.lstApplConn.SetStringItem(self.idxSel,5,netDoc.GetFN(),-1)
                finally:
                    dlg.Destroy()
                self.Show(True)
            else:
                sMsg=_(u'Connection still active!')+' \n\n'+_(u'Close connection to use this feautre.')
                dlg=wx.MessageDialog(self,sMsg ,
                            u'vtNetSecXmlGuiMasterDialog',
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg.Destroy()
                return
        event.Skip()

    def OnCbSaveButton(self, event):
        if self.idxSel==-1:
            if self.netMaster.IsAllSettled()==False:
                return
            if self.netMaster.IsDataAccessed()==True:
                return
            netDoc=self.netMaster.dNetDoc[self.netMaster.lNetAppl[0]]
            netDoc.SetFlag(netDoc.CFG_VALID,False)
            for netDoc in self.netMaster.dNetDoc.values():
                netDoc.Pause(True)
                if netDoc.WaitNotRunning(5)==False:
                    vtLog.vtLngCur(vtLog.ERROR,
                            'name:%s;netDoc:%s'%(self.netMaster.lNetAppl[self.idxSel],repr(netDoc)),origin='vNetMasterDlg')
                    sMsg=_(u'Timeout on pausing')+' %s.'%self.netMaster.lNetAppl[self.idxSel]
                    dlg=wx.MessageDialog(self,sMsg ,
                                u'vtNetSecXmlGuiMasterDialog',
                                wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                    dlg.ShowModal()
                    dlg.Destroy()
                    continue
                if netDoc.IsFlag(netDoc.OPENED):
                    #netDoc.AlignDoc()
                    netDoc.Save()
                netDoc.Pause(False)
        else:
            netDoc=self.netMaster.dNetDoc[self.netMaster.lNetAppl[self.idxSel]]
            netDoc.Pause(True)
            if netDoc.WaitNotRunning(5)==False:
            #if netDoc.IsRunning():
                vtLog.vtLngCur(vtLog.ERROR,
                        'name:%s;netDoc:%s'%(self.netMaster.lNetAppl[self.idxSel],repr(netDoc)),origin='vNetMasterDlg')
                sMsg=_(u'Timeout on pausing')+' %s.'%self.netMaster.lNetAppl[self.idxSel]
                dlg=wx.MessageDialog(self,sMsg ,
                            u'vtNetSecXmlGuiMasterDialog',
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg.Destroy()
                return
            if netDoc.IsFlag(netDoc.OPENED):
                #netDoc.AlignDoc()
                netDoc.Save()
            netDoc.Pause(False)
        event.Skip()

    def OnLstApplConnLeftDclick(self, event):
        self.OnCbConnButton(None)
        event.Skip()
    def Stop(self):
        self.Show(False)
        self.bBlockNotify=True
        dlg=vNetXmlGuiMasterCloseDialog(self)
        dlg.Centre()
        dlg.SetDictNet(self.netMaster.dNetDoc)
        dlg.ShowModal()
        time.sleep(2)
    def ShutDown(self):
        #for k in self.netMaster.dNetDoc.keys():
        #    netDoc=self.netMaster.dNetDoc[k]
        #    netDoc.SetFlag(netDoc.SHUTDOWN,True)
        #self.thdLog.Stop()
        #while self.thdLog.IsRunning():
        #    time.sleep(1)
        #self.Stop()
        self.netMaster=None

    def OnCbStopButton(self, event):
        event.Skip()

    def __getNodeInfoBySynch__(self,netDoc,k,synchInfo):
        sID='-1'
        sName=''
        node=None
        if k in ['edit','edit_fault','add','add_fault','move','move_fault','ident']:
            sID=synchInfo
            node=netDoc.getNodeById(sID)
            sName=netDoc.ConvertIdNum2Str(long(sID))
        elif k in ['edit_offline','add_offline']:
            node=k
            sName=netDoc.getKey(k)
        elif k in ['del','del_fault']:
            sName=synchInfo
        return long(sID),sName,node
    def __updateConnSynch__(self):
        iSel=self.chcConnSynch.GetSelection()
        netDoc=self.netMaster.dNetDoc[self.netMaster.lNetAppl[iSel]]
        sSynchFN=netDoc.GetSynchFN()
        self.docSynch.Open(sSynchFN)
        self.trlstSynch.SetNode(None)
        
    def OnChcConnChoice(self, event):
        iSel=self.chcConn.GetSelection()
        self.slConn.SetValue(iSel)
        self.__updateConnState__()
        event.Skip()

    def OnSlConnScroll(self, event):
        iSel=self.slConn.GetValue()
        self.chcConn.SetSelection(iSel)
        self.__updateConnState__()
        event.Skip()

    def OnChcConnSynchChoice(self, event):
        iSel=self.chcConnSynch.GetSelection()
        self.slConnSynch.SetValue(iSel)
        event.Skip()

    def OnSlConnSynchScroll(self, event):
        iSel=self.slConnSynch.GetValue()
        self.chcConnSynch.SetSelection(iSel)
        event.Skip()

    def OnNbMainNotebookPageChanging(self, event):
        event.Skip()

    def OnNbMainNotebookPageChanged(self, event):
        old = event.GetOldSelection()
        sel = event.GetSelection()
        #if sel>=len(self.lstPanel):
        #    return
        try:
            if sel==0:
                self.__updateConnState__()
        except:
            pass
        event.Skip()

    def OnCbUpdateSynchButton(self, event):
        self.__updateConnSynch__()
        event.Skip()

    def OnSynchTreeItemSel(self,evt):
        pass
    def OnSynchAddElement(self,evt):
        pass
    def OnSynchAddFinished(self,evt):
        pass

    def OnCbLogButton(self, event):
        if self.dlgLog is not None:
            self.dlgLog.Show()
            self.dlgLog.SetFocus()
        event.Skip()

    def OnCbStatusButton(self, event):
        if self.dlgStatus is not None:
            self.dlgStatus.Show()
            self.dlgStatus.SetFocus()
        event.Skip()
