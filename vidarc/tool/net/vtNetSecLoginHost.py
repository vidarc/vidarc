#----------------------------------------------------------------------------
# Name:         vNetSecLoginHost.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060413
# CVS-ID:       $Id: vtNetSecLoginHost.py,v 1.8 2006/09/26 05:06:00 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.sec.vtSecLoginInfo import vtSecLoginInfo
import os.path
import vidarc.tool.log.vtLog as vtLog

def getHostAliasStr(host,port,alias):
    try:
        port=int(port)
    except:
        port=50000
    return u'%s:%d//%s'%(host,port,alias)
def setHostAliasStr(s):
    i=s.find(':')
    j=s.find('//')
    if i<0 or j<0:
        return '','50000',''
    return s[:i],s[i+1:j],s[j+2:]
def getHostConnectionStr(host,port,usr,alias):
    try:
        port=int(port)
    except:
        port=50000
    return u'%s:%d//%s@%s'%(host,port,usr,alias)
def setHostConnectionStr(s):
    i=s.find(':')
    j=s.find('//')
    k=s.find('@')
    if i<0 or j<0 or k<0:
        return '','50000','',''
    return s[:i],s[i+1:j],s[j+2:k],s[k+1:]
def getApplHostConnectionStr(host,port,usr,alias,appl):
    return u'%s@%s'%(appl,getHostConnectionStr(host,port,usr,alias))
def setApplHostConnectionStr(s):
    i=s.find('@')
    if i>=0:
        return s[:i],setHostConnectionStr(s[i+1:])
    else:
        return '',setHostConnectionStr('')

class vtNetSecLoginHost(vtSecLoginInfo):
    def __init__(self,passwd='',usr='',usrId='-1',grpIds=[],grps=[],iSecLv=0,bAdmin=False,host='',port='',appl='',alias=''):
        self.passwd=passwd
        try:
            iSecLv=int(iSecLv)
        except:
            iSecLv=0
        vtSecLoginInfo.__init__(self,usr,usrId,grpIds,grps,iSecLv,bAdmin)
        self.host=host
        try:
            self.port=int(port)
        except:
            self.port=50000
            vtLog.vtLngTB(self.__class__.__name__)
        self.appl=appl
        self.alias=alias
        self.fn=''
        self.b2Cfg=False
    def __str__(self):
        return ''.join(['appl:%s;alias:%s;b2Cfg:%d'%(self.appl,self.alias,self.b2Cfg),
                self.GetStr()])
    def Set(self,o):
        vtSecLoginInfo.Set(self,o)
        self.host=o.host
        self.port=o.port
        self.appl=o.appl
        self.alias=o.alias
        self.fn=o.fn
        self.b2Cfg=False
    def SetLoginInfo(self,o):
        vtSecLoginInfo.Set(self,o)
    def GetHost(self):
        return self.host
    def GetPort(self):
        return self.port
    def GetAppl(self):
        return self.appl
    def GetAlias(self):
        return self.alias
    def SetAlias(self,s):
        self.alias=s
    def GetPasswd(self):
        return self.passwd
    def GetStr(self):
        s='host:%s;port:%d;usr:%s,%s;grp:%s;secLv:%d;passwd:%s'%(self.host,self.port,self.GetUsr(),str(self.GetUsrId()),','.join(map(str,self.GetGrpIds())),self.GetSecLv(),self.passwd)
        return s
    def GetHostAliasStr(self):
        if self.IsOffline():
            return '---'
        return getHostAliasStr(self.host,self.port,self.alias)
        #return getHostConnectionStr(self.host,self.port,self.usr,self.alias)
    def GetHostConnectionStr_New(self):
        if self.IsOffline():
            return '---'
        return getHostConnectionStr(self.host,self.port,self.usr,self.alias)
    def SetFN(self,fn):
        self.fn=fn
    def GetFN(self):
        return self.fn
    def GenAutoFN(self,dn):
        try:
            if self.IsOffline():
                return self.fn
            else:
                if self.IsOnlineValid():
                    return os.path.join(dn,self.GetHost(),
                                    self.GetUsr(),
                                    self.GetAlias(),
                                    self.appl+'.xml')
                else:
                    return None
        except:
            vtLog.vtLogTB(self.__class__.__name__)
            return os.path.join(dn,'local','usr','dummy',self.appl+'.xml')
    def IsOnlineValid(self):
        if len(self.host)==0:
            return False
        if len(self.alias)==0:
            return False
        return True
    def IsOffline(self):
        if self.port<0:
            return True
        else:
            return False
    def SetOffline(self,flag):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurCls(vtLog.DEBUG,''%(),self)
        if flag:
            self.port=-1
        else:
            if self.port==-1:
                self.port=50000
    def SetLogin(self,usr,passwd):
        if self.IsOffline():
            return
        else:
            self.SetUsr(usr)
            self.passwd=passwd
    def Set2Cfg(self,bFlag):
        self.b2Cfg=bFlag
    def Is2Cfg(self):
        if self.b2Cfg:
            return True
        if self.IsOffline():
            if len(self.fn)<=0:
                return True
            else:
                try:
                    os.path.exists(self.fn)
                    return False
                except:
                    return True
        else:
            if len(self.host)<=0:
                return True
            if len(self.alias)<=0:
                return True
            if len(self.usr)<=0:
                return True
        return False
    def SetHostAliasStr(self,s):
        self.host,self.port,self.alias=setHostAliasStr(s)
        try:
            self.port=int(self.port)
        except:
            self.port=50000
            vtLog.vtLngTB(self.__class__.__name__)
    def GetHostConnectionStr(self):
        if self.IsOffline():
            return '---'
        return getHostConnectionStr(self.host,self.port,self.usr,self.alias)
    def SetHostConnectionStr(self,s):
        self.host,self.port,self.usr,self.alias=setHostConnectionStr(s)
        try:
            self.port=int(self.port)
        except:
            self.port=50000
            vtLog.vtLngTB(self.__class__.__name__)
