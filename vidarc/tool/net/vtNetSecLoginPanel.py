#Boa:FramePanel:vtNetSecLoginPanel
#----------------------------------------------------------------------------
# Name:         vtNetSecLoginPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060416
# CVS-ID:       $Id: vtNetSecLoginPanel.py,v 1.4 2006/08/29 10:06:24 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import sys

from vidarc.tool.net.vtNetSecLoginDialog import vtNetSecLoginDialog
from vidarc.tool.net.vtNetSecLoginHost import vtNetSecLoginHost
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_VTNETSECLOGINPANEL, wxID_VTNETSECLOGINPANELCBADD, 
 wxID_VTNETSECLOGINPANELCBDEL, wxID_VTNETSECLOGINPANELCBEDIT, 
 wxID_VTNETSECLOGINPANELLSTHOST, 
] = [wx.NewId() for _init_ctrls in range(5)]

class vtNetSecLoginPanel(wx.Panel):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbEdit, 0, border=4, flag=wx.BOTTOM)
        parent.AddWindow(self.cbDel, 0, border=4, flag=wx.BOTTOM)
        parent.AddWindow(self.cbAdd, 0, border=4, flag=0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstHost, 1, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.BOTTOM | wx.TOP | wx.LEFT)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.RIGHT | wx.LEFT)

    def _init_coll_lstHost_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT, heading=u'Appl',
              width=80)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT, heading=u'Alias',
              width=80)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT, heading=u'Host',
              width=80)
        parent.InsertColumn(col=3, format=wx.LIST_FORMAT_LEFT, heading=u'Login',
              width=60)
        parent.InsertColumn(col=4, format=wx.LIST_FORMAT_LEFT, heading=u'Lv',
              width=30)
        parent.InsertColumn(col=5, format=wx.LIST_FORMAT_LEFT,
              heading=u'Groups', width=-1)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=2, hgap=0, rows=1, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTNETSECLOGINPANEL,
              name=u'vtNetSecLoginPanel', parent=prnt, pos=wx.Point(414, 248),
              size=wx.Size(199, 136), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(191, 109))

        self.lstHost = wx.ListCtrl(id=wxID_VTNETSECLOGINPANELLSTHOST,
              name=u'lstHost', parent=self, pos=wx.Point(4, 4),
              size=wx.Size(144, 101), style=wx.LC_REPORT)
        self._init_coll_lstHost_Columns(self.lstHost)
        self.lstHost.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstHostListColClick,
              id=wxID_VTNETSECLOGINPANELLSTHOST)
        self.lstHost.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstHostListItemDeselected,
              id=wxID_VTNETSECLOGINPANELLSTHOST)
        self.lstHost.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstHostListItemSelected,
              id=wxID_VTNETSECLOGINPANELLSTHOST)

        self.cbEdit = wx.lib.buttons.GenBitmapButton(ID=wxID_VTNETSECLOGINPANELCBEDIT,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbEdit', parent=self,
              pos=wx.Point(156, 4), size=wx.Size(31, 30), style=0)
        self.cbEdit.Bind(wx.EVT_BUTTON, self.OnCbEditButton,
              id=wxID_VTNETSECLOGINPANELCBEDIT)

        self.cbDel = wx.lib.buttons.GenBitmapButton(ID=wxID_VTNETSECLOGINPANELCBDEL,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbDel', parent=self,
              pos=wx.Point(156, 38), size=wx.Size(31, 30), style=0)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VTNETSECLOGINPANELCBDEL)

        self.cbAdd = wx.lib.buttons.GenBitmapButton(ID=wxID_VTNETSECLOGINPANELCBADD,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbAdd', parent=self,
              pos=wx.Point(156, 72), size=wx.Size(31, 30), style=0)
        self.cbAdd.Bind(wx.EVT_BUTTON, self.OnCbAddButton,
              id=wxID_VTNETSECLOGINPANELCBADD)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        self._init_ctrls(parent)
        self.selIdx=-1
        self.oLogin=None
        self.dlgLogin=None
        
        self.cbAdd.SetBitmapLabel(vtArt.getBitmap(vtArt.Add))
        self.cbDel.SetBitmapLabel(vtArt.getBitmap(vtArt.Del))
        self.cbEdit.SetBitmapLabel(vtArt.getBitmap(vtArt.Edit))
        
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
        #self.fgsData.Layout()
        self.fgsData.Fit(self)
    def OnLstHostListColClick(self, event):
        self.selIdx=-1
        event.Skip()

    def OnLstHostListItemDeselected(self, event):
        self.selIdx=-1
        event.Skip()

    def OnLstHostListItemSelected(self, event):
        self.selIdx=event.GetIndex()
        event.Skip()

    def OnCbAddButton(self, event):
        if self.dlgLogin is None:
            self.dlgLogin=vtNetSecLoginDialog(self)
            self.dlgLogin.Centre()
        self.dlgLogin.SetValues('',50000,'','','','')
        iRet=self.dlgLogin.ShowModal()
        if iRet>0:
            sHost=self.dlgLogin.GetHost()
            sPort=self.dlgLogin.GetPort()
            sPasswd=self.dlgLogin.GetPasswd()
            sUsr=self.dlgLogin.GetUsr()
            sUsrId=self.dlgLogin.GetUsrId()
            sGrpIds=self.dlgLogin.GetGrpIds()
            sSecLv=self.dlgLogin.GetSecLv()
            sAppl=self.dlgLogin.GetAppl()
            sAlias=self.dlgLogin.GetAlias()
            o=vtNetSecLoginHost(sPasswd,sUsr,long(sUsrId),sGrpIds,sSecLv,
                    sHost,sPort,sAppl,sAlias)
            self.oLogin.AddHost(o)
        event.Skip()

    def OnCbEditButton(self, event):
        event.Skip()
        if self.selIdx<0:
            return
        if self.dlgLogin is None:
            self.dlgLogin=vtNetSecLoginDialog(self)
            self.dlgLogin.Centre()
        d=self.oLogin.GetHosts()
        o=d[self.selIdx]
        self.dlgLogin.SetValues(o.GetHost(),o.GetPort(),o.GetAppl(),o.GetAlias(),
                o.GetUsr(),o.GetPasswd(),False)
        iRet=self.dlgLogin.ShowModal()
        if iRet>0:
            self.oLogin.DelHost(self.selIdx)
            sHost=self.dlgLogin.GetHost()
            sPort=self.dlgLogin.GetPort()
            sPasswd=self.dlgLogin.GetPasswd()
            sUsr=self.dlgLogin.GetUsr()
            sUsrId=self.dlgLogin.GetUsrId()
            sGrpIds=self.dlgLogin.GetGrpIds()
            sSecLv=self.dlgLogin.GetSecLv()
            sAppl=self.dlgLogin.GetAppl()
            sAlias=self.dlgLogin.GetAlias()
            o=vtNetSecLoginHost(sPasswd,sUsr,long(sUsrId),sGrpIds,sSecLv,
                    sHost,sPort,sAppl,sAlias)
            self.oLogin.AddHost(o,0)
            self.__showLogin__()
    
    def OnCbDelButton(self, event):
        event.Skip()
        if self.selIdx<0:
            return
        self.oLogin.DelHost(self.selIdx)
        self.__showLogin__()
    def __showLogin__(self):
        self.lstHost.DeleteAllItems()
        try:
            d=self.oLogin.GetHosts()
            docHum=self.oLogin.GetHumanDoc()
            for o in d:
                sAppl=o.GetAppl()
                sAlias=o.GetAlias()
                idx=self.lstHost.InsertStringItem(sys.maxint,sAppl)
                sUsr=o.GetUsr()
                if o.GetUsrId()>=0:
                    tmp=docHum.getNodeByIdNum(o.GetUsrId())
                    if tmp is not None:
                        sUsr=docHum.getNodeText(tmp,'name')
                self.lstHost.SetStringItem(idx,1,sAlias)
                self.lstHost.SetStringItem(idx,2,o.GetHost())
                self.lstHost.SetStringItem(idx,3,sUsr)
                self.lstHost.SetStringItem(idx,4,'%02d'%o.GetSecLv())
                lGrps=[]
                l=o.GetGrpIds()
                for i in l:
                    tmp=docHum.getNodeByIdNum(i)
                    if tmp is not None:
                        lGrps.append(docHum.getNodeText(tmp,'name'))
                    else:
                        lGrps.append(str(i))
                lGrps.sort()
                self.lstHost.SetStringItem(idx,5,','.join(lGrps))
                
        except:
            vtLog.vtLngTB(self.GetName())
    def SetNetSecLogin(self,o=None):
        if o is None:
            o=vtNetSecLogin(self.GetName())
        self.oLogin=o
        self.__showLogin__()
    def GetNetSecLogin(self):
        return self.oLogin
    def SetDoc(self,node,bNet=False):
        try:
            self.oLogin.SetDoc(node,bNet)
        except:
            vtLog.vtLngTB(self.GetName())
    def SetNode(self,node):
        try:
            self.oLogin.SetNode(node)
            self.__showLogin__()
        except:
            vtLog.vtLngTB(self.GetName())
    def GetNode(self,node=None):
        try:
            self.oLogin.GetNode(node)
        except:
            vtLog.vtLngTB(self.GetName())
    def AddHost(self,obj):
        self.oLogin.AddHost(obj)
        self.__showLogin__()
