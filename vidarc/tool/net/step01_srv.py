#----------------------------------------------------------------------------
# Name:         step01_srv.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: step01_srv.py,v 1.1 2005/12/11 23:04:49 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import SimpleXMLRPCServer

def getServerStatus():
    return 'step01_srv.'
def replyMsg(origin):
    return 'step01_srv',origin
server=SimpleXMLRPCServer.SimpleXMLRPCServer(("localhost",8000))
server.register_function(getServerStatus)
server.register_function(replyMsg)
server.serve_forever()