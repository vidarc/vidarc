#Boa:FramePanel:vNetPlacePanel
#----------------------------------------------------------------------------
# Name:         vNetPlacePanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vNetPlacePanel.py,v 1.2 2013/12/09 08:06:54 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#from boa.xml.vtXmlDom import vtXmlDom

import wx
import sys
if sys.version_info >= (2,6,0):
    import hashlib
    def newSha():
        return hashlib.sha1()
else:
    import sha
    def newSha():
        return sha.new()

[wxID_VNETPLACEPANEL, wxID_VNETPLACEPANELLBLALIAS, wxID_VNETPLACEPANELLBLAPPL, 
 wxID_VNETPLACEPANELLBLHOST, wxID_VNETPLACEPANELLBLPASSWD, 
 wxID_VNETPLACEPANELLBLPORT, wxID_VNETPLACEPANELLBLUSR, 
 wxID_VNETPLACEPANELTXTALIAS, wxID_VNETPLACEPANELTXTAPPL, 
 wxID_VNETPLACEPANELTXTHOST, wxID_VNETPLACEPANELTXTPASSWD, 
 wxID_VNETPLACEPANELTXTPORT, wxID_VNETPLACEPANELTXTUSR, 
] = [wx.NewId() for _init_ctrls in range(13)]

class vNetPlacePanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VNETPLACEPANEL, name=u'vNetPlacePanel',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(291, 115),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(283, 81))

        self.lblAppl = wx.StaticText(id=wxID_VNETPLACEPANELLBLAPPL,
              label=u'Appl', name=u'lblAppl', parent=self, pos=wx.Point(8, 8),
              size=wx.Size(21, 13), style=0)

        self.txtAppl = wx.TextCtrl(id=wxID_VNETPLACEPANELTXTAPPL,
              name=u'txtAppl', parent=self, pos=wx.Point(40, 4),
              size=wx.Size(80, 21), style=wx.TE_READONLY, value=u'')

        self.lblAlias = wx.StaticText(id=wxID_VNETPLACEPANELLBLALIAS,
              label=u'Alias', name=u'lblAlias', parent=self, pos=wx.Point(152,
              8), size=wx.Size(22, 13), style=0)

        self.txtAlias = wx.TextCtrl(id=wxID_VNETPLACEPANELTXTALIAS,
              name=u'txtAlias', parent=self, pos=wx.Point(176, 4),
              size=wx.Size(100, 21), style=0, value=u'')

        self.lblHost = wx.StaticText(id=wxID_VNETPLACEPANELLBLHOST,
              label=u'Host', name=u'lblHost', parent=self, pos=wx.Point(8, 32),
              size=wx.Size(22, 13), style=0)

        self.txtHost = wx.TextCtrl(id=wxID_VNETPLACEPANELTXTHOST,
              name=u'txtHost', parent=self, pos=wx.Point(40, 28),
              size=wx.Size(80, 21), style=0, value=u'')

        self.lblPort = wx.StaticText(id=wxID_VNETPLACEPANELLBLPORT,
              label=u'Port', name=u'lblPort', parent=self, pos=wx.Point(152,
              32), size=wx.Size(19, 13), style=0)

        self.txtPort = wx.TextCtrl(id=wxID_VNETPLACEPANELTXTPORT,
              name=u'txtPort', parent=self, pos=wx.Point(176, 28),
              size=wx.Size(100, 21), style=0, value=u'')

        self.lblUsr = wx.StaticText(id=wxID_VNETPLACEPANELLBLUSR, label=u'User',
              name=u'lblUsr', parent=self, pos=wx.Point(8, 56), size=wx.Size(22,
              13), style=0)

        self.txtUsr = wx.TextCtrl(id=wxID_VNETPLACEPANELTXTUSR, name=u'txtUsr',
              parent=self, pos=wx.Point(40, 52), size=wx.Size(80, 21), style=0,
              value=u'')

        self.lblPasswd = wx.StaticText(id=wxID_VNETPLACEPANELLBLPASSWD,
              label=u'Password', name=u'lblPasswd', parent=self,
              pos=wx.Point(128, 56), size=wx.Size(46, 13), style=0)

        self.txtPasswd = wx.TextCtrl(id=wxID_VNETPLACEPANELTXTPASSWD,
              name=u'txtPasswd', parent=self, pos=wx.Point(176, 52),
              size=wx.Size(100, 21), style=wx.TE_PASSWORD, value=u'')
        self.txtPasswd.Bind(wx.EVT_TEXT, self.OnTxtPasswdText,
              id=wxID_VNETPLACEPANELTXTPASSWD)

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        self.Move(pos)
        self.doc=None
        self.node=None
        self.passwd=''
    def __clear__(self):
        self.txtAppl.SetValue('')
        self.txtAlias.SetValue('')
        self.txtHost.SetValue('')
        self.txtPort.SetValue('')
        self.txtUsr.SetValue('')
        self.txtPasswd.SetValue('')
    def Set(self,appl,alias,host,port,usr,passwd):
        self.txtAppl.SetValue(appl)
        self.txtAlias.SetValue(alias)
        self.txtHost.SetValue(host)
        self.txtPort.SetValue(port)
        self.txtUsr.SetValue(usr)
        self.txtPasswd.SetValue('')
        if len(passwd)>0:
            self.passwd=passwd
        else:
            self.passwd=None
    def SetNode(self,doc,node,appl):
        self.doc=doc
        if self.doc is None:
            self.__clear__()
        self.node=node
        if self.node is None:
            self.__clear__()
        passwd=self.doc.getNodeText(node,'passwd')
        if len(appl)<=0:
            self.txtAppl.SetValue(self.doc.getNodeText(node,'appl'))
        else:
            self.txtAppl.SetValue(appl)
        self.txtAlias.SetValue(self.doc.getNodeText(node,'alias'))
        self.txtHost.SetValue(self.doc.getNodeText(node,'host'))
        self.txtPort.SetValue(self.doc.getNodeText(node,'port'))
        self.txtUsr.SetValue(self.doc.getNodeText(node,'usr'))
        self.txtPasswd.SetValue('')
        if len(passwd)>0:
            self.passwd=passwd
        else:
            self.passwd=None
    def GetNode(self):
        if self.doc is None:
            return
        if self.node is None:
            return
        self.doc.setNodeText(self.node,'appl',self.txtAppl.GetValue())
        self.doc.setNodeText(self.node,'alias',self.txtAlias.GetValue())
        self.doc.setNodeText(self.node,'host',self.txtHost.GetValue())
        self.doc.setNodeText(self.node,'port',self.txtPort.GetValue())
        self.doc.setNodeText(self.node,'usr',self.txtUsr.GetValue())
        self.doc.setNodeText(self.node,'passwd',self.GetPasswd())
    def GetAppl(self):
        return self.txtAppl.GetValue()
    def GetAlias(self):
        return self.txtAlias.GetValue()
    def GetHost(self):
        return self.txtHost.GetValue()
    def GetPort(self):
        return self.txtPort.GetValue()
    def GetUsr(self):
        return self.txtUsr.GetValue()
    def GetPasswd(self):
        if self.passwd is None:
            passwd=self.txtPasswd.GetValue()
            if len(passwd)>0:
                m=newSha()#sha.new()
                m.update(passwd)
                return m.hexdigest()
            else:
                return ''
        else:
            return self.passwd
    def OnTxtPasswdText(self, event):
        self.passwd=None
        event.Skip()
