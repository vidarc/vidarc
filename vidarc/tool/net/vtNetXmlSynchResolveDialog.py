#----------------------------------------------------------------------------
# Name:         vtNetXmlSynchResolveDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20070831
# CVS-ID:       $Id: vtNetXmlSynchResolveDialog.py,v 1.3 2007/10/15 21:30:20 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

from vidarc.tool.misc.vtmCompareItemViewDialog import vtmCompareItemViewDialog
#from vidarc.tool.misc.vtmCompareResults import vtmCompareResults
from vidarc.tool.vtThread import vtThread

from vidarc.tool.vtThreadCore import getLock

from vidarc.tool.net.vtNetXmlSynchResolve import vtNetXmlSynchResolve
from vidarc.tool.net.vtNetXmlSynchResolvePanel import vtNetXmlSynchResolvePanel
from vidarc.tool.net.vtNetXmlSynchShowEntryPanel import vtNetXmlSynchShowEntryPanel

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

class vtNetXmlSynchResolveDialog(vtmCompareItemViewDialog):
    def __init__(self,parent,sTitle):
        vtmCompareItemViewDialog.__init__(self,parent,
                    vtNetXmlSynchResolvePanel,'pnRsolve',sTitle)
        self.sTitle=sTitle
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        self.bxsBt.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        self.cbSynch = wx.lib.buttons.GenBitmapTextButton(ID=-1,
              bitmap=vtArt.getBitmap(vtArt.Synch), name=u'cbSynch',
              label=_(u'Synch'),
              parent=self, pos=wx.Point(259, 112), size=wx.Size(31, 30),
              style=0)
        self.cbSynch.SetMinSize(wx.Size(-1, -1))
        self.cbSynch.Bind(wx.EVT_BUTTON, self.OnCbSynchButton)
        self.bxsBt.AddWindow(self.cbSynch, 0, border=4, flag=wx.LEFT | wx.EXPAND)
        
        self.semResolve=getLock()
        self.dResolve={}
        pn=self.GetPanel()
        self.oSynchResolve=vtNetXmlSynchResolve()
        pn.SetCompareObj(self.oSynchResolve)
        pn.SetShowPanelX(vtNetXmlSynchShowEntryPanel,
                    u'vtNetXmlSynchShowEntryClient',
                    _(u'vtNet Sycnh Resolve:local entry'),
                    self.__funcShowEntryClt)
        pn.SetShowPanelY(vtNetXmlSynchShowEntryPanel,
                    u'vtNetXmlSynchShowEntryServer',
                    _(u'vtNet Sycnh Resolve:server entry'),
                    self.__funcShowEntryClt)
        pn.dlgShowX.SetSize((400,300))
        pn.dlgShowY.SetSize((400,300))
        wx.CallAfter(self.Bind,wx.EVT_CLOSE, self.OnClose)
    def acquire(self):
        self.semResolve.acquire()
    def release(self):
        self.semResolve.release()
    def SetNotify(self,doc,appl,dNotify,dProc,func,*args,**kwargs):
        self.dResolve[appl]=[doc,dNotify,dProc,func,args,kwargs]
        #self.acquire()
        pn=self.GetPanel()
        self.oSynchResolve.SetNotify(self.dResolve[appl])
        self.oSynchResolve.Compare()
        self.SetTitle(u''.join([self.sTitle,u' (',appl,u')']))
        pn.UpdateResult()
    def SetResolved(self,appl):
        if appl in self.dResolve:
            del self.dResolve[appl]
        self.oSynchResolve.SetNotify(None)
        pn=self.GetPanel()
        pn.ClearResult()
        #self.release()
    def __funcShowEntryClt(self,id):
        doc=self.oSynchResolve.GetDoc()
        n=doc.getNodeByIdNum(long(id))
        pn=self.GetPanel()
        pnView=pn.dlgShowX.GetPanel()
        pnView.ShowNode(doc,n)
        pass
    def __funcShowEntrySrv(self,id):
        pass
    #def OnCbApplyButton(self,evt):
    #    pass
    #def OnCbCancelButton(self,evt):
    #    pass
    def OnCbSynchButton(self,evt):
        evt.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.INFO,''%(),self)
            #pn=self.GetPanel()
            #pn.Synch()
            self.oSynchResolve.Synch()
            self.release()
            #self.oSynchResolve.SetNotify(None)
            pn=self.GetPanel()
            pn.ClearResult()
            #self.dResolve={}
            if self.IsModal():
                self.EndModal(0)
            else:
                self.Show(False)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnClose(self,evt):
        if evt.CanVeto():
            if len(self.dResolve)>0:
                evt.Veto()
            else:
                evt.Skip()
                try:
                    self.release()
                except:
                    pass
        else:
            vtLog.vtLngCurWX(vtLog.WARN,'closing forced',self)
            evt.Skip()
    def OnCbCancelButton(self,evt):
        evt.Skip()
        try:
            #self.pn.Close()
            #self.pn.Cancel()
            self.oSynchResolve.Synch()
            self.release()
            #self.oSynchResolve.SetNotify(None)
            pn=self.GetPanel()
            pn.ClearResult()
            if self.IsModal():
                self.EndModal(0)
            else:
                #self.Close()
                self.Show(False)
        except:
            vtLog.vtLngTB(self.GetName())
