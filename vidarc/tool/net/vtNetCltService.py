#----------------------------------------------------------------------------
# Name:         vNetCltService.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtNetCltService.py,v 1.1 2005/12/11 23:04:49 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import time

from vidarc.tool.net.vtNetSrv import vtServiceCltSock
from vidarc.tool.net.vtNetClt import vtNetClt

if __name__=='__main__':
    cltServ=vtNetClt(None,'',50008,vtServiceCltSock,1)
    cltServ.Connect()
    srvSock=cltServ.GetSocketInstance()
    
    #srvSock.Login()
    srvSock.GetConnections()
    while srvSock.IsLoggedIn()==False:
        time.sleep(1)
    srvSock.GetConnections()
    srvSock.GetDataConnections()
    time.sleep(1)
    #srvSock.ShutDown()
    #time.sleep(1)
    cltServ.Close()
    