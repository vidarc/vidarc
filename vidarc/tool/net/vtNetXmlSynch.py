#----------------------------------------------------------------------------
# Name:         vtNetXmlSynch.py
# Purpose:      
#               based on vNetXmlSynch.py
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtNetXmlSynch.py,v 1.20 2012/01/29 13:13:40 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import types,time,sys
import os,threading,thread,traceback,copy
import vidarc.tool.InOut.fnUtil as fnUtil
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.vtThread import vtThread
from vNetXmlSynchProc import vNetXmlSynchProc

class vtNetXmlSynch(vtThread):
    TIME_SLEEP=0.05
    TIME_UPDATE=20
    TIME_TIMEOUT=30
    TIME_ABORT=180
    MAX_PENDING=100
    def __init__(self,doc=None,par=None,bPost=False,verbose=0,origin=None):
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        vtThread.__init__(self,par,bPost=bPost,verbose=verbose,origin=origin)
        self.busy=False
        self.doc=doc
        self.semProc=self.getLock()#threading.Semaphore()
        # multi lang trick
        self.dNotifyTrans={}
        self.dNotifyTrans['edit']=_(u'edit')
        self.dNotifyTrans['edit_fault']=_(u'edit_fault')
        self.dNotifyTrans['edit_offline']=_(u'edit_offline')
        self.dNotifyTrans['edit_resolve']=_(u'edit_resolve')
        self.dNotifyTrans['add']=_(u'add')
        self.dNotifyTrans['add_fault']=_(u'add_fault')
        self.dNotifyTrans['add_offline']=_(u'add_offline')
        self.dNotifyTrans['move']=_(u'move')
        self.dNotifyTrans['move_fault']=_(u'move_fault')
        self.dNotifyTrans['del']=_(u'del')
        self.dNotifyTrans['del_fault']=_(u'del_fault')
        self.dNotifyTrans['ident']=_(u'identical')
        self.PHASE=[
            'getKeys',
            'add_offline',
            'proc',
            'del',
            'move']
        self.Clear()
    def Clear(self):
        self.keys=None
        self.iKeyCount=0
        self.iTimeOut=10
        self.iReqCount=100
        self.iReqOpen=0
        self.iReqRestart=25
        self.iReqAct=0
        self.docSynch=None
        self.synchFN=''
        self.bChanged=False
        self.bAllRemoteGot=False
        self.iAct=0
        self.iCount=100
        self.iKeyCount=0
        self.iTime=0
        self.sActKind=''
        self.lstOffline={}
        self.lstOffCont={}
        self.lstOffLineAdd=None
        self.offKeys=[]
        self.lstAdd=[]
        self.lstMove=[]
        self.dProc=None
        self.dNotify={'edit':[],'edit_fault':[],'edit_offline':[],
                'edit_resolve':[],
                'add':[],'add_fault':[],'add_offline':[],
                'move':[],'move_fault':[],
                'del':[],'del_fault':[],'del_offline':[],
                'ident':[]}
    def SetPostWid(self,par,bPost=False):
        #if self.IsRunning():
        #    vtLog.vtLngCur(vtLog.CRITICAL,'wait until sync is inactive'%(),self.GetOrigin())
        #    return
        vtThread.SetPostWid(self,par,bPost=bPost)
        self.doc=par
        #vtLog.CallStack('')
        #print self.docSynch
    def acquire(self):
        #vtLog.vtLngCS(vtLog.DEBUG,'semaphore acquire',self.origin)
        self.semProc.acquire()
    def release(self):
        #vtLog.vtLngCS(vtLog.DEBUG,'semaphore released',self.origin)
        self.semProc.release()
    def Synch(self):
        #vtLog.CallStack('')
        if self.IsRunning():
            vtLog.vtLngCur(vtLog.ERROR,'already running',self.origin)
            return
        if self.verbose>0:
            vtLog.vtLngCur(vtLog.INFO,'',self.origin)
        self.Do(self.doSynch)
    def SetKeys(self,sKeys):
        if self.verbose>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'sKeys:%s'%(sKeys),self.origin)
        #self.keys=keys.split(',')
        zStart=time.clock()
        docIds=self.doc.getIds()
        #self.docIds=self.doc.getIds()
        docKeys=docIds.GetIDs()
        keys=[]
        self.iCount=0
        try:
            docKeys.sort()
            
            #print docIds
            #print self.docIds.GetIDs()
            self.acquire()
            self.dProc={
                'add2srv':[],'add2loc':[],'synch2loc':[],'synch2srv':[],
                'moveAtSrv':[],'moveAtLoc':[],
                'delAtSrv':[],'delAtLoc':[],
                'resolve':[],
                }
            keyFPs=sKeys.split(',')
            s=keyFPs[0].split(';')
            key=long(s[0])
            n=self.doc.getBaseNode()
            sTagLoc=self.doc.getTagName(n)
            if self.doc.getFingerPrint(n)!=s[1]:
                keys.append(s[0])
                self.dProc['synch2loc'].append(long(s[0]))
            if sTagLoc!=s[2]:
                # consider abort base node tagnames dont match
                vtLog.vtLngCur(vtLog.CRITICAL,
                        'base node missmatch(id:%s);tag(loc):%s;tag(rmt):%s'%(
                        key,sTagLoc,s[2]),
                        self.origin)
                self.__close__()
                return
            self.keys2Del=[]
            fps=docIds.GetFPs()
            offIds=docIds.GetOfflineIds()
            #offContIds=self.doc.getIds().GetOfflineContentIds()
            offIds.sort()
            #offContIds.sort()
            bDbg=False
            if self.verbose>0:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'fps:%s'%(vtLog.pformat(fps)),self.origin)
                    vtLog.vtLngCur(vtLog.DEBUG,'offIds:%s'%(vtLog.pformat(offIds)),self.origin)
                    #vtLog.vtLngCur(vtLog.DEBUG,'offContIds:%s'%(vtLog.pformat(offContIds)),self.origin)
                    bDbg=True
            iAct=0
            bAction=True
            for keyFP in keyFPs[1:]:
                s=keyFP.split(';')
                try:
                    key=long(s[0])
                    keys.append(key)
                    if key not in docKeys:
                        # remote present local missing -> add 2 local
                        if bDbg:
                            vtLog.vtLngCur(vtLog.DEBUG,'kind:add2loc;id:%08d;key:%s;fp:%s;tag:%s'%(key,s[0],s[1],s[2]),self.origin)
                        self.dProc['add2loc'].append(key)
                        self.iCount+=1
                        continue
                    n=self.doc.getNodeByIdNum(key)
                    sTagLoc=self.doc.getTagName(n)
                    # ++++ 061003 wro
                    if key in offIds:
                        if docIds.IsOfflineDel(key):
                            if sTagLoc!=s[2]:
                                # tagname differ
                                if bDbg:
                                    vtLog.vtLngCur(vtLog.DEBUG,'kind:delAtSrv;id:%08d;key:%s;fp:%s;tag:%s'%(key,s[0],s[1],s[2]),self.origin)
                                self.dProc['delAtLoc'].append(key)
                                self.iCount+=1
                            else:
                                # node deleted on local -> del at srv
                                if fps[key]!=s[1]:
                                    # fingerprint missmatch
                                    # node deleted on remote and local -> change local Id
                                    if bDbg:
                                        vtLog.vtLngCur(vtLog.DEBUG,'kind:resolve;id:%08d;key:%s;fp:%s;tag:%s'%(key,s[0],s[1],s[2]),self.origin)
                                    self.dProc['resolve'].append(key)
                                else:
                                    if bDbg:
                                        vtLog.vtLngCur(vtLog.DEBUG,'kind:delAtSrv;id:%08d;key:%s;fp:%s;tag:%s'%(key,s[0],s[1],s[2]),self.origin)
                                    self.dProc['delAtSrv'].append(key)
                                self.iCount+=1
                        elif docIds.IsOfflineAdd(key):
                            # new node added on local -> add 2 srv
                            if sTagLoc!=s[2]:
                                # new node added on remote and local -> change local Id
                                vtLog.vtLngCur(vtLog.INFO,
                                        'id:%08d;tag(loc):%s;tag(rmt):%s'%(
                                        key,sTagLoc,s[2]),
                                        self.origin)
                                if bDbg:
                                    vtLog.vtLngCur(vtLog.DEBUG,'kind:add2loc;id:%08d;key:%s;fp:%s;tag:%s'%(key,s[0],s[1],s[2]),self.origin)
                                self.dProc['add2loc'].append(key)
                                self.iCount+=1
                            elif fps[key]!=s[1]:
                                # new node added on remote and local -> change local Id
                                if bDbg:
                                    vtLog.vtLngCur(vtLog.DEBUG,'kind:add2loc;id:%08d;key:%s;fp:%s;tag:%s'%(key,s[0],s[1],s[2]),self.origin)
                                self.dProc['add2loc'].append(key)
                                self.iCount+=1
                            self.dProc['add2srv'].append(key)
                            self.iCount+=1
                        elif docIds.IsOfflineEdit(key):
                            if sTagLoc!=s[2]:
                                vtLog.vtLngCur(vtLog.ERROR,
                                        'id:%08d;tag(loc):%s;tag(rmt):%s'%(
                                        key,sTagLoc,s[2]),
                                        self.origin)
                                if bDbg:
                                    vtLog.vtLngCur(vtLog.DEBUG,'kind:resolve;id:%08d;key:%s;fp:%s;tag:%s'%(key,s[0],s[1],s[2]),self.origin)
                                self.dProc['resolve'].append(key)
                                self.dNotify['edit_resolve'].append(key)
                                self.iCount+=1
                            elif fps[key]!=s[1]:
                                if self.doc.IsNodeAclOk(n,self.doc.ACL_MSK_WRITE):
                                    # different fingerprint synch this node
                                    # but add edited local -> resolve manually
                                    if bDbg:
                                        vtLog.vtLngCur(vtLog.DEBUG,'kind:resolve;id:%08d;key:%s;fp:%s;tag:%s'%(key,s[0],s[1],s[2]),self.origin)
                                    self.dProc['resolve'].append(key)
                                    self.dNotify['edit_resolve'].append(key)
                                    self.iCount+=1
                                else:
                                    # no permission to edit, so get remote
                                    self.doc.clearFingerPrint(n)
                                    docIds.__clrOffline__(key,'edit',self.doc)
                                    if bDbg:
                                        vtLog.vtLngCur(vtLog.DEBUG,'kind:synch2loc;id:%08d;key:%s;fp:%s;tag:%s'%(key,s[0],s[1],s[2]),self.origin)
                                    self.dProc['synch2loc'].append(key)
                                    self.iCount+=1
                            else:
                                if self.doc.IsNodeAclOk(n,self.doc.ACL_MSK_WRITE):
                                    # perfect write 2 srv
                                    if bDbg:
                                        vtLog.vtLngCur(vtLog.DEBUG,'kind:synch2srv;id:%08d;key:%s;fp:%s;tag:%s'%(key,s[0],s[1],s[2]),self.origin)
                                    self.dProc['synch2srv'].append(key)
                                    self.iCount+=1
                                else:
                                    # no permission to edit, so get remote
                                    self.doc.clearFingerPrint(n)
                                    docIds.__clrOffline__(key,'edit',self.doc)
                                    if bDbg:
                                        vtLog.vtLngCur(vtLog.DEBUG,'kind:synch2loc;id:%08d;key:%s;fp:%s;tag:%s'%(key,s[0],s[1],s[2]),self.origin)
                                    self.dProc['synch2loc'].append(key)
                                    self.iCount+=1
                        if docIds.IsOfflineMove(key):
                            if sTagLoc!=s[2]:
                                # tagnames differ
                                if self.doc.IsNodeAclOk(n,self.doc.ACL_MSK_ADD):
                                    # perfect write 2 srv
                                    if bDbg:
                                        vtLog.vtLngCur(vtLog.DEBUG,'kind:add2srv;id:%08d;key:%s;fp:%s;tag:%s'%(key,s[0],s[1],s[2]),self.origin)
                                    self.dProc['add2srv'].append(key)
                                    self.iCount+=1
                                else:
                                    if bDbg:
                                        vtLog.vtLngCur(vtLog.DEBUG,'kind:resolve;id:%08d;key:%s;fp:%s;tag:%s'%(key,s[0],s[1],s[2]),self.origin)
                                    self.dProc['resolve'].append(key)
                                    self.dNotify['add_fault'].append(key)
                                    self.iCount+=1
                            else:
                                # move remote
                                idPar=self.doc.getKeyNum(self.doc.getParent(n))
                                if bDbg:
                                    vtLog.vtLngCur(vtLog.DEBUG,'kind:moveAtSrv;id:%08d;key:%s;fp:%s;tag:%s'%(key,s[0],s[1],s[2]),self.origin)
                                self.dProc['moveAtSrv'].append((key,idPar,long(s[3])))
                                self.iCount+=1
                            #if self.doc.getKeyNum(self.doc.getParent(n))!=long(s[3]):
                                # resolve manually
                            #    if bDbg:
                            #        vtLog.vtLngCur(vtLog.DEBUG,'kind:resolve;id:%08d;key:%s;fp:%s;tag:%s'%(key,s[0],s[1],s[2]),self.origin)
                            #    self.dProc['resolve'].append(key)
                            #    self.dNotify['edit_resolve'].append(key)
                            #    self.iCount+=1
                            #else:
                                # move remote
                                # resolve by user
                            #    if bDbg:
                            #        vtLog.vtLngCur(vtLog.DEBUG,'kind:moveAtSrv;id:%08d;key:%s;fp:%s;tag:%s'%(key,s[0],s[1],s[2]),self.origin)
                            #    self.dProc['moveAtSrv'].append(key)
                            #    self.iCount+=1
                    # 070202: wro 
                    #       process offline content modification normal
                    #if key in offContIds:   
                    #    continue
                    # ----
                    elif key in fps:
                        if sTagLoc!=s[2]:
                            # diffent tagname
                            vtLog.vtLngCur(vtLog.ERROR,
                                    'id:%08d;tag(loc):%s;tag(rmt):%s'%(
                                    key,sTagLoc,s[2]),
                                    self.origin)
                            # change local id, handle like offline added
                            if bDbg:
                                vtLog.vtLngCur(vtLog.DEBUG,'kind:add2srv;id:%08d;key:%s;fp:%s;tag:%s'%(key,s[0],s[1],s[2]),self.origin)
                            self.dProc['add2srv'].append(key)
                            self.iCount+=1
                            continue
                            pass
                        if fps[key]!=s[1]:
                            # different fingerprint synch this node
                            # but not edited local -> get from remote
                            if bDbg:
                                vtLog.vtLngCur(vtLog.DEBUG,'kind:synch2loc;id:%08d;key:%s;fp:%s;tag:%s'%(key,s[0],s[1],s[2]),self.origin)
                            self.dProc['synch2loc'].append(key)
                            self.iCount+=1
                        if self.doc.getKeyNum(self.doc.getParent(n))!=long(s[3]):
                            # move it
                            if docIds.IsOfflineMove(n):
                                # move remote
                                # resolve by used
                                if bDbg:
                                    vtLog.vtLngCur(vtLog.DEBUG,'kind:moveAtSrv;id:%08d;key:%s;fp:%s;tag:%s'%(key,s[0],s[1],s[2]),self.origin)
                                self.dProc['moveAtSrv'].append(key)
                                self.iCount+=1
                            else:
                                # move local
                                if bDbg:
                                    vtLog.vtLngCur(vtLog.DEBUG,'kind:moveAtLoc;id:%08d;key:%s;fp:%s;tag:%s'%(key,s[0],s[1],s[2]),self.origin)
                                self.dProc['moveAtLoc'].append((key,long(s[3])))
                                self.iCount+=1
                    else:
                        # you are not supposed to be here, never
                        vtLog.vtLngCur(vtLog.ERROR,
                                    'id:%08d;tag(loc):%s;tag(rmt):%s'%(
                                    key,sTagLoc,s[2]),
                                    self.origin)
                        #keys.append(s[0])
                        #self.iCount+=1
                    #else:
                    #    self.keys2Del.append(key)
                except:
                    vtLog.vtLngCur(vtLog.DEBUG,'keyFP:%s'%(keyFP),self.origin)
                    vtLog.vtLngTB(self.origin)
            for k in docKeys:
                if k in keys:
                    pass
                else:
                    # node is 2 delete, properly due changed security
                    if docIds.IsOfflineDel(k):
                        if docIds.IsOfflineAdd(k):
                            if k not in self.dProc['delAtLoc']:
                                # node offline added and deleted
                                self.dProc['delAtLoc'].append(k)
                                self.iCount+=1
                        else:
                            # delete loc
                            if k not in self.dProc['delAtLoc']:
                                if bDbg:
                                    vtLog.vtLngCur(vtLog.DEBUG,'kind:delAtLoc;id:%08d'%(key),self.origin)
                                self.dProc['delAtSrv'].append(k)
                                self.iCount+=1
                    elif docIds.IsOfflineAdd(k):
                        if k not in self.dProc['add2srv']:
                            if bDbg:
                                vtLog.vtLngCur(vtLog.DEBUG,'kind:add2Srv;id:%08d;'%(key),self.origin)
                            self.dProc['add2srv'].append(k)
                            self.iCount+=1
                    else:
                        # node is 2 delete, properly due changed security
                        self.dProc['delAtLoc'].append(k)
                        self.iCount+=1
            #if len(keys)==0:
            #    self.bAllRemoteGot=True
            #self.keys=keys
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'time elapsed:%f'%(time.clock()-zStart),self.origin)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'dProc:%s'%(vtLog.pformat(self.dProc)),self.origin)
            self.__sortIdByLevel__('add2srv')
            #self.__sortIdByLevel__('synch2srv')
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'time elapsed:%f'%(time.clock()-zStart),self.origin)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'dProc:%s'%(vtLog.pformat(self.dProc)),self.origin)
        except:
            vtLog.vtLngTB(self.origin)
            self.Stop()
        self.release()
    def __showReqInfo__(self):
        print self.iAct,self.iKeyCount,self.bAllRemoteGot,self.iReqOpen,self.iReqCount,self.iReqRestart
    def __doEdit__(self,docNode,synchNode):
        if self.Is2Stop():
            return
        try:
            self.doc.clearFingerPrint(docNode)
            #vtLog.CallStack('')
            #print 'docNode',docNode
            #print 'synchNode',synchNode
            #self.doc.acquire()
            iEdit=self.doc.__synchEdit__(docNode,synchNode)
            self.docSynch.AddProcResponse(self.iAct-self.iReqOpen,docNode,synchNode,'edit',iEdit)
            #self.docSynch.SetProcResponseEditResult(iEdit)
            #self.doc.release()
            if iEdit==1:
                if self.verbose>0:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCS(vtLog.DEBUG,
                            'synch proc response edit id:%s'%(self.doc.getKey(docNode)),
                            origin=self.origin)
                #self.dNotify['edit'].append(id)
                self.doc.clearFingerPrint(docNode)
                self.doc.GetFingerPrintAndStore(docNode)
                self.iCount+=1
            elif iEdit==2:
                if self.verbose>0:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCS(vtLog.DEBUG,
                            'synch proc response edit offline id:%s'%(self.doc.getKey(docNode)),
                            origin=self.origin)
                #self.dNotify['edit_offline'].append(docNode)
                self.iCount+1
            elif iEdit==0:
                if self.verbose>0:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCS(vtLog.DEBUG,
                            'synch proc response identical id:%s'%(self.doc.getKey(docNode)),
                            origin=self.origin)
                self.doc.clearFingerPrint(docNode)
                self.doc.GetFingerPrintAndStore(docNode)
                pass
            else:
                if self.verbose>0:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCS(vtLog.DEBUG,
                            'synch proc response edit fault id:%s'%(self.doc.getKey(docNode)),
                            origin=self.origin)
                #self.dNotify['edit_fault'].append(id)
                self.iCount+=1
        except:
            vtLog.vtLngTB(self.origin)
    def __procResponse__(self,parID,id,level,synchNode):
        try:
            bLogStatus=False
            if self.verbose>5:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    bLogStatus=True
            if self.verbose>5:
                if vtLog.vtLngIsLogged(vtLog.INFO):
                    vtLog.vtLngCur(vtLog.INFO,
                            'id:%s;parID:%s;level:%s;'%\
                            (id,parID,level),origin=self.origin)
            self.acquire()
            if bLogStatus:
                vtLog.vtLngCur(vtLog.DEBUG,'kind:%s'%(self.sActKind),self.origin)
            #l=self.dProc['synch2loc']
            l=self.dProc[self.sActKind]
            k=long(id)
            if k in l:
                self.iPending-=1
                if self.iPending<0:
                    vtLog.vtLngCur(vtLog.CRITICAL,'pending:%d'%(self.iPending),origin=self.origin)
                    self.iPending=0
                if bLogStatus:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'pending:%d'%(self.iPending),origin=self.origin)
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,
                                'iAct:%d-%d iOpen:%d'%\
                                (self.iAct,self.iCount,self.iReqOpen),origin=self.origin)
                try:
                    if self.sActKind=='synch2loc':
                        if k==-1:#k<0:
                            docNode=self.doc.getBaseNode()
                        else:
                            docNode=self.doc.getNodeByIdNum(k)
                        if synchNode is not None:
                            self.__doEdit__(docNode,synchNode)
                        else:
                            self.docSynch.AddProcResponse(self.iAct,docNode,None,'identical',1)
                            #self.docSynch.SetProcResponseIdentical(1)
                            vtLog.vtLngCS(vtLog.DEBUG,'id:%s identical fp'%id,origin=self.origin)
                            vtLog.vtLngCur(vtLog.WARN,'id:%s identical fp;you are not supposed to be here'%(id),self.origin)
                    elif self.sActKind=='add2loc':
                        docNode=self.doc.getNodeByIdNum(k)
                        if docNode is None:
                            iAdd=self.doc.__synchAdd__(parID,synchNode)
                            if iAdd>0:
                                self.docSynch.AddProcResponse(self.iAct,None,synchNode,'add',iAdd,parID)
                            else:
                                self.docSynch.AddProcResponse(self.iAct,None,synchNode,'add',iAdd,parID)
                                if bLogStatus:
                                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                        vtLog.vtLngCur(vtLog.DEBUG,
                                                'id:%s;parID:%s'%(id,parID),
                                                origin=self.origin)
                                self.dNotify['add_fault'].append(k)
                        else:
                            vtLog.vtLngCur(vtLog.ERROR,'id:%08d is already present'%(k),self.origin)
                            self.dNotify['add_fault'].append(k)
                except:
                    vtLog.vtLngTB(self.origin)
                l.remove(k)
                self.iTime=0
                self.iAct+=1
                #self.__proc__('synch2loc',self.__doSynch2Loc__,bAcquire=False)
                if self.sActKind=='synch2loc':
                    self.release()
                    self.__proc__(self.sActKind,False,self.__doSynch2Loc__)
                    return
                elif self.sActKind=='add2loc':
                    self.release()
                    self.__proc__(self.sActKind,False,self.__doAdd2Loc__)
                    return
            else:
                vtLog.vtLngCur(vtLog.WARN,'sKind:%s;id:%s not in request list'%(
                        self.sActKind,id),self.origin)
                if bLogStatus:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'request list:%s'%(
                                self.dProc[self.sActKind]),self.origin)
        except:
            vtLog.vtLngTB(self.origin)
        self.release()
    def __doDelAtLoc__(self,iAct,id):
        if self.Is2Stop():
            return
        bLogDbg=False
        if self.verbose>5:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                bLogDbg=True
        if bLogDbg:
            vtLog.vtLngCur(vtLog.DEBUG,'iAct:%d;id:%08d'%(iAct,id),self.origin)
        try:
            #self.iPending+=1
            #l=self.dProc['delAtLoc']
            #for id in l:
            if 1:
                #self.iAct+=1
                node=self.doc.getNodeByIdNum(id)
                if node is None:
                    #self.iPending-=1
                    # node was deleted in the mean while, just fine
                    vtLog.vtLngCur(vtLog.WARN,
                            'id:%08d scheduled to delAtLoc, but is alreay deleted'%(
                            id),self.origin)
                    #continue
                    return 0
                self.docSynch.AddProcResponse(iAct,node,None,'del',0)
                iRes=self.doc.__synchDel__(node)
                if bLogDbg:
                    vtLog.vtLngCur(vtLog.DEBUG,'id:%08d;iRes:%d'%(id,iRes),
                                        origin=self.origin)
                if iRes>0:
                    self.dNotify['del'].append(id)
                else:
                    self.iPending-=1
                    self.dNotify['del_fault'].append(id)
            #l=[]
        except:
            #self.iPending-=1
            vtLog.vtLngTB(self.origin)
            #try:
            #    i=l.index(id)
            #    l=l[i:]
            #except:
            #    pass
        #self.dProc['delAtLoc']=l
        #self.release()
        #if bAcquire:
        #    self.doc.release('dom')
        return 0
    def __doDelAtSrv__(self,iAct,id):
        if self.Is2Stop():
            return
        bLogDbg=False
        if self.verbose>5:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                bLogDbg=True
        if bLogDbg:
            vtLog.vtLngCur(vtLog.DEBUG,'iAct:%d;id:%08d'%(iAct,id),self.origin)
        node=self.doc.getNodeByIdNum(id)
        if node is None:
            # node was deleted in the mean while, just fine
            vtLog.vtLngCur(vtLog.WARN,
                    'id:%08d scheduled to delAtLoc, but is alreay deleted'%(
                    id),self.origin)
            return 0
        try:
            self.iPending+=1
            #self.doc.delNode(node)
            self.doc.delNode2Srv(node)
            #iRes=0
            if bLogDbg:
                vtLog.vtLngCur(vtLog.DEBUG,'id:%08d'%(id),
                                    origin=self.origin)
                        #self.docSynch.DelProc(i,docNode)
                        #self.docSynch.SetProcDelResult(iRes)
            #if iRes>=0:
            #    self.dNotify['del'].append(id)
            #    ret=0
            #else:
            #    self.dNotify['del_fault'].append(id)
            ret=1
        except:
            self.iPending-=1
            vtLog.vtLngTB(self.origin)
            ret=-1
        return ret
    def __chkNodePresent(self,id):
        docNode=self.doc.GetId(id)[1]
        return docNode is None
    def __doMoveAtSrv__(self,iAct,tIds):
        if self.Is2Stop():
            return
        bLogDbg=False
        if self.verbose>5:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                bLogDbg=True
        if bLogDbg:
            vtLog.vtLngCur(vtLog.DEBUG,'iAct:%d;tIds:%s'%(iAct,vtLog.pformat(tIds)),self.origin)
        # location on client has changed
        ret=0
        try:
            self.iPending+=1
            id=tIds[0]
            idPar=tIds[1]
            node=self.doc.getNodeByIdNum(id)
            nodePar=self.doc.getNodeByIdNum(idPar)
            if (node is None) or (nodePar is None):
                self.iPending-=1
                # node was deleted in the mean while, that is not very good
                vtLog.vtLngCur(vtLog.ERROR,
                            'id:%08d;idPar:%08d scheduled to moveAtSrv, but is alreay deleted;node is None==%dnodePar is None==%d'%(
                            id,idPar,node is None,nodePar is None),self.origin)
                self.docSynch.AddProcResponse(iAct,node,None,'move',-2,idPar)
                return -1
            self.doc.moveNode2Srv(nodePar,node)
            ret=1
        except:
            self.iPending-=1
            vtLog.vtLngTB(self.origin)
            ret=-1
        return ret
    def __doMoveAtLoc__(self,iAct,t):
        if self.Is2Stop():
            return
        bLogDbg=False
        if self.verbose>5:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                bLogDbg=True
        if bLogDbg:
            vtLog.vtLngCur(vtLog.DEBUG,'iAct:%d;t:%s'%(iAct,vtLog.pformat(t)),self.origin)
        #self.acquire()
        try:
            #l=self.dProc['moveAtLoc']
            #self.iPending+=1
            id=t[0]
            idPar=t[1]
            #for id,idPar in l:
            if 1:
                #self.iAct+=1
                #self.iTime=0
                node=self.doc.getNodeByIdNum(id)
                if node is None:
                    #self.iPending-=1
                    # node was deleted in the mean while, just fine
                    vtLog.vtLngCur(vtLog.ERROR,
                            'id:%08d scheduled to moveAtLoc, but is alreay deleted;idPar:%08d'%(
                            id,idPar),self.origin)
                    self.docSynch.AddProcResponse(iAct,node,None,'move',-2,idPar)
                    #continue
                    return 0
                nodePar=self.doc.getNodeByIdNum(idPar)
                if nodePar is None:
                    #self.iPending-=1
                    # node was deleted in the mean while, just fine
                    vtLog.vtLngCur(vtLog.WARN,
                            'id:%08d scheduled to moveAtLoc, but parent node is alreay deleted;idPar:%08d'%(
                            id,idPar),self.origin)
                    self.docSynch.AddProcResponse(iAct,node,None,'move',-3,idPar)
                    #continue
                    return 0
                iRes=self.doc.__synchMove__(nodePar,node)
                self.docSynch.AddProcResponse(iAct,node,None,'move',iRes,idPar)
                if bLogDbg:
                    vtLog.vtLngCur(vtLog.DEBUG,'id:%08d;idPar:%08d,iRes:%d'%(id,idPar,iRes),
                                        origin=self.origin)
                if iRes>0:
                    self.dNotify['move'].append(id)
                else:
                    #self.iPending-=1
                    self.dNotify['move_fault'].append(id)
                return 0
            #l=[]
        except:
            #self.iPending-=1
            vtLog.vtLngTB(self.origin)
            #try:
            #    i=l.index((id,idPar))
            #    l=l[i:]
            #except:
            #    pass
        #self.dProc['moveAtLoc']=l
        #self.release()
        #if bAcquire:
        #    self.doc.release('dom')
        return 0
    def __sortIdByLevel__(self,s):
        def cmpAddNode(aNode,bNode):
            return self.doc.getLevel(aNode)-self.doc.getLevel(bNode)
        try:
            #vtLog.CallStack('')
            # add offline added to server
            bLogDbg=False
            if self.verbose>5:
                if vtLog.vtLngIsLogged(vtLog.INFO):
                    bLogDbg=True
            if bLogDbg:
                vtLog.vtLngCur(vtLog.INFO,'',self.origin)
            l=self.dProc[s]
            self.dProc[s]=[]
            if self.verbose>0:
                vtLog.vtLngCur(vtLog.INFO,'kind:%s;l:%s'%(s,vtLog.pformat(l)),self.origin)
            lstAdd=[self.doc.getNodeByIdNum(id) for id in l]
            if bLogDbg:
                vtLog.vtLngCur(vtLog.INFO,'started;count:%d'%(len(lstAdd)),
                    origin=self.origin)
            lstAdd.sort(cmpAddNode)
            l=[self.doc.getKeyNum(node) for node in lstAdd]
            self.dProc[s]=l
            if bLogDbg:
                vtLog.vtLngCur(vtLog.INFO,'kind:%s;sorted;l:%s'%(s,vtLog.pformat(l)),self.origin)
        except:
            vtLog.vtLngTB(self.origin)
    def __doAdd2Srv__(self,iAct,id):
        if self.Is2Stop():
            return
        bLogDbg=False
        if self.verbose>5:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                bLogDbg=True
        if bLogDbg:
            vtLog.vtLngCur(vtLog.DEBUG,'iAct:%d;id:%08d'%(iAct,id),self.origin)
        node=self.doc.getNodeByIdNum(id)
        if node is None:
            # node was deleted in the mean while
            vtLog.vtLngCur(vtLog.WARN,'id:%08d scheduled to add2srv, but delete in the meanwhile'%(id),self.origin)
            #if bAcquire:
            #    self.doc.release('dom')
            return -1
        try:
            self.iPending+=1
            iAdd=self.doc.__synchAdd2Srv__(node)#self.lstOffline[k])
            if iAdd>0:
                ret=1
            else:
                self.iPending-=1
                self.dNotify['add_fault'].append((id,'init'))
                ret=-1
        except:
            self.iPending-=1
            vtLog.vtLngTB(self.origin)
            ret=-1
        return ret
    def __doSynch2Srv__(self,iAct,id):
        if self.Is2Stop():
            return
        bLogDbg=False
        if self.verbose>5:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                bLogDbg=True
        if bLogDbg:
            vtLog.vtLngCur(vtLog.DEBUG,'iAct:%d;id:%08d'%(iAct,id),self.origin)
        node=self.doc.getNodeByIdNum(id)
        if node is None:
            # node was deleted in the mean while
            vtLog.vtLngCur(vtLog.WARN,'id:%08d scheduled to synch2srv, but delete in the meanwhile'%(id),self.origin)
            return -1
        try:
            self.iPending+=1
            iEdit=self.doc.__synchSet2Srv__(node)
            if iEdit>0:
                ret=1
            else:
                self.iPending-=1
                self.dNotify['edit_fault'].append((id,'init'))
                ret=-1
        except:
            self.iPending-=1
            vtLog.vtLngTB(self.origin)
            ret=-1
        return ret
    def __doSynch2Loc__(self,iAct,id):
        if self.Is2Stop():
            return
        bLogDbg=False
        if self.verbose>5:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                bLogDbg=True
        if bLogDbg:
            vtLog.vtLngCur(vtLog.DEBUG,'iAct:%d;id:%08d'%(iAct,id),self.origin)
        node=self.doc.getNodeByIdNum(id)
        if node is None:
            # node was deleted in the mean while
            vtLog.vtLngCur(vtLog.WARN,
                    'id:%08d scheduled to synch2loc, but delete in the meanwhile'%(
                    id),self.origin)
            #return -1
        try:
            self.iPending+=1
            self.doc.GetSynchNode(str(id))
            ret=1
        except:
            self.iPending-=1
            vtLog.vtLngTB(self.origin)
            ret=-1
        return ret
    def __doAdd2Loc__(self,iAct,id):
        if self.Is2Stop():
            return
        bLogDbg=False
        if self.verbose>5:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                bLogDbg=True
        if bLogDbg:
            vtLog.vtLngCur(vtLog.DEBUG,'iAct:%d;id:%08d'%(iAct,id),self.origin)
        node=self.doc.getNodeByIdNum(id)
        if node is not None:
            # node was deleted in the mean while
            vtLog.vtLngCur(vtLog.WARN,
                    'id:%08d scheduled to add2loc, but is alreay present'%(
                    id),self.origin)
            #return -1
        try:
            self.iPending+=1
            self.doc.GetSynchNode(str(id))
            ret=1
        except:
            self.iPending-=1
            vtLog.vtLngTB(self.origin)
            ret=-1
        return ret
    def __proc__(self,sKind,bAcquire,func,*args,**kwargs):
        if self.Is2Stop():
            return
        bLogStatus=False
        if self.verbose>5:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                bLogStatus=True
        if bLogStatus:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'sKind:%s;bAcquire:%d'%(sKind,bAcquire),self.origin)
        if bAcquire:
            self.doc.acquire('dom')
        try:
            self.sActKind=sKind
            l=self.dProc[sKind]
            iLen=len(l)
            #while len(l)>0:
            while self.__chk(sKind):
                if self.Is2Stop():
                    return
                try:
                    #self.doc.NotifySynchProc(self.iAct,self.iCount)
                    self.acquire()
                    l=self.dProc[sKind]
                    iLen=len(l)
                    #print sKind,iLen,self.iPending,
                    if iLen<=self.iPending:
                        #print
                        self.release()
                        #time.sleep(0.5)
                        break
                    #id=l[0]
                    id=l[self.iPending]
                    #print id
                    if bLogStatus:
                        try:
                            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                vtLog.vtLngCur(vtLog.DEBUG,'kind:%s;ids:%s;iAct:%d,iCount:%s'%(
                                                sKind,vtLog.pformat(l),self.iAct,self.iCount),
                                                origin=self.origin)
                        except:
                            vtLog.vtLngTB(self.origin)
                    iAct=self.iAct
                    self.release()
                    if func(iAct,id,*args,**kwargs)<=0:
                        self.acquire()
                        try:
                            l=self.dProc[sKind]
                            del l[0]
                        except:
                            vtLog.vtLngCS(vtLog.ERROR,
                                'proc:%s;l:%s;%s'%(sKind,vtLog.pformat(l),
                                traceback.format_exc()),
                                origin=self.origin)
                        self.iAct+=1
                        self.iTime=0
                        self.release()
                        #self.release()
                        #self.__proc__(sKind,func,*args,**kwargs)
                        #return
                    else:
                        #self.iTime=0
                        if bLogStatus:
                            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                vtLog.vtLngCur(vtLog.DEBUG,'pending:%d'%(self.iPending),origin=self.origin)
                        if sKind in ['add2loc','synch2srv']:
                            if self.iPending>self.MAX_PENDING:
                                break
                        else:
                            break
                except:
                    if vtLog.vtLngIsLogged(vtLog.ERROR):
                        vtLog.vtLngCS(vtLog.ERROR,
                                'proc:%s;%s'%(sKind,traceback.format_exc()),
                                origin=self.origin)
                    #self.iAct+=1
                    self.acquire()
                    try:
                        l=self.dProc[sKind]
                        del l[0]
                    except:
                        vtLog.vtLngCS(vtLog.ERROR,
                                'proc:%s;l:%s;%s'%(sKind,vtLog.pformat(l),
                                traceback.format_exc()),
                                origin=self.origin)
                    self.iAct+=1
                    self.iTime=0
                    self.release()
                    #self.release()
                    #self.__proc__(sKind,func,*args,**kwargs)
                    #return
                    break
        except:
            vtLog.vtLngTB(self.origin)
        if bAcquire:
            self.doc.release('dom')
    def __procAddNodeResponse__(self,sOldID,sNewID,sResp):
        if self.Is2Stop():
            return
        if self.verbose>0:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'sIdOld:%s;sIdNew:%s;res:%s'%(sOldID,sNewID,sResp),self.origin)
        try:
            self.acquire()
            #l=self.dProc['add2srv']
            l=self.dProc[self.sActKind]
            iOldID=long(sOldID)
            iNewID=long(sNewID)
            if iOldID in l:
                self.iPending-=1
                if self.iPending<0:
                    vtLog.vtLngCur(vtLog.CRITICAL,'pending:%d'%(self.iPending),origin=self.origin)
                    self.iPending=0
                if self.verbose>0:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'pending:%d'%(self.iPending),origin=self.origin)
                #nodeOld=self.doc.getNodeById(sOldID)
                nodeNew=self.doc.getNodeById(sNewID)
                node=nodeNew
                idPar=self.doc.getKey(self.doc.getParent(node))
                if self.verbose>0:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'iAct:%d,iCount:%s'%(self.iAct,self.iCount),self.origin)
                if sResp!='ok':
                    vtLog.vtLngCur(vtLog.ERROR,
                                'kind:add2srv;idOld:%08d;idNew:%08d;respone:%s;act:%d'%(
                                iOldID,iNewID,sResp,self.iAct),
                                origin=self.origin)
                    self.dProc['resolve'].append(iNewID)
                    self.dNotify['add_fault'].append((iNewID,sResp))
                    self.docSynch.AddProcResponse(self.iAct,node,None,'add_offline',-1,idPar)
                else:
                    self.dNotify['add'].append(iNewID)
                    self.docSynch.AddProcResponse(self.iAct,node,None,'add_offline',1,idPar)
                    self.doc.getIds().__clrOffline__(iNewID,'add',self.doc)
                if self.verbose>0:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,
                                'act:%d'%(self.iAct),
                                origin=self.origin)
                #self.dNotify['add_offline'].append(node)
                l.remove(iOldID)
                #del self.lstOffLineAdd[0]
                self.iAct+=1
                self.iTime=0
                self.release()
                #self.__proc__('add2srv',False,self.__doAdd2Srv__)
                if self.sActKind=='add2srv':
                    self.__proc__('add2srv',False,self.__doAdd2Srv__)
                #self.__procAddOffLine__()
                return
            else:
                vtLog.vtLngCur(vtLog.WARN,'id:%08d;not in request list'%(iOldID),self.origin)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'request list:%s'%(vtLog.pformat(l)),self.origin)
        except:
            vtLog.vtLngTB(self.origin)
        self.release()
    def __procSetNodeResponse__(self,sID,sNewID,sResp):
        if self.Is2Stop():
            return
        if self.verbose>0:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'id:%s;idNew:%s;resp:%s'%
                    (sID,sNewID,sResp),self.origin)
        try:
            self.acquire()
            l=self.dProc['synch2srv']
            iID=long(sID)
            iNewID=long(sNewID)
            if iID in l:
                self.iPending-=1
                if self.iPending<0:
                    vtLog.vtLngCur(vtLog.CRITICAL,'pending:%d'%(self.iPending),origin=self.origin)
                    self.iPending=0
                if self.verbose>0:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'pending:%d'%(self.iPending),origin=self.origin)
                node=self.doc.getNodeByIdNum(iNewID)
                if sResp!='ok':
                    vtLog.vtLngCur(vtLog.ERROR,
                                'kind:synch2srv;idOld:%08d;idNew:%08d;respone:%s;act:%d'%(
                                iID,iNewID,sResp,self.iAct),
                                origin=self.origin)
                    if sResp=='acl':
                        self.dProc['synch2loc'].append(iID)
                        self.iCount+=1
                    else:
                        self.dProc['resolve'].append(iNewID)
                    self.dNotify['edit_fault'].append((iID,sResp))
                    self.docSynch.AddProcResponse(self.iAct,node,None,'edit_offline',-1)
                else:
                    self.dNotify['edit'].append(iID)
                    self.docSynch.AddProcResponse(self.iAct,node,None,'edit_offline',1)
                    ids=self.doc.getIds()
                    ids.__clrOffline__(iNewID,'edit',self.doc)
                    self.doc.clearFingerPrint(node)
                    ids.__calc__(iNewID,self.doc)
                if self.verbose>0:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,
                                'act:%d'%(self.iAct),
                                origin=self.origin)
                l.remove(iID)
                #del self.lstOffLineAdd[0]
                self.iAct+=1
                self.iTime=0
                self.release()
                self.__proc__('synch2srv',False,self.__doSynch2Srv__)
                return
            else:
                vtLog.vtLngCur(vtLog.WARN,'id:%08d;not in request list'%(iID),self.origin)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'request list:%s'%(vtLog.pformat(l)),self.origin)
        except:
            vtLog.vtLngTB(self.origin)
        self.release()
    def __procDelNodeResponse__(self,sID,sResp):
        if self.Is2Stop():
            return
        if self.verbose>0:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'id:%s;resp:%s'%
                    (sID,sResp),self.origin)
        try:
            self.acquire()
            l=self.dProc['delAtSrv']
            iID=long(sID)
            if iID in l:
                self.iPending-=1
                if self.iPending<0:
                    vtLog.vtLngCur(vtLog.CRITICAL,'pending:%d'%(self.iPending),origin=self.origin)
                    self.iPending=0
                if self.verbose>0:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'pending:%d'%(self.iPending),origin=self.origin)
                node=self.doc.getNodeByIdNum(iID)
                if sResp!='ok':
                    vtLog.vtLngCur(vtLog.ERROR,
                                'kind:delAtSrv;id:%08d;respone:%s;act:%d'%(
                                iID,sResp,self.iAct),
                                origin=self.origin)
                    self.docSynch.AddProcResponse(self.iAct,node,None,'del',-1)
                    if sResp=='acl':
                        ids=self.doc.getIds()
                        ids.__clrOffline__(iID,'del',self.doc)
                        # handle other offline states -> properly restart synch
                        if ids.IsOffline(iID):
                            vtLog.vtLngCur(vtLog.CRITICAL,'id:%08d;handle other offline status:%d'%(
                                    iID,ids.GetOfflineId(iID)),self.origin)
                    self.dProc['resolve'].append(iID)
                    self.dNotify['del_fault'].append((iID,sResp))
                else:
                    self.docSynch.AddProcResponse(self.iAct,node,None,'del',0)
                    iRes=self.doc.__synchDel__(node)
                    self.dNotify['del'].append(iID)
                l.remove(iID)
                self.iAct+=1
                self.iTime=0
                self.release()
                self.__proc__('delAtSrv',False,self.__doDelAtSrv__)
                return
            else:
                vtLog.vtLngCur(vtLog.WARN,'id:%08d;not in request list'%(iID),self.origin)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'request list:%s'%(vtLog.pformat(l)),self.origin)
        except:
            vtLog.vtLngTB(self.origin)
        self.release()
    def __procMoveNodeResponse__(self,sID,sParID,sResp):
        if self.Is2Stop():
            return
        if self.verbose>0:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'id:%s;idPar:%s;resp:%s'%
                        (sID,sParID,sResp),self.origin)
        try:
            self.acquire()
            l=self.dProc['moveAtSrv']
            iID=long(sID)
            iParID=long(sParID)
            idx=0
            idxFound=-1
            for tIds in l:
                if tIds[0]==iID:
                    idxFound=idx
                    break
                idx+=1
            if idxFound>=0:
                self.iPending-=1
                if self.iPending<0:
                    vtLog.vtLngCur(vtLog.CRITICAL,'pending:%d'%(self.iPending),origin=self.origin)
                    self.iPending=0
                if self.verbose>0:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'pending:%d'%(self.iPending),origin=self.origin)
                tIds=l[idxFound]
                node=self.doc.getNodeByIdNum(iID)
                if sResp!='ok':
                    vtLog.vtLngCur(vtLog.ERROR,
                                'kind:moveAtSrv;id:%08d;idParOL:%08d;idParRmt:%08d;idParResp:%08d;respone:%s;act:%d'%(
                                iID,tIds[1],tIds[2],iParID,sResp,self.iAct),
                                origin=self.origin)
                    self.docSynch.AddProcResponse(self.iAct,node,None,'move',-1,iParID)
                    if sResp=='acl':
                        self.doc.getIds().__clrOffline__(iID,'move',self.doc)
                        # handle other offline states -> properly restart synch
                        #if self.doc.getIds().IsOffline(iID):
                        #    vtLog.vtLngCur(vtLog.CRITICAL,'id:%08d;handle other offline status'%(
                        #            iID),self.origin)
                    else:
                        self.dProc['resolve'].append(iID)
                    self.dNotify['move_fault'].append((iID,tIds[1],tIds[2],iParID,sResp))
                else:
                    self.doc.getIds().__clrOffline__(iID,'move',self.doc)
                    self.docSynch.AddProcResponse(self.iAct,node,None,'move',0,iParID)
                    #iRes=self.doc.__synchDel__(node)
                    self.dNotify['move'].append(iID)
                #l.remove(iID)
                del l[idxFound]
                self.iAct+=1
                self.iTime=0
                self.release()
                self.__proc__('moveAtSrv',False,self.__doMoveAtSrv__)
                return
            else:
                vtLog.vtLngCur(vtLog.WARN,'id:%08d;idPar:%08d;not in request list'%(iID,iParID),self.origin)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'request list:%s'%(vtLog.pformat(l)),self.origin)
        except:
            vtLog.vtLngTB(self.origin)
        self.release()
    def GetAddOffline(self):
        try:
            return self.dNotify['add_offline']
        except:
            return []
    def GetEditOffline(self):
        try:
            return self.dNotify['edit_offline']
        except:
            return []
    def GetProc(self):
        return self.dProc
    def GetNotify(self):
        try:
            return self.dNotify,self.dNotifyTrans,self.running
        except:
            return {},self.dNotifyTrans,self.running
    def GetSynchFN(self):
        return self.synchFN
    def IsChanged(self):
        return self.bChanged
    def CheckChanged(self):
        try:
            self.bChanged=False
            for k in self.dNotify.keys():
                if k=='ident':
                    continue
                elif len(self.dNotify[k])>0:
                    self.bChanged=True
        except:
            pass
    def __chk(self,s):
        self.acquire()
        if self.iPending>0:
            bRet=True
        else:
            bRet=len(self.dProc[s])!=0
        self.release()
        return bRet
    def __chkAddOffLine(self,iCount):
        return self.iAct<iCount
    def __doWaitCheckDone__(self,func,*args,**kwargs):
        self.iUpdate=0
        self.iTime=0.0
        while func(*args,**kwargs) and (self.Is2Stop()==False):
            #print self.iAct,iCount,self.iAct<iCount
            #vtLog.vtLngCur(vtLog.DEBUG,''%(),self.origin)
            self.acquire()
            self.iTime+=self.TIME_SLEEP
            self.iUpdate+=1
            self.release()
            if self.iTime>self.TIME_ABORT:
                self.Stop()
                self.acquire()
                if vtLog.vtLngIsLogged(vtLog.ERROR):
                    vtLog.vtLngCur(vtLog.ERROR,
                        'synch:%s time out (%d);l:%s'%(
                        self.sActKind,self.iTime,
                        vtLog.pformat(self.dProc[self.sActKind])),
                        self.origin)
                self.release()
            if self.iUpdate>self.TIME_UPDATE:
                self.acquire()
                try:
                    self.doc.NotifySynchProc(self.iAct-self.iPending,self.iCount)
                except:
                    pass
                self.iUpdate=-1
                self.release()
            time.sleep(self.TIME_SLEEP)
        if self.verbose>10:
            vtLog.vtLngCur(vtLog.DEBUG,''%(),self.origin)
        return not func(*args,**kwargs)
    def __logStatus__(self,bOffline=False):
        if self.verbose<=0:
            return
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'dProc:%s;dNotify:%s'%(
                    vtLog.pformat(self.dProc),vtLog.pformat(self.dNotify)),
                    self.origin)
        if bOffline:
            try:
                self.doc.acquire('dom')
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    docIds=self.doc.GetIds()
                    offIds=docIds.GetOfflineIds()
                    #offContIds=self.doc.getIds().GetOfflineContentIds()
                    offIds.sort()
                    #offContIds.sort()
                    #vtLog.vtLngCur(vtLog.DEBUG,'fps:%s'%(vtLog.pformat(fps)),self.origin)
                    vtLog.vtLngCur(vtLog.DEBUG,'offIds:%s'%(vtLog.pformat(offIds)),self.origin)
            except:
                pass
            self.doc.release('dom')
    def __close__(self):
        self.CheckChanged()
        self.doc.NotifySynchAborted(self.bChanged)
        #self.doc.release('thread')
        #self.doc.release('dom')
        self.doc.release('synch_result')
        if self.verbose>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'release',origin=self.origin)
        #self.running=False
        self.docSynch.Close()
    def IsBusy(self):
        try:
            self._acquire()
            if self.busy:
                if self.paused:
                    return False
                return True
            return False
        finally:
            self._release()
    def doSynchProc(self):
        bFirst=True
        bSynch=True
        bLogStatus=False
        if self.verbose>5:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                bLogStatus=True
        while bSynch==True:
            if self.Is2Stop():
                return
            try:
                self._acquire()
                self.busy=True
                self._release()
                zStart=time.clock()
                self.iPending=0
                #self.lstOffLineAdd=None
                #self.__procAddOffLine__()
                self.__proc__('add2srv',True,self.__doAdd2Srv__)
                iRes=self.__doWaitCheckDone__(self.__chk,'add2srv')
                if bLogStatus:
                    self.__logStatus__()
                if self.Is2Stop():
                    self.__close__()
                    return
                self.__proc__('synch2srv',True,self.__doSynch2Srv__)
                iRes=self.__doWaitCheckDone__(self.__chk,'synch2srv')
                if bLogStatus:
                    self.__logStatus__()
                if self.Is2Stop():
                    self.__close__()
                    return
                if self.verbose>0:
                    vtLog.vtLngCur(vtLog.INFO,'addOffline finished:%d;time elapsed:%f'%(iRes,time.clock()-zStart),self.origin)
                #self.iAct=0
                self.__proc__('add2loc',True,self.__doAdd2Loc__)
                iRes=self.__doWaitCheckDone__(self.__chk,'add2loc')
                if bLogStatus:
                    self.__logStatus__()
                if self.Is2Stop():
                    self.__close__()
                    return
                self.__proc__('synch2loc',True,self.__doSynch2Loc__)
                iRes=self.__doWaitCheckDone__(self.__chk,'synch2loc')
                if bLogStatus:
                    self.__logStatus__()
                if self.Is2Stop():
                    self.__close__()
                    return
                #self.__procMoveAtLoc__(bAcquire=True)
                self.__proc__('moveAtLoc',True,self.__doMoveAtLoc__)
                iRes=self.__doWaitCheckDone__(self.__chk,'moveAtLoc')
                if bLogStatus:
                    self.__logStatus__(bOffline=True)
                if self.Is2Stop():
                    self.__close__()
                    return
                self.__proc__('moveAtSrv',True,self.__doMoveAtSrv__)
                iRes=self.__doWaitCheckDone__(self.__chk,'moveAtSrv')
                if bLogStatus:
                    self.__logStatus__(bOffline=True)
                if self.Is2Stop():
                    self.__close__()
                    return
                #self.__procDelAtLoc__(bAcquire=True)
                self.__proc__('delAtLoc',True,self.__doDelAtLoc__)
                iRes=self.__doWaitCheckDone__(self.__chk,'delAtLoc')
                if bLogStatus:
                    self.__logStatus__(bOffline=True)
                if self.Is2Stop():
                    self.__close__()
                    return
                self.__proc__('delAtSrv',True,self.__doDelAtSrv__)
                iRes=self.__doWaitCheckDone__(self.__chk,'delAtSrv')
                if bLogStatus:
                    self.__logStatus__(bOffline=True)
                if self.Is2Stop():
                    self.__close__()
                    return
                if bLogStatus:
                    vtLog.vtLngCur(vtLog.INFO,'time elapsed:%f'%(time.clock()-zStart),self.origin)
                #self.__procAdd__()
                #self.__procMove__()
                #self.__procDel__()
                #self.__procAddOffLine__()   # 070201:wro
                    
                if bLogStatus:
                    vtLog.vtLngCur(vtLog.INFO,'time elapsed:%f;add edit finished'%(time.clock()-zStart),self.origin)
                #print 'finished'
                #while self.iAct<self.iCount:
                #    self.doc.NotifySynchProc(self.iAct,self.iCount)
                if self.Is2Stop():
                    return
                self._acquire()
                self.busy=False
                self._release()
                bSynch=self.doResolve(bFirst=bFirst)
                bFirst=False
                
            except:
                vtLog.vtLngTB(self.origin)
    def doSynch(self):
        self.Clear()
        #vtLog.CallStack('')
        vtLog.vtLngCur(vtLog.INFO,'Synch:run start',origin=self.origin)
        try:
            zStart=time.clock()
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'acquire',origin=self.origin)
            #self.doc.acquire('thread')
            #self.doc.acquire('dom')
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'acquired',origin=self.origin)
            
            # build synch proc file
            self.doc.acquire('dom')
            self.doc.acquire('synch_result')
            self.docSynch=vNetXmlSynchProc(self.doc,True)
            self.synchFN=self.docSynch.GetFN()
        except:
            vtLog.vtLngTB(self.origin)
            self.doc.NotifySynchAborted(self.bChanged)
            #self.doc.release('thread')
            self.doc.release('synch_result')
            self.doc.release('dom')
            self.running=False
            return
        self.doc.release('dom')
        try:
            #vtLog.CallStack(self.synchFN)
            
            self.doc.NotifySynchStart(self.iCount)
            #time.sleep(40)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'GetKey start',origin=self.origin)
            self.doc.GetKeys()
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'GetKey send',origin=self.origin)
            iTime=0
            #while (self.dProc is None) or (not self.doc.IsFlag(self.doc.OPENED)):
            while (self.dProc is None) or (not self.doc.IsOpened()):
                #self.doc.NotifySynchStart(self.iCount)
                #if self.keys is None:
                #    print 'wait remote keys'
                #if self.doc.IsFlag(self.doc.OPENED)==False:
                #    print 'wait file to be opened'
                iTime+=self.TIME_SLEEP
                if iTime>self.TIME_ABORT:
                    self.Stop()
                else:
                    if iTime%self.TIME_TIMEOUT==0:
                        if self.keys is None:
                            self.doc.GetKeys()
                            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                vtLog.vtLngCur(vtLog.DEBUG,'get keys',
                                    origin=self.origin)
                if self.Is2Stop():
                    break
                time.sleep(self.TIME_SLEEP)
            vtLog.vtLngCur(vtLog.INFO,'time elapsed:%f;keys received'%(time.clock()-zStart),self.origin)
            if self.Is2Stop():
                self.__close__()
                return
        except:
            vtLog.vtLngTB(self.origin)
        try:
            self.doSynchProc()          # perform actual synch
            
            vtLog.vtLngCur(vtLog.INFO,'time elapsed:%f'%(time.clock()-zStart),self.origin)
            #    self.iAct+=1
            self.CheckChanged()
            #self.doc.AlignDoc()
            self.doc.acquire('dom')
            #try:
            #    lIds=self.doc.CreateReq()
                #self.dProc['createReg']=lIds
                #self.__proc__('createReg',True,self.__doCreateReq__)
                #self.sActKind='createReg'
                #iRes=self.__doWaitCheckDone__(self.__chk,'createReg')
                #self.__logStatus__(bOffline=True)
            #except:
            #    vtLog.vtLngTB(self.origin)
            try:
                #self.doc.Build()
                self.doc.genIdsRef(bAcquire=False,bThreaded=False)
            except:
                pass
            self.doc.release('dom')
            self.doc.__Save__(self.doc.GetFN())#(bThread=True)
            try:
                self.doc.Build()
            except:
                pass
            #self.doc.release('thread')
            self.running = False
            #  close and remove synch proc
            #self.docSynch.AlignDoc()
            self.docSynch.Save()
            self.docSynch.Close()
            self.doc.release('synch_result')
            del self.docSynch
            self.docSynch=None
            #del self.dNotify
            #self.dNotify={}
            vtLog.vtLngCur(vtLog.INFO,'time elapsed:%f;synch finished'%(time.clock()-zStart),self.origin)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'release',origin=self.origin)
            if self.keepGoing:
                self.doc.NotifySynchFinished(self.bChanged)
            else:
                self.doc.NotifySynchAborted(self.bChanged)
            vtLog.vtLngCur(vtLog.INFO,'Synch:run finished',origin=self.origin)
            return
        except:
            vtLog.vtLngTB(self.origin)
        #self.keepGoing = self.running = False
        #self.doc.release('dom')
        self.doc.release('synch_result')
        self.doc.NotifySynchAborted(self.bChanged)
        vtLog.vtLngCur(vtLog.INFO,'Synch:run aborted',origin=self.origin)
    def _clrResolve(self):
        try:
            dlg=self.doc.GetEditResolveDlg()
            if dlg is None:
                vtLog.vtLngCur(vtLog.CRITICAL,'no synch resolve dialog defined'%(),self.GetOrigin())
            else:
                dlg.SetResolved(self.doc.GetAppl())
        except:
            vtLog.vtLngTB(self.origin)
    def _setResolve(self):
        try:
            dlg=self.doc.GetEditResolveDlg()
            if dlg is None:
                vtLog.vtLngCur(vtLog.CRITICAL,'no synch resolve dialog defined'%(),self.GetOrigin())
                return False
            dlg.SetNotify(self.doc,self.doc.GetAppl(),self.dNotify,self.dProc,
                        self.doResolveProc)
            wx.CallAfter(self.showResolve,dlg)
            #dlg.Show(True)
        except:
            vtLog.vtLngTB(self.origin)
    def doResolve(self,bFirst=True):
        bResolve=False
        if self.verbose>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'dNotify:%s'%(vtLog.pformat(self.dNotify)),self.GetOrigin())
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'dProc:%s'%(vtLog.pformat(self.dProc)),self.GetOrigin())
        #for s in ['edit_resolve']:
        for s in ['resolve']:
            if s in self.dProc:
                if len(self.dProc[s])>0:
                    bResolve=True
                    break
        if bResolve==False:
            self.CallBack(self._clrResolve)
            return False
        try:
            dlg=self.doc.GetEditResolveDlg()
            if dlg is None:
                vtLog.vtLngCur(vtLog.CRITICAL,'no synch resolve dialog defined'%(),self.GetOrigin())
                return False
            dlg.acquire()
            self.CallBack(self._setResolve)
        except:
            vtLog.vtLngTB(self.origin)
        #if bFirst==False:
        #    return 
        return True
    def showResolve(self,dlg):
        dlg.SetSize((500,400))
        dlg.Centre()
        dlg.Show(True)
    def doResolveProc(self,dProc):
        try:
            if self.verbose>0:
                if vtLog.vtLngIsLogged(vtLog.INFO):
                    vtLog.vtLngCur(vtLog.INFO,'dProc:%s'%(vtLog.pformat(dProc)),self.GetOrigin())
            #dNotify=self.dNotify
            self.dProc=dProc
            self.dNotify['edit']=[]
            #,'edit_fault':[],'edit_offline':[],
            self.dNotify['edit_resolve']=[]
            self.dNotify['add']=[]
            #self.dNotify['add_fault']=[]
            self.dNotify['add_offline']=[]
            self.dNotify['move']=[]
            #self.dNotify['move_fault']=[]
            self.dNotify['del']=[]
            docIds=self.doc.getIds()
            #for key in dProc['synch2srv']:
            #    docIds.__clrOffline__(key,'edit',self.doc)
            for key in dProc['synch2loc']:
                docIds.__clrOffline__(key,'edit',self.doc)
            #self.dNotify['del_fault']=[]
            #self.dNotify['ident']=[]
            #self.doSynchProc()          # perform actual synch
            #self.doResolve(bFirst=False)
        except:
            vtLog.vtLngTB(self.origin)
