#----------------------------------------------------------------------------
# Name:         vNetXmlLock.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060207
# CVS-ID:       $Id: vtNetLock.py,v 1.14 2007/08/06 22:53:55 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import sys
from vidarc.tool.log import vtLog
from vidarc.tool.time import vtTime
import threading

class vtNetLock:
    def __init__(self,origin,par):
        self.dId={}
        self.dIdOld={}
        self.par=par
        self.zNow=vtTime.vtDateTime()
        self.zPrev=vtTime.vtDateTime()
        self.semAccess=threading.Lock()#threading.Semaphore()
        self.SetOrigin(origin)
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        #self.semAccess.acquire()
        try:
            #vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
            del self.dId
            del self.dIdOld
        except:
            #vtLog.vtLngTB('del')
            pass
        self.dId=None
        self.dIdOld=None
        #self.semAccess.release()
    def ShutDown(self):
        self.par=None
    def SetOrigin(self,origin):
        self.origin=':'.join([origin,self.__class__.__name__])
    def Add(self,id):
        self.semAccess.acquire()
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,str(id),self.origin)
            self.dId[long(id)]=vtTime.vtDateTime()
            self.semAccess.release()
        except:
            self.semAccess.release()
            vtLog.vtLngTB(self.origin)
    def Del(self,id):
        self.semAccess.acquire()
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,str(id),self.origin)
            self.__del(id)
            self.semAccess.release()
        except:
            self.semAccess.release()
            vtLog.vtLngTB(self.origin)
    def __del(self,id):
        idNum=long(id)
        if idNum in self.dId:
            del self.dId[idNum]
        if idNum in self.dIdOld:
            del self.dIdOld[idNum]
    def HasLock(self,id):
        self.semAccess.acquire()
        try:
            idNum=long(id)
            if idNum in self.dId:
                #t=self.dId[long(id)]
                self.semAccess.release()
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'id:%s True'%str(id),self.origin)
                return True
            else:
                self.semAccess.release()
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'id:%s False'%str(id),self.origin)
                return False
        except:
            self.semAccess.release()
            vtLog.vtLngTB(self.origin)
            return False
    def GetLockIds(self):
        try:
            return self.dId.keys()
        except:
            return []
    def ChangeId(self,id,idNew):
        self.semAccess.acquire()
        try:
            idLock=self.dIdOld[long(id)]
        except:
            idLock=id
        try:
            zTime=self.dId[long(id)]
            self.__del(id)
            self.dId[long(idNew)]=zTime
            self.dIdOld[long(idNew)]=idLock
        except:
            vtLog.vtLngTB(self.origin)
        self.semAccess.release()
    def __getLockId(self,id):
        try:
            iId=long(id)
            if iId in self.dIdOld:
                return self.dIdOld[iId]
            if iId in self.dId:
                return id
            return -2
        except:
            return -3
    def GetLockId(self,id):
        self.semAccess.acquire()
        iId=self.__getLockId(id)
        self.semAccess.release()
        return iId
    def GetLockTime(self,id):
        zTm=None
        self.semAccess.acquire()
        try:
            iId=long(id)
            if iId in self.dId:
                zTm=self.dId[iId]
            self.semAccess.release()
        except:
            vtLog.vtLngTB(self.origin)
            self.semAccess.release()
        return zTm
    def FindTimeOut(self,fSecTimeOut):
        self.semAccess.acquire()
        keys=self.dId.keys()
        self.zNow.Now()
        iFirst=-1
        fFirst=-1
        for k in keys:
            fSec=self.dId[k].CalcDiffSecFloat(self.zNow)
            if fSec>fSecTimeOut:
                if fFirst<fSec:
                    iFirst=k
                    fFirst=fSec
        self.semAccess.release()
        return iFirst
    def __str__(self):
        s=';'.join(['(%08d : at %s)'%(k,v.__str__()) for k,v in self.dId.items()])
        return s
    def __repr__(self):
        return ';'.join(['%08d'%k for k in self.dId.keys()])
