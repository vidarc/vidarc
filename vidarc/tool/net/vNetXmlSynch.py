#----------------------------------------------------------------------------
# Name:         vNetXmlSynch.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vNetXmlSynch.py,v 1.26 2007/08/06 22:53:44 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import libxml2
import string,types,time,sys
import os,threading,thread,traceback,copy
import vidarc.tool.InOut.fnUtil as fnUtil
import vidarc.tool.log.vtLog as vtLog
from vNetXmlSynchProc import vNetXmlSynchProc

class thdNetXmlSynch:
    TIME_SLEEP=0.05
    TIME_UPDATE=20
    TIME_TIMEOUT=30
    TIME_ABORT=180
    def __init__(self,doc=None,name='',verbose=0):
        self.verbose=verbose
        self.doc=doc
        self.running=False
        self.keys=None
        self.iKeyCount=0
        self.name=name
        self.iTimeOut=10
        self.iReqCount=100
        self.iReqOpen=0
        self.iReqRestart=25
        self.iReqAct=0
        self.semProc=threading.Lock()#threading.Semaphore()
        # multi lang trick
        self.dNotifyTrans={}
        self.dNotifyTrans['edit']=_(u'edit')
        self.dNotifyTrans['edit_fault']=_(u'edit_fault')
        self.dNotifyTrans['edit_offline']=_(u'edit_offline')
        self.dNotifyTrans['edit_resolve']=_(u'edit_resolve')
        self.dNotifyTrans['add']=_(u'add')
        self.dNotifyTrans['add_fault']=_(u'add_fault')
        self.dNotifyTrans['add_offline']=_(u'add_offline')
        self.dNotifyTrans['move']=_(u'move')
        self.dNotifyTrans['move_fault']=_(u'move_fault')
        self.dNotifyTrans['del']=_(u'del')
        self.dNotifyTrans['del_fault']=_(u'del_fault')
        self.dNotifyTrans['ident']=_(u'identical')
        self.docSynch=None
        self.synchFN=''
        self.bChanged=False
        self.PHASE=[
            'getKeys',
            'add_offline',
            'proc',
            'del',
            'move']
    
    def Synch(self):
        #vtLog.CallStack('')
        vtLog.vtLngCur(vtLog.INFO,'',self.name)
        self.keys=None
        self.keepGoing = self.running = True
        self.dNotify={'edit':[],'edit_fault':[],'edit_offline':[],
                'edit_resolve':[],
                'add':[],'add_fault':[],'add_offline':[],
                'move':[],'move_fault':[],
                'del':[],'del_fault':[],'ident':[]}
        self.iReqOpen=0
        self.tid=thread.start_new_thread(self.Run, ())
    def SetKeys(self,sKeys):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'sKeys:%s'%(sKeys),self.name)
        #self.keys=keys.split(',')
        zStart=time.clock()
        self.docIds=self.doc.getIds()
        self.docKeys=copy.copy(self.docIds.GetIDs())
        keys=[]
        try:
            keyFPs=sKeys.split(',')
            s=keyFPs[0].split(';')
            if self.doc.getFingerPrint(self.doc.getBaseNode())!=s[1]:
                keys.append(s[0])
            elif self.doc.getTagName(self.doc.getBaseNode())!=s[2]:
                # consider abort base node tagnames dont match
                keys.append(s[0])
            self.keys2Del=[]
            fps=self.doc.getIds().GetFPs()
            offIds=self.doc.getIds().GetOfflineAddIds()
            offContIds=self.doc.getIds().GetOfflineContentIds()
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'fps:%s'%(vtLog.pformat(fps)),self.name)
                vtLog.vtLngCur(vtLog.DEBUG,'offIds:%s'%(vtLog.pformat(offIds)),self.name)
                vtLog.vtLngCur(vtLog.DEBUG,'offContIds:%s'%(vtLog.pformat(offContIds)),self.name)
            iAct=0
            for keyFP in keyFPs[1:]:
                s=keyFP.split(';')
                try:
                    key=long(s[0])
                    # ++++ 061003 wro
                    if key in offIds:
                        continue
                    # 070202: wro 
                    #       process offline content modification normal
                    #if key in offContIds:   
                    #    continue
                    # ----
                    if key in fps:
                        n=self.doc.getNodeByIdNum(key)
                        if fps[key]!=s[1]:
                            keys.append(s[0])
                        elif self.doc.getTagName(n)!=s[2]:
                            keys.append(s[0])
                        elif self.doc.getKeyNum(self.doc.getParent(n))!=long(s[3]):
                            keys.append(s[0])
                        else:
                            self.docKeys.remove(key)
                            self.docSynch.AddProcIdentical(iAct,key,'identical',1)
                            iAct+=1
                    else:
                        keys.append(s[0])
                    #else:
                    #    self.keys2Del.append(key)
                except:
                    vtLog.vtLngCur(vtLog.DEBUG,'keyFP:%s'%(keyFP),self.name)
                    vtLog.vtLngTB(self.name)
            if len(keys)==0:
                self.bAllRemoteGot=True
            self.keys=keys
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'time elapsed:%f'%(time.clock()-zStart),self.name)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'docKeys:%s;remote keys:%s'%(vtLog.pformat(self.docKeys),vtLog.pformat(self.keys)),self.name)
        except:
            vtLog.vtLngTB(self.name)
    def Stop(self):
        self.keepGoing = False

    def IsRunning(self):
        return self.running
    
    def __getIds__(self,node):
        d={}
        for c in self.doc.getChilds(node):
            key=self.doc.getKey(c)
            if len(key)>0:
                d[key]=c
        return d
    def __clearOfflineOld__(self,node):
        docKeys=self.docIds.GetIDs()
        for k in docKeys:
            c=self.doc.GetId(k)[1]
            off=self.doc.getAttribute(c,'offline')
            if len(off)>0:
                id=self.doc.getKey(c)
                self.lstOffline[long(id)]=c
                self.doc.removeId(c)
                self.doc.removeAttribute(c,'offline')
                self.doc.removeAttribute(c,'offline_content')
            else:
                off=self.doc.getAttribute(c,'offline_content')
                if len(off)>0:
                    id=self.doc.getKey(c)
                    #self.doc.removeId(c)
                    self.lstOffCont[long(id)]=c
                    self.doc.removeAttribute(c,'offline_content')
    def __clrOffline__(self,c,*args,**kwargs):
        try:
            off=self.doc.getAttribute(c,'offline')
            #if len(off)<=0:
            #    return -1
            #self.doc.removeId(c)
            self.doc.removeAttribute(c,'offline')
            self.doc.removeAttribute(c,'offline_content')
        except:
            vtLog.vtLngTB(self.name)
        return 1
    def __clrOfflineContent__(self,c):
        self.doc.removeAttribute(c,'offline_content')
        return 1
    def __clearOfflineNode__(self,c,*args,**kwargs):
        try:
            id=self.doc.getKey(c)
            off=self.doc.getAttribute(c,'offline')
            if len(off)>0:
                id=self.doc.getKey(c)
                self.lstOffline[long(id)]=c
                #self.doc.removeId(c)
                #self.doc.unlinkNode(c)  # 070201:wro
                self.doc.procKeysRec(sys.maxint,c,self.__clrOffline__)
            else:
                off=self.doc.getAttribute(c,'offline_content')
                if len(off)>0:
                    id=self.doc.getKey(c)
                    #self.doc.removeId(c)
                    #self.doc.unlinkNode(c)  # 070201:wro
                    self.lstOffCont[long(id)]=c
                    self.doc.removeAttribute(c,'offline_content')
        except:
            vtLog.vtLngTB(self.name)
        return 1
    def __clearOffline__(self,node):
        try:
            vtLog.vtLngCur(vtLog.INFO,'started',self.name)
            #zStart=time.clock()
            #nodeBase=self.doc.getBaseNode()
            #self.doc.procChildsKeysRec(sys.maxint,nodeBase,self.__clearOfflineNode__)
            #docKeys=self.docIds.GetIDs()
            docKeys=copy.copy(self.docIds.GetOfflineIds())
            
            for id in docKeys:
                c=self.doc.getNodeByIdNum(id)
                if c is not None:
                    self.lstOffline[id]=c
                    try:
                        self.docKeys.remove(id)
                    except:
                        vtLog.vtLngTB(self.name)
                    #self.doc.removeId(c)
                    self.doc.removeAttribute(c,'offline')
                    self.doc.removeAttribute(c,'offline_content')
                else:
                    vtLog.vtLngCur(vtLog.ERROR,'id:%d node not found'%(id),self.name)
                
            docKeys=copy.copy(self.docIds.GetOfflineContentIds())
            for id in docKeys:
                c=self.doc.getNodeByIdNum(id)
                if c is not None:
                    if id not in self.lstOffline:
                        self.lstOffCont[id]=c
                        self.doc.removeAttribute(c,'offline_content')
                else:
                    vtLog.vtLngCur(vtLog.ERROR,'id:%d node not found'%(id),self.name)
            docKeys={}
            for k in docKeys:
                c=self.doc.GetId(k)[1]
                def getFirstNode(c):
                    id=self.doc.getKey(c)
                    if self.doc.IsKeyValid(id)==False:
                        return None
                    off=self.doc.getAttribute(c,'offline')
                    if len(off)>0:
                        pass
                    else:
                        off=self.doc.getAttribute(c,'offline_content')
                        if len(off)>0:
                            pass
                        else:
                            return None
                    p=self.doc.getParent(c)
                    pp=getFirstNode(p)
                    if pp is not None:
                        return pp
                    return c
                        
                p=getFirstNode(c)
                if p is not None:
                    self.__clearOfflineNode__(p)
            vtLog.vtLngCur(vtLog.INFO,'finished',self.name)
        except:
            vtLog.vtLngTB(self.name)
    def __showReqInfo__(self):
        print self.iAct,self.iKeyCount,self.bAllRemoteGot,self.iReqOpen,self.iReqCount,self.iReqRestart
    def __proc__(self):
        #vtLog.CallStack('')
        self.semProc.acquire()
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,
                        'iAct:%d-%d iOpen:%d'%(self.iAct,self.iKeyCount,
                        self.iReqOpen),self.name)
            if self.iReqCount==0 and 0:
                self.doc.GetSynchNode(self.keys[self.iAct])
                self.iAct+=1
            else:
                if self.iReqOpen<self.iReqRestart:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,
                                'iReqAct:%d-%d;iReqOpen:%d'%(self.iReqAct,
                                self.iKeyCount,self.iReqOpen),
                                origin=self.name)
                    #self.doc.GetSynchNode(self.keys[self.iAct])
                    iCount=self.iKeyCount-self.iReqAct
                    if iCount>self.iReqCount:
                        iCount=self.iReqCount
                    if iCount>0:
                        self.doc.GetSynchNodes(self.keys[self.iReqAct:self.iReqAct+iCount])
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCur(vtLog.DEBUG,
                                'send;%s'%(vtLog.pformat(self.keys[self.iReqAct:self.iReqAct+iCount])),
                                origin=self.name)
                        self.iReqOpen+=iCount
                        #self.iAct+=iCount
                        self.iReqAct+=iCount
                    else:
                        if self.iReqOpen<=0:
                            #self.__showReqInfo__()
                            self.bAllRemoteGot=True
        except:
            if vtLog.vtLngIsLogged(vtLog.ERROR):
                vtLog.vtLngCur(vtLog.ERROR,
                        'iAct:%d iOpen:%d;%s'%(self.iAct,self.iReqOpen,traceback.format_exc()),
                        origin=self.name)
            if self.iReqOpen<0:
                self.bAllRemoteGot=True
        self.semProc.release()
        #vtLog.CallStack('')
    def __doEdit__(self,docNode,synchNode):
        try:
            self.doc.clearFingerPrint(docNode)
            #print docNode,synchNode
            #self.doc.acquire()
            iEdit=self.doc.__synchEdit__(docNode,synchNode)
            self.docSynch.AddProcResponse(self.iAct-self.iReqOpen,docNode,synchNode,'edit',iEdit)
            #self.docSynch.SetProcResponseEditResult(iEdit)
            #self.doc.release()
            if iEdit==1:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,
                        'synch proc response edit id:%s'%(self.doc.getKey(docNode)),
                        origin=self.name)
                #self.dNotify['edit'].append(id)
                self.doc.clearFingerPrint(docNode)
                self.doc.GetFingerPrintAndStore(docNode)
                self.iCount+=1
            elif iEdit==2:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,
                        'synch proc response edit offline id:%s'%(self.doc.getKey(docNode)),
                        origin=self.name)
                #self.dNotify['edit_offline'].append(docNode)
                self.iCount+1
            elif iEdit==0:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,
                        'synch proc response identical id:%s'%(self.doc.getKey(docNode)),
                        origin=self.name)
                self.doc.clearFingerPrint(docNode)
                self.doc.GetFingerPrintAndStore(docNode)
                pass
            else:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCS(vtLog.DEBUG,
                        'synch proc response edit fault id:%s'%(self.doc.getKey(docNode)),
                        origin=self.name)
                #self.dNotify['edit_fault'].append(id)
                self.iCount+=1
        except:
            vtLog.vtLngTB(self.name)
    def __procResponse__(self,parID,id,level,synchNode):
        try:
            #vtLog.CallStack('')
            #print id,synchNode
            self.semProc.acquire()
            self.iTime=0
            #self.__showReqInfo__()
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,
                        'id:%s;parID:%s;level:%s;'%\
                        (id,parID,level),origin=self.name)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,
                        'iAct:%d-%d iOpen:%d'%\
                        (self.iAct,self.iKeyCount,self.iReqOpen),origin=self.name)
                vtLog.vtLngCur(vtLog.DEBUG,'docKeys:%s'%
                        (vtLog.pformat(self.docKeys)),self.name)
            k=long(id)
            self.doc.acquire()
            try:
                if k==-1:#k<0:
                    # special handling of base node
                    try:
                        docNode=self.doc.getBaseNode()
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCS(vtLog.DEBUG,
                                        'synch proc response parId:%s id:%s level:%s'%(parID,id,level),
                                        origin=self.name)
                        if synchNode is not None:
                            self.__doEdit__(docNode,synchNode)
                        else:
                            #try:
                            #    self.lstOffCont[k]
                            if k in self.lstOffCont:
                                self.dNotify['edit_offline'].append(-1L)
                                #self.docSynch.AddProcResponse(self.iAct-self.iReqOpen,docNode,None)
                                #self.docSynch.SetProcResponseEditOffline(1)
                                self.docSynch.AddProcResponse(self.iAct-self.iReqOpen,docNode,None,'edit_offline',1)
                            #except:
                            else:
                                self.docSynch.AddProcResponse(self.iAct-self.iReqOpen,docNode,None,'identical',1)
                                #self.docSynch.SetProcResponseIdentical(1)
                                vtLog.vtLngCS(vtLog.DEBUG,'id:%s identical fp'%id,origin=self.name)
                                #self.dNotify['ident'].append(id)
                            pass
                    except:
                        if vtLog.vtLngIsLogged(vtLog.ERROR):
                            vtLog.vtLngCS(vtLog.ERROR,
                                    'id:%s can not be removed;%s'%(id,traceback.format_exc()),
                                    origin=self.name)
                else:
                    # normal node handling
                    if synchNode is not None:
                        bIs2Add=False
                        docNode=self.doc.GetId(k)[1]
                        #if (docNode is not None) and (k in self.docKeys):
                        if docNode is not None:
                            try:
                                #vtLog.CallStack(self.name)
                                #print docNode
                                #print k
                                #print self.docKeys
                                self.docKeys.remove(k)
                                #vtLog.vtLngCur(vtLog.DEBUG,'synch proc response parid:%s id:%s level:%d'%(parID,id,level))
                                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                    vtLog.vtLngCS(vtLog.DEBUG,
                                        'synch proc response parId:%s id:%s level:%s'%(parID,id,level),
                                        origin=self.name)
                                bSkipEdit=False
                                #try:
                                #    self.lstOffCont[k]
                                if k in self.lstOffCont:
                                    if self.doc.getFingerPrint(docNode)==self.doc.getFingerPrint(synchNode):
                                        # identical fingerprints and offline edit
                                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                            vtLog.vtLngCS(vtLog.DEBUG,
                                                'id:%s idential fp offline edited'%(id),
                                                origin=self.name)
                                    else:
                                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                            vtLog.vtLngCS(vtLog.DEBUG,
                                                'id:%s different fp offline edited'%(id),
                                                origin=self.name)
                                        #FIXME: resolve conflict manually (delayed)
                                        self.dNotify['edit_resolve'].append({'id':id,'parID':parID,
                                                'local':docNode,'remote':synchNode})
                                        bSkipEdit=True
                                #except:
                                else:
                                    pass
                                if bSkipEdit==False:
                                    self.__doEdit__(docNode,synchNode)
                                iParID=long(parID)
                                idPar=self.doc.getKeyNum(self.doc.getParent(docNode))
                                if idPar!=iParID:
                                    # location on server has changed
                                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                        vtLog.vtLngCur(vtLog.DEBUG,'id:%s;locPar:%s;rmtPar:%d;schedule 4 move'%
                                            (id,idPar,iParID),self.name)
                                    self.lstMove.append((parID,id))
                            except:
                                vtLog.vtLngTB(self.name)
                                bIs2Add=True
                                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                    vtLog.vtLngCS(vtLog.DEBUG,
                                            'synch proc response add id:%s;parID:%s;level:%s'%(id,parID,level),
                                            origin=self.name)
                                vtLog.vtLngTB(self.name)
                                #self.docSynch.AddProcResponse(self.iAct-self.iReqOpen,None,synchNode)
                                #self.docSynch.SetProcResponseAdd(-1)
                                #self.lstAdd.append((k,level,parID,self.doc.cloneNode(synchNode,True)))
                        else:
                            bIs2Add=True
                        if bIs2Add:
                            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                vtLog.vtLngCur(vtLog.DEBUG,
                                        'id:%d;add'%\
                                        (k),origin=self.name)
                            self.lstAdd.append((k,level,parID,synchNode))
                            self.iCount+=1     # 070201:wro needed?
                    else:
                        if long(parID)>-2:
                            try:
                                #k=long(id)
                                docNode=self.doc.GetId(k)[1]
                                try:
                                    self.docKeys.remove(k)
                                    self.lstOffCont[k]
                                    # identical fingerprints and offline edit
                                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                        vtLog.vtLngCur(vtLog.DEBUG,
                                            'id:%s idential fp offline edited'%(id),
                                            origin=self.name)
                                    self.dNotify['edit_offline'].append(k)
                                    #self.docSynch.AddProcResponse(self.iAct-self.iReqOpen,docNode,None)
                                    #self.docSynch.SetProcResponseEditOffline(1)
                                except:
                                    try:
                                        self.docSynch.AddProcResponse(self.iAct-self.iReqOpen,docNode,None,'identical',1)
                                        #self.docSynch.SetProcResponseIdentical(1)
                                        #self.docKeys.remove(k)
                                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                            vtLog.vtLngCS(vtLog.DEBUG,'id:%s identical fp'%id,origin=self.name)
                                        #self.dNotify['ident'].append(id)
                                        if len(parID)>0:
                                            idPar=self.doc.getKey(self.doc.getParent(docNode))
                                            if long(idPar)!=long(parID):
                                                docIds=self.doc.getIds()
                                                #print self.docIds
                                                docKeys=docIds.GetIDs()
                                                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                                    vtLog.vtLngCur(vtLog.DEBUG,
                                                            'identical but move id:%s parID:%s remoteParID:%s'%(id,idPar,parID),
                                                            origin=self.name)
                                                self.lstMove.append((parID,id))
                                        else:
                                            if vtLog.vtLngIsLogged(vtLog.WARN):
                                                vtLog.vtLngCur(vtLog.WARN,
                                                    'identical unable to check correct position id:%s'%(id),
                                                    origin=self.name)
                                    except:
                                        if vtLog.vtLngIsLogged(vtLog.ERROR):
                                            vtLog.vtLngCur(vtLog.ERROR,
                                                'id:%s some fault;%s'%(id,traceback.format_exc()),
                                                origin=self.name)
                            except:
                                if vtLog.vtLngIsLogged(vtLog.ERROR):
                                    vtLog.vtLngCS(vtLog.ERROR,
                                        'id:%s can not be removed;%s'%(id,traceback.format_exc()),
                                        origin=self.name)
            except:
                vtLog.vtLngTB(self.name)
            self.doc.release()
            self.iAct+=1
            #if self.iReqOpen>0:
            #    self.iReqOpen-=1
            #else:
            #    self.iReqOpen=0
            self.iReqOpen-=1
            self.semProc.release()
            #if self.iReqOpen<=0:
            #    self.bAllRemoteGot=True
            #else:
            #    self.__proc__()
            self.__proc__()
        except:
            vtLog.vtLngTB(self.name)
    def __procDel__(self):
        try:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'docKeys:%s'%
                        (vtLog.pformat(self.docKeys)),self.name)
            #vtLog.CallStack('')
            # delete
            self.doc.acquire()
            #if vtLog.vtLngIsLogged(vtLog.INFO):
            #    vtLog.vtLngCur(vtLog.INFO,'del:%s'%(self.docKeys),
            #            origin=self.name)
            i=0
            for k in self.docKeys:
                docNode=self.doc.GetId(k)[1]
                if docNode is not None:
                    try:
                        #vtLog.CallStack(k)
                        #docNode=self.doc.GetId(k)[1]
                        #print docNode
                        iRes=1
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCS(vtLog.DEBUG,
                                    'id:%s'%(k),
                                    origin=self.name)
                        self.docSynch.AddProcResponse(self.iAct+i,docNode,None,'del',iRes)
                        iRes=self.doc.__synchDel__(docNode)
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCS(vtLog.DEBUG,'id:%s;iRes"%d'%(k,iRes),
                                    origin=self.name)
                        #self.docSynch.DelProc(i,docNode)
                        #self.docSynch.SetProcDelResult(iRes)
                        if iRes>0:
                            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                vtLog.vtLngCS(vtLog.DEBUG,
                                        'del id:%s'%(k),
                                        origin=self.name)
                            #self.dNotify['del'].append(k)
                        else:
                            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                vtLog.vtLngCS(vtLog.DEBUG,
                                        'del id:%s'%(k),
                                        origin=self.name)
                            #self.dNotify['del_fault'].append(k)
                    except:
                        vtLog.vtLngCur(vtLog.ERROR,
                                'id:%s can not be removed;%s'%(k,traceback.format_exc()),
                                origin=self.name)
                        vtLog.vtLngTB(self.name)
                else:
                    if vtLog.vtLngIsLogged(vtLog.ERROR):
                            vtLog.vtLngCur(vtLog.ERROR,
                                'id:%d can not be removed'%(k),
                                origin=self.name)
                i+=1
            self.doc.release()
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'finished',self.name)
        except:
            vtLog.vtLngTB(self.name)
    def __chkNodePresent(self,id):
        docNode=self.doc.GetId(id)[1]
        return docNode is None
    def __procAdd__(self):
        try:
            # add
            def cmpAdd(a,b):
                return int(long(a[1])-long(b[1]))
            self.lstAdd.sort(cmpAdd)
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'',origin=self.name)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'add:%s'%(vtLog.pformat(self.lstAdd)),
                        origin=self.name)
            for tup in self.lstAdd:
                self.doc.NotifySynchProc(self.iAct,self.iCount)
                #self.__doWaitCheckDone__(self.__chkNodePresent,long(tup[2]))
                docNode=self.doc.GetIdStr(tup[2])[1]
                #print self.doc.getIds().GetIDs()
                #print tup,docNode is not None
                if docNode is not None:
                    self.doc.acquire()
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,
                            'parID:%s;id:%d'%(tup[2],tup[0]),
                            origin=self.name)
                        #vtLog.vtLngCur(vtLog.DEBUG,'parID:%s node:%s'%(tup[2],tup[3]),
                        #    origin=self.name)
                    iAdd=self.doc.__synchAdd__(tup[2],tup[3])
                    #self.docSynch.AddProc(self.iAct,tup[2],tup[3],iAdd)
                    self.doc.release()
                    if iAdd>0:
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCur(vtLog.DEBUG,
                                'parID:%s;id:%d;level:%s'%(tup[2],tup[0],tup[1]),
                                origin=self.name)
                        self.docSynch.AddProcResponse(self.iAct,None,tup[3],'add',iAdd,tup[2])
                        #self.dNotify['add'].append(tup[0])
                    else:
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCur(vtLog.DEBUG,
                                'id:%s'%(tup[2]),
                                origin=self.name)
                        #self.dNotify['add_fault'].append(tup[0])
                    #vtLog.vtLngCur(vtLog.DEBUG,''%(),self.name)
                self.iAct+=1
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'finished',self.name)
        except:
            vtLog.vtLngTB(self.name)
    def __procMove__(self):
        # location on server has changed
        try:
            #vtLog.CallStack('')
            # move
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'',
                        origin=self.name)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'lstMove:%s'%
                        (vtLog.pformat(self.lstMove)),self.name)
            i=0
            for parID,id in self.lstMove:
                try:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,
                            'parID:%s id:%s'%(parID,id),
                            origin=self.name)
                    parNode=self.doc.GetIdStr(parID)[1]
                    node=self.doc.GetIdStr(id)[1]
                    iRes=self.doc.__synchMove__(parNode,node)
                    #self.docSynch.MoveProc(i,parID,id,node,iRes)
                    if iRes>0:
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCur(vtLog.DEBUG,
                                'id:%s'%(id),
                                origin=self.name)
                        self.docSynch.AddProcResponse(self.iAct+i,node,None,'move',iRes,parID)
                        #self.dNotify['move'].append(id)
                    else:
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCur(vtLog.DEBUG,
                                'fault id:%s'%(id),
                                origin=self.name)
                        #self.dNotify['move_fault'].append(id)
                except:
                    self.docSynch.MoveProc(i,parID,id,None,-1)
                    if vtLog.vtLngIsLogged(vtLog.ERROR):
                        vtLog.vtLngCur(vtLog.ERROR,
                            'fault id:%s;%s'%(parID,id,traceback.format_exc()),
                            origin=self.name)
                    #self.dNotify['move_fault'].append(id)
                i+=1
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'finished',self.name)
        except:
            vtLog.vtLngTB(self.name)
    def __procAddOffLine__(self):
        try:
            def cmpAddNode(aNode,bNode):
                return self.doc.getLevel(aNode)-self.doc.getLevel(bNode)
            #vtLog.CallStack('')
            # add offline added to server
            if self.lstOffLineAdd is None:
                lstAdd=self.lstOffline.values()
                if vtLog.vtLngIsLogged(vtLog.INFO):
                    vtLog.vtLngCur(vtLog.INFO,'started;count:%d'%(len(lstAdd)),
                        origin=self.name)
                lstAdd.sort(cmpAddNode)
                self.lstOffLineAdd=[self.doc.getKeyNum(node) for node in lstAdd]
            #if vtLog.vtLngIsLogged(vtLog.DEBUG):
            #    vtLog.vtLngCur(vtLog.DEBUG,'add ids:%'%(vtLog.pformat(lstAdd)),
            #        origin=self.name)
            try:
                if len(self.lstOffLineAdd)>0:
                    node=self.doc.getNodeByIdNum(self.lstOffLineAdd[0])
                    if node is None:
                        self.iAct+=1
                        del self.lstOffLineAdd[0]
                        self.__procAddOffLine__()
                        return
                    self.doc.NotifySynchProc(self.iAct,self.iCount)
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'iAct:%d,iCount:%s'%(self.iAct,self.iCount),self.name)
                    iAdd=self.doc.__synchAdd2Srv__(node)#self.lstOffline[k])
            except:
                if vtLog.vtLngIsLogged(vtLog.ERROR):
                    vtLog.vtLngCS(vtLog.ERROR,
                            'proc add offline %s'%(traceback.format_exc()),
                            origin=self.name)
                self.iAct+=1
                del self.lstOffLineAdd[0]
                self.__procAddOffLine__()
            return
            for node in lstAdd:
                try:
                    #if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    #    vtLog.vtLngCur(vtLog.DEBUG,'add node:%s'%(node),
                    #        origin=self.name)
                    self.doc.NotifySynchProc(self.iAct,self.iCount)
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'iAct:%d,iCount:%s'%(self.iAct,self.iCount),self.name)
                    iAdd=self.doc.__synchAdd2Srv__(node)#self.lstOffline[k])
                    #self.docSynch.AddOfflineProc(self.iAct,node,iAdd)
                    #vtLog.vtLngCur(vtLog.DEBUG,'FIXME;check add respone from server',self)
                    #idPar=self.doc.getKey(self.doc.getParent(node))
                    #if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    #    vtLog.vtLngCur(vtLog.DEBUG,'iAct:%d,iCount:%s;AddprocResponse'%(self.iAct,self.iCount),self.name)
                    #self.docSynch.AddProcResponse(self.iAct,node,None,'add_offline',iAdd,idPar)
                    #if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    #    vtLog.vtLngCur(vtLog.DEBUG,
                    #        'act:%d;%s'%(self.iAct,node),
                    #        origin=self.name)
                    #self.dNotify['add_offline'].append(node)
                    #self.iAct+=1
                except:
                    if vtLog.vtLngIsLogged(vtLog.ERROR):
                        vtLog.vtLngCS(vtLog.ERROR,
                            'proc add offline %s'%(traceback.format_exc()),
                            origin=self.name)
                    self.iAct+=1
                    
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'finished',
                    origin=self.name)
        except:
            vtLog.vtLngTB(self.name)
    def __procAddNodeResponse__(self,sOldID,sNewID,sRes):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'sIdOld:%s;sIdNew:%s;res:%s'%(sOldID,sNewID,sRes),self.name)
        try:
            iOldID=long(sOldID)
            if iOldID in self.lstOffline:
                self.semProc.acquire()
                self.iTime=0
                self.semProc.release()
                nodeOld=self.doc.getNodeById(sOldID)
                nodeNew=self.doc.getNodeById(sNewID)
                node=nodeNew
                idPar=self.doc.getKey(self.doc.getParent(node))
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'iAct:%d,iCount:%s'%(self.iAct,self.iCount),self.name)
                self.docSynch.AddProcResponse(self.iAct,node,None,'add_offline',self.iAct,idPar)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,
                                'act:%d'%(self.iAct),
                                origin=self.name)
                #self.dNotify['add_offline'].append(node)
                del self.lstOffLineAdd[0]
                self.iAct+=1
                self.__procAddOffLine__()
            else:
                return
        except:
            vtLog.vtLngTB(self.name)
    def __procSetNodeResponse__(self,sID,sIDNew,sResp):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%s;idNew:%s;resp:%s'%
                    (sID,sIDNew,sResp),self.name)
        try:
            pass
        except:
            vtLog.vtLngTB(self.name)
    def __procDelNodeResponse__(self,sID,sResp):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%s;resp:%s'%
                    (sID,sResp),self.name)
        try:
            pass
        except:
            vtLog.vtLngTB(self.name)
    def __procMoveNodeResponse__(self,sID,sParID,sResp):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%s;idPar:%s;resp:%s'%
                    (sID,sParID,sResp),self.name)
        try:
            pass
        except:
            vtLog.vtLngTB(self.name)
    def GetAddOffline(self):
        try:
            return self.dNotify['add_offline']
        except:
            return []
    def GetEditOffline(self):
        try:
            return self.dNotify['edit_offline']
        except:
            return []
    def GetNotify(self):
        try:
            return self.dNotify,self.dNotifyTrans,self.running
        except:
            return {},self.dNotifyTrans,self.running
    def GetSynchFN(self):
        return self.synchFN
    def IsChanged(self):
        return self.bChanged
    def CheckChanged(self):
        try:
            self.bChanged=False
            for k in self.dNotify.keys():
                if k=='ident':
                    continue
                elif len(self.dNotify[k])>0:
                    self.bChanged=True
        except:
            pass
    def __chkAddOffLine(self,iCount):
        return self.iAct<iCount
    def __doWaitCheckDone__(self,func,*args,**kwargs):
        self.iUpdate=0
        self.iTime=0.0
        while func(*args,**kwargs) and (self.keepGoing==True):
            #print self.iAct,iCount,self.iAct<iCount
            self.semProc.acquire()
            self.iTime+=self.TIME_SLEEP
            self.semProc.release()
            if self.iTime>self.TIME_ABORT:
                self.keepGoing=False
                if vtLog.vtLngIsLogged(vtLog.ERROR):
                    vtLog.vtLngCur(vtLog.ERROR,
                        'synch add offline time out (%d)'%self.iTime,
                        self.name)
            if self.iUpdate>self.TIME_UPDATE:
                self.semProc.acquire()
                self.doc.NotifySynchProc(self.iAct,self.iCount)
                self.semProc.release()
                self.iUpdate=-1
            self.iUpdate+=1
            time.sleep(self.TIME_SLEEP)
        return not func(*args,**kwargs)
    def Run(self):
        self.bChanged=False
        #vtLog.CallStack('')
        vtLog.vtLngCur(vtLog.INFO,'Synch:run start',origin=self.name)
        try:
            zStart=time.clock()
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'acquire',origin=self.name)
            #self.doc.acquire('thread')
            self.doc.acquire('dom')
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'acquired',origin=self.name)
            
            # build synch proc file
            self.doc.acquire('synch_result')
            self.docSynch=vNetXmlSynchProc(self.doc,True)
            self.synchFN=self.docSynch.GetFN()
        except:
            vtLog.vtLngTB(self.name)
            self.doc.NotifySynchAborted(self.bChanged)
            #self.doc.release('thread')
            self.doc.release('dom')
            self.doc.release('synch_result')
            self.running=False
            return
        try:
            #vtLog.CallStack(self.synchFN)
            
            self.bAllRemoteGot=False
            self.iCount=100
            self.doc.NotifySynchStart(self.iCount)
            #time.sleep(40)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'GetKey start',origin=self.name)
            self.doc.GetKeys()
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'GetKey send',origin=self.name)
            iTime=0
            while (self.keys is None) or (not self.doc.IsFlag(self.doc.OPENED)):
                #self.doc.NotifySynchStart(self.iCount)
                #if self.keys is None:
                #    print 'wait remote keys'
                #if self.doc.IsFlag(self.doc.OPENED)==False:
                #    print 'wait file to be opened'
                iTime+=self.TIME_SLEEP
                if iTime>self.TIME_ABORT:
                    self.keepGoing=False
                else:
                    if iTime%self.TIME_TIMEOUT==0:
                        if self.keys is None:
                            self.doc.GetKeys()
                            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                vtLog.vtLngCur(vtLog.DEBUG,'get keys',
                                    origin=self.name)
                if self.keepGoing == False:
                    break
                time.sleep(self.TIME_SLEEP)
            vtLog.vtLngCur(vtLog.INFO,'time elapsed:%f;keys received'%(time.clock()-zStart),self.name)
            if self.keepGoing == False:
                self.CheckChanged()
                self.doc.NotifySynchAborted(self.bChanged)
                #self.doc.release('thread')
                self.doc.release('dom')
                self.doc.release('synch_result')
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'release',
                        origin=self.name)
                self.running=False
                self.docSynch.Close()
                return
            self.iKeyCount=len(self.keys)
            iDocElemCount=self.doc.GetElementCount()
            iDocAttrCount=self.doc.GetElementAttrCount()
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'local keys:%s'%(self.docKeys),
                        origin=self.name)
        
            if self.verbose>0:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,
                        'doc count elem:%5d count attr:%5d ids:%5d'%(iDocElemCount,iDocAttrCount,len(self.docKeys)),
                        origin=self.name)
            #print iDocElemCount,iDocAttrCount,self.docIds#len(self.docIds)
            #print self.docKeys
            self.lstOffline={}
            self.lstOffCont={}
            docNode=self.doc.doc.getRootElement()
            self.__clearOffline__(docNode)
        except:
            vtLog.vtLngTB(self.name)
        try:
            vtLog.vtLngCur(vtLog.INFO,'time elapsed:%f;offline cleared'%(time.clock()-zStart),self.name)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'docKeys:%s'%(vtLog.pformat(self.docKeys)),self.name)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'offline;edit:%s;added:%s'%(self.lstOffCont,self.lstOffline),self.name)
            #print self.lstOffCont
            #print self.lstOffline
            #iDocElemCount=self.doc.GetElementCount()
            #iDocAttrCount=self.doc.GetElementAttrCount()
            
            self.iAct=0
            self.iReqAct=0
            self.iReqOpen=0
            self.iCount=0
            iCountOffLineAdd=len(self.lstOffline.keys())
            self.iCount+=iCountOffLineAdd
            
            self.offKeys=self.lstOffline.keys()
            self.lstAdd=[]
            self.lstMove=[]
            
            #print '**************************************'
            #print self.keys
            #print '**************************************'
            iElemCount=len(self.docKeys)
            self.iCount+=iElemCount
            iLen=len(self.keys)
            if self.iCount<iLen:
                self.iCount=iLen
        except:
            vtLog.vtLngTB(self.name)
            
        self.doc.release('dom')
        try:
            self.lstOffLineAdd=None
            self.__procAddOffLine__()
            iRes=self.__doWaitCheckDone__(self.__chkAddOffLine,iCountOffLineAdd)
            vtLog.vtLngCur(vtLog.INFO,'addOffline finished:%d;time elapsed:%f'%(iRes,time.clock()-zStart),self.name)
            #self.iAct=0
            if self.keepGoing:
                try:
                    self.__proc__()
                except:
                    vtLog.vtLngTB(self.name)
            self.iTime=0
            #self.iAct=0
            iUpdate=0
            vtLog.vtLngCur(vtLog.INFO,'time elapsed:%f'%(time.clock()-zStart),self.name)
            while (self.bAllRemoteGot==False) and (self.keepGoing==True):
                #print 'wait'
                self.semProc.acquire()
                if iUpdate>self.TIME_UPDATE:
                    self.doc.NotifySynchProc(self.iAct,self.iCount)
                    iUpdate=-1
                iUpdate+=1
                self.iTime+=self.TIME_SLEEP
                self.semProc.release()
                if self.iTime>self.TIME_TIMEOUT:
                    self.keepGoing=False
                    if vtLog.vtLngIsLogged(vtLog.INFO):
                        vtLog.vtLngCS(vtLog.INFO,
                            'synch time out (%d)'%self.iTime,
                            origin=self.name)
                time.sleep(self.TIME_SLEEP)
            vtLog.vtLngCur(vtLog.INFO,'time elapsed:%f;remote data got and analysed'%(time.clock()-zStart),self.name)
            if self.keepGoing:
                self.__procAdd__()
                self.__procMove__()
                self.__procDel__()
                #self.__procAddOffLine__()   # 070201:wro
                
                #resolve multiple edit
                if len(self.dNotify['edit_resolve'])>0:
                    #print self.dNotify['edit_resolve']
                    #dlg=self.doc.GetEditResolveDlg()
                    wx.MutexGuiEnter()
                    try:
                        dlg=self.doc.GetEditResolveDlg()
                        lstEdit=self.dNotify['edit_resolve']
                        dlg.SetEdit2Resolve(self.doc,lstEdit)
                        dlg.Show()
                    except:
                        pass
                    wx.MutexGuiLeave()
                    while 1:
                        wx.MutexGuiEnter()
                        try:
                            bShown=dlg.IsShown()
                        except:
                            bShown=True
                        wx.MutexGuiLeave()
                        if bShown==False:
                            break
                        time.sleep(0.5)
                    #print 'closed'
                    for tup in lstEdit:
                        if tup['valid']==2:
                            self.__doEdit__(tup['local'],tup['remote'])
                        elif tup['valid']==1:
                            # keep local
                            self.dNotify['edit_offline'].append(tup['local'])
                            pass
                    pass
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,
                        'synch add offline',
                        origin=self.name)
                for node in self.GetAddOffline():
                    par=self.doc.getParent(node)
                    self.doc.thdAdd.ScheduleAdd(par,node)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,
                        'synch add offline start size:%s'%self.doc.thdAdd.iCount,
                        origin=self.name)
                self.doc.thdAdd.Apply()
                self.iTime=0
                while self.doc.thdAdd.IsRunning()  and (self.keepGoing==True):
                    self.iTime+=0.1
                    if self.iTime>30:
                        self.keepGoing=False
                        if vtLog.vtLngIsLogged(vtLog.ERROR):
                            vtLog.vtLngCS(vtLog.ERROR,
                                'synch add offline time out (%d)'%self.iTime,
                                origin=self.name)
                    if self.iTime%1==0.0:
                        self.semProc.acquire()
                        self.doc.NotifySynchProc(self.iAct+self.doc.thdAdd.iAct,self.iCount)
                        self.semProc.release()
                    time.sleep(0.1)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,
                        'synch add offline finished :%d'%self.keepGoing,
                        origin=self.name)
                if self.keepGoing:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,
                            'synch edit offline',
                            origin=self.name)
                    for tmp in self.GetEditOffline():
                        if type(tmp)==types.LongType:
                            node=self.doc.getNodeByIdNum(id)
                        else:
                            node=tmp
                        if node is not None:
                            self.doc.thdEdit.ScheduleEdit(node)
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,
                            'synch edit offline size:%d'%self.doc.thdEdit.iCount,
                            origin=self.name)
                    self.doc.thdEdit.Apply()
                    self.iTime=0
                    #print self.doc.thdEdit.IsRunning()
                    while self.doc.thdEdit.IsRunning()  and (self.keepGoing==True):
                        self.iTime+=0.1
                        #if self.iTime>30:
                        #    self.keepGoing=False
                        #    if vtLog.vtLngIsLogged(vtLog.INFO):
                        #        vtLog.vtLngCur(vtLog.INFO,
                        #            'synch edit offline time out (%d)'%self.iTime,
                        #            origin=self.name)
                        #    vtLog.PrintMsg(_(u'synch edit offline time out (%d)')%self.iTime)
                        if self.iTime%1==0.0:
                            self.semProc.acquire()
                            self.doc.NotifySynchProc(self.iAct+self.doc.thdEdit.iAct,self.iCount)
                            self.semProc.release()
                        time.sleep(0.1)
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,
                            'synch edit offline finished :%d'%self.keepGoing,
                            origin=self.name)
            vtLog.vtLngCur(vtLog.INFO,'time elapsed:%f;add edit finished'%(time.clock()-zStart),self.name)
            #print 'finished'
            #while self.iAct<self.iCount:
            #    self.doc.NotifySynchProc(self.iAct,self.iCount)
                
            #    self.iAct+=1
            self.CheckChanged()
            #self.doc.AlignDoc()
            self.doc.acquire('dom')
            try:
                self.doc.Build()
            except:
                pass
            self.doc.release('dom')
            self.doc.Save()
            #self.doc.release('thread')
            self.running = False
            #  close and remove synch proc
            #self.docSynch.AlignDoc()
            self.docSynch.Save()
            self.docSynch.Close()
            self.doc.release('synch_result')
            del self.docSynch
            self.docSynch=None
            del self.dNotify
            self.dNotify={}
            vtLog.vtLngCur(vtLog.INFO,'time elapsed:%f;synch finished'%(time.clock()-zStart),self.name)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'release',origin=self.name)
            if self.keepGoing:
                self.doc.NotifySynchFinished(self.bChanged)
            else:
                self.doc.NotifySynchAborted(self.bChanged)
            vtLog.vtLngCur(vtLog.INFO,'Synch:run finished',origin=self.name)
            return
        except:
            vtLog.vtLngTB(self.name)
        self.keepGoing = self.running = False
        #self.doc.release('dom')
        self.doc.release('synch_result')
        self.doc.NotifySynchAborted(self.bChanged)
        vtLog.vtLngCur(vtLog.INFO,'Synch:run aborted',origin=self.name)
