#Boa:Dialog:vtNetSecCfgDialog
#----------------------------------------------------------------------------
# Name:         vtNetSecCfgDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060416
# CVS-ID:       $Id: vtNetSecCfgDialog.py,v 1.18 2007/08/01 15:42:28 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import vidarc.tool.net.vtNetSecLoginAliasPanel
import wx.lib.filebrowsebutton

import os.path

from vidarc.tool.net.vtNetSecLoginAliases import vtNetSecLoginAliases
from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.config.vcCust as vcCust

def create(parent):
    return vtNetSecCfgDialog(parent)

wxEVT_VTNETSEC_CFG_DIALOG_APPLY=wx.NewEventType()
vEVT_VTNETSEC_CFG_DIALOG_APPLY=wx.PyEventBinder(wxEVT_VTNETSEC_CFG_DIALOG_APPLY,1)
def EVT_VTNETSEC_CFG_DIALOG_APPLY(win,func):
    win.Connect(-1,-1,wxEVT_VTNETSEC_CFG_DIALOG_APPLY,func)
class vtNetSecCfgDialogApply(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTNETSEC_CFG_DIALOG_APPLY(<widget_name>, self.OnApply)
    """
    def __init__(self,obj,bAdd,appl):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTNETSEC_CFG_DIALOG_APPLY)
        self.obj=obj
        self.bAdd=bAdd
        self.appl=appl
    def GetObject(self):
        return self.obj
    def IsAdd(self):
        return self.bAdd
    def GetAppl(self):
        return self.appl

wxEVT_VTNETSEC_CFG_DIALOG_CANCEL=wx.NewEventType()
vEVT_VTNETSEC_CFG_DIALOG_CANCEL=wx.PyEventBinder(wxEVT_VTNETSEC_CFG_DIALOG_CANCEL,1)
def EVT_VTNETSEC_CFG_DIALOG_CANCEL(win,func):
    win.Connect(-1,-1,wxEVT_VTNETSEC_CFG_DIALOG_CANCEL,func)
class vtNetSecCfgDialogCancel(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTNETSEC_CFG_DIALOG_CANCEL(<widget_name>, self.OnCancel)
    """
    def __init__(self,obj,bAdd,appl):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTNETSEC_CFG_DIALOG_CANCEL)
        self.obj=obj
        self.bAdd=bAdd
        self.appl=appl
    def GetObject(self):
        return self.obj
    def IsAdd(self):
        return self.bAdd
    def GetAppl(self):
        return self.appl

[wxID_VTNETSECCFGDIALOG, wxID_VTNETSECCFGDIALOGCBAPPLY, 
 wxID_VTNETSECCFGDIALOGCBCANCEL, wxID_VTNETSECCFGDIALOGDBBDIR, 
 wxID_VTNETSECCFGDIALOGPNALIAS, 
] = [wx.NewId() for _init_ctrls in range(5)]

class vtNetSecCfgDialog(wx.Dialog,vtXmlDomConsumer):
    VERBOSE=0
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.dbbDir, 1, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT | wx.TOP)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.pnAlias, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.BOTTOM | wx.ALIGN_CENTER)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=5, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTNETSECCFGDIALOG,
              name=u'vtNetSecCfgDialog', parent=prnt, pos=wx.Point(448, 240),
              size=wx.Size(307, 274),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE | wx.STAY_ON_TOP,
              title=u'vtNet Sec Config Dialog')
        self.SetClientSize(wx.Size(299, 247))

        self.dbbDir = wx.lib.filebrowsebutton.DirBrowseButton(buttonText='...',
              dialogTitle=_('Select local directory'),
              id=wxID_VTNETSECCFGDIALOGDBBDIR, labelText=_('local directory:'),
              parent=self, pos=wx.Point(4, 4), size=wx.Size(291, 29),
              startDirectory='.', style=0)
        self.dbbDir.SetMinSize(wx.Size(-1, -1))

        self.pnAlias = vidarc.tool.net.vtNetSecLoginAliasPanel.vtNetSecLoginAliasPanel(id=wxID_VTNETSECCFGDIALOGPNALIAS,
              name=u'pnAlias', parent=self, pos=wx.Point(4, 45),
              size=wx.Size(291, 161), style=0)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(id=wxID_VTNETSECCFGDIALOGCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'),
              name=u'cbApply', parent=self, pos=wx.Point(57, 214),
              size=wx.Size(76, 30), style=0)
        self.cbApply.SetMinSize(wx.Size(-1, -1))
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VTNETSECCFGDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(id=wxID_VTNETSECCFGDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(165, 214),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.SetMinSize(wx.Size(-1, -1))
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VTNETSECCFGDIALOGCBCANCEL)

        self._init_sizers()

    def __init__(self, parent):
        vtXmlDomConsumer.__init__(self)
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        self._init_ctrls(parent)
        try:
            icon = wx.EmptyIcon()
            icon.CopyFromBitmap(vtArt.getBitmap(vtArt.Settings))
            self.SetIcon(icon)
        except:
            vtLog.vtLngTB(self.sOrigin)
        self.docCfg=None
        self.bAdd=False
        self.appl=None
        #self.sDN=os.path.expanduser('~')
        self.sDN=vcCust.USR_LOCAL_DN
        self.sFallbackDN=self.sDN
        
        self.lChild=['config']
        self.fgsData.Fit(self)
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        vtXmlDomConsumer.__del__(self)
    def ShutDown(self):
        self.docCfg=None
    def SetDoc(self,doc,bNet=False):
        #vtLog.CallStack('')
        vtXmlDomConsumer.SetDoc(self,doc)
        self.pnAlias.SetDoc(doc)
    def SetLocalDN(self,dn):
        vtLog.vtLngCurWX(vtLog.INFO,'DN:%s'%(dn),self)
        self.sFallbackDN=dn
    def GetLoginNode(self):
        self.pnAlias.GetLoginNode()
    def SetCfg(self,doc,lst,appls):
        vtLog.vtLngCurWX(vtLog.INFO,'',self)
        if self.VERBOSE:
            vtLog.CallStack('')
            print lst
            print appls
        try:
            self.node=None
            self.docCfg=doc
            if lst is not None:
                self.lChild=lst
            o=vtNetSecLoginAliases(appls)
            self.pnAlias.SetNetSecLogin(o)
            self.pnAlias.SetLoginDoc(doc)
        except:
            vtLog.vtLngTB(self.GetName())
        try:
            #self.sDN=os.path.expanduser('~')
            self.sDN=self.sFallbackDN
            if self.docCfg is not None:
                try:
                    if self.docCfg.acquire('dom',blocking=False)==False:
                        vtLog.vtLngCurWX(vtLog.ERROR,'lock can not be acquired, abort...',self)
                        return
                    self.node=self.docCfg.getChildByLstForced(self.docCfg.getRoot(),self.lChild)
                    if self.VERBOSE:
                        #print self.docCfg.getRoot()
                        print self.node
                    if self.node is not None:
                        sDN=self.docCfg.getNodeText(self.node,'path')
                        if len(sDN)>0:
                            self.sDN=sDN
                        try:
                            os.path.exists(self.sDN)
                        except:
                            vtLog.vtLngTB(self.GetName())
                            self.sDN=self.sFallbackDN#os.path.expanduser('~')
                        nodeTmp=self.docCfg.getChildForced(self.node,'connection')
                    else:
                        nodeTmp=None
                except:
                    vtLog.vtLngTB(self.GetName())
                    nodeTmp=None
                self.docCfg.release('dom')
                self.pnAlias.SetLoginNode(nodeTmp)
            self.dbbDir.SetValue(self.sDN)
        except:
            vtLog.vtLngTB(self.GetName())
    def SaveSelectedConnection(self):
        try:
            if self.docCfg is None:
                return
            if self.docCfg.acquire('dom',blocking=False)==False:
                vtLog.vtLngCurWX(vtLog.ERROR,'lock can not be acquired, abort...',self)
            if self.node is not None:
                nodeTmp=self.docCfg.getChildForced(self.node,'connection')
                self.pnAlias.GetLoginNode(nodeTmp)
                self.docCfg.AlignNode(self.node,iRec=1)
        except:
            vtLog.vtLngTB(self.GetName())
        self.docCfg.release('dom')
    def BindEvents(self,funcApply=None,funcCancel=None):
        if funcApply is not None:
            EVT_VTNETSEC_CFG_DIALOG_APPLY(self,funcApply)
        if funcCancel is not None:
            EVT_VTNETSEC_CFG_DIALOG_CANCEL(self,funcCancel)
    def __endDialog__(self,bApply):
        if bApply:
            wx.PostEvent(self,vtNetSecCfgDialogApply(self,self.bAdd,self.appl))
        else:
            wx.PostEvent(self,vtNetSecCfgDialogCancel(self,self.bAdd,self.appl))
        if self.IsModal():
            if bApply:
                self.EndModal(1)
            else:
                self.EndModal(0)
        else:
            self.Show(False,False)
    def OnCbApplyButton(self, event):
        if event is not None:
            event.Skip()
        vtLog.vtLngCurWX(vtLog.INFO,'',self)
        self.sDN=self.dbbDir.GetValue()
        try:
            os.makedirs(self.sDN)
        except:
            pass
        try:
            if self.docCfg is None:
                self.__endDialog__(True)
                return
            if self.docCfg.acquire('dom',blocking=False)==False:
                vtLog.vtLngCurWX(vtLog.ERROR,'lock can not be acquired, abort...',self)
                return
            if self.node is None:
                if self.docCfg is not None:
                    self.node=self.docCfg.getChildByLstForced(self.docCfg.getRoot(),self.lChild)
            if self.node is None:
                self.docCfg.release('dom')
                self.__endDialog__(True)
                return
            self.docCfg.setNodeText(self.node,'path',self.sDN)
            nodeTmp=self.docCfg.getChildForced(self.node,'connection')
            self.pnAlias.SelectSelectedConnection()
            self.pnAlias.GetLoginNode(nodeTmp)
            self.docCfg.AlignNode(self.node,iRec=1)
        except:
            vtLog.vtLngTB(self.GetName())
        self.docCfg.release('dom')
        self.__endDialog__(True)
    def OnCbCancelButton(self, event):
        event.Skip()
        self.__endDialog__(False)
    def GetObjConnections(self):
        return self.pnAlias.GetObjConnections()
    def GetConnections(self):
        return self.pnAlias.GetConnections()
    def SelectConnection(self,sName):
        return self.pnAlias.SelectConnection(sName)
    def GetConnection(self,sName=None):
        return self.pnAlias.GetConnection(sName)
    def GetConnectionAppl2Cfg(self):
        return self.pnAlias.GetConnectionAppl2Cfg()
    def ConfigAppl(self,appl):
        return self.pnAlias.ConfigAppl(appl)
    def ConfigAdd(self):
        self.pnAlias.ConfigAdd()
    def ConfigDel(self,sConn):
        self.pnAlias.ConfigDel(sConn)
        wx.CallAfter(self.OnCbApplyButton,None)
    def GetDN(self):
        vtLog.vtLngCurWX(vtLog.INFO,'DN:%s'%(self.sDN),self)
        return self.sDN
    def Show(self,bAdd,flag=True,appl=None):
        self.bAdd=bAdd
        self.appl=appl
        vtLog.vtLngCurWX(vtLog.INFO,'bAdd:%s,flag:%s'%(`bAdd`,`flag`),self)
        if flag:
            self.pnAlias.UpdateData()
        wx.Dialog.Show(self,flag)
    def ShowModal(self,bAdd,appl):
        vtLog.vtLngCurWX(vtLog.ERROR,'',self)
        self.bAdd=bAdd
        self.appl=appl
        self.pnAlias.UpdateData()
        ret=wx.Dialog.ShowModal(self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'ret:%d'%(ret),self)
        return ret
        
