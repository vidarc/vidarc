#----------------------------------------------------------------------------
# Name:         vNetXmlClt.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtNetXmlClt.py,v 1.50 2013/12/09 08:06:54 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import thread,traceback
import time,random
import socket,binascii
import sys
if sys.version_info >= (2,6,0):
    import hashlib
    def newSha():
        return hashlib.sha1()
else:
    import sha
    def newSha():
        return sha.new()

import sys,string
from vidarc.tool.log import vtLog
#from vidarc.tool.net import vtNetSock
#from vidarc.tool.net.vtNetSock import *
from vidarc.tool.sock.vtSock import vtSock
from vidarc.tool.sec.vtSecXmlLogin import vtSecXmlLogin

#from vidarc.tool.vtThread import vtThread
from vidarc.tool.vtThreadEndless import vtThreadEndless
from vidarc.tool.xml.vtXmlDom import vtXmlDom

import __init__
if __init__.SSL:
    from OpenSSL import SSL
    from OpenSSL import tsafe
BUFFER_MAX=40*1024*1024

iSockNr=-1
def getSockNr():
    global iSockNr
    iSockNr+=1
    return iSockNr
    
class vtNetXmlCltSock(vtLog.vtLogMixIn,vtSock):
    #SSL=True
    SSL=__init__.SSL
    #def __init__(self,server,conn,adr,sID='',verbose=0):
        #vtLog.CallStack('')
    #    vtSock.__init__(self,server,conn,adr,sID,BUFFER_MAX,verbose=verbose)
    def __initCtrl__(self,*args,**kwargs):
        try:
            if self.SSL:
                #sys.stderr.write('\n%s'%(''.join(traceback.format_stack())))
                #self.conn.do_handshake()
                pass
        except:
            #vtLog.vtLngTB(self.__class__.__name__)
            pass
        self.oConn.AddFlagDict({
                0x0100:{'name':'ESTABLISHED','is':'IsEstablished','order':10},
                0x0200:{'name':'LOGGEDIN','is':'IsLoggedIn','order':11},
                0x0400:{'name':'LOGIN_ENCODED','is':'IsLoginEncoded','order':12},
                
                })
        self.bEstabilshed=False
        self.loggedin=False
        #self.bLoginSend=False
        self.oLogin=vtSecXmlLogin()
        self.encoded_passwd=False
        self.lCmds=[
            ('lock_node',self.__lock_node__),
            ('unlock_node',self.__unlock_node__),
            ('get_node',self.__get_node__),
            ('get_synch_node',self.__get_synch_node__),
            ('get_full_node',self.__get_node_full__),
            ('set_node',self.__set_node__),
            ('set_full_node',self.__set_node_full__),
            ('set_resp_full',self.__set_node_full_resp__),
            ('add_node',self.__add_node__),
            ('add_resp',self.__add_node_resp__),
            ('del_node',self.__del_node__),
            ('remove_node',self.__remove_node__),
            ('move_node',self.__move_node__),
            ('move_resp',self.__move_node_resp__),
            ('get_content',self.__get_content__),
            ('count_key',self.__count_key__),
            ('count_element',self.__count_element__),
            ('count_node',self.__count_node__),
            ('get_config',self.__get_config__),
            ('get_acl',self.__get_acl__),
            ('get_special',self.__get_special__),
            ('get_keys',self.__get_keys__),
            ('alias',self.__alias__),
            ('login',self.__login__),
            ('loggedin',self.__loggedin__),
            ('browse',self.__browse__),
            ('hypbrowse',self.__hyper_browse__),
            ('cmd',self.__cmd__),
            ('lock_request',self.__lock_request__),
            ('lockfull_request',self.__lock_full_request__),
            #('lockfull_release',self.__lock_full_release__),
            (u'sessionID',self.__sessionID__),
            (u'shutdown',self.__shutdown__)]
        #self.nodes2keep=[]
        #self.nodes2synch=[]
        #self.thdModify=vtThread(None,bPost=False)
        self._iSockNr=getSockNr()
        self.par=None
        self.appl_alias=''
        self.appl=kwargs.get('appl','')
        self.widLogging=None
        self.thdModify=vtThreadEndless(self.par,bPost=False)
        self.SetParent(kwargs.get('sockpar',None))
        vtSock.__initCtrl__(self,*args,**kwargs)
    def __logStack__(self):
        vtLog.vtLngCur(vtLog.DEBUG,''%(),self.GetOrigin(),level2skip=2)
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        try:
            del self.thdModify
        except:
            #vtLog.vtLngTB('del')
            pass
        #vtLog.CallStack('')
        pass
    def DoVerify(self,conn, cert, errnum, depth, ok):
        #traceback.print_stack()
        #sys.stderr.write('\n%s'%(''.join(traceback.format_stack())))
        vtLog.vtLngCur(vtLog.INFO,'ok:%d'%(ok),self.GetOrigin())
        return
        if ok:
            #self.SetServing(True)
            vtLog.PrintMsg(_('alias:%s SSL got certificate: %s')%(self.appl_alias,cert.get_subject()),self.widLogging)
            self.oConn.SetState(self.oConn.CONNECTED)
        else:
            #self.SetServing(False)
            #vtLog.vtLngCur(vtLog.ERROR,'errnum:%d'%(errnum),self.appl_alias)
            vtLog.vtLngCurCls(vtLog.ERROR,'errnum:%s;depth:%s;ok:%s'%(`errnum`,`depth`,`ok`),self.GetOrigin())
            self.oConn.SetState(self.oConn.CONNECTION_FLT)
        if self.IsServing()==False:
            vtLog.PrintMsg(_('alias:%s SSL verify fault')%(self.appl_alias),self.widLogging)
            #if self.par is not None:
            #    self.par.NotifyConnectionFault()
    def CheckPause(self):
        self._acquire()
        try:
            if self.paused!=self.pausing:
                self.paused=self.pausing
                if self.par is not None:
                    vtLog.vtLngCS(vtLog.INFO,
                                'pause=%s'%self.paused,origin=self.GetOrigin())
                    self.par.NotifyPause(self.paused)
                else:
                    vtLog.vtLngCS(vtLog.INFO,
                            'pause=%s'%self.paused,origin=self.GetOrigin())
        except:
            self.__logTB__()
            vtLog.vtLngCS(vtLog.ERROR,
                        traceback.format_exc(),origin=self.GetOrigin())
        self._release()
    def IsLoggedIn(self):
        return self.loggedin
    def HandleDataOld(self):
        try:
            #vtLog.vtLngCallStack(self,vtLog.DEBUG,'HandleData')
            if self.verbose>0:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCallStack(self,vtLog.DEBUG,
                                'HandleData; len:%8d %8d; act:%8d;start:%8d; end:%8d'%(len(self.data),self.len,self.iAct,self.iStart,self.iEnd),
                                origin=self.GetOrigin())
            if self.iStart==-1:
                self.iStart=string.find(self.data,self.startMarker,self.iAct)
                #self.iAct=self.iStart   # 060225
                #self.iStart+=self.iStartMarker
                if self.iStart>=0:
                    self.iAct=self.iStart+self.iStartMarker
            if self.iStart>=0:
                # start marker found
                try:
                    self.iEnd=string.find(self.data,self.endMarker,self.iAct)
                except:
                    pass
                #self.iAct=self.len
                if self.verbose>0:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCallStack(self,vtLog.DEBUG,
                                    'HandleData telegram found; len:%8d %08d; act:%8d;start:%8d; end:%8d'%(len(self.data),self.len,self.iAct,self.iStart,self.iEnd),
                                    origin=self.GetOrigin())
                if self.iEnd>0:
                    # end marker found, telegram is complete
                    self.iStart+=self.iStartMarker
                    val=self.data[self.iStart:self.iEnd]
                    #vtLog.vtLngCallStack(self,vtLog.DEBUG,'  '+val[:60])
                    for cmd in self.lCmds:
                        k=cmd[0]
                        iLen=len(k)
                        if val[:iLen]==k:
                            #cmd[1](val[iLen:].encode('ISO-8859-1'))
                            self.iTeleRcv+=1
                            cmd[1](val[iLen:])
                            break
                    self.iEnd+=self.iEndMarker
                    #self.data=self.data[self.iEnd:]   # 060225
                    #self.len-=self.iEnd               # 060225 
                    #self.iStart=self.iEnd=self.iAct=-1  # 060225
                    self.iAct=self.iEnd
                    self.iStart=self.iEnd=-1
                    if self.iAct>=self.len:
                        self.data=''
                        self.len=0
                        self.iAct=0
                        if self.verbose>0:
                            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                vtLog.vtLngCallStack(self,vtLog.DEBUG,
                                    'HandleData reduce buffer; len:%8d %8d; act:%8d;start:%8d; end:%8d'%(len(self.data),self.len,self.iAct,self.iStart,self.iEnd),
                                    origin=self.GetOrigin())
                        return 0
                    if self.verbose>0:
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCallStack(self,vtLog.DEBUG,
                                    'HandleData; len:%8d %8d; act:%8d;start:%8d; end:%8d'%(len(self.data),self.len,self.iAct,self.iStart,self.iEnd),
                                    origin=self.GetOrigin())    
                    return 1
                else:
                    self.data=self.data[self.iStart:]
                    self.iAct=0
                    self.len-=self.iStart
                    self.iStart=0
                    if self.verbose>0:
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCallStack(self,vtLog.DEBUG,
                                    'HandleData reduce buffer; len:%8d %8d; act:%8d;start:%8d; end:%8d'%(len(self.data),self.len,self.iAct,self.iStart,self.iEnd),
                                    origin=self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
        return 0
    def SetParent(self,par):
        vtLog.vtLngCur(vtLog.DEBUG,'%d'%(self._iSockNr),self.GetOrigin())
        self.par=par
        if hasattr(self,'thdModify'):
            self.thdModify.SetPostWid(par)
        self.SetOrigin()
        if self.par is None:
            self.widLogging=None
            return
        try:
            self.widLogging=vtLog.GetPrintMsgWid(self.par)
        except:
            self.widLogging=None
        vtLog.vtLngCur(vtLog.DEBUG,'%d'%(self._iSockNr),self.GetOrigin())
    def SetOrigin(self):
        try:
            if self.par is not None:
                #self.origin=''.join([self.par.GetOrigin(),'.',self.appl_alias])
                self.origin=''.join([self.par.GetOrigin(),'.',self.appl_alias,
                        '(%d)'%self._iSockNr])
            else:
                self.origin=self.appl_alias
                if len(self.origin)==0:
                    self.origin=self.__class__.__name__
        except:
            vtLog.vtLngTB(self.__class__.__name__)
            self.origin=self.__class__.__name__
        try:
            if hasattr(self,'thdModify'):
                self.thdModify.SetOrigin(self.origin)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def GetOrigin(self):
        return self.origin
    def SocketAborted(self):
        vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        if hasattr(self,'thdModify'):
            self.thdModify.Do(self.do__connectionClosed__,self.bEstabilshed,False)
            self.thdModify.Do(self.do__SocketAborted__)
            return
            self.thdModify.Stop()
            fTime=0
            zSleep=0.2
            while self.thdModify.IsRunning() and (fTime<30.0):
                time.sleep(zSleep)
                fTime+=zSleep
            self.thdModify.SetPostWid(None)
            del self.thdModify
        try:
            if self.par is not None:
                #self.thdModify.Do(self.do__connectionClosed__,self.bEstabilshed,True)
                self.par.__connectionClosed__(self.bEstabilshed)
                self.par.NotifyConnectionFault()
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.SetParent(None)
        #self.par=None
        del self.lCmds
        self.oLogin=None
        vtSock.SocketAborted(self)
    def do__SocketAborted__(self):
        try:
            self.par.NotifyConnectionFault()
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.thdModify.Stop()
        self.thdModify.SetPostWid(None)
        del self.thdModify
        self.SetParent(None)
        del self.lCmds
        self.oLogin=None
        vtSock.SocketClosed(self)
    def do__connectionClosed__(self,bEstabilshed,bAborted,blocking=True):
        if self.par is not None:
            if hasattr(self.par,'acquire'):
                self.par.acquire('dom',blocking=blocking)
                bAcquire=True
            else:
                bAcquire=False
            try:
                self.par.__connectionClosed__(bEstabilshed)
                if bAborted:
                    self.par.NotifyConnectionFault()
            except:
                vtLog.vtLngTB(self.GetOrigin())
            if bAcquire:
                self.par.release('dom')

    def SocketClosed(self):
        vtLog.vtLngCur(vtLog.DEBUG,'%d'%(self._iSockNr),self.GetOrigin())
        vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        if hasattr(self,'thdModify'):
            self.thdModify.Do(self.do__connectionClosed__,self.bEstabilshed,False)
            self.thdModify.Do(self.do__SocketClosed__)
            if 0:
                self.thdModify.Stop()
                fTime=0
                zSleep=0.2
                while self.thdModify.IsRunning() and (fTime<30.0):
                    time.sleep(zSleep)
                    fTime+=zSleep
                self.thdModify.SetPostWid(None)
                del self.thdModify
        else:
            try:
                if self.par is not None:
                    #self.thdModify.Do(self.do__connectionClosed__,self.bEstabilshed,False)
                    self.par.__connectionClosed__(self.bEstabilshed)
            except:
                vtLog.vtLngCur(vtLog.WARN,traceback.format_exc(),self.GetOrigin())
            self.SetParent(None)
            #self.par=None
            del self.lCmds
            self.oLogin=None
            vtSock.SocketClosed(self)
    def do__SocketClosed__(self):
            self.thdModify.Stop()
            self.thdModify.SetPostWid(None)
            del self.thdModify
            self.SetParent(None)
            #self.par=None
            del self.lCmds
            self.oLogin=None
            vtSock.SocketClosed(self)
        #vtLog.CallStack('')
        #print vtLog.pformat(self.__dict__)
    #def IsThreadRunning(self):
    #    if self.doc is not None:
    #        return self.doc.IsRunning()
    #    return False
    def Stop(self):
        vtLog.vtLngCS(vtLog.INFO,'Stop',
                origin=self.GetOrigin())
        #vtLog.vtLogCallDepth(None,'',1)
        #if self.doc is not None:
            #print self.doc
            #vtLog.vtLogCallDepth(None,'runn:%d'%(self.doc.IsRunning()),1)
        #    if self.doc.IsRunning():
        #        vtLog.vtLogCallDepth(None,'wa',1)
        #        self.doc.Stop()
                #time.sleep(1)
        #        vtLog.vtLogCallDepth(None,'wait',1)
        #        return
        #vtLog.vtLogCallDepth(None,'wait 3',1)
        vtSock.Stop(self)
        #vtLog.vtLogCallDepth(None,'wait 4',1)
        #self.stopping=True
        #if self.sock is not None:
        #    self.sock.Stop()
    def IsRunning(self):
        return vtSock.IsRunning(self)
    def Login(self,usr,passwd):
        vtLog.vtLngCur(vtLog.INFO,'usr:%s'%(usr),self.GetOrigin())
        self.usr=usr
        self.passwd=passwd
        self.encoded_passwd=False
        self.bLoginSend=False
    def LoginEncoded(self,usr,passwd):
        vtLog.vtLngCur(vtLog.INFO,'usr:%s'%(usr),self.GetOrigin())
        self.usr=usr
        self.passwd=passwd
        self.encoded_passwd=True
        self.bLoginSend=False
    def __sessionID__(self,val):
        vtLog.vtLngCur(vtLog.INFO,'%s'%(val[1:]),
                origin=self.GetOrigin())
        #vtNetXmlSock.__sessionID__(self,val)
        self.sessionID=val[1:]
        if self.par is not None:
            self.par.NotifyConnected()
        #self.__doLogin__()
    def __doLogin__(self):
        vtLog.vtLngCS(vtLog.INFO,'__doLogin__',
                origin=self.GetOrigin())
        if len(self.passwd)>0:
            if self.encoded_passwd:
                sPasswd=self.passwd
            else:
                m=newSha()#sha.new()
                m.update(self.passwd)
                sPasswd=m.hexdigest()
            m=newSha()#sha.new()
            m.update(sPasswd)
            m.update(self.sessionID)
            s=binascii.b2a_uu(m.digest())
        else:
            s=''
        self.SendTelegram('login|'+self.usr+','+s)
        self.bLoginSend=True
    def __login__(self,val):
        vtLog.vtLngCS(vtLog.INFO,'__login__',
                origin=self.GetOrigin())
        if self.bLoginSend==False:
            self.__doLogin__()
        else:
            self.par.NotifyLogin()
        vtLog.vtLngCallStack(self,vtLog.DEBUG,'',
                origin=self.GetOrigin())
    def __loggedin__(self,val):
        vtLog.vtLngCS(vtLog.INFO,'__loggedin__',
                origin=self.GetOrigin())
        self.loggedin=True
        self.oLogin.SetStr(val[1:])
        try:
            self.thdModify.Do(self.do__loggedin__,self.oLogin)
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'class:%s'%self.par.__class__.__name__,origin=self.GetOrigin())
    def do__loggedin__(self,oLogin,blocking=True):
        if hasattr(self.par,'acquire'):
            self.par.acquire('dom',blocking=blocking)
            bAcquire=True
        else:
            bAcquire=False
        try:
            self.par.NotifyLoggedIn(oLogin)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        if bAcquire==True:
            self.par.release('dom')
    
    def __browse__(self,val):
        vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        try:
            if hasattr(self.par,'acquire'):
                self.thdModify.Do(self.do__browse__,val[1:].split(';'))
            else:
                self.par.NotifyBrowse(val[1:].split(';'))
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def do__browse__(self,l,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        try:
            self.par.NotifyBrowse(l)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
    
    def __hyper_browse__(self,val):
        vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        try:
            if hasattr(self.par,'acquire'):
                self.thdModify.Do(self.do__hyper_browse__,val[1:].split(';'))
            else:
                self.par.NotifyBrowse(val[1:].split(';'))
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def do__hyper_browse__(self,l,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        try:
            self.par.NotifyBrowse(l)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
    
    def ShutDown(self):
        pass
    def Cmd(self,action,**kwargs):
        try:
            s=';'.join([':'.join([k,str(v)]) for k,v in kwargs.items()])
        except:
            s=''
        sCmdArgs=','.join([action,s])
        m=newSha()#sha.new()
        m.update(sCmdArgs)
        m.update(self.sessionID)
        self.SendTelegram('cmd|'+','.join([m.hexdigest(),sCmdArgs]))
    def Browse(self,appl):
        vtLog.vtLngCur(vtLog.INFO,'appl:%s'%(appl),self.GetOrigin())
        self.SendTelegram('browse|'+appl)
    def HyperBrowse(self,usr,appl):
        self.SendTelegram('hypbrowse|'+','.join([usr,appl]))
    def Alias(self,appl,alias):
        self.appl=appl
        self.appl_alias=appl+':'+alias
        self.SendTelegram('alias|'+appl+':'+alias)
        self.SetOrigin()
    def GetConfig(self):
        vtLog.vtLngCur(vtLog.INFO,'',
                origin=self.GetOrigin())
        self.SendTelegram('get_config|')
    def GetLoggedInAcl(self):
        vtLog.vtLngCur(vtLog.INFO,'',
                origin=self.GetOrigin())
        self.SendTelegram('get_acl|')
    def GetSpecial(self,sTagName):
        vtLog.vtLngCur(vtLog.INFO,'',
                origin=self.GetOrigin())
        self.SendTelegram('get_special|'+sTagName)
    def GetKeys(self):
        vtLog.vtLngCur(vtLog.INFO,'',
                origin=self.GetOrigin())
        self.SendTelegram('get_keys|')
    def GetSynchNode(self,id,fingerprint,tagName):
        vtLog.vtLngCur(vtLog.INFO,'id:%s'%id,
                origin=self.GetOrigin())
        self.SendTelegram(''.join(['get_synch_node|',id,',',fingerprint,',',tagName]))
    def GetSynchNodeStr(self,id,fingerprint,tagName):
        vtLog.vtLngCur(vtLog.INFO,'id:%s,tagName:%s'%(id,tagName),
                origin=self.GetOrigin())
        return ''.join([self.startMarker,'get_synch_node|',id,',',fingerprint,',',tagName,self.endMarker])
    def __cmd__(self,val):
        vtLog.vtLngCur(vtLog.DEBUG,val,
                origin=self.GetOrigin())
        try:
            i=string.find(val,'|')
            if i>=0:
                iFlt=1
                j=string.find(val,',',i)
                if j>0:
                    iFlt=2
                    fp=val[i+1:j]
                    
                    m=newSha()#sha.new()
                    m.update(val[j+1:])
                    m.update(self.sessionID)
                    if m.hexdigest()==fp:
                        strs=val[j+1:].split(';')
                        self.par.NotifyCmd(strs[0],int(strs[1]))
            vtLog.vtLngCur(vtLog.WARN,'fault:%d'%iFlt,origin=self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def do(self,id,idNew,k,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        try:
            pass
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
    def __alias__(self,val):
        vtLog.vtLngCur(vtLog.DEBUG,val,
                origin=self.GetOrigin())
        i=string.find(val,'|')
        if i>=0:
            try:
                if val[i+1:]=='served':
                    # notify served
                    if self.par is not None:
                        self.thdModify.Do(self.do__alias__,True)
                elif val[i+1:]=='rejected':
                    if self.par is not None:
                        self.thdModify.Do(self.do__alias__,False)
                        vtLog.PrintMsg(_(u'alias %s rejected')%self.appl_alias,self.widLogging)
                else:
                    if self.par is not None:
                        self.par.NotifyServed(False)
            except:
                pass
    def do__alias__(self,flag,blocking=True):
        if hasattr(self.par,'acquire'):
            self.par.acquire('dom',blocking=blocking)
            bAcquire=True
        else:
            bAcquire=False
        try:
            self.par.NotifyServed(flag)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        if bAcquire:
            self.par.release('dom')
        try:
            if flag is False:
                #self.thdModify.Do(self.SetParent,None)
                self.SetParent(None)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def __shutdown__(self,val):
        vtLog.vtLngCS(vtLog.INFO,'__shutdown__',
                origin=self.GetOrigin())
        vtLog.vtLngCallStack(self,vtLog.DEBUG,val,
                origin=self.GetOrigin())
        pass
    def __count_key__(self,val):
        vtLog.vtLngCS(vtLog.INFO,'__count_key__',
                origin=self.GetOrigin())
        vtLog.vtLngCallStack(self,vtLog.DEBUG,val,
                origin=self.GetOrigin())
        try:
            i=string.find(val,'|')
            if i>=0:
                if self.par is not None:
                    self.thdModify.Do(self.do__count_key__,long(val[i+1:]))
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.GetOrigin())
    def do__count_key__(self,iCount,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        try:
            self.par.NotifyCountKey(iCount)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
        
    def __count_element__(self,val):
        vtLog.vtLngCS(vtLog.INFO,'__count_element__',
                origin=self.GetOrigin())
        vtLog.vtLngCallStack(self,vtLog.DEBUG,val,
                origin=self.GetOrigin())
        try:
            i=string.find(val,'|')
            if i>=0:
                if self.par is not None:
                    self.thdModify.Do(self.do__count_element__,long(val[i+1:]))
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.GetOrigin())
    def do__count_element__(self,iCount,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        try:
            self.par.NotifyCountElement(iCount)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
        
    def __count_node__(self,val):
        vtLog.vtLngCS(vtLog.INFO,'__count_node__',
                origin=self.GetOrigin())
        vtLog.vtLngCallStack(self,vtLog.DEBUG,val,
                origin=self.GetOrigin())
        try:
            i=string.find(val,'|')
            if i>=0:
                if self.par is not None:
                    self.thdModify.Do(self.do__count_node__,long(val[i+1:]))
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.GetOrigin())
    def do__count_node__(self,iCount,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        try:
            self.par.NotifyCountNode(iCount)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
        
    def __get_content__(self,val):
        vtLog.vtLngCS(vtLog.INFO,'__get_content__',
                origin=self.GetOrigin())
        #vtLog.vtLngCallStack(self,vtLog.DEBUG,val[:140],
        #        origin=self.GetOrigin())
        try:
            #if self.doc is not None:
            #    while self.doc.IsRunning():
                    #print 'wait thread is running'
            #        time.sleep(1)
            #    self.doc.acquire()
            #    self.doc.release()
            iFlt=0
            i=string.find(val,'|')
            if i>=0:
                iFlt=1
                j=string.find(val,',',i)
                if j>0:
                    iFlt=2
                    fp=val[i+1:j]
                    m=newSha()#sha.new()
                    m.update(val[j+1:])
                    m.update(self.sessionID)
                    if m.hexdigest()==val[i+1:j]:
                        if self.par is not None:
                            #if self.doc.IsSynchThreadEnabled==False:
                            #    nodes=[]
                            #    for tagName in self.nodes2keep:
                            #        n=self.doc.getChild(None,tagName)
                            #        if n is not None:
                            #            nodes.append(self.doc.cloneNode(n,True))
                            #print type(val)
                            #print val[j+1:]
                            #print val[j+1:].decode('ISO-8859-1')
                            self.thdModify.Do(self.do__get_content__,long(val[j+1:]))
                            #self.par.ParseBuffer(val[j+1:].encode('ISO-8859-1')) #060521
                            #self.par.ParseBuffer(val[j+1:].decode('ISO-8859-1'))
                            #self.dataSettled=True
                            
                            #if self.doc.IsSynchThreadEnabled==False:
                            #    par=self.doc.getRoot()
                            #    for tagName in self.nodes2keep:
                            #        n=self.doc.getChild(None,tagName)
                            #        if n is not None:
                            #            self.doc.deleteNode(n)
                            #    for n in nodes:
                            #        self.doc.appendChild(par,n)
                        #self.bEstabilshed=True
                        #if self.par is not None:
                        #    self.par.bDataSettled=True
                        #    self.par.NotifyContent()
                        return
            vtLog.vtLngCS(vtLog.WARN,'get content fault:%d'%iFlt,
                    origin=self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
        #try:
        #    vtLog.vtLngCS(vtLog.ERROR,'%s'%(val),origin=self.GetOrigin())
        #except:
        #    pass
        if self.par is not None:
            self.par.NotifyFault('get_content',iFlt)
    def do__get_content__(self,val,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        try:
            self.par.ParseBuffer(val)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
        
    def __get_node__(self,val):
        #vtLog.vtLngCallStack(self,vtLog.DEBUG,val,
        #        origin=self.GetOrigin())
        try:
            iFlt=0
            i=string.find(val,'|')
            if i>=0:
                iFlt=1
                j=string.find(val,',',i)
                if j>0:
                    iFlt=2
                    k=string.find(val,',',j+1)
                    if k>0:
                        iFlt=3
                        fp=val[i+1:j]
                        m=newSha()#sha.new()
                        m.update(val[k+1:])
                        m.update(self.sessionID)
                        if m.hexdigest()==val[i+1:j]:
                            id=val[j+1:k]
                            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%id,
                                        origin=self.GetOrigin())
                            if self.par is not None:
                                self.thdModify.Do(self.do__get_node__,
                                                id,val[k+1:])
                            return
            vtLog.vtLngCur(vtLog.WARN,'fault :%d;%s'%(iFlt,val),
                    origin=self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.GetOrigin())
        if self.par is not None:
            self.par.NotifyFault('get_node',iFlt)
    def do__get_node__(self,id,val,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        try:
            node=self.par.getNodeByIdNum(long(id))
            if self.par.SetNodeXmlContent(node,val) is None:
                vtLog.vtLngCur(vtLog.CRITICAL,'set xml content failded'%(),self)
            else:
                self.par.NotifyGetNode(id)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
    def __get_node_full__(self,val):
        #vtLog.vtLngCallStack(self,vtLog.DEBUG,val,
        #        origin=self.GetOrigin())
        try:
            iFlt=0
            i=string.find(val,'|')
            if i>=0:
                iFlt=1
                j=string.find(val,',',i)
                if j>0:
                    iFlt=2
                    k=string.find(val,',',j+1)
                    if k>0:
                        iFlt=3
                        fp=val[i+1:j]
                        m=newSha()#sha.new()
                        m.update(val[k+1:])
                        m.update(self.sessionID)
                        if m.hexdigest()==val[i+1:j]:
                            id=val[j+1:k]
                            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%id,
                                        origin=self.GetOrigin())
                            if self.par is not None:
                                self.thdModify.Do(self.do__get_node_full__,
                                                id,val[k+1:])
                            return
            vtLog.vtLngCur(vtLog.WARN,'get node full fault :%d;%s'%(iFlt,val),
                    origin=self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.GetOrigin())
        if self.par is not None:
            self.par.NotifyFault('get_node_full',iFlt)
    def do__get_node_full__(self,id,val,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        try:
            node=self.par.getNodeByIdNum(long(id))
            if self.par.SetNodeXmlContentFull(node,val) is None:
                vtLog.vtLngCur(vtLog.CRITICAL,'set xml content failded'%(),self)
            else:
                self.par.NotifyGetNode(id)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
    def __get_config__(self,val):
        vtLog.vtLngCur(vtLog.INFO,'',self.GetOrigin())
        try:
            iFlt=0
            i=string.find(val,'|')
            if i>=0:
                iFlt=1
                j=string.find(val,',',i)
                if j>0:
                    iFlt=2
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'%s'%val,
                                    origin=self.GetOrigin())
                    else:
                        vtLog.vtLngCur(vtLog.INFO,'',
                                    origin=self.GetOrigin())
                    m=newSha()#sha.new()
                    m.update(val[j+1:])
                    m.update(self.sessionID)
                    if m.hexdigest()==val[i+1:j]:
                        if self.par is not None:
                            self.thdModify.Do(self.do__get_config__,
                                                None,val[j+1:])
                        return
                    else:
                        vtLog.vtLngCur(vtLog.ERROR,'sessionID:%s;'
                            'digLoc:%s;digRmt:%s;len:%s'%(self.sessionID,
                            m.hexdigest(),val[i+1:j],len(val[j+1:])),
                            self.GetOrigin())
            vtLog.vtLngCur(vtLog.WARN,'fault :%d'%(iFlt),
                    origin=self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.GetOrigin())
        if self.par is not None:
            self.par.NotifyFault('get_config',iFlt)
    def do__get_config__(self,node,val,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        try:
            self.par.SetNodeXmlConfig(None,val)
            self.par.NotifyConfig(val)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
    def __get_acl__(self,val):
        vtLog.vtLngCur(vtLog.INFO,'',self.GetOrigin())
        try:
            iFlt=0
            i=string.find(val,'|')
            if i>=0:
                iFlt=1
                j=string.find(val,',',i)
                if j>0:
                    iFlt=2
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'content:%s'%(val[j+1:]),self.GetOrigin())
                    m=newSha()#sha.new()
                    m.update(val[j+1:])
                    m.update(self.sessionID)
                    if m.hexdigest()==val[i+1:j]:
                        if self.par is not None:
                            self.thdModify.Do(self.do__get_acl__,val[j+1:])
                        pass
                        return
                    else:
                        vtLog.vtLngCur(vtLog.ERROR,'sessionID:%s;'
                            'digLoc:%s;digRmt:%s;len:%s'%(self.sessionID,
                            m.hexdigest(),val[i+1:j],len(val[j+1:])),
                            self.GetOrigin())
            vtLog.vtLngCur(vtLog.WARN,'fault :%d'%(iFlt),self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),self.GetOrigin())
        if self.par is not None:
            self.par.NotifyFault('get_acl',iFlt)
    def do__get_acl__(self,val,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        try:
            #raise Exception
            nodeRoot=self.par.getRoot()
            if nodeRoot is None:
                vtLog.vtLngCur(vtLog.CRITICAL,'missing root node',self.GetOrigin())
                self.par.release('dom')
                return
            docTmp,nodeTmp=self.par.getNode2Add(val)
            nodeAcl=self.par.getChild(nodeRoot,'acl')
            if nodeAcl is not None:
                self.par.deleteNode(nodeAcl,nodeRoot)
            self.par.appendChild(nodeRoot,nodeTmp)
            self.par.AlignNode(nodeTmp)
            self.par.__setModified__()
            self.par.CreateAcl()
            self.par.NotifyAcl()
            self.par.__freeDocRaw__(docTmp)
            #vtLog.vtLngCur(vtLog.DEBUG,'acl:%s'%nodeTmp,self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
    def __get_special__(self,val):
        vtLog.vtLngCur(vtLog.DEBUG,'',self.GetOrigin())
        try:
            iFlt=0
            i=string.find(val,'|')
            if i>=0:
                iFlt=1
                j=string.find(val,',',i)
                if j>0:
                    iFlt=2
                    k=string.find(val,',',j+1)
                    if k>0:
                        iFlt=3
                        sTagName=val[j+1:k]
                        vtLog.vtLngCur(vtLog.INFO,'',
                                        origin=self.GetOrigin())
                        vtLog.vtLngCS(vtLog.INFO,'get special;%s'%val,
                                        origin=self.GetOrigin())
                        m=newSha()#sha.new()
                        m.update(val[k+1:])
                        m.update(self.sessionID)
                        if m.hexdigest()==val[i+1:j]:
                            if self.par is not None:
                                self.thdModify.Do(self.do__get_special__,
                                                None,sTagName,val[k+1:])
                        return
            vtLog.vtLngCur(vtLog.WARN,'fault :%d'%(iFlt),
                    origin=self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCS(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.GetOrigin())
        if self.par is not None:
            self.par.NotifyFault('get_specail',iFlt)
    def do__get_special__(self,node,sTagName,val,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        try:
            self.par.SetNodeXmlSpecial(None,sTagName,val)
            #self.par.NotifySpecial(val)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
    def __get_keys__(self,val):
        #vtLog.vtLngCur(vtLog.DEBUG,val,
        #        origin=self.GetOrigin())
        try:
            iFlt=0
            i=string.find(val,'|')
            if i>=0:
                iFlt=1
                j=string.find(val,',',i)
                if j>0:
                    iFlt=2
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'%s'%(val[j+1:]),self.GetOrigin())
                    #vtLog.vtLngCS(vtLog.INFO,'get keys',
                    #                origin=self.GetOrigin())
                    m=newSha()#sha.new()
                    m.update(val[j:])
                    m.update(self.sessionID)
                    if m.hexdigest()==val[i+1:j]:
                        if self.par is not None:
                            self.thdModify.Do(self.do__get_keys__,val[j+1:])
                            #self.par.NotifyKeys(val[j+1:])
                        return
            vtLog.vtLngCS(vtLog.WARN,'get key fault :%d'%(iFlt),
                    origin=self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCS(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.GetOrigin())
        if self.par is not None:
            self.par.NotifyFault('get_keys',iFlt)
    def do__get_keys__(self,val,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        try:
            self.par.NotifyKeys(val)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
    def __get_synch_node__(self,val):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'val:%s'%(val),self.GetOrigin())
        #iLog=vtLog.vtLngCallStack(self,vtLog.DEBUG,val,
        #        origin=self.GetOrigin())
        try:
            iFlt=0
            i=string.find(val,'|')
            if i>=0:
                iFlt=1
                j=string.find(val,',',i)
                if j>0:
                    iFlt=2
                    k=string.find(val,',',j+1)
                    if k>0:
                        iFlt=3
                        l=string.find(val,',',k+1)
                        if l>0:
                            iFlt=4
                            n=string.find(val,',',l+1)
                            if n>0:
                                iFlt=5
                                if i+1==j:
                                    parID=val[j+1:k]
                                    id=val[k+1:l]
                                    # identical fingerprints
                                    if self.par is not None:
                                        self.thdModify.Do(self.do__get_synch_node__,
                                                    parID,id,'','')
                                        #self.par.synchNode(parID,id,'','')
                                    return
                                else:
                                    m=newSha()#sha.new()
                                    m.update(val[n+1:])
                                    m.update(self.sessionID)
                                    if m.hexdigest()==val[i+1:j]:
                                        parID=val[j+1:k]
                                        id=val[k+1:l]
                                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                            vtLog.vtLngCur(vtLog.DEBUG,
                                                    'id:%s;idPar:%s'%(id,parID),
                                                    origin=self.GetOrigin())
                                        level=val[l+1:n]
                                        if self.par is not None:
                                            self.thdModify.Do(self.do__get_synch_node__,
                                                    parID,id,level,val[n+1:])
                                            #self.par.synchNode(parID,id,level,val[n+1:])
                                        return                        
            vtLog.vtLngCur(vtLog.WARN,'fault :%d;%s'%(iFlt,val),
                    origin=self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.GetOrigin())
        if self.par is not None:
            self.par.NotifyFault('get_synch_node',iFlt)
    def do__get_synch_node__(self,idPar,id,level,val,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        try:
            self.par.synchNode(idPar,id,level,val)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
    def __set_node__(self,val):
        #vtLog.vtLngCur(vtLog.DEBUG,val,
        #        origin=self.GetOrigin())
        try:
            iFlt=0
            i=string.find(val,'|')
            if i>=0:
                iFlt=1
                j=string.find(val,',',i)
                if j>0:
                    iFlt=2
                    k=string.find(val,',',j+1)
                    if k>0:
                        iFlt=2
                        id=val[i+1:j]
                        idNew=val[j+1:k]
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCur(vtLog.DEBUG,
                                        'id:%s;idNew:%s'%(id,idNew),
                                        origin=self.GetOrigin())
                        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        #    vtLog.vtLngCur(vtLog.DEBUG,
                        #            'val:%s'%(val),
                        #            origin=self.GetOrigin())
                        if self.par is not None:
                            self.thdModify.Do(self.do__set_node__,
                                                id,idNew,val[k+1:])
                        return
            vtLog.vtLngCur(vtLog.WARN,'fault :%d;%s'%(iFlt,val),
                    origin=self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.GetOrigin())
        if self.par is not None:
            self.par.NotifyFault('set_node',iFlt)
    def do__set_node__(self,id,idNew,val,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        try:
            self.par.NotifySetNode(id,idNew,val)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
    def __set_node_full__(self,val):
        #iLog=vtLog.vtLngCur(vtLog.DEBUG,val,
        #        origin=self.GetOrigin())
        try:
            iFlt=0
            i=string.find(val,'|')
            if i>=0:
                iFlt=1
                j=string.find(val,',',i)
                if j>0:
                    iFlt=2
                    k=string.find(val,',',j+1)
                    if k>0:
                        iFlt=3
                        l=string.find(val,',',k+1)
                        if l>0:
                            idPar=val[j+1:k]
                            id=val[k+1:l]
                            #vtLog.vtLog(None,'idPar:%s id:%s'%(idPar,id),level=1,verbose=iLog)
                            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                vtLog.vtLngCur(vtLog.DEBUG,
                                        'id:%s;idPar:%s'%(id,idPar),
                                        origin=self.GetOrigin())
                            iFlt=4
                            m=newSha()#sha.new()
                            m.update(val[l+1:])
                            m.update(self.sessionID)
                            if m.hexdigest()==val[i+1:j]:
                                if self.par is not None:
                                    self.thdModify.Do(self.do__set_node_full__,
                                                idPar,id,val[l+1:])
                                return
            vtLog.vtLngCur(vtLog.WARN,'fault :%d;%s'%(iFlt,val),
                    origin=self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.GetOrigin())
        if self.par is not None:
            self.par.NotifyFault('set_node_full',iFlt)
    def do__set_node_full__(self,idPar,id,val,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        try:
            nodePar=self.par.getNodeByIdNum(long(idPar))
            idNew=self.par.SetNodeXmlContentFull(nodePar,val)
            self.par.NotifyAddNode(idPar,id,val)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
    def __set_node_full_resp__(self,val):
        #vtLog.vtLngCur(vtLog.DEBUG,val,
        #        origin=self.GetOrigin())
        try:
            iFlt=0
            i=string.find(val,'|')
            if i>=0:
                iFlt=1
                j=string.find(val,',',i)
                if j>0:
                    iFlt=2
                    k=string.find(val,',',j+1)
                    if k>0:
                        iFlt=3
                        id=val[i+1:j]
                        idNew=val[j+1:k]
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCur(vtLog.DEBUG,
                                    'id:%s;idNew:%s'%(id,idNew),
                                    origin=self.GetOrigin())
                        sResp=val[k+1:]
                        if sResp!='ok':
                            vtLog.vtLngCur(vtLog.CRITICAL,
                                'id:%s idNew:%s;response:%s'%(id,idNew,sResp),
                                origin=self.GetOrigin())
                            return
                        if self.par is not None:
                            if long(id)!=long(idNew):
                                self.thdModify.Do(self.do__set_node_full_resp__,
                                            id,idNew)
                            # FIXME handle response
                            #self.par.NotifySetNodeResponse(id,val[k+1:])
                        return
            vtLog.vtLngCur(vtLog.WARN,'fault :%d;%s'%(iFlt,val),
                    origin=self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.GetOrigin())
        if self.par is not None:
            self.par.NotifyFault('set_node_full_resp',iFlt)
    def do__set_node_full_resp__(self,id,idNew,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        try:
            node=self.par.getNodeByIdNum(long(id))
            self.par.changeId(node,idNew)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')

    def __remove_node__(self,val):
        #vtLog.vtLngCur(vtLog.DEBUG,'val:%s'%(val),
        #        origin=self.GetOrigin())
        try:
            iFlt=0
            i=string.find(val,'|')
            if i>=0:
                iFlt=1
                id=val[i+1:]
                vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(id),self.GetOrigin())
                if self.par is not None:
                    self.thdModify.Do(self.do__remove_node__,id)
                return
            vtLog.vtLngCur(vtLog.WARN,'fault :%d;%s'%(iFlt,val),
                    origin=self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.GetOrigin())
        if self.par is not None:
            self.par.NotifyFault('remove_node',iFlt)
    def do__remove_node__(self,id,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        try:
            self.par.NotifyRemoveNode(id)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
    
    def __lock_node__(self,val):
        #iLog=vtLog.vtLngCur(vtLog.DEBUG,'val:%s'%(val),
        #        origin=self.GetOrigin())
        try:
            iFlt=0
            i=string.find(val,'|')
            if i>=0:
                iFlt=1
                j=string.find(val,',',i)
                if j>0:
                    iFlt=2
                    id=val[i+1:j]
                    vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(id),self.GetOrigin())
                    if self.par is not None:
                        self.thdModify.Do(self.do__lock_node__,id,val[j+1:])
                    return
            vtLog.vtLngCur(vtLog.WARN,'fault :%d;%s'%(iFlt,val),
                    origin=self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.GetOrigin())
        if self.par is not None:
            self.par.NotifyFault('lock_node',iFlt)
    def do__lock_node__(self,id,msg,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        try:
            self.par.NotifyLockNode(id,msg)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
    
    def __unlock_node__(self,val):
        #iLog=vtLog.vtLngCur(vtLog.DEBUG,'val:%s'%(val),
        #        origin=self.GetOrigin())
        try:
            iFlt=0
            i=string.find(val,'|')
            if i>=0:
                iFlt=1
                j=string.find(val,',',i)
                if j>0:
                    iFlt=2
                    id=val[i+1:j]
                    vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(id),self.GetOrigin())
                    if self.par is not None:
                        self.thdModify.Do(self.do__unlock_node__,id,val[j+1:])
                    return
            vtLog.vtLngCur(vtLog.WARN,'unlock node fault :%d;%s'%(iFlt,val),
                    origin=self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.GetOrigin())
        if self.par is not None:
            self.par.NotifyFault('unlock_node',iFlt)
    def do__unlock_node__(self,id,msg,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        try:
            self.par.NotifyUnLockNode(id,msg)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
    
    def __lock_request__(self,val):
        #iLog=vtLog.vtLngCur(vtLog.DEBUG,'val:%s'%(val),
        #        origin=self.GetOrigin())
        try:
            iFlt=0
            i=string.find(val,'|')
            if i>=0:
                j=string.find(val,',',i)
                if j>0:
                    iFlt=2
                    id=val[i+1:j]
                    vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(id),self.GetOrigin())
                    if self.par is not None:
                        self.thdModify.Do(self.do__lock_request__,id,val[j+1:])
                    return
            vtLog.vtLngCur(vtLog.WARN,'fault :%d;%s'%(iFlt,val),
                    origin=self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.GetOrigin())
        if self.par is not None:
            self.par.NotifyFault('lock_request',iFlt)
    def do__lock_request__(self,id,msg,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        try:
            self.par.NotifyLockRequest(id,msg)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
    
    def __lock_full_request__(self,val):
        #iLog=vtLog.vtLngCur(vtLog.DEBUG,'val:%s'%(val),
        #        origin=self.GetOrigin())
        try:
            iFlt=0
            i=string.find(val,'|')
            if i>=0:
                j=string.find(val,',',i)
                if j>0:
                    iFlt=2
                    id=val[i+1:j]
                    vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(id),self.GetOrigin())
                    if self.par is not None:
                        self.thdModify.Do(self.do__lock_full_request__,id,val[j+1:])
                    return
            vtLog.vtLngCur(vtLog.WARN,'fault :%d;%s'%(iFlt,val),
                    origin=self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.GetOrigin())
        if self.par is not None:
            self.par.NotifyFault('lockfull_request',iFlt)
    def do__lock_full_request__(self,id,msg,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        try:
            self.par.NotifyLockRequest(id,msg)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
    
    def __add_node__(self,val):
        #iLog=vtLog.vtLngCur(vtLog.DEBUG,'val:%s'%(val),
        #        origin=self.GetOrigin())
        try:
            iFlt=0
            i=string.find(val,'|')
            if i>=0:
                iFlt=1
                j=string.find(val,',',i)
                if j>0:
                    iFlt=2
                    k=string.find(val,',',j+1)
                    if k>0:
                        iFlt=3
                        l=string.find(val,',',k+1)
                        if l>0:
                            idPar=val[j+1:k]
                            id=val[k+1:l]
                            vtLog.vtLngCur(vtLog.DEBUG,
                                    'id:%s;idPar:%s'%(id,idPar),
                                    origin=self.GetOrigin())
                            iFlt=4
                            m=newSha()#sha.new()
                            m.update(val[l+1:])
                            m.update(self.sessionID)
                            if m.hexdigest()==val[i+1:j]:
                                if self.par is not None:
                                    self.thdModify.Do(self.do__add_node__,
                                            idPar,id,val[l+1:])
                                    return
            vtLog.vtLngCur(vtLog.WARN,'fault :%d;%s'%(iFlt,val),
                    origin=self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.GetOrigin())
        if self.par is not None:
            self.par.NotifyFault('add_node',iFlt)
    def do__add_node__(self,idPar,id,val,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        try:
            nodePar=self.par.getNodeByIdNum(long(idPar))
            idNew=self.par.AddNodeXmlContent(nodePar,val)
            self.par.NotifyAddNode(idPar,id,val)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
    def __add_node_resp__(self,val):
        #vtLog.vtLngCur(vtLog.DEBUG,'val:%s'%(val),
        #        origin=self.GetOrigin())
        try:
            iFlt=0
            i=string.find(val,'|')
            if i>=0:
                iFlt=1
                j=string.find(val,',',i)
                if j>0:
                    iFlt=2
                    k=string.find(val,',',j+1)
                    if k>0:
                        iFlt=3
                        id=val[i+1:j]
                        idNew=val[j+1:k]
                        resp=val[k+1:]
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCur(vtLog.DEBUG,
                                    'id:%s;idPar:%s;resp:%s'%(id,idNew,resp),
                                    origin=self.GetOrigin())
                        if self.par is not None:
                            self.thdModify.Do(self.do__add_node_resp__,id,idNew,resp)
                        return
            vtLog.vtLngCur(vtLog.WARN,'fault :%d;%s'%(iFlt,val),
                    origin=self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.GetOrigin())
        if self.par is not None:
            self.par.NotifyFault('add_node_resp',iFlt)
    def do__add_node_resp__(self,id,idNew,val,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        try:
            #vtLog.vtLngCur(vtLog.DEBUG,'id:%s;idNew:%s;resp:%s'%(id,idNew,val),self.GetOrigin())
            if val=='acl':
                node=self.par.getNodeByIdNum(long(id))
                if node is not None:
                    nodePar=self.par.getParent(node)
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCur(vtLog.DEBUG,'node:%s'%(self.par.GetNodeXmlContent(node,True,True)),self.GetOrigin())
                    #self.par.PauseConsumer()            # 070717:wro pause first
                    #self.par.WaitConsumerNotBusy()      # 070717:wro pause first
                    try:
                        #self.par.acquire('dom',blocking=blocking)
                        #self.par.DoCallBack(self.par.deleteNode,node,nodePar)   #081209:wro do not call from thread
                        #self.par.deleteNode(node,nodePar)   #081209:wro do not call from thread
                        self.par.__deleteNode__(node,nodePar=nodePar)
                        pass
                    except:
                        vtLog.vtLngTB(self.GetOrigin())
                #self.par.release('dom')
                #self.par.ResumeConsumer()               # 070717:wro pause first
            else:
                node=self.par.getNodeByIdNum(long(id))
                if long(id)!=long(idNew):
                    try:
                        #self.par.acquire('dom',blocking=blocking)
                        self.par.changeId(node,idNew)
                    except:
                        vtLog.vtLngTB(self.GetOrigin())
                    #self.par.release('dom')
            self.par.NotifyAddNodeResponse(id,idNew,val)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
    def __del_node__(self,val):
        #vtLog.vtLngCur(vtLog.DEBUG,'val:%s'%(val),
        #        origin=self.GetOrigin())
        try:
            iFlt=0
            i=string.find(val,'|')
            if i>=0:
                iFlt=1
                j=string.find(val,',',i+1)
                if j>0:
                    iFlt=2
                    id=val[i+1:j]
                    resp=val[j+1:]
                    vtLog.vtLngCur(vtLog.DEBUG,
                                'id:%s;resp:%s'%(id,resp),self.GetOrigin())
                    if self.par is not None:
                        self.thdModify.Do(self.do__del_node__,id,resp)
                    return
            vtLog.vtLngCur(vtLog.WARN,'fault :%d;%s'%(iFlt,val),
                    origin=self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.GetOrigin())
        if self.par is not None:
            self.par.NotifyFault('del_node',iFlt)
    def do__del_node__(self,id,resp,blocking=True):
        try:
            #vtLog.vtLngCur(vtLog.DEBUG,'id:%s;resp:%s'%(id,resp),self.GetOrigin())
            #self.par.PauseConsumer()            # 070717:wro pause first
            #self.par.WaitConsumerNotBusy()      # 070717:wro pause first
            self.par.acquire('dom',blocking=blocking)
            try:
                self.par.NotifyDelNode(id,resp)
            except:
                vtLog.vtLngTB(self.GetOrigin())
            self.par.release('dom')
            #self.par.ResumeConsumer()               # 070717:wro pause first
            #if resp=='ok':
            #    if self.par is not None:
                    #node=self.par.getNodeByIdNum(long(id))
            #        self.par.NotifyDelNode(id,resp)
            #else:
            #    if self.par is not None:
            #        self.par.NotifyDelNode(id,resp)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def __move_node__(self,val):
        #iLog=vtLog.vtLngCur(vtLog.DEBUG,'val:%s'%(val),
        #        origin=self.GetOrigin())
        try:
            iFlt=0
            i=string.find(val,'|')
            if i>=0:
                iFlt=1
                j=string.find(val,',',i)
                if j>0:
                    iFlt=2
                    idPar=val[i+1:j]
                    id=val[j+1:]
                    #vtLog.vtLog(None,'idPar:%s id:%s'%(idPar,id),level=1,verbose=iLog)
                    vtLog.vtLngCur(vtLog.DEBUG,
                                        'id:%s;idPar:%s'%(id,idPar),
                                        origin=self.GetOrigin())
                    if self.par is not None:
                        self.thdModify.Do(self.do__move_node__,idPar,id)
                    return
            vtLog.vtLngCur(vtLog.WARN,'fault :%d;%s'%(iFlt,val),
                    origin=self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.GetOrigin())
        if self.par is not None:
            self.par.NotifyFault('move_node',iFlt)
    def do__move_node__(self,idPar,id,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        try:
            node=self.par.getNodeByIdNum(long(id))
            nodePar=self.par.getNodeByIdNum(long(idPar))
            #idNew=self.par.moveNode(nodePar,node,False)    # 070905:wro ???? why this
            self.par.moveNode(nodePar,node)
            self.par.NotifyMoveNode(idPar,id)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
    def __move_node_resp__(self,val):
        #vtLog.vtLngCur(vtLog.DEBUG,'val:%s'%(val),self.GetOrigin())
        try:
            iFlt=0
            i=string.find(val,'|')
            if i>=0:
                iFlt=1
                j=string.find(val,',',i)
                if j>0:
                    iFlt=2
                    k=string.find(val,',',j+1)
                    if j>0:
                        iFlt=3
                        idPar=val[i+1:j]
                        id=val[j+1:k]
                        resp=val[k+1:]
                        vtLog.vtLngCur(vtLog.DEBUG,'id:%s;idPar:%s;resp:%s'%(id,idPar,resp),self.GetOrigin())
                    #node=self.doc.getNodeByIdNum(long(id))
                    #if long(id)!=long(idNew):
                    #    self.doc.changeId(node,idNew)
                    if self.par is not None:
                        self.thdModify.Do(self.do__move_node_resp__,idPar,id,resp)
                        #self.par.NotifyMoveNodeResponse(idPar,id,resp)
                    return
            vtLog.vtLngCur(vtLog.WARN,'fault :%d;%s'%(iFlt,val),
                    origin=self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s;%s'%(val,traceback.format_exc()),origin=self.GetOrigin())
        if self.par is not None:
            self.par.NotifyFault('move_response',iFlt)
    def do__move_node_resp__(self,idPar,id,resp,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        try:
            self.par.NotifyMoveNodeResponse(idPar,id,resp)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
    
    def addNode(self,idPar,id):
        vtLog.vtLngCur(vtLog.DEBUG,'id:%s;parId:%s'%(id,idPar),self.GetOrigin())
        try:
            if id=='':
                return
            iFlt=0
            if self.par is not None:
                iFlt=1
                if self.par.IsKeyValid(id):
                    self.thdModify.Do(self.do_addNode,idPar,id)
                    return
            vtLog.vtLngCur(vtLog.WARN,'fault :%d;%s'%(iFlt,val),
                    origin=self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s'%(traceback.format_exc()),origin=self.GetOrigin())
    def do_addNode(self,idPar,id,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        #vtLog.vtLngCur(vtLog.DEBUG,'id:%s;parId:%s'%(id,idPar),self.GetOrigin())
        try:
            node=self.par.getNodeByIdNum(long(id))
            self.par.clearFingerPrint(node)
            self.par.GetFingerPrintAndStore(node)
            #content=self.par.GetNodeXmlContent(node,bFull=True,bIncludeNodes=True)
            content=self.par.GetNodeXmlContent(node,bFull=False,bIncludeNodes=False)
            m=newSha()#sha.new()
            m.update(content)
            m.update(self.sessionID)
            fp=m.hexdigest()
            #print type(content),content
            self.SendTelegram('add_node|'+fp+','+idPar+','+id+','+content)
            #self.SendTelegram(''.join(['add_node|',fp,',',idPar,',',id,',',content]))
            #self.Send(self.startMarker)
            #self.Send(''.join(['add_node|',fp,',',idPar,',',id,',']))
            #self.Send(content.encode('iso-8859-1'))
            #self.Send(self.endMarker)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
    
    def addNodeFull(self,idPar,id):
        vtLog.vtLngCur(vtLog.DEBUG,'id:%s;parId:%s'%(id,idPar),self.GetOrigin())
        try:
            if id=='':
                return
            iFlt=0
            if self.par is not None:
                iFlt=1
                if self.par.IsKeyValid(id):
                    self.thdModify.Do(self.do_addNodeFull,idPar,id)
                    return
            vtLog.vtLngCur(vtLog.WARN,'fault :%d;%s'%(iFlt,val),
                    origin=self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s'%(traceback.format_exc()),origin=self.GetOrigin())
    def do_addNodeFull(self,idPar,id,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        #vtLog.vtLngCur(vtLog.DEBUG,'id:%s;parId:%s'%(id,idPar),self.GetOrigin())
        try:
            node=self.par.getNodeByIdNum(long(id))
            content=self.par.GetNodeXmlContent(node,bFull=True,bIncludeNodes=True)
            m=newSha()#sha.new()
            m.update(content)
            m.update(self.sessionID)
            fp=m.hexdigest()
            self.SendTelegram('add_node|'+fp+','+idPar+','+id+','+content)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
    def delNode(self,id):
        vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(id),
                origin=self.GetOrigin())
        try:
            if id=='':
                return
            #if long(id)>=0:
            if self.par.IsKeyValid(id):
                self.thdModify.Do(self.do_delNode,id)
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s'%(traceback.format_exc()),origin=self.GetOrigin())
    def do_delNode(self,id,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        try:
            self.SendTelegram('del_node|'+id)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
    def moveNode(self,idPar,id):
        vtLog.vtLngCur(vtLog.INFO,'id:%s;parId:%s'%(id,idPar),self.GetOrigin())
        try:
            if id=='':
                return
            #if long(id)>=0:
            if self.par.IsKeyValid(id):
                self.SendTelegram('move_node|'+idPar+','+id)
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s'%(traceback.format_exc()),origin=self.GetOrigin())
    def set_node(self,idLock,id):
        vtLog.vtLngCur(vtLog.DEBUG,'idLock:%s;id:%s'%(idLock,id),
                origin=self.GetOrigin())
        try:
            if len(id)>0:
                if self.par is not None:
                    self.thdModify.Do(self.do_set_node,idLock,id)
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s'%(traceback.format_exc()),origin=self.GetOrigin())
    def do_set_node(self,idLock,id,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        #vtLog.vtLngCur(vtLog.DEBUG,'idLock:%s;id:%s'%(idLock,id),
        #        origin=self.GetOrigin())
        try:
            node=self.par.getNodeByIdNum(long(id))
            content=self.par.GetNodeXmlContent(node)
            #if vtLog.vtLngIsLogged(vtLog.DEBUG):
            #    vtLog.vtLngCur(vtLog.DEBUG,'node:%s'%node,origin=self.GetOrigin())
            #    vtLog.vtLngCur(vtLog.DEBUG,'content:%s'%content,origin=self.GetOrigin())
            m=newSha()#sha.new()
            m.update(content)
            m.update(self.sessionID)
            fp=m.hexdigest()
            self.SendTelegram('set_node|'+fp+','+idLock+','+id+','+content)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
    def set_node_full(self,idLock,id):
        vtLog.vtLngCur(vtLog.DEBUG,'idLock:%s;id:%s'%(idLock,id),
                origin=self.GetOrigin())
        try:
            if len(id)>0:
                if self.par is not None:
                    self.thdModify.Do(self.do_set_node_full,idLock,id)
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCS(vtLog.ERROR,'%s'%(traceback.format_exc()),origin=self.GetOrigin())
    def do_set_node_full(self,idLock,id,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        #vtLog.vtLngCur(vtLog.DEBUG,'idLock:%s;id:%s'%(idLock,id),
        #        origin=self.GetOrigin())
        try:
            node=self.par.getNodeByIdNum(long(id))
            content=self.par.GetNodeXmlContent(node,bFull=True,bIncludeNodes=True)
            m=newSha()#sha.new()
            m.update(content)
            m.update(self.sessionID)
            fp=m.hexdigest()
            self.SendTelegram('set_full_node|'+fp+','+idLock+','+id+','+content)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
    def lock_node(self,id):
        iLog=vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(id),self.GetOrigin())
        try:
            if id=='':
                return
            if long(id)>=0:
                self.thdModify.Do(self.do_lock_node,id)
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s'%(traceback.format_exc()),origin=self.GetOrigin())
    def do_lock_node(self,id,blocking=True):
        #self.par.acquire('dom',blocking=blocking)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(id),
        #            origin=self.GetOrigin())
        try:
            self.SendTelegram('lock_node|'+id)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        #self.par.release('dom')
    def unlock_node(self,id):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    iLog=vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(id),
        #            origin=self.GetOrigin())
        try:
            if id=='':
                return
            if long(id)>=0:
                self.thdModify.Do(self.do_unlock_node,id)
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCS(vtLog.ERROR,'%s'%(traceback.format_exc()),origin=self.GetOrigin())
    def do_unlock_node(self,id,blocking=True):
        #self.par.acquire('dom',blocking=blocking)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(id),
        #            origin=self.GetOrigin())
        try:
            self.SendTelegram('unlock_node|'+id)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        #self.par.release('dom')
    def reqlock_node(self,id):
        iLog=vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(id),
                origin=self.GetOrigin())
        try:
            if id=='':
                return
            if long(id)>=0:
                self.thdModify.Do(self.do_reqlock_node,id)
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s'%(traceback.format_exc()),origin=self.GetOrigin())
    def do_reqlock_node(self,id,blocking=True):
        #self.par.acquire('dom',blocking=blocking)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(id),
        #            origin=self.GetOrigin())
        try:
            self.SendTelegram('lock_request|'+id)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        #self.par.release('dom')
    def rellock_node(self,id,kind,msg):
        iLog=vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(id),
                origin=self.GetOrigin())
        try:
            if id=='':
                return
            if long(id)>=0:
                self.thdModify.Do(self.do_rellock_node,id,kind,msg)
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s'%(traceback.format_exc()),origin=self.GetOrigin())
    def do_rellock_node(self,id,kind,msg,blocking=True):
        #self.par.acquire('dom',blocking=blocking)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(id),
                    origin=self.GetOrigin())
        try:
            self.SendTelegram('lock_release|%s,%s,%s'%(id,kind,msg))
        except:
            vtLog.vtLngTB(self.GetOrigin())
        #self.par.release('dom')
    def reqlockfull_node(self,id):
        iLog=vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(id),
                origin=self.GetOrigin())
        try:
            if id=='':
                return
            if long(id)>=0:
                self.thdModify.Do(self.do_reqlockfull_node,id)
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s'%(traceback.format_exc()),origin=self.GetOrigin())
    def do_reqlockfull_node(self,id,blocking=True):
        #self.par.acquire('dom',blocking=blocking)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(id),
                    origin=self.GetOrigin())
        try:
            self.SendTelegram('lockfull_request|'+id)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        #self.par.release('dom')
    def rellockfull_node(self,id,kind,msg):
        iLog=vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(id),
                origin=self.GetOrigin())
        try:
            if id=='':
                return
            if long(id)>=0:
                self.thdModify.Do(self.do_rellockfull_node,id,kind,msg)
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s'%(traceback.format_exc()),origin=self.GetOrigin())
    def do_rellockfull_node(self,id,kind,msg,blocking=True):
        #self.par.acquire('dom',blocking=blocking)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(id),
                    origin=self.GetOrigin())
        try:
            self.SendTelegram('lockfull_release|%s,%s,%s'%(id,kind,msg))
        except:
            vtLog.vtLngTB(self.GetOrigin())
        #self.par.release('dom')
    def set_content(self):
        vtLog.vtLngCur(vtLog.DEBUG,'',
                origin=self.GetOrigin())
        if self.par is None:
            return
        try:
            self.thdModify.Do(self.do_set_content)
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s'%(traceback.format_exc()),origin=self.GetOrigin())
    def do_set_content(self,blocking=True):
        self.par.acquire('dom',blocking=blocking)
        try:
            content=self.par.GetXmlContent()
            m=newSha()#sha.new()
            m.update(content)
            m.update(self.sessionID)
            fp=binascii.b2a_uu(m.digest())
            self.SendTelegram('set_content|'+fp+','+content)
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self.par.release('dom')
