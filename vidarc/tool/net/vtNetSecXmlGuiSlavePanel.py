#Boa:FramePanel:vtNetSecXmlGuiSlavePanel
#----------------------------------------------------------------------------
# Name:         vtNetSecXmlGuiSlavePanel.py
# Purpose:      
#               
# Author:       Walter Obweger
#
# Created:      20060705
# CVS-ID:       $Id: vtNetSecXmlGuiSlavePanel.py,v 1.11 2007/11/18 10:20:27 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import wx.lib.scrolledpanel

from vidarc.tool.net.vNetXmlWxGui import *

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
import sys,os

import vidarc.tool.net.vtNetArt as vtNetArt

def create(parent):
    return vtNetSecXmlGuiSlavePanel(parent)

[wxID_VTNETSECXMLGUISLAVEPANEL, wxID_VTNETSECXMLGUISLAVEPANELCBCONN, 
 wxID_VTNETSECXMLGUISLAVEPANELCBDISCONN, wxID_VTNETSECXMLGUISLAVEPANELCBLOGIN, 
 wxID_VTNETSECXMLGUISLAVEPANELCBOPEN, wxID_VTNETSECXMLGUISLAVEPANELCBSAVE, 
 wxID_VTNETSECXMLGUISLAVEPANELLSTCONNSTATE, 
] = [wx.NewId() for _init_ctrls in range(7)]

class vtNetSecXmlGuiSlavePanel(wx.Panel):
    UPDATE_TIME = 500
    #STATE_MAP=[
    #    []  ,  # state
    #    [_(u'no access')    ,_(u'access')]  ,      # configuation   DATA_ACCESS
    #    [_('notify')        ,_(u'blocked')]  ,     # notify         BLOCK_NOTIFY
    #    [_(u'closed')       ,_(u'opened')]  ,      # open           OPENED
    #    [_(u'unknown')      ,_(u'valid')]  ,       # config         CONFIG_VALID
    #    [_(u'copy')         ,_(u'synchable')]  ,   # synch          SYNCHABLE
    #    [_(u'disconnected') ,_(u'established')]  , # connection     CONN_ACTIVE
    #    [_(u'running')      ,_(u'paused')]  ,      # connection     CONN_ACTIVE
    #    
    #    ]
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableRow(1)
        parent.AddGrowableCol(0)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbConn, 0, border=4, flag=0)
        parent.AddWindow(self.cbDisConn, 0, border=4, flag=wx.TOP)
        parent.AddWindow(self.cbLogin, 0, border=4, flag=wx.TOP)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=wx.TOP)
        parent.AddWindow(self.cbOpen, 0, border=4, flag=wx.TOP)
        parent.AddWindow(self.cbSave, 0, border=4, flag=wx.TOP)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstConnState, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.RIGHT | wx.LEFT)

    def _init_coll_lstConnState_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'state'), width=40)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'appl'), width=60)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'alias'), width=60)
        parent.InsertColumn(col=3, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'host'), width=60)
        parent.InsertColumn(col=4, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'usr'), width=60)
        parent.InsertColumn(col=5, format=wx.LIST_FORMAT_LEFT, heading=_(u'fn'),
              width=160)
        parent.InsertColumn(col=6, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'port'), width=60)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=2, hgap=0, rows=2, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTNETSECXMLGUISLAVEPANEL,
              name=u'vtNetSecXmlGuiSlavePanel', parent=prnt, pos=wx.Point(244,
              152), size=wx.Size(366, 216), style=wx.DEFAULT_DIALOG_STYLE)
        self.SetClientSize(wx.Size(358, 189))

        self.lstConnState = wx.ListView(id=wxID_VTNETSECXMLGUISLAVEPANELLSTCONNSTATE,
              name=u'lstConnState', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(274, 189), style=wx.LC_REPORT)
        self.lstConnState.SetMinSize(wx.Size(-1, -1))
        self._init_coll_lstConnState_Columns(self.lstConnState)
        self.lstConnState.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstConnStateListItemDeselected,
              id=wxID_VTNETSECXMLGUISLAVEPANELLSTCONNSTATE)
        self.lstConnState.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstConnStateListItemSelected,
              id=wxID_VTNETSECXMLGUISLAVEPANELLSTCONNSTATE)

        self.cbConn = wx.lib.buttons.GenBitmapTextButton(id=wxID_VTNETSECXMLGUISLAVEPANELCBCONN,
              bitmap=vtNetArt.getBitmap(vtNetArt.Connect), label=_(u'Connect'),
              name=u'cbConn', parent=self, pos=wx.Point(278, 4),
              size=wx.Size(76, 30), style=0)
        self.cbConn.Bind(wx.EVT_BUTTON, self.OnCbConnButton,
              id=wxID_VTNETSECXMLGUISLAVEPANELCBCONN)

        self.cbDisConn = wx.lib.buttons.GenBitmapTextButton(id=wxID_VTNETSECXMLGUISLAVEPANELCBDISCONN,
              bitmap=vtNetArt.getBitmap(vtNetArt.Stopped), label=_(u'Discon'),
              name=u'cbDisConn', parent=self, pos=wx.Point(278, 38),
              size=wx.Size(76, 30), style=0)
        self.cbDisConn.Bind(wx.EVT_BUTTON, self.OnCbDisConnButton,
              id=wxID_VTNETSECXMLGUISLAVEPANELCBDISCONN)

        self.cbLogin = wx.lib.buttons.GenBitmapTextButton(id=wxID_VTNETSECXMLGUISLAVEPANELCBLOGIN,
              bitmap=vtNetArt.getBitmap(vtNetArt.Login), label=_(u'Login'), name=u'cbLogin',
              parent=self, pos=wx.Point(278, 72), size=wx.Size(76, 30),
              style=0)
        self.cbLogin.Bind(wx.EVT_BUTTON, self.OnCbLoginButton,
              id=wxID_VTNETSECXMLGUISLAVEPANELCBLOGIN)

        self.cbOpen = wx.lib.buttons.GenBitmapTextButton(id=wxID_VTNETSECXMLGUISLAVEPANELCBOPEN,
              bitmap=vtNetArt.getBitmap(vtNetArt.Open), label=_(u'Open'), name=u'cbOpen',
              parent=self, pos=wx.Point(278, 114), size=wx.Size(76, 30),
              style=0)
        self.cbOpen.Bind(wx.EVT_BUTTON, self.OnCbOpenButton,
              id=wxID_VTNETSECXMLGUISLAVEPANELCBOPEN)

        self.cbSave = wx.lib.buttons.GenBitmapTextButton(id=wxID_VTNETSECXMLGUISLAVEPANELCBSAVE,
              bitmap=vtNetArt.getBitmap(vtNetArt.Save), label=_(u'Save'), name=u'cbSave',
              parent=self, pos=wx.Point(278, 148), size=wx.Size(76, 30),
              style=0)
        self.cbSave.Bind(wx.EVT_BUTTON, self.OnCbSaveButton,
              id=wxID_VTNETSECXMLGUISLAVEPANELCBSAVE)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name,verbose=0):
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        self._init_ctrls(parent)
        
        self.lstNetAppl=[]
        self.dictNet={}
        self.dictLbl={}
        self.dictProcess={}
        self.netDocMaster=None
        self.netMaster=None
        self.selIdx=-1
        
        self.pnGui=wx.lib.scrolledpanel.ScrolledPanel(self,id=wx.NewId(),
                pos=(8,8),size=(188, 200),
                style = wx.TAB_TRAVERSAL|wx.SUNKEN_BORDER)
        self.pnGui.Show(False)
        self.pnGuiSizer = wx.FlexGridSizer(cols=16, vgap=4, hgap=4)
        self.pnGui.SetSizer(self.pnGuiSizer)
        self.pnGui.SetAutoLayout(1)
        self.fgsData.AddWindow(self.pnGui, 1, border=4, 
                flag=wx.BOTTOM | wx.TOP | wx.EXPAND | wx.RIGHT | wx.LEFT)
        
        self.SetupImageList()
        
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
        self.fgsData.Fit(self)
    def SetupImageList(self):
        self.imgDict={}
        self.imgLstTyp=wx.ImageList(16,16)
        self.imgDict['dft']={-1:self.imgLstTyp.Add(vtNetArt.getBitmap(vtNetArt.Error))}
        
        self.lstConnState.SetImageList(self.imgLstTyp,wx.IMAGE_LIST_SMALL)
    def SetNetMaster(self,netMaster):
        self.netMaster=netMaster
    def GetDictNet(self):
        return self.dictNet
    def GetNetNames(self):
        return self.lstNetAppl
    def GetNetDoc(self,name,alias):
        if name in self.dictNet:
            dNet=self.dictNet[name]
            dAlias=dNet['alias']
            if alias in dAlias:
                return dAlias[alias][0]
            return None
        else:
            return None
    def GetNetDocBySlaves(self,name,alias,d={}):
        if name in self.dictNet:
            dNet=self.dictNet[name]
            dAlias=dNet['alias']
            if alias in dAlias:
                lSlaves=dAlias[alias][2]
                for s in lSlaves:
                    strs=s.split(':')
                    if strs[0] not in d:
                        doc=self.GetNetDoc(strs[0],strs[1])
                        d[strs[0]]={'doc':doc,'bNet':True}
                        self.GetNetDocBySlaves(strs[0],strs[1],d)
            return d
        else:
            return d
    def SetCfg(self,appls,mainAppl):
        self.appls=appls
        self.mainAppl=mainAppl
        self.pnGui.SetupScrolling()
    def GetNetControlParent(self):
        return self.scwConn
    def __getImgIndex__(self,name,status):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'name:%s;status:%d;imgDict:%s'%(name,status,vtLog.pformat(self.imgDict)),self)
        try:
            return self.imgDict[name][status]
        except:
            try:
                return self.imgDict['dft'][status]
            except:
                return self.imgDict['dft'][-1]
    def AddStatus(self,name,iStatus):
        try:
            idx=self.lstNetAppl.index(name)
            netDoc=self.dictNet[name]
            self.lstApplConn.SetStringItem(idx,0,'',self.__getImgIndex__(name,netDoc.GetStatus(iStatus)))
        except:
            traceback.print_exc()
    def AddNetControl(self,name,alias):
        if name not in self.dictNet:
            vtLog.vtLngCurWX(vtLog.ERROR,'net control:%s not registered'%(name),self)
            return None
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'dictNet:%s'%(vtLog.pformat(self.dictNet)),self)
        dNet=self.dictNet[name]
        dAlias=dNet['alias']
        if alias in dAlias:
            vtLog.vtLngCurWX(vtLog.INFO,'appl:%s;alias:%s already created'%(name,alias),self)
            return dAlias[alias][0]
        else:
            vtLog.vtLngCurWX(vtLog.INFO,'appl:%s;alias:%s is to create'%(name,alias),self)
        ctrlClass,name,args,kwargs=dNet['regCls']
        
        if kwargs.has_key('size')==False:
            kwargs['size']=wx.Size(32,32)
        ctrl=ctrlClass(self.pnGui,name,*args,**kwargs)
        ctrl.SetAppl(name)
        self.pnGuiSizer.Add(ctrl, flag=wx.ALIGN_LEFT)
        self.pnGuiSizer.Layout()
        ctrl.SetNetMaster(self.netMaster)
        if name not in self.imgDict:
            d=ctrl.GetListImgDict(self.imgLstTyp)
            if d is not None:
                self.imgDict[name]=d
        dNet['alias'][alias]=[ctrl,-1,[]]
        self.__updateListCfg__()
        return ctrl
    def DelNetControl(self,name,alias):
        if name not in self.dictNet:
            vtLog.vtLngCurWX(vtLog.ERROR,'net control:%s not registered'%(name),self)
        dNet=self.dictNet[name]
        dAlias,idx=dNet['alias']
        if alias in dAlias:
            vtLog.vtLngCurWX(vtLog.INFO,'appl:%s;alias:%s is to delete'%(name,alias),self)
        else:
            vtLog.vtLngCurWX(vtLog.INFO,'appl:%s;alias:%s is not created'%(name,alias),self)
    def __check__(self,appl,alias):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'appl:%s;alias:%s'%(appl,alias),self)
        if appl not in self.dictNet:
            return False
        dNet=self.dictNet[appl]
        dAlias=dNet['alias']
        if alias in dAlias:
            netDoc=dAlias[alias][0]
            if netDoc.IsSettled()==False:
                return False
            lSlaves=dAlias[alias][2]
            for s in lSlaves:
                strs=s.split(':')
                if self.__check__(strs[0],strs[1])==False:
                    return False
            #if netDoc.IsBusy():
            #    return False
            return True
        return False
    def RegNetControl(self,ctrlClass,name,*args,**kwargs):
        if self.dictNet.has_key(name):
            return None
        self.lstNetAppl.append(name)
        self.dictNet[name]={'regCls':(ctrlClass,name,args,kwargs),'alias':{}}
    def GetRegNetControl(self):
        return self.dictNet.keys()
    def GetSlaves(self,appl,alias):
        if appl not in self.dictNet:
            return None
        dNet=self.dictNet[appl]
        dAlias=dNet['alias']
        if alias not in dAlias:
            return None
        return dAlias[alias][2]
    def IsModified(self):
        for dNet in self.dictNet.values():
            for l in dNet['alias'].values():
                netDoc=l[0]
                if netDoc.IsModified():
                    return True
        return False
    def Save(self,bModified=False):
        for dNet in self.dictNet.values():
            for l in dNet['alias'].values():
                netDoc=l[0]
                if bModified:
                    if netDoc.IsModified():
                        netDoc.Save()
                else:
                    netDoc.Save()
    def Close(self):
        #vtLog.CallStack('')
        #print self.lstNetAppl
        #print self.dictNet
        #print self.dictLbl
        #print self.dictProcess
        #print self.netDocMaster
        #print self.netMaster
        #print self.dictNet
        for dNet in self.dictNet.values():
            for l in dNet['alias'].values():
                netDoc=l[0]
                netDoc.Close()
                #netDoc.Destroy()
        #self.dictNet={}
        self.__updateListCfg__()
    def __getNetDocIdxName__(self,netDoc):
        i=0
        for name in self.lstNetAppl:
            if self.dictNet[name]==netDoc:
                return i,name
            i+=1
        return -1,''
    def __getDocByIdx__(self,idx):
        for dNet in self.dictNet.values():
            for l in dNet['alias'].values():
                if idx==l[1]:
                    return l[0]
        return None
    def __getNetDocIdx__(self,name,alias):
        if name in self.dictNet:
            dNet=self.dictNet[name]
            dAlias=dNet['alias']
            if alias in dAlias:
                return dAlias[alias][0],dAlias[alias][1]
            return None,-1
        else:
            return None,-2
    def Notify(self,appl,alias,netDoc,iStatusPrev,iStatus,*args,**kwargs):
        doc,idx=self.__getNetDocIdx__(appl,alias)
        if idx<0:
            if vtLog.vtLngIsLogged(vtLog.ERROR):
                vtLog.vtLngCurWX(vtLog.ERROR,'appl:%s,alias:%s;idx:%d'%(appl,alias,idx),self)
            return
        self.lstConnState.SetStringItem(idx,0,'',self.__getImgIndex__(appl,netDoc.GetStatus(iStatus)))
        self.__updateListAlias__(idx,netDoc)
    def __updateListAlias__(self,idx,netDoc):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'idx:%d'%(idx),self)
            #netDoc.GenAutoFN(conn.GetAlias(self.netMaster.lNetMaster[i]),dn)
            try:
                fn=netDoc.GetFN()
                if fn is None:
                    fn=u'???'
            except:
                fn=u'???'
            if netDoc is not None:
                #self.lstApplConn.SetStringItem(idx,2,netDoc.GetAlias(),-1)
                self.lstConnState.SetStringItem(idx,3,netDoc.GetHost(),-1)
                self.lstConnState.SetStringItem(idx,4,netDoc.GetUsr(),-1)
                self.lstConnState.SetStringItem(idx,5,fn,-1)
                self.lstConnState.SetStringItem(idx,6,str(netDoc.GetPort()),-1)
            else:
                #self.lstApplConn.SetStringItem(i,2,'',-1)
                self.lstConnState.SetStringItem(i,3,'',-1)
                self.lstConnState.SetStringItem(i,4,'',-1)
                self.lstConnState.SetStringItem(i,5,'',-1)
                self.lstConnState.SetStringItem(i,6,'',-1)
        except:
            vtLog.vtLngTB(self.GetName())
    def __updateListCfg__(self,idx=-1):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'idx:%d'%(idx),self)
            if idx==-1:
                self.selIdx=-1
                self.lstConnState.DeleteAllItems()
                names=self.dictNet.keys()
                names.sort()
                for name in names:
                    dAlias=self.dictNet[name]['alias']
                    aliases=dAlias.keys()
                    aliases.sort()
                    for alias in aliases:
                        netDoc=dAlias[alias][0]
                        try:
                            img=self.__getImgIndex__(name,netDoc.STOPPED)
                        except:
                            vtLog.vtLngTB(self.GetName())
                            img=-1
                        idx=self.lstConnState.InsertImageStringItem(sys.maxint,'',img)
                        self.lstConnState.SetStringItem(idx,1,name)
                        self.lstConnState.SetStringItem(idx,2,alias)
                        dAlias[alias][1]=idx
                        self.__updateListAlias__(idx,dAlias[alias][0])
            else:
                doc=self.__getDocByIdx__(idx)
                self.__updateListAlias__(idx,doc)
        except:
            vtLog.vtLngTB(self.GetName())
    def ShutDown(self):
        vtLog.vtLngCurWX(vtLog.INFO,''%(),self)
        for d in self.dictNet.values():
            dAlias=d['alias']
            for t in dAlias.values():
                netDoc=t[0]
                #netDoc.SetFlag(netDoc.SHUTDOWN,True)
                netDoc.ShutDown()
    def IsShutDown(self):
        bDone=False
        bOne=False
        for d in self.dictNet.itervalues():
            dAlias=d['alias']
            for t in dAlias.itervalues():
                bOne=True
                netDoc=t[0]
                if netDoc.IsFlag(netDoc.SHUTDOWN)==False:
                    return False
                if netDoc.GetStatus() == netDoc.STOPPED:
                    bDone=True
                if netDoc.GetStatus() == netDoc.OPEN_OK:
                    bDone=True
        if bOne==False:
            return True
        return bDone

    def OnCbSaveButton(self, event):
        event.Skip()
        try:
            if self.selIdx>=0:
                doc=self.__getDocByIdx__(self.selIdx)
                doc.Save()
            else:
                self.Save(True)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnLstConnStateListItemDeselected(self, event):
        event.Skip()
        self.selIdx=-1

    def OnLstConnStateListItemSelected(self, event):
        event.Skip()
        self.selIdx=event.GetIndex()

    def OnCbConnButton(self, event):
        event.Skip()
        try:
            if self.selIdx>=0:
                doc=self.__getDocByIdx__(self.selIdx)
                if doc.IsConnActive()==False:
                    doc.Connect2Srv()
            else:
                #self.Save(True)
                pass
        except:
            vtLog.vtLngTB(self.GetName())

    def OnCbDisConnButton(self, event):
        event.Skip()
        try:
            if self.selIdx>=0:
                doc=self.__getDocByIdx__(self.selIdx)
                if doc.IsConnActive():
                    doc.DisConnect()
            else:
                #self.Save(True)
                pass
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbLoginButton(self, event):
        event.Skip()
        try:
            if self.selIdx>=0:
                doc=self.__getDocByIdx__(self.selIdx)
                doc.Login()
            else:
                #self.Save(True)
                pass
        except:
            vtLog.vtLngTB(self.GetName())

    def OnCbOpenButton(self, event):
        event.Skip()
