#----------------------------------------------------------------------------
# Name:         vNetSecLogin.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060413
# CVS-ID:       $Id: vtNetSecLogin.py,v 1.1 2006/04/19 11:52:17 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.net.vtNetSecLoginHost import vtNetSecLoginHost
from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer

import vidarc.tool.log.vtLog as vtLog

class vtNetSecLogin(vtXmlDomConsumer):
    def __init__(self):
        vtXmlDomConsumer.__init__(self)
        self.docHum=None
        self.lHost=[]
    def SetHumanDoc(self,doc):
        self.docHum=doc
    def GetHumanDoc(self):
        return self.docHum
    def Clear(self):
        vtXmlDomConsumer.Clear(self)
        self.lHost=[]
    def SetNode(self,node,bNet=False):
        self.Clear()
        vtXmlDomConsumer.SetNode(self,node)
        if self.doc is None:
            return
        cc=self.doc.getChilds(self.node,'host')
        try:
            for c in cc:
                sHost=self.doc.getNodeText(c,'name')
                sPort=self.doc.getNodeText(c,'port')
                sAppl=self.doc.getNodeText(c,'appl')
                sAlias=self.doc.getNodeText(c,'alias')
                sUsr=self.doc.getNodeText(c,'usr')
                cTmp=self.doc.getChild(c,'usr')
                if cTmp is not None:
                    sUsrId=self.doc.getAttribute(cTmp,'fid')
                else:
                    sUsrId='-1'
                sGrpIds=self.doc.getNodeText(c,'grps').split(',')
                sSec=self.doc.getNodeText(c,'secLv')
                sPasswd=self.doc.getNodeText(c,'passwd')
                o=vtNetSecLoginHost(sPasswd,sUsr,long(sUsrId),sGrpIds,sSec,sHost,
                                sPort,sAppl,sAlias)
                self.AddHost(o)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def GetNode(self,node=None):
        if node is None:
            node=self.node
        if node is None:
            return
        cc=self.doc.getChilds(node,'host')
        for c in cc:
            self.doc.delNode(c)
        
        for o in self.lHost:
            self.doc.createChildByLst(node,'host',[
                    {'tag':'name','val':o.GetHost()},
                    {'tag':'port','val':str(o.GetPort())},
                    {'tag':'appl','val':o.GetAppl()},
                    {'tag':'alias','val':o.GetAlias()},
                    {'tag':'usr','val':o.GetUsr(),'attr':['fid','%08d'%o.GetUsrId()]},
                    {'tag':'grps','val':','.join(map(str,o.GetGrpIds()))},
                    {'tag':'secLv','val':str(o.GetSecLv())},
                    {'tag':'passwd','val':o.GetPasswd()}])
        self.doc.AlignNode(node,iRec=3)
    def GetHosts(self):
        return self.lHost
    def AddHost(self,obj,pos=-1):
        sName=obj.GetHost()
        self.lHost.insert(pos,obj)
    def DelHost(self,iSel):
        try:
            self.lHost=self.lHost[:iSel]+self.lHost[iSel+1:]
        except:
            pass
    
