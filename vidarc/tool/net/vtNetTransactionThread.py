#----------------------------------------------------------------------------
# Name:         vtNetTransactionThread.py
# Purpose:
# Author:       Walter Obweger
#
# Created:      20061119
# CVS-ID:       $Id: vtNetTransactionThread.py,v 1.6 2007/10/28 23:25:10 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.vtThread import vtThread
from vidarc.tool.vtThreadEndless import vtThreadEndless
from vidarc.tool.vtSlotKeyBuffer import vtSlotKeyBuffer
from vidarc.tool.net.vNetXmlWxGuiEvents import *
from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer
import thread,time

VERBOSE=0

wxEVT_VT_NET_TRANSACTION_THREAD_FINISHED=wx.NewEventType()
vEVT_VT_NET_TRANSACTION_THREAD_FINISHED=wx.PyEventBinder(wxEVT_VT_NET_TRANSACTION_THREAD_FINISHED,1)
def EVT_VT_NET_TRANSACTION_THREAD_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_VT_NET_TRANSACTION_THREAD_FINISHED,func)
def EVT_VT_NET_TRANSACTION_THREAD_FINISHED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,wxEVT_VT_NET_TRANSACTION_THREAD_FINISHED,func)
class vtNetTransactionThreadFinished(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VT_NET_TRANSACTION_THREAD_FINISHED(<widget_name>, self.OnFinished)
    """
    def __init__(self):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VT_NET_TRANSACTION_THREAD_FINISHED)
wxEVT_VT_NET_TRANSACTION_THREAD_ABORTED=wx.NewEventType()
vEVT_VT_NET_TRANSACTION_THREAD_ABORTED=wx.PyEventBinder(wxEVT_VT_NET_TRANSACTION_THREAD_ABORTED,1)
def EVT_VT_NET_TRANSACTION_THREAD_ABORTED(win,func):
    win.Connect(-1,-1,wxEVT_VT_NET_TRANSACTION_THREAD_ABORTED,func)
def EVT_VT_NET_TRANSACTION_THREAD_ABORTED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,wxEVT_VT_NET_TRANSACTION_THREAD_ABORTED,func)
class vtNetTransactionThreadAborted(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VT_NET_TRANSACTION_THREAD_ABORTED(<widget_name>, self.OnAborted)
    """
    def __init__(self):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VT_NET_TRANSACTION_THREAD_ABORTED)

class vtNetTransactionThread(vtThread,vtXmlDomConsumer):
    def __init__(self,par,bPost=False,zTimeOut=100,szBuffer=10,zSleep=0.3,verbose=0,origin=None):
        vtThread.__init__(self,par,bPost=bPost,verbose=verbose,origin=origin)
        vtXmlDomConsumer.__init__(self)
        self.thdExec=vtThreadEndless(None,bPost=False,verbose=verbose-1)
        self.zTimeOut=zTimeOut
        self.zSleep=zSleep
        try:
            self.widLogging=vtLog.GetPrintMsgWid(self.par)
        except:
            self.widLogging=None
        self.iTransIDAct=0
        self.iTransID=0
        self.sbTrans=vtSlotKeyBuffer(szBuffer)
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        vtThread.__del__(self)
        vtXmlConsumer.__del__(self)
    def Clear(self):
        pass
    def SetDoc(self,doc):
        if self.doc is not None:
            EVT_NET_XML_ADD_NODE_RESPONSE_DISCONNECT(self.doc,self.OnAddResponse)
            EVT_NET_XML_LOCK_DISCONNECT(self.doc,self.OnLock)
        vtXmlDomConsumer.SetDoc(self,doc)
        if self.doc is not None:
            EVT_NET_XML_ADD_NODE_RESPONSE(self.doc,self.OnAddResponse)
            EVT_NET_XML_LOCK(self.doc,self.OnLock)
    def Do(self,cmd,id,func,*args,**kwargs):
        if self.verbose:
            vtLog.vtLngCur(vtLog.DEBUG,'running:%d keepgoing:%d;func;%s,args:%s;kwargs:%s'%(self.running,self.keepGoing,func,`args`,`kwargs`),
                            self.origin)
        self._acquire()
        if self.keepGoing==False:
            vtLog.vtLngCur(vtLog.CRITICAL,''%(),self.GetOrigin())
            self._release()
            return
        self.qSched.put((self.iTransIDAct,cmd,id,func,args,kwargs))
        self.iTransIDAct+=1
        if self.running:
            self._release()
            return
        self.keepGoing=self.running=True
        if self.verbose:
            vtLog.vtLngCur(vtLog.DEBUG,'running:%d keepgoing:%d'%(self.running,self.keepGoing),
                        self.origin)
        self._release()
        self.__runThread__()
    def Start(self):
        vtThread.Start(self)
        self.thdExec.Start()
    def Stop(self):
        vtThread.Stop(self)
        self.thdExec.Stop()
    def Is2Stop(self):
        if vtThread.Is2Stop(self):
            return True
        if self.thdExec.Is2Stop():
            return True
        return False
    def IsRunning(self):
        if vtThread.IsRunning(self):
            return True
        if self.thdExec.IsRunning():
            return True
        return False
    def _addTrans(self,par,node,tup=None):
        iTransID,cmd,f,a,kw=tup
        idChild=self.doc.getKeyNum(node)
        iPos=self.sbTrans.put(idChild,(iTransID,cmd,idChild,f,a,kw))
    def __run__(self):
        self.running=True
        try:
            self.__runStart__()
            if self.verbose:
                vtLog.vtLngCur(vtLog.DEBUG,'running:%d keepgoing:%d'%(self.running,self.keepGoing),
                                self.origin)
            self.zTime=0.0
            zNotify=0.0
            while self.Is2Stop()==False:
            #while self.keepGoing:
                while (self.qSched.empty()==False) and self.sbTrans.isFree():
                    tup=self.qSched.get(0)
                    iTransID,cmd,id,func,args,kwargs=tup
                    if self.verbose:
                        vtLog.vtLngCur(vtLog.DEBUG,'running:%d keepgoing:%d'%(self.running,self.keepGoing),
                                self.origin)
                    try:
                        if cmd=='edit':
                            node=self.doc.getNodeByIdNum(id)
                            if node is not None:
                                iPos=self.sbTrans.put(id,tup)
                                self.doc.startEdit(node)
                                vtLog.vtLngCur(vtLog.INFO,'id:%08d;edit'%(id),self.origin)
                            else:
                                vtLog.vtLngCur(vtLog.INFO,'id:%08d;node not found for edit'%(id),self.origin)
                        elif cmd=='add':
                            t=func(*args,**kwargs)
                            if t is not None:
                                idChild,node,c,f,a,kw=t
                                self.doc.appendChild(node,c)
                                #self.doc.AlignNode(c)
                                self.doc.addNode(node,c,self._addTrans,
                                        tup=(iTransID,cmd,f,a,kw))
                            vtLog.vtLngCur(vtLog.INFO,'id:%08d;add'%(id),self.origin)
                    except:
                        vtLog.vtLngTB(self.origin)
                        #self.sbTrans.pop(iPos)
                if self.qSched.empty():
                    if self.sbTrans.isFill()==False:
                        break
                self.zTime+=self.zSleep
                zNotify+=self.zSleep
                if self.zTime>self.zTimeOut:
                    #self.keepGoing=False
                    #self.bAbort=True
                    vtLog.vtLngCur(vtLog.ERROR,'time out %3.1f %3.1f [s]'%\
                                    (self.zTime,self.zTimeOut),self.origin)
                    #if self.bUpdate:
                    #    vtLog.PrintMsg(_(u'alias:%s id:%s wait for lock time out %3.1f [s]')%(self.doc.appl,id,self.zTime),self.widLogging)
                    #self.doc.endEdit(node)
                    #self.__next__()
                    self.zTime=0.0
                    zNotify=0.0
                if zNotify>=2.0:
                    #node=self.__getInfo__()
                    #id=self.doc.getKey(node)
                    #vtLog.vtLngCur(vtLog.DEBUG,'id:%s wait for lock time %3.1f %3.1f [1s]'%\
                    #                (id,self.zTime,self.zTimeOut),self.doc.GetOrigin())
                    zNotify=0.0
                time.sleep(self.zSleep)
                
            if self.Is2Stop()==True:
            #if self.keepGoing==False:
                while 1:
                    t=self.qSched.get(0)
        except:
            #self.keepGoing=
            vtLog.vtLngTB(self.origin)
        self._acquire()
        self.__runEnd__()
        self.__clrWakeUp__()
        #self.running=False
        if self.bPost:
            if self.Is2Stop()==False:
            #if self.keepGoing:
                wx.PostEvent(self.par,vtNetTransactionThreadFinished())
            else:
                wx.PostEvent(self.par,vtNetTransactionThreadAborted())
        vtLog.vtLngCur(vtLog.DEBUG,'running:%d keepgoing:%d'%(self.running,self.keepGoing),
                                self.origin)
        #self.keepGoing=self.running=False
        self._release()
    def OnLock(self,evt):
        evt.Skip()
        vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(evt.GetID()),self.origin)
        try:
            id=long(evt.GetID())
            iPos=self.sbTrans.find(id)
            if iPos>=0:
                tup=self.sbTrans.pop(iPos)
                self.zTime=0.0
                resp=evt.GetResponse()
                if resp in  ['ok','already locked']:
                    try:
                        iTransID,cmd,id,func,args,kwargs=tup
                        #func(id,*args,**kwargs)
                        #self.thdExec.Do(func,id,*args,**kwargs)
                    except:
                        vtLog.vtLngTB(self.doc.GetOrigin())
                else:
                    # not locked
                    pass
                #self.__next__()
        except:
            vtLog.vtLngTB(self.origin)
    def OnAddResponse(self,evt):
        evt.Skip()
        vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(evt.GetID()),self.origin)
        try:
            id=long(evt.GetID())
            iPos=self.sbTrans.find(id)
            if iPos>=0:
                resp=evt.GetResponse()
                tup=self.sbTrans.pop(iPos)
                #self.zTime=0.0
                if resp in  ['ok']:
                    try:
                        iTransID,cmd,idA,func,args,kwargs=tup
                        idNew=long(evt.GetNewID())
                        #func(id,idNew,*args,**kwargs)
                        self.thdExec.Do(funcid,idNew,*args,**kwargs)
                    except:
                        vtLog.vtLngTB(self.origin)
        except:
            vtLog.vtLngTB(self.origin)
