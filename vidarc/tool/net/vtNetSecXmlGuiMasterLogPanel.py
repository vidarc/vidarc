#Boa:FramePanel:vtNetSecXmlGuiMasterLogPanel
#----------------------------------------------------------------------------
# Name:         vtNetSecXmlGuiMasterLogPanel.py
# Purpose:      
#               derived from vNetXmlGuiMasterDialog.py
# Author:       Walter Obweger
#
# Created:      20060415
# CVS-ID:       $Id: vtNetSecXmlGuiMasterLogPanel.py,v 1.6 2007/07/15 05:42:07 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
import sys

def create(parent):
    return vtNetSecXmlGuiMasterLogPanel(parent)

[wxID_VTNETSECXMLGUIMASTERLOGPANEL, 
 wxID_VTNETSECXMLGUIMASTERLOGPANELCHCREVERSE, 
 wxID_VTNETSECXMLGUIMASTERLOGPANELLBLMAX, 
 wxID_VTNETSECXMLGUIMASTERLOGPANELLSTLOG, 
 wxID_VTNETSECXMLGUIMASTERLOGPANELSPNMAX, 
] = [wx.NewId() for _init_ctrls in range(5)]

class thdNetXmlLog:
    def __init__(self,par,verbose=0):
        self.verbose=verbose
        self.par=par
        self.running=False
    def Start(self):
        thread.start_new_thread(self.Run, ())
    def Stop(self):
        self.keepGoing=False
    def IsRunning(self):
        return self.running
    def Run(self):
        self.keepGoing=self.running=True
        try:
            while self.keepGoing:
                self.par.__checkScheduled__()
                time.sleep(0.5)
        except:
            traceback.print_exc()
        self.running=False
class vtNetSecXmlGuiMasterLogPanel(wx.Panel):
    UPDATE_TIME = 500
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_bxsInfo_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblMax, 1, border=4, flag=0)
        parent.AddWindow(self.spnMax, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.chcReverse, 1, border=4, flag=wx.EXPAND | wx.LEFT)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstLog, 1, border=4,
              flag=wx.BOTTOM | wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsInfo, 0, border=4,
              flag=wx.BOTTOM | wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self.bxsInfo = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsInfo_Items(self.bxsInfo)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTNETSECXMLGUIMASTERLOGPANEL,
              name=u'vtNetSecXmlGuiMasterLogPanel', parent=prnt,
              pos=wx.Point(244, 152), size=wx.Size(251, 164),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(243, 137))

        self.lstLog = wx.ListView(id=wxID_VTNETSECXMLGUIMASTERLOGPANELLSTLOG,
              name=u'lstLog', parent=self, pos=wx.Point(4, 4), size=wx.Size(235,
              100), style=wx.LC_REPORT)

        self.spnMax = wx.SpinCtrl(id=wxID_VTNETSECXMLGUIMASTERLOGPANELSPNMAX,
              initial=1000, max=10000, min=100, name=u'spnMax', parent=self,
              pos=wx.Point(86, 112), size=wx.Size(74, 21),
              style=wx.SP_ARROW_KEYS)
        self.spnMax.SetMinSize(wx.Size(-1, -1))

        self.chcReverse = wx.CheckBox(id=wxID_VTNETSECXMLGUIMASTERLOGPANELCHCREVERSE,
              label=u'reverse', name=u'chcReverse', parent=self,
              pos=wx.Point(164, 112), size=wx.Size(74, 21), style=0)
        self.chcReverse.SetValue(False)
        self.chcReverse.SetMinSize(wx.Size(-1, -1))

        self.lblMax = wx.StaticText(id=wxID_VTNETSECXMLGUIMASTERLOGPANELLBLMAX,
              label=u'max logs', name=u'lblMax', parent=self, pos=wx.Point(4,
              112), size=wx.Size(78, 13), style=wx.ALIGN_RIGHT)
        self.lblMax.SetMinSize(wx.Size(-1, -1))

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name,verbose=0):
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        self.STATE_MAP=[
            []  ,  # state
            [_(u'no access')    ,_(u'access')]  ,      # configuation   DATA_ACCESS
            [_('notify')        ,_(u'blocked')]  ,     # notify         BLOCK_NOTIFY
            [_(u'closed')       ,_(u'opened')]  ,      # open           OPENED
            [_(u'unknown')      ,_(u'valid')]  ,       # config         CONFIG_VALID
            [_(u'copy')         ,_(u'synchable')]  ,   # synch          SYNCHABLE
            [_(u'disconnected') ,_(u'established')]  , # connection     CONN_ACTIVE
            [_(u'running')      ,_(u'paused')]  ,      # connection     CONN_ACTIVE
            
            ]
        self._init_ctrls(parent)
        
        self.verbose=verbose
        
        self.lstLog.InsertColumn(0,_(u'time'),wx.LIST_FORMAT_LEFT,140)
        self.lstLog.InsertColumn(1,_(u'appl'),wx.LIST_FORMAT_LEFT,80)
        self.lstLog.InsertColumn(2,_(u'msg'),wx.LIST_FORMAT_LEFT,100)
        self.lstLog.InsertColumn(3,_(u'status'),wx.LIST_FORMAT_LEFT,80)
        
        self.chcReverse.SetValue(True)
        self.spnMax.SetValue(1000)
        
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
    def AddLog(self,sTime,name,msg,iStatusPrev,iStatus):
        try:
            bRev=self.chcReverse.GetValue()
            if bRev:
                i=0
            else:
                i=sys.maxint
            idx=self.lstLog.InsertImageStringItem(i, sTime,-1)
            self.lstLog.SetStringItem(idx,1,name,-1)
            self.lstLog.SetStringItem(idx,2,msg,-1)
            self.lstLog.SetStringItem(idx,3,'0x%04x'%iStatus,-1)
            
            iMax=self.spnMax.GetValue()
            for i in range(iMax,self.lstLog.GetItemCount()):
                if bRev:
                    self.lstLog.DeleteItem(iMax)
                else:
                    self.lstLog.DeleteItem(0)
        except:
            vtLog.vtLngTB(self.GetName())
