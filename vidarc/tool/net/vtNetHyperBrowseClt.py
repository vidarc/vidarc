#----------------------------------------------------------------------
# Name:         vMESHyperBrowseClt.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20061228
# CVS-ID:       $Id: vtNetHyperBrowseClt.py,v 1.1 2010/07/19 09:53:17 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog

import sha,Queue

import vidarc.tool.net.img_gui as img_gui

class vtNetHyperBrowseCltTransientPopup(wx.Dialog):
    #SIZE_PN_SCROLL=30
    def __init__(self, parent, size,style,name=''):
        self.SIZE_PN_SCROLL=wx.SystemSettings.GetMetric(wx.SYS_VSCROLL_X)
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        
        wx.Dialog.__init__(self, parent,style=wx.RESIZE_BORDER)#wx.BORDER_SIMPLE)#|wx.STAY_ON_TOP)
        id=wx.NewId()
        wx.EVT_SIZE(self,self.OnSize)
        st = wx.StaticText(self, -1,name,pos=(70,4))
        self.cbCancel = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Cancel), name=u'cbCancel',
              parent=self, pos=(0,0), size=(30,30), style=wx.BU_AUTODRAW)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              self.cbCancel)
              
        self.cbApply = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Apply), name=u'cbApply',
              parent=self, pos=wx.Point(32, 00), size=wx.Size(30, 30),
              style=wx.BU_AUTODRAW)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              self.cbApply)
        
        bxs = wx.BoxSizer(orient=wx.VERTICAL)
        
        bxsConn = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.cbConnect = wx.lib.buttons.GenBitmapTextButton(ID=-1,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Connect'),
              name=u'cbConnect', parent=self, pos=wx.DefaultPosition,
              size=wx.DefaultSize, style=0)
        self.cbConnect.SetMinSize(wx.Size(-1, -1))
        self.cbConnect.Bind(wx.EVT_BUTTON, self.OnCbConnectButton,self.cbConnect)
        bxsConn.AddWindow(self.cbConnect, 0, border=4, flag=wx.LEFT|wx.RIGHT)
        self.cbStop = wx.lib.buttons.GenBitmapButton(ID=-1,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbStop', parent=self,
              pos=wx.DefaultPosition, size=wx.DefaultSize, style=0)
        self.cbStop.SetToolTipString(_(u'stop'))
        self.cbStop.Bind(wx.EVT_BUTTON, self.OnCbStopButton,self.cbStop)
        bxsConn.AddWindow(self.cbStop, 0, border=4, flag=wx.LEFT|wx.RIGHT)
        bxs.AddSizer(bxsConn, 1, border=4, flag=wx.LEFT|wx.RIGHT|wx.EXPAND)
        
        bxsResult = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.stbResult = wx.StaticBitmap(bitmap=vtArt.getBitmap(vtArt.Invisible),
              id=-1, name=u'stbResult',
              parent=self, pos=wx.DefaultPosition, size=wx.DefaultSize, style=0)
        bxsResult.AddWindow(self.stbResult, 0, border=0, flag=wx.ALIGN_CENTER)
        self.lblResult = wx.StaticText(id=-1,
              label=u'', name=u'lblResult', parent=self, pos=wx.DefaultPosition,
              size=wx.DefaultSize, style=0)
        self.lblResult.SetMinSize(wx.Size(-1, -1))
        bxsResult.AddWindow(self.lblResult, 1, border=4, flag=wx.LEFT|wx.EXPAND)
        bxs.AddSizer(bxsResult, 1, border=4, flag=wx.LEFT|wx.RIGHT|wx.EXPAND)
        
        self.qMsg=Queue.Queue()
        self.qBrowse=Queue.Queue()
        
        if size[1]<40:
            size=(size[0],40)
        i=0
        iY=40
        iH=size[1]+4
        iSzHeight=0
        
        self.SetSizer(bxs)
        self.Layout()
        self.szOrig=(200,240)
        self.SetSize(self.szOrig)
    def OnSize(self,evt):
        iW,iH=self.GetSize()
        if iW<self.szOrig[0]:
            iW=self.szOrig[0]
        #if iH<self.szMin[1]:
        #    iH=self.szMin[1]
        if iH<self.szOrig[1]:
            iH=self.szOrig[1]
        self.SetSize((iW,iH))
        if self.trInfo is not None:
            self.trInfo.SetSize((iW-16,iH-52))
        evt.Skip()
    def OnCbCancelButton(self,evt):
        self.Show(False)
    def OnCbApplyButton(self,evt):
        #self.Apply()
        try:
            par=self.GetParent()
            #par.__apply__(self.trInfo.GetSelected())
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        self.Show(False)
    def OnCbStopButton(self, evt):
        evt.Skip()
    def OnCbConnectButton(self,evt):
        evt.Skip()
    def Show(self,flag):
        try:
            if flag:
                par=self.GetParent()
                #doc=par.__getDoc__()
                #node=par.__getNode__()
                #fid=par.__getSelID__()
                #self.trInfo.SelectByID(fid)
            else:
                par=self.GetParent()
                par.cbPopup.SetValue(False)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        wx.Dialog.Show(self,flag)
class vtNetHyperBrowseClt(wx.Panel):
    def __init__(self,*_args,**_kwargs):
        try:
            sz=_kwargs['size']
        except:
            sz=wx.Size(100,30)
            _kwargs['size']=sz
        try:
            szCb=_kwargs['size_button']
            del _kwargs['size_button']
        except:
            szCb=wx.Size(24,24)
        try:
            szTree=_kwargs['size_tree']
            del _kwargs['size_tree']
        except:
            szTree=wx.Size(sz[0]-szCb[0]-2,sz[1]-4)
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        self.SetAutoLayout(True)
        
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        size=(szTree[0],szTree[1])
        pos=(0,(sz[1]-szTree[1])/2)
        self.trAlias = wx.TreeCtrl(id=-1,
              name=u'trAlias', parent=self, pos=wx.DefaultPosition,
              size=wx.Size(-1, 200), style=wx.TR_HAS_BUTTONS|wx.TR_HIDE_ROOT|wx.TR_LINES_AT_ROOT)
        size=(szCb[0],szCb[1])
        pos=(szTree[0]+(sz[0]-(szTree[0]+szCb[0]))/2,(sz[1]-szCb[1])/2)
        id=wx.NewId()
        self.cbPopup = wx.lib.buttons.GenBitmapToggleButton(ID=id,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbPopup',
              parent=self, pos=pos, size=size, style=wx.BU_AUTODRAW)
        #self.cbPopup.SetConstraints(LayoutAnchors(self.cbPopup, False, True,
        #      True, False))
        bxs.AddWindow(self.cbPopup, 0, border=0, flag=wx.ALIGN_CENTER)
        self.SetSizer(bxs)
        self.cbPopup.SetBitmapLabel(vtArt.getBitmap(vtArt.Down))
        self.cbPopup.SetBitmapSelected(vtArt.getBitmap(vtArt.Down))
        self.cbPopup.Bind(wx.EVT_BUTTON,self.OnPopupButton,self.cbPopup)#id=id)
        self.popWin=None
    def Enable(self,flag):
        wx.Panel.Enable(self,flag)
        self.cbPopup.Refresh()
    def __createPopup__(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCS(vtLog.DEBUG,'__createPopup__',
                        origin=self.GetName())
        if self.popWin is None:
            sz=self.GetSize()
            sz=(sz[0],sz[1]-20)
            try:
                self.popWin=vtNetHyperBrowseTransientPopup(self,sz,wx.SIMPLE_BORDER)
            except:
                vtLog.vtLngTB(self.GetName())
            #self.popWin.SetVal(self.txtVal.GetValue(),self.doc.GetLang())
        else:
            pass
    def OnPopupButton(self,evt):
        self.__createPopup__()
        if self.cbPopup.GetValue()==True:
            btn=self
            iX,iY = btn.ClientToScreen( (0,0) )
            iW,iH =  btn.GetSize()
            iPopW,iPopH=self.popWin.GetSize()
            iScreenW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            iScreenH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            if iX+iPopW>iScreenW:
                iX=iScreenW-iPopW-4
                if iX<0:
                    iX=0
            iY+=iH
            if iY+iPopH>iScreenH:
                iY=iScreenH-iPopH
                if iY<0:
                    iY=0
            
            self.popWin.Move((iX,iY))
            #self.popWin.Move((pos[0],pos[1]+sz[1]))
            self.popWin.Show(True)
        else:
            self.popWin.Show(False)
        evt.Skip()
