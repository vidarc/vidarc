#----------------------------------------------------------------------------
# Name:         vtNetXmlLocks.py
# Purpose:      
#               
# Author:       Walter Obweger
#
# Created:      20060417
# CVS-ID:       $Id: vtNetXmlLock.py,v 1.2 2006/08/29 10:06:24 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.time.vtTime import vtDateTime

class vtNetXmlLock:
    def __init__(self,id=-1,addr='',port='',login=None):
        try:
            self.id=long(id)
        except:
            self.id=-1
        self.addr=addr
        try:
            self.port=int(port)
        except:
            self.port=-1
        if login is not None:
            self.usrId=login.GetUsrId()
            self.usr=login.GetUsr()
        else:
            self.usrId=-1
            self.usr=''
        self.dt=vtDateTime()
    def IsAddr(self,addr,port):
        return self.addr==addr and self.port==port
    def IsAddrPort(self,addr):
        return self.addr==addr[0] and self.port==addr[1]
    def IsID(self,id):
        return self.id==long(id)
    def IsIDAddr(self,id,addr,port):
        if self.id==long(id):
            if self.addr==addr and self.port==port:
                return True
        return False
    def IsIDAddrPort(self,id,addr):
        if self.id==long(id):
            if self.addr==addr[0] and self.port==addr[1]:
                return True
        return False
    def GetID(self):
        return self.id
    def GetUsr(self):
        return self.usr
    def GetAddrPort(self):
        return (self.addr,self.port)
    def GetAddr(self):
        return self.addr
    def GetPort(self):
        return self.port
    def GetDateTimeStr(self):
        return self.dt.GetDateTimeStr()
    def GetStr(self):
        sDt=self.dt.GetDateTimeStr()
        return '%d;%s;%d;%s;%s'%(self.id,self.addr,self.port,self.usr,sDt)
    def SetStr(self,s):
        strs=s.split(';')
        try:
            self.id=long(strs[0])
        except:
            self.id=-1
        self.addr=strs[1]
        try:
            self.port=int(strs[2])
        except:
            self.port=-1
        self.usr=strs[3]
        self.dt.SetStr(strs[4])
        
