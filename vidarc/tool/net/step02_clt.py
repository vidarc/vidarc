#----------------------------------------------------------------------------
# Name:         step02_clt.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: step02_clt.py,v 1.1 2005/12/11 23:04:49 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import thread
import time
import socket

# Echo client program
import socket
import sys

HOST = ''    # The remote host
PORT = 50007              # The same port as used by the server
s = None
for res in socket.getaddrinfo(HOST, PORT, socket.AF_UNSPEC, socket.SOCK_STREAM):
    af, socktype, proto, canonname, sa = res
    try:
    	s = socket.socket(af, socktype, proto)
    except socket.error, msg:
    	s = None
    	continue
    try:
    	s.connect(sa)
    except socket.error, msg:
    	s.close()
    	s = None
    	continue
    break
if s is None:
    print 'could not open socket'
    sys.exit(1)
s.send('Hello, world')
data = s.recv(1024)
s.close()
print 'Received', repr(data)

