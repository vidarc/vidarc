#----------------------------------------------------------------------------
# Name:         vtNetXmlNodeHostsClt.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060628
# CVS-ID:       $Id: vtNetXmlNodeHostsClt.py,v 1.2 2006/08/29 10:06:24 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlNodeBase import *

class vtNetXmlNodeHostsClt(vtXmlNodeBase):
    NODE_ATTRS=[
        ]
    FUNCS_GET_SET_4_LST=[]
    def __init__(self,tagName='Hosts'):
        global _
        _=vtLgBase.assignPluginLang('vtNet')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'hosts')
    # ---------------------------------------------------------
    # specific
    def IsSkip(self):
        "do not display node in tree"
        return True
    def GetEditDialogClass(self):
        return None
    def GetAddDialogClass(self):
        return None
    def GetPanelClass(self):
        return None
