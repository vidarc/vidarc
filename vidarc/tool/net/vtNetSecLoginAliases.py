#----------------------------------------------------------------------------
# Name:         vNetSecLoginAliases.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060414
# CVS-ID:       $Id: vtNetSecLoginAliases.py,v 1.7 2008/02/02 13:43:38 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import types

from vidarc.tool.net.vtNetSecLoginAppl import vtNetSecLoginAppl
from vidarc.tool.net.vtNetSecLoginHost import vtNetSecLoginHost
from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer

import vidarc.tool.log.vtLog as vtLog


class vtNetSecLoginAliases(vtXmlDomConsumer):
    VERBOSE=0
    def __init__(self,appls):
        vtXmlDomConsumer.__init__(self)
        self.docHum=None
        self.appls=appls
        self.lAppl=[]
    def Clear(self):
        vtXmlDomConsumer.Clear(self)
        self.lAppl=[]
    def __ensureOfflineConnection__(self):
        if self.GetConnection('---') is None:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCurCls(vtLog.INFO,'add offline'%(),self)
            oA=vtNetSecLoginAppl('---','0')
            for appl in self.appls:
                o=vtNetSecLoginHost(passwd='',
                        usr='',usrId='-1',grpIds='',grps='',
                        iSecLv='0',bAdmin='0',
                        host='',port='-1',appl=appl,alias='')
                oA.AddAlias(appl,o)
            self.AddAppl(oA)
    def __showAppls__(self):
        vtLog.CallStack('')
        print self.lAppl
        for a in self.lAppl:
            #print a.GetHostAliasStr(self.appls[0])
            print a.GetHostConnectionStr(self.appls[0])
    def SetNode(self,node,bNet=False):
        if self.VERBOSE:
            vtLog.CallStack('')
            print self.appls
        self.Clear()
        vtXmlDomConsumer.SetNode(self,node)
        if self.doc is None:
            self.__ensureOfflineConnection__()
            return
        #if self.VERBOSE:
        #    print cc
        try:
            self.doc.acquire('dom')
            cc=self.doc.getChilds(self.node,'alias')
            for c in cc:
                sName=self.appls[0]
                sRecent=self.doc.getNodeText(c,'recent')
                sAuto=self.doc.getNodeText(c,'autoconnect')
                sRecent=self.doc.getNodeText(c,'recent')
                sDt=self.doc.getNodeText(c,'datetime')
                iIdent=self.doc.GetValue(c,'ident',int,1)
                iStore=self.doc.GetValue(c,'store',int,0)
                oA=vtNetSecLoginAppl(sName,sAuto,sRecent,sDt,store=iStore,ident=iIdent)
                for appl in self.appls:
                    o=vtNetSecLoginHost(passwd='',
                            usr='',usrId='-1',grpIds='',grps='',
                            iSecLv='0',bAdmin='0',
                            host='',port='-1',appl=appl,alias='')
                    oA.AddAlias(appl,o)
                sApplMain=self.appls[0]
                sUsr,sUsrId,sGrpIds,sGrps,sSec,sAdmin,sPasswd='','-1',[],[],'0','0',''
                for appl in self.appls:
                    cAppl=self.doc.getChild(c,appl)
                    if self.VERBOSE:
                        print 'appl',appl,cAppl
                    if cAppl is not None:
                        sPort=self.doc.getNodeText(cAppl,'port')
                        try:
                            iPort=int(sPort)
                            if iPort<0:
                                #offline
                                sFN=self.doc.getNodeText(cAppl,'fn')
                                o=vtNetSecLoginHost(passwd='',
                                        usr='',usrId=-1,grpIds='',grps='',
                                        iSecLv='0',bAdmin='0',
                                        host='',port='-1',appl=appl,alias='')
                                o.SetFN(sFN)
                                oA.AddAlias(appl,o)
                                continue
                        except:
                            vtLog.vtLngTB(self.__class__.__name__)
                            pass
                        sHost=self.doc.getNodeText(cAppl,'name')
                        sAppl=appl
                        sAlias=self.doc.getNodeText(cAppl,'alias')
                        if iIdent and sApplMain!=appl:
                            pass
                        else:
                            sUsr=self.doc.getNodeText(cAppl,'usr')
                            cTmp=self.doc.getChild(cAppl,'usr')
                            if cTmp is not None:
                                sUsrId=self.doc.getAttribute(cTmp,'fid')
                            else:
                                sUsrId='-1'
                            try:
                                sGrpIds=self.doc.getNodeText(cAppl,'grpIds').split(',')
                            except:
                                sGrpIds=[]
                            try:
                                sGrps=self.doc.getNodeText(cAppl,'grps').split(',')
                            except:
                                sGrps=[]
                            
                            sSec=self.doc.getNodeText(cAppl,'secLv')
                            sAdmin=self.doc.getNodeText(cAppl,'admin')
                            if iStore:
                                sPasswd=self.doc.getNodeText(cAppl,'passwd')
                            else:
                                sPasswd=''
                        o=vtNetSecLoginHost(sPasswd,usr=sUsr,usrId=long(sUsrId),
                                grpIds=sGrpIds,grps=sGrps,iSecLv=sSec,bAdmin=sAdmin,
                                host=sHost,port=sPort,appl=sAppl,alias=sAlias)
                        oA.AddAlias(appl,o)
                        if self.VERBOSE:
                            print o.GetStr()
                self.AddAppl(oA)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        self.doc.release('dom')
        self.__ensureOfflineConnection__()
        
    def GetNode(self,node=None):
        if node is None:
            node=self.node
        if node is None:
            return
        cc=self.doc.getChilds(node,'alias')
        for c in cc:
            self.doc.deleteNode(c,node)
        for oA in self.lAppl:
            sName=oA.GetHostAliasStr(self.appls[0])
            sAuto=oA.GetAuto()
            sRecent=oA.GetRecentStr()
            sDt=oA.GetDateTimeGM()
            sIdent=oA.GetIdentStr()
            sStore=oA.GetStoreStr()
                
            cTmp=self.doc.createChildByLst(node,'alias',[
                        {'tag':'name','val':sName},
                        {'tag':'autoconnect','val':sAuto},
                        {'tag':'recent','val':sRecent},
                        {'tag':'datetime','val':sDt},
                        {'tag':'store','val':sStore},
                        {'tag':'ident','val':sIdent},
                        ])
            d=oA.GetAliases()
            for k in self.appls:
                try:
                    o=d[k]
                    if o.IsOffline():
                        self.doc.createChildByLst(cTmp,k,[
                            {'tag':'name','val':o.GetHost()},
                            {'tag':'port','val':str(o.GetPort())},
                            {'tag':'appl','val':o.GetAppl()},
                            {'tag':'alias','val':o.GetAlias()},
                            {'tag':'usr','val':o.GetUsr()},
                            {'tag':'fn','val':o.GetFN()}])
                    else:
                        if oA.IsStore():
                            sPasswd=o.GetPasswd()
                        else:
                            sPasswd=''
                        self.doc.createChildByLst(cTmp,k,[
                            {'tag':'name','val':o.GetHost()},
                            {'tag':'port','val':str(o.GetPort())},
                            {'tag':'appl','val':o.GetAppl()},
                            {'tag':'alias','val':o.GetAlias()},
                            {'tag':'usr','val':o.GetUsr(),'attr':['fid','%08d'%o.GetUsrId()]},
                            {'tag':'grpIds','val':','.join(map(str,o.GetGrpIds()))},
                            {'tag':'grps','val':','.join(map(str,o.GetGrps()))},
                            {'tag':'secLv','val':str(o.GetSecLv())},
                            {'tag':'admin','val':str(o.IsAdmin())},
                            {'tag':'passwd','val':sPasswd}])
                except:
                    vtLog.vtLngTB(self.__class__.__name__)
        self.doc.AlignNode(node,iRec=3)
    def GetAppls(self):
        return self.lAppl
    def GetConnections(self):
        l=[]
        for it in self.lAppl:
            #l.append(it.GetHostAliasStr(self.appls[0]))
            l.append(it.GetHostConnectionStr(self.appls[0]))
        return l
    def SelectConnection(self,sName):
        try:
            l=self.GetConnections()
            if sName not in l:
                return -2
            iSel=l.index(sName)
            self.lAppl=[self.lAppl[iSel]]+self.lAppl[:iSel]+self.lAppl[iSel+1:]
            self.GetNode()
            return 0
        except:
            vtLog.vtLngTB(self.__class__.__name__)
            return -1
    def GetConnection(self,sName=None):
        try:
            if self.VERBOSE:
                vtLog.vtLngCurCls(vtLog.DEBUG,'sName:%s'%(repr(sName)),self)
            if sName is None:
                return self.lAppl[0]
            else:
                l=self.GetConnections()
                i=l.index(sName)
                return self.lAppl[i]
        except:
            if self.VERBOSE:
                vtLog.vtLngTB(self.__class__.__name__)
            pass
        return None
    def GetConnectionAppl2Cfg(self):
        conn=self.GetConnection()
        for k in self.appls:
            if conn.IsAppl2Cfg(k)==True:
                return k
        return None
    def GetAppl(self):
        return self.appls
    def __getApplName__(self,i):
        return self.appls[i]
    def SetAlias(self,idx,appl,o):
        try:
            oAlias=self.lAppl[idx]
            oAlias.AddAlias(appl,o)
            return True
        except:
            return False
    def DelAlias(self,idx,iAppl):
        oAlias=self.lAppl[idx]
        if iAppl==0:
            #oAlias.Clear(self.appls[0])
            oAlias.ClearAll(self.appls)
        else:
            oAlias.DelAlias(self.__getApplName__(iAppl))
    def GetApplLst(self,idx):
        l=[]
        try:
            oA=self.lAppl[idx]
            d=oA.GetAliases()
            for k in self.appls:
                try:
                    l.append(d[k])
                except:
                    pass
        except:
            pass
        return l
    def AddApplAlias(self,obj,pos=-1):
        #sName=obj.GetAppl()
        #oA=vtNetSecLoginAppl(obj.GetHostAliasStr(),'1')
        oA=vtNetSecLoginAppl(obj.GetHostConnectionStr(),'1')
        oA.AddAlias(self.appls[0],obj)
        for appl in self.appls[1:]:
            o=vtNetSecLoginHost(passwd=obj.GetPasswd(),
                            usr=obj.GetUsr(),usrId=obj.GetUsrId(),
                            grpIds=obj.GetGrpIds(),grps=obj.GetGrps(),
                            iSecLv=obj.GetSecLv(),bAdmin=obj.IsAdmin(),
                            host=obj.GetHost(),port=obj.GetPort(),appl=appl,alias='')
            oA.AddAlias(appl,o)
        if pos>=0:
            self.lAppl.insert(pos,oA)
        else:
            self.lAppl.append(oA)
    def GetApplLogin(self,idx):
        try:
            return self.lAppl[idx]
        except:
            return None
    def AddAppl(self,obj,pos=-1):
        #sName=obj.GetAppl()
        if pos>0:
            self.lAppl.insert(pos,obj)
        else:
            self.lAppl.append(obj)
    def DelAppl(self,iSel):
        try:
            self.lAppl=self.lAppl[:iSel]+self.lAppl[iSel+1:]
        except:
            pass
