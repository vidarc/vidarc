#----------------------------------------------------------------------------
# Name:         vtParserStream.py
# Purpose:      very basic stream parser object 
#
# Author:       Walter Obweger
#
# Created:      20090410
# CVS-ID:       $Id: vtParserStream.py,v 1.6 2009/04/26 17:10:20 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os
from types import ListType,StringType,IntType,TupleType
from cStringIO import StringIO

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
from vtBuffer import vtBuffer
from vtStateFlagSimple import vtStateFlagSimple

class vtParserLevelSizeException(Exception):
    def __init__(self,s1,s2):
        self.sLevelUp=s1
        self.sLevelDown=s2
    def __str__(self):
        return 'vtParserLevelSizeException:level up:%s level down:%s'%(self.sLevelUp,self.sLevelDown)

class vtParserStream(vtLog.vtLogMixIn,vtBuffer):
    VERBOSE=0
    #brace {}
    #bracket ()
    #square bracket []
    CMD=None
    WILDCARD='*'
    WHITESPACE=['\t','\n','\r','\x0b','\x0c']
    END=[' ','\n','\r']
    def __init__(self,origin=None,max=2048,verbose=0):
        vtBuffer.__init__(self,max,reorganise=max/4)
        self.VERBOSE=verbose
        self.oParse=vtStateFlagSimple(
                    {
                    0x00:{'name':'UNDEF','is':'IsUndef','init':True,
                        'enter':{'func':(self.NotifyUndef,(),{}),'clr':['lv','cmd','comm']}},
                    0x01:{'name':'GEN','is':'IsGenerated',
                        'enter':{'func':(self.NotifyGen,(),{}),'clr':['lv','cmd','comm']}},
                    0x02:{'name':'CLR','is':'IsClr',
                        'enter':{'func':(self.NotifyClr,(),{}),'clr':['lv','cmd','comm']}},
                    0x03:{'name':'START','is':'IsStart',
                        'enter':{'func':(self.NotifyStart,(),{}),'clr':['lv','cmd','comm']}},
                    0x04:{'name':'END','is':'IsEnd',
                        'enter':{'func':(self.NotifyEnd,(),{}),'clr':['lv','cmd','comm']}},
                    0x10:{'name':'CMD_STA','is':'IsCmdStart',
                        'enter':{'func':(self.NotifyCmdStart,(),{}),'set':['cmd']}},
                    0x11:{'name':'CMD_OK','is':'IsCmdOk',
                        'enter':{'func':(self.NotifyCmdOk,(),{})}},
                    0x12:{'name':'CMD_END','is':'IsCmdEnd',
                        'enter':{'func':(self.NotifyCmdEnd,(),{})}},
                    0x13:{'name':'CMD_FLT','is':'IsCmdFlt',
                        'enter':{'func':(self.NotifyCmdFault,(),{})}},
                    0x19:{'name':'CMD_UNDEF','is':'IsCmdUnDef',
                        'enter':{'func':(self.NotifyCmdUnDef,(),{})}},
                    0x20:{'name':'LV_UP',#'is':'IsBracketOpen',
                        'enter':{'func':(self.NotifyLvUp,(),{}),'set':['lv']}},
                    0x21:{'name':'LV_DAT',#'is':'IsBracketOpen',
                        'enter':{'func':(self.NotifyLvData,(),{})}},
                    0x30:{'name':'LV_DN',#'is':'IsBracketClosed',
                        'enter':{'func':(self.NotifyLvDn,(),{})}},
                    0x40:{'name':'COMM_STA','is':'IsComment',
                        'enter':{'func':(self.NotifyCommentStart,(),{}),'set':['comm']}},
                    0x41:{'name':'COMM_END',#'is':'IsComment',
                        'enter':{'func':(self.NotifyCommentEnd,(),{}),'clr':['comm']}},
                    0x80:{'name':'WC_STA',#'is':'IsBracketOpen',
                        'enter':{'func':(self.NotifyWildCardStart,(),{}),'clr':['cmd'],'set':['wc']}},
                    0x81:{'name':'WC_END',#'is':'IsBracketOpen',
                        'enter':{'func':(self.NotifyWildCardEnd,(),{}),'clr':['wc'],'set':['cmd']}},
                    },
                    {
                    0x0001:{'name': 'lv',           'order':1,  'is':'IsLevelAct'},
                    0x0002:{'name': 'comm',         'order':2,  'is':'IsCommentAct'},
                    0x0004:{'name': 'cmd',          'order':3,  'is':'IsCmdAct'},
                    0x0008:{'name': 'wc',           'order':5,  'is':'IsWildCardAct'},
                    },
                    origin=origin)
        self.GetOrigin=self.oParse.GetOrigin
        self.dParse=None
        self.dParseAct=None
        self.dParseStore=None
        self.lFunc=None
        self.iProcFound=-1
        self.dLevel={}
        self.iProc=0
        self.iProcPrev=-1
        self.iStart=-1
        self.iStartCmd=-1
        self.lIdx=[]
    def SetParseCmd(self,l):
        try:
            if self._acquire(blocking=True):
                self.CMDS=l
                self.oParse.SetState(self.oParse.UNDEF)
                self._release()
            #self.__genParse__()
        except:
            self.__logTB__()
    def AddParseCmd(self,l):
        try:
            if self._acquire(blocking=True):
                self._release()
        except:
            self.__logTB__()
    def SetRecursion(self,iRec=1,iNum=0):
        try:
            if self._acquire(blocking=True):
                if iRec>0:
                    if iNum>0:
                        sOrigin=':'.join(self.GetOrigin().split(':')[:-1]+['%d'%iNum])
                    else:
                        sOrigin=':'.join([self.GetOrigin(),'%d'%iNum])
                    self.oRec=vtParser(sOrigin,self.iMax,self.lLevelUp,self.lLevelDn,
                            max(0,self.VERBOSE-1))
                    self.oRec.WILDCARD=self.WILDCARD
                    if iRec>1:
                        self.oRec.SetRecursion(iRec-1,iNum+1)
                else:
                    self.oRec=None
                self._release()
        except:
            self.__logTB__()
    def Init(self,dParse,dComment):
        try:
            if self._acquire(blocking=True):
                self.iProcPrev=0
                vtBuffer.clear(self)
                self.dParse=dParse
                self.dComment=dComment
                self.oParse.SetState(self.oParse.CLR)
                self.oParse.SetState(self.oParse.START)
                if self.oRec is not None:
                    self.oRec.Init()
                if self.oSlv is not None:
                    self.oSlv.Init()
                self._release()
        except:
            self.__logTB__()
    def Reset(self):
        try:
            if self._acquire(blocking=True):
                vtBuffer.clear(self)
                self.oParse.SetState(self.oParse.UNDEF)
                if self.oRec is not None:
                    self.oRec.Reset()
                if self.oSlv is not None:
                    self.oSlv.Reset()
                self._release()
        except:
            self.__logTB__()
    def Clear(self):
        try:
            if self._acquire(blocking=True):
                self.Parse(None)
                self.oParse.SetState(self.oParse.END)
                self.oParse.SetState(self.oParse.CLR)
                vtBuffer.clear(self)
                if self.oRec is not None:
                    self.oRec.Clear()
                if self.oSlv is not None:
                    self.oSlv.Clear()
                self._release()
        except:
            self.__logTB__()
    def __getLevelIdx__(self,c):
        return self.dLevel.get(c,0)
        if c in self.dLevel:
            return self.dLevel[c]
        return 0
        if c in self.lLevelUp:
            idx=self.lLevelUp.index(c)
            return idx+1
        elif c in self.lLevelDn:
            idx=self.lLevelDn.index(c)
            return -idx-1
        return 0
    def __mapParseDict__(self,d,lS=None,iLv=0,iLimit=20):
        bFirst=False
        if lS is None:
            lS=['']
            bFirst=True
        if type(d)==ListType:
            return
        keys=d.keys()
        keys.sort()
        for k in keys:
            if type(d[k])==IntType:
                lS.append(''.join([('  '*iLv),'%4d'%k,':',str(d[k])]))
            elif type(d[k])==ListType:
                lS.append(''.join([('  '*iLv),'%4s'%repr(k),':',self.__logFmt__(d[k])]))
            else:
                lS.append(''.join([('  '*iLv),'%4s'%repr(k),':']))
                if iLimit>0:
                    self.__mapParseDict__(d[k],lS,iLv+1,iLimit-1)
        if bFirst:
            return ';'.join(lS)
    def __gen__(self,d,t):
        bWildCard=False
        #bFirst=True
        for c in t[0]:
            dLast=d
            if c in self.WILDCARD:
                if bWildCard:
                    # qutoed
                    #d[c]={}
                    #d=d[c]
                    bWildCard=False
                    #continue
                else:
                    bWildCard=True
                    continue
            if bWildCard:
                    d[-2]={}
                    #d=d[-2]
                    bWildCard=False
                    #continue
            if c not in d:
                d[c]={}
                d=d[c]
                #if bFirst:
                #    d[-9]=
                #    bFirst=False
            else:
                d=d[c]
        if bWildCard:
            d={}
            dLast[-2]=d
            #d=d[-2]
            bWildCard=False
        return dLast,c,d
    def __applyFunc__(self,d,lArgs):
            #print d,lArgs
            mode=d[0]
            t=d[1]
            
            if mode==1:
                    if self.VERBOSE>0:
                        self.__logDebug__('call f1;'%())
                    return t[0](*lArgs)
            elif mode==2:
                    args=tuple(lArgs+list(t[1]))#+self.args
                    if self.VERBOSE>0:
                        self.__logDebug__('call f2;args:%s'%(
                                self.__logFmt__(args)))
                    return t[0](*args)
            elif mode==3:
                    args=tuple(lArgs+list(t[1]))#+self.args
                    kwargs=t[2].copy()
                    #kwargs.update(self.kwargs)
                    if self.VERBOSE>0:
                        self.__logDebug__('call f3;args:%s;kwargs:%s'%(
                                self.__logFmt__(args),
                                self.__logFmt__(kwargs)))
                    return t[0](*args,**kwargs)
            elif mode==10:
                if self.VERBOSE>0:
                    self.__logDebug__('call str'%())
                if hasattr(self,t):
                    return getattr(self,t)(tuple(lArgs))
            elif mode==20:
                if self.VERBOSE>0:
                    self.__logDebug__('call f0;args:%s;kwargs:%s'%(
                                self.__logFmt__(lLv),
                                #self.__logFmt__(self.args),
                                #self.__logFmt__(self.kwargs)))
                                '',''))
                if mode==0:
                    return t(*self.args,**self.kwargs)
                    pass
                elif mode==1:
                    lFmt=d[2]
                    if lFmt is None:
                        return t(*lVal)
                    else:
                        return t(*[f(v) for f,v in zip(lFmt,lVal)])
            else:
                self.__logCritical__('%s'%(self.__logFmt__(d)))
            return None
    def __getFunc__(self,t):
        iLen=len(t)
        if iLen==1:
            return None
        elif iLen==2:
            ttype=type(t[1])
            if ttype==StringType:
                return [10,t[1]]
            elif ttype==TupleType:
                return [len(t[1]),t[1]]
            return [0,t[1]]
        else:
            return [-1,None]
    def __genParse__(self):
        if self.CMDS is None:
            self.dParse={}
            self.dParseAct=self.dParse
            return
        self.dParse={}
        self.dComment={}
        self.dParseAct=self.dParse
        for t in self.CMDS:
            d=self.dParse
            if type(t[0])==ListType:
                dAct=None
                for tt in t[0]:
                    if self.VERBOSE>1:
                        if self.__isLogDebug__():
                            self.__logDebug__('tt:%s'%(self.__logFmt__(tt)))
                    dAct,c,d=self.__gen__(d,tt)
                    if self.VERBOSE>1:
                        if self.__isLogDebug__():
                            self.__logDebug__('d:%s'%self.__mapParseDict__(d))
                            self.__logDebug__('dParse:%s'%self.__mapParseDict__(self.dParse))
                    if c in dAct:
                        d=dAct[c]
                    elif -2 in dAct:
                        #d=dAct[-2]
                        d=dAct
                    else:
                        self.__logCritical__('parse definition fault;fmt:%s'%(self.__logFmt__(t)))
                        dAct=None
                        break
                    iLen=len(tt)
                    if iLen>1:
                        dAct[-3]=self.__getFunc__(tt)#[0,tt]
                dLast=dAct
                if dLast is not None:
                    #if -1 in d:
                    #    vtLog.vtLngCur(vtLog.CRITICAL,'cmd:%s;dParse;%s'%(t[0],
                    #                vtLog.pformat(self.dParse)),self.GetOrigin())
                    #else:
                    #    d[c]=self.__getFunc__(t)#[0,t[1]]
                    if c in dLast:
                        if type(dLast[c])==ListType:
                            vtLog.vtLngCur(vtLog.CRITICAL,'cmd:%s;dParse;%s'%(t[0],
                                    vtLog.pformat(self.dParse)),self.GetOrigin())
                        else:
                            dLast[c]=self.__getFunc__(t)#[0,t[1]]
            else:
                dLast,c,d=self.__gen__(d,t)
                #if -1 in d:
                #    vtLog.vtLngCur(vtLog.CRITICAL,'cmd:%s;dParse;%s'%(t[0],
                #                vtLog.pformat(self.dParse)),self.GetOrigin())
                #else:
                #    d[c]=self.__getFunc__(t)#[0,t[1]]
                if c in dLast:
                    if type(dLast[c])==ListType:
                        vtLog.vtLngCur(vtLog.CRITICAL,'cmd:%s;dParse;%s'%(t[0],
                                vtLog.pformat(self.dParse)),self.GetOrigin())
                    else:
                        dLast[c]=self.__getFunc__(t)#[0,t[1]]
            if self.VERBOSE>2:
                if self.__isLogDebug__():
                    self.__logDebug__('cmd:%s;dParse;%s'%(t[0],self.__logFmt__(self.dParse)))
        if self.__isLogDebug__():
            self.__logDebug__('dParse:%s'%self.__mapParseDict__(self.dParse))
            self.__logDebug__('dComment:%s'%self.__mapParseDict__(self.dComment))
        self.oParse.SetState(self.oParse.START)
    def __getParseAct__(self):
        return self.dParseAct
    def __storeIdxState__(self,iState,iLvIdx=-1,iLv=-1,tFunc=None):
        self.lIdx.append((self.iAct,iState,iLvIdx,iLv,tFunc))
    def __getDataByIdx__(self):
        try:
            i=self.buf.tell()
            if len(self.lIdx)>1:
                iO=self.lIdx[0][0]
                iA=self.lIdx[-1][0]
                self.buf.seek(iO)
                return self.buf.read(iA-iO)
        finally:
            self.buf.seek(i)
        return ''
    def __logObjData__(self,bRaw=False):
        if self.__isLogDebug__()==False:
            return
        try:
            if self.VERBOSE==1:
                if bRaw:
                    sRaw=self.__getDataByIdx__()
                else:
                    sRaw=''
                self.__logDebug__(u'state:;%s;iAct:%8d,iProc:%8d,iFill:%8d;iStart:%8d,iStartCmd:%8d;sRaw:%s'%(self.oParse.GetStrFull(';'),
                            self.iAct,self.iProc,self.iFill,self.iStart,self.iStartCmd,self.__logConv__(sRaw),),
                            level2skip=1)
            elif self.VERBOSE>1:
                s=self.__getDataByIdx__()
                self.__logDebug__(u'state:;%s;iAct:%8d,iProc:%8d,iFill:%8d;iStart:%8d,iStartCmd:%8d;>%s<'%(self.oParse.GetStrFull(';'),
                            self.iAct,self.iProc,self.iFill,self.iStart,self.iStartCmd,self.__logConv__(s),),
                            level2skip=1)
        except:
            self.__logDebug__(u'state:;%s;iAct:%8d,iProc:%8d,iFill:%8d'%(self.oParse.GetStrFull(';'),
                            self.iAct,self.iProc,self.iFill,),
                            level2skip=1)
            self.__logTB__()
    def NotifyUndef(self):
        try:
            self.dParse=None
            self.dComment=None
            self.dParseAct=None
            self.dParseStore=None
            self.lFunc=None
            self.iProcFound=-1
            self.iStart=-1
            self.iStartCmd=-1
            self.dLevel={}
            self.iProc=0
            self.iProcPrev=-1
            self.lIdx=[]
            #self.dParseAct=self.dParse
            if self.VERBOSE>0:
                self.__logObjData__()
        except:
            self.__logTB__()
    def NotifyGen(self):
        try:
            self.__genParse__()
            if self.VERBOSE>0:
                self.__logObjData__()
        except:
            self.__logTB__()
    def NotifyClr(self):
        try:
            #self.dParse=None
            #self.dComment=None
            #self.dParseAct=None
            self.dParseStore=None
            self.lFunc=None
            self.iProcFound=-1
            #self.dLevel={}
            self.iProc=0
            self.iProcPrev=-1
            self.lIdx=[]
            #self.dParseAct=self.dParse
            if self.VERBOSE>0:
                self.__logObjData__()
        except:
            self.__logTB__()
    def __reorganise__(self):
        iAct=self.iAct
        vtBuffer.__reorganise__(self)
        if iAct!=self.iAct:
            self.iProc=self.iProc-(iAct-self.iAct)
            self.buf.seek(self.iProc+1)
            if self.VERBOSE>0:
                self.__logObjData__()
            
    def NotifyStart(self):
        try:
            #self.__reorganise__()
            self.__clear__()
            self.iProcPrev=-1
            self.lIdx=[]
            self.lFunc=None
            self.iProcFound=-1
            self.iStart=self.iProc
            self.iStartCmd=-1
            #self.__storeIdxState__()
            self.dParseAct=self.dParse
            self.dParseStore=None
            if self.VERBOSE>1:
                self.__logObjData__()
        except:
            self.__logTB__()
    def NotifyLvUp(self):
        try:
            if self.VERBOSE>1:
                self.__logObjData__()
            if self.iLevelIdx<0:
                self.oParse.SetState(self.oParse.CMD_FLT)
                return
            self.lLevel[self.iLevelIdx]+=1
            self.__storeIdxState__(self.iLevelIdx,self.lLevel[self.iLevelIdx])
            if self.VERBOSE>1:
                self.__logObjData__()
            self.oParse.SetState(self.oParse.LV_DAT)
        except:
            self.__logTB__()
    def NotifyLvData(self):
        try:
            if self.VERBOSE>1:
                self.__logObjData__()
        except:
            self.__logTB__()
    def NotifyLvDn(self):
        try:
            if self.iLevelIdx<0:
                self.oParse.SetState(self.oParse.CMD_FLT)
                return
            self.lLevel[self.iLevelIdx]-=1
            if self.lLevel[self.iLevelIdx]<-1:
                self.oParse.SetState(self.oParse.CMD_FLT)
            self.__storeIdxState__(self.iLevelIdx,self.lLevel[self.iLevelIdx])
            if self.VERBOSE>1:
                self.__logObjData__()
            for iLv in self.lLevel:
                if iLv>-1:
                    self.oParse.SetState(self.oParse.LV_DAT)
                    break
        except:
            self.__logTB__()
    def NotifyWildCardStart(self):
        try:
            self.__storeIdxState__(self.oParse.WC_STA,0,0)
            if self.VERBOSE>1:
                self.__logObjData__()
        except:
            self.__logTB__()
    def NotifyWildCardEnd(self):
        try:
            if -3 in self.dParseAct:
                self.__storeIdxState__(self.oParse.WC_END,0,0,self.dParseAct[-3])
            else:
                self.__storeIdxState__(self.oParse.WC_END,0,0)
            if self.VERBOSE>1:
                self.__logObjData__()
        except:
            self.__logTB__()
    def NotifyCmdStart(self):
        try:
            self.iStartCmd=self.iProc
            #self.__storeIdxState__()
            if self.VERBOSE>1:
                self.__logObjData__()
        except:
            self.__logTB__()
    def __get__(self,iO,iA):
        if iO>=0 and iA>=0:
            self.buf.seek(iO)
            if self.VERBOSE>2 and self.__isLogDebug__():
                self.__logDebug__('__get__;iO:%d;iA:%d;diff:%d'%(iO,iA,iA-iO))
            return self.buf.read(iA-iO)
            #if self.bUnicode:
            #    return self.buf.read(iA-iO).decode('utf-8')
            #else:
            #    return self.buf.read(iA-iO)
        else:
            return ''
            #if self.bUnicode:
            #    return u''
            #else:
            #    return ''
    def __write__(self,f,iO,iA):
        if iO<iA and iO>=0:
            self.buf.seek(iO)
            if self.VERBOSE>2 and self.__isLogDebug__():
                self.__logDebug__('__write__;iO:%d;iA:%d;diff:%d'%(iO,iA,iA-iO))
            f.write(self.buf.read(iA-iO))
            #if self.bUnicode:
            #    f.write(self.buf.read(iA-iO).decode('utf-8'))
            #else:
            #    f.write(self.buf.read(iA-iO))
    def NotifyCmdOk(self):
        try:
            #self.__storeIdxState__()
            d=self.dParseAct
            if self.VERBOSE>0:
                self.__logObjData__(bRaw=True)
            iIdx=-1
            lVal=[]
            #iAct=self.buf.tell()
            iO=-1
            iA=-1
            iOVal=-1
            iAVal=-1
            if self.VERBOSE>0:
                self.__logDebug__('lIdx:%s'%(self.__logFmt__(self.lIdx)))
            for t in self.lIdx:
                cmd=t[1]
                if cmd==self.oParse.WC_STA:
                    if iOVal==-1:
                        iOVal=t[0]
                elif cmd==self.oParse.WC_END:
                    if iOVal>=0:
                        iAVal=t[0]
                        val=self.__get__(iOVal,iAVal)
                        if t[4] is not None:
                            val=self.__applyFunc__(t[4],[val])
                        lVal.append(val)
                        iOVal,iAVal=-1,-1
            if self.VERBOSE>0:
                self.__logDebug__('lIdx:%s;lVal:%s'%(self.__logFmt__(self.lIdx),
                            self.__logFmt__(lVal)))
            #self.buf.seek(iAct)
            ##self.iAct=self.iProc
            #self.__clear__()
            self.oParse.SetState(self.oParse.START)
            
            self.__applyFunc__(d,lVal)
            
            if self.VERBOSE>0:
                self.__logObjData__()
        except:
            self.__logTB__()
        #self.iAct=self.iProc
        #self.oParse.SetState(self.oParse.START)
    def NotifyCmdEnd(self):
        try:
            #self.__storeIdxState__()
            if self.VERBOSE>0:
                self.__logObjData__()
        except:
            self.__logTB__()
        self.oParse.SetState(self.oParse.START)
    def NotifyCmdFault(self):
        try:
            if self.VERBOSE>1:
                self.__logObjData__(bRaw=True)
            #self.__clear__()
            #self.__storeIdxState__()
            #s=self.__getDataByIdx__()
            #if self.fOut is not None:
                ##s=self.__get__(self.iStart,self.iProc)
                ##self.fOut.write(s)
                #self.__write__(self.fOut,self.iStart,self.iProc)
            #self.iProc=self.lIdx[-1][0]
            #self.iProc=self.iStartCmd
            ##self.buf.seek(self.iAct)
            ##self.iAct=self.iProc
            ##self.iProcPrev=self.iAct-1
            if self.VERBOSE>1:
                self.__logObjData__(bRaw=True)
        except:
            self.__logTB__()
        self.oParse.SetState(self.oParse.START)
    def NotifyCmdUnDef(self):
        try:
            if self.VERBOSE>0:
                self.__logObjData__()
        except:
            self.__logTB__()
    def NotifyCommentStart(self):
        try:
            self.__storeIdxState__()
            self.iAct=self.iProc+1
            if self.VERBOSE>1:
                self.__logObjData__()
        except:
            self.__logTB__()
    def NotifyCommentEnd(self):
        try:
            self.__storeIdxState__()
            if self.VERBOSE>1:
                self.__logObjData__()
            if self.oParse.IsCommandAct==False:
                iO=-1
                iA=-1
                iIdx=-1
                lLv=[]
                iAct=self.buf.tell()
                iO=self.iStart
                for t in self.lIdx:
                    cmd=t[1]
                    if cmd==self.oParse.COMM_STA:
                        iA=t[0]
                        if self.fOut is not None:
                            #self.fOut.write(self.__get__(iO,iA))
                            self.__write__(self.fOut,iO,iA)
                            iO,iA=-1,-1
                self.buf.seek(iAct)
            self.iAct=self.iProc
            if self.VERBOSE>1:
                self.__logObjData__(bRaw=True)
        except:
            self.__logTB__()
        if self.oParse.IsCommandAct:
            self.dParseAct=self.dParseStore
        else:
            self.oParse.SetState(self.oParse.START)
    def NotifyEnd(self):
        try:
            if not self.oParse.IsStart:
                if self.lFunc is not None:
                    self.iProc=self.iProcFound
                    self.dParseAct=self.lFunc
                    self.oParse.SetState(self.oParse.CMD_OK)
            #if len(self.lIdx)>0:
            iO=self.iStart
            if iO>=0:
                #iO=self.lIdx[0][0]
                self.buf.seek(iO)
                
                self.__write__(self.fOut,iO,self.iFill)
                #s=self.__get__(self.iFill-iO)#self.buf.read(self.iFill-iO)
                self.iAct=self.iProc=self.iFill
                #if self.fOut is not None:
                #    self.fOut.write(s)
                self.lIdx=[]
                self.iProc=0
                self.iProcPrev=-1
                self.iStart=0
                self.iStartCmd=-1
                vtBuffer.clear(self)
                
            if self.VERBOSE>1:
                self.__logObjData__()
        except:
            self.__logTB__()
        #self.oParse.SetState(self.oParse.CLR)
        self.oParse.SetState(self.oParse.START)
    def Parse(self,s):
        try:
            if self.VERBOSE>0:
                self.__logDebug__(s)
            if s is None:
                self.oParse.SetState(self.oParse.END)
                return
            if self.oParse.IsUndef:
                self.oParse.SetState(self.oParse.GEN)
            iRet=0
            if self._acquire(blocking=True):
                iRet=self._parse(s)
                #self._Parse(s)
                self._release()
            return iRet
            self.push(s,False)
            #if self.oParse.IsCmdOk:
            #    return
            while self._Parse():
                pass
        except:
            self.__logTB__()
    def _parse(self,s):
        try:
            if self.VERBOSE>0:
                self.__logObjData__()
                self.__logDebug__(s)
            iCount=self.__getDataLen__()
            iLen=len(s)
            if (iCount+iLen)>=self.iMax:
                self.__logError__('possible buffer overrun;%d;max:%d;'%(iCount+iLen,self.GetMax()))
                return -2
            self.buf.seek(self.iAct)
            self.iProcPrev=self.iAct
            bDbg=self.__isLogDebug__()
            if self.VERBOSE==1:
                def verbose(fmt,args,d):
                    self.__logObjData__()
            elif self.VERBOSE>2:
                def verbose(fmt,args,d):
                    #self.__logObjData__()
                    lS=[fmt%args]
                    self.__mapParseDict__(d,lS,0,1)
                    self.__logDebug__(';'.join(lS),level2skip=1)
            else:
                def verbose(fmt,args,d):
                    pass
            d=self.dParseAct
            oP=self.oParse
            bCmd=oP.IsCmdAct
            bWC=oP.IsWildCardAct
            for c in s:
            #for iOfs in xrange(iCount):
                #c=self.buf.read(1)
                #self.iProc=self.iAct+iOfs
                if bDbg:
                    verbose('c:%s;%d',(repr(c),c in self.dParseAct),
                            self.dParseAct)
                    #verbose('iOfs:%d;iProc:%d;c:%s;%d',(iOfs,
                    #        self.iProc,repr(c),c in self.dParseAct),
                    #        self.dParseAct)
                if c in self.dParseAct:#d:
                    #if oP.IsWildCardAct:
                    if bWC:
                        #oP.SetState(oP.WC_END)
                        oP.SetFlag(oP.wc,False)
                        bWC=False
                        self.NotifyWildCardEnd()
                    self.dParseAct=self.dParseAct[c]
                    #dd=d[c]
                    #if oP.IsStart:
                    if bCmd==False:
                        oP.SetState(oP.CMD_STA)
                        bCmd=True
                        #self.buf.write(c)
                        #self.iFill+=1
                        #self.iAct=self.iFill-1
                        #d=dd
                    if type(self.dParseAct)==ListType:
                    #if type(dd)==ListType:
                        #self.dParseAct=dd
                        oP.SetState(oP.CMD_OK)
                        bCmd=False
                        bWC=False
                        if bDbg:
                            verbose('ret0;iAct:%d;iProc:%d',(self.iAct,
                                    self.iProc),self.dParseAct)
                        #d=self.dParseAct
                        #return True
                    #else:
                    #    d=dd
                else:
                    if -2 in self.dParseAct:#d:#c=='*':
                        #if oP.IsCmdAct:
                        if bCmd:
                            #if not oP.IsWildCardAct:
                            if not bWC:
                                #oP.SetState(oP.WC_STA)
                                oP.SetFlag(oP.wc,True)
                                bWC=True
                                self.NotifyWildCardStart()
                    else:
                        #if oP.IsCmdAct:
                        if bCmd:
                            oP.SetState(oP.CMD_FLT)
                            if bDbg:
                                verbose('ret4;iAct:%d;iProc:%d',(self.iAct,
                                        self.iProc),self.dParseAct)
                            bCmd=True
                            bWC=False
                            #d=self.dParseAct
                            #return True
                #if oP.IsCmdAct:
                if bCmd:
                    self.buf.write(c)
                    self.iFill+=1
                    self.iAct=self.iFill
                    #self.__logDebug__('%d;%d;%d'%(self.iAct,self.iFill,
                    #            self.buf.tell()))
            #self.iAct=self.iProc+1
            #if ' ' in d:
            #    self.lFunc=d[' ']
            #    self.iProcFound=self.iProc+1
            if bDbg:
                verbose('ret for;iAct:%d;iProc:%d',(self.iAct,
                        self.iProc),self.dParseAct)
        except:
            self.__logTB__()
        return 0
    def _Parse(self):
        try:
            if self.VERBOSE>1:
                self.__logObjData__()
            if self.oParse.IsUndef:
                self.oParse.SetState(self.oParse.GEN)
                iProc=self.iAct
            else:
                iProc=-1
            if self.iAct<self.iProcPrev:
                self.__logCritical__('recursion detected;iAct:%d;iProcPrev:%d'%(
                            self.iAct,self.iProcPrev))
                return False
            if self._acquire(blocking=False):
                self._parse()
                self._release()
            #self.dParseAct=d
            if self.VERBOSE>1:
                self.__logObjData__()
            return False
        except:
            self.__logTB__()
        return False
