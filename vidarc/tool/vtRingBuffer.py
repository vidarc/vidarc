#----------------------------------------------------------------------------
# Name:         vtRingBuffer.py
# Purpose:
# Author:       Walter Obweger
#
# Created:      20060804
# CVS-ID:       $Id: vtRingBuffer.py,v 1.3 2009/05/01 20:04:13 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vtThreadCore import getLock

class vtRingBuffer:
    def __init__(self,size):
        self.iSize=size
        self.iAct=self.iSize-1
        self.lSlot=[None]*self.iSize
        self.semAccess=getLock()
    def __next__(self):
        self.iAct= (self.iAct+1) % self.iSize
    def next(self):
        try:
            self.semAccess.acquire()
            self.__next__()
        finally:
            self.semAccess.release()
    def put(self,t):
        try:
            self.semAccess.acquire()
            self.__next__()
            self.lSlot[self.iAct]=t
        finally:
            self.semAccess.release()
    def get(self,iPos=0):
        try:
            self.semAccess.acquire()
            iPos=iPos % self.iSize
            iAct=self.iAct
            for i in xrange(0,iPos):
                iAct-=1
                if iAct<0:
                    iAct+=self.iSize
            return self.lSlot[iAct]
        finally:
            self.semAccess.release()
    def proc(self,func,*args,**kwargs):
        try:
            self.semAccess.acquire()
            iAct=self.iAct
            for i in xrange(0,self.iSize):
                func(self.lSlot[iAct],*args,**kwargs)
                iAct-=1
                if iAct<0:
                    iAct+=self.iSize
        finally:
            self.semAccess.release()
