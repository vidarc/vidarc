#----------------------------------------------------------------------------
# Name:         vtSockCltEcho.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20090812
# CVS-ID:       $Id: vtSockCltEcho.py,v 1.1 2009/08/13 09:59:20 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vtSock import vtSock

# Echo client program
class vtSockCltEcho(vtSock):
    #def __init__(self,*args):
    #    vtSock.__init__(self,args)
    def HandleData(self):
        print self.data
        self.data=''
        self.len=0
    def ParseData(self):
        while 1:
            data=self.conn.recv(1024)
            if not data:
                self.serving=False
                return
            print data
            #print
class vtSockCltEchoSilent(vtSock):
    def __init__(self,server,conn,adr):
        vtSock.__init__(self,server,conn,adr)
        self.receivedCount=0
    def HandleData(self):
        self.receivedCount+=len(self.data)
        self.data=''
        self.len=0
    def ParseData(self):
        while 1:
            data=self.conn.recv(1024)
            if not data:
                self.serving=False
                return
            self.receivedCount+=len(data)

