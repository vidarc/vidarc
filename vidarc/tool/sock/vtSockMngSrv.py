#----------------------------------------------------------------------------
# Name:         vtSockMngSrv.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20090812
# CVS-ID:       $Id: vtSockMngSrv.py,v 1.8 2011/03/31 11:25:55 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import time,random
import socket,sha,binascii
import select
import string

import vSystem

from vtSockMngCore import vtSockMngCore

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

FORCE_SHUTDOWN=20

SESSION_ID_CHARS=string.ascii_letters+string.digits
SESSION_ID_CHARS_LEN=len(SESSION_ID_CHARS)

class vtSockMngSrv(vtSockMngCore):
    def __init__(self,par,server,*args,**kwargs):
        vtSockMngCore.__init__(self,*args,**kwargs)
        
        self.srv=server
        self.SetPar(par)
        self.passwd=None
        self.sock=None
    def __initCtrl__(self,*args,**kwargs):
        vtSockMngCore.__initCtrl__(self,*args,**kwargs)
        pass
    def __initStateDict__(self):
        return {
                0:{'name':'CLOSED','is':'IsClosed','init':True,
                        'enter':{#'func':(self.SocketClosed,(),{}),
                                 'clr':['ACTIVE','PAUSING','PAUSED','STARTING','STOPPING','SERVING']}
                    },
                1:{'name':'SERVE','is':'IsServe',
                        'enter':{'set':['STARTING'],
                                 #'func':(self.SocketClosed,(),{}),
                                 'clr':['ACTIVE','PAUSING','PAUSED','SERVING']},
                    },
                2:{'name':'CLOSE','is':'IsClose',
                        'enter':{'set':['STOPPING'],
                                 #'func':(self.SocketClosed,(),{}),
                                 #'clr':['ACTIVE','PAUSING','PAUSED','STARTING']
                                },
                    },
                8:{'name':'SHUTTINGDOWN','is':'IsShuttingDown',
                        'enter':{#'func':(self.NotifyConnected,(),{}),
                                 'set':['STOPPING',],
                                 'clr':['PAUSING','PAUSED']}
                    },
                9:{'name':'SHUTDOWN','is':'IsShutDown',
                        'enter':{#'func':(self.NotifyConnected,(),{}),
                                 #'set':[],
                                 'clr':['ACTIVE','PAUSING','PAUSED','STARTING','STOPPING','SERVING']}
                    },
                }
    def __initFlagDict__(self):
        return {
                0x0001:{'name':'ACTIVE','is':'IsActive','order':0},
                0x0002:{'name':'PAUSING','is':'IsPausing','order':1},
                0x0004:{'name':'PAUSED','is':'IsPaused','order':2},
                0x0008:{'name':'SERVING','is':'IsServing','order':3},
                0x0020:{'name':'STARTING','is':'IsStarting','order':4},
                0x0010:{'name':'NOTITY_CONNECT_IMMEDIATLY','is':'IsNotifyConnectImmediatly'},
                0x0040:{'name':'STOPPING','is':'IsStopping','order':5},
                0x0080:{'name':'SELECT','is':'IsSelect','order':5},
                }
    def GenerateSessionID(self):
        sessionID=''
        for k in xrange(32):
            for l in [0,1,2,3]:
                f=random.random()
                s=repr(f)[2:]
                iLen=len(s)
                if iLen<16:
                    s+='4539869453023104356456983145'[:16-iLen]
                i=0
                while i<16:
                    j=int(s[i:i+2],16)%SESSION_ID_CHARS_LEN
                    sessionID+=SESSION_ID_CHARS[j]
                    i+=2
            if len(sessionID)==32:
                break
            else:
                sessionID=''
        return sessionID
    def SetPasswd(self,passwd):
        if self.passwd!=passwd:
            self.Close()
        self.passwd=passwd
    def Start(self,bUseSelect=False):
        self.__logInfo__(''%())
        try:
            if self.oSF.IsServing:
                self.__logError__('already served')
                return
            if self.oSF.IsStarting:
                self.__logError__('already starting')
                return
            if self.thdSockMng.Is2Stop():
                if self.thdSockMng.IsRunning():
                    self.__logError__('is2Stop active')
                    return
            if self.oSF.IsShutDown:
                self.__logError__('shut down is active')
                return
            if self.oSF.IsShuttingDown:
                self.__logError__('shutting down is active')
                return
            self.thdSockMng.Start()
            self.oSF.SetState(self.oSF.SERVE)
            if bUseSelect:
                self.thdSockMng.Do(self.doCommunicationSel)
            else:
                self.thdSockMng.Do(self.doCommunication)
        except:
            self.__logTB__()
    def Stop(self):
        self.__logInfo__(''%())
        self.oSF.SetState(self.oSF.CLOSE)
        self.thdSockMng.Stop()
        try:
            for v in self.connections.values():
                try:
                    v.Stop()
                except:
                    self.__logTB__()
        except:
            self.__logTB__()
        try:
            pass
        except:
            self.__logTB__()
    def Close(self):
        self.__logInfo__(''%())
        self.oSF.SetState(self.oSF.CLOSE)
        try:
            for v in self.connections.values():
                try:
                    v.Close()
                except:
                    self.__logTB__()
        except:
            self.__logTB__()
        self.connections={}
    def IsBusy(self):
        return False
    def StopSocket(self,adr,sockInst):
        try:
            self.__logInfo__('%r'%(repr(adr)))
            sockInst=self.connections[adr]
            client=sockInst.GetSock()
            cli=sockInst.GetSockFD()
            self.qConn.put((self.__removeSock__,(cli,),{}))
            if adr in self.connections:
                del self.connections[adr]
        except:
            self.__logTB__()
    def ShutDown(self):
        self.__logInfo__(''%())
        if self.oSF.IsShutDown:
            return
        self.srv=None
        if self.oSF.IsServing:
            self.oSF.SetState(self.oSF.SHUTDOWN)
        else:
            self.oSF.SetState(self.oSF.SHUTTINGDOWN)
        self.thdSockMng.Stop()
    def __calcConnected__(self):
        self.__logInfo__(''%())
        self._acquire()
        try:
            self._iConnectedCount=0
            if len(self.lRd)>0:
                self._iConnectedCount=reduce(lambda x,y:x+y,[len(lRd) for lRd in self.lRd])
                if self.oSF.IsSelect:
                    if self._iConnectedCount>0:
                        self._iConnectedCount-=1
        except:
            self.__logTB__()
            self._iConnectedCount=0
        self.oSF.SetFlag(self.oSF.ACTIVE,self._iConnectedCount!=0)
        self._release()
        if self.thdSockMng.IsRunning()==False:
            self.oSF.SetFlag(self.oSF.SERVING,self._iConnectedCount!=0)
        if self.thdSockMng.Is2Stop()==True:
            self.oSF.SetFlag(self.oSF.SERVING,self._iConnectedCount!=0)
    def __accept__(self,bUseSelect4Comm=True):
        try:
            conn, addr = self.sock.accept()
            socketClass,args,kwargs=self.tClsSock
            self.__logInfo__('incomming connection;class:%s'%(
                            socketClass))
            self.__logInfo__('Connected by %s'%str(addr))
            sID=self.GenerateSessionID()
            self.__logInfo__('sessionID:%s'%(sID))
            kkwargs=kwargs.copy()
            kkwargs.update({'bUseSelect4Comm':bUseSelect4Comm})
            sock=socketClass(self,conn,addr,sID,*args,**kkwargs)
            sock.SetParent(self.par)
            self.__addSock__(sock)
        except socket.timeout:
            pass
        except:
            self.__logTB__()
    def __procQueue__(self):
        while self.qConn.empty()==False:
            try:
                f,args,kwargs=self.qConn.get()
                f(*args,**kwargs)
            except:
                self.__logTB__()
    def doCommunicationSel(self):
        self.__logDebug__(''%())
        self.oSF.SetFlag(self.oSF.SERVING,True)
        self.oSF.SetFlag(self.oSF.STARTING,False)
        self._acquire()
        self.connections={}
        self._release()
        HOST=None#'localhost'
        self.__logInfo__('Run on port %d'%self.port)
        for res in socket.getaddrinfo(HOST, self.port, socket.AF_UNSPEC, socket.SOCK_STREAM, 0, socket.AI_PASSIVE):
            af, socktype, proto, canonname, sa = res
            try:
                self.sock = socket.socket(af, socktype, proto)
            except socket.error, msg:
                self.sock = None
                continue
            try:
                self.sock.bind(sa)
                self.sock.listen(3)
            except socket.error, msg:
                self.sock.close()
                self.sock = None
                continue
            break
        if self.sock is None:
            self.__logInfo__('socket is None')
            return
        self.sock.settimeout(2.0)
        self._acquire()
        fdSrv=self.sock.fileno()
        self.lRd=[[fdSrv]]
        self.dConn={} #{fdSrv:self}
        srv=self.sock
        self.lWr=[]
        self._release()
        self.oSF.SetFlag(self.oSF.SELECT,True)
        lEvt=[]
        lWr=[]
        self.ZWAIT_SELECT=1.0
        while self.oSF.IsServing:
            self.__procQueue__()
            if self.thdSockMng.Is2Stop():
                break
            try:
                bDbg=False
                if VERBOSE>10:
                    if self.__isLogDebug__():
                        bDbg=True
                        self.__logDebug__('%s;;lRd:%s;lWr;%s;dConn:%s'%(self.oSF.GetStrFull(';'),
                            self.__logFmt__(self.lRd),self.__logFmt__(lWr),
                            self.__logFmt__(self.dConn)))
            except:
                self.__logTB__()
            try:
                for lRd in self.lRd:
                    try:
                        lRdRdy,lWrRdy , lEvtRdy = select.select(lRd, [] , lEvt, self.ZWAIT_SELECT)
                    except socket.timeout,msg:
                        #print 'timeout'
                        #self.CheckPause()
                        continue
                    except:
                        self.__logTB__()
                        #break
                    if bDbg:
                        self.__logDebug__('rd:%d;%s'%(len(lRdRdy),self.__logFmt__(lRdRdy)))
                    for cli in lRdRdy:
                        if cli == fdSrv:
                            self.qConn.put((self.__accept__,(),{}))
                        else:
                            if cli in self.dConn:
                                if bDbg:
                                    self.__logDebug__('cli:%d;%s'%(cli,sock.__str__()))
                                sock=self.dConn[cli]
                                if sock.doRecv()<0:
                                    self.RemoveSock(sock,cli)
                            else:
                                self.__logError__('fd:%d;lRd:%s;dConn:%s'%(cli,
                                        self.__logFmt__(lRd),
                                        self.__logFmt__(self.dConn)))
            except:
                self.__logTB__()
            try:
                for fdCli,sock in self.dConn.iteritems():
                    sock.DoResp()
                if self.par is not None:
                    self.par.DoResp()
            except:
                self.__logTB__()
            try:
                lWr=[]
                iCount=0
                def selectWr(lWr):
                    lRdRdy,lWrRdy , lEvtRdy = select.select([], lWr , [], 0)#self.ZWAIT_SELECT)
                    for cli in lWrRdy:
                        if cli in self.dConn:
                            sock=self.dConn[cli]
                            if sock.doSend()<0:
                                self.RemoveSock(sock,cli)
                        else:
                            self.__logError__('cli:%d;lRd:%s;dConn:%s'%(cli,
                                            self.__logFmt__(lRd),
                                            self.__logFmt__(self.dConn)))
                for fdCli,sock in self.dConn.iteritems():
                    if sock.Is2Snd():
                        lWr.append(fdCli)
                        iCount+=1
                    if iCount>32:
                        selectWr(lWr)
                        iCount=0
                        lWr=[]
                if iCount>0:
                    selectWr(lWr)
            except:
                self.__logTB__()
        self.CloseAllConnections()
        self._acquire()
        try:
            self.sock.close()
        except:
            self.__logTB__()
        self._release()
        self.WaitAllConnectionsInActive()
        if self.oSF.IsShuttingDown:
            self.oSF.SetState(self.oSF.SHUTDOWN)
        else:
            self.oSF.SetState(self.oSF.CLOSED)
        self.oSF.SetFlag(self.oSF.STOPPING,False)
        self.__logInfo__('stopped on %d'%self.port)
    def doCommunication(self):
        self.oSF.SetFlag(self.oSF.SERVING,True)
        self.oSF.SetFlag(self.oSF.STARTING,False)
        self._acquire()
        self.connections={}
        self._release()
        HOST=None#'localhost'
        self.__logInfo__('Run on port %d'%self.port)
        for res in socket.getaddrinfo(HOST, self.port, socket.AF_UNSPEC, socket.SOCK_STREAM, 0, socket.AI_PASSIVE):
            af, socktype, proto, canonname, sa = res
            try:
                self.sock = socket.socket(af, socktype, proto)
            except socket.error, msg:
                self.sock = None
                continue
            try:
                self.sock.bind(sa)
                self.sock.listen(3)
            except socket.error, msg:
                self.sock.close()
                self.sock = None
                continue
            break
        if self.sock is None:
            self.__logInfo__('socket is None')
            return
        self.sock.settimeout(2.0)
        while self.oSF.IsServing:
            self.__procQueue__()
            if self.thdSockMng.Is2Stop():
                break
            try:
                self.qConn.put((self.__accept__,(),{'bUseSelect4Comm':False}))
                if 0:
                    conn, addr = self.sock.accept()
                    self.__logInfo__('incomming connection;class:%s'%(
                                self.socketClass))
                    self.__logInfo__('Connected by %s'%str(addr))
                    sID=self.GenerateSessionID()
                    self.__logInfo__('sessionID:%s'%(sID))
                    self.__logInfo__('sock inst:%s'%(self.socketClass))
                    sock=self.socketClass(self,conn,addr,sID)
                    self.__logInfo__('sock inst:%s'%(self.socketClass))
            except socket.timeout:
                pass
            except:
                self.__logTB__()
        self.CloseAllConnections()
        self._acquire()
        try:
            self.sock.close()
        except:
            self.__logTB__()
        self._release()
        self.WaitAllConnectionsInActive()
        if self.oSF.IsShuttingDown:
            self.oSF.SetState(self.oSF.SHUTDOWN)
        else:
            self.oSF.SetState(self.oSF.CLOSED)
        self.oSF.SetFlag(self.oSF.STOPPING,False)
        self.__logInfo__('stopped on %d'%self.port)
    def NotifyConnected(self,sock):
        #sock.SendSessionID()
        addr=sock.adr
        sID=sock.sessionID
        self.__logInfo__('sessionID:%s;send'%(sID))
        self.connections[addr]=sock
    def GetConnectionsStr(self):
        s=''
        keys=self.connections.keys()
        def compFunc(a,b):
            i=cmp(a[0],b[0])
            if i==0:
                return a[1]-b[1]
            return i
        keys.sort(compFunc)
        lst=[]
        for k in keys:
            sock=self.connections[k]
            lst.append(k[0]+';'+str(k[1])+';'+str(sock.received)+';'+str(sock.send)+';'+sock.zConn)
        return ','.join(lst)
