#----------------------------------------------------------------------------
# Name:         vtSockMngAppl.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20090817
# CVS-ID:       $Id: vtSockMngAppl.py,v 1.3 2009/08/25 11:18:34 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.vtSched import vtSched
from vidarc.tool.vtThreadCore import vtThreadCore,getRLock

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

class vtSockMngAppl:
    def __init__(self,*args,**kwargs):
        if hasattr(self,'thdSockMng')==False:
            self.thdSockMng=vtThreadCore(bPost=False,verbose=VERBOSE-1,
                    origin=u':'.join([self.GetOrigin(),'mng']))
        self.shdComm=vtSched(kwargs.get('scheduleSize',32))
        if hasattr(self,'semAccess')==False:
            self.semAccess=getRLock()
        
        self.CallBack=self.thdSockMng.CallBack
        self.CallBackDelayed=self.thdSockMng.CallBackDelayed
        self.DoCallBack=self.thdSockMng.DoCallBack
        self.DoCB=self.thdSockMng.DoCB
        
    def acquire(self,blocking=True):
        return self.semAccess.acquire(blocking)
    def release(self):
        self.semAccess.release()
    def DoSafe(self,func,*args,**kwargs):
        try:
            self.acquire()
            return func(*args,**kwargs)
        finally:
            self.release()
    def SchedPut(self,func,*args,**kwargs):
        self.shdComm.put(func,*args,**kwargs)
    def SchedPop(self):
        self.shdComm.pop()
    def DoResp(self):
        try:
            while self.shdComm.pop()>0:
                pass
        except:
            self.__logTB__()
    def __getattr__(self,name):
        if hasattr(self.thdSockMng,name):
            return getattr(self.thdSockMng,name)
        raise AttributeError(name)
    
#class vtSockMngApplLogOrigin(vtLog.vtLogMixIn,vtLog.vtLogOrigin,vtSockMngAppl):
#    def __init__(self,*args,**kwargs):
#        vtLog.vtLogOrigin.__init__(self,**{'origin':kwargs.get('origin',None)})
#        vtSockMngAppl.__init__(self,*args,**kwargs)
