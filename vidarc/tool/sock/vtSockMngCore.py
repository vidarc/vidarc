#----------------------------------------------------------------------------
# Name:         vtSockMngCore.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20090812
# CVS-ID:       $Id: vtSockMngCore.py,v 1.6 2011/03/31 11:25:54 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import time
from Queue import Queue

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.vtThreadCore import vtThreadCore
from vidarc.tool.vtStateFlagSimple import vtStateFlagSimple

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

class vtSockMngCore(vtLog.vtLogMixIn,vtLog.vtLogOrigin):
    ZWAIT_SELECT=0.1
    ZWAIT_SELECT_ACTIVE=0.5
    SELECT_FD_SIZE=500
    def __init__(self,port,verbose=0,*args,**kwargs):
        vtLog.vtLogOrigin.__init__(self,**{'origin':kwargs.get('origin',None)})
        
        if hasattr(self,'thdSockMng')==False:
            self.thdSockMng=vtThreadCore(bPost=False,verbose=verbose-1)
        if hasattr(self,'oSF')==False:
            self.oSF=vtStateFlagSimple({},{})
        try:
            self.oSF.AddStateDict(self.__initStateDict__())
            self.oSF.AddFlagDict(self.__initFlagDict__())
        except:
            self.__logTB__()
        if hasattr(self,'semAccess')==False:
            self.semAccess=self.thdSockMng.getLock()
        
        self.port=port or -1
        
        self.SetPar(None)
        
        self.__initCtrl__(*args,**kwargs)
    def __initCtrl__(self,*args,**kwargs):
        self._iConnectedCount=0
        self.connections={}
        self.lRd=[]
        self.dConn={}
        self.lWr=[]
        self.qConn=Queue()
        
        self.tClsSock=kwargs.get('clsSock',None)
        
        self.tNotify=kwargs.get('notify',None)
    def __del__(self):
        if self.par is not None:
            self.par=None
    def _acquire(self,blocking=True):
        return self.semAccess.acquire(blocking)
    def _release(self):
        self.semAccess.release()
    def __callFuncTup__(self,func,*args,**kwargs):
        try:
            func(*args,**kwargs)
        except:
            self.__logTB__()
    def __initStateDict__(self):
        return {}
    def __initFlagDict__(self):
        return {
                0x0001:{'name':'ACTIVE','is':'IsActive','order':0},
                0x0002:{'name':'PAUSING','is':'IsPausing','order':1},
                0x0004:{'name':'PAUSED','is':'IsPaused','order':2},
                0x0008:{'name':'SERVING','is':'IsServing','order':3},
                0x0020:{'name':'STARTING','is':'IsStarting','order':4},
                #0x0010:{'name':'NOTITY_CONNECT_IMMEDIATLY','is':'IsNotifyConnectImmediatly'},
                0x0040:{'name':'STOPPING','is':'IsStopping','order':5},
                }
    def SetPar(self,par):
        self.par=par
    def SetHostPort(self,host,port):
        self.host=host or 'localhost'
        self.port=port
    def __removeSock__(self,cli):
        self._acquire()
        try:
            self.__logInfo__('remove:%d'%(cli))
            for lRd in self.lRd:
                if cli in lRd:
                    self.__logInfo__('remove from lRd'%())
                    lRd.remove(cli)
                    break
            self.lRd=[lRd for lRd in self.lRd if len(lRd)>0]
            # compress lists
            iL=len(self.lRd)
            for i in xrange(iL-1,0,-1):
                iL1=len(self.lRd[i])
                iL2=len(self.lRd[i-1])
                if (iL1+iL2)<self.SELECT_FD_SIZE:
                    self.lRd[i-1]=self.lRd[i-1]+self.lRd[i]
                    del self.lRd[i]
        except:
            self.__logTB__()
        try:
            if cli in self.dConn:
                del self.dConn[cli]
        except:
            self.__logTB__()
        try:
            if self.__isLogDebug__():
                self.__logDebug__('lRd:%s;dConn:%s'%(vtLog.pformat(self.lRd),vtLog.pformat(self.dConn)))
        except:
            self.__logTB__()
        self._release()
        self.__calcConnected__()
    def removeSock(self,sock,cli):
        self.__logInfo__('close connection'%())
        try:
            sock.SocketClose()
        except:
            self.__logTB__()
    def RemoveSock(self,sock,cli):
        self.__logInfo__('close connection'%())
        self._acquire()
        self.qConn.put((self.__removeSock__,(cli,),{}))
        self.qConn.put((self.removeSock,(sock,cli,),{}))
        self._release()
    def __calcConnected__(self):
        self.__logInfo__(''%())
        self._acquire()
        try:
            self._iConnectedCount=0
            if len(self.lRd)>0:
                self._iConnectedCount=reduce(lambda x,y:x+y,[len(lRd) for lRd in self.lRd])
            if VERBOSE>10:
                self.__logInfo__('lRd;%s;dConn;%s'%(vtLog.pformat(self.lRd),vtLog.pformat(self.dConn)))
        except:
            self.__logTB__()
            self._iConnectedCount=0
        bConnected=self._iConnectedCount!=0
        try:
            if VERBOSE>5:
                self.__logInfo__('connected:%d;count%d'%(bConnected,self._iConnectedCount))
        except:
            self.__logTB__()
        self._release()
        self.oSF.SetFlag(self.oSF.ACTIVE,bConnected)
        if self.thdSockMng.IsRunning()==False:
            self.oSF.SetFlag(self.oSF.SERVING,bConnected)
        if self.thdSockMng.Is2Stop()==True:
            self.oSF.SetFlag(self.oSF.SERVING,bConnected)
    def __addSock__(self,sock):
        try:
            cli=sock.GetSockFD()
            self.__logInfo__('fd:%d'%(cli))
            l=None
            for lRd in self.lRd:
                if len(lRd)<self.SELECT_FD_SIZE:
                    l=lRd
                    break
            if l is None:
                l=[cli]
                self.lRd.append(l)
            else:
                l.append(cli)
            self.dConn[cli]=sock
            if self.__isLogDebug__():
                self.__logDebug__('lRd:%s;dConn:%s'%(vtLog.pformat(self.lRd),vtLog.pformat(self.dConn)))
        except:
            self.__logTB__()
        self.__calcConnected__()
    def SetServing(self,flag):
        self.__logInfo__(''%())
        self.oSF.SetFlag(self.oSF.SERVING,flag)
    def IsServing(self):
        return self.oSF.IsServing
    def CloseAllConnections(self):
        self.__logInfo__(''%())
        self._acquire()
        try:
            l=self.dConn.values()
        except:
            self.__logTB__()
        self._release()
        try:
            for sock in l:
                try:
                    #cli.shutdown()
                    sock.SocketClose()
                except:
                    self.__logTB__()
        except:
            self.__logTB__()
    def WaitAllConnectionsInActive(self):
        try:
            self.__logInfo__(''%())
            zTm=0
            while zTm<60:
                if self.oSF.IsActive==False:
                    break
                zTm+=0.1
                time.sleep(0.25)
                self.__procQueue__()
                self.__calcConnected__()
        except:
            self.__logTB__()
        self.__logInfo__('zTm:%d'%(zTm))
    def __procQueue__(self):
        while self.qConn.empty()==False:
            try:
                f,args,kwargs=self.qConn.get()
                f(*args,**kwargs)
            except:
                self.__logTB__()
    def Notify(self,iTyp,sock):
        try:
            if self.tNotify is not None:
                func,args,kwargs=self.tNotify
                func(iTyp,sock,*args,**kwargs)
        except:
            self.__logTB__()
