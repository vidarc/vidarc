#----------------------------------------------------------------------------
# Name:         vtSockMngClt.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtSockMngClt.py,v 1.5 2009/08/24 03:36:48 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import socket,select,time
import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.vtThreadCore import vtThreadCore
from vidarc.tool.vtStateFlagSimple import vtStateFlagSimple

from vtSockMngCore import vtSockMngCore

class vtSockMngClt(vtSockMngCore):
    ZWAIT_SELECT=0.1
    ZWAIT_SELECT_ACTIVE=0.5
    SELECT_FD_SIZE=500
    def __init__(self,par,host,port,verbose=0,*args,**kwargs):
        vtSockMngCore.__init__(self,port,verbose=verbose,
                *args,**kwargs)
        self.__logInfo__(''%())
        if hasattr(self,'thdAccess')==False:
            self.thdAccess=vtThreadCore(self,False,
                    origin=':'.join([self.GetOrigin(),'thdPoll']))
        
        self.CallBack=self.thdAccess.CallBack
        self.CallBackDelayed=self.thdAccess.CallBackDelayed
        self.DoCallBack=self.thdAccess.DoCallBack
        self.DoCB=self.thdAccess.DoCB
        
        self.host=host or'localhost'
        self.port=port or -1
        
        self.__logInfo__('host:%s;port:%s'%(repr(self.host),repr(self.port)))
        if self.port>0:
            vtLog.vtLogOrigin.__init__(self,''.join(['vtSockClient:','(',repr(self.host),
                        ':',repr(self.port),')']))
        else:
            vtLog.vtLogOrigin.__init__(self,''.join(['vtSockClient:','select']))
        
        self.serving=False
        self.connecting=False
        #self.stopping=False
        self.connected=False
        #self.pausing=False
        self.paused=False
        self._iConnectingCount=0
        #self.dataSettled=False
        self.SetPar(par)
        self.sock=None
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        try:
            del self.sock
        except:
            #vtLog.vtLngTB('del')
            pass
    def __initCtrl__(self,*args,**kwargs):
        vtSockMngCore.__initCtrl__(self,*args,**kwargs)
    def __initStateDict__(self):
        return {
                0x00:{'name':'CLOSED','is':'IsCLosed','init':True,
                        'enter':{#'func':(self.SocketClosed,(),{}),
                                 'clr':['ACTIVE''PAUSING','PAUSED','SERVING','STARTING']}},
                0x01:{'name':'CONNECTING','is':'IsConnecting'},
                0x02:{'name':'CONNECTION','is':'IsConnectionOk'},
                0x04:{'name':'CONNECTED','is':'IsConnected',
                        #'enter':{#'func':(self.NotifyConnected,(),{}),
                        #         'set':['ACTIVE','SERVING'],
                        #         'clr':['PAUSING','PAUSED']}
                                     },
                0x10:{'name':'CONNECTION_OK','is':'IsConnectionOk',
                        #'enter':{'func':(self.NotifyConnectionOk,(),{})}
                            },
                0x11:{'name':'CONNECTION_FLT','is':'IsConnectionFlt',
                        #'enter':{'func':(self.NotifyConnectionFault,(),{})}
                            },
                0xff:{'name':'ABORTED','is':'IsAbortd','init':True,
                        'enter':{#'func':(self.SocketClosed,(),{}),
                                 'clr':['ACTIVE''PAUSING','PAUSED','SERVING','STARTING']}},
                }
    def __initFlagDict__(self):
        return {
                0x0001:{'name':'ACTIVE','is':'IsActive','order':0},
                0x0002:{'name':'PAUSING','is':'IsPausing','order':1},
                0x0004:{'name':'PAUSED','is':'IsPaused','order':2},
                0x0008:{'name':'SERVING','is':'IsServing','order':3},
                0x0020:{'name':'STARTING','is':'IsStarting','order':4},
                0x0010:{'name':'NOTITY_CONNECT_IMMEDIATLY','is':'IsNotifyConnectImmediatly'},
                0x0040:{'name':'STOPPING','is':'IsStopping','order':5},
                0x0080:{'name':'SELECT','is':'IsSelect','order':5},
                }
    def SetHostPort(self,host,port):
        self.host=host or 'localhost'
        self.port=port
    def GetSocketInstance(self):
        return self.sock
    def Start(self):
        self.__logInfo__(''%())
        try:
            if self.IsServing():
                self.__logWarn__('already served')
                return
            if self.oSF.IsStarting:
                self.__logWarn__('already starting')
                return
            if self.oSF.IsStopping:
                self.__logWarn__('stopping active')
                return
            self.thdSockMng.Start()
            self.oSF.SetFlag(self.oSF.STARTING,True)
            self.thdSockMng.Do(self.doCommunication)
        except:
            self.__logTB__()
    def Stop(self):
        self.__logInfo__(''%())
        self.thdSockMng.Stop()
        self.oSF.SetFlag(self.oSF.STOPPING,True)
        try:
            if self.sock is not None:
                self.sock.Stop()
        except:
            self.__logTB__()
        self._acquire()
        try:
            l=self.dConn.values()
        except:
            self.__logTB__()
            l=[]
        self._release()
        for sock in l:
            try:
                #sock.SocketClose()
                sock.Stop()
            except:
                self.__logTB__()
    def Pause(self,flag):
        try:
            self.oSF.SetFlag(self.oSF.PAUSING,flag)
            if self.sock is not None:
                self.sock.Pause(flag)
        except:
            self.__logTB__()
        for sock in self.dConn.itervalues():
            try:
                sock.Pause(Flag)
            except:
                self.__logTB__()
    def IsConnecting(self):
        self._acquire()
        bRet=self._iConnectingCount>0
        self._release()
        return bRet
    def SetConnecting(self,flag):
        self.__logCritical__(''%())
    def IsConnected(self):
        if self.IsConnecting():
            return False
        self._acquire()
        try:
            if self.sock is not None:
                if self.sock.IsConnected():
                    return True
            for sock in self.dConn.itervalues():
                if sock.IsConnected():
                    return True
        finally:
            self._release()
        return False
    def SetConnected(self,flag):
        self.__logCritical__(''%())
    def SetServing(self,flag):
        self.__logInfo__(''%())
        self.oSF.SetFlag(self.oSF.SERVING,flag)
    def IsServing(self):
        return self.oSF.IsServing
    def StopSocket(self,adr,sockInst):
        self._acquire()
        try:
            if self.sock==sockInst:
                self.sock=None
        except:
            self.__logTB__()
        self._release()
        try:
            cli=sockInst.GetSockFD()
            if self.thdSockMng.IsRunning():
                self.qConn.put((self.__removeSock__,(cli,),{}))
            else:
                self.__removeSock__(cli)
        except:
            self.__logTB__()
        try:
            lst=self.connections[adr]
            lst.remove(sockInst)
        except:
            pass
    def ConnectSock(self,host,port,bUseSelect=False,tClsSock=None,funcNotified=None):
        try:
            self.__logInfo__(''%())
            self._acquire()
            self._iConnectingCount+=1
            self._release()
            bThdAct=self.thdSockMng.IsRunning() or self.oSF.IsStarting
            if bUseSelect:
                if bThdAct==False:
                    self.__logError__('thread inactive'%())
                    return -1
            else:
                if bThdAct==True:
                    self.__logError__('thread active'%())
                    return -2
                else:
                    #self.oSF.SetFlag(self.oSF.SERVING,True)
                    pass
            #self.oSF.SetFlag(self.oSF.SELECT,True)
            if tClsSock is None:
                tClsSock=self.tClsSock
            if tClsSock is not None:
                socketClass,args,kwargs=tClsSock
            else:
                self.__logError__('no socket class defined'%())
                return
            sock=socketClass(self,None,None,*args,**kwargs)
            sock.SetParent(self.par)
            sock.Connect(host, port,funcNotified=funcNotified,
                        bUseSelect=bUseSelect)
            self.__logInfo__(''%())
        except:
            self.__logTB__()
    def GetConnectingCount(self):
        try:
            self._acquire()
            return self._iConnectingCount
        finally:
            self._release()
    def NotifyConnected(self,sock):
        self.__logInfo__(''%())
        self._acquire()
        try:
            self._iConnectingCount-=1
            self._iConnectedCount+=1
            self.oSF.SetFlag(self.oSF.ACTIVE,self._iConnectedCount!=0)
            if self._iConnectingCount<0:
                self.__logCritical__('connecting count undeeflow;%d'%(self._iConnectingCount))
        except:
            self.__logTB__()
        self._release()
        if self.thdSockMng.IsRunning():
            self.qConn.put((self.__addSock__,(sock,),{}))
        else:
            self.__addSock__(sock)
        if 'bNotifyConnectImmediatly' in self.socketKwArgs and 0:
            if self.socketKwArgs['bNotifyConnectImmediatly']:
                if self.par is not None:
                    self.par.NotifyConnected()
    def NotifyConnectionFault(self):
        self.__logInfo__(''%())
        self._acquire()
        try:
            self._iConnectingCount-=1
            if self._iConnectingCount<0:
                self.__logCritical__('connecting count underflow;%d'%(self._iConnectingCount))
            if self.oSF.IsSelect==False:
                self.sock=None
                self.oSF.SetFlag(self.oSF.SERVING,False)
        except:
            pass
        self._release()
    def doCommunication(self):
        self.oSF.SetFlag(self.oSF.SERVING,True)
        self.oSF.SetFlag(self.oSF.STARTING,False)
        lEvt=[]
        lWr=[]
        while self.oSF.IsServing:
            self.__procQueue__()
            if self.thdSockMng.Is2Stop():
                #self.oSF.SetFlag(self.oSF.SERVING,False)
                break
            if self.oSF.IsActive==False:
                time.sleep(self.ZWAIT_SELECT_ACTIVE)
                continue
            try:
                for lRd in self.lRd:
                    try:
                        lRdRdy,lWrRdy , lEvtRdy = select.select(lRd, [] , [], self.ZWAIT_SELECT)
                    except socket.timeout,msg:
                        #print 'timeout'
                        #self.CheckPause()
                        continue
                    except:
                        self.__logTB__()
                        continue
                        #break
                    for cli in lRdRdy:
                        if cli in self.dConn:
                            sock=self.dConn[cli]
                            if sock.doRecv()<0:
                                self.__logInfo__('close connection'%())
                                try:
                                    self.__logInfo__('fd:%d;doRecv returned fault'%(cli))
                                    self.__logInfo__('fd:%d;lRd:%s;dConn:%s'%(cli,
                                            vtLog.pformat(lRd),
                                            vtLog.pformat(self.dConn)))
                                except:
                                    self.__logTB__()
                                try:
                                    sock.SocketClose()
                                except:
                                    self.__logTB__()
                        else:
                            self.__logError__('fd:%d;lRd:%s;dConn:%s'%(cli,
                                        vtLog.pformat(lRd),
                                        vtLog.pformat(self.dConn)))
            except:
                self.__logTB__()
            try:
                for fdCli,sock in self.dConn.iteritems():
                    sock.DoResp()
            except:
                self.__logTB__()
            try:
                lWr=[]
                iCount=0
                def selectWr(lWr):
                    lRdRdy,lWrRdy , lEvtRdy = select.select([], lWr , [], 0)#self.ZWAIT_SELECT)
                    for cli in lWrRdy:
                        if cli in self.dConn:
                            sock=self.dConn[cli]
                            if sock.doSend()<0:
                                #self.RemoveSock(sock,cli)
                                pass
                        else:
                            self.__logError__('cli:%d;lRd:%s;dConn:%s'%(cli,
                                            vtLog.pformat(lRd),
                                            vtLog.pformat(self.dConn)))
                for fdCli,sock in self.dConn.iteritems():
                    if sock.Is2Snd():
                        lWr.append(fdCli)
                        iCount+=1
                    if iCount>32:
                        selectWr(lWr)
                        iCount=0
                        lWr=[]
                if iCount>0:
                    selectWr(lWr)
            except:
                self.__logTB__()
            try:
                for fdCli,sock in self.dConn.iteritems():
                    if sock.Is2Stop():
                        sock.SocketClose()
                        #sock.Close()
            except:
                self.__logTB__()
        self.CloseAllConnections()
        self.WaitAllConnectionsInActive()
        self.oSF.SetFlag(self.oSF.STOPPING,False)
        self.__logInfo__(''%())
    def Send(self,str):
        if self.sock is not None:
            self.sock.Send(str)
    def Close(self):
        self.__logInfo__(''%())
        if self.sock is not None:
            self.sock.Close()
        #self.oSF.SetFlag(self.oSF.SERVING,False)
