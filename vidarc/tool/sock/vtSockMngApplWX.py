#----------------------------------------------------------------------------
# Name:         vtSockMngApplWX.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20090820
# CVS-ID:       $Id: vtSockMngApplWX.py,v 1.1 2009/08/24 03:37:09 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.vtThread import vtThread
from vtSockMngAppl import vtSockMngAppl

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

class vtSockMngApplWX(vtSockMngAppl):
    def __init__(self,*args,**kwargs):
        if hasattr(self,'thdSockMng')==False:
            self.thdSockMng=vtThread(None,bPost=False,verbose=VERBOSE,
                    origin=u':'.join([self.GetOrigin(),'mng']))
        vtSockMngAppl.__init__(self,*args,**kwargs)
