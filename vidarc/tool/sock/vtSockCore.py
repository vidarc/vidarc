#----------------------------------------------------------------------------
# Name:         vtSockCore.py
# Purpose:      core socket interface
#               dervied from vtSock.py
#               dervied from vNetSock
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtSockCore.py,v 1.29 2011/03/31 11:25:54 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import time
import socket,types,cStringIO
from select import select
socket.setdefaulttimeout(0.5)

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

from vidarc.tool.vtThreadCore import vtThreadCore
from vidarc.tool.vtStateFlagSimple import vtStateFlagSimple

class vtSockCore(vtThreadCore):
    SEND_ALL=False
    VERBOSE=0
    RECV_SIZE=8192
    MARKER_START='u1u1u1u1'
    MARKER_END='u0u0u0u0'
    def __init__(self,server,conn,adr,sID='',max=33554432,verbose=0,origin=None,*args,**kwargs):  #131072
        vtThreadCore.__init__(self,verbose=verbose,origin=origin)
        self.par=None
        self.srv=None
        
        self.pausing=False
        self.paused=False
        self.max=max
        self.sessionID=sID
        try:
            self.iStartMarker=len(self.MARKER_START)
        except:
            self.iStartMarker=0
        try:
            self.iEndMarker=len(self.MARKER_END)
        except:
            self.iEndMarker=0
        self.iStart=-1
        self.iAct=0
        self.iEnd=-1
        self.fdConn=-1
        self.iDataRcv=0
        self.iDataSnd=0
        self.iTeleSnd=0
        self.iTeleRcv=0
        self.bufSnd=cStringIO.StringIO()
        self.bData2Snd=False
        self.lCmd=None
        self.zConn=time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())
        self.semSend=self.getLock()
        self.oConn=vtStateFlagSimple(
                    {
                    0x00:{'name':'CLOSED','is':'IsClosed','init':True,
                        'enter':{'func':kwargs.get('notifySocketClosed',None),#(self.SocketClosed,(),{}),
                                 'clr':['ACTIVE','PAUSING','PAUSED','SERVING']}},
                    0x01:{'name':'CONNECTING','is':'IsConnecting'},
                    0x02:{'name':'CONNECTION','is':'IsConnectionOk'},
                    0x04:{'name':'CONNECTED','is':'IsConnected',
                        'enter':{'func':(self.NotifyConnected,(),{}),
                                 'set':['ACTIVE','SERVING'],
                                 'clr':['PAUSING','PAUSED']}},
                    0x10:{'name':'CONNECTION_OK','is':'IsConnectionOk',
                        'enter':{'func':(self.NotifyConnectionOk,(),{})}},
                    0x11:{'name':'CONNECTION_FLT','is':'IsConnectionFlt',
                        'enter':{'func':(self.NotifyConnectionFault,(),{})}},
                    0xff:{'name':'ABORTED','is':'IsAbortd','init':True,
                        'enter':{'func':kwargs.get('notifySocketAborted',None),#'func':(self.SocketClosed,(),{}),
                                 'clr':['ACTIVE','PAUSING','PAUSED','SERVING']}},
                    
                    },
                    {
                    0x0001:{'name':'ACTIVE','is':'IsActive','order':0},
                    0x0002:{'name':'PAUSING','is':'IsPausing','order':1},
                    0x0004:{'name':'PAUSED','is':'IsPaused','order':2},
                    0x0008:{'name':'SERVING','is':'IsServing','order':3},
                    0x0010:{'name':'NOTITY_CONNECT_IMMEDIATLY','is':'IsNotifyConnectImmediatly'},
                    
                    })
        self.oParse=vtStateFlagSimple(
                    {
                    0x00:{'name':'UNDEF','is':'IsUndef','init':True},
                    0x01:{'name':'START','is':'IsStart'},
                    0x02:{'name':'END','is':'IsEnd'},
                    0x10:{'name':'TELE_OK','is':'IsTeleOk',
                        'enter':{'func':(self.NotifyTeleOk,(),{})}},
                    0x11:{'name':'TELE_FLT','is':'IsTeleFlt',
                        'enter':{'func':(self.NotifyTeleFault,(),{})}},
                    0x19:{'name':'TELE_UNDEF','is':'IsTeleUnDef',
                        'enter':{'func':(self.NotifyTeleUnDef,(),{})}},
                    },
                    {
                    
                    })
        if kwargs.get('bNotifyConnectImmediatly',None)==True:
            self.oConn.SetFlagByName('NOTITY_CONNECT_IMMEDIATLY')
        self.__initCtrl__(*args,**kwargs)
        if self.iStartMarker>0:
            self.__sendMarkerStart__=self.__sendMarkerStartDo__
        else:
            self.__sendMarkerStart__=self.__sendMarkerStartSkip__
        if self.iEndMarker>0:
            self.__sendMarkerEnd__=self.__sendMarkerEndDo__
        else:
            self.__sendMarkerEnd__=self.__sendMarkerEndSkip__
        self.__initConnection__(server,conn,adr,*args,**kwargs)
        if kwargs.get('bUseSelect4Comm',False)==False:
            self.__logInfo__('use thread'%())
            self.__initStart__()
        else:
            self.__logInfo__('use select'%())
            self.oConn.SetFlagByName('SERVING')
    def __initCtrl__(self,*args,**kwargs):
        pass
    def __initStart__(self):
        if self.conn is not None:
            self.Start()
    def __initConnection__(self,server,conn,adr,*args,**kwargs):
        self.srv=server
        self.conn=conn
        self.adr=adr
        self.__logInfo__('conn:%s;serving:%s;running:%s'%(repr(self.conn),
                self.IsServing(),self.IsRunning()),self.VERBOSE)
        if self.conn is not None:
            if kwargs.get('bUseSelect4Comm',False)==False:
                self.conn.settimeout(0.1)
            else:
                #self.conn.settimeout(0.0)
                self.conn.setblocking(0)
            self.fdConn=self.conn.fileno()
            self.oConn.SetState(self.oConn.CONNECTED)
            self.__logInfo__('conn:%s;serving:%s;running:%s'%(repr(self.conn),
                self.IsServing(),self.IsRunning()),self.VERBOSE)
    def GetMax(self):
        return self.max
    def SetParent(self,par):
        self.par=par
    def GetParent(self):
        return self.par
    def GetSock(self):
        return self.conn
    def GetSockFD(self):
        self.__logInfo__(''%())
        return self.fdConn
    def Do(self,func,*args,**kwargs):
        try:
            self.__logInfo__(';'.join([`func`,`args`,`kwargs`]),self.VERBOSE)
        except:
            self.__logTB__()
        vtThreadCore.Do(self,func,*args,**kwargs)
    def Start(self,bUseSelect=False):
        try:
            if self.conn is None:
                return
            self.__logInfo__('conn:%s;serving:%s;running:%s'%(repr(self.conn),
                    self.IsServing(),self.IsRunning()),self.VERBOSE)
        except:
            self.__logTB__()
            return
        try:
            #if self.IsServing():
            #    return
            if self.IsRunning():
                return
            self.Do(self.doCommunication)
        except:
            self.__logTB__()
    def Connect(self,host,port,funcNotified=None,bUseSelect=False):
        self.__logInfo__('%s;host:%s;port:%s;bUseSelect:%d'%(vtThreadCore.__str__(self),
                        repr(host),repr(port),bUseSelect),self.VERBOSE)
        try:
            if self.oConn.IsNotifyConnectImmediatly and 0:
                self.par.NotifyConnected()
        except:
            pass
        self.Do(self.doConnect,host,port,funcNotified,bUseSelect=bUseSelect)
    def doConnect(self,host,port,funcNotified=None,bUseSelect=False):
        self.__logInfo__('host:%s;port:%s'%(repr(host),repr(port)),self.VERBOSE)
        conn = None
        self.conn = None
        self.oConn.SetState(self.oConn.CONNECTING)
        self.__logInfo__('host:%s;port:%s'%(repr(host),repr(port)),self.VERBOSE)
        try:
            for res in socket.getaddrinfo(host, port, socket.AF_UNSPEC, socket.SOCK_STREAM):
                af, socktype, proto, canonname, sa = res
                try:
                    conn = socket.socket(af, socktype, proto)
                except socket.error, msg:
                    #print msg
                    #traceback.print_exc()
                    conn = None
                    continue
                try:
                    conn.settimeout(1.0)
                    conn.connect(sa)
                except socket.error, msg:
                    conn.close()
                    conn = None
                    continue
                except socket.timeout,msg:
                    self.__logTB__()
                except:
                    self.__logTB__()
                break
        except:
            # address fault
            self.__logTB__()
            pass
        if conn is None:
            #self.SetConnecting(False)
            self.oConn.SetState(self.oConn.CONNECTION_FLT)
            self.__logInfo__(self.oConn.GetStrFull(';'))
            self.__logInfo__('aborted;host:%s;port:%s'%(repr(host),repr(port)),self.VERBOSE)
            try:
                if funcNotified is not None:
                    funcNotified(self,False)
            except:
                self.__logTB__()
            return
        conn.settimeout(0.05)
        #conn.setblocking(0)
        self.conn=conn
        self.fdConn=conn.fileno()
        self.oConn.SetState(self.oConn.CONNECTED)
        self.__logInfo__('established;host:%s;port:%s'%(repr(host),repr(port)),self.VERBOSE)
        #self.SetConnecting(False)
        if bUseSelect==False:
            self.Do(self.doCommunication)
        try:
            if funcNotified is not None:
                funcNotified(self,True)
        except:
            self.__logTB__()
    def IsThreadRunning(self):
        return False
    def IsConnected(self):
        return self.oConn.IsActive
    def IsConnecting(self):
        return self.oConn.IsConnecting
    def IsServing(self):
        return self.oConn.IsServing
    def SetServing(self,flag):
        self.__logInfo__('flag:%d'%(flag))
        self.oConn.SetFlagByName('SERVING',flag)
    def __handle_recv__(self,data):
        iLen=len(data)
        self.iDataRcv+=iLen
    def IsBusy(self):
        try:
            self._acquire()
            return self.__getDataLen__()>0
        finally:
            self._release()
    def doRecv(self):
        if self.IsServing():
            try:
                #if self.__doPause__()==True:
                #    continue
                data=self.conn.recv(self.RECV_SIZE)
                #self.CheckPause()
                if self.verbose>20:
                    if self.srv:
                        sType='srv'
                    else:
                        sType='clt'
                    self.__logDebug__('%s;%s'%(sType,repr(data)))
                if not data:
                    self.SetServing(False)
                    #self.conn=None      # 090603:wro socket closed remote
                else:
                    self.__handle_recv__(data)
                    #if self.IsPaused()==False:
                    while self.HandleData()>0:
                        self.__logDebug__(''%())
                        pass
            except socket.timeout,msg:
                #self.CheckPause()
                #self.__logDebug__('msg:%s'%(repr(msg)))
                pass
            except:
                self.__logTB__(bLogAsDbg=True)
                self.SetServing(False)
                return -2
            return 0
        return -1
    def doCommunication(self):
        self.__logInfo__()
        self.oConn.SetFlagByName('SERVING')
        #self.conn.settimeout(None)
        try:
            #lRd=[self.conn]
            #lWr=[self.conn]
            #lEvt=[]
            while self.IsServing():
                if self.Is2Stop():
                    self.SetServing(False)
                    continue
                #try:
                #    lRdRdy , lWrRdy , lEvtRdy = select(lRd,[],lEvt,0.1)
                #    self.__logDebug__('lRd:%s;lWr:%s;lEvt:%s'%(repr(lRdRdy) , 
                #                repr(lWrRdy) , repr(lEvtRdy)))
                #    if len(lRdRdy)>0:
                #        self.doRecv()
                #    lRdRdy , lWrRdy , lEvtRdy = select([],lWr,[],0.0)
                #    if len(lWrRdy)>0:
                #        self.doSend(bSingle=False)
                #except:
                #    pass
                self.doRecv()
                #self.__logDebug__(''%())
                #self.DoResp()
                self.doSend(bSingle=False)
            #self.SocketClose()
            self.__logInfo__('serving ended')
        except:
            self.__logTB__()
            self.SetServing(False)
        self.SocketClose()
    def SocketClose(self):
        # call to close connection, server object will be notified
        self.__logInfo__(''%())
        self.SetServing(False)
        try:
            self.srv.Notify(5000,self)
        except:
            pass
        try:
            if self.conn is not None:
                self.conn.close()
        except:
            self.__logTB__()
        try:
            if self.srv is not None:
                self.srv.StopSocket(self.adr,self)
            else:
                self.__logWarn__('srv is None')
        except:
            self.__logTB__()
        try:
            self.SocketClosed()
        except:
            self.__logTB__()
        self.srv=None
    def SocketAborted(self):
        self.__logInfo__(''%())
        try:
            self.srv.Notify(9000,self)
        except:
            pass
        self.conn=None
        self.srv=None
        self.oConn.SetState(self.oConn.ABORTED)
    def SocketClosed(self):
        self.__logInfo__(''%())
        self.oConn.SetState(self.oConn.CLOSED)
        try:
            self.srv.Notify(5900,self)
        except:
            pass
        self.conn=None
        self.srv=None
    def HandleData(self):
        #self.__logDebug__(''%())
        bLoop=True
        bHandled=False
        while bLoop:
            self.__findTeleStart__()
            self.__findTeleEnd__()
            if self.oParse.IsEnd:
                bHandled=True
                self.__handleTele__()
                #print self.GetOrigin(),'remaining',self.data[:200]
                #return 1
            else:
                bLoop=False
        if bHandled==True:
            self.__reorganise__()
        return 0
    def __findTeleStart__(self):
        if self.oParse.IsUndef:
            i=self.data.find(self.MARKER_START,self.iAct)
            if i>=0:
                self.iStart=i+self.iStartMarker
                self.iEnd=-1
                self.iAct=self.iStart
                self.oParse.SetState(self.oParse.START)
    def __findTeleEnd__(self):
        if self.oParse.IsStart:
            l=self.__getDataLen__()
            i=self.data.find(self.MARKER_END,self.iAct)
            if i>=0:
                self.iEnd=i
                self.oParse.SetState(self.oParse.END)
            else:
                self.iAct=max(l-self.iEndMarker,self.iStart)
    def __handleTele__(self):
        """do never ever call this method directly, it shall only be
        initiated by parsing state/flag object"""
        val=self.__getTeleData__()
        try:
            if self.lCmds is not None:
                for cmd in self.lCmds:
                    k=cmd[0]
                    iLen=len(k)
                    if val[:iLen]==k:
                        #print 'telegram',k,'found' ,val[:200]
                        #cmd[1](val[iLen:].encode('ISO-8859-1'))
                        self.iTeleRcv+=1
                        try:
                            cmd[1](val[iLen:])
                            self.oParse.SetState(self.oParse.TELE_OK)
                        except:
                            self.__logTB__()
                            self.oParse.SetState(self.oParse.TELE_FLT)
                        self.oParse.SetState(self.oParse.UNDEF)
                        return
                self.oParse.SetState(self.oParse.TELE_UNDEF)
                self.oParse.SetState(self.oParse.UNDEF)
            else:
                self.Send(val)
        except:
            self.__logTB__()
        self.oParse.SetState(self.oParse.UNDEF)
        #self.Send(self.data.getvalue())
        #self.data.clear()
        #self.len=0
    def Send(self,data,bRaw=False):
        if self.verbose>10:
            self.__logDebug__('start'%())
        self.semSend.acquire()
        if self.verbose>10:
            self.__logDebug__('acquired'%())
        #self._acquireSnd()
        if self.verbose>1:
            self.__logDebug__('%s;raw:%d'%(data,bRaw))
        try:
            self.__send__(data,bRaw)
        except:
            self.__logTB__()
        self.semSend.release()
        if self.verbose>10:
            self.__logDebug__('done'%())
        #self._releaseSnd()
    def __send__(self,data,bRaw=False):
        try:
            if bRaw==False:
                if type(data)==types.UnicodeType:
                    data=data.encode('utf-8')
            self.bufSnd.seek(0,2)
            self.bufSnd.write(data)
            self.bData2Snd=True
        except:
            self.__logTB__()
    def Is2Snd(self):
        if self.semSend.acquire(0):
            #self._acquireSnd()
            try:
                return self.bData2Snd
            finally:
                self.semSend.release()
                #self._releaseSnd()
        else:
            return False
    def doSend(self,bSingle=True):
        iRet=-1
        if self.IsServing():
            if self.verbose>10:
                self.__logDebug__(''%())
                bDbg=True
            else:
                bDbg=False
            if bDbg:
                self.__logDebug__('start;bSingle:%d'%(bSingle))
            self.semSend.acquire()
            #self._acquireSnd()
            try:
                if self.bData2Snd:
                    data=self.bufSnd.getvalue()
                    iLen=len(data)
                    if bDbg:
                        self.__logDebug__('iDataSnd:%d;iLen:%d;data:%s'%(self.iDataSnd,
                                    iLen,data))
                    iSend=0
                    if self.SEND_ALL:
                        self.conn.sendall(data)
                        self.iDataSnd+=iLen
                        iSend=iLen
                    else:
                        iRetries=0
                        iCur=0
                        while iSend<iLen:
                            iCur=-1
                            try:
                                iCur=self.conn.send(data[iSend:iLen])
                                self.iDataSnd+=iCur
                                if bDbg:
                                    self.__logDebug__('iCur:%d;iSend:%d;iDataSnd:%d'%(iCur,iSend,self.iDataSnd))
                                iSend+=iCur
                            except socket.timeout,msg:
                                self.__logWarn__('timeout'%())
                                iCur=0
                            except:
                                self.__logTB__()
                                self.__logDebug__('iCur:%d;iSend:%d;iDataSnd:%d'%(iCur,iSend,self.iDataSnd))
                                iCur=0
                            if bSingle:
                                break
                            if iRetries>3:
                                break
                            if iSend<iLen:
                                time.sleep(0.5)
                                iRetries+=1
                    self.bufSnd.seek(0)
                    self.bufSnd.truncate()
                    if iSend<iLen:
                        self.bufSnd.write(data[iSend:])
                        self.bData2Snd=True
                    else:
                        self.bData2Snd=False
                    iRet=0
            except socket.error,msg:
                #self.__logTB__()
                #self.SetServed(False)
                #iRet=-2
                pass
            except:
                if self.verbose>5:
                    self.__logTB__()
                self.oConn.SetServed(False)
                iRet=-2
            self.semSend.release()
            if bDbg:
                self.__logDebug__('done;bSingle:%d'%(bSingle))
            #self._release()
        return iRet
    def __sendMarkerStartDo__(self):
        self.__send__(self.MARKER_START)
    def __sendMarkerStartSkip__(self):
            pass
    def __sendMarkerEndDo__(self):
        self.__send__(self.MARKER_END)
    def __sendMarkerEndSkip__(self):
        pass
    #def _acquireSnd(self):
        #self.__logDebug__(''%())
    #    self.semSend.acquire()
    #def _releaseSnd(self):
    #    self.semSend.release()
        #self.__logDebug__(''%())
    def SendTelegram(self,data):
        try:
            self.iTeleSnd+=1
            #self.__logStack__()
            if self.verbose>10:
                self.__logDebug__(''%())
            self.semSend.acquire()
            if self.verbose>10:
                self.__logDebug__('acquired'%())
            if self.verbose>1 or VERBOSE>0:
                self.__logDebug__('%s'%(data))
            #self.__logDebug__(''%())
            #self._acquireSnd()
            
            #self.__sendMarkerStart__()
            #self.__send__(data)
            #self.__sendMarkerEnd__()
            #if self.iStartMarker>0:
            #    self.bufSnd.write(self.MARKER_START)
            #self.bufSnd.write(data)
            #if self.iEndMarker>0:
            #    self.bufSnd.write(self.MARKER_END)
            if self.iStartMarker>0:
                self.__send__(self.MARKER_START)
            self.__send__(data)
            if self.iEndMarker>0:
                self.__send__(self.MARKER_END)
            #try:
            #    self.__send__(self.bufSnd.getvalue())
            #except:
            #    self.__logTB__()
        except:
            self.__logTB__()
        self.semSend.release()
        if self.verbose>10:
            self.__logDebug__('done'%())
        #self._releaseSnd()
    def SendSessionID(self):
        self.__logInfo__()
        self.SendTelegram(''.join(['sessionID|',self.sessionID]))
    def Close(self):
        # call when connection is lost by any reason
        self.__logInfo__(''%())
        self.SetServing(False)
        try:
            if self.conn is not None:
                self.conn.close()                       # 090601:wro
        except:
            self.__logTB__()
    def GetDataSend(self):
        return self.iDataSnd
    def GetDataRecv(self):
        return self.iDataRcv
    def GetDataTraffic(self):
        return self.iDataRcv+self.iDataSnd
    def GetTeleSend(self):
        return self.iTeleSnd
    def GetTeleRecv(self):
        return self.iTeleRcv
    def GetTeleTraffic(self):
        return self.iTeleRcv+self.iTeleSnd
    
    # notify methods
    def NotifyTeleOk(self):
        pass
    def NotifyTeleFault(self):
        pass
    def NotifyTeleUnDef(self):
        pass
    
    def NotifyConnected(self):
        try:
            #self.__logError__('')
            #print self.oConn
            #print self.oConn.IsNotifyConnectImmediatly
            #print self.par
            if self.oConn.IsNotifyConnectImmediatly==True:
                self.par.NotifyConnected()
            #self.par.NotifyConnected()
            pass
        except:
            pass
        try:
            self.srv.NotifyConnected(self)
        except:
            pass
            #self.__logTB__()
        try:
            self.srv.Notify(0,self)
        except:
            pass
        try:
            #self.Do(self.doCommunication)
            #self.__initStart__()
            pass
        except:
            self.__logTB__()
    def NotifyConnectionOk(self):
        try:
            self.srv.NotifyConnectionOk()
        except:
            pass
        try:
            self.srv.Notify(1500,self)
        except:
            pass
        try:
            self.par.NotifyConnectionOk()
        except:
            pass
    def NotifyConnectionFault(self):
        self.__logDebug__()
        try:
            self.srv.NotifyConnectionFault()
        except:
            pass
        try:
            self.srv.Notify(-1,self)
        except:
            pass
        try:
            self.par.NotifyConnectionFault()
        except:
            pass
            #self.__logTB__()
    def GetNow(self):
        return time.time()
    def DoResp(self):
        pass
    def nop(self,*args,**kwargs):
        pass
