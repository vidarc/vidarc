#----------------------------------------------------------------------------
# Name:         vtSockBuf.py
# Purpose:      socket interface uses vtBuffer 
#               dervied from vtSock.py
#               dervied from vNetSock
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtSockBufSSL.py,v 1.2 2008/03/05 12:07:44 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.vtBuffer import vtBuffer
from vtSockCoreSSL import vtSockCoreSSL

class vtSockBufSSL(vtSockCoreSSL):
    def __initCtrl__(self,*args,**kwargs):
        vtSockCoreSSL.__initCtrl__(self,*args,**kwargs)
        max=self.GetMax()
        self.data=vtBuffer(max=max,reorganise=max/4)
    def __handle_recv__(self,data):
        iLen=len(data)
        self.iDataRcv+=iLen
        self.data.push(data)
    def __getData__(self,iCount=-1):
        return self.data.pop(iCount)
    def __getDataLen__(self):
        return self.data.getDataLen()
    def __getTeleData__(self):
        if self.oParse.IsEnd:
            self.data.pop(self.iStart)
            val=self.data.pop(self.iEnd-self.iStart)
            self.data.pop(self.iEndMarker)
            self.iAct=0
            self.iStart=self.iEnd=-1
            return val
        return None
    def __reorganise__(self):
        pass
