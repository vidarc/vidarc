#----------------------------------------------------------------------------
# Name:         vtSockStrSSL.py
# Purpose:      socket inferface uses vtBuffer
#               dervied from vtSock.py
#               dervied from vNetSock
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtSockStrSSL.py,v 1.2 2008/03/05 12:07:45 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vtSockCoreSSL import vtSockCoreSSL

class vtSockStrSSL(vtSockCoreSSL):
    def __initCtrl__(self,*args,**kwargs):
        vtSockCoreSSL.__initCtrl__(self,*args,**kwargs)
        self.data=u''
        self.len=0
        self.iPos=0
    def __handle_recv__(self,data):
        iLen=len(data)
        self.iDataRcv+=iLen
        self.len+=iLen
        self.data+=data#.decode('utf-8')#.decode('ISO-8859-1')
        #self.data+=data.decode('ISO-8859-1')
        #print self.len,self.max
    def __getData__(self,iCount=-1):
        if iCount<0:
            s=self.data
            self.data=''
            self.len=0
        else:
            s=self.data[:iCount]
            self.data=self.data[iCount:]
            self.len-=iCount
        return s
    def __getDataLen__(self):
        return self.len
    def __getTeleData__(self):
        if self.oParse.IsEnd:
            val=self.data[self.iStart:self.iEnd]
            self.iEnd+=self.iEndMarker
            self.iAct=self.iEnd
            self.iStart=self.iEnd=-1
            return val
        return None
    def __reorganise__(self):
        if self.iAct>=self.len:
            self.data=''
            self.len=0
            self.iAct=0
        else:
            if self.iStart>0:
                self.data=self.data[self.iStart:]
                self.len-=self.iStart
                self.iStart=0
                self.iAct=0
