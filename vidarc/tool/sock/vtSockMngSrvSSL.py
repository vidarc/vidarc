#----------------------------------------------------------------------------
# Name:         vtSockMngSrvSSL.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20090812
# CVS-ID:       $Id: vtSockMngSrvSSL.py,v 1.1 2009/08/13 09:59:20 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vtSockMngSrv import vtSockMngSrv

class vtSockMngSrvSSL(vtSockMngSrv):
    def __accept__(self):
        try:
            
            sock, addr = self.socket.accept()
            #conn = tsafe.Connection(connection=sock)
            conn = sock
            vtLog.vtLngCur(vtLog.INFO,'incomming connection;class:%s'%(
                        self.socketClass),self.GetOrigin())
            vtLog.vtLngCur(vtLog.INFO,'Connected by %s'%str(addr),
                        self.GetOrigin())
            sID=self.GenerateSessionID()
            vtLog.vtLngCur(vtLog.INFO,'sessionID:%s'%(sID),self.GetOrigin())
            vtLog.vtLngCur(vtLog.INFO,'sock inst:%s'%(self.socketClass),self.GetOrigin())
            sock=self.socketClass(self,conn,addr,sID,
                                bUseSelect4Comm=True)
            fd=conn.fileno()
            self.dConn[fd]=sock
            
            self.__addSock__(sock)
        except socket.timeout:
            pass
        except (SSL.WantReadError, SSL.WantWriteError, SSL.WantX509LookupError):
            vtLog.vtLngTB(self.GetOrigin())
            pass
        except SSL.ZeroReturnError:
            vtLog.vtLngTB(self.GetOrigin())
        except SSL.Error, errors:
            vtLog.vtLngTB(self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def RunSelect(self):
        vtLog.vtLngCur(vtLog.DEBUG,'',self.GetOrigin())
        self._acquire()
        self.serving=True
        self.connections={}
        self._release()
        HOST=None#'localhost'
        vtLog.vtLngCur(vtLog.INFO,'Run on port %d'%self.port,self.GetOrigin())
        for res in socket.getaddrinfo(HOST, self.port, socket.AF_UNSPEC, socket.SOCK_STREAM, 0, socket.AI_PASSIVE):
            #vtLog.vtLngCurCls(vtLog.INFO,repr(res),self)
            af, socktype, proto, canonname, sa = res
            try:
                self.socket = tsafe.Connection(self.ctx, socket.socket(af, socktype, proto))
            except socket.error, msg:
                self.socket = None
                continue
            try:
                self.socket.bind(sa)
                self.socket.listen(3)
            except socket.error, msg:
                self.socket.close()
                self.socket = None
                continue
            break
            #if s is None:
            #    print 'could not open socket'
                #sys.exit(1)
            #print s
        if self.socket is None:
            self.serving=False
            vtLog.vtLngCur(vtLog.INFO,'socket is None',self.GetOrigin())
            return
        #self.socket.setblocking(2.0)
        #self.socket.setblocking(0.0)
        self.socket.settimeout(0.5)
        self._acquire()
        fdSrv=self.socket.fileno()
        self.lRd=[[fdSrv]]
        self.dConn={} #{fdSrv:self}
        srv=self.socket
        self.lWr=[]
        self._release()
        lEvt=[]
        lWr=[]
        while self.IsServing():
            while self.qConn.empty()==False:
                try:
                    f,args,kwargs=self.qConn.get()
                    f(*args,**kwargs)
                except:
                    vtLog.vtLngTB(self.GetOrigin())
            if self.Is2Stop():
                break
            try:
                for lRd in self.lRd:
                    try:
                        #lRdRdy,lWrRdy , lEvtRdy = select.select(lRd, lWr , lEvt, self.ZWAIT_SELECT)
                        lRdRdy,lWrRdy , lEvtRdy = select.select(lRd, [] , lEvt, self.ZWAIT_SELECT)
                    except socket.timeout,msg:
                        vtLog.vtLngCur(vtLog.DEBUG,''%(),self.GetOrigin())
                        #print 'timeout'
                        #self.CheckPause()
                        continue
                    except:
                        vtLog.vtLngTB(self.GetOrigin())
                        #break
                    #vtLog.vtLngCur(vtLog.DEBUG,'rd:%d;wr:%d'%(len(lRdRdy),len(lWrRdy)),self.GetOrigin())
                    for cli in lRdRdy:
                        #if cli == srv:
                        if cli == fdSrv:
                            self.qConn.put((self.__acceptSSL__,(),{}))
                        else:
                            sock=self.dConn[cli]
                            #print 'recv',cli,sock,sock.IsServing()
                            if sock.doRecv()<0:
                                self.RemoveSock(sock,cli)
            except:
                vtLog.vtLngTB(self.GetOrigin())
            try:
                lWr=[]
                iCount=0
                def selectWr(lWr):
                    lRdRdy,lWrRdy , lEvtRdy = select.select([], lWr , [], self.ZWAIT_SELECT)
                    for cli in lWrRdy:
                        sock=self.dConn[cli]
                        #print 'recv',cli,sock,sock.IsServing()
                        if sock.doSend()<0:
                            self.RemoveSock(sock,cli)
                for fdCli,sock in self.dConn.iteritems():
                    if sock.Is2Snd():
                        lWr.append(fdCli)
                        iCount+=1
                    if iCount>32:
                        selectWr(lWr)
                        iCount=0
                        lWr=[]
                if iCount>0:
                    selectWr(lWr)
            except:
                vtLog.vtLngTB(self.GetOrigin())
        self._acquire()
        try:
            #del self.dConn[srv]
            #del self.dConn[fdSrv]
            l=self.dConn.values()
        except:
            vtLog.vtLngTB(self.GetOrigin())
            l=None
        self._release()
        try:
            for sock in l:
            #for cli,sock in self.dConn.iteritems():
                try:
                    #cli.shutdown()
                    sock.SocketClose()
                except:
                    vtLog.vtLngTB(self.GetOrigin())
            self.lRd=[]
            self.dConn={}
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self._acquire()
        try:
            self.serving=False
            self.socket.close()
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self._release()
        vtLog.vtLngCur(vtLog.INFO,'stopped on %d'%self.port,self.GetOrigin())
    def Run(self):
        self._acquire()
        self.serving=True
        self.connections={}
        self._release()
        HOST=None#'localhost'
        vtLog.vtLngCur(vtLog.INFO,'Run on port %d'%self.port,self.GetOrigin())
        for res in socket.getaddrinfo(HOST, self.port, socket.AF_UNSPEC, socket.SOCK_STREAM, 0, socket.AI_PASSIVE):
            #vtLog.vtLngCurCls(vtLog.INFO,repr(res),self)
            af, socktype, proto, canonname, sa = res
            try:
                #self.socket = socket.socket(af, socktype, proto)
                self.socket = tsafe.Connection(self.ctx, socket.socket(af, socktype, proto))
                #self.socket.set_app_data(self)
                #self.socket.bind(('', self.port))
                #self.socket.listen(3) 
                #self.socket.settimeout(1)
                #self.socket.setblocking(0)
                #self.socket.setblocking(1)
            
            except socket.error, msg:
                self.socket = None
                continue
            try:
                self.socket.bind(sa)
                self.socket.listen(3)
                self.socket.settimeout(1)
                #self.socket.setblocking(1)
            except socket.error, msg:
                self.socket.close()
                self.socket = None
                continue
            break
            #if s is None:
            #    print 'could not open socket'
                #sys.exit(1)
            #print s
        if self.socket is None:
            self.serving=False
            return
        self.socket.settimeout(2.0)
        #self.socket.setblocking(1)
        while self.IsServing():
            if self.Is2Stop():
                break
            #if self.stopping:
            #    break
            #vtLog.vtLngCurCls(vtLog.INFO,'wait for connection',self)
            try:
                #tup=select.select([self.socket],[],[])
                #print tup
                #self.socket.setblocking(1)
                #conn, addr = self.socket.accept()
                sock, addr = self.socket.accept()
                conn = tsafe.Connection(connection=sock)
                #print sock
                #conn = tsafe.Connection(self.ctx, sock)
                #conn=sock
                #conn.set_app_data(self)
                #print conn
                vtLog.vtLngCur(vtLog.INFO,'incomming connection;class:%s'%(
                        self.socketClass),self.GetOrigin())
                #print s,'accept'
                vtLog.vtLngCur(vtLog.INFO,'Connected by %s'%str(addr),
                        self.GetOrigin())
                #if self.SSL:
                #    import os
                #    print os.getcwd()
                #    conn=socket.ssl(conn,'key.pem','cert.pem')
                #    print conn
                #self.semVerify.acquire()
                #print 'runSSL','ver',1
                sID=self.GenerateSessionID()
                vtLog.vtLngCur(vtLog.INFO,'sessionID:%s'%(sID),self.GetOrigin())
                vtLog.vtLngCur(vtLog.INFO,'sock inst:%s'%(self.socketClass),self.GetOrigin())
                sock=self.socketClass(self,conn,addr,sID)
                #sock=self.socketClass(self,None,addr,sID)
                #ctx=conn.get_context()
                #ctx.set_app_data(sock)
                #time.sleep(5.5)
                
                #conn.set_app_data(self)
                #self.semVerify.acquire()
                #while sock.iVerify==0:
                    #time.sleep(0.1)
                    #time.sleep(1)
                #    print sock.iVerify
                #    print conn.state_string()
                    
                #print conn.state_string()
                #print 'runSSL','ver',2
                #self.semVerify.release()
                #print 'runSSL','ver',3
                #vtLog.vtLngCur(vtLog.INFO,'sock inst:%s'%(self.socketClass),self.GetOrigin())
                #sock.SendSessionID()
                #vtLog.vtLngCur(vtLog.INFO,'sessionID:%s;send'%(sID),self.GetOrigin())
                #self.connections[addr]=sock
            except socket.timeout:
                pass
            except (SSL.WantReadError, SSL.WantWriteError, SSL.WantX509LookupError):
                vtLog.vtLngTB(self.GetOrigin())
                pass
            except SSL.ZeroReturnError:
                #dropClient(cli)
                #print 'SSL.ZeroReturnError'
                vtLog.vtLngTB(self.GetOrigin())
                #self.conn.shutdown()
                #self.serving=False
            except SSL.Error, errors:
                #dropClient(cli, errors)
                #print 'SSL.Error',errors
                vtLog.vtLngTB(self.GetOrigin())
                #traceback.print_exc()
                #self.serving=False
            except:
                vtLog.vtLngTB(self.GetOrigin())
        #print self.port
        self._acquire()
        try:
            self.serving=False
            self.socket.close()
        except:
            vtLog.vtLngTB(self.GetOrigin())
        self._release()
        vtLog.vtLngCur(vtLog.INFO,'stopped on %d'%self.port,self.GetOrigin())
