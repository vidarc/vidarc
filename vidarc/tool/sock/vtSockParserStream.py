#----------------------------------------------------------------------------
# Name:         vtSockParserStream.py
# Purpose:      socket interface uses vtParserStream
#               dervied from vtSock.py
#               dervied from vNetSock
# Author:       Walter Obweger
#
# Created:      20090424
# CVS-ID:       $Id: vtSockParserStream.py,v 1.1 2009/04/25 00:11:02 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.vtParserStream import vtParserStream
from vtSockCore import vtSockCore

class vtSockParserStream(vtSockCore):
    def __initCtrl__(self,*args,**kwargs):
        vtSockCore.__initCtrl__(self,*args,**kwargs)
        max=self.GetMax()
        self.oParse=vtParserStream(origin='.'.join([self.GetOrigin(),'parser']),
                    max=max,verbose=self.verbose-1)
        self.data=None#vtBuffer(max=max,reorganise=max/4)
        # process lCmds
    def __handle_recv__(self,data):
        iLen=len(data)
        self.iDataRcv+=iLen
        #self.data.push(data)
        #print data
        self.oParse.Parse(data)
    def __getData__(self,iCount=-1):
        self.__logCritical__(''%())
        return ''
        return self.data.pop(iCount)
    def __getDataLen__(self):
        #self.__logCritical__(''%())
        return 0
        return self.data.getDataLen()
    def __getTeleData__(self):
        if self.oParse.IsEnd:
            self.data.pop(self.iStart)
            val=self.data.pop(self.iEnd-self.iStart)
            self.data.pop(self.iEndMarker)
            self.iAct=0
            self.iStart=self.iEnd=-1
            return val
        return None
    def __reorganise__(self):
        pass
    def HandleData(self):
        return 0
