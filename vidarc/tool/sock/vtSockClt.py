#----------------------------------------------------------------------------
# Name:         vtSockClt.py
# Purpose:      
#               
# Author:       Walter Obweger
#
# Created:      20090516
# CVS-ID:       $Id: vtSockClt.py,v 1.4 2009/06/04 03:07:55 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from  vidarc.tool.vtThreadCore import vtThreadCore

class vtSockClt:
    def __init__(self,netClt=None,netSrvHost=None,netSrvPort=None):
        self.__logInfo__(''%())
        self.cltSock=None
        self._cltNet=netClt
        self._host=netSrvHost
        self._port=netSrvPort
        if hasattr(self,'thdAccess')==False:
            self.thdAccess=vtThreadCore(self,False,
                    origin=':'.join([self.GetOrigin(),'thdPoll']))
        if hasattr(self,'semAccess')==False:
            self.semAccess=self.thdAccess.getLock()
        self.CallBack=self.thdAccess.CallBack
        self.CallBackDelayed=self.thdAccess.CallBackDelayed
        self.DoCallBack=self.thdAccess.DoCallBack
        self.DoCB=self.thdAccess.DoCB
    def _acquire(self,blocking=True):
        return self.semAccess.acquire(blocking)
    def _release(self):
        self.semAccess.release()
    def SetNetClt(self,oClt):
        try:
            if oClt is not None:
                if self._cltNet is not None:
                    self.__logError__('network client already set'%())
                    return -2
            else:
                if self._cltNet.IsServing():
                    self.__logCritical__('network client is serving;%s'%(self._cltNet))
                
            self._cltNet=oClt
            self.__logDebug__('%s'%(self._cltNet))
            return 0
        except:
            self.__logTB__()
        return -1
    def GetNetClt(self):
        return self._cltNet
    def GetClt(self):
        return self.cltSock
    def SetSrvAdr(self,host,port):
        self._host=host
        self._port=port
    def __storeClt__(self,sock,bFlag):
        self.__logInfo__('bFlag:%d;start'%(bFlag))
        self._acquire()
        try:
            if bFlag:
                self.cltSock=sock
            else:
                self.cltSock=None
        except:
            self.__logTB__()
        self._release()
        if bFlag:
            self.NotifyConnected()
        else:
            self.NotifyConnectionFault()
        self.__logInfo__('bFlag:%d;done'%(bFlag))
    def Connect4Select(self,clt,host,port,clsSock,**kwargs):
        try:
            if clt is None:
                clt=self._cltNet
            if host is None:
                host=self._host
            if host is None:
                self.__logError__('host not definied'%())
            if port is None:
                port=self._port
            if port is None:
                self.__logError__('port not defined'%())
            clt.Connect4Select(host,port,clsSock,self.__storeClt__,**kwargs)
        except:
            self.__logTB__()
    ConnectSock=Connect4Select
    def NotifyConnected(self):
        self.__logInfo__(''%())
    def NotifyConnectionFault(self):
        self.__logError__(''%())
    def ShuttingDown(self):
        self.__logInfo__(''%())
    def NotifyShuttingDown(self):
        self.__logInfo__(''%())
    def __shutdown__(self):
        self.__logInfo__(''%())
    def NotifyShutDown(self):
        self.__logInfo__(''%())
    def DisConnectSock(self):
        try:
            if self.cltSock is not None:
                #self.cltSock.Close()
                self.cltSock.Stop()
        except:
            self.__logTB__()

