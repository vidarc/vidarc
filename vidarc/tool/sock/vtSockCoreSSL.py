#----------------------------------------------------------------------------
# Name:         vtSockCoreSSL.py
# Purpose:      
#               dervied from vNetSock
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtSockCoreSSL.py,v 1.7 2009/04/25 17:53:16 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os
import socket,types,time
import traceback

from OpenSSL import SSL,tsafe
from OpenSSL import crypto
import certgen 

import __init__

def hasSSL():
    return __init__.SSL

from vtSockCore import vtSockCore
import vSystem

VERBOSE_CERT=0
if VERBOSE_CERT:
    import sys

#from OpenSSL import tsafe
def verify_cb(conn, cert, errnum, depth, ok):
    #traceback.print_stack()
    #print 'conn:%s;cert:%s;errnum:%s,depth:%s;ok:%s'%(repr(conn),
    #                repr(cert),repr(errnum),repr(depth),repr(ok))
    # This obviously has to be updated
    #vtLog.vtLngCurCls(vtLog.INFO,'Got certificate: %s' % cert.get_subject(),self)
    #vtLog.vtLngCurCls(vtLog.ERROR,'errnum:%s;depth:%s;ok:%s'%(`errnum`,`depth`,`ok`),self)
    ctx=conn.get_context()
    self=ctx.get_app_data()
    #print 'Got certificate: %s' % cert.get_subject()
    #print 'errnum:%s;depth:%s;ok:%s'%(`errnum`,`depth`,`ok`)
    #if ok:
    #    self.iVerify=1
    #else:
    #    self.iVerify=-1
    self.DoVerify(conn,cert,errnum,depth,ok)
    return ok

def getContextSSL(privkey=None,cert=None,verify=None,DN=None,applData=None):
    if DN is None:
        DN=vSystem.getUsrCfgDN('VIDARC')
        #DN='c:/apps/vidarc/'#vSystem.getPlugInDN()
    if privkey is None:
        privkey='vMESClt.pkey'
    if cert is None:
        cert='vMESClt.cert'
    if verify is None:
        verify='CA.cert'
    #try:
    #    DNcert=vSystem.getUsrCfgDN()
    #except:
    #    DNcert=DN
    DNcert=DN
    if VERBOSE_CERT:
        sys.stderr.write('\n%s'%(''.join(traceback.format_stack())))
        sys.stderr.write('\nappl:%s\ndnCert:%s\ndn:%s %d\n'%(sys.argv[0],DNcert,os.path.join(DN, privkey),os.path.exists(os.path.join(DN, privkey))))
        sys.stderr.write('       nfn:%s %d\n'%(os.path.join(DN, privkey),os.path.exists(os.path.join(DN, privkey))))
        sys.stderr.write('       nfn:%s %d\n'%(os.path.join(DN, cert),os.path.exists(os.path.join(DN, cert))))
        sys.stderr.write('       nfn:%s %d\n'%(os.path.join(DNcert, verify),os.path.exists(os.path.join(DNcert, verify))))
    if os.path.exists(os.path.join(DN, privkey))==False:
        certgen.mkClientCert(DN,vSystem.getHostFull(),
                    [('vMESClt','Client')])
    ctx = SSL.Context(SSL.SSLv23_METHOD)
    ctx.set_options(SSL.OP_NO_SSLv2)
    if applData is not None:
        ctx.set_app_data(applData)
    ctx.set_verify(SSL.VERIFY_PEER|SSL.VERIFY_FAIL_IF_NO_PEER_CERT, verify_cb) # Demand a certificate
    #print DN,privkey
    ctx.use_privatekey_file (os.path.join(DN, privkey))
    ctx.use_certificate_file(os.path.join(DN, cert))
    ctx.load_verify_locations(os.path.join(DNcert, verify))
    return ctx

class vtSockCoreSSL(vtSockCore):
    def __initCtrl__(self,*args,**kwargs):
        self.HANDSHAKE_TIMEOUT=5
        self.iVerify=0
        #self.ctx=self.conn.get_context()
        #self.conn.set_app_data(self)
    def __initConnection__(self,server,conn,adr,*args,**kwargs):
        self.srv=server
        self.conn=conn
        self.adr=adr
        if kwargs.get('bUseSelect4Comm',False)==True:
            self.Start(bUseSelect=True)
    def DoVerify(self,conn, cert, errnum, depth, ok):
        try:
            self.__logInfo__('conn:%s;cert:%s;errnum:%s,depth:%s;ok:%s'%(repr(conn),
                    repr(cert),repr(errnum),repr(depth),repr(ok)))
            self.__logInfo__('Got certificate: %s' % cert.get_subject())
        except:
            self.__logTB__()
        #self.__logStack__()
        if ok:
            pass
        else:
            pass
    def Start(self,bUseSelect=False):
        if self.conn is None:
            return
        try:
            #if self.IsServing():
            #    return
            if self.IsRunning():
                return
            #self.SetServing(False)
            ctx=self.conn.get_context()
            ctx.set_app_data(self)
            self.oConn.SetState(self.oConn.CONNECTING)
            #self.Do(self.doCommunication)
            self.Do(self.__do_handshake__,True,bUseSelect=bUseSelect)
        except:
            self.__logTB__()
    #def doConnect(self,host,port):
    def doConnect(self,host,port,funcNotified=None,bUseSelect=False):
        conn = None
        self.conn = None
        self.oConn.SetState(self.oConn.CONNECTING)
        self.__logInfo__('host:%s;port:%s'%(repr(host),repr(port)))
        try:
            for res in socket.getaddrinfo(host, port, socket.AF_UNSPEC, socket.SOCK_STREAM):
                af, socktype, proto, canonname, sa = res
                try:
                    self.ctx=getContextSSL(applData=None)
                    sck = socket.socket(af, socktype, proto)
                    sck.settimeout(0.1)
                    #sck.setblocking(0)
                    conn = tsafe.Connection(self.ctx,sck)
                    #conn.setblocking(0)
                    ctx=conn.get_context()
                    ctx.set_app_data(self)
                except socket.error, msg:
                    self.__logTB__()
                    conn = None
                    continue
                try:
                    sck.settimeout(0.1)
                    conn.connect(sa)
                except socket.error, msg:
                    conn.close()
                    conn = None
                    continue
                break
        except:
            # address fault
            self.__logTB__()
            pass
        if conn is None:
            #self.SetConnecting(False)
            self.oConn.SetState(self.oConn.CONNECTION_FLT)
            self.__logInfo__('aborted;host:%s;port:%s'%(repr(host),repr(port)))
            try:
                if funcNotified is not None:
                    funcNotified(self,False)
            except:
                self.__logTB__()
            return
        #conn.settimeout(0.1)
        #conn.setblocking(0)
        self.__logInfo__('established;host:%s;port:%s'%(repr(host),repr(port)))
        self.conn=conn
        try:
            self.conn.do_handshake()
        except:
            pass
        self.Do(self.__do_handshake__,True,funcNotified=funcNotified,bUseSelect=bUseSelect)
    def __do_handshake__(self,bRead,funcNotified=None,bUseSelect=False):
        #conn.renegotiate()
        try:
            iTime=0.0
            self.__logInfo__(self.oConn.GetStrFull(';'))
            #self.conn.setblocking(0)
            #self.conn.settimeout(0.1)
            #while self.oConn.IsConnecting==True and iTime<self.HANDSHAKE_TIMEOUT:
            if bRead and 0:
                try:
                        ret = self.conn.send('   ')
                except (SSL.WantReadError, SSL.WantWriteError, SSL.WantX509LookupError):
                        pass
                except SSL.ZeroReturnError:
                        pass
                        #self.__logTB__()
                except SSL.Error, errors:
                        #dropClient(cli, errors)
                        self.__logTB__()
                else:
                        pass
            while iTime<self.HANDSHAKE_TIMEOUT:
                s=self.conn.state_string()
                self.__logDebug__(s)
                self.__logInfo__(s)
                if s=='SSL negotiation finished successfully':
                    break
                if bRead:
                    if s.find('read finished')>0 and 0:
                        self.__logInfo__(s)
                        try:
                            ret = self.conn.send('')
                        except (SSL.WantReadError, SSL.WantWriteError, SSL.WantX509LookupError):
                            pass
                        except SSL.ZeroReturnError:
                            pass
                            #self.__logTB__()
                        except SSL.Error, errors:
                            #dropClient(cli, errors)
                            self.__logTB__()
                        else:
                            pass
                    else:
                        try:
                            ret = self.conn.recv(1024)
                        except (SSL.WantReadError, SSL.WantWriteError, SSL.WantX509LookupError):
                            pass
                        except SSL.ZeroReturnError:
                            pass
                            #self.__logTB__()
                        except SSL.Error, errors:
                            #dropClient(cli, errors)
                            self.__logTB__()
                            self.oConn.SetState(self.oConn.CONNECTION_FLT)
                            return
                        else:
                            pass
                time.sleep(0.2)
                iTime+=0.02
            if iTime>self.HANDSHAKE_TIMEOUT:
                try:
                    self.__logInfo__(self.oConn.GetStrFull(';'))
                    s=self.conn.state_string()
                    self.__logError__(s)
                except:
                    self.__logTB__()
                try:
                    self.conn.shutdown()
                except:
                    self.__logTB__()
                try:
                    self.conn.close()
                except:
                    self.__logTB__()
                self.oConn.SetState(self.oConn.CONNECTION_FLT)
                #self.SocketAborted()
                try:
                    if funcNotified is not None:
                        funcNotified(self,False)
                except:
                    self.__logTB__()
                return
            self.conn.settimeout(0.1)
            self.fdConn=self.conn.fileno()
            time.sleep(0.5)
            self.oConn.SetState(self.oConn.CONNECTED)
            self.__logInfo__(''%())
            #self.Do(self.doCommunication)
            if bUseSelect==False:
                self.Do(self.doCommunication)
            try:
                if funcNotified is not None:
                    funcNotified(self,True)
            except:
                self.__logTB__()
            #self.SetConnecting(False)
        except:
            self.__logTB__()
            #self.SetServing(False)
            self.oConn.SetState(self.oConn.CONNECTION_FLT)
    def doRecv(self):
        if self.IsServing():
            try:
                #self.conn.setblocking(1)
                #print 'run',1
                #print self.ctx
                #print self.conn
                data=self.conn.recv(self.RECV_SIZE)
                if self.__doPause__()==True:
                    return 0
                if self.verbose>10:
                    if self.srv:
                        sType='srv'
                    else:
                        sType='clt'
                    self.__logDebug__('%s;%s'%(sType,repr(data)))
                if not data:
                    self.SetServing(False)
                    return -2
                else:
                    self.__handle_recv__(data)
                    #if self.IsPaused()==False:
                    #if self.paused==False:
                    while self.HandleData()>0:
                        self.__logDebug__(''%())
                        pass
            except (SSL.WantReadError, SSL.WantWriteError, SSL.WantX509LookupError):
                #time.sleep(0.05)
                return 1
            except SSL.ZeroReturnError:
                self.conn.shutdown()
                self.SetServing(False)
                return -3
            except SSL.Error, errors:
                self.SetServing(False)
                return -2
                #self.__logTB__()
            except:
                #self.__logTB__()
                self.SetServing(False)
                #self.serving=False
            return 0
        return -1
    def doCommunication(self):
        try:
            self.__logInfo__(self.oConn.GetStrFull(';'))
            if self.VERBOSE:
                print 'run',0,self.oConn.GetStrFull(';')
            if 0 and self.oConn.IsConnecting==True:
                iTime=0.0
                #self.conn.setblocking(0)
                while self.oConn.IsSSLVerified==False and self.IsServing()==False and self.Is2Stop()==False:
                    s=self.conn.state_string()
                    self.__logDebug__(s)
                    if s=='SSL negotiation finished successfully':
                        self.oConn.SetState(self.oConn.CONNECTED)
                        break
                    if 1:
                        try:
                            ret = self.conn.recv(1024)
                        except (SSL.WantReadError, SSL.WantWriteError, SSL.WantX509LookupError):
                            pass
                        except SSL.ZeroReturnError:
                            pass
                            #self.__logTB__()
                        except SSL.Error, errors:
                            #dropClient(cli, errors)
                            #self.__logTB__()
                            #break
                            pass
                        else:
                            pass
                    time.sleep(0.2)
                    iTime+=0.02
                    if iTime>self.HANDSHAKE_TIMEOUT:
                        try:
                            self.__logInfo__(self.oConn.GetStrFull(';'))
                            s=self.conn.state_string()
                            self.__logError__(s)
                        except:
                            self.__logTB__()
                        try:
                            self.conn.shutdown()
                        except:
                            self.__logTB__()
                        try:
                            self.conn.close()
                        except:
                            self.__logTB__()
                        self.oConn.SetState(self.oConn.CONNECTION_FLT)
                        #self.SocketAborted()
                        return
                
                self.conn.settimeout(0.1)
                self.oConn.SetState(self.oConn.CONNECTED)
                #self.conn.setblocking(1)
                #self.conn.setblocking(0)
                if 0:
                    try:
                        #self.conn.setblocking(0)
                        if self.VERBOSE:
                            print self.iVerify
                            print self.conn.state_string()
                        while self.iVerify==0:
                            time.sleep(0.2)
                            if self.VERBOSE:
                                print self.iVerify
                                print self.conn.state_string()
                            #break
                        if self.VERBOSE:
                            print self.iVerify
                            print self.conn.state_string()
                        #self.conn.setblocking(1)
                    except:
                        self.__logTB__()
                        self.SetServing(False)
                #time.sleep(0.2)  # fixes startup problem
                #self.conn.settimeout(0.1)
        except:
            self.__logTB__()
            #self.SetServing(False)
            self.oConn.SetState(self.oConn.CONNECTION_FLT)
        #self.conn.setblocking(1)
        self.__logInfo__(self.oConn.GetStrFull(';'))
        try:
            while self.IsServing():
                if self.Is2Stop():
                    self.SetServing(False)
                    continue
                if self.doRecv()>0:
                    time.sleep(0.05)
                #self.__logDebug__(''%())
                self.doSend(bSingle=False)
        except:
            self.__logTB__()
            self.SetServing(False)
        if self.VERBOSE:
            print 'run',50
            print 'closeing'
        try:
            self.conn.shutdown()
        except:
            self.__logTB__()
        try:
            self.conn.close()
        except:
            self.__logTB__()
        try:
            self.srv.StopSocket(self.adr,self)
        except:
            self.__logTB__()
        self.srv=None
        try:
            self.SocketClosed()
        except:
            self.__logTB__()
            pass
        if self.VERBOSE:
            print 'exit thread'
        self.SetServing(False)
    def doSend(self,bSingle=True):
        iRet=-1
        if self.IsServing():
            #self.__logDebug__(''%())
            if self.verbose>10:
                self.__logDebug__(''%())
                bDbg=True
            else:
                bDbg=False
            if bDbg:
                self.__logDebug__('start;bSingle:%d'%(bSingle))
            self.semSend.acquire()
            #self._acquireSnd()
            #self.__logDebug__(''%())
            try:
                #self.__logDebug__('iDataSnd:%d;iLen:%d;data:%s'%(self.iDataSnd,
                #            iLen,data))
                if self.bData2Snd:
                    data=self.bufSnd.getvalue()
                    iLen=len(data)
                    if bDbg:
                        self.__logDebug__('iDataSnd:%d;iLen:%d;data:%s'%(self.iDataSnd,
                                    iLen,data))
                    iSend=0
                    iRetries=0
                    while iSend<iLen:
                        iCur=-1
                        try:
                            iCur=self.conn.send(data[iSend:iLen])
                            self.iDataSnd+=iCur
                            if bDbg:
                                self.__logDebug__('iCur:%d;iSend:%d;iDataSnd:%d'%(iCur,iSend,self.iDataSnd))
                            iSend+=iCur
                        except SSL.WantReadError:
                            #time.sleep(0.5)
                            #print iCur,iSend
                            self.__logError__('iCur:%d;iSend:%d;iDataSnd:%d'%(iCur,iSend,self.iDataSnd))
                            break
                        except (SSL.WantWriteError, SSL.WantX509LookupError):
                            #self.__logTB__()
                            #time.sleep(0.5)
                            #print iCur,iSend
                            self.__logError__('iCur:%d;iSend:%d;iDataSnd:%d'%(iCur,iSend,self.iDataSnd))
                            break
                            pass
                        except SSL.ZeroReturnError:
                            self.__logTB__()
                            self.conn.shutdown()
                            self.SetServing(False)
                            self.semSend.release()
                            #self._releaseSnd()
                            #self.__logDebug__(''%())
                            return -6
                        except SSL.Error, errors:
                            self.__logTB__()
                            self.SetServing(False)
                            self.semSend.release()
                            #self._releaseSnd()
                            #self.__logDebug__(''%())
                            return -5
                        except:
                            self.__logTB__()
                            iCur=0
                            #vtLog.vtLngCallStack(None,vtLog.ERROR,
                            #        'exception:%s'%traceback.format_exc())
                            #self.SocketAborted()
                            self.SetServing(False)
                            self.semSend.release()
                            #self._releaseSnd()
                            #self.__logDebug__(''%())
                            return -4
                        if bSingle:
                            break
                        if iSend<iLen:
                            #if vtLog.vtLngIsLogged(vtLog.INFO):
                            #    vtLog.vtLngCallStack(None,vtLog.INFO,
                            #        'not all data send;size:%8d;send:%8d;act :%8d'%(iLen,iSend,iCur))
                            time.sleep(0.05)
                            iRetries+=1
                    self.bufSnd.seek(0)
                    self.bufSnd.truncate()
                    if iSend<iLen:
                        self.bufSnd.write(data[iSend:])
                        self.bData2Snd=True
                    else:
                        self.bData2Snd=False
                    iRet=0
            except socket.error,msg:
                self.__logTB__()
                #self.SetServed(False)
                iRet=-2
                #self.__logDebug__(''%())
            except:
                self.__logTB__()
                self.SetServed(False)
                iRet=-2
                #self.__logDebug__(''%())
            self.semSend.release()
            if bDbg:
                self.__logDebug__('done;bSingle:%d'%(bSingle))
            #self._releaseSnd()
        #self.__logDebug__('iRet:%d'%(iRet))
        return iRet
    def __send__old(self,data,bRaw=False):
        try:
            if bRaw==False:
                if type(data)==types.UnicodeType:
                    data=data.encode('utf-8')#encode('ISO-8859-1')
            iLen=len(data)
            iSend=0
            iCur=0
            while iSend<iLen:
                try:
                    iCur=self.conn.send(data[iSend:iLen])
                    iSend+=iCur
                except SSL.WantReadError:
                    time.sleep(0.5)
                except (SSL.WantWriteError, SSL.WantX509LookupError):
                    #self.__logTB__()
                    time.sleep(0.5)
                    pass
                except SSL.Error, errors:
                    self.__logTB__()
                    self.SetServing(False)
                    return
                except:
                    self.__logTB__()
                    iCur=0
                    #vtLog.vtLngCallStack(None,vtLog.ERROR,
                    #        'exception:%s'%traceback.format_exc())
                    #self.SocketAborted()
                    self.SetServing(False)
                    return
                if iSend<iLen:
                    #if vtLog.vtLngIsLogged(vtLog.INFO):
                    #    vtLog.vtLngCallStack(None,vtLog.INFO,
                    #        'not all data send;size:%8d;send:%8d;act :%8d'%(iLen,iSend,iCur))
                    time.sleep(0.5)
        except (SSL.WantReadError, SSL.WantWriteError, SSL.WantX509LookupError):
            time.sleep(0.5)
            pass
        except SSL.ZeroReturnError:
            self.__logTB__()
            self.conn.shutdown()
            self.SetServing(False)
        except SSL.Error, errors:
            self.__logTB__()
            self.SetServing(False)
        except:
            self.__logTB__()
            self.SocketAborted()
            self.SetServing(False)
    def NotifyConnected(self):
    #    self.Do(time.sleep,0.5)
    #    self.Do(self.__do_NotifyConnected__)
    #def __do_NotifyConnected__(self):
        try:
            #self.__logError__('')
            #print self.oConn
            #print self.oConn.IsNotifyConnectImmediatly
            #print self.par
            if self.oConn.IsNotifyConnectImmediatly==True:
                self.par.NotifyConnected()
            #self.par.NotifyConnected()
            pass
        except:
            pass
        try:
            self.srv.NotifyConnected(self)
        except:
            #self.__logTB__()
            #self.__logTB__()
            pass
        try:
            #self.Do(self.doCommunication)
            #self.__initStart__()
            pass
        except:
            pass
            #self.__logTB__()

