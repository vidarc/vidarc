#----------------------------------------------------------------------------
# Name:         vtSock.py
# Purpose:      
#               dervied from vNetSock
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtSock.py,v 1.12 2009/03/30 22:08:08 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.config.vcCust as vcCust

sName=vcCust.is2Import('vtSock','lib','vtBuffer')
#sName=vcCust.is2Import('vtSock','lib','string')

try:
    if sName=='vtBuffer':
        from vtSockBuf import vtSockBuf as vtSock
    elif sName=='vtBufferSSL':
        from vtSockBufSSL import vtSockBufSSL as vtSock
    elif sName=='string':
        from vtSockStr import vtSockStr as vtSock
    elif sName=='stringSSL':
        from vtSockStrSSL import vtSockStrSSL as vtSock
    else:
        from vtSockStr import vtSockStr as vtSock
except:
    import traceback
    traceback.print_exc()
    from vtSockStr import vtSockStr as vtSock

def hasSSL():
    try:
        from vtSockCoreSSL import hasSSL as coreHasSSL
        return True
    except:
        pass
    return False

def isSSL():
    if hasSSL():
        if sName.endswith('SSL'):
            return True
    return False
        
