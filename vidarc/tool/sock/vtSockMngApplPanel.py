#----------------------------------------------------------------------------
# Name:         vtSockMngApplPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20090822
# CVS-ID:       $Id: vtSockMngApplPanel.py,v 1.3 2009/08/25 11:20:23 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.state.vtArtState as vtArtState

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

wxEVT_VTSOCKMNGAPPL_START=wx.NewEventType()
vEVT_TOOL_SOCK_MNG_APPL_START=wx.PyEventBinder(wxEVT_VTSOCKMNGAPPL_START,1)
def EVT_TOOL_SOCK_MNG_APPL_START(win,func):
    win.Connect(-1,-1,wxEVT_VTSOCKMNGAPPL_START,func)
def EVT_TOOL_SOCK_MNG_APPL_START_DISCONNECT(win,func):
    win.Disconnect(-1,-1,wxEVT_VTSOCKMNGAPPL_START,func)
class vtSockMngApplStart(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_TOOL_SOCK_MNG_APPL_START(<widget_name>, self.OnItemStart)
    """

    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTSOCKMNGAPPL_START)
        #self.obj=obj

wxEVT_VTSOCKMNGAPPL_STOP=wx.NewEventType()
vEVT_TOOL_SOCK_MNG_APPL_STOP=wx.PyEventBinder(wxEVT_VTSOCKMNGAPPL_STOP,1)
def EVT_TOOL_SOCK_MNG_APPL_STOP(win,func):
    win.Connect(-1,-1,wxEVT_VTSOCKMNGAPPL_STOP,func)
def EVT_TOOL_SOCK_MNG_APPL_STOP_DISCONNECT(win,func):
    win.Disconnect(-1,-1,wxEVT_VTSOCKMNGAPPL_STOP,func)
class vtSockMngApplStop(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_TOOL_SOCK_MNG_APPL_STOP(<widget_name>, self.OnItemStop)
    """

    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTSOCKMNGAPPL_STOP)
        #self.obj=obj

wxEVT_VTSOCKMNGAPPL_PAUSE=wx.NewEventType()
vEVT_TOOL_SOCK_MNG_APPL_PAUSE=wx.PyEventBinder(wxEVT_VTSOCKMNGAPPL_PAUSE,1)
def EVT_TOOL_SOCK_MNG_APPL_PAUSE(win,func):
    win.Connect(-1,-1,wxEVT_VTSOCKMNGAPPL_PAUSE,func)
def EVT_TOOL_SOCK_MNG_APPL_PAUSE_DISCONNECT(win,func):
    win.Disconnect(-1,-1,wxEVT_VTSOCKMNGAPPL_PAUSE,func)
class vtSockMngApplPause(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_TOOL_SOCK_MNG_APPL_PAUSE(<widget_name>, self.OnItemPause)
    """

    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTSOCKMNGAPPL_PAUSE)
        #self.obj=obj

[
  wxID_VTSOCKMNGAPPLPANELCBSTART,wxID_VTSOCKMNGAPPLPANELCBSTOP,
  wxID_VTSOCKMNGAPPLPANELCBPAUSE,wxID_VTSOCKMNGAPPLPANELTXTVAL,
  wxID_VTSOCKMNGAPPLPANELTXTCOUNT,wxID_VTSOCKMNGAPPLPANELPBPROC
] = [wx.NewId() for __init__ in xrange(6)]


class vtSockMngApplPanel(wx.Panel,vtLog.vtLogOriginWX):
    def __init__(self,button_pause=False,*args,**kwargs):
        if 'size' not in kwargs:
            sz=wx.Size(100,34)
            kwargs['size']=sz
        else:
            sz=kwargs['size']
        apply(wx.Panel.__init__,(self,) + args,kwargs)
        vtLog.vtLogOriginWX.__init__(self)
        if wx.Thread_IsMain()==False:
            self.__logCritical__('called by thread'%())
        bxsMain = wx.FlexGridSizer(cols=1, hgap=0, rows=2, vgap=0)
        #bxsMain.AddGrowableRow(0,1)
        #bxsMain.AddGrowableCol(0,1)
        bxsMain.SetFlexibleDirection(wx.BOTH)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_MENU)
        #print bkgCol
        style=wx.TE_READONLY#|wx.NO_BORDER
        self.txtVal = wx.TextCtrl(id=wxID_VTSOCKMNGAPPLPANELTXTVAL,
              name=u'txtVal', parent=self, pos=wx.DefaultPosition,
              size=wx.DefaultSize, style=style, value=u'')
        self.txtVal.SetMinSize(wx.Size(-1, -1))
        self.txtVal.SetBackgroundColour(bkgCol)
        #self.txtVal.Enable(False)
        bxs.AddWindow(self.txtVal, 4, border=0, flag=wx.RIGHT|wx.EXPAND)
        
        self.cbStart = wx.lib.buttons.GenBitmapButton(bitmap=vtArtState.getBitmap(vtArtState.Start),
              id=wxID_VTSOCKMNGAPPLPANELCBSTART, 
              name=u'cbExec', parent=self, pos=wx.Point(70, 158),
              size=wx.Size(30, 30), style=0)
        self.cbStart.SetMinSize(wx.Size(-1, -1))
        #self.cbStart.Enable(False)
        self.cbStart.Bind(wx.EVT_BUTTON, self.OnCbStartButton,
              id=wxID_VTSOCKMNGAPPLPANELCBSTART)
        bxs.AddWindow(self.cbStart, 0, border=4, flag=wx.LEFT|wx.RIGHT)
        
        self.cbStop = wx.lib.buttons.GenBitmapButton(bitmap=vtArtState.getBitmap(vtArtState.Stop),
              id=wxID_VTSOCKMNGAPPLPANELCBSTOP, 
              name=u'cbStop', parent=self, pos=wx.Point(150, 158),
              size=wx.DefaultSize, style=0)
        self.cbStop.Bind(wx.EVT_BUTTON, self.OnCbStopButton,
              id=wxID_VTSOCKMNGAPPLPANELCBSTOP)
        self.cbStop.Enable(False)
        if button_pause==False:
            bxs.AddWindow(self.cbStop, 0, border=0, flag=wx.RIGHT)
            self.cbPause=None
        else:
            bxs.AddWindow(self.cbStop, 0, border=4, flag=wx.RIGHT)
            self.cbPause = wx.lib.buttons.GenBitmapButton(bitmap=vtArtState.getBitmap(vtArtState.Pause),
                  id=wxID_VTSOCKMNGAPPLPANELCBPAUSE, 
                  name=u'cbPause', parent=self, pos=wx.Point(150, 158),
                  size=wx.DefaultSize, style=0)
            self.cbPause.Bind(wx.EVT_BUTTON, self.OnCbPauseButton,
                  id=wxID_VTSOCKMNGAPPLPANELCBPAUSE)
            self.cbPause.Enable(False)
            bxs.AddWindow(self.cbPause, 0, border=0, flag=wx.RIGHT)
        
        bxsMain.AddSizer(bxs, 1, border=0, flag=wx.EXPAND)
        
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.pbProc = wx.Gauge(id=wxID_VTSOCKMNGAPPLPANELPBPROC,
              name=u'pbProc', parent=self, pos=wx.DefaultPosition,
              range=1000, size=wx.DefaultSize, style=wx.GA_HORIZONTAL|wx.GA_SMOOTH)
        self.pbProc.SetMinSize(wx.Size(-1, 8))
        self.pbProc.SetMaxSize(wx.Size(-1, 12))
        bxs.AddWindow(self.pbProc, 1, border=4, flag=wx.TOP|wx.EXPAND)
        bxsMain.AddSizer(bxs, 1, border=2, flag=wx.EXPAND|wx.TOP)
        bxsMain.AddGrowableRow(0,1)
        bxsMain.AddGrowableCol(0,1)
        self.SetSizer(bxsMain)
        #self.SetSize(sz)
        bxsMain.Layout()
        
        self.oAppl=None
        self.dFunc={}
    def GetCtrlSizer(self):
        bxsMain=self.GetSizer()
        bxs=bxsMain.GetChildren()[0].GetSizer()
        return bxs
    def AddWindow(self,*args,**kwargs):
        bxs=self.GetCtrlSizer()
        bxs.AddWindow(*args,**kwargs)
    def AddSizer(self,*args,**kwargs):
        bxs=self.GetCtrlSizer()
        bxs.AddSizer(*args,**kwargs)
    def AddSpacer(self,*args,**kwargs):
        bxs=self.GetCtrlSizer()
        bxs.AddSpacer(*args,**kwargs)
    def SetValue(self,fVal,fCount):
        try:
            wx.CallAfter(self.pbProc.SetValue,int((fVal/fCount)*1000.0))
        except:
            self.__logTB__()
            wx.CallAfter(self.pbProc.SetValue,0)
    def SetString(self,sVal):
        try:
            wx.CallAfter(self.txtVal.SetValue,sVal)
        except:
            self.__logTB__()
            wx.CallAfter(self.txtVal.SetValue,'')
    def SetFunc(self,name,func,*args,**kwargs):
        if name is None:
            self.dFunc={}
            return
        if func is None:
            if name in self.dFunc:
                del self.dFunc[name]
        else:
            self.dFunc[name]=(func,args,kwargs)
    def __doFunc__(self,name):
        try:
            if name in self.dFunc:
                func,args,kwargs=self.dFunc[name]
                func(*args,**kwargs)
        except:
            self.__logTB__()
    def SetAppl(self,oAppl):
        self.oAppl=oAppl
    def GetAppl(self):
        return self.oAppl
    def Start(self):
        try:
            self.__logInfo__(''%())
            if self.oAppl is not None:
                self.oAppl.Start()
                self.__start__(not self.oAppl.IsRunning())
            else:
                self.__start__(True)
            self.__doFunc__('start')
        except:
            self.__logTB__()
    def Stop(self):
        try:
            self.__logInfo__(''%())
            self.__doFunc__('stop')
            if self.oAppl is not None:
                self.oAppl.Stop()
            self.__stop__()
        except:
            self.__logTB__()
    def Pause(self):
        try:
            self.__logInfo__(''%())
            self.__doFunc__('Pause')
            if self.oAppl is not None:
                self.oAppl.Pause()
            #self.__stop__()
        except:
            self.__logTB__()
    def __start__(self,bClr=True):
        if bClr==True:
            self.txtVal.SetValue(u'')
            #self.txtCount.SetValue(u'')
            self.pbProc.SetValue(0)
        self.cbStart.Enable(False)
        if self.cbPause:
            self.cbPause.Enable(True)
        self.cbStop.Enable(True)
    def __stop__(self,bEnStart=True,bClr=True):
        if bEnStart:
            if bClr==True:
                self.txtVal.SetValue(u'')
                #self.txtCount.SetValue(u'')
            self.pbProc.SetValue(0)
            self.cbStart.Enable(True)
            if self.cbPause:
                self.cbPause.Enable(False)
        self.cbStop.Enable(False)
        self.fProcLast=-1
    def EnableStart(self,bFlag=True):
        self.cbStart.Enable(bFlag)
    def EnableStop(self,bFlag=True):
        self.cbStop.Enable(bFlag)
    def Do(self,f,*args,**kwargs):
        if self.oAppl is not None:
            self.oAppl.Do(f,*args,**kwargs)
            self.Start()
            #if self.thd2Ctrl.IsRunning()==False:
            #    
        else:
            f(*args,**kwargs)
    def OnCbStartButton(self,evt):
        evt.Skip()
        try:
            self.__logInfo__(''%())
            self.Start()
            #self.Start()
            wx.PostEvent(self,vtSockMngApplStart(self))
        except:
            self.__logTB__()
    def OnCbStopButton(self,evt):
        evt.Skip()
        try:
            self.__logInfo__(''%())
            self.__doFunc__('stop')
            if self.oAppl is not None:
                self.oAppl.Stop()
                self.__stop__(bEnStart=not self.oAppl.IsRunning(),bClr=False)
            else:
                self.__stop__(bEnStart=True,bClr=False)
            wx.PostEvent(self,vtSockMngApplStop(self))
        except:
            self.__logTB__()
    def OnCbPauseButton(self,evt):
        evt.Skip()
        try:
            self.__logInfo__(''%())
            self.__doFunc__('pause')
            wx.PostEvent(self,vtSockMngApplPause(self))
        except:
            self.__logTB__()
    def __getattr__(self,name):
        if self.oAppl is not None:
            if hasattr(self.oAppl,name):
                return getattr(self.oAppl,name)
        raise AttributeError(name)
