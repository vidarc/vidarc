#Boa:FramePanel:vtInputAttrPanel
#----------------------------------------------------------------------
# Name:         vtInputAttrPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060206
# CVS-ID:       $Id: vtInputAttrPanel.py,v 1.17 2010/03/10 22:38:53 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import wx
    import wx.lib.buttons
    from wx.lib.anchors import LayoutAnchors
    import sys,string
    import vidarc.tool.lang.vtLgBase as vtLgBase
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel
    import vidarc.tool.art.vtArt as vtArt
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

#from vidarc.tool.net.vNetXmlWxGui import *
#from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_GOT_CONTENT
#from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_GET_NODE
#from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_REMOVE_NODE
#from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_LOCK
#from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_UNLOCK
#from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_SET_NODE

[wxID_VTINPUTATTRPANEL, wxID_VTINPUTATTRPANELCBAPPLY, 
 wxID_VTINPUTATTRPANELCBDEL, wxID_VTINPUTATTRPANELCMBATTRNAME, 
 wxID_VTINPUTATTRPANELLBLATTRNAME, wxID_VTINPUTATTRPANELLBLATTRVAL, 
 wxID_VTINPUTATTRPANELLSTATTR, wxID_VTINPUTATTRPANELTXTATTRVAL, 
 wxID_VTINPUTATTRPANELTXTSUFF, 
] = [wx.NewId() for _init_ctrls in range(9)]

class vtInputAttrPanel(wx.Panel,vtXmlNodePanel):
    IMG_DICT={'tel':vtArt.Tel,
            'mobile':vtArt.Mobile,
            'fax':vtArt.Fax,
            'email':vtArt.EMail,
            'mail':vtArt.Mail,
            'mailbox':vtArt.MailBox,
            'reference':vtArt.Link,
            'safe':vtArt.Safe,
            'tools':vtArt.Tools,
            'client':vtArt.Customer,
            'clientshort':vtArt.CustomerShort,
            'facility':vtArt.Facility,
            'manager':vtArt.Manager,
            'location':vtArt.Loc,
            'calc':vtArt.Calc,
            'cash':vtArt.Cash,
            'country':vtArt.Country,
            'region':vtArt.Region,
            'address':vtArt.Addr,
            'area':vtArt.Area,
            }
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbDel, 0, border=0, flag=0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsAttr, 3, border=0, flag=wx.SHRINK | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsVal, 3, border=0, flag=wx.SHRINK | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.lstAttr, 3, border=0, flag=wx.SHRINK | wx.EXPAND)
        parent.AddSizer(self.bxsBt, 1, border=4, flag=wx.LEFT)

    def _init_coll_bxsAttr_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblAttrName, 1, border=4,
              flag=wx.RIGHT | wx.SHRINK | wx.EXPAND)
        parent.AddWindow(self.cmbAttrName, 2, border=0,
              flag=wx.SHRINK | wx.EXPAND)
        parent.AddWindow(self.txtSuff, 1, border=0, flag=wx.SHRINK | wx.EXPAND)

    def _init_coll_bxsVal_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblAttrVal, 1, border=4,
              flag=wx.RIGHT | wx.SHRINK | wx.EXPAND)
        parent.AddWindow(self.txtAttrVal, 3, border=0,
              flag=wx.SHRINK | wx.EXPAND)

    def _init_coll_lstAttr_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'attribute'), width=120)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'value'), width=250)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=2, hgap=0, rows=3, vgap=0)

        self.bxsAttr = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsVal = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsAttr_Items(self.bxsAttr)
        self._init_coll_bxsVal_Items(self.bxsVal)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTINPUTATTRPANEL,
              name=u'vtInputAttrPanel', parent=prnt, pos=wx.Point(109, 318),
              size=wx.Size(426, 192), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(418, 165))
        self.SetAutoLayout(True)

        self.lblAttrName = wx.StaticText(id=wxID_VTINPUTATTRPANELLBLATTRNAME,
              label=_(u'attribute'), name=u'lblAttrName', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(81, 23), style=wx.ALIGN_RIGHT)

        self.cmbAttrName = wx.ComboBox(choices=[],
              id=wxID_VTINPUTATTRPANELCMBATTRNAME, name=u'cmbAttrName',
              parent=self, pos=wx.Point(85, 0), size=wx.Size(170, 21),
              style=wx.CB_SORT, value=u'')
        self.cmbAttrName.SetLabel(u'')
        self.cmbAttrName.Bind(wx.EVT_TEXT_ENTER, self.OnCmbAttrNameTextEnter,
              id=wxID_VTINPUTATTRPANELCMBATTRNAME)
        self.cmbAttrName.Bind(wx.EVT_COMBOBOX, self.OnCmbAttrNameCombobox,
              id=wxID_VTINPUTATTRPANELCMBATTRNAME)
        self.cmbAttrName.Bind(wx.EVT_TEXT, self.OnCmbAttrNameText,
              id=wxID_VTINPUTATTRPANELCMBATTRNAME)

        self.txtSuff = wx.TextCtrl(id=wxID_VTINPUTATTRPANELTXTSUFF,
              name=u'txtSuff', parent=self, pos=wx.Point(255, 0),
              size=wx.Size(85, 23), style=0, value=u'')

        self.lblAttrVal = wx.StaticText(id=wxID_VTINPUTATTRPANELLBLATTRVAL,
              label=_(u'value'), name=u'lblAttrVal', parent=self,
              pos=wx.Point(0, 23), size=wx.Size(81, 23), style=wx.ALIGN_RIGHT)

        self.txtAttrVal = wx.TextCtrl(id=wxID_VTINPUTATTRPANELTXTATTRVAL,
              name=u'txtAttrVal', parent=self, pos=wx.Point(85, 23),
              size=wx.Size(255, 23), style=0, value=u'')

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTINPUTATTRPANELCBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Apply'), name=u'cbApply',
              parent=self, pos=wx.Point(345, 46), size=wx.Size(76, 30),
              style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VTINPUTATTRPANELCBAPPLY)

        self.cbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTINPUTATTRPANELCBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Del'), name=u'cbDel',
              parent=self, pos=wx.Point(345, 84), size=wx.Size(76, 30),
              style=0)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VTINPUTATTRPANELCBDEL)

        self.lstAttr = wx.ListCtrl(id=wxID_VTINPUTATTRPANELLSTATTR,
              name=u'lstAttr', parent=self, pos=wx.Point(0, 46),
              size=wx.Size(341, 119), style=wx.LC_REPORT|wx.LC_SORT_ASCENDING)
        self._init_coll_lstAttr_Columns(self.lstAttr)
        self.lstAttr.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstAttrListItemSelected, id=wxID_VTINPUTATTRPANELLSTATTR)
        self.lstAttr.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstAttrListItemDeselected,
              id=wxID_VTINPUTATTRPANELLSTATTR)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vtInput')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,applName=_(u'vtInput Attr'),
                lWidgets=[self.lstAttr])
        #self.bModified=False
        #self.bEnableMark=True
        #self.verbose=VERBOSE
        self.selectedIdx=-1
        
        self.SetImageDict(self.IMG_DICT)
        #self.gbsData.AddGrowableRow(3)
        #self.gbsData.AddGrowableCol(1)
        
        self.cbApply.SetBitmapLabel(vtArt.getBitmap(vtArt.ApplyAttr))
        self.cbDel.SetBitmapLabel(vtArt.getBitmap(vtArt.DelAttr))
        self.__setupImageList__()
        
        self.Move(pos)
        self.SetSize(size)
    def SetEnableMarkOld(self,flag):
        self.bEnableMark=flag
    def SetImageDict(self,d):
        self.dImages=d
    def __setupImageList__(self):
        self.imgDict={}
        self.imgLstTyp=wx.ImageList(16,16)
        keys=self.dImages.keys()
        keys.sort()
        for k in keys:
            self.imgDict[k]=self.imgLstTyp.Add(vtArt.getBitmap(self.dImages[k]))
            self.cmbAttrName.Append(k)
        self.lstAttr.SetImageList(self.imgLstTyp,wx.IMAGE_LIST_SMALL)

    def SetModifiedOld(self,state):
        self.bModified=state
    def GetModifiedOld(self):
        return self.bModified
    def __isModified__Old(self):
        if self.bModified:
            return True
        return False
    def __markModified__Old(self,flag=True):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        if self.bEnableMark==False:
            return
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'__markModified__;flag:%s'%(flag),
        #                self)
        if flag:
            f=self.lstAttr.GetFont()
            f.SetWeight(wx.FONTWEIGHT_BOLD)
            self.lstAttr.SetFont(f)
        else:
            f=self.lstAttr.GetFont()
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
            self.lstAttr.SetFont(f)
        self.lstAttr.Refresh()
    def __Clear__(self):
        try:
            #self.__markModified__(False)
            self.cmbAttrName.SetValue('')
            self.txtAttrVal.SetValue('')
            self.txtSuff.SetValue('')
            self.lstAttr.DeleteAllItems()
            self.selectedIdx=-1
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
    def IsBusy(self):
        return False
    def Stop(self):
        pass
    def SetRegNode(self,obj):
        pass
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
    def OnGotContentOld(self,evt):
        evt.Skip()
    def OnGetNodeOld(self,evt):
        evt.Skip()
        if self.node is None:
            return
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            self.SetNode(self.node)
    def OnSetNodeOld(self,evt):
        evt.Skip()
    def OnAddNodeOld(self,evt):
        evt.Skip()
    def OnDelNodeOld(self,evt):
        evt.Skip()
    def OnRemoveNodeOld(self,evt):
        evt.Skip()
    def OnLockOld(self,evt):
        evt.Skip()
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                self.Lock(False)
            else:
                self.Lock(True)
    def OnUnLockOld(self,evt):
        evt.Skip()
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['released']:
                self.Lock(False)
    def __getAttrNameSuff__(self,sAttrName):
        strs=string.split(sAttrName,'_')
        try:
            if len(strs)>1:
                sSuff=strs[-1]
                sAttrName=sAttrName[:-(len(sSuff)+1)]
            else:
                sSuff=''
        except:
            sSuff=''
        return sAttrName,sSuff
    def __getAttrName__(self,sAttrName):
        sAttr,sSuff=self.__getAttrNameSuff__(sAttrName)
        return self.__buildAttrName__(sAttr,sSuff)
    def __buildAttrName__(self,sAttr,sSuff):
        if len(sSuff)>0:
            return sAttr+'_'+sSuff
        return sAttr
    def __SetNode__(self,node):
        if VERBOSE>0:
            self.__logDebug__(''%())
        try:
            attrNode=self.doc.getChild(node,'attributes')
            if attrNode is not None:
                attrs={}
                for o in self.doc.getChilds(attrNode):
                    s=self.doc.getText(o)
                    sAttr=self.doc.getTagName(o)
                    attrs[sAttr]=s
            else:
                attrs={}
                #attrs['email']=''
            keys=attrs.keys()
            keys.sort()
            for k in keys:
                sAttr,sSuff=self.__getAttrNameSuff__(k)
                try:
                    imgId=self.imgDict[sAttr]
                except:
                    imgId=-1
                index = self.lstAttr.InsertImageStringItem(sys.maxint, k, imgId)
                self.lstAttr.SetStringItem(index, 1, attrs[k])
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            attrNode=self.doc.getChildForced(node,'attributes')
            if attrNode is None:
                self.__logCritical__('%r'%(repr(node)))
                return  # fix me, add element node
            childs=self.doc.getChilds(attrNode)
            d={}
            for c in childs:
                d[self.doc.getTagName(c)]=c
            for i in range(0,self.lstAttr.GetItemCount()):
                it=self.lstAttr.GetItem(i)
                sAttrName=it.m_text
                it=self.lstAttr.GetItem(i,1)
                sAttrVal=it.m_text
                d[sAttrName]=None
                self.doc.setNodeText(attrNode,sAttrName,sAttrVal)
            for k in d.keys():
                if d[k] is not None:
                    self.doc.deleteNode(d[k],attrNode)
            #self.GetNodeFin(node)
            #self.SetModified(False)
            #self.__markModified__(False)
        except:
            self.__logTB__()
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        #if wx.Thread_IsMain()==False:
        #    vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if flag:
            self.cbApply.Enable(False)
            self.cbDel.Enable(False)
            self.cmbAttrName.Enable(False)
            self.txtAttrVal.Enable(False)
            self.lstAttr.Enable(False)
            pass
        else:
            self.cbApply.Enable(True)
            self.cbDel.Enable(True)
            self.cmbAttrName.Enable(True)
            self.txtAttrVal.Enable(True)
            self.lstAttr.Enable(True)
            pass

    def OnCbApplyButton(self, event):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        self.SetModified(True)
        #self.__markModified__(True)
        sNewAttr=self.cmbAttrName.GetValue()
        sSuff=self.txtSuff.GetValue()
        if len(sSuff)>0:
            sNewAttr+='_%s'%sSuff
        for i in range(0,self.lstAttr.GetItemCount()):
            it=self.lstAttr.GetItem(i)
            sAttrName=it.m_text
            if sAttrName==sNewAttr:
                it.m_col=1
                it.m_mask = wx.LIST_MASK_TEXT
                it.m_text=self.txtAttrVal.GetValue()
                self.lstAttr.SetItem(it)
                return
        try:
            sImgName,sSuff=self.__getAttrNameSuff__(sNewAttr)
            imgId=self.imgDict[sImgName]
        except:
            imgId=-1
        index = self.lstAttr.InsertImageStringItem(sys.maxint, sNewAttr, imgId)
        self.lstAttr.SetStringItem(index, 1, self.txtAttrVal.GetValue())
        for i in range(self.lstAttr.GetItemCount()):
            self.lstAttr.SetItemState(i,0,wx.LIST_STATE_SELECTED)
        self.lstAttr.SetItemState(index,wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
        event.Skip()

    def OnCbDelButton(self, event):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        if self.selectedIdx>=0:
            self.lstAttr.DeleteItem(self.selectedIdx)
            self.SetModified(True)
            #self.__markModified__(True)
        event.Skip()
    def OnLstAttrListItemSelected(self, event):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        idx=event.GetIndex()
        #it=event.GetItem()
        #it.m_mask = wx.LIST_MASK_TEXT
        #it.m_itemId=idx
        #it.m_col=0
        it=self.lstAttr.GetItem(idx,0)
        sAttrName=it.m_text
        #it.m_col=1
        it=self.lstAttr.GetItem(idx,1)
        sAttrVal=it.m_text
        #self.txtAttrName.SetValue(sAttrName)
        
        sAttrName,sSuff=self.__getAttrNameSuff__(sAttrName)
        self.txtSuff.SetValue(sSuff)
        self.cmbAttrName.SetValue(sAttrName)
        self.txtAttrVal.SetValue(sAttrVal)
        self.selectedIdx=it.m_itemId
        event.Skip()

    def OnLstAttrListItemDeselected(self, event):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        self.selectedIdx=-1
        self.cmbAttrName.SetValue('')
        self.txtAttrVal.SetValue('')
        self.txtSuff.SetValue('')
        event.Skip()
    def SetVal(self,sVal,attr,suff=None):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        if suff is None:
            for i in range(0,self.lstAttr.GetItemCount()):
                it=self.lstAttr.GetItem(i)
                sAttrName=it.m_text
                
                sAttr,sSuff=self.__getAttrNameSuff__(sAttrName)
                if sAttr==attr:
                    try:
                        imgId=self.imgDict[sAttr]
                    except:
                        imgId=-1
                    it=self.lstAttr.SetStringItem(i,1,sVal,imgId)
        else:
            for i in range(0,self.lstAttr.GetItemCount()):
                it=self.lstAttr.GetItem(i)
                sAttrName=it.m_text
                sAttr,sSuff=self.__getAttrNameSuff__(sAttrName)
                if sAttr==attr:
                    if sSuff==suff:
                        try:
                            imgId=self.imgDict[sAttr]
                        except:
                            imgId=-1
                        it=self.lstAttr.SetStringItem(i,1,sVal,imgId)
            return None
    def GetVal(self,attr,suff=None):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        if suff is None:
            lst=[]
            for i in range(0,self.lstAttr.GetItemCount()):
                it=self.lstAttr.GetItem(i)
                sAttrName=it.m_text
                
                sAttr,sSuff=self.__getAttrNameSuff__(sAttrName)
                if sAttr==attr:
                    it=self.lstAttr.GetItem(i,1)
                    sAttrVal=it.m_text
                    lst.append((sAttr,sSuff,sAttrVal))
            return lst
        else:
            for i in range(0,self.lstAttr.GetItemCount()):
                it=self.lstAttr.GetItem(i)
                sAttrName=it.m_text
                sAttr,sSuff=self.__getAttrNameSuff__(sAttrName)
                if sAttr==attr:
                    if sSuff==suff:
                        it=self.lstAttr.GetItem(i,1)
                        sAttrVal=it.m_text
                        return sAttrVal
            return None
    def OnCmbAttrNameTextEnter(self, event):
        event.Skip()

    def OnCmbAttrNameCombobox(self, event):
        event.Skip()

    def OnCmbAttrNameText(self, event):
        event.Skip()
            
