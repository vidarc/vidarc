#----------------------------------------------------------------------------
# Name:         vtInputGridSmall.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vtInputGridSmall.py,v 1.15 2008/03/22 14:34:25 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import  wx
import  wx.grid as gridlib
import string,copy
import vidarc.tool.xml.vtXmlDom as vtXmlDom
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.input.vtInputGrid import vtInputGridInfoChanged
from vidarc.tool.input.vtInputGrid import vtInputGridInfoSelected

VERBOSE=0
#---------------------------------------------------------------------------
# defined event for vgpXmlTree item selected
wxEVT_VTINPUT_GRIDSMALL_INFO_CHANGED=wx.NewEventType()
vEVT_VTINPUT_GRIDSMALL_INFO_CHANGED=wx.PyEventBinder(wxEVT_VTINPUT_GRIDSMALL_INFO_CHANGED,1)
def EVT_VTINPUT_GRIDSMALL_INFO_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTINPUT_GRIDSMALL_INFO_CHANGED,func)
class vtInputGridSmallInfoChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTINPUT_GRIDSMALL_INFO_CHANGED(<widget_name>, self.OnInfoChanged)
    """

    def __init__(self,obj,row,col):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTINPUT_GRIDSMALL_INFO_CHANGED)
        self.obj=obj
        self.row=row
        self.col=col
    def GetObj(self):
        return self.obj
    def GetRow(self):
        return self.row
    def GetCol(self):
        return self.col

wxEVT_VTINPUT_GRIDSMALL_INFO_SELECTED=wx.NewEventType()
vEVT_VTINPUT_GRIDSMALL_INFO_SELECTED=wx.PyEventBinder(wxEVT_VTINPUT_GRIDSMALL_INFO_SELECTED,1)
def EVT_VTINPUT_GRIDSMALL_INFO_SELECTED(win,func):
    win.Connect(-1,-1,wxEVT_VTINPUT_GRIDSMALL_INFO_SELECTED,func)
class vtInputGridSmallInfoSelected(wx.PyEvent):
    """
    Posted Events:
        Project Info SELECTED event
            EVT_VTINPUT_GRIDSMALL_INFO_SELECTED(<widget_name>, self.OnInfoSELECTED)
    """

    def __init__(self,obj,row,col):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTINPUT_GRIDSMALL_INFO_SELECTED)
        self.obj=obj
        self.row=row
        self.col=col
    def GetObj(self):
        return self.obj
    def GetRow(self):
        return self.row
    def GetCol(self):
        return self.col

class vtInputGridSmall(gridlib.Grid):
    EDIT_DIRECTION_LEFT_RIGHT = 0
    EDIT_DIRECTION_TOP_DOWN = 1
    def __init__(self, parent, id, pos, size,name='vtInputGridSmall'):
        gridlib.Grid.__init__(self, parent, id,pos,size,name=name)
        self.table=None
        self.emptyEditor=None
        self.emptyRenderer=None
        self.imgRenderer=None
        self.lstIDNodes=None
        #table = gridAttrData()

        # The second parameter means that the grid is to take ownership of the
        # table and will destroy it when done.  Otherwise you would need to keep
        # a reference to it and call it's Destroy method later.
        #sself.SetTable(table, True)
        self.lstColumnSize=None
        self.lstColumnAlign=None
        self.SetColLabelSize(20)
        self.SetRowLabelSize(120)
        self.SetMargins(0,0)
        self.AutoSizeColumns(True)
        self.AutoSizeRows(True)
        self.tooltip=wx.ToolTip('dfg')
        self.SetToolTip(self.tooltip)
        #self.tooltip.Enable(True)
        #self.tooltip.SetDelay(500)

        gridlib.EVT_GRID_CELL_LEFT_CLICK(self, self.OnLeftClick)
        gridlib.EVT_GRID_CELL_LEFT_DCLICK(self, self.OnLeftDClick)
        gridlib.EVT_GRID_CELL_CHANGE(self, self.OnCellChange)
        gridlib.EVT_GRID_SELECT_CELL(self, self.OnCellSelect)
        gridlib.EVT_GRID_EDITOR_SHOWN(self, self.OnEditorShown)
        self.Bind(wx.EVT_KEY_DOWN, self.OnKeyDown)
        
        #gridlib.EVT_GRID_RANGE_SELECT(self,self._OnSelectedRange)
        
        self.editDirection=self.EDIT_DIRECTION_LEFT_RIGHT
        self.bLockLabels=False
        #self.editDirection=self.EDIT_DIRECTION_TOP_DOWN
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
    def SetEditDirection(self,type):
        self.editDirection=type
    def SetLockLabels(self,bFlag):
        self.bLockLabels=bFlag
    def _OnSelectedRange(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #vtLog.CallStack('')
        if evt.Selecting():
            #print 'multi',
            #print evt.GetTopRow(),evt.GetBottomRow()
            self.ClearSelection()
            #wx.CallAfter(self.ClearSelection)
        else:
            #print 'single',
            #print evt.GetTopRow(),evt.GetBottomRow()
            pass
    def OnKeyDown(self, evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        try:
            #vtLog.CallStack(evt.KeyCode())
            if wx.VERSION >= (2,8):
                key=evt.GetKeyCode()
            else:
                key = evt.KeyCode()
            if key != wx.WXK_RETURN:
                evt.Skip()
                return
            
            if evt.ControlDown():   # the edit control needs this key
                evt.Skip()
                return
            
            self.DisableCellEditControl()
            if self.editDirection==self.EDIT_DIRECTION_LEFT_RIGHT:
                def moveRight():
                    success = self.MoveCursorRight(evt.ShiftDown())
                    if not success:
                        newRow = self.GetGridCursorRow() + 1
                        
                        if newRow < self.GetTable().GetNumberRows():
                            
                            self.SetGridCursor(newRow, 0)
                            #self.MakeCellVisible(newRow, 0)
                            
                        else:
                            # this would be a good place to add a new row if your app
                            # needs to do that
                            return True
                    return not self.IsReadOnly(self.GetGridCursorRow(),self.GetGridCursorCol())
                while moveRight()==False:
                    pass
            elif self.editDirection==self.EDIT_DIRECTION_TOP_DOWN:
                def moveDown():
                    success = self.MoveCursorDown(evt.ShiftDown())
                    
                    if not success:
                        newCol = self.GetGridCursorCol() + 1
                        
                        if newCol < self.GetTable().GetNumberCols():
                            self.SetGridCursor(0,newCol)
                            #self.MakeCellVisible(0,newCol)
                        else:
                            # this would be a good place to add a new row if your app
                            # needs to do that
                            return True
                    return not self.IsReadOnly(self.GetGridCursorRow(),self.GetGridCursorCol())
                while moveDown()==False:
                    pass
            #iRow,iCol=self.GetGridCursorRow(),self.GetGridCursorCol()
        except:
            vtLog.vtLngTB(self.GetName())
        #evt.Skip()

    # I do this because I don't like the default behaviour of not starting the
    # cell editor on double clicks, but only a second click.
    def OnLeftClick(self, evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        try:
            if self.CanEnableCellControl():
                self.EnableCellEditControl()
            #self.tooltip.Enable(True)
            #self.tooltip.SetDelay(500)
            #self.tooltip.SetTip('sdfd')
            if VERBOSE:
                vtLog.CallStack('')
                print evt.GetRow(),evt.GetCol()
                print self.GetCellValue(evt.GetRow(),evt.GetCol())
        except:
            vtLog.vtLngTB(self.GetName())
        evt.Skip()
    def OnLeftDClick(self, evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.CanEnableCellControl():
            self.EnableCellEditControl()
        evt.Skip()
    def OnEditorShown(self,evt):
        #vtLog.CallStack('')
        evt.Skip()
    def OnCellChange(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        try:
            #vtLog.CallStack('')
            #self.__calcSum__(evt.GetCol())
            if VERBOSE:
                vtLog.CallStack('')
            font = self.GetFont()
            font.SetWeight(wx.BOLD)
            iRow,iCol=evt.GetRow(),evt.GetCol()
            gridlib.Grid.SetCellFont(self,iRow,iCol,font)
            doc,nodeType,sType=self.dataSrc.GetRowColType(iRow,iCol)
            min,max=self.__getGridTypeLimit__(doc,nodeType,sType)
            sVal=self.GetCellValue(iRow,iCol)
            val=self.__getVal__(sType,sVal)
            if min is not None:
                if val<min:
                    val=min
                    #self.SetCellValue(iRow,iCol,sVal)
            if max is not None:
                if val>max:
                    val=max
                    #self.SetCellValue(iRow,iCol,sVal)
            sVal=self.__convVal2DispType(sType,val)
            if VERBOSE:
                print '   row:%04d,col:%04d'%(iRow,iCol),
                print min,type(min),max,type(max),
                #print '      ',nodeType
                print sType,val,type(val),sVal,type(sVal)
                #print type
            self.SetCellValue(iRow,iCol,sVal)
            wx.PostEvent(self,vtInputGridInfoChanged(self,evt.GetRow(),evt.GetCol()))
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'row:%d,col:%d;min:%s(%s);max:%s(%s);sType:%s;val:%s(%s);sVal:%s(%s)'%(iRow,
                            iCol,min,type(min),max,type(max),sType,val,type(val),sVal,type(sVal)),self)
        except:
            vtLog.vtLngTB(self.GetName())
        evt.Skip()
    def OnCellSelect(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        evt.Skip()
        if self.bLockLabels==True:
            return
        self.BeginBatch()
        try:
            #vtLog.CallStack('')
            iRow,iCol=evt.GetRow(),evt.GetCol()
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'row:%d;col:%d'%(iRow,iCol),self)
            wx.PostEvent(self,vtInputGridInfoSelected(self,evt.GetRow(),evt.GetCol()))
            for i in range(self.GetNumberCols()):
                sCol=self.dataSrc.GetColLabelValue(i,iRow)
                #self.m_table.SetColLabelValue(i,sCol)
                self.SetColLabelValue(i,sCol)
                #wx.CallAfter(self.SetColLabelValue,i,sCol)
                #if sCol is not None:
                    #self.table.SetChangeableColLabelValue(i,sCol)
                #    self.SetCellValue(0,i,sCol)
        except:
            vtLog.vtLngTB(self.GetName())
        self.EndBatch()
        #self.ForceRefresh()
        #msg = gridlib.GridTableMessage(self.table, gridlib.GRIDTABLE_REQUEST_VIEW_GET_VALUES)
        #msg = gridlib.GridTableMessage(self.table, gridlib.GRIDTABLE_NOTIFY_COLS_INSERTED)
        #self.ProcessTableMessage(msg)
    def SetColumnSize(self,lst):
        self.lstColumnSize=lst
    def SetColumnAlign(self,lst):
        self.lstColumnAlign=lst
    def SetUp(self,lstNodes,dataSrc):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #self.doc=doc
        #if lstIDNodes is not None:
        #    self.lstIDNodes=lstIDNodes
        #else:
        #    lstIDNodes=self.lstIDNodes
        #if self.CanEnableCellControl():
        #    self.DisableCellEditControl()
        self.dataSrc=dataSrc
        lstNodes=dataSrc.GetDataNodes()
        iRowCount=0
        iColCount=0
        for lst in lstNodes:
            i=len(lst)
            if i>iColCount:
                iColCount=i
            iRowCount+=1
        #iRowCount-=1
        #iColCount-=1
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,
                        'col:%d;row:%d'%(iColCount,iRowCount),self)
        tup2Sel=None
        if iColCount==0 and iRowCount==0 and 0:
            self.ClearGrid()
            tb=self.GetTable()
            if tb is not None:
                tb.Clear()
            vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
            return
        self.BeginBatch()
        try:
            try:
                self.CreateGrid(iRowCount,iColCount)
            except:
                iCol=self.GetNumberCols()
                iColDiff=iColCount-iCol
                if VERBOSE:
                    vtLog.vtLngCurWX(vtLog.DEBUG,
                            'col:%d;colDiff:%d'%(iCol,iColDiff),self)
                if iColDiff!=0:
                    if iColCount>iCol:
                        self.AppendCols(iColCount-iCol)
                    else:
                        self.DeleteCols(iColCount,iCol-iColCount)
                iRow=self.GetNumberRows()
                iRowDiff=iRowCount-iRow
                if VERBOSE:
                    vtLog.vtLngCurWX(vtLog.DEBUG,
                            'row:%d;rowDiff:%d'%(iRow,iRowDiff),self)
                if iRowDiff!=0:
                    if iRowCount>iRow:
                        self.AppendRows(iRowCount-iRow)
                    else:
                        self.DeleteRows(iRowCount,iRow-iRowCount)
            font = self.GetFont()
            font.SetWeight(wx.NORMAL)
            
            for i in range(self.GetNumberCols()):
                sCol=self.dataSrc.GetColLabelValue(i,0)
                self.SetColLabelValue(i,sCol)
            for iRow in range(iRowCount):
                sRow=self.dataSrc.GetRowLabelValue(iRow)
                self.SetRowLabelValue(iRow,sRow)
                for iCol in range(iColCount):
                    doc,nodeType,sType=self.dataSrc.GetRowColType(iRow,iCol)
                    type,edt,rend=self.__getGridType__(doc,nodeType,sType)
                    if VERBOSE:
                        print '   row:%04d,col:%04d'%(iRow,iCol),
                        #print '      ',nodeType
                        print sType,
                        print type
                        print '      ',edt
                        print '      ',rend
                        vtLog.vtLngCurWX(vtLog.DEBUG,'row:%d,col:%d;sType:%s;type:%s'%(
                                iRow,iCol,sType,type),self)
                    self.SetCellEditor(iRow,iCol,edt)
                    self.SetCellRenderer(iRow,iCol,rend)
                    if nodeType is None:
                        #gridlib.Grid.SetReadOnly(self,row,col)
                        self.SetReadOnly(iRow,iCol)
                        self.SetCellBackgroundColour(iRow,iCol,wx.LIGHT_GREY)
                        self.SetCellAlignment(iRow,iCol,wx.ALIGN_CENTRE,wx.ALIGN_CENTRE)
                        self.SetCellValue(iRow,iCol,'---')
                    else:
                        if tup2Sel is None:
                            tup2Sel=(iRow,iCol)
                        self.SetCellBackgroundColour(iRow,iCol,wx.WHITE)
                        self.SetCellAlignment(iRow,iCol,wx.ALIGN_RIGHT,wx.ALIGN_CENTRE)
                        self.SetReadOnly(iRow,iCol,False)
                        sVal=self.dataSrc.GetRowColValue(iRow,iCol)
                        val=self.__getVal__(sType,sVal)
                        sVal=self.__convVal2DispType(sType,val)
                        if VERBOSE:
                            print '      ',val,sVal
                        self.SetCellValue(iRow,iCol,sVal)
                    self.SetCellFont(iRow,iCol,font)
            #self.AutoSizeColumns()
            self.SetRowLabelAlignment(wx.ALIGN_LEFT,wx.ALIGN_CENTRE)
            self.SetRowLabelSize(self.lstColumnSize[0])
            for i in range(1,len(self.lstColumnSize)):
                try:
                    self.SetColSize(i-1,self.lstColumnSize[i])
                except:
                    pass
                
                #self.SelectBlock(tup2Sel[0],tup2Sel[1],tup2Sel[0],tup2Sel[1])
            if tup2Sel is not None:
                self.SetGridCursor(tup2Sel[0],tup2Sel[1])
        except:
            vtLog.vtLngTB(self.GetName())
        self.EndBatch()
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
        #self.SetColLabelSize(100)
        #self.table = gridAttrData(None,self.lstFilter)
        #self.SetTable(self.table, True)
        #gridlib.Grid.ClearGrid(self)
        #self.table = vtInputGridData(lstNodes,dataSrc)
        #self.SetTable(self.table, True)
        #self.SetTable(self.table, False)
        #self.SetReadOnly()
        #self.ForceRefresh()
    def GetRowColValueActual(self,iRow,iCol):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        try:
            return gridlib.Grid.GetCellValue(self,iRow,iCol)
        except:
            return ''
    def __getGridType__(self,doc,node,attrTyp):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if node is None:
            edt=gridlib.GridCellTextEditor()
            rend=gridlib.GridCellStringRenderer()
            datTyp=gridlib.GRID_VALUE_TEXT
        elif attrTyp=='datetime':
            datTyp=gridlib.GRID_VALUE_DATETIME
            edt=gridlib.GridCellTextEditor()
            rend=gridlib.GridCellStringRenderer()
        elif attrTyp=='int':
            datTyp=gridlib.GRID_VALUE_NUMBER
            #datTyp=gridlib.GRID_VALUE_FLOAT
            iDigits=8
            try:
                sMin=doc.getNodeText(node,'min')
                iMin=int(sMin)
            except:
                sMin=''
                iMin=0
            try:
                sMax=doc.getNodeText(node,'max')
                iMax=int(sMax)
            except:
                sMax=''
                iMax=0
            if len(sMin)>0 or len(sMax)>0:
                s=':'+sMin+','+sMax
                datTyp=datTyp+s
            else:
                pass
            #try:
            #    print iMin,iMax,
            #    print sMin,sMax,
            #    print datTyp
            #except:
            #    vtLog.vtLngTB(self.GetName())
            edt=gridlib.GridCellFloatEditor(iDigits,0)
            #edt=gridlib.GridCellNumberEditor(iMin,iMax)
            rend=gridlib.GridCellNumberRenderer()
        elif attrTyp=='float':
            datTyp=gridlib.GRID_VALUE_FLOAT
            try:
                sDigits=doc.getNodeText(node,'digits')
                iDigits=int(sDigits)
            except:
                sDigits=''
                iDigits=8
            try:
                sComma=doc.getNodeText(node,'comma')
                iComma=int(sComma)
            except:
                sComma=''
                iComma=2
            if len(sDigits)>0 and len(sComma)>0:
                s=':'+sDigits+','+sComma
                datTyp=datTyp+s
            else:
                pass
            edt=gridlib.GridCellFloatEditor(iDigits,iComma)
            rend=gridlib.GridCellFloatRenderer(iDigits,iComma)
        elif attrTyp=='long':
            datTyp=gridlib.GRID_VALUE_LONG
            iDigits=15
            try:
                sMin=doc.getNodeText(node,'min')
                iMin=int(sMin)
            except:
                sMin=''
                iMin=0
            try:
                sMax=doc.getNodeText(node,'max')
                iMax=int(sMax)
            except:
                sMax=''
                iMax=100
            if len(sMin)>0 or len(sMax)>0:
                s=':'+sMin+','+sMax
                datTyp=datTyp+s
            else:
                pass
            edt=gridlib.GridCellFloatEditor(iDigits,0)
            #edt=gridlib.GridCellNumberEditor(iMin,iMax)
            rend=gridlib.GridCellNumberRenderer()
        elif attrTyp=='choiceint':
            datTyp=gridlib.GRID_VALUE_CHOICEINT
            edt=gridlib.GridCellTextEditor()
            rend=gridlib.GridCellStringRenderer()
        elif attrTyp=='choice':
            datTyp=gridlib.GRID_VALUE_CHOICE
            keys=typeInfos.keys()
            keys.sort()
            choices=[]
            for k in keys:
                if k[:2]=='it':
                    choices.append(typeInfos[k])
            if len(choices)>0:
                datTyp=datTyp+':'+string.join(choices,',')
            edt=gridlib.GridCellChoiceEditor(string.join(choices,','))
            rend=gridlib.GridCellStringRenderer()
        elif attrTyp=='string':
            try:
                iLen=int(doc.getNodeText(node,'len'))
            except:
                iLen=-1
            datTyp=gridlib.GRID_VALUE_STRING
            edt=gridlib.GridCellTextEditor()
            if iLen>0:
                edt.SetParameters('%d'%iLen)
            rend=gridlib.GridCellStringRenderer()
        elif attrTyp=='text':
            try:
                iLen=int(doc.getNodeText(node,'len'))
            except:
                iLen=-1
            datTyp=gridlib.GRID_VALUE_TEXT
            edt=gridlib.GridCellTextEditor()
            if iLen>0:
                edt.SetParameters('%d'%iLen)
            rend=gridlib.GridCellStringRenderer()
        elif attrTyp=='bool':
            datTyp=gridlib.GRID_VALUE_BOOL
            edt=gridlib.GridCellBoolEditor()
            rend=gridlib.GridCellBoolRenderer()
        else:
            datTyp=gridlib.GRID_VALUE_STRING
            edt=gridlib.GridCellTextEditor()
            rend=gridlib.GridCellStringRenderer()
        return datTyp,edt,rend
    def __getGridTypeLimit__(self,doc,node,attrTyp):
        if node is None:
            return None,None
        if attrTyp=='datetime':
            return None,None
        elif attrTyp=='int':
            try:
                sMin=doc.getNodeText(node,'min')
                iMin=int(sMin)
            except:
                sMin=''
                iMin=None
            try:
                sMax=doc.getNodeText(node,'max')
                iMax=int(sMax)
            except:
                sMax=''
                iMax=None
            return iMin,iMax
        elif attrTyp=='float':
            try:
                sMin=doc.getNodeText(node,'min')
                iMin=float(sMin)
            except:
                sMin=''
                iMin=None
            try:
                sMax=doc.getNodeText(node,'max')
                iMax=float(sMax)
            except:
                sMax=''
                iMax=None
            return iMin,iMax
        elif attrTyp=='long':
            try:
                sMin=doc.getNodeText(node,'min')
                iMin=long(sMin)
            except:
                sMin=''
                iMin=None
            try:
                sMax=doc.getNodeText(node,'max')
                iMax=long(sMax)
            except:
                sMax=''
                iMax=None
            return iMin,iMax
        elif attrTyp=='bool':
            return None,None
        return None,None
    def __convVal2DispType(self,sType,val):
        try:
            if sType=='float':
                return vtLgBase.format('%4.2f',float(val))
            elif sType=='bool':
                if val in ['1','True','TRUE',True]:
                    return u'1'
                else:
                    #return u'0'        # 070412:wro causes failur on wx2.8
                    return u''
            return unicode(val)
        except:
            #vtLog.vtLngCurWX(vtLog.ERROR,'col:%d;val:%s;lSetup:%s'%(iCol,unicode(val),vtLog.pformat(self.lSetup)),self)
            #vtLog.vtLngTB(self.GetName())
            pass
        return unicode(val)
    def __convVal2StoreType(self,sType,val):
        try:
            if sType=='datetime':
                return val
            elif sType=='int':
                return unicode(val)
            elif sType=='long':
                return unicode(val)
            elif sType=='choiceint':
                return unicode(val)
            elif sType=='bool':
                if val:
                    return '1'
                else:
                    return '0'
            elif sType=='float':
                #return vtLgBase.format('%4.2f',float(val))
                return '%4.2f'%float(val)   # 070905:wro return non language specific value
            return unicode(val)
        except:
            #vtLog.vtLngCurWX(vtLog.ERROR,'col:%d;val:%s;lSetup:%s'%(iCol,unicode(val),vtLog.pformat(self.lSetup)),self)
            #vtLog.vtLngTB(self.GetName())
            pass
        return unicode(val)
    def __getVal__(self,attrTyp,val):
        try:
            if attrTyp=='datetime':
                return val
            elif attrTyp=='int':
                try:
                    return int(val)
                except:
                    return int(float(val))
            elif attrTyp=='float':
                return float(val)
            elif attrTyp=='long':
                try:
                    return long(val)
                except:
                    return long(float(val))
            elif attrTyp=='choiceint':
                return int(val)
            elif attrTyp=='choice':
                return val
            elif attrTyp=='string':
                return val
            elif attrTyp=='text':
                return val
            if attrTyp=='bool':
                if val=='0' or val=='False' or val=='':
                    return False
                else:
                    return True
            else:
                return val
            return val
        except:
            return ''
    def Get(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #dataNodes=self.table.GetDataNodes()
        if VERBOSE:
            vtLog.CallStack('')
        font = self.GetFont()
        for row in range(self.GetNumberRows()):
            for col in range(self.GetNumberCols()):
                if not self.IsReadOnly(row,col):
                    #node=dataNodes[row][col]
                    #if node is not None:
                    sVal=self.GetCellValue(row,col)
                    doc,nodeType,sType=self.dataSrc.GetRowColType(row,col)
                    val=self.__getVal__(sType,sVal)
                    s=self.__convVal2StoreType(sType,val)
                    #print 'r/c',row,col,sType
                    #print '   ',sVal,val,s
                    self.dataSrc.SetRowColValue(row,col,s)
                    gridlib.Grid.SetCellFont(self,row,col,font)
        self.ForceRefresh()
        return 1
    def Set(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if VERBOSE:
            vtLog.CallStack('')
        if self.CanEnableCellControl():
            self.DisableCellEditControl()
        #dataNodes=self.table.GetDataNodes()
        font = self.GetFont()
        font.SetWeight(wx.NORMAL)
        for row in range(self.GetNumberRows()):
            for col in range(self.GetNumberCols()):
                if node is not None:
                    #sVal=self.doc.getText(node)
                    #self.table.SetValue(row,col,sVal)
                    self.SetCellValue(row,col,sVal)
                    #print row,col,sVal
                    gridlib.Grid.SetCellFont(self,row,col,font)
        if len(dataNodes)>0:
            col=0
            for dataLstNode in dataNodes[0]:
                self.__calcSum__(col)
                col=col+1
        self.ForceRefresh()
        return 1
    def SetReadOnlyP(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #dataNodes=self.table.GetDataNodes()
        #dataTypes=self.table.GetDataTypes()
        #iRowCount=len(dataNodes)
        iRowCount=self.table.GetNumberRows()
        iColCount=self.table.GetNumberCols()
        bRowLabel=self.dataSrc.HasRowLabelChangeable()
        #if len(dataNodes)<=0:
        #    return
        if self.lstColumnSize:
            for i in range(len(self.lstColumnSize)):
                gridlib.Grid.SetColSize(self,i,self.lstColumnSize[i])
        for row in range(iRowCount):
            for col in range(iColCount):
                if self.dataSrc.GetDataNode(row,col) is None:
                    emptyEditor=gridlib.GridCellTextEditor()
                    emptyRenderer=gridlib.GridCellStringRenderer()
                    gridlib.Grid.SetCellEditor(self,row,col,emptyEditor)
                    gridlib.Grid.SetCellRenderer(self,row,col,emptyRenderer)
                    #if col>2:
                    #    gridlib.Grid.SetCellValue(self,row,col,'---')
                    gridlib.Grid.SetReadOnly(self,row,col)
                    gridlib.Grid.SetCellBackgroundColour(self,row,col,wx.LIGHT_GREY)
                    gridlib.Grid.SetCellAlignment(self,row,col,wx.ALIGN_CENTRE,wx.ALIGN_CENTRE)
                if bRowLabel:
                    if col==0 and row>0:
                        sVal=self.dataSrc.GetRowLabelValue(row)
                        self.SetCellValue(row,col,sVal)
        return 1
    def SetReadOnlyCols(self,cols):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        for col in cols:
            for row in range(self.GetNumberRows()):
                emptyEditor=gridlib.GridCellTextEditor()
                emptyRenderer=gridlib.GridCellStringRenderer()
                gridlib.Grid.SetCellEditor(self,row,col,emptyEditor)
                gridlib.Grid.SetCellRenderer(self,row,col,emptyRenderer)
                #if col>2:
                #    gridlib.Grid.SetCellValue(self,row,col,'---')
                gridlib.Grid.SetReadOnly(self,row,col)
                gridlib.Grid.SetCellBackgroundColour(self,row,col,wx.LIGHT_GREY)
                gridlib.Grid.SetCellAlignment(self,row,col,wx.ALIGN_CENTRE,wx.ALIGN_CENTRE)


#---------------------------------------------------------------------------

class TestFrame(wx.Frame):
    def __init__(self, parent, log):

        wx.Frame.__init__(
            self, parent, -1, "Custom Table, data driven Grid  Demo", size=(640,480)
            )

        p = wx.Panel(self, -1, style=0)
        grid = CustTableGrid(p, log)
        b = wx.Button(p, -1, "Another Control...")
        b.SetDefault()
        self.Bind(wx.EVT_BUTTON, self.OnButton, b)
        b.Bind(wx.EVT_SET_FOCUS, self.OnButtonFocus)
        bs = wx.BoxSizer(wx.VERTICAL)
        bs.Add(grid, 1, wx.GROW|wx.ALL, 5)
        bs.Add(b)
        p.SetSizer(bs)

    def OnButton(self, evt):
        print "button selected"

    def OnButtonFocus(self, evt):
        print "button focus"


#---------------------------------------------------------------------------

if __name__ == '__main__':
    import sys
    app = wx.PySimpleApp()
    frame = TestFrame(None, sys.stdout)
    frame.Show(True)
    app.MainLoop()


#---------------------------------------------------------------------------
