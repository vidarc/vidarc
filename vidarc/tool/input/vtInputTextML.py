#----------------------------------------------------------------------
# Name:         vtInputTextML.py
# Purpose:      multilanguage text control with popup window
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vtInputTextML.py,v 1.23 2012/04/07 23:46:04 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
from wx.lib.anchors import LayoutAnchors
import  wx.lib.scrolledpanel as scrolled

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.log.vtLog as vtLog
    from vidarc.tool.xml.vtXmlDomConsumer import *
    from vidarc.tool.xml.vtXmlDomConsumerLang import *
    
    from vidarc.tool.lang.vtLgSelector import vtLgSelector
    from vidarc.tool.lang.vtLgSelector import vEVT_VTLANG_SELECTION_CHANGED
    
    #import images
    
    if __name__ == "__main__":
        from vidarc.tool.xml.vtXmlDom import *
        import images_lang
        import sys
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

dValClipBoard=None

# defined event for vtInputTextML item changed
wxEVT_VTINPUT_TEXTML_CHANGED=wx.NewEventType()
vEVT_VTINPUT_TEXTML_CHANGED=wx.PyEventBinder(wxEVT_VTINPUT_TEXTML_CHANGED,1)
def EVT_VTINPUT_TEXTML_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTINPUT_TEXTML_CHANGED,func)
class vtInputTextMLChanged(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTINPUT_TEXTML_CHANGED(<widget_name>, self.OnChanged)
    """

    def __init__(self,obj,val):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTINPUT_TEXTML_CHANGED)
        self.val=val
    def GetValue(self):
        return self.val
    

class vtInputTextMLTransientPopup(wx.Dialog):
    def __init__(self, parent, size,style,doc,name=''):
        self.SIZE_PN_SCROLL=wx.SystemSettings.GetMetric(wx.SYS_VSCROLL_X)
        wx.Dialog.__init__(self, parent,style=wx.RESIZE_BORDER)#wx.BORDER_SIMPLE)#|wx.STAY_ON_TOP)
        id=wx.NewId()
        #wx.EVT_SIZE(self,self.OnSize)
        st = wx.StaticText(self, -1,name,pos=(70,4))
        self.cbCancel = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Cancel), name=u'cbCancel',
              parent=self, pos=(0,0), size=(30,30), style=wx.BU_AUTODRAW)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              self.cbCancel)
              
        self.cbApply = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Apply), name=u'cbApply',
              parent=self, pos=wx.Point(32, 00), size=wx.Size(30, 30),
              style=wx.BU_AUTODRAW)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              self.cbApply)
        self.cbClr = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Erase), name=u'cbClr',
              parent=self, pos=wx.Point(32, 00), size=wx.Size(30, 30),
              style=wx.BU_AUTODRAW)
        self.cbClr.Bind(wx.EVT_BUTTON, self.OnCbClrButton,
              self.cbClr)
        self.cbCopy = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Copy), name=u'cbCopy',
              parent=self, pos=wx.Point(32, 00), size=wx.Size(30, 30),
              style=wx.BU_AUTODRAW)
        self.cbCopy.Bind(wx.EVT_BUTTON, self.OnCbCopyButton,
              self.cbCopy)
        self.cbPaste = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Paste), name=u'cbPaste',
              parent=self, pos=wx.Point(32, 00), size=wx.Size(30, 30),
              style=wx.BU_AUTODRAW)
        self.cbPaste.Bind(wx.EVT_BUTTON, self.OnCbPasteButton,
              self.cbPaste)
        fgs=wx.FlexGridSizer(cols=10, vgap=4, hgap=4)
        fgs.AddWindow(self.cbCancel,0,border=4)
        fgs.AddWindow(self.cbApply,0,border=4)
        fgs.AddSpacer((8,16))
        fgs.AddWindow(self.cbClr,0,border=4)
        fgs.AddSpacer((8,16))
        fgs.AddWindow(self.cbCopy,0,border=4)
        fgs.AddWindow(self.cbPaste,0,border=4)
        
        if size[1]<22:
            size=(size[0],22)
        self.pn=None
        i=0
        iY=40
        iH=size[1]+4
        iSzHeight=0
        self.dTxt={}
        self.dVal={}
        self.lstMarkObjs=[]
        self.bBlock=False
        self.bMod=False
        self.bEnableMark=True
        bEditable=parent.txtVal.IsEditable()
        self.cbApply.Enable(bEditable)
        fgsMain=wx.FlexGridSizer(cols=2, vgap=4, hgap=4)
        fgsMain.AddSizer(fgs,1,wx.EXPAND)
        fgsMain.AddSpacer((16,16))
        self.txtRef=wx.TextCtrl(id=-1, name='txtRef',
                        parent=self, 
                        style=0, value='')
        self.viLangRef = vtLgSelector(id=-1,
              name=u'viLangRef', parent=self, pos=wx.Point(307, 102),
              size=wx.Size(30, 30), style=0)
        self.viLangRef.Bind(vEVT_VTLANG_SELECTION_CHANGED,self.OnViLangRefChanged)
        self.viLangRef.SetDoc(doc)
        fgsMain.Add(self.txtRef,1,border=4,flag=wx.EXPAND)
        fgsMain.Add(self.viLangRef,0,border=0,flag=wx.ALIGN_CENTRE)
        
        self.txtEdit=wx.TextCtrl(id=-1, name='txtEdit',
                        parent=self, 
                        style=0, value='')
        self.viLangEdit = vtLgSelector(id=-1,
              name=u'viLangEdit', parent=self, pos=wx.Point(307, 102),
              size=wx.Size(30, 30), style=0)
        self.viLangEdit.Bind(vEVT_VTLANG_SELECTION_CHANGED,self.OnViLangEditChanged)
        self.viLangEdit.SetDoc(doc)
        
        self.txtRef.SetEditable(bEditable)
        self.txtRef.Bind(wx.EVT_TEXT, self.OnTextTextRef,self.txtRef)
        self.lstMarkObjs.append(self.txtRef)
        self.txtEdit.SetEditable(bEditable)
        self.txtEdit.Bind(wx.EVT_TEXT, self.OnTextTextEdit,self.txtEdit)
        self.lstMarkObjs.append(self.txtEdit)
        
        fgsMain.Add(self.txtEdit,1,border=4,flag=wx.EXPAND)
        fgsMain.Add(self.viLangEdit,0,border=0,flag=wx.ALIGN_CENTRE)
        fgsMain.AddGrowableCol(0)
        #fgsMain.AddGrowableRow(1)
        #fgsMain.AddGrowableRow(2)
        self.SetSizer( fgsMain )
        self.SetAutoLayout(1)
        fgsMain.Fit(self)
        if doc is not None:
            langs=doc.GetLanguages()
            iCount=len(langs)
            if (iCount>1):
                self.viLangEdit.SetValue(langs[1][1])
    def OnCbClrButton(self,evt):
        try:
            lKey=self.dVal.keys()
            for sK in lKey:
                self.dVal[sK]=''
            self.OnViLangRefChanged(None)
            self.OnViLangEditChanged(None)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
    def OnCbCopyButton(self,evt):
        try:
            global dValClipBoard
            dValClipBoard=self.dVal.copy()
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
    def OnCbPasteButton(self,evt):
        try:
            global dValClipBoard
            if dValClipBoard is None:
                return
            self.dVal=dValClipBoard.copy()
            self.OnViLangRefChanged(None)
            self.OnViLangEditChanged(None)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
    def OnViLangRefChanged(self,evt):
        try:
            sLang=self.viLangRef.GetValue()
            self.txtRef.SetValue(self.dVal.get(self.viLangRef.GetValue(),''))
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
    def OnViLangEditChanged(self,evt):
        try:
            sLang=self.viLangEdit.GetValue()
            self.txtEdit.SetValue(self.dVal.get(self.viLangEdit.GetValue(),''))
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
    def SetEnableMark(self,flag):
        self.bEnableMark=flag
    def SetModified(self,flag):
        self.__markModified__(flag)
    def IsModified(self):
        return self.bMod
    def __clearBlock__(self):
        self.bBlock=False
    def __markModified__(self,flag=True,obj=None):
        if self.bEnableMark==False:
            return
        if VERBOSE>10:
            vtLog.vtLngCurWX(vtLog.DEBUG,'flag:%d'%(flag),self)
        def markObj(obj,flag):
            if flag:
                f=obj.GetFont()
                f.SetWeight(wx.FONTWEIGHT_BOLD)
                obj.SetFont(f)
            else:
                f=obj.GetFont()
                f.SetWeight(wx.FONTWEIGHT_NORMAL)
                obj.SetFont(f)
            obj.Refresh()
        if obj is not None:
            markObj(obj,flag)
        else:
            for obj in self.lstMarkObjs:
                markObj(obj,flag)
        self.bMod=flag
    def OnTextText(self,evt):
        if VERBOSE>10:
            vtLog.vtLngCurWX(vtLog.DEBUG,'bBlock:%d'%(self.bBlock),self)
        if self.bBlock==False:
            self.__markModified__(True,evt.GetEventObject())
        evt.Skip()
    def OnTextTextRef(self,evt):
        try:
            sLang=self.viLangRef.GetValue()
            sVal=self.txtRef.GetValue()
            self.dVal[sLang]=sVal
            if self.bBlock==False:
                self.__markModified__(True,evt.GetEventObject())
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
    def OnTextTextEdit(self,evt):
        try:
            sLang=self.viLangEdit.GetValue()
            sVal=self.txtEdit.GetValue()
            self.dVal[sLang]=sVal
            if self.bBlock==False:
                self.__markModified__(True,evt.GetEventObject())
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
    def OnSize(self,evt):
        iW,iH=self.GetSize()
        if iW<90:
            iW=90
        #if iH<45:
        #    iH=45
        #if iH>self.szOrig[1]:
        #    iH=self.szOrig[1]
        #if iH<self.szOrig[1]:
        #    iH=self.szOrig[1]
        iH=self.szOrig[1]
        self.SetSize((iW,iH))
        if self.pn is not None:
            sz=self.pn.GetSize()
            self.pn.SetSize((iW-8,sz[1]))
            #self.pn.SetSize((iW-8,iH))
            iW-=self.SIZE_PN_SCROLL
        iW-=32
        if self.pn is None:
            for bmp,txt in self.dTxt.values():
                sz=txt.GetSize()
                txt.SetSize((iW,sz[1]))
        else:
            doc=self.GetParent().__getDoc__()
            if doc is not None:
                langs=doc.GetLanguages()
            
                bmp,txt=self.dTxt[langs[0][1]]
                sz=txt.GetSize()
                txt.SetSize((iW,sz[1]))
        evt.Skip()
    def OnCbCancelButton(self,evt):
        if VERBOSE>10:
            vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
        self.Show(False)
    def OnCbApplyButton(self,evt):
        if VERBOSE>10:
            vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
        self.Apply()
        self.Show(False)
    def SetNode(self):
        if VERBOSE>10:
            vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
        self.bBlock=True
        par=self.GetParent()
        doc=par.__getDoc__()
        node=par.__getNode__()
        tagName=par.__getTagName__()
        if doc is not None and node is not None and tagName is not None:
            langs=doc.GetLanguages()
            iCount=len(langs)
            for tup in langs:
                sVal=doc.getNodeTextLang(node,tagName,tup[1]).split('\n')[0]
                self.dVal[tup[1]]=sVal
        else:
            for k in self.dVal.keys():
                self.dVal[k]=''
        self.__markModified__(False,None)
        wx.CallAfter(self.__clearBlock__)
    def Show(self,flag):
        try:
            if VERBOSE>10:
                vtLog.vtLngCurWX(vtLog.DEBUG,'flag:%d'%(flag),self)
            if flag:
                self.bBlock=True
                par=self.GetParent()
                doc=par.__getDoc__()
                node=par.__getNode__()
                tagName=par.__getTagName__()
                if doc is not None and node is not None and tagName is not None:
                    self.txtRef.SetValue(self.dVal.get(self.viLangRef.GetValue(),''))
                    self.txtEdit.SetValue(self.dVal.get(self.viLangEdit.GetValue(),''))
                else:
                    self.txtRef.SetValue('')
                    self.txtEdit.SetValue('')
                if doc is not None:
                    if par.IsModified():
                        self.__markModified__(True,self.txtRef)
                        self.__markModified__(True,self.txtEdit)
                wx.CallAfter(self.__clearBlock__)
            else:
                par=self.GetParent()
                #par.cbPopup.SetValue(False)
                par.__setPopupState__(False)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        wx.Dialog.Show(self,flag)
    def Apply(self):
        try:
            bDbg=False
            if VERBOSE>10:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'dVal:%s'%(vtLog.pformat(self.dVal)),self)
                    bDbg=True
            par=self.GetParent()
            doc=par.__getDoc__()
            node=par.__getNode__()
            tagName=par.__getTagName__()
            if bDbg:
                vtLog.vtLngCurWX(vtLog.DEBUG,'dVal:%s'%(vtLog.pformat(self.dVal)),self)
            par.__UpdateLang__()
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
    def GetNode(self,node=None):
        if node is None:
            node=self.node
        if node is None:
            return
        try:
            par=self.GetParent()
            doc=par.__getDoc__()
            node=par.__getNode__(node)
            tagName=par.__getTagName__()
            if VERBOSE>0:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'tagName:%s;IsModified:%s'%(tagName,
                            self.bMod),self)
            if doc is not None:
                langs=doc.GetLanguages()
                iCount=len(langs)
                if VERBOSE>10:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurWX(vtLog.DEBUG,'tagName:%s;langs:%s;dVal:%s'%(tagName,
                                vtLog.pformat(langs),vtLog.pformat(self.dVal)),self)
                for tup in langs:
                    sVal=self.dVal[tup[1]]
                    doc.setNodeTextLang(node,tagName,sVal,tup[1])
                doc.AlignNode(node)
            #par.UpdateLang()
            self.__markModified__(False,None)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
    def GetVal(self,lang):
        try:
            if VERBOSE>10:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'dVal:%s;lang:%s'%(vtLog.pformat(self.dVal),lang),self)
            return self.dVal[lang]
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
            return ''
    def SetVal(self,val,lang):
        try:
            if VERBOSE>10:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'val:%s;lang:%s;dVal:%s'%(val,lang,vtLog.pformat(self.dVal)),self)
            self.dVal[lang]=val
            self.txtRef.SetValue(self.dVal.get(self.viLangRef.GetValue(),''))
            self.txtEdit.SetValue(self.dVal.get(self.viLangEdit.GetValue(),''))
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
    def SetValAllLangs(self,val):
        try:
            bDbg=False
            if VERBOSE>10:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'val:%s;dVal:%s'%(val,vtLog.pformat(self.dVal)),self)
                    bDbg=True
            par=self.GetParent()
            doc=par.__getDoc__()
            if doc is not None:
                langs=doc.GetLanguages()
                iCount=len(langs)
                for tup in langs:
                    self.dVal[tup[1]]=val
                if bDbg:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'val:%s;dVal:%s'%(val,vtLog.pformat(self.dVal)),self)
                self.txtRef.SetValue(self.dVal.get(self.viLangRef.GetValue(),''))
                self.txtEdit.SetValue(self.dVal.get(self.viLangEdit.GetValue(),''))
            else:
                vtLog.vtLngCurWX(vtLog.ERROR,'doc is None'%(),self)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
class vtInputTextML(wx.Panel,vtXmlDomConsumer,vtXmlDomConsumerLang):
    def __init__(self,*_args,**_kwargs):
        vtXmlDomConsumer.__init__(self)
        vtXmlDomConsumerLang.__init__(self)
        try:
            sz=_kwargs['size']
        except:
            sz=wx.Size(100,40)
            _kwargs['size']=sz
        try:
            szCb=_kwargs['size_button']
            del _kwargs['size_button']
        except:
            szCb=wx.Size(31,30)
        try:
            szTxt=_kwargs['size_text']
            del _kwargs['size_text']
        except:
            szTxt=wx.Size(sz[0]-szCb[0]-2,22)
        
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        self.SetMinSize(wx.Size(-1, -1))
        #self.SetAutoLayout(True)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        size=(szTxt[0],szTxt[1])
        pos=(0,(sz[1]-szTxt[1])/2)
        self.txtVal = wx.TextCtrl(id=-1, name='txtVal',
              parent=self, pos=pos, size=size,
              style=0, value='')
        self.txtVal.SetMinSize(wx.Size(-1, -1))
        bxs.AddWindow(self.txtVal, 1, border=0, flag=wx.EXPAND|wx.ALL)
        #self.txtVal.SetConstraints(LayoutAnchors(self.txtVal, True, True,
        #      True, False))
        self.txtVal.Bind(wx.EVT_TEXT, self.OnTextText,self.txtVal)
        
        size=(szCb[0],szCb[1])
        pos=(szTxt[0]+(sz[0]-(szTxt[0]+szCb[0]))/2,(sz[1]-szCb[1])/2)
        id=wx.NewId()
        self.cbPopup = wx.lib.buttons.GenBitmapToggleButton(ID=id,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbPopup',
              parent=self, pos=pos, size=size, style=wx.BU_AUTODRAW)
        bxs.AddWindow(self.cbPopup, 0, border=0, flag=wx.ALIGN_CENTER)
        #self.cbPopup.SetConstraints(LayoutAnchors(self.cbPopup, False, True,
        #      True, False))
        self.SetSizer(bxs)
        self.cbPopup.SetBitmapLabel(vtArt.getBitmap(vtArt.LangML))
        self.cbPopup.SetBitmapSelected(vtArt.getBitmap(vtArt.LangML))
        self.cbPopup.Bind(wx.EVT_BUTTON,self.OnPopupButton,self.cbPopup)#id=id)
        self.popWin=None
        self.doc=None
        self.node=None
        self.tagName=None
        self.bBlock=False
        self.bMod=False
        self.bEnableMark=True
        self.bBusy=False
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
    def SetEditable(self,flag):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.txtVal.SetEditable(flag)
    def Enable(self,flag):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        wx.Panel.Enable(self,flag)
        self.cbPopup.Refresh()
    def SetEnableMark(self,flag):
        self.bEnableMark=flag
    def SetModified(self,flag):
        self.__markModified__(flag)
    def __markModified__(self,flag=True,store_mod=True):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.bEnableMark==False:
            return
        if VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'flag:%s,store_mod:%s;bMod:%s'%(flag,
                            store_mod,self.bMod),
                            self)
        if flag:
            f=self.txtVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_BOLD)
            self.txtVal.SetFont(f)
        else:
            f=self.txtVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
            self.txtVal.SetFont(f)
        if store_mod:
            self.bMod=flag
        self.txtVal.Refresh()
    def IsModified(self):
        return self.bMod
    def ClearLang(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.bBlock=True
        if VERBOSE>0:
            vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
        self.txtVal.SetValue('')
        wx.CallAfter(self.__clearBlock__)
    def UpdateLang(self):
        self.bBlock=True
        self.__markModified__(False)
        self.__UpdateLang__()
        wx.CallAfter(self.__clearBlock__)
    def __UpdateLang__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if VERBOSE>0:
            vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
        try:
            self.bBlock=True
            if self.popWin is None:
                sVal=self.doc.getNodeTextLang(self.node,self.tagName,None).split('\n')[0]
            else:
                sVal=self.popWin.GetVal(self.doc.GetLang())
                if self.popWin.IsModified():
                    self.__markModified__(True,True)
            self.txtVal.SetValue(sVal)
        except:
            vtLog.vtLngTB(self.GetName())
        wx.CallAfter(self.__clearBlock__)
    def Clear(self):
        if VERBOSE>0:
            vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
        self.ClearLang()
        #self.tagName=None
        self.node=None
    def __setPopupState__(self,flag):
        self.bBusy=flag
        self.cbPopup.SetValue(flag)
    def IsBusy(self):
        return self.bBusy
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.popWin is not None:
            return self.popWin.IsShown()
        else:
            return False
    def __Close__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.popWin is not None:
            self.popWin.Show(False)
            self.__setPopupState__(False)
    def Stop(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'Stop',self)
        wx.CallAfter(self.__Close__)
    def ClearDoc(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlDomConsumer.ClearDoc(self)
        vtXmlDomConsumerLang.ClearDoc(self)
    def SetDoc(self,doc):
        vtXmlDomConsumer.SetDoc(self,doc)
        vtXmlDomConsumerLang.SetDoc(self,doc)
    def SetTagName(self,tagName):
        self.tagName=tagName
    def SetNode(self,node,tagName=None):
        if VERBOSE>0:
            vtLog.vtLngCurWX(vtLog.DEBUG,'node:%r;tagName:%s'%(node,tagName),self)
        self.bBlock=True
        self.Clear()
        self.node=node
        if tagName is not None:
            self.tagName=tagName
        if self.doc is None:
            return
        if self.node is None:
            return
        if self.tagName is None:
            return
        if self.popWin is not None:
            self.popWin.SetNode()
        self.__UpdateLang__()
        self.__markModified__(False)
        wx.CallAfter(self.__clearBlock__)
    def GetNode(self,node=None):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.Close()
        if node is None:
            node=self.node
        if node is None:
            return
        try:
            sVal=self.txtVal.GetValue()
            bDbg=False
            if VERBOSE>0:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'sVal:%s;popWin:%d,IsModified:%s'%(sVal,
                            self.popWin is not None,self.IsModified()),self)
                    bDbg=True
            #self.doc.setNodeTextLang(node,self.tagName,sVal,None)
            if self.IsModified():
                if self.popWin is None:
                    langs=self.doc.GetLanguages()
                    iCount=len(langs)
                    for tup in langs:
                        self.doc.setNodeTextLang(node,self.tagName,sVal,tup[1])
                else:
                    self.popWin.GetNode(node)
            self.__markModified__(False)
        except:
            vtLog.vtLngTB(self.GetName())
    def SetValue(self,val,lang=None):
        try:
            if wx.Thread_IsMain()==False:
                vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
            bDbg=False
            if VERBOSE>0:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'sVal:%s;poWin:%d'%(sVal,self.popWin is not None),self)
                    bDbg=True
            self.txtVal.SetValue(val)
            if self.popWin is not None:
                if self.doc is not None:
                    langs=self.doc.GetLanguages()
                    if bDbg:
                        vtLog.vtLngCurWX(vtLog.DEBUG,'langs:%s'%(vtLog.pformat(langs)),self)
                    for tup in langs:
                        self.popWin.SetVal(val,tup[1])
        except:
            vtLog.vtLngTB(self.GetName())
    def Close(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.cbPopup.GetValue()==True:
            if self.popWin is not None:
                self.popWin.Show(False)
    def __clearBlock__(self):
        self.bBlock=False
    def __getDoc__(self):
        return self.doc
    def __getNode__(self,node=None):
        if node is None:
            return self.node
        else:
            return node
    def __getTagName__(self):
        return self.tagName
    def __createPopup__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.popWin is None:
            sz=self.GetSize()
            sz=(sz[0],sz[1]-20)
            self.popWin=vtInputTextMLTransientPopup(self,sz,wx.SIMPLE_BORDER,self.doc)
            self.popWin.SetEnableMark(self.bEnableMark)
            self.popWin.SetNode()
            sVal=self.txtVal.GetValue()
            if self.IsModified():
                if 1:       # 110110:wro consider single lang set
                    self.popWin.SetValAllLangs(sVal)
                else:
                    self.popWin.SetVal(sVal,self.doc.GetLang())
            else:
                self.popWin.SetVal(sVal,self.doc.GetLang())
        else:
            pass
    def OnTextText(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.bBlock:
            return
        sVal=self.txtVal.GetValue()
        if self.popWin is not None:
            if 1:       # 110110:wro consider single lang set
                self.popWin.SetValAllLangs(sVal)
            else:
                self.popWin.SetVal(sVal,self.doc.GetLang())
        bDbg=False
        if VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'sVal:%s;popWin:%d'%(sVal,self.popWin is not None),self)
                bDbg=True
        self.__markModified__(True)
        wx.PostEvent(self,vtInputTextMLChanged(self,sVal))
    def OnPopupButton(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.__createPopup__()
        if self.cbPopup.GetValue()==True:
            btn=self
            iX,iY = btn.ClientToScreen( (0,0) )
            iW,iH =  btn.GetSize()
            iPopW,iPopH=self.popWin.GetSize()
            iScreenW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            iScreenH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            if iX+iPopW>iScreenW:
                iX=iScreenW-iPopW-4
                if iX<0:
                    iX=0
            iY+=iH
            if iY+iPopH>iScreenH:
                iY=iScreenH-iPopH
                if iY<0:
                    iY=0
            
            self.popWin.Move((iX,iY))
            self.bBusy=True
            self.popWin.Show(True)
        else:
            self.popWin.Show(False)
            self.bBusy=False
        evt.Skip()
        
class TestFrame(wx.Frame):
    def __init__(self, parent, ID, title, pos=wx.DefaultPosition,
                 size=wx.DefaultSize, style=wx.DEFAULT_FRAME_STYLE):
        wx.Frame.__init__(self, parent, ID, title, pos, size, style)
        self.panel=wx.Panel(self,-1)
        self.inTxtML1=vtInputTextML(self.panel,id=-1,pos=(4,4))
        EVT_VTINPUT_TEXTML_CHANGED(self.inTxtML1,self.OnTxtMl1Changed)
        self.inTxtML2=vtInputTextML(self.panel,id=-1,pos=(4,40),size=(150,40))
        self.inTxtML3=vtInputTextML(self.panel,id=-1,pos=(4,80),size=(150,40))
        self.cbSave = wx.Button(id=-1,
              label='save', name=u'cbSave',
              parent=self.panel, pos=wx.Point(170, 4), size=wx.Size(60, 30),
              style=0)
        self.cbSave.Bind(wx.EVT_BUTTON, self.OnCbSaveButton,
              self.cbSave)
        self.cbApply = wx.Button(id=-1,
              label='apply', name=u'cbSave',
              parent=self.panel, pos=wx.Point(170, 40), size=wx.Size(60, 30),
              style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              self.cbApply)
        self.cbApply2 = wx.Button(id=-1,
              label='apply2', name=u'cbSave',
              parent=self.panel, pos=wx.Point(170, 80), size=wx.Size(60, 30),
              style=0)
        self.cbApply2.Bind(wx.EVT_BUTTON, self.OnCbApply2Button,
              self.cbApply2)
        self.cbApply3 = wx.Button(id=-1,
              label='apply3', name=u'cbSave',
              parent=self.panel, pos=wx.Point(170, 120), size=wx.Size(60, 30),
              style=0)
        self.cbApply3.Bind(wx.EVT_BUTTON, self.OnCbApply3Button,
              self.cbApply3)
        doc=vtXmlDom()
        doc.SetLanguages([
            ('english','en',images_lang.getLangEnBitmap()),
            ('german','de',images_lang.getLangDeBitmap()),
            ('french','fr',images_lang.getLangDeBitmap()),
            ('swedish','se',images_lang.getLangDeBitmap()),
            ])
        doc.SetLang('en')
        doc.Open('test/testML.xml')
        self.SetDoc(doc)
        self.zTime=10
        self.timer = wx.PyTimer(self.Notify)
        self.timer.Start(1000)
        self.Notify()
    def OnTxtMl1Changed(self,evt):
        vtLog.CallStack('')
        print evt.GetValue()
    def OnCbSaveButton(self,evt):
        node=self.doc.getChildByLst(self.doc.getRoot(),['root','input03'])
        #self.doc.AlignNode(node)
        self.doc.AlignNode(node,iRec=1)
        #self.doc.AlignNode(node,iRec=2)
        self.doc.Save('test/testML1.xml')
    def OnCbApplyButton(self,evt):
        self.inTxtML1.GetNode()
    def OnCbApply2Button(self,evt):
        self.inTxtML2.GetNode()
    def OnCbApply3Button(self,evt):
        self.inTxtML3.GetNode()
    def Notify(self):
        if self.zTime>=0:
            self.zTime-=1
            print 'notify:%3d'%self.zTime
            if self.zTime==0:
                self.doc.Open('test/testML2.xml')
                node=self.doc.getChildByLst(self.doc.getRoot(),['root','input01'])
                self.inTxtML1.SetNode(node,'name')
                self.inTxtML2.SetNode(node,'name')
                node=self.doc.getChildByLst(self.doc.getRoot(),['root','input03','input01','input02'])
                self.inTxtML3.SetNode(node,'name')
    def SetDoc(self,doc):
        self.doc=doc
        self.inTxtML1.SetDoc(doc)
        self.inTxtML2.SetDoc(doc)
        self.inTxtML3.SetDoc(doc)
        node=doc.getChildByLst(doc.getRoot(),['root','input01'])
        self.inTxtML1.SetNode(node,'name')
        node=doc.getChildByLst(doc.getRoot(),['root','input01'])
        self.inTxtML2.SetNode(node,'name')
        node=doc.getChildByLst(doc.getRoot(),['root','input03','input01','input02'])
        self.inTxtML3.SetNode(node,'name')
    def OnCloseMe(self, event):
        self.Close(True)
    def OnCloseWindow(self, event):
        self.Destroy()
    def getFileInfo(self):
        return self.panel
def runTest(frame, nb, log):
    win = TestPanel(nb, -1, log)
    return win


#---------------------------------------------------------------------------

def MessageDlg(self, message, type = 'Message'):
    dlg = wxMessageDialog(self, message, type, wxOK | wxICON_INFORMATION)
    dlg.ShowModal()
    dlg.Destroy()

#---------------------------------------------------------------------------

def RunStandalone():
    app = wx.PySimpleApp()
    frame = TestFrame(None, -1, "vtInputTextML Test Frame", size=(240, 300))
    frame.Show(True)
    app.MainLoop()
#----------------------------------------------------------------------------
if __name__ == "__main__":
    vtLog.vtLngInit('vtInputTextML','vtInputTextML.log',
            vtLog.DEBUG,iSockPort=60001)
    RunStandalone()


overview="""<html><body>
<h2>Calender</h2>

<p>show calender
"""
