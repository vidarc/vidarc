#----------------------------------------------------------------------------
# Name:         vtInputUnique.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060814
# CVS-ID:       $Id: vtInputUnique.py,v 1.5 2008/03/22 14:34:26 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from vidarc.tool.input.vtInputText import *

VERBOSE=0
if VERBOSE:
    import vidarc.tool.log.vtLog as vtLog
class vtInputUnique(vtInputText):
    def __init__(self,*_args,**_kwargs):
        vtInputText.__init__(self,*_args,**_kwargs)
        self.bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW)
        #self.reMatch=re.compile('[a-z]*[A-Z]*[0-9]*')
        #self.reMatch=re.compile('[a-zA-Z0-9]+')
        self.func=None
        self.args=()
        self.kwargs={}
    def SetUniqueFunc(self,func,*args,**kwargs):
        self.func=func
        self.args=args
        self.kwargs=kwargs
    def __markModified__(self,flag=True):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.bEnableMark==False:
            return
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'__markModified__;flag:%s'%(flag),
        #                self)
        if flag:
            f=self.txtVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_BOLD)
            self.txtVal.SetFont(f)
        else:
            f=self.txtVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
            self.txtVal.SetFont(f)
        self.__markValid__()
        self.bMod=flag
        self.txtVal.Refresh()
    def __markValid__(self,flag=False):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.func is None:
            return True
        if flag==False:
            sVal=self.txtVal.GetValue()
            bFlt=False
            try:
                if len(sVal)>0:
                    id=long(sVal)
                    if VERBOSE:
                        if self.bBlock:
                            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                    ids=self.func(*self.args,**self.kwargs)
                    if ids is not None:
                        if VERBOSE:
                            if self.bBlock:
                                vtLog.vtLngCurWX(vtLog.DEBUG,'keys:%s;ids:%s'%\
                                        (vtLog.pformat(ids.keys),vtLog.pformat(ids.ids)),self)
                        o,lbl,iID=ids.GetInfoById(id)
                        if o is None:
                            bFlt=False
                        else:
                            if self.doc.isSame(o,self.node):
                                bFlt=False
                            else:
                                bFlt=True
            except:
                vtLog.vtLngTB(self.GetName())
                bFlt=True
        else:
            bFlt=False
        if bFlt:
            self.txtVal.SetBackgroundColour("YELLOW")
        else:
            self.txtVal.SetBackgroundColour(self.bkgCol)
        return not bFlt
    def Clear(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if VERBOSE:
            vtLog.CallStack('')
        #self.tagName=None
        self.bBlock=True
        self.__markModified__(False)
        self.__markValid__(True)
        sVal=''
        if self.func is not None:
            ids=self.func(*self.args,**self.kwargs)
            if ids is not None:
                sVal=str(ids.GetFirstFreeId())
        self.txtVal.SetValue(sVal)
        self.node=None
        wx.CallAfter(self.__clearBlock__)
    def SetNode(self,node,tagName=None):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if VERBOSE:
            vtLog.CallStack('')
            print node
            print tagName
        self.bBlock=True
        self.Clear()
        self.__markModified__(False)
        wx.CallAfter(self.__clearBlock__)
        self.node=node
        if tagName is not None:
            self.tagName=tagName
        if self.doc is None:
            return
        if self.node is None:
            return
        if self.tagName is None:
            return
        sVal=self.doc.getAttribute(node,self.tagName)
        self.txtVal.SetValue(sVal)
        self.__markValid__()
    def GetNode(self,node=None):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if node is None:
            node=self.node
        if node is None:
            return
        try:
            sOld=self.doc.getAttribute(node,self.tagName)
            if VERBOSE:
                vtLog.CallStack('')
                print self.tagName,sOld
            if self.func is not None:
                ids=self.func(*self.args,**self.kwargs)
                if ids is not None:
                    ids.RemoveId(sOld)
            else:
                ids=None
            sVal=self.GetValue()
            self.__markModified__(False)
            self.doc.setAttribute(node,self.tagName,sVal)
            if VERBOSE:
                vtLog.CallStack('')
                print self.tagName,sVal
            if ids is not None:
                ids.AddNodeSingle(node,self.tagName,None,self.doc,True)
                if VERBOSE:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'keys:%s;ids:%s'%\
                            (vtLog.pformat(ids.keys),vtLog.pformat(ids.ids)),self)
        except:
            vtLog.vtLngTB(self.GetName())
    def SetValue(self,sVal):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.__markModified__(False)
        self.bBlock=True
        self.txtVal.SetValue(sVal)
        self.__markValid__()
        wx.CallAfter(self.__clearBlock__)
    def GetValue(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        sVal=self.txtVal.GetValue()
        try:
            if self.__markValid__():
                return sVal
            else:
                return self.doc.getAttribute(self.node,self.tagName)
        except:
            vtLog.vtLngTB(self.GetName())
            sVal=''
        return sVal
    def OnTextText(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.bBlock:
            return
        self.__markModified__()
        self.__markValid__()
        sVal=self.txtVal.GetValue()
        wx.PostEvent(self,vtInputTextChanged(self,sVal))
        
