#----------------------------------------------------------------------
# Name:         vtInputTextPopup.py
# Purpose:      text control with popup window
#
# Author:       Walter Obweger
#
# Created:      20060424
# CVS-ID:       $Id: vtInputTextPopup.py,v 1.7 2010/05/02 16:25:25 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomConsumer import *
from vidarc.tool.input.vtInputText import EVT_VTINPUT_TEXT_CHANGED
from vidarc.tool.input.vtInputText import vtInputTextChanged

import string
    
VERBOSE=0
    

class vtInputTextPopup(wx.Panel,vtXmlDomConsumer):
    def __init__(self,*_args,**_kwargs):
        vtXmlDomConsumer.__init__(self)
        try:
            sz=_kwargs['size']
        except:
            sz=wx.Size(76,24)
            _kwargs['size']=sz
        try:
            szCb=_kwargs['size_button']
            del _kwargs['size_button']
        except:
            szCb=wx.Size(24,24)
        
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        self.SetAutoLayout(True)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        self.txtVal = wx.TextCtrl(id=-1, name='txtVal',
              parent=self, pos=(0,0), size=sz,
              style=0, value='')
        bxs.AddWindow(self.txtVal, 1, border=0, flag=wx.EXPAND|wx.ALL)
        self.txtVal.Bind(wx.EVT_TEXT, self.OnTextText,self.txtVal)
        id=wx.NewId()
        self.cbPopup = wx.lib.buttons.GenBitmapToggleButton(ID=id,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbPopup',
              parent=self, pos=(76,0), size=szCb, style=wx.BU_AUTODRAW)
        bxs.AddWindow(self.cbPopup, 0, border=0, flag=wx.ALIGN_CENTER)
        self.cbPopup.SetBitmapLabel(vtArt.getBitmap(vtArt.Down))
        self.cbPopup.SetBitmapSelected(vtArt.getBitmap(vtArt.Down))
        self.cbPopup.Bind(wx.EVT_BUTTON,self.OnPopupButton,self.cbPopup)#id=id)
        self.cbPopup.Enable(False)
        #self.cbPopup.Show(False)
        
        self.SetSizer(bxs)
        
        self.popWin=None
        self.tagName=None
        self.bBlock=False
        self.bMod=False
        self.bEnableMark=True
        self.bBusy=False
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
    def SetPopupWin(self,win):
        """
        """
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.popWin=win
        self.cbPopup.Enable(self.txtVal.IsEnabled())
        self.bBusy=True
        self.cbPopup.Show(True)
    def Enable(self,flag):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if flag:
            self.txtVal.Enable(True)
            if self.popWin is not None:
                self.cbPopup.Enable(True)
        else:
            self.txtVal.Enable(False)
            self.cbPopup.Enable(False)
    def Close(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.bBusy=False
        if self.cbPopup.GetValue()==True:
            if self.popWin is not None:
                self.popWin.Show(False)
    def __setPopupState__(self,flag):
        self.bBusy=flag
        self.cbPopup.SetValue(flag)
    def IsBusy(self):
        return self.bBusy
    def SetEnableMark(self,flag):
        self.bEnableMark=flag
    def SetModified(self,flag):
        self.__markModified__(flag)
    def IsModified(self):
        return self.bMod
    def __markModified__(self,flag=True):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.bEnableMark==False:
            return
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'__markModified__;flag:%s'%(flag),
        #                self)
        if flag:
            f=self.txtVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_BOLD)
            self.txtVal.SetFont(f)
        else:
            f=self.txtVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
            self.txtVal.SetFont(f)
        self.bMod=flag
        self.txtVal.Refresh()
    def Clear(self):
        if VERBOSE:
            vtLog.CallStack('')
        self.__markModified__(False)
        #self.tagName=None
        self.node=None
    def __Close__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.popWin is not None:
            self.popWin.Show(False)
            self.__setPopupState__(False)
    def Stop(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'Stop',self)
        wx.CallAfter(self.__Close__)
    def SetDoc(self,doc):
        vtXmlDomConsumer.SetDoc(self,doc)
    def SetTagName(self,tagName):
        self.tagName=tagName
    def SetNode(self,node,tagName=None):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if VERBOSE:
            vtLog.CallStack('')
            print node
            print tagName
            
        self.bBlock=True
        self.Clear()
        self.__markModified__(False)
        wx.CallAfter(self.__clearBlock__)
        self.node=node
        if tagName is not None:
            self.tagName=tagName
        if self.doc is None:
            return
        if self.node is None:
            return
        if self.tagName is None:
            return
        sVal=self.doc.getNodeText(node,self.tagName).split('\n')[0]
        self.txtVal.SetValue(sVal)
    def GetNode(self,node=None):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if node is None:
            node=self.node
        if node is None:
            return
        try:
            sVal=self.txtVal.GetValue()
            self.__markModified__(False)
            self.doc.setNodeText(node,self.tagName,sVal)
        except:
            vtLog.vtLngTB(self.GetName())
    def SetValue(self,sVal,mod=False):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if mod==-1:
            pass
        else:
            self.SetModified(mod)
            if mod:
                self.__markModified__()
        self.bBlock=True
        self.txtVal.SetValue(sVal)
        wx.CallAfter(self.__clearBlock__)
    def GetValue(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        return self.txtVal.GetValue()
    def __clearBlock__(self):
        self.bBlock=False
    def __getDoc__(self):
        return self.doc
    def __getNode__(self,node=None):
        if node is None:
            return self.node
        else:
            return node
    def __getTagName__(self):
        return self.tagName
    def OnTextText(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.bBlock:
            return
        self.__markModified__()
        sVal=self.txtVal.GetValue()
        wx.PostEvent(self,vtInputTextChanged(self,sVal))
    def OnPopupButton(self,evt):
        evt.Skip()
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.popWin is None:
            return
        if self.cbPopup.GetValue()==True:
            btn=self
            iX,iY = btn.ClientToScreen( (0,0) )
            iW,iH =  btn.GetSize()
            iPopW,iPopH=self.popWin.GetSize()
            iScreenW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            iScreenH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            if iX+iPopW>iScreenW:
                iX=iScreenW-iPopW-4
                if iX<0:
                    iX=0
            iY+=iH
            if iY+iPopH>iScreenH:
                iY=iScreenH-iPopH
                if iY<0:
                    iY=0
            
            self.popWin.Move((iX,iY))
            #self.popWin.Move((pos[0],pos[1]+sz[1]))
            self.popWin.Show(True)
        else:
            self.popWin.Show(False)
