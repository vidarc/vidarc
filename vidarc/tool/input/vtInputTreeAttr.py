#----------------------------------------------------------------------
# Name:         vtInputTreeAttr.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120515
# CVS-ID:       $Id: vtInputTreeAttr.py,v 1.5 2015/02/27 18:49:12 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import vidarc.tool.lang.vtLgBase as vtLgBase
    from vidarc.tool.xml.vtXmlNodeGuiPanel import vtXmlNodeGuiPanel
    from vidarc.tool.xml.vtXmlGrpTreeReg import vtXmlGrpTreeReg
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vtInputTreeAttr(vtXmlNodeGuiPanel):
    VERBOSE=10
    def __initObj__(self,*args,**kwargs):
        try:
            vtXmlNodeGuiPanel.__initObj__(self,*args,**kwargs)
            
            bDbg=self.GetVerboseDbg(20)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
            
            self.trNode=None
        except:
            self.__logTB__()
    def __initCtrl__(self,*args,**kwargs):
        global _
        _=vtLgBase.assignPluginLang('vtInput')
        cls=kwargs.get('TreeClass',vtXmlGrpTreeReg)
        if kwargs.get('iOrientation',0)==1:
            lWid=[
                (cls,       (0,0),(1,1),{'name':'trNode',
                            'tip':'alias value','style':0,
                            #'guiStore':'trNode',
                            'pos':(-1,-1),'size':(-1,-1),
                            },{
                                'selected':self.OnTrNodeSel,
                            }),
                ('lstCtrl', (1,0),(1,1),{'name':'lstAttr',
                            'tip':_(u'attributes'),
                            'cols':[
                                (_(u'attr'),-1,80,1),
                                (_(u'value'),-1,-1,1)],
                            },{
                                'lst_item_sel':self.OnLstAttrSel,
                            }),
                #('cbBmpLbl',            (0,4),(1,1),{'name':'cbAliasApply','label':_(u'Apply'),'bitmap':'Apply',},
                #    {'btn':self.OnAliasApply}),
                #('szBoxVert',           (1,4),(4,1),{'name':'boxBt',},[
                #    ('cbBmpLbl',              0,4,{'name':'cbAliasApply1','label':_(u'Apply'),'bitmap':'Apply',},
                #        {'btn':self.OnAliasApply}),
                #    ]),
                ]
            lGrowRow=[
                (0,kwargs.get('lPropNode',1)),
                (1,kwargs.get('lPropAttr',1))]
            lGrowCol=[(0,1)]
        else:
            lWid=[
                (cls,       (0,0),(1,1),{'name':'trNode','style':0,
                            'tip':'alias value',
                            'pos':(-1,-1),'size':(-1,-1),
                            },{
                                'selected':self.OnTrNodeSel,
                            }),
                ('lstCtrl', (0,1),(1,1),{'name':'lstAttr',
                            'tip':_(u'attributes'),
                            'multiple_sel':False,
                            'cols':[
                                (_(u'attr'),-1,80,1),
                                (_(u'value'),-1,-1,1)],
                            },{
                                'lst_item_sel':self.OnLstAttrSel,
                            }),
                #('cbBmpLbl',            (0,4),(1,1),{'name':'cbAliasApply','label':_(u'Apply'),'bitmap':'Apply',},
                #    {'btn':self.OnAliasApply}),
                #('szBoxVert',           (1,4),(4,1),{'name':'boxBt',},[
                #    ('cbBmpLbl',              0,4,{'name':'cbAliasApply1','label':_(u'Apply'),'bitmap':'Apply',},
                #        {'btn':self.OnAliasApply}),
                #    ]),
                ]
            lGrowCol=[
                (0,kwargs.get('lPropNode',1)),
                (1,kwargs.get('lPropAttr',1))]
            lGrowRow=[(0,1)]
        vtXmlNodeGuiPanel.__initCtrl__(self,lWid=lWid,VERBOSE=VERBOSE)
        self.sAttr2Sel=None
        return lGrowRow,lGrowCol
    def GetWidMod(self):
        return [self.trNode,self.lstAttr.GetWidMod()]
    def OnTrNodeSel(self,evt):
        try:
            self.__logDebug__(''%())
            if evt is not None:
                iID=evt.GetTreeNodeIDNum()
            else:
                iID=None
            lD=[]
            if iID is not None:
                node=self.doc.getNodeByIdNum(iID)
                if node is not None:
                    dVal=self.doc.GetAttrValueToDict(node)#,lang=self.lang)
                    if dVal is None:
                        l=[]
                    else:
                        l=dVal.keys()
                    l.sort()
                    for sK in l:
                        lD.append([sK,[sK,dVal[sK]]])
            self.lstAttr.SetValue(lD)
            if self.sAttr2Sel is not None:
                self.__selAttr__()
        except:
            self.__logTB__()
    def OnLstAttrSel(self,evt):
        try:
            if self._iVerbose>0:
                self.__logDebug__(''%())
            #lVal=self.lstAttr.GetSelected([-1])
            #print self.trNode.GetSelectedIDNum(),self.trNode.GetSelectedID()
        except:
            self.__logTB__()
    def GetValue(self):
        try:
            if self._iVerbose>0:
                self.__logDebug__(''%())
            if self.doc.IsKeyValid(self.trNode.GetSelectedIDNum()):
                sID=self.trNode.GetSelectedID()
                lVal=self.lstAttr.GetSelected([-1])
                lV=[lK[0] for lK in lVal]
                if self._iVerbose>0:
                    self.__logDebug__({'sID':sID,'lVal':lVal,'lV':lV})
                return sID,lV
            else:
                return -1,None
        except:
            self.__logTB__()
    def SetValue(self,sID,sAttr):
        try:
            if self._iVerbose>0:
                self.__logDebug__({'sID':sID,'sAttr':sAttr})
            if self.trNode.GetSelectedID()!=sID:
                self.trNode.SelectByID(sID)
                self.sAttr2Sel=sAttr
            else:
                self.sAttr2Sel=sAttr
                self.__selAttr__()
        except:
            self.__logTB__()
    def __selAttr__(self):
        try:
            sAttr=self.sAttr2Sel
            self.sAttr2Sel=None
            l=[self.lstAttr.GetDataKey(sAttr)]
            l=self.lstAttr.FindValue([sAttr],[-1],[-2,-1])
            self.lstAttr.SetSelected(None)#self.lstAttr.GetSelected([-2]))
            ll=[v[0] for v in l]
            self.lstAttr.SetSelected(ll)
        except:
            self.__logTB__()
    def __Clear__(self):
        if self._iVerbose>0:
            self.__logDebug__(''%())
        # add code here
        self.sAttr2Sel=None
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if self._iVerbose>0:
            self.__logDebug__(''%())
        # add code here
        self.trNode.SetDoc(doc,bNet)
    def __SetNode__(self,node):
        try:
            if self._iVerbose>0:
                self.__logDebug__(''%())
            # add code here
            self.trNode.SetNode(node)
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if self._iVerbose>0:
                self.__logDebug__(''%())
            node=self.GetNodeStart(node)
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
                print node
            if node is None:
                return
            # add code here
        except:
            self.__logTB__()
    def __Close__(self):
        if self._iVerbose>0:
            self.__logDebug__(''%())
        # add code here
    def __Cancel__(self):
        if self._iVerbose>0:
            self.__logDebug__(''%())
        # add code here
    def __Lock__(self,flag):
        if self._iVerbose>0:
            self.__logDebug__(''%())
        if flag:
            # add code here
            pass
        else:
            # add code here
            pass
