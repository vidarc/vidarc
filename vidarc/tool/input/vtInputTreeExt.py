#----------------------------------------------------------------------
# Name:         vtInputTreeExt.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120527
# CVS-ID:       $Id: vtInputTreeExt.py,v 1.4 2015/07/24 08:08:07 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import vidarc.tool.lang.vtLgBase as vtLgBase
    from vidarc.tool.xml.vtXmlNodeGuiPanel import vtXmlNodeGuiPanel
    from vidarc.tool.xml.vtXmlGrpTreeReg import vtXmlGrpTreeReg
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vtInputTreeExt(vtXmlNodeGuiPanel):
    VERBOSE=10
    def __initCtrl__(self,*args,**kwargs):
        global _
        _=vtLgBase.assignPluginLang('vtInput')
        cls=kwargs.get('TreeClass',vtXmlGrpTreeReg)
        lWidExt=kwargs.get('lWid',None)
        try:
            bDbg=self.GetVerboseDbg(5)
            if bDbg:
                self.__logDebug__(kwargs)
            
        except:
            self.__logTB__()
        if kwargs.get('iOrientation',0)==1:
            lWid=[
                (cls,                   (0,0),(1,1),{'name':'trNode','tip':'alias value','style':0,
                                                    'controller':False,
                                                    'pos':(-1,-1),'size':(-1,-1)},{
                                                    'selected':self.OnTrNodeSel,
                                                    }),
                ]
            lGrowRow=[
                (0,kwargs.get('lPropNode',1)),
                ]
            if lWidExt is not None:
                lWid.append(
                    ('szBoxVert',       (1,0),(1,1),{'name':'boxExt',},lWidExt),
                )
                lGrowRow.append((1,kwargs.get('lPropAttr',1)))
            lGrowCol=[(0,1)]
        else:
            lWid=[
                (cls,                   (0,0),(1,1),{'name':'trNode','tip':'alias value','style':0,
                                                    'pos':(-1,-1),'size':(-1,-1)},{
                                                    'selected':self.OnTrNodeSel,
                                                    }),
                ]
            lGrowCol=[
                (0,kwargs.get('lPropNode',1)),
                ]
            if lWidExt is not None:
                lWid.append(
                    ('szBoxVert',       (0,1),(1,1),{'name':'boxExt',},lWidExt)
                    )
                lGrowCol.append((1,kwargs.get('lPropAttr',1)))
            lGrowRow=[(0,1)]
        vtXmlNodeGuiPanel.__initCtrl__(self,lWid=lWid,VERBOSE=VERBOSE)
        return lGrowRow,lGrowCol
    def __initCls__(self,*args,**kwargs):
        self.SetVerbose(__name__,iVerbose=VERBOSE,kwargs=kwargs)
    def GetWidMod(self):
        return [self.trNode,]
    def OnTrNodeSel(self,evt):
        try:
            self.__logDebug__(''%())
            if evt is not None:
                iID=evt.GetTreeNodeIDNum()
            else:
                iID=None
            lD=[]
            if iID is not None:
                self.Post('sel',iID)
            else:
                self.Post('sel',None)
        except:
            self.__logTB__()
    def GetValue(self):
        try:
            if self._iVerbose>0:
                self.__logDebug__(''%())
            if self.doc.IsKeyValid(self.trNode.GetSelectedIDNum()):
                sID=self.trNode.GetSelectedID()
                lV=[]
                return sID,lV
            else:
                return -1,None
        except:
            self.__logTB__()
    def SetValue(self,sID):
        try:
            if self._iVerbose>0:
                self.__logDebug__({'sID':sID})
            if self.trNode.GetSelectedID()!=sID:
                self.trNode.SelectByID(sID)
            else:
                pass
        except:
            self.__logTB__()
    def __Clear__(self):
        if self._iVerbose>0:
            self.__logDebug__(''%())
        # add code here
        self.Post('clr')
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if self._iVerbose>0:
            self.__logDebug__('doc is None=%d'%(doc is None))
        # add code here
        self.__logDebug__(''%())
        self.trNode.SetNodeInfos(['tag','name','|id'])
        self.trNode.SetDoc(doc,bNet)
        #self.Post('setDoc')
    def __SetNode__(self,node):
        try:
            if self._iVerbose>0:
                self.__logDebug__(''%())
            self.__logDebug__(''%())
            # add code here
            self.trNode.SetNode(node)
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if self._iVerbose>0:
                self.__logDebug__(''%())
            node=self.GetNodeStart(node)
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
                print node
            if node is None:
                return
            # add code here
        except:
            self.__logTB__()
    def __Close__(self):
        if self._iVerbose>0:
            self.__logDebug__(''%())
        # add code here
        self.Post('close')
    def __Cancel__(self):
        if self._iVerbose>0:
            self.__logDebug__(''%())
        # add code here
        self.Post('cancel')
    def __Lock__(self,flag):
        if self._iVerbose>0:
            self.__logDebug__(''%())
        if flag:
            # add code here
            pass
        else:
            # add code here
            pass
        self.Post('lock',flag)
