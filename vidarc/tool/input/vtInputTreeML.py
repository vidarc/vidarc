#----------------------------------------------------------------------
# Name:         vtInputTreeML.py
# Purpose:      text control with tree popup window
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vtInputTreeML.py,v 1.28 2010/04/10 15:21:06 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
from wx.lib.anchors import LayoutAnchors

import string,cStringIO

import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlTree import EVT_VTXMLTREE_THREAD_ADD_ELEMENTS
from vidarc.tool.xml.vtXmlTree import EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_ABORTED
from vidarc.tool.xml.vtXmlTree import EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED
from vidarc.tool.xml.vtXmlGrpTree import vtXmlGrpTree
#from vidarc.tool.xml.vtXmlGrpTree import *
from vidarc.tool.xml.vtXmlDomConsumer import *
from vidarc.tool.xml.vtXmlDomConsumerLang import *
    
VERBOSE=0

#----------------------------------------------------------------------
def getDownData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x83IDAT8\x8d\xb5\x92\xd1\r\x84 \x10D\xdf\xaa5l\x05\xd4y\xa2\xdfj\
\x85TA\x03\xde\x17fUH\xee\x02\xcc\x17\t\xcc\xcc\xdb\r"\xc3H\x8d\x86*70\xa5\
\xc3\xba\xf83\x84\xf0\x93IU\xd9\xf6C\x00\xc4\x8e\xb0.\xfe\x04(\x05\xa9*\xc0e\
\xbe\x11X9\xe7\xb0A\xc9\x98\x93<\x97\x98(\x92b\x8c\xb7{\xdb\x0e\r\x96\xf8\n\
\xf8\xcc^r\x0fs\xed}\x08J\x14\xb9\xf6~\x04M\x02\xec\x18%\xfc&\x04\xaf\x8f\
\xf4\xaf\xaa\t\xbe\x1el!\xed\xde\xc2%\x86\x00\x00\x00\x00IEND\xaeB`\x82' 

def getDownBitmap():
    return wx.BitmapFromImage(getDownImage())

def getDownImage():
    stream = cStringIO.StringIO(getDownData())
    return wx.ImageFromStream(stream)

# defined event for vtInputTextML item changed
wxEVT_VTINPUT_TREEML_CHANGED=wx.NewEventType()
vEVT_VTINPUT_TREEML_CHANGED=wx.PyEventBinder(wxEVT_VTINPUT_TREEML_CHANGED,1)
def EVT_VTINPUT_TREEML_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTINPUT_TREEML_CHANGED,func)
class vtInputTreeMLChanged(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTINPUT_TREEML_CHANGED(<widget_name>, self.OnChanged)
    """

    def __init__(self,obj,node,fid,val):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTINPUT_TREEML_CHANGED)
        self.node=node
        self.fid=fid
        self.val=val
    def GetNode(self):
        return self.node
    def GetId(self):
        return self.fid
    def GetValue(self):
        return self.val
    

class vtInputTreeMLTransientPopup(wx.Dialog):
    #SIZE_PN_SCROLL=30
    def __init__(self, parent, size,style,name=''):
        self.SIZE_PN_SCROLL=wx.SystemSettings.GetMetric(wx.SYS_VSCROLL_X)
        wx.Dialog.__init__(self, parent,style=wx.RESIZE_BORDER)#wx.BORDER_SIMPLE)#|wx.STAY_ON_TOP)
        id=wx.NewId()
        wx.EVT_SIZE(self,self.OnSize)
        st = wx.StaticText(self, -1,name,pos=(70,4))
        self.cbCancel = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Cancel), name=u'cbCancel',
              parent=self, pos=(0,0), size=(30,30), style=wx.BU_AUTODRAW)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              self.cbCancel)
              
        self.cbApply = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Apply), name=u'cbApply',
              parent=self, pos=wx.Point(32, 00), size=wx.Size(30, 30),
              style=wx.BU_AUTODRAW)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              self.cbApply)
        
        self.pbProg = wx.Gauge(id=-1, name='pbProg', parent=self,
              pos=wx.Point(70, 8), range=100, size=wx.Size(116, 12),
              style=wx.GA_SMOOTH | wx.GA_HORIZONTAL)
        self.pbProg.Show(False)
        self.bSelProc=False
        
        if size[1]<40:
            size=(size[0],40)
        i=0
        iY=40
        iH=size[1]+4
        iSzHeight=0
        self.trInfo=parent.__getTreeInstance__(parent=self,pos=(4,40),size=(192,196))
        #self.trInfo.SetBuildOnDemand(1)    # wro 060819
        self.trInfo.SetNotifyAddThread()
        try:
            wx.EVT_LEFT_DCLICK(self.trInfo, self.OnDblClickLeft)
        except:
            pass
        #print self.trInfo
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.trInfo,self.OnAdd)
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.trInfo,self.OnAddFinished)
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_ABORTED(self.trInfo,self.OnAddAborted)
        #self.trInfo.Bind(vEVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED,self.OnAddFinished,self.trInfo)
        self.szOrig=(200,240)
        self.SetSize(self.szOrig)

    def OnAdd(self,evt):
        try:
            iCount=evt.GetCount()
            if iCount>0:
                self.pbProg.SetRange(iCount)
                self.pbProg.Show(True)
            self.pbProg.SetValue(evt.GetValue())
        except:
            #vtLog.vtLngTB(self.GetValue())
            vtLog.vtLngTB(self.GetParent().GetName())
    def OnAddFinished(self,evt):
        try:
            if self.bSelProc:
                return
            self.bSelProc=True
            par=self.GetParent()
            self.trInfo.SelectByID(par.__getSelID__())
            self.pbProg.Show(False)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
    def OnAddAborted(self,evt):
        try:
            if self.bSelProc:
                return
            self.bSelProc=True
            par=self.GetParent()
            self.trInfo.SelectByID(par.__getSelID__())
            self.pbProg.Show(False)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
    def OnSize(self,evt):
        iW,iH=self.GetSize()
        if iW<self.szOrig[0]:
            iW=self.szOrig[0]
        #if iH<self.szMin[1]:
        #    iH=self.szMin[1]
        if iH<self.szOrig[1]:
            iH=self.szOrig[1]
        self.SetSize((iW,iH))
        if self.trInfo is not None:
            self.trInfo.SetSize((iW-16,iH-52))
        evt.Skip()
    def OnCbCancelButton(self,evt):
        self.Show(False)
    def OnCbApplyButton(self,evt):
        #self.Apply()
        try:
            par=self.GetParent()
            self.bBlock=True
            par.__apply__(self.trInfo.GetSelected())
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        self.Show(False)
    def OnDblClickLeft(self,evt):
        try:
            par=self.GetParent()
            par.__apply__(self.trInfo.GetSelected())
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        self.Show(False)
    def SetDocTree(self,doc,bNet):
        self.trInfo.SetDoc(doc,bNet)
    def SetNodeTree(self,node):
        if VERBOSE:
            vtLog.CallStack(node)
        self.bSelProc=False
        self.trInfo.SetNode(node)
    def SetNode(self,fid):
        #par=self.GetParent()
        #doc=par.__getDoc__()
        #node=par.__getNode__()
        try:
            self.trInfo.SelectByID(fid)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
    def Show(self,flag):
        try:
            par=self.GetParent()
            if flag:
                doc=par.__getDoc__()
                node=par.__getNode__()
                fid=par.__getSelID__()
                self.trInfo.SelectByID(fid)
            else:
                #par.cbPopup.SetValue(False)
                par.__setPopupState__(False)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        wx.Dialog.Show(self,flag)
    def Apply(self):
        try:
            par=self.GetParent()
            doc=par.__getDoc__()
            node=par.__getNode__()
            par.UpdateLang()
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
    def GetVal(self,lang):
        try:
            return ''
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
            return ''
    def SetVal(self,val,lang):
        try:
            pass
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
class vtInputTreeML(wx.Panel,vtXmlDomConsumer,vtXmlDomConsumerLang):
    def __init__(self,*_args,**_kwargs):
        vtXmlDomConsumer.__init__(self)
        vtXmlDomConsumerLang.__init__(self)
        try:
            sz=_kwargs['size']
        except:
            sz=wx.Size(100,30)
            _kwargs['size']=sz
        try:
            szCb=_kwargs['size_button']
            del _kwargs['size_button']
        except:
            szCb=wx.Size(24,24)
        try:
            szTxt=_kwargs['size_text']
            del _kwargs['size_text']
        except:
            szTxt=wx.Size(sz[0]-szCb[0]-2,sz[1]-4)
        try:
            self.trClass=_kwargs['tree_class']
            del _kwargs['tree_class']
        except:
            self.trClass=vtXmlGrpTree
        try:
            self.trSetNode=_kwargs['set_node']
            del _kwargs['set_node']
        except:
            self.trSetNode=None#self.__setNode__
        try:
            self.trGetNode=_kwargs['get_node']
            del _kwargs['get_node']
        except:
            self.trGetNode=None
        try:
            self.trGetId=_kwargs['get_id']
            del _kwargs['get_id']
        except:
            self.trGetId=None
        try:
            self.trShow=_kwargs['show_node_func']
            del _kwargs['show_node_func']
        except:
            self.trShow=None
        
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        self.SetAutoLayout(True)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        
        size=(szTxt[0],szTxt[1])
        pos=(0,(sz[1]-szTxt[1])/2)
        self.txtVal = wx.TextCtrl(id=-1, name='txtVal',
              parent=self, pos=pos, size=size,
              style=0, value='')
        #self.txtVal.SetConstraints(LayoutAnchors(self.txtVal, True, True,
        #      True, False))
        bxs.AddWindow(self.txtVal, 1, border=0, flag=wx.EXPAND|wx.ALL)
        self.txtVal.Bind(wx.EVT_TEXT, self.OnTextText,self.txtVal)
        
        size=(szCb[0],szCb[1])
        pos=(szTxt[0]+(sz[0]-(szTxt[0]+szCb[0]))/2,(sz[1]-szCb[1])/2)
        id=wx.NewId()
        self.cbPopup = wx.lib.buttons.GenBitmapToggleButton(ID=id,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbPopup',
              parent=self, pos=pos, size=size, style=wx.BU_AUTODRAW)
        #self.cbPopup.SetConstraints(LayoutAnchors(self.cbPopup, False, True,
        #      True, False))
        bxs.AddWindow(self.cbPopup, 0, border=0, flag=wx.ALIGN_CENTER)#wx.EXPAND)
        self.SetSizer(bxs)
        self.cbPopup.SetBitmapLabel(vtArt.getBitmap(vtArt.Down))
        self.cbPopup.SetBitmapSelected(vtArt.getBitmap(vtArt.Down))
        self.cbPopup.Bind(wx.EVT_BUTTON,self.OnPopupButton,self.cbPopup)#id=id)
        self.popWin=None
        self.doc=None
        self.docTreeTup=None
        self.nodeTree=None
        self.nodeSel=None
        self.fid=''
        self.tagName='name'
        self.tagNameInt='name'
        self.bBlock=False
        self.appl=None
        self.node=None
        self.bMod=False
        self.bEnableMark=True
        self.bBusy=False
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.__buildLangVals__()
    def __del__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        
        #vtLog.vtLngCur(vtLog.DEBUG,''%(),'del')
        try:
            if self.docTreeTup is not None:
                if self.docTreeTup[0] is not None:
                    self.docTreeTup[0].DelConsumer(self)
            self.docTreeTup=None
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def Enable(self,flag):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        
        wx.Panel.Enable(self,flag)
        self.cbPopup.Refresh()
    def SetAppl(self,appl):
        self.appl=appl
    def SetEnableMark(self,flag):
        self.bEnableMark=flag
    def __buildLangVals__(self):
        self.dVal={}
        if self.doc is not None:
            langs=self.doc.GetLanguages()
            iCount=len(langs)
            for tup in langs:
                self.dVal[tup[1]]=''
    def __clearLangVals__(self):
        for k in self.dVal.keys():
            self.dVal[k]=''
    def __markModified__(self,flag=True,store_mod=True):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        
        if self.bEnableMark==False:
            return
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'flag:%s'%(flag),
                        self)
        if flag:
            f=self.txtVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_BOLD)
            self.txtVal.SetFont(f)
        else:
            f=self.txtVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
            self.txtVal.SetFont(f)
        if store_mod:
            self.bMod=flag
    def SetModified(self,flag):
        self.__markModified__(flag)
    def IsModified(self):
        return self.bMod
    def SetTreeClass(self,trCls):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.trClass=trCls
    def SetTreeFunc(self,createFunc,showFunc):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.trClass=createFunc
        self.trShow=showFunc
    def SetTagNames(self,tagName,tagNameInt):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'tagname:%s,%s'%(tagName,tagNameInt),
                        self)
        self.tagName=tagName
        self.tagNameInt=tagNameInt
    def SetDocTree(self,doc,bNet=False):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if self.docTreeTup is not None:
            if self.docTreeTup[0] is not None:
                self.docTreeTup[0].DelConsumer(self)
        self.ClearTree()
        if doc is not None:
            doc.AddConsumer(self,self.ClearTree)
        self.docTreeTup=(doc,bNet)
        if self.popWin is not None:
            self.popWin.SetDoc(self.docTreeTup)
        self.__getSelNode__()
    def SetNodeTree(self,node):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'id:%s'%(self.docTreeTup[0].getKey(node)),
                        self)
        except:
            pass
        self.nodeTree=node
    def ClearDoc(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlDomConsumer.ClearDoc(self)
        vtXmlDomConsumerLang.ClearDoc(self)
        if self.docTreeTup is not None:
            if self.docTreeTup[0] is not None:
                self.docTreeTup[0].DelConsumer(self)
        self.docTreeTup=None
    def ClearTree(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.nodeSel=None
        self.nodeTree=None
        self.__markModified__(False)
    def ClearLang(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if VERBOSE:
            vtLog.CallStack('')
        self.bBlock=True
        self.txtVal.SetValue('')
        wx.CallAfter(self.__clearBlock__)
    def UpdateLang(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if VERBOSE:
            vtLog.CallStack('')
        try:
            #if self.popWin is None:
            #    sVal=self.doc.getNodeTextLang(self.node,self.tagName,None)
            #else:
            #    sVal=self.popWin.GetVal(self.doc.GetLang())
            sVal=self.dVal[self.doc.GetLang()]
            self.bBlock=True
            self.txtVal.SetValue(sVal.split('\n')[0])
            wx.CallAfter(self.__clearBlock__)
            #self.txtVal.SetValue(sVal)
        except:
            vtLog.vtLngTB(self.GetName())
    def Clear(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if VERBOSE:
            vtLog.CallStack('')
        self.ClearLang()
        self.__clearLangVals__()
        self.__markModified__(False)
        #self.tagName=None
        self.node=None
    def __setPopupState__(self,flag):
        self.bBusy=flag
        self.cbPopup.SetValue(flag)
    def IsBusy(self):
        return self.bBusy
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if self.popWin is not None:
            return self.popWin.IsShown()
        else:
            return False
    def __Close__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.popWin is not None:
            self.popWin.Show(False)
            self.__setPopupState__(False)
    def Stop(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'Stop',self)
        wx.CallAfter(self.__Close__)
    def SetDoc(self,doc):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlDomConsumer.SetDoc(self,doc)
        vtXmlDomConsumerLang.SetDoc(self,doc)
        self.__buildLangVals__()
    def SetNode(self,node):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        
        if VERBOSE:
            vtLog.CallStack('')
            print node
            
        self.Clear()
        self.node=node
        #self.tagName=tagName
        if self.doc is None:
            return
        if self.node is None:
            return
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'id:%s'%(self.doc.getKey(node)),
                        self)
        #if self.tagName is None:
        #    return
        try:
            langs=self.doc.GetLanguages()
            iCount=len(langs)
            for tup in langs:
                sVal=self.doc.getNodeTextLang(node,self.tagName,tup[1]).split('\n')[0]
                self.dVal[tup[1]]=sVal
            #sVal=string.split(self.doc.getNodeTextLang(self.node,self.tagName,None),'\n')[0]
            self.bBlock=True
            self.__markModified__(False)
            self.txtVal.SetValue(self.dVal[self.doc.GetLang()])
            wx.CallAfter(self.__clearBlock__)
        except:
            vtLog.vtLngTB(self.GetName())
        self.__getSelNode__()
        self.UpdateLang()
    def __clearBlock__(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.bBlock=False
    def __getSelNode__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        
        if VERBOSE:
            vtLog.CallStack('')
        try:
            if self.doc is None:
                return
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'id:%s'%(self.doc.getKey(self.node)),self)
            self.fid=self.doc.getForeignKey(self.doc.getChild(self.node,self.tagName),'fid',self.appl)
            if self.appl is not None and self.fid=='-3':
                self.fid=self.doc.getForeignKey(self.doc.getChild(self.node,self.tagName),'fid',None)
                vtLog.vtLngCurWX(vtLog.WARN,'fallback id:%s'%self.fid,self)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'fid:%s'%(self.fid),self)
            self.nodeSel=None
            try:
                if self.docTreeTup is not None:
                    docTree=self.docTreeTup[0]
                    if long(self.fid)>=0:
                        self.nodeSel=docTree.getNodeById(self.fid)
                else:
                    vtLog.vtLngCurWX(vtLog.WARN,'docTree is None',self)
                    return
            except:
                vtLog.vtLngCurWX(vtLog.WARN,'fid:%s'%(self.fid),self)
                #vtLog.vtLngTB(self.GetName())
            #print self.fid
            if self.nodeSel is not None:
                langs=self.doc.GetLanguages()
                iCount=len(langs)
                sVal=self.dVal[self.doc.GetLang()]
                for tup in langs:
                    sVal=self.doc.getNodeTextLang(self.nodeSel,self.tagNameInt,tup[1]).split('\n')[0]
                    self.dVal[tup[1]]=sVal
                sVal=self.txtVal.GetValue()
                sValNew=self.dVal[self.doc.GetLang()]
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'sVal:%s;sValNew:%s'%(sVal,sValNew),self)
                if sVal!=sValNew:
                    self.txtVal.SetValue(sValNew)
                    self.__markModified__(True)
                    wx.PostEvent(self,vtInputTreeMLChanged(self,self.nodeSel,self.fid,self.txtVal.GetValue()))
            else:
                #print self.dVal
                #self.__clearLangVals__()
                self.__markModified__(True)
                wx.PostEvent(self,vtInputTreeMLChanged(self,self.nodeSel,self.fid,self.txtVal.GetValue()))
                #self.__apply__(self.nodeSel)
                #if sVal!=self.txtVal.GetValue():
                #    self.__markModified__(True)
                #    wx.PostEvent(self,vtInputTreeMLChanged(self,self.nodeSel,self.fid,self.txtVal.GetValue()))
        except:
            vtLog.vtLngTB(self.GetName())
    def __setNode__(self,node):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'fid:%s;idSel:%s;id:%s'%\
                        (self.fid,self.doc.getKey(self.nodeSel),self.doc.getKey(node)),
                        self)
        try:
            docTree=self.docTreeTup[0]
            langs=self.doc.GetLanguages()
            if len(langs)>0:
                if self.nodeSel is None:
                    fid=None
                #    sVal=self.txtVal.GetValue()
                else:
                    fid=self.fid
                for tup in langs:
                    sVal=self.dVal[tup[1]]
                    #    sVal=string.split(docTree.getNodeTextLang(self.nodeSel,self.tagName,tup[1]),'\n')[0]
                    self.doc.setNodeTextLang(node,self.tagName,sVal,tup[1])
                    tmp=self.doc.getChildLang(node,self.tagName,tup[1])
                    if tmp is not None:
                        if fid is None:
                            self.doc.removeAttribute(tmp,'fid')
                        else:
                            self.doc.setForeignKey(tmp,attr='fid',attrVal=self.fid,appl=self.appl)
            #else:
            #    if self.nodeSel is None:
            #        sVal=self.txtVal.GetValue()
            #    else:
            #        langs=docTree.GetLanguages()
            #        if len(langs)>0:
            #            tup=langs[0]
            #            sVal=docTree.getNodeTextLang(self.nodeSel,self.tagName,tup[1])
            #        else:
            #            sVal=docTree.getNodeText(self.nodeSel,self.tagName)
            #    sVal=string.split(sVal,'\n')[0]
            #    self.doc.setNodeText(self.node,self.tagName,sVal)
        except:
            vtLog.vtLngTB(self.GetName())


    def GetNode(self,node=None):
        self.Close()
        if node is None:
            node=self.node
        if node is None:
            return
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'fid:%s;id:%s;idSel:%s'%\
                        (self.fid,self.doc.getKey(self.node),self.doc.getKey(self.nodeSel)),
                        self)
        if VERBOSE:
            vtLog.CallStack('')
            print self.nodeSel
            print self.node
        try:
            for c in self.doc.getChilds(node,self.tagName):
                self.doc.deleteNode(c,node)#self.doc.delNode(c)
            #self.doc.setAttribute(self.node,'fid',self.fid)
            self.__setNode__(node)
            
            self.__markModified__(False)
            self.doc.AlignNode(node)
        except:
            vtLog.vtLngTB(self.GetName())
    def Close(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        
        if self.cbPopup.GetValue()==True:
            if self.popWin is not None:
                self.popWin.Show(False)
    def __getDoc__(self):
        return self.doc
    def __getNode__(self):
        return self.node
    def __getSelID__(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'fid:%s'%self.fid,
                        self)
        if VERBOSE:
            vtLog.CallStack('')
        try:
            return self.fid
        except:
            return ''
    def __getTreeInstance__(self,*args,**kwargs):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'args:%s;kwargs:%s'%(args,kwargs),
                        self)
        if not kwargs.has_key('id'):
            kwargs['id']=-1
        if not kwargs.has_key('pos'):
            kwargs['pos']=(4,4)
        if not kwargs.has_key('size'):
            kwargs['size']=wx.DefaultSize
        if not kwargs.has_key('style'):
            kwargs['style']=0
        if not kwargs.has_key('name'):
            kwargs['name']='treeInstance'
        return self.trClass(*args,**kwargs)
    def __setNodeS__(self,doc,node):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'id:%s'%(self.doc.getKey(node)),
                        self)
        try:
            langs=self.doc.GetLanguages()
            iCount=len(langs)
            if len(langs)>0:
                for tup in langs:
                    sVal=self.doc.getNodeTextLang(node,self.tagName,tup[1]).split('\n')[0]
                    self.dVal[tup[1]]=sVal
            sVal=self.dVal[self.doc.GetLang()]
            fid=doc.getKey(node)
        except:
            sVal=''
            fid=-2
        #self.fid=fid
        #if self.node is not None:
        #    doc.setNodeText(self.node,self.tagName,sVal)
        #    tmp=doc.getChild(self.node,self.tagName)
        #    if fid<0:
        #        doc.removeAttribute(tmp,'fid')
        #    else:
        #        doc.setAttribute(tmp,'fid',fid)
        return sVal
    def __apply__(self,node):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'id:%s'%(self.doc.getKey(node)),
                        self)
        if VERBOSE:
            vtLog.CallStack(node)
        try:
            self.nodeSel=node
            self.fid=self.docTreeTup[0].getKey(node)
            if self.nodeSel is not None:
                langs=self.doc.GetLanguages()
                iCount=len(langs)
                for tup in langs:
                    sVal=self.doc.getNodeTextLang(self.nodeSel,self.tagNameInt,tup[1]).split('\n')[0]
                    self.dVal[tup[1]]=sVal
            else:
                self.__clearLangVals__()
            #if self.trSetNode is not None:
            #    sVal=self.trSetNode(self.docTreeTup[0],node)
            #else:
            #    sVal=''
            if self.trShow is not None:
                sVal=self.trShow(self.docTreeTup[0],node,self.doc.GetLang())
            else:
                try:
                    sVal=self.dVal[self.doc.GetLang()]
                except:
                    sVal=''
            if self.bBlock==False:
                self.__markModified__(True)
                wx.PostEvent(self,vtInputTreeMLChanged(self,self.nodeSel,self.fid,sVal))
        except:
            sVal=_(u'fault')
            vtLog.vtLngTB(self.GetName())
        self.bBlock=True
        self.txtVal.SetValue(sVal.split('\n')[0])
        wx.CallAfter(self.__clearBlock__)
        return sVal
        
    def __createPopup__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if self.popWin is None:
            sz=self.GetSize()
            sz=(sz[0],sz[1]-20)
            try:
                self.popWin=vtInputTreeMLTransientPopup(self,sz,wx.SIMPLE_BORDER)
                self.popWin.SetDocTree(self.docTreeTup[0],self.docTreeTup[1])
                self.popWin.SetNodeTree(self.nodeTree)
                if self.trSetNode is not None:
                    self.popWin.SetNode(self.trSetNode(self.doc,self.node))
            except:
                vtLog.vtLngTB(self.GetName())
            #self.popWin.SetVal(self.txtVal.GetValue(),self.doc.GetLang())
        else:
            pass
    def OnTextText(self,evt):
        try:
            if self.bBlock:
                return
            if self.doc is None:
                return
            self.nodeSel=None
            self.fid=''
            sVal=self.txtVal.GetValue()
            self.dVal[self.doc.GetLang()]=sVal
            self.__markModified__(True)
            wx.PostEvent(self,vtInputTreeMLChanged(self,self.nodeSel,self.fid,sVal))
        except:
            vtLog.vtLngTB(self.GetName())
    def GetValue(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%self.txtVal.GetValue(),
                        self)
        return self.txtVal.GetValue()
    def SetValueID(self,sVal,id):
        try:
            self.bBlock=True
            self.__markModified__(False)
            if self.docTreeTup is None:
                return
            node=self.docTreeTup[0].getNodeByIdNum(id)
            self.__apply__(node)
        except:
            vtLog.vtLngTB(self.GetName())
        wx.CallAfter(self.__clearBlock__)
    def GetID(self):
        return self.fid
    def GetForeignID(self):
        if self.appl is None:
            return self.fid
        else:
            return '@'.join([self.fid,self.appl])
    def OnPopupButton(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        
        self.__createPopup__()
        if self.cbPopup.GetValue()==True:
            btn=self
            iX,iY = btn.ClientToScreen( (0,0) )
            iW,iH =  btn.GetSize()
            iPopW,iPopH=self.popWin.GetSize()
            iScreenW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            iScreenH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            if iX+iPopW>iScreenW:
                iX=iScreenW-iPopW-4
                if iX<0:
                    iX=0
            iY+=iH
            if iY+iPopH>iScreenH:
                iY=iScreenH-iPopH
                if iY<0:
                    iY=0
            
            self.popWin.Move((iX,iY))
            #self.popWin.Move((pos[0],pos[1]+sz[1]))
            self.bBusy=True
            self.popWin.Show(True)
        else:
            self.popWin.Show(False)
            self.bBusy=False
        evt.Skip()
