#Boa:FramePanel:vtInputValue
#----------------------------------------------------------------------------
# Name:         vtInputValue.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060502
# CVS-ID:       $Id: vtInputValue.py,v 1.15 2011/08/26 09:38:17 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.masked.numctrl
import wx.lib.masked.maskededit
import vidarc.tool.time.vtTimeInputDateTime
import vidarc.tool.time.vtTimeTimeEdit
import vidarc.tool.time.vtTimeDateGM
import string
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.tool.log.vtLog as vtLog

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

wxEVT_VTINPUT_VALUE_CHANGED=wx.NewEventType()
vEVT_VTINPUT_VALUE_CHANGED=wx.PyEventBinder(wxEVT_VTINPUT_VALUE_CHANGED,1)
def EVT_VTINPUT_VALUE_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTINPUT_VALUE_CHANGED,func)
class vtInputValueChanged(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTINPUT_VALUE_CHANGED(<widget_name>, self.OnChanged)
    """

    def __init__(self,obj,text):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTINPUT_VALUE_CHANGED)
        self.text=text
    def GetText(self):
        return self.text

wxEVT_VTINPUT_VALUE_APPLY=wx.NewEventType()
vEVT_VTINPUT_VALUE_APPLY=wx.PyEventBinder(wxEVT_VTINPUT_VALUE_APPLY,1)
def EVT_VTINPUT_VALUE_APPLY(win,func):
    win.Connect(-1,-1,wxEVT_VTINPUT_VALUE_APPLY,func)
class vtInputValueApply(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTINPUT_VALUE_APPLY(<widget_name>, self.OnChanged)
    """

    def __init__(self,obj,text):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTINPUT_VALUE_APPLY)
        self.text=text
    def GetText(self):
        return self.text

class vtInputValue(wx.Panel):
    TYPES=['string','text','int','long','float','double','choice','choiceML',
            'date','time','datetime','email','creditcard','IP']
    TYPE_DICT={
        'string':['len','val','unit'],
        'text':['len','val','unit'],
        'email':['val'],
        'creditcard':['val'],
        'date':['val'],
        'time':['val'],
        'datetime':['val'],
        'IP':['val'],
        'int':['min','max','digits','val','unit'],
        'long':['min','max','digits','val','unit'],
        'float':['min','max','digits','comma','val','unit'],
        'double':['min','max','digits','comma','val','unit'],
        'choice':['it00','it01','it02','it04','val','unit'],
        'choiceML':['it00','it01','it02','it04','val','unit'],
        'pseudofloat':['min','max','digits','comma','val','unit'],
        }
    
    def __init__(self,*_args,**_kwargs):
        try:
            sizeunit=_kwargs['size_unit']
            del _kwargs['size_unit']
        except:
            sizeunit=wx.Size(60,-1)
        
        try:
            sz=_kwargs['size']
        except:
            sz=wx.Size(100,22)
            _kwargs['size']=sz
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        self.SetMinSize((-1,-1))
        self.SetAutoLayout(True)
        self.iSel=0
        self._bLock=False
        self.lang=None
        self.oldVal=None
        #print sz
        szIn=wx.Size(sz[0]-sizeunit[0],sz[1])
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW)
        id=wx.NewId()
        self.mskTxtValue = wx.lib.masked.textctrl.TextCtrl(id=id,
              name=u'mskText', parent=self, pos=wx.Point(0, 0),
              size=szIn, style=0, value='')
        self.mskTxtValue.SetMinSize((-1,-1))
        self.mskTxtValue.Bind(wx.EVT_TEXT,self.OnTxtChanged,id=id)
        #self.mskTxtValue.SetAutoLayout(True)
        self.mskTxtValue.SetAutoLayout(False)
        self.mskTxtValue.SetCtrlParameters(emptyBackgroundColour=bkgCol,
                #formatcodes='V>_',
                validBackgroundColour=bkgCol)
        self.mskTxtValue.Bind(wx.EVT_TEXT_ENTER, self.OnTxtApply,id=id)
        #self.mskTxtValue.SetAutoSize(False)
        #self.mskTxtValue.SetAutoSize(False)
        bxs.AddWindow(self.mskTxtValue, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        id=wx.NewId()
        self.txtValue = wx.TextCtrl(id=id,
              name=u'txtValue', parent=self, pos=wx.Point(0, 0),
              size=szIn, style=0, value='')
        self.txtValue.SetMinSize((-1,-1))
        self.txtValue.SetMaxSize((200,-1))
        self.txtValue.Bind(wx.EVT_TEXT,self.OnTxtChanged,id=id)
        self.txtValue.Bind(wx.EVT_TEXT_ENTER, self.OnTxtApply,id=id)
        #self.mskTxtValue.SetAutoLayout(True)
        bxs.AddWindow(self.txtValue, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        id=wx.NewId()
        decSep,thousSep=vtLgBase.getNumSeperatorsFromNumMask()
        self.mskNumValue = wx.lib.masked.numctrl.NumCtrl(id=id,
              name=u'mskNum', parent=self, pos=wx.Point(0, 0), size=szIn, style=0, value=0,
              decimalChar=decSep,groupChar=thousSep)
        self.mskNumValue.Bind(wx.EVT_TEXT,self.OnTxtChanged,id=id)
        self.mskNumValue.SetCtrlParameters(formatcodes='R>-,',
                                emptyBackgroundColour=bkgCol,
                                validBackgroundColour=bkgCol)
        #self.mskNumValue.Bind(wx.lib.masked.numctrl.EVT_MASKEDNUM,
        #      self.OnNumChanged, id=id)
        self.mskNumValue.SetMinSize((-1,-1))
        self.mskNumValue.SetMaxSize((200,-1))
        self.mskNumValue.SetAutoLayout(True)
        self.mskNumValue.SetAutoSize(False)
        self.mskNumValue.Show(False)
        self.mskNumValue.Bind(wx.EVT_TEXT_ENTER, self.OnTxtApply,id=id)
        #self.mskNumValue.SetDecimalChar(ld['decimal_point'])
        #self.mskNumValue.SetGroupChar(ld['grouping'])
        bxs.AddWindow(self.mskNumValue, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        id=wx.NewId()
        self.mskCmbValue = wx.lib.masked.combobox.ComboBox(choices=[],
              id=id, name=u'mskChoice', parent=self,
              pos=wx.Point(0, 0), size=szIn, style=0, value='')
        self.mskCmbValue.SetMinSize((-1,-1))
        self.mskCmbValue.SetMaxSize((200,-1))
        self.mskCmbValue.SetCtrlParameters(emptyBackgroundColour=bkgCol,
                #formatcodes='V>_',
                validBackgroundColour=bkgCol)
        self.mskCmbValue.Show(False)
        #self.mskCmbValue.SetAutoLayout(True)
        self.mskCmbValue.SetAutoLayout(False)
        #self.mskCmbValue.SetAutoSize(False)
        self.mskCmbValue.Bind(wx.EVT_TEXT_ENTER, self.OnTxtApply,id=id)
        bxs.AddWindow(self.mskCmbValue, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        id=wx.NewId()
        self.mskTimeValue = wx.lib.masked.timectrl.TimeCtrl(id=id,
              name=u'mskTimeValue', parent=self, pos=wx.Point(0,0),
              #size=wx.Size(size.GetWidth()-w,size.GetHeight()), style=0, value=0)
              size=szIn, style=0, value=0)
        self.mskTimeValue.SetMinSize((-1,-1))
        self.mskTimeValue.SetMaxSize((200,-1))
        self.mskTimeValue.SetCtrlParameters(emptyBackgroundColour=bkgCol,
                    #formatcodes='V>_',
                    validBackgroundColour=bkgCol)
        self.mskTimeValue.SetAutoLayout(False)
        self.mskTimeValue.Bind(wx.EVT_TEXT_ENTER, self.OnTxtApply,id=id)
        #self.mskTimeValue.SetAutoSize(False)
        bxs.AddWindow(self.mskTimeValue, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        id=wx.NewId()
        self.mskIPValue = wx.lib.masked.ipaddrctrl.IpAddrCtrl(id=id,
              name=u'mskIPValue', parent=self, pos=wx.Point(0, 0),
              #size=wx.Size(size.GetWidth()-w,size.GetHeight()), style=0, value=0)
              size=szIn, style=0, value=0)
        #self.mskIPValue.SetAutoSize(False)
        self.mskIPValue.SetMinSize((-1,-1))
        self.mskIPValue.SetMaxSize((200,-1))
        self.mskIPValue.SetCtrlParameters(emptyBackgroundColour=bkgCol,
                    #formatcodes='V>_',
                    validBackgroundColour=bkgCol)
        self.mskIPValue.SetAutoLayout(False)
        #self.mskIPValue.SetAutoSize(False)
        self.mskIPValue.Show(False)
        self.mskIPValue.Bind(wx.EVT_TEXT_ENTER, self.OnTxtApply,id=id)
        bxs.AddWindow(self.mskIPValue, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        self.viDate = vidarc.tool.time.vtTimeDateGM.vtTimeDateGM(id=wx.NewId(),
              parent=self, pos=wx.Point(0, 0), size=szIn,style=0)
        self.viDate.SetMinSize((-1,-1))
        self.viDate.Show(False)
        self.viDate.SetEnableMark(False)
        bxs.AddWindow(self.viDate, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        self.viTime = vidarc.tool.time.vtTimeTimeEdit.vtTimeTimeEdit(id=wx.NewId(),
              parent=self, pos=wx.Point(0, 0), size=szIn,style=0)
        self.viTime.SetMinSize((-1,-1))
        self.viTime.Show(False)
        self.viTime.SetEnableMark(False)
        bxs.AddWindow(self.viTime, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        self.viDtTm = vidarc.tool.time.vtTimeInputDateTime.vtTimeInputDateTime(id=wx.NewId(),
              name='viDtTm', parent=self, pos=wx.Point(0, 0),
              size=szIn, style=0)
        self.viDtTm.SetMinSize((-1,-1))
        self.viDtTm.Show(False)
        self.viDtTm.SetEnableMark(False)
        bxs.AddWindow(self.viDtTm, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        self.lblUnit = wx.StaticText(id=wx.NewId(),
              label=u'[unit]', name=u'lblUnit', parent=self, pos=wx.Point(szIn[0]+4,
              4), size=sizeunit, style=0)
        self.lblUnit.SetMinSize((-1,-1))
        bxs.AddWindow(self.lblUnit, 0, border=4, flag=wx.EXPAND|wx.LEFT|wx.RIGHT)
        self.SetSizer(bxs)
        
        self.Show(True,0)
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
    def Show(self,state,num=0,unit=False):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if VERBOSE>4:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,
                    'state:%d;num:%d;unit:%d'%(state,num,unit),self)
        #self.strValue.Show(state)
        #self.numValue.Show(False)
        if num==0:
            self.mskTxtValue.Show(state)
        else:
            self.mskTxtValue.Show(False)
        if num==1:
            self.mskCmbValue.Show(state)
        else:
            self.mskCmbValue.Show(False)
        if num==2:
            self.mskTimeValue.Show(state)
        else:
            self.mskTimeValue.Show(False)
        if num==3:
            self.mskIPValue.Show(state)
        else:
            self.mskIPValue.Show(False)
        if num in [4,5]:
            self.mskNumValue.Show(state)
        else:
            self.mskNumValue.Show(False)
        if num==6:
            self.viDate.Show(state)
        else:
            self.viDate.Show(False)
        if num==7:
            self.viTime.Show(state)
        else:
            self.viTime.Show(False)
        if num==8:
            self.viDtTm.Show(state)
        else:
            self.viDtTm.Show(False)
        if num==9:
            self.txtValue.Show(state)
        else:
            self.txtValue.Show(False)
        self.lblUnit.Show(unit)
        self.Layout()
    def Clear(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if VERBOSE>4:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
        self.iSel=0
        self.oldVal=None
        self.mskTxtValue.ClearValue()
        self.txtValue.SetValue('')
        self.mskNumValue.ClearValue()
        self.mskCmbValue.ClearValue()
        self.mskTimeValue.ClearValue()
        self.mskIPValue.ClearValue()
        self.viDate.Clear()
        self.viTime.Clear()
        self.viDtTm.Clear()
        self.Show(True,0)
        #self.lblUnit.SetLabel('')
    def Close(self):
        self.viDate.__Close__()
        self.viTime.__Close__()
        self.viDtTm.__Close__()
    def Lock(self,flag):
        self._bLock=flag
    def Enable(self,state):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #vtLog.vtLngCurWX(vtLog.DEBUG,'flag:%d,_bEnable:%d'%(state,self._bEnable),self)
        if self._bLock==True:
            state=False
        if self.iSel==0:
            self.mskTxtValue.Enable(state)
        if self.iSel==1:
            self.mskCmbValue.Enable(state)
        if self.iSel==2:
            self.mskTimeValue.Enable(state)
        if self.iSel==3:
            self.mskIPValue.Enable(state)
        if self.iSel==4:
            self.mskNumValue.Enable(state)
        if self.iSel==5:
            self.mskNumValue.Enable(state)
        if self.iSel==6:
            self.viDate.Enable(state)
        if self.iSel==7:
            self.viTime.Enable(state)
        if self.iSel==8:
            self.viDtTm.Enable(state)
        if self.iSel==9:
            self.txtValue.Enable(state)
    def SetValue(self,val):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #self.oldVal=val
        if self.iSel==0:
            iLen=len(self.mskTxtValue.GetMask())
            self.mskTxtValue.SetValue(val[:iLen])
            wx.CallAfter(self.mskTxtValue.SetInsertionPoint,0)
        if self.iSel==1:
            self.mskCmbValue.SetValue(val)
        if self.iSel==2:
            self.mskTimeValue.SetValue(val)
        if self.iSel==3:
            self.mskIPValue.SetValue(val)
        if self.iSel==4:
            self.mskNumValue.SetValue(val)
        if self.iSel==5:
            self.mskNumValue.SetValue(val)
        if self.iSel==6:
            self.viDate.SetValue(val)
        if self.iSel==7:
            self.viTime.SetValueStr(val)
        if self.iSel==8:
            self.viDtTm.SetValue(val)
        if self.iSel==9:
            self.txtValue.SetValue(val)
    def SetValueStr(self,val,bModOnFlt=True):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        try:
            #self.oldVal=val
            if self.iSel==0:
                iLen=len(self.mskTxtValue.GetMask())
                self.mskTxtValue.SetValue(val[:iLen])
                wx.CallAfter(self.mskTxtValue.SetInsertionPoint,0)
            if self.iSel==1:
                self.mskCmbValue.SetValue(val)
            if self.iSel==2:
                self.mskTimeValue.SetValue(val)
            if self.iSel==3:
                self.mskIPValue.SetValue(val)
            if self.iSel==4:
                self.mskNumValue.SetValue(long(val))
            if self.iSel==5:
                self.mskNumValue.SetValue(float(val))
            if self.iSel==6:
                self.viDate.SetValueStr(val)
            if self.iSel==7:
                self.viTime.SetValue(val)
            if self.iSel==8:
                self.viDtTm.SetValue(val)
            if self.iSel==9:
                self.txtValue.SetValue(val)
        except:
            if bModOnFlt:
                vtLog.vtLngTB(self.GetName())
                self.iSel=0
                self.SetValueStr(val,False)
            else:
                vtLog.vtLngCurWX(vtLog.WARN,'error setting val:%s for input:%d'%
                        (val,self.iSel),self)
                vtLog.vtLngTB(self.GetName(),True)
    def GetValue(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.iSel==0:
            return self.mskTxtValue.GetValue()
        if self.iSel==1:
            return self.mskCmbValue.GetValue()
        if self.iSel==2:
            return self.mskTimeValue.GetValue()
        if self.iSel==3:
            return self.mskIPValue.GetValue()
        if self.iSel==4:
            return self.mskNumValue.GetValue()
        if self.iSel==5:
            return self.mskNumValue.GetValue()
        if self.iSel==6:
            return self.viDate.GetValue()
        if self.iSel==7:
            return self.viTime.GetValue()
        if self.iSel==8:
            return self.viDtTm.GetValue()
        if self.iSel==9:
            return self.txtValue.GetValue()
    def GetValueStr(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.iSel==0:
            return self.mskTxtValue.GetValue()
        if self.iSel==1:
            return self.mskCmbValue.GetValue()
        if self.iSel==2:
            return self.mskTimeValue.GetValue()
        if self.iSel==3:
            return self.mskIPValue.GetValue()
        if self.iSel==4:
            try:
                return self.fmt%self.mskNumValue.GetValue()
            except:
                return str(self.mskNumValue.GetValue())
        if self.iSel==5:
            try:
                return self.fmt%self.mskNumValue.GetValue()
            except:
                return str(self.mskNumValue.GetValue())
        if self.iSel==6:
            return self.viDate.GetValueStr()
        if self.iSel==7:
            return self.viTime.GetValueStr()
        if self.iSel==8:
            return self.viDtTm.GetValue()
        if self.iSel==9:
            return self.txtValue.GetValue()
    def Set(self,dict):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.oldVal=None
        try:
            sType=dict['type']
        except:
            sType='text'
        try:
            sUnit=dict['unit']
        except:
            sUnit=''
        if sType in ['string']:
            self.iSel=0
            try:
                iLen=int(dict['len'])
            except:
                iLen=40
            sMask='X{%d}'%iLen
            self.mskTxtValue.ClearValue()
            #self.mskTxtValue.SetCtrlParameters(mask=sMask,formatcodes='_',validRegex='')
            self.mskTxtValue.SetCtrlParameters(mask=sMask,formatcodes='V>_',validRegex='')
            #sMask=''#'N{%d}'%iLen
            #self.mskTxtValue.SetCtrlParameters(mask=sMask,formatcodes='V>',validRegex='[a-zA-Z0-9]+')
            #self.mskTxtValue.SetMask(sMask)
            #self.mskTxtValue.SetFormatcodes('_')
            
            #self.mskTxtValue.Show(True)
            #self.mskTxtValue.SetDefaultValue(dict['val'])
            try:
                self.mskTxtValue.SetValue(dict['val'][:iLen])
            except:
                self.mskTxtValue.SetValue('')
            wx.CallAfter(self.mskTxtValue.SetInsertionPoint,0)
        elif sType in ['text']:
            self.iSel=9
            try:
                iLen=int(dict['len'])
            except:
                iLen=20
            try:
                self.txtValue.SetValue(dict['val'][:iLen])
            except:
                self.txtValue.SetValue('')
        elif sType in ['email','creditcard']:
            self.iSel=0
            sMask='X{100}'
            iLen=100
            self.mskTxtValue.ClearValue()
            if sType=='email':
                sMask='X{46}'
                iLen=46
                #self.mskTxtValue.SetCtrlParameters(autoformat='EMAIL')
                self.mskTxtValue.SetCtrlParameters(mask=sMask,formatcodes='V>',autoformat='EMAIL')
            elif sType=='creditcard':
                sMask="####-####-####-####"
                iLen=len(sMask)
                #self.mskTxtValue.SetCtrlParameters(autoformat='CREDITCARD')
                self.mskTxtValue.SetCtrlParameters(mask=sMask,autoformat='CREDITCARD')
            try:
                self.mskTxtValue.SetValue(dict['val'][:iLen])
            except:
                self.mskTxtValue.SetValue('')
            wx.CallAfter(self.mskTxtValue.SetInsertionPoint,0)
        elif sType in ['int','long']:
            self.iSel=4
            #self.mskValue.Show(True)
            try:
                sMin=long(dict['min'])
            except:
                sMin=None
            self.mskNumValue.SetMin(sMin)
            try:
                sMax=long(dict['max'])
            except:
                sMax=None
            self.mskNumValue.SetMax(sMax)
            try:
                iIntW=int(dict['digits'])
            except:
                iIntW=8
            self.mskNumValue.SetIntegerWidth(iIntW)
            #sMask='#{%d}'%iIntW
            #self.mskNumValue.SetMask(sMask)
            iFriW=0
            if sType=='int':
                self.fmt='%'+'%d'%(iIntW)+'d'
            elif sType=='long':
                self.fmt='%'+'%d'%(iIntW)+'d'
            else:
                self.fmt='%d'                
            self.mskNumValue.SetFractionWidth(iFriW)
            try:
                self.mskNumValue.SetValue(long(dict['val']))
            except:
                sVal=sMin or sMax
                if sVal is None:
                    self.mskNumValue.SetValue(0)
                else:
                    self.mskNumValue.SetValue(sVal)
        elif sType in ['float','double','pseudofloat']:
            self.iSel=5
            #self.mskValue.Show(True)
            try:
                sMin=float(dict['min'])
            except:
                sMin=None
            self.mskNumValue.SetMin(sMin)
            try:
                sMax=float(dict['max'])
            except:
                sMax=None
            self.mskNumValue.SetMax(sMax)
            try:
                iIntW=int(dict['digits'])
            except:
                iIntW=8
            self.mskNumValue.SetIntegerWidth(iIntW)
            try:
                iFriW=int(dict['comma'])
            except:
                iFriW=2
            self.mskNumValue.SetFractionWidth(iFriW)
            #sMask='#{%d}.#{%d}'%(iIntW,iFriW)
            self.mskNumValue.ClearValue()
            #self.mskTxtValue.SetMask(sMask)
            #self.mskTxtValue.SetFormatcodes('-_,._>')
            self.mskNumValue.SetDefaultValue(0.0)
            self.fmt='%'+'%d'%iIntW+'.'+'%d'%iFriW+'f'
            #self.numValue.SetFractionWidth(iFriW)
            #self.mskNumValue.
            try:
                self.mskNumValue.SetValue(float(dict['val']))
            except:
                #self.mskTxtValue.SetValue('0.0')
                pass
        elif sType in ['choice','choiceML']:
            self.iSel=1
            keys=dict.keys()
            keys.sort()
            lst=[]
            iLen=0
            for k in keys:
                if k[:2]=='it':
                    lst.append(dict[k])
                    iL=len(dict[k])
                    if iL>iLen:
                        iLen=iL
            sMask='*{%d}'%iLen
            self.mskCmbValue.ClearValue()
            self.mskCmbValue.SetCtrlParameters(choices=lst,mask=sMask,)
            #self.mskCmbValue.SetChoices(lst)
            #self.mskCmbValue.SetMask(sMask)
            try:
                #print dict['val']
                #print dict['it%02d'%int(dict['val'])]
                #self.mskCmbValue.SetValue(dict['it%02d'%int(dict['val'])])
                self.mskCmbValue.SetValue(dict['val'])
            except:
                try:
                    #print dictType['val']
                    #print dictType['it%02d'%int(dictType['val'])]
                    #self.mskCmbValue.SetValue(dictType['it%02d'%int(dictType['val'])])
                    self.mskCmbValue.SetValue(dict['it%02d'%int(dictType['val'])])
                except:
                    self.mskCmbValue.SetValue(dict['it00'])
        elif sType in ['IP']:
            self.iSel=3
            self.mskIPValue.ClearValue()
        elif sType in ['date']:
            self.iSel=6
            self.viDate.Clear()
        elif sType in ['time']:
            self.iSel=7
            self.viTime.Clear()
        elif sType in ['datetime']:
            self.iSel=8
            self.viDtTm.Clear()
        self.lblUnit.SetLabel(sUnit)
        #print 'unit',self.lblUnit.GetBestSize()
        #self.lblUnit.Show(True)
        self.Show(True,self.iSel,len(sUnit)>0)
    def SetLang(self,lang):
        self.lang=lang
    def SetNode(self,name,node,doc):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.oldVal=None
        tmp=doc.getChild(node,'inheritance_type')
        if tmp is not None:
            dictType={}
            for c in doc.getChilds(tmp):
                val=doc.getText(c)
                try:
                    v=dictType[doc.getTagName(c)]
                    continue
                except:
                    dictType[doc.getTagName(c)]=val
            if self.lang is not None:
                for c in doc.getChildsAttrVal(tmp,attr='language',attrVal=self.lang):
                    val=doc.getText(c)
                    dictType[doc.getTagName(c)]=val
        else:
            dictType=None
        tmp=doc.getChild(node,name)
        #print tmp,name
        if tmp is None:
            self.Show(False,0,False)
            self.iSel=0
        else:
            dict={}
            for c in doc.getChilds(tmp):
                val=doc.getText(c)
                try:
                    v=dict[doc.getTagName(c)]
                    continue
                except:
                    dict[doc.getTagName(c)]=val
            if self.lang is not None:
                for c in doc.getChildsAttrVal(tmp,attr='language',attrVal=self.lang):
                    val=doc.getText(c)
                    dict[doc.getTagName(c)]=val
            #print dict,dictType
            if dictType is not None:
                dictType['val']=dict['val']
            else:
                dictType=dict
            #self.Set(dict,dictType)         # 070223:wro
            self.Set(dictType)         # 070223:wro
    def OnTxtChanged(self,event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #print 'event txt'#,event.GetValue()
        #print event.GetId()
        #print type(self.GetValue()),type(self.oldVal)
        #print self.GetValue(),self.oldVal
        if self.oldVal==None:
            self.oldVal=self.GetValue()
        else:
            if self.oldVal!=self.GetValue():
                wx.PostEvent(self,vtInputValueChanged(self,unicode(self.GetValue())))
        event.Skip()
    def OnTxtApply(self,evt):
        evt.Skip()
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        wx.PostEvent(self,vtInputValueApply(self,unicode(self.GetValue())))
        
