#----------------------------------------------------------------------------
# Name:         vtInputState.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060420
# CVS-ID:       $Id: vtInputState.py,v 1.5 2008/03/22 14:34:25 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt

import sys

from vidarc.tool.xml.vtXmlDomConsumer import *
from vidarc.tool.xml.vtXmlDomConsumerLang import *

wxEVT_VTINPUT_STATE_CHANGED=wx.NewEventType()
vEVT_VTINPUT_STATE_CHANGED=wx.PyEventBinder(wxEVT_VTINPUT_STATE_CHANGED,1)
def EVT_VTINPUT_STATE_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTINPUT_STATE_CHANGED,func)
class vtInputStateChanged(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTINPUT_STATE_CHANGED(<widget_name>, self.OnChanged)
    """

    def __init__(self,obj,val,s):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTINPUT_STATE_CHANGED)
        self.val=val
        self.sVal=s
    def GetValue(self):
        return self.val
    def GetValueStr(self):
        return self.sVal

class vtInputStateTransientPopup(wx.Dialog):
    #SIZE_PN_SCROLL=30
    def __init__(self, parent, size,style,name=''):
        self.SIZE_PN_SCROLL=wx.SystemSettings.GetMetric(wx.SYS_VSCROLL_X)
        wx.Dialog.__init__(self, parent,style=wx.RESIZE_BORDER)#wx.BORDER_SIMPLE)#|wx.STAY_ON_TOP)
        id=wx.NewId()
        wx.EVT_SIZE(self,self.OnSize)
        st = wx.StaticText(self, -1,name,pos=(70,4))
        self.cbCancel = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Cancel), name=u'cbCancel',
              parent=self, pos=(0,0), size=(30,30), style=wx.BU_AUTODRAW)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              self.cbCancel)
              
        self.cbApply = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Apply), name=u'cbApply',
              parent=self, pos=wx.Point(32, 00), size=wx.Size(30, 30),
              style=wx.BU_AUTODRAW)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              self.cbApply)
        if size[1]<40:
            size=(size[0],40)
        i=0
        iY=40
        iH=size[1]+4
        iSzHeight=0
        id=wx.NewId()
        self.lstStates = wx.ListCtrl(id=id, name=u'lstStates',
              parent=self, pos=wx.Point(4, 40), size=wx.Size(222,86),
              style=wx.LC_REPORT)
        self.lstStates.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstBitsListColClick,id=id)
        self.lstStates.Bind(wx.EVT_LIST_ITEM_DESELECTED,self.OnLstBitsListItemDeselected, id=id)
        self.lstStates.Bind(wx.EVT_LIST_ITEM_SELECTED,self.OnLstBitsListItemSelected, id=id)
        self.lstStates.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
                                    heading=_('tag'), width=80)
        self.lstStates.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT,
                                    heading=_('name'), width=120)
        self.selIdx=-1
        
        self.szOrig=(240,140)
        self.SetSize(self.szOrig)
    def OnLstBitsListColClick(self,evt):
        self.selIdx=-1
    def OnLstBitsListItemDeselected(self,evt):
        self.selIdx=-1
    def OnLstBitsListItemSelected(self,evt):
        self.selIdx=evt.GetIndex()
    def OnSize(self,evt):
        iW,iH=self.GetSize()
        if iW<self.szOrig[0]:
            iW=self.szOrig[0]
        #if iH<self.szMin[1]:
        #    iH=self.szMin[1]
        if iH<self.szOrig[1]:
            iH=self.szOrig[1]
        self.SetSize((iW,iH))
        #if self.trInfo is not None:
        #    self.trInfo.SetSize((iW-16,iH-52))
        evt.Skip()
    def OnCbCancelButton(self,evt):
        self.Show(False)
    def OnCbApplyButton(self,evt):
        try:
            self.Apply()
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        self.Show(False)
    def Show(self,flag):
        try:
            if flag:
                par=self.GetParent()
                #doc=par.__getDoc__()
                #node=par.__getNode__()
                #fid=par.__getSelID__()
                #self.trInfo.SelectByID(fid)
                l=par.__getPossibleTrans__()
                self.lstStates.DeleteAllItems()
                self.selIdx=-1
                for tup in l:
                    idx=self.lstStates.InsertStringItem(sys.maxint,tup[0])
                    self.lstStates.SetStringItem(idx,1,tup[1])
                    self.lstStates.SetItemData(idx,tup[2])
            else:
                par=self.GetParent()
                par.cbPopup.SetValue(False)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        wx.Dialog.Show(self,flag)
    def Apply(self):
        try:
            if self.selIdx<0:
                return
            par=self.GetParent()
            id=self.lstStates.GetItemData(self.selIdx)
            par.__apply__(id)
            #doc=par.__getDoc__()
            #node=par.__getNode__()
            par.UpdateLang()
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
    def GetVal(self,lang):
        try:
            return ''
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
            return ''
    def SetVal(self,val,lang):
        try:
            pass
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
class vtInputState(wx.Panel,vtXmlDomConsumer,vtXmlDomConsumerLang):
    VERBOSE=1
    def __init__(self,*_args,**_kwargs):
        vtXmlDomConsumer.__init__(self)
        vtXmlDomConsumerLang.__init__(self)
        try:
            sz=_kwargs['size']
        except:
            sz=wx.Size(100,34)
            _kwargs['size']=sz
        try:
            self.iDft=_kwargs['default']
            del _kwargs['default']
        except:
            self.iDft=0
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        self.SetAutoLayout(True)
        
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        self.txtTag = wx.TextCtrl(id=wx.NewId(), name=u'txtTag',
              parent=self, pos=wx.Point(0, 0), size=wx.Size(40, 21), style=wx.TE_READONLY,
              value=u'')
        #self.txtTag.Enable(False)
        bxs.AddWindow(self.txtTag, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        
        self.txtName = wx.TextCtrl(id=wx.NewId(), name=u'txtName',
              parent=self, pos=wx.Point(40, 0), size=wx.Size(80, 21),
              style=wx.TE_READONLY, value=u'')
        #self.txtName.Enable(False)
        bxs.AddWindow(self.txtName, 2, border=4, flag=wx.EXPAND)
        
        id=wx.NewId()
        self.cbPopup = wx.lib.buttons.GenBitmapToggleButton(ID=id,
              bitmap=vtArt.getBitmap(vtArt.Down), name=u'cbPopup',
              parent=self, pos=(80,0), size=(24,24), style=wx.BU_AUTODRAW)
        #self.cbPopup.SetBitmapLabel(getDownBitmap())
        #self.cbPopup.SetBitmapSelected(getDownBitmap())
        self.cbPopup.Bind(wx.EVT_BUTTON,self.OnPopupButton,self.cbPopup)#id=id)
        bxs.AddWindow(self.cbPopup, 0, border=0, flag=wx.ALIGN_CENTER)
        #bxs.Layout()
        self.SetSizer(bxs)
        self.Layout()
        
        self.popWin=None
        
        self.fid=''
        
        self.bBlock=True
        wx.CallAfter(self.__clearBlock__)
        self.tagName='state'
        self.keyNode=None
        self.objSM=None
        self.bEnableMark=True
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
    def SetEnableMark(self,flag):
        self.bEnableMark=flag
    def SetTagName(self,tagName):
        self.tagName=tagName
    def SetNodeAttr(self,nodeName,attrName):
        self.keyNode,self.tagName=nodeName,attrName
    def __markModified__(self,flag=True):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.bEnableMark==False:
            return
        if flag:
            f=self.txtTag.GetFont()
            f.SetWeight(wx.FONTWEIGHT_BOLD)
            self.txtTag.SetFont(f)
            f=self.txtName.GetFont()
            f.SetWeight(wx.FONTWEIGHT_BOLD)
            self.txtName.SetFont(f)
        else:
            f=self.txtTag.GetFont()
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
            self.txtTag.SetFont(f)
            f=self.txtName.GetFont()
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
            self.txtName.SetFont(f)
        self.txtTag.Refresh()
        self.txtName.Refresh()
    def __clearBlock__(self):
        self.bBlock=False
    def Clear(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.bBlock=True
        self.__markModified__(False)
        self.txtTag.SetValue('')
        self.txtName.SetValue('')
        self.cbPopup.Enable(True)
        vtXmlDomConsumer.Clear(self)
        wx.CallAfter(self.__clearBlock__)
    def Enable(self,state):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.txtTag.Enable(state)
        self.txtName.Enable(state)
    def SetValue(self,val):
        self.bBlock=True
        self.__markModified__(False)
        wx.CallAfter(self.__clearBlock__)
    def GetValue(self):
        val=0
        return val
    def GetValueStr(self):
        return '%08d'%self.GetValue()
    def SetValueStr(self,val):
        try:
            self.SetValue(long(val))
        except:
            self.SetValue(self.iDft)
    def SetDoc(self,doc,bNet=False):
        vtXmlDomConsumer.SetDoc(self,doc)
        vtXmlDomConsumerLang.SetDoc(self,doc)
        try:
            self.objSM=self.doc.GetStateMachine(self.keyNode,self.tagName)
        except:
            vtLog.vtLngTB(self.GetName())
    def SetNode(self,node):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.Clear()
        vtXmlDomConsumer.SetNode(self,node)
        self.bBlock=True
        self.__markModified__(False)
        if self.VERBOSE:
            vtLog.CallStack(node)
        self.fid,tup=self.objSM.GetStateAndIdByNode(self.doc,node)
        self.txtTag.SetValue(tup[0])
        self.txtName.SetValue(tup[1])
        self.cbPopup.Enable(True)
        wx.CallAfter(self.__clearBlock__)
    def GetNode(self,node=None):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if node is None:
            node=self.node
        if node is None:
            return
        self.bBlock=True
        self.__markModified__(False)
        try:
            if node is None:
                node=self.node
            if node is None:
                return
            self.objSM.DoTransitionByNode(self.doc,node,self.fid)
            #tmp=self.doc.getChildForced(node,self.tagName)
            #self.doc.setAttribute(tmp,'fid',self.doc.ConvertIdNum2Str(self.fid))
            self.cbPopup.Enable(True)
            #self.doc.setChildAttribute(node,self.tagName,'fid',self.doc.ConvertIdNum2Str(self.fid))
        except:
            vtLog.vtLngTB(self.GetName())
        wx.CallAfter(self.__clearBlock__)
    def __getDoc__(self):
        return self.doc
    def __getNode__(self,node=None):
        if node is None:
            return self.node
        else:
            return node
    def __getPossibleTrans__(self):
        return self.objSM.GetPossibleTransitionsByNode(self.doc,self.node)
    def __getSelID__(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'fid:%s'%self.fid,self)
        if self.VERBOSE:
            vtLog.CallStack('')
        try:
            return self.fid
        except:
            return ''
    def __apply__(self,id):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'id:%08d'%(id),self)
        if self.VERBOSE:
            vtLog.CallStack(id)
        try:
            self.fid=id
            tup=self.objSM.GetState(id)
            self.txtTag.SetValue(tup[0])
            self.txtName.SetValue(tup[1])
            self.cbPopup.Enable(False)
            
            self.__markModified__(True)
            #wx.PostEvent(self,vtInputTreeChanged(self,self.nodeSel,self.fid,sVal))
        except:
            sVal=_(u'fault')
            vtLog.vtLngTB(self.GetName())
        self.bBlock=True
        #self.txtVal.SetValue(string.split(sVal,'\n')[0])
        wx.CallAfter(self.__clearBlock__)
    def __createPopup__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if self.popWin is None:
            sz=self.GetSize()
            sz=(sz[0],sz[1]-20)
            try:
                self.popWin=vtInputStateTransientPopup(self,sz,wx.SIMPLE_BORDER)
                #self.popWin.SetDocTree(self.docTreeTup[0],self.docTreeTup[1])
                #self.popWin.SetNodeTree(self.nodeTree)
                #if self.trSetNode is not None:
                #    self.popWin.SetNode(self.trSetNode(self.doc,self.node))
            except:
                vtLog.vtLngTB(self.GetName())
            #self.popWin.SetVal(self.txtVal.GetValue(),self.doc.GetLang())
        else:
            pass
    def OnPopupButton(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.__createPopup__()
        if self.cbPopup.GetValue()==True:
            btn=self
            iX,iY = btn.ClientToScreen( (0,0) )
            iW,iH =  btn.GetSize()
            iPopW,iPopH=self.popWin.GetSize()
            iScreenW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            iScreenH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            if iX+iPopW>iScreenW:
                iX=iScreenW-iPopW-4
                if iX<0:
                    iX=0
            iY+=iH
            if iY+iPopH>iScreenH:
                iY=iScreenH-iPopH
                if iY<0:
                    iY=0
            
            self.popWin.Move((iX,iY))
            #self.popWin.Move((pos[0],pos[1]+sz[1]))
            self.popWin.Show(True)
        else:
            self.popWin.Show(False)
        evt.Skip()
