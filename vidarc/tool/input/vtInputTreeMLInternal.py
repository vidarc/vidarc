#----------------------------------------------------------------------------
# Name:         vtInputTreeMLInternal.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060204
# CVS-ID:       $Id: vtInputTreeMLInternal.py,v 1.16 2008/04/28 11:43:46 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from vidarc.tool.input.vtInputTreeML import *
from vidarc.tool.xml.vtXmlHierarchy import getTagNames
import vidarc.tool.log.vtLog as vtLog

VERBOSE=0

class vtInputTreeMLInternal(vtInputTreeML):
    def __init__(self,*_args,**_kwargs):
        try:
            self.bFullName=_kwargs['show_fullname']
            del _kwargs['show_fullname']
        except:
            self.bFullName=True
        apply(vtInputTreeML.__init__,(self,) + _args,_kwargs)
        if self.trShow is None:
            self.trShow=self.__showNode__
        #if self.trSetNode is None:
        #    self.trSetNode=self.__setNode__
        self.lTagNames2Base=[]
        self.dCalls={}
    def SetShowFullName(self,flag):
        self.bFullName=flag
    def SetTreeClass(self,trCls):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.trClass=trCls
    def SetTreeFunc(self,trClass,showFunc):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if trClass is not None:
            self.trClass=trClass
        if showFunc is not None:
            self.trShow=showFunc
    def SetTagNames2Base(self,lst):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'lst:%s'%lst,
                        self)
        self.lTagNames2Base=lst
        self.__setTreeNode__()
    def __getSelNode__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if VERBOSE:
            vtLog.CallStack('')
        try:
            if self.doc is None:
                return
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'id:%s'%(self.doc.getKey(self.node)),self)
            #self.fid=self.doc.getAttribute(self.doc.getChild(self.node,self.tagName),'fid')
            tmpNode=self.doc.getChild(self.node,self.tagName)
            self.fid=self.doc.getForeignKey(tmpNode,'fid',appl=self.appl)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'fid:%s'%(self.fid),self)
            self.nodeSel=None
            try:
                if self.docTreeTup is not None:
                    docTree=self.docTreeTup[0]
                    if long(self.fid)>=0:
                        self.nodeSel=docTree.getNodeById(self.fid)
                else:
                    docTree=self.doc
            except:
                vtLog.vtLngCurWX(vtLog.WARN,'fid:%s'%(self.fid),self)
                #vtLog.vtLngTB(self.GetName())
            if self.nodeTree is None:
                self.__setTreeNode__()
            if self.nodeSel is not None:
                langs=self.doc.GetLanguages()
                iCount=len(langs)
                for tup in langs:
                    sVal=self.doc.getNodeTextLang(self.nodeSel,self.tagNameInt,tup[1]).split(u'\n')[0]
                    if self.bFullName:
                        sVal=getTagNames(docTree,self.nodeTree,None,self.nodeSel)+u'.'+sVal
                    self.dVal[tup[1]]=sVal
            else:
                self.__clearLangVals__()
            sVal=self.txtVal.GetValue()
            sValNew=self.dVal.get(self.doc.GetLang(),u'')
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'sVal:%s;sValNew:%s'%(sVal,sValNew),self)
            if sVal!=sValNew:
                self.txtVal.SetValue(sValNew)
                self.__markModified__(True)
                wx.PostEvent(self,vtInputTreeMLChanged(self,self.nodeSel,self.fid,self.txtVal.GetValue()))
                #self.__apply__(self.nodeSel)
                #if sVal!=self.txtVal.GetValue():
                #    self.__markModified__(True)
                #    wx.PostEvent(self,vtInputTreeMLChanged(self,self.nodeSel,self.fid,self.txtVal.GetValue()))
        except:
            vtLog.vtLngTB(self.GetName())
    def __setNode__(self,node):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'__setNode__;fid:%s;nodeSel:%s;node:%s'%(self.fid,self.nodeSel,node),
        #                self)
        try:
            docTree=self.docTreeTup[0]
            langs=self.doc.GetLanguages()
            if len(langs)>0:
                if self.nodeSel is None:
                    fid=None
                #    sVal=self.txtVal.GetValue()
                else:
                    fid=self.fid
                for tup in langs:
                    if tup[1] not in self.dVal:
                        vtLog.vtLngCurWX(vtLog.WARN,'lang:%s not in;dVal:%s'%(
                                    tup[1],vtLog.pformat(self.dVal)),self)
                        continue
                    sVal=self.dVal.get(tup[1],u'')
                    #    sVal=string.split(docTree.getNodeTextLang(self.nodeSel,self.tagName,tup[1]),'\n')[0]
                    #if self.bFullName:
                    #    sVal=getTagNames(self.doc,self.nodeTree,None,
                    #            self.doc.getChild(self.nodeSel,self.tagNameInt))+sVal
                    self.doc.setNodeTextLang(node,self.tagName,sVal,tup[1])
                    tmp=self.doc.getChildLang(node,self.tagName,tup[1])
                    if tmp is not None:     # 070223:wro
                        if fid is None:
                            self.doc.removeAttribute(tmp,'fid')
                        else:
                            #self.doc.setAttribute(tmp,'fid',self.fid)    # 070223:wro
                            self.doc.setForeignKey(tmp,attr='fid',        # 070223:wro
                                    attrVal=self.fid,appl=self.appl)
            #else:
            #    if self.nodeSel is None:
            #        sVal=self.txtVal.GetValue()
            #    else:
            #        langs=docTree.GetLanguages()
            #        if len(langs)>0:
            #            tup=langs[0]
            #            sVal=docTree.getNodeTextLang(self.nodeSel,self.tagName,tup[1])
            #        else:
            #            sVal=docTree.getNodeText(self.nodeSel,self.tagName)
            #    sVal=string.split(sVal,'\n')[0]
            #    self.doc.setNodeText(self.node,self.tagName,sVal)
        except:
            vtLog.vtLngTB(self.GetName())
    def __apply__(self,node):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'__apply__;node:%s'%(node),
        #                self)
        if VERBOSE:
            vtLog.CallStack('')
            print node
        try:
            self.nodeSel=node
            self.fid=self.docTreeTup[0].getKey(node)
            docTree=self.docTreeTup[0]
            if self.nodeTree is None:
                self.__setTreeNode__()
            if self.nodeSel is not None:
                langs=self.doc.GetLanguages()
                iCount=len(langs)
                for tup in langs:
                    sVal=self.doc.getNodeTextLang(self.nodeSel,self.tagNameInt,tup[1]).split(u'\n')[0]
                    if self.bFullName:
                        #sVal=getTagNames(self.doc,self.nodeTree,None,
                        #        self.doc.getChild(self.nodeSel,self.tagNameInt))+sVal
                        sVal=getTagNames(docTree,self.nodeTree,None,self.nodeSel)+u'.'+sVal
                    self.dVal[tup[1]]=sVal
            else:
                self.__clearLangVals__()
            #if self.trSetNode is not None:
            #    sVal=self.trSetNode(self.docTreeTup[0],node)
            #else:
            #    sVal=''
            if self.trShow is not None:
                sVal=self.trShow(self.docTreeTup[0],node,self.doc.GetLang())
            else:
                sVal=''
            if VERBOSE:
                print sVal
            if self.bFullName:
                #sVal=getTagNames(self.doc,self.nodeTree,None,
                #            docTree.getChild(self.nodeSel,self.tagNameInt))+sVal
                sVal=getTagNames(docTree,self.nodeTree,None,self.nodeSel)+u'.'+sVal
            if self.bBlock==False:
                self.__markModified__(True)
                wx.PostEvent(self,vtInputTreeMLChanged(self,self.nodeSel,self.fid,sVal))
        except:
            sVal=_(u'fault')
            vtLog.vtLngTB(self.GetName())
        self.bBlock=True
        self.txtVal.SetValue(sVal.split(u'\n')[0])
        if VERBOSE:
            print sVal
        wx.CallAfter(self.__clearBlock__)
        return sVal
    def __setTreeNode__(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%self.lTagNames2Base,
                        self)
        try:
            tmp=None
            try:
                doc=self.docTreeTup[0]
            except:
                doc=None
            if doc is not None:
                tmp=doc.getChildByLst(doc.getRoot(),self.lTagNames2Base)
            if VERBOSE:
                vtLog.CallStack('')
            #if vtLog.vtLngIsLogged(vtLog.DEBUG):
            #    vtLog.vtLngCurWX(vtLog.DEBUG,'id:%s'%(self.doc.getKey(tmp)),
            #                self)
            self.SetNodeTree(tmp)
            if self.popWin is not None:
                self.popWin.SetNodeTree(tmp)
        except:
            vtLog.vtLngTB(self.GetName())
        
    def AddTreeCall(self,name,call,*args,**kwargs):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if not self.dCalls.has_key(name):
            self.dCalls[name]=[]
        self.dCalls[name].append((call,args,kwargs))
    def __doCalls__(self,name,tree):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%name,
                        self)
        if VERBOSE:
            vtLog.CallStack(name)
        if not self.dCalls.has_key(name):
            return
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%repr(self.dCalls[name]),
                        self)
        lst=self.dCalls[name]
        for tup in lst:
            if VERBOSE:
                print tup
            try:
                eval('tree.%s'%tup[0])(*tup[1],**tup[2])
            except:
                vtLog.vtLngTB(self.GetName())
    #def Clear(self):
    #    vtInputTree.Clear(self)
    #    if VERBOSE:
    #        vtLog.CallStack('')
        #self.__setTreeNode__()
    def SetDoc(self,doc,bNet=False,bExternal=False):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlDomConsumer.SetDoc(self,doc)
        vtXmlDomConsumerLang.SetDoc(self,doc)
        if bExternal==False:
            self.SetDocTree(doc,bNet)
    def __getTreeInstance__(self,*args,**kwargs):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'args:%s;kwargs:%s'%(args,kwargs),
                        self)
        #print kwargs
        if not kwargs.has_key('id'):
            kwargs['id']=-1
        if not kwargs.has_key('pos'):
            kwargs['pos']=(4,4)
        if not kwargs.has_key('size'):
            kwargs['size']=wx.DefaultSize
        if not kwargs.has_key('style'):
            kwargs['style']=0
        if not kwargs.has_key('name'):
            kwargs['name']='treeInstance'
        if VERBOSE:
            kwargs['verbose']=1
        tr=self.trClass(*args,**kwargs)
        tr.EnableLanguageMenu()
        self.__doCalls__('init',tr)
        return tr
    def __createPopup__(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if self.nodeTree is None:
            self.__setTreeNode__()
        vtInputTreeML.__createPopup__(self)
    def __createTree__(*args,**kwargs):
        vtLog.CallStack('')
        vtLog.pprint(args)
        vtLog.pprint(kwargs)
        
        tr=vXmlHumTree(**kwargs)
        tr.SetNodeInfos(['name','|id'])
        tr.SetGrouping([],[('name','')])
        tr.EnableLanguageMenu()
        return tr
    def __showNode__(self,doc,node,lang):
        return doc.getNodeTextLang(node,self.tagNameInt,lang)  #'name'

