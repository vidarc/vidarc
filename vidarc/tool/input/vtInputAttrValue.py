#Boa:FramePanel:vtInputAttrValue
#----------------------------------------------------------------------------
# Name:         vtInputAttrValue.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051120
# CVS-ID:       $Id: vtInputAttrValue.py,v 1.6 2008/03/22 14:34:24 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.masked.numctrl
import wx.lib.masked.maskededit
import string
import vidarc.tool.lang.vtLgBase as vtLgBase

wxEVT_VTINPUT_ATTRVALUE_CHANGED=wx.NewEventType()
vEVT_VTINPUT_ATTRVALUE_CHANGED=wx.PyEventBinder(wxEVT_VTINPUT_ATTRVALUE_CHANGED,1)
def EVT_VTINPUT_ATTRVALUE_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTINPUT_ATTRVALUE_CHANGED,func)
class vtInputAttrValueChanged(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTINPUT_ATTRVALUE_CHANGED(<widget_name>, self.OnChanged)
    """

    def __init__(self,obj,text):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTINPUT_ATTRVALUE_CHANGED)
        self.text=text
    def GetText(self):
        return self.text

class vtInputAttrValue(wx.Panel):
    def __init__(self,*_args,**_kwargs):
        try:
            sizeunit=_kwargs['size_unit']
            del _kwargs['size_unit']
        except:
            sizeunit=wx.Size(0,0)
        
        try:
            sz=_kwargs['size']
        except:
            sz=wx.Size(100,22)
            _kwargs['size']=sz
        self.iSel=0
        self.lang=None
        self.oldVal=None
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        self.SetMinSize((-1,-1))
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        
        #print sz
        szIn=wx.Size(sz[0]-sizeunit[0],sz[1])
        id=wx.NewId()
        self.mskTxtValue = wx.lib.masked.textctrl.TextCtrl(id=id,
              name=u'mskText', parent=self, pos=wx.Point(0, 0),
              size=szIn, style=0, value='')
        self.mskTxtValue.SetMinSize((-1,-1))
        self.mskTxtValue.Bind(wx.EVT_TEXT,self.OnTxtChanged,id=id)
        bxs.AddWindow(self.mskTxtValue, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        id=wx.NewId()
        decSep,thousSep=vtLgBase.getNumSeperatorsFromNumMask()
        self.mskNumValue = wx.lib.masked.numctrl.NumCtrl(id=id,
              name=u'mskNum', parent=self, pos=wx.Point(0, 0), size=szIn, style=0, value=0,
              decimalChar=decSep,groupChar=thousSep)
        self.mskNumValue.Show(False)
        self.mskNumValue.SetMinSize((-1,-1))
        self.mskNumValue.Bind(wx.EVT_TEXT,self.OnTxtChanged,id=id)
        #self.mskNumValue.Bind(wx.lib.masked.numctrl.EVT_MASKEDNUM,
        #      self.OnNumChanged, id=id)
        #self.mskNumValue.SetAutoSize(True)
        #self.mskNumValue.Show(False)
        #self.mskNumValue.SetDecimalChar(ld['decimal_point'])
        #self.mskNumValue.SetGroupChar(ld['grouping'])
        bxs.AddWindow(self.mskNumValue, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        self.mskCmbValue = wx.lib.masked.combobox.ComboBox(choices=[],
              id=wx.NewId(), name=u'mskChoice', parent=self,
              pos=wx.Point(0, 0), size=szIn, style=0, value='')
        self.mskCmbValue.SetMinSize((-1,-1))
        self.mskCmbValue.Show(False)
        bxs.AddWindow(self.mskCmbValue, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        self.mskTimeValue = wx.lib.masked.timectrl.TimeCtrl(id=wx.NewId(),
              name=u'mskTimeValue', parent=self, pos=wx.Point(0,0),
              #size=wx.Size(size.GetWidth()-w,size.GetHeight()), style=0, value=0)
              size=szIn, style=0, value=0)
        self.mskTimeValue.SetMinSize((-1,-1))
        self.mskTimeValue.Show(False)
        bxs.AddWindow(self.mskTimeValue, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        self.mskIPValue = wx.lib.masked.ipaddrctrl.IpAddrCtrl(id=wx.NewId(),
              name=u'mskIPValue', parent=self, pos=wx.Point(0, 0),
              #size=wx.Size(size.GetWidth()-w,size.GetHeight()), style=0, value=0)
              size=szIn, style=0, value=0)
        self.mskIPValue.SetMinSize((-1,-1))
        self.mskIPValue.Show(False)
        bxs.AddWindow(self.mskIPValue, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        self.lblUnit = wx.StaticText(id=wx.NewId(),
              label=u'[unit]', name=u'lblUnit', parent=self, pos=wx.Point(szIn[0]+4,
              4), size=sizeunit, style=0)
        self.lblUnit.SetMinSize((-1,-1))
        bxs.AddWindow(self.lblUnit, 0, border=4, flag=wx.EXPAND|wx.LEFT|wx.RIGHT)
        
        self.Show(True,0)
    def Show(self,state,num=0,unit=True):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #self.strValue.Show(state)
        #self.numValue.Show(False)
        if num==0:
            self.mskTxtValue.Show(state)
        else:
            self.mskTxtValue.Show(False)
        if num==1:
            self.mskCmbValue.Show(state)
        else:
            self.mskCmbValue.Show(False)
        if num==2:
            self.mskTimeValue.Show(state)
        else:
            self.mskTimeValue.Show(False)
        if num==3:
            self.mskIPValue.Show(state)
        else:
            self.mskIPValue.Show(False)
        if num==4:
            self.mskNumValue.Show(state)
        else:
            self.mskNumValue.Show(False)
        self.lblUnit.Show(unit)
        self.Layout()
    def Clear(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.iSel=0
        self.oldVal=None
        self.mskTxtValue.ClearValue()
        self.mskNumValue.ClearValue()
        self.mskCmbValue.ClearValue()
        self.mskTimeValue.ClearValue()
        self.mskIPValue.ClearValue()
        self.Show(True,0)
        #self.lblUnit.SetLabel('')
    def Enable(self,state):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.iSel==0:
            self.mskTxtValue.Enable(state)
        if self.iSel==1:
            self.mskCmbValue.Enable(state)
        if self.iSel==2:
            self.mskTimeValue.Enable(state)
        if self.iSel==3:
            self.mskIPValue.Enable(state)
        if self.iSel==4:
            self.mskNumValue.Enable(state)
    def SetValue(self,val):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #self.oldVal=val
        if self.iSel==0:
            self.mskTxtValue.SetValue(val)
        if self.iSel==1:
            self.mskCmbValue.SetValue(val)
        if self.iSel==2:
            self.mskTimeValue.SetValue(val)
        if self.iSel==3:
            self.mskIPValue.SetValue(val)
        if self.iSel==4:
            self.mskNumValue.SetValue(val)
        pass
    def GetValue(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.iSel==0:
            return self.mskTxtValue.GetValue()
        if self.iSel==1:
            return self.mskCmbValue.GetValue()
        if self.iSel==2:
            return self.mskTimeValue.GetValue()
        if self.iSel==3:
            return self.mskIPValue.GetValue()
        if self.iSel==4:
            return self.mskNumValue.GetValue()
        
    def Set(self,dict,dictType):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.oldVal=None
        if dictType is None:
            dictType=dict
            try:
                sType=dict['type']
            except:
                sType='text'
        else:
            if dict.has_key('inheritance'):
                #print dict
                if string.find(dict['inheritance'],'type')>=0:
                    try:
                        sType=dictType['type']
                    except:
                        sType='text'
                else:
                    try:
                        sType=dict['type']
                    except:
                        sType='text'
            else:
                try:
                    sType=dict['type']
                except:
                    sType='text'
        try:
            sUnit=dictType['unit']
        except:
            try:
                sUnit=dict['unit']
            except:
                sUnit=''
        if sType in ['string','text']:
            self.iSel=0
            try:
                iLen=int(dict['len'])
            except:
                iLen=100
            sMask='*{%d}'%iLen
            self.mskTxtValue.ClearValue()
            self.mskTxtValue.SetCtrlParameters(mask=sMask,formatcodes='_')
            #self.mskTxtValue.SetMask(sMask)
            #self.mskTxtValue.SetFormatcodes('_')
            
            #self.mskTxtValue.Show(True)
            #self.mskTxtValue.SetDefaultValue(dict['val'])
            self.mskTxtValue.SetValue(dict['val'][:iLen])
            self.mskTxtValue.SetInsertionPoint(0)
        elif sType in ['int','long']:
            self.iSel=4
            #self.mskValue.Show(True)
            try:
                sMin=long(dictType['min'])
            except:
                sMin=None
            self.mskNumValue.SetMin(sMin)
            try:
                sMax=long(dictType['max'])
            except:
                sMax=None
            self.mskNumValue.SetMax(sMax)
            try:
                iIntW=int(dictType['digits'])
            except:
                iIntW=8
            self.mskNumValue.SetIntegerWidth(iIntW)
            #sMask='#{%d}'%iIntW
            #self.mskNumValue.SetMask(sMask)
            iFriW=0
            if sType=='int':
                self.fmt='%'+'%d'%(iIntW)+'d'
            elif sType=='long':
                self.fmt='%'+'%d'%(iIntW)+'d'
            else:
                self.fmt='%d'                
            self.mskNumValue.SetFractionWidth(iFriW)
            try:
                self.mskNumValue.SetValue(long(dict['val']))
            except:
                self.mskNumValue.SetValue(0)
        elif sType in ['float','double','pseudofloat']:
            self.iSel=4
            #self.mskValue.Show(True)
            try:
                sMin=float(dictType['min'])
            except:
                sMin=None
            self.mskNumValue.SetMin(sMin)
            try:
                sMax=float(dictType['max'])
            except:
                sMax=None
            self.mskNumValue.SetMax(sMax)
            try:
                iIntW=int(dictType['digits'])
            except:
                iIntW=8
            self.mskNumValue.SetIntegerWidth(iIntW)
            try:
                iFriW=int(dictType['comma'])
            except:
                iFriW=2
            self.mskNumValue.SetFractionWidth(iFriW)
            #sMask='#{%d}.#{%d}'%(iIntW,iFriW)
            self.mskNumValue.ClearValue()
            #self.mskTxtValue.SetMask(sMask)
            #self.mskTxtValue.SetFormatcodes('-_,._>')
            self.mskNumValue.SetDefaultValue(0.0)
            self.fmt='%'+'%d'%iIntW+'.'+'%d'%iFriW+'f'
            #self.numValue.SetFractionWidth(iFriW)
            #self.mskNumValue.
            try:
                self.mskNumValue.SetValue(float(dict['val']))
            except:
                #self.mskTxtValue.SetValue('0.0')
                pass
        elif sType=='choice':
            self.iSel=1
            keys=dictType.keys()
            keys.sort()
            lst=[]
            iLen=0
            for k in keys:
                if k[:2]=='it':
                    lst.append(dictType[k])
                    iL=len(dictType[k])
                    if iL>iLen:
                        iLen=iL
            sMask='*{%d}'%iLen
            self.mskCmbValue.ClearValue()
            self.mskCmbValue.SetCtrlParameters(choices=lst,mask=sMask,)
            #self.mskCmbValue.SetChoices(lst)
            #self.mskCmbValue.SetMask(sMask)
            try:
                #print dict['val']
                #print dict['it%02d'%int(dict['val'])]
                #self.mskCmbValue.SetValue(dict['it%02d'%int(dict['val'])])
                self.mskCmbValue.SetValue(dict['val'])
            except:
                try:
                    #print dictType['val']
                    #print dictType['it%02d'%int(dictType['val'])]
                    #self.mskCmbValue.SetValue(dictType['it%02d'%int(dictType['val'])])
                    self.mskCmbValue.SetValue(dictType['it%02d'%int(dictType['val'])])
                except:
                    self.mskCmbValue.SetValue(dictType['it00'])
        self.lblUnit.SetLabel(sUnit)
        #self.lblUnit.Show(True)
        self.Show(True,self.iSel)
    def SetLang(self,lang):
        self.lang=lang
    def SetNode(self,name,node,doc):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.oldVal=None
        tmp=doc.getChild(node,'inheritance_type')
        if tmp is not None:
            dictType={}
            for c in doc.getChilds(tmp):
                val=doc.getText(c)
                try:
                    v=dictType[doc.getTagName(c)]
                    continue
                except:
                    dictType[doc.getTagName(c)]=val
            if self.lang is not None:
                for c in doc.getChildsAttrVal(tmp,attr='language',attrVal=self.lang):
                    val=doc.getText(c)
                    dictType[doc.getTagName(c)]=val
        else:
            dictType=None
        tmp=doc.getChild(node,name)
        #print tmp,name
        if tmp is None:
            self.Show(False,0,False)
            self.iSel=0
        else:
            dict={}
            for c in doc.getChilds(tmp):
                val=doc.getText(c)
                try:
                    v=dict[doc.getTagName(c)]
                    continue
                except:
                    dict[doc.getTagName(c)]=val
            if self.lang is not None:
                for c in doc.getChildsAttrVal(tmp,attr='language',attrVal=self.lang):
                    val=doc.getText(c)
                    dict[doc.getTagName(c)]=val
            #print dict,dictType
            self.Set(dict,dictType)
    def GetNode(self,name,node,doc):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        tmp=doc.getChild(node,name)
        if tmp is not None:
            doc.setNodeText(tmp,'val',unicode(self.GetValue()))
    def OnTxtChanged(self,event):
        #print 'event txt'#,event.GetValue()
        #print event.GetId()
        #print type(self.GetValue()),type(self.oldVal)
        #print self.GetValue(),self.oldVal
        if self.oldVal==None:
            self.oldVal=self.GetValue()
        else:
            if self.oldVal!=self.GetValue():
                wx.PostEvent(self,vtInputAttrValueChanged(self,str(self.GetValue())))
        event.Skip()
        