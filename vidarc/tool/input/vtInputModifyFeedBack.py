#----------------------------------------------------------------------------
# Name:         vtInputModifyFeedBack.py
# Purpose:      provides operator feedback for WX-widgets
#               modified widgets are highlighted somehow
# Author:       Walter Obweger
#
# Created:      20060419
# CVS-ID:       $Id: vtInputModifyFeedBack.py,v 1.9 2009/10/18 08:56:21 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.log.vtLog as vtLog

class vtInputModifyFeedBack:
    def __init__(self,lWidgets=[],lEvent=[],bEnableMark=True,bEnableMod=True):
        """ bind events due lEvent list:
           [(obj_01 , evt_01) , (obj_02 , evt_02),]
        """
        self.lWidgets=lWidgets
        for obj,evt in lEvent:
            # FIXME bind to events
            obj.Bind(evt, self.OnChange , obj)
        self.bModified=False
        self.bBlock=False
        #self.bBlock=0
        self.bEnableMod=bEnableMod
        self.bEnableMark=bEnableMark
    def SetEnableMark(self,flag):
        #vtLog.vtLngCurWX(vtLog.DEBUG,'flag:%d'%(flag),self)
        self.bEnableMark=flag
    def SetEnableModification(self,flag):
        #vtLog.vtLngCurWX(vtLog.DEBUG,'flag:%d'%(flag),self)
        self.bEnableMod=flag
    def OnChange(self,evt):
        evt.Skip()
        obj=None
        if hasattr(evt,'GetEventObj'):
            obj=evt.GetEventObj()
        if hasattr(evt,'GetEventObject'):
            obj=evt.GetEventObject()
        if obj is None:
            #vtLog.vtLngCurWX(vtLog.ERROR,'evt object not found',self)
            return
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'name:%s;bBlock:%d'%(obj.GetName(),self.bBlock),self)
        if self.bBlock>0:
            return
        self.SetModified(True,obj)
        return 
        try:
            self.SetModified(True,evt.GetEventObj())
        except:
            self.SetModified(True,evt.GetEventObject())
    def IsBlocked(self):
        return self.bBlock>0
    def SetBlock(self):
        #vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.bBlock=True
        #self.bBlock+=1
    def ClrBlockDelayed(self):
        #vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        wx.CallAfter(self.ClrBlock)
    def ClrBlock(self):
        #vtLog.vtLngCurWX(vtLog.DEBUG,'bBlock:%d'%(self.bBlock),self)
        self.bBlock=False
        #if self.bBlock>0:
        #    self.bBlock-=1
    def SetModified(self,state,obj=None):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'bModified:%d;state:%d'%
        #                (self.bModified,state),self)
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.bEnableMod:
            self.bModified=state
        if self.bEnableMark==False:
            return
        if self.bModified!=state:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'bModified:%d;state:%d'%
                        (self.bModified,state),self)
        if state:
            if obj is None:
                for obj in self.lWidgets:
                    f=obj.GetFont()
                    f.SetWeight(wx.FONTWEIGHT_BOLD)
                    obj.SetFont(f)
                    obj.Refresh()
            else:
                f=obj.GetFont()
                f.SetWeight(wx.FONTWEIGHT_BOLD)
                obj.SetFont(f)
                obj.Refresh()
        else:
            if obj is None:
                for obj in self.lWidgets:
                    f=obj.GetFont()
                    f.SetWeight(wx.FONTWEIGHT_NORMAL)
                    obj.SetFont(f)
                    obj.Refresh()
            else:
                f=obj.GetFont()
                f.SetWeight(wx.FONTWEIGHT_NORMAL)
                obj.SetFont(f)
                obj.Refresh()
    def GetModified(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'bModified:%d'%(self.bModified),self)
        return self.bModified
