#Boa:FramePanel:vtInputCalcPanel
#----------------------------------------------------------------------------
# Name:         vtImportCalcPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060508
# CVS-ID:       $Id: vtInputCalcPanel.py,v 1.8 2008/03/22 14:34:24 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

from vidarc.tool.input.vtInputCalcEditDialog import *
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

wxEVT_VTINPUT_CALC_CHANGED=wx.NewEventType()
vEVT_VTINPUT_CALC_CHANGED=wx.PyEventBinder(wxEVT_VTINPUT_CALC_CHANGED,1)
def EVT_VTINPUT_CALC_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTINPUT_CALC_CHANGED,func)
class vtInputCalcChanged(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VTINPUT_CALC_CHANGED(<widget_name>, self.OnChanged)
    """
    def __init__(self,obj,lFormel):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTINPUT_CALC_CHANGED)
        self.lFormel=lFormel
    def GetFormel(self):
        return self.lFormel

[wxID_VTINPUTCALCPANEL, wxID_VTINPUTCALCPANELCBCALC, 
 wxID_VTINPUTCALCPANELCBEDIT, wxID_VTINPUTCALCPANELTXTRES, 
] = [wx.NewId() for _init_ctrls in range(4)]

class vtInputCalcPanel(wx.Panel):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(2)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbEdit, 0, border=0, flag=0)
        parent.AddWindow(self.cbCalc, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.txtRes, 0, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=3, hgap=0, rows=1, vgap=0)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt,id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=id, name=u'vtInputCalcPanel', parent=prnt,
              pos=wx.Point(420, 252), size=wx.Size(200, 53),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(192, 26))

        self.cbEdit = wx.BitmapButton(bitmap=vtArt.getBitmap(vtArt.Edit),
              id=wxID_VTINPUTCALCPANELCBEDIT, name=u'cbEdit', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(24, 24), style=wx.BU_AUTODRAW)
        self.cbEdit.Bind(wx.EVT_BUTTON, self.OnCbEditButton,
              id=wxID_VTINPUTCALCPANELCBEDIT)

        self.cbCalc = wx.BitmapButton(bitmap=vtArt.getBitmap(vtArt.Calc),
              id=wxID_VTINPUTCALCPANELCBCALC, name=u'cbCalc', parent=self,
              pos=wx.Point(28, 0), size=wx.Size(24, 24), style=wx.BU_AUTODRAW)
        self.cbCalc.Bind(wx.EVT_BUTTON, self.OnCbCalcButton,
              id=wxID_VTINPUTCALCPANELCBCALC)

        self.txtRes = wx.TextCtrl(id=wxID_VTINPUTCALCPANELTXTRES,
              name=u'txtRes', parent=self, pos=wx.Point(56, 0),
              size=wx.Size(136, 24), style=0, value=u'')

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vtInput')
        self._init_ctrls(parent,id)
        self.dlgEdit=None
        self.val=''
        self.vals=None
        self.dValMap=None
        self.oReg=None
        self.lFormel=[]
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
    def SetRegObj(self,o):
        self.oReg=o
        if self.dlgEdit is not None:
            self.dlgEdit.SetRegObj(self.oReg)
    def SetValueMap(self,dValMap):
        self.dValMap=dValMap
    def SetValue(self,val,vals):
        self.val=val
        self.vals=vals
    def GetValue(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        return self.txtRes.GetValue()
    def SetFormel(self,l):
        self.lFormel=l
    def GetFormel(self):
        return self.lFormel
    def Calc(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.oReg is not None:
            s=self.oReg.Calc(self.val,self.vals,self.lFormel,self.dValMap)
            self.txtRes.SetValue(unicode(s))
        else:
            self.txtRes.SetValue(u'*****')
    def SetEditDlg(self,dlg):
        self.dlgEdit=dlg
    def GetEditDlg(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.dlgEdit is None:
            self.dlgEdit=vtInputCalcEditDialog(self)
            self.dlgEdit.Centre()
            self.dlgEdit.SetRegObj(self.oReg)
        return self.dlgEdit
    def OnCbEditButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.GetEditDlg()
        self.dlgEdit.SetValue(self.lFormel,self.val,self.vals,self.dValMap)
        iRet=self.dlgEdit.ShowModal()
        if iRet>0:
            #vtLog.CallStack('')
            self.lFormel=self.dlgEdit.GetValue()
            wx.PostEvent(self,vtInputCalcChanged(self,self.lFormel))
        event.Skip()
    def OnCbCalcButton(self, event):
        event.Skip()
        self.Calc()
