#----------------------------------------------------------------------
# Name:         vtInputDistributeTree.py
# Purpose:      list control with tree popup window and percentages
#
# Author:       Walter Obweger
#
# Created:      20060725
# CVS-ID:       $Id: vtInputDistributeTree.py,v 1.7 2008/03/22 18:19:46 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from vidarc.tool.input.vtInputMultipleTree import *
import vidarc.tool.input.vtInputFloat
import sys
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.xml.vtXmlDomConsumer import *
from vidarc.tool.xml.vtXmlDomConsumerLang import *
from vidarc.tool.xml.vtXmlGrpTree import vtXmlGrpTree

class vtInputDistributeTree(vtInputMultipleTree):
    VERBOSE=0
    def __init__(self,*_args,**_kwargs):
        global _
        _=vtLgBase.assignPluginLang('vtInput')
        vtXmlDomConsumer.__init__(self)
        vtXmlDomConsumerLang.__init__(self)
        if 'size' in _kwargs:
            sz=_kwargs['size']
        else:
            sz=wx.Size(100,30)
            _kwargs['size']=sz
        try:
            szCb=_kwargs['size_button']
            del _kwargs['size_button']
        except:
            szCb=wx.Size(32,32)
        try:
            szTxt=_kwargs['size_text']
            del _kwargs['size_text']
        except:
            szTxt=wx.Size(sz[0]-szCb[0]-2,sz[1]-4)
        try:
            self.trClass=_kwargs['tree_class']
            del _kwargs['tree_class']
        except:
            self.trClass=vtXmlGrpTree
        try:
            self.trSetNode=_kwargs['set_node']
            del _kwargs['set_node']
        except:
            self.trSetNode=None#self.__setNode__
        try:
            self.trGetNode=_kwargs['get_node']
            del _kwargs['get_node']
        except:
            self.trGetNode=None
        try:
            self.trGetId=_kwargs['get_id']
            del _kwargs['get_id']
        except:
            self.trGetId=None
        try:
            self.trShow=_kwargs['show_node_func']
            del _kwargs['show_node_func']
        except:
            self.trShow=None
        try:
            self.tagName=_kwargs['tag_name']
            del _kwargs['tag_name']
        except:
            self.tagName='name'
        try:
            self.tagGrp=_kwargs['tag_grp']
            del _kwargs['tag_grp']
        except:
            self.tagGrp=None
        if 'tag_name_int' in _kwargs:
            self.tagNameInt=_kwargs['tag_name_int']
            del _kwargs['tag_name_int']
        else:
            self.tagNameInt='name'
        try:
            self.tagNameValue=_kwargs['tag_value']
            del _kwargs['tag_value']
        except:
            self.tagNameValue='percent'
        try:
            self.fLimit=_kwargs['limit']
            del _kwargs['limit']
        except:
            self.fLimit=100.0
        if 'col_value' in _kwargs:
            sColValue=_kwargs['col_value']
            del _kwargs['col_value']
        else:
            sColValue=_(u'value')
        try:
            self.lColumns=_kwargs['columns']
            del _kwargs['columns']
        except:
            self.lColumns=[('name',-1),('id',-1)]
        try:
            self.lShowAttrs=_kwargs['show_attrs']
            del _kwargs['show_attrs']
        except:
            self.lShowAttrs=['name','|id']
        try:
            self.lTreeBaseNode=_kwargs['tree_base_node']
            del _kwargs['tree_base_node']
        except:
            self.lTreeBaseNode=None
        try:
            self.fmt=_kwargs['fmt']
            del _kwargs['fmt']
        except:
            self.fmt='%4.2f'
        
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        self.SetAutoLayout(True)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        bxsLst = wx.FlexGridSizer(cols=1, hgap=0, rows=2, vgap=0)
        bxsLst.AddGrowableCol(0)
        bxsLst.AddGrowableRow(0)
        
        #size=(szTxt[0],szTxt[1])
        #pos=(0,(sz[1]-szTxt[1])/2)
        self.lstVal = wx.ListCtrl(id=-1, name='lstVal',
              #parent=self, pos=pos, size=size,
              parent=self, pos=wx.DefaultPosition, size=wx.DefaultSize,
              style=wx.LC_REPORT|wx.LC_SORT_ASCENDING)
        #self.txtVal.SetConstraints(LayoutAnchors(self.txtVal, True, True,
        #      True, False))
        bxsLst.AddWindow(self.lstVal, 1, border=0, flag=wx.EXPAND|wx.ALL)
        #self.txtVal.Bind(wx.EVT_TEXT, self.OnTextText,self.txtVal)
        self.lstVal.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnListItemDeselected, self.lstVal)
        self.lstVal.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnListItemSelected, self.lstVal)
        self.lstVal.Bind(wx.EVT_LIST_COL_CLICK, self.OnListColClick,
              self.lstVal)
        i=2
        self.lstVal.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT, heading=u'',
              width=20)
        self.lstVal.InsertColumn(col=1, format=wx.LIST_FORMAT_RIGHT, heading=sColValue,
              width=-1)
        
        for tup in self.lColumns:
            fmt=wx.LIST_FORMAT_LEFT
            w=-1
            try:
                if len(tup)>0:
                    sHdr=tup[0]
                    try:
                        w=tup[1]
                    except:
                        pass
                    try:
                        fmt=tup[2]
                    except:
                        pass
            except:
                sHdr=tup
            self.lstVal.InsertColumn(i,sHdr,fmt,w)
            i+=1
        
        bxsPer = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.slPer = wx.Slider(id=-1, maxValue=int(self.fLimit),
              minValue=0, name=u'slPer', parent=self, point=wx.DefaultPosition,
              size=wx.Size(200, 23), style=wx.SL_HORIZONTAL, value=0)
        self.slPer.Bind(wx.EVT_SCROLL, self.OnSlPerScroll)
        bxsPer.AddWindow(self.slPer, 3, border=4, flag=wx.EXPAND | wx.RIGHT)
        
        self.viPer = vidarc.tool.input.vtInputFloat.vtInputFloat(default=0.0,
              fraction_width=2, id=-1, integer_width=4,
              maximum=self.fLimit, minimum=0.0, name=u'viPer', parent=self,
              pos=wx.DefaultPosition, size=wx.DefaultSize, style=0)
        self.viPer.Bind(vidarc.tool.input.vtInputFloat.vEVT_VTINPUT_FLOAT_CHANGED,
              self.OnViPerVtinputFloatChanged, self.viPer)
        bxsPer.AddWindow(self.viPer, 1, border=4, flag= wx.EXPAND)
        bxsLst.AddSizer(bxsPer, 1, border=4, flag=wx.TOP | wx.EXPAND)
        bxs.AddSizer(bxsLst, 1, border=4, flag=wx.LEFT | wx.EXPAND)

        
        size=(szCb[0],szCb[1])
        pos=(szTxt[0]+(sz[0]-(szTxt[0]+szCb[0]))/2,(sz[1]-szCb[1])/2)
        bxsBt=wx.BoxSizer(orient=wx.VERTICAL)
        id=wx.NewId()
        self.cbPopup = wx.lib.buttons.GenBitmapToggleButton(ID=id,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbPopup',
              parent=self, pos=pos, size=size, style=wx.BU_AUTODRAW)
        #self.cbPopup.SetConstraints(LayoutAnchors(self.cbPopup, False, True,
        #      True, False))
        bxsBt.AddWindow(self.cbPopup, 0, border=0, flag=wx.ALIGN_CENTER)#wx.EXPAND)
        id=wx.NewId()
        self.tgFixed = wx.lib.buttons.GenBitmapToggleButton(ID=id,
              bitmap=vtArt.getBitmap(vtArt.SecLocked),
              name=u'tgFixed', parent=self, pos=wx.DefaultPosition, 
              size=size, style=wx.BU_AUTODRAW)
        self.tgFixed.SetToggle(False)
        self.tgFixed.Bind(wx.EVT_BUTTON, self.OnTgFixedButton,
              self.tgFixed)
        bxsBt.AddWindow(self.tgFixed, 0, border=4, flag=wx.TOP | wx.BOTTOM | wx.ALIGN_CENTER)
        
        id=wx.NewId()
        self.cbDel = wx.lib.buttons.GenBitmapButton(ID=id,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbDel',
              parent=self, pos=pos, size=size, style=wx.BU_AUTODRAW)
        #self.cbPopup.SetConstraints(LayoutAnchors(self.cbPopup, False, True,
        #      True, False))
        bxsBt.AddSpacer(wx.Size(4, 4), border=0, flag=0)
        bxsBt.AddWindow(self.cbDel, 0, border=0, flag=wx.ALIGN_CENTER)#wx.EXPAND)
        bxs.AddSizer(bxsBt, 0, border=4, flag=wx.LEFT)
        
        self.SetSizer(bxs)
        self.Layout()
        self.cbPopup.SetBitmapLabel(getAddBitmap())
        self.cbPopup.SetBitmapSelected(getAddAttrBitmap())
        self.cbPopup.Bind(wx.EVT_BUTTON,self.OnPopupButton,self.cbPopup)#id=id)
        self.cbDel.SetBitmapLabel(getDelBitmap())
        #self.cbDel.SetBitmapSelected(getDelBitmap())
        self.cbDel.Bind(wx.EVT_BUTTON,self.OnDelButton,self.cbDel)#id=id)
        
        self.slPer.SetTickFreq(5, 1)
        self.imgLst=wx.ImageList(16,16)
        self.imgLst.Add(vtArt.getBitmap(vtArt.Invisible))
        self.imgLst.Add(vtArt.getBitmap(vtArt.SecLocked))
        self.imgLst.Add(vtArt.getBitmap(vtArt.Sum))
        self.lstVal.SetImageList(self.imgLst,wx.IMAGE_LIST_SMALL)

        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.popWin=None
        self.doc=None
        self.docTreeTup=None
        self.nodeTree=None
        self.nodeSel=None
        #self.lVal=[]
        self.bBlock=False
        self.appl=None
        self.node=None
        self.bAscend=True
        self.bMod=False
        self.bEnableMark=True
        self.bBusy=False
        self.Clear()
        self.SetLimit(self.fLimit)
        self.__markModified__(False)
    def Clear(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.bBlock=True
        vtInputMultipleTree.Clear(self)
        #self.bBlock=True
        self.lstSel=[]
        self.lstPos=[]
        self.idxSel=-1
        self.idxSelVal=-1
        self.idxSelCol=0
        self.tgFixed.SetValue(False)
        self.tgFixed.Enable(False)
        idx=self.lstVal.InsertImageStringItem(sys.maxint,'',2)
        self.lstVal.SetStringItem(idx,1,vtLgBase.format(self.fmt,0.0))
        self.lstVal.SetStringItem(idx,2,_('sum'))
        self.lstVal.SetItemData(idx,-2)
        #self.SetModified(False)
        self.__markModified__(False)
        #self.bBlock=True
    def SetLimit(self,fLimit):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.fLimit=fLimit
        if self.idxSel>=0:
            self.lstVal.SetItemState(self.idxSel,0,wx.LIST_STATE_SELECTED)
            self.idxSel=-1
        self.viPer.SetLimit(0.0,self.fLimit)
        self.slPer.SetRange(0,int(self.fLimit))
        self.__calcVal2limitUpdate__()
        self.__refreshVal__()
        self.__setVal__(None)
        #iCount=self.lstVal.GetItemCount()
    def SetTagNames(self,tagName=None,tagNameInt=None,tagNameValue=None):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'tagname:%s,%s,%s'%(tagName,tagNameInt,tagNameValue),
                        self)
        if tagName is not None:
            self.tagName=tagName
        if tagNameInt is not None:
            self.tagNameInt=tagNameInt
        if tagNameValue is not None:
            self.tagNameValue=tagNameValue
    def Enable(self,flag):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        vtInputMultipleTree.Enable(self,flag)
        self.tgFixed.Enable(flag)
        self.slPer.Enable(flag)
        self.viPer.Enable(flag)
        self.tgFixed.Refresh()
    def OnListItemSelected(self,evt):
        self.idxSel=evt.GetIndex()
        self.__convLstValSel__()
    def __convLstValSel__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.idxSelVal=self.idxSel
        if self.VERBOSE:
            vtLog.CallStack('')
            print 'raw sel',self.idxSel
        tup=None
        if self.idxSel<0:
            self.fid=-1
            self.__setVal__(tup)
            return
        try:
            fid=self.lstVal.GetItemData(self.idxSel)
            if fid<0:
                self.fid=-1
                self.idxSel=-1
            else:
                self.idxSelVal=0
                for tup in self.lstSel:
                    if long(tup[0])==fid:
                        break
                    self.idxSelVal+=1
                self.fid=self.docTreeTup[0].ConvertIdNum2Str(fid)
                tup=self.lstSel[self.idxSelVal]
        except:
            vtLog.vtLngTB(self.GetName())
            self.fid=-1
        if self.VERBOSE:
            print 'conv sel',self.idxSel,self.idxSelVal
        self.__setVal__(tup)
    def UpdateLang(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if VERBOSE:
            vtLog.CallStack('')
        try:
            #if self.popWin is None:
            #    sVal=self.doc.getNodeTextLang(self.node,self.tagName,None)
            #else:
            #    sVal=self.popWin.GetVal(self.doc.GetLang())
            self.lstVal.DeleteAllItems()
            self.idxSel=-1
            if self.docTreeTup is None:
                if self.docTreeTup[0] is None:
                    return
            docTree=self.docTreeTup[0]
            docTree.acquire()
            try:
                sInfoName=self.GetName()
                iCols=len(self.lColumns)
                iNum=0
                fPerSum=0.0
                for id,name,bFixed,fPer in self.lstSel:
                    n=docTree.getNodeById(id)
                    if n is not None:
                        tagName,infos=docTree.getNodeInfos(n,self.doc.GetLang(),sInfoName)
                        idx=-1
                        if bFixed==True:
                            img=1
                        else:
                            img=0
                        fPerSum+=fPer
                        
                        idx=self.lstVal.InsertImageStringItem(sys.maxint,'',img)
                        self.lstVal.SetStringItem(idx,1,vtLgBase.format(self.fmt,fPer))
                        
                        for i in range(iCols):
                            self.lstVal.SetStringItem(idx,i+2,infos[self.lShowAttrs[i]])
                        self.lstVal.SetItemData(idx,long(id))
                        try:
                            sName=infos[self.tagNameInt]
                            if name!=sName:
                                self.__markModified__(True)
                            self.lstSel[iNum][1]=sName
                        except:
                            pass
                    iNum+=1
                idx=self.lstVal.InsertImageStringItem(sys.maxint,'',2)
                self.lstVal.SetStringItem(idx,1,vtLgBase.format(self.fmt,fPerSum))
                self.lstVal.SetStringItem(idx,2,_('sum'))
                self.lstVal.SetItemData(idx,-2)
            except:
                vtLog.vtLngTB(self.GetName())
            docTree.release()
            wx.CallAfter(self.__doSortLst__)
            wx.CallAfter(self.__doRefresh__)
            #self.txtVal.SetValue(sVal)
        except:
            vtLog.vtLngTB(self.GetName())
    def __doRefresh__(self):
        self.__calcVal__()
        self.__refreshVal__()
    def SetNode(self,node):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'SetNode;node:%s'%(node),
        #                self)
        if self.VERBOSE:
            vtLog.CallStack('')
            print node
            
        self.Clear()
        self.node=node
        #self.tagName=tagName
        if self.doc is None:
            return
        if self.node is None:
            return
        #if self.tagName is None:
        #    return
        if self.tagGrp is None:
            nodeTmp=self.node
        else:
            nodeTmp=self.doc.getChild(self.node,self.tagGrp)
        try:
            docTree=self.docTreeTup[0]
            for c in self.doc.getChilds(nodeTmp,self.tagName):
                #fid=self.doc.getAttribute(c,'fid')
                fid=self.doc.getForeignKey(c,'fid',self.appl) #self.doc.getChild(c,self.tagName)
                if self.VERBOSE:
                    print 'fid',fid
                if self.appl is not None and fid=='-3':
                    fid=self.doc.getForeignKey(c,'fid',None) #self.doc.getChild(c,self.tagName)
                    vtLog.vtLngCurWX(vtLog.WARN,'fallback id:%s'%fid,self)
                    #netDoc,nodeFor=self.doc.GetNode(fid)
                else:
                    if len(fid)==0:
                        continue
                    #netDoc,nodeFor=self.doc.GetNode('@'fid)
                if self.VERBOSE:
                    print nodeTmp
                n=docTree.getNodeById(fid)
                if n is None:
                    continue
                sFixed=self.doc.getAttribute(c,attr='fixed')
                if sFixed in ['1','True']:
                    bFixed=True
                else:
                    bFixed=False
                fPer=self.doc.GetValue(c,self.tagNameValue,float,0.0)
                sName=self.doc.getText(c)
                self.lstSel.append([fid,sName,bFixed,fPer])
            
            self.__markModified__(False)
        except:
            vtLog.vtLngTB(self.GetName())
        self.UpdateLang()
    def __setNode__(self,node):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        try:
            docTree=self.docTreeTup[0]
            def compFunc(a,b):
                l=long(a[0])-long(b[0])
                if l<0:
                    return -1
                elif l>=0:
                    return 1
                return 0
            if self.VERBOSE:
                vtLog.CallStack('')
                print self.lstSel
            self.lstSel.sort(compFunc)
            for id,sVal,bFixed,fPer in self.lstSel:
                #self.doc.setNodeTextAttr(node,self.tagName,sVal,'fid',id)
                nodeTmp=self.doc.createSubNodeText(node,self.tagName,sVal)
                self.doc.setForeignKey(nodeTmp,attr='fid',attrVal=id,appl=self.appl)
                if bFixed:
                    sFixed='1'
                else:
                    sFixed='0'
                self.doc.setAttribute(nodeTmp,'fixed',sFixed)
                sPer=`fPer`
                self.doc.SetValue(nodeTmp,self.tagNameValue,sPer)
                if self.VERBOSE:
                    print nodeTmp
        except:
            vtLog.vtLngTB(self.GetName())
    def __apply__(self,node):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'__apply__;node:%s'%(node),
        #                self)
        if VERBOSE:
            vtLog.CallStack(node)
        try:
            fid=self.docTreeTup[0].getKey(node)
            bFound=False
            for id,n,bFixed,fPer in self.lstSel:
                if id==fid:
                    bFound=True
                    break
            if bFound==False:
                sName=self.docTreeTup[0].getNodeText(node,self.tagNameInt)
                bFixed=self.tgFixed.GetValue()
                fPer=self.viPer.GetValue()
                self.lstSel.append([fid,sName,bFixed,fPer])
                self.UpdateLang()
                self.__markModified__(True)
                wx.PostEvent(self,vtInputMultipleTreeChanged(self))
        except:
            sVal=_(u'fault')
            vtLog.vtLngTB(self.GetName())
    def OnListColClick(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.idxSel=-1
        idxSelColOld=self.idxSelCol
        self.idxSelCol=evt.GetColumn()
        if self.idxSelCol<2:
            return
        self.idxSelCol-=2
        try:
            sInfoName=self.GetName()
            iCols=len(self.lColumns)
            iNum=0
            sAttr=self.lShowAttrs[self.idxSelCol]
            if idxSelColOld==self.idxSelCol:
                self.bAscend=not self.bAscend
            else:
                self.bAscend=True
        except:
            vtLog.vtLngTB(self.GetName())
        self.__doSortLst__()
    def __doSortLst__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'idxSelCol:%d;asc:%d'%(self.idxSelCol,self.bAscend),
                        self)
        if self.docTreeTup is None:
            if self.docTreeTup[0] is None:
                return
        docTree=self.docTreeTup[0]
        docTree.acquire()
        try:
            sInfoName=self.GetName()
            iCols=len(self.lColumns)
            iNum=0
            sAttr=self.lShowAttrs[self.idxSelCol]
            #if idxSelColOld==self.idxSelCol:
            #    self.bAscend=not self.bAscend
            #else:
            #    self.bAscend=True
            def __cmpListItems__(idA,idB):
                if idA==-2:
                    return 1
                if idB==-2:
                    return -1
                nA=docTree.getNodeByIdNum(idA)
                nB=docTree.getNodeByIdNum(idB)
                tagNameA,infosA=docTree.getNodeInfos(nA,self.doc.GetLang(),sInfoName)
                sValA=infosA[sAttr]
                tagNameB,infosB=docTree.getNodeInfos(nB,self.doc.GetLang(),sInfoName)
                sValB=infosB[sAttr]
                if self.bAscend:
                    return cmp(sValA,sValB)
                else:
                    return cmp(sValB,sValA)
            self.lstVal.SortItems(__cmpListItems__)
        except:
            vtLog.vtLngTB(self.GetName())
        docTree.release()
        iCount=self.lstVal.GetItemCount()
        self.idxSel=-1
        for idx in xrange(iCount):
            if self.lstVal.GetItemState(idx,wx.LIST_STATE_SELECTED)!=0:
                self.idxSel=idx
                break
        self.__convLstValSel__()
        pass
    def __showVal__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.lstVal.DeleteAllItems()
        self.idxSel=-1
        fPerSum=0.0
        for tup in self.lstSel:
            if tup[2]==True:
                sF='F'
                sF=''
                img=1
            else:
                sF=''
                img=0
            fPerSum+=tup[3]
            idx=self.lstVal.InsertImageStringItem(sys.maxint,sF,img)
            self.lstVal.SetStringItem(idx,1,vtLgBase.format(self.fmt,tup[1]))
            self.lstVal.SetStringItem(idx,2,tup[2])
        idx=self.lstVal.InsertImageStringItem(sys.maxint,'',2)
        self.lstVal.SetStringItem(idx,1,vtLgBase.format(self.fmt,fPerSum))
        self.lstVal.SetStringItem(idx,2,'sum')
    def __refreshVal__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        idx=0
        iCount=self.lstVal.GetItemCount()
        if self.VERBOSE:
            vtLog.CallStack('')
            print 'count',iCount
        iCount-=1
        if iCount<0:
            return
        d={}
        fPerSum=0.0
        for tup in self.lstSel:
            if self.VERBOSE:
                print tup
            fPerSum+=tup[3]
            d[long(tup[0])]=tup[3]
        if self.VERBOSE:
            print d
        for idx in xrange(iCount):
            id=self.lstVal.GetItemData(idx)
            if self.VERBOSE:
                print '  ',id
            if id in d:
                fPer=d[id]
            else:
                fPer=0.0
            if self.VERBOSE:
                print fPer
            self.lstVal.SetStringItem(idx,1,vtLgBase.format(self.fmt,fPer))
        self.lstVal.SetStringItem(iCount,1,vtLgBase.format(self.fmt,fPerSum))
    def __setVal__(self,tup):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if tup is None:
            self.tgFixed.Enable(False)
            self.viPer.Enable(False)
            self.slPer.Enable(False)
            self.viPer.SetValue(self.fLimit)
            self.slPer.SetValue(round(self.fLimit))
            
        else:
            self.tgFixed.Enable(True)
            self.viPer.Enable(True)
            self.slPer.Enable(True)
            self.tgFixed.SetValue(tup[2])
            self.viPer.SetValue(tup[3])
            self.slPer.SetValue(round(tup[3]))
    def __calcVal__(self,iSkip=-1):
        lIdx=[]
        idx=0
        fPerFixed=0.0
        for tup in self.lstSel:
            if iSkip>=0:
                if idx==iSkip:
                    idx+=1
                    fPerFixed+=tup[3]
                    continue
            if tup[2]==False:
                lIdx.append(idx)
            else:
                fPerFixed+=tup[3]
            idx+=1
        if len(lIdx)==0:
            return
        if self.VERBOSE:
            vtLog.CallStack('')
            print lIdx,len(lIdx)
        fPerLeft=self.fLimit - fPerFixed
        fPerEqual=fPerLeft / float(len(lIdx))
        if self.VERBOSE:
            print 'fixed',fPerFixed,'left',fPerLeft,'equal',fPerEqual
            print 'before',self.lstSel
        bMod=False
        for idx in lIdx:
            if self.lstSel[idx][3]!=fPerEqual:
                bMod=True
            self.lstSel[idx][3]=fPerEqual
        if self.VERBOSE:
            print 'after',self.lstSel
        if bMod:
            self.__markModified__(True)
    def __calcVal2limitUpdate__(self):
        fGes=0.0
        for tup in self.lstSel:
            fGes+=tup[3]
        if fGes==self.fLimit:
            return
        if self.VERBOSE:
            print 'before',self.lstSel
        for tup in self.lstSel:
            tup[3]=tup[3]/fGes*self.fLimit
        if self.VERBOSE:
            print 'after',self.lstSel
        self.__markModified__(True)
    def OnSlPerScroll(self, event):
        event.Skip()
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.idxSel>=0:
            tup=self.lstSel[self.idxSelVal]
            fVal=float(self.slPer.GetValue())
            tup[3]=fVal
            self.__calcVal__(self.idxSelVal)
            self.__refreshVal__()
            self.viPer.SetValue(fVal)
    def OnViPerVtinputFloatChanged(self, event):
        event.Skip()
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.idxSel>=0:
            tup=self.lstSel[self.idxSelVal]
            fVal=self.viPer.GetValue()
            tup[3]=fVal
            self.__calcVal__(self.idxSelVal)
            self.__refreshVal__()
            self.slPer.SetValue(round(fVal))
            #self.lstVal.SetStringItem(self.idxSel,1,vtLgBase.format(self.fmt,tup[1]))

    def OnTgFixedButton(self, event):
        event.Skip()
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.idxSel>=0:
            tup=self.lstSel[self.idxSelVal]
            bFixed=self.tgFixed.GetValue()
            tup[2]=bFixed
            if tup[2]==True:
                sF=''
                img=1
            else:
                sF=''
                img=0
            self.__calcVal__()
            self.__refreshVal__()
            self.lstVal.SetStringItem(self.idxSel,0,sF,img)
    def GetValueFullLst(self):
        l=[]
        for t in self.lstSel:
            l.append([t[3],t[1],(long(t[0]),self.appl)])
        return l
