#----------------------------------------------------------------------
# Name:         vtInputMultipledValues.py
# Purpose:      text control with tree popup window
#
# Author:       Walter Obweger
#
# Created:      20060619
# CVS-ID:       $Id: vtInputMultipledValues.py,v 1.9 2008/03/26 23:15:29 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
from vidarc.tool.xml.vtXmlDomConsumer import *
from vidarc.tool.xml.vtXmlDomConsumerLang import *
    
VERBOSE=0

# defined event for vtInputMultipleValues item changed
wxEVT_VTINPUT_MULTIPLED_VALUES_CHANGED=wx.NewEventType()
vEVT_VTINPUT_MULTIPLED_VALUES_CHANGED=wx.PyEventBinder(wxEVT_VTINPUT_MULTIPLED_VALUES_CHANGED,1)
def EVT_VTINPUT_MULTIPLED_VALUES_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTINPUT_MULTIPLED_VALUES_CHANGED,func)
class vtInputMultipledValuesChanged(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTINPUT_MULTIPLED_VALUES_CHANGED(<widget_name>, self.OnChanged)
    """

    def __init__(self,obj,val):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTINPUT_MULTIPLED_VALUES_CHANGED)
        self.val=val
    def GetValue(self):
        return self.val
    
# defined event for vtInputMultipleValues item selected
wxEVT_VTINPUT_MULTIPLED_VALUES_SELECTED=wx.NewEventType()
vEVT_VTINPUT_MULTIPLED_VALUES_SELECTED=wx.PyEventBinder(wxEVT_VTINPUT_MULTIPLED_VALUES_SELECTED,1)
def EVT_VTINPUT_MULTIPLED_VALUES_SELECTED(win,func):
    win.Connect(-1,-1,wxEVT_VTINPUT_MULTIPLED_VALUES_SELECTED,func)
class vtInputMultipledValuesSelected(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTINPUT_MULTIPLED_VALUES_SELECTED(<widget_name>, self.OnSelected)
    """

    def __init__(self,obj,val):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTINPUT_MULTIPLED_VALUES_SELECTED)
        self.val=val
    def GetValue(self):
        return self.val
    
# defined event for vtInputMultipleValues item added
wxEVT_VTINPUT_MULTIPLED_VALUES_ADDED=wx.NewEventType()
vEVT_VTINPUT_MULTIPLED_VALUES_ADDED=wx.PyEventBinder(wxEVT_VTINPUT_MULTIPLED_VALUES_ADDED,1)
def EVT_VTINPUT_MULTIPLED_VALUES_ADDED(win,func):
    win.Connect(-1,-1,wxEVT_VTINPUT_MULTIPLED_VALUES_ADDED,func)
class vtInputMultipledValuesAdded(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTINPUT_MULTIPLED_VALUES_ADDED(<widget_name>, self.OnAdded)
    """

    def __init__(self,obj,val):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTINPUT_MULTIPLED_VALUES_ADDED)
        self.val=val
    def GetValue(self):
        return self.val
    
# defined event for vtInputMultipleValues item deleted
wxEVT_VTINPUT_MULTIPLED_VALUES_DELETED=wx.NewEventType()
vEVT_VTINPUT_MULTIPLED_VALUES_DELETED=wx.PyEventBinder(wxEVT_VTINPUT_MULTIPLED_VALUES_DELETED,1)
def EVT_VTINPUT_MULTIPLED_VALUES_DELETED(win,func):
    win.Connect(-1,-1,wxEVT_VTINPUT_MULTIPLED_VALUES_DELETED,func)
class vtInputMultipledValuesDeleted(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTINPUT_MULTIPLED_VALUES_DELETED(<widget_name>, self.OnDeleted)
    """

    def __init__(self,obj,val):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTINPUT_MULTIPLED_VALUES_DELETED)
        self.val=val
    def GetValue(self):
        return self.val
    

class vtInputMultipledValuesTransientPopup(wx.Dialog):
    #SIZE_PN_SCROLL=30
    def __init__(self, parent, size,style,name=''):
        self.SIZE_PN_SCROLL=wx.SystemSettings.GetMetric(wx.SYS_VSCROLL_X)
        wx.Dialog.__init__(self, parent,style=wx.RESIZE_BORDER)#wx.BORDER_SIMPLE)#|wx.STAY_ON_TOP)
        id=wx.NewId()
        wx.EVT_SIZE(self,self.OnSize)
        
        fgs = wx.FlexGridSizer(cols=1, hgap=0, rows=2, vgap=0)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        self.cbCancel = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Cancel), name=u'cbCancel',
              parent=self, pos=(0,0), size=(30,30), style=wx.BU_AUTODRAW)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              self.cbCancel)
        bxs.AddWindow(self.cbCancel, 2, border=4, flag=0)
        
        self.cbApply = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Apply), name=u'cbApply',
              parent=self, pos=wx.Point(32, 00), size=wx.Size(30, 30),
              style=wx.BU_AUTODRAW)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              self.cbApply)
        bxs.AddWindow(self.cbApply, 2, border=4, flag=wx.LEFT)
        
        self.cbAdd = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Add), name=u'cbAdd',
              parent=self, pos=wx.Point(64, 00), size=wx.Size(30, 30),
              style=wx.BU_AUTODRAW)
        self.cbAdd.Bind(wx.EVT_BUTTON, self.OnCbAddButton,
              self.cbAdd)
        bxs.AddWindow(self.cbAdd, 2, border=4, flag=wx.LEFT)
        
        self.cbDel = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Del), name=u'cbDel',
              parent=self, pos=wx.Point(96, 00), size=wx.Size(30, 30),
              style=wx.BU_AUTODRAW)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              self.cbDel)
        bxs.AddWindow(self.cbDel, 2, border=4, flag=wx.LEFT)
        
        fgs.AddSizer(bxs, 0, border=4, flag=wx.ALL)
        
        if size[1]<40:
            size=(size[0],40)
        i=0
        iY=40
        iH=size[1]+4
        iSzHeight=0
        sVal=parent.GetValue()
        
        self.txtVal = wx.TextCtrl(id=-1, name='txtVal',
              parent=self, pos=wx.Point(4, 40), size=wx.Size(100, 21), style=0,
              value='')
        fgs.AddWindow(self.txtVal, 0, border=4, flag=wx.ALL|wx.EXPAND)
        
        fgs.AddGrowableCol(0)
        self.SetSizer(fgs)
        self.Layout()
        #self.szOrig=(200,244)
        #self.SetSize(self.szOrig)
        self.szOrig=(40,40)
        self.Fit()
    def OnSize(self,evt):
        iW,iH=self.GetSize()
        if iW<self.szOrig[0]:
            iW=self.szOrig[0]
        #if iH<self.szMin[1]:
        #    iH=self.szMin[1]
        if iH<self.szOrig[1]:
            iH=self.szOrig[1]
        self.SetSize((iW,iH))
        evt.Skip()
    def OnCbCancelButton(self,evt):
        self.Show(False)
    def OnCbApplyButton(self,evt):
        #self.Apply()
        try:
            par=self.GetParent()
            sVal=self.txtVal.GetValue()
            par.__apply__(sVal)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        self.Show(False)
    def OnCbAddButton(self,evt):
        try:
            par=self.GetParent()
            sVal=self.txtVal.GetValue()
            par.__apply__(sVal,bAdd=True)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        self.Show(False)
    def OnCbDelButton(self,evt):
        try:
            par=self.GetParent()
            sVal=self.txtVal.GetValue()
            par.__apply__(sVal,bDel=True)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        self.Show(False)
    def Show(self,flag):
        try:
            par=self.GetParent()
            if flag:
                #vtLog.CallStack('')
                #print par.GetValueIdx()
                if par.GetValueIdx()<0:
                    self.cbApply.Enable(False)
                    self.cbDel.Enable(False)
                else:
                    self.cbApply.Enable(True)
                    self.cbDel.Enable(True)
                sVal=par.GetValueStr()
                self.SetVal(sVal)
                #self.calCalendar.Refresh()
                #self.calCalendar.Update()
            else:
                #par.cbPopup.SetValue(False)
                par.__setPopupState__(False)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        wx.Dialog.Show(self,flag)
    def GetVal(self):
        try:
            sVal=self.txtVal.GetValueStr()
            return sVal
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
            return ''
    def SetVal(self,sVal):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'val:%s'%(sVal),self)
        try:
            self.txtVal.SetValue(sVal)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
class vtInputMultipledValues(wx.Panel,vtXmlDomConsumer,vtXmlDomConsumerLang):
    VERBOSE=0
    def __init__(self,*_args,**_kwargs):
        global _
        _=vtLgBase.assignPluginLang('vtInput')
        vtXmlDomConsumer.__init__(self)
        vtXmlDomConsumerLang.__init__(self)
        try:
            sz=_kwargs['size']
        except:
            sz=wx.Size(120,30)
            _kwargs['size']=sz
        try:
            szCb=_kwargs['size_button']
            del _kwargs['size_button']
        except:
            szCb=wx.Size(24,24)
        try:
            szTxt=_kwargs['size_text']
            del _kwargs['size_text']
        except:
            szTxt=wx.Size(sz[0]-szCb[0]-2,sz[1]-4)
        
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        self.SetAutoLayout(True)
        
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        size=(szTxt[0],szTxt[1])
        pos=(0,(sz[1]-szTxt[1])/2)
        self.chcMult = wx.Choice(id=-1, name='chcMult',
              parent=self, pos=pos, size=size,
              style=0, choices=[])
        self.chcMult.SetMinSize((-1,-1))
        #self.chcMult.SetToolTipString('YYYY-MM-DD')
        #self.chcMult.SetConstraints(LayoutAnchors(self.chcMult, True, True,
        #      True, False))
        bxs.AddWindow(self.chcMult, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        #self.chcMult.Bind(wx.EVT_TEXT, self.OnTextText,self.chcMult)
        self.chcMult.Bind(wx.EVT_CHOICE, self.OnChoice,self.chcMult)
        
        size=(szCb[0],szCb[1])
        pos=(szTxt[0]+(sz[0]-(szTxt[0]+szCb[0]))/2,(sz[1]-szCb[1])/2)
        id=wx.NewId()
        self.cbPopup = wx.lib.buttons.GenBitmapToggleButton(ID=id,
              bitmap=wx.EmptyBitmap(8, 8), name=u'cbPopup',
              parent=self, pos=pos, size=size, style=wx.BU_AUTODRAW)
        #self.cbPopup.SetConstraints(LayoutAnchors(self.cbPopup, False, True,
        #      True, False))
        bxs.AddWindow(self.cbPopup, 0, border=0, flag=wx.EXPAND)
        self.SetSizer(bxs)
        self.cbPopup.SetBitmapLabel(vtArt.getBitmap(vtArt.Edit))
        self.cbPopup.SetBitmapSelected(vtArt.getBitmap(vtArt.Edit))
        self.cbPopup.Bind(wx.EVT_BUTTON,self.OnPopupButton,self.cbPopup)#id=id)
        self.popWin=None
        self.tagName='multipled'
        self.bBlock=True
        self.lMultipled=[]
        self.args=()
        self.kwargs={}
        self.func=None
        self.lWid=[]
        #self.dftBkgColor=self.chcMult.GetBackgroundColour()
        self.bMod=False
        self.bEnableMark=True
        self.bBusy=False
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
    def SetCB(self,func,*args,**kwargs):
        self.func=func
        self.args=args
        self.kwargs=kwargs
    def DoCB(self,idx):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'idx:%d'%(idx),self)
        try:
            if self.VERBOSE:
                vtLog.CallStack('')
                print idx
                print self.lMultipled
            node=None
            try:
                if idx>=0:
                    tup=self.lMultipled[idx]
                    node=tup[2]
            except:
                pass
        except:
            vtLog.vtLngTB(self.GetName())
        if self.func is not None:
            self.func(node,*self.args,**self.kwargs)
    def EnableEdit(self,flag):
        self.cbPopup.Show(flag)
    def Enable(self,flag):
        wx.Panel.Enable(self,flag)
        self.cbPopup.Refresh()
    def SetEnableMark(self,flag):
        self.bEnableMark=flag
    def SetModified(self,flag):
        self.__markModified__(flag)
    def IsModified(self):
        return self.bMod
    def __markModified__(self,flag=True):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.bEnableMark==False:
            return
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'__markModified__;flag:%s'%(flag),
        #                self)
        if flag:
            f=self.chcMult.GetFont()
            f.SetWeight(wx.FONTWEIGHT_BOLD)
            self.chcMult.SetFont(f)
        else:
            f=self.chcMult.GetFont()
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
            self.chcMult.SetFont(f)
        self.chcMult.Refresh()
        #if store_mod:
        self.bMod=flag
    def __markFlt__(self,flag=False):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if flag:
            color=wx.TheColourDatabase.Find('YELLOW')
            self.chcMult.SetBackgroundColour(color)
        else:
            color=wx.SystemSettings_GetColour(wx.SYS_COLOUR_WINDOW)
            self.chcMult.SetBackgroundColour(color)
    def SetTagNames(self,tagName):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'SetTagNames;tagname:%s,%s'%(tagName,tagNameInt),
        #                self)
        self.tagName=tagName
    SetTagName=SetTagNames
    def ClearLang(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'ClearLang',
        #                self)
        #if VERBOSE:
        #    vtLog.CallStack('')
        self.chcMult.Clear()
        pass
    def UpdateLang(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'UpdateLang',
        #                self)
        return
        #if VERBOSE:
        #    vtLog.CallStack('')
        try:
            if self.popWin is None:
                sVal=self.doc.getNodeText(self.node,self.tagName)
            else:
                sVal=self.popWin.GetVal(self.doc.GetLang())
            self.bBlock=True
            self.chcMult.SetValue(sVal.split('\n')[0])
            wx.CallAfter(self.__clearBlock__)
            #self.chcMult.SetValue(sVal)
        except:
            vtLog.vtLngTB(self.GetName())
    def __clearInt__(self):
        self.lMultipled=[]
        self.ClearLang()
        vtXmlDomConsumer.Clear(self)
        self.__markModified__(False)
    def Clear(self):
        self.__clearInt__()
        self.DoCB(-1)
    def __setPopupState__(self,flag):
        self.bBusy=flag
        self.cbPopup.SetValue(flag)
    def IsBusy(self):
        return self.bBusy
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.popWin is not None:
            return self.popWin.IsShown()
        else:
            return False
    def __Close__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.popWin is not None:
            self.popWin.Show(False)
            self.__setPopupState__(False)
    def Stop(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'Stop',self)
        wx.CallAfter(self.__Close__)
    def SetDoc(self,doc):
        vtXmlDomConsumer.SetDoc(self,doc)
        vtXmlDomConsumerLang.SetDoc(self,doc)
    def __addMultipled__(self,node,*args):
        sTag=self.doc.getTagName(node)
        if sTag==self.tagName:
            sVal=self.doc.getAttribute(node,'multiple')
            if self.VERBOSE:
                print sVal
            try:
                self.lMultipled.append([sVal,sVal,node,len(self.lMultipled)])
            except:
                self.__markModified__(True)
                vtLog.vtLngTB(self.GetName())
                pass
        return 0
    def SetNode(self,node,val=None):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'SetNode;node:%s'%(node),
        #                self)
        if self.VERBOSE:
            vtLog.CallStack('')
            print node
        self.Clear()
        vtXmlDomConsumer.SetNode(self,node)
        if self.doc is None:
            return
        if self.node is None:
            return
        try:
            self.doc.procChilds(self.node,self.__addMultipled__)
            self.__showMultiple__(val=val)
        except:
            vtLog.vtLngTB(self.GetName())
        #self.__getSelNode__()
        self.UpdateLang()
    def __showMultiple__(self,val=None,bDoCB=True):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'val:%s;bDoCB:%d'%(val,bDoCB),self)
            self.lMultipled.sort()
            self.chcMult.Clear()
            for tup in self.lMultipled:
                self.chcMult.Append(tup[0],tup[-1])
            
            self.bBlock=True
            self.chcMult.Enable(True)
            self.cbPopup.Enable(True)
            if len(self.lMultipled)>0:
                iSel=-1
                if val is not None:
                    i=0
                    for tup in self.lMultipled:
                        if tup[0]==val:
                            iSel=i
                            break
                        i+=1
                
                if iSel>=0:
                    wx.CallAfter(self.chcMult.SetSelection,iSel)
                    if bDoCB:
                        self.DoCB(iSel)
                else:
                    self.chcMult.SetSelection(0)
                    if bDoCB:
                        self.DoCB(0)
            else:
                if bDoCB:
                    self.DoCB(-1)
            wx.CallAfter(self.__clearBlock__)
        except:
            vtLog.vtLngTB(self.GetName())
    def __validate__(self,sVal,bSet2NowOnFlt=False):
        #if VERBOSE:
        #    vtLog.CallStack(sVal)
        bMod=False
        bOk=False
        try:
            #self.dt.SetDateStr(sVal)
            bOk=True
        except:
            #vtLog.vtLngCurWX(vtLog.WARN,sVal,self)
            if bSet2NowOnFlt:
                bMod=True
        if bMod:
            if bSet2NowOnFlt:
                self.dt=self.dt.Now()
        return bOk,bMod,sVal
    def __clearBlock__(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'__clearBlock__',
        #                self)
        self.bBlock=False
    def Enable(self,flag):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.chcMult.Enable(flag)
        self.cbPopup.Enable(flag)
    def __setMultipled__(self,node,*args):
        i=args[0][0]
        nodePar=args[0][1]
        sTag=self.doc.getTagName(node)
        if sTag==self.tagName:
            sVal=self.doc.getAttribute(node,'multiple')
            if self.VERBOSE:
                print sVal
            for tup in self.lMultipled:
                if tup[1]==sVal:
                    if tup[0]!=None:
                        if tup[0]!=tup[1]:
                            self.doc.setAttribute(node,'multiple',tup[0])
                        if i==self.iAct:
                            self.node2Sel=node
                    else:
                        self.doc.deleteNode(node,nodePar)
            self.iAct+=1
        return 0
    def GetNode(self,node=None):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if node is None:
            node=self.node
        if node is None:
            return
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'GetNode',
        #                self)
        if self.VERBOSE:
            vtLog.CallStack('')
        try:
            val=None
            try:
                idx=self.chcMult.GetSelection()
                i=self.chcMult.GetClientData(idx)
                val=self.chcMult.GetStringSelection()
            except:
                i=-1
            self.iAct=0
            self.node2Sel=None
            self.doc.procChilds(node,self.__setMultipled__,i,node)
            if self.VERBOSE:
                print self.node2Sel
            for tup in self.lMultipled:
                if tup[1] is None:
                    if tup[0] is not None:
                        c=self.doc.createSubNode(node,self.tagName)
                        self.doc.setAttribute(c,'multiple',tup[0])
                        self.node2Sel=c
                        tup[2]=c
            if self.VERBOSE:
                print self.lMultipled
            l=[]
            i=0
            iDel=0
            for tup in self.lMultipled:
                if tup[0] is not None:
                    tup[1]=tup[0]
                    tup[3]=tup[3]-iDel#i
                    l.append(tup)
                    i+=1
                else:
                    iDel+=1
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                lOld=[[t[0],t[1],t[-1]] for t in self.lMultipled]
                lNew=[[t[0],t[1],t[-1]] for t in l]
                vtLog.vtLngCurWX(vtLog.DEBUG,'lMultipled:%s;l:%s'%
                        (vtLog.pformat(lOld),vtLog.pformat(lNew)),self)
            bDoCB=len(self.lMultipled)!=len(l)
            self.lMultipled=l
            if self.VERBOSE:
                print self.node2Sel
            wx.CallAfter(self.__showMultiple__,val=val,bDoCB=True)
            self.Enable(True)
            self.__markModified__(False)
            self.doc.AlignNode(node)
            return self.node2Sel
        except:
            vtLog.vtLngTB(self.GetName())
        return None
    def GetValue(self,node=None):
        if node is None:
            node=self.node
        if node is None:
            return ''
        try:
            tmp=self.GetNode(node)
            if tmp is not None:
                return self.doc.getAttribute(tmp,'multiple')
        except:
            vtLog.vtLngTB(self.GetName())
        return ''
    def __getDoc__(self):
        return self.doc
    def __getNode__(self,node=None):
        if node is None:
            return self.node
        return node
    def __apply__(self,sVal,bAdd=False,bDel=False):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'val:%s,bAdd:%d;bDel:%d'%
                        (sVal,bAdd,bDel),
                        self)
        #if VERBOSE:
        #    vtLog.CallStack(sVal)
        try:
            #sVal=self.popWin.GetVal()
            #if self.trSetNode is not None:
            #    sVal=self.trSetNode(self.docTreeTup[0],node)
            #else:
            #    sVal=''
            bOk,bMod,sVal=self.__validate__(sVal)
            self.__markFlt__(not bOk)
            if bAdd==False:
                try:
                    idx=self.chcMult.GetSelection()
                    i=self.chcMult.GetClientData(idx)
                    if bDel==False:
                        s=self.chcMult.GetStringSelection()
                        self.lMultipled[idx][0]=sVal
                        if sVal!=s:
                            bMod=True
                        self.chcMult.SetString(idx,sVal)
                        self.chcMult.SetSelection(idx)
                        if bMod:
                            if self.bBlock==False:
                                self.__markModified__(True)
                                wx.PostEvent(self,vtInputMultipledValuesChanged(self,sVal))
                    else:
                        self.lMultipled[idx][0]=None
                        #self.chcMult.Delete(idx)
                        #self.chcMult.SetSelection(0)
                        self.chcMult.Enable(False)
                        self.cbPopup.Enable(False)
                        #wx.CallAfter(self.DoCB,0)
                        #wx.CallAfter(self.OnChoice,None)
                        wx.PostEvent(self,vtInputMultipledValuesDeleted(self,sVal))
                except:
                    bMod=True
            else:
                self.lMultipled.append([sVal,None,None,len(self.lMultipled)])
                bMod=True
                self.chcMult.Append(sVal,len(self.lMultipled)-1)
                self.chcMult.SetSelection(len(self.lMultipled)-1)
                self.chcMult.Enable(False)
                self.cbPopup.Enable(False)
                wx.PostEvent(self,vtInputMultipledValuesAdded(self,sVal))
        except:
            sVal=u'****'
            vtLog.vtLngTB(self.GetName())
        self.bBlock=True
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'lMultipled:%s'%
                        (vtLog.pformat(self.lMultipled)),
                        self)
        #self.chcMult.SetValue(string.split(sVal,'\n')[0])
        wx.CallAfter(self.__clearBlock__)
        return sVal
        
    def __createPopup__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'__createPopup__',
        #                self)
        if self.popWin is None:
            sz=self.GetSize()
            sz=(sz[0],sz[1]-20)
            try:
                self.popWin=vtInputMultipledValuesTransientPopup(self,sz,wx.SIMPLE_BORDER)
            except:
                vtLog.vtLngTB(self.GetName())
            #self.popWin.SetVal(self.chcMult.GetValue(),self.doc.GetLang())
        else:
            pass
    def SetValueStr(self,sVal):
        #self.chcMult.SetValue(val)
        self.__apply__(sVal[0:4]+sVal[5:7]+sVal[8:10])
        #self.__apply__(sVal)
        #self.__validate__(sVal,bTxt=True)
    def SetValue(self,sVal):
        #self.chcMult.SetValue(val)
        self.__apply__(sVal)
    def GetValueIdx(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'%d %s'%(self.chcMult.GetSelection(),self.chcMult.GetStringSelection()),self)
        return self.chcMult.GetSelection()
    def GetValueStr(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'%d %s'%(self.chcMult.GetSelection(),self.chcMult.GetStringSelection()),self)
        return self.chcMult.GetStringSelection()
    def GetValue(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        sVal=self.chcMult.GetStringSelection()
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%sVal,self)
        return sVal
            
    def OnTextText(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.bBlock:
            return
        sVal=self.chcMult.GetValue()
        try:
            bOk,bMod,val=self.__validate__(sVal,False)
            self.__markFlt__(not bOk)
            if bMod:
                self.chcMult.SetValue(val)
        except:
            vtLog.vtLngTB(self.GetName())
        self.__markModified__()
        wx.PostEvent(self,vtInputMultipledChanged(self,sVal))
    def OnChoice(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        try:
            sVal=self.chcMult.GetStringSelection()
            idx=self.chcMult.GetSelection()
        except:
            sVal=''
            idx=-1
        self.DoCB(idx)
        wx.PostEvent(self,vtInputMultipledValuesSelected(self,sVal))
    def OnPopupButton(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.__createPopup__()
        if self.cbPopup.GetValue()==True:
            btn=self
            iX,iY = btn.ClientToScreen( (0,0) )
            iW,iH =  btn.GetSize()
            iPopW,iPopH=self.popWin.GetSize()
            iScreenW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            iScreenH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            if iX+iPopW>iScreenW:
                iX=iScreenW-iPopW-4
                if iX<0:
                    iX=0
            iY+=iH
            if iY+iPopH>iScreenH:
                iY=iScreenH-iPopH
                if iY<0:
                    iY=0
            
            self.popWin.Move((iX,iY))
            #self.popWin.Move((pos[0],pos[1]+sz[1]))
            self.bBusy=True
            self.popWin.Show(True)
        else:
            self.popWin.Show(False)
            self.bBusy=False
        evt.Skip()

