#----------------------------------------------------------------------------
# Name:         vtInputTag.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051120
# CVS-ID:       $Id: vtInputTag.py,v 1.4 2008/03/22 14:34:25 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from vidarc.tool.input.vtInputText import *
#from vidarc.tool.xml.vtXmlDomConsumer import *
import re

VERBOSE=0

class vtInputTag(vtInputText):
    def __init__(self,*_args,**_kwargs):
        vtInputText.__init__(self,*_args,**_kwargs)
        self.bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW)
        #self.reMatch=re.compile('[a-z]*[A-Z]*[0-9]*')
        self.reMatch=re.compile('[a-zA-Z0-9]+')
    def __markModified__(self,flag=True):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.bEnableMark==False:
            return
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'__markModified__;flag:%s'%(flag),
        #                self)
        if flag:
            f=self.txtVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_BOLD)
            self.txtVal.SetFont(f)
        else:
            f=self.txtVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
            self.txtVal.SetFont(f)
        self.__markValid__()
        self.bMod=flag
        self.txtVal.Refresh()
    def __markValid__(self,flag=True):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        sVal=self.txtVal.GetValue()
        try:
            o=self.reMatch.match(sVal)
            bFlt=False
            if o is None:
                bFlt=True
            else:
                if o.start()!=0 or o.end()!=len(sVal):
                    bFlt=True
            if bFlt:
                self.txtVal.SetBackgroundColour("YELLOW")
            else:
                self.txtVal.SetBackgroundColour(self.bkgCol)
        except:
            pass
    def SetNode(self,node,tagName=None):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if VERBOSE:
            vtLog.CallStack('')
            print node
            print tagName
            
        self.bBlock=True
        self.Clear()
        self.__markModified__(False)
        wx.CallAfter(self.__clearBlock__)
        self.node=node
        if tagName is not None:
            self.tagName=tagName
        if self.doc is None:
            return
        if self.node is None:
            return
        if self.tagName is None:
            return
        sVal=self.doc.getNodeText(node,self.tagName).split('\n')[0]
        self.txtVal.SetValue(sVal)
        self.__markValid__()
    def GetNode(self,node=None):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if node is None:
            node=self.node
        if node is None:
            return
        try:
            sVal=self.GetValue()
            self.__markModified__(False)
            self.doc.setNodeText(node,self.tagName,sVal)
        except:
            vtLog.vtLngTB(self.GetName())
    def SetValue(self,sVal):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.__markModified__(False)
        self.bBlock=True
        self.txtVal.SetValue(sVal)
        self.__markValid__()
        wx.CallAfter(self.__clearBlock__)
    def GetValue(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        sVal=self.txtVal.GetValue()
        try:
            o=self.reMatch.match(sVal)
            bFlt=False
            if o is None:
                bFlt=True
            else:
                if o.start()!=0 or o.end()!=len(sVal):
                    bFlt=True
            if bFlt:
                return ''
            else:
                return sVal
        except:
            sVal=''
        return sVal
    def OnTextText(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.bBlock:
            return
        self.__markModified__()
        sVal=self.txtVal.GetValue()
        wx.PostEvent(self,vtInputTextChanged(self,sVal))
        
