#----------------------------------------------------------------------
# Name:         vtInputTextMultiLine.py
# Purpose:      text control with popup window
#
# Author:       Walter Obweger
#
# Created:      20060218
# CVS-ID:       $Id: vtInputTextMultiLine.py,v 1.8 2008/03/22 14:34:25 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from vidarc.tool.xml.vtXmlDomConsumer import *

import vidarc.tool.log.vtLog as vtLog

VERBOSE=0

# defined event for vtInputTextML item changed
wxEVT_VTINPUT_TEXTMULTILINE_CHANGED=wx.NewEventType()
vEVT_VTINPUT_TEXTMULTILINE_CHANGED=wx.PyEventBinder(wxEVT_VTINPUT_TEXTMULTILINE_CHANGED,1)
def EVT_VTINPUT_TEXTMULTILINE_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTINPUT_TEXTMULTILINE_CHANGED,func)
class vtInputTextMultiLineChanged(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTINPUT_TEXTMULTILINE_CHANGED(<widget_name>, self.OnChanged)
    """

    def __init__(self,obj,val):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTINPUT_TEXTMULTILINE_CHANGED)
        self.val=val
    def GetValue(self):
        return self.val
    

class vtInputTextMultiLine(wx.Panel,vtXmlDomConsumer):
    def __init__(self,*_args,**_kwargs):
        vtXmlDomConsumer.__init__(self)
        try:
            sz=_kwargs['size']
        except:
            sz=wx.Size(100,50)
            _kwargs['size']=sz
        
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        self.SetMinSize(wx.Size(-1, -1))
        #self.SetAutoLayout(True)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        self.txtVal = wx.TextCtrl(id=-1, name='txtVal',
              parent=self, pos=(0,0), size=sz,
              style=wx.TE_MULTILINE, value='')
        self.txtVal.SetMinSize(wx.Size(-1, -1))
        bxs.AddWindow(self.txtVal, 1, border=0, flag=wx.EXPAND|wx.ALL)
        #self.txtVal.SetConstraints(LayoutAnchors(self.txtVal, True, True,
        #      True, False))
        self.txtVal.Bind(wx.EVT_TEXT, self.OnTextText,self.txtVal)
        self.SetSizer(bxs)
        
        self.tagName=None
        self.bBlock=False
        self.bMod=False
        self.bEnableMark=True
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
    def SetEditable(self,flag):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.txtVal.SetEditable(flag)
    def SetEnableMark(self,flag):
        self.bEnableMark=flag
    def SetModified(self,flag):
        self.__markModified__(flag)
    def IsModified(self):
        return self.bMod
    def __markModified__(self,flag=True):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'__markModified__;flag:%s'%(flag),
        #                self)
        if self.bEnableMark==False:
            return
        if flag:
            f=self.txtVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_BOLD)
            self.txtVal.SetFont(f)
        else:
            f=self.txtVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
            self.txtVal.SetFont(f)
        self.bMod=flag
        self.txtVal.Refresh()
    def Clear(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if VERBOSE:
            vtLog.CallStack('')
        #self.tagName=None
        self.txtVal.SetValue('')
        self.node=None
    def IsBusy(self):
        return False
    def Stop(self):
        pass
    def SetDoc(self,doc):
        vtXmlDomConsumer.SetDoc(self,doc)
    def SetTagName(self,tagName):
        self.tagName=tagName
    def SetNode(self,node,tagName=None):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if VERBOSE:
            vtLog.CallStack('')
            print node
            print tagName
            
        self.bBlock=True
        self.Clear()
        self.node=node
        if tagName is not None:
            self.tagName=tagName
        if self.doc is None:
            return
        if self.node is None:
            return
        if self.tagName is None:
            return
        self.__markModified__(False)
        sVal=self.doc.getNodeText(node,self.tagName)
        self.txtVal.SetValue(sVal)
        wx.CallAfter(self.__clearBlock__)
    def GetNode(self,node=None):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if node is None:
            node=self.node
        if node is None:
            return
        try:
            sVal=self.txtVal.GetValue()
            self.__markModified__(False)
            self.doc.setNodeText(node,self.tagName,sVal)
        except:
            vtLog.vtLngTB(self.GetName())
    def SetValue(self,sVal):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.SetModified(False)
        self.bBlock=True
        self.txtVal.SetValue(sVal)
        wx.CallAfter(self.__clearBlock__)
    def GetValue(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        return self.txtVal.GetValue()
    def __clearBlock__(self):
        self.bBlock=False
    def __getDoc__(self):
        return self.doc
    def __getNode__(self,node=None):
        if node is None:
            return self.node
        else:
            return node
    def __getTagName__(self):
        return self.tagName
    def OnTextText(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.bBlock:
            return
        self.__markModified__()
        sVal=self.txtVal.GetValue()
        wx.PostEvent(self,vtInputTextMultiLineChanged(self,sVal))
