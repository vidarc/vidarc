#----------------------------------------------------------------------------
# Name:         vtInputAttrValue.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051120
# CVS-ID:       $Id: vtInputFloat.py,v 1.8 2010/05/19 21:59:26 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.masked.maskededit
import vidarc.tool.lang.vtLgBase as vtLgBase

import vidarc.tool.log.vtLog as vtLog

wxEVT_VTINPUT_FLOAT_CHANGED=wx.NewEventType()
vEVT_VTINPUT_FLOAT_CHANGED=wx.PyEventBinder(wxEVT_VTINPUT_FLOAT_CHANGED,1)
def EVT_VTINPUT_FLOAT_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTINPUT_FLOAT_CHANGED,func)
class vtInputFloatChanged(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTINPUT_FLOAT_CHANGED(<widget_name>, self.OnChanged)
    """

    def __init__(self,obj,text):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTINPUT_FLOAT_CHANGED)
        self.text=text
    def GetValue(self):
        return self.text

class vtInputFloat(wx.Panel):
    def __init__(self,*_args,**_kwargs):
        try:
            sz=_kwargs['size']
        except:
            sz=wx.Size(100,22)
            _kwargs['size']=sz
        try:
            self.fDft=_kwargs['default']
            del _kwargs['default']
        except:
            self.fDft=0.0
        try:
            fMin=_kwargs['minimum']
            del _kwargs['minimum']
        except:
            fMin=0.0
        try:
            fMax=_kwargs['maximum']
            del _kwargs['maximum']
        except:
            fMax=100.0
        try:
            iIntW=_kwargs['integer_width']
            del _kwargs['integer_width']
        except:
            iIntW=8
        try:
            iFriW=_kwargs['fraction_width']
            del _kwargs['fraction_width']
        except:
            iFriW=2
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        self.SetAutoLayout(True)
        
        bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.bBlock=True
        self.bEnableMark=True
        self.bMod=False
        id=wx.NewId()
        decSep,thousSep=vtLgBase.getNumSeperatorsFromNumMask()
        self.mskNumValue = wx.lib.masked.numctrl.NumCtrl(id=id,
              name=u'mskNum', parent=self, pos=wx.Point(0, 0), size=sz, style=0, value=0,
              decimalChar=decSep,groupChar=thousSep)
        bxs.AddWindow(self.mskNumValue, 1, border=0, flag=wx.EXPAND|wx.ALL)
        self.SetSizer(bxs)
        self.mskNumValue.Bind(wx.EVT_TEXT,self.OnTxtChanged,id=id)
        self.mskNumValue.SetCtrlParameters(formatcodes='R>-,',
                                emptyBackgroundColour=bkgCol,
                                validBackgroundColour=bkgCol)
        self.mskNumValue.SetMin(fMin)
        self.mskNumValue.SetMax(fMax)
        self.mskNumValue.SetIntegerWidth(iIntW)
        self.mskNumValue.SetFractionWidth(iFriW)
        self.__genToolTip__(fMin,fMax)
        self.mskNumValue.SetValue(self.fDft)
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        wx.CallAfter(self.__clearBlock__)
        self.tagName=None
    def __genToolTip__(self,fMin,fMax):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        iIntW=self.mskNumValue.GetIntegerWidth()
        iFriW=self.mskNumValue.GetFractionWidth()
        fmt='%'+str(iIntW)+'.'+str(iFriW)+'f'
        sBase=getattr(self,'_tip',u'')
        s=u''.join([sBase,_(u'range from'),u' ',
                vtLgBase.format(fmt,fMin),u' ',_(u'to'),u' ',
                vtLgBase.format(fmt,fMax)])
        self.mskNumValue.SetToolTipString(s)
    def SetToolTipString(self,sTip):
        self._tip=sTip[:]
        self.__genToolTip__(self.mskNumValue.GetMin(),
                self.mskNumValue.GetMax())
    def SetLimit(self,fMin,fMax):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.mskNumValue.SetMin(fMin)
        self.mskNumValue.SetMax(fMax)
        self.__genToolTip__(fMin,fMax)
    def SetTagName(self,tagName):
        self.tagName=tagName
    def SetEnableMark(self,flag):
        self.bEnableMark=flag
    def SetModified(self,flag):
        self.__markModified__(flag)
    def IsModified(self):
        return self.bMod
    def __markModified__(self,flag=True):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'__markModified__;flag:%s'%(flag),
        #                self)
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.bEnableMark==False:
            return
        if flag:
            f=self.mskNumValue.GetFont()
            f.SetWeight(wx.FONTWEIGHT_BOLD)
            self.mskNumValue.SetFont(f)
        else:
            f=self.mskNumValue.GetFont()
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
            self.mskNumValue.SetFont(f)
        self.bMod=flag
        self.mskNumValue.Refresh()
    def __clearBlock__(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'__clearBlock__',
        #                self)
        self.bBlock=False
    def Clear(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.bBlock=True
        self.__markModified__(False)
        self.mskNumValue.ClearValue()
        wx.CallAfter(self.__clearBlock__)
    def Enable(self,state):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.mskNumValue.Enable(state)
        self.mskNumValue.Refresh()
    def SetValue(self,val):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.bBlock=True
        self.__markModified__(False)
        self.mskNumValue.SetValue(val)
        wx.CallAfter(self.__clearBlock__)
    def GetValue(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        return self.mskNumValue.GetValue()
    def SetNode(self,node,doc):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.bBlock=True
        self.__markModified__(False)
        try:
            sVal=float(doc.getNodeText(node,self.tagName))
        except:
            sVal=self.fDft
        self.mskNumValue.SetValue(sVal)
        wx.CallAfter(self.__clearBlock__)
    def GetNode(self,node,doc):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.bBlock=True
        self.__markModified__(False)
        try:
            doc.setNodeText(node,self.tagName,str(self.mskNumValue.GetValue()))
        except:
            vtLog.vtLngTB(self.GetName())
        wx.CallAfter(self.__clearBlock__)
    def OnTxtChanged(self,event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.bBlock:
            return
        sVal=self.mskNumValue.GetValue()
        self.__markModified__()
        wx.PostEvent(self,vtInputFloatChanged(self,sVal))

