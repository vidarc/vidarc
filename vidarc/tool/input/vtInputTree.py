#----------------------------------------------------------------------
# Name:         vtInputTree.py
# Purpose:      text control with tree popup window
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vtInputTree.py,v 1.44 2015/02/27 18:49:34 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
from wx.lib.anchors import LayoutAnchors

import cStringIO

import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlTree import EVT_VTXMLTREE_THREAD_ADD_ELEMENTS
from vidarc.tool.xml.vtXmlTree import EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_ABORTED
from vidarc.tool.xml.vtXmlTree import EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED
#from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_GOT_CONTENT
#from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_BUILD_TREE

#from vidarc.tool.xml.vtXmlGrpTree import *
from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer
from vidarc.tool.xml.vtXmlDomConsumerLang import vtXmlDomConsumerLang

from vGuiCorePanWX import vGuiCorePanWX

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
VERBOSE_MARKUP=vcLog.is2Log(__name__,'__VERBOSE_MARKUP__')

#----------------------------------------------------------------------
def getDownData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x83IDAT8\x8d\xb5\x92\xd1\r\x84 \x10D\xdf\xaa5l\x05\xd4y\xa2\xdfj\
\x85TA\x03\xde\x17fUH\xee\x02\xcc\x17\t\xcc\xcc\xdb\r"\xc3H\x8d\x86*70\xa5\
\xc3\xba\xf83\x84\xf0\x93IU\xd9\xf6C\x00\xc4\x8e\xb0.\xfe\x04(\x05\xa9*\xc0e\
\xbe\x11X9\xe7\xb0A\xc9\x98\x93<\x97\x98(\x92b\x8c\xb7{\xdb\x0e\r\x96\xf8\n\
\xf8\xcc^r\x0fs\xed}\x08J\x14\xb9\xf6~\x04M\x02\xec\x18%\xfc&\x04\xaf\x8f\
\xf4\xaf\xaa\t\xbe\x1el!\xed\xde\xc2%\x86\x00\x00\x00\x00IEND\xaeB`\x82' 

def getDownBitmap():
    return wx.BitmapFromImage(getDownImage())

def getDownImage():
    stream = cStringIO.StringIO(getDownData())
    return wx.ImageFromStream(stream)

# defined event for vtInputTextML item changed
wxEVT_VTINPUT_TREE_CHANGED=wx.NewEventType()
vEVT_VTINPUT_TREE_CHANGED=wx.PyEventBinder(wxEVT_VTINPUT_TREE_CHANGED,1)
def EVT_VTINPUT_TREE_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTINPUT_TREE_CHANGED,func)
def EVT_VTINPUT_TREE_CHANGED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,wxEVT_VTINPUT_TREE_CHANGED,func)
class vtInputTreeChanged(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTINPUT_TREE_CHANGED(<widget_name>, self.OnChanged)
    """

    def __init__(self,obj,node,fid,val):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTINPUT_TREE_CHANGED)
        self.node=node
        self.fid=fid
        self.val=val
    def GetNode(self):
        return self.node
    def GetId(self):
        return self.fid
    def GetID(self):
        return self.fid
    def GetValue(self):
        return self.val
    

class vtInputTreeTransientPopup(wx.Dialog,vtXmlDomConsumer):
    #SIZE_PN_SCROLL=30
    def __init__(self, parent, size,style,name=''):
        vtXmlDomConsumer.__init__(self)
        self.SIZE_PN_SCROLL=wx.SystemSettings.GetMetric(wx.SYS_VSCROLL_X)
        wx.Dialog.__init__(self, parent,style=wx.RESIZE_BORDER)#wx.BORDER_SIMPLE)#|wx.STAY_ON_TOP)
        id=wx.NewId()
        wx.EVT_SIZE(self,self.OnSize)
        st = wx.StaticText(self, -1,name,pos=(70,4))
        self.cbCancel = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Cancel), name=u'cbCancel',
              parent=self, pos=(0,0), size=(30,30), style=wx.BU_AUTODRAW)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              self.cbCancel)
              
        self.cbApply = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Apply), name=u'cbApply',
              parent=self, pos=wx.Point(32, 00), size=wx.Size(30, 30),
              style=wx.BU_AUTODRAW)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              self.cbApply)
              
        self.pbProg = wx.Gauge(id=-1, name='pbProg', parent=self,
              pos=wx.Point(70, 8), range=100, size=wx.Size(116, 12),
              style=wx.GA_SMOOTH | wx.GA_HORIZONTAL)
        self.pbProg.Show(False)
        self.bSelProc=False
        #self.par=parent
        if size[1]<40:
            size=(size[0],40)
        i=0
        iY=40
        iH=size[1]+4
        iSzHeight=0
        self.trInfo=parent.__getTreeInstance__(parent=self,pos=(4,40),size=(192,196))
        #self.trInfo.SetBuildOnDemand(1)    # wro 060819
        self.trInfo.SetNotifyAddThread()
        try:
            wx.EVT_LEFT_DCLICK(self.trInfo, self.OnDblClickLeft)
        except:
            pass
        #print self.trInfo
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.trInfo,self.OnAdd)
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.trInfo,self.OnAddFinished)
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_ABORTED(self.trInfo,self.OnAddAborted)
        #self.trInfo.Bind(vEVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED,self.OnAddFinished,self.trInfo)
        self.szOrig=(200,240)
        self.SetSize(self.szOrig)
    def OnAdd(self,evt):
        try:
            iCount=evt.GetCount()
            if iCount>0:
                self.pbProg.SetRange(iCount)
                self.pbProg.Show(True)
            self.pbProg.SetValue(evt.GetValue())
        except:
            vtLog.vtLngTB(self.GetValue())
    def OnAddFinished(self,evt):
        try:
            if self.bSelProc:
                return
            self.bSelProc=True
            par=self.GetParent()
            self.trInfo.SelectByID(par.__getSelID__())
            self.pbProg.Show(False)
        except:
            self.GetParent().__logTB__()
    def OnAddAborted(self,evt):
        try:
            if self.bSelProc:
                return
            self.bSelProc=True
            par=self.GetParent()
            self.trInfo.SelectByID(par.__getSelID__())
            self.pbProg.Show(False)
        except:
            self.GetParent().__logTB__()
    def OnSize(self,evt):
        iW,iH=self.GetSize()
        if iW<self.szOrig[0]:
            iW=self.szOrig[0]
        #if iH<self.szMin[1]:
        #    iH=self.szMin[1]
        if iH<self.szOrig[1]:
            iH=self.szOrig[1]
        self.SetSize((iW,iH))
        if self.trInfo is not None:
            self.trInfo.SetSize((iW-16,iH-52))
        evt.Skip()
    def OnCbCancelButton(self,evt):
        self.Show(False)
    def OnCbApplyButton(self,evt):
        #self.Apply()
        try:
            par=self.GetParent()
            par.__apply__(self.trInfo.GetSelected())
        except:
            self.GetParent().__logTB__()
        self.Show(False)
    def OnDblClickLeft(self,evt):
        try:
            par=self.GetParent()
            par.__apply__(self.trInfo.GetSelected())
        except:
            self.GetParent().__logTB__()
        self.Show(False)
    def ClearDoc(self):
        par=self.GetParent()
        if par is not None:
            par.docTreeTup=None
        vtXmlDomConsumer.ClearDoc(self)
    def SetDocTree(self,doc,bNet):
        par=self.GetParent()
        if par is not None:
            bDbg=par.GetVerboseDbg()
            if bDbg:
                par.__logDebug__('bNet:%d'%(bNet))
        vtXmlDomConsumer.SetDoc(self,doc)
        self.trInfo.SetDoc(doc,bNet)
    def SetNodeTree(self,node):
        #if VERBOSE:
        #    vtLog.CallStack(node)
        self.bSelProc=False
        self.trInfo.SetNode(node)
    def SetNode(self,fid):
        par=self.GetParent()
        if par is not None:
            bDbg=par.GetVerboseDbg()
            if bDbg:
                par.__logDebug__('fid:%s'%(fid))
        #doc=par.__getDoc__()
        #node=par.__getNode__()
        try:
            self.trInfo.SelectByID(fid)
        except:
            par.__logTB__()
    def Show(self,flag):
        try:
            par=self.GetParent()
            if flag:
                doc=par.__getDoc__()
                node=par.__getNode__()
                fid=par.__getSelID__()
                self.trInfo.SelectByID(fid)
            else:
                #par.cbPopup.SetValue(False)
                par.__setPopupState__(False)
        except:
            par.__logTB__()
        wx.Dialog.Show(self,flag)
    def Apply(self):
        try:
            par=self.GetParent()
            doc=par.__getDoc__()
            node=par.__getNode__()
            par.UpdateLang()
        except:
            par.__logTB__()
    def GetVal(self,lang):
        try:
            return ''
        except:
            self.GetParent().__logTB__()
            return ''
    def SetVal(self,val,lang):
        try:
            pass
        except:
            self.GetParent().__logTB__()
class vtInputTree(vGuiCorePanWX,vtXmlDomConsumer,vtXmlDomConsumerLang):
    SIZE_DFT=(100,34)
    # ++++++++++++++++++++++++++++++++++++++++
    def BindEvent(self,name,func,par=None):
        if name.startswith('tr_'):
            wid=self
            if name in ['tr_item_sel','tr_item_selected']:
                wid.Bind(wx.EVT_LIST_ITEM_SELECTED,func, par or wid)
            elif name in ['tr_chg']:
                EVT_VTINPUT_TREE_CHANGED(par or wid,func)
        else:
            vGuiCoreWX.BindEvent(self,name,func,par=par)
    def UnBindEvent(self,name,func,par=None):
        if name.startswith('tr_'):
            wid=self
            if name in ['tr_item_sel','tr_item_selected']:
                wid.Bind(wx.EVT_LIST_ITEM_SELECTED,func, par or wid)
            elif name in ['tr_chg']:
                EVT_VTINPUT_TREE_CHANGED_DISCONNECT(par or wid,func)
        else:
            vGuiCoreWX.BindEvent(self,name,func,par=par)
    def __initCls__(self,*args,**kwargs):
        self.SetVerbose(__name__,kwargs=kwargs)
    def __initObj__(self,*args,**kwargs):
        try:
            vGuiCorePanWX.__initObj__(self,*args,**kwargs)
            bDbg=self.GetVerboseDbg(20)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
            
            self.txtVal=None
            self.popWin=None
            self.doc=None
            self.docTreeTup=None
            self.nodeTree=None
            self.nodeSel=None
            #self.netMaster=None
            self.fid=''
            self.tagName='name'
            self.bBlock=False
            self.tagNameInt='name'
            self.appl=None
            self.node=None
            self.bMod=False
            self.bEnableMark=True
            self.bBusy=False
            
            self.trClass=kwargs.get('tree_class',vtXmlGrpTree)
            self.trSetNode=kwargs.get('set_node',None)
            self.trGetNode=kwargs.get('get_node',None)
            self.trGetId=kwargs.get('get_id',None)
            self.trShow=kwargs.get('show_node_func',None)
        except:
            self.__logTB__()
        try:
            vtXmlDomConsumer.__init__(self)
            vtXmlDomConsumerLang.__init__(self)
        except:
            self.__logTB__()
    def __initCtrlVal__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            
            mode=kwargs.get('mode','txtRd')
            iProp=kwargs.get('iProp',1)
            dW=self.__getClsDftArgs__(mode)
            cls,lKey,kw,flag=dW['CLS'],dW['key'],dW['kw'],dW['flag']
            kkw=kwargs.get('kwVal',{'name':'txtVal'})
            kw.update(kkw)
            kw['parent']=self.GetWid()
            _args,_kwargs=self.GetGuiArgs(kw,lKey,{
                    'pos':(0,0),'size':(-1,24),
                    'style':0,
                    'name':'txtVal',
                    'value':'',
                    },
                    sNameSub='txtVal')
            if bDbg:
                self.__logDebugAdd__('txtVal',
                        {'_args':_args,'_kwargs':_kwargs})
            self.txtVal=cls(*_args,**_kwargs)
            #self.bxs.AddWindow(self.txtVal, iProp, border=0, flag=flag)
            self.AddSz(self.txtVal,iProp, border=0, flag=flag)
            if mode=='txt':
                #self.txtVal.Bind(wx.EVT_TEXT, self.OnTextText)
                #self.txtVal.Bind(wx.EVT_KEY_DOWN, self.OnKeyPressed)
                self.BindEventWid(self.txtVal,'txt',self.OnTextText)
                #self.BindEventWid(self.txtVal,'keyDn',self.OnKeyPressed)
        except:
            self.__logTB__()
    def __initCtrl__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            vGuiCorePanWX.__initCtrl__(self,*args,**kwargs)
            self.__initCtrlVal__(*args,**kwargs)
        except:
            self.__logTB__()
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
        except:
            self.__logTB__()
    def __initPopup__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            
            vGuiCorePanWX.__initPopup__(self,*args,**kwargs)
            
            # 20150109 wro: finally rename popup button
            self.cbPopup=self.widBt
            self.widBt.Enable(True)
            del self.widBt
        except:
            self.__logTB__()
    def __init_wid__old(self,*args,**kwargs):
        wid=None
        #vtInputTreeEvt.__init__(self)
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
        except:
            self.__logTB__()
        try:
            sz=_kwargs['size']
        except:
            sz=wx.Size(100,30)
            _kwargs['size']=sz
        try:
            szCb=_kwargs['size_button']
            del _kwargs['size_button']
        except:
            szCb=wx.Size(24,24)
        try:
            szTxt=_kwargs['size_text']
            del _kwargs['size_text']
        except:
            szTxt=wx.Size(sz[0]-szCb[0]-2,sz[1]-4)
        
        __args,__kwargs=self.GetGuiArgs(_kwargs,
                ['id','name','parent','pos','size','style'],
                {'pos':(0,0),'size':self.SIZE_DFT,'style':wx.TAB_TRAVERSAL})
        wx.Panel.__init__(self,*__args,**__kwargs)
        #self.wid=self
        #self.wid=wx.Panel(*__args,**__kwargs)
        self.SetAutoLayout(True)
        vGuiCoreWX.__init__(self)
        self.SetVerbose(__name__,iVerbose=-1,kwargs=_kwargs)
        
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        size=(szTxt[0],szTxt[1])
        pos=(0,(sz[1]-szTxt[1])/2)
        self.txtVal = wx.TextCtrl(id=-1, name='txtVal',
              parent=self, pos=pos, size=size,
              style=0, value='')
        #self.txtVal.SetConstraints(LayoutAnchors(self.txtVal, True, True,
        #      True, False))
        bxs.AddWindow(self.txtVal, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        self.txtVal.Bind(wx.EVT_TEXT, self.OnTextText,self.txtVal)
        
        size=(szCb[0],szCb[1])
        pos=(szTxt[0]+(sz[0]-(szTxt[0]+szCb[0]))/2,(sz[1]-szCb[1])/2)
        id=wx.NewId()
        self.cbPopup = wx.lib.buttons.GenBitmapToggleButton(ID=id,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbPopup',
              parent=self, pos=pos, size=size, style=wx.BU_AUTODRAW)
        #self.cbPopup.SetConstraints(LayoutAnchors(self.cbPopup, False, True,
        #      True, False))
        bxs.AddWindow(self.cbPopup, 0, border=0, flag=wx.ALIGN_CENTER)
        self.SetSizer(bxs)
        self.cbPopup.SetBitmapLabel(vtArt.getBitmap(vtArt.Down))
        self.cbPopup.SetBitmapSelected(vtArt.getBitmap(vtArt.Down))
        self.cbPopup.Bind(wx.EVT_BUTTON,self.OnPopupButton,self.cbPopup)#id=id)
        self.SetOriginByWid(self)
        if self.IsMainThread()==False:
            return
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,''%(),'del')
        try:
            if self.docTreeTup is not None:
                if self.docTreeTup[0] is not None:
                    self.docTreeTup[0].DelConsumer(self)
            self.docTreeTup=None
        except:
            self.__logTB__()
    def GetWid(self):
        return self
    def GetWidMod(self):
        return self.txtVal
    def Enable(self,flag):
        if self.IsMainThread()==False:
            return
        wx.Panel.Enable(self,flag)
        self.cbPopup.Refresh()
    def SetAppl(self,appl):
        self.appl=appl
    def SetEnableMark(self,flag):
        self.bEnableMark=flag
    def __markModified__(self,flag=True,store_mod=True):
        if self.IsMainThread()==False:
            return
        if self.bEnableMark==False:
            return
        if VERBOSE_MARKUP:
            if self.__isLogDebug__():
                self.__logDebug__('flag:%s'%(flag))
        if flag:
            f=self.txtVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_BOLD)
            self.txtVal.SetFont(f)
        else:
            f=self.txtVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
            self.txtVal.SetFont(f)
        self.txtVal.Refresh()
        if store_mod:
            self.bMod=flag
    def SetModified(self,flag):
        self.__markModified__(flag)
    def IsModified(self):
        return self.bMod
    def SetTreeClass(self,trCls):
        bDbg=self.GetVerboseDbg(4)
        if bDbg:
            self.__logDebug__('')
        self.trClass=trCls
    def SetTreeFunc(self,createFunc,showFunc):
        bDbg=self.GetVerboseDbg(4)
        if bDbg:
            self.__logDebug__('')
        self.trClass=createFunc
        self.trShow=showFunc
    def SetTagNames(self,tagName,tagNameInt):
        bDbg=self.GetVerboseDbg(4)
        if bDbg:
            self.__logDebug__('tagname:%s,%s'%(tagName,tagNameInt))
        self.tagName=tagName
        self.tagNameInt=tagNameInt
    def SetDocTree(self,doc,bNet=False):
        bDbg=self.GetVerboseDbg(4)
        if bDbg:
            self.__logDebug__('')
        if self.docTreeTup is not None:
            if self.docTreeTup[0] is not None:
                self.docTreeTup[0].DelConsumer(self)
        self.ClearTree()
        if doc is not None:
            doc.AddConsumer(self,self.ClearTree)
            #if bNet:
                #vtLog.CallStack('')
                #EVT_NET_XML_GOT_CONTENT(doc,self.OnTreeDocGotContent)
                #EVT_NET_XML_BUILD_TREE(doc,self.OnTreeDocGotContent)
        self.docTreeTup=(doc,bNet)
        if self.popWin is not None:
            self.popWin.SetDocTree(doc,bNet)
            self.SetNodeTree(None)
        self.__getSelNode__()
    def SetNetMaster(self,netMaster):
        #self.netMaster=netMaster
        pass
    def OnTreeDocGotContent(self,evt):
        evt.Skip()
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'node:%s'%self.nodeTree,self)
        self.nodeTree=None
        if self.popWin is not None:
            self.popWin.SetNodeTree(None)
        
    def SetNodeTree(self,node):
        bDbg=self.GetVerboseDbg(4)
        if bDbg:
            try:
                self.__logDebug__('id:%s'%(self.doc.getKey(node)))
            except:
                pass
        self.nodeTree=node
        if self.popWin is not None:
            self.popWin.SetNodeTree(self.nodeTree)
    def ClearTree(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'ClearTree',
        #                self)
        self.nodeSel=None
        self.nodeTree=None
    def ClearLang(self):
        if self.IsMainThread()==False:
            return
        bDbg=self.GetVerboseDbg(4)
        if bDbg:
            self.__logDebug__('')
        #if VERBOSE:
        #    vtLog.CallStack('')
        self.bBlock=True
        self.txtVal.SetValue('')
        wx.CallAfter(self.__clearBlock__)
    def UpdateLang(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'UpdateLang',
        #                self)
        return
        if VERBOSE:
            vtLog.CallStack('')
        try:
            if self.popWin is None:
                sVal=self.doc.getNodeText(self.node,self.tagName)
            else:
                sVal=self.popWin.GetVal(self.doc.GetLang())
            self.bBlock=True
            self.txtVal.SetValue(sVal.split('\n')[0])
            wx.CallAfter(self.__clearBlock__)
            #self.txtVal.SetValue(sVal)
        except:
            vtLog.vtLngTB(self.GetName())

    def Clear(self):
        bDbg=self.GetVerboseDbg(4)
        if bDbg:
            self.__logDebug__('')
        self.ClearLang()
        self.node=None
        self.fid=''
        self.nodeSel=None
        self.__markModified__(False)
    def __setPopupState__(self,flag):
        self.bBusy=flag
        self.cbPopup.SetValue(flag)
    def IsBusy(self):
        return self.bBusy
    def __Close__(self):
        if self.IsMainThread()==False:
            return
        if self.popWin is not None:
            self.popWin.Show(False)
            self.__setPopupState__(False)
    def Stop(self):
        bDbg=self.GetVerboseDbg(4)
        if bDbg:
            self.__logDebug__('stop')
        wx.CallAfter(self.__Close__)
    def ClearDoc(self):
        bDbg=self.GetVerboseDbg(4)
        if bDbg:
            self.__logDebug__('')
        vtXmlDomConsumer.ClearDoc(self)
        self.docTreeTup=None
    def SetDoc(self,doc,bNet=False):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'SetDoc',
        #                self)
        vtXmlDomConsumer.SetDoc(self,doc)
        vtXmlDomConsumerLang.SetDoc(self,doc)
    def SetNode(self,node):
        if self.IsMainThread()==False:
            return
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'SetNode;node:%s'%(node),
        #                self)
        #if VERBOSE:
        #    vtLog.CallStack('')
        #    print node
        self.Clear()
        self.node=node
        if self.doc is None:
            return
        if self.node is None:
            return
        try:
            sVal=self.doc.getNodeText(self.node,self.tagName).split('\n')[0]
            self.bBlock=True
            self.__markModified__(False)
            self.txtVal.SetValue(sVal)
            wx.CallAfter(self.__clearBlock__)
        except:
            pass
        self.__getSelNode__()
        self.UpdateLang()
    def IsModified(self):
        return self.bMod
    def __clearBlock__(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'__clearBlock__',
        #                self)
        self.bBlock=False
    def __getSelNode__(self):
        if self.IsMainThread()==False:
            return
        bDbg=self.GetVerboseDbg(4)
        if bDbg:
            self.__logDebug__('')
        #if VERBOSE:
        #    vtLog.CallStack('')
        try:
            if self.node is None or self.doc is None:
                self.fid=''
                return
            self.fid=self.doc.getForeignKey(self.doc.getChild(self.node,self.tagName),'fid',self.appl)
            self.__logDebug__('fid:%s'%(self.fid))
            if self.appl is not None and self.fid=='-3':
                self.fid=self.doc.getForeignKey(self.doc.getChild(self.node,self.tagName),'fid',None)
                self.__logWarn__('fallback id:%s'%self.fid)
            if VERBOSE:
                print self.fid
            self.nodeSel=None
            if self.docTreeTup is not None:
                docTree=self.docTreeTup[0]
                self.nodeSel=docTree.getNodeById(self.fid)
                if VERBOSE:
                    print self.nodeSel
            #vtLog.CallStack('')
            #print self.nodeSel
            if self.nodeSel is not None:
                sVal=self.txtVal.GetValue()
                self.__apply__(self.nodeSel)
                if sVal!=self.txtVal.GetValue():
                    self.__markModified__(True)
            else:
                if self.node is not None:
                    self.__logWarn__('%s;%s'%(self.fid,self.node))
            wx.PostEvent(self,vtInputTreeChanged(self,self.nodeSel,self.fid,self.txtVal.GetValue()))
        except:
            self.__logTB__()
    def __setNode__(self,node):
        if self.IsMainThread()==False:
            return
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'__setNode__;node:%s'%(self.node),
        #                self)
        try:
            docTree=self.docTreeTup[0]
            if self.nodeSel is None:
                sVal=self.txtVal.GetValue()
            else:
                sVal=docTree.getNodeText(self.nodeSel,self.tagNameInt).split('\n')[0]
            self.doc.setNodeText(node,self.tagName,sVal)
            tmp=self.doc.getChild(node,self.tagName)
            if tmp is not None:
                if len(self.fid)==0:
                    self.doc.removeAttribute(tmp,'fid')
                else:
                    self.doc.setForeignKey(tmp,attr='fid',attrVal=self.fid,appl=self.appl)
        except:
            self.__logTB__()
    def GetNode(self,node=None):
        self.Close()
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'GetNode',
        #                self)
        if node is None:
            node=self.node
        if node is None:
            return
        
        #if VERBOSE:
        #    vtLog.CallStack('')
        #    print self.nodeSel
        #    print self.node
        try:
            for c in self.doc.getChilds(node,self.tagName):
                self.doc.deleteNode(c,node)
            #self.doc.setAttribute(self.node,'fid',self.fid)
            self.__setNode__(node)

            self.__markModified__(False)
            self.doc.AlignNode(node)
        except:
            self.__logTB__()
    def Close(self):
        if self.IsMainThread()==False:
            return
        if self.cbPopup.GetValue()==True:
            if self.popWin is not None:
                self.popWin.Show(False)
    def __getDoc__(self):
        return self.doc
    def __getDocTree__(self):
        try:
            return self.docTreeTup[0]
        except:
            None
    def __getNode__(self,node=None):
        if node is None:
            return self.node
        else:
            return node
    def __getSelID__(self):
        bDbg=self.GetVerboseDbg(4)
        if bDbg:
            self.__logDebug__('fid:%s'%self.fid)
        #if VERBOSE:
        #    vtLog.CallStack('')
        try:
            return self.fid
        except:
            return ''
    def __getTreeInstance__(self,*args,**kwargs):
        bDbg=self.GetVerboseDbg(4)
        if bDbg:
            self.__logDebug__('args:%s;kwargs:%s'%(args,kwargs))
        #print kwargs
        if not kwargs.has_key('id'):
            kwargs['id']=-1
        if not kwargs.has_key('pos'):
            kwargs['pos']=(4,4)
        if not kwargs.has_key('size'):
            kwargs['size']=wx.DefaultSize
        if not kwargs.has_key('style'):
            kwargs['style']=0
        if not kwargs.has_key('name'):
            kwargs['name']='treeInstance'
        return self.trClass(*args,**kwargs)
    def __apply__(self,node):
        if self.IsMainThread()==False:
            return
        bDbg=self.GetVerboseDbg(4)
        if bDbg:
            try:
                self.__logDebug__('id:%s'%(self.doc.getKey(node)))
            except:
                pass
        #if VERBOSE:
        #    vtLog.CallStack(node)
        try:
            self.fid=''
            self.nodeSel=node
            if self.nodeSel is not None:
                self.fid=self.docTreeTup[0].getKey(node)
            #if self.trSetNode is not None:
            #    sVal=self.trSetNode(self.docTreeTup[0],node)
            #else:
            #    sVal=''
            if self.trShow is not None:
                sVal=self.trShow(self.docTreeTup[0],node,self.doc.GetLang())
            else:
                sVal=''
            if self.bBlock==False:
                self.__markModified__(True)
            wx.PostEvent(self,vtInputTreeChanged(self,self.nodeSel,self.fid,sVal))
        except:
            sVal=_(u'fault')
            self.__logTB__()
        self.bBlock=True
        self.txtVal.SetValue(sVal.split('\n')[0])
        wx.CallAfter(self.__clearBlock__)
        return sVal
    def __createPopup__(self):
        if self.IsMainThread()==False:
            return
        bDbg=self.GetVerboseDbg(4)
        if bDbg:
            self.__logDebug__('')
        if self.popWin is None:
            sz=self.GetSize()
            sz=(sz[0],sz[1]-20)
            try:
                self.popWin=vtInputTreeTransientPopup(self,sz,wx.SIMPLE_BORDER)
                self.popWin.SetDocTree(self.docTreeTup[0],self.docTreeTup[1])
                self.popWin.SetNodeTree(self.nodeTree)
                if self.trSetNode is not None:
                    self.popWin.SetNode(self.trSetNode(self.doc,self.node))
            except:
                self.__logTB__()
            #self.popWin.SetVal(self.txtVal.GetValue(),self.doc.GetLang())
        else:
            pass
    def GetValue(self):
        if self.IsMainThread()==False:
            return
        bDbg=self.GetVerboseDbg(4)
        if bDbg:
            self.__logDebug__('%s'%self.txtVal.GetValue())
        return self.txtVal.GetValue()
    def SetValueID(self,sVal,id):
        try:
            self.bBlock=True
            self.__markModified__(False)
            if self.docTreeTup is None:
                return
            node=self.docTreeTup[0].getNodeByIdNum(id)
            self.__apply__(node)
        except:
            self.__logTB__()
        wx.CallAfter(self.__clearBlock__)
    def GetID(self):
        return self.fid
    def GetForeignID(self):
        if self.appl is None:
            return self.fid
        else:
            return '@'.join([self.fid,self.appl])
    def GetSelectedNode(self):
        return self.nodeSel
    def GetSelectedNodeTagName(self):
        try:
            return self.docTreeTup[0].getTagName(self.nodeSel)
        except:
            return None
    def OnTextText(self,evt):
        if self.IsMainThread()==False:
            return
        bDbg=self.GetVerboseDbg(4)
        if bDbg:
            self.__logDebug__('%s;block:%d'%(self.txtVal.GetValue(),self.bBlock))
        if self.bBlock:
            return
        self.nodeSel=None
        self.fid=''
        sVal=self.txtVal.GetValue()
        self.__markModified__()
        wx.PostEvent(self,vtInputTreeChanged(self,self.nodeSel,self.fid,sVal))
    def OnPopupButton(self,evt):
        if self.IsMainThread()==False:
            return
        self.__createPopup__()
        if self.cbPopup.GetValue()==True:
            btn=self
            iX,iY = btn.ClientToScreen( (0,0) )
            iW,iH =  btn.GetSize()
            iPopW,iPopH=self.popWin.GetSize()
            iScreenW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            iScreenH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            if iX+iPopW>iScreenW:
                iX=iScreenW-iPopW-4
                if iX<0:
                    iX=0
            iY+=iH
            if iY+iPopH>iScreenH:
                iY=iScreenH-iPopH
                if iY<0:
                    iY=0
            
            self.popWin.Move((iX,iY))
            self.bBusy=True
            #self.popWin.Move((pos[0],pos[1]+sz[1]))
            self.popWin.Show(True)
        else:
            self.popWin.Show(False)
            self.bBusy=False
        evt.Skip()
from vidarc.tool.xml.vtXmlGrpTree import vtXmlGrpTree
