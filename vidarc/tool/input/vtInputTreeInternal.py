#----------------------------------------------------------------------------
# Name:         vtInputTreeInternal.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060204
# CVS-ID:       $Id: vtInputTreeInternal.py,v 1.16 2015/01/21 12:52:28 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import wx
    from vidarc.tool.input.vtInputTree import *
    from vidarc.tool.xml.vtXmlHierarchy import getTagNames
    import vidarc.tool.log.vtLog as vtLog
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vtInputTreeInternal(vtInputTree):
    def __init__(self,*_args,**_kwargs):
        vpc.logDebug({'args':_args,'kwargs':_kwargs},__name__)
        try:
            self.bFullName=_kwargs['show_fullname']
            del _kwargs['show_fullname']
        except:
            self.bFullName=True
        apply(vtInputTree.__init__,(self,) + _args,_kwargs)
        if self.trShow is None:
            self.trShow=self.__showNode__
        #if self.trSetNode is None:
        #    self.trSetNode=self.__setNode__
        self.lTagNames2Base=[]
        self.dCalls={}
        self.tagNameInt='name'
    def SetShowFullName(self,flag):
        self.bFullName=flag
    def SetTreeClass(self,trCls):
        bDbg=self.GetVerboseDbg(0)
        if bDbg:
            self.__logDebug__({})
        self.trClass=trCls
    def SetTreeFunc(self,trClass,showFunc):
        bDbg=self.GetVerboseDbg(0)
        if bDbg:
            self.__logDebug__({})
        if trClass is not None:
            self.trClass=trClass
        if showFunc is not None:
            self.trShow=showFunc
    #def SetTagNameInternal(self,tagName):
    #    if vtLog.vtLngIsLogged(vtLog.DEBUG):
    #        vtLog.vtLngCurWX(vtLog.DEBUG,'SetTagNameInternal;tagname:%s'%tagName,
    #                    self)
    #    self.tagNameInt=tagName
    def SetTagNames2Base(self,lst):
        bDbg=self.GetVerboseDbg(0)
        if bDbg:
            self.__logDebug__('lst:%s'%lst)
        self.lTagNames2Base=lst
        self.__setTreeNode__()
    def __setNode__(self,node):
        if self.IsMainThread()==False:
            return
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'__setNode__;node:%s'%(`node`),
        #                self)
        try:
            if self.nodeSel is None:
                sVal=self.txtVal.GetValue()
                fid=''
            else:
                docTree=self.docTreeTup[0]
                sVal=docTree.getNodeText(self.nodeSel,self.tagNameInt)
                if self.bFullName:
                    #sVal=getTagNames(self.doc,self.nodeTree,None,self.nodeSel)#+sVal
                    sVal=getTagNames(docTree,self.nodeTree,None,self.nodeSel)#+sVal
                                #docTree.getChild(self.nodeSel,self.tagNameInt))+sVal
                fid=docTree.getKey(self.nodeSel)
            #self.fid=fid
            if self.node is not None:
                self.doc.setNodeText(node,self.tagName,sVal)
                tmp=self.doc.getChild(node,self.tagName)
                if tmp is not None:     # 070223:wro
                    if len(fid)==0:
                        self.doc.removeAttribute(tmp,'fid')
                    else:
                        #self.doc.setAttribute(tmp,'fid',fid)    # 070223:wro
                        self.doc.setForeignKey(tmp,attr='fid',   # 070223:wro
                                attrVal=self.fid,appl=self.appl)
        except:
            self.__logTB__()
    def __apply__(self,node):
        if self.IsMainThread()==False:
            return
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'__apply__;node:%s'%(`node`),
        #                self)
        if VERBOSE:
            vtLog.CallStack(node)
        try:
            self.nodeSel=node
            self.fid=self.docTreeTup[0].getKey(node)
            if self.nodeTree is None:
                self.__setTreeNode__()
            if self.nodeSel is None:
                sVal=self.txtVal.GetValue()
                fid=''
            else:
                docTree=self.docTreeTup[0]
                if self.trShow is None:
                    sVal=docTree.getNodeText(self.nodeSel,self.tagNameInt)
                    if self.bFullName:
                        #sVal=getTagNames(self.doc,self.nodeTree,None,self.nodeSel)
                        sVal=getTagNames(docTree,self.nodeTree,None,self.nodeSel)
                                    #docTree.getChild(self.nodeSel,self.tagNameInt))#+sVal
                else:
                    sVal=self.trShow(self.docTreeTup[0],self.nodeSel,self.doc.GetLang())
                fid=docTree.getKey(self.nodeSel)
            #if self.trSetNode is not None:
            #    sVal=self.trSetNode(self.docTreeTup[0],node)
            #else:
            #    sVal=''
            #if self.trShow is not None:
            #    sVal=self.trShow(self.docTreeTup[0],node,self.doc.GetLang())
            #else:
            #    sVal=''
            if self.bBlock==False:
                self.__markModified__(True)
                wx.PostEvent(self,vtInputTreeChanged(self,self.nodeSel,self.fid,sVal))
        except:
            sVal=_(u'fault')
            self.__logTB__()
        self.bBlock=True
        self.txtVal.SetValue(sVal.split('\n')[0])
        wx.CallAfter(self.__clearBlock__)
        return sVal
    def __setTreeNode__(self):
        bDbg=self.GetVerboseDbg(0)
        if bDbg:
            self.__logDebug__({})
        tmp=None
        try:
            doc=self.docTreeTup[0]
        except:
            doc=None
        if doc is not None:
            tmp=doc.getChildByLst(doc.getBaseNode(),self.lTagNames2Base)
        if VERBOSE:
            vtLog.CallStack('')
            print tmp
        self.SetNodeTree(tmp)
        if self.popWin is not None:
            self.popWin.SetNodeTree(tmp)
    def AddTreeCall(self,name,call,*args,**kwargs):
        bDbg=self.GetVerboseDbg(0)
        if bDbg:
            self.__logDebug__({})
        if not self.dCalls.has_key(name):
            self.dCalls[name]=[]
        self.dCalls[name].append((call,args,kwargs))
    def __doCalls__(self,name,tree):
        bDbg=self.GetVerboseDbg(0)
        if bDbg:
            self.__logDebug__('%s'%name)
        if VERBOSE:
            vtLog.CallStack(name)
        if not self.dCalls.has_key(name):
            return
        bDbg=self.GetVerboseDbg(0)
        if bDbg:
            self.__logDebug__('%s'%repr(self.dCalls[name]))
        lst=self.dCalls[name]
        for tup in lst:
            if VERBOSE:
                print tup
            try:
                eval('tree.%s'%tup[0])(*tup[1],**tup[2])
            except:
                self.__logTB__()
    #def Clear(self):
    #    vtInputTree.Clear(self)
    #    if VERBOSE:
    #        vtLog.CallStack('')
        #self.__setTreeNode__()
    def SetDoc(self,doc,bNet=False,bExternal=False):
        bDbg=self.GetVerboseDbg(0)
        if bDbg:
            self.__logDebug__({})
        vtXmlDomConsumer.SetDoc(self,doc)
        vtXmlDomConsumerLang.SetDoc(self,doc)
        if bExternal==False:
            self.nodeTree=None
            self.SetDocTree(doc,bNet)
    def __getTreeInstance__(self,*args,**kwargs):
        bDbg=self.GetVerboseDbg(0)
        if bDbg:
            self.__logDebug__('args:%s;kwargs:%s'%(args,kwargs))
        #print kwargs
        if not kwargs.has_key('id'):
            kwargs['id']=-1
        if not kwargs.has_key('pos'):
            kwargs['pos']=(4,4)
        if not kwargs.has_key('size'):
            kwargs['size']=wx.DefaultSize
        if not kwargs.has_key('style'):
            kwargs['style']=0
        if not kwargs.has_key('name'):
            kwargs['name']='treeInstance'
        if VERBOSE:
            kwargs['verbose']=1
        tr=self.trClass(*args,**kwargs)
        self.__doCalls__('init',tr)
        return tr
    def __createPopup__(self):
        if self.IsMainThread()==False:
            return
        bDbg=self.GetVerboseDbg(0)
        if bDbg:
            self.__logDebug__({})
        if self.nodeTree is None:
            self.__setTreeNode__()
        vtInputTree.__createPopup__(self)
    def __createTree__(*args,**kwargs):
        if self.IsMainThread()==False:
            return
        if VERBOSE:
            vtLog.CallStack('')
            vtLog.pprint(args)
            vtLog.pprint(kwargs)
        tr=vXmlHumTree(**kwargs)
        tr.SetNodeInfos(['name','|id'])
        tr.SetGrouping([],[('name','')])
        tr.EnableLanguageMenu()
        return tr
    def __showNode__(self,doc,node,lang):
        if self.bFullName:
            #sVal=getTagNames(self.doc,self.nodeTree,None,node)#+sVal
            if self.docTreeTup is not None:
                docTree=self.docTreeTup[0]
                sVal=getTagNames(docTree,self.nodeTree,None,node)#+sVal
            return sVal
        return doc.getNodeText(node,self.tagNameInt)  #'name'
