#----------------------------------------------------------------------
# Name:         vtInputMultipleTree.py
# Purpose:      text control with tree popup window
#
# Author:       Walter Obweger
#
# Created:      20060228
# CVS-ID:       $Id: vtInputMultipleTree.py,v 1.15 2013/06/17 11:28:01 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
from wx.lib.anchors import LayoutAnchors

import string,cStringIO,sys

import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlTree import EVT_VTXMLTREE_THREAD_ADD_ELEMENTS
from vidarc.tool.xml.vtXmlTree import EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_ABORTED
from vidarc.tool.xml.vtXmlTree import EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED
from vidarc.tool.xml.vtXmlGrpTree import vtXmlGrpTree
from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_BUILD_TREE
#from vidarc.tool.xml.vtXmlGrpTree import *
from vidarc.tool.xml.vtXmlDomConsumer import *
from vidarc.tool.xml.vtXmlDomConsumerLang import *
import vidarc.tool.vtSystem as vtSystem

VERBOSE=0
import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

#----------------------------------------------------------------------
def getAddData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x9bIDAT8\x8d\xad\x93\xd1\x11\xc3 \x0cC\x9fi\x86K2\x07cy\x0e\xc2v\
\xf4\'\xeeQ\xc7\xe1\xb8\xb4\xfeC\x9cd\xd9 \x91\xf4\xc2\xd7\xbe\xad\xed\x02\
\x02\xe5\xa8\xe2\xb1%"\xe6\x9c#>@\xf3B\x1f\x81}[\xdb\x80\x88\x13n&\x92FdUEUC\
!s\x9b\x86-\'j\x99\xb1\x1e\xd5\xc9i\xbf;\xe8\x0f\xd1\xbc\x11\xde;\xfe\xaf\
\x03\xbf\x0b\xeb<\xda\xd1c\x07\xaaJ9\xaa\xa4rT\xb9\x9b}\xa6\x1e9\xb0\xee\x00\
ba\x9a\xfd\x0f=\x19\xba%\x9e\xe0m\x98lL\x9f\xc8\xafW\xe8./q\x8e\xa2\x0c\xf0\
\x06\xb1kJ$\x1ck%y\x00\x00\x00\x00IEND\xaeB`\x82' 

def getAddBitmap():
    return wx.BitmapFromImage(getAddImage())

def getAddImage():
    stream = cStringIO.StringIO(getAddData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getAddAttrData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x96IDAT8\x8d\xad\x93Q\x12\xc3 \x08D\x17\xe2\xc1\xb8X\x9bQ/\xb67\xb3\
\x1f\x86\x8eC\xd261\xdd/\x05\x1f.\x8c\x8a\xe8\x02\xd7\xfa|4\x0003\x1c\x89$\
\x00 \x97*\x1eK#\x1cAc\xdf\xd3:8\xe4\x9b\x17\xd1_p\\{!w\x9b\x8e\xe03\xda\x98\
\xa6\x97\xc9 \x9d\xb9}t\xf1\x1eb\xecsw8\xe4}\xb0z\x06\xfeV\xf0\xfe\x0cF;W\
\xe4L\x8a\x01\xd7\xa7\x9ew\x0e\xfcy\xce\x88\xe4\x1ff\x90K\x95\x19\x17$\x91K\
\x15\x05\xfa\xef\x8aE\xc6\x9ec\xff\x0e\x03\x80\xdc\xfd\xce/\xa1"M8_2\x04\xe8\
\x00\x00\x00\x00IEND\xaeB`\x82' 

def getAddAttrBitmap():
    return wx.BitmapFromImage(getAddAttrImage())

def getAddAttrImage():
    stream = cStringIO.StringIO(getAddAttrData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getDelData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xf1IDAT8\x8d\x95\x93Qn\xc4 \x0cD\x9fI\xee\x15\x0e\x90\xeb\xf0\xd9b\
\xba\x9f\xb9N\x0e@\x0e\xd6\xba\x1f\xadW\x0e\xd1n\xb7#!\xc0\xc63\x03\x16"i"\
\xa2\xbe\xbfY\xdc\xb7\x8f\x9b\xf0\x04\xf3XXk\xa5\xb5F\xad\xd5S\xf6\x94HUMU\
\xcd\xa1\xaa\x06X\xef\xddz\xef\xa7\xb8\xaa\x9a\xa4\x898fW\x05h\xad\xdd\x89{\
\xef\xe4\x9c9\x8e\x83xft$\xaaj9g\x00\x96e\x01@DPU\x00<\xe7\x88g$M2\xc7\xa4\
\xab\x99\xd9\xc5Q,\x8e\xf1\xf9\xd7\x8aE\xa5u]\x01\xd8\xf7\xfdD0\x12B\xe8\x82\
c\xdb6J)\'\xa2R\n\xbd\xf7K1@\x1a\x8b\xe3\xfc\x08\xaa\x8a\xa4I.\x04\xae\x1cg_\
?B\x82\x9f\x96\x8c\x16_u\x93\xc6@)\xe5\xe5b\x00\xf1\xbf`_\x9f\x06\xdc\x1f+>f\
\x8c\xc7\xfbC\xe8\x82\x07s\xce\'\xa2\xbfpi\xe3\x7f\x89d\xfc\xce#\xfcj\xa3\
\x80\xe3\x1b00\x8cM\xc8\xff\xc0\xbf\x00\x00\x00\x00IEND\xaeB`\x82' 

def getDelBitmap():
    return wx.BitmapFromImage(getDelImage())

def getDelImage():
    stream = cStringIO.StringIO(getDelData())
    return wx.ImageFromStream(stream)

# defined event for vtInputTextML item changed
wxEVT_VTINPUTMULTIPLE_TREE_CHANGED=wx.NewEventType()
vEVT_VTINPUTMULTIPLE_TREE_CHANGED=wx.PyEventBinder(wxEVT_VTINPUTMULTIPLE_TREE_CHANGED,1)
def EVT_VTINPUTMULTIPLE_TREE_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTINPUTMULTIPLE_TREE_CHANGED,func)
class vtInputMultipleTreeChanged(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTINPUTMULTIPLE_TREE_CHANGED(<widget_name>, self.OnChanged)
    """

    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTINPUTMULTIPLE_TREE_CHANGED)
        self.obj=obj
    def GetObject(self):
        return self.obj
    

class vtInputMultipleTreeTransientPopup(wx.Dialog):
    #SIZE_PN_SCROLL=30
    def __init__(self, parent, size,style,name=''):
        self.SIZE_PN_SCROLL=wx.SystemSettings.GetMetric(wx.SYS_VSCROLL_X)
        wx.Dialog.__init__(self, parent,style=wx.RESIZE_BORDER)#wx.BORDER_SIMPLE)#|wx.STAY_ON_TOP)
        id=wx.NewId()
        wx.EVT_SIZE(self,self.OnSize)
        st = wx.StaticText(self, -1,name,pos=(70,4))
        self.cbCancel = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Cancel), name=u'cbCancel',
              parent=self, pos=(0,0), size=(30,30), style=wx.BU_AUTODRAW)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              self.cbCancel)
              
        self.cbApply = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Apply), name=u'cbApply',
              parent=self, pos=wx.Point(32, 00), size=wx.Size(30, 30),
              style=wx.BU_AUTODRAW)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              self.cbApply)
        
        self.pbProg = wx.Gauge(id=-1, name='pbProg', parent=self,
              pos=wx.Point(70, 8), range=100, size=wx.Size(116, 12),
              style=wx.GA_SMOOTH | wx.GA_HORIZONTAL)
        self.pbProg.Show(False)
        self.bSelProc=False
        
        if size[1]<40:
            size=(size[0],40)
        i=0
        iY=40
        iH=size[1]+4
        iSzHeight=0
        self.trInfo=parent.__getTreeInstance__(parent=self,pos=(4,40),size=(192,196))
        self.trInfo.SetBuildOnDemand(1)    # wro 060819
        self.trInfo.SetNotifyAddThread()
        #print self.trInfo
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.trInfo,self.OnAdd)
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.trInfo,self.OnAddFinished)
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_ABORTED(self.trInfo,self.OnAddAborted)
        #self.trInfo.Bind(vEVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED,self.OnAddFinished,self.trInfo)
        self.szOrig=(200,240)
        self.SetSize(self.szOrig)

    def OnAdd(self,evt):
        try:
            iCount=evt.GetCount()
            if iCount>0:
                self.pbProg.SetRange(iCount)
                self.pbProg.Show(True)
            self.pbProg.SetValue(evt.GetValue())
        except:
            vtLog.vtLngTB(self.GetValue())
    def OnAddFinished(self,evt):
        try:
            if self.bSelProc:
                return
            self.bSelProc=True
            par=self.GetParent()
            self.trInfo.SelectByID(par.__getSelID__())
            self.pbProg.Show(False)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
    def OnAddAborted(self,evt):
        try:
            if self.bSelProc:
                return
            self.bSelProc=True
            par=self.GetParent()
            self.trInfo.SelectByID(par.__getSelID__())
            self.pbProg.Show(False)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
    def OnSize(self,evt):
        iW,iH=self.GetSize()
        if iW<self.szOrig[0]:
            iW=self.szOrig[0]
        #if iH<self.szMin[1]:
        #    iH=self.szMin[1]
        if iH<self.szOrig[1]:
            iH=self.szOrig[1]
        self.SetSize((iW,iH))
        if self.trInfo is not None:
            self.trInfo.SetSize((iW-16,iH-52))
        evt.Skip()
    def OnCbCancelButton(self,evt):
        self.Show(False)
    def OnCbApplyButton(self,evt):
        #self.Apply()
        try:
            par=self.GetParent()
            self.bBlock=True
            par.__apply__(self.trInfo.GetSelected())
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        self.Show(False)
    def SetDocTree(self,doc,bNet):
        self.trInfo.SetDoc(doc,bNet)
    def SetNodeTree(self,node):
        #if VERBOSE>0:
        #    vtLog.CallStack(node)
        self.bSelProc=False
        self.trInfo.SetNode(node)
    def SetNode(self,fid):
        #par=self.GetParent()
        #doc=par.__getDoc__()
        #node=par.__getNode__()
        try:
            self.trInfo.SelectByID(fid)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
    def Show(self,flag):
        try:
            par=self.GetParent()
            if flag:
                doc=par.__getDoc__()
                node=par.__getNode__()
                fid=par.__getSelID__()
                self.trInfo.SelectByID(fid)
            else:
                par.cbPopup.SetValue(False)
                par.__setPopupState__(False)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        wx.Dialog.Show(self,flag)
    def Apply(self):
        try:
            par=self.GetParent()
            doc=par.__getDoc__()
            node=par.__getNode__()
            par.UpdateLang()
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
    def GetVal(self,lang):
        try:
            return ''
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
            return ''
    def SetVal(self,val,lang):
        try:
            pass
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
class vtInputMultipleTree(wx.Panel,vtXmlDomConsumer,vtXmlDomConsumerLang):
    def __init__(self,*_args,**_kwargs):
        vtXmlDomConsumer.__init__(self)
        vtXmlDomConsumerLang.__init__(self)
        try:
            sz=_kwargs['size']
        except:
            sz=wx.Size(100,30)
            _kwargs['size']=sz
        try:
            szCb=_kwargs['size_button']
            del _kwargs['size_button']
        except:
            szCb=wx.Size(32,32)
        try:
            szTxt=_kwargs['size_text']
            del _kwargs['size_text']
        except:
            szTxt=wx.Size(sz[0]-szCb[0]-2,sz[1]-4)
        try:
            self.trClass=_kwargs['tree_class']
            del _kwargs['tree_class']
        except:
            self.trClass=vtXmlGrpTree
        try:
            self.trSetNode=_kwargs['set_node']
            del _kwargs['set_node']
        except:
            self.trSetNode=None#self.__setNode__
        try:
            self.trGetNode=_kwargs['get_node']
            del _kwargs['get_node']
        except:
            self.trGetNode=None
        try:
            self.trGetId=_kwargs['get_id']
            del _kwargs['get_id']
        except:
            self.trGetId=None
        try:
            self.trShow=_kwargs['show_node_func']
            del _kwargs['show_node_func']
        except:
            self.trShow=None
        try:
            self.tagName=_kwargs['tag_name']
            del _kwargs['tag_name']
        except:
            self.tagName='name'
        try:
            self.tagGrp=_kwargs['tag_grp']
            del _kwargs['tag_grp']
        except:
            self.tagGrp=None
        try:
            self.tagNameInt=_kwargs['tag_name_int']
            del _kwargs['tag_name_int']
        except:
            self.tagNameInt='name'
        try:
            self.lColumns=_kwargs['columns']
            del _kwargs['columns']
        except:
            self.lColumns=[('name',-1),('id',-1)]
        try:
            self.lShowAttrs=_kwargs['show_attrs']
            del _kwargs['show_attrs']
        except:
            self.lShowAttrs=['name','|id']
        try:
            self.lTreeBaseNode=_kwargs['tree_base_node']
            del _kwargs['tree_base_node']
        except:
            self.lTreeBaseNode=None
        
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        self.SetAutoLayout(True)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        size=(szTxt[0],szTxt[1])
        pos=(0,(sz[1]-szTxt[1])/2)
        self.lstVal = wx.ListCtrl(id=-1, name='lstVal',
              parent=self, pos=pos, size=size,
              style=wx.LC_REPORT|wx.LC_SORT_ASCENDING)
        #self.txtVal.SetConstraints(LayoutAnchors(self.txtVal, True, True,
        #      True, False))
        bxs.AddWindow(self.lstVal, 1, border=0, flag=wx.EXPAND|wx.ALL)
        #self.txtVal.Bind(wx.EVT_TEXT, self.OnTextText,self.txtVal)
        self.lstVal.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnListItemDeselected, self.lstVal)
        self.lstVal.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnListItemSelected, self.lstVal)
        self.lstVal.Bind(wx.EVT_LIST_COL_CLICK, self.OnListColClick,
              self.lstVal)
        i=0
        for tup in self.lColumns:
            fmt=wx.LIST_FORMAT_LEFT
            w=-1
            try:
                if len(tup)>0:
                    sHdr=tup[0]
                    try:
                        w=tup[1]
                    except:
                        pass
                    try:
                        fmt=tup[2]
                    except:
                        pass
            except:
                sHdr=tup
            self.lstVal.InsertColumn(i,sHdr,fmt,w)
            i+=1
        
        size=(szCb[0],szCb[1])
        pos=(szTxt[0]+(sz[0]-(szTxt[0]+szCb[0]))/2,(sz[1]-szCb[1])/2)
        bxsBt=wx.BoxSizer(orient=wx.VERTICAL)
        id=wx.NewId()
        self.cbPopup = wx.lib.buttons.GenBitmapToggleButton(ID=id,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbPopup',
              parent=self, pos=pos, size=size, style=wx.BU_AUTODRAW)
        #self.cbPopup.SetConstraints(LayoutAnchors(self.cbPopup, False, True,
        #      True, False))
        bxsBt.AddWindow(self.cbPopup, 0, border=0, flag=wx.ALIGN_CENTER)#wx.EXPAND)
        id=wx.NewId()
        self.cbDel = wx.lib.buttons.GenBitmapButton(ID=id,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbDel',
              parent=self, pos=pos, size=size, style=wx.BU_AUTODRAW)
        #self.cbPopup.SetConstraints(LayoutAnchors(self.cbPopup, False, True,
        #      True, False))
        bxsBt.AddSpacer(wx.Size(4, 4), border=0, flag=0)
        bxsBt.AddWindow(self.cbDel, 0, border=0, flag=wx.ALIGN_CENTER)#wx.EXPAND)
        bxs.AddSizer(bxsBt, 0, border=4, flag=wx.LEFT)
        self.SetSizer(bxs)
        self.Layout()
        self.cbPopup.SetBitmapLabel(getAddBitmap())
        self.cbPopup.SetBitmapSelected(getAddAttrBitmap())
        self.cbPopup.Bind(wx.EVT_BUTTON,self.OnPopupButton,self.cbPopup)#id=id)
        self.cbDel.SetBitmapLabel(getDelBitmap())
        #self.cbDel.SetBitmapSelected(getDelBitmap())
        self.cbDel.Bind(wx.EVT_BUTTON,self.OnDelButton,self.cbDel)#id=id)
        self.popWin=None
        self.doc=None
        self.docTreeTup=None
        self.nodeTree=None
        self.nodeSel=None
        self.lstSel=[]
        self.lstPos=[]
        self.bBlock=False
        self.appl=None
        self.node=None
        self.idxSel=-1
        self.idxSelCol=0
        self.bAscend=True
        self.bMod=False
        self.bEnableMark=True
        self.bBusy=False
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
    def Enable(self,flag):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.lstVal.Enable(flag)
        self.cbPopup.Enable(flag)
        self.cbDel.Enable(flag)
        self.cbPopup.Refresh()
        self.cbDel.Refresh()
    def SetAppl(self,appl):
        self.appl=appl
    def __clearLangVals__(self):
        self.lstSel=[]
    def SetEnableMark(self,flag):
        self.bEnableMark=flag
    def __markModified__(self,flag=True,store_mod=True):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'flag:%s'%(flag),self)
        if self.bEnableMark==False:
            return
        if flag:
            f=self.lstVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_BOLD)
            self.lstVal.SetFont(f)
        else:
            f=self.lstVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
            self.lstVal.SetFont(f)
        self.lstVal.Refresh()
        if store_mod:
            self.bMod=flag
    def SetModified(self,flag):
        self.__markModified__(flag)
    def IsModified(self):
        return self.bMod
    def SetTreeFunc(self,createFunc,showFunc):
        if VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.trClass=createFunc
        self.trShow=showFunc
    def SetTagNames(self,tagName,tagNameInt):
        if VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'tagname:%s,%s'%(tagName,tagNameInt),
                        self)
        self.tagName=tagName
        self.tagNameInt=tagNameInt
    def OnBuild(self,evt):
        evt.Skip()
        self.lstPos=[]
    def SetDocTree(self,doc,bNet=False):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if self.docTreeTup is not None:
            if self.docTreeTup[0] is not None:
                self.docTreeTup[0].DelConsumer(self)
        self.ClearTree()
        if doc is not None:
            doc.AddConsumer(self,self.ClearTree)
            if bNet:
                EVT_NET_XML_BUILD_TREE(self,self.OnBuild)
        self.docTreeTup=(doc,bNet)
        if doc.hasNodeInfos2Get(self.GetName())==False:     # 070920:wro check it first
            doc.setNodeInfos2Get(self.lShowAttrs,self.GetName())
        if self.popWin is not None:
            self.popWin.SetDoc(self.docTreeTup)
    def SetNodeTree(self,treeBaseNode):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'SetTreeNode;node:%s'%treeBaseNode,
        #                self)
        self.lTreeBaseNode=treeBaseNode
        self.__getTreeBaseNode__()
    def __getTreeBaseNode__(self):
        if self.docTreeTup is None:
            if self.docTreeTup[0] is None:
                self.nodeTree=None
        doc=self.docTreeTup[0]
        if self.lTreeBaseNode is None:
            self.nodeTree=doc.getChildByLst(doc.getBaseNode(),self.lTreeBaseNode)
        else:
            self.nodeTree=doc.getBaseNode()
    def ClearTree(self):
        if VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.nodeSel=None
        self.nodeTree=None
    def ClearLang(self):
        if VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        #if VERBOSE>9:
        #    vtLog.CallStack('')
        self.lstVal.DeleteAllItems()
        self.idxSel=-1
    def UpdateLang(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        #if VERBOSE>9:
        #    vtLog.CallStack('')
        try:
            #if self.popWin is None:
            #    sVal=self.doc.getNodeTextLang(self.node,self.tagName,None)
            #else:
            #    sVal=self.popWin.GetVal(self.doc.GetLang())
            self.lstVal.DeleteAllItems()
            self.idxSel=-1
            if self.docTreeTup is None:
                if self.docTreeTup[0] is None:
                    return
            docTree=self.docTreeTup[0]
            docTree.acquire()
            try:
                sInfoName=self.GetName()
                iCols=len(self.lColumns)
                iNum=0
                for id,name in self.lstSel:
                    n=docTree.getNodeById(id)
                    if n is not None:
                        tagName,infos=docTree.getNodeInfos(n,self.doc.GetLang(),sInfoName)
                        idx=-1
                        for i in range(iCols):
                            if i==0:
                                idx=self.lstVal.InsertStringItem(sys.maxint,infos[self.lShowAttrs[i]])
                            else:
                                self.lstVal.SetStringItem(idx,i,infos[self.lShowAttrs[i]])
                        self.lstVal.SetItemData(idx,long(id))
                        try:
                            sName=infos[self.tagNameInt]
                            if name!=sName:
                                self.__markModified__(True)
                            self.lstSel[iNum][1]=sName
                        except:
                            pass
                    iNum+=1
                
            except:
                vtLog.vtLngTB(self.GetName())
            docTree.release()
            #self.txtVal.SetValue(sVal)
        except:
            vtLog.vtLngTB(self.GetName())

    def Clear(self):
        if VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'Clear',
                        self)
        #if VERBOSE>9:
        #    vtLog.CallStack('')
        self.ClearLang()
        self.__clearLangVals__()
        #self.tagName=None
        self.node=None
        self.lstSel=[]
    def __setPopupState__(self,flag):
        self.bBusy=flag
        self.cbPopup.SetValue(flag)
    def IsBusy(self):
        return self.bBusy
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'IsBusy',
                        self)
        if self.popWin is not None:
            return self.popWin.IsShown()
        else:
            return False
    def __Close__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.popWin is not None:
            self.popWin.Show(False)
            self.__setPopupState__(False)
    def Stop(self):
        if VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'Stop',self)
        wx.CallAfter(self.__Close__)
    def SetDoc(self,doc):
        if VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'SetDoc',
                        self)
        vtXmlDomConsumer.SetDoc(self,doc)
        vtXmlDomConsumerLang.SetDoc(self,doc)
    def SetNode(self,node):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'node:%s'%(node),
        #                self)
        #if VERBOSE>9:
        #    vtLog.CallStack('')
        #    print node
        
        self.Clear()
        self.node=node
        #self.tagName=tagName
        if self.doc is None:
            return
        if self.node is None:
            return
        #if self.tagName is None:
        #    return
        if self.tagGrp is None:
            nodeTmp=self.node
        else:
            nodeTmp=self.doc.getChild(self.node,self.tagGrp)
        try:
            for c in self.doc.getChilds(nodeTmp,self.tagName):
                #fid=self.doc.getAttribute(c,'fid')
                fid=self.doc.getForeignKey(c,'fid',self.appl) #self.doc.getChild(c,self.tagName)
                if self.appl is not None and fid=='-3':
                    fid=self.doc.getForeignKey(c,'fid',None) #self.doc.getChild(c,self.tagName)
                    vtLog.vtLngCurWX(vtLog.WARN,'fallback id:%s'%fid,self)
                
                sName=self.doc.getText(c)
                self.lstSel.append([fid,sName])
            
            self.__markModified__(False)
        except:
            vtLog.vtLngTB(self.GetName())
        self.UpdateLang()
    def __setNode__(self,node):
        if VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        try:
            docTree=self.docTreeTup[0]
            def compFunc(a,b):
                l=long(a[0])-long(b[0])
                if l<0:
                    return -1
                elif l>=0:
                    return 1
                return 0
            self.lstSel.sort(compFunc)
            for tup in self.lstSel:
                id,sVal=tup[0],tup[1]
                #self.doc.setNodeTextAttr(node,self.tagName,sVal,'fid',id)
                nodeTmp=self.doc.createSubNodeText(node,self.tagName,sVal)
                self.doc.setForeignKey(nodeTmp,attr='fid',attrVal=id,appl=self.appl)
        except:
            vtLog.vtLngTB(self.GetName())
    def GetNode(self,node=None):
        if node is None:
            node=self.node
        if node is None:
            return
        if VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'GetNode',
                        self)
        #if VERBOSE>9:
        #    vtLog.CallStack('')
        #    print self.nodeSel
        #    print self.node
        if self.tagGrp is None:
            nodeTmp=node
        else:
            nodeTmp=self.doc.getChildForced(node,self.tagGrp)
            self.doc.setAttribute(nodeTmp,'multiple','1')
        try:
            for c in self.doc.getChilds(nodeTmp,self.tagName):
                self.doc.deleteNode(c,nodeTmp)
            self.__setNode__(nodeTmp)
            
            self.__markModified__(False)
            self.doc.AlignNode(nodeTmp)
        except:
            vtLog.vtLngTB(self.GetName())
    def __getDoc__(self):
        return self.doc
    def __getNode__(self):
        return self.node
    def __getSelID__(self):
        if VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        #if VERBOSE>9:
        #    vtLog.CallStack('')
        try:
            return self.fid
        except:
            return ''
    def __getTreeInstance__(self,*args,**kwargs):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'args:%s;kwargs:%s'%(args,kwargs),
                        self)
        if not kwargs.has_key('id'):
            kwargs['id']=-1
        if not kwargs.has_key('pos'):
            kwargs['pos']=(4,4)
        if not kwargs.has_key('size'):
            kwargs['size']=wx.DefaultSize
        if not kwargs.has_key('style'):
            kwargs['style']=0
        if not kwargs.has_key('name'):
            kwargs['name']='treeInstance'
        return self.trClass(*args,**kwargs)
    def __setNodeS__(self,doc,node):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'__setNode__;node:%s'%(node),
        #                self)
        try:
            langs=self.doc.GetLanguages()
            iCount=len(langs)
            if len(langs)>0:
                for tup in langs:
                    sVal=string.split(self.doc.getNodeTextLang(node,self.tagName,tup[1]),'\n')[0]
                    self.dVal[tup[1]]=sVal
            sVal=self.dVal[self.doc.GetLang()]
            fid=doc.getKey(node)
        except:
            sVal=''
            fid=-2
        #self.fid=fid
        #if self.node is not None:
        #    doc.setNodeText(self.node,self.tagName,sVal)
        #    tmp=doc.getChild(self.node,self.tagName)
        #    if fid<0:
        #        doc.removeAttribute(tmp,'fid')
        #    else:
        #        doc.setAttribute(tmp,'fid',fid)
        return sVal
    def __apply__(self,node):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'__apply__;node:%s'%(node),
        #                self)
        #if VERBOSE>9:
        #    vtLog.CallStack(node)
        try:
            fid=self.docTreeTup[0].getKey(node)
            bFound=False
            for id,n in self.lstSel:
                if id==fid:
                    bFound=True
                    break
            if bFound==False:
                sName=self.docTreeTup[0].getNodeText(node,self.tagNameInt)
                self.lstSel.append([fid,sName])
                self.UpdateLang()
                self.__markModified__(True)
                wx.PostEvent(self,vtInputMultipleTreeChanged(self))
        except:
            sVal=_(u'fault')
            vtLog.vtLngTB(self.GetName())
        #return sVal
        
    def __createPopup__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if self.popWin is None:
            sz=self.GetSize()
            sz=(sz[0],sz[1]-20)
            try:
                self.popWin=vtInputMultipleTreeTransientPopup(self,sz,wx.SIMPLE_BORDER)
                self.popWin.SetDocTree(self.docTreeTup[0],self.docTreeTup[1])
                self.popWin.SetNodeTree(self.nodeTree)
                if self.trSetNode is not None:
                    self.popWin.SetNode(self.trSetNode(self.doc,self.node))
            except:
                vtLog.vtLngTB(self.GetName())
            #self.popWin.SetVal(self.txtVal.GetValue(),self.doc.GetLang())
        else:
            pass
    def OnListItemDeselected(self,evt):
        self.idxSel=-1
        pass
    def OnListItemSelected(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.idxSel=evt.GetIndex()
        try:
            self.fid=self.docTreeTup[0].ConvertIdNum2Str(self.lstVal.GetItemData(self.idxSel))
        except:
            vtLog.vtLngTB(self.GetName())
            self.fid=-1
        pass
    def OnListColClick(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.idxSel=-1
        idxSelColOld=self.idxSelCol
        self.idxSelCol=evt.GetColumn()
        if self.docTreeTup is None:
            if self.docTreeTup[0] is None:
                return
        docTree=self.docTreeTup[0]
        docTree.acquire()
        try:
            sInfoName=self.GetName()
            iCols=len(self.lColumns)
            iNum=0
            sAttr=self.lShowAttrs[self.idxSelCol]
            if idxSelColOld==self.idxSelCol:
                self.bAscend=not self.bAscend
            else:
                self.bAscend=True
            def __cmpListItems__(idA,idB):
                nA=docTree.getNodeByIdNum(idA)
                nB=docTree.getNodeByIdNum(idB)
                tagNameA,infosA=docTree.getNodeInfos(nA,self.doc.GetLang(),sInfoName)
                sValA=infosA[sAttr]
                tagNameB,infosB=docTree.getNodeInfos(nB,self.doc.GetLang(),sInfoName)
                sValB=infosB[sAttr]
                if self.bAscend:
                    return cmp(sValA,sValB)
                else:
                    return cmp(sValB,sValA)
            self.lstVal.SortItems(__cmpListItems__)
        except:
            vtLog.vtLngTB(self.GetName())
        docTree.release()
        pass
    def GetValue(self):
        if VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        def conv(a):
            return a[1]
        return ','.join(map(conv,self.lstSel))
    def GetID(self):
        if VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        def conv(a):
            return a[0]
        return ','.join(map(conv,self.lstSel))
    def OnPopupButton(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.__createPopup__()
        if self.cbPopup.GetValue()==True:
            #btn=self
            btn=self.cbPopup
            iX,iY = btn.ClientToScreen( (0,0) )
            iW,iH =  btn.GetSize()
            iPopW,iPopH=self.popWin.GetSize()
            iX,iY,iWidth,iHeight=vtSystem.LimitWindowToScreen(iX-iPopW,iY,iPopW,iPopH)
            if 0:
                iScreenW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
                iScreenH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
                if iX+iPopW>iScreenW:
                    iX=iScreenW-iPopW-4
                    if iX<0:
                        iX=0
                iY+=iH
                if iY+iPopH>iScreenH:
                    iY=iScreenH-iPopH
                    if iY<0:
                        iY=0
            
            self.popWin.Move((iX,iY))
            #self.popWin.Move((pos[0],pos[1]+sz[1]))
            self.bBusy=True
            self.popWin.Show(True)
        else:
            self.popWin.Show(False)
            self.bBusy=False
        evt.Skip()
    def OnDelButton(self,evt):
        try:
            bFound=False
            i=0
            fid=long(self.fid)
            for tup in self.lstSel:
                id=tup[0]
                if long(id)==fid:
                    bFound=True
                    self.lstSel=self.lstSel[:i]+self.lstSel[i+1:]
                    break
                i+=1
            self.fid=-1
            self.__markModified__(True)
            self.UpdateLang()
        except:
            vtLog.vtLngTB(self.GetName())
    def GetValueFullLst(self):
        l=[]
        for t in self.lstSel:
            l.append([t[1],(long(t[0]),self.appl)])
        return l
