#Boa:FramePanel:vtInputAttrValMLPanel
#----------------------------------------------------------------------
# Name:         vtInputAttrValMLPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060425
# CVS-ID:       $Id: vtInputAttrValMLPanel.py,v 1.14 2010/03/10 22:38:53 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import wx
    import vidarc.tool.lang.vtLgSelector
    import wx.lib.buttons
    from wx.lib.anchors import LayoutAnchors
    import sys,string
    import vidarc.tool.lang.vtLgBase as vtLgBase

    from vidarc.tool.lang.vtLgSelector import vtLgSelector
    from vidarc.tool.lang.vtLgSelector import EVT_VTLANG_SELECTION_CHANGED

    #from vidarc.tool.net.vNetXmlWxGui import *
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel
    from vidarc.tool.xml.vtXmlDomConsumerLang import vtXmlDomConsumerLang
    #from vidarc.tool.xml.vtXmlDomConsumerLang import vtXmlDomConsumerLang
    #from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_GOT_CONTENT
    #from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_GET_NODE
    #from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_REMOVE_NODE
    #from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_LOCK
    #from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_UNLOCK
    #from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_SET_NODE

    import vidarc.tool.art.vtArt as vtArt
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)
#import vidarc.tool.log.vtLog as vtLog
#import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_VTINPUTATTRVALMLPANEL, wxID_VTINPUTATTRVALMLPANELCBAPPLY, 
 wxID_VTINPUTATTRVALMLPANELCBDEL, wxID_VTINPUTATTRVALMLPANELCMBATTRNAME, 
 wxID_VTINPUTATTRVALMLPANELLBLATTRNAME, wxID_VTINPUTATTRVALMLPANELLBLATTRVAL, 
 wxID_VTINPUTATTRVALMLPANELLSTATTR, wxID_VTINPUTATTRVALMLPANELTXTATTRVAL, 
 wxID_VTINPUTATTRVALMLPANELTXTSUFF, wxID_VTINPUTATTRVALMLPANELVILANG, 
] = [wx.NewId() for _init_ctrls in range(10)]

class vtInputAttrValMLPanel(wx.Panel,vtXmlNodePanel,vtXmlDomConsumerLang):
    VERBOSE=0
    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblAttrName, (0, 0), border=2,
              flag=wx.EXPAND | wx.RIGHT, span=(1, 1))
        parent.AddWindow(self.cmbAttrName, (0, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.txtSuff, (0, 2), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.cbApply, (0, 4), border=0, flag=0, span=(2, 1))
        parent.AddWindow(self.lblAttrVal, (1, 0), border=2,
              flag=wx.ALIGN_RIGHT | wx.EXPAND, span=(1, 1))
        parent.AddWindow(self.txtAttrVal, (1, 1), border=0, flag=wx.EXPAND,
              span=(1, 2))
        parent.AddWindow(self.cbDel, (2, 4), border=0, flag=0, span=(1, 1))
        parent.AddWindow(self.lstAttr, (2, 0), border=0, flag=wx.EXPAND,
              span=(2, 3))
        parent.AddSpacer(wx.Size(4, 4), (0,3), border=0, flag=0, span=(1, 1))
        parent.AddWindow(self.viLang, (3, 4), border=0, flag=0, span=(1, 1))

    def _init_coll_lstAttr_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'attribute'), width=120)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'value'), width=250)

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=0, vgap=0)

        self._init_coll_gbsData_Items(self.gbsData)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTINPUTATTRVALMLPANEL,
              name=u'vtInputAttrMLPanel', parent=prnt, pos=wx.Point(212, 288),
              size=wx.Size(383, 192), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(375, 165))
        self.SetAutoLayout(True)

        self.lblAttrName = wx.StaticText(id=wxID_VTINPUTATTRVALMLPANELLBLATTRNAME,
              label=_(u'attribute'), name=u'lblAttrName', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(96, 21), style=wx.ALIGN_RIGHT)

        self.cmbAttrName = wx.ComboBox(choices=[],
              id=wxID_VTINPUTATTRVALMLPANELCMBATTRNAME, name=u'cmbAttrName',
              parent=self, pos=wx.Point(98, 0), size=wx.Size(100, 21),
              style=wx.CB_SORT, value=u'')
        self.cmbAttrName.SetLabel(u'')
        self.cmbAttrName.Bind(wx.EVT_TEXT_ENTER, self.OnCmbAttrNameTextEnter,
              id=wxID_VTINPUTATTRVALMLPANELCMBATTRNAME)
        self.cmbAttrName.Bind(wx.EVT_COMBOBOX, self.OnCmbAttrNameCombobox,
              id=wxID_VTINPUTATTRVALMLPANELCMBATTRNAME)
        self.cmbAttrName.Bind(wx.EVT_TEXT, self.OnCmbAttrNameText,
              id=wxID_VTINPUTATTRVALMLPANELCMBATTRNAME)

        self.txtSuff = wx.TextCtrl(id=wxID_VTINPUTATTRVALMLPANELTXTSUFF,
              name=u'txtSuff', parent=self, pos=wx.Point(198, 0),
              size=wx.Size(99, 21), style=0, value=u'')

        self.lblAttrVal = wx.StaticText(id=wxID_VTINPUTATTRVALMLPANELLBLATTRVAL,
              label=_(u'value'), name=u'lblAttrVal', parent=self,
              pos=wx.Point(0, 21), size=wx.Size(98, 21), style=wx.ALIGN_RIGHT)

        self.txtAttrVal = wx.TextCtrl(id=wxID_VTINPUTATTRVALMLPANELTXTATTRVAL,
              name=u'txtAttrVal', parent=self, pos=wx.Point(98, 21),
              size=wx.Size(199, 21), style=0)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTINPUTATTRVALMLPANELCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.ApplyAttr), label=_(u'Apply'),
              name=u'cbApply', parent=self, pos=wx.Point(307, 0),
              size=wx.Size(76, 30), style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VTINPUTATTRVALMLPANELCBAPPLY)

        self.cbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTINPUTATTRVALMLPANELCBDEL,
              bitmap=vtArt.getBitmap(vtArt.DelAttr), label=_(u'Del'),
              name=u'cbDel', parent=self, pos=wx.Point(307, 42),
              size=wx.Size(76, 30), style=0)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VTINPUTATTRVALMLPANELCBDEL)

        self.lstAttr = wx.ListCtrl(id=wxID_VTINPUTATTRVALMLPANELLSTATTR,
              name=u'lstAttr', parent=self, pos=wx.Point(0, 42),
              size=wx.Size(297, 120), style=wx.LC_REPORT|wx.LC_SORT_ASCENDING)
        self._init_coll_lstAttr_Columns(self.lstAttr)
        self.lstAttr.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstAttrListItemSelected,
              id=wxID_VTINPUTATTRVALMLPANELLSTATTR)
        self.lstAttr.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstAttrListItemDeselected,
              id=wxID_VTINPUTATTRVALMLPANELLSTATTR)

        self.viLang = vidarc.tool.lang.vtLgSelector.vtLgSelector(id=wxID_VTINPUTATTRVALMLPANELVILANG,
              name=u'viLang', parent=self, pos=wx.Point(307, 102),
              size=wx.Size(30, 30), style=0)
        self.viLang.Bind(vidarc.tool.lang.vtLgSelector.vEVT_VTLANG_SELECTION_CHANGED,
              self.OnViLangVtlangSelectionChanged)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name,dftAttrs=[]):
        """ dftAttrs=[('tel',bmp)]
        """
        global _
        _=vtLgBase.assignPluginLang('vtInput')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,applName=_(u'vtInput Attr Val ML'),
                lWidgets=[self.lstAttr])
        vtXmlDomConsumerLang.__init__(self)
        #vtXmlDomConsumerLang.__init__(self)
        #self.bModified=False
        self.selectedIdx=-1
        
        self.dData={}
        
        self.SetDftAttrs(dftAttrs)
        self.gbsData.AddGrowableRow(3)
        self.gbsData.AddGrowableCol(1)
        
        self.cbApply.SetBitmapLabel(vtArt.getBitmap(vtArt.ApplyAttr))
        self.cbDel.SetBitmapLabel(vtArt.getBitmap(vtArt.DelAttr))
        
        self.Move(pos)
        self.SetSize(size)
    def __del__(self):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
       #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        vtXmlDomConsumer.__del__(self)
    def SetDftAttrs(self,d):
        #if self.VERBOSE:
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.lAttrs=d
        self.__setupImageList__()
        
    def __setupImageList__(self):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        self.imgDict={}
        self.imgLstTyp=wx.ImageList(16,16)
        try:
            for s,bmp in self.lAttrs:
                self.imgDict[s]=self.imgLstTyp.Add(bmp)
                self.cmbAttrName.Append(s)
        except:
            self.__logTB__()
        self.lstAttr.SetImageList(self.imgLstTyp,wx.IMAGE_LIST_SMALL)

    def SetModified(self,state):
        self.bModified=state
    def GetModified(self):
        return self.bModified
    def __isModified__(self):
        if self.bModified:
            return True
        return False
    #def ClearLang(self):
    #    vtXmlDomConsumerLang.Clear(self)
        
    def __Clear__(self):
        try:
            #vtXmlDomConsumer.Clear(self)
            self.cmbAttrName.SetValue('')
            self.txtAttrVal.SetValue('')
            self.txtSuff.SetValue('')
            self.lstAttr.DeleteAllItems()
            self.selectedIdx=-1
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
    def IsBusy(self):
        return False
    def Stop(self):
        pass
    def UpdateLang(self):
        pass
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        vtXmlDomConsumerLang.SetDoc(self,doc)
        # add code here
        self.viLang.SetDoc(doc)
    def __getAttrNameSuff__(self,sAttrName):
        strs=string.split(sAttrName,'_')
        try:
            if len(strs)>1:
                sSuff=strs[-1]
                sAttrName=sAttrName[:-(len(sSuff)+1)]
            else:
                sSuff=''
        except:
            sSuff=''
        return sAttrName,sSuff
    def __getAttrName__(self,sAttrName):
        sAttr,sSuff=self.__getAttrNameSuff__(sAttrName)
        return self.__buildAttrName__(sAttr,sSuff)
    def __buildAttrName__(self,sAttr,sSuff):
        if len(sSuff)>0:
            return sAttr+'_'+sSuff
        return sAttr
    def __getAttrNameSuffML__(self,sAttrName):
        strs=string.split(sAttrName,'_')
        try:
            if len(strs)>1:
                sSuff=strs[-1]
                sAttrName=sAttrName[:-(len(sSuff)+1)]
            else:
                sSuff=''
        except:
            sSuff=''
        return _(sAttrName),sSuff
    def __getAttrNameML__(self,sAttrName):
        sAttr,sSuff=self.__getAttrNameSuffML__(sAttrName)
        return self.__buildAttrName__(sAttr,sSuff)
    def __getAttrKey__(self,key):
        return key
        for k in self.dImages.keys():
            if _(k)==key:
                return k
        return key
    def __getValMlDict__(self):
        d={}
        langs=self.doc.GetLanguages()
        for tup in langs:
            d[tup[1]]=''
        return d
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            self.dData={}
            langs=self.doc.GetLanguages()
            attrNode=self.doc.getChild(node,'attributes')
            if attrNode is not None:
                for o in self.doc.getChilds(attrNode):
                    sAttr=self.doc.getTagName(o)
                    if self.dData.has_key(sAttr):
                        continue
                    d={}
                    for tup in langs:
                        d[tup[1]]=self.doc.getNodeTextLang(attrNode,sAttr,lang=tup[1])
                    self.dData[sAttr]=d
            else:
                self.dData={}
            if self.__isLogDebug__():
                self.__logDebug__('%s'%(self.__logFmt__(self.dData)))
        except:
            self.__logTB__()
            self.dData={}
        self.__showLst__()
    def __showLst__(self):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        try:
            self.lstAttr.DeleteAllItems()
            self.txtAttrVal.SetValue('')
            keys=self.dData.keys()
            keys.sort()
            lang=self.viLang.GetValue()
            for k in keys:
                #self.lstAttr.InsertStringItem(0,k)
                #self.lstAttr.InsertStringItem(1,attrs[k])
                sAttr,sSuff=self.__getAttrNameSuff__(k)
                try:
                    imgId=self.imgDict[k]
                except:
                    imgId=-1
                sAttrName=self.__getAttrNameML__(k)
                
                index = self.lstAttr.InsertImageStringItem(sys.maxint, sAttrName, imgId)
                if index==self.selectedIdx:
                    self.lstAttr.SetItemState(index,wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
                    self.cmbAttrName.SetValue(sAttr)
                    self.txtAttrVal.SetValue(sSuff)
                    try:
                        self.txtAttrVal.SetValue(self.dData[k][lang])
                    except:
                        pass
                try:
                    self.lstAttr.SetStringItem(index, 1, self.dData[k][lang])
                except:
                    pass
        except:
            self.__logTB__()
    def GetValueDict(self):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        try:
            d={}
            keys=self.dData.keys()
            keys.sort()
            lang=self.viLang.GetValue()
            for k in keys:
                d[k]=self.dData[k][lang]
        except:
            self.__logTB__()
        return d
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            attrNode=self.doc.getChildForced(node,'attributes')
            if attrNode is None:
                self.__logCritical__('%s'%(self.doc.getKey(self.node)))
                return  # fix me, add element node
            childs=self.doc.getChilds(attrNode)
            d={}
            for c in childs:
                self.doc.deleteNode(c,attrNode)
            langs=self.doc.GetLanguages()
            keys=self.dData.keys()
            keys.sort()
            for k in keys:
                sAttr,sSuff=self.__getAttrNameSuff__(k)
                sAttrKey=self.__getAttrKey__(sAttr)
                sAttrName=self.__buildAttrName__(sAttrKey,sSuff)
                d=self.dData[k]
                for tup in langs:
                    try:
                        sAttrVal=d[tup[1]]
                        self.doc.setNodeTextLang(attrNode,sAttrName,sAttrVal,lang=tup[1])
                    except:
                        pass
        except:
            self.__logTB__()
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            self.cbApply.Enable(False)
            self.cbDel.Enable(False)
            self.cmbAttrName.Enable(False)
            self.txtAttrVal.Enable(False)
            self.lstAttr.Enable(False)
        else:
            self.cbApply.Enable(True)
            self.cbDel.Enable(True)
            self.cmbAttrName.Enable(True)
            self.txtAttrVal.Enable(True)
            self.lstAttr.Enable(True)
    def OnCbApplyButton(self, event):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        try:
            if VERBOSE>0:
                if self.__isLogDebug__():
                    self.__logDebug__('dData;%s;lang:%s'%(self.viLang.GetValue(),
                            self.__logFmt__(self.dData)))
            sNewAttr=self.cmbAttrName.GetValue()
            sSuff=self.txtSuff.GetValue()
            if len(sSuff)>0:
                sNewAttr+='_%s'%sSuff
            for i in range(0,self.lstAttr.GetItemCount()):
                it=self.lstAttr.GetItem(i)
                sAttrName=it.m_text
                if sAttrName==sNewAttr:
                    it.m_col=1
                    it.m_mask = wx.LIST_MASK_TEXT
                    it.m_text=self.txtAttrVal.GetValue()
                    self.lstAttr.SetItem(it)
                    self.dData[sAttrName][self.viLang.GetValue()]=self.txtAttrVal.GetValue()
                    return
            try:
                sImgName,sSuff=self.__getAttrNameSuff__(sNewAttr)
                imgId=self.imgDict[self.__getAttrKey__(sImgName)]
            except:
                imgId=-1
            self.dData[sNewAttr]=self.__getValMlDict__()
            self.dData[sNewAttr][self.viLang.GetValue()]=self.txtAttrVal.GetValue()
            index = self.lstAttr.InsertImageStringItem(sys.maxint, sNewAttr, imgId)
            self.lstAttr.SetStringItem(index, 1, self.txtAttrVal.GetValue())
            for i in range(self.lstAttr.GetItemCount()):
                self.lstAttr.SetItemState(i,0,wx.LIST_STATE_SELECTED)
            self.lstAttr.SetItemState(index,wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
            if VERBOSE>0:
                if self.__isLogDebug__():
                    self.__logDebug__('dData;%s;lang:%s'%(self.viLang.GetValue(),
                            self.__logFmt__(self.dData)))
        except:
            self.__logTB__()
        event.Skip()

    def OnCbDelButton(self, event):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        if self.selectedIdx>=0:
            sAttr=self.lstAttr.GetItemText(self.selectedIdx)
            del self.dData[sAttr]
            self.lstAttr.DeleteItem(self.selectedIdx)
        event.Skip()
    def OnLstAttrListItemSelected(self, event):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        idx=event.GetIndex()
        #it=event.GetItem()
        #it.m_mask = wx.LIST_MASK_TEXT
        #it.m_itemId=idx
        #it.m_col=0
        it=self.lstAttr.GetItem(idx,0)
        sAttrName=it.m_text
        #it.m_col=1
        it=self.lstAttr.GetItem(idx,1)
        sAttrVal=it.m_text
        #self.txtAttrName.SetValue(sAttrName)
        
        sAttrName,sSuff=self.__getAttrNameSuff__(sAttrName)
        self.txtSuff.SetValue(sSuff)
        self.cmbAttrName.SetValue(sAttrName)
        self.txtAttrVal.SetValue(sAttrVal)
        self.selectedIdx=it.m_itemId
        event.Skip()

    def OnLstAttrListItemDeselected(self, event):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        self.selectedIdx=-1
        self.cmbAttrName.SetValue('')
        self.txtAttrVal.SetValue('')
        self.txtSuff.SetValue('')
        event.Skip()
    def GetVal(self,attr,suff=None):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        if suff is None:
            lst=[]
            for i in range(0,self.lstAttr.GetItemCount()):
                it=self.lstAttr.GetItem(i)
                sAttrName=it.m_text
                
                sAttr,sSuff=self.__getAttrNameSuff__(sAttrName)
                if sAttr==attr:
                    it=self.lstAttr.GetItem(i,1)
                    sAttrVal=it.m_text
                    lst.append((sAttr,sSuff,sAttrVal))
            return lst
        else:
            for i in range(0,self.lstAttr.GetItemCount()):
                it=self.lstAttr.GetItem(i)
                sAttrName=it.m_text
                
                sAttr,sSuff=self.__getAttrNameSuff__(sAttrName)
                if sAttr==attr:
                    if sSuff==suff:
                        it=self.lstAttr.GetItem(i,1)
                        sAttrVal=it.m_text
                        return sAttrVal
            return None
    def OnCmbAttrNameTextEnter(self, event):
        event.Skip()

    def OnCmbAttrNameCombobox(self, event):
        event.Skip()

    def OnCmbAttrNameText(self, event):
        event.Skip()

    def OnViLangVtlangSelectionChanged(self, event):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            self.__showLst__()
        except:
            self.__logTB__()
        event.Skip()
            
