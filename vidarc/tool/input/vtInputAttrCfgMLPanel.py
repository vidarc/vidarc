#Boa:FramePanel:vtInputAttrCfgMLPanel
#----------------------------------------------------------------------
# Name:         vtInputAttrCfgMLPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060502
# CVS-ID:       $Id: vtInputAttrCfgMLPanel.py,v 1.32 2012/09/10 03:14:36 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import wx
    import vidarc.tool.input.vtInputValue
    import vidarc.tool.lang.vtLgSelector
    import wx.lib.buttons
    from wx.lib.anchors import LayoutAnchors
    import sys
    import types
    from vidarc.tool.net.vNetXmlWxGui import *
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel
    from vidarc.tool.xml.vtXmlDomConsumerLang import vtXmlDomConsumerLang
    from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_GOT_CONTENT
    from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_GET_NODE
    from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_REMOVE_NODE
    from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_LOCK
    from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_UNLOCK
    from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_SET_NODE

    from vidarc.tool.input.vtInputAttrCfgMLDialog import *

    import vidarc.tool.art.vtArt as vtArt
    #import vidarc.tool.lang.vtLgBase as vtLgBase
    from vidarc.tool.misc.vtmMsgDialog import vtmMsgDialog
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VTINPUTATTRCFGMLPANEL, wxID_VTINPUTATTRCFGMLPANELCBAPPLY, 
 wxID_VTINPUTATTRCFGMLPANELCBCFG, wxID_VTINPUTATTRCFGMLPANELCBDEL, 
 wxID_VTINPUTATTRCFGMLPANELCMBATTRNAME, wxID_VTINPUTATTRCFGMLPANELLBLATTRNAME, 
 wxID_VTINPUTATTRCFGMLPANELLBLATTRVAL, wxID_VTINPUTATTRCFGMLPANELLSTATTR, 
 wxID_VTINPUTATTRCFGMLPANELTXTSUFF, wxID_VTINPUTATTRCFGMLPANELVIATTRVAL, 
 wxID_VTINPUTATTRCFGMLPANELVILGSEL, 
] = [wx.NewId() for _init_ctrls in range(11)]

class vtInputAttrCfgMLPanel(wx.Panel,vtXmlNodePanel,vtXmlDomConsumerLang):
    VERBOSE=0
    ATTR_UNCFG=u'???'
    ATTR_UNCFG_TYPECFG={
        'type':'text',
        'len':255
        }
    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblAttrName, (0, 0), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.cmbAttrName, (0, 1), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.txtSuff, (0, 2), border=0, flag=wx.EXPAND, span=(1, 1))
        parent.AddWindow(self.cbCfg, (0, 3), border=0, flag=wx.EXPAND, span=(1, 1))
        parent.AddWindow(self.lblAttrVal, (1, 0), border=0, flag=wx.EXPAND, span=(1, 1))
        parent.AddWindow(self.viAttrVal, (1, 1), border=0, flag=wx.EXPAND, span=(1, 2))
        parent.AddWindow(self.cbApply, (1, 3), border=0, flag=wx.EXPAND, span=(1, 1))
        parent.AddWindow(self.cbDel, (2, 3), border=0, flag=wx.EXPAND, span=(1, 1))
        parent.AddWindow(self.viLgSel, (3, 3), border=8, flag=wx.ALIGN_CENTER | wx.ALL, span=(1, 1))
        parent.AddWindow(self.lstAttr, (2, 0), border=0, flag=wx.EXPAND, span=(3, 3))

    def _init_coll_lstAttr_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'attribute'), width=120)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'value'), width=250)

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=4, vgap=4)

        self._init_coll_gbsData_Items(self.gbsData)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTINPUTATTRCFGMLPANEL,
              name=u'vtInputAttrCfgMLPanel', parent=prnt, pos=wx.Point(208,
              200), size=wx.Size(308, 180), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(300, 153))
        self.SetMinSize(wx.Size(-1, -1))

        self.lblAttrName = wx.StaticText(id=wxID_VTINPUTATTRCFGMLPANELLBLATTRNAME,
              label=_(u'attribute'), name=u'lblAttrName', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(42, 13), style=wx.ALIGN_RIGHT)
        self.lblAttrName.SetMinSize(wx.Size(-1, -1))

        self.cmbAttrName = wx.Choice(choices=[],
              id=wxID_VTINPUTATTRCFGMLPANELCMBATTRNAME, name=u'cmbAttrName',
              parent=self, pos=wx.Point(46, 0), size=wx.Size(130, 21), style=0)
        self.cmbAttrName.SetMinSize(wx.Size(-1, -1))
        self.cmbAttrName.Bind(wx.EVT_CHOICE, self.OnCmbAttrNameCombobox,
              id=wxID_VTINPUTATTRCFGMLPANELCMBATTRNAME)
        self.cmbAttrName.Bind(wx.EVT_CHAR, self.OnCmbAttrNameChar)

        self.txtSuff = wx.TextCtrl(id=wxID_VTINPUTATTRCFGMLPANELTXTSUFF,
              name=u'txtSuff', parent=self, pos=wx.Point(180, 0),
              size=wx.Size(100, 21), style=0, value=u'')
        self.txtSuff.SetMinSize(wx.Size(-1, -1))

        self.lblAttrVal = wx.StaticText(id=wxID_VTINPUTATTRCFGMLPANELLBLATTRVAL,
              label=_(u'value'), name=u'lblAttrVal', parent=self,
              pos=wx.Point(0, 34), size=wx.Size(26, 13), style=wx.ALIGN_RIGHT)
        self.lblAttrVal.SetMinSize(wx.Size(-1, -1))

        self.viAttrVal = vidarc.tool.input.vtInputValue.vtInputValue(id=wxID_VTINPUTATTRCFGMLPANELVIATTRVAL,
              name=u'viAttrVal', parent=self, pos=wx.Point(79, 34),
              size=wx.Size(221, 30), style=0)
        self.viAttrVal.SetMinSize(wx.Size(-1, -1))
        self.viAttrVal.Bind(vidarc.tool.input.vtInputValue.vEVT_VTINPUT_VALUE_APPLY,
              self.OnViAttrValVtinputValueApply)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTINPUTATTRCFGMLPANELCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.ApplyAttr), label=_(u'Apply'),
              name=u'cbApply', parent=self, pos=wx.Point(220, 34),
              size=wx.Size(76, 30), style=0)
        self.cbApply.SetMinSize(wx.Size(-1, -1))
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VTINPUTATTRCFGMLPANELCBAPPLY)

        self.cbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTINPUTATTRCFGMLPANELCBDEL,
              bitmap=vtArt.getBitmap(vtArt.DelAttr), label=_(u'Del'),
              name=u'cbDel', parent=self, pos=wx.Point(284, 58),
              size=wx.Size(76, 30), style=0)
        self.cbDel.SetMinSize(wx.Size(-1, -1))
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VTINPUTATTRCFGMLPANELCBDEL)

        self.lstAttr = wx.ListCtrl(id=wxID_VTINPUTATTRCFGMLPANELLSTATTR,
              name=u'lstAttr', parent=self, pos=wx.Point(0, 68),
              size=wx.Size(216, 85), style=wx.LC_REPORT|wx.LC_SORT_ASCENDING)
        self.lstAttr.SetMinSize(wx.Size(-1, -1))
        self._init_coll_lstAttr_Columns(self.lstAttr)
        self.lstAttr.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstAttrListItemSelected,
              id=wxID_VTINPUTATTRCFGMLPANELLSTATTR)
        self.lstAttr.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstAttrListItemDeselected,
              id=wxID_VTINPUTATTRCFGMLPANELLSTATTR)

        self.cbCfg = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTINPUTATTRCFGMLPANELCBCFG,
              bitmap=vtArt.getBitmap(vtArt.Config), label=_(u'Config'),
              name=u'cbCfg', parent=self, pos=wx.Point(284, 0), size=wx.Size(76,
              30), style=0)
        self.cbCfg.SetMinSize(wx.Size(-1, -1))
        self.cbCfg.Bind(wx.EVT_BUTTON, self.OnCbCfgButton,
              id=wxID_VTINPUTATTRCFGMLPANELCBCFG)

        self.viLgSel = vidarc.tool.lang.vtLgSelector.vtLgSelector(id=wxID_VTINPUTATTRCFGMLPANELVILGSEL,
              name=u'viLgSel', parent=self, pos=wx.Point(284, 116),
              size=wx.Size(30, 30), style=0)
        self.viLgSel.Bind(vidarc.tool.lang.vtLgSelector.vEVT_VTLANG_SELECTION_CHANGED,
              self.OnViLgSelVtlangSelectionChanged)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name,bAutoApply=False):
        global _
        _=vtLgBase.assignPluginLang('vtInput')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,applName=_(u'vtInput Attr ML'),
                lWidgets=[self.lstAttr])
        vtXmlDomConsumerLang.__init__(self)
        self.dlgCfg=None
        self.bModified=False
        self.selectedIdx=-1
        
        self.SetMinSize(wx.Size(-1, -1))
        #self.fgsData.SetMinSize(wx.Size(-1, -1))
        #self.bxsAttr.SetMinSize(wx.Size(-1, -1))
        #self.bxsVal.SetMinSize(wx.Size(-1, -1))
        #self.bxsBt.SetMinSize(wx.Size(-1, -1))
        #self.Layout()
        #self.fgsData.SetSizeHints(self)

        self.lTagNames2Base=[]
        self.bEnableMark=True
        self.dCfg={}
        self.attrs={}
        self.attrsFlt={}
        self.lLstAttr=[]
        self.funcIs2Skip=None
        
        self.__setupImageList__()
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
        
        self.gbsData.AddGrowableCol(1,2)
        self.gbsData.AddGrowableCol(2,1)
        self.gbsData.AddGrowableRow(4,1)
        
        self.gbsData.Layout()
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        #self.Refresh()
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        vtXmlNodePanel.__del__(self)
    def SetFuncIs2Skip(self,func):
        self.funcIs2Skip=func
    def LayoutOld(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        wx.Panel.Layout(self)
    def SetSizeOld(self,size):
        print size
        wx.Panel.SetSize(self,size)
    def OnSize(self,evt):
        if VERBOSE>0:
            self.__logDebug__('size:%r;cfg;%r;achc;%r;%r;suff;%r;%r;attr;%r;%r;lst;%r;%r'%(evt.GetSize(),
                    self.cbCfg.GetPosition(),
                    self.cmbAttrName.GetPosition(),self.cmbAttrName.GetSize(),
                    self.txtSuff.GetPosition(),self.txtSuff.GetSize(),
                    self.viAttrVal.GetPosition(),self.viAttrVal.GetSize(),
                    self.lstAttr.GetPosition(),self.lstAttr.GetSize()))
        evt.Skip()
    def SetEnableMarkOld(self,flag):
        self.bEnableMark=flag
    def SetTagNames2Base(self,lst):
        if self.__isLogDebug__():
            self.__logDebug__('lst:%s'%(self.__logFmt__(lst)))
        self.lTagNames2Base=lst
        self.dCfg={}
        if self.objRegNode is not None:
            if hasattr(self.objRegNode,'SetCfgBase'):
                if self.__isLogWarn__():
                    self.__logWarn__('override reg node setting:%s'%(vtLog.pformat(lst)))
                self.objRegNode.SetCfgBase(lst)

    def __buildCfg__(self):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            if self.objRegNode is not None:
                lang=self.viLgSel.GetValue()
                if hasattr(self.objRegNode,'GetCfgDict'):
                    self.dCfg=self.objRegNode.GetCfgDict(None,lang)
                    if VERBOSE>4:
                        if self.__isLogDebug__():
                            self.__logDebug__('dCfg:%s'%(vtLog.pformat(self.dCfg)))
                    return
            self.dCfg={}
            if self.doc is None:
                return
            if len(self.lTagNames2Base)==0:
                return
            nodeCfg=self.doc.getChildByLst(self.doc.getBaseNode(),self.lTagNames2Base)
            if nodeCfg is None:
                par=self.doc.getBaseNode()
                if par is None:
                    return
                for tag in self.lTagNames2Base:
                    c=self.doc.getChild(par,tag)
                    if c is None:
                        c=self.doc.createSubNode(par,tag)
                        self.doc.setAttribute(c,self.doc.attr,'')
                        self.doc.AlignNode(c)
                        self.doc.addNode(par,c)
                    par=c
                nodeCfg=c
            lang=self.viLgSel.GetValue()
            if self.VERBOSE:
                print nodeCfg
                print lang
            obj=self.doc.GetReg('attrML')
            if nodeCfg is None:
                self.__logError__('reg node %s not found'%(self.lTagNames2Base))
                return
            if obj is None:
                self.__logError__('reg node %s not found'%('attrML'))
                return
            self.dCfg=obj.GetCfgDict(nodeCfg,lang)
            if hasattr(self.objRegNode,'GetCfgDict'):
                self.dCfg.update(self.objRegNode.GetCfgDict(nodeCfg,lang))
            if VERBOSE>4:
                if self.__isLogDebug__():
                    self.__logDebug__('dCfg:%s'%(vtLog.pformat(self.dCfg)))
        except:
            self.__logTB__()
    def __setupImageList__(self):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        if VERBOSE>0:
            self.__logDebug__('dCfg;%s'%(self.__logFmt__(self.dCfg)))
        self.imgDict={}
        self.imgLstTyp=wx.ImageList(16,16)
        keys=self.dCfg.keys()
        l=[]
        for k in keys:
            l.append(self.dCfg[k])
        l.sort()
        try:
            iSel=self.cmbAttrName.GetSelection()
            iIDSel=self.cmbAttrName.GetClientData(iSel)
        except:
            iSel=0
            iIDSel=-1
        self.cmbAttrName.Clear()
        iSel=0
        i=0
        self.imgLstTyp.Add(vtArt.getBitmap(vtArt.Error))
        for tup in l:
            nodeTmp=self.doc.getNodeByIdNum(tup[2])
            sImg=self.doc.getNodeText(nodeTmp,'img')
            if len(sImg)>0:
                if sImg[:3]=='wx.':
                    bmp = wx.ArtProvider_GetBitmap(eval(sImg), eval('wx.ART_TOOLBAR'), (16,16))
                elif sImg[:6]=='vtArt.':
                    bmp=vtArt.getBitmap(eval(sImg))
                else:
                    stream = cStringIO.StringIO(binascii.unhexlify(sImg))
                    bmp=wx.BitmapFromImage(wx.ImageFromStream(stream))
                self.imgDict[tup[1]]=self.imgLstTyp.Add(bmp)
            self.cmbAttrName.Append(tup[0],tup[2])
            if tup[2]==iIDSel:
                iSel=i
            i+=1
        self.lstAttr.SetImageList(self.imgLstTyp,wx.IMAGE_LIST_SMALL)
        
        self.cmbAttrName.SetSelection(iSel)
        if VERBOSE>4:
            if self.__isLogDebug__():
                self.__logDebug__('OnCmbAttrNameCombobox;callafter'%())
        wx.CallAfter(self.OnCmbAttrNameCombobox,None)
    def SetModifiedOld(self,state):
        self.bModified=state
    def GetModifiedOld(self):
        return self.bModified
    def __isModified__Old(self):
        if self.bModified:
            return True
        return False
    def __markModified__Old(self,flag=True):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        if self.bEnableMark==False:
            return
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'__markModified__;flag:%s'%(flag),
        #                self)
        if flag:
            f=self.lstAttr.GetFont()
            f.SetWeight(wx.FONTWEIGHT_BOLD)
            self.lstAttr.SetFont(f)
        else:
            f=self.lstAttr.GetFont()
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
            self.lstAttr.SetFont(f)
        self.lstAttr.Refresh()
    def ClearLang(self):
        self.dCfg={}
    def UpdateLang(self):
        self.viLgSel.SetValue(self.doc.GetLang())
        self.__buildCfg__()
        #self.__setupImageList__()
    def __Clear__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        try:
            self.attrs={}
            self.attrsFlt={}
            #self.__markModified__(False)
            try:
                self.cmbAttrName.SetSelection(0)
                if VERBOSE>4:
                    if self.__isLogDebug__():
                        self.__logDebug__('OnCmbAttrNameCombobox;callafter'%())
                wx.CallAfter(self.OnCmbAttrNameCombobox,None)
            except:
                pass
            self.viAttrVal.Clear()
            self.txtSuff.SetValue('')
            self.lstAttr.DeleteAllItems()
            self.lLstAttr=[]
            self.selectedIdx=-1
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def IsBusy(self):
        return False
    def Stop(self):
        pass
    def SetRegNode(self,obj):
        vtXmlNodePanel.SetRegNode(self,obj)
        self.SetTagNames2Base(obj.GetCfgBase())
        if self.doc is not None:
            self.__buildCfg__()
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        #if doc is None:
        #    return
        #if bNet==True:
        #    EVT_NET_XML_GOT_CONTENT(doc,self.OnGotContent)
        #    EVT_NET_XML_GET_NODE(doc,self.OnGetNode)
        #    EVT_NET_XML_REMOVE_NODE(doc,self.OnRemoveNode)
        #    EVT_NET_XML_LOCK(doc,self.OnLock)
        #    EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        vtXmlDomConsumerLang.SetDoc(self,doc)
        if self.dlgCfg is not None:
            self.dlgCfg.SetDoc(doc)
        #self.viAttrVal.SetDoc(doc)
        self.viLgSel.SetDoc(doc)
        self.__buildCfg__()
        if doc is None:
            return
        self.__setupImageList__()
    def OnGotContent(self,evt):
        evt.Skip()
        self.__buildCfg__()
        self.__setupImageList__()
    def __getAttrNameSuff__(self,sAttrName):
        strs=string.split(sAttrName,'_')
        try:
            if len(strs)>1:
                sSuff=strs[-1]
                sAttrName=sAttrName[:-(len(sSuff)+1)]
            else:
                sSuff=''
        except:
            sSuff=''
        return sAttrName,sSuff
    def __getAttrName__(self,sAttrName):
        sAttr,sSuff=self.__getAttrNameSuff__(sAttrName)
        return self.__buildAttrName__(sAttr,sSuff)
    def __buildAttrName__(self,sAttr,sSuff):
        if len(sSuff)>0:
            return sAttr+'_'+sSuff
        return sAttr
    def __getAttrNameSuffML__(self,sAttrName):
        strs=string.split(sAttrName,'_')
        try:
            if len(strs)>1:
                sSuff=strs[-1]
                sAttrName=sAttrName[:-(len(sSuff)+1)]
            else:
                sSuff=''
        except:
            sSuff=''
        for tup in self.dCfg.values():
            if tup[1]==sAttrName:
                return tup[0],sSuff
        return sAttrName,sSuff
    def __getAttrNameSuffML2Key__(self,sAttrName):
        strs=string.split(sAttrName,'_')
        try:
            if len(strs)>1:
                sSuff=strs[-1]
                sAttrName=sAttrName[:-(len(sSuff)+1)]
            else:
                sSuff=''
        except:
            sSuff=''
        for tup in self.dCfg.values():
            if tup[0]==sAttrName:
                return tup[1],sSuff
        return sAttrName,sSuff
    def __getAttrNameML__(self,sAttrName):
        sAttr,sSuff=self.__getAttrNameSuffML__(sAttrName)
        return self.__buildAttrName__(sAttr,sSuff)
    def __getAttrTupByAID__(self,iAID):
        for t in self.dCfg.itervalues():
            if t[2]==iAID:
                return t
        return None
    def __hasAttrKey__(self,key):
        return self.dCfg.has_key(key)
    def __SetNode__(self,node):
        if VERBOSE>0:
            self.__logDebug__(''%())
        try:
            lang=self.viLgSel.GetValue()
            for o in self.doc.getChildsNoAttr(node,self.doc.attr):
                sAttrName=self.doc.getTagName(o)
                sAttr,sAttrSuf=self.__getAttrNameSuff__(sAttrName)
                if self.funcIs2Skip is not None:
                    if self.funcIs2Skip(sAttr):
                        continue
                if self.objRegNode.IsAttrHidden(sAttr):
                    continue
                if self.__hasAttrKey__(sAttr):
                    s=self.doc.getText(o)
                    if self.attrs.has_key(sAttrName)==False:
                        self.attrs[sAttrName]={}
                    sLg=self.doc.getAttribute(o,'language')
                    self.attrs[sAttrName][sLg]=s
                else:
                    # add faulty
                    s=self.doc.getText(o)
                    self.attrsFlt[sAttrName]=s
            if VERBOSE>4:
                if self.__isLogDebug__():
                    self.__logDebug__('attrs:%s;attrsFlt:%s'%(self.__logFmt__(self.attrs),
                            self.__logFmt__(self.attrsFlt)))
            self.__showAttrs__()
        except:
            self.__logTB__()
    def __showAttrs__(self):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        try:
            if VERBOSE>0:
                self.__logDebug__('imgDict;%s;attrs;%s;attrFlt;%s'%(\
                        self.__logFmt__(self.imgDict),
                        self.__logFmt__(self.attrs),
                        self.__logFmt__(self.attrsFlt)))
            self.lstAttr.DeleteAllItems()
            self.lLstAttr=[]
            keys=self.attrsFlt.keys()
            keys.sort()
            for k in keys:
                sVal=self.attrsFlt[k]
                index = self.lstAttr.InsertImageStringItem(sys.maxint, k, 0)
                self.lstAttr.SetStringItem(index, 1, sVal)
                self.lstAttr.SetItemData(index,len(self.lLstAttr))
                self.lLstAttr.append(k)
            self.__ensureUnCfgAttr__(len(keys)!=0)
            keys=self.attrs.keys()
            keys.sort()
            for k in keys:
                #self.lstAttr.InsertStringItem(0,k)
                #self.lstAttr.InsertStringItem(1,attrs[k])
                sAttr,sSuff=self.__getAttrNameSuff__(k)
                if self.VERBOSE:
                    print 'attr',k
                    print '    ',sAttr,sSuff
                try:
                    imgId=self.imgDict[sAttr]
                except:
                    imgId=-1
                sAttrName=self.__getAttrNameML__(k)
                try:
                    sVal=self.attrs[k][self.viLgSel.GetValue()]
                except:
                    try:
                        sVal=self.attrs[k]['en']
                    except:
                        try:
                            sVal=self.attrs[k]['']
                        except:
                            sVal=''
                #sAttrName,sVal2Disp=self.__getVal2Disp__(k,sVal)
                index = self.lstAttr.InsertImageStringItem(sys.maxint, sAttrName, imgId)
                self.lstAttr.SetStringItem(index, 1, sVal)
                self.lstAttr.SetItemData(index,len(self.lLstAttr))
                self.lLstAttr.append(k)
        except:
            self.__logTB__()
    def __ensureUnCfgAttr__(self,bFlag):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        iCount=self.cmbAttrName.GetCount()
        if iCount>0:
            iPos=iCount-1
            sName=self.cmbAttrName.GetString(iPos)
            if bFlag:
                if sName==self.ATTR_UNCFG:
                    return
                else:
                    self.cmbAttrName.Append(self.ATTR_UNCFG,-1)
            else:
                if sName==self.ATTR_UNCFG:
                    self.cmbAttrName.Delete(iPos)
        else:
            if bFlag:
                self.cmbAttrName.Append(self.ATTR_UNCFG,-1)
    def __GetNode__(self,node):
        try:
            bDbg=False
            if VERBOSE>0 or self.VERBOSE>0:
                if self.__isLogDebug__():
                    bDbg=True
            if bDbg:
                self.__logDebug__('attrs:%s;attrsFlt:%s;lLstAttr:%s'%(\
                        self.__logFmt__(self.attrs),
                        self.__logFmt__(self.attrsFlt),
                        self.__logFmt__(self.lLstAttr)))
            if node is None:
                return
            childs=self.doc.getChilds(node)
            d={}
            for c in childs:
                sAttrName=self.doc.getTagName(c)
                sAttr,sSuff=self.__getAttrNameSuff__(sAttrName)
                if self.funcIs2Skip is not None:
                    if self.funcIs2Skip(sAttr):
                        continue
                if self.objRegNode.IsAttrHidden(sAttr):
                    continue
                if self.__hasAttrKey__(sAttr):
                    #self.doc.deleteNode(c,node)
                    self.doc.__deleteNode__(c,node)
            keys=self.attrs.keys()
            keys.sort()
            if bDbg:
                self.__logDebug__('keys:%s'%(self.__logFmt__(keys)))
            for k in keys:
                sAttr,sSuff=self.__getAttrNameSuff__(k)
                id=self.__getSelTypeID__(sAttr)
                d=self.attrs[k]
                if bDbg:
                    self.__logDebug__('k:%s;sAttr:%s,sSuff:%s;id:%s;d:%s'%(k,
                            sAttr,sSuff,repr(id),self.__logFmt__(d)))
                if d.has_key(''):
                    if id is None or self.doc.IsKeyValid(id)==False:
                        self.doc.setNodeText(node,k,d[''])
                    else:
                        c=self.doc.createSubNodeDict(node,{'tag':k,'attr':('aid','%08d'%id),'val':d['']})
                else:
                    kll=d.keys()
                    kll.sort()
                    for kl in kll:
                        if id is None or self.doc.IsKeyValid(id)==False:
                            self.doc.setNodeTextLang(node,k,d[kl],kl)
                        else:
                            c=self.doc.createSubNodeDict(node,{'tag':k,'attr':('aid','%08d'%id),'val':d[kl]})
                            self.doc.setAttribute(c,'language',kl)
            self.__delAttrFltDeleted__(node)
            if bDbg:
                self.__logDebug__('done'%())
        except:
            self.__logTB__()
    def __delAttrFltDeleted__(self,node):
        keys=self.attrsFlt.keys()
        for k in keys:
            if self.attrsFlt[k] is None:
                if VERBOSE>4:
                    if self.__isLogInfo__():
                        self.__logInfo__('delete child:%s'%(k))
                childs=self.doc.getChilds(node,k)
                for c in childs:
                    #self.doc.deleteNode(c,node)
                    self.doc.__deleteNode__(c,node)
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        self.viAttrVal.Lock(flag)
        if flag:
            self.cbApply.Enable(False)
            self.cbDel.Enable(False)
            self.cmbAttrName.Enable(False)
            self.txtSuff.Enable(False)
            self.viAttrVal.Enable(False)
            self.lstAttr.Enable(False)
        else:
            self.cbApply.Enable(True)
            self.cbDel.Enable(True)
            self.cmbAttrName.Enable(True)
            self.txtSuff.Enable(True)
            self.viAttrVal.Enable(True)
            self.lstAttr.Enable(True)
    def __setAttrVal__(self,attr,attrSuff,sVal):
        tup,tupFull=self.__getSelTypeFull__(attr)
        sVal=sVal.strip()
        self.__logDebug__('dCfg;%s'%(self.__logFmt__(self.dCfg)))
        try:
            bDbg=False
            lang=self.viLgSel.GetValue()
            if VERBOSE>0 or self.VERBOSE>0:
                if self.__isLogDebug__():
                    bDbg=True
                    self.__logDebug__('attr:%s;attrSuff:%s;sVal:%s;'\
                            'tup;%s;tupFull;%s;lang:%r;attrs;%s'%(attr,
                            attrSuff,sVal,
                            self.__logFmt__(tup),self.__logFmt__(tupFull),
                            lang,self.__logFmt__(self.attrs)))
            bFound=False
            #for k in tupFull.keys():
            k='val'
            if k in tupFull:
                d=tupFull[k]
                if type(d)==types.DictType:
                    if bDbg:
                        self.__logDebug__('key:%s;d:%s'%(k,self.__logFmt__(d)))
                    if lang in d:
                        if d[lang]==sVal:
                            self.attrs[attrSuff]=d
                            bFound=True
                            #break
                    #if bFound==False:
                    #    if attrSuff not in self.attrs:
                    #        self.attrs[attrSuff]={lang:sVal}
                    #    else:
                    #        self.attrs[attrSuff][lang]=sVal
                    #    bFound=True
                    #else:
                        #if '' in d:
                        #    if d['']==sVal:
                        #        self.attrs[attrSuff]=d
                        #        bFound=True
                                #break
            if bFound==False:
                self.attrs[attrSuff]={'':sVal}
            if bDbg:
                    self.__logDebug__('attr:%s;attrSuff:%s;sVal:%s;'\
                            'tup;%s;tupFull;%s;lang:%r;attrs;%s'%(attr,
                            attrSuff,sVal,
                            self.__logFmt__(tup),self.__logFmt__(tupFull),
                            lang,self.__logFmt__(self.attrs)))
        except:
            self.__logTB__()
    def OnCbApplyButton(self, event):
        try:
            self.__logDebug__(''%())
            bDbg=False
            if VERBOSE>0 or self.VERBOSE>0:
                if self.__isLogDebug__():
                    self.__logDebug__(''%())
                    bDbg=True
            if self.IsMainThread()==False:
                self.__logCritical__('called by thread'%())
            
            if self.lstAttr.GetSelectedItemCount()>1:
                self.__logWarn__('%d items selected,apply to multiple '\
                        'attrs is not permitted'%
                        (self.lstAttr.GetSelectedItemCount()),self)
                return
            sNewAttr=self.cmbAttrName.GetStringSelection()
            if bDbg:
                self.__logDebug__('%20s:%s'%('sNewAttr',sNewAttr))
            if sNewAttr==self.ATTR_UNCFG:
                self.__logWarn__('%s;unconfigured attr'%(sNewAttr))
                iIdx=self.lstAttr.GetFirstSelected()
                it=self.lstAttr.GetItem(iIdx,0)
                sNewAttr=it.m_text
                self.__logWarn__('unconfigured attr:%s can not be modified'%(sNewAttr))
                return
                if self.VERBOSE:
                    #print sNewAttr
                    print '%20s:%s'%('sNewAttr',sNewAttr)
                sNewAttr,sSuff=self.__getAttrNameSuff__(sNewAttr)
            
            if bDbg:
                self.__logDebug__('%20s:%s'%('sNewAttr',sNewAttr))
            iAID=self.cmbAttrName.GetClientData(self.cmbAttrName.GetSelection())
            tAttr=self.__getAttrTupByAID__(iAID)
            sAttrName=tAttr[0]
            sAttr=tAttr[1]
            sSuff=self.txtSuff.GetValue()
            sAttrNameSuff=self.__buildAttrName__(sAttrName,sSuff)
            sAttrSuff=self.__buildAttrName__(sAttr,sSuff)
            try:
                imgId=self.imgDict[sAttr]
            except:
                imgId=-1
            #if len(sSuff)>0:
            #    sNewAttr+='_%s'%sSuff
            if bDbg:
                self.__logDebug__('%20s:%s;%20s:%s;%20s:%s;%20s:%s;%s'%('sAttr',
                        sAttr,'sAttrSuff',sAttrSuff,'sAttrName',sAttrName,
                        'sAttrNameSuff',sAttrNameSuff,
                        self.__logFmt__(self.lLstAttr)))
            iLenLstAttr=len(self.lLstAttr)
            #if self.VERBOSE:
            #    print '  %4s, %4s,%-30s,%-35s'%('i','iPos','sAttrTmpp','sAttrSuff')
            for i in range(0,self.lstAttr.GetItemCount()):
                #it=self.lstAttr.GetItem(i)
                #sAttrName=it.m_text
                iPos=self.lstAttr.GetItemData(i)
                if iPos>=iLenLstAttr:
                    continue
                sAttrTmp=self.lLstAttr[iPos]
                #if self.VERBOSE:
                #    print '  %04d, %04d,%-30s,%-35s'%(i,iPos,sAttrTmp,sAttrSuff)
                #if sAttrTmp==sAttr:
                if sAttrTmp==sAttrSuff:
                    it=self.lstAttr.GetItem(i)
                    it.m_col=1
                    it.m_mask = wx.LIST_MASK_TEXT
                    it.m_text=self.viAttrVal.GetValueStr()
                    #sAttr,sSuff=self.__getAttrNameSuff__(k)
                    #sAttr,sSuff=self.__getAttrNameSuffML2Key__(sNewAttr)
                    
                    if bDbg:
                        self.__logDebug__('name suff:%s;attr suff:%s;attr:%s'%(sAttrNameSuff,
                                sAttrSuff,self.attrs[sAttrSuff]))
                    self.__setAttrVal__(sAttr,sAttrSuff,it.m_text)
                    #sAttrNameML=self.__getAttrNameML__(k)
                    #self.attrs[sAttrSuff]=it.m_text
                    self.lstAttr.SetItem(it)
                    self.SetModified(True)
                    return
            if bDbg:
                self.__logDebug__('sNewAttr:%s'%(sNewAttr))
            tup,tupFull=self.__getSelTypeFull__(sAttr)
            if bDbg:
                self.__logDebug__('sAttr:%s;tup:%s;tupFull:%s'%(sAttr,
                        self.__logFmt__(tup),self.__logFmt__(tupFull)))
            if tup is None:
                self.__logDebug__('tup is None'%())
                return
            
            sVal=self.viAttrVal.GetValueStr()
            lang=self.viLgSel.GetValue()
            if bDbg:
                self.__logDebug__('sVal:%s;tup:%s;tupFull:%s'\
                        'sAttrName:%s;sAttrSuff:%s;attrs;%s'%(sVal,
                        self.__logFmt__(tup),self.__logFmt__(tupFull),
                        sAttrName,sAttrSuff,self.__logFmt__(self.attrs)))
            #sAttrSuff=self.__buildAttrName__(sImgName,sSuff)
            self.__setAttrVal__(sAttr,sAttrSuff,sVal)
            if bDbg:
                self.__logDebug__('attrs;%s'%(self.__logFmt__(self.attrs)))
            self.__logDebug__(''%())
            
            index = self.lstAttr.InsertImageStringItem(sys.maxint, sAttrNameSuff, imgId)
            self.lstAttr.SetStringItem(index, 1, self.viAttrVal.GetValueStr())
            self.lstAttr.SetItemData(index,len(self.lLstAttr))
            self.lLstAttr.append(sAttrSuff)
            for i in range(self.lstAttr.GetItemCount()):
                self.lstAttr.SetItemState(i,0,wx.LIST_STATE_SELECTED)
            self.lstAttr.SetItemState(index,wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
            self.SetModified(True)
            self.__logDebug__(''%())
        except:
            self.__logTB__()
        if event is not None:
            event.Skip()

    def OnCbDelButton(self, event):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        if self.selectedIdx>=0:
            sAttr2Del=self.lstAttr.GetItem(self.selectedIdx,0).m_text
            sAttr,sSuff=self.__getAttrNameSuffML2Key__(sAttr2Del)
            sAttrSuff=self.__buildAttrName__(sAttr,sSuff)
            if VERBOSE>4:
                if self.__isLogDebug__():
                    self.__logDebug__('sAttr2Del:%s;attrs:%s suff:%s;attrsuff:%s'%(sAttr2Del,sAttr,sSuff,sAttrSuff))
            if sAttrSuff in self.attrs:
                del self.attrs[sAttrSuff]
            else:
                if sAttr2Del in self.attrsFlt:
                    #del self.attrsFlt[sAttr2Del]
                    self.attrsFlt[sAttr2Del]=None
            iData=self.lstAttr.GetItemData(self.selectedIdx)
            self.lLstAttr[iData]=None
            self.lstAttr.DeleteItem(self.selectedIdx)
            self.SetModified(True)
        event.Skip()
    def OnLstAttrListItemSelected(self, event):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        idx=event.GetIndex()
        #it=event.GetItem()
        #it.m_mask = wx.LIST_MASK_TEXT
        #it.m_itemId=idx
        #it.m_col=0
        it=self.lstAttr.GetItem(idx,0)
        sAttrName=it.m_text
        #it.m_col=1
        it=self.lstAttr.GetItem(idx,1)
        sAttrVal=it.m_text
        iData=self.lstAttr.GetItemData(idx)
        sAttrKey=self.lLstAttr[iData]
        #self.txtAttrName.SetValue(sAttrName)
        if VERBOSE>0:
            self.__logDebug__('sAttrName:%s;aAttrKey;%s'%(sAttrName,sAttrKey))
        sAttrName,sSuff=self.__getAttrNameSuff__(sAttrName)
        sAttrKey,sKeySuff=self.__getAttrNameSuff__(sAttrKey)
        self.txtSuff.SetValue(sSuff)
        if self.VERBOSE:
            print sAttrName,sSuff
        if self.__hasAttrKey__(sAttrKey):
            t=self.dCfg[sAttrKey]
            if self.VERBOSE:
                print t
            idAttr=t[2]
            iSel=-1
            iCount=self.cmbAttrName.GetCount()
            for i in xrange(iCount):
                if self.cmbAttrName.GetClientData(i)==idAttr:
                    iSel=i
            if iSel>=0:
                self.cmbAttrName.SetSelection(iSel)
            else:
                self.__logError__('attr:%s;idAttr:%08d;not found in combobox'%
                        (sAttrKey,idAttr))
            #self.cmbAttrName.SetStringSelection(sAttrName)
        else:
            if self.VERBOSE:
                print 'attr not configured'
            iPos=self.cmbAttrName.GetCount()-1
            if iPos>=0:
                self.cmbAttrName.SetSelection(iPos)
            else:
                self.__logError__('unconfigured attr not in combobox')
        if VERBOSE>4:
            if self.__isLogDebug__():
                self.__logDebug__('OnCmbAttrNameCombobox;callafter'%())
        wx.CallAfter(self.OnCmbAttrNameCombobox,None)
        wx.CallAfter(self.viAttrVal.SetValueStr,sAttrVal,False)
        self.selectedIdx=it.m_itemId
        event.Skip()

    def OnLstAttrListItemDeselected(self, event):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        self.selectedIdx=-1
        self.cmbAttrName.SetSelection(0)
        self.viAttrVal.Clear()
        self.txtSuff.SetValue('')
        if VERBOSE>4:
            if self.__isLogDebug__():
                self.__logDebug__('OnCmbAttrNameCombobox;callafter'%())
        wx.CallAfter(self.OnCmbAttrNameCombobox,None)
        event.Skip()
    def GetVal(self,attr,suff=None):
        if suff is None:
            lst=[]
            for i in range(0,self.lstAttr.GetItemCount()):
                it=self.lstAttr.GetItem(i)
                sAttrName=it.m_text
                
                sAttr,sSuff=self.__getAttrNameSuff__(sAttrName)
                if sAttr==attr:
                    it=self.lstAttr.GetItem(i,1)
                    sAttrVal=it.m_text
                    lst.append((sAttr,sSuff,sAttrVal))
            return lst
        else:
            for i in range(0,self.lstAttr.GetItemCount()):
                it=self.lstAttr.GetItem(i)
                sAttrName=it.m_text
                
                sAttr,sSuff=self.__getAttrNameSuff__(sAttrName)
                if sAttr==attr:
                    if sSuff==suff:
                        it=self.lstAttr.GetItem(i,1)
                        sAttrVal=it.m_text
                        return sAttrVal
            return None
    def OnCmbAttrNameTextEnter(self, event):
        event.Skip()
    def __getSelType__(self):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        try:
            idx=self.cmbAttrName.GetSelection()
            if idx<0:
                return None,False
            sK=self.cmbAttrName.GetStringSelection()
            iID=self.cmbAttrName.GetClientData(idx)
            if VERBOSE>4:
                if self.__isLogDebug__():
                    self.__logDebug__('idx:%d;sK:%s;iID:%d;lLstAttr;%s'%(idx,
                        sK,iID,self.__logFmt__(self.lLstAttr)))
            else:
                if VERBOSE>0:
                    if self.__isLogDebug__():
                        self.__logDebug__('sK:%s;iID:%d'%(sK,iID))
            for tup in self.dCfg.values():
                if tup[2]==iID:
                    return tup[3],True
            if iID<0:
                # unconfigured attr return string configuration
                return self.ATTR_UNCFG_TYPECFG,False
        except:
            self.__logTB__()
        return None,False
    def __getSelTypeFull__(self,attr):
        try:
            for tup in self.dCfg.values():
                if tup[1]==attr:
                    return tup[3],tup[4]
        except:
            self.__logTB__()
        return None,None
    def __getSelTypeID__(self,attr):
        try:
            for tup in self.dCfg.values():
                if tup[1]==attr:
                    return tup[2]
        except:
            self.__logTB__()
        return None
    def OnCmbAttrNameCombobox(self, event):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        if event is not None:
            event.Skip()
        try:
            tup,bEnable=self.__getSelType__()
            if VERBOSE>4:
                if self.__isLogDebug__():
                    self.__logDebug__('tup:%s'%(self.__logFmt__(tup)))
            self.viAttrVal.Set(tup)
            self.viAttrVal.Enable(bEnable)
        except:
            self.__logTB__()
    def OnCmbAttrNameChar(self, event):
        event.Skip()
        k=event.GetKeyCode()
        if k==wx.WXK_RETURN:
            wx.CallAfter(self.viAttrVal.SetFocus)
    def OnCmbAttrNameText(self, event):
        event.Skip()
    def procCfgAddNode(self,oP,oN,lTag=[]):
        try:
            self.__logInfo__('node added;lTag:%r'%(lTag))
            if len(lTag)>0:
                oC=self.doc.createNode(lTag[0])
                self.doc.appendChild(oN,oC)
                self.doc.addNode(oN,oC,self.procCfgAddNode,
                                lTag=self.lTagNames2Base[1:])
        except:
            self.__logTB__()
    def doCfgAddNode(self,iRet):
        try:
            if iRet<=0:
                return 
            oN=self.doc.getBaseNode()
            iOfs=0
            for sTag in self.lTagNames2Base:
                oC=self.doc.getChild(oN,sTag)
                if oC is None:
                    self.__logDebug__('create child node tag:%s'%(sTag))
                    oC=self.doc.createNode(sTag)
                    self.doc.appendChild(oN,oC)
                    self.doc.addNode(oN,oC,self.procCfgAddNode,
                                lTag=self.lTagNames2Base[iOfs+1:])
                else:
                    oN=oC
                    iOfs=iOfs+1
        except:
            self.__logTB__()
    def OnCbCfgButton(self, event):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        event.Skip()
        try:
            nodeCfg=self.doc.getChildByLst(self.doc.getBaseNode(),self.lTagNames2Base)
            if nodeCfg is None:
                self.__logError__('nodeCfg not found;lTagNames2Base:%s'%
                            (self.__logFmt__(self.lTagNames2Base)))
                self.ShowMsgDlg(self.YESNO|self.EXCLAMATION,
                            _(u'Configuration node not found, feature is therefore not available!') ,
                            #'vtInputAttrCfgMLPanel',
                            self.doCfgAddNode)
                nodeCfg=self.doc.getChildByLst(self.doc.getBaseNode(),self.lTagNames2Base)
                if nodeCfg is None:
                    return
            if self.dlgCfg is None:
                self.dlgCfg=vtInputAttrCfgMLDialog(self)
                self.dlgCfg.Centre()
                self.dlgCfg.SetDoc(self.doc)
            idx=self.cmbAttrName.GetSelection()
            if idx>=0:
                iID=self.cmbAttrName.GetClientData(idx)
                if self.doc.IsKeyValid(iID):
                    node=self.doc.getNodeByIdNum(iID)
                    try:
                        self.doc.startEdit(node)
                    except:
                        pass
                    self.dlgCfg.SetNode(node)
                else:
                    node=None
                    self.dlgCfg.SetNode(node)
                    k=self.cmbAttrName.GetStringSelection()
                    if k in self.dCfg:
                        self.dlgCfg.SetByCfgLst(k,self.dCfg[k])
            else:
                node=None
                self.dlgCfg.SetNode(node)
            iRet=self.dlgCfg.ShowModal()
            if iRet==1:
                if node is None:
                    nodeCfg=self.doc.getChildByLst(self.doc.getBaseNode(),self.lTagNames2Base)
                    c=self.doc.createSubNode(nodeCfg,'attrML')
                    self.dlgCfg.GetNode(c)
                    self.doc.addNode(nodeCfg,c)
                else:
                    self.dlgCfg.GetNode(node)
                    try:
                        self.doc.doEdit(node)
                        self.doc.endEdit(node)
                    except:
                        pass
            elif iRet==2:
                bDoAdd=True
                if node is not None:
                    try:
                        if self.dlgCfg.GetTagName()==self.doc.getNodeText(node,'tag'):
                            self.dlgCfg.GetNode(node)
                            try:
                                self.doc.doEdit(node)
                                self.doc.endEdit(node)
                            except:
                                self.__logTB__()
                            bDoAdd=False
                        else:
                            self.doc.endEdit(node)
                    except:
                        pass
                if bDoAdd:
                    sTag=self.dlgCfg.GetTagName()
                    if sTag in self.dCfg:
                        l=self.dCfg[sTag]
                        if self.doc.IsKeyValid(l[2]):
                            bDoAdd=False
                            self.__logError__('tag:%s already configured;dCfg:%s'%
                                    (sTag,self.__logFmt__(self.dCfg)))
                            self.ShowMsgDlg(self.OK|self.EXCLAMATION,
                                    _(u'Tag %s is already configured, adding aborted!')%(sTag) ,
                                    'vtInputAttrCfgMLPanel',None)
                if bDoAdd:
                    nodeCfg=self.doc.getChildByLst(self.doc.getBaseNode(),self.lTagNames2Base)
                    if nodeCfg is None:
                        self.__logError__('nodeCfg not found;lTagNames2Base:%s'%
                                    (self.__logFmt__(self.lTagNames2Base)))
                    c=self.doc.createSubNode(nodeCfg,'attrML')
                    self.dlgCfg.GetNode(c)
                    self.doc.addNode(nodeCfg,c)
            elif iRet==3:
                if node is not None:
                    self.doc.delNode(node)
            else:
                if node is not None:
                    try:
                        self.doc.endEdit(node)
                    except:
                        pass
            if iRet>0:
                self.__buildCfg__()
                self.__setupImageList__()
                self.__showAttrs__()
        except:
            self.__logTB__()
    def OnViLgSelVtlangSelectionChanged(self, event):
        event.Skip()
        self.__buildCfg__()
        self.__setupImageList__()
        self.__showAttrs__()
    def Enable(self,flag):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        wx.Panel.Enable(self,flag)
        self.Refresh()
    def OnViAttrValVtinputValueApply(self,evt):
        evt.Skip()
        self.OnCbApplyButton(None)
