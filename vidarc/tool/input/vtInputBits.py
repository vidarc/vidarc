#----------------------------------------------------------------------------
# Name:         vtInputBits.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060420
# CVS-ID:       $Id: vtInputBits.py,v 1.3 2008/03/22 14:34:24 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import vidarc.tool.log.vtLog as vtLog

wxEVT_VTINPUT_BITS_CHANGED=wx.NewEventType()
vEVT_VTINPUT_BITS_CHANGED=wx.PyEventBinder(wxEVT_VTINPUT_BITS_CHANGED,1)
def EVT_VTINPUT_BITS_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTINPUT_BITS_CHANGED,func)
class vtInputBitsChanged(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTINPUT_BITS_CHANGED(<widget_name>, self.OnChanged)
    """

    def __init__(self,obj,val,s):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTINPUT_BITS_CHANGED)
        self.val=val
        self.sVal=s
    def GetValue(self):
        return self.val
    def GetValueStr(self):
        return self.sVal

class vtInputBits(wx.Panel):
    def __init__(self,*_args,**_kwargs):
        try:
            sz=_kwargs['size']
        except:
            sz=wx.Size(100,34)
            _kwargs['size']=sz
        try:
            self.iDft=_kwargs['default']
            del _kwargs['default']
        except:
            self.iDft=0
        try:
            self.lBits=_kwargs['bits']
            del _kwargs['bits']
        except:
            self.lBits=[]
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        self.SetAutoLayout(True)
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        
        self.lBitsWidgets=[]
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        i=0
        for iBit,bmp,tool in self.lBits:
            try:
                tg = wx.lib.buttons.GenBitmapToggleButton(ID=wx.NewId(),
                        bitmap=bmp, name=u'tgBit%02d'%i, parent=self,
                        pos=wx.Point(34*i, 0), size=wx.Size(30, 30), style=0)
                tg.SetToolTipString(tool)
                if i==0:
                    bxs.AddWindow(tg, 0, border=4, flag=0)
                else:
                    bxs.AddWindow(tg, 0, border=4, flag=wx.LEFT)
                tg.Bind(wx.EVT_TOGGLEBUTTON,self.OnTgChanged,tg)
            except:
                tg=None
                vtLog.vtLngTB(self.__class__.__name__)
            if tg is not None:
                tg.SetBitmapLabel(bmp)
                tg.SetBitmapSelected(bmp)
            self.lBitsWidgets.append(tg)
            i+=1
        self.lblChg = wx.StaticText(id=wx.NewId(),
              label=u' ', name=u'lblChanged', parent=self, pos=wx.Point(24*i, 0),
              size=wx.Size(24, 24), style=0)
        bxs.AddWindow(self.lblChg, 0, border=4, flag=wx.LEFT)
        self.SetSizer(bxs)
        bxs.Layout()
        
        self.bBlock=True
        id=wx.NewId()
        wx.CallAfter(self.__clearBlock__)
        self.tagName=None
        self.bMod=False
        self.bEnableMark=True
    def SetEnableMark(self,flag):
        self.bEnableMark=flag
    def SetTagName(self,tagName):
        self.tagName=tagName
    def SetModified(self,flag):
        self.__markModified__(flag)
    def IsModified(self):
        return self.bMod
    def __markModified__(self,flag=True):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.bEnableMark==False:
            return
        if flag:
            f=self.lblChg.GetFont()
            f.SetWeight(wx.FONTWEIGHT_BOLD)
            self.lblChg.SetFont(f)
            self.lblChg.SetValue('*')
        else:
            f=self.lblChg.GetFont()
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
            self.lblChg.SetFont(f)
        self.lblChg.Refresh()
        self.bMod=flag
    def __clearBlock__(self):
        self.bBlock=False
    def Clear(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.bBlock=True
        self.__markModified__(False)
        for tg in self.lBitsWidgets:
            if tg is not None:
                tg.SetValue(False)
        wx.CallAfter(self.__clearBlock__)
    def Enable(self,state):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        for tg in self.lBitsWidgets:
            if tg is not None:
                tg.Enable(state)
    def SetValue(self,val):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.bBlock=True
        self.__markModified__(False)
        i=0
        for tg in self.lBitsWidgets:
            if tg is not None:
                if (self.lBits[i][0]&val)!=0:
                    tg.SetValue(True)
                else:
                    tg.SetValue(False)
            i+=1
        wx.CallAfter(self.__clearBlock__)
    def GetValue(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        val=0
        i=0
        for tg in self.lBitsWidgets:
            if tg is not None:
                if tg.GetValue():
                    val|=self.lBits[i][0]
            i+=1
        return val
    def GetValueStr(self):
        return '%08d'%self.GetValue()
    def SetValueStr(self,val):
        try:
            self.SetValue(long(val))
        except:
            self.SetValue(self.iDft)
    def SetNode(self,node,doc):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.bBlock=True
        self.__markModified__(False)
        try:
            sVal=long(doc.getNodeText(node,self.tagName))
        except:
            sVal=self.iDft
        self.SetValue(sVal)
        wx.CallAfter(self.__clearBlock__)
    def GetNode(self,node,doc):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.bBlock=True
        self.__markModified__(False)
        try:
            doc.setNodeText(node,self.tagName,self.GetValueStr())
        except:
            vtLog.vtLngTB(self.GetName())
        wx.CallAfter(self.__clearBlock__)
    def OnTgChanged(self,event):
        if self.bBlock:
            return
        #sVal=self.mskNumValue.GetValue()
        self.__markModified__()
        wx.PostEvent(self,vtInputBitsChanged(self,self.GetValue(),self.GetValueStr()))
