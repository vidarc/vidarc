#----------------------------------------------------------------------------
# Name:         vtInputGrid.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vtInputGrid.py,v 1.6 2008/03/22 14:34:24 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import  wx
import  wx.grid as gridlib
import string,copy
import vidarc.tool.xml.vtXmlDom as vtXmlDom
import vidarc.tool.log.vtLog as vtLog

#---------------------------------------------------------------------------
# defined event for vgpXmlTree item selected
wxEVT_VTINPUT_GRID_INFO_CHANGED=wx.NewEventType()
vEVT_VTINPUT_GRID_INFO_CHANGED=wx.PyEventBinder(wxEVT_VTINPUT_GRID_INFO_CHANGED,1)
def EVT_VTINPUTGRID_INFO_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTINPUT_GRID_INFO_CHANGED,func)
class vtInputGridInfoChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTINPUTGRID_INFO_CHANGED(<widget_name>, self.OnInfoChanged)
    """

    def __init__(self,obj,row,col):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTINPUT_GRID_INFO_CHANGED)
        self.obj=obj
        self.row=row
        self.col=col
    def GetObj(self):
        return self.obj
    def GetRow(self):
        return self.row
    def GetCol(self):
        return self.col

wxEVT_VTINPUT_GRID_INFO_SELECTED=wx.NewEventType()
vEVT_VTINPUT_GRID_INFO_SELECTED=wx.PyEventBinder(wxEVT_VTINPUT_GRID_INFO_SELECTED,1)
def EVT_VTINPUT_GRID_INFO_SELECTED(win,func):
    win.Connect(-1,-1,wxEVT_VTINPUT_GRID_INFO_SELECTED,func)
class vtInputGridInfoSelected(wx.PyEvent):
    """
    Posted Events:
        Project Info SELECTED event
            EVT_VTINPUT_GRID_INFO_SELECTED(<widget_name>, self.OnInfoSELECTED)
    """

    def __init__(self,obj,row,col):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTINPUT_GRID_INFO_SELECTED)
        self.obj=obj
        self.row=row
        self.col=col
    def GetObj(self):
        return self.obj
    def GetRow(self):
        return self.row
    def GetCol(self):
        return self.col

class vtInputGridData(gridlib.PyGridTableBase):
#class vtInputGridData(gridlib.GridTableBase):
    def __init__(self,lstNodes,dataSrc):#funcId,funcGetType,funcGetValue,funcSetValue):
        gridlib.PyGridTableBase.__init__(self)
        #gridlib.GridTableBase.__init__(self)
        self.dataSrc=dataSrc
        #self.funcGetType=funcGetType
        #self.funcGetValue=funcGetValue
        #self.funcSetValue=funcSetValue
        #ids=doc.getIds()
        #self.colLabels = []
        self.dftDatTypes= []
        self.dataTypes = []
        self.data = []
        #self.dataNodes = []
        datItem=None
        if lstNodes is None:
            #self.colLabels = []
            self.dataTypes = [[]]
            self.data=[[]]
            #self.dataNodes=[[]]
            return
        iCount=0
        for lst in lstNodes:
            i=len(lst)
            if i>iCount:
                iCount=i
        self.dftDatItem=[]
        lst=[]
        for i in range(iCount):
            self.dftDatItem.append('')
            #self.colLabels.append('col %2d'%i)
            self.dftDatTypes.append(gridlib.GRID_VALUE_TEXT)
            lst.append('')
        #self.data.append(lst)
        if self.dataSrc.HasRowLabelChangeable():
            self.dftDatItem.append('')
            self.dftDatTypes.append(gridlib.GRID_VALUE_TEXT)
            lst.append('')
        row=0
        if self.dataSrc.HasColLabelChangeable():
            datType=copy.copy(self.dftDatTypes)
            datItem=copy.copy(self.dftDatItem)
            self.dataTypes.append(datType)
            self.data.append(datItem)
            row+=1
        for lst in lstNodes:
            datNodes=[]
            datType=copy.copy(self.dftDatTypes)
            datItem=copy.copy(self.dftDatItem)
            if self.dataSrc.HasRowLabelChangeable():
                iIdx=1
            else:
                iIdx=0
            for node in lst:
                #sTagName=doc.getTagName(node)
                #sId=funcId(node)
                doc,nodeType,sType=self.dataSrc.GetRowColType(row,iIdx)
                sVal=self.dataSrc.GetRowColValue(row,iIdx)
                if 1==0:
                    if funcBase is None:
                        nodeBase=node
                    else:
                        nodeBase=funcBase(node)
                    print nodeBase
                    nodeType=doc.getAttributeTypeNode(nodeBase)
                    print nodeType
                    print
                    sType=doc.getNodeText(nodeType,'type')
                #type=self.__getGridType__(doc,nodeBase,sType)
                type=self.__getGridType__(doc,nodeType,sType)
                #sVal=doc.getAttributeVal(node)
                val=self.__getVal__(sType,sVal)
                datItem[iIdx]=val
                datType[iIdx]=type
                iIdx+=1
                #print sTagName,sId
            self.dataTypes.append(datType)
            self.data.append(datItem)
            #self.dataNodes.append(lst)
            row+=1
    def __getGridType__(self,doc,node,attrTyp):
        if attrTyp=='datetime':
            datTyp=gridlib.GRID_VALUE_DATETIME
        elif attrTyp=='int':
            datTyp=gridlib.GRID_VALUE_NUMBER
            try:
                sMin=doc.getNodeText(node,'min')
            except:
                sMin=''
            try:
                sMax=doc.getNodeText(node,'max')
            except:
                sMax=''
            if len(sMin)>0 or len(sMax)>0:
                s=':'+sMin+','+sMax
                datTyp=datTyp+s
            else:
                pass
        elif attrTyp=='float':
            datTyp=gridlib.GRID_VALUE_FLOAT
            try:
                sDigits=doc.getNodeText(node,'digits')
            except:
                sDigits=''
            try:
                sComma=doc.getNodeText(node,'comma')
            except:
                sComma=''
            if len(sDigits)>0 and len(sComma)>0:
                s=':'+sDigits+','+sComma
                datTyp=datTyp+s
            else:
                pass
        elif attrTyp=='long':
            datTyp=gridlib.GRID_VALUE_LONG
            try:
                sMin=doc.getNodeText(node,'min')
            except:
                sMin=''
            try:
                sMax=doc.getNodeText(node,'max')
            except:
                sMax=''
            if len(sMin)>0 or len(sMax)>0:
                s=':'+sMin+','+sMax
                datTyp=datTyp+s
            else:
                pass
        elif attrTyp=='choiceint':
            datTyp=gridlib.GRID_VALUE_CHOICEINT
        elif attrTyp=='choice':
            datTyp=gridlib.GRID_VALUE_CHOICE
            keys=typeInfos.keys()
            keys.sort()
            choices=[]
            for k in keys:
                if k[:2]=='it':
                    choices.append(typeInfos[k])
            if len(choices)>0:
                datTyp=datTyp+':'+string.join(choices,',')
        elif attrTyp=='string':
            datTyp=gridlib.GRID_VALUE_STRING
        elif attrTyp=='text':
            datTyp=gridlib.GRID_VALUE_TEXT
        else:
            datTyp=gridlib.GRID_VALUE_STRING
        return datTyp
    def __getVal__(self,attrTyp,val):
        if attrTyp=='datetime':
            return val
        elif attrTyp=='int':
            return int(val)
        elif attrTyp=='float':
            return float(val)
        elif attrTyp=='long':
            return long(val)
        elif attrTyp=='choiceint':
            return int(val)
        elif attrTyp=='choice':
            return val
        elif attrTyp=='string':
            return val
        elif attrTyp=='text':
            return val
        else:
            return val
        return val
    def __getAttrIdx__(self,lstAttr,key):
        idx=0
        for attr in lstAttr:
            if attr[0]==key:
                return idx
            idx=idx+1
    def GetNumberRows(self):
        return len(self.data)
    def GetNumberCols(self):
        return len(self.data[0])
    def IsEmptyCell(self, row, col):
        try:
            return not self.data[row][col]
        except IndexError:
            return True

    # Get/Set values in the table.  The Python version of these
    # methods can handle any data-type, (as long as the Editor and
    # Renderer understands the type too,) not just strings as in the
    # C++ version.
    def GetValue(self, row, col):
        try:
            return self.data[row][col]
        except IndexError:
            return ''
    def SetValue(self, row, col, value):
        try:
            if value!='---':
                sType=self.dataTypes[row][col]
                if string.find(sType,gridlib.GRID_VALUE_NUMBER)>=0:
                    try:
                        value=int(value)
                    except:
                        value=0
                elif string.find(sType,gridlib.GRID_VALUE_LONG)>=0:
                    try:
                        value=long(value)
                    except:
                        value=0
                elif string.find(sType,gridlib.GRID_VALUE_FLOAT)>=0:
                    try:
                        value=float(value)
                    except:
                        value=0
            self.data[row][col] = value
        except IndexError:
            # add a new row
            self.data.append([''] * self.GetNumberCols())
            self.SetValue(row, col, value)

            # tell the grid we've added a row
            msg = gridlib.GridTableMessage(self,            # The table
                    gridlib.GRIDTABLE_NOTIFY_ROWS_APPENDED, # what we did to it
                    1                                       # how many
                    )
            self.GetView().ProcessTableMessage(msg)
    #--------------------------------------------------
    # Some optional methods
    # Called when the grid needs to display labels
    def GetColLabelValue(self, col):
        #return gridlib.PyGridTableBase.base_GetColLabelValue(self,col)
        return self.dataSrc.GetColLabelValue(col,0)
    def GetRowLabelValue(self, row):
        return self.dataSrc.GetRowLabelValue(row)
    def SetColLabelValue(self, col,sVal):
        #self.m_table.SetColLabelValue(i,sCol)
        #print col,sVal
        #self.base_SetColLabelValue(col,sVal)
        #gridlib.GridTableBase.base_SetColLabelValue(self,col,sVal)
        gridlib.PyGridTableBase.base_SetColLabelValue(self,col,sVal)
        #self.UpdateAttrCols(col,1)
    def SetChangeableColLabelValue(self,col,sVal):
        #if self.dataSrc.HasRowLabelChangeable():
        #    col+=1
        self.SetValue(0, col, sVal)
    #    pass
        #grid=self.GetView()
        #grid.GetColumnHeading (col,sVal)
    #    gridlib.PyGridTableBase.SetColLabelValue(self,col,sVal)
    #    pass
    
    # Called to determine the kind of editor/renderer to use by
    # default, doesn't necessarily have to be the same type used
    # natively by the editor/renderer if they know how to convert.
    def GetTypeName(self, row, col):
        try:
            return self.dataTypes[row][col]
        except:
            return gridlib.GRID_VALUE_STRING

    # Called to determine how the data can be fetched and stored by the
    # editor and renderer.  This allows you to enforce some type-safety
    # in the grid.
    def CanGetValueAs(self, row, col, typeName):
        colType = self.dataTypes[row][col].split(':')[0]
        if typeName == colType:
            return True
        else:
            return False

    def CanSetValueAs(self, row, col, typeName):
        return self.CanGetValueAs(row, col, typeName)

    def GetDataNodes(self):
        return self.dataSrc.GetDataNodes()
    def GetDataTypes(self):
        return self.dataTypes
    


#---------------------------------------------------------------------------

class gridImageRenderer(gridlib.PyGridCellRenderer):
    def __init__(self):
        """
        Image Renderer Test.  This just places an image in a cell
        based on the row index.  There are N choices and the
        choice is made by  choice[row%N]
        """
        gridlib.PyGridCellRenderer.__init__(self)
        #self.imgDict = {}
        #for e in ELEMENTS:
        #    try:
        #        imgName=e[1]['__img']
        #        f = getattr(images_s88, 'get%sBitmap' % imgName)
        #        img = f()
        #        self.imgDict[e[0]]=img
        #    except Exception,list:
                #self.SetStatusText('open fault. (%s)'%list, 1)
        #        pass
        #imgName='Sum'
        #f = getattr(images_s88, 'get%sBitmap' % imgName)
        #img = f()
        #self.imgDict['Sum']=img
        

        self.colSize = None
        self.rowSize = None

    def Draw(self, grid, attr, dc, rect, row, col, isSelected):
        #choice = self.table.GetRawValue(row, col)
        choice = grid.GetCellValue(row,col)#self.table.GetRawValue(row, col)
        #print row,col,choice,self.imgDict
        try:
            if choice=='Sum':
                imgName=choice
            else:
                imgName='None'
                for e in ELEMENTS:
                    if e[0]==choice:
                        imgName=e[1]['__img']
                        break
            f = getattr(images_s88, 'get%sBitmap' % imgName)
            img = f()
        except:
            #f = getattr(images_s88, 'get%sBitmap' % 'None')
            #img = f()
            img=wx.EmptyBitmap(16,16)
            
        #self.imgDict['Sum']=img
        
        #bmp = self.imgDict[choice]
        image = wx.MemoryDC()
        image.SelectObject(img)

        # clear the background
        dc.SetBackgroundMode(wx.SOLID)

        if isSelected:
            dc.SetBrush(wx.Brush(wx.BLUE, wx.SOLID))
            dc.SetPen(wx.Pen(wx.BLUE, 1, wx.SOLID))
        else:
            dc.SetBrush(wx.Brush(wx.LIGHT_GREY, wx.SOLID))
            dc.SetPen(wx.Pen(wx.LIGHT_GREY, 1, wx.SOLID))
        dc.DrawRectangleRect(rect)


        # copy the image but only to the size of the grid cell
        width, height = img.GetWidth(), img.GetHeight()

        if width > rect.width-2:
            width = rect.width-2

        if height > rect.height-2:
            height = rect.height-2

        dc.Blit(rect.x+1, rect.y+1, width, height,
                image,
                0, 0, wx.COPY, True)


class vtInputGrid(gridlib.Grid):
    EDIT_DIRECTION_LEFT_RIGHT = 0
    EDIT_DIRECTION_TOP_DOWN = 1
    def __init__(self, parent, id, pos, size,name='vtInputGridSmall'):
        gridlib.Grid.__init__(self, parent, id,pos,size,name=name)
        self.table=None
        self.emptyEditor=None
        self.emptyRenderer=None
        self.imgRenderer=None
        self.lstIDNodes=None
        #table = gridAttrData()

        # The second parameter means that the grid is to take ownership of the
        # table and will destroy it when done.  Otherwise you would need to keep
        # a reference to it and call it's Destroy method later.
        #sself.SetTable(table, True)
        self.lstColumnSize=None
        self.lstColumnAlign=None
        self.SetColLabelSize(20)
        self.SetRowLabelSize(50)
        self.SetMargins(0,0)
        self.AutoSizeColumns(True)
        self.AutoSizeRows(True)
        self.tooltip=wx.ToolTip('dfg')
        self.SetToolTip(self.tooltip)
        #self.tooltip.Enable(True)
        #self.tooltip.SetDelay(500)

        gridlib.EVT_GRID_CELL_LEFT_CLICK(self, self.OnLeftClick)
        gridlib.EVT_GRID_CELL_LEFT_DCLICK(self, self.OnLeftDClick)
        gridlib.EVT_GRID_CELL_CHANGE(self, self.OnCellChange)
        gridlib.EVT_GRID_SELECT_CELL(self, self.OnCellSelect)
        gridlib.EVT_GRID_EDITOR_SHOWN(self, self.OnEditorShown)
        self.Bind(wx.EVT_KEY_DOWN, self.OnKeyDown)
        
        self.editDirection=self.EDIT_DIRECTION_LEFT_RIGHT
        #self.editDirection=self.EDIT_DIRECTION_TOP_DOWN
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
    def SetEditDirection(self,type):
        self.editDirection=type
    def OnKeyDown(self, evt):
        vtLog.CallStack(evt.KeyCode())
        if evt.KeyCode() != wx.WXK_RETURN:
            evt.Skip()
            return
        
        if evt.ControlDown():   # the edit control needs this key
            evt.Skip()
            return
        
        self.DisableCellEditControl()
        if self.editDirection==self.EDIT_DIRECTION_LEFT_RIGHT:
            def moveRight():
                success = self.MoveCursorRight(evt.ShiftDown())
                if not success:
                    newRow = self.GetGridCursorRow() + 1
                    
                    if newRow < self.GetTable().GetNumberRows():
                        
                        self.SetGridCursor(newRow, 0)
                        #self.MakeCellVisible(newRow, 0)
                        
                    else:
                        # this would be a good place to add a new row if your app
                        # needs to do that
                        return True
                return not self.IsReadOnly(self.GetGridCursorRow(),self.GetGridCursorCol())
            while moveRight()==False:
                pass
        elif self.editDirection==self.EDIT_DIRECTION_TOP_DOWN:
            def moveDown():
                success = self.MoveCursorDown(evt.ShiftDown())
                
                if not success:
                    newCol = self.GetGridCursorCol() + 1
                    
                    if newCol < self.GetTable().GetNumberCols():
                        self.SetGridCursor(0,newCol)
                        #self.MakeCellVisible(0,newCol)
                    else:
                        # this would be a good place to add a new row if your app
                        # needs to do that
                        return True
                return not self.IsReadOnly(self.GetGridCursorRow(),self.GetGridCursorCol())
            while moveDown()==False:
                pass
        #print self.GetGridCursorRow(),self.GetGridCursorCol()
        #evt.Skip()

    # I do this because I don't like the default behaviour of not starting the
    # cell editor on double clicks, but only a second click.
    def OnLeftClick(self, evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.CanEnableCellControl():
            self.EnableCellEditControl()
        #self.tooltip.Enable(True)
        #self.tooltip.SetDelay(500)
        self.tooltip.SetTip('sdfd')
        evt.Skip()
    def OnLeftDClick(self, evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.CanEnableCellControl():
            self.EnableCellEditControl()
        evt.Skip()
    def OnEditorShown(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        vtLog.CallStack('')
        evt.Skip()
    def __calcSum__(self,col):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        dataNodes=self.table.GetDataNodes()
        if len(dataNodes)>0:
            def init(a):
                return 0
            sumLst=map(init,range(0,len(dataNodes[0])+2))
        else:
            return
        dataTypes=self.table.GetDataTypes()
        row=0
        for dataLstNode in dataNodes[:-1]:
            node=dataLstNode[col]
            try:
                #v=int(gridlib.Grid.GetCellValue(self,row,col))
                #sumLst[col]=sumLst[col]+v
                if string.find(dataTypes[col],gridlib.GRID_VALUE_NUMBER)>=0:
                    v=int(gridlib.Grid.GetCellValue(self,row,col))
                    sumLst[col]=sumLst[col]+v
                elif string.find(dataTypes[col],gridlib.GRID_VALUE_LONG)>=0:
                    v=long(gridlib.Grid.GetCellValue(self,row,col))
                    sumLst[col]=sumLst[col]+v
                elif string.find(dataTypes[col],gridlib.GRID_VALUE_FLOAT)>=0:
                    v=float(gridlib.Grid.GetCellValue(self,row,col))
                    sumLst[col]=sumLst[col]+v
                else:
                    pass
            except:
                pass
            row=row+1
        row=len(dataNodes)-1
        if sumLst[col] is not None:
            try:
                gridlib.Grid.SetCellValue(self,row,col,sumLst[col].__str__())
            except:
                pass

    def OnCellChange(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        vtLog.CallStack('')
        #self.__calcSum__(evt.GetCol())
        font = self.GetFont()
        font.SetWeight(wx.BOLD)
        gridlib.Grid.SetCellFont(self,evt.GetRow(),evt.GetCol(),font)
        
        wx.PostEvent(self,vtInputGridInfoChanged(self,evt.GetRow(),evt.GetCol()))
        evt.Skip()
    def OnCellSelect(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #vtLog.CallStack('')
        self.BeginBatch()
        try:
            iRow,iCol=evt.GetRow(),evt.GetCol()
            wx.PostEvent(self,vtInputGridInfoSelected(self,evt.GetRow(),evt.GetCol()))
            for i in range(self.GetTable().GetNumberCols()-1):
                sCol=self.dataSrc.GetColLabelValue(i,iRow)
                #self.m_table.SetColLabelValue(i,sCol)
                self.table.SetColLabelValue(i,sCol)
                #wx.CallAfter(self.SetColLabelValue,i,sCol)
                if sCol is not None:
                    #self.table.SetChangeableColLabelValue(i,sCol)
                    self.SetCellValue(0,i,sCol)
        except:
            vtLog.vtLngTB(self.GetName())
        self.EndBatch()
        self.ForceRefresh()
        msg = gridlib.GridTableMessage(self.table, gridlib.GRIDTABLE_REQUEST_VIEW_GET_VALUES)
        #msg = gridlib.GridTableMessage(self.table, gridlib.GRIDTABLE_NOTIFY_COLS_INSERTED)
        self.ProcessTableMessage(msg)

        evt.Skip()
    def SetColumnSize(self,lst):
        self.lstColumnSize=lst
    def SetColumnAlign(self,lst):
        self.lstColumnAlign=lst
    def SetUp(self,lstNodes,dataSrc):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #self.doc=doc
        #if lstIDNodes is not None:
        #    self.lstIDNodes=lstIDNodes
        #else:
        #    lstIDNodes=self.lstIDNodes
        if self.CanEnableCellControl():
            self.DisableCellEditControl()
        self.dataSrc=dataSrc
        #self.table = gridAttrData(None,self.lstFilter)
        #self.SetTable(self.table, True)
        gridlib.Grid.ClearGrid(self)
        self.table = vtInputGridData(lstNodes,dataSrc)
        #self.SetTable(self.table, True)
        self.SetTable(self.table, False)
        self.SetReadOnly()
        self.ForceRefresh()
    def Get(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        vtLog.CallStack('')
        #dataNodes=self.table.GetDataNodes()
        font = self.GetFont()
        for row in range(self.table.GetNumberRows()):
            for col in range(self.table.GetNumberCols()):
                if not self.table.IsEmptyCell(row,col):
                    #node=dataNodes[row][col]
                    #if node is not None:
                    val=self.GetCellValue(row,col)
                    try:
                        s=val.__str__()
                    except:
                        s=val
                    #self.dataSrc.SetVal(node,s)
                    self.dataSrc.SetRowColVal(row,col,s)
                    gridlib.Grid.SetCellFont(self,row,col,font)
        self.ForceRefresh()
        return 1
    def Set(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        vtLog.CallStack('')
        if self.CanEnableCellControl():
            self.DisableCellEditControl()
        #dataNodes=self.table.GetDataNodes()
        font = self.GetFont()
        font.SetWeight(wx.NORMAL)
        for row in range(self.table.GetNumberRows()):
            for col in range(self.table.GetNumberCols()):
                if node is not None:
                    #sVal=self.doc.getText(node)
                    #self.table.SetValue(row,col,sVal)
                    sVal=self.table.SetValue(row,col)
                    #print row,col,sVal
                    gridlib.Grid.SetCellFont(self,row,col,font)
        if len(dataNodes)>0:
            col=0
            for dataLstNode in dataNodes[0]:
                self.__calcSum__(col)
                col=col+1
        self.ForceRefresh()
        return 1
    def SetReadOnly(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #dataNodes=self.table.GetDataNodes()
        #dataTypes=self.table.GetDataTypes()
        #iRowCount=len(dataNodes)
        iRowCount=self.table.GetNumberRows()
        iColCount=self.table.GetNumberCols()
        bRowLabel=self.dataSrc.HasRowLabelChangeable()
        #if len(dataNodes)<=0:
        #    return
        if self.lstColumnSize:
            try:
                for i in range(len(self.lstColumnSize)):
                    gridlib.Grid.SetColSize(self,i,self.lstColumnSize[i])
            except:
                vtLog.vtLngTB(self.GetName())
                vtLog.vtLngCurWX(vtLog.ERROR,'i=%d;iLen=%d'%(i,len(self.lstColumnSize)),self)
        for row in range(iRowCount):
            for col in range(iColCount):
                if self.dataSrc.GetDataNode(row,col) is None:
                    emptyEditor=gridlib.GridCellTextEditor()
                    emptyRenderer=gridlib.GridCellStringRenderer()
                    gridlib.Grid.SetCellEditor(self,row,col,emptyEditor)
                    gridlib.Grid.SetCellRenderer(self,row,col,emptyRenderer)
                    #if col>2:
                    #    gridlib.Grid.SetCellValue(self,row,col,'---')
                    gridlib.Grid.SetReadOnly(self,row,col)
                    gridlib.Grid.SetCellBackgroundColour(self,row,col,wx.LIGHT_GREY)
                    gridlib.Grid.SetCellAlignment(self,row,col,wx.ALIGN_CENTRE,wx.ALIGN_CENTRE)
                if bRowLabel:
                    if col==0 and row>0:
                        sVal=self.dataSrc.GetRowLabelValue(row)
                        self.SetCellValue(row,col,sVal)
        return 1
    def SetReadOnlyCols(self,cols):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        for col in cols:
            for row in range(self.GetNumberRows()):
                emptyEditor=gridlib.GridCellTextEditor()
                emptyRenderer=gridlib.GridCellStringRenderer()
                gridlib.Grid.SetCellEditor(self,row,col,emptyEditor)
                gridlib.Grid.SetCellRenderer(self,row,col,emptyRenderer)
                #if col>2:
                #    gridlib.Grid.SetCellValue(self,row,col,'---')
                gridlib.Grid.SetReadOnly(self,row,col)
                gridlib.Grid.SetCellBackgroundColour(self,row,col,wx.LIGHT_GREY)
                gridlib.Grid.SetCellAlignment(self,row,col,wx.ALIGN_CENTRE,wx.ALIGN_CENTRE)


#---------------------------------------------------------------------------

class TestFrame(wx.Frame):
    def __init__(self, parent, log):

        wx.Frame.__init__(
            self, parent, -1, "Custom Table, data driven Grid  Demo", size=(640,480)
            )

        p = wx.Panel(self, -1, style=0)
        grid = CustTableGrid(p, log)
        b = wx.Button(p, -1, "Another Control...")
        b.SetDefault()
        self.Bind(wx.EVT_BUTTON, self.OnButton, b)
        b.Bind(wx.EVT_SET_FOCUS, self.OnButtonFocus)
        bs = wx.BoxSizer(wx.VERTICAL)
        bs.Add(grid, 1, wx.GROW|wx.ALL, 5)
        bs.Add(b)
        p.SetSizer(bs)

    def OnButton(self, evt):
        print "button selected"

    def OnButtonFocus(self, evt):
        print "button focus"


#---------------------------------------------------------------------------

if __name__ == '__main__':
    import sys
    app = wx.PySimpleApp()
    frame = TestFrame(None, sys.stdout)
    frame.Show(True)
    app.MainLoop()


#---------------------------------------------------------------------------
