#----------------------------------------------------------------------------
# Name:         vtInputBitsList.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060420
# CVS-ID:       $Id: vtInputBitsList.py,v 1.7 2008/03/22 14:34:24 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt

from vidarc.tool.xml.vtXmlDomConsumer import *
from vidarc.tool.xml.vtXmlDomConsumerLang import *

import sys

from vidarc.tool.input.vtInputBits import *

wxEVT_VTINPUT_BITS_LIST_CHANGED=wx.NewEventType()
vEVT_VTINPUT_BITS_LIST_CHANGED=wx.PyEventBinder(wxEVT_VTINPUT_BITS_LIST_CHANGED,1)
def EVT_VTINPUT_BITS_LIST_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTINPUT_BITS_LIST_CHANGED,func)
class vtInputBitsListChanged(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTINPUT_BITS_LIST_CHANGED(<widget_name>, self.OnChanged)
    """

    def __init__(self,obj,val,s):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTINPUT_BITS_LIST_CHANGED)
        self.val=val
        self.sVal=s
    def GetValue(self):
        return self.val
    def GetValueStr(self):
        return self.sVal

class vtInputBitsList(wx.Panel,vtXmlDomConsumer,vtXmlDomConsumerLang):
    VERBOSE=0
    def __init__(self,*_args,**_kwargs):
        vtXmlDomConsumer.__init__(self)
        vtXmlDomConsumerLang.__init__(self)
        try:
            sz=_kwargs['size']
        except:
            sz=wx.Size(260,150)
            _kwargs['size']=sz
        try:
            self.iDft=_kwargs['default']
            del _kwargs['default']
        except:
            self.iDft=0
        try:
            self.lBits=_kwargs['bits']
            del _kwargs['bits']
        except:
            self.lBits=[]
        try:
            self.lChoice=_kwargs['choice']
            del _kwargs['choice']
        except:
            self.lChoice=[]
        try:
            cols=_kwargs['cols']
            del _kwargs['cols']
        except:
            cols=['name']
        try:
            arrange=_kwargs['arrange']
            del _kwargs['arrange']
        except:
            arrange='bottom'
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        self.SetAutoLayout(True)
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        
        fgs = wx.FlexGridSizer(cols=2, hgap=0, rows=2, vgap=0)
        def addLst():
            id=wx.NewId()
            self.lstBits = wx.ListCtrl(id=id, name=u'lstBits',
                  parent=self, pos=wx.Point(0, 0), size=wx.Size(50, 60),
                  style=wx.LC_REPORT)
            self.lstBits.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstBitsListColClick,id=id)
            self.lstBits.Bind(wx.EVT_LIST_ITEM_DESELECTED,self.OnLstBitsListItemDeselected, id=id)
            self.lstBits.Bind(wx.EVT_LIST_ITEM_SELECTED,self.OnLstBitsListItemSelected, id=id)
            
            if arrange=='bottom':
                fgs.AddWindow(self.lstBits, 1, border=0, flag=wx.EXPAND)
            else:
                fgs.AddWindow(self.lstBits, 1, border=10, flag=wx.EXPAND|wx.BOTTOM)
                
            id=wx.NewId()
            self.cbDel = wx.lib.buttons.GenBitmapButton(ID=id,
                  bitmap=vtArt.getBitmap(vtArt.Del), name=u'cbDel', parent=self,
                  pos=wx.Point(121, 0), size=wx.Size(31, 30), style=0)
            self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton, id=id)
            fgs.AddWindow(self.cbDel, 0, border=4, flag=wx.LEFT)
        
        def addCmd():
            #bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
            bxs = wx.BoxSizer(orient=wx.VERTICAL)
            self.chc = wx.Choice(choices=[], id=wx.NewId(), name=u'chc',
                  parent=self, pos=wx.Point(0, 94), size=wx.Size(22, 21), style=0)
            self.viBits = vtInputBits(bits=self.lBits, default=0,
                  id=wx.NewId(), name='vtInputBits1', parent=self,
                  pos=wx.Point(26, 94), size=wx.Size(90, 30), style=0)
            bxs.AddWindow(self.chc, 1, border=0, flag=wx.EXPAND)
            #bxs.AddWindow(self.chc, 0, border=0, flag=0)
            bxs.AddWindow(self.viBits, 2, border=4,flag=wx.TOP)# | wx.EXPAND)
            #bxs.Layout()
            
            id=wx.NewId()
            self.cbSet = wx.lib.buttons.GenBitmapButton(ID=id,
                  bitmap=vtArt.getBitmap(vtArt.Edit), name=u'cbSet', parent=self,
                  pos=wx.Point(121, 94), size=wx.Size(31, 30), style=0)
            self.cbSet.Bind(wx.EVT_BUTTON, self.OnCbSetButton, id=id)
            if arrange=='bottom':
                fgs.AddSizer(bxs, 1, border=4, flag=wx.TOP|wx.EXPAND)
                #fgs.AddSizer(bxs, 1, border=4, flag=wx.TOP)#|wx.EXPAND)
                fgs.AddWindow(self.cbSet, 0, border=4, flag=wx.TOP | wx.LEFT)
            else:
                fgs.AddSizer(bxs, 1, border=4, flag=wx.TOP|wx.EXPAND)
                fgs.AddWindow(self.cbSet, 0, border=4, flag=wx.TOP | wx.LEFT)
                
        if arrange=='top':
            addCmd()
            addLst()
            fgs.AddGrowableRow(1)
            fgs.AddGrowableCol(0)
        elif arrange=='bottom':
            addLst()
            addCmd()
            fgs.AddGrowableRow(0)
            fgs.AddGrowableCol(0)
        else:
            addLst()
            addCmd()
            fgs.AddGrowableRow(0)
            fgs.AddGrowableCol(0)
            
        #self.imgLst=wx.ImageList(16,16)
        #self.dImg={}
        
        try:
            self.lstBits.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
                                    heading=cols[0], width=140)
            i=1
            for iBit,bmp,tool in self.lBits:
                try:
                    self.lstBits.InsertColumn(col=i, format=wx.LIST_FORMAT_CENTRE,
                                    heading=cols[i], width=24)
                    #self.dImg[iBit]=self.imgLst.Add(bmp)
                except:
                    pass
                i+=1
        except:
            pass
        #self.lstBits.SetImageList(self.imgLst,wx.IMAGE_LIST_NORMAL)
        #self.lstBits.SetImageList(self.imgLst,wx.IMAGE_LIST_SMALL)
        self.SetSizer(fgs)
        fgs.Layout()
        self.__showChoices__()
        
        self.selIdx=-1
        self.lVal=[]
        self.bBlock=True
        wx.CallAfter(self.__clearBlock__)
        self.tagName='bits'
        self.bMod=False
        self.bEnableMark=True
    def OnCbDelButton(self,evt):
        evt.Skip()
        if self.selIdx<0:
            return
        self.lVal=self.lVal[:self.selIdx]+self.lVal[self.selIdx+1:]
        self.__showValues__()
    def OnCbSetButton(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        evt.Skip()
        i=0
        try:
            i=self.chc.GetSelection()
            idx=self.chc.GetClientData(i)
            tup=[self.lChoice[idx][0],self.viBits.GetValue()]
            i=0
            for sName,num in self.lVal:
                if sName==tup[0]:
                    self.lVal[i]=tup
                    self.__showValues__()
                    return
                i+=1
            self.lVal.append(tup)
            self.__showValues__()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnLstBitsListColClick(self,evt):
        evt.Skip()
        self.selIdx=-1
        self.__showValue__()
    def OnLstBitsListItemDeselected(self,evt):
        evt.Skip()
        self.selIdx=-1
        self.__showValue__()
    def OnLstBitsListItemSelected(self,evt):
        evt.Skip()
        self.selIdx=evt.GetIndex()
        self.__showValue__()
    def __showValue__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.selIdx<0:
            self.viBits.Clear()
            self.chc.SetSelection(0)
        else:
            try:
                sTrans=self.dChcTrans[self.lVal[self.selIdx][0]]
                self.chc.SetStringSelection(sTrans)
                self.viBits.SetValue(self.lVal[self.selIdx][1])
            except:
                self.viBits.Clear()
    def __showValues__(self,flag=True):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.selIdx=-1
        self.lstBits.DeleteAllItems()
        if flag:
            self.__markModified__(True)
        i=0
        l=[]
        for s,num in self.lVal:
            try:
                sTrans=self.dChcTrans[s]
                l.append([sTrans,num])
            except:
                self.__markModified__(True)
                continue
        l.sort()
        for sTrans,num in l:
            idx=self.lstBits.InsertStringItem(sys.maxint,sTrans)
            j=1
            for tup in self.lBits:
                if (tup[0]&num)!=0:
                    self.lstBits.SetStringItem(idx,j,'X',-1)
                j+=1
            i+=1
    def __showChoices__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.chc.Clear()
        self.dChcTrans={}
        lChcTrans=[]
        i=0
        for sTag,sTrans in self.lChoice:
            lChcTrans.append((sTrans,i))
            self.dChcTrans[sTag]=sTrans
            i+=1
        lChcTrans.sort()
        i=0
        iSel=0
        for sTrans,idx in lChcTrans:
            if idx==0:
                iSel=i
            self.chc.Append(sTrans,idx)
            i+=1
        try:
            self.chc.SetSelection(iSel)
        except:
            pass
    def SetEnableMark(self,flag):
        self.bEnableMark=flag
    def SetTagName(self,tagName):
        self.tagName=tagName
    def SetModified(self,flag):
        self.__markModified__(flag)
    def IsModified(self):
        return self.bMod
    def __markModified__(self,flag=True):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.bEnableMark==False:
            return
        if flag:
            f=self.lstBits.GetFont()
            f.SetWeight(wx.FONTWEIGHT_BOLD)
            self.lstBits.SetFont(f)
        else:
            f=self.lstBits.GetFont()
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
            self.lstBits.SetFont(f)
        self.lstBits.Refresh()
        self.bMod=flag
    def __clearBlock__(self):
        self.bBlock=False
    def ClearInt(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.VERBOSE:
            vtLog.CallStack('')
        self.bBlock=True
        self.__markModified__(False)
        self.lVal=[]
        self.selIdx=-1
        self.lstBits.DeleteAllItems()
        self.viBits.Clear()
        wx.CallAfter(self.__clearBlock__)
    def Clear(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.VERBOSE:
            vtLog.CallStack('')
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'Clear',
                        self)
        self.ClearLang()
        self.ClearInt()
        self.node=None
        self.__markModified__(False)
    def ClearLang(self):
        if self.VERBOSE:
            vtLog.CallStack('')
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'ClearLang',
                        self)
        self.bBlock=True
        #self.txtVal.SetValue('')
        wx.CallAfter(self.__clearBlock__)
    def UpdateLang(self):
        try:
            self.__showChoices__()
            self.__showValues__()
        except:
            vtLog.vtLngTB(self.GetName())

    def Enable(self,state):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.lstBits.Enable(state)
        self.viBits.Enable(state)
        self.cbSet.Enable(state)
        self.cbDel.Enable(state)
        self.chc.Enable(state)
    def SetValue(self,lst):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.Clear()
        self.bBlock=True
        self.__markModified__(False)
        i=0
        for s,num in lst:
            try:
                sTrans=self.dChcTrans[s]
            except:
                self.__markModified__(True)
                continue
            self.lVal.append([s,num])
            idx=self.lstBits.InsertStringItem(sys.maxint,sTrans)
            j=1
            for tup in self.lBits:
                if (tup[0]&num)!=0:
                    self.lstBits.SetStringItem(idx,j,'X',-1)
                j+=1
            i+=1
        wx.CallAfter(self.__clearBlock__)
    def GetValue(self):
        return self.lVal
    def GetValueStr(self):
        def conv(a):
            return '%s,0x%08x'%(a[0],a[1])
        return ';'.join(map(conv,self.lVal))
    def SetValueStr(self,val):
        lVal=[]
        strs=val.split(';')
        for s in strs:
            ss=s.split(',')
            lVal.append((ss[0],long(ss[1],16)))
        self.SetValue(lVal)
    def SetDoc(self,doc,bNet=False):
        vtXmlDomConsumer.SetDoc(self,doc)
        vtXmlDomConsumerLang.SetDoc(self,doc)
    def SetNode(self,node):
        self.Clear()
        vtXmlDomConsumer.SetNode(self,node)
        self.bBlock=True
        self.__markModified__(False)
        try:
            if self.VERBOSE:
                vtLog.CallStack('')
                print node
                print self.tagName
            for c in self.doc.getChilds(node,self.tagName):
                s=self.doc.getAttribute(c,'choice')
                sNum=self.doc.getText(c)
                try:
                    sTrans=self.dChcTrans[s]
                    self.lVal.append([s,long(sNum,16)])
                except:
                    self.__markModified__(True)
        except:
            vtLog.vtLngTB(self.GetName())
        self.__showValues__(False)
        wx.CallAfter(self.__clearBlock__)
    def GetNode(self,node=None):
        if self.VERBOSE:
            vtLog.CallStack('')
            print node
        if node is None:
            node=self.node
        if node is None:
            return
        self.bBlock=True
        self.__markModified__(False)
        try:
            for c in self.doc.getChilds(node,self.tagName):
                self.doc.deleteNode(c,node)
            self.lVal.sort()
            for s,num in self.lVal:
                self.doc.createSubNodeTextAttr(node,self.tagName,'0x%08x'%num,'choice',s)
            self.doc.AlignNode(node)
        except:
            vtLog.vtLngTB(self.GetName())
        wx.CallAfter(self.__clearBlock__)
    def OnTgChanged(self,event):
        if self.bBlock:
            return
        #sVal=self.mskNumValue.GetValue()
        self.__markModified__()
        wx.PostEvent(self,vtInputBitsChanged(self,self.GetValue(),self.GetValueStr()))
