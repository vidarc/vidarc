#Boa:Dialog:vtInputCalcEditDialog
#----------------------------------------------------------------------------
# Name:         vtInputCalcEditDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060508
# CVS-ID:       $Id: vtInputCalcEditDialog.py,v 1.6 2008/03/22 14:34:24 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import sys,types
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase

def create(parent):
    return vtInputCalcEditDialog(parent)

[wxID_VTINPUTCALCEDITDIALOG, wxID_VTINPUTCALCEDITDIALOGCBADD, 
 wxID_VTINPUTCALCEDITDIALOGCBAPPLY, wxID_VTINPUTCALCEDITDIALOGCBCALC, 
 wxID_VTINPUTCALCEDITDIALOGCBCANCEL, wxID_VTINPUTCALCEDITDIALOGCBDEL, 
 wxID_VTINPUTCALCEDITDIALOGCBDN, wxID_VTINPUTCALCEDITDIALOGCBUP, 
 wxID_VTINPUTCALCEDITDIALOGCHCQUICK, wxID_VTINPUTCALCEDITDIALOGLBLFORMEL, 
 wxID_VTINPUTCALCEDITDIALOGLBLRES, wxID_VTINPUTCALCEDITDIALOGLSTFORMEL, 
 wxID_VTINPUTCALCEDITDIALOGTXTFORMEL, wxID_VTINPUTCALCEDITDIALOGTXTRES, 
] = [wx.NewId() for _init_ctrls in range(14)]

class vtInputCalcEditDialog(wx.Dialog):
    VERBOSE=1
    QUICK=[ ('original value','$0$'),
            ('first reslt','$1$'),
            ('slice','[:]'),
        ]
    def _init_coll_bxsEdit_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.chcQuick, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtFormel, 2, border=4, flag=wx.EXPAND | wx.LEFT)

    def _init_coll_bxsRes_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblRes, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtRes, 2, border=4, flag=wx.EXPAND | wx.LEFT)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsEdit, 0, border=4,
              flag=wx.TOP | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbAdd, 0, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.TOP)
        parent.AddWindow(self.lblFormel, 0, border=4, flag=wx.EXPAND | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.lstFormel, 0, border=4,
              flag=wx.TOP | wx.EXPAND | wx.LEFT)
        parent.AddSizer(self.bxsBtFormel, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsRes, 1, border=4,
              flag=wx.TOP | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbCalc, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=wx.ALIGN_CENTER)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.TOP | wx.BOTTOM | wx.ALIGN_CENTER)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(16, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_bxsBtFormel_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbDel, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbUp, 0, border=0, flag=0)
        parent.AddWindow(self.cbDn, 0, border=4, flag=wx.TOP)

    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_lstFormel_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT, heading=_(u'Nr'),
              width=40)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'Formel'), width=160)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=2, hgap=0, rows=6, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBtFormel = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsEdit = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsRes = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsBtFormel_Items(self.bxsBtFormel)
        self._init_coll_bxsEdit_Items(self.bxsEdit)
        self._init_coll_bxsRes_Items(self.bxsRes)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTINPUTCALCEDITDIALOG,
              name=u'vtInputCalcEditDialog', parent=prnt, pos=wx.Point(359,
              182), size=wx.Size(301, 258),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE,
              title=u'vtInput Calc Edit Dialog')
        self.SetClientSize(wx.Size(293, 231))

        self.lblFormel = wx.StaticText(id=wxID_VTINPUTCALCEDITDIALOGLBLFORMEL,
              label=_(u'Formel'), name=u'lblFormel', parent=self,
              pos=wx.Point(4, 34), size=wx.Size(250, 13), style=0)
        self.lblFormel.SetMinSize(wx.Size(-1, -1))

        self.chcQuick = wx.Choice(choices=[],
              id=wxID_VTINPUTCALCEDITDIALOGCHCQUICK, name=u'chcQuick',
              parent=self, pos=wx.Point(4, 4), size=wx.Size(83, 21), style=0)
        self.chcQuick.SetMinSize(wx.Size(-1, -1))
        self.chcQuick.Bind(wx.EVT_CHOICE, self.OnChcQuickChoice,
              id=wxID_VTINPUTCALCEDITDIALOGCHCQUICK)

        self.txtFormel = wx.TextCtrl(id=wxID_VTINPUTCALCEDITDIALOGTXTFORMEL,
              name=u'txtFormel', parent=self, pos=wx.Point(91, 4),
              size=wx.Size(162, 30), style=0, value=u'')
        self.txtFormel.SetMinSize(wx.Size(-1, -1))

        self.cbAdd = wx.lib.buttons.GenBitmapButton(ID=wxID_VTINPUTCALCEDITDIALOGCBADD,
              bitmap=vtArt.getBitmap(vtArt.Add), name=u'cbAdd', parent=self,
              pos=wx.Point(258, 4), size=wx.Size(31, 30), style=0)
        self.cbAdd.Bind(wx.EVT_BUTTON, self.OnCbAddButton,
              id=wxID_VTINPUTCALCEDITDIALOGCBADD)

        self.lstFormel = wx.ListCtrl(id=wxID_VTINPUTCALCEDITDIALOGLSTFORMEL,
              name=u'lstFormel', parent=self, pos=wx.Point(4, 51),
              size=wx.Size(250, 102), style=wx.LC_REPORT)
        self.lstFormel.SetMinSize(wx.Size(-1, -1))
        self._init_coll_lstFormel_Columns(self.lstFormel)
        self.lstFormel.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstFormelListColClick,
              id=wxID_VTINPUTCALCEDITDIALOGLSTFORMEL)
        self.lstFormel.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstFormelListItemDeselected,
              id=wxID_VTINPUTCALCEDITDIALOGLSTFORMEL)
        self.lstFormel.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstFormelListItemSelected,
              id=wxID_VTINPUTCALCEDITDIALOGLSTFORMEL)

        self.cbDel = wx.lib.buttons.GenBitmapButton(ID=wxID_VTINPUTCALCEDITDIALOGCBDEL,
              bitmap=vtArt.getBitmap(vtArt.Del), name=u'cbDel', parent=self,
              pos=wx.Point(258, 51), size=wx.Size(31, 30), style=0)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VTINPUTCALCEDITDIALOGCBDEL)

        self.cbUp = wx.lib.buttons.GenBitmapButton(ID=wxID_VTINPUTCALCEDITDIALOGCBUP,
              bitmap=vtArt.getBitmap(vtArt.Up), name=u'cbUp', parent=self,
              pos=wx.Point(258, 89), size=wx.Size(31, 30), style=0)
        self.cbUp.Bind(wx.EVT_BUTTON, self.OnCbUpButton,
              id=wxID_VTINPUTCALCEDITDIALOGCBUP)

        self.cbDn = wx.lib.buttons.GenBitmapButton(ID=wxID_VTINPUTCALCEDITDIALOGCBDN,
              bitmap=vtArt.getBitmap(vtArt.Down), name=u'cbDn', parent=self,
              pos=wx.Point(258, 123), size=wx.Size(31, 30), style=0)
        self.cbDn.Bind(wx.EVT_BUTTON, self.OnCbDnButton,
              id=wxID_VTINPUTCALCEDITDIALOGCBDN)

        self.lblRes = wx.StaticText(id=wxID_VTINPUTCALCEDITDIALOGLBLRES,
              label=u'Result', name=u'lblRes', parent=self, pos=wx.Point(4,
              157), size=wx.Size(83, 30), style=wx.ALIGN_RIGHT)
        self.lblRes.SetMinSize(wx.Size(-1, -1))

        self.txtRes = wx.TextCtrl(id=wxID_VTINPUTCALCEDITDIALOGTXTRES,
              name=u'txtRes', parent=self, pos=wx.Point(91, 157),
              size=wx.Size(162, 30), style=0, value=u'')
        self.txtRes.SetMinSize(wx.Size(-1, -1))

        self.cbCalc = wx.lib.buttons.GenBitmapButton(ID=wxID_VTINPUTCALCEDITDIALOGCBCALC,
              bitmap=vtArt.getBitmap(vtArt.Calc), name=u'cbCalc', parent=self,
              pos=wx.Point(258, 157), size=wx.Size(31, 30), style=0)
        self.cbCalc.Bind(wx.EVT_BUTTON, self.OnCbCalcButton,
              id=wxID_VTINPUTCALCEDITDIALOGCBCALC)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTINPUTCALCEDITDIALOGCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'),
              name=u'cbApply', parent=self, pos=wx.Point(43, 198),
              size=wx.Size(76, 30), style=0)
        self.cbApply.SetMinSize(wx.Size(-1, -1))
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VTINPUTCALCEDITDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTINPUTCALCEDITDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(135, 198),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.SetMinSize(wx.Size(-1, -1))
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VTINPUTCALCEDITDIALOGCBCANCEL)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vtInput')
        self._init_ctrls(parent)
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        
        self.fgsData.SetMinSize(wx.Size(-1, -1))
        self.bxsBt.SetMinSize(wx.Size(-1, -1))
        self.bxsBtFormel.SetMinSize(wx.Size(-1, -1))
        self.bxsEdit.SetMinSize(wx.Size(-1, -1))
        self.bxsRes.SetMinSize(wx.Size(-1, -1))
        
        self.Layout()
        self.Fit()
        
        self.oReg=None
        self.vals=None
        self.dValMap=None
        self.val=''
        self.idxSel=-1
        
        self.__setup__()
        self.lFormel=[]
    def SetRegObj(self,o):
        self.oReg=o
    def __setup__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.chcQuick.Clear()
        _(u'original value'),_(u'first result'),_(u'slice')
        i=0
        for tup in self.QUICK:
            self.chcQuick.Append(_(tup[0]),i)
            i+=1
        if self.vals is not None:
            if type(self.vals)==types.DictionaryType:
                keys=self.vals.keys()
                keys.sort()
                self.chcQuick.Append('---',-1)
                for k in keys:
                    self.chcQuick.Append(k,-2)
        if self.dValMap is not None:
            keys=self.dValMap.keys()
            keys.sort()
            self.chcQuick.Append('---',-1)
            for k in keys:
                self.chcQuick.Append(k,-3)
            
    def SetValue(self,l,val,vals=None,dValMap=None):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.txtRes.SetValue('')
        self.lFormel=l
        self.val=val
        self.vals=vals
        self.dValMap=dValMap
        self.__setup__()
        self.__showFormel__()
    def __showFormel__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.idxSel=-1
        self.lstFormel.DeleteAllItems()
        i=1
        for it in self.lFormel:
            idx=self.lstFormel.InsertStringItem(sys.maxint,str(i))
            self.lstFormel.SetStringItem(idx,1,it)
            i+=1
    def GetValue(self):
        return self.lFormel
    
    def OnCbApplyButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.EndModal(1)
        event.Skip()

    def OnCbCancelButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.EndModal(0)
        event.Skip()

    def OnCbAddButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.txtRes.SetValue('')
        try:
            s=self.txtFormel.GetValue()
            if len(s)>0:
                self.lFormel.append(s)
                self.__showFormel__()
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()

    def OnCbDelButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.txtRes.SetValue('')
        try:
            if self.idxSel>=0:
                self.lFormel=self.lFormel[:self.idxSel]+self.lFormel[self.idxSel+1:]
                self.__showFormel__()
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()

    def OnCbUpButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.txtRes.SetValue('')
        try:
            if self.idxSel>0:
                self.lFormel=self.lFormel[:self.idxSel-1]+[self.lFormel[self.idxSel],self.lFormel[self.idxSel-1]]+self.lFormel[self.idxSel+1:]
                self.__showFormel__()
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()

    def OnCbDnButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.txtRes.SetValue('')
        try:
            iCount=self.lstFormel.GetItemCount()
            if self.idxSel<(iCount-1):
                self.lFormel=self.lFormel[:self.idxSel]+[self.lFormel[self.idxSel+1],self.lFormel[self.idxSel]]+self.lFormel[self.idxSel+2:]
                self.__showFormel__()
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()

    def OnChcQuickChoice(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        try:
            idx=self.chcQuick.GetSelection()
            i=self.chcQuick.GetClientData(idx)
            if i>=0:
                s=self.QUICK[i][1]
            elif i<-1:
                s='$'+self.chcQuick.GetStringSelection()+'$'
            sOld=self.txtFormel.GetValue()    
            self.txtFormel.SetValue(sOld+s)
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()

    def OnLstFormelListColClick(self, event):
        self.idxSel=-1
        event.Skip()

    def OnLstFormelListItemDeselected(self, event):
        self.idxSel=-1
        event.Skip()

    def OnLstFormelListItemSelected(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.idxSel=event.GetIndex()
        if self.idxSel>=0:
            self.txtFormel.SetValue(self.lFormel[self.idxSel])
        event.Skip()

    def OnCbCalcButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        sRes=self.oReg.Calc(self.val,self.vals,self.lFormel,self.dValMap)
        self.txtRes.SetValue(sRes)
        event.Skip()
