#Boa:FramePanel:vtInputAttrMLPanel
#----------------------------------------------------------------------
# Name:         vtInputAttrMLPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060219
# CVS-ID:       $Id: vtInputAttrMLPanel.py,v 1.11 2008/03/22 14:34:24 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
from wx.lib.anchors import LayoutAnchors
import sys,string
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.net.vNetXmlWxGui import *
import vidarc.tool.xml.vtXmlDomConsumer as vtXmlDomConsumer
from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_GOT_CONTENT
from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_GET_NODE
from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_REMOVE_NODE
from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_LOCK
from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_UNLOCK
from vidarc.tool.net.vNetXmlWxGui import EVT_NET_XML_SET_NODE

import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
#import vidarc.tool.lang.vtLgBase as vtLgBase

VERBOSE=0

[wxID_VTINPUTATTRMLPANEL, wxID_VTINPUTATTRMLPANELCBAPPLY, 
 wxID_VTINPUTATTRMLPANELCBDEL, wxID_VTINPUTATTRMLPANELCMBATTRNAME, 
 wxID_VTINPUTATTRMLPANELLBLATTRNAME, wxID_VTINPUTATTRMLPANELLBLATTRVAL, 
 wxID_VTINPUTATTRMLPANELLSTATTR, wxID_VTINPUTATTRMLPANELTXTATTRVAL, 
 wxID_VTINPUTATTRMLPANELTXTSUFF, 
] = [wx.NewId() for _init_ctrls in range(9)]

class vtInputAttrMLPanel(wx.Panel,vtXmlDomConsumer.vtXmlDomConsumer):
    IMG_DICT={'tel':vtArt.Tel,
            'mobile':vtArt.Mobile,
            'fax':vtArt.Fax,
            'email':vtArt.EMail,
            'mail':vtArt.Mail,
            'mailbox':vtArt.MailBox,
            'reference':vtArt.Link,
            'safe':vtArt.Safe,
            'tools':vtArt.Tools,
            'client':vtArt.Customer,
            'clientshort':vtArt.CustomerShort,
            'facility':vtArt.Facility,
            'manager':vtArt.Manager,
            'location':vtArt.Loc,
            'calc':vtArt.Calc,
            'cash':vtArt.Cash,
            'country':vtArt.Country,
            'region':vtArt.Region,
            'address':vtArt.Addr,
            'area':vtArt.Area,
            }
    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblAttrName, (0, 0), border=2,
              flag=wx.EXPAND | wx.RIGHT, span=(1, 1))
        parent.AddWindow(self.cmbAttrName, (0, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.txtSuff, (0, 2), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.cbApply, (0, 4), border=0, flag=0, span=(2, 1))
        parent.AddWindow(self.lblAttrVal, (1, 0), border=2,
              flag=wx.ALIGN_RIGHT | wx.EXPAND, span=(1, 1))
        parent.AddWindow(self.txtAttrVal, (1, 1), border=0, flag=wx.EXPAND,
              span=(1, 2))
        parent.AddWindow(self.cbDel, (2, 4), border=0, flag=0, span=(1, 1))
        parent.AddWindow(self.lstAttr, (2, 0), border=0, flag=wx.EXPAND,
              span=(2, 3))
        parent.AddSpacer(wx.Size(4, 4), (0,3), border=0, flag=0, span=(1, 1))

    def _init_coll_lstAttr_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'attribute'), width=120)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'value'), width=250)

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=0, vgap=0)

        self._init_coll_gbsData_Items(self.gbsData)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTINPUTATTRMLPANEL,
              name=u'vtInputAttrMLPanel', parent=prnt, pos=wx.Point(101, 264),
              size=wx.Size(359, 192), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(351, 165))
        self.SetAutoLayout(True)

        self.lblAttrName = wx.StaticText(id=wxID_VTINPUTATTRMLPANELLBLATTRNAME,
              label=_(u'attribute'), name=u'lblAttrName', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(90, 21), style=wx.ALIGN_RIGHT)

        self.cmbAttrName = wx.ComboBox(choices=[],
              id=wxID_VTINPUTATTRMLPANELCMBATTRNAME, name=u'cmbAttrName',
              parent=self, pos=wx.Point(92, 0), size=wx.Size(100, 21),
              style=wx.CB_SORT, value=u'')
        self.cmbAttrName.SetLabel(u'')
        self.cmbAttrName.Bind(wx.EVT_TEXT_ENTER, self.OnCmbAttrNameTextEnter,
              id=wxID_VTINPUTATTRMLPANELCMBATTRNAME)
        self.cmbAttrName.Bind(wx.EVT_COMBOBOX, self.OnCmbAttrNameCombobox,
              id=wxID_VTINPUTATTRMLPANELCMBATTRNAME)
        self.cmbAttrName.Bind(wx.EVT_TEXT, self.OnCmbAttrNameText,
              id=wxID_VTINPUTATTRMLPANELCMBATTRNAME)

        self.txtSuff = wx.TextCtrl(id=wxID_VTINPUTATTRMLPANELTXTSUFF,
              name=u'txtSuff', parent=self, pos=wx.Point(192, 0),
              size=wx.Size(85, 21), style=0, value=u'')

        self.lblAttrVal = wx.StaticText(id=wxID_VTINPUTATTRMLPANELLBLATTRVAL,
              label=_(u'value'), name=u'lblAttrVal', parent=self,
              pos=wx.Point(0, 21), size=wx.Size(92, 21), style=wx.ALIGN_RIGHT)
        id=wx.NewId()
        self.txtAttrVal = wx.TextCtrl(id=id,
              name=u'txtAttrVal', parent=self, pos=wx.Point(92, 21),
              size=wx.Size(185, 21), style=0,value='')

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTINPUTATTRMLPANELCBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Apply'), name=u'cbApply',
              parent=self, pos=wx.Point(287, 0), size=wx.Size(76, 30), style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VTINPUTATTRMLPANELCBAPPLY)

        self.cbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTINPUTATTRMLPANELCBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Del'), name=u'cbDel',
              parent=self, pos=wx.Point(287, 42), size=wx.Size(76, 30),
              style=0)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VTINPUTATTRMLPANELCBDEL)

        self.lstAttr = wx.ListCtrl(id=wxID_VTINPUTATTRMLPANELLSTATTR,
              name=u'lstAttr', parent=self, pos=wx.Point(0, 42),
              size=wx.Size(277, 120), style=wx.LC_REPORT|wx.LC_SORT_ASCENDING)
        self._init_coll_lstAttr_Columns(self.lstAttr)
        self.lstAttr.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstAttrListItemSelected,
              id=wxID_VTINPUTATTRMLPANELLSTATTR)
        self.lstAttr.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstAttrListItemDeselected,
              id=wxID_VTINPUTATTRMLPANELLSTATTR)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vtInput')
        vtXmlDomConsumer.vtXmlDomConsumer.__init__(self)
        self._init_ctrls(parent)
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.node=None
        self.doc=None
        self.bModified=False
        self.bEnableMark=True
        self.verbose=VERBOSE
        self.selectedIdx=-1
        
        self.SetImageDict(self.IMG_DICT)
        self.gbsData.AddGrowableRow(3)
        self.gbsData.AddGrowableCol(1)
        
        self.cbApply.SetBitmapLabel(vtArt.getBitmap(vtArt.ApplyAttr))
        self.cbDel.SetBitmapLabel(vtArt.getBitmap(vtArt.DelAttr))
        self.__setupImageList__()
        
        self.Move(pos)
        self.SetSize(size)
    def SetEnableMark(self,flag):
        self.bEnableMark=flag
    def SetImageDict(self,d):
        self.dImages=d
    def __setupImageList__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        _(u'tel'),_(u'email'),_(u'mobil'),_('fax'),_('safe'),_(u'tools')
        _(u'client'),_(u'clientshort'),_(u'mail'),_(u'mailbox')
        _(u'reference'),_(u'facility'),_(u'manager'),_(u'location')
        _(u'calc'),_(u'cash'),_(u'country'),_(u'region')
        _(u'address'),_(u'area')
        self.imgDict={}
        self.imgLstTyp=wx.ImageList(16,16)
        keys=self.dImages.keys()
        keys.sort()
        for k in keys:
            self.imgDict[k]=self.imgLstTyp.Add(vtArt.getBitmap(self.dImages[k]))
            self.cmbAttrName.Append(_(k))
        #img=images_attr.getTelBitmap()
        #self.imgDict['tel']=self.imgLstTyp.Add(img)
        #img=images_attr.getMobileBitmap()
        #self.imgDict['mobile']=self.imgLstTyp.Add(img)
        #img=images_attr.getFaxBitmap()
        #self.imgDict['fax']=self.imgLstTyp.Add(img)
        #img=images_attr.getEmailBitmap()
        #self.imgDict['email']=self.imgLstTyp.Add(img)
        self.lstAttr.SetImageList(self.imgLstTyp,wx.IMAGE_LIST_SMALL)

    def SetModified(self,state):
        self.bModified=state
    def GetModified(self):
        return self.bModified
    def __isModified__(self):
        if self.bModified:
            return True
        return False
    def __markModified__(self,flag=True):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.bEnableMark==False:
            return
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'__markModified__;flag:%s'%(flag),
        #                self)
        if flag:
            f=self.lstAttr.GetFont()
            f.SetWeight(wx.FONTWEIGHT_BOLD)
            self.lstAttr.SetFont(f)
        else:
            f=self.lstAttr.GetFont()
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
            self.lstAttr.SetFont(f)
        self.lstAttr.Refresh()
    def Clear(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.verbose:
            vtLog.CallStack('')
        self.SetModified(False)
        self.__markModified__(False)
        vtXmlDomConsumer.vtXmlDomConsumer.Clear(self)
        self.cmbAttrName.SetValue('')
        self.txtAttrVal.SetValue('')
        self.txtSuff.SetValue('')
        self.lstAttr.DeleteAllItems()
        self.selectedIdx=-1
    def IsBusy(self):
        return False
    def Stop(self):
        pass
    def SetRegNode(self,obj):
        pass
    def SetDoc(self,doc,bNet=False):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if doc is None:
            return
        if bNet==True:
            EVT_NET_XML_GOT_CONTENT(doc,self.OnGotContent)
            EVT_NET_XML_GET_NODE(doc,self.OnGetNode)
            EVT_NET_XML_REMOVE_NODE(doc,self.OnRemoveNode)
            EVT_NET_XML_LOCK(doc,self.OnLock)
            EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        vtXmlDomConsumer.vtXmlDomConsumer.SetDoc(self,doc)
        
    def OnGotContent(self,evt):
        evt.Skip()
    def OnGetNode(self,evt):
        evt.Skip()
        if self.node is None:
            return
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            self.SetNode(self.node)
    def OnSetNode(self,evt):
        evt.Skip()
    def OnAddNode(self,evt):
        evt.Skip()
    def OnDelNode(self,evt):
        evt.Skip()
    def OnRemoveNode(self,evt):
        evt.Skip()
    def OnLock(self,evt):
        evt.Skip()
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                self.Lock(False)
            else:
                self.Lock(True)
    def OnUnLock(self,evt):
        evt.Skip()
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['released']:
                self.Lock(False)
    def __getAttrNameSuff__(self,sAttrName):
        strs=string.split(sAttrName,'_')
        try:
            if len(strs)>1:
                sSuff=strs[-1]
                sAttrName=sAttrName[:-(len(sSuff)+1)]
            else:
                sSuff=''
        except:
            sSuff=''
        return sAttrName,sSuff
    def __getAttrName__(self,sAttrName):
        sAttr,sSuff=self.__getAttrNameSuff__(sAttrName)
        return self.__buildAttrName__(sAttr,sSuff)
    def __buildAttrName__(self,sAttr,sSuff):
        if len(sSuff)>0:
            return sAttr+'_'+sSuff
        return sAttr
    def __getAttrNameSuffML__(self,sAttrName):
        strs=string.split(sAttrName,'_')
        try:
            if len(strs)>1:
                sSuff=strs[-1]
                sAttrName=sAttrName[:-(len(sSuff)+1)]
            else:
                sSuff=''
        except:
            sSuff=''
        return _(sAttrName),sSuff
    def __getAttrNameML__(self,sAttrName):
        sAttr,sSuff=self.__getAttrNameSuffML__(sAttrName)
        return self.__buildAttrName__(sAttr,sSuff)
    def __getAttrKey__(self,key):
        for k in self.dImages.keys():
            if _(k)==key:
                return k
        return key
    def SetNode(self,node):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'SetNode',origin='vLocPanel')
        self.Clear()
        self.node=node
        vtXmlDomConsumer.vtXmlDomConsumer.SetNode(self,node)
        if self.verbose:
            vtLog.CallStack('')
            print self.node
        attrNode=self.doc.getChild(node,'attributes')
        if attrNode is not None:
            attrs={}
            for o in self.doc.getChilds(attrNode):
                s=self.doc.getText(o)
                sAttr=self.doc.getTagName(o)
                attrs[sAttr]=s
        else:
            attrs={}
            #attrs['email']=''
        if self.verbose:
            print vtLog.pprint(attrs)
        keys=attrs.keys()
        keys.sort()
        for k in keys:
            #self.lstAttr.InsertStringItem(0,k)
            #self.lstAttr.InsertStringItem(1,attrs[k])
            sAttr,sSuff=self.__getAttrNameSuff__(k)
            try:
                imgId=self.imgDict[k]
            except:
                imgId=-1
            sAttrName=self.__getAttrNameML__(k)
            index = self.lstAttr.InsertImageStringItem(sys.maxint, sAttrName, imgId)
            self.lstAttr.SetStringItem(index, 1, attrs[k])
        
        pass
    def GetNode(self,node=None):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.doc is None:
            return
        if node is None:
            if self.node is None:
                return
            node=self.node
        attrNode=self.doc.getChildForced(self.node,'attributes')
        if attrNode is None:
            vtLog.vtLngCurWX(vtLog.CRITICAL,self.node,self)
            return  # fix me, add element node
        childs=self.doc.getChilds(attrNode)
        d={}
        for c in childs:
            d[self.doc.getTagName(c)]=c
        for i in range(0,self.lstAttr.GetItemCount()):
            it=self.lstAttr.GetItem(i)
            sAttrName=it.m_text
            it=self.lstAttr.GetItem(i,1)
            sAttrVal=it.m_text
            sAttr,sSuff=self.__getAttrNameSuff__(sAttrName)
            sAttrKey=self.__getAttrKey__(sAttr)
            sAttrName=self.__buildAttrName__(sAttrKey,sSuff)
            d[sAttrName]=None
            self.doc.setNodeText(attrNode,sAttrName,sAttrVal)
        for k in d.keys():
            if d[k] is not None:
                self.doc.deleteNode(d[k],attrNode)
        self.SetModified(False)
        self.__markModified__(False)
        self.doc.AlignNode(node,iRec=2)
        pass
    def GetValueDict(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        d={}
        for i in xrange(0,self.lstAttr.GetItemCount()):
            it=self.lstAttr.GetItem(i)
            sAttrName=it.m_text
            it=self.lstAttr.GetItem(i,1)
            sAttrVal=it.m_text
            d[sAttrName]=sAttrVal
        return d
    def Lock(self,flag):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if flag:
            self.cbApply.Enable(False)
            self.cbDel.Enable(False)
            self.cmbAttrName.Enable(False)
            self.txtAttrVal.Enable(False)
            self.lstAttr.Enable(False)
            pass
        else:
            self.cbApply.Enable(True)
            self.cbDel.Enable(True)
            self.cmbAttrName.Enable(True)
            self.txtAttrVal.Enable(True)
            self.lstAttr.Enable(True)
            pass

    def OnCbApplyButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        sNewAttr=self.cmbAttrName.GetValue()
        sSuff=self.txtSuff.GetValue()
        self.SetModified(True)
        self.__markModified__(True)
        if len(sSuff)>0:
            sNewAttr+='_%s'%sSuff
        for i in range(0,self.lstAttr.GetItemCount()):
            it=self.lstAttr.GetItem(i)
            sAttrName=it.m_text
            if sAttrName==sNewAttr:
                it.m_col=1
                it.m_mask = wx.LIST_MASK_TEXT
                it.m_text=self.txtAttrVal.GetValue()
                self.lstAttr.SetItem(it)
                return
        try:
            sImgName,sSuff=self.__getAttrNameSuff__(sNewAttr)
            imgId=self.imgDict[self.__getAttrKey__(sImgName)]
        except:
            imgId=-1
        index = self.lstAttr.InsertImageStringItem(sys.maxint, sNewAttr, imgId)
        self.lstAttr.SetStringItem(index, 1, self.txtAttrVal.GetValue())
        for i in range(self.lstAttr.GetItemCount()):
            self.lstAttr.SetItemState(i,0,wx.LIST_STATE_SELECTED)
        self.lstAttr.SetItemState(index,wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
        event.Skip()

    def OnCbDelButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.selectedIdx>=0:
            self.lstAttr.DeleteItem(self.selectedIdx)
            self.SetModified(True)
            self.__markModified__(True)
        event.Skip()
    def OnLstAttrListItemSelected(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        idx=event.GetIndex()
        #it=event.GetItem()
        #it.m_mask = wx.LIST_MASK_TEXT
        #it.m_itemId=idx
        #it.m_col=0
        it=self.lstAttr.GetItem(idx,0)
        sAttrName=it.m_text
        #it.m_col=1
        it=self.lstAttr.GetItem(idx,1)
        sAttrVal=it.m_text
        #self.txtAttrName.SetValue(sAttrName)
        
        sAttrName,sSuff=self.__getAttrNameSuff__(sAttrName)
        self.txtSuff.SetValue(sSuff)
        self.cmbAttrName.SetValue(sAttrName)
        self.txtAttrVal.SetValue(sAttrVal)
        self.selectedIdx=it.m_itemId
        event.Skip()

    def OnLstAttrListItemDeselected(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.selectedIdx=-1
        self.cmbAttrName.SetValue('')
        self.txtAttrVal.SetValue('')
        self.txtSuff.SetValue('')
        event.Skip()
    def GetVal(self,attr,suff=None):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if suff is None:
            lst=[]
            for i in range(0,self.lstAttr.GetItemCount()):
                it=self.lstAttr.GetItem(i)
                sAttrName=it.m_text
                
                sAttr,sSuff=self.__getAttrNameSuff__(sAttrName)
                if sAttr==attr:
                    it=self.lstAttr.GetItem(i,1)
                    sAttrVal=it.m_text
                    lst.append((sAttr,sSuff,sAttrVal))
            return lst
        else:
            for i in range(0,self.lstAttr.GetItemCount()):
                it=self.lstAttr.GetItem(i)
                sAttrName=it.m_text
                
                sAttr,sSuff=self.__getAttrNameSuff__(sAttrName)
                if sAttr==attr:
                    if sSuff==suff:
                        it=self.lstAttr.GetItem(i,1)
                        sAttrVal=it.m_text
                        return sAttrVal
            return None
    def OnCmbAttrNameTextEnter(self, event):
        event.Skip()

    def OnCmbAttrNameCombobox(self, event):
        event.Skip()

    def OnCmbAttrNameText(self, event):
        event.Skip()
            
