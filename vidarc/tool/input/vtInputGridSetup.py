#----------------------------------------------------------------------------
# Name:         vtInputGridSetup.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060621
# CVS-ID:       $Id: vtInputGridSetup.py,v 1.9 2008/03/22 14:34:24 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import  wx
import  wx.grid as gridlib

import re

from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer
from vidarc.tool.xml.vtXmlDomConsumerLang import vtXmlDomConsumerLang
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase

wxEVT_VTINPUT_GRIDSETUP_INFO_CHANGED=wx.NewEventType()
vEVT_VTINPUT_GRIDSETUP_INFO_CHANGED=wx.PyEventBinder(wxEVT_VTINPUT_GRIDSETUP_INFO_CHANGED,1)
def EVT_VTINPUT_GRIDSETUP_INFO_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTINPUT_GRID_INFO_CHANGED,func)
class vtInputGridSetupInfoChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTINPUT_GRIDSETUP_INFO_CHANGED(<widget_name>, self.OnInfoChanged)
    """

    def __init__(self,obj,row,col):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTINPUT_GRIDSETUP_INFO_CHANGED)
        self.obj=obj
        self.row=row
        self.col=col
    def GetObj(self):
        return self.obj
    def GetRow(self):
        return self.row
    def GetCol(self):
        return self.col

wxEVT_VTINPUT_GRIDSETUP_INFO_SELECTED=wx.NewEventType()
vEVT_VTINPUT_GRIDSETUP_INFO_SELECTED=wx.PyEventBinder(wxEVT_VTINPUT_GRIDSETUP_INFO_SELECTED,1)
def EVT_VTINPUT_GRIDSETUP_INFO_SELECTED(win,func):
    win.Connect(-1,-1,wxEVT_VTINPUT_GRIDSETUP_INFO_SELECTED,func)
class vtInputGridSetupInfoSelected(wx.PyEvent):
    """
    Posted Events:
        Project Info SELECTED event
            EVT_VTINPUT_GRIDSETUP_INFO_SELECTED(<widget_name>, self.OnInfoSELECTED)
    """

    def __init__(self,obj,row,col):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTINPUT_GRIDSETUP_INFO_SELECTED)
        self.obj=obj
        self.row=row
        self.col=col
    def GetObj(self):
        return self.obj
    def GetRow(self):
        return self.row
    def GetCol(self):
        return self.col

class vtInputGridSetup(gridlib.Grid,vtXmlDomConsumer,vtXmlDomConsumerLang):
    VERBOSE=0
    EDIT_DIRECTION_LEFT_RIGHT = 0
    EDIT_DIRECTION_TOP_DOWN = 1
    def __init__(self, parent, id, pos, size,name='vtInputGridSetup', style=0, \
                colLabelSize=20,rowLabelSize=120,bAutoStretch=False):
        vtXmlDomConsumer.__init__(self)
        vtXmlDomConsumerLang.__init__(self)
        gridlib.Grid.__init__(self, parent, id,pos,size,name=name)
        self.table=None
        self.emptyEditor=None
        self.emptyRenderer=None
        self.imgRenderer=None
        self.lstIDNodes=None
        self.lSetup=None
        self.bAutoStretch=bAutoStretch
        self.lStretchFact=None
        self.dCalc={}
        self.lSum=None
        self.tagName='data'
        #table = gridAttrData()

        # The second parameter means that the grid is to take ownership of the
        # table and will destroy it when done.  Otherwise you would need to keep
        # a reference to it and call it's Destroy method later.
        #sself.SetTable(table, True)
        self.lstColumnSize=None
        self.lstColumnAlign=None
        self.SetColLabelSize(colLabelSize)
        self.SetRowLabelSize(rowLabelSize)
        self.SetMargins(0,0)
        self.AutoSizeColumns(True)
        self.AutoSizeRows(True)
        
        gridlib.EVT_GRID_CELL_LEFT_CLICK(self, self.OnLeftClick)
        gridlib.EVT_GRID_CELL_LEFT_DCLICK(self, self.OnLeftDClick)
        gridlib.EVT_GRID_CELL_CHANGE(self, self.OnCellChange)
        gridlib.EVT_GRID_SELECT_CELL(self, self.OnCellSelect)
        gridlib.EVT_GRID_EDITOR_SHOWN(self, self.OnEditorShown)
        wx.EVT_SIZE(self, self.OnSize)
        self.Bind(wx.EVT_KEY_DOWN, self.OnKeyDown)
        
        self.editDirection=self.EDIT_DIRECTION_LEFT_RIGHT
        #self.editDirection=self.EDIT_DIRECTION_TOP_DOWN
        self.bMod=False
        self.bEnableMark=True
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
    def SetAutoStretchFactor(self,lFact):
        self.lStretchFact=lFact
    def SetAutoStretch(self,flag):
        self.bAutoStretch=flag
    def SetEnableMark(self,flag):
        self.bEnableMark=flag
        self.__markModified__(flag)
    def __markModified__(self,flag=True):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if flag==False:
            iRowCount=self.GetNumberRows()
            iColCount=self.GetNumberCols()
            font = self.GetFont()
            font.SetWeight(wx.NORMAL)
            for iRow in xrange(iRowCount):
                for iCol in xrange(iColCount):
                    gridlib.Grid.SetCellFont(self,iRow,iCol,font)
        self.Refresh()
    def IsModified(self):
        return self.bMod
    def SetModified(self,flag):
        self.bMod=flag
        self.__markModified__(flag)
    def IsBusy(self):
        return False
    def Stop(self):
        pass
    def SetDoc(self,doc):
        vtXmlDomConsumer.SetDoc(self,doc)
        #vtXmlDomConsumerLang.SetDoc(self,doc)
    def SetTagName(self,tagName):
        self.tagName=tagName
    def Clear(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.VERBOSE:
            vtLog.CallStack('')
        iRowCount=self.GetNumberRows()
        if self.lSum is not None:
            iRowCount-=1
        if iRowCount>0:
            self.DeleteRows(0,iRowCount)
        self.InsertRows(0,1)
        self.SetupRow(0)
        if self.lSum is not None:
            for iCol in self.lSum:
                self.__calcSum__(iCol)
        self.__markModified__(False)
        self.bMod=False
    def ClearLang(self):
        pass
    def UpdateLang(self):
        vtLgBase.setLocale(self.doc.GetLang())
    def SetNode(self,node,tagName=None):
        if VERBOSE:
            vtLog.CallStack('')
            print node
            print tagName
        self.Clear()
        self.node=node
        if tagName is not None:
            self.tagName=tagName
        if self.doc is None:
            return
        if self.node is None:
            return
        if self.tagName is None:
            return
        lVal=[]
        nodeData=self.doc.getChild(self.node,self.tagName)
        if nodeData is None:
            for cRow in self.doc.getChilds(nodeData,'row'):
                lCol=[]
                for cCol in self.doc.getChilds(cRow,'col'):
                    lCol.append(self.doc.getText(cCol))
                lVal.append(lCol)
        self.SetValue(lVal)
    def GetNode(self,node=None):
        if node is None:
            node=self.node
        if node is None:
            return
        nodeData=self.doc.getChild(node,self.tagName)
        if nodeData is not None:
            self.doc.deleteNode(nodeData,node)
        nodeData=self.doc.getChildForced(node,self.tagName)
        lVal=self.GetValueStr()
        if self.lSum is not None:
            for lRow in lVal[:-1]:
                nodeRow=self.doc.createSubNode(nodeData,'row')
                for sVal in lRow:
                    self.doc.createSubNodeText(nodeRow,'col',sVal)
        else:
            for lRow in lVal:
                nodeRow=self.doc.createSubNode(nodeData,'row')
                for sVal in lRow:
                    self.doc.createSubNodeText(nodeRow,'col',sVal)
    def SetEditDirection(self,type):
        self.editDirection=type
    def OnKeyDown(self, evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #vtLog.CallStack(evt.KeyCode())
        if wx.VERSION >= (2,8):
            key=evt.GetKeyCode()
        else:
            key = evt.KeyCode()
        if key != wx.WXK_RETURN:
            evt.Skip()
            return
        
        if evt.ControlDown():   # the edit control needs this key
            evt.Skip()
            return
        
        self.DisableCellEditControl()
        iRowCount=self.GetTable().GetNumberRows()
        if self.lSum is not None:
            iRowCount-=1
        if self.editDirection==self.EDIT_DIRECTION_LEFT_RIGHT:
            def moveRight():
                success = self.MoveCursorRight(evt.ShiftDown())
                if not success:
                    newRow = self.GetGridCursorRow() + 1
                    if newRow < iRowCount:
                        
                        self.SetGridCursor(newRow, 0)
                        self.MakeCellVisible(newRow, 0)
                        
                    else:
                        # this would be a good place to add a new row if your app
                        # needs to do that
                        self.InsertRows(iRowCount,1)
                        self.SetupRow(newRow)
                        self.SetGridCursor(newRow, 0)
                        self.MakeCellVisible(newRow, 0)
                        return True
                return not self.IsReadOnly(self.GetGridCursorRow(),self.GetGridCursorCol())
            while moveRight()==False:
                pass
        elif self.editDirection==self.EDIT_DIRECTION_TOP_DOWN:
            def moveDown():
                success = self.MoveCursorDown(evt.ShiftDown())
                
                if not success:
                    newCol = self.GetGridCursorCol() + 1
                    
                    if newCol < iRowCount:
                        self.SetGridCursor(0,newCol)
                        #self.MakeCellVisible(0,newCol)
                    else:
                        # this would be a good place to add a new row if your app
                        # needs to do that
                        self.InsertRows(iRowCount,1)
                        return True
                return not self.IsReadOnly(self.GetGridCursorRow(),self.GetGridCursorCol())
            while moveDown()==False:
                pass
        #print self.GetGridCursorRow(),self.GetGridCursorCol()
        #evt.Skip()

    # I do this because I don't like the default behaviour of not starting the
    # cell editor on double clicks, but only a second click.
    def OnLeftClick(self, evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.CanEnableCellControl():
            self.EnableCellEditControl()
        evt.Skip()
    def OnLeftDClick(self, evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.CanEnableCellControl():
            self.EnableCellEditControl()
        evt.Skip()
    def OnEditorShown(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #vtLog.CallStack('')
        evt.Skip()
    def OnSize(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if evt is not None:
            evt.Skip()
        if self.bAutoStretch:
            if self.lStretchFact is None:
                #self.AutoSize()
                self.AutoSizeColumns()
            else:
                try:
                    sz=self.GetClientSize()
                    iW=sz.GetWidth()
                    iW-=self.GetRowLabelSize()
                    if iW<100:
                        iW=100
                    i=0
                    for fact in self.lStretchFact:
                        self.SetColSize(i,int(fact*iW))
                        i+=1
                except:
                    vtLog.vtLngTB(self.GetName())
    def OnCellChange(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #vtLog.CallStack('')
        #self.__calcSum__(evt.GetCol())
        iRow,iCol=evt.GetRow(),evt.GetCol()
        if self.bEnableMark:
            font = self.GetFont()
            font.SetWeight(wx.BOLD)
            gridlib.Grid.SetCellFont(self,iRow,iCol,font)
        min,max=self.__getGridTypeLimit__(self.lSetup[iCol]['typedef'])
        sVal=self.GetCellValue(iRow,iCol)
        self.__calc__(iRow,iCol)
        val=self.__getVal__(self.lSetup[iCol]['typedef'],sVal)
        if min is not None:
            if val<min:
                val=min
                #self.SetCellValue(iRow,iCol,self.__convVal2Disp(iCol,val))
                #self.SetCellValue(iRow,iCol,str(val))
        if max is not None:
            if val>max:
                val=max
                #self.SetCellValue(iRow,iCol,self.__convVal2Disp(iCol,val))
                #self.SetCellValue(iRow,iCol,str(val))
        self.SetCellValue(iRow,iCol,self.__convVal2Disp(iCol,val))
        self.bMod=True
        wx.PostEvent(self,vtInputGridSetupInfoChanged(self,evt.GetRow(),evt.GetCol()))
        evt.Skip()
        wx.CallAfter(self.OnSize,None)
    def OnCellSelect(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #vtLog.CallStack('')
        #self.BeginBatch()
        #self.EndBatch()
        #self.ForceRefresh()
        #msg = gridlib.GridTableMessage(self.table, gridlib.GRIDTABLE_REQUEST_VIEW_GET_VALUES)
        #msg = gridlib.GridTableMessage(self.table, gridlib.GRIDTABLE_NOTIFY_COLS_INSERTED)
        #self.ProcessTableMessage(msg)
        evt.Skip()
        wx.PostEvent(self,vtInputGridSetupInfoSelected(self,evt.GetRow(),evt.GetCol()))
    def SetColumnSize(self,lst):
        self.lstColumnSize=lst
    def SetColumnAlign(self,lst):
        self.lstColumnAlign=lst

    def SetupRow(self,iRow,lVal=None,bFroceReadOnly=False):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.lSetup is None:
            return
        font = self.GetFont()
        font.SetWeight(wx.NORMAL)
        for iCol in xrange(len(self.lSetup)):
            d=self.lSetup[iCol]
            if d['typedef'] is not None:
                sType,edt,rend,align=self.__getGridType__(d['typedef'])
            else:
                sType=None
            if sType is None:
                self.SetReadOnly(iRow,iCol)
                self.SetCellBackgroundColour(iRow,iCol,wx.LIGHT_GREY)
                self.SetCellAlignment(iRow,iCol,align,wx.ALIGN_CENTRE)
                self.SetCellValue(iRow,iCol,'---')
            else:
                self.SetCellEditor(iRow,iCol,edt)
                self.SetCellRenderer(iRow,iCol,rend)
                self.SetCellAlignment(iRow,iCol,align,wx.ALIGN_CENTRE)
                if 'readonly' in d or bFroceReadOnly:
                    self.SetCellBackgroundColour(iRow,iCol,wx.LIGHT_GREY)
                    self.SetReadOnly(iRow,iCol)
                else:
                    self.SetCellBackgroundColour(iRow,iCol,wx.WHITE)
                    self.SetReadOnly(iRow,iCol,False)
                if lVal is not None:
                    try:
                        sVal=lVal[iRow][iCol]
                        val=self.__getVal__(self.lSetup[iCol]['typedef'],sVal)
                        sVal=self.__convVal2Disp(iCol,val)
                        self.SetCellValue(iRow,iCol,sVal)
                    except:
                        vtLog.vtLngTB(self.GetName())
            self.SetCellFont(iRow,iCol,font)
    def SetCellValue(self,iRow,iCol,sVal):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,
                            'col:%d;row:%d;bCalc:%s'%(iCol,iRow,self.bCalc),self)
        gridlib.Grid.SetCellValue(self,iRow,iCol,sVal)
        if self.bCalc:
            self.__calc__(iRow,iCol)
    def __convVal2Disp(self,iCol,val):
        try:
            d=self.lSetup[iCol]
            sType=d['typedef']['type']
            if sType=='float':
                return vtLgBase.format(d['typedef']['fmt'],float(val))
            return unicode(val)
        except:
            #vtLog.vtLngCurWX(vtLog.ERROR,'col:%d;val:%s;lSetup:%s'%(iCol,unicode(val),vtLog.pformat(self.lSetup)),self)
            #vtLog.vtLngTB(self.GetName())
            pass
        return unicode(val)
    def __calcSum__(self,iCol):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.bCalcSum==False:
            return
        try:
            if self.lSum is None:
                return
            if iCol in self.lSum:
                iRowCount=self.GetNumberRows()
                d=self.lSetup[iCol]
                sType=d['typedef']['type']
                if sType=='float':
                    sum=0.0
                    f=float
                elif sType=='int':
                    sum=0
                    f=int
                elif sType=='long':
                    sum=0
                    f=long
                else:
                    return
                for iRow in xrange(iRowCount-1):
                    try:
                        valPart=self.__getVal__(self.lSetup[iCol]['typedef'],
                                    self.GetCellValue(iRow,iCol))
                        #print valPart,type(valPart),sum,type(sum)
                        sum+=f(valPart)
                        #sum+=f(self.GetCellValue(iRow,iCol))
                    except:
                        pass
                gridlib.Grid.SetCellValue(self,iRowCount-1,iCol,self.__convVal2Disp(iCol,sum))
        except:
            vtLog.vtLngTB(self.GetName())
    def __calc__(self,iRow,iCol):
        try:
            #vtLog.CallStack(str(iCol))
            #vtLog.pprint(self.dCalc)
            if iCol not in self.dCalc:
                self.__calcSum__(iCol)
                return
            for iColCalc in self.dCalc[iCol]:
                d=self.lSetup[iColCalc]
                if self.VERBOSE:
                    vtLog.vtLngCurWX(vtLog.DEBUG,
                                'col:%d;row:%d;d:%s'%(iColCalc,iRow,vtLog.pformat(d)),self)
                if 'calc' not in d:
                    return
                s=d['calc']
                res=re.split("(#\d+#)",s)
                for iGrp in xrange(len(res)):
                    sPart=res[iGrp]
                    if len(sPart)>0:
                        if sPart[0]=='#' and sPart[-1]=='#':
                            i=int(sPart[1:-1])
                            valPart=self.__getVal__(self.lSetup[iColCalc]['typedef'],
                                    self.GetCellValue(iRow,i))
                            #sPart=self.__convVal2Disp(iColCalc,valPart)
                            sPart=unicode(valPart)
                            #sPart=unicode(self.GetCellValue(iRow,i))
                            res[iGrp]=sPart
                try:
                    if self.VERBOSE:
                        vtLog.vtLngCurWX(vtLog.DEBUG,
                                    'formel:%s;res:%s;val:%s'%(s,vtLog.pformat(res),unicode(eval(''.join(res)))),self)
                    #print res,type(eval(''.join(res))),self.__convVal2Disp(iColCalc,eval(''.join(res)))
                    gridlib.Grid.SetCellValue(self,iRow,iColCalc,self.__convVal2Disp(iColCalc,eval(''.join(res))))
                    #gridlib.Grid.SetCellValue(self,iRow,iColCalc,unicode(eval(''.join(res))))
                except:
                    pass
                self.__calcSum__(iColCalc)
        except:
            vtLog.vtLngTB(self.GetName())
    def Setup(self,lSetup,lVal):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if lVal is None:
            iRowCount=0
        else:
            iRowCount=len(lVal)
        iColCount=len(lSetup)
        iCol=0
        self.bCalc=False
        self.bCalcSum=False
        self.dCalc={}
        self.lSum=None
        if self.bAutoStretch:
            self.lStretchFact=[]
        else:
            self.lStretchFact=None
        for d in lSetup:
            if 'calc' in d:
                s=d['calc']
                res=re.split("(#\d+#)",s)
                for sPart in res:
                    if len(sPart)>0:
                        if sPart[0]=='#' and sPart[-1]=='#':
                            i=int(sPart[1:-1])
                            if i not in self.dCalc:
                                self.dCalc[i]=[]
                            self.dCalc[i].append(iCol)
            if 'total' in d:
                if self.lSum is None:
                    self.lSum=[]
                self.lSum.append(iCol)
            if self.bAutoStretch:
                if 'stretch' in d:
                    fact=d['stretch']
                else:
                    fact=0.1
                self.lStretchFact.append(fact)
            iCol+=1
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,
                        'col:%d;row:%d;dCalc:%s;lSum:%s'%(iColCount,iRowCount,vtLog.pformat(self.dCalc),self.lSum),self)
        self.BeginBatch()
        try:
            self.CreateGrid(iRowCount,iColCount)
        except:
            iCol=self.GetNumberCols()
            iColDiff=iColCount-iCol
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,
                        'col:%d;colDiff:%d'%(iCol,iColDiff),self)
            if iColDiff!=0:
                if iColCount>iCol:
                    self.AppendCols(iColCount-iCol)
                else:
                    self.DeleteCols(iColCount,iCol-iColCount)
            iRow=self.GetNumberRows()
            iRowDiff=iRowCount-iRow
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,
                        'row:%d;rowDiff:%d'%(iRow,iRowDiff),self)
            if iRowDiff!=0:
                if iRowCount>iRow:
                    self.AppendRows(iRowCount-iRow)
                else:
                    self.DeleteRows(iRowCount,iRow-iRowCount)
        i=0
        for d in lSetup:
            self.SetColLabelValue(i,d['label'])
            i+=1
            #sRow=self.dataSrc.GetRowLabelValue(iRow)
            #self.SetRowLabelValue(iRow,sRow)
        self.lSetup=lSetup
        for iRow in xrange(iRowCount):
            self.SetupRow(iRow,lVal)
        self.bCalc=True
        for iRow in xrange(iRowCount):
            for iCol in xrange(iColCount):
                self.__calc__(iRow,iCol)
        self.bCalcSum=True
        if self.lSum is not None:
            self.AppendRows(1,True)
            self.SetupRow(iRowCount,None,bFroceReadOnly=True)
            for iCol in self.lSum:
                self.__calcSum__(iCol)
        self.bCalc=True
        self.AutoSize()
        self.EndBatch()
        #self.ForceRefresh()
    def __getGridType__(self,dTyp):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if dTyp is None:
            return None,None,None,wx.ALIGN_CENTRE
        elif dTyp['type']=='datetime':
            datTyp=gridlib.GRID_VALUE_DATETIME
            edt=gridlib.GridCellTextEditor()
            rend=gridlib.GridCellStringRenderer()
            align=wx.ALIGN_CENTRE
        elif dTyp['type']=='int':
            datTyp=gridlib.GRID_VALUE_NUMBER
            try:
                sMin=dTyp['min']
                iMin=int(sMin)
            except:
                sMin=''
                iMin=0
            try:
                sMax=dTyp['max']
                iMax=int(sMax)
            except:
                sMax=''
                iMax=0
            if len(sMin)>0 or len(sMax)>0:
                s=':'+sMin+','+sMax
                datTyp=datTyp+s
            else:
                pass
            edt=gridlib.GridCellNumberEditor(iMin,iMax)
            rend=gridlib.GridCellNumberRenderer()
            align=wx.ALIGN_RIGHT
        elif dTyp['type']=='float':
            datTyp=gridlib.GRID_VALUE_FLOAT
            try:
                sDigits=dTyp['digits']
                iDigits=int(sDigits)
            except:
                sDigits=''
                iDigits=8
            try:
                sComma=dTyp['comma']
                iComma=int(sComma)
            except:
                sComma=''
                iComma=2
            if len(sDigits)>0 and len(sComma)>0:
                s=':'+sDigits+','+sComma
                datTyp=datTyp+s
            else:
                pass
            edt=gridlib.GridCellFloatEditor(iDigits,iComma)
            rend=gridlib.GridCellFloatRenderer(iDigits,iComma)
            align=wx.ALIGN_RIGHT
        elif dTyp['type']=='long':
            datTyp=gridlib.GRID_VALUE_LONG
            try:
                sMin=dTyp['min']
                iMin=int(sMin)
            except:
                sMin=''
                iMin=0
            try:
                sMax=dTyp['max']
                iMax=int(sMax)
            except:
                sMax=''
                iMax=100
            if len(sMin)>0 or len(sMax)>0:
                s=':'+sMin+','+sMax
                datTyp=datTyp+s
            else:
                pass
            edt=gridlib.GridCellNumberEditor(iMin,iMax)
            rend=gridlib.GridCellNumberRenderer()
            align=wx.ALIGN_RIGHT
        elif dTyp['type']=='choiceint':
            datTyp=gridlib.GRID_VALUE_CHOICEINT
            edt=gridlib.GridCellTextEditor()
            rend=gridlib.GridCellStringRenderer()
            align=wx.ALIGN_RIGHT
        elif dTyp['type']=='choice':
            datTyp=gridlib.GRID_VALUE_CHOICE
            keys=dTyp['infos'].keys()
            keys.sort()
            choices=[]
            for k in keys:
                if k[:2]=='it':
                    choices.append(dTyp['infos'][k])
            if len(choices)>0:
                datTyp=datTyp+':'+','.join(choices)
            edt=gridlib.GridCellChoiceEditor(choices)
            #edt=gridlib.GridCellChoiceEditor(','.join(choices))
            rend=gridlib.GridCellStringRenderer()
            align=wx.ALIGN_CENTRE
        elif dTyp['type']=='string':
            datTyp=gridlib.GRID_VALUE_STRING
            edt=gridlib.GridCellTextEditor()
            rend=gridlib.GridCellStringRenderer()
            align=wx.ALIGN_LEFT
        elif dTyp['type']=='text':
            datTyp=gridlib.GRID_VALUE_TEXT
            edt=gridlib.GridCellTextEditor()
            rend=gridlib.GridCellStringRenderer()
            align=wx.ALIGN_LEFT
        elif dTyp['type']=='bool':
            datTyp=gridlib.GRID_VALUE_BOOL
            edt=gridlib.GridCellBoolEditor()
            rend=gridlib.GridCellBoolRenderer()
            align=wx.ALIGN_CENTRE
        else:
            datTyp=gridlib.GRID_VALUE_STRING
            edt=gridlib.GridCellTextEditor()
            rend=gridlib.GridCellStringRenderer()
            align=wx.ALIGN_LEFT
        return datTyp,edt,rend,align
    def __getGridTypeLimit__(self,dTyp):
        if dTyp['type']=='datetime':
            return None,None
        elif dTyp['type']=='int':
            try:
                sMin=dTyp['min']
                iMin=int(sMin)
            except:
                sMin=''
                iMin=None
            try:
                sMax=dTyp['max']
                iMax=int(sMax)
            except:
                sMax=''
                iMax=None
            return iMin,iMax
        elif dTyp['type']=='float':
            try:
                sMin=dTyp['min']
                iMin=float(sMin)
            except:
                sMin=''
                iMin=None
            try:
                sMax=dTyp['max']
                iMax=float(sMax)
            except:
                sMax=''
                iMax=None
            return iMin,iMax
        elif dTyp['type']=='long':
            try:
                sMin=dTyp['min']
                iMin=long(sMin)
            except:
                sMin=''
                iMin=None
            try:
                sMax=dTyp['max']
                iMax=long(sMax)
            except:
                sMax=''
                iMax=None
            return iMin,iMax
        elif dTyp['type']=='bool':
            return None,None
        return None,None
    def __getVal__(self,dTyp,val):
        try:
            if dTyp['type']=='datetime':
                return val
            elif dTyp['type']=='int':
                try:
                    return int(val)
                except:
                    return int(float(val))
            elif dTyp['type']=='float':
                return float(val)
            elif dTyp['type']=='long':
                try:
                    return long(val)
                except:
                    return long(float(val))
            elif dTyp['type']=='choiceint':
                return int(val)
            elif dTyp['type']=='choice':
                return val
            elif dTyp['type']=='string':
                return val
            elif dTyp['type']=='text':
                return val
            if dTyp['type']=='bool':
                #print val,type(val)
                if val=='1' or val=='True':
                    return True
                else:
                    return False
            else:
                return val
            return val
        except:
            pass
        return ''
    def GetValue(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        iColCount=self.GetNumberCols()
        iRowCount=self.GetNumberRows()
        if self.lSum is None:
            lVal=[ [self.__getVal__(self.lSetup[iCol]['typedef'],self.GetCellValue(iRow,iCol)) for iCol in xrange(iColCount)] \
                    for iRow in xrange(iRowCount) ]
            lSum=None
        else:
            lVal=[ [self.__getVal__(self.lSetup[iCol]['typedef'],self.GetCellValue(iRow,iCol)) for iCol in xrange(iColCount)] \
                    for iRow in xrange(iRowCount-1) ]
            lSum=[self.__getVal__(self.lSetup[iCol]['typedef'],self.GetCellValue(iRowCount-1,iCol)) for iCol in xrange(iColCount)]
        return lVal,lSum
    def GetValueStr(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        lValOld,lSumOld=self.GetValue()
        iColCount=self.GetNumberCols()
        iRowCount=self.GetNumberRows()
        lVal=[ [unicode(val) for val in l] for l in lValOld]
        if lSumOld is not None:
            lSum=[unicode(val) for val in lSumOld]
        else:
            lSum=None
        return lVal,lSum
    def SetValue(self,lVal):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        iColCount=len(self.lSetup)
        iRowCount=len(lVal)
        if iRowCount<=0:
            iRowCount=1
            lVal=None
        self.BeginBatch()
        if self.CanEnableCellControl():
            self.DisableCellEditControl()
        font = self.GetFont()
        font.SetWeight(wx.NORMAL)
        
        iCol=self.GetNumberCols()
        iColDiff=iColCount-iCol
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,
                    'col:%d;colDiff:%d'%(iCol,iColDiff),self)
        if iColDiff!=0:
            if iColCount>iCol:
                self.AppendCols(iColCount-iCol)
            else:
                self.DeleteCols(iColCount,iCol-iColCount)
        iRow=self.GetNumberRows()
        if self.lSum is not None:
            iRow-=1
        iRowDiff=iRowCount-iRow
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,
                    'row:%d;rowDiff:%d;lVal:%s'%(iRow,iRowDiff,vtLog.pformat(lVal)),self)
        if iRowDiff!=0:
            if iRowCount>iRow:
                self.InsertRows(iRow,iRowCount-iRow)
            else:
                self.DeleteRows(iRowCount,iRow-iRowCount)
        
        self.bCalc=False
        self.bCalcSum=False
        for iRow in xrange(iRowCount):
            self.SetupRow(iRow,lVal)
        self.bCalc=True
        for iRow in xrange(iRowCount):
            for iCol in xrange(iColCount):
                self.__calc__(iRow,iCol)
        self.bCalcSum=True
        if self.lSum is not None:
            iRowCount=self.GetNumberRows()-1
            self.SetupRow(iRowCount,None,bFroceReadOnly=True)
            for iCol in self.lSum:
                self.__calcSum__(iCol)
        #self.AutoSize()
        self.EndBatch()
        self.OnSize(None)
        #self.ForceRefresh()
        #self.Refresh()
        #self.Layout()
        return 1
    def SetReadOnlyP(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #dataNodes=self.table.GetDataNodes()
        #dataTypes=self.table.GetDataTypes()
        #iRowCount=len(dataNodes)
        iRowCount=self.table.GetNumberRows()
        iColCount=self.table.GetNumberCols()
        bRowLabel=self.dataSrc.HasRowLabelChangeable()
        #if len(dataNodes)<=0:
        #    return
        if self.lstColumnSize:
            for i in range(len(self.lstColumnSize)):
                gridlib.Grid.SetColSize(self,i,self.lstColumnSize[i])
        for row in range(iRowCount):
            for col in range(iColCount):
                if self.dataSrc.GetDataNode(row,col) is None:
                    emptyEditor=gridlib.GridCellTextEditor()
                    emptyRenderer=gridlib.GridCellStringRenderer()
                    gridlib.Grid.SetCellEditor(self,row,col,emptyEditor)
                    gridlib.Grid.SetCellRenderer(self,row,col,emptyRenderer)
                    #if col>2:
                    #    gridlib.Grid.SetCellValue(self,row,col,'---')
                    gridlib.Grid.SetReadOnly(self,row,col)
                    gridlib.Grid.SetCellBackgroundColour(self,row,col,wx.LIGHT_GREY)
                    gridlib.Grid.SetCellAlignment(self,row,col,wx.ALIGN_CENTRE,wx.ALIGN_CENTRE)
                if bRowLabel:
                    if col==0 and row>0:
                        sVal=self.dataSrc.GetRowLabelValue(row)
                        self.SetCellValue(row,col,sVal)
        return 1
    def SetReadOnlyCols(self,cols):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        for col in cols:
            for row in range(self.GetNumberRows()):
                emptyEditor=gridlib.GridCellTextEditor()
                emptyRenderer=gridlib.GridCellStringRenderer()
                gridlib.Grid.SetCellEditor(self,row,col,emptyEditor)
                gridlib.Grid.SetCellRenderer(self,row,col,emptyRenderer)
                #if col>2:
                #    gridlib.Grid.SetCellValue(self,row,col,'---')
                gridlib.Grid.SetReadOnly(self,row,col)
                gridlib.Grid.SetCellBackgroundColour(self,row,col,wx.LIGHT_GREY)
                gridlib.Grid.SetCellAlignment(self,row,col,wx.ALIGN_CENTRE,wx.ALIGN_CENTRE)


