#Boa:Dialog:vtInputAttrCfgMLDialog
#----------------------------------------------------------------------
# Name:         vtInputAttrCfgMLDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060502
# CVS-ID:       $Id: vtInputAttrCfgMLDialog.py,v 1.9 2008/04/26 09:53:29 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.lang.vtLgSelector
import vidarc.tool.input.vtInputTag
import vidarc.tool.input.vtInputText
import wx.lib.buttons
import vidarc.tool.input.vtInputTextML
import vidarc.tool.lang.vtLgBase as vtLgBase

import sys,types,tempfile,os,cStringIO,binascii
import wx.tools.img2img as img2img
import wx.tools.img2py as img2py

from vidarc.tool.xml.vtXmlDomConsumer import *
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.input.vtInputValue import vtInputValue

class vFileDropTarget(wx.FileDropTarget):
    def __init__(self, window):
        wx.FileDropTarget.__init__(self)
        self.window = window
    def OnDropFiles(self, x, y, filenames):
        for file in filenames:
            self.window.AddFile(file)
            pass

def create(parent):
    return vtInputAttrCfgMLDialog(parent)

[wxID_VTINPUTATTRCFGMLDIALOG, wxID_VTINPUTATTRCFGMLDIALOGCBADD, 
 wxID_VTINPUTATTRCFGMLDIALOGCBAPPLY, wxID_VTINPUTATTRCFGMLDIALOGCBBROWSE, 
 wxID_VTINPUTATTRCFGMLDIALOGCBCANCEL, wxID_VTINPUTATTRCFGMLDIALOGCBDEL, 
 wxID_VTINPUTATTRCFGMLDIALOGCBREMOVE, wxID_VTINPUTATTRCFGMLDIALOGCBSET, 
 wxID_VTINPUTATTRCFGMLDIALOGCHCIMG, wxID_VTINPUTATTRCFGMLDIALOGCHCTYPE, 
 wxID_VTINPUTATTRCFGMLDIALOGLBLATTR, wxID_VTINPUTATTRCFGMLDIALOGLBLIMG, 
 wxID_VTINPUTATTRCFGMLDIALOGLBLNAME, wxID_VTINPUTATTRCFGMLDIALOGLBLTRANS, 
 wxID_VTINPUTATTRCFGMLDIALOGLBLTYPE, wxID_VTINPUTATTRCFGMLDIALOGLSTATTR, 
 wxID_VTINPUTATTRCFGMLDIALOGTXTNAME, wxID_VTINPUTATTRCFGMLDIALOGVIATTR, 
 wxID_VTINPUTATTRCFGMLDIALOGVIATTRVAL, wxID_VTINPUTATTRCFGMLDIALOGVILGSEL, 
 wxID_VTINPUTATTRCFGMLDIALOGVITRANS, 
] = [wx.NewId() for _init_ctrls in range(21)]

class vtInputAttrCfgMLDialog(wx.Dialog,vtXmlDomConsumer):
    VERBOSE=0
    def _init_coll_bxsName_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblName, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtName, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsAttr_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblAttr, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.viAttr, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.viAttrVal, 1, border=4, flag=wx.EXPAND | wx.LEFT)
        parent.AddWindow(self.cbSet, 0, border=4, flag=wx.LEFT)

    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(6)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsName, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsTrans, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsImg, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsType, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsAttr, 1, border=4,
              flag=wx.TOP | wx.EXPAND | wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsAttrVal, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsBt2, 0, border=4,
              flag=wx.BOTTOM | wx.ALIGN_CENTER | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.ALIGN_CENTER | wx.TOP | wx.BOTTOM | wx.RIGHT | wx.LEFT)

    def _init_coll_bxsTrans_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblTrans, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.viTrans, 2, border=4, flag=wx.EXPAND | wx.LEFT)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(16, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_bxsBt2_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbAdd, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(16, 8), border=0, flag=0)
        parent.AddWindow(self.cbRemove, 0, border=0, flag=0)

    def _init_coll_bxsImg_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblImg, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.chcImg, 2, border=4, flag=wx.EXPAND | wx.LEFT)
        parent.AddWindow(self.cbBrowse, 0, border=4, flag=wx.LEFT)

    def _init_coll_bxsBtMisc_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbDel, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 16), border=0, flag=0)
        parent.AddWindow(self.viLgSel, 0, border=0, flag=0)

    def _init_coll_bxsAttrVal_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstAttr, 1, border=4, flag=wx.EXPAND)
        parent.AddSizer(self.bxsBtMisc, 0, border=4, flag=wx.TOP | wx.LEFT)

    def _init_coll_bxsType_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblType, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.chcType, 2, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddSpacer(wx.Size(31, 8), border=4, flag=wx.LEFT)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=9, vgap=0)

        self.bxsName = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsTrans = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsType = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsAttr = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsAttrVal = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBtMisc = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsImg = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBt2 = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsName_Items(self.bxsName)
        self._init_coll_bxsTrans_Items(self.bxsTrans)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsType_Items(self.bxsType)
        self._init_coll_bxsAttr_Items(self.bxsAttr)
        self._init_coll_bxsAttrVal_Items(self.bxsAttrVal)
        self._init_coll_bxsBtMisc_Items(self.bxsBtMisc)
        self._init_coll_bxsImg_Items(self.bxsImg)
        self._init_coll_bxsBt2_Items(self.bxsBt2)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTINPUTATTRCFGMLDIALOG,
              name=u'vtInputAttrCfgMLDialog', parent=prnt, pos=wx.Point(463,
              161), size=wx.Size(277, 355),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE,
              title=u'vtInputAttr Config ML Dialog')
        self.SetClientSize(wx.Size(269, 328))

        self.lblName = wx.StaticText(id=wxID_VTINPUTATTRCFGMLDIALOGLBLNAME,
              label=_(u'Name'), name=u'lblName', parent=self, pos=wx.Point(4,
              4), size=wx.Size(87, 21), style=wx.ALIGN_RIGHT)
        self.lblName.SetMinSize(wx.Size(-1, -1))

        self.txtName = vidarc.tool.input.vtInputTag.vtInputTag(id=wxID_VTINPUTATTRCFGMLDIALOGTXTNAME,
              name=u'txtName', parent=self, pos=wx.Point(95, 4),
              size=wx.Size(170, 21), style=0)
        self.txtName.SetMinSize(wx.Size(-1, -1))

        self.viTrans = vidarc.tool.input.vtInputTextML.vtInputTextML(id=wxID_VTINPUTATTRCFGMLDIALOGVITRANS,
              name=u'viTrans', parent=self, pos=wx.Point(95, 29),
              size=wx.Size(170, 30), style=0)

        self.lblTrans = wx.StaticText(id=wxID_VTINPUTATTRCFGMLDIALOGLBLTRANS,
              label=_(u'Translation'), name=u'lblTrans', parent=self,
              pos=wx.Point(4, 29), size=wx.Size(87, 30), style=wx.ALIGN_RIGHT)
        self.lblTrans.SetMinSize(wx.Size(-1, -1))

        self.lblType = wx.StaticText(id=wxID_VTINPUTATTRCFGMLDIALOGLBLTYPE,
              label=_(u'Type'), name=u'lblType', parent=self, pos=wx.Point(4,
              105), size=wx.Size(75, 21), style=wx.ALIGN_RIGHT)
        self.lblType.SetMinSize(wx.Size(-1, -1))

        self.chcImg = wx.Choice(choices=[],
              id=wxID_VTINPUTATTRCFGMLDIALOGCHCIMG, name=u'chcImg', parent=self,
              pos=wx.Point(83, 71), size=wx.Size(146, 21), style=0)
        self.chcImg.SetMinSize(wx.Size(-1, -1))
        self.chcImg.Bind(wx.EVT_CHOICE, self.OnChcImgChoice,
              id=wxID_VTINPUTATTRCFGMLDIALOGCHCIMG)

        self.cbBrowse = wx.lib.buttons.GenBitmapButton(ID=wxID_VTINPUTATTRCFGMLDIALOGCBBROWSE,
              bitmap=vtArt.getBitmap(vtArt.Invisible), name=u'cbBrowse',
              parent=self, pos=wx.Point(233, 71), size=wx.Size(31, 30),
              style=0)
        self.cbBrowse.Bind(wx.EVT_BUTTON, self.OnCbBrowseButton,
              id=wxID_VTINPUTATTRCFGMLDIALOGCBBROWSE)

        self.chcType = wx.Choice(choices=[],
              id=wxID_VTINPUTATTRCFGMLDIALOGCHCTYPE, name=u'chcType',
              parent=self, pos=wx.Point(83, 105), size=wx.Size(146, 21),
              style=0)
        self.chcType.SetMinSize(wx.Size(-1, -1))
        self.chcType.Bind(wx.EVT_CHOICE, self.OnChcTypeChoice,
              id=wxID_VTINPUTATTRCFGMLDIALOGCHCTYPE)

        self.lblAttr = wx.StaticText(id=wxID_VTINPUTATTRCFGMLDIALOGLBLATTR,
              label=_(u'Attribute'), name=u'lblAttr', parent=self,
              pos=wx.Point(4, 130), size=wx.Size(75, 30), style=wx.ALIGN_RIGHT)
        self.lblAttr.SetMinSize(wx.Size(-1, -1))

        self.viAttr = vidarc.tool.input.vtInputText.vtInputText(id=wxID_VTINPUTATTRCFGMLDIALOGVIATTR,
              name=u'viAttr', parent=self, pos=wx.Point(83, 130),
              size=wx.Size(71, 30), style=0)

        self.viAttrVal = vidarc.tool.input.vtInputText.vtInputText(id=wxID_VTINPUTATTRCFGMLDIALOGVIATTRVAL,
              name=u'viAttrVal', parent=self, pos=wx.Point(158, 130),
              size=wx.Size(71, 30), style=0)

        self.lstAttr = wx.ListCtrl(id=wxID_VTINPUTATTRCFGMLDIALOGLSTATTR,
              name=u'lstAttr', parent=self, pos=wx.Point(4, 164),
              size=wx.Size(226, 80), style=wx.LC_REPORT)
        self.lstAttr.SetMinSize(wx.Size(-1, -1))
        self.lstAttr.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstAttrListColClick,
              id=wxID_VTINPUTATTRCFGMLDIALOGLSTATTR)
        self.lstAttr.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstAttrListItemDeselected,
              id=wxID_VTINPUTATTRCFGMLDIALOGLSTATTR)
        self.lstAttr.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstAttrListItemSelected,
              id=wxID_VTINPUTATTRCFGMLDIALOGLSTATTR)

        self.cbSet = wx.lib.buttons.GenBitmapButton(ID=wxID_VTINPUTATTRCFGMLDIALOGCBSET,
              bitmap=vtArt.getBitmap(vtArt.ApplyAttr), name=u'cbSet',
              parent=self, pos=wx.Point(233, 130), size=wx.Size(31, 30),
              style=0)
        self.cbSet.Bind(wx.EVT_BUTTON, self.OnCbSetButton,
              id=wxID_VTINPUTATTRCFGMLDIALOGCBSET)

        self.cbDel = wx.lib.buttons.GenBitmapButton(ID=wxID_VTINPUTATTRCFGMLDIALOGCBDEL,
              bitmap=vtArt.getBitmap(vtArt.DelAttr), name=u'cbDel', parent=self,
              pos=wx.Point(234, 168), size=wx.Size(31, 30), style=0)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VTINPUTATTRCFGMLDIALOGCBDEL)

        self.viLgSel = vidarc.tool.lang.vtLgSelector.vtLgSelector(id=wxID_VTINPUTATTRCFGMLDIALOGVILGSEL,
              name=u'viLgSel', parent=self, pos=wx.Point(234, 214),
              size=wx.Size(30, 30), style=0)

        self.lblImg = wx.StaticText(id=wxID_VTINPUTATTRCFGMLDIALOGLBLIMG,
              label=u'Image', name=u'lblImg', parent=self, pos=wx.Point(4, 71),
              size=wx.Size(75, 30), style=wx.ALIGN_RIGHT)
        self.lblImg.SetMinSize(wx.Size(-1, -1))

        self.cbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTINPUTATTRCFGMLDIALOGCBADD,
              bitmap=vtArt.getBitmap(vtArt.Add), label=_(u'Add'), name=u'cbAdd', parent=self,
              pos=wx.Point(50, 256), size=wx.Size(76, 30), style=0)
        self.cbAdd.SetMinSize(wx.Size(-1, -1))
        self.cbAdd.Bind(wx.EVT_BUTTON, self.OnCbAddButton,
              id=wxID_VTINPUTATTRCFGMLDIALOGCBADD)

        self.cbRemove = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTINPUTATTRCFGMLDIALOGCBREMOVE,
              bitmap=vtArt.getBitmap(vtArt.Del), label=_(u'Delete'), name=u'cbRemove',
              parent=self, pos=wx.Point(142, 256), size=wx.Size(76, 30),
              style=0)
        self.cbRemove.SetMinSize(wx.Size(-1, -1))
        self.cbRemove.Bind(wx.EVT_BUTTON, self.OnCbRemoveButton,
              id=wxID_VTINPUTATTRCFGMLDIALOGCBREMOVE)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTINPUTATTRCFGMLDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(142, 294),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.SetMinSize(wx.Size(-1, -1))
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VTINPUTATTRCFGMLDIALOGCBCANCEL)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTINPUTATTRCFGMLDIALOGCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'),
              name=u'cbApply', parent=self, pos=wx.Point(50, 294),
              size=wx.Size(76, 30), style=0)
        self.cbApply.SetMinSize(wx.Size(-1, -1))
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VTINPUTATTRCFGMLDIALOGCBAPPLY)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vtInput')
        self._init_ctrls(parent)
        vtXmlDomConsumer.__init__(self)
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.selIdx=-1
        self.lang='en'
        self.viTrans.SetTagName('name')
        self.dType={}
        l=vtInputValue.TYPES
        for s in l:
            self.chcType.Append(s)
        self.chcType.SetSelection(0)
        ArtIDs = [ "wx.ART_ADD_BOOKMARK",
           "wx.ART_DEL_BOOKMARK",
           "wx.ART_HELP_SIDE_PANEL",
           "wx.ART_HELP_SETTINGS",
           "wx.ART_HELP_BOOK",
           "wx.ART_HELP_FOLDER",
           "wx.ART_HELP_PAGE",
           "wx.ART_GO_BACK",
           "wx.ART_GO_FORWARD",
           "wx.ART_GO_UP",
           "wx.ART_GO_DOWN",
           "wx.ART_GO_TO_PARENT",
           "wx.ART_GO_HOME",
           "wx.ART_FILE_OPEN",
           "wx.ART_FILE_SAVE",
           "wx.ART_FILE_SAVE_AS",
           "wx.ART_PRINT",
           "wx.ART_HELP",
           "wx.ART_TIP",
           "wx.ART_REPORT_VIEW",
           "wx.ART_LIST_VIEW",
           "wx.ART_NEW_DIR",
           "wx.ART_HARDDISK",
           "wx.ART_FLOPPY",
           "wx.ART_CDROM",
           "wx.ART_REMOVABLE",
           "wx.ART_FOLDER",
           "wx.ART_FOLDER_OPEN",
           "wx.ART_GO_DIR_UP",
           "wx.ART_EXECUTABLE_FILE",
           "wx.ART_NORMAL_FILE",
           "wx.ART_TICK_MARK",
           "wx.ART_CROSS_MARK",
           "wx.ART_ERROR",
           "wx.ART_QUESTION",
           "wx.ART_WARNING",
           "wx.ART_INFORMATION",
           "wx.ART_MISSING_IMAGE",
           "wx.ART_COPY",
           "wx.ART_CUT",
           "wx.ART_PASTE",
           "wx.ART_DELETE",
           "wx.ART_NEW",
           "wx.ART_UNDO",
           "wx.ART_REDO",
           "wx.ART_QUIT",
           "wx.ART_FIND",
           "wx.ART_FIND_AND_REPLACE",
           ]
        for s in vtArt.lNames:
            self.chcImg.Append('vtArt.'+s[3:-6])
        for s in ArtIDs:
            self.chcImg.Append(s)
        self.Layout()
        self._bShown=True
    def Show(self,flag):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self._bShown=flag
        wx.Dialog.Show(self,flag)
    def ShowModal(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self._bShown=True
        try:
            return wx.Dialog.ShowModal(self)
        finally:
            self._bShown=False
    def Clear(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        vtXmlDomConsumer.Clear(self)
        self.sImg=''
        self.dType={}
        self.txtName.SetValue('')
        self.viAttr.Clear()
        self.viAttrVal.Clear()
        self.viTrans.Clear()
        self.lstAttr.DeleteAllItems()
        self.cbBrowse.SetBitmapLabel(vtArt.getBitmap(vtArt.Invisible))
    def IsBusy(self):
        return self._bShown
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        return self.IsShown()
    def SetDoc(self,doc):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        vtXmlDomConsumer.SetDoc(self,doc)
        self.viTrans.SetDoc(doc)
        
        self.lstAttr.ClearAll()
        langIds=self.doc.GetLanguageIds()
        col=2
        self.lstAttr.InsertColumn(col=0,heading='attr')
        self.lstAttr.InsertColumn(col=1,heading='')
        for lang in langIds:
            self.lstAttr.InsertColumn(col=col,heading=lang)
            col+=1
        self.lang=self.doc.GetLang()
        self.viLgSel.SetDoc(self.doc)
    def __isAttrML__(self,sType,k):
        bML=False
        if sType in ['string']:
            #if k in ['val','unit']:
            #    bML=True
            if k in ['unit']:
                bML=True
        elif sType in ['text']:
            if k in ['val','unit']:
                bML=True
            if k in ['unit']:
                bML=True
        elif sType in ['int','long','float','pseudofloat']:
            if k in ['unit']:
                bML=True
        elif sType=='choice':
            if k in ['unit']:
                bML=True
            #bML=False
        elif sType=='choiceML':
            bML=True
        return bML
    def SetNode(self,node):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.Clear()
        vtXmlDomConsumer.SetNode(self,node)
        try:
            self.cbApply.Enable(True)
            if self.node is None:
                self.cbRemove.Enable(False)
                nodeTmp=None
            else:
                self.cbRemove.Enable(True)
                sTag=self.doc.getNodeText(self.node,'tag')
                self.txtName.SetValue(sTag)
                self.viTrans.SetNode(self.node)
                self.sImg=self.doc.getNodeText(self.node,'img')
                self.__setImgData__()
                langIds=self.doc.GetLanguageIds()
                nodeTmp=self.doc.getChild(self.node,sTag)
            sType='string'
            if nodeTmp is not None:
                for c in self.doc.getChilds(nodeTmp):
                    sTag=self.doc.getTagName(c)
                    if sTag=='type':
                        sType=self.doc.getText(c)
                        continue
                    sLang=self.doc.getAttribute(c,'language')
                    if self.dType.has_key(sTag):
                        if len(sLang)>0:
                            self.dType[sTag][sLang]=self.doc.getText(c)
                        else:
                            self.dType[sTag]=self.doc.getText(c)
                    else:
                        if len(sLang)>0:
                            self.dType[sTag]={sLang:self.doc.getText(c)}
                        else:
                            self.dType[sTag]=self.doc.getText(c)
            else:
                self.chcType.SetStringSelection(sType)
                self.OnChcTypeChoice(None)
                return
            self.chcType.SetStringSelection(sType)
            keys=self.dType.keys()
            keys.sort()
            for k in keys:
                idx=self.lstAttr.InsertStringItem(sys.maxint,k)
                bML=self.__isAttrML__(sType,k)
                if bML:
                    if type(self.dType[k])!=types.DictionaryType:
                        sVal=self.dType[k]
                        d={}
                        for lang in langIds:
                            d[lang]=sVal
                        self.dType[k]=d
                else:
                    if type(self.dType[k])==types.DictionaryType:
                        self.dType[k]=''
                
                if type(self.dType[k])==types.DictionaryType:
                    d=self.dType[k]
                    col=2
                    for lang in langIds:
                        if d.has_key(lang):
                            self.lstAttr.SetStringItem(idx,col,d[lang])
                        col+=1
                else:
                    col=1
                    self.lstAttr.SetStringItem(idx,col,self.dType[k])
        except:
            self.Clear()
            vtLog.vtLngTB(self.GetName())
    def GetNode(self,node=None):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        try:
            if node is None:
                node=self.node
            if node is None:
                return
            if self.doc is None:
                return
            sTagOld=self.doc.getNodeText(node,'tag')
            sTag=self.txtName.GetValue()
            sType=self.chcType.GetStringSelection()
            self.doc.setNodeText(node,'tag',sTag)
            self.doc.setNodeText(node,'img',self.sImg)
            self.viTrans.GetNode(node)
            if len(sTagOld)>0:
                tmp=self.doc.getChild(node,sTagOld)
                self.doc.deleteNode(tmp,node)
            tmp=self.doc.getChildForced(node,sTag)
            self.doc.setNodeText(tmp,'type',sType)
            keys=self.dType.keys()
            keys.sort()
            langIds=self.doc.GetLanguageIds()
            for k in keys:
                if type(self.dType[k])==types.DictionaryType:
                    d=self.dType[k]
                    for lang in langIds:
                        try:
                            sVal=d[lang]
                        except:
                            sVal=''
                        self.doc.setNodeTextLang(tmp,k,sVal,lang)
                else:
                    self.doc.setNodeText(tmp,k,self.dType[k])
            self.doc.AlignNode(node)
        except:
            vtLog.vtLngTB(self.GetName())
    def SetByCfgLst(self,sTag,lCfg):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        try:
            self.cbApply.Enable(False)
            self.txtName.SetValue(sTag)
            self.viTrans.SetValue(lCfg[0])
            langIds=self.doc.GetLanguageIds()
            dType=lCfg[3]
            sType=dType['type']
            self.dType={}
            for k,v in dType.iteritems():
                if k=='type':
                    continue
                self.dType[k]=v
            self.lstAttr.DeleteAllItems()
            self.chcType.SetStringSelection(sType)
            keys=self.dType.keys()
            keys.sort()
            for k in keys:
                idx=self.lstAttr.InsertStringItem(sys.maxint,k)
                bML=self.__isAttrML__(sType,k)
                if bML:
                    if type(self.dType[k])!=types.DictionaryType:
                        sVal=self.dType[k]
                        d={}
                        for lang in langIds:
                            d[lang]=sVal
                        self.dType[k]=d
                else:
                    if type(self.dType[k])==types.DictionaryType:
                        self.dType[k]=''
                
                if type(self.dType[k])==types.DictionaryType:
                    d=self.dType[k]
                    col=2
                    for lang in langIds:
                        if d.has_key(lang):
                            self.lstAttr.SetStringItem(idx,col,d[lang])
                        col+=1
                else:
                    col=1
                    self.lstAttr.SetStringItem(idx,col,self.dType[k])
        except:
            self.Clear()
            vtLog.vtLngTB(self.GetName())
    def GetTagName(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        return self.txtName.GetValue()
    def OnCbApplyButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.EndModal(1)
        event.Skip()

    def OnCbCancelButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.EndModal(0)
        event.Skip()

    def OnCbSetButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        event.Skip()
        try:
            k=self.viAttr.GetValue()
            idx=self.lstAttr.FindItem(-1,k)
            try:
                sType=self.chcType.GetStringSelection()
            except:
                sType='string'
            bML=self.__isAttrML__(sType,k)
            try:
                d=self.dType[k]
            except:
                if bML:
                    d={}
            sVal=self.viAttrVal.GetValue()
            if bML:
                d[self.viLgSel.GetValue()]=sVal
                self.dType[k]=d
            else:
                self.dType[k]=sVal
                
            if idx<0:
                idx=self.lstAttr.InsertStringItem(sys.maxint,k)
            if idx>=0:
                if self.VERBOSE:
                    print d,self.viLgSel.GetValue()
                if bML:
                    col=2
                    langIds=self.doc.GetLanguageIds()
                    lang=self.viLgSel.GetValue()
                    for s in langIds:
                        if s==lang:
                            self.lstAttr.SetStringItem(idx,col,sVal)
                            self.viAttr.Clear()
                            self.viAttrVal.Clear()
                            return
                        col+=1
                else:
                    col=1
                self.lstAttr.SetStringItem(idx,col,sVal)
        except:
            vtLog.vtLngTB(self.GetName())
        self.viAttr.Clear()
        self.viAttrVal.Clear()

    def OnCbDelButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        event.Skip()
        try:
            if self.selIdx>=0:
                k=self.lstAttr.GetItemText(self.selIdx)
                del self.dType[k]
                self.lstAttr.DeleteItem(self.selIdx)
                self.selIdx=-1
        except:
            vtLog.vtLngTB(self.GetName())

    def OnChcTypeChoice(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if event is not None:
            event.Skip()
        sType=self.chcType.GetStringSelection()
        self.lstAttr.DeleteAllItems()
        l=vtInputValue.TYPE_DICT[sType]
        lang=self.viLgSel.GetValue()
        dOld=self.dType
        langIds=self.doc.GetLanguageIds()
        self.dType={}
        for k in l:
            idx=self.lstAttr.InsertStringItem(sys.maxint,k)
            bML=self.__isAttrML__(sType,k)
            if bML:
                d={}
                col=2
                for lId in langIds:
                    try:
                        sVal=dOld[k]
                        if type(sVal)==types.DictionaryType:
                            try:
                                sVal=sVal[lId]
                            except:
                                sVal=''
                    except:
                        sVal=''
                    try:
                        self.lstAttr.SetStringItem(idx,col,sVal)
                    except:
                        sVal=''
                        self.lstAttr.SetStringItem(idx,col,sVal)
                    d[lId]=sVal
                    col+=1
                self.dType[k]=d
            else:
                try:
                    sVal=dOld[k]
                    if type(sVal)==types.DictionaryType:
                        try:
                            sVal=sVal[lang]
                        except:
                            sVal=sVal[sVal.keys()[0]]
                except:
                    sVal=''
                try:
                    self.lstAttr.SetStringItem(idx,1,sVal)
                except:
                    sVal=''
                    self.lstAttr.SetStringItem(idx,1,sVal)
                self.dType[k]=sVal
        
    def OnLstAttrListColClick(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.selIdx=-1
        self.viAttr.SetValue('')
        self.viAttrVal.SetValue('')
        event.Skip()

    def OnLstAttrListItemDeselected(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.selIdx=-1
        self.viAttr.SetValue('')
        self.viAttrVal.SetValue('')
        event.Skip()

    def OnLstAttrListItemSelected(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.viAttr.SetValue('')
        self.viAttrVal.SetValue('')
        try:
            self.selIdx=event.GetIndex()
            if self.selIdx>=0:
                k=self.lstAttr.GetItemText(self.selIdx)
                if self.VERBOSE:
                    vtLog.CallStack('')
                    print self.selIdx,k
                d=self.dType[k]
                if self.VERBOSE:
                    print d,self.viLgSel.GetValue()
                self.viAttr.SetValue(k)
                if type(d)==types.DictionaryType:
                    try:
                        sVal=d[self.viLgSel.GetValue()]
                    except:
                        sVal=''
                else:
                    sVal=d
                self.viAttrVal.SetValue(sVal)
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()
    def __setImgData__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        try:
            if len(self.sImg)!=0:
                if self.sImg[:3]=='wx.':
                    bmp = wx.ArtProvider_GetBitmap(eval(self.sImg), eval('wx.ART_TOOLBAR'), (16,16))
                elif self.sImg[:6]=='vtArt.':
                    bmp=vtArt.getBitmap(eval(self.sImg))
                else:
                    stream = cStringIO.StringIO(binascii.unhexlify(self.sImg))
                    bmp=wx.BitmapFromImage(wx.ImageFromStream(stream))
                self.cbBrowse.SetBitmapLabel(bmp)
        except:
            vtLog.vtLngTB(self.GetName())
            self.cbBrowse.SetBitmapLabel(vtArt.getBitmap(vtArt.Invisible))
    def AddFile(self,sFullFN):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        try:
            if self.VERBOSE:
                vtLog.CallStack('')
                print sFullFN
            compressed = 0
            maskClr = None
            tfname = tempfile.mktemp()
            ok, msg = img2img.convert(sFullFN, maskClr, None, tfname, wx.BITMAP_TYPE_PNG, ".png")
            if not ok:
                vtLog.vtLngCurWX(self.ERROR,msg,self)
            data = open(tfname, "rb").read()
            self.sImg=binascii.hexlify(data)
            os.unlink(tfname)
            if self.VERBOSE:
                print self.sImg
            self.__setImgData__()
        except:
            vtLog.vtLngTB(self.GetName())
            self.sImg=''
    def OnCbBrowseButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        dlg = wx.FileDialog(self, _("Import Image"), ".", "", _(u"PNG files (*.png)|*.png|all files (*.*)|*.*"), wx.OPEN)
        try:
            if dlg.ShowModal() == wx.ID_OK:
                sFN = dlg.GetPath()
                self.AddFile(sFN)
            return
        finally:
            dlg.Destroy()
        event.Skip()

    def OnChcImgChoice(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        event.Skip()
        s=self.chcImg.GetStringSelection()
        self.sImg=s
        if s[:3]=='wx.':
            bmp = wx.ArtProvider_GetBitmap(eval(s), eval('wx.ART_TOOLBAR'), (16,16))
        else:
            bmp=vtArt.getBitmap(eval(s))
        self.cbBrowse.SetBitmapLabel(bmp)
        self.cbBrowse.Refresh()

    def OnCbAddButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.EndModal(2)
        event.Skip()

    def OnCbRemoveButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.EndModal(3)
        event.Skip()

        
