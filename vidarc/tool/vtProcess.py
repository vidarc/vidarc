#----------------------------------------------------------------------------
# Name:         vtProcess.py
# Purpose:      process control and communication obecjt
#               provide a common interface
#
# Author:       Walter Obweger
#
# Created:      20080213
# CVS-ID:       $Id: vtProcess.py,v 1.10 2012/01/12 04:38:06 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os,stat,subprocess,errno,sys
import traceback,types,locale
import time._time as time
from vtBuffer import vtBuffer

PIPE = subprocess.PIPE

class vtProcess:
    def __init__(self,sCmd,shell=False,max=8096,reorganise=2048,zSleep=0.1,timeout=-1,
                detached=False):
        if type(sCmd)==types.UnicodeType:
            sCmd=sCmd.encode(locale.getpreferredencoding())
        if detached==True:
            self._p=subprocess.Popen(sCmd,shell=False,
                        stdin=None,stdout=None,
                        stderr=None)
            return 
        else:
            self._p=subprocess.Popen(sCmd,bufsize=512,shell=shell,
                        #universal_newlines=True,
                        #universal_newlines=False,
                        #close_fds=True,
                        stdin=subprocess.PIPE,stdout=subprocess.PIPE,
                        stderr=subprocess.PIPE)
        self._stdinfd=self._p.stdin.fileno()
        self._stdoutfd=self._p.stdout.fileno()
        self._stderrfd=self._p.stderr.fileno()
        self._bufout=vtBuffer(max=max,reorganise=reorganise)
        self._buferr=vtBuffer(max=max,reorganise=reorganise)
        self._iReorganise=reorganise
        self._Sleep=zSleep
        self._iTimeOut=timeout
        self._lTime=[0]*3
        self.lCB=[None]*3
    def SetCB(self,kind,separator,func,*args,**kwargs):
        if kind>=0 and kind<=2:
            self.lCB[kind]=(separator,func,args,kwargs)
    def Kill(self):
        sPlat=sys.platform
        if sPlat=='win32':
            try:
                subprocess.Popen('taskkill /F /T /PID %i'%(self._p.pid),
                        stdin=None,stdout=subprocess.PIPE,stderr=None)
                return 1
            except:
                traceback.print_exc()
                return -1
        return -100
    def __analyseBuf__(self,buf,t):
        if t is not None:
            separator,func,args,kwargs=t
            if separator is None:
                s=buf.pop()
                func(s,*args,**kwargs)
            else:
                i=0
                while i>=0:
                    i=buf.find(separator)
                    if i>=0:
                        s=buf.pop(i+1)
                        #print buf,i,s
                        func(s,*args,**kwargs)
        else:
            buf.clear()
    def __checkTimeOut__(self,i):
            if i==1:
                buf=self._bufout
            elif i==2:
                buf=self._buferr
            else:
                return
            if self._iTimeOut<0:
                return
            if self._lTime[i]>=self._iTimeOut:
                if buf.__getDataLen__()>0:
                    if self.lCB[i] is not None:
                        separator,func,args,kwargs=self.lCB[i]
                        func(buf.pop(),*args,**kwargs)
                        self._lTime[i]=0
    def communicate(self):
        try:
            bGotten=False
            self._lTime[1]+=1
            while self._stdoutfd>=0:
                if 0:
                    s=self._p.stdout.read(self._iReorganise)
                    #print s
                    iSz=len(s)
                    #print iSz
                    if iSz>0:
                        bGotten=True
                        self._lTime[1]=0
                        self._bufout.push(s)
                        self.__analyseBuf__(self._bufout,self.lCB[1])
                    else:
                        break
                    continue
                #os.fsync(self._stdoutfd)
                self._p.stdout.flush()
                #print self._stdoutfd
                #os.fsync(self._p.stdout)
                st=os.fstat(self._stdoutfd)
                iSz=st[stat.ST_SIZE]
                #print self._stdoutfd,iSz,st    ## check py25
                #s=self._p.stdout.read(self._iReorganise)
                #iSz=len(s)
                if iSz>0:
                    #print 'coomm','out',
                    #print iSz
                    bGotten=True
                    self._lTime[1]=0
                    ##iRd=0#min(iSz,self._iReorganise)
                    ##while iRd<iSz:
                    ##    iBlkSz=min(iSz-iRd,self._iReorganise)
                    ##    print '%4d,%4d,%4d,%4d'%(iRd,iBlkSz,iSz,self._iReorganise)
                    ##    self._bufout.push(self._p.stdout.read(iBlkSz))
                    ##    self.__analyseBuf__(self._bufout,self.lCB[1])
                    ##    iRd+=iBlkSz
                    ##print
                    ##break
                    if iSz>self._iReorganise:
                        #print 'cut',iSz,self._iReorganise
                        iSz=self._iReorganise
                    self._bufout.push(self._p.stdout.read(iSz))
                    #self._bufout.push(s)
                    self.__analyseBuf__(self._bufout,self.lCB[1])
                else:
                    break
            self.__checkTimeOut__(1)
            ##print 'timeout 1'
            self._lTime[2]+=1
            while self._stderrfd>=0:
                if 0:
                    s=self._p.stderr.read(self._iReorganise)
                    #print s
                    iSz=len(s)
                    #print iSz
                    if iSz>0:
                        bGotten=True
                        self._lTime[1]=0
                        self._buferr.push(s)
                        self.__analyseBuf__(self._buferr,self.lCB[2])
                    else:
                        break
                    continue
                #self._p.stderr.flush()
                st=os.fstat(self._stderrfd)
                iSz=st[stat.ST_SIZE]
                if iSz>0:
                    #print 'coomm','err',
                    #print iSz
                    bGotten=True
                    self._lTime[2]=0
                    if iSz>self._iReorganise:
                        iSz=self._iReorganise
                    self._buferr.push(self._p.stderr.read(iSz))
                    self.__analyseBuf__(self._buferr,self.lCB[2])
                else:
                    break
            self.__checkTimeOut__(2)
            ##print 'timeout 2'
        except:
            traceback.print_exc()
        return bGotten
    def communicateLoop(self):
        bRunning=True
        try:
            while self._p.poll() is None:
                self.communicate()
                time.sleep(self._Sleep)
        except:
            traceback.print_exc()
        try:
            i=10
            while i>0:
                if self.communicate():
                    #i=10
                    #print 'gotten'
                    i+=1
                    pass
                else:
                    i-=1
                time.sleep(0.025)
        except:
            traceback.print_exc()
        return
        try:
            self._bufout.push(self._p.stdout.read())
            self.__analyseBuf__(self._bufout,self.lCB[1])
            self._buferr.push(self._p.stderr.read())
            self.__analyseBuf__(self._buferr,self.lCB[2])
        except:
            traceback.print_exc()
    def isRunning(self):
        return self._p.returncode is None
    def checkRunning(self):
        return self._p.poll() is None
    def close(self):
        self._p.stdin.close()
        self._p.stdout.close()
        self._p.stderr.close()

