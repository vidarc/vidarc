#----------------------------------------------------------------------------
# Name:         vtRngBuf.py
# Purpose:      ring buffer object where items are customizable objects
#               thread safe
#
# Author:       Walter Obweger
#
# Created:      20090501
# CVS-ID:       $Id: vtRngBuf.py,v 1.6 2009/07/25 14:02:53 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vtThreadCore import getRLock

class vtRngBufItem:
    def __init__(self,*args):
        self.Clr()
    def Clr(self):
        pass
    def Set(self):
        pass
    def Get(self):
        return None

class vtRngBufItemFunc(vtRngBufItem):
    def Clr(self):
        self.func=None
        self.args=None
        self.kwargs=None
    def Set(self,func,*args,**kwargs):
        self.func=func
        self.args=args
        self.kwargs=kwargs
    def Get(self):
        return self.func,self.args,self.kwargs
    
class vtRngBuf:
    def __init__(self,size,cls,*args):
        self.iSize=size
        self.iAct=0
        self.iFill=0
        self.lSlot=[cls(*args) for i in xrange(self.iSize)]
        self.semAccess=getRLock()
    #def __next__(self):
    #    self.iAct= (self.iAct+1) % self.iSize
    def __str__(self):
        try:
            self.semAccess.acquire()
            return 'act:%d,fill:%d,size:%d,free:%d'%(self.iAct,self.iFill,
                    self.iSize,self.__free__())
        finally:
            self.semAccess.release()
    def __free__(self):
        return self.iSize-self.iFill
    def getFree(self):
        try:
            self.semAccess.acquire()
            return self.__free__()
        finally:
            self.semAccess.release()
    def isFree(self):
        return self.getFree()>0
    #def next(self):
    #    try:
    #        self.semAccess.acquire()
    #        self.__next__()
    #    finally:
    #        self.semAccess.release()
    def put(self,*args,**kwargs):
        try:
            self.semAccess.acquire()
            if self.__free__()>0:
                self.lSlot[(self.iAct+self.iFill)%self.iSize].Set(*args,**kwargs)
                self.iFill+=1
                return 0
            else:
                return -1
        finally:
            self.semAccess.release()
    def push(self,l):
        try:
            self.semAccess.acquire()
            if self.__free__()>=len(l):
                for args,kwargs in l:
                    self.lSlot[(self.iAct+self.iFill)%self.iSize].Set(*args,**kwargs)
                    self.iFill+=1
                return 0
            else:
                return -1
        finally:
            self.semAccess.release()
    def pop(self):
        try:
            self.semAccess.acquire()
            if self.iFill==0:
                return None
            r=self.lSlot[self.iAct].Get()
            self.lSlot[self.iAct].Clr()
            self.iAct=(self.iAct+1)%self.iSize
            self.iFill-=1
            return r
        finally:
            self.semAccess.release()
    def get(self,iPos=0):
        try:
            self.semAccess.acquire()
            if self.iFill==0:
                return None
            if iPos>=self.iSize:
                return None
            iAct=(self.iAct+iPos) % self.iSize
            return self.lSlot[iAct].Get()
        finally:
            self.semAccess.release()
    def getItem(self,iPos=0):
        try:
            self.semAccess.acquire()
            if iPos>self.iSize:
                return None
            iAct=(self.iAct+iPos) % self.iSize
            return self.lSlot[iAct]
        finally:
            self.semAccess.release()
    def proc(self,func,*args,**kwargs):
        try:
            self.semAccess.acquire()
            iAct=self.iAct
            for i in xrange(0,self.iSize):
                func(self.lSlot[iAct].Get(),*args,**kwargs)
                iAct+=1
                if iAct>=self.iSize:
                    iAct=0
        finally:
            self.semAccess.release()
    def empty(self):
        try:
            self.semAccess.acquire()
            iAct=self.iAct
            for i in xrange(self.iFill):
                self.lSlot[iAct].Clr()
                iAct+=1
                if iAct>=self.iSize:
                    iAct=0
            self.iFill=0
        finally:
            self.semAccess.release()
