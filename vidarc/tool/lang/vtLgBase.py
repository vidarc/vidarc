#----------------------------------------------------------------------------
# Name:         vtLgBase.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtLgBase.py,v 1.26 2012/05/09 21:24:19 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import os,os.path,sys,getopt
import gettext,traceback
import wx
import locale
import vSystem

import __init__
VERBOSE=0
VERBOSE_STD=0
VERBOSE_LOG=0

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
import vidarc.config.vcLog as vcLog
VERBOSE_STD=vcLog.is2Log(__name__,'__VERBOSE_STD__')
import vidarc.config.vcLog as vcLog
VERBOSE_LOG=vcLog.is2Log(__name__,'__VERBOSE_LOG__')

try:
    import vidarc.tool.log.vtLog as vtLog
    #vtLog.vtLngIsLogged(vtLog.DEBUG)
except:
    VERBOSE_LOG=0

MAP_LOCALE_ON_MSWIN={
    'en_US':'English_US',
    'de_DE':'German_Germany',
    'de_CH':'German_Switzerland',
    'de_AT':'German_Austria',
    }
    
APPL_BASE_DN=''
PLUGIN_BASE_DN={}
def getApplBaseDN():
    global APPL_BASE_DN
    if len(APPL_BASE_DN)==0:
        basepath = vSystem.getPlugInDN()
        localedir = vSystem.getPlugInLocaleDN()
        if 0:
            sTmp=os.getenv('vidarcLocaleDN')
            sTmp=None
            if sTmp is not None:
                localedir=sTmp
                basepath,sTmp=os.path.split(localedir)
            else:
                basepath = os.path.abspath(os.path.dirname(sys.argv[0]))
                localedir = os.path.join(basepath, "locale")
                if os.path.isdir(localedir)==False:
                    basepath=vSystem.getPlugInDN()
        APPL_BASE_DN=basepath
    if VERBOSE_LOG:
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'basepath:%s'%(APPL_BASE_DN),__name__)
    return APPL_BASE_DN
def getPlugInBaseDN(domain):
    global PLUGIN_BASE_DN
    if VERBOSE_LOG:
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'domain:%s;plugin:%s'%(domain,
                    vtLog.pformat(PLUGIN_BASE_DN)),__name__)
    if domain in PLUGIN_BASE_DN:
        return PLUGIN_BASE_DN[domain]
    elif None in PLUGIN_BASE_DN:
        return PLUGIN_BASE_DN[None]
    else:
        return 'en'
APPL_LANG='en'
def getApplLang():
    global APPL_LANG
    return APPL_LANG
def setApplLang(lang):
    global APPL_LANG
    APPL_LANG=lang
    setLocale(APPL_LANG)        # 070907:wro
APPL_DOMAIN={}
def getApplDomain():
    global APPL_DOMAIN
    return APPL_DOMAIN
def initAppl(optshort,optlang,domain):
    langid = wx.LANGUAGE_DEFAULT    # use OS default; or use LANGUAGE_JAPANESE, etc.
    lang=locale.getdefaultlocale()[0]
    try:
        optlist , args = getopt.getopt(sys.argv[1:],optshort,optlang)
        for o in optlist:
            if o[0] in ['--lang','-l']:
                lang=o[1]
            elif o[0] in ['--langlog']:
                global VERBOSE
                global VERBOSE_STD
                global VERBOSE_LOG
                try:
                    import vidarc.tool.log.vtLog as vtLog
                    iLv=int(o[1])
                    if iLv>1:
                        VERBOSE_LOG=1
                    if iLv>2:
                        VERBOSE_STD=1
                    if iLv>3:
                        VERBOSE=1
                except:
                    VERBOSE_LOG=0
                    traceback.print_exc()
    except:
        traceback.print_exc()
    ll=wx.Locale.FindLanguageInfo(lang)
    if ll is None:
        ll=wx.Locale.FindLanguageInfo('en')
    langid=ll.Language
    lang=lang[:2]
    #vtLog.CallStack('')
    #print 'log ver',VERBOSE_LOG
    #basepath = os.path.abspath(os.path.dirname(sys.argv[0]))
    #localedir = os.path.join(basepath, "locale")
    basepath = vSystem.getPlugInDN()
    localedir = vSystem.getPlugInLocaleDN()
    #if os.path.isdir(localedir)==False:
        #sTmp=os.getenv('vidarcLocaleDN')
    #    sTmp=vSystem.getPlugInLocaleDN()
    #    if sTmp is not None:
    #        localedir=sTmp
    #        basepath,sTmp=os.path.split(localedir)
    global APPL_LANG
    APPL_LANG=lang
    global APPL_BASE_DN
    APPL_BASE_DN=basepath
    global APPL_DOMAIN
    APPL_DOMAIN=domain
    global PLUGIN_BASE_DN
    PLUGIN_BASE_DN[None]=vSystem.getPlugInDN()    #basepath
    if VERBOSE_LOG:
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'basepath:%s'%(basepath),__name__)
                vtLog.vtLngCur(vtLog.DEBUG,'plugin:%s'%(vtLog.pformat(PLUGIN_BASE_DN)),__name__)
        except:
            VERBOSE_LOG=0
    # Set locale for wxWidgets
    if wx.GetApp() is not None:
        mylocale = wx.Locale(langid)
        mylocale.AddCatalogLookupPathPrefix(localedir)
        mylocale.AddCatalog(domain)

    # Set up Python's gettext
    mytranslation = gettext.translation(domain, localedir,
        #[mylocale.GetCanonicalName()], fallback = True)
        [lang], fallback = True)
    mytranslation.install(unicode=1)
    #locale.setlocale(locale.LC_ALL,'')
    setLocale(APPL_LANG)        # 070907:wro
def getPossibleLang(domain,basepath):
    if VERBOSE:
        traceback.print_stack()
        print domain,basepath
    if VERBOSE_STD:
        sys.stderr.write('+'*30)
        sys.stderr.write('\ngetPossibleLang\n')
        #sys.stderr.write(''.join(traceback.format_stack()))
        sys.stderr.write(' '.join(['\n    ',domain,basepath,'\n']))
    if VERBOSE_LOG:
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'domain:%s;basepath:%s'%(domain,basepath),__name__)
    langs=[]
    localedir = os.path.join(basepath, "locale")
    try:
        if os.path.isdir(localedir)==False:
            localedir = vSystem.getPlugInLocaleDN()
            basepath,sTmp=os.path.split(localedir)
        files=os.listdir(localedir)
        for fn in files:
            try:
                sFullFN=os.path.join(localedir,fn)
                if os.path.isdir(sFullFN):
                    if os.path.isdir(os.path.join(sFullFN,'LC_MESSAGES')):
                        langs.append(fn)
            except:
                pass
        if VERBOSE_STD:
            sys.stderr.write(' '.join(['    ']+langs+['\n']))
        if VERBOSE_LOG:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'langs:%s'%(vtLog.pformat(langs)),__name__)
    except:
        vtLog.vtLngTB(__name__)
    if VERBOSE_STD:
        sys.stderr.write('-'*30)
        sys.stderr.write('\n\n')
    return langs
def setPluginLang(domain,dn,lang):
    if VERBOSE:
        traceback.print_stack()
        print domain,dn,lang
    if VERBOSE_STD:
        sys.stderr.write('+'*30)
        sys.stderr.write('\nsetPluginLang\n')
        #sys.stderr.write(''.join(traceback.format_stack()))
        sys.stderr.write(' '.join(['    ',domain,`dn`,lang,'\n']))
    if VERBOSE_LOG:
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'domain:%s;dn:%s;lang:%s'%(domain,dn,lang),__name__)
    
    if 0:
        langid = wx.LANGUAGE_DEFAULT    # use OS default; or use LANGUAGE_JAPANESE, etc.
        if lang=='en':
            langid = wx.LANGUAGE_ENGLISH    # use OS default; or use LANGUAGE_JAPANESE, etc.
        elif lang=='de':
            langid = wx.LANGUAGE_GERMAN
        if langid==wx.LANGUAGE_ENGLISH:
            lang='en'
        elif langid==wx.LANGUAGE_GERMAN:
            lang='de'
        else:
            lang='en'
    try:
        if lang is None:
            lang=getApplLang()
    except:
        lang=locale.getdefaultlocale()[0]
        ll=wx.Locale.FindLanguageInfo(lang)
        if ll is None:
            wx.Locale.FindLanguageInfo('en')
        langid=ll.Language
        lang=lang[:2]
    global PLUGIN_BASE_DN
    if VERBOSE_LOG:
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'plugin:%s'%(vtLog.pformat(PLUGIN_BASE_DN)),__name__)
    if dn is None:
        try:
            dn=PLUGIN_BASE_DN[domain]
        except:
            if None in PLUGIN_BASE_DN:
                dn=PLUGIN_BASE_DN[None]
            else:
                dn = vSystem.getPlugInDN()
                #try:
                #    dn=os.path.abspath(os.path.dirname(sys.argv[0]))
                #except:
                #    dn=os.getcwd()
    else:
        PLUGIN_BASE_DN[domain]=dn
        if None not in PLUGIN_BASE_DN:
            PLUGIN_BASE_DN[None]=dn
    if VERBOSE_STD:
        sys.stderr.write(' '.join(['    ','act',domain,dn,lang,'\n']))
    # Set locale for wxWidgets
    #mylocale = wx.Locale(langid)
    #mylocale.AddCatalogLookupPathPrefix(localedir)
    #mylocale.AddCatalog(domain)

    # Set up Python's gettext
    try:
        if dn.endswith('locale'):
            localedir = dn
        else:
            localedir = os.path.join(dn, "locale")
        if VERBOSE_LOG:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'domain:%s;dn:%s;lang:%s;check'%(domain,localedir,lang),__name__)
        mytranslation = gettext.translation(domain, localedir,
                    [lang], fallback = False)
        if VERBOSE_LOG:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'domain:%s;dn:%s;lang:%s;found'%(domain,localedir,lang),__name__)
    except:
        #if VERBOSE_LOG:
        #vtLog.vtLngTB(__name__)
        try:
            localedir = vSystem.getPlugInLocaleDN()
            if 0:
                dnTmp=os.getenv('vidarcLocaleDN')
                if dnTmp is not None:
                    if len(dnTmp)>0:
                        dn=dnTmp
                if dn.endswith('locale'):
                    localedir = dn
                else:
                    localedir = os.path.join(dn, "locale")
            if VERBOSE_LOG:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'domain:%s;dn:%s;lang:%s;check'%(domain,localedir,lang),__name__)
            mytranslation = gettext.translation(domain, localedir,
                    [lang], fallback = True)
            if VERBOSE_LOG:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'domain:%s;dn:%s;lang:%s;found'%(domain,localedir,lang),__name__)
        except:
            if VERBOSE_LOG:
                vtLog.vtLngTB(__name__)
    #locale.setlocale(locale.LC_ALL,'')        # 070907:wro
    #setLocale()
    #traceback.print_stack()
    dnStore,sTmp=os.path.split(localedir)
    PLUGIN_BASE_DN[domain]=dnStore
    try:
        if VERBOSE_STD:
            sys.stderr.write(' '.join(['    ','domains']))
            sys.stderr.write(' '.join([':'.join([`k`,v]) for k,v in PLUGIN_BASE_DN.items()]))
            sys.stderr.write(' '.join(['\n']))
        if not hasattr(__init__,'PLUGIN_LANG_DICT'):
            __init__.PLUGIN_LANG_DICT={}
            __init__.PLUGIN_TRANS={}
        __init__.PLUGIN_LANG=lang
        __init__.PLUGIN_LANG_DICT[domain]=lang
        __init__.PLUGIN_TRANS[domain]=mytranslation
        if VERBOSE_STD:
            sys.stderr.write(' '.join(['    ','set']))
            sys.stderr.write(' '.join([':'.join([`k`,v]) for k,v in __init__.PLUGIN_LANG_DICT.items()]))
            sys.stderr.write(' '.join(['\n']))
        
        if VERBOSE_LOG:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'domain:%s;lang:%s;plugin:%s;trans:%s;dn:%s'%(domain,
                        __init__.PLUGIN_LANG,
                        vtLog.pformat(__init__.PLUGIN_LANG_DICT),
                        vtLog.pformat(__init__.PLUGIN_TRANS),
                        vtLog.pformat(PLUGIN_BASE_DN)),__name__)
        #__init__.PLUGIN_LANG=lang
        #__init__.PLUGIN_TRANS=mytranslation
        #__init__.PLUGIN_DOMAIN=domain
        #if VERBOSE_STD:
        #    sys.stderr.write(' '.join(['    ','set',__init__.PLUGIN_DOMAIN,__init__.PLUGIN_LANG,dn,__init__.__file__,'\n']))
    except:
        if VERBOSE_LOG:
            vtLog.vtLngTB(__name__)
        pass
    if VERBOSE_STD:
        sys.stderr.write('-'*30)
        sys.stderr.write('\n\n')
    return mytranslation.ugettext
def getPluginLang():
    try:
        if VERBOSE_STD:
            sys.stderr.write('+'*30)
            sys.stderr.write('\ngetPluginLang\n')
            #sys.stderr.write(''.join(traceback.format_stack()))
            sys.stderr.write(' '.join(['    ',__init__.PLUGIN_DOMAIN,__init__.PLUGIN_LANG,__init__.__file__,'\n']))
            sys.stderr.write(' '.join(['    ',__init__.PLUGIN_LANG,__init__.__file__,'\n']))
            try:
                sys.stderr.write(' '.join([':'.join([`k`,v]) for k,v in __init__.PLUGIN_LANG_DICT.items()]))
                sys.stderr.write('\n')
            except:
                sys.stderr.write(''.join(['      no plugin dict','\n']))
            sys.stderr.write('-'*30)
            sys.stderr.write('\n\n')
        return __init__.PLUGIN_LANG
    except:
        return 'en'
def setPluginDefaultLang(lang):
    try:
        if VERBOSE_STD:
            sys.stderr.write('+'*30)
            sys.stderr.write('\nsetPluginDefaultLang\n')
            #sys.stderr.write(''.join(traceback.format_stack()))
            sys.stderr.write(' '.join(['    ',lang,'\n']))
            sys.stderr.write(' '.join(['    ',__init__.PLUGIN_LANG,__init__.__file__,'\n']))
            try:
                sys.stderr.write(' '.join([':'.join([`k`,v]) for k,v in __init__.PLUGIN_LANG_DICT.items()]))
                sys.stderr.write('\n')
            except:
                sys.stderr.write(''.join(['      no plugin dict','\n']))
            sys.stderr.write('-'*30)
            sys.stderr.write('\n\n')
        __init__.PLUGIN_LANG=lang
    except:
        __init__.PLUGIN_LANG='en'
def getPluginDomain():
    try:
        if VERBOSE_STD:
            sys.stderr.write('+'*30)
            sys.stderr.write('\ngetPluginDomain\n')
            #sys.stderr.write(''.join(traceback.format_stack()))
            sys.stderr.write(' '.join(['    ',__init__.PLUGIN_DOMAIN,'\n']))
            sys.stderr.write('-'*30)
            sys.stderr.write('\n\n')
        return __init__.PLUGIN_DOMAIN
    except:
        return ''
def assignPluginLang(domain):
    if VERBOSE:
        traceback.print_stack()
    if VERBOSE_STD:
        sys.stderr.write('+'*30)
        sys.stderr.write('\nassignPluginLang domain:%s\n'%domain)
        #sys.stderr.write(''.join(traceback.format_stack()))
    try:
        #if VERBOSE_STD:
        #    sys.stderr.write('    ')
        #    sys.stderr.write(' '.join([':'.join([`k`,v]) for k,v in __init__.PLUGIN_LANG_DICT.items()]))
        #    sys.stderr.write('\n')
        #    sys.stderr.write('-'*30)
        #    sys.stderr.write('\n\n')
        try:
            lang=__init__.PLUGIN_LANG
        except:
            lang=getApplLang()
        try:
            if VERBOSE_LOG:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'domain:%s;lang:%s;dict:%s'%(domain,
                        lang,vtLog.pformat(__init__.PLUGIN_LANG_DICT)),__name__)
            if VERBOSE_STD:
                sys.stderr.write('domain:%s;lang:%s;dict:%s\n'%(domain,
                    lang,vtLog.pformat(__init__.PLUGIN_LANG_DICT)))
            if domain not in __init__.PLUGIN_LANG_DICT:
                setPluginLang(domain,None,lang)
            elif __init__.PLUGIN_LANG_DICT[domain]!=lang:
                setPluginLang(domain,None,lang)
        except:
            setPluginLang(domain,None,lang)
        return __init__.PLUGIN_TRANS[domain].ugettext
    except:
        if VERBOSE_STD:
            sys.stderr.write('-'*30)
            sys.stderr.write('\n\n')
        if VERBOSE_LOG:
            vtLog.vtLngTB(__name__)
        return _
def assignPluginLangOld():
    if VERBOSE:
        traceback.print_stack()
    if VERBOSE_STD:
        sys.stderr.write('+'*30)
        sys.stderr.write('\nassignPluginLang\n')
        #sys.stderr.write(''.join(traceback.format_stack()))
    try:
        if VERBOSE_STD:
            sys.stderr.write(' '.join(['    ',__init__.PLUGIN_DOMAIN,__init__.PLUGIN_LANG,__init__.__file__,getApplBaseDN(),'\n']))
            sys.stderr.write('-'*30)
            sys.stderr.write('\n\n')
        return __init__.PLUGIN_TRANS.ugettext
    except:
        if VERBOSE_STD:
            sys.stderr.write('-'*30)
            sys.stderr.write('\n\n')
        return _
class vtLangSetLocale(Exception):
    def __init__(self,value):
        self.value=value
    def __str__(self):
        return 'unknow locale:'+self.value
def setLocale(str=None):
    #import vidarc.tool.log.vtLog as vtLog
    #vtLog.CallStack(str)
    #print 'setLocale',str
    if str is None:
        #locale.setlocale(locale.LC_ALL,str)
        locale.setlocale(locale.LC_ALL,'')
    elif len(str)==0:
        locale.setlocale(locale.LC_ALL,str)
    elif wx.Platform=='__WXMSW__':
        if str=='en':
            strWin='en_US'
        elif str=='de':
            strWin='de_DE'
        else:
            strWin=str
        #print strWin
        try:
            locale.setlocale(locale.LC_ALL,MAP_LOCALE_ON_MSWIN[strWin])
        except:
            raise vtLangSetLocale(strWin)
    else:
        locale.setlocale(locale.LC_ALL,str)
def format(fmt,val):
    return locale.format(fmt,val)
def getNumSeperatorsFromNumMask():
    ld=locale.localeconv()
    sDec=ld['decimal_point']
    sTho=ld['thousands_sep']
    if len(sTho)==0:
        if sDec=='.':
            sTho=','
        else:
            sTho='.'
    #import vidarc.tool.log.vtLog as vtLog
    #vtLog.CallStack('')
    #print sDec,sTho
    return sDec,sTho
