#----------------------------------------------------------------------
# Name:         vtLgSelector.py
# Purpose:      language selector control with popup window
#
# Author:       Walter Obweger
#
# Created:      20060425
# CVS-ID:       $Id: vtLgSelector.py,v 1.5 2012/05/28 22:42:12 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
from wx.lib.anchors import LayoutAnchors
import  wx.lib.scrolledpanel as scrolled

from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer 
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog

import string

#import images

# defined event for vtInputTextML item changed
wxEVT_VTLANG_SELECTION_CHANGED=wx.NewEventType()
vEVT_VTLANG_SELECTION_CHANGED=wx.PyEventBinder(wxEVT_VTLANG_SELECTION_CHANGED,1)
def EVT_VTLANG_SELECTION_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTINPUT_TEXTML_CHANGED,func)
class vtLangSelectionChanged(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTLANG_SELECTION_CHANGED(<widget_name>, self.OnChanged)
    """

    def __init__(self,obj,val):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTLANG_SELECTION_CHANGED)
        self.val=val
    def GetValue(self):
        return self.val
    

class vtLgSelectionTransientPopup(wx.Dialog):
    def __init__(self, parent, size,style,doc,name=''):
        self.SIZE_PN_SCROLL=wx.SystemSettings.GetMetric(wx.SYS_VSCROLL_X)
        wx.Dialog.__init__(self, parent,size=size,style=wx.RESIZE_BORDER)#wx.BORDER_SIMPLE)#|wx.STAY_ON_TOP)
        id=wx.NewId()
        wx.EVT_SIZE(self,self.OnSize)
        #st = wx.StaticText(self, -1,name,pos=(70,4))
        iBtsPerLine=4
        if doc is not None:
            langs=doc.GetLanguages()
            iCount=len(langs)
            rows=int(round((iCount/float(iBtsPerLine))+0.5))
            rows+=1
        else:
            langs=[]
            rows=1
        fgs=wx.FlexGridSizer(cols=iBtsPerLine, vgap=4, rows=rows,hgap=4)
        
        self.cbCancel = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Cancel), name=u'cbCancel',
              parent=self, pos=(0,0), size=(30,30), style=wx.BU_AUTODRAW)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              self.cbCancel)
        for i in range(iBtsPerLine):
            fgs.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        
        i=0
        self.dBt={}
        for tup in langs:
            id=wx.NewId()
            bt = wx.BitmapButton(id=id,
                bitmap=tup[2], name=u'cb'+tup[1],
                parent=self, pos=wx.Point(32, 00), size=wx.Size(28, 28),
                style=wx.BU_AUTODRAW)
            bt.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,bt)
            self.dBt[id]=tup[1]
            fgs.AddWindow(bt, 0, border=0, flag=0)
            i+=1
        for i in range(iBtsPerLine):
            fgs.AddGrowableCol(i)
        for i in range(rows):
            fgs.AddGrowableRow(i)
        self.SetSizer(fgs)
        self.Layout()
        
    def IsModified(self):
        return self.bMod
    def __clearBlock__(self):
        self.bBlock=False
    def __markModified__(self,flag=True,obj=None):
        def markObj(obj,flag):
            if flag:
                f=obj.GetFont()
                f.SetWeight(wx.FONTWEIGHT_BOLD)
                obj.SetFont(f)
            else:
                f=obj.GetFont()
                f.SetWeight(wx.FONTWEIGHT_NORMAL)
                obj.SetFont(f)
            obj.Refresh()
        if obj is not None:
            markObj(obj,flag)
        else:
            for obj in self.lstMarkObjs:
                markObj(obj,flag)
        self.bMod=flag
    def OnTextText(self,evt):
        if self.bBlock==False:
            self.__markModified__(True,evt.GetEventObject())
        evt.Skip()
    def OnSize(self,evt):
        iW,iH=self.GetSize()
        if iW<90:
            iW=90
        if iH<80:
            iH=80
        self.SetSize((iW,iH))
        evt.Skip()
    def OnCbCancelButton(self,evt):
        par=self.GetParent()
        par.Close()
        #self.Show(False)
    def OnCbApplyButton(self,evt):
        id=evt.GetId()
        try:
            lang=self.dBt[id]
            par=self.GetParent()
            par.__apply__(lang)
            
        except:
            vtLog.vtLngTB(self.GetName())
        #self.Show(False)
    
class vtLgSelector(wx.Panel,vtXmlDomConsumer):
    def __init__(self,*_args,**_kwargs):
        vtXmlDomConsumer.__init__(self)
        try:
            szCb=_kwargs['size']
        except:
            szCb=wx.Size(24,24)
            _kwargs['size']=szCb
        
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        self.SetAutoLayout(True)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        id=wx.NewId()
        self.cbPopup = wx.lib.buttons.GenBitmapToggleButton(ID=id,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbPopup',
              parent=self, pos=(0,0), size=szCb, style=wx.BU_AUTODRAW)
        bxs.AddWindow(self.cbPopup, 0, border=0, flag=wx.ALIGN_CENTER)
        self.SetSizer(bxs)
        self.cbPopup.SetBitmapLabel(vtArt.getBitmap(vtArt.LangML))
        self.cbPopup.SetBitmapSelected(vtArt.getBitmap(vtArt.LangML))
        self.cbPopup.Bind(wx.EVT_BUTTON,self.OnPopupButton,self.cbPopup)#id=id)
        self.popWin=None
        self.doc=None
        self.node=None
        self.tagName=None
        self.val=None
        self.bBlock=False
        self.bMod=False
        self.bEnableMark=True
    def __apply__(self,lang):
        try:
            langs=self.doc.GetLanguages()
            for tup in langs:
                if tup[1]==lang:
                    self.cbPopup.SetBitmapLabel(tup[2])
                    self.cbPopup.SetBitmapSelected(tup[2])
                    #self.cbPopup.Refresh()
                    self.val=lang
                    self.Close()
                    wx.PostEvent(self,vtLangSelectionChanged(self,lang))
                    return
        except:
            self.cbPopup.SetBitmapLabel(vtArt.getBitmap(vtArt.LangML))
            self.cbPopup.SetBitmapSelected(vtArt.getBitmap(vtArt.LangML))
            vtLog.vtLngTB(self.GetName())
    def SetEnableMark(self,flag):
        self.bEnableMark=flag
    def __markModified__(self,flag=True,store_mod=True):
        if self.bEnableMark==False:
            return
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'__markModified__;flag:%s'%(flag),
        #                origin=self.GetName())
        return
        if flag:
            f=self.txtVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_BOLD)
            self.txtVal.SetFont(f)
        else:
            f=self.txtVal.GetFont()
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
            self.txtVal.SetFont(f)
        if store_mod:
            self.bMod=flag
        self.txtVal.Refresh()
    def IsModified(self):
        return self.bMod
    def Close(self):
        self.cbPopup.SetValue(False)
        if self.popWin is not None:
            if self.popWin.IsShown():
                self.popWin.Show(False)
        
    def Clear(self):
        self.node=None
    def IsBusy(self):
        if self.popWin is not None:
            return self.popWin.IsShown()
        else:
            return False
    def Stop(self):
        if self.popWin is not None:
            self.popWin.Show(False)
    def SetDoc(self,doc):
        vtXmlDomConsumer.SetDoc(self,doc)
        try:
            if self.doc is not None:
                self.lang=self.doc.GetLang()
                self.__apply__(self.lang)
        except:
            vtLog.vtLngTB(self.GetName())
    def GetValue(self):
        return self.val
    def SetValue(self,val):
        self.__apply__(val)
    def SetTagName(self,tagName):
        self.tagName=tagName
    def SetNode(self,node,tagName=None):
        self.bBlock=True
        self.Clear()
        self.node=node
        if tagName is not None:
            self.tagName=tagName
        if self.doc is None:
            return
        if self.node is None:
            return
        if self.tagName is None:
            return
        #if self.popWin is not None:
        #    self.popWin.SetNode()
        self.__markModified__(False)
        wx.CallAfter(self.__clearBlock__)
    def GetNode(self,node=None):
        self.Close()
        if node is None:
            node=self.node
        if node is None:
            return
        return
        try:
            sVal=self.txtVal.GetValue()
            self.doc.setNodeTextLang(node,self.tagName,sVal,None)
            if self.popWin is not None:
                self.popWin.GetNode(node)
            self.__markModified__(False)
        except:
            vtLog.vtLngTB(self.GetName())
    def CloseOld(self):
        if self.cbPopup.GetValue()==True:
            if self.popWin is not None:
                self.popWin.Show(False)
    def __clearBlock__(self):
        self.bBlock=False
    def __getTagName__(self):
        return self.tagName
    def __createPopup__(self):
        if self.popWin is None:
            #sz=self.GetSize()
            #sz=(sz[0],sz[1]-20)
            sz=(90,90)
            self.popWin=vtLgSelectionTransientPopup(self,sz,wx.SIMPLE_BORDER,self.doc)
            #self.popWin.SetNode()
            #self.popWin.SetVal(self.txtVal.GetValue(),self.doc.GetLang())
        else:
            pass
    def OnPopupButton(self,evt):
        self.__createPopup__()
        if self.cbPopup.GetValue()==True:
            btn=self
            iX,iY = btn.ClientToScreen( (0,0) )
            iW,iH =  btn.GetSize()
            iPopW,iPopH=self.popWin.GetSize()
            iScreenW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            iScreenH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            if iX+iPopW>iScreenW:
                iX=iScreenW-iPopW-4
                if iX<0:
                    iX=0
            iY+=iH
            if iY+iPopH>iScreenH:
                iY=iScreenH-iPopH
                if iY<0:
                    iY=0
            
            self.popWin.Move((iX,iY))
            self.popWin.Show(True)
        else:
            self.popWin.Show(False)
        evt.Skip()
        
