#----------------------------------------------------------------------------
# Name:         vtLgDictML.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060422
# CVS-ID:       $Id: vtLgDictML.py,v 1.5 2015/07/24 08:11:29 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.lang.vtLgBase as vtLgBase
import os,gettext
#import sys,traceback

class vtLangError(Exception):
    def __init__(self,domain,dn,lang,msg):
        self.sDN=dn
        self.sDomain=domain
        self.lLang=lang
        self.sMsg=msg
    def __str__(self):
        return 'vtLang Error domain:%s lang:%s dn:%s message:%s'%(self.sDomain,
                repr(self.lLang),repr(self.sDN),self.sMsg)

class vtLgDictML:
    def __init__(self,domain,dn=None,langs=['en','de','fr']):
        try:
            if len(langs)>0:            # 20150425 wro:check before using
                self.sDftLang=langs[0]
            else:
                self.sDftLang='en'      # 20150425 wro:add FB
            self.dTrans={}
            #traceback.print_stack(file=sys.stderr)
            sCallDN=dn
            if dn is None:
                #dn=vtLgBase.getApplBaseDN()
                dn=vtLgBase.getPlugInBaseDN(domain)
                #sys.stderr.write('appl base dn:%s\n'%(dn))
            elif len(dn)==0:
                #dn=vtLgBase.getApplBaseDN()
                dn=vtLgBase.getPlugInBaseDN(domain)
            elif dn[0]=='.':
                dn=os.path.join(vtLgBase.getApplBaseDN(),dn)
                dn=os.path.join(vtLgBase.getPlugInBaseDN(),dn)
                #sys.stderr.write('appl base dn:%s\n'%(dn))
            if dn[0]=='<':
                dn=os.path.join(dn[1:], "locale")
            else:
                dn=os.path.join(dn, "locale")
            #sys.stderr.write('locale dn:%s\n'%(dn))
            for lang in langs:
                self.dTrans[lang]=gettext.translation(domain, dn,[lang])
        except:
            raise vtLangError(domain,dn,langs,'start DN:%s'%(repr(sCallDN)))
    def Get(self,msg,lang='en'):
        if self.dTrans.has_key(lang):
            return self.dTrans[lang].ugettext(msg)
        else:
            return self.dTrans[self.sDftLang].ugettext(msg)

