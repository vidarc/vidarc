#----------------------------------------------------------------------------
# Name:         vtDbTxtToMYS.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20150316
# CVS-ID:       $Id: vtDbTxtToMYS.py,v 1.1 2015/03/30 13:41:01 wal Exp $
# Copyright:    (c) 2015 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import os
    
    VERBOSE=vpc.getVerboseType(__name__)
    vpc.logDebug('VERBOSE:%d'%(VERBOSE),__name__)
except:
    vpc.logTB(__name__)

class vtDbTxtToMYS(vpc.vcOrg):
    def __init__(self,iVerbose=-1):
        vpc.vcOrg.__init__(self)
        self.SetOrigin('vtDbTxtToMYS')
        self.SetVerbose(max(iVerbose,VERBOSE))
        self.lCol=[]
        self.lLines=[]
    def setCol(self,lCol):
        self.lCol=lCol
    def rdFile(self,sFN,sSepTxt="'",sSepCol=","):
        try:
            bDbg=self.GetVerboseDbg(0)
            bDbgDeep=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('sFN:%s'%(sFN))
            self.lLines=[]
            fIn=open(sFN,'rb')
            lLines=fIn.readlines()
            lAct=None
            for l in lLines:
                if bDbg:
                    self.__logDebug__(l)
                #print l
                #lAct.append(l)
                bEnd=False
                if len(l)>2:
                    if bDbgDeep:
                        self.__logDebug__({-3:l[-3],-2:l[-2],-1:l[-1]})
                        self.__logDebug__({-3:ord(l[-3]),-2:ord(l[-2]),
                                -1:ord(l[-1])})
                    if l[-3]==sSepTxt:
                        bEnd=True
                    elif l[-3]==sSepCol:
                        bEnd=True
                if bEnd==True:
                    if lAct is None:
                        lAct=l[:-2]
                    else:
                        lAct=''.join([lAct,'\\r\\n',l[:-2]])
                    lCol=lAct.split(sSepCol)
                    lColFix=[]
                    for itCol in lCol:
                        if len(itCol)==0:
                            lColFix.append(''.join([sSepTxt,sSepTxt]))
                        else:
                            lColFix.append(itCol)
                    
                    self.lLines.append(sSepCol.join(lColFix))
                    lAct=None
                else:
                    if lAct is None:
                        lAct=l[:-2]
                    else:
                        lAct=''.join([lAct,'\\r\\n',l[:-2]])
        except:
            self.__logTB__()
    def wrFile(self,sFN,sTbl=None,sSepTxt="'",sSepCol=","):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('sFN:%s'%(sFN))
            fOt=open(sFN,'ab')
            if sTbl is None:
                sTbl=sFN[:-4]
            fOt.write(os.linesep)
            fOt.write('DROP TABLE IF EXISTS `%s`;'%(sTbl))
            fOt.write(os.linesep)
            fOt.write('CREATE TABLE `%s` ('%(sTbl))
            fOt.write(os.linesep)
            for itCol in self.lCol:
                if itCol[0] is None:
                    for s in itCol[1]:
                        fOt.write(s)
                        fOt.write(os.linesep)
                else:
                    fOt.write(' '.join(itCol))
                    fOt.write(',')
                    fOt.write(os.linesep)
            fOt.write(') ENGINE=MyISAM DEFAULT CHARSET=utf8;');
            fOt.write(os.linesep)
            fOt.write(os.linesep)
            fOt.write('/*!40000 ALTER TABLE `%s` DISABLE KEYS */;'%(sTbl))
            fOt.write(os.linesep)
            fOt.write('INSERT INTO `%s` ('%(sTbl))
            iOfs=0
            for itCol in self.lCol:
                if itCol[0] is None:
                    pass
                else:
                    if iOfs>0:
                        fOt.write(sSepCol)
                    fOt.write(itCol[0])
                    iOfs+=1
            fOt.write(') VALUES ')
            fOt.write(os.linesep)
            iCnt=len(self.lLines)
            iOfs=1
            for l in self.lLines:
                fOt.write('(')
                fOt.write(l)
                fOt.write(')')
                if iOfs<iCnt:
                    fOt.write(',')
                else:
                    fOt.write(';')
                iOfs+=1
                fOt.write(os.linesep)
            fOt.write('/*!40000 ALTER TABLE `%s` ENABLE KEYS */;'%(sTbl))
            fOt.write(os.linesep)
        except:
            self.__logTB__()

