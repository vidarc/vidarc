#----------------------------------------------------------------------------
# Name:         vtDbDatabase.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20070920
# CVS-ID:       $Id: vtDbDatabase.py,v 1.7 2013/06/26 10:10:30 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog

from vidarc.tool.vtThread import vtThread
import pyodbc

class vtDbDatabase(vtThread):
    def __init__(self,sDSN,sUsr=None,sPwd=None,origin=None):
        vtLog.vtLngCurCls(vtLog.DEBUG,'dsn:%s'%(sDSN),self)
        if origin is None:
            origin='vtDbDatabase:thdProc'
        vtThread.__init__(self,None,origin=origin)
        self.sDSN=sDSN
        self.sUsr=sUsr
        self.sPwd=sPwd
        self.dbConn=None
        #self.thdProc=vtThread(None,origin='vtDbSrv:thdProc')
        #self.semSrv=self.thdProc.getLock()
    def Start(self):
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
        try:
            vtThread.Start(self)
            self.Do(self._doConnect,self.sDSN,self.sUsr,self.sPwd)
            pass
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def Stop(self):
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
        try:
            self.Do(self._doDisConnect)
            self.Do(vtThread.Stop,self)
            pass
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def _doConnect(self,sDSN,sUsr,sPwd):
        self._acquire()
        try:
            lConn=['DSN=%s'%(sDSN)]
            if sUsr is not None:
                lConn.append('UID=%s;'%(sUsr))
            if sPwd is not None:
                lConn.append('PWD=%s'%(sPwd))
            
            self.dbConn=pyodbc.connect(';'.join(lConn))
            self.Post('proc',1,'Connect')
        except:
            vtLog.vtLngTB(self.GetOrigin())
            self.Post('proc',-1,'Connect')
        self._release()
    def _doDisConnect(self):
        self._acquire()
        try:
            self.dbConn.close()
            del self.dbConn
            self.Post('proc',1,'DisConnect')
        except:
            vtLog.vtLngTB(self.GetOrigin())
            self.Post('proc',-1,'DisConnect')
        self._release()
    def GetTables(self,func,*args,**kwargs):
        self.Do(self._doGetTables,None,None,None,func,*args,**kwargs)
    def GetTablesFiltered(self,tbl,catalog,schema,func,*args,**kwargs):
        self.Do(self._doGetTables,tbl,catalog,schema,func,*args,**kwargs)
    def _doGetTables(self,tbl,catalog,schema,func,*args,**kwargs):
        self._acquire()
        try:
            l=[]
            cur=self.dbConn.cursor()
            #l=[row.TABLE_NAME[:] for row in cur.tables()]      # 090630
            for row in cur.tables(tbl or '%',catalog or '%',schema or '%'):
                l.append(row.table_name[:])
                if func is not None:
                    func(row,*args,**kwargs)
            #l=[row.table_name[:] for row in cur.tables()]
            if func is not None:
                func(None,*args,**kwargs)
            self.Post('proc',';'.join(l),'GetTables')
        except:
            vtLog.vtLngTB(self.GetOrigin())
            self.Post('proc',-1,'GetTables')
        self._release()
    def GetColumns(self,tbl,catalog,schema,column,func,*args,**kwargs):
        self.Do(self._doGetColumns,tbl,catalog,schema,column,func,*args,**kwargs)
    def _doGetColumns(self,tbl,catalog,schema,column,func,*args,**kwargs):
        self._acquire()
        try:
            l=[]
            cur=self.dbConn.cursor()
            i=0
            print tbl,catalog,schema,column
            for c in cur.columns(tbl,catalog or '',schema or '',column or '%'):
                l.append([c.column_name])
                if func is not None:
                    func(c,*args,**kwargs)
                i+=1
            if func is not None:
                func(None,*args,**kwargs)
            self.Post('proc',';'.join(l),'GetColumns')
        except:
            vtLog.vtLngTB(self.GetOrigin())
            try:
                vtLog.vtLngCur(vtLog.CRITICAL,'tbl:%s'%(tbl),self.GetOrigin())
            except:
                vtLog.vtLngTB(self.GetOrigin())
            self.Post('proc',0,'GetColumns')
        self._release()
    def SqlRaw(self,statement,func,*args,**kwargs):
        self.Do(self._doSqlRaw,statement,func,*args,**kwargs)
    def _doSqlRaw(self,statement,func,*args,**kwargs):
        self._acquire()
        try:
            self.__logDebug__('statement;%s'%(statement))
            if len(statement.strip())>0:
                cur=self.dbConn.cursor()
                ret=cur.execute(statement)
                self.__logDebug__('exec;do call;%s'%(func))
            else:
                self.__logDebug__('skip;do call;%s'%(func))
            if func is not None:
                func(ret,*args,**kwargs)
                func(None,*args,**kwargs)
            else:
                pass
            self.__logDebug__('done'%())
        except:
            vtLog.vtLngTB(self.GetOrigin())
            try:
                vtLog.vtLngCur(vtLog.CRITICAL,'statement:%s'%(statement),self.GetOrigin())
            except:
                vtLog.vtLngTB(self.GetOrigin())
            self.Post('proc',0,'SqlRaw')
        self._release()
    def SelectRaw(self,statement,func,*args,**kwargs):
        self.Do(self._doSelectRaw,statement,func,*args,**kwargs)
    def _doSelectRaw(self,statement,func,*args,**kwargs):
        self._acquire()
        try:
            cur=self.dbConn.cursor()
            rows=cur.execute(statement).fetchall()
            if func is not None:
                for r in rows:
                    func(r,*args,**kwargs)
                func(None,*args,**kwargs)
            else:
                pass
        except:
            vtLog.vtLngTB(self.GetOrigin())
            try:
                vtLog.vtLngCur(vtLog.CRITICAL,'statement:%s'%(statement),self.GetOrigin())
            except:
                vtLog.vtLngTB(self.GetOrigin())
            self.Post('proc',0,'SelectRaw')
        self._release()
    def SelectUpdate(self,sTbl,statement,func,*args,**kwargs):
        self.Do(self._doSelectUpdate,sTbl,statement,func,*args,**kwargs)
    def _doSelectUpdate(self,sTbl,statement,func,*args,**kwargs):
        self._acquire()
        try:
            cur=self.dbConn.cursor()
            upd=self.dbConn.cursor()
            rows=cur.execute(statement).fetchall()
            if func is not None:
                for r in rows:
                    t=func(r,*args,**kwargs)
                    if t is not None:
                        lVal=t[0]
                        lFlt=t[1]
                        s=u"".join([u"update %s SET "%(sTbl),u','.join(lVal),' ',
                                'where ', ' and '.join(lFlt)])
                        cur.execute(s)
                func(None,*args,**kwargs)
            else:
                pass
        except:
            vtLog.vtLngTB(self.GetOrigin())
            try:
                vtLog.vtLngCur(vtLog.CRITICAL,'statement:%s'%(statement),self.GetOrigin())
            except:
                vtLog.vtLngTB(self.GetOrigin())
            self.Post('proc',0,'SelectRaw')
        self._release()
    def UpdateInsert(self,sTbl,d,keys4sel,keys4upd=None,keys4ins=None,tFunc=None):
        self.Do(self._doUpdateInsert,sTbl,d,keys4sel,keys4upd=keys4upd,
                keys4ins=keys4ins,tFunc=tFunc)
    def _doUpdateInsert(self,sTbl,d,keys4sel,keys4upd=None,keys4ins=None,tFunc=None):
        self._acquire()
        try:
            cur=self.dbConn.cursor()
            lFlt=[]
            for k in keys4sel:
                lFlt.append('='.join([k,d[k]]))
            rows=cur.execute(u'select count(*) from %s where %s'%(sTbl,
                    ' and '.join(lFlt))).fetchall()
            iLen=len(rows)
            if iLen==0:
                # insert
                lCol=[]
                lVal=[]
                if keys4ins is None:
                    for k,v in d.iteritems():
                        lCol.append(k)
                        lVal.append(d[k])
                        #lVal.append(u"%s='%s'"%(k,v))
                else:
                    for k in keys4ins:
                        if k in d:
                            lCol.append(k)
                            lVal.append(d[k])
                            #lVal.append(u"%s='%s'"%(k,d[k]))
                #s=u"".join([u"insert into %s SET "%(sTbl),u','.join(lVal)])
                vtLog.vtLngCur(vtLog.DEBUG,'d:%s;lCol:%s;lVal:%s'%(vtLog.pformat(d),
                        vtLog.pformat(lCol),vtLog.pformat(lVal)),
                        self.GetOrigin())
                s=u"".join([u"insert into %s ( "%(sTbl),
                        u','.join(lCol),
                        u') VALUES (',
                        u','.join(lVal),
                        u')'
                        ])
                vtLog.vtLngCur(vtLog.DEBUG,'d:%s;s:%s'%(vtLog.pformat(d),s),self.GetOrigin())
                cur.execute(s)
                
                #cur.commit()
                
                #cur.close()
                self.Post('proc',1,'UpdateInsert')
            elif iLen==1:
                # update
                lVal=[]
                if keys4upd is None:
                    for k,v in d.iteritems():
                        lVal.append(u"%s='%s'"%(k,v))
                else:
                    for k in keys4upd:
                        if k in d:
                            lVal.append(u"%s='%s'"%(k,d[k]))
                s=u"".join([u"update %s SET "%(sTbl),u','.join(lVal),' ',
                            'where ', ' and '.join(lFlt)])
                cur.execute(s)
                self.Post('proc',0,'UpdateInsert')
                if tFunc is not None:
                    try:
                        f,args,kwargs=tFunc
                        f(d,*args,**kwargs)
                    except:
                        vtLog.vtLngTB(self.GetOrigin())
            else:
                # should never happend
                pass
                self.Post('proc',-2,'UpdateInsert')
            pass
        except:
            vtLog.vtLngTB(self.GetOrigin())
            try:
                vtLog.vtLngCur(vtLog.CRITICAL,'d:%s'%(vtLog.pformat(d)),self.GetOrigin())
            except:
                vtLog.vtLngTB(self.GetOrigin())
            self.Post('proc',0,'UpdateInsert')
        self._release()
    def Commit(self):
        self.Do(self._doCommit)
    def _doCommit(self):
        self._acquire()
        try:
            self.dbConn.commit()
            self.Post('proc',0,'Commit')
        except:
            vtLog.vtLngTB(self.GetOrigin())
            self.Post('proc',-1,'Commit')
        self._release()
    def GetEscapeFilterStr(self,s):
        for t in [
                    ('_','\_'),
                    ('\\','\\\\'),
                    ('*','%'),
                    ('?','_'),
                    ]:
            s=s.replace(t[0],t[1])
        return s
