#----------------------------------------------------------------------------
# Name:         vtDbDatabaseCtrl.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20120317
# CVS-ID:       $Id: vtDbDatabaseCtrl.py,v 1.12 2015/01/21 12:51:26 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import fnmatch
    
    import vLang
    import imgCore as imgCore
    import imgStat as imgStat
    
    from vGuiPanelWX import vGuiPanelWX
    from vGuiFrmWX import vGuiFrmWX
    
    VERBOSE=vpc.getVerboseType(__name__)
    from vidarc.tool.db.vtDbDatabase import vtDbDatabase
except:
    vpc.logTB(__name__)

class vtDbDatabaseCtrl(vGuiPanelWX):
    def __initCtrl__(self,*args,**kwargs):
        global _
        _=vLang.assignPluginLang('db')
        lWid=[
            ('txtRd',   (0,0),(1,3),{'name':'txtStat','value':_(u'undefined')},None),
            ('tgPopup', (0,3),(1,1),{
                'name':'tgCfg',#'iArrange':-1,
                },None),
            ]
        vGuiPanelWX.__initCtrl__(self,lWid=lWid)
        
        p=vGuiFrmWX(parent=self.tgCfg.GetWid(),kind='popup',
                iArrange=0,widArrange=self.txtStat,bAnchor=True,
                lGrowRow=[(3,1),],
                lGrowCol=[(1,1),(3,1)],
                lWid=[
                        ('lblRg',   (0,0),(1,1),{'name':'lblDSN','label':_(u'DSN')},None),
                        ('txt',     (0,1),(1,1),{'name':'txtDSN','value':kwargs.get('dsn',u'')},None),
                        ('lblRg',   (0,2),(1,1),{'name':'lblDB','label':_(u'DB')},None),
                        ('txt',     (0,3),(1,1),{'name':'txtDB','value':kwargs.get('db',u'')},None),
                        ('cbBmp',   (0,4),(1,1),{'name':'cbConn',
                                'tip':_(u'connect to database'),
                                'bitmap':imgStat.getStartBitmap()},
                                {'btn':self.OnConnect}),
                        ('lblRg',   (1,0),(1,1),{'name':'lblUsr','label':_(u'user')},None),
                        ('txt',     (1,1),(1,1),{'name':'txtUsr','value':kwargs.get('usr',u'')},None),
                        ('lblRg',   (1,2),(1,1),{'name':'lblPwd','label':_(u'password')},None),
                        ('txtPwd',  (1,3),(1,1),{'name':'txtPwd','value':kwargs.get('pwd',u'')},None),
                        ('cbBmp',   (1,4),(1,1),{'name':'cbDisConn',
                                'tip':_(u'disconnect from database'),
                                'bitmap':imgStat.getStopBitmap()},
                                {'btn':self.OnDisConnect}),
                        ('lblRg',   (2,0),(1,1),{'name':'lblTbl','label':_(u'tables')},None),
                        ('txtRd',   (2,1),(1,1),{'name':'txtTblCnt','value':''},None),
                        ('cbBmp',   (2,4),(1,1),{'name':'cbGetTbl',
                                'tip':_(u'get tables'),
                                'bitmap':imgCore.getSearchBitmap()},
                                {'btn':self.OnGetTbl}),
                        ('lstCtrl', (3,0),(2,4),{'name':'lstTbl',
                                'cols':[
                                    ['',-3,20,0],
                                    [_(u'table'),-1,-1,1],
                                    [_(u'count'),-2,60,1],
                                    [_(u'prev'),-2,60,1],
                                    [_(u'diff'),-2,60,1],
                                    ],
                                'map':[('name',1),('cnt',2)],
                                'tip':_(u'tables')},
                                {
                                    'dblClk':   self.OnLstTblDblClk,
                                },
                                ),
                        ('szBoxVert',            (3,4),(1,1),{'name':'boxBtTbl',},[
                            ('cbBmp',   0,4,{'name':'cbGetTblCnt',
                                    'tip':_(u'get tables count'),
                                    'bitmap':imgCore.getSearchBitmap()},
                                    {'btn':self.OnGetTblCnt}),
                            ]),
                        ])
        self.tgCfg.SetWidPopup(p)
        self.oDB=None
        self.dTbl={}
        return [(3,1),],[(1,1),(2,1)]
    def GetDB(self):
        try:
            self.__logDebug__(''%())
            return self.oDB
        except:
            self.__logTB__()
        return None
    # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # + database processing
    def OnThdDbProc(self,evt):
        try:
            if self.IsMainThread()==False:
                return
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def OnThdDbAbort(self,evt):
        try:
            if self.IsMainThread()==False:
                return
            self.__logDebug__(''%())
            if self.oDB is None:
                self.txtStat.SetValue(_(u'disconnected.'))
                return
            if self.oDB.IsRunning():
                s=self.txtStat.GetValue()
                self.txtStat.SetValue(s+' '+_(u'failed.'))
            else:
                self.txtStat.SetValue(_(u'disconnected.'))
        except:
            self.__logTB__()
    def OnThdDbFin(self,evt):
        try:
            if self.IsMainThread()==False:
                return
            self.__logDebug__(''%())
            s=self.txtStat.GetValue()
            self.txtStat.SetValue(s+' '+_(u'done.'))
            if self.iStat==0:
                #self.__do_call__('doDbUse',self.sDBname)
                self.DoDbUse(self.sDB)
                self.iStat=1
            elif self.iStat==1:
                #self.doDbGetTbl()
                self.PostOk('connected',1)
                self.iStat=10
            elif self.iStat==10:
                self.PostOk('done',1)
        except:
            self.__logTB__()
    def SetValue(self,sDSN=None,sDB=None,sUsr=None,sPwd=None):
        try:
            if self.IsMainThread()==False:
                return
            widPop=self.tgCfg.GetWidPopup()
            if widPop is None:
                return
            if sDSN is not None:
                widPop.txtDSN.SetValue(sDSN)
            if sUsr is not None:
                widPop.txtUsr.SetValue(sUsr)
            if sPwd is not None:
                widPop.txtPwd.SetValue(sPwd)
            if sDB is not None:
                widPop.txtDB.SetValue(sDB)
        except:
            self.__logTB__()
    # - database processing
    # --------------------------------------------------------------------------------
    def prcConnect(self):
        try:
            self.oDB.Start()
        except:
            self.__logTB__()
    def DoConnect(self):
        try:
            if self.IsMainThread()==False:
                return
            if self.oDB is not None:
                self.oDB.Stop()
            widPop=self.tgCfg.GetWidPopup()
            sDSN=   widPop.txtDSN.GetValue()
            sUsr=   widPop.txtUsr.GetValue()
            sPwd=   widPop.txtPwd.GetValue()
            self.sDB=widPop.txtDB.GetValue()
            self.iStat=0
            self.oDB=vtDbDatabase(sDSN,sUsr,sPwd)
            #self.oDB.Start()
            self.txtStat.SetValue(_(u'connecting...'))
            self.oDB.SetPostWid(self.GetWid(),True)
            self.oDB.BindEvents(self.OnThdDbProc,self.OnThdDbAbort,
                        self.OnThdDbFin)
            self.oDB.Do(self.prcConnect)    # 120811:wro use thread -> connection timeout
        except:
            self.__logTB__()
    def DoDisConnect(self):
        try:
            if self.IsMainThread()==False:
                return
            if self.oDB is not None:
                self.oDB.Stop()
                self.oDB=None
        except:
            self.__logTB__()
    def OnConnect(self,evt):
        try:
            self.DoConnect()
        except:
            self.__logTB__()
    def OnDisConnect(self,evt):
        try:
            self.__logDebug__(''%())
            self.DoDisConnect()
        except:
            self.__logTB__()
    def OnTgCfgButton(self,evt):
        try:
            self.__logDebug__(''%())
            if self.tgCfg.GetValue()==True:
                self.popCfg.Show(True)
            else:
                self.popCfg.Show(False)
        except:
            self.__logTB__()
    def procGetTbl(self,tbl,dTbl,lstTbl,bDbg):
        try:
            self.__logDebug__(''%())
            if tbl is not None:
                if bDbg:
                    self.__logDebug__('tbl;name:%s;cat:%s;schema:%s;'\
                        'type:%s'%(tbl.table_name,tbl.table_cat,
                        tbl.table_schem,tbl.table_type))
                if tbl.table_type=='TABLE':
                    self.__logDebug__('tbl;name:%s'%(tbl.table_name))
                    d={'cnt':-1,'prev':-1,'diff':-1,
                        'cat':tbl.table_cat,
                        'schema':tbl.table_schem,
                        'tbl':tbl.table_name}
                    sN='.'.join([tbl.table_cat,
                            tbl.table_schem,tbl.table_name])
                    dTbl[sN]=d
                    self.oDB.CallBack(
                        #lstTbl.Insert,{-1:tbl.table_name,
                        #        0:-1,
                        #        'name':tbl.table_name,
                        #        'cnt':-1})
                        lstTbl.Insert,[[{'key':sN,'cat':tbl.table_cat,
                                'schema':tbl.table_schem,'tbl':tbl.table_name},
                                ['',sN,-1]]])
            else:
                if bDbg:
                    self.__logDebug__('finshed'%())
                self.Post('getTbl','fin')
        except:
            self.__logTB__()
    def OnGetTbl(self,evt):
        try:
            bDbg=False
            if VERBOSE>10:
                if self.__isLogDebug__():
                    bDbg=True
            self.dTbl={}
            widPop=self.tgCfg.GetWidPopup()
            lstTbl=widPop.lstTbl
            sDB=widPop.txtDB.GetValue()
            #print sDB
            #sDB='MII%'
            #sDB=sDB.replace('_','\_')
            #sDB=sDB.replace('_','%')
            #print sDB
            self.oDB.GetTablesFiltered(None,sDB,None,
                    self.procGetTbl,self.dTbl,lstTbl,bDbg)
        except:
            self.__logTB__()
    def DoGetTbl(self):
        try:
            self.OnGetTbl(None)
        except:
            self.__logTB__()
    def FindTbl(self,sMatch):
        try:
            lTbl=[]
            for s,v in self.dTbl.iteritems():
                if fnmatch.fnmatchcase(s,sMatch)==True:
                    lTbl.append([s,v])
            return lTbl
        except:
            self.__logTB__()
            return []
    def procGetTblCnt(self,r,dTbl,lstTbl,iCache,iPos,dKey,bDbg):
        try:
            if bDbg:
                self.__logDebug__(dKey)
            sN=dKey['key']
            d=dTbl.get(sN,{'cnt':0,'prev':0,'diff':0})
            if r is None:
                if bDbg:
                    self.__logDebug__(d)
                self.CallBack(
                    lstTbl.SetValue,[[None,['',sN,str(d['cnt']),str(d['prev']),str(d['diff'])]]],iPos)
            else:
                j=r.iCnt
                i=d['cnt']
                if i<0:
                    i=0
                d['prev']=i
                d['cnt']=j
                d['diff']=j-i
                if bDbg:
                    self.__logDebug__(d)
        except:
            self.__logTB__()
    def OnGetTblCnt(self,evt):
        try:
            self.__logDebug__(''%())
            bDbg=False
            if VERBOSE>10:
                if self.__isLogDebug__():
                    bDbg=True
            widPop=self.tgCfg.GetWidPopup()
            lstTbl=widPop.lstTbl
            lSel=lstTbl.GetSelected([-3,-2,-1])
            self.__logDebug__(lSel)
            for l in lSel:
                d=l[2]
                iCache=l[0]
                iPos=l[1]
                self.oDB.SqlRaw('use %s'%d['cat'],self.useResponse)
                #self.oDB.SqlRaw('select count(*) as iCnt from %s;'%(d['tbl']),
                #        self.procGetTblCnt,self.dTbl,lstTbl,iCache,iPos,d,bDbg)
                self.oDB.SelectRaw('SELECT COUNT(*) AS iCnt FROM [%s].[%s];'%(d['schema'],d['tbl']),
                        self.procGetTblCnt,self.dTbl,lstTbl,iCache,iPos,d,bDbg)
        except:
            self.__logTB__()
    def OnLstTblDblClk(self,evt):
        try:
            self.__logDebug__(''%())
            iIdx=evt.GetIndex()
            widPop=self.tgCfg.GetWidPopup()
            lstTbl=widPop.lstTbl
            t=lstTbl.GetItemTup(iIdx,-1)
            self.__logDebug__({'-1':t})
            sDB=t['cat']
            widPop.txtDB.SetValue(sDB)
            self.Post('db',sDB)
        except:
            self.__logTB__()
    # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # + use database
    def useResponse(self,r):
        try:
            self.__logDebug__(r)
        except:
            self.__logTB__()
    def DoDbUse(self,sName):
        try:
            self.__logDebug__('sName:%s'%(sName))
            if self.IsMainThread()==False:
                return
            if len(sName)==0:
                pass
            if len(sName)>0:
                self.txtStat.SetValue(_(u'current database:%s')%(sName))
                self.oDB.SqlRaw('use %s'%sName,self.useResponse)
        except:
            self.__logTB__()
    # - use database
    # --------------------------------------------------------------------------------
    # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # + commit
    def DoCommit(self):
        try:
            self.__logDebug__(''%())
            self.oDB.Commit()
        except:
            self.__logTB__()
    # - commit
    # --------------------------------------------------------------------------------
    # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # + escape filter string
    def GetEscapeFilterStr(self,s):
        return self.oDB.GetEscapeFilterStr(s)
    # - escape filter string
    # --------------------------------------------------------------------------------
    def SetStat(self,sMsg):
        if self.IsMainThread(bSilent=True)==False:
            self.oDB.CallBack(self.SetStat,sMsg)
        else:
            self.txtStat.SetValue(sMsg)
    def DoSqlRaw(self,statement,func,*args,**kwargs):
        self.oDB.SqlRaw(statement,func,*args,**kwargs)
    def DoSqlRawMsg(self,sMsg,statement,func,*args,**kwargs):
        self.SetStat(sMsg)
        self.oDB.SqlRaw(statement,func,*args,**kwargs)
    def DoSelectRaw(self,statement,func,*args,**kwargs):
        self.oDB.SelectRaw(statement,func,*args,**kwargs)
    def DoSelectRawMsg(self,sMsg,statement,func,*args,**kwargs):
        self.SetStat(sMsg)
        self.oDB.SelectRaw(statement,func,*args,**kwargs)
    def DoUpdateInsert(self,sTbl,d,keys4sel,keys4upd=None,keys4ins=None,tFunc=None):
        self.oDB.UpdateInsert(sTbl,d,keys4sel,
                keys4upd=keys4upd,keys4ins=keys4ins,tFunc=tFunc)
    def CallBack(self,func,*args,**kwargs):
        self.oDB.CallBack(func,*args,**kwargs)
    def DoCB(self,func,*args,**kwargs):
        self.oDB.DoCB(func,*args,**kwargs)
    def DoPrc(self,func,*args,**kwargs):
        self.oDB.Do(func,*args,**kwargs)
