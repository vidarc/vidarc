#----------------------------------------------------------------------------
# Name:         vtDbSrv.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20070920
# CVS-ID:       $Id: vtDbSrv.py,v 1.3 2008/03/16 22:20:21 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog

from vidarc.tool.db.vtDbDatabase import vtDbDatabase
from vidarc.tool.vtThreadCoreCyclic import vtThreadCoreCyclic

class vtDbSrv(vtDbDatabase):
    def __init__(self,server,port,socketClass,verbose=0):
        vtLog.vtLngCurCls(vtLog.DEBUG,'dsn:%s'%(port),self)
        vtDbDatabase.__init__(self,port,origin='vtDbSrv:thdProc')
        self.thdCyc=vtThreadCoreCyclic()
        #self.thdProc=vtThread(None,origin='vtDbSrv:thdProc')
        #self.semSrv=self.thdProc.getLock()
    def Close(self):
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
        try:
            pass
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def IsServing(self):
        return self.IsRunning()
    def ShutDown(self):
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetOrigin())
        try:
            pass
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def SetCyclic(self,fDelay,func,*args,**kwargs):
        self.thdCyc.SetDelay(fDelay)
        self.thdCyc.SetCB(func,*args,**kwargs)
        self.thdCyc.Stop()
        self.thdCyc.Start()
