#----------------------------------------------------------------------------
# Name:         vtThreadMixinWX.py
# Purpose:
# Author:       Walter Obweger
#
# Created:      20071020
# CVS-ID:       $Id: vtThreadMixinWX.py,v 1.14 2010/04/10 13:34:23 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

class vtThreadMixinWX:
    def __init__(self):
        self._semWid=self.getLock()
        self._semDone=self.getLock()
        self._retWidMan=None
        self._bDoWidgetDone=False
    def doWidgetManipulation(self,func,*args,**kwargs):
        if VERBOSE>0:
            if self.__isLogDebug__():
                self.__logDebug__('execute')
        try:
            self._retWidMan=func(*args,**kwargs)
        except:
            self.__logTB__()
        self._bDoWidgetDone=True
        self._semDone.release()
        if VERBOSE>0:
            if self.__isLogDebug__():
                self.__logDebug__('semDone releaseed')
        #self._semWid.release()
    def CallBackWX(self,func,*args,**kwargs):
        if VERBOSE>5:
            bDbg=self.__isLogDebug__()
        else:
            bDbg=False
        if self.IsMainThread():
            return func(*args,**kwargs)
        try:
            if bDbg:
                self.__logDebug__('%s;scheduled;args:%s;kwargs:%s'%(
                        func.__name__,self.__logFmtLm__(args),
                        self.__logFmtLm__(kwargs)))
            if VERBOSE>0:
                if self.__isLogDebug__():
                    self.__logDebug__('%s;scheduled'%(func.__name__))
        except:
            self.__logTB__()
        if bDbg:
            self.__logDebug__('semWid acquiring')
        self._semWid.acquire()
        if bDbg:
            self.__logDebug__('semWid acquired')
        self._retWidMan=None
        self._bDoWidgetDone=False
        if bDbg:
            self.__logDebug__('semDone acquiring')
        self._semDone.acquire()
        if bDbg:
            self.__logDebug__('semDone acquired')
        try:
            self.CallBack(self.doWidgetManipulation,func,*args,**kwargs)
        except:
            self.__logTB__()
            self._bDoWidgetDone=False
            self._semDone.release()
        if bDbg:
            self.__logDebug__('semDone acquiring')
        self._semDone.acquire()
        if bDbg:
            self.__logDebug__('semDone acquired')
        #self._semWid.acquire()
        try:
            return self._retWidMan
        finally:
            try:
                self._semDone.release()
                if bDbg:
                    self.__logDebug__('semDone released')
            except:
                self.__logTB___()
            self._semWid.release()
            if bDbg:
                self.__logDebug__('semWid released')
    def DoCallBackWX(self,func,*args,**kwargs):
        try:
            if VERBOSE>5:
                if self.__isLogDebug__():
                    self.__logDebug__('%s;scheduled;args:%s;kwargs:%s'%(
                            func.__name__,self.__logFmtLm__(args),
                            self.__logFmtLm__(kwargs)))
                elif self.__isLogInfo__():
                    self.__logInfo__('%s;scheduled'%(func.__name__))
        except:
            self.__logTB__()
        self.Do(self.CallBackWX,func,*args,**kwargs)
    def CallBackBranchWX(self,funcCB,branchOkCB,branchFltCB):
        func,args,kwargs=funcCB
        try:
            if VERBOSE>5:
                if self.__isLogDebug__():
                    self.__logDebug__('%s;scheduled cb;args:%s;kwargs:%s'%(
                            func.__name__,self.__logFmtLm__(args),
                            self.__logFmtLm__(kwargs)))
                elif self.__isLogInfo__():
                    self.__logInfo__('%s;scheduled'%(func.__name__))
        except:
            self.__logTB__()
        ret=self.CallBackWX(func,*args,**kwargs)
        if ret>=0:
            func,args,kwargs=branchOkCB
            self.CallBack(func,*args,**kwargs)
        else:
            if branchFltCB is None:
                self.DoCallBackBranchWX(funcCB,branchOkCB,branchFltCB)
            else:
                func,args,kwargs=branchFltCB
                self.CallBack(func,*args,**kwargs)
    def DoCallBackBranchWX(self,funcCB,branchOkCB,branchFltCB):
        func,args,kwargs=funcCB
        try:
            if VERBOSE>5:
                if self.__isLogDebug__():
                    self.__logDebug__('%s;scheduled;args:%s;kwargs:%s'%(
                            func.__name__,self.__logFmtLm__(args),
                            self.__logFmtLm__(kwargs)))
                elif self.__isLogInfo__():
                    self.__logInfo__('%s;scheduled'%(func.__name__))
        except:
            self.__logTB__()
        self.Do(self.CallBackBranchWX,funcCB,branchOkCB,branchFltCB)
    def _isResultValid(self,r):
        if r is None:
            return False
        else:
            return True
    def CallBackTimeOutResult(self,zSleep,zTimeOut,funcIsResultValid,func,*args,**kwargs):
        self._semWid.acquire()
        self._retWidMan=None
        if funcIsResultValid is None:
            funcIsResultValid=self._isResultValid
            
        self._bDoWidgetDone=False
        zAct=0
        while funcIsResultValid(self._retWidMan)==False:#self._retWidMan is None:
            self._semDone.acquire()
            if zAct==0:
                self.CallBack(self.doWidgetManipulation,
                            func,*args,**kwargs)
            else:
                self.CallBackDelayed(zSleep,self.doWidgetManipulation,
                            func,*args,**kwargs)
            self._semDone.acquire()
            zAct+=zSleep
            if zAct>zTimeOut:
                break
            else:
                if funcIsResultValid(self._retWidMan)==False:
                    self._semDone.release()
            
        #self._semWid.acquire()
        try:
            return self._retWidMan
        finally:
            try:
                self._semDone.release()
            except:
                self.__logTB___()
            self._semWid.release()
    def CallBackMultiBranchWX(self,funcCB,dBranchCB):
        func,args,kwargs=funcCB
        ret=self.CallBackWX(func,*args,**kwargs)
        if ret in dBranchCB:
            t=dBranchCB[ret]
            if t is None:
                return
            func,args,kwargs=t
            self.CallBack(func,*args,**kwargs)
        elif None in dBranchCB:
            func,args,kwargs=dBranchCB[None]
            self.CallBack(func,*args,**kwargs)
        else:
            self.DoCallBackMultiBranchWX(funcCB,dBranchCB)
    def DoCallBackMultiBranchWX(self,funcCB,dBranchCB):
        self.Do(self.CallBackMultiBranchWX,funcCB,dBranchCB)
    def CallBackTimeOutWX(self,zSleep,zTimeOut,funcCmp,funcOkCB,funcFltCB):
        try:
            if self.Is2Stop():
                return
            func,args,kwargs=funcCmp
            ret=self.CallBackWX(func,*args,**kwargs)
            if self.Is2Stop():
                return
            if ret==True:
                if funcOkCB is not None:
                    func,args,kwargs=funcOkCB
                    self.CallBack(func,*args,**kwargs)
            else:
                if zTimeOut>0:
                    self.Do(self.CallBackDelayed,zSleep,
                            self.DoCallBackTimeOutWX,zSleep,zTimeOut-zSleep,
                            funcCmp,funcOkCB,funcFltCB)
                else:
                    if funcFltCB is not None:
                        func,args,kwargs=funcFltCB
                        self.CallBack(func,*args,**kwargs)
        except:
            import traceback
            traceback.print_exc()
    def DoCallBackTimeOutWX(self,zSleep,zTimeOut,funcCmp,funcOkCB,funcFltCB):
        self.Do(self.CallBackTimeOutWX,zSleep,zTimeOut,funcCmp,funcOkCB,funcFltCB)

