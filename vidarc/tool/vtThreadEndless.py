#----------------------------------------------------------------------------
# Name:         vtThreadEndless.py
# Purpose:
# Author:       Walter Obweger
#
# Created:      20061120
# CVS-ID:       $Id: vtThreadEndless.py,v 1.19 2008/04/26 23:09:57 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import thread
from wx import PostEvent

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.vtThread import vtThread
from vidarc.tool.vtThread import vtThreadFinished
from vidarc.tool.vtThread import vtThreadAborted
from vidarc.tool.vtThreadMixinWX import vtThreadMixinWX

class vtThreadEndless(vtThread):
    def __init__(self,par,bPost=False,verbose=0,origin=None):
        vtThread.__init__(self,par,bPost=bPost,verbose=verbose,origin=origin)
        self.exit=False
        self.busy=False
        self.keepGoing=True
        #thread.start_new_thread(self.Run,())
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'','del')
        try:
            #if self.IsRunning():
            #    self.Exit()
            #while self.IsRunning():
            #    self.__pauseSleep__()
            vtThread.__del__(self)
        except:
            #vtLog.vtLngTB('del')
            pass
    def Pause(self):
        vtThread.Pause(self)
        if self.IsRunning():
            # push dummy to ensure exit
            self.qSched.put(None)
        else:
            self.qSched.put(None)
        vtLog.vtLngCur(vtLog.INFO,'',self.GetOrigin())
    def IsBusy(self):
        try:
            self._acquire()
            if self.busy:
                if self.paused:
                    return False
                return True
            return False
        finally:
            self._release()
    def Do(self,func,*args,**kwargs):
        if self.verbose:
            try:
                vtLog.vtLngCur(vtLog.DEBUG,'status:%s'%(self.__str__()),self.origin)
            except:
                vtLog.vtLngTB(self.origin)
        self._acquire()
        if self.verbose:
            try:
                vtLog.vtLngCur(vtLog.DEBUG,
                        'func;%s,args:%s;kwargs:%s'%(func,`args`,`kwargs`),
                        self.origin)
            except:
                vtLog.vtLngTB(self.origin)
        if self.exit:
            vtLog.vtLngCur(vtLog.CRITICAL,'exit active'%(),self.origin)
            self._release()
            return
        if self.keepGoing==False:
            vtLog.vtLngCur(vtLog.CRITICAL,'Is2Stop active'%(),self.origin)
            self._release()
            return
        self._release()
        self.qSched.put((func,args,kwargs))
        self._ensureRun()
    def _ensureRun(self):
        self._acquire()
        if self.keepGoing==False:
            vtLog.vtLngCur(vtLog.INFO,'exit active'%(),self.origin)
            self._release()
            return
        if not self.running:
            vtLog.vtLngCur(vtLog.DEBUG,'start thread'%(),self.origin)
            # restart now
            self.running=self.keepGoing=True
            self.exit = False
            self._release()
            self.__runThread__()
            return
        self._release()
    def _proc(self):
        if self.verbose:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'check queue'%(),self.origin)
        self._acquire()
        self.busy=not self.qSched.empty()
        self._release()
        t=self.qSched.get()
        #try:
        #    if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #        vtLog.vtLngCur(vtLog.DEBUG,'got from queue;t:%s'%(`t`),self.origin)
        #except:
        #    vtLog.vtLngTB(self.origin)
        if t is None:
            # dummy entry to ensure exit
            vtLog.vtLngCur(vtLog.INFO,'dummy processing element found',self.origin)
            if self.__doPause__()==True:
                return
        else:
            if self.__doPause__()==True:
                return
            self._acquire()
            self.busy=True
            self._release()
            self._doProc(t)
    def __run__(self):
        #self.running=self.keepGoing=True
        try:
            self.__runStart__()
            while self.Is2Stop()==False:
                if self.verbose:
                    vtLog.vtLngCur(vtLog.DEBUG,self.__str__(),self.origin)
                self._proc()
                #if self.keepGoing==False:
                #self._acquire()
                #self.running=False   # 070630:wro
                try:
                    if self.bPost:
                        if self.Is2Stop():
                            PostEvent(self.par,vtThreadAborted())
                        else:
                            PostEvent(self.par,vtThreadFinished())
                except:
                    vtLog.vtLngTB(self.origin)
                #if self.Is2Stop():
                #    self.__clrSched__()
                #    self.__clrWakeUp__()
                #    if self.bPost:
                #        PostEvent(self.par,vtThreadAborted())
                #else:
                #    if self.bPost:
                #        PostEvent(self.par,vtThreadFinished())
            vtLog.vtLngCur(vtLog.INFO,'clean up queue',self.origin)
            self.__clrSched__()
            self.__clrWakeUp__()
        except:
            vtLog.vtLngTB(self.origin)
            try:
                if self.bPost:
                    PostEvent(self.par,vtThreadAborted())
            except:
                vtLog.vtLngTB(self.origin)
        self._acquire()
        #self.exit=False
        self.__runEnd__()
        self.running=False
        self.busy=False
        #self.keepGoing=False
        #self.keepGoing=True
        vtLog.vtLngCur(vtLog.INFO,'rip thread',self.origin)
        self._release()
    def _exit(self):
        pass
    def Restart(self):
        self.Start()
    def Start(self):
        vtLog.vtLngCur(vtLog.INFO,'',self.origin)
        self._acquire()
        self.exit = False
        self.keepGoing=True
        self.qWakeUp.put(1)
        self._release()
        self._ensureRun()
    def Exit(self):
        vtLog.vtLngCur(vtLog.INFO,'',self.origin)
        self._acquire()
        self.exit = True
        self.qWakeUp.put(-2)
        self._release()
        self.Stop()
        vtLog.vtLngCur(vtLog.INFO,'exit',self.origin)
    def Is2Exit(self):
        self._acquire()
        bRet=self.exit==True
        self._release()
        return bRet
    def Stop(self):
        vtLog.vtLngCur(vtLog.INFO,'',self.origin)
        self._acquire()
        self.exit = True
        self.keepGoing=False
        self.qWakeUp.put(-1)
        self._release()
        if self.IsRunning():
            # push dummy to ensure exit
            self.qSched.put(None)
        vtLog.vtLngCur(vtLog.INFO,'exit',self.origin)
        #self.Exit()
    def IsEmpty(self):
        return self.qSched.empty()

class vtThreadEndlessWX(vtThreadEndless,vtThreadMixinWX):
    def __init__(self,*args,**kwargs):
        vtThreadEndless.__init__(self,*args,**kwargs)
        vtThreadMixinWX.__init__(self)
    def DoWX(self,func,*args,**kwargs):
        self.Do(self.CallBackWX,func,*args,**kwargs)
