#----------------------------------------------------------------------------
# Name:         vtThreadCyclic.py
# Purpose:      cyclic threading interface
#               
# Author:       Walter Obweger
#
# Created:      20070930
# CVS-ID:       $Id: vtThreadCoreCyclic.py,v 1.2 2008/03/20 21:55:57 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.vtThreadCore import vtThreadCore

class vtThreadCoreCyclic(vtThreadCore):
    def __init__(self,bPost=False,verbose=0,origin=None):
        vtThreadCore.__init__(self,bPost=bPost,verbose=verbose,origin=origin)
        self.funcCB=None
        self.argsCB=()
        self.kwargsCB={}
        self.fDelay=0.25
        self.busy=False
    def IsBusy(self):
        try:
            self._acquire()
            if self.busy:
                if self.paused:
                    return False
                return True
            return False
        finally:
            self._release()
    def SetDelay(self,fDelay):
        self.fDelay=fDelay
    def SetCB(self,func,*args,**kwargs):
        self.funcCB=func
        self.argsCB=args
        self.kwargsCB=kwargs
    def Start(self):
        vtThreadCore.Start(self)
        while self.qWakeUp.empty()==False:
            self.qWakeUp.get()
        if self.funcCB is not None:
            self.Do(self.__procDelay__)
    def __procDelay__(self):
        try:
            val=self.qWakeUp.get(block=True,timeout=self.fDelay)
        except:
            val=100
        if val>0:
            self._acquire()
            self.busy=True
            self._release()
            try:
                self.funcCB(*self.argsCB,**self.kwargsCB)
            except:
                self.__logTB__()
            self._acquire()
            self.busy=False
            self._release()
        if val>=0:
            self.Do(self.__procDelay__)

