#!/usr/bin/env python
#Boa:App:BoaApp
#----------------------------------------------------------------------------
# Name:         vReportBuildAppl.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vReportBuildAppl.py,v 1.1 2005/12/11 23:17:22 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase

import vReportBuildFrame

modules ={u'vReportBuildFrame': [1,
                        'Main frame of Application',
                        u'vReportBuildFrame.py']}

class BoaApp(wx.App):
    def OnInit(self):
        wx.InitAllImageHandlers()
        self.main = vReportBuildFrame.create(None)
        self.main.Show()
        self.SetTopWindow(self.main)
        return True

def main():
    optshort,optlong='l:',['lang=']
    vtLgBase.initAppl(optshort,optlong,'vRepBuildAppl')
    
    application = BoaApp(0)
    application.MainLoop()

if __name__ == '__main__':
    main()
