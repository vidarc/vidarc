#Boa:Dialog:vRepBuildDialog
#----------------------------------------------------------------------------
# Name:         vRepBuildDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vRepBuildDialog.py,v 1.1 2005/12/11 23:17:21 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import images

from vidarc.tool.report.vRepBuildPanel import *

def create(parent):
    return vRepBuildDialog(parent)

[wxID_VREPBUILDDIALOG, wxID_VREPBUILDDIALOGCBCLOSE, wxID_VREPBUILDDIALOGCBOK, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vRepBuildDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VREPBUILDDIALOG,
              name=u'vRepBuildDialog', parent=prnt, pos=wx.Point(269, 132),
              size=wx.Size(422, 344), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vReport Build Dialog')
        self.SetClientSize(wx.Size(414, 317))
        self.SetAutoLayout(True)

        self.cbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VREPBUILDDIALOGCBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'cbOk',
              parent=self, pos=wx.Point(112, 280), size=wx.Size(76, 30),
              style=0)
        self.cbOk.Bind(wx.EVT_BUTTON, self.OnCbOkButton,
              id=wxID_VREPBUILDDIALOGCBOK)

        self.cbClose = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VREPBUILDDIALOGCBCLOSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Close', name=u'cbClose',
              parent=self, pos=wx.Point(216, 280), size=wx.Size(76, 30),
              style=0)
        self.cbClose.Bind(wx.EVT_BUTTON, self.OnCbCloseButton,
              id=wxID_VREPBUILDDIALOGCBCLOSE)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        self.pnBuild=vRepBuildPanel(self,wx.NewId(),
              wx.Point(0, 0), wx.Size(400, 300), 
              wx.TAB_TRAVERSAL, u'vRepBuildPanel')
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self, wx.Top)
        lc.left.SameAs      (self, wx.Left, 4)
        lc.right.SameAs     (self, wx.Right, 4)
        lc.bottom.SameAs    (self.cbOk, wx.Top, 4)
        self.pnBuild.SetConstraints(lc)
        
        self.cbOk.SetBitmapLabel(images.getOkBitmap())
        self.cbClose.SetBitmapLabel(images.getCancelBitmap())
        
    def SetRepTex(self,repTex):
        self.pnBuild.SetRepTex(repTex)
    def Build(self,info,*args,**kwargs):
        self.pnBuild.Build(info,*args,**kwargs)
    def Show(self,flag):
        self.pnBuild.EnableButtons()
        wx.Dialog.Show(self,flag)
    def OnCbOkButton(self, event):
        self.Show(False)
        event.Skip()

    def OnCbCloseButton(self, event):
        self.Show(False)
        event.Skip()
