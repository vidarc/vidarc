#!/usr/bin/env python
#----------------------------------------------------------------------
#----------------------------------------------------------------------------
# Name:         encode_bitmaps.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: encode_bitmaps.py,v 1.2 2006/04/26 19:05:55 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    "-u -i -n Build img/Build02_16.png images.py",
    "-a -u -n Add img/add.png images.py",
    "-a -u -n Del img/waste.png images.py",
    "-a -u -n Apply img/checkmrk.png images.py",
    "-a -u -n Open img/Pdf02_16.png images.py",
    "-a -u -n Ok img/ok_2.png images.py",
    "-a -u -n Cancel img/abort.png images.py",
    "-a -u -n Browse img/BrowseFile01_16.png images.py",
    "-a -u -n Stop img/SrvStpd03_32.png images.py",
    "-a -u -n Run img/SrvRng02_32.png images.py",
    "-a -u -n Abort img/SrvStop01_32.png images.py",
    "-a -u -n Error img/Error01_16.png images.py",
    "-a -u -n Warning img/Warning01_16.png images.py",
    "-a -u -n DocTmpl img/DocTmpl03_16.png images.py",
    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)

