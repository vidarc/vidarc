#Boa:FramePanel:vRepBuildPanel
#----------------------------------------------------------------------------
# Name:         vRepBuildPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vRepBuildPanel.py,v 1.3 2008/04/09 23:09:44 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.report.vRepBuildTex import EVT_THREAD_TEX_BUILD
from vidarc.tool.report.vRepBuildTex import EVT_THREAD_TEX_BUILD_FINISHED
from vidarc.tool.report.vRepBuildTex import EVT_THREAD_TEX_BUILD_ABORTED
from vidarc.tool.report.vRepBuildTex import EVT_THREAD_TEX_COMPILE_RESULT
#from vidarc.tool.report.vRepBuildTex import *

import traceback,sys,string,os

[wxID_VREPBUILDPANEL, wxID_VREPBUILDPANELBMPRES, wxID_VREPBUILDPANELCBBUILD, 
 wxID_VREPBUILDPANELCBOPEN, wxID_VREPBUILDPANELCHCSHOW, 
 wxID_VREPBUILDPANELLBLERRORS, wxID_VREPBUILDPANELLBLINFILE, 
 wxID_VREPBUILDPANELLBLOUTFILE, wxID_VREPBUILDPANELLBLWARNS, 
 wxID_VREPBUILDPANELLSTRES, wxID_VREPBUILDPANELPBPROCESS, 
 wxID_VREPBUILDPANELPBSTEP, wxID_VREPBUILDPANELTXTERRORS, 
 wxID_VREPBUILDPANELTXTINFILE, wxID_VREPBUILDPANELTXTOUTFILE, 
 wxID_VREPBUILDPANELTXTWARNS, 
] = [wx.NewId() for _init_ctrls in range(16)]

import images

class vRepBuildPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VREPBUILDPANEL, name=u'vRepBuildPanel',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(400, 300),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(392, 273))
        self.SetAutoLayout(True)

        self.pbStep = wx.Gauge(id=wxID_VREPBUILDPANELPBSTEP, name=u'pbStep',
              parent=self, pos=wx.Point(8, 256), range=2, size=wx.Size(100, 13),
              style=wx.GA_HORIZONTAL | wx.GA_SMOOTH)

        self.pbProcess = wx.Gauge(id=wxID_VREPBUILDPANELPBPROCESS,
              name=u'pbProcess', parent=self, pos=wx.Point(120, 256), range=100,
              size=wx.Size(264, 13), style=wx.GA_HORIZONTAL | wx.GA_SMOOTH)

        self.bmpRes = wx.StaticBitmap(bitmap=wx.EmptyBitmap(32, 32),
              id=wxID_VREPBUILDPANELBMPRES, name=u'bmpRes', parent=self,
              pos=wx.Point(88, 216), size=wx.Size(32, 32), style=0)

        self.cbBuild = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VREPBUILDPANELCBBUILD,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Build'), name=u'cbBuild',
              parent=self, pos=wx.Point(8, 216), size=wx.Size(76, 30), style=0)
        self.cbBuild.Bind(wx.EVT_BUTTON, self.OnCbBuildButton,
              id=wxID_VREPBUILDPANELCBBUILD)

        self.cbOpen = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VREPBUILDPANELCBOPEN,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Open'), name=u'cbOpen',
              parent=self, pos=wx.Point(248, 216), size=wx.Size(76, 30),
              style=0)
        self.cbOpen.Bind(wx.EVT_BUTTON, self.OnCbOpenButton,
              id=wxID_VREPBUILDPANELCBOPEN)

        self.txtInFile = wx.TextCtrl(id=wxID_VREPBUILDPANELTXTINFILE,
              name=u'txtInFile', parent=self, pos=wx.Point(48, 4),
              size=wx.Size(336, 21), style=wx.TE_READONLY, value=u'')
        self.txtInFile.SetConstraints(LayoutAnchors(self.txtInFile, True, True,
              True, False))

        self.txtOutFile = wx.TextCtrl(id=wxID_VREPBUILDPANELTXTOUTFILE,
              name=u'txtOutFile', parent=self, pos=wx.Point(48, 28),
              size=wx.Size(336, 21), style=wx.TE_READONLY, value=u'')
        self.txtOutFile.SetConstraints(LayoutAnchors(self.txtOutFile, True,
              True, True, False))

        self.lblInFile = wx.StaticText(id=wxID_VREPBUILDPANELLBLINFILE,
              label=u'Input', name=u'lblInFile', parent=self, pos=wx.Point(4,
              8), size=wx.Size(24, 13), style=0)

        self.lblOutFile = wx.StaticText(id=wxID_VREPBUILDPANELLBLOUTFILE,
              label=u'Output', name=u'lblOutFile', parent=self, pos=wx.Point(4,
              32), size=wx.Size(36, 13), style=0)

        self.txtErrors = wx.TextCtrl(id=wxID_VREPBUILDPANELTXTERRORS,
              name=u'txtErrors', parent=self, pos=wx.Point(200, 56),
              size=wx.Size(56, 21), style=wx.TE_RIGHT, value=u'')

        self.lblErrors = wx.StaticText(id=wxID_VREPBUILDPANELLBLERRORS,
              label=u'Errors', name=u'lblErrors', parent=self, pos=wx.Point(156,
              60), size=wx.Size(27, 13), style=0)

        self.lblWarns = wx.StaticText(id=wxID_VREPBUILDPANELLBLWARNS,
              label=u'Warnings', name=u'lblWarns', parent=self,
              pos=wx.Point(272, 60), size=wx.Size(45, 13), style=0)

        self.txtWarns = wx.TextCtrl(id=wxID_VREPBUILDPANELTXTWARNS,
              name=u'txtWarns', parent=self, pos=wx.Point(328, 56),
              size=wx.Size(56, 21), style=wx.TE_RIGHT, value=u'')

        self.chcShow = wx.Choice(choices=[_('errors'), _(u'warnings'),
              _(u'all')], id=wxID_VREPBUILDPANELCHCSHOW, name=u'chcShow',
              parent=self, pos=wx.Point(48, 56), size=wx.Size(80, 21), style=0)

        self.lstRes = wx.ListView(id=wxID_VREPBUILDPANELLSTRES, name=u'lstRes',
              parent=self, pos=wx.Point(4, 88), size=wx.Size(380, 120),
              style=wx.LC_REPORT)

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self, wx.Bottom, -25)
        lc.left.SameAs      (self, wx.Left, 4)
        lc.height.AsIs      ()
        lc.width.PercentOf  (self, wx.Width, 20)
        self.pbStep.SetConstraints(lc)
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.pbStep, wx.Top)
        lc.left.SameAs      (self.pbStep, wx.Right, 4)
        lc.right.SameAs     (self, wx.Right, 4)
        lc.height.AsIs      ()
        self.pbProcess.SetConstraints(lc)
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.pbStep, wx.Bottom, -48)
        lc.left.SameAs      (self.pbStep, wx.Left, 0)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.cbBuild.SetConstraints(lc)
        
        lc = wx.LayoutConstraints()
        lc.centreY.SameAs       (self.cbBuild, wx.CentreY)
        lc.left.SameAs      (self.cbBuild, wx.Right, 4)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.bmpRes.SetConstraints(lc)
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.cbBuild, wx.Top)
        lc.left.SameAs      (self, wx.Right, -80)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.cbOpen.SetConstraints(lc)
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.txtErrors, wx.Bottom, 4)
        lc.left.SameAs      (self, wx.Left, 4)
        lc.right.SameAs     (self, wx.Right, 4)
        lc.bottom.SameAs    (self.bmpRes, wx.Top, 4)
        self.lstRes.SetConstraints(lc)
        
        self.cbBuild.SetBitmapLabel(images.getBuildBitmap())
        self.cbOpen.SetBitmapLabel(images.getOpenBitmap())
        self.bmpRes.SetBitmap(images.getStopBitmap())
        
        self.imgLstRes=wx.ImageList(16,16)
        self.imgLstRes.Add(images.getErrorBitmap())
        self.imgLstRes.Add(images.getWarningBitmap())
        self.dictImg={}
        self.lstRes.SetImageList(self.imgLstRes,wx.IMAGE_LIST_NORMAL)
        self.lstRes.SetImageList(self.imgLstRes,wx.IMAGE_LIST_SMALL)

        self.lstRes.InsertColumn(0,_(u'type'),wx.LIST_FORMAT_LEFT,40)
        self.lstRes.InsertColumn(1,_(u'value'),wx.LIST_FORMAT_LEFT,300)
        
        self.chcShow.SetSelection(0)
        
        self.repTex=None
    def OnTexBuild(self,evt):
        self.pbStep.SetValue(evt.GetStep())
        iCount=evt.GetCount()
        if iCount>0:
            self.pbProcess.SetRange(iCount)
        self.pbProcess.SetValue(evt.GetValue())
        evt.Skip()
    def OnTexBuildFin(self,evt):
        self.pbProcess.SetValue(0)
        self.pbStep.SetValue(0)
        self.cbBuild.Enable(True)
        self.cbOpen.Enable(True)
        self.bmpRes.SetBitmap(images.getStopBitmap())
        #self.__startMiTek__()
        evt.Skip()
    def OnTexBuildAbort(self,evt):
        self.pbStep.SetValue(0)
        self.pbProcess.SetValue(0)
        self.cbBuild.Enable(True)
        self.cbOpen.Enable(True)
        self.bmpRes.SetBitmap(images.getAbortBitmap())
        evt.Skip()
    def __isErr__(self,val):
        if string.find(val,'Emergency stop')>=0:
            return True
        if string.find(val,'Error')>=0:
            return True
        if string.find(val,'Misplaced')>=0:
            return True
        return False
    def __isWarn__(self,val):
        if string.find(val,'Warning')>=0:
            return True
        return False
    def OnTexCompRes(self,evt):
        #self.txtRes.SetValue(evt.GetResult())
        self.lstRes.DeleteAllItems()
        iSel=self.chcShow.GetSelection()
        bWarnAct=False
        bErrAct=False
        iWarnCount=0
        iErrCount=0
        for val in evt.GetResult():
            bAdd=False
            bErr=self.__isErr__(val)
            bWarn=self.__isWarn__(val)
            if bWarn:
                iWarnCount+=1
            if bErr:
                iErrCount+=1
            if iSel==0:
                bAdd=bErr
            if iSel==1:
                if bErr or bWarn:
                    bAdd=True
            if iSel==2:
                bAdd=True
            if len(string.strip(val))<=0:
                bErrAct=False
                bWarnAct=False
            if bErrAct or bWarnAct:
                bAdd=True
            if bAdd:
                i=-1
                if bErr:
                    i=0
                    bErrAct=True
                if bWarn:
                    i=1
                    bWarnAct=True
                idx=self.lstRes.InsertImageStringItem(sys.maxint, '', i)
                self.lstRes.SetStringItem(idx,1,string.replace(val,'\n',''),-1)
        self.txtWarns.SetValue('%d'%iWarnCount)
        self.txtErrors.SetValue('%d'%iErrCount)
        evt.Skip()
    def SetRepTex(self,repTex):
        self.repTex=repTex
        if self.repTex.par is not None:
            EVT_THREAD_TEX_BUILD(self.repTex.par,self.OnTexBuild)
            EVT_THREAD_TEX_BUILD_FINISHED(self.repTex.par,self.OnTexBuildFin)
            EVT_THREAD_TEX_BUILD_ABORTED(self.repTex.par,self.OnTexBuildAbort)
            EVT_THREAD_TEX_COMPILE_RESULT(self.repTex.par,self.OnTexCompRes)
    def Build(self,info,*args,**kwargs):
        vtLog.CallStack('')
        if self.repTex is None:
            return -1
        self.cbBuild.Enable(False)
        self.cbOpen.Enable(False)
        self.bmpRes.SetBitmap(images.getRunBitmap())
        try:
            self.repTex.Build(info,*args,**kwargs)
        except:
            self.cbBuild.Enable(True)
            self.cbOpen.Enable(True)
            pass

    def OnCbBuildButton(self, event):
        if self.repTex is None:
            return -1
        if self.repTex.StartBuild()>=0:
            self.cbBuild.Enable(False)
            self.cbOpen.Enable(False)
            self.bmpRes.SetBitmap(images.getRunBitmap())
            self.txtInFile.SetValue(self.repTex.GetInputFN())
            self.txtOutFile.SetValue(self.repTex.GetOutputFN())
            self.lstRes.DeleteAllItems()
        event.Skip()

    def OnCbOpenButton(self, event):
        self.repTex.OpenOutput()
        event.Skip()
    def EnableButtons(self):
        self.txtInFile.SetValue(self.repTex.GetInputFN())
        self.txtOutFile.SetValue('')
        self.lstRes.DeleteAllItems()
        self.cbBuild.Enable(True)
        self.cbOpen.Enable(True)
            
