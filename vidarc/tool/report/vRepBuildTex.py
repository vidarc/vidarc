#----------------------------------------------------------------------------
# Name:         vRepBuildTex.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vRepBuildTex.py,v 1.7 2007/10/28 22:46:16 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.report.vRepDocTex import *

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.vtThread import vtThread

import os,shutil,copy,string
import thread,threading,traceback

wxEVT_THREAD_TEX_BUILD=wx.NewEventType()
def EVT_THREAD_TEX_BUILD(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_TEX_BUILD,func)
class wxThreadTexBuild(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_TEX_BUILD(<widget_name>, self.OnItemSel)
    """

    def __init__(self,iStep,iVal,iCount=-1):
        wx.PyEvent.__init__(self)
        self.val=iVal
        self.step=iStep
        self.count=iCount
        self.SetEventType(wxEVT_THREAD_TEX_BUILD)
    def GetStep(self):
        return self.step
    def GetValue(self):
        return self.val
    def GetCount(self):
        return self.count

wxEVT_THREAD_TEX_BUILD_FINISHED=wx.NewEventType()
def EVT_THREAD_TEX_BUILD_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_TEX_BUILD_FINISHED,func)
class wxThreadTexBuildFinished(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_TEX_BUILD_FINISHED(<widget_name>, self.OnItemSel)
    """
    def __init__(self):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_THREAD_TEX_BUILD_FINISHED)
    
wxEVT_THREAD_TEX_BUILD_ABORTED=wx.NewEventType()
def EVT_THREAD_TEX_BUILD_ABORTED(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_TEX_BUILD_ABORTED,func)
class wxThreadTexBuildAborted(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_TEX_BUILD_ABORTED(<widget_name>, self.OnItemSel)
    """
    def __init__(self):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_THREAD_TEX_BUILD_ABORTED)

wxEVT_THREAD_TEX_COMPILE_RESULT=wx.NewEventType()
def EVT_THREAD_TEX_COMPILE_RESULT(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_TEX_COMPILE_RESULT,func)
class wxThreadTexCompileResult(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_TEX_COMPILE_RESULT(<widget_name>, self.OnItemSel)
    """
    def __init__(self,lines):
        wx.PyEvent.__init__(self)
        self.lines=copy.copy(lines)
        self.SetEventType(wxEVT_THREAD_TEX_COMPILE_RESULT)
    def GetResult(self):
        return self.lines

from vidarc.tool.report.vRepBuildDialog import *

class thdTexBuild(vtThread):
    def __init__(self,par,verbose=0):
        vtThread.__init__(self,None,False)
        self.par=par
        #self.running = False
        #self.verbose=verbose
    def Build(self,func=None,*args,**kwargs):
        self.Do(self.Run,func,*args,**kwargs)
        return
        #if self.verbose>0:
        #    vtLog.vtLngCallStack(self,vtLog.DEBUG,'func:%s args:%s'%(repr(func),repr(args)))
        if self.running:
            return
        self.func=func
        self.args=args
        self.kwargs=kwargs
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.keepGoing = self.running = True
        thread.start_new_thread(self.Run, ())
    #def Stop(self):
    #    self.keepGoing = False
    #def IsRunning(self):
    #    return self.running
    def Run(self,func=None,*args,**kwargs):
        try:
            self.par.__doBuild__()
            if func is not None:
                func(*args,**kwargs)
        except:
            traceback.print_exc()
    def RunOld(self):
        try:
            self.par.__doBuild__()
            if self.func is not None:
                self.func(*self.args,**self.kwargs)
        except:
            traceback.print_exc()
        self.keepGoing = False
        self.running = False
        
class vRepBuildTex(vRepDocTex):
    def __init__(self,par,verbose=0):
        vRepDocTex.__init__(self,verbose)
        self.par=par
        self.thdBuild=thdTexBuild(self)
        self.dlgBuild=None
        self.sTitle=''
        self.sRev=''
    def ClearDoc(self):
        vRepDocTex.ClearDoc(self)
        self.thdBuild.SetPostWid(None)
    def IsBusy(self):
        if self.thdBuild.IsRunning():
            return True
        return False
    def Restart(self):
        self.thdBuild.Start()
    def Stop(self):
        self.thdBuild.Stop()
    #def Resume(self):
    #    self.thdBuild.Resume()
    #def Pause(self):
    #    self.thdBuild.Pause()
    def OpenOutput(self):
        if self.sFN is None:
            return
        if len(self.sFN)<=0:
            return -2
        sExts=string.split(self.sFN,'.')
        sExts[-1]='pdf'
        sExt=sExts[-1]
        fileType = wx.TheMimeTypesManager.GetFileTypeFromExtension(sExt)
        #filename=fnUtil.getAbsoluteFilenameRel2BaseDir(sFN,self.prjDN)
        mime = fileType.GetMimeType()
        if mime is None:
            mime="File extension"
        cmd = fileType.GetOpenCommand(string.join(sExts,'.'), mime)
        wx.Execute(cmd)
    def GetInputFN(self):
        if self.sFN is None:
            return u'???'
        return self.sFN
    def GetOutputFN(self):
        if self.sFN is None:
            return u'???'
        sExts=string.split(self.sFN,'.')
        sExts[-1]='pdf'
        return string.join(sExts,'.')
    def SetDlg(self,dlg):
        self.dlgBuild=dlg
    def getDlg(self):
        if self.dlgBuild is None:
            if self.par is not None:
                self.dlgBuild=vRepBuildDialog(self.par)
                self.dlgBuild.Centre()
                self.dlgBuild.SetRepTex(self)
        return self.dlgBuild
    def GetFileName(self,sTitle,sRev,selDoc,bSave,origin,parent=None):
        self.sTitle=sTitle
        self.sRev=sRev
        return vRepDocTex.GetFileName(self,sTitle,sRev,selDoc,bSave,origin,parent)
    def BuildGui(self,info,*args,**kwargs):
        self.info=info
        self.args=args
        self.kwargs=kwargs
        dlg=self.getDlg()
        if dlg is not None:
            dlg.Show(True)
            #dlg.Build(info,*args,**kwargs)
        else:
            self.Build(info,*args,**kwargs)
    def __buildReplace__(self):
        self.dictRepl['doc_ml_prj_name']=_(u'Project')
        self.dictRepl['doc_ml_prj_nr']=_(u'Proj.No.')
        self.dictRepl['doc_ml_doc_nr']=_(u'Doc.No.')
        self.dictRepl['doc_ml_date']=_(u'Date')
        self.dictRepl['doc_ml_page']=_(u'Page')
        self.dictRepl['doc_ml_page_of']=_(u'of')
        self.dictRepl['doc_ml_doc_ident']=_(u'Documentenidentification')
        self.dictRepl['doc_ml_rev']=_(u'Revision')
        self.dictRepl['doc_ml_rev_abbr']=_(u'Rev.')
        self.dictRepl['doc_ml_rev_date']=_(u'Revisionsdate')
        self.dictRepl['doc_ml_title']=_(u'Title')
        self.dictRepl['doc_ml_prepared']=_(u'Prepared by')
        self.dictRepl['doc_ml_review']=_(u'Reviewed by')
        self.dictRepl['doc_ml_approved']=_(u'Approved by')
        self.dictRepl['doc_ml_company']=_(u'Company')
        self.dictRepl['doc_ml_name']=_(u'Name')
        self.dictRepl['doc_ml_signature']=_(u'Signature')
        self.dictRepl['doc_ml_replaced_rev']=_(u'Replaced Revision')
        self.dictRepl['doc_ml_doc_status']=_(u'Document Status')
        
    def Build(self,info,*args,**kwargs):
        self.info=info
        self.args=args
        self.kwargs=kwargs
        self.__buildReplace__()
        if len(self.sFN)<=0:
            return -2
        if len(self.sTmplLatexFN)<=0:
            return -1
        self.thdBuild.Build()
        return 0
    def StartBuild(self):
        self.Build(self.info,*self.args,**self.kwargs)
        return 0
    def __doBuild__(self):
        #vtLog.CallStack('')
        try:
            sDir=os.path.split(self.sFN)
            os.makedirs(sDir[0])
        except:
            pass
        fnUtil.shiftFile(self.sFN)
        e=u'ISO-8859-1'
        tmpl=codecs.open(self.sTmplLatexFN,'r',e)
        f=codecs.open(self.sFN,"w",e)
        if self.par is not None:
            wx.PostEvent(self.par,wxThreadTexBuild(1,1,3))
        self.__startDoc__(tmpl,f)
        if self.par is not None:
            wx.PostEvent(self.par,wxThreadTexBuild(1,2,3))
        self.__procInfo__(f,self.info)
        if self.par is not None:
            wx.PostEvent(self.par,wxThreadTexBuild(1,3,3))
        self.__endDoc__(f)
        f.close()
        tmpl.close()
        
        sOldDir=os.getcwd()
        sNewDir=os.path.join(sDir[0],'build')
        try:
            os.makedirs(sNewDir)
        except:
            pass
        os.chdir(sNewDir)
        for i in range(4):
            try:
                if self.par is not None:
                    wx.PostEvent(self.par,wxThreadTexBuild(2,i,4))
                p=os.popen('pdflatex ../'+sDir[1])
                l=p.readlines()
                if self.par is not None:
                    wx.PostEvent(self.par,wxThreadTexCompileResult(l))
                p.close()
            except:
                traceback.print_exc()
        try:
            shutil.copy(sDir[1][:-3]+'pdf','..')
        except:
            pass
        os.chdir(sOldDir)
        if self.par is not None:
            wx.PostEvent(self.par,wxThreadTexBuildFinished())
        
        sRelFN=self.GetRelativeFN()
        iRes=self.netPrjDoc.SetDocument(self.netDoc,self.selDocNode,self.sTitle,self.sRev,sRelFN)
        if iRes!=0:
            if self.par is not None:
                dlg=wx.MessageDialog(self.par,_(u'Document could not be added!') ,
                                self.origin,
                                wx.OK|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg.Destroy()
            return iRes
        
        return iRes
    def __startDoc__(self,tmpl,f):
        #vtLog.CallStack('')
        lines=tmpl.readlines()
        for l in lines:
            i=string.find(l,'$')
            if i>0:
                j=string.find(l,'$',i+1)
                if j>0:
                    k=l[i+1:j]
                    if self.dictRepl.has_key(k):
                        l=l[:i]+self.dictRepl[k]+l[j+1:]
            f.write(l)
        return 0
    def __procInfo__(self,f,info):
        return 0
    
    def dummy(self):
        pass
        
