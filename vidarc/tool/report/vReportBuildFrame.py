#Boa:Frame:vReportBuildFrame
#----------------------------------------------------------------------------
# Name:         vReportBuildFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vReportBuildFrame.py,v 1.1 2005/12/11 23:17:22 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

from vidarc.tool.report.vRepBuildTex import *
from vidarc.tool.report.vRepBuildPanel import *
from vidarc.tool.report.vRepBuildDialog import *

import vidarc.tool.xml.vtXmlDom as vtXmlDom
from vidarc.vApps.vDoc.vXmlDomDoc import *
from vidarc.vApps.vPrjDoc.vNetPrjDoc import *

def create(parent):
    return vReportBuildFrame(parent)

[wxID_VREPORTBUILDFRAME, wxID_VREPORTBUILDFRAMECBBUILD1, 
 wxID_VREPORTBUILDFRAMECBRESULT, wxID_VREPORTBUILDFRAMEPBPROCESS, 
 wxID_VREPORTBUILDFRAMEPBSTEP, wxID_VREPORTBUILDFRAMEPNMAIN, 
 wxID_VREPORTBUILDFRAMETXTRES, wxID_VREPORTBUILDFRAMETXTREV, 
 wxID_VREPORTBUILDFRAMETXTTITLE, 
] = [wx.NewId() for _init_ctrls in range(9)]

class vReportBuildFrame(wx.Frame):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VREPORTBUILDFRAME,
              name=u'vReportBuildFrame', parent=prnt, pos=wx.Point(284, 142),
              size=wx.Size(457, 332), style=wx.DEFAULT_FRAME_STYLE,
              title=u'vReport Build Frame')
        self.SetClientSize(wx.Size(449, 305))

        self.pnMain = wx.Panel(id=wxID_VREPORTBUILDFRAMEPNMAIN, name=u'pnMain',
              parent=self, pos=wx.Point(0, 0), size=wx.Size(449, 305),
              style=wx.TAB_TRAVERSAL)

        self.cbBuild1 = wx.Button(id=wxID_VREPORTBUILDFRAMECBBUILD1,
              label=u'build 1', name=u'cbBuild1', parent=self.pnMain,
              pos=wx.Point(16, 16), size=wx.Size(75, 23), style=0)
        self.cbBuild1.Bind(wx.EVT_BUTTON, self.OnCbBuild1Button,
              id=wxID_VREPORTBUILDFRAMECBBUILD1)

        self.txtRes = wx.TextCtrl(id=wxID_VREPORTBUILDFRAMETXTRES,
              name=u'txtRes', parent=self.pnMain, pos=wx.Point(16, 56),
              size=wx.Size(208, 232), style=wx.TE_MULTILINE, value=u'')

        self.cbResult = wx.Button(id=wxID_VREPORTBUILDFRAMECBRESULT,
              label=u'result', name=u'cbResult', parent=self.pnMain,
              pos=wx.Point(368, 16), size=wx.Size(75, 23), style=0)
        self.cbResult.Bind(wx.EVT_BUTTON, self.OnCbResultButton,
              id=wxID_VREPORTBUILDFRAMECBRESULT)

        self.pbStep = wx.Gauge(id=wxID_VREPORTBUILDFRAMEPBSTEP, name=u'pbStep',
              parent=self.pnMain, pos=wx.Point(112, 16), range=100,
              size=wx.Size(64, 13), style=wx.GA_HORIZONTAL | wx.GA_SMOOTH)

        self.pbProcess = wx.Gauge(id=wxID_VREPORTBUILDFRAMEPBPROCESS,
              name=u'pbProcess', parent=self.pnMain, pos=wx.Point(184, 16),
              range=100, size=wx.Size(160, 13),
              style=wx.GA_HORIZONTAL | wx.GA_SMOOTH)

        self.txtTitle = wx.TextCtrl(id=wxID_VREPORTBUILDFRAMETXTTITLE,
              name=u'txtTitle', parent=self.pnMain, pos=wx.Point(256, 48),
              size=wx.Size(100, 21), style=0, value=u'Test')

        self.txtRev = wx.TextCtrl(id=wxID_VREPORTBUILDFRAMETXTREV,
              name=u'txtRev', parent=self.pnMain, pos=wx.Point(256, 80),
              size=wx.Size(100, 21), style=0, value=u'0.0')

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.repTex=vRepBuildTex(self,verbose=1)
        self.xmlDoc=vXmlDomDoc()
        self.xmlPrjDoc=vNetPrjDocuments(self,'vPrjDoc')
        self.xmlDoc.Open('test/doc.xml')
        self.xmlPrjDoc.Open('test/prjdoc.xml')
        self.repTex.SetDocs(self.xmlDoc,self.xmlPrjDoc)
        self.docNode=self.xmlDoc.getChildByLst(None,['maingroup','docgroup'])
    def OnCbBuild1Button(self, event):
        iRes,sFN=self.repTex.GetFileName(
                self.txtTitle.GetValue(),
                self.txtRev.GetValue(),
                self.docNode,
                True,
                u'vRepBuildAppl',self)
        if iRes==0:
            self.repTex.BuildGui('what ever')
        event.Skip()

    def OnCbResultButton(self, event):
        self.xmlPrjDoc.Save()
        event.Skip()
