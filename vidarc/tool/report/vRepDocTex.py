#----------------------------------------------------------------------------
# Name:         vRepDocTex.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vRepDocTex.py,v 1.9 2014/10/10 06:04:51 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer

import wx
import math
import time,os,os.path,string,stat,shutil,codecs
import vidarc.tool.InOut.fnUtil as fnUtil

from vidarc.tool.misc.vtmMsgDialog import vtmMsgDialog

FAULTY_TITLE               = 0x0001
FAULTY_VERSION             = 0x0002
FAULTY_TMPL_TEX            = 0x0004
FAULTY_PRJ_DIR             = 0x0008
FAULTY_DOC_GRP             = 0x0010
FAULTY_FILE_EXISTS         = 0x0020
FAULTY_FILE_NOT_EXISTS     = 0x0040
class vRepDocTex(vtXmlDomConsumer):
    COLORS=[('black','yellow!50!brown'),
            ('black','yellow!50!orange'),
            ('black','green!50!purple'),
            ('black','green!50!violet'),
            ('black','green!50!orange'),
            ('black','green!50!brown'),
            ('black','red!50!yellow'),
            ('black','red!50!orange'),
            ('black','red!50!purple')]
    def __init__(self,verbose=0):
        vtXmlDomConsumer.__init__(self)
        self.verbose=verbose
        self.netDoc=None
        self.netPrjDoc=None
        self.selDocNode=None
        self.showing=[]
        self.grouping=[]
        self.origin='vRepDocTex'
        self.sFN=''
        self.sTmplLatexFN=''
        self.lang='en'
        self.dictRepl={}
        self.dictRepl['author']='WRO'
        self.dictRepl['author_company']='VID'
        self.dictRepl['reviewer']=''
        self.dictRepl['review_company']=''
        self.dictRepl['review_date']=''
        self.dictRepl['appoved_by']=''
        self.dictRepl['approve_company']=''
        self.dictRepl['approve_date']=''
        self.dictRepl['watermark']=''
        self.dictRepl['doc_state']='report'
    def ClearDoc(self):
        self.netDoc=None
        self.netPrjDoc=None
        vtXmlDomConsumer.ClearDoc(self)
    def Clear(self):
        vtXmlDomConsumer.Clear(self)
    def IsBusy(self):
        return False
    def Stop(self):
        pass
    def SetDoc(self,doc,bNet=False):
        vtXmlDomConsumer.SetDoc(self,doc)
    def SetDocDoc(self,netDoc):
        if self.netDoc is not None:
            self.netDoc.DelConsumer(self)
        self.netDoc=netDoc
        if self.netDoc is not None:
            self.netDoc.AddConsumer(self,self.Clear)
    def SetDocPrjDoc(self,netPrjDoc):
        if self.netPrjDoc is not None:
            self.netPrjDoc.DelConsumer(self)
        self.netPrjDoc=netPrjDoc
        if self.netPrjDoc is not None:
            self.netPrjDoc.AddConsumer(self,self.Clear)
    def SetDocs(self,netDoc,netPrjDoc):
        self.SetDocDoc(netDoc)
        self.SetDocPrjDoc(netPrjDoc)
        self.selDocNode=None
    def SetLang(self,lang):
        self.lang=lang
    def SetReplaceInfo(self,key,info):
        self.dictRepl[key]=info
    def GetReplaceInfo(self,key):
        return self.dictRepl[key]
    def GetFileName(self,sTitle,sRev,selDoc,bSave,origin,parent=None):
        self.selDocNode=selDoc
        self.origin=origin
        iRes=0
        bFault=False
        if len(sRev)<=0:
            sRev='??'
            iRes|=FAULTY_VERSION
        else:
            sRepl=fnUtil.replaceSuspectChars(sRev)
            if sRepl!=sRev:
                iRes|=FAULTY_VERSION
        sTitleFN=sTitle
        if len(sTitle)<=0:
            iRes|=FAULTY_TITLE
        else:
            sRepl=fnUtil.replaceSuspectChars(sTitle,'')
            #if sRepl!=sTitle:
            #    iRes|=FAULTY_TITLE
            sTitleFN=sRepl
        nodeSettings=self.netPrjDoc.getChild(self.netPrjDoc.getRoot(),'settings')
        nodePrjInf=self.netPrjDoc.getChild(self.netPrjDoc.getRoot(),'prjinfo')
        
        sPrjDN=self.netPrjDoc.GetPrjDN()
        if len(sPrjDN)==0:
            sPrjDN=self.netPrjDoc.getNodeText(nodeSettings,'projectdn')
        sTmplFN=self.netPrjDoc.getNodeText(nodeSettings,'latexfn')
        if self.verbose:
            #vtLog.vtLogCallDepth(self,'prjinfo:%s'%nodePrjInf,callstack=False)
            vtLog.vtLogCallDepth(self,'latexFN:%s'%sTmplFN,callstack=False)
            vtLog.vtLogCallDepth(self,'prjDN:%s'%sPrjDN,callstack=False)
        if len(sPrjDN)<=0:
            iRes|=FAULTY_PRJ_DIR
        if len(sTmplFN)<=0:
            iRes|=FAULTY_TMPL_TEX
        sDir=os.path.split(sTmplFN)
        self.sTmplLatexFN=os.path.join(sDir[0],fnUtil.replaceSuspectChars(sDir[1]))
        
        sPrj=self.netPrjDoc.getNodeText(nodePrjInf,'name')
        sClt=self.netPrjDoc.getNodeText(nodePrjInf,'clientshort')
        sFac=self.netPrjDoc.getNodeText(nodePrjInf,'facility')
        #sDocGrp=self.netDoc.getNodeText(self.selDocNode,'key')
        sDocGrp=self.netDoc.getKey(self.selDocNode)
        if len(sDocGrp)<=0:
            iRes|=FAULTY_DOC_GRP
        lstFN=[string.join(map(string.capitalize,[sClt,sPrj]),'')]
        lstFN.append(sDocGrp)
        lstFN.append(sTitleFN)
        lstFN.append(sRev)
        
        sFN='_'.join(lstFN)
        sOutputFN=sFN+'.pdf'
        sDocPath=string.join(lstPath,os.path.sep)
        
        iDocGrpID=self.netDoc.getKeyNum(self.selDocNode)
        if sDocPath.startswith('./'):
            sDocPath=sDocPath[2:]
        sDocPath=sDocPath.replace('/',os.path.sep)
        sDocPath=self.netPrjDoc.GenDocGrpDN(iDocGrpID)
        vtLog.vtLngCur(vtLog.DEBUG,'%r'%{'sDocPath':sDocPath,
                'sFN':sFN,
                'GenDocGrpFN':self.netPrjDoc.GenDocGrpFN(iDocGrpID,sTitle,sRev,'')},
                self.netPrjDoc.GetOrigin())
        
        sFN=os.path.join(sPrjDN,sDocPath,sFN+'.tex')
        self.prjDN=sPrjDN
        def __replaceLatexSpecialChars__(str):
            if str is None:
                return ''
            return string.replace(str,'_','\\_')
        sPrjClientLong=self.netPrjDoc.getNodeText(nodePrjInf,'client')
        sPrjClient=self.netPrjDoc.getNodeText(nodePrjInf,'clientshort')
        sPrjName=self.netPrjDoc.getNodeText(nodePrjInf,'longname')
        sPrjShortCut=self.netPrjDoc.getNodeText(nodePrjInf,'name')
        sFacility=self.netPrjDoc.getNodeText(nodePrjInf,'facility')
        sPrjNr=self.netPrjDoc.getNodeText(nodePrjInf,'prjnumber')
        
        mainDocGrp=self.netDoc.getParent(self.selDocNode)
        sDocGrpTitle=self.netDoc.getNodeTextAttr(mainDocGrp,'title','language',self.lang)
        sDocTitle=self.netDoc.getNodeTextAttr(self.selDocNode,'title','language',self.lang)
        sPrevRev='--'
        sDateCreate=''#self.doc.getNodeText(self.node,'__createdate')
        if len(sDateCreate)==0:
            dt=wx.DateTime.Today()
            sDateCreate=dt.FormatISODate()
            #self.doc.setNodeText(self.node,'__createdate',sDateCreate)
        sDateRevision=''#vtXmlDomTree.getNodeText(self.node,'createdate')
        
        if len(sDateRevision)==0:
            dt=wx.DateTime.Today()
            sDateRevision=dt.FormatISODate()
            #vtXmlDomTree.setNodeText(self.node,'createdate',sDateCreate)
        
        self.dictRepl['clientlong']=__replaceLatexSpecialChars__(sPrjClientLong)
        self.dictRepl['client']=__replaceLatexSpecialChars__(sPrjClient)
        self.dictRepl['project_name']=__replaceLatexSpecialChars__(sPrjName)
        self.dictRepl['project_shortcut']=__replaceLatexSpecialChars__(sPrjShortCut)
        self.dictRepl['project_number']=__replaceLatexSpecialChars__(sPrjNr)
        self.dictRepl['document_group_number']=__replaceLatexSpecialChars__(sDocGrp)
        self.dictRepl['title']=__replaceLatexSpecialChars__(sTitle)
        self.dictRepl['facility']=__replaceLatexSpecialChars__(sFacility)
        self.dictRepl['grpdocument_title']=__replaceLatexSpecialChars__(sDocGrpTitle)
        self.dictRepl['subdocument_title']=__replaceLatexSpecialChars__(sDocTitle)
        self.dictRepl['doc_title']=__replaceLatexSpecialChars__(sTitle)
        self.dictRepl['doc_name']=__replaceLatexSpecialChars__(sTitle)
        self.dictRepl['date_create']=sDateCreate
        self.dictRepl['date_revision']=sDateRevision
        self.dictRepl['revision']=__replaceLatexSpecialChars__(sRev)
        self.dictRepl['replace_revision']=__replaceLatexSpecialChars__(sPrevRev)
        self.dictRepl['output_fn']=__replaceLatexSpecialChars__(sOutputFN)
        if self.verbose:
            keys=self.dictRepl.keys()
            keys.sort()
            for k in keys:
                vtLog.vtLogCallDepth(self,'key:%s %s'%(k,self.dictRepl[k]),callstack=False)
        
        try:
            mode=os.stat(sFN)
            if bSave:
                if parent is not None:
                    sInfo=sFN+"\n"
                    sInfo+='\n'+_('created at:')+time.strftime(_("%Y-%m-%d %H:%M:%S"),time.localtime(mode[stat.ST_CTIME]))
                    sInfo+='\n'+_('last modified at:')+time.strftime(_("%Y-%m-%d %H:%M:%S"),time.localtime(mode[stat.ST_MTIME]))
                    dlg=vtmMsgDialog(parent,_(u'Do you want to overwrite existing file?')+'\n\n'+sInfo ,
                                origin,
                                wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                    if dlg.ShowModal()==wx.ID_NO:
                        iRes|=FAULTY_FILE_EXISTS
                    dlg.Destroy()
                else:
                    iRes|=FAULTY_FILE_EXISTS
        except Exception,list:
            if bSave==False:
                if parent is not None:
                    sInfo=sFN
                    dlg=vtmMsgDialog(parent,_(u'File does not exist!')+' '+sInfo ,
                                origin,
                                wx.OK|wx.ICON_EXCLAMATION )
                    iRes|=FAULTY_FILE_NOT_EXISTS
                    dlg.ShowModal()
                    dlg.Destroy()
                else:
                    iRes|=FAULTY_FILE_NOT_EXISTS
        
        if parent is not None:
            if iRes & FAULTY_VERSION:
                sMsg=_(u'Faulty version!')+' \n\n'+sRev+_(u'should not be used. Use any way?')
                dlg=vtmMsgDialog(parent,sMsg ,
                            origin,
                            wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                if dlg.ShowModal()==wx.ID_YES:
                    iRes&=~FAULTY_VERSION
                dlg.Destroy()
            if iRes & FAULTY_TITLE:
                sMsg=_(u'Faulty title!')+' \n\n'+sTitle+' '+_(u'should not be used. Use any way?')
                dlg=vtmMsgDialog(parent,sMsg ,
                            origin,
                            wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                if dlg.ShowModal()==wx.ID_YES:
                    iRes&=~FAULTY_TITLE
                dlg.Destroy()
            if iRes & FAULTY_PRJ_DIR:
                sMsg=_(u'Project directory not set!')+' \n\n'+_(u'Use vPrjDoc-application to set it.')
                dlg=vtmMsgDialog(parent,sMsg ,
                            origin,
                            wx.OK|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg.Destroy()
            if iRes & FAULTY_TMPL_TEX:
                sMsg=_(u'Latex template file not set!')+' \n\n'+_(u'Use vPrjDoc-application to set it.')
                dlg=vtmMsgDialog(parent,sMsg ,
                            origin,
                            wx.OK|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg.Destroy()
            if iRes & FAULTY_DOC_GRP:
                sMsg=_(u'Document group number not set!')+' \n\n'+_(u'Use vDoc-application to set it.')
                dlg=vtmMsgDialog(parent,sMsg ,
                            origin,
                            wx.OK|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg.Destroy()
        if iRes==0:
            self.sFN=sFN
        else:
            self.sFN=''
        return iRes,sFN
    def GetRelativeFN(self):
        return fnUtil.getFilenameRel2BaseDir(self.sFN,self.prjDN)
    def GetPrjDN(self):
        return self.prjDN
    def __startDoc__(self,tmpl,f):
        lines=tmpl.readlines()
        for l in lines:
            i=string.find(l,'$')
            if i>0:
                j=string.find(l,'$',i+1)
                if j>0:
                    k=l[i+1:j]
                    if self.dictRepl.has_key(k):
                        l=l[:i]+self.dictRepl[k]+l[j+1:]
            f.write(l)
        return 0
    def __procInfo__(self,f,showing,info):
        
        return 0
    def __endDoc__(self,f):
        f.write(os.linesep+'\\end{document}'+os.linesep)
        return 0
    def __procGrph__(self,name,lst,dLimit=None,count=25):
        fMax=0.0
        for it in lst:
            if fMax<it[1]:
                fMax=it[1]
        
        strs=[]
        strs.append('\\begin{center}')
        s=''
        sFmt='  group '
        for i in range(count):
            s+='p{0.02cm}'#'p{0.002\linewidth}'
            sFmt+='& '
        strs.append('  \\begin{longtable}{l|'+s+'}')
        strs.append(sFmt+' \\\\ \\hline')
        strs.append('  \\endfirsthead')
        #tup=('    \\textbf{Nr}','\\textbf{to}','\\textbf{Name}','','','','')
        strs.append(sFmt+' \\\\ \\hline')
        strs.append('  \\endhead')
        i=0
        for it in lst:
            try:
                limit=it[2]
            except:
                limit=None
            iBlks=round((it[1]/fMax)*count)
            #idx=i%len(self.COLORS)
            if limit is None:
                if dLimit is not None:
                    strsLim=string.split(it[0],':')
                    d=dLimit
                    for sLim in strsLim:
                        try:
                            limit,d=d[sLim]
                        except:
                            limit=None
                else:
                    limit=None
            if limit is None:
                idx=int(it[1]/fMax*float(len(self.COLORS)-1))
            else:
                fWidth=limit/float(len(self.COLORS)-4)
                iMax=len(self.COLORS)-1
                try:
                    idx=int(math.ceil(it[1]/fWidth))
                except:
                    idx=iMax
                if idx>=iMax:
                    idx=iMax
            tup=(it[0],iBlks, self.COLORS[idx][0], self.COLORS[idx][1],'%6.2f'%it[1])
            strs.append('   %s & \\multicolumn{%d}{|>{\\color{%s}\\columncolor{%s}}l}{%s} \\\\'%tup)
            i+=1
        strs.append('  \\caption{%s}'%name)
        strs.append('  \\label{fig:%s}'%name)
            
        strs.append('  \\end{longtable}')
        strs.append('\\end{center}')
        return strs
        
    def Build(self,info,*args):
        if len(self.sFN)<=0:
            return -2
        if len(self.sTmplLatexFN)<=0:
            return -1
        try:
            sDir=os.path.split(self.sFN)
            os.makedirs(sDir[0])
        except:
            pass
        self.args=args
        fnUtil.shiftFile(self.sFN)
        e=u'ISO-8859-1'
        tmpl=codecs.open(self.sTmplLatexFN,'r',e)
        f=codecs.open(self.sFN,"w",e)
        self.__startDoc__(tmpl,f)
        self.__procInfo__(f,info)
        self.__endDoc__(f)
        f.close()
        tmpl.close()
        
        return 0
    def Open(self):
        if self.sFN is None:
            return
        if len(self.sFN)<=0:
            return -2
        sExts=string.split(self.sFN,'.')
        sExt=sExts[-1]
        fileType = wx.TheMimeTypesManager.GetFileTypeFromExtension(sExt)
        #filename=fnUtil.getAbsoluteFilenameRel2BaseDir(sFN,self.prjDN)
        mime = fileType.GetMimeType()
        if mime is None:
            mime="File extension"
        cmd = fileType.GetOpenCommand(self.sFN, mime)
        wx.Execute(cmd)
    
        
