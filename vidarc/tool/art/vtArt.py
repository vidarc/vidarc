#----------------------------------------------------------------------------
# Name:         vtArt.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060216
# CVS-ID:       $Id: vtArt.py,v 1.9 2010/02/21 00:20:35 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import wx
#import vLogFallBack
#vLogFallBack.logDebug('import;start',__name__)
from wx import NullBitmap,NullImage,EmptyIcon
import vidarc.tool.art.images as images
import fnmatch

#vLogFallBack.logDebug('import;done',__name__)

__j=0
lNames=[]
lIds=[]
def addBitmaps(name):
    global __j
    global lNames
    if fnmatch.fnmatch(name,'get*Bitmap'):
        __j+=1
        lNames.append(name)
        s='%s="%s"'%(name[3:-6],name)
        lIds.append(s)
    return __j


__i=0
def add():
    global __i
    __i+=1
    return __i

#vLogFallBack.logDebug('j:%d;lNames:%s;lIds:%s'%(__j,repr(lNames),repr(lIds)),__name__)

lMap=map(addBitmaps,dir(images))

#vLogFallBack.logDebug('lMap:%s'%(repr(lMap)),__name__)
#vLogFallBack.logDebug('j:%d;lNames:%s;lIds:%s'%(__j,repr(lNames),repr(lIds)),__name__)

def addBitmapFromMod(m):
    lN=[name[3:-6] for name in dir(m) if fnmatch.fnmatch(name,'get*Bitmap')]
    return lN

lN=addBitmapFromMod(images)
for it in lIds:
    exec(it)

class vtArtProvider:
    def __init__(self):
        self.bmpNull=NullBitmap
        self.imgNull=NullImage
        self.dCache={}
        self.dCacheImg={}
    def GetBitmap(self,artid,client='VIDARC',sz=(16,16)):
        if artid in self.dCache:
            return self.dCache[artid]
        else:
            img=self.CreateBitmap(artid,client,sz)
            self.dCache[artid]=img
            return img
    def CreateBitmap(self, artid, client, size):
        try:
            bmp=getattr(images,artid)()
            return bmp
        except:
            pass
        return self.bmpNull
    def GetImage(self,artid,client='VIDARC',sz=(16,16)):
        if artid in self.dCacheImg:
            return self.dCacheImg[artid]
        else:
            img=self.CreateImage(artid,client,sz)
            self.dCacheImg[artid]=img
            return img
    def CreateImage(self, artid, client, size):
        try:
            #global lNames
            #bmp=getattr(images,lNames[artid])()
            bmp=getattr(images,artid[:-6]+'Image')()
            return bmp
        except:
            pass
        return self.bmpNull

__artProv=None
__bInstalled=False

def Install():
    global __bInstalled
    if __bInstalled:
        return
    __bInstalled=True
    global __artProv
    __artProv=vtArtProvider()
def DeInstall():
    global __bInstalled
    if __bInstalled==False:
        return
    __bInstalled=False
    global __artProv
    __artProv=None
def getBitmap(artid,client='VIDARC',sz=(16,16)):
    global __bInstalled
    if __bInstalled:
        pass
    else:
        Install()
    global __artProv
    return __artProv.GetBitmap(artid, client, sz)

def getIcon(bmp):
    icon = EmptyIcon()
    icon.CopyFromBitmap(bmp)
    return icon

def getImage(artid,client='VIDARC',sz=(16,16)):
    global __bInstalled
    if __bInstalled:
        pass
    else:
        Install()
    global __artProv
    return __artProv.GetImage(artid, client, sz)

#Install()
