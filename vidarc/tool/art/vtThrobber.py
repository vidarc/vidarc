#----------------------------------------------------------------------------
# Name:         vtThrobber.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060223
# CVS-ID:       $Id: vtThrobber.py,v 1.2 2010/05/21 23:01:10 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import  wx
import  wx.lib.throbber as  throb

import vidarc.tool.art.images_throbber as images_throbber

class vtThrobber(throb.Throbber):
    def __init__(self,parent,id=-1,pos=wx.DefaultPosition,size=wx.DefaultSize,style=0,name='vtThrobber',frameDelay=0.2):
        
        throb.Throbber.__init__(self,parent , id, [
                images_throbber.getThrobber_00Bitmap(),
                images_throbber.getThrobber_01Bitmap(),
                images_throbber.getThrobber_02Bitmap(),
                images_throbber.getThrobber_03Bitmap(),
                images_throbber.getThrobber_04Bitmap(),
                images_throbber.getThrobber_05Bitmap(),
                images_throbber.getThrobber_06Bitmap(),
                images_throbber.getThrobber_07Bitmap(),
                images_throbber.getThrobber_08Bitmap(),
                images_throbber.getThrobber_09Bitmap(),
                images_throbber.getThrobber_10Bitmap(),
                images_throbber.getThrobber_11Bitmap(),
                images_throbber.getThrobber_12Bitmap(),
                images_throbber.getThrobber_13Bitmap(),
                images_throbber.getThrobber_14Bitmap(),
                images_throbber.getThrobber_15Bitmap(),
                ],
                size=(15, 15),frameDelay = frameDelay)

class vtThrobberDelay(throb.Throbber):
    def __init__(self,parent,id=-1,pos=wx.DefaultPosition,size=wx.DefaultSize,style=0,name='vtThrobberDelay',frameDelay=0.2):
        
        throb.Throbber.__init__(self,parent , id, [
                images_throbber.getThrobber00Bitmap(),
                images_throbber.getThrobber01Bitmap(),
                images_throbber.getThrobber02Bitmap(),
                images_throbber.getThrobber03Bitmap(),
                images_throbber.getThrobber04Bitmap(),
                images_throbber.getThrobber05Bitmap(),
                images_throbber.getThrobber06Bitmap(),
                images_throbber.getThrobber07Bitmap(),
                images_throbber.getThrobber08Bitmap(),
                images_throbber.getThrobber09Bitmap(),
                images_throbber.getThrobber10Bitmap(),
                images_throbber.getThrobber11Bitmap(),
                images_throbber.getThrobber12Bitmap(),
                images_throbber.getThrobber13Bitmap(),
                images_throbber.getThrobber14Bitmap(),
                images_throbber.getThrobber15Bitmap(),
                images_throbber.getThrobber16Bitmap(),
                images_throbber.getThrobber17Bitmap(),
                images_throbber.getThrobber18Bitmap(),
                images_throbber.getThrobber19Bitmap(),
                images_throbber.getThrobber20Bitmap(),
                images_throbber.getThrobber21Bitmap(),
                images_throbber.getThrobber22Bitmap(),
                images_throbber.getThrobber23Bitmap(),
                images_throbber.getThrobber24Bitmap(),
                images_throbber.getThrobber25Bitmap(),
                images_throbber.getThrobber26Bitmap(),
                images_throbber.getThrobber27Bitmap(),
                images_throbber.getThrobber28Bitmap(),
                images_throbber.getThrobber29Bitmap(),
                images_throbber.getThrobber30Bitmap(),
                images_throbber.getThrobber31Bitmap(),
                ],
                size=(15, 15),frameDelay = frameDelay)

class vtThrobberFree(throb.Throbber):
    def __init__(self,parent,id=-1,pos=wx.DefaultPosition,size=wx.DefaultSize,style=0,
                name='vtThrobberFree',frameDelay=0.2,lImg=[]):
        
        throb.Throbber.__init__(self,parent , id, lImg,
                size=(15, 15),frameDelay = frameDelay)


