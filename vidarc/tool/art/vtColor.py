#----------------------------------------------------------------------------
# Name:         vtColor.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060221
# CVS-ID:       $Id: vtColor.py,v 1.4 2014/06/19 17:20:12 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

#  +++++++++++++++++++  use colour DB  +++++++++++++++++++++
import wx.lib.colourdb

def addColor(i):
    name=LST_COLOR_LIST[i]
    tup=LST_COLOR_INFO_LIST[i]
    s='%s=%s'%(name.replace(' ','_'),(tup[1],tup[2],tup[3]))
    return s

LST_COLOR_LIST=wx.lib.colourdb.getColourList()
LST_COLOR_INFO_LIST=wx.lib.colourdb.getColourInfoList()
lColor=map(addColor,range(len(LST_COLOR_LIST)))
for it in lColor:
    exec(it)
#  -------------------  use colour DB  ---------------------

NUM_0       =(224,164, 96)
NUM_1       =(220, 20, 60)
NUM_2       =(204,153,221)
NUM_3       =(100,149,237)
NUM_4       =(  0,221,221)
NUM_5       =(102,205,170)
NUM_6       =( 60,179,113)
NUM_7       =(222,184,135)
NUM_8       =(255,127, 80)
NUM_9       =(255,203,  0)

if 1:
    SATURDAY    =NUM_3
    SUNDAY      =NUM_4
    MONDAY      =NUM_5
    TUESDAY     =NUM_6
    WEDNESDAY   =NUM_7
    THURSDAY    =NUM_8
    FRIDAY      =NUM_9
else:
    SATURDAY    =(255,  0,  0)
    SUNDAY      =(255,  0,  0)
    MONDAY      =(  0,  0,  0)
    TUESDAY     =(  0,  0,  0)
    WEDNESDAY   =(  0,  0,  0)
    THURSDAY    =(  0,  0,  0)
    FRIDAY      =(  0,  0,  0)

def Scale(color,f):
    return (int(color[0]*float(f)),int(color[1]*float(f)),int(color[2]*float(f)))
def ScaleFloat(color,f):
    return (color[0]*float(f),color[1]*float(f),color[2]*float(f))
