# -*- coding: iso-8859-1 -*-
#----------------------------------------------------------------------
# Name:         vtInOutFileTimes.py
# Purpose:      file time helper functions
#
# Author:       Walter Obweger
#
# Created:      20080508
# CVS-ID:       $Id: vtInOutFileTimes.py,v 1.2 2009/02/22 17:54:31 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import sys

if sys.platform.startswith('win'):
    SYSTEM=1
    import win32file
    import win32con
    import pywintypes
elif sys.platform.startswith('linux'):
    SYSTEM=0
else:
    SYSTEM=0

def setFileTimes(sFullFN,zCreate,zAccess,zModify):
    if SYSTEM==1:
        hFile = win32file.CreateFile (
                            sFullFN,
                            win32con.GENERIC_WRITE ,
                            win32con.FILE_SHARE_READ | win32con.FILE_SHARE_WRITE,
                            None,
                            win32con.OPEN_EXISTING,
                            win32con.FILE_FLAG_BACKUP_SEMANTICS,
                            None
                            )
        oC=pywintypes.Time(zCreate)
        oA=pywintypes.Time(zAccess)
        oW=pywintypes.Time(zModify)
        win32file.SetFileTime(hFile,oC,oA,oW)
        win32file.CloseHandle(hFile)
