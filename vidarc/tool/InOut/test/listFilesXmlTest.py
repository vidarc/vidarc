#----------------------------------------------------------------------------
# Name:         listFiles.py
# Purpose:      list files test script
# Author:       Walter Obweger
#
# Created:      20060407
# CVS-ID:       $Id: listFilesXmlTest.py,v 1.1 2006/04/11 17:48:17 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.InOut.listFilesXml import *

if __name__=='__main__':
    iTest=2
    if (iTest&1)==1:
        dir1=DirEntryXml()
        # this test processes the specified directory
        # and write the result to the xml-file
        dir1.process('c:/temp/sigpack',style=1)
        #dir1.read('c:/temp')
        dir1.write('test_dir1.xml',0)
    if (iTest&2)==2:
        dir2=DirEntryXml()
            # this test reads a xml-file (previously generated
        # by test 1
        dir2.read('test_dir2.xml')
        dir2.write('test_dir3.xml',0)
