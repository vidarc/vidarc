<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html>
<head></head>
<body>
<table border="1">
<tr>
<td><b></b></td>
<td><b></b></td>
<td><b></b></td>
</tr>
<xsl:apply-templates/>
</table>
</body>
</html>
</xsl:template>

<xsl:template match="//world/continent">
<tr>
<td><xsl:value-of select="period"/></td>
<td><xsl:value-of select="period"/></td>
<td><xsl:value-of select="period"/></td>
</tr>
</xsl:template>
</xsl:stylesheet>
