#----------------------------------------------------------------------------
# Name:         csvObjTest.py
# Purpose:      CSV object test script
# Author:       Walter Obweger
#
# Created:      20060407
# CVS-ID:       $Id: csvObjTest.py,v 1.1 2006/04/11 17:48:17 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------


import time
from vidarc.tool.InOut.csvObj import *

def demo_merge():
	print '----- demo merge'
	d1=csv.getKeyObjs(['name'])
	#print len(d1.values())
	lst=map(None,d1.keys(),d1.values())
	#for i in lst:
	#	print i[0],i[1]
	#print
	print
	csv_third=vtIOcsvObjs()
	csv_third.merge([csv,['name'],['size']],[csv_second,['filename'],['md5']])
	print
	csv_third.showOverview()
	csv_third.show()

def demo_reduce():
	print '----- demo reduce'
	csv_fourth=vtIOcsvObjs()
	csv_fourth.reduce([csv,['name'],['size']],[csv_second,['filename'],['md5']])
	print
	csv_fourth.showOverview()
	csv_fourth.show()
def demo_compare():
	print '----- demo compare'
	inf=open('test03.csv')
	lines=inf.readlines()
	inf.close()

	zStart=time.clock()
	csv1=vtIOcsvObjs()
	csv1.setHdr(lines[1][:-1])
	for l in lines[2:]:
		csv1.addLineObj(l[:-1])

	inf=open('test04.csv')
	lines=inf.readlines()
	inf.close()

	csv2=vtIOcsvObjs()
	csv2.setHdr(lines[1][:-1])
	for l in lines[2:]:
		csv2.addLineObj(l[:-1])

	csv_res=vtIOcsvObjs()
	csv_res.compare([csv1,['name'],['md5']],[csv2,['name'],['md5']])
	csv_res.showOverview()
	csv_res.show()


if __name__=='__main__':
	print 'aab <-> baa',strCmp('aab','baa'),cmp('aab','baa')
	print 'aab <-> aab',strCmp('aab','aab'),cmp('aab','aab')
	print 'aab <-> aac',strCmp('aab','aac'),cmp('aab','aac')
	print 'aab <-> aaba',strCmp('aab','aaba'),cmp('aab','aaba')
	print 'aabaa <-> aaba',strCmp('aabaa','aaba'),cmp('aabaa','aaba')
	print

	inf=open('test01.csv')
	lines=inf.readlines()
	inf.close()

	zStart=time.clock()
	csv=vtIOcsvObjs()
	csv.setHdr(lines[1][:-1])
	for l in lines[2:]:
		csv.addLineObj(l[:-1])
	zEnd=time.clock()
	print 'time elapsed:',zEnd-zStart
	csv.evalObjs('dir_file','os.path.split(o.name)')
	zEnd2=time.clock()
	print 'time elapsed:',zEnd2-zEnd

	csv.showOverview()
	#csv.show()

	inf=open('test02.csv')
	lines=inf.readlines()
	inf.close()
	csv_second=vtIOcsvObjs()
	csv_second.setHdr(lines[0][:-1])
	for l in lines[1:]:
		csv_second.addLineObj(l[:-1])

	#demo_merge()
	#demo_reduce()
	demo_compare()

	print 'generate output'
	zStart=time.clock()
	csv_second.writeSimpleXml('test02.xml')
	zEnd=time.clock()
	print 'time elapsed:',zEnd-zStart
