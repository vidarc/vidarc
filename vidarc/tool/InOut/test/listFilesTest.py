#----------------------------------------------------------------------------
# Name:         listFiles.py
# Purpose:      list files test script
# Author:       Walter Obweger
#
# Created:      20060407
# CVS-ID:       $Id: listFilesTest.py,v 1.1 2006/04/11 17:48:17 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------


import os.path,os
from vidarc.tool.InOut.listFiles import *

def shiftFile(fn,max=2):
	hfn="%s.%d"%(fn,max)
	if os.path.exists(hfn):
		try:
			os.remove(hfn)
		except:
			pass
	ohfn=hfn
	l=range(0,max)
	l.reverse()
	for i in l:
		hfn="%s.%d"%(fn,i)
		print i,hfn
		if os.path.exists(hfn):
			try:
				print hfn,ohfn
				os.rename(hfn,ohfn)
			except:
				pass
		ohfn=hfn
	try:
		os.rename(fn,hfn)
	except:
		pass

def delShiftFile(fn,max=2):
	for i in range(0,max+1):
		hfn="%s.%d"%(fn,i)
		try:
			os.remove(hfn)
		except:
			pass

def genTstFile(fn,num):
	s="%d"%num
	f=open(fn,"w")
	f.write(s)
	f.close()

def tstShift(fn,max):
	delShiftFile(fn,max)
	for i in range(0,max):
		shiftFile(fn,max)
		genTstFile(fn,i)

def demoSize():
	print getSizeStr(1022,1)
	print getSizeStr(1022*1024,1)
	print getSizeStr(1022*1024*1024,1)
	print getSizeStr(1022*1024*1024*1024,1)
	print getSizeStr(1022*1024*1024*1024*1024,1)
	print getSizeStr(1022*1024*1024*1024*1024*1024,1)
	
def demo():
	obj=DirEntry()
	obj.read('c:/temp')
	obj.show()

if __name__=='__main__':
	demoSize()
	#listFiles('g:/VB_lonza/LON/7_msr_automation')
	#listFiles('g:/VB_Lonza/LON/7_msr_automat/771')
	# use this function for write filenames recusivly to a csv-file
	#writeListFiles2File('c:/temp/LON_771_listFiles_local.csv',
	#					'g:/VB_Lonza/LON/7_msr_automat/771',
	#					fileinfo2str)
	#writeListFiles2File('c:/temp/LON_771_listFiles_PCserv.csv',
	#					'l:/LON/7_msr_automat/771',
	#					fileinfo2str)

	if 0:
		filters={}
		filters['dir_exFilter_one']=['*/CVS']
		filters['file_exFilter_one']=['*.bak','*.pyc','*.aut']
		writeListFiles2File('c:/temp/listFiles_tst.csv',
						'c:/Data',
						fileinfo_MD5_4cmp2str,skip_filename_characters=8,filters=filters)

	#listFiles('g:/VB_Lonza/LON/7_msr_automat/771',visitfile)

	# compare 2 files and write the comparision to file
	#diff=compareFiles('a.txt','b.txt')
	#logCompareFiles('l.txt',diff)
	#tstShift('tstShift.txt',2)

	#shiftFile('c:/temp/listFiles_tst.csv',2)
	#writeListFiles2File('c:/temp/listFiles_tst.csv',
	#					'c:/Data',
	#					fileinfo_MD5_4cmp2str,skip_filename_characters=8)
	#diff=compareFiles('c:/temp/listFiles_tst.csv','c:/temp/listFiles_tst.csv.0')
	#logCompareFiles('c:/temp/listFiles_tst.csv.log.txt',diff)
