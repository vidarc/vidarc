<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html>
<head></head>
<body>
<b>Date created:</b><xsl:value-of select="/csv2xml/date"/><br/>
<b>Time created:</b><xsl:value-of select="/csv2xml/time"/><br/>
<b>AscTime created:</b><xsl:value-of select="/csv2xml/asctime"/><br/>


<table border="1">
<tr>
<td><b>filename</b></td>
<td><b>size</b></td>
<td><b>md5</b></td>
</tr>
<xsl:apply-templates/>
</table>
</body>
</html>
</xsl:template>

<xsl:template match="//csv2xml">
<tr>
<td><xsl:value-of select="date"/></td>
<td><xsl:value-of select="time"/></td>
<td><xsl:value-of select="asctime"/></td>
</tr>
</xsl:template>

<xsl:template match="//csv2xml/csv/row">
<tr>
<td><xsl:value-of select="filename"/></td>
<td><xsl:value-of select="size"/></td>
<td><xsl:value-of select="md5"/></td>
</tr>
</xsl:template>
</xsl:stylesheet>
