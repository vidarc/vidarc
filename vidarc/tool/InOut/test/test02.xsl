<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body>
    <h2>File Content Output</h2>
    <xsl:apply-templates/> 
    </body>
    </html>
    </xsl:template>

    <xsl:template match="csv2xml">
    <p>
    <xsl:apply-templates select="date"/> 
    <xsl:apply-templates select="time"/>
    <xsl:apply-templates select="csv"/>
    </p>
    </xsl:template>
    
    <xsl:template match="date">Date: <span style="color:#ff0000">
    <xsl:value-of select="."/></span><br />
    </xsl:template>
    
    <xsl:template match="time">Time: <span style="color:#00ff00">
    <xsl:value-of select="."/></span><br />
    </xsl:template>
    
    <xsl:template match="csv">
    <table border="1">
      <tr bgcolor="#9acd32">
        <th>filename</th>
        <th>size</th>
      </tr>
      <xsl:for-each select="row">
      <xsl:sort select="size" data-type="number" order="descending" />
      <tr>
        <td><xsl:value-of select="filename"/></td>
      	<xsl:choose>
          <xsl:when test="type = file">
            <td bgcolor="#ff00ff">
            <xsl:value-of select="size"/></td>
          </xsl:when>
          <xsl:otherwise>
            <td><xsl:value-of select="size"/></td>
          </xsl:otherwise>
        </xsl:choose>
      </tr>
      </xsl:for-each>
    </table>
    </xsl:template>

</xsl:stylesheet>