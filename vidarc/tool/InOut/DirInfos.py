import vidarc.tool.InOut.listFiles as listFiles
import vidarc.tool.InOut.csvObj as csvObj
import os.path
import string

class DirInfos:
	def __init__(self):
		self.flat=1
		self.base=''
		self.hdrs=['name','size']
		self.dirs={}
		self.files={}
		self.size=0
		self.csvObj=csvObj.csvObjs()
		
	def _getUniqueKey(self,d,k,i=-1):
		if i==-1:
			key2check=k
		else:
			key2check='%s:%d'%(k,i)
		try:
			d[key2check]
			return self._getUniqueKey(d,k,i+1)
		except:
			return key2check
	def getFileObj(self,path,k):
		if self.flat==0:
			dirObj=self.getPath(path[:-1])
		else:
			dirObj=self
		try:
			return dirObj.files[k]
		except:
			return None
	def getDirObj(self,path,k):
		if self.flat==0:
			dirObj=self.getPath(path[:-1])
		else:
			dirObj=self
		try:
			return dirObj.dirs[k]
		except:
			return None
	def addObj(self,path,o):
		if o.type=='file':
			if self.flat==0:
				dirObj=self.getPath(path[:-1])
			else:
				dirObj=self
			tmp=dirObj._getUniqueKey(dirObj.files,path[-1])
			dirObj.files[tmp]=o
		elif o.type=='cmp':
			if self.flat==0:
				dirObj=self.getPath(path[:-1])
			else:
				dirObj=self
			sCmp=self.csvObj.getCmpStr(o)
			tmp=dirObj._getUniqueKey(dirObj.files,sCmp+'_'+path[-1])
			dirObj.files[tmp]=o
		elif o.type=='dir':
			if self.flat==0:
				dirObj=self.getPath(path[:-1])
			else:
				return
			tmp=dirObj._getUniqueKey(dirObj.dirs,path[-1])
			dirObj.dirs[tmp]=o
	def getHeaders(self):
		return self.hdrs
	def getFiles(self,path):
		base=self.getPath(path)
		lst=base.files.keys()
		lst.sort()
		return lst
	def getDirs(self,path):
		base=self.getPath(path)
		lst=base.dirs.keys()
		lst.sort()
		return lst
	def getPath(self,path):
		if len(path)==0:
			return self
		try:
			tmp=self.dirs[path[0]]
		except:
			tmp=DirInfos()
			tmp.base=self.base+'/'+path[0]
			self.dirs[path[0]]=tmp
		return tmp.getPath(path[1:])
	def setFlat(self,flag=1):
		self.flat=flag
	def _calc(self):
		iSize=0
		try:
			for d in self.dirs.values():
				d._calc()
			for f in self.files.values():
				iSize=iSize+string.atol(f.size)
			for d in self.dirs.values():
				iSize=iSize+d.size
			self.size=iSize
			self.name=self.base
			self.type='directory'
		except:
			self.size='???'
			self.name=self.base
			self.type='directory'
	def clear(self):
		self.dirs={}
		self.files={}
		self.size={}
		self.csvObj.clear()
	def analyse(self):
		self.dirs={}
		self.files={}
		self.size={}
		oldPathLevel=-1
		for o in self.csvObj.objs:
			dLst=self.getPathLst(o.name)
			iLevel=len(dLst)
			#tmp=os.path.split(o.name)
			#dLst=self.getPathLst(tmp[0])
			self.addObj(dLst,o)
		self._calc()
	def read(self,fn):
		try:
			inf=open(fn,'r')
			lines=inf.readlines()
			inf.close()
			self.base=string.split(lines[0][:-1],',')[1]
			self.hdrs=string.split(lines[1][:-1],',')
			self.csvObj=csvObj.csvObjs()
			self.csvObj.setHdr(lines[1][:-1])
			for l in lines[2:]:
				self.csvObj.addLineObj(l[:-1])
			self.analyse()
		except:
			self.clear()
	def getPathLst(self,fn):
		if self.base is not None:
			tmp=fn[len(self.base):]
		else:
			tmp=fn
		d=os.path.splitdrive(tmp)
		if self.base is None:
			self.base=d[0]
		if self.base is None:
			self.base=''
		t=d[1]
		lstPath=[]
		while len(t)>1:
			m=os.path.split(t)
			lstPath.append(m[1])
			t=m[0]
			#t=''
		lstPath.reverse()
		return lstPath
	def test(s):
		d=os.path.splitdrive(s)
		t=d[1]
		lst=[]
		while len(t)>1:
			m=os.path.split(t)
			print 'pathsplit',m
			lst.append(m[1])
			t=m[0]
			#t=''
		lst.reverse()
		print lst
	def show(self,recursion=10,level=0):
		prefix=''
		for i in range(0,level):
			prefix=prefix+"  "
		print prefix,self.base
		print prefix,'dirs'
		keys=self.dirs.keys()
		keys.sort()
		for k in keys:
			print prefix,'  ',k
		print prefix,'files'
		keys=self.files.keys()
		keys.sort()
		for k in keys:
			print prefix,'  ',k
		keys=self.dirs.keys()
		keys.sort()
		for k in keys:
			if recursion>0:
				self.dirs[k].show(recursion=recursion-1,level=level+1)
				
class TestObj:
	def __init__(self,n):
		self.name=n
		self.type='file'
if __name__=='__main__':
	reload(csvObj)
	pass
	lst=[]
	lst.append(TestObj('C:\TEMP/SCA_734_symb_01.pdf'))
	lst.append(TestObj('C:\TEMP/_ISTMP1.DIR/_ISTMP0.DIR/BBrd1.bmp'))
	lst.append(TestObj('C:\TEMP/_ISTMP1.DIR/_ISTMP0.DIR/BBrd4.bmp'))
	lst.append(TestObj('D:\Data\proj\company\python/app/InOut/Script1.py'))
	lst.append(TestObj('D:\Data\proj\company\python/app/InOut/test.ini'))
	#lst.append(TestObj(''))
	#lst.append(TestObj(''))
	print '--------------flat---------------'
	o=DirInfos()
	for i in lst:
		dlst=o.getPathLst(i.name)
		o.addObj(dlst,i)
	#o.addObj(['Data','proj'],'file01.txt')
	o.show()
	print '--------------flat---------------'
	print
	print
	print '--------------hiear---------------'
	o=DirInfos()
	o.flat=0
	for i in lst:
		dlst=o.getPathLst(i.name)
		print dlst
		o.addObj(dlst,i)
	#o.addObj(['Data','proj'],'file01.txt')
	o.show(recursion=10)
	print '--------------hiear---------------'
