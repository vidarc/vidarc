#----------------------------------------------------------------------------
# Name:         genFN.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: genFN.py,v 1.2 2007/01/22 16:57:15 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import time,os,platform

def getCurDateFN(name,ext=None):
    if ext is None:
        return '_'.join([name,
                time.strftime('%y%m%d',time.localtime(time.time()))])
    else:
        return '.'.join(['_'.join([name,
                time.strftime('%y%m%d',time.localtime(time.time()))]),ext])
def getCurDateTimeFN(name,ext=None):
    if ext is None:
        return '_'.join([name,
                time.strftime('%y%m%d_%H%M%S',time.localtime(time.time()))])
    else:
        return '.'.join(['_'.join([name,
                time.strftime('%y%m%d_%H%M%S',time.localtime(time.time()))]),ext])
def getCurDateTimeAddLstFN(name,lAdd,ext=None):
    if ext is None:
        return '_'.join([name,
                time.strftime('%y%m%d_%H%M%S',time.localtime(time.time())),
                '_'.join(lAdd)])
    else:
        return '.'.join(['_'.join([name,
                time.strftime('%y%m%d_%H%M%S',time.localtime(time.time())),
                '_'.join(lAdd)]),ext])
def getCurDateTimeHostFN(name,ext=None):
    if ext is None:
        return '_'.join([name,
                time.strftime('%y%m%d_%H%M%S',time.localtime(time.time())),
                platform.uname()[1]])
    else:
        return '.'.join(['_'.join([name,
                time.strftime('%y%m%d_%H%M%S',time.localtime(time.time())),
                platform.uname()[1]]),ext])
def getCurDateTimeHostAddLstFN(name,lAdd,ext=None):
    if ext is None:
        return '_'.join([name,
                time.strftime('%y%m%d_%H%M%S',time.localtime(time.time())),
                platform.uname()[1],
                '_'.join(lAdd)])
    else:
        return '.'.join(['_'.join([name,
                time.strftime('%y%m%d_%H%M%S',time.localtime(time.time())),
                platform.uname()[1],
                '_'.join(lAdd)]),ext])
    
def getCurGMDateFN(name,ext=None):
    if ext is None:
        return '_'.join([name,
                time.strftime('%y%m%d',time.gmtime(time.time()))])
    else:
        return '.'.join(['_'.join([name,
                time.strftime('%y%m%d',time.gmtime(time.time()))]),ext])
def getCurGMDateTimeFN(name,ext=None):
    if ext is None:
        return '_'.join([name,
                time.strftime('%y%m%d_%H%M%S',time.gmtime(time.time()))])
    else:
        return '.'.join(['_'.join([name,
                time.strftime('%y%m%d_%H%M%S',time.gmtime(time.time()))]),ext])
def getUnique(nFN):
    bOk=False
    i=0
    sExts=nFN.split('.')
    sExt=sExts[-1]
    while bOk==False:
        if i==0:
            s=''
        else:
            s='%d.'%i
        
        nTestFN=nFN[:-len(sExt)]+s+sExt
        try:
            os.stat(nTestFN)
            i+=1
        except:
            nFN=nTestFN
            bOk=True
    return nFN
if __name__=='__main__':
    print getCurDateFN('test','txt')
    print getCurDateTimeFN('test','txt')
    print getCurGMDateFN('test','txt')
    print getCurGMDateTimeFN('test','txt')
