#----------------------------------------------------------------------------
# Name:         vtInOutReadThread.py
# Purpose:
# Author:       Walter Obweger
#
# Created:      20060604
# CVS-ID:       $Id: vtInOutReadThread.py,v 1.11 2009/02/05 21:55:07 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.vtThread import vtThread

import time
import sys,os,stat,md5
from os.path import join, getsize

import vidarc.tool.InOut.listFiles as listFiles

VERBOSE=0

class vtInOutReadThread(vtThread):
    """
style
    0x01 ... calc details
    0x02 ... 
    0x04 ... 
    0x0F ... calc detail mask
    0x10 ... 0 = post every tenth block / 1 = post all sizes
    0x20 ... calc file info possible
    """
    MASK_DETAIL=0x0F
    POST_ALL_SIZES=0x10
    CALC_FILE_INFO=0x20
    def __init__(self,par,bPost=False,verbose=0):
        vtThread.__init__(self,par,bPost=bPost,verbose=verbose)
        self.ClearCount()
    def ClearCount(self):
        self.bSize=False
        self.iCount=0
        self.iCountDir=0
        self.iCountFile=0
        self.lSize=0
        self.lAct=0
    def GetCount(self):
        return self.iCount
    def GetCountDir(self):
        return self.iCountDir
    def GetCountFile(self):
        return self.iCountFile
    def GetCountTup(self):
        return self.iCount,self.iCountDir,self.iCountFile,self.lSize
    def GetSize(self):
        return self.lSize
    def GetAct(self):
        return self.lAct
    def ReadOld(self,sDN,filter,rec,skip,style,func,*args,**kwargs):
        self.Do(self.procRead,sDN,filter,rec,skip,style,func,*args,**kwargs)
    def Read(self,sDN,filter,rec,skip,style,func,*args,**kwargs):
        self.Do(self.procReadWalk,sDN,filter,rec,skip,style,True,func,*args,**kwargs)
    def ReadDir(self,sDN,filter,rec,skip,func,*args,**kwargs):
        self.Do(self.procReadWalk,sDN,filter,rec,skip,0,False,func,*args,**kwargs)
    def CalcCount(self,sDN,filter,rec,skip,func,*args,**kwargs):
        self.Do(self.procCalcCount,sDN,filter,rec,skip,func,*args,**kwargs)
    def CalcSize(self,sDN,filter,rec,skip,func,*args,**kwargs):
        self.Do(self.procCalcSize,sDN,filter,rec,skip,func,*args,**kwargs)
    def procRead(self,sDN,filter,rec,skip,style,func,*args,**kwargs):
        try:
            dn=sDN.replace('\\','/')
            if dn[-1]=='/':
                skip-=1
            if dn[-1]==':':
                dn=dn+u'/'
            #    dn=dn[:-1]
            if self.__isLogInfo__():
                self.__logInfo__('start;dn:%s'%dn)
            self.lSize=self.calcSize(dn,filter,rec,skip)
            if self.__isLogInfo__():
                self.__logInfo__('size;dn:%s'%dn)
            self.lAct=0
            if self.bPost:
                self.Post('proc',self.lAct,self.lSize)
                #wx.PostEvent(self.par,vtInOutReadThreadElements(self.lAct,self.lSize))
            dirEntry=listFiles.DirEntry(dn)
            self.read(dirEntry,dn,filter,rec,skip,style)
            if func is not None:
                if self.Is2Stop():
                    func(sDN,None,*args,**kwargs)
                else:
                    func(sDN,dirEntry,*args,**kwargs)
            if self.bPost:
                if self.Is2Stop():
                    self.Post('aborted')
                else:
                    self.Post('finished')
                #wx.PostEvent(self.par,vtInOutReadThreadFinished())
        except:
            #vtLog.vtLngTB(self.GetOrigin())
            self.__logTB__()
            if self.bPost:
                self.Post('aborted')
                #wx.PostEvent(self.par,vtInOutReadThreadAborted())
        if self.__isLogInfo__():
            self.__logInfo__('fin;dn:%s'%dn)
    def procReadWalk(self,sDN,filter,rec,skip,style,bFiles,func,*args,**kwargs):
        try:
            self.ClearCount()
            dn=sDN.replace('\\','/')
            #if dn[-1]=='/':
            #    skip-=1
            #if dn[-1]==':':
            #    dn=dn+u'/'
            #    dn=dn[:-1]
            if self.__isLogInfo__():
                self.__logInfo__('start;dn:%s'%dn)
            self.procCalcQuick(dn,rec)
            if (style & self.MASK_DETAIL)==0 and (style & self.CALC_FILE_INFO)==0:
                #self.procCalcCount(dn,filter,rec,skip,None)
                #self.procCalcSize(dn,filter,rec,skip,None)
                pass
            else:
                #self.procCalcCount(dn,filter,rec,skip,None)
                #self.procCalcSize(dn,filter,rec,skip,None)
                #self.iCount,self.iCountDir,self.iCountFile,self.lSize=t
                pass
            if self.__isLogInfo__():
                self.__logInfo__('size;dn:%s'%dn)
            self.lAct=0
            if self.bPost:
                self.Post('proc',self.lAct,self.lSize)
                #wx.PostEvent(self.par,vtInOutReadThreadElements(self.lAct,self.lSize))
            dirEntry=listFiles.DirEntry(dn)
            if (style & self.MASK_DETAIL)==0:
                self.readWalk(dirEntry,dn,filter,rec,skip,bFiles)
            else:
                self.readWalkDetail(dirEntry,dn,filter,rec,skip,style)
            if func is not None:
                if self.Is2Stop():
                    func(sDN,None,*args,**kwargs)
                else:
                    func(sDN,dirEntry,*args,**kwargs)
            if self.bPost:
                if self.Is2Stop():
                    self.Post('aborted')
                else:
                    self.Post('finished')
            #if self.bPost:
            #    wx.PostEvent(self.par,vtInOutReadThreadFinished())
        except:
            #vtLog.vtLngTB(self.GetOrigin())
            self.__logTB__()
            #if self.bPost:
            #    wx.PostEvent(self.par,vtInOutReadThreadAborted())
        if self.__isLogInfo__():
            self.__logInfo__('fin;dn:%s'%dn)
    def procCalcQuick(self,dn,rec):
        try:
            self.ClearCount()
            lSize=[]
            dn=dn.replace(os.sep,'/')
            iSkip=len(dn)
            if dn[-1]!='/':
                iSkip+=1
            for sBaseDN,lDir,lFile in os.walk(dn):
                if sBaseDN.count(os.sep,iSkip)>=rec:
                    for i in xrange(len(lDir)):
                        del lDir[0]
                lSize.append(len(lFile))
            
            self.iCountFile=reduce(lambda x,y:x+y,lSize)
            self.iCountDir=len(lSize)
            self.iCount=self.iCountDir+self.iCountFile
            self.lSize=self.iCount+self.iCountFile
        except:
            self.__logTB__()
    def procCalcCount(self,dn,filter,rec,skip,func,*args,**kwargs):
        if self.__isLogInfo__():
            self.__logInfo__('start;dn:%s'%dn)
        if self.__isLogDebug__():
            bDbg=True
        else:
            bDbg=False
        self.ClearCount()
        iCount=0
        iCountDir=1
        iCountFile=0
        sTmpDN=dn.replace(os.sep,'/')
        iHierBase=sTmpDN.count('/')
        for sDN,lDir,lFile in os.walk(dn):
            #rec-=1
            sDN=sDN.replace(os.sep,'/')
            iHier=sDN.count('/')
            iRec=iHier-iHierBase
            if filter is not None:
                l=[]
                if rec>=iRec:
                    for s in lDir:
                        if filter.apply('/'.join([sDN,s]),skip,isDir=True)==False:
                            if bDbg:
                                self.__logDebug__('dn:%s;filter'%s)
                            l.append(s)
                            continue
                        iCountDir+=1
                    for s in l:
                        lDir.remove(s)
                else:
                    for i in xrange(len(lDir)):
                        del lDir[0]
                l=None
                for s in lFile:
                    if filter.apply('/'.join([sDN,s]),skip)==False:
                        continue
                    #l.append(s)
                    iCountFile+=1
                #lFile=l
            else:
                if rec>=iRec:
                    iCountDir+=len(lDir)
                iCountFile+=len(lFile)
            if rec<iRec:
                print rec
                for i in xrange(len(lDir)):
                    del lDir[0]
                #lDir=[]
        if self.__isLogInfo__():
            self.__logInfo__('fin;dn:%s'%dn)
        if func is not None:
            func(dn,iCountDir+iCountFile,iCountDir,iCountFile,*args,**kwargs)
        self.iCount=iCountDir+iCountFile
        self.iCountDir=iCountDir
        self.iCountFile=iCountFile
        self.lSize=iCountDir+iCountFile
        return self.iCount,self.iCountDir,self.iCountFile
    def procCalcSize(self,dn,filter,rec,skip,func,*args,**kwargs):
        if self.__isLogInfo__():
            self.__logInfo__('start;dn:%s'%dn)
        if self.__isLogDebug__():
            bDbg=True
        else:
            bDbg=False
        self.ClearCount()
        self.bSize=True
        lSize=0
        iCount=0
        iCountDir=1
        iCountFile=0
        sTmpDN=dn.replace(os.sep,'/')
        iHierBase=sTmpDN.count('/')
        for sDN,lDir,lFile in os.walk(dn):
            if bDbg:
                self.__logDebug__('dn:%s;lDir:%s'%(sDN,self.__logFmt__(lDir)))
            #rec-=1
            sDN=sDN.replace(os.sep,'/')
            iHier=sDN.count('/')
            iRec=iHier-iHierBase
            if filter is not None:
                l=[]
                if rec>=iRec:
                    for s in lDir:
                        if filter.apply('/'.join([sDN,s]),skip,isDir=True)==False:
                            if bDbg:
                                self.__logDebug__('dn:%s;filter'%s)
                            l.append(s)
                            continue
                        iCountDir+=1
                    for s in l:
                        lDir.remove(s)
                else:
                    for i in xrange(len(lDir)):
                        del lDir[0]
                l=None
                iCountFile+=len(lFile)
                lSize+=sum(getsize(join(root, name)) for name in lFile if filter.apply('/'.join([sDN,name]),skip)==True)
                #for s in lFile:
                #    sFN='/'.join([sDN,s])
                #    if filter.apply(sFN,skip)==False:
                #        continue
                    #l.append(s)
                #    iCountFile+=1
                #    st=os.stat(sFN)
                #    mode=st[stat.ST_MODE]
                #    lSize+=st[stat.ST_SIZE]
                #lFile=l
            else:
                if rec>=iRec:
                    iCountDir+=len(lDir)
                iCountFile+=len(lFile)
                lSize+=sum(getsize(join(sDN, name)) for name in lFile)
                #for s in lFile:
                #    sFN='/'.join([sDN,s])
                #    st=os.stat(sFN)
                #    mode=st[stat.ST_MODE]
                #    lSize+=st[stat.ST_SIZE]
            if rec<iRec:
                for i in xrange(len(lDir)):
                    del lDir[0]
                #lDir=[]
        if self.__isLogInfo__():
            self.__logInfo__('fin;dn:%s'%dn)
        if func is not None:
            func(dn,iCountDir+iCountFile,iCountDir,iCountFile,lSize,*args,**kwargs)
        self.iCount=iCountDir+iCountFile
        self.iCountDir=iCountDir
        self.iCountFile=iCountFile
        self.lSize=lSize
        return self.iCount,self.iCountDir,self.iCountFile,self.lSize
        #return iCountDir+iCountFile,iCountDir,iCountFile,lSize
    def calcSize(self,dn,filter,rec,skip):
        lSize=0
        for f in os.listdir(dn):
            sFN=os.path.join(dn,f)
            if filter is not None:
                if filter.apply(sFN,skip):
                    continue
            st=os.stat(sFN)
            mode=st[stat.ST_MODE]
            if stat.S_ISDIR(mode):
                if rec>0:
                    lSize+=self.calcSize(sFN,filter,rec-1,skip)
            elif stat.S_ISREG(mode):
                lSize+=st[stat.ST_SIZE]
        return lSize
    def calcFileInfo(self,item,sFN,style):
        if self.__isLogDebug__():
            self.__logDebug__('sFN:%s;style:0x%04x'%(sFN,style))
        try:
            if (style & 1)==1:
                md5info=md5.new()
                inf=open(sFN,"rb")
                buf=" "
                count=0
                while len(buf)>0:
                    if self.Is2Stop():
                        return
                    buf=inf.read(8192)
                    md5info.update(buf)
                    count=count+1
                    self.lAct+=len(buf)
                    if self.bPost:
                        if ((style & self.POST_ALL_SIZES)!=0) | ((count%10)==0):
                            self.Post('proc',self.lAct,self.lSize)
                            #wx.PostEvent(self.par,vtInOutReadThreadElements(self.lAct,self.lSize))
                inf.close()
                item.md5=md5info.hexdigest()
            else:
                item.md5=''
                self.lAct+=item.size
                if self.bPost:
                    self.Post('proc',self.lAct,self.lSize)
                    #wx.PostEvent(self.par,vtInOutReadThreadElements(self.lAct,self.lSize))
        except:
            #vtLog.vtLngTB(self.GetOrigin())
            self.__logTB__()
            item.md5='---???---'
    def read(self,dirEntry,dn,filter,rec,skip,style):
        if self.__isLogInfo__():
            self.__logInfo__('start;dn:%s'%dn)
        iSize=0
        for f in os.listdir(dn):
            if self.Is2Stop():
                return
            #sFN=dn+'/'+f
            sFN='/'.join([dn,f])
            if filter is not None:
                if filter.apply(sFN,skip)==0:
                    continue
            st=os.stat(sFN)
            mode=st[stat.ST_MODE]
            if stat.S_ISDIR(mode):
                if rec>0:
                    item=listFiles.DirEntry(sFN)
                    self.read(item,sFN,filter,rec-1,skip,style)
                    dirEntry.dirs.append(item)
            elif stat.S_ISREG(mode):
                item=listFiles.FileEntry()
                item.set2stat(f,st)
                iSize+=item.size
                dirEntry.files.append(item)
                self.calcFileInfo(item,sFN,style)
                #lAct+=st[stat.ST_SIZE]
            if self.bPost:
                self.Post('proc',self.lAct,self.lSize)
                #wx.PostEvent(self.par,vtInOutReadThreadElements(self.lAct,self.lSize))
        dirEntry.file_size=iSize
        for d in dirEntry.dirs:
            iSize+=d.size
        dirEntry.size=iSize
        if self.__isLogInfo__():
            self.__logInfo__('fin;dn:%s'%dn)
    def readWalk(self,dirEntry,dn,filter,rec,skip,bFiles=True):
        if self.__isLogInfo__():
            self.__logInfo__('start;dn:%s'%dn)
        if self.__isLogDebug__():
            bDbg=True
        else:
            bDbg=False
        dn=dn.replace(os.sep,'/')
        iLenDN=len(dn)
        if dn[-1]=='/':
            iSkipBase=iLenDN
        else:
            iSkipBase=iLenDN+1
        d={dn:dirEntry}
        for sDN,lDir,lFile in os.walk(dn):
            if self.Is2Stop():
                return
            #rec-=1
            sDN=sDN.replace(os.sep,'/')
            #if bDbg==True:
            #    self.__logDebug__('sDN:%s;d:%s'%(sDN,self.__logFmt__(d)))
            iRec=sDN.count('/',iLenDN)
            dE=d[sDN]
            if sDN[-1]=='/':
                sDN=sDN[:-1]
            #self.Post('proc',sDN,None)
            if filter is not None:
                l=[]
                if iRec<rec:
                    for s in lDir:
                        sFN='/'.join([sDN,s])
                        if filter.apply(sFN,skip,isDir=True)==False:
                            l.append(s)
                            continue
                        #l.append(s)
                        item=listFiles.DirEntry(sFN)
                        d[sFN]=item
                        dE.dirs.append(item)
                        item.file_size=0
                for s in l:
                    lDir.remove(s)
                #lDir=l
                l=[]
                for s in lFile:
                    sFN='/'.join([sDN,s])
                    if filter.apply(sFN,skip)==False:
                        continue
                    l.append(s)
                lFile=l
            else:
                if iRec<rec:
                    for s in lDir:
                        sFN='/'.join([sDN,s])
                        item=listFiles.DirEntry(sFN)
                        d[sFN]=item
                        dE.dirs.append(item)
                        item.file_size=0
            #dirEntry=d[sDN]
            iSize=0
            lAct=self.lAct
            if self.bSize==False:
                self.lAct+=1
            #if self.bPost:
                #self.Post('proc',self.lAct,self.lSize)
            #    self.Post('proc',self.lAct,self.iCount)
            for s in lFile:
                if self.Is2Stop():
                    return
                self.lAct+=1
                sFN='/'.join([sDN,s])
                st=os.stat(sFN)
                if bFiles:
                    item=listFiles.FileEntry()
                    item.fullname=sFN[iSkipBase:]
                    item.set2stat(s,st)
                    iSize+=item.size
                    #self.lSize+=item.size
                    item.md5=''
                    dE.files.append(item)
                    #self.lAct+=1+item.size
                    #if self.bPost:
                    #    wx.PostEvent(self.par,vtInOutReadThreadElements(self.lAct,self.lSize+1000))
                else:
                    iSize+=st[stat.ST_SIZE]
                #if self.bSize==False:
                #    self.lAct+=1
                #else:
                #self.lAct+=item.size
                if self.bPost:
                    #self.Post('proc',self.lAct,self.lSize)
                    self.Post('proc',self.lAct,self.iCount)
                #wx.PostEvent(self.par,vtInOutReadThreadElements(self.lAct,self.lSize))
            self.lSize+=iSize
            dE.file_size=iSize
            if iRec>=rec:
                for i in xrange(len(lDir)):
                    del lDir[0]
                #lDir=[]
        keys=d.keys()
        keys.sort()
        keys.reverse()
        for k in keys:
            dE=d[k]
            iSize=0
            for dd in dE.dirs:
                iSize+=dd.size
            try:
                dE.size=iSize+dE.file_size
            except:
                dE.size=iSize
                self.__logError__('%s;%s'%(k,repr(dE)))
        if self.__isLogInfo__():
            self.__logInfo__('fin;dn:%s'%dn)
    def readWalkDetail(self,dirEntry,dn,filter,rec,skip,style):
        if self.__isLogInfo__():
            self.__logInfo__('start;dn:%s'%dn)
        d={dn:dirEntry}
        dn=dn.replace(os.sep,'/')
        iLenDN=len(dn)
        if dn[-1]=='/':
            iSkipBase=iLenDN-1
        else:
            iSkipBase=iLenDN
        for sDN,lDir,lFile in os.walk(dn):
            if self.Is2Stop():
                return
            #rec-=1
            sDN=sDN.replace(os.sep,'/')
            iRec=sDN.count('/',iLenDN)
            dE=d[sDN]
            if sDN[-1]=='/':
                sDN=sDN[:-1]
            #self.Post('proc',sDN,None)
            if filter is not None:
                l=[]
                if iRec<rec:
                    for s in lDir:
                        sFN='/'.join([sDN,s])
                        if filter.apply(sFN,skip,isDir=True)==False:
                            l.append(s)
                            continue
                        #l.append(s)
                        item=listFiles.DirEntry(sFN)
                        d[sFN]=item
                        dE.dirs.append(item)
                        item.file_size=0
                for s in l:
                    lDir.remove(s)
                #lDir=l
                l=[]
                for s in lFile:
                    sFN='/'.join([sDN,s])
                    if filter.apply(sFN,skip)==False:
                        continue
                    l.append(s)
                lFile=l
            else:
                if iRec<rec:
                    for s in lDir:
                        sFN='/'.join([sDN,s])
                        item=listFiles.DirEntry(sFN)
                        d[sFN]=item
                        dE.dirs.append(item)
                        item.file_size=0
            #dirEntry=d[sDN]
            iSize=0
            for s in lFile:
                if self.Is2Stop():
                    return
                sFN='/'.join([sDN,s])
                st=os.stat(sFN)
                item=listFiles.FileEntry()
                item.fullname=sFN[iSkipBase:]
                item.set2stat(s,st)
                iSize+=item.size
                self.lSize+=item.size
                dE.files.append(item)
                self.calcFileInfo(item,sFN,style)
            dE.file_size=iSize
            if iRec>=rec:
                for i in xrange(len(lDir)):
                    del lDir[0]
                #lDir=[]
        keys=d.keys()
        keys.sort()
        keys.reverse()
        for k in keys:
            dE=d[k]
            iSize=0
            for dd in dE.dirs:
                iSize+=dd.size
            try:
                dE.size=iSize+dE.file_size
            except:
                dE.size=iSize
                self.__logError__('%s;%s'%(k,repr(dE)))
        if self.__isLogInfo__():
            self.__logInfo__('fin;dn:%s'%dn)
