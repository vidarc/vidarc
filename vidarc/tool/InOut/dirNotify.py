#----------------------------------------------------------------------------
# Name:         dirNotify.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060127
# CVS-ID:       $Id: dirNotify.py,v 1.3 2009/02/22 17:54:43 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os
#import platform
import sys
import time,thread,threading,traceback
import Queue


#if platform.system()=='Windows':
if sys.platform.startswith('win'):
    SYSTEM=1
    import win32file
    import win32con
    FILE_LIST_DIRECTORY = 0x0001
    
#elif platform.system()=='Linux':
elif sys.platform.startswith('linux'):
    SYSTEM=0
    pass
    
class dirNotifyReceiver:
    def __init__(self):
        pass
    def added(self,sAct,sFN,zTm):
        print 'added',sAct,sFN,time.asctime(time.localtime(zTm))
    def deleted(self,sAct,sFN,zTm):
        print 'deleted',sAct,sFN,time.asctime(time.localtime(zTm))
    def renamed(self,sAct,sFN,zTm):
        print 'renamed',sAct,sFN,time.asctime(time.localtime(zTm))
    def changed(self,sAct,sFN,zTm):
        print 'changed',sAct,sFN,time.asctime(time.localtime(zTm))

class dirNotify:
    def __init__(self,dn,zMinNotifyDelay=1,verbose=0):
        self.zMinNotifyDelay=zMinNotifyDelay
        self.zLastNotify=0#time.time()
        
        self.dn=dn
        self.qAdd=Queue.Queue()
        self.qDel=Queue.Queue()
        self.qRen=Queue.Queue()
        self.qMod=Queue.Queue()
        self.QUEUES={
            1 : self.qAdd,
            2 : self.qDel,
            3 : self.qMod,
            4 : self.qRen,
            5 : self.qRen
            }
        self.verbose=verbose
        self.serving=False
        self.stopping=False
        self.monitor=None
        self.receiver=[]
        if SYSTEM==1:
            try:
                self.hDir = win32file.CreateFile (
                    dn,
                    FILE_LIST_DIRECTORY,
                    win32con.FILE_SHARE_READ | win32con.FILE_SHARE_WRITE,
                    None,
                    win32con.OPEN_EXISTING,
                    win32con.FILE_FLAG_BACKUP_SEMANTICS,
                    None
                    )
            except:
                self.hDir=None
    def Is2Notify(self):
        if time.time()-self.zLastNotify>self.zMinNotifyDelay:
            return True
        else:
            return False
    def AddReceiver(self,receiver):
        self.receiver.append(receiver)
    def DelReceiver(self,receiver):
        try:
            self.receiver.remove(receiver)
        except:
            pass
    def Add2Monitor4Change(self,name):
        if self.monitor is None:
            self.monitor=[]
        self.monitor.append(name)
    def Start(self):
        if self.serving:
            return
        if self.stopping:
            return
        self.serving=True
        self.stopping=False
        if SYSTEM==1:
            thread.start_new_thread(self.RunWin,())
        thread.start_new_thread(self.RunNotify,())
    def Stop(self):
        #vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.stopping=True
    def Pause(self,flag):
        self.pausing=flag
    def IsRunning(self):
        return self.serving
    def RunNotify(self):
        while self.serving:
            lst=[]
            try:
                while 1:
                    sAct,sFN,zTm=self.qAdd.get(False)
                    try:
                        lst.index(sFN)
                    except:
                        for rcv in self.receiver:
                            rcv.added(sAct,sFN,zTm)
                        lst.append(sFN)
            except:
                pass
            lst=[]
            try:
                while 1:
                    sAct,sFN,zTm=self.qDel.get(False)
                    try:
                        lst.index(sFN)
                    except:
                        for rcv in self.receiver:
                            rcv.deleted(sAct,sFN,zTm)
                        lst.append(sFN)
            except:
                pass
            lst=[]
            try:
                
                while 1:
                    sAct,sFN,zTm=self.qMod.get(False)
                    try:
                        lst.index(sFN)
                    except:
                        for rcv in self.receiver:
                            rcv.changed(sAct,sFN,zTm)
                        lst.append(sFN)
            except:
                pass
            lst=[]
            try:
                while 1:
                    sAct,sFN,zTm=self.qRen.get(False)
                    try:
                        lst.index(sFN)
                    except:
                        for rcv in self.receiver:
                            rcv.renamed(sAct,sFN,zTm)
                        lst.append(sFN)
            except:
                pass
            time.sleep(self.zMinNotifyDelay)
    def RunWin(self):
        ACTIONS = {
            1 : "Created",
            2 : "Deleted",
            3 : "Updated",
            4 : "Renamed to something",
            5 : "Renamed from something"
            }
        while self.serving:
            if self.stopping==True:
                self.serving=False
                continue
            try:
                results = win32file.ReadDirectoryChangesW (
                                self.hDir,
                                1024,
                                True,
                                win32con.FILE_NOTIFY_CHANGE_FILE_NAME | 
                                win32con.FILE_NOTIFY_CHANGE_DIR_NAME |
                                win32con.FILE_NOTIFY_CHANGE_ATTRIBUTES |
                                win32con.FILE_NOTIFY_CHANGE_SIZE |
                                win32con.FILE_NOTIFY_CHANGE_LAST_WRITE |
                                win32con.FILE_NOTIFY_CHANGE_SECURITY,
                                None,
                                None
                                )
                zClk=time.clock()
                for action, file in results:
                    if action==3:
                        if self.monitor is not None:
                            try:
                                self.monitor.index(file)
                            except:
                                continue
                    sFN=os.path.join(self.dn, file)
                    sAct=ACTIONS.get (action, "Unknown")
                    #print sFN, sAct,zClk
                    qTmp=self.QUEUES.get(action,None)
                    if qTmp is not None:
                        qTmp.put((sAct,sFN,time.time()))
            except:
                traceback.print_exc()
                self.serving=False
    
def test():
    dn=dirNotify('.')
    drcv=dirNotifyReceiver()
    dn.AddReceiver(drcv)
    dn.Start()
    for i in range(100):
        time.sleep(10)
    pass

if __name__=='__main__':
    test()
