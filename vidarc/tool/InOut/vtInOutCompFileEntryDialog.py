#Boa:Dialog:vtInOutCompFileEntryDialog
#----------------------------------------------------------------------------
# Name:         vtInOutCompFileEntryDialog.py
#               derived from vTools/vCheckDir/vCheckDirResultDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080508
# CVS-ID:       $$
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

from vidarc.tool.InOut.listFiles import getSizeStr,ftime2str

def create(parent):
    return vtInOutCompFileEntryDialog(parent)

[wxID_VTINOUTCOMPFILEENTRYDIALOG, wxID_VTINOUTCOMPFILEENTRYDIALOGLBLACCESS, 
 wxID_VTINOUTCOMPFILEENTRYDIALOGLBLBASEDN, 
 wxID_VTINOUTCOMPFILEENTRYDIALOGLBLCREATE, 
 wxID_VTINOUTCOMPFILEENTRYDIALOGLBLDN, wxID_VTINOUTCOMPFILEENTRYDIALOGLBLFN, 
 wxID_VTINOUTCOMPFILEENTRYDIALOGLBLMD51, 
 wxID_VTINOUTCOMPFILEENTRYDIALOGLBLMOD1, 
 wxID_VTINOUTCOMPFILEENTRYDIALOGLBLSIZE, 
 wxID_VTINOUTCOMPFILEENTRYDIALOGLBLSRC1, 
 wxID_VTINOUTCOMPFILEENTRYDIALOGLBLSRC2, 
 wxID_VTINOUTCOMPFILEENTRYDIALOGTXTACCESS1, 
 wxID_VTINOUTCOMPFILEENTRYDIALOGTXTACCESS2, 
 wxID_VTINOUTCOMPFILEENTRYDIALOGTXTBASEDN1, 
 wxID_VTINOUTCOMPFILEENTRYDIALOGTXTBASEDN2, 
 wxID_VTINOUTCOMPFILEENTRYDIALOGTXTCREATE1, 
 wxID_VTINOUTCOMPFILEENTRYDIALOGTXTCREATE2, 
 wxID_VTINOUTCOMPFILEENTRYDIALOGTXTDN1, wxID_VTINOUTCOMPFILEENTRYDIALOGTXTDN2, 
 wxID_VTINOUTCOMPFILEENTRYDIALOGTXTFN1, wxID_VTINOUTCOMPFILEENTRYDIALOGTXTFN2, 
 wxID_VTINOUTCOMPFILEENTRYDIALOGTXTMD51, 
 wxID_VTINOUTCOMPFILEENTRYDIALOGTXTMD52, 
 wxID_VTINOUTCOMPFILEENTRYDIALOGTXTMOD1, 
 wxID_VTINOUTCOMPFILEENTRYDIALOGTXTMOD2, 
 wxID_VTINOUTCOMPFILEENTRYDIALOGTXTSIZE1, 
 wxID_VTINOUTCOMPFILEENTRYDIALOGTXTSIZE2, 
] = [wx.NewId() for _init_ctrls in range(27)]

class vtInOutCompFileEntryDialog(wx.Dialog):
    def _init_coll_bxsBaseDN_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.txtBaseDN1, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtBaseDN2, 1, border=0, flag=wx.EXPAND)

    def _init_coll_bxsSz_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.txtSize1, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtSize2, 1, border=0, flag=wx.EXPAND)

    def _init_coll_bxsMD5_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.txtMD51, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtMD52, 1, border=0, flag=wx.EXPAND)

    def _init_coll_bxsDN_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.txtDN1, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtDN2, 1, border=0, flag=wx.EXPAND)

    def _init_coll_bxsCreate_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.txtCreate1, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtCreate2, 1, border=0, flag=wx.EXPAND)

    def _init_coll_fgsMain_Items(self, parent):
        # generated method, don't edit

        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsLbl, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblBaseDN, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsBaseDN, 0, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.lblDN, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsDN, 0, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.lblFN, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsFN, 0, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.lblSize, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsSz, 0, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.lblMD51, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsMD5, 0, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.lblCreate, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsCreate, 0, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.lblMod1, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsMod, 0, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.lblAccess, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsAccess, 0, border=4, flag=wx.RIGHT | wx.EXPAND)

    def _init_coll_bxsFN_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.txtFN1, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtFN2, 1, border=0, flag=wx.EXPAND)

    def _init_coll_fgsMain_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(1)
        parent.AddGrowableCol(2)

    def _init_coll_bxsLbl_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblSrc1, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblSrc2, 1, border=0, flag=wx.EXPAND)

    def _init_coll_bxsAccess_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.txtAccess1, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtAccess2, 1, border=0, flag=wx.EXPAND)

    def _init_coll_bxsMod_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.txtMod1, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtMod2, 1, border=0, flag=wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsMain = wx.FlexGridSizer(cols=2, hgap=4, rows=9, vgap=4)

        self.bxsBaseDN = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsDN = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsFN = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsSz = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsMD5 = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsCreate = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsMod = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsAccess = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsLbl = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsMain_Items(self.fgsMain)
        self._init_coll_fgsMain_Growables(self.fgsMain)
        self._init_coll_bxsBaseDN_Items(self.bxsBaseDN)
        self._init_coll_bxsDN_Items(self.bxsDN)
        self._init_coll_bxsFN_Items(self.bxsFN)
        self._init_coll_bxsSz_Items(self.bxsSz)
        self._init_coll_bxsMD5_Items(self.bxsMD5)
        self._init_coll_bxsCreate_Items(self.bxsCreate)
        self._init_coll_bxsMod_Items(self.bxsMod)
        self._init_coll_bxsAccess_Items(self.bxsAccess)
        self._init_coll_bxsLbl_Items(self.bxsLbl)

        self.SetSizer(self.fgsMain)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTINOUTCOMPFILEENTRYDIALOG,
              name=u'vtInOutCompFileEntryDialog', parent=prnt, pos=wx.Point(90,
              103), size=wx.Size(426, 250), style=wx.RESIZE_BORDER,
              title=u'vtInOutCompFileEntryDialog')
        self.SetClientSize(wx.Size(418, 223))

        self.lblSrc2 = wx.StaticText(id=wxID_VTINOUTCOMPFILEENTRYDIALOGLBLSRC2,
              label=u'2', name=u'lblSrc2', parent=self, pos=wx.Point(267, 0),
              size=wx.Size(151, 13), style=wx.ALIGN_CENTRE)

        self.txtFN1 = wx.TextCtrl(id=wxID_VTINOUTCOMPFILEENTRYDIALOGTXTFN1,
              name=u'txtFN1', parent=self, pos=wx.Point(116, 67),
              size=wx.Size(149, 21), style=wx.TE_READONLY, value=u'')

        self.txtFN2 = wx.TextCtrl(id=wxID_VTINOUTCOMPFILEENTRYDIALOGTXTFN2,
              name=u'txtFN2', parent=self, pos=wx.Point(265, 67),
              size=wx.Size(149, 21), style=wx.TE_READONLY, value=u'')

        self.lblSize = wx.StaticText(id=wxID_VTINOUTCOMPFILEENTRYDIALOGLBLSIZE,
              label=_(u'size'), name=u'lblSize', parent=self, pos=wx.Point(0,
              92), size=wx.Size(112, 21), style=wx.ALIGN_RIGHT)

        self.txtSize1 = wx.TextCtrl(id=wxID_VTINOUTCOMPFILEENTRYDIALOGTXTSIZE1,
              name=u'txtSize1', parent=self, pos=wx.Point(116, 92),
              size=wx.Size(149, 21), style=wx.TE_RIGHT | wx.TE_READONLY,
              value=u'')

        self.txtSize2 = wx.TextCtrl(id=wxID_VTINOUTCOMPFILEENTRYDIALOGTXTSIZE2,
              name=u'txtSize2', parent=self, pos=wx.Point(265, 92),
              size=wx.Size(149, 21), style=wx.TE_RIGHT | wx.TE_READONLY,
              value=u'')

        self.lblMD51 = wx.StaticText(id=wxID_VTINOUTCOMPFILEENTRYDIALOGLBLMD51,
              label=u'MD5', name=u'lblMD51', parent=self, pos=wx.Point(0, 117),
              size=wx.Size(112, 21), style=wx.ALIGN_RIGHT)

        self.txtMD51 = wx.TextCtrl(id=wxID_VTINOUTCOMPFILEENTRYDIALOGTXTMD51,
              name=u'txtMD51', parent=self, pos=wx.Point(116, 117),
              size=wx.Size(149, 21), style=wx.TE_READONLY, value=u'')

        self.txtMD52 = wx.TextCtrl(id=wxID_VTINOUTCOMPFILEENTRYDIALOGTXTMD52,
              name=u'txtMD52', parent=self, pos=wx.Point(265, 117),
              size=wx.Size(149, 21), style=wx.TE_READONLY, value=u'')

        self.lblCreate = wx.StaticText(id=wxID_VTINOUTCOMPFILEENTRYDIALOGLBLCREATE,
              label=_(u'create time'), name=u'lblCreate', parent=self,
              pos=wx.Point(0, 142), size=wx.Size(112, 21),
              style=wx.ALIGN_RIGHT)

        self.txtCreate1 = wx.TextCtrl(id=wxID_VTINOUTCOMPFILEENTRYDIALOGTXTCREATE1,
              name=u'txtCreate1', parent=self, pos=wx.Point(116, 142),
              size=wx.Size(149, 21), style=wx.TE_READONLY, value=u'')

        self.txtCreate2 = wx.TextCtrl(id=wxID_VTINOUTCOMPFILEENTRYDIALOGTXTCREATE2,
              name=u'txtCreate2', parent=self, pos=wx.Point(265, 142),
              size=wx.Size(149, 21), style=wx.TE_READONLY, value=u'')

        self.lblMod1 = wx.StaticText(id=wxID_VTINOUTCOMPFILEENTRYDIALOGLBLMOD1,
              label=_(u'modification time'), name=u'lblMod1', parent=self,
              pos=wx.Point(0, 167), size=wx.Size(112, 21),
              style=wx.ALIGN_RIGHT)

        self.txtMod1 = wx.TextCtrl(id=wxID_VTINOUTCOMPFILEENTRYDIALOGTXTMOD1,
              name=u'txtMod1', parent=self, pos=wx.Point(116, 167),
              size=wx.Size(149, 21), style=wx.TE_READONLY, value=u'')

        self.txtMod2 = wx.TextCtrl(id=wxID_VTINOUTCOMPFILEENTRYDIALOGTXTMOD2,
              name=u'txtMod2', parent=self, pos=wx.Point(265, 167),
              size=wx.Size(149, 21), style=wx.TE_READONLY, value=u'')

        self.lblAccess = wx.StaticText(id=wxID_VTINOUTCOMPFILEENTRYDIALOGLBLACCESS,
              label=_(u'access time'), name=u'lblAccess', parent=self,
              pos=wx.Point(0, 192), size=wx.Size(112, 21),
              style=wx.ALIGN_RIGHT)

        self.txtAccess1 = wx.TextCtrl(id=wxID_VTINOUTCOMPFILEENTRYDIALOGTXTACCESS1,
              name=u'txtAccess1', parent=self, pos=wx.Point(116, 192),
              size=wx.Size(149, 21), style=wx.TE_READONLY, value=u'')

        self.txtAccess2 = wx.TextCtrl(id=wxID_VTINOUTCOMPFILEENTRYDIALOGTXTACCESS2,
              name=u'txtAccess2', parent=self, pos=wx.Point(265, 192),
              size=wx.Size(149, 21), style=wx.TE_READONLY, value=u'')

        self.lblSrc1 = wx.StaticText(id=wxID_VTINOUTCOMPFILEENTRYDIALOGLBLSRC1,
              label=u'1', name=u'lblSrc1', parent=self, pos=wx.Point(116, 0),
              size=wx.Size(151, 13), style=wx.ALIGN_CENTRE)

        self.lblFN = wx.StaticText(id=wxID_VTINOUTCOMPFILEENTRYDIALOGLBLFN,
              label=_(u'filename'), name=u'lblFN', parent=self, pos=wx.Point(0,
              67), size=wx.Size(112, 21), style=wx.ALIGN_RIGHT)

        self.lblBaseDN = wx.StaticText(id=wxID_VTINOUTCOMPFILEENTRYDIALOGLBLBASEDN,
              label=_(u'base directory'), name=u'lblBaseDN', parent=self,
              pos=wx.Point(0, 17), size=wx.Size(112, 21), style=wx.ALIGN_RIGHT)

        self.txtBaseDN1 = wx.TextCtrl(id=wxID_VTINOUTCOMPFILEENTRYDIALOGTXTBASEDN1,
              name=u'txtBaseDN1', parent=self, pos=wx.Point(116, 17),
              size=wx.Size(149, 21), style=wx.TE_READONLY, value=u'')

        self.txtBaseDN2 = wx.TextCtrl(id=wxID_VTINOUTCOMPFILEENTRYDIALOGTXTBASEDN2,
              name=u'txtBaseDN2', parent=self, pos=wx.Point(265, 17),
              size=wx.Size(149, 21), style=wx.TE_READONLY, value=u'')

        self.lblDN = wx.StaticText(id=wxID_VTINOUTCOMPFILEENTRYDIALOGLBLDN,
              label=_(u'directory'), name=u'lblDN', parent=self, pos=wx.Point(0,
              42), size=wx.Size(112, 21), style=wx.ALIGN_RIGHT)

        self.txtDN1 = wx.TextCtrl(id=wxID_VTINOUTCOMPFILEENTRYDIALOGTXTDN1,
              name=u'txtDN1', parent=self, pos=wx.Point(116, 42),
              size=wx.Size(149, 21), style=wx.TE_READONLY, value=u'')

        self.txtDN2 = wx.TextCtrl(id=wxID_VTINOUTCOMPFILEENTRYDIALOGTXTDN2,
              name=u'txtDN2', parent=self, pos=wx.Point(265, 42),
              size=wx.Size(149, 21), style=wx.TE_READONLY, value=u'')

        self._init_sizers()

    def __init__(self, parent):
        self._init_ctrls(parent)
        #self.fgsMain.Layout()
        self.Fit()
    def SetSrc1(self,f,baseDN,dn):
        if f is None:
            self.txtBaseDN1.SetValue('')
            self.txtDN1.SetValue('')
            self.txtMD51.SetValue('')
            self.txtCreate1.SetValue('')
            self.txtMod1.SetValue('')
            self.txtAccess1.SetValue('')
            self.txtFN1.SetValue('')
            self.txtSize1.SetValue('')
            return
        self.txtBaseDN1.SetValue(baseDN)
        self.txtBaseDN1.SetInsertionPointEnd()
        self.txtDN1.SetValue(dn)
        self.txtDN1.SetInsertionPointEnd()
        self.txtFN1.SetValue(f.name)
        self.txtSize1.SetValue(getSizeStr(f.size,2))
        self.txtMD51.SetValue(f.md5)
        self.txtCreate1.SetValue(ftime2str(f.stat_access))
        self.txtMod1.SetValue(ftime2str(f.mod_access))
        self.txtAccess1.SetValue(ftime2str(f.last_access))
    def SetSrc2(self,f,baseDN,dn):
        if f is None:
            self.txtBaseDN2.SetValue('')
            self.txtDN2.SetValue('')
            self.txtMD52.SetValue('')
            self.txtCreate2.SetValue('')
            self.txtMod2.SetValue('')
            self.txtAccess2.SetValue('')
            self.txtFN2.SetValue('')
            self.txtSize2.SetValue('')
            return
        self.txtBaseDN2.SetValue(baseDN)
        self.txtBaseDN2.SetInsertionPointEnd()
        self.txtDN2.SetValue(dn)
        self.txtDN2.SetInsertionPointEnd()
        self.txtFN2.SetValue(f.name)
        self.txtSize2.SetValue(getSizeStr(f.size,2))
        self.txtMD52.SetValue(f.md5)
        self.txtCreate2.SetValue(ftime2str(f.stat_access))
        self.txtMod2.SetValue(ftime2str(f.mod_access))
        self.txtAccess2.SetValue(ftime2str(f.last_access))
        
