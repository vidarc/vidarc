# -*- coding: iso-8859-1 -*-
#----------------------------------------------------------------------
# Name:         vtInOutNotify.py
# Purpose:      file tools functions
#
# Author:       Walter Obweger
#
# Created:      20100215
# CVS-ID:       $Id: vtInOutNotify.py,v 1.7 2014/09/27 07:52:03 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.vtThreadCore import vtThreadCore
from vidarc.tool.misc.vtmThread import vtmThreadInterFace
import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

import sys,time,os

if sys.platform.startswith('win'):
    SYSTEM=1
    import win32file
    import win32event
    import win32con
    import pywintypes
    FILE_LIST_DIRECTORY = 0x0001
    import ntsecuritycon
else:
    SYSTEM=0

class vtInOutNotify(vtmThreadInterFace):
    def __init__(self,sDN,bPost=False,verbose=-1,*args,**kwargs):
        if hasattr(self,'thdNotify')==False:
            self.thdNotify=vtThreadCore(bPost=bPost,verbose=max(verbose-1,VERBOSE),
                                origin=u':'.join([self.GetOrigin(),'thdNotify']))
        self.__initCtrl__(*args,**kwargs)
        vtmThreadInterFace.__init__(self,self.thdNotify)
        self.IsBusy=self.thdNotify.IsBusy
        self.ShutDown=self.thdNotify.Stop
        self.hDN=None
        self.sDN=None
        self.bChg=False
        self.SetDN(sDN)
    def __initCtrl__(self,*args,**kwargs):
        self.zWait=kwargs.get('zWait',1000)
        self.zLimit=kwargs.get('zLimit',10000)
        self.tNotify=kwargs.get('notify',None)
    def SetDN(self,sDN):
        try:
            self.__logDebug__('sDN;%r;%r'%(self.sDN,sDN))
            bChg=False
            #if self.hDN is None:
            #    bChg=True
            if cmp(self.sDN,sDN):
                bChg=True
            if bChg:
                if SYSTEM==1:
                    #if self.hDN is not None:
                    #    win32file.CloseHandle(self.hDN)
                    try:
                        self.thdNotify._acquire()
                        self.bChg=True
                        #self.hDN=None
                        if sDN is not None:
                            self.sDN=sDN[:]
                        else:
                            self.sDN=None
                    except:
                        self.__logTB__()
                    self.thdNotify._release()
                    #self.Do(self.procWatchWin)     # 20140628 wro:do not call
        except:
            self.__logTB__()
            self.hDN
    def procWatchWin(self):
        self.__logDebug__('start'%())
        ACTIONS = {
            1 : "Created",
            2 : "Deleted",
            3 : "Updated",
            4 : "Renamed to something",
            5 : "Renamed from something"
            }
        #flags = win32con.FILE_NOTIFY_CHANGE_FILE_NAME
        buf = win32file.AllocateReadBuffer(8192)
        overlapped = pywintypes.OVERLAPPED()
        overlapped.hEvent = win32event.CreateEvent(None, 0, 0, None)
        bSlowDown=False
        while self.Is2Stop()==False:
            try:
                try:
                    self.thdNotify._acquire()
                    if (self.bChg==True) or (bSlowDown==True):
                        if VERBOSE>0:
                            self.__logDebug__('sDN:%r;chg:%d;slow:%d'%(self.sDN,
                                    self.bChg,bSlowDown))
                        if self.bChg==True:
                            zWait=self.zWait
                        if self.hDN is not None:
                            if VERBOSE>0:
                                self.__logDebug__('close handle')
                            win32file.CloseHandle(self.hDN)
                            self.hDN=None
                        bSlowDown=False
                    self.bChg=False
                    if self.hDN is None:
                        #zWait=self.zWait
                        if self.sDN is not None:
                            if os.path.exists(self.sDN):
                                self.hDN = win32file.CreateFile (
                                    self.sDN,
                                    ntsecuritycon.FILE_LIST_DIRECTORY,
                                    win32con.FILE_SHARE_READ | win32con.FILE_SHARE_WRITE,
                                    None,
                                    win32con.OPEN_EXISTING,
                                    win32con.FILE_FLAG_BACKUP_SEMANTICS |
                                    win32con.FILE_FLAG_OVERLAPPED,
                                    None
                                    )
                except:
                    self.__logTB__()
                self.thdNotify._release()
                if self.hDN is not None:
                    win32file.ReadDirectoryChangesW (self.hDN,buf,False,
                                win32con.FILE_NOTIFY_CHANGE_FILE_NAME | 
                                win32con.FILE_NOTIFY_CHANGE_DIR_NAME |
                                win32con.FILE_NOTIFY_CHANGE_ATTRIBUTES |
                                win32con.FILE_NOTIFY_CHANGE_SIZE |
                                win32con.FILE_NOTIFY_CHANGE_LAST_WRITE |
                                win32con.FILE_NOTIFY_CHANGE_SECURITY,
                                overlapped
                                )
                if VERBOSE>0:
                    self.__logDebug__('sDN:%r;zWait:%d'%(self.sDN,zWait))
                iRet = win32event.WaitForSingleObject(overlapped.hEvent, zWait)
                if iRet == win32event.WAIT_OBJECT_0:
                    iBytes = win32file.GetOverlappedResult(self.hDN, overlapped, True)
                    if iBytes:
                        results = win32file.FILE_NOTIFY_INFORMATION(buf, iBytes)
                        #changes.extend(bits)
                    else:
                        # This is "normal" exit - our 'tearDown' closes the
                        # handle.
                        # print "looks like dir handle was closed!"
                        #return
                        continue
                else:
                    continue
                zClk=time.clock()
                for iAction, sFN in results:
                    #if action==3:
                    #    if self.monitor is not None:
                    #        try:
                    #            self.monitor.index(file)
                    #        except:
                    #            continue
                    #sFN=os.path.join(self.sDN, file)
                    sAct=ACTIONS.get(iAction, "Unknown")
                    try:
                        if self.tNotify is not None:
                            func,args,kwargs=self.tNotify
                            func(iAction,sAct,sFN,zClk,*args,**kwargs)
                    except:
                        self.__logTB__()
                    #qTmp=self.QUEUES.get(action,None)
                    #if qTmp is not None:
                    #    qTmp.put((sAct,sFN,time.time()))
            except:
                self.__logTB__()
                zWait+=self.zWait
                self.__logDebug__('sDN:%r;slowdown to zWait:%d'%(self.sDN,zWait))
                bSlowDown=True
                
                try:
                    self.thdNotify._acquire()
                    if zWait>=self.zLimit:
                        self.__logError__('sDN:%r;disconnect to limit'%(self.sDN))
                        self.bChg=True
                        self.sDN=None
                except:
                    self.__logTB__()
                self.thdNotify._release()
                #traceback.print_exc()
                #self.serving=False
        if self.hDN is not None:
            win32file.CloseHandle(self.hDN)
            self.hDN=None
        self.__logDebug__('done'%())

class vtInOutNotifyLg(vtLog.vtLogMixIn,vtLog.vtLogOrigin,vtInOutNotify):
    def __init__(self,*args,**kwargs):
        vtLog.vtLogOrigin.__init__(self,**{'origin':kwargs.get('origin',None)})
        vtInOutNotify.__init__(self,*args,**kwargs)
