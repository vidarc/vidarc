#----------------------------------------------------------------------------
# Name:         listFiles.py
# Purpose:      list files
# Author:       Walter Obweger
#
# Created:      20060407
# CVS-ID:       $Id: listFiles.py,v 1.7 2008/05/10 09:29:38 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os,time,sys,md5,string
from stat import *
import glob
import vidarc.tool.InOut.fnFilters as filters
import vidarc.tool.log.vtLog as vtLog

def getChar4Level(c,level):
    """return a level depended number of charaters c
    parameter c ... charater
    parameter level ... count of level"""
    s=''
    for i in range(0,level):
        s=s+c
    return s

def listFiles(dir,level=0):
    files=os.listdir(dir)
    files.sort()
    for f in files:
        fn=dir+'/'+f
        m=os.stat(fn)[ST_MODE]
        if S_ISDIR(m):
            listFiles(fn,level+1)
        elif S_ISREG(m):
            print fn
    print files

def ftime2str(ft):
    "convert file time to predefined string"
    s=''
    t=time.gmtime(ft)
    s=time.strftime("%Y-%m-%d %H:%M:%S",t)
    return s

def getSizeStr(size,style=0):
    if style==0:
        return `size`
    elif style==1:
        s=0
        while (size>=1000) and (s<4):
            size=size/1024.0
            s=s+1
        sizeLst=['B','kB','MB','GB','TB']
        str="%6.3f%s"%(size,sizeLst[s])
        return str
    elif style==2:
        s=0
        while (size>=1000) and (s<4):
            size=size/1024.0
            s=s+1
        sizeLst=['  B',' kB',' MB',' GB',' TB']
        str="%6.3f%s"%(size,sizeLst[s])
        return str

class DirEntryCount:
    def __init__(self):
        self.dir=0
        self.file=0
    def add_dir(self):
        self.dir=self.dir+1
    def add_file(self):
        self.file=self.file+1
    def get_dir(self):
        return self.dir
    def get_file(self):
        return self.file
    def get(self):
        return self.dir+self.file
class DirEntrySize:
    def __init__(self):
        self.processed=0
        self.complete=0
    def reset(self,i):
        self.processed=0
        self.complete=i
    def add(self,i):
        self.processed=self.processed+i
    def get_processed_short(self):
        return self.processed / 1048576
    def get_processed(self):
        return self.processed
    def get_processed_human(self):
        return self.convert2human(self.processed)
    def get_complete_short(self):
        return self.complete / 1048576
    def get_complete(self):
        return self.complete
    def get_complete_human(self):
        return self.convert2human(self.complete)
    def convert2human(self,conv):
        ikB=conv / 1024
        #if ikB == 0:
        #    return "%d B"%(conv)
        #
        if ikB < 1000:
            return "%07.3f kB"%((conv+0.0)/1024.0)
        iMB = ikB / 1024
        if iMB < 1000:
            return "%07.3f MB"%((conv+0.0)/1048576.0)
        iGB = iMB / 1024
        if iGB < 1000:
            return "%07.3f GB"%((conv+0.0)/1073741824.0)
        #iTB = iGB / 1024
        return "%07.3 TB"%((conv+0.0)/1099511627776.0)
        #iMB = ikB / 1024
        #if iMB == 0:
        #    return "%07.3f kB"%(conv/1024.0)
        #iGB = iMB / 1024
        #if iGB == 0:
        #    return "%07.3f MB"%(conv/1048576.0)
        #iTB = iGB / 1024
        #if iTB == 0:
        #    return "%07.3f GB"%(conv/1073741824.0)
        #return "%07.3 TB"%(conv/1099511627776.0)

class FileEntry:
    def __init__(self):
        self.name=None
        self.size=0
        self.md5=""
        self.last_access=0
        self.mod_access=0
        self.stat_access=0
        self.md5=None
    def stat(self,fn,dir):
        m=os.stat(dir+'/'+fn)
        if S_ISREG(m[ST_MODE]):
            self.set2stat(fn,m)
    def set2stat(self,fn,m):
        self.name=fn
        self.size=m[ST_SIZE]
        self.last_access=m[ST_ATIME]
        self.mod_access=m[ST_MTIME]
        self.stat_access=m[ST_CTIME]
        self.md5=None
    def __str__(self):
        try:
            return self.name+','+`self.size`
        except:
            return 'None'
    def __repr__(self):
        try:
            return self.name
        except:
            return 'None'
    def calcInfo(self,dir):
        try:
            md5info=md5.new()
            fn=dir+'/'+self.name
            inf=open(fn,"rb")
            buf=" "
            count=0
            while len(buf)>0:
                buf=inf.read(8192)
                md5info.update(buf)
                count=count+1
            inf.close()
            self.md5=md5info.hexdigest()
        except:
            self.md5='---???---'
    def getInfo4cmp2str(self,dir,skip_filename_characters=0,style=1):
        if self.name is None:
            if (style & 1)!=0:
                return "name,type,size,access_time,mod_time,create_time,md5"
            else:
                return "name,type,size,access_time,mod_time,create_time"
        else:
            add_info='%d,%s,%s,%s,'%(self.size,
                                 ftime2str(self.last_access),
                                 ftime2str(self.mod_access),
                                 ftime2str(self.stat_access))
        if len(dir)>0:
            tfn=dir+'/'+self.name
        else:
            tfn=self.name
        # replace , by the string SEPcommaREPL
        tfn=string.replace(tfn[skip_filename_characters:],',','SEPcommaREPL')
        s=tfn+','+'file,'+add_info
        if (style & 1)==1:
            if self.md5 is None:
                self.calcInfo(dir)
            s=s+self.md5
        return s
    def getFullFN(self,dir,skip=0):
        if len(dir)>0:
            tfn=dir+'/'+self.name
        else:
            tfn=self.name
        return tfn

class DirEntry:
    def __init__(self,base=None):
        self.base=base
        self.dirs=[]
        self.files=[]
        self.size=0
    def clear(self):
        self.dirs=[]
        self.files=[]
        self.size=0
    def refresh(self,fn):
        if fn.startswith(self.base):
            iSkip=len(self.base)+1
            sRel=fn[iSkip:]
            l=sRel.split('/')
            iDir=len(l)-1
            lDE=[None]*(iDir)
            
            dE=self
            #for s in l[:-1]:
            for i in xrange(iDir):
                s=l[i]
                d=None
                for dTmp in dE.dirs:
                    if sRel.startswith(dTmp.base[iSkip:]):
                        d=dTmp
                        break
                if d is None:
                    d=DirEntry()
                    d.base='/'.join([dE,s])
                    dE.dirs.append(d)
                lDE[i]=d
                dE=d
            s=l[-1]
            item=None
            for f in dE.files:
                if f.name==s:
                    item=f
                    break
            if item is None:
                item=FileEntry()
                dE.files.append(item)
            
            fn=dE.base+'/'+s
            m=os.stat(fn)
            item.set2stat(s,m)
            
            return item,l,lDE
        return None,None,None
    def read(self,dir,recursion=10,count=None,filter=None,skip=0):
        vtLog.vtLngCurCls(vtLog.DEBUG,'dn:%s'%(dir),self)
        self.base=dir
        files=os.listdir(dir)
        try:
            if len(files)>0:
                files.sort()
        except:
            pass
        for f in files:
            inFilter=0
            fn=dir+'/'+f
            if filter is not None:
                if filter.apply(fn,skip,verbose=0)==0:
                    continue
            try:
                #print fn
                m=os.stat(fn)
                if S_ISDIR(m[ST_MODE]):
                    if count is not None:
                        count.add_dir()
                    if recursion>0:
                        item=DirEntry()
                        item.read(fn,recursion-1,count,filter=filter,skip=skip)
                        self.dirs.append(item)
                        #vtLog.vtLngCurCls(vtLog.DEBUG,'dns:%s'%(vtLog.pformat(self.dirs)),self)
                elif S_ISREG(m[ST_MODE]):
                    if count is not None:
                        count.add_file()
                    item=FileEntry()
                    #print f,m
                    item.set2stat(f,m)
                    self.files.append(item)
            except:
                pass
        #print 'calc size',self.base
        self.size=self.calc_size()
        #print self.size
    def calc_size(self):
        self.size=0
        file_size=0
        dirs_size=0

        def fsize(x):return x.size
        def add(x,y):return x+y
        fs=map(fsize,self.files)
        try:
            file_size=reduce(add,fs)
        except:
            file_size=0
        ds=map(fsize,self.dirs)
        try:
            dirs_size=reduce(add,ds)
        except:
            dirs_size=0
        self.size=file_size+dirs_size
        return file_size+dirs_size
    def calcInfo(self,objCount=None,objSize=None):
        if objCount is not None:
            pass
        #if objSize is not None:
        #    objSize.reset(self.size)
        for f in self.files:
            f.calcInfo(self.base)
            if objCount is not None:
                objCount.add_file()
            if objSize is not None:
                objSize.add(f.size)
        for f in self.dirs:
            f.calcInfo(objCount,objSize)
            if objCount is not None:
                objCount.add_dir()
    def write(self,fn,style=1):
        outf=open(fn,'w')
        outf.write('base,'+self.base+'\n')
        t=FileEntry()
        outf.write(t.getInfo4cmp2str(self.base,style=style)+'\n')
        self.__write__(outf,style=style)
        outf.close()
    def __write__(self,outf,style=1):
        for f in self.files:
            s=f.getInfo4cmp2str(self.base,style=style)
            outf.write(s+'\n')
        for f in self.dirs:
            if (style & 2) == 2:
                s=f.base+'/,'+'dir,'
                outf.write(s+'\n')
            f.__write__(outf,style=style)

    def show(self,detail=0):
        print self.base
        print '  size',getSizeStr(self.size,1)
        if detail>0:
            for f in self.files:
                print ' ',f
            for i in self.dirs:
                i.show(detail)

def writeListFiles(of,dir,func,level=0,skip_filename_characters=0,filters=None):
    files=os.listdir(dir)
    files.sort()
    for f in files:
        fn=dir+'/'+f
        filter_res=applyFilters(dir,f,fn,skip_filename_characters,filters)
        fileinfo=os.stat(fn)
        m=fileinfo[ST_MODE]
        if filter_res>0:
            s=func(dir,f,fileinfo,level,skip_filename_characters=skip_filename_characters)
        
        if S_ISDIR(m):
            if filter_res>0:
                of.write(s+'\n')
            writeListFiles(of,fn,func,level+1,skip_filename_characters=skip_filename_characters,filters=filters)
        elif S_ISREG(m):
            if filter_res>0:
                of.write(s+'\n')

def writeListFiles2File(ofn,dir,func,skip_filename_characters=0,filters=None):
    of=open(ofn,'w')
    of.write(func(dir,None,None)+'\n')
    writeListFiles(of,dir,func,skip_filename_characters=skip_filename_characters,filters=filters)
    of.close()
#def walktree(dir, callback):
#    '''recursively descend the directory rooted at dir,
#        calling the callback function for each regular file'''
#    for f in os.listdir(dir):
#        pathname = '%s/%s' % (dir, f)
#        mode = os.stat(pathname)[ST_MODE]
#        if S_ISDIR(mode):
#            # It's a directory, recurse into it
#            walktree(pathname, callback)
#        elif S_ISREG(mode):
#            # It's a file, call the callback function
#            callback(pathname)
#        else:
#            # Unknown file type, print a message
#            pass
#        print 'Skipping %s' % pathname
#
#def visitfile(file):
#    print 'visiting', file

def compareFileInfos(a,b,hdr):
    if a[0]==b[0]:
        i=len(hdr)
        for h in hdr[1:]:
            if a[h[1]]!=b[h[2]]:
                return 3
        return 0
    else:
        if a[0]<b[0]:
            return 1
        else:
            return 2

def mergeHeaders(hdr1,hdr2):
    hdr=[]
    for h in hdr2:
        try:
            i=hdr1.index(h)
            j=hdr2.index(h)
            hdr.append((h,i,j))
        except:
            pass
    return hdr

def validHeaders(hdr_lst,valid_hdr):
    hdr=[]
    for h in valid_hdr:
        try:
            tt=None
            for t in hdr_lst:
                if t[0]==h:
                    tt=t
                    break
            if tt is not None:
                hdr.append(t)
        except:
            pass
    return hdr

def compareFiles(fn1,fn2,check_hdr=None):
    f1=open(fn1,"r")
    lines1=f1.readlines()
    f1.close()
    f2=open(fn2,"r")
    lines2=f2.readlines()
    f2.close()

    mhdr=mergeHeaders(string.split(lines1[0][:-1],','),
                        string.split(lines2[0][:-1],','))
    if check_hdr is not None:
        mhdr=validHeaders(mhdr,string.split(check_hdr,','))
    ident_lines=[]
    diff_lines=[]

    count1=len(lines1)
    count2=len(lines2)
    i=j=1
    stop=0
    while stop==0:
        obj1=string.split(lines1[i][:-1],',')
        obj2=string.split(lines2[j][:-1],',')
        c=compareFileInfos(obj1,obj2,mhdr)
        if c==0:
            ident_lines.append(lines1[i])
            i=i+1
            j=j+1
        elif c==3:
            diff_lines.append("diff,"+lines1[i])
            i=i+1
            j=j+1
        elif c==1:
            diff_lines.append("lst1,"+lines1[i])
            i=i+1
        elif c==2:
            diff_lines.append("lst2,"+lines2[j])
            j=j+1
        else:
            print 'undef'
        if i>=count1:
            diff_lines=diff_lines+lines2[j:]
            stop=1
        if j>=count2:
            diff_lines=diff_lines+lines1[i:]
            stop=1
    #print '\n\nidentical\n'
    #print ident_lines
    #print '\n\ndiff\n'
    #print diff_lines
    return diff_lines

def logCompareFiles(fn,diff):
    try:
        f=open(fn,"r")
        l=f.readlines()
        f.close()
    except:
        l=[]
    s=time.strftime("%Y%m%d_%H%M%S:",time.localtime(time.time()))
    f=open(fn,"w")
    f.write(s+'\n')
    f.writelines(diff)
    f.write('\n')
    f.writelines(l)
    f.close()

