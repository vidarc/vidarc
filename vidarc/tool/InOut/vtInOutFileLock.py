#----------------------------------------------------------------------------
# Name:         vtInOutFileLock.py
# Purpose:
# Author:       Walter Obweger
#
# Created:      20060923
# CVS-ID:       $Id: vtInOutFileLock.py,v 1.2 2008/05/04 20:58:44 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os,sys,stat,time
if sys.platform=='win32':
    import msvcrt

class vtInOutFileLock:
    def __init__(self,fn):
        self.sFN=fn
        self.fd=None
    def open(self,mode=None,timeout=60):
        if mode is None:
            mode=os.O_RDWR
        self.fd=os.open(self.sFN,mode)
        return self.__lockFd(timeout=timeout)
    def __lockFd(self,timeout=60,bCloseOnLockFlt=True):
        #self.iSize=os.fstat(self.fd)[stat.ST_SIZE]
        self.iSize=sys.maxint
        zStart=time.clock()
        zEnd=zStart+timeout
        while time.clock()<zEnd:
            iRet=self.__lock()
            if iRet>0:
                return self.fd
            elif iRet<=0:
                if bCloseOnLockFlt:
                    os.close(self.fd)
                    self.fd=None
                return None
            time.sleep(1)
        return None
    def lock(self,f,timeout=60):
        self.fd=f.fileno()
        return self.__lockFd(timeout=timeout,bCloseOnLockFlt=False)
    def unlock(self):
        return self.__unlock()
    def __lock(self):
        if sys.platform=='win32':
            try:
                msvcrt.locking(self.fd,msvcrt.LK_NBLCK ,self.iSize)
                return 1
            except:
                return 0
        return -1
    def __unlock(self):
        if sys.platform=='win32':
            #os.lseek(self.fd,0,0)
            #print self.iSize
            #self.iSize=os.fstat(self.fd)[stat.ST_SIZE]
            #print self.iSize
            try:
                msvcrt.locking(self.fd,msvcrt.LK_UNLCK,self.iSize)
            except:
                #print 'fault'
                pass
    def close(self):
        self.__unlock()
        os.close(self.fd)
