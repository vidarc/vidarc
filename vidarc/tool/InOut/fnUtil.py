#----------------------------------------------------------------------------
# Name:         fnUtil.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: fnUtil.py,v 1.7 2008/08/14 22:50:26 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os,os.path,string,time,fnmatch,traceback,sys

def getShiftFN(fn,i):
    hfn="%s.%d"%(fn,i)
    return hfn

def logTB():
    try:
        import vidarc.tool.log.vtLog as vtLog
        if vtLog.vtLngGetFN() is None:
            traceback.print_stack(file=sys.stderr)
            traceback.print_exc(file=sys.stderr)
        else:
            vtLog.vtLngTB(__file__)
    except:
        traceback.print_stack(file=sys.stderr)
        traceback.print_exc(file=sys.stderr)

def shiftFileTimed(fn,max=2,interval='H',backupCount=10):
    try:
        dn,sTmp=os.path.split(fn)
        files=os.listdir(dn)
        l=[]
        sFlt=''.join(['_'.join([sTmp[:-4],'????????','??']),sTmp[-4:]])
        for sTmp in files:
            if fnmatch.fnmatch(sTmp,sFlt):
                l.append(sTmp)
        l.sort()
        for sTmp in l[:-backupCount]:
            try:
                os.remove(os.path.join(dn,sTmp))
            except:
                logTB()
    except:
        logTB()
    zNow=time.localtime(time.time())
    if interval=='H':
        iHrS=(zNow[3]/max)*max
        sFN=''.join(['_'.join([fn[:-4],'%04d%02d%02d'%zNow[:3],'%02d'%(iHrS)]),fn[-4:]])
        zStart=time.mktime(list(zNow[:3])+[iHrS,0,0]+list(zNow[-3:]))
        return sFN,zStart
    else:
        raise ImplementError

def shiftFile(fn,max=2,fd=None):
    hfn="%s.%d"%(fn,max)
    if os.path.exists(hfn):
        try:
            os.remove(hfn)
        except:
            logTB()
    ohfn=hfn
    for i in xrange(max-1,-1,-1):
        hfn="%s.%d"%(fn,i)
        if os.path.exists(hfn):
            try:
                os.rename(hfn,ohfn)
            except:
                logTB()
                pass
                #import traceback
                #print hfn,ohfn
                #traceback.print_stack()
                #traceback.print_exc()
        ohfn=hfn
    try:
        os.rename(fn,hfn)
    except:
        if fd is not None:
            os.lseek(fd,0,0)
            fd1=os.open(hfn,os.O_CREAT|os.O_TRUNC|os.O_WRONLY)
            while 1:
                buf=os.read(fd,1024)
                os.write(fd1,buf)
                if len(buf)!=1024:
                    break
            os.close(fd1)
            os.lseek(fd,0,0)
            return 0
        else:
            #print fn,hfn
            #import traceback
            #traceback.print_exc()
            return -1
    return 0

def delShiftFile(fn,max=2):
    for i in range(0,max+1):
        hfn="%s.%d"%(fn,i)
        try:
            os.remove(hfn)
        except:
            logTB()

def genTstFile(fn,num):
    s="%d"%num
    f=open(fn,"w")
    f.write(s)
    f.close()

def tstShift(fn,max):
    delShiftFile(fn,max)
    for i in range(0,max):
        shiftFile(fn,max)
        genTstFile(fn,i)

ranges=[]
t=(ord(string.ascii_lowercase[0]),ord(string.ascii_lowercase[-1]))
ranges.append(t)
t=(ord(string.ascii_uppercase[0]),ord(string.ascii_uppercase[-1]))
ranges.append(t)
t=(ord(string.digits[0]),ord(string.digits[-1]))
ranges.append(t)
t=(ord('_'),ord('_'))
ranges.append(t)
t=(ord('.'),ord('.'))
ranges.append(t)

def replaceSuspectChars(fn,replaceChar='_'):
    """replace all characters except 'A'..'Z','a'..'b','0'..'9','.' and '_' by '_'
    """
    def chk(a):
        i=ord(a)
        found = 0
        for t in ranges:
            if (i >= t[0]) and (i <= t[1]):
                found=1
        return found
    result=[]
    for s in fn:
        if chk(s)==0:
            result.append(replaceChar)
        else:
            result.append(s)
    return string.join(result,'')

def getAbsoluteFilenameRel2BaseDir(fn,baseDN):
    if baseDN is not None:
        if len(fn)>0:
            if fn[:2]=='./':
                return baseDN+fn[1:]
            elif fn[:2]=='.'+os.path.sep:
                return baseDN+fn[1:]
        else:
            return None
    else:
        return fn

def getFilenameRel2BaseDir(fn,baseDN):
    sFN=fn.replace('\\','/')
    if baseDN is not None:
        sBaseDN=baseDN.replace('\\','/')
        iLen=len(sBaseDN)
        if sFN.startswith(sBaseDN) and sFN[iLen]=='/':
            return '.'+sFN[iLen:]
        else:
            return sFN
    else:
        return sFN
                
if __name__=='__main__':
    tstShift('tstShiftFN.txt',2)