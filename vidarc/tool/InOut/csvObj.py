#----------------------------------------------------------------------------
# Name:         csvObj.py
# Purpose:      CSV object
# Author:       Walter Obweger
#
# Created:      20060407
# CVS-ID:       $Id: csvObj.py,v 1.1 2006/04/11 17:46:51 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os,string
import time
from types import *
import copy
import string
import xmlBase

CSV_OBJ_CMP_IDENT=0
CSV_OBJ_CMP_DIFF=1
CSV_OBJ_SRC1_VALID=2
CSV_OBJ_SRC2_VALID=3
CSV_OBJ_SRC1_PRESENT=4
CSV_OBJ_SRC2_PRESENT=5

def strCmp(s1,s2):
	if s1 is None:
		return 1
	if s2 is None:
		return -1
	cLst=map(None,s1,s2)
	for c in cLst:
		if c[0] is None:
			return -1
		elif c[1] is None:
			return 1
		else:
			r=ord(c[0])-ord(c[1])
			if r!=0:
				return r
	return 0

class vtIOcsvObj:
	def __init__(self):
		pass
		#self.keys=[]
		#self.links=[]
	def compare(self,comp,lst1,lst2):
		def tmp(s):
			return obj.__dict__[s]
		obj=self
		v1=map(tmp,lst1)
		obj=comp
		v2=map(tmp,lst2)
		v3=map(None,v1,v2)
		for i in v3:
			if i[0] is None:
				return -1
			elif i[1] is None:
				return 1
			else:
				#r=cmp(i[0],i[1])
				r=strCmp(i[0],i[1])
				if r!=0:
					return r
		return 0
	def parse(self,items,hdr):
		def tmp(s1,s2):
			return (s1,s2)
		lst=map(tmp,hdr,items)
		for h in lst:
			self.__dict__[h[0]]=h[1]
	def show(self,hdr=None):
		if hdr is None:
			lst=map(None,self.__dict__.keys(),self.__dict__.values())
		else:
			def tmp(v):
				return self.__dict__[v]
			vals=map(tmp,hdr)
			lst=map(None,hdr,vals)
		for i in lst:
			print '   ',i[0],'=',i[1]

	def write(self,of,hdr=None,sep=',',write_hdr=0):
		if hdr is None:
			lst=map(None,self.__dict__.keys(),self.__dict__.values())
		else:
			def tmp(v):
				try:
					return self.__dict__[v]
				except:
					return ''
			vals=map(tmp,hdr)
			lst=map(None,hdr,vals)
		s=''
		if write_hdr>0:
			for i in lst:
				s=s+i[1]+sep
			of.write(s)
		else:
			for i in lst:
				s=s+str(i[1])+sep
			of.write(s)
	def writeXml(self,of,hdr=None,strLevel=''):
		if hdr is None:
			lst=map(None,self.__dict__.keys(),self.__dict__.values())
		else:
			def tmp(v):
				try:
					return self.__dict__[v]
				except:
					return ''
			vals=map(tmp,hdr)
			lst=map(None,hdr,vals)
		s=''
		for i in lst:
			try:
				strH=xmlBase.genXmlTagName(i[0])
				of.write(strLevel+'<'+strH+'>'+str(i[1])+'</'+strH+'>\n')
			except:
				pass
class vtIOcsvConfig:
	def __init__(self):
		objs={}
	def read(fn):
		f=open(fn,'r')
		lines=f.readlines()
		f.close()
		for l in lines:
			strs=string.split(l,',')
class vtIOcsvObjs:
	def __init__(self,sep=','):
		self.sep=sep
		self.reset()
	def reset(self):
		self.hdr=None
		self.objs=[]
	def clear(self):
		self.objs=[]
	def __validate_hdr__(self,verbose=0):
		""" this internal method enforces unique header istems.
		"""
		if verbose>0:
			print '__validate_hdr__'
		new_hdr=[]
		for h in self.hdr:
			k=h
			i=0
			if len(h)==0:
				h='EmptyHdr'
			while (i>=0):
				if i==0:
					k=h
				else:
					k='%s_%d'%(h,i)
				try:
					if verbose>0:
						print ' test key',k
					idx=new_hdr.index(k)
					i=i+1
				except:
					new_hdr.append(k)
					i=-1  # exit loop
		if verbose>0:
			print '__validate_hdr__'
			print '   old',self.hdr
			print '   new',new_hdr
		self.hdr=new_hdr
	def setHdr(self,hdr):
		t=type(hdr)
		if t==StringType:
			self.hdr=string.split(hdr,self.sep)
		elif t==ListType:
			self.hdr=hdr
		else:
			raise "wrong type",hdr
		self.__validate_hdr__()
	def addLineObj(self,line):
		strs=string.split(line,self.sep)
		o=vtIOcsvObj()
		o.parse(strs,self.hdr)
		self.objs.append(o)
	def delLineObjIdx(self,idx):
		self.objs=self.objs[:idx]+self.objs[idx+1:]
	def evalObjs(self,result,eval_str,dft=''):
		for o in self.objs:
			try:
				o.__dict__[result]=eval(eval_str)
			except:   # don't forget to catch exception
				o.__dict__[result]=dft
	def getKey(self,keys):
		k=''
		for i in keys:
			k=k+o.__dict__[i]
		return k
	def getKeyObjs(self,keys,log=None):
		d={}
		for o in self.objs:
			k=''
			try:
				for i in keys:
					k=k+o.__dict__[i]
			except:
				continue  #FIXME: do log
			d[k]=o
		return d
	def getKeyObjsLst(self,keys,log=None):
		"""generate a dictionary objs.
		the parameter key is a list of object properties that form the dictonary
		key.
		the value of the dictonary item is a list of at least one item.
		"""
		d={}
		for o in self.objs:
			k=''
			try:
				for i in keys:
					k=k+o.__dict__[i]
			except:
				k=''   # FIXME: do log
			try:
				l=d[k]
				l.append(o)
			except:
				d[k]=[o]
		return d
	def compare(self,csvInfo1,csvInfo2):
		keyObjs1=csvInfo1[0].getKeyObjs(csvInfo1[1])
		keyObjs2=csvInfo2[0].getKeyObjs(csvInfo2[1])
		keys1=keyObjs1.keys()
		keys1.sort()
		keys2=keyObjs2.keys()
		keys2.sort()
		bFin=0
		idx1=idx2=0
		cnt1=len(keys1)
		cnt2=len(keys2)
		self.objs=[]
		while bFin==0:
			try:
				k1=keys1[idx1]
			except:
				k1=None
			try:
				k2=keys2[idx2]
			except:
				k2=None
			#iKeyRes=cmp(k1,k2)
			iKeyRes=strCmp(k1,k2)
			if iKeyRes==0:
				o1=keyObjs1[k1]
				o2=keyObjs2[k2]
				iRes=o1.compare(o2,csvInfo1[2],csvInfo2[2])
				o=vtIOcsvObj()
				o.type='cmp'
				o.key=k1
				o.obj1=o1
				o.obj2=o2
				if iRes==0:
					o.cmp=CSV_OBJ_CMP_IDENT
				else:
					o.cmp=CSV_OBJ_CMP_DIFF
				idx1=idx1+1
				idx2=idx2+1
			elif iKeyRes<0:
				o=vtIOcsvObj()
				o.type='cmp'
				o.key=k1
				o.obj1=keyObjs1[k1]
				o.obj2=None
				o.cmp=CSV_OBJ_SRC1_PRESENT
				idx1=idx1+1
			elif iKeyRes>0:
				o=vtIOcsvObj()
				o.key=k2
				o.type='cmp'
				o.obj1=None
				o.obj2=keyObjs2[k2]
				o2=keyObjs2[k2]
				o.cmp=CSV_OBJ_SRC2_PRESENT
				idx2=idx2+1
			if idx1>=cnt1 and idx2>=cnt2:
				bFin=1
			self.objs.append(o)
		self.calc_name()
	def getCmpStr(self,o):
		if o.cmp==CSV_OBJ_CMP_IDENT:
			s=' = '
		elif o.cmp==CSV_OBJ_CMP_DIFF:
			s='!= '
		elif o.cmp==CSV_OBJ_SRC1_PRESENT:
			s='1! '
		elif o.cmp==CSV_OBJ_SRC2_PRESENT:
			s='2! '
		elif o.cmp==CSV_OBJ_SRC1_VALID:
			s='1  '
		elif o.cmp==CSV_OBJ_SRC2_VALID:
			s='2  '
		return s
	def calc_name(self):
		for o in self.objs:
			
			o.name=o.key
			
	def merge(self,csvInfo1,csvInfo2):
		self.objs=[]
		for o in csvInfo1[0].objs:
			co=copy.deepcopy(o)
			self.objs.append(co)
		keyObjs1=self.getKeyObjs(csvInfo1[1])
		keyObjs2=csvInfo2[0].getKeyObjs(csvInfo2[1])
		#print keyObjs1.keys()
		#print keyObjs2.keys()
		keyObjs={}
		for k in keyObjs1.keys():
			try:
				k1=keyObjs2[k]
				#found
				keyObjs[k]=(keyObjs1[k],keyObjs2[k])
			except:
				pass
		for k in keyObjs.keys():
			try:
				obj=keyObjs[k]
				lstLink=map(None,csvInfo1[2],csvInfo2[2])
				for lnk in lstLink:
					obj[0].__dict__[lnk[0]]=obj[1].__dict__[lnk[1]]
			except:
				pass
	def reduce(self,csvInfo1,csvInfo2):
		self.objs=[]
		keyObjs1=csvInfo1[0].getKeyObjs(csvInfo1[1])
		keyObjs2=csvInfo2[0].getKeyObjs(csvInfo2[1])
		keyObjs={}
		for k in keyObjs1.keys():
			try:
				k1=keyObjs2[k]
				#found
				keyObjs[k]=(keyObjs1[k],keyObjs2[k])
			except:
				pass
		for k in keyObjs.keys():
			try:
				objTmp=keyObjs[k]
				obj=copy.deepcopy(objTmp[0])
				self.objs.append(obj)
				lstLink=map(None,csvInfo1[2],csvInfo2[2])
				for lnk in lstLink:
					obj.__dict__[lnk[0]]=obj[1].__dict__[lnk[1]]
			except:
				pass
	
	def show(self,start=0,count=10):
		for i in range(start,start+count):
			try:
				self.objs[i].show()
			except:
				return
	def read(self,fn,sep=',',skip=0):
		self.sep=sep
		
		inf=open(fn)
		lines=inf.readlines()
		inf.close()
		self.setHdr(lines[skip][:-1])
		for l in lines[skip+1:]:
			self.addLineObj(l[:-1])

	def writeSimple(self,fn,hdr=None,sep=','):
		outf=open(fn,'w')
		if hdr is None:
			hdr=self.hdr
		s=string.join(hdr,sep)
		outf.write(s+'\n')
		for o in self.objs:
			o.write(outf,hdr,sep)
			outf.write('\n')
		outf.close()
		
	def write(self,of,hdr,hdr1,hdr2,multiline=0,write_hdrs=0):
		try:
			#write hdr
			if write_hdrs>0:
				s=string.join(hdr+hdr1+hdr2,self.sep)
				of.write(s+'\n')
			def tmp(a):
				return self.sep
			s1=string.join(map(tmp,hdr1),'')
			s2=string.join(map(tmp,hdr2),'')
			for o in self.objs:
				try:
					o.write(of,hdr,self.sep)
					if multiline>0:
						of.write('\n')
					#of.write(self.sep)
					try:
						o.obj1.write(of,hdr1,self.sep)
					except:
						of.write(s1)
					if multiline>0:
						of.write('\n')
					#of.write(self.sep)
					try:
						o.obj2.write(of,hdr2,self.sep)
					except:
						of.write(s2)
					of.write('\n')
				except:
					pass
		except:
			pass
	def writeSimpleXml(self,fn,hdr=None,row_name="row",level=0,export_name="csv"):
		""" write all objects to a xml-file.
		the row-name is given row_name. consider a header(hdr) like ["name","note"]
		will produce a xml file like:
		<?xml version="1.0" encoding="UTF-8"?>
		<csv>
		  <date>20031206</date>
		  <filename>/tmp/test.csv</filename>
		  <row>
		    <name>Walter</name>
		    <note>first name</note>
		  </row>
		  <row>
		    <name>Obweger</name>
		    <note>surename</note>
		  </row>
		</csv>
		"""
		outf=open(fn,'w')
		outf.write('<?xml version="1.0" encoding="UTF-8"?>')
		outf.write('<csv2xml>')
		outf.write('  <date>'+time.strftime('%Y%m%d',time.localtime(time.time()))+'</date>')
		outf.write('  <time>'+time.strftime('%H%M%S',time.localtime(time.time()))+'</time>')
		outf.write('  <asctime>'+time.asctime(time.localtime(time.time()))+'</asctime>')
		
		self.appendSimpleXml(outf,hdr,row_name,level=1)
		outf.write('</csv2xml>')
		outf.close()
	def appendSimpleXml(self,of,hdr=None,row_name="row",level=0,export_name="csv"):
		""" append all objects to a xml-file that's is already open (of).
		the row-name is given row_name. consider a header(hdr) like ["name","note"]
		will append xml content like:
		<csv>
		  <date>20031206</date>
		  <filename>/tmp/test.csv</filename>
		  <row>
		    <name>Walter</name>
		    <note>first name</note>
		  </row>
		  <row>
		    <name>Obweger</name>
		    <note>surename</note>
		  </row>
		</csv>
		"""
		strAdd=""
		for i in range(0,level):
			strAdd=strAdd+"  "
		of.write(strAdd+'<'+export_name+'>\n')
		
		if hdr is None:
			hdr=self.hdr
		strHdrs={}
		for h in hdr:
			strH=xmlBase.genXmlTagName(h)
			strHdrs[h]=strH
		for o in self.objs:
			of.write(strAdd+'<'+row_name+'>\n')
			for h in hdr:
				strH=strHdrs[h]
				try:
					of.write(strAdd+'  <'+strH+'>'+o.__dict__[h]+'</'+strH+'>\n')
					pass
				except:
					pass
			of.write(strAdd+'</'+row_name+'>\n')
		of.write(strAdd+'</'+export_name+'>\n')
	
	def writeXml(self,of,hdr,hdr1,hdr2,row_name="row",level=0,export_name="csv"):
		try:
			#write hdr
			def tmp(a):
				return self.sep
			s1=string.join(map(tmp,hdr1),'')
			s2=string.join(map(tmp,hdr2),'')
			strAdd="  "
			for i in range(0,level):
				strAdd=strAdd+"  "
			outf.write(strAdd[:-2]+'<'+export_name+'>')
			
			strLevel=strAdd+"  "
			for o in self.objs:
				outf.write(strAdd+'<'+row_name+'>\n')
				try:
					o.writeXml(of,hdr,strLevel)
					try:
						o.obj1.writeXml(of,hdr1,strLevel)
					except:
						pass
					try:
						o.obj2.writeXml(of,hdr2,strLevel)
					except:
						pass
				except:
					pass
				outf.write(strAdd+'</'+row_name+'>\n')
			outf.write(strAdd[:-2]+'</'+export_name+'>')
		except:
			pass
	def showOverview(self):
		print 'vtIOcsvObj'
		print '  count:',len(self.objs)
