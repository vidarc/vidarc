#----------------------------------------------------------------------------
# Name:         vtInOutCleanUp.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtInOutCleanUp.py,v 1.6 2008/05/20 23:25:10 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os,stat,os.path,time,datetime
import fnmatch

def vtInOutCleanUpByTime(dn,filter,keep=0,time_sel=stat.ST_MTIME):
    if time_sel not in [stat.ST_CTIME,stat.ST_MTIME]:
        return -1
    lst=[]
    try:
        files=os.listdir(dn)
    except:
        return -1
    for f in files:
        fn = os.path.join(dn, f)
        try:
            if keep>0:
                s=os.stat(fn)
                mode = s[stat.ST_MODE]
                if stat.S_ISREG(mode):
                    if fnmatch.fnmatch(f,filter):
                        # It's a file, call the callback function
                        zAccess=s[time_sel]
                        lst.append((zAccess,fn))
            else:
                if fnmatch.fnmatch(f,filter):
                    lst.append((0,fn))
        except:
            pass
    def compFunc(a,b):
        return b[0]-a[0]
    lst.sort(compFunc)
    for z,fn in lst[keep:]:
        os.remove(fn)
    return 0

def vtInOutCleanUpByDate(dn,filter,keep=10,keepDays=7,time_sel=stat.ST_CTIME,keepMin=1):
    if time_sel not in [stat.ST_CTIME,stat.ST_MTIME]:
        return -1
    t=time.localtime(time.time())
    zFile=datetime.date(2006,9,17)
    zNow=zFile.today()
    if keepDays<0:
        keepDays=0
    if keep<3:
        keep=3
    lst=[]
    try:
        files=os.listdir(dn)
    except:
        return -1
    for f in files:
        fn = os.path.join(dn, f)
        try:
            s=os.stat(fn)
            mode = s[stat.ST_MODE]
            if stat.S_ISREG(mode):
                if fnmatch.fnmatch(f,filter):
                    # It's a file, call the callback function
                    zAccess=s[time_sel]
                    lst.append((zAccess,fn))
        except:
            pass
    def compFunc(a,b):
        return b[0]-a[0]
    lst.sort(compFunc)
    for zAccess,fn in lst[keepMin:keep]:
        try:
            t=time.localtime(zAccess)
            zFile=zFile.replace(t[0],t[1],t[2])
            zDif=zNow-zFile
            if zDif.days>keepDays:
                os.remove(fn)
        except:
            pass
    for zAccess,fn in lst[keep:]:
        try:
            os.remove(fn)
        except:
            pass
    return 0

if __name__=='__main__':
    import time
    print 'generate cleanup test files modified time'
    for i in range(4):
        print '  generate file',i
        f=open('cleanTestMod%02d.txt'%i,'w')
        f.write('test file')
        f.close()
        time.sleep(1)
    vtInOutCleanUpByTime('.','clean*Mod*.txt',2)
    print 'generate cleanup test files create time'
    for i in range(4):
        print '  generate file',i
        f=open('cleanTestCreate%02d.txt'%i,'w')
        f.write('test file')
        f.close()
        time.sleep(1)
    vtInOutCleanUpByTime('.','clean*Create*.txt',2,stat.ST_CTIME)
    print 'test complete'
