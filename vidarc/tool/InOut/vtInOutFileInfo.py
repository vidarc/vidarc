# -*- coding: iso-8859-1 -*-
#----------------------------------------------------------------------
# Name:         vtInOutFileInfo.py
# Purpose:      file tools functions
#
# Author:       Walter Obweger
#
# Created:      20100216
# CVS-ID:       $Id: vtInOutFileInfo.py,v 1.7 2010/06/29 13:25:03 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os,stat,time
from vSystem import isWin

def clock():
    return time.close()
def now():
    return time.time()

def ftime2str(ft):
    "convert file time to predefined string"
    s=''
    t=time.gmtime(ft)
    s=time.strftime("%Y-%m-%d %H:%M:%S",t)
    return s

def getSizeStr(size,style=0):
    if style==0:
        return `size`
    elif style==1:
        s=0
        while (size>=1000) and (s<4):
            size=size/1024.0
            s=s+1
        sizeLst=['B','kB','MB','GB','TB']
        str="%6.3f%s"%(size,sizeLst[s])
        return str
    elif style==2:
        s=0
        while (size>=1000) and (s<4):
            size=size/1024.0
            s=s+1
        sizeLst=['  B',' kB',' MB',' GB',' TB']
        str="%6.3f%s"%(size,sizeLst[s])
        return str

def getFileInfoShort(sFN):
    try:
        s=os.stat(sFN)
        m=s[stat.ST_MODE]
        return {'size':s[stat.ST_SIZE],'zMod':s[stat.ST_MTIME]}
    except:
        pass
    return None

def getFileInfoFull(sFN):
    try:
        s=os.stat(sFN)
        m=s[stat.ST_MODE]
        return {
            'size':     s[stat.ST_SIZE],
            'zCreate':  s[stat.ST_CTIME],
            'zAccess':  s[stat.ST_ATIME],
            'zMod':     s[stat.ST_MTIME]}
    except:
        pass
    return None

def readInfo(sFN,sDN=None,dInfo=None):
    try:
        if dInfo is None:
            dInfo={}
        if sDN is not None:
            sFN=os.path.join(sDN,sFN)
        s=os.stat(sFN)
        
        m=s[stat.ST_MODE]
        if stat.S_ISDIR(m):
            dInfo.update({'type':'dir','size':-1})
        elif stat.S_ISREG(m):
            dInfo.update({'size':s[stat.ST_SIZE],'zMod':s[stat.ST_MTIME]})
        else:
            pass
        return dInfo
    except:
        return None

def readContense(sDN,dContense=None):
    if os.path.exists(sDN)==False:
        return None,-1
    if dContense is None:
        dContense={'root':sDN,'dDN':{},'dFN':{},'dErr':{}}
    dDN=dContense['dDN']
    dFN=dContense['dFN']
    dErr=dContense['dErr']
    M_IDX=stat.ST_MODE
    S_IDX=stat.ST_SIZE
    ZM_IDX=stat.ST_MTIME
    iSz=0
    for sN in os.listdir(sDN):
        sFN=os.path.join(sDN,sN)
        try:
            s=os.stat(sFN)
            m=s[M_IDX]
            if stat.S_ISDIR(m):
                dDN[sN]={'type':'dir','size':-1}#.append(sN)
            elif stat.S_ISREG(m):
                #lFN.append(sN)
                dFN[sN]={'size':s[S_IDX],'zMod':s[ZM_IDX]}
                iSz+=s[S_IDX]
            else:
                pass
        except:
            dErr[sN]={'type':'undef','size':-1}
    return dContense,iSz

def getExt(sFN):
    if sFN is None:
        return ''
    iExt=sFN.rfind('.')
    if iExt>0:
        return sFN[iExt+1:]
    return ''

def exists(sFN):
    if sFN is None:
        return -1
    if os.path.exists(sFN)==True:
        return 1
    return 0

def split(sFN):
    if sFN is None:
        return None,None
    sDN,s=os.path.split(sFN.replace('\\','/'))
    return sDN,s

def join(l):
    return u'/'.join([s.replace('\\','/') for s in l if s is not None])

def fn2posix(s):
    return s.replace('\\','/')

def posix2fn(s):
    if isWin():
        return s.replace('/','\\')
    else:
        return s[:]

def getcwd(bPosix=True):
    s=os.getcwd()
    if bPosix==True:
        return fn2posix(s)
    else:
        return s

def chdir(sDN):
    if isWin():
        os.chdir(posix2fn(sDN))
    else:
        os.chdir(fn2posix(sDN))
