#----------------------------------------------------------------------------
# Name:         vtInOutComp.py
# Purpose:
# Author:       Walter Obweger
#
# Created:      20060604
# CVS-ID:       $Id: vtInOutComp.py,v 1.7 2009/05/11 15:27:28 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.InOut.listFiles as listFiles
import vidarc.tool.InOut.listFilesXml as listFilesXml
import os
import time
import calendar
from vidarc.tool.xml.vtXmlDom import vtXmlDom
import vidarc.tool.log.vtLog as vtLog
import zipfile

class vtInOutCompFileEntry:
    UNDEFINED          = 0x000
    FILE_EQUAL         = 0x001
    FILE_SOURCE_1      = 0x002
    FILE_SOURCE_2      = 0x004
    FILE_DIF_SIZE      = 0x008
    FILE_DIF_ACCESS    = 0x010
    FILE_DIF_MOD       = 0x020
    FILE_DIF_STAT      = 0x040
    FILE_DIF_CONTENT   = 0x080
    FILE_RESULT_STR = ['undefined','equal','source 1','source 2',
                       'different size','different access',
                       'different modification','different stat',
                       'different content']
    FILE_RESULT_STR_XML = ['UNDEFINED','EQUAL','SOURCE_1','SOURCE_2',
                       'DIFF_SIZE','DIFF_ACCESS',
                       'DIFF_MOD','DIFF_STA',
                       'DIFF_CONTENT']
    COMP_DFT = 0x0A
    def __init__(self):
        self.result=vtInOutCompFileEntry.UNDEFINED
        self.file1=None
        self.file2=None
    def compare(self,file1=None,file2=None,style=0xf):
        """

style
    0x01 ... check last access time
    0x02 ... check modification time
    0x04 ... check stat time
    0x08 ... check content (by MD5)
    0x10 ... check content when necessary (by MD5)
        """
        self.file1=file1
        self.file2=file2
        self.result=vtInOutCompFileEntry.UNDEFINED
        if self.file1 is None:
            if self.file2 is None:
                self.result=vtInOutCompFileEntry.UNDEFINED
            else:
                self.result=vtInOutCompFileEntry.FILE_SOURCE_2
        else:
            if self.file2 is None:
                self.result=vtInOutCompFileEntry.FILE_SOURCE_1
            else:
                # process compare
                if self.file1.size <> self.file2.size:
                    self.result |= vtInOutCompFileEntry.FILE_DIF_SIZE
                if (style & 0x01) == 0x01:
                    if self.file1.last_access != self.file2.last_access:
                        self.result |= vtInOutCompFileEntry.FILE_DIF_ACCESS
                if (style & 0x02) == 0x02:
                    if self.file1.mod_access != self.file2.mod_access:
                        self.result |= vtInOutCompFileEntry.FILE_DIF_MOD
                if (style & 0x04) == 0x04:
                    if self.file1.stat_access != self.file2.stat_access:
                        self.result |= vtInOutCompFileEntry.FILE_DIF_STAT
                if (style & 0x08) == 0x08:
                    if self.file1.md5 != self.file2.md5:
                        self.result |= vtInOutCompFileEntry.FILE_DIF_CONTENT
                if (self.result & 0xF8) == 0:
                    self.result |= vtInOutCompFileEntry.FILE_EQUAL
    def getResultStr(self):
        sRes=''
        for i in range(0,8):
            if ((self.result & 1 << i) != 0):
                sRes=sRes+vtInOutCompFileEntry.FILE_RESULT_STR[i+1]+','
        return sRes
    def isEqual(self):
        return self.result==self.FILE_EQUAL
    def isDiffSize(self):
        return (self.result&self.FILE_DIF_SIZE)!=0
    def isDiffAccess(self):
        return (self.result&self.FILE_DIF_ACCESS)!=0
    def isDiffModify(self):
        return (self.result&self.FILE_DIF_MOD)!=0
    def isDiffCreate(self):
        return (self.result&self.FILE_DIF_STAT)!=0
    def isDiffContent(self):
        return (self.result&self.FILE_DIF_CONTENT)!=0
    def getDiffAccess(self):
        if self.file1 is None:
            return -1
        if self.file2 is None:
            return 1
        return self.file2.last_access - self.file1.last_access
    def getDiffModify(self):
        if self.file1 is None:
            return -1
        if self.file2 is None:
            return 1
        return self.file2.mod_access - self.file1.mod_access
    def getDiffCreate(self):
        if self.file1 is None:
            return -1
        if self.file2 is None:
            return 1
        return self.file2.stat_access - self.file1.stat_access
    def hasSrc1(self):
        return self.file1 is not None
    def hasSrc2(self):
        return self.file2 is not None
    def getName(self):
        if self.file1 is not None:
            return self.file1.name
        elif self.file2 is not None:
            return self.file2.name
        else:
            return ''
    def getSize(self):
        if self.file1 is not None:
            return self.file1.size
        elif self.file2 is not None:
            return self.file2.size
        else:
            return -1
    def getMD5(self):
        if self.file1 is not None:
            return self.file1.md5 or ''
        elif self.file2 is not None:
            return self.file2.md5 or ''
        else:
            return ''
    def getFullFN(self,sDN,skip=0):
        if self.file1 is not None:
            return self.file1.getFullFN(sDN,skip)
        elif self.file2 is not None:
            return self.file2.getFullFN(sDN,skip)
        else:
            return ''
    def show(self,level=0):
        try:
            fn1=self.file1.name
        except:
            fn1="None"
        try:
            fn2=self.file2.name
        except:
            fn2="None"
        try:
            ffn1=self.file1.fullname
        except:
            ffn1="None"
        try:
            ffn2=self.file2.fullname
        except:
            ffn2="None"
        sRes=''
        for i in range(0,8):
            if ((self.result & 1 << i) != 0):
                sRes=sRes+vtInOutCompFileEntry.FILE_RESULT_STR[i+1]+','
        print sRes,fn1,fn2
        if level>1:
            print "  ",ffn1
        if level>2:
            try:
                print "    ",self.file1.size
                print "    ",self.file1.last_access
            except:
                pass
        if level>1:
            print "  ",ffn2
        if level>2:
            try:
                print "    ",self.file2.size
                print "    ",self.file2.last_access
            except:
                pass

class vtInOutCompDirEntry:
    def __init__(self,bCreate=False):
        self.base=''
        self.dir_results=[]
        self.file_results=[]
        if bCreate:
            self.doc=vtXmlDom(appl=self.__class__.__name__,audit_trail=False)
        else:
            self.doc=None
        self.iDiff=0
        self.ClearCount()
    def ClearCount(self):
        self.lAct=0
        self.lSize=0
    def getDiffCount(self):
        return self.iDiff
    def unlink(self):
        if self.doc is not None:
            self.doc.unlink()
    def _addNode_(self,elem,dirEntry,level=1,style=0xf):
        """
style
    0x01 ... include equal
    0x02 ... include results which are only part of source 1
    0x04 ... include results which are only part of source 2
    0x08 ... include different
        """
        elemDir=self.doc.createChildByLst(elem,'DIR',[
            #{'tag':'BASE','val':unicode(dirEntry.base,'iso-8859-1')},
            {'tag':'BASE','val':dirEntry.base},
            ])
        
        try:
            if len(dirEntry.base_dir1)>0:
                # add base1
                self.doc.setNodeText(elemDir,"BASE_DIR_1",dirEntry.base_dir1)
        except:
            pass
        
        try:
            if len(dirEntry.base_dir2)>0:
                # add base2
                self.doc.setNodeText(elemDir,"BASE_DIR_2",dirEntry.base_dir2)
        except:
            pass
        
        for f in dirEntry.file_results:
            bOutput=0
            if ((f.result & f.FILE_EQUAL) == f.FILE_EQUAL) and ((style & 0x1) == 0x01):
                bOutput=1
            if ((f.result & f.FILE_SOURCE_1) == f.FILE_SOURCE_1) and ((style & 0x2) == 0x02):
                bOutput=1
            if ((f.result & f.FILE_SOURCE_2) == f.FILE_SOURCE_2) and ((style & 0x4) == 0x04):
                bOutput=1
            if ((f.result & 0xF8) != 0) and ((style & 0x8) == 0x08):
                bOutput=1
            
            if bOutput > 0:
                l=[]
                for i in range(0,8):
                    if ((f.result & 1 << i) != 0):
                        sRes=vtInOutCompFileEntry.FILE_RESULT_STR_XML[i+1]
                        # add compare result
                        l.append({'tag':sRes,'val':'true'})
                
                if f.file1 is not None:
                    if f.file1.md5==None:
                        strMD5=""
                    else:
                        strMD5=f.file1.md5
                    lFile1=[{'tag':"NAME",'val':f.file1.name},
                        {'tag':"FULLNAME",'val':f.file1.fullname},
                        {'tag':"SIZE",'val':"%d"%f.file1.size},
                        {'tag':"LAST_ACCESS",'val':listFilesXml.ftime2xml(f.file1.last_access)},
                        {'tag':"MOD_ACCESS",'val':listFilesXml.ftime2xml(f.file1.mod_access)},
                        {'tag':"STAT_ACCESS",'val':listFilesXml.ftime2xml(f.file1.stat_access)},
                        {'tag':"MD5",'val':strMD5}]
                    l.append({'tag':'FILE_1','lst':lFile1})
                if f.file2 is not None:
                    if f.file2.md5==None:
                        strMD5=""
                    else:
                        strMD5=f.file2.md5
                    lFile2=[{'tag':"NAME",'val':f.file2.name},
                        {'tag':"FULLNAME",'val':f.file2.fullname},
                        {'tag':"SIZE",'val':"%d"%f.file2.size},
                        {'tag':"LAST_ACCESS",'val':listFilesXml.ftime2xml(f.file2.last_access)},
                        {'tag':"MOD_ACCESS",'val':listFilesXml.ftime2xml(f.file2.mod_access)},
                        {'tag':"STAT_ACCESS",'val':listFilesXml.ftime2xml(f.file2.stat_access)},
                        {'tag':"MD5",'val':strMD5}]
                
                    l.append({'tag':'FILE_2','lst':lFile2})
                elemCompNode=self.doc.createChildByLst(elemDir,"FILE_COMPARE",l)
        
        for f in dirEntry.dir_results:
            self._addNode_(elemDir,f,level+1,style)
        
    def buildXml(self,style=0xf):
        """
style
    0x01 ... include equal
    0x02 ... include results which are only part of source 1
    0x04 ... include results which are only part of source 2
    0x08 ... include different
        """
        self.doc.Close()
        self.doc.New(root='DirEntry')
        self._addNode_(self.doc.getRoot(),self,0,style)
        self.doc.AlignDoc()

    def _buildDir(self,dirEntry,d,skip=0,flat=0,thd=None):
        try:
            dirN=dirEntry.base[skip:]
        except:
            dirN=''
        if thd is not None:
            def post():
                thd.Post('proc',thd.lAct,thd.lSize)
            def addAct(i):
                thd.lAct+=i
            def addSize(i):
                thd.lSize+=i
        else:
            def post():
                pass
            def addAct(i):
                pass
            def addSize(i):
                pass
        addAct(1)
        post()
        df=d['file']
        for f in dirEntry.files:
            addAct(1)
            post()
            key=f.name
            if len(dirN):
                f.fullname=dirN+'/'+f.name
            else:
                f.fullname=f.name
            if key in df:
                e=df[key]
                e.append(f)
                #addSize(f.size)
            else:
                e=[f]
                df[key]=e
        if flat!=0:
            for dir in dirEntry.dirs:
                self._buildDir(dir,d,skip,flat,thd=thd)
        else:
            for dir in dirEntry.dirs:
                nd={'dir':{},'file':{}}
                nd['baseDN']=dir.base
                d['dir'][dir.base]=nd
                self._buildDir(dir,nd,skip,flat,thd=thd)
    def _doCompare(self,dirTree1,dirTree2,baseDN1,baseDN2,
                    style=0xf,skip1=0,skip2=0,thd=None):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            bDbg=True
        else:
            bDbg=False
        dirFile1=dirTree1['file']
        keys1=dirFile1.keys()
        dirFile2=dirTree2['file']
        keys2=dirFile2.keys()
        #baseDN1=dirTree1.get('baseDN',None)
        #baseDN2=dirTree2.get('baseDN',None)
        if baseDN1 is None:
            vtLog.vtLngCurCls(vtLog.ERROR,'baseDN1 unknown'%(),self)
        if baseDN2 is None:
            vtLog.vtLngCurCls(vtLog.ERROR,'baseDN2 unknown'%(),self)
        if (style & 0x10) == 0x10:
            bCalcMD5=True
        else:
            bCalcMD5=False
        #vtLog.CallStack('')
        #print dirTree1
        #print dirTree2
        if thd is not None:
            def post():
                #vtLog.vtLngCurCls(vtLog.DEBUG,''%(),self)
                thd.Post('proc',thd.lAct,thd.lSize)
            def addAct(i):
                thd.lAct+=i
            def is2Stop():
                return thd.Is2Stop()
        else:
            def post():
                pass
            def addAct(i):
                pass
            def is2Stop():
                return False
        keys1.sort()
        keys2.sort()
        bFin=0
        iLen1=len(keys1)
        iLen2=len(keys2)
        i=0
        j=0
        iDiff=0
        while (bFin==0) and (is2Stop()==False):
            iAdd=0
            try:
                k1=keys1[i]
                files1=dirFile1[k1]
            except:
                k1=''
                files1=[]
                iAdd=2
            try:
                k2=keys2[j]
                files2=dirFile2[k2]
            except:
                k2=''
                files2=[]
                iAdd=iAdd+1
            if iAdd==0:
                if k1==k2:
                    iAdd=0
                else:
                    if k1<k2:
                        iAdd=1
                    else:
                        iAdd=2
            if bDbg:
                vtLog.vtLngCurCls(vtLog.DEBUG,'i:%8d;j:%8d;k1:%s;k2:%s;iAdd:%d'%(i,j,k1,k2,iAdd),self)
            if iAdd==0:
                #same key
                lCalc=[0]
                for f1 in files1:
                    bCalcF1=False
                    if bDbg:
                        vtLog.vtLngCurCls(vtLog.DEBUG,'fn1:%s;dn:%s'%(f1.fullname,baseDN1),self)
                    for f2 in files2:
                        cf=vtInOutCompFileEntry()
                        if bDbg:
                            vtLog.vtLngCurCls(vtLog.DEBUG,'fn2:%s;dn:%s'%(f2.fullname,baseDN2),self)
                        if bCalcMD5==True:
                            if f1.size == f2.size:
                                if f1.md5 is None or len(f1.md5)==0:
                                    if baseDN1 is not None:
                                        if thd is not None:
                                            bCalcF1=True
                                            sFN='/'.join([baseDN1,f1.fullname])
                                            thd.calcFileInfo(f1,sFN,style=0x01)
                                        else:
                                            sFN='/'.join([baseDN1,f1.fullname])
                                            sTmpDN,sTmpFN=os.path.split(sFN)
                                            f1.calcInfo(sTmpDN)
                                #else:
                                #    addAct(f1.size)
                                if f2.md5 is None or len(f2.md5)==0:
                                    if baseDN2 is not None:
                                        if thd is not None:
                                            lCalc.append(f2.size)
                                            sFN='/'.join([baseDN2,f2.fullname])
                                            thd.calcFileInfo(f2,sFN,style=0x01)
                                        else:
                                            sFN='/'.join([baseDN2,f2.fullname])
                                            sTmpDN,sTmpFN=os.path.split(sFN)
                                            f2.calcInfo(sTmpDN)
                                #else:
                                #    addAct(f2.size)
                            #else:
                                #addAct(f1.size)
                            #    addAct(f2.size)
                        #else:
                            #addAct(f1.size)
                        #    addAct(f2.size)
                        post()
                        cf.compare(f1,f2,style)
                        self.file_results.append(cf)
                        if (cf.result&0xf8)!=0:
                            iDiff+=1
                    if bCalcF1==False:
                        addAct(f1.size)
                iAllSize=0
                for f2 in files2:
                    iAllSize+=f2.size
                addAct(iAllSize-reduce(lambda x,y:x+y,lCalc))
                i=i+1
                j=j+1
            else:
                if iAdd==1:
                    for f1 in files1:
                        cf=vtInOutCompFileEntry()
                        cf.compare(f1,None,style)
                        self.file_results.append(cf)
                        addAct(f1.size)
                        post()
                        iDiff+=1
                    if i<iLen1:
                        i=i+1
                elif iAdd==2:
                    for f2 in files2:
                        cf=vtInOutCompFileEntry()
                        cf.compare(None,f2,style)
                        self.file_results.append(cf)
                        addAct(f2.size)
                        post()
                        iDiff+=1
                    if j<iLen2:
                        j=j+1
            post()
            if (i>=iLen1) and (j>=iLen2):
                bFin=1
        self.iDiff=iDiff
        dirDir1=dirTree1['dir']
        keys1=dirDir1.keys()
        dirDir2=dirTree2['dir']
        keys2=dirDir2.keys()
        keys1.sort()
        keys2.sort()
        bFin=0
        iLen1=len(keys1)
        iLen2=len(keys2)
        i=0
        j=0
        while bFin==0:
            iAdd=0
            try:
                k1=keys1[i]
                d1=dirDir1[k1]
            except:
                k1=''
                d1={'dir':{},'file':{}}
                iAdd=2
            try:
                k2=keys2[j]
                d2=dirDir2[k2]
            except:
                k2=''
                d2={'dir':{},'file':{}}
                iAdd=iAdd+1
            if iAdd==0:
                if k1[skip1:]==k2[skip2:]:
                    iAdd=0
                else:
                    if k1[skip1:]<k2[skip2:]:
                        iAdd=1
                    else:
                        iAdd=2
            #print self.base,i,j
            #print "key",k1,k2,iAdd
            if iAdd==0:
                compDir=vtInOutCompDirEntry(bCreate=False)
                addAct(1)
                post()
                #if len(self.base)>0:
                #    compDir.base=self.base+'/'+k1[skip1:]
                #else:
                #    compDir.base=k1[skip1:]
                compDir.base=k1[skip1:]
                compDir._doCompare(d1,d2,baseDN1,baseDN2,
                                    style,skip1,skip2,
                                    thd=thd)
                self.iDiff+=compDir.iDiff
                self.dir_results.append(compDir)
                i=i+1
                j=j+1
            else:
                if iAdd==1:
                    compDir=vtInOutCompDirEntry(bCreate=False)
                    #if len(self.base)>0:
                    #    compDir.base=self.base+'/'+k1[skip1:]
                    #else:
                    #    compDir.base=k1[skip1:]
                    compDir.base=k1[skip1:]
                    compDir._doCompare(d1,{'dir':{},'file':{}},
                                    baseDN1,baseDN2,
                                    style,skip1=skip1,skip2=0,
                                    thd=thd)
                    self.iDiff+=compDir.iDiff
                    self.dir_results.append(compDir)
                    if i<iLen1:
                        i=i+1
                elif iAdd==2:
                    compDir=vtInOutCompDirEntry(bCreate=False)
                    #if len(self.base)>0:
                    #    compDir.base=self.base+'/'+k2[skip2:]
                    #else:
                    #    compDir.base=k2[skip2:]
                    compDir.base=k2[skip2:]
                    compDir._doCompare({'dir':{},'file':{}},d2,
                                    baseDN1,baseDN2,
                                    style,skip1=0,skip2=skip2,
                                    thd=thd)
                    self.iDiff+=compDir.iDiff
                    self.dir_results.append(compDir)
                    if j<iLen2:
                        j=j+1
            if (i>=iLen1) and (j>=iLen2):
                bFin=1
    def compareFlat(self,dirEntry1,dirEntry2,style=0xf,iCount=0,thd=None):
        if thd is not None:
            thd.ClearCount()
            thd.lSize=iCount
        self.base=''
        self.dir_results=[]
        self.file_results=[]
        t1={'dir':{},'file':{}}
        t1['baseDN']=dirEntry1.base
        skip1=len(dirEntry1.base)
        if dirEntry1.base[-1]=='/':
            pass
        else:
            skip1+=1
        self._buildDir(dirEntry1,t1,skip=skip1,flat=1,thd=thd)
        t2={'dir':{},'file':{}}
        t2['baseDN']=dirEntry2.base
        skip2=len(dirEntry2.base)
        if dirEntry2.base[-1]=='/':
            pass
        else:
            skip2+=1
        self._buildDir(dirEntry2,t2,skip=skip2,flat=1,thd=thd)
        #if thd is not None:
        #    if thd.__isLogDebug__():
        #        thd.__logDebug__('t1:%s'%(thd.__logFmt__(t1)))
        #        thd.__logDebug__('t2:%s'%(thd.__logFmt__(t2)))
        
        #self.lSize=dirEntry1.size+dirEntry2.size
        self.resultTree={'equal':[],'':[],'':[],'diff':[]}
        self.base=''
        self.base_dir1=dirEntry1.base
        self.base_dir2=dirEntry2.base
        self._doCompare(t1,t2,dirEntry1.base,dirEntry2.base,
                        style,skip1,skip2,thd=thd)
        #self.buildXml()
    def compareHier(self,dirEntry1,dirEntry2,style=0xf,iCount=0,thd=None):
        if thd is not None:
            thd.ClearCount()
            thd.lSize=iCount
        self.base=''
        self.dir_results=[]
        self.file_results=[]
        t1={'dir':{},'file':{}}
        t1['baseDN']=dirEntry1.base
        skip1=len(dirEntry1.base)
        if dirEntry1.base[-1]=='/':
            pass
        else:
            skip1+=1
        self._buildDir(dirEntry1,t1,skip=skip1,thd=thd)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurCls(vtLog.DEBUG,vtLog.pformat(t1),self)
        t2={'dir':{},'file':{}}
        t2['baseDN']=dirEntry2.base
        skip2=len(dirEntry2.base)
        if dirEntry2.base[-1]=='/':
            pass
        else:
            skip2+=1
        self._buildDir(dirEntry2,t2,skip=skip2,thd=thd)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurCls(vtLog.DEBUG,vtLog.pformat(t2),self)
        #self.lSize=dirEntry1.size+dirEntry2.size
        self.base=''
        self.base_dir1=dirEntry1.base
        self.base_dir2=dirEntry2.base
        self._doCompare(t1,t2,dirEntry1.base,dirEntry2.base,
                        style,skip1,skip2,thd=thd)
        #self.buildXml()
    def compare(self,dirEntry1,dirEntry2,style=0x1f):
        """

style
    0x01 ... check last access time
    0x02 ... check modification time
    0x04 ... check stat time
    0x08 ... check content (by MD5)
    0x10 ... check filenames
        """
        self.base=''
        self.dir_results=[]
        self.file_results=[]
        if (style & 0x10) == 0x10:
            self.compareFlat(dirEntry1,dirEntry2,style)
        else:
            self.compareHier(dirEntry1,dirEntry2,style)
    def write(self,fn,mode=1,style=0xf):
        """writes information to an xml-file named <fn>.
fn      name for the output file
mode   definies write mode
   0 ... generate xml-file
   1 ... compress xml-file into a zipfile named <fn.zip> and remove plant xml-file.
        """
        """
style
    0x01 ... include equal
    0x02 ... include results which are only part of source 1
    0x04 ... include results which are only part of source 2
    0x08 ... include different
        """
        try:
            self.buildXml(style)
            self.doc.Save(fn)
            if mode==1:
                #do zip
                try:
                    zf=zipfile.ZipFile(fn+".zip","w",zipfile.ZIP_DEFLATED)
                    zf.write(fn)
                    zf.close()
                    os.remove(fn)
                except:
                    pass
        except:
            import traceback
            traceback.print_exc()
            pass
    def show(self,level=1):
        print self.base
        for f in self.file_results:
            f.show(level)
        for d in self.dir_results:
            d.show(level)
def demo():
    print
    print "CompareFilesXml","demo"
    t=time.time()
    f1=listFiles.FileEntry()
    f1.name="test.doc"
    f1.size=100
    f1.last_access=t
    
    f2=listFiles.FileEntry()
    f2.name="test.doc"
    f2.size=100
    f2.last_access=t

    print 'should return: equal'
    cf1=vtInOutCompFileEntry()
    cf1.compare(f1,f2)
    cf1.show()
    
    print 'should return: different access'
    f2.last_access=t-1
    cf1.compare(f1,f2)
    cf1.show()

    print 'should return: different modification'
    f1.mod_access=t
    f2.mod_access=t-1
    f2.last_access=t
    cf1.compare(f1,f2)
    cf1.show()
    
    print 'should return: different stat'
    f1.mod_access=t
    f1.stat_access=t
    f2.mod_access=t
    f2.stat_access=t-1
    cf1.compare(f1,f2)
    cf1.show()
    
    print 'should return: different content'
    f1.md5="1234"
    f1.stat_access=t
    f2.md5="12345"
    f2.stat_access=t
    cf1.compare(f1,f2)
    cf1.show()
    
    print 'should return: different content'
    f1.md5="123456"
    f1.stat_access=t
    f2.md5="12345"
    f2.stat_access=t
    cf1.compare(f1,f2)
    cf1.show()
    
    print 'should return: equal'
    f1.md5="12345"
    f1.stat_access=t
    f2.md5="12345"
    f2.stat_access=t
    cf1.compare(f1,f2)
    cf1.show()
    
    print "CompareFilesXml","demo","end"
    print
    
def demoDirs():
    print
    print "CompareFilesXml","demoDirs"
    dirs=listFiles.DirEntry()
    dirs.base="folder"
    
    t=time.time()
    f1=listFiles.FileEntry()
    f1.name="file01.doc"
    f1.size=101
    f1.last_access=t
    
    f2=listFiles.FileEntry()
    f2.name="file02.doc"
    f2.size=102
    f2.last_access=t

    dirs.files.append(f1)
    dirs.files.append(f2)
    
    d2=listFiles.DirEntry()
    d2.base="folder/subfolder1"
    f1=listFiles.FileEntry()
    f1.name="file03.doc"
    f1.size=103
    f1.last_access=t
    
    f2=listFiles.FileEntry()
    f2.name="file04.doc"
    f2.size=104
    f2.last_access=t
    d2.files.append(f1)
    d2.files.append(f2)
    dirs.dirs.append(d2)
    
    d2=listFiles.DirEntry()
    d2.base="folder/subfolder2"
    f1=listFiles.FileEntry()
    f1.name="file05.doc"
    f1.size=105
    f1.last_access=t
    
    f2=listFiles.FileEntry()
    f2.name="file06.doc"
    f2.size=106
    f2.last_access=t
    f3=listFiles.FileEntry()
    f3.name="file01.doc"
    f3.size=101
    f3.last_access=t
    f4=listFiles.FileEntry()
    f4.name="file08.doc"
    f4.size=101
    f4.last_access=t
    d2.files.append(f1)
    d2.files.append(f2)
    d2.files.append(f3)
    d2.files.append(f4)
    dirs.dirs.append(d2)

    d3=listFiles.DirEntry()
    d3.base="folder/subfolder4"
    f1=listFiles.FileEntry()
    f1.name="file15.doc"
    f1.size=105
    f1.last_access=t
    d3.files.append(f1)
    dirs.dirs.append(d3)

    cdirs=vtInOutCompDirEntry()
    t1={'dir':{},'file':{}}
    cdirs._buildDir(dirs,t1,skip=len(dirs.base)+1)
    t2={'dir':{},'file':{}}
    cdirs._buildDir(dirs,t2,skip=len(dirs.base)+1,flat=1)
    
    print
    print 'directory tree 1'
    print t1
    print
    print 'directory tree 2'
    print t2
    
    
    dirs2=listFiles.DirEntry()
    dirs2.base="folder"
    
    f1=listFiles.FileEntry()
    f1.name="file01.doc"
    f1.size=111
    f1.last_access=t
    
    f2=listFiles.FileEntry()
    f2.name="file02.doc"
    f2.size=102
    f2.last_access=t-1

    dirs2.files.append(f1)
    dirs2.files.append(f2)
    
    d2=listFiles.DirEntry()
    d2.base="folder/subfolder1"
    f1=listFiles.FileEntry()
    f1.name="file03.doc"
    f1.size=103
    f1.last_access=t
    
    f2=listFiles.FileEntry()
    f2.name="file04.doc"
    f2.size=104
    f2.last_access=t
    d2.files.append(f1)
    d2.files.append(f2)
    dirs2.dirs.append(d2)
    
    d2=listFiles.DirEntry()
    d2.base="folder/subfolder2"
    f1=listFiles.FileEntry()
    f1.name="file05.doc"
    f1.size=105
    f1.last_access=t+1
    
    f2=listFiles.FileEntry()
    f2.name="file06.doc"
    f2.size=106
    f2.last_access=t
    f3=listFiles.FileEntry()
    f3.name="file01.doc"
    f3.size=101
    f3.last_access=t
    f4=listFiles.FileEntry()
    f4.name="file07.doc"
    f4.size=101
    f4.last_access=t
    d2.files.append(f1)
    d2.files.append(f2)
    d2.files.append(f3)
    d2.files.append(f4)
    dirs2.dirs.append(d2)
    
    d3=listFiles.DirEntry()
    d3.base="folder/subfolder3"
    f1=listFiles.FileEntry()
    f1.name="file09.doc"
    f1.size=105
    f1.last_access=t+1
    d3.files.append(f1)
    dirs2.dirs.append(d3)
    
    
    cdirs.compareFlat(dirs,dirs2)
    cdirs.show()
    cdirs.write('testComp1Flat.xml',0)

    cdirs.compareHier(dirs,dirs2)
    cdirs.show()
    cdirs.write('testComp2Hier.xml',0)
    
    cdirsHier=vtInOutCompDirEntry()
    cdirsHier.compareHier(dirs,dirs2)
    cdirsHier.show()
    cdirsHier.write('testComp1Hier.xml',0)
    cdirsHier.write('testComp3Hier.xml',1)

    cdirsHier.write('testComp1Hier_equal.xml',0,0x1)  # equal
    cdirsHier.write('testComp1Hier_src1.xml',0,0x2)   # src1
    cdirsHier.write('testComp1Hier_src2.xml',0,0x4)   # src2
    cdirsHier.write('testComp1Hier_dif.xml',0,0x8)    # diff

if __name__=="__main__":
    demo()
    demoDirs()
            
