#----------------------------------------------------------------------------
# Name:         vtInOutFileIdxLinesWid.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20061122
# CVS-ID:       $Id: vtInOutFileIdxLinesWid.py,v 1.11 2009/05/07 19:08:55 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

import  colorsys
import vidarc.tool.art.vtArt as vtArt
from vidarc.tool.InOut.vtInOutFileIdxLines import vtInOutFileIdxLines
from vidarc.tool.vtThreadMixinWX import vtThreadMixinWX

wxEVT_VTINOUT_FILEIDXLINES_ADD=wx.NewEventType()
vEVT_VTINOUT_FILEIDXLINES_ADD=wx.PyEventBinder(wxEVT_VTINOUT_FILEIDXLINES_ADD,1)
def EVT_VTINOUT_FILEIDXLINES_ADD(win,func):
    win.Connect(-1,-1,wxEVT_VTINOUT_FILEIDXLINES_ADD,func)
class vtInOutFileIdxLinesAdd(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VTINOUT_FILEIDXLINES_ADD(<widget_name>, self.OnAdd)
    """
    def __init__(self,obj,s,bEnd=False,bDel=False,idx=-1,bSel=True):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTINOUT_FILEIDXLINES_ADD)
        self.bEnd=bEnd
        self.bDel=bDel
        self.iIdx=idx
        self.sVal=s
        self.bSel=bSel
    def AtEnd(self):
        return self.bEnd
    def Is2Del(self):
        return self.bDel
    def GetStr(self):
        return self.sVal
    def GetIdx(self):
        return self.iIdx
    def Is2Sel(self):
        return self.bSel
class vtInOutBufIndicator(wx.PyWindow):
    def __init__(self, parent, ID=-1, pos=wx.DefaultPosition, size=wx.DefaultSize,
                 style=0, name="vtInOutBufIndicator",bReverse=False,space=4):
        wx.PyWindow.__init__(self, parent, ID, pos, size, style, name)
        self.bReverse=bReverse
        size = wx.Size(*size)
        bestSize = self.GetBestSize()
        size.x = max(size.x, bestSize.x)
        size.y = max(size.y, bestSize.y)
        self.iSpace=space
        self.SetSize(size)
        self.Set(0,1000,100,False)
        self._genBmp(size.x,size.y)
        self.Bind(wx.EVT_PAINT, self.OnPaint)
        self.Bind(wx.EVT_ERASE_BACKGROUND, lambda x: None)
        self.Bind(wx.EVT_SIZE, self.OnSize)
        #self.Bind(wx.EVT_RIGHT_DOWN, self.OnRightDown)
        #self.Bind(wx.EVT_RIGHT_UP, self.OnRightClick)         
    def DoGetBestSize(self):
        return wx.Size(100,12)
    def OnPaint(self, event):
        dc = wx.BufferedPaintDC(self)
        #if hasattr(self, 'coords'):
        self._doDraw(dc, True)
    def OnSize(self, evt):
        size  = self.GetClientSize()
        if size.x < 5 or size.y < 5:
            return
        self._genBmp(size[0],size[1])
    def _genBmp(self,iW,iH):
        self.bmpIndicator = wx.EmptyBitmap(max(iW,5), max(iH,5))
        backgroundBrush = wx.Brush(wx.Colour(184,184,184), wx.SOLID)
        #backgroundBrush = wx.Brush(self.GetParent().GetBackgroundColour(), wx.SOLID)
        drawDC = wx.MemoryDC()
        drawDC.SelectObject(self.bmpIndicator)
        drawDC.SetBackground(backgroundBrush)
        drawDC.Clear()
        drawDC.DrawBitmap(self.bmpIndicator, 0,0)
        #drawDC.SetBrush(wx.Brush(wx.Colour(124,124,124), wx.SOLID))
        #drawDC.DrawRectangle(0,0,iW,iH)
        self._doDraw(drawDC)
    def _doDraw(self, drawDC, force=0):
        size = self.GetClientSize()
        iW,iH=size
        self.iThumb=max(int(round(iW*self.fPer+0.5)),self.iSpace*2)
        #drawDC.SelectObject(self.bmpIndicator)
        drawDC.DrawBitmap(self.bmpIndicator, 0,0)
        iX=int((self.iPos/float(self.iSize))*(iW-self.iThumb))
        hS,sS,vS = colorsys.rgb_to_hsv( 35 / 255.0, 182 / 255.0,  35 / 255.0)
        #hE,sE,vE = colorsys.rgb_to_hsv(183 / 255.0,  0 / 255.0, 0 / 255.0)
        hE,sE,vE = hS,sS,vS
        vE=0.2
        d=float(self.iThumb)
        fH=float(hE-hS) / d
        fS=float(sE-sS) / d
        fE=float(vE-vS) / d
        #print 
        #print self.iPos,self.iSize,self.iBufSize,self.fPer,self.iThumb
        #print '   ',iX,iW,iH,self.iSpace
        #print fColPer
        backgroundBrush = wx.Brush(wx.Colour(184,184,184), wx.SOLID)
        drawDC.SetBrush(wx.Brush(wx.Colour(124,124,124), wx.SOLID))
        drawDC.DrawRectangle(0,0,iW,iH)
        if self.bReverse:
            iX+=self.iThumb
            pass
        #else:
        #    iX-=self.iSpace
        for x in xrange(self.iThumb):
            drawDC.SetPen(wx.BLACK_PEN)
            r,g,b = [c * 255.0 for c in colorsys.hsv_to_rgb(hS+fH*x,sS+fS*x,vS+fE*x)]
            colour = wx.Colour(int(r), int(g), int(b))
            drawDC.SetPen(wx.Pen(colour, 1, wx.SOLID))
            if self.bReverse:
                drawDC.DrawRectangle(iX-x-1,1,1,iH-2)
            else:
                drawDC.DrawRectangle(iX+x,1,1,iH-2)
        drawDC.SetPen(wx.BLACK_PEN)
        drawDC.DrawLine(0,0,iW,0)
        drawDC.DrawLine(0,0,0,iH)
        drawDC.SetPen(wx.WHITE_PEN)
        drawDC.DrawLine(0,iH-1,iW,iH-1)
        drawDC.DrawLine(iW-1,0,iW-1,iH)
    def SetReverse(self,bReverse,bUpdate=False):
        if self.bReverse!=bReverse:
            self.bReverse=bReverse
        if bUpdate:
            self.DoDraw()
    def Set(self,iPos,iSize,iBufSz,bUpdate=True):
        self.iBufSize=iBufSz
        if iSize<=0:
            iSize=iBufSz
            if iSize==0:
                iSize=1
        
        if self.bReverse:
            self.iPos=iPos-iBufSz-1
            self.iSize=iSize#-iBufSz
        else:
            self.iPos=iPos
            self.iSize=iSize
        self.fPer=iBufSz/float(iSize)
        if self.iPos<0:
            self.iPos=0
        self.iThumb=-1
        if bUpdate:
            self.DoDraw()
    def DoDraw(self):
        size = self.GetClientSize()
        dc = wx.BufferedDC(wx.ClientDC(self), size)
        self._doDraw(dc, True)

class vtInOutFieldIdxLinesTransientPopup(wx.Dialog):
    def __init__(self, parent, size,style,name=''):
        self.SIZE_PN_SCROLL=wx.SystemSettings.GetMetric(wx.SYS_VSCROLL_X)
        wx.Dialog.__init__(self, parent,style=wx.RESIZE_BORDER,
                    name=parent.GetName()+'_Popup')#wx.BORDER_SIMPLE)#|wx.STAY_ON_TOP)
        self.fgsMain = wx.FlexGridSizer(cols=1, hgap=0, rows=4, vgap=4)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        id=wx.NewId()
        wx.EVT_SIZE(self,self.OnSize)
        #st = wx.StaticText(self, -1,name,pos=(70,4))
        self.cbCancel = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Cancel), name=u'cbCancel',
              parent=self, pos=(0,0), size=(30,30), style=wx.BU_AUTODRAW)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              self.cbCancel)
        bxs.AddWindow(self.cbCancel, 0, border=0, flag=wx.ALIGN_CENTER)
        
        self.cbApply = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Apply), name=u'cbApply',
              parent=self, pos=wx.Point(32, 00), size=wx.Size(30, 30),
              style=wx.BU_AUTODRAW)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              self.cbApply)
        bxs.AddWindow(self.cbApply, 0, border=0, flag=wx.ALIGN_CENTER)
        #bxs.AddWindow(st, 0, border=4, flag=wx.LEFT|wx.EXPAND)
        #self.fgsMain.AddSizer(bxs,1,border=0,flag=wx.EXPAND)
        
        self.cbReload = wx.lib.buttons.GenBitmapTextButton(ID=-1,
              bitmap=vtArt.getBitmap(vtArt.Synch), label=_(u'Reload'),
              name=u'cbReload', parent=self, 
              size=wx.Size(124, 30), style=0)
        self.cbReload.SetMinSize((-1,-1))
        self.cbReload.Bind(wx.EVT_BUTTON, self.OnCbReloadButton,
              self.cbReload)
        bxs.AddWindow(self.cbReload, 1, border=4, flag=wx.LEFT|wx.RIGHT|wx.EXPAND)
        
        #bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.chcRev = wx.CheckBox(id=-1,
              label=_(u'reverse'), name=u'chcRev', parent=self,
              pos=wx.DefaultPosition, size=wx.DefaultSize, style=0)
        self.chcRev.SetValue(True)
        bxs.AddWindow(self.chcRev, 1, border=4, flag=wx.LEFT|wx.RIGHT|wx.EXPAND)
        
        st = wx.StaticText(self, -1,_(u'buffer size'),style=wx.ALIGN_RIGHT)
        self.spLimit = wx.SpinCtrl(id=-1,size=wx.Size(60, 30),
              initial=20, max=1000, min=1, name=u'spLimit', parent=self,
              style=wx.SP_ARROW_KEYS|wx.ALIGN_RIGHT)
        self.spLimit.SetMinSize((-1,-1))
        self.spLimit.SetToolTipString(_(u'buffer size'))
        bxs.AddWindow(st, 1, border=4, flag=wx.LEFT|wx.EXPAND)
        bxs.AddWindow(self.spLimit, 1, border=4, flag=wx.LEFT|wx.RIGHT|wx.EXPAND)
        self.spLimit.SetValue(20)
        
        self.fgsMain.AddSizer(bxs,1,border=0,flag=wx.EXPAND)
        self.fgsMain.AddGrowableCol(0)
        #self.fgsMain.AddGrowableRow(1)
        
        self.SetSizer(self.fgsMain)
        if size[1]<22:
            size=(size[0],22)
        self.pn=None
    def OnSize(self,evt):
        iW,iH=self.GetSize()
        if iW<90:
            iW=90
        if iH<45:
            iH=45
        #if iH>self.szOrig[1]:
        #    iH=self.szOrig[1]
        #if iH<self.szOrig[1]:
        #    iH=self.szOrig[1]
        #iH=self.szOrig[1]
        self.SetSize((iW,iH))
        self.Layout()
        return
        if self.pn is not None:
            sz=self.pn.GetSize()
            self.pn.SetSize((iW-8,sz[1]))
            #self.pn.SetSize((iW-8,iH))
            iW-=self.SIZE_PN_SCROLL
        iW-=32
        if self.pn is None:
            for bmp,txt in self.dTxt.values():
                sz=txt.GetSize()
                txt.SetSize((iW,sz[1]))
        else:
            doc=self.GetParent().__getDoc__()
            if doc is not None:
                langs=doc.GetLanguages()
            
                bmp,txt=self.dTxt[langs[0][1]]
                sz=txt.GetSize()
                txt.SetSize((iW,sz[1]))
        evt.Skip()
    def OnCbReloadButton(self,evt):
        evt.Skip()
        par=self.GetParent()
        try:
            if par.IsRunning():
                return
            par.Index(bForce=False)
        except:
            pass
    def OnCbCancelButton(self,evt):
        self.Show(False)
    def OnCbApplyButton(self,evt):
        self.Apply()
        self.Show(False)
    def Show(self,flag):
        try:
            if flag:
                par=self.GetParent()
                self.spLimit.SetValue(par.GetBufSize())
                self.chcRev.SetValue(par.IsReverse())
                pass
            else:
                par=self.GetParent()
                par.cbPopup.SetValue(False)
        except:
            pass
            #vtLog.vtLngTB(self.GetParent().GetName())
        wx.Dialog.Show(self,flag)
    def Apply(self):
        try:
            par=self.GetParent()
            par.SetBufSize(self.spLimit.GetValue(),bUpdate=False)
            par.SetReverse(self.chcRev.GetValue(),bUpdate=False)
            #par.Index(bForce=True)
            par.Index(bForce=False)
            #par._scrollFull()
        except:
            import traceback
            traceback.print_exc()
            pass
class vtInOutFileIdxLinesWid(wx.Panel,vtInOutFileIdxLines,vtThreadMixinWX):
    def __init__(self, parent, id=-1, pos=wx.DefaultPosition, size=wx.DefaultSize, 
                style=wx.SB_HORIZONTAL, name='vtInOutFileIdxLinesWid',
                bufferSize=20,widPost=None,bReverse=False,
                size_button=wx.Size(31,30)):
        wx.Panel.__init__(self,parent,id=id,pos=pos,size=size,style=style,name=name)
        self.SetMinSize(wx.Size(-1, -1))
        self.bxsMain = wx.BoxSizer(orient=wx.VERTICAL)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        id=wx.NewId()
        self.cbLeft = wx.lib.buttons.GenBitmapButton(ID=id,
              bitmap=vtArt.getBitmap(vtArt.Left), name=u'cbLeft',
              parent=self, pos=wx.DefaultPosition, size=size_button, style=0)
        bxs.AddWindow(self.cbLeft, 0, border=0, flag=wx.ALIGN_CENTER)
        self.cbLeft.Bind(wx.EVT_BUTTON,self.OnLeft,self.cbLeft)
        
        bxsPos = wx.BoxSizer(orient=wx.VERTICAL)
        id=wx.NewId()
        self.indPos=vtInOutBufIndicator(self,id,bReverse=bReverse,
            space=5)
        bxsPos.AddWindow(self.indPos, 1, border=8, flag=wx.LEFT|wx.RIGHT|wx.EXPAND)
        id=wx.NewId()
        self.slPos = wx.Slider(self, id, 0,0, 1000, style=wx.SL_HORIZONTAL, 
                size=(-1,size_button[1]/3*2))
        #self.Bind(wx.EVT_COMMAND_SCROLL, self.OnScroll, self.slPos)
        self.Bind(wx.EVT_COMMAND_SCROLL_THUMBTRACK, self.OnScrollTrack, self.slPos)
        self.Bind(wx.EVT_COMMAND_SCROLL_THUMBRELEASE, self.OnScrollTrackRelease, self.slPos)
        self.Bind(wx.EVT_COMMAND_SCROLL_PAGEUP, self.OnScrollPgUp, self.slPos)
        self.Bind(wx.EVT_COMMAND_SCROLL_PAGEDOWN, self.OnScrollPgDn, self.slPos)
        bxsPos.AddWindow(self.slPos, 2, border=0, flag=wx.EXPAND)
        bxs.AddSizer(bxsPos,1,border=4,flag=wx.LEFT|wx.RIGHT|wx.EXPAND)
        
        id=wx.NewId()
        self.cbRight = wx.lib.buttons.GenBitmapButton(ID=id,
              bitmap=vtArt.getBitmap(vtArt.Right), name=u'cbRight',
              parent=self, pos=wx.DefaultPosition, size=size_button, style=0)
        bxs.AddWindow(self.cbRight, 0, border=0, flag=wx.ALIGN_CENTER)
        self.cbRight.Bind(wx.EVT_BUTTON,self.OnRight,self.cbRight)
        
        id=wx.NewId()
        self.cbPopup = wx.lib.buttons.GenBitmapToggleButton(ID=id,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbPopup',
              parent=self, pos=wx.DefaultPosition, size=size_button, style=wx.BU_AUTODRAW)
        bxs.AddWindow(self.cbPopup, 0, border=4, flag=wx.LEFT|wx.ALIGN_CENTER)
        self.cbPopup.SetBitmapLabel(vtArt.getBitmap(vtArt.Down))
        self.cbPopup.SetBitmapSelected(vtArt.getBitmap(vtArt.Down))
        self.cbPopup.Bind(wx.EVT_BUTTON,self.OnPopupButton,self.cbPopup)#id=id)
        self.popWin=None
        
        self.bxsMain.AddSizer(bxs,1,border=0,flag=wx.EXPAND)
        self.SetSizer(self.bxsMain)
        vtInOutFileIdxLines.__init__(self,bufferSize=bufferSize,par=widPost,bPost=widPost is not None)
        vtThreadMixinWX.__init__(self)
        
        self.bReverse=bReverse
        #self.Bind(wx.EVT_SCROLL, self.OnScroll)
        self.iActScPos=0
    def SetReverse(self,bReverse,bUpdate=False):
        if self.bReverse!=bReverse:
            self.bReverse=bReverse
            if bUpdate:
                self.Index(bForce=True)
                self._scrollFull()
            self.indPos.SetReverse(self.bReverse)
    def IsReverse(self):
        return self.bReverse
    def GetPos(self):
        return self.slPos.GetValue()
    def OnLeft(self,evt):
        iPos=self.slPos.GetValue()
        if iPos>0:
            self.slPos.SetValue(iPos-1)
            self._scroll()
    def OnRight(self,evt):
        iPos=self.slPos.GetValue()
        if iPos<self.GetCount():
            self.slPos.SetValue(iPos+1)
            self._scroll()
    def SetBufSize(self,bufferSize,bUpdate=False):
        vtInOutFileIdxLines.SetBufSize(self,bufferSize)
        if bUpdate:
            self._scrollFull()
    def _Index(self,bForce=False):
        #return
        vtInOutFileIdxLines._Index(self,bForce=bForce)
    def _IndexFinalize(self):
        iCount=self.GetCount()    #vtInOutFileIdxLines.GetCount(self)
        #iCount-=1
        iBufSz=self.GetBufSize()
        #print 'iCount',iCount,iBufSz,self.GetBufSize()
        if self.bReverse:
            #iCount-=1
            iPos=iCount#-1
            iStart=iBufSz#-1
            iEnd=iCount#-1
            if iStart>iEnd:
                iStart=iEnd
            i=iPos-iBufSz
            #print iStart,iEnd,iPos,i
            if i<0:
                i=0
            vtInOutFileIdxLines.SetStart(self,i)
            vtInOutFileIdxLines.SetEnd(self,iPos)
            wx.PostEvent(self,vtInOutFileIdxLinesAdd(self,None,False,idx=i,bSel=False))
            for s in self:
                wx.PostEvent(self,vtInOutFileIdxLinesAdd(self,s,False,idx=i,bSel=False))
                i+=1
            wx.PostEvent(self,vtInOutFileIdxLinesAdd(self,None,False,idx=i,bSel=True))
            #iPos-=iCount%iBufSz
            self.iActScPos=iPos
        else:
            iPos=0
            i=0
            vtInOutFileIdxLines.SetStart(self,iPos)
            vtInOutFileIdxLines.SetEnd(self,self.bufferSize)
            wx.PostEvent(self,vtInOutFileIdxLinesAdd(self,None,False,idx=i,bSel=False))
            for s in self:
                wx.PostEvent(self,vtInOutFileIdxLinesAdd(self,s,True,idx=i,bSel=False))
                i+=1
            wx.PostEvent(self,vtInOutFileIdxLinesAdd(self,None,True,idx=i,bSel=True))
            self.iActScPos=0
            iStart=0
            iEnd=iCount-iBufSz
            if iEnd<0:
                iEnd=0
        #wx.MutexGuiEnter()
        try:
            #print '+++++'
            #print vtInOutFileIdxLines.GetCount(self),iCount,iBufSz,iPos,iStart,iEnd,self.bReverse
            #print iCount%iBufSz
            #print '_IndexFinalize',iStart,iEnd,iPos,iCount,iBufSz
            self.CallBack(self.__setWidValues__,iStart,iEnd,iPos,
                    iCount-iBufSz,iBufSz)
            #self.slPos.SetRange(iStart,iEnd)
            #self.slPos.SetValue(iPos)
            #self.indPos.Set(iPos,iCount-self.GetBufSize(),self.GetBufSize())
            #wx.ScrollBar.SetScrollbar(self,iPos,iBufSz,iCount,iBufSz,True)
        except:
            pass
        #wx.MutexGuiLeave()
        self.iActScPos=iPos
    def __setWidValues__(self,iStart,iEnd,iPos,iCount,iSize):
        #print '__setWidValues__',iStart,iEnd,iPos,iCount,iSize
        iCount=max(0,iCount)
        #print '__setWidValues__',iStart,iEnd,iPos,iCount,iSize
        self.slPos.SetRange(iStart,iEnd)
        self.slPos.SetValue(iPos)
        self.indPos.Set(iPos,iCount,iSize)
    def OnScroll(self,evt):
        #print 'scoll'
        self._scroll()
    def _scroll(self):
    #    self.CallBack(self.__scroll)
    #def __scroll(self):
        #print 
        iPos = self.slPos.GetValue()
        self.indPos.Set(iPos,self.GetCount()-self.GetBufSize(),self.GetBufSize())
        #return
        #iPos=evt.GetPosition()
        #print 'pos',iPos,self.iActScPos
        if self.bReverse:
            if iPos<self.iActScPos:
                j=self.iActScPos
                
                iDelta=self.iActScPos-iPos
                
                #print iDelta
                #iPos+=self.GetThumbSize()
                #print '   ',self.slPos.GetPageSize(),self.slPos.GetLineSize()
                iAnf=iPos-self.GetBufSize()
                iEnd=iAnf+iDelta
                #print '  left 1',iPos,iAnf,iEnd,iDelta
                if iAnf<0:
                    iAnf=0
                    #self.slPos.SetValue(self.iActScPos)
                    #return
                if iEnd<0:
                    iEnd=0
                #print '  left 2',iPos,iAnf,iEnd,iDelta
                for i in xrange(iEnd,iAnf,-1):
                    s=self.GetData(i-1)
                    wx.PostEvent(self,vtInOutFileIdxLinesAdd(self,s,True,idx=i-1))
            elif iPos>self.iActScPos:
                j=self.iActScPos
                iDelta=iPos-self.iActScPos
                iAnf=self.iActScPos#+1#iPos#+self.GetThumbSize()#self.bufferSize
                iEnd=iAnf+iDelta
                #print '  right 1',iPos,iAnf,iEnd,iDelta
                #print iAnf,iEnd
                if iAnf>self.iCount:
                    iAnf=self.iCount
                if iEnd>(self.iCount+1):
                    iEnd=self.iCount+1
                    iDelta=iEnd-iAnf
                #print '  right 2',iPos,iAnf,iEnd,iDelta
                #for i in xrange(iAnf,iEnd):
                for i in xrange(iDelta):
                    s=self.GetData(iAnf+i)
                    wx.PostEvent(self,vtInOutFileIdxLinesAdd(self,s,False,idx=i+iAnf))
        else:
            if self.iActScPos<iPos:
                iDelta=iPos-self.iActScPos
                iAnf=self.iActScPos+self.GetBufSize()
                iEnd=iAnf+iDelta
                iCount=self.GetCount()
                #print '  right 1',iPos,iAnf,iEnd,iDelta
                #print iDelta
                #if iAnf>self.iCount:
                #    iAnf=self.iCount
                if iEnd>self.iCount:
                    iEnd=self.iCount
                iDelta=iEnd-iAnf
                j=self.iActScPos
                for i in xrange(iAnf,iEnd):
                    s=self.GetData(i)
                    wx.PostEvent(self,vtInOutFileIdxLinesAdd(self,s,True,idx=i))
            elif self.iActScPos>iPos:
                j=self.iActScPos
                iDelta=self.iActScPos-iPos
                for i in xrange(iDelta):
                    idx=j-i-1
                    s=self.GetData(idx)
                    wx.PostEvent(self,vtInOutFileIdxLinesAdd(self,s,False,idx=idx))
        self.iActScPos=iPos
    def OnScrollTrackRelease(self,evt):
        self._scrollFull(bSel=True)
    def OnScrollPgUp(self,evt):
        wx.PostEvent(self,vtInOutFileIdxLinesAdd(self,None,False,idx=0,bSel=False))
        self._scrollFull(bSel=True)
    def OnScrollPgDn(self,evt):
        wx.PostEvent(self,vtInOutFileIdxLinesAdd(self,None,False,idx=0,bSel=False))
        self._scrollFull(bSel=True)
    def _scrollFull(self,bSel=False):
    #    self.CallBack(self.__scrollFull)
    #def __scrollFull(self):
        iPos = self.slPos.GetValue()
        #print '_scrollFull',iPos,self.GetCount(),self.GetBufSize(),bSel
        self.indPos.Set(iPos,self.GetCount()-self.GetBufSize(),self.GetBufSize())
        wx.PostEvent(self,vtInOutFileIdxLinesAdd(self,None,False,idx=0,bSel=False))
        if self.bReverse:
            iEnd=iPos-1
            iAnf=iEnd-self.GetBufferSize()
            #print '   ',iPos,iAnf,iEnd
            if iAnf<-1:
                iAnf=-1
            for i in xrange(iEnd,iAnf,-1):
                s=self.GetData(i)
                wx.PostEvent(self,vtInOutFileIdxLinesAdd(self,s,True,idx=i,bSel=bSel))
                bSel=False
            #iPos+=1
        else:
            #iPos+=1
            iEnd=iPos+self.GetBufferSize()
            iCount=self.GetCount()
            if iEnd>iCount:
                iEnd=iCount
            for i in xrange(iPos,iEnd):
                s=self.GetData(i)
                wx.PostEvent(self,vtInOutFileIdxLinesAdd(self,s,True,idx=i,bSel=bSel))
                bSel=False
        self.iActScPos=iPos
    def OnScrollTrack(self,evt):
        #print 'track'
        iPos = self.slPos.GetValue()
        #print iPos
        #if self.bReverse:
        #    iPos-=1
        #    if iPos<0:
        #        iPos=0
        #print 'OnScrollTrack',iPos,self.GetCount(),self.GetBufSize()
        self.indPos.Set(iPos,self.GetCount()-self.GetBufSize(),self.GetBufSize())
        wx.PostEvent(self,vtInOutFileIdxLinesAdd(self,None,False,idx=0,bSel=False))
        if self.bReverse:
            s=self.GetData(iPos-1)
            wx.PostEvent(self,vtInOutFileIdxLinesAdd(self,s,False,idx=iPos-1))
        else:
            s=self.GetData(iPos)
            wx.PostEvent(self,vtInOutFileIdxLinesAdd(self,s,False,idx=iPos))
        #s=self.GetData(iPos)
        #wx.PostEvent(self,vtInOutFileIdxLinesAdd(self,s,False,idx=iPos))
        self.iActScPos=iPos
    def __createPopup__(self):
        if self.popWin is None:
            sz=self.GetSize()
            sz=(sz[0],sz[1]-20)
            self.popWin=vtInOutFieldIdxLinesTransientPopup(self,sz,wx.SIMPLE_BORDER)
            #self.popWin.SetNode()
            #self.popWin.SetVal(self.txtVal.GetValue(),self.doc.GetLang())
        else:
            pass
    def OnPopupButton(self,evt):
        self.__createPopup__()
        if self.cbPopup.GetValue()==True:
            btn=self.cbPopup
            iX,iY = btn.ClientToScreen( (0,0) )
            iW,iH =  btn.GetSize()
            iPopW,iPopH=self.popWin.GetSize()
            iScreenW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            iScreenH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            if iX+iPopW>iScreenW:
                iX=iScreenW-iPopW-4
                if iX<0:
                    iX=0
            iY+=iH
            if iY+iPopH>iScreenH:
                iY=iScreenH-iPopH
                if iY<0:
                    iY=0
            
            self.popWin.Move((iX,iY))
            self.popWin.Show(True)
        else:
            self.popWin.Show(False)
        evt.Skip()
    def Close(self):
        vtInOutFileIdxLines.Close(self)
