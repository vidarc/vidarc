#----------------------------------------------------------------------------
# Name:         mailBox.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: mailBox.py,v 1.7 2016/02/03 19:53:46 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import email
import email.Errors
import email.Parser
import email.Utils
import email.Header 
import mailbox
import mimetypes
import time,os,os.path,string,stat,codecs
import vidarc.tool.InOut.fnUtil as fnUtil
import vidarc.tool.log.vtLog as vtLog

def msgfactory(fp):
    try:
        return email.message_from_file(fp)
    except email.Errors.MessageParseError:
        # Don't return None since that will
        # stop the mailbox iterator
        return ''

def processMsg(msg,f,dn,sTitle,func=None):
    lst=msg.get_payload()
    if type(lst)==type(''):
        f.write(lst)
        pass
    else:
        for part in lst:
            if type(part)!=type(''):
                maintype =  part.get_content_maintype()
                filename = part.get_filename()
                if maintype=='multipart' or maintype=='message':
                    processMsg(part,f,dn,sTitle,func)
                else:
                    if filename is not None:
                        if filename.startswith('=?'):
                            if filename.endswith('?='):
                                try:
                                    sFN=email.Header.decode_header(filename)[0][0]
                                    filename=sFN
                                except:
                                    pass
                        filename=fnUtil.replaceSuspectChars(filename)
                        try:
                            os.makedirs(dn+os.sep+'attach')
                        except:
                            pass
                        sAttachFN=dn+os.sep+'attach'+os.sep+filename
                        afp = open (sAttachFN, "wb")
                        afp.write(part.get_payload(decode=1))
                        if func is not None:
                            func(sTitle+u'_'+filename,sAttachFN)
                        afp.close()
                    else:
                        f.write(part.get_payload(decode=1))
def processMsgPlain(msg,f,fn,dn,sTitle,func=None,iAttach=0,iLv=0,bDbg=0):
    sLv=''.join(['  ']*iLv)
    lst=msg.get_payload()
    if type(lst)==type(''):
        f.write(lst)
    else:
        for part in lst:
            if type(part)!=type(''):
                maintype =  part.get_content_maintype()
                filename = part.get_filename()
                if bDbg>0:
                    try:
                        vtLog.vtLngCur(vtLog.DEBUG,'sTitle:%s;type:%r;main:%r;fn:%r'%(
                                sTitle,type(part),maintype,filename),'msgPlain')
                    except:
                        print type(part),maintype,filename
                if filename is None:
                    if maintype in ['application',]:
                        contype =  part.get_content_type()
                        subtype =  part.get_content_subtype()
                        filename = part.get_param('name')
                        if bDbg>10:
                            vtLog.vtLngCur(vtLog.DEBUG,'sTitle:%s;type:%r;con:%r;main:%r;sub:%r;fn:%r'%(
                                    sTitle,type(part),contype,maintype,subtype,filename),'msgPlain')
                if filename is not None:
                    if filename.startswith('=?'):
                        if filename.endswith('?='):
                            try:
                                sFN=email.Header.decode_header(filename)[0][0]
                                if bDbg>10:
                                    vtLog.vtLngCur(vtLog.DEBUG,'sTitle:%s;sFN:%r,%r;filename:%r'%(
                                            sTitle,sFN,type(sFN),filename),'msgPlain')
                                filename=sFN
                            except:
                                pass

                if maintype in ['multipart','message']:
                    processMsgPlain(part,f,fn,dn,sTitle,func,iAttach,iLv+1)
                elif maintype in ['application',]:
                    if bDbg>10:
                        vtLog.vtLngCur(vtLog.DEBUG,repr(part),'exp')
                        vtLog.vtLngCur(vtLog.DEBUG,repr(part.__dict__),'exp')
                    if filename is not None:
                        try:
                            sAttachFN=''.join([fn,'_%d_'%iAttach,filename])
                        except:
                            sFNascii=filename.decode('ascii','ignore')
                            sAttachFN=''.join([fn,'_%d_'%iAttach,sFNascii])
                    else:
                        sAttachFN=''.join([fn,'_%d_'%iAttach])
                    iAttach+=1
                    try:
                        afp = open (sAttachFN, "wb")
                        afp.write(part.get_payload(decode=1))
                        if func is not None:
                            func(sTitle+u'_'+filename,sAttachFN)
                        afp.close()
                    except:
                        pass
                else:
                    if filename is not None:
                        filename=fnUtil.replaceSuspectChars(filename)
                        
                        sAttachFN=''.join([fn,'_%d_'%iAttach,filename])
                        iAttach+=1
                        afp = open (sAttachFN, "wb")
                        afp.write(part.get_payload(decode=1))
                        if func is not None:
                            func(sTitle+u'_'+filename,sAttachFN)
                        afp.close()
                    else:
                        #print part
                        f.write(part.get_payload(decode=1))
def processMsgEml(msg,f):
    f.write(msg.as_string())
def processMsgEml2(msg,f):
    lst=msg.get_payload()
    if type(lst)==type(''):
        f.write(msg.as_string())
        pass
    else:
        for part in lst:
            if type(part)!=type(''):
                maintype =  part.get_content_maintype()
                filename = part.get_filename()
                if filename is not None:
                    f.write(part.as_string())
                else:
                    if maintype=='multipart' or maintype=='message':
                        processMsgEml(part,f)
                    else:
                        f.write(part.as_string())
def genUniqueDN(mdn,bDoNotOverride=True):
    bCreated=False
    i=0
    while bCreated==False:
        try:
            if i==0:
                os.makedirs(mdn)
            else:
                s='.%d'%i
                os.makedirs(''.join([mdn,s]))
            bCreated=True
        except:
            if bDoNotOverride==False:
                bCreated=True
            else:
                i+=1
    if i>0:
        mdn=''.join([mdn,s])
    return mdn
def genUniqueFN(mfn,ext,bDoNotOverride=True):
    bCreated=False
    i=0
    s=''
    while bCreated==False:
        try:
            if i>0:
                s='.%d'%i
            os.stat(mfn+s+ext)
            i+=1
            if bDoNotOverride==False:
                bCreated=True
        except:
            bCreated=True
            pass
    mfn=''.join([mfn,s,ext])
    return mfn
def export(fn,dn,prefix='',maxFrom=40,maxSubject=80,
            bDoNotOverride=True,gProc=None,func=None,funcProc=None):
    mode=os.stat(fn)
    iSize=mode.st_size/(1024)
    if gProc is not None:
        gProc.SetValue(0)
        gProc.SetRange(iSize)
    if funcProc is not None:
        funcProc(0,iSize)
    fp=open(fn,'rb')
    #mbox = mailbox.PortableUnixMailbox(fp, email.message_from_file)
    mbox = mailbox.UnixMailbox(fp, email.message_from_file)
    msg= mbox.next()
    iErr=0
    #msg= mbox.next()
    while msg is not None:
        #print dir(msg)
        #print msg.keys()
        #print msg.items()
        #print 'as'
        #print msg.as_string()[:1000]
        
        #print msg['X-Mozilla-Status2']#.keys()
        #print msg.__dict__.keys()
        try:
            #print msg['From'],msg['To'],msg['Date'],msg['Subject']
            dt=email.Utils.parsedate(msg['Date'])
            #print dt
            sDt=time.strftime('%Y%m%d_%H%M%S',dt)
            strs=[s.capitalize for s in msg['Subject'].split()]
            sSub=''.join(strs)[:maxSubject]
                
            #strs=map(string.capitalize,string.split(string.replace(msg['From'],'"',''))[:-1])
            strs=[s.capitalize() for s in msg['From'].replace('"','').split()[:-1]]
            sFrom=''.join(strs)[:maxFrom]
            sTitle=fnUtil.replaceSuspectChars(''.join([prefix,sDt,'_',sFrom,'_',sSub]))
            mdn=''.join([dn,os.sep,sTitle])
            mdn=genUniqueDN(mdn,bDoNotOverride=bDoNotOverride)
            mf=open(mdn+os.sep+'message.txt','wb')
            if func is not None:
                func(sTitle,mdn+os.sep+'message.txt')
            processMsg(msg,mf,mdn,sTitle,func)
            mf.close()
        except:
            sTitle='NoneStandard%03d'%iErr
            mdn=dn+os.sep+sTitle
            mdn=genUniqueDN(mdn,bDoNotOverride=bDoNotOverride)
            mf=open(mdn+os.sep+'message.txt','wb')
            if func is not None:
                func(sTitle,mdn+os.sep+'message.txt')
            mf.write(msg.as_string())
            mf.close()
            iErr+=1
            vtLog.vtLngTB(__file__)
        iPos=fp.tell()/1024
        if gProc is not None:
            gProc.SetValue(iPos)
        if funcProc is not None:
            funcProc(iPos,iSize)
        
        msg= mbox.next()
    #o=email.Parser.Parser().parsestr(str)
    #print o
    fp.close()
def exportPlain(fn,dn,prefix='',maxFrom=40,maxSubject=80,
            bDoNotOverride=True,gProc=None,func=None,funcProc=None,
            bDbg=0):
    mode=os.stat(fn)
    iSize=mode.st_size/(1024)
    if gProc is not None:
        gProc.SetValue(0)
        gProc.SetRange(iSize)
    if funcProc is not None:
        funcProc(0,iSize)
    fp=open(fn,'rb')
    #mbox = mailbox.PortableUnixMailbox(fp, email.message_from_file)
    mbox = mailbox.UnixMailbox(fp, email.message_from_file)
    msg= mbox.next()
    iErr=0
    #msg= mbox.next()
    try:
        os.makedirs(dn)
    except:
        pass
    while msg is not None:
        #print dir(msg)
        #print msg.keys()
        #print msg.items()
        #print 'as'
        #print msg.as_string()[:1000]
        
        #print msg['X-Mozilla-Status2']#.keys()
        #print msg.__dict__.keys()
        def expMsg(msg,dn,bDoNotOverride):
            dt=email.Utils.parsedate(msg['Date'])
            sDt=time.strftime('%Y%m%d_%H%M%S',dt)
            strs=[s.capitalize() for s in msg['Subject'].split()]
            sSub=''.join(strs)[:maxSubject]
                
            strs=[s.capitalize() for s in msg['From'].replace('"','').split()[:-1]]
            sFrom=''.join(strs)[:maxFrom]
            sTitle=fnUtil.replaceSuspectChars(''.join([prefix,sDt,'_',sFrom,'_',sSub]))
            mfn=''.join([dn,os.sep,sTitle])
            mfn=genUniqueFN(mfn,'.txt',bDoNotOverride=bDoNotOverride)
            mf=open(mfn,'wb')
            if func is not None:
                func(sTitle,mfn)
            processMsgPlain(msg,mf,mfn,dn,sTitle,func,bDbg=bDbg)
            mf.close()
        try:
            expMsg(msg,dn,bDoNotOverride)
        except:
            vtLog.vtLngTB(__file__)
            bSkip=False
            try:
                s=msg.as_string()
                i=s.find('>From -')
                #i=s.find('Received:')
                j=-1
                if i>0:
                    #i=s.find('X-Mozilla-Status: 0001',i)
                    j=s.find('\n',i)
                    if j>0:
                        i=j+1
                    else:
                        i=-1
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'%d;%d;%s'%(i,j,s),__file__)
                if i>0:
                    p=email.Parser.FeedParser()
                    p.feed(s[i:])
                    msg1=p.close()
                    expMsg(msg1,dn,bDoNotOverride)
                    bSkip=True
            except:
                vtLog.vtLngTB(__file__)
            if bSkip==False:
                sTitle='NoneStandard%03d'%iErr
                mdn=dn+os.sep+sTitle
                mdn=genUniqueDN(mdn,bDoNotOverride=bDoNotOverride)
                mf=open(mdn+os.sep+'message.txt','wb')
                if func is not None:
                    func(sTitle,mdn+os.sep+'message.txt')
                mf.write(msg.as_string())
                mf.close()
                iErr+=1
                #vtLog.vtLngTB(__file__)
        iPos=fp.tell()/1024
        if gProc is not None:
            gProc.SetValue(iPos)
        if funcProc is not None:
            funcProc(iPos,iSize)
        
        msg= mbox.next()
    #o=email.Parser.Parser().parsestr(str)
    #print o
    fp.close()

def exportEml(fn,dn,prefix='',maxFrom=40,maxSubject=80,
            bDoNotOverride=True,gProc=None,func=None,funcProc=None):
    mode=os.stat(fn)
    iSize=mode.st_size/(1024)
    if gProc is not None:
        gProc.SetValue(0)
        gProc.SetRange(iSize)
    if funcProc is not None:
        funcProc(0,iSize)
    fp=open(fn,'rb')                        # 20140926 wro:use binary mode
    mbox = mailbox.UnixMailbox(fp, email.message_from_file)
    msg= mbox.next()
    iErr=0
    try:
        os.makedirs(dn)
    except:
        pass
    while msg is not None:
        #print msg['From'],msg['To'],msg['Date'],msg['Subject']
        def expMsg(msg,dn,bDoNotOverride):
            dt=email.Utils.parsedate(msg['Date'])
            sDt=time.strftime('%Y%m%d_%H%M%S',dt)
            strs=[s.capitalize() for s in msg['Subject'].split()]
            sSub=''.join(strs)[:maxSubject]
            
            #strs=map(string.capitalize,string.split(string.replace(msg['From'],'"',''))[:-1])
            strs=[s.capitalize() for s in msg['From'].replace('"','').split()[:-1]]
            sFrom=''.join(strs)[:maxFrom]
            sTitle=fnUtil.replaceSuspectChars(''.join([prefix,sDt,'_',sFrom,'_',sSub]))
            mfn=''.join([dn,os.sep,sTitle])
            mfn=genUniqueFN(mfn,'.eml',bDoNotOverride=bDoNotOverride)
            mf=open(mfn,'wb')               # 20140926 wro:use binary mode
            processMsgEml(msg,mf)
            mf.close()
            if func is not None:
                func(sTitle,mfn)
        try:
            expMsg(msg,dn,bDoNotOverride)
        except:
            vtLog.vtLngTB(__file__)
            bSkip=False
            try:
                s=msg.as_string()
                i=s.find('>From -')
                #i=s.find('Received:')
                j=-1
                if i>0:
                    #i=s.find('X-Mozilla-Status: 0001',i)
                    j=s.find('\n',i)
                    if j>0:
                        i=j+1
                    else:
                        i=-1
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'%d;%d;%s'%(i,j,s),__file__)
                if i>0:
                    p=email.Parser.FeedParser()
                    p.feed(s[i:])
                    msg1=p.close()
                    expMsg(msg1,dn,bDoNotOverride)
                    bSkip=True
            except:
                vtLog.vtLngTB(__file__)
            if bSkip==False:
                sTitle='NoneStandard%03d'%iErr
                mdn=dn+os.sep+sTitle
                mdn=genUniqueDN(mdn,bDoNotOverride=bDoNotOverride)
                mf=open(mdn+os.sep+'message.txt','wb')
                mf.write(msg.as_string())
                mf.close()
                if func is not None:
                    func(sTitle,mdn+os.sep+'message.txt')
                iErr+=1
                #vtLog.vtLngTB(__file__)
        iPos=fp.tell()/1024
        if gProc is not None:
            gProc.SetValue(iPos)
        if funcProc is not None:
            funcProc(iPos,iSize)
        msg= mbox.next()
    #o=email.Parser.Parser().parsestr(str)
    #print o
    fp.close()

if __name__=='__main__':
    export('bioS30l','.')
