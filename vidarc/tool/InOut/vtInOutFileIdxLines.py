#----------------------------------------------------------------------------
# Name:         vtInOutFileIdxLines.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20061122
# CVS-ID:       $Id: vtInOutFileIdxLines.py,v 1.7 2009/05/07 19:08:48 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.vtThread import vtThread
import os,stat,codecs

class vtInOutFileIdxLines(vtThread):
    def __init__(self,bufferSize=1000,par=None,bPost=False):
        vtThread.__init__(self,par=par,bPost=bPost)
        self.fIn=None
        self.bufferSize=bufferSize
        self._clr()
    def SetBufSize(self,bufferSize):
        self.bufferSize=bufferSize
    def GetBufferSize(self):
        return self.bufferSize
    def _clr(self):
        self.idx=0
        self.idxEnd=-1
        self.pos=-1
        self.iCount=1
        self.lIdxPos=[]
        self.lIdxSz=[]
    def Open(self,fn):
        try:
            if self.Close()==False:
                return -2
            self.fIn=file(fn,'rb')
            #self.fIn=codecs.open(fn,'rb',encoding='UTF-8')
            self._clr()
            return 0
        except:
            return -1
    def Close(self):
        if self.fIn is not None:
            if self.IsRunning():
                return False
            self.fIn.close()
            self.fIn=None
            self._clr()
            return True
        return True
    def IsClosed(self):
        return self.fIn is None
    def Index(self,bForce=False):
        if self.IsRunning():
            return
        if self.fIn is None:
            return
        s=os.fstat(self.fIn.fileno())
        self.iSize=s[stat.ST_SIZE]
        self.Do(self._Index,bForce=bForce)
        self.Do(self._IndexFinalize)
    def _notify(self,pos,size):
        pass
    def _analyse(self,line):
        pass
    def _lineValid(self,line):
        return line[-1]=='\n'
    def _Index(self,bForce=False):
        if bForce:
            self._clr()
            self.fIn.seek(0)
        try:
            i=0
            iPosOld=-1
            iPosValid=-1
            #print 'pos',self.pos
            if self.pos>=0:
                self.fIn.seek(self.pos)
                i=self.pos
                bAddSz=False
                if self.lIdxSz[-1]==0:
                    del self.lIdxPos[-1]
                    del self.lIdxSz[-1]
            else:
                bAddSz=True
            for line in self.fIn:
                if self.Is2Stop():
                    break
                if self._lineValid(line)==False:
                    iPosValid=self.fIn.tell()
                    #print iPosValid,
                    iPosValid-=len(line)
                    #print iPosValid
                    continue
                else:
                    iPosValid=-1
                #print 'i',i,len(self.lIdxPos),self.lIdxPos
                self.lIdxPos.append(i)
                sz=len(line)
                if ord(line[-2])==13:
                    # CR/LF fix
                    i+=2
                    sz-=2
                else:
                    i+=1
                    sz-=1
                #print 's',sz,len(self.lIdxSz),self.lIdxSz
                self.lIdxSz.append(sz)
                i+=sz
                iPosNew=self.fIn.tell()
                self._analyse(line)
                if iPosOld!=iPosNew:
                    iPosOld=iPosNew
                    self._notify(iPosNew,self.iSize)
            #print len(self.lIdxPos),self.lIdxPos
            #print len(self.lIdxSz),self.lIdxSz
            if iPosValid<0:
                self.pos=self.fIn.tell()
                self.lIdxPos.append(i)
                self.lIdxSz.append(0)
            else:
                self.lIdxPos.append(iPosValid)
                self.lIdxSz.append(0)
                self.pos=iPosValid
            #if bAddSz:
            #    self.lIdxSz.append(0)
            #else:
            #    self.lIdxSz.append(0)
            self.iCount=len(self.lIdxPos)-1
        except:
            return -2
        return 0
    def _IndexFinalize(self):
        pass
    def GetCount(self):
        return self.iCount
    def GetBufSize(self):
        return self.bufferSize
    def Get(self,i):
        return self.lIdxPos[i]
    def GetLst(self):
        return self.lIdxPos
    def GetIdx(self):
        return self.idx
    def SetStart(self,i):
        self.idx=i-1
    def SetEnd(self,i):
        self.idxEnd=i
    def __iter__(self):
        self.fIn.seek(self.lIdxPos[self.idx])
        return self
    def next(self):
        self.idx+=1
        if self.idxEnd>0:
            if self.idx>=self.idxEnd:
                raise StopIteration
        if self.idx>=self.iCount:
            raise StopIteration 
        else:
            try:
                self._acquire()
                self.fIn.seek(self.lIdxPos[self.idx])
                return self.fIn.read(self.lIdxSz[self.idx])
            finally:
                self._release()
    def GetData(self,i):
        if self.fIn is None:
            return None
        try:
            self._acquire()
            self.fIn.seek(self.lIdxPos[i])
            return self.fIn.read(self.lIdxSz[i])
        finally:
            self._release()
    def __getitem__(self,i):
        return i
    def __getslice__(self,i,j):
        return self.lIdxPos[i:j]
