#----------------------------------------------------------------------------
# Name:         listFiles.py
# Purpose:      list files
# Author:       Walter Obweger
#
# Created:      20060407
# CVS-ID:       $Id: listFilesXml.py,v 1.7 2008/05/10 09:29:38 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#from listFiles import DirEntry
import vidarc.tool.InOut.listFiles as listFiles
import string
import os
import time
import calendar
from vidarc.tool.xml.vtXmlDom import vtXmlDom
import vidarc.tool.log.vtLog as vtLog
import zipfile

def ftime2xml(ft):
    "convert file time to predefined xml string as gm-time"
    t=time.gmtime(ft)
    return time.strftime("%Y%m%d_%H%M%S",t)
def xml2ftime(str):
    "convert prefined time string into time(seconds), time is stored as gm-time"
    try:
        s=(string.atoi(str[0:4]),string.atoi(str[4:6]),string.atoi(str[6:8]),
            string.atoi(str[9:11]),string.atoi(str[11:13]),string.atoi(str[13:15]),
            0,0,0)
        return calendar.timegm(s)
    except:
        return time.time()
    
class DirEntryXml(listFiles.DirEntry):
    def __init__(self,doc=None,node=None,bRead=False):
        listFiles.DirEntry.__init__(self)
        if doc is not None:
            self.doc=doc
        else:
            self.doc=vtXmlDom(appl=self.__class__.__name__,audit_trail=False)
        self.node=node
        if bRead and node is not None:
            for c in self.doc.getChilds(self.node):
                if self.doc.getTagName(c)=='DIR':
                        self._addDirEntry_(c,self)
    def ClearDoc(self):
        self.doc=None
    def getDirEntry(self,dEntry,sDN,lDN):
        #vtLog.vtLngCurCls(vtLog.DEBUG,'<%s>;<%s>;lDN:%s'%(dEntry.base,sDN,lDN),self)
        if cmp(dEntry.base,sDN)==0:
            #vtLog.vtLngCurCls(vtLog.DEBUG,'found;%s:%s'%(dEntry.base,sDN),self)
            return dEntry
        dirs=dEntry.dirs
        if len(lDN)>0:
            sDNpart=lDN[0]
            for d in dirs:
                i=d.base.rfind('/')
                if d.base[i+1:]==sDNpart:
                #if sDN.find(d.base):
                    #vtLog.vtLngCurCls(vtLog.DEBUG,'part match;%d;%s'%(i,d.base[i:]),self)
                    return self.getDirEntry(d,sDN,lDN[1:])
        #vtLog.vtLngCurCls(vtLog.DEBUG,'base:<%s>;new dEntry:<%s>;%d'%(dEntry.base,sDN,cmp(dEntry.base,sDN)),self)
        sDNnew=sDN[len(dEntry.base)+1:]
        sDNbase=dEntry.base
        l=sDNnew.split('/')
        #vtLog.vtLngCurCls(vtLog.DEBUG,'new parts:%s,l:%s;lDN:%s'%(sDNnew,l,lDN),self)
        dd=dEntry
        for sNew in l:
            d=listFiles.DirEntry()
            d.base='/'.join([dd.base,sNew])
            vtLog.vtLngCurCls(vtLog.DEBUG,d.base,self)
            dd.dirs.append(d)
            #print dEntry.dirs
            dd=d
        return dd
    def AddEntryByLst(self,sDN,lVal):
        fEntry=listFiles.FileEntry()
        for s in lVal:
            sTagName,sVal=s.split(':')
            if sTagName == 'NAME':
                fEntry.name=sVal
            if sTagName == 'SIZE':
                try:
                    fEntry.size=long(sVal)
                except:
                    fEntry.size=0
            if sTagName == 'LAST_ACCESS':
                fEntry.last_access=xml2ftime(sVal)
            if sTagName == 'MOD_ACCESS':
                fEntry.mod_access=xml2ftime(sVal)
            if sTagName == 'STAT_ACCESS':
                fEntry.stat_access=xml2ftime(sVal)
            if sTagName == 'MD5':
                fEntry.md5=sVal
        lDN=sDN.split('/')
        if lDN[0]=='.':
            del lDN[0]
        dEntry=self.getDirEntry(self,sDN,lDN)
        dEntry.files.append(fEntry)
        pass
    def unlink(self):
        if self.node is None:
            self.doc.unlink()
    def _addNode_(self,elem,dirEntry):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurCls(vtLog.DEBUG,'base:%s'%(dirEntry.base),self)
        elemDir=self.doc.createChildByLst(elem,'DIR',[
            #{'tag':'BASE','val':unicode(dirEntry.base,'iso-8859-1')},
            {'tag':'BASE','val':dirEntry.base},
            {'tag':'SIZE','val':"%d"%dirEntry.size},
            ])
        for f in dirEntry.files:
            if f.md5==None:
                sMD5=""
            else:
                sMD5=f.md5
            self.doc.createChildByLst(elemDir,'FILE',[
                #{'tag':'NAME','val':unicode(f.name,'iso-8859-1')},
                {'tag':'NAME','val':f.name},
                {'tag':'SIZE','val':"%d"%f.size},
                {'tag':'LAST_ACCESS','val':ftime2xml(f.last_access)},
                {'tag':'MOD_ACCESS','val':ftime2xml(f.mod_access)},
                {'tag':'STAT_ACCESS','val':ftime2xml(f.stat_access)},
                {'tag':'MD5','val':sMD5},
                ])
        for f in dirEntry.dirs:
            self._addNode_(elemDir,f)
    def refresh(self,fn):
        f,l,lDE=listFiles.DirEntry.refresh(self,fn)
        if f is not None:
            iSkip=len(self.base)+1
            if self.node is None:
                return
            n=self.doc.getChild(self.node,'DIR')
            if n is None:
                return
            #for s in l:
            iDir=len(lDE)
            for i in xrange(iDir):
                s='/'.join(l[:i+1])
                nF=None
                for c in self.doc.getChilds(n,'DIR'):
                    sN=self.doc.getNodeText(c,'BASE')
                    print sN[iSkip:],s
                    if sN[iSkip:]==s:
                        nF=c
                        break
                if nF is None:
                    dE=lDE[i]
                    nF=self.doc.createChildByLst(n,'DIR',[
                        #{'tag':'BASE','val':unicode(dirEntry.base,'iso-8859-1')},
                        {'tag':'BASE','val':dE.base},
                        {'tag':'SIZE','val':"%d"%dE.size},
                        ])
                n=nF
            sDN,sFN=os.path.split(fn)
            nF=None
            for c in self.doc.getChilds(n,'FILE'):
                sN=self.doc.getNodeText(c,'NAME')
                if sN==sFN:
                    nF=c
                    break
            if nF is None:
                if f.md5==None:
                    sMD5=""
                else:
                    sMD5=f.md5
                self.doc.createChildByLst(n,'FILE',[
                    #{'tag':'NAME','val':unicode(f.name,'iso-8859-1')},
                    {'tag':'NAME','val':f.name},
                    {'tag':'SIZE','val':"%d"%f.size},
                    {'tag':'LAST_ACCESS','val':ftime2xml(f.last_access)},
                    {'tag':'MOD_ACCESS','val':ftime2xml(f.mod_access)},
                    {'tag':'STAT_ACCESS','val':ftime2xml(f.stat_access)},
                    {'tag':'MD5','val':sMD5},
                    ])
            else:
                self.doc.setNodeText(nF,'SIZE',"%d"%f.size)
                self.doc.setNodeText(nF,'LAST_ACCESS',ftime2xml(f.last_access))
                self.doc.setNodeText(nF,'MOD_ACCESS',ftime2xml(f.mod_access))
                self.doc.setNodeText(nF,'STAT_ACCESS',ftime2xml(f.stat_access))
                self.doc.setNodeText(nF,'MD5',f.md5 or '')
    def process(self,dir,recursion=10,count=None,filter=None,skip=0,style=0):
        self.clear()
        listFiles.DirEntry.read(self,dir,recursion,count,filter,skip)
        if (style & 1) == 1:
            self.calcInfo()
        self.buildXml()
    def setDirEntry(self,dirEntry):
        self.base=dirEntry.base
        self.dirs=dirEntry.dirs
        self.files=dirEntry.files
        self.size=dirEntry.size
        self.buildXml()
    def buildXml(self,dirEntry=None):
        if self.node is None:
            self.doc.Close()
            self.doc.New(root='DirEntry')
            if dirEntry is None:
                dirEntry=self
            self._addNode_(self.doc.getRoot(),dirEntry)
            self.doc.AlignDoc()
        else:
            if dirEntry is None:
                dirEntry=self
            for c in self.doc.getChilds(self.node):
                self.doc.deleteNode(c,self.node)
            self._addNode_(self.node,dirEntry)
    def _getFileEntry_(self,elem):
        fEntry=listFiles.FileEntry()
        for c in self.doc.getChilds(elem):
            sTagName=self.doc.getTagName(c)
            if sTagName == 'NAME':
                fEntry.name=self.doc.getText(c)
            if sTagName == 'SIZE':
                try:
                    fEntry.size=long(self.doc.getText(c))
                except:
                    fEntry.size=0
            if sTagName == 'LAST_ACCESS':
                fEntry.last_access=xml2ftime(self.doc.getText(c))
            if sTagName == 'MOD_ACCESS':
                fEntry.mod_access=xml2ftime(self.doc.getText(c))
            if sTagName == 'STAT_ACCESS':
                fEntry.stat_access=xml2ftime(self.doc.getText(c))
            if sTagName == 'MD5':
                fEntry.md5=self.doc.getText(c)

        return fEntry
    def _addDirEntry_(self,elem,dirEntry):
        for c in self.doc.getChilds(elem):
            sTagName=self.doc.getTagName(c)
            if sTagName == 'BASE':
                dirEntry.base=self.doc.getText(c)
            if sTagName == 'SIZE':
                try:
                    dirEntry.size=long(self.doc.getText(c))
                except:
                    dirEntry.size=0
            if sTagName == 'FILE':
                dirEntry.files.append(self._getFileEntry_(c))
            if sTagName == 'DIR':
                dEntry=listFiles.DirEntry()
                dirEntry.dirs.append(dEntry)
                self._addDirEntry_(c,dEntry)
    def read(self,fn,mode=0):
        """reades information to an xml-file named <fn>.
fn      name for the output file
mode   definies write mode
   0 ... generate xml-file
   1 ... compress xml-file into a zipfile named <fn.zip> and remove plant xml-file.
        """
        self.doc.Close()
        
        #self.doc=self.dom.createDocument(None,"DirEntry",None)
        #self.topelem=self.doc.documentElement
        if mode==1:
            if fn[-3:]!='zip':
                z=zipfile.ZipFile(fn+'.zip')
            else:
                z=zipfile.ZipFile(fn)
            for tfn in z.namelist():
                lines=z.read(tfn)
                tf=open(tfn,'w')
                tf.write(lines)
                tf.close()
            z.close()
        self.doc.Open(fn)
        if mode==1:
            if fn[-3:]!='zip':
                z=zipfile.ZipFile(fn+'.zip')
            else:
                z=zipfile.ZipFile(fn)
            for tfn in z.namelist():
                os.remove(tfn)
            z.close()
        #self.doc=doc
        #self.doc=xml.dom.minidom.parse(fn)
        #self.topelem=self.doc.documentElement
        #print "doc is",self.doc
        topelem=self.doc.getRoot()
        #print "docElem",self.topelem
        listFiles.DirEntry.__init__(self)
        for c in self.doc.getChilds(topelem):
            if self.doc.getTagName(c)=='DIR':
                    self._addDirEntry_(c,self)

        #self.buildXml()
    def write(self,fn,mode=0):
        """writes information to an xml-file named <fn>.
fn      name for the output file
mode   definies write mode
   0 ... generate xml-file
   1 ... compress xml-file into a zipfile named <fn.zip> and remove plant xml-file.
        """
        #dom=xml.dom.minidom.getDOMImplementation()
        #doc=dom.createDocument(None,"DirEntry",None)
        #topelem=doc.documentElement
        #textelem=doc.createTextNode('node')
        #topelem.appendChild(textelem)
        self.doc.Save(fn)
        if mode==1:
            #do zip
            try:
                zf=zipfile.ZipFile(fn+".zip","w",zipfile.ZIP_DEFLATED)
                zf.write(fn)
                zf.close()
                os.remove(fn)
            except:
                pass

