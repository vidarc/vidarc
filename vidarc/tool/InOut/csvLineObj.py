#----------------------------------------------------------------------------
# Name:         csvLineObj.py
# Purpose:      used??
# Author:       Walter Obweger
#
# Created:      20060407
# CVS-ID:       $Id: csvLineObj.py,v 1.1 2006/04/11 17:46:51 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os,string

class csvLineObjProp:
    def __init__(self):
        self.name=''
        self.value=''
class csvLineObjStyle:
    def __init__(self):
        self.key=[]
        self.link=[]
    def add(items):
        pass
    def addKey(obj):
        self.key.append(obj)
    def addLink(obj):
        self.link.append(obj)
    
class csvConfig:
    def __init__(self):
        objs={}
        
    def read(fn):
        f=open(fn,'r')
        lines=f.readlines()
        f.close()
        for l in lines:
            strs=string.string(l,',')
            if not objs.has_key(strs[0]):
                objs[strs[0]]=[]
            t=csvLineObjStyle(strs[1:])

if __name__=='__main__':
    pass
