# -*- coding: iso-8859-1 -*-
#----------------------------------------------------------------------
# Name:         vtInOutNetTools.py
# Purpose:      network tools functions
#
# Author:       Walter Obweger
#
# Created:      20100219
# CVS-ID:       $Id: vtInOutNetTools.py,v 1.2 2010/02/26 20:21:40 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import sys

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

from vSystem import isPlatForm
if isPlatForm('win'):
    from vtInOutNetToolsWin import *
else:
    from vtInOutNetToolsDummy import *
