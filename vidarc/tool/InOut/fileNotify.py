#----------------------------------------------------------------------------
# Name:         dirNotify.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060127
# CVS-ID:       $Id: fileNotify.py,v 1.3 2009/02/22 17:54:43 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os,sys
import time,thread,threading,traceback
import Queue

if sys.platform.startswith('win'):
    SYSTEM=1
    import win32file
    import win32con
    FILE_LIST_DIRECTORY = 0x0001
elif sys.platform.startswith('linux'):
    SYSTEM=0
    pass
    
class fileNotifyReceiver:
    def __init__(self):
        pass
    def changed(self,sAct,sFN,zTm):
        print 'changed',sAct,sFN,time.asctime(time.localtime(zTm))

class fileNotify:
    def __init__(self,dn,fn,zMinNotifyDelay=1,verbose=0):
        self.zMinNotifyDelay=zMinNotifyDelay
        self.zLastNotify=0#time.time()
        
        self.dn=dn
        self.fn=fn
        self.qMod=Queue.Queue()
        self.QUEUES={
            3 : self.qMod,
            }
        self.verbose=verbose
        self.serving=False
        self.stopping=False
        self.receiver=[]
        if SYSTEM==1:
            try:
                self.hDir = win32file.CreateFile (
                    dn,
                    FILE_LIST_DIRECTORY,
                    win32con.FILE_SHARE_READ | win32con.FILE_SHARE_WRITE,
                    None,
                    win32con.OPEN_EXISTING,
                    win32con.FILE_FLAG_BACKUP_SEMANTICS,
                    None
                    )
            except:
                self.hDir=None
    def Is2Notify(self):
        if time.time()-self.zLastNotify>self.zMinNotifyDelay:
            return True
        else:
            return False
    def AddReceiver(self,receiver):
        self.receiver.append(receiver)
    def DelReceiver(self,receiver):
        try:
            self.receiver.remove(receiver)
        except:
            pass
    def Add2Monitor4Change(self,name):
        if self.monitor is None:
            self.monitor=[]
        self.monitor.append(name)
    def Start(self):
        if self.serving:
            return
        if self.stopping:
            return
        self.serving=True
        self.stopping=False
        if SYSTEM==1:
            thread.start_new_thread(self.RunWin,())
        thread.start_new_thread(self.RunNotify,())
    def Stop(self):
        #vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.stopping=True
    def Pause(self,flag):
        self.pausing=flag
    def IsRunning(self):
        return self.serving
    def RunNotify(self):
        while self.serving:
            lst=[]
            try:
                
                while 1:
                    sAct,sFN,zTm=self.qMod.get(False)
                    try:
                        lst.index(sFN)
                    except:
                        for rcv in self.receiver:
                            rcv.changed(sAct,sFN,zTm)
                        lst.append(sFN)
            except:
                pass
            time.sleep(self.zMinNotifyDelay)
    def RunWin(self):
        ACTIONS = {
            3 : "Updated",
            }
        while self.serving:
            if self.stopping==True:
                self.serving=False
                continue
            try:
                results = win32file.ReadDirectoryChangesW (
                                self.hDir,
                                1024,
                                True,
                                win32con.FILE_NOTIFY_CHANGE_FILE_NAME | 
                                win32con.FILE_NOTIFY_CHANGE_DIR_NAME |
                                win32con.FILE_NOTIFY_CHANGE_ATTRIBUTES |
                                win32con.FILE_NOTIFY_CHANGE_SIZE |
                                win32con.FILE_NOTIFY_CHANGE_LAST_WRITE |
                                win32con.FILE_NOTIFY_CHANGE_SECURITY,
                                None,
                                None
                                )
                zClk=time.clock()
                for action, file in results:
                    if action==3:
                        if self.fn==file:
                            sFN=os.path.join(self.dn, file)
                            sAct=ACTIONS.get (action, "Unknown")
                            qTmp=self.QUEUES.get(action,None)
                            if qTmp is not None:
                                qTmp.put((sAct,sFN,time.time()))
            except:
                traceback.print_exc()
                self.serving=False
    
def test():
    dn=fileNotify('.','test02.txt')
    drcv=fileNotifyReceiver()
    dn.AddReceiver(drcv)
    dn.Start()
    for i in range(10):
        time.sleep(1)
    dn.Stop()
    pass

if __name__=='__main__':
    test()
