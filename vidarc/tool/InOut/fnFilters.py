#----------------------------------------------------------------------------
# Name:         fnFilters.py
# Purpose:      filename filters
# Author:       Walter Obweger
#
# Created:      20060407
# CVS-ID:       $Id: fnFilters.py,v 1.2 2008/05/07 17:16:28 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os.path
import glob

DIR_OneInFilter=0
DIR_OneExFilter=1
FILE_OneInFilter=2
FILE_OneExFilter=3
FULL_OneInFilter=4
FULL_OneExFilter=5
DIR_AllInFilter=6
DIR_AllExFilter=7
FILE_AllInFilter=8
FILE_AllExFilter=9
FULL_AllInFilter=10
FULL_AllExFilter=11

ONE_idx=[DIR_OneInFilter,
    DIR_OneExFilter,
    FILE_OneInFilter,
    FILE_OneExFilter,
    FULL_OneInFilter,
    FULL_OneExFilter
    ]
IN_idx=[DIR_OneInFilter,
    FILE_OneInFilter,
    FULL_OneInFilter,
    DIR_AllInFilter,
    FILE_AllInFilter,
    FULL_AllInFilter]
EX_idx=[DIR_OneExFilter,
    FILE_OneExFilter,
    FULL_OneExFilter,
    DIR_AllExFilter,
    FILE_AllExFilter,
    FULL_AllExFilter]
DIR_idx=[DIR_OneInFilter,
    DIR_OneExFilter,
    DIR_AllInFilter,
    DIR_AllExFilter]
FILE_idx=[FILE_OneInFilter,
    FILE_OneExFilter,
    FILE_AllInFilter,
    FILE_AllExFilter]
FULL_idx=[FULL_OneInFilter,
    FULL_OneExFilter,
    FULL_AllInFilter,
    FULL_AllExFilter]

filter_str=[
    'dir_incl_one',
    'dir_excl_one',
    'file_in_one',
    'file_excl_one',
    'full_incl_one',
    'full_excl_one',
    'dir_incl_all',
    'dir_excl_all',
    'file_incl_all',
    'file_excl_all',
    'full_incl_all',
    'full_excl_all']
    
filter_str_desc={
    'dir_incl_one':"""the directory is included
if one matches""",
    'dir_excl_one':"""the directory is excluded
if one matches""",
    'file_in_one':"""the file is included
if one matches""",
    'file_excl_one':"""the file is excluded
if one matches""",
    'full_incl_one':"""the fullname (directory+file)
is included
if one matches""",
    'full_excl_one':"""the fullname (directoy+file)
is excluded
if one matches""",
    'dir_incl_all':"""the directory is included
if all matches""",
    'dir_excl_all':"""the directory is excluded
if all matches""",
    'file_incl_all':"""the file is included
if all matches""",
    'file_excl_all':"""the file is excluded
if all matches""",
    'full_incl_all':"""the fullname (directory+file)
is included
if all matches""",
    'full_excl_all':"""the fullname (directory+file)
is excluded
if all matches"""}


class vtIOFileNameFilter:
    def __init__(self):
        self.filters=[]
        for i in range(0,12):
            self.filters.append([])
    def getFilterStr(self):
        return filter_str
    def getFilters(self,type):
        try:
            #print 'getFilters',type
            #print self.filters[type]
            return self.filters[type]
        except:
            return []
    def add(self,type,filter):
        """add filter 
                type ... filter style
                filter ... filter string
            """
        try:
            try:
                num=filter_str.index(type)
            except:
                num=type
            self.filters[num].append(filter)
        except:
            print 'filter string add fault, type=',type,' filtername=',filter
            pass
        
    def apply(self,fn,skip_filename_characters=0,verbose=0,isDir=False):
        """apply different filters to directory and filename.
            filters['dir_inFilter_one']=[....]
            filters['dir_exFilter_one']=[....]
            filters['file_inFilter_one']=[.....]
            filters['file_exFilter_one']=[......]
            filters['fullfile_inFilter_one']=[......]
            filters['fullfile_exFilter_one']=[.......]
            filters['dir_inFilter_all']=[....]
            filters['dir_exFilter_all']=[....]
            filters['file_inFilter_all']=[.....]
            filters['file_exFilter_all']=[......]
            filters['fullfile_inFilter_all']=[......]
            filters['fullfile_exFilter_all']=[.......]
        """
        if verbose>0:
            print '\n',fn
        def isInOneFilter(str,filters,verbose=0):
            for filter in filters:
                if len(filter)>=1:
                    if verbose>0:
                        print 'isOne',str,filter
                    if glob.fnmatch.fnmatch(str,filter)>0:
                        return 1
            return 0
        def isInAllFilter(str,filters,verbose=0):
            for filter in filters:
                if len(filter)>=1:
                    if verbose>0:
                        print 'isAll',str,filter
                    if glob.fnmatch.fnmatch(str,filter)==0:
                        return 0
            return 1
        def check(tmp,i,verbose=0):
            if verbose>0:
                print self.filters[i],i,len(self.filters[i])
            if len(self.filters[i])<1:
                if verbose>0:
                    print 'check len=0'
                return 2
            try:
                ONE_idx.index(i)
                if verbose>0:
                    print 'check one',i
                return isInOneFilter(tmp,self.filters[i])
            except:
                if verbose>0:
                    print 'check all',i
                return isInAllFilter(tmp,self.filters[i])
            return 0
        def checkInEx(tmp,i):
            try:
                IN_idx.index(i)
                iRet=1
            except:
                iRet=2
            ret=check(tmp,i)
            if verbose>0:
                print 'checkInEx',i,iRet,'1=in,2=out ','ret:',ret
            if iRet==2:
                if ret==1:
                    ret=0
                elif ret==0:
                    ret=1
            return ret
        fn=fn[skip_filename_characters:]
        if isDir==False:
            tmp=os.path.dirname(fn)
        else:
            tmp=fn
        for i in DIR_idx:
            if checkInEx(tmp,i)<1:
                return 0
        #tmp=os.path.dirname(fn)
        for i in DIR_idx:
            if checkInEx(tmp,i)<1:
                return 0
        if isDir==False:
            tmp=os.path.basename(fn)
            for i in FILE_idx:
                if checkInEx(tmp,i)<1:
                    return 0
            for i in FULL_idx:
                if checkInEx(tmp,i)<1:
                    return 0
        return 1

if __name__=='__main__':
    f=FileFilter()
    f.add(DIR_OneExFilter,'*/CVS')
    f.add(FILE_OneExFilter,'*.txt')
    print "\n\n"
    s='/test/CVS/test.doc'
    print s,f.apply(s,verbose=1)
    s='/test/CVSs/test.doc'
    print s,f.apply(s)
    s='/test/test.txt'
    print s,f.apply(s)