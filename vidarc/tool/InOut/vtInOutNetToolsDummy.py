# -*- coding: iso-8859-1 -*-
#----------------------------------------------------------------------
# Name:         vtInOutNetToolsDummy.py
# Purpose:      network tools functions for dummy platform
#
# Author:       Walter Obweger
#
# Created:      20100226
# CVS-ID:       $Id: vtInOutNetToolsDummy.py,v 1.1 2010/02/26 20:21:40 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import sys

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

def getNetUsrLst(sHost=None):
    lUsr=[]
    try:
        return lUsr
    except:
        vtLog.vtLngTB(__name__)
    return None
def getHostLst(sDomain=None,sHost=None):
    lHost=[]
    try:
        return lHost
    except:
        vtLog.vtLngTB(__name__)
    return None
def getMntDict(sHost=None):
    dMnt={None:{}}
    try:
        return dMnt
    except:
        vtLog.vtLngTB(__name__)
    return None
def getHostInfo(sHost=None):
    dRes={}
    return dRes
def getHostShareDict(sHost=None):
    dMnt={None:{}}
    try:
        return dMnt
    except:
        vtLog.vtLngTB(__name__)
    return None
def getHostDiskLst(sHost):
    l=[]
    return l
def SambaMnt(sSrc,sMnt,sUsr,sPwd,thd=None):
    try:
        return 0
    except:
        vtLog.vtLngTB(__name__)
    return -1
def SambaUnMnt(sMnt,iForce=0,thd=None):
    try:
        return 0
    except:
        vtLog.vtLngTB(__name__)
    return -1
