# -*- coding: iso-8859-1 -*-
#----------------------------------------------------------------------
# Name:         vtInOutFileTools.py
# Purpose:      file tools functions
#
# Author:       Walter Obweger
#
# Created:      20100215
# CVS-ID:       $Id: vtInOutFileTools.py,v 1.6 2010/04/03 19:16:01 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os,stat,time

from vidarc.tool.InOut.genFN import getCurDateTimeFN
from vidarc.tool.InOut.vtInOutFileTimes import setFileTimes

from vSystem import isPlatForm
WIN_FILE=0
if isPlatForm('win'):
    SYSTEM=1
    try:
        import win32file
        WIN_FILE=1
    except:
        pass
else:
    SYSTEM=0

def makedirs(sDN,thd=None):
    """ return
         2  ... created
         1  ... exists already
         0  ... nothing to do
        -1  ... fault during creating
    """
    if sDN is not None:
        if SYSTEM==1:
            if sDN[-1]==':':
                return 1
        if os.path.exists(sDN)==False:
            try:
                os.makedirs(sDN)
                return 2
            except:
                if thd is not None:
                    thd.__logTB__()
                    thd.__logError__('sDN:%s'%(sDN))
                return -1
        else:
            return 1
    return 0
def removedirs(sDN,thd=None):
    """ return
         2  ... removed
         1  ... already removed
         0  ... nothing to do
        -1  ... fault during removing
    """
    if sDN is not None:
        if os.path.exists(sDN)==True:
            try:
                os.removedirs(sDN)
                return 2
            except:
                if thd is not None:
                    thd.__logTB__()
                return -1
        else:
            return 1
    return 0
def copy(sSrcFN,sDstFN,thd=None,bKeepDestUntilDone=False,iCur=0,iSz=None,fUpd=0.1):
    if thd is not None:
        post=thd.Post
        logInfo=thd.__logInfo__
        logTB=thd.__logTB__
        procBar=thd.__logSetProcBarVal__
    else:
        def post(*args,**kwargs):
            pass
        logInfo=post
        logTB=post
        procBar=post
    #logInfo('sSrcFN:%s;sDstFN:%s'%(sSrcFN,sDstFN))
    post('proc',sSrcFN,None)
    #post('proc',('cp',0,sSrcFN,sDstFN),None)
    if bKeepDestUntilDone:
        sTmpFN=getCurDateTimeFN(sDstFN)
        dn,fn=os.path.split(sTmpFN)
        try:
            os.makedirs(dn)
        except:
            pass
        try:
            if os.path.exists(sDstFN):
                os.rename(sDstFN,sTmpFN)
        except:
            post('proc','destination file could not be removed',None)
            #post('proc',('cp',-3,sSrcFN,sDstFN),None)
            logTB()
            return -3
    try:
        st=os.stat(sSrcFN)
        iMode=st[stat.ST_MODE]
        if stat.S_ISREG(iMode)==False:
            post('proc','file is not a regular one',None)
            return -4
        iSize=st[stat.ST_SIZE]
        if iSz is None:
            iSz=iSize
        iAct=0
        fOut=open(sDstFN,'wb')
        fIn=open(sSrcFN,'rb')
        zUpd=time.clock()+fUpd
        procBar(iAct,iSize)
        while iAct!=iSize:
            blk=fIn.read(4096)
            iAct=fIn.tell()
            fOut.write(blk)
            zClk=time.clock()
            if zClk>zUpd:
                zUpd=zClk+fUpd
                post('proc',iCur+iAct,iSz)
                procBar(iAct,iSize)
        post('proc',iCur+iAct,iSz)
        procBar(iAct,iSize)
        fIn.close()
        fOut.close()
    except:
        logTB()
        try:
            fIn.close()
        except:
            pass
        try:
            fOut.close()
        except:
            pass
        if bKeepDestUntilDone:
            try:
                if os.path.exists(sDstFN):
                    os.remove(sDstFN)
                if os.path.exists(sTmpFN):
                    os.rename(sTmpFN,sDstFN)
            except:
                logTB()
        #post('proc',('cp',-1,sSrcFN,sDstFN),None)
        return -1
    if bKeepDestUntilDone:
        try:
            if os.path.exists(sTmpFN):
                os.remove(sTmpFN)
        except:
            logTB()
            post('proc','destination file could not be removed',None)
            #post('proc',('cp',-2,sSrcFN,sDstFN),None)
            return -2
    try:
        #st=os.stat(sSrcFN)
        setFileTimes(sDstFN,st[stat.ST_CTIME],st[stat.ST_ATIME],st[stat.ST_MTIME])
        os.chmod(sDstFN,stat.S_IMODE(iMode))
        if SYSTEM==0:
            os.chown(sDstFN,st[stat.ST_UID],st[stat.ST_GID])
    except:
        logTB()
    post('proc',('cp',0,sSrcFN,sDstFN),None)
    return 0
def move(sSrcFN,sDstFN,thd=None,bKeepDestUntilDone=False,iCur=0,iSz=None):
    if thd is not None:
        post=thd.Post
        logInfo=thd.__logInfo__
        logTB=thd.__logTB__
    else:
        def post(*args,**kwargs):
            pass
        logInfo=post
        logTB=post
    logInfo('sSrcFN:%s;sDstFN:%s'%(sSrcFN,sDstFN))
    iRet=copy(sSrcFN,sDstFN,thd,bKeepDestUntilDone=bKeepDestUntilDone,iCur=iCur,iSz=iSz)
    if iRet==0:
        try:
            if os.path.exists(sSrcFN):
                os.remove(sSrcFN)
                return 0
        except:
            logTB()
            post('proc','source file could not be removed',None)
            return -4
        return -5
    return iRet
def remove(sSrcFN,thd=None):
    if thd is not None:
        post=thd.Post
        logInfo=thd.__logInfo__
        logTB=thd.__logTB__
    else:
        def post(*args,**kwargs):
            pass
        logInfo=post
        logTB=post
    logInfo('sSrcFN:%s'%(sSrcFN))
    #self.iAct+=1
    try:
        if os.path.exists(sSrcFN):
            st=os.stat(sSrcFN)
            iMode=st[stat.ST_MODE]
            if stat.S_ISREG(iMode)==False:
                post('proc','file is not a regular one',None)
                return -6
            try:
                os.remove(sSrcFN)
            except:
                if WIN_FILE>0:
                    iAttr=win32file.GetFileAttributes(sSrcFN)
                    logInfo('%s;%x'%(sSrcFN,iAttr))
                    win32file.SetFileAttributes(sSrcFN,iAttr & 0xFFFF00)
                    #iAttr=win32file.GetFileAttributes(sSrcFN)
                    #logInfo('%s;%x'%(sSrcFN,iAttr))
                    win32file.DeleteFile(sSrcFN)
                    return 0
                else:
                    os.chmod(sSrcFN,stat.S_IMODE(iMode)|stat.S_IWRITE|stat.S_IREAD)
                os.remove(sSrcFN)
            return 0
    except:
        logTB()
        post('proc','source file could not be removed',None)
        return -4
    return -5
