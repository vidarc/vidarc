# -*- coding: iso-8859-1 -*-
#----------------------------------------------------------------------
# Name:         vtInOutNetToolsWin.py
# Purpose:      network tools functions for Windows Platform
#
# Author:       Walter Obweger
#
# Created:      20100226
# CVS-ID:       $Id: vtInOutNetToolsWin.py,v 1.1 2010/02/26 20:21:40 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import sys

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

import win32net
import win32netcon
import time
def getNetUsrLst(sHost=None):
    bLoop=True
    pref=win32netcon.MAX_PREFERRED_LENGTH
    iLv=0 #setting it to 1 will provide more detailed info
    lUsr=[]
    hRes=0
    try:
        while bLoop:
            l,iCnt,hRes=win32net.NetWkstaUserEnum(sHost,iLv,hRes,pref)
            if hRes==0:
                bLoop=False
            for it in l:
                lUsr.append(it['username'])
        return lUsr
    except:
        vtLog.vtLngTB(__name__)
    return None
def getHostLst(sDomain=None,sHost=None):
    bLoop=True
    pref=win32netcon.MAX_PREFERRED_LENGTH
    iLv=101
    iType=win32netcon.SV_TYPE_ALL
    lHost=[]
    hRes=0
    try:
        while bLoop:
            l,iCnt,hRes=win32net.NetServerEnum(sHost,iLv,iType,sDomain,hRes,pref)
            if hRes==0:
                bLoop=False
            lHost.extend(l)
        return lHost
    except:
        vtLog.vtLngTB(__name__)
    return None
def getMntDict(sHost=None):
    bLoop=True
    pref=win32netcon.MAX_PREFERRED_LENGTH
    iLv=2
    dMnt={None:{}}
    hRes=0
    try:
        while bLoop:
            l,iCnt,hRes=win32net.NetUseEnum(sHost,iLv,hRes,pref)
            if hRes==0:
                bLoop=False
            for it in l:
                dMnt[it['local']+'\\']=it['remote']
                dMnt[None][it['local']+'\\']=it
        return dMnt
    except:
        vtLog.vtLngTB(__name__)
    return None
def getHostInfo(sHost=None):
    dRes={}
    try:
        d=win32net.NetWkstaGetInfo(sHost,102)
        dRes['host']=d['computername']
        dRes['domain']=d['langroup']
        dRes['os_ver']='%d.%d'%(d['ver_major'],d['ver_minor'])
        dRes[None]=d.copy()
    except:
        vtLog.vtLngTB(__name__)
    try:
        d=win32net.NetServerGetInfo(sHost,102)
        dRes[None].update(d)
    except:
        vtLog.vtLngTB(__name__)
    try:
        d=win32net.NetServerGetInfo(sHost,502)
        dRes[None].update(d)
    except:
        vtLog.vtLngTB(__name__)
    try:
        d=win32net.NetServerGetInfo(sHost,503)
        dRes[None].update(d)
    except:
        vtLog.vtLngTB(__name__)
    return dRes
    #d=win32net.NetMessageNameEnum(sHost)
    #print vtLog.pformat(d)
def getHostShareDict(sHost=None):
    bLoop=True
    pref=win32netcon.MAX_PREFERRED_LENGTH
    iLv=2
    dMnt={None:{}}
    hRes=0
    try:
        sHostName=sHost or '127.0.0.1'
        while bLoop:
            l,iCnt,hRes=win32net.NetShareEnum(sHost,iLv,hRes,pref)
            if hRes==0:
                bLoop=False
            for it in l:
                sName=it['netname']
                sMnt='\\'.join(['','',sHostName,sName])
                d={
                    'mnt':sMnt,'type':'smb','name':sName,
                    'host':sHostName,
                    }
                dMnt[sMnt]=d
                dMnt[None][sMnt]=it
        return dMnt
    except:
        vtLog.vtLngTB(__name__)
    return None
def getHostDiskLst(sHost):
    l=win32net.NetServerDiskEnum(sHost,0)
    return l
def SambaMnt(sSrc,sMnt,sUsr,sPwd,thd=None):
    try:
        win32net.NetUseAdd(None,2,{'remote':sSrc,'local':sMnt,
                'username':sUsr,'password':sPwd})
        return 0
    except:
        vtLog.vtLngTB(__name__)
    return -1
def SambaUnMnt(sMnt,iForce=0,thd=None):
    try:
        if iForce==1:
            iForce=win32net.USE_FORCE
        elif iForce==1:
            iForce=win32net.USE_LOTS_OF_FORCE
        else:
            iForce=win32net.USE_NOFORCE
        time.sleep(0.5)
        win32net.NetUseDel(None,sMnt,iForce)
        return 0
    except:
        vtLog.vtLngTB(__name__)
    return -1
