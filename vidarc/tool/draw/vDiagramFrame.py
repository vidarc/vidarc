#Boa:Frame:vDiagramFrame
#----------------------------------------------------------------------------
# Name:         vDiagramFrame.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vDiagramFrame.py,v 1.1 2005/12/11 23:04:48 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons

import string,os,time,sys
import thread,threading,traceback

from vidarc.tool.draw.vtDrawDiagramPanel import *

import images


def create(parent):
    return vDiagramFrame(parent)

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon

[wxID_VDIAGRAMFRAME, wxID_VDIAGRAMFRAMECBGRPS, wxID_VDIAGRAMFRAMECBLINE, 
 wxID_VDIAGRAMFRAMEPNMAIN, wxID_VDIAGRAMFRAMESBSTATUS, 
 wxID_VDIAGRAMFRAMETBMAIN, 
] = [wx.NewId() for _init_ctrls in range(6)]

[wxID_VDIAGRAMFRAMEMNFILEITEM_EXIT, wxID_VDIAGRAMFRAMEMNFILEITEM_OPEN, 
] = [wx.NewId() for _init_coll_mnFile_Items in range(2)]


STATUS_PROCESS_POS=0
STATUS_CLOCK_POS=1

class vDiagramFrame(wx.Frame):
    def _init_coll_mnFile_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_VDIAGRAMFRAMEMNFILEITEM_OPEN,
              kind=wx.ITEM_NORMAL, text=_(u'Open'))
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VDIAGRAMFRAMEMNFILEITEM_EXIT,
              kind=wx.ITEM_NORMAL, text=_(u'Exit'))
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_openMenu,
              id=wxID_VDIAGRAMFRAMEMNFILEITEM_OPEN)
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_exitMenu,
              id=wxID_VDIAGRAMFRAMEMNFILEITEM_EXIT)

    def _init_coll_mnBar_Menus(self, parent):
        # generated method, don't edit

        parent.Append(menu=self.mnFile, title=_(u'File'))

    def _init_coll_sbStatus_Fields(self, parent):
        # generated method, don't edit
        parent.SetFieldsCount(2)

        parent.SetStatusText(number=0, text=u'Process')
        parent.SetStatusText(number=1, text=u'Clock')

        parent.SetStatusWidths([-1, 155])

    def _init_utils(self):
        # generated method, don't edit
        self.mnBar = wx.MenuBar()

        self.mnFile = wx.Menu(title=u'')

        self._init_coll_mnBar_Menus(self.mnBar)
        self._init_coll_mnFile_Items(self.mnFile)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VDIAGRAMFRAME, name=u'vDiagramFrame',
              parent=prnt, pos=wx.Point(312, 47), size=wx.Size(579, 474),
              style=wx.DEFAULT_FRAME_STYLE, title=u'vDiagramFrame')
        self._init_utils()
        self.SetClientSize(wx.Size(571, 447))
        self.SetMenuBar(self.mnBar)

        self.sbStatus = wx.StatusBar(id=wxID_VDIAGRAMFRAMESBSTATUS,
              name=u'sbStatus', parent=self, style=0)
        self.sbStatus.SetFieldsCount(2)
        self._init_coll_sbStatus_Fields(self.sbStatus)
        self.SetStatusBar(self.sbStatus)

        self.pnMain = wx.Panel(id=wxID_VDIAGRAMFRAMEPNMAIN, name=u'pnMain',
              parent=self, pos=wx.Point(0, 0), size=wx.Size(571, 408),
              style=wx.TAB_TRAVERSAL)
        self.pnMain.SetAutoLayout(True)

        self.tbMain = wx.ToolBar(id=wxID_VDIAGRAMFRAMETBMAIN, name=u'tbMain',
              parent=self.pnMain, pos=wx.Point(0, 0), size=wx.Size(571, 28),
              style=wx.TB_HORIZONTAL | wx.NO_BORDER)

        self.cbLine = wx.BitmapButton(bitmap=wx.NullBitmap,
              id=wxID_VDIAGRAMFRAMECBLINE, name=u'cbLine', parent=self.tbMain,
              pos=wx.Point(0, 0), size=wx.Size(24, 24), style=wx.BU_AUTODRAW)

        self.cbGrps = wx.BitmapButton(bitmap=wx.NullBitmap,
              id=wxID_VDIAGRAMFRAMECBGRPS, name=u'cbGrps', parent=self.tbMain,
              pos=wx.Point(32, 0), size=wx.Size(24, 24), style=wx.BU_AUTODRAW)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.bModified=False
        self.Clear()
        
        tup=[[(0,'0'),(1,'1')],[(0.0,'x'),(1.5,'y')]]
        self.dgTiming=vtDrawDiagramPanel(self.pnMain,labels=tup,max=(100,100),
                    grid=(20,20),pos=(10,10),size=(300,300))
        vals=[{'name':'bbb','range':(0,1),'values':[(0,0),(1,0),(1,1),(4,1),(-1,-1),(5,0),(6,0),(6,0.5),(7,1)]},
            {'name':'aaa','range':(2,5),'values':[(0,0),(1,0),(1,1),(4,1),(-1,-1),(5,0),(6,0),(6,0.5),(7,1)]},
            {'name':'ccc','range':(3,5),'values':[(0,0),(1,0),(1,1),(4,1),(-1,-1),(5,0),(6,0),(6,0.5),(7,1)]}]
        self.dgTiming.SetValues(vals)
        
        # setup statusbar
        self.timer = wx.PyTimer(self.Notify)
        self.timer.Start(1000)
        self.Notify()
        
        icon = getApplicationIcon()
        self.SetIcon(icon)
        
        rect = self.sbStatus.GetFieldRect(STATUS_PROCESS_POS)
        self.gProcess = wx.Gauge(
                    self.sbStatus, -1, 50, (rect.x+2, rect.y+2), 
                    (rect.width-4, rect.height-4), 
                    wx.GA_HORIZONTAL|wx.GA_SMOOTH
                    )
        self.gProcess.SetRange(100)
        self.gProcess.SetValue(0)
        
    def Notify(self):
        try:
            t = time.localtime(time.time())
            st = time.strftime(_("%b-%d-%Y   %H:%M:%S wk:%W"), t)
            self.SetStatusText(st, STATUS_CLOCK_POS)
        except:
            pass
    def __makeTitle__(self):
        s=_("VIDARC Diagram")
        if self.fn is None:
            s=s+_(" (undef*)")
        else:
            s=s+" ("+self.fn
            if self.bModified:
                s=s+"*)"
            else:
                s=s+")"
        self.SetTitle(s)
    def __setModified__(self,state):
        self.bModified=state
        self.__makeTitle__()
    
    def Clear(self):
        pass
    def OpenFile(self,fn=None):
        pass
    def OnMnFileItem_openMenu(self, event):
        if self.thdOpen.IsRunning():
            return 
        dlg = wx.FileDialog(self, "Open", ".", "", "log files (*.log)|*.log|all files (*.*)|*.*", wx.OPEN)
        try:
            fn=self.GetFN()
            if fn is not None:
                dlg.SetPath(fn)
            #else:
            #    dlg.SetPath('.')
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.OpenFile(filename)
                self.__setModified__(False)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()

    def OnMnFileItem_exitMenu(self, event):
        self.Close()
        event.Skip()        
        
