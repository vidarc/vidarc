#----------------------------------------------------------------------------
# Name:         vtDrawCanvasText.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtDrawCanvasText.py,v 1.4 2006/03/14 14:26:17 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vtDrawCanvasObjectBase import *
import vidarc.tool.report.vRepLatex as vtRepLatex

class vtDrawCanvasText(vtDrawCanvasObjectBase):
    ALIGN_CENTER = 0
    ALIGN_TOP = 1
    ALIGN_BOTTOM = 2
    ALIGN_LEFT = 3
    ALIGN_RIGHT = 4
    ALIGN_HORIZONTAL_ENTRIES=[u'center',u'left',u'right']
    ALIGN_VERTICAL_ENTRIES=[u'center',u'top',u'bottom']
    #ALIGN_HORIZONTAL_MAP={u'center':ALIGN_CENTER,u'left':ALIGN_LEFT,u'right':ALIGN_RIGHT}
    #ALIGN_VERTICAL_MAP={u'center':ALIGN_CENTER,u'top':ALIGN_TOP,u'bottom':ALIGN_BOTTOM}
    
    def __init__(self,id=-1,name='',text='',x=0,y=0,w=-1,h=-1,
            align_horizontal=ALIGN_CENTER,align_vertical=ALIGN_CENTER,layer=0):
        vtDrawCanvasObjectBase.__init__(self,id,name,x,y,w,h,layer)
        self.text=text
        self.alignHori=align_horizontal
        self.alignVert=align_vertical
        self.margin=0
    def SetMargin(self,val):
        self.margin=val
    def GetMargin(self):
        return self.margin
    def GetAttr(self,idx):
        if idx<6:
            return vtDrawCanvasObjectBase.GetAttr(self,idx)
        if idx==6:
            return 'text',self.text,'%s',0
        elif idx==7:
            if self.alignHori==self.ALIGN_CENTER:
                val=_('center')
            elif self.alignHori==self.ALIGN_LEFT:
                val=_('left')
            elif self.alignHori==self.ALIGN_RIGHT:
                val=_('right')
            return 'horizontal',val,'%s',2
        elif idx==8:
            if self.alignVert==self.ALIGN_CENTER:
                val=_('center')
            elif self.alignVert==self.ALIGN_TOP:
                val=_('top')
            elif self.alignVert==self.ALIGN_BOTTOM:
                val=_('bottom')
            return 'vertical',val,'%s',2
        elif idx==9:
            return 'margin',self.margin,'%d',1
        return '','','',-1
    def SetAttr(self,idx,val):
        if idx<6:
            return vtDrawCanvasObjectBase.SetAttr(self,idx,val)
        if idx==6:
            self.text=val
        elif idx==7:
            if val==0:
                self.alignHori=self.ALIGN_CENTER
            elif val==1:
                self.alignHori=self.ALIGN_LEFT
            elif val==2:
                self.alignHori=self.ALIGN_RIGHT
            return 0
        elif idx==8:
            if val==0:
                self.alignVert=self.ALIGN_CENTER
            elif val==1:
                self.alignVert=self.ALIGN_TOP
            elif val==2:
                self.alignVert=self.ALIGN_BOTTOM
            return 0
        elif idx==9:
            self.margin=val
        return -1
    def GetAttrEntries(self,idx):
        if idx==7:
            #return self.ALIGN_HORIZONTAL_ENTRIES,[_(u'center'),_(u'left'),_(u'right')]
            return [_(u'center'),_(u'left'),_(u'right')]
        elif idx==8:
            #return self.ALIGN_VERTICAL_ENTRIES,[_(u'center'),_(u'top'),_(u'bottom')]
            return [_(u'center'),_(u'top'),_(u'bottom')]
    def GetAttrEntrySel(self,idx):
        if idx==7:
            if self.alignHori==self.ALIGN_CENTER:
                return 0
            elif self.alignHori==self.ALIGN_LEFT:
                return 1
            elif self.alignHori==self.ALIGN_RIGHT:
                return 2
            return 0
        elif idx==8:
            if self.alignVert==self.ALIGN_CENTER:
                return 0
            elif self.alignVert==self.ALIGN_TOP:
                return 1
            elif self.alignVert==self.ALIGN_BOTTOM:
                return 2
            return 0
        return -1
    def GetTypeName(self):
        # replace me
        return 'text'
    def SetNode(self,doc,node,canvas):
        vtDrawCanvasObjectBase.SetNode(self,doc,node,canvas)
        self.text=doc.getNodeText(node,'text')
        sVal=doc.getNodeText(node,'align_horizontal')
        if sVal=='center':
            self.alignHori=self.ALIGN_CENTER
        elif sVal=='left':
            self.alignHori=self.ALIGN_LEFT
        elif sVal=='right':
            self.alignHori=self.ALIGN_RIGHT
        else:
            self.alignHoriz=self.ALIGN_CENTER
        
        sVal=doc.getNodeText(node,'align_vertical')
        if sVal=='center':
            self.alignVert=self.ALIGN_CENTER
        elif sVal=='top':
            self.alignVert=self.ALIGN_TOP
        elif sVal=='bottom':
            self.alignVert=self.ALIGN_BOTTOM
        else:
            self.alignVert=self.ALIGN_CENTER
        sVal=doc.getNodeText(node,'margin')
        try:
            self.SetMargin(int(sVal))
        except:
            self.SetMargin(0)
        
    def GetNode(self,doc,parNode,canvas):
        node=vtDrawCanvasObjectBase.GetNode(self,doc,parNode,canvas)
        doc.setNodeText(node,'text',self.text)
        if self.alignHori==self.ALIGN_CENTER:
            doc.setNodeText(node,'align_horizontal','center')
        elif self.alignHori==self.ALIGN_LEFT:
            doc.setNodeText(node,'align_horizontal','left')
        elif self.alignHori==self.ALIGN_RIGHT:
            doc.setNodeText(node,'align_horizontal','right')
        
        if self.alignVert==self.ALIGN_CENTER:
            doc.setNodeText(node,'align_vertical','center')
        elif self.alignVert==self.ALIGN_TOP:
            doc.setNodeText(node,'align_vertical','top')
        elif self.alignVert==self.ALIGN_BOTTOM:
            doc.setNodeText(node,'align_certivall','bottom')
        doc.setNodeText(node,'margin','%d'%self.GetMargin())
        return node
    def __draw__(self,canvas,aX=0,aY=0,aW=-1,aH=-1,iDriver=-1):
        if self.bVisible==False:
            return
        drvInfo=canvas.getDriverData(iDriver)
        drv=canvas.getDriver(iDriver)
        canvas.setActLayer(self.iLayer,self.bActive,iDriver)
        if drv==self.DRV_WX:
            dc=drvInfo['dc']
            if aW>=0 and aH>=0:
                ax,ay=canvas.calcCoor(aX,aY)
                aw,ah=canvas.calcCoor(aW,aH)
                dc.SetClippingRegion(ax,ay,aw+1,ah+1)
            x,y=canvas.calcCoor(self.iX+aX,self.iY+aY)
            w,h=canvas.calcCoor(self.iWidth,self.iHeight)
            
            te = dc.GetTextExtent(self.text)
            if w<0:
                w=te[0]
                if self.alignHori==self.ALIGN_CENTER:
                    x-=w/2
                elif self.alignHori==self.ALIGN_LEFT:
                    x+=self.margin
                elif self.alignHori==self.ALIGN_RIGHT:
                    x-=w+self.margin
                else:
                    pass
            else:
                if self.alignHori==self.ALIGN_CENTER:
                    x+=w/2-te[0]/2
                elif self.alignHori==self.ALIGN_LEFT:
                    x+=self.margin
                elif self.alignHori==self.ALIGN_RIGHT:
                    x+=w-te[0]-self.margin
                else:
                    pass
            
            if h<0:
                h=te[1]
                if self.alignVert==self.ALIGN_CENTER:
                    y-=h/2
                elif self.alignVert==self.ALIGN_TOP:
                    pass
                elif self.alignVert==self.ALIGN_BOTTOM:
                    y-=h
                else:
                    pass
            else:
                if self.alignVert==self.ALIGN_CENTER:
                    y+=h/2-te[1]/2
                elif self.alignVert==self.ALIGN_TOP:
                    pass
                elif self.alignVert==self.ALIGN_BOTTOM:
                    y+=h-te[1]
                else:
                    pass
            
            dc.DrawText(self.text, x, y)
            if aW>=0 and aH>=0:
                dc.DestroyClippingRegion()
        elif drv==self.DRV_LATEX:
            if aW>=0 and aH>=0:
                ax,ay=canvas.calcCoorLatex(aX,aY)
                aw,ah=canvas.calcCoorLatex(aW,aH)
            x,y=canvas.calcCoorLatex(self.iX+aX,self.iY+aY)
            w,h=canvas.calcCoorLatex(self.iWidth,self.iHeight)
            if w<0:
                w=0
            if h<0:
                h=0
            if self.alignVert==self.ALIGN_CENTER:
                sVert='c'
            elif self.alignVert==self.ALIGN_TOP:
                sVert='t'
            elif self.alignVert==self.ALIGN_BOTTOM:
                sVert='b'
            else:
                sVert='c'
            if self.alignHori==self.ALIGN_CENTER:
                sHoriz='c'
            elif self.alignHori==self.ALIGN_LEFT:
                sHoriz='l'
            elif self.alignHori==self.ALIGN_RIGHT:
                sHoriz='r'
            else:
                sHoriz='c'
            c=drvInfo['color']
            strs=drvInfo['strs']
            strs.append('    \\put(%4.1f,%4.1f){%s\\makebox(%d,%d)[%s%s]{%s %s}}'%(x,
                    drvInfo['y_max']-y-h,c,w,h,sVert,sHoriz,drvInfo['font_size'],
                    vtRepLatex.texReplace(self.text)))
        
        pass
    def __getExtend__(self,canvas):
        drvInfo=canvas.getDriverData()
        if canvas.getDriver()==self.DRV_WX:
            x,y=canvas.calcCoor(self.iX,self.iY)
            w,h=canvas.calcCoor(self.iWidth,self.iHeight)
            if w<0 or h<0:
                dc=drvInfo['dc']
                if dc is None:
                    te=(1,1)
                else:
                    canvas.setActLayer(self.iLayer,self.bActive)
                    te = dc.GetTextExtent(self.text)
            if w<0:
                w=te[0]
                if self.alignHori==self.ALIGN_CENTER:
                    x-=w/2
                elif self.alignHori==self.ALIGN_LEFT:
                    pass
                elif self.alignHori==self.ALIGN_RIGHT:
                    x-=w
                else:
                    pass
            if h<0:
                h=te[1]
                if self.alignVert==self.ALIGN_CENTER:
                    y-=h/2
                elif self.alignVert==self.ALIGN_TOP:
                    pass
                elif self.alignVert==self.ALIGN_BOTTOM:
                    y-=h
                else:
                    pass
            xa,ya=canvas.calcPos(x,y,False)
            xe,ye=canvas.calcPos(x+w,y+h,True)
            return int(xa),int(ya),int(xe),int(ye)
    