#Boa:FramePanel:vtDrawCanvasPanel
#----------------------------------------------------------------------------
# Name:         vtDrawCanvasPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtDrawCanvasEditPanel.py,v 1.5 2008/03/26 23:11:43 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
from wx.lib.anchors import LayoutAnchors
import traceback,time,thread,threading
import string,math,copy,os

from vidarc.tool.draw.vtDrawCanvasDialog import *

import vidarc.tool.report.vRepLatex as vtRepLatex
import vidarc.tool.xml.vtXmlDom as vtXmlDom
import vidarc.tool.xml.vtXmlHierarchy as vtXmlHierarchy
import vidarc.tool.log.vtLog as vtLog

from vtDrawCanvasObjectBase import *

VERBOSE=0
BUFFERED=1

[wxID_VTDRAWCANVASPANEL, wxID_VTDRAWCANVASPANELCBEDIT, 
 wxID_VTDRAWCANVASPANELPBPROCESS, wxID_VTDRAWCANVASPANELSCWDRAWING, 
] = [wx.NewId() for _init_ctrls in range(4)]

# defined event for vgpXmlTree item selected
wxEVT_VTDRAW_CANVAS_CHANGED=wx.NewEventType()
def EVT_VTDRAW_CANVAS_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTDRAW_CANVAS_CHANGED,func)
class vtDrawCanvasChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTDRAW_CANVAS_CHANGED(<widget_name>, self.OnInfoChanged)
    """

    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VTDRAW_CANVAS_CHANGED)
        self.obj=obj
    def GetObject(self):
        return self.obj

# defined event for vgpXmlTree item selected
wxEVT_DRAW_THREAD_ELEMENTS=wx.NewEventType()
def EVT_DRAW_THREAD_ELEMENTS(win,func):
    win.Connect(-1,-1,wxEVT_DRAW_THREAD_ELEMENTS,func)
class wxDrawThreadElements(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_DRAW_THREAD_ELEMENTS(<widget_name>, self.OnItemSel)
    """

    def __init__(self,iVal,iCount=-1):
        wx.PyEvent.__init__(self)
        self.val=iVal
        self.count=iCount
        self.SetEventType(wxEVT_DRAW_THREAD_ELEMENTS)
    def GetValue(self):
        return self.val
    def GetCount(self):
        return self.count
# defined event for vgpXmlTree item selected
wxEVT_DRAW_THREAD_ELEMENTS_FINISHED=wx.NewEventType()
def EVT_DRAW_THREAD_ELEMENTS_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_DRAW_THREAD_ELEMENTS_FINISHED,func)
class wxDrawThreadElementsFinished(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_DRAW_THREAD_ELEMENTS_FINISHED(<widget_name>, self.OnItemSel)
    """
    def __init__(self):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_DRAW_THREAD_ELEMENTS_FINISHED)

# defined event for vgpXmlTree item selected
wxEVT_DRAW_THREAD_ELEMENTS_ABORTED=wx.NewEventType()
def EVT_DRAW_THREAD_ELEMENTS_ABORTED(win,func):
    win.Connect(-1,-1,wxEVT_DRAW_THREAD_ELEMENTS_ABORTED,func)
class wxDrawThreadElementsAborted(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_DRAW_THREAD_ELEMENTS_ABORTED(<widget_name>, self.OnItemSel)
    """
    def __init__(self):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_DRAW_THREAD_ELEMENTS_ABORTED)

class thdDraw:
    def __init__(self,par,verbose=0):
        self.verbose=verbose
        self.par=par
        self.Clear()
        self.running=False
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
    def Clear(self):
        self.ClearInt()
    def ClearInt(self):
        self.iCount=0
        self.iAct=0
        self.lst=[]

    def Start(self,canvas):
        self.canvas=canvas
        self.keepGoing = self.running = True
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        thread.start_new_thread(self.RunDraw, ())
    def Stop(self):
        self.keepGoing = False
    def IsRunning(self):
        return self.running
    def RunDraw(self):
        self.bAbort=False
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        try:
            iCount=len(self.canvas.objs)
            iAct=0
            i=0
            if self.par is not None:
                wx.PostEvent(self.par,wxDrawThreadElements(self.iAct,iCount))
            for o in self.canvas.objs:
                iAct+=1
                if self.par is not None:
                    wx.PostEvent(self.par,wxDrawThreadElements(self.iAct,iCount))
                o.__draw__(self.canvas)
        except:
            traceback.print_exc()
        self.canvas.drvInfo[self.canvas.objBase.DRV_WX]['dc']=None
        #while self.keepGoing:
        #    if self.par is not None:
        #        wx.PostEvent(self.par,wxDrawThreadElements(self.iAct,iCount))
        self.keepGoing = self.running = False
        try:
            if self.par is not None:
                wx.PostEvent(self.par,wxDrawThreadElements(0))
                if self.bAbort:
                    wx.PostEvent(self.par,wxDrawThreadElementsAborted())
                else:
                    wx.PostEvent(self.par,wxDrawThreadElementsFinished())
            self.Clear()
            if self.verbose:
                vtLog.vtLngCallStack(self,vtLog.DEBUG,'finished:%d'%self.bAbort)
        except:
            traceback.print_exc()

        
class vtDrawCanvasPanel(wx.Panel):
    DFT_COLOR=[
        (178, 107, 000),
        (178, 145, 000),
        ( 76, 152, 127),
        ( 79,  79,  59),
        (120,  78, 146),
        ( 31, 156, 127),
        (178, 100, 178)
        ]
    DFT_DIS_COLOR=[
        (167, 100, 000),
        (167, 136, 000),
        ( 67, 135, 112),
        (116, 130, 147),
        (100, 166, 100),
        (150, 114,  22),
        (147,  82, 147)
        ]
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTDRAWCANVASPANEL, name='', parent=prnt,
              pos=wx.Point(0, 0), size=wx.Size(545, 307),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(537, 280))
        self.SetAutoLayout(True)

        self.scwDrawing = wx.ScrolledWindow(id=wxID_VTDRAWCANVASPANELSCWDRAWING,
              name=u'scwDrawing', parent=self, pos=wx.Point(8, 8),
              size=wx.Size(440, 240), style=wx.HSCROLL | wx.VSCROLL)
        self.scwDrawing.SetConstraints(LayoutAnchors(self.scwDrawing, True,
              True, True, True))

        self.pbProcess = wx.Gauge(id=wxID_VTDRAWCANVASPANELPBPROCESS,
              name=u'pbProcess', parent=self, pos=wx.Point(8, 256), range=100,
              size=wx.Size(440, 16), style=wx.GA_HORIZONTAL)
        self.pbProcess.SetConstraints(LayoutAnchors(self.pbProcess, True, False,
              True, True))

        self.cbEdit = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTDRAWCANVASPANELCBEDIT,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Edit', name=u'cbEdit',
              parent=self, pos=wx.Point(456, 32), size=wx.Size(76, 30),
              style=0)
        self.cbEdit.SetConstraints(LayoutAnchors(self.cbEdit, False, True, True,
              False))
        self.cbEdit.Bind(wx.EVT_BUTTON, self.OnCbEditButton,
              id=wxID_VTDRAWCANVASPANELCBEDIT)

    def __init__(self, parent, 
                id=wx.NewId(), pos=wx.DefaultPosition, size=wx.Size(100, 50), 
                style=wx.TAB_TRAVERSAL, name=u'vtDrawCanvasPanel'):
        self._init_ctrls(parent)
        self.SetName(name)
        self.doc=None
        self.node=None
        self.baseNode=None
        self.dlg=None
        self.buffer=None
        self.name=u'vtDrawCanvas'
        self.fScale=1.0
        self.iFontSize=2
        self.bRotate=False
        self.iDriver=-1
        self.objBase=None
        self.semDraw=threading.Lock()#threading.Semaphore()
        self.thdDraw=thdDraw(self,verbose=1)
        self.regObjs=[]
        
        self.bAutoApply=False
        self.bModified=False
        self.drvInfo={}
        
        self.verbose=VERBOSE
    
        self.__setDftColors__()
        self.drawing=False
        self.selectableBlks=[]
        self.selectedBlk=None
        
        self.objs=[]
        self.grid=(5,5)
        self.max=(10,10)
        self.scale=(1.0,1.0)
        
        self.cbEdit.SetBitmapLabel(images.getEditBitmap())
        self.SetSize(size)
        self.Move(pos)
        self.__calcSize__()
        self.scwDrawing.Bind(wx.EVT_RIGHT_DOWN,self.OnRightButtonEvent)
        self.scwDrawing.Bind(wx.EVT_PAINT, self.OnPaint)
        EVT_DRAW_THREAD_ELEMENTS(self,self.OnDrawElement)
        EVT_DRAW_THREAD_ELEMENTS_FINISHED(self,self.OnDrawFinished)
        EVT_DRAW_THREAD_ELEMENTS_ABORTED(self,self.OnDrawAborted)
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
    def OnDrawElement(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        iCount=evt.GetCount()
        iAct=evt.GetValue()
        if iCount>=0:
            self.pbProcess.SetRange(iCount)
        self.pbProcess.SetValue(iAct)
        evt.Skip()
    def OnDrawFinished(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #self.drvInfo[self.objBase.DRV_WX]['dc']=None
        self.pbProcess.SetValue(0)
        evt.Skip()
    def OnDrawAborted(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #self.drvInfo[self.objBase.DRV_WX]['dc']=None
        self.pbProcess.SetValue(0)
        evt.Skip()
    def SetConfig(self,obj,drv,max=(10,10), grid=(10, 10)):
        self.max=max
        self.grid=grid
        self.objBase=obj
        self.iDriver=drv
        #self.drvInfo={1:{'dc':None,'printing':False}}
        for d in obj.DRIVERS:
            self.drvInfo[d]={}
        self.__calcSize__()
        #self.__doDraw__()
    def GetMax(self):
        return self.max
    def SetMax(self,max,bUpdate=True):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.max!=max:
            self.max=max
            #if self.__isWxDrv__()==False:
            #    return
            if bUpdate:
                cdc = wx.ClientDC(self.scwDrawing)
                self.scwDrawing.DoPrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
                dc.BeginDrawing()
                dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
                dc.Clear()
                dc.EndDrawing()
                self.__calcSize__()
                #self.__drawObjects__()
                self.scwDrawing.Refresh()
    def GetGrid(self):
        return self.grid
    def SetGrid(self,grid,bUpdate=True):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.grid!=grid:
            self.grid=grid
            #if self.__isWxDrv__()==False:
            #    return
            if bUpdate:
                cdc = wx.ClientDC(self.scwDrawing)
                self.scwDrawing.DoPrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
                dc.BeginDrawing()
                dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
                dc.Clear()
                dc.EndDrawing()
                self.__calcSize__()
                #self.__drawObjects__()
                self.scwDrawing.Refresh()
    def GetDlgData(self):
        if self.dlg is not None:
            self.objs=self.dlg.GetValue('objects')
            self.fScale=self.dlg.GetValue('report_scale')
            self.iFontSize=self.dlg.GetValue('report_font')
            self.bRotate==self.dlg.GetValue('report_rotate')
            max=self.dlg.GetValue('max')
            grid=self.dlg.GetValue('grid')
            bResize=False
            if max!=self.max:
                bResize=True
            if grid!=self.grid:
                bResize=True
            self.max=max
            self.grid=grid
            if bResize:
                return 1
            else:
                return 0
        return -1
            
    def RegisterObject(self,obj):
        self.regObjs.append(obj)
    def Add(self,obj):
        self.objs.append(obj)
    def Del(self,obj,objs=None):
        if objs is None:
            objs=self.objs
        try:
            #i=objs.index(obj)
            objs.remove(obj)
            return 1
        except:
            for o in objs:
                try:
                    print o
                    iRet=self.Del(o,o.objs)
                    if iRet>0:
                        return iRet
                except:
                    pass
            return 0
        return -1
    def SetDriver(self,i):
        if i in self.DRIVERS:
            self.iDriver=i
    def getDriver(self,iDriver=-1):
        if iDriver<0:
            return self.iDriver
        return iDriver
    def getDriverData(self,iDriver=-1):
        if iDriver<0:
            iDriver=self.iDriver
        return self.drvInfo[iDriver]
    def isLayerActive(self,layer):
        return True
    def setActLayer(self,layer,iDriver=-1):
        if iDriver<0:
            iDriver=self.iDriver
        try:
            self.drvInfo[iDriver]['act_layer']
        except:
            self.drvInfo[iDriver]['act_layer']=-1
        if self.drvInfo[iDriver]['act_layer']!=layer:
            self.drvInfo[iDriver]['act_layer']=layer
            if iDriver==self.objBase.DRV_WX:
                if wx.Thread_IsMain()==False:
                    vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
                dc=self.drvInfo[iDriver]['dc']
                if self.IsEnabled():
                    color=self.color[layer%len(self.DFT_COLOR)]
                    brush=self.brush[layer%len(self.DFT_COLOR)]
                    dc.SetBrush(brush)
                else:
                    color=self.colorDis[layer%len(self.DFT_DIS_COLOR)]
                    brush=self.brushDis[layer%len(self.DFT_DIS_COLOR)]
                    dc.SetBrush(brush)
                dc.SetPen(wx.Pen(color,1))
                dc.SetFont(wx.Font(7, wx.SWISS, wx.NORMAL, wx.NORMAL))
                dc.SetTextForeground(color)
            elif iDriver==self.objBase.DRV_LATEX:
                d=self.drvInfo[iDriver]
                c=self.color[layer%len(self.DFT_COLOR)]
                d['color']='\\color[rgb]{%3.2f,%3.2f,%3.2f}'%(float(c.Red()/255.0) ,float(c.Green()/255.0) ,float(c.Blue()/255.0))
    def __isWxDrv__(self):
        try:
            if self.iDriver==self.objBase.DRV_WX:
                return True
        except:
            pass
        return False
    def __calcSize__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.maxWidth=self.grid[0]*self.max[0]
        self.maxHeight=self.grid[1]*self.max[1]
        #if self.__isWxDrv__()==False:
        #    return
        self.scwDrawing.SetVirtualSize((self.maxWidth+5, self.maxHeight+self.grid[1]+5))
        self.scwDrawing.SetScrollRate(20,20)
        if BUFFERED:
            # Initialize the buffer bitmap.  No real DC is needed at this point.
            self.buffer = wx.EmptyBitmap(self.maxWidth+1, self.maxHeight+self.grid[1]+1)
            dc = wx.BufferedDC(None, self.buffer)
            dc.BeginDrawing()
            dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
            dc.Clear()
            self.DoDrawing(dc)
            dc.EndDrawing()
    def Lock(self,flag):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if flag:
            self.Enable(False)
        else:
            self.Enable(True)
    def SetName(self,name):
        self.name=name
    def GetDlg(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.dlg is None:
            self.dlg=vtDrawCanvasDialog(self)
            self.dlg.SetName(self.name)
            self.dlg.Centre()
        return self.dlg
    def CloseDlg(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.dlg is not None:
            self.dlg.Show(False)
    def __setDftColors__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.color=[]
        for c in self.DFT_COLOR:
            self.color.append(wx.Colour(c[0], c[1], c[2]))
        
        self.colorDis=[]
        for c in self.DFT_DIS_COLOR:
            self.color.append(wx.Colour(c[0], c[1], c[2]))
        self.__setDftBrush__()
        self.__setDftPen__()
        
    def __setDftBrush__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.brush=[]
        for c in self.color:
            self.brush.append(wx.Brush(c))
        self.brushDis=[]
        for c in self.colorDis:
            self.brushDis.append(wx.Brush(c))
    def __setDftPen__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.pen=[]
        for c in self.color:
            self.pen.append(wx.Pen(c,1))
        self.penDis=[]
        for c in self.colorDis:
            self.penDis.append(wx.Pen(c,1))
    def calcCoor(self,x,y):
        return x*self.grid[0],y*self.grid[1]
    def calcCoorLatex(self,x,y):
        return x*self.grid[0]*self.fScale,y*self.grid[1]*self.fScale
    def getLatexTextExtent(self,t):
        sFont=self.drvInfo[self.objBase.DRV_LATEX]['font_size']
        if sFont=='\\tiny':
            return (0.2*len(t),0.2)
        elif sFont=='\\scriptsize':
            return (0.2*len(t),0.2)
        elif sFont=='\\small':
            return (0.2*len(t),0.2)
        return (0.2*len(t),0.2)
    def calcPos(self,x,y,flag):
        if flag:
            return round((x/self.grid[0])+0.5),round((y/self.grid[1])+0.5)
        x,y=round((x/self.grid[0])-0.5),round((y/self.grid[1])-0.5)
        if x<0:
            x=0
        if y<0:
            y=0
        return x,y
    def SetXY(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.x, self.y = self.ConvertEventCoords(event)
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        
    def ConvertEventCoords(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        xView, yView = self.scwDrawing.GetViewStart()
        xDelta, yDelta = self.scwDrawing.GetScrollPixelsPerUnit()
        return (event.GetX() + (xView * xDelta),
                event.GetY() + (yView * yDelta))

    def OnRightButtonEvent(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if event.RightDown():
            self.SetFocus()
            self.SetXY(event)
            self.__openDlg__()
    def __setDlgValues__(self):
        dlg=self.GetDlg()
        if dlg is not None:
            dlg.SetValue(objects=self.objs,
                    grid=self.grid,max=self.max,canvas=self,
                    report_scale=self.fScale,
                    report_font=self.iFontSize,
                    report_rotate=self.bRotate)
            return 0
        return -1
    def __getDlgValues__(self):
        dlg=self.GetDlg()
        if dlg is not None:
            self.objs=dlg.GetValue('objects')
            max=dlg.GetValue('max')
            grid=dlg.GetValue('grid')
            self.fScale=self.GetValue('report_scale')
            self.iFontSize=self.GetValue('report_font')
            self.bRotate=self.GetValue('report_rotate')
            bResize=False
            if max!=self.max:
                bResize=True
            if grid!=self.grid:
                bResize=True
            self.max=max
            self.grid=grid
            if bResize:
                return 1
            else:
                return 0
        return -1
    
    def __openDlg__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        iY=self.y/self.grid[1]
        if iY>self.max[1]:
            iY=-1
        iX=self.x/self.grid[0]
        if iX>self.max[0]:
            iX=-1
            
        dlg=self.GetDlg()
        self.__setDlgValues__()
        #dlg.SetValue([],self.objs,self.grid,self.max)
        #dlg.SetCanvas(self)
        dlg.Show(True)
    def ShowObj(self,obj,flag):
        if self.__isWxDrv__()==False:
            return
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.semDraw.acquire(False):
            cdc = wx.ClientDC(self.scwDrawing)
            self.scwDrawing.DoPrepareDC(cdc)
            dc = wx.BufferedDC(cdc, self.buffer)
            dc.BeginDrawing()
            dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
            try:
                self.drvInfo[self.objBase.DRV_WX]['dc']=dc
                self.drvInfo[self.objBase.DRV_WX]['printing']=False
            except:
                traceback.print_exc()
                self.semDraw.release()
                return
            if flag==False:
                self.__updateGrid__(obj,dc,False)
            self.drvInfo[self.objBase.DRV_WX]['act_layer']=-1
            if flag:
                obj.__draw__(self)
            else:
                oiXa,oiYa,oiXe,oiYe=obj.__getExtend__(self)
                for o in self.objs:
                    #o.__drawOnIntersect__(self,obj)
                    o.__drawOnIntersectTup__(self,oiXa,oiYa,oiXe,oiYe)
            self.drvInfo[self.objBase.DRV_WX]['dc']=None
            dc.EndDrawing()
            self.semDraw.release()
        self.__setModified__(True)
        wx.PostEvent(self,vtDrawCanvasChanged(self))
        
    def ShowAll(self):
        if self.__isWxDrv__()==False:
            return
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.semDraw.acquire(False)==False:
            return
        dlg=self.GetDlg()
        if self.GetDlgData()>0:
            if 1==1:
                cdc = wx.ClientDC(self.scwDrawing)
                self.scwDrawing.DoPrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
                dc.BeginDrawing()
                dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
                dc.Clear()
                dc.EndDrawing()
            else:
                self.Clear()
            self.__calcSize__()
            #self.__drawObjects__()
            self.scwDrawing.Refresh()
        else:
            cdc = wx.ClientDC(self.scwDrawing)
            self.scwDrawing.DoPrepareDC(cdc)
            dc = wx.BufferedDC(cdc, self.buffer)
            dc.BeginDrawing()
            dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
            dc.Clear()
            try:
                self.drvInfo[self.objBase.DRV_WX]['dc']=dc
                self.drvInfo[self.objBase.DRV_WX]['printing']=False
            except:
                traceback.print_exc()
                return
            self.drvInfo[self.objBase.DRV_WX]['act_layer']=-1
            self.__drawGrid__(dc,printing=False)
            self.__drawObjects__(dc,printing=False)
            
            self.drvInfo[self.objBase.DRV_WX]['dc']=None
            dc.EndDrawing()
        self.semDraw.release()
        self.__setModified__(True)
        wx.PostEvent(self,vtDrawCanvasChanged(self))
    def Recalc(self,lstGrp,obj):
        pass
    def OnPaint(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.verbose:
            vtLog.vtLogCallDepth(self,'buffer:%d'%BUFFERED)
        if BUFFERED:
            # Create a buffered paint DC.  It will create the real
            # wx.PaintDC and then blit the bitmap to it when dc is
            # deleted.  Since we don't need to draw anything else
            # here that's all there is to it.
            dc = wx.BufferedPaintDC(self.scwDrawing, self.buffer, wx.BUFFER_VIRTUAL_AREA)
        else:
            dc = wx.PaintDC(self.scwDrawing)
            self.PrepareDC(dc)
            # since we're not buffering in this case, we have to
            # paint the whole window, potentially very time consuming.
            self.DoDrawing(dc)
        

    def DoDrawing(self, dc, printing=False):
        #if self.semDraw.acquire(False)==False:
        #    return
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        if self.__isWxDrv__()==False:
            return
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        dc.BeginDrawing()
        self.__drawGrid__(dc,printing=printing)
        self.__drawObjects__(dc,printing=printing)
        #self.__drawLegend__(dc,printing=printing)
        dc.EndDrawing()
        #self.semDraw.release()
        
    def __doDraw__(self):
        #if self.semDraw.acquire(False)==False:
        #    return
        if self.__isWxDrv__()==False:
            return
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        cdc = wx.ClientDC(self.scwDrawing)
        self.scwDrawing.DoPrepareDC(cdc)
        dc = wx.BufferedDC(cdc, self.buffer)
        dc.BeginDrawing()
        dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
        self.__clear__(dc,False)
        self.DoDrawing(dc,False)
        dc.EndDrawing()
        #self.semDraw.release()
        
    def __clear__(self,dc,printing):
        if self.__isWxDrv__()==False:
            return
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        brush=wx.Brush(self.scwDrawing.GetBackgroundColour())
        dc.SetBrush(brush)
        dc.SetPen(wx.Pen(self.scwDrawing.GetBackgroundColour(), 1))
        dc.DrawRectangle(0, 0, self.maxWidth+1, self.maxHeight+1)
        self.__drawGrid__(dc,printing=printing)
        
    def __draw__(self,blk,selected,dc,printing):
        if self.__isWxDrv__()==False:
            return
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.IsEnabled()==False:
            selected=False
        if selected==False:
            if self.IsEnabled():
                color=self.color[blk[-1]%len(self.DFT_COLOR)]
                brush=self.brush[blk[-1]%len(self.DFT_COLOR)]
                dc.SetBrush(brush)
            else:
                color=self.colorDis[blk[-1]%len(self.DFT_DIS_COLOR)]
                brush=self.brushDis[blk[-1]%len(self.DFT_DIS_COLOR)]
                dc.SetBrush(brush)
            dc.SetPen(wx.Pen(color,1))
            dc.DrawLine(blk[0], blk[1], blk[2], blk[3])
        else:
            dc.SetPen(wx.Pen('BLUE', 1))
            dc.SetBrush(wx.WHITE_BRUSH)
            dc.DrawRectangle(blk[0], blk[1], blk[2], blk[3])
        
    def __updateGrid__(self,obj,dc,printing):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        x,y,w,h=obj.GetX(),obj.GetY(),obj.GetWidth(),obj.GetHeight()
        xa,ya,xe,ye=obj.__getExtend__(self)
        x,y,w,h=xa,ya,xe-xa,ye-ya
        dc.SetBrush(wx.Brush(self.scwDrawing.GetBackgroundColour()))
        dc.SetPen(wx.Pen(self.scwDrawing.GetBackgroundColour(),1))
        dc.DrawRectangle(x*self.grid[0],y*self.grid[1],
                    w*self.grid[0],h*self.grid[1])
        self.__setGridColor__(dc,printing)
        for i in range(w+1):
            dc.DrawLine((x+i)*self.grid[0],y*self.grid[1],
                    (x+i)*self.grid[0],(y+h)*self.grid[1])
        for i in range(h+1):
            dc.DrawLine(x*self.grid[0],(y+i)*self.grid[1],
                    (x+w)*self.grid[0],(y+i)*self.grid[1])
        
    def __setGridColor__(self,dc,printing):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.IsEnabled():
            dc.SetPen(wx.Pen(wx.Colour(0xC0, 0xC0, 0xC0), 1))
        else:
            dc.SetPen(wx.Pen(wx.Colour(0xD0, 0xD0, 0xD0), 1))
    def __setGridTextColor__(self,dc,printing):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.IsEnabled():
            dc.SetTextForeground(wx.Colour(0x60, 0x60, 0x60))
        else:
            dc.SetTextForeground(wx.Colour(0xC0, 0xC0, 0xC0))
    def __drawGrid__(self,dc,printing):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        
        self.__setGridColor__(dc,printing)
        for i in range(0,self.max[1]+1):
            y=i*self.grid[1]
            dc.DrawLine(0,y,self.maxWidth,y)
        dc.DrawLine(0,0,0,self.maxHeight)
        y=0
        for i in range(0,self.max[0]+1):
            x=i*self.grid[0]
            dc.DrawLine(x,y,x,self.maxHeight)
    
    def __drawObjects__(self,dc,printing):
        # overload me
        #vtLog.vtLogCallDepth(None,'',verbose=1)
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        try:
            self.drvInfo[self.objBase.DRV_WX]['dc']=dc
            self.drvInfo[self.objBase.DRV_WX]['printing']=printing
        except:
            #traceback.print_exc()
            return
        if 1==0:
            if self.thdDraw.IsRunning():
                return
            self.drvInfo[self.objBase.DRV_WX]['act_layer']=-1
            self.thdDraw.Start(self)
        else:
            #self.semDraw.acquire()
            self.drvInfo[self.objBase.DRV_WX]['act_layer']=-1
            for o in self.objs:
                o.__draw__(self)
            self.drvInfo[self.objBase.DRV_WX]['dc']=None
            #self.semDraw.release()
        pass
        
    def __getCoor__(self,day):
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        try:
            return self.coorDay[day]
        except:
            return (-1,-1)
    def __getOfsDay__(self,day):
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        try:
            return self.ofsDay[day]
        except:
            return -1
    def Clear(self):
        self.ClearInt()
    def ClearInt(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(None,vtLog.DEBUG,
                'Clear',
                origin=self.GetName())
        self.CloseDlg()
        self.node=None
        self.lst=[]
        del self.selectableBlks
        self.selectableBlks=[]
        self.selectedBlk=None
        #print 'clear'
        #vtLog.vtLogCallDepth(None,'',verbose=1)
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        if self.__isWxDrv__()==False:
            return
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if BUFFERED:
            #if self.buffer is None:
                # Initialize the buffer bitmap.  No real DC is needed at this point.
            #    self.buffer = wx.EmptyBitmap(self.maxWidth+2, self.maxHeight+2+self.sumY)
            #    dc = wx.BufferedDC(None, self.buffer)
            #    dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
            #    dc.Clear()
            #    self.DoDrawing(dc)
            #else:
                cdc = wx.ClientDC(self.scwDrawing)
                self.scwDrawing.DoPrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
                dc.BeginDrawing()
                self.__clear__(dc,False)
                dc.EndDrawing()
        self.scwDrawing.Refresh()
        
    def SetValues(self,lst):
        #print 'setvale',lst
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        self.lst=lst
        #if self.__isWxDrv__()==False:
        #    return
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if BUFFERED:
            #if self.buffer is None:
                # Initialize the buffer bitmap.  No real DC is needed at this point.
            #    self.buffer = wx.EmptyBitmap(self.maxWidth+2, self.maxHeight+2+self.sumY)
            #    dc = wx.BufferedDC(None, self.buffer)
            #    dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
            #    dc.Clear()
            #    self.DoDrawing(dc)
            #else:
                cdc = wx.ClientDC(self.scwDrawing)
                self.scwDrawing.DoPrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
                dc.BeginDrawing()
                self.__clear__(dc,False)
                #dc.EndDrawing()
            
                self.__drawGrid__(dc,False)
                self.__drawObjects__(dc,False)
                dc.EndDrawing()
            
        
        self.scwDrawing.Refresh()
        #self.OnPaint()
    def GetValue(self):
        return self.lst
    def SetAutoApply(self,flag):
        self.bAutoApply=flag
    def IsBusy(self):
        #if self.thdAddElements.IsRunning():
        #    return True
        return False
    def IsThreadRunning(self):
        bRunning=False
        #if self.thdAddElements.IsRunning():
        #    bRunning=True
        return bRunning
    def Stop(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(None,vtLog.DEBUG,
                'Stop',
                origin=self.name)
        #if self.thdAddElements.IsRunning():
        #    self.thdAddElements.Stop()
        #while self.IsThreadRunning():
        #    time.sleep(1.0)
        pass
    
    def __setModified__(self,state):
        self.bModified=state
    def SetDoc(self,doc,bNet=False):
        if doc is None:
            return
        if self.doc is not None:
            self.doc.DelConsumer(self)
        self.node=None
        self.doc=doc
        self.doc.AddConsumer(self,self.Clear)
        
    def __setNodeObjs__(self,objs,node):
        for l in self.doc.getChildsAttr(node,'type'):
            sType=self.doc.getAttribute(l,'type')
            for o in self.regObjs:
                if sType==o.GetTypeName():
                    obj=copy.deepcopy(o)
                    obj.SetNode(self.doc,l,self)
                    objs.append(obj)
                    break
    def __getNodeObjs__(self,objs,node):
        for o in objs:
            o.GetNode(self.doc,node,self)
            
    def SetNode(self,node):
        #vtLog.vtLogCallDepth(None,'',verbose=1)
        if self.bModified==True:
            if self.bAutoApply:
                self.GetNode(self.node)
            else:
                # ask
                dlg=wx.MessageDialog(self,_(u'Do you to apply modified data?') ,
                            self.name,
                            wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                if dlg.ShowModal()==wx.ID_YES:
                    self.GetNode()
        
        self.__setModified__(False)
        self.ClearInt()
        self.node=node
        grid=self.grid
        max=self.max
        self.labels=[]
        self.objs=[]
        try:
            n=self.doc.getChild(node,'report')
            try:
                f=float(self.doc.getNodeText(n,'scale'))
            except:
                f=1.0
            self.fScale=f
            try:
                iY=int(self.doc.getNodeText(n,'fontsize'))
            except:
                iY=2
            self.iFontSize=iY
            try:
                iY=int(self.doc.getNodeText(n,'rotate'))
                if iY==1:
                    self.bRotate=True
            except:
                self.bRotate=False
            n=self.doc.getChild(node,'grid')
            try:
                iX=int(self.doc.getNodeText(n,'x'))
            except:
                iX=self.grid[0]
            try:
                iY=int(self.doc.getNodeText(n,'y'))
            except:
                iY=self.grid[1]
            self.grid=(iX,iY)
            
            n=self.doc.getChild(node,'max')
            try:
                iX=int(self.doc.getNodeText(n,'x'))
            except:
                iX=self.max[0]
            try:
                iY=int(self.doc.getNodeText(n,'y'))
            except:
                iY=self.max[1]
            self.max=(iX,iY)
            
            n=self.doc.getChild(node,'scale')
            try:
                iX=int(self.doc.getNodeText(n,'x'))
            except:
                iX=self.scale[0]
            try:
                iY=int(self.doc.getNodeText(n,'y'))
            except:
                iY=self.scale[1]
            self.scale=(iX,iY)
            
            n=self.doc.getChild(node,'objects')
            self.__setNodeObjs__(self.objs,n)
            
            bResize=False
            if max!=self.max:
                bResize=True
            if grid!=self.grid:
                bResize=True
            if bResize:
                self.__calcSize__()
            else:
                self.__doDraw__()
            if self.iDriver==self.objBase.DRV_WX:
                self.scwDrawing.Refresh()
        except:
            traceback.print_exc()
        
    def GetNode(self,node=None):
        if node is None:
            node=self.node
        if node is None:
            self.__setModified__(False)
            return
        try:
            n=self.doc.getChildForced(node,'report')
            self.doc.setNodeText(n,'scale','%4.2f'%self.fScale)
            self.doc.setNodeText(n,'fontsize','%d'%self.iFontSize)
            if self.bRotate:
                self.doc.setNodeText(n,'rotate','1')
            else:
                self.doc.setNodeText(n,'rotate','0')            
            
            n=self.doc.getChildForced(node,'grid')
            self.doc.setNodeText(n,'x','%d'%self.grid[0])
            self.doc.setNodeText(n,'y','%d'%self.grid[1])
            
            n=self.doc.getChildForced(node,'max')
            self.doc.setNodeText(n,'x','%d'%self.max[0])
            self.doc.setNodeText(n,'y','%d'%self.max[1])
            
            n=self.doc.getChildForced(node,'scale')
            self.doc.setNodeText(n,'x','%d'%self.scale[0])
            self.doc.setNodeText(n,'y','%d'%self.scale[1])
            
            n=self.doc.getChild(node,'objects')
            self.doc.deleteNode(n,node)
            n=self.doc.getChildForced(node,'objects')
            self.__getNodeObjs__(self.objs,n)
            
            self.doc.AlignNode(node,iRec=10)
        except:
            traceback.print_exc()
            vtLog.vtLngCallStack(None,vtLog.ERROR,'exception:%s'%traceback.format_exc())
        self.__setModified__(False)
    
    #def OnSize(self, event):
    #    if self.verbose:
    #        vtLog.vtLogCallDepth(self,'')
    #    s=event.GetSize()
    #    self.scwDrawing.SetSize(s)
    #    event.Skip()
    def Enable(self,flag):
        #if self.__isWxDrv__()==False:
        #    return
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        wx.Panel.Enable(self,flag)
        if BUFFERED:
            #if self.buffer is None:
                # Initialize the buffer bitmap.  No real DC is needed at this point.
            #    self.buffer = wx.EmptyBitmap(self.maxWidth+2, self.maxHeight+2+self.sumY)
            #    dc = wx.BufferedDC(None, self.buffer)
            #    dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
            #    dc.Clear()
            #    self.DoDrawing(dc)
            #else:
                cdc = wx.ClientDC(self.scwDrawing)
                self.scwDrawing.DoPrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
                dc.BeginDrawing()
                self.__clear__(dc,False)
                self.__drawGrid__(dc,False)
                self.__drawObjects__(dc,False)
                dc.EndDrawing()
    def GenTexPic(self):
        #if self.semDraw.acquire(False)==False:
        #    return
        try:
            self.drvInfo[self.objBase.DRV_LATEX]['color']=''
            self.drvInfo[self.objBase.DRV_LATEX]['act_layer']=-1
        except:
            traceback.print_exc()
            #self.semDraw.release()
            return
        if self.iFontSize==0:
            sFontSize='\\tiny'
        elif self.iFontSize==1:
            sFontSize='\\scriptsize'
        elif self.iFontSize==3:
            sFontSize='\\scriptsize'
        elif self.iFontSize==4:
            sFontSize='\\scriptsize'
        else:
            sFontSize='\\small'
        xMax=0
        yMax=0
        xMin=sys.maxint
        yMin=sys.maxint
        scaleX=self.fScale
        scaleY=self.fScale
        strs=[]
        xMin,yMin=0,0
        xMax,yMax=self.maxWidth,self.maxHeight
        xMin*=scaleX
        yMin*=scaleY
        xMax*=scaleX
        yMax*=scaleY
        self.drvInfo[self.objBase.DRV_LATEX]['y_max']=yMax
        self.drvInfo[self.objBase.DRV_LATEX]['font_size']=sFontSize
        strs=['  \\begin{picture}(%d,%d)'%(xMax,yMax)]
        self.drvInfo[self.objBase.DRV_LATEX]['strs']=strs
        #for dict in self.lst:
        #    rng=dict['range']
        #    oY=rng[1]*self.grid[1]
        #    dY=(rng[1]-rng[0])*self.grid[1]
        #    vals=dict['values']
        #    xa,ya=-1,-1
        #    for x,y in vals:
        #        if x<0:
        #            xa,ya=-1,-1
        #            continue
        #        y=self.ofs[1]+oY-(y*dY)
        #        x=self.ofs[0]+self.grid[0]*x
        #        if xa>=0:
        #            l=[xa,ya,x,y,i]
        #            self.__draw__(l,False,dc,False)
        #            tup=vtRepLatex.lineFitPict2(xa,ya,x,y)
        #            strs.append('    \\put(%4.1f,%4.1f){%s\\line(%d,%d){%4.1f}}'%tup)
        #        xa,ya=x,y
        #    i+=1
        for o in self.objs:
            o.__draw__(self,iDriver=self.objBase.DRV_LATEX)
        strs.append('  \\end{picture}')
        return string.join(strs,os.linesep)
    def GenTexFigFloat(self):
        if self.node is not None:
            par=self.doc.getParent(self.node)
            sHier=vtXmlHierarchy.getTagNamesWithRel(self.doc,self.baseNode,self.relNode,par)+ ' ' + \
                        self.doc.getNodeText(par,'name') + ' '+_(u'diagram')
            sHierFull=vtXmlHierarchy.getTagNames(self.doc,self.baseNode,None,par)+ ' ' + \
                        self.doc.getNodeText(par,'name') + ' '+_(u'diagram')
        else:
            sHier=''
            sHierFull=''
        sCaption='  \\caption{%s}'%vtRepLatex.texReplace(sHier)
        sRef='  \\label{fig:%s}'%vtRepLatex.texReplace4Lbl(sHierFull)
        fig=['\\begin{figure}[!hb]',self.GenTexPic(),sCaption,sRef,'\\end{figure}']
        return string.join(fig,os.linesep)
    def GenTexFig(self):
        if self.node is not None:
            par=self.doc.getParent(self.node)
            sHier=vtXmlHierarchy.getTagNamesWithRel(self.doc,self.baseNode,self.relNode,par)+ ' ' + \
                        self.doc.getNodeText(par,'name') + ' '+_(u'diagram')
            sHierFull=vtXmlHierarchy.getTagNames(self.doc,self.baseNode,None,par)+ ' ' + \
                        self.doc.getNodeText(par,'name') + ' '+_(u'diagram')
        else:
            sHier=''
            sHierFull=''
        sCaption='  \\caption{%s}'%vtRepLatex.texReplace(sHier)
        sRef='  \\label{fig:%s}'%vtRepLatex.texReplace4Lbl(sHierFull)
        fig=['\\begin{center}','\\begin{longtable}{c}',self.GenTexPic(),'  \\\\',sCaption,sRef,'\\end{longtable}','\\end{center}']
        return string.join(fig,os.linesep)
    def GenTexFigRef(self,node,baseNode):
        if node is not None:
            par=self.doc.getParent(node)
            sHier=vtXmlHierarchy.getTagNames(self.doc,baseNode,None,par)+ ' ' + \
                        self.doc.getNodeText(par,'name') + ' '+_(u'diagram')
            sHier=vtRepLatex.texReplace4Lbl(sHier)
            sRef='\\ref{fig:%s} \#\\pageref{fig:%s}'%(sHier,sHier)
            return sRef
        return ''
    def SetBaseNode(self,node):
        self.baseNode=node
    def SetRelNode(self,node):
        self.relNode=node

    def OnCbEditButton(self, event):
        event.Skip()

    def OnCbLayerButton(self, event):
        event.Skip()

    def OnCbDelButton(self, event):
        event.Skip()
        
