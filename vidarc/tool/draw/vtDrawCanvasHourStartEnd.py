#----------------------------------------------------------------------------
# Name:         vtDrawCanvasHour.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060221
# CVS-ID:       $Id: vtDrawCanvasHourStartEnd.py,v 1.1 2006/02/22 11:24:58 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.draw.vtDrawCanvasRectangleFilled import *

class vtDrawCanvasHourStartEnd(vtDrawCanvasRectangleFilled):
    def __init__(self,id=-1,name='',start=0,end=0,x=0,y=0,w=0,h=0,layer=0):
        vtDrawCanvasRectangleFilled.__init__(self,id=id,name=name,x=x,y=y,w=w,h=h,layer=layer)
        self.iStart=start
        self.iEnd=end
    def GetStart(self):
        return self.iStart
    def SetStart(self,iVal):
        try:
            self.iStart=int(iVal)
        except:
            self.iStart=0
        #self.__checkMin__()
    def GetEnd(self):
        return self.iEnd
    def SetEnd(self,iVal):
        try:
            self.iEnd=int(iVal)
        except:
            self.iEnd=0
        #self.__checkMin__()
    def __checkMin__(self):
        if self.iStart<0:
            self.iStart=0
        if self.iEnd<0:
            self.iEnd=0
        if self.iStart>self.iEnd:
            self.iStart,self.iEnd=self.iEnd,self.iStart
    def GetTypeName(self):
        # replace me
        return 'hourStartEnd'
    def GetAttr(self,idx):
        if idx==6:
            return 'start',self.GetStart(),'%d',1
        elif idx==7:
            return 'end',self.GetEnd(),'%d',1
        else:
            return vtDrawCanvasRectangleFilled.GetAttr(self,idx)
    def SetAttr(self,idx,val):
        if idx==6:
            self.SetStart(val)
            return 0
        elif idx==7:
            self.SetEnd(val)
            return 0
        else:
            return vtDrawCanvasRectangleFilled.SetAttr(self,idx)
        
    def __draw__(self,canvas,aX=0,aY=0,aW=-1,aH=-1,iDriver=-1):
        self.__checkMin__()
        
        if self.bVisible==False:
            return
        drvInfo=canvas.getDriverData(iDriver)
        drv=canvas.getDriver(iDriver)
        canvas.setActLayer(self.iLayer,self.bActive,iDriver)
        if drv==self.DRV_WX:
            dc=drvInfo['dc']
            if aW>=0 and aH>=0:
                ax,ay=canvas.calcCoor(aX,aY)
                aw,ah=canvas.calcCoor(aW,aH)
                dc.SetClippingRegion(ax,ay,aw+1,ah+1)
            else:
                ax,ay=canvas.calcCoor(self.iX,self.iY)
                aw,ah=canvas.calcCoor(self.iWidth,self.iHeight)
                dc.SetClippingRegion(ax,ay,aw+1,ah+1)
            x,y=canvas.calcCoor(self.iX+aX,self.iY+aY)
            w,h=canvas.calcCoor(self.iWidth,self.iHeight)
            iS=int(w*((self.iStart/60.0)))
            if self.iEnd==self.iStart:
                iE=0
            else:
                iE=round(w*(((self.iEnd-self.iStart)/60.0))+0.5)
            if iS<0:
                iS=0
            if iE<0:
                iE=0
            #w=int(w*((self.iStart/60.0)/100.0))
            #print w,self.fPercentage,self.fPercentage/100.0
            dc.DrawRectangle(x+iS,y,iE,h)
            if aW>=0 and aH>=0:
                dc.DestroyClippingRegion()
            else:
                dc.DestroyClippingRegion()
        elif drv==self.DRV_LATEX:
            if aW>=0 and aH>=0:
                ax,ay=canvas.calcCoorLatex(aX,aY)
                aw,ah=canvas.calcCoorLatex(aW,aH)
            x,y=canvas.calcCoorLatex(self.iX+aX,self.iY+aY)
            w,h=canvas.calcCoorLatex(self.iWidth,self.iHeight)
            w=int(w*(self.fPercentage)/100.0)
            
            y=drvInfo['y_max']-y-h
            c=drvInfo['color']
            strs=drvInfo['strs']
            tup=vtRepLatex.lineFitPict2(x,y,x+w,y)
            strs.append('    \\put(%4.1f,%4.1f){%s\\line(%d,%d){%4.1f}}'%(tup[0],tup[1],c,tup[2],tup[3],tup[4]))
            tup=vtRepLatex.lineFitPict2(x+w,y,x+w,y+h)
            strs.append('    \\put(%4.1f,%4.1f){%s\\line(%d,%d){%4.1f}}'%(tup[0],tup[1],c,tup[2],tup[3],tup[4]))
            tup=vtRepLatex.lineFitPict2(x,y+h,x+w,y+h)
            strs.append('    \\put(%4.1f,%4.1f){%s\\line(%d,%d){%4.1f}}'%(tup[0],tup[1],c,tup[2],tup[3],tup[4]))
            tup=vtRepLatex.lineFitPict2(x,y,x,y+h)
            strs.append('    \\put(%4.1f,%4.1f){%s\\line(%d,%d){%4.1f}}'%(tup[0],tup[1],c,tup[2],tup[3],tup[4]))
            pass
        
        vtDrawCanvasObjectBase.__drawSelect__(self,canvas,aX=aX,aY=aY,aW=aW,aH=aH,iDriver=iDriver)

