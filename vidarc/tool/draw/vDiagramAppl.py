#!/usr/bin/env python
#Boa:App:BoaApp
#----------------------------------------------------------------------------
# Name:         vDiagramAppl.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vDiagramAppl.py,v 1.2 2007/07/28 14:43:29 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import os,sys,getopt
import vDiagramFrame
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.tool.log.vtLog as vtLog

modules ={u'vDiagramFrame': [1,
                           'Main frame of Application',
                           u'vDiagramFrame.py']}

class BoaApp(wx.App):
    def OnInit(self):
        wx.InitAllImageHandlers()
        self.main = vDiagramFrame.create(None)
        self.main.Show()
        self.SetTopWindow(self.main)
        return True
    def OpenFile(self,fn):
        self.main.OpenFile(fn)
    
def showHelp():
    print _('help')
    print sys.version
    print "  ",_("valid flags:")
    print "  ",_("-f <filename>")
    print "  ",_("--file <filename>")
    print "        ",_("open the <filename> at start")
    print "  ","-h"
    print "  ",_("--help")
    print "        ",_("show this screen")
    print "  ","-l"
    print "  ",_("--lang <language id according ISO639>")

def main():
    optshort,optlong='l:f:c:h',['lang=','file=','help']
    vtLgBase.initAppl(optshort,optlong,'vDiagram')
    vtLog.vtLngInit('vDiagramAppl','vDiagramAppl.log',vtLog.DEBUG)
    
    fn=None
    try:
        optlist , args = getopt.getopt(sys.argv[1:],'l:f:h',['lang=','file=','help'])
        for o in optlist:
            if o[0] in ['-h','--help']:
                showHelp()
            
                return
            if o[0] in ['--file','-f']:
                fn=o[1]
    except:
        showHelp()
    
    application = BoaApp(0)
    if fn is not None:
        application.OpenFile(fn)
    application.MainLoop()

if __name__ == '__main__':
    main()
