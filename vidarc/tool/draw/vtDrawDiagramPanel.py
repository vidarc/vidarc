#Boa:FramePanel:vtDrawDiagramPanel
#----------------------------------------------------------------------------
# Name:         vtDrawDiagramPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtDrawDiagramPanel.py,v 1.6 2010/04/10 12:32:14 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors
import traceback,time
import string,math

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.tool.draw.vtDrawDiagramDialog import *

    import vidarc.tool.report.vRepLatex as vtRepLatex
    import vidarc.tool.xml.vtXmlDom as vtXmlDom
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel
    from vidarc.tool.xml.vtXmlDomConsumerLang import vtXmlDomConsumerLang
    import vidarc.tool.xml.vtXmlHierarchy as vtXmlHierarchy
    import vidarc.tool.log.vtLog as vtLog
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

BUFFERED=1

# defined event for vgpXmlTree item selected
wxEVT_VTDRAW_DIAGRAM_CHANGED=wx.NewEventType()
def EVT_VTDRAW_DIAGRAM_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VTDRAW_DIAGRAM_CHANGED,func)
class vtDrawDiagramChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VTDRAW_DIAGRAM_CHANGED(<widget_name>, self.OnInfoChanged)
    """

    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VTDRAW_DIAGRAM_CHANGED)
        self.obj=obj
    def GetObject(self):
        return self.obj

class vtDrawDiagramPanel(wx.Panel,vtXmlNodePanel,vtXmlDomConsumerLang):
    DFT_COLOR=[
        (255, 154, 000),
        (255, 207, 000),
        (102, 205, 170),
        (136, 136, 102),
        (144, 208, 144),
        (218, 165,  32),
        (226, 126, 226)
        ]
    DFT_DIS_COLOR=[
        (167, 100, 000),
        (167, 136, 000),
        ( 67, 135, 112),
        (116, 130, 147),
        (100, 166, 100),
        (150, 114,  22),
        (147,  82, 147)
        ]
        
    def _init_ctrls(self, prnt, id, pos, size, style,name=u'vtDrawDiagramPanel'):
        wx.Panel.__init__(self, id=id,
              name=name, parent=prnt, pos=pos,size=size, style=style)
        self.Bind(wx.EVT_SIZE, self.OnSize)

        self.scwDrawing = wx.ScrolledWindow(id=wx.NewId(),
              name=u'scwDiagram', parent=self, pos=wx.Point(0, 0),
              size=size, style=wx.HSCROLL | wx.VSCROLL)
        self.scwDrawing.SetAutoLayout(False)

    def __init__(self, parent, 
                id=wx.NewId(), pos=wx.DefaultPosition, size=wx.Size(100, 50), 
                style=wx.TAB_TRAVERSAL, name=u'vtDrawDiagramPanel',
                max=(10,10),  
                labels=None, ofs=(50,10), grid=(15, 15)):
        self._init_ctrls(parent,id,pos,size,style,name)
        vtXmlNodePanel.__init__(self,lWidgets=[],
                lEvent=[])
        vtXmlDomConsumerLang.__init__(self)
        #self.doc=None
        #self.node=None
        self.baseNode=None
        self.dlg=None
        self.buffer=None
        self.name=u'vtDrawDiagram'
        self.fScale=1.0
        self.iFontSize=2
        self.bRotate=False
        
        self.bAutoApply=False
        self.bModified=False
    
        self.verbose=VERBOSE
    
        self.__setDftColors__()
        self.labels=[[],[]]
        self.lst=[]
        self.drawing=False
        self.selectableBlks=[]
        self.selectedBlk=None
        
        self.values=None
        self.labels=labels
        self.grid=grid
        self.max=max
        self.ofs=ofs
        
        self.__calcSize__()
        self.scwDrawing.Bind(wx.EVT_RIGHT_DOWN,self.OnRightButtonEvent)
        self.scwDrawing.Bind(wx.EVT_PAINT, self.OnPaint)
    def __calcSize__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'grid:%s;max:%s'%\
                    (vtLog.pformat(self.grid),vtLog.pformat(self.max)),self)
        self.maxWidth=self.grid[0]*self.max[0]
        self.maxHeight=self.grid[1]*self.max[1]
        self.scwDrawing.SetVirtualSize((self.maxWidth+self.ofs[0]+5, self.maxHeight+self.ofs[1]+self.grid[1]+5))
        self.scwDrawing.SetScrollRate(20,20)
        if BUFFERED:
            # Initialize the buffer bitmap.  No real DC is needed at this point.
            self.buffer = wx.EmptyBitmap(self.maxWidth+self.ofs[0]+1, self.maxHeight+self.ofs[1]+self.grid[1]+1)
            dc = wx.BufferedDC(None, self.buffer)
            dc.BeginDrawing()
            dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
            dc.Clear()
            self.DoDrawing(dc)
            dc.EndDrawing()
    def __Lock__(self,flag):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if flag:
            self.Enable(False)
        else:
            self.Enable(True)
    def SetName(self,name):
        self.name=name
    def GetDlg(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.dlg is None:
            self.dlg=vtDrawDiagramDialog(self)
            self.dlg.SetName(self.name)
        return self.dlg
    def __setDftColors__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.color=[]
        for c in self.DFT_COLOR:
            self.color.append(wx.Colour(c[0], c[1], c[2]))
        
        self.colorDis=[]
        for c in self.DFT_DIS_COLOR:
            self.color.append(wx.Colour(c[0], c[1], c[2]))
        self.__setDftBrush__()
        self.__setDftPen__()
        
    def __setDftBrush__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.brush=[]
        for c in self.color:
            self.brush.append(wx.Brush(c))
        self.brushDis=[]
        for c in self.colorDis:
            self.brushDis.append(wx.Brush(c))
    def __setDftPen__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.pen=[]
        for c in self.color:
            self.pen.append(wx.Pen(c,1))
        self.penDis=[]
        for c in self.colorDis:
            self.penDis.append(wx.Pen(c,1))
    def SetXY(self, event):
        self.x, self.y = self.ConvertEventCoords(event)
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        
    def ConvertEventCoords(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        xView, yView = self.scwDrawing.GetViewStart()
        xDelta, yDelta = self.scwDrawing.GetScrollPixelsPerUnit()
        return (event.GetX() + (xView * xDelta),
                event.GetY() + (yView * yDelta))
    def SelectValueOld(self,day,hr):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        if BUFFERED:
            # If doing buffered drawing, create the buffered DC, giving it
            # it a real DC to blit to when done.
            cdc = wx.ClientDC(self.scwDrawing)
            self.scwDrawing.DoPrepareDC(cdc)
            dc = wx.BufferedDC(cdc, self.buffer)
            dc.BeginDrawing()
        else:
            dc = wx.ClientDC(self)
            self.PrepareDC(dc)
        
        if self.selectedBlk is not None:
            # paint old
            self.__drawHour__(self.selectedBlk,False,dc,False)
            #oBlk=self.selectedBlk
            #color=self.color[oBlk[-2]%7]
            #brush=self.brush[oBlk[-2]%7]
            #dc.SetBrush(brush)
            #dc.DrawRectangle(oBlk[0], oBlk[1], 
            #                    oBlk[2]-oBlk[0], oBlk[3]-oBlk[1])
        self.selectedBlk=None
        blk=self.__getSelectableBlk__(day,hr)
        if blk is not None:
            self.__drawHour__(blk,True,dc,False)
            #dc.SetPen(wx.Pen('BLUE', 1))
            #dc.SetBrush(wx.LIGHT_GREY_BRUSH)
            #dc.DrawRectangle(blk[0], blk[1], 
            #                    blk[2]-blk[0], blk[3]-blk[1])
            self.selectedBlk=blk
            wx.PostEvent(self,vtDrawDiagramChanged(self,day,hr))
        #self.selectedBlk=node
        self.__drawGrid__(dc,False)
        self.__drawGridText__(dc,False)
        dc.EndDrawing()
    def __getSelectableBlkOld__(self,x,y):
        for blks in self.selectableBlks:
            if (x>blks[0]) and (x<blks[2]):
                if (y>blks[1]) and (y<blks[3]):
                    return blks
        return None
    def __getSelectableIdxOld__(self,day,hour):
        idx=0
        for tup in self.selectableBlks:
            if tup[-2]==day:
                if tup[-1]==hour:
                    return idx
            idx+=1
        return -1
    def __setHourOld__(self,iDay,hour):
        try:
            day=self.lst[iDay]
            idx=0
            idxSel=-1
            for hr in day:
                if (hour[0]>=hr[0]) and (hour[0]<=hr[1]):
                    # start inside
                    if hour[1]<=hr[1]:
                        hour=None
                        break
                    else:
                        idxSel=self.__getSelectableIdx__(iDay,hr)
                        if self.verbose:
                            vtLog.vtLogCallDepth(self,'%s'%repr(idxSel),callstack=False)
                        hr[1]=hour[1]
                        hour=hr
                        #break
                if (hour[1]>=hr[0]) and (hour[1]<=hr[1]):
                    #end inside
                    if hour[0]<hr[0]:
                        hr[0]=hour[0]
                        idxSel=self.__getSelectableIdx__(iDay,hr)
                        hr[0]=hour[0]
                        hour=hr
                        #break
            if hour is not None:
                rect,sum=self.__getHourRect__(iDay,hour)
                if idxSel>=0:
                    r=self.selectableBlks[idxSel]
                    if self.verbose:
                        vtLog.vtLogCallDepth(self,'%s'%repr(r),callstack=False)
                    r[0]=rect[0]
                    r[1]=rect[1]
                    r[2]=rect[0]+rect[2]
                    r[3]=rect[1]+rect[3]
                    if self.verbose:
                        vtLog.vtLogCallDepth(self,'%s'%repr(r),callstack=False)
                    for i in range(len(day)-1):
                        hr1=day[i]
                        hr2=day[i+1]
                        if hr1[0]==hr2[0]:
                            if hr1[1]<hr2[1]:
                                idxSel=self.__getSelectableIdx__(iDay,hr1)
                                if self.verbose:
                                    vtLog.vtLogCallDepth(self,'hr1:%s hr2:%s idx:%d'%(hr1,hr2,idxSel),callstack=False)
                                self.selectableBlks=self.selectableBlks[:idxSel]+self.selectableBlks[idxSel+1:]
                                day.remove(hr1)
                                break
                    return r
                else:
                    day.append(hour)
                    def compFunc(a,b):
                        return cmp(a[0],b[0])
                    day.sort(compFunc)
                    self.lst[iDay]=day
                    r=[rect[0], rect[1], rect[0] + rect[2], rect[1] + rect[3] , iDay,hour]
                    self.selectableBlks.append(r)
                    if self.verbose:
                        vtLog.vtLogCallDepth(self,'%s'%repr(r),callstack=False)
                    return r
        except Exception,list:
            print list
            return None
    def OnRightButtonEvent(self, event):
        if event.RightDown():
            self.SetFocus()
            self.SetXY(event)
            self.__openDlg__()
    def __setDlgValues__(self):
        dlg=self.GetDlg()
        if dlg is not None:
            dlg.SetValue(labels=self.labels,list=self.lst,
                    grid=self.grid,max=self.max,
                    report_scale=self.fScale,
                    report_font=self.iFontSize,
                    report_rotate=self.bRotate)
            return 0
        return -1
    def __getDlgValues__(self):
        dlg=self.GetDlg()
        if dlg is not None:
            self.lst=dlg.GetValue('list')
            self.labels=dlg.GetValue('labels')
            max=dlg.GetValue('max')
            grid=dlg.GetValue('grid')
            self.fScale=dlg.GetValue('report_scale')
            self.iFontSize=dlg.GetValue('report_font')
            self.bRotate=dlg.GetValue('report_rotate')
            bResize=False
            if max!=self.max:
                bResize=True
            if grid!=self.grid:
                bResize=True
            self.max=max
            self.grid=grid
            if bResize:
                return 1
            else:
                return 0
        return -1
    def __openDlg__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        iY=(self.y-self.ofs[1])/self.grid[1]
        if iY>self.max[1]:
            iY=-1
        iX=(self.x-self.ofs[0])/self.grid[0]
        if iX>self.max[0]:
            iX=-1
            
        dlg=self.GetDlg()
        dlg.Centre()
        self.__setDlgValues__()
        if dlg.ShowModal()>0:
            if self.__getDlgValues__()>0:
                if 1==1:
                    cdc = wx.ClientDC(self.scwDrawing)
                    self.scwDrawing.DoPrepareDC(cdc)
                    dc = wx.BufferedDC(cdc, self.buffer)
                    dc.BeginDrawing()
                    dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
                    dc.Clear()
                    dc.EndDrawing()
                else:
                    self.Clear()
                self.__calcSize__()
                #self.__drawValues__()
                self.scwDrawing.Refresh()
            else:
                cdc = wx.ClientDC(self.scwDrawing)
                self.scwDrawing.DoPrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
                dc.BeginDrawing()
                dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
                dc.Clear()
                self.__drawGrid__(dc,printing=False)
                self.__drawGridText__(dc,printing=False)
                self.__drawValues__(dc,printing=False)
                
                dc.EndDrawing()
            self.__setModified__(True)
            wx.PostEvent(self,vtDrawDiagramChanged(self))
                
    def OnPaint(self, event):
        if self.verbose:
            vtLog.vtLogCallDepth(self,'buffer:%d'%BUFFERED)
        if BUFFERED:
            # Create a buffered paint DC.  It will create the real
            # wx.PaintDC and then blit the bitmap to it when dc is
            # deleted.  Since we don't need to draw anything else
            # here that's all there is to it.
            dc = wx.BufferedPaintDC(self.scwDrawing, self.buffer, wx.BUFFER_VIRTUAL_AREA)
        else:
            dc = wx.PaintDC(self.scwDrawing)
            self.PrepareDC(dc)
            # since we're not buffering in this case, we have to
            # paint the whole window, potentially very time consuming.
            self.DoDrawing(dc)
        

    def DoDrawing(self, dc, printing=False):
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        dc.BeginDrawing()
        self.__drawGrid__(dc,printing=printing)
        self.__drawGridText__(dc,printing=printing)
        self.__drawValues__(dc,printing=printing)
        #self.__drawLegend__(dc,printing=printing)
        dc.EndDrawing()

    
    def __clear__(self,dc,printing):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        brush=wx.Brush(self.scwDrawing.GetBackgroundColour())
        dc.SetBrush(brush)
        dc.SetPen(wx.Pen(self.scwDrawing.GetBackgroundColour(), 1))
        dc.DrawRectangle(0, 0, self.maxWidth+self.ofs[0]+1, self.maxHeight+1+self.ofs[1])
        self.__drawGrid__(dc,printing=printing)
        self.__drawGridText__(dc,printing=printing)
        
    def __draw__(self,blk,selected,dc,printing):
        if self.IsEnabled()==False:
            selected=False
        if selected==False:
            if self.IsEnabled():
                color=self.color[blk[-1]%len(self.DFT_COLOR)]
                brush=self.brush[blk[-1]%len(self.DFT_COLOR)]
                dc.SetBrush(brush)
            else:
                color=self.colorDis[blk[-1]%len(self.DFT_DIS_COLOR)]
                brush=self.brushDis[blk[-1]%len(self.DFT_DIS_COLOR)]
                dc.SetBrush(brush)
            dc.SetPen(wx.Pen(color,1))
            dc.DrawLine(blk[0], blk[1], blk[2], blk[3])
        else:
            dc.SetPen(wx.Pen('BLUE', 1))
            dc.SetBrush(wx.WHITE_BRUSH)
            dc.DrawRectangle(blk[0], blk[1], blk[2], blk[3])
        
    def __updateGrid__(self,iDay,hour,dc,printing):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        iStartHr=int(hour[0][0:2])
        iEndHr=int(hour[1][0:2])
        iEndMin=int(hour[1][2:4])
        if iEndMin>0:
            iEndHr+=1
        y=iDay*self.gridY
        xa=iStartHr*self.gridX+self.ofsX
        xe=iEndHr*self.gridX+self.ofsX
        self.__setGridColor__(dc,printing)
        #dc.SetPen(wx.Pen('VIOLET', 1))
        dc.SetFont(wx.Font(7, wx.SWISS, wx.NORMAL, wx.NORMAL))
        self.__setGridTextColor__(dc,printing)
        #dc.SetTextForeground(wx.Colour(0x60, 0x60, 0x60))
        dc.DrawLine(xa,y,xe,y)
        dc.DrawLine(xa,y,xe,y)
        gXh=self.gridX/2
        gYh=self.gridY/2
        for iHr in range(iStartHr,iEndHr):
            x=iHr*self.gridX+self.ofsX
            dc.DrawLine(x,y,x,y+self.gridY)
            sHr='%02d'%iHr
            te = dc.GetTextExtent(sHr)
            x=iHr*self.gridX + gXh - te[0]/2 + 2+ self.ofsX
            y=iDay*self.gridY + gYh - te[1]/2 + 1
            #if (x>=xLim) and (y>=yLim):
            dc.DrawText(sHr, x, y)
        
    def __setGridColor__(self,dc,printing):
        if self.IsEnabled():
            dc.SetPen(wx.Pen(wx.Colour(0xC0, 0xC0, 0xC0), 1))
        else:
            dc.SetPen(wx.Pen(wx.Colour(0xD0, 0xD0, 0xD0), 1))
    def __setGridTextColor__(self,dc,printing):
        if self.IsEnabled():
            dc.SetTextForeground(wx.Colour(0x60, 0x60, 0x60))
        else:
            dc.SetTextForeground(wx.Colour(0xC0, 0xC0, 0xC0))
    def __drawGrid__(self,dc,printing):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        
        self.__setGridColor__(dc,printing)
        for i in range(0,self.max[1]+1):
            y=i*self.grid[1]+self.ofs[1]
            dc.DrawLine(self.ofs[0],y,self.ofs[0]+self.maxWidth,y)
        dc.DrawLine(self.ofs[0],self.ofs[1],self.ofs[0],self.ofs[1]+self.maxHeight)
        y=self.ofs[1]
        for i in range(0,self.max[0]+1):
            x=i*self.grid[0]+self.ofs[0]
            dc.DrawLine(x,y,x,self.ofs[1]+self.maxHeight)
        if 1==0:
            y=0
            gXh=self.gridX/2
            gYh=self.gridY/2
            
            self.ofsDay={}
            for w in days:
                y=y+self.gridY
                i=0
                for d in w:
                    if d>0:
                        sDay="%d"%d
                        self.coorDay[d]=(i*self.gridX,y)
                        self.ofsDay[d]=(0,0)
                    i=i+1
        pass
    def __drawGridText__(self,dc,printing):
        #xScroll,yScroll=self.scwDrawing.GetScrollPixelsPerUnit()
        #xView,yView=self.scwDrawing.GetViewStart()
        #xLim=xView*xScroll
        #yLim=yView*yScroll
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
            #vtLog.vtLogCallDepth(self,'view:(%d,%d)'%(xView,yView))
            #vtLog.vtLogCallDepth(self,'lim:(%d,%d)'%(xLim,yLim),callstack=False)
        #dc.SetPen(wx.Pen('BLACK', 1))
        self.__setGridColor__(dc,printing)
        y=0
        gXh=self.grid[0]/2
        gYh=self.grid[1]/2
        dc.SetFont(wx.Font(7, wx.SWISS, wx.NORMAL, wx.NORMAL))
        self.__setGridTextColor__(dc,printing)
        if self.labels is not None:
            i=0
            lst=self.labels[0]
            for i,sLbl in lst:
                te = dc.GetTextExtent(sLbl)
                x=self.ofs[0]-te[0]-2
                y=self.ofs[1]+self.maxHeight-i*self.grid[1]-te[1]/2+1
                #if (x>=xLim) and (y>=yLim):
                dc.DrawText(sLbl, x, y)
            lst=self.labels[1]
            y=self.maxHeight+self.ofs[1]
            for i,sLbl in lst:
                te = dc.GetTextExtent(sLbl)
                x=self.ofs[0]+i*self.grid[0]-te[0]/2+1
                #if (x>=xLim) and (y>=yLim):
                dc.DrawText(sLbl, x, y)
        if 1==0:
            for h in range(self.hours):
                sHr='%02d'%h
                te = dc.GetTextExtent(sHr)
                for d in range(self.days):
                    x=h*self.gridX + gXh - te[0]/2 + 2+ self.ofsX
                    y=d*self.gridY + gYh - te[1]/2 + 1
                    #if (x>=xLim) and (y>=yLim):
                    dc.DrawText(sHr, x, y)
    
    def __drawValues__(self,dc,printing):
        #vtLog.vtLogCallDepth(None,'',verbose=1)
        # overload me
        pass
        
    def __getCoor__(self,day):
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        try:
            return self.coorDay[day]
        except:
            return (-1,-1)
    def __getOfsDay__(self,day):
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        try:
            return self.ofsDay[day]
        except:
            return -1
    def ClearLang(self):
        pass
    def UpdateLang(self):
        pass
    def ClearInt(self):
        self.lst=[]
        del self.selectableBlks
        self.selectableBlks=[]
        self.selectedBlk=None
    def __Clear__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.node=None
        #print 'clear'
        #vtLog.vtLogCallDepth(None,'',verbose=1)
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        if BUFFERED:
            #if self.buffer is None:
                # Initialize the buffer bitmap.  No real DC is needed at this point.
            #    self.buffer = wx.EmptyBitmap(self.maxWidth+2, self.maxHeight+2+self.sumY)
            #    dc = wx.BufferedDC(None, self.buffer)
            #    dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
            #    dc.Clear()
            #    self.DoDrawing(dc)
            #else:
                cdc = wx.ClientDC(self.scwDrawing)
                self.scwDrawing.DoPrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
                dc.BeginDrawing()
                self.__clear__(dc,False)
                dc.EndDrawing()
        self.scwDrawing.Refresh()
        
    def SetValues(self,lst):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #print 'setvale',lst
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        self.lst=lst
        if BUFFERED:
            #if self.buffer is None:
                # Initialize the buffer bitmap.  No real DC is needed at this point.
            #    self.buffer = wx.EmptyBitmap(self.maxWidth+2, self.maxHeight+2+self.sumY)
            #    dc = wx.BufferedDC(None, self.buffer)
            #    dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
            #    dc.Clear()
            #    self.DoDrawing(dc)
            #else:
                cdc = wx.ClientDC(self.scwDrawing)
                self.scwDrawing.DoPrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
                dc.BeginDrawing()
                self.__clear__(dc,False)
                #dc.EndDrawing()
            
                self.__drawGrid__(dc,False)
                self.__drawGridText__(dc,False)
                self.__drawValues__(dc,False)
                dc.EndDrawing()
            
        
        self.scwDrawing.Refresh()
        #self.OnPaint()
    def GetValue(self):
        return self.lst
    def SetAutoApply(self,flag):
        self.bAutoApply=flag
    def __setModified__(self,state):
        self.bModified=state
    def SetNetDocs(self,d):
        #if 'vHum' in d:
        #    dd=d['vHum']
        pass
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            grid=self.grid
            max=self.max
            #self.__setModified__(False)
            #self.Clear()
            self.labels=[]
            self.lst=[]
            try:
                n=self.doc.getChild(self.node,'grid')
                try:
                    iX=int(self.doc.getNodeText(n,'x'))
                except:
                    iX=10
                try:
                    iY=int(self.doc.getNodeText(n,'y'))
                except:
                    iY=10
                self.grid=(iX,iY)
                
                n=self.doc.getChild(self.node,'max')
                try:
                    iX=int(self.doc.getNodeText(n,'x'))
                except:
                    iX=10
                try:
                    iY=int(self.doc.getNodeText(n,'y'))
                except:
                    iY=10
                self.max=(iX,iY)
                
                for m in ['label_x','label_y']:
                    lst=[]
                    n=self.doc.getChild(self.node,m)
                    for c in self.doc.getChilds(n,'label'):
                        sPos=self.doc.getNodeText(c,'pos')
                        sLbl=self.doc.getNodeText(c,'name')
                        try:
                            lst.append((float(sPos),sLbl))
                        except:
                            pass
                    self.labels.append(lst)
                
                for l in self.doc.getChilds(self.node,'curve'):
                    sName=self.doc.getNodeText(l,'name')
                    n=self.doc.getChild(l,'range')
                    sRngMin=self.doc.getNodeText(n,'min')
                    try:
                        fRngMin=float(sRngMin)
                    except:
                        fRngMin=0
                    sRngMax=self.doc.getNodeText(n,'max')
                    try:
                        fRngMax=float(sRngMax)
                    except:
                        fRngMax=1
                    
                    lst=[]
                    for v in self.doc.getChilds(l,'value'):
                        sX=self.doc.getNodeText(v,'x')
                        sY=self.doc.getNodeText(v,'y')
                        try:
                            lst.append((float(sX),float(sY)))
                        except:
                            lst.append((-1,-1))
                    self.lst.append({'name':sName,'range':(fRngMin,fRngMax),'values':lst})
                bResize=False
                if max!=self.max:
                    bResize=True
                if grid!=self.grid:
                    bResize=True
                if bResize:
                    self.__calcSize__()
                else:
                    pass
                self.scwDrawing.Refresh()
            except:
                self.__logTB__()
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            n=self.doc.getChildForced(node,'grid')
            self.doc.setNodeText(n,'x','%d'%self.grid[0])
            self.doc.setNodeText(n,'y','%d'%self.grid[1])
            
            n=self.doc.getChildForced(node,'max')
            self.doc.setNodeText(n,'x','%d'%self.max[0])
            self.doc.setNodeText(n,'y','%d'%self.max[1])
            
            for m in ['label_x','label_y']:
                n=self.doc.getChildForced(node,m)
                childs=self.doc.getChilds(n,'label')
                iLen=len(childs)
                iPos=0
                if m=='label_x':
                    lbls=self.labels[0]
                else:
                    lbls=self.labels[1]
                for it in lbls:
                    if iPos<iLen:
                        # replace existing node info
                        c=childs[iPos]
                    else:
                        c=self.doc.createSubNode(n,'label')
                    self.doc.setNodeText(c,'pos','%4.2f'%it[0])
                    self.doc.setNodeText(c,'name',it[1])
                    iPos+=1
                # delete node
                for i in range(iPos,iLen):
                    #self.doc.deleteNode(childs[i],n)
                    self.doc.__deleteNode__(childs[i],n)
            childs=self.doc.getChilds(node,'curve')
            iLen=len(childs)
            iPos=0
            for curveDict in self.lst:
                if iPos<iLen:
                    c=childs[iPos]
                else:
                    c=self.doc.createSubNode(node,'curve')
                self.doc.setNodeText(c,'name',curveDict['name'])
                nRng=self.doc.getChildForced(c,'range')
                self.doc.setNodeText(nRng,'min','%6.2f'%curveDict['range'][0])
                self.doc.setNodeText(nRng,'max','%6.2f'%curveDict['range'][1])
                
                values=self.doc.getChilds(c,'value')
                iValLen=len(values)
                iValPos=0
                for it in curveDict['values']:
                    if iValPos<iValLen:
                        nVal=values[iValPos]
                    else:
                        nVal=self.doc.createSubNode(c,'value')
                    self.doc.setNodeText(nVal,'x','%6.2f'%it[0])
                    self.doc.setNodeText(nVal,'y','%6.2f'%it[1])
                    iValPos+=1
                # delete not needed values node
                for i in range(iValPos,iValLen):
                    #self.doc.deleteNode(values[i],c)
                    self.doc.__deleteNode__(values[i],c)
                iPos+=1
            # delete not needed curve node
            for i in range(iPos,iLen):
                #self.doc.deleteNode(childs[i],node)
                self.doc.__deleteNode__(childs[i],node)
            #self.doc.AlignNode(node,iRec=5)
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viName.Close()
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def OnSize(self, event):
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        s=event.GetSize()
        self.scwDrawing.SetSize(s)
        event.Skip()
    def Enable(self,flag):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        wx.Panel.Enable(self,flag)
        if BUFFERED:
            #if self.buffer is None:
                # Initialize the buffer bitmap.  No real DC is needed at this point.
            #    self.buffer = wx.EmptyBitmap(self.maxWidth+2, self.maxHeight+2+self.sumY)
            #    dc = wx.BufferedDC(None, self.buffer)
            #    dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
            #    dc.Clear()
            #    self.DoDrawing(dc)
            #else:
                cdc = wx.ClientDC(self.scwDrawing)
                self.scwDrawing.DoPrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
                dc.BeginDrawing()
                self.__clear__(dc,False)
                self.__drawGrid__(dc,False)
                self.__drawGridText__(dc,False)
                self.__drawValues__(dc,False)
                dc.EndDrawing()
    def GenTexPic(self):
        xMax=0
        yMax=0
        xMin=sys.maxint
        yMin=sys.maxint
        scaleX=1.5
        scaleY=1.5
        strs=[]
        xMin,yMin=0,0
        xMax,yMax=self.maxWidth+self.ofs[0],self.maxHeight+self.ofs[1]
        xMin/=scaleX
        yMin/=scaleY
        xMax/=scaleX
        yMax/=scaleY
        strs=['  \\begin{picture}(%d,%d)'%(xMax,yMax)]
        for dict in self.lst:
            rng=dict['range']
            oY=rng[1]*self.grid[1]
            dY=(rng[1]-rng[0])*self.grid[1]
            vals=dict['values']
            xa,ya=-1,-1
            for x,y in vals:
                if x<0:
                    xa,ya=-1,-1
                    continue
                y=self.ofs[1]+oY-(y*dY)
                x=self.ofs[0]+self.grid[0]*x
                if xa>=0:
                    l=[xa,ya,x,y,i]
                    self.__draw__(l,False,dc,False)
                    tup=vtRepLatex.lineFitPict2(xa,ya,x,y)
                    strs.append('    \\put(%4.1f,%4.1f){%s\\line(%d,%d){%4.1f}}'%tup)
                xa,ya=x,y
            i+=1
        strs.append('  \\end{picture}')
        return string.join(strs,os.linesep)
    def GenTexFigFloat(self):
        if self.node is not None:
            par=self.doc.getParent(self.node)
            sHier=vtXmlHierarchy.getTagNamesWithRel(self.doc,self.baseNode,self.relNode,par)+ ' ' + \
                        self.doc.getNodeText(par,'name') + ' '+_(u'diagram')
            sHier=vtXmlHierarchy.getTagNames(self.doc,self.baseNode,None,par)+ ' ' + \
                        self.doc.getNodeText(par,'name') + ' '+_(u'diagram')
        sCaption='  \\caption{%s}'%vtRepLatex.texReplace(sHier)
        sRef='  \\label{fig:%s}'%vtRepLatex.texReplace4Lbl(sHierFull)
        fig=['\\begin{figure}[!hb]',self.GenTexPic(),sCaption,sRef,'\\end{figure}']
        return string.join(fig,os.linesep)
    def GenTexFig(self):
        if self.node is not None:
            par=self.doc.getParent(self.node)
            sHier=vtXmlHierarchy.getTagNamesWithRel(self.doc,self.baseNode,self.relNode,par)+ ' ' + \
                        self.doc.getNodeText(par,'name') + ' '+_(u'diagram')
            sHierFull=vtXmlHierarchy.getTagNames(self.doc,self.baseNode,None,par)+ ' ' + \
                        self.doc.getNodeText(par,'name') + ' '+_(u'diagram')
        sCaption='  \\caption{%s}'%vtRepLatex.texReplace(sHier)
        sRef='  \\label{fig:%s}'%vtRepLatex.texReplace4Lbl(sHierFull)
        fig=['\\begin{center}','\\begin{longtable}{c}',self.GenTexPic(),'  \\\\',sCaption,sRef,'\\end{longtable}','\\end{center}']
        return string.join(fig,os.linesep)
    def GenTexFigRef(self,node,baseNode):
        if node is not None:
            par=self.doc.getParent(node)
            sHier=vtXmlHierarchy.getTagNames(self.doc,baseNode,None,par)+ ' ' + \
                        self.doc.getNodeText(par,'name') + ' '+_(u'diagram')
            sHier=vtRepLatex.texReplace4Lbl(sHier)
            sRef='\\ref{fig:%s} \#\\pageref{fig:%s}'%(sHier,sHier)
            return sRef
        return ''
    def SetBaseNode(self,node):
        self.baseNode=node
    def SetRelNode(self,node):
        self.relNode=node
        
