#----------------------------------------------------------------------------
# Name:         vtDrawCanvasObjectBase.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtDrawCanvasObjectBase.py,v 1.7 2006/03/14 14:26:17 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------


class vtDrawCanvasObjectBase:
    DRV_UNDEF       = 0
    DRV_WX          = 1
    DRV_LATEX       = 2
    DRIVERS         = [DRV_WX , DRV_LATEX]
    def __init__(self,id=-1,name='',x=0,y=0,w=0,h=0,layer=0):
        self.id=id
        self.name=name
        self.iX=x
        self.iY=y
        self.iWidth=w
        self.iHeight=h
        self.bVisible=True
        self.iLayer=layer
        self.iAngle=0
        self.usrData=None
        self.bActive=False
        self.bSelected=False  # temporary flag
        self.bFocus=False     # temporary flag
    def GetName(self):
        return self.name
    def GetId(self):
        return self.id
    def GetVisible(self):
        return self.bVisible
    def SetVisible(self,flag):
        self.bVisible=flag
    def SetUsrData(self,data):
        self.usrData=data
    def GetUsrData(self):
        return self.usrData
    def GetX(self):
        return self.iX
    def GetY(self):
        return self.iY
    def GetWidth(self):
        return self.iWidth
    def GetHeight(self):
        return self.iHeight
    def GetLayer(self):
        return self.iLayer
    def GetAngle(self):
        return self.iAngle
    def GetSelected(self):
        return self.bSelected
    def GetFocus(self):
        return self.bFocus
    def GetActive(self):
        return self.bActive
    def SetSelected(self,flag):
        self.bSelected=flag
    def SetFocus(self,flag):
        self.bFocus=flag
    def SetActive(self,flag):
        self.bActive=flag
    def SetX(self,x):
        self.iX=x
    def SetY(self,y):
        self.iY=y
    def SetWidth(self,w):
        self.iWidth=w
    def SetHeight(self,h):
        self.iHeight=h
    def SetLayer(self,layer):
        self.iLayer=layer
    def SetAngle(self,angle):
        self.iAngle=angle
    def GetAttr(self,idx):
        if idx<0:
            return '','','',-1
        elif idx==0:
            return 'x',self.GetX(),'%d',1
        elif idx==1:
            return 'y',self.GetY(),'%d',1
        elif idx==2:
            return 'width',self.GetWidth(),'%d',1
        elif idx==3:
            return 'height',self.GetHeight(),'%d',1
        elif idx==4:
            return 'layer',self.GetLayer(),'%d',1
        elif idx==5:
            return 'angle',self.GetAngle(),'%d',1
        
        return '','','',-1
    def SetAttr(self,idx,val):
        if idx<0:
            return -1
        elif idx==0:
            self.SetX(val)
            return 0
        elif idx==1:
            self.SetY(val)
            return 0
        elif idx==2:
            self.SetWidth(val)
            return 0
        elif idx==3:
            self.SetHeight(val)
            return 0
        elif idx==4:
            self.SetLayer(val)
            return 0
        elif idx==5:
            self.SetAngle(val)
            return 0
        return -1
    def GetDefaults(self):
        return ''
    def SetDefaults(self,s):
        pass
    def GetAttrEntries(self,idx):
        return []
    def GetAttrEntrySel(self,idx):
        return -1
    def GetTypeName(self):
        # replace me
        return 'base'
    def SetNode(self,doc,node,canvas):
        sVal=doc.getNodeText(node,'name')
        self.name=sVal
        sVal=doc.getNodeText(node,'id')
        try:
            self.id=int(sVal)
        except:
            self.id=-1
        
        sVal=doc.getNodeText(node,'x')
        try:
            self.SetX(int(sVal))
        except:
            self.SetX(0)
        sVal=doc.getNodeText(node,'y')
        try:
            self.SetY(int(sVal))
        except:
            self.SetY(0)
        sVal=doc.getNodeText(node,'width')
        try:
            self.SetWidth(int(sVal))
        except:
            self.SetWidth(0)
        sVal=doc.getNodeText(node,'height')
        try:
            self.SetHeight(int(sVal))
        except:
            self.SetHeight(0)
        sVal=doc.getNodeText(node,'layer')
        try:
            self.SetLayer(int(sVal))
        except:
            self.SetLayer(0)
        sVal=doc.getNodeText(node,'angle')
        try:
            self.SetAngle(int(sVal))
        except:
            self.SetAngle(0)
        sVal=doc.getNodeText(node,'visible')
        try:
            if sVal=='1':
                self.SetVisible(True)
            else:
                self.SetVisible(False)
        except:
            self.bVisible=True
    def GetNode(self,doc,parNode,canvas):
        node=doc.createSubNode(parNode,'object')
        doc.setAttribute(node,'type',self.GetTypeName())
        doc.setNodeText(node,'id','%d'%self.id)
        doc.setNodeText(node,'name',self.name)
        doc.setNodeText(node,'x','%d'%self.GetX())
        doc.setNodeText(node,'y','%d'%self.GetY())
        doc.setNodeText(node,'width','%d'%self.GetWidth())
        doc.setNodeText(node,'height','%d'%self.GetHeight())
        doc.setNodeText(node,'layer','%d'%self.GetLayer())
        doc.setNodeText(node,'angle','%d'%self.GetAngle())
        if self.bVisible:
            doc.setNodeText(node,'visible','1')
        else:
            doc.setNodeText(node,'visible','0')
        return node
    def IsIntersect(self,canvas,o):
        iXa,iYa,iXe,iYe=self.__getExtend__(canvas)
        oiXa,oiYa,oiXe,oiYe=o.__getExtend__(canvas)
        if (iXa>=oiXa and iXa<=oiXe) or (iXe>=oiXa and iXe<=oiXe):
            if (iYa>=oiYa and iYa<=oiYe) or (iYe>=oiYa and iYe<=oiYe):
                return True
        if (oiXa>=iXa and oiXa<=iXe) or (oiXe>=iXa and oiXe<=iXe):
            if (oiYa>=iYa and oiYa<=iYe) or (oiYe>=iYa and oiYe<=iYe):
                return True
        return False
    def IsIntersectTup(self,canvas,oiXa,oiYa,oiXe,oiYe):
        iXa,iYa,iXe,iYe=self.__getExtend__(canvas)
        if (iXa>=oiXa and iXa<=oiXe) or (iXe>=oiXa and iXe<=oiXe):
            if (iYa>=oiYa and iYa<=oiYe) or (iYe>=oiYa and iYe<=oiYe):
                return True
        if (oiXa>=iXa and oiXa<=iXe) or (oiXe>=iXa and oiXe<=iXe):
            if (oiYa>=iYa and oiYa<=iYe) or (oiYe>=iYa and oiYe<=iYe):
                return True
        return False
    def IsIntersectStrict(self,canvas,o):
        iXa,iYa,iXe,iYe=self.__getExtend__(canvas)
        oiXa,oiYa,oiXe,oiYe=o.__getExtend__(canvas)
        if (iXa>=oiXa and iXa<oiXe) or (iXe>oiXa and iXe<=oiXe):
            if (iYa>=oiYa and iYa<oiYe) or (iYe>oiYa and iYe<=oiYe):
                return True
        if (oiXa>=iXa and oiXa<iXe) or (oiXe>iXa and oiXe<=iXe):
            if (oiYa>=iYa and oiYa<iYe) or (oiYe>iYa and oiYe<=iYe):
                return True
        return False
    def IsIntersectTupStrict(self,canvas,oiXa,oiYa,oiXe,oiYe):
        iXa,iYa,iXe,iYe=self.__getExtend__(canvas)
        if (iXa>=oiXa and iXa<oiXe) and (iXe>oiXa and iXe<=oiXe):
            if (iYa>=oiYa and iYa<oiYe) and (iYe>oiYa and iYe<=oiYe):
                return True
        if (oiXa>=iXa and oiXa<iXe) and (oiXe>iXa and oiXe<=iXe):
            if (oiYa>=iYa and oiYa<iYe) and (oiYe>iYa and oiYe<=iYe):
                return True
        return False
    def IsInsideTup(self,canvas,oiXa,oiYa):
        iXa,iYa,iXe,iYe=self.__getExtend__(canvas)
        if (oiXa>=iXa and oiXa<iXe):
            if (oiYa>=iYa and oiYa<iYe):
                return True
        return False
    def __drawOnIntersect__(self,canvas,o):
        if self.bVisible==False:
            return False
        if self.IsIntersect(canvas,o):
            self.__draw__(canvas)
            return True
        
        #oiX,oiY,oiWidth,oiHeight=o.__getExtend__(canvas)
        #if (self.iX>=oiX and self.iX<=(oiX+oiWidth)) or ((self.iX+self.iWidth)>=oiX and (self.iX+self.iWidth)<=(oiX+oiWidth)):
        #        if (self.iY>=oiY and self.iY<=(oiY+oiHeight)) or ((self.iY+self.iHeight)>=oiY and (self.iY+self.iHeight)<=(oiY+oiHeight)):
        #                self.__draw__(canvas)
        #                return True
        #if (self.iX>=o.iX and self.iX<=(o.iX+o.iWidth)) or ((self.iX+self.iWidth)>=o.iX and (self.iX+self.iWidth)<=(o.iX+o.iWidth)):
        #        if (self.iY>=o.iY and self.iY<=(o.iY+o.iHeight)) or ((self.iY+self.iHeight)>=o.iY and (self.iY+self.iHeight)<=(o.iY+o.iHeight)):
        #                self.__draw__(canvas)
        #                return True
        return False
    def __drawOnIntersectTup__(self,canvas,oiXa,oiYa,oiXe,oiYe):
        if self.bVisible==False:
            return False
        if self.IsIntersectTup(canvas,oiXa,oiYa,oiXe,oiYe):
            self.__draw__(canvas)
            return True
        return False
    def __draw__(self,canvas,aX=0,aY=0,aW=-1,aH=-1,iDriver=-1):
        pass
    def __drawSelect__(self,canvas,aX=0,aY=0,aW=-1,aH=-1,iDriver=-1):
        if self.bVisible==False:
            return
        if self.bSelected:
            drvInfo=canvas.getDriverData(iDriver)
            drv=canvas.getDriver(iDriver)
            if drv!=self.DRV_WX:
                return
            drvInfo=canvas.getDriverData(iDriver)
            drv=canvas.getDriver(iDriver)
            if canvas.SELECTED_LAYER<0:
                iLayer=self.iLayer
            else:
                iLayer=canvas.SELECTED_LAYER
            canvas.setActLayer(iLayer,self.bActive,iDriver)
            if drv==self.DRV_WX:
                dc=drvInfo['dc']
                if aW>=0 and aH>=0:
                    ax,ay=canvas.calcCoor(aX,aY)
                    aw,ah=canvas.calcCoor(aW,aH)
                    dc.SetClippingRegion(ax,ay,aw+1,ah+1)
                else:
                    ax,ay=canvas.calcCoor(self.iX,self.iY)
                    aw,ah=canvas.calcCoor(self.iWidth,self.iHeight)
                    dc.SetClippingRegion(ax,ay,aw+1,ah+1)
                x,y=canvas.calcCoor(self.iX+aX,self.iY+aY)
                w,h=canvas.calcCoor(self.iWidth,self.iHeight)
                grid=canvas.GetGrid()
                if canvas.SELECTED_WIDTH<0:
                    gW2=grid[0]/2
                else:
                    gW2=canvas.SELECTED_WIDTH
                if canvas.SELECTED_HEIGHT<0:
                    gH2=grid[1]/2
                else:
                    gH2=canvas.SELECTED_HEIGHT
                dc.DrawPolygon([(x,y),(x+gW2,y),(x,y+gH2)])
                #dc.DrawLine(x+w,y,x+w,y+h)
                #dc.DrawLine(x,y+h,x+w,y+h)
                #dc.DrawLine(x,y,x,y+h)
                if aW>=0 and aH>=0:
                    dc.DestroyClippingRegion()
                else:
                    dc.DestroyClippingRegion()
        self.__drawFocus__(canvas,aX=aX,aY=aY,aW=aW,aH=aW,iDriver=iDriver)
    def __drawFocus__(self,canvas,aX=0,aY=0,aW=-1,aH=-1,iDriver=-1):
        if self.bVisible==False:
            return
        if self.bFocus:
            drvInfo=canvas.getDriverData(iDriver)
            drv=canvas.getDriver(iDriver)
            if drv!=self.DRV_WX:
                return
            if canvas.FOCUS_LAYER<0:
                iLayer=self.iLayer
            else:
                iLayer=canvas.FOCUS_LAYER
            canvas.setActLayer(iLayer,self.bActive,iDriver)
            if drv==self.DRV_WX:
                dc=drvInfo['dc']
                #print aX,aY,aW,aH
                if aW>=0 and aH>=0:
                    #print 'clip'
                    ax,ay=canvas.calcCoor(aX,aY)
                    aw,ah=canvas.calcCoor(aW,aH)
                    dc.SetClippingRegion(ax,ay,aw+1,ah+1)
                else:
                    ax,ay=canvas.calcCoor(self.iX,self.iY)
                    aw,ah=canvas.calcCoor(self.iWidth,self.iHeight)
                    dc.SetClippingRegion(ax,ay,aw,ah)
                x,y=canvas.calcCoor(self.iX+aX,self.iY+aY)
                w,h=canvas.calcCoor(self.iWidth,self.iHeight)
                grid=canvas.GetGrid()
                if canvas.FOCUS_WIDTH<0:
                    gW2=grid[0]/3
                else:
                    gW2=canvas.FOCUS_WIDTH
                if canvas.FOCUS_HEIGHT<0:
                    gH2=grid[1]/3
                else:
                    gH2=canvas.FOCUS_HEIGHT
                dc.DrawPolygon([(x,y),(x+gW2,y),(x,y+gH2)])
                dc.DrawPolygon([(x+w-gW2-1,y),(x+w-1,y),(x+w-1,y+gH2)])
                dc.DrawPolygon([(x+w-gW2-1,y+h-1),(x+w-1,y+h-1),(x+w-1,y+h-gH2-1)])
                dc.DrawPolygon([(x,y+h-gH2-1),(x+gW2,y+h-1),(x,y+h-1)])
                
                #dc.DrawLine(x+w,y,x+w,y+h)
                #dc.DrawLine(x,y+h,x+w,y+h)
                #dc.DrawLine(x,y,x,y+h)
                if aW>=0 and aH>=0:
                    dc.DestroyClippingRegion()
                else:
                    dc.DestroyClippingRegion()
    def __str__(self):
        return self.GetTypeName()+' name:'+self.name+' id:'+str(self.id)+' (%d,%d),(%d,%d) %d'%(self.iX,self.iY,self.iWidth,self.iHeight,self.bVisible)
    def __getExtend__(self,canvas):
        #return self.iX,self.iY,self.iWidth,sefl.iHeight
        return self.iX,self.iY,self.iX+self.iWidth,self.iY+self.iHeight
    def calcExtend(self,canvas):
        return self.__getExtend__(canvas)
    def calcPosition(self,obj,canvas):
        pass
    def FindByAttr(self,lv,val,start=[],attr='name',matchFunc=None):
        try:
            v=getattr(self,attr)
            if matchFunc(v,val):
                #return (True,start,self,v)
                return self
        except:
            pass
        #return (False,start,None,None)
        return None
        
