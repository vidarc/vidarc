#Boa:Frame:vCanvasFrame
#----------------------------------------------------------------------------
# Name:         vCanvasFrame.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vCanvasFrame.py,v 1.1 2005/12/11 23:04:48 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons

import string,os,time,sys
import thread,threading,traceback

from vidarc.tool.draw.vtDrawCanvasPanel import *
from vidarc.tool.draw.vtDrawCanvasObjectBase import *
from vidarc.tool.draw.vtDrawCanvasLine import *
from vidarc.tool.draw.vtDrawCanvasRectangle import *
from vidarc.tool.draw.vtDrawCanvasText import *
from vidarc.tool.draw.vtDrawCanvasGroup import *

from vidarc.tool.xml.vtXmlDom import *

import images


def create(parent):
    return vCanvasFrame(parent)

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon
[wxID_VCANVASFRAME, wxID_VCANVASFRAMECBOPEN, wxID_VCANVASFRAMECBSAVE, 
 wxID_VCANVASFRAMEPNMAIN, wxID_VCANVASFRAMESBSTATUS, 
] = [wx.NewId() for _init_ctrls in range(5)]

[wxID_VDIAGRAMFRAMEMNFILEITEM_EXIT, wxID_VDIAGRAMFRAMEMNFILEITEM_OPEN, 
] = [wx.NewId() for _init_coll_mnFile_Items in range(2)]


STATUS_PROCESS_POS=0
STATUS_CLOCK_POS=1

class vCanvasFrame(wx.Frame):
    def _init_coll_mnFile_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_VDIAGRAMFRAMEMNFILEITEM_OPEN,
              kind=wx.ITEM_NORMAL, text=_(u'Open'))
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VDIAGRAMFRAMEMNFILEITEM_EXIT,
              kind=wx.ITEM_NORMAL, text=_(u'Exit'))
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_openMenu,
              id=wxID_VDIAGRAMFRAMEMNFILEITEM_OPEN)
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_exitMenu,
              id=wxID_VDIAGRAMFRAMEMNFILEITEM_EXIT)

    def _init_coll_mnBar_Menus(self, parent):
        # generated method, don't edit

        parent.Append(menu=self.mnFile, title=_(u'File'))

    def _init_coll_sbStatus_Fields(self, parent):
        # generated method, don't edit
        parent.SetFieldsCount(2)

        parent.SetStatusText(number=0, text=u'Process')
        parent.SetStatusText(number=1, text=u'Clock')

        parent.SetStatusWidths([-1, 155])

    def _init_utils(self):
        # generated method, don't edit
        self.mnBar = wx.MenuBar()

        self.mnFile = wx.Menu(title=u'')

        self._init_coll_mnBar_Menus(self.mnBar)
        self._init_coll_mnFile_Items(self.mnFile)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VCANVASFRAME, name=u'vDiagramFrame',
              parent=prnt, pos=wx.Point(312, 47), size=wx.Size(579, 474),
              style=wx.DEFAULT_FRAME_STYLE, title=u'vDiagramFrame')
        self._init_utils()
        self.SetClientSize(wx.Size(571, 447))
        self.SetMenuBar(self.mnBar)

        self.sbStatus = wx.StatusBar(id=wxID_VCANVASFRAMESBSTATUS,
              name=u'sbStatus', parent=self, style=0)
        self.sbStatus.SetFieldsCount(2)
        self._init_coll_sbStatus_Fields(self.sbStatus)
        self.SetStatusBar(self.sbStatus)

        self.pnMain = wx.Panel(id=wxID_VCANVASFRAMEPNMAIN, name=u'pnMain',
              parent=self, pos=wx.Point(0, 0), size=wx.Size(571, 408),
              style=wx.TAB_TRAVERSAL)
        self.pnMain.SetAutoLayout(True)

        self.cbSave = wx.Button(id=wxID_VCANVASFRAMECBSAVE, label=u'Save',
              name=u'cbSave', parent=self.pnMain, pos=wx.Point(144, 376),
              size=wx.Size(75, 23), style=0)
        self.cbSave.Bind(wx.EVT_BUTTON, self.OnCbSaveButton,
              id=wxID_VCANVASFRAMECBSAVE)

        self.cbOpen = wx.Button(id=wxID_VCANVASFRAMECBOPEN, label=u'Open',
              name=u'cbOpen', parent=self.pnMain, pos=wx.Point(48, 376),
              size=wx.Size(75, 23), style=0)
        self.cbOpen.Bind(wx.EVT_BUTTON, self.OnCbOpenButton,
              id=wxID_VCANVASFRAMECBOPEN)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.bModified=False
        self.Clear()
        
        tup=[[(0,'0'),(1,'1')],[(0.0,'x'),(1.5,'y')]]
        self.dgCanvas=vtDrawCanvasPanel(self.pnMain,pos=(10,40),size=(300,300))
        self.dgCanvas.SetConstraints(LayoutAnchors(self.dgCanvas, True,
              True, True, True))
        obj=vtDrawCanvasObjectBase()
        self.dgCanvas.Add(vtDrawCanvasLine(name='line001',x=1,y=1,w=5,h=5))
        self.dgCanvas.Add(vtDrawCanvasRectangle(name='rect001',x=5,y=5,w=2,h=2,layer=4))
        self.dgCanvas.Add(vtDrawCanvasText(name='text001',text='test',x=5,y=5,w=-1,h=-1,layer=4))
        self.dgCanvas.Add(vtDrawCanvasRectangle(name='rect002',x=2,y=5,w=2,h=2,layer=2))
        self.dgCanvas.Add(vtDrawCanvasText(name='text002',text='test2',x=2,y=5,w=2,h=2,layer=2))
        self.dgCanvas.Add(vtDrawCanvasRectangle(name='rect003',x=2,y=0,w=2,h=2,layer=3))
        self.dgCanvas.Add(vtDrawCanvasText(name='text003',text='test3',x=2,y=0,w=2,h=2,align_horizontal=vtDrawCanvasText.ALIGN_RIGHT,layer=3))
        self.dgCanvas.Add(vtDrawCanvasRectangle(name='rect004',x=2,y=2,w=2,h=2,layer=5))
        self.dgCanvas.Add(vtDrawCanvasText(name='text004',text='test4',x=2,y=2,w=2,h=2,align_vertical=vtDrawCanvasText.ALIGN_BOTTOM,layer=5))
        
        grp=vtDrawCanvasGroup(name='group001',x=5,y=2,w=3,h=3,layer=5)
        grp.Add(vtDrawCanvasLine(name='line011',x=2,y=2,w=4,h=0))
        grp.Add(vtDrawCanvasLine(name='line012',x=0,y=0,w=1,h=0))
        grp.Add(vtDrawCanvasLine(name='line013',x=0,y=1,w=1,h=0))
        grp.Add(vtDrawCanvasLine(name='line014',x=2,y=0,w=0,h=1))
        grp.Add(vtDrawCanvasRectangle(name='rect011',x=2,y=0,w=1,h=1,layer=3))
        grp.Add(vtDrawCanvasText(name='text011',text='test11',x=2,y=2,w=2,h=2,layer=2))
        self.dgCanvas.Add(grp)
        self.dgCanvas.SetConfig(obj,obj.DRV_WX,max=(100,100),
                    grid=(20,20))
        self.dgCanvas.RegisterObject(vtDrawCanvasLine())
        self.dgCanvas.RegisterObject(vtDrawCanvasRectangle())
        self.dgCanvas.RegisterObject(vtDrawCanvasText())
        self.dgCanvas.RegisterObject(vtDrawCanvasGroup())
        
        
        #self.dgTiming.SetValues(vals)
        
        # setup statusbar
        self.timer = wx.PyTimer(self.Notify)
        self.timer.Start(1000)
        self.Notify()
        
        icon = getApplicationIcon()
        self.SetIcon(icon)
        
        rect = self.sbStatus.GetFieldRect(STATUS_PROCESS_POS)
        self.gProcess = wx.Gauge(
                    self.sbStatus, -1, 50, (rect.x+2, rect.y+2), 
                    (rect.width-4, rect.height-4), 
                    wx.GA_HORIZONTAL|wx.GA_SMOOTH
                    )
        self.gProcess.SetRange(100)
        self.gProcess.SetValue(0)
        
    def Notify(self):
        try:
            t = time.localtime(time.time())
            st = time.strftime(_("%b-%d-%Y   %H:%M:%S wk:%W"), t)
            self.SetStatusText(st, STATUS_CLOCK_POS)
        except:
            pass
    def __makeTitle__(self):
        s=_("VIDARC Diagram")
        if self.fn is None:
            s=s+_(" (undef*)")
        else:
            s=s+" ("+self.fn
            if self.bModified:
                s=s+"*)"
            else:
                s=s+")"
        self.SetTitle(s)
    def __setModified__(self,state):
        self.bModified=state
        self.__makeTitle__()
    
    def Clear(self):
        pass
    def OpenFile(self,fn=None):
        pass
    def OnMnFileItem_openMenu(self, event):
        if self.thdOpen.IsRunning():
            return 
        dlg = wx.FileDialog(self, "Open", ".", "", "log files (*.log)|*.log|all files (*.*)|*.*", wx.OPEN)
        try:
            fn=self.GetFN()
            if fn is not None:
                dlg.SetPath(fn)
            #else:
            #    dlg.SetPath('.')
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.OpenFile(filename)
                self.__setModified__(False)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()

    def OnMnFileItem_exitMenu(self, event):
        self.Close()
        event.Skip()        

    def OnCbOpenButton(self, event):
        dlg = wx.FileDialog(self, _("Open"), ".", "", _("XML files (*.xml)|*.xml|all files (*.*)|*.*"), wx.OPEN)
        try:
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                dom=vtXmlDom()
                dom.Open(filename)
                node=dom.getChild(dom.getRoot(),'canvas')
                self.dgCanvas.SetDoc(dom)
                self.dgCanvas.SetNode(node)
                dom.Close()
                del dom
        finally:
            dlg.Destroy()
        event.Skip()
        
    def OnCbSaveButton(self, event):
        dlg = wx.FileDialog(self, _("Save File As"), ".", "", _("XML files (*.xml)|*.xml|all files (*.*)|*.*"), wx.SAVE)
        try:
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                #self.txtEditor.SaveFile(filename)
                dom=vtXmlDom()
                dom.New(root='canvas_base')
                node=dom.createSubNode(dom.getRoot(),'canvas')
                self.dgCanvas.SetDoc(dom)
                self.dgCanvas.GetNode(node)
                dom.AlignDoc()
                dom.Save(filename)
                dom.Close()
                del dom
        finally:
            dlg.Destroy()
        event.Skip()

    def OnCbOpenXmlButton(self, event):
        dlg = wx.FileDialog(self, _("Open External"), ".", "", _("XML files (*.xml)|*.xml|all files (*.*)|*.*"), wx.OPEN)
        try:
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                dom=vtXmlDom()
                dom.Open(filename)
                node=dom.getChild(dom.getRoot(),'canvas')
                self.dgCanvas.SetDocExt(dom)
                self.dgCanvas.SetNodeExt(node)
                dom.Close()
                del dom
        finally:
            dlg.Destroy()
        event.Skip()

