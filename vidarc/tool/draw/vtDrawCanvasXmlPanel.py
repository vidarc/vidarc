#----------------------------------------------------------------------------
# Name:         vtDrawCanvasXmlPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtDrawCanvasXmlPanel.py,v 1.6 2010/04/10 12:32:14 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

from vtDrawCanvasPanel import *
from vtDrawCanvasXmlGroup import *
from vtDrawCanvasBlockIO import *

from vtDrawCanvasRectangle import *
from vtDrawCanvasLine import *
from vtDrawCanvasText import *

from vtDrawCanvasXmlDialog import vtDrawCanvasXmlDialog
import vidarc.tool.xml.vtXmlHierarchy as vtXmlHierarchy
import vidarc.tool.log.vtLog as vtLog

import traceback

class vtDrawCanvasXmlPanel(vtDrawCanvasPanel):
    def __init__(self, parent, 
                id=wx.NewId(), pos=wx.DefaultPosition, size=wx.Size(100, 50), 
                style=wx.TAB_TRAVERSAL, name=u'vtDrawCanvasXmlPanel'):
        vtDrawCanvasPanel.__init__(self,parent,id=id,pos=pos,size=size,style=style,name=name)
        self.name=name
        self.iXmlLevel=3
        self.iNodeLevel=0
        self.docExt=None
        self.nodeExt=None
        self.regXmlActions={}
    def RegisterXmlActions(self,tagName,create,addAttr=None,delAttr=None):
        self.regXmlActions[tagName]={'create':create,
                    'add_attr':addAttr,'del_attr':delAttr}
    def __Clear__(self):
        vtDrawCanvasPanel.__Clear__(self)
        #self.nodeExt=None
        if self.dlg is not None:
            self.dlg.__Clear__()
    def ClearInt(self):
        vtDrawCanvasPanel.ClearInt(self)
        #self.nodeExt=None
        if self.dlg is not None:
            self.dlg.ClearInt()
    def __SetNode__(self,node,bBlockCheck=False):
        vtDrawCanvasPanel.__SetNode__(self,node)
        if bBlockCheck==False:
            self.checkObjects()
    def SetNodeDfts(self,node):
        dlg=self.GetDlg()
        if dlg is not None:
            dlg.SetNodeDfts(node)
    def GetDlg(self):
        if self.dlg is None:
            self.dlg=vtDrawCanvasXmlDialog(self)
            self.dlg.SetName(self.name)
            self.dlg.Centre()
        return self.dlg
    def __setDlgValues__(self):
        dlg=self.GetDlg()
        if dlg is not None:
            dlg.SetValue(objects=self.objs,
                    grid=self.grid,max=self.max,
                    canvas=self,xml_level=self.iXmlLevel,
                    report_scale=self.fScale,
                    report_font=self.iFontSize,
                    report_rotate=self.bRotate)
            return 0
        return -1
    def __getDlgValues__F(self):
        dlg=self.GetDlg()
        if dlg is not None:
            self.lst=dlg.GetValue('objects')
            self.labels=dlg.GetLabels()
            max=dlg.GetMax()
            grid=dlg.GetGrid()
            bResize=False
            if max!=self.max:
                bResize=True
            if grid!=self.grid:
                bResize=True
            self.max=max
            self.grid=grid
            if bResize:
                return 1
            else:
                return 0
        return -1
    def __openDlg__(self):
        dlg=self.GetDlg()
        vtDrawCanvasPanel.__openDlg__(self)
    def GetDlgData(self):
        iRet=vtDrawCanvasPanel.GetDlgData(self)
        try:
            if self.dlg is not None:
                #self.fScale,self.iFontSize,self.bRotate=self.dlg.GetReportValue()
                self.iXmlLevel=self.dlg.GetValue('xml_level')
        except:
            pass
        return iRet
        
    def SetDocExt(self,doc,bNet=False):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.docExt=doc
        dlg=self.GetDlg()
        dlg.SetDoc(self.docExt)
        if bNet==True:
            pass
    def SetNodeExt(self,node):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.nodeExt=node
        dlg=self.GetDlg()
        dlg.SetNode(self.nodeExt)
        self.iNodeLevel=self.docExt.getLevel(self.nodeExt)
        self.checkObjects()
        self.__doDraw__()
        pass
    def GetDocExt(self):
        return self.docExt
    def GetNodeExt(self):
        return self.nodeExt
    def Recalc(self,objBase,obj,objAttr):
        try:
            #print
            #print obj,lstGrp
            obj.calcPosition(None,self)
            if isinstance(objBase, vtDrawCanvasBlock):
                #print lstGrp[-1]
                objBase.calcPosition(None,self)
        except:
            pass
        pass
    def calcExtend(self,obj):
        iXmlX,iXmlY,iXmlW,iXmlH=obj.calcExtend(self)
    def calcPosition(self,oPar,o):
        oPar.calcPosition(o,self)
        pass
    def addNodeAttr(self,node,attr,defaults='',type=vtDrawCanvasBlockIO.TYPE_IN,display=vtDrawCanvasBlockIO.DISPLAY_VAL):
        id=long(self.docExt.getKey(node))
        o,lvObj=self.__find__(self.objs,id)
        if o is not None:
            try:
                tagName=self.docExt.getTagName(node)
                f=self.regXmlActions[tagName]['add_attr']
                if f is not None:
                    f(o,node,attr,defaults,self)
            except:
                #traceback.print_exc()
                vtLog.vtLngCallStack(None,vtLog.ERROR,
                                'exception:%s'%traceback.format_exc(),
                                origin=self.name)
                sAttrName=self.docExt.getTagName(attr)
                sAttrVal=self.docExt.getAttributeVal(attr)
                o.AddAttr(sAttrName,sAttrVal,defaults,type,display,self)
            id=long(self.docExt.getKey(self.docExt.getParent(node)))
            oPar,lvObjPar=self.__find__(self.objs,id)
            #self.calcPosition(oPar,o)
    def delNodeAttr(self,node,attr):
        id=long(self.docExt.getKey(node))
        o,lvObj=self.__find__(self.objs,id)
        if o is not None:
            try:
                tagName=self.docExt.getTagName(node)
                f=self.regXmlActions[tagName]['del_attr']
                if f is not None:
                    f(o,node,attr,self)
            except:
                sAttrName=self.docExt.getTagName(attr)
                o.DelAttr(sAttrName,self)
                #traceback.print_exc()
                vtLog.vtLngCallStack(None,vtLog.ERROR,
                                'exception:%s'%traceback.format_exc(),
                                origin=self.name)
                    
    def checkNode(self,node,bAdded):
        bDbg=False
        if self.__isLogDebug__():
            self.__logDebug__('id:%s added:%d'%(self.docExt.getKey(node),bAdded))
            bDbg=True
        try:
            id=self.docExt.getKey(node)
            o,lvObj=self.__find__(self.objs,long(id))
            if o is None:
                if bAdded:
                    # add
                    idPar=self.docExt.getKey(self.docExt.getParent(node))
                    oPar,lvParObj=self.__find__(self.objs,long(idPar))
                    sName=vtXmlHierarchy.getTagNamesWithRel(self.docExt,self.nodeExt,None,node)
                    layer=self.docExt.getLevel(node)-self.iNodeLevel
                    try:
                        tagName=self.docExt.getTagName(node)
                        o=self.regXmlActions[tagName]['create'](node,self)
                    except:
                        #traceback.print_exc()
                        o=vtDrawCanvasXmlGroup(id=long(id),name=sName,x=0,y=0,w=10,h=10,layer=layer)
                        self.__logTB__()
                    self.calcExtend(o)
                    #r=vtDrawCanvasRectangle(id=-1,name='rect000',x=0,y=0,w=10,h=10,layer=layer)
                    #o.AddXml(r)
                    #r=vtDrawCanvasRectangle(id=-1,name='rect000',x=0,y=0,w=10,h=10,layer=layer)
                    #o.AddXml(r)
                    if oPar is None:
                        lv=self.docExt.getLevel(node)
                        if len(self.objs)>0:
                            nn=self.docExt.getNodeByIdNum(self.objs[0].id)
                            if nn is not None:
                                lvN=self.docExt.getLevel(nn)
                            else:
                                # should never happen
                                lvN=-1
                                self.__logError__('id:%s not found'%(self.objs[0].id))
                        else:
                            lvN=-1
                        if bDbg:
                            self.__logDebug__('no parent level:%d;'\
                                    'first objects level:%d'%(lv,lvN))
                        if lv==lvN:
                            self.objs.append(o)
                        else:
                            o.objs=self.objs
                            self.objs=[o]
                    else:
                        lv=self.docExt.getLevel(node)
                        if len(oPar.objs):
                            nn=self.docExt.getNodeByIdNum(oPar.objs[0].id)
                            if nn is not None:
                                lvN=self.docExt.getLevel(nn)
                            else:
                                # should never happen
                                lvN=-1
                                self.__logError__('id:%s not found'%(oPar.objs[0].id))
                        else:
                            lvN=-1
                        if bDbg:
                            self.__logDebug__('parent %s level:%d '\
                                'first objects level:%d'%(o,lv,lvN))
                        #self.calcPosition(oPar,o)
                        oPar.calcPosition(o,self)
                        oPar.Add(o)
                        self.calcExtend(oPar)
                    self.__doDraw__()
                    return self.objs
                else:
                    # do nothing
                    return None
            else:
                if bAdded:
                    # do nothing
                    return None
                else:
                    # del
                    if lvObj==0:
                        self.objs.remove(o)
                        if len(self.objs)==0:
                            self.objs=o.objs
                            o.objs=None
                            del o
                    else:
                        self.__delete__(self.objs,long(id))
                    self.__doDraw__()
                    return self.objs
        except:
            self.__logTB__()
    def findNode(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__('id:%s'%(self.docExt.getKey(node)))
            id=self.docExt.getKey(node)
            o,lvObj=self.__find__(self.objs,long(id))
            return o,lvObj
        except:
            self.__logTB__()
            pass
        return None,-1
    def __find__(self,objs,id,lv=0):
        for o in objs:
            if o.id==id:
                return o,lv
            if hasattr(o,'objs'):
                if len(o.objs)>0:
                    n,lvN=self.__find__(o.objs,id,lv+1)
                    if n is not None:
                        return n,lvN
        return None,-1
    def __delete__(self,objs,id):
        for o in objs:
            if o.id==id:
                objs.remove(o)
                del o
                return 1
            if hasattr(o,'objs'):
                if len(o.objs)>0:
                    if self.__delete__(o.objs,id)>0:
                        return 1
        return 0
    def __check__(self,objs,node):
        childs=self.docExt.getChildsAttr(node,self.docExt.attr)
        d={}
        for c in childs:
            id=self.docExt.getKey(c)
            if self.docExt.IsKeyValid(id)==False:
                continue
            d[long(id)]=c
        lst=[]
        for o in objs:
            try:
                if isinstance(o,vtDrawCanvasLine):
                    lst.append(o)
                    continue
                if isinstance(o,vtDrawCanvasText):
                    lst.append(o)
                    continue
                if isinstance(o,vtDrawCanvasTextMultiLine):
                    lst.append(o)
                    continue
                if isinstance(o,vtDrawCanvasRectangle):
                    lst.append(o)
                    continue
                if isinstance(o,vtDrawCanvasRectangle):
                    lst.append(o)
                    continue
                c=d[o.id]
                lst.append(o)
                if hasattr(o,'objs'):
                    if len(o.objs)>0:
                        o.objs=self.__check__(o.objs,c)
            except:
                pass
        return lst
    def __findFirstAddedNode__(self,id,node,lv=0):
        if node is None:
            return None
        if id==long(self.docExt.getKey(node)):
            return node
        for c in self.docExt.getChildsAttr(node,self.docExt.attr):
            if id==long(self.docExt.getKey(c)):
                return c
            else:
                if lv<self.iXmlLevel:
                    n=self.__findFirstAddedNode__(id,c,lv+1)
                    if n is not None:
                        return n
        return None
    def checkObjects(self):
        if self.nodeExt is None:
            return
        if len(self.objs)>0:
            #node=self.__findFirstAddedNode__(self.objs[0].id,self.nodeExt)
            node=self.nodeExt
            if node is None:
                self.objs=[]
            else:
                self.objs=self.__check__(self.objs,node)
        dlg=self.GetDlg()
        dlg.SetCanvas(self)
        dlg.checkObjects()
        pass
    
        
