#----------------------------------------------------------------------------
# Name:         vtDrawTimingNode.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060924
# CVS-ID:       $Id: vtDrawTimingNode.py,v 1.2 2006/09/26 05:05:59 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

import sys,os
import vidarc.tool.report.vRepLatex as vtRepLatex
import vidarc.tool.xml.vtXmlHierarchy as vtXmlHierarchy

from vidarc.tool.xml.vtXmlNodeTag import vtXmlNodeTag
try:
    if vcCust.is2Import(__name__):
        from vtDrawTimingPanel import *
        #from vtXmlNodeTagEditDialog import *
        #from vtXmlNodeTagAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

FACT=0.95

class vtDrawTimingNode(vtXmlNodeTag):
    #NODE_ATTRS=[
    #        ('Tag',None,'tag',None),
    #        ('Name',None,'name',None),
    #        ('Desc',None,'description',None),
    #    ]
    #FUNCS_GET_SET_4_LST=['Tag','Name']
    #FUNCS_GET_4_TREE=['Tag','Name']
    #FMT_GET_4_TREE=[('Tag',''),('Name','')]
    DFT_COLOR=[
        (178*FACT, 107*FACT, 000*FACT),
        (178*FACT, 145*FACT, 000*FACT),
        ( 76*FACT, 152*FACT, 127*FACT),
        ( 79*FACT,  79*FACT,  59*FACT),
        (120*FACT,  78*FACT, 146*FACT),
        ( 31*FACT, 156*FACT, 127*FACT),
        (178*FACT, 100*FACT, 178*FACT)
        ]
    DFT_DIS_COLOR=[
        (167, 100, 000),
        (167, 136, 000),
        ( 67, 135, 112),
        (116, 130, 147),
        (100, 166, 100),
        (150, 114,  22),
        (147,  82, 147)
        ]
    def __init__(self,tagName='drawTiming'):
        global _
        _=vtLgBase.assignPluginLang('vtDraw')
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'timing diagram')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def Is2Create(self):
        return False
    def Is2Add(self):
        return True
    def IsMultiple(self):
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return False
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00uIDAT8\x8d\xcdR1\x12\x80 \x0cKZ\xbf\xad\xa2\xf8l\xa5\x0e\xea\x9d\x03\
\x94\xa1\x0e\xe6\x8ecHi\x9b\x10R\x14\x11\x0c\xbd\x02\x9bFkq\\3\xbb\r\x9eBw\
\x03oJ\xb7y\xd4\x03\t\xbd\xc6-!\x97\xa5*a\x94\x995n\xc7\x0e\x00X$\x13\x14m\
\x9e\r\x9by<E\x7f !\xfc\x0b\xd5 Y:\xfc\\\x94\xeb\xe2\xaa\xed$2i3}o|cbwe\x07a\
\x13O\x0c\xe7'\xa4\x96; \xec\x00\x00\x00\x00IEND\xaeB`\x82" 
  
    def getSelImageData(self):
        return self.getImageData()

    def GetEditDialogClass(self):
        if GUI:
            return vtXmlNodeTag.GetEditDialogClass(self)
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return vtXmlNodeTag.GetAddDialogClass(self)
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vtDrawTimingPanel
        else:
            return None
    def Process(self,fOut,*args,**kwargs):
        try:
            if 0:
                vtLog.CallStack('')
                print args
                print kwargs
            node=kwargs['node']
            
            #grid=self.grid
            #max=self.max
            labels=[]
            lst=[]
            n=self.doc.getChild(node,'report')
            try:
                f=float(self.doc.getNodeText(n,'scale'))
            except:
                f=1.0
            fScale=f
            try:
                iY=int(self.doc.getNodeText(n,'fontsize'))
            except:
                iY=2
            iFontSize=iY
            try:
                iY=int(self.doc.getNodeText(n,'rotate'))
                if iY==1:
                    bRotate=True
            except:
                bRotate=False
            
            n=self.doc.getChild(node,'grid')
            try:
                iX=int(self.doc.getNodeText(n,'x'))
            except:
                iX=10
            try:
                iY=int(self.doc.getNodeText(n,'y'))
            except:
                iY=10
            grid=(iX,iY)
            
            n=self.doc.getChild(node,'max')
            try:
                iX=int(self.doc.getNodeText(n,'x'))
            except:
                iX=10
            try:
                iY=int(self.doc.getNodeText(n,'y'))
            except:
                iY=10
            max=(iX,iY)
            
            for m in ['label_x','label_y']:
                llst=[]
                n=self.doc.getChild(node,m)
                for c in self.doc.getChilds(n,'label'):
                    sPos=self.doc.getNodeText(c,'pos')
                    sLbl=self.doc.getNodeText(c,'name')
                    try:
                        llst.append((float(sPos),sLbl))
                    except:
                        pass
                labels.append(llst)
            
            for l in self.doc.getChilds(node,'curve'):
                sName=self.doc.getNodeText(l,'name')
                n=self.doc.getChild(l,'range')
                sRngMin=self.doc.getNodeText(n,'min')
                try:
                    fRngMin=float(sRngMin)
                except:
                    fRngMin=0
                sRngMax=self.doc.getNodeText(n,'max')
                try:
                    fRngMax=float(sRngMax)
                except:
                    fRngMax=1
                
                llst=[]
                for v in self.doc.getChilds(l,'value'):
                    sX=self.doc.getNodeText(v,'x')
                    sY=self.doc.getNodeText(v,'y')
                    try:
                        llst.append([float(sX),float(sY)])
                    except:
                        llst.append([-1,-1])
                lst.append({'name':sName,'range':(fRngMin,fRngMax),'values':llst})
            
            maxWidth=grid[0]*max[0]
            maxHeight=grid[1]*max[1]
            if iFontSize==0:
                sFontSize='\\tiny'
            elif iFontSize==1:
                sFontSize='\\scriptsize'
            elif iFontSize==3:
                sFontSize='\\scriptsize'
            elif iFontSize==4:
                sFontSize='\\scriptsize'
            else:
                sFontSize='\\small'
            xMax=0
            yMax=0
            xMin=sys.maxint
            yMin=sys.maxint
            scaleX=fScale
            scaleY=fScale
            ofs=(50,10)
            strs=[]
            xMin,yMin=0,0
            xMax,yMax=maxWidth+ofs[0],maxHeight+ofs[1]
            xMin*=scaleX
            yMin*=scaleY
            xMax*=scaleX
            yMax*=scaleY
            strs=['\\begin{figure}[!hb]','  \\begin{picture}(%d,%d)'%(xMax,yMax)]
            dx=maxWidth
            strs.append('    \\thinlines')
            for i in range(max[1]):
                x=ofs[0]
                y=ofs[1]+grid[1]*i
                c=(0xD0,0xD0,0xD0)
                tup=vtRepLatex.lineFitPict2(x*scaleX,y*scaleY,(x+dx)*scaleX,y*scaleY)
                tup=(tup[0],tup[1],float(c[0]/255.0) ,float(c[1]/255.0) ,float(c[2]/255.0) ,tup[2],tup[3],tup[4])
                strs.append('    \\put(%4.1f,%4.1f){\\color[rgb]{%3.2f,%3.2f,%3.2f}\\line(%d,%d){%4.1f}}'%tup)
            dy=maxHeight
            for i in range(max[0]):
                x=ofs[0]+grid[0]*i
                y=ofs[1]
                c=(0xD0,0xD0,0xD0)
                tup=vtRepLatex.lineFitPict2(x*scaleX,y*scaleY,x*scaleX,(y+dy)*scaleY)
                tup=(tup[0],tup[1],float(c[0]/255.0) ,float(c[1]/255.0) ,float(c[2]/255.0) ,tup[2],tup[3],tup[4])
                strs.append('    \\put(%4.1f,%4.1f){\\color[rgb]{%3.2f,%3.2f,%3.2f}\\line(%d,%d){%4.1f}}'%tup)
            c=(0xC0,0xC0,0xC0)
            tup=vtRepLatex.lineFitPict2(ofs[0]*scaleX,(ofs[1]+maxHeight)*scaleY,
                    (ofs[0]+maxWidth)*scaleX,(ofs[1]+maxHeight)*scaleY)
            tup=(tup[0],tup[1],float(c[0]/255.0) ,float(c[1]/255.0) ,float(c[2]/255.0) ,tup[2],tup[3],tup[4])
            strs.append('    \\put(%4.1f,%4.1f){\\color[rgb]{%3.2f,%3.2f,%3.2f}\\line(%d,%d){%4.1f}}'%tup)
            tup=vtRepLatex.lineFitPict2((ofs[0]+maxWidth)*scaleX,ofs[1]*scaleY,
                    (ofs[0]+maxWidth)*scaleX,(ofs[1]+maxHeight)*scaleY)
            tup=(tup[0],tup[1],float(c[0]/255.0) ,float(c[1]/255.0) ,float(c[2]/255.0) ,tup[2],tup[3],tup[4])
            strs.append('    \\put(%4.1f,%4.1f){\\color[rgb]{%3.2f,%3.2f,%3.2f}\\line(%d,%d){%4.1f}}'%tup)
                
            dx,dy=int(ofs[0]*scaleX),int((ofs[1]-5)*scaleY)
            for lbl in labels[0]:
                x=0
                y=ofs[1]-dy/2.0+lbl[0]*grid[1]
                strs.append('    \\put(%4.1f,%4.1f){\\makebox(%d,%d)[r]{%s %s}}'%(x*scaleX,y*scaleY,dx,dy,sFontSize,lbl[1]))
            for lbl in labels[1]:
                x=dx/2.0+lbl[0]*grid[0]
                y=0
                strs.append('    \\put(%4.1f,%4.1f){\\makebox(%d,%d)[tc]{%s %s}}'%(x*scaleX,y*scaleY,dx,dy,sFontSize,lbl[1]))
            
            strs.append('    \\thicklines')
            i=0
            for dict in lst:
                rng=dict['range']
                oY=rng[0]*grid[1]
                dY=(rng[1]-rng[0])*grid[1]
                yMin=ofs[1]+oY
                yMax=ofs[1]+oY+dY
                yMid=ofs[1]+oY+dY*0.5
                xUndef=grid[0]*0.15
                dx,dy=int(ofs[0]*scaleX),int((ofs[1]-5)*scaleY)
                x=0
                y=ofs[1]-dy/2.0+(oY+dY/2.0)
                c=self.DFT_COLOR[i]
                strs.append('    \\put(%4.1f,%4.1f){\\color[rgb]{%3.2f,%3.2f,%3.2f}\\makebox(%d,%d)[r]{%s %s}}'%(x*scaleX,y*scaleY,float(c[0]/255.0) ,float(c[1]/255.0) ,float(c[2]/255.0),dx,dy,sFontSize,dict['name']))
                
                def addTexValLine(l):
                    tup=vtRepLatex.lineFitPict2(l[0]*scaleX,l[1]*scaleY,l[2]*scaleX,l[3]*scaleY)
                    c=self.DFT_COLOR[l[4]]
                    tup=(tup[0],tup[1],float(c[0]/255.0) ,float(c[1]/255.0) ,float(c[2]/255.0) ,tup[2],tup[3],tup[4])
                    strs.append('    \\put(%4.1f,%4.1f){\\color[rgb]{%3.2f,%3.2f,%3.2f}\\line(%d,%d){%4.1f}}'%tup)
    
                vals=dict['values']
                
                xa,ya=-1,-1
                for x,y in vals:
                    if x<0:
                        xa,ya=x,y
                        continue
                    # +++++++++++++++++++++++
                    if y<0:
                        x=ofs[0]+grid[0]*x
                        if xa>=0:
                            if ya<0:
                                l=[xa+xUndef,yMin,x-xUndef,yMin,i]
                                addTexValLine(l)
                                l=[x-xUndef,yMin,x,yMid,i]
                                addTexValLine(l)
                                l=[xa+xUndef,yMax,x-xUndef,yMax,i]
                                addTexValLine(l)
                                l=[x-xUndef,yMax,x,yMid,i]
                                addTexValLine(l)
                            else:
                                l=[x,ya,x,yMid,i]
                                addTexValLine(l)
                                
                            l=[x,yMid,x+xUndef,yMin,i]
                            addTexValLine(l)
                            l=[x,yMid,x+xUndef,yMax,i]
                            addTexValLine(l)
                    else:
                        y=ofs[1]+oY+(y*dY)
                        x=ofs[0]+grid[0]*x
                        if xa>=0:
                            if ya<0:
                                l=[xa+xUndef,yMin,x-xUndef,yMin,i]
                                addTexValLine(l)
                                l=[x-xUndef,yMin,x,yMid,i]
                                addTexValLine(l)
                                l=[xa+xUndef,yMax,x-xUndef,yMax,i]
                                addTexValLine(l)
                                l=[x-xUndef,yMax,x,yMid,i]
                                addTexValLine(l)
                                l=[x,yMid,x,y,i]
                                addTexValLine(l)
                            else:
                                l=[xa,ya,x,y,i]
                                addTexValLine(l)
                                
                    # -----------------------
                    #y=ofs[1]+oY+(y*dY)
                    #x=ofs[0]+grid[0]*x
                    #if xa>=0:
                    #    tup=vtRepLatex.lineFitPict2(xa*scaleX,ya*scaleY,x*scaleX,y*scaleY)
                    #    c=self.DFT_COLOR[i]
                    #    tup=(tup[0],tup[1],float(c[0]/255.0) ,float(c[1]/255.0) ,float(c[2]/255.0) ,tup[2],tup[3],tup[4])
                    #    strs.append('    \\put(%4.1f,%4.1f){\\color[rgb]{%3.2f,%3.2f,%3.2f}\\line(%d,%d){%4.1f}}'%tup)
                    xa,ya=x,y
                i+=1
                i=i%(len(self.DFT_COLOR))
            strs.append('  \\end{picture}')
            #if self.node is not None:
            #par=self.doc.getParent(node)
            par=node
            if '__base_node' in kwargs:
                baseNode=kwargs['__base_node']
            else:
                baseNode=self.doc.GetBaseNode(node)
            if '__rel_node' in kwargs:
                relNode=kwargs['__rel_node']
            else:
                relNode=self.doc.GetRelBaseNode(node)
            
            sHier=vtXmlHierarchy.getTagNamesWithRel(self.doc,baseNode,relNode,par)+ ' ' + \
                            self.doc.getNodeText(par,'name') + ' '+_(u'diagram')
            sHierFull=vtXmlHierarchy.getTagNames(self.doc,baseNode,None,par)+ ' ' + \
                            self.doc.getNodeText(par,'name') + ' '+_(u'diagram')
            #sCaption='  \\caption{%s}'%vtRepLatex.texReplace(sHier)
            #sRef='  \\label{fig:%s}'%vtRepLatex.texReplace4Lbl(sHierFull)
            #fig=['\\begin{figure}[!hb]',self.GenTexPic(),sCaption,sRef,'\\end{figure}']
            strs.append('  \\label{fig:%s}'%vtRepLatex.texReplace4Lbl(sHierFull))
            strs.append('  \\caption{%s}'%vtRepLatex.texReplace(sHier))
            strs.append('\\end{figure}')
            #return string.join(strs,os.linesep)
            fOut.write(os.linesep.join(strs))
        except:
            vtLog.vtLngTB(self.__class__.__name__)

