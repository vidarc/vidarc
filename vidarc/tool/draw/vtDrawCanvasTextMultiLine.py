#----------------------------------------------------------------------------
# Name:         vtDrawCanvasTextMultiLine.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtDrawCanvasTextMultiLine.py,v 1.3 2006/02/14 12:25:22 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import string
from vtDrawCanvasText import *
import vidarc.tool.report.vRepLatex as vtRepLatex

class vtDrawCanvasTextMultiLine(vtDrawCanvasText):
    def GetTypeName(self):
        # replace me
        return 'textMultiLine'
    def GetAttr(self,idx):
        if idx==6:
            return 'text',self.text,'%s',3
        else:
            return vtDrawCanvasText.GetAttr(self,idx)
    def SetAttr(self,idx,val):
        if idx==6:
            self.text=val
        else:
            vtDrawCanvasText.SetAttr(self,idx,val)
    def __draw__(self,canvas,aX=0,aY=0,aW=-1,aH=-1,iDriver=-1):
        if self.bVisible==False:
            return
        drvInfo=canvas.getDriverData(iDriver)
        drv=canvas.getDriver(iDriver)
        canvas.setActLayer(self.iLayer,self.bActive,iDriver)
        if drv==self.DRV_WX:
            dc=drvInfo['dc']
            if aW>=0 and aH>=0:
                ax,ay=canvas.calcCoor(aX,aY)
                aw,ah=canvas.calcCoor(aW,aH)
                dc.SetClippingRegion(ax,ay,aw+1,ah+1)
            x,y=canvas.calcCoor(self.iX+aX,self.iY+aY)
            w,h=canvas.calcCoor(self.iWidth,self.iHeight)
            
            wTe=0
            hTe=0
            for t in string.split(self.text,'\n'):
                te = dc.GetTextExtent(t)
                if te[0]>wTe:
                    wTe=te[0]
                hTe+=te[1]+4
            hTe-=4
            if w<0:
                w=wTe
                if self.alignHori==self.ALIGN_CENTER:
                    x-=w/2
                elif self.alignHori==self.ALIGN_LEFT:
                    pass
                elif self.alignHori==self.ALIGN_RIGHT:
                    x-=w
                else:
                    pass
            else:
                if self.alignHori==self.ALIGN_CENTER:
                    x+=w/2
                elif self.alignHori==self.ALIGN_LEFT:
                    pass
                elif self.alignHori==self.ALIGN_RIGHT:
                    x+=w
                else:
                    pass
            
            if h<0:
                h=hTe
                if self.alignVert==self.ALIGN_CENTER:
                    y-=h/2
                elif self.alignVert==self.ALIGN_TOP:
                    pass
                elif self.alignVert==self.ALIGN_BOTTOM:
                    y-=h
                else:
                    pass
            else:
                if self.alignVert==self.ALIGN_CENTER:
                    y+=h/2-hTe/2
                elif self.alignVert==self.ALIGN_TOP:
                    pass
                elif self.alignVert==self.ALIGN_BOTTOM:
                    y+=h-hTe
                else:
                    pass
            xH=x
            yH=y
            #if self.alignVert==self.ALIGN_CENTER:
            #    yH=y
            if self.alignHori==self.ALIGN_LEFT:
                xH+=self.margin
            elif self.alignHori==self.ALIGN_RIGHT:
                xH-=self.margin
            for t in string.split(self.text,'\n'):
                te = dc.GetTextExtent(t)
                if self.alignHori==self.ALIGN_CENTER:
                    xH=x-te[0]/2
                elif self.alignHori==self.ALIGN_LEFT:
                    pass
                elif self.alignHori==self.ALIGN_RIGHT:
                    xH=x-te[0]
                dc.DrawText(t, xH, yH)
                yH+=te[1]+4
            if aW>=0 and aH>=0:
                dc.DestroyClippingRegion()
        elif drv==self.DRV_LATEX:
            if aW>=0 and aH>=0:
                ax,ay=canvas.calcCoorLatex(aX,aY)
                aw,ah=canvas.calcCoorLatex(aW,aH)
            x,y=canvas.calcCoorLatex(self.iX+aX,self.iY+aY)
            w,h=canvas.calcCoorLatex(self.iWidth,self.iHeight)
            if w<0:
                w=0
            if h<0:
                h=0
            if self.alignVert==self.ALIGN_CENTER:
                sVert='c'
            elif self.alignVert==self.ALIGN_TOP:
                sVert='t'
            elif self.alignVert==self.ALIGN_BOTTOM:
                sVert='b'
            else:
                sVert='c'
            if self.alignHori==self.ALIGN_CENTER:
                sHoriz='c'
            elif self.alignHori==self.ALIGN_LEFT:
                sHoriz='l'
            elif self.alignHori==self.ALIGN_RIGHT:
                sHoriz='r'
            else:
                sHoriz='c'
            c=drvInfo['color']
            strs=drvInfo['strs']
            strTxt=vtRepLatex.texReplace(string.replace(self.text,'\n','\\\\'))
            while 1==1:
                strTxt=string.strip(strTxt)
                if strTxt[-2:]=='\\\\':
                    strTxt=strTxt[:-2]
                else:
                    break
            strs.append('    \\put(%4.1f,%4.1f){%s\\makebox(%d,%d)[%s%s]{%s \\shortstack[%s]{%s}}}'%(x,
                    drvInfo['y_max']-y-h,c,w,h,sVert,sHoriz,drvInfo['font_size'],
                    sHoriz,strTxt))
            
        pass
    
