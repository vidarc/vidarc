#----------------------------------------------------------------------------
# Name:         vtDrawCanvasBlock.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtDrawCanvasBlock.py,v 1.2 2007/07/28 14:43:29 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vtDrawCanvasXmlGroup import *
from vtDrawCanvasBlockIO import *
from vtDrawCanvasRectangle import *
import vidarc.tool.log.vtLog as vtLog

class vtDrawCanvasBlock(vtDrawCanvasXmlGroup):
    def __init__(self,id=-1,name='',x=0,y=0,w=0,h=0,layer=0):
        if w<=0:
            w=1+3+1+3+1+3+1
        vtDrawCanvasXmlGroup.__init__(self,id,name,x,y,w,h,layer)
        self.rect=vtDrawCanvasRectangle(id,'__border',1,y,w-2,h,layer)
        #self.Add(self.rect)
        self.iWidthParam=[3,3,3]
    def GetWidthParam(self,idx):
        try:
            return self.iWidthParam[idx]
        except:
            return 3
    def SetWidthParam(self,idx,val):
        try:
            self.iWidthParam[idx]=val
            #self.calcPosition(None,canvas)
        except:
            pass
    def GetAttr(self,idx):
        if idx<5:
            return vtDrawCanvasGroup.GetAttr(self,idx)
        if idx==5:
            return 'width input',self.GetWidthParam(0),'%d',1
        if idx==6:
            return 'width parameter',self.GetWidthParam(1),'%d',1
        if idx==7:
            return 'width output',self.GetWidthParam(2),'%d',1
        return '','','',-1
    def SetAttr(self,idx,val):
        if idx<5:
            return vtDrawCanvasGroup.SetAttr(self,idx,val)
        if idx==5:
            self.SetWidthParam(0,val)
        if idx==6:
            self.SetWidthParam(1,val)
        if idx==7:
            self.SetWidthParam(2,val)
        return -1
    def SetX(self,val):
        vtDrawCanvasXmlGroup.SetX(self,val)
        self.rect.SetX(val+1)
    def SetY(self,val):
        vtDrawCanvasXmlGroup.SetY(self,val)
        self.rect.SetY(val)
    def SetWidth(self,val):
        vtDrawCanvasXmlGroup.SetWidth(self,val)
        self.rect.SetWidth(val-2)
    def SetHeight(self,val):
        vtDrawCanvasXmlGroup.SetHeight(self,val)
        self.rect.SetHeight(val)
    def GetInsideX(self):
        return vtDrawCanvasXmlGroup.GetX(self)+1
    def GetInsideY(self):
        return vtDrawCanvasXmlGroup.GetY(self)
    def GetInsideWidth(self):
        return vtDrawCanvasXmlGroup.GetWidth(self)-2
    def GetInsideHeight(self):
        return vtDrawCanvasXmlGroup.GetHeight(self)
    def SetVisible(self,flag):
        vtDrawCanvasXmlGroup.SetVisible(self,flag)
        self.rect.SetVisible(flag)
    def GetDefaults(self):
        return 'width_input:%d,width_parameter:%d,width_output:%d'%(self.GetWidthParam(0),self.GetWidthParam(1),self.GetWidthParam(2))
    def SetDefaults(self,s):
        strs=string.split(s,',')
        for sd in strs:
            s2=string.split(sd,':')
            try:
                if s2[0]=='width_input':
                    self.SetWidthParam(0,int(s2[1]))
                if s2[0]=='width_parameter':
                    self.SetWidthParam(1,int(s2[1]))
                if s2[0]=='width_output':
                    self.SetWidthParam(2,int(s2[1]))
            except:
                pass
    def __draw__(self,canvas,aX=0,aY=0,aW=-1,aH=-1,iDriver=-1):
        vtDrawCanvasXmlGroup.__draw__(self,canvas,aX,aY,aW,aH,iDriver)
        self.rect.__draw__(canvas,aX,aY,aW,aH,iDriver)
    
    def AddAttr(self,sAttrName,sAttrVal,defaults,type,display,canvas):
        i=-1
        iMax=[0,0,0,0]
        
        for o in self.objsXml:
            #print o,isinstance(o, vtDrawCanvasBlockIO)
            if isinstance(o, vtDrawCanvasBlockIO):
                #print o.type,type
                if type==o.type:
                    if o.order>i:
                        i=o.order
                if o.order>iMax[o.type]:
                    iMax[o.type]=o.order
        #vtLog.CallStack(str(iMax))
        h=2
        w=3
        if type==vtDrawCanvasBlockIO.TYPE_IN:
            x=1
            y=0
        elif type==vtDrawCanvasBlockIO.TYPE_OUT:
            x=self.iWidth-1
            y=0
        elif type==vtDrawCanvasBlockIO.TYPE_INOUT:
            x=1
            y=0
        elif type==vtDrawCanvasBlockIO.TYPE_PARAM:
            x=round(self.iWidth/2)
            y=0
        o=vtDrawCanvasBlockIO(id=-1,name=sAttrName,text=sAttrVal,x=x,y=y,w=w,h=h,layer=self.GetLayer(),
                type=type,order=i+1,display=display)  #vtDrawCanvasBlockIO.TYPE_IN
        o.SetDefaults(defaults)
        self.objsXml.append(o)
        self.calcPosition(o,canvas)
        #self.objsXml.append(o)
        #o=vtDrawCanvasText(id=-1,name='attr_%s_val'%sAttrName,text=sAttrVal,x=o.GetX(),y=o.GetY()+1,w=3,h=1,layer=o.GetLayer(),
        #        align_horizontal=vtDrawCanvasText.ALIGN_LEFT,align_vertical=vtDrawCanvasText.ALIGN_TOP)
        #self.objsXml.append(o)
        #iX,iY,iW,iH=self.calcExtend(canvas)
        #self.SetWidth(iW)
        #self.SetHeight(iH)
    def DelAttr(self,sAttrName,canvas):
        lst=[]
        sAttrName=sAttrName
        iLen=len(sAttrName)
        for o in self.objsXml:
            if o.GetName()[:iLen]!=sAttrName:
                lst.append(o)
            else:
                obj2Del=o
                iDelType=o.type
                iDelOrder=o.order
        iOrder=iDelOrder
        for o in self.objsXml:
            if isinstance(o, vtDrawCanvasBlockIO):
                if o.type==iDelType:
                    if o.order>iDelOrder:
                        o.order=iOrder
                        iOrder+=1
        self.objsXml=lst
        self.calcPosition(o,canvas)
        #iX,iY,iW,iH=self.calcExtend(canvas)
        #self.SetWidth(iW)
        #self.SetHeight(iH)
    def GetTypeName(self):
        # replace me
        return 'block'
    
    def SetNode(self,doc,node,canvas):
        vtDrawCanvasXmlGroup.SetNode(self,doc,node,canvas)
        sVal=doc.getNodeText(node,'width_input')
        try:
            self.SetWidthParam(0,int(sVal))
        except:
            self.SetWidthParam(0,3)
        sVal=doc.getNodeText(node,'width_param')
        try:
            self.SetWidthParam(1,int(sVal))
        except:
            self.SetWidthParam(1,3)
        sVal=doc.getNodeText(node,'width_output')
        try:
            self.SetWidthParam(2,int(sVal))
        except:
            self.SetWidthParam(2,3)
        self.calcPosition(None,canvas)
    def GetNode(self,doc,parNode,canvas):
        node=vtDrawCanvasXmlGroup.GetNode(self,doc,parNode,canvas)
        doc.setNodeText(node,'width_input',str(self.GetWidthParam(0)))
        doc.setNodeText(node,'width_param',str(self.GetWidthParam(1)))
        doc.setNodeText(node,'width_output',str(self.GetWidthParam(2)))
        return node
    def calcExtend(self,canvas):
        try:
            sOrigin=canvas.GetName()
        except:
            sOrigin=''
        #vtLog.CallStack(self)
        #print self
        x,y,iW,iH=vtDrawCanvasGroup.calcExtend(self,canvas)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(None,vtLog.DEBUG,
                'calcExtend,group extend,x=%d,y=%d,w=%d,h=%d'%(x,y,iW,iH),
                origin=sOrigin)
        for o in self.objsXml:
            xa,ya,xe,ye=o.calcExtend(canvas)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCallStack(None,vtLog.DEBUG,
                    'calcExtend,obj %s,x=%d,y=%d,w=%d,h=%d'%(o.GetTypeName(),xa,ya,xe,ye),
                    origin=sOrigin)
            if xe>iW:
                iW=xe
            if ye>iH:
                iH=ye
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(None,vtLog.DEBUG,
                'calcExtend,group extend result,x=%d,y=%d,w=%d,h=%d'%(0,0,iW,iH),
                origin=sOrigin)
        #print self
        return 0,0,iW,iH
    def calcPosition(self,obj,canvas):
        try:
            #sOrigin=canvas.GetName()
            sOrigin=canvas.name
        except:
            sOrigin=''
        #vtLog.CallStack(self)
        #print self
        max=canvas.GetMax()
        if obj is not None:
            oiXa,oiYa,oiXe,oiYe=obj.__getExtend__(canvas)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCallStack(None,vtLog.DEBUG,
                    'calcPosition,group extend,xa=%d,ya=%d,xe=%d,ye=%d'%(oiXa,oiYa,oiXe,oiYe),
                    origin=sOrigin)
        iMax=[0,0,0,0]
        for o in self.objsXml:
            if isinstance(o, vtDrawCanvasBlockIO):
                #if o.type==vtDrawCanvasBlockIO.TYPE_INOUT:
                #    order=o.order+1
                #else:
                #    order=o.order
                if o.order>=iMax[o.type]:
                    iMax[o.type]=o.order+1
        i=0
        for max in iMax[:-1]:
            if i<max:
                i=max
        if i<(iMax[0]+iMax[3]):
            i=iMax[0]+iMax[3]
        self.SetHeight(i+i)
        
        i=1
        lstX=[0,0,0]
        for j in range(3):
            #print j,iMax[j]
            if iMax[j]>0:
                lstX[j]=i
                i+=self.GetWidthParam(j)+1
        self.SetWidth(i)
        for o in self.objsXml:
            if isinstance(o, vtDrawCanvasBlockIO):
                if o.type==vtDrawCanvasBlockIO.TYPE_IN:
                    o.SetX(lstX[0])
                    o.SetY(o.order*2)
                    o.SetWidth(self.GetWidthParam(0))
                    o.alignHori=o.ALIGN_LEFT
                elif o.type==vtDrawCanvasBlockIO.TYPE_OUT:
                    #o.SetX(lstX[2]-1-o.iWidth)
                    o.SetX(lstX[2])
                    o.SetY(o.order*2)
                    o.SetWidth(self.GetWidthParam(2))
                    o.alignHori=o.ALIGN_RIGHT
                elif o.type==vtDrawCanvasBlockIO.TYPE_INOUT:
                    o.SetX(lstX[0])
                    o.SetY((iMax[0]+o.order)*2)
                    o.SetWidth(self.GetWidthParam(0))
                    o.alignHori=o.ALIGN_LEFT
                elif o.type==vtDrawCanvasBlockIO.TYPE_PARAM:
                    o.SetX(lstX[1])
                    o.SetY(o.order*2)
                    o.SetWidth(self.GetWidthParam(1))
                    o.alignHori=o.ALIGN_CENTER
        for o in self.objs:
            iXa,iYa,iXe,iYe=o.__getExtend__(canvas)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCallStack(None,vtLog.DEBUG,
                    'calcPosition,obj %s,xa=%d,ya=%d,xe=%d,ye=%d'%(o.GetTypeName(),iXa,iYa,iXe,iYe),
                    origin=sOrigin)
            if iXe>self.GetWidth():
                self.SetWidth(self.GetWidth()+iXe-self.GetWidth())
            if iYe>self.GetHeight():
                self.SetHeight(self.GetHeight()+iYe-self.GetHeight())
        #for o in self.objsXml:
        #    iXa,iYa,iXe,iYe=o.__getExtend__(canvas)
        #    if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #        vtLog.vtLngCallStack(None,vtLog.DEBUG,
        #            'calcPosition,obj %s,xa=%d,ya=%d,xe=%d,ye=%d'%(o.GetTypeName(),iXa,iYa,iXe,iYe),
        #            origin=sOrigin)
        #    if iXe>self.GetInsideWidth():
        #        self.SetWidth(self.GetWidth()+iXe-self.GetInsideWidth())
        #    if iYe>self.GetInsideHeight():
        #        self.SetHeight(self.GetHeight()+iYe-self.GetInsideHeight())
            
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(None,vtLog.DEBUG,
                'calcPosition,block extend,x=%d,y=%d,w=%d,h=%d'%(self.GetX(),self.GetY(),self.GetWidth(),self.GetHeight()),
                origin=sOrigin)
        
        #self.SetHeight(w)
        #self.SetWidth(1+3+1+3+1+3+1)
        #print self
        return True
    def __str__(self):
        return self.GetTypeName()+' name:'+self.name+' id:'+str(self.id)+' (%d,%d),(%d,%d) %d'%(self.iX,self.iY,self.iWidth,self.iHeight,self.bVisible)
    
