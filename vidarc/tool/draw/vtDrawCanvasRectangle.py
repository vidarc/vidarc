#----------------------------------------------------------------------------
# Name:         vtDrawCanvasRectangle.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtDrawCanvasRectangle.py,v 1.3 2006/02/14 12:25:22 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vtDrawCanvasObjectBase import *
import vidarc.tool.report.vRepLatex as vtRepLatex

class vtDrawCanvasRectangle(vtDrawCanvasObjectBase):
    def __init__(self,id=-1,name='',x=0,y=0,w=0,h=0,layer=0):
        vtDrawCanvasObjectBase.__init__(self,id,name,x,y,w,h,layer)
    def GetTypeName(self):
        # replace me
        return 'rectangle'
    def __draw__(self,canvas,aX=0,aY=0,aW=-1,aH=-1,iDriver=-1):
        if self.bVisible==False:
            return
        vtDrawCanvasObjectBase.__drawSelect__(self,canvas,aX=aX,aY=aY,aW=aW,aH=aH,iDriver=iDriver)
        drvInfo=canvas.getDriverData(iDriver)
        drv=canvas.getDriver(iDriver)
        canvas.setActLayer(self.iLayer,self.bActive,iDriver)
        if drv==self.DRV_WX:
            dc=drvInfo['dc']
            if aW>=0 and aH>=0:
                ax,ay=canvas.calcCoor(aX,aY)
                aw,ah=canvas.calcCoor(aW,aH)
                dc.SetClippingRegion(ax,ay,aw+1,ah+1)
            x,y=canvas.calcCoor(self.iX+aX,self.iY+aY)
            w,h=canvas.calcCoor(self.iWidth,self.iHeight)
            dc.DrawLine(x,y,x+w,y)
            dc.DrawLine(x+w,y,x+w,y+h)
            dc.DrawLine(x,y+h,x+w,y+h)
            dc.DrawLine(x,y,x,y+h)
            if aW>=0 and aH>=0:
                dc.DestroyClippingRegion()
        elif drv==self.DRV_LATEX:
            if aW>=0 and aH>=0:
                ax,ay=canvas.calcCoorLatex(aX,aY)
                aw,ah=canvas.calcCoorLatex(aW,aH)
            x,y=canvas.calcCoorLatex(self.iX+aX,self.iY+aY)
            w,h=canvas.calcCoorLatex(self.iWidth,self.iHeight)
            y=drvInfo['y_max']-y-h
            c=drvInfo['color']
            strs=drvInfo['strs']
            tup=vtRepLatex.lineFitPict2(x,y,x+w,y)
            strs.append('    \\put(%4.1f,%4.1f){%s\\line(%d,%d){%4.1f}}'%(tup[0],tup[1],c,tup[2],tup[3],tup[4]))
            tup=vtRepLatex.lineFitPict2(x+w,y,x+w,y+h)
            strs.append('    \\put(%4.1f,%4.1f){%s\\line(%d,%d){%4.1f}}'%(tup[0],tup[1],c,tup[2],tup[3],tup[4]))
            tup=vtRepLatex.lineFitPict2(x,y+h,x+w,y+h)
            strs.append('    \\put(%4.1f,%4.1f){%s\\line(%d,%d){%4.1f}}'%(tup[0],tup[1],c,tup[2],tup[3],tup[4]))
            tup=vtRepLatex.lineFitPict2(x,y,x,y+h)
            strs.append('    \\put(%4.1f,%4.1f){%s\\line(%d,%d){%4.1f}}'%(tup[0],tup[1],c,tup[2],tup[3],tup[4]))
            pass
    
