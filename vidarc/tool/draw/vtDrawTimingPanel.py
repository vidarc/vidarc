#Boa:FramePanel:vtDrawTimingPanel
#----------------------------------------------------------------------------
# Name:         vtDrawTimingPanel.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vtDrawTimingPanel.py,v 1.8 2010/04/10 12:32:14 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors
import traceback,time
import string,math,os

from vidarc.tool.draw.vtDrawTimingDialog import *
from vidarc.tool.draw.vtDrawDiagramPanel import *

import vidarc.tool.report.vRepLatex as vtRepLatex
import vidarc.tool.xml.vtXmlDom as vtXmlDom
import vidarc.tool.xml.vtXmlHierarchy as vtXmlHierarchy
import vidarc.tool.log.vtLog as vtLog

VERBOSE=0
BUFFERED=1
FACT=0.95
class vtDrawTimingPanel(vtDrawDiagramPanel):
    DFT_COLOR=[
        (178*FACT, 107*FACT, 000*FACT),
        (178*FACT, 145*FACT, 000*FACT),
        ( 76*FACT, 152*FACT, 127*FACT),
        ( 79*FACT,  79*FACT,  59*FACT),
        (120*FACT,  78*FACT, 146*FACT),
        ( 31*FACT, 156*FACT, 127*FACT),
        (178*FACT, 100*FACT, 178*FACT)
        ]
    DFT_DIS_COLOR=[
        (167, 100, 000),
        (167, 136, 000),
        ( 67, 135, 112),
        (116, 130, 147),
        (100, 166, 100),
        (150, 114,  22),
        (147,  82, 147)
        ]
    def GetDlg(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.dlg is None:
            self.dlg=vtDrawTimingDialog(self)
            self.dlg.SetName(self.name)
        return self.dlg
    def __openDlg__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        iY=(self.y-self.ofs[1])/self.grid[1]
        if iY>self.max[1]:
            iY=-1
        iX=(self.x-self.ofs[0])/self.grid[0]
        if iX>self.max[0]:
            iX=-1
            
        dlg=self.GetDlg()
        dlg.Centre()
        self.__setDlgValues__()
        if dlg.ShowModal()>0:
            if self.__getDlgValues__()>0:
                if 1==1:
                    cdc = wx.ClientDC(self.scwDrawing)
                    self.scwDrawing.DoPrepareDC(cdc)
                    dc = wx.BufferedDC(cdc, self.buffer)
                    dc.BeginDrawing()
                    dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
                    dc.Clear()
                    dc.EndDrawing()
                else:
                    self.Clear()
                self.__calcSize__()
                #self.__drawValues__()
                self.scwDrawing.Refresh()
            else:
                cdc = wx.ClientDC(self.scwDrawing)
                self.scwDrawing.DoPrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
                dc.BeginDrawing()
                dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
                dc.Clear()
                self.__drawGrid__(dc,printing=False)
                self.__drawGridText__(dc,printing=False)
                self.__drawValues__(dc,printing=False)
                
                dc.EndDrawing()
            self.__setModified__(True)
            wx.PostEvent(self,vtDrawDiagramChanged(self))
                
    def __drawGridText__(self,dc,printing):
        vtDrawDiagramPanel.__drawGridText__(self,dc,printing)
    
    def __drawValues__(self,dc,printing):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        if self.lst is None:
            return
        self.selectableBlks=[]
        self.selectedBlk=None
        
        dc.SetFont(wx.Font(7, wx.SWISS, wx.NORMAL, wx.NORMAL))
        i=0
        for dict in self.lst:
            rng=dict['range']
            oY=self.maxHeight-rng[0]*self.grid[1]
            dY=(rng[1]-rng[0])*self.grid[1]
            sName=dict['name']
            te = dc.GetTextExtent(sName)
            x=self.ofs[0]-te[0]-2
            y=self.ofs[1]+oY-dY/2-te[1]/2+1
            color=self.color[i%(len(self.DFT_COLOR))]
            dc.SetTextForeground(color)
            dc.DrawText(sName, x, y)
            
            vals=dict['values']
            xa,ya=-1,0
            yMin=self.ofs[1]+oY
            yMid=self.ofs[1]+oY-(0.5*dY)
            yMax=self.ofs[1]+oY-dY
            xUndef=self.grid[0]*0.15
            for x,y in vals:
                if x<0:
                    xa,ya=x,ya
                    continue
                if y<0:
                    x=self.ofs[0]+self.grid[0]*x
                    if xa>=0:
                        if ya<0:
                            l=[xa+xUndef,yMin,x-xUndef,yMin,i]
                            self.__draw__(l,False,dc,False)
                            l=[x-xUndef,yMin,x,yMid,i]
                            self.__draw__(l,False,dc,False)
                            l=[xa+xUndef,yMax,x-xUndef,yMax,i]
                            self.__draw__(l,False,dc,False)
                            l=[x-xUndef,yMax,x,yMid,i]
                            self.__draw__(l,False,dc,False)
                        else:
                            l=[x,ya,x,yMid,i]
                            self.__draw__(l,False,dc,False)
                            
                        l=[x,yMid,x+xUndef,yMin,i]
                        self.__draw__(l,False,dc,False)
                        l=[x,yMid,x+xUndef,yMax,i]
                        self.__draw__(l,False,dc,False)
                else:
                    y=self.ofs[1]+oY-(y*dY)
                    x=self.ofs[0]+self.grid[0]*x
                    if xa>=0:
                        if ya<0:
                            l=[xa+xUndef,yMin,x-xUndef,yMin,i]
                            self.__draw__(l,False,dc,False)
                            l=[x-xUndef,yMin,x,yMid,i]
                            self.__draw__(l,False,dc,False)
                            l=[xa+xUndef,yMax,x-xUndef,yMax,i]
                            self.__draw__(l,False,dc,False)
                            l=[x-xUndef,yMax,x,yMid,i]
                            self.__draw__(l,False,dc,False)
                            l=[x,yMid,x,y,i]
                            self.__draw__(l,False,dc,False)
                        else:
                            l=[xa,ya,x,y,i]
                            self.__draw__(l,False,dc,False)
                xa,ya=x,y
            i+=1

    def Clear(self):
        vtDrawDiagramPanel.Clear(self)
    def ClearInt(self):
        #self.labels=[]
        self.lst=[]
    def SetValues(self,lst):
        #print 'setvale',lst
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        self.lst=lst
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if BUFFERED:
            #if self.buffer is None:
                # Initialize the buffer bitmap.  No real DC is needed at this point.
            #    self.buffer = wx.EmptyBitmap(self.maxWidth+2, self.maxHeight+2+self.sumY)
            #    dc = wx.BufferedDC(None, self.buffer)
            #    dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
            #    dc.Clear()
            #    self.DoDrawing(dc)
            #else:
                cdc = wx.ClientDC(self.scwDrawing)
                self.scwDrawing.DoPrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
                dc.BeginDrawing()
                self.__clear__(dc,False)
                #dc.EndDrawing()
            
                self.__drawGrid__(dc,False)
                self.__drawGridText__(dc,False)
                self.__drawValues__(dc,False)
                dc.EndDrawing()
            
        
        self.scwDrawing.Refresh()
        #self.OnPaint()
    def GetValue(self):
        return self.lst
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            grid=self.grid
            max=self.max
            self.labels=[]
            self.lst=[]
            
            n=self.doc.getChild(node,'report')
            try:
                f=float(self.doc.getNodeText(n,'scale'))
            except:
                f=1.0
            self.fScale=f
            try:
                iY=int(self.doc.getNodeText(n,'fontsize'))
            except:
                iY=2
            self.iFontSize=iY
            try:
                iY=int(self.doc.getNodeText(n,'rotate'))
                if iY==1:
                    self.bRotate=True
            except:
                self.bRotate=False
            
            n=self.doc.getChild(node,'grid')
            try:
                iX=int(self.doc.getNodeText(n,'x'))
            except:
                iX=10
            try:
                iY=int(self.doc.getNodeText(n,'y'))
            except:
                iY=10
            self.grid=(iX,iY)
            
            n=self.doc.getChild(node,'max')
            try:
                iX=int(self.doc.getNodeText(n,'x'))
            except:
                iX=10
            try:
                iY=int(self.doc.getNodeText(n,'y'))
            except:
                iY=10
            self.max=(iX,iY)
            
            for m in ['label_x','label_y']:
                lst=[]
                n=self.doc.getChild(node,m)
                for c in self.doc.getChilds(n,'label'):
                    sPos=self.doc.getNodeText(c,'pos')
                    sLbl=self.doc.getNodeText(c,'name')
                    try:
                        lst.append((float(sPos),sLbl))
                    except:
                        pass
                self.labels.append(lst)
            
            for l in self.doc.getChilds(node,'curve'):
                sName=self.doc.getNodeText(l,'name')
                n=self.doc.getChild(l,'range')
                sRngMin=self.doc.getNodeText(n,'min')
                try:
                    fRngMin=float(sRngMin)
                except:
                    fRngMin=0
                sRngMax=self.doc.getNodeText(n,'max')
                try:
                    fRngMax=float(sRngMax)
                except:
                    fRngMax=1
                
                lst=[]
                for v in self.doc.getChilds(l,'value'):
                    sX=self.doc.getNodeText(v,'x')
                    sY=self.doc.getNodeText(v,'y')
                    try:
                        lst.append([float(sX),float(sY)])
                    except:
                        lst.append([-1,-1])
                self.lst.append({'name':sName,'range':(fRngMin,fRngMax),'values':lst})
            bResize=False
            if max!=self.max:
                bResize=True
            if grid!=self.grid:
                bResize=True
            if bResize:
                self.__calcSize__()
            else:
                pass
            cdc = wx.ClientDC(self.scwDrawing)
            self.scwDrawing.DoPrepareDC(cdc)
            dc = wx.BufferedDC(cdc, self.buffer)
            dc.BeginDrawing()
            #dc.SetBackground(wx.Brush(self.scwDrawing.GetBackgroundColour()))
            #dc.Clear()
            #self.__drawGrid__(dc,printing=False)
            #self.__drawGridText__(dc,printing=False)
            self.__drawValues__(dc,printing=False)
                
            dc.EndDrawing()
            self.scwDrawing.Refresh()
            
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            n=self.doc.getChildForced(node,'report')
            self.doc.setNodeText(n,'scale','%4.2f'%self.fScale)
            self.doc.setNodeText(n,'fontsize','%d'%self.iFontSize)
            if self.bRotate:
                self.doc.setNodeText(n,'rotate','1')
            else:
                self.doc.setNodeText(n,'rotate','0')            
            
            n=self.doc.getChildForced(node,'grid')
            self.doc.setNodeText(n,'x','%d'%self.grid[0])
            self.doc.setNodeText(n,'y','%d'%self.grid[1])
            
            n=self.doc.getChildForced(node,'max')
            self.doc.setNodeText(n,'x','%d'%self.max[0])
            self.doc.setNodeText(n,'y','%d'%self.max[1])
            
            for m in ['label_x','label_y']:
                n=self.doc.getChildForced(node,m)
                childs=self.doc.getChilds(n,'label')
                iLen=len(childs)
                iPos=0
                if m=='label_x':
                    lbls=self.labels[0]
                else:
                    lbls=self.labels[1]
                for it in lbls:
                    if iPos<iLen:
                        # replace existing node info
                        c=childs[iPos]
                    else:
                        c=self.doc.createSubNode(n,'label')
                    self.doc.setNodeText(c,'pos','%4.2f'%it[0])
                    self.doc.setNodeText(c,'name',it[1])
                    iPos+=1
                # delete node
                for i in range(iPos,iLen):
                    #self.doc.deleteNode(childs[i],n)
                    self.doc.__deleteNode__(childs[i],n)
            childs=self.doc.getChilds(node,'curve')
            iLen=len(childs)
            iPos=0
            for curveDict in self.lst:
                if iPos<iLen:
                    c=childs[iPos]
                else:
                    c=self.doc.createSubNode(node,'curve')
                self.doc.setNodeText(c,'name',curveDict['name'])
                nRng=self.doc.getChildForced(c,'range')
                self.doc.setNodeText(nRng,'min','%6.2f'%curveDict['range'][0])
                self.doc.setNodeText(nRng,'max','%6.2f'%curveDict['range'][1])
                
                values=self.doc.getChilds(c,'value')
                iValLen=len(values)
                iValPos=0
                for it in curveDict['values']:
                    if iValPos<iValLen:
                        nVal=values[iValPos]
                    else:
                        nVal=self.doc.createSubNode(c,'value')
                    self.doc.setNodeText(nVal,'x','%6.2f'%it[0])
                    self.doc.setNodeText(nVal,'y','%6.2f'%it[1])
                    iValPos+=1
                # delete not needed values node
                for i in range(iValPos,iValLen):
                    #self.doc.deleteNode(values[i],c)
                    self.doc.__deleteNode__(values[i],c)
                iPos+=1
            # delete not needed curve node
            for i in range(iPos,iLen):
                #self.doc.deleteNode(childs[i],node)
                self.doc.__deleteNode__(childs[i],node)
            #self.doc.AlignNode(node,iRec=5)
        except:
            self.__logTB__()
    
    def GenTexPic(self):
        if self.iFontSize==0:
            sFontSize='\\tiny'
        elif self.iFontSize==1:
            sFontSize='\\scriptsize'
        elif self.iFontSize==3:
            sFontSize='\\scriptsize'
        elif self.iFontSize==4:
            sFontSize='\\scriptsize'
        else:
            sFontSize='\\small'
        xMax=0
        yMax=0
        xMin=sys.maxint
        yMin=sys.maxint
        scaleX=self.fScale
        scaleY=self.fScale
        strs=[]
        xMin,yMin=0,0
        xMax,yMax=self.maxWidth+self.ofs[0],self.maxHeight+self.ofs[1]
        xMin*=scaleX
        yMin*=scaleY
        xMax*=scaleX
        yMax*=scaleY
        strs=['  \\begin{picture}(%d,%d)'%(xMax,yMax)]
        dx=self.maxWidth
        strs.append('    \\thinlines')
        for i in range(self.max[1]):
            x=self.ofs[0]
            y=self.ofs[1]+self.grid[1]*i
            c=(0xD0,0xD0,0xD0)
            tup=vtRepLatex.lineFitPict2(x*scaleX,y*scaleY,(x+dx)*scaleX,y*scaleY)
            tup=(tup[0],tup[1],float(c[0]/255.0) ,float(c[1]/255.0) ,float(c[2]/255.0) ,tup[2],tup[3],tup[4])
            strs.append('    \\put(%4.1f,%4.1f){\\color[rgb]{%3.2f,%3.2f,%3.2f}\\line(%d,%d){%4.1f}}'%tup)
        dy=self.maxHeight
        for i in range(self.max[0]):
            x=self.ofs[0]+self.grid[0]*i
            y=self.ofs[1]
            c=(0xD0,0xD0,0xD0)
            tup=vtRepLatex.lineFitPict2(x*scaleX,y*scaleY,x*scaleX,(y+dy)*scaleY)
            tup=(tup[0],tup[1],float(c[0]/255.0) ,float(c[1]/255.0) ,float(c[2]/255.0) ,tup[2],tup[3],tup[4])
            strs.append('    \\put(%4.1f,%4.1f){\\color[rgb]{%3.2f,%3.2f,%3.2f}\\line(%d,%d){%4.1f}}'%tup)
        c=(0xC0,0xC0,0xC0)
        tup=vtRepLatex.lineFitPict2(self.ofs[0]*scaleX,(self.ofs[1]+self.maxHeight)*scaleY,
                (self.ofs[0]+self.maxWidth)*scaleX,(self.ofs[1]+self.maxHeight)*scaleY)
        tup=(tup[0],tup[1],float(c[0]/255.0) ,float(c[1]/255.0) ,float(c[2]/255.0) ,tup[2],tup[3],tup[4])
        strs.append('    \\put(%4.1f,%4.1f){\\color[rgb]{%3.2f,%3.2f,%3.2f}\\line(%d,%d){%4.1f}}'%tup)
        tup=vtRepLatex.lineFitPict2((self.ofs[0]+self.maxWidth)*scaleX,self.ofs[1]*scaleY,
                (self.ofs[0]+self.maxWidth)*scaleX,(self.ofs[1]+self.maxHeight)*scaleY)
        tup=(tup[0],tup[1],float(c[0]/255.0) ,float(c[1]/255.0) ,float(c[2]/255.0) ,tup[2],tup[3],tup[4])
        strs.append('    \\put(%4.1f,%4.1f){\\color[rgb]{%3.2f,%3.2f,%3.2f}\\line(%d,%d){%4.1f}}'%tup)
            
        dx,dy=int(self.ofs[0]*scaleX),int((self.ofs[1]-5)*scaleY)
        for lbl in self.labels[0]:
            x=0
            y=self.ofs[1]-dy/2.0+lbl[0]*self.grid[1]
            strs.append('    \\put(%4.1f,%4.1f){\\makebox(%d,%d)[r]{%s %s}}'%(x*scaleX,y*scaleY,dx,dy,sFontSize,lbl[1]))
        for lbl in self.labels[1]:
            x=dx/2.0+lbl[0]*self.grid[0]
            y=0
            strs.append('    \\put(%4.1f,%4.1f){\\makebox(%d,%d)[tc]{%s %s}}'%(x*scaleX,y*scaleY,dx,dy,sFontSize,lbl[1]))
        
        strs.append('    \\thicklines')
        i=0
        for dict in self.lst:
            rng=dict['range']
            oY=rng[0]*self.grid[1]
            dY=(rng[1]-rng[0])*self.grid[1]
            yMin=self.ofs[1]+oY
            yMax=self.ofs[1]+oY+dY
            yMid=self.ofs[1]+oY+dY*0.5
            xUndef=self.grid[0]*0.15
            dx,dy=int(self.ofs[0]*scaleX),int((self.ofs[1]-5)*scaleY)
            x=0
            y=self.ofs[1]-dy/2.0+(oY+dY/2.0)
            c=self.DFT_COLOR[i]
            strs.append('    \\put(%4.1f,%4.1f){\\color[rgb]{%3.2f,%3.2f,%3.2f}\\makebox(%d,%d)[r]{%s %s}}'%(x*scaleX,y*scaleY,float(c[0]/255.0) ,float(c[1]/255.0) ,float(c[2]/255.0),dx,dy,sFontSize,dict['name']))
            
            def addTexValLine(l):
                tup=vtRepLatex.lineFitPict2(l[0]*scaleX,l[1]*scaleY,l[2]*scaleX,l[3]*scaleY)
                c=self.DFT_COLOR[l[4]]
                tup=(tup[0],tup[1],float(c[0]/255.0) ,float(c[1]/255.0) ,float(c[2]/255.0) ,tup[2],tup[3],tup[4])
                strs.append('    \\put(%4.1f,%4.1f){\\color[rgb]{%3.2f,%3.2f,%3.2f}\\line(%d,%d){%4.1f}}'%tup)

            vals=dict['values']
            
            xa,ya=-1,-1
            for x,y in vals:
                if x<0:
                    xa,ya=x,y
                    continue
                # +++++++++++++++++++++++
                if y<0:
                    x=self.ofs[0]+self.grid[0]*x
                    if xa>=0:
                        if ya<0:
                            l=[xa+xUndef,yMin,x-xUndef,yMin,i]
                            addTexValLine(l)
                            l=[x-xUndef,yMin,x,yMid,i]
                            addTexValLine(l)
                            l=[xa+xUndef,yMax,x-xUndef,yMax,i]
                            addTexValLine(l)
                            l=[x-xUndef,yMax,x,yMid,i]
                            addTexValLine(l)
                        else:
                            l=[x,ya,x,yMid,i]
                            addTexValLine(l)
                            
                        l=[x,yMid,x+xUndef,yMin,i]
                        addTexValLine(l)
                        l=[x,yMid,x+xUndef,yMax,i]
                        addTexValLine(l)
                else:
                    y=self.ofs[1]+oY+(y*dY)
                    x=self.ofs[0]+self.grid[0]*x
                    if xa>=0:
                        if ya<0:
                            l=[xa+xUndef,yMin,x-xUndef,yMin,i]
                            addTexValLine(l)
                            l=[x-xUndef,yMin,x,yMid,i]
                            addTexValLine(l)
                            l=[xa+xUndef,yMax,x-xUndef,yMax,i]
                            addTexValLine(l)
                            l=[x-xUndef,yMax,x,yMid,i]
                            addTexValLine(l)
                            l=[x,yMid,x,y,i]
                            addTexValLine(l)
                        else:
                            l=[xa,ya,x,y,i]
                            addTexValLine(l)
                            
                # -----------------------
                #y=self.ofs[1]+oY+(y*dY)
                #x=self.ofs[0]+self.grid[0]*x
                #if xa>=0:
                #    tup=vtRepLatex.lineFitPict2(xa*scaleX,ya*scaleY,x*scaleX,y*scaleY)
                #    c=self.DFT_COLOR[i]
                #    tup=(tup[0],tup[1],float(c[0]/255.0) ,float(c[1]/255.0) ,float(c[2]/255.0) ,tup[2],tup[3],tup[4])
                #    strs.append('    \\put(%4.1f,%4.1f){\\color[rgb]{%3.2f,%3.2f,%3.2f}\\line(%d,%d){%4.1f}}'%tup)
                xa,ya=x,y
            i+=1
            i=i%(len(self.DFT_COLOR))
        strs.append('  \\end{picture}')
        return string.join(strs,os.linesep)
    def GenTexFigFloat(self):
        if self.node is not None:
            par=self.doc.getParent(self.node)
            sHier=vtXmlHierarchy.getTagNamesWithRel(self.doc,self.baseNode,self.relNode,par)+ ' ' + \
                        self.doc.getNodeText(par,'name') + ' '+_(u'diagram')
            sHierFull=vtXmlHierarchy.getTagNames(self.doc,self.baseNode,None,par)+ ' ' + \
                        self.doc.getNodeText(par,'name') + ' '+_(u'diagram')
        sCaption='  \\caption{%s}'%vtRepLatex.texReplace(sHier)
        sRef='  \\label{fig:%s}'%vtRepLatex.texReplace4Lbl(sHierFull)
        fig=['\\begin{figure}[!hb]',self.GenTexPic(),sCaption,sRef,'\\end{figure}']
        return string.join(fig,os.linesep)
    def GenTexFig(self):
        sCaption=''
        sRef=''
        if self.node is not None:
            par=self.doc.getParent(self.node)
            sHier=vtXmlHierarchy.getTagNamesWithRel(self.doc,self.baseNode,self.relNode,par)+ ' ' + \
                        self.doc.getNodeText(par,'name') + ' '+_(u'diagram')
            sHierFull=vtXmlHierarchy.getTagNames(self.doc,self.baseNode,None,par)+ ' ' + \
                        self.doc.getNodeText(par,'name') + ' '+_(u'diagram')
            sCaption='  \\caption{%s}'%vtRepLatex.texReplace(sHier)
            sRef='  \\label{fig:%s}'%vtRepLatex.texReplace4Lbl(sHierFull)
        fig=['\\begin{center}','\\begin{longtable}{c}',self.GenTexPic(),'  \\\\',sCaption,sRef,'\\end{longtable}','\\end{center}']
        return string.join(fig,os.linesep)
    def GenTexFigRef(self,node,baseNode):
        if node is not None:
            par=self.doc.getParent(node)
            sHier=vtXmlHierarchy.getTagNames(self.doc,baseNode,None,par)+ ' ' + \
                        self.doc.getNodeText(par,'name') + ' '+_(u'diagram')
            sHier=vtRepLatex.texReplace4Lbl(sHier)
            sRef='\\ref{fig:%s} \#\\pageref{fig:%s}'%(sHier,sHier)
            return sRef
        return ''
    def SetBaseNode(self,node):
        self.baseNode=node
    def SetRelNode(self,node):
        self.relNode=node
        
