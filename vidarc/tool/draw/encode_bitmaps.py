#!/usr/bin/env python
#----------------------------------------------------------------------

#----------------------------------------------------------------------------
# Name:         enocde_bitmaps.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: encode_bitmaps.py,v 1.2 2006/02/22 11:24:58 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    #"   -u -i -n Application Tools01_16.png images.py",
#    "   -u -i -n Mondrian mondrian.ico images.py",
    "-u -i -n Cancel img/abort.png images.py",
    "-a -u -n Ok img/ok.png images.py",
    "-a -u -n Del img/waste.png images.py",
    "-a -u -n Add img/add.png images.py",
    "-a -u -n Edit img/Edit01_16.png images.py",
    "-a -u -n Draw img/Draw01_16.png images.py",
    "-a -u -n Note img/Note04_16.png images.py",
    "-a -u -n Browse img/BrowseFile01_16.png images.py",
    "-a -u -n Link img/Link03_16.png images.py",
    "-a -u -n SigToggle img/SignalTransitionToggle01_16.png images.py",
    "-a -u -n SigUndef img/SignalTransitionUndef01_16.png images.py",
    "-a -u -n SigLowHigh img/SignalTransitionLowHigh01_16.png images.py",
    "-a -u -n SigHighLow img/SignalTransitionHighLow01_16.png images.py",
    "-a -u -n SigMoveUp img/SignalMoveUp01_16.png images.py",
    "-a -u -n SigMoveDown img/SignalMoveDown01_16.png images.py",
    "-a -u -n Trunc img/Trunc01_16.png images.py",
    "-a -u -n GrpDown img/Close01_16.png images.py",
    "-a -u -n GrpUp img/Open01_16.png images.py",
    
    "-a -u -n Elem img/DrawXml01_16.png images.py",
    "-a -u -n ElemSel img/DrawXml01_16.png images.py",
    "-a -u -n Attribute img/Attr01_16.png images.py",
    "-a -u -n AttributeSel img/AttrSel01_16.png images.py",
    "-a -u -n DrawGrp img/DrawGrp01_16.png images.py",
    "-a -u -n DrawGrpXml img/DrawGrpXml01_16.png images.py",
    "-a -u -n DrawLine img/DrawLine01_16.png images.py",
    "-a -u -n DrawRect img/DrawRect01_16.png images.py",
    "-a -u -n DrawMultiText img/DrawMultiText01_16.png images.py",
    "-a -u -n DrawText img/DrawText01_16.png images.py",
    
    "-a -u -n Left      img/Left07.png images.py",
    "-a -u -n Right     img/Right07.png images.py",
    "-a -u -n Up        img/Up07.png images.py",
    "-a -u -n Down      img/Down07.png images.py",
    
    
    "-a -u -n Application img/vidarcAppIconDiagram_16.ico images.py",

    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)

