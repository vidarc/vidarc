#Boa:Frame:vCanvasFrame
#----------------------------------------------------------------------------
# Name:         vCanvasXmlFrame.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vCanvasXmlFrame.py,v 1.1 2005/12/11 23:04:48 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons

import string,os,time,sys
import thread,threading,traceback

from vidarc.tool.draw.vtDrawCanvasXmlPanel import *
from vidarc.tool.draw.vtDrawCanvasObjectBase import *
from vidarc.tool.draw.vtDrawCanvasLine import *
from vidarc.tool.draw.vtDrawCanvasRectangle import *
from vidarc.tool.draw.vtDrawCanvasText import *
from vidarc.tool.draw.vtDrawCanvasTextMultiLine import *
from vidarc.tool.draw.vtDrawCanvasGroup import *
from vidarc.tool.draw.vtDrawCanvasXmlGroup import *
from vidarc.tool.draw.vtDrawCanvasBlock import *
from vidarc.tool.draw.vtDrawCanvasBlockIO import *
from vidarc.tool.draw.vtDrawCanvasXmlTreeList import *

from vidarc.tool.xml.vtXmlTreeListGrpAttr import *
from vidarc.tool.xml.vtXmlDom import *

import images

def xmlCMcreate(node,canvas):
    layer=0
    doc=canvas.GetDocExt()
    nodeExt=canvas.GetNodeExt()
    sName=vtXmlHierarchy.getTagNamesWithRel(doc,nodeExt,None,node)
    id=doc.getKey(node)
    o=vtDrawCanvasXmlGroup(id=long(id),name=sName,x=0,y=0,w=10,h=10,layer=layer)
    r=vtDrawCanvasRectangle(id=-1,name='__rect000',x=0,y=0,w=1,h=1,layer=layer)
    o.AddXml(r)
    r=vtDrawCanvasRectangle(id=-1,name='__rect001',x=1,y=1,w=1,h=1,layer=layer)
    o.AddXml(r)
    iX,iY,iW,iH=o.calcExtend(canvas)
    o.SetWidth(iW)
    o.SetHeight(iH)
    return o

def xmlModCreate(node,canvas):
    layer=0
    doc=canvas.GetDocExt()
    nodeExt=canvas.GetNodeExt()
    sName=vtXmlHierarchy.getTagNamesWithRel(doc,nodeExt,None,node)
    id=doc.getKey(node)
    o=vtDrawCanvasBlock(id=long(id),name=sName,x=0,y=0,w=8,h=10,layer=layer)
    return o
def xmlModAddAttr(obj,node,attr,canvas):
    layer=0
    doc=canvas.GetDocExt()
    sAttrName=doc.getTagName(attr)
    sAttrVal=doc.getAttributeVal(attr)
    obj.AddAttr(sAttrName,sAttrVal,vtDrawCanvasBlockIO.TYPE_IN,canvas)
    pass
def xmlModDelAttr(o,node,attr,canvas):
    doc=canvas.GetDocExt()
    sAttrName=self.docExt.getTagName(attr)
    pass

def create(parent):
    return vCanvasFrame(parent)

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon
[wxID_VCANVASFRAME, wxID_VCANVASFRAMECBCLOSEXML, wxID_VCANVASFRAMECBOPEN, 
 wxID_VCANVASFRAMECBOPENXML, wxID_VCANVASFRAMECBSAVE, wxID_VCANVASFRAMECBTEX, 
 wxID_VCANVASFRAMEPNMAIN, wxID_VCANVASFRAMESBSTATUS, 
] = [wx.NewId() for _init_ctrls in range(8)]

[wxID_VDIAGRAMFRAMEMNFILEITEM_EXIT, wxID_VDIAGRAMFRAMEMNFILEITEM_OPEN, 
] = [wx.NewId() for _init_coll_mnFile_Items in range(2)]


STATUS_PROCESS_POS=0
STATUS_CLOCK_POS=1

class vCanvasFrame(wx.Frame):
    def _init_coll_mnFile_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_VDIAGRAMFRAMEMNFILEITEM_OPEN,
              kind=wx.ITEM_NORMAL, text=_(u'Open'))
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VDIAGRAMFRAMEMNFILEITEM_EXIT,
              kind=wx.ITEM_NORMAL, text=_(u'Exit'))
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_openMenu,
              id=wxID_VDIAGRAMFRAMEMNFILEITEM_OPEN)
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_exitMenu,
              id=wxID_VDIAGRAMFRAMEMNFILEITEM_EXIT)

    def _init_coll_mnBar_Menus(self, parent):
        # generated method, don't edit

        parent.Append(menu=self.mnFile, title=_(u'File'))

    def _init_coll_sbStatus_Fields(self, parent):
        # generated method, don't edit
        parent.SetFieldsCount(2)

        parent.SetStatusText(number=0, text=u'Process')
        parent.SetStatusText(number=1, text=u'Clock')

        parent.SetStatusWidths([-1, 155])

    def _init_utils(self):
        # generated method, don't edit
        self.mnBar = wx.MenuBar()

        self.mnFile = wx.Menu(title=u'')

        self._init_coll_mnBar_Menus(self.mnBar)
        self._init_coll_mnFile_Items(self.mnFile)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VCANVASFRAME, name=u'vCanvasXmlFrame',
              parent=prnt, pos=wx.Point(112, 47), size=wx.Size(579, 474),
              style=wx.DEFAULT_FRAME_STYLE, title=u'vCanvasXmlFrame')
        self._init_utils()
        self.SetClientSize(wx.Size(571, 447))
        self.SetMenuBar(self.mnBar)

        self.sbStatus = wx.StatusBar(id=wxID_VCANVASFRAMESBSTATUS,
              name=u'sbStatus', parent=self, style=0)
        self.sbStatus.SetFieldsCount(2)
        self._init_coll_sbStatus_Fields(self.sbStatus)
        self.SetStatusBar(self.sbStatus)

        self.pnMain = wx.Panel(id=wxID_VCANVASFRAMEPNMAIN, name=u'pnMain',
              parent=self, pos=wx.Point(0, 0), size=wx.Size(571, 408),
              style=wx.TAB_TRAVERSAL)
        self.pnMain.SetAutoLayout(True)

        self.cbSave = wx.Button(id=wxID_VCANVASFRAMECBSAVE, label=u'Save',
              name=u'cbSave', parent=self.pnMain, pos=wx.Point(488, 312),
              size=wx.Size(75, 23), style=0)
        self.cbSave.Bind(wx.EVT_BUTTON, self.OnCbSaveButton,
              id=wxID_VCANVASFRAMECBSAVE)

        self.cbOpen = wx.Button(id=wxID_VCANVASFRAMECBOPEN, label=u'Open',
              name=u'cbOpen', parent=self.pnMain, pos=wx.Point(488, 280),
              size=wx.Size(75, 23), style=0)
        self.cbOpen.Bind(wx.EVT_BUTTON, self.OnCbOpenButton,
              id=wxID_VCANVASFRAMECBOPEN)

        self.cbOpenXml = wx.Button(id=wxID_VCANVASFRAMECBOPENXML,
              label=u'Open XML', name=u'cbOpenXml', parent=self.pnMain,
              pos=wx.Point(488, 344), size=wx.Size(75, 23), style=0)
        self.cbOpenXml.Bind(wx.EVT_BUTTON, self.OnCbOpenXmlButton,
              id=wxID_VCANVASFRAMECBOPENXML)

        self.cbCloseXml = wx.Button(id=wxID_VCANVASFRAMECBCLOSEXML,
              label=u'Close XML', name=u'cbCloseXml', parent=self.pnMain,
              pos=wx.Point(488, 376), size=wx.Size(75, 23), style=0)
        self.cbCloseXml.Bind(wx.EVT_BUTTON, self.OnCbCloseXmlButton,
              id=wxID_VCANVASFRAMECBCLOSEXML)

        self.cbTex = wx.Button(id=wxID_VCANVASFRAMECBTEX, label=u'Latex',
              name=u'cbTex', parent=self.pnMain, pos=wx.Point(488, 248),
              size=wx.Size(75, 23), style=0)
        self.cbTex.Bind(wx.EVT_BUTTON, self.OnCbTexButton,
              id=wxID_VCANVASFRAMECBTEX)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.bModified=False
        self.Clear()
        self.docExt=vtXmlDom()
        
        tup=[[(0,'0'),(1,'1')],[(0.0,'x'),(1.5,'y')]]
        self.dgCanvas=vtDrawCanvasXmlPanel(self.pnMain,pos=(4,4),size=(500,220))
        self.dgCanvas.SetConstraints(LayoutAnchors(self.dgCanvas, True,
              True, True, True))
        obj=vtDrawCanvasObjectBase()
        self.dgCanvas.Add(vtDrawCanvasLine(name='line001',x=1,y=1,w=5,h=5))
        self.dgCanvas.Add(vtDrawCanvasRectangle(name='rect001',x=5,y=5,w=2,h=2,layer=4))
        self.dgCanvas.Add(vtDrawCanvasText(name='text001',text='test',x=5,y=5,w=-1,h=-1,layer=4))
        self.dgCanvas.Add(vtDrawCanvasRectangle(name='rect002',x=2,y=5,w=2,h=2,layer=2))
        self.dgCanvas.Add(vtDrawCanvasText(name='text002',text='test2',x=2,y=5,w=2,h=2,layer=2))
        self.dgCanvas.Add(vtDrawCanvasRectangle(name='rect003',x=2,y=0,w=2,h=2,layer=3))
        self.dgCanvas.Add(vtDrawCanvasText(name='text003',text='test3',x=2,y=0,w=2,h=2,align_horizontal=vtDrawCanvasText.ALIGN_RIGHT,layer=3))
        self.dgCanvas.Add(vtDrawCanvasRectangle(name='rect004',x=2,y=2,w=2,h=2,layer=5))
        self.dgCanvas.Add(vtDrawCanvasText(name='text004',text='test4',x=2,y=2,w=2,h=2,align_vertical=vtDrawCanvasText.ALIGN_BOTTOM,layer=5))
        self.dgCanvas.Add(vtDrawCanvasTextMultiLine(name='text005',text='line50\nline51',x=0,y=0,w=2,h=2,align_vertical=vtDrawCanvasText.ALIGN_BOTTOM,layer=5))
        self.dgCanvas.Add(vtDrawCanvasTextMultiLine(name='text006',text='line60\nline61',x=0,y=2,w=2,h=2,align_vertical=vtDrawCanvasText.ALIGN_TOP,layer=5))
        self.dgCanvas.Add(vtDrawCanvasTextMultiLine(name='text007',text='line70\nline71',x=0,y=4,w=2,h=2,align_vertical=vtDrawCanvasText.ALIGN_CENTER,layer=5))
        self.dgCanvas.Add(vtDrawCanvasTextMultiLine(name='text008',text='line80\nline81',x=0,y=6,w=2,h=2,align_horizontal=vtDrawCanvasText.ALIGN_LEFT,layer=5))
        self.dgCanvas.Add(vtDrawCanvasTextMultiLine(name='text009',text='line90\nline91',x=0,y=8,w=2,h=2,align_horizontal=vtDrawCanvasText.ALIGN_RIGHT,layer=5))
        self.dgCanvas.Add(vtDrawCanvasTextMultiLine(name='text010',text='lineA0\nlineA1',x=2,y=8,w=2,h=2,align_horizontal=vtDrawCanvasText.ALIGN_CENTER,layer=5))
        
        grp=vtDrawCanvasGroup(name='group001',x=5,y=2,w=3,h=3,layer=5)
        grp.Add(vtDrawCanvasLine(name='line011',x=2,y=2,w=4,h=0))
        grp.Add(vtDrawCanvasLine(name='line012',x=0,y=0,w=1,h=0))
        grp.Add(vtDrawCanvasLine(name='line013',x=0,y=1,w=1,h=0))
        grp.Add(vtDrawCanvasLine(name='line014',x=2,y=0,w=0,h=1))
        grp.Add(vtDrawCanvasRectangle(name='rect011',x=2,y=0,w=1,h=1,layer=3))
        grp.Add(vtDrawCanvasText(name='text011',text='test11',x=2,y=2,w=2,h=2,layer=2))
        self.dgCanvas.Add(grp)
        
        grp=vtDrawCanvasGroup(name='group002',x=9,y=1,w=5,h=5,layer=5)
        grp.Add(vtDrawCanvasLine(name='line021',x=0,y=1,w=1,h=0))
        grp.Add(vtDrawCanvasLine(name='line022',x=0,y=0,w=1,h=0))
        grp2=vtDrawCanvasGroup(name='group021',x=2,y=2,w=1,h=1,layer=6)
        grp2.Add(vtDrawCanvasLine(name='line021',x=0,y=1,w=1,h=0))
        grp2.Add(vtDrawCanvasLine(name='line022',x=0,y=0,w=1,h=0))
        grp.Add(grp2)
        self.dgCanvas.Add(grp)
        
        self.dgCanvas.SetConfig(obj,obj.DRV_WX,max=(20,20),
                    grid=(20,20))
        self.dgCanvas.RegisterObject(vtDrawCanvasLine())
        self.dgCanvas.RegisterObject(vtDrawCanvasRectangle())
        self.dgCanvas.RegisterObject(vtDrawCanvasText())
        self.dgCanvas.RegisterObject(vtDrawCanvasTextMultiLine())
        self.dgCanvas.RegisterObject(vtDrawCanvasGroup())
        self.dgCanvas.RegisterObject(vtDrawCanvasXmlGroup())
        self.dgCanvas.RegisterObject(vtDrawCanvasBlock())
        self.dgCanvas.RegisterObject(vtDrawCanvasBlockIO())
        
        self.dgCanvas.RegisterXmlActions('CM',xmlCMcreate)
        self.dgCanvas.RegisterXmlActions('Mod',xmlModCreate,xmlModAddAttr,xmlModDelAttr)
        
        self.trlstExternal=vtXmlTreeListGrpAttr(parent=self.pnMain,
                id=wx.NewId(), name=u'trlstExternal', 
                pos=wx.Point(4, 226), size=wx.Size(200, 176), style=wx.TR_HAS_BUTTONS,
                cols=[_(u'tag'),_(u'name'),_(u'type'),_(u'added')],
                master=False,verbose=1)
        EVT_VGTRXMLTREE_ITEM_SELECTED(self.trlstExternal,self.OnTreeItemSel)
        self.trlstExternal.SetColumnWidth(0, 150)
        self.trlstExternal.SetColumnWidth(1, 80)
        self.trlstExternal.SetColumnWidth(2, 40)
        self.trlstExternal.SetColumnWidth(3, 20)
        #EVT_THREAD_ADD_ELEMENTS_FINISHED(self.trlstExternal,self.OnAddFinished)
        
        self.trlstInternal=vtDrawCanvasXmlTreeList(parent=self.pnMain,
                id=wx.NewId(), name=u'trlstInternal', 
                pos=wx.Point(224, 226), size=wx.Size(240, 176), style=wx.TR_HAS_BUTTONS)
        self.trlstInternal.SetObjects(self.dgCanvas.objs)
        #self.dgTiming.SetValues(vals)
        self.dgCanvas.SetDocExt(self.docExt)
        self.trlstExternal.SetDoc(self.docExt)
        
        # setup statusbar
        self.timer = wx.PyTimer(self.Notify)
        self.timer.Start(1000)
        self.Notify()
        
        icon = getApplicationIcon()
        self.SetIcon(icon)
        
        rect = self.sbStatus.GetFieldRect(STATUS_PROCESS_POS)
        self.gProcess = wx.Gauge(
                    self.sbStatus, -1, 50, (rect.x+2, rect.y+2), 
                    (rect.width-4, rect.height-4), 
                    wx.GA_HORIZONTAL|wx.GA_SMOOTH
                    )
        self.gProcess.SetRange(100)
        self.gProcess.SetValue(0)
    
    def OnTreeItemSel(self,evt):
        #print evt.GetTreeNodeData()
        evt.Skip()
    def Notify(self):
        try:
            t = time.localtime(time.time())
            st = time.strftime(_("%b-%d-%Y   %H:%M:%S wk:%W"), t)
            self.SetStatusText(st, STATUS_CLOCK_POS)
        except:
            pass
    def __makeTitle__(self):
        s=_("VIDARC Diagram")
        if self.fn is None:
            s=s+_(" (undef*)")
        else:
            s=s+" ("+self.fn
            if self.bModified:
                s=s+"*)"
            else:
                s=s+")"
        self.SetTitle(s)
    def __setModified__(self,state):
        self.bModified=state
        self.__makeTitle__()
    
    def Clear(self):
        pass
    def OpenFile(self,fn=None):
        pass
    def OnMnFileItem_openMenu(self, event):
        if self.thdOpen.IsRunning():
            return 
        dlg = wx.FileDialog(self, "Open", ".", "", "log files (*.log)|*.log|all files (*.*)|*.*", wx.OPEN)
        try:
            fn=self.GetFN()
            if fn is not None:
                dlg.SetPath(fn)
            #else:
            #    dlg.SetPath('.')
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.OpenFile(filename)
                self.__setModified__(False)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()

    def OnMnFileItem_exitMenu(self, event):
        self.Close()
        event.Skip()        

    def OnCbOpenButton(self, event):
        dlg = wx.FileDialog(self, _("Open"), ".", "", _("XML files (*.xml)|*.xml|all files (*.*)|*.*"), wx.OPEN)
        try:
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                dom=vtXmlDom()
                dom.Open(filename)
                node=dom.getChild(dom.getRoot(),'canvas')
                self.dgCanvas.SetDoc(dom)
                self.dgCanvas.SetNode(node)
                self.trlstInternal.SetObjects(self.dgCanvas.objs)
                dom.Close()
                del dom
        finally:
            dlg.Destroy()
        event.Skip()
        
    def OnCbSaveButton(self, event):
        dlg = wx.FileDialog(self, _("Save File As"), ".", "", _("XML files (*.xml)|*.xml|all files (*.*)|*.*"), wx.SAVE)
        try:
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                #self.txtEditor.SaveFile(filename)
                dom=vtXmlDom()
                dom.New(root='canvas_base')
                node=dom.createSubNode(dom.getRoot(),'canvas')
                self.dgCanvas.SetDoc(dom)
                self.dgCanvas.GetNode(node)
                dom.AlignDoc()
                dom.Save(filename)
                dom.Close()
                del dom
        finally:
            dlg.Destroy()
        event.Skip()

    def OnCbOpenXmlButton(self, event):
        dlg = wx.FileDialog(self, _("Open External"), ".", "", _("XML files (*.xml)|*.xml|all files (*.*)|*.*"), wx.OPEN)
        try:
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.docExt.Open(filename)
                node=self.docExt.getChild(self.docExt.getRoot(),'prjengs')
                node=self.docExt.getChild(node,'instance')
                self.dgCanvas.SetNodeExt(node)
                self.trlstExternal.SetNode(node)
                #self.docExt.Close()
                #del dom
        finally:
            dlg.Destroy()
        event.Skip()

    def OnCbCloseXmlButton(self, event):
        self.docExt.Close()
        #del self.docExt
        event.Skip()

    def OnCbTexButton(self, event):
        f=open(os.getenv('PythonProjBaseDir')+'/boa/tool/report/TmplLatex_Med_EN.tex','r')
        strs=f.readlines()
        f.close()
        f=open(os.getenv('PythonProjBaseDir')+'/boa/tool/draw/vCanvasXml_Test01.tex','w')
        f.writelines(strs)
        s=self.dgCanvas.GenTexFigFloat()
        f.write(s)
        f.write('\\end{document}')
        f.close()
        event.Skip()

