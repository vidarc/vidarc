#----------------------------------------------------------------------------
# Name:         vtDrawCanvasXmlGroup.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtDrawCanvasXmlGroup.py,v 1.4 2007/07/28 14:43:29 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vtDrawCanvasGroup import *
from vtDrawCanvasText import *
import vidarc.tool.log.vtLog as vtLog

class vtDrawCanvasXmlGroup(vtDrawCanvasGroup):
    def __init__(self,id=-1,name='',x=0,y=0,w=0,h=0,layer=0):
        vtDrawCanvasGroup.__init__(self,id,name,x,y,w,h,layer)
        self.objsXml=[]
    def SetActive(self,flag):
        vtDrawCanvasGroup.SetActive(self,flag)
        for o in self.objsXml:
            o.SetActive(flag)
    def AddXml(self,obj):
        self.objsXml.append(obj)
    def AddAttr(self,sAttrName,sAttrVal,canvas):
        o=vtDrawCanvasText(id=-1,name=sAttrName,text=sAttrVal,x=0,y=0,w=1,h=1,layer=self.GetLayer(),
                align_horizontal=vtDrawCanvasText.ALIGN_LEFT,align_vertical=vtDrawCanvasText.ALIGN_TOP)
        self.calcPosition(o,canvas)
        self.objsXml.append(o)
        #o=vtDrawCanvasText(id=-1,name='attr_%s_val'%sAttrName,text=sAttrVal,x=o.GetX(),y=o.GetY()+1,w=3,h=1,layer=o.GetLayer(),
        #        align_horizontal=vtDrawCanvasText.ALIGN_LEFT,align_vertical=vtDrawCanvasText.ALIGN_TOP)
        #self.objsXml.append(o)
        iX,iY,iW,iH=self.calcExtend(canvas)
        self.SetWidth(iW)
        self.SetHeight(iH)
    def DelAttr(self,sAttrName,canvas):
        lst=[]
        sAttrName=sAttrName
        iLen=len(sAttrName)
        for o in self.objsXml:
            if o.GetName()!=sAttrName:
                lst.append(o)
        self.objsXml=lst
    def GetObjectsXml(self):
        return self.objsXml
    def GetTypeName(self):
        # replace me
        return 'groupXml'
    
    def SetNode(self,doc,node,canvas):
        vtDrawCanvasGroup.SetNode(self,doc,node,canvas)
        n=doc.getChild(node,'groupedXml')
        canvas.__setNodeObjs__(self.objsXml,n)
        
    def GetNode(self,doc,parNode,canvas):
        node=vtDrawCanvasGroup.GetNode(self,doc,parNode,canvas)
        n=doc.getChildForced(node,'groupedXml')
        canvas.__getNodeObjs__(self.objsXml,n)
        return node
    def __draw__(self,canvas,aX=0,aY=0,aW=-1,aH=-1,iDriver=-1):
        #vtLog.CallStack(str(self))
        if self.bVisible==False:
            return
        vtDrawCanvasObjectBase.__drawSelect__(self,canvas,aX=aX,aY=aY,aW=aW,aH=aH,iDriver=iDriver)
        iW,iH=self.iWidth,self.iHeight
        iX,iY=aX+self.iX,aY+self.iY
        if aW>=0 and aH>=0:
            xe=aX+aW
            if iX>xe:
                return
            ye=aY+aH
            if iY>ye:
                return
            if iX+iW>xe:
                iW=xe-iX
            if iY+iH>ye:
                iH=ye-iY
        for o in self.objs:
            o.__draw__(canvas,iX,iY,iW,iH,iDriver)
        for o in self.objsXml:
            o.__draw__(canvas,iX,iY,iW,iH,iDriver)
        pass
    def calcExtend(self,canvas):
        try:
            sOrigin=canvas.GetName()
        except:
            sOrigin=''
        
        #vtLog.CallStack('')
        #print 'calcextend',self
        x,y,iW,iH=vtDrawCanvasGroup.calcExtend(self,canvas)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(None,vtLog.DEBUG,
                'calcExtend,group extend,x=%d,y=%d,w=%d,h=%d'%(x,y,iW,iH),
                origin=sOrigin)
        for o in self.objsXml:
            #print '  ',o
            xa,ya,xe,ye=o.calcExtend(canvas)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCallStack(None,vtLog.DEBUG,
                    'calcExtend,obj,x=%d,y=%d,w=%d,h=%d'%(xa,ya,xe,ye),
                    origin=sOrigin)
            #print '  ',o
            if xe>iW:
                iW=xe
            if ye>iH:
                iH=ye
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(None,vtLog.DEBUG,
                'calcExtend,group extend result,x=%d,y=%d,w=%d,h=%d'%(0,0,iW,iH),
                origin=sOrigin)
        
        return 0,0,iW,iH
    def calcPosition(self,obj,canvas):
        vtLog.CallStack('')
        print 'calc pos',self
        max=canvas.GetMax()
        bInterSect=False
        oiXa,oiYa,oiXe,oiYe=obj.__getExtend__(canvas)
        for dy in range(self.GetHeight()-obj.GetHeight()):
            for dx in range(self.GetWidth()-obj.GetWidth()):
                iXa,iYa,iXe,iYe=oiXa+dx,oiYa+dy,oiXe+dx,oiYe+dy
                bInterSect=False
                for o in self.objsXml:
                    #print o
                    if iXa<0:
                        iXe-=iXa
                        iXa=0
                    if iYa<0:
                        iYe-=iYa
                        iYa=0
                    #print '  ',o
                    if o.IsIntersectTupStrict(canvas,iXa,iYa,iXe,iYe):
                        #print ' intersect',o
                        bInterSect=True
                        break
                    #print '  ',o
                if bInterSect==False:
                    for o in self.objs:
                        #print o
                        if iXa<0:
                            iXe-=iXa
                            iXa=0
                        if iYa<0:
                            iYe-=iYa
                            iYa=0
                        if o.IsIntersectTupStrict(canvas,iXa,iYa,iXe,iYe):
                            #print ' intersect',o
                            bInterSect=True
                            break
                        #print '  ',o
                if bInterSect==False:
                    #print obj
                    #print 'set',iXa,iYa
                    obj.SetX(iXa)
                    obj.SetY(iYa)
                    #print obj
                    return True
        if bInterSect:
            if self.GetWidth()==self.GetHeight():
                obj.SetX(self.GetWidth())
                obj.SetY(0)
                self.SetWidth(obj.GetX()+obj.GetWidth())
            else:
                obj.SetX(0)
                obj.SetY(self.GetHeight())
                self.SetHeight(obj.GetY()+obj.GetHeight())
        #print self
        return True
    def __str__(self):
        return self.GetTypeName()+' name:'+self.name+' id:'+str(self.id)+' (%d,%d),(%d,%d) %d'%(self.iX,self.iY,self.iWidth,self.iHeight,self.bVisible)
