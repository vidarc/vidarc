#----------------------------------------------------------------------------
# Name:         vtDrawCanvasGroup.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtDrawCanvasGroup.py,v 1.3 2006/02/14 19:35:29 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import sys
from vtDrawCanvasObjectBase import *

class vtDrawCanvasGroup(vtDrawCanvasObjectBase):
    def __init__(self,id=-1,name='',x=0,y=0,w=0,h=0,layer=0):
        vtDrawCanvasObjectBase.__init__(self,id,name,x,y,w,h,layer)
        self.objs=[]
    def SetActive(self,flag):
        vtDrawCanvasObjectBase.SetActive(self,flag)
        for o in self.objs:
            o.SetActive(flag)
    def Add(self,obj):
        self.objs.append(obj)
    def GetObjects(self):
        return self.objs
    def GetTypeName(self):
        # replace me
        return 'group'
    
    def SetNode(self,doc,node,canvas):
        vtDrawCanvasObjectBase.SetNode(self,doc,node,canvas)
        n=doc.getChild(node,'grouped')
        canvas.__setNodeObjs__(self.objs,n)
        
    def GetNode(self,doc,parNode,canvas):
        node=vtDrawCanvasObjectBase.GetNode(self,doc,parNode,canvas)
        n=doc.getChildForced(node,'grouped')
        canvas.__getNodeObjs__(self.objs,n)
        return node
    def __draw__(self,canvas,aX=0,aY=0,aW=-1,aH=-1,iDriver=-1):
        if self.bVisible==False:
            return
        iW,iH=self.iWidth,self.iHeight
        iX,iY=aX+self.iX,aY+self.iY
        if aW>=0 and aH>=0:
            xe=aX+aW
            if iX>xe:
                return
            ye=aY+aH
            if iY>ye:
                return
            if iX+iW>xe:
                iW=xe-iX
            if iY+iH>ye:
                iH=ye-iY
        for o in self.objs:
            o.__draw__(canvas,iX,iY,iW,iH,iDriver)
        #print aX,aY,aW,aH
        #print iX,iY,iW,iH
        #self.__drawSelect__(canvas,aX,aY,aW,aH,iDriver)
        pass
    def calcExtend(self,canvas):
        iX,iY,iW,iH=0,0,-1,-1
        for o in self.objs:
            xa,ya,w,h=o.calcExtend(canvas)
            xe=xa+w
            if xe>iW:
                iW=xe
            ye=ya+h
            if ye>iH:
                iH=ye
        return 0,0,iW,iH
    def calcPosition(self,obj,canvas):
        max=canvas.GetMax()
        oiXa,oiYa,oiXe,oiYe=self.__getCanvas__(canvas)
        for dy in range(self.GetHeight()):
            for dx in range(self.GetWidth()):
                iXa,iYa,iXe,iYe=oiXa+dx,oiYa+dy,oiXe+dx,oiYe+dy
                bInterSect=False
                for o in self.objs:
                    if o.IsIntersectTup(canvas,iXa,iYa,iXe,iYe):
                        bInterSect=True
                        break
                if bInterSect==False:
                    obj.SetX(iXa)
                    obj.SetY(iYa)
                    return True
        if self.GetWidth()==self.GetHeight():
            obj.SetX(self.GetWidth())
        else:
            obj.SetY(self.GetHeight())
        return True
    