#----------------------------------------------------------------------------
# Name:         vtDrawCanvasPercentage.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060221
# CVS-ID:       $Id: vtDrawCanvasPercentage.py,v 1.1 2006/02/22 11:24:58 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.draw.vtDrawCanvasRectangleFilled import *
class vtDrawCanvasPercentage(vtDrawCanvasRectangleFilled):
    def __init__(self,id=-1,name='',percentage=0,x=0,y=0,w=0,h=0,layer=0):
        vtDrawCanvasRectangleFilled.__init__(self,id,name,x,y,w,h,layer)
        self.fPercentage=percentage
    def GetPercentage(self):
        return self.fPercentage
    def SetPercentage(self,fVal):
        try:
            self.fPercentage=float(fVal)
        except:
            self.fPercentage=0.0
    def GetTypeName(self):
        # replace me
        return 'percentage'
    def GetAttr(self,idx):
        if idx==6:
            return 'percentage',self.GetPercentage(),'%3.2f',1
        else:
            return vtDrawCanvasRectangleFilled.GetAttr(self,idx)
    def SetAttr(self,idx,val):
        if idx==6:
            self.SetPercentage(val)
            return 0
        else:
            return vtDrawCanvasRectangleFilled.SetAttr(self,idx)
        
    def __draw__(self,canvas,aX=0,aY=0,aW=-1,aH=-1,iDriver=-1):
        if self.bVisible==False:
            return
        drvInfo=canvas.getDriverData(iDriver)
        drv=canvas.getDriver(iDriver)
        canvas.setActLayer(self.iLayer,self.bActive,iDriver)
        if drv==self.DRV_WX:
            dc=drvInfo['dc']
            if aW>=0 and aH>=0:
                ax,ay=canvas.calcCoor(aX,aY)
                aw,ah=canvas.calcCoor(aW,aH)
                dc.SetClippingRegion(ax,ay,aw+1,ah+1)
            x,y=canvas.calcCoor(self.iX+aX,self.iY+aY)
            w,h=canvas.calcCoor(self.iWidth,self.iHeight)
            w=int(w*((self.fPercentage)/100.0))
            dc.DrawRectangle(x,y,w,h)
            if aW>=0 and aH>=0:
                dc.DestroyClippingRegion()
        elif drv==self.DRV_LATEX:
            if aW>=0 and aH>=0:
                ax,ay=canvas.calcCoorLatex(aX,aY)
                aw,ah=canvas.calcCoorLatex(aW,aH)
            x,y=canvas.calcCoorLatex(self.iX+aX,self.iY+aY)
            w,h=canvas.calcCoorLatex(self.iWidth,self.iHeight)
            w=int(w*(self.fPercentage)/100.0)
            
            y=drvInfo['y_max']-y-h
            c=drvInfo['color']
            strs=drvInfo['strs']
            tup=vtRepLatex.lineFitPict2(x,y,x+w,y)
            strs.append('    \\put(%4.1f,%4.1f){%s\\line(%d,%d){%4.1f}}'%(tup[0],tup[1],c,tup[2],tup[3],tup[4]))
            tup=vtRepLatex.lineFitPict2(x+w,y,x+w,y+h)
            strs.append('    \\put(%4.1f,%4.1f){%s\\line(%d,%d){%4.1f}}'%(tup[0],tup[1],c,tup[2],tup[3],tup[4]))
            tup=vtRepLatex.lineFitPict2(x,y+h,x+w,y+h)
            strs.append('    \\put(%4.1f,%4.1f){%s\\line(%d,%d){%4.1f}}'%(tup[0],tup[1],c,tup[2],tup[3],tup[4]))
            tup=vtRepLatex.lineFitPict2(x,y,x,y+h)
            strs.append('    \\put(%4.1f,%4.1f){%s\\line(%d,%d){%4.1f}}'%(tup[0],tup[1],c,tup[2],tup[3],tup[4]))
            pass
        vtDrawCanvasObjectBase.__drawSelect__(self,canvas,aX=aX,aY=aY,aW=aW,aH=aH,iDriver=iDriver)

