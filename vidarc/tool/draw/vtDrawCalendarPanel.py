#----------------------------------------------------------------------------
# Name:         vtDrawCalendarPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060221
# CVS-ID:       $Id: vtDrawCalendarPanel.py,v 1.15 2008/06/08 16:36:31 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import cStringIO

from vidarc.tool.draw.vtDrawCanvasPanel import *
from vidarc.tool.draw.vtDrawCanvasObjectBase import *
from vidarc.tool.draw.vtDrawCanvasGroup import *
from vidarc.tool.draw.vtDrawCanvasLine import *
from vidarc.tool.draw.vtDrawCanvasRectangle import *
from vidarc.tool.draw.vtDrawCanvasRectangleFilled import *
from vidarc.tool.draw.vtDrawCanvasText import *
from vidarc.tool.draw.vtDrawCanvasTextMultiLine import *
from vidarc.tool.draw.vtDrawCanvasTextSelectable import *
from vidarc.tool.draw.vtDrawCanvasPercentage import *
from vidarc.tool.draw.vtDrawCanvasPercentageLabel import *
from vidarc.tool.draw.vtDrawCanvasHourStartEnd import *
from vidarc.tool.draw.vtDrawCanvasHourStartEndLabel import *

import vidarc.tool.art.vtColor as vtColor
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
import calendar,types,copy
import vidarc.tool.time.vtTime as vtTime

[wxID_VTDRAWCANVASPANEL, wxID_VTDRAWCANVASPANELSCWDRAWING, 
 wxID_VTDRAWCANVASPANELCHCMONTH, wxID_VTDRAWCANVASPANELSPYEAR, 
 wxID_VTDRAWCANVASPANELSPWEEK, wxID_VTDRAWCANVASPANELSPMONCOUNT,
] = [wx.NewId() for _init_ctrls in range(6)]


#----------------------------------------------------------------------
def getLeftData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x0c\x00\x00\x00\x0c\x08\x06\
\x00\x00\x00Vu\\\xe7\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\x00\
\x00aIDAT(\x91\x8d\xd0\xc1\r\x80@\x08\x04\xc0E\xaf\xff\xbf\xda\x82J-\xf4\xc0\
\xf7j\xd0\x17\x913\x88\xec{gI \x9afTs\x1e\xfb\xd5\xaaE\x00\x10\x11\xa4\xc0\
\x8a>!\x88\x8a"2\x82\xa8\x14\x85\x98\xf9\xb7h\xeb\x00\xd0z\xef\x95\xe1\xe7\
\x82\xbdu[\x97\xf4\x92\xaa\x8e\xc0\'\xc2)\xf8\x82\xaa\x9a\x837,\x03\x0fo\xde\
\xad5;\xa6j\xabw\x00\x00\x00\x00IEND\xaeB`\x82' 

def getLeftBitmap():
    return wx.BitmapFromImage(getLeftImage())

def getLeftImage():
    stream = cStringIO.StringIO(getLeftData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getRightData():
    return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x0c\x00\x00\x00\x0c\x08\x06\
\x00\x00\x00Vu\\\xe7\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\x00\
\x00kIDAT(\x91\x95\xd2\xcb\r\x80 \x10E\xd1\x8b\xda\xaa\xda\x06j\x1b\xf40\xb5\
\xd0\x03[j\xc0\x95D\xf9\xfb\x12\x16$\xef0\xb3\x00cLP\xd3\xcc\xf0\x11\x91\xe0\
\xbd\x07`\xddvE'\x1f\xf0\xa4\x05\x95\x88\x04\x80\x14\xd5`\x13\x94p\x04#\x08`\
y_\x9cs\xe3\xc0Z\xdb,\x1e\xe7\xa5\xb2\t\xb5R6!}\xbdT\xccV\xea\x15c\xb4\xd6\
\xbf\xbe\xc6\rN\xf25\x89\xc7\xed\x86d\x00\x00\x00\x00IEND\xaeB`\x82" 

def getRightBitmap():
    return wx.BitmapFromImage(getRightImage())

def getRightImage():
    stream = cStringIO.StringIO(getRightData())
    return wx.ImageFromStream(stream)


class vtDrawCalendarPanel(vtDrawCanvasPanel):
    VERBOSE=0
    DFT_COLOR=[
        vtColor.MONDAY,
        vtColor.TUESDAY,
        vtColor.WEDNESDAY,
        vtColor.THURSDAY,
        vtColor.FRIDAY,
        vtColor.SATURDAY,
        vtColor.SUNDAY,
        
        vtColor.SEAGREEN,           # week layer = 7
        vtColor.SADDLE_BROWN,
        vtColor.WHITE,
        vtColor.BLACK,
        
        vtColor.INDIANRED,          # result layer = 11
        vtColor.ORANGE,
        vtColor.SEAGREEN,
        
        vtColor.RED2,               # result layer = 14
        ]
    DFT_DIS_COLOR=[
        vtColor.Scale(vtColor.MONDAY,0.8),
        vtColor.Scale(vtColor.TUESDAY,0.8),
        vtColor.Scale(vtColor.WEDNESDAY,0.8),
        vtColor.Scale(vtColor.THURSDAY,0.8),
        vtColor.Scale(vtColor.FRIDAY,0.8),
        vtColor.Scale(vtColor.SATURDAY,0.8),
        vtColor.Scale(vtColor.SUNDAY,0.8),
        vtColor.Scale(vtColor.SEAGREEN,0.8),            # week layer = 7
        vtColor.Scale(vtColor.SADDLE_BROWN,0.8),
        vtColor.Scale(vtColor.WHITE,0.8),
        vtColor.Scale(vtColor.BLACK,0.8),
        
        vtColor.Scale(vtColor.INDIANRED,0.8),           # vacation layer = 11
        vtColor.Scale(vtColor.ORANGE,0.8),
        vtColor.Scale(vtColor.SEAGREEN,0.8),
        
        vtColor.Scale(vtColor.RED2,0.8),                # overload
        ]
    FOCUS_LAYER=7
    RESULT_LAYER=12
    OVERLOAD_LAYER=14
    DFT_DATA_LAYER=10
    
    ACT_COLOR_FACTOR=0.85
    DIS_COLOR_FACTOR=0.7
    DFT_COLOR_FACTOR=0.85
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTDRAWCANVASPANEL, name='', parent=prnt,
              pos=wx.Point(0, 0), size=wx.Size(100, 100),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(100, 100))
        self.SetAutoLayout(True)
        fgs = wx.FlexGridSizer(cols=1, hgap=0, rows=2, vgap=0)
        fgs.AddGrowableRow(1)
        fgs.AddGrowableCol(0)
        #fgs.AddGrowableCol(1)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        self.spYear = wx.SpinCtrl(id=wxID_VTDRAWCANVASPANELSPYEAR, initial=2000, max=2100,
              min=0, name=u'spYear', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(117, 21), style=wx.SP_ARROW_KEYS)
        self.spYear.SetMinSize((-1,-1))
        #self.spYear.Bind(wx.EVT_SPINCTRL, self.OnUpdateDate,self.spYear)
        #self.spYear.Bind(wx.EVT_TEXT, self.OnUpdateDate,self.spYear)
        self.spYear.Bind(wx.EVT_SPIN, self.OnUpdateDate,self.spYear)
        self.spYear.Bind(wx.EVT_TEXT_ENTER, self.OnUpdateDate,self.spYear)
        
        id=wx.NewId()
        self.cbDec = wx.lib.buttons.GenBitmapButton(ID=id,
              bitmap=vtArt.getBitmap(vtArt.Left), name=u'cbDec',
              parent=self, style=wx.BU_AUTODRAW)
        #self.cbDec.SetBitmapSelected(getLeftBitmap())
        self.cbDec.Bind(wx.EVT_BUTTON,self.OnDecButton,self.cbDec)
        
        id=wx.NewId()
        self.cbInc = wx.lib.buttons.GenBitmapButton(ID=id,
              bitmap=vtArt.getBitmap(vtArt.Right), name=u'cbInc',
              parent=self, style=wx.BU_AUTODRAW)
        #self.cbInc.SetBitmapSelected(getRightBitmap())
        self.cbInc.Bind(wx.EVT_BUTTON,self.OnIncButton,self.cbInc)
        
        self.chcMonth = wx.Choice(choices=[], id=wxID_VTDRAWCANVASPANELCHCMONTH,
              name=u'chcMonth', parent=self, pos=wx.Point(117, 0),
              size=wx.Size(130, 21), style=0)
        self.chcMonth.Bind(wx.EVT_CHOICE, self.OnUpdateDateChc,self.chcMonth)
        self.chcMonth.SetMinSize((-1,-1))
        
        self.lblMonCount=wx.StaticText(id=-1,
              label=_('month count'), name='lblMonCount', parent=self,
              style=wx.ALIGN_RIGHT)
        self.spMonCount = wx.SpinCtrl(id=wxID_VTDRAWCANVASPANELSPMONCOUNT, initial=1,
              max=48, min=1, name=u'spMonCount', parent=self, pos=wx.Point(16,
              48), size=wx.Size(117, 21), style=wx.SP_ARROW_KEYS)
        #self.spMonCount.Bind(wx.EVT_SPINCTRL, self.OnSpMonCountSpinctrl,
        #      id=wxID_VTDRAWCANVASPANELSPMONCOUNT)
        self.spMonCount.Bind(wx.EVT_SPIN, self.OnSpMonCountSpinctrl,
              id=wxID_VTDRAWCANVASPANELSPMONCOUNT)
        self.spMonCount.Bind(wx.EVT_TEXT_ENTER, self.OnSpMonCountTextEnter,
              id=wxID_VTDRAWCANVASPANELSPMONCOUNT)
        self.spMonCount.SetMinSize((-1,-1))
        self.spMonCount.SetValue(1)
        #self.spWk = wx.SpinCtrl(id=wxID_VTDRAWCANVASPANELSPWEEK, initial=1, max=53, min=1,
        #      name=u'spWk', parent=self, pos=wx.Point(247, 0), size=wx.Size(117,
        #      21), style=wx.SP_ARROW_KEYS)
        self.lblUsage=wx.StaticText(id=-1,
              label=_('usage'), name='lblUsage', parent=self,
              style=wx.ALIGN_RIGHT)
        self.txtUsage=wx.TextCtrl(id=-1, name='txtUsage',parent=self, 
              style=wx.TE_READONLY, value='%4.1f - %4.1f'%(0.0,0.0))
        #self.txtUsage.Enable(False)
        self.txtUsage.SetMinSize((-1,-1))
        self.lblMin=wx.StaticText(id=-1,
              label=_('minutes'), name='lblMin', parent=self,
              style=wx.ALIGN_RIGHT)
        self.lblMin.SetMinSize((-1,-1))
        self.txtMin=wx.TextCtrl(id=-1, name='txtMin',parent=self, 
              style=wx.TE_READONLY, value='%02d / %02d'%(0,0))
        #self.txtMin.Enable(False)
        self.txtMin.SetMinSize((-1,-1))
        
        idTmp=wx.NewId()
        self.cbSave = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Save),
              id=idTmp, label=_(u'Save'),
              name=u'cbSave', parent=self, pos=wx.DefaultPosition,
              size=wx.Size(50, 30), style=0)
        self.cbSave.Bind(wx.EVT_BUTTON, self.OnCbSaveButton,id=idTmp)
        
        idTmp=wx.NewId()
        self.cbGoTo = wx.lib.buttons.GenBitmapButton(ID=idTmp,
              bitmap=vtArt.getBitmap(vtArt.Navigate), name=u'cbGoTo',
              parent=self, pos=wx.Point(281, 0), size=wx.Size(31, 30), style=0)
        self.cbGoTo.Bind(wx.EVT_BUTTON, self.OnCbGoToButton,id=idTmp)
        
        bxs.AddWindow(self.spYear, 3, border=0, flag=wx.EXPAND)
        bxs.AddWindow(self.cbDec, 0, border=0, flag=wx.ALIGN_CENTRE)
        bxs.AddWindow(self.chcMonth, 4, border=0, flag=wx.EXPAND)
        bxs.AddWindow(self.cbInc, 0, border=4, flag=wx.ALIGN_CENTRE|wx.RIGHT)
        bxs.AddWindow(self.lblMonCount, 2, border=4, flag=wx.LEFT|wx.RIGHT|wx.EXPAND)
        bxs.AddWindow(self.spMonCount, 2, border=4, flag=wx.LEFT|wx.EXPAND)
        #bxs.AddWindow(self.spWk, 1, border=0, flag=wx.EXPAND)
        bxs.AddWindow(self.lblUsage, 2, border=4, flag=wx.LEFT|wx.RIGHT)
        bxs.AddWindow(self.txtUsage, 3, border=0, flag=wx.EXPAND)
        bxs.AddWindow(self.lblMin, 2, border=4, flag=wx.LEFT|wx.RIGHT)
        bxs.AddWindow(self.txtMin, 2, border=0, flag=wx.EXPAND)
        bxs.AddWindow(self.cbSave, 3, border=4, flag=wx.LEFT|wx.EXPAND|wx.RIGHT)
        bxs.AddWindow(self.cbGoTo, 0, border=4, flag=wx.LEFT)
        
        self.scwDrawing = wx.ScrolledWindow(id=wxID_VTDRAWCANVASPANELSCWDRAWING,
              name=u'scwDrawing', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(100, 100), style=wx.SUNKEN_BORDER | wx.HSCROLL | wx.VSCROLL)
        #self.scwDrawing.SetConstraints(LayoutAnchors(self.scwDrawing, True,
        #      True, True, True))
        
        fgs.AddSizer(bxs, 1, border=0, flag=wx.EXPAND|wx.LEFT|wx.RIGHT)
        fgs.AddWindow(self.scwDrawing, 1, border=4, flag=wx.EXPAND|wx.TOP)
        self.SetSizer(fgs)
        EVT_VTDRAW_CANVAS_ITEM_ACTIVATED(self,self.OnActivatedElement)
        EVT_VTDRAW_CANVAS_ITEM_DEACTIVATED(self,self.OnDeActivatedElement)
        EVT_VTDRAW_CANVAS_ITEM_SELECTED(self,self.OnSelectedElement)
        EVT_VTDRAW_CANVAS_ITEM_UNSELECTED(self,self.OnUnSelectedElement)
    def __init__(self, parent, 
                id=wx.NewId(), pos=wx.DefaultPosition, size=wx.Size(100, 50), 
                style=wx.TAB_TRAVERSAL, name=u'vtdCalendar',
                draw_grid=True,is_gui_wx=True,
                flip=False,label_len=2,
                grid_x=20,grid_y=20,month_count=1):
        global _
        _=vtLgBase.assignPluginLang('vtDraw')
        self.bFlip=flip
        self.iLblOfs=label_len
        if self.bFlip:
            max=(20,self.iLblOfs+(31*month_count))
        else:
            max=(self.iLblOfs+(31*month_count),20)
        vtDrawCanvasPanel.__init__(self, parent, 
                id=wx.NewId(), pos=pos, size=size, 
                style=style, name=name,
                draw_grid=draw_grid,
                is_gui_wx=is_gui_wx,
                max_x=max[0],max_y=max[1],
                grid_x=grid_x,grid_y=grid_y)
        
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #self.bFlip=True
        
        self.dVal=None
        self.funcVal=None
        self.args=()
        self.kwargs={}
        self.iHrLimit=8.0#24.0
        self.spMonCount.SetValue(month_count)
        self.__updateLang__()
        self.dt=vtTime.vtDateTime(True)
        self.dt.SetDay(1)
        self.__initDayLblBlks__()
        self.__showDate__()
        
        obj=vtDrawCanvasObjectBase()
        self.SetConfig(obj,obj.DRV_WX,max=max,
                    grid=(grid_x,grid_y))
        
    def __initDayLblBlks__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.lblBlks=[]
        self.lblBlksMon=[]
        self.lblBlksWk=[]
        self.lblBlksHr=[]
        self.lnBorder=[]
        self.iBorder=0
        self.dCacheSel={}
        
        iYear=self.dt.GetYear()
        iMon=self.dt.GetMonth()
        iYearStart,iMonStart=iYear,iMon
        iMonCount=self.spMonCount.GetValue()
        if self.bFlip:
            iMonX,iMonY=1,self.iLblOfs+0
            iWkX,iWkY=2,self.iLblOfs+0
            iW,iH=2,1
            iX,iY=1+iWkX,self.iLblOfs+0
        else:
            iMonX,iMonY=self.iLblOfs+0,1
            iWkX,iWkY=self.iLblOfs+0,2
            iW,iH=1,2
            iX,iY=self.iLblOfs+0,1+iWkY
        for iMonRep in xrange(iMonCount):
            mrng=calendar.monthrange(iYear,iMon)
            self.dt.SetDate(iYear,iMon,1)
            iWk=self.dt.GetCalendarISO()[1]
            iLv=mrng[0]
            bWk=True
            blk=vtDrawCanvasText(id=-1,name='mon_%04d%02d'%(iYear,iMon),
                            text='%04d-%02d-01'%(iYear,iMon),
                            x=iMonX,y=iMonY,w=10,h=1,
                            align_horizontal=3,
                            layer=self.DFT_DATA_LAYER)
            self.lblBlksMon.append(blk)
            for i in range(1,32):
                blk=vtDrawCanvasText(id=-1,name='wk_%02d'%i,
                            text='%02d'%(iWk),
                            x=iWkX,y=iWkY,w=1,h=1,layer=7)
                if bWk and i<=mrng[1]:
                    blk.SetVisible(True)
                else:
                    blk.SetVisible(False)
                self.lblBlksWk.append(blk)
                bWk=False
                
                sTag=calendar.day_abbr[iLv]
                blk=vtDrawCanvasTextMultiLine(id=-1,name='day_%02d'%i,
                            text='%s\n%02d'%(sTag,i),
                            x=iX,y=iY,w=iW,h=iH,layer=iLv)
                if i>mrng[1]:
                    blk.SetVisible(False)
                else:
                    blk.SetVisible(True)
                if self.bFlip:
                    iY+=1
                    iWkY+=1
                else:
                    iX+=1
                    iWkX+=1
                self.lblBlks.append(blk)
                iLv+=1
                iLv%=7
                if iLv==0:
                    iWk+=1
                    bWk=True
                    if iMon==1 and iWk>52:
                        iWk=1
            iMon+=1
            if iMon>12:
                iYear+=1
                iMon=1
        if self.bFlip:
            iX,iY=0,self.iLblOfs+0
            iW,iH=1,1
        else:
            iX,iY=self.iLblOfs+0,0
            iW,iH=1,1
        for i in range(0,24):
            blk=vtDrawCanvasTextSelectable(id=i,name='hr_%02d'%i,
                    text='%02d'%i,x=iX,y=iY,w=iW,h=iH,layer=8)
            blk=vtDrawCanvasPercentage(id=i,name='hr_%02d'%i,
                        percentage=(i/24.0)*100,
                        x=iX,y=iY,w=iW,h=iH,layer=8)
            blk=vtDrawCanvasHourStartEndLabel(id=i,name='hr_%02d'%i,
                        text='%02d'%i,
                        start=0,end=0,
                        #start=0,end=(i/24.0)*60,
                        x=iX,y=iY,w=iW,h=iH,layer=2)
            blk.SetVisible(False)
            #blk=vtDrawCanvasHourStartEnd(id=i,name='hr_%02d'%i,
            #        start=15,end=(i/24.0)*60,
            #        x=iX,y=iY,w=iW,h=iH,layer=2)
            self.lblBlksHr.append(blk)
            if self.bFlip:
                iY+=1
            else:
                iX+=1
        if self.bFlip:
            blk=vtDrawCanvasLine(id=-1,name='',y=self.iLblOfs,x=2,h=0,w=20,layer=8)
            self.lnBorder.append(blk)
            for i in range(7*iMonCount):
                blk=vtDrawCanvasLine(id=-1,name='',y=self.iLblOfs+31,x=2,h=0,w=20,layer=8)
                self.lnBorder.append(blk)
            blk=vtDrawCanvasLine(id=-1,name='',y=0,x=5,h=self.iLblOfs+31,w=0,layer=8)
            self.lnBorder.append(blk)
            blk=vtDrawCanvasLine(id=-1,name='',y=0,x=self.max[1],h=self.iLblOfs+31,w=0,layer=8)
            self.lnBorder.append(blk)
        else:
            blk=vtDrawCanvasLine(id=-1,name='',x=self.iLblOfs,y=2,w=0,h=20,layer=8)
            self.lnBorder.append(blk)
            for i in range(7*iMonCount):
                blk=vtDrawCanvasLine(id=-1,name='',x=self.iLblOfs+31,y=2,w=0,h=20,layer=8)
                self.lnBorder.append(blk)
            blk=vtDrawCanvasLine(id=-1,name='',x=0,y=5,w=self.iLblOfs+(31*iMonCount),h=0,layer=8)
            self.lnBorder.append(blk)
            blk=vtDrawCanvasLine(id=-1,name='',x=0,y=self.max[1],w=self.iLblOfs+(31*iMonCount),h=0,layer=8)
            self.lnBorder.append(blk)
        #self.objs=self.lblBlksHr
        #self.__updateDayLblBlks__()
        self.iBorder=len(self.lnBorder)
        self.dt.SetDate(iYearStart,iMonStart,1)
    def __clearInt__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.txtUsage.SetValue('')
        self.txtMin.SetValue('')
        self.dCacheSel={}
        self.idSel=None
        self.cbGoTo.Enable(False)
    def __updateDayLblBlks__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        iYear=self.dt.GetYear()
        iMon=self.dt.GetMonth()
        iYearStart,iMonStart=iYear,iMon
        iMonCount=self.spMonCount.GetValue()
        iOfs=0
        for blk in self.lblBlks:
            blk.SetVisible(False)
        for blk in self.lblBlksWk:
            blk.SetVisible(False)
        for iMonRep in xrange(iMonCount):
            mrng=calendar.monthrange(iYear,iMon)
            iDays=mrng[1]
            self.dt.SetDate(iYear,iMon,1)
            iLv=mrng[0]
            iWk=self.dt.GetCalendarISO()[1]
            
            blk=self.lblBlksMon[iMonRep]
            blk.SetAttr(6,'%04d-%02d-01'%(iYear,iMon))
            if self.bFlip:
                blk.SetY(self.iLblOfs+iOfs)
            else:
                blk.SetX(self.iLblOfs+iOfs)
            #i=1
            #for blk in self.lblBlks:
            bWk=True
            for i in range(0,iDays):
                blk=self.lblBlksWk[i+iOfs]
                blk.SetAttr(6,'%02d'%(iWk))
                if bWk and i<mrng[1]:
                    blk.SetVisible(True)
                else:
                    blk.SetVisible(False)
                bWk=False
                
                blk=self.lblBlks[i+iOfs]
                sTag=calendar.day_abbr[iLv]
                blk.SetAttr(6,'%s\n%02d'%(sTag,i+1))
                blk.SetLayer(iLv)
                if i>=mrng[1]:
                    blk.SetVisible(False)
                else:
                    blk.SetVisible(True)
                #i+=1
                iLv+=1
                iLv%=7
                if iLv==0:
                    iWk+=1
                    bWk=True
                    if iMon==1 and iWk>52:
                        iWk=1
            iOfs+=iDays
            iMon+=1
            if iMon>12:
                iYear+=1
                iMon=1
        for blk in self.lblBlksHr:
            blk.SetVisible(False)
        self.dt.SetDate(iYearStart,iMonStart,1)
        self.objs=[]#self.lblBlksHr
        #self.objs=copy.deepcopy(self.lblBlksHr)
        self.__showValues__()
    #def __drawGrid__(self,dc,printing):
    def __drawObjects__(self,dc,printing):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        # overload me
        #vtLog.vtLogCallDepth(None,'',self.VERBOSE=1)
        try:
            self.drvInfo[self.objBase.DRV_WX]['dc']=dc
            self.drvInfo[self.objBase.DRV_WX]['printing']=printing
        except:
            #traceback.print_exc()
            return
        if 1==0:
            if self.thdDraw.IsRunning():
                return
            self.drvInfo[self.objBase.DRV_WX]['act_layer']=-1
            self.thdDraw.Start(self)
        else:
            #self.semDraw.acquire()
            #vtLog.CallStack('')
            self.drvInfo[self.objBase.DRV_WX]['act_layer']=-1
            #for blk in self.lblBlksHr:
            #    blk.__draw__(self)
            for blk in self.lblBlksMon:
                blk.__draw__(self)
            for blk in self.lblBlksWk:
                blk.__draw__(self)
            for blk in self.lblBlks:
                blk.__draw__(self)
            for o in self.objs:
                o.__draw__(self)
            for blk in self.lnBorder:
                blk.__draw__(self)
            self.drvInfo[self.objBase.DRV_WX]['dc']=None
            #self.semDraw.release()
        #vtDrawCanvasPanel.__drawGrid__(self,dc,printing)
        #vtDrawCanvasPanel.__drawObjects__(self,dc,printing)
        #for blk in self.lblBlks:
        #    blk.__draw__(self)
    def SetFlip(self,flag):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if self.bFlip!=flag:
            self.bFlip=flag
    def __updateLang__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.chcMonth.Clear()
        for i in range(1,13):
            self.chcMonth.Append(calendar.month_name[i])
    def __showDate__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.spYear.SetValue(self.dt.GetYear())
        self.chcMonth.SetSelection(self.dt.GetMonth()-1)
        #mrng=calendar.monthrange(self.dt.GetYear(),self.dt.GetMonth())
        self.__updateDayLblBlks__()
    def OnDecButton(self,evt):
        try:
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            iYear=self.dt.GetYear()
            iMon=self.dt.GetMonth()
            iMon-=1
            if iMon<1:
                iYear-=1
                iMon=12
            self.dt.SetDate(iYear,iMon,1)
            self.__showDate__()
            self.ShowAll()
            self.scwDrawing.SetFocus()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnIncButton(self,evt):
        try:
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            iYear=self.dt.GetYear()
            iMon=self.dt.GetMonth()
            iMon+=1
            if iMon>12:
                iYear+=1
                iMon=1
            self.dt.SetDate(iYear,iMon,1)
            self.__showDate__()
            self.ShowAll()
            self.scwDrawing.SetFocus()
        except:
            vtLog.vtLngTB(self.GetName())
        #evt.Skip()
    def OnUpdateDate(self,evt):
        evt.Skip()
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        try:
            iYear=int(self.spYear.GetValue())
            self.dt.SetYear(iYear)
            self.__showDate__()
            self.ShowAll()
            self.scwDrawing.SetFocus()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnUpdateDateChc(self,evt):
        evt.Skip()
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        try:
            self.dt.SetMonth(self.chcMonth.GetSelection()+1)
            self.__showDate__()
            self.ShowAll()
            self.scwDrawing.SetFocus()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSpMonCountSpinctrl(self, event):
        event.Skip()
        try:
            if wx.Thread_IsMain()==False:
                vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
            self.__initDayLblBlks__()
            self.__showDate__()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSpMonCountTextEnter(self, event):
        event.Skip()
        try:
            if wx.Thread_IsMain()==False:
                vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
            self.__initDayLblBlks__()
            self.__showDate__()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbSaveButton(self,evt):
        evt.Skip()
        try:
            dlg = wx.FileDialog(self, _(u"Save File As"), ".", "", 
                        _(u"PNG files (*.png)|*.png|JPG files (*.jpg)|*.jpg|BMP files (*.bmp)|*.bmp|CSV files (*.csv)|*.csv"), wx.SAVE)
            if dlg.ShowModal() == wx.ID_OK:
                sFN = dlg.GetPath()
                sExt=sFN[-3:]
                if sExt.lower()=='csv':
                    cSep=';'
                    iYear=self.dt.GetYear()
                    iMonth=self.dt.GetMonth()
                    iMonCount=self.spMonCount.GetValue()
                    fOut=open(sFN,'w')
                    try:
                        dRes=self.__getCurDateValues__()
                        def doValues(fOut,tup,iYear,iMonth,iMonCount,cSep):
                            for iMonRepeat in xrange(iMonCount):
                                mrng=calendar.monthrange(iYear,iMonth)
                                #print self.dVal
                                iDayStart,iDayMax=mrng
                                if tup is None:
                                    for iDay in xrange(iDayMax):
                                        fOut.write('%04d-%02d-%02d'%(iYear,iMonth,1+iDay))
                                        fOut.write(cSep)
                                else:
                                    fLimit,dYear=tup[0],tup[1]
                                    dDay={}
                                    if iYear in dYear:
                                        dMon=dYear[iYear]
                                        if iMonth in dMon:
                                            fScale=1.0
                                            if len(tup)>2:
                                                try:
                                                    dInfo=tup[2]
                                                    if '__res' in dInfo:
                                                        iLayer=self.RESULT_LAYER+dInfo['__res']
                                                    fScale=dInfo['__per']/dInfo['__lim']
                                                except:
                                                    fScale=1.0
                                            dDay=dMon[iMonth]
                                    for iDay in xrange(iDayMax):
                                        try:
                                            if iDay in dDay:
                                                lHrs=dDay[iDay]
                                                if type(lHrs)==types.ListType:
                                                    iHr=0.0
                                                    for hr in lHrs:
                                                        iHrS,iMnS=hr[0]
                                                        iHrE,iMnE=hr[1]
                                                        #print hr
                                                        iHr+=float(iHrE)+iMnE/60.0
                                                        iHr-=float(iHrS)+iMnS/60.0
                                                        #print iHr
                                                    fPer=((iHr/fLimit)*fScale)*100.0
                                                else:
                                                    fPer=(lHrs/fLimit)*100.0
                                                fOut.write(str(fPer))
                                        except:
                                            vtLog.vtLngTB(self.GetName())
                                        fOut.write(cSep)
                                    
                                iMonth+=1
                                if iMonth>12:
                                    iYear+=1
                                    iMonth=1
                        def doDict(fOut,d,iYear,iMonth,iMonCount,lHier,cSep):
                                keys=[k for k in d.keys() if k.startswith('__')==False]
                                keys.sort()
                                for k in keys:
                                    tup=d[k]
                                    if type(tup)==types.ListType:
                                        fOut.write('.'.join(lHier+[k]))
                                        doValues(fOut,tup,iYear,iMonth,iMonCount,cSep)
                                        fOut.write('\n')
                                    else:
                                        fOut.write('.'.join(lHier+[k]))
                                        if '__sum' in tup:
                                            doValues(fOut,tup['__sum'],iYear,iMonth,iMonCount,cSep)
                                        fOut.write('\n')
                                        doDict(fOut,tup,iYear,iMonth,iMonCount,lHier+[k],cSep)
                        fOut.write(cSep)
                        doValues(fOut,None,iYear,iMonth,iMonCount,cSep)
                        fOut.write('\n')
                        doDict(fOut,dRes,iYear,iMonth,iMonCount,[],cSep)
                    except:
                        vtLog.vtLngTB(self.GetName())
                    fOut.close()
                else:
                    d={ 'png':wx.BITMAP_TYPE_PNG,
                        'bmp':wx.BITMAP_TYPE_BMP,
                        'jpg':wx.BITMAP_TYPE_JPEG,
                        }
                    if sExt in d:
                        t=d[sExt]
                    else:
                        t=wx.BITMAP_TYPE_PNG
                    #img=self.buffer.ConvertToImage()
                    #bmp=img.ConvertToBitmap()
                    #bmp.SaveFile(sFN,t)
                    self.buffer.SaveFile(sFN,t)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbGoToButton(self,evt):
        evt.Skip()
        try:
            self.doc.SelectById(str(self.idSel))
        except:
            vtLog.vtLngTB(self.GetName())
    def SetValue(self,d,*args,**kwargs):
        """ dVal={2006:{
                02:{
                    'prt':(8.0,{
                        10:[((8,0),(8,60)),((9,5),(9,60)),((10,10),(10,60)),
                            ((11,15),(11,60)),((12,20),(12,60)),((13,25),(13,60)),
                            ((14,30),(14,60)),((15,35),(15,60)),((16,40),(16,60)),
                            ((17,45),(17,60)),((18,50),(18,60)),((19,55),(19,60))],
                        13:[((8,15),(9,30)),((10,10),(14,40)),((15,00),(18,30))],
                        15:[((8,20),(8,45)),((10,00),(14,00)),((15,00),(15,60))],
                        }),
                    },
                },
            }
        """
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.dVal=None
        self.funcVal=None
        self.args=args
        self.kwargs=kwargs
        if type(d)==types.DictionaryType:
            self.dVal=d
        elif type(d)==types.FunctionType:
            self.funcVal=d
        elif type(d)==types.MethodType:
            self.funcVal=d
        self.__showDate__()
        self.ShowAll()
    def __getCurDateValues__(self):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        try:
            if wx.Thread_IsMain()==False:
                vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
            iYear=self.dt.GetYear()
            iMonth=self.dt.GetMonth()
            iMonCount=self.spMonCount.GetValue()
            if self.dVal is not None:
                return self.dVal[iYear][iMonth]
            elif self.funcVal is not None:
                return self.funcVal(iYear,iMonth,iMonCount,*self.args,**self.kwargs)
        except:
            vtLog.vtLngTB(self.GetName())
            return {}
    def __drawEntryBlock__(self,objs,iX,iY,iW,iH,sLbl,tup,iYear,iMon,iDayStart,iDayMax,iDayOfs,lHier):
        try:
            if iDayOfs==0:
                blk=vtDrawCanvasText(id=-1,name=sLbl,text=sLbl,
                                x=iX,y=iY,w=iW,h=iH,layer=10,
                                align_horizontal=vtDrawCanvasText.ALIGN_LEFT)
                objs.append(blk)
            
            #fLimit,dDay=tup[0],tup[1]
            fLimit,dYear=tup[0],tup[1]
            if iYear not in dYear:
                #print 'year not found',iYear,vtLog.pformat(dYear)
                return
            dMon=dYear[iYear]
            if iMon not in dMon:
                #print 'mon not found',iYear,iMon,vtLog.pformat(dMon)
                return
            iLayer=self.DFT_DATA_LAYER
            fScale=1.0
            fLimOvLd=None
            fLimOvLdPer=None
            if len(tup)>2:
                try:
                    dInfo=tup[2]
                    if '__res' in dInfo:
                        iLayer=self.RESULT_LAYER+dInfo['__res']
                    fScale=dInfo['__per']/dInfo['__lim']
                except:
                    fScale=1.0
                try:
                    if '__limOvLd' in dInfo:
                        fLimOvLd=dInfo['__limOvLd']
                        fLimOvLdPer=(fLimOvLd/fLimit)*100.0
                except:
                    pass
            dDay=dMon[iMon]
            keysDay=dDay.keys()
            keysDay.sort()
            for iDay in keysDay:
                #print 'day',iDay
                lHrs=dDay[iDay]
                if iDay>iDayMax:
                    vtLog.vtLngCurWX(vtLog.WARN,'key:%s;year:%d;month:%d;day:%d;hours:%s'%(sLbl,
                                iYear,iMon,iDay,lHrs),self)
                    break
                if iDay<1:
                    vtLog.vtLngCurWX(vtLog.WARN,'key:%s;year:%d;month:%d;day:%d;hours:%s'%(sLbl,
                                iYear,iMon,iDay,lHrs),self)
                    continue
                #print '   ',lHrs
                bFilled=True
                if type(lHrs)==types.ListType:
                    iHr=0.0
                    for hr in lHrs:
                        #if type(hr)==types.IntType:
                        if len(hr)>=2:
                            iLayer=self.RESULT_LAYER+hr[2]
                            #continue
                        bFilled=False
                        iHrS,iMnS=hr[0]
                        iHrE,iMnE=hr[1]
                        #print hr
                        iHr+=float(iHrE)+iMnE/60.0
                        iHr-=float(iHrS)+iMnS/60.0
                        #print iHr
                    fPer=((iHr/fLimit)*fScale)*100.0
                else:
                    fPer=(lHrs/fLimit)*100.0
                    if fLimOvLdPer is not None:
                        if fPer>=fLimOvLdPer:
                            iLayer=self.OVERLOAD_LAYER
                if self.bFlip:
                    iHrX,iHrY=iX,self.iLblOfs+iDay+iDayOfs-1
                else:
                    iHrX,iHrY=self.iLblOfs+iDay+iDayOfs-1,iY
                #print iHrX,iHrY
                blk=vtDrawCanvasPercentageLabel(id=-1,
                                name=sLbl+'_%04d%02d%02d'%(iYear,iMon,iDay),
                                percentage=fPer,
                                x=iHrX,y=iHrY,w=1,h=1,
                                layer=iLayer,bFilled=bFilled)#(iDayStart+iDay-1)%7)
                blk.SetUsrData(lHier+[iYear,iMon,iDay])
                if 0:
                    if iHrX in self.dCacheSel:
                        dY=self.dCacheSel[iHrX]
                    else:
                        dY={}
                        self.dCacheSel[iHrX]=dY
                    if iHrY in dY:
                        pass
                    else:
                        dY[iHrY]=blk
                objs.append(blk)
        except:
            vtLog.vtLngTB(self.GetName())
    def __addBorderLine(self,iX,iY,iLayer,iMonCount):
        if self.bFlip:
            blk=vtDrawCanvasLine(id=-1,name='',
                    x=self.iLblOfs+31,y=0,w=0,h=self.iLblOfs+(31*iMonCount),layer=iLayer)
        else:
            blk=vtDrawCanvasLine(id=-1,name='',
                    x=iX,y=iY,w=self.iLblOfs+(31*iMonCount),h=0,layer=iLayer)
            self.lnBorder.append(blk)
        
    def __showValues__(self):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.__clearInt__()
        self.lnBorder=self.lnBorder[:self.iBorder]
        #for blk in self.lnBorder:
        #    blk.SetVisible(False)
        iYear=self.dt.GetYear()
        iMonth=self.dt.GetMonth()
        iMonCount=self.spMonCount.GetValue()
        try:
            #dRes=self.dVal[iYear][iMonth]
            dRes=self.__getCurDateValues__()
            if dRes is None:
                return
            if self.VERBOSE:
                vtLog.CallStack('')
                print vtLog.pformat(dRes)
            iDayOfs=0
            for iMonRepeat in xrange(iMonCount):
                mrng=calendar.monthrange(iYear,iMonth)
                #print self.dVal
                iDayStart,iDayMax=mrng
                i=-1
                if self.bFlip:
                    iX,iY=4,0
                    iW,iH=1,self.iLblOfs-1
                else:
                    iX,iY=0,4
                    iW,iH=self.iLblOfs-1,1
                def doDict(d,i,iX,iY,iMax,iYear,iMon,iDayStart,iDayMax,iDayOfs,bFirst,lHier):
                    keys=[k for k in d.keys() if k.startswith('__')==False]
                    keys.sort()
                    iXStart=iX
                    iYStart=iY
                    bDict=False
                    for k in keys:
                        i+=1
                        if self.bFlip:
                            iX+=1
                            iYlbl=iY+1
                            iYval=iY+2
                            iXlbl=iX
                            iXval=iX
                            iHlbl=iH-1
                            iHval=iH-2
                            iWlbl=iW
                            iWval=iW
                        else:
                            iY+=1
                            iXlbl=iX+1
                            iXval=iX+2
                            iYlbl=iY
                            iYval=iY
                            iWlbl=iW-1
                            iWval=iW-2
                            iHlbl=iH
                            iHval=iH
                        tup=d[k]
                        if type(tup)==types.ListType:
                            self.__drawEntryBlock__(self.objs,iXval,iYval,iWval,iHval,k,tup,iYear,iMon,
                                                iDayStart,iDayMax,iDayOfs,lHier+[k])
                        else:
                            if '__sum' in tup:
                                self.__drawEntryBlock__(self.objs,iXlbl,iYlbl,iWlbl,iHlbl,k,tup['__sum'],
                                                iYear,iMon,iDayStart,iDayMax,iDayOfs,lHier+[k,'__sum'])
                            else:
                                blk=vtDrawCanvasText(id=-1,name=k,text=k,
                                    x=iX,y=iY,w=iW,h=iH,layer=10,
                                    align_horizontal=vtDrawCanvasText.ALIGN_LEFT)
                                    
                                self.objs.append(blk)
                            if bFirst:
                                self.__addBorderLine(iX,iY,8,iMonCount)
                            i,iX,iY,iMax=doDict(tup,i,iX,iY,iMax,iYear,iMon,
                                            iDayStart,iDayMax,iDayOfs,bFirst,lHier+[k])
                            #self.__addBorderLine(iX,iY,8)
                            bDict=True
                    iCount=len(keys)
                    if self.bFlip:
                        max=(iCount+5,self.iLblOfs+31)
                    else:
                        max=(self.iLblOfs+31,iCount+5)
                    if self.bFlip:
                        #blk.SetX(max[0])
                        #blk.SetWidth(self.max[0])
                        iResSep=int(round(iCount/5.0+0.5))
                    else:
                        #blk.SetY(max[1])
                        iResSep=int(round(iCount/5.0+0.5))
                    if bDict==False and 0:
                        for i in xrange(iCount):
                            if self.bFlip:
                                blk=vtDrawCanvasLine(id=-1,name='',x=self.max[1],y=0,w=0,h=self.iLblOfs+31,layer=8)
                            else:
                                blk=vtDrawCanvasLine(id=-1,name='',
                                    x=0,y=iYStart+i*5+1,w=self.iLblOfs+31,h=0,layer=8)
                            self.lnBorder.append(blk)
                        
                    if 0:
                        for i in range(len(self.lnBorder)-9,iResSep):
                            if self.bFlip:
                                blk=vtDrawCanvasLine(id=-1,name='',x=self.max[1],y=0,w=0,h=self.iLblOfs+31,layer=8)
                                self.lnBorder.append(blk)
                            else:
                                blk=vtDrawCanvasLine(id=-1,name='',x=0,y=self.max[1],w=self.iLblOfs+31,h=0,layer=8)
                                self.lnBorder.append(blk)
                    return i,iX,iY,iMax+len(keys)
                i,iX,iY,iMax=doDict(dRes,i,iX,iY,0,iYear,iMonth,iDayStart,iDayMax,iDayOfs,iMonRepeat==0,
                                    [])
                iMonth+=1
                if iMonth>12:
                    iMonth=1
                    iYear+=1
                iDayOfs+=iDayMax
            iLastWk=0
            iLast=0
            if self.bFlip:
                max=(iMax+5,self.iLblOfs+(31*iMonCount))
            else:
                max=(self.iLblOfs+(31*iMonCount),iMax+5)
            #vtLog.CallStack('')
            #print len(self.lnBorder)
            iDayOfs=0
            iWkOfs=0
            iYear=self.dt.GetYear()
            iMonth=self.dt.GetMonth()
            if '__holidays' in dRes:
                dHolidays=dRes['__holidays']
            else:
                dHolidays=None
            for iMonRepeat in xrange(iMonCount):
                mrng=calendar.monthrange(iYear,iMonth)
                #print self.dVal
                iDayStart,iDayMax=mrng
                dHol={}
                if dHolidays is not None:
                    if iYear in dHolidays:
                        if iMonth in dHolidays[iYear]:
                            dHol=dHolidays[iYear][iMonth]
                for iDay in range(0,31):
                    if (iDayStart+iDay)%7==0:
                        iWkTmp=(iDayStart+iDay)/7
                        iLastWk=iWkTmp
                        blk=self.lnBorder[iWkOfs+iWkTmp]
                        iLast=self.iLblOfs+iDay+iDayOfs
                        #print '%04d %02d %02d %02d %03d %02d %02d %02d %03d'%(iYear,iMonth,iDay,
                        #            iDayStart,iDayOfs,iWkOfs,iWkTmp,iLastWk,iLast)
                        if self.bFlip:
                            blk.SetY(iLast)
                            blk.SetWidth(max[0]-2)
                        else:
                            blk.SetX(iLast)
                            blk.SetHeight(max[1]-2)
                        blk.SetVisible(True)
                    if (iDay+1) in dHol:
                        blk=self.lblBlks[iDayOfs+iDay]
                        blk.SetLayer(6)
                for iWkTmp in range(iLastWk+1,7):
                    blk=self.lnBorder[iWkOfs+iWkTmp]
                    if self.bFlip:
                        blk.SetY(iLast)
                        blk.SetWidth(max[0]-2)
                    else:
                        blk.SetX(iLast)
                        blk.SetHeight(max[1]-2)
                    blk.SetVisible(False)
                for i in [0,7]:
                    blk=self.lnBorder[iWkOfs+i]
                    if self.bFlip:
                        blk.SetY(self.iLblOfs+iDayOfs)
                        blk.SetWidth(max[0]-2)
                        #blk.SetWidth(self.max[0])
                    else:
                        blk.SetX(self.iLblOfs+iDayOfs)
                        blk.SetHeight(max[1]-2)
                        #blk.SetHeight(self.max[1])
                    #blk.SetVisible(True)
                iMonth+=1
                iDayOfs+=iDayMax
                iWkOfs+=7#iLastWk#(iDayStart+iDayMax)/7
                if iMonth>12:
                    iMonth=1
                    iYear+=1
            
            blk=self.lnBorder[iWkOfs+2]
            if self.bFlip:
                    blk.SetX(max[0])
                    #blk.SetWidth(self.max[0])
                    iResSep=int(round((max[0]-5)/5.0+0.5))
            else:
                    blk.SetY(max[1])
                    iResSep=int(round((max[1]-5)/5.0+0.5))
            if 0:
                for i in range(len(self.lnBorder)-9,iResSep):
                    if self.bFlip:
                        blk=vtDrawCanvasLine(id=-1,name='',x=self.max[1],y=0,w=0,h=self.iLblOfs+31,layer=8)
                        self.lnBorder.append(blk)
                    else:
                        blk=vtDrawCanvasLine(id=-1,name='',x=0,y=self.max[1],w=self.iLblOfs+31,h=0,layer=8)
                        self.lnBorder.append(blk)
                iPos=5+5
                for i in range(10,len(self.lnBorder)):
                    blk=self.lnBorder[i]
                    if self.bFlip:
                        blk.SetX(iPos)
                    else:
                        blk.SetY(iPos)
                    iPos+=5
            #if self.bFlip:
            #    max=(len(keys)+5,self.iLblOfs+31)
            #else:
            #    max=(self.iLblOfs+31,len(keys)+5)
            if self.max!=max:
                self.SetMax(max,True)
        except:
            vtLog.vtLngTB(self.GetName())
            pass
    def OnActivatedElement(self,evt):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        #print 'act',evt.GetBlock()
        evt.Skip()
    def OnDeActivatedElement(self,evt):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        #print 'deact',evt.GetBlock()
        evt.Skip()
    def OnMoveEvent(self,evt):
        #evt.Skip()
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.SetXY(evt)
        xCoor,yCoor=self.calcPos(self.x,self.y,False)
        if 0:
            #vtDrawCanvasPanel.OnMoveEvent(self,evt)
            blk=None
            if xCoor in self.dCacheSel:
                dY=self.dCacheSel[xCoor]
                if yCoor in dY:
                    blk=dY[yCoor]
            if blk is not None:
                if self.focusBlk==blk:
                    pass
                else:
                    if self.focusBlk is not None:
                        self.focusBlk.SetFocus(False)
                        self.ShowObj(self.focusBlk,False)

                    self.focusBlk=blk
                    self.focusBlk.SetFocus(True)
                    self.ShowObj(self.focusBlk,False)
            else:
                if self.focusBlk is not None:
                    self.focusBlk.SetFocus(False)
                    self.ShowObj(self.focusBlk,False)
                    self.focusBlk=None
        
        for o in self.lblBlksHr:
            if o.GetVisible():
                iInterSect=o.IsInsideTup(self,xCoor,yCoor)
                if iInterSect:
                    #print 'found',o
                    self.txtMin.SetValue('%02d / %02d'%(o.GetStart(),o.GetEnd()))
                    return
            else:
                break
        self.txtMin.SetValue('')
        #self.__drawBorder__()
    def OnKillFocus(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtDrawCanvasPanel.OnKillFocus(self,evt)
        self.__drawBorder__()
        self.txtMin.SetValue('')
        #self.__clearInt__()
    def __setHourValues__(self,blk):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if blk is None:
            return
        sName=blk.GetName()
        fPercent=blk.GetPercentage()
        fLimit=0.0
        try:
            lHier=blk.GetUsrData()
            if lHier is None:
                return
            sRes,iDay=sName.split('_')
            iDay=int(iDay)
            
            dRes=self.__getCurDateValues__()
            
            d=dRes
            for s in lHier[:-3]:
                d=d[s]
            fLimit,dYear=d[0],d[1]
            fScale=1.0
            if len(d)>2:
                try:
                    dInfo=d[2]
                    fScale=dInfo['__per']/dInfo['__lim']
                except:
                    fScale=1.0
            
            iYear,iMonth,iDay=lHier[-3],lHier[-2],lHier[-1]
            lHrs=dYear[iYear][iMonth][iDay]
            #tup=dRes[sRes]
            #fLimit,dDay=tup[0],tup[1]
            #lHrs=dDay[iDay]
            #iYear=self.dt.GetYear()
            #iMonth=self.dt.GetMonth()
            iLv=(calendar.monthrange(iYear,iMonth)[0]+iDay-1)%7
            for iHr in range(0,24):
                blk=self.lblBlksHr[iHr]
                #blk=self.objs[iHr]
                blk.SetStart(0)
                blk.SetEnd(0)
                blk.SetLayer(iLv)
            iLayer=self.DFT_DATA_LAYER
            if type(lHrs)==types.ListType:
                pass
            elif type(lHrs)==types.FloatType:
                fVal=lHrs
                iHr=int(fVal)
                lHrs=[((0,0),(iHr,int((fVal-iHr)*60)))]
            for hr in lHrs:
                if len(hr)>2:
                    iLv=self.RESULT_LAYER+hr[2]
                iHrS,iMnS=hr[0]
                iHrE,iMnE=hr[1]
                if iMnE==0 and iHrS!=iHrE:
                    iHrE-=1
                    iMnE=60
                if iHrS>23 or iHrS<0:
                    vtLog.vtLngCurWX(vtLog.WARN,'key:%s;day:%d;start hour:%d,hours:%s'%(sRes,iDay,iHrS,lHrs),self)
                    continue
                if iHrE>23 or iHrE<0:
                    vtLog.vtLngCurWX(vtLog.WARN,'key:%s;day:%d;end hour:%d,hours:%s'%(sRes,iDay,iHrS,lHrs),self)
                    continue
                        
                if iHrS==iHrE:
                    blk=self.lblBlksHr[iHrS]
                    #blk=self.objs[iHrS]
                    blk.SetStart(iMnS)
                    blk.SetEnd(iMnS+max(int((iMnE-iMnS)*fScale),1))
                    blk.SetLayer(iLv)
                else:
                    blk=self.lblBlksHr[iHrS]
                    #blk=self.objs[iHrS]
                    blk.SetStart(iMnS)
                    blk.SetEnd(iMnS+max(int((60-iMnS)*fScale),1))
                    blk.SetLayer(iLv)
                    
                    for iHr in range(iHrS+1,iHrE):
                        blk=self.lblBlksHr[iHr]
                        #blk=self.objs[iHr]
                        blk.SetStart(0)
                        blk.SetEnd(max(int(60*fScale),1))
                        blk.SetLayer(iLv)
                    blk=self.lblBlksHr[iHrE]
                    #blk=self.objs[iHrE]
                    blk.SetStart(0)
                    blk.SetEnd(max(int(iMnE*fScale),1))
                    blk.SetLayer(iLv)
        except:
            vtLog.vtLngTB(self.GetName())
            pass
        fUsage=fPercent/100.0*fLimit
        self.txtUsage.SetValue('%4.1f - %4.1f'%(fUsage,fLimit))
        #self.txtMin.SetValue('%02d / %02d'%(0,0))
    def __drawBorder__(self):
        for blk in self.lnBorder:
            self.ShowObj(blk,True)
        
    def OnSelectedElement(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.BeginBatch()
        try:
            blkSel=evt.GetBlock()
            lHier=blkSel.GetUsrData()
            if lHier is None:
                self.idSel=None
            else:
                dRes=self.__getCurDateValues__()
                
                d=dRes
                for s in lHier[:-3]:
                    d=d[s]
                fLimit,dYear=d[0],d[1]
                iYear,iMonth,iDay=lHier[-3],lHier[-2],lHier[-1]
                lHrs=dYear[iYear][iMonth][iDay]
                self.idSel=None
                if type(lHrs)==types.ListType:
                    try:
                        self.idSel=lHrs[0][3]
                    except:
                        pass
                if len(d)>2:
                    try:
                        dInfo=d[2]
                        if 'id' in dInfo:
                            self.idSel=dInfo['id']
                    except:
                        pass
            if self.idSel is None:
                self.cbGoTo.Enable(False)
            else:
                self.cbGoTo.Enable(True)
            self.__setHourValues__(blkSel)
            
            for blk in self.lblBlksHr:
                blk.SetVisible(True)
                self.ShowObj(blk,True)
            #for i in range(len(self.lblBlksHr)):
            #    blk=self.objs[i]
            #    blk.SetVisible(True)
            #    self.ShowObj(blk,True)
            self.__drawBorder__()
        except:
            vtLog.vtLngTB(self.GetName())
        self.EndBatch()
        evt.Skip()
    def OnUnSelectedElement(self,evt):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.BeginBatch()
        try:
            self.__clearInt__()
            for blk in self.lblBlksHr:
                blk.SetVisible(False)
                self.ShowObj(blk,False)
            #for i in range(len(self.lblBlksHr)):
            #    blk=self.objs[i]
            #    blk.SetVisible(False)
            #    self.ShowObj(blk,False)
            self.__drawBorder__()
        except:
            vtLog.vtLngTB(self.GetName())
        self.EndBatch()
        evt.Skip()
        
