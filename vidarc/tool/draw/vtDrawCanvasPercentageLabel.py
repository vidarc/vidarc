#----------------------------------------------------------------------------
# Name:         vtDrawCanvasPercentage.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060221
# CVS-ID:       $Id: vtDrawCanvasPercentageLabel.py,v 1.3 2008/04/16 08:24:33 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.draw.vtDrawCanvasRectangleFilled import *
from vidarc.tool.draw.vtDrawCanvasText import *

class vtDrawCanvasPercentageLabel(vtDrawCanvasRectangleFilled,vtDrawCanvasText):
    def __init__(self,id=-1,name='',percentage=0,x=0,y=0,w=0,h=0,layer=0,bFilled=True):
        vtDrawCanvasText.__init__(self,id=id,name=name,text='%02d'%percentage,
                            x=x,y=y,w=w,h=h,layer=layer,
                            align_vertical=vtDrawCanvasText.ALIGN_TOP,
                            align_horizontal=vtDrawCanvasText.ALIGN_RIGHT)
        vtDrawCanvasRectangleFilled.__init__(self,id,name,x,y,w,h,layer)
        self.fPercentage=percentage
        self.bFilled=bFilled
    def GetPercentage(self):
        return self.fPercentage
    def SetPercentage(self,fVal):
        try:
            self.fPercentage=float(fVal)
        except:
            self.fPercentage=0.0
        vtDrawCanvasText.SetAttr(self,6,'%02d'%self.fPercentage)
    def GetTypeName(self):
        # replace me
        return 'percentageLabel'
    def GetAttr(self,idx):
        if idx==6:
            return 'percentage',self.GetPercentage(),'%3.2f',1
        else:
            return vtDrawCanvasRectangleFilled.GetAttr(self,idx)
    def SetAttr(self,idx,val):
        if idx==6:
            self.SetPercentage(val)
            return 0
        else:
            return vtDrawCanvasRectangleFilled.SetAttr(self,idx)
        
    def __draw__(self,canvas,aX=0,aY=0,aW=-1,aH=-1,iDriver=-1):
        if self.bVisible==False:
            return
        drvInfo=canvas.getDriverData(iDriver)
        drv=canvas.getDriver(iDriver)
        canvas.setActLayer(self.iLayer,self.bActive,iDriver)
        if drv==self.DRV_WX:
            dc=drvInfo['dc']
            if aW>=0 and aH>=0:
                ax,ay=canvas.calcCoor(aX,aY)
                aw,ah=canvas.calcCoor(aW,aH)
                dc.SetClippingRegion(ax,ay,aw+1,ah+1)
            x,y=canvas.calcCoor(self.iX+aX,self.iY+aY)
            w,h=canvas.calcCoor(self.iWidth,self.iHeight)
            #w=int(w*min((self.fPercentage)/100.0,1.0))
            te = dc.GetTextExtent('Xy')
            if self.bFilled:
                hh=int((h-te[1])*min((self.fPercentage)/100.0,1.0))
                dc.DrawRectangle(x,y+(h-hh),w,hh)
            else:
                h-=1
                hh=int((h-te[1])*min((self.fPercentage)/100.0,1.0))
                dc.DrawLine(x,y+(h-hh),x+w,y+(h-hh))
                dc.DrawLine(x+w,y+(h-hh),x+w,y+h)
                dc.DrawLine(x,y+h,x+w,y+h)
                dc.DrawLine(x,y+(h-hh),x,y+h)
                
            if aW>=0 and aH>=0:
                dc.DestroyClippingRegion()
        elif drv==self.DRV_LATEX:
            if aW>=0 and aH>=0:
                ax,ay=canvas.calcCoorLatex(aX,aY)
                aw,ah=canvas.calcCoorLatex(aW,aH)
            x,y=canvas.calcCoorLatex(self.iX+aX,self.iY+aY)
            w,h=canvas.calcCoorLatex(self.iWidth,self.iHeight)
            w=int(w*min(self.fPercentage/100.0,1.0))
            
            y=drvInfo['y_max']-y-h
            c=drvInfo['color']
            strs=drvInfo['strs']
            tup=vtRepLatex.lineFitPict2(x,y,x+w,y)
            strs.append('    \\put(%4.1f,%4.1f){%s\\line(%d,%d){%4.1f}}'%(tup[0],tup[1],c,tup[2],tup[3],tup[4]))
            tup=vtRepLatex.lineFitPict2(x+w,y,x+w,y+h)
            strs.append('    \\put(%4.1f,%4.1f){%s\\line(%d,%d){%4.1f}}'%(tup[0],tup[1],c,tup[2],tup[3],tup[4]))
            tup=vtRepLatex.lineFitPict2(x,y+h,x+w,y+h)
            strs.append('    \\put(%4.1f,%4.1f){%s\\line(%d,%d){%4.1f}}'%(tup[0],tup[1],c,tup[2],tup[3],tup[4]))
            tup=vtRepLatex.lineFitPict2(x,y,x,y+h)
            strs.append('    \\put(%4.1f,%4.1f){%s\\line(%d,%d){%4.1f}}'%(tup[0],tup[1],c,tup[2],tup[3],tup[4]))
            pass
        if drv==self.DRV_WX:
            canvas.setActLayer(-1,self.bActive,iDriver)
            te = dc.GetTextExtent(self.text)
            dc.DrawRectangle(x,y,w,te[1])
        vtDrawCanvasText.__draw__(self,canvas,aX,aY,aW,aH,iDriver)

        vtDrawCanvasObjectBase.__drawSelect__(self,canvas,aX=aX,aY=aY,aW=aW,aH=aH,iDriver=iDriver)

