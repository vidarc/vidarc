#Boa:Dialog:vtDrawCanvasDialog
#----------------------------------------------------------------------------
# Name:         vtDrawCanvasDialog.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtDrawCanvasDialog.py,v 1.2 2008/03/26 23:11:43 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import wx.lib.masked.numctrl

import sys,traceback

import vidarc.tool.log.vtLog as vtLog

import images

def create(parent):
    return vtDrawCanvasDialog(parent)

[wxID_VTDRAWCANVASDIALOG, wxID_VTDRAWCANVASDIALOGCBADDLAYER, 
 wxID_VTDRAWCANVASDIALOGCBAPPLY, wxID_VTDRAWCANVASDIALOGCBCANCEL, 
 wxID_VTDRAWCANVASDIALOGCBDELLAYER, wxID_VTDRAWCANVASDIALOGCBDOWN, 
 wxID_VTDRAWCANVASDIALOGCBOK, wxID_VTDRAWCANVASDIALOGCBUP, 
 wxID_VTDRAWCANVASDIALOGCHCFONT, wxID_VTDRAWCANVASDIALOGCHCLAYER, 
 wxID_VTDRAWCANVASDIALOGCHCNAME, wxID_VTDRAWCANVASDIALOGCHCVAL, 
 wxID_VTDRAWCANVASDIALOGCHKENABLE, wxID_VTDRAWCANVASDIALOGCHKROTATE, 
 wxID_VTDRAWCANVASDIALOGLBLFONT, wxID_VTDRAWCANVASDIALOGLBLGRID, 
 wxID_VTDRAWCANVASDIALOGLBLLBLNAME, wxID_VTDRAWCANVASDIALOGLBLMAX, 
 wxID_VTDRAWCANVASDIALOGLBLNAME, wxID_VTDRAWCANVASDIALOGLBLSCALE, 
 wxID_VTDRAWCANVASDIALOGLBLVALUE, wxID_VTDRAWCANVASDIALOGLSTVAL, 
 wxID_VTDRAWCANVASDIALOGNBEDIT, wxID_VTDRAWCANVASDIALOGNUMSCALE, 
 wxID_VTDRAWCANVASDIALOGNUMSCALEX, wxID_VTDRAWCANVASDIALOGNUMSCALEY, 
 wxID_VTDRAWCANVASDIALOGPNGEN, wxID_VTDRAWCANVASDIALOGPNLABELS, 
 wxID_VTDRAWCANVASDIALOGPNREP, wxID_VTDRAWCANVASDIALOGPNVAL, 
 wxID_VTDRAWCANVASDIALOGSCALEVIS, wxID_VTDRAWCANVASDIALOGSNBSCALEX, 
 wxID_VTDRAWCANVASDIALOGSNVAL, wxID_VTDRAWCANVASDIALOGSPBSCALEY, 
 wxID_VTDRAWCANVASDIALOGSPGRIDX, wxID_VTDRAWCANVASDIALOGSPGRIDY, 
 wxID_VTDRAWCANVASDIALOGSPMAXX, wxID_VTDRAWCANVASDIALOGSPMAXY, 
 wxID_VTDRAWCANVASDIALOGTXTLBLNAME, wxID_VTDRAWCANVASDIALOGTXTNAME, 
 wxID_VTDRAWCANVASDIALOGTXTVAL, 
] = [wx.NewId() for _init_ctrls in range(41)]

class vtDrawCanvasDialog(wx.Dialog):
    def _init_coll_nbEdit_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.pnGen, select=False,
              text=u'General')
        parent.AddPage(imageId=-1, page=self.pnVal, select=True,
              text=u'Objects')
        parent.AddPage(imageId=-1, page=self.pnLabels, select=False,
              text=u'Layer')
        parent.AddPage(imageId=-1, page=self.pnRep, select=False,
              text=u'Report')

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTDRAWCANVASDIALOG,
              name=u'vtDrawCanvasDialog', parent=prnt, pos=wx.Point(333, 170),
              size=wx.Size(423, 332), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vtDrawCanvas Dialog')
        self.SetClientSize(wx.Size(415, 305))

        self.cbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTDRAWCANVASDIALOGCBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'cbOk',
              parent=self, pos=wx.Point(104, 272), size=wx.Size(76, 30),
              style=0)
        self.cbOk.Bind(wx.EVT_BUTTON, self.OnCbOkButton,
              id=wxID_VTDRAWCANVASDIALOGCBOK)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTDRAWCANVASDIALOGCBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel', name=u'cbCancel',
              parent=self, pos=wx.Point(216, 272), size=wx.Size(76, 30),
              style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VTDRAWCANVASDIALOGCBCANCEL)

        self.nbEdit = wx.Notebook(id=wxID_VTDRAWCANVASDIALOGNBEDIT,
              name=u'nbEdit', parent=self, pos=wx.Point(8, 8), size=wx.Size(400,
              256), style=0)

        self.pnGen = wx.Panel(id=wxID_VTDRAWCANVASDIALOGPNGEN, name=u'pnGen',
              parent=self.nbEdit, pos=wx.Point(0, 0), size=wx.Size(392, 230),
              style=wx.TAB_TRAVERSAL)

        self.lblGrid = wx.StaticText(id=wxID_VTDRAWCANVASDIALOGLBLGRID,
              label=u'Grid', name=u'lblGrid', parent=self.pnGen, pos=wx.Point(8,
              14), size=wx.Size(19, 13), style=0)

        self.spGridX = wx.SpinCtrl(id=wxID_VTDRAWCANVASDIALOGSPGRIDX, initial=0,
              max=100, min=0, name=u'spGridX', parent=self.pnGen,
              pos=wx.Point(50, 10), size=wx.Size(50, 21),
              style=wx.SP_ARROW_KEYS)
        self.spGridX.Bind(wx.EVT_TEXT, self.OnSpGridXText,
              id=wxID_VTDRAWCANVASDIALOGSPGRIDX)

        self.spGridY = wx.SpinCtrl(id=wxID_VTDRAWCANVASDIALOGSPGRIDY, initial=0,
              max=100, min=0, name=u'spGridY', parent=self.pnGen,
              pos=wx.Point(110, 10), size=wx.Size(50, 21),
              style=wx.SP_ARROW_KEYS)
        self.spGridY.Bind(wx.EVT_TEXT, self.OnSpGridYText,
              id=wxID_VTDRAWCANVASDIALOGSPGRIDY)

        self.pnVal = wx.Panel(id=wxID_VTDRAWCANVASDIALOGPNVAL, name=u'pnVal',
              parent=self.nbEdit, pos=wx.Point(0, 0), size=wx.Size(392, 230),
              style=wx.TAB_TRAVERSAL)

        self.lblName = wx.StaticText(id=wxID_VTDRAWCANVASDIALOGLBLNAME,
              label=u'Name', name=u'lblName', parent=self.pnVal, pos=wx.Point(8,
              12), size=wx.Size(28, 13), style=0)

        self.chcName = wx.Choice(choices=[], id=wxID_VTDRAWCANVASDIALOGCHCNAME,
              name=u'chcName', parent=self.pnVal, pos=wx.Point(48, 10),
              size=wx.Size(112, 21), style=wx.CB_SORT)
        self.chcName.Bind(wx.EVT_CHOICE, self.OnChcNameChoice,
              id=wxID_VTDRAWCANVASDIALOGCHCNAME)

        self.txtName = wx.TextCtrl(id=wxID_VTDRAWCANVASDIALOGTXTNAME,
              name=u'txtName', parent=self.pnVal, pos=wx.Point(164, 10),
              size=wx.Size(80, 21), style=0, value=u'')

        self.cbUp = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTDRAWCANVASDIALOGCBUP,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Up', name=u'cbUp',
              parent=self.pnVal, pos=wx.Point(248, 4), size=wx.Size(64, 30),
              style=0)
        self.cbUp.Bind(wx.EVT_BUTTON, self.OnCbUpButton,
              id=wxID_VTDRAWCANVASDIALOGCBUP)

        self.cbDown = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTDRAWCANVASDIALOGCBDOWN,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Down', name=u'cbDown',
              parent=self.pnVal, pos=wx.Point(320, 4), size=wx.Size(65, 30),
              style=0)
        self.cbDown.Bind(wx.EVT_BUTTON, self.OnCbDownButton,
              id=wxID_VTDRAWCANVASDIALOGCBDOWN)

        self.lblValue = wx.StaticText(id=wxID_VTDRAWCANVASDIALOGLBLVALUE,
              label=u'Value', name=u'lblValue', parent=self.pnVal,
              pos=wx.Point(4, 68), size=wx.Size(27, 13), style=0)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTDRAWCANVASDIALOGCBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Apply', name=u'cbApply',
              parent=self.pnVal, pos=wx.Point(152, 56), size=wx.Size(65, 30),
              style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VTDRAWCANVASDIALOGCBAPPLY)

        self.lstVal = wx.ListCtrl(id=wxID_VTDRAWCANVASDIALOGLSTVAL,
              name=u'lstVal', parent=self.pnVal, pos=wx.Point(8, 96),
              size=wx.Size(384, 128), style=wx.LC_REPORT)
        self.lstVal.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstValListItemSelected, id=wxID_VTDRAWCANVASDIALOGLSTVAL)
        self.lstVal.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstValListItemDeselected,
              id=wxID_VTDRAWCANVASDIALOGLSTVAL)

        self.lblMax = wx.StaticText(id=wxID_VTDRAWCANVASDIALOGLBLMAX,
              label=u'Max', name=u'lblMax', parent=self.pnGen, pos=wx.Point(8,
              54), size=wx.Size(20, 13), style=0)

        self.spMaxX = wx.SpinCtrl(id=wxID_VTDRAWCANVASDIALOGSPMAXX, initial=0,
              max=1000, min=0, name=u'spMaxX', parent=self.pnGen,
              pos=wx.Point(50, 50), size=wx.Size(50, 21),
              style=wx.SP_ARROW_KEYS)
        self.spMaxX.Bind(wx.EVT_TEXT, self.OnSpMaxXText,
              id=wxID_VTDRAWCANVASDIALOGSPMAXX)

        self.spMaxY = wx.SpinCtrl(id=wxID_VTDRAWCANVASDIALOGSPMAXY, initial=0,
              max=1000, min=0, name=u'spMaxY', parent=self.pnGen,
              pos=wx.Point(110, 50), size=wx.Size(50, 21),
              style=wx.SP_ARROW_KEYS)
        self.spMaxY.Bind(wx.EVT_TEXT, self.OnSpMaxYText,
              id=wxID_VTDRAWCANVASDIALOGSPMAXY)

        self.pnLabels = wx.Panel(id=wxID_VTDRAWCANVASDIALOGPNLABELS,
              name=u'pnLabels', parent=self.nbEdit, pos=wx.Point(0, 0),
              size=wx.Size(392, 230), style=wx.TAB_TRAVERSAL)

        self.chcLayer = wx.Choice(choices=[],
              id=wxID_VTDRAWCANVASDIALOGCHCLAYER, name=u'chcLayer',
              parent=self.pnLabels, pos=wx.Point(40, 8), size=wx.Size(130, 21),
              style=0)
        self.chcLayer.Bind(wx.EVT_CHOICE, self.OnChcLabelChoice,
              id=wxID_VTDRAWCANVASDIALOGCHCLAYER)

        self.txtLblName = wx.TextCtrl(id=wxID_VTDRAWCANVASDIALOGTXTLBLNAME,
              name=u'txtLblName', parent=self.pnLabels, pos=wx.Point(56, 40),
              size=wx.Size(72, 21), style=0, value=u'')

        self.lblLblName = wx.StaticText(id=wxID_VTDRAWCANVASDIALOGLBLLBLNAME,
              label=u'Name', name=u'lblLblName', parent=self.pnLabels,
              pos=wx.Point(8, 44), size=wx.Size(28, 13), style=0)

        self.cbAddLayer = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTDRAWCANVASDIALOGCBADDLAYER,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'cbAddLayer',
              parent=self.pnLabels, pos=wx.Point(200, 8), size=wx.Size(60, 30),
              style=0)
        self.cbAddLayer.Bind(wx.EVT_BUTTON, self.OnCbAddLayerButton,
              id=wxID_VTDRAWCANVASDIALOGCBADDLAYER)

        self.cbDelLayer = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTDRAWCANVASDIALOGCBDELLAYER,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Del', name=u'cbDelLayer',
              parent=self.pnLabels, pos=wx.Point(272, 8), size=wx.Size(60, 30),
              style=0)
        self.cbDelLayer.Bind(wx.EVT_BUTTON, self.OnCbDelLayerButton,
              id=wxID_VTDRAWCANVASDIALOGCBDELLAYER)

        self.pnRep = wx.Panel(id=wxID_VTDRAWCANVASDIALOGPNREP, name=u'pnRep',
              parent=self.nbEdit, pos=wx.Point(0, 0), size=wx.Size(392, 230),
              style=wx.TAB_TRAVERSAL)

        self.lblScale = wx.StaticText(id=wxID_VTDRAWCANVASDIALOGLBLSCALE,
              label=u'Scale', name=u'lblScale', parent=self.pnRep,
              pos=wx.Point(8, 12), size=wx.Size(27, 13), style=0)

        self.numScale = wx.lib.masked.numctrl.NumCtrl(id=wxID_VTDRAWCANVASDIALOGNUMSCALE,
              name=u'numScale', parent=self.pnRep, pos=wx.Point(48, 8),
              size=wx.Size(50, 22), style=0, value=0)
        self.numScale.SetFractionWidth(2)
        self.numScale.SetIntegerWidth(2)

        self.lblFont = wx.StaticText(id=wxID_VTDRAWCANVASDIALOGLBLFONT,
              label=u'Font', name=u'lblFont', parent=self.pnRep, pos=wx.Point(8,
              44), size=wx.Size(21, 13), style=0)

        self.chcFont = wx.Choice(choices=[u'micro', u'smal', u'normal', u'big',
              u'huge'], id=wxID_VTDRAWCANVASDIALOGCHCFONT, name=u'chcFont',
              parent=self.pnRep, pos=wx.Point(48, 40), size=wx.Size(100, 21),
              style=0)
        self.chcFont.SetSelection(2)

        self.chkRotate = wx.CheckBox(id=wxID_VTDRAWCANVASDIALOGCHKROTATE,
              label=u'rotate', name=u'chkRotate', parent=self.pnRep,
              pos=wx.Point(208, 8), size=wx.Size(73, 13), style=0)
        self.chkRotate.SetValue(False)

        self.numScaleX = wx.lib.masked.numctrl.NumCtrl(id=wxID_VTDRAWCANVASDIALOGNUMSCALEX,
              name=u'numScaleX', parent=self.pnGen, pos=wx.Point(50, 90),
              size=wx.Size(50, 22), style=0, value=0)
        self.numScaleX.SetIntegerWidth(2)
        self.numScaleX.SetInsertionPoint(14)
        self.numScaleX.SetFractionWidth(2)

        self.numScaleY = wx.lib.masked.numctrl.NumCtrl(id=wxID_VTDRAWCANVASDIALOGNUMSCALEY,
              name=u'numScaleY', parent=self.pnGen, pos=wx.Point(134, 90),
              size=wx.Size(50, 22), style=0, value=0)
        self.numScaleY.SetIntegerWidth(2)
        self.numScaleY.SetInsertionPoint(14)
        self.numScaleY.SetFractionWidth(2)

        self.spbScaleY = wx.SpinButton(id=wxID_VTDRAWCANVASDIALOGSPBSCALEY,
              name=u'spbScaleY', parent=self.pnGen, pos=wx.Point(186, 90),
              size=wx.Size(16, 24), style=wx.SP_VERTICAL)
        self.spbScaleY.Bind(wx.EVT_SPIN_DOWN, self.OnSpbScaleYSpinDown,
              id=wxID_VTDRAWCANVASDIALOGSPBSCALEY)
        self.spbScaleY.Bind(wx.EVT_SPIN_UP, self.OnSpbScaleYSpinUp,
              id=wxID_VTDRAWCANVASDIALOGSPBSCALEY)

        self.snbScaleX = wx.SpinButton(id=wxID_VTDRAWCANVASDIALOGSNBSCALEX,
              name=u'snbScaleX', parent=self.pnGen, pos=wx.Point(98, 90),
              size=wx.Size(16, 24), style=wx.SP_VERTICAL)
        self.snbScaleX.Bind(wx.EVT_SPIN_DOWN, self.OnSpbScaleXSpinDown,
              id=wxID_VTDRAWCANVASDIALOGSNBSCALEX)
        self.snbScaleX.Bind(wx.EVT_SPIN_UP, self.OnSpbScaleXSpinUp,
              id=wxID_VTDRAWCANVASDIALOGSNBSCALEX)

        self.ScaleVis = wx.StaticText(id=wxID_VTDRAWCANVASDIALOGSCALEVIS,
              label=u'Scale', name=u'ScaleVis', parent=self.pnGen,
              pos=wx.Point(8, 94), size=wx.Size(27, 13), style=0)

        self.txtVal = wx.TextCtrl(id=wxID_VTDRAWCANVASDIALOGTXTVAL,
              name=u'txtVal', parent=self.pnVal, pos=wx.Point(40, 64),
              size=wx.Size(100, 21), style=0, value=u'')

        self.snVal = wx.SpinCtrl(id=wxID_VTDRAWCANVASDIALOGSNVAL, initial=0,
              max=100, min=0, name=u'snVal', parent=self.pnVal, pos=wx.Point(40,
              64), size=wx.Size(100, 21), style=wx.SP_ARROW_KEYS)
        self.snVal.Show(False)

        self.chkEnable = wx.CheckBox(id=wxID_VTDRAWCANVASDIALOGCHKENABLE,
              label=_(u'visible'), name=u'chkEnable', parent=self.pnVal,
              pos=wx.Point(40, 40), size=wx.Size(73, 13), style=0)
        self.chkEnable.SetValue(False)

        self.chcVal = wx.Choice(choices=[], id=wxID_VTDRAWCANVASDIALOGCHCVAL,
              name=u'chcVal', parent=self.pnVal, pos=wx.Point(40, 64),
              size=wx.Size(100, 21), style=0)
        self.chcVal.Show(False)

        self._init_coll_nbEdit_Pages(self.nbEdit)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        self.name=u'vtDrawTiming Dialog'
        img=images.getOkBitmap()
        self.cbOk.SetBitmapLabel(img)
        self.cbApply.SetBitmapLabel(img)
        self.cbAddLayer.SetBitmapLabel(img)
        
        self.cbUp.SetBitmapLabel(images.getGrpUpBitmap())
        self.cbDown.SetBitmapLabel(images.getGrpDownBitmap())
        
        self.txtName.Enable(False)
        
        img=images.getCancelBitmap()
        self.cbCancel.SetBitmapLabel(img)
        self.iSelLblIdx=-1
        self.iSelIdx=-1
        self.canvas=None
        self.lstGrp=[]
        self.numScaleX.SetValue(1.0)
        self.numScaleY.SetValue(1.0)
        
        img=images.getDelBitmap()
        #self.cbDel.SetBitmapLabel(img)
        #self.cbDelName.SetBitmapLabel(img)
        #self.cbDelLbl.SetBitmapLabel(img)
        
        self.chcLayer.SetSelection(0)
        
        self.lstVal.InsertColumn(0,_(u'Nr'),wx.LIST_FORMAT_RIGHT,30)
        self.lstVal.InsertColumn(1,_(u'Name'),wx.LIST_FORMAT_LEFT,80)
        self.lstVal.InsertColumn(2,_(u'Value'),wx.LIST_FORMAT_RIGHT,60)
        
        self.txtVal.Show(False)
        self.snVal.Show(False)
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        
    def SetName(self,name):
        self.name=name
    def SetReportValue(self,scale,fontsize,rotate):
        self.numScale.SetValue(scale)
        self.chcFont.SetSelection(fontsize)
        self.chkRotate.SetValue(rotate)
    def SetCanvas(self,canvas):
        self.canvas=canvas
    def SetValue(self,**kwargs):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if kwargs.has_key('grid'):
            self.grid=kwargs['grid']
            self.spGridX.SetValue(self.grid[0])
            self.spGridY.SetValue(self.grid[1])
        if kwargs.has_key('max'):
            self.max=kwargs['max']
            self.spMaxX.SetValue(self.max[0])
            self.spMaxY.SetValue(self.max[1])
        if kwargs.has_key('objects'):
            self.lst=kwargs['objects']
        if kwargs.has_key('canvas'):
            self.canvas=kwargs['canvas']
        if kwargs.has_key('report_scale'):
            self.numScale.SetValue(kwargs['report_scale'])
        if kwargs.has_key('report_font'):
            self.chcFont.SetSelection(kwargs['report_font'])
        if kwargs.has_key('report_rotate'):
            self.chkRotate.SetValue(kwargs['report_rotate'])
        
        self.iSelIdx=-1
        self.iSelLblIdx=-1
        #self.__updateLabels__()
        self.lstGrp=[]
        self.txtName.SetValue('')
        
        self.__updateLst__()
    def GetValue(self,name):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if name=='objects':
            return self.lst
        elif name=='max':
            return self.max
        elif name=='grid':
            return self.grid
        elif name=='xml_level':
            return self.iXmlLevel
        elif name=='report_scale':
            return self.numScale.GetValue()
        elif name=='report_font':
            return self.chcFont.GetSelection()
        elif name=='report_rotate':
            return self.chkRotate.GetValue()

    def __updateLst__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.txtVal.Show(False)
        self.snVal.Show(False)
        self.chcVal.Show(False)
        
        self.chcName.Clear()
        self.chcLayer.Clear()
        self.lstVal.DeleteAllItems()
        try:
            i=0
            for it in self.lst:
                sName=it.GetName()
                idx=self.chcName.Append(sName)
                self.chcName.SetClientData(idx,i)
                i+=1
            self.chcName.SetSelection(0)
            wx.CallAfter(self.__selName__)
        except:
            traceback.print_exc()
    def __updateLayer__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        i=self.chcLayer.GetSelection()
        #self.lstLabels.DeleteAllItems()
        
        if self.labels==None:
            self.labels=[[],[]]
        try:
            lst=self.labels[i]
            def cmpFunc(a,b):
                return int(a[0]-b[0])
            lst.sort(cmpFunc)
            for it in lst:
                idx=self.lstLabels.InsertImageStringItem(sys.maxint,'%4.2f'%it[0],-1)
                self.lstLabels.SetStringItem(idx,1,it[1],-1)
        except:
            pass
    def __selName__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        idx=self.chcName.GetSelection()
        iSel=self.chcName.GetClientData(idx)
        obj=self.lst[iSel]
        self.lstVal.DeleteAllItems()
        self.iSelIdx=-1
        self.chkEnable.SetValue(obj.GetVisible())
        i=0
        bFound=True
        while 1==1:
            sName,sVal,sFmt,sTyp=obj.GetAttr(i)
            if sTyp==-1:
                break
            idx=self.lstVal.InsertImageStringItem(sys.maxint, '%d'%i, -1)
            self.lstVal.SetStringItem(idx,1,sName,-1)
            self.lstVal.SetStringItem(idx,2,sFmt%sVal,-1)
            i+=1
        
    def OnCbOkButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.canvas.ShowAll()
        self.Show(False)
        event.Skip()

    def OnCbCancelButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.Show(False)
        event.Skip()

    def OnLstValListItemSelected(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.iSelIdx=event.GetIndex()
        idx=self.chcName.GetSelection()
        iSel=self.chcName.GetClientData(idx)
        obj=self.lst[iSel]
        
        self.txtVal.Show(False)
        self.snVal.Show(False)
        
        sName,sVal,sFmt,sTyp=obj.GetAttr(self.iSelIdx)
        if sTyp==-1:
            pass
        elif sTyp==0:
            self.txtVal.SetValue(sVal)
            self.txtVal.Show(True)
        elif sTyp==1:
            self.snVal.SetValue(sVal)
            self.snVal.Show(True)
        elif sTyp==2:
            self.chcVal.Clear()
            for s in obj.GetAttrEntries(self.iSelIdx):
                self.chcVal.Append(s)
            self.chcVal.SetSelection(obj.GetAttrEntrySel(self.iSelIdx))
            self.chcVal.Show(True)
        event.Skip()

    def OnLstValListItemDeselected(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.iSelIdx=-1
        self.txtVal.Show(False)
        self.snVal.Show(False)
        event.Skip()

    def OnChcNameChoice(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.__selName__()
        event.Skip()

    def OnSpbScaleXSpinDown(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        fVal=self.numScaleX.GetValue()
        fVal-=0.1
        self.numScaleX.SetValue(fVal)
        event.Skip()

    def OnSpbScaleXSpinUp(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        fVal=self.numScaleX.GetValue()
        fVal+=0.1
        self.numScaleX.SetValue(fVal)
        event.Skip()

    def OnSpbScaleYSpinDown(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        fVal=self.numScaleY.GetValue()
        fVal-=0.1
        self.numScaleY.SetValue(fVal)
        event.Skip()

    def OnSpbScaleYSpinUp(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        fVal=self.numScaleY.GetValue()
        fVal+=0.1
        self.numScaleY.SetValue(fVal)
        event.Skip()
    
    def __getBaseObj__(self,obj):
        if len(self.lstGrp)>0:
            return self.lstGrp[0][1]
        return obj
    def OnCbApplyButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.iSelIdx<0:
            return
        idx=self.chcName.GetSelection()
        iSel=self.chcName.GetClientData(idx)
        obj=self.lst[iSel]
        
        obj.SetVisible(False)
        objBase=self.__getBaseObj__(obj)
        self.canvas.ShowObj(objBase,False)
        
        sName,sVal,sFmt,sTyp=obj.GetAttr(self.iSelIdx)
        if sTyp==-1:
            return
        elif sTyp==0:
            val=self.txtVal.GetValue()
        elif sTyp==1:
            val=self.snVal.GetValue()
        elif sTyp==2:
            val=self.chcVal.GetSelection()
            
        obj.SetAttr(self.iSelIdx,val)
        sName,val,sFmt,sTyp=obj.GetAttr(self.iSelIdx)
        if self.iSelIdx>=0:
            self.lstVal.SetStringItem(self.iSelIdx,2,sFmt%val,-1)
        #event.Skip()
        obj.SetVisible(self.chkEnable.GetValue())
        objBase=self.__getBaseObj__(obj)
        self.canvas.ShowObj(objBase,True)
        
    def OnCbDelButton(self, event):
        return
        iSel=self.chcName.GetSelection()
        dict=self.lst[iSel]
        vals=dict['values']
        if self.iSelIdx>=0:
            dict['values']=vals[:self.iSelIdx]+vals[self.iSelIdx+1:]
            self.lstVal.DeleteItem(self.iSelIdx)
            self.iSelIdx=-1
        event.Skip()

    def OnCbUpButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if len(self.lstGrp)>0:
            self.lst=self.lstGrp[-1][0]
            self.lstGrp=self.lstGrp[:-1]
            if len(self.lstGrp)>0:
                self.txtName.SetValue(self.lstGrp[-1][1].GetName())
            else:
                self.txtName.SetValue('')
            self.__updateLst__()
        event.Skip()

    def OnCbDownButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        idx=self.chcName.GetSelection()
        iSel=self.chcName.GetClientData(idx)
        obj=self.lst[iSel]
        try:
            objs=obj.GetObjects()
            self.txtName.SetValue(obj.GetName())
            self.lstGrp.append((self.lst,obj))
            self.lst=objs
            self.__updateLst__()
        except:
            pass
        event.Skip()

    def OnSpMaxXText(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.max=(self.spMaxX.GetValue(),self.max[1])
        event.Skip()

    def OnSpMaxYText(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.max=(self.max[0],self.spMaxY.GetValue())
        event.Skip()

    def OnLstLabelsListItemDeselected(self, event):
        self.iSelLblIdx=-1
        event.Skip()

    def OnLstLabelsListItemSelected(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.iSelLblIdx=event.GetIndex()
        idx=self.iSelLblIdx
        it=self.lstLabels.GetItem(idx,0)
        sPos=it.m_text
        it=self.lstLabels.GetItem(idx,1)
        sName=it.m_text
        self.txtLblName.SetValue(sName)
        self.numPos.SetValue(sPos)
        event.Skip()

    def OnCbAddLayerButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        i=self.chcLabel.GetSelection()
        tup=(self.numPos.GetValue(),self.txtLblName.GetValue())
        if self.iSelLblIdx>=0:
            lbls=self.labels[i]
            self.labels[i]=lbls[:self.iSelLblIdx]+[tup]+lbls[self.iSelLblIdx+1:]
            self.lstLabels.SetStringItem(self.iSelLblIdx,0,'%4.2f'%tup[0],-1)
            self.lstLabels.SetStringItem(self.iSelLblIdx,1,tup[1],-1)
        else:
            self.labels[i].append(tup)
            idx=self.lstLabels.InsertImageStringItem(sys.maxint,'%4.2f'%tup[0],-1)
            self.lstLabels.SetStringItem(idx,1,tup[1],-1)
        event.Skip()

    def OnCbDelLayerButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        i=self.chcLabel.GetSelection()
        if self.iSelLblIdx>=0:
            lbls=self.labels[i]
            self.labels[i]=lbls[:self.iSelLblIdx]+lbls[self.iSelLblIdx+1:]
            self.lstLabels.DeleteItem(self.iSelLblIdx)
            self.iSelLblIdx=-1
        event.Skip()

    def OnSpRngMaxText(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        iSel=self.chcName.GetSelection()
        dict=self.lst[iSel]
        dict['range']=(self.spRngMin.GetValue(),self.spRngMax.GetValue())
        event.Skip()

    def OnSpRngMinText(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        iSel=self.chcName.GetSelection()
        dict=self.lst[iSel]
        dict['range']=(self.spRngMin.GetValue(),self.spRngMax.GetValue())
        event.Skip()

    def OnChcLabelChoice(self, event):
        self.__updateLabels__()
        event.Skip()
    
    def GetReportValueF(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        return self.numScale.GetValue(),self.chcFont.GetSelection(),self.chkRotate.GetValue()
        
    def GetFontSizeF(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        return self.chcFont.GetSelection()
    
    def GetRotateF(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        return self.chkRotate.GetValue()
        
    def GetScaleF(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        return self.numScale.GetValue()
        
    def GetMaxF(self):
        return self.max
    
    def GetGridF(self):
        return self.grid
    
    def GetValuesF(self):
        return self.lst
        
    def GetLabelsF(self):
        return self.labels
        
    def OnSpGridXText(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.grid=(self.spGridX.GetValue(),self.grid[1])
        event.Skip()

    def OnSpGridYText(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.grid=(self.grid[0],self.spGridY.GetValue())
        event.Skip()

    def __checkAddPossible__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.iSelIdx==-1:
            dlg=wx.MessageDialog(self,_(u'Select list item first!') ,
                        self.name,
                        wx.CANCEL|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.Centre()
            dlg.ShowModal()
            dlg.Destroy()
            return False
        return True
    def OnCbAddSetMinButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.__checkAddPossible__()==False:
            return
        v=self.numY.GetValue()
        if v!=-1.0:
            self.__add__([v,0.0])
        else:
            self.__add__([0.0])
        event.Skip()

    def __add__(self,nValLst):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        i,vals=self.__getSelVal__()
        i=self.__getNextValidTimeEntry__(i,vals)
        if i<0:
            return
        tup=vals[i]
        iOld=i
        for nVal in nValLst:
            n=[round(tup[0]-0.5)+1.0,nVal]
            vals.insert(i+1,n)
            idx=self.lstVal.InsertImageStringItem(i+1, '%d'%(i+1), -1)
            self.lstVal.SetStringItem(idx,1,'%6.2f'%n[0],-1)
            self.lstVal.SetStringItem(idx,2,'%6.2f'%n[1],-1)
            i+=1
        
        for idx in range(i+1,len(vals)):
            self.lstVal.SetStringItem(idx,0,'%d'%idx,-1)
            
        self.__manipulateVals__(1.0,i+1)
        self.lstVal.SetItemState(iOld, 0 ,wx.LIST_STATE_SELECTED )
        self.lstVal.SetItemState(i, wx.LIST_STATE_SELECTED ,wx.LIST_STATE_SELECTED )
    def OnCbAddSetMaxButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.__checkAddPossible__()==False:
            return
        v=self.numY.GetValue()
        if v!=-1.0:
            self.__add__([v,1.0])
        else:
            self.__add__([1.0])
        event.Skip()

    def OnCbAddSetUndefButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.__checkAddPossible__()==False:
            return
        v=self.numY.GetValue()
        if v!=-1.0:
            self.__add__([v,-1.0])
        else:
            self.__add__([-1.0])
        event.Skip()

    def OnCbAddToggleButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if self.__checkAddPossible__()==False:
            return
        v=self.numY.GetValue()
        if v==0.0:
            self.__add__([0.0,1.0])
        elif v==1.0:
            self.__add__([1.0,0.0])
        elif v==-1.0:
            self.__add__([-1.0])
        else:
            self.__add__([v])
        event.Skip()

    def OnCbTruncXButton(self, event):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        fVal=self.numX.GetValue()
        self.numX.SetValue(round(fVal-0.5))
        event.Skip()

    def OnCbMoveDownButton(self, event):
        if self.__checkAddPossible__()==False:
            return
        self.__manipulateVals__(1.0)
        event.Skip()

    def OnCbMoveUpButton(self, event):
        if self.__checkAddPossible__()==False:
            return
        self.__manipulateVals__(-1.0)
        event.Skip()
    def __getNextValidTimeEntry__(self,idx,vals):
        bFound=False
        i=idx
        iLen=len(vals)
        while bFound==False:
            if i>0:
                if i>=iLen:
                    return -1
                v=vals[i][0]
                if v>=0:
                    return i
            else:
                return -1
            i+=1
            
    def __manipulateVals__(self,dif,idx=-1):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        i,vals=self.__getSelVal__()
        if idx==-1:
            idx=i
        bFound=False
        i=idx
        while bFound==False:
            i-=1
            if i>0:
                v=vals[i][0]
                if v>=0:
                    bFound=True
            else:
                v=0.0
                bFound=True
        bPossible=True
        for i in range(idx,len(vals)):
            if vals[i][0]<0:
                continue
            nv=vals[i][0]+dif
            if nv<v:
                bPossible=False
                break
            v=nv
        if bPossible:
            for i in range(idx,len(vals)):
                if vals[i][0]<0:
                    continue
                vals[i][0]+=dif
                self.lstVal.SetStringItem(i,1,'%6.2f'%vals[i][0],-1)
        
    def __getSelVal__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        iSel=self.chcName.GetSelection()
        dict=self.lst[iSel]
        vals=dict['values']
        return (self.iSelIdx , vals)
        if 1==0:
            dict['values']=vals[:self.iSelIdx]+[tup]+vals[self.iSelIdx+1:]
            self.lstVal.SetStringItem(self.iSelIdx,1,'%6.2f'%tup[0],-1)
            self.lstVal.SetStringItem(self.iSelIdx,2,'%6.2f'%tup[1],-1)
        else:
            idx=self.lstVal.InsertImageStringItem(sys.maxint, '%d'%len(vals), -1)
            self.lstVal.SetStringItem(idx,1,'%6.2f'%tup[0],-1)
            self.lstVal.SetStringItem(idx,2,'%6.2f'%tup[1],-1)
            vals.append(tup)
        
