#----------------------------------------------------------------------------
# Name:         vtDrawCanvasTreeList.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtDrawCanvasTreeList.py,v 1.6 2008/03/26 23:11:43 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import wx
import wx.gizmos

import vidarc.tool.log.vtLog as vtLog

import traceback
import fnmatch,time
import thread,threading


from vtDrawCanvasGroup import vtDrawCanvasGroup
from vidarc.tool.xml.vtXmlGrpTree import vtXmlGrpTree

import images

class vtDrawCanvasTreeList(wx.gizmos.TreeListCtrl,vtXmlGrpTree):
    def __init__(self, parent, id, pos, size, style, name,cols=None,
                    verbose=0):
        if id<0:
            id=wx.NewId()
        #vtXmlGrpTree.__init__(self)
        wx.gizmos.TreeListCtrl.__init__(self,id=id, name=name,
              parent=parent, pos=pos, size=size,style=style)
        if cols is None:
            cols=[_(u'Name'),_(u'Value')]
        self.cols=cols
        for it in cols:
            #wx.gizmos.TreeListCtrl.AddColumn(self,it)
            self.AddColumn(it)
        self.SetMainColumn(0)
        self.SetColumnWidth(0,200)
        
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.lang=None
        self.languages=[]
        self.languageIds=[]
        
        self.SetupImageList()
        self.SetImageList(self.imgLstTyp)
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        try:
            del self.languages
            del self.languageIds
        except:
            #vtLog.vtLngTB('del')
            pass
    def Clear(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        wx.gizmos.TreeListCtrl.DeleteAllItems(self)
    def SetupImageList(self):
        if vtXmlGrpTree.SetupImageList(self)==False:
            return
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        #self.imgDict={'elem':{},'attr':{},'grp':{}}
        #self.imgLstTyp=wx.ImageList(16,16)
        if 'elem' not in self.imgDict:
            self.imgDict['elem']={}
        if 'attr' not in self.imgDict:
            self.imgDict['attr']={}
        if 'grp' not in self.imgDict:
            self.imgDict['grp']={}
        img=images.getElemBitmap()
        self.imgDict['elem']['dft']=[self.imgLstTyp.Add(img)]
        img=images.getElemSelBitmap()
        self.imgDict['elem']['dft'].append(self.imgLstTyp.Add(img))
        
        img=images.getDrawLineBitmap()
        idx=self.imgLstTyp.Add(img)
        self.imgDict['elem']['line']=[idx,idx]
        
        img=images.getDrawTextBitmap()
        idx=self.imgLstTyp.Add(img)
        self.imgDict['elem']['text']=[idx,idx]
        
        img=images.getDrawMultiTextBitmap()
        idx=self.imgLstTyp.Add(img)
        self.imgDict['elem']['textMultiLine']=[idx,idx]
        
        img=images.getDrawRectBitmap()
        idx=self.imgLstTyp.Add(img)
        self.imgDict['elem']['rectangle']=[idx,idx]
        
        img=images.getDrawGrpBitmap()
        idx=self.imgLstTyp.Add(img)
        self.imgDict['elem']['group']=[idx,idx]
        self.imgDict['grp']['group']=[idx,idx]
        
        img=images.getAttributeBitmap()
        self.imgDict['attr']['dft']=[self.imgLstTyp.Add(img)]
        img=images.getAttributeSelBitmap()
        self.imgDict['attr']['dft'].append(self.imgLstTyp.Add(img))
        
    def __getImages__(self,sKind,sFmt,sName):
        try:
            img=self.imgDict[sKind][sFmt][0]
            imgSel=self.imgDict[sKind][sFmt][1]
        except:
            try:
                img=self.imgDict[sKind]['dft'][0]
                imgSel=self.imgDict[sKind]['dft'][1]
            except:
                img=self.imgDict['elem']['dft'][0]
                imgSel=self.imgDict['elem']['dft'][1]
        return img,imgSel
    def __getRootName__(self):
        return _(u'canvas objects')
    def AddGrpObj(self,tnparent,obj):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        try:
            if tnparent is None:
                return
            op=self.GetPyData(tnparent)
            tnGroup=None
            ti=self.GetFirstChild(tnparent)
            while ti[0].IsOk():
                o=self.GetPyData(ti[0])
                if o==op:
                    tnGroup=ti[0]
                    break
                ti=self.GetNextChild(tnparent,ti[1])
            if tnGroup is None:
                return
            sType=obj.GetTypeName()
            sName=obj.GetName()
            tid=wx.TreeItemData()
            tid.SetData(obj)
            imgTuple=self.__getImages__('elem',sType,sName)
            
            tn=self.AppendItem(tnGroup,sName,-1,-1,tid)
            self.SetItemImage(tn,imgTuple[0],0,wx.TreeItemIcon_Normal)
            self.SetItemImage(tn,imgTuple[1],0,wx.TreeItemIcon_Expanded)
            
            imgTuple=self.__getImages__('attr','dft',sName)
            tnGrp=self.AppendItem(tn,_(u'attribute'),-1,-1,None)
            self.SetItemImage(tnGrp,imgTuple[0],0,wx.TreeItemIcon_Normal)
            self.SetItemImage(tnGrp,imgTuple[1],0,wx.TreeItemIcon_Expanded)
            i=0
            while 1==1:
                sAttrName,sAttrVal,sAttrFmt,iAttrType=obj.GetAttr(i)
                if iAttrType==-1:
                    break
                tid=wx.TreeItemData()
                tid.SetData((obj,i))
                tnAttr=self.AppendItem(tnGrp,sAttrName,-1,-1,tid)
                self.SetItemImage(tnAttr,imgTuple[0],0,wx.TreeItemIcon_Normal)
                self.SetItemImage(tnAttr,imgTuple[1],0,wx.TreeItemIcon_Expanded)
                self.SetItemText(tnAttr,sAttrFmt%sAttrVal,1)
                i+=1
            self.SortChildren(tnGroup)
        except:
            traceback.print_exc()
    def __addObjects__(self,tnparent,objs):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        for it in objs:
            sType=it.GetTypeName()
            sName=it.GetName()
            tid=wx.TreeItemData()
            tid.SetData(it)
            imgTuple=self.__getImages__('elem',sType,sName)
            
            tn=self.AppendItem(tnparent,sName,-1,-1,tid)
            self.SetItemImage(tn,imgTuple[0],0,wx.TreeItemIcon_Normal)
            self.SetItemImage(tn,imgTuple[1],0,wx.TreeItemIcon_Expanded)
            
            imgTuple=self.__getImages__('attr','dft',sName)
            tnGrp=self.AppendItem(tn,_(u'attribute'),-1,-1,None)
            self.SetItemImage(tnGrp,imgTuple[0],0,wx.TreeItemIcon_Normal)
            self.SetItemImage(tnGrp,imgTuple[1],0,wx.TreeItemIcon_Expanded)
            i=0
            while 1==1:
                sAttrName,sAttrVal,sAttrFmt,iAttrType=it.GetAttr(i)
                if iAttrType==-1:
                    break
                tid=wx.TreeItemData()
                tid.SetData((it,i))
                tnAttr=self.AppendItem(tnGrp,sAttrName,-1,-1,tid)
                self.SetItemImage(tnAttr,imgTuple[0],0,wx.TreeItemIcon_Normal)
                self.SetItemImage(tnAttr,imgTuple[1],0,wx.TreeItemIcon_Expanded)
                self.SetItemText(tnAttr,sAttrFmt%sAttrVal,1)
                i+=1
            try:
                objsChild=it.GetObjects()
                imgTuple=self.__getImages__('grp','group',sName)
                tid=wx.TreeItemData()
                tid.SetData(it)
                tnGrp=self.AppendItem(tn,_(u'group'),-1,-1,tid)
                self.SetItemImage(tnGrp,imgTuple[0],0,wx.TreeItemIcon_Normal)
                self.SetItemImage(tnGrp,imgTuple[1],0,wx.TreeItemIcon_Expanded)
                self.__addObjects__(tnGrp,objsChild)
            except:
                pass
        self.SortChildren(tnparent)
    def GetTreeItemAttr(self,ti):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if ti is None:
            return None
        sCmp=_(u'attribute')
        tic=self.GetFirstChild(ti)
        while tic[0].IsOk():
            if self.GetItemText(tic[0])==sCmp:
                return tic[0]
            tic=self.GetNextChild(ti,tic[1])
        return None
    def GetTreeItemGrp(self,ti):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if ti is None:
            return None
        sCmp=_(u'group')
        tic=self.GetFirstChild(ti)
        while tic[0].IsOk():
            if self.GetItemText(tic[0])==sCmp:
                return tic[0]
            tic=self.GetNextChild(ti,tic[1])
        return None
    def GetChildTreeItem(self,ti,sCmp):
        if ti is None:
            return None
        tic=self.GetFirstChild(ti)
        while tic[0].IsOk():
            if self.GetItemText(tic[0])==sCmp:
                return tic[0]
            tic=self.GetNextChild(ti,tic[1])
        return None
    def SetObjects(self,objs):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        self.Clear()
        
        #tid=wxTreeItemData()
        #tid.SetData(o)
        tn=self.AddRoot(self.__getRootName__(),-1,-1,None)
        imgTuple=self.__getImages__('grp','group','')
        self.SetItemImage(tn,imgTuple[0],0,wx.TreeItemIcon_Normal)
        self.SetItemImage(tn,imgTuple[1],0,wx.TreeItemIcon_Expanded)
        self.__addObjects__(tn,objs)
        pass
    def FindTreeItemByObj(self,obj,ti=None):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if ti is None:
            ti=self.GetRootItem()
            if ti is None:
                return None
        tic=self.GetFirstChild(ti)
        while tic[0].IsOk():
            o=self.GetPyData(tic[0])
            if o==obj:
                return tic[0]
            if self.ItemHasChildren(tic[0])==True:
                nf=self.FindTreeItemByObj(obj,tic[0])
                if nf is not None:
                    return nf
            #ti=self.GetNextChild(ti[0],ti[1])
            tic=self.GetNextChild(ti,tic[1])
        return None
    def FindTreeItemByID(self,id,ti=None):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if ti is None:
            ti=self.GetRootItem()
            if ti is None:
                return None
        tic=self.GetFirstChild(ti)
        while tic[0].IsOk():
            o=self.GetPyData(tic[0])
            try:
                if o.id==id:
                    return tic[0]
            except:
                pass
            if self.ItemHasChildren(tic[0])==True:
                nf=self.FindTreeItemByID(id,tic[0])
                if nf is not None:
                    return nf
            #ti=self.GetNextChild(ti[0],ti[1])
            tic=self.GetNextChild(ti,tic[1])
        return None
    def __checkMainThd__(self):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
