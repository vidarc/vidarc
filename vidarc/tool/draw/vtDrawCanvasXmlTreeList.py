#----------------------------------------------------------------------------
# Name:         vtDrawCanvasXmlTreeList.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtDrawCanvasXmlTreeList.py,v 1.2 2008/03/26 23:11:43 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vtDrawCanvasTreeList import *

class vtDrawCanvasXmlTreeList(vtDrawCanvasTreeList):
    def __getRootName__(self):
        return _(u'canvas XML objects')
    def SetupImageList(self):
        vtDrawCanvasTreeList.SetupImageList(self)
        
        img=images.getDrawGrpXmlBitmap()
        idx=self.imgLstTyp.Add(img)
        self.imgDict['elem']['XmlGroup']=[idx,idx]
        self.imgDict['grp']['XmlGroup']=[idx,idx]
        
    def __addObjects__(self,tnparent,objs):
        self.__checkMainThd__()
        for it in objs:
            sType=it.GetTypeName()
            sName=it.GetName()
            tid=wx.TreeItemData()
            tid.SetData(it)
            imgTuple=self.__getImages__('elem',sType,sName)
            
            tn=self.AppendItem(tnparent,sName,-1,-1,tid)
            self.SetItemImage(tn,imgTuple[0],0,wx.TreeItemIcon_Normal)
            self.SetItemImage(tn,imgTuple[1],0,wx.TreeItemIcon_Expanded)
            
            imgTuple=self.__getImages__('attr','dft',sName)
            tnGrp=self.AppendItem(tn,_(u'attribute'),-1,-1,None)
            self.SetItemImage(tnGrp,imgTuple[0],0,wx.TreeItemIcon_Normal)
            self.SetItemImage(tnGrp,imgTuple[1],0,wx.TreeItemIcon_Expanded)
            i=0
            while 1==1:
                sAttrName,sAttrVal,sAttrFmt,iAttrType=it.GetAttr(i)
                if iAttrType==-1:
                    break
                tid=wx.TreeItemData()
                tid.SetData((it,i))
                tnAttr=self.AppendItem(tnGrp,sAttrName,-1,-1,tid)
                self.SetItemImage(tnAttr,imgTuple[0],0,wx.TreeItemIcon_Normal)
                self.SetItemImage(tnAttr,imgTuple[1],0,wx.TreeItemIcon_Expanded)
                self.SetItemText(tnAttr,sAttrFmt%sAttrVal,1)
                i+=1
            try:
                objsChild=it.GetObjectsXml()
                imgTuple=self.__getImages__('grp','XmlGroup',sName)
                tnGrp=self.AppendItem(tn,_(u'XML'),-1,-1,None)
                self.SetItemImage(tnGrp,imgTuple[0],0,wx.TreeItemIcon_Normal)
                self.SetItemImage(tnGrp,imgTuple[1],0,wx.TreeItemIcon_Expanded)
                self.__addObjects__(tnGrp,objsChild)
            except:
                pass
            try:
                objsChild=it.GetObjects()
                imgTuple=self.__getImages__('grp','group',sName)
                tid=wx.TreeItemData()
                tid.SetData(it)
                tnGrp=self.AppendItem(tn,_(u'group'),-1,-1,tid)
                self.SetItemImage(tnGrp,imgTuple[0],0,wx.TreeItemIcon_Normal)
                self.SetItemImage(tnGrp,imgTuple[1],0,wx.TreeItemIcon_Expanded)
                self.__addObjects__(tnGrp,objsChild)
            except:
                pass
        self.SortChildren(tnparent)
    def GetTreeItemXml(self,ti):
        self.__checkMainThd__()
        if ti is None:
            return None
        sCmp=_(u'XML')
        tic=self.GetFirstChild(ti)
        while tic[0].IsOk():
            if self.GetItemText(tic[0])==sCmp:
                return tic[0]
            tic=self.GetNextChild(ti,tic[1])
        return None
    def dummy(self):
        pass

