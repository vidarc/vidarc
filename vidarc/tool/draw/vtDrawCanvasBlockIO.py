#----------------------------------------------------------------------------
# Name:         vtDrawCanvasBlockIO.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtDrawCanvasBlockIO.py,v 1.1 2005/12/11 23:04:48 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vtDrawCanvasTextMultiLine import *
from vtDrawCanvasLine import *

class vtDrawCanvasBlockIO(vtDrawCanvasTextMultiLine):
    TYPE_IN     = 0
    TYPE_OUT    = 2
    TYPE_PARAM  = 1
    TYPE_INOUT  = 3
    TYPES=[u'in',u'out',u'inout',u'param']
    DISPLAY_VAL     = 0
    DISPLAY_NAME    = 1
    DISPLAY_BOTH    = 2
    DISPLAY=[u'value',u'name',u'both']
    def __init__(self,id=-1,name='',text='',x=0,y=0,w=-1,h=-1,
            type=TYPE_IN,order=-1,display=DISPLAY_VAL,layer=0):
        vtDrawCanvasTextMultiLine.__init__(self,id,name,text,x,y,w,h,layer)
        self.line=vtDrawCanvasLine(id,'__line',x,y,1,0,layer)
        self.SetMargin(4)
        self.type=type
        self.sVal=text
        self.display=display
        if type==self.TYPE_IN:
            self.alignHori=self.ALIGN_LEFT
        elif type==self.TYPE_OUT:
            self.alignHori=self.ALIGN_RIGHT
        elif type==self.TYPE_INOUT:
            self.alignHori=self.ALIGN_LEFT
        else:
            self.alignHori=self.ALIGN_CENTER
        self.order=order
        self.__setText__()
        self.__setLine__()
    def __setLine__(self):
        #print self.type,self.TYPE_IN,self.TYPE_INOUT,self.TYPE_OUT
        if self.type==self.TYPE_IN:
            self.line.SetX(0)
            self.line.SetVisible(True)
        elif self.type==self.TYPE_OUT:
            self.line.SetX(self.iX+self.iWidth)
            self.line.SetVisible(True)
        elif self.type==self.TYPE_INOUT:
            self.line.SetX(0)
            self.line.SetVisible(True)
        else:
            #print 'invis'
            self.alignHori=self.ALIGN_CENTER
            self.line.SetVisible(False)
        self.line.SetY(self.iY+round(self.iHeight/2.0))
    def SetX(self,val):
        vtDrawCanvasTextMultiLine.SetX(self,val)
        self.__setLine__()
    def SetY(self,val):
        vtDrawCanvasTextMultiLine.SetY(self,val)
        self.__setLine__()
    def SetVisible(self,flag):
        vtDrawCanvasTextMultiLine.SetVisible(self,flag)
        self.line.SetVisible(flag)
    def __setText__(self):
        if self.display==self.DISPLAY_VAL:
            self.text=self.sVal
        elif self.display==self.DISPLAY_NAME:
            self.text=self.name
        elif self.display==self.DISPLAY_BOTH:
            self.text=self.name+'\n'+self.sVal
        
    def GetAttr(self,idx):
        if idx==0:
            return 'text',self.sVal,'%s',0
        elif idx==1:
            if self.type==self.TYPE_IN:
                val=_('input')
            elif self.type==self.TYPE_OUT:
                val=_('output')
            elif self.type==self.TYPE_INOUT:
                val=_('input output')
            elif self.type==self.TYPE_PARAM:
                val=_('parameter')
            return 'type',val,'%s',2
        elif idx==2:
            return 'order',self.order,'%d',1
        elif idx==3:
            if self.display==self.DISPLAY_VAL:
                val=_('value')
            elif self.display==self.DISPLAY_NAME:
                val=_('name')
            elif self.display==self.DISPLAY_BOTH:
                val=_('both')
            return 'display',val,'%s',2
        return '','','',-1
    def SetAttr(self,idx,val):
        if idx==0:
            self.sVal=val
            self.__setText__()
        elif idx==1:
            if val==0:
                self.type=self.TYPE_IN
            elif val==1:
                self.type=self.TYPE_OUT
            elif val==2:
                self.type=self.TYPE_INOUT
            elif val==3:
                self.type=self.TYPE_PARAM
            self.__setLine__()
            return 0
        elif idx==2:
            self.order=val
            return 0
        elif idx==3:
            if val==0:
                self.display=self.DISPLAY_VAL
            elif val==1:
                self.display=self.DISPLAY_NAME
            elif val==2:
                self.display=self.DISPLAY_BOTH
            self.__setText__()
            return 0
        return -1
    def GetDefaults(self):
        return 'order:%d,type:%d,display:%d'%(self.order,self.type,self.display)
    def SetDefaults(self,s):
        strs=string.split(s,',')
        for sd in strs:
            s2=string.split(sd,':')
            try:
                if s2[0]=='type':
                    self.type=int(s2[1])
                if s2[0]=='display':
                    self.display=int(s2[1])
                if s2[0]=='order':
                    self.order=int(s2[1])
            except:
                pass
        self.__setText__()
        self.__setLine__()
        pass
    
    def GetAttrEntries(self,idx):
        if idx==1:
            return [_(u'input'),_(u'output'),_(u'input output'),_(u'parameter')]
        if idx==3:
            return [_(u'value'),_(u'name'),_(u'both')]
    def GetAttrEntrySel(self,idx):
        if idx==1:
            if self.type==self.TYPE_IN:
                return 0
            elif self.type==self.TYPE_OUT:
                return 1
            elif self.type==self.TYPE_INOUT:
                return 2
            elif self.type==self.TYPE_PARAM:
                return 3
            return 0
        if idx==3:
            if self.display==self.DISPLAY_VAL:
                return 0
            elif self.display==self.DISPLAY_NAME:
                return 1
            elif self.display==self.DISPLAY_BOTH:
                return 2
            return 0
        return -1
    def GetTypeName(self):
        # replace me
        return 'blockIO'
    def SetNode(self,doc,node,canvas):
        vtDrawCanvasTextMultiLine.SetNode(self,doc,node,canvas)
        self.sVal=doc.getNodeText(node,'value')
        
        sVal=doc.getNodeText(node,'type')
        if sVal=='in':
            self.type=self.TYPE_IN
        elif sVal=='out':
            self.type=self.TYPE_OUT
        elif sVal=='inout':
            self.type=self.TYPE_INOUT
        elif sVal=='param':
            self.type=self.TYPE_PARAM
        else:
            self.type=self.TYPE_IN
        
        sVal=doc.getNodeText(node,'order')
        try:
            self.order=int(sVal)
        except:
            self.order=-1
        
        sVal=doc.getNodeText(node,'display')
        if sVal=='value':
            self.display=self.DISPLAY_VAL
        elif sVal=='name':
            self.display=self.DISPLAY_NAME
        elif sVal=='both':
            self.display=self.DISPLAY_BOTH
        else:
            self.display=self.DISPLAY_VAL
        self.__setText__()
        self.__setLine__()
    def GetNode(self,doc,parNode,canvas):
        node=vtDrawCanvasText.GetNode(self,doc,parNode,canvas)
        doc.setNodeText(node,'value',self.sVal)
        
        if self.type==self.TYPE_IN:
            doc.setNodeText(node,'type','in')
        elif self.type==self.TYPE_OUT:
            doc.setNodeText(node,'type','out')
        elif self.type==self.TYPE_INOUT:
            doc.setNodeText(node,'type','inout')
        elif self.type==self.TYPE_PARAM:
            doc.setNodeText(node,'type','param')
        doc.setNodeText(node,'order',str(self.order))
        
        if self.display==self.DISPLAY_VAL:
            doc.setNodeText(node,'display','value')
        elif self.display==self.DISPLAY_NAME:
            doc.setNodeText(node,'display','name')
        elif self.display==self.DISPLAY_BOTH:
            doc.setNodeText(node,'display','both')
        else:
            doc.setNodeText(node,'display','value')
        return node
    def __str__(self):
        return 'blockIO name:%s type:%d order:%d (%d,%d)'%(self.name,self.type,self.order,self.iX,self.iY)
    def __draw__(self,canvas,aX=0,aY=0,aW=-1,aH=-1,iDriver=-1):
        vtDrawCanvasTextMultiLine.__draw__(self,canvas,aX,aY,aW,aH,iDriver)
        self.line.__draw__(canvas,aX,aY,aW,aH,iDriver)
    
