#----------------------------------------------------------------------------
# Name:         vtDrawCanvasTextSelectable.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060221
# CVS-ID:       $Id: vtDrawCanvasTextSelectable.py,v 1.3 2006/06/12 09:35:01 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vtDrawCanvasObjectBase import *
from vidarc.tool.draw.vtDrawCanvasGroup import *
from vidarc.tool.draw.vtDrawCanvasRectangle import *
from vidarc.tool.draw.vtDrawCanvasRectangleFilled import *
from vidarc.tool.draw.vtDrawCanvasText import *

class vtDrawCanvasTextSelectable(vtDrawCanvasGroup):
    def __init__(self,id=-1,name='',text='',x=0,y=0,w=0,h=0,layer=0):
        vtDrawCanvasGroup.__init__(self,id,name,x,y,w,h,layer)
        obj=vtDrawCanvasRectangle(id=-1,name=name+'_rct',
                        x=0,y=0,w=w,h=h,layer=layer)
        self.Add(obj)
        obj=vtDrawCanvasRectangleFilled(id=-1,name=name+'_rcf',
                        x=0,y=0,w=w,h=h,layer=layer)
        obj.SetVisible(False)
        self.Add(obj)
        obj=vtDrawCanvasText(id=-1,name=name+'_txt',text=text,
                        x=0,y=0,w=w,h=h,layer=layer)
        self.Add(obj)
    def GetAttr(self,idx):
        if idx<6:
            return vtDrawCanvasObjectBase.GetAttr(self,idx)
        return self.objs[2].GetAttr(idx)
    def SetAttr(self,idx,val):
        if idx<6:
            return vtDrawCanvasObjectBase.SetAttr(self,idx,val)
        return self.objs[2].SetAttr(idx,val)
    def SetLayer(self,layer):
        vtDrawCanvasGroup.SetLayer(self,layer)
        for o in self.objs:
            o.SetLayer(layer)
    def __draw__(self,canvas,aX=0,aY=0,aW=-1,aH=-1,iDriver=-1):
        if self.bActive:
            self.objs[1].SetVisible(True)
            #self.objs[1].SetLayer(self.GetLayer())
            self.objs[2].SetLayer(-1)
        else:
            self.objs[1].SetVisible(False)
            self.objs[2].SetLayer(self.GetLayer())
            
        vtDrawCanvasGroup.__draw__(self,canvas,aX,aY,aW,aH,iDriver)
        
        #if self.bActive:
        #    self.objs[1].SetFocus(True)
            #self.objs[1].SetLayer(-1)
        #    self.objs[1].__drawSelect__(canvas,aX,aY,aW,aH,iDriver)
        #    self.objs[1].SetFocus(False)
        #else:
        #    self.objs[0].SetActive(True)
        #    self.objs[0].__drawSelect__(canvas,aX,aY,aW,aH,iDriver)
        self.SetSelected(False)
        if self.bActive:
            iLayer=self.GetLayer()
            self.SetLayer(-1)
        self.__drawSelect__(canvas,aX,aY,aW,aH,iDriver)
        if self.bActive:
            self.SetLayer(iLayer)
        #self.txt.__draw__(canvas,aX,aY,aW,aH,iDriver)
