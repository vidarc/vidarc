#----------------------------------------------------------------------------
# Name:         vtDrawCanvasRectangleFilled.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060116
# CVS-ID:       $Id: vtDrawCanvasRectangleFilled.py,v 1.4 2006/02/22 11:24:58 wal Exp $
# Copyright:    customized project for ALRO Control Systems AG
#               (c) 2005 by VIDARC Automation GmbH
# Licence:      customized project
#----------------------------------------------------------------------------

from vtDrawCanvasRectangle import *
class vtDrawCanvasRectangleFilled(vtDrawCanvasRectangle):
    def GetTypeName(self):
        # replace me
        return 'filled_rectangle'
    def __draw__(self,canvas,aX=0,aY=0,aW=-1,aH=-1,iDriver=-1):
        if self.bVisible==False:
            return
        drvInfo=canvas.getDriverData(iDriver)
        drv=canvas.getDriver(iDriver)
        canvas.setActLayer(self.iLayer,self.bActive,iDriver)
        if drv==self.DRV_WX:
            dc=drvInfo['dc']
            if aW>=0 and aH>=0:
                ax,ay=canvas.calcCoor(aX,aY)
                aw,ah=canvas.calcCoor(aW,aH)
                dc.SetClippingRegion(ax,ay,aw+1,ah+1)
            x,y=canvas.calcCoor(self.iX+aX,self.iY+aY)
            w,h=canvas.calcCoor(self.iWidth,self.iHeight)
            dc.DrawRectangle(x,y,w,h)
            if aW>=0 and aH>=0:
                dc.DestroyClippingRegion()
        elif drv==self.DRV_LATEX:
            if aW>=0 and aH>=0:
                ax,ay=canvas.calcCoorLatex(aX,aY)
                aw,ah=canvas.calcCoorLatex(aW,aH)
            x,y=canvas.calcCoorLatex(self.iX+aX,self.iY+aY)
            w,h=canvas.calcCoorLatex(self.iWidth,self.iHeight)
            y=drvInfo['y_max']-y-h
            c=drvInfo['color']
            strs=drvInfo['strs']
            tup=vtRepLatex.lineFitPict2(x,y,x+w,y)
            strs.append('    \\put(%4.1f,%4.1f){%s\\line(%d,%d){%4.1f}}'%(tup[0],tup[1],c,tup[2],tup[3],tup[4]))
            tup=vtRepLatex.lineFitPict2(x+w,y,x+w,y+h)
            strs.append('    \\put(%4.1f,%4.1f){%s\\line(%d,%d){%4.1f}}'%(tup[0],tup[1],c,tup[2],tup[3],tup[4]))
            tup=vtRepLatex.lineFitPict2(x,y+h,x+w,y+h)
            strs.append('    \\put(%4.1f,%4.1f){%s\\line(%d,%d){%4.1f}}'%(tup[0],tup[1],c,tup[2],tup[3],tup[4]))
            tup=vtRepLatex.lineFitPict2(x,y,x,y+h)
            strs.append('    \\put(%4.1f,%4.1f){%s\\line(%d,%d){%4.1f}}'%(tup[0],tup[1],c,tup[2],tup[3],tup[4]))
            pass
        vtDrawCanvasObjectBase.__drawSelect__(self,canvas,aX=aX,aY=aY,aW=aW,aH=aH,iDriver=iDriver)

    def __drawSelect__(self,canvas,aX=0,aY=0,aW=-1,aH=-1,iDriver=-1):
        if self.bVisible==False:
            return
        if self.bSelected:
            drvInfo=canvas.getDriverData(iDriver)
            drv=canvas.getDriver(iDriver)
            if drv!=self.DRV_WX:
                return
            drvInfo=canvas.getDriverData(iDriver)
            drv=canvas.getDriver(iDriver)
            if canvas.SELECTED_LAYER<0:
                iLayer=self.iLayer
            else:
                iLayer=canvas.SELECTED_LAYER
            iLayer=-1
            canvas.setActLayer(iLayer,self.bActive,iDriver)
            if drv==self.DRV_WX:
                dc=drvInfo['dc']
                if aW>=0 and aH>=0:
                    ax,ay=canvas.calcCoor(aX,aY)
                    aw,ah=canvas.calcCoor(aW,aH)
                    dc.SetClippingRegion(ax,ay,aw+1,ah+1)
                else:
                    ax,ay=canvas.calcCoor(self.iX,self.iY)
                    aw,ah=canvas.calcCoor(self.iWidth,self.iHeight)
                    dc.SetClippingRegion(ax,ay,aw+1,ah+1)
                x,y=canvas.calcCoor(self.iX+aX,self.iY+aY)
                w,h=canvas.calcCoor(self.iWidth,self.iHeight)
                grid=canvas.GetGrid()
                if canvas.SELECTED_WIDTH<0:
                    gW2=grid[0]/2
                else:
                    gW2=canvas.SELECTED_WIDTH
                if canvas.SELECTED_HEIGHT<0:
                    gH2=grid[1]/2
                else:
                    gH2=canvas.SELECTED_HEIGHT
                dc.DrawPolygon([(x,y),(x+gW2,y),(x,y+gH2)])
                #dc.DrawLine(x+w,y,x+w,y+h)
                #dc.DrawLine(x,y+h,x+w,y+h)
                #dc.DrawLine(x,y,x,y+h)
                if aW>=0 and aH>=0:
                    dc.DestroyClippingRegion()
                else:
                    dc.DestroyClippingRegion()
        self.__drawFocus__(canvas,aX=aX,aY=aY,aW=aW,aH=aW,iDriver=iDriver)
    def __drawFocus__(self,canvas,aX=0,aY=0,aW=-1,aH=-1,iDriver=-1):
        if self.bVisible==False:
            return
        if self.bFocus:
            drvInfo=canvas.getDriverData(iDriver)
            drv=canvas.getDriver(iDriver)
            if drv!=self.DRV_WX:
                return
            if canvas.FOCUS_LAYER<0:
                iLayer=self.iLayer
            else:
                iLayer=canvas.FOCUS_LAYER
            iLayer=-1
            canvas.setActLayer(iLayer,self.bActive,iDriver)
            if drv==self.DRV_WX:
                dc=drvInfo['dc']
                #print aX,aY,aW,aH
                if aW>=0 and aH>=0:
                    #print 'clip'
                    ax,ay=canvas.calcCoor(aX,aY)
                    aw,ah=canvas.calcCoor(aW,aH)
                    dc.SetClippingRegion(ax,ay,aw+1,ah+1)
                else:
                    ax,ay=canvas.calcCoor(self.iX,self.iY)
                    aw,ah=canvas.calcCoor(self.iWidth,self.iHeight)
                    dc.SetClippingRegion(ax,ay,aw+1,ah+1)
                x,y=canvas.calcCoor(self.iX+aX,self.iY+aY)
                w,h=canvas.calcCoor(self.iWidth,self.iHeight)
                grid=canvas.GetGrid()
                if canvas.FOCUS_WIDTH<0:
                    gW2=grid[0]/4
                else:
                    gW2=canvas.FOCUS_WIDTH
                if canvas.FOCUS_HEIGHT<0:
                    gH2=grid[1]/4
                else:
                    gH2=canvas.FOCUS_HEIGHT
                dc.DrawPolygon([(x,y),(x+gW2,y),(x,y+gH2)])
                dc.DrawPolygon([(x+w-gW2,y),(x+w,y),(x+w,y+gH2)])
                dc.DrawPolygon([(x+w-gW2,y+h),(x+w,y+h),(x+w,y+h-gH2)])
                dc.DrawPolygon([(x,y+h-gH2),(x+gW2,y+h),(x,y+h)])
                
                #dc.DrawLine(x+w,y,x+w,y+h)
                #dc.DrawLine(x,y+h,x+w,y+h)
                #dc.DrawLine(x,y,x,y+h)
                if aW>=0 and aH>=0:
                    dc.DestroyClippingRegion()
                else:
                    dc.DestroyClippingRegion()
