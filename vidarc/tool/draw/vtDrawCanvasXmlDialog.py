#Boa:Dialog:vtDrawCanvasXmlDialog
#----------------------------------------------------------------------------
# Name:         vtDrawCanvasXmlDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtDrawCanvasXmlDialog.py,v 1.12 2010/04/10 12:32:14 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import wx.lib.masked.numctrl

import sys,traceback,types

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer
from vidarc.tool.xml.vtXmlGrpAttrTreeList import *
from vidarc.tool.draw.vtDrawCanvasXmlTreeList import *
from vidarc.tool.draw.vtDrawCanvasLine import vtDrawCanvasLine
from vidarc.tool.draw.vtDrawCanvasRectangle import vtDrawCanvasRectangle
from vidarc.tool.draw.vtDrawCanvasText import vtDrawCanvasText
from vidarc.tool.draw.vtDrawCanvasTextMultiLine import vtDrawCanvasTextMultiLine
from vidarc.tool.draw.vtDrawCanvasGroup import vtDrawCanvasGroup
from vidarc.tool.draw.vtDrawCanvasXmlGroup import vtDrawCanvasXmlGroup

import images

def create(parent):
    return vtDrawCanvasXmlDialog(parent)

[wxID_VTDRAWCANVASXMLDIALOG, wxID_VTDRAWCANVASXMLDIALOGCBADDLAYER, 
 wxID_VTDRAWCANVASXMLDIALOGCBADDOBJ, wxID_VTDRAWCANVASXMLDIALOGCBAPPLY, 
 wxID_VTDRAWCANVASXMLDIALOGCBDELLAYER, wxID_VTDRAWCANVASXMLDIALOGCBDELOBJ, 
 wxID_VTDRAWCANVASXMLDIALOGCBOK, wxID_VTDRAWCANVASXMLDIALOGCHCFONT, 
 wxID_VTDRAWCANVASXMLDIALOGCHCLAYER, wxID_VTDRAWCANVASXMLDIALOGCHCVAL, 
 wxID_VTDRAWCANVASXMLDIALOGCHKADDED, wxID_VTDRAWCANVASXMLDIALOGCHKAUTO, 
 wxID_VTDRAWCANVASXMLDIALOGCHKENABLE, wxID_VTDRAWCANVASXMLDIALOGCHKROTATE, 
 wxID_VTDRAWCANVASXMLDIALOGLBLFONT, wxID_VTDRAWCANVASXMLDIALOGLBLGRID, 
 wxID_VTDRAWCANVASXMLDIALOGLBLLBLNAME, wxID_VTDRAWCANVASXMLDIALOGLBLMAX, 
 wxID_VTDRAWCANVASXMLDIALOGLBLSCALE, wxID_VTDRAWCANVASXMLDIALOGLBLXMLLEVEL, 
 wxID_VTDRAWCANVASXMLDIALOGLSTADDOBJ, wxID_VTDRAWCANVASXMLDIALOGNBEDIT, 
 wxID_VTDRAWCANVASXMLDIALOGNUMSCALE, wxID_VTDRAWCANVASXMLDIALOGNUMSCALEX, 
 wxID_VTDRAWCANVASXMLDIALOGNUMSCALEY, wxID_VTDRAWCANVASXMLDIALOGPGPROCESS, 
 wxID_VTDRAWCANVASXMLDIALOGPNGEN, wxID_VTDRAWCANVASXMLDIALOGPNLABELS, 
 wxID_VTDRAWCANVASXMLDIALOGPNREP, wxID_VTDRAWCANVASXMLDIALOGPNVAL, 
 wxID_VTDRAWCANVASXMLDIALOGPNXML, wxID_VTDRAWCANVASXMLDIALOGSCALEVIS, 
 wxID_VTDRAWCANVASXMLDIALOGSNBSCALEX, wxID_VTDRAWCANVASXMLDIALOGSNVAL, 
 wxID_VTDRAWCANVASXMLDIALOGSPBSCALEY, wxID_VTDRAWCANVASXMLDIALOGSPGRIDX, 
 wxID_VTDRAWCANVASXMLDIALOGSPGRIDY, wxID_VTDRAWCANVASXMLDIALOGSPMAXX, 
 wxID_VTDRAWCANVASXMLDIALOGSPMAXY, wxID_VTDRAWCANVASXMLDIALOGSPXMLLEVELS, 
 wxID_VTDRAWCANVASXMLDIALOGTXTLBLNAME, wxID_VTDRAWCANVASXMLDIALOGTXTNAME, 
 wxID_VTDRAWCANVASXMLDIALOGTXTVAL, wxID_VTDRAWCANVASXMLDIALOGTXTVALMULTILINE, 
] = [wx.NewId() for _init_ctrls in range(44)]

class vtDrawCanvasXmlDialog(wx.Dialog,vtXmlDomConsumer,vtLog.vtLogOriginWX):
    ADDED_TREE_IDX     = 3
    ADDED_MARKER       = u'X'
    NOT_ADDED_MARKER   = u''
    
    def _init_coll_nbEdit_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.pnGen, select=False,
              text=u'General')
        parent.AddPage(imageId=-1, page=self.pnXml, select=False, text=u'XML')
        parent.AddPage(imageId=-1, page=self.pnVal, select=True,
              text=u'Objects')
        parent.AddPage(imageId=-1, page=self.pnLabels, select=False,
              text=u'Layer')
        parent.AddPage(imageId=-1, page=self.pnRep, select=False,
              text=u'Report')

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTDRAWCANVASXMLDIALOG,
              name=u'vtDrawCanvasXmlDialog', parent=prnt, pos=wx.Point(333,
              170), size=wx.Size(423, 332), style=wx.DEFAULT_DIALOG_STYLE|wx.RESIZE_BORDER,
              title=u'vtDrawCanvasXml Dialog')
        self.SetClientSize(wx.Size(415, 305))
        self.Bind(wx.EVT_ACTIVATE, self.OnVtDrawCanvasXmlDialogActivate)

        self.cbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTDRAWCANVASXMLDIALOGCBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'cbOk',
              parent=self, pos=wx.Point(168, 272), size=wx.Size(76, 30),
              style=0)
        self.cbOk.Bind(wx.EVT_BUTTON, self.OnCbOkButton,
              id=wxID_VTDRAWCANVASXMLDIALOGCBOK)

        self.nbEdit = wx.Notebook(id=wxID_VTDRAWCANVASXMLDIALOGNBEDIT,
              name=u'nbEdit', parent=self, pos=wx.Point(8, 8), size=wx.Size(400,
              256), style=0)

        self.pnGen = wx.Panel(id=wxID_VTDRAWCANVASXMLDIALOGPNGEN, name=u'pnGen',
              parent=self.nbEdit, pos=wx.Point(0, 0), size=wx.Size(392, 230),
              style=wx.TAB_TRAVERSAL)

        self.lblGrid = wx.StaticText(id=wxID_VTDRAWCANVASXMLDIALOGLBLGRID,
              label=u'Grid', name=u'lblGrid', parent=self.pnGen, pos=wx.Point(8,
              14), size=wx.Size(19, 13), style=0)

        self.spGridX = wx.SpinCtrl(id=wxID_VTDRAWCANVASXMLDIALOGSPGRIDX,
              initial=0, max=100, min=0, name=u'spGridX', parent=self.pnGen,
              pos=wx.Point(50, 10), size=wx.Size(50, 21),
              style=wx.SP_ARROW_KEYS)
        self.spGridX.Bind(wx.EVT_TEXT, self.OnSpGridXText,
              id=wxID_VTDRAWCANVASXMLDIALOGSPGRIDX)

        self.spGridY = wx.SpinCtrl(id=wxID_VTDRAWCANVASXMLDIALOGSPGRIDY,
              initial=0, max=100, min=0, name=u'spGridY', parent=self.pnGen,
              pos=wx.Point(110, 10), size=wx.Size(50, 21),
              style=wx.SP_ARROW_KEYS)
        self.spGridY.Bind(wx.EVT_TEXT, self.OnSpGridYText,
              id=wxID_VTDRAWCANVASXMLDIALOGSPGRIDY)

        self.pnVal = wx.Panel(id=wxID_VTDRAWCANVASXMLDIALOGPNVAL, name=u'pnVal',
              parent=self.nbEdit, pos=wx.Point(0, 0), size=wx.Size(392, 230),
              style=wx.TAB_TRAVERSAL)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTDRAWCANVASXMLDIALOGCBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Apply', name=u'cbApply',
              parent=self.pnVal, pos=wx.Point(112, 6), size=wx.Size(65, 30),
              style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VTDRAWCANVASXMLDIALOGCBAPPLY)

        self.lblMax = wx.StaticText(id=wxID_VTDRAWCANVASXMLDIALOGLBLMAX,
              label=u'Max', name=u'lblMax', parent=self.pnGen, pos=wx.Point(8,
              54), size=wx.Size(20, 13), style=0)

        self.spMaxX = wx.SpinCtrl(id=wxID_VTDRAWCANVASXMLDIALOGSPMAXX,
              initial=0, max=1000, min=0, name=u'spMaxX', parent=self.pnGen,
              pos=wx.Point(50, 50), size=wx.Size(50, 21),
              style=wx.SP_ARROW_KEYS)
        self.spMaxX.Bind(wx.EVT_TEXT, self.OnSpMaxXText,
              id=wxID_VTDRAWCANVASXMLDIALOGSPMAXX)

        self.spMaxY = wx.SpinCtrl(id=wxID_VTDRAWCANVASXMLDIALOGSPMAXY,
              initial=0, max=1000, min=0, name=u'spMaxY', parent=self.pnGen,
              pos=wx.Point(110, 50), size=wx.Size(50, 21),
              style=wx.SP_ARROW_KEYS)
        self.spMaxY.Bind(wx.EVT_TEXT, self.OnSpMaxYText,
              id=wxID_VTDRAWCANVASXMLDIALOGSPMAXY)

        self.pnLabels = wx.Panel(id=wxID_VTDRAWCANVASXMLDIALOGPNLABELS,
              name=u'pnLabels', parent=self.nbEdit, pos=wx.Point(0, 0),
              size=wx.Size(392, 230), style=wx.TAB_TRAVERSAL)

        self.chcLayer = wx.Choice(choices=[],
              id=wxID_VTDRAWCANVASXMLDIALOGCHCLAYER, name=u'chcLayer',
              parent=self.pnLabels, pos=wx.Point(40, 8), size=wx.Size(130, 21),
              style=0)
        self.chcLayer.Bind(wx.EVT_CHOICE, self.OnChcLabelChoice,
              id=wxID_VTDRAWCANVASXMLDIALOGCHCLAYER)

        self.txtLblName = wx.TextCtrl(id=wxID_VTDRAWCANVASXMLDIALOGTXTLBLNAME,
              name=u'txtLblName', parent=self.pnLabels, pos=wx.Point(56, 40),
              size=wx.Size(72, 21), style=0, value=u'')

        self.lblLblName = wx.StaticText(id=wxID_VTDRAWCANVASXMLDIALOGLBLLBLNAME,
              label=u'Name', name=u'lblLblName', parent=self.pnLabels,
              pos=wx.Point(8, 44), size=wx.Size(28, 13), style=0)

        self.cbAddLayer = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTDRAWCANVASXMLDIALOGCBADDLAYER,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'cbAddLayer',
              parent=self.pnLabels, pos=wx.Point(200, 8), size=wx.Size(60, 30),
              style=0)
        self.cbAddLayer.Bind(wx.EVT_BUTTON, self.OnCbAddLayerButton,
              id=wxID_VTDRAWCANVASXMLDIALOGCBADDLAYER)

        self.cbDelLayer = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTDRAWCANVASXMLDIALOGCBDELLAYER,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Del', name=u'cbDelLayer',
              parent=self.pnLabels, pos=wx.Point(272, 8), size=wx.Size(60, 30),
              style=0)
        self.cbDelLayer.Bind(wx.EVT_BUTTON, self.OnCbDelLayerButton,
              id=wxID_VTDRAWCANVASXMLDIALOGCBDELLAYER)

        self.pnRep = wx.Panel(id=wxID_VTDRAWCANVASXMLDIALOGPNREP, name=u'pnRep',
              parent=self.nbEdit, pos=wx.Point(0, 0), size=wx.Size(392, 230),
              style=wx.TAB_TRAVERSAL)

        self.lblScale = wx.StaticText(id=wxID_VTDRAWCANVASXMLDIALOGLBLSCALE,
              label=u'Scale', name=u'lblScale', parent=self.pnRep,
              pos=wx.Point(8, 12), size=wx.Size(27, 13), style=0)

        self.numScale = wx.lib.masked.numctrl.NumCtrl(id=wxID_VTDRAWCANVASXMLDIALOGNUMSCALE,
              name=u'numScale', parent=self.pnRep, pos=wx.Point(48, 8),
              size=wx.Size(50, 22), style=0, value=0)
        self.numScale.SetFractionWidth(2)
        self.numScale.SetIntegerWidth(2)

        self.lblFont = wx.StaticText(id=wxID_VTDRAWCANVASXMLDIALOGLBLFONT,
              label=u'Font', name=u'lblFont', parent=self.pnRep, pos=wx.Point(8,
              44), size=wx.Size(21, 13), style=0)

        self.chcFont = wx.Choice(choices=[u'micro', u'smal', u'normal', u'big',
              u'huge'], id=wxID_VTDRAWCANVASXMLDIALOGCHCFONT, name=u'chcFont',
              parent=self.pnRep, pos=wx.Point(48, 40), size=wx.Size(100, 21),
              style=0)
        self.chcFont.SetSelection(2)

        self.chkRotate = wx.CheckBox(id=wxID_VTDRAWCANVASXMLDIALOGCHKROTATE,
              label=u'rotate', name=u'chkRotate', parent=self.pnRep,
              pos=wx.Point(208, 8), size=wx.Size(73, 13), style=0)
        self.chkRotate.SetValue(False)

        self.numScaleX = wx.lib.masked.numctrl.NumCtrl(id=wxID_VTDRAWCANVASXMLDIALOGNUMSCALEX,
              name=u'numScaleX', parent=self.pnGen, pos=wx.Point(50, 90),
              size=wx.Size(50, 22), style=0, value=0)
        self.numScaleX.SetIntegerWidth(2)
        self.numScaleX.SetInsertionPoint(14)
        self.numScaleX.SetFractionWidth(2)

        self.numScaleY = wx.lib.masked.numctrl.NumCtrl(id=wxID_VTDRAWCANVASXMLDIALOGNUMSCALEY,
              name=u'numScaleY', parent=self.pnGen, pos=wx.Point(134, 90),
              size=wx.Size(50, 22), style=0, value=0)
        self.numScaleY.SetIntegerWidth(2)
        self.numScaleY.SetInsertionPoint(14)
        self.numScaleY.SetFractionWidth(2)

        self.spbScaleY = wx.SpinButton(id=wxID_VTDRAWCANVASXMLDIALOGSPBSCALEY,
              name=u'spbScaleY', parent=self.pnGen, pos=wx.Point(186, 90),
              size=wx.Size(16, 24), style=wx.SP_VERTICAL)
        self.spbScaleY.Bind(wx.EVT_SPIN_DOWN, self.OnSpbScaleYSpinDown,
              id=wxID_VTDRAWCANVASXMLDIALOGSPBSCALEY)
        self.spbScaleY.Bind(wx.EVT_SPIN_UP, self.OnSpbScaleYSpinUp,
              id=wxID_VTDRAWCANVASXMLDIALOGSPBSCALEY)

        self.snbScaleX = wx.SpinButton(id=wxID_VTDRAWCANVASXMLDIALOGSNBSCALEX,
              name=u'snbScaleX', parent=self.pnGen, pos=wx.Point(98, 90),
              size=wx.Size(16, 24), style=wx.SP_VERTICAL)
        self.snbScaleX.Bind(wx.EVT_SPIN_DOWN, self.OnSpbScaleXSpinDown,
              id=wxID_VTDRAWCANVASXMLDIALOGSNBSCALEX)
        self.snbScaleX.Bind(wx.EVT_SPIN_UP, self.OnSpbScaleXSpinUp,
              id=wxID_VTDRAWCANVASXMLDIALOGSNBSCALEX)

        self.ScaleVis = wx.StaticText(id=wxID_VTDRAWCANVASXMLDIALOGSCALEVIS,
              label=u'Scale', name=u'ScaleVis', parent=self.pnGen,
              pos=wx.Point(8, 94), size=wx.Size(27, 13), style=0)

        self.txtVal = wx.TextCtrl(id=wxID_VTDRAWCANVASXMLDIALOGTXTVAL,
              name=u'txtVal', parent=self.pnVal, pos=wx.Point(4, 10),
              size=wx.Size(100, 21), style=0, value=u'')
        self.txtVal.Bind(wx.EVT_TEXT, self.OnTxtValText,
              id=wxID_VTDRAWCANVASXMLDIALOGTXTVAL)

        self.snVal = wx.SpinCtrl(id=wxID_VTDRAWCANVASXMLDIALOGSNVAL, initial=0,
              max=100, min=0, name=u'snVal', parent=self.pnVal, pos=wx.Point(4,
              10), size=wx.Size(100, 21), style=wx.SP_ARROW_KEYS)
        self.snVal.Show(False)
        self.snVal.Bind(wx.EVT_TEXT, self.OnSnValText,
              id=wxID_VTDRAWCANVASXMLDIALOGSNVAL)

        self.chkEnable = wx.CheckBox(id=wxID_VTDRAWCANVASXMLDIALOGCHKENABLE,
              label=_(u'visible'), name=u'chkEnable', parent=self.pnVal,
              pos=wx.Point(190, 24), size=wx.Size(73, 13), style=0)
        self.chkEnable.SetValue(False)
        self.chkEnable.Bind(wx.EVT_CHECKBOX, self.OnChkEnableCheckbox,
              id=wxID_VTDRAWCANVASXMLDIALOGCHKENABLE)

        self.chcVal = wx.Choice(choices=[], id=wxID_VTDRAWCANVASXMLDIALOGCHCVAL,
              name=u'chcVal', parent=self.pnVal, pos=wx.Point(4, 10),
              size=wx.Size(100, 21), style=0)
        self.chcVal.Show(False)
        self.chcVal.Bind(wx.EVT_CHOICE, self.OnChcValChoice,
              id=wxID_VTDRAWCANVASXMLDIALOGCHCVAL)

        self.pnXml = wx.Panel(id=wxID_VTDRAWCANVASXMLDIALOGPNXML, name=u'pnXml',
              parent=self.nbEdit, pos=wx.Point(0, 0), size=wx.Size(392, 230),
              style=wx.TAB_TRAVERSAL)

        self.chkAdded = wx.CheckBox(id=wxID_VTDRAWCANVASXMLDIALOGCHKADDED,
              label=u'added', name=u'chkAdded', parent=self.pnXml,
              pos=wx.Point(16, 208), size=wx.Size(73, 13), style=0)
        self.chkAdded.SetValue(False)
        self.chkAdded.Bind(wx.EVT_CHECKBOX, self.OnChkAddedCheckbox,
              id=wxID_VTDRAWCANVASXMLDIALOGCHKADDED)

        self.spXmlLevels = wx.SpinCtrl(id=wxID_VTDRAWCANVASXMLDIALOGSPXMLLEVELS,
              initial=1, max=1000, min=1, name=u'spXmlLevels',
              parent=self.pnGen, pos=wx.Point(106, 150), size=wx.Size(50, 21),
              style=wx.SP_ARROW_KEYS)
        self.spXmlLevels.Bind(wx.EVT_TEXT, self.OnSpXmlMaxLevelText,
              id=wxID_VTDRAWCANVASXMLDIALOGSPXMLLEVELS)

        self.lblXmlLevel = wx.StaticText(id=wxID_VTDRAWCANVASXMLDIALOGLBLXMLLEVEL,
              label=u'XML Level', name=u'lblXmlLevel', parent=self.pnGen,
              pos=wx.Point(8, 154), size=wx.Size(51, 13), style=0)

        self.chkAuto = wx.CheckBox(id=wxID_VTDRAWCANVASXMLDIALOGCHKAUTO,
              label=u'auto', name=u'chkAuto', parent=self.pnVal,
              pos=wx.Point(190, 4), size=wx.Size(82, 13), style=0)
        self.chkAuto.SetValue(True)

        self.pgProcess = wx.Gauge(id=wxID_VTDRAWCANVASXMLDIALOGPGPROCESS,
              name=u'pgProcess', parent=self.pnXml, pos=wx.Point(180, 210),
              range=1000, size=wx.Size(200, 13),
              style=wx.GA_HORIZONTAL|wx.GA_SMOOTH)

        self.lstAddObj = wx.ListCtrl(id=wxID_VTDRAWCANVASXMLDIALOGLSTADDOBJ,
              name=u'lstAddObj', parent=self.pnVal, pos=wx.Point(308, 80),
              size=wx.Size(76, 144), style=wx.LC_SMALL_ICON)
        self.lstAddObj.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstAddObjListItemDeselected,
              id=wxID_VTDRAWCANVASXMLDIALOGLSTADDOBJ)
        self.lstAddObj.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstAddObjListItemSelected,
              id=wxID_VTDRAWCANVASXMLDIALOGLSTADDOBJ)

        self.cbAddObj = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTDRAWCANVASXMLDIALOGCBADDOBJ,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'cbAddObj',
              parent=self.pnVal, pos=wx.Point(312, 40), size=wx.Size(74, 30),
              style=0)
        self.cbAddObj.Bind(wx.EVT_BUTTON, self.OnCbAddObjButton,
              id=wxID_VTDRAWCANVASXMLDIALOGCBADDOBJ)

        self.txtValMultiLine = wx.TextCtrl(id=wxID_VTDRAWCANVASXMLDIALOGTXTVALMULTILINE,
              name=u'txtValMultiLine', parent=self.pnVal, pos=wx.Point(4, 10),
              size=wx.Size(100, 21), style=wx.TE_MULTILINE, value=u'')
        self.txtValMultiLine.Bind(wx.EVT_TEXT, self.OnTxtValMultiLineText,
              id=wxID_VTDRAWCANVASXMLDIALOGTXTVALMULTILINE)

        self.txtName = wx.TextCtrl(id=wxID_VTDRAWCANVASXMLDIALOGTXTNAME,
              name=u'txtName', parent=self.pnVal, pos=wx.Point(286, 10),
              size=wx.Size(100, 21), style=0, value=u'')

        self.cbDelObj = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTDRAWCANVASXMLDIALOGCBDELOBJ,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Del', name=u'cbDelObj',
              parent=self.pnVal, pos=wx.Point(312, 40), size=wx.Size(74, 30),
              style=0)
        self.cbDelObj.Show(False)
        self.cbDelObj.Bind(wx.EVT_BUTTON, self.OnCbDelObjButton,
              id=wxID_VTDRAWCANVASXMLDIALOGCBDELOBJ)

        self._init_coll_nbEdit_Pages(self.nbEdit)

    def __init__(self, parent, TreeListClass=vtXmlGrpAttrTreeList):
        vtXmlDomConsumer.__init__(self)
        self.bNode2Update=False
        self._init_ctrls(parent)
        vtLog.vtLogOriginWX.__init__(self)
        self.doc=None
        self.node=None
        self.nodeDfts=None
        self.objChk=None
        self.bNode2Update=False
        
        self.name=u'vtDrawCanvasXml Dialog'
        img=images.getOkBitmap()
        self.cbOk.SetBitmapLabel(img)
        self.cbApply.SetBitmapLabel(img)
        self.cbAddLayer.SetBitmapLabel(img)
        self.cbAddObj.SetBitmapLabel(img)
        
        #self.cbDown.SetBitmapLabel(images.getGrpDownBitmap())
        
        self.trlstExternal=TreeListClass(parent=self.pnXml,
                id=wx.NewId(), name=u'trlstExternal', 
                pos=wx.Point(4, 4), size=wx.Size(380, 200), style=wx.TR_HAS_BUTTONS,
                cols=[_(u'tag'),_(u'name'),_(u'type'),_(u'added')],
                master=False,verbose=1)
        self.trlstExternal.SetIncludeAttr(False)
        #EVT_VTXMLTREE_ITEM_SELECTED(self.trlstExternal,self.OnTreeItemSel)
        self.trlstExternal.BindEvent('selected',self.OnTreeItemSel)
        wx.EVT_TREE_ITEM_RIGHT_CLICK(self.trlstExternal,self.trlstExternal.GetId(), self.OnTreeRightDown)
        self.trlstExternal.SetColumnWidth(0, 200)
        self.trlstExternal.SetColumnWidth(1, 80)
        self.trlstExternal.SetColumnWidth(2, 40)
        self.trlstExternal.SetColumnWidth(3, 20)
        self.trlstExternal.SetNotifyAddThread(True)
        #EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.trlstExternal,self.OnAddElement)
        #EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.trlstExternal,self.OnAddFinished)
        #self.trlstExternal.BindEvent('thdAddElem',self.OnAddElement)
        #self.trlstExternal.BindEvent('thdAddElemFin',self.OnAddFinished)
        self.trlstExternal.BindEvents(self.OnAddElement,None,self.OnAddFinished)
        #self.trlstExternal.BindEvents('thdAddElemAbort',self.OnAddElement)
        self.selNode=None
        
        img=images.getCancelBitmap()
        self.cbDelObj.SetBitmapLabel(img)
        
        self.tnSel=None
        self.iSelIdx=-1
        self.objChk=None
        self.objBaseChk=None
        self.iSelIdxAddObj=-1
        
        self.canvas=None
        self.lst=[]
        self.numScaleX.SetValue(1.0)
        self.numScaleY.SetValue(1.0)
        
        img=images.getDelBitmap()
        #self.cbDel.SetBitmapLabel(img)
        #self.cbDelName.SetBitmapLabel(img)
        #self.cbDelLbl.SetBitmapLabel(img)
        id=wx.NewId()
        self.trlstInternal=vtDrawCanvasXmlTreeList(parent=self.pnVal,
                id=id, name=u'trlstInternal', 
                pos=wx.Point(4, 42),size=wx.Size(300, 180), style=wx.TR_HAS_BUTTONS)
        wx.EVT_TREE_SEL_CHANGED(self, id,
              self.OnTreeSelChanged)
        
        self.__addObjs2Add__()
        self.chcLayer.SetSelection(0)
        
        self.txtVal.Show(False)
        self.snVal.Show(False)
        self.chcVal.Show(False)
        self.txtValMultiLine.Show(False)
        self.txtName.Show(False)
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        if self.doc is not None:
            self.doc.DelConsumer(self)
        self.doc=None
        self.node=None
    def IsBusy(self):
        #if self.thdAddElements.IsRunning():
        #    return True
        return False
    def IsThreadRunning(self):
        bRunning=False
        #if self.thdAddElements.IsRunning():
        #    bRunning=True
        return bRunning
    def Stop(self):
        self.__logDebug__(''%())
        #if self.thdAddElements.IsRunning():
        #    self.thdAddElements.Stop()
        #while self.IsThreadRunning():
        #    time.sleep(1.0)
        pass
    def ClearDoc(self):
        self.ClearInt()
        vtXmlDomConsumer.ClearDoc(self)
    def Clear(self):
        self.__logDebug__(''%())
        self.node=None
        self.nodeDfts=None
        self.ClearInt()
        vtXmlDomConsumer.Clear(self)
    def __Clear__(self):
        if Thread_IsMain()==False:
            self.__logCritical__('called by thread'%())
        self.txtVal.Show(False)
        self.snVal.Show(False)
        self.chcVal.Show(False)
        self.txtValMultiLine.Show(False)
        
        self.chcLayer.Clear()
        #self.trlstExternal.DeleteAllItems()
        self.trlstInternal.DeleteAllItems()
    def ClearInt(self):
        self.__logDebug__(''%())
        self.selNode=None
        self.objChk=None
        
        self.tnSel=None
        self.iSelIdx=-1
        self.objChk=None
        self.objBaseChk=None
        self.__Clear__()
        
    def OnTreeItemSel(self,evt):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            #vtLog.vtLogCallDepth(None,'',verbose=1)
            self.selNode=evt.GetTreeNodeData()
            sVal=self.trlstExternal.GetValue(self.ADDED_TREE_IDX)
            if sVal==self.ADDED_MARKER:
                self.chkAdded.SetValue(True)
            else:
                self.chkAdded.SetValue(False)
        except:
            self.__logTB__()
        evt.Skip()
    def OnTreeRightDown(self,evt):
        try:
            self.__logDebug__(''%())
            x,y=evt.GetPoint()
            xa,ya=self.trlstInternal.GetPosition()
            x+=xa
            y+=ya
            if not hasattr(self,'popupIdGoto'):
                self.popupIdGoto=wx.NewId()
                self.popupIdGotoAttr=wx.NewId()
                self.popupIdGotoXml=wx.NewId()
                self.popupIdGotoGrp=wx.NewId()
                self.popupIdSetDft=wx.NewId()
                wx.EVT_MENU(self,self.popupIdSetDft,self.OnTreeSetDft)
                wx.EVT_MENU(self,self.popupIdGoto,self.OnTreeGoto)
                wx.EVT_MENU(self,self.popupIdGotoAttr,self.OnTreeGotoAttr)
                wx.EVT_MENU(self,self.popupIdGotoXml,self.OnTreeGotoXml)
                wx.EVT_MENU(self,self.popupIdGotoGrp,self.OnTreeGotoGrp)
            menu=wx.Menu()
            #menu.Append(self.popupIdAddUsr,'Add User')
            id=-1
            if self.selNode is not None:
                try:
                    id=long(self.doc.getKey(self.selNode))
                except:
                    pass
            if id>=0:
                menu.Append(self.popupIdSetDft,'set as default')
                menu.AppendSeparator()
            menu.Append(self.popupIdGoto,'Go to')
            menu.Append(self.popupIdGotoAttr,'  ... attribute')
            if id>=0:
                menu.Append(self.popupIdGotoXml,'  ... XML')
                menu.Append(self.popupIdGotoGrp,'  ... group')
            
            self.PopupMenu(menu,wx.Point(x, y))
            menu.Destroy()
        except:
            self.__logTB__()
        evt.Skip()
    def __getKeyBySel__(self):
        try:
            id=long(self.doc.getKey(self.selNode))
            return id
        except:
            id=long(self.doc.getKey(self.doc.getParent(self.selNode)))
            return id
        return -1
    def __getIntTreeItemBySel__(self):
        if self.selNode is not None:
            try:
                id=self.__getKeyBySel__()
                if id<0:
                    return None
                ti=self.trlstInternal.FindTreeItemByID(id)
                try:
                    id=long(self.doc.getKey(self.selNode))
                except:
                    # set down
                    tic=self.trlstInternal.GetTreeItemXml(ti)
                    if tic is not None:
                        ticc=self.trlstInternal.GetChildTreeItem(tic,self.doc.getTagName(self.selNode))
                        if ticc is not None:
                            return ticc
                return ti
            except:
                pass
        return None
    def OnTreeGoto(self,evt):
        try:
            self.__logDebug__(''%())
            ti=self.__getIntTreeItemBySel__()
            if ti is not None:
                self.trlstInternal.SelectItem(ti)
                self.trlstInternal.EnsureVisible(ti)
                self.trlstInternal.SetFocus()
                self.nbEdit.SetSelection(2)
        except:
            self.__logTB__()
        evt.Skip()
    def OnTreeGotoAttr(self,evt):
        try:
            self.__logDebug__(''%())
            ti=self.__getIntTreeItemBySel__()
            ti=self.trlstInternal.GetTreeItemAttr(ti)
            if ti is not None:
                self.trlstInternal.SelectItem(ti)
                self.trlstInternal.EnsureVisible(ti)
                self.trlstInternal.Expand(ti)
                self.trlstInternal.SetFocus()
                self.nbEdit.SetSelection(2)
        except:
            self.__logTB__()
        evt.Skip()
    def OnTreeGotoXml(self,evt):
        try:
            self.__logDebug__(''%())
            ti=self.__getIntTreeItemBySel__()
            ti=self.trlstInternal.GetTreeItemXml(ti)
            if ti is not None:
                self.trlstInternal.SelectItem(ti)
                self.trlstInternal.EnsureVisible(ti)
                self.trlstInternal.Expand(ti)
                self.trlstInternal.SetFocus()
                self.nbEdit.SetSelection(2)
        except:
            self.__logTB__()
        evt.Skip()
    def OnTreeGotoGrp(self,evt):
        try:
            self.__logDebug__(''%())
            ti=self.__getIntTreeItemBySel__()
            ti=self.trlstInternal.GetTreeItemGrp(ti)
            if ti is not None:
                self.trlstInternal.SelectItem(ti)
                self.trlstInternal.EnsureVisible(ti)
                self.trlstInternal.Expand(ti)
        except:
            self.__logTB__()
        evt.Skip()
    def __storeDfts__(self,node,o):
        #vtLog.CallStack('')
        if self.nodeDfts is None:
            return
        if node is None:
            return
        if o is None:
            return
        tagName=self.doc.getTagName(self.selNode)
        tmpNode=self.doc.getChildForced(self.nodeDfts,tagName)
        for c in self.doc.getChilds(tmpNode):
            self.doc.delNode(c)
        self.doc.setNodeText(tmpNode,'defaults',o.GetDefaults())
        objsXml=o.GetObjectsXml()
        tmp=self.doc.getChildForced(tmpNode,'attributes')
        for it in objsXml:
            tagName=it.name
            self.doc.setNodeText(tmp,tagName,it.GetDefaults())
            #self.doc.setNodeText(c,'type','')
        self.doc.AlignNode(self.nodeDfts,iRec=3)
    def __getNodeDfts__(self,tagName):
        if self.nodeDfts is None:
            return None
        return self.doc.getChild(self.nodeDfts,tagName)
    def __getNodeObjDfts__(self,node):
        if node is None:
            return 0,''
        return 1,self.doc.getNodeText(node,'defaults')
    def __getAttrDfts__(self,node,tagName):
        if node is None:
            return 0,''
        #tmp=self.doc.getChildByLst(node,['attributes',tagName])
        #if tmp is None:
        #    return 0,''
        #return 1,self.doc.getText(tmp)
        return 1,self.doc.getText(node)
    def OnTreeSetDft(self,evt):
        try:
            self.__logDebug__(''%())
            if self.selNode is not None:
                try:
                    id=long(self.doc.getKey(self.selNode))
                    if id<0:
                        return None
                    ti=self.trlstInternal.FindTreeItemByID(id)
                    if ti is not None:
                        o=self.trlstInternal.GetPyData(ti)
                        print o
                        self.__storeDfts__(self.selNode,o)
                except:
                    self.__logTB__()
        except:
            self.__logTB__()
        evt.Skip()
    def OnAddElement(self,evt):
        try:
            self.__logDebug__(''%())
            #if evt.GetCount()>0:
            #    self.pgProcess.SetRange(evt.GetCount())
            #self.pgProcess.SetValue(evt.GetValue())
            self.pgProcess.SetValue(evt.GetNormalized())
        except:
            self.__logTB__()
    def OnAddFinished(self,evt):
        try:
            if VERBOSE>0:
                if self.canvas is not None:
                    self.__logDebug__('lst:%s canvas:%s'%(self.__logFmt__(self.lst),
                            self.__logFmt__(self.canvas.objs)))
                else:
                    self.__logDebug__('lst:%s canvas:None'%(self.__logFmt__(self.lst)))
            if self.trlstExternal.IsScheduled()==False:
                self.checkObjects()
                self.trlstExternal.Enable(True)
        except:
            self.__logTB__()
        evt.Skip()
    def SetName(self,name):
        self.name=name
    def SetReportValue(self,scale,fontsize,rotate):
        self.numScale.SetValue(scale)
        self.chcFont.SetSelection(fontsize)
        self.chkRotate.SetValue(rotate)
    def SetDoc(self,doc,bNet=False):
        vtXmlDomConsumer.SetDoc(self,doc,bNet)
        self.node=None
        self.nodeDfts=None
        self.trlstExternal.SetDoc(doc,bNet)
    def SetNode(self,node):
        self.__logDebug__('SetNode,node:%s'%(self.doc.getKey(node)))
        vtXmlDomConsumer.SetNode(self,node)
        if self.IsShown():
            self.trlstExternal.Enable(False)
            self.trlstExternal.SetNode(node)
        else:
            self.bNode2Update=True
    def SetNodeDfts(self,node):
        self.__logDebug__('SetNodeDfts,node:%s'%(self.doc.getKey(node)))
        self.nodeDfts=node
    def SetCanvas(self,canvas):
        self.canvas=canvas
    def SetValue(self,**kwargs):
        try:
            self.tnSel=None
            self.iSelIdx=-1
            self.objChk=None
            self.objBaseChk=None
            self.lst=[]
            if kwargs.has_key('grid'):
                self.grid=kwargs['grid']
                self.spGridX.SetValue(self.grid[0])
                self.spGridY.SetValue(self.grid[1])
            if kwargs.has_key('max'):
                self.max=kwargs['max']
                self.spMaxX.SetValue(self.max[0])
                self.spMaxY.SetValue(self.max[1])
            if kwargs.has_key('objects'):
                self.lst=kwargs['objects']
            if kwargs.has_key('canvas'):
                self.canvas=kwargs['canvas']
            if kwargs.has_key('xml_level'):
                self.iXmlLevel=kwargs['xml_level']
                self.spXmlLevels.SetValue(self.iXmlLevel)
                self.trlstExternal.SetLevelLimit(self.iXmlLevel,-1)
            if kwargs.has_key('report_scale'):
                self.numScale.SetValue(kwargs['report_scale'])
            if kwargs.has_key('report_font'):
                self.chcFont.SetSelection(kwargs['report_font'])
            if kwargs.has_key('report_rotate'):
                self.chkRotate.SetValue(kwargs['report_rotate'])
            
            self.trlstInternal.SetObjects(self.lst)
            
            self.__updateLst__()
        except:
            self.__logTB__()
    def GetValue(self,name):
        if name=='objects':
            return self.lst
        elif name=='max':
            return self.max
        elif name=='grid':
            return self.grid
        elif name=='xml_level':
            return self.iXmlLevel
        elif name=='report_scale':
            return self.numScale.GetValue()
        elif name=='report_font':
            return self.chcFont.GetSelection()
        elif name=='report_rotate':
            return self.chkRotate.GetValue()
        elif name=='xml_level':
            return self.iXmlLevel
    
    def __addObjs2Add__(self):
        self.imgLstTyp=wx.ImageList(16,16)
        imgLst=[]
        img=images.getDrawLineBitmap()
        idx=self.imgLstTyp.Add(img)
        imgLst.append(idx)
        
        img=images.getDrawTextBitmap()
        idx=self.imgLstTyp.Add(img)
        imgLst.append(idx)
        
        img=images.getDrawMultiTextBitmap()
        idx=self.imgLstTyp.Add(img)
        imgLst.append(idx)
        
        img=images.getDrawRectBitmap()
        idx=self.imgLstTyp.Add(img)
        imgLst.append(idx)
        
        img=images.getDrawGrpBitmap()
        idx=self.imgLstTyp.Add(img)
        imgLst.append(idx)
        self.lstAddObj.AssignImageList(self.imgLstTyp,wx.IMAGE_LIST_SMALL)
        
        self.lstAddObj.InsertImageStringItem(sys.maxint,'line',imgLst[0])
        self.lstAddObj.InsertImageStringItem(sys.maxint,'text',imgLst[1])
        self.lstAddObj.InsertImageStringItem(sys.maxint,'multiline',imgLst[2])
        self.lstAddObj.InsertImageStringItem(sys.maxint,'rectangle',imgLst[3])
        self.lstAddObj.InsertImageStringItem(sys.maxint,'group',imgLst[4])
        self.cbAddObj.Enable(False)
        pass
    def __updateLst__(self):
        self.iSelObjIdx=-1
        self.objChk=None
        self.txtVal.Show(False)
        self.snVal.Show(False)
        self.chcVal.Show(False)
        self.txtValMultiLine.Show(False)
        
        self.chcLayer.Clear()
        
    def __updateLayer__(self):
        i=self.chcLayer.GetSelection()
        #self.lstLabels.DeleteAllItems()
        
        if self.labels==None:
            self.labels=[[],[]]
        try:
            lst=self.labels[i]
            def cmpFunc(a,b):
                return int(a[0]-b[0])
            lst.sort(cmpFunc)
            for it in lst:
                idx=self.lstLabels.InsertImageStringItem(sys.maxint,'%4.2f'%it[0],-1)
                self.lstLabels.SetStringItem(idx,1,it[1],-1)
        except:
            pass
    def OnCbOkButton(self, event):
        try:
            self.canvas.ShowAll()
            self.Show(False)
        except:
            self.__logTB__()
        event.Skip()
    def __getObjChk__(self,tn):
        try:
            while 1==1:
                tn=self.trlstInternal.GetItemParent(tn)
                if tn is None:
                    return None
                tnd=self.trlstInternal.GetItemData(tn)
                if tnd is not None:
                    o=tnd.GetData()
                    if o is not None:
                        if isinstance(o,vtDrawCanvasXmlGroup):
                            return o
                        if isinstance(o,vtDrawCanvasGroup):
                            return o
        except:
            pass
        return None
    def OnTreeSelChanged(self,event):
        try:
            tn=event.GetItem()
            self.tnSel=tn
            self.iSelIdx=-1
            self.objChk=None
            self.objBaseChk=None
            obj=None
            if tn is not None:
                try:
                    tnd=self.trlstInternal.GetItemData(tn)
                    o=tnd.GetData()
                    if o is not None:
                        if type(o)==types.TupleType:
                            self.iSelIdx=o[1]
                            obj=o[0]
                            self.objBaseChk=self.__getObjChk__(tn)
                            self.objChk=obj
                        else:
                            obj=o
                            self.objChk=obj
                            self.objBaseChk=self.__getObjChk__(tn)
                        
                except:
                    traceback.print_exc()
                    pass
            self.txtVal.Show(False)
            self.snVal.Show(False)
            self.chcVal.Show(False)
            self.txtValMultiLine.Show(False)
            
            if self.objChk is not None:
                self.chkEnable.SetValue(self.objChk.GetVisible())
            else:
                self.chkEnable.SetValue(True)
            if obj is not None:
                sName,sVal,sFmt,sTyp=obj.GetAttr(self.iSelIdx)
                if sTyp==-1:
                    pass
                elif sTyp==0:
                    self.txtVal.SetValue(sVal)
                    self.txtVal.Show(True)
                elif sTyp==1:
                    self.snVal.SetValue(sVal)
                    self.snVal.Show(True)
                    self.snVal.SetFocus()
                elif sTyp==2:
                    self.chcVal.Clear()
                    for s in obj.GetAttrEntries(self.iSelIdx):
                        self.chcVal.Append(s)
                    self.chcVal.SetSelection(obj.GetAttrEntrySel(self.iSelIdx))
                    self.chcVal.Show(True)
                elif sTyp==3:
                    self.txtValMultiLine.SetValue(sVal)
                    self.txtValMultiLine.Show(True)
            self.__checkAddObj__()
        except:
            self.__logTB__()
        event.Skip()
    def OnSpbScaleXSpinDown(self, event):
        try:
            fVal=self.numScaleX.GetValue()
            fVal-=0.1
            self.numScaleX.SetValue(fVal)
        except:
            self.__logTB__()
        event.Skip()
    def OnSpbScaleXSpinUp(self, event):
        try:
            fVal=self.numScaleX.GetValue()
            fVal+=0.1
            self.numScaleX.SetValue(fVal)
        except:
            self.__logTB__()
        event.Skip()
    def OnSpbScaleYSpinDown(self, event):
        try:
            fVal=self.numScaleY.GetValue()
            fVal-=0.1
            self.numScaleY.SetValue(fVal)
        except:
            self.__logTB__()
        event.Skip()
    def OnSpbScaleYSpinUp(self, event):
        try:
            fVal=self.numScaleY.GetValue()
            fVal+=0.1
            self.numScaleY.SetValue(fVal)
        except:
            self.__logTB__()
        event.Skip()
    def __getBaseObj__(self,tn):
        try:
            if tn is None:
                return None
            objPar=None
            tp=self.trlstInternal.GetItemParent(tn)
            if tp is not None:
                try:
                    td=self.trlstInternal.GetPyData(tp)
                    if td is not None:
                        if isinstance(td,vtDrawCanvasGroup):
                            objPar=td
                        if isinstance(td,vtDrawCanvasXmlGroup):
                            objPar=td
                    tmp=self.__getBaseObj__(tp)
                    if tmp is None:
                        return objPar
                    else:
                        return tmp
                except:
                    pass
            return None
        except:
            self.__logTB__()
        return None
    def OnCbApplyButton(self, event):
        try:
            #vtLog.CallStack('')
            if self.objChk is None:
                return
            obj=self.objChk
            obj.SetVisible(False)
            objBase=self.__getBaseObj__(self.tnSel)
            if objBase is None:
                objBase=obj
            self.canvas.ShowObj(objBase,False)
            
            sName,sVal,sFmt,sTyp=obj.GetAttr(self.iSelIdx)
            if sTyp==-1:
                return
            elif sTyp==0:
                val=self.txtVal.GetValue()
            elif sTyp==1:
                val=self.snVal.GetValue()
            elif sTyp==2:
                val=self.chcVal.GetSelection()
            elif sTyp==3:
                val=self.txtValMultiLine.GetValue()
                
            obj.SetAttr(self.iSelIdx,val)
            def recalc(tn):
                if tn is None:
                    return None
                objPar=None
                tp=self.trlstInternal.GetItemParent(tn)
                if tp is not None:
                    try:
                        td=self.trlstInternal.GetPyData(tp)
                        if td is not None:
                            if isinstance(td,vtDrawCanvasGroup):
                                self.canvas.Recalc(None,td,None)
                                objPar=td
                            if isinstance(td,vtDrawCanvasXmlGroup):
                                self.canvas.Recalc(None,td,None)
                                objPar=td
                        tmp=recalc(tp)
                        if tmp is None:
                            return objPar
                        else:
                            return tmp
                    except:
                        pass
                return None
            tmp=recalc(self.tnSel)
            sName,val,sFmt,sTyp=obj.GetAttr(self.iSelIdx)
            if self.iSelIdx>=0:
                self.trlstInternal.SetItemText(self.tnSel,sFmt%val,1)
            obj.SetVisible(self.chkEnable.GetValue())
            self.canvas.ShowObj(objBase,True)
        except:
            self.__logTB__()
    def OnCbDelButton(self, event):
        return
        iSel=self.chcName.GetSelection()
        dict=self.lst[iSel]
        vals=dict['values']
        if self.iSelIdx>=0:
            dict['values']=vals[:self.iSelIdx]+vals[self.iSelIdx+1:]
            self.lstVal.DeleteItem(self.iSelIdx)
            self.iSelIdx=-1
        event.Skip()
    def OnSpMaxXText(self, event):
        try:
            self.max=(self.spMaxX.GetValue(),self.max[1])
        except:
            self.__logTB__()
        event.Skip()
    def OnSpMaxYText(self, event):
        try:
            self.max=(self.max[0],self.spMaxY.GetValue())
        except:
            self.__logTB__()
        event.Skip()
    def OnSpXmlMaxLevelText(self, event):
        try:
            self.iXmlLevel=self.spXmlLevels.GetValue()
            self.trlstExternal.SetLevelLimit(self.iXmlLevel,-1)
        except:
            self.__logTB__()
        event.Skip()
    def OnLstLabelsListItemDeselected(self, event):
        self.iSelLblIdx=-1
        event.Skip()

    def OnLstLabelsListItemSelected(self, event):
        try:
            self.iSelLblIdx=event.GetIndex()
            idx=self.iSelLblIdx
            it=self.lstLabels.GetItem(idx,0)
            sPos=it.m_text
            it=self.lstLabels.GetItem(idx,1)
            sName=it.m_text
            self.txtLblName.SetValue(sName)
            self.numPos.SetValue(sPos)
        except:
            self.__logTB__()
        event.Skip()
    def OnCbAddLayerButton(self, event):
        try:
            i=self.chcLabel.GetSelection()
            tup=(self.numPos.GetValue(),self.txtLblName.GetValue())
            if self.iSelLblIdx>=0:
                lbls=self.labels[i]
                self.labels[i]=lbls[:self.iSelLblIdx]+[tup]+lbls[self.iSelLblIdx+1:]
                self.lstLabels.SetStringItem(self.iSelLblIdx,0,'%4.2f'%tup[0],-1)
                self.lstLabels.SetStringItem(self.iSelLblIdx,1,tup[1],-1)
            else:
                self.labels[i].append(tup)
                idx=self.lstLabels.InsertImageStringItem(sys.maxint,'%4.2f'%tup[0],-1)
                self.lstLabels.SetStringItem(idx,1,tup[1],-1)
        except:
            self.__logTB__()
        event.Skip()
    def OnCbDelLayerButton(self, event):
        try:
            i=self.chcLabel.GetSelection()
            if self.iSelLblIdx>=0:
                lbls=self.labels[i]
                self.labels[i]=lbls[:self.iSelLblIdx]+lbls[self.iSelLblIdx+1:]
                self.lstLabels.DeleteItem(self.iSelLblIdx)
                self.iSelLblIdx=-1
        except:
            self.__logTB__()
        event.Skip()
    def OnSpRngMaxText(self, event):
        #iSel=self.chcName.GetSelection()
        #dict=self.lst[iSel]
        #dict['range']=(self.spRngMin.GetValue(),self.spRngMax.GetValue())
        event.Skip()
    def OnSpRngMinText(self, event):
        #iSel=self.chcName.GetSelection()
        #dict=self.lst[iSel]
        #dict['range']=(self.spRngMin.GetValue(),self.spRngMax.GetValue())
        event.Skip()
    def OnChcLabelChoice(self, event):
        try:
            self.__updateLabels__()
        except:
            self.__logTB__()
        event.Skip()
    def OnSpGridXText(self, event):
        try:
            self.grid=(self.spGridX.GetValue(),self.grid[1])
        except:
            self.__logTB__()
        event.Skip()
    def OnSpGridYText(self, event):
        try:
            self.grid=(self.grid[0],self.spGridY.GetValue())
        except:
            self.__logTB__()
        event.Skip()
    def __checkAddPossible__(self):
        if self.iSelIdx==-1:
            dlg=wx.MessageDialog(self,_(u'Select list item first!') ,
                        self.name,
                        wx.CANCEL|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.Centre()
            dlg.ShowModal()
            dlg.Destroy()
            return False
        return True
    def OnCbAddSetMinButton(self, event):
        try:
            if self.__checkAddPossible__()==False:
                return
            v=self.numY.GetValue()
            if v!=-1.0:
                self.__add__([v,0.0])
            else:
                self.__add__([0.0])
        except:
            self.__logTB__()
        event.Skip()
    def __add__(self,nValLst):
        i,vals=self.__getSelVal__()
        i=self.__getNextValidTimeEntry__(i,vals)
        if i<0:
            return
        tup=vals[i]
        iOld=i
        for nVal in nValLst:
            n=[round(tup[0]-0.5)+1.0,nVal]
            vals.insert(i+1,n)
            idx=self.lstVal.InsertImageStringItem(i+1, '%d'%(i+1), -1)
            self.lstVal.SetStringItem(idx,1,'%6.2f'%n[0],-1)
            self.lstVal.SetStringItem(idx,2,'%6.2f'%n[1],-1)
            i+=1
        
        for idx in range(i+1,len(vals)):
            self.lstVal.SetStringItem(idx,0,'%d'%idx,-1)
            
        self.__manipulateVals__(1.0,i+1)
        self.lstVal.SetItemState(iOld, 0 ,wx.LIST_STATE_SELECTED )
        self.lstVal.SetItemState(i, wx.LIST_STATE_SELECTED ,wx.LIST_STATE_SELECTED )
    def OnCbAddSetMaxButton(self, event):
        try:
            if self.__checkAddPossible__()==False:
                return
            v=self.numY.GetValue()
            if v!=-1.0:
                self.__add__([v,1.0])
            else:
                self.__add__([1.0])
        except:
            self.__logTB__()
        event.Skip()
    def OnCbAddSetUndefButton(self, event):
        try:
            if self.__checkAddPossible__()==False:
                return
            v=self.numY.GetValue()
            if v!=-1.0:
                self.__add__([v,-1.0])
            else:
                self.__add__([-1.0])
        except:
            self.__logTB__()
        event.Skip()
    def OnCbAddToggleButton(self, event):
        try:
            if self.__checkAddPossible__()==False:
                return
            v=self.numY.GetValue()
            if v==0.0:
                self.__add__([0.0,1.0])
            elif v==1.0:
                self.__add__([1.0,0.0])
            elif v==-1.0:
                self.__add__([-1.0])
            else:
                self.__add__([v])
        except:
            self.__logTB__()
        event.Skip()
    def OnCbTruncXButton(self, event):
        try:
            fVal=self.numX.GetValue()
            self.numX.SetValue(round(fVal-0.5))
        except:
            self.__logTB__()
        event.Skip()
    def OnCbMoveDownButton(self, event):
        try:
            if self.__checkAddPossible__()==False:
                return
            self.__manipulateVals__(1.0)
        except:
            self.__logTB__()
        event.Skip()
    def OnCbMoveUpButton(self, event):
        try:
            if self.__checkAddPossible__()==False:
                return
            self.__manipulateVals__(-1.0)
        except:
            self.__logTB__()
        event.Skip()
    def __getNextValidTimeEntry__(self,idx,vals):
        bFound=False
        i=idx
        iLen=len(vals)
        while bFound==False:
            if i>0:
                if i>=iLen:
                    return -1
                v=vals[i][0]
                if v>=0:
                    return i
            else:
                return -1
            i+=1
            
    def __manipulateVals__(self,dif,idx=-1):
        i,vals=self.__getSelVal__()
        if idx==-1:
            idx=i
        bFound=False
        i=idx
        while bFound==False:
            i-=1
            if i>0:
                v=vals[i][0]
                if v>=0:
                    bFound=True
            else:
                v=0.0
                bFound=True
        bPossible=True
        for i in range(idx,len(vals)):
            if vals[i][0]<0:
                continue
            nv=vals[i][0]+dif
            if nv<v:
                bPossible=False
                break
            v=nv
        if bPossible:
            for i in range(idx,len(vals)):
                if vals[i][0]<0:
                    continue
                vals[i][0]+=dif
                self.lstVal.SetStringItem(i,1,'%6.2f'%vals[i][0],-1)
        
    def __getSelVal__(self):
        #iSel=self.chcName.GetSelection()
        #dict=self.lst[iSel]
        #vals=dict['values']
        #return (self.iSelIdx , vals)
        return (-1,[])
        if 1==0:
            dict['values']=vals[:self.iSelIdx]+[tup]+vals[self.iSelIdx+1:]
            self.lstVal.SetStringItem(self.iSelIdx,1,'%6.2f'%tup[0],-1)
            self.lstVal.SetStringItem(self.iSelIdx,2,'%6.2f'%tup[1],-1)
        else:
            idx=self.lstVal.InsertImageStringItem(sys.maxint, '%d'%len(vals), -1)
            self.lstVal.SetStringItem(idx,1,'%6.2f'%tup[0],-1)
            self.lstVal.SetStringItem(idx,2,'%6.2f'%tup[1],-1)
            vals.append(tup)

    def OnChkAddedCheckbox(self, event):
        try:
            bDbg=False
            if VERBOSE>0:
                if self.__isLogDebug__():
                    bDbg=True
            ti=self.trlstExternal.GetSelectedTreeItem()
            tiData=self.trlstExternal.GetPyData(ti)
            if bDbg:
                self.__logDebug__('sel node==None:%d;tiData:%s'%(self.selNode is not None,self.__logFmt__(tiData)))
            bAdded=self.chkAdded.GetValue()
            if bAdded:
                sVal=self.ADDED_MARKER
            else:
                sVal=self.NOT_ADDED_MARKER
            bUpdate=False
            
            if tiData is None:
                pass
            elif type(tiData)==types.TupleType:
                n=self.doc.getNodeByIdNum(tiData[0])
                if tiData[2] is None:
                    nAttr=self.doc.getChild(n,tiData[1])
                else:
                    nAttr=self.doc.getChildLang(n,tiData[1],tiData[2])
                if bAdded==True:
                    self.canvas.addNodeAttr(n,nAttr)
                else:
                    self.canvas.delNodeAttr(n,nAttr)
                self.trlstExternal.SetValue(self.ADDED_TREE_IDX,sVal)
                bUpdate=True
            else:
                self.trlstExternal.SetValue(self.ADDED_TREE_IDX,sVal)
                n=self.doc.getNodeByIdNum(tiData)
                if bAdded==True:
                        os=self.canvas.checkNode(n,bAdded)
                        if os is not None:
                            bUpdate=True
                        
                        tagName=self.doc.getTagName(n)
                        nodeDfts=self.__getNodeDfts__(tagName)
                        if bDbg:
                            self.__logDebug__('nodeDfts;%s'%(self.__logFmt__(nodeDfts)))
                        
                        obj,lvObj=self.canvas.findNode(n)
                        if obj is not None:
                            iRes,sDfts=self.__getNodeObjDfts__(nodeDfts)
                            obj.SetDefaults(sDfts)
                        if bDbg:
                            self.__logDebug__(''%())
                        lst=self.trlstExternal.GetNodeAttrsTup()
                        if bDbg:
                            self.__logDebug__('lst;%s'%(self.__logFmt__(lst)))
                        for it in lst:
                            if bDbg:
                                self.__logDebug__('it:%s'%(self.__logFmt__(it)))
                            #tagNameTree=self.trlstExternal.GetItemText(it[0])
                            tagNameChild=self.doc.getTagName(it[1])
                            self.trlstExternal.SetTreeItemValue(it[0],self.ADDED_TREE_IDX,sVal)
                            #self.canvas.addNodeAttr(it[1],None)#,sDfts)
                            self.canvas.addNodeAttr(n,it[1])#,sDfts)
                else:
                        os=self.canvas.checkNode(n,bAdded)
                        if os is not None:
                            bUpdate=True
                        lst=self.trlstExternal.GetNodeAttrsTup()
                        if bDbg:
                            self.__logDebug__('lst;%s'%(self.__logFmt__(lst)))
                        for it in lst:
                            if bDbg:
                                self.__logDebug__('it:%s'%(self.__logFmt__(it)))
                            #tagNameTree=self.trlstExternal.GetItemText(it[0])
                            tagNameChild=self.doc.getTagName(it[1])
                            self.trlstExternal.SetTreeItemValue(it[0],self.ADDED_TREE_IDX,sVal)
                            #self.canvas.delNodeAttr(it[1],None)#,sDfts)
                            self.canvas.delNodeAttr(n,it[1])#,sDfts)
                    
                bUpdate=True
            print bUpdate,self.lst
            if bUpdate:
                    self.lst=self.canvas.objs#nObjs
                    self.trlstInternal.SetObjects(self.lst)
                    self.__updateLst__()
                    self.canvas.ShowAll()
            return
            if self.selNode is not None:
                bAdded=self.chkAdded.GetValue()
                if bAdded:
                    sVal=self.ADDED_MARKER
                else:
                    sVal=self.NOT_ADDED_MARKER
                self.trlstExternal.SetValue(self.ADDED_TREE_IDX,sVal)
                id=self.doc.getKey(self.selNode)
                if bDbg:
                    self.__logDebug__('idSel:%s;bAdded:%s'%(id,bAdded))
                if self.doc.IsKeyValid(id):
                    bUpdate=False
                    if bAdded:
                        node=self.selNode
                        idNode=self.doc.getKey(self.node)
                        id=self.doc.getKey(node)
                        lst=[]
                        while id!=idNode:
                            if self.trlstExternal.GetValue(self.ADDED_TREE_IDX,node)!=self.ADDED_MARKER:
                                self.trlstExternal.SetValue(self.ADDED_TREE_IDX,self.ADDED_MARKER,node)
                                lst.append(node)
                                #os=self.canvas.checkNode(node,bAdded)
                                #if os is not None:
                                #    bUpdate=True
                            node=self.doc.getParent(node)
                            id=self.doc.getKey(node)
                        lst.reverse()
                        lst.append(self.selNode)
                        if bDbg:
                            self.__logDebug__('%s'%(self.__logFmt__(lst)))
                        for node in lst:
                            os=self.canvas.checkNode(node,bAdded)
                            if os is not None:
                                bUpdate=True
                        #nObjs=self.canvas.checkNode(self.selNode,bAdded)
                        #if nObjs is not None:
                        #    bUpdate=True
                        if bDbg:
                            self.__logDebug__(''%())
                    else:
                        def __clrSel__(n):
                            lst=self.trlstExternal.GetNodeAttrsTup(n)
                            for it in lst:
                                self.trlstExternal.SetTreeItemValue(it[0],self.ADDED_TREE_IDX,sVal)
                            for c in self.doc.getChildsAttr(n,self.doc.attr):
                                if self.trlstExternal.GetValue(self.ADDED_TREE_IDX,c)==self.ADDED_MARKER:
                                    os=self.canvas.checkNode(c,bAdded)
                                    if os is not None:
                                        bUpdate=True
                                    self.trlstExternal.SetValue(self.ADDED_TREE_IDX,sVal,c)
                                lst=self.trlstExternal.GetNodeAttrsTup(c)
                                for it in lst:
                                    self.trlstExternal.SetTreeItemValue(it[0],self.ADDED_TREE_IDX,sVal)
                                __clrSel__(c)
                            os=self.canvas.checkNode(n,bAdded)
                            if os is not None:
                                bUpdate=True
                        __clrSel__(self.selNode)
                        if bDbg:
                            self.__logDebug__(''%())
                    #ti=self.trlstExternal.Get
                    if bAdded:
                        if bDbg:
                            self.__logDebug__(''%())
                        node=self.selNode
                        id=self.doc.getKey(node)
                        tagName=self.doc.getTagName(node)
                        nodeDfts=self.__getNodeDfts__(tagName)
                        if bDbg:
                            self.__logDebug__('nodeDfts;%s'%(self.__logFmt__(nodeDfts)))
                        
                        obj,lvObj=self.canvas.findNode(self.selNode)
                        if obj is not None:
                            iRes,sDfts=self.__getNodeObjDfts__(nodeDfts)
                            obj.SetDefaults(sDfts)
                        if bDbg:
                            self.__logDebug__(''%())
                        lst=self.trlstExternal.GetNodeAttrsTup()
                        if bDbg:
                            self.__logDebug__('lst;%s'%(self.__logFmt__(lst)))
                        for it in lst:
                            if bDbg:
                                self.__logDebug__('it:%s'%(self.__logFmt__(it)))
                            #tagNameTree=self.trlstExternal.GetItemText(it[0])
                            tagNameChild=self.doc.getTagName(it[1])
                            self.trlstExternal.SetTreeItemValue(it[0],self.ADDED_TREE_IDX,sVal)
                            self.canvas.addNodeAttr(self.selNode,it[1])#,sDfts)
                            
                            #tagNameChild=self.doc.getTagName(it[1])
                            #iRet,sDfts=self.__getAttrDfts__(nodeDfts,tagNameChild)
                            #if iRet>0:
                            #    self.trlstExternal.SetTreeItemValue(it[0],self.ADDED_TREE_IDX,sVal)
                            #    self.canvas.addNodeAttr(self.selNode,it[1],sDfts)
                            ##else:
                            ##    self.canvas.addNodeAttr(self.selNode,it[1])
                    if bDbg:
                        self.__logDebug__(''%())
                else:
                    nodePar=self.doc.getParent(self.selNode)
                    if bAdded:
                        self.canvas.addNodeAttr(nodePar,self.selNode)
                    else:
                        self.canvas.delNodeAttr(nodePar,self.selNode)
                    bUpdate=True
                #if nObjs is not None:
                if bUpdate:
                    self.lst=self.canvas.objs#nObjs
                    self.trlstInternal.SetObjects(self.lst)
                    self.__updateLst__()
                    self.canvas.ShowAll()
        except:
            self.__logTB__()
        if bDbg:
            self.__logDebug__(''%())
        event.Skip()
    def __checkObjects__(self,objs):
        for o in objs:
            id=o.id
            it=self.trlstExternal.__findNodeByID__(None,id)
            if it is not None:
                self.trlstExternal.SetTreeItemValue(it[0],
                                self.ADDED_TREE_IDX,self.ADDED_MARKER)
                lst=self.trlstExternal.GetNodeAttrsTup(it[1])
                for it in lst:
                    def isAttrAdded(name):
                        sName=name
                        for ao in o.objsXml:
                            if ao.name==sName:
                                return True
                        return False
                    if isAttrAdded(self.doc.getTagName(it[1])):
                        self.trlstExternal.SetTreeItemValue(it[0],
                                self.ADDED_TREE_IDX,self.ADDED_MARKER)
            try:
                if len(o.objs)>0:
                    self.__checkObjects__(o.objs)
            except:
                pass
    def checkObjects(self):
        self.trlstExternal.SetTreeItemValueAll(None,
                    self.ADDED_TREE_IDX,self.NOT_ADDED_MARKER)
        if self.canvas is None:
            self.__logDebug__('lst:%s canvas:None'%(self.__logFmt__(self.lst)))
            return
        if VERBOSE>0:
            self.__logDebug__('lst:%s canvas:%s'%(self.__logFmt__(self.lst),
                        self.__logFmt__(self.canvas.objs)))
        self.lst=self.canvas.objs
        self.__checkObjects__(self.lst)
        self.__updateLst__()
    def OnLstAttrsChecklistbox(self, event):
        event.Skip()

    def OnLstAttrsListbox(self, event):
        try:
            idxChk=event.GetSelection()
        except:
            self.__logTB__()
        event.Skip()
    def OnChkEnableCheckbox(self, event):
        try:
            if self.objChk is not None and self.tnSel is not None:
                objBase=self.__getBaseObj__(self.tnSel)
                if objBase is None:
                    objBase=self.objChk
                #objBase=self.__getBaseObj__(self.objChk)
                self.objChk.SetVisible(False)
                self.canvas.ShowObj(objBase,False)
                self.objChk.SetVisible(self.chkEnable.GetValue())
                self.canvas.ShowObj(objBase,True)
        except:
            self.__logTB__()
        event.Skip()
    def OnTxtValText(self, event):
        try:
            if self.chkAuto.GetValue():
                self.OnCbApplyButton(None)
        except:
            self.__logTB__()
        event.Skip()
    def OnSnValText(self, event):
        try:
            if self.chkAuto.GetValue():
                self.OnCbApplyButton(None)
        except:
            self.__logTB__()
        event.Skip()
    def OnChcValChoice(self, event):
        try:
            if self.chkAuto.GetValue():
                self.OnCbApplyButton(None)
        except:
            self.__logTB__()
        event.Skip()
    def OnVtDrawCanvasXmlDialogActivate(self, event):
        try:
            #if vtLog.vtLngIsLogged(vtLog.DEBUG):
            #    vtLog.vtLngCallStack(None,vtLog.DEBUG,
            #        'active,2update:%d|node:%s'%(self.bNode2Update,self.node),
            #        origin=self.name)
            if self.bNode2Update:
                self.trlstExternal.Enable(False)
                self.trlstExternal.SetNode(self.node)
                self.bNode2Update=False
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()

    def OnCbAddObjButton(self, event):
        try:
            if self.iSelIdxAddObj<0:
                event.Skip()
                return
            if self.objChk!=self.objBaseChk:
                try:
                    objsChild=self.objChk.GetObjects()
                    obj=None
                    layer=self.objChk.GetLayer()
                    try:
                        layer=objsChild[-1].GetLayer()
                    except:
                        pass
                    sName=self.txtName.GetValue()
                    if self.iSelIdxAddObj==0:
                        obj=vtDrawCanvasLine(id=-1,name=sName,x=0,y=0,w=1,h=0,layer=layer)
                    elif self.iSelIdxAddObj==1:
                        obj=vtDrawCanvasText(id=-1,name=sName,x=0,y=0,w=1,h=0,layer=layer)
                    elif self.iSelIdxAddObj==2:
                        obj=vtDrawCanvasTextMultiLine(id=-1,name=sName,x=0,y=0,w=1,h=0,layer=layer)
                    elif self.iSelIdxAddObj==3:
                        obj=vtDrawCanvasRectangle(id=-1,name=sName,x=0,y=0,w=1,h=0,layer=layer)
                    elif self.iSelIdxAddObj==4:
                        obj=vtDrawCanvasGroup(id=-1,name=sName,x=0,y=0,w=1,h=0,layer=layer)
                    self.objChk.Add(obj)
                    self.trlstInternal.AddGrpObj(self.tnSel,obj)
                except:
                    self.__logTB__()
        except:
            self.__logTB__()
        event.Skip()

    def OnLstAddObjListItemDeselected(self, event):
        try:
            self.iSelIdxAddObj=-1
            self.txtName.Show(False)
        except:
            self.__logTB__()
        event.Skip()
    def OnLstAddObjListItemSelected(self, event):
        try:
            self.iSelIdxAddObj=event.GetIndex()
            if self.iSelIdxAddObj<0:
                self.txtName.Show(False)
                event.Skip()
                return
            if self.iSelIdxAddObj==0:
                self.txtName.SetValue(u'line')
            elif self.iSelIdxAddObj==1:
                self.txtName.SetValue(u'text')
            elif self.iSelIdxAddObj==2:
                self.txtName.SetValue(u'text')
            elif self.iSelIdxAddObj==3:
                self.txtName.SetValue(u'rect')
            elif self.iSelIdxAddObj==4:
                self.txtName.SetValue(u'group')
            
            self.__checkAddObj__()
            self.txtName.Show(True)
        except:
            self.__logTB__()
        event.Skip()
    def OnTxtValMultiLineText(self, event):
        try:
            self.OnCbApplyButton(None)
        except:
            self.__logTB__()
        event.Skip()
    def __checkAddObj__(self):
        bShowAdd=False
        if self.iSelIdxAddObj>=0:
            if self.objChk!=self.objBaseChk:
                try:
                    objsChild=self.objChk.GetObjects()
                    bShowAdd=True
                except:
                    pass
        self.cbAddObj.Enable(bShowAdd)
        if self.objChk is None:
            bShowAdd=True
        else:
            try:
                objs=self.objChk.GetObjects()
                bShowAdd=True
            except:
                bShowAdd=False
            #if self.objChk.GetId()==-2:
            #    bShowAdd=False
            #else:
            #    bShowAdd=True
        self.cbAddObj.Show(bShowAdd)
        self.cbDelObj.Show(not(bShowAdd))

    def OnCbDelObjButton(self, event):
        try:
            if self.iSelIdx>0:
                return
            if self.objChk is None:
                return
            oPar=self.trlstInternal.GetPyData(self.trlstInternal.GetItemParent(self.tnSel))
            try:
                objs=self.objBaseChk.GetObjects()
            except:
                objs=None
            iRet=self.canvas.Del(self.objChk,objs)
            if iRet>0:
                # object deleted
                objBase=self.__getBaseObj__(self.tnSel)
                if objBase is None:
                    objBase=self.objChk
                #objBase=self.__getBaseObj__(self.objChk)
                self.objChk.SetVisible(False)
                self.canvas.ShowObj(objBase,False)
                
                tnPar=self.trlstInternal.GetItemParent(self.tnSel)
                self.trlstInternal.Delete(self.tnSel)
                self.tnSel=None
                self.objChk=None
                self.objBaseChk=None
                self.__checkAddObj__()
        except:
            self.__logTB__()
        event.Skip()
