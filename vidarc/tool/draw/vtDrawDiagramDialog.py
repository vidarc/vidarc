#Boa:Dialog:vtDrawDiagramDialog
#----------------------------------------------------------------------------
# Name:         vtDrawDiagramDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtDrawDiagramDialog.py,v 1.1 2005/12/11 23:04:48 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import wx.lib.masked.numctrl

import sys,traceback

import vidarc.tool.log.vtLog as vtLog

import images

def create(parent):
    return vtDrawDiagramDialog(parent)

[wxID_VTDRAWDIAGRAMDIALOG, wxID_VTDRAWDIAGRAMDIALOGCBADD, 
 wxID_VTDRAWDIAGRAMDIALOGCBADDLBL, wxID_VTDRAWDIAGRAMDIALOGCBADDNAME, 
 wxID_VTDRAWDIAGRAMDIALOGCBCANCEL, wxID_VTDRAWDIAGRAMDIALOGCBDEL, 
 wxID_VTDRAWDIAGRAMDIALOGCBDELLBL, wxID_VTDRAWDIAGRAMDIALOGCBDELNAME, 
 wxID_VTDRAWDIAGRAMDIALOGCBOK, wxID_VTDRAWDIAGRAMDIALOGCBYPREC0, 
 wxID_VTDRAWDIAGRAMDIALOGCBYPREC100, wxID_VTDRAWDIAGRAMDIALOGCBYPREC50, 
 wxID_VTDRAWDIAGRAMDIALOGCHCLABEL, wxID_VTDRAWDIAGRAMDIALOGCHCNAME, 
 wxID_VTDRAWDIAGRAMDIALOGLBLGRID, wxID_VTDRAWDIAGRAMDIALOGLBLLBLNAME, 
 wxID_VTDRAWDIAGRAMDIALOGLBLLBLPOS, wxID_VTDRAWDIAGRAMDIALOGLBLMAX, 
 wxID_VTDRAWDIAGRAMDIALOGLBLNAME, wxID_VTDRAWDIAGRAMDIALOGLBLRANGE, 
 wxID_VTDRAWDIAGRAMDIALOGLBLX, wxID_VTDRAWDIAGRAMDIALOGLBLY, 
 wxID_VTDRAWDIAGRAMDIALOGLSTLABELS, wxID_VTDRAWDIAGRAMDIALOGLSTVAL, 
 wxID_VTDRAWDIAGRAMDIALOGNBEDIT, wxID_VTDRAWDIAGRAMDIALOGNUMPOS, 
 wxID_VTDRAWDIAGRAMDIALOGNUMX, wxID_VTDRAWDIAGRAMDIALOGNUMY, 
 wxID_VTDRAWDIAGRAMDIALOGPNGEN, wxID_VTDRAWDIAGRAMDIALOGPNLABELS, 
 wxID_VTDRAWDIAGRAMDIALOGPNVAL, wxID_VTDRAWDIAGRAMDIALOGSNBX, 
 wxID_VTDRAWDIAGRAMDIALOGSPBY, wxID_VTDRAWDIAGRAMDIALOGSPGRIDX, 
 wxID_VTDRAWDIAGRAMDIALOGSPGRIDY, wxID_VTDRAWDIAGRAMDIALOGSPMAXX, 
 wxID_VTDRAWDIAGRAMDIALOGSPMAXY, wxID_VTDRAWDIAGRAMDIALOGSPRNGMAX, 
 wxID_VTDRAWDIAGRAMDIALOGSPRNGMIN, wxID_VTDRAWDIAGRAMDIALOGTXTLBLNAME, 
 wxID_VTDRAWDIAGRAMDIALOGTXTNAME, 
] = [wx.NewId() for _init_ctrls in range(41)]

class vtDrawDiagramDialog(wx.Dialog):
    def _init_coll_nbEdit_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.pnGen, select=False,
              text=u'General')
        parent.AddPage(imageId=-1, page=self.pnVal, select=True, text=u'Values')
        parent.AddPage(imageId=-1, page=self.pnLabels, select=False,
              text=u'Labels')

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTDRAWDIAGRAMDIALOG,
              name=u'vtDrawDiagramDialog', parent=prnt, pos=wx.Point(333, 170),
              size=wx.Size(423, 332), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vtDrawDiagram Dialog')
        self.SetClientSize(wx.Size(415, 305))

        self.cbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTDRAWDIAGRAMDIALOGCBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'cbOk',
              parent=self, pos=wx.Point(104, 272), size=wx.Size(76, 30),
              style=0)
        self.cbOk.Bind(wx.EVT_BUTTON, self.OnCbOkButton,
              id=wxID_VTDRAWDIAGRAMDIALOGCBOK)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTDRAWDIAGRAMDIALOGCBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel', name=u'cbCancel',
              parent=self, pos=wx.Point(216, 272), size=wx.Size(76, 30),
              style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VTDRAWDIAGRAMDIALOGCBCANCEL)

        self.nbEdit = wx.Notebook(id=wxID_VTDRAWDIAGRAMDIALOGNBEDIT,
              name=u'nbEdit', parent=self, pos=wx.Point(8, 8), size=wx.Size(400,
              256), style=0)

        self.pnGen = wx.Panel(id=wxID_VTDRAWDIAGRAMDIALOGPNGEN, name=u'pnGen',
              parent=self.nbEdit, pos=wx.Point(0, 0), size=wx.Size(392, 230),
              style=wx.TAB_TRAVERSAL)

        self.lblGrid = wx.StaticText(id=wxID_VTDRAWDIAGRAMDIALOGLBLGRID,
              label=u'Grid', name=u'lblGrid', parent=self.pnGen, pos=wx.Point(8,
              14), size=wx.Size(19, 13), style=0)

        self.spGridX = wx.SpinCtrl(id=wxID_VTDRAWDIAGRAMDIALOGSPGRIDX,
              initial=0, max=100, min=0, name=u'spGridX', parent=self.pnGen,
              pos=wx.Point(50, 10), size=wx.Size(50, 21),
              style=wx.SP_ARROW_KEYS)
        self.spGridX.Bind(wx.EVT_TEXT, self.OnSpGridXText,
              id=wxID_VTDRAWDIAGRAMDIALOGSPGRIDX)

        self.spGridY = wx.SpinCtrl(id=wxID_VTDRAWDIAGRAMDIALOGSPGRIDY,
              initial=0, max=100, min=0, name=u'spGridY', parent=self.pnGen,
              pos=wx.Point(110, 10), size=wx.Size(50, 21),
              style=wx.SP_ARROW_KEYS)
        self.spGridY.Bind(wx.EVT_TEXT, self.OnSpGridYText,
              id=wxID_VTDRAWDIAGRAMDIALOGSPGRIDY)

        self.pnVal = wx.Panel(id=wxID_VTDRAWDIAGRAMDIALOGPNVAL, name=u'pnVal',
              parent=self.nbEdit, pos=wx.Point(0, 0), size=wx.Size(392, 230),
              style=wx.TAB_TRAVERSAL)

        self.lblName = wx.StaticText(id=wxID_VTDRAWDIAGRAMDIALOGLBLNAME,
              label=u'Name', name=u'lblName', parent=self.pnVal, pos=wx.Point(8,
              12), size=wx.Size(28, 13), style=0)

        self.chcName = wx.Choice(choices=[], id=wxID_VTDRAWDIAGRAMDIALOGCHCNAME,
              name=u'chcName', parent=self.pnVal, pos=wx.Point(48, 10),
              size=wx.Size(112, 21), style=0)
        self.chcName.Bind(wx.EVT_CHOICE, self.OnChcNameChoice,
              id=wxID_VTDRAWDIAGRAMDIALOGCHCNAME)

        self.txtName = wx.TextCtrl(id=wxID_VTDRAWDIAGRAMDIALOGTXTNAME,
              name=u'txtName', parent=self.pnVal, pos=wx.Point(164, 10),
              size=wx.Size(80, 21), style=0, value=u'')

        self.cbAddName = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTDRAWDIAGRAMDIALOGCBADDNAME,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'cbAddName',
              parent=self.pnVal, pos=wx.Point(248, 4), size=wx.Size(64, 30),
              style=0)
        self.cbAddName.Bind(wx.EVT_BUTTON, self.OnCbAddNameButton,
              id=wxID_VTDRAWDIAGRAMDIALOGCBADDNAME)

        self.cbDelName = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTDRAWDIAGRAMDIALOGCBDELNAME,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Del', name=u'cbDelName',
              parent=self.pnVal, pos=wx.Point(320, 4), size=wx.Size(65, 30),
              style=0)
        self.cbDelName.Bind(wx.EVT_BUTTON, self.OnCbDelNameButton,
              id=wxID_VTDRAWDIAGRAMDIALOGCBDELNAME)

        self.lblRange = wx.StaticText(id=wxID_VTDRAWDIAGRAMDIALOGLBLRANGE,
              label=u'Range', name=u'lblRange', parent=self.pnVal,
              pos=wx.Point(8, 176), size=wx.Size(32, 13), style=0)

        self.spRngMax = wx.SpinCtrl(id=wxID_VTDRAWDIAGRAMDIALOGSPRNGMAX,
              initial=0, max=100, min=0, name=u'spRngMax', parent=self.pnVal,
              pos=wx.Point(78, 172), size=wx.Size(60, 21),
              style=wx.SP_ARROW_KEYS)
        self.spRngMax.Bind(wx.EVT_TEXT, self.OnSpRngMaxText,
              id=wxID_VTDRAWDIAGRAMDIALOGSPRNGMAX)

        self.spRngMin = wx.SpinCtrl(id=wxID_VTDRAWDIAGRAMDIALOGSPRNGMIN,
              initial=0, max=100, min=0, name=u'spRngMin', parent=self.pnVal,
              pos=wx.Point(78, 198), size=wx.Size(60, 21),
              style=wx.SP_ARROW_KEYS)
        self.spRngMin.Bind(wx.EVT_TEXT, self.OnSpRngMinText,
              id=wxID_VTDRAWDIAGRAMDIALOGSPRNGMIN)

        self.lblX = wx.StaticText(id=wxID_VTDRAWDIAGRAMDIALOGLBLX, label=u'X',
              name=u'lblX', parent=self.pnVal, pos=wx.Point(4, 98),
              size=wx.Size(20, 13), style=0)

        self.lblY = wx.StaticText(id=wxID_VTDRAWDIAGRAMDIALOGLBLY, label=u'Y',
              name=u'lblY', parent=self.pnVal, pos=wx.Point(4, 64),
              size=wx.Size(20, 13), style=0)

        self.numX = wx.lib.masked.numctrl.NumCtrl(id=wxID_VTDRAWDIAGRAMDIALOGNUMX,
              name=u'numX', parent=self.pnVal, pos=wx.Point(32, 94),
              size=wx.Size(71, 22), style=0, value=0)
        self.numX.SetIntegerWidth(4)
        self.numX.SetInsertionPoint(14)
        self.numX.SetFractionWidth(2)

        self.numY = wx.lib.masked.numctrl.NumCtrl(id=wxID_VTDRAWDIAGRAMDIALOGNUMY,
              name=u'numY', parent=self.pnVal, pos=wx.Point(32, 60),
              size=wx.Size(50, 22), style=0, value=0)
        self.numY.SetIntegerWidth(2)
        self.numY.SetInsertionPoint(14)
        self.numY.SetFractionWidth(2)

        self.cbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTDRAWDIAGRAMDIALOGCBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Del', name=u'cbDel',
              parent=self.pnVal, pos=wx.Point(320, 56), size=wx.Size(65, 30),
              style=0)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VTDRAWDIAGRAMDIALOGCBDEL)

        self.cbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTDRAWDIAGRAMDIALOGCBADD,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'cbAdd',
              parent=self.pnVal, pos=wx.Point(248, 56), size=wx.Size(65, 30),
              style=0)
        self.cbAdd.Bind(wx.EVT_BUTTON, self.OnCbAddButton,
              id=wxID_VTDRAWDIAGRAMDIALOGCBADD)

        self.lstVal = wx.ListCtrl(id=wxID_VTDRAWDIAGRAMDIALOGLSTVAL,
              name=u'lstVal', parent=self.pnVal, pos=wx.Point(192, 96),
              size=wx.Size(200, 128), style=wx.LC_REPORT)
        self.lstVal.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstValListItemSelected, id=wxID_VTDRAWDIAGRAMDIALOGLSTVAL)
        self.lstVal.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstValListItemDeselected,
              id=wxID_VTDRAWDIAGRAMDIALOGLSTVAL)

        self.lblMax = wx.StaticText(id=wxID_VTDRAWDIAGRAMDIALOGLBLMAX,
              label=u'Max', name=u'lblMax', parent=self.pnGen, pos=wx.Point(8,
              54), size=wx.Size(20, 13), style=0)

        self.spMaxX = wx.SpinCtrl(id=wxID_VTDRAWDIAGRAMDIALOGSPMAXX, initial=0,
              max=1000, min=0, name=u'spMaxX', parent=self.pnGen,
              pos=wx.Point(50, 50), size=wx.Size(50, 21),
              style=wx.SP_ARROW_KEYS)
        self.spMaxX.Bind(wx.EVT_TEXT, self.OnSpMaxXText,
              id=wxID_VTDRAWDIAGRAMDIALOGSPMAXX)

        self.spMaxY = wx.SpinCtrl(id=wxID_VTDRAWDIAGRAMDIALOGSPMAXY, initial=0,
              max=1000, min=0, name=u'spMaxY', parent=self.pnGen,
              pos=wx.Point(110, 50), size=wx.Size(50, 21),
              style=wx.SP_ARROW_KEYS)
        self.spMaxY.Bind(wx.EVT_TEXT, self.OnSpMaxYText,
              id=wxID_VTDRAWDIAGRAMDIALOGSPMAXY)

        self.pnLabels = wx.Panel(id=wxID_VTDRAWDIAGRAMDIALOGPNLABELS,
              name=u'pnLabels', parent=self.nbEdit, pos=wx.Point(0, 0),
              size=wx.Size(392, 230), style=wx.TAB_TRAVERSAL)

        self.lstLabels = wx.ListCtrl(id=wxID_VTDRAWDIAGRAMDIALOGLSTLABELS,
              name=u'lstLabels', parent=self.pnLabels, pos=wx.Point(8, 40),
              size=wx.Size(184, 100), style=wx.LC_REPORT)
        self.lstLabels.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstLabelsListItemDeselected,
              id=wxID_VTDRAWDIAGRAMDIALOGLSTLABELS)
        self.lstLabels.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstLabelsListItemSelected,
              id=wxID_VTDRAWDIAGRAMDIALOGLSTLABELS)

        self.chcLabel = wx.Choice(choices=[u'x', 'y'],
              id=wxID_VTDRAWDIAGRAMDIALOGCHCLABEL, name=u'chcLabel',
              parent=self.pnLabels, pos=wx.Point(40, 8), size=wx.Size(130, 21),
              style=0)
        self.chcLabel.Bind(wx.EVT_CHOICE, self.OnChcLabelChoice,
              id=wxID_VTDRAWDIAGRAMDIALOGCHCLABEL)

        self.txtLblName = wx.TextCtrl(id=wxID_VTDRAWDIAGRAMDIALOGTXTLBLNAME,
              name=u'txtLblName', parent=self.pnLabels, pos=wx.Point(64, 152),
              size=wx.Size(72, 21), style=0, value=u'')

        self.lblLblName = wx.StaticText(id=wxID_VTDRAWDIAGRAMDIALOGLBLLBLNAME,
              label=u'Label', name=u'lblLblName', parent=self.pnLabels,
              pos=wx.Point(8, 156), size=wx.Size(26, 13), style=0)

        self.lblLblPos = wx.StaticText(id=wxID_VTDRAWDIAGRAMDIALOGLBLLBLPOS,
              label=u'Position', name=u'lblLblPos', parent=self.pnLabels,
              pos=wx.Point(8, 188), size=wx.Size(37, 13), style=0)

        self.numPos = wx.lib.masked.numctrl.NumCtrl(id=wxID_VTDRAWDIAGRAMDIALOGNUMPOS,
              name=u'numPos', parent=self.pnLabels, pos=wx.Point(64, 184),
              size=wx.Size(71, 22), style=0, value=0)
        self.numPos.SetFractionWidth(2)
        self.numPos.SetIntegerWidth(4)

        self.cbAddLbl = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTDRAWDIAGRAMDIALOGCBADDLBL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'cbAddLbl',
              parent=self.pnLabels, pos=wx.Point(208, 168), size=wx.Size(60,
              30), style=0)
        self.cbAddLbl.Bind(wx.EVT_BUTTON, self.OnCbAddLblButton,
              id=wxID_VTDRAWDIAGRAMDIALOGCBADDLBL)

        self.cbDelLbl = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTDRAWDIAGRAMDIALOGCBDELLBL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Del', name=u'cbDelLbl',
              parent=self.pnLabels, pos=wx.Point(208, 64), size=wx.Size(60, 30),
              style=0)
        self.cbDelLbl.Bind(wx.EVT_BUTTON, self.OnCbDelLblButton,
              id=wxID_VTDRAWDIAGRAMDIALOGCBDELLBL)

        self.spbY = wx.SpinButton(id=wxID_VTDRAWDIAGRAMDIALOGSPBY, name=u'spbY',
              parent=self.pnVal, pos=wx.Point(86, 60), size=wx.Size(16, 24),
              style=wx.SP_VERTICAL)
        self.spbY.Bind(wx.EVT_SPIN_DOWN, self.OnSpbYSpinDown,
              id=wxID_VTDRAWDIAGRAMDIALOGSPBY)
        self.spbY.Bind(wx.EVT_SPIN_UP, self.OnSpbYSpinUp,
              id=wxID_VTDRAWDIAGRAMDIALOGSPBY)

        self.snbX = wx.SpinButton(id=wxID_VTDRAWDIAGRAMDIALOGSNBX, name=u'snbX',
              parent=self.pnVal, pos=wx.Point(108, 92), size=wx.Size(16, 24),
              style=wx.SP_VERTICAL)
        self.snbX.Bind(wx.EVT_SPIN_DOWN, self.OnSpbXSpinDown,
              id=wxID_VTDRAWDIAGRAMDIALOGSNBX)
        self.snbX.Bind(wx.EVT_SPIN_UP, self.OnSpbXSpinUp,
              id=wxID_VTDRAWDIAGRAMDIALOGSNBX)

        self.cbYprec0 = wx.Button(id=wxID_VTDRAWDIAGRAMDIALOGCBYPREC0,
              label=u'0%', name=u'cbYprec0', parent=self.pnVal,
              pos=wx.Point(110, 60), size=wx.Size(30, 23), style=0)
        self.cbYprec0.Bind(wx.EVT_BUTTON, self.OnCbYprec0Button,
              id=wxID_VTDRAWDIAGRAMDIALOGCBYPREC0)

        self.cbYprec100 = wx.Button(id=wxID_VTDRAWDIAGRAMDIALOGCBYPREC100,
              label=u'100%', name=u'cbYprec100', parent=self.pnVal,
              pos=wx.Point(180, 60), size=wx.Size(34, 23), style=0)
        self.cbYprec100.Bind(wx.EVT_BUTTON, self.OnCbYprec100Button,
              id=wxID_VTDRAWDIAGRAMDIALOGCBYPREC100)

        self.cbYprec50 = wx.Button(id=wxID_VTDRAWDIAGRAMDIALOGCBYPREC50,
              label=u'50%', name=u'cbYprec50', parent=self.pnVal,
              pos=wx.Point(145, 60), size=wx.Size(30, 23), style=0)
        self.cbYprec50.Bind(wx.EVT_BUTTON, self.OnCbYprec50Button,
              id=wxID_VTDRAWDIAGRAMDIALOGCBYPREC50)

        self._init_coll_nbEdit_Pages(self.nbEdit)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        self.name=u'vtDrawDiagram Dialog'
        img=images.getOkBitmap()
        self.cbOk.SetBitmapLabel(img)
        self.cbAdd.SetBitmapLabel(img)
        self.cbAddName.SetBitmapLabel(img)
        self.cbAddLbl.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.cbCancel.SetBitmapLabel(img)
        self.iSelLblIdx=-1
        self.iSelIdx=-1
        self.labels=[[],[]]
        
        img=images.getDelBitmap()
        self.cbDel.SetBitmapLabel(img)
        self.cbDelName.SetBitmapLabel(img)
        self.cbDelLbl.SetBitmapLabel(img)
        
        self.chcLabel.SetSelection(0)
        
        self.lstVal.InsertColumn(0,_(u'Nr'),wx.LIST_FORMAT_RIGHT,30)
        self.lstVal.InsertColumn(1,_(u'x'),wx.LIST_FORMAT_RIGHT,60)
        self.lstVal.InsertColumn(2,_(u'y'),wx.LIST_FORMAT_RIGHT,60)
        
        self.lstLabels.InsertColumn(0,_(u'Pos'),wx.LIST_FORMAT_RIGHT,50)
        self.lstLabels.InsertColumn(1,_(u'Label'),wx.LIST_FORMAT_LEFT,50)
    def SetName(self,name):
        self.name=name
    def SetCanvas(self,canvas):
        self.canvas=canvas
    def SetValue(self,**kwargs):#labels,lst,grid,max):
        if kwargs.has_key('grid'):
            self.grid=kwargs['grid']
            self.spGridX.SetValue(self.grid[0])
            self.spGridY.SetValue(self.grid[1])
        if kwargs.has_key('max'):
            self.max=kwargs['max']
            self.spMaxX.SetValue(self.max[0])
            self.spMaxY.SetValue(self.max[1])
        if kwargs.has_key('labels'):
            self.labels=kwargs['labels']
        if kwargs.has_key('list'):
            self.lst=kwargs['list']
        if kwargs.has_key('canvas'):
            self.canvas=kwargs['canvas']
        #if kwargs.has_key('report_scale'):
        #    self.numScale.SetValue(kwargs['report_scale'])
        #if kwargs.has_key('report_font'):
        #    self.chcFont.SetSelection(kwargs['report_font'])
        #if kwargs.has_key('report_rotate'):
        #    self.chkRotate.SetValue(kwargs['report_rotate'])
                
        self.iSelIdx=-1
        self.iSelLblIdx=-1
        self.__updateLabels__()
        
        self.chcName.Clear()
        self.lstVal.DeleteAllItems()
        try:
            for it in self.lst:
                sName=it['name']
                self.chcName.Append(sName)
            self.chcName.SetSelection(0)
            wx.CallAfter(self.__selName__)
        except:
            traceback.print_exc()
    def GetValue(self,name):
        if name=='list':
            return self.lst
        elif name=='labels':
            return self.labels
        elif name=='max':
            return self.max
        elif name=='grid':
            return self.grid
        #elif name=='report_scale':
        #    return self.numScale.GetValue()
        #elif name=='report_font':
        #    return self.chcFont.GetSelection()
        #elif name=='report_rotate':
        #    return self.chkRotate.GetValue()
        
    def __updateLabels__(self):
        i=self.chcLabel.GetSelection()
        self.lstLabels.DeleteAllItems()
        
        if self.labels==None:
            self.labels=[[],[]]
        try:
            lst=self.labels[i]
            def cmpFunc(a,b):
                return int(a[0]-b[0])
            lst.sort(cmpFunc)
            for it in lst:
                idx=self.lstLabels.InsertImageStringItem(sys.maxint,'%4.2f'%it[0],-1)
                self.lstLabels.SetStringItem(idx,1,it[1],-1)
        except:
            pass
    def __selName__(self):
        iSel=self.chcName.GetSelection()
        #vtLog.vtLogCallDepth(None,'',verbose=1)
        dict=self.lst[iSel]
        vals=dict['values']
        rng=dict['range']
        self.spRngMin.SetValue(rng[0])
        self.spRngMax.SetValue(rng[1])
        self.lstVal.DeleteAllItems()
        self.iSelIdx=-1
        i=0
        for it in vals:
            idx=self.lstVal.InsertImageStringItem(sys.maxint, '%d'%i, -1)
            self.lstVal.SetStringItem(idx,1,'%6.2f'%it[0],-1)
            self.lstVal.SetStringItem(idx,2,'%6.2f'%it[1],-1)
            i+=1
        
    def OnCbOkButton(self, event):
        self.EndModal(1)
        event.Skip()

    def OnCbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()

    def OnLstValListItemSelected(self, event):
        self.iSelIdx=event.GetIndex()
        idx=self.iSelIdx
        it=self.lstVal.GetItem(idx,1)
        sX=it.m_text
        it=self.lstVal.GetItem(idx,2)
        sY=it.m_text
        self.numX.SetValue(sX)
        self.numY.SetValue(sY)
        
        event.Skip()

    def OnLstValListItemDeselected(self, event):
        self.iSelIdx=-1
        event.Skip()

    def OnChcNameChoice(self, event):
        self.__selName__()
        event.Skip()

    def OnSpbXSpinDown(self, event):
        fVal=self.numX.GetValue()
        fVal-=1.0
        self.numX.SetValue(fVal)
        event.Skip()

    def OnSpbXSpinUp(self, event):
        fVal=self.numX.GetValue()
        fVal+=1.0
        self.numX.SetValue(fVal)
        event.Skip()

    def OnSpbYSpinDown(self, event):
        fVal=self.numY.GetValue()
        fVal-=0.1
        self.numY.SetValue(fVal)
        event.Skip()

    def OnSpbYSpinUp(self, event):
        fVal=self.numY.GetValue()
        fVal+=0.1
        self.numY.SetValue(fVal)
        event.Skip()

    def OnCbYprec0Button(self, event):
        self.numY.SetValue(0.0)
        event.Skip()

    def OnCbYprec50Button(self, event):
        self.numY.SetValue(0.5)
        event.Skip()

    def OnCbYprec100Button(self, event):
        self.numY.SetValue(1.0)
        event.Skip()

    def OnCbAddButton(self, event):
        iSel=self.chcName.GetSelection()
        dict=self.lst[iSel]
        vals=dict['values']
        rng=dict['range']
        tup=(self.numX.GetValue(),self.numY.GetValue())
        if self.iSelIdx>=0:
            dict['values']=vals[:self.iSelIdx]+[tup]+vals[self.iSelIdx+1:]
            self.lstVal.SetStringItem(self.iSelIdx,1,'%6.2f'%tup[0],-1)
            self.lstVal.SetStringItem(self.iSelIdx,2,'%6.2f'%tup[1],-1)
        else:
            idx=self.lstVal.InsertImageStringItem(sys.maxint, '%d'%len(vals), -1)
            self.lstVal.SetStringItem(idx,1,'%6.2f'%tup[0],-1)
            self.lstVal.SetStringItem(idx,2,'%6.2f'%tup[1],-1)
            vals.append(tup)
        event.Skip()

    def OnCbDelButton(self, event):
        if self.iSelIdx>=0:
            dict['values']=vals[:self.iSelIdx]+vals[self.iSelIdx+1:]
            self.lstVal.DeleteItem(self.iSelIdx)
            self.iSelIdx=-1
        event.Skip()

    def OnCbAddNameButton(self, event):
        dict={'name':self.txtName.GetValue(),
            'range':(self.spRngMin.GetValue(),self.spRngMax.GetValue()),
            'values':[]}
        self.lst.append(dict)
        self.chcName.Append(self.txtName.GetValue())
        self.chcName.SetSelection(len(self.lst)-1)
        self.lstVal.DeleteAllItems()
        self.txtName.SetValue('')
        event.Skip()

    def OnCbDelNameButton(self, event):
        iSel=self.chcName.GetSelection()
        self.lst=self.lst[:iSel]+self.lst[iSel+1:]
        self.chcName.Delete(iSel)
        self.chcName.SetSelection(0)
        self.__selName__()
        event.Skip()

    def OnSpMaxXText(self, event):
        self.max=(self.spMaxX.GetValue(),self.max[1])
        event.Skip()

    def OnSpMaxYText(self, event):
        self.max=(self.max[0],self.spMaxY.GetValue())
        self.spRngMin.SetRange(0,self.max[1])
        self.spRngMax.SetRange(0,self.max[1])
        event.Skip()

    def OnLstLabelsListItemDeselected(self, event):
        self.iSelLblIdx=-1
        event.Skip()

    def OnLstLabelsListItemSelected(self, event):
        self.iSelLblIdx=event.GetIndex()
        idx=self.iSelLblIdx
        it=self.lstLabels.GetItem(idx,0)
        sPos=it.m_text
        it=self.lstLabels.GetItem(idx,1)
        sName=it.m_text
        self.txtLblName.SetValue(sName)
        self.numPos.SetValue(sPos)
        event.Skip()

    def OnCbAddLblButton(self, event):
        i=self.chcLabel.GetSelection()
        tup=(self.numPos.GetValue(),self.txtLblName.GetValue())
        if self.iSelLblIdx>=0:
            lbls=self.labels[i]
            self.labels[i]=lbls[:self.iSelLblIdx]+[tup]+lbls[self.iSelLblIdx+1:]
            self.lstLabels.SetStringItem(self.iSelLblIdx,0,'%4.2f'%tup[0],-1)
            self.lstLabels.SetStringItem(self.iSelLblIdx,1,tup[1],-1)
        else:
            self.labels[i].append(tup)
            idx=self.lstLabels.InsertImageStringItem(sys.maxint,'%4.2f'%tup[0],-1)
            self.lstLabels.SetStringItem(idx,1,tup[1],-1)
        event.Skip()

    def OnCbDelLblButton(self, event):
        i=self.chcLabel.GetSelection()
        if self.iSelLblIdx>=0:
            lbls=self.labels[i]
            self.labels[i]=lbls[:self.iSelLblIdx]+lbls[self.iSelLblIdx+1:]
            self.lstLabels.DeleteItem(self.iSelLblIdx)
            self.iSelLblIdx=-1
        event.Skip()

    def OnSpRngMaxText(self, event):
        iSel=self.chcName.GetSelection()
        dict=self.lst[iSel]
        dict['range']=(self.spRngMin.GetValue(),self.spRngMax.GetValue())
        event.Skip()

    def OnSpRngMinText(self, event):
        iSel=self.chcName.GetSelection()
        dict=self.lst[iSel]
        dict['range']=(self.spRngMin.GetValue(),self.spRngMax.GetValue())
        event.Skip()

    def OnChcLabelChoice(self, event):
        self.__updateLabels__()
        event.Skip()
    
    def GetReportValue(self):
        return 1.0,1,False
    
    def GetMax(self):
        return self.max
    
    def GetGrid(self):
        return self.grid
    
    def GetValues(self):
        return self.lst
        
    def GetLabels(self):
        return self.labels
        
    def OnSpGridXText(self, event):
        self.grid=(self.spGridX.GetValue(),self.grid[1])
        event.Skip()

    def OnSpGridYText(self, event):
        self.grid=(self.grid[0],self.spGridY.GetValue())
        event.Skip()


