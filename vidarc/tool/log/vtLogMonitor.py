#----------------------------------------------------------------------------
# Name:         vtLogMonitor.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20070208
# CVS-ID:       $Id: vtLogMonitor.py,v 1.1 2007/04/18 15:56:05 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import logging
import traceback,time
import wx
import pyHook
lngFileHandler=None

import win32com.client
import win32api,win32gui
# create an instance of WSH Shell
ws_shell = win32com.client.Dispatch("WScript.Shell")

# press some keys
def SendKeys(keys, shift=False):
  '''
  Inject a key press into the input queue of the focused window.

  @param keys: Keys to press
  @type keys: string
  @param shift: Hold the shift key?
  @type shift: boolean
  '''
  if shift: keys = '+'+keys
  ws_shell.SendKeys(keys)

def OnMouseEvent(event):
    w=wx.FindWindowAtPoint(event.Position)
    if w is None:
        return True
    #print w
    #print w.GetName(),w.GetId()
    
    #global lngFileHandler
    s=''.join([
            'Time:',repr(event.Time),';',
            'MessageName:',event.MessageName,';',
            'Message:',repr(event.Message),';',
            'id:',repr(w.GetId()),';',
            #'Window',':',repr(event.Window),';',
            #'WindowName',':',repr(event.WindowName),';',
            'Position:',repr(event.Position),';',
            'Wheel:',repr(event.Wheel),';',
            'Injected:',repr(event.Injected),
            '|'])
    logger=logging.getLogger('monitor')
    logger.log(70,s)
    # return True to pass the event to other handlers
    # return False to stop the event from propagating
    return True

def OnKeyboardEvent(event):
    #o=event.GetEventObject()
    #print o.GetName()
    #w=wx.FindWindowAtPoint(event.Position)
    #if w is None:
    #    return True
    s=''.join([
            'Time:',repr(event.Time),';',
            'MessageName',':',event.MessageName,';',
            'Message:',repr(event.Message),';',
            #'id:',repr(w.GetId()),';',
            #'Window:',repr(event.Window),';',
            #'WindowName:',repr(event.WindowName),';',
            'Ascii:', repr(event.Ascii),';',
            'Key:', repr(event.Key),';',
            'KeyID:', repr(event.KeyID),';',
            'ScanCode:', repr(event.ScanCode),';',
            'Extended:', repr(event.Extended),';',
            'Injected:', repr(event.Injected),';',
            'Alt:', repr(event.Alt),';',
            'Transition:', repr(event.Transition),
            '|'])
    logger=logging.getLogger('monitor')
    logger.log(70,s)
    # return True to pass the event to other handlers
    # return False to stop the event from propagating
    return True

def vtLogMonitorInit(sFN,sLogDN=None):
    import os
    global lngFileHandler
    import logging.handlers
    if sLogDN is None:
        try:
            # search enviroment
            sLogDN=os.getenv('vidarcLogDN',None)
        except:
            sLogDN=None
    try:
        if lngFileHandler is None:
            if sLogDN is not None:
                try:
                    os.makedirs(sLogDN)
                except:
                    pass
                lngFN=os.path.join(sLogDN,sFN)
            else:
                lngFN=sFN
            lngFileHandler=logging.handlers.RotatingFileHandler(lngFN,backupCount=3)
            lngFileHandler.setLevel(10)
            formatter=logging.Formatter('%(asctime)s|%(name)-18s|%(levelname)-9s|%(message)s')
            lngFileHandler.setFormatter(formatter)
            logging.getLogger('monitor').addHandler(lngFileHandler)
            lngFileHandler.doRollover()
            
            # create the hook mananger
            hm = pyHook.HookManager()
            # register two callbacks
            hm.MouseAllButtonsDown = OnMouseEvent
            hm.MouseAllButtonsUp = OnMouseEvent
            #hm.MouseAll = OnMouseEvent
            
            #hm.KeyDown = OnKeyboardEvent
            hm.KeyAll = OnKeyboardEvent
            
            # hook into the mouse and keyboard events
            hm.HookMouse()
            hm.HookKeyboard()
            #import pythoncom
            #pythoncom.PumpMessages()
    except:
        traceback.print_exc()
def _doReplay(fIn,wid,iScaleX,iScaleY):
    fTime=-1
    wwid=wid
    for sLine in fIn:
        strs=sLine.split('|')
        lMsgPart=strs[3].split(';')
        d={}
        try:
            for sMsg in lMsgPart:
                lMsg=sMsg.split(':')
                d[lMsg[0]]=lMsg[1]
        except:
            traceback.print_exc()
        fTimeNew=long(d['Time'])/1000.0
        if fTime>0:
            fDelta=fTimeNew-fTime
            if fDelta<0:
                fDelta=0.025
            time.sleep(fDelta)
        else:
            time.sleep(3)
        fTime=fTimeNew
        try:
            print d
            sMsg=d['MessageName']
            if sMsg in ['mouse left down','mouse left up']:
                id=int(d['id'])
                sPos=d['Position']
                x,y=sPos[1:-1].split(',')
                iX=int(x)
                iY=int(y)
                if 0:
                    wx.MutexGuiEnter()
                    wwid=wx.FindWindowAtPoint((iX,iY))
                    if wwid is not None:
                        pos=wwid.GetScreenPosition()
                        id=wwid.GetId()
                    wx.MutexGuiLeave()
                    print '   ',wwid
                    if wwid is not None:
                        print '   ',pos
                        print '     ',iX,iY
                        #win32api.SetCursorPos((iX,iY))
                        #iX,iY=iX-pos[0],iY-pos[1]
                        print '     ',iX,iY
                        if sMsg=='mouse left down':
                            iXdown,iYdown=iX,iY
                        elif sMsg=='mouse left up':
                            evt = wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, 
                                    id)
                            wx.PostEvent(wwid,evt)
                            
                        elif 0:
                            evt=wx.MouseEvent(wx.wxEVT_MOTION)
                            evt.m_x=iXdown
                            evt.m_y=iYdown
                            wx.PostEvent(wwid,evt)
                            
                            evt=wx.MouseEvent(wx.wxEVT_LEFT_DOWN)
                            evt.m_leftDown=True
                            evt.m_x=iXdown
                            evt.m_y=iYdown
                            print '   ','down',iXdown,iYdown#,evt.Button(wx.MOUSE_BTN_LEFT)
                            wx.PostEvent(wwid,evt)
                            
                            evt=wx.MouseEvent(wx.wxEVT_MOTION)
                            evt.m_x=iX
                            evt.m_y=iY
                            wx.PostEvent(wwid,evt)
                            
                            #wx.PostEvent(wwid,evt)
                            evt=wx.MouseEvent(wx.wxEVT_LEFT_UP)
                            #evt.m_leftDown=True
                            evt.m_x=iX
                            evt.m_y=iY
                            print '   ',' up ',iX,iY#,evt.Button(wx.MOUSE_BTN_LEFT)
                            wx.PostEvent(wwid,evt)
                if 1:
                    #win32api.SetCursorPos((iX,iY))
                    #hwnd=win32gui.WindowFromPoint((iX,iY))
                    #time.sleep(0.5)
                    iX*=iScaleX
                    iY*=iScaleY
                    if sMsg=='mouse left down':
                        #win32api.mouse_event(pyHook.HookConstants.WM_LBUTTONDOWN,0,0,0,0)
                        #win32api.mouse_event(1,iX,iY,0,0)
                        #win32gui.SetFocus(hwnd)
                        #win32api.mouse_event(0x201,0,0,0,0)
                        win32api.mouse_event(0x8001,iX,iY,0,0)
                        win32api.mouse_event(0x8002,0,0,0,0)
                    elif sMsg=='mouse left up':
                        #win32api.mouse_event(pyHook.HookConstants.WM_LBUTTONUP,0,0,0,0)
                        #win32api.mouse_event(0x202,0,0,0,0)
                        win32api.mouse_event(0x8001,iX,iY,0,0)
                        win32api.mouse_event(0x8004,0,0,0,0)
                        #win32api.mouse_event(0x20A,0,0,0,0)
                        
                if 0:
                    wx.MutexGuiEnter()
                    wwid=wx.FindWindowAtPoint((int(x),int(y)))
                    wx.MutexGuiLeave()
                    print '   ',wwid
                    if wwid is not None:
                        evt=None
                        pos=wwid.GetPosition()
                        iX=int(x)#-pos[0]
                        iY=int(y)#-pos[1]
                        print '   ',x,y,pos
                        if sMsg=='mouse left down':
                            evt=wx.MouseEvent(wx.wxEVT_LEFT_DOWN)
                            evt.m_leftDown=True
                        elif sMsg=='mouse left up':
                            #evt=wx.MouseEvent(wx.wxEVT_LEFT_UP)
                            #evt.m_leftDown=True
                            pass
                        if evt is not None:
                            evt.m_x=iX
                            evt.m_y=iY
                            win32api.SetCursorPos((iX,iY))
                            win32api.mouse_event(0x201,0,0,0,0)
                            evt2=wx.MouseEvent(wx.wxEVT_MOTION)
                            evt.m_x=iX
                            evt.m_y=iY
                            wx.PostEvent(wwid,evt2)
                            
                            wx.PostEvent(wwid,evt)
                            evt=wx.MouseEvent(wx.wxEVT_LEFT_UP)
                            #evt.m_leftDown=True
                            evt.m_x=iX
                            evt.m_y=iY
                            wx.PostEvent(wwid,evt)
                    else:
                        print 'window not found'
            elif sMsg in ['key down','key sys down','key up','key sys up']:
                if 1:
                    print '   ',sMsg,int(d['ScanCode'])
                    if sMsg in ['key down','key sys down']:
                        #win32api.keybd_event(0x0,0x8,0x8,long(d['ScanCode']))
                        win32api.keybd_event(0x0,int(d['ScanCode']),0x8,0)
                    elif sMsg in ['key up','key sys up']:
                        #win32api.keybd_event(0x0,0x8,0xA,long(d['ScanCode']))
                        win32api.keybd_event(0x0,int(d['ScanCode']),0xA,0)
                if 0:
                    evt=None
                    if sMsg in ['key down','key sys down']:
                        evt=wx.KeyEvent(wx.wxEVT_KEY_DOWN)
                    if sMsg in ['key up','key sys up']:
                        evt=wx.KeyEvent(wx.wxEVT_KEY_UP)
                    if evt is not None:
                        evt.m_keyCode=int(d['KeyID'])
                        if int(d['Alt'])==32:
                            evt.m_altDown=True
                        if d['Key']=='Lshift':
                            evt.m_shiftDown=True
                        if d['Key']=='Lcontrol':
                            evt.m_controlDown=True
                        wx.PostEvent(wwid,evt)
            elif sMsg=='SendKey':
                SendKeys(d['Keys'])
        except:
            traceback.print_exc()
    fIn.close()
def vtLogMonitorReplay(sFN,wid):
    try:
        #app=wx.GetApp()
        sz= wx.DisplaySize()
        
        fIn=open(sFN,'r')
        from vidarc.tool.vtThread import vtThread
        thdReplay=vtThread(None,bPost=False)
        iScaleX=65535/sz[0]
        iScaleY=65535/sz[1]
        print iScaleX,iScaleY
        thdReplay.Do(_doReplay,fIn,wid,iScaleX,iScaleY)
    except:
        traceback.print_exc()
