#----------------------------------------------------------------------------
# Name:         __init__.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: __init__.py,v 1.5 2011/03/31 10:34:27 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import __config__

import images as imgPlg

PYTHON_LOGGIN_SOCKET=0
PYTHON_LOGGING=-1
#PYTHON_LOGGING=0

VERSION=__config__.VERSION
try:
    getPluginData=imgPlg.getPluginData
except:
    getPluginData=None

PLUGABLE='vtLogFileViewerFrame'
PLUGABLE_FRAME=False
DOMAINS=['tools','log','vtLogFileViewer']

__all__=['DOMAINS','VERSION','PLUGABLE','PLUGABLE_FRAME','getPluginData',]

SSL=0
import sys
if hasattr(sys, 'importers'):
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=0
else:
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=0
