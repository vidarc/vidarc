#----------------------------------------------------------------------------
# Name:         vtLogList.py
# Purpose:      list control with limited entries
# Author:       Walter Obweger
#
# Created:      20080312
# CVS-ID:       $Id: vtLogList.py,v 1.4 2009/11/18 20:20:24 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

import sys

from vidarc.tool.log.vtLog import vtLogOriginWX
from vidarc.tool.vtThread import CallBack
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.time.vtTime import vtDateTime
from vidarc.tool.misc.vtmColumnStretch import vtmColumnStretch

class vtLogList(vtLogOriginWX,vtmColumnStretch,wx.ListCtrl):
    VERBOSE=0
    def __init__(self,id=-1,parent=None,pos=(0,0),size=(400,100),name='lstLog',
                style=0,time_align=wx.LIST_FORMAT_LEFT,time_width=140):
        global _
        _=vtLgBase.assignPluginLang('vtLog')
        if id<0:
            id=wx.NewId()
        wx.ListCtrl.__init__(self,id=id, name=name,parent=parent, pos=pos, size=size,
              style=wx.LC_REPORT)
        vtLogOriginWX.__init__(self)
        self.Bind(wx.EVT_LIST_COL_CLICK, self.OnColClick,id=id)
        self.Bind(wx.EVT_LIST_ITEM_SELECTED,self.OnItemSelected, id=id)
        
        vtmColumnStretch.__init__(self)
        self._iColTm=-1
        self._iLimitEntries=100
        self._bReverse=True
        self._iLock=-1
        self._GetColCount=self.GetColumnCount
        self._GetColWidth=self.GetColumnWidth
        self._SetColWidth=self.SetColumnWidth
        self.zDt=vtDateTime(True)
        wx.CallAfter(self._addTimeCol,time_align,time_width)
        wx.CallAfter(wx.EVT_SIZE,self,self.OnSizeStretch)
    def GetOrigin(self):
        return self.GetName()
    def OnSize(self,evt):
        evt.Skip()
    def _addTimeCol(self,time_align=wx.LIST_FORMAT_LEFT,time_width=140):
        if self._iColTm<0:
            iCols=self.GetColumnCount()
            self._iColTm=iCols
            self.InsertColumn(col=iCols,format=time_align,heading=_(u'time'),
                        width=time_width)
    def SetLimitEntries(self,val):
        self._iLimitEntries=val
    def GetLimitEntries(self):
        return self._iLimitEntries
    def SetReverse(self,bFlag):
        self._bReverse=bFlag
    def GetReverse(self,bFlag):
        return self._bReverse
    def IsReverse(self):
        return self._bReverse
    def __limitEntries__(self):
        iDiff=0
        for i in xrange(self._iLimitEntries,self.GetItemCount()):
            if self._bReverse:
                self.DeleteItem(self._iLimitEntries)
                iDiff+=1
            else:
                self.DeleteItem(0)
                iDiff+=1
        return iDiff
    def LimitEntries(self):
        CallBack(self.limitEntries)
    def limitEntries(self):
        iDiff=0
        try:
            self.Freeze()
            iDiff=self.__limitEntries__()
        except:
            vtLog.vtLngTB(self.GetName())
        self.Thaw()
        return iDiff
    def AddEntry(self,lst,imgId=-1,iData=None):
        CallBack(self.addEntry,lst,imgId=imgId,iData=iData)
    def addEntry(self,lst,imgId=-1,iData=None):
        try:
            self.Freeze()
            self.zDt.Now()
            if self.IsReverse():
                iPos=0
            else:
                iPos=sys.maxint
            idx=wx.ListCtrl.InsertImageStringItem(self,iPos,lst[0],imgId)
            iCol=1
            for s in lst[1:]:
                self.SetStringItem(idx,iCol,s)
                iCol+=1
            if iData is not None:
                self.SetItemData(iData)
            self.SetStringItem(idx,self._iColTm,self.zDt.GetDateTimeStr(' '))
            if self._iLock==-1:
                self.EnsureVisible(idx)
                self.__limitEntries__()
            else:
                self._iLock+=1
                self.EnsureVisible(self._iLock)
        except:
            self.__logTB__()
        self.Thaw()
    def OnColClick(self,evt):
        evt.Skip()
        self._iLock=-1
        try:
            self.Freeze()
            self.__limitEntries__()
        except:
            self.__logTB__()
        self.Thaw()
    def OnItemSelected(self,evt):
        evt.Skip()
        self._iLock=evt.GetIndex()
    def GetSelLst(self,iS=0,iE=-1):
        l=[]
        try:
            if iE<0:
                iE=self.GetItemCount()
            for iIdx in xrange(iS,iE):
                if self.GetItemState(iIdx,wx.LIST_STATE_SELECTED)==wx.LIST_STATE_SELECTED:
                    l.append(iIdx)
        except:
            self.__logTB__()
        return l
