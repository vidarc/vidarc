#----------------------------------------------------------------------------
# Name:         vtLog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtLog.py,v 1.87 2018/01/07 05:05:28 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import sys,os,os.path,traceback,types,time,getopt,threading,codecs
import inspect
import pprint
import __init__
import binascii
import sys
if sys.version_info<(3,0):
    import cPickle
    import cStringIO
else:
    import _pickle as cPickle
    import io as cStringIO
    
import vSystem

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

VERBOSE_DEEP=VERBOSE>10

try:
    if __init__.PYTHON_LOGGING==-1:
        import math
        import vp.vCore as vpc
except:
    __init__.PYTHON_LOGGING=0
if VERBOSE>0:
    print('logging style:',__init__.PYTHON_LOGGING)
if VERBOSE_DEEP:
    print('vtLog ... 01')
if __init__.PYTHON_LOGGING==1:
    import logging
    import logging.handlers
    DEBUG=logging.DEBUG
    WARN=logging.WARN
    WARNING=logging.WARNING     # is same as WARN
    INFO=logging.INFO
    ERROR=logging.ERROR
    CRITICAL=logging.CRITICAL
    FATAL=logging.FATAL         # is same as CRITICAL
elif __init__.PYTHON_LOGGING==-1:
    DEBUG=vpc.vLog_DEBUG
    WARN=vpc.vLog_WARN
    WARNING=vpc.vLog_WARN     # is same as WARN
    INFO=vpc.vLog_INFO
    ERROR=vpc.vLog_ERROR
    CRITICAL=vpc.vLog_CRITICAL
    FATAL=vpc.vLog_CRITICAL   # is same as WARN
else:
    import vidarc.tool.InOut.fnUtil as fnUtil
    _zLoaded=time.time()
    _zNextRollover=None
    DEBUG=10
    WARN=30
    WARNING=30     # is same as WARN
    INFO=20
    ERROR=40
    CRITICAL=50
    FATAL=50         # is same as CRITICAL
    _LOGNAMES_DICT={
            DEBUG       :'DEBUG   ',
            INFO        :'INFO    ',
            WARN        :'WARN    ',
            ERROR       :'ERROR   ',
            CRITICAL    :'CRITICAL',
            'DEBUG'     :DEBUG,
            'INFO'      :INFO,
            'WARN'      :WARN,
            'ERROR'     :ERROR,
            'CRITICAL'  :CRITICAL,
            }
    #_srcfile is used when walking the stack to check when we've got the first
    # caller stack frame.
    #
    if hasattr(sys, 'frozen'): #support for py2exe
        _srcfile = "logging%s__init__%s" % (os.sep, __file__[-4:])
    elif __file__[-4:].lower() in ['.pyc', '.pyo']:
        _srcfile = __file__[:-4] + '.py'
    else:
        _srcfile = __file__
    _srcfile = os.path.normcase(_srcfile)
    
    # next bit filched from 1.5.2's inspect.py
    def currentframe():
        """Return the frame object for the caller's stack frame."""
        try:
            raise Exception
        except:
            return sys.exc_traceback.tb_frame.f_back
    
    if hasattr(sys, '_getframe'): currentframe = sys._getframe
    # done filching
    USE_CONSOLE_LOGGER=0
    if USE_CONSOLE_LOGGER:
        import logging
        import logging.handlers
        console = logging.StreamHandler()
        console.setLevel(logging.DEBUG)
        # set a format which is simpler for console use
        formatter = logging.Formatter('%(name)-18s %(levelname)-8s: %(message)s')
        # tell the handler to use this format
        console.setFormatter(formatter)
        logging.getLogger('').addHandler(console)
        lngConsoleDbg=console
        lnggerDbg=logging.getLogger('')
        lnggerDbg.setLevel(logging.DEBUG)

#_acquire=logging._acquireLock
#_release=logging._releaseLock
if sys.platform=='win32':
    REPLACE_DRIVE_JUNK=True
else:
    REPLACE_DRIVE_JUNK=False

MAX_CALLSTACK_DEPTH=10

_semLock=threading.Lock()#threading.Semaphore()
_acquire=_semLock.acquire
_release=_semLock.release
def _acquireOld():
    global _semLock
    global lngConsoleDbg
    #sio = cStringIO.StringIO()
    #sio.write('------------acquire\n')
    #traceback.print_stack(file=sio,limit=5)
    #s = sio.getvalue()
    #lnggerDbg.debug(s)
    #sio.close()
    #print '------------acquire------------'
    #traceback.print_stack(limit=5)
    _semLock.acquire()
    #sio = cStringIO.StringIO()
    #sio.write('++++++++++++acquire\n')
    #traceback.print_stack(file=sio,limit=5)
    #s = sio.getvalue()
    #lnggerDbg.debug(s)
    #sio.close()
    #lnggerDbg.debug('++++++++++++acquire++++++++++++')
def _releaseOld():
    global _semLock
    #sio = cStringIO.StringIO()
    #sio.write('++++++++++++release\n')
    #traceback.print_stack(file=sio,limit=5)
    #s = sio.getvalue()
    #lnggerDbg.debug(s)
    #print '++++++++++++release++++++++++++'
    #traceback.print_stack(limit=3)
    _semLock.release()
    #lnggerDbg.debug('------------release------------')

def _acquireFmt():
    pass
def _releaseFmt():
    pass

LOG_LEVELS=[CRITICAL,ERROR,WARN,INFO,DEBUG]
LOG_LEVELS_SHORT={
    CRITICAL:'c',ERROR:'e',
    WARN:'w',INFO:'i',DEBUG:'d'
    }
if CRITICAL!=FATAL:
    LOG_LEVELS.insert(0,FATAL)
    LOG_LEVELS_SHORT[FATAL]='f'

def _createLevelDict():
    global LOG_LEVELS
    d={}
    for lv in LOG_LEVELS:
        d[lv]=0
    return d

dLng=_createLevelDict()
dLngStore=_createLevelDict()
dLngTrend=_createLevelDict()
dLngTrendPrev=_createLevelDict()
from vidarc .tool.vtRingBuffer import vtRingBuffer
rbLng=None
rbLng=None

if VERBOSE_DEEP:
    print('vtLog ... 10')

class vtLogRingBuffer(vtRingBuffer):
    def calcTrend(self,dLngTrend):
        iAct=self.iAct
        for i in xrange(0,self.iSize):
            d=self.lSlot[iAct]
            for k,v in d.iteritems():
                if k in dLngTrend:
                    dLngTrend[k]+=v
            iAct-=1
            if iAct<0:
                iAct+=self.iSize

def _addLogNum(lv,i=1,bAcquire=True):
    #global rbLng
    global dLng
    global LEVEL
    if bAcquire:
        _acquire()
    try:
        if lv>=LEVEL:
            #d=rbLng.get()
            #if lv in d:
            #    d[lv]+=i
            if lv in dLng:
                dLng[lv]+=i
            return True
        return False
    finally:
        if bAcquire:
            _release()
def _getOrderedStr(d):
    return ''.join([''.join([vtLngGetShortCut(lngNr),':%03d'%d[lngNr],' ']) for lngNr in LOG_LEVELS])

PLUGIN_ORIGIN=''
TRANS_STR=''.join([chr(i) for i in xrange(256)])
TRANS_STR=TRANS_STR.replace(chr(10),';')
TRANS_STR=TRANS_STR.replace('|',':')

# toplevel window for PrintMsg
gWidTopWidget=None
gsTopWidgetName=''
def setPluginOrigin(domain):
    global PLUGIN_ORIGIN
    try:
        PLUGIN_ORIGIN=domain+'_'
    except:
        PLUGIN_ORIGIN=''

def _resetDict(d):
    for k in d.iterkeys():
        d[k]=0

#vtLngNumReset()


EMPTY='                                                             '
EMPTY_LEN=len(EMPTY)
lngConsole=None
lngCfgSrv=None
lngFileHandler=None
lngFN=None
lngActFN=None
lngSockHandler=None
lngPort=''

def vtLngHasNumDictChanged(dOld):
    d=vtLngGetNumDict()
    keys=d.keys()
    for k in keys:
        if d[k]!=dOld[k]:
            return True
    return False

if VERBOSE_DEEP:
    print('vtLog ... 20')

if __init__.PYTHON_LOGGING==-1:
    LEVEL=vpc.vLog_ERROR
    def createRingBuffer(iSz):
        if VERBOSE_DEEP:
            print('vtLog ... 20 create')
        return vpc.vLogNumCreate(iSz+1)
    def vtLngGetNumDict():
        global LOG_LEVELS
        d={}
        try:
            vpc.vLogNumStore()
            for lv in LOG_LEVELS:
                d[lv]=vpc.vLogNumGet(lv)
        finally:
            pass
        return d
    def vtLngNumReset(d=None):
        return vpc.vLogNumReset()
    def vtLngNumStore():
        return vpc.vLogNumStore()
    def vtLngGetNum(lngNr):
        return vpc.vLogNumGet(lngNr)
    def vtLngNumTrend():
        return vpc.vLogNumTrend()
    def vtLngGetNumDiff(lngNr):
        return vpc.vLogNumGetDiff(lngNr)
    def vtLngGetNumDiffStr(lngNr,limit=100,strLimit='**',fmt=':%02d'):
        s=vpc.vLogGetDiffStr(lngNr,2)
        return s
    def vtLngGetNumDiffOrderedStr(limit=100,strLimit='**',fmt=':%02d'):
        s=vpc.vLogGetDiffStr(2)
        return s
    def vtLngGetTrendStr(lngNr,limit=100,strLimit='**',fmt=':%02d'):
        s=vpc.vLogGetTrendStr(2)
        return s
    def vtLngGetTrendOrderedStr(limit=100,strLimit='**',fmt=':%02d'):
        if limit!=100:
            s=vpc.vLogGetTrendStr(max(1,int(math.ceil(math.log10(limit)))))
        else:
            s=vpc.vLogGetTrendStr(2)
        return s
else:
    from vtLogSrv import *
    LEVEL=logging.ERROR
    def vtLngGetNumDict():
        global dLng
        d={}
        _acquire()
        try:
            d.update(dLng)
        finally:
            _release()
        return d
    def createRingBuffer(size):
        global rbLng
        _acquire()
        try:
            rbLng=vtLogRingBuffer(size)
            for lv in xrange(size):
                rbLng.put(_createLevelDict())
        finally:
            _release()
    def vtLngNumReset(d=None):
        global dLng
        global dLngStore
        global dLngTrend
        global dLngTrendPrev
        global rbLng
        _acquire()
        try:
            if d is None:
                d=dLng
                _resetDict(dLngStore)
                def resetNum(d):
                    _resetDict(d)
                rbLng.proc(resetNum)
                _resetDict(dLngTrend)
                _resetDict(dLngTrendPrev)
            _resetDict(d)
        finally:
            _release()
    def vtLngNumTrend():
        global rbLng
        global dLng
        global dLngTrend
        global dLngTrendPrev
        _acquire()
        try:
            _resetDict(dLngTrend)
            def addNum(d,dLngTrend):
                for k,v in d.iteritems():
                    if k in dLngTrend:
                        dLngTrend[k]+=v
            rbLng.next()
            d=rbLng.get()
            for lv in LOG_LEVELS:
                d[lv]=dLng[lv]-dLngTrendPrev[lv]
            dLngTrendPrev.update(dLng)
            #rbLng.proc(addNum,dLngTrend)
            rbLng.calcTrend(dLngTrend)
            if __init__.PYTHON_LOGGING==False:
                global lngFileHandler
                #global lngSockHandler
                if lngFileHandler is not None:
                    lngFileHandler.flush()
        finally:
            _release()

    def vtLngNumStore():
        global rbLng
        global dLng
        global dLngStore
        _acquire()
        try:
            _resetDict(dLngStore)
            dLngStore.update(dLng)
            rbLng.next()
            if __init__.PYTHON_LOGGING==False:
                global lngFileHandler
                #global lngSockHandler
                if lngFileHandler is not None:
                    lngFileHandler.flush()
        finally:
            _release()
    def vtLngGetNum(lngNr):
        global dLng
        _acquire()
        i=-1
        try:
            if lngNr in dLng:
                i=dLng[lngNr]
            else:
                i=-1
        finally:
            _release()
        return i
    def vtLngGetNumDiff(lngNr):
        global dLng
        global dLngStore
        _acquire()
        i=-1
        try:
            if lngNr in dLngStore:
                i=dLng[lngNr]-dLngStore[lngNr]
            else:
                i=-1
        finally:
            _release()
        return i
    def vtLngGetNumDiffStr(lngNr,limit=100,strLimit='**',fmt=':%02d'):
        i=vtLngGetNumDiff(lngNr)
        if i>0:
            if i>=limit:
                s=''.join([vtLngGetShortCut(lngNr),':',strLimit,' '])
            else:
                s=''.join([vtLngGetShortCut(lngNr),fmt%i,' '])
        return ''

    def vtLngGetNumDiffOrderedStr(limit=100,strLimit='**',fmt=':%02d'):
        return ''.join([vtLngGetNumDiffStr(lngNr,limit,strLimit,fmt) for lngNr in LOG_LEVELS])
    def vtLngGetTrendStr(lngNr,limit=100,strLimit='**',fmt=':%02d'):
        global dLngTrend
        _acquire()
        try:
            s=''
            if lngNr in dLngTrend:
                i=dLngTrend[lngNr]
                if i>0:
                    if i>=limit:
                        s=''.join([vtLngGetShortCut(lngNr),':',strLimit,' '])
                    else:
                        s=''.join([vtLngGetShortCut(lngNr),fmt%i,' '])
        finally:
            _release()
        return s

    def vtLngGetTrendOrderedStr(limit=100,strLimit='**',fmt=':%02d'):
        #return ''.join([vtLngGetTrendStr(lngNr,limit,strLimit,fmt) for lngNr in LOG_LEVELS])
        global dLngTrend
        _acquire()
        try:
            l=[]
            for lngNr in LOG_LEVELS:
                if lngNr in dLngTrend:
                    i=dLngTrend[lngNr]
                    if i>0:
                        if i>=limit:
                            s=''.join([vtLngGetShortCut(lngNr),':',strLimit,' '])
                        else:
                            s=''.join([vtLngGetShortCut(lngNr),fmt%i,' '])
                        l.append(s)
        finally:
            _release()
        return ''.join(l)
    if VERBOSE_DEEP:
        print('vtLog ... 21')
    createRingBuffer(30)
    if VERBOSE_DEEP:
        print('vtLog ... 22')


def vtLngGetShortCut(lngNr):
    global LOG_LEVELS_SHORT
    if lngNr in LOG_LEVELS_SHORT:
        return LOG_LEVELS_SHORT[lngNr]
    return ''

def vtLngGetNumDiffSum():
    i=0
    for lngNr in LOG_LEVELS:
        if vtLngIsLogged(lngNr):
            j=vtLngGetNumDiff(lngNr)
            if j>0:
                i+=j
    return i

#from vtLogSrv import *
def vtLngDump(o):
    return binascii.hexlify(cPickle.dumps(o))
def __conv__(msg):
    t=type(msg)
    #print(t)
    if t==types.UnicodeType:
        pass
    elif t==types.StringType:
        try:
            msg=unicode(msg)
        except:
            msg=binascii.hexlify(cPickle.dumps(msg))
    else:
        try:
            msg=str(msg).decode('utf-8')
            #msg=repr(msg).decode('utf-8')
        except:
            msg=binascii.hexlify(cPickle.dumps(msg))
            
    #print type(msg),msg
    msg=msg.replace(chr(10),u';')
    msg=msg.replace(u'|',u':')
    return msg
    if t==types.UnicodeType:
        #return msg.encode('ISO-8859-1')
        return msg.encode('ascii','ignore')#'xmlcharrefreplace')
    elif t==types.StringType:
        return msg
    else:
        return binascii.hexlify(cPickle.dumps(msg))

def __getCallStack(level2skip,deep=MAX_CALLSTACK_DEPTH):
    lStack=traceback.extract_stack(limit=level2skip+1+deep)
    if REPLACE_DRIVE_JUNK:
        return [''.join([fn.replace(':','_'),':',sMethod,'(%d)'%lno]) for fn,lno,sMethod,cmd in lStack[:-(level2skip+1)]]
    else:
        return [''.join([fn,':',sMethod,'(%d)'%lno]) for fn,lno,sMethod,cmd in lStack[:-(level2skip+1)]]

def getCallStack(level2skip,deep=MAX_CALLSTACK_DEPTH):
    lStack=traceback.extract_stack(limit=level2skip+1+deep)
    return lStack[:-(level2skip+1)]

if __init__.PYTHON_LOGGING==1:
    def _log(lv,msg,origin,bAcquire=False):
        logger=logging.getLogger(origin)
        logger.log(lv,msg)
elif __init__.PYTHON_LOGGING==-1:
    if VERBOSE_DEEP:
        print('vtLog ... 30')
    def _log(lv,msg,origin,bAcquire=True,level2skip=1):
        try:
            vpc.Log(lv,msg,origin,level2skip+1)
        except:
            vpc.TB(origin)
else:
    def _log(lv,msg,origin,bAcquire=True):
        try:
            #lnggerDbg.debug('_log')
            if bAcquire:
                _acquire()
            #global LEVEL
            #if lv<LEVEL:
            #    return
            global lngFileHandler
            global lngSockHandler
            global _LOGNAMES_DICT
            global _zLoaded
            global _zNextRollover
            global lngFN
            global lngActFN
            if lv in _LOGNAMES_DICT:
                sLngLv=u'%-8s'%_LOGNAMES_DICT[lv]
            else:
                sLngLv=u'  ????  '
            if lngFileHandler:
                #lnggerDbg.debug('get time')
                #_zNow.Now()
                
                #lngFileHandler.write(u'|'.join([_zNow.GetDateTimeStr(u' '),
                #            u'%-18s'%origin,sLngLv,u' ']))
                #lnggerDbg.debug('get call stack')
                try:
                    zNow=time.time()
                    if _zNextRollover is not None:
                        if zNow > _zNextRollover:
                            sTmpFN,_zNextRollover=fnUtil.shiftFileTimed(lngFN,2,'H')
                            _zNextRollover+=7200
                            try:
                                f=codecs.open(sTmpFN, 'a', 'UTF-8')
                                lngFileHandler.close()
                                lngFileHandler=f
                                lngActFN=sTmpFN
                            except:
                                pass
                    iMilliSec=(zNow - long(zNow)) * 1000
                    lngFileHandler.write(time.strftime(u"%Y-%m-%d %H:%M:%S", time.localtime(zNow)))
                    lngFileHandler.write(u",%03d" % (iMilliSec))
                    #lngFileHandler.write(unicode(_zNow.GetDateTimeStr(' ')))
                except:
                    pass
                #lnggerDbg.debug('time written')
                lngFileHandler.write(u'|')
                try:
                    lngFileHandler.write(u'%-18s'%origin)
                except:
                    pass
                #lnggerDbg.debug('origin written')
                lngFileHandler.write(u'|')
                try:
                    lngFileHandler.write(sLngLv)
                except:
                    pass
                #lnggerDbg.debug('level written')
                lngFileHandler.write(u'|')
                try:
                    lngFileHandler.write(msg)
                except:
                    pass
                #lnggerDbg.debug('msg written')
                lngFileHandler.write(u'\n')
                #lngFileHandler.flush()
        finally:
            #lnggerDbg.debug('finally')
            if bAcquire:
                _release()
            pass
if __init__.PYTHON_LOGGING==-1:
    if VERBOSE_DEEP:
        print('vtLog ... 40')
    def vtLog(self,msg,level=0,verbose=-1):
        try:
            vpc.Verbose(self,msg,level=level,verbose=verbose,level2skip=2)
        except:
            vpc.TB('vtLog')
    def vtCallStack(deep=1,verbose=1,level=1):
        try:
            vpc.CallStack(deep,verbose=verbose,level=level,level2skip=2)
        except:
            vpc.TB('vtLog')
    def CallStack(msg):
        try:
            vpc.CallStack(3,verbose=1,level=0,level2skip=2)
            msg=__conv__(msg)
            #print msg
        except:
            vpc.TB('vtLog')
    def vtLngTB(origin='',bLogAsDbg=False,lngName=''):
        try:
            vpc.TB(origin,level2skip=2)
        except:
            vpc.TB('vtLog')
    def vtLngCS(lngNr,msg,origin=''):
        try:
            vpc.Log(lngNr,msg,origin,2)
        except:
            vpc.TB('vtLog')
    def vtLngCallStack(self,lngNr,msg,verbose=-1,origin=''):
        try:
            if self is not None:
                if verbose==-1:
                    verbose=self.verbose
                if verbose>0:
                    _log(lngNr,msg,origin)
            else:
                if verbose>0:
                    _log(lngNr,msg,origin)
        except:
            vpc.TB('vtLog')
        if verbose>0:
            return 1
        else:
            return 0
    def vtLngCur(lngNr,msg,origin='',level2skip=1,lngName=''):
        try:
            if vpc.vLogIsLogged(lngNr,origin):
                vpc.Log(lngNr,msg,origin,level2skip=level2skip+1)
        except:
            vpc.TB('vtLog')
else:
    def vtLog(self,msg,level=0,verbose=-1):
        #_acquireFmt()
        try:
            try:
                global EMPTY_LEN
                if self is not None:
                    if verbose==-1:
                        verbose=self.verbose
                    sObj='%s'%(self.__class__)
                if verbose>0:
                    msg=__conv__(msg)
                    lstCall=__getCallStack(1,1)
                    lstCall.reverse()
                    if level>0:
                        i=level+level
                        if i>EMPTY_LEN:
                            i=EMPTY_LEN
                        if self is not None:
                            print(EMPTY[:i],sObj)
                        print(EMPTY[:i],lstCall[-1])
                    else:
                        if self is not None:
                            print(sObj)
                        print(lstCall[-1])
                        print('        :',msg)
                    #_releaseFmt()
                    return 1
            except:
                sys.stderr.write('\n\ntime:%s\n'%time.asctime(time.localtime(time.time())))
                traceback.print_stack(file=sys.stderr)
                traceback.print_exc(file=sys.stderr)
            return 0
        finally:
            #_releaseFmt()
            pass
    def vtCallStack(deep=1,verbose=1,level=1):
        if verbose<=0:
            return
        try:
            #_acquireFmt()
            try:
                j=0
                if level>0:
                    j=level+level
                    if j>EMPTY_LEN:
                        j=EMPTY_LEN
                sPrefix='%s%s'%(EMPTY[:j],'callstack')
                lstCall=__getCallStack(1,deep)
                lstCall.reverse()
                j=0
                for s in lstCall:
                    print(sPrefix,'%3d'%j,s)
                    j+=1
            except:
                sys.stderr.write('\n\ntime:%s\n'%time.asctime(time.localtime(time.time())))
                traceback.print_stack(file=sys.stderr)
                traceback.print_exc(file=sys.stderr)
        finally:
            #_releaseFmt()
            pass
    def vtLngTB(origin='',bLogAsDbg=False,lngName=''):
        global lngFN
        if lngFN is None:
            return
        try:
            #_acquireFmt()
            try:
                lstCall=__getCallStack(1)
                lstCall.reverse()
                #msg=traceback.format_exc().replace(chr(10),';')
                msg=__conv__(traceback.format_exc())
                if bLogAsDbg:
                    _log(DEBUG,u'|'.join([msg,u','.join(lstCall)]),origin)
                    _addLogNum(DEBUG)
                else:
                    _log(ERROR,u'|'.join([msg,u','.join(lstCall)]),origin)
                    _addLogNum(ERROR)
            except:
                sys.stderr.write('\n\ntime:%s\n'%time.asctime(time.localtime(time.time())))
                traceback.print_stack(file=sys.stderr)
                traceback.print_exc(file=sys.stderr)
        finally:
            #_releaseFmt()
            pass
    def vtLngCS(lngNr,msg,origin=''):
        global lngFN
        global LEVEL
        if lngNr<LEVEL:
            return
        if lngFN is None:
            return
        try:
            #_acquireFmt()
            try:
                lstCall=__getCallStack(1)
                lstCall.reverse()
                #msg=string.replace(msg,chr(10),';')
                #msg=string.replace(msg,'|',':')
                msg=__conv__(msg)#.translate(TRANS_STR)
                _log(lngNr,u'|'.join([msg,u','.join(lstCall)]),origin)
                _addLogNum(lngNr)
            except:
                sys.stderr.write('\n\ntime:%s\n'%time.asctime(time.localtime(time.time())))
                traceback.print_stack(file=sys.stderr)
                traceback.print_exc(file=sys.stderr)
        finally:
            #_releaseFmt()
            pass
    def vtLngCallStack(self,lngNr,msg,verbose=-1,origin=''):
        global lngFN
        global LEVEL
        if lngNr<LEVEL:
            return
        if lngFN is None:
            return
        try:
            #_acquireFmt()
            try:
                msg=__conv__(msg)
                lstCall=__getCallStack(1)
                lstCall.reverse()
                if self is not None:
                    if verbose==-1:
                        verbose=self.verbose
                    if verbose>0:
                        _log(lngNr,u'|'.join([msg,u','.join(lstCall)]),origin)
                        _addLogNum(lngNr)
                else:
                    if verbose>0:
                        _log(lngNr,u'|'.join([msg,u','.join(lstCall)]),origin)
                        _addLogNum(lngNr)
            except:
                #for s in lstCall:
                #    print s
                #traceback.print_exc()
                sys.stderr.write('\n\ntime:%s\n'%time.asctime(time.localtime(time.time())))
                traceback.print_stack(file=sys.stderr)
                traceback.print_exc(file=sys.stderr)
        finally:
            #_releaseFmt()
            pass
        if verbose>0:
            return 1
        else:
            return 0
    def vtLngCur(lngNr,msg,origin='',level2skip=1,lngName=''):
        global lngFN
        global LEVEL
        if lngNr<LEVEL:
            return
        #global PLUGIN_ORIGIN
        if lngFN is None:
            return
        try:
            #_acquireFmt()
            try:
                #lnggerDbg.debug('get call stack')
                lstCall=__getCallStack(level2skip)
                lstCall.reverse()
                #try:
                #    sFn=os.path.split(sys._getframe(level2skip).f_code.co_filename)[1]
                #except:
                #    sFn=''
                #lnggerDbg.debug('got call stack')
                try:
                    s=lstCall[0]
                    i=s.find(':')
                    j=s.find('(',i)
                    sMethod=s[i+1:j]
                except:
                    sMethod=''
                #lnggerDbg.debug('get method')
                if type(msg)==types.ListType:
                    s=cStringIO.StringIO()
                    for o in msg:
                        fmtLimit(o,s=s,iLimit=2)
                        s.write(';')
                    msg=s.getvalue()
                else:
                    msg=__conv__(msg)
                #lnggerDbg.debug('msg converted')
                if type(msg)==types.UnicodeType:
                    msg_out=u'|'.join([u';'.join([sMethod,msg]),
                                    u','.join(lstCall)])
                else:
                    msg_out=u'|'.join([u';'.join([sMethod,unicode(msg)]),
                                    u','.join(lstCall)])
                #lnggerDbg.debug('msg joined')
                #print '',lngNr,msg_out
                _log(lngNr,msg_out,origin,bAcquire=True)
                #lnggerDbg.debug('logged')
                _addLogNum(lngNr,bAcquire=True)
                #lnggerDbg.debug('counter updated')
                #del lstCall
                #del msg
            except:
                #lnggerDbg.debug('exception')
                sys.stderr.write('\n\ntime:%s\n'%time.asctime(time.localtime(time.time())))
                traceback.print_stack(file=sys.stderr)
                traceback.print_exc(file=sys.stderr)
                print(origin,type(origin))
                print(msg,type(msg))
                #print sMethod
                #print lstCall
        finally:
            #lnggerDbg.debug('finally')
            #_releaseFmt()
            pass
        #lnggerDbg.debug('exit')
    def CallStack(msg):
        try:
            #_acquireFmt()
            try:
                global EMPTY_LEN
                msg=__conv__(msg)
                lstCall=__getCallStack(1)
                #lstCall.reverse()
                i=0
                for s in lstCall:
                    j=i*2
                    if j>EMPTY_LEN:
                        j=EMPTY_LEN
                    fn,sm=s.split(':')
                    
                    print('%3d'%i,'%-90s'%''.join([EMPTY[:j],sm]),fn)
                    i+=1
                print('___',EMPTY[:j],msg)
            except:
                sys.stderr.write('\n\ntime:%s\n'%time.asctime(time.localtime(time.time())))
                traceback.print_stack(file=sys.stderr)
                traceback.print_exc(file=sys.stderr)
        finally:
            #_releaseFmt()
            pass
        return 0

if VERBOSE_DEEP:
    print('vtLog ... 50')

def vtLogCallDepth(self,msg,verbose=-1,callstack=True):
    return 0
def vtSpace():
    print('') 
def vtLngTBWX(obj,bLogAsDbg=False,lngName=''):
    origin=vtLngGetRelevantNamesWX(obj)
    vtLngTB(origin,bLogAsDbg=bLogAsDbg,lngName=lngName)
def vtLngCurWX(lngNr,msg,obj,level2skip=1,lngName=''):
    if vtLngIsLogged(lngNr)==False:
        return
    origin=vtLngGetRelevantNamesWX(obj)
    vtLngCur(lngNr,msg,origin,level2skip+1,lngName=lngName)
def vtLngGetRelevantNamesWX(par,appl=None):
    if hasattr(par,'_originLogging'):
        if VERBOSE:
            vtLngCur(DEBUG,';'.join(['par def',par._originLogging]),'vtLog')
        return par._originLogging
    lName=[]
    if appl is not None:
        lName.append(appl)
    lName.append(par.GetName())
    wid=par
    par=par.GetParent()
    while par is not None:
        if par.GetClassName() in ['wxFrame','wxMDIChildFrame','wxDialog']:
            lName.append(par.GetName())
        elif par.GetClassName() in ['wxMDIParentFrame',]:
            lName.append(par.GetName())
            break
        elif getattr(par,'IsLogRelevant',False):
            lName.append(par.GetName())
        par=par.GetParent()
    lName.reverse()
    s='.'.join(lName)
    if VERBOSE:
        vtLngCur(DEBUG,';'.join(['calc',s]),'vtLog')
    wid._originLogging=s
    del lName
    return s
    
def vtLngCurCls(lngNr,msg,obj,level2skip=1,lngName=''):
    try:
        origin=obj.__class__.__name__
    except:
        origin=u''
    vtLngCur(lngNr,msg,origin,level2skip+1,lngName=lngName)

def InitMsgWid():
    global gWidTopWidget
    global gsTopWidgetName
    try:
        from wx import GetApp
        app=GetApp()
        if app is not None:
            gWidTopWidget=app.GetTopWindow()
            if gWidTopWidget is None:
                gsTopWidgetName=''
            else:
                gsTopWidgetName=gWidTopWidget.GetName()
        else:
            gWidTopWidget=None
            gsTopWidgetName=''
    except:
        gWidTopWidget=None
        gsTopWidgetName=''

if __init__.PYTHON_LOGGING==1:
    def vtLngInitConsole(lvConsole=-1):
        global lngConsole
        if lngConsole is None:
            console = logging.StreamHandler()
            if lvConsole==-1:
                console.setLevel(logging.DEBUG)
            else:
                console.setLevel(lvConsole)
            # set a format which is simpler for console use
            formatter = logging.Formatter('%(name)-18s %(levelname)-8s: %(message)s')
            # tell the handler to use this format
            console.setFormatter(formatter)
            logging.getLogger('').addHandler(console)
            lngConsole=console
    def vtLngStopConsole():
        global lngConsole
        if lngConsole is not None:
            logging.getLogger('').removeHandler(lngConsole)
            lngConsole=None
    def vtLngInit(applname,sFN,level,bConsole=False,lvConsole=-1,iSockPort=-1,bTime=False,
                encoding='UTF-8',bRollOver=True,sLogDN=None):
        global LEVEL
        global lngFileHandler
        global lngSockHandler
        global lngFN
        global lngPort
        vtLngNumReset()
        InitMsgWid()
        logging.getLogger('').setLevel(logging.DEBUG)
        if sLogDN is None:
            try:
                # search enviroment
                sLogDN=os.getenv('vidarcLogDN',None)
            except:
                sLogDN=None
            if sLogDN is None:
                sLogDN=vSystem.getLogDN()
        try:
            if lngFileHandler is None:
                if sLogDN is not None:
                    try:
                        os.makedirs(sLogDN)
                    except:
                        pass
                    lngFN=os.path.join(sLogDN,sFN)
                else:
                    lngFN=sFN
                lngActFN=lngFN
                if bTime:
                    lngFileHandler=logging.handlers.TimedRotatingFileHandler(lngFN,'H',2,backupCount=10)
                    lngFileHandler.setLevel(level)
                    formatter=logging.Formatter('%(asctime)s|%(name)-18s|%(levelname)-8s|%(message)s')
                    lngFileHandler.setFormatter(formatter)
                    logging.getLogger('').addHandler(lngFileHandler)
                    lngFileHandler.doRollover()
                else:
                    lngFileHandler=logging.handlers.RotatingFileHandler(lngFN,backupCount=3)
                    lngFileHandler.setLevel(level)
                    formatter=logging.Formatter('%(asctime)s|%(name)-18s|%(levelname)-8s|%(message)s')
                    lngFileHandler.setFormatter(formatter)
                    logging.getLogger('').addHandler(lngFileHandler)
                    lngFileHandler.doRollover()
        except:
            traceback.print_exc()
            logging.shutdown()
            try:
                logging.getLogger('').removeHandler(lngFileHandler)
            except:
                pass
            lngFileHandler=None
            return -1
    
        LEVEL=level
        if bConsole:
            if lvConsole==-1:
                lvConsole=level
            vtLngInitConsole(level)
        if iSockPort>=0:
            lngPort=str(iSockPort)
            if __init__.PYTHON_LOGGIN_SOCKET:
                #python socket loggin
                #global lngCfgSrv
                #lngCfgSrv=listen(iSockPort)
                if USE_LOGGING:
                    lngSockHandler=logging.handlers.SocketHandler('localhost',iSockPort)
                    lngSockHandler.setLevel(logging.DEBUG)
                    logging.getLogger('').addHandler(lngSockHandler)
            else:
                #vidarc logging
                lngSockHandler=vtLogNetSrv(None,iSockPort)
                if lngSockHandler.IsServing():
                #lngSockHandler.setLevel(logging.DEBUG)
                    lngSockHandler.Start()
                else:
                    return -2
        _log(INFO,'------------------------appl started-------------','',
                    bAcquire=True)
        return 0
    def vtLngStopFile():
        global lngFileHandler
        if lngFileHandler is not None:
            lngFileHandler.close()
            logging.getLogger('').removeHandler(lngFileHandler)
            lngFileHandler=None
    if __init__.PYTHON_LOGGIN_SOCKET:
        def vtLngStop():
            global lngSockHandler
            if lngSockHandler is not None:
                lngSockHandler.close()
    else:
        def vtLngStop():
            global lngSockHandler
            if lngSockHandler is not None:
                lngSockHandler.Close()
    def vtLngSetLevel(level):
        global LEVEL
        global lngFileHandler
        global lngSockHandler
        LEVEL=level
        if lngFileHandler is not None:
            lngFileHandler.setLevel(LEVEL)
        try:
            if lngSockHandler is not None:
                lngSockHandler.setLevel(LEVEL)
        except:
            pass
    def vtLngGetLevel():
        global LEVEL
        return LEVEL
    def vtLngGetFN():
        global lngActFN
        return lngActFN
    def vtLngGetPort():
        global lngPort
        return lngPort
    def vtLngIsLogged(lv):
        global LEVEL
        global lngSockHandler
        if lngSockHandler is not None:
            if lngSockHandler.GetConnectionsCount()>0:
                return True
        if lv>=LEVEL:
            return True
        return False
elif __init__.PYTHON_LOGGING==-1:
    def vtLngInitConsole(lvConsole=-1):
        return
    def vtLngStopConsole():
        return
    def vtLngInit(applname,sFN,level,bConsole=False,lvConsole=-1,iSockPort=-1,bTime=False,
                    encoding='UTF-8',bRollOver=True,sLogDN=None):
        global LEVEL
        global lngFN
        InitMsgWid()
        if sLogDN is None:
            try:
                # search enviroment
                sLogDN=os.getenv('vidarcLogDN',None)
            except:
                sLogDN=None
            if sLogDN is None:
                sLogDN=vSystem.getLogDN()
        if type(sFN)==types.UnicodeType:
            sFN=sFN.encode()
        if sFN.endswith('.log'):
            s=sFN[:-4]
            vpc.vLogSetUp(s,iSockPort)
        else:
            vpc.vLogSetUp(sFN,iSockPort)
        #vpc.vLogSetTarget(vpc.vLog_CONSOLE|vpc.vLog_FILE)
        vpc.vLogSetLevel(level)
        LEVEL=level
        #LEVEL=vpc.vLogGetLevel()
        lngFN=sFN
        _log(INFO,'------------------------appl started-------------','',
                    bAcquire=True)
        return 0
    def vtLngStopFile():
        global lngFileHandler
        if lngFileHandler is not None:
            lngFileHandler.close()
            lngFileHandler=None
    def vtLngStop():
        global lngSockHandler
        if lngSockHandler is not None:
            pass
    def vtLngSetLevel(level):
        global LEVEL
        global lngFileHandler
        global lngSockHandler
        vpc.vLogSetLevel(level)
        LEVEL=level
        #LEVEL=vpc.vLogGetLevel()
        try:
            if lngSockHandler is not None:
                lngSockHandler.setLevel(LEVEL)
        except:
            pass
    def vtLngGetLevel():
        global LEVEL
        return LEVEL
        return vpc.vLogGetLevel()
    def vtLngGetFN():
        sFN=vpc.vLogGetFN()
        return sFN
    def vtLngGetPort():
        global lngPort
        return lngPort
    def vtLngIsLogged(lv):
        global LEVEL
        global lngSockHandler
        if lngSockHandler is not None:
            if lngSockHandler.GetConnectionsCount()>0:
                return True
        if lv>=LEVEL:
            return True
        return False
else:
    ##
    ##  do not use python logging
    ##
    def vtLngInitConsole(lvConsole=-1):
        return
    def vtLngStopConsole():
        return
    def vtLngInit(applname,sFN,level,bConsole=False,lvConsole=-1,iSockPort=-1,bTime=False,
                    encoding='UTF-8',bRollOver=True,sLogDN=None):
        global LEVEL
        global lngFileHandler
        global lngSockHandler
        global lngFN
        global lngActFN
        global lngPort
        global _zNextRollover
        vtLngNumReset()
        InitMsgWid()
        if sLogDN is None:
            try:
                # search enviroment
                sLogDN=os.getenv('vidarcLogDN',None)
            except:
                sLogDN=None
            if sLogDN is None:
                sLogDN=vSystem.getLogDN()
        try:
            if lngFileHandler is None:
                if sLogDN is not None:
                    try:
                        os.makedirs(sLogDN)
                    except:
                        pass
                    lngFN=os.path.join(sLogDN,sFN)
                else:
                    lngFN=sFN
                if bTime:
                    sTmpFN,_zNextRollover=fnUtil.shiftFileTimed(lngFN,2,'H',backupCount=10)
                    _zNextRollover+=7200
                    lngFileHandler=codecs.open(sTmpFN, 'a', encoding)
                    lngActFN=sTmpFN
                else:
                    if bRollOver==True:
                        fnUtil.shiftFile(lngFN)
                        lngFileHandler=codecs.open(lngFN, 'w', encoding)
                    else:
                        lngFileHandler=codecs.open(lngFN, 'a', encoding)
                    lngActFN=lngFN
        except:
            traceback.print_exc()
            lngFileHandler=None
            return -1
        
        LEVEL=level
        _log(INFO,'------------------------appl started-------------','',
                    bAcquire=True)
        return 0
    def vtLngStopFile():
        global lngFileHandler
        if lngFileHandler is not None:
            lngFileHandler.close()
            lngFileHandler=None
    def vtLngStop():
        global lngSockHandler
        if lngSockHandler is not None:
            pass
    def vtLngSetLevel(level):
        global LEVEL
        global lngFileHandler
        global lngSockHandler
        LEVEL=level
        try:
            if lngSockHandler is not None:
                lngSockHandler.setLevel(LEVEL)
        except:
            pass
    def vtLngGetLevel():
        global LEVEL
        return LEVEL
    def vtLngGetFN():
        global lngActFN
        return lngActFN
    def vtLngGetPort():
        global lngPort
        return lngPort
    def vtLngIsLogged(lv):
        global LEVEL
        global lngSockHandler
        if lngSockHandler is not None:
            if lngSockHandler.GetConnectionsCount()>0:
                return True
        if lv>=LEVEL:
            return True
        return False

###############################

pp=pprint.PrettyPrinter(indent=2)
def pprint(obj):
    try:
        global pp
        pp.pprint(obj)
    except:
        pass
def pformat(obj):
    try:
        global pp
        return pp.pformat(obj)#.encode('ascii','replace')
    except:
        return ''
def fmtLimit(obj,iLimit=1,s=None,iLv=0,iLstLimit=5):
    if s is None:
        s=cStringIO.StringIO()
    t=type(obj)
    if t==types.ListType:
        for i in xrange(iLv):
            s.write('  ')
        s.write('[')
        i=0
        for o in obj:
            if i>iLstLimit:
                s.write('...')
                break
            tt=type(o)
            if tt==types.ListType:
                s.write('[..],')
            elif tt==types.DictType:
                s.write('{..},')
            else:
                s.write(__conv__(repr(o)))
                s.write(',')
            i+=1
        s.write(']')
    elif t==types.DictType:
        keys=obj.keys()
        keys.sort()
        for i in xrange(iLv):
            s.write('  ')
        s.write('{;')
        for k in keys:
            for i in xrange(iLv):
                s.write('  ')
            s.write(__conv__(repr(k)))
            s.write(':')
            o=obj[k]
            tt=type(o)
            if tt==types.ListType:
                if iLimit>0:
                    fmtLimit(o,iLimit-1,s,iLv=iLv+1)
                    s.write(',;')
                else:
                    s.write('[..];')
            elif tt==types.DictType:
                if iLimit>0:
                    s.write(';')
                    fmtLimit(obj[k],iLimit-1,s,iLv=iLv+1)
                    s.write(',;')
                else:
                    s.write('{..};')
            else:
                s.write(__conv__(repr(obj[k])))
                s.write(';')
            #if iLimit>0:
            #    fmtLimit(obj[k],iLimit-1,s,iLv=iLv+1)
        #s.write(';')
        for i in xrange(iLv):
            s.write('  ')
        s.write('};')
    else:
        s.write(__conv__(repr(obj)))
    if iLv==0:
        return s.getvalue()

def GetPrintMsgWid(wid,orig=None):
    if hasattr(wid,'_originPrint'):
        if orig is not None:
            orig._originPrint=wid._originPrint
        return wid._originPrint
    if orig is None:
        orig=wid
    if hasattr(wid,'PrintMsg'):
        if orig is not None:
            orig._originPrint=wid
        return wid
    else:
        par=wid.GetParent()
        if par is None:
            return None
        return GetPrintMsgWid(par,orig)
def PrintMsg(sMsg,wid=None,bForce=False):
    try:
        #return
        if wid is None:
            global gWidTopWidget
            wid=gWidTopWidget
            if wid is None:
                return
        wid=GetPrintMsgWid(wid)
        if wid is not None:
            wid.PrintMsg(sMsg,bForce=bForce)
    except:
        vtLngTB(__name__)
def SetProcBarVal(iVal,iCount,wid=None,bForce=False):
    try:
        #return
        if wid is None:
            global gWidTopWidget
            wid=gWidTopWidget
            if wid is None:
                return
        wid=GetPrintMsgWid(wid)
        if wid is not None:
            wid.SetProcBarVal(iVal,iCount,bForce=bForce)
    except:
        vtLngTB(__name__)

def testLv1():
    vtLngCallStack(None,logging.DEBUG,'test',verbose=1)    
def test():
    vtLngCallStack(None,logging.DEBUG,'test',verbose=1)
    testLv1()

def testO2Lv1():
    vtLngCallStack(None,logging.DEBUG,'test',verbose=1,origin='test')
def testO2():
    vtLngCallStack(None,logging.DEBUG,'test',verbose=1,origin='test')
    testO2Lv1()

def testO3():
    vtLngCallStack(None,logging.DEBUG,'debug',verbose=1,origin='possible')
    vtLngCallStack(None,logging.INFO,'info',verbose=1,origin='possible')
    vtLngCallStack(None,logging.WARN,'warn',verbose=1,origin='possible')
    vtLngCallStack(None,logging.WARNING,'warning',verbose=1,origin='possible')
    vtLngCallStack(None,logging.ERROR,'error',verbose=1,origin='possible')
    vtLngCallStack(None,logging.CRITICAL,'critical;multiline',verbose=1,origin='possible')
    vtLngCS(logging.CRITICAL,'critical;multiline',origin='possible')
    try:
        if i==1:
            pass
        raise Exception
    except:
        vtLngTB()

class vtLogOrigin:
    def __init__(self,origin=None,sSuffix=None):
        if origin is None:
            self._origin=self.__class__.__name__
        else:
            self._origin=origin[:]
        if sSuffix is not None:
            self._origin=u':'.join([self._origin,sSuffix])
    def GetOrigin(self):
        return self._origin

class vtLogMixIn:
    def __isLogDebug__(self):
        return vtLngIsLogged(DEBUG)
    def __isLogInfo__(self):
        return vtLngIsLogged(INFO)
    def __isLogWarn__(self):
        return vtLngIsLogged(WARN)
    def __isLogError__(self):
        return vtLngIsLogged(ERROR)
    def __isLogCritical__(self):
        return vtLngIsLogged(CRITICAL)
    def __logDebug__(self,msg='',verbose=0,level2skip=0):
        vtLngCur(DEBUG,msg,self.GetOrigin(),level2skip=level2skip+2)
    def __logInfo__(self,msg='',verbose=0,level2skip=0):
        vtLngCur(INFO,msg,self.GetOrigin(),level2skip=level2skip+2)
    def __logWarn__(self,msg='',verbose=0,level2skip=0):
        vtLngCur(WARN,msg,self.GetOrigin(),level2skip=level2skip+2)
    def __logError__(self,msg='',level2skip=0):
        vtLngCur(ERROR,msg,self.GetOrigin(),level2skip=level2skip+2)
    def __logCritical__(self,msg='',level2skip=0):
        vtLngCur(CRITICAL,msg,self.GetOrigin(),level2skip=level2skip+2)
    def __logFatal__(self,msg='',level2skip=0):
        vtLngCur(FATAL,msg,self.GetOrigin(),level2skip=level2skip+2)
    def __logTB__(self,bLogAsDbg=False):
        vtLngTB(self.GetOrigin(),bLogAsDbg=bLogAsDbg)
    def __logFmt__(self,l):
        return pformat(l)
    def __logFmtLm__(self,obj,iLimit=1,s=None,iLv=0,iLstLimit=5):
        return fmtLimit(obj,iLimit=iLimit,s=s,iLv=iLv,iLstLimit=iLstLimit)
    def __logConv__(self,s):
        return __conv__(s)
    def __logPrintMsg__(self,sMsg,wid=None,bForce=False):
        PrintMsg(sMsg,wid=wid or self,bForce=bForce)
    def __logSetProcBarVal__(self,iVal,iCount,wid=None,bForce=False):
        SetProcBarVal(iVal,iCount,wid=wid or self,bForce=False)
    def __logGetPrintMsgWid__(self,orig=None):
        return GetPrintMsgWid(self,orig=orig)

class vtLogOriginMixIn(vtLogMixIn,vtLogOrigin):
    pass

class vtLogOriginWX(vtLogMixIn,vtLogOrigin):
    def __init__(self,origin=None,bExtended=False):
        if origin is None:
            vtLngGetRelevantNamesWX(self,None)
        else:
            self._originLogging=origin[:]
        self._logMeasure={}
        if bExtended==True:
            self.widLogging=GetPrintMsgWid(self)
        else:
            #self.widLogging=None
            GetPrintMsgWid(self)
    def GetOrigin(self):
        return self._originLogging
    def GetTopWid(self,):
        return self._originPrint
    def CallTopWid(self,funcName,*args,**kwargs):
        if hasattr(self,'_originPrint')==False:
            self.__logDummyFunc__(funcName,*args,**kwargs)
        f=getattr(self._originPrint,funcName,None)
        if f is not None:
            f(*args,**kwargs)
        else:
            self.__logError__('method:%s not available'%(funcName))
    def GetTopWidFunc(self,name):
        if hasattr(self,'_originPrint')==False:
            return self.__logDummyFunc__
        return getattr(self._originPrint,name,self.__logDummyFunc__)
    def __logDummyFunc__(self,*args,**kwargs):
        self.__logError__('dummy func called;args;%s;kwargs;%s'%(self.__logFmt__(args),
                self.__logFmt__(kwargs)))
    def __logMeasure__(self,k,iNum=0):
        if vtLngIsLogged(INFO):
            if k not in self._logMeasure:
                l=[]
                self._logMeasure[k]=l
            else:
                l=self._logMeasure[k]
            zCPU=reduce(lambda x,y:x+y,os.times())
            l.append((time.time(),zCPU,iNum))
            if iNum<0:
                if k in self._logMeasure:
                    l=self._logMeasure[k]
                    zS=l[0][0]
                    zCPU=l[0][1]
                    ll=['%8d : %6.3f[s] (%6.3f[s])'%(iNum,zA-zS,zCpu-zCPU) for zA,zCpu,iNum in l]
                    self.__logInfo__('measure;%s'%(';'.join(ll)),level2skip=1)
                    del self._logMeasure[k]

if __name__=='__main__':
    i=1
    if i==0:
        vtLngInit('test','test.log',logging.DEBUG,bConsole=True)
        test()
        testO2()
        testO3()
    elif i==1:
        vtLngInit('test','test2.log',logging.DEBUG,bConsole=False,iSockPort=60000)
        import vtLogSockClt
        import time
        vtLogSockClt.thdMain()
        time.sleep(1)
        for i in range(10):
            test()
            testO2()
            testO3()
            #time.sleep(1)
        time.sleep(2)
        vtLngStop()
        