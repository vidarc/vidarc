#!/usr/bin/env python
#Boa:App:BoaApp
#----------------------------------------------------------------------------
# Name:         vtLogSockViewerAppl.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtLogSockViewerAppl.py,v 1.1 2005/12/11 23:04:49 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import os,sys,getopt
import vtLogSockViewerFrame
import vidarc.tool.lang.vtLgBase as vtLgBase

modules ={u'vtLogSockViewerFrame': [1,
                           'Main frame of Application',
                           u'vtLogSockViewerFrame.py']}

class BoaApp(wx.App):
    def OnInit(self):
        wx.InitAllImageHandlers()
        self.main = vtLogSockViewerFrame.create(None)
        self.main.Show()
        self.SetTopWindow(self.main)
        return True
    def OpenFile(self,fn):
        self.main.OpenFile(fn)
    
def showHelp():
    print _('help')
    print sys.version
    print "  ",_("valid flags:")
    print "  ",_("-f <filename>")
    print "  ",_("--file <filename>")
    print "        ",_("open the <filename> at start")
    print "  ","-h"
    print "  ",_("--help")
    print "        ",_("show this screen")
    print "  ","-l"
    print "  ",_("--lang <language id according ISO639>")

def main():
    optshort,optlong='l:f:c:h',['lang=','file=','help']
    vtLgBase.initAppl(optshort,optlong,'log')
    
    fn=None
    try:
        optlist , args = getopt.getopt(sys.argv[1:],'l:f:h',['lang=','file=','help'])
        for o in optlist:
            if o[0] in ['-h','--help']:
                showHelp()
            
                return
            if o[0] in ['--file','-f']:
                fn=o[1]
    except:
        showHelp()
    
    application = BoaApp(0)
    if fn is not None:
        application.OpenFile(fn)
    application.MainLoop()

if __name__ == '__main__':
    main()
