#----------------------------------------------------------------------------
# Name:         vtLogSrv.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtLogSrv.py,v 1.5 2007/01/30 15:45:51 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import logging
import socket,random
import time,random,string
import socket,sha,binascii
import thread,threading,traceback
#from vidarc.tool.net import vtNetSock
#from vidarc.tool.net.vtNetSock import vtSock
#import vidarc.tool.net.vtNetSock as vtNetSock
from vidarc.tool.sock.vtSock import vtSock

SESSION_ID_CHARS=string.ascii_letters+string.digits
SESSION_ID_CHARS_LEN=len(SESSION_ID_CHARS)
FORCE_SHUTDOWN=20

class vtLogSrvSock(vtSock):
    def __init__(self,server,conn,adr,sID='',verbose=0):
        vtSock.__init__(self,server,conn,adr)
        
        self.loggedin=False
        self.objLogin=None
        self.par=None
        self.cmds=[]
        self.logHndlr = logging.StreamHandler(self)
        self.logHndlr.setLevel(logging.DEBUG)
        # set a format which is simpler for console use
        formatter = logging.Formatter('%(asctime)s|%(name)-18s|%(levelname)-8s|%(message)s')
        # tell the handler to use this format
        self.logHndlr.setFormatter(formatter)
        logging.getLogger('').addHandler(self.logHndlr)
        self.Send('test')
    def HandleData(self):
        try:
            self.Send(self.data)
            self.data=''
            self.len=0
        except:
            self.Stop()
    def SocketClosed(self):
        vtSock.SocketClosed(self)
        #vtLog.CallStack('')
        logging.getLogger('').removeHandler(self.logHndlr)
    def write(self,arg):
        try:
            self.SendTelegram(arg)
        except:
            self.Stop()
    def flush(self):
        pass
class vtLogNetSrv:
    def __init__(self,server,port,socketClass=vtLogSrvSock,verbose=0):
        self.srv=server
        self.port=port
        self.serving=False
        self.stopping=False
        self.socketClass=socketClass
        self.passwd=None
        self.connections={}
        self.iConnections=0
        self.verbose=verbose
        
        self.serving=True
        self.__createSock__()
    def GenerateSessionID(self):
        sessionID=''
        for l in [0,1,2,3]:
            f=random.random()
            s=repr(f)[2:]
            iLen=len(s)
            if iLen<16:
                s+='4539869453023104356456983145'[:16-iLen]
            i=0
            while i<16:
                j=int(s[i:i+2])%SESSION_ID_CHARS_LEN
                sessionID+=SESSION_ID_CHARS[j]
                i+=2
        return sessionID
    def SetPasswd(self,passwd):
        if self.passwd!=passwd:
            self.Close()
        self.passwd=passwd
    def Start(self):
        if not self.serving:
            return
        if self.stopping:
            return
        self.serving=True
        self.stopping=False
        self.connections={}
        self.thdID=thread.start_new_thread(self.Run, ())
    def Stop(self):
        try:
            thread.exit_thread(self.thdID)
            self.socket.close()
        except:
            pass
        self.stopping=True
        try:
            for v in self.connections.values():
                v.Stop()
        except:
            pass
    def Close(self):
        for v in self.connections.values():
            v.Close()
        self.connections={}
        self.iConnections=0
        self.serving=False
    def IsServing(self):
        return self.serving
    def StopSocket(self,adr,sockInst):
        try:
            del self.connections[adr]
        except:
            pass
        self.iConnections-=1
        if self.iConnections<0:
            self.iConnections=0
    def __createSock__(self):
        HOST='localhost'
        for res in socket.getaddrinfo(HOST, self.port, socket.AF_UNSPEC, socket.SOCK_STREAM, 0, socket.AI_PASSIVE):
            af, socktype, proto, canonname, sa = res
            try:
                self.socket = socket.socket(af, socktype, proto)
            except socket.error, msg:
                self.socket = None
                #traceback.print_exc()
                continue
            try:
                self.socket.bind(sa)
                self.socket.listen(1)
            except socket.error, msg:
                self.socket.close()
                self.socket = None
                #traceback.print_exc()
                continue
            if self.socket is None:
                self.serving=False
            break
        if self.socket is None:
            self.serving=False
    def Run(self):
        while self.serving:
            if self.stopping:
                break
            try:
                conn, addr = self.socket.accept()
                sID=self.GenerateSessionID()
                sock=self.socketClass(self,conn,addr,sID)
                sock.SendSessionID()
                self.connections[addr]=sock
                self.iConnections+=1
            except:
                traceback.print_exc()
                time.sleep(1)
                pass
        self.serving=False
    def GetConnectionsCount(self):
        return self.iConnections
        #return len(self.connections.keys())
    def GetConnectionsStr(self):
        s=''
        keys=self.connections.keys()
        def compFunc(a,b):
            i=cmp(a[0],b[0])
            if i==0:
                return a[1]-b[1]
            return i
        keys.sort(compFunc)
        lst=[]
        for k in keys:
            sock=self.connections[k]
            lst.append(k[0]+';'+str(k[1])+';'+str(sock.received)+';'+str(sock.send)+';'+sock.zConn)
        return string.join(lst,',')
    
        