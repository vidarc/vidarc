#Boa:Frame:vtLogSockViewerFrame
#----------------------------------------------------------------------------
# Name:         vtLogSockViewerFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtLogSockViewerFrame.py,v 1.7 2006/08/29 10:06:22 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import cPickle
import logging,logging.handlers
import SocketServer
import struct
import thread,time

import wx
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons
import wx.lib.foldpanelbar as fpb

import string,os,time,sys,platform
import thread,threading,traceback
from vidarc.tool.log.vtLogClt import *
import images

MAP_NR_TO_LEVEL_NAME=['DEBUG','INFO','WARNING','WARNING','ERROR','CRITICAL','FATAL']

def GetCollapsedIconData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\x8eIDAT8\x8d\xa5\x93-n\xe4@\x10\x85?g\x03\n6lh)\xc4\xd2\x12\xc3\x81\
\xd6\xa2I\x90\x154\xb9\x81\x8f1G\xc8\x11\x16\x86\xcd\xa0\x99F\xb3A\x91\xa1\
\xc9J&\x96L"5lX\xcc\x0bl\xf7v\xb2\x7fZ\xa5\x98\xebU\xbdz\xf5\\\x9deW\x9f\xf8\
H\\\xbfO|{y\x9dT\x15P\x04\x01\x01UPUD\x84\xdb/7YZ\x9f\xa5\n\xce\x97aRU\x8a\
\xdc`\xacA\x00\x04P\xf0!0\xf6\x81\xa0\xf0p\xff9\xfb\x85\xe0|\x19&T)K\x8b\x18\
\xf9\xa3\xe4\xbe\xf3\x8c^#\xc9\xd5\n\xa8*\xc5?\x9a\x01\x8a\xd2b\r\x1cN\xc3\
\x14\t\xce\x97a\xb2F0Ks\xd58\xaa\xc6\xc5\xa6\xf7\xdfya\xe7\xbdR\x13M2\xf9\
\xf9qKQ\x1fi\xf6-\x00~T\xfac\x1dq#\x82,\xe5q\x05\x91D\xba@\xefj\xba1\xf0\xdc\
zzW\xcff&\xb8,\x89\xa8@Q\xd6\xaaf\xdfRm,\xee\xb1BDxr#\xae\xf5|\xddo\xd6\xe2H\
\x18\x15\x84\xa0q@]\xe54\x8d\xa3\xedf\x05M\xe3\xd8Uy\xc4\x15\x8d\xf5\xd7\x8b\
~\x82\x0fh\x0e"\xb0\xad,\xee\xb8c\xbb\x18\xe7\x8e;6\xa5\x89\x04\xde\xff\x1c\
\x16\xef\xe0p\xfa>\x19\x11\xca\x8d\x8d\xe0\x93\x1b\x01\xd8m\xf3(;x\xa5\xef=\
\xb7w\xf3\x1d$\x7f\xc1\xe0\xbd\xa7\xeb\xa0(,"Kc\x12\xc1+\xfd\xe8\tI\xee\xed)\
\xbf\xbcN\xc1{D\x04k\x05#\x12\xfd\xf2a\xde[\x81\x87\xbb\xdf\x9cr\x1a\x87\xd3\
0)\xba>\x83\xd5\xb97o\xe0\xaf\x04\xff\x13?\x00\xd2\xfb\xa9`z\xac\x80w\x00\
\x00\x00\x00IEND\xaeB`\x82' 

def GetCollapsedIconBitmap():
    return wx.BitmapFromImage(GetCollapsedIconImage())

def GetCollapsedIconImage():
    import cStringIO
    stream = cStringIO.StringIO(GetCollapsedIconData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def GetExpandedIconData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\x9fIDAT8\x8d\x95\x93\xa1\x8e\xdc0\x14EO\xb2\xc4\xd0\xd2\x12\xb7(mI\
\xa4%V\xd1lQT4[4-\x9a\xfe\xc1\xc2|\xc6\xc2~BY\x83:A3E\xd3\xa0*\xa4\xd2\x90H!\
\x95\x0c\r\r\x1fK\x81g\xb2\x99\x84\xb4\x0fY\xd6\xbb\xc7\xf7>=\'Iz\xc3\xbcv\
\xfbn\xb8\x9c\x15 \xe7\xf3\xc7\x0fw\xc9\xbc7\x99\x03\x0e\xfbn0\x99F+\x85R\
\x80RH\x10\x82\x08\xde\x05\x1ef\x90+\xc0\xe1\xd8\ryn\xd0Z-\\A\xb4\xd2\xf7\
\x9e\xfbwoF\xc8\x088\x1c\xbbae\xb3\xe8y&\x9a\xdf\xf5\xbd\xe7\xfem\x84\xa4\
\x97\xccYf\x16\x8d\xdb\xb2a]\xfeX\x18\xc9s\xc3\xe1\x18\xe7\x94\x12cb\xcc\xb5\
\xfa\xb1l8\xf5\x01\xe7\x84\xc7\xb2Y@\xb2\xcc0\x02\xb4\x9a\x88%\xbe\xdc\xb4\
\x9e\xb6Zs\xaa74\xadg[6\x88<\xb7]\xc6\x14\x1dL\x86\xe6\x83\xa0\x81\xba\xda\
\x10\x02x/\xd4\xd5\x06\r\x840!\x9c\x1fM\x92\xf4\x86\x9f\xbf\xfe\x0c\xd6\x9ae\
\xd6u\x8d \xf4\xf5\x165\x9b\x8f\x04\xe1\xc5\xcb\xdb$\x05\x90\xa97@\x04lQas\
\xcd*7\x14\xdb\x9aY\xcb\xb8\\\xe9E\x10|\xbc\xf2^\xb0E\x85\xc95_\x9f\n\xaa/\
\x05\x10\x81\xce\xc9\xa8\xf6><G\xd8\xed\xbbA)X\xd9\x0c\x01\x9a\xc6Q\x14\xd9h\
[\x04\xda\xd6c\xadFkE\xf0\xc2\xab\xd7\xb7\xc9\x08\x00\xf8\xf6\xbd\x1b\x8cQ\
\xd8|\xb9\x0f\xd3\x9a\x8a\xc7\x08\x00\x9f?\xdd%\xde\x07\xda\x93\xc3{\x19C\
\x8a\x9c\x03\x0b8\x17\xe8\x9d\xbf\x02.>\x13\xc0n\xff{PJ\xc5\xfdP\x11""<\xbc\
\xff\x87\xdf\xf8\xbf\xf5\x17FF\xaf\x8f\x8b\xd3\xe6K\x00\x00\x00\x00IEND\xaeB\
`\x82' 

def GetExpandedIconBitmap():
    return wx.BitmapFromImage(GetExpandedIconImage())

def GetExpandedIconImage():
    import cStringIO
    stream = cStringIO.StringIO(GetExpandedIconData())
    return wx.ImageFromStream(stream)

class LogRecordStreamHandler(SocketServer.StreamRequestHandler):
    """Handler for a streaming logging request.

    This basically logs the record using whatever logging policy is
    configured locally.
    """

    def handle(self):
        """
        Handle multiple requests - each expected to be a 4-byte length,
        followed by the LogRecord in pickle format. Logs the record
        according to whatever policy is configured locally.
        """
        while 1:
            chunk = self.connection.recv(4)
            if len(chunk) < 4:
                break
            slen = struct.unpack(">L", chunk)[0]
            chunk = self.connection.recv(slen)
            while len(chunk) < slen:
                chunk = chunk + self.connection.recv(slen - len(chunk))
            #print 'received',slen
            obj = self.unPickle(chunk)
            record = logging.makeLogRecord(obj)
            self.handleLogRecord(record)

    def unPickle(self, data):
        return cPickle.loads(data)

    def handleLogRecord(self, record):
        #print 'handleLogRecord',record
        #print record.__dict__
        sTime=time.strftime('%Y-%m-%d %H:%M:%S,',time.localtime(record.created))+str(int(record.msecs))
        try:
            par=self.server.par
            if par.AddOrigin(record.name):
                par.AddMsg(record.name,record.levelname,record.message,sTime)
        except:
            print ' no parent'
        #print '%s origin:%s %s'%(sTime,record.name,record.message)
        # if a name is specified, we use the named logger rather than the one
        # implied by the record.
        #if self.server.logname is not None:
        #    name = self.server.logname
        #else:
        #    name = record.name
        #logger = logging.getLogger(name)
        # N.B. EVERY record gets logged. This is because Logger.handle
        # is normally called AFTER logger-level filtering. If you want
        # to do filtering, do it at the client end to save wasting
        # cycles and network bandwidth!
        #logger.handle(record)

class LogRecordSocketReceiver(SocketServer.ThreadingTCPServer):
    """simple TCP socket-based logging receiver suitable for testing.
    """

    allow_reuse_address = 1

    def __init__(self, par, host='localhost',
                 port=logging.handlers.DEFAULT_TCP_LOGGING_PORT,
                 handler=LogRecordStreamHandler):
        SocketServer.ThreadingTCPServer.__init__(self, (host, port), handler)
        self.par=par
        self.abort = 0
        self.stopped=0
        self.timeout = 1
        self.logname = None

    def serve_until_stopped(self):
        import select
        abort = 0
        print 'serve'
        while not abort:
            rd, wr, ex = select.select([self.socket.fileno()],
                                       [], [],
                                       self.timeout)
            if rd:
                self.handle_request()
            abort = self.abort
        self.stopped=1
        print '\n\nstopped\n\n'
    
class vtLogViewerSock(vtLogCltSock):
    def doMessage(self,msg):
        if self.par is not None:
            strs=string.split(msg,'|')
            try:
                if self.par.AddOrigin(strs[1]):
                    self.par.AddMsg(strs[1],strs[2],strs[3],strs[0],strs[4])
            except:
                pass
            pass
        
def create(parent):
    return vtLogSockViewerFrame(parent)

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplSockBitmap())
    return icon

[wxID_VTLOGSOCKVIEWERFRAME, wxID_VTLOGSOCKVIEWERFRAMECBCLR, 
 wxID_VTLOGSOCKVIEWERFRAMECBFIND, wxID_VTLOGSOCKVIEWERFRAMECHCFINDLEVEL, 
 wxID_VTLOGSOCKVIEWERFRAMECHCFINDORIGIN, 
 wxID_VTLOGSOCKVIEWERFRAMELBLFINDLEVEL, wxID_VTLOGSOCKVIEWERFRAMELBLFINDMSG, 
 wxID_VTLOGSOCKVIEWERFRAMELBLFINDORIGIN, 
 wxID_VTLOGSOCKVIEWERFRAMELSTCALLSTACK, wxID_VTLOGSOCKVIEWERFRAMELSTLOGS, 
 wxID_VTLOGSOCKVIEWERFRAMENBINFO, wxID_VTLOGSOCKVIEWERFRAMEPNMAIN, 
 wxID_VTLOGSOCKVIEWERFRAMEPNTOP, wxID_VTLOGSOCKVIEWERFRAMESBSTATUS, 
 wxID_VTLOGSOCKVIEWERFRAMESLWNAV, wxID_VTLOGSOCKVIEWERFRAMESLWTOP, 
 wxID_VTLOGSOCKVIEWERFRAMETGFINDDIR, wxID_VTLOGSOCKVIEWERFRAMETGORIGINREFRESH, 
 wxID_VTLOGSOCKVIEWERFRAMETXTFINDMSG, wxID_VTLOGSOCKVIEWERFRAMETXTMSG, 
] = [wx.NewId() for _init_ctrls in range(20)]

[wxID_VTLOGSOCKVIEWERFRAMEMNFILEMN_FILE_EXIT, 
 wxID_VTLOGSOCKVIEWERFRAMEMNFILEMN_FILE_OPEN, 
 wxID_VTLOGSOCKVIEWERFRAMEMNFILEMN_FILE_OPEN_CFG, 
 wxID_VTLOGSOCKVIEWERFRAMEMNFILEMN_FILE_SAVE, 
 wxID_VTLOGSOCKVIEWERFRAMEMNFILEMN_FILE_SAVE_CFG, 
] = [wx.NewId() for _init_coll_mnFile_Items in range(5)]


STATUS_PROCESS_POS=0
STATUS_MSG_POS=1
STATUS_CLOCK_POS=2

class vtLogSockViewerFrame(wx.Frame):
    MAP={u'vSrv':60000, u'vHum':60001, u'vLoc':60002, u'vDoc':60003,
        u'vPrj':60004, u'vPrjDoc':60005, u'vPrjEng':60006, u'vPrjTimer':60007,
        u'vCustomer':60008,u'vPrjPlan':60009,u'vRessource':60010,
        u'vSoftware':60011,u'vSupplier':60012,u'vPurchase':60013,
        u'vBill':60014,u'vOffer':60015,u'vContact':60016,
        u'vEngineering':60020,u'vTask':60021,
        u'vPlugIn':60088,u'vMsg':60089,
        u'vGlobals':60090,
        u'vMsgSrv':60095,u'vPlugInSrv':60096,u'vMESSrvSvc':60097,u'vMESSrv':60098,u'vMES':60099}
    def _init_coll_bxsFind_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.tgOriginRefresh, 0, border=4, flag=wx.RIGHT)
        parent.AddWindow(self.lblFindOrigin, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.chcFindOrigin, 2, border=4,
              flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblFindLevel, 1, border=4,
              flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.chcFindLevel, 1, border=4,
              flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.tgFindDir, 0, border=4, flag=wx.LEFT)

    def _init_coll_bxsFindMsg_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbClr, 0, border=4, flag=wx.RIGHT)
        parent.AddWindow(self.lblFindMsg, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtFindMsg, 4, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbFind, 0, border=4, flag=wx.LEFT)

    def _init_coll_fgsMain_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsFind, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsFindMsg, 0, border=4, flag=wx.ALL | wx.EXPAND)
        parent.AddWindow(self.lstLogs, 0, border=4, flag=wx.ALL | wx.EXPAND)

    def _init_coll_fgsMain_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_mnFile_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_VTLOGSOCKVIEWERFRAMEMNFILEMN_FILE_OPEN,
              kind=wx.ITEM_NORMAL, text=_(u'Open\tCTRL+O'))
        parent.Append(help='', id=wxID_VTLOGSOCKVIEWERFRAMEMNFILEMN_FILE_SAVE,
              kind=wx.ITEM_NORMAL, text=_(u'Save\tCTRL+S'))
        parent.AppendSeparator()
        parent.Append(help='',
              id=wxID_VTLOGSOCKVIEWERFRAMEMNFILEMN_FILE_OPEN_CFG,
              kind=wx.ITEM_NORMAL, text=_(u'Open Config\tCTRL+ALT+O'))
        parent.Append(help='',
              id=wxID_VTLOGSOCKVIEWERFRAMEMNFILEMN_FILE_SAVE_CFG,
              kind=wx.ITEM_NORMAL, text=_(u'Save Config\tCTRL+ALT+S'))
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VTLOGSOCKVIEWERFRAMEMNFILEMN_FILE_EXIT,
              kind=wx.ITEM_NORMAL, text=_('E&xit\tALT+X'))
        self.Bind(wx.EVT_MENU, self.OnMnFileMn_file_saveMenu,
              id=wxID_VTLOGSOCKVIEWERFRAMEMNFILEMN_FILE_SAVE)
        self.Bind(wx.EVT_MENU, self.OnMnFileMn_file_exitMenu,
              id=wxID_VTLOGSOCKVIEWERFRAMEMNFILEMN_FILE_EXIT)
        self.Bind(wx.EVT_MENU, self.OnMnFileMn_file_openMenu,
              id=wxID_VTLOGSOCKVIEWERFRAMEMNFILEMN_FILE_OPEN)
        self.Bind(wx.EVT_MENU, self.OnMnFileMn_file_open_cfgMenu,
              id=wxID_VTLOGSOCKVIEWERFRAMEMNFILEMN_FILE_OPEN_CFG)
        self.Bind(wx.EVT_MENU, self.OnMnFileMn_file_save_cfgMenu,
              id=wxID_VTLOGSOCKVIEWERFRAMEMNFILEMN_FILE_SAVE_CFG)

    def _init_coll_mnBar_Menus(self, parent):
        # generated method, don't edit

        parent.Append(menu=self.mnFile, title=_(u'&File'))

    def _init_coll_sbStatus_Fields(self, parent):
        # generated method, don't edit
        parent.SetFieldsCount(3)

        parent.SetStatusText(number=0, text=u'')
        parent.SetStatusText(number=1, text=u'')
        parent.SetStatusText(number=2, text=u'')

        parent.SetStatusWidths([100, -1, 200])

    def _init_coll_nbInfo_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.lstCallStack, select=False,
              text=_(u'Call Stack'))
        parent.AddPage(imageId=-1, page=self.txtMsg, select=True,
              text=_(u'Message'))

    def _init_utils(self):
        # generated method, don't edit
        self.mnBar = wx.MenuBar()

        self.mnFile = wx.Menu(title=u'')

        self._init_coll_mnBar_Menus(self.mnBar)
        self._init_coll_mnFile_Items(self.mnFile)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsMain = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self.bxsFind = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsFindMsg = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsMain_Items(self.fgsMain)
        self._init_coll_fgsMain_Growables(self.fgsMain)
        self._init_coll_bxsFind_Items(self.bxsFind)
        self._init_coll_bxsFindMsg_Items(self.bxsFindMsg)

        self.pnTop.SetSizer(self.fgsMain)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VTLOGSOCKVIEWERFRAME,
              name=u'vtLogSockViewerFrame', parent=prnt, pos=wx.Point(25, 19),
              size=wx.Size(574, 608), style=wx.DEFAULT_FRAME_STYLE,
              title=u'vtLogSockViewerFrame')
        self._init_utils()
        self.SetClientSize(wx.Size(566, 581))
        self.SetMenuBar(self.mnBar)
        self.Bind(wx.EVT_SIZE, self.OnVtLogSockViewerFrameSize)
        self.Bind(wx.EVT_CLOSE, self.OnVtLogSockViewerFrameClose)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VTLOGSOCKVIEWERFRAMESLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 0), size=wx.Size(144,
              560), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetDefaultSize(wx.Size(120, 1000))
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VTLOGSOCKVIEWERFRAMESLWNAV)

        self.slwTop = wx.SashLayoutWindow(id=wxID_VTLOGSOCKVIEWERFRAMESLWTOP,
              name=u'slwTop', parent=self, pos=wx.Point(144, 0),
              size=wx.Size(424, 392), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwTop.SetDefaultSize((1000, 392))
        self.slwTop.SetOrientation(wx.LAYOUT_HORIZONTAL)
        self.slwTop.SetAlignment(wx.LAYOUT_TOP)
        self.slwTop.SetSashVisible(wx.SASH_BOTTOM, True)
        self.slwTop.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwTopSashDragged,
              id=wxID_VTLOGSOCKVIEWERFRAMESLWTOP)

        self.pnMain = wx.Panel(id=wxID_VTLOGSOCKVIEWERFRAMEPNMAIN,
              name=u'pnMain', parent=self, pos=wx.Point(152, 400),
              size=wx.Size(408, 152), style=wx.TAB_TRAVERSAL)
        self.pnMain.SetAutoLayout(True)

        self.nbInfo = wx.Notebook(id=wxID_VTLOGSOCKVIEWERFRAMENBINFO,
              name=u'nbInfo', parent=self.pnMain, pos=wx.Point(8, 8),
              size=wx.Size(392, 136), style=0)
        self.nbInfo.SetConstraints(LayoutAnchors(self.nbInfo, True, True, True,
              True))

        self.lstCallStack = wx.ListCtrl(id=wxID_VTLOGSOCKVIEWERFRAMELSTCALLSTACK,
              name=u'lstCallStack', parent=self.nbInfo, pos=wx.Point(0, 0),
              size=wx.Size(384, 110), style=wx.LC_REPORT)
        self.lstCallStack.SetConstraints(LayoutAnchors(self.lstCallStack, True,
              False, True, True))

        self.txtMsg = wx.TextCtrl(id=wxID_VTLOGSOCKVIEWERFRAMETXTMSG,
              name=u'txtMsg', parent=self.nbInfo, pos=wx.Point(0, 0),
              size=wx.Size(384, 110), style=wx.TE_MULTILINE, value=u'')
        self.txtMsg.SetFont(wx.Font(10, wx.SWISS, wx.NORMAL, wx.NORMAL, False,
              u'Courier New'))

        self.sbStatus = wx.StatusBar(id=wxID_VTLOGSOCKVIEWERFRAMESBSTATUS,
              name=u'sbStatus', parent=self, style=0)
        self.sbStatus.SetFieldsCount(2)
        self._init_coll_sbStatus_Fields(self.sbStatus)
        self.SetStatusBar(self.sbStatus)

        self.pnTop = wx.Panel(id=wxID_VTLOGSOCKVIEWERFRAMEPNTOP, name=u'pnTop',
              parent=self.slwTop, pos=wx.Point(0, 0), size=wx.Size(416, 384),
              style=wx.TAB_TRAVERSAL)

        self.lstLogs = wx.ListCtrl(id=wxID_VTLOGSOCKVIEWERFRAMELSTLOGS,
              name=u'lstLogs', parent=self.pnTop, pos=wx.Point(4, 76),
              size=wx.Size(408, 304), style=wx.LC_REPORT)
        self.lstLogs.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstLogsListItemSelected,
              id=wxID_VTLOGSOCKVIEWERFRAMELSTLOGS)
        self.lstLogs.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstLogsListItemDeselected,
              id=wxID_VTLOGSOCKVIEWERFRAMELSTLOGS)

        self.lblFindOrigin = wx.StaticText(id=wxID_VTLOGSOCKVIEWERFRAMELBLFINDORIGIN,
              label=u'origin', name=u'lblFindOrigin', parent=self.pnTop,
              pos=wx.Point(39, 4), size=wx.Size(67, 30), style=wx.ALIGN_RIGHT)
        self.lblFindOrigin.SetMinSize(wx.Size(-1, -1))

        self.chcFindOrigin = wx.Choice(choices=['---'],
              id=wxID_VTLOGSOCKVIEWERFRAMECHCFINDORIGIN, name=u'chcFindOrigin',
              parent=self.pnTop, pos=wx.Point(110, 4), size=wx.Size(131, 21),
              style=0)

        self.lblFindLevel = wx.StaticText(id=wxID_VTLOGSOCKVIEWERFRAMELBLFINDLEVEL,
              label=u'level', name=u'lblFindLevel', parent=self.pnTop,
              pos=wx.Point(245, 4), size=wx.Size(63, 30), style=wx.ALIGN_RIGHT)

        self.chcFindLevel = wx.Choice(choices=['---', u'debug', u'info',
              u'warn', u'warning', u'error', u'critical', u'fatal'],
              id=wxID_VTLOGSOCKVIEWERFRAMECHCFINDLEVEL, name=u'chcFindLevel',
              parent=self.pnTop, pos=wx.Point(312, 4), size=wx.Size(63, 21),
              style=0)

        self.tgFindDir = wx.lib.buttons.GenBitmapToggleButton(ID=wxID_VTLOGSOCKVIEWERFRAMETGFINDDIR,
              bitmap=wx.EmptyBitmap(16, 16), name=u'tgFindDir',
              parent=self.pnTop, pos=wx.Point(379, 4), size=wx.Size(31, 30),
              style=0)

        self.lblFindMsg = wx.StaticText(id=wxID_VTLOGSOCKVIEWERFRAMELBLFINDMSG,
              label=u'message', name=u'lblFindMsg', parent=self.pnTop,
              pos=wx.Point(39, 38), size=wx.Size(67, 30), style=wx.ALIGN_RIGHT)
        self.lblFindMsg.SetMinSize(wx.Size(-1, -1))

        self.txtFindMsg = wx.TextCtrl(id=wxID_VTLOGSOCKVIEWERFRAMETXTFINDMSG,
              name=u'txtFindMsg', parent=self.pnTop, pos=wx.Point(110, 38),
              size=wx.Size(266, 30), style=0, value=u'')

        self.cbFind = wx.lib.buttons.GenBitmapButton(ID=wxID_VTLOGSOCKVIEWERFRAMECBFIND,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbFind', parent=self.pnTop,
              pos=wx.Point(380, 38), size=wx.Size(31, 30), style=0)
        self.cbFind.Bind(wx.EVT_BUTTON, self.OnCbFindButton,
              id=wxID_VTLOGSOCKVIEWERFRAMECBFIND)

        self.cbClr = wx.lib.buttons.GenBitmapButton(ID=wxID_VTLOGSOCKVIEWERFRAMECBCLR,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbClr', parent=self.pnTop,
              pos=wx.Point(4, 38), size=wx.Size(31, 30), style=0)
        self.cbClr.Bind(wx.EVT_BUTTON, self.OnCbClrButton,
              id=wxID_VTLOGSOCKVIEWERFRAMECBCLR)

        self.tgOriginRefresh = wx.lib.buttons.GenBitmapToggleButton(ID=wxID_VTLOGSOCKVIEWERFRAMETGORIGINREFRESH,
              bitmap=wx.EmptyBitmap(16, 16), name=u'tgOriginRefresh',
              parent=self.pnTop, pos=wx.Point(4, 4), size=wx.Size(31, 30),
              style=0)

        self._init_coll_nbInfo_Pages(self.nbInfo)

        self._init_sizers()

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.bModified=False
        self.iSockPort=-1
        self.idxSel=-1
        self.fn=None
        self.lngReceiver=None
        self.sFN='.'
        
        # setup statusbar
        self.timer = wx.PyTimer(self.Notify)
        self.timer.Start(1000)
        self.Notify()
        
        icon = getApplicationIcon()
        self.SetIcon(icon)

        col2 = wx.Colour(46, 138,87)
        col1 = wx.Colour(167, 206,184)

        style = fpb.CaptionBarStyle()
        style.SetFirstColour(col1)
        style.SetSecondColour(col2)
        style.SetCaptionStyle(fpb.CAPTIONBAR_GRADIENT_H)
        self.fldNav = fpb.FoldPanelBar(self.slwNav, -1, wx.DefaultPosition,
                                     wx.Size(-1,-1), fpb.CAPTIONBAR_GRADIENT_H, 0)
        Images = wx.ImageList(16,16)
        Images.Add(GetExpandedIconBitmap())
        Images.Add(GetCollapsedIconBitmap())

        item = self.fldNav.AddFoldPanel(_(u"connection"), collapsed=False,
                                      foldIcons=Images,cbstyle=style)
        self.chcAppl = wx.Choice(choices=[u'vMES', u'vMESSrv', u'vMESSrvSvc',
              u'vSrv', u'vHum', u'vLoc', u'vDoc', u'vPrj', u'vPrjDoc',
              u'vPrjEng', u'vPrjTimer', u'vCustomer', u'vPrjPlan',
              u'vPrjRessource', u'vSoftware', u'vSupplier', u'vPurchase',
              u'vBill', u'vOffer', u'vContact', u'vEngineering', u'vPlugIn',
              u'vMsg', u'vGlobals', u'vMsgSrv', u'vPlugInSrv'],
              id=-1, name=u'chcAppl',
              parent=item, pos=wx.DefaultPosition, size=wx.DefaultSize,
              style=0)
        self.chcAppl.Bind(wx.EVT_CHOICE, self.OnChcApplChoice)
        self.fldNav.AddFoldPanelWindow(item, self.chcAppl,fpb.FPB_ALIGN_WIDTH, 5, 0) 
        self.lblHost = wx.StaticText(id=-1,
              label=_(u'Host'), name=u'lblHost', parent=item,
              pos=wx.DefaultPosition, size=wx.DefaultSize, style=0)
        self.fldNav.AddFoldPanelWindow(item, self.lblHost,fpb.FPB_ALIGN_WIDTH, 5, 0) 
        self.txtHost = wx.TextCtrl(id=-1,
              name=u'txtHost', parent=item, pos=wx.DefaultPosition,
              size=wx.DefaultSize, style=0, value=u'')
        self.fldNav.AddFoldPanelWindow(item, self.txtHost,fpb.FPB_ALIGN_WIDTH, 5, 0) 
        self.lblPort = wx.StaticText(id=-1,
              label=u'Port', name=u'lblPort', parent=item,
              pos=wx.DefaultPosition, size=wx.DefaultSize,
              style=0)
        self.fldNav.AddFoldPanelWindow(item, self.lblPort,fpb.FPB_ALIGN_WIDTH, 5, 0) 
        self.txtPort = wx.TextCtrl(id=-1,
              name=u'txtPort', parent=item, pos=wx.DefaultPosition,
              size=wx.DefaultSize, style=0, value=u'')
        self.fldNav.AddFoldPanelWindow(item, self.txtPort,fpb.FPB_ALIGN_WIDTH, 5, 0) 
        self.tgConnect = wx.lib.buttons.GenBitmapTextToggleButton(ID=-1,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Connect',
              name=u'tgConnect', parent=item, pos=wx.DefaultPosition,
              size=wx.DefaultSize, style=0)
        self.tgConnect.SetToggle(False)
        self.tgConnect.Bind(wx.EVT_BUTTON, self.OnTgConnectButton)
        self.fldNav.AddFoldPanelWindow(item, self.tgConnect,fpb.FPB_ALIGN_WIDTH, 5, 0) 
        self.lblConnState = wx.StaticBitmap(bitmap=wx.EmptyBitmap(16, 16),
              id=-1, name=u'lblConnState',
              parent=item, pos=wx.DefaultPosition, size=wx.DefaultSize,
              style=0)
        self.fldNav.AddFoldPanelWindow(item, self.lblConnState,fpb.FPB_ALIGN_WIDTH, 5, 0) 
        
        item = self.fldNav.AddFoldPanel(_(u"limit"), collapsed=True,
                                      foldIcons=Images,cbstyle=style)
        self.chcRev = wx.CheckBox(id=-1,
              label=_(u'reverse'), name=u'chcRev', parent=item,
              pos=wx.DefaultPosition, size=wx.DefaultSize, style=0)
        self.chcRev.SetValue(True)
        self.fldNav.AddFoldPanelWindow(item, self.chcRev,fpb.FPB_ALIGN_WIDTH, 5, 0) 
        self.spLimit = wx.SpinCtrl(id=-1,
              initial=1, max=100, min=1, name=u'spLimit',
              parent=item, pos=wx.DefaultPosition, size=wx.DefaultSize,
              style=wx.SP_ARROW_KEYS)
        self.spLimit.SetToolTipString(_(u'buffer size [k]'))
        self.fldNav.AddFoldPanelWindow(item, self.spLimit,fpb.FPB_ALIGN_WIDTH, 5, 0) 
        
        item = self.fldNav.AddFoldPanel(_(u"origin"), collapsed=True,
                                      foldIcons=Images,cbstyle=style)
        self.chcOrigin = wx.CheckListBox(choices=[],
              id=-1, name=u'chcOrigin',
              parent=item, pos=wx.DefaultPosition, size=wx.Size(-1,300),
              style=0)
        self.chcOrigin.Bind(wx.EVT_CHECKLISTBOX, self.OnChcOriginChecklistbox)
        self.fldNav.AddFoldPanelWindow(item, self.chcOrigin,fpb.FPB_ALIGN_WIDTH, 5, 0) 
        
        self.cbSelAll = wx.lib.buttons.GenBitmapTextButton(ID=-1,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Sel'), name=u'cbSelAll',
              parent=item, pos=wx.DefaultPosition, size=wx.DefaultSize,
              style=0)
        self.cbSelAll.Bind(wx.EVT_BUTTON, self.OnCbSelAllButton)
        self.fldNav.AddFoldPanelWindow(item, self.cbSelAll,fpb.FPB_ALIGN_WIDTH, 5, 0) 
        
        self.cbUnSelAll = wx.lib.buttons.GenBitmapTextButton(ID=-1,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Unsel'),
              name=u'cbUnSelAll', parent=item, pos=wx.DefaultPosition,
              size=wx.DefaultSize, style=0)
        self.cbUnSelAll.Bind(wx.EVT_BUTTON, self.OnCbUnSelAllButton)
        self.fldNav.AddFoldPanelWindow(item, self.cbUnSelAll,fpb.FPB_ALIGN_WIDTH, 5, 0) 
        
        item = self.fldNav.AddFoldPanel(_(u"level"), collapsed=True,
                                      foldIcons=Images,cbstyle=style)
        self.chkLevel = wx.CheckListBox(choices=[u'debug', u'info', u'warn',
              u'warning', u'error', u'critical', u'fatal'],
              id=-1, name=u'chkLevel',
              parent=item, pos=wx.DefaultPosition, size=wx.DefaultSize,
              style=0)
        self.chkLevel.Bind(wx.EVT_CHECKLISTBOX, self.OnChkLevelChecklistbox)
        self.fldNav.AddFoldPanelWindow(item, self.chkLevel,fpb.FPB_ALIGN_WIDTH, 5, 0) 
        
        self.slwNav.SizeWindows()
        
        rect = self.sbStatus.GetFieldRect(STATUS_PROCESS_POS)
        self.gProcess = wx.Gauge(
                    self.sbStatus, -1, 50, (rect.x+2, rect.y+2), 
                    (rect.width-4, rect.height-4), 
                    wx.GA_HORIZONTAL|wx.GA_SMOOTH
                    )
        self.gProcess.SetRange(1000)
        self.gProcess.SetValue(0)
        iLen=self.chkLevel.GetCount()
        for i in range(iLen):
            self.chkLevel.Check(i,True)
        self.lstLogs.InsertColumn(0,_(u'origin'),wx.LIST_FORMAT_LEFT,80)
        self.lstLogs.InsertColumn(1,_(u'level'),wx.LIST_FORMAT_LEFT,60)
        self.lstLogs.InsertColumn(2,_(u'msg'),wx.LIST_FORMAT_LEFT,150)
        self.lstLogs.InsertColumn(3,_(u'time'),wx.LIST_FORMAT_LEFT,140)
        self.lstLogs.InsertColumn(4,_(u'callstack'),wx.LIST_FORMAT_LEFT,0)
        
        self.lstCallStack.InsertColumn(0,_(u'line'),wx.LIST_FORMAT_RIGHT,60)
        self.lstCallStack.InsertColumn(1,_(u'file'),wx.LIST_FORMAT_LEFT,120)
        self.lstCallStack.InsertColumn(2,_(u'methode'),wx.LIST_FORMAT_LEFT,150)
        
        self.cbSelAll.SetBitmapLabel(images.getSelAllBitmap())
        self.cbUnSelAll.SetBitmapLabel(images.getUnSelAllBitmap())
        self.tgConnect.SetBitmapLabel(images.getDisconnectBitmap())
        self.tgConnect.SetBitmapSelected(images.getConnectBitmap())
        self.lblConnState.SetBitmap(images.getDisconnectBitmap())
        self.lblConnState.Show(False)
        self.cbFind.SetBitmapLabel(images.getFindBitmap())
        self.tgFindDir.SetBitmapLabel(images.getDownBitmap())
        self.tgFindDir.SetBitmapSelected(images.getUpBitmap())
        self.tgOriginRefresh.SetBitmapLabel(images.getReloadBitmap())
        self.tgOriginRefresh.SetBitmapSelected(images.getReloadBitmap())
        self.tgOriginRefresh.SetValue(True)
        self.cbClr.SetBitmapLabel(images.getDelBitmap())
        self.chcFindOrigin.SetSelection(0)
        self.chcFindLevel.SetSelection(0)
        
        self.spLimit.SetValue(5)
        self.txtHost.SetValue('localhost')
        self.dLevel={'DEBUG':True,'INFO':True,'WARN':True,'WARNING':True,
            'ERROR':True,'CRITICAL':True,'FATAL':True}
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnMain)
        self.pnMain.Refresh()
        self.__makeTitle__()
        self.Clear()

    def Notify(self):
        try:
            t = time.localtime(time.time())
            st = time.strftime(_("%b-%d-%Y   %H:%M:%S wk:%W"), t)
            self.SetStatusText(st, STATUS_CLOCK_POS)
        except:
            pass
    def __makeTitle__(self):
        sOld=self.GetTitle()
        try:
            sAppl='('+self.chcAppl.GetStringSelection()+')'
        except:
            sAppl='()'
        s="VIDARC Log Sock Viewer "+sAppl
        if sOld!=s:
            self.SetTitle(s)
    def __setModified__(self,state):
        self.bModified=state
        self.__makeTitle__()
    
    def Clear(self):
        self.dOrigin={}
        self.chcOrigin.Clear()
        self.chcFindOrigin.Clear()
        self.appls=[]
        self.lstLogs.DeleteAllItems()
        self.lstCallStack.DeleteAllItems()
        self.txtMsg.SetValue('')
        self.__updateStatusBar__()
    def GetFN(self):
        return self.fn
    def AddOrigin(self,origin):
        if origin in self.dOrigin:
            bEnable=self.dOrigin[origin]
        else:
            if self.tgOriginRefresh.GetValue():
                bEnable=True
                self.dOrigin[origin]=bEnable
                self.__updateOrigin__()
            else:
                bEnable=False
        return bEnable
        try:
            bEnable=self.dOrigin[origin]
        except:
            bEnable=True
            self.dOrigin[origin]=bEnable
            self.__updateOrigin__()
        return bEnable
    def AddMsg(self,sOrigin,sLv,sMsg,sTime,sCall):
        try:
            sLv=string.strip(sLv)
            if self.dLevel[sLv]==False:
                return
            bEnable=self.dOrigin[sOrigin]
            if self.chcRev.GetValue():
                i2Add=0
            else:
                i2Add=sys.maxint
            idx=self.lstLogs.InsertImageStringItem(i2Add, sOrigin, -1)
            self.lstLogs.SetStringItem(idx,1,sLv,-1)
            self.lstLogs.SetStringItem(idx,2,string.strip(sMsg),-1)
            self.lstLogs.SetStringItem(idx,3,string.strip(sTime),-1)
            self.lstLogs.SetStringItem(idx,4,string.strip(sCall),-1)
            #self.lstLogs.SetItemData(idx,iLogNr)
            self.__updateStatusBar__()
        except:
            pass
    def __updateStatusBar__(self):
            i=self.lstLogs.GetItemCount()
            iMax=self.spLimit.GetValue()*1000
            while i>iMax:
                i-=1
                self.lstLogs.DeleteItem(i)
            self.gProcess.SetValue(int((i/float(iMax))*1000))
            s='%08d / %08d'%(i,iMax)
            self.SetStatusText(s, STATUS_MSG_POS)

    def __connect__(self):
        self.__disconnect__()        
        #self.lngReceiver=LogRecordSocketReceiver(self,self.txtHost.GetValue(),
        #    self.iSockPort)
        try:
            iSockPort=int(self.txtPort.GetValue())
        except:
            iSockPort=self.iSockPort
        self.lngReceiver=vtLogClt(self,self.txtHost.GetValue(),iSockPort,vtLogViewerSock)
        self.lngReceiver.Connect()

        #self.chcOrigin.Clear()
        #thread.start_new_thread(self.lngReceiver.serve_until_stopped,())
    def __disconnect__(self):
        if self.lngReceiver is not None:
            #self.lngReceiver.abort=True
            #while not self.lngReceiver.stopped:
            #    time.sleep(0.1)
            self.lngReceiver.Close()
            try:
                while not self.lngReceiver.IsServing():
                    time.sleep(0.1)
            except:
                pass
        self.lngReceiver=None
    def OnChcApplChoice(self, event):
        self.iSockPort=self.MAP[self.chcAppl.GetStringSelection()]
        self.txtPort.SetValue(str(self.iSockPort))
        self.__makeTitle__()
        event.Skip()
    def __updateOrigin__(self):
        origins=self.dOrigin.keys()
        origins.sort()
        self.chcOrigin.Clear()
        self.chcFindOrigin.Clear()
        self.chcFindOrigin.Append('---')
        for org in origins:
            self.chcOrigin.Append(org)
            self.chcFindOrigin.Append(org)
        iLen=self.chcOrigin.GetCount()
        for i in range(iLen):
            self.chcOrigin.Check(i,self.dOrigin[origins[i]])
        
    def OnCbGenButton(self, event):
        if self.thdBuild.IsRunning():
            return
        self.thdBuild.Start()
        event.Skip()

    def OnLstLogsListItemSelected(self, event):
        self.idxSel=event.GetIndex()
        self.__updateCallStack__()
        self.__updateMsg__()
        event.Skip()

    def OnLstLogsListItemDeselected(self, event):
        self.idxSel=-1
        self.__updateCallStack__()
        self.__updateMsg__()
        event.Skip()
        
    def __updateMsg__(self):
        if self.idxSel<0:
            self.txtMsg.SetValue(u'')
            return
        #idxAppl=self.chcAppl.GetSelection()
        #if idxAppl<0:
        #    return
        try:
            msg=string.split(self.lstLogs.GetItem(self.idxSel,2).m_text,'|')[0]
        except:
            msg=u''
        #idxLog=self.lstLogs.GetItemData(self.idxSel)
        #log=self.appls[idxAppl][1][idxLog]
        
        msg=string.replace(msg,';',chr(10))
        #msg=string.replace(msg,',',chr(10))
        msg=string.replace(msg,'\n',chr(10))
        self.txtMsg.SetValue(msg)
    def __updateCallStack__(self):
        self.lstCallStack.DeleteAllItems()
        if self.idxSel<0:
            return
        
        #idxAppl=self.chcAppl.GetSelection()
        #if idxAppl<0:
        #    return
        #idxLog=self.lstLogs.GetItemData(self.idxSel)
        #log=self.appls[idxAppl][1][idxLog]
        try:
            cs=self.lstLogs.GetItem(self.idxSel,4).m_text
        except:
            cs=u''
        strs=string.split(cs,',')
        for s in strs:
            sFile=u'???'
            sMethod=u'???'
            sLine=u'???'
            i=string.find(s,':')
            if i>=0:
                j=string.find(s,'(')
                if j>=0:
                    k=string.find(s,')')
                    if k>=0:
                        #all ok
                        sFile=s[:i]
                        sMethod=s[i+1:j]
                        sLine=s[j+1:k]
            idx=self.lstCallStack.InsertImageStringItem(sys.maxint,sLine,-1)
            self.lstCallStack.SetStringItem(idx,1,sFile)
            self.lstCallStack.SetStringItem(idx,2,sMethod)

    def OnCbSelAllButton(self, event):
        iLen=self.chcOrigin.GetCount()
        for i in range(iLen):
            self.dOrigin[self.chcOrigin.GetString(i)]=True
            self.chcOrigin.Check(i,True)
        event.Skip()

    def OnCbUnSelAllButton(self, event):
        iLen=self.chcOrigin.GetCount()
        for i in range(iLen):
            self.dOrigin[self.chcOrigin.GetString(i)]=False
            self.chcOrigin.Check(i,False)
        event.Skip()

    def OnTgConnectButton(self, event):
        
        if self.tgConnect.GetToggle():
            self.__connect__()
        else:
            self.__disconnect__()
        event.Skip()

    def OnChcOriginChecklistbox(self, event):
        i=event.GetSelection()
        self.dOrigin[self.chcOrigin.GetString(i)]=self.chcOrigin.IsChecked(i)
        event.Skip()
    def NotifyLoggedIn(self):
        pass
    def NotifyPause(self):
        pass
    def NotifyConnectionFault(self):
        self.lblConnState.SetBitmap(images.getFaultBitmap())
        self.tgConnect.SetToggle(False)
        self.lblConnState.Show(True)
        pass
    def NotifyConnected(self):
        self.lblConnState.SetBitmap(images.getConnectBitmap())
        self.lblConnState.Show(False)
        pass
    def __connectionClosed__(self,bEstablished):
        self.lblConnState.SetBitmap(images.getDisconnectBitmap())
        del self.lngReceiver
        self.lngReceiver=None
        self.tgConnect.SetToggle(False)
        self.lblConnState.Show(False)

    def OnChkLevelChecklistbox(self, event):
        i=event.GetSelection()
        s=string.upper(self.chkLevel.GetString(i))
        self.dLevel[s]=self.chkLevel.IsChecked(i)
        event.Skip()
        
    def OnSlwNavSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iWidth=event.GetDragRect().width
            self.slwNav.SetDefaultSize((iWidth, 1000))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnMain)
            self.pnMain.Refresh()
        except:
            traceback.print_exc()
            #vtLog.vtLngTB(self.GetName())
        #event.Skip()
    def OnSlwTopSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iHeight=event.GetDragRect().height
            self.slwTop.SetDefaultSize((1000, iHeight))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnMain)
            self.pnMain.Refresh()
        except:
            traceback.print_exc()

    def OnVtLogSockViewerFrameSize(self, event):
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnMain)
        #event.Skip()

    def OnCbFindButton(self, event):
        event.Skip()
        if self.chcFindOrigin.GetSelection()>0:
            sOrigin=self.chcFindOrigin.GetStringSelection()
        else:
            sOrigin=None
        if self.chcFindLevel.GetSelection()>0:
            sLevel=MAP_NR_TO_LEVEL_NAME[self.chcFindLevel.GetSelection()-1]
        else:
            sLevel=None
        sMsg=self.txtFindMsg.GetValue()
        if len(sMsg)==0:
            sMsg=None
            
        tup2Find=[sOrigin,sLevel,sMsg]
        idx=self.idxSel
        iLen=self.lstLogs.GetItemCount()
        if self.tgFindDir.GetValue():
            iDir=-1
            if idx<=0:
                idx=iLen
            if idx>iLen:
                idx=iLen
        else:
            iDir=1
            if idx<0:
                idx=-1
            if idx>=(iLen-1):
                idx=-1
        #if idx>iLen:
        #    idx=0
        idxStart=-3
        if iLen==0:
            return
        idx+=iDir
        #if idx>iLen:
        #    idx=0
        #if idx<0:
        #    idx=iLen-1
        while idx!=idxStart:
            if idxStart==-3:
                idxStart=idx
            bMatch=True
            for iCol in xrange(0,3):
                it=self.lstLogs.GetItem(idx,iCol)
                sVal=it.m_text
                if tup2Find[iCol] is not None:
                    if sVal.find(tup2Find[iCol])<0:
                        bMatch=False
                        break
            if bMatch:
                self.lstLogs.SetItemState(self.idxSel,0,wx.LIST_STATE_SELECTED)
                self.lstLogs.SetItemState(idx,wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
                self.lstLogs.EnsureVisible(idx)
                self.idxSel=idx
                break
            idx+=iDir
            if idx>=iLen:
                idx=0
            if idx<0:
                idx=iLen-1
    def GetFN(self):
        return self.sFN
    def OpenFile(self,fn):
        self.sFN=fn
        pass
    def OnCbClrButton(self, event):
        event.Skip()
        self.Clear()

    def OnCbSaveButton(self, event):
        event.Skip()

    def OnMnFileMn_file_saveMenu(self, event):
        event.Skip()
        dlg = wx.FileDialog(self, _("Save"), ".", "", _("log files (*.log)|*.log|all files (*.*)|*.*"), wx.SAVE)
        try:
            u=platform.uname()
            fn=self.chcAppl.GetStringSelection()+'_'+u[1]+'_'+time.strftime('%Y%m%d_%H%M%S',time.localtime(time.time()))
            if fn is not None:
                dlg.SetPath(fn)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.SaveFile(filename)
        finally:
            dlg.Destroy()
    def SaveFile(self,fn):
        fOut=open(fn,'a')
        try:
            fOut.write(time.strftime('%Y-%m-%d %H:%M:%S,000|                  |INFO    |',time.localtime(time.time())))
            fOut.write('------------------------appl started-------------\n')
            iCount=self.lstLogs.GetItemCount()
            for idx in xrange(iCount):
                #strs[1],strs[2],strs[3],strs[0],strs[4]
                fOut.write(self.lstLogs.GetItem(idx,3).m_text)
                fOut.write('|%-18s|%-8s|'%(self.lstLogs.GetItem(idx,0).m_text,self.lstLogs.GetItem(idx,1).m_text))
                fOut.write(self.lstLogs.GetItem(idx,2).m_text)
                fOut.write('|')
                fOut.write(self.lstLogs.GetItem(idx,4).m_text)
                fOut.write('\n')
        except:
            fOut.close()
    def OnMnFileMn_file_exitMenu(self, event):
        event.Skip()
        self.Close()

    def OnMnFileMn_file_openMenu(self, event):
        event.Skip()
        dlg = wx.FileDialog(self, _("Open"), ".", "", _("log files (*.log)|*.log|all files (*.*)|*.*"), wx.OPEN)
        try:
            fn=self.GetFN()
            if fn is not None:
                dlg.SetPath(fn)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.OpenFile(filename)
        finally:
            dlg.Destroy()

    def OnMnFileMn_file_open_cfgMenu(self, event):
        event.Skip()
        dlg = wx.FileDialog(self, _("Open"), ".", "", _("config files (*.cfg)|*.cfg|all files (*.*)|*.*"), wx.OPEN)
        try:
            #fn=self.GetFN()
            #if fn is not None:
            #    dlg.SetPath(fn)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                fOut=open(filename,'r')
                self.dOrigin={}
                try:
                    for l in fOut.readlines():
                        s='%-18s'%l.strip()
                        self.dOrigin[s]=True
                except:
                    pass
                self.__updateOrigin__()
                fOut.close()
        finally:
            dlg.Destroy()

    def OnMnFileMn_file_save_cfgMenu(self, event):
        event.Skip()
        dlg = wx.FileDialog(self, _("Save"), ".", "", _("config files (*.cfg)|*.cfg|all files (*.*)|*.*"), wx.SAVE)
        try:
            u=platform.uname()
            fn=self.chcAppl.GetStringSelection()+'_'+u[1]+'_'+time.strftime('%Y%m%d_%H%M%S',time.localtime(time.time()))
            if fn is not None:
                dlg.SetPath(fn)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                fOut=open(filename,'w')
                try:
                    origins=self.dOrigin.keys()
                    origins.sort()
                    for k in origins:
                        if self.dOrigin[k]:
                            fOut.write(k)
                            fOut.write('\n')
                except:
                    pass
                fOut.close()
        finally:
            dlg.Destroy()
        

    def OnVtLogSockViewerFrameClose(self, event):
        event.Skip()
        if self.tgConnect.GetToggle()==True:
            self.__disconnect__()
