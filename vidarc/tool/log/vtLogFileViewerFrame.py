#Boa:Frame:vtLogFileViewerFrame
#----------------------------------------------------------------------------
# Name:         vtLogFileViewerFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtLogFileViewerFrame.py,v 1.38 2010/08/08 12:09:02 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import string,os,os.path,time,sys,stat
import thread,threading,traceback

import wx
import wx.gizmos
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons
import vidarc.tool.vtThread
import vidarc.tool.InOut.vtInOutFileIdxLinesWid
from vidarc.tool.InOut.cfgFile import vtIOCfgConfig
import vidarc.tool.log.vtLogFile

import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.time.vtTimeLoadBound as vtLdBd
from vidarc.tool.misc.vtmMsgDialog import vtmMsgDialog
import vidarc.vApps.common.vAboutDialog as vAboutDialog
import vidarc.vApps.common.vSystem as vSystem

import vidarc.tool.log.images as images

wxEVT_LOG_VIEWER_PROC=wx.NewEventType()
vEVT_LOG_VIEWER_PROC=wx.PyEventBinder(wxEVT_LOG_VIEWER_PROC,1)
def EVT_LOG_VIEWER_PROC(win,func):
    win.Connect(-1,-1,wxEVT_LOG_VIEWER_PROC,func)
class vtLogViewerProc(wx.PyEvent):
    """
    Posted Events:
        Tree Item added event, reports the parent node of the added one
            EVT_LOG_VIEWER_PROC(<widget_name>, self.OnItemSel)
    """

    def __init__(self,obj,pos,size):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_LOG_VIEWER_PROC)
        self.obj=obj
        self.pos=pos
        self.size=size
    def GetPos(self):
        return self.pos
    def GetSize(self):
        return self.size

wxEVT_LOG_VIEWER_READ_FIN=wx.NewEventType()
vEVT_LOG_VIEWER_READ_FIN=wx.PyEventBinder(wxEVT_LOG_VIEWER_READ_FIN,1)
def EVT_LOG_VIEWER_READ_FIN(win,func):
    win.Connect(-1,-1,wxEVT_LOG_VIEWER_READ_FIN,func)
class vtLogViewerReadFin(wx.PyEvent):
    """
    Posted Events:
        Tree Item added event, reports the parent node of the added one
            EVT_LOG_VIEWER_READ_FIN(<widget_name>, self.OnReadFin)
    """

    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_LOG_VIEWER_READ_FIN)
        self.obj=obj

wxEVT_LOG_VIEWER_ADD=wx.NewEventType()
vEVT_LOG_VIEWER_ADD=wx.PyEventBinder(wxEVT_LOG_VIEWER_ADD,1)
def EVT_LOG_VIEWER_ADD(win,func):
    win.Connect(-1,-1,wxEVT_LOG_VIEWER_ADD,func)
class vtLogViewerAdd(wx.PyEvent):
    """
    Posted Events:
        Tree Item added event, reports the parent node of the added one
            EVT_LOG_VIEWER_ADD(<widget_name>, self.OnAdd)
    """

    def __init__(self,obj,iLogNr,log):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_LOG_VIEWER_ADD)
        self.obj=obj
        self.iLogNr=iLogNr
        self.log=log
    def GetLogNr(self):
        return self.iLogNr
    def GetLog(self):
        return self.log

MAP_NR_TO_LEVEL_NAME=['DEBUG','INFO','WARNING','WARNING','ERROR','CRITICAL','FATAL']
def create(parent):
    return vtLogFileViewerFrame(parent)

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon

[wxID_VTLOGFILEVIEWERFRAME, wxID_VTLOGFILEVIEWERFRAMECBCMPSET1, 
 wxID_VTLOGFILEVIEWERFRAMECBCMPSET2, wxID_VTLOGFILEVIEWERFRAMECBCMPUPDATE, 
 wxID_VTLOGFILEVIEWERFRAMEFILLOG, wxID_VTLOGFILEVIEWERFRAMELSTCALLSTACK, 
 wxID_VTLOGFILEVIEWERFRAMELSTCMP, wxID_VTLOGFILEVIEWERFRAMELSTLOGS, 
 wxID_VTLOGFILEVIEWERFRAMENBINFO, wxID_VTLOGFILEVIEWERFRAMEPNBOT, 
 wxID_VTLOGFILEVIEWERFRAMEPNCMP, wxID_VTLOGFILEVIEWERFRAMEPNMAIN, 
 wxID_VTLOGFILEVIEWERFRAMESBSTATUS, wxID_VTLOGFILEVIEWERFRAMESLWBOT, 
 wxID_VTLOGFILEVIEWERFRAMETXTMSG, wxID_VTLOGFILEVIEWERFRAMETXTORIGIN, 
] = [wx.NewId() for _init_ctrls in range(16)]

[wxID_VTLOGFILEVIEWERFRAMEMNFILEITEM_CLOSE, 
 wxID_VTLOGFILEVIEWERFRAMEMNFILEITEM_EXIT, 
 wxID_VTLOGFILEVIEWERFRAMEMNFILEITEM_OPEN, 
 wxID_VTLOGFILEVIEWERFRAMEMNFILEMNFILE_CFG_OPEN, 
 wxID_VTLOGFILEVIEWERFRAMEMNFILEMNFILE_SAVE_CFG, 
] = [wx.NewId() for _init_coll_mnFile_Items in range(5)]


STATUS_PROCESS_POS=0
STATUS_TEXT_POS=0
STATUS_CLOCK_POS=2

[wxID_VTLOGFILEVIEWERFRAMEMNHELPMN_HELP_ABOUT] = [wx.NewId() for _init_coll_mnHelp_Items in range(1)]

class vtLogFileViewerFrame(wx.Frame):
    def _init_coll_fgsCmp_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(1)

    def _init_coll_bxsCmpBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbCmpSet1, 0, border=0, flag=0)
        parent.AddWindow(self.cbCmpSet2, 0, border=4, flag=wx.TOP)
        parent.AddWindow(self.cbCmpUpdate, 0, border=4, flag=wx.TOP)

    def _init_coll_fgsMain_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.filLog, 1, border=4,
              flag=wx.EXPAND|wx.LEFT|wx.RIGHT|wx.TOP)
        parent.AddWindow(self.lstLogs, 0, border=4,
              flag=wx.EXPAND|wx.LEFT|wx.RIGHT|wx.BOTTOM)
        parent.AddWindow(self.txtOrigin, 1, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.EXPAND)

    def _init_coll_fgsCmp_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsCmpBt, 0, border=7,
              flag=wx.RIGHT | wx.LEFT | wx.TOP)
        parent.AddWindow(self.lstCmp, 0, border=0, flag=wx.EXPAND)

    def _init_coll_fgsMain_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)
        parent.AddGrowableCol(0)

    def _init_coll_mnFile_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_VTLOGFILEVIEWERFRAMEMNFILEITEM_OPEN,
              kind=wx.ITEM_NORMAL, text=_(u'Open'))
        parent.Append(help='', id=wxID_VTLOGFILEVIEWERFRAMEMNFILEITEM_CLOSE,
              kind=wx.ITEM_NORMAL, text=_(u'Close'))
        parent.AppendSeparator()
        parent.Append(help='',
              id=wxID_VTLOGFILEVIEWERFRAMEMNFILEMNFILE_CFG_OPEN,
              kind=wx.ITEM_NORMAL, text=u'Open Config')
        parent.Append(help='',
              id=wxID_VTLOGFILEVIEWERFRAMEMNFILEMNFILE_SAVE_CFG,
              kind=wx.ITEM_NORMAL, text=u'Save Config')
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VTLOGFILEVIEWERFRAMEMNFILEITEM_EXIT,
              kind=wx.ITEM_NORMAL, text=_(u'Exit'))
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_openMenu,
              id=wxID_VTLOGFILEVIEWERFRAMEMNFILEITEM_OPEN)
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_exitMenu,
              id=wxID_VTLOGFILEVIEWERFRAMEMNFILEITEM_EXIT)
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_closeMenu,
              id=wxID_VTLOGFILEVIEWERFRAMEMNFILEITEM_CLOSE)
        self.Bind(wx.EVT_MENU, self.OnMnFileMnfile_cfg_openMenu,
              id=wxID_VTLOGFILEVIEWERFRAMEMNFILEMNFILE_CFG_OPEN)
        self.Bind(wx.EVT_MENU, self.OnMnFileMnfile_save_cfgMenu,
              id=wxID_VTLOGFILEVIEWERFRAMEMNFILEMNFILE_SAVE_CFG)

    def _init_coll_mnBar_Menus(self, parent):
        # generated method, don't edit

        parent.Append(menu=self.mnFile, title=_(u'File'))
        parent.Append(menu=self.mnHelp, title=u'?')

    def _init_coll_mnHelp_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_VTLOGFILEVIEWERFRAMEMNHELPMN_HELP_ABOUT,
              kind=wx.ITEM_NORMAL, text=u'About')
        self.Bind(wx.EVT_MENU, self.OnMnHelpMn_help_aboutMenu,
              id=wxID_VTLOGFILEVIEWERFRAMEMNHELPMN_HELP_ABOUT)

    def _init_coll_sbStatus_Fields(self, parent):
        # generated method, don't edit
        parent.SetFieldsCount(3)

        parent.SetStatusText(number=0, text=u'')
        parent.SetStatusText(number=1, text=u'')
        parent.SetStatusText(number=2, text=u'')

        parent.SetStatusWidths([100, -1, 200])

    def _init_coll_lstCmp_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT, heading=u'',
              width=20)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT, heading=u'x',
              width=200)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT, heading=u'y',
              width=200)

    def _init_coll_nbInfo_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.lstCallStack, select=False,
              text=_(u'Call Stack'))
        parent.AddPage(imageId=-1, page=self.txtMsg, select=True,
              text=_(u'Message'))
        parent.AddPage(imageId=-1, page=self.pnCmp, select=False,
              text=u'Compare')

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsMain = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self.fgsCmp = wx.FlexGridSizer(cols=2, hgap=4, rows=1, vgap=4)

        self.bxsCmpBt = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_fgsMain_Items(self.fgsMain)
        self._init_coll_fgsMain_Growables(self.fgsMain)
        self._init_coll_fgsCmp_Items(self.fgsCmp)
        self._init_coll_fgsCmp_Growables(self.fgsCmp)
        self._init_coll_bxsCmpBt_Items(self.bxsCmpBt)

        self.pnCmp.SetSizer(self.fgsCmp)
        self.pnMain.SetSizer(self.fgsMain)

    def _init_utils(self):
        # generated method, don't edit
        self.mnBar = wx.MenuBar()

        self.mnFile = wx.Menu(title=u'')

        self.mnHelp = wx.Menu(title='')

        self._init_coll_mnBar_Menus(self.mnBar)
        self._init_coll_mnFile_Items(self.mnFile)
        self._init_coll_mnHelp_Items(self.mnHelp)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VTLOGFILEVIEWERFRAME,
              name=u'vtLogFileViewerFrame', parent=prnt, pos=wx.Point(312, 47),
              size=wx.Size(626, 527), style=wx.DEFAULT_FRAME_STYLE,
              title=_(u'vtLogFileViewerFrame'))
        self._init_utils()
        
        self.SetClientSize(wx.Size(618, 500))
        self.SetMenuBar(self.mnBar)
        self.Bind(wx.EVT_SIZE, self.OnVtLogFileViewerFrameSize)
        self.Bind(wx.EVT_MOUSEWHEEL, self.OnVtLogFileViewerFrameMouseWheel)

        self.slwBot = wx.SashLayoutWindow(id=wxID_VTLOGFILEVIEWERFRAMESLWBOT,
              name=u'slwBot', parent=self, pos=wx.Point(0, 300),
              size=wx.Size(616, 160), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwBot.SetDefaultSize(wx.Size(464, 160))
        self.slwBot.SetOrientation(wx.LAYOUT_HORIZONTAL)
        self.slwBot.SetAlignment(wx.LAYOUT_BOTTOM)
        self.slwBot.SetSashVisible(wx.SASH_TOP, True)
        self.slwBot.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwBotSashDragged,
              id=wxID_VTLOGFILEVIEWERFRAMESLWBOT)

        self.pnMain = wx.Panel(id=wxID_VTLOGFILEVIEWERFRAMEPNMAIN,
              name=u'pnMain', parent=self, pos=wx.Point(0, 0), size=wx.Size(616,
              292), style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)
        self.pnMain.SetAutoLayout(True)
        self.pnMain.SetMinSize(wx.Size(100, 100))

        self.sbStatus = wx.StatusBar(id=wxID_VTLOGFILEVIEWERFRAMESBSTATUS,
              name=u'sbStatus', parent=self, style=0)
        self.sbStatus.SetFieldsCount(2)
        self._init_coll_sbStatus_Fields(self.sbStatus)
        self.SetStatusBar(self.sbStatus)

        self.lstLogs = wx.ListView(id=wxID_VTLOGFILEVIEWERFRAMELSTLOGS,
              name=u'lstLogs', parent=self.pnMain, pos=wx.Point(4, 47),
              size=wx.Size(604, 216), style=wx.LC_REPORT | wx.LC_SINGLE_SEL)
        self.lstLogs.SetConstraints(LayoutAnchors(self.lstLogs, True, True,
              True, True))
        self.lstLogs.SetMinSize(wx.Size(-1, -1))
        self.lstLogs.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstLogsListItemSelected,
              id=wxID_VTLOGFILEVIEWERFRAMELSTLOGS)
        self.lstLogs.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstLogsListItemDeselected,
              id=wxID_VTLOGFILEVIEWERFRAMELSTLOGS)
        self.lstLogs.Bind(wx.EVT_LEFT_DCLICK, self.OnLstLogsLeftDclick)
        self.lstLogs.Bind(wx.EVT_MOUSEWHEEL, self.OnLstLogsMouseWheel)
        self.lstLogs.Bind(wx.EVT_CHAR, self.OnLstLogsChar)

        self.pnBot = wx.Panel(id=wxID_VTLOGFILEVIEWERFRAMEPNBOT, name=u'pnBot',
              parent=self.slwBot, pos=wx.Point(0, 3), size=wx.Size(616, 157),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)
        self.pnBot.SetAutoLayout(True)

        self.nbInfo = wx.Notebook(id=wxID_VTLOGFILEVIEWERFRAMENBINFO,
              name=u'nbInfo', parent=self.pnBot, pos=wx.Point(8, 8),
              size=wx.Size(598, 143), style=0)
        self.nbInfo.SetConstraints(LayoutAnchors(self.nbInfo, True, True, True,
              True))

        self.lstCallStack = wx.ListCtrl(id=wxID_VTLOGFILEVIEWERFRAMELSTCALLSTACK,
              name=u'lstCallStack', parent=self.nbInfo, pos=wx.Point(0, 0),
              size=wx.Size(590, 117), style=wx.LC_REPORT | wx.LC_SINGLE_SEL)
        self.lstCallStack.SetConstraints(LayoutAnchors(self.lstCallStack, True,
              False, True, True))

        self.txtMsg = wx.TextCtrl(id=wxID_VTLOGFILEVIEWERFRAMETXTMSG,
              name=u'txtMsg', parent=self.nbInfo, pos=wx.Point(0, 0),
              size=wx.Size(590, 117), style=wx.TE_MULTILINE, value=u'')
        self.txtMsg.SetFont(wx.Font(10, wx.SWISS, wx.NORMAL, wx.NORMAL, False,
              u'Courier New'))

        self.filLog = vidarc.tool.log.vtLogFile.vtLogFile(id=wxID_VTLOGFILEVIEWERFRAMEFILLOG,
              name=u'filLog', parent=self.pnMain, pos=wx.Point(4, 4),
              size=wx.Size(604, 43), style=wx.SB_HORIZONTAL)
        self.filLog.Bind(vidarc.tool.InOut.vtInOutFileIdxLinesWid.vEVT_VTINOUT_FILEIDXLINES_ADD,
              self.OnFilLogVtinoutFileidxlinesAdd,
              id=wxID_VTLOGFILEVIEWERFRAMEFILLOG)

        self.txtOrigin = wx.TextCtrl(id=wxID_VTLOGFILEVIEWERFRAMETXTORIGIN,
              name=u'txtOrigin', parent=self.pnMain, pos=wx.Point(4, 267),
              size=wx.Size(604, 21), style=wx.NO_BORDER | wx.NO_3D, value=u'')
        self.txtOrigin.SetMinSize(wx.Size(-1, -1))

        self.pnCmp = wx.Panel(id=wxID_VTLOGFILEVIEWERFRAMEPNCMP, name=u'pnCmp',
              parent=self.nbInfo, pos=wx.Point(0, 0), size=wx.Size(590, 117),
              style=wx.TAB_TRAVERSAL)

        self.cbCmpSet1 = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Right),
              id=wxID_VTLOGFILEVIEWERFRAMECBCMPSET1, name=u'cbCmpSet1',
              parent=self.pnCmp, pos=wx.Point(7, 7), size=wx.Size(31, 30),
              style=0)
        self.cbCmpSet1.Bind(wx.EVT_BUTTON, self.OnCbCmpSet1Button,
              id=wxID_VTLOGFILEVIEWERFRAMECBCMPSET1)

        self.cbCmpSet2 = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Right),
              id=wxID_VTLOGFILEVIEWERFRAMECBCMPSET2, name=u'cbCmpSet2',
              parent=self.pnCmp, pos=wx.Point(7, 41), size=wx.Size(31, 30),
              style=0)
        self.cbCmpSet2.Bind(wx.EVT_BUTTON, self.OnCbCmpSet2Button,
              id=wxID_VTLOGFILEVIEWERFRAMECBCMPSET2)

        self.cbCmpUpdate = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Synch),
              id=wxID_VTLOGFILEVIEWERFRAMECBCMPUPDATE, name=u'cbCmpUpdate',
              parent=self.pnCmp, pos=wx.Point(7, 75), size=wx.Size(31, 30),
              style=0)
        self.cbCmpUpdate.Bind(wx.EVT_BUTTON, self.OnCbCmpUpdateButton,
              id=wxID_VTLOGFILEVIEWERFRAMECBCMPUPDATE)

        self.lstCmp = wx.ListCtrl(id=wxID_VTLOGFILEVIEWERFRAMELSTCMP,
              name=u'lstCmp', parent=self.pnCmp, pos=wx.Point(49, 0),
              size=wx.Size(541, 117), style=wx.LC_REPORT | wx.LC_SINGLE_SEL)
        self._init_coll_lstCmp_Columns(self.lstCmp)

        self._init_coll_nbInfo_Pages(self.nbInfo)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vtLog')
        self._init_ctrls(parent)
        
        lMnIt=self.mnFile.GetMenuItems()
        lMnIt[0].SetBitmap(vtArt.getBitmap(vtArt.OpenFile))
        lMnIt[1].SetBitmap(vtArt.getBitmap(vtArt.CloseFile))
        lMnIt[3].SetBitmap(vtArt.getBitmap(vtArt.Open))
        lMnIt[4].SetBitmap(vtArt.getBitmap(vtArt.Save))
        lMnIt[6].SetBitmap(vtArt.getBitmap(vtArt.PowerDown))
        lMnIt=self.mnHelp.GetMenuItems()
        lMnIt[0].SetBitmap(vtArt.getBitmap(vtArt.About))
        
        self.bModified=False
        self.idxSel=-1
        self.fn=None
        self.appls=[]
        self.cfg=vtIOCfgConfig()
        self.Clear()
        self.__markCloseMnIt(False)
        
        #self.filLog.SetPostWid(self,True)
        #self.Bind(vidarc.tool.vtThread.vEVT_VT_THREAD_FINISHED,
        #                self.OnFilLogVtThreadFinished)
        #self.Bind(vidarc.tool.vtThread.vEVT_VT_THREAD_ABORTED,
        #                self.OnFilLogVtThreadAborted)
        self.filLog.Bind(vidarc.tool.log.vtLogFile.vEVT_VTLOG_FILE_PROC,
                        self.OnLogViewerProc)
        self.filLog.Bind(vidarc.tool.log.vtLogFile.vEVT_VTLOG_FILE_SEL,
                        self.OnFilLogSel)
        self.filLog.Bind(vidarc.tool.log.vtLogFile.vEVT_VTLOG_FILE_MARK,
                        self.OnFilLogMark)
        # setup statusbar
        self.timer = wx.PyTimer(self.Notify)
        self.timer.Start(1000)
        self.Notify()
        
        icon = getApplicationIcon()
        self.SetIcon(icon)
        
        rect = self.sbStatus.GetFieldRect(STATUS_PROCESS_POS)
        self.gProcess = wx.Gauge(
                    self.sbStatus, -1, 50, (rect.x+2, rect.y+2), 
                    (rect.width-4, rect.height-4), 
                    wx.GA_HORIZONTAL|wx.GA_SMOOTH
                    )
        self.gProcess.SetRange(1000)
        self.gProcess.SetValue(0)
        self.lstLogs.InsertColumn(0,_(u'origin'),wx.LIST_FORMAT_LEFT,160)
        self.lstLogs.InsertColumn(1,_(u'level'),wx.LIST_FORMAT_LEFT,60)
        self.lstLogs.InsertColumn(2,_(u'msg'),wx.LIST_FORMAT_LEFT,240)
        self.lstLogs.InsertColumn(3,_(u'time'),wx.LIST_FORMAT_LEFT,140)
        
        self.lstCallStack.InsertColumn(0,_(u'line'),wx.LIST_FORMAT_RIGHT,60)
        self.lstCallStack.InsertColumn(1,_(u'file'),wx.LIST_FORMAT_LEFT,340)
        self.lstCallStack.InsertColumn(2,_(u'methode'),wx.LIST_FORMAT_LEFT,150)
        
        self.imgLstTyp=wx.ImageList(16,16)
        self.imgDict={}
        img=wx.ArtProvider.GetBitmap(wx.ART_TICK_MARK, size=(16, 16))
        self.imgDict['def']=self.imgLstTyp.Add(img)
        img=wx.ArtProvider.GetBitmap(wx.ART_WARNING, size=(16, 16))
        self.imgDict['warn']=self.imgLstTyp.Add(img)
        img=wx.ArtProvider.GetBitmap(wx.ART_ERROR, size=(16, 16))
        self.imgDict['error']=self.imgLstTyp.Add(img)
        img=vtArt.getBitmap(vtArt.Invisible)
        self.imgDict['empty']=self.imgLstTyp.Add(img)
        self.lstCmp.SetImageList(self.imgLstTyp,wx.IMAGE_LIST_SMALL)
        
        self.filLog.SetBufSize(20)
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnMain)
        vtLdBd.vtTimeLoadBoundInit()
        self.Clear()
        wx.CallAfter(self.Bind,wx.EVT_CLOSE, self.OnMainClose)
        
        f=self.fntItemNor=self.lstLogs.GetFont()
        self.fntItemSel=wx.Font(f.GetPointSize(),f.GetFamily(),
                f.GetStyle(),wx.BOLD,f.GetUnderlined(),f.GetFaceName(),
                f.GetDefaultEncoding())
        #self.pnMain.Refresh()
        #self.fgsTop.Fit(self)
    def Notify(self):
        try:
            t = time.localtime(time.time())
            st = time.strftime(_("%b-%d-%Y   %H:%M:%S wk:%W"), t)
            self.SetStatusText(st, STATUS_CLOCK_POS)
        except:
            pass
    def __makeTitle__(self):
        s="VIDARC Log File Viewer"
        if self.fn is None:
            s=s+" (undef*)"
        else:
            s=s+" ("+self.fn
            if self.bModified:
                s=s+"*)"
            else:
                s=s+")"
        self.SetTitle(s)
    def __setModified__(self,state):
        self.bModified=state
        self.__makeTitle__()
    def Clear(self):
        #self.chcOrigin.Clear()
        self.lstLogs.DeleteAllItems()
        self.lstCallStack.DeleteAllItems()
        self.lstCmp.DeleteAllItems()
        self.__markCloseMnIt(False)
        self.txtMsg.SetValue('')
        self.iX=-1
        self.iY=-1
    def __markCloseMnIt(self,bFlag):
        try:
            mnBar=self.GetMenuBar()
            mn=mnBar.GetMenu(0)
            mnIt=mn.FindItemById(wxID_VTLOGFILEVIEWERFRAMEMNFILEITEM_CLOSE)
            mnIt.Enable(bFlag)
        except:
            traceback.print_exc()
            pass
    def GetFN(self):
        return self.fn
    def OpenFile(self,fn=None):
        #vtLog.CallStack('')
        if self.filLog.IsRunning():
        #if self.thdOpen.IsRunning():
            dlg=vtmMsgDialog(self,_(u'Open file not possible yet, thread still running.') ,
                        'vtLog',
                        wx.OK|wx.ICON_EXCLAMATION )
            dlg.Destroy()
            return
        if fn is not None:
            sDN,sFN=os.path.split(fn)
            if len(sDN)==0:
                sDN=os.getcwd()
            fn=os.path.join(sDN,sFN)
            self.fn=fn
            self.__makeTitle__()
        #self.thdOpen.Start()
        self.lstLogs.DeleteAllItems()
        self.lstCallStack.DeleteAllItems()
        self.lstCmp.DeleteAllItems()
        self.iX=-1
        self.iY=-1
        self.__markCloseMnIt(True)
        self.filLog.Open(self.fn)
        self.filLog.Index()
        sIniFN=self.fn[:-3]+'ini'
        self.OpenCfgFile(sIniFN)
    def OnMnFileItem_openMenu(self, event):
        if self.filLog.IsRunning():
            return 
        dlg = wx.FileDialog(self, _(u"Open"), "", "", _(u"log files (*.log)|*.log|all files (*.*)|*.*"), wx.OPEN)
        try:
            fn=self.GetFN()
            if fn is not None:
                dlg.SetPath(fn)
            else:
                sLogDN=os.getenv('vidarcLogDN')
                if sLogDN is not None:
                    dlg.SetPath(sLogDN+os.path.sep)
            #else:
            #    dlg.SetPath('.')
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.OpenFile(filename)
                self.__setModified__(False)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()
    def OnMainClose(self,evt):
        try:
            if evt.CanVeto():
                if self.filLog.IsRunning():
                    evt.Veto()
                    self.filLog.Stop()
                    return
        except:
            pass
        evt.Skip()
    def OnMnFileItem_exitMenu(self, event):
        #self.SaveCfgFile()
        self.Close()
        event.Skip()
    def OnLstLogsListItemSelected(self, event):
        try:
            self.idxSel=event.GetIndex()
            idxLog=self.lstLogs.GetItemData(self.idxSel)
            self.filLog.SetSel(idxLog)
            self.__updateCallStack__()
            self.__updateMsg__()
            self.__updateMark__()
            #print self.idxSel
            #self.filLog.DoCallBack(self.lstLogs.SetItemState,self.idxSel,
            #                wx.LIST_STATE_FOCUSED,
            #                wx.LIST_STATE_FOCUSED)
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()
    def OnLstLogsListItemDeselected(self, event):
        try:
            self.idxSel=-1
            self.filLog.SetSel(-1)
            self.__updateCallStack__()
            self.__updateMsg__()
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()
    def OnLstLogsLeftDclick(self, event):
        event.Skip()
        if self.idxSel<0:
            self.filLog.SetMsg2Find('')
        else:
            idxLog=self.lstLogs.GetItemData(self.idxSel)
            log=self.filLog.GetData(idxLog).split('|')
            msg=string.replace(log[3],';',chr(10))
            i=msg.find(chr(10))
            if i>=0:
                self.filLog.SetMsg2Find(msg[:i]+'*')
            else:
                self.filLog.SetMsg2Find(msg+'*')
    def OnLstLogsMouseWheel(self,event):
        if event.GetWheelRotation()<0:
            self.filLog.MoveValue(-event.GetLinesPerAction())
        else:
            self.filLog.MoveValue(event.GetLinesPerAction())
    def OnVtLogFileViewerFrameMouseWheel(self,event):
        if event.GetWheelRotation()<0:
            self.filLog.MoveValue(-event.GetLinesPerAction())
        else:
            self.filLog.MoveValue(event.GetLinesPerAction())
    def __updateMark__(self):
        return
        for idx in xrange(self.lstLogs.GetItemCount()):
            i=self.lstLogs.GetItemData(idx)
            if self.filLog.IsMark(i):
                self.lstLogs.SetItemFont(idx,self.fntItemSel)
            else:
                self.lstLogs.SetItemFont(idx,self.fntItemNor)
    def __updateMsg__(self):
        if self.idxSel<0:
            self.txtMsg.SetValue(u'')
            return
        idxLog=self.lstLogs.GetItemData(self.idxSel)
        sLog=self.filLog.GetData(idxLog).decode('utf-8')
        log=sLog.split(u'|')
        #log=self.filLog.GetData(idxLog).split(u'|')
        #vtLog.CallStack('')
        #print 'lstIdx:',self.idxSel,'idxLog:',idxLog,len(log)
        #print log
        msg=string.replace(log[3],u';',unicode(chr(10)))
        #msg=string.replace(msg,',',chr(10))
        msg=string.replace(msg,u'\n',unicode(chr(10)))
        self.lstCmp.DeleteAllItems()
        self.txtMsg.SetValue(msg)
        self.iX=-1
        self.iY=-1
        if msg.startswith('CompareLst'):
            l=msg.split('\n')
            i=0
            j=k=-1
            for s in l:
                if s.startswith('cur:'):
                    j=i
                if s.startswith('rec:'):
                    k=i
                    break
                i+=1
            if (j>=0) and (k>=0):
                self.__updateCmpByLst__(l[j:k],l[k:],bCmpLst=True)
    def __updateCmpByLst__(self,lX,lY,bCmpLst=False):
        self.lstCmp.DeleteAllItems()
        bFirst=True
        for sX,sY in zip(lX,lY):
            try:
                if bCmpLst and bFirst:
                    sX=sX[4:]
                    sY=sY[4:]
                if sX==sY:
                    imgNum=-1  #self.imgDict['def']
                else:
                    imgNum=self.imgDict['warn']
            except:
                imgNum=self.imgDict['error']
            idx=self.lstCmp.InsertImageItem(sys.maxint,imgNum)
            if sX is not None:
                self.lstCmp.SetStringItem(idx,1,sX)
            if sY is not None:
                self.lstCmp.SetStringItem(idx,2,sY)
            bFirst=False
    def __updateCallStack__(self):
        self.lstCallStack.DeleteAllItems()
        if self.idxSel<0:
            self.txtOrigin.SetValue(u'')
            return
        try:
            idxLog=self.lstLogs.GetItemData(self.idxSel)
            sLog=self.filLog.GetData(idxLog).decode('utf-8')
            log=sLog.split(u'|')
            self.txtOrigin.SetValue(log[1])
            strs=string.split(log[4],u',')
            for s in strs:
                sFile=u'???'
                sMethod=u'???'
                sLine=u'???'
                i=string.find(s,u':')
                if i>=0:
                    j=string.find(s,u'(')
                    if j>=0:
                        k=string.find(s,u')')
                        if k>=0:
                            #all ok
                            sFile=s[:i]#.decode('utf-8')
                            sMethod=s[i+1:j]#.decode('utf-8')
                            sLine=s[j+1:k]#.decode('utf-8')
                    idx=self.lstCallStack.InsertImageStringItem(sys.maxint,sLine,-1)
                    self.lstCallStack.SetStringItem(idx,1,sFile)
                    self.lstCallStack.SetStringItem(idx,2,sMethod)
        except:
            #traceback.print_exc()
            pass
    def OnVtLogFileViewerFrameSize(self, event):
        #event.Skip()
        #print 'size',event.GetSize(),self.pnMain.GetSize()
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnMain)
    def OnSlwBotSashDragged(self, event):
        if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
            return
        iHeight=event.GetDragRect().height
        #print 'bottom',iHeight
        sz=self.GetSize()
        #szTop=self.slwTop.GetSize()
        #print sz,szTop
        iH=sz[1]-200
        #print iH
        #if iHeight>iH:
        #    iHeight=iH
        self.slwBot.SetDefaultSize((1000 , iHeight))
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnMain)
        self.pnMain.Refresh()
        #event.Skip()
    def OnLogViewerProc(self,evt):
        evt.Skip()
        if evt.GetSize()==0:
            iProc=0
        else:
            iProc=int((evt.GetPos()/float(evt.GetSize()))*1000)
        self.gProcess.SetValue(iProc)
    def OnFilLogSel(self,evt):
        evt.Skip()
        iPos=evt.GetPos()
        #print 'OnFilLogSel',iPos
        for idx in xrange(self.lstLogs.GetItemCount()):
            idxLog=self.lstLogs.GetItemData(idx)
            #print idx,idxLog,iPos,idxLog==iPos
            if idxLog==iPos:
                self.lstLogs.SetItemState(idx,
                        wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
                self.lstLogs.EnsureVisible(idx)
                break
    def OnFilLogMark(self,evt):
        evt.Skip()
        iPos=evt.GetPos()
        bMark=evt.GetMark()
        for idx in xrange(self.lstLogs.GetItemCount()):
            idxLog=self.lstLogs.GetItemData(idx)
            if idxLog==iPos:
                if bMark:
                    self.lstLogs.SetItemFont(idx,self.fntItemSel)
                else:
                    self.lstLogs.SetItemFont(idx,self.fntItemNor)
    def _addLog(self,pos,line,idxLog,bSel,bMark):
        sLine=line.decode('UTF-8')
        log=sLine.split(u'|')
        if len(log)<4:
            return
        #print log
        #print pos,idxLog,bSel,
        iSel=self.lstLogs.GetFirstSelected()
        #print iSel
        if bSel:
            if iSel==-1:
                #iSel=0
                #bSel=False
                pass
            else:
                #bSel=True
                self.lstLogs.SetItemState(iSel,
                        wx.LIST_STATE_SELECTED|wx.LIST_STATE_FOCUSED,0)
                #self.lstLogs.Select(iSel,False)
        idx=self.lstLogs.InsertImageStringItem(pos, log[1], -1)
        self.lstLogs.SetStringItem(idx,1,string.strip(log[2]),-1)
        self.lstLogs.SetStringItem(idx,2,string.strip(log[3]),-1)
        self.lstLogs.SetStringItem(idx,3,string.strip(log[0]),-1)
        self.lstLogs.SetItemData(idx,idxLog)
        if bMark:
            self.lstLogs.SetItemFont(idx,self.fntItemSel)
        if bSel:
            if iSel==-1:
                wx.CallAfter(self.lstLogs.Select,0)
            elif pos==0:
                self.lstLogs.Select(iSel)
            else:
                #self.lstLogs.Select(iSel+1)
                self.lstLogs.SetItemState(iSel+1,
                        wx.LIST_STATE_SELECTED|wx.LIST_STATE_FOCUSED,0)
    def OnFilLogVtinoutFileidxlinesAdd(self, evt):
        evt.Skip()
        obj=self.filLog
        if evt.GetStr() is None:
            if evt.Is2Sel():
                self.lstLogs.Select(0)
                self.idxSel=0
                return
            self.lstLogs.DeleteAllItems()
            self.idxSel=-1
            return
        self.lstLogs.Freeze()
        try:
            bMark=self.filLog.IsMark(evt.GetIdx())
            iSel=self.lstLogs.GetFirstSelected()
            if evt.AtEnd():
                #self.lstLogs.InsertStringItem(sys.maxint,evt.GetStr())
                self._addLog(sys.maxint,evt.GetStr(),evt.GetIdx(),
                            evt.Is2Sel(),bMark)
                if self.lstLogs.GetItemCount()>obj.GetBufSize():
                    self.lstLogs.DeleteItem(0)
            else:
                #self.lstLogs.InsertStringItem(0,evt.GetStr())
                self._addLog(0,evt.GetStr(),evt.GetIdx(),evt.Is2Sel(),
                            bMark)
                if self.lstLogs.GetItemCount()>obj.GetBufSize():
                    self.lstLogs.DeleteItem(self.lstLogs.GetItemCount()-1)
            #self.lstLogs.Select(iSel)
            self.lstLogs.SetItemState(iSel,
                    wx.LIST_STATE_SELECTED|wx.LIST_STATE_FOCUSED,
                    wx.LIST_STATE_SELECTED|wx.LIST_STATE_FOCUSED)
        except:
            vtLog.vtLngTB(self.GetName())
        self.lstLogs.Thaw()
    def OnMnFileItem_closeMenu(self, event):
        event.Skip()
        try:
            self.CloseFile()
        except:
            traceback.print_exc()
    def CloseFile(self):
        try:
            self.filLog.Close()
            if self.filLog.IsClosed():
                self.Clear()
        except:
            traceback.print_exc()
    def OpenCfgFile(self,sFN):
        sIniDN=os.getenv('vidarcIniDN')
        sTmpDN,sTmpFN=os.path.split(sFN)
        bExist=False
        if sIniDN is not None:
            sTmpFN=os.path.join(sIniDN,sTmpFN)
            if os.path.exists(sTmpFN):
                sFN=sTmpFN
                bExist=True
        if bExist==False:
            if os.path.exists(sFN):
                bExist=True
        if bExist==False:
            return
        self.cfg.readFN(sFN)
        sect=self.cfg.getSection('layout')
        try:
            iX=int(sect.getInfo('x'))
            iY=int(sect.getInfo('y'))
            iWidth=int(sect.getInfo('width'))
            iHeight=int(sect.getInfo('height'))
            iBottomHeight=int(sect.getInfo('bottom_height'))
            iX,iY,iWidth,iHeight=vSystem.LimitWindowToScreen(iX,iY,iWidth,iHeight)
            
            if iBottomHeight>(iHeight/2):
                iBottomHeight=iHeight/2
            if iBottomHeight<30:
                iBottomHeight=30
            self.Move((iX,iY))
            self.SetSize((iWidth,iHeight))
            self.slwBot.SetDefaultSize((1000 , iBottomHeight))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnMain)
            self.pnMain.Refresh()
        except:
            pass
        l=[]
        sect=self.cfg.getSection('find')
        if sect is not None:
            for i in xrange(50):
                val=sect.getInfo('store_%02d'%i)
                if val is not None:
                    v=val.strip()
                    if len(v)>0:
                        l.append(val)
        self.filLog.SetFindStoreLst(l)
    def SaveCfgFile(self,sFN=None):
        l=self.filLog.GetFindStoreLst()
        sect=self.cfg.getSection('layout')
        pos=self.GetPosition()
        sz=self.GetSize()
        sect.setInfo('x',str(pos[0]))
        sect.setInfo('y',str(pos[1]))
        sect.setInfo('width',str(sz[0]))
        sect.setInfo('height',str(sz[1]))
        sect.setInfo('bottom_height',str(self.slwBot.GetSize()[1]))
        sect=self.cfg.getSection('find')
        i=0
        iCount=len(l)
        i=iCount-50
        if i<0:
            i=0
        iNum=0
        for s in l[i:]:
            sect.setInfo('store_%02d'%iNum,s)
            iNum+=1
        for i in xrange(iNum,50):
            sect.setInfo('store_%02d'%i,'')
        self.cfg.writeFN(sFN)
    def OnMnFileMnfile_cfg_openMenu(self, event):
        event.Skip()
        dlg = wx.FileDialog(self, _(u"Open Config"), "", "", _(u"ini files (*.ini)|*.ini|all files (*.*)|*.*"), wx.OPEN)
        try:
            fn=self.GetFN()
            if fn is not None:
                sLogDN=os.getenv('vidarcIniDN')
                if sLogDN is not None:
                    sTmpDN,sTmpFN=os.path.split(fn)
                    dlg.SetPath(os.path.join(sLogDN,sTmpFN))
                else:
                    dlg.SetPath(fn)
            else:
                sLogDN=os.getenv('vidarcIniDN')
                if sLogDN is not None:
                    dlg.SetPath(sLogDN+os.path.sep)
                else:
                    sLogDN=os.getenv('vidarcLogDN')
                    if sLogDN is not None:
                        dlg.SetPath(sLogDN+os.path.sep)
            #else:
            #    dlg.SetPath('.')
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.OpenCfgFile(filename)
                #self.__setModified__(False)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
    def OnMnFileMnfile_save_cfgMenu(self, event):
        event.Skip()
        dlg = wx.FileDialog(self, _("Save Config"), ".", "", _("ini files (*.ini)|*.ini|all files (*.*)|*.*"), wx.SAVE)
        try:
            fn=self.GetFN()
            if fn is not None:
                dlg.SetPath(fn[:-3]+'ini')
            else:
                sLogDN=os.getenv('vidarcIniDN')
                if sLogDN is not None:
                    dlg.SetPath(sLogDN+os.path.sep)
                else:
                    sLogDN=os.getenv('vidarcLogDN')
                    if sLogDN is not None:
                        dlg.SetPath(sLogDN+os.path.sep)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.SaveCfgFile(filename)
        finally:
            dlg.Destroy()
    def OnMnHelpMn_help_aboutMenu(self, event):
        event.Skip()
        try:
            tup=self.GetAboutData()
            dlg=vAboutDialog.create(self,images.getApplicationBitmap(),
                    tup[0],tup[1],tup[2],False)
            dlg.Centre()
            dlg.ShowModal()
            dlg.Destroy()
        except:
            traceback.print_exc()
    def GetAboutData(self):
        import __config__
        version=u' %d.%d.%d.%d'%(__config__.LOG_FILE_VIEWER_VER_MAJOR,
                    __config__.LOG_FILE_VIEWER_VER_MINOR,
                    __config__.LOG_FILE_VIEWER_VER_RELEASE,
                    __config__.LOG_FILE_VIEWER_VER_SUBREL)
        desc=_(u"""Graphical log file viewer to analyse VIDARC log outputs.

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
""")
        return _(u'VIDARC log-file viewer'),desc,version

    def OnCbCmpSet1Button(self, event):
        event.Skip()
        self.iX=self.txtMsg.GetInsertionPoint()
    def OnCbCmpSet2Button(self, event):
        event.Skip()
        self.iY=self.txtMsg.GetInsertionPoint()
    def OnCbCmpUpdateButton(self, event):
        event.Skip()
        if self.iX<0:
            return
        if self.iY<0:
            return
        sMsg=self.txtMsg.GetValue()
        lX=sMsg[self.iX:self.iY].split('\n')
        lY=sMsg[self.iY:].split('\n')
        #self.lstCmp.DeleteAllItems()
        self.__updateCmpByLst__(lX,lY)
    def __checkIdxChange__(self,idx,iD):
        if self.idxSel==idx:
            self.lstLogs.SetItemState(self.idxSel,
                    wx.LIST_STATE_SELECTED|wx.LIST_STATE_FOCUSED,0)
            if self.filLog.IsReverse():
                if iD<0:
                    self.filLog.OnLeft(None)
                elif iD>0:
                    self.filLog.OnRight(None)
            else:
                if iD<0:
                    self.filLog.OnRight(None)
                elif iD>0:
                    self.filLog.OnLeft(None)
            self.filLog.CallBack(self.lstLogs.SetItemState,self.idxSel,
                            wx.LIST_STATE_SELECTED|wx.LIST_STATE_FOCUSED,
                            wx.LIST_STATE_SELECTED|wx.LIST_STATE_FOCUSED)
    def OnLstLogsChar(self, event):
        event.Skip()
        c=event.GetKeyCode()
        if c==wx.WXK_UP:
            iD=1
        elif c==wx.WXK_DOWN:
            iD=-1
        else:
            iD=0
        wx.CallAfter(self.__checkIdxChange__,self.idxSel,iD)
