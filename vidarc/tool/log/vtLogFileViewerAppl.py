#!/usr/bin/env python
#Boa:App:BoaApp
#----------------------------------------------------------------------------
# Name:         vtLogFileViewerAppl.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtLogFileViewerAppl.py,v 1.6 2010/02/21 15:54:00 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os,sys,getopt
import vSystem
import wx
import vtLogDef
import vtLgBase

#import vidarc.tool.lang.vtLgBase as vtLgBase
#import vidarc.vApps.common.vSplashFrame as vSplashFrame

modules ={u'vtLogFileViewerFrame': [1,'Main frame of Application',u'vtLogFileViewerFrame.py']}

class BoaApp(wx.App):
    def RedirectStdio(self, filename=None):
        """Redirect sys.stdout and sys.stderr to a file or a popup window."""
        if filename:
            sys.stderr = open(filename, 'w')
            sys.stdout = open(filename.replace('.err.','.out.'), 'w')
            #if self.iLogLv==vtLogDef.DEBUG or self.iLogLv==vtLogDef.INFO:
            #    sys.stdout = self.outputWindowClass()
            #else:
            #    sys.stdout = sys.stderr
        else:
            self.stdioWin = self.outputWindowClass()
            sys.stdout = sys.stderr = self.stdioWin
    def OnInit(self):
        #self.RedirectStdio(vSystem.getErrFN('vtLogFileViewer'))
        OPT_SHORT,OPT_LONG='l:f:c:h',['lang=','file=','help']
        vtLgBase.initAppl(OPT_SHORT,OPT_LONG,'log')
        wx.InitAllImageHandlers()
        #wx.HandleFatalExceptions()
        #wx.Log_SetActiveTarget(wx.LogStderr())
        #wx.Log_SetTraceMask(0|
                #wx.TraceMessages|
                #wx.TraceMemAlloc|
                #wx.TraceResAlloc|
                #wx.TraceRefCount|
        #        0)
        #wx.Log_AddTraceMask(wx.TraceMemAlloc)
        #wx.Log_AddTraceMask(wx.TRACE_MemAlloc)
        #wx.Log_AddTraceMask(wx.TRACE_ResAlloc)
        #wx.Log_AddTraceMask(wx.TRACE_RefCount)
        #wx.Log_AddTraceMask(wx.TRACE_Messages)
        #vtLgBase.initAppl(optshort,optlong,'log')
        from vidarc.tool.log.vtLogFileViewerFrame import create#vtLogFileViewerFrame
        #self.main = vtLogFileViewerFrame.create(None)
        self.main = create(None)
        self.main.Show()
        self.SetTopWindow(self.main)
        return True
    def OpenFile(self,fn):
        self.main.OpenFile(fn)
    def OpenCfgFile(self,fn):
        self.main.OpenCfgFile(fn)
    def OnFatalException(self):
        print 'fatal'
        #try:
        #    vtLog.vtLngCurWX(vtLog.FATAL,''%(),self.main)
        #except:
        #    vtLog.vtLngCurCls(vtLog.FATAL,''%(),self)
    def OnExceptionInMainLoop(self):
        print 'OnExceptionInMainLoop'
        return False
    def OnUnhandledException(self):
        print 'OnUnhandledException'
    
def showHelp():
    print _('help')
    print sys.version
    print "  ",_("valid flags:")
    print "  ",_("-f <filename>")
    print "  ",_("--file <filename>")
    print "        ",_("open the <filename> at start")
    print "  ","-h"
    print "  ",_("--help")
    print "        ",_("show this screen")
    print "  ","-l"
    print "  ",_("--lang <language id according ISO639>")

def main():
    optshort,optlong='l:f:c:h',['lang=','file=','help']
    #vtLgBase.initAppl(optshort,optlong,'log')
    
    fn=None
    cfgFN='vtLogFileViewer.ini'
    try:
        optlist , args = getopt.getopt(sys.argv[1:],'l:f:c:h',['lang=','file=','config=','help'])
        for o in optlist:
            if o[0] in ['-h','--help']:
                vtLgBase.initAppl(optshort,optlong,'log')
                showHelp()
            
                return
            if o[0] in ['--file','-f']:
                fn=o[1]
            if o[0] in ['--config','-c']:
                cfgFN=o[1]
    except:
        vtLgBase.initAppl(optshort,optlong,'log')
        showHelp()
    if fn is None:
        if len(sys.argv)==2:
            fn=sys.argv[-1]
    application = BoaApp(0)
    if fn is not None:
        application.OpenFile(fn)
    if cfgFN is not None:
        application.OpenCfgFile(cfgFN)
    application.MainLoop()

if __name__ == '__main__':
    main()
