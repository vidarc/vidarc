#----------------------------------------------------------------------------
# Name:         vtLogFile.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20061123
# CVS-ID:       $Id: vtLogFile.py,v 1.20 2010/08/08 12:08:34 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.gizmos
import vidarc.tool.art.vtThrobber

import fnmatch,time,traceback

from vidarc.tool.InOut.vtInOutFileIdxLines import vtInOutFileIdxLines
from vidarc.tool.InOut.vtInOutFileIdxLinesWid import vtInOutFileIdxLinesWid
from vidarc.tool.InOut.vtInOutFileIdxLinesWid import vtInOutFieldIdxLinesTransientPopup
from vidarc.tool.InOut.vtInOutFileIdxLinesWid import vtInOutBufIndicator
import vidarc.tool.vtThread
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.time.vtTimeLoadBound as vtLdBd
from vidarc.tool.misc.vtmMsgDialog import vtmMsgDialog

VERBOSE_FIND=0

wxEVT_VTLOG_FILE_PROC=wx.NewEventType()
vEVT_VTLOG_FILE_PROC=wx.PyEventBinder(wxEVT_VTLOG_FILE_PROC,1)
def EVT_VTLOG_FILE_PROC(win,func):
    win.Connect(-1,-1,wxEVT_VTLOG_FILE_PROC,func)
class vtLogFileProc(wx.PyEvent):
    """
    Posted Events:
        Tree Item added event, reports the parent node of the added one
            EVT_VTLOG_FILE_PROC(<widget_name>, self.OnProc)
    """
    def __init__(self,obj,pos,size):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTLOG_FILE_PROC)
        self.obj=obj
        self.pos=pos
        self.size=size
    def GetPos(self):
        return self.pos
    def GetSize(self):
        return self.size

wxEVT_VTLOG_FILE_SEL=wx.NewEventType()
vEVT_VTLOG_FILE_SEL=wx.PyEventBinder(wxEVT_VTLOG_FILE_SEL,1)
def EVT_VTLOG_FILE_SEL(win,func):
    win.Connect(-1,-1,wxEVT_VTLOG_FILE_SEL,func)
class vtLogFileSel(wx.PyEvent):
    """
    Posted Events:
        Tree Item added event, reports the parent node of the added one
            EVT_VTLOG_FILE_SEL(<widget_name>, self.OnSel)
    """
    def __init__(self,obj,pos):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTLOG_FILE_SEL)
        self.obj=obj
        self.pos=pos
    def GetPos(self):
        return self.pos

wxEVT_VTLOG_FILE_MARK=wx.NewEventType()
vEVT_VTLOG_FILE_MARK=wx.PyEventBinder(wxEVT_VTLOG_FILE_MARK,1)
def EVT_VTLOG_FILE_MARK(win,func):
    win.Connect(-1,-1,wxEVT_VTLOG_FILE_MARK,func)
class vtLogFileMark(wx.PyEvent):
    """
    Posted Events:
        Tree Item added event, reports the parent node of the added one
            EVT_VTLOG_FILE_MARK(<widget_name>, self.OnSel)
    """
    def __init__(self,obj,pos,bFlag):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTLOG_FILE_MARK)
        self.obj=obj
        self.pos=pos
        self.flag=bFlag
    def GetPos(self):
        return self.pos
    def GetMark(self):
        return self.flag

class vtLogFileTransientPopup(vtInOutFieldIdxLinesTransientPopup):
    MAP_LEVEL={'DEBUG':2,'INFO':3,'WARN':4,'WARNING':4,'ERROR':5,
            'CRITICAL':6,'FATAL':7,'':8}
    def __init__(self, parent, size,style,name=''):
        vtInOutFieldIdxLinesTransientPopup.__init__(self,
                parent,size,style,name)
        
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        bxs.AddSpacer(wx.Size(30, 8), border=0, flag=0)
        bxs.AddSpacer(wx.Size(30, 8), border=0, flag=0)
        #bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.cbBuild = wx.lib.buttons.GenBitmapTextButton(ID=-1,
              bitmap=vtArt.getBitmap(vtArt.Build), label=_(u'Build'),
              name=u'cbBuild', parent=self, 
              size=wx.Size(124, 30), style=0)
        self.cbBuild.SetMinSize((-1,-1))
        self.cbBuild.Bind(wx.EVT_BUTTON, self.OnCbBuildButton,
              self.cbBuild)
        bxs.AddWindow(self.cbBuild, 1, border=4, flag=wx.LEFT|wx.RIGHT|wx.EXPAND)
        
        self.cbOpen = wx.lib.buttons.GenBitmapTextButton(ID=-1,
              bitmap=vtArt.getBitmap(vtArt.OpenFile), label=_(u'Open'),
              name=u'cbOpen', parent=self, 
              size=wx.Size(124, 30), style=0)
        self.cbOpen.SetMinSize((-1,-1))
        self.cbOpen.Bind(wx.EVT_BUTTON, self.OnCbOpenButton,
              self.cbOpen)
        #st = wx.StaticText(self, -1,'',style=wx.ALIGN_RIGHT)
        bxs.AddWindow(self.cbOpen, 1, border=4, flag=wx.LEFT|wx.EXPAND)
        
        #st = wx.StaticText(self, -1,'',style=wx.ALIGN_RIGHT)
        self.cbClose = wx.lib.buttons.GenBitmapTextButton(ID=-1,
              bitmap=vtArt.getBitmap(vtArt.CloseFile), label=_(u'Close'),
              name=u'cbClose', parent=self, 
              size=wx.Size(124, 30), style=0)
        self.cbClose.SetMinSize((-1,-1))
        self.cbClose.Bind(wx.EVT_BUTTON, self.OnCbCloseButton,
              self.cbClose)
        bxs.AddWindow(self.cbClose, 1, border=4, flag=wx.LEFT|wx.EXPAND)
        
        bxsMark = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.tgMark = wx.lib.buttons.GenBitmapToggleButton(ID=-1,
              bitmap=wx.EmptyBitmap(16, 16), name=u'tgFindDir',
              parent=self, size=wx.Size(30, 30),
              style=0)
        self.tgMark.SetBitmapLabel(vtArt.getBitmap(vtArt.Pin))
        self.tgMark.SetBitmapSelected(vtArt.getBitmap(vtArt.Pin))
        self.tgMark.Bind(wx.EVT_BUTTON, self.OnTgMarkButton,
              self.tgMark)
        bxsMark.AddWindow(self.tgMark, 0, border=4, flag=wx.LEFT|wx.RIGHT)
        
        self.cbMoveMarkPrev = wx.lib.buttons.GenBitmapButton(ID=-1,
              bitmap=vtArt.getBitmap(vtArt.Left), 
              name=u'cbMoveMarkPrev', parent=self, 
              size=wx.Size(30, 30), style=0)
        #self.cbMoveMarkPrev.SetMinSize((-1,-1))
        self.cbMoveMarkPrev.Bind(wx.EVT_BUTTON, self.OnCbMoveMarkPrevButton,
              self.cbMoveMarkPrev)
        bxsMark.AddWindow(self.cbMoveMarkPrev, 0, border=4, flag=wx.RIGHT)
        
        self.cbMoveMarkNext = wx.lib.buttons.GenBitmapButton(ID=-1,
              bitmap=vtArt.getBitmap(vtArt.Right), 
              name=u'cbMoveMarkNext', parent=self, 
              size=wx.Size(30, 30), style=0)
        #self.cbMoveMarkNext.SetMinSize((-1,-1))
        self.cbMoveMarkNext.Bind(wx.EVT_BUTTON, self.OnCbMoveMarkNextButton,
              self.cbMoveMarkNext)
        bxsMark.AddWindow(self.cbMoveMarkNext, 0, border=4, flag=0)
        
        st = wx.StaticText(self, -1,'',style=wx.ALIGN_RIGHT)
        bxsMark.AddWindow(st, 1, border=16, flag=wx.LEFT|wx.EXPAND)
        bxs.AddSizer(bxsMark, 1,border=0, flag=wx.EXPAND)
        
        self.fgsMain.AddSizer(bxs,1,border=4,flag=wx.TOP|wx.EXPAND)
        
        self.trlstOrigin = wx.gizmos.TreeListCtrl(id=-1,
              name=u'trlstOrigin', parent=self,size=wx.Size(124, 150),
              style=wx.TR_DEFAULT_STYLE|wx.TR_HAS_BUTTONS|wx.TR_MULTIPLE|wx.TR_FULL_ROW_HIGHLIGHT)
        self.trlstOrigin.AddColumn(text=u'origin')
        self.trlstOrigin.AddColumn(text=u'X')
        self.trlstOrigin.AddColumn(text=u'D')
        self.trlstOrigin.AddColumn(text=u'I')
        self.trlstOrigin.AddColumn(text=u'W')
        self.trlstOrigin.AddColumn(text=u'E')
        self.trlstOrigin.AddColumn(text=u'C')
        self.trlstOrigin.AddColumn(text=u'F')
        self.trlstOrigin.AddColumn(text=u'V')
        self.trlstOrigin.SetColumnWidth(0,150)
        self.trlstOrigin.SetColumnWidth(1,30)
        self.trlstOrigin.SetColumnWidth(2,60)
        self.trlstOrigin.SetColumnAlignment(2,wx.gizmos.TL_ALIGN_RIGHT)
        self.trlstOrigin.SetColumnWidth(3,60)
        self.trlstOrigin.SetColumnAlignment(3,wx.gizmos.TL_ALIGN_RIGHT)
        self.trlstOrigin.SetColumnWidth(4,60)
        self.trlstOrigin.SetColumnAlignment(4,wx.gizmos.TL_ALIGN_RIGHT)
        self.trlstOrigin.SetColumnWidth(5,60)
        self.trlstOrigin.SetColumnAlignment(5,wx.gizmos.TL_ALIGN_RIGHT)
        self.trlstOrigin.SetColumnWidth(6,60)
        self.trlstOrigin.SetColumnAlignment(6,wx.gizmos.TL_ALIGN_RIGHT)
        self.trlstOrigin.SetColumnWidth(7,60)
        self.trlstOrigin.SetColumnAlignment(7,wx.gizmos.TL_ALIGN_RIGHT)
        self.trlstOrigin.SetColumnWidth(8,60)
        self.trlstOrigin.SetColumnAlignment(8,wx.gizmos.TL_ALIGN_RIGHT)
        self.imgDict={}
        self.imgLstTyp=wx.ImageList(16,16)
        self.imgDict['include']=self.imgLstTyp.Add(vtArt.getBitmap(vtArt.Apply))
        self.imgDict['exclude']=self.imgLstTyp.Add(vtArt.getBitmap(vtArt.Cancel))
        self.trlstOrigin.SetImageList(self.imgLstTyp)
        self.fgsMain.AddWindow(self.trlstOrigin, 1, border=4, flag=wx.LEFT|wx.RIGHT|wx.TOP|wx.BOTTOM|wx.EXPAND)
        self.fgsMain.AddGrowableRow(2)
        wx.EVT_TREE_ITEM_RIGHT_CLICK(self, self.trlstOrigin.GetId(),
                self.OnTreeTreeLstOriginRight)
        
        #wx.EVT_RIGHT_UP(self,self.trlstOrigin.GetId(),self.OnRightUp)
        #self.trlstOrigin.Bind(wx.EVT_RIGHT_UP, self.OnRightUp)
        #wx.EVT_RIGHT_UP(self,self.OnRightUp)
        
        st = wx.StaticText(self, -1,_(u'search parameter'),style=wx.ALIGN_RIGHT)
        self.fgsMain.AddWindow(st, 1, border=4, flag=wx.LEFT|wx.RIGHT|wx.TOP|wx.ALIGN_CENTER)
        #bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        #bxs.AddWindow(st, 1, border=4, flag=wx.RIGHT|wx.EXPAND)
        #st = wx.StaticText(self, -1,'',style=wx.ALIGN_RIGHT)
        #bxs.AddWindow(st, 5, border=4, flag=wx.RIGHT|wx.EXPAND)
        #self.fgsMain.AddSizer(bxs,1,border=0,flag=wx.EXPAND)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        st = wx.StaticText(self, -1,_(u'level'),style=wx.ALIGN_RIGHT)
        bxs.AddWindow(st, 1, border=4, flag=wx.RIGHT|wx.EXPAND)
        self.chcFindOrigin = wx.Choice(choices=['---','debug','info','warn',
              'warning','error','critical','fatal'],
              id=-1, name=u'chcFindOrigin',
              parent=self,style=0)
        bxs.AddWindow(self.chcFindOrigin, 5, border=4, flag=wx.RIGHT|wx.EXPAND)
        self.tgFindDir = wx.lib.buttons.GenBitmapToggleButton(ID=-1,
              bitmap=wx.EmptyBitmap(16, 16), name=u'tgFindDir',
              parent=self, size=wx.Size(31, 30),
              style=0)
        self.tgFindDir.SetBitmapLabel(vtArt.getBitmap(vtArt.Down))
        self.tgFindDir.SetBitmapSelected(vtArt.getBitmap(vtArt.Up))
        bxs.AddWindow(self.tgFindDir, 0, border=4, flag=wx.RIGHT)
        self.fgsMain.AddSizer(bxs,1,border=0,flag=wx.EXPAND)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        st = wx.StaticText(self, -1,_(u'message'),style=wx.ALIGN_RIGHT)
        bxs.AddWindow(st, 1, border=4, flag=wx.RIGHT|wx.EXPAND)
        self.txtFindMsg = wx.TextCtrl(id=-1,
              name=u'txtFindMsg', parent=self, style=0, value=u'')
        bxs.AddWindow(self.txtFindMsg, 5, border=4, flag=wx.RIGHT|wx.EXPAND)
        self.tgCase = wx.lib.buttons.GenBitmapToggleButton(ID=-1,
              bitmap=wx.EmptyBitmap(16, 16), name=u'tgCase',
              parent=self, size=wx.Size(31, 30),
              style=0)
        self.tgCase.SetBitmapLabel(vtArt.getBitmap(vtArt.Glue))
        self.tgCase.SetBitmapSelected(vtArt.getBitmap(vtArt.RubberBand))
        bxs.AddWindow(self.tgCase, 0, border=4, flag=wx.RIGHT)
        self.fgsMain.AddSizer(bxs,1,border=0,flag=wx.EXPAND)
        
        # memorized search parameters
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        st = wx.StaticText(self, -1,_(u'stored'),style=wx.ALIGN_RIGHT)
        bxs.AddWindow(st, 1, border=4, flag=wx.RIGHT|wx.EXPAND)
        
        bxs2 = wx.BoxSizer(orient=wx.HORIZONTAL)
        bxs.AddSizer(bxs2,5,border=0,flag=wx.EXPAND)
        self.chcFindStored = wx.Choice(choices=['---'],
              id=-1, name=u'chcFindStored',
              parent=self,style=0)
        self.chcFindStored.Bind(wx.EVT_CHOICE, self.OnChoiceFindStore)
        bxs2.AddWindow(self.chcFindStored, 2, border=4, flag=wx.RIGHT|wx.EXPAND)
        self.cbFindStoreDel = wx.lib.buttons.GenBitmapButton(ID=-1,
              bitmap=vtArt.getBitmap(vtArt.Del), name=u'cbFindStoreDel',
              parent=self, size=wx.Size(31, 30),
              style=0)
        bxs2.AddWindow(self.cbFindStoreDel, 0, border=4, flag=wx.RIGHT)
        self.cbFindStoreDel.Bind(wx.EVT_BUTTON, self.OnFindStoreDel)
        st = wx.StaticText(self, -1,_(u'new name'),style=wx.ALIGN_RIGHT)
        bxs2.AddWindow(st, 1, border=4, flag=wx.RIGHT|wx.EXPAND)
        self.txtFindStoreName = wx.TextCtrl(id=-1, name='txtFindStoreName',
              parent=self, pos=wx.Point(16, 0), size=wx.Size(100, 21), style=0,
              value='')
        bxs2.AddWindow(self.txtFindStoreName, 2, border=4, flag=wx.RIGHT|wx.EXPAND)
        self.cbFindStoreAdd = wx.lib.buttons.GenBitmapButton(ID=-1,
              bitmap=vtArt.getBitmap(vtArt.Add), name=u'cbFindStoreAdd',
              parent=self, size=wx.Size(31, 30),
              style=0)
        bxs.AddWindow(self.cbFindStoreAdd, 0, border=4, flag=wx.RIGHT)
        self.cbFindStoreAdd.Bind(wx.EVT_BUTTON, self.OnFindStoreAdd)
        self.fgsMain.AddSizer(bxs,1,border=0,flag=wx.EXPAND)
        
        self.chcFindOrigin.SetSelection(0)
        self.chcFindStored.SetSelection(0)
        self.lFindStored=[]
        self.dFindStore={}
        self.iFindStore=0
        
        self._clr()
        self.fgsMain.Layout()
        self.fgsMain.Fit(self)
        self.szOrig=self.GetClientSize()
        self.szOrig=(max(560,self.szOrig[0]),max(350,self.szOrig[1]))
        self.SetSize(self.szOrig)
    def OnSize(self,evt):
        iW,iH=self.GetSize()
        if iW<250:
            iW=250
        if iH<45:
            iH=45
        self.SetSize((iW,iH))
        self.Layout()
    def OnCbOpenButton(self,evt):
        evt.Skip()
        par=self.GetParent()
        try:
            if par.IsRunning():
                dlg=vtmMsgDialog(self,_(u'Thread is already busy, try later again.'),
                                'vtLogFile',
                                wx.OK|wx.ICON_EXCLAMATION )
                dlg.Centre()
                dlg.Destroy()
                return
            par.SetBufSize(self.spLimit.GetValue(),bUpdate=False)
            par.SetReverse(self.chcRev.GetValue(),bUpdate=False)
            while par is not None:
                if hasattr(par,'OpenFile'):
                    par.OpenFile()
                    break
                else:
                    par=par.GetParent()
            #par.Open(par.fIn)#(bForce=False)
        except:
            pass
    def OnCbCloseButton(self,evt):
        evt.Skip()
        par=self.GetParent()
        try:
            if par.IsRunning():
                dlg=vtmMsgDialog(self,_(u'Thread is already busy, try later again.'),
                                'vtLogFile',
                                wx.OK|wx.ICON_EXCLAMATION )
                dlg.Centre()
                dlg.Destroy()
                return
            par.SetBufSize(self.spLimit.GetValue(),bUpdate=False)
            par.SetReverse(self.chcRev.GetValue(),bUpdate=False)
            while par is not None:
                if hasattr(par,'CloseFile'):
                    par.CloseFile()
                    break
                else:
                    par=par.GetParent()
            #par.Open(par.fIn)#(bForce=False)
        except:
            pass
    def OnCbReloadButton(self,evt):
        evt.Skip()
        par=self.GetParent()
        try:
            if par.IsRunning():
                dlg=vtmMsgDialog(self,_(u'Thread is already busy, try later again.'),
                                'vtLogFile',
                                wx.OK|wx.ICON_EXCLAMATION )
                dlg.Centre()
                dlg.Destroy()
                return
            par.SetBufSize(self.spLimit.GetValue(),bUpdate=False)
            par.SetReverse(self.chcRev.GetValue(),bUpdate=False)
            par.Index(bForce=False)
        except:
            pass
    def SetMark(self,bFlag):
        self.tgMark.SetValue(bFlag)
    def OnTgMarkButton(self,evt):
        evt.Skip()
        par=self.GetParent()
        try:
            par.SetMark(par.GetSel(),self.tgMark.GetValue())
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbMoveMarkPrevButton(self,evt):
        evt.Skip()
        par=self.GetParent()
        try:
            par.MoveMarkPrev(par.GetSel())
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbMoveMarkNextButton(self,evt):
        evt.Skip()
        par=self.GetParent()
        try:
            par.MoveMarkNext(par.GetSel())
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbBuildButton(self,evt):
        evt.Skip()
        try:
            par=self.GetParent()
            if par.IsRunning():
                dlg=vtmMsgDialog(self,_(u'Thread is already busy, try later again.'),
                                'vtLogFile',
                                wx.OK|wx.ICON_EXCLAMATION )
                dlg.Centre()
                dlg.Destroy()
                return
            par.SetBufSize(self.spLimit.GetValue(),bUpdate=False)
            par.SetReverse(self.chcRev.GetValue(),bUpdate=False)
            par.Index(bForce=True)
        except:
            vtLog.vtLngTB(self.GetName())
    def _addOrigin(self,tip,dOrigin,lOrigin,iPos,iLen):
        sOrigin=u'.'.join(lOrigin[:iPos+1])
        if sOrigin in dOrigin:
            ti,bSel,dCount=dOrigin[sOrigin]
            if ti is not None:
                if iPos<iLen:
                    self._addOrigin(ti,dOrigin,lOrigin,iPos+1,iLen)
                return
        else:
            bSel=True
            dOrigin[sOrigin]=[None,True,None]
            dCount=None
        if bSel:
            img=self.imgDict['include']
        else:
            img=self.imgDict['exclude']
        tic=self.trlstOrigin.AppendItem(tip,lOrigin[iPos])
        self.trlstOrigin.SetItemHasChildren(tic)
        self.trlstOrigin.SetItemImage(tic,img,0,which=wx.TreeItemIcon_Normal)
        if bSel:
            self.trlstOrigin.SetItemText(tic,'X',1)
        dOrigin[sOrigin][0]=tic
        #dCount=dOrigin[sOrigin][2]
        if dCount is not None:
            keys=[(k.strip(),k) for k in dCount.keys()]
            keys.sort()
            iVar=0
            for k,kOrig in keys:
                if k in self.MAP_LEVEL:
                    self.trlstOrigin.SetItemText(tic,str(dCount[kOrig]),self.MAP_LEVEL[k])
                else:
                    iVar+=dCount[kOrig]
            self.trlstOrigin.SetItemText(tic,str(iVar),self.MAP_LEVEL[''])
        if iPos<iLen:
            #print iPos,iLen,lOrigin
            self._addOrigin(tic,dOrigin,lOrigin,iPos+1,iLen)
    def _updateOrigin(self):
        par=self.GetParent()
        dOrigin=par.GetOriginDict()
        keys=dOrigin.keys()
        keys.sort()
        tip=self.trlstOrigin.GetRootItem()
        for sOrigin in keys:
            lOrigin=sOrigin.split('.')
            self._addOrigin(tip,dOrigin,lOrigin,0,len(lOrigin))
        tp=self.trlstOrigin.GetRootItem()
        self.trlstOrigin.Expand(tp)
    def _clr(self):
        self.trlstOrigin.DeleteAllItems()
        ti=self.trlstOrigin.AddRoot(_('origin'))
        self.trlstOrigin.SetItemText(ti,'X',1)
    def OnTreeTreeLstOriginRight(self,evt):
        #print 'OnTreeTreeLstOriginRight'
        tn=evt.GetItem()
        sSel=self.trlstOrigin.GetItemText(tn,1)
        self._modSel(tn,sSel)
    def OnFindStoreAdd(self,evt):
        evt.Skip()
        sName=self.txtFindStoreName.GetValue()
        if len(sName)==0:
            dlg=vtmMsgDialog(self,_(u'Please enter a name first!'),
                    'vtLogFile',wx.OK|wx.ICON_EXCLAMATION)
            dlg.Centre()
            dlg.ShowModal()
            self.txtFindStoreName.SetFocus()
            dlg.Destroy()
            return
        if sName =='---':
            dlg=vtmMsgDialog(self,_(u'Please enter different name!'),
                    'vtLogFile',wx.OK|wx.ICON_EXCLAMATION)
            dlg.Centre()
            dlg.ShowModal()
            self.txtFindStoreName.SetValue('')
            self.txtFindStoreName.SetFocus()
            dlg.Destroy()
            return
        if sName in self.dFindStore:
            dlg=vtmMsgDialog(self,_(u'Name %s already used, set to new search parameters?\n\nYES  ... will use it anyway\n NO  ... keep present.'),
                    'vtLogFile',wx.YES_NO|wx.YES_DEFAULT|wx.ICON_EXCLAMATION)
            dlg.Centre()
            if dlg.ShowModal()==wx.ID_NO:
                dlg.Destroy()
                self.txtFindStoreName.SetFocus()
                return
            dlg.Destroy()
            return
        iLv=self.chcFindOrigin.GetSelection()
        sFlt=self.txtFindMsg.GetValue()
        iCase=self.tgCase.GetValue()
        self.__addFindStore__(sName,iLv,sFlt,iCase)
    def __addFindStore__(self,sName,iLv,sFlt,iCase):
        self.iFindStore+=1
        #iNum=len(self.lFindStore)
        t=(sName,iLv,sFlt,iCase,self.iFindStore)
        self.lFindStore.append(t)
        self.lFindStore.sort()
        self.dFindStore[sName]=t
        keys=self.dFindStore.keys()
        keys.sort()
        iNum=keys.index(sName)
        self.chcFindStored.Insert(sName,iNum+1)
    def OnFindStoreDel(self,evt):
        evt.Skip()
        sName=self.chcFindStored.GetStringSelection()
        if sName=='---':
            return
        iNum=self.chcFindStored.GetSelection()
        self.chcFindStored.Delete(iNum)
        del self.dFindStore[sName]
        iNum=0
        for t in self.lFindStore:
            if t[0]==sName:
                break
            iNum+=1
        del self.lFindStore[iNum]
        self.chcFindStored.SetSelection(iNum)
    def OnChoiceFindStore(self,evt):
        evt.Skip()
        i=self.chcFindStored.GetSelection()
        if i<1:
            return
        sName=self.chcFindStored.GetStringSelection()
        #iNum=0
        for t in self.lFindStore:
            if t[0]==sName:
                self.chcFindOrigin.SetSelection(t[1])
                self.txtFindMsg.SetValue(t[2])
                self.tgCase.SetValue(t[3])
                break
            #iNum+=1
        #self.lFindStore[iNum]
        
    def OnRightUp(self,evt):
        #print 'OnRightUp'
        pos = evt.GetPosition()
        item, flags, col = self.trlstOrigin.HitTest(pos)
        #print item,flags,col
    def SetFindStoreLst(self,l):
        self.lFindStore=[]
        self.dFindStore={}
        self.iFindStore=0
        self.chcFindStored.Clear()
        self.chcFindStored.Append('---')
        for s in l:
            ll=s.split(';')
            sName=ll[0]
            iLv=int(ll[1])
            #sFlt=ll[2]
            #iCase=int(ll[3])
            sFlt=';'.join(ll[2:-1])
            iCase=int(ll[-1])
            self.__addFindStore__(sName,iLv,sFlt,iCase)
        self.chcFindStored.SetSelection(0)
    def GetFindStoreLst(self):
        l=[]
        def cmpNum(a,b):
            return a[4]-b[4]
        self.lFindStore.sort(cmpNum)
        for ll in self.lFindStore:
            s='%s;%d;%s;%d'%(ll[0],ll[1],ll[2],ll[3])
            l.append(s)
        self.lFindStore.sort()
        return l
    def _modSel(self,tn,sSel):
        if len(sSel)>0:
            sSel=''
            img=self.imgDict['exclude']
            bSel=False
        else:
            sSel='X'
            img=self.imgDict['include']
            bSel=True
        l=[]
        try:
            ti=tn
            while 1:
                sVal=self.trlstOrigin.GetItemText(ti,0)
                l.append(sVal)
                ti=self.trlstOrigin.GetItemParent(ti)
        except:
            pass
        l.reverse()
        sOrigin='.'.join(l[1:])
        dOrigin=self.GetParent().GetOriginDict()
        def set(ti,sSel,img,sOrigin,dOrigin,bSel):
            triChild=self.trlstOrigin.GetFirstChild(ti)
            while triChild[0].IsOk():
                sPart=self.trlstOrigin.GetItemText(triChild[0],0)
                if sOrigin is None:
                    k=sPart
                else:
                    k=sOrigin+'.'+sPart
                dOrigin[k][1]=bSel
                self.trlstOrigin.SetItemText(triChild[0],sSel,1)
                self.trlstOrigin.SetItemImage(triChild[0],img,0,which=wx.TreeItemIcon_Normal)
                if self.trlstOrigin.ItemHasChildren(triChild[0]):
                    set(triChild[0],sSel,img,k,dOrigin,bSel)
                triChild=self.trlstOrigin.GetNextChild(ti,triChild[1])
        self.trlstOrigin.SetItemText(tn,sSel,1)
        if len(sOrigin)>0:
            self.trlstOrigin.SetItemImage(tn,img,0,which=wx.TreeItemIcon_Normal)
            dOrigin[sOrigin][1]=bSel
        else:
            sOrigin=None
        set(tn,sSel,img,sOrigin,dOrigin,bSel)
        #self.__updateOrigin__()
    def GetOrigins2Find(self):
        lOrigin=[]
        dOrigin=self.GetParent().GetOriginDict()
        for k,t in dOrigin.items():
            if self.trlstOrigin.IsSelected(t[0]):
                lOrigin.append(k)
        if len(lOrigin)>0:
            return lOrigin
        return None
    def GetLevel2Find(self):
        iSel=self.chcFindOrigin.GetSelection()
        if iSel>0:
            sLevel=self.chcFindOrigin.GetStringSelection().upper()
            return sLevel
        return None
    def GetMsg2Find(self):
        sVal=self.txtFindMsg.GetValue()
        if len(sVal)>0:
            if self.GetCaseSensitive2Find():
                return sVal
            else:
                return sVal.lower()
        return None
    def SetMsg2Find(self,s):
        self.txtFindMsg.SetValue(s)
    def GetDirection2Find(self):
        return not self.tgFindDir.GetValue()
    def GetCaseSensitive2Find(self):
        return not self.tgCase.GetValue()

class vtLogFile(vtInOutFileIdxLinesWid):
    TIME_UPDATE_INTERVAL=0.1
    TIME_DELAY_START=100
    TIME_DELAY_DELTA=10
    TIME_DELAY_FIN=10
    TIME_DELAY_DEC_COUNT=5
    MAP_NR_TO_LEVEL_NAME=['DEBUG','INFO','WARNING','WARNING','ERROR','CRITICAL','FATAL']
    def __init__(self, parent, id=-1, pos=wx.DefaultPosition, size=wx.DefaultSize, 
                style=wx.SB_HORIZONTAL, name='vtInOutFileIdxLinesWid',
                bufferSize=20,widPost=None,bReverse=True,
                size_button=wx.Size(31,30)):
        self.dOrigin={}
        wx.Panel.__init__(self,parent,id=id,pos=pos,size=size,style=style,name=name)
        self.SetMinSize(wx.Size(-1, -1))
        self.bxsMain = wx.BoxSizer(orient=wx.VERTICAL)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        bxsPos = wx.BoxSizer(orient=wx.VERTICAL)
        id=wx.NewId()
        self.lblPos = wx.StaticText(self, id,'0',style=wx.ALIGN_RIGHT|wx.ST_NO_AUTORESIZE)
        bxsPos.AddWindow(self.lblPos, 2, border=0, flag=wx.RIGHT|wx.EXPAND)
        self.lblCount = wx.StaticText(self, id,'0',style=wx.ALIGN_RIGHT|wx.ST_NO_AUTORESIZE)
        bxsPos.AddWindow(self.lblCount, 1, border=0, flag=wx.RIGHT|wx.EXPAND)
        bxs.AddSizer(bxsPos,1,border=4,flag=wx.LEFT|wx.BOTTOM|wx.EXPAND)
        
        bxsPos = wx.BoxSizer(orient=wx.VERTICAL)
        id=wx.NewId()
        self.indPos=vtInOutBufIndicator(self,id,bReverse=bReverse,
            space=5)
        bxsPos.AddWindow(self.indPos, 1, border=8, flag=wx.LEFT|wx.RIGHT|wx.EXPAND)
        id=wx.NewId()
        self.slPos = wx.Slider(self, id, 0,0, 1000, style=wx.SL_HORIZONTAL, 
                size=(-1,size_button[1]/3*2))
        #self.Bind(wx.EVT_COMMAND_SCROLL, self.OnScroll, self.slPos)
        self.Bind(wx.EVT_COMMAND_SCROLL_THUMBTRACK, self.OnScrollTrack, self.slPos)
        self.Bind(wx.EVT_COMMAND_SCROLL_THUMBRELEASE, self.OnScrollTrackRelease, self.slPos)
        self.Bind(wx.EVT_COMMAND_SCROLL_PAGEUP, self.OnScrollPgUp, self.slPos)
        self.Bind(wx.EVT_COMMAND_SCROLL_PAGEDOWN, self.OnScrollPgDn, self.slPos)
        bxsPos.AddWindow(self.slPos, 2, border=0, flag=wx.EXPAND)
        bxs.AddSizer(bxsPos,6,border=4,flag=wx.LEFT|wx.RIGHT|wx.EXPAND)
        
        id=wx.NewId()
        self.tgLeft = wx.lib.buttons.GenBitmapToggleButton(ID=id,
              bitmap=vtArt.getBitmap(vtArt.Left), name=u'tgLeft',
              parent=self, pos=wx.DefaultPosition, size=size_button, style=0)
        bxs.AddWindow(self.tgLeft, 0, border=4, flag=wx.LEFT|wx.ALIGN_CENTER)
        self.tgLeft.Bind(wx.EVT_BUTTON,self.OnLeftAuto,self.tgLeft)
        
        id=wx.NewId()
        self.cbLeft = wx.lib.buttons.GenBitmapButton(ID=id,
              bitmap=vtArt.getBitmap(vtArt.Left), name=u'cbLeft',
              parent=self, pos=wx.DefaultPosition, size=size_button, style=0)
        bxs.AddWindow(self.cbLeft, 0, border=4, flag=wx.LEFT|wx.RIGHT|wx.ALIGN_CENTER)
        self.cbLeft.Bind(wx.EVT_BUTTON,self.OnLeft,self.cbLeft)
        #self.cbLeft.Bind(wx.EVT_LEFT_DOWN, self.OnScrollStartLeft)
        #self.cbLeft.Bind(wx.EVT_LEFT_UP, self.OnScrollStop)
        
        id=wx.NewId()
        self.cbRight = wx.lib.buttons.GenBitmapButton(ID=id,
              bitmap=vtArt.getBitmap(vtArt.Right), name=u'cbRight',
              parent=self, pos=wx.DefaultPosition, size=size_button, style=0)
        bxs.AddWindow(self.cbRight, 0, border=4, flag=wx.LEFT|wx.RIGHT|wx.ALIGN_CENTER)
        self.cbRight.Bind(wx.EVT_BUTTON,self.OnRight,self.cbRight)
        #self.cbRight.Bind(wx.EVT_LEFT_DOWN, self.OnScrollStartRight)
        #self.cbRight.Bind(wx.EVT_LEFT_UP, self.OnScrollStop)
        
        id=wx.NewId()
        self.tgRight = wx.lib.buttons.GenBitmapToggleButton(ID=id,
              bitmap=vtArt.getBitmap(vtArt.Right), name=u'tgRight',
              parent=self, pos=wx.DefaultPosition, size=size_button, style=0)
        bxs.AddWindow(self.tgRight, 0, border=4, flag=wx.RIGHT|wx.ALIGN_CENTER)
        self.tgRight.Bind(wx.EVT_BUTTON,self.OnRightAuto,self.tgRight)
        
        id=wx.NewId()
        self.cbFind = wx.lib.buttons.GenBitmapButton(ID=id,
              bitmap=vtArt.getBitmap(vtArt.Find), name=u'cbFind',
              parent=self, pos=wx.DefaultPosition, size=size_button, style=0)
        bxs.AddWindow(self.cbFind, 0, border=4, flag=wx.ALIGN_CENTER|wx.LEFT)
        self.cbFind.Bind(wx.EVT_BUTTON,self.OnFind,self.cbFind)
        #self.cbFind.Bind(wx.EVT_LEFT_DOWN, self.OnFindStart)
        #self.cbFind.Bind(wx.EVT_LEFT_UP, self.OnFindStop)
        self.cbFind.Enable(False)
        
        id=wx.NewId()
        self.thrProc = vidarc.tool.art.vtThrobber.vtThrobberDelay(id=id,
              name=u'thrProc', parent=self, pos=wx.Point(0, 164),
              size=wx.Size(15, 15), style=0)
        bxs.AddWindow(self.thrProc, 0, border=4, flag=wx.LEFT|wx.ALIGN_CENTER)
        
        id=wx.NewId()
        self.cbStop = wx.lib.buttons.GenBitmapButton(id=id,
              bitmap=vtArt.getBitmap(vtArt.Stop), name=u'cbStop',
              parent=self, pos=wx.Point(81, 199), size=wx.Size(76, 30),
              style=0)
        bxs.AddWindow(self.cbStop, 0, border=4, flag=wx.LEFT|wx.ALIGN_CENTER)
        self.cbStop.SetMinSize(wx.Size(-1, -1))
        self.cbStop.Bind(wx.EVT_BUTTON, self.OnCbStopButton)

        id=wx.NewId()
        self.cbPopup = wx.lib.buttons.GenBitmapToggleButton(ID=id,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbPopup',
              parent=self, pos=wx.DefaultPosition, size=size_button, style=wx.BU_AUTODRAW)
        bxs.AddWindow(self.cbPopup, 0, border=4, flag=wx.LEFT|wx.ALIGN_CENTER)
        self.cbPopup.SetBitmapLabel(vtArt.getBitmap(vtArt.Down))
        self.cbPopup.SetBitmapSelected(vtArt.getBitmap(vtArt.Down))
        self.cbPopup.Bind(wx.EVT_BUTTON,self.OnPopupButton,self.cbPopup)#id=id)
        self.popWin=None
        
        self.bxsMain.AddSizer(bxs,1,border=0,flag=wx.EXPAND)
        self.SetSizer(self.bxsMain)
        vtInOutFileIdxLines.__init__(self,bufferSize=bufferSize,par=widPost,bPost=widPost is not None)
        self.bReverse=bReverse
        #self.Bind(wx.EVT_SCROLL, self.OnScroll)
        self.iActScPos=0
        self.iSel=-1
        
        self.SetPostWid(self,True)
        self.Bind(vidarc.tool.vtThread.vEVT_VT_THREAD_FINISHED,
                        self.OnFilLogVtThreadFinished)
        self.Bind(vidarc.tool.vtThread.vEVT_VT_THREAD_ABORTED,
                        self.OnFilLogVtThreadAborted)
        
        self.bScrollLeft=False
        self.bScrollRight=False
        self.iScroll=-1
        self.zScroll = None#wx.Timer(self)
        self.zUpdate=0
        
        self.lFindStoreRaw=[]
        self.lMark=[]
        #self.Bind(wx.EVT_TIMER, self.OnScroll)
    #def Start(self):
    #    vtInOutFileIdxLinesWid
    def Open(self,fn):
        self.Start()
        iRet=vtInOutFileIdxLinesWid.Open(self,fn)
        if iRet>=0:
            self.lblPos.SetLabel(u'0')
            self.lblCount.SetLabel(u'0')
            del self.dOrigin
            self.dOrigin={}
            self.lMark=[]
            self.dCount=self._getOriginEmptyDict()
        return iRet
    def __runThread__(self):
        vtInOutFileIdxLinesWid.__runThread__(self)
        self.CallBack(self.thrProc.Start)
    def _scroll(self):
        vtInOutFileIdxLinesWid._scroll(self)
        self.lblPos.SetLabel(unicode(self.iActScPos))
    def _scrollFull(self,bSel=False):
        vtInOutFileIdxLinesWid._scrollFull(self,bSel=bSel)
        self.lblPos.SetLabel(unicode(self.iActScPos))
    def Stop(self):
        vtInOutFileIdxLinesWid.Stop(self)
        self.CallBack(self.thrProc.Stop)
    def _is2Update(self):
        zNow=time.clock()
        if zNow>(self.zUpdate+self.TIME_UPDATE_INTERVAL):
            self.zUpdate=zNow
            return True
        return False
    def OnLeftAuto(self,evt):
        evt.Skip()
        iVal=self.tgLeft.GetValue()
        if iVal:
            self.tgRight.SetValue(False)
            if self.zScroll is not None:
                self.zScroll.Stop()
                self.zScroll=None
            self.zScroll=wx.FutureCall(self.TIME_DELAY_START,
                        self.OnScrollTimeOut,self.TIME_DELAY_START,-1)
        else:
            self.zScroll.Stop()
            self.zScroll=None
    def OnRightAuto(self,evt):
        evt.Skip()
        iVal=self.tgRight.GetValue()
        if iVal:
            self.tgLeft.SetValue(False)
            if self.zScroll is not None:
                self.zScroll.Stop()
                self.zScroll=None
            self.zScroll=wx.FutureCall(self.TIME_DELAY_START,
                        self.OnScrollTimeOut,self.TIME_DELAY_START,1)
        else:
            self.zScroll.Stop()
            self.zScroll=None
    def OnScrollStartLeft(self,evt):
        self.zScroll=wx.FutureCall(self.TIME_DELAY_START,
                self.OnScrollTimeOut,self.TIME_DELAY_START,-1)
        evt.Skip()
    def OnScrollStartRight(self,evt):
        self.zScroll=wx.FutureCall(self.TIME_DELAY_START,
                self.OnScrollTimeOut,self.TIME_DELAY_START,1)
        evt.Skip()
    def OnScrollStop(self,evt):
        self.zScroll.Stop()
        evt.Skip()
    def OnScrollTimeOut(self,zDly,iCount):
        if iCount<0:
            self.OnLeft(None)
            iCount-=1
        else:
            self.OnRight(None)
            iCount+=1
        if (iCount%self.TIME_DELAY_DEC_COUNT)==0:
            zDly-=self.TIME_DELAY_DELTA
            if zDly<self.TIME_DELAY_FIN:
                zDly=self.TIME_DELAY_FIN
        self.zScroll.Restart(zDly,zDly,iCount)
    def OnCbStopButton(self,evt):
        evt.Skip()
        self.Stop()
    def OnFindStart(self,evt):
        self.zScroll=wx.FutureCall(self.TIME_DELAY_START,
                self.OnFindTimeOut,self.TIME_DELAY_START)
        evt.Skip()
    def OnFindStop(self,evt):
        self.zScroll.Stop()
        evt.Skip()
    def OnFindTimeOut(self,zDly):
        self.OnFind(None)
        self.zScroll.Restart(zDly,zDly)
    def _getOriginEmptyDict(self):
        return {
                'DEBUG   ':0,
                'INFO    ':0,
                'WARNING ':0,
                'WARN    ':0,
                'ERROR   ':0,
                'CRITICAL':0,
                'FATAL   ':0,
                }
    def _clr(self):
        vtInOutFileIdxLinesWid._clr(self)
        for v in self.dOrigin.itervalues():
            v[0]=None
            v[2]=self._getOriginEmptyDict()
        if self.popWin is not None:
            self.popWin._clr()
        #self.lMark=[]
        self.CallBack(self.lblPos.SetLabel,u'-1')
        self.CallBack(self.lblCount.SetLabel,u'-1')
    def _notify(self,pos,size):
        wx.PostEvent(self,vtLogFileProc(self,pos,size))
    def __addOrigin__(self,sOrigin,sLevel):
        if sOrigin not in self.dOrigin:
            self.dOrigin[sOrigin]=[None,True,self._getOriginEmptyDict()]
        dCount=self.dOrigin[sOrigin][2]
        if sLevel in self.dCount:
            self.dCount[sLevel]=self.dCount[sLevel]+1
        else:
            self.dCount[sLevel]=1
        if sLevel in dCount:
            dCount[sLevel]=dCount[sLevel]+1
        else:
            dCount[sLevel]=1
    def __isOrigin2Add__(self,sOrigin):
        if sOrigin in self.dOrigin:
            return self.dOrigin[sOrigin][1]
        return True
    def __isOrigin2Skip__(self,sOrigin):
        return not self.__isOrigin2Add__(sOrigin)
    def _analyse(self,line):
        try:
            vtLdBd.check()
            line=line.decode('UTF-8')
            iStartLv=line.find(u'|',24)
            if iStartLv>512:
                return
            iEndLv=line.find(u'|',iStartLv+1)
            sOrigin=line[24:iStartLv]
            sLevel=line[iStartLv+1:iEndLv]
            if self.__isOrigin2Skip__(sOrigin):
                    del self.lIdxPos[-1]
                    del self.lIdxSz[-1]
            else:
                self.__addOrigin__(sOrigin,sLevel)
        except:
            pass
    def GetLvCount(self):
        return self.dCount
    def GetOriginDict(self):
        return self.dOrigin
    def __createPopup__(self):
        if self.popWin is None:
            sz=self.GetSize()
            sz=(sz[0],sz[1]-20)
            self.popWin=vtLogFileTransientPopup(self,sz,wx.SIMPLE_BORDER)
            self.popWin._updateOrigin()
            self.cbFind.Enable(True)
            #self.popWin.SetNode()
            #self.popWin.SetVal(self.txtVal.GetValue(),self.doc.GetLang())
            self.popWin.SetFindStoreLst(self.lFindStoreRaw)
        else:
            pass
    def OnFilLogVtThreadFinished(self, event):
        event.Skip()
        self.thrProc.Stop()
        if self.popWin is not None:
            self.popWin._updateOrigin()
        self._notify(0,0)
        self.lblPos.SetLabel(unicode(self.iActScPos))
        self.lblCount.SetLabel(unicode(self.GetCount()))
    def OnFilLogVtThreadAborted(self, event):
        event.Skip()
        self.thrProc.Stop()
        self._notify(0,0)
        self.lblPos.SetLabel(unicode(self.iActScPos))
        self.lblCount.SetLabel(unicode(self.GetCount()))
    def SetMsg2Find(self,s):
        if self.popWin is not None:
            self.popWin.SetMsg2Find(s)
    def SetSel(self,j):
        self.iSel=j
        if self.popWin is not None:
            self.popWin.SetMark(self.IsMark(j))
    def GetSel(self):
        return self.iSel
    def SetMark(self,i,bFlag):
        try:
            if bFlag:
                if i not in self.lMark:
                    self.lMark.append(i)
                    self.lMark.sort()
            else:
                if i in self.lMark:
                    self.lMark.remove(i)
            wx.PostEvent(self,vtLogFileMark(self,i,bFlag))
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'lMark:%s'%(vtLog.pformat(self.lMark)),self)
        except:
            vtLog.vtLngTB(self.GetName())
    def IsMark(self,i):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'%d;lMark:%s'%(i,
        #                vtLog.pformat(self.lMark)),self)
        return i in self.lMark
    def MoveMarkPrev(self,i):
        self.checkRunning()
        iFound=-1
        for iMark in self.lMark:
            if iMark>=i:
                if iFound>=0:
                    self.__findSel__(iFound)
                    return
                break
            else:
                iFound=iMark
        if len(self.lMark)>0:
            self.__findSel__(self.lMark[-1])
    def MoveMarkNext(self,i):
        self.checkRunning()
        for iMark in self.lMark:
            if iMark>i:
                self.__findSel__(iMark)
                return
        if len(self.lMark)>0:
            self.__findSel__(self.lMark[0])
    def checkRunning(self):
        if self.IsRunning():
            dlg=vtmMsgDialog(self,u'Thread is already busy, try later again?'+u'\n\n'+ \
                            u'YES   ... current search will be continued.'+u'\n'+ \
                            u'NO    ... new search will be started.',
                            'vtLogFile',
                            wx.YES_NO|wx.YES_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.Centre()
            if dlg.ShowModal()==wx.ID_YES:
                
                dlg.Destroy()
                return
            else:
                self.Stop()
                while self.IsRunning():
                    time.sleep(0.05)
                dlg.Destroy()
    def OnFind(self, event):
        if event is not None:
            event.Skip()
        if self.popWin is None:
            return
        if self.zScroll is not None:
            self.zScroll.Stop()
            self.zScroll=None
            self.tgLeft.SetValue(False)
            self.tgRight.SetValue(False)
        self.checkRunning()
        
        lOrigin=self.popWin.GetOrigins2Find()
        sLevel=self.popWin.GetLevel2Find()
        sMsg=self.popWin.GetMsg2Find()
        bDirUp=self.popWin.GetDirection2Find()
        bCase=self.popWin.GetCaseSensitive2Find()
        if self.bReverse==False:
            bDirUp=not bDirUp
        self.zUpdate=0
        
        #print 'OnFind',
        #print self.iSel,
        if self.iSel<0:
            iPos=self.slPos.GetValue()
            self.iSel=iPos
        else:
            iPos=self.iSel
        #print iPos
        
        iCount=self.GetCount()
        tup2Find=[lOrigin,sLevel,sMsg]
        bOrigin,bLevel,bMsg=[v is not None for v in tup2Find]
        self.Start()
        self.Do(self._find,iPos,iCount,bCase,bDirUp,bOrigin,bLevel,bMsg,tup2Find)
    def _find(self,iPos,iCount,bCase,bDirUp,bOrigin,bLevel,bMsg,tup2Find):
        try:
            if VERBOSE_FIND>0:
                print '_find pos:%05d count:%05d bCase:%d bDirUp:%d bOrigin:%d bLevel:%d bMsg:%d'%(iPos,
                                        iCount,bCase,bDirUp,bOrigin,bLevel,bMsg)
                print '     ',tup2Find
            lOrigin,sLevel,sMsg=tup2Find
            for i in xrange(1,iCount):
                if self.Is2Stop():
                    break
                if self._is2Update():
                    self._notify(i,iCount)
                vtLdBd.check()
                if bDirUp:
                    j=iPos-i
                    if j<0:
                        j+=iCount
                else:
                    j=iPos+i
                    if j>=iCount:
                        j-=iCount
                #print j,
                try:
                    s=self.GetData(j).decode('utf-8')
                except:
                    traceback.print_exc()
                    continue
                l=s.split(u'|')
                if len(l)<4:
                    if VERBOSE_FIND:
                        print '    invalid format at',j,l
                        break
                    continue
                bFound=False
                if bOrigin:
                    if l[1] not in lOrigin:
                        continue
                if bLevel:
                    if not l[2].startswith(sLevel):
                        continue
                if bMsg:
                    if bCase:
                        if not fnmatch.fnmatchcase(l[3],sMsg):
                            continue
                    else:
                        if not fnmatch.fnmatch(l[3].lower(),sMsg):
                            continue
                if VERBOSE_FIND>0:
                    #print
                    print '   found at',j
                    print s[:70]
                    #print j,s,l
                self._notify(0,0)
                self.CallBack(self.__findSel__,j)
                break
            #if VERBOSE_FIND>0:
                #print
                #print '   found'
                #print j,s,l
        except:
            traceback.print_exc()
    def __findSel__(self,j):
        if self.bReverse:
            self.slPos.SetValue(j+1)
        else:
            self.slPos.SetValue(j)
        #self.OnScrollTrack(None)
        self._scrollFull()
        wx.PostEvent(self,vtLogFileSel(self,j))
        self.iSel=j
    def SetFindStoreLst(self,l):
        self.lFindStoreRaw=l
        if self.popWin is not None:
            self.popWin.SetFindStoreLst(self.lFindStoreRaw)
    def GetFindStoreLst(self):
        if self.popWin is not None:
            self.lFindStoreRaw=self.popWin.GetFindStoreLst()
        return self.lFindStoreRaw
    def MoveValue(self,iDelta=1):
        if self.bReverse==False:
            iDelta=-iDelta
        iPos=self.slPos.GetValue()
        iPos=iPos+iDelta
        if iPos<0:
            iPos=0
        if iPos>self.GetCount():
            iPos=self.GetCount()
        self.slPos.SetValue(iPos)
        self._scroll()
