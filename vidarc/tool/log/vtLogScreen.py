#----------------------------------------------------------------------------
# Name:         vtLogScreen.py
# Purpose:      generate screen shot
#
# Author:       Walter Obweger
#
# Created:      20070120
# CVS-ID:       $Id: vtLogScreen.py,v 1.1 2007/01/22 16:57:16 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from wx import WindowDC as wxWindowDC
from wx import MemoryDC as wxMemoryDC
from wx import EmptyBitmap as wxEmptyBitmap
from wx import BITMAP_TYPE_PNG as wxBITMAP_TYPE_PNG

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.InOut.genFN as genFN

def genScreenShot(frm,widLogging,appl=None):
    try:
        sz=frm.GetSize()
        cdc = wxWindowDC(frm)
        mdc=wxMemoryDC()
        bmp=wxEmptyBitmap(sz[0],sz[1])
        mdc.SelectObject(bmp)
        mdc.Blit(0,0,sz[0],sz[1],cdc,0,0)
        sLogFN=vtLog.vtLngGetFN()
        if appl is None:
            sPicFN=genFN.getCurDateTimeHostFN(sLogFN[:-4],'png')
            sPicFN=genFN.getUnique(sPicFN)
        else:
            sPicFN=genFN.getCurDateTimeHostAddLstFN(sLogFN[:-4],[appl],'png')
            sPicFN=genFN.getUnique(sPicFN)
        bmp.SaveFile(sPicFN,wxBITMAP_TYPE_PNG)
        #self.lScreeShots.append(sPicFN)
        vtLog.PrintMsg(_(u'screenshot %s')%(sPicFN),widLogging)
        return sPicFN
    except:
        vtLog.vtLngTB(widLogging.GetName())
        return None
    
