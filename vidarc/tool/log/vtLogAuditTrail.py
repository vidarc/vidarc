#----------------------------------------------------------------------------
# Name:         vtLogAuditTrail.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060913
# CVS-ID:       $Id: vtLogAuditTrail.py,v 1.15 2008/03/21 16:01:56 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import binascii
import cPickle
USE_LOGGING=0
if USE_LOGGING:
    import logging
    import logging.handlers
    def _getAcquire(origin):
        return None
else:
    import vidarc.tool.InOut.fnUtil as fnUtil
    from vidarc.tool.time.vtTime import vtDateTime
    import threading,os,stat,codecs,time
    _semAccess=threading.Lock()#threading.Semaphore()
    _dSemAccess={}
    def _getAcquire(origin):
        global _dSemAccess
        global _semAccess
        try:
            _semAccess.acquire()
            if origin in _dSemAccess:
                return _dSemAccess[origin]
            else:
                sem=threading.Lock()#threading.Semaphore()
                _dSemAccess[origin]=sem
                return sem
        finally:
            _semAccess.release()
DEBUG=10
WARN=30
INFO=20
ERROR=40
CRITICAL=50
LV_AUDIT_CMD=60
LV_AUDIT_OLD=61
LV_AUDIT_NEW=62
LV_AUDIT_SEC=63
LV_AUDIT_IDX=64
LV_AUDIT_OPK=65
LV_AUDIT_NPK=66
_LOGNAMES_DICT={
            DEBUG       :'DEBUG   ',
            INFO        :'INFO    ',
            WARN        :'WARN    ',
            ERROR       :'ERROR   ',
            CRITICAL    :'CRITICAL',
            LV_AUDIT_CMD:'AUDIT_CMD',
            LV_AUDIT_OLD:'AUDIT_OLD',
            LV_AUDIT_NEW:'AUDIT_NEW',
            LV_AUDIT_SEC:'AUDIT_SEC',
            LV_AUDIT_IDX:'AUDIT_IDX',
            LV_AUDIT_OPK:'AUDIT_OPK',
            LV_AUDIT_NPK:'AUDIT_NPK',
            'DEBUG'     :DEBUG,
            'INFO'      :INFO,
            'WARN'      :WARN,
            'ERROR'     :ERROR,
            'CRITICAL'  :CRITICAL,
            'AUDIT_CMD' :LV_AUDIT_CMD,
            'AUDIT_OLD' :LV_AUDIT_OLD,
            'AUDIT_NEW' :LV_AUDIT_NEW,
            'AUDIT_SEC' :LV_AUDIT_SEC,
            'AUDIT_IDX' :LV_AUDIT_IDX,
            'AUDIT_OPK' :LV_AUDIT_OPK,
            'AUDIT_NPK' :LV_AUDIT_NPK,
            }
LEVELS=[(LV_AUDIT_CMD,'AUDIT_CMD'),
            (LV_AUDIT_OLD,'AUDIT_OLD'),(LV_AUDIT_NEW,'AUDIT_NEW'),
            (LV_AUDIT_OLD,'AUDIT_OPK'),(LV_AUDIT_NEW,'AUDIT_NPK'),
            (LV_AUDIT_SEC,'AUDIT_SEC'),
            (LV_AUDIT_IDX,'AUDIT_IDX')]
import types,time,shutil,os,os.path
import traceback
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.InOut.vtInOutCleanUp as vtInOutCleanUp

class vtLogAuditTrail:
    def __init__(self,origin,size=10485760,keepDays=31,keep=40):
        self.iSize=size
        self.lngFileAuditTrail=None
        self.sFN=None
        self.origin='_'.join([origin,'Audit'])
        self.enable=True
        self.keepDays=keepDays
        self.keep=keep
        self.iIdx=0
        self.semAccess=threading.Lock()#threading.Semaphore()
        #if USE_LOGGING:
        #    pass
        #else:
        #    self.semAccess=_getAcquire(origin)
        #    self._zNow=vtDateTime(bLocal=True)
    def __del__(self):
        #self.stop()
        del self.semAccess
    def _acquire(self):
        self.semAccess.acquire()
    def _release(self):
        self.semAccess.release()
    def setEnable(self,flag):
        self.enable=flag
        #self.enable=False
    def isEnabled(self):
        return self.enable
    def backup(self,sFN):
        if self.enable==False:
            return
        sNow=time.strftime('%Y%m%d_%H%M%S',time.localtime(time.time()))
        try:
            sDNtmp,sFNtmp=os.path.split(sFN)
            vtInOutCleanUp.vtInOutCleanUpByDate(sDNtmp,'*.xml.????????_??????',
                    self.keep,self.keepDays)
        except:
            #logging.getLogger('audit').log(40,traceback.format_exc())
            vtLog.vtLngTB('audit')
        try:
            shutil.copyfile(sFN,'.'.join([sFN,sNow]))
            self.log(20,''.join(['backup:',sFN,'.',sNow]))
        except:
            #logging.getLogger('audit').log(40,traceback.format_exc())
            vtLog.vtLngTB('audit')
    def start(self,sFN,origin=None):
        if self.enable==False:
            return
        try:
            if self.lngFileAuditTrail is not None:
                self.stop()
            self.lngFileAuditTrail=None
            if self.enable==False:
                return
            if origin is not None:
                self.origin='_'.join([origin,'Audit'])
            if sFN is None:
                self.sFN='.'.join([self.origin.split('_')[0],'vat'])
            else:
                self.sFN='.'.join([sFN,'vat'])
            try:
                sDNtmp,sFNtmp=os.path.split(self.sFN)
                if len(sDNtmp)>0:
                    os.makedirs(sDNtmp)
            except:
                pass
            #self.log(20,''.join(['audit_trail:',self.sFN]))
            vtLog.vtLngCur(vtLog.INFO,'sFN:%s'%(self.sFN),'audit')
            if USE_LOGGING:
                for lvl,name in LEVELS:
                    logging.addLevelName(lvl,name)
                self.lngFileAuditTrail=logging.handlers.RotatingFileHandler(self.sFN,maxBytes=self.iSize)
                self.lngFileAuditTrail.setLevel(10)
                formatter=logging.Formatter('%(asctime)s|%(name)-18s|%(levelname)-9s|%(message)s')
                self.lngFileAuditTrail.setFormatter(formatter)
                logging.getLogger(self.origin).addHandler(self.lngFileAuditTrail)
            else:
                try:
                    st=os.stat(self.sFN)
                    iCount=st[stat.ST_SIZE]
                except:
                    iCount=0
                if iCount>self.iSize:
                    fnUtil.shiftFile(self.sFN,max=self.keep)
                self.lngFileAuditTrail=codecs.open(self.sFN, 'a', 'UTF-8')
        except:
            #logging.getLogger('audit').log(40,traceback.format_exc())
            vtLog.vtLngTB('audit')
    def stop(self):
        if self.enable==False:
            return
        if self.sFN is None:
            vtLog.vtLngCur(vtLog.INFO,'skipped sFN is None','audit')
            return
        try:
            vtLog.vtLngCur(vtLog.INFO,'sFN:%s'%(self.sFN),'audit')
        except:
            vtLog.vtLngTB('audit')
        try:
            self.log(20,' '.join(['stop',repr(self.origin)]))
            if self.lngFileAuditTrail is not None:
                if USE_LOGGING:
                    logger=logging.getLogger(self.origin)
                    self.lngFileAuditTrail.close()
                    logger.removeHandler(self.lngFileAuditTrail)
                    #self.lngFileAuditTrail.close()
                else:
                    self.lngFileAuditTrail.close()
            #self.sFN=None
            self.lngFileAuditTrail=None
            self.sFN=None
        except:
            #logging.getLogger('audit').log(40,traceback.format_exc())
            vtLog.vtLngTB('audit')
    def checkRename(self,fn):
        if self.enable==False:
            return
        if fn is None:
            return
        try:
            sFN='.'.join([fn,'vat'])
            try:
                vtLog.vtLngCur(vtLog.INFO,'new:%s;old:%s'%(sFN,self.sFN),'audit')
            except:
                vtLog.vtLngTB('audit')
            if sFN!=self.sFN:
                self.stop()
                self.start(fn)
        except:
            vtLog.vtLngTB('audit')
    def __conv__(self,msg):
        t=type(msg)
        if t==types.UnicodeType:
            return repr(msg.encode('ascii','ignore'))#'ISO-8859-1')
        elif t==types.StringType:
            return msg
        else:
            return binascii.hexlify(cPickle.dumps(msg))
    def flush(self,bAcquire=True):
        try:
            if USE_LOGGING:
                pass
            else:
                try:
                    if bAcquire:
                        self._acquire()
                    if self.lngFileAuditTrail:
                        self.lngFileAuditTrail.flush()
                finally:
                    if bAcquire:
                        self._release()
        except:
            vtLog.vtLngTB('audit')
    def log(self,lv,msg,bAcquire=True,origin=None):
        try:
            if self.enable==False:
                return
            if origin is None:
                origin=self.origin
            if USE_LOGGING:
                logger=logging.getLogger(origin)
                logger.log(lv,self.__conv__(msg))
            else:
                try:
                    if bAcquire:
                        self._acquire()
                    if lv in _LOGNAMES_DICT:
                        sLngLv='%-8s'%_LOGNAMES_DICT[lv]
                    else:
                        sLngLv='  ????  '
                    if self.lngFileAuditTrail:
                        zNow=time.time()
                        iMilliSec=(zNow - long(zNow)) * 1000
                        #self._zNow.Now()
                        self.lngFileAuditTrail.write('|'.join([
                                    ','.join([time.strftime(u"%Y-%m-%d %H:%M:%S", time.localtime(zNow)),
                                        u"%03d" % (iMilliSec)]),
                                    '%-18s'%origin,sLngLv,'']))
                        self.lngFileAuditTrail.write(msg)
                        self.lngFileAuditTrail.write('\n')
                        self.lngFileAuditTrail.flush()
                finally:
                    if bAcquire:
                        self._release()
        except:
            #logging.getLogger('audit').log(50,traceback.format_exc())
            vtLog.vtLngTB('audit')
    def sec(self,sLogin,msg='',res=''):
        try:
            if self.enable==False:
                return
            self.log(LV_AUDIT_SEC,';'.join([self.__conv__(sLogin),self.__conv__(msg),self.__conv__(res)]))
        except:
            #logging.getLogger('audit').log(50,traceback.format_exc())
            vtLog.vtLngTB('audit')
    def write(self,action,data_new,data_old=None,iIdx=None,origin=None):
        try:
            idx=-1
            if self.enable==False:
                return idx
            if origin is None:
                vtLog.vtLngCur(vtLog.ERROR,''%(),'audit')
            self._acquire()
            self.log(LV_AUDIT_CMD,action,bAcquire=False,origin=origin)
            if data_old is not None:
                self.log(LV_AUDIT_OLD,binascii.hexlify(data_old),bAcquire=False,origin=origin)
            else:
                if iIdx is not None:
                    self.log(LV_AUDIT_IDX,'%d'%iIdx,bAcquire=False,origin=origin)
            if data_new is not None:
                self.log(LV_AUDIT_NEW,binascii.hexlify(data_new),bAcquire=False,origin=origin)
            else:
                idx=self.iIdx
                self.iIdx+=1
                self.log(LV_AUDIT_IDX,'%d'%idx,bAcquire=False,origin=origin)
            self.flush(bAcquire=False)
        except:
            #logging.getLogger('audit').log(50,traceback.format_exc())
            vtLog.vtLngTB('audit')
        self._release()
        return idx
    def writeObj(self,action,data_new,data_old=None,iIdx=None,origin=None):
        try:
            idx=-1
            if self.enable==False:
                return idx
            self._acquire()
            self.log(LV_AUDIT_CMD,action,bAcquire=False,origin=origin)
            if data_old is not None:
                self.log(LV_AUDIT_OPK,binascii.hexlify(cPickle.dumps(data_old)),bAcquire=False,origin=origin)
            else:
                if iIdx is not None:
                    self.log(LV_AUDIT_IDX,'%d'%iIdx,bAcquire=False,origin=origin)
            if data_new is not None:
                self.log(LV_AUDIT_NPK,binascii.hexlify(cPickle.dumps(data_new)),bAcquire=False,origin=origin)
            else:
                idx=self.iIdx
                self.iIdx+=1
                self.log(LV_AUDIT_IDX,'%d'%idx,bAcquire=False,origin=origin)
            self.flush(bAcquire=False)
        except:
            #logging.getLogger('audit').log(50,traceback.format_exc())
            vtLog.vtLngTB('audit')
        self._release()
        return idx
    def GetFN(self):
        if self.isEnabled():
            return self.sFN
        else:
            return None
