#Boa:Frame:vtLogAuditTrailFileViewerFrame
#----------------------------------------------------------------------------
# Name:         vtLogAuditTrailFileViewerFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080127
# CVS-ID:       $Id: vtLogAuditTrailFileViewerFrame.py,v 1.10 2010/03/03 02:17:10 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.gizmos
import vidarc.tool.vtThread
import vidarc.tool.InOut.vtInOutFileIdxLinesWid
from vidarc.tool.InOut.cfgFile import vtIOCfgConfig
import vidarc.tool.log.vtLogAuditTrailFile
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons

import string,os,os.path,time,sys,stat
import thread,threading,traceback
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.time.vtTimeLoadBound as vtLdBd
from vidarc.tool.misc.vtmMsgDialog import vtmMsgDialog
import vidarc.vApps.common.vAboutDialog as vAboutDialog
import vidarc.vApps.common.vSystem as vSystem

import vidarc.tool.log.images as images

wxEVT_LOGAUDITTRAIL_VIEWER_PROC=wx.NewEventType()
vEVT_LOGAUDITTRAIL_VIEWER_PROC=wx.PyEventBinder(wxEVT_LOGAUDITTRAIL_VIEWER_PROC,1)
def EVT_LOGAUDITTRAIL_VIEWER_PROC(win,func):
    win.Connect(-1,-1,wxEVT_LOGAUDITTRAIL_VIEWER_PROC,func)
class vtLogAuditTrailViewerProc(wx.PyEvent):
    """
    Posted Events:
        Tree Item added event, reports the parent node of the added one
            EVT_LOGAUDITTRAIL_VIEWER_PROC(<widget_name>, self.OnItemSel)
    """

    def __init__(self,obj,pos,size):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_LOGAUDITTRAIL_VIEWER_PROC)
        self.obj=obj
        self.pos=pos
        self.size=size
    def GetPos(self):
        return self.pos
    def GetSize(self):
        return self.size

wxEVT_LOGAUDITTRAIL_VIEWER_READ_FIN=wx.NewEventType()
vEVT_LOGAUDITTRAIL_VIEWER_READ_FIN=wx.PyEventBinder(wxEVT_LOGAUDITTRAIL_VIEWER_READ_FIN,1)
def EVT_LOGAUDITTRAIL_VIEWER_READ_FIN(win,func):
    win.Connect(-1,-1,wxEVT_LOGAUDITTRAIL_VIEWER_READ_FIN,func)
class vtLogAuditTrailViewerReadFin(wx.PyEvent):
    """
    Posted Events:
        Tree Item added event, reports the parent node of the added one
            EVT_LOGAUDITTRAIL_VIEWER_READ_FIN(<widget_name>, self.OnReadFin)
    """

    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_LOGAUDITTRAIL_VIEWER_READ_FIN)
        self.obj=obj

wxEVT_LOGAUDITTRAIL_VIEWER_ADD=wx.NewEventType()
vEVT_LOGAUDITTRAIL_VIEWER_ADD=wx.PyEventBinder(wxEVT_LOGAUDITTRAIL_VIEWER_ADD,1)
def EVT_LOGAUDITTRAIL_VIEWER_ADD(win,func):
    win.Connect(-1,-1,wxEVT_LOGAUDITTRAIL_VIEWER_ADD,func)
class vtLogAuditTrailViewerAdd(wx.PyEvent):
    """
    Posted Events:
        Tree Item added event, reports the parent node of the added one
            EVT_LOGAUDITTRAIL_VIEWER_ADD(<widget_name>, self.OnAdd)
    """

    def __init__(self,obj,iLogNr,log):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_LOGAUDITTRAIL_VIEWER_ADD)
        self.obj=obj
        self.iLogNr=iLogNr
        self.log=log
    def GetLogNr(self):
        return self.iLogNr
    def GetLog(self):
        return self.log

MAP_NR_TO_LEVEL_NAME=['DEBUG','INFO','WARNING','WARNING','ERROR','CRITICAL','FATAL']
def create(parent):
    return vtLogAuditTrailFileViewerFrame(parent)

def getApplAuditTrailBitmap():
    return images.getApplAuditTrailBitmap()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplAuditTrailBitmap())
    return icon

[wxID_VTLOGAUDITTRAILFILEVIEWERFRAME, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMECBAPPLYXMLX, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMECBAPPLYXMLY, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMECBNAVXML, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMECBSHOWXMLOBJX, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMECBSHOWXMLOBJY, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMECBSHOWXMLSTRX, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMECBSHOWXMLSTRY, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEFILLOG, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMELBLACTIONXML, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMELBLCMPMSG, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMELBLCMPXMLDUMMY, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMELBLDIFFXML, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMELBLSHOWOBJXML, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMELBLSHOWSTRINGXML, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMELSTCMPSTR, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMELSTLOGS, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMENBCMP, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEPNBOT, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEPNCMPOBJ, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEPNCMPSTR, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEPNCMPXML, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEPNMAIN, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEPNMSG, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMESBSTATUS, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMESLWBOT, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMETRLSTCMPXML, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMETXTLV, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMETXTMSG, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMETXTORIGIN, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMETXTTMNEW, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMETXTTMOLD, 
] = [wx.NewId() for _init_ctrls in range(32)]

[wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEMNFILEITEM_CLOSE, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEMNFILEITEM_EXIT, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEMNFILEITEM_OPEN, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEMNFILEMNFILE_CFG_OPEN, 
 wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEMNFILEMNFILE_SAVE_CFG, 
] = [wx.NewId() for _init_coll_mnFile_Items in range(5)]


STATUS_PROCESS_POS=0
STATUS_TEXT_POS=0
STATUS_CLOCK_POS=2

[wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEMNHELPMN_HELP_ABOUT] = [wx.NewId() for _init_coll_mnHelp_Items in range(1)]

class vtLogAuditTrailFileViewerFrame(wx.Frame):
    def _init_coll_fgsMsg_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.txtMsg, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.BOTTOM | wx.EXPAND)

    def _init_coll_fgsCmpXml_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblDiffXml, 0, border=4,
              flag=wx.LEFT | wx.TOP | wx.EXPAND)
        parent.AddWindow(self.lblShowStringXml, 0, border=4,
              flag=wx.TOP | wx.EXPAND)
        parent.AddWindow(self.lblActionXml, 0, border=4,
              flag=wx.RIGHT | wx.EXPAND | wx.TOP)
        parent.AddWindow(self.trlstCmpXml, 0, border=4,
              flag=wx.BOTTOM | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsShowStrXml, 0, border=0, flag=0)
        parent.AddSizer(self.bxsCmdXml, 0, border=4, flag=wx.RIGHT | wx.EXPAND)

    def _init_coll_fgsCmpStr_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstCmpStr, 0, border=0, flag=wx.EXPAND)

    def _init_coll_bxsShowStrXml_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbShowXmlStrX, 0, border=0, flag=0)
        parent.AddWindow(self.cbShowXmlStrY, 0, border=4, flag=wx.TOP)
        parent.AddWindow(self.lblShowObjXml, 0, border=8,
              flag=wx.TOP | wx.EXPAND)
        parent.AddWindow(self.cbShowXmlObjX, 0, border=4, flag=wx.TOP)
        parent.AddWindow(self.cbShowXmlObjY, 0, border=4, flag=wx.TOP)

    def _init_coll_fgsCmpXml_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)
        parent.AddGrowableCol(0)

    def _init_coll_fgsCmpStr_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_bxsCmdXml_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApplyXmlX, 0, border=4, flag=0)
        parent.AddWindow(self.cbApplyXmlY, 0, border=4, flag=wx.TOP)
        parent.AddWindow(self.lblCmpXmlDummy, 0, border=8, flag=wx.TOP)
        parent.AddWindow(self.cbNavXml, 0, border=4, flag=wx.TOP)

    def _init_coll_fgsMain_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.filLog, 1, border=4,
              flag=wx.EXPAND|wx.LEFT|wx.RIGHT|wx.TOP)
        parent.AddWindow(self.lstLogs, 0, border=4,
              flag=wx.EXPAND|wx.LEFT|wx.RIGHT|wx.BOTTOM)
        parent.AddSizer(self.bxsAuditTrailInfo, 0, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT)

    def _init_coll_bxsBot_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.nbCmp, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)

    def _init_coll_fgsMain_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)
        parent.AddGrowableCol(0)

    def _init_coll_fgsCmpObj_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_bxsAuditTrailInfo_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.txtOrigin, 3, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtLv, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.txtTmOld, 2, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.txtTmNew, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fgsCmpObj_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblCmpMsg, 0, border=0,
              flag=wx.EXPAND | wx.ALIGN_CENTER)

    def _init_coll_fgsMsg_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_mnFile_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='',
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEMNFILEITEM_OPEN,
              kind=wx.ITEM_NORMAL, text=_(u'Open'))
        parent.Append(help='',
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEMNFILEITEM_CLOSE,
              kind=wx.ITEM_NORMAL, text=_(u'Close'))
        parent.AppendSeparator()
        parent.Append(help='',
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEMNFILEMNFILE_CFG_OPEN,
              kind=wx.ITEM_NORMAL, text=u'Open Config')
        parent.Append(help='',
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEMNFILEMNFILE_SAVE_CFG,
              kind=wx.ITEM_NORMAL, text=u'Save Config')
        parent.AppendSeparator()
        parent.Append(help='',
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEMNFILEITEM_EXIT,
              kind=wx.ITEM_NORMAL, text=_(u'Exit'))
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_openMenu,
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEMNFILEITEM_OPEN)
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_exitMenu,
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEMNFILEITEM_EXIT)
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_closeMenu,
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEMNFILEITEM_CLOSE)
        self.Bind(wx.EVT_MENU, self.OnMnFileMnfile_cfg_openMenu,
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEMNFILEMNFILE_CFG_OPEN)
        self.Bind(wx.EVT_MENU, self.OnMnFileMnfile_save_cfgMenu,
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEMNFILEMNFILE_SAVE_CFG)

    def _init_coll_mnBar_Menus(self, parent):
        # generated method, don't edit

        parent.Append(menu=self.mnFile, title=_(u'File'))
        parent.Append(menu=self.mnHelp, title=u'?')

    def _init_coll_mnHelp_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='',
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEMNHELPMN_HELP_ABOUT,
              kind=wx.ITEM_NORMAL, text=u'About')
        self.Bind(wx.EVT_MENU, self.OnMnHelpMn_help_aboutMenu,
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEMNHELPMN_HELP_ABOUT)

    def _init_coll_sbStatus_Fields(self, parent):
        # generated method, don't edit
        parent.SetFieldsCount(3)

        parent.SetStatusText(number=0, text=u'')
        parent.SetStatusText(number=1, text=u'')
        parent.SetStatusText(number=2, text=u'')

        parent.SetStatusWidths([100, -1, 200])

    def _init_coll_lstCmpStr_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT, heading=u'',
              width=20)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT, heading=u'x',
              width=260)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT, heading=u'y',
              width=260)

    def _init_coll_trlstCmpXml_Columns(self, parent):
        # generated method, don't edit

        parent.AddColumn(text=_(u'tag'))
        parent.AddColumn(text=_(u'x (old value)'))
        parent.AddColumn(text=_(u'y (new value)'))

    def _init_coll_nbCmp_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.pnCmpXml, select=True, text=u'xml')
        parent.AddPage(imageId=-1, page=self.pnCmpStr, select=False,
              text=_(u'string'))
        parent.AddPage(imageId=-1, page=self.pnCmpObj, select=False,
              text=_(u'object'))
        parent.AddPage(imageId=-1, page=self.pnMsg, select=False,
              text=_(u'message'))

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsMain = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self.fgsCmpStr = wx.FlexGridSizer(cols=2, hgap=4, rows=1, vgap=4)

        self.bxsAuditTrailInfo = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBot = wx.BoxSizer(orient=wx.VERTICAL)

        self.fgsCmpXml = wx.FlexGridSizer(cols=3, hgap=4, rows=2, vgap=4)

        self.bxsShowStrXml = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsCmdXml = wx.BoxSizer(orient=wx.VERTICAL)

        self.fgsCmpObj = wx.FlexGridSizer(cols=1, hgap=0, rows=1, vgap=0)

        self.fgsMsg = wx.FlexGridSizer(cols=1, hgap=0, rows=1, vgap=0)

        self._init_coll_fgsMain_Items(self.fgsMain)
        self._init_coll_fgsMain_Growables(self.fgsMain)
        self._init_coll_fgsCmpStr_Items(self.fgsCmpStr)
        self._init_coll_fgsCmpStr_Growables(self.fgsCmpStr)
        self._init_coll_bxsAuditTrailInfo_Items(self.bxsAuditTrailInfo)
        self._init_coll_bxsBot_Items(self.bxsBot)
        self._init_coll_fgsCmpXml_Items(self.fgsCmpXml)
        self._init_coll_fgsCmpXml_Growables(self.fgsCmpXml)
        self._init_coll_bxsShowStrXml_Items(self.bxsShowStrXml)
        self._init_coll_bxsCmdXml_Items(self.bxsCmdXml)
        self._init_coll_fgsCmpObj_Items(self.fgsCmpObj)
        self._init_coll_fgsCmpObj_Growables(self.fgsCmpObj)
        self._init_coll_fgsMsg_Items(self.fgsMsg)
        self._init_coll_fgsMsg_Growables(self.fgsMsg)

        self.pnMsg.SetSizer(self.fgsMsg)
        self.pnCmpXml.SetSizer(self.fgsCmpXml)
        self.pnBot.SetSizer(self.bxsBot)
        self.pnMain.SetSizer(self.fgsMain)
        self.pnCmpObj.SetSizer(self.fgsCmpObj)
        self.pnCmpStr.SetSizer(self.fgsCmpStr)

    def _init_utils(self):
        # generated method, don't edit
        self.mnBar = wx.MenuBar()

        self.mnFile = wx.Menu(title=u'')

        self.mnHelp = wx.Menu(title='')

        self._init_coll_mnBar_Menus(self.mnBar)
        self._init_coll_mnFile_Items(self.mnFile)
        self._init_coll_mnHelp_Items(self.mnHelp)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAME,
              name=u'vtLogAuditTrailFileViewerFrame', parent=prnt,
              pos=wx.Point(312, 47), size=wx.Size(528, 589),
              style=wx.DEFAULT_FRAME_STYLE,
              title=_(u'vtLogAuditTrailFileViewerFrame'))
        self._init_utils()
        self.SetClientSize(wx.Size(520, 562))
        self.SetMenuBar(self.mnBar)
        self.Bind(wx.EVT_SIZE, self.OnVtLogFileViewerFrameSize)
        self.Bind(wx.EVT_MOUSEWHEEL, self.OnVtLogFileViewerFrameMouseWheel)

        self.slwBot = wx.SashLayoutWindow(id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMESLWBOT,
              name=u'slwBot', parent=self, pos=wx.Point(0, 272),
              size=wx.Size(520, 248), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwBot.SetDefaultSize(wx.Size(464, 220))
        self.slwBot.SetOrientation(wx.LAYOUT_HORIZONTAL)
        self.slwBot.SetAlignment(wx.LAYOUT_BOTTOM)
        self.slwBot.SetSashVisible(wx.SASH_TOP, True)
        self.slwBot.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwBotSashDragged,
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMESLWBOT)

        self.pnMain = wx.Panel(id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEPNMAIN,
              name=u'pnMain', parent=self, pos=wx.Point(0, 0), size=wx.Size(520,
              264), style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)
        self.pnMain.SetAutoLayout(True)
        self.pnMain.SetMinSize(wx.Size(100, 100))

        self.sbStatus = wx.StatusBar(id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMESBSTATUS,
              name=u'sbStatus', parent=self, style=0)
        self.sbStatus.SetFieldsCount(2)
        self._init_coll_sbStatus_Fields(self.sbStatus)
        self.SetStatusBar(self.sbStatus)

        self.lstLogs = wx.ListView(id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMELSTLOGS,
              name=u'lstLogs', parent=self.pnMain, pos=wx.Point(4, 47),
              size=wx.Size(508, 188), style=wx.LC_REPORT | wx.LC_SINGLE_SEL)
        self.lstLogs.SetConstraints(LayoutAnchors(self.lstLogs, True, True,
              True, True))
        self.lstLogs.SetMinSize(wx.Size(-1, -1))
        self.lstLogs.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstLogsListItemSelected,
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMELSTLOGS)
        self.lstLogs.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstLogsListItemDeselected,
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMELSTLOGS)
        self.lstLogs.Bind(wx.EVT_LEFT_DCLICK, self.OnLstLogsLeftDclick)
        self.lstLogs.Bind(wx.EVT_MOUSEWHEEL, self.OnLstLogsMouseWheel)

        self.filLog = vidarc.tool.log.vtLogAuditTrailFile.vtLogAuditTrailFile(id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEFILLOG,
              name=u'filLog', parent=self.pnMain, pos=wx.Point(4, 4),
              size=wx.Size(508, 43), style=wx.SB_HORIZONTAL)
        self.filLog.Bind(vidarc.tool.InOut.vtInOutFileIdxLinesWid.vEVT_VTINOUT_FILEIDXLINES_ADD,
              self.OnFilLogVtinoutFileidxlinesAdd,
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEFILLOG)

        self.txtOrigin = wx.TextCtrl(id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMETXTORIGIN,
              name=u'txtOrigin', parent=self.pnMain, pos=wx.Point(4, 239),
              size=wx.Size(190, 21), style=0, value=u'')

        self.txtLv = wx.TextCtrl(id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMETXTLV,
              name=u'txtLv', parent=self.pnMain, pos=wx.Point(198, 239),
              size=wx.Size(59, 21), style=0, value=u'')
        self.txtLv.SetToolTipString(_(u'audit trail level'))

        self.txtTmOld = wx.TextCtrl(id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMETXTTMOLD,
              name=u'txtTmOld', parent=self.pnMain, pos=wx.Point(261, 239),
              size=wx.Size(123, 21), style=0, value=u'')
        self.txtTmOld.SetToolTipString(_(u'time old data'))

        self.txtTmNew = wx.TextCtrl(id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMETXTTMNEW,
              name=u'txtTmNew', parent=self.pnMain, pos=wx.Point(388, 239),
              size=wx.Size(123, 21), style=0, value=u'')
        self.txtTmNew.SetToolTipString(_(u'time new data'))

        self.pnBot = wx.Panel(id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEPNBOT,
              name=u'pnBot', parent=self.slwBot, pos=wx.Point(0, 3),
              size=wx.Size(520, 245),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)
        self.pnBot.SetAutoLayout(True)

        self.nbCmp = wx.Notebook(id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMENBCMP,
              name=u'nbCmp', parent=self.pnBot, pos=wx.Point(4, 4),
              size=wx.Size(508, 237), style=0)

        self.pnCmpStr = wx.Panel(id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEPNCMPSTR,
              name=u'pnCmpStr', parent=self.nbCmp, pos=wx.Point(0, 0),
              size=wx.Size(500, 211),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)

        self.lstCmpStr = wx.ListCtrl(id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMELSTCMPSTR,
              name=u'lstCmpStr', parent=self.pnCmpStr, pos=wx.Point(0, 0),
              size=wx.Size(496, 207), style=wx.LC_REPORT | wx.LC_SINGLE_SEL)
        self.lstCmpStr.SetMinSize((-1,-1))
        self._init_coll_lstCmpStr_Columns(self.lstCmpStr)

        self.pnCmpXml = wx.Panel(id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEPNCMPXML,
              name=u'pnCmpXml', parent=self.nbCmp, pos=wx.Point(0, 0),
              size=wx.Size(500, 211), style=wx.TAB_TRAVERSAL)

        self.trlstCmpXml = wx.gizmos.TreeListCtrl(id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMETRLSTCMPXML,
              name=u'trlstCmpXml', parent=self.pnCmpXml, pos=wx.Point(4, 21),
              size=wx.Size(372, 186),
              style=wx.TR_HAS_BUTTONS|wx.TR_TWIST_BUTTONS| wx.TR_FULL_ROW_HIGHLIGHT)
        self.trlstCmpXml.SetMinSize((-1,-1))
        self._init_coll_trlstCmpXml_Columns(self.trlstCmpXml)

        self.cbShowXmlStrX = wx.lib.buttons.GenBitmapTextToggleButton(bitmap=vtArt.getBitmap(vtArt.Search),
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMECBSHOWXMLSTRX, label=u'x',
              name=u'cbShowXmlStrX', parent=self.pnCmpXml, pos=wx.Point(380,
              21), size=wx.Size(56, 30), style=0)
        self.cbShowXmlStrX.Bind(wx.EVT_BUTTON, self.OnCbShowXmlStrXButton,
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMECBSHOWXMLSTRX)

        self.cbShowXmlStrY = wx.lib.buttons.GenBitmapTextToggleButton(bitmap=vtArt.getBitmap(vtArt.Search),
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMECBSHOWXMLSTRY, label=u'y',
              name=u'cbShowXmlStrY', parent=self.pnCmpXml, pos=wx.Point(380,
              55), size=wx.Size(56, 30), style=0)
        self.cbShowXmlStrY.Bind(wx.EVT_BUTTON, self.OnCbShowXmlStrYButton,
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMECBSHOWXMLSTRY)

        self.cbShowXmlObjX = wx.lib.buttons.GenBitmapTextToggleButton(bitmap=vtArt.getBitmap(vtArt.Search),
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMECBSHOWXMLOBJX, label=u'x',
              name=u'cbShowXmlObjX', parent=self.pnCmpXml, pos=wx.Point(380,
              110), size=wx.Size(52, 30), style=0)
        self.cbShowXmlObjX.Bind(wx.EVT_BUTTON, self.OnCbShowXmlObjXButton,
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMECBSHOWXMLOBJX)

        self.cbShowXmlObjY = wx.lib.buttons.GenBitmapTextToggleButton(bitmap=vtArt.getBitmap(vtArt.Search),
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMECBSHOWXMLOBJY, label=u'y',
              name=u'cbShowXmlObjY', parent=self.pnCmpXml, pos=wx.Point(380,
              144), size=wx.Size(52, 30), style=0)
        self.cbShowXmlObjY.Bind(wx.EVT_BUTTON, self.OnCbShowXmlObjYButton,
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMECBSHOWXMLOBJY)

        self.lblShowStringXml = wx.StaticText(id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMELBLSHOWSTRINGXML,
              label=u'string', name=u'lblShowStringXml', parent=self.pnCmpXml,
              pos=wx.Point(380, 4), size=wx.Size(56, 13),
              style=wx.ALIGN_CENTRE)

        self.lblShowObjXml = wx.StaticText(id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMELBLSHOWOBJXML,
              label=u'object', name=u'lblShowObjXml', parent=self.pnCmpXml,
              pos=wx.Point(380, 93), size=wx.Size(56, 13),
              style=wx.ALIGN_CENTRE)

        self.lblDiffXml = wx.StaticText(id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMELBLDIFFXML,
              label=_(u'difference'), name=u'lblDiffXml', parent=self.pnCmpXml,
              pos=wx.Point(4, 4), size=wx.Size(372, 13), style=0)
        self.lblDiffXml.SetMinSize((-1,-1))

        self.lblActionXml = wx.StaticText(id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMELBLACTIONXML,
              label=_(u'action'), name=u'lblActionXml', parent=self.pnCmpXml,
              pos=wx.Point(440, 4), size=wx.Size(56, 13),
              style=wx.ALIGN_CENTRE)

        self.cbApplyXmlX = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Apply),
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMECBAPPLYXMLX, label=u'x',
              name=u'cbApplyXmlX', parent=self.pnCmpXml, pos=wx.Point(440, 21),
              size=wx.Size(56, 30), style=0)
        self.cbApplyXmlX.Bind(wx.EVT_BUTTON, self.OnCbApplyXmlXButton,
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMECBAPPLYXMLX)

        self.cbApplyXmlY = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Apply),
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMECBAPPLYXMLY, label=u'y',
              name=u'cbApplyXmlY', parent=self.pnCmpXml, pos=wx.Point(440, 55),
              size=wx.Size(56, 30), style=0)
        self.cbApplyXmlY.Bind(wx.EVT_BUTTON, self.OnCbApplyXmlYButton,
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMECBAPPLYXMLY)

        self.cbNavXml = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Navigate),
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMECBNAVXML, name=u'cbNavXml',
              parent=self.pnCmpXml, pos=wx.Point(440, 110), size=wx.Size(56,
              30), style=0)
        self.cbNavXml.Bind(wx.EVT_BUTTON, self.OnCbNavXmlButton,
              id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMECBNAVXML)

        self.pnCmpObj = wx.Panel(id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEPNCMPOBJ,
              name=u'pnCmpObj', parent=self.nbCmp, pos=wx.Point(0, 0),
              size=wx.Size(500, 211), style=wx.TAB_TRAVERSAL)

        self.pnMsg = wx.Panel(id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEPNMSG,
              name=u'pnMsg', parent=self.nbCmp, pos=wx.Point(0, 0),
              size=wx.Size(500, 211), style=wx.TAB_TRAVERSAL)

        self.lblCmpMsg = wx.StaticText(id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMELBLCMPMSG,
              label=_(u'Sorry, feature not released yet.'), name=u'lblCmpMsg',
              parent=self.pnCmpObj, pos=wx.Point(0, 0), size=wx.Size(500, 211),
              style=0)

        self.txtMsg = wx.TextCtrl(id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMETXTMSG,
              name=u'txtMsg', parent=self.pnMsg, pos=wx.Point(4, 4),
              size=wx.Size(492, 203), style=wx.TE_MULTILINE, value=u'')

        self.lblCmpXmlDummy = wx.StaticText(id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMELBLCMPXMLDUMMY,
              label=u'', name=u'lblCmpXmlDummy', parent=self.pnCmpXml,
              pos=wx.Point(440, 93), size=wx.Size(0, 13), style=0)

        self._init_coll_nbCmp_Pages(self.nbCmp)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vtLog')
        self._init_ctrls(parent)
        
        lMnIt=self.mnFile.GetMenuItems()
        lMnIt[0].SetBitmap(vtArt.getBitmap(vtArt.Open))
        lMnIt[1].SetBitmap(vtArt.getBitmap(vtArt.Close))
        lMnIt[3].SetBitmap(vtArt.getBitmap(vtArt.Open))
        lMnIt[4].SetBitmap(vtArt.getBitmap(vtArt.Save))
        lMnIt[6].SetBitmap(vtArt.getBitmap(vtArt.PowerDown))
        lMnIt=self.mnHelp.GetMenuItems()
        lMnIt[0].SetBitmap(vtArt.getBitmap(vtArt.About))
        
        self.bModified=False
        self.idxSel=-1
        self.fn=None
        self.appls=[]
        self.doc=None
        self.trMain=None
        self.dlgShowXmlObjX={'':None}
        self.dlgShowXmlObjY={'':None}
        self.cfg=vtIOCfgConfig()
        self.Clear()
        self.__markCloseMnIt(False)
        
        #self.filLog.SetPostWid(self,True)
        #self.Bind(vidarc.tool.vtThread.vEVT_VT_THREAD_FINISHED,
        #                self.OnFilLogVtThreadFinished)
        #self.Bind(vidarc.tool.vtThread.vEVT_VT_THREAD_ABORTED,
        #                self.OnFilLogVtThreadAborted)
        self.filLog.Bind(vidarc.tool.log.vtLogAuditTrailFile.vEVT_VTLOGAUDITTRAIL_FILE_PROC,
                        self.OnLogViewerProc)
        self.filLog.Bind(vidarc.tool.log.vtLogAuditTrailFile.vEVT_VTLOGAUDITTRAIL_FILE_SEL,
                        self.OnFilLogSel)
        # setup statusbar
        self.timer = wx.PyTimer(self.Notify)
        self.timer.Start(1000)
        self.Notify()
        
        icon = getApplicationIcon()
        self.SetIcon(icon)
        
        rect = self.sbStatus.GetFieldRect(STATUS_PROCESS_POS)
        self.gProcess = wx.Gauge(
                    self.sbStatus, -1, 50, (rect.x+2, rect.y+2), 
                    (rect.width-4, rect.height-4), 
                    wx.GA_HORIZONTAL|wx.GA_SMOOTH
                    )
        self.gProcess.SetRange(1000)
        self.gProcess.SetValue(0)
        self.lstLogs.InsertColumn(0,u'',wx.LIST_FORMAT_LEFT,20)
        self.lstLogs.InsertColumn(1,_(u'id'),wx.LIST_FORMAT_RIGHT,80)
        self.lstLogs.InsertColumn(2,_(u'tag name'),wx.LIST_FORMAT_LEFT,120)
        self.lstLogs.InsertColumn(3,_(u'action / message'),wx.LIST_FORMAT_LEFT,120)
        self.lstLogs.InsertColumn(4,_(u'time'),wx.LIST_FORMAT_LEFT,140)
        self.lstLogs.InsertColumn(5,_(u'level'),wx.LIST_FORMAT_LEFT,0)
        self.lstLogs.InsertColumn(6,_(u'origin'),wx.LIST_FORMAT_LEFT,0)
        self.imgLstLog=wx.ImageList(16,16)
        self.imgDictLog={}
        img=wx.ArtProvider.GetBitmap(wx.ART_TICK_MARK, size=(16, 16))
        self.imgDictLog['def']=self.imgLstLog.Add(img)
        img=wx.ArtProvider.GetBitmap(wx.ART_WARNING, size=(16, 16))
        self.imgDictLog['warn']=self.imgLstLog.Add(img)
        img=wx.ArtProvider.GetBitmap(wx.ART_ERROR, size=(16, 16))
        self.imgDictLog['error']=self.imgLstLog.Add(img)
        img=vtArt.getBitmap(vtArt.Question)
        self.imgDictLog['quest']=self.imgLstLog.Add(img)
        img=vtArt.getBitmap(vtArt.Invisible)
        self.imgDictLog['empty']=self.imgLstLog.Add(img)
        img=vtArt.getBitmap(vtArt.Add)
        self.imgDictLog['addNode']=self.imgLstLog.Add(img)
        img=vtArt.getBitmap(vtArt.Del)
        self.imgDictLog['delNode']=self.imgLstLog.Add(img)
        img=vtArt.getBitmap(vtArt.Edit)
        self.imgDictLog['startEdit']=self.imgLstLog.Add(img)
        img=vtArt.getBitmap(vtArt.Edit)
        self.imgDictLog['doEdit']=self.imgLstLog.Add(img)
        img=vtArt.getBitmap(vtArt.Edit)
        self.imgDictLog['endEdit']=self.imgLstLog.Add(img)
        self.lstLogs.SetImageList(self.imgLstLog,wx.IMAGE_LIST_SMALL)
        
        self.imgLstXml=wx.ImageList(16,16)
        self.imgDictXml={}
        img=vtArt.getBitmap(vtArt.Invisible)
        self.imgDictXml['empty']=self.imgLstXml.Add(img)
        img=vtArt.getBitmap(vtArt.Element)
        self.imgDictXml['element']=self.imgLstXml.Add(img)
        img=vtArt.getBitmap(vtArt.Attr)
        self.imgDictXml['attr']=self.imgLstXml.Add(img)
        img=vtArt.getBitmap(vtArt.Attrs)
        self.imgDictXml['attrs']=self.imgLstXml.Add(img)
        self.trlstCmpXml.SetImageList(self.imgLstXml)
        self.trlstCmpXml.SetColumnWidth(0,200)
        self.trlstCmpXml.SetColumnWidth(1,70)
        self.trlstCmpXml.SetColumnWidth(2,70)
        
        self.imgLstTyp=wx.ImageList(16,16)
        self.imgDict={}
        img=wx.ArtProvider.GetBitmap(wx.ART_TICK_MARK, size=(16, 16))
        self.imgDict['def']=self.imgLstTyp.Add(img)
        img=wx.ArtProvider.GetBitmap(wx.ART_WARNING, size=(16, 16))
        self.imgDict['warn']=self.imgLstTyp.Add(img)
        img=wx.ArtProvider.GetBitmap(wx.ART_ERROR, size=(16, 16))
        self.imgDict['error']=self.imgLstTyp.Add(img)
        img=vtArt.getBitmap(vtArt.Invisible)
        self.imgDict['empty']=self.imgLstTyp.Add(img)
        self.lstCmpStr.SetImageList(self.imgLstTyp,wx.IMAGE_LIST_SMALL)
        
        self.filLog.SetBufSize(20)
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnMain)
        vtLdBd.vtTimeLoadBoundInit()
        self.Clear()
        wx.CallAfter(self.Bind,wx.EVT_CLOSE, self.OnMainClose)
        
        self.dlgShowXmlX=None
        self.dlgShowXmlY=None
        self.tConvRes4Disp=None         # convert diff result dict for tree display
        
        self.NB_CMP_XML=0
        self.NB_CMP_STR=1
        self.NB_CMP_OBJ=2
        self.NB_CMP_MSG=3
        #self.fgsMain.Layout()
        #self.fgsMain.Fit(self)
        iX,iY,iW,iH=vSystem.LimitWindowToScreen(0,0,520,720)
        self.Move((iX,iX))
        self.SetSize((iW,iH))
        #wx.CallAfter(self.Move,(iX,iX))
        #wx.CallAfter(self.SetSize,(iW,iH))
        #wx.LayoutAlgorithm().LayoutWindow(self, self.pnMain)
        #self.pnMain.Refresh()
        #self.fgsTop.Fit(self)
    def SetDocClass(self,clsDoc,*args,**kwargs):
        self.filLog.SetDocClass(clsDoc,*args,**kwargs)
    def SetDiffClass(self,clsDiff,*args,**kwargs):
        self.filLog.SetDiffClass(clsDiff,*args,**kwargs)
    def SetConvRes4DispFunc(self,func,*args,**kwargs):
        self.tConvRes4Disp=(func,args,kwargs)
    def SetDoc(self,doc):
        self.doc=doc
        try:
            self.OpenFile(self.doc.audit.GetFN())
        except:
            vtLog.vtLngTB(self.GetName())
    def SetTreeMain(self,trMain):
        try:
            self.trMain=trMain
        except:
            vtLog.vtLngTB(self.GetName())
    def Notify(self):
        try:
            t = time.localtime(time.time())
            st = time.strftime(_("%b-%d-%Y   %H:%M:%S wk:%W"), t)
            self.SetStatusText(st, STATUS_CLOCK_POS)
        except:
            pass
    def __makeTitle__(self):
        s="VIDARC Log Audit Trail File Viewer"
        if self.fn is None:
            s=s+" (undef*)"
        else:
            s=s+" ("+self.fn
            if self.bModified:
                s=s+"*)"
            else:
                s=s+")"
        self.SetTitle(s)
    def __setModified__(self,state):
        self.bModified=state
        self.__makeTitle__()
    def Clear(self):
        #self.chcOrigin.Clear()
        #self.lstLogs.DeleteAllItems()
        #self.lstCallStack.DeleteAllItems()
        self.lstLogs.DeleteAllItems()
        self.lstCmpStr.DeleteAllItems()
        self.trlstCmpXml.DeleteAllItems()
        self.__markCloseMnIt(False)
        #self.txtMsg.SetValue('')
        self.iX=-1
        self.iY=-1
        self.idxSel=-1
        self.nodeX=None
        self.nodeY=None
        self.__enableCmpXml__()
    def __markCloseMnIt(self,bFlag):
        try:
            mnBar=self.GetMenuBar()
            mn=mnBar.GetMenu(0)
            mnIt=mn.FindItemById(wxID_VTLOGAUDITTRAILFILEVIEWERFRAMEMNFILEITEM_CLOSE)
            mnIt.Enable(bFlag)
        except:
            traceback.print_exc()
            pass
    def GetFN(self):
        return self.fn
    def OpenFile(self,fn=None):
        try:
            #vtLog.CallStack('')
            if self.filLog.IsRunning():
            #if self.thdOpen.IsRunning():
                dlg=vtmMsgDialog(self,_(u'Open file not possible yet, thread still running.') ,
                            'vtLog',
                            wx.OK|wx.ICON_EXCLAMATION )
                dlg.Destroy()
                return
            if fn is not None:
                sDN,sFN=os.path.split(fn)
                if len(sDN)==0:
                    sDN=os.getcwd()
                fn=os.path.join(sDN,sFN)
                self.fn=fn
                self.__makeTitle__()
            #self.thdOpen.Start()
            self.Clear()
            #self.lstCallStack.DeleteAllItems()
            #self.lstCmp.DeleteAllItems()
            #self.iX=-1
            #self.iY=-1
            self.__markCloseMnIt(True)
            self.filLog.Open(self.fn)
            self.filLog.Index()
            sIniFN=self.fn[:-3]+'ini'
            if os.path.exists(sIniFN):
                self.OpenCfgFile(sIniFN)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnMnFileItem_openMenu(self, event):
        if self.filLog.IsRunning():
            return 
        dlg = wx.FileDialog(self, _(u"Open"), "", "", _(u"log files (*.log)|*.log|all files (*.*)|*.*"), wx.OPEN)
        try:
            fn=self.GetFN()
            if fn is not None:
                dlg.SetPath(fn)
            else:
                sLogDN=os.getenv('vidarcLogDN')
                if sLogDN is not None:
                    dlg.SetPath(sLogDN+os.path.sep)
            #else:
            #    dlg.SetPath('.')
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.OpenFile(filename)
                self.__setModified__(False)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()
    def OnMainClose(self,evt):
        self.doc=None
        self.trMain=None
        try:
            for k,v in self.dlgShowXmlObjX.iteritems():
                if len(k)>0:
                    v.Close()
            for k,v in self.dlgShowXmlObjY.iteritems():
                if len(k)>0:
                    v.Close()
            self.dlgShowXmlObjX={}
            self.dlgShowXmlObjY={}
        except:
            vtLog.vtLngTB(self.GetName())
        try:
            if evt.CanVeto():
                if self.filLog.IsRunning():
                    evt.Veto()
                    self.filLog.Stop()
                    return
        except:
            pass
        evt.Skip()
    def OnMnFileItem_exitMenu(self, event):
        #self.SaveCfgFile()
        self.Close()
        event.Skip()
    def OnLstLogsListItemSelected(self, event):
        try:
            self.idxSel=event.GetIndex()
            idxLog=self.lstLogs.GetItemData(self.idxSel)
            self.filLog.SetSel(idxLog)
            self.__updateCallStack__()
            self.__updateMsg__()
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()
    def OnLstLogsListItemDeselected(self, event):
        try:
            self.idxSel=-1
            self.filLog.SetSel(-1)
            self.__updateCallStack__()
            self.__updateMsg__()
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()
    def OnLstLogsLeftDclick(self, event):
        event.Skip()
        if self.idxSel<0:
            self.filLog.SetMsg2Find('')
        else:
            idxLog=self.lstLogs.GetItemData(self.idxSel)
            log=self.filLog.GetData(idxLog).split('|')
            msg=string.replace(log[3],';',chr(10))
            i=msg.find(chr(10))
            if i>=0:
                self.filLog.SetMsg2Find(msg[:i]+'*')
            else:
                self.filLog.SetMsg2Find(msg+'*')
    def OnLstLogsMouseWheel(self,event):
        if event.GetWheelRotation()<0:
            self.filLog.MoveValue(-event.GetLinesPerAction())
        else:
            self.filLog.MoveValue(event.GetLinesPerAction())
    def OnVtLogFileViewerFrameMouseWheel(self,event):
        if event.GetWheelRotation()<0:
            self.filLog.MoveValue(-event.GetLinesPerAction())
        else:
            self.filLog.MoveValue(event.GetLinesPerAction())
    def __clrMsgWid__(self):
        self.lstCmpStr.DeleteAllItems()
        self.txtOrigin.SetValue(u'')
        self.txtTmOld.SetValue(u'')
        self.txtTmNew.SetValue(u'')
        self.trlstCmpXml.DeleteAllItems()
        self.txtMsg.SetValue(u'')
        self.nodeX=None
        self.nodeY=None
        self.__enableCmpXml__()
        if self.dlgShowXmlX is not None:
            self.dlgShowXmlX.SetNode(None)
        if self.dlgShowXmlY is not None:
            self.dlgShowXmlY.SetNode(None)
    def __updateMsg__(self):
        try:
            self.__clrMsgWid__()
            if self.idxSel<0:
                #self.txtMsg.SetValue(u'')
                return
            self.Freeze()
            idxLog=self.lstLogs.GetItemData(self.idxSel)
            #lAT=self.filLog.GetData(idxLog)
            vtLog.vtLngCurWX(vtLog.DEBUG,'idxSel:%d;idxLog:%d'%(self.idxSel,idxLog),self)
            tup=self.filLog.GetData(idxLog)
            lAT=self.filLog.GetValue(tup)
            if len(lAT)==1:
                strs=lAT[0].split(u'|')
                self.txtTmNew.SetValue(strs[0])
                self.txtOrigin.SetValue(strs[1])
                self.txtLv.SetValue(strs[2])
                self.txtMsg.SetValue(strs[3])
                #self.nbCmp.SetSelection(self.NB_CMP_MSG)
            else:
                self.__updateCmp__(lAT)
        except:
            vtLog.vtLngTB(self.GetName())
        self.Thaw()
    def __updateCmp__(self,lAT):
        try:
            bObj=False
            bXml=False
            lInfoX=u''
            lInfoY=u''
            if len(lAT)>6:
                if lAT[6] is not None:
                    self.txtLv.SetValue(lAT[3])
                    bXml=True
            #print
            #print 'lAT',len(lAT),lAT
            lC=self.filLog.GetAuditValue(lAT,0)
            if lC is None:
                return
            self.txtOrigin.SetValue(lC[0])
            
            #print 'lC',lC
            lX=self.filLog.GetAuditValue(lAT,1)
            if lX is not None:
                self.txtTmOld.SetValue(lX[3])
            #print 'lX',lX
            lY=self.filLog.GetAuditValue(lAT,2)
            if lY is not None:
                self.txtTmNew.SetValue(lY[3])
            #print 'lY',lY
            try:
                imgNum=-1
                if lX is None:
                    lInfoX=u''
                    if lY is None:
                        lInfoY=u''
                        imgNum=self.imgDict['error']
                    else:
                        imgNum=-1
                        if lY[1]==u'AUDIT_NPK':
                            bObj=True
                        else:
                            lInfoY=lY[2]
                else:
                    if lY is None:
                        imgNum=-1
                        if lX[1]==u'AUDIT_NPK':
                            bObj=True
                        else:
                            lInfoY=u''
                            lInfoX=lX[2]
                    else:
                        if lX[0]!=lY[0]:
                            imgNum=self.imgDict['error']
                        if lX[1]==u'AUDIT_OLD':
                            if bXml:
                                lInfoX=lX[2]
                                lInfoY=lY[2]
                            else:
                                if lY[1]!=u'AUDIT_NEW':
                                    imgNum=self.imgDict['error']
                                else:
                                    lInfoX=lX[2]
                                    lInfoY=lY[2]
                        if lX[1]==u'AUDIT_OPK':
                            if lY[1]!=u'AUDIT_NPK':
                                imgNum=self.imgDict['error']
                            else:
                                bObj=True
                #imgNum=self.imgDict['warn']
            except:
                imgNum=self.imgDict['error']
            #idx=self.lstCmp.InsertImageItem(sys.maxint,imgNum)
            #self.lstCmp.SetStringItem(idx,1,sTmX)
            #self.lstCmp.SetStringItem(idx,2,sTmY)
            if bObj:
                self.__updateCmpObj__(lX[2],lY[2])
                #self.nbCmp.SetSelection(self.NB_CMP_OBJ)
            else:
                if bXml:
                    self.__updateCmpXml__(lInfoX,lInfoY)
                    self.__updateCmpStr__(lInfoX,lInfoY)
                    #self.nbCmp.SetSelection(self.NB_CMP_XML)
                else:
                    self.__updateCmpStr__(lInfoX,lInfoY)
                    #self.nbCmp.SetSelection(self.NB_CMP_STR)
        except:
            vtLog.vtLngTB(self.GetName())
    def __updateCmpObj__(self,oX,oY):
        vtLog.vtLngCurWX(vtLog.ERROR,'feautre not released yet.'%(),self)
    def __updateCmpStr__(self,sX,sY):
        lX=sX.split('\n')
        lY=sY.split('\n')
        iLenX=len(lX)
        iLenY=len(lY)
        iX,iY=0,0
        while iX<iLenX or iY<iLenY:
            iCmp=0
            if iX<iLenX:
                if iY<iLenY:
                    iCmp=cmp(lX[iX],lY[iY])
                else:
                    iCmp=-1
            else:
                iCmp=1
            if 0:
                if iCmp<0:
                    imgNum=self.imgDict['warn']
                    idx=self.lstCmpStr.InsertImageItem(sys.maxint,imgNum)
                    self.lstCmpStr.SetStringItem(idx,1,lX[iX])
                    iX+=1
                elif iCmp>0:
                    imgNum=self.imgDict['warn']
                    idx=self.lstCmpStr.InsertImageItem(sys.maxint,imgNum)
                    self.lstCmpStr.SetStringItem(idx,2,lY[iY])
                    iY+=1
            if iCmp!=0:
                imgNum=self.imgDict['warn']
                idx=self.lstCmpStr.InsertImageItem(sys.maxint,imgNum)
                if iX<iLenX:
                    self.lstCmpStr.SetStringItem(idx,1,lX[iX])
                if iY<iLenY:
                    self.lstCmpStr.SetStringItem(idx,2,lY[iY])
                iX+=1
                iY+=1
            else:
                imgNum=-1
                idx=self.lstCmpStr.InsertImageItem(sys.maxint,imgNum)
                self.lstCmpStr.SetStringItem(idx,1,lX[iX])
                self.lstCmpStr.SetStringItem(idx,2,lY[iY])
                iX+=1
                iY+=1
    def __addXmlAttr__(self,trlst,ti,docX,nX,docY,nY):
        lAttrsX=docX.getAttrsValues(nX,bSkip=False)
        lAttrsY=docY.getAttrsValues(nY,bSkip=False)
        iLenX=len(lAttrsX)
        iLenY=len(lAttrsY)
        iX=0
        iY=0
        #if iLenX>0 or iLenY>0:
        #    ti=trlst.AppendItem(tip,'__attrs__')
        while iX<iLenX or iY<iLenY:
            if iX<iLenX:
                if iY<iLenY:
                    iC=cmp(lAttrsX[iX][0],lAttrsY[iY][0])
                else:
                    iC=-1
            else:
                if iY<iLenY:
                    iC=1
                else:
                    break
            if iC == 0:
                tic=trlst.AppendItem(ti,lAttrsX[iX][0])
                trlst.SetItemHasChildren(tic,True)
                #trlst.SetItemImage(tic,img,0,which=wx.TreeItemIcon_Normal)
                trlst.SetItemText(tic,lAttrsX[iX][1],1)
                trlst.SetItemText(tic,lAttrsY[iY][1],2)
                iX+=1
                iY+=1
            elif iC < 0:
                tic=trlst.AppendItem(ti,lAttrsX[iX][0])
                trlst.SetItemHasChildren(tic,True)
                trlst.SetItemText(tic,lAttrsX[iX][1],1)
                iX+=1
            else:
                tic=trlst.AppendItem(ti,lAttrsY[iY][0])
                trlst.SetItemHasChildren(tic,True)
                trlst.SetItemText(tic,lAttrsY[iY][1],2)
                iY+=1
    def __addXmlDiffRes__(self,trlst,ti,dRes):
        def addKeys(ti,keys,imgIdx):
            l=[]
            for k in keys:
                tic=trlst.AppendItem(ti,k)
                trlst.SetItemImage(tic,imgIdx,0,which=wx.TreeItemIcon_Normal)
                trlst.SetItemHasChildren(tic,True)
                l.append((k,tic))
            return l
        def addInfos(ti,tKeys,d):
            for k,tic in tKeys:
                if k in d:
                    lRes=d[k]
                    if lRes is not None:
                        if lRes[0] is not None:
                            sN=lRes[0][0]
                            sX=lRes[0][1]
                        else:
                            sX=''
                        if lRes[1] is not None:
                            sN=lRes[1][0]
                            sY=lRes[1][1]
                        else:
                            sY=''
                        strsX=sX.split('\n')
                        strsY=sY.split('\n')
                        trlst.SetItemText(tic,strsX[0],1)
                        trlst.SetItemText(tic,strsY[0],2)
                        iLenX=len(strsX)
                        iLenY=len(strsY)
                        i=1
                        for i in xrange(1,max(iLenX,iLenY)):
                        #for v in strs[1:]:
                            ticc=trlst.AppendItem(tic,_(u'line:%d')%i)
                            trlst.SetItemImage(ticc,self.imgDictXml['empty'],0,
                                        which=wx.TreeItemIcon_Normal)
                            if i<iLenX:
                                trlst.SetItemText(ticc,strsX[i],1)
                            if i<iLenY:
                                trlst.SetItemText(ticc,strsY[i],2)
        d=dRes[1]
        keys=d.keys()
        keys.sort()
        tKeys=addKeys(ti,keys,self.imgDictXml['attrs'])
        addInfos(ti,tKeys,d)
        d=dRes[0]
        keys=d.keys()
        keys.sort()
        dC=dRes[2]
        keysC=dC.keys()
        keysC.sort()
        for k in keysC:
            if k not in keys:
                keys.append(k)
        keys.sort()
        tKeys=addKeys(ti,keys,self.imgDictXml['attr'])
        addInfos(ti,tKeys,d)
        for k,tic in tKeys:
            if k in dC:
                dd=dC[k]
                if dd is None:
                    continue
                try:
                    self.__addXmlDiffRes__(trlst,tic,dd)
                    trlst.Expand(tic)
                except:
                    vtLog.vtLngTB(self.GetName())
                    vtLog.vtLngCurWX(vtLog.ERROR,'k:%s;dd:%s'%(k,vtLog.pformat(dd)),self)
    def __updateCmpXml__(self,sX,sY):
        try:
            if 0:
                if len(sX):
                    docX,nX,idX=self.filLog.ConvDataNode(sX)
                else:
                    docX,nX,idX=None,None,-1
                if len(sY):
                    docY,nY,idY=self.filLog.ConvDataNode(sY)
                else:
                    docY,nY,idY=None,None,-1
            #if vtLog.vtLngIsLogged(vtLog.DEBUG):
            #    vtLog.vtLngCurWX(vtLog.DEBUG,'sX;%s;sY:%s'%(sX,sY),self)
            dResRaw,doc,tX,tY=self.filLog.GetXmlDiffResByStr(sX,sY)
            if self.tConvRes4Disp is not None:
                func,args,kwargs=self.tConvRes4Disp
                dRes=func(dResRaw,doc,tX,tY,*args,**kwargs)
            else:
                dRes=dResRaw
                
            docX,nX,idX=tX
            docY,nY,idY=tY
            if docX is None:
                docX=doc
            if docY is None:
                docY=doc
            trlst=self.trlstCmpXml
            if nY is not None:
                ti=trlst.AddRoot(docY.getTagName(nY))
            elif nX is not None:
                ti=trlst.AddRoot(docY.getTagName(nX))
            else:
                ti=trlst.AddRoot(u'???')
            trlst.SetItemImage(ti,self.imgDictXml['element'],0,
                        which=wx.TreeItemIcon_Normal)
            trlst.SetItemText(ti,docX.getTagName(nX),1)
            trlst.SetItemText(ti,docY.getTagName(nY),2)
            #self.__addXmlAttr__(trlst,ti,docX,nX,docY,nY)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'dRes:%s'%(vtLog.pformat(dRes)),self)
            self.__addXmlDiffRes__(trlst,ti,dRes)
            trlst.Expand(ti)
            self.nodeX=nX
            self.nodeY=nY
            self.__enableCmpXml__()
            #if self.cbShowXmlStrX.GetValue():
            #    if self.dlgShowXmlX is not None:
            #        self.dlgShowXmlX.SetNode(self.nodeX)
            #if self.cbShowXmlStrY.GetValue():
            #    if self.dlgShowXmlY is not None:
            #        self.dlgShowXmlY.SetNode(self.nodeY)
            self.OnCbShowXmlStrXButton(None)
            self.OnCbShowXmlStrYButton(None)
            self.OnCbShowXmlObjXButton(None)
            self.OnCbShowXmlObjYButton(None)
        except:
            vtLog.vtLngTB(self.GetName())
    def __enableCmpXml__(self):
        if self.nodeX is None:
            self.cbShowXmlStrX.Enable(False)
            self.cbShowXmlObjX.Enable(False)
            self.cbApplyXmlX.Enable(False)
        else:
            self.cbShowXmlStrX.Enable(True)
            if self.doc is not None:
                try:
                    oReg=self.doc.GetRegByNode(self.nodeY)
                    if oReg.GetPanelClass() is None:
                        self.cbShowXmlObjX.Enable(False)
                    else:
                        self.cbShowXmlObjX.Enable(True)
                except:
                    self.cbShowXmlObjX.Enable(False)
                self.cbApplyXmlX.Enable(True)
            else:
                self.cbShowXmlObjX.Enable(False)
                self.cbApplyXmlX.Enable(False)
        if self.nodeY is None:
            self.cbShowXmlStrY.Enable(False)
            self.cbShowXmlObjY.Enable(False)
            self.cbApplyXmlY.Enable(False)
        else:
            self.cbShowXmlStrY.Enable(True)
            if self.doc is not None:
                try:
                    oReg=self.doc.GetRegByNode(self.nodeY)
                    if oReg.GetPanelClass() is None:
                        self.cbShowXmlObjY.Enable(False)
                    else:
                        self.cbShowXmlObjY.Enable(True)
                except:
                    self.cbShowXmlObjY.Enable(False)
                self.cbApplyXmlY.Enable(True)
            else:
                self.cbShowXmlObjY.Enable(False)
                self.cbApplyXmlY.Enable(False)
        if self.trMain is None or (self.nodeX is None and self.nodeY is None):
            self.cbNavXml.Enable(False)
        else:
            self.cbNavXml.Enable(True)
    def __updateCmpByLst__(self,lX,lY,bCmpLst=False):
        return
        self.lstCmpStr.DeleteAllItems()
        bFirst=True
        for sX,sY in zip(lX,lY):
            try:
                if bCmpLst and bFirst:
                    sX=sX[4:]
                    sY=sY[4:]
                if sX==sY:
                    imgNum=-1  #self.imgDict['def']
                else:
                    imgNum=self.imgDict['warn']
            except:
                imgNum=self.imgDict['error']
            idx=self.lstCmpStr.InsertImageItem(sys.maxint,imgNum)
            if sX is not None:
                self.lstCmpStr.SetStringItem(idx,1,sX)
            if sY is not None:
                self.lstCmpStr.SetStringItem(idx,2,sY)
            bFirst=False
    def __updateCallStack__(self):
        return
        self.lstCallStack.DeleteAllItems()
        if self.idxSel<0:
            self.txtOrigin.SetValue(u'')
            return
        try:
            idxLog=self.lstLogs.GetItemData(self.idxSel)
            lAT=self.filLog.GetData(idxLog)
            print lAT
            return
            log=sLog.split(u'|')
            self.txtOrigin.SetValue(log[1])
            strs=string.split(log[-1],u',')
            for s in strs:
                sFile=u'???'
                sMethod=u'???'
                sLine=u'???'
                i=string.find(s,u':')
                if i>=0:
                    j=string.find(s,u'(')
                    if j>=0:
                        k=string.find(s,u')')
                        if k>=0:
                            #all ok
                            sFile=s[:i]#.decode('utf-8')
                            sMethod=s[i+1:j]#.decode('utf-8')
                            sLine=s[j+1:k]#.decode('utf-8')
                    idx=self.lstCallStack.InsertImageStringItem(sys.maxint,sLine,-1)
                    self.lstCallStack.SetStringItem(idx,1,sFile)
                    self.lstCallStack.SetStringItem(idx,2,sMethod)
        except:
            traceback.print_exc()
    def OnVtLogFileViewerFrameSize(self, event):
        #event.Skip()
        #print 'size',event.GetSize(),self.pnMain.GetSize()
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnMain)
        self.pnMain.Refresh()
    def OnSlwBotSashDragged(self, event):
        if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
            return
        iHeight=event.GetDragRect().height
        #print 'bottom',iHeight
        sz=self.GetSize()
        #szTop=self.slwTop.GetSize()
        #print sz,szTop
        iH=sz[1]-200
        #print iH
        #if iHeight>iH:
        #    iHeight=iH
        self.slwBot.SetDefaultSize((1000 , iHeight))
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnMain)
        self.pnMain.Refresh()
        #event.Skip()
    def OnLogViewerProc(self,evt):
        evt.Skip()
        if evt.GetSize()==0:
            iProc=0
        else:
            iProc=int((evt.GetPos()/float(evt.GetSize()))*1000)
        self.gProcess.SetValue(iProc)
    def OnFilLogSel(self,evt):
        evt.Skip()
        iPos=evt.GetPos()
        for idx in xrange(self.lstLogs.GetItemCount()):
            idxLog=self.lstLogs.GetItemData(idx)
            if idxLog==iPos:
                self.lstLogs.SetItemState(idx,
                        wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
                self.lstLogs.EnsureVisible(idx)
                break
    #def _addLog(self,pos,lAT,idxLog):
    def _addLog(self,pos,tup,idxLog,bSel):
        try:
            lAT=self.filLog.GetValue(tup)
            l=self.filLog.GetAuditValue(lAT,0)
            if l is None:
                return
            #print l
            #print log
            iSel=self.lstLogs.GetFirstSelected()
            if bSel:
                if iSel==-1:
                    #iSel=0
                    pass
                else:
                    self.lstLogs.Select(iSel,False)
            #if len(lAT)>=6:
            #    id=l[4]
            #    sTagName=l[5]
            #else:
            id,sTagName=self.filLog.GetNodeInfoByAuditTrail(lAT)
            sOrigin=self.filLog.GetOriginByAuditTrail(lAT)
            imgIdx=-1
            try:
                if lAT[3] in self.imgDictLog:
                    imgIdx=self.imgDictLog[lAT[3]]
                else:
                    imgIdx=self.imgDictLog['quest']
            except:
                pass
            idx=self.lstLogs.InsertImageItem(pos, imgIdx)
            #idx=self.lstLogs.InsertImageStringItem(pos, l[0], -1)
            if id>=0:
                self.lstLogs.SetStringItem(idx,1,str(id),-1)
            if sTagName is not None:
                self.lstLogs.SetStringItem(idx,2,sTagName,-1)
            
            if len(lAT)>3:
                if lAT[4] is not None:
                    self.lstLogs.SetStringItem(idx,3,lAT[4],-1)
                else:
                    self.lstLogs.SetStringItem(idx,3,string.strip(l[2]),-1)
            else:
                self.lstLogs.SetStringItem(idx,3,string.strip(l[2]),-1)
            self.lstLogs.SetStringItem(idx,4,string.strip(l[3]),-1)
            self.lstLogs.SetStringItem(idx,5,string.strip(l[1]),-1)
            self.lstLogs.SetStringItem(idx,6,sOrigin,-1)
            self.lstLogs.SetItemData(idx,idxLog)
            if bSel:
                if iSel==-1:
                    wx.CallAfter(self.lstLogs.Select,0)
                elif pos==0:
                    self.lstLogs.Select(iSel)
                else:
                    self.lstLogs.Select(iSel+1)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnFilLogVtinoutFileidxlinesAdd(self, evt):
        evt.Skip()
        lAT=evt.GetStr()
        obj=self.filLog
        if lAT is None:
            if evt.Is2Sel():
                self.lstLogs.Select(0)
                self.idxSel=0
                return
            self.lstLogs.DeleteAllItems()
            return
        self.lstLogs.Freeze()
        try:
            if evt.AtEnd():
                #self.lstLogs.InsertStringItem(sys.maxint,evt.GetStr())
                self._addLog(sys.maxint,lAT,evt.GetIdx(),evt.Is2Sel())
                if self.lstLogs.GetItemCount()>obj.GetBufSize():
                    self.lstLogs.DeleteItem(0)
            else:
                #self.lstLogs.InsertStringItem(0,evt.GetStr())
                self._addLog(0,lAT,evt.GetIdx(),evt.Is2Sel())
                if self.lstLogs.GetItemCount()>obj.GetBufSize():
                    self.lstLogs.DeleteItem(self.lstLogs.GetItemCount()-1)
        except:
            pass
        self.lstLogs.Thaw()
    def OnMnFileItem_closeMenu(self, event):
        event.Skip()
        try:
            self.filLog.Close()
            if self.filLog.IsClosed():
                self.Clear()
        except:
            traceback.print_exc()
    def OpenCfgFile(self,sFN):
        self.cfg.readFN(sFN)
        sect=self.cfg.getSection('layout')
        try:
            iX=int(sect.getInfo('x'))
            iY=int(sect.getInfo('y'))
            iWidth=int(sect.getInfo('width'))
            iHeight=int(sect.getInfo('height'))
            iBottomHeight=int(sect.getInfo('bottom_height'))
            iX,iY,iWidth,iHeight=vSystem.LimitWindowToScreen(iX,iY,iWidth,iHeight)
            
            if iBottomHeight>(iHeight/2):
                iBottomHeight=iHeight/2
            if iBottomHeight<30:
                iBottomHeight=30
            self.Move((iX,iY))
            self.SetSize((iWidth,iHeight))
            self.slwBot.SetDefaultSize((1000 , iBottomHeight))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnMain)
            self.pnMain.Refresh()
        except:
            pass
        l=[]
        sect=self.cfg.getSection('find')
        if sect is not None:
            for i in xrange(50):
                val=sect.getInfo('store_%02d'%i)
                if val is not None:
                    v=val.strip()
                    if len(v)>0:
                        l.append(val)
        self.filLog.SetFindStoreLst(l)
    def SaveCfgFile(self,sFN=None):
        l=self.filLog.GetFindStoreLst()
        sect=self.cfg.getSection('layout')
        pos=self.GetPosition()
        sz=self.GetSize()
        sect.setInfo('x',str(pos[0]))
        sect.setInfo('y',str(pos[1]))
        sect.setInfo('width',str(sz[0]))
        sect.setInfo('height',str(sz[1]))
        sect.setInfo('bottom_height',str(self.slwBot.GetSize()[1]))
        sect=self.cfg.getSection('find')
        i=0
        iCount=len(l)
        i=iCount-50
        if i<0:
            i=0
        iNum=0
        for s in l[i:]:
            sect.setInfo('store_%02d'%iNum,s)
            iNum+=1
        for i in xrange(iNum,50):
            sect.setInfo('store_%02d'%i,'')
        self.cfg.writeFN(sFN)
    def OnMnFileMnfile_cfg_openMenu(self, event):
        event.Skip()
        dlg = wx.FileDialog(self, _(u"Open Config"), "", "", _(u"ini files (*.ini)|*.ini|all files (*.*)|*.*"), wx.OPEN)
        try:
            fn=self.GetFN()
            if fn is not None:
                dlg.SetPath(fn)
            else:
                sLogDN=os.getenv('vidarcLogDN')
                if sLogDN is not None:
                    dlg.SetPath(sLogDN+os.path.sep)
            #else:
            #    dlg.SetPath('.')
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.OpenCfgFile(filename)
                #self.__setModified__(False)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
    def OnMnFileMnfile_save_cfgMenu(self, event):
        event.Skip()
        dlg = wx.FileDialog(self, _("Save Config"), ".", "", _("ini files (*.ini)|*.ini|all files (*.*)|*.*"), wx.SAVE)
        try:
            fn=self.GetFN()
            if fn is not None:
                dlg.SetPath(fn[:-3]+'ini')
            else:
                sLogDN=os.getenv('vidarcLogDN')
                if sLogDN is not None:
                    dlg.SetPath(sLogDN+os.path.sep)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.SaveCfgFile(filename)
        finally:
            dlg.Destroy()
    def OnMnHelpMn_help_aboutMenu(self, event):
        event.Skip()
        try:
            tup=self.GetAboutData()
            dlg=vAboutDialog.create(self,images.getApplAuditTrailBitmap(),
                    tup[0],tup[1],tup[2],False)
            dlg.Centre()
            dlg.ShowModal()
            dlg.Destroy()
        except:
            traceback.print_exc()

    def OnCbCmpSet1Button(self, event):
        event.Skip()

    def OnCbCmpSet2Button(self, event):
        event.Skip()

    def OnCbCmpUpdateButton(self, event):
        event.Skip()
    def __createXmlDlg__(self,name,tgBtn,title):
        doc=self.filLog.GetNewDoc()
        dlg=vtLogAuditTrailFileViewerXmlDlg(doc,tgBtn,title,
                    self,wx.DefaultSize,0,name)
        return dlg
    def __createXmlObjDlg__(self,name,tgBtn,title,doc,oReg,cls):
        #doc=self.filLog.GetNewDoc()
        dlg=vtLogAuditTrailFileViewerXmlObjDlg(doc,tgBtn,title,oReg,cls,
                    self,wx.DefaultSize,0,name)
        return dlg
    def OnCbShowXmlStrXButton(self, event):
        if event is not None:
            event.Skip()
        if self.dlgShowXmlX is None:
            self.dlgShowXmlX=self.__createXmlDlg__('vtLogAuditTrailFileViewerXML_X',
                    self.cbShowXmlStrX,_(u'VIDARC Log Audit Trail View: XML x (old data)'))
        if self.cbShowXmlStrX.GetValue():
            self.dlgShowXmlX.SetNode(self.nodeX)
            self.dlgShowXmlX.Show(True)
        else:
            self.dlgShowXmlX.SetNode(None)
            self.dlgShowXmlX.Show(False)
    def OnCbShowXmlStrYButton(self, event):
        if event is not None:
            event.Skip()
        if self.dlgShowXmlY is None:
            self.dlgShowXmlY=self.__createXmlDlg__('vtLogAuditTrailFileViewerXML_Y',
                    self.cbShowXmlStrY,_(u'VIDARC Log Audit Trail View: XML y (new data)'))
        if self.cbShowXmlStrY.GetValue():
            self.dlgShowXmlY.SetNode(self.nodeY)
            self.dlgShowXmlY.Show(True)
        else:
            self.dlgShowXmlY.SetNode(None)
            self.dlgShowXmlY.Show(False)
    def OnCbShowXmlObjXButton(self, event):
        if event is not None:
            event.Skip()
        try:
            if self.doc is not None:
                if self.dlgShowXmlObjX[''] is not None:
                    sTagOld=self.dlgShowXmlObjX['']
                    if sTagOld in self.dlgShowXmlObjX:
                        self.dlgShowXmlObjX[sTagOld].Show(False)
                        self.dlgShowXmlObjX['']=None
                if self.nodeX is None:
                    self.cbShowXmlObjX.SetValue(False)
                    return
                if self.cbShowXmlObjX.GetValue()==False:
                    return
                oReg=self.doc.GetRegByNode(self.nodeX)
                #oReg.DoEdit(self,self.doc,self.nodeX,bModal=False)
                sTag=oReg.GetTagName()
                if sTag in self.dlgShowXmlObjX:
                    dlg=self.dlgShowXmlObjX[sTag]
                else:
                    dlg=self.__createXmlObjDlg__('vtLogAuditTrailFileViewerXMLObj_X',
                            self.cbShowXmlObjX,_(u'VIDARC Log Audit Trail View: XML object x (old data)'),
                            self.doc,oReg,oReg.GetPanelClass())
                    self.dlgShowXmlObjX[sTag]=dlg
                if self.cbShowXmlObjX.GetValue():
                    dlg.SetNode(self.nodeX)
                    dlg.Show(True)
                    self.dlgShowXmlObjX['']=sTag
                else:
                    dlg.SetNode(None)
                    dlg.Show(False)
                    self.dlgShowXmlObjX['']=None
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbShowXmlObjYButton(self, event):
        if event is not None:
            event.Skip()
        try:
            if self.doc is not None:
                if self.dlgShowXmlObjY[''] is not None:
                    sTagOld=self.dlgShowXmlObjY['']
                    if sTagOld in self.dlgShowXmlObjY:
                        self.dlgShowXmlObjY[sTagOld].Show(False)
                        self.dlgShowXmlObjY['']=None
                if self.nodeY is None:
                    self.cbShowXmlObjY.SetValue(False)
                    return
                if self.cbShowXmlObjY.GetValue()==False:
                    return
                oReg=self.doc.GetRegByNode(self.nodeY)
                #oReg.DoEdit(self,self.doc,self.nodeY,bModal=False)
                sTag=oReg.GetTagName()
                if sTag in self.dlgShowXmlObjY:
                    dlg=self.dlgShowXmlObjY[sTag]
                else:
                    dlg=self.__createXmlObjDlg__('vtLogAuditTrailFileViewerXMLObj_Y',
                            self.cbShowXmlObjY,_(u'VIDARC Log Audit Trail View: XML object y (new data)'),
                            self.doc,oReg,oReg.GetPanelClass())
                    self.dlgShowXmlObjY[sTag]=dlg
                if self.cbShowXmlObjY.GetValue():
                    dlg.SetNode(self.nodeY)
                    dlg.Show(True)
                    self.dlgShowXmlObjY['']=sTag
                else:
                    dlg.SetNode(None)
                    dlg.Show(False)
                    self.dlgShowXmlObjY['']=None
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbApplyXmlXButton(self, event):
        event.Skip()
        try:
            if self.nodeX is not None:
                if self.doc.IsLocked(self.nodeX):
                    s=self.doc.GetNodeXmlContent(self.nodeX,False,False)
                    idNum=self.doc.getKeyNum(self.nodeX)
                    node=self.doc.getNodeByIdNum(idNum)
                    if node is not None:
                        #self.doc.startEdit(node)
                        self.doc.setNodeXmlContent(node,s)
                        self.doc.doEdit(node)
                        #self.doc.endEdit(node)
                        #n=self.doc.getBaseNode()
                        #self.trMain.Select(n)
                        #wx.CallAfter(self.doc.SelectById,'-1')
                        #wx.CallAfter(self.doc.SelectById,str(idNum))
                else:
                    dlg=vtmMsgDialog(self,_(u'Node id:%s is not locked\n\nEdit is not possible!')%self.doc.getKey(self.nodeX) ,
                            'vtLog Audit Trail',
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                    dlg.ShowModal()
                    dlg.Destroy()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbApplyXmlYButton(self, event):
        event.Skip()
        try:
            if self.nodeY is not None:
                if self.doc.IsLocked(self.nodeY):
                    s=self.doc.GetNodeXmlContent(self.nodeY,False,False)
                    idNum=self.doc.getKeyNum(self.nodeY)
                    node=self.doc.getNodeByIdNum(idNum)
                    if node is not None:
                        #self.doc.startEdit(node)
                        self.doc.setNodeXmlContent(node,s)
                        self.doc.doEdit(node)
                        #self.doc.endEdit(node)
                        #n=self.doc.getBaseNode()
                        #self.trMain.Select(n)
                        #wx.CallAfter(self.doc.SelectById,'-1')
                        #wx.CallAfter(self.doc.SelectById,str(idNum))
                else:
                    dlg=vtmMsgDialog(self,_(u'Node id:%s is not locked\n\nEdit is not possible!')%self.doc.getKey(self.nodeY) ,
                            'vtLog Audit Trail',
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                    dlg.ShowModal()
                    dlg.Destroy()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbNavXmlButton(self, event):
        event.Skip()
        try:
            if self.trMain is not None:
                id=None
                if self.nodeY is not None:
                    id=self.doc.getKey(self.nodeY)
                elif self.nodeX is not None:
                    id=self.doc.getKey(self.nodeX)
                if id is not None:
                    self.trMain.SelectByID(id)
        except:
            vtLog.vtLngTB(self.GetName())
    def GetAboutData(self):
        import __config__
        version=u' %d.%d.%d.%d'%(__config__.LOG_AUDIT_TRAIL_FILE_VIEWER_VER_MAJOR,
                    __config__.LOG_AUDIT_TRAIL_FILE_VIEWER_VER_MINOR,
                    __config__.LOG_AUDIT_TRAIL_FILE_VIEWER_VER_RELEASE,
                    __config__.LOG_AUDIT_TRAIL_FILE_VIEWER_VER_SUBREL)
        desc=_(u"""Graphical log file viewer to analyse VIDARC log outputs.

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
""")
        return _(u'VIDARC log-audit-trail-file viewer'),desc,version

    def OnCbCmpSet1Button(self, event):
        event.Skip()
        self.iX=self.txtMsg.GetInsertionPoint()
    def OnCbCmpSet2Button(self, event):
        event.Skip()
        self.iY=self.txtMsg.GetInsertionPoint()
    def OnCbCmpUpdateButton(self, event):
        event.Skip()
        if self.iX<0:
            return
        if self.iY<0:
            return
        sMsg=self.txtMsg.GetValue()
        lX=sMsg[self.iX:self.iY].split('\n')
        lY=sMsg[self.iY:].split('\n')
        #self.lstCmpStr.DeleteAllItems()
        self.__updateCmpByLst__(lX,lY)

class vtLogAuditTrailFileViewerXmlObjDlg(wx.Dialog):
    def __init__(self, doc,tgBtn,title,oReg,cls,parent, size,style,name=''):
        wx.Dialog.__init__(self,parent=parent,style=wx.RESIZE_BORDER | wx.CAPTION, #wx.DEFAULT_DIALOG_STYLE,
                    name=name)#wx.BORDER_SIMPLE)#|wx.STAY_ON_TOP)
        fgsMain = wx.FlexGridSizer(cols=1, hgap=0, rows=4, vgap=4)
        self.SetTitle(title)
        self.pnObj=cls(self,-1,wx.DefaultPosition,wx.DefaultSize,0,
                '.'.join([name,'panel']))
        fgsMain.AddWindow(self.pnObj, 1, border=4, flag=wx.LEFT|wx.RIGHT|wx.EXPAND)
        self.doc=doc
        self.tgBtn=tgBtn
        #self.pnObj.
        self.pnObj.SetRegNode(oReg)
        self.pnObj.SetNetDocs(doc.GetNetDocs())
        self.pnObj.SetDoc(doc)
        
        cbClose = wx.lib.buttons.GenBitmapTextButton(ID=-1,
              bitmap=vtArt.getBitmap(vtArt.Close), label=_(u'Close'),
              name=u'cbClose', parent=self, 
              size=wx.Size(124, 30), style=0)
        #self.cbReload.SetMinSize((-1,-1))
        cbClose.Bind(wx.EVT_BUTTON, self.OnCbCloseButton,cbClose)
        fgsMain.AddWindow(cbClose, 1, border=4, flag=wx.LEFT|wx.RIGHT|wx.EXPAND)
        fgsMain.AddGrowableCol(0)
        fgsMain.AddGrowableRow(0)
        self.SetSizer(fgsMain)
        fgsMain.Layout()
        fgsMain.Fit(self)
        self.Centre()
        sz=self.GetSize()
    def SetNode(self,node):
        if node is not None:
            self.pnObj.SetNode(node)
            #self.pnObj.Lock(True)
        else:
            self.pnObj.Clear()
    def OnCbCloseButton(self,evt):
        if self.tgBtn is not None:
            self.tgBtn.SetValue(False)
        self.Show(False)
    def Close(self):
        if self.doc is not None:
            self.pnObj.SetDoc(None)
            self.doc.DelConsumer(self.pnObj)

class vtLogAuditTrailFileViewerXmlDlg(wx.Dialog):
    def __init__(self, doc,tgBtn,title,parent, size,style,name=''):
        wx.Dialog.__init__(self,parent=parent,style=wx.RESIZE_BORDER | wx.CAPTION, #wx.DEFAULT_DIALOG_STYLE,
                    name=name)#wx.BORDER_SIMPLE)#|wx.STAY_ON_TOP)
        fgsMain = wx.FlexGridSizer(cols=1, hgap=0, rows=4, vgap=4)
        self.SetTitle(title)
        #self.trAttr=vtXmlGrpAttrTreeList(parent=self,id=-1,
        #        pos=(0,0),size=(200,100),style=0,name='trAttr',
        #        cols=['tag','name'])
        #self.trAttr.SetDoc(doc)
        #fgsMain.AddWindow(self.trAttr, 1, border=4, flag=wx.LEFT|wx.RIGHT|wx.EXPAND)
        self.trlstAttr = wx.gizmos.TreeListCtrl(id=wxID_VTLOGAUDITTRAILFILEVIEWERFRAMETRLSTCMPXML,
              name=u'trlstAttr', parent=self, pos=wx.Point(4, 21),
              size=wx.Size(372, 386), style=wx.TR_HAS_BUTTONS|wx.TR_TWIST_BUTTONS| wx.TR_FULL_ROW_HIGHLIGHT)
        self.imgLstXml=wx.ImageList(16,16)
        self.imgDictXml={}
        img=vtArt.getBitmap(vtArt.Invisible)
        self.imgDictXml['empty']=self.imgLstXml.Add(img)
        img=vtArt.getBitmap(vtArt.Element)
        self.imgDictXml['element']=self.imgLstXml.Add(img)
        img=vtArt.getBitmap(vtArt.Attr)
        self.imgDictXml['attr']=self.imgLstXml.Add(img)
        img=vtArt.getBitmap(vtArt.Attrs)
        self.imgDictXml['attrs']=self.imgLstXml.Add(img)
        self.trlstAttr.SetImageList(self.imgLstXml)
        self.trlstAttr.AddColumn(_(u'tag'))
        self.trlstAttr.AddColumn(_(u'value'))
        self.trlstAttr.SetMinSize((-1,-1))
        fgsMain.AddWindow(self.trlstAttr, 1, border=4, flag=wx.LEFT|wx.RIGHT|wx.EXPAND)
        self.doc=doc
        self.tgBtn=tgBtn
        
        cbClose = wx.lib.buttons.GenBitmapTextButton(ID=-1,
              bitmap=vtArt.getBitmap(vtArt.Close), label=_(u'Close'),
              name=u'cbClose', parent=self, 
              size=wx.Size(124, 30), style=0)
        #self.cbReload.SetMinSize((-1,-1))
        cbClose.Bind(wx.EVT_BUTTON, self.OnCbCloseButton,cbClose)
        fgsMain.AddWindow(cbClose, 1, border=4, flag=wx.LEFT|wx.RIGHT|wx.EXPAND)
        fgsMain.AddGrowableCol(0)
        fgsMain.AddGrowableRow(0)
        self.SetSizer(fgsMain)
        self.Centre()
        sz=self.GetSize()
        w=sz[0]/3
        self.trlstAttr.SetColumnWidth(0,w)
        self.trlstAttr.SetColumnWidth(1,sz[0]-w-30)
        
    def __addXmlAttr__(self,trlst,ti,doc,n):
        lAttrs=doc.getAttrsValues(n,bSkip=False)
        img=self.imgDictXml['attrs']
        for k,v in lAttrs:
            tic=trlst.AppendItem(ti,k)
            trlst.SetItemHasChildren(tic,True)
            trlst.SetItemImage(tic,img,0,which=wx.TreeItemIcon_Normal)
            trlst.SetItemText(tic,v,1)
    def __addXmlChild__(self,trlst,ti,doc,n):
        childs=doc.getChilds(n)
        tKeys=[(doc.getTagName(c),c) for c in childs]
        def cmpFunc(a,b):
            return cmp(a[0],b[0])
        tKeys.sort(cmpFunc)
        
        for k,c in tKeys:
            tic=trlst.AppendItem(ti,k)
            trlst.SetItemHasChildren(tic,True)
            if doc.IsNodeKeyValid(c):
                imgIdx=self.imgDictXml['element']
            else:
                imgIdx=self.imgDictXml['attr']
            trlst.SetItemImage(tic,imgIdx,0,which=wx.TreeItemIcon_Normal)
            v=doc.getText(c)
            strs=v.split('\n')
            trlst.SetItemText(tic,strs[0],1)
            i=1
            for v in strs[1:]:
                ticc=trlst.AppendItem(tic,_(u'line:%d')%i)
                trlst.SetItemImage(ticc,self.imgDictXml['empty'],0,
                            which=wx.TreeItemIcon_Normal)
                trlst.SetItemText(ticc,v,1)
                i+=1
            self.__addXmlAttr__(trlst,tic,doc,c)
            if doc.hasChilds(n):
                self.__addXmlChild__(trlst,tic,doc,c)
        trlst.Expand(ti)
    def SetNode(self,node):
        self.trlstAttr.DeleteAllItems()
        if node is not None:
            trlst=self.trlstAttr
            ti=trlst.AddRoot(self.doc.getTagName(node))
            trlst.SetItemImage(ti,self.imgDictXml['element'],0,
                        which=wx.TreeItemIcon_Normal)
            self.__addXmlAttr__(trlst,ti,self.doc,node)
            self.__addXmlChild__(trlst,ti,self.doc,node)
    def OnCbCloseButton(self,evt):
        if self.tgBtn is not None:
            self.tgBtn.SetValue(False)
        self.Show(False)
