#----------------------------------------------------------------------------
# Name:         vtLogAuditTrailFile.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080126
# CVS-ID:       $Id: vtLogAuditTrailFile.py,v 1.10 2009/05/17 11:43:28 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.gizmos
import vidarc.tool.art.vtThrobber

import fnmatch,time,traceback,types
import binascii,cPickle

from vidarc.tool.InOut.vtInOutFileIdxLines import vtInOutFileIdxLines
from vidarc.tool.InOut.vtInOutFileIdxLinesWid import vtInOutFileIdxLinesWid
from vidarc.tool.InOut.vtInOutFileIdxLinesWid import vtInOutFieldIdxLinesTransientPopup
from vidarc.tool.InOut.vtInOutFileIdxLinesWid import vtInOutBufIndicator
import vidarc.tool.vtThread
import vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.tool.time.vtTimeLoadBound as vtLdBd
import vidarc.tool.log.vtLogAuditTrail as vtLAT
import vtLog
from vidarc.tool.misc.vtmMsgDialog import vtmMsgDialog
from vidarc.tool.xml.vtXmlDom import vtXmlDom
from vidarc.tool.xml.vtXmlDiff import vtXmlDiff

VERBOSE_FIND=0

wxEVT_VTLOGAUDITTRAIL_FILE_PROC=wx.NewEventType()
vEVT_VTLOGAUDITTRAIL_FILE_PROC=wx.PyEventBinder(wxEVT_VTLOGAUDITTRAIL_FILE_PROC,1)
def EVT_VTLOGAUDITTRAIL_FILE_PROC(win,func):
    win.Connect(-1,-1,wxEVT_VTLOGAUDITTRAIL_FILE_PROC,func)
class vtLogAuditTrailFileProc(wx.PyEvent):
    """
    Posted Events:
        Tree Item added event, reports the parent node of the added one
            EVT_VTLOG_FILE_PROC(<widget_name>, self.OnProc)
    """
    def __init__(self,obj,pos,size):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTLOGAUDITTRAIL_FILE_PROC)
        self.obj=obj
        self.pos=pos
        self.size=size
    def GetPos(self):
        return self.pos
    def GetSize(self):
        return self.size

wxEVT_VTLOGAUDITTRAIL_FILE_SEL=wx.NewEventType()
vEVT_VTLOGAUDITTRAIL_FILE_SEL=wx.PyEventBinder(wxEVT_VTLOGAUDITTRAIL_FILE_SEL,1)
def EVT_VTLOGAUDITTRAIL_FILE_SEL(win,func):
    win.Connect(-1,-1,wxEVT_VTLOGAUDITTRAIL_FILE_SEL,func)
class vtLogAuditTrailFileSel(wx.PyEvent):
    """
    Posted Events:
        Tree Item added event, reports the parent node of the added one
            EVT_VTLOG_FILE_SEL(<widget_name>, self.OnSel)
    """
    def __init__(self,obj,pos):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VTLOGAUDITTRAIL_FILE_SEL)
        self.obj=obj
        self.pos=pos
    def GetPos(self):
        return self.pos

class vtLogAuditTrailFileTransientPopup(vtInOutFieldIdxLinesTransientPopup):
    MAP_LEVEL={'AUDIT_SEC':2,'AUDIT_CMD':3,
            'DEBUG':4,'INFO':5,'WARN':6,'WARNING':6,'ERROR':7,
            'CRITICAL':8,'FATAL':8,'':9}
    def __init__(self, parent, size,style,name=''):
        #vtInOutFieldIdxLinesTransientPopup.__init__(self,
        #        parent,size,style,name)
        self.SIZE_PN_SCROLL=wx.SystemSettings.GetMetric(wx.SYS_VSCROLL_X)
        wx.Dialog.__init__(self, parent,style=wx.RESIZE_BORDER,
                    name=parent.GetName()+'_Popup')#wx.BORDER_SIMPLE)#|wx.STAY_ON_TOP)
        self.fgsMain = wx.FlexGridSizer(cols=1, hgap=0, rows=4, vgap=4)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        id=wx.NewId()
        wx.EVT_SIZE(self,self.OnSize)
        #st = wx.StaticText(self, -1,name,pos=(70,4))
        self.cbCancel = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Cancel), name=u'cbCancel',
              parent=self, pos=(0,0), size=(30,30), style=wx.BU_AUTODRAW)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              self.cbCancel)
        bxs.AddWindow(self.cbCancel, 0, border=0, flag=wx.ALIGN_CENTER)
        
        self.cbApply = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Apply), name=u'cbApply',
              parent=self, pos=wx.Point(32, 00), size=wx.Size(30, 30),
              style=wx.BU_AUTODRAW)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              self.cbApply)
        bxs.AddWindow(self.cbApply, 0, border=0, flag=wx.ALIGN_CENTER)
        #bxs.AddWindow(st, 0, border=4, flag=wx.LEFT|wx.EXPAND)
        #self.fgsMain.AddSizer(bxs,1,border=0,flag=wx.EXPAND)
        
        self.cbReload = wx.lib.buttons.GenBitmapTextButton(ID=-1,
              bitmap=vtArt.getBitmap(vtArt.Synch), label=_(u'Reload'),
              name=u'cbReload', parent=self, 
              size=wx.Size(124, 30), style=0)
        self.cbReload.SetMinSize((-1,-1))
        self.cbReload.Bind(wx.EVT_BUTTON, self.OnCbReloadButton,
              self.cbReload)
        bxs.AddWindow(self.cbReload, 1, border=4, flag=wx.LEFT|wx.RIGHT|wx.EXPAND)
        
        self.chcRev = wx.CheckBox(id=-1,
              label=_(u'reverse'), name=u'chcRev', parent=self,
              pos=wx.DefaultPosition, size=wx.DefaultSize, style=0)
        self.chcRev.SetValue(parent.IsReverse())
        bxs.AddWindow(self.chcRev, 1, border=4, flag=wx.LEFT|wx.RIGHT|wx.EXPAND)
        
        st = wx.StaticText(self, -1,_(u'buffer size'),style=wx.ALIGN_RIGHT)
        self.spLimit = wx.SpinCtrl(id=-1,size=wx.Size(60, 30),
              initial=20, max=1000, min=1, name=u'spLimit', parent=self,
              style=wx.SP_ARROW_KEYS|wx.ALIGN_RIGHT)
        self.spLimit.SetMinSize((-1,-1))
        self.spLimit.SetToolTipString(_(u'buffer size'))
        bxs.AddWindow(st, 1, border=16, flag=wx.LEFT|wx.EXPAND)
        bxs.AddWindow(self.spLimit, 1, border=4, flag=wx.LEFT|wx.RIGHT|wx.EXPAND)
        self.spLimit.SetValue(20)
        self.fgsMain.AddSizer(bxs,1,border=0,flag=wx.EXPAND)
        
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        bxs.AddSpacer(wx.Size(30, 8), border=0, flag=0)
        bxs.AddSpacer(wx.Size(30, 8), border=0, flag=0)
        #bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.cbBuild = wx.lib.buttons.GenBitmapTextButton(ID=-1,
              bitmap=vtArt.getBitmap(vtArt.Build), label=_(u'Build'),
              name=u'cbBuild', parent=self, 
              size=wx.Size(124, 30), style=0)
        self.cbBuild.SetMinSize((-1,-1))
        self.cbBuild.Bind(wx.EVT_BUTTON, self.OnCbBuildButton,
              self.cbBuild)
        bxs.AddWindow(self.cbBuild, 1, border=4, flag=wx.LEFT|wx.RIGHT|wx.EXPAND)
        
        self.chcIncomplete = wx.CheckBox(id=-1,
              label=_(u'incomplete'), name=u'chcIncomplete', parent=self,
              pos=wx.DefaultPosition, size=wx.DefaultSize, style=0)
        self.chcIncomplete.SetValue(parent.IsShowIncomplete())
        bxs.AddWindow(self.chcIncomplete, 1, border=4, flag=wx.LEFT|wx.RIGHT|wx.EXPAND)
        
        self.chcModified = wx.CheckBox(id=-1,
              label=_(u'modified'), name=u'chcModified', parent=self,
              pos=wx.DefaultPosition, size=wx.DefaultSize, style=0)
        self.chcModified.SetValue(parent.IsShowModified())
        bxs.AddWindow(self.chcModified, 1, border=4, flag=wx.LEFT|wx.RIGHT|wx.EXPAND)
        
        st = wx.StaticText(self, -1,'',style=wx.ALIGN_RIGHT)
        bxs.AddWindow(st, 1, border=16, flag=wx.LEFT|wx.EXPAND)
        
        
        self.fgsMain.AddSizer(bxs,1,border=4,flag=wx.TOP|wx.EXPAND)
        self.fgsMain.AddGrowableCol(0)
        #self.fgsMain.AddGrowableRow(1)
        
        self.SetSizer(self.fgsMain)
        if size[1]<22:
            size=(size[0],22)
        self.pn=None
        
        self.trlstOrigin = wx.gizmos.TreeListCtrl(id=-1,
              name=u'trlstOrigin', parent=self,size=wx.Size(124, 150),
              style=wx.TR_DEFAULT_STYLE|wx.TR_HAS_BUTTONS|wx.TR_MULTIPLE|wx.TR_FULL_ROW_HIGHLIGHT)
        self.trlstOrigin.AddColumn(text=u'origin')
        self.trlstOrigin.AddColumn(text=u'X')
        self.trlstOrigin.AddColumn(text=u'sec')
        self.trlstOrigin.AddColumn(text=u'cmd')
        self.trlstOrigin.AddColumn(text=u'D')
        self.trlstOrigin.AddColumn(text=u'I')
        self.trlstOrigin.AddColumn(text=u'W')
        self.trlstOrigin.AddColumn(text=u'E')
        self.trlstOrigin.AddColumn(text=u'C')
        self.trlstOrigin.AddColumn(text=u'F')
        self.trlstOrigin.AddColumn(text=u'V')
        self.trlstOrigin.SetColumnWidth(0,150)
        self.trlstOrigin.SetColumnWidth(1,30)
        self.trlstOrigin.SetColumnWidth(2,60)
        self.trlstOrigin.SetColumnAlignment(2,wx.gizmos.TL_ALIGN_RIGHT)
        self.trlstOrigin.SetColumnWidth(3,60)
        self.trlstOrigin.SetColumnAlignment(3,wx.gizmos.TL_ALIGN_RIGHT)
        self.trlstOrigin.SetColumnWidth(4,60)
        self.trlstOrigin.SetColumnAlignment(4,wx.gizmos.TL_ALIGN_RIGHT)
        self.trlstOrigin.SetColumnWidth(5,60)
        self.trlstOrigin.SetColumnAlignment(5,wx.gizmos.TL_ALIGN_RIGHT)
        self.trlstOrigin.SetColumnWidth(6,60)
        self.trlstOrigin.SetColumnAlignment(6,wx.gizmos.TL_ALIGN_RIGHT)
        self.trlstOrigin.SetColumnWidth(7,60)
        self.trlstOrigin.SetColumnAlignment(7,wx.gizmos.TL_ALIGN_RIGHT)
        self.trlstOrigin.SetColumnWidth(8,60)
        self.trlstOrigin.SetColumnAlignment(8,wx.gizmos.TL_ALIGN_RIGHT)
        self.trlstOrigin.SetColumnWidth(9,60)
        self.trlstOrigin.SetColumnAlignment(9,wx.gizmos.TL_ALIGN_RIGHT)
        self.imgDict={}
        self.imgLstTyp=wx.ImageList(16,16)
        self.imgDict['include']=self.imgLstTyp.Add(vtArt.getBitmap(vtArt.Apply))
        self.imgDict['exclude']=self.imgLstTyp.Add(vtArt.getBitmap(vtArt.Cancel))
        self.trlstOrigin.SetImageList(self.imgLstTyp)
        self.fgsMain.AddWindow(self.trlstOrigin, 1, border=4, flag=wx.LEFT|wx.RIGHT|wx.TOP|wx.BOTTOM|wx.EXPAND)
        self.fgsMain.AddGrowableRow(2)
        wx.EVT_TREE_ITEM_RIGHT_CLICK(self, self.trlstOrigin.GetId(),
                self.OnTreeTreeLstOriginRight)
        
        #wx.EVT_RIGHT_UP(self,self.trlstOrigin.GetId(),self.OnRightUp)
        #self.trlstOrigin.Bind(wx.EVT_RIGHT_UP, self.OnRightUp)
        #wx.EVT_RIGHT_UP(self,self.OnRightUp)
        
        st = wx.StaticText(self, -1,_(u'search parameter'),style=wx.ALIGN_RIGHT)
        self.fgsMain.AddWindow(st, 1, border=4, flag=wx.LEFT|wx.RIGHT|wx.TOP|wx.ALIGN_CENTER)
        #bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        #bxs.AddWindow(st, 1, border=4, flag=wx.RIGHT|wx.EXPAND)
        #st = wx.StaticText(self, -1,'',style=wx.ALIGN_RIGHT)
        #bxs.AddWindow(st, 5, border=4, flag=wx.RIGHT|wx.EXPAND)
        #self.fgsMain.AddSizer(bxs,1,border=0,flag=wx.EXPAND)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        st = wx.StaticText(self, -1,_(u'level'),style=wx.ALIGN_RIGHT)
        bxs.AddWindow(st, 1, border=4, flag=wx.RIGHT|wx.EXPAND)
        self.chcFindOrigin = wx.Choice(choices=['---','sec','cmd',
              'debug','info','warn',
              'warning','error','critical','fatal'],
              id=-1, name=u'chcFindOrigin',
              parent=self,style=0)
        bxs.AddWindow(self.chcFindOrigin, 5, border=4, flag=wx.RIGHT|wx.EXPAND)
        self.tgFindDir = wx.lib.buttons.GenBitmapToggleButton(ID=-1,
              bitmap=wx.EmptyBitmap(16, 16), name=u'tgFindDir',
              parent=self, size=wx.Size(31, 30),
              style=0)
        self.tgFindDir.SetBitmapLabel(vtArt.getBitmap(vtArt.Down))
        self.tgFindDir.SetBitmapSelected(vtArt.getBitmap(vtArt.Up))
        bxs.AddWindow(self.tgFindDir, 0, border=4, flag=wx.RIGHT)
        self.fgsMain.AddSizer(bxs,1,border=0,flag=wx.EXPAND)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        st = wx.StaticText(self, -1,_(u'value'),style=wx.ALIGN_RIGHT)
        bxs.AddWindow(st, 1, border=4, flag=wx.RIGHT|wx.EXPAND)
        self.txtFindMsg = wx.TextCtrl(id=-1,
              name=u'txtFindMsg', parent=self, style=0, value=u'')
        bxs.AddWindow(self.txtFindMsg, 5, border=4, flag=wx.RIGHT|wx.EXPAND)
        self.tgCase = wx.lib.buttons.GenBitmapToggleButton(ID=-1,
              bitmap=wx.EmptyBitmap(16, 16), name=u'tgCase',
              parent=self, size=wx.Size(31, 30),
              style=0)
        self.tgCase.SetBitmapLabel(vtArt.getBitmap(vtArt.Glue))
        self.tgCase.SetBitmapSelected(vtArt.getBitmap(vtArt.RubberBand))
        bxs.AddWindow(self.tgCase, 0, border=4, flag=wx.RIGHT)
        self.fgsMain.AddSizer(bxs,1,border=0,flag=wx.EXPAND)
        
        # memorized search parameters
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        st = wx.StaticText(self, -1,_(u'stored'),style=wx.ALIGN_RIGHT)
        bxs.AddWindow(st, 1, border=4, flag=wx.RIGHT|wx.EXPAND)
        
        bxs2 = wx.BoxSizer(orient=wx.HORIZONTAL)
        bxs.AddSizer(bxs2,5,border=0,flag=wx.EXPAND)
        self.chcFindStored = wx.Choice(choices=['---'],
              id=-1, name=u'chcFindStored',
              parent=self,style=0)
        self.chcFindStored.Bind(wx.EVT_CHOICE, self.OnChoiceFindStore)
        bxs2.AddWindow(self.chcFindStored, 2, border=4, flag=wx.RIGHT|wx.EXPAND)
        self.cbFindStoreDel = wx.lib.buttons.GenBitmapButton(ID=-1,
              bitmap=vtArt.getBitmap(vtArt.Del), name=u'cbFindStoreDel',
              parent=self, size=wx.Size(31, 30),
              style=0)
        bxs2.AddWindow(self.cbFindStoreDel, 0, border=4, flag=wx.RIGHT)
        self.cbFindStoreDel.Bind(wx.EVT_BUTTON, self.OnFindStoreDel)
        st = wx.StaticText(self, -1,_(u'new name'),style=wx.ALIGN_RIGHT)
        bxs2.AddWindow(st, 1, border=4, flag=wx.RIGHT|wx.EXPAND)
        self.txtFindStoreName = wx.TextCtrl(id=-1, name='txtFindStoreName',
              parent=self, pos=wx.Point(16, 0), size=wx.Size(100, 21), style=0,
              value='')
        bxs2.AddWindow(self.txtFindStoreName, 2, border=4, flag=wx.RIGHT|wx.EXPAND)
        self.cbFindStoreAdd = wx.lib.buttons.GenBitmapButton(ID=-1,
              bitmap=vtArt.getBitmap(vtArt.Add), name=u'cbFindStoreAdd',
              parent=self, size=wx.Size(31, 30),
              style=0)
        bxs.AddWindow(self.cbFindStoreAdd, 0, border=4, flag=wx.RIGHT)
        self.cbFindStoreAdd.Bind(wx.EVT_BUTTON, self.OnFindStoreAdd)
        self.fgsMain.AddSizer(bxs,1,border=0,flag=wx.EXPAND)
        
        self.chcFindOrigin.SetSelection(0)
        self.chcFindStored.SetSelection(0)
        self.lFindStored=[]
        self.dFindStore={}
        self.iFindStore=0
        self.bForce=False
        
        self._clr()
        self.fgsMain.Layout()
        self.fgsMain.Fit(self)
        self.szOrig=self.GetClientSize()
        self.szOrig=(max(460,self.szOrig[0]),max(540,self.szOrig[1]))
        self.SetSize(self.szOrig)
    def OnSize(self,evt):
        iW,iH=self.GetSize()
        if iW<250:
            iW=250
        if iH<45:
            iH=45
        self.SetSize((iW,iH))
        self.Layout()
    def OnCbReloadButton(self,evt):
        evt.Skip()
        par=self.GetParent()
        try:
            if par.IsRunning():
                dlg=vtmMsgDialog(self,_(u'Thread is already busy, try later again.'),
                                'vtLogAuditTrailFile',
                                wx.OK|wx.ICON_EXCLAMATION )
                dlg.Centre()
                dlg.Destroy()
                return
            par.Index(bForce=False)
        except:
            pass
    def OnCbBuildButton(self,evt):
        evt.Skip()
        try:
            par=self.GetParent()
            if par.IsRunning():
                dlg=vtmMsgDialog(self,_(u'Thread is already busy, try later again.'),
                                'vtLogAuditTrailFile',
                                wx.OK|wx.ICON_EXCLAMATION )
                dlg.Centre()
                dlg.Destroy()
                return
            par.SetBufSize(self.spLimit.GetValue(),bUpdate=False)
            par.SetReverse(self.chcRev.GetValue(),bUpdate=False)
            par.SetShowIncomplete(self.chcIncomplete.GetValue(),bUpdate=False)
            par.SetShowModified(self.chcModified.GetValue(),bUpdate=False)
            par.Index(bForce=True)
        except:
            vtLog.vtLngTB(self.GetName())
    def _addOrigin(self,tip,dOrigin,lOrigin,iPos,iLen,sLogin=None):
        sOrigin=u'.'.join(lOrigin[:iPos+1])
        if iPos==iLen:
            if sLogin is not None:
                sOrigin=u';'.join([sOrigin,sLogin])
        if sOrigin in dOrigin:
            ti,bSel,dCount=dOrigin[sOrigin]
            if ti is not None:
                if iPos<iLen:
                    self._addOrigin(ti,dOrigin,lOrigin,iPos+1,iLen,sLogin=sLogin)
                return
        else:
            bSel=True
            dOrigin[sOrigin]=[None,True,None]
            dCount=None
        if bSel:
            img=self.imgDict['include']
        else:
            img=self.imgDict['exclude']
        if iPos<iLen:
            tic=self.trlstOrigin.AppendItem(tip,lOrigin[iPos])
        else:
            tic=self.trlstOrigin.AppendItem(tip,sLogin)
            self.trlstOrigin.SetPyData(tic,True)
        self.trlstOrigin.SetItemHasChildren(tic)
        self.trlstOrigin.SetItemImage(tic,img,0,which=wx.TreeItemIcon_Normal)
        if bSel:
            self.trlstOrigin.SetItemText(tic,'X',1)
        dOrigin[sOrigin][0]=tic
        #dCount=dOrigin[sOrigin][2]
        if dCount is not None:
            keys=[(k.strip(),k) for k in dCount.keys()]
            keys.sort()
            iVar=0
            for k,kOrig in keys:
                if k in self.MAP_LEVEL:
                    self.trlstOrigin.SetItemText(tic,str(dCount[kOrig]),self.MAP_LEVEL[k])
                else:
                    iVar+=dCount[kOrig]
            self.trlstOrigin.SetItemText(tic,str(iVar),self.MAP_LEVEL[''])
        if iPos<iLen:
            #print iPos,iLen,lOrigin
            self._addOrigin(tic,dOrigin,lOrigin,iPos+1,iLen,sLogin)
    def _updateOrigin(self):
        par=self.GetParent()
        dOrigin=par.GetOriginDict()
        #print vtLog.pformat(dOrigin)
        keys=dOrigin.keys()
        keys.sort()
        tip=self.trlstOrigin.GetRootItem()
        for sOrigin in keys:
            j=sOrigin.rfind(';')
            if j>=0:
                lOrigin=sOrigin[:j].split('.')#+sOrigin[j+1:].split('.')
                sLogin=sOrigin[j+1:]
            else:
                lOrigin=sOrigin.split('.')
                sLogin=None
            self._addOrigin(tip,dOrigin,lOrigin,0,len(lOrigin),sLogin=sLogin)
        #print vtLog.pformat(dOrigin)
        tp=self.trlstOrigin.GetRootItem()
        self.trlstOrigin.Expand(tp)
    def _clr(self):
        self.bForce=False
        self.trlstOrigin.DeleteAllItems()
        ti=self.trlstOrigin.AddRoot(_('origin'))
        self.trlstOrigin.SetItemText(ti,'X',1)
    def OnTreeTreeLstOriginRight(self,evt):
        #print 'OnTreeTreeLstOriginRight'
        self.bForce=True
        tn=evt.GetItem()
        sSel=self.trlstOrigin.GetItemText(tn,1)
        self._modSel(tn,sSel)
    def OnFindStoreAdd(self,evt):
        evt.Skip()
        sName=self.txtFindStoreName.GetValue()
        if len(sName)==0:
            dlg=vtmMsgDialog(self,_(u'Please enter a name first!'),
                    'vtLogAuditTrailFile',wx.OK|wx.ICON_EXCLAMATION)
            dlg.Centre()
            dlg.ShowModal()
            self.txtFindStoreName.SetFocus()
            dlg.Destroy()
            return
        if sName =='---':
            dlg=vtmMsgDialog(self,_(u'Please enter different name!'),
                    'vtLogAuditTrailFile',wx.OK|wx.ICON_EXCLAMATION)
            dlg.Centre()
            dlg.ShowModal()
            self.txtFindStoreName.SetValue('')
            self.txtFindStoreName.SetFocus()
            dlg.Destroy()
            return
        if sName in self.dFindStore:
            dlg=vtmMsgDialog(self,_(u'Name %s already used, set to new search parameters?\n\nYES  ... will use it anyway\n NO  ... keep present.'),
                    'vtLogAuditTrailFile',wx.YES_NO|wx.YES_DEFAULT|wx.ICON_EXCLAMATION)
            dlg.Centre()
            if dlg.ShowModal()==wx.ID_NO:
                dlg.Destroy()
                self.txtFindStoreName.SetFocus()
                return
            dlg.Destroy()
            return
        iLv=self.chcFindOrigin.GetSelection()
        sFlt=self.txtFindMsg.GetValue()
        iCase=self.tgCase.GetValue()
        self.__addFindStore__(sName,iLv,sFlt,iCase)
    def __addFindStore__(self,sName,iLv,sFlt,iCase):
        self.iFindStore+=1
        #iNum=len(self.lFindStore)
        t=(sName,iLv,sFlt,iCase,self.iFindStore)
        self.lFindStore.append(t)
        self.lFindStore.sort()
        self.dFindStore[sName]=t
        keys=self.dFindStore.keys()
        keys.sort()
        iNum=keys.index(sName)
        self.chcFindStored.Insert(sName,iNum+1)
    def OnFindStoreDel(self,evt):
        evt.Skip()
        sName=self.chcFindStored.GetStringSelection()
        if sName=='---':
            return
        iNum=self.chcFindStored.GetSelection()
        self.chcFindStored.Delete(iNum)
        del self.dFindStore[sName]
        iNum=0
        for t in self.lFindStore:
            if t[0]==sName:
                break
            iNum+=1
        del self.lFindStore[iNum]
        self.chcFindStored.SetSelection(iNum)
    def OnChoiceFindStore(self,evt):
        evt.Skip()
        i=self.chcFindStored.GetSelection()
        if i<1:
            return
        sName=self.chcFindStored.GetStringSelection()
        #iNum=0
        for t in self.lFindStore:
            if t[0]==sName:
                self.chcFindOrigin.SetSelection(t[1])
                self.txtFindMsg.SetValue(t[2])
                self.tgCase.SetValue(t[3])
                break
            #iNum+=1
        #self.lFindStore[iNum]
        
    def OnRightUp(self,evt):
        #print 'OnRightUp'
        pos = evt.GetPosition()
        item, flags, col = self.trlstOrigin.HitTest(pos)
        #print item,flags,col
    def SetFindStoreLst(self,l):
        self.lFindStore=[]
        self.dFindStore={}
        self.iFindStore=0
        self.chcFindStored.Clear()
        self.chcFindStored.Append('---')
        for s in l:
            ll=s.split(';')
            sName=ll[0]
            iLv=int(ll[1])
            #sFlt=ll[2]
            #iCase=int(ll[3])
            sFlt=';'.join(ll[2:-1])
            iCase=int(ll[-1])
            self.__addFindStore__(sName,iLv,sFlt,iCase)
        self.chcFindStored.SetSelection(0)
    def GetFindStoreLst(self):
        l=[]
        def cmpNum(a,b):
            return a[4]-b[4]
        self.lFindStore.sort(cmpNum)
        for ll in self.lFindStore:
            s='%s;%d;%s;%d'%(ll[0],ll[1],ll[2],ll[3])
            l.append(s)
        self.lFindStore.sort()
        return l
    def _modSel(self,tn,sSel):
        if len(sSel)>0:
            sSel=''
            img=self.imgDict['exclude']
            bSel=False
        else:
            sSel='X'
            img=self.imgDict['include']
            bSel=True
        l=[]
        try:
            ti=tn
            while 1:
                sVal=self.trlstOrigin.GetItemText(ti,0)
                l.append(sVal)
                ti=self.trlstOrigin.GetItemParent(ti)
        except:
            pass
        l.reverse()
        sOrigin='.'.join(l[1:])
        dOrigin=self.GetParent().GetOriginDict()
        def set(ti,sSel,img,sOrigin,dOrigin,bSel):
            triChild=self.trlstOrigin.GetFirstChild(ti)
            while triChild[0].IsOk():
                sPart=self.trlstOrigin.GetItemText(triChild[0],0)
                tid=self.trlstOrigin.GetPyData(triChild[0])
                if tid is None:
                    if sOrigin is None:
                        k=sPart
                    else:
                        k=sOrigin+'.'+sPart
                else:
                    k=sOrigin+';'+sPart
                dOrigin[k][1]=bSel
                self.trlstOrigin.SetItemText(triChild[0],sSel,1)
                self.trlstOrigin.SetItemImage(triChild[0],img,0,which=wx.TreeItemIcon_Normal)
                if self.trlstOrigin.ItemHasChildren(triChild[0]):
                    set(triChild[0],sSel,img,k,dOrigin,bSel)
                triChild=self.trlstOrigin.GetNextChild(ti,triChild[1])
        self.trlstOrigin.SetItemText(tn,sSel,1)
        if len(sOrigin)>0:
            self.trlstOrigin.SetItemImage(tn,img,0,which=wx.TreeItemIcon_Normal)
            dOrigin[sOrigin][1]=bSel
        else:
            sOrigin=None
        set(tn,sSel,img,sOrigin,dOrigin,bSel)
        #self.__updateOrigin__()
    def GetOrigins2Find(self):
        lOrigin=[]
        dOrigin=self.GetParent().GetOriginDict()
        for k,t in dOrigin.items():
            if t[0] is not None:
                if self.trlstOrigin.IsSelected(t[0]):
                    lOrigin.append(k)
        if len(lOrigin)>0:
            return lOrigin
        return None
    def GetLevel2Find(self):
        iSel=self.chcFindOrigin.GetSelection()
        if iSel>0:
            sLevel=self.chcFindOrigin.GetStringSelection().upper()
            return sLevel
        return None
    def GetMsg2Find(self):
        sVal=self.txtFindMsg.GetValue()
        if len(sVal)>0:
            if self.GetCaseSensitive2Find():
                return sVal
            else:
                return sVal.lower()
        return None
    def SetMsg2Find(self,s):
        self.txtFindMsg.SetValue(s)
    def GetDirection2Find(self):
        return not self.tgFindDir.GetValue()
    def GetCaseSensitive2Find(self):
        return not self.tgCase.GetValue()
    def Apply(self):
        try:
            par=self.GetParent()
            par.SetBufSize(self.spLimit.GetValue(),bUpdate=False)
            par.SetReverse(self.chcRev.GetValue(),bUpdate=False)
            par.SetShowIncomplete(self.chcIncomplete.GetValue(),bUpdate=False)
            par.SetShowModified(self.chcModified.GetValue(),bUpdate=False)
            #par.Index(bForce=self.bForce)
            par.Index(bForce=False)
            #par._scrollFull()
        except:
            import traceback
            traceback.print_exc()
            pass

class vtLogAuditTrailFile(vtInOutFileIdxLinesWid):
    TIME_UPDATE_INTERVAL=0.1
    TIME_DELAY_START=100
    TIME_DELAY_DELTA=10
    TIME_DELAY_FIN=10
    TIME_DELAY_DEC_COUNT=5
    MAP_NR_TO_LEVEL_NAME=['AUDIT_CMD','AUDIT_SEC',
            'DEBUG','INFO','WARNING','WARNING','ERROR','CRITICAL','FATAL']
    OFS_CMD=0
    OFS_IDX=1
    OFS_OLD=2
    OFS_NEW=3
    OFS_CMD_COMPLETE=4       # set valid when endEdit
    OFS_ID=5
    OFS_TAG=6
    #OFS_CMD_UNKNOWN=7
    VERBOSE=-1
    MAP_TAG_FAKE_ID={'config':-100,'settings':-101,'acl':-102,'security_acl':-103}
    def __init__(self, parent, id=-1, pos=wx.DefaultPosition, size=wx.DefaultSize, 
                style=wx.SB_HORIZONTAL, name='vtLogAuditTrailFileWid',
                bufferSize=20,widPost=None,bReverse=True,
                size_button=wx.Size(31,30)):
        global _
        _=vtLgBase.assignPluginLang('vtLog')
        self.dOrigin={}
        wx.Panel.__init__(self,parent,id=id,pos=pos,size=size,style=style,name=name)
        self.SetMinSize(wx.Size(-1, -1))
        self.bxsMain = wx.BoxSizer(orient=wx.VERTICAL)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        bxsPos = wx.BoxSizer(orient=wx.VERTICAL)
        id=wx.NewId()
        self.lblPos = wx.StaticText(self, id,'0',style=wx.ALIGN_RIGHT|wx.ST_NO_AUTORESIZE)
        bxsPos.AddWindow(self.lblPos, 2, border=0, flag=wx.RIGHT|wx.EXPAND)
        self.lblCount = wx.StaticText(self, id,'0',style=wx.ALIGN_RIGHT|wx.ST_NO_AUTORESIZE)
        bxsPos.AddWindow(self.lblCount, 1, border=0, flag=wx.RIGHT|wx.EXPAND)
        bxs.AddSizer(bxsPos,1,border=4,flag=wx.LEFT|wx.BOTTOM|wx.EXPAND)
        
        bxsPos = wx.BoxSizer(orient=wx.VERTICAL)
        id=wx.NewId()
        self.indPos=vtInOutBufIndicator(self,id,bReverse=bReverse,
            space=5)
        bxsPos.AddWindow(self.indPos, 1, border=8, flag=wx.LEFT|wx.RIGHT|wx.EXPAND)
        id=wx.NewId()
        self.slPos = wx.Slider(self, id, 0,0, 1000, style=wx.SL_HORIZONTAL, 
                size=(-1,size_button[1]/3*2))
        #self.Bind(wx.EVT_COMMAND_SCROLL, self.OnScroll, self.slPos)
        self.Bind(wx.EVT_COMMAND_SCROLL_THUMBTRACK, self.OnScrollTrack, self.slPos)
        self.Bind(wx.EVT_COMMAND_SCROLL_THUMBRELEASE, self.OnScrollTrackRelease, self.slPos)
        self.Bind(wx.EVT_COMMAND_SCROLL_PAGEUP, self.OnScrollPgUp, self.slPos)
        self.Bind(wx.EVT_COMMAND_SCROLL_PAGEDOWN, self.OnScrollPgDn, self.slPos)
        bxsPos.AddWindow(self.slPos, 2, border=0, flag=wx.EXPAND)
        bxs.AddSizer(bxsPos,6,border=4,flag=wx.LEFT|wx.RIGHT|wx.EXPAND)
        
        id=wx.NewId()
        self.tgLeft = wx.lib.buttons.GenBitmapToggleButton(ID=id,
              bitmap=vtArt.getBitmap(vtArt.Left), name=u'tgLeft',
              parent=self, pos=wx.DefaultPosition, size=size_button, style=0)
        bxs.AddWindow(self.tgLeft, 0, border=4, flag=wx.LEFT|wx.ALIGN_CENTER)
        self.tgLeft.Bind(wx.EVT_BUTTON,self.OnLeftAuto,self.tgLeft)
        
        id=wx.NewId()
        self.cbLeft = wx.lib.buttons.GenBitmapButton(ID=id,
              bitmap=vtArt.getBitmap(vtArt.Left), name=u'cbLeft',
              parent=self, pos=wx.DefaultPosition, size=size_button, style=0)
        bxs.AddWindow(self.cbLeft, 0, border=4, flag=wx.LEFT|wx.RIGHT|wx.ALIGN_CENTER)
        self.cbLeft.Bind(wx.EVT_BUTTON,self.OnLeft,self.cbLeft)
        #self.cbLeft.Bind(wx.EVT_LEFT_DOWN, self.OnScrollStartLeft)
        #self.cbLeft.Bind(wx.EVT_LEFT_UP, self.OnScrollStop)
        
        id=wx.NewId()
        self.cbRight = wx.lib.buttons.GenBitmapButton(ID=id,
              bitmap=vtArt.getBitmap(vtArt.Right), name=u'cbRight',
              parent=self, pos=wx.DefaultPosition, size=size_button, style=0)
        bxs.AddWindow(self.cbRight, 0, border=4, flag=wx.LEFT|wx.RIGHT|wx.ALIGN_CENTER)
        self.cbRight.Bind(wx.EVT_BUTTON,self.OnRight,self.cbRight)
        #self.cbRight.Bind(wx.EVT_LEFT_DOWN, self.OnScrollStartRight)
        #self.cbRight.Bind(wx.EVT_LEFT_UP, self.OnScrollStop)
        
        id=wx.NewId()
        self.tgRight = wx.lib.buttons.GenBitmapToggleButton(ID=id,
              bitmap=vtArt.getBitmap(vtArt.Right), name=u'tgRight',
              parent=self, pos=wx.DefaultPosition, size=size_button, style=0)
        bxs.AddWindow(self.tgRight, 0, border=4, flag=wx.RIGHT|wx.ALIGN_CENTER)
        self.tgRight.Bind(wx.EVT_BUTTON,self.OnRightAuto,self.tgRight)
        
        id=wx.NewId()
        self.cbFind = wx.lib.buttons.GenBitmapButton(ID=id,
              bitmap=vtArt.getBitmap(vtArt.Find), name=u'cbFind',
              parent=self, pos=wx.DefaultPosition, size=size_button, style=0)
        bxs.AddWindow(self.cbFind, 0, border=4, flag=wx.ALIGN_CENTER|wx.LEFT)
        self.cbFind.Bind(wx.EVT_BUTTON,self.OnFind,self.cbFind)
        #self.cbFind.Bind(wx.EVT_LEFT_DOWN, self.OnFindStart)
        #self.cbFind.Bind(wx.EVT_LEFT_UP, self.OnFindStop)
        self.cbFind.Enable(False)
        
        id=wx.NewId()
        self.thrProc = vidarc.tool.art.vtThrobber.vtThrobberDelay(id=id,
              name=u'thrProc', parent=self, pos=wx.Point(0, 164),
              size=wx.Size(15, 15), style=0)
        bxs.AddWindow(self.thrProc, 0, border=4, flag=wx.LEFT|wx.ALIGN_CENTER)
        
        id=wx.NewId()
        self.cbStop = wx.lib.buttons.GenBitmapButton(id=id,
              bitmap=vtArt.getBitmap(vtArt.Stop), name=u'cbStop',
              parent=self, pos=wx.Point(81, 199), size=wx.Size(76, 30),
              style=0)
        bxs.AddWindow(self.cbStop, 0, border=4, flag=wx.LEFT|wx.ALIGN_CENTER)
        self.cbStop.SetMinSize(wx.Size(-1, -1))
        self.cbStop.Bind(wx.EVT_BUTTON, self.OnCbStopButton)

        id=wx.NewId()
        self.cbPopup = wx.lib.buttons.GenBitmapToggleButton(ID=id,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbPopup',
              parent=self, pos=wx.DefaultPosition, size=size_button, style=wx.BU_AUTODRAW)
        bxs.AddWindow(self.cbPopup, 0, border=4, flag=wx.LEFT|wx.ALIGN_CENTER)
        self.cbPopup.SetBitmapLabel(vtArt.getBitmap(vtArt.Down))
        self.cbPopup.SetBitmapSelected(vtArt.getBitmap(vtArt.Down))
        self.cbPopup.Bind(wx.EVT_BUTTON,self.OnPopupButton,self.cbPopup)#id=id)
        self.popWin=None
        
        self.bxsMain.AddSizer(bxs,1,border=0,flag=wx.EXPAND)
        self.SetSizer(self.bxsMain)
        vtInOutFileIdxLines.__init__(self,bufferSize=bufferSize,par=widPost,bPost=widPost is not None)
        self.bReverse=bReverse
        #self.Bind(wx.EVT_SCROLL, self.OnScroll)
        self.iActScPos=0
        self.iSel=-1
        
        self.SetPostWid(self,True)
        self.Bind(vidarc.tool.vtThread.vEVT_VT_THREAD_FINISHED,
                        self.OnFilLogVtThreadFinished)
        self.Bind(vidarc.tool.vtThread.vEVT_VT_THREAD_ABORTED,
                        self.OnFilLogVtThreadAborted)
        
        self.bScrollLeft=False
        self.bScrollRight=False
        self.iScroll=-1
        self.zScroll = None#wx.Timer(self)
        self.zUpdate=0
        self.bShowIncomplete=False
        self.bShowModified=True
        #self.bShowIncomplete=True
        
        self.tclsDoc=(vtXmlDom,(),{'audit_trail':False})
        self.tclsDiff=(vtXmlDiff,(),{})
        self.doc=None
        self.MAP_CMD_COMPLETE_TRANS={
            10:_(u'edit started'),
            11:_(u'edit done'),
            12:_(u'edit ended'),
            20:_(u'add node'),
            30:_(u'delete node'),
            99:_(u'???'),
            }
        self.LST_CMD_COMPLETED=[11,12,20,30,99]
        self.MAP_AUDIT_TO_CMD_COMPLETE={
            'startEdit':10,'doEdit':11,'endEdit':12,
            'addNode':20,
            'delNode':30,
            '???':99,
            }
        self.MAP_CMD_COMPLETE={}
        for k,v in self.MAP_AUDIT_TO_CMD_COMPLETE.iteritems():
            self.MAP_CMD_COMPLETE[v]=k
        self.lFindStoreRaw=[]
        #self.Bind(wx.EVT_TIMER, self.OnScroll)
    #def Start(self):
    #    vtInOutFileIdxLinesWid
    def SetDocClass(self,clsDoc,*args,**kwargs):
        self.tclsDoc=(clsDoc,args,kwargs)
    def SetDiffClass(self,clsDiff,*args,**kwargs):
        self.tclsDiff=(clsDiff,args,kwargs)
    def GetNewDoc(self):
        #doc=self.clsDoc(audit_trail=False)
        cls,args,kwargs=self.tclsDoc
        doc=cls(*args,**kwargs)
        return doc
    def __genDoc__(self):
        #self.doc=self.clsDoc(audit_trail=False)
        cls,args,kwargs=self.tclsDoc
        self.doc=cls(*args,**kwargs)
        sAppl=u'.'.join([self.doc.GetAppl(),'vtLogAuditTrailFile'])
        self.doc.SetOrigin(sAppl)
        self.sKeyName=' %s="'%(self.doc.attr)
        #self.oDiff=vtXmlDiff(self.doc)#,verbose=3)
        cls,args,kwargs=self.tclsDiff
        self.oDiff=cls(self.doc,*args,**kwargs)#,verbose=3)
    def GetOrigin(self):
        try:
            return self.doc.GetOrigin()
        except:
            return u'vtLogAuditTrailFile'
    def Open(self,fn):
        if self.doc is None:
            self.__genDoc__()
        self.iLastFakeID=-1
        for v in self.MAP_TAG_FAKE_ID.itervalues():
            if v<self.iLastFakeID:
                self.iLastFakeID=v
        self.iLastFakeID-=1
        self.Start()
        iRet=vtInOutFileIdxLinesWid.Open(self,fn)
        if iRet>=0:
            self.lblPos.SetLabel(u'0')
            self.lblCount.SetLabel(u'0')
            del self.dOrigin
            self.dOrigin={}
            self.dCount=self._getOriginEmptyDict()
        return iRet
    def __runThread__(self):
        vtInOutFileIdxLinesWid.__runThread__(self)
        self.CallBack(self.thrProc.Start)
    def _scroll(self):
        vtInOutFileIdxLinesWid._scroll(self)
        self.CallBack(self.lblPos.SetLabel,unicode(self.iActScPos))
    def _scrollFull(self,bSel=False):
        vtInOutFileIdxLinesWid._scrollFull(self,bSel=bSel)
        self.CallBack(self.lblPos.SetLabel,unicode(self.iActScPos))
    def Stop(self):
        vtInOutFileIdxLinesWid.Stop(self)
        self.CallBack(self.thrProc.Stop)
    def _is2Update(self):
        zNow=time.clock()
        if zNow>(self.zUpdate+self.TIME_UPDATE_INTERVAL):
            self.zUpdate=zNow
            return True
        return False
    def SetShowIncomplete(self,bFlag,bUpdate=False):
        if self.bShowIncomplete!=bFlag:
            self.bShowIncomplete=bFlag
            if bUpdate:
                self.Index(bForce=True)
                self._scrollFull()
    def IsShowIncomplete(self):
        return self.bShowIncomplete
    def SetShowModified(self,bFlag,bUpdate=False):
        if self.bShowModified!=bFlag:
            self.bShowModified=bFlag
            if bUpdate:
                self.Index(bForce=True)
                self._scrollFull()
    def IsShowModified(self):
        return self.bShowModified
    def OnLeftAuto(self,evt):
        evt.Skip()
        iVal=self.tgLeft.GetValue()
        if iVal:
            self.tgRight.SetValue(False)
            if self.zScroll is not None:
                self.zScroll.Stop()
                self.zScroll=None
            self.zScroll=wx.FutureCall(self.TIME_DELAY_START,
                        self.OnScrollTimeOut,self.TIME_DELAY_START,-1)
        else:
            self.zScroll.Stop()
            self.zScroll=None
    def OnRightAuto(self,evt):
        evt.Skip()
        iVal=self.tgRight.GetValue()
        if iVal:
            self.tgLeft.SetValue(False)
            if self.zScroll is not None:
                self.zScroll.Stop()
                self.zScroll=None
            self.zScroll=wx.FutureCall(self.TIME_DELAY_START,
                        self.OnScrollTimeOut,self.TIME_DELAY_START,1)
        else:
            self.zScroll.Stop()
            self.zScroll=None
    def OnScrollStartLeft(self,evt):
        self.zScroll=wx.FutureCall(self.TIME_DELAY_START,
                self.OnScrollTimeOut,self.TIME_DELAY_START,-1)
        evt.Skip()
    def OnScrollStartRight(self,evt):
        self.zScroll=wx.FutureCall(self.TIME_DELAY_START,
                self.OnScrollTimeOut,self.TIME_DELAY_START,1)
        evt.Skip()
    def OnScrollStop(self,evt):
        self.zScroll.Stop()
        evt.Skip()
    def OnScrollTimeOut(self,zDly,iCount):
        if iCount<0:
            self.OnLeft(None)
            iCount-=1
        else:
            self.OnRight(None)
            iCount+=1
        if (iCount%self.TIME_DELAY_DEC_COUNT)==0:
            zDly-=self.TIME_DELAY_DELTA
            if zDly<self.TIME_DELAY_FIN:
                zDly=self.TIME_DELAY_FIN
        self.zScroll.Restart(zDly,zDly,iCount)
    def OnCbStopButton(self,evt):
        evt.Skip()
        self.Stop()
    def OnFindStart(self,evt):
        self.zScroll=wx.FutureCall(self.TIME_DELAY_START,
                self.OnFindTimeOut,self.TIME_DELAY_START)
        evt.Skip()
    def OnFindStop(self,evt):
        self.zScroll.Stop()
        evt.Skip()
    def OnFindTimeOut(self,zDly):
        self.OnFind(None)
        self.zScroll.Restart(zDly,zDly)
    def _getOriginEmptyDict(self):
        return {
                'addNode'  :0,
                'delNode'  :0,
                'editNode' :0,
                'startEdit':0,
                'doEdit'   :0,
                'endEdit'  :0,
                'AUDIT_CMD':0,
                'AUDIT_OLD':0,
                'AUDIT_OPK':0,
                'AUDIT_NEW':0,
                'AUDIT_NPK':0,
                'AUDIT_SEC':0,
                'AUDIT_IDX':0,
                'DEBUG   ':0,
                'INFO    ':0,
                'WARNING ':0,
                'WARN    ':0,
                'ERROR   ':0,
                'CRITICAL':0,
                'FATAL   ':0,
                }
    def _clr(self):
        vtInOutFileIdxLinesWid._clr(self)
        #self.dCount=self._getOriginEmptyDict()
        #del self.dOrigin
        #self.dOrigin={}
        for v in self.dOrigin.itervalues():
            v[0]=None
            v[2]=self._getOriginEmptyDict()
        self.lAuditPos=[]
        self.lAuditIncompletePos=[]
        self.iAuditCount=0
        self.iAuditLastIdx=-1
        self.iAuditLastLv=-1
        self.lAuditCurPos=None
        self.sLastCmd=None
        self.iIdxCount=0
        if self.popWin is not None:
            self.popWin._clr()
        self.CallBack(self.lblPos.SetLabel,u'-1')
        self.CallBack(self.lblCount.SetLabel,u'-1')
        if self.VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'dOrigin:%s'%(vtLog.pformat(self.dOrigin)),
                            self.GetOrigin())
    def _Index(self,bForce=False):
        vtInOutFileIdxLinesWid._Index(self,bForce=bForce)
        def isValid(it):
            t=type(it)
            if t==types.ListType:
                if it[self.OFS_CMD]>=0:
                    if it[self.OFS_OLD]>=0:
                        if it[self.OFS_NEW]>=0:
                            return True
                return False
            elif t==types.LongType:
                return True
            return False
        #l=[it for it in self.lAuditPos if isValid(it)]
        #self.lAuditPos=l
        self.iIdxCount=self.iCount
        self.iCount=len(self.lAuditPos)#-1
    def _IndexFinalize(self):
        tOld=(self.iCount,self.iAuditCount)
        try:
            if self.bShowModified==True:
                for it in self.lAuditPos:
                    t=type(it)
                    if t==types.ListType:
                        try:
                            lAT=self.GetValue(it)
                            if lAT[1] is not None:
                                t1=self.__getLogSep__(lAT[1])
                                if lAT[2] is not None:
                                    t2=self.__getLogSep__(lAT[2])
                                    if lAT[1][t1[-1]:]==lAT[2][t2[-1]:]:
                                        it[self.OFS_CMD_COMPLETE]=-1
                        except:
                            pass
            if self.bShowIncomplete==False:
                l=[]
                for it in self.lAuditPos:
                    t=type(it)
                    if t==types.IntType or t==types.LongType:
                        l.append(it)
                    else:
                        if it[self.OFS_CMD_COMPLETE] in self.LST_CMD_COMPLETED:
                            l.append(it)
                self.lAuditPos=l
                self.iAuditCount=len(l)
                self.iCount=self.iAuditCount
            elif self.bShowModified==True:
                l=[]
                for it in self.lAuditPos:
                    t=type(it)
                    if t==types.IntType or t==types.LongType:
                        l.append(it)
                    else:
                        if it[self.OFS_CMD_COMPLETE]!=-1:
                            l.append(it)
                self.lAuditPos=l
                self.iAuditCount=len(l)
                self.iCount=self.iAuditCount
                
        except:
            vtLog.vtLngTB(self.GetOrigin())
        tNew=(self.iCount,self.iAuditCount)
        if self.VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'dOrigin:%s'%(vtLog.pformat(self.dOrigin)),
                            self.GetOrigin())
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'old:%s;new:%s'%(vtLog.pformat(tOld),vtLog.pformat(tNew)),self.GetOrigin())
        vtInOutFileIdxLinesWid._IndexFinalize(self)
        
    def Get(self,i):
        return self.lAuditPos[i]
    def GetLst(self):
        return self.lAuditPos
    def __iter__(self):
        it=self.lAuditPos[self.idx]
        t=type(it)
        #print 'iter',it,t
        if t==types.IntType or t==types.LongType:
            idx=it
        else:
            idx=it[self.OFS_CMD]
        self.fIn.seek(self.lIdxPos[idx])
        return self
    def next(self):
        self.idx+=1
        if self.idxEnd>0:
            if self.idx>=self.idxEnd:
                raise StopIteration
        if self.idx>=self.iCount:
            raise StopIteration 
        else:
            it=self.lAuditPos[self.idx]
            return it
    def GetValue(self,it):
        try:
            self._acquire()
            t=type(it)
            if t==types.IntType or t==types.LongType:
                idx=it
                self.fIn.seek(self.lIdxPos[idx])
                return [self.fIn.read(self.lIdxSz[idx])]
            l=[]
            for ofs in [self.OFS_CMD,self.OFS_OLD,self.OFS_NEW]:
                idx=it[ofs]
                if idx>=0:
                    self.fIn.seek(self.lIdxPos[idx])
                    l.append(self.fIn.read(self.lIdxSz[idx]))
                else:
                    l.append(None)
            iCmd=it[self.OFS_CMD_COMPLETE]
            if iCmd==99:
                l.append(self.MAP_CMD_COMPLETE[iCmd])
                l.append(it[-1])
                l.append(it[self.OFS_ID])
                l.append(it[self.OFS_TAG])
            elif iCmd>=0:
                l.append(self.MAP_CMD_COMPLETE[iCmd])
                l.append(self.MAP_CMD_COMPLETE_TRANS[iCmd])
                l.append(it[self.OFS_ID])
                l.append(it[self.OFS_TAG])
            else:
                l.append(None)
                l.append(None)
                l.append(None)
                l.append(None)
            return l
        finally:
            self._release()
    def GetData(self,i):
        try:
            self._acquire()
            if self.fIn is None:
                return None
            it=self.lAuditPos[i]
            t=type(it)
            if t==types.IntType or t==types.LongType:
                idx=it
                return idx
                self.fIn.seek(self.lIdxPos[idx])
                return [self.fIn.read(self.lIdxSz[idx])]
            else:
                return it
        finally:
            self._release()
    def _notify(self,pos,size):
        wx.PostEvent(self,vtLogAuditTrailFileProc(self,pos,size))
    def _findAuditLst(self,iOfs,iMatch=-1):
        if self.VERBOSE>1:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'iOfs:%d;iMatch:%d;lAuditPos:%s'%(iOfs,iMatch,
                        vtLog.pformat(self.lAuditPos)),self.GetOrigin())
        for i in xrange(self.iAuditCount-1,-1,-1):
            l=self.lAuditPos[i]
            if type(l)==types.ListType:
                if l[iOfs]==iMatch:
                    return l,i
        return None,-1
    def _findAuditIncompleteLst(self,iOfs,iMatch=-1):
        if self.VERBOSE>1:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'iOfs:%d;iMatch:%d;lAuditIncompletePos:%s'%(iOfs,iMatch,
                        vtLog.pformat(self.lAuditIncompletePos)),self.GetOrigin())
        for i in xrange(self.iAuditIncompleteCount-1,-1,-1):
            l=self.lAuditIncompletePos[i]
            if type(l)==types.ListType:
                if l[iOfs]==iMatch:
                    return l,i
        return None,-1
    def __getId__(self,sTag,sId):
        try:
            return long(sId)
        except:
            if sTag in self.MAP_TAG_FAKE_ID:
                return self.MAP_TAG_FAKE_ID[sTag]
            vtLog.vtLngCur(vtLog.ERROR,'no fake id for;tag:%s'%(sTag),self.GetOrigin())
            self.MAP_TAG_FAKE_ID[sTag]=self.iLastFakeID
            self.iLastFakeID-=1
            return self.MAP_TAG_FAKE_ID[sTag]
            raise
            return -1
    def __convDataGetKeyNum__(self,s):
        try:
            i=s.find('3e')
            if i>0:
                sVal=binascii.unhexlify(s[:i])
            else:
                sVal=binascii.unhexlify(s.strip())
            if self.VERBOSE>2:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'val:%s'%(sVal),self.GetOrigin())
            i=sVal.find(' ')
            if i>0:
                sTagName=sVal[1:i]
            else:
                sTagName=''
            i=sVal.find(self.sKeyName)
            if i>0:
                i+=len(self.sKeyName)
                j=sVal.find('"',i)
                #print sVal[i:j]
                return sTagName,self.__getId__(sTagName,sVal[i:j])
        except:
            if self.VERBOSE>=0:
                vtLog.vtLngTB(self.GetOrigin())
                vtLog.vtLngCur(vtLog.DEBUG,s,self.GetOrigin())
            try:
                doc,n,id=self.__convDataNode__(s)
                doc=self.doc
                return doc.getTagName(n),id
            except:
                pass
            return '',-2
        
        doc,n,id=self.__convDataNode__(s)
        doc=self.doc
        return doc.getTagName(n),id
        #except:
        #    return '',-2
    def __convDataNode__(self,s):
        try:
            sVal=binascii.unhexlify(s.strip())
            if self.VERBOSE>1:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'val:%s'%(sVal),self.GetOrigin())
            doc,n=self.doc.getNode2Add(sVal)
            #id=long(self.doc.getAttribute(n,self.doc.attr))
            id=self.__getId__(self.doc.getTagName(n),self.doc.getAttribute(n,self.doc.attr))
            if self.VERBOSE>2:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'id:%d'%(id),self.GetOrigin())
            return self.doc,n,id
        except:
            if self.VERBOSE>=0:
                vtLog.vtLngTB(self.GetOrigin())
                try:
                    vtLog.vtLngCur(vtLog.ERROR,sVal,self.GetOrigin())
                except:
                    vtLog.vtLngCur(vtLog.ERROR,s,self.GetOrigin())
            return None,None,-2
    def ConvDataNode(self,sVal):
        try:
            #print sVal
            doc,n=self.doc.getNode2Add(sVal)
            #id=long(self.doc.getAttribute(n,self.doc.attr))
            id=self.__getId__(self.doc.getTagName(n),self.doc.getAttribute(n,self.doc.attr))
            if self.VERBOSE>1:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'id:%d;val:%s'%(id,sVal),self.GetOrigin())
            return self.doc,n,id
        except:
            if self.VERBOSE>=0:
                vtLog.vtLngTB(self.GetOrigin())
                vtLog.vtLngCur(vtLog.ERROR,'val:%s'%(sVal),self.GetOrigin())
            return None,None,-2
    def __getLogSep__(self,line):
        i=line.find('|',24)
        if i>0:
            j=line.find('|',i+1)
            if j>0:
                return 24,i+1,j+1
        return 24,-1,-1
    def GetNodeInfoByAuditTrail(self,lAuditTrail):
        if len(lAuditTrail)>=7:
            return lAuditTrail[5],lAuditTrail[6]
        try:
            if lAuditTrail[2] is not None:
                it=lAuditTrail[2]
            elif lAuditTrail[1] is not None:
                it=lAuditTrail[1]
            tIdx=self.__getLogSep__(it)
            sTagName,id=self.__convDataGetKeyNum__(it[tIdx[2]:])
            return id,sTagName
        except:
            pass
        return -1,None  #lAuditTrail[0][0]
    def GetNodeInfoStrByAuditTrail(self,lAuditTrail):
        id,sTagName=self.GetNodeInfoByAuditTrail(lAuditTrail)
        if sTagName is None:
            return u''
        return u'id:%08d node:%s'%(id,sTagName)
    def GetOriginByAuditTrail(self,lAuditTrail,sLine=None):
        #if len(lAuditTrail)>=7:
        #    return lAuditTrail[5],lAuditTrail[6]
        try:
            if sLine is not None:
                it=sLine
            else:
                #vtLog.CallStack('')
                it=lAuditTrail[0]
            #print it,
            tIdx=self.__getLogSep__(it)
            #print tIdx
            s=it[tIdx[0]:tIdx[1]-1]
            #print s
            if s.find('.')>=0:
                #vtLog.CallStack('')
                #print s
                return s
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'lAuditTrail:%s'%(vtLog.pformat(lAuditTrail)),self.GetOrigin())
            
        id,sTagName=self.GetNodeInfoByAuditTrail(lAuditTrail)
        if sTagName is None:
            it=lAuditTrail[0][0]
            tIdx=self.__getLogSep__(it)
            return it[tIdx[1]:tIdx[2]-1]
        return u'.'.join([sTagName,u'%08d'%(id)])
        return u'id:%08d node:%s'%(id,sTagName)
    def GetXmlDiffResByStr(self,sX,sY):
        self.oDiff.Clear()
        if len(sX):
            docX,nX,idX=self.ConvDataNode(sX)
            self.oDiff.AddInfo(nX,False,doc=docX)
        else:
            docX,nX,idX=None,None,-1
            self.oDiff.AddInfo(nX,False,doc=docX)
        if len(sY):
            docY,nY,idY=self.ConvDataNode(sY)
            self.oDiff.AddInfo(nY,False,doc=docY)
        else:
            docY,nY,idY=None,None,-1
            self.oDiff.AddInfo(nY,False,doc=docY)
        self.oDiff.CalcDiff()
        d=self.oDiff.GetDiff()
        return d,self.doc,(docX,nX,idX),(docY,nY,idY)
    def __addOrigin__(self,sOrigin,sLevel):
        if sOrigin not in self.dOrigin:
            self.dOrigin[sOrigin]=[None,True,self._getOriginEmptyDict()]
        dCount=self.dOrigin[sOrigin][2]
        if sLevel in self.dCount:
            self.dCount[sLevel]=self.dCount[sLevel]+1
        else:
            self.dCount[sLevel]=1
        if sLevel in dCount:
            dCount[sLevel]=dCount[sLevel]+1
        else:
            dCount[sLevel]=1
    def __isOrigin2Add__(self,sOrigin):
        if sOrigin in self.dOrigin:
            if self.VERBOSE>0:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'sOrigin:%s found;val:%s'%(sOrigin,self.dOrigin[sOrigin][1]),self.GetOrigin())
            return self.dOrigin[sOrigin][1]
        if self.VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'sOrigin:%s;fallback=>true'%(sOrigin),self.GetOrigin())
        return True
    def __isOrigin2Skip__(self,sOrigin):
        return not self.__isOrigin2Add__(sOrigin)
    def _analyse(self,line):
        try:
            vtLdBd.check()
            line=line.decode('UTF-8')
            iStartLv=line.find(u'|',24)
            if iStartLv>512:
                return
            iEndLv=line.find(u'|',iStartLv+1)
            sOrigin=line[24:iStartLv]
            sLevel=line[iStartLv+1:iEndLv]
            
            if sLevel in vtLAT._LOGNAMES_DICT:
                iAuditLv=vtLAT._LOGNAMES_DICT[sLevel]
            else:
                iAuditLv=-1
            #print s,iAuditLv
            if self.VERBOSE>0:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'%s;iAuditLv:%d'%(line[:iEndLv+15],iAuditLv),self.GetOrigin())
            if iAuditLv==vtLAT.LV_AUDIT_CMD:
                if self.lAuditCurPos is not None:
                    self.lAuditPos.append(self.lAuditCurPos)
                    self.iAuditCount+=1
                self.lAuditCurPos=[self.iIdxCount,-1,-1,-1,-1,-1,None]
                self.sLastCmd=line[iEndLv+1:]
                self.iAuditLastIdx=-2
                #self.lAuditPos.append(self.lAuditCurPos)
                #self.iAuditCount+=1
                if self.sLastCmd.startswith('startEdit'):
                    self.lAuditCurPos[self.OFS_CMD_COMPLETE]=self.MAP_AUDIT_TO_CMD_COMPLETE['startEdit']
                    #self.lAuditPos.append(self.lAuditCurPos)
            elif iAuditLv==vtLAT.LV_AUDIT_IDX:
                try:
                    self.iAuditLastIdx=long(line[iEndLv+1:])
                    if self.iAuditLastLv==vtLAT.LV_AUDIT_OLD or self.iAuditLastLv==vtLAT.LV_AUDIT_OPK:
                        self.lAuditCurPos[self.OFS_IDX]=self.iAuditLastIdx
                        self.lAuditPos.append(self.lAuditCurPos)
                        self.iAuditCount+=1
                        self.lAuditCurPos=None
                        #l,iPos=self._findAuditLst(self.OFS_IDX)
                        #if l is not None:
                        #    l[self.OFS_IDX]=self.iAuditLastIdx
                except:
                    vtLog.vtLngTB(self.GetOrigin())
            elif iAuditLv==vtLAT.LV_AUDIT_OLD:
                self.lAuditCurPos[self.OFS_OLD]=self.iIdxCount
                if self.sLastCmd.startswith('startEdit'):
                    #doc,n,id=self.__convDataNode__(line[iEndLv+1:])
                    #if n is not None:
                    sTagName,id=self.__convDataGetKeyNum__(line[iEndLv+1:])
                    if vtLog.vtLngIsLogged(vtLog.WARN):
                        vtLog.vtLngCur(vtLog.WARN,'zTm:%s;origin:%s;level:%s'%(line[:iStartLv],
                                    sOrigin,sLevel),self.GetOrigin())
                    if id>=0:
                        self.lAuditCurPos[self.OFS_ID]=id
                        self.lAuditCurPos[self.OFS_TAG]=sTagName
                    if self.VERBOSE>0:
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCur(vtLog.DEBUG,'id:%d;id:%s;lAuditCurPos:%s;lAuditPos:%s'%(id,
                                    self.iAuditLastIdx,vtLog.pformat(self.lAuditCurPos),vtLog.pformat(self.lAuditPos)),self.GetOrigin())
                if self.iAuditLastIdx==-1 and 0:
                        # old style parse xml to find id
                        try:
                            sVal=binascii.unhexlify(line[iEndLv+1:].strip())
                            doc,n=self.doc.getNode2Add(sVal)
                            id=long(self.doc.getAttribute(n,self.doc.attr))
                            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                vtLog.vtLngCur(vtLog.DEBUG,'id:%d;id:%s;val:%s'%(id,self.iAuditLastIdx,sVal),self.GetOrigin())
                            #self.iAuditLastIdx=id
                            self.lAuditCurPos[self.OFS_ID]=id
                        except:
                            vtLog.vtLngTB(self.GetOrigin())
                            pass
                #l,iPos=self._findAuditLst(self.OFS_OLD)
                #if l is not None:
                #    l[self.OFS_OLD]=self.iIdxCount
            elif iAuditLv==vtLAT.LV_AUDIT_NEW:
                sTagName,id=self.__convDataGetKeyNum__(line[iEndLv+1:])
                if vtLog.vtLngIsLogged(vtLog.WARN):
                    vtLog.vtLngCur(vtLog.WARN,'zTm:%s;origin:%s;level:%s'%(line[:iStartLv],
                                    sOrigin,sLevel),self.GetOrigin())
                if self.iAuditLastLv==vtLAT.LV_AUDIT_OLD:
                    if id!=-1:
                        l=self.lAuditCurPos
                        l[self.OFS_NEW]=self.iIdxCount
                        l[self.OFS_ID]=id
                        l[self.OFS_TAG]=sTagName
                        l[self.OFS_CMD_COMPLETE]=self.MAP_AUDIT_TO_CMD_COMPLETE['???']
                        l.append(self.sLastCmd.strip())
                        sOrigin=self.GetOriginByAuditTrail(l,line)
                        self.__addOrigin__(sOrigin,
                                    self.MAP_CMD_COMPLETE[l[self.OFS_CMD_COMPLETE]])
                    self.lAuditCurPos[self.OFS_NEW]=self.iIdxCount
                    self.lAuditPos.append(self.lAuditCurPos)
                    self.iAuditCount+=1
                    self.lAuditCurPos=None
                    if self.VERBOSE>0:
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCur(vtLog.DEBUG,'id:%d;id:%s;lAuditCurPos:%s;lAuditPos:%s'%(id,
                                    self.iAuditLastIdx,vtLog.pformat(self.lAuditCurPos),vtLog.pformat(self.lAuditPos)),self.GetOrigin())
                else:
                    #l=self.lAuditCurPos
                    #self.lAuditCurPos=None
                    if self.VERBOSE>0:
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCur(vtLog.DEBUG,'id:%d;id:%s;lAuditCurPos:%s;lAuditPos:%s'%(id,
                                    self.iAuditLastIdx,vtLog.pformat(self.lAuditCurPos),vtLog.pformat(self.lAuditPos)),self.GetOrigin())
                    if self.sLastCmd.startswith('endEdit'):
                        self.lAuditCurPos=None
                        #sTagName,id=self.__convDataGetKeyNum__(line[iEndLv+1:])
                        if self.iAuditLastIdx>=0:
                            l,iPos=self._findAuditLst(self.OFS_IDX,self.iAuditLastIdx)
                        else:
                            l,iPos=self._findAuditLst(self.OFS_ID,id)
                        if self.VERBOSE>0:
                            vtLog.vtLngCur(vtLog.DEBUG,'id:%d;id:%s;l:%s'%(id,
                                    self.iAuditLastIdx,vtLog.pformat(l)),self.GetOrigin())
                        if l is not None:
                            if l[self.OFS_CMD_COMPLETE]<20:
                                #l[self.OFS_END]=self.iIdxCount
                                l[self.OFS_NEW]=self.iIdxCount
                                l[self.OFS_CMD_COMPLETE]=self.MAP_AUDIT_TO_CMD_COMPLETE['endEdit']
                                sOrigin=self.GetOriginByAuditTrail(l,line)
                                self.__addOrigin__(sOrigin,
                                        self.MAP_CMD_COMPLETE[l[self.OFS_CMD_COMPLETE]])
                                if self.__isOrigin2Skip__(sOrigin):
                                    l[self.OFS_CMD_COMPLETE]=-1
                        if self.VERBOSE>0:
                            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                vtLog.vtLngCur(vtLog.DEBUG,'cmd:%s;lastIdx:%d;l:%s;iPos:%d;id:%d'%(self.sLastCmd,
                                            self.iAuditLastIdx,vtLog.pformat(l),iPos,id),self.GetOrigin())
                    elif self.sLastCmd.startswith('doEdit'):
                        #doc,n,id=self.__convDataNode__(line[iEndLv+1:])
                        #if self.iAuditLastIdx>=0:
                        self.lAuditCurPos=None
                        #sTagName,id=self.__convDataGetKeyNum__(line[iEndLv+1:])
                        #if id>=0:
                        if self.iAuditLastIdx>=0:
                            l,iPos=self._findAuditLst(self.OFS_IDX,self.iAuditLastIdx)
                        else:
                            l,iPos=self._findAuditLst(self.OFS_ID,id)
                        if self.VERBOSE>0:
                            vtLog.vtLngCur(vtLog.DEBUG,'id:%d;id:%s;l:%s'%(id,
                                    self.iAuditLastIdx,vtLog.pformat(l)),self.GetOrigin())
                        if l is not None:
                            if l[self.OFS_CMD_COMPLETE]<20:
                                l[self.OFS_NEW]=self.iIdxCount
                                l[self.OFS_CMD_COMPLETE]=self.MAP_AUDIT_TO_CMD_COMPLETE['doEdit']
                                sOrigin=self.GetOriginByAuditTrail(l,line)
                                self.__addOrigin__(sOrigin,
                                        self.MAP_CMD_COMPLETE[l[self.OFS_CMD_COMPLETE]])
                                if self.__isOrigin2Skip__(sOrigin):
                                    l[self.OFS_CMD_COMPLETE]=-1
                                #if self.__isOrigin2Skip__(sOrigin)==True:
                                #    l[self.OFS_CMD_COMPLETE]
                        if self.VERBOSE>0:
                            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                vtLog.vtLngCur(vtLog.DEBUG,'cmd:%s;lastIdx:%d;l:%s;iPos:%d;id:%d'%(self.sLastCmd,
                                            self.iAuditLastIdx,vtLog.pformat(l),iPos,id),self.GetOrigin())
                    elif self.sLastCmd.startswith('addNode'):
                        #doc,n,id=self.__convDataNode__(line[iEndLv+1:])
                        #sTagName,id=self.__convDataGetKeyNum__(line[iEndLv+1:])
                        #if self.iAuditLastIdx>=0:
                        #    l,iPos=self._findAuditLst(self.OFS_IDX,self.iAuditLastIdx)
                        #else:
                        #    l,iPos=self._findAuditLst(self.OFS_ID,id)
                        #l=None
                        iPos=0
                        #if l is None:
                        #    l=self.lAuditCurPos
                        l=self.lAuditCurPos
                        if l is not None:
                            l[self.OFS_NEW]=self.iIdxCount
                            l[self.OFS_ID]=id
                            l[self.OFS_TAG]=sTagName
                            l[self.OFS_CMD_COMPLETE]=self.MAP_AUDIT_TO_CMD_COMPLETE['addNode']
                            sOrigin=self.GetOriginByAuditTrail(l,line)
                            self.__addOrigin__(sOrigin,
                                    self.MAP_CMD_COMPLETE[l[self.OFS_CMD_COMPLETE]])
                            if self.VERBOSE>0:
                                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                    vtLog.vtLngCur(vtLog.DEBUG,'cmd:%s;lastIdx:%d;l:%s;iPos:%d;id:%d'%(self.sLastCmd,
                                                self.iAuditLastIdx,vtLog.pformat(l),iPos,id),self.GetOrigin())
                            if self.__isOrigin2Add__(sOrigin):
                                self.lAuditPos.append(l)
                                self.iAuditCount+=1
                        self.lAuditCurPos=None
                    elif self.sLastCmd.startswith('delNode'):
                        #doc,n,id=self.__convDataNode__(line[iEndLv+1:])
                        #sTagName,id=self.__convDataGetKeyNum__(line[iEndLv+1:])
                        #if self.iAuditLastIdx>=0:
                        #    l,iPos=self._findAuditLst(self.OFS_IDX,self.iAuditLastIdx)
                        #else:
                        #    l,iPos=self._findAuditLst(self.OFS_ID,id)
                        #l=None
                        iPos=0
                        #if l is None:
                        #    l=self.lAuditCurPos
                        l=self.lAuditCurPos
                        if l is not None:
                            
                            l[self.OFS_NEW]=self.iIdxCount
                            l[self.OFS_ID]=id
                            l[self.OFS_TAG]=sTagName
                            l[self.OFS_CMD_COMPLETE]=self.MAP_AUDIT_TO_CMD_COMPLETE['delNode']
                            sOrigin=self.GetOriginByAuditTrail(l,line)
                            self.__addOrigin__(sOrigin,
                                    self.MAP_CMD_COMPLETE[l[self.OFS_CMD_COMPLETE]])
                            if self.VERBOSE>0:
                                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                    vtLog.vtLngCur(vtLog.DEBUG,'cmd:%s;lastIdx:%d;l:%s;iPos:%d;id:%d'%(self.sLastCmd,
                                                self.iAuditLastIdx,vtLog.pformat(l),iPos,id),self.GetOrigin())
                            if self.__isOrigin2Add__(sOrigin):
                                self.lAuditPos.append(l)
                                self.iAuditCount+=1
                        self.lAuditCurPos=None
                    else:
                        if self.iAuditLastIdx>=0:
                            l,iPos=self._findAuditLst(self.OFS_IDX,self.iAuditLastIdx)
                            self.lAuditCurPos=None
                        else:
                            l,iPos=self._findAuditLst(self.OFS_ID,id)
                        if self.VERBOSE>0:
                            vtLog.vtLngCur(vtLog.DEBUG,'id:%d;id:%s;l:%s'%(id,
                                    self.iAuditLastIdx,vtLog.pformat(l)),self.GetOrigin())
                        if l is None:
                            l=self.lAuditCurPos
                        if l is not None:
                            l[self.OFS_NEW]=self.iIdxCount
                            l[self.OFS_ID]=id
                            l[self.OFS_TAG]=sTagName
                            l[self.OFS_CMD_COMPLETE]=self.MAP_AUDIT_TO_CMD_COMPLETE['???']
                            l.append(self.sLastCmd.strip())
                            sOrigin=self.GetOriginByAuditTrail(l,line)
                            self.__addOrigin__(sOrigin,
                                    self.MAP_CMD_COMPLETE[l[self.OFS_CMD_COMPLETE]])
                        if self.VERBOSE>0:
                            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                vtLog.vtLngCur(vtLog.DEBUG,'cmd:%s;lastIdx:%d;l:%s;iPos:%d;id:%d'%(self.sLastCmd,
                                            self.iAuditLastIdx,vtLog.pformat(l),iPos,id),self.GetOrigin())
                        #if self.__isOrigin2Add__(self.GetOriginByAuditTrail(l)):
                        #    self.lAuditPos.append(l)
                        #    self.iAuditCount+=1
                        #self.lAuditCurPos=None
                        
                        if 0:
                            l=self.lAuditCurPos
                            if self.iAuditLastIdx>=0:
                                l,iPos=self._findAuditLst(self.OFS_IDX,self.iAuditLastIdx)
                                if self.VERBOSE>0:
                                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                        vtLog.vtLngCur(vtLog.DEBUG,'cmd:%s;lastIdx:%d;l:%s;iPos:%d;id:%d'%(self.sLastCmd,
                                                    self.iAuditLastIdx,vtLog.pformat(l),iPos,id),self.GetOrigin())
                                if l is not None:
                                    l[self.OFS_NEW]=self.iIdxCount
            elif iAuditLv==vtLAT.LV_AUDIT_SEC:
                self.__addOrigin__(sOrigin,sLevel)
                self.lAuditPos.append(self.iIdxCount)
                self.iAuditCount+=1
            elif iAuditLv==-1:
                self.__addOrigin__(sOrigin,sLevel)
                if self.__isOrigin2Add__(sOrigin):
                    self.lAuditPos.append(self.iIdxCount)
                    self.iAuditCount+=1
            elif iAuditLv==vtLAT.LV_AUDIT_OPK:
                self.lAuditCurPos[self.OFS_OLD]=self.iIdxCount
            elif iAuditLv==vtLAT.LV_AUDIT_NPK:
                if self.iAuditLastLv==vtLAT.LV_AUDIT_OPK:
                    self.lAuditCurPos[self.OFS_NEW]=self.iIdxCount
                    self.lAuditPos.append(self.lAuditCurPos)
                    self.iAuditCount+=1
                    self.lAuditCurPos=None
                else:
                    self.lAuditCurPos=None
                    l,iPos=self._findAuditLst(self.OFS_IDX,self.iAuditLastIdx)
                    #print 'new',self.iAuditLastIdx,l,iPos
                    if l is not None:
                        l[self.OFS_NEW]=self.iIdxCount
            self.iAuditLastLv=iAuditLv
            
            #print '+++++ audit'
            #print 'lv:%8d count:%8d idx:%8d'%(self.iAuditLastLv,
            #            self.iAuditCount,self.iAuditLastIdx)
            #print self.lAuditPos
            #print '----- audit'
            
            self.iIdxCount+=1
            if self.VERBOSE>0:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'lAuditPos:%s'%(vtLog.pformat(self.lAuditPos)),self.GetOrigin())
            if self.VERBOSE>1:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'lAuditIncompletePos:%s'%(vtLog.pformat(self.lAuditIncompletePos)),self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
            vtLog.vtLngCur(vtLog.ERROR,'%s'%(line),self.GetOrigin())
            pass
    def GetLvCount(self):
        return self.dCount
    def GetOriginDict(self):
        return self.dOrigin
    def __createPopup__(self):
        if self.popWin is None:
            sz=self.GetSize()
            sz=(sz[0],sz[1]-20)
            self.popWin=vtLogAuditTrailFileTransientPopup(self,sz,wx.SIMPLE_BORDER)
            self.popWin._updateOrigin()
            self.cbFind.Enable(True)
            #self.popWin.SetNode()
            #self.popWin.SetVal(self.txtVal.GetValue(),self.doc.GetLang())
            self.popWin.SetFindStoreLst(self.lFindStoreRaw)
        else:
            pass
    def OnFilLogVtThreadFinished(self, event):
        event.Skip()
        self.thrProc.Stop()
        if self.popWin is not None:
            self.popWin._updateOrigin()
        self._notify(0,0)
        self.lblPos.SetLabel(unicode(self.iActScPos))
        self.lblCount.SetLabel(unicode(self.GetCount()))
    def OnFilLogVtThreadAborted(self, event):
        event.Skip()
        self.thrProc.Stop()
        self._notify(0,0)
        self.lblPos.SetLabel(unicode(self.iActScPos))
        self.lblCount.SetLabel(unicode(self.GetCount()))
    def SetMsg2Find(self,s):
        if self.popWin is not None:
            self.popWin.SetMsg2Find(s)
    def SetSel(self,j):
        self.iSel=j
    def OnFind(self, event):
        if event is not None:
            event.Skip()
        if self.popWin is None:
            return
        if self.zScroll is not None:
            self.zScroll.Stop()
            self.zScroll=None
            self.tgLeft.SetValue(False)
            self.tgRight.SetValue(False)
        if self.IsRunning():
            dlg=vtmMsgDialog(self,u'Thread is already busy, try later again?'+u'\n\n'+ \
                            u'YES   ... current search will be continued.'+u'\n'+ \
                            u'NO    ... new search will be started.',
                            'vtLogAuditTrailFile',
                            wx.YES_NO|wx.YES_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.Centre()
            if dlg.ShowModal()==wx.ID_YES:
                
                dlg.Destroy()
                return
            else:
                self.Stop()
                while self.IsRunning():
                    time.sleep(0.05)
                dlg.Destroy()
                
        lOrigin=self.popWin.GetOrigins2Find()
        sLevel=self.popWin.GetLevel2Find()
        sMsg=self.popWin.GetMsg2Find()
        bDirUp=self.popWin.GetDirection2Find()
        bCase=self.popWin.GetCaseSensitive2Find()
        if self.bReverse==False:
            bDirUp=not bDirUp
        self.zUpdate=0
        
        #print 'OnFind',
        #print self.iSel,
        if self.iSel<0:
            iPos=self.slPos.GetValue()
            self.iSel=iPos
        else:
            iPos=self.iSel
        #print iPos
        
        iCount=self.GetCount()
        tup2Find=[lOrigin,sLevel,sMsg]
        bOrigin,bLevel,bMsg=[v is not None for v in tup2Find]
        self.Start()
        self.Do(self._find,iPos,iCount,bCase,bDirUp,bOrigin,bLevel,bMsg,tup2Find)
    def _find(self,iPos,iCount,bCase,bDirUp,bOrigin,bLevel,bMsg,tup2Find):
        try:
            if VERBOSE_FIND>0:
                print '_find pos:%05d count:%05d bCase:%d bDirUp:%d bOrigin:%d bLevel:%d bMsg:%d'%(iPos,
                                        iCount,bCase,bDirUp,bOrigin,bLevel,bMsg)
                print '     ',tup2Find
            lOrigin,sLevel,sMsg=tup2Find
            for i in xrange(1,iCount):
                if self.Is2Stop():
                    break
                if self._is2Update():
                    self._notify(i,iCount)
                vtLdBd.check()
                if bDirUp:
                    j=iPos-i
                    if j<0:
                        j+=iCount
                else:
                    j=iPos+i
                    if j>=iCount:
                        j-=iCount
                #print j,
                try:
                    l=self.GetData(j)
                    if type(l)!=types.ListType:
                        continue
                except:
                    vtLog.vtLngTB(self.GetOrigin())
                    continue
                if len(l)<self.OFS_TAG:
                    if VERBOSE_FIND:
                        print '    invalid format at',j,l
                        break
                    continue
                bFound=False
                lAT=self.GetValue(l)
                if bOrigin:
                    #lO=self.GetOriginByAuditTrail(l).split('.')
                    sOrigin=self.GetOriginByAuditTrail(lAT)
                    iO=sOrigin.rfind(';')
                    if iO>=0:
                        lO=sOrigin[:iO].split('.')#+sOrigin[j+1:].split('.')
                    else:
                        lO=sOrigin.split('.')
                    bOriginFound=False
                    for iO in xrange(len(lO)):
                        if  u'.'.join(lO[:iO+1]) in lOrigin:
                            bOriginFound=True
                            break
                    if bOriginFound==False:
                        continue
                if bLevel:
                    if not l[self.OFS_CMD_COMPLETE].startswith(sLevel):
                        continue
                if bMsg:
                    try:
                        bMsgFound=False
                        for iInfo in xrange(3):
                            lVal=self.GetAuditValue(lAT,iInfo)
                            if lVal is not None:
                                if bCase:
                                    for s in lVal[2].split('\n'):
                                        if fnmatch.fnmatchcase(s,sMsg):
                                            bMsgFound=True
                                            break
                                else:
                                    for s in lVal[2].split('\n'):
                                        if fnmatch.fnmatch(s.lower(),sMsg):
                                            bMsgFound=True
                                            break
                        if bMsgFound==False:
                            continue
                    except:
                        vtLog.vtLngTB(self.GetOrigin())
                        continue
                if VERBOSE_FIND>0:
                    #print
                    print '   found at',j
                    #print j,s,l
                self._notify(0,0)
                self.CallBack(self.__findSel__,j)
                break
            #if VERBOSE_FIND>0:
                #print
                #print '   found'
                #print j,s,l
        except:
            traceback.print_exc()
    def __findSel__(self,j):
        if self.bReverse:
            self.slPos.SetValue(j+1)
        else:
            self.slPos.SetValue(j)
        #self.OnScrollTrack(None)
        self._scrollFull()
        wx.PostEvent(self,vtLogAuditTrailFileSel(self,j))
        self.iSel=j
    def SetFindStoreLst(self,l):
        self.lFindStoreRaw=l
        if self.popWin is not None:
            self.popWin.SetFindStoreLst(self.lFindStoreRaw)
    def GetFindStoreLst(self):
        if self.popWin is not None:
            self.lFindStoreRaw=self.popWin.GetFindStoreLst()
        return self.lFindStoreRaw
    def MoveValue(self,iDelta=1):
        if self.bReverse==False:
            iDelta=-iDelta
        iPos=self.slPos.GetValue()
        iPos=iPos+iDelta
        if iPos<0:
            iPos=0
        if iPos>self.GetCount():
            iPos=self.GetCount()
        self.slPos.SetValue(iPos)
        self._scroll()
    def GetAuditValue(self,lVal,iOfs):
        try:
            if lVal is None:
                return None
            #print lVal
            iLen=len(lVal)
            if iOfs>=iLen:
                return None
            else:
                line=lVal[iOfs]
                if line is None:
                    return None
                sLine=line.decode('UTF-8')
                log=sLine.split(u'|')
                if len(log)<4:
                    return None
                sLv=log[2].strip()
                try:
                    if sLv=='AUDIT_OLD':
                        sVal=binascii.unhexlify(log[3].strip())
                    elif sLv=='AUDIT_OPK':
                        sValPk=binascii.unhexlify(log[3].strip())
                        sVal=cPickle.loads(sValPk)
                    elif sLv=='AUDIT_NEW':
                        sVal=binascii.unhexlify(log[3].strip())
                    elif sLv=='AUDIT_NPK':
                        sValPk=binascii.unhexlify(log[3].strip())
                        sVal=cPickle.loads(sValPk)
                    else:
                        sVal=log[3].strip()
                except:
                    vtLog.vtLngTB(self.GetOrigin())
                    sVal=u''.join([u'!!!',log[3].strip(),u'!!!'])
                return log[1], sLv,sVal,log[0].strip(),
                #self.lstLogs.SetItemData(idx,idxLog)
        except:
            vtLog.vtLngTB(self.GetOrigin())
