#!/usr/bin/env python
#Boa:App:BoaApp
#----------------------------------------------------------------------------
# Name:         vtLogAuditTrailFileViewerAppl.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080127
# CVS-ID:       $Id: vtLogAuditTrailFileViewerAppl.py,v 1.1 2008/01/27 20:58:45 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import os,sys,getopt
import vtLogAuditTrailFileViewerFrame
import vidarc.tool.lang.vtLgBase as vtLgBase

modules ={u'vtLogAuditTrailFileViewerFrame': [1,
                           'Main frame of Application',
                           u'vtLogAuditTrailFileViewerFrame.py']}

class BoaApp(wx.App):
    def OnInit(self):
        #wx.HandleFatalExceptions()
        #wx.Log_SetActiveTarget(wx.LogStderr())
        #wx.Log_SetTraceMask(0|
                #wx.TraceMessages|
                #wx.TraceMemAlloc|
                #wx.TraceResAlloc|
                #wx.TraceRefCount|
        #        0)
        #wx.Log_AddTraceMask(wx.TraceMemAlloc)
        #wx.Log_AddTraceMask(wx.TRACE_MemAlloc)
        #wx.Log_AddTraceMask(wx.TRACE_ResAlloc)
        #wx.Log_AddTraceMask(wx.TRACE_RefCount)
        #wx.Log_AddTraceMask(wx.TRACE_Messages)
        optshort,optlong='l:f:c:h',['lang=','file=','help']
        vtLgBase.initAppl(optshort,optlong,'log')
        wx.InitAllImageHandlers()
        self.main = vtLogAuditTrailFileViewerFrame.create(None)
        self.main.Show()
        self.SetTopWindow(self.main)
        return True
    def OpenFile(self,fn):
        self.main.OpenFile(fn)
    def OpenCfgFile(self,fn):
        self.main.OpenCfgFile(fn)
    def OnFatalException(self):
        print 'fatal'
        #try:
        #    vtLog.vtLngCurWX(vtLog.FATAL,''%(),self.main)
        #except:
        #    vtLog.vtLngCurCls(vtLog.FATAL,''%(),self)
    def OnExceptionInMainLoop(self):
        print 'OnExceptionInMainLoop'
        return False
    def OnUnhandledException(self):
        print 'OnUnhandledException'
    
def showHelp():
    print _('help')
    print sys.version
    print "  ",_("valid flags:")
    print "  ",_("-f <filename>")
    print "  ",_("--file <filename>")
    print "        ",_("open the <filename> at start")
    print "  ","-h"
    print "  ",_("--help")
    print "        ",_("show this screen")
    print "  ","-l"
    print "  ",_("--lang <language id according ISO639>")

def main():
    optshort,optlong='l:f:c:h',['lang=','file=','help']
    #vtLgBase.initAppl(optshort,optlong,'log')
    
    fn=None
    cfgFN='vtLogFileViewer.ini'
    try:
        optlist , args = getopt.getopt(sys.argv[1:],'l:f:c:h',['lang=','file=','config=','help'])
        for o in optlist:
            if o[0] in ['-h','--help']:
                vtLgBase.initAppl(optshort,optlong,'log')
                showHelp()
            
                return
            if o[0] in ['--file','-f']:
                fn=o[1]
            if o[0] in ['--config','-c']:
                cfgFN=o[1]
    except:
        vtLgBase.initAppl(optshort,optlong,'log')
        showHelp()
    if fn is None:
        if len(sys.argv)==2:
            fn=sys.argv[-1]
    application = BoaApp(0)
    if fn is not None:
        application.OpenFile(fn)
    if cfgFN is not None:
        application.OpenCfgFile(cfgFN)
    application.MainLoop()

if __name__ == '__main__':
    main()
