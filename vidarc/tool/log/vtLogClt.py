#----------------------------------------------------------------------------
# Name:         vtLogClt.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtLogClt.py,v 1.4 2007/07/28 14:43:30 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import thread,traceback
import time,random
import socket,sha,binascii

import sys,string
from vidarc.tool.log.vtLogSrv import vtLogSrvSock
#from vidarc.tool.net import vtNetSock
#from vidarc.tool.sock import vtSock
from vidarc.tool.sock.vtSock import vtSock
#from vidarc.tool.xml.vtXmlDom import vtXmlDom

BUFFER_MAX=40*1024*1024

class vtLogCltSock(vtSock):
    def __init__(self,server,conn,adr,sID='',verbose=0):
        vtSock.__init__(self,server,conn,adr)
        self.bEstabilshed=False
        self.loggedin=False
        self.iStart=-1
        self.iAct=-1
        self.iEnd=-1
        self.encoded_passwd=False
        self.verbose=verbose
        self.cmds=[]
        #self.nodes2keep=[]
        #self.nodes2synch=[]
        self.appl_alias=''
        self.appl=''
        self.par=None
    def CheckPause(self):
        try:
            if self.paused!=self.pausing:
                self.paused=self.pausing
                if self.par is not None:
                    self.par.NotifyPause(self.paused)
                else:
                    pass
        except:
            traceback.print_exc()
    def IsLoggedIn(self):
        return self.loggedin
    def HandleData(self):
        if self.iStart==-1:
            self.iStart=string.find(self.data,self.startMarker)
            self.iAct=self.iStart
        if self.iStart>=0:
            # start marker found
            try:
                self.iEnd=string.find(self.data,self.endMarker,self.iAct)
            except:
                pass
            self.iAct=self.len
            if self.iEnd>0:
                # end marker found, telegram is complete
                self.iStart+=self.iStartMarker
                val=self.data[self.iStart:self.iEnd]
                #vtLog.vtLngCallStack(self,vtLog.DEBUG,'  '+val[:60])
                try:
                    msg=val.encode('ISO-8859-1')
                except:
                    msg=val
                self.doMessage(msg)
                self.iEnd+=self.iEndMarker
                self.data=self.data[self.iEnd:]
                self.len-=self.iEnd
                self.iStart=self.iEnd=self.iAct=-1
                return 1
        return 0
    def doMessage(self,msg):
        #overload me
        print msg
        pass
    def SetParent(self,par):
        self.par=par
    def SocketClosed(self):
        if self.par is not None:
            self.par.__connectionClosed__(self.bEstabilshed)
    def Stop(self):
        vtSock.Stop(self)
    def IsRunning(self):
        return vtSock.IsRunning(self)
    def Login(self,usr,passwd):
        self.usr=usr
        self.passwd=passwd
        self.encoded_passwd=False
        self.bLoginSend=False
    def LoginEncoded(self,usr,passwd):
        self.usr=usr
        self.passwd=passwd
        self.encoded_passwd=True
        self.bLoginSend=False
    def __sessionID__(self,val):
        #vtNetXmlSock.__sessionID__(self,val)
        self.sessionID=val[1:]
        #self.__doLogin__()
    def __doLogin__(self):
        if len(self.passwd)>0:
            if self.encoded_passwd:
                sPasswd=self.passwd
            else:
                m=sha.new()
                m.update(self.passwd)
                sPasswd=m.hexdigest()
            m=sha.new()
            m.update(sPasswd)
            m.update(self.sessionID)
            s=binascii.b2a_uu(m.digest())
        else:
            s=''
        self.SendTelegram('login|'+self.usr+','+s)
        self.bLoginSend=True
    def __login__(self,val):
        if self.bLoginSend==False:
            self.__doLogin__()
        else:
            self.par.NotifyLogin()
    def __loggedin__(self,val):
        self.loggedin=True
        self.par.NotifyLoggedIn()
    def ShutDown(self):
        pass
    def Alias(self,appl,alias):
        self.appl=appl
        self.appl_alias=appl+':'+alias
        self.SendTelegram('alias|'+appl+':'+alias)
    
class vtLogClt:
    def __init__(self,par,host,port,socketClass,verbose=0):
        self.par=par
        self.host=host
        self.port=port
        self.serving=False
        self.connecting=False
        self.stopping=False
        self.connected=False
        self.pausing=False
        self.paused=False
        #self.dataSettled=False
        self.sock=None
        self.socketClass=socketClass
        self.connections={}
        self.verbose=verbose
    def GetSocketInstance(self):
        return self.sock
    def Start(self):
        if self.serving:
            return
        if self.stopping:
            return
        self.serving=True
        self.stopping=False
        thread.start_new_thread(self.Run, ())
    def Stop(self):
        #print 'netclt','stop'
        #print '   conn:%d stop:%d serv:%d'%(self.connecting,self.stopping,self.serving)
        #print type(self.sock),self.sock
        self.stopping=True
        while self.connecting:
            time.sleep(1)
        if self.sock is not None:
            self.sock.Stop()
        #self.serving=False
    def Pause(self,flag):
        self.pausing=flag
        if self.sock is not None:
            self.sock.Pause(flag)
    def IsRunning(self):
        return False
    def IsServing(self):
        if self.connecting:
            return True
        if self.sock is not None:
            if self.sock.IsRunning():
                if self.sock.paused:
                    return False
                return True
            #if self.sock.IsThreadRunning():
            #    return True
        else:
            return False
        return self.serving
    def StopSocket(self,adr,socket):
        try:
            lst=self.connections[adr]
            lst.remove(socket)
        except:
            pass
    def Connect(self):
        #self.dataSettled=False
        self.connecting=True
        thread.start_new_thread(self.RunConnect, ())
    def RunConnect(self):
        self.connected=False
        self.socket = None
        try:
            for res in socket.getaddrinfo(self.host, self.port, socket.AF_UNSPEC, socket.SOCK_STREAM):
                af, socktype, proto, canonname, sa = res
                try:
                    self.socket = socket.socket(af, socktype, proto)
                except socket.error, msg:
                    traceback.print_exc()
                    selft.socket = None
                    continue
                try:
                    self.socket.connect(sa)
                except socket.error, msg:
                    self.socket.close()
                    self.socket = None
                    traceback.print_exc()
                    continue
                break
        except:
            # address fault
            pass
        if self.socket is None:
            try:
                if self.par is not None:
                    self.par.NotifyConnectionFault()
                self.connecting=False
                #self.dataSettled=True
                #self.serving=False
            except:
                traceback.print_exc()
            return
        try:
            self.sock=self.socketClass(self,self.socket,None,verbose=self.verbose)
            self.sock.SetParent(self.par)
            #if self.par.bSynch:
            #    self.dataSettled=True
            if self.par is not None:
                self.par.NotifyConnected()
        except:
            traceback.print_exc()
            
        self.connected=True
        self.connecting=False
        #return 0
    def Run(self):
        while self.serving:
            if self.stopping:
                self.serving=False
                #if self.sock.IsThreadRunning():
                #    self.sock.Stop()
                #    time.sleep(1)
                #    continue
                #else:
                break
            self.socket.send(self.data)
            data = self.socket.recv(1024)
            
            self.socket.close()
    def Send(self,str):
        if self.connected:
            self.sock.Send(str)
    def Close(self):
        #self.par.NotifyDisConnected()
        if self.sock is not None:
            self.sock.Close()
