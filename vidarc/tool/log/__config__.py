#----------------------------------------------------------------------------
# Name:         __config__.py
# Purpose:      package configuration.
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: __config__.py,v 1.21 2012/01/29 17:18:14 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#----------------------------------------------------------------------
# flags and values that affect this script
#----------------------------------------------------------------------

VER_MAJOR        = 1      # The first three must match wxWidgets
VER_MINOR        = 3
VER_RELEASE      = 2
VER_SUBREL       = 0      # wxPython release num for x.y.z release of wxWidgets
VER_FLAGS        = ""     # release flags, such as prerelease or RC num, etc.
VERSION          = u'%d.%d.%d'%(VER_MAJOR,VER_MINOR,VER_RELEASE)

LOG_FILE_VIEWER_VER_MAJOR        = 1      # The first three must match wxWidgets
LOG_FILE_VIEWER_VER_MINOR        = 4
LOG_FILE_VIEWER_VER_RELEASE      = 0
LOG_FILE_VIEWER_VER_SUBREL       = 0      # wxPython release num for x.y.z release of wxWidgets

LOG_SOCK_VIEWER_VER_MAJOR        = 1      # The first three must match wxWidgets
LOG_SOCK_VIEWER_VER_MINOR        = 2
LOG_SOCK_VIEWER_VER_RELEASE      = 1
LOG_SOCK_VIEWER_VER_SUBREL       = 0      # wxPython release num for x.y.z release of wxWidgets

LOG_AUDIT_TRAIL_FILE_VIEWER_VER_MAJOR        = 1      # The first three must match wxWidgets
LOG_AUDIT_TRAIL_FILE_VIEWER_VER_MINOR        = 0
LOG_AUDIT_TRAIL_FILE_VIEWER_VER_RELEASE      = 3
LOG_AUDIT_TRAIL_FILE_VIEWER_VER_SUBREL       = 0      # wxPython release num for x.y.z release of wxWidgets

DESCRIPTION      = "VIDARC Tool log"
AUTHOR           = "Walter Obweger"
AUTHOR_EMAIL     = "Walter Obweger <walter.obweger@vidarc.com>"
MAINTAINER       = "Walter Obweger"
MAINTAINER_EMAIL = "Walter Obweger <walter.obweger@vidarc.com>"
URL              = "http://www.vidarc.com/"
DOWNLOAD_URL     = "http://www.vidarc.com/"
LICENSE          = "VIDARC license"
PLATFORMS        = "WIN32,OSX,POSIX"
KEYWORDS         = "library,tool,log"

LONG_DESCRIPTION = """\
vidarc tool log library.
"""

CLASSIFIERS      = """\
Development Status :: 2 - Development
Environment :: Win32 (MS Windows)
Environment :: X11 Applications :: GTK
Intended Audience :: Developers
License :: 
Operating System :: MacOS :: MacOS X
Operating System :: Microsoft :: Windows :: Windows 95/98/2000/XP
Operating System :: POSIX
Programming Language :: Python
Topic :: Software Development :: User Interfaces
"""
INST_PATH="vidarc/tools"
GROUP_BASE="VIDARC\Tools"

APP='vLogFileViewer'
ICONS=[(1,'img/vLogViewer04_32.ico')]
APP_PY='vtLogFileViewerAppl.py'
APP_INCL=['vidarc.tool.log.vtLogFileViewerFrame']
APP_EXCL=[]
LANG=['en','de']

def getAppl():
    return [
        [0,'vLogFileViewer',
            u'%d.%d.%d'%(LOG_FILE_VIEWER_VER_MAJOR,LOG_FILE_VIEWER_VER_MINOR,LOG_FILE_VIEWER_VER_RELEASE),
            'vtLogFileViewerAppl.py',
            ICONS,
            None
        ],
        ]
