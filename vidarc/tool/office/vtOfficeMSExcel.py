#----------------------------------------------------------------------------
# Name:         vtOfficeMSExcel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtOfficeMSExcel.py,v 1.5 2012/12/14 12:43:25 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import vidarc.tool.office.vtOfficeAppl as vtOfficeAppl
#import win32con,win32api,win32ui,win32con,commctrl
#import win32com,traceback,win32com.client.dynamic
#from pywin.mfc import dialog
import time

class vtOfficeMSExcel(vtOfficeAppl.vtOfficeAppl):
	def __init__(self):
		bFailed=False
		vtOfficeAppl.vtOfficeAppl.__init__(self,'Excel.Application')
		self.wrk=None
		self.sht=None
	def openFile(self,fn):
		self.appl.Application.Workbooks.Open(fn)
	def addWorkbook(self):
		self.appl.Application.Workbooks.Add()
	def saveAll(self):
		for wrk in self.appl.Application.Workbooks:
			wrk.Save()
	def saveWorkbook(self,sFN=None):
		if self.wrk is not None:
			tmp=self.appl.Workbooks.Item(self.wrk)
			if sFN is not None:
				tmp.SaveAs(sFN)
			else:
				tmp.Save()
	def getWorkbooks(self):
		lst=[]
		tmp=self.appl.Application.Workbooks
		for i in range(1,tmp.Count+1):
			lst.append(tmp.Item(i).Name)
		lst.sort()
		return lst
	def selWorkbook(self,name):
		self.wrk=name
	def getSheets(self,wrk=None):
		lst=[]
		if wrk is None:
			tmpWrk=self.wrk
		else:
			tmpWrk=wrk
		if tmpWrk is None:
			return []
		tmp=self.appl.Workbooks.Item(tmpWrk).Sheets
		for i in range(1,tmp.Count+1):
			lst.append(tmp.Item(i).Name)
		lst.sort()
		return lst
	def selSheet(self,name):
		self.sht=name
	def getSheet(self,sht=None,wrk=None):
		if wrk is None:
			tmpWrk=self.wrk
		else:
			tmpWrk=wrk
		if tmpWrk is None:
			return None
		if sht is None:
			tmpSht=self.sht
		else:
			tmpSht=sht
		if tmpSht is None:
			return None
		#print tmpWrk,tmpSht
		return self.appl.Workbooks.Item(tmpWrk).Sheets.Item(tmpSht)
	def getColumns(self,sht=None):
		if sht is None:
			sht=self.getSheet()
		c=sht.Columns.Count
		inf=self._getCells(1,1,1,c)
		return inf[0]
	def getRows(self,sht=None):
		if sht is None:
			sht=self.getSheet()
		c=sht.Rows.Count
		inf=self._getCells(1,c,1,1)
		tmp=[]
		for i in inf:
			tmp.append(i[0])
		return tmp
	def getColumn(self,num,sht=None,stop_limit=0):
		if sht is None:
			sht=self.getSheet()
		c=sht.Rows.Count
		inf=self._getCells(1,c,num,num,stop_limit=stop_limit)
		return inf[0]
	def getRow(self,num,sht=None,stop_limit=0):
		if sht is None:
			sht=self.getSheet()
		c=sht.Columns.Count
		inf=self._getCells(num,num,1,c,stop_limit=stop_limit)
		return inf[0]
	def setCellValue(self,row,col,val,sht=None):
		if sht is None:
			sht=self.getSheet()
		sht.Cells(row,col).Value=val
	def getCellValue(self,row,col,sht=None):
		if sht is None:
			sht=self.getSheet()
		return sht.Cells(row,col).Value
	def screenUpdate(self,iFlag):
		if iFlag>0:
			self.appl.ScreenUpdating=True
			self.appl.Calculation=-4105
		else:
			self.appl.ScreenUpdating=False
			self.appl.Calculation=-4135
	def setCellValues(self,ll,rs,re,cs,ce,sht=None):
		if sht is None:
			sht=self.getSheet()
		sht.Range(sht.Cells(rs,cs),sht.Cells(re,ce)).Value=ll
	def getCellValues(self,rs,re,cs,ce,sht=None):
		if sht is None:
			sht=self.getSheet()
		ll=sht.Range(sht.Cells(rs,cs),sht.Cells(re,ce)).Value
		return ll
	def _getCells(self,rs,re,cs,ce,sht=None,stop_limit=0):
		if sht is None:
			sht=self.getSheet()
		lsts=[]
		emptyCount=0
		for i in range(rs,re+1):
			lst=[]
			for j in range(cs,ce+1):
				cell=sht.Cells(i,j)
				if cell.Value is None:
					emptyCount=emptyCount+1
					lst.append('')
				else:
					emptyCount=0
					lst.append(cell.Value)
				if emptyCount>stop_limit:
					#lst.sort()
					if len(lst)>0:
						lsts.append(lst)
					return lsts
			if len(lst)>0:
				lsts.append(lst)
		return lsts
	def _getCells2(self,rs,re,cs,ce,sht=None,stop_limit=0):
		if sht is None:
			sht=self.getSheet()
		lsts=[]
		emptyCount=0
		range=sht.Range(sht.Cells(rs,cs),sht.Cells(re+1,ce+1))
		for i in range(1,re+1-rs):
			lst=[]
			for j in range(1,ce+1-cs):
				cell=range.Cells(i,j)
				if cell.Value is None:
					emptyCount=emptyCount+1
					lst.append('')
				else:
					emptyCount=0
					lst.append(cell.Value)
				if emptyCount>stop_limit:
					#lst.sort()
					if len(lst)>0:
						lsts.append(lst)
					return lsts
			if len(lst)>0:
				lsts.append(lst)
		return lsts

if __name__=='__main__':
	itest=2
	if itest==2:
		excel=Excel()
		excel.show()
		excel.openFile('c:\\data\\proj\\company\\python\\app\\office\\test.xls')
		wrks=excel.getWorkbooks()
		print wrks
		excel.selWorkbook(wrks[0])
		shts=excel.getSheets()
		print shts
		excel.selSheet(shts[0])
		rowStart=1
		rowEnd=10
		colStart=1
		colEnd=10
		start_time=time.time()
		hdrs=excel._getCells2(rowStart,rowEnd,colStart,colEnd,stop_limit=100)
		durationTimeStr=time.strftime("%H:%M:%S",time.localtime(time.time()-start_time))
		print "duration:",durationTimeStr
		print hdrs
		
		start_time=time.time()
		hdrs=excel._getCells(rowStart,rowEnd,colStart,colEnd,stop_limit=100)
		durationTimeStr=time.strftime("%H:%M:%S",time.localtime(time.time()-start_time))
		print "duration:",durationTimeStr
		print hdrs
		#hdrs=excel.getRows()
	elif itest==0:
		print 'test'
		excel=Excel()
		excel.show()
		excel.openFile('c:\\data\\proj\\company\\python\\app\\office\\test.xls')
		wrks=excel.getWorkbooks()
		print wrks
		excel.selWorkbook(wrks[0])
		shts=excel.getSheets()
		print shts
		excel.selSheet(shts[0])
		hdrs=excel.getColumns()
		print hdrs
		hdrs=excel.getRows()
		print hdrs
		col=excel.getColumn(1)
		print col
		col=excel.getColumn(2)
		print col
		row=excel.getRow(1)
		print row
		row=excel.getRow(2)
		print row
		print 'csv'
		excel.openFile('c:\\data\\proj\\company\\python\\app\\office\\test.csv')
		print wrks
		excel.selWorkbook('test.csv')
		shts=excel.getSheets()
		print shts
		excel.selSheet(shts[0])
		hdrs=excel.getColumns()
		print hdrs
		hdrs=excel.getRows()
		print hdrs
	