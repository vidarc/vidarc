#----------------------------------------------------------------------------
# Name:         vtOfficeAppl.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtOfficeAppl.py,v 1.5 2012/12/14 12:43:25 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
#import win32com.client
import sys
if sys.platform.startswith('win'):
    import win32com.client.dynamic
    WIN=1
else:
    WIN=0
#import pythoncom
#from win32com.client import gencache
#from pywintypes import Unicode
#from win32com.client import DispatchWithEvents, Dispatch


class vtOfficeAppl:
	def __init__(self,Appl=None):
		self.appl=None
		if Appl is not None:
			self.open(Appl)
	def quit(self):
		if WIN:
			self.appl.Quit()
			self.appl=None
	def open(self,Appl):
		try:
			if WIN:
				self.appl=win32com.client.dynamic.Dispatch(Appl)
			else:
				self.appl=None
		except:
			if WIN:
				self.appl=win32com.client.gencache.EnsureDispatch(Appl)
			else:
				self.appl=None
		#self.show
	def show(self):
		self.appl.Visible=1
	def hide(self):
		self.appl.Visible=0

