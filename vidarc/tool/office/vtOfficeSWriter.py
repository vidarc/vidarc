#----------------------------------------------------------------------------
# Name:         vtOfficeSWriter.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtOfficeSWriter.py,v 1.1 2005/12/13 13:19:33 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import vidarc.tool.office.vtOfficeAppl as vtOfficeAppl
#import win32com.client
import string

class vtOfficeSWriter(vtOfficeAppl.vtOfficeAppl):
    def __init__(self):
        vtOfficeAppl.vtOfficeAppl.__init__(self,'com.sun.star.ServiceManager')
        self.doc=None
        self.appl._FlagAsMethod("CreateInstance")
        self.desktop = self.appl.CreateInstance("com.sun.star.frame.Desktop")
        self.desktop._FlagAsMethod("loadComponentFromURL")
        pass
    def createStruct(strTypeName):
        return self.appl.Bridge_GetStruct(strTypeName)
    def newFile(self):
        args=[]
        self.doc = self.desktop.loadComponentFromURL("private:factory/swriter", "_blank", 0, args)
        self.doc._FlagAsMethod("GetText","createInstance")
        self.doc._FlagAsMethod("createSearchDescriptor","createReplaceDescriptor")
        self.text=self.doc.GetText()
        self.text._FlagAsMethod("createTextCursor","insertString")
        self.cursor = self.text.createTextCursor()
    def convertFileName2Url(self,fn):
        fn=string.replace(fn,':','|')
        fn=string.replace(fn,'\\','/')
        return 'file:///'+fn
    def openFile(self,fn):
        args=[]
        sFN=self.convertFileName2Url(fn)
        self.doc = self.desktop.loadComponentFromURL(sFN, "_blank", 0, args)
        pass
    def save(self):
        self.doc._FlagAsMethod("store")
        self.doc.store()
    def insertString(self,text):
        self.text.insertString(self.cursor, text, 0)
    def replaceAll(self,findtext,replacetext):
        self.doc._FlagAsMethod("createSearchDescriptor","createReplaceDescriptor")
        self.doc._FlagAsMethod("replaceAll")
        oRepl = self.doc.createReplaceDescriptor()
        oRepl.searchString=findtext
        oRepl.replaceString=replacetext
        self.doc.replaceAll(oRepl)
    def insertTable(self,rows,cols,vals):
        tbl=self.doc.createInstance("com.sun.star.text.TextTable")
        tbl.initialize(rows,cols)
        #self.text._FlagAsMethod("CellRange")
        #tbl._FlagAsMethod("cellRange")
        cr=tbl.CellRange
        self.text.insertTextContent(self.cursor,tbl,0)
        tbl._FlagAsMethod("CellNames","createCursorByCellName")
        tblcur=tbl.createCursorByCellName(tbl.CellNames(0))
        tblcur.gotoStart(0)
        for valrow in vals:
            for val in valrows:
                cellname=tblcur.getRangeName()
                cell=tbl.getCellByName(cellname)
                cell.String=val
                tblcur.goRight(1,0)
            tblcur.goFirst(0)
            tblcur.goDown(1,0)
def test():
    objServiceManager = win32com.client.Dispatch("com.sun.star.ServiceManager")
    objServiceManager._FlagAsMethod("CreateInstance")
    objDesktop = objServiceManager.CreateInstance("com.sun.star.frame.Desktop")
    objDesktop._FlagAsMethod("loadComponentFromURL")
    print "test"
    args = []
    objDocument = objDesktop.loadComponentFromURL("private:factory/swriter", "_blank", 0, args)
    objDocument._FlagAsMethod("GetText")
    objText = objDocument.GetText()
    objText._FlagAsMethod("createTextCursor","insertString")
    objCursor = objText.createTextCursor()
    objText.insertString(objCursor, "The first line in the newly created text document.\n", 0)

    objDocument._FlagAsMethod("createSearchDescriptor","createReplaceDescriptor")
    oSearch = objDocument.createSearchDescriptor()
    oSearch.searchString="line"
    oFound=objDocument.findFirst(oSearch)
    print oFound,oFound.__dict__.keys()
    oFound.charFontName="test"
    
    oRepl = objDocument.createReplaceDescriptor()
    oRepl.searchString="line"
    oRepl.replaceString="LINE"
    objDocument.replaceAll(oRepl)
    
def test1():
    appl=SWriter()
    appl.newFile()
    appl.insertString("The first line in the newly created text document.\n")
    appl.replaceAll("line","LINE")
    #appl.insertTable(5,3,[['cell 1/1','cell 1/2','cell 1/3'],
    #            ['cell 2/1','cell 2/2','cell 2/3']])
if __name__=="__main__":
    #print "started"
    #test()
    test1()