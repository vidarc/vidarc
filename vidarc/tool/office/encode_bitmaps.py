#!/usr/bin/env python
#----------------------------------------------------------------------
#----------------------------------------------------------------------------
# Name:         __init__.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: encode_bitmaps.py,v 1.1 2005/12/13 13:19:33 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    #"   -u -i -n Application Tools01_16.png images.py",
#    "   -u -i -n Mondrian mondrian.ico images.py",
    "-u -i -n Add img/add.png images.py",
    "-a -u -n Cancel img/abort.png images.py",
    "-a -u -n Del img/waste.png images.py",
    "-a -u -n Apply img/ok.png images.py",
    "-a -u -n Browse img/BrowseFile01_16.png images.py",
    "-a -u -n Import img/MsOf03ExcelExp01_16.png images.py",
    "-a -u -n Link img/Synch01_16.png images.py",
    "-a -u -n Build img/Build02_16.png images.py",
    
    "-a -u -n NoIcon  img/noicon.png  images.py"
    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)

