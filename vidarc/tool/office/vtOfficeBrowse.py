#----------------------------------------------------------------------------
# Name:         vtOfficeBrowse.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtOfficeBrowse.py,v 1.1 2005/12/13 13:19:33 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import OfficeAppl

class Browse4Dir(OfficeAppl.OfficeAppl):
	def __init__(self):
		OfficeAppl.OfficeAppl.__init__(self,'Shell.Application')
		self.wrk=None
		self.sht=None

	def browse(self):
		oFolder=self.appl.BrowseForFolder(0,"Choose a folder",0)
		print oFolder
		if oFolder:
			return oFolder.Items().Item().Path
		return ""
		

if __name__=='__main__':
	o=Browse4Dir()
	print o.browse()
	