#----------------------------------------------------------------------------
# Name:         vtOfficeMSExcelDlg.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtOfficeMSExcelDialog.py,v 1.1 2005/12/13 13:19:33 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import vidarc.tool.office.vtOfficeAppl as vtOfficeAppl
#import win32con,win32api,win32ui,win32con,commctrl
#import win32com,traceback,win32com.client.dynamic
#from pywin.mfc import dialog
import time

class vtOfficeMSExcel(vtOfficeAppl.vtOfficeAppl):
	def __init__(self):
		vtOfficeAppl.vtOfficeAppl.__init__(self,'Excel.Application')
		self.wrk=None
		self.sht=None
		pass
	def openFile(self,fn):
		self.appl.Application.Workbooks.Open(fn)
	def getWorkbooks(self):
		lst=[]
		tmp=self.appl.Application.Workbooks
		for i in range(1,tmp.Count+1):
			lst.append(tmp.Item(i).Name)
		lst.sort()
		return lst
	def selWorkbook(self,name):
		self.wrk=name
	def getSheets(self,wrk=None):
		lst=[]
		if wrk is None:
			tmpWrk=self.wrk
		else:
			tmpWrk=wrk
		if tmpWrk is None:
			return []
		tmp=self.appl.Workbooks.Item(tmpWrk).Sheets
		for i in range(1,tmp.Count+1):
			lst.append(tmp.Item(i).Name)
		lst.sort()
		return lst
	def selSheet(self,name):
		self.sht=name
	def getSheet(self,sht=None,wrk=None):
		if wrk is None:
			tmpWrk=self.wrk
		else:
			tmpWrk=wrk
		if tmpWrk is None:
			return None
		if sht is None:
			tmpSht=self.sht
		else:
			tmpSht=sht
		if tmpSht is None:
			return None
		#print tmpWrk,tmpSht
		return self.appl.Workbooks.Item(tmpWrk).Sheets.Item(tmpSht)
	def getColumns(self,sht=None):
		if sht is None:
			sht=self.getSheet()
		c=sht.Columns.Count
		inf=self._getCells(1,1,1,c)
		return inf[0]
	def getRows(self,sht=None):
		if sht is None:
			sht=self.getSheet()
		c=sht.Rows.Count
		inf=self._getCells(1,c,1,1)
		tmp=[]
		for i in inf:
			tmp.append(i[0])
		return tmp
	def getColumn(self,num,sht=None,stop_limit=0):
		if sht is None:
			sht=self.getSheet()
		c=sht.Rows.Count
		inf=self._getCells(1,c,num,num,stop_limit=stop_limit)
		return inf[0]
	def getRow(self,num,sht=None,stop_limit=0):
		if sht is None:
			sht=self.getSheet()
		c=sht.Columns.Count
		inf=self._getCells(num,num,1,c,stop_limit=stop_limit)
		return inf[0]
	def setCellValue(self,row,col,val,sht=None):
		if sht is None:
			sht=self.getSheet()
		sht.Cells(row,col).Value=val
	def _getCells(self,rs,re,cs,ce,sht=None,stop_limit=0):
		if sht is None:
			sht=self.getSheet()
		lsts=[]
		emptyCount=0
		for i in range(rs,re+1):
			lst=[]
			for j in range(cs,ce+1):
				cell=sht.Cells(i,j)
				if cell.Value is None:
					emptyCount=emptyCount+1
					lst.append('')
				else:
					emptyCount=0
					lst.append(cell.Value)
				if emptyCount>stop_limit:
					#lst.sort()
					if len(lst)>0:
						lsts.append(lst)
					return lsts
			if len(lst)>0:
				lsts.append(lst)
		return lsts
	def _getCells2(self,rs,re,cs,ce,sht=None,stop_limit=0):
		if sht is None:
			sht=self.getSheet()
		lsts=[]
		emptyCount=0
		range=sht.Range(sht.Cells(rs,cs),sht.Cells(re+1,ce+1))
		for i in range(1,re+1-rs):
			lst=[]
			for j in range(1,ce+1-cs):
				cell=range.Cells(i,j)
				if cell.Value is None:
					emptyCount=emptyCount+1
					lst.append('')
				else:
					emptyCount=0
					lst.append(cell.Value)
				if emptyCount>stop_limit:
					#lst.sort()
					if len(lst)>0:
						lsts.append(lst)
					return lsts
			if len(lst)>0:
				lsts.append(lst)
		return lsts

def MakeDlgTemplate(w,h):
	style = (win32con.DS_MODALFRAME |
		win32con.WS_POPUP |
		win32con.WS_VISIBLE |
		win32con.WS_CAPTION |
		win32con.WS_SYSMENU |
		win32con.DS_SETFONT)
	cs = (win32con.WS_CHILD |
		win32con.WS_VISIBLE)

class vtOfficeMSExcelDialog(dialog.Dialog):
	def _GetDialogTemplate(self):
		style = win32con.DS_MODALFRAME | win32con.WS_POPUP | win32con.WS_VISIBLE | win32con.WS_CAPTION | win32con.WS_SYSMENU | win32con.DS_SETFONT
		visible = win32con.WS_CHILD | win32con.WS_VISIBLE

		w=300
		h=200
		
		s=win32con.WS_CHILD|win32con.WS_VISIBLE|win32con.WS_TABSTOP|win32con.WS_BORDER|win32con.ES_AUTOHSCROLL
		ss=win32con.WS_CHILD|win32con.WS_VISIBLE
		dt = [
			["View Dialog", (0, 0, w, h), style, None, (8, "MS Sans Serif")],
			["Static","File1",80,(5,5,25,14),win32con.WS_CHILD|win32con.WS_VISIBLE],
			["Button","Open",1000,(235,5, 30, 14), visible],
			["Edit","",1001,(30,5,200,14),s],
			["Static","File2",81,(5,20,25,14),win32con.WS_CHILD|win32con.WS_VISIBLE],
			["Button","Open",1002,(235,20, 30, 14), visible],
			["Edit","",1003,(30,20,200,14),s],

			["Static","File1",81,(5,40,25,14),win32con.WS_CHILD|win32con.WS_VISIBLE],
			["Combobox","None",2200,(5,50,70,70),win32con.WS_CHILD|win32con.WS_VISIBLE|win32con.WS_TABSTOP|win32con.WS_BORDER|win32con.CBS_DROPDOWNLIST|win32con.CBS_SORT|win32con.WS_VSCROLL],
			["Combobox","None",2202,(5,70,70,70),win32con.WS_CHILD|win32con.WS_VISIBLE|win32con.WS_TABSTOP|win32con.WS_BORDER|win32con.CBS_DROPDOWNLIST|win32con.WS_VSCROLL],
#			["Listbox","None",2000,(5,20,50,70),win32con.WS_CHILD|win32con.WS_VISIBLE|win32con.WS_TABSTOP|win32con.WS_BORDER|win32con.WS_VSCROLL|win32con.WS_HSCROLL|win32con.LBS_SORT|win32con.LBS_NOTIFY|win32con.LBS_EXTENDEDSEL],
			["Static","File2",81,(120,40,25,14),win32con.WS_CHILD|win32con.WS_VISIBLE],
			["Combobox","None",2201,(120,50,70,70),win32con.WS_CHILD|win32con.WS_VISIBLE|win32con.WS_TABSTOP|win32con.WS_BORDER|win32con.CBS_DROPDOWNLIST|win32con.CBS_SORT|win32con.WS_VSCROLL],
			["Combobox","None",2203,(120,70,70,70),win32con.WS_CHILD|win32con.WS_VISIBLE|win32con.WS_TABSTOP|win32con.WS_BORDER|win32con.CBS_DROPDOWNLIST|win32con.WS_VSCROLL],
			["Button","Link",1040,(85, 50, 25, 14), visible],
			["Button","Keys",1041,(85, 70, 25, 14), visible],
##			["Listbox","None",2001,(60,20,50,70),win32con.WS_CHILD|win32con.WS_VISIBLE|win32con.WS_TABSTOP|win32con.WS_BORDER|win32con.WS_VSCROLL|win32con.LBS_SORT|win32con.LBS_NOTIFY|win32con.LBS_EXTENDEDSEL],
#			["Listbox","None",2002,(115,20,50,70),win32con.WS_CHILD|win32con.WS_VISIBLE|win32con.WS_TABSTOP|win32con.WS_BORDER|win32con.WS_VSCROLL|win32con.LBS_SORT|win32con.LBS_NOTIFY|win32con.LBS_EXTENDEDSEL],
#			["Listbox","None",2003,(170,20,50,70),win32con.WS_CHILD|win32con.WS_VISIBLE|win32con.WS_TABSTOP|win32con.WS_BORDER|win32con.WS_VSCROLL|win32con.LBS_SORT|win32con.LBS_NOTIFY|win32con.LBS_EXTENDEDSEL],

#			["Static","count=-1",80,(10,110,50,14),ss],
			#["Static","",81,(60,110,100,14),ss],
			["Listbox","None",2300,(5,90,100,70),win32con.WS_CHILD|win32con.WS_VISIBLE|win32con.WS_TABSTOP|win32con.WS_BORDER|win32con.WS_VSCROLL|win32con.LBS_NOTIFY|win32con.LBS_EXTENDEDSEL],
			["Button","Del",2302,(5, 150, 30, 14), visible],
			["Button","Add",2304,(35, 150, 30, 14), visible],
			["Edit","",2306,(5,165,70,14),s],
			["Listbox","None",2301,(115,90,100,70),win32con.WS_CHILD|win32con.WS_VISIBLE|win32con.WS_TABSTOP|win32con.WS_BORDER|win32con.WS_VSCROLL|win32con.LBS_NOTIFY|win32con.LBS_EXTENDEDSEL],
			["Button","Del",2303,(115, 150, 30, 14), visible],
			#["Button","Add",2305,(145, 150, 30, 14), visible],
			#["Edit","",2307,(115,165,70,14),s],
			#["Static","x",180,(10,30,10,14),ss],
			#["Static","",181,(60,30,30,14),ss],
			#["Static","y",182,(10,50,10,14),ss],
			#["Static","",183,(60,50,30,14),ss],
			#["Static","zoom",282,(10,70,30,14),ss],
			#["Edit","",1000,(30,70,30,14),s],
			["Button","Show",1050,(230, 50, 50, 14), visible],
			["Button","Hide",1051,(230, 70, 50, 14), visible],
			["Button","Merge",1060,(230, 150, 50, 14), visible],
			
#			["Button","C&alc",1002,(30, 90, 50, 14), visible],
#			["Button","Export",1003,(230, 90, 50, 14), visible],
			[128,"&Ok",win32con.IDOK,(10, h-18, 50, 14), visible | win32con.BS_DEFPUSHBUTTON],
			[128,"Cancel",win32con.IDCANCEL,(w - 60, h-18, 50, 14), visible | win32con.BS_PUSHBUTTON]
			]
		return dt
	def __init__(self,dlgID,dll=None):
		self.tags=None
		self.calcTags=None
		self.keys=[]
		self.link=[]
		self.excel=Excel()
		dialog.Dialog.__init__(self,self._GetDialogTemplate())
	def OnInitDialog(self):
		rc = dialog.Dialog.OnInitDialog(self)
		#self.HookMessage (self.OnDestroy, win32con.WM_DESTROY)
		self.HookCommand(self.OnOpenFile1,1000)
		self.HookCommand(self.OnOpenFile2,1002)
		
		self.HookCommand(self.OnMerge,1060)
		self.HookCommand(self.OnShow,1050)
		self.HookCommand(self.OnHide,1051)
		self.HookCommand(self.OnAddColumn,2304)
		self.HookCommand(self.OnAddKey,1041)
		self.HookCommand(self.OnAddLink,1040)
		self.HookCommand(self.OnSheet0Sel,2200)
		self.HookCommand(self.OnSheet1Sel,2201)
		self.HookCommand(self.OnCol0Sel,2202)
		self.HookCommand(self.OnCol1Sel,2203)
		self.txtFile1=self.GetDlgItem(1001)
		self.txtFile2=self.GetDlgItem(1003)
		self.lstSheets=[None,None]
		self.lstSheets[0]=self.GetDlgItem(2200)
		self.lstSheets[1]=self.GetDlgItem(2201)
		self.lstColumns=[None,None]
		self.lstColumns[0]=self.GetDlgItem(2202)
		self.lstColumns[1]=self.GetDlgItem(2203)
		self.fullfilename=[None,None]
		self.filename=[None,None]
		self.sheet2Show=[None,None]
		self.col2Set=[None,None]
		self.countColumns=[0,0]
		self.keys=[]
		self.links=[]
		self.maxHdrs=2
		self.lstKey=self.GetDlgItem(2300)
		self.lstLink=self.GetDlgItem(2301)
		self.txtColumn=self.GetDlgItem(2306)
		self.HookCommand(self.OnDelKey,2302)
		self.HookCommand(self.OnDelLink,2303)
		return rc
	def _isTupInLst(self,tupLst,tup):
		for item in tupLst:
			if item==tup:
				return 1
		return 0
	def _showHeaders(self):
		infos=string.split(self.tags.fileHeader[:-1],',')
		infos.sort()
		for i in range(0,self.maxHdrs):
			self.lstHeaders[i].ResetContent()
			self.lstHeaders[i].AddString(' ')
			for inf in infos:
				self.lstHeaders[i].AddString(inf)
	def _checkInfo(self):
		for i in range(self.maxHdrs-1,-1):
			for j in range(0,self.maxHdrs):
				if i<>j:
					if self.info2Show[j] is not None:
						if self.info2Show[i] is not None:
							if self.info2Show[i]==self.info2Show[j]:
								self.info2Show[i]=None
		for i in range(0,self.maxHdrs):
			if self.info2Show[i]==' ':
				self.info2Show[i]=None
		try:
			for i in range(0,self.maxHdrs):
				if self.info2Show[i] is None:
					self.lstHeaders[i].SetCurSel(0)
					#self.txtInfo[i].SetWindowText('')
		except:
			pass
	def _showSheets(self,num):
		if num<0 or num>=self.maxHdrs:
			return
		self.lstSheets[num].ResetContent()
		self.excel.openFile(self.fullfilename[num])
		#wrks=self.excel.getWorkbooks()
		self.excel.selWorkbook(self.filename[num])
		#self.excel.show()
		shts=self.excel.getSheets()
		for tmp in shts:
			self.lstSheets[num].AddString(tmp)
	def _showHdrs(self,num):
		if num<0 or num>=self.maxHdrs:
			return
		self.lstColumns[num].ResetContent()
		if self.sheet2Show is None:
			return
		self.excel.selWorkbook(self.filename[num])
		self.excel.selSheet(self.sheet2Show[num])
		hdrs=self.excel.getColumns()
		for tmp in hdrs:
			self.lstColumns[num].AddString(tmp)
		self.countColumns[num]=len(hdrs)
	def _showKeys(self):
		self.lstKey.ResetContent()
		for k in self.keys:
			s="%s <-> %s"%(k[0][0],k[1][0])
			self.lstKey.AddString(s)
		pass
	def _showLinks(self):
		self.lstLink.ResetContent()
		for k in self.links:
			s="%s <-> %s"%(k[0][0],k[1][0])
			self.lstLink.AddString(s)
		pass
	def OnMerge(self,id,msg):
		start_time=time.time()
		self.excel.selWorkbook(self.filename[1])
		self.excel.selSheet(self.sheet2Show[1])
		rows=self.excel.getRows()
		count=len(rows)
		tmp1={}
		print "rows:%d"%count
		for i in range(2,count):
			tmpKey=''
			for key in self.keys:
				idx=key[1][1]+1
				row=self.excel._getCells(i,i,idx,idx)[0]
				s=`row[0]`
				tmpKey=tmpKey+s
			rowinfo=[]
			for link in self.links:
				idx=link[1][1]+1
				s=self.excel._getCells(i,i,idx,idx)[0]
				rowinfo.append(s[0])
			tmp1[tmpKey]=rowinfo
		self.excel.selWorkbook(self.filename[0])
		self.excel.selSheet(self.sheet2Show[0])
		rows=self.excel.getRows()
		count=len(rows)
		for i in range(2,count):
			tmpKey=''
			for key in self.keys:
				idx=key[0][1]+1
				row=self.excel._getCells(i,i,idx,idx)[0]
				s=`row[0]`
				tmpKey=tmpKey+s
			try:
				value=tmp1[tmpKey]
				idx=0
				for link in self.links:
					self.excel.setCellValue(i,link[0][1]+1,value[idx])
					idx=idx+1
			except:
				pass
		end_time=time.time()
		duration=time.localtime(end_time-start_time)
		print "%d:%d:%d"%(duration[3],duration[4],duration[5])
		pass
	def OnMergeOld(self,id,msg):
		start_time=time.time()
		#print self.filename[1]
		#print self.sheet2Show[1]
		self.excel.selWorkbook(self.filename[1])
		self.excel.selSheet(self.sheet2Show[1])
		#print self.excel.getRow(1)
		
		rows=self.excel.getRows()
		count=len(rows)
		tmp1={}
		print "rows:%d"%count
		for i in range(2,count):
			row=self.excel._getCells(i,i,1,self.countColumns[1],stop_limit=1000)[0]
			tmpKey=''
			for key in self.keys:
				s=`row[key[1][1]]`
				tmpKey=tmpKey+s
			#print tmpKey
			#rowinfo=[]
			#for link in self.links:
					#print link[1]
					#print row
					#print row[link[1][1]]
			#		rowinfo.append(row[link[1][1]+1])
					#self.excel.setCellValue(i,link[0][1]+1,value[link[1][1]]) 
			#print "got rowinfo",rowinfo
			tmp1[tmpKey]=row
		#print 'test'
		#print self.links
		#print tmp1
		#print self.filename[0]
		#print self.sheet2Show[0]
		self.excel.selWorkbook(self.filename[0])
		self.excel.selSheet(self.sheet2Show[0])
		rows=self.excel.getRows()
		count=len(rows)
		for i in range(2,count):
			#row=self.excel.getRow(i)
			row=self.excel._getCells(i,i,1,self.countColumns[0],stop_limit=1000)[0]
			tmpKey=''
			for key in self.keys:
				s=`row[key[0][1]]`
				tmpKey=tmpKey+s
			#print tmpKey
			try:
				value=tmp1[tmpKey]
				#print tmpKey,value
				for link in self.links:
					#print link
					#print link[0]
					#print link[1]
					#print value
					#print "set link %s %d %s"%(link[0][0],link[0][1],value[link[1][1]])
					self.excel.setCellValue(i,link[0][1]+1,value[link[1][1]])
			except:
				pass
		end_time=time.time()
		duration=time.localtime(end_time-start_time)
		print "%d:%d:%d"%(duration[3],duration[4],duration[5])
		pass
	
	def OnShow(self,id,msg):
		self.excel.show()
	def OnHide(self,id,msg):
		self.excel.hide()
	def OnOpenFile1(self,id,msg):
		flags=win32con.OFN_HIDEREADONLY|win32con.OFN_OVERWRITEPROMPT
		filter=None#"Tree (*.tree)|*.tree|All (*.*)|*.*||"
		dlgFile=win32ui.CreateFileDialog(1,None,None,flags,filter,self)
		dlgFile.SetOFNInitialDir(self.txtFile1.GetWindowText())
		if dlgFile.DoModal()==win32con.IDOK:
			try:
				#i=len(dlgFile.GetFileName())
				s=dlgFile.GetPathName()
			except:
				s=dlgFile.GetPathName()
			self.txtFile1.SetWindowText(s)
			self.fullfilename[0]=s
			self.filename[0]=dlgFile.GetFileName()
			self._showSheets(0)
			self.keys=[]
			self.link=[]
			self._showKeys()
			self._showLinks()
		pass
	def OnOpenFile2(self,id,msg):
		flags=win32con.OFN_HIDEREADONLY|win32con.OFN_OVERWRITEPROMPT
		filter=None#"Tree (*.tree)|*.tree|All (*.*)|*.*||"
		dlgFile=win32ui.CreateFileDialog(1,None,None,flags,filter,self)
		dlgFile.SetOFNInitialDir(self.txtFile2.GetWindowText())
		if dlgFile.DoModal()==win32con.IDOK:
			try:
				s=dlgFile.GetPathName()
			except:
				s=dlgFile.GetPathName()
			self.txtFile2.SetWindowText(s)
			self.fullfilename[1]=s
			self.filename[1]=dlgFile.GetFileName()
			self._showSheets(1)
			self.keys=[]
			self.link=[]
			self._showKeys()
			self._showLinks()
		pass
	def OnExport(self,id,msg):
		flags=win32con.OFN_HIDEREADONLY|win32con.OFN_OVERWRITEPROMPT
		filter="auto_tags (auto_tag*.csv)|auto_tag*.csv|comma sep. file (*.csv)|*.csv|All (*.*)|*.*||"
		dlgFile=win32ui.CreateFileDialog(1,None,None,flags,filter,self)
		if dlgFile.DoModal()==win32con.IDOK:
			ofn=dlgFile.GetPathName()
			if ofn is None:
				return
			else:
				outf=open(ofn,"w")
			outf.write(self.tags.fileHeader)
			for tmpVal in self.calcTags:
				tmpVal.write(outf,self.tags.fileHeader)
			outf.close()
	def OnCalc(self,id,msg):
		self._checkInfo()
		tmpSelLst=[]
		self.calcTags=None
		for i in range(0,self.maxHdrs):
			if self.info2Show[i] is not None:
				tmpSel=self.lstSel[i].GetSelTextItems()
				if len(tmpSel)>0:
					tmpSelLst.append(tmpSel)
				else:
					tmpSelLst.append(None)
			else:
				tmpSelLst.append(None)
		units=self.tags.getUnits()
		tmpTags=[]
		for unit in units:
			for tmp in self.tags.getTags(unit,None):
				tmpFound=tmp
				for i in range(0,self.maxHdrs):
					if self.info2Show[i] is not None:
						tmpInf=tmp.getInfo(self.info2Show[i])
						try:
							if tmpSelLst[i] is not None:
								idx=tmpSelLst[i].index(tmpInf)
						except:
							tmpFound=None
							break
				if tmpFound is not None:
					tmpTags.append(tmpFound)
		s="count=%d"%len(tmpTags)
		self.lblCount.SetWindowText(s)
		self.lstResult.ResetContent()
		for tmp in tmpTags:
			s=tmp.getExtTagId()
			self.lstResult.AddString(s)
		self.calcTags=tmpTags
		#hz=string.atof(self.txtZoom.GetWindowText())
		pass
	def OnSheet0Sel(self,id,msg):
		if msg==1:
			idx=self.lstSheets[0].GetCurSel()
			self.sheet2Show[0]=self.lstSheets[0].GetLBText(idx)
			self._showHdrs(0)
	def OnSheet1Sel(self,id,msg):
		if msg==1:
			idx=self.lstSheets[1].GetCurSel()
			self.sheet2Show[1]=self.lstSheets[1].GetLBText(idx)
			self._showHdrs(1)
	def OnDelKey(self,id,msg):
		idx=self.lstKey.GetCurSel()
		if idx>=0:
#			self.keys.remove(self.lstKey.GetText(idx))
			self.lstKey.DeleteString(idx)
			self.keys=self.keys[:idx]+self.keys[idx+1:]
	def OnDelLink(self,id,msg):
		idx=self.lstLink.GetCurSel()
		if idx>=0:
#			self.links.remove(self.lstLink.GetText(idx))
			self.lstLink.DeleteString(idx)
			self.links=self.links[:idx]+self.links[idx+1:]
	def OnCol0Sel(self,id,msg):
		if msg==1:
			idx=self.lstColumns[0].GetCurSel()
			self.col2Set[0]=(self.lstColumns[0].GetLBText(idx),idx)
			#self._showInfo(2)
	def OnCol1Sel(self,id,msg):
		if msg==1:
			idx=self.lstColumns[1].GetCurSel()
			self.col2Set[1]=(self.lstColumns[1].GetLBText(idx),idx)
			#self._showInfo(3)
	def OnAddColumn(self,id,msg):
		s=self.txtColumn.GetWindowText()
		if len(s)<1:
			return
		self.lstColumns[0].AddString(s)
	def OnAddKey(self,id,msg):
		if self.col2Set[0] is None:
			return
		if self.col2Set[1] is None:
			return
		tup=(self.col2Set[0],self.col2Set[1])
		if self._isTupInLst(self.keys,tup)<>0:
			return 
		self.keys.append(tup)
		self._showKeys()
	def OnAddLink(self,id,msg):
		if self.col2Set[0] is None:
			return
		if self.col2Set[1] is None:
			return
		tup=(self.col2Set[0],self.col2Set[1])
		if self._isTupInLst(self.links,tup)<>0:
			return 
		if self._isTupInLst(self.keys,tup)<>0:
			return 
		self.links.append(tup)
		self._showLinks()
		pass
	
	def SetTags(self,nt):
		print "set tags"
		self.tags=nt
		#self._showHeaders()

def openDialog(modal=0,sNewDwgDir=None):
	d = ExcelDialog (MakeDlgTemplate(300,150))
	#if sNewDwgDir is not None:
	#	d.SetDwgDir(sNewDwgDir)
	#else:
	#	d.SetDwgDir(sDwgDir)
	if modal:
		d.DoModal()
	else:
		d.CreateWindow()

if __name__=='__main__':
	itest=2
	if itest==2:
		excel=Excel()
		excel.show()
		excel.openFile('c:\\data\\proj\\company\\python\\app\\office\\test.xls')
		wrks=excel.getWorkbooks()
		print wrks
		excel.selWorkbook(wrks[0])
		shts=excel.getSheets()
		print shts
		excel.selSheet(shts[0])
		rowStart=1
		rowEnd=10
		colStart=1
		colEnd=10
		start_time=time.time()
		hdrs=excel._getCells2(rowStart,rowEnd,colStart,colEnd,stop_limit=100)
		durationTimeStr=time.strftime("%H:%M:%S",time.localtime(time.time()-start_time))
		print "duration:",durationTimeStr
		print hdrs
		
		start_time=time.time()
		hdrs=excel._getCells(rowStart,rowEnd,colStart,colEnd,stop_limit=100)
		durationTimeStr=time.strftime("%H:%M:%S",time.localtime(time.time()-start_time))
		print "duration:",durationTimeStr
		print hdrs
		#hdrs=excel.getRows()
	elif itest==1:
		openDialog(0,'G:\\VB_Lonza\\LON\\5_piping\\512_020528\\')
	elif itest==0:
		print 'test'
		excel=Excel()
		excel.show()
		excel.openFile('c:\\data\\proj\\company\\python\\app\\office\\test.xls')
		wrks=excel.getWorkbooks()
		print wrks
		excel.selWorkbook(wrks[0])
		shts=excel.getSheets()
		print shts
		excel.selSheet(shts[0])
		hdrs=excel.getColumns()
		print hdrs
		hdrs=excel.getRows()
		print hdrs
		col=excel.getColumn(1)
		print col
		col=excel.getColumn(2)
		print col
		row=excel.getRow(1)
		print row
		row=excel.getRow(2)
		print row
		print 'csv'
		excel.openFile('c:\\data\\proj\\company\\python\\app\\office\\test.csv')
		print wrks
		excel.selWorkbook('test.csv')
		shts=excel.getSheets()
		print shts
		excel.selSheet(shts[0])
		hdrs=excel.getColumns()
		print hdrs
		hdrs=excel.getRows()
		print hdrs
	