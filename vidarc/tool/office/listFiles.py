import win32con,win32api,win32ui,win32con,commctrl
import win32com,traceback,win32com.client.dynamic
from pywin.mfc import dialog
import os,sys
from stat import *


def MakeDlgTemplate(w,h):
	style = (win32con.DS_MODALFRAME |
		win32con.WS_POPUP |
		win32con.WS_VISIBLE |
		win32con.WS_CAPTION |
		win32con.WS_SYSMENU |
		win32con.DS_SETFONT)
	cs = (win32con.WS_CHILD |
		win32con.WS_VISIBLE)

class ListFilesDialog(dialog.Dialog):
	def _GetDialogTemplate(self):
		style = win32con.DS_MODALFRAME | win32con.WS_POPUP | win32con.WS_VISIBLE | win32con.WS_CAPTION | win32con.WS_SYSMENU | win32con.DS_SETFONT
		visible = win32con.WS_CHILD | win32con.WS_VISIBLE

		w=300
		h=200
		
		s=win32con.WS_CHILD|win32con.WS_VISIBLE|win32con.WS_TABSTOP|win32con.WS_BORDER|win32con.ES_AUTOHSCROLL
		ss=win32con.WS_CHILD|win32con.WS_VISIBLE
		dt = [
			["View Dialog", (0, 0, w, h), style, None, (8, "MS Sans Serif")],
			["Static","File1",80,(5,5,25,14),win32con.WS_CHILD|win32con.WS_VISIBLE],
			["Button","Open",1000,(235,5, 30, 14), visible],
			["Edit","",1001,(30,5,200,14),s],

			["Listbox","None",2300,(5,20,100,70),win32con.WS_CHILD|win32con.WS_VISIBLE|win32con.WS_TABSTOP|win32con.WS_BORDER|win32con.WS_VSCROLL|win32con.LBS_NOTIFY|win32con.LBS_EXTENDEDSEL],
			["Button","Del",2302,(5, 150, 30, 14), visible],
			["Button","Add",2304,(35, 150, 30, 14), visible],
			["Edit","",2306,(5,165,70,14),s],
			["Listbox","None",2301,(115,90,100,70),win32con.WS_CHILD|win32con.WS_VISIBLE|win32con.WS_TABSTOP|win32con.WS_BORDER|win32con.WS_VSCROLL|win32con.LBS_NOTIFY|win32con.LBS_EXTENDEDSEL],
			["Button","Del",2303,(115, 150, 30, 14), visible],

			["Button","Show",1050,(230, 50, 50, 14), visible],
			["Button","Hide",1051,(230, 70, 50, 14), visible],
			["Button","Merge",1060,(230, 150, 50, 14), visible],
			
			["Button","Export",1003,(230, 90, 50, 14), visible],
			[128,"&Ok",win32con.IDOK,(10, h-18, 50, 14), visible | win32con.BS_DEFPUSHBUTTON],
			[128,"Cancel",win32con.IDCANCEL,(w - 60, h-18, 50, 14), visible | win32con.BS_PUSHBUTTON]
			]
		return dt
	def __init__(self,dlgID,dll=None):
		self.tags=None
		self.calcTags=None
		self.keys=[]
		self.link=[]
		dialog.Dialog.__init__(self,self._GetDialogTemplate())
	def OnInitDialog(self):
		rc = dialog.Dialog.OnInitDialog(self)
		#self.HookMessage (self.OnDestroy, win32con.WM_DESTROY)
		self.HookCommand(self.OnOpen,1000)
		
		self.txtFile=self.GetDlgItem(1001)
		
		self.lstDirInfo=self.GetDlgItem(2300)
		#self.HookCommand(self.OnDelKey,2302)
		#self.HookCommand(self.OnDelLink,2303)
		return rc
	def OnOpen(self,id,msg):
		flags=win32con.OFN_HIDEREADONLY|win32con.OFN_OVERWRITEPROMPT
		filter=None#"Tree (*.tree)|*.tree|All (*.*)|*.*||"
		dlgFile=win32ui.CreateFileDialog(1,None,None,flags,filter,self)
		#dlgFile.SetOFNInitialDir(self.txtFile1.GetWindowText())
		if dlgFile.DoModal()==win32con.IDOK:
			try:
				i=len(dlgFile.GetFileName())
				s=dlgFile.GetPathName()
			except:
				s=dlgFile.GetPathName()
			#self.txtFile1.SetWindowText(s)
			#self.fullfilename=s
			#self.filename=dlgFile.GetFileName()
			dirinfo=self._readDir(s[:i])
			
			self._showDirs()
		pass
	def _readDir(self,dir):
		dl=os.listdir(dir)
		print dl

def openDialog(modal=0,sNewDwgDir=None):
	d = ListFilesDialog (MakeDlgTemplate(300,150))
	#if sNewDwgDir is not None:
	#	d.SetDwgDir(sNewDwgDir)
	#else:
	#	d.SetDwgDir(sDwgDir)
	if modal:
		d.DoModal()
	else:
		d.CreateWindow()

def getChar4Level(c,level):
	s=''
	for i in range(0,level):
		s=s+c
	return s

def listFiles(dir,level=0):
	files=os.listdir(dir)
	files.sort()
	for f in files:
		fn=dir+'/'+f
		m=os.stat(fn)[ST_MODE]
		if S_ISDIR(m):
			listFiles(fn,level+1)
		elif S_ISREG(m):
			print fn
	print files
	

def writeListFiles(of,dir,level=0):
	files=os.listdir(dir)
	files.sort()
	for f in files:
		fn=dir+'/'+f
		m=os.stat(fn)[ST_MODE]
		if S_ISDIR(m):
			of.write(getChar4Level(',',level)+f+'\n')
			writeListFiles(of,fn,level+1)
		elif S_ISREG(m):
			of.write(getChar4Level(',',level)+f+'\n')

def writeListFiles2File(ofn,dir):
	of=open(ofn,'w')
	writeListFiles(of,dir)
	of.close()
#def walktree(dir, callback):
#	'''recursively descend the directory rooted at dir,
#		calling the callback function for each regular file'''
#	for f in os.listdir(dir):
#		pathname = '%s/%s' % (dir, f)
#		mode = os.stat(pathname)[ST_MODE]
#		if S_ISDIR(mode):
#			# It's a directory, recurse into it
#			walktree(pathname, callback)
#		elif S_ISREG(mode):
#			# It's a file, call the callback function
#			callback(pathname)
#		else:
#			# Unknown file type, print a message
#			pass
#		print 'Skipping %s' % pathname
#
#def visitfile(file):
#	print 'visiting', file

	
if __name__=='__main__':
	#listFiles('g:/VB_lonza/LON/7_msr_automation')
	#listFiles('g:/VB_Lonza/LON/7_msr_automat/771')
	#writeListFiles2File('c:/temp/listFiles.csv','g:/VB_Lonza/LON/7_msr_automat/771')
	#listFiles('g:/VB_Lonza/LON/7_msr_automat/771',visitfile)
	#walktree('g:/VB_Lonza/LON/7_msr_automat',visitfile)
	openDialog(0)