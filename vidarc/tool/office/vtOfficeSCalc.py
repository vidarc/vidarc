#----------------------------------------------------------------------------
# Name:         vtOfficeSCalc.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtOfficeSCalc.py,v 1.3 2010/03/03 02:17:11 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import vidarc.tool.office.vtOfficeAppl as vtOfficeAppl
#import win32com.client
import string

class vtOfficeSCalc(vtOfficeAppl.vtOfficeAppl):
    def __init__(self):
        vtOfficeAppl.vtOfficeAppl.__init__(self,'com.sun.star.ServiceManager')
        self.doc=None
        self.appl._FlagAsMethod("Bridge_GetStruct")
        self.appl._FlagAsMethod("CreateInstance")
        self.desktop = self.appl.CreateInstance("com.sun.star.frame.Desktop")
        self.desktop._FlagAsMethod("loadComponentFromURL")
        pass
    def createStruct(strTypeName):
        return self.appl.Bridge_GetStruct(strTypeName)
    def convertFileName2Url(self,fn):
        fn=string.replace(fn,':','|')
        fn=string.replace(fn,'\\','/')
        return 'file:///'+fn
    def newFile(self):
        args=[]
        self.doc = self.desktop.loadComponentFromURL("private:factory/scalc", "_blank", 0, args)
        self.doc._FlagAsMethod("Sheets","getSheets")
        #self.doc._FlagAsMethod("createSearchDescriptor","createReplaceDescriptor")
        #self.text=self.doc.GetText()
        #self.text._FlagAsMethod("createTextCursor","insertString")
        #self.cursor = self.text.createTextCursor()
    def openFile(self,fn):
        #self.appl._FlagAsMethod("createStruct")
        #hiddenProperty = self.appl.createStruct("com.sun.star.beans.PropertyValue")
        #hiddenProperty.Name = "Hidden"
        #hiddenProperty.Value = "True"
        #args=[hiddenProperty]
        #args=[("Hidden",1)]
        args=[]
        sFN=self.convertFileName2Url(fn)
        #print sFN
        self.doc = self.desktop.loadComponentFromURL(sFN, "_blank", 0, args)
        self.doc._FlagAsMethod("Sheets","getSheets")
        pass
    def saveAs(self,fn):
        saveProperty = self.createStruct("com.sun.star.beans.PropertyValue")
        saveProperty.Name = "FilterName"
        saveProperty.Value = "MS PowerPoint 97"
        present.storeAsUrl("file:///d|/collection_data.ppt",[saveProperty])
    def save(self):
        self.doc._FlagAsMethod("store")
        self.doc.store()
    def closeFile(self):
        #self.doc._FlagAsMethod("close")
        #self.doc.close()
        self.doc._FlagAsMethod("dispose")
        self.doc.dispose()
        pass
    def close(self):
        pass
    def selSheet(self,sht=None):
        shts=self.doc.getSheets()#self.doc.Sheets(0)
        shts._FlagAsMethod("getElementNames")
        shtnames=shts.getElementNames()
        try:
            #n=shtnames.index(sht)
            self.sheet=shts.getByName(sht)
        except:
            sht=shtnames[0]
            self.sheet=shts.getByName(sht)
        self.sheet._FlagAsMethod("getCellRangeByPosition","getCellRangeByName")
    def getSheetNames(self):
        shts=self.doc.getSheets()#self.doc.Sheets(0)
        shts._FlagAsMethod("getElementNames")
        return shts.getElementNames()
    def getSheet(self,sht=None):
        shts=self.doc.getSheets()#self.doc.Sheets(0)
        shts._FlagAsMethod("getElementNames","getCellRangeByPosition")
        shtnames=shts.getElementNames()
        try:
            n=shtnames.index(sht)
        except:
            sht=shtnames[0]
        return shts.getByName(sht)
    def getColumns(self,sht=None):
        if sht is None:
            sht=self.getSheet()
    #    c=sht.Columns.Count
    #    inf=self._getCells(1,1,1,c)
    #    return inf[0]
    def setCell(self,col,row,val):
        c=self.sheet.getCellByPosition(col,row)
        c.String=val
    def setCellNum(self,col,row,val):
        c=self.sheet.getCellByPosition(col,row)
        c.Value=val
    def setCellByName(self,name,val):
        cr=self.sheet.getCellRangeByName(name+':'+name)
        cr._FlagAsMethod("getCellByPosition")
        c=cr.getCellByPosition(0,0)
        c.String=val
    def getCell(self,row,col):
        return self.sheet.getCellByPosition(col,row)
    def getCellRange(self,row_anf,col_anf,row_end,col_end):
        cr=self.sheet.getCellRangeByPosition(col_anf,row_anf,
                    col_end,row_end)
        #cr._FlagAsMethod("getCellRangeByPosition")
        lst=[]
        iRows=cr.Rows.Count
        for j in range(0,cr.Rows.Count):
            l=[]
            for i in range(0,cr.Columns.Count):
                c=cr.getCellByPosition(i,j)
                val=c.String
                l.append(val)
            lst.append(l)
        return lst
    def getCellRangeByName(self,topleft,bottomright):
        cr=self.sheet.getCellRangeByName(topleft+':'+bottomright)
        #cr._FlagAsMethod("getCellRangeByPosition")
        cr._FlagAsMethod("getCellByPosition")
        lst=[]
        iRows=cr.Rows.Count
        for j in range(0,cr.Rows.Count):
            l=[]
            for i in range(0,cr.Columns.Count):
                c=cr.getCellByPosition(i,j)
                val=c.String
                l.append(val)
            lst.append(l)
        return lst
    def setCells(self,row,col,vals):
        i=row
        for rowLst in vals:
            j=col
            for v in rowLst:
                c=self.sheet.getCellByPosition(j,i)
                #c=self.getCell(i,j)
                c.String=v
                j=j+1
            i=i+1
    def setCells2(self,row,col,vals):
        i=row
        for rowLst in vals:
            j=col
            for v in rowLst:
                #c=self.sheet.getCellByPosition(i,j)
                c=self.getCell(i,j)
                c.String=v
                j=j+1
            i=i+1
    def replaceAll(self,findtext,replacetext):
        oRepl = self.doc.createReplaceDescriptor()
        oRepl.searchString=findtext
        oRepl.replaceString=replacetext
        self.doc.replaceAll(oRepl)
    def setVisible(self,flag):
        self.doc._FlagAsMethod("setVisible")
        self.doc.setVisible(flag)
        #self.desktop._FlagAsMethod("setVisible")
        #self.desktop.setVisible(flag)
        #self.appl._FlagAsMethod("setVisible")
        #self.appl.setVisible(flag)
def test():
    objServiceManager = win32com.client.Dispatch("com.sun.star.ServiceManager")
    objServiceManager._FlagAsMethod("CreateInstance")
    objDesktop = objServiceManager.CreateInstance("com.sun.star.frame.Desktop")
    objDesktop._FlagAsMethod("loadComponentFromURL")
    print "test"
    args = []
    objDocument = objDesktop.loadComponentFromURL("private:factory/swriter", "_blank", 0, args)
    objDocument._FlagAsMethod("GetText")
    objText = objDocument.GetText()
    objText._FlagAsMethod("createTextCursor","insertString")
    objCursor = objText.createTextCursor()
    objText.insertString(objCursor, "The first line in the newly created text document.\n", 0)

    objDocument._FlagAsMethod("createSearchDescriptor","createReplaceDescriptor")
    oSearch = objDocument.createSearchDescriptor()
    oSearch.searchString="line"
    oFound=objDocument.findFirst(oSearch)
    print oFound,oFound.__dict__.keys()
    oFound.charFontName="test"
    
    oRepl = objDocument.createReplaceDescriptor()
    oRepl.searchString="line"
    oRepl.replaceString="LINE"
    objDocument.replaceAll(oRepl)
    
def test1():
    import time
    appl=vtOfficeSCalc()
    appl.newFile()
    appl.selSheet()
    c=appl.getCell(2,5)
    #c.insertString('test')
    c.String='test'
    cr=appl.getCellRange(0,0,5,5)
    print cr
    
    lst=[]
    for i in range(0,30):
        l=[]
        for j in range(0,10):
            l.append('cell %03d/%03d'%(i,j))
        lst.append(l)
    lst1=[]
    for i in range(0,30):
        l=[]
        for j in range(0,10):
            l.append('CELL %03d/%03d'%(i,j))
        lst1.append(l)
    print "test assigning var 1"
    c1=time.clock()
    appl.setCells(10,2,lst)
    c2=time.clock()
    print "   ",c2-c1
    
    print "test assigning var 2"
    c1=time.clock()
    appl.setCells2(10,2,lst1)
    c2=time.clock()
    print "   ",c2-c1
    
    print "test assigning var 1"
    c1=time.clock()
    appl.setCells(10,2,lst)
    c2=time.clock()
    print "   ",c2-c1
    
    print "test assigning var 2"
    c1=time.clock()
    appl.setCells2(10,2,lst1)
    c2=time.clock()
    print "   ",c2-c1
    
    cr=appl.getCellRangeByName('f12','k20')
    print cr
    cr=appl.getCellRangeByName('f11','f11')
    print cr
    
def test2():
    appl=vtOfficeSCalc()
    #appl.openFile('/c|/temp/test.sxc')
    #appl.openFile('/C|/temp/test.sxc')
    #appl.openFile('file:///c|/temp/test.sxc')
    appl.openFile('c:/temp/test.sxc')
if __name__=="__main__":
    #print "started"
    #test()
    #test1()
    test2()