#----------------------------------------------------------------------------
# Name:         vtThread.py
# Purpose:
# Author:       Walter Obweger
#
# Created:      20060605
# CVS-ID:       $Id: vtThread.py,v 1.50 2015/02/27 20:24:53 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.vtThreadCore import vtThreadCore
from vidarc.tool.vtThreadMixinWX import vtThreadMixinWX
from vidarc.tool.vtThreadCore import _acquireThd
from vidarc.tool.vtThreadCore import _releaseThd
import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

wxEVT_VT_THREAD_FINISHED=wx.NewEventType()
vEVT_VT_THREAD_FINISHED=wx.PyEventBinder(wxEVT_VT_THREAD_FINISHED,1)
def EVT_VT_THREAD_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_VT_THREAD_FINISHED,func)
def EVT_VT_THREAD_FINISHED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,wxEVT_VT_THREAD_FINISHED,func)
class vtThreadFinished(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VT_THREAD_FINISHED(<widget_name>, self.OnFinished)
    """
    def __init__(self):
        if VERBOSE>20:
            vtLog.vtLngCS(vtLog.DEBUG,'vtThreadFinished','vtThread')
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VT_THREAD_FINISHED)
wxEVT_VT_THREAD_ABORTED=wx.NewEventType()
vEVT_VT_THREAD_ABORTED=wx.PyEventBinder(wxEVT_VT_THREAD_ABORTED,1)
def EVT_VT_THREAD_ABORTED(win,func):
    win.Connect(-1,-1,wxEVT_VT_THREAD_ABORTED,func)
def EVT_VT_THREAD_ABORTED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,wxEVT_VT_THREAD_ABORTED,func)
class vtThreadAborted(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VT_THREAD_ABORTED(<widget_name>, self.OnAborted)
    """
    def __init__(self):
        if VERBOSE>20:
            vtLog.vtLngCS(vtLog.DEBUG,'vtThreadAborted','vtThread')
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VT_THREAD_ABORTED)
wxEVT_VT_THREAD_PROC=wx.NewEventType()
vEVT_VT_THREAD_PROC=wx.PyEventBinder(wxEVT_VT_THREAD_PROC,1)
def EVT_VT_THREAD_PROC(win,func):
    win.Connect(-1,-1,wxEVT_VT_THREAD_PROC,func)
def EVT_VT_THREAD_PROC_DISCONNECT(win,func):
    win.Disconnect(-1,-1,wxEVT_VT_THREAD_PROC,func)
class vtThreadProc(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VT_THREAD_PROC(<widget_name>, self.OnProc)
    """
    def __init__(self,iPos,iCount=0):
        if VERBOSE>20:
            vtLog.vtLngCS(vtLog.DEBUG,'vtThreadProc','vtThread')
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VT_THREAD_PROC)
        self.iPos=iPos
        self.iCount=iCount
    def GetAct(self):
        return self.iPos
    def GetCount(self):
        return self.iCount
    def GetNormalized(self,iRange=1000):
        if self.iCount==0:
            return 0
        f=self.iPos/float(self.iCount)
        return long(f*iRange)

wxEVT_VT_THREAD_CALLBACK=wx.NewEventType()
vEVT_VT_THREAD_CALLBACK=wx.PyEventBinder(wxEVT_VT_THREAD_CALLBACK,1)
def EVT_VT_THREAD_CALLBACK(win,func):
    win.Connect(-1,-1,wxEVT_VT_THREAD_CALLBACK,func)
def EVT_VT_THREAD_CALLBACK_DISCONNECT(win,func):
    win.Disconnect(-1,-1,wxEVT_VT_THREAD_CALLBACK,func)
class vtThreadCallBack(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VT_THREAD_CALLBACK(<widget_name>, self.OnProc)
    """
    def __init__(self,func,*args,**kwargs):
        if VERBOSE>20:
            vtLog.vtLngCur(vtLog.DEBUG,'','vtThread')
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VT_THREAD_CALLBACK)
        self.func=func
        self.args=args
        self.kwargs=kwargs
    def DoCB(self):
        self.func(*self.args,**self.kwargs)

def _doCallBack(evt):
    evt.DoCB()
def CallBack(func,*args,**kwargs):
    try:
        _acquireThd()
        global _widCB
        if _widCB is None:
            if Install(bAcquire=False)==False:
                vtLog.vtLngCur(vtLog.CRITICAL,''%(),'vtThread')
                return
        try:
            if VERBOSE>0 and vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,
                        '%s;scheduled;args:%s;kwargs:%s'%(func.__name__,
                        vtLog.fmtLimit(args),vtLog.fmtLimit(kwargs)),
                        'vtThread')
            #elif vtLog.vtLngIsLogged(vtLog.INFO):
            #    vtLog.vtLngCur(vtLog.INFO,'%s;scheduled'%(func.__name__),'vtThread')
        except:
            vtLog.vtLngTB('vtThread')
        wx.PostEvent(_widCB,vtThreadCallBack(func,*args,**kwargs))
    finally:
        _releaseThd()

_widCB=None
def DeInstall():
    global _widCB
    vtLog.vtLngCur(vtLog.INFO,''%(),'vtThread')
    EVT_VT_THREAD_CALLBACK_DISCONNECT(_widCB,_doCallBack)
    _widCB=None
def Install(wid=None,bAcquire=True):
    try:
        if bAcquire:
            _acquireThd()
        global _widCB
        if _widCB is not None:
            vtLog.vtLngCur(vtLog.WARN,''%(),'vtThread')
            return True
        app=wx.GetApp()
        if app is None:
            vtLog.vtLngCur(vtLog.ERROR,'cannot be used without some wx application','vtThread')
            return False
        vtLog.vtLngCur(vtLog.INFO,''%(),'vtThread')
        _widCB=app
        EVT_VT_THREAD_CALLBACK(_widCB,_doCallBack)
        import vidarc
        vidarc.AddCleanUp(DeInstall)
        return True
    finally:
        if bAcquire:
            _releaseThd()
    vtLog.vtLngTB('vtThread')
    return False

class vtThread(vtLog.vtLogMixIn,vtThreadCore):
    def __init__(self,par,bPost=False,verbose=0,origin=None):
        self.par=par
        vtThreadCore.__init__(self,verbose=verbose,origin=origin)
        self.bPost=bPost
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'','del')
        try:
            #if self.IsRunning():
            #    self.Stop()
            #while self.IsRunning():
            #    self.__pauseSleep__()
            vtThreadCore.__del__(self)
        except:
            #vtLog.vtLngTB('del')
            pass
    def __origin__(self,origin):
        if origin is None:
            if self.par is not None:
                try:
                    self.originRaw=vtLog.vtLngGetRelevantNamesWX(self.par,self.__class__.__name__)
                except:
                    try:
                        baseWid=vtLog.GetPrintMsgWid(self.par)
                        if baseWid is not None:
                            self.originRaw=''.join([baseWid.GetName(),'.',par.GetName(),':',self.__class__.__name__])
                        else:
                            self.originRaw=''.join([self.par.GetName(),':',self.__class__.__name__])#':thread'
                    except:
                        vtLog.vtLngTB(self.__class__.__name__)
                        self.originRaw=self.__class__.__name__
            else:
                vtLog.vtLngCurCls(vtLog.WARN,'no parent defined and no origin given'%(),self)
                self.originRaw=self.__class__.__name__
        else:
            self.originRaw=origin
    def SetPostWid(self,par,bPost=False):
        vtLog.vtLngCur(vtLog.INFO,'par is None==%d'%(par is None),self.GetOrigin())
        self.par=par
        self.bPost=bPost
    def GetParent(self):
        return self.par
    def Do(self,func,*args,**kwargs):
        #try:
        #    if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #        vtLog.vtLngCur(vtLog.DEBUG,'status:%s;func;%s,args:%s;kwargs:%s'%(self.__str__(),
        #                    func,`args`,`kwargs`),self.GetOrigin())
        #except:
        #    self.__logTB__()
        try:
            if VERBOSE>0 and self.__isLogDebug__():
                self.__logDebug__('%s;scheduled;args:%s;kwargs:%s'%(
                        func.__name__,self.__logFmtLm__(args),
                        self.__logFmtLm__(kwargs)))
            #elif self.__isLogInfo__():
            #    self.__logInfo__('%s;scheduled'%(func.__name__))
        except:
            self.__logTB__()
        vtThreadCore.Do(self,func,*args,**kwargs)
    def Pause(self):
        vtThreadCore.Pause(self)
        if VERBOSE>=10:
            vtLog.vtLngCur(vtLog.INFO,'',self.GetOrigin())
    def Resume(self):
        vtThreadCore.Resume(self)
        if VERBOSE>=10:
            vtLog.vtLngCur(vtLog.INFO,'',self.GetOrigin())
    def Start(self):
        vtThreadCore.Start(self)
        if VERBOSE>=10:
            vtLog.vtLngCur(vtLog.INFO,'',self.GetOrigin())
    def Stop(self):
        vtThreadCore.Stop(self)
        if VERBOSE>=10:
            vtLog.vtLngCur(vtLog.INFO,'',self.GetOrigin())
    def __doPause__(self):
        """ 
        return True         ... stop processing
        return False        ... continue processing
        """
        if self.Is2Stop():
            vtLog.vtLngCur(vtLog.INFO,'Is2Stop',self.GetOrigin())
            return True
        if self.IsPause():
            self.__Paused__(True)
            if VERBOSE>=10:
                vtLog.vtLngCur(vtLog.INFO,'paused=True'%(),self.GetOrigin())
            while self.IsPause():
                self.__pauseSleep__()
                if VERBOSE>0:
                    vtLog.vtLngCur(vtLog.DEBUG,self.__str__(),self.GetOrigin())
                if self.Is2Stop():
                    vtLog.vtLngCur(vtLog.INFO,'Is2Stop',self.GetOrigin())
                    self.__Paused__(False)
                    return True
            self.__Paused__(False)
            if VERBOSE>=10:
                vtLog.vtLngCur(vtLog.INFO,'paused=False'%(),self.GetOrigin())
            if self.Is2Stop():
                vtLog.vtLngCur(vtLog.INFO,'Is2Stop',self.GetOrigin())
                return True
        return False
    def _doProc(self,t):
        if VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'proc'%(),self.GetOrigin())
        try:
            func,args,kwargs=t
            if func is not None:
                func(*args,**kwargs)
        except:
            vtLog.vtLngTB(self.origin)
        if VERBOSE>0:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'proc done'%(),self.GetOrigin())
    def Post(self,sEvent,*args,**kwargs):
        if self.par is None:
            return False
        if self.bPost==False:
            return False
        try:
            if sEvent=='proc':
                wx.PostEvent(self.par,vtThreadProc(*args,**kwargs))
            elif sEvent=='aborted':
                wx.PostEvent(self.par,vtThreadAborted(*args,**kwargs))
            elif sEvent=='finished':
                wx.PostEvent(self.par,vtThreadFinished(*args,**kwargs))
        except:
            return False
        return True
    def CallBack(self,func,*args,**kwargs):
        if 'thd' in kwargs:
            if kwargs['thd']=='self':
                kwargs['thd']=self
        try:
            if VERBOSE>0:
                if self.__isLogDebug__():
                    self.__logDebug__('%s;scheduled;args:%s;kwargs:%s'%(
                            func.__name__,self.__logFmtLm__(args),
                            self.__logFmtLm__(kwargs)))
                elif self.__isLogInfo__():
                    self.__logInfo__('%s;scheduled'%(func.__name__))
        except:
            self.__logTB__()
        try:
            _acquireThd()
            global _widCB
            if _widCB is None:
                if Install(bAcquire=False)==False:
                    vtLog.vtLngCur(vtLog.CRITICAL,''%(),'vtThread')
                    return
            try:
                if _widCB.IsMainLoopRunning():      # 10307:wro
                    wx.PostEvent(_widCB,vtThreadCallBack(func,*args,**kwargs))
                else:
                    func(*args,**kwargs)
            except:
                self.__logTB__()
                func(*args,**kwargs)
        finally:
            _releaseThd()
    def CallBackDelayed(self,zSleep,func,*args,**kwargs):
        try:
            if VERBOSE>0:
                if self.__isLogDebug__():
                    self.__logDebug__('%s;scheduled in %f;args:%s;kwargs:%s'%(
                            func.__name__,zSleep,self.__logFmtLm__(args),
                            self.__logFmtLm__(kwargs)))
                elif self.__isLogInfo__():
                    self.__logInfo__('%s;scheduled in %f'%(func.__name__,zSleep))
        except:
            self.__logTB__()
        try:
            self.CallBack(wx.FutureCall,int(zSleep*1000),func,*args,**kwargs)
        except:
            self.__logTB__()
    def DoCallBack(self,func,*args,**kwargs):
        try:
            if VERBOSE>0:
                if self.__isLogDebug__():
                    self.__logDebug__('%s;scheduled;args:%s;kwargs:%s'%(
                            func.__name__,self.__logFmtLm__(args),
                            self.__logFmtLm__(kwargs)))
                elif self.__isLogInfo__():
                    self.__logInfo__('%s;scheduled'%(func.__name__))
        except:
            self.__logTB__()
        self.Do(self.CallBack,func,*args,**kwargs)
    DoCB=DoCallBack
    def Branch(self,funcCB,branchOkCB,branchFltCB):
        func,args,kwargs=funcCB
        if 'thd' in kwargs:
            if kwargs['thd']=='self':
                kwargs['thd']=self
        try:
            if VERBOSE>5:
                if self.__isLogDebug__():
                    self.__logDebug__('%s;scheduled cb;args:%s;kwargs:%s'%(
                            func.__name__,self.__logFmtLm__(args),
                            self.__logFmtLm__(kwargs)))
                elif self.__isLogInfo__():
                    self.__logInfo__('%s;scheduled'%(func.__name__))
        except:
            self.__logTB__()
        ret=func(*args,**kwargs)
        if ret>=0:
            func,args,kwargs=branchOkCB
            self.CallBack(func,*args,**kwargs)
        else:
            if branchFltCB is None:
                self.DoCallBackBranch(funcCB,branchOkCB,branchFltCB)
            else:
                func,args,kwargs=branchFltCB
                self.CallBack(func,*args,**kwargs)
    def DoBranch(self,funcCB,branchOkCB,branchFltCB):
        func,args,kwargs=funcCB
        try:
            if VERBOSE>5:
                if self.__isLogDebug__():
                    self.__logDebug__('%s;scheduled;args:%s;kwargs:%s'%(
                            func.__name__,self.__logFmtLm__(args),
                            self.__logFmtLm__(kwargs)))
                elif self.__isLogInfo__():
                    self.__logInfo__('%s;scheduled'%(func.__name__))
        except:
            self.__logTB__()
        self.Do(self.Branch,funcCB,branchOkCB,branchFltCB)
    def CallBackBranch(self,funcCB,branchOkCB,branchFltCB):
        func,args,kwargs=funcCB
        try:
            if VERBOSE>5:
                if self.__isLogDebug__():
                    self.__logDebug__('%s;scheduled cb;args:%s;kwargs:%s'%(
                            func.__name__,self.__logFmtLm__(args),
                            self.__logFmtLm__(kwargs)))
                elif self.__isLogInfo__():
                    self.__logInfo__('%s;scheduled'%(func.__name__))
        except:
            self.__logTB__()
        ret=self.CallBack(func,*args,**kwargs)
        if ret>=0:
            func,args,kwargs=branchOkCB
            self.CallBack(func,*args,**kwargs)
        else:
            if branchFltCB is None:
                self.DoCallBackBranch(funcCB,branchOkCB,branchFltCB)
            else:
                func,args,kwargs=branchFltCB
                self.CallBack(func,*args,**kwargs)
    def DoCallBackBranch(self,funcCB,branchOkCB,branchFltCB):
        func,args,kwargs=funcCB
        try:
            if VERBOSE>5:
                if self.__isLogDebug__():
                    self.__logDebug__('%s;scheduled;args:%s;kwargs:%s'%(
                            func.__name__,self.__logFmtLm__(args),
                            self.__logFmtLm__(kwargs)))
                elif self.__isLogInfo__():
                    self.__logInfo__('%s;scheduled'%(func.__name__))
        except:
            self.__logTB__()
        self.Do(self.CallBackBranch,funcCB,branchOkCB,branchFltCB)
    def __run__(self):
        try:
            self.__runStart__()
            try:
                #while self.keepGoing:# and (self.qSched.empty()==False):
                while self.Is2Stop()==False:
                    if self.__doPause__():
                        break
                    else:
                        t=self.qSched.get(0)
                        self._doProc(t)
                self._acquire()
                self.running=False   # 070630:wro
                if self.keepGoing==False:
                    self.__clrSched__()
            except:
                self._acquire()
                ##self.keepGoing=
                self.running=False   # 070620:wro
                #if self.bPost:
                #    if self.keepGoing:
                #        wx.PostEvent(self.par,vtThreadFinished())
                #    else:
                #        wx.PostEvent(self.par,vtThreadAborted())
        except:
            self._acquire()
            #self.keepGoing=
            self.running=False    # 070620:wro
            vtLog.vtLngTB(self.origin)
        if self.bPost:
                if self.keepGoing:
                    wx.PostEvent(self.par,vtThreadFinished())
                else:
                    wx.PostEvent(self.par,vtThreadAborted())
        #self.running=False    # 070620:wro
        self.__runEnd__()
        self.__clrWakeUp__()
        #self.keepGoing=True
        self.running=False   # 070620:wro activated
        vtLog.vtLngCur(vtLog.INFO,'ident:%s running:%d keepgoing:%d'%(self.ident,self.running,self.keepGoing),
                                self.GetOrigin())
        self._release()
    def BindEvents(self,funcProc=None,funcAborted=None,funcFinished=None):
        if self.par is not None:
            if funcProc is not None:
                EVT_VT_THREAD_PROC(self.par,funcProc)
            if funcAborted is not None:
                EVT_VT_THREAD_ABORTED(self.par,funcAborted)
            if funcFinished is not None:
                EVT_VT_THREAD_FINISHED(self.par,funcFinished)
    def IsMainThread(self,bMain=True,bLog=False):
        if wx.Thread_IsMain()!=bMain:
            if bLog==True:
                if bMain:
                    self.__logCritical__('called by thread'%())
                else:
                    self.__logCritical__('called by main thread'%())
            return False
        return True

class vtThreadWX(vtThread,vtThreadMixinWX):
    def __init__(self,*args,**kwargs):
        vtThread.__init__(self,*args,**kwargs)
        vtThreadMixinWX.__init__(self)
