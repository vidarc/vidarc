#----------------------------------------------------------------------------
# Name:         vtgBmpStatic.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20100521
# CVS-ID:       $Id: vtgBmpStatic.py,v 1.1 2010/05/21 16:54:16 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------


import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import wx
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vtgBmpStatic(wx.PyPanel):
    def __init__(self, parent, id,
                key=[],
                bitmap=[],
                pos = wx.DefaultPosition,
                size = wx.DefaultSize,
                label = None,    # optional text to be displayed
                overlay = None,  # optional image to overlay on animation
                style = 0,       # window style
                name = "vtgBmpStatic",
                ):
        wx.PyPanel.__init__(self, parent, id, pos, size, style, name)
        self.name = name
        self._sLbl = label
        _seqTypes = (type([]), type(()))
        self._lKey=[s[:] for s in key]
        # set size, guessing if necessary
        width, height = size
        if width == -1:
            if type(bitmap) in _seqTypes:
                width = bitmap[0].GetWidth()
            else:
                if frameWidth:
                    width = frameWidth
        if height == -1:
            if type(bitmap) in _seqTypes:
                height = bitmap[0].GetHeight()
            else:
                height = bitmap.GetHeight()
        self.width, self.height = width, height

        # double check it
        assert width != -1 and height != -1, "Unable to guess size"

        if label:
            extentX, extentY = self.GetTextExtent(label)
            self._iLblX = (width - extentX)/2
            self._iLblY = (height - extentY)/2
        self._iCur = 0
        self._ovl = overlay
        if overlay is not None:
            self._ovl = overlay
            self._iOvlX = (width - self._ovl.GetWidth()) / 2
            self._iOvlY = (height - self._ovl.GetHeight()) / 2
        self._bOvl = overlay is not None
        self._bLbl = label is not None

        if type(bitmap) in _seqTypes:
            self._lBmp = bitmap
            self._iCnt = len(self._lBmp)
        else:
            assert 0,'bitmap must be list'
        self.SetClientSize((width, height))

        self.Bind(wx.EVT_PAINT, self.OnPaint)
    def DoGetBestSize(self):
        return (self.width, self.height)
    def Draw(self, dc):
        dc.DrawBitmap(self._lBmp[self._iCur], 0, 0, True)
        if self._ovl and self._bOvl:
            dc.DrawBitmap(self._ovl, self._iOvlX, self._iOvlY, True)
        if self._sLbl and self._bLbl:
            dc.DrawText(self._sLbl, self._iLblX, self._iLblY)
            dc.SetTextForeground(wx.WHITE)
            dc.DrawText(self._sLbl, self._iLblX-1, self._iLblY-1)
    def OnPaint(self, event):
        self.Draw(wx.PaintDC(self))
        event.Skip()
    # --------- public methods ---------
    def SetFont(self, font):
        """Set the font for the label"""
        wx.Panel.SetFont(self, font)
        self.SetLabel(self._sLbl)
        self.Draw(wx.ClientDC(self))
    def SetCurrent(self, current):
        """Set current image"""
        self._iCur = current
        self.Draw(wx.ClientDC(self))
    def ToggleOverlay(self, state = None):
        """Toggle the overlay image"""
        if state is None:
            self._bOvl = not self._bOvl
        else:
            self._bOvl = state
        self.Draw(wx.ClientDC(self))
    def ToggleLabel(self, state = None):
        """Toggle the label"""
        if state is None:
            self._bLbl = not self._bLbl
        else:
            self._bLbl = state
        self.Draw(wx.ClientDC(self))
    def SetLabel(self, label):
        """Change the text of the label"""
        self._sLbl = label
        if label:
            extentX, extentY = self.GetTextExtent(label)
            self._iLblX = (self.width - extentX)/2
            self._iLblY = (self.height - extentY)/2
        self.Draw(wx.ClientDC(self))
    def SetValue(self,val):
        if type(val)==type(''):
            try:
                iVal=self._lKey.index(val)
            except:
                iVal=0
            self._iCur=iVal
        else:
            self._iCur=val
        if self._iCur<0:
            self._iCur=0
        if self._iCur>=self._iCnt:
            self._iCur=self._iCnt-1
        self.Draw(wx.ClientDC(self))
        self.Refresh()

