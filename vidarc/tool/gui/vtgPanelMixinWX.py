#----------------------------------------------------------------------------
# Name:         vtgPanelMixinWX.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20100213
# CVS-ID:       $Id: vtgPanelMixinWX.py,v 1.6 2010/05/19 21:59:54 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import wx
    import vSystem
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
    
    from vidarc.tool.log.vtLog import vtLogOriginWX
    from vidarc.tool.gui.vtgEvent import vtgEvent
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

vtgxEVT_PANEL_CMD=wx.NewEventType()
def EVT_PANEL_CMD(win,func):
    win.Connect(-1,-1,vtgxEVT_PANEL_CMD,func)
def EVT_PANEL_CMD_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtgxEVT_PANEL_CMD,func)
class vtgxPanelCmd(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_PANEL_CMD(<widget_name>, xxx)
    """
    def __init__(self,obj,cmd,data=None):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtgxEVT_PANEL_CMD)
        self.cmd=cmd
        self.data=data
    def GetCmd(self):
        return self.cmd
    def GetData(self):
        return self.data

class vtgxPanelEvent(vtgEvent):
    _MAP_EVENT={
            'cmd':EVT_PANEL_CMD,
            }
    _MAP_EVENT_UNBIND={
            'cmd':EVT_PANEL_CMD_DISCONNECT,
            }

class vtgPanelMixinWX(vtgxPanelEvent,vtLogOriginWX):
    def __init__(self,bExtended=False):
        vtLogOriginWX.__init__(self,bExtended=bExtended)
    def SetCfgData(self):
        vtLogOriginWX.GetTopWidFunc(self,'SetCfgData')()
    def GetCfgData(self):
        vtLogOriginWX.GetTopWidFunc(self,'GetCfgData')()
    def SetCfgDft(self,dDft):
        vtLogOriginWX.GetTopWidFunc(self,'setCfgDft')(dDft)
    def BuildCfgDict(self):
        vtLogOriginWX.GetTopWidFunc(self,'buildCfgDict')()
    def SetCfgVal(self,v,lHier,d=None,fallback=None,funcConv=None,**kwargs):
        return vtLogOriginWX.GetTopWidFunc(self,'setCfgVal')(v,lHier,d=d,
                fallback=fallback,funcConv=funcConv,**kwargs)
    def GetCfgVal(self,lHier,d=None,fallback=None,funcConv=None,**kwargs):
        return vtLogOriginWX.GetTopWidFunc(self,'getCfgVal')(lHier,d=d,
                fallback=fallback,funcConv=funcConv,**kwargs)
    def __do_call__(self,f,*args,**kwargs):
        if VERBOSE>5:
            self.__logDebug__(''%())
        l=[]
        for k,o in self.__dict__.iteritems():
            if k in ['originLogging','_originPrint','widLogging']:
                continue
            if hasattr(o,f):
                l.append(getattr(o,f)(*args,**kwargs))
        return l
        return [getattr(o,f)(*args,**kwargs) for k,o in self.__dict__.iteritems() if k not in ['originLogging',''] and hasattr(o,f)]
        for o in self.__dict__.itervalues():
            try:
                if hasattr(o,f):
                    func=getattr(o,f)
                    func(o,*args,**kwargs)
            except:
                self.__logTB__()
        if VERBOSE>5:
            self.__logDebug__(''%())
    def Post(self,cmd,data=None):
        wx.PostEvent(self,vtgxPanelCmd(self,cmd,data=data))
    def IsMainThread(self,bMain=True):
        if wx.Thread_IsMain()!=bMain:
            if bMain:
                self.__logCritical__('called by thread'%())
            else:
                self.__logCritical__('called by main thread'%())
            return False
        return True
    def HandleKey(self,evt,bRetDict=False):
        evt.Skip()
        iCode=-1
        bAlt=False
        bCtrl=False
        bMeta=False
        bShift=False
        try:
            cVal=u'%c'%evt.GetUnicodeKey()
            iCode=evt.GetKeyCode()
            bAlt=evt.AltDown()
            bCtrl=evt.ControlDown()
            bMeta=evt.MetaDown()
            bShift=evt.ControlDown()
        except:
            self.__logTB__()
        if bRetDict==True:
            return {'v':cVal,'code':iCode,'shift':bShift,'alt':bAlt,'ctrl':bCtrl,'meta':bMeta}
    def GetFontByName(self,sName):
        """
        sName
            "fixed"     ... 
        """
        try:
            iSz=8
            sFont='Tahoma'
            iWeight=wx.FONTWEIGHT_NORMAL
            
            if vSystem.isWin():
                if sName=='fixed':
                    iSz=8
                    sFont='Courier New'
                    iWeight=wx.FONTWEIGHT_NORMAL
            elif vSystem.isUnx():
                if sName=='fixed':
                    iSz=8
                    sFont='Helvetica'
                    iWeight=wx.FONTWEIGHT_NORMAL
            if VERBOSE>0:
                self.__logDebug__('font:%s;size:%d;weight:%d'%(sFont,iSz,iWeight))
            font=wx.Font(iSz,wx.FONTFAMILY_SWISS,wx.FONTSTYLE_NORMAL,
                        iWeight,False,sFont,
                        wx.FONTENCODING_ISO8859_1)
            return font
        except:
            self.__logTB__()
        return None
