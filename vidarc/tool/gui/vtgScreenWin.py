# -*- coding: iso-8859-1 -*-
#----------------------------------------------------------------------
# Name:         vtgScreenWin.py
# Purpose:      screen functions for Windows Platform
#
# Author:       Walter Obweger
#
# Created:      20100226
# CVS-ID:       $Id: vtgScreenWin.py,v 1.1 2010/02/26 18:15:28 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import sys

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
VERBOSE=10
import win32api
import win32gui
import win32con
def getScreenDict(sScreen=None,thd=None):
    bLoop=True
    iMode=0
    dRes={'taskbar':None}
    try:
        bDbg=False
        if VERBOSE>5:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                bDbg=True
        hWin=win32gui.FindWindow('Shell_TrayWnd',None)
        if hWin is None:
            dTask=None
        else:
            #t=win32gui.GetClientRect(hWin)
            t=win32gui.GetWindowRect(hWin)
            dTask={
                    'x':t[0],'y':t[1],
                    'width':t[2]-t[0],
                    'height':t[3]-t[1],
                    'iXa':t[0],'iYa':t[1],
                    'iXe':t[2],'iYe':t[3],
                    }
            dTask['edges']=[
                        (t[0],t[1]),
                        (t[0]+dTask['width'],t[1]),
                        (t[0]+dTask['width'],t[1]+dTask['height']),
                        (t[0],t[1]+dTask['height']),
                        ]
            if bDbg==True:
                vtLog.vtLngCur(vtLog.DEBUG,'dTask:%s'%(vtLog.pformat(dTask)),__name__)
        iScr=0
        iIdx=0
        while bLoop==True:
            try:
                it=win32api.EnumDisplayDevices(sScreen,iIdx,iMode)
                if (it.StateFlags&win32con.DISPLAY_DEVICE_ATTACHED_TO_DESKTOP)!=0:
                    itCfg=win32api.EnumDisplaySettings(it.DeviceName,win32con.ENUM_CURRENT_SETTINGS)
                    bPrimary=(it.StateFlags&win32con.DISPLAY_DEVICE_PRIMARY_DEVICE)!=0
                    dScr={
                            'x':itCfg.Position_x,'y':itCfg.Position_y,
                            'width':itCfg.PelsWidth,'height':itCfg.PelsHeight,
                            'pos':(itCfg.Position_x,itCfg.Position_y),
                            'size':(itCfg.PelsWidth,itCfg.PelsHeight),
                            'name':it.DeviceName,
                            'bPrimary':bPrimary,
                            }
                    dScr['iXa']=dScr['x']
                    dScr['iYa']=dScr['y']
                    dScr['iXe']=dScr['x']+dScr['width']
                    dScr['iYe']=dScr['y']+dScr['height']
                    def inside(e,x,y):
                        if (x>=e[0][0]) and (x<=e[2][0]):
                            if (y>=e[0][1]) and (y<=e[2][1]):
                                return True
                        return False
                    if bDbg==True:
                        vtLog.vtLngCur(vtLog.DEBUG,'dScr:%s'%(vtLog.pformat(dScr)),__name__)
                    
                    if inside(dTask['edges'],dScr['iXa'],dScr['iYa']):
                        if inside(dTask['edges'],dScr['iXa'],dScr['iYe']):
                        #if inside(dTask['edges'],dScr['iXa']):
                        #if inside(dScr['iYa'],dTask['iYa'],dTask['iYe']):
                            dScr['iXa']=dTask['iXe']
                            if bDbg==True:
                                vtLog.vtLngCur(vtLog.DEBUG,'mod left;dScr:%s'%(vtLog.pformat(dScr)),__name__)
                        if inside(dTask['edges'],dScr['iXe'],dScr['iYa']):
                            dScr['iYa']=dTask['iYe']
                            if bDbg==True:
                                vtLog.vtLngCur(vtLog.DEBUG,'mod top;dScr:%s'%(vtLog.pformat(dScr)),__name__)
                    if inside(dTask['edges'],dScr['iXa'],dScr['iYe']):
                        if inside(dTask['edges'],dScr['iXe'],dScr['iYe']):
                            dScr['iYe']=dTask['iYa']
                            if bDbg==True:
                                vtLog.vtLngCur(vtLog.DEBUG,'mod bottom;dScr:%s'%(vtLog.pformat(dScr)),__name__)
                    if inside(dTask['edges'],dScr['iXe'],dScr['iYa']):
                        if inside(dTask['edges'],dScr['iXe'],dScr['iYe']):
                            dScr['iXe']=dTask['iXa']
                            if bDbg==True:
                                vtLog.vtLngCur(vtLog.DEBUG,'mod right;dScr:%s'%(vtLog.pformat(dScr)),__name__)
                    
                    dRes[iScr]=dScr
                    #if bPrimary==True:
                    #    dRes[None]=dScr
                    iScr+=1
                iIdx+=1
            except:
                bLoop=False
        for iIdx in xrange(iScr):
            dScr=dRes[iIdx]
            if iIdx==0:
                dDeskTop={'iXa':dScr['iXa'],'iXe':dScr['iXe'],
                        'iYa':dScr['iYa'],'iYe':dScr['iYe']}
                dRes[None]=dDeskTop
            else:
                dDeskTop['iXa']=min(dScr['iXa'],dDeskTop['iXa'])
                dDeskTop['iYa']=min(dScr['iYa'],dDeskTop['iYa'])
                dDeskTop['iXe']=max(dScr['iXe'],dDeskTop['iXe'])
                dDeskTop['iYe']=max(dScr['iYe'],dDeskTop['iYe'])
        dRes['taskbar']=dTask
        if bDbg==True:
            vtLog.vtLngCur(vtLog.DEBUG,'dRes:%s'%(vtLog.pformat(dRes)),__name__)
        return dRes
    except:
        vtLog.vtLngTB(__name__)
    return None
def getScreenSettingLst(sScreen=None):
    iMode=win32con.ENUM_CURRENT_SETTINGS
    lRes=[]
    try:
        l=win32api.EnumDisplaySettings(sScreen,iMode)
        if VERBOSE>5:
            vtLog.vtLngCur(vtLog.DEBUG,'%s'%(vtLog.pformat(l)),__name__)
        for it in l:
            pass
        return lRes
    except:
        vtLog.vtLngTB(__name__)
    return None
