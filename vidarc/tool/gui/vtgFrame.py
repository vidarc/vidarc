#----------------------------------------------------------------------------
# Name:         vtgFrame.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20091101
# CVS-ID:       $Id: vtgFrame.py,v 1.11 2012/12/22 11:34:39 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import types,os

import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase
from vtgFrmMixin import vtgFrmMixin
from vtgCore import vtGuiCoreArrangeWidget
import vLogFallBack
import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

def create(parent,title,pnCls,name='vtgFrame',
            pos=None,sz=None,
            pnPos=wx.DefaultPosition,pnSz=wx.DefaultSize,pnStyle=0,
            pnName='pn',bmp=None,bToolBar=False,bStatusBar=False,
            lMnCfg=None):
    return vtgFrame(parent,title,pnCls,name=name,pos=pos,sz=sz,
            pnPos=nPos,pnSz=pnSz,pnStyle=pnStyle,pnName=pnName,
            bmp=bmp,bToolBar=bToolBar,bStatusBar=bStatusBar,
            lMnCfg=lMnCfg)

class vtgFrame(wx.Frame,vtgFrmMixin):
    def __init__(self, parent,title,pnCls,name='vtgFrame',
                pos=None,sz=None,iArrange=-1,widArrange=None,
                pnPos=wx.DefaultPosition,pnSz=wx.DefaultSize,pnStyle=0,
                pnName='pn',bmp=None,bToolBar=False,bStatusBar=False,
                lMnCfg=None,**kwargs):
        global _
        _=vtLgBase.assignPluginLang('vtGui')
        wx.Frame.__init__(self, id=wx.NewId(),
              name=u''.join([name,'Frame']), parent=parent, 
              pos=wx.Point(0,0), size=wx.Size(100, 100),
              style=wx.DEFAULT_FRAME_STYLE | wx.RESIZE_BORDER,
              title=title)
        self.pn=None
        #self.SetClientSize(wx.Size(392, 393))
        self.Bind(wx.EVT_CLOSE, self.OnFrameClose)
        self._bLayout=False

        self.pnMain = wx.Panel(id=wx.NewId(),
              name='pnMain', parent=self, 
              #pos=wx.Point(0, 0), size=wx.Size(392,223), 
              style=wx.TAB_TRAVERSAL)
        #self._init_ctrls(parent,,title)
        self.pnMain.AddMenu=self.AddMenu
        self.pnMain.AddToolBar=self.AddToolBar
        self.pnMain.EnableTool=self.EnableTool
        
        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.gbsData = wx.GridBagSizer(hgap=0, vgap=0)
        self.gbsData.AddSizer(self.bxsBt, (1, 0), border=4,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER, span=(1, 1))
        
        vtgFrmMixin.__init__(self)
        if bmp is None:
            icon=vtArt.getIcon(vtArt.getBitmap(vtArt.Mod))
        else:
            icon=vtArt.getIcon(bmp)
        self.SetIcon(icon)
        try:
            self.gbsData.AddGrowableCol(0)
            self.gbsData.AddGrowableRow(0)
            self.pn=pnCls(self.pnMain,id=wx.NewId(),pos=pnPos,size=pnSz,
                        style=pnStyle,name=pnName,**kwargs)
            self.gbsData.AddWindow(self.pn, (0, 0), border=4,
                  flag=wx.BOTTOM | wx.TOP | wx.LEFT | wx.RIGHT | wx.EXPAND, span=(1, 1))
            self.gbsData.Layout()
            self.gbsData.Fit(self)
        except:
            self.__logTB__()
        iId=wx.NewId()
        self.cbClose = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Close),
              id=iId, label=_(u'Close'),
              name=u'cbClose', parent=self.pnMain, pos=wx.Point(0, 24),
              size=wx.Size(76, 30), style=0)
        self.cbClose.Bind(wx.EVT_BUTTON, self.OnCbCloseButton,id=iId)
        self.bxsBt.AddWindow(self.cbClose, 0, border=0, flag=0)
        self.pnMain.SetSizer(self.gbsData)
        
        if lMnCfg is not None:
            self.AddMenu(0,lMnCfg,None)
        if iArrange>=0:
            if widArrange is not None:
                vtGuiCoreArrangeWidget(widArrange,self,iArrange)
            else:
                vtGuiCoreArrangeWidget(parent,self,iArrange)
            #self.gbsData.Layout()
        else:
            if sz is not None:
                self.SetSize(sz)
            else:
                #self.Fit()
                self.gbsData.Layout()
                self.gbsData.Fit(self)
                self._bLayout=True
            if pos is not None:
                self.Move(pos)
            else:
                if parent is not None:
                    self.Centre()
    def SetSize(self,sz):
        try:
            self._bLayout=False
            wx.Frame.SetSize(self,sz)
        except:
            self.__logTB__()
    def OnCbCloseButton(self, event):
        event.Skip()
        try:
            self.Show(False)
        except:
            self.__logTB__()
    def OnFrameClose(self,evt):
        try:
            self.Show(False)
        except:
            self.__logTB__()
    def Show(self,bFlag=True):
        try:
            wx.Frame.Show(self,bFlag)
            if bFlag==True:
                self.SetFocus()
        except:
            self.__logTB__()
    def SetFocus(self):
        try:
            if hasattr(self,'cbClose'):
                self.cbClose.SetFocus()
            else:
                wx.Frame.SetFocus(self)
        except:
            self.__logTB__()
    def __getattr__(self,name):
        try:
            if hasattr(self.pn,name):
                return getattr(self.pn,name)
        except:
            #vLogFallBack.logTB(__name__)
            if VERBOSE>10:
                if name!='pn':
                    vLogFallBack.logError('name:%s'%(name),__name__)
        raise AttributeError(name)
