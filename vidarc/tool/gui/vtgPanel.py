#----------------------------------------------------------------------------
# Name:         vtgPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20100214
# CVS-ID:       $Id: vtgPanel.py,v 1.13 2012/01/15 13:48:47 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import types
    import wx
    import wx.lib.buttons
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
    #from vidarc.tool.log.vtLog import vtLogOriginWX
    from vidarc.tool.gui.vtgPanelMixinWX import vtgPanelMixinWX
    from vidarc.tool.misc.vtmMsgDialog import vtmMsgDialog
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

CACHE_ID={}

def getWidId(par,sN):
    global CACHE_ID
    try:
        kPar=par.GetName()#par.GetId()
        if kPar in CACHE_ID:
            d=CACHE_ID[kPar]
        else:
            d={}
            CACHE_ID[kPar]=d
        if sN in d:
            return d[sN]
        else:
            iId=wx.NewId()
            d[sN]=iId
            return iId
    except:
        vLogFallBack.logTB(__name__)
        try:
            par.__logTB__()
        except:
            vLogFallBack.logTB(__name__)
    return wx.NewId
class vtgPanel(wx.Panel,vtgPanelMixinWX):
    SIZE_DFT=(100,34)
    YESNO=0x1
    OK=0x2
    OK=0x4
    OPEN=0x10
    SAVE=0x20
    MULTIPLE=0x20
    EXCLAMATION=0x100
    ERROR=0x200
    INFORMATION=0x300
    QUESTION=0x400
    FLEX_SIZER=0
    def __init__(self,*args,**kwargs):
        _kwargs={}
        for s in ['id','name','parent','pos','size','style']:
            if s in kwargs:
                _kwargs[s]=kwargs[s]
        if 'size' not in _kwargs:
            sz=self.SIZE_DFT
            _kwargs['size']=sz
        else:
            sz=_kwargs['size']
        if 'id' not in _kwargs:
            iId=wx.NewId()
            _kwargs['id']=iId
        else:
            iId=_kwargs['id']
        if 'style' not in _kwargs:
            _kwargs['style']=wx.TAB_TRAVERSAL
        else:
            if _kwargs['style'] is None:
                _kwargs['style']=wx.TAB_TRAVERSAL
        bDbg=False
        if VERBOSE>0:
            if self.__isLogDebug__():
                bDbg=True
        apply(wx.Panel.__init__,(self,) + args,_kwargs)
        vtgPanelMixinWX.__init__(self)#,bExtended=True)
        if bDbg:
            self.__logDebug__([kwargs,_kwargs])
        if kwargs.get('bFlexGridSizer',self.FLEX_SIZER)==0:
            self.gbsData = wx.GridBagSizer(hgap=4, vgap=4)
        else:
            if type(self.FLEX_SIZER)==types.TupleType:
                self.gbsData = wx.FlexGridSizer(cols=self.FLEX_SIZER[1],rows=self.FLEX_SIZER[0],hgap=4, vgap=4)
            else:
                self.gbsData = wx.FlexGridSizer(cols=self.FLEX_SIZER,rows=1,hgap=4, vgap=4)
        
        self.bMod=False
        self.bBlk=False
        self.bEnMark=kwargs.get('bEnMark',True)
        self.bEnMod=kwargs.get('bEnMod',True)
        self.bLogRelWid=kwargs.get('bLogRelWid',True)
        self.lWid=[]
        
        self.IsMainThread()
        lGrowRow,lGrowCol=self.__initCtrl__(*args,**kwargs)
        if lGrowRow is None:
            if 'lGrowRow' in kwargs:
                lGrowRow=kwargs['lGrowRow']
        if lGrowCol is None:
            if 'lGrowCol' in kwargs:
                lGrowCol=kwargs['lGrowCol']
        if lGrowRow is not None:
            for t in lGrowRow:
                self.gbsData.AddGrowableRow(t[0],t[1])
        if lGrowCol is not None:
            for t in lGrowCol:
                self.gbsData.AddGrowableCol(t[0],t[1])
        self.SetSizer(self.gbsData)
        if (lGrowRow is not None) and (lGrowCol is not None):
            self.gbsData.Layout()
            self.gbsData.Fit(self)
        if bDbg:
            self.__logDebug__([lGrowRow,lGrowCol,
                'pos',self.GetPosition(),'size',self.GetSize()])
    def IsLogRelevant(self):
        return self.bLogRelWid
    def __initCtrl__(self,*args,**kwargs):
        """ override this
          return lGrowRow,lGrowCol
          lGrowRow=[] or [(1,2),(2,1)] or None
          lGrowCol=[] or [(1,2),(2,1)] or None
          tuple[0] ... number row or col
          tuple[1] ... proportion
          if both or None no layout is calculated
        """
        try:
            if VERBOSE>0:
                self.__logDebug__('started')
            if 'lWid' in kwargs:
                self.__add_widget__(kwargs['lWid'])
        except:
            self.__logTB__()
        if VERBOSE>0:
            self.__logDebug__('done')
        return None,None
    def __get_widget_map__(self):
        dEvtWid={'char':wx.EVT_CHAR,'char_hook':wx.EVT_CHAR_HOOK,
            'keyDn':wx.EVT_KEY_DOWN,'keyUp':wx.EVT_KEY_UP,
            'mouse':wx.EVT_MOUSE_EVENTS,'wheel':wx.EVT_MOUSEWHEEL,
            'rgDbl':wx.EVT_RIGHT_DCLICK,'mdDbl':wx.EVT_MIDDLE_DCLICK,
            'lfDbl':wx.EVT_LEFT_DCLICK,
            'motion':wx.EVT_MOTION,
            'rgDn':wx.EVT_RIGHT_DOWN,'rgUp':wx.EVT_RIGHT_UP,
            'mdUp':wx.EVT_MIDDLE_UP,'mdDn':wx.EVT_MIDDLE_DOWN,
            'lfUp':wx.EVT_LEFT_UP,'lfDn':wx.EVT_LEFT_DOWN,
            'enter':wx.EVT_ENTER_WINDOW,'leave':wx.EVT_LEAVE_WINDOW,
            'move':wx.EVT_MOVE,'size':wx.EVT_SIZE,
            'erase':wx.EVT_ERASE_BACKGROUND,'paint':wx.EVT_PAINT,
            'focusKill':wx.EVT_KILL_FOCUS,'focusSet':wx.EVT_SET_FOCUS,
            'help':wx.EVT_HELP,'colorChanged':wx.EVT_SYS_COLOUR_CHANGED
            }
        dEvtDlg={
            'idle':wx.EVT_IDLE,'navKey':wx.EVT_NAVIGATION_KEY,
            'iconize':wx.EVT_ICONIZE,'maximize':wx.EVT_MAXIMIZE,
            'dropFiles':wx.EVT_DROP_FILES,'close':wx.EVT_CLOSE,
            'activate':wx.EVT_ACTIVATE,'init':wx.EVT_INIT_DIALOG,
            }
        
        return dEvtWid,{
            'lblLf':{
                'cls':  wx.StaticText,
                'kw':   {'style':wx.ALIGN_LEFT},
                'flag': wx.EXPAND,
                },
            'lblRg':{
                'cls':  wx.StaticText,
                'kw':   {'style':wx.ALIGN_RIGHT},
                'flag': wx.EXPAND,
                },
            'lblCt':{
                'cls':  wx.StaticText,
                'kw':   {'style':wx.ALIGN_CENTRE},
                'flag': wx.EXPAND,
                },
            'lbl':{
                'cls':  wx.StaticText,
                'kw':   {'style':wx.ALIGN_RIGHT},
                'flag': wx.EXPAND,
                },
            'lblBx':{
                'cls':  wx.StaticBox,
                'kw':   {'style':wx.ALIGN_RIGHT},
                'flag': wx.EXPAND,
                },
            'lblBmp':{
                'cls':   wx.lib.buttons.GenBitmapButton,
                'kw':   {'style':wx.NO_3D | wx.NO_BORDER},
                'evt':  {'btn':wx.EVT_BUTTON},
                'mark': False,
                #'mod':  ['btn'],
                },
            'cbBmp':{
                'cls':   wx.lib.buttons.GenBitmapButton,
                'kw':   {},#{'bitmap':wx.EmptyButton(16,16)},
                'evt':  {'btn':wx.EVT_BUTTON},
                'mark': False,
                #'mod':  ['btn'],
                },
            'cbBmpLbl':{
                'cls':  wx.lib.buttons.GenBitmapTextButton,
                'kw':   {},#{'bitmap':wx.EmptyButton(16,16)},
                'evt':  {'btn':wx.EVT_BUTTON},
                'mark': True,
                #'mod':  ['btn'],
                },
            'chc':{
                'cls':  wx.Choice,
                'kw':   {'choices':[]},
                'evt':  {'choice':wx.EVT_CHOICE},
                'flag': wx.EXPAND,
                'mark': True,
                'mod':  ['choice'],
                },
            'chk':{
                'cls':  wx.CheckBox,
                'kw':   {'style':0},
                'evt':  {'check':wx.EVT_CHECKBOX},
                'flag': wx.EXPAND,
                'mark': True,
                'mod':  ['check'],
                },
            'chkLst':{
                'cls':  wx.CheckListBox,
                'kw':   {'choices':[]},
                'evt':  {'check':wx.EVT_CHECKLISTBOX},
                'flag': wx.EXPAND,
                'mark': True,
                'mod':  ['check'],
                },
            'lst':{
                'cls':  wx.ListBox,
                'kw':   {'style':0,'size':(100,100)},
                'evt':  {'sel':wx.EVT_LISTBOX,'dblclk':wx.EVT_LISTBOX_DCLICK},
                'flag': wx.EXPAND,
                'mark': True,
                'mod':  ['sel'],
                },
            'lstMulti':{
                'cls':  wx.ListBox,
                'kw':   {'style':wx.LB_MULTIPLE,'size':(100,100)},
                'evt':  {'sel':wx.EVT_LISTBOX,'dblclk':wx.EVT_LISTBOX_DCLICK},
                'flag': wx.EXPAND,
                'mark': True,
                'mod':  ['sel'],
                },
            'lstExt':{
                'cls':  wx.ListCtrl,
                'kw':   {'style':wx.LC_REPORT},
                'flag': wx.EXPAND,
                'mark': True,
                'mod':  ['check'],
                },
            'tgBmp':{
                'cls':  wx.lib.buttons.GenBitmapToggleButton,
                'kw':   {},
                'evt':  {'btn':wx.EVT_BUTTON},
                #'mark': True,
                'mod':  ['btn'],
                },
            'tgBmpLbl':{
                'cls':  wx.lib.buttons.GenBitmapTextToggleButton,
                'kw':   {},#{'bitmap':wx.EmptyButton(16,16)},
                'evt':  {'btn':wx.EVT_BUTTON},
                'flag': wx.EXPAND,
                'mark': True,
                'mod':  ['btn'],
                },
            'tr':{
                'cls':  wx.TreeCtrl,
                'kw':   {'style':wx.TR_HAS_BUTTONS},
                'flag': wx.EXPAND,
                'mark': True,
                'mod':  ['check'],
                },
            'txt':{
                'cls':  wx.TextCtrl,
                'kw':   {'style':0},
                'evt':  {'txt':wx.EVT_TEXT,'ent':wx.EVT_TEXT_ENTER},
                'flag': wx.EXPAND,
                'mark': True,
                'mod':  ['txt'],
                },
            'txtEnt':{
                'cls':  wx.TextCtrl,
                'kw':   {'style': wx.TE_PROCESS_ENTER},
                'evt':  {'txt':wx.EVT_TEXT,'ent':wx.EVT_TEXT_ENTER},
                'flag': wx.EXPAND,
                'mark': True,
                'mod':  ['txt'],
                },
            'txtPwd':{
                'cls':  wx.TextCtrl,
                'kw':   {'style':wx.TE_PASSWORD},
                'evt':  {'txt':wx.EVT_TEXT,'ent':wx.EVT_TEXT_ENTER},
                'flag': wx.EXPAND,
                'mark': True,
                'mod':  ['txt'],
                },
            'txtPwdEnt':{
                'cls':  wx.TextCtrl,
                'kw':   {'style':wx.TE_PASSWORD | wx.TE_PROCESS_ENTER},
                'evt':  {'txt':wx.EVT_TEXT,'ent':wx.EVT_TEXT_ENTER},
                'flag': wx.EXPAND,
                'mark': True,
                'mod':  ['txt'],
                },
            'txtLn':{
                'cls':  wx.TextCtrl,
                'kw':   {'style':wx.TE_MULTILINE },
                'evt':  {'txt':wx.EVT_TEXT,'ent':wx.EVT_TEXT_ENTER},
                'flag': wx.EXPAND,
                'mark': True,
                'mod':  ['txt'],
                },
            'txtRd':{
                'cls':  wx.TextCtrl,
                'kw':   {'style':wx.TE_READONLY},# | wx.TE_PROCESS_ENTER},
                #'evt':  {'txt':wx.EVT_TEXT,'ent':wx.EVT_TEXT_ENTER},
                'flag': wx.EXPAND,
                'mark': False,
                #'mod':  ['txt'],
                },
            'txtRdWrp':{
                'cls':  wx.TextCtrl,
                'kw':   {'style':wx.TE_READONLY|wx.TE_WORDWRAP},# | wx.TE_PROCESS_ENTER},
                #'evt':  {'txt':wx.EVT_TEXT,'ent':wx.EVT_TEXT_ENTER},
                'flag': wx.EXPAND,
                'mark': False,
                #'mod':  ['txt'],
                },
            'txtRdRg':{
                'cls':  wx.TextCtrl,
                'kw':   {'style':wx.TE_READONLY|wx.ALIGN_RIGHT},# | wx.TE_PROCESS_ENTER},
                #'evt':  {'txt':wx.EVT_TEXT,'ent':wx.EVT_TEXT_ENTER},
                'flag': wx.EXPAND,
                'mark': False,
                #'mod':  ['txt'],
                },
            'szFlex':{
                'cls':  wx.FlexGridSizer,
                'kw':   {'cols':1,'rows':1,'hgap':4,'vgap':0},
                'flag': wx.EXPAND,
                'mark': False,
                'sizer':True,
                },
            'szBoxHor':{
                'cls':  wx.BoxSizer,
                'kw':   {'orient':wx.HORIZONTAL},
                'flag': wx.EXPAND|wx.RIGHT,
                'mark': False,
                'sizer':True,
                },
            'szBoxVert':{
                'cls':  wx.BoxSizer,
                'kw':   {'orient':wx.VERTICAL},
                'flag': wx.EXPAND|wx.BOTTOM,
                'mark': False,
                'sizer':True,
                },
            'spInt':{
                'cls':  wx.SpinCtrl,
                'kw':   {'style':wx.SP_ARROW_KEYS,'min':0,'max':100,'initial':0},
                'flag': wx.EXPAND,
                'mark': True,
                'mod':  ['check'],
                },
            }
    def AddWidget(self,sK,pos,span,kwargs,evt,tSz=None,tDef=None,bDbg=False):
        try:
            if tDef is None:
                dEvtWid,dMap=self.__get_widget_map__()
            else:
                dEvtWid,dMap=tDef
            if sK is None:
                if bDbg==True:
                    self.__logDebug__(['sK',sK,'pos',pos,'span',span,
                            'kwargs',kwargs,'tSz',tSz,'flexsizer',self.FLEX_SIZER])
                if self.FLEX_SIZER==0:
                    if type(pos)==types.TupleType:
                        if tSz is None:
                            if type(span)==types.TupleType:
                                self.gbsData.AddSpacer(span,pos, border=0, flag=0,span=(1,1))
                            else:
                                self.gbsData.AddSpacer((span,span),pos, border=0, flag=0,span=(1,1))
                        else:
                            oSz,iFlag=tSz
                            oSz.AddSpacer(pos, border=span, flag=0)
                    pass
                else:
                    if type(pos)==types.TupleType:
                        if tSz is None:
                            self.gbsData.AddSpacer(pos, border=span, flag=0)
                        else:
                            oSz,iFlag=tSz
                            oSz.AddSpacer(pos, border=span, flag=0)
                return
            if 'name' not in kwargs:
                self.__logError__('name unknown;pos:%s;span:%s;kwargs:%s'%(sK,
                    self.__logFmt__(pos),self.__logFmt__(span),self.__logFmt__(kwargs)))
                return
            
            t=type(sK)
            if sK in dMap:
                dW=dMap[sK]
                cls=dW['cls']
                kw=dW['kw'].copy()
            elif t in [types.ClassType,types.TypeType]:
                cls=sK
                kw={}
                dW={'flag': wx.EXPAND,
                    'mark': True,
                    }
            else:
                dW=None
                cls=None
            if bDbg==True:
                self.__logDebug__(['sK',sK,'pos',pos,'span',span,
                        'kwargs',kwargs,'t',t,'dW',dW,'cls',cls])
            if cls is not None:
                if 'sizer' in dW:
                    kw.update(kwargs)
                    kkw=kw.copy()
                    del kkw['name']
                    if bDbg==True:
                        self.__logDebug__(['kkw',kkw])
                    w=cls(**kkw)
                    setattr(self,kwargs['name'],w)
                    if tSz is None:
                        if self.FLEX_SIZER==0:
                            self.gbsData.AddSizer(w, pos,span, flag=dW.get('flag',wx.ALIGN_CENTRE))
                        else:
                            self.gbsData.AddSizer(w, pos,border=span, flag=dW.get('flag',wx.ALIGN_CENTRE))
                        #self.gbsData.Add(w, 
                        #    wx.GBPosition(pos[0],pos[1]),wx.GBSpan(span[0],span[1]),
                        #    flag=dW.get('flag',wx.ALIGN_CENTRE))
                    else:
                        oSz,iFlag=tSz
                        oSz.AddSizer(w,pos,border=span, flag=dW.get('flag',wx.ALIGN_CENTRE)|iFlag)
                    for it in evt:
                        self.AddWidget(it[0],it[1],it[2],it[3],it[4],
                                tSz=(w,dW.get('flag',wx.ALIGN_CENTRE)),
                                tDef=(dEvtWid,dMap),bDbg=bDbg)
                    return w
                kw.update(kwargs)
                if 'id' not in kw:
                    #iId=wx.NewId()
                    iId=getWidId(self,kwargs['name'])
                    kw['id']=iId
                if 'tip' in kw:
                    sTip=kw['tip']
                    del kw['tip']
                else:
                    sTip=None
                kw['parent']=self
                if bDbg==True:
                    self.__logDebug__(['kw',kw])
                w=cls(**kw)
                if sTip is not None:
                    if hasattr(w,'SetToolTipString'):
                        w.SetToolTipString(sTip)
                        setattr(w,'_tip',sTip)
                setattr(self,kwargs['name'],w)
                if tSz is None:
                    if self.FLEX_SIZER==0:
                        self.gbsData.AddWindow(w, pos,span, flag=dW.get('flag',wx.ALIGN_CENTRE))
                    else:
                        self.gbsData.AddWindow(w, pos,border=span, flag=dW.get('flag',wx.ALIGN_CENTRE))
                    #self.gbsData.Add(w, 
                    #        wx.GBPosition(pos[0],pos[1]),wx.GBSpan(span[0],span[1]),
                    #        flag=dW.get('flag',wx.ALIGN_CENTRE))
                else:
                    oSz,iFlag=tSz
                    oSz.AddWindow(w,pos,border=span, flag=dW.get('flag',wx.ALIGN_CENTRE)|iFlag)
                if evt is not None:
                    if 'evt' in dW:
                        dEvt=dW['evt']
                    else:
                        dEvt={}
                    if None in evt:
                        wPost=evt[None]
                    else:
                        wPost=w
                    for e,f in evt.iteritems():
                        if e is None:
                            continue
                        t=type(e)
                        if t==types.ClassType:
                            wPost.Bind(e,f,id=iId)
                        elif e in dEvt:
                            wPost.Bind(dEvt[e],f,id=iId)
                        elif e in dEvtWid:
                            wPost.Bind(dEvtWid[e],f,id=iId)
                        elif t==types.StringType:
                            w.BindEvent(e,f)
                if self.bEnMark:
                    if dW.get('mark',False)==True:
                        self.lWid.append(w)
                if self.bEnMod:
                    l=dW.get('mod',None)
                    if (l is not None) and ('evt' in dW):
                        dEvt=dW['evt']
                        for e in l:
                            if e in dEvt:
                                w.Bind(dEvt[e],self.OnChange,id=iId)
                return w
            else:
                self.__logError__('widget:%s unknown;pos:%s;span:%s;kwargs:%s'%(sK,
                    self.__logFmt__(pos),self.__logFmt__(span),self.__logFmt__(kwargs)))
        except:
            self.__logTB__()
        return None
    def __add_widget__(self,lWid):
        try:
            bDbg=False
            if VERBOSE>5:
                if self.__isLogDebug__():
                    bDbg=True
            dEvtWid,dMap=self.__get_widget_map__()
            for it in lWid:
                t=type(it)
                if t==types.TupleType:
                    self.AddWidget(it[0],it[1],it[2],it[3],it[4],
                            tDef=(dEvtWid,dMap),bDbg=bDbg)
        except:
            self.__logTB__()
    def SetEnableMark(self,flag):
        self.__logDebug__('flag:%d'%(flag))
        self.bEnMark=flag
    def SetEnableModification(self,flag):
        self.__logDebug__('flag:%d'%(flag))
        self.bEnMod=flag
    def OnChange(self,evt):
        evt.Skip()
        obj=None
        if hasattr(evt,'GetEventObj'):
            obj=evt.GetEventObj()
        if hasattr(evt,'GetEventObject'):
            obj=evt.GetEventObject()
        if obj is None:
            if VERBOSE>5:
                self.__logDebug__('no obj'%())
            return
        if self.bBlk>0:
            return
        if VERBOSE>5:
            self.__logDebug__('obj:%s(%d)'%(obj.GetName(),obj.GetId()))
        self.SetModified(True,obj)
    def IsBlocked(self):
        return self.bBlk>0
    def GetValue(self,sName):
        try:
            if sName is None:
                pass
            else:
                w=getattr(self,sName,None)
                if w is not None:
                    return w.GetValue()
        except:
            self.__logTB__()
        return None
    def SetValue(self,sName,*args,**kwargs):
        try:
            self.SetBlock()
            self.ClrBlockDelayed()
            self.SetModified(False,None)
            if sName is None:
                pass
            else:
                w=getattr(self,sName,None)
                if w is not None:
                    w.SetValue(*args,**kwargs)
                else:
                    self.__logError__('widget with name:%s not found'%(sName))
        except:
            self.__logTB__()
    def SetBlock(self):
        #vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.bBlk=True
        #self.bBlock+=1
    def ClrBlockDelayed(self):
        #vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        wx.CallAfter(self.ClrBlock)
    def ClrBlock(self):
        #vtLog.vtLngCurWX(vtLog.DEBUG,'bBlock:%d'%(self.bBlock),self)
        self.bBlk=False
        #if self.bBlock>0:
        #    self.bBlock-=1
    def SetModified(self,state,obj=None):
        try:
            #if vtLog.vtLngIsLogged(vtLog.DEBUG):
            #    vtLog.vtLngCurWX(vtLog.DEBUG,'bModified:%d;state:%d'%
            #                (self.bModified,state),self)
            self.IsMainThread()
            if self.bMod!=state:
                if self.__isLogDebug__():
                    self.__logDebug__('bMod:%d;state:%d'%(self.bMod,state))
            if self.bEnMod:
                self.bMod=state
            if self.bEnMark==False:
                return
            if state:
                if obj is None:
                    for obj in self.lWid:
                        f=obj.GetFont()
                        f.SetWeight(wx.FONTWEIGHT_BOLD)
                        obj.SetFont(f)
                        obj.Refresh()
                else:
                    f=obj.GetFont()
                    f.SetWeight(wx.FONTWEIGHT_BOLD)
                    obj.SetFont(f)
                    obj.Refresh()
            else:
                if obj is None:
                    for obj in self.lWid:
                        f=obj.GetFont()
                        f.SetWeight(wx.FONTWEIGHT_NORMAL)
                        obj.SetFont(f)
                        obj.Refresh()
                else:
                    f=obj.GetFont()
                    f.SetWeight(wx.FONTWEIGHT_NORMAL)
                    obj.SetFont(f)
                    obj.Refresh()
        except:
            self.__logTB__()
    def GetModified(self):
        if self.__isLogDebug__():
            self.__logDebug__('bModified:%d'%(self.bMod))
        return self.bMod
    def GetWidgetByIdx(self,iIdx):
        try:
            return self.lWid[iIdx]
        except:
            pass#self.__logTB__()
        return None
    def GetMainWidget(self):
        return self.GetWidgetByIdx(0)
    def ShowMsgDlg(self,iStyle,sMsg,func,*args,**kwargs):
        try:
            dlg=None
            style=0
            iStyleChk=iStyle&0xff
            if iStyleChk==self.YESNO:
                style|=wx.YES_NO|wx.NO_DEFAULT
            elif iStyleChk==self.OK:
                style=wx.OK|wx.NO_DEFAULT
            else:
                style=wx.OK
            iStyleChk=iStyle&0xff00
            if (iStyle==0xff00)==self.EXCLAMATION:
                style|=wx.ICON_EXCLAMATION
            elif (iStyle==0xff00)==self.ERROR:
                style|=wx.ICON_ERROR
            elif (iStyle==0xff00)==self.INFORMATION:
                style|=wx.ICON_INFORMATION
            elif (iStyle==0xff00)==self.QUESTION:
                style|=wx.ICON_QUESTION
            else:
                style|=wx.ICON_EXCLAMATION
            sTitle='vtgPanel'
            if hasattr(self,'applName'):
                sTitle=self.applName
            dlg=vtmMsgDialog(self,sMsg,sTitle,style)
            if dlg.ShowModal()==wx.ID_YES:
                if func is not None:
                    func(1,*args,**kwargs)
            else:
                if func is not None:
                    func(0,*args,**kwargs)
        except:
            self.__logTB__()
        if dlg is not None:
            dlg.Destroy()
    def ChooseFile(self,iStyle,sMsg,sWildCard,sFN,sDftFN,func,*args,**kwargs):
        try:
            dlg=None
            self.__logInfo__(''%())
            iStyleChk=iStyle&0xff
            style=wx.OPEN
            bMulti=False
            if iStyleChk==self.SAVE:
                style=wx.SAVE
            if iStyleChk==self.MULTIPLE:
                style|=wx.MULTIPLE
                bMulit=True
            bPosixFN=True
            dlg=wx.FileDialog(self,message=sMsg,
                defaultDir='.',defaultFile=sDftFN or '',
                wildcard=sWildCard,
                style=style)
            dlg.Centre()
            try:
                if sFN is not None:
                    if len(sFN)>0:
                        sTmpDN,sTmpFN=os.path.split(sFN)
                        dlg.SetDirectory(sTmpDN)
                        dlg.SetFilename(sTmpFN)
            except:
                self.__logTB__()
            iRet=dlg.ShowModal()
            if iRet==wx.ID_OK:
                sTmpDN=dlg.GetDirectory()
                sTmpDN=sTmpDN.replace('\\','/')
                if bPosixFN==True:
                    sTmpDN=sTmpDN.replace('\\','/')
                if bMulti==True:
                    lTmpFN=dlg.GetFilenames()
                    if bPosixFN==True:
                        lFN=['/'.join([sTmpDN,sTmpFN]) for sTmpFN in lTmpFN]
                    else:
                        lFN=[os.path.join(sTmpDN,sTmpFN) for sTmpFN in lTmpFN]
                    lFN.sort()
                    iCnt=len(lFN)
                    sFN=lFN[0]
                    self.Post('files',lFN)
                    if func is not None:
                        iOfs=0
                        for sFN in lFN:
                            func(iOfs,iCnt,sFN,*args,**kwargs)
                            iOfs=iOfs+1
                else:
                    sTmpFN=dlg.GetFilename()
                    if bPosixFN==True:
                        sFN='/'.join([sTmpDN,sTmpFN])
                    else:
                        sFN=os.path.join(sTmpDN,sTmpFN)
                    if func is not None:
                        func(0,1,sFN,*args,**kwargs)
        except:
            self.__logTB__()
        if dlg is not None:
            dlg.Destroy()

