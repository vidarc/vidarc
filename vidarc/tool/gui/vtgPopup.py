#----------------------------------------------------------------------------
# Name:         vtgPopup.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20100214
# CVS-ID:       $Id: vtgPopup.py,v 1.9 2010/06/29 13:25:27 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
from vidarc.tool.log.vtLog import vtLogOriginWX
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.tool.art.vtArt as vtArt
from vtgCore import vtGuiCoreArrangeWidget
from vidarc.tool.gui.vtgEvent import vtgEvent

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

vtgxEVT_DLG_OK=wx.NewEventType()
def EVT_DLG_OK(win,func):
    win.Connect(-1,-1,vtgxEVT_DLG_OK,func)
def EVT_DLG_OK_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtgxEVT_DLG_OK,func)
class vtgxDialogOk(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_DIALOG_OK(<widget_name>, xxx)
    """
    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtgxEVT_DLG_OK)

vtgxEVT_DLG_CANCEL=wx.NewEventType()
def EVT_DLG_CANCEL(win,func):
    win.Connect(-1,-1,vtgxEVT_DLG_CANCEL,func)
def EVT_DLG_CANCEL_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtgxEVT_DLG_CANCEL,func)
class vtgxDialogCancel(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_DLG_CANCEL(<widget_name>, xxx)
    """
    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtgxEVT_DLG_CANCEL)

vtgxEVT_DLG_CMD=wx.NewEventType()
def EVT_DLG_CMD(win,func):
    win.Connect(-1,-1,vtgxEVT_DLG_CMD,func)
def EVT_DLG_CMD_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vtgxEVT_DLG_CMD,func)
class vtgxDialogCmd(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_DLG_CMD(<widget_name>, xxx)
    """
    def __init__(self,obj,cmd,data=None):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vtgxEVT_DLG_CMD)
        self.cmd=cmd
        self.data=data
    def GetCmd(self):
        return self.cmd
    def GetData(self):
        return self.data

class vtgPopupEvent(vtgEvent):
    _MAP_EVENT={
            'cmd':      EVT_DLG_CMD,
            'ok':       EVT_DLG_OK,
            'cancel':   EVT_DLG_CANCEL,
            }
    _MAP_EVENT_UNBIND={
            'cmd':      EVT_DLG_CMD_DISCONNECT,
            'ok':       EVT_DLG_OK_DISCONNECT,
            'cancel':   EVT_DLG_CANCEL_DISCONNECT,
            }

class vtgPopup(wx.Dialog,vtLogOriginWX,vtgPopupEvent):
    def __init__(self, parent,pnCls,name=None,
                pos=None,sz=None,iArrange=-1,widArrange=None,
                kind='popup',bmp=None,title=None,
                pnPos=None,pnSz=None,pnStyle=None,
                pnName='pn',**pnKwargs):
        self.SIZE_PN_SCROLL=wx.SystemSettings.GetMetric(wx.SYS_VSCROLL_X)
        
        style,iKind={
                'popup':    (wx.RESIZE_BORDER,0),
                'frame':    (wx.RESIZE_BORDER|wx.DEFAULT_DIALOG_STYLE,1),
                'frmSngCls':(wx.RESIZE_BORDER|wx.DEFAULT_DIALOG_STYLE,2),
                'dlg':      (wx.RESIZE_BORDER,3),
                'dialog':   (wx.DEFAULT_DIALOG_STYLE,1)}.get(kind,(wx.DEFAULT_DIALOG_STYLE,1))
        global _
        _=vtLgBase.assignPluginLang('vtMisc')
        
        if name is None:
            name=parent.GetName()+'_Popup'
        wx.Dialog.__init__(self, parent,style=style,
                    title=title or '',
                    name=name)
        self._iArrange=iArrange
        wx.EVT_SIZE(self,self.OnSize)
        #wx.EVT_CHAR(self,self.OnChar)
        #wx.EVT_KEY_UP(self,self.OnChar)
        self.Bind(wx.EVT_CLOSE, self.OnClose)
        self._bLayout=False
        
        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.gbsData = wx.GridBagSizer(hgap=0, vgap=0)
        self.cbApply=None
        self.cbCancel=None
        if iKind==2:
            self.gbsData.AddSizer(self.bxsBt, (1, 0), border=4,
                  flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTRE , span=(1, 1))
            id=wx.NewId()
            
            self.cbCancel = wx.lib.buttons.GenBitmapTextButton(id=-1,
                  label=_(u'Close'),
                  bitmap=vtArt.getBitmap(vtArt.Close), name=u'cbCancel',
                  parent=self, pos=(0,0), size=(120,30), style=0)
            self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
                  self.cbCancel)
            self.bxsBt.AddWindow(self.cbCancel, 0, border=0, flag=wx.ALIGN_CENTER)
        elif iKind==1:
            self.gbsData.AddSizer(self.bxsBt, (1, 0), border=4,
                  flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTRE , span=(1, 1))
            id=wx.NewId()
            
            self.cbApply = wx.lib.buttons.GenBitmapTextButton(id=-1,
                  label=_(u'Apply'),
                  bitmap=vtArt.getBitmap(vtArt.Apply), name=u'cbApply',
                  parent=self, pos=(0, 0), size=(120, 30),style=0)
            self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
                  self.cbApply)
            self.bxsBt.AddWindow(self.cbApply, 0, border=0, flag=wx.ALIGN_CENTER)
            
            self.bxsBt.AddSpacer(wx.Size(32, 8), border=0, flag=0)
            
            self.cbCancel = wx.lib.buttons.GenBitmapTextButton(id=-1,
                  label=_(u'Cancel'),
                  bitmap=vtArt.getBitmap(vtArt.Cancel), name=u'cbCancel',
                  parent=self, pos=(0,0), size=(120,30), style=0)
            self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
                  self.cbCancel)
            self.bxsBt.AddWindow(self.cbCancel, 0, border=0, flag=wx.ALIGN_CENTER)
        elif iKind==0:
            self.gbsData.AddSizer(self.bxsBt, (0, 0), border=4,
                  flag=wx.BOTTOM | wx.ALIGN_LEFT, span=(1, 1))
            id=wx.NewId()
            
            self.cbCancel = wx.BitmapButton(id=-1,
                  bitmap=vtArt.getBitmap(vtArt.Cancel), name=u'cbCancel',
                  parent=self, pos=(0,0), size=(30,30), style=wx.BU_AUTODRAW)
            self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
                  self.cbCancel)
            self.bxsBt.AddWindow(self.cbCancel, 0, border=0, flag=wx.ALIGN_CENTER)
            
            self.cbApply = wx.BitmapButton(id=-1,
                  bitmap=vtArt.getBitmap(vtArt.Apply), name=u'cbApply',
                  parent=self, pos=wx.Point(32, 00), size=wx.Size(30, 30),
                  style=wx.BU_AUTODRAW)
            self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
                  self.cbApply)
            self.bxsBt.AddWindow(self.cbApply, 0, border=0, flag=wx.ALIGN_CENTER)
        elif iKind==3:
            pass
        else:
            self.gbsData.AddSizer(self.bxsBt, (1, 0), border=4,
                  flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTRE , span=(1, 1))
            id=wx.NewId()
            
            self.cbApply = wx.lib.buttons.GenBitmapTextButton(id=-1,
                  label=_(u'Apply'),
                  bitmap=vtArt.getBitmap(vtArt.Apply), name=u'cbApply',
                  parent=self, pos=(0, 0), size=(120, 30),style=0)
            self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
                  self.cbApply)
            self.bxsBt.AddWindow(self.cbApply, 0, border=0, flag=wx.ALIGN_CENTER)
            
            self.bxsBt.AddSpacer(wx.Size(32, 8), border=0, flag=0)
            
            self.cbCancel = wx.lib.buttons.GenBitmapTextButton(id=-1,
                  label=_(u'Cancel'),
                  bitmap=vtArt.getBitmap(vtArt.Cancel), name=u'cbCancel',
                  parent=self, pos=(0,0), size=(120,30), style=0)
            self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
                  self.cbCancel)
            self.bxsBt.AddWindow(self.cbCancel, 0, border=0, flag=wx.ALIGN_CENTER)
            
        vtLogOriginWX.__init__(self)
        self.pn=None
        if iKind!=0:
            if bmp is None:
                icon=vtArt.getIcon(vtArt.getBitmap(vtArt.Mod))
            else:
                icon=vtArt.getIcon(bmp)
            self.SetIcon(icon)
        try:
            self.pn=pnCls(self,id=wx.NewId(),
                        pos=pnPos or wx.DefaultPosition,
                        size=pnSz or wx.DefaultSize,
                        style=pnStyle,name=pnName,**pnKwargs)
            if iKind==0:
                self.gbsData.AddGrowableCol(0)
                self.gbsData.AddGrowableRow(1)
                self.gbsData.AddWindow(self.pn, (1, 0), border=4,
                      flag=wx.BOTTOM | wx.TOP | wx.LEFT | wx.RIGHT | wx.EXPAND, span=(1, 1))
            else:
                self.gbsData.AddGrowableCol(0)
                self.gbsData.AddGrowableRow(0)
                self.gbsData.AddWindow(self.pn, (0, 0), border=4,
                      flag=wx.BOTTOM | wx.TOP | wx.LEFT | wx.RIGHT | wx.EXPAND, span=(1, 1))
            self.gbsData.Layout()
            self.gbsData.Fit(self)
        except:
            self.__logTB__()
        #print widArrange,iArrange,parent
        self.SetSizer(self.gbsData)
        self._pos=pos
        self._sz=sz
        self._iArrange=iArrange
        self._widArrange=widArrange
        self.Arrange()
    def Arrange(self):
        if self._iArrange>=0:
            if self._iArrange<10:
                if self._sz is not None:
                    self.SetSize(self._sz)
                else:
                    self.gbsData.Layout()
                    self.gbsData.Fit(self)
            if self._widArrange is not None:
                vtGuiCoreArrangeWidget(self._widArrange,self,self._iArrange)
                #wx.EVT_MOVE(widArrange,self.OnMove)
            else:
                parent=self.GetParent()
                vtGuiCoreArrangeWidget(parent,self,self._iArrange)
                #wx.EVT_MOVE(parent,self.OnMove)
            #self.gbsData.Layout()
        else:
            if self._sz is not None:
                self.SetSize(self._sz)
            else:
                #self.Fit()
                self.gbsData.Layout()
                self.gbsData.Fit(self)
                self._bLayout=True
            if self._pos is not None:
                self.Move(self._pos)
            else:
                parent=self.GetParent()
                if parent is not None:
                    self.Centre()
        #self.fgsMain.AddSizer(bxs,1,border=0,flag=wx.EXPAND)
        #self.fgsMain.AddGrowableCol(0)
        
        #if size[1]<22:
        #    size=(size[0],22)
    def SetSize(self,sz):
        try:
            self._bLayout=False
            wx.Dialog.SetSize(self,sz)
        except:
            self.__logTB__()
    def OnMove(self,evt):
        try:
            pos=evt.GetPosition()
            if VERBOSE>5:
                self.__logDebug__('pos:%s;iArrage:%d'%(self.__logFmt__(pos),
                        self._iArrange))
        except:
            self.__logTB__()
    def OnSize(self,evt):
        evt.Skip()
        try:
            sz=evt.GetSize()
            if VERBOSE>5:
                self.__logDebug__('sz:%s;iArrage:%d'%(self.__logFmt__(sz),
                        self._iArrange))
        except:
            self.__logTB__()
    def OnClose(self,evt):
        try:
            self.Show(False)
        except:
            self.__logTB__()
    def HandleKey(self,evt,bRetDict=False,bApply=True,bCancel=True):
        evt.Skip()
        iCode=-1
        bAlt=False
        bCtrl=False
        bMeta=False
        bShift=False
        try:
            cVal=u'%c'%evt.GetUnicodeKey()
            iCode=evt.GetKeyCode()
            bAlt=evt.AltDown()
            bCtrl=evt.ControlDown()
            bMeta=evt.MetaDown()
            bShift=evt.ControlDown()
            if bCancel==True:
                if iCode==wx.WXK_ESCAPE:
                    self.OnCbCancelButton(None)
            if bApply==True:
                if iCode==wx.WXK_RETURN:
                    self.OnCbApplyButton(None)
                if iCode==wx.WXK_NUMPAD_ENTER:
                    self.OnCbApplyButton(None)
        except:
            self.__logTB__()
        if bRetDict==True:
            return {'v':cVal,'code':iCode,'shift':bShift,'alt':bAlt,'ctrl':bCtrl,'meta':bMeta}
    def Show(self,bFlag=True):
        try:
            if bFlag==True:
                self.Arrange()
            self.__Show__(bFlag=bFlag)
        except:
            self.__logTB__()
    def __Show__(self,bFlag=True):
        try:
            wx.Dialog.Show(self,bFlag)
            self.SetFocus()
        except:
            self.__logTB__()
    def SetFocus(self):
        try:
            if self.cbCancel is not None:
                self.cbCancel.SetFocus()
        except:
            self.__logTB__()
    def Post(self,cmd,data=None):
        wx.PostEvent(self,vtgxDialogCmd(self,cmd,data=data))
    def OnCbCancelButton(self,evt):
        try:
            self.__logDebug__(''%())
            wx.PostEvent(self,vtgxDialogCancel(self))
            if self.IsModal():
                self.EndModal(0)
            else:
                self.Show(False)
        except:
            self.__logTB__()
    def OnCbApplyButton(self,evt):
        try:
            self.__logDebug__(''%())
            wx.PostEvent(self,vtgxDialogOk(self))
            if self.IsModal():
                self.EndModal(1)
            else:
                self.Show(False)
        except:
            self.__logTB__()
    def GetPanel(self):
        return self.pn
    def __getattr__(self,name):
        if hasattr(self.pn,name):
            return getattr(self.pn,name)
        raise AttributeError(name)

