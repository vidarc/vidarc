#----------------------------------------------------------------------------
# Name:         vtgFrmMain.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20100309
# CVS-ID:       $Id: vtgFrmMain.py,v 1.1 2010/06/14 09:56:58 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    
    from vtgFrm import vtgFrm
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vtgFrmMain(vtgFrm):
    def __init__(self,*args,**kwargs):
        vtgFrm.__init__(self,*args,**kwargs)
