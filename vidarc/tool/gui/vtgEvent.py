#----------------------------------------------------------------------------
# Name:         vtgEvent.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20100117
# CVS-ID:       $Id: vtgEvent.py,v 1.2 2010/02/13 22:43:57 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

class vtgEvent:
    _MAP_EVENT={}
    _MAP_EVENT_UNBIND={}
    def BindEvent(self,name,func,par=None):
        try:
            if name in self._MAP_EVENT:
                if par is None:
                    self._MAP_EVENT[name](self,func)
                else:
                    self._MAP_EVENT[name](par,func)
                return 0
            else:
                self.__logCritical__('evt name:%s not resolved'%(name))
        except:
            self.__logTB__()
        return -1
    def UnBindEvent(self,name,func,par=None):
        try:
            if name in self._MAP_EVENT_UNBIND:
                if par is None:
                    self._MAP_EVENT_UNBIND[name](self,func)
                else:
                    self._MAP_EVENT_UNBIND[name](par,func)
                return 0
            else:
                self.__logCritical__('evt name:%s not resolved'%(name))
        except:
            self.__logTB__()
        return -1
