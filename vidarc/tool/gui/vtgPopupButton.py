#----------------------------------------------------------------------------
# Name:         vtgPopupButton.py
# Purpose:      popup button
# Author:       Walter Obweger
#
# Created:      20100530
# CVS-ID:       $Id: vtgPopupButton.py,v 1.2 2010/06/10 08:50:00 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import wx
    from wx.lib.buttons import GenBitmapToggleButton

    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

    from vidarc.tool.gui.vtgPanelMixinWX import vtgPanelMixinWX
    import vidarc.tool.art.vtArt as vtArt
    from vtgCore import vtGuiCoreArrangeWidget
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vtgPopupButton(vtgPanelMixinWX,GenBitmapToggleButton):
    def __init__(self,id=-1,parent=None,pos=(0,0),size=(400,100),name='cbPopup',
                style=0,bmp=None,wid=None,
                bAnchor=True,iArrange=0):
        GenBitmapToggleButton.__init__(self,id=id,
                parent=parent,pos=pos,size=size,
                bitmap=bmp or vtArt.getBitmap(vtArt.Down),
                name=name,
                style=style)
        vtgPanelMixinWX.__init__(self)
        #wx.EVT_BUTTON(self.OnPopupButton,self)
        self.Bind(wx.EVT_BUTTON,self.OnPopupButton,self)
        if bAnchor==True:
            bFound=False
            par=parent
            while par is not None:
                if par.GetClassName() in ['wxFrame','wxMDIChildFrame','wxDialog']:
                    bFound=True
                    break
                par=par.GetParent()
            if bFound==True:
                par.Bind(wx.EVT_MOVE,self.OnMoveButton)
            else:
                self.__logError__('anchor not possible, no valid parent widget found'%())
        self.Enable(False)
        #wx.EVT_LEFT_UP(self, self.OnLeftDown)
        #wx.EVT_RIGHT_DOWN(self, self.OnRightDown)
        self.bAnchor=bAnchor
        self.iArrange=iArrange
        self.widPop=None
        self.bBusy=False
        if wid is not None:
            self.SetPopupWid(wid)
    def SetPopupWid(self,wid):
        """
        """
        try:
            if self.IsMainThread()==False:
                return
            self.widPop=wid
            self.Enable(True)
            self.bBusy=True
            if self.widPop is not None:
                vtGuiCoreArrangeWidget(self,self.widPop,self.iArrange)
                if hasattr(self.widPop,'BindEvent'):
                    self.widPop.BindEvent('ok',self.OnPopupOk)
                    self.widPop.BindEvent('cancel',self.OnPopupCancel)
        except:
            self.__logTB__()
    def OnPopupButton(self,evt):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            if self.widPop is not None:
                if self.GetValue()==True:
                    vtGuiCoreArrangeWidget(self,self.widPop,self.iArrange)
                    self.widPop.Show(True)
                else:
                    self.widPop.Show(False)
            else:
                self.__logError__('no popup widget set yet'%())
                #vtGuiCoreArrangeWidget(self,self,iArrange)
        except:
            self.__logTB__()
    def OnMoveButton(self,evt):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            if self.widPop is not None:
                vtGuiCoreArrangeWidget(self,self.widPop,self.iArrange)
        except:
            self.__logTB__()
    def __Close__(self):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            if self.IsMainThread()==False:
                return
            if self.widPop is not None:
                self.widPop.Show(False)
            self.SetValue(False)
        except:
            self.__logTB__()
    Close=__Close__
    def Popup(self,bFlag=True):
        self.SetValue(bFlag)
        self.OnPopupButton(None)
    def OnPopupOk(self,evt):
        evt.Skip()
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            self.__Close__()
        except:
            self.__logTB__()
    def OnPopupCancel(self,evt):
        evt.Skip()
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            self.__Close__()
        except:
            self.__logTB__()
