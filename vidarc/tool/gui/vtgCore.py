#----------------------------------------------------------------------------
# Name:         vtCore.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20070130
# CVS-ID:       $Id: vtgCore.py,v 1.7 2011/01/10 14:20:32 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------


import wx
from vtgScreen import getScreenDict

def vtGuiCoreLimitWindowToScreen(iX,iY,iWidth,iHeight):
    #try:
        dScreens=getScreenDict()
        if dScreens is None:
            iMaxW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            iMaxH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            iMaxW-=wx.SystemSettings.GetMetric(wx.SYS_MENU_Y)
            iMaxH-=wx.SystemSettings.GetMetric(wx.SYS_MENU_Y)
            iXa,iYa=0,0
            iXe,iYe=iMaxW,iMaxH
        else:
            dScr=dScreens[None]
            iXa=dScr.get('iXa',0)
            iYa=dScr.get('iYa',0)
            iXe=dScr.get('iXe',0)
            iYe=dScr.get('iYe',0)
            iMaxW=iXe-iXa
            iMaxH=iYe-iYa
        if iX>iMaxW:
            iX=iMaxW-iWidth
        if iY>iMaxH:
            iY=iMaxH-iHeight
        if iX<iXa:
            iX=0
        if iY<iYa:
            iY=0
        if iX+iWidth>iXe:
            iX=iXe-iWidth
        if iY+iHeight>iYe:
            iY=iYe-iHeight
        if iX<iXa:
            iX=iXa
        if iY<iYa:
            iY=iYa
        if iX+iWidth>iXe:
            iWidth=iMaxW-iX
        if iY+iHeight>iMaxH:
            iHeight=iMaxH-iY
    #except:
    #    iX=iY=0
    #    iWidth=100
    #    iHeight=100
        return iX,iY,iWidth,iHeight

def vtGuiCoreArrangeWidget(widBase,widSub,how):
    """
    how = 0     ... bottom left of widBase size of widSub
    how = 1     ... bottom right of widBase size of widSub
    how = 2     ... top left of widBase size of widSub
    how = 3     ... top right of widBase size of widSub
    how =10     ... top right of widBase
    how =20     ... top left of widBase
    """
    iBaseX,iBaseY = widBase.ClientToScreen( (0,0) )
    #iBaseW,iBaseH =  widBase.GetSize()
    iBaseW,iBaseH =  widBase.GetClientSize()
    iSubX,iSubY=widSub.GetPosition()
    iSubW,iSubH=widSub.GetSize()
    if how==0:
        iX,iY,iW,iH=vtGuiCoreLimitWindowToScreen(iBaseX,iBaseY+iBaseH,iSubW,iSubH)
    elif how==1:
        iX,iY,iW,iH=vtGuiCoreLimitWindowToScreen(iBaseX-iSubW+iBaseW,iBaseY+iBaseH,iSubW,iSubH)
    elif how==2:
        iX,iY,iW,iH=vtGuiCoreLimitWindowToScreen(iBaseX,iBaseY-iSubH,iSubW,iSubH)
    elif how==3:
        iX,iY,iW,iH=vtGuiCoreLimitWindowToScreen(iBaseX-iSubW+iBaseW,iBaseY-iSubH,iSubW,iSubH)
    elif how==10:
        iX,iY,iW,iH=vtGuiCoreLimitWindowToScreen(iBaseX+iBaseW,iBaseY,iSubW,iBaseH)
    elif how==20:
        iX,iY,iW,iH=vtGuiCoreLimitWindowToScreen(iBaseX,iBaseY-iSubH,iSubW,iSubH)
    else:
        iX,iY,iW,iH=vtGuiCoreLimitWindowToScreen(iBaseX,iBaseY+iBaseH,iSubW,iSubH)
    widSub.Move((iX,iY))
    widSub.SetSize((iW,iH))

def vtGuiCoreGetBaseWidget(par):
    wid=par
    while wid is not None:
        if wid.GetClassName() in ['wxFrame','wxMDIChildFrame',
                    'wxMDIParentFrame','wxDialog']:
            return wid
        wid=wid.GetParent()
    return par

def vtGuiCoreGetColorRGB(iColor):
    iNum=wx.SYS_COLOUR_WINDOW
    if iColor==0:
        iNum=wx.SYS_COLOUR_WINDOW
    oCol=wx.SystemSettings.GetColour(iNum)
    return [oCol.Red(),oCol.Green(),oCol.Blue()]
