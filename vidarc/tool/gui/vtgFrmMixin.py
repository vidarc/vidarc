#----------------------------------------------------------------------------
# Name:         vtgFrmMixin.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20100309
# CVS-ID:       $Id: vtgFrmMixin.py,v 1.1 2010/06/14 09:56:15 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import wx
    import os,types
    from vidarc.tool.log.vtLog import vtLogOriginWX
    from vtgPopup import vtgPopup
    from vtgPanel import vtgPanel
    from vidarc.tool.vtThread import vtThreadWX
    from vidarc.tool.misc.vtmThread import vtmThreadInterFace
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vtgFrmMixin(vtLogOriginWX):
    def __init__(self):
        vtLogOriginWX.__init__(self)
    def GetToolBar(self):
        return getattr(self,'_tbMain')
    def AddToolBar(self,iOfs,l,sz=(20,20),sName='_tbMain'):
        try:
            if hasattr(self,'_tbMain')==False:
                iId=wx.NewId()
                self._tbMain = wx.ToolBar(id=iId,
                    name=u'tbMain', parent=self, pos=wx.Point(0,0),
                    size=wx.DefaultSize, style=wx.TB_HORIZONTAL | wx.NO_BORDER)
                self._tbMain.SetToolBitmapSize(sz)
                self.SetToolBar(self._tbMain)
                self._dToolBarId={}
                self._dToolBar={}
                if self._bLayout==True:
                    wx.CallAfter(self.gbsData.Layout)
                    wx.CallAfter(self.gbsData.Fit,self)
                
            tb=getattr(self,'_tbMain')
            d=getattr(self,'_dToolBar')
            dId=getattr(self,'_dToolBarId')
            for tup in l:
                t=type(tup)
                if tup is None:
                    tb.InsertSeparator(iOfs)
                    iOfs+=1
                elif t==types.ListType:
                    pass
                elif t==types.TupleType:
                    if tup[0] in d:
                        self.__logError__('key:%s already in d;%s'%(tup[0],
                                self.__logFmt__(d)))
                        continue
                    if tup[1]=='txt':
                        iId=wx.NewId()
                        txtCtrl=wx.TextCtrl(tb, iId, tup[2], size=(150, -1))
                        if tup[3] is not None:
                            pass
                        #tb.AddControl(txtCtrl)
                        tb.InsertControl(iOfs,txtCtrl)
                        d[tup[0]]=iId
                        dId[iId]=(tup[4],tup[5],tup[6])
                        iOfs+=1
                    elif tup[1]=='tg':
                        iId=wx.NewId()
                        if type(tup[2])==types.TupleType:
                            bmp0=tup[2][0]
                            bmp1=tup[2][1]
                        else:
                            bmp0=tup[2]
                            bmp1=None
                        tgCtrl=wx.lib.buttons.GenBitmapToggleButton(bitmap=bmp0,
                                id=iId, name=tup[0],parent=tb, 
                                pos=wx.DefaultPosition, size=wx.DefaultSize,
                                style=0)
                        if bmp1 is not None:
                            tgCtrl.SetBitmapSelected(bmp1)
                        tb.InsertControl(iOfs,tgCtrl)
                        self.Bind(wx.EVT_BUTTON, self.OnAddToolBar, id=iId)
                        d[tup[0]]=iId
                        dId[iId]=(tup[4],tup[5],tup[6])
                        iOfs+=1
                    elif tup[1]=='bt':
                        iId=wx.NewId()
                        tb.InsertSimpleTool(iOfs,iId, tup[2],  
                                shortHelpString=tup[3])
                        self.Bind(wx.EVT_TOOL, self.OnAddToolBar, id=iId)
                        d[tup[0]]=iId
                        dId[iId]=(tup[4],tup[5],tup[6])
                        iOfs+=1
            tb.Realize()
        except:
            self.__logTB__()
    def OnAddToolBar(self,evt):
        try:
            eId=evt.GetId()
            if eId in self._dToolBarId:
                t=self._dToolBarId[eId]
                t[0](*t[1],**t[2])
        except:
            self.__logTB__()
    def EnableTool(self,v,bFlag=True):
        try:
            if type(v)==types.ListType:
                for k in v:
                    if k in self._dToolBar:
                        self._tbMain.EnableTool(self._dToolBar[k],bFlag)
            else:
                if v in self._dToolBar:
                    self._tbMain.EnableTool(self._dToolBar[v],bFlag)
        except:
            self.__logTB__()
    def AddMenu(self,iOfs,l,lHier=None):
        try:
            if hasattr(self,'_mnbMain')==False:
                mnb=wx.MenuBar()
                setattr(self,'_mnbMain',mnb)
                self.SetMenuBar(mnb)
                if self._bLayout==True:
                    wx.CallAfter(self.gbsData.Layout)
                    wx.CallAfter(self.gbsData.Fit,self)
            if hasattr(self,'_mnbMain'):
                mnb=getattr(self,'_mnbMain')
                mn=None
                if lHier is not None:
                    iPos=lHier[0]
                    iCnt=mn.GetMenuCount()
                    if iPos>=iCnt:
                        self.__logError__('iPos:%d;iCnt:%d'%(iPos,iCnt))
                        mn=mn.GetMenu(iCnt-1)
                    else:
                        mn=mn.GetMenu(iPos)
                    for iPos in lHier[1:]:
                        iCnt=mn.GetMenuItemCount()
                        if iPos>=iCnt:
                            self.__logError__('iPos:%d;iCnt:%d'%(iPos,iCnt))
                            mn=mn.GetMenuItems(iCnt-1)
                        else:
                            mn=mn.GetMenuItems(iPos)
                #mn=getattr(self,sMnName)
                sAttr='_popIdMenu'
                if hasattr(self,sAttr):
                    dId=getattr(self,sAttr)
                else:
                    dId={}
                    setattr(self,sAttr,dId)
                sAttr='_dMenu'
                if hasattr(self,sAttr):
                    d=getattr(self,sAttr)
                else:
                    d={}
                    setattr(self,sAttr,d)
                    #wx.EVT_MENU(self,self.popupIdHelp,self.OnHelp)
                def addItems(mn,d,iOfs,l,mnb):
                    for tup in l:
                        t=type(tup)
                        if tup is None:
                            mn.InsertSeparator(iOfs)
                            iOfs+=1
                        elif tup[0] in d:
                            self.__logError__('key:%s already in d;%s'%(tup[0],
                                    self.__logFmt__(d)))
                            #return
                        elif t==types.ListType:
                            iId=wx.NewId()
                            mnSub=wx.Menu()
                            addItems(mnSub,d,0,tup[6],mnb)
                            if mn is None:
                                mnb.Insert(iOfs,menu=mnSub,title=tup[1])
                                d[tup[0]]=iId
                            else:
                                mnIt=wx.MenuItem(mn,iId,tup[1],subMenu=mnSub)
                                if tup[2] is not None:
                                    mnIt.SetBitmap(tup[2])
                                wx.EVT_MENU(self,iId,self.OnAddMenuItems)
                                d[tup[0]]=iId
                                dId[iId]=(tup[3],tup[4],tup[5])
                                mn.InsertItem(iOfs,mnIt)
                            iOfs+=1
                        else:
                            iId=wx.NewId()
                            mnIt=wx.MenuItem(mn,iId,tup[1])
                            if tup[2] is not None:
                                mnIt.SetBitmap(tup[2])
                            wx.EVT_MENU(self,iId,self.OnAddMenuItems)
                            d[tup[0]]=iId
                            dId[iId]=(tup[3],tup[4],tup[5])
                            mn.InsertItem(iOfs,mnIt)
                            iOfs+=1
                addItems(mn,d,iOfs,l,mnb)
                #if self.__isLogDebug__():
                #    self.__logDebug__('menu:%s;d:;%s'%(sMnName,
                #            vtLog.pformat(self._popIdAddMenuItems)))
            else:
                self.__logError__('menu:%s not found'%(sMnName))
        except:
            self.__logTB__()
    def OnAddMenuItems(self,evt):
        try:
            eId=evt.GetId()
            sLang=None
            if eId in self._popIdMenu:
                t=self._popIdMenu[eId]
                t[0](*t[1],**t[2])
            else:
                self.__logError__('eId:%d not found;%s'%(eId,
                        vtLog.pformat(self._popIdAddMenuItems)))
        except:
            self.__logTB__()
    def EnableMenu(self,v,bFlag=True):
        try:
            if type(v)==types.ListType:
                for k in v:
                    if k in self._dMenu:
                        self._mnbMain.Enable(self._dMenu[k],bFlag)
            else:
                if v in self._dMenu:
                    self._mnbMain.Enable(self._dMenu[v],bFlag)
        except:
            self.__logTB__()
    def ChooseFile(self,sFN='',sMsg=None,sWildCard=None,sDftFN=None,
                bSave=False,bMulti=False,bPosixFN=True):
        try:
            self.__logInfo__(''%())
            if bSave==True:
                iStyle=wx.SAVE
            else:
                iStyle=wx.OPEN
            if bMulti==True:
                iStyle|=wx.MULTIPLE
            dlg=wx.FileDialog(self,message=sMsg or _(u'choose file'),
                defaultDir='.',defaultFile=sDftFN or 'empty.xml',
                wildcard=sWildCard or _(u'xml files|*.xml|all files|*.*'),
                style=iStyle)
            dlg.Centre()
            try:
                if len(sFN)>0:
                    sTmpDN,sTmpFN=os.path.split(sFN)
                    dlg.SetDirectory(sTmpDN)
                    dlg.SetFilename(sTmpFN)
            except:
                self.__logTB__()
            iRet=dlg.ShowModal()
            if iRet==wx.ID_OK:
                sTmpDN=dlg.GetDirectory()
                if bPosixFN==True:
                    sTmpDN=sTmpDN.replace('\\','/')
                if bMulti==True:
                    lTmpFN=dlg.GetFilenames()
                    if bPosixFN==True:
                        lFN=['/'.join([sTmpDN,sTmpFN]) for sTmpFN in lTmpFN]
                    else:
                        lFN=[os.path.join(sTmpDN,sTmpFN) for sTmpFN in lTmpFN]
                    lFN.sort()
                    dlg.Destroy()
                    return lFN
                else:
                    sTmpFN=dlg.GetFilename()
                    if bPosixFN==True:
                        sFN='/'.join([sTmpDN,sTmpFN])
                    else:
                        sFN=os.path.join(sTmpDN,sTmpFN)
                    dlg.Destroy()
                    return sFN
            dlg.Destroy()
        except:
            self.__logTB__()
        return None

class vtgFrmMixinMain(vtmThreadInterFace):
    def __init__(self):
        self.thdFrm=vtThreadWX(self,bPost=True,
                origin=':'.join([self.GetOrigin(),'frmThd']))
        vtmThreadInterFace.__init__(self,self.thdFrm)
        #vtgPopup
