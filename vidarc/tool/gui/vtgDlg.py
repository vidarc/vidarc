#----------------------------------------------------------------------------
# Name:         vtgPopup.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20100214
# CVS-ID:       $Id: vtgDlg.py,v 1.3 2010/04/03 19:12:14 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.gui.vtgPopup import vtgPopup

class vtgDlg(vtgPopup):
    def __init__(self, parent,pnCls,name=None,
                pos=None,sz=None,iArrange=-1,widArrange=None,
                kind='dialog',bmp=None,title=None,
                pnPos=None,pnSz=None,pnStyle=None,
                pnName='pn',**pnKwargs):
        vtgPopup.__init__(self, parent,pnCls,name=name,
                pos=pos,sz=sz,iArrange=iArrange,widArrange=widArrange,
                kind=kind,bmp=bmp,title=title,
                pnPos=pnPos,pnSz=pnSz,pnStyle=pnStyle,
                pnName=pnName,**pnKwargs)
    def Show(self,bFlag=True):
        try:
            self.__Show__(bFlag=bFlag)
        except:
            self.__logTB__()
