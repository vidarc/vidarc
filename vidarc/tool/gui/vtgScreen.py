# -*- coding: iso-8859-1 -*-
#----------------------------------------------------------------------
# Name:         vtGuiScreen.py
# Purpose:      screen related functions
#
# Author:       Walter Obweger
#
# Created:      20100226
# CVS-ID:       $Id: vtgScreen.py,v 1.1 2010/02/26 18:15:28 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import sys

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

from vSystem import isPlatForm
if isPlatForm('win'):
    from vtgScreenWin import *
else:
    from vtgScreenDummy import *
