#----------------------------------------------------------------------------
# Name:         vtgButtonStateFlag.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20090428
# CVS-ID:       $Id: vtgButtonStateFlag.py,v 1.9 2010/03/05 20:26:57 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.buttons import GenBitmapButton

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
from vidarc.tool.vtStateFlag import vtStateFlag

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

class vtgButtonStateFlag(vtLog.vtLogOriginWX,GenBitmapButton):
    def __init__(self,id=-1,parent=None,pos=(0,0),size=(16,16),
                name='vtgButtonStateFlag',style=0,small=True,ident=None,
                #idWid=None,idWidFunc=None,
                *args,**kwargs):
        if small:
            #img=wx.EmptyBitmap(16, 16)
            sz=(16,16)
            bSmall=True
        else:
            #img=wx.EmptyBitmap(32, 32)
            sz=(32,32)
            bSmall=False
        img=vtArt.getBitmap(vtArt.Invisible,sz)
        GenBitmapButton.__init__(self,#id=id,
                parent=parent,pos=pos,size=size,
                bitmap=img,name=name,
                style=style)
        wx.EVT_LEFT_UP(self, self.OnLeftDown)
        wx.EVT_RIGHT_DOWN(self, self.OnRightDown)
        sOrigin=u'.'.join([vtLog.vtLngGetRelevantNamesWX(parent,None),name])
        #l=sOrigin.split('.')
        #sOrigin='.'.join(l[:-3]+[l[-1]])
        #vtLog.vtLogOrigin.__init__(self,sOrigin)
        vtLog.vtLogOriginWX.__init__(self,sOrigin)
        #print idWid,idWidFunc
        #if idWid is None:
        #    if idWidFunc is not None:
        #        idWid=idWidFunc()
        self.oSF=vtStateFlag(
                    {
                    },
                    {
                    },
                    verbose=VERBOSE-10,
                    par=self,
                    ident=ident or self,#.GetId(),
                    #idWid=idWid,
                    funcNotify=kwargs.get('funcNotify',None),
                    funcNotifyCB=kwargs.get('funcNotifyCB',self.NotifyStateChange),
                    origin=u':'.join([sOrigin,u'StateFlag']))
        self.__init_ctrl__(*args,**kwargs)
        self.__setupImageList__(bSmall)
    def __init_ctrl__(self,*args,**kwargs):
        pass
    # +++++ GenBitmapButton
    def DrawBezel(self, dc, x1, y1, x2, y2):
        if self.hasFocus and self.useFocusInd:
            return
        # draw the upper left sides
        if self.up:
            #dc.SetPen(self.highlightPen)
            dc.SetPen(self.shadowPen)
        else:
            dc.SetPen(self.shadowPen)
        for i in range(self.bezelWidth):
            dc.DrawLine(x1+i, y1, x1+i, y2-i)
            dc.DrawLine(x1, y1+i, x2-i, y1+i)
        # draw the lower right sides
        if self.up:
            dc.SetPen(self.shadowPen)
        else:
            #dc.SetPen(self.highlightPen)
            dc.SetPen(self.shadowPen)
        for i in range(self.bezelWidth):
            dc.DrawLine(x1+i, y2-i, x2+1, y2-i)
            dc.DrawLine(x2-i, y1+i, x2-i, y2)
    def DrawFocusIndicator(self, dc, w, h):
        bw = self.bezelWidth
        self.focusIndPen.SetColour(self.focusClr)
        dc.SetLogicalFunction(wx.INVERT)
        dc.SetPen(self.focusIndPen)
        dc.SetBrush(wx.TRANSPARENT_BRUSH)
        dc.DrawRectangle(bw,bw,  w-bw*2, h-bw*2)
        dc.SetLogicalFunction(wx.COPY)
    def DrawLabel(self, dc, width, height, dw=0, dy=0):
        try:
            x,y=0,0
            def getXY(bmp):
                bw,bh = bmp.GetWidth(), bmp.GetHeight()
                x,y=(width-bw)/2+dw, (height-bh)/2+dy
                return x,y
            bmp=self.__getBitmapByStatus__()
            if VERBOSE>10:
                self.__logDebug__('bmp==None:%d'%(bmp is None))
            hasMask = bmp.GetMask() != None
            x,y=getXY(bmp)
            dc.DrawBitmap(bmp,x,y,hasMask)
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def OnPaint(self, event):
        (width, height) = self.GetClientSizeTuple()
        x1 = y1 = 0
        x2 = width-1
        y2 = height-1
        dc = wx.PaintDC(self)
        brush = self.GetBackgroundBrush(dc)
        if brush is not None:
            dc.SetBackground(brush)
            dc.Clear()
        self.DrawLabel(dc, width, height)
        self.DrawBezel(dc, x1, y1, x2, y2)
        if self.hasFocus and self.useFocusInd:
            self.DrawFocusIndicator(dc, width, height)
    # ----- GenBitmapButton
    def __setupImageList__(self,bSmall=True):
        try:
            if VERBOSE>5:
                self.__logDebug__(''%())
            if bSmall:
                sz=(16,16)
            else:
                sz=(32,32)
            self.imgDict={}
            for state,t in self.oSF.GetStateInfoLst('img'):
                if t is None:
                    continue
                f,args,kwargs=t
                kkwargs=kwargs.copy()
                kkwargs.update({'sz':sz})
                self.imgDict[state]=f(*args,**kkwargs)
                #vtNetArt.getBitmap(sImg,sz=sz)
            self.imgDict[None]=vtArt.getBitmap(vtArt.Error,sz)
            if VERBOSE>5:
                self.__logDebug__('imgDict:%s'%(self.__logFmt__(self.imgDict)))
        except:
            self.__logTB__()
    def __getBitmapByStatus__(self,iStatus=None):
        iStatus=self.GetStatus(iStatus)
        if VERBOSE>10:
            self.__logDebug__('iStatus:%d;in imgDict=%d'%(iStatus,iStatus in self.imgDict))
        if iStatus in self.imgDict:
            return self.imgDict[iStatus]
        else:
            return self.imgDict[None]
    def GetStatus(self,iStatus=None):
        if iStatus is None:
            return self.oSF.GetState()
        else:
            return iStatus
    def OnLeftDown(self,evt):
        evt.Skip()
    def OnRightDown(self,evt):
        evt.Skip()
    def __getattr__(self,name):
        if hasattr(self.oSF,name):
            return getattr(self.oSF,name)
        raise AttributeError(name)
    def PostEvent(self,*args,**kwargs):
        wx.PostEvent(*args,**kwargs)
    # +++++ StateFlag object
    def BindEventsStateFlag(self,*args,**kwargs):
        self.oSF.BindEvents(*args,**kwargs)
    def NotifyStateChange(self,*args,**kwargs):
        """ called in case of state change from wx MainLoop
        """
        self.Refresh()
    # ----- StateFlag object
