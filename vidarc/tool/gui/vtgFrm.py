#----------------------------------------------------------------------------
# Name:         vtgFrm.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20100309
# CVS-ID:       $Id: vtgFrm.py,v 1.1 2010/06/14 09:56:58 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import wx
    
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    from vtgPanel import vtgPanel
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vtgFrm(vtgPanel):
    def __init__(self,*args,**kwargs):
        _kwargs={}
        for s in ['id','name','parent','pos','size','style','title']:
            if s in kwargs:
                _kwargs[s]=kwargs[s]
        if 'size' not in _kwargs:
            sz=self.SIZE_DFT
            _kwargs['size']=sz
        else:
            sz=_kwargs['size']
        if 'id' not in _kwargs:
            iId=wx.NewId()
            _kwargs['id']=iId
        else:
            iId=_kwargs['id']
        if 'parent' not in _kwargs:
            _kwargs['parent']=None
        if 'style' not in _kwargs:
            _kwargs['style']=wx.DEFAULT_FRAME_STYLE
        #else:
        #    if _kwargs['style'] is None:
        #        _kwargs['style']=wx.TAB_TRAVERSAL
        if kwargs.get('bMDI',False):
            self._frm=wx.MDIChildFrame(**_kwargs)
        else:
            self._frm=wx.Frame(**_kwargs)
    def __getattr__(self,name):
        if hasattr(self._frm,name):
            return getattr(self._frm,name)
        raise AttributeError(name)
    
