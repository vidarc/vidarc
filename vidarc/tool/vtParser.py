#----------------------------------------------------------------------------
# Name:         vtParser.py
# Purpose:      very basic parser object 
#
# Author:       Walter Obweger
#
# Created:      20090116
# CVS-ID:       $Id: vtParser.py,v 1.11 2009/05/09 17:34:41 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os
from types import ListType,StringType,IntType,TupleType
from cStringIO import StringIO

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
from vtBuffer import vtBuffer
from vtStateFlagSimple import vtStateFlagSimple

class vtParserLevelSizeException(Exception):
    def __init__(self,s1,s2):
        self.sLevelUp=s1
        self.sLevelDown=s2
    def __str__(self):
        return 'vtParserLevelSizeException:level up:%s level down:%s'%(self.sLevelUp,self.sLevelDown)

class vtParser(vtLog.vtLogMixIn,vtBuffer):
    VERBOSE=0
    #brace {}
    #bracket ()
    #square bracket []
    CMD=None
    WILDCARD='*'
    WHITESPACE=['\t','\n','\r','\x0b','\x0c']
    END=[' ','\n','\r']
    def __init__(self,origin=None,max=2048,LEVEL_UP=['[','{','('],
                LEVEL_DOWN=[']','}',')'],verbose=0):
        vtBuffer.__init__(self,max,reorganise=max/4)
        self.VERBOSE=verbose
        self.oParse=vtStateFlagSimple(
                    {
                    0x00:{'name':'UNDEF','is':'IsUndef','init':True,
                        'enter':{'func':(self.NotifyUndef,(),{}),'clr':['lv','cmd','comm']}},
                    0x01:{'name':'GEN','is':'IsGenerated',
                        'enter':{'func':(self.NotifyGen,(),{}),'clr':['lv','cmd','comm']}},
                    0x02:{'name':'CLR','is':'IsClr',
                        'enter':{'func':(self.NotifyClr,(),{}),'clr':['lv','cmd','comm']}},
                    0x03:{'name':'START','is':'IsStart',
                        'enter':{'func':(self.NotifyStart,(),{}),'clr':['lv','cmd','comm']}},
                    0x04:{'name':'END','is':'IsEnd',
                        'enter':{'func':(self.NotifyEnd,(),{}),'clr':['lv','cmd','comm']}},
                    0x10:{'name':'CMD_STA','is':'IsCmdStart',
                        'enter':{'func':(self.NotifyCmdStart,(),{}),'set':['cmd']}},
                    0x11:{'name':'CMD_OK','is':'IsCmdOk',
                        'enter':{'func':(self.NotifyCmdOk,(),{})}},
                    0x12:{'name':'CMD_END','is':'IsCmdEnd',
                        'enter':{'func':(self.NotifyCmdEnd,(),{})}},
                    0x13:{'name':'CMD_FLT','is':'IsCmdFlt',
                        'enter':{'func':(self.NotifyCmdFault,(),{})}},
                    0x19:{'name':'CMD_UNDEF','is':'IsCmdUnDef',
                        'enter':{'func':(self.NotifyCmdUnDef,(),{})}},
                    0x20:{'name':'LV_UP',#'is':'IsBracketOpen',
                        'enter':{'func':(self.NotifyLvUp,(),{}),'set':['lv']}},
                    0x21:{'name':'LV_DAT',#'is':'IsBracketOpen',
                        'enter':{'func':(self.NotifyLvData,(),{})}},
                    0x30:{'name':'LV_DN',#'is':'IsBracketClosed',
                        'enter':{'func':(self.NotifyLvDn,(),{})}},
                    0x40:{'name':'COMM_STA','is':'IsComment',
                        'enter':{'func':(self.NotifyCommentStart,(),{}),'set':['comm']}},
                    0x41:{'name':'COMM_END',#'is':'IsComment',
                        'enter':{'func':(self.NotifyCommentEnd,(),{}),'clr':['comm']}},
                    0x80:{'name':'WC_STA',#'is':'IsBracketOpen',
                        'enter':{'func':(self.NotifyWildCardStart,(),{}),'set':['wc']}},
                    0x81:{'name':'WC_END',#'is':'IsBracketOpen',
                        'enter':{'func':(self.NotifyWildCardEnd,(),{}),'clr':['wc']}},
                    },
                    {
                    0x0001:{'name': 'lv',           'order':1,  'is':'IsLevelAct'},
                    0x0002:{'name': 'comm',         'order':2,  'is':'IsCommentAct'},
                    0x0004:{'name': 'cmd',          'order':3,  'is':'IsCommandAct'},
                    0x0008:{'name': 'wc',           'order':5,  'is':'IsWildCardAct'},
                    },
                    origin=origin)
        self.GetOrigin=self.oParse.GetOrigin
        self.dParse=None
        self.dComment=None
        self.dParseAct=None
        self.dParseStore=None
        self.oRec=None
        self.oSlv=None
        self.lFunc=None
        self.iProcFound=-1
        self.bUnicode=True
        self.dLevel={}
        self.iProc=0
        self.iProcPrev=-1
        self.iStart=-1
        self.iStartCmd=-1
        self.lIdx=[]
        self.lLevelUp=LEVEL_UP
        self.lLevelDn=LEVEL_DOWN
        if len(LEVEL_UP)!=len(LEVEL_DOWN):
            raise vtParserLevelSizeException(LEVEL_UP,LEVEL_DOWN)
        self.iLevelIdx=-1
        self.lLevel=[-1]*len(LEVEL_UP)
        self.fOut=None
        self.args=()
        self.kwargs={}
    def SetOutput(self,f,args=(),kwargs={},bUnicode=True):
        self.fOut=f
        self.args=args
        self.kwargs=kwargs
        self.bUnicode=bUnicode
    def SetOutputSlave(self,f,args=(),kwargs={},bUnicode=True):
        self.oSlv.SetOutput(f,args=args,kwargs=kwargs,bUnicode=bUnicode)
    def SetParseCmd(self,l):
        try:
            self.CMDS=l
            self.oParse.SetState(self.oParse.UNDEF)
            #self.__genParse__()
        except:
            self.__logTB__()
    def SetRecursion(self,iRec=1,iNum=0):
        if iRec>0:
            if iNum>0:
                sOrigin=':'.join(self.GetOrigin().split(':')[:-1]+['%d'%iNum])
            else:
                sOrigin=':'.join([self.GetOrigin(),'%d'%iNum])
            self.oRec=vtParser(sOrigin,self.iMax,self.lLevelUp,self.lLevelDn,
                    max(0,self.VERBOSE-1))
            self.oRec.WILDCARD=self.WILDCARD
            if iRec>1:
                self.oRec.SetRecursion(iRec-1,iNum+1)
        else:
            self.oRec=None
    def SetSlave(self):
        iNum=0
        sOrigin=':'.join([self.GetOrigin(),'slv%d'%iNum])
        self.oSlv=vtParser(sOrigin,self.iMax,self.lLevelUp,self.lLevelDn,
                    max(0,self.VERBOSE-1))
        self.oSlv.WILDCARD=self.WILDCARD
    def Init(self,dParse,dComment):
        try:
            self.iProcPrev=0
            vtBuffer.clear(self)
            self.dParse=dParse
            self.dComment=dComment
            self.oParse.SetState(self.oParse.CLR)
            self.oParse.SetState(self.oParse.START)
            if self.oRec is not None:
                self.oRec.Init()
            if self.oSlv is not None:
                self.oSlv.Init()
        except:
            self.__logTB__()
    def Reset(self):
        try:
            vtBuffer.clear(self)
            self.oParse.SetState(self.oParse.UNDEF)
            if self.oRec is not None:
                self.oRec.Reset()
            if self.oSlv is not None:
                self.oSlv.Reset()
        except:
            self.__logTB__()
    def Clear(self):
        try:
            self.Parse(None)
            self.oParse.SetState(self.oParse.END)
            self.oParse.SetState(self.oParse.CLR)
            vtBuffer.clear(self)
            if self.oRec is not None:
                self.oRec.Clear()
            if self.oSlv is not None:
                self.oSlv.Clear()
        except:
            self.__logTB__()
    def __getLevelIdx__(self,c):
        return self.dLevel.get(c,0)
        if c in self.dLevel:
            return self.dLevel[c]
        return 0
        if c in self.lLevelUp:
            idx=self.lLevelUp.index(c)
            return idx+1
        elif c in self.lLevelDn:
            idx=self.lLevelDn.index(c)
            return -idx-1
        return 0
    def __mapParseDict__(self,d,lS=None,iLv=0,iLimit=20):
        bFirst=False
        if lS is None:
            lS=['']
            bFirst=True
        if type(d)==ListType:
            return
        keys=d.keys()
        keys.sort()
        for k in keys:
            if type(d[k])==IntType:
                lS.append(''.join([('  '*iLv),'%4d'%k,':',str(d[k])]))
            elif type(d[k])==ListType:
                lS.append(''.join([('  '*iLv),'%4s'%repr(k),':',self.__logFmt__(d[k])]))
            else:
                lS.append(''.join([('  '*iLv),'%4s'%repr(k),':']))
                if iLimit>0:
                    self.__mapParseDict__(d[k],lS,iLv+1,iLimit-1)
        if bFirst:
            return ';'.join(lS)
    def __genParse__(self):
        if self.CMDS is None:
            self.dParse={}
            self.dParseAct=self.dParse
            return
        self.dParse={}
        self.dComment={}
        self.dParseAct=self.dParse
        for t in self.CMDS:
            if t[1] is None:
                d=self.dComment
            else:
                d=self.dParse
            dLast=None
            for c in t[0]:
                dLast=d
                cLast=c
                if c not in d:
                    d[c]={}
                    if c in self.lLevelUp:
                        idx=self.lLevelUp.index(c)
                        d=d[c]
                        d[0]=idx+1
                    elif c in self.lLevelDn:
                        idx=self.lLevelDn.index(c)
                        d=d[c]
                        d[0]=-idx-1
                    else:
                        if c==self.WILDCARD:
                            d[1]=0          # 090326:wro
                            d[2]=0          # 090326:wro
                            pass
                        else:
                            d=d[c]
                else:
                    if c==self.WILDCARD:
                        pass
                    else:
                        d=d[c]
            if t[1] is None:
                d[-1]=lCall=[0,t[1]]
                d[' ']=lCall=[0,t[1]]
            else:
                if len(self.END)==0:
                    dLast[c]=[1,t[1]]
                    if len(t)>2:
                        dLast[c].append(t[2])
                    else:
                        dLast[c].append(None)
                else:
                    for c in self.END:
                        lCall=[0,t[1]]
                        if c not in d:
                                d[c]=lCall
                        else:
                            vtLog.vtLngCur(vtLog.CRITICAL,'cmd:%s;dParse;%s'%(t[0],
                                    vtLog.pformat(self.dParse)),self.GetOrigin())
            if self.VERBOSE>2:
                if self.__isLogDebug__():
                    self.__logDebug__('cmd:%s;dParse;%s'%(t[0],self.__logFmt__(self.dParse)))
        if self.__isLogDebug__():
            self.__logDebug__('dParse:%s'%self.__mapParseDict__(self.dParse))
            self.__logDebug__('dComment:%s'%self.__mapParseDict__(self.dComment))
        self.oParse.SetState(self.oParse.START)
        i=1
        for c in self.lLevelUp:
            self.dLevel[c]=i
            i+=1
        i=-1
        for c in self.lLevelDn:
            self.dLevel[c]=i
            i-=1
    def __getParseAct__(self):
        return self.dParseAct
    def __storeIdxState__(self,iLvIdx=-1,iLv=-1):
        self.lIdx.append((self.iProc,self.oParse.GetState(),iLvIdx,iLv))
    def __getDataByIdx__(self):
        try:
            i=self.buf.tell()
            if len(self.lIdx)>1:
                iO=self.lIdx[0][0]
                iA=self.lIdx[-1][0]
                self.buf.seek(iO)
                return self.buf.read(iA-iO)
        finally:
            self.buf.seek(i)
        return ''
    def __logObjData__(self,bRaw=False):
        if self.__isLogDebug__()==False:
            return
        try:
            if self.VERBOSE==1:
                if bRaw:
                    sRaw=self.__getDataByIdx__()
                else:
                    sRaw=''
                self.__logDebug__(u'state:%s;%s;iAct:%8d,iProc:%8d,iFill:%8d;iStart:%8d,iStartCmd:%8d;sRaw:%s;lIdx:%s;lLevel:%s %d'%(self.oParse.GetStateNameAct(),self.oParse.GetStrFull(','),
                            self.iAct,self.iProc,self.iFill,self.iStart,self.iStartCmd,self.__logConv__(sRaw),
                            self.__logFmt__(self.lIdx),self.__logFmt__(self.lLevel),self.iLevelIdx),
                            level2skip=1)
            elif self.VERBOSE>1:
                s=self.__getDataByIdx__()
                self.__logDebug__(u'state:%s;%s;iAct:%8d,iProc:%8d,iFill:%8d;iStart:%8d,iStartCmd:%8d;>%s<;lIdx:%s;lLevel:%s %d'%(self.oParse.GetStateNameAct(),self.oParse.GetStrFull(','),
                            self.iAct,self.iProc,self.iFill,self.iStart,self.iStartCmd,self.__logConv__(s),
                            self.__logFmt__(self.lIdx),self.__logFmt__(self.lLevel),self.iLevelIdx),
                            level2skip=1)
        except:
            self.__logDebug__(u'state:%s;%s;iAct:%8d,iProc:%8d,iFill:%8d;lIdx:%s;lLevel:%s %d'%(self.oParse.GetStateNameAct(),self.oParse.GetStrFull(','),
                            self.iAct,self.iProc,self.iFill,
                            self.__logFmt__(self.lIdx),self.__logFmt__(self.lLevel),self.iLevelIdx),
                            level2skip=1)
            self.__logTB__()
    def NotifyUndef(self):
        try:
            self.dParse=None
            self.dComment=None
            self.dParseAct=None
            self.dParseStore=None
            self.lFunc=None
            self.iProcFound=-1
            self.iStart=-1
            self.iStartCmd=-1
            self.dLevel={}
            self.iProc=0
            self.iProcPrev=-1
            self.lIdx=[]
            #self.dParseAct=self.dParse
            if self.VERBOSE>0:
                self.__logObjData__()
        except:
            self.__logTB__()
    def NotifyGen(self):
        try:
            self.__genParse__()
            if self.VERBOSE>0:
                self.__logObjData__()
        except:
            self.__logTB__()
    def NotifyClr(self):
        try:
            #self.dParse=None
            #self.dComment=None
            #self.dParseAct=None
            self.dParseStore=None
            self.lFunc=None
            self.iProcFound=-1
            #self.dLevel={}
            self.iProc=0
            self.iProcPrev=-1
            self.lIdx=[]
            #self.dParseAct=self.dParse
            if self.VERBOSE>0:
                self.__logObjData__()
        except:
            self.__logTB__()
    def __reorganise__(self):
        iAct=self.iAct
        vtBuffer.__reorganise__(self)
        if iAct!=self.iAct:
            self.iProc=self.iProc-(iAct-self.iAct)
            if self.VERBOSE>0:
                self.__logObjData__()
            
    def NotifyStart(self):
        try:
            self.__reorganise__()
            self.iProcPrev=-1
            self.lIdx=[]
            self.lFunc=None
            self.iProcFound=-1
            self.iStart=self.iProc
            self.iStartCmd=-1
            #self.__storeIdxState__()
            self.dParseAct=self.dParse
            self.dParseStore=None
            if self.VERBOSE>1:
                self.__logObjData__()
        except:
            self.__logTB__()
    def NotifyLvUp(self):
        try:
            if self.VERBOSE>1:
                self.__logObjData__()
            if self.iLevelIdx<0:
                self.oParse.SetState(self.oParse.CMD_FLT)
                return
            self.lLevel[self.iLevelIdx]+=1
            self.__storeIdxState__(self.iLevelIdx,self.lLevel[self.iLevelIdx])
            if self.VERBOSE>1:
                self.__logObjData__()
            self.oParse.SetState(self.oParse.LV_DAT)
        except:
            self.__logTB__()
    def NotifyLvData(self):
        try:
            if self.VERBOSE>1:
                self.__logObjData__()
        except:
            self.__logTB__()
    def NotifyLvDn(self):
        try:
            if self.iLevelIdx<0:
                self.oParse.SetState(self.oParse.CMD_FLT)
                return
            self.lLevel[self.iLevelIdx]-=1
            if self.lLevel[self.iLevelIdx]<-1:
                self.oParse.SetState(self.oParse.CMD_FLT)
            self.__storeIdxState__(self.iLevelIdx,self.lLevel[self.iLevelIdx])
            if self.VERBOSE>1:
                self.__logObjData__()
            for iLv in self.lLevel:
                if iLv>-1:
                    self.oParse.SetState(self.oParse.LV_DAT)
                    break
        except:
            self.__logTB__()
    def NotifyWildCardStart(self):
        try:
            self.__storeIdxState__(0,0)
            if self.VERBOSE>1:
                self.__logObjData__()
        except:
            self.__logTB__()
    def NotifyWildCardEnd(self):
        try:
            self.__storeIdxState__(0,0)
            if self.VERBOSE>1:
                self.__logObjData__()
        except:
            self.__logTB__()
    def NotifyCmdStart(self):
        try:
            self.iStartCmd=self.iProc
            self.__storeIdxState__()
            if self.VERBOSE>1:
                self.__logObjData__()
        except:
            self.__logTB__()
    def __get__(self,iO,iA):
        if iO>=0 and iA>=0:
            self.buf.seek(iO)
            if self.VERBOSE>2 and self.__isLogDebug__():
                self.__logDebug__('__get__;iO:%d;iA:%d;diff:%d'%(iO,iA,iA-iO))
            if self.bUnicode:
                return self.buf.read(iA-iO).decode('utf-8')
            else:
                return self.buf.read(iA-iO)
        else:
            if self.bUnicode:
                return u''
            else:
                return ''
    def __write__(self,f,iO,iA):
        if iO<iA and iO>=0:
            self.buf.seek(iO)
            if self.VERBOSE>2 and self.__isLogDebug__():
                self.__logDebug__('__write__;iO:%d;iA:%d;diff:%d'%(iO,iA,iA-iO))
            if self.bUnicode:
                f.write(self.buf.read(iA-iO).decode('utf-8'))
            else:
                f.write(self.buf.read(iA-iO))
    def NotifyCmdOk(self):
        try:
            #self.__storeIdxState__()
            d=self.dParseAct
            if self.VERBOSE>0:
                self.__logObjData__(bRaw=True)
            iO=-1
            iA=-1
            iIdx=-1
            t=d[0]
            if type(t)==StringType:
                v=''
            else:
                v=None
            lLv=[]
            lVal=[]
            iAct=self.buf.tell()
            iO=self.iStart
            iA=self.iStartCmd
            if self.fOut is not None:
                #self.fOut.write(self.__get__(iO,iA))
                self.__write__(self.fOut,iO,iA)
            if v is not None:
                v=self.__get__(iA,iAct)
            iO=-1
            iA=-1
            iOVal=-1
            iAVal=-1
            if self.VERBOSE>0:
                self.__logDebug__('lIdx:%s'%(self.__logFmt__(self.lIdx)))
            for t in self.lIdx:
                cmd=t[1]
                #if cmd==self.oParse.START:
                #    iO=t[0]
                #elif cmd==self.oParse.CMD_STA:
                #    iA=t[0]
                #    if self.fOut is not None:
                #        self.fOut.write(self.__get__(iO,iA))
                #        if v is not None:
                #            v=self.__get__(iA,iAct)
                            #print len(v),type(v),v
                #        iO,iA=-1,-1
                #el
                if cmd==self.oParse.LV_UP:
                    if iO==-1:
                        iIdx=t[2]
                        iO=t[0]+1
                elif cmd==self.oParse.LV_DN:
                    if t[2]==iIdx or iIdx==-1:
                        if t[3]==-1:
                            iA=t[0]
                            lLv.append(self.__get__(iO,iA))
                            iO,iA=-1,-1
                elif cmd==self.oParse.WC_STA:
                    if iOVal==-1:
                        iOVal=t[0]
                elif cmd==self.oParse.WC_END:
                    if iOVal>=0:
                        iAVal=t[0]
                        lVal.append(self.__get__(iOVal,iAVal))
                        iOVal,iAVal=-1,-1
            self.buf.seek(iAct)
            if self.oRec is not None:
                l=[]
                self.oRec.Init(self.dParse,self.dComment)
                for v in lLv:
                    buf=StringIO()
                    self.oRec.SetOutput(buf,bUnicode=False)
                    self.oRec.Parse(v)
                    self.oRec.Parse(None)
                    s=buf.getvalue()
                    su=s.decode('utf-8')
                    l.append(su)
                    self.oRec.SetOutput(None)
                #self.oRec.Clear()
                self.oRec.Reset()
                if self.VERBOSE>0:
                    self.__logDebug__('lLV;old:%s;new:%s'%(
                            self.__logFmt__(lLv),self.__logFmt__(l)))
                lLv=l
            self.iAct=self.iProc
            self.oParse.SetState(self.oParse.START)
            mode=d[0]
            t=d[1]
            
            ttype=type(t)
            if t is None:
                pass
            elif ttype==StringType:
                if self.VERBOSE>0:
                    self.__logDebug__('call str;lLV%s;v:%s'%(
                            self.__logFmt__(lLv),self.__logFmt__(v)))
                try:
                    if hasattr(self,t):
                        getattr(self,t)(self.fOut,lLv,v,*self.args,**self.kwargs)
                except:
                    self.__logTB__()
                    self.__logError__('lLV%s;args:%s;kwargs:%s'%(
                            self.__logFmt__(lLv),
                            self.__logFmt__(self.args),
                            self.__logFmt__(self.kwargs)))
            elif ttype==TupleType:
                iLenT=len(t)
                if iLenT==1:
                    if self.VERBOSE>0:
                        self.__logDebug__('call f1;lLV%s;args:%s;kwargs:%s'%(
                                self.__logFmt__(lLv),
                                self.__logFmt__(self.args),
                                self.__logFmt__(self.kwargs)))
                    try:
                        t[0](self.fOut,lLv,*self.args,**self.kwargs)
                    except:
                        self.__logTB__()
                        self.__logError__('lLV%s;args:%s;kwargs:%s'%(
                                self.__logFmt__(lLv),
                                self.__logFmt__(self.args),
                                self.__logFmt__(self.kwargs)))
                elif iLenT==2:
                    args=t[1]+self.args
                    kwargs=t[2].copy()
                    kwargs.update(self.kwargs)
                    if self.VERBOSE>0:
                        self.__logDebug__('call f2;lLV%s;args:%s;kwargs:%s'%(
                                self.__logFmt__(lLv),self.__logFmt__(args),
                                self.__logFmt__(kwargs)))
                    try:
                        t[0](self.fOut,lLv,*args,**kwargs)
                    except:
                        self.__logTB__()
                        self.__logError__('lLV%s;args:%s;kwargs:%s'%(
                                self.__logFmt__(lLv),
                                self.__logFmt__(self.args),
                                self.__logFmt__(self.kwargs)))
                else:
                    args=t[1]+self.args
                    kwargs=t[2].copy()
                    kwargs.update(self.kwargs)
                    if self.VERBOSE>0:
                        self.__logDebug__('call f3;lLV%s;args:%s;kwargs:%s'%(
                                self.__logFmt__(lLv),self.__logFmt__(args),
                                self.__logFmt__(kwargs)))
                    try:
                        t[0](self.fOut,lLv,*args,**kwargs)
                    except:
                        self.__logTB__()
                        self.__logError__('lLV%s;args:%s;kwargs:%s'%(
                                self.__logFmt__(lLv),
                                self.__logFmt__(self.args),
                                self.__logFmt__(self.kwargs)))
            else:
                if self.VERBOSE>0:
                    self.__logDebug__('call f0;lLV%s;args:%s;kwargs:%s'%(
                                self.__logFmt__(lLv),
                                self.__logFmt__(self.args),
                                self.__logFmt__(self.kwargs)))
                if mode==0:
                    try:
                        t(self.fOut,lLv,*self.args,**self.kwargs)
                    except:
                        self.__logTB__()
                        self.__logError__('lLV%s;args:%s;kwargs:%s'%(
                                self.__logFmt__(lLv),
                                self.__logFmt__(self.args),
                                self.__logFmt__(self.kwargs)))
                elif mode==1:
                    try:
                        lFmt=d[2]
                        if lFmt is None:
                            t(*lVal)
                        else:
                            t(*[f(v) for f,v in zip(lFmt,lVal)])
                    except:
                        self.__logTB__()
                        self.__logError__('lVal:%s'%(self.__logFmt__(lVal)))
            if self.VERBOSE>0:
                self.__logObjData__()
        except:
            self.__logTB__()
        #self.iAct=self.iProc
        #self.oParse.SetState(self.oParse.START)
    def NotifyCmdEnd(self):
        try:
            self.__storeIdxState__()
            if self.VERBOSE>0:
                self.__logObjData__()
        except:
            self.__logTB__()
        self.oParse.SetState(self.oParse.START)
    def NotifyCmdFault(self):
        try:
            if self.VERBOSE>1:
                self.__logObjData__(bRaw=True)
            #self.__storeIdxState__()
            #s=self.__getDataByIdx__()
            if self.fOut is not None:
                #s=self.__get__(self.iStart,self.iProc)
                #self.fOut.write(s)
                self.__write__(self.fOut,self.iStart,self.iProc)
            #self.iProc=self.lIdx[-1][0]
            #self.iProc=self.iStartCmd
            self.buf.seek(self.iAct)
            self.iAct=self.iProc
            self.iProcPrev=self.iAct-1
            if self.VERBOSE>1:
                self.__logObjData__(bRaw=True)
        except:
            self.__logTB__()
        self.oParse.SetState(self.oParse.START)
    def NotifyCmdUnDef(self):
        try:
            if self.VERBOSE>0:
                self.__logObjData__()
        except:
            self.__logTB__()
    def NotifyCommentStart(self):
        try:
            self.__storeIdxState__()
            self.iAct=self.iProc+1
            if self.VERBOSE>1:
                self.__logObjData__()
        except:
            self.__logTB__()
    def NotifyCommentEnd(self):
        try:
            self.__storeIdxState__()
            if self.VERBOSE>1:
                self.__logObjData__()
            if self.oParse.IsCommandAct==False:
                iO=-1
                iA=-1
                iIdx=-1
                lLv=[]
                iAct=self.buf.tell()
                iO=self.iStart
                for t in self.lIdx:
                    cmd=t[1]
                    if cmd==self.oParse.COMM_STA:
                        iA=t[0]
                        if self.fOut is not None:
                            #self.fOut.write(self.__get__(iO,iA))
                            self.__write__(self.fOut,iO,iA)
                            iO,iA=-1,-1
                self.buf.seek(iAct)
            self.iAct=self.iProc
            if self.VERBOSE>1:
                self.__logObjData__(bRaw=True)
        except:
            self.__logTB__()
        if self.oParse.IsCommandAct:
            self.dParseAct=self.dParseStore
        else:
            self.oParse.SetState(self.oParse.START)
    def NotifyEnd(self):
        try:
            if not self.oParse.IsStart:
                if self.lFunc is not None:
                    self.iProc=self.iProcFound
                    self.dParseAct=self.lFunc
                    self.oParse.SetState(self.oParse.CMD_OK)
            #if len(self.lIdx)>0:
            iO=self.iStart
            if iO>=0:
                #iO=self.lIdx[0][0]
                self.buf.seek(iO)
                
                self.__write__(self.fOut,iO,self.iFill)
                #s=self.__get__(self.iFill-iO)#self.buf.read(self.iFill-iO)
                self.iAct=self.iProc=self.iFill
                #if self.fOut is not None:
                #    self.fOut.write(s)
                self.lIdx=[]
                self.iProc=0
                self.iProcPrev=-1
                self.iStart=0
                self.iStartCmd=-1
                vtBuffer.clear(self)
                
            if self.VERBOSE>1:
                self.__logObjData__()
        except:
            self.__logTB__()
        #self.oParse.SetState(self.oParse.CLR)
        self.oParse.SetState(self.oParse.START)
    def Parse(self,s):
        try:
            if self.VERBOSE>0:
                self.__logDebug__(s)
            if s is None:
                self.oParse.SetState(self.oParse.END)
                return
            self.push(s,False)
            #if self.oParse.IsCmdOk:
            #    return
            while self.HandleData(s):
                pass
        except:
            self.__logTB__()
    def ParseSlave(self,s):
        if self.oSlv.oParse.IsUndef:
            self.oSlv.Init(self.dParser,self.dComment)
        self.oSlv.Parse(s)
        if s is None:
            self.oSlv.Reset()
    def HandleData(self,s):
        try:
            if self.VERBOSE>1:
                self.__logObjData__()
            iCount=self.__getDataLen__()
            if self.oParse.IsUndef:
                self.oParse.SetState(self.oParse.GEN)
                iProc=self.iAct
            else:
                iProc=-1
            self.buf.seek(self.iAct)
            if self.iAct<self.iProcPrev:
                self.__logCritical__('recursion detected;iAct:%d;iProcPrev:%d'%(
                            self.iAct,self.iProcPrev))
                return False
            self.iProcPrev=self.iAct
            if self.VERBOSE>2:
                def verbose(fmt,args,d):
                    lS=[fmt%args]
                    self.__mapParseDict__(d,lS,0,1)
                    self.__logDebug__(';'.join(lS),level2skip=1)
            else:
                def verbose(fmt,args,d):
                    pass
            d=self.dParseAct
            for iOfs in xrange(iCount):
                c=self.buf.read(1)
                self.iProc=self.iAct+iOfs
                verbose('iOfs:%d;iProc:%d;c:%s;%d',(iOfs,self.iProc,repr(c),c in d),d)
                if c in d:
                    if self.oParse.IsWildCardAct:
                        self.oParse.SetState(self.oParse.WC_END)
                    dd=d[c]
                    if self.oParse.IsStart:
                        if -1 in dd:
                            self.oParse.SetState(self.oParse.COMM_STA)
                        else:
                            self.oParse.SetState(self.oParse.CMD_STA)
                            d=dd
                            if c in self.END:
                                d=dd[c]
                    else:
                        if -1 in dd:
                            self.iProc+=1
                            self.oParse.SetState(self.oParse.COMM_END)
                            return True
                        if type(dd)==ListType:
                            self.dParseAct=dd
                            self.oParse.SetState(self.oParse.CMD_OK)
                            verbose('ret0;iAct:%d;iProc:%d',(self.iAct,self.iProc),self.dParseAct)
                            return True
                    if 0 in dd:
                        self.iLevelIdx=dd[0]
                        if self.iLevelIdx>0:
                            self.iLevelIdx-=1
                            self.oParse.SetState(self.oParse.LV_UP)
                            if self.lLevel[self.iLevelIdx]==0:
                                d=dd
                        elif self.iLevelIdx<0:
                            self.iLevelIdx=-self.iLevelIdx
                            self.iLevelIdx-=1
                            self.oParse.SetState(self.oParse.LV_DN)
                            if self.lLevel[self.iLevelIdx]==-1:
                                d=dd
                                if ' ' in d:
                                    self.lFunc=d[' ']
                                    self.iProcFound=self.iProc+1
                    else:
                        d=dd
                else:
                    if c in self.dComment:
                        if self.oParse.IsCommentAct==False:
                            self.dParseStore=d
                            self.dParseAct=self.dComment[c]
                            self.oParse.SetState(self.oParse.COMM_STA)
                            return True
                    if self.WILDCARD in d:#c=='*':
                        if self.oParse.IsCommentAct==False:
                            self.oParse.SetState(self.oParse.WC_STA)
                            if self.oParse.IsLevelAct:
                                self.iLevelIdx=self.dLevel.get(c,0)
                                if self.iLevelIdx>0:
                                    self.iLevelIdx=self.iLevelIdx-1
                                    self.oParse.SetState(self.oParse.LV_UP)
                                elif self.iLevelIdx<0:
                                    self.iLevelIdx=-self.iLevelIdx-1
                                    self.oParse.SetState(self.oParse.LV_DN)
                    else:
                        if c in self.WHITESPACE:
                            continue
                        if not self.oParse.IsStart:
                            if self.lFunc is not None:
                                self.iProc=self.iProcFound
                                self.dParseAct=self.lFunc
                                self.oParse.SetState(self.oParse.CMD_OK)
                            else:
                                self.oParse.SetState(self.oParse.CMD_FLT)
                            verbose('ret4;iAct:%d;iProc:%d',(self.iAct,self.iProc),self.dParseAct)
                            return True
            self.iAct=self.iProc   # 090410: wro needed??? +1
            self.dParseAct=d
            if ' ' in d:
                self.lFunc=d[' ']
                self.iProcFound=self.iProc+1
            verbose('ret for;iAct:%d;iProc:%d',(self.iAct,self.iProc),self.dParseAct)
            if self.VERBOSE>1:
                self.__logObjData__()
            return False
        except:
            self.__logTB__()
        return False
