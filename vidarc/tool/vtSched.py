#----------------------------------------------------------------------------
# Name:         vtSched.py
# Purpose:      scheduling object enqueue function calls to allow
#               thread safe FIFO execution
#               add single call using method put
#               add multiple calls using push method
#               signal call completion using pop method
# Author:       Walter Obweger
#
# Created:      20090501
# CVS-ID:       $Id: vtSched.py,v 1.3 2009/05/07 19:06:59 wal Exp $
# Copyright:    (c) 2009 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vtRngBuf import vtRngBuf,vtRngBufItemFunc

class vtSched(vtRngBuf):
    def __init__(self,size):
        vtRngBuf.__init__(self,size,vtRngBufItemFunc)
    def put(self,func,*args,**kwargs):
        try:
            self.semAccess.acquire()
            if self.__free__()>0:
                bCall=self.iFill==0
                self.lSlot[(self.iAct+self.iFill)%self.iSize].Set(func,*args,**kwargs)
                self.iFill+=1
                if bCall:
                    func,args,kwargs=self.lSlot[self.iAct].Get()
                    func(*args,**kwargs)
                return 0
            else:
                return -1
        finally:
            self.semAccess.release()
    def push(self,l,bClr=False):
        try:
            self.semAccess.acquire()
            if bClr:
                self.__empty__()
            if self.__free__()>=len(l):
                bCall=self.iFill==0
                for func,args,kwargs in l:
                    self.lSlot[(self.iAct+self.iFill)%self.iSize].Set(func,*args,**kwargs)
                    self.iFill+=1
                if bCall:
                    func,args,kwargs=self.lSlot[self.iAct].Get()
                    func(*args,**kwargs)
                return 0
            else:
                return -1
        finally:
            self.semAccess.release()
    def __pop__(self):
        self.lSlot[self.iAct].Clr()
        self.iAct=(self.iAct+1)%self.iSize
        self.iFill-=1
        if self.iFill>0:
            func,args,kwargs=self.lSlot[self.iAct].Get()
            func(*args,**kwargs)
            return 1
        return 0
    def pop(self):
        try:
            self.semAccess.acquire()
            if self.iFill==0:
                return -1
            return self.__pop__()
        finally:
            self.semAccess.release()
    def proc(self,func,*args,**kwargs):
        pass
    def __empty__(self):
        iAct=(self.iAct+1)%self.iSize
        i2Clr=self.iFill-1
        for i in xrange(i2Clr):
            self.lSlot[iAct].Clr()
            iAct=(iAct+1)%self.iSize
            self.iFill-=1
    def empty(self):
        try:
            self.semAccess.acquire()
            self.__empty__()
        finally:
            self.semAccess.release()
