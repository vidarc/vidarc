#----------------------------------------------------------------------------
# Name:         __init__.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20070919
# CVS-ID:       $Id: __init__.py,v 1.2 2007/09/20 15:24:16 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

PLUGABLE_SRV='vXmlDjangoSrv'
PLUGABLE_SRV_ORDER=500

import sys
if hasattr(sys, 'importers'):
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=1
else:
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=0

