#----------------------------------------------------------------------------
# Name:         vXmlNodeXmlDjangoSrvAliasRefTravelCost.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080105
# CVS-ID:       $Id: vXmlNodeXmlDjangoSrvAliasRefTravelCost.py,v 1.2 2008/01/10 11:03:45 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.sec.vtSecXmlLogin import vtSecXmlLogin
from vidarc.tool.time.vtTime import vtDateTime

from vidarc.vSrv.vXmlDjangoSrv.vXmlNodeXmlDjangoSrvAliasRef import vXmlNodeXmlDjangoSrvAliasRef
try:
    if vcCust.is2Import(__name__):
        from vidarc.vSrv.vXmlDjangoSrv.vXmlNodeXmlDjangoSrvAliasRefTravelCostPanel import vXmlNodeXmlDjangoSrvAliasRefTravelCostPanel
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeXmlDjangoSrvAliasRefTravelCost(vXmlNodeXmlDjangoSrvAliasRef):
    MAP_MILAGE_CATEGORY={'private':'privCar','company':'compCar'}
    def __init__(self,tagName='AliasRef_vTravelCost'):
        global _
        _=vtLgBase.assignPluginLang('vXmlDjangoSrv')
        vXmlNodeXmlDjangoSrvAliasRef.__init__(self,tagName)
        self.zDt=vtDateTime(True)
    def GetDescription(self):
        return _(u'travel cost alias reference XML server')
    # ---------------------------------------------------------
    # specific
    def __addTrvCostItemPrivData__(self,oParts,node,r,docRef,bCreate=False):
        #print node
        nD=docRef.getChildForced(node,'data')
        nRow=docRef.createSubNode(nD,'row')
        docRef.setNodeText(nRow,'item',r.name)
        docRef.setNodeText(nRow,'receipt','')
        docRef.setNodeText(nRow,'amount','%6.2f'%r.price)
        if bCreate:
            nRow=docRef.createSubNode(nD,'sum')
            docRef.setNodeText(nRow,'item','')
            docRef.setNodeText(nRow,'receipt','')
            docRef.setNodeText(nRow,'amount','')
    def __procTrvCostItemsPrivReadBack__(self,r,docRef,docRefHum,docRefGlbs,
                    srvXml,srvMsg,lAdd):
        if r is None:
            return None
        nodeTrv=docRef.getNodeByIdNum(r.vMES)
        if nodeTrv is None:
            vtLog.vtLngCurCls(vtLog.CRITICAL,'id:%08d;node not found'%(r.vMES),self)
        oReg=docRef.GetReg('travelCostItemsPriv')
        nodeItem=docRef.getChild(nodeTrv,'travelCostItemsPriv')
        if nodeItem is None:
            try:
                nodeItem=oReg.Create(nodeTrv,self.__addTrvCostItemPrivData__,r,docRef,True)
                idPar=docRef.getKeyNum(nodeTrv)
                idAdd=docRef.getKeyNum(nodeItem)
                lAdd.append(idAdd)
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        else:
            self.__addTrvCostItemPrivData__(oReg,nodeItem,r,docRef)
            docRef.AlignNode(nodeItem,iRec=3)
    def __addTrvCostItemCompData__(self,oParts,node,r,docRef,bCreate=False):
        #print node
        nD=docRef.getChildForced(node,'data')
        nRow=docRef.createSubNode(nD,'row')
        docRef.setNodeText(nRow,'item',r.name)
        docRef.setNodeText(nRow,'receipt','')
        docRef.setNodeText(nRow,'amount','%6.2f'%r.price)
        if bCreate:
            nRow=docRef.createSubNode(nD,'sum')
            docRef.setNodeText(nRow,'item','')
            docRef.setNodeText(nRow,'receipt','')
            docRef.setNodeText(nRow,'amount','')
    def __procTrvCostItemsCompReadBack__(self,r,docRef,docRefHum,docRefGlbs,
                    srvXml,srvMsg,lAdd):
        if r is None:
            return None
        nodeTrv=docRef.getNodeByIdNum(r.vMES)
        if nodeTrv is None:
            vtLog.vtLngCurCls(vtLog.CRITICAL,'id:%08d;node not found'%(r.vMES),self)
        oReg=docRef.GetReg('travelCostItemsComp')
        nodeItem=docRef.getChild(nodeTrv,'travelCostItemsComp')
        if nodeItem is None:
            try:
                nodeItem=oReg.Create(nodeTrv,self.__addTrvCostItemCompData__,r,docRef,True)
                idPar=docRef.getKeyNum(nodeTrv)
                idAdd=docRef.getKeyNum(nodeItem)
                lAdd.append(idAdd)
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        else:
            self.__addTrvCostItemCompData__(oReg,nodeItem,r,docRef)
            docRef.AlignNode(nodeItem,iRec=3)
    def __addTrvCostTimesData__(self,oParts,node,r,docRef):
        #print node
        nD=docRef.getChildForced(node,'data')
        nRow=docRef.createSubNode(nD,'row')
        docRef.setNodeText(nRow,'type',r.category)
        docRef.setNodeText(nRow,'country',r.country)
        docRef.setNodeText(nRow,'loc',r.location)
        docRef.setNodeText(nRow,'dt','T'.join([
                r.start_time.isoformat(),
                r.start_time.isoformat(),
                u'+01:00']))
        docRef.setNodeText(nRow,'milage',unicode(r.milage))
        docRef.setNodeText(nRow,'company',r.company)
        docRef.setNodeText(nRow,'contact',r.contact)
    def __procTrvCostTimesReadBack__(self,r,docRef,docRefHum,docRefGlbs,
                    srvXml,srvMsg,lAdd):
        if r is None:
            return None
        nodeTrv=docRef.getNodeByIdNum(r.vMES)
        if nodeTrv is None:
            vtLog.vtLngCurCls(vtLog.CRITICAL,'id:%08d;node not found'%(r.vMES),self)
        oReg=docRef.GetReg('travelCostTimes')
        nodeDest=docRef.getChild(nodeTrv,'travelCostTimes')
        if nodeDest is None:
            try:
                nodeDest=oReg.Create(nodeTrv,self.__addTrvCostTimesData__,r,docRef)
                idPar=docRef.getKeyNum(nodeTrv)
                idAdd=docRef.getKeyNum(nodeDest)
                lAdd.append(idAdd)
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        else:
            self.__addTrvCostTimesData__(oReg,nodeDest,r,docRef)
            docRef.AlignNode(nodeDest,iRec=3)
    def __addTrvCostPassengersData__(self,oParts,node,r,docRef):
        #print node
        nD=docRef.getChildForced(node,'data')
        nRow=docRef.createSubNode(nD,'row')
        docRef.setNodeText(nRow,'surname',r.surname)
        docRef.setNodeText(nRow,'firstname',r.firstname)
    def __procTrvCostPassengersReadBack__(self,r,docRef,docRefHum,docRefGlbs,
                    srvXml,srvMsg,lAdd):
        if r is None:
            return None
        nodeTrv=docRef.getNodeByIdNum(r.vMES)
        if nodeTrv is None:
            vtLog.vtLngCurCls(vtLog.CRITICAL,'id:%08d;node not found'%(r.vMES),self)
        oReg=docRef.GetReg('travelCostFellowPassenger')
        nodePass=docRef.getChild(nodeTrv,'travelCostFellowPassenger')
        if nodePass is None:
            try:
                nodePass=oReg.Create(nodeTrv,self.__addTrvCostPassengersData__,r,docRef)
                idPar=docRef.getKeyNum(nodeTrv)
                idAdd=docRef.getKeyNum(nodePass)
                lAdd.append(idAdd)
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        else:
            self.__addTrvCostPassengersData__(oReg,nodePass,r,docRef)
            docRef.AlignNode(nodePass,iRec=3)
    def __addTrvCostMilageData__(self,oParts,node,r,docRef):
        #print node
        docRef.setNodeText(node,'licenseNr',r.license_number)
        docRef.setNodeText(node,'milageStart',unicode(r.start_milage))
        docRef.setNodeText(node,'milageEnd',unicode(r.end_milage))
        docRef.setNodeText(node,'distance',unicode(r.distance))
        self.MAP_MILAGE_CATEGORY[r.category]
        docRef.setNodeText(node,'category',unicode(r.distance))
    def __procTrvCostMilageReadBack__(self,r,docRef,docRefHum,docRefGlbs,
                    srvXml,srvMsg,lAdd):
        if r is None:
            return None
        nodeTrv=docRef.getNodeByIdNum(r.vMES)
        if nodeTrv is None:
            vtLog.vtLngCurCls(vtLog.CRITICAL,'id:%08d;node not found'%(r.vMES),self)
        oReg=docRef.GetReg('travelCostMilage')
        nodePass=docRef.getChild(nodeTrv,'travelCostMilage')
        if nodePass is None:
            try:
                nodePass=oReg.Create(nodeTrv,self.__addTrvCostMilageData__,r,docRef)
                idPar=docRef.getKeyNum(nodeTrv)
                idAdd=docRef.getKeyNum(nodePass)
                lAdd.append(idAdd)
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        else:
            self.__addTrvCostMilageData__(oReg,nodePass,r,docRef)
            docRef.AlignNode(nodePass,iRec=3)
    def __addTrvCostMilagePassengersData__(self,oParts,node,r,docRef):
        #print node
        nD=docRef.getChildForced(node,'data')
        nRow=docRef.createSubNode(nD,'row')
        docRef.setNodeText(nRow,'surname',r.surname)
        docRef.setNodeText(nRow,'firstname',r.firstname)
        docRef.setNodeText(nRow,'startloc',r.start_location)
        docRef.setNodeText(nRow,'endloc',r.end_location)
        docRef.setNodeText(nRow,'distance',unicode(r.distance))
    def __procTrvCostMilagePassengersReadBack__(self,r,docRef,docRefHum,docRefGlbs,
                    srvXml,srvMsg,lAdd):
        if r is None:
            return None
        nodeTrv=docRef.getNodeByIdNum(r.vMES)
        if nodeTrv is None:
            vtLog.vtLngCurCls(vtLog.CRITICAL,'id:%08d;node not found'%(r.vMES),self)
        oReg=docRef.GetReg('travelCostMilage')
        nodePass=docRef.getChild(nodeTrv,'travelCostMilage')
        if nodePass is None:
            try:
                nodePass=oReg.Create(nodeTrv,self.__addTrvCostMilagePassengersData__,r,docRef)
                idPar=docRef.getKeyNum(nodeTrv)
                idAdd=docRef.getKeyNum(nodePass)
                lAdd.append(idAdd)
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        else:
            self.__addTrvCostMilagePassengersData__(oReg,nodePass,r,docRef)
            docRef.AlignNode(nodePass,iRec=3)
    def __setTrvCostData__(self,oTrv,node,r,docRef,sTag,idHum,docRefGlbs):
        self.zDt.Now()
        sDt='now'
        sDtTm=self.zDt.GetDateTimeStr(' ')
        sDt=sDtTm[:10]
        oTrv.SetTag(node,sTag+' '+sDt)
        oTrv.SetName(node,r.description,lang='en')
        oTrv.SetName(node,r.description,lang='de')
        #sOrderType='Bestellart: ???'
        #if r.orderType=='me':
        #    sOrderType='Bestellart: Anforderer bestellt selbst'
        #elif r.orderType=='me':
        #    sOrderType='Bestellart: Sektritariat moege bestellen'
        #sDeliverer='Lieferant: %s'%r.deliveredBy
        #sDelivererContact='Kontakt: %s'%r.deliverContact
        #oTrv.SetDesc(node,u'\n'.join([sOrderType,sDeliverer,sDelivererContact]),lang='en')
        #oTrv.SetDesc(node,u'\n'.join([sOrderType,sDeliverer,sDelivererContact]),lang='de')
        oTrv.SetPrjId(node,r.prj_id)
        oTrv.SetTravelerId(node,idHum)
        oTrv.SetCurrency(node,u'EUR')
        trvIds=docRef.GetSortedTravelCostNrIds()
        oTrv.SetTravelCostID(node,unicode(trvIds.GetFirstFreeId()))
        #oTrv.SetSupplierStr(node,r.deliveredBy)
        #oTrv.SetSupplierContactStr(node,r.deliverContact)
        oTrv.SetStartDtTm(node,
            u'T'.join([
                r.start_date.isoformat(),
                r.start_time.isoformat(),
                u'+01:00'
                ]))
        oTrv.SetEndDtTm(node,
            u'T'.join([
                r.end_date.isoformat(),
                r.end_time.isoformat(),
                u'+01:00'
                ]))
        oTrv.SetOwner(node,idHum)
        #print node
    def __setTrvCostAttrData__(self,oTrvAttr,node,r,docRef,sTag,idHum,docRefGlbs):
        s=r.publish.lower()
        if s.startswith('yes'):
            val='Ja'
        elif s.startswith('ja'):
            val='Ja'
        else:
            val='Nein'
        docRef.setNodeText(node,'Veroeffentlichung',val)
        docRef.setNodeText(node,'PDEnr',r.PDEnr)
    def __procTrvCostReadBack__(self,r,docRef,docRefHum,docRefGlbs,
                    srvXml,srvMsg,lAdd):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        if r is None:
            return None
        t=None
        try:
            idHum=r.orderedBy_id
            nodeHum=docRefHum.getNodeByIdNum(idHum)
            oReg=docRef.GetReg('travelCost')
            oHum=docRefHum.GetRegByNode(nodeHum)
            sTag=oHum.GetName(nodeHum)
            oLogin=vtSecXmlLogin(usr=sTag,usrId=idHum)
            docRef.SetLogin(oLogin)
            nodeTmp=docRef.getBaseNode()
            n=None
            try:
                docRefHum.SetNetMaster(docRef)
                n=oReg.Create(nodeTmp,self.__setTrvCostData__,
                                r,docRef,sTag,idHum,docRefGlbs)
                idPar=docRef.getKeyNum(nodeTmp)
                idAdd=docRef.getKeyNum(n)
                lAdd.append(idAdd)
            except:
                vtLog.vtLngTB(self.__class__.__name__)
            if n is not None:
                try:
                    oAttr=docRef.GetReg('travelCostAttr')
                    nAttr=oAttr.Create(n,self.__setTrvCostAttrData__,
                                    r,docRef,sTag,idHum,docRefGlbs)
                    #idPar=docRef.getKeyNum(nodeTmp)
                    idAddAttr=docRef.getKeyNum(nAttr)
                    lAdd.append(idAddAttr)
                except:
                    vtLog.vtLngTB(self.__class__.__name__)
                
            docRefHum.SetNetMaster(None)
            idPur=self.doc.getKeyNum(n)
            t=(['vMES=%d'%idPur],
                    ['id=%s'%r.id])
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
        return t
    def __procTrvCostFinReadBack__(self,r,docRef,docRefHum,docRefGlbs,
                    srvXml,srvMsg,lAdd):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        if r is None:
            return None
        t=None
        try:
            nodeTrv=docRef.getNodeByIdNum(r.vMES)
            if nodeTrv is None:
                vtLog.vtLngCurCls(vtLog.CRITICAL,'id:%08d;node not found'%(r.vMES),self)
            else:
                t=(['status="chk"'],
                    ['id=%s'%r.id])
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
        return t
    def __postProc__(self,dataInst,srvXml,srvMsg,docRef,d,lAdd):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        try:
            docRefMsg=d['vMsg']['doc']
            
            if len(lAdd):
                docRef.ClearCache()
                docRefMsg.ClearCache()
            for idAdd in lAdd:
                try:
                    n=docRef.getNodeByIdNum(idAdd)
                    docRef.calcFingerPrint(n)
                    nPar=docRef.getParent(n)
                    idPar=docRef.getKeyNum(nPar)
                    sTag=docRef.getTagName(n)
                    try:
                        if sTag=='travelCost':
                            trvIds=docRef.GetSortedTravelCostNrIds()
                            #trvIds.AddNode(n)
                        elif sTag=='travelCostItemsPriv':
                            oItems=docRef.GetRegByNode(n)
                            oItems.GenerateSum(n)
                            #nItems=docRef.getChild(n,'travelItems')
                            #if nItems is not None:
                            #    oItems=docRef.GetRegByNode(nItems)
                            #    oItems.GenerateSum(nItems)
                        elif sTag=='travelCostItemsComp':
                            oItems=docRef.GetRegByNode(n)
                            oItems.GenerateSum(n)
                    except:
                        vtLog.vtLngTB(self.__class__.__name__)
                    val=docRef.GetNodeXmlContent(n,
                                    bFull=False,bIncludeNodes=False)
                    iRet=srvXml.AddNode(docRef.GetApplAlias(),
                                    None,idPar,idAdd,val)
                except:
                    iRet=-1
                    vtLog.vtLngTB(self.__class__.__name__)
                
            iLastIdMsgOld=d['vMsg']['lastId']
            iLastIdMsgNew=docRefMsg.getIds().GetLastId()
            iLastIdMsgOld+=1
            iLastIdMsgNew+=1
            for idAdd in range(iLastIdMsgOld,iLastIdMsgNew):
                n=docRefMsg.getNodeByIdNum(idAdd)
                if n is None:
                    continue
                nPar=docRefMsg.getParent(n)
                idPar=docRefMsg.getKeyNum(nPar)
                try:
                    val=docRefMsg.GetNodeXmlContent(n,
                            bFull=False,bIncludeNodes=False)
                    iRet=srvMsg.AddNode(docRefMsg.GetApplAlias(),
                                    None,idPar,idAdd,val)
                except:
                    vtLog.vtLngTB(self.__class__.__name__)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            self.releaseDict(d,['vHum','vMsg',],sLockName='dom')
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            dataInst.Commit()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            if srvXml is not None:
                srvXml.release()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            if srvMsg is not None:
                srvMsg.release()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        vtLog.vtLngCurCls(vtLog.INFO,'done'%(),self)
    def __procTrvCostWriteResult__(self,r,docRef,docRefHum,srvXml,srvMsg):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        if r is None:
            return None
        t=None
        try:
            idPur=r.vMES
            n=None
            if idPur>=0:
                n=docRef.getNodeByIdNum(idPur)
                oReg=docRef.GetRegByNode(n)
                if oReg is not None:
                    if oReg.GetTagName()=='travelCost':
                        # ok check result
                        iRes=oReg.GetResultInt(n)
                        if iRes==1:
                            # go on vacation
                            t=(['status="go"'],
                                ['id=%s'%r.id])
                        elif iRes==-1:
                            # no-go on vacation
                            t=(['status="ngo"'],
                                ['id=%s'%r.id])
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
        return t
    def __preProc__(self,dataInst,srvXml,srvMsg,docRef,d):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        try:
            if srvXml is not None:
                srvXml.acquire()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            if srvMsg is not None:
                srvMsg.acquire()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        if self.acquireDict(d,['vHum','vMsg',],
                        sLockName='dom',blocking=False)==False:
            vtLog.vtLngCur(vtLog.CRITICAL,'locks could not be acquired'%(),self.__class__.__name__)
            #dataInst.Do(self.__preProcClear__,dataInst,srvXml,srvMsg)
            #return
        self.StoreLastId(d)
        docRef.SetNetDocs(d)
        docRef.GenerateSortedTravelCostNrIds()  # FIXME:ensure valid travel nrs in server
        vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
    def __preProcClear__(self,dataInst,srvXml,srvMsg):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        try:
            if srvXml is not None:
                srvXml.release()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            if srvMsg is not None:
                srvMsg.release()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
    def SynchWithDB(self,node,dataInst,lang='en'):
        vtLog.vtLngCurCls(vtLog.INFO,'id:%s;starting'%(self.doc.getKey(node)),self)
        srvXml=None
        srvMsg=None
        docRef=None
        lAdd=[]
        d={}
        try:
            srvXml=self.GetRefDataInst(node)
            srvMsg=self.GetRefDataInst(node,sTagAliasRef='aliasRefMsg')
            docRef=self.GetRefDoc(node)
            if docRef is None:
                vtLog.vtLngCurCls(vtLog.ERROR,'alias reference doc not found',self)
            nodeBase=docRef.getBaseNode()
            if docRef.hasNodeInfos2Get(self.__class__.__name__)==False:
                docRef.setNodeInfos2Get(['|id','tag','name','description'],
                        name=self.__class__.__name__)
            
            c=docRef.getChildByLst(docRef.getRoot(),['config','vHum','host'])
            alias=docRef.getNodeText(c,'alias')
            docRefHum=self.GetSubRefDoc(node,'vHum:%s'%alias)
            c=docRef.getChildByLst(docRef.getRoot(),['config','vGlobals','host'])
            alias=docRef.getNodeText(c,'alias')
            docRefGlbs=self.GetSubRefDoc(node,'vGlobals:%s'%alias)
            docRefMsg=self.GetRefDoc(node,sTagAliasRef='aliasRefMsg')
            d={ 'vHum':     {'doc':docRefHum,   'bNet':'False'},
                'vGlobals': {'doc':docRefGlbs,  'bNet':'False'},
                'vMsg':     {'doc':docRefMsg,   'bNet':'False'},}
            #docRef.procChildsKeysRec(0,nodeBase,self.__procGlobals__,
            #            docRef,dataInst,lang=lang)
            dataInst.Do(self.__preProc__,dataInst,srvXml,srvMsg,docRef,d)
            dataInst.SelectUpdate('travelcost_travelcostadvanced',
                    u'select * from travelcost_travelcostadvanced where status="req"',
                    self.__procTrvCostReadBack__,docRef,docRefHum,docRefGlbs,
                                    srvXml,srvMsg,lAdd)
            dataInst.Commit()
            dataInst.SelectUpdate('travelcost_travelcostadvanced',
                    u'select * from travelcost_travelcostadvanced,travelcost_travelcostitemadvanced as c where status="req" and c.travel_id=travelcost_travelcostadvanced.id and c.private=1',
                    self.__procTrvCostItemsPrivReadBack__,docRef,docRefHum,docRefGlbs,
                                    srvXml,srvMsg,lAdd)
            dataInst.SelectUpdate('travelcost_travelcostadvanced',
                    u'select * from travelcost_travelcostadvanced,travelcost_travelcostitemadvanced as c where status="req" and c.travel_id=travelcost_travelcostadvanced.id and c.private=0',
                    self.__procTrvCostItemsCompReadBack__,docRef,docRefHum,docRefGlbs,
                                    srvXml,srvMsg,lAdd)
            dataInst.SelectUpdate('travelcost_travelcostadvanced',
                    u'select * from travelcost_travelcostadvanced,travelcost_travelcosttimesadvanced where status="req" and travelcost_travelcosttimesadvanced.travel_id=travelcost_travelcostadvanced.id',
                    self.__procTrvCostTimesReadBack__,docRef,docRefHum,docRefGlbs,
                                    srvXml,srvMsg,lAdd)
            dataInst.SelectUpdate('travelcost_travelcostadvanced',
                    u'select * from travelcost_travelcostadvanced,travelcost_travelcostfellowpassengers where status="req" and travelcost_travelcostfellowpassengers.travel_id=travelcost_travelcostadvanced.id',
                    self.__procTrvCostPassengersReadBack__,docRef,docRefHum,docRefGlbs,
                                    srvXml,srvMsg,lAdd)
            dataInst.SelectUpdate('travelcost_travelcostadvanced',
                    u'select * from travelcost_travelcostadvanced,travelcost_travelcostmilage where status="req" and travelcost_travelcostmilage.travel_id=travelcost_travelcostadvanced.id',
                    self.__procTrvCostMilageReadBack__,docRef,docRefHum,docRefGlbs,
                                    srvXml,srvMsg,lAdd)
            dataInst.SelectUpdate('travelcost_travelcostadvanced',
                    u'select t.id as tc_id,t.vMES,m.id as tcm_id,m.distance as disttotal,p.* from travelcost_travelcostadvanced as t,travelcost_travelcostmilage as m,travelcost_travelcostmilagepassenger as p where status="req" and m.travel_id=t.id and p.travel_milage_id=m.id',
                    self.__procTrvCostMilagePassengersReadBack__,docRef,docRefHum,docRefGlbs,
                                    srvXml,srvMsg,lAdd)
            dataInst.SelectUpdate('travelcost_travelcostadvanced',
                    u'select * from travelcost_travelcostadvanced where status="req"',
                    self.__procTrvCostFinReadBack__,docRef,docRefHum,docRefGlbs,
                                    srvXml,srvMsg,lAdd)
            dataInst.SelectUpdate('travelcost_travelcostadvanced',
                    u'select * from travelcost_travelcostadvanced where status="chk"',
                    self.__procTrvCostWriteResult__,docRef,docRefHum,srvXml,srvMsg)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            dataInst.Do(self.__postProc__,dataInst,srvXml,srvMsg,docRef,d,lAdd)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    # ---------------------------------------------------------
    # inheritance
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x01\xb2IDAT(\x91}R=HBQ\x14\xfe\x94\x0ba\x05\xd5h\xd2\x12Z\xd1\xd0\xf0\xa0\
\xa1\x96\x16\xa1\x86 \xa2\xa2\xc1\xc1\x1cZ\x1a\x1a\x0c^\x199DC\xd4\xe0\xd8"$\
\x05\x0f\xb3\x1f\xa2\xa9\x87I\x8b\xd0`x\xad\xc4\xa4\xff\xe8EMR\x04\xfdry6\
\xdc\xba\xbe^\xd4\xd9\xce\xf9\xcew\xce\xf9\x0e\x9f%JU\x18b6\x11\xc1\xcf\x08\
\xb8}\xc6\xd4\xfa\x0fL\xfd\x8a\xa9\x1b\x00\x11\x83\xa9_\x91B\x1ec\xb7\xa2(\
\x8c1\xaf\xd7\xcb\xeb\x9cl\x15p2\x99\xa4~E\xa4\xdb\xaaz\x97NW3\xc6S\x01}\x11\
\xc2\xe1p\xb1X<8<\x12\xc0N<~\xff\xfa\x9a\xd74Ji6\x9b\x15\x9b--\xf3\x83\x01\
\xb7o\xa0\xc5\xbd\xbe\xbe\xd1\xd9Pu\xf6\xf0&ut\xe7r\xb9L&S14d\xbc\x9eQu6\x11\
\xb1r\x01R\xc8\xd3\xdf\xdf\x97:\xb9v\xd4\xb9\x16#\x91\x97\xe7gS7\x00"u\x95D\
\x03`\x8c\x9d\x16\xf4\xfd\xd5\xdd\xd6\xd6\xe6\xdb\xf6v^\x94..\x1d\x8eZB\x08\
\x80MB\x82r\xacD \x84\xd4T7\x16\xf5\x9aTj\xaf\t\x00\xe0\xdaZ\xd04-\x9f?\xd6u\
\xddf\xb3\xf52&\x85<D\xbc%\x1a\x8d\xbf\xbf9\xed\xa3\r\xf6\xef\x11g=#b\xdc;\
\x00\xc6\xbeN\n\xb8}+k\xab\xd77O\xf8H\x17\xa6\x87\xf52\xa7sb\x1c@/cF\rkG\t\
\x00\x16n\x8d\xdb\xb1\xb9\xfa\xb6\xb6\x93\xcar\xd7\xe4\x14\xfe\x08F\xd5\x92\
\xe8\xc7\xab\xab\x82\xbdV^^\x82<!\x85<A9\x06`f~\xd0H\x08p\xa9<\x99>?\x07`\
\xb4\x06\x80\xa0\x1c3r\xf8[-&\xb7\xe2\xdb\xb0|\xc9\xef\xab\xccn\x15\xf66\xdd\
#4|\x02IP\xc5[\x8cw\xb7\xa2\x00\x00\x00\x00IEND\xaeB`\x82' 
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeXmlDjangoSrvAliasRefTravelCostPanel
        else:
            return None
