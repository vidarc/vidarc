#----------------------------------------------------------------------------
# Name:         vNetXmlDjangoSrv.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060708
# CVS-ID:       $Id: vNetXmlDjangoSrv.py,v 1.1 2007/09/19 21:24:05 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog

from vidarc.vSrv.vXmlDjangoSrv.vXmlXmlDjangoSrv import vXmlXmlDjangoSrv
from vidarc.tool.net.vNetXmlWxGui import *

class vNetXmlDjangoSrv(vXmlXmlDjangoSrv,vNetXmlWxGui):
    def __init__(self,parent,appl,fn='tmpl.xml',attr='id',skip=[],
                pos=(0,0),size=(16,12),synch=False,verbose=0,audit_trail=False):
        vNetXmlWxGui.__init__(self,parent,appl,pos,size,synch=synch,verbose=verbose)
        vXmlXmlDjangoSrv.__init__(self,appl=appl,attr=attr,skip=skip,synch=synch,verbose=verbose,
                    audit_trail=audit_trail)
        vXmlXmlDjangoSrv.SetFN(self,fn)
    def New(self,root='XmlDjangoSrvRoot'):
        vNetXmlWxGui.New(self)
        vXmlXmlDjangoSrv.New(self,root=root,bConnection=self.IsFlag(self.CONN_ACTIVE))
    def GetFN(self):
        sFN=vNetXmlWxGui.GetFN(self)
        if sFN is not None:
            return sFN
        else:
            return vXmlXmlDjangoSrv.GetFN(self)
    def Open(self,fn=None,silent=False,bThread=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCS(vtLog.INFO,'file:%s;slient:%d;thread:%d'%(repr(fn),silent,bThread),origin=self.appl)
        if fn is None:
            sFN=vNetXmlWxGui.GetFN(self)
            if sFN is not None:
                fn=sFN
        if fn is None:
            vXmlXmlDjangoSrv.New(self)
            return -2
        if bThread:
            self.silent=silent
            iRet=vXmlXmlDjangoSrv.Open(self,fn,True)
        else:
            iRet=vXmlXmlDjangoSrv.Open(self,fn)
            if iRet>=0:
                if silent==False:
                    self.NotifyContent()
            else:
                self.New()
        return iRet
    def Save(self,fn=None,encoding='ISO-8859-1'):
        vNetXmlWxGui.SaveApplDocs(self,encoding=encoding)
        return vXmlXmlDjangoSrv.Save(self,fn,encoding)
    def getSetupChildByLst(self,node,lst):
        if lst is None:
            lst=self.lstSetupNode
        return self.docSetup.getChildByLst(node,lst)
    def getChildByLst(self,node,lst):
        if lst is None:
            return None
        return vXmlXmlSrv.getChildByLst(self,node,lst)
    def addNode(self,par,node,func=None,**kwargs):
        vXmlXmlDjangoSrv.addNode(self,par,node)
        vNetXmlWxGui.addNode(self,par,node,func,**kwargs)
    def delNode(self,node):
        vNetXmlWxGui.delNode(self,node)
        vXmlXmlDjangoSrv.delNode(self,node)
    def IsRunning(self):
        if vNetXmlWxGui.__isRunning__(self):
            return True
        if vXmlXmlDjangoSrv.__isRunning__(self):
            return True
        return False
    def __stop__(self):
        vNetXmlWxGui.__stop__(self)
        vXmlXmlDjangoSrv.__stop__(self)
