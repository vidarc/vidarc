#----------------------------------------------------------------------------
# Name:         vXmlNodeXmlDjangoSrvAliasRefTravel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080103
# CVS-ID:       $Id: vXmlNodeXmlDjangoSrvAliasRefTravel.py,v 1.3 2008/01/06 14:32:47 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.sec.vtSecXmlLogin import vtSecXmlLogin
from vidarc.tool.time.vtTime import vtDateTime

from vidarc.vSrv.vXmlDjangoSrv.vXmlNodeXmlDjangoSrvAliasRef import vXmlNodeXmlDjangoSrvAliasRef
try:
    if vcCust.is2Import(__name__):
        from vidarc.vSrv.vXmlDjangoSrv.vXmlNodeXmlDjangoSrvAliasRefTravelPanel import vXmlNodeXmlDjangoSrvAliasRefTravelPanel
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeXmlDjangoSrvAliasRefTravel(vXmlNodeXmlDjangoSrvAliasRef):
    def __init__(self,tagName='AliasRef_vTravel'):
        global _
        _=vtLgBase.assignPluginLang('vXmlDjangoSrv')
        vXmlNodeXmlDjangoSrvAliasRef.__init__(self,tagName)
        self.zDt=vtDateTime(True)
    def GetDescription(self):
        return _(u'travel alias reference XML server')
    # ---------------------------------------------------------
    # specific
    def __addTrvItemData__(self,oParts,node,r,docRef,bCreate=False):
        #print node
        nD=docRef.getChildForced(node,'data')
        nRow=docRef.createSubNode(nD,'row')
        docRef.setNodeText(nRow,'count','%d'%r.countItems)
        docRef.setNodeText(nRow,'item',r.name)
        docRef.setNodeText(nRow,'estimated','True')
        docRef.setNodeText(nRow,'price','%6.2f'%r.price)
        docRef.setNodeText(nRow,'pricesum','%6.2f'%(r.price*r.countItems))
        if bCreate:
            nRow=docRef.createSubNode(nD,'sum')
            docRef.setNodeText(nRow,'count','')
            docRef.setNodeText(nRow,'item','')
            docRef.setNodeText(nRow,'estimated','')
            docRef.setNodeText(nRow,'price','')
            docRef.setNodeText(nRow,'pricesum','')
    def __procTrvItemsReadBack__(self,r,docRef,docRefHum,docRefGlbs,
                    srvXml,srvMsg,lAdd):
        if r is None:
            return None
        nodeTrv=docRef.getNodeByIdNum(r.vMES)
        if nodeTrv is None:
            vtLog.vtLngCurCls(vtLog.CRITICAL,'id:%08d;node not found'%(r.vMES),self)
        oReg=docRef.GetReg('travelItems')
        nodeItem=docRef.getChild(nodeTrv,'travelItems')
        if nodeItem is None:
            try:
                nodeItem=oReg.Create(nodeTrv,self.__addTrvItemData__,r,docRef,True)
                idPar=docRef.getKeyNum(nodeTrv)
                idAdd=docRef.getKeyNum(nodeItem)
                lAdd.append(idAdd)
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        else:
            self.__addTrvItemData__(oReg,nodeItem,r,docRef)
            docRef.AlignNode(nodeItem,iRec=3)
    def __addTrvDestData__(self,oParts,node,r,docRef):
        #print node
        nD=docRef.getChildForced(node,'data')
        nRow=docRef.createSubNode(nD,'row')
        docRef.setNodeText(nRow,'country',r.country)
        docRef.setNodeText(nRow,'location',r.location)
        docRef.setNodeText(nRow,'company',r.company)
        docRef.setNodeText(nRow,'contact',r.contact)
    def __procTrvDestReadBack__(self,r,docRef,docRefHum,docRefGlbs,
                    srvXml,srvMsg,lAdd):
        if r is None:
            return None
        nodeTrv=docRef.getNodeByIdNum(r.vMES)
        if nodeTrv is None:
            vtLog.vtLngCurCls(vtLog.CRITICAL,'id:%08d;node not found'%(r.vMES),self)
        oReg=docRef.GetReg('travelDestinations')
        nodeDest=docRef.getChild(nodeTrv,'travelDestinations')
        if nodeDest is None:
            try:
                nodeDest=oReg.Create(nodeTrv,self.__addTrvDestData__,r,docRef)
                idPar=docRef.getKeyNum(nodeTrv)
                idAdd=docRef.getKeyNum(nodeDest)
                lAdd.append(idAdd)
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        else:
            self.__addTrvDestData__(oReg,nodeDest,r,docRef)
            docRef.AlignNode(nodeDest,iRec=3)
    def __addTrvPassengersData__(self,oParts,node,r,docRef):
        #print node
        nD=docRef.getChildForced(node,'data')
        nRow=docRef.createSubNode(nD,'row')
        docRef.setNodeText(nRow,'surname',r.surname)
        docRef.setNodeText(nRow,'firstname',r.firstname)
    def __procTrvPassengersReadBack__(self,r,docRef,docRefHum,docRefGlbs,
                    srvXml,srvMsg,lAdd):
        if r is None:
            return None
        nodeTrv=docRef.getNodeByIdNum(r.vMES)
        if nodeTrv is None:
            vtLog.vtLngCurCls(vtLog.CRITICAL,'id:%08d;node not found'%(r.vMES),self)
        oReg=docRef.GetReg('travelFellowPassenger')
        nodePass=docRef.getChild(nodeTrv,'travelFellowPassenger')
        if nodePass is None:
            try:
                nodePass=oReg.Create(nodeTrv,self.__addTrvPassengersData__,r,docRef)
                idPar=docRef.getKeyNum(nodeTrv)
                idAdd=docRef.getKeyNum(nodePass)
                lAdd.append(idAdd)
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        else:
            self.__addTrvPassengersData__(oReg,nodePass,r,docRef)
            docRef.AlignNode(nodePass,iRec=3)
    def __setTrvData__(self,oTrv,node,r,docRef,sTag,idHum,docRefGlbs):
        self.zDt.Now()
        sDt='now'
        sDtTm=self.zDt.GetDateTimeStr(' ')
        sDt=sDtTm[:10]
        oTrv.SetTag(node,sTag+' '+sDt)
        oTrv.SetName(node,r.description,lang='en')
        oTrv.SetName(node,r.description,lang='de')
        #sOrderType='Bestellart: ???'
        #if r.orderType=='me':
        #    sOrderType='Bestellart: Anforderer bestellt selbst'
        #elif r.orderType=='me':
        #    sOrderType='Bestellart: Sektritariat moege bestellen'
        #sDeliverer='Lieferant: %s'%r.deliveredBy
        #sDelivererContact='Kontakt: %s'%r.deliverContact
        #oTrv.SetDesc(node,u'\n'.join([sOrderType,sDeliverer,sDelivererContact]),lang='en')
        #oTrv.SetDesc(node,u'\n'.join([sOrderType,sDeliverer,sDelivererContact]),lang='de')
        oTrv.SetPrjId(node,r.prj_id)
        oTrv.SetTravelerId(node,idHum)
        oTrv.SetCurrency(node,u'EUR')
        trvIds=docRef.GetSortedTravelNrIds()
        oTrv.SetTravelID(node,unicode(trvIds.GetFirstFreeId()))
        #oTrv.SetSupplierStr(node,r.deliveredBy)
        #oTrv.SetSupplierContactStr(node,r.deliverContact)
        oTrv.SetStartDtTm(node,
            u'T'.join([
                r.start_date.isoformat(),
                r.start_time.isoformat(),
                u'+01:00'
                ]))
        oTrv.SetEndDtTm(node,
            u'T'.join([
                r.end_date.isoformat(),
                r.end_time.isoformat(),
                u'+01:00'
                ]))
        oTrv.SetOwner(node,idHum)
        #print node
    def __setTrvAttrData__(self,oTrvAttr,node,r,docRef,sTag,idHum,docRefGlbs):
        s=r.publish.lower()
        if s.startswith('yes'):
            val='Ja'
        elif s.startswith('ja'):
            val='Ja'
        else:
            val='Nein'
        docRef.setNodeText(node,'Veroeffentlichung',val)
    def __procTrvReadBack__(self,r,docRef,docRefHum,docRefGlbs,
                    srvXml,srvMsg,lAdd):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        if r is None:
            return None
        t=None
        try:
            idHum=r.orderedBy_id
            nodeHum=docRefHum.getNodeByIdNum(idHum)
            oReg=docRef.GetReg('travel')
            oHum=docRefHum.GetRegByNode(nodeHum)
            sTag=oHum.GetName(nodeHum)
            oLogin=vtSecXmlLogin(usr=sTag,usrId=idHum)
            docRef.SetLogin(oLogin)
            nodeTmp=docRef.getBaseNode()
            n=None
            try:
                docRefHum.SetNetMaster(docRef)
                n=oReg.Create(nodeTmp,self.__setTrvData__,
                                r,docRef,sTag,idHum,docRefGlbs)
                idPar=docRef.getKeyNum(nodeTmp)
                idAdd=docRef.getKeyNum(n)
                lAdd.append(idAdd)
            except:
                vtLog.vtLngTB(self.__class__.__name__)
            if n is not None:
                try:
                    oAttr=docRef.GetReg('travelAttr')
                    nAttr=oAttr.Create(n,self.__setTrvAttrData__,
                                    r,docRef,sTag,idHum,docRefGlbs)
                    #idPar=docRef.getKeyNum(nodeTmp)
                    idAddAttr=docRef.getKeyNum(nAttr)
                    lAdd.append(idAddAttr)
                except:
                    vtLog.vtLngTB(self.__class__.__name__)
                
            docRefHum.SetNetMaster(None)
            idPur=self.doc.getKeyNum(n)
            t=(['vMES=%d'%idPur],
                    ['id=%s'%r.id])
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
        return t
    def __procTrvFinReadBack__(self,r,docRef,docRefHum,docRefGlbs,
                    srvXml,srvMsg,lAdd):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        if r is None:
            return None
        t=None
        try:
            nodeTrv=docRef.getNodeByIdNum(r.vMES)
            if nodeTrv is None:
                vtLog.vtLngCurCls(vtLog.CRITICAL,'id:%08d;node not found'%(r.vMES),self)
            else:
                t=(['status="chk"'],
                    ['id=%s'%r.id])
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
        return t
    def __postProc__(self,dataInst,srvXml,srvMsg,docRef,d,lAdd):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        try:
            docRefMsg=d['vMsg']['doc']
            
            if len(lAdd):
                docRef.ClearCache()
                docRefMsg.ClearCache()
            for idAdd in lAdd:
                try:
                    n=docRef.getNodeByIdNum(idAdd)
                    docRef.calcFingerPrint(n)
                    nPar=docRef.getParent(n)
                    idPar=docRef.getKeyNum(nPar)
                    sTag=docRef.getTagName(n)
                    try:
                        if sTag=='travel':
                            trvIds=docRef.GetSortedTravelNrIds()
                            #trvIds.AddNode(n)
                        elif sTag=='travelItems':
                            oItems=docRef.GetRegByNode(n)
                            oItems.GenerateSum(n)
                            #nItems=docRef.getChild(n,'travelItems')
                            #if nItems is not None:
                            #    oItems=docRef.GetRegByNode(nItems)
                            #    oItems.GenerateSum(nItems)
                    except:
                        vtLog.vtLngTB(self.__class__.__name__)
                    val=docRef.GetNodeXmlContent(n,
                                    bFull=False,bIncludeNodes=False)
                    iRet=srvXml.AddNode(docRef.GetApplAlias(),
                                    None,idPar,idAdd,val)
                except:
                    iRet=-1
                    vtLog.vtLngTB(self.__class__.__name__)
                
            iLastIdMsgOld=d['vMsg']['lastId']
            iLastIdMsgNew=docRefMsg.getIds().GetLastId()
            iLastIdMsgOld+=1
            iLastIdMsgNew+=1
            for idAdd in range(iLastIdMsgOld,iLastIdMsgNew):
                n=docRefMsg.getNodeByIdNum(idAdd)
                if n is None:
                    continue
                nPar=docRefMsg.getParent(n)
                idPar=docRefMsg.getKeyNum(nPar)
                try:
                    val=docRefMsg.GetNodeXmlContent(n,
                            bFull=False,bIncludeNodes=False)
                    iRet=srvMsg.AddNode(docRefMsg.GetApplAlias(),
                                    None,idPar,idAdd,val)
                except:
                    vtLog.vtLngTB(self.__class__.__name__)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            self.releaseDict(d,['vHum','vMsg',],sLockName='dom')
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            dataInst.Commit()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            if srvXml is not None:
                srvXml.release()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            if srvMsg is not None:
                srvMsg.release()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        vtLog.vtLngCurCls(vtLog.INFO,'done'%(),self)
    def __procTrvWriteResult__(self,r,docRef,docRefHum,srvXml,srvMsg):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        if r is None:
            return None
        t=None
        try:
            idPur=r.vMES
            n=None
            if idPur>=0:
                n=docRef.getNodeByIdNum(idPur)
                oReg=docRef.GetRegByNode(n)
                if oReg is not None:
                    if oReg.GetTagName()=='travel':
                        # ok check result
                        iRes=oReg.GetResultInt(n)
                        if iRes==1:
                            # go on vacation
                            t=(['status="go"'],
                                ['id=%s'%r.id])
                        elif iRes==-1:
                            # no-go on vacation
                            t=(['status="ngo"'],
                                ['id=%s'%r.id])
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
        return t
    def __preProc__(self,dataInst,srvXml,srvMsg,docRef,d):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        try:
            if srvXml is not None:
                srvXml.acquire()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            if srvMsg is not None:
                srvMsg.acquire()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        if self.acquireDict(d,['vHum','vMsg',],
                        sLockName='dom',blocking=False)==False:
            vtLog.vtLngCur(vtLog.CRITICAL,'locks could not be acquired'%(),self.__class__.__name__)
            #dataInst.Do(self.__preProcClear__,dataInst,srvXml,srvMsg)
            #return
        self.StoreLastId(d)
        docRef.SetNetDocs(d)
        docRef.GenerateSortedTravelNrIds()  # FIXME:ensure valid travel nrs in server
        vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
    def __preProcClear__(self,dataInst,srvXml,srvMsg):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        try:
            if srvXml is not None:
                srvXml.release()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            if srvMsg is not None:
                srvMsg.release()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
    def SynchWithDB(self,node,dataInst,lang='en'):
        vtLog.vtLngCurCls(vtLog.INFO,'id:%s;starting'%(self.doc.getKey(node)),self)
        srvXml=None
        srvMsg=None
        docRef=None
        lAdd=[]
        d={}
        try:
            srvXml=self.GetRefDataInst(node)
            srvMsg=self.GetRefDataInst(node,sTagAliasRef='aliasRefMsg')
            docRef=self.GetRefDoc(node)
            if docRef is None:
                vtLog.vtLngCurCls(vtLog.ERROR,'alias reference doc not found',self)
            nodeBase=docRef.getBaseNode()
            if docRef.hasNodeInfos2Get(self.__class__.__name__)==False:
                docRef.setNodeInfos2Get(['|id','tag','name','description'],
                        name=self.__class__.__name__)
            
            c=docRef.getChildByLst(docRef.getRoot(),['config','vHum','host'])
            alias=docRef.getNodeText(c,'alias')
            docRefHum=self.GetSubRefDoc(node,'vHum:%s'%alias)
            c=docRef.getChildByLst(docRef.getRoot(),['config','vGlobals','host'])
            alias=docRef.getNodeText(c,'alias')
            docRefGlbs=self.GetSubRefDoc(node,'vGlobals:%s'%alias)
            docRefMsg=self.GetRefDoc(node,sTagAliasRef='aliasRefMsg')
            d={ 'vHum':     {'doc':docRefHum,   'bNet':'False'},
                'vGlobals': {'doc':docRefGlbs,  'bNet':'False'},
                'vMsg':     {'doc':docRefMsg,   'bNet':'False'},}
            #docRef.procChildsKeysRec(0,nodeBase,self.__procGlobals__,
            #            docRef,dataInst,lang=lang)
            dataInst.Do(self.__preProc__,dataInst,srvXml,srvMsg,docRef,d)
            dataInst.SelectUpdate('travel_traveladvanced',
                    u'select * from travel_traveladvanced where status="req"',
                    self.__procTrvReadBack__,docRef,docRefHum,docRefGlbs,
                                    srvXml,srvMsg,lAdd)
            dataInst.Commit()
            dataInst.SelectUpdate('travel_traveladvanced',
                    u'select * from travel_traveladvanced,travel_travelitemadvanced where status="req" and travel_travelitemadvanced.travel_id=travel_traveladvanced.id',
                    self.__procTrvItemsReadBack__,docRef,docRefHum,docRefGlbs,
                                    srvXml,srvMsg,lAdd)
            dataInst.SelectUpdate('travel_traveladvanced',
                    u'select * from travel_traveladvanced,travel_traveldestination where status="req" and travel_traveldestination.travel_id=travel_traveladvanced.id',
                    self.__procTrvDestReadBack__,docRef,docRefHum,docRefGlbs,
                                    srvXml,srvMsg,lAdd)
            dataInst.SelectUpdate('travel_traveladvanced',
                    u'select * from travel_traveladvanced,travel_travelfellowpassengers where status="req" and travel_travelfellowpassengers.travel_id=travel_traveladvanced.id',
                    self.__procTrvPassengersReadBack__,docRef,docRefHum,docRefGlbs,
                                    srvXml,srvMsg,lAdd)
            dataInst.SelectUpdate('travel_traveladvanced',
                    u'select * from travel_traveladvanced where status="req"',
                    self.__procTrvFinReadBack__,docRef,docRefHum,docRefGlbs,
                                    srvXml,srvMsg,lAdd)
            dataInst.SelectUpdate('travel_traveladvanced',
                    u'select * from travel_traveladvanced where status="chk"',
                    self.__procTrvWriteResult__,docRef,docRefHum,srvXml,srvMsg)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            dataInst.Do(self.__postProc__,dataInst,srvXml,srvMsg,docRef,d,lAdd)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    # ---------------------------------------------------------
    # inheritance
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x01\xacIDAT(\x91c\\~n\x07\x03\x12h\xdf3\x9f\x01\x15T\xba$"s\x99\xf0H\x9f+Z\
\x8a\xa6\x9a\x81\x81\x81\x05n\xf0\xb9\xa2\xa5F}\xd1\xc8\xaa\x97.]\xfa\xe7\
\xcf\x9f\xf8\xf8x\x888D3\x13\\\xfa\xf0\xe1\xc3\xe7\x8a\x96\xc2\xb9\xdbw\xecx\
v\xf6\xac\xc0\x9f?\x10.\\\n\xaaa\xf6\xec\xd9\xff\xff\xff\xbfp\xf1\x12\\b\xf7\
\xae]\xcf\xbf\x7f\xbf\xfe\xf8\xf1\xb9s\xe7._\xbe\x0c\xb7\x99\xf9\xb6\xf2o[%\
\xc3X\xaf\xd0\xd3\xa7\xcf\x98\x8a\xfc\xbdz\xe3ZCd\xe5\xd5\xabW\x19\x19\x19\
\xd9\xb8\xb9Y88\xde\xbe}\xfb\xf3\xe7O\r\r\x8dk/\xef\xb5\xef\x99\xcf\xa8\xd7\
\x15\x0ew\xc6\xde\xd5st\x8cm\xb7\x1e<\xa6\xab\xad\xad\xa2\xa2\xb8x\xc92>>~\
\x0b\x0b\x0b\r\r\r\xb8\xf7\x10\xa1\xf4\xe7\xcf\x9f[o\xff\xcd_\xb5O^NI^^f\xcd\
\xda\xf5L\xcc\xcc\x8e\x8eN***\x10\xe3PB\x89\x81\x81\x81\x85\x85EP@\xfd\xff?\
\xc1S\xa7\x8e12\xfds\xd3\x97\xfe\xfc\xfa\xe9\xe3\xc7\x8f\xaf_\xbf\xf6\xef\
\xdf?NNNH0\xb2\xc0\xdd\xb3|\xf9\xae\x9f?T\x9e=\xdd\xf5\xeb\xf7\x83\xd7\xaf\
\x85\xfe}de\xf8+\xc8\xc8\xf1\xe3\xdf\xbf\x7f/_\xbe\x94\x91\x91A\xd8P\xe9\x92\
\xb8b\xf5\xaa\x07\x8f>1\xfc:+\xc8\xf5\xe8\x1f\xbbJxX\x18\x03\x06X}i\x0f\x03\
\x03\x03#$i<)\xeeT\xb2\xb4\xbc\xc9\xc3\xa5\xa1\xae\xee\xe7\x1f\x88\x1c\x83p\
\x00\x898\xa8\x93>\xdc\xbf\xffVR\xaal\xd1B\x06\x06\x06\xac\xaa\x11^\x85P\x8d\
w\xee\x10T\n\x01\x8ch\xa9\x95\x015\xc1b&>\xf4\xd4\x8a\x96\xbc1S;\x00\x8b\x93\
\xc8K\xd1\xf5\xf2O\x00\x00\x00\x00IEND\xaeB`\x82' 
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeXmlDjangoSrvAliasRefTravelPanel
        else:
            return None
