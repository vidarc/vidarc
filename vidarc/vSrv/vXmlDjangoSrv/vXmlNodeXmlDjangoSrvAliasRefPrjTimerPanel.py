#Boa:FramePanel:vXmlNodeXmlDjangoSrvAliasRefPrjTimerPanel
#----------------------------------------------------------------------------
# Name:         vsNetXmlNodeAliasRefPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060711
# CVS-ID:       $Id: vXmlNodeXmlDjangoSrvAliasRefPrjTimerPanel.py,v 1.2 2010/03/29 08:55:34 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import wx.lib.filebrowsebutton
import vidarc.tool.input.vtInputTree
import vidarc.tool.input.vtInputTreeInternal

import sys

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VXMLNODEXMLDJANGOSRVALIASREFPRJTIMERPANEL, 
 wxID_VXMLNODEXMLDJANGOSRVALIASREFPRJTIMERPANELCBEXPORTBMD, 
 wxID_VXMLNODEXMLDJANGOSRVALIASREFPRJTIMERPANELDBBEXPORT, 
 wxID_VXMLNODEXMLDJANGOSRVALIASREFPRJTIMERPANELLBLREF, 
 wxID_VXMLNODEXMLDJANGOSRVALIASREFPRJTIMERPANELTXTEXPORTRES, 
 wxID_VXMLNODEXMLDJANGOSRVALIASREFPRJTIMERPANELVIREF, 
] = [wx.NewId() for _init_ctrls in range(6)]

class vXmlNodeXmlDjangoSrvAliasRefPrjTimerPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)

    def _init_coll_bxsRef_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblRef, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viRef, 2, border=4, flag=wx.EXPAND)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbExportBMD, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.txtExportRes, 1, border=0, flag=wx.EXPAND)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsRef, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.dbbExport, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=4, vgap=0)

        self.bxsRef = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_bxsRef_Items(self.bxsRef)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self,
              id=wxID_VXMLNODEXMLDJANGOSRVALIASREFPRJTIMERPANEL,
              name=u'vXmlNodeXmlDjangoSrvAliasRefPrjTimerPanel', parent=prnt,
              pos=wx.Point(0, 0), size=wx.Size(312, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 180))
        self.SetAutoLayout(True)

        self.lblRef = wx.StaticText(id=wxID_VXMLNODEXMLDJANGOSRVALIASREFPRJTIMERPANELLBLREF,
              label=u'alias reference', name=u'lblRef', parent=self,
              pos=wx.Point(4, 4), size=wx.Size(94, 24), style=wx.ALIGN_RIGHT)
        self.lblRef.SetMinSize((-1,-1))

        self.viRef = vidarc.tool.input.vtInputTreeInternal.vtInputTreeInternal(id=wxID_VXMLNODEXMLDJANGOSRVALIASREFPRJTIMERPANELVIREF,
              name=u'viRef', parent=self, pos=wx.Point(102, 4),
              size=wx.Size(197, 24), style=0)

        self.dbbExport = wx.lib.filebrowsebutton.DirBrowseButton(buttonText='...',
              dialogTitle=_(u'Choose a directory'),
              id=wxID_VXMLNODEXMLDJANGOSRVALIASREFPRJTIMERPANELDBBEXPORT,
              labelText=_(u'Select a directory:'), parent=self, pos=wx.Point(4,
              32), size=wx.Size(296, 29), startDirectory='.', style=0)

        self.cbExportBMD = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODEXMLDJANGOSRVALIASREFPRJTIMERPANELCBEXPORTBMD,
              bitmap=vtArt.getBitmap(vtArt.Export), label=_(u'Export BMD'),
              name=u'cbExportBMD', parent=self, pos=wx.Point(4, 69),
              size=wx.Size(96, 30), style=0)
        self.cbExportBMD.Bind(wx.EVT_BUTTON, self.OnCbExportBMDButton,
              id=wxID_VXMLNODEXMLDJANGOSRVALIASREFPRJTIMERPANELCBEXPORTBMD)

        self.txtExportRes = wx.TextCtrl(id=wxID_VXMLNODEXMLDJANGOSRVALIASREFPRJTIMERPANELTXTEXPORTRES,
              name=u'txtExportRes', parent=self, pos=wx.Point(108, 69),
              size=wx.Size(192, 30), style=wx.TE_READONLY, value=u'')

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vSrvNet')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.viRef.AddTreeCall('init','SetNodeInfos',['tag','name'])
        self.viRef.AddTreeCall('init','SetGrouping',[],[('tag',''),('name','')])
        self.viRef.SetTagNames2Base([])
        self.viRef.SetTagNames('aliasRef','tag')
        self.viRef.SetShowFullName(True)
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def __Clear__(self):
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
        # add code here
        self.viRef.Clear()
        self.txtExportRes.SetValue('')
    def SetNetDocs(self,d):
        self.__logCritical__('FIXME'%())
        return
        if d.has_key('vHum'):
            dd=d['vHum']
        # add code here
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viRef.SetDoc(doc,bNet)
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viRef.SetNode(node)
            
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viRef.GetNode(node)
            
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viRef.Close()
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viRef.Cancel()
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            # add code here
            self.viRef.Enable(False)
            self.dbbExport.Enable(False)
            self.cbExportBMD.Enable(False)
        else:
            # add code here
            self.viRef.Enable(True)
            self.dbbExport.Enable(True)
            self.cbExportBMD.Enable(True)

    def OnCbExportButton(self, event):
        event.Skip()

    def OnCbExportBMDButton(self, event):
        event.Skip()
