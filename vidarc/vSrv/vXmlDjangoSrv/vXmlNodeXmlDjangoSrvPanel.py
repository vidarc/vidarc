#Boa:FramePanel:vXmlNodeXmlDjangoSrvPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeXmlDjangoPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20070919
# CVS-ID:       $Id: vXmlNodeXmlDjangoSrvPanel.py,v 1.4 2010/03/29 08:55:35 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputText
import wx.lib.buttons
import vidarc.vSrv.net.vsNetSrvControl
import vidarc.tool.xml.vtXmlNodeTagPanel
import vidarc.tool.art.vtArt as vtArt

import sys

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

from vidarc.tool.db.vtDbSrv import vtDbSrv

[wxID_VXMLNODEXMLDJANGOSRVPANEL, wxID_VXMLNODEXMLDJANGOSRVPANELCBSTOP, 
 wxID_VXMLNODEXMLDJANGOSRVPANELCBSYNCH, 
 wxID_VXMLNODEXMLDJANGOSRVPANELCHCUPDATE, 
 wxID_VXMLNODEXMLDJANGOSRVPANELLBLDSN, wxID_VXMLNODEXMLDJANGOSRVPANELLSTLOG, 
 wxID_VXMLNODEXMLDJANGOSRVPANELPBPROC, wxID_VXMLNODEXMLDJANGOSRVPANELVIDSN, 
 wxID_VXMLNODEXMLDJANGOSRVPANELVITAG, wxID_VXMLNODEXMLDJANGOSRVPANELVSSRV, 
] = [wx.NewId() for _init_ctrls in range(10)]

class vXmlNodeXmlDjangoSrvPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_bxsDSN_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblDSN, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viDSN, 6, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.vsSrv, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.cbSynch, 0, border=0, flag=0)
        parent.AddWindow(self.cbStop, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.chcUpdate, 0, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viTag, 0, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsDSN, 0, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsBt, 0, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.lstLog, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.pbProc, 0, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableRow(6)
        parent.AddGrowableCol(0)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=0, rows=7, vgap=0)

        self.bxsDSN = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsDSN_Items(self.bxsDSN)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEXMLDJANGOSRVPANEL,
              name=u'vXmlNodeXmlDjangoSrvPanel', parent=prnt, pos=wx.Point(0,
              0), size=wx.Size(363, 316),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(355, 289))
        self.SetAutoLayout(True)

        self.viTag = vidarc.tool.xml.vtXmlNodeTagPanel.vtXmlNodeTagPanel(id=wxID_VXMLNODEXMLDJANGOSRVPANELVITAG,
              name=u'viTag', parent=self, pos=wx.Point(0, 0), size=wx.Size(355,
              95), style=0)

        self.vsSrv = vidarc.vSrv.net.vsNetSrvControl.vsNetSrvControl(id=wxID_VXMLNODEXMLDJANGOSRVPANELVSSRV,
              name=u'vsSrv', parent=self, pos=wx.Point(0, 132), size=wx.Size(58,
              30), style=0)
        self.vsSrv.Bind(vidarc.vSrv.net.vsNetSrvControl.vEVT_VSNET_SRVCONTROL_STOPPED,
              self.OnVsSrvVsnetSrvcontrolStopped,
              id=wxID_VXMLNODEXMLDJANGOSRVPANELVSSRV)
        self.vsSrv.Bind(vidarc.vSrv.net.vsNetSrvControl.vEVT_VSNET_SRVCONTROL_STARTED,
              self.OnVsSrvVsnetSrvcontrolStarted,
              id=wxID_VXMLNODEXMLDJANGOSRVPANELVSSRV)

        self.lblDSN = wx.StaticText(id=wxID_VXMLNODEXMLDJANGOSRVPANELLBLDSN,
              label=_(u'DSN'), name=u'lblDSN', parent=self, pos=wx.Point(0,
              103), size=wx.Size(50, 21), style=wx.ALIGN_RIGHT)
        self.lblDSN.SetMinSize(wx.Size(54, -1))

        self.cbSynch = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Synch),
              id=wxID_VXMLNODEXMLDJANGOSRVPANELCBSYNCH, label=u'Synch',
              name=u'cbSynch', parent=self, pos=wx.Point(90, 132),
              size=wx.Size(86, 30), style=0)
        self.cbSynch.Bind(wx.EVT_BUTTON, self.OnCbSynchButton,
              id=wxID_VXMLNODEXMLDJANGOSRVPANELCBSYNCH)

        self.lstLog = wx.ListCtrl(id=wxID_VXMLNODEXMLDJANGOSRVPANELLSTLOG,
              name=u'lstLog', parent=self, pos=wx.Point(0, 170),
              size=wx.Size(355, 105), style=wx.LC_REPORT)

        self.cbStop = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Stop),
              id=wxID_VXMLNODEXMLDJANGOSRVPANELCBSTOP, label=u'Stop',
              name=u'cbStop', parent=self, pos=wx.Point(180, 132),
              size=wx.Size(72, 30), style=0)
        self.cbStop.Bind(wx.EVT_BUTTON, self.OnCbStopButton,
              id=wxID_VXMLNODEXMLDJANGOSRVPANELCBSTOP)

        self.pbProc = wx.Gauge(id=wxID_VXMLNODEXMLDJANGOSRVPANELPBPROC,
              name=u'pbProc', parent=self, pos=wx.Point(0, 275), range=1000,
              size=wx.Size(355, 13), style=wx.GA_SMOOTH | wx.GA_HORIZONTAL)
        self.pbProc.SetMinSize(wx.Size(100, 13))
        self.pbProc.SetMaxSize(wx.Size(-1, 13))

        self.viDSN = vidarc.tool.input.vtInputText.vtInputText(id=wxID_VXMLNODEXMLDJANGOSRVPANELVIDSN,
              name=u'viDSN', parent=self, pos=wx.Point(54, 103),
              size=wx.Size(300, 21), style=0)

        self.chcUpdate = wx.Choice(choices=[],
              id=wxID_VXMLNODEXMLDJANGOSRVPANELCHCUPDATE, name=u'chcUpdate',
              parent=self, pos=wx.Point(256, 132), size=wx.Size(82, 21),
              style=0)
        self.chcUpdate.SetMinSize(wx.Size(-1, -1))

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vXmlDjangoSrv')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.viDSN.SetTagName('dsn')
        
        self.lstLog.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
                                    heading=_('message'), width=280)
        self.lstLog.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT,
                                    heading=_('time'), width=120)
        self.MAP_UPDATE=[(_(u'5 min'),'5m'),(_(u'15 min'),'15m'),
                (_(u'30 min'),'30m'),(_(u'1 hr'),'1h'),(_(u'6 hr'),'6h'),
                (_(u'12 hr'),'12h'),
                #(_(u'00:00'),'00:00'),(_(u'04:00'),'04:00')
                ]
        for t in self.MAP_UPDATE:
            self.chcUpdate.Append(t[0])
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def __Clear__(self):
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
        # add code here
        self.viTag.Clear()
        self.viDSN.Clear()
    def SetRegNode(self,obj):
        vtXmlNodePanel.SetRegNode(self,obj)
        self.vsSrv.SetRegNode(obj)
    def SetNetDocs(self,d):
        self.__logCritical__('FIXME'%())
        return
        if d.has_key('vHum'):
            dd=d['vHum']
        # add code here
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viTag.SetDoc(doc,bNet)
        self.viDSN.SetDoc(doc)
        self.vsSrv.SetDoc(doc,bNet)
        self.vsSrv.SetDataSrvClass(vtDbSrv)
        self.vsSrv.SetDataClass(None)
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viTag.SetNode(self.node)
            self.viDSN.SetNode(self.node)
            #oTrans=self.doc.GetRegisteredNode(self.objRegNode.GetTagNameTransition())
            
            self.vsSrv.SetNode(self.node)
            self.vsSrv.SetNetSrv(self.doc.GetServerInstanceByNode(self.node))
            
            zUpd=self.objRegNode.GetUpdateTime(node)
            i=0
            iFound=-1
            for t in self.MAP_UPDATE:
                if t[1]==zUpd:
                    iFound=i
                    break
                i+=1
            if iFound>=0:
                self.chcUpdate.SetSelection(iFound)
            else:
                self.chcUpdate.SetSelection(len(self.MAP_UPDATE)-1)
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viTag.GetNode(node)
            self.viDSN.GetNode(node)
            self.objRegNode.SetUpdateTime(node,
                    self.MAP_UPDATE[self.chcUpdate.GetSelection()][1])
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viTag.Close()
        self.viDSN.Close()
        self.vsSrv.Close()
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viTag.Cancel()
        self.viDSN.Cancel()
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        self.viTag.Lock(flag)
        if flag:
            # add code here
            self.viDSN.Enable(False)
        else:
            # add code here
            self.viDSN.Enable(True)
    def OnVsSrvVsnetSrvcontrolStopped(self, event):
        event.Skip()
        try:
            self.__logInfo__(''%())
            #self.doc.StopServer(self,node)
            #self.doc.DelServerInstanceByNode(self.node)
        except:
            self.__logTB__()
    def OnVsSrvVsnetSrvcontrolStarted(self, event):
        event.Skip()
        try:
            self.__logInfo__(''%())
            netHum=self.doc.GetNetDoc('vHum')
            srv=self.vsSrv.GetNetSrv()
            if srv is None:
                self.__logError__('server is None')
                return
            srvDB=srv.GetDataInstance()
            if srvDB is None:
                self.__logError__('server data-instance is None')
                return
            if srvDB.IsEmpty()==False:
                self.__logCritical__('server data-instance is still busy'%())
            #srvXml.SetHumDoc4SecAcl(netHum)
            #self.objRegNode.AddServerInstanceByNode(self.node,srv)
        except:
            self.__logTB__()
    def OnCbSynchButton(self, event):
        event.Skip()
        try:
            srv=self.vsSrv.GetNetSrv()
            if srv is None:
                self.__logError__('server is None')
                return
            srvDB=srv.GetDataInstance()
            if srvDB is None:
                self.__logError__('server data-instance is None')
                return
            if srvDB.IsEmpty()==False:
                self.__logCritical__('server data-instance is still busy'%())
            self.objRegNode.Synch(self.node,srvDB)
        except:
            self.__logTB__()
    def OnCbStopButton(self, event):
        event.Skip()
