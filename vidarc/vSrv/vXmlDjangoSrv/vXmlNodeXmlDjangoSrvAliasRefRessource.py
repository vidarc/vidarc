#----------------------------------------------------------------------------
# Name:         vXmlNodeXmlDjangoSrvAliasRefRessource.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20070924
# CVS-ID:       $Id: vXmlNodeXmlDjangoSrvAliasRefRessource.py,v 1.2 2007/10/02 03:37:02 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.sec.vtSecXmlLogin import vtSecXmlLogin
from vidarc.tool.time.vtTime import vtDateTime

from vidarc.vSrv.vXmlDjangoSrv.vXmlNodeXmlDjangoSrvAliasRef import vXmlNodeXmlDjangoSrvAliasRef
try:
    if vcCust.is2Import(__name__):
        from vidarc.vSrv.vXmlDjangoSrv.vXmlNodeXmlDjangoSrvAliasRefRessourcePanel import vXmlNodeXmlDjangoSrvAliasRefRessourcePanel
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeXmlDjangoSrvAliasRefRessource(vXmlNodeXmlDjangoSrvAliasRef):
    def __init__(self,tagName='AliasRef_vRessource'):
        global _
        _=vtLgBase.assignPluginLang('vXmlDjangoSrv')
        vXmlNodeXmlDjangoSrvAliasRef.__init__(self,tagName)
        self.zDt=vtDateTime(True)
    def GetDescription(self):
        return _(u'ressource alias reference XML server')
    # ---------------------------------------------------------
    # specific
    def __procVac__(self,node,docRef,dataInst,lang=None):
        try:
            sTag,infos=docRef.getNodeInfos(node,lang=lang,name=self.__class__.__name__)
            if sTag=='vacation':
                d={
                    'id':str(long(infos['|id'])),
                    'name':infos['name'][:],
                    }
                dataInst.UpdateInsert('vacation_vactype',d,['id'],
                            keys4upd=['name'],
                            keys4ins=['id','name'])
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return 0
    def __procGlobals__(self,node,docRef,dataInst,lang=None):
        try:
            sTag,infos=docRef.getNodeInfos(node,lang=lang,name=self.__class__.__name__)
            if sTag=='globalsVacation':
                docRef.procChildsKeysRec(0,node,self.__procVac__,
                                docRef,dataInst,lang=lang)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return 0
    def __setVacData__(self,oVac,node,r,docRef,sTag,idHum,docRefGlbs):
        #print node,r
        #vtLog.CallStack('')
        self.zDt.Now()
        sDt='now'
        sDtTm=self.zDt.GetDateTimeStr(' ')[:19]
        sDt=sDtTm[:10]
        oVac.SetTag(node,sTag+' '+sDt)
        oVac.SetName(node,sTag+' '+sDtTm)
        oVac.SetOwner(node,r.employee_id)
        s=r.start_date.isoformat()+'T'
        #print r.start_time,type(r.start_time)
        try:
            s+=r.start_time.isoformat()
        except:
            s+='00:00:00'
        #print s
        #print r.end_date
        #print r.end_time,type(r.end_time)
        oVac.SetStartDt(node,s)
        s=r.end_date.isoformat()+'T'
        try:
            #s+=r.end_time.isoformat() # FIXME
            s+='23:59:59'
        except:
            s+='23:59:59'
        #print s
        #print docRef.GetLogin()
        oVac.SetEndDt(node,s)
        oVac.SetVacTypeId(node,r.type_id)
        oVac.SetDuration(node,0.0)
    def __procResHumMember__(self,node,docRef,oReg,lGrp,lRes):
        sTagName=docRef.getTagName(node)
        if sTagName=='ressourceHuman':
            grpId=oReg.GetGroupId(node)
            if grpId in lGrp:
                lRes.append(docRef.getKeyNum(node))
        return 0
    def __procVacReadBack__(self,r,docRef,docRefHum,docRefGlbs,srvXml,srvMsg,lAdd):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        if r is None:
            return None
        t=None
        #print r.start_time,type(r.start_time)
        #print r.end_time,type(r.end_time)
        #docRef.acquire('dom')
        try:
            idHum=r.employee_id
            try:
                #docRefHum.acquire('dom')
                nodeHum=docRefHum.getNodeByIdNum(r.employee_id)
                if nodeHum is None:
                    lGrp=None
                else:
                    nodeSec=docRefHum.getChild(nodeHum,'security')
                    if nodeSec is None:
                        lGrp=None
                    else:
                        cc=docRefHum.getChilds(nodeSec,'grp')
                        lGrp=[]
                        for c in cc:
                            if docRefHum.getTagName(c)=='grp':
                                lGrp.append(long(docRefHum.getAttribute(c,'fid')))
            except:
                lGrp=None
                vtLog.vtLngTB(self.__class__.__name__)
            #docRefHum.release('dom')
            if lGrp is None:
                pass
            else:
                n=None
                lRes=[]
                oReg=docRef.GetReg('ressourceHuman')
                docRef.procChildsKeys(docRef.getBaseNode(),
                            self.__procResHumMember__,docRef,oReg,lGrp,lRes)
                iLen=len(lRes)
                if iLen==0:
                    vtLog.vtLngCurCls(vtLog.ERROR,'id:%s;no valid group assignment'%(r.employee_id),self)
                else:
                    if iLen==1:
                        pass
                    else:
                        vtLog.vtLngCurCls(vtLog.ERROR,'id:%s;too many valid group assignment'%(r.employee_id),self)
                    
                    idRes=lRes[0]
                    node=docRef.getNodeByIdNum(idRes)
                    if node is not None:
                        nodeMember=docRef.getChild(node,'ressourceHumanMember')
                        if nodeMember is not None:
                            oUsrRef=docRef.GetRegisteredNode('userReferenced')
                            nodeTmp=None
                            for c in docRef.getChilds(nodeMember,oUsrRef.GetTagName()):
                                #print idHum,oUsrRef.GetUsrId(c),idHum==oUsrRef.GetUsrId(c)
                                if idHum==oUsrRef.GetUsrId(c):
                                    nodeTmp=c
                                    break
                            if nodeTmp is not None:
                                #oReg=docRef.GetRegByNode(nodeMember)
                                oReg=docRef.GetReg('vacation')
                                oHum=docRefHum.GetRegByNode(nodeHum)
                                sTag=oHum.GetName(nodeHum)
                                oLogin=vtSecXmlLogin(usr=sTag,usrId=idHum)
                                docRef.SetLogin(oLogin)
                                
                                #docRefHum.acquire('dom')
                                try:
                                    docRefHum.SetNetMaster(docRef)
                                    n=oReg.Create(nodeTmp,self.__setVacData__,
                                                r,docRef,sTag,idHum,docRefGlbs)
                                    docRefHum.SetNetMaster(None)
                                    idPar=docRef.getKeyNum(nodeTmp)
                                    idAdd=docRef.getKeyNum(n)
                                    lAdd.append(idAdd)
                                    try:
                                        val=docRef.GetNodeXmlContent(n,
                                                bFull=False,bIncludeNodes=False)
                                        iRet=srvXml.AddNode(docRef.GetApplAlias(),
                                                None,idPar,idAdd,val)
                                    except:
                                        iRet=-1
                                        vtLog.vtLngTB(self.__class__.__name__)
                                except:
                                    vtLog.vtLngTB(self.__class__.__name__)
                                    docRefHum.SetNetMaster(None)
                                #docRefHum.release('dom')
                                docRef.SetLogin(None)
                            #if oReg.DoAddVac(self,nodeMember,oLogin.GetUsrId())==False:
                #print nodeHum
                t=(['vMES=%s'%self.doc.getKeyNum(n),'status="chk"'],
                    ['id=%s'%r.id])
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        #docRef.release('dom')
        vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
        return t
    def __procVacWriteResult__(self,r,docRef,docRefHum,srvXml,srvMsg):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        if r is None:
            return None
        t=None
        #docRef.acquire('dom')
        try:
            idVac=r.vMES
            n=None
            if idVac>=0:
                n=docRef.getNodeByIdNum(idVac)
                oReg=docRef.GetRegByNode(n)
                if oReg is not None:
                    if oReg.GetTagName()=='vacation':
                        # ok check result
                        iRes=oReg.GetResultInt(n)
                        if iRes==1:
                            # go on vacation
                            t=(['status="go"'],
                                ['id=%s'%r.id])
                        elif iRes==-1:
                            # no-go on vacation
                            t=(['status="ngo"'],
                                ['id=%s'%r.id])
            #if n is None:
            #    t=self.__addVac__(r,docRef,docRefHum)
            #else:
            #    pass
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        #docRef.release('dom')
        vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
        return t
    def __preProc__(self,dataInst,srvXml,srvMsg,docRef,d):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        try:
            if srvXml is not None:
                srvXml.acquire()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            if srvMsg is not None:
                srvMsg.acquire()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        if self.acquireDict(d,['vHum','vMsg',],
                            sLockName='dom',blocking=False)==False:
            vtLog.vtLngCur(vtLog.CRITICAL,'locks could not be acquired'%(),self.__class__.__name__)
            #dataInst.Do(self.__preProcClear__,dataInst,srvXml,srvMsg)
            #return
        self.StoreLastId(d)
        docRef.SetNetDocs(d)
        vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
    def __preProcClear__(self,dataInst,srvXml,srvMsg):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        try:
            if srvXml is not None:
                srvXml.release()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            if srvMsg is not None:
                srvMsg.release()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
    def __postProc__(self,dataInst,srvXml,srvMsg,docRef,d,lAdd):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        try:
            #docRef=d['vMsg']['doc']
            docRefMsg=d['vMsg']['doc']
            
            if len(lAdd):
                docRef.ClearCache()
                docRefMsg.ClearCache()
            iLastIdMsgOld=d['vMsg']['lastId']
            iLastIdMsgNew=docRefMsg.getIds().GetLastId()
            iLastIdMsgOld+=1
            iLastIdMsgNew+=1
            for idAdd in range(iLastIdMsgOld,iLastIdMsgNew):
                n=docRefMsg.getNodeByIdNum(idAdd)
                if n is None:
                    continue
                nPar=docRefMsg.getParent(n)
                idPar=docRefMsg.getKeyNum(nPar)
                try:
                    val=docRefMsg.GetNodeXmlContent(n,
                            bFull=False,bIncludeNodes=False)
                    iRet=srvMsg.AddNode(docRefMsg.GetApplAlias(),
                                    None,idPar,idAdd,val)
                except:
                    vtLog.vtLngTB(self.__class__.__name__)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            self.releaseDict(d,['vHum','vMsg',],sLockName='dom')
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            dataInst.Commit()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            if srvXml is not None:
                srvXml.release()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            if srvMsg is not None:
                srvMsg.release()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        vtLog.vtLngCurCls(vtLog.INFO,'done'%(),self)
    def SynchWithDB(self,node,dataInst,lang='en'):
        vtLog.vtLngCurCls(vtLog.INFO,'id:%s;starting'%(self.doc.getKey(node)),self)
        srvXml=None
        srvMsg=None
        docRef=None
        lAdd=[]
        d={}
        try:
            #if dataInst.IsRunning():
            #    vtLog.vtLngCurCls(vtLog.CRITICAL,'id:%s;database synch is still busy'%(self.doc.getKey(node)),self)
            #    return
            srvXml=self.GetRefDataInst(node)
            srvMsg=self.GetRefDataInst(node,sTagAliasRef='aliasRefMsg')
            docRef=self.GetRefDoc(node)
            if docRef is None:
                vtLog.vtLngCurCls(vtLog.ERROR,'alias reference doc not found',self)
            nodeBase=docRef.getBaseNode()
            if docRef.hasNodeInfos2Get(self.__class__.__name__)==False:
                docRef.setNodeInfos2Get(['|id','tag','name','description'],
                        name=self.__class__.__name__)
            #docRef.procChildsKeysRec(0,nodeBase,self.__procGlobals__,
            #            docRef,dataInst,lang=lang)
            #dataInst.Commit()
            
            c=docRef.getChildByLst(docRef.getRoot(),['config','vHum','host'])
            alias=docRef.getNodeText(c,'alias')
            docRefHum=self.GetSubRefDoc(node,'vHum:%s'%alias)
            c=docRef.getChildByLst(docRef.getRoot(),['config','vGlobals','host'])
            alias=docRef.getNodeText(c,'alias')
            docRefGlbs=self.GetSubRefDoc(node,'vGlobals:%s'%alias)
            docRefMsg=self.GetRefDoc(node,sTagAliasRef='aliasRefMsg')
            d={ 'vHum':     {'doc':docRefHum,   'bNet':'False'},
                'vGlobals': {'doc':docRefGlbs,  'bNet':'False'},
                'vMsg':     {'doc':docRefMsg,   'bNet':'False'},}
            dataInst.Do(self.__preProc__,dataInst,srvXml,srvMsg,docRef,d)
            dataInst.SelectUpdate('vacation_vacation',
                    u'select * from vacation_vacation where status="req"',
                    self.__procVacReadBack__,docRef,docRefHum,docRefGlbs,srvXml,srvMsg,lAdd)
            dataInst.SelectUpdate('vacation_vacation',
                    u'select * from vacation_vacation where status="chk"',
                    self.__procVacWriteResult__,docRef,docRefHum,srvXml,srvMsg)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            dataInst.Do(self.__postProc__,dataInst,srvXml,srvMsg,docRef,d,lAdd)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        #vtLog.CallStack('')
    # ---------------------------------------------------------
    # inheritance
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x026IDAT8\x8d\x95\x93\xdfKSq\x18\xc6?g;*\xd9\xc5\x0esns\xd3S\x93P\x02\
\x8fP\xd0\x8f\xbb\xa0\xa0p\x9c\x0b/\n!bf\x89\xce.L\xcbD\xa4\x0b\xbbP\xf3\x98\
\xb0 \x9a\t]\xe5\xc59 \xd1\xc5a\xfe\x03\xddD\x97Z\xf7M\xa5\xd6dt\xd1\x0f(\
\xdd\xdb\xd5\x16\x9be\xf5\xc2\x17\xbe<\xdf\xef\xfb\xf2\xf0<\xcf\xab(\x1e/\
\xd5e\xdc\xbf({@`mbE\xa9\xc6<\xff\xfa\xf1wX\xc5\x80\xde\xbeK\xd2\xd0\x1c\x13\
)\xb6\x8b57\xbb\x87\x8157+Rl\x97\x80~X\x06\x06\xfa~\xbd+\x1e/\x8a\xc7K\xe0\
\xd0Q9\x93\x10\xd1\xb4\x19\x89DC\x12\x8b\xb5\x8am\xdbb\xdb\xb6\xc4b\xad\x12\
\x89\x86D\xd3\xa6\xe5\xdc5\x11\xb5V\x95R\x9fR\xd2@\x8amR_\xd7\xcb\xf0\xe8\
\x1b\x1a\x1bO\xd0\xd2\xd2\xcc\xe2b\x1a\x80dr\x88\x8d\x8dM\xf2\xf9\xd7\xa4\
\x16\x8e\xb1+S\xec\x14\xbf(\x00\x9e\x92`\x96u\x19-p\x97`\xf0\x14\x91H\x13\
\xf9|\x0e\xc3\xe8\xc00:\xc8\xe7sD"M\x04\x83\xa79\xe8\xbb\xc5\xb3\xe5\'e\xb1=\
\xa5\xcb2\xeb\xfc\xf8\x1eB\xd7\xa3\x14\n\xdbd2.\xa6\xd9\x85iv\x91\xc9\xb8\
\x14\n\xdb\xe8z\x94o_5f\xb2/\xcaN)\x9dVOY\x90I\xbd\x9b\xa5\xa5\xa7\x18\x86\
\x81ivaY\xf3\x00\x8c\x8f\xdf\xc1uWY[[gp\xb0\x9f\xe9w\xcf\xf7\xb7\xf1\x7f\xaa\
\x82\xc1\xfb\xf9\x97\xa4\xd3\x8f\xc8\xe5>\x90\xc9\xb8\x8c\x8c\xdc\x04 \x95zH\
<n\x12\n5\x91H\\\xe5\xc8\xd4\x85J\x06k\x13+\xca\x15:\xa8\xa9\xcd\x91\xcdn\
\xe1\xf7\x07\x88\xc7M\\w\x15\xd7]%\x1e7\xf1\xfb\x03d\xb3[\x1c\xa8\xff\xc4\
\xa4\xde]\x0eV\x95\x8d\t\x86G\xdf\x12l<I\x8b\xdeL:\xfd\x18\x80\xa1\xa1\x1bld\
7\xf9\x98\x7fEj\xe18\xbb\xdccg\xf7\xb3R\x11$\xbf\xde&g\xaf\x8b\xf8|\x0f\xa4!\
\xa0I8\x1c\x16\xc7q\xc4q\x1c\t\x87\xc3\xd2\x10\xd0\xc4\xe7\x9b\x97\xf3I\x11\
\xc5K9H\xe5\x01\x83\xc9~Q\xebTQU\x9f8\x8e-\x9dVO\xc5q\x1c[T\xd5\'5u^\x19\x1b\
\xbb\xbd7\x89\xd5U\xbd\x91\x7f]\xa6\xfd\x9a\xff\x84\x01\xfc\x04n=\xec{]r\xb6\
\xe7\x00\x00\x00\x00IEND\xaeB`\x82' 
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeXmlDjangoSrvAliasRefRessourcePanel
        else:
            return None
