#----------------------------------------------------------------------------
# Name:         vXmlXmlDjangoSrv.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20070919
# CVS-ID:       $Id: vXmlXmlDjangoSrv.py,v 1.2 2007/11/04 12:42:52 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vidarc.vSrv.vXmlDjangoSrv.vXmlNodeXmlDjangoSrvRoot import vXmlNodeXmlDjangoSrvRoot

import vidarc.vSrv.vXmlDjangoSrv.__register__ as __register__

class vXmlXmlDjangoSrv(vtXmlDomReg):
    VERBOSE=1
    TAGNAME_REFERENCE='tag'
    TAGNAME_ROOT='XmlDjangoSrvRoot'
    def __init__(self,appl='vXmlDjangoSrv',attr='id',skip=[],synch=False,verbose=0,
                    audit_trail=True):
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        try:
            vtXmlDomReg.__init__(self,appl=appl,attr=attr,skip=skip,synch=synch,verbose=verbose,
                        audit_trail=audit_trail)
            oRoot=vXmlNodeXmlDjangoSrvRoot(tagName='root')
            self.RegisterNode(oRoot,False)
            oRoot=vXmlNodeXmlDjangoSrvRoot()
            self.RegisterNode(oRoot,False)
            
            __register__.RegisterNodes(self,oRoot,True)
        except:
            vtLog.vtLngTB(self.appl)
        self.SetDftLanguages()
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'XmlDjangoSrvs')
    #def New(self,revision='1.0',root='XmlSrvRoot',bConnection=False):
    #    vtXmlDomReg.New(self,revision,root)
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elem=self.createSubNode(self.getRoot(),'XmlDjangoSrvs')
        if bConnection==False:
            self.CreateReq()
        #self.createSubNodeTextAttr(elem,'cfg','','id','')
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        elem=self.createSubNode(self.getRoot(),'security_acl')
        self.__checkReqBase__()
        #self.AlignDoc()
