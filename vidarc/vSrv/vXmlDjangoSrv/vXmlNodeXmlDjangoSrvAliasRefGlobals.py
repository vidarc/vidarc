#----------------------------------------------------------------------------
# Name:         vXmlNodeXmlDjangoSrvAliasRefGlobals.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20070924
# CVS-ID:       $Id: vXmlNodeXmlDjangoSrvAliasRefGlobals.py,v 1.2 2007/10/02 03:37:02 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vSrv.vXmlDjangoSrv.vXmlNodeXmlDjangoSrvAliasRef import vXmlNodeXmlDjangoSrvAliasRef
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeXmlDjangoSrvAliasRefGlobals(vXmlNodeXmlDjangoSrvAliasRef):
    def __init__(self,tagName='AliasRef_vGlobals'):
        global _
        _=vtLgBase.assignPluginLang('vXmlDjangoSrv')
        vXmlNodeXmlDjangoSrvAliasRef.__init__(self,tagName)
    def GetDescription(self):
        return _(u'globals alias reference XML server')
    # ---------------------------------------------------------
    # specific
    def __procVac__(self,node,docRef,dataInst,lang=None):
        try:
            sTag,infos=docRef.getNodeInfos(node,lang=lang,name=self.__class__.__name__)
            if sTag=='vacation':
                d={
                    'id':str(long(infos['|id'])),
                    'name':infos['name'][:],
                    }
                dataInst.UpdateInsert('vacation_vactype',d,['id'],
                            keys4upd=['name'],
                            keys4ins=['id','name'])
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return 0
    def __procGlobals__(self,node,docRef,dataInst,lang=None):
        try:
            sTag,infos=docRef.getNodeInfos(node,lang=lang,name=self.__class__.__name__)
            if sTag=='globalsVacation':
                docRef.procChildsKeysRec(0,node,self.__procVac__,
                                docRef,dataInst,lang=lang)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return 0
    def __preProc__(self,dataInst,srvXml,docRef):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        try:
            if srvXml is not None:
                srvXml.acquire()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
    def __postProc__(self,dataInst,srvXml,docRef):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        try:
            dataInst.Commit()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            if srvXml is not None:
                srvXml.release()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        vtLog.vtLngCurCls(vtLog.INFO,'done'%(),self)
    def SynchWithDB(self,node,dataInst,lang='en'):
        srvXml=None
        docRef=None
        try:
            srvXml=self.GetRefDataInst(node)
            docRef=self.GetRefDoc(node)
            if docRef is None:
                vtLog.vtLngCurCls(vtLog.ERROR,'alias reference doc not found',self)
            nodeBase=docRef.getBaseNode()
            if docRef.hasNodeInfos2Get(self.__class__.__name__)==False:
                docRef.setNodeInfos2Get(['|id','tag','name','description'],
                        name=self.__class__.__name__)
            dataInst.Do(self.__preProc__,dataInst,srvXml,docRef)
            dataInst.Do(docRef.procChildsKeysRec,0,nodeBase,self.__procGlobals__,
                        docRef,dataInst,lang=lang)
            #dataInst.Commit()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            dataInst.Do(self.__postProc__,dataInst,srvXml,docRef)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    # ---------------------------------------------------------
    # inheritance
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x8aIDAT8\x8d\xc5S\xc1\t\x800\x10\xcb\xd5y\x14\xbaK\xa1\x8b\xf8\xe8\
\x08"]Dp\x17\x1f\x1d\xa8\xbe*R/\xd8\xe2\xc3\xbcJ\xb8\xa49\xc8\x89\x98\x015\
\xc6\xc5\xe5\x07\t \x85]j\xce\xb4\x0ej\x1c\x00HI\xc0~e(\x86j\x82\x1e|7\xe8\
\x8d~\xc7\xb8\xb8,\xd3\xea\xa9\xc11o\x00\x00\x1b=O\xf0&\xae\xdf\xcd\x06\xad\
\xa0\x06\xf7\xd8\xaf+\xb0\x92\xd8\xe8\xa9\xf8\xea\x01\x13\xb7 \x85]\xfe/\x92\
h\xd7\x08<o\x83\xad\xaa&\xd0\xda\xc9\x1a{\x02\x80\xb5-U\xfa\xca\x90L\x00\x00\
\x00\x00IEND\xaeB`\x82' 
