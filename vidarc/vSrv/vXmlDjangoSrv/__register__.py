#----------------------------------------------------------------------------
# Name:         __register__.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20070919
# CVS-ID:       $Id: __register__.py,v 1.5 2008/01/08 10:33:15 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.vSrv.vXmlDjangoSrv.vXmlNodeXmlDjangoSrv import vXmlNodeXmlDjangoSrv
#from vidarc.vSrv.vXmlDjangoSrv.vXmlNodeXmlDjangoSrvAlias import vXmlNodeXmlDjangoSrvAlias
#from vidarc.vSrv.vXmlDjangoSrv.vXmlNodeXmlDjangoSrvAliasSummary import vXmlNodeXmlDjangoSrvAliasSummary
from vidarc.vSrv.net.vsNetXmlNodeAliasRef import vsNetXmlNodeAliasRef
from vidarc.vSrv.net.vXmlNodeHosts import vXmlNodeHosts
from vidarc.vSrv.vXmlDjangoSrv.vXmlNodeXmlDjangoSrvAliasRefHum import vXmlNodeXmlDjangoSrvAliasRefHum
from vidarc.vSrv.vXmlDjangoSrv.vXmlNodeXmlDjangoSrvAliasRefGlobals import vXmlNodeXmlDjangoSrvAliasRefGlobals
from vidarc.vSrv.vXmlDjangoSrv.vXmlNodeXmlDjangoSrvAliasRefPrj import vXmlNodeXmlDjangoSrvAliasRefPrj
from vidarc.vSrv.vXmlDjangoSrv.vXmlNodeXmlDjangoSrvAliasRefPrjTimer import vXmlNodeXmlDjangoSrvAliasRefPrjTimer
from vidarc.vSrv.vXmlDjangoSrv.vXmlNodeXmlDjangoSrvAliasRefTask import vXmlNodeXmlDjangoSrvAliasRefTask
from vidarc.vSrv.vXmlDjangoSrv.vXmlNodeXmlDjangoSrvAliasRefRessource import vXmlNodeXmlDjangoSrvAliasRefRessource
from vidarc.vSrv.vXmlDjangoSrv.vXmlNodeXmlDjangoSrvAliasRefPurchase import vXmlNodeXmlDjangoSrvAliasRefPurchase
from vidarc.vSrv.vXmlDjangoSrv.vXmlNodeXmlDjangoSrvAliasRefTravel import vXmlNodeXmlDjangoSrvAliasRefTravel
from vidarc.vSrv.vXmlDjangoSrv.vXmlNodeXmlDjangoSrvAliasRefTravelCost import vXmlNodeXmlDjangoSrvAliasRefTravelCost

def RegisterNodes(self,oRoot,bRegAsRoot=True):
    try:
        oXmlSrv=vXmlNodeXmlDjangoSrv()
        #oXmlSrvAlias=vXmlNodeXmlDjangoSrvAlias()
        #oXmlSrvAliasSummary=vXmlNodeXmlDjangoSrvAliasSummary()
        #oXmlHum=vsNetXmlNodeAliasRef(tagName='AliasRefvHum')
        oXmlHum=vXmlNodeXmlDjangoSrvAliasRefHum()
        oXmlGlobals=vXmlNodeXmlDjangoSrvAliasRefGlobals()
        oXmlPrj=vXmlNodeXmlDjangoSrvAliasRefPrj()
        oXmlPrjTimer=vXmlNodeXmlDjangoSrvAliasRefPrjTimer()
        oXmlTask=vXmlNodeXmlDjangoSrvAliasRefTask()
        oXmlRess=vXmlNodeXmlDjangoSrvAliasRefRessource()
        oXmlPur=vXmlNodeXmlDjangoSrvAliasRefPurchase()
        oXmlTrv=vXmlNodeXmlDjangoSrvAliasRefTravel()
        oXmlTrvCost=vXmlNodeXmlDjangoSrvAliasRefTravelCost()
        #oHosts=vXmlNodeHosts()
        
        self.RegisterNode(oXmlSrv,bRegAsRoot)
        #self.RegisterNode(oXmlSrvAlias,False)
        #self.RegisterNode(oXmlSrvAliasSummary,False)
        self.RegisterNode(oXmlHum,False)
        self.RegisterNode(oXmlGlobals,False)
        self.RegisterNode(oXmlPrj,False)
        self.RegisterNode(oXmlPrjTimer,False)
        self.RegisterNode(oXmlTask,False)
        self.RegisterNode(oXmlRess,False)
        self.RegisterNode(oXmlPur,False)
        self.RegisterNode(oXmlTrv,False)
        self.RegisterNode(oXmlTrvCost,False)
        #self.RegisterNode(oHosts,False)
        self.LinkRegisteredNode(oRoot,oXmlSrv)
        #self.LinkRegisteredNode(oXmlSrv,oXmlSrvAlias)
        #self.LinkRegisteredNode(oXmlSrv,oXmlSrvAliasSummary)
        self.LinkRegisteredNode(oXmlSrv,oXmlHum)
        self.LinkRegisteredNode(oXmlSrv,oXmlGlobals)
        self.LinkRegisteredNode(oXmlSrv,oXmlPrj)
        self.LinkRegisteredNode(oXmlSrv,oXmlPrjTimer)
        self.LinkRegisteredNode(oXmlSrv,oXmlTask)
        self.LinkRegisteredNode(oXmlSrv,oXmlRess)
        self.LinkRegisteredNode(oXmlSrv,oXmlPur)
        self.LinkRegisteredNode(oXmlSrv,oXmlTrv)
        self.LinkRegisteredNode(oXmlSrv,oXmlTrvCost)
        #self.LinkRegisteredNode(oXmlSrv,oHosts)
        #self.LinkRegisteredNode(oXmlSrvAlias,oHosts)
        #self.LinkRegisteredNode(oXmlSrvAliasSummary,oHosts)
        return oXmlSrv
    except:
        vtLog.vtLngTB(__name__)
    return None
