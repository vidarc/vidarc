#----------------------------------------------------------------------------
# Name:         vXmlNodeXmlDjangoSrvAliasRefHum.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20070920
# CVS-ID:       $Id: vXmlNodeXmlDjangoSrvAliasRefHum.py,v 1.3 2007/10/02 03:37:02 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase
import time,sha,random

from vidarc.vSrv.vXmlDjangoSrv.vXmlNodeXmlDjangoSrvAliasRef import vXmlNodeXmlDjangoSrvAliasRef
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeXmlDjangoSrvAliasRefHum(vXmlNodeXmlDjangoSrvAliasRef):
    def __init__(self,tagName='AliasRef_vHum'):
        global _
        _=vtLgBase.assignPluginLang('vXmlDjangoSrv')
        vXmlNodeXmlDjangoSrvAliasRef.__init__(self,tagName)
    def GetDescription(self):
        return _(u'human alias reference XML server')
    # ---------------------------------------------------------
    # specific
    def __procUsr__(self,node,oUsr,docRef,dataInst,zNow,lang=None):
        try:
            sTag,infos=docRef.getNodeInfos(node,lang=lang,name=self.__class__.__name__)
            if sTag=='user':
                try:
                    raw_password=str(long(infos['|persID']))
                    algo = 'sha1'
                    salt = sha.new(str(random.random())).hexdigest()[:5]
                    hsh = sha.new(salt+raw_password).hexdigest()
                    sPwd='%s$%s$%s' % (algo, salt, hsh)
                except:
                    raw_password=''
                    algo = 'sha1'
                    salt = sha.new(str(random.random())).hexdigest()[:5]
                    hsh = sha.new(salt+raw_password).hexdigest()
                    sPwd='%s$%s$%s' % (algo, salt, hsh)
                sActive='1'
                try:
                    if infos['company:aktiv']=='nein':
                        sActive='0'
                except:
                    pass
                d={
                    'id':str(long(infos['|id'])),
                    'username':infos['name'][:],
                    'first_name':infos['firstname'][:],
                    'last_name':infos['surname'][:],
                    'password':sPwd,
                    #'email':''.join([infos['firstname'][:],'.',infos['surname'][:],'@vif.intern']),
                    'email':infos['personal:email'][:],
                    'is_staff':sActive,
                    'is_active':sActive,
                    'is_superuser':'0',
                    'last_login':zNow,
                    'date_joined':zNow,
                    }
                dataInst.UpdateInsert('auth_user',d,['id'],
                        keys4upd=['username','first_name','last_name',
                                'email','is_staff','is_active'])
                d={
                    'id':str(long(infos['|id'])),
                    'title':infos['personal:title'][:],
                    'firstname':infos['firstname'][:],
                    'surname':infos['surname'][:],
                    'tel_internal':infos['personal:tel'][:],
                    'tel_mobile':infos['personal:handy'][:],
                    'email':infos['personal:email'][:],
                    'active':sActive,
                    }
                dataInst.UpdateInsert('employee_employee',d,['id'],
                        keys4upd=['firstname','surname','title',
                                'tel_internal','tel_mobile','email','active'])
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return 0
    def __preProc__(self,dataInst,srvXml,docRef):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        try:
            if srvXml is not None:
                srvXml.acquire()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
    def __postProc__(self,dataInst,srvXml,docRef):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        try:
            dataInst.Commit()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            if srvXml is not None:
                srvXml.release()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        vtLog.vtLngCurCls(vtLog.INFO,'done'%(),self)
    def __procHum__(self,nodeUsers,oUsr,docRef,dataInst,zNow,lang):
        docRef.procChildsKeysRec(0,nodeUsers,self.__procUsr__,
                        oUsr,docRef,dataInst,zNow,lang=lang)
    def SynchWithDB(self,node,dataInst,lang='en'):
        srvXml=None
        docRef=None
        try:
            docRef=self.GetRefDoc(node)
            if docRef is None:
                vtLog.vtLngCurCls(vtLog.ERROR,'alias reference doc not found',self)
            nodeBase=docRef.getBaseNode()
            nodeUsers=docRef.getChild(nodeBase,'users')
            oUsr=docRef.GetReg('user')
            if docRef.hasNodeInfos2Get(self.__class__.__name__)==False:
                docRef.setNodeInfos2Get(['|id','name','surname','firstname',
                        '|persID','personal:email','personal:tel',
                        'personal:title','personal:handy',
                        'company:aktiv'],
                        name=self.__class__.__name__)
            zNow=time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))
            dataInst.Do(self.__preProc__,dataInst,srvXml,docRef)
            #dataInst.Do(docRef.__procPrj__,nodeUsers,oUsr,docRef,
            #            dataInst,zNow,lang)
            dataInst.Do(docRef.procChildsKeysRec,0,nodeUsers,self.__procUsr__,
                        oUsr,docRef,dataInst,zNow,lang=lang)
            #dataInst.Commit()
            #nodeAlias=self.doc.getChild(node,'aliasRef')
            #oReg=self.doc.GetRegByNode(nodeAlias)
            #print oReg
            #if oReg is None:
            #    vtLog.vtLngCurCls(vtLog.ERROR,'alias reference not registered',self)
            #    return
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            dataInst.Do(self.__postProc__,dataInst,srvXml,docRef)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    # ---------------------------------------------------------
    # inheritance
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x98IDAT8\x8d\xadS\xb1\r\x800\x0cs\n\xaf0\xc2\x13}\x01\x89\x89?8\xa1\
\xe2\rV^\xe0\nF\x8e\x81\tT5qU\x04\x19\x9d\xd8u#G\xc4UH\xab\r\xfd\xa9@\x00\
\xfb\xb4J\x8a\xb9\xd2A\x0b\x03\x00\xb9\x1d\xb0WY\xdd\x82\xa6\x837\x95\x15\
\x18\xfd\x81\xd1\x1fy\x01f=&2\x916\xf4\xa7t\xf3\xa0\x04\x18a\xd9\x1a\xed\xa0\
\x94\xccz\xff/\xd1\xb2\x99\xeb9@\x87\xc4\x1aL\xb1'\x07,a1\x81\xb9\xda\xa7Uj\
\xea7C\x8c\xeb\xf3\x12\xc5\xbaF@\xdf\x06\xfb\xaa\xe9\xc0J'K\xec\x05\xafK8\
\xb4%M\x8aV\x00\x00\x00\x00IEND\xaeB`\x82" 
