#----------------------------------------------------------------------------
# Name:         vXmlNodeXmlDjangoSrvAliasRefPrjTimer.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20070920
# CVS-ID:       $Id: vXmlNodeXmlDjangoSrvAliasRefPrjTimer.py,v 1.6 2008/03/26 23:09:03 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import calendar

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.time.vtTime import vtDateTime
from vidarc.tool.time.vtTime import vtDateTimeStartEnd

from vidarc.vSrv.vXmlDjangoSrv.vXmlNodeXmlDjangoSrvAliasRef import vXmlNodeXmlDjangoSrvAliasRef
try:
    if vcCust.is2Import(__name__):
        from vidarc.vSrv.vXmlDjangoSrv.vXmlNodeXmlDjangoSrvAliasRefPrjTimerPanel import vXmlNodeXmlDjangoSrvAliasRefPrjTimerPanel
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeXmlDjangoSrvAliasRefPrjTimer(vXmlNodeXmlDjangoSrvAliasRef):
    def __init__(self,tagName='AliasRef_vPrjTimer'):
        global _
        _=vtLgBase.assignPluginLang('vXmlDjangoSrv')
        vXmlNodeXmlDjangoSrvAliasRef.__init__(self,tagName)
    def GetDescription(self):
        return _(u'project timer alias reference XML server')
    # ---------------------------------------------------------
    # specific
    def __procPrjTimer__(self,node,oPrj,docRef,dataInst,lang=None):
        try:
            sTag,infos=docRef.getNodeInfos(node,lang=lang,name=self.__class__.__name__)
            if sTag=='prjtasks':
                if len(infos['|fid'])==0:
                    d={
                        'id':str(long(infos['|id'])),
                        'name':infos['name'][:],
                        }
                    dataInst.UpdateInsert('timer_task',d,['id'],
                            keys4upd=['name'],
                            keys4ins=['id','name'])
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return 0
    def __procPrjTimerReadBack__(self,r,docRef,docRefHum,docRefGlbs,
                    srvXml,zDt,zStartEnd,dT,lAdd):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        if r is None:
            return None
        t=None
        try:
            idHum=r.employee_id
            idPrj=r.prj_id
            idTask=r.task_id
            zDt.SetDateStr(r.date.isoformat())
            zStartEnd.SetDateStr(r.date.isoformat())
            zStartEnd.SetTimeStr(r.start.isoformat())
            zStartEnd.SetEndTimeStr(r.end.isoformat(),':')
            zStartEnd.CalcDiff()
            iYear=zStartEnd.GetYear()
            iMon=zStartEnd.GetMonth()
            iDay=zStartEnd.GetDay()
            if iYear in dT:
                dY=dT[iYear]
            else:
                dY={}
                dT[iYear]=dY
            if iMon in dY:
                dM=dY[iMon]
            else:
                dM={}
                dY[iMon]=dM
            if idHum in dM:
                dH=dM[idHum]
            else:
                dH={'sum':0.0}
                dM[idHum]=dH
            fDiff=zStartEnd.GetHourDiff()+zStartEnd.GetMinuteDiff()/60.0++zStartEnd.GetSecondDiff()/3600.0
            dH['sum']=dH['sum']+fDiff
            if idPrj in dH:
                dP=dH[idPrj]
            else:
                dP={'sum':0.0}
                dH[idPrj]=dP
            dP['sum']=dP['sum']+fDiff
            if idTask in dP:
                dP[idTask]=dP[idTask]+fDiff
            else:
                dP[idTask]=fDiff
            #print 'date year:%04d mon:%02d day:%02d'%(zDt.GetYear(),zDt.GetMonth(),zDt.GetDay())
            #print 'date year:%04d mon:%02d day:%02d %02d:%02d %02d:%02d'%(zStartEnd.GetYear(),
            #        zStartEnd.GetMonth(),zStartEnd.GetDay(),
            #        zStartEnd.GetHour(),zStartEnd.GetMinute(),
            #        zStartEnd.GetHourEnd(),zStartEnd.GetMinuteEnd())
            #print fDiff,vtLog.pformat(dH)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
        return t
    def __procPrjTimerYearReadBack__(self,node,iYear,dY,oYear,
                    docRef,docRefHum,docRefGlbs,srvXml,lFound):
        try:
            sTag=docRef.getTagName(node)
            #vtLog.vtLngCurCls(vtLog.DEBUG,'do;tag:%s'%(sTag),self)
            if sTag==oYear.GetTagName():
                iY=oYear.GetYear(node)
                #vtLog.vtLngCurCls(vtLog.DEBUG,'do;iY:%s %s;iYear:%s %s'%(repr(iY),type(iY),repr(iYear),type(iYear)),self)
                if iYear==iY:
                    #print 'found'
                    lFound.append(node)
                    return -1
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        #vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
        return 0
    def __setYearData__(self,oYear,node,iYear):
        oYear.SetYear(node,u'%04d'%iYear)
    def __procPrjTimerMonReadBack__(self,node,iMon,dM,oMon,
                    docRef,docRefHum,docRefGlbs,srvXml,lFound):
        try:
            sTag=docRef.getTagName(node)
            #vtLog.vtLngCurCls(vtLog.DEBUG,'do;tag:%s'%(sTag),self)
            if sTag==oMon.GetTagName():
                iM=oMon.GetMonth(node)
                #vtLog.vtLngCurCls(vtLog.DEBUG,'do;iM:%s %s;iMon:%s %s'%(repr(iM),type(iM),repr(iMon),type(iMon)),self)
                if iMon==iM:
                    #print 'found'
                    lFound.append(node)
                    return -1
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        #vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
        return 0
    def __setMonthData__(self,oMon,node,iMon):
        oMon.SetMonth(node,u'%02d'%iMon)
    def __setEmployeeData__(self,oEmployee,node,idHum,sLogin):
        oEmployee.SetEmployeeID(node,idHum)
        oEmployee.SetEmployee(node,sLogin)
    def __procPrjTimerEmplyoeeReadBack__(self,node,oEmployee,
                    docRef,docRefHum,docRefGlbs,srvXml,dHumFound):
        #vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        try:
            sTag=docRef.getTagName(node)
            if sTag==oEmployee.GetTagName():
                idHum=long(oEmployee.GetEmployeeID(node))
                if idHum not in dHumFound:
                    dHumFound[idHum]=node
                else:
                    vtLog.vtLngCurCls(vtLog.ERROR,'id:%08d;multiple employee nodes;dHumFound:%s'%(idHum,vtLog.pformat(dHumFound)),self)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        #vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
        return 0
    def __procPrjTask__(self,node,docRef,oPrjTask,dPrjTask):
        #vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        try:
            sTag=docRef.getTagName(node)
            if sTag==oPrjTask.GetTagName():
                t=(oPrjTask.GetProjectID(node),oPrjTask.GetTaskID(node))
                if t in dPrjTask:
                    dPrjTask[t].append(node)
                else:
                    dPrjTask[t]=[0,node]
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        #vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
        return 0
    def __setPrjTaskData__(self,oPrjTask,node,idHum,sLogin,
                idPrj,sPrj,idTask,sTask,sDate,val):
        oPrjTask.SetPersonID(node,idHum)
        oPrjTask.SetPerson(node,sLogin)
        oPrjTask.SetProjectID(node,idPrj)
        oPrjTask.SetProject(node,sPrj)
        oPrjTask.SetTaskID(node,idTask)
        oPrjTask.SetTask(node,sTask)
        oPrjTask.SetDate(node,sDate)
        oPrjTask.SetDuration(node,unicode(val))
    def __procPrjTimerCreateNodesReadBack__(self,dataInst,srvXml,
                docRef,docRefHum,docRefGlbs,docRefPrj,docRefTask,
                dT,d,zDt,lAdd,lEdit):
        try:
            vtLog.vtLngCurCls(vtLog.DEBUG,'started',self)
            #vtLog.CallStack('')
            #print vtLog.pformat(dT)
            nodeBase=docRef.getBaseNode()
            oYear=docRef.GetReg('year')
            oMon=docRef.GetReg('month')
            oEmployee=docRef.GetReg('employee')
            oPrjTask=docRef.GetReg('prjtasksum')
            zDt.Now()
            lSkip=['sum','__id__']
            for iYear,dY in dT.iteritems():
                if iYear in lSkip:
                    continue
                # check/add year
                lFound=[]
                docRef.procChildsKeysRec(0,nodeBase,
                    self.__procPrjTimerYearReadBack__,
                    iYear,dY,oYear,docRef,docRefHum,docRefGlbs,
                    srvXml,lFound)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    #vtLog.vtLngCurCls(vtLog.DEBUG,'%s'%(u''.join([u'tag:',docRef.getTagName(n),u' id:',docRef.getKey(n) for n in lFound])),self)
                    vtLog.vtLngCurCls(vtLog.DEBUG,'%s'%(vtLog.pformat(lFound)),self)
                #print lFound
                if len(lFound)==0:
                    # add year node
                    #print 'node year to add'
                    nYear=oYear.Create(nodeBase,self.__setYearData__,iYear)
                    id=docRef.getKeyNum(nYear)
                    lAdd.append(id)
                    #print nYear
                else:
                    id=docRef.getKeyNum(lFound[-1])
                    nYear=docRef.getNodeByIdNum(id)
                dY['__id__']=id
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurCls(vtLog.DEBUG,'dY:%s'%(vtLog.pformat(dY)),self)
                # check/add month
                for iMon,dM in dY.iteritems():
                    if iMon in lSkip:
                        continue
                    lFound=[]
                    docRef.procChildsKeysRec(0,nYear,
                        self.__procPrjTimerMonReadBack__,
                        iMon,dM,oMon,docRef,docRefHum,docRefGlbs,
                        srvXml,lFound)
                    #print lFound
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        #vtLog.vtLngCurCls(vtLog.DEBUG,'%s'%(u''.join([u'tag:',docRef.getTagName(n),u' id:',docRef.getKey(n) for n in lFound])),self)
                        vtLog.vtLngCurCls(vtLog.DEBUG,'%s'%(vtLog.pformat(lFound)),self)
                    if len(lFound)==0:
                        # add month node
                        #print 'node month to add'
                        nMon=oMon.Create(nYear,self.__setMonthData__,iMon)
                        id=docRef.getKeyNum(nMon)
                        lAdd.append(id)
                        #print nMon
                    else:
                        id=docRef.getKeyNum(lFound[-1])
                        nMon=docRef.getNodeByIdNum(id)
                    dM['__id__']=id
                    dHumFound={}
                    docRef.procChildsKeysRec(0,nMon,
                        self.__procPrjTimerEmplyoeeReadBack__,
                        oEmployee,
                        docRef,docRefHum,docRefGlbs,
                        srvXml,dHumFound)
                    #print vtLog.pformat(dHumFound)
                    #print 'dM',vtLog.pformat(dM)
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurCls(vtLog.DEBUG,'dHumFound:%s'%(vtLog.pformat(dHumFound)),self)
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurCls(vtLog.DEBUG,'dM:%s'%(vtLog.pformat(dM)),self)
                    for idHum,dH in dM.iteritems():
                        if idHum in lSkip:
                            continue
                        if idHum in dHumFound:
                            nEmployee=dHumFound[idHum]
                            nodeHum=docRefHum.getNodeByIdNum(idHum)
                            oHum=docRefHum.GetRegByNode(nodeHum)
                            sLogin=oHum.GetName(nodeHum)
                        else:
                            # add employee node
                            #print 'node employee to add'
                            nodeHum=docRefHum.getNodeByIdNum(idHum)
                            if nodeHum is None:
                                vtLog.vtLngCurCls(vtLog.ERROR,'node for id:%s not found'%(repr(idHum)),self)
                            oHum=docRefHum.GetRegByNode(nodeHum)
                            if oHum is None:
                                vtLog.vtLngCurCls(vtLog.ERROR,'object for node id:%s not found'%(repr(idHum)),self)
                            continue
                            sLogin=oHum.GetName(nodeHum)
                            nEmployee=oEmployee.Create(nMon,self.__setEmployeeData__,idHum,sLogin)
                            idEmployee=docRef.getKeyNum(nEmployee)
                            lAdd.append(idEmployee)
                            #print nEmployee
                            dHumFound[idHum]=nEmployee
                        dPrjTask={}
                        for idPrj,dPrj in dH.iteritems():
                            if idPrj in lSkip:
                                continue
                            for idTask,val in dPrj.iteritems():
                                if idTask in lSkip:
                                    continue
                                dPrjTask[(idPrj,idTask)]=[val]
                        #print 'dPrj',vtLog.pformat(dPrj)
                        #print 'dPrjTask',vtLog.pformat(dPrjTask)
                        #lNodesPrjTask=[]
                        docRef.procChildsKeysRec(0,nEmployee,
                                self.__procPrjTask__,docRef,oPrjTask,dPrjTask)
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCurCls(vtLog.DEBUG,'dPrjTask:%s'%(vtLog.pformat(dPrjTask)),self)
                        sDate=None
                        if zDt.GetYear()==iYear:
                            if zDt.GetMonth()==iMon:
                                sDate=zDt.GetDateSmallStr()
                        if sDate==None:
                            sDate=u'%04d-%02d-%02d'%(iYear,iMon,
                                    calendar.monthrange(iYear,iMon)[1])
                        for t,l in dPrjTask.iteritems():
                            try:
                                nodePrj=docRefPrj.getNodeByIdNum(t[0])
                                oPrj=docRefPrj.GetRegByNode(nodePrj)
                                sPrj=oPrj.GetName(nodePrj)
                            except:
                                vtLog.vtLngTB(self.__class__.__name__)
                                sPrj=u''
                            try:
                                nodeTask=docRefTask.getNodeByIdNum(t[1])
                                oTask=docRefTask.GetRegByNode(nodeTask)
                                sTask=u'.'+oTask.GetTag(nodeTask)
                            except:
                                vtLog.vtLngTB(self.__class__.__name__)
                                sTask=u''
                            if len(l)==1:
                                # add projecttask node
                                nPrjTask=oPrjTask.Create(nEmployee,
                                            self.__setPrjTaskData__,
                                            idHum,sLogin,
                                            t[0],sPrj,
                                            t[1],sTask,
                                            sDate,l[0])
                                idPrjTask=docRef.getKeyNum(nPrjTask)
                                lAdd.append(idPrjTask)
                                #print nPrjTask
                                
                            else:
                                # edit projecttask node
                                if l[0] is None:
                                    oPrjTask.SetDate(l[1],sDate)
                                    oPrjTask.SetDuration(l[1],0)
                                else:
                                    oPrjTask.SetDate(l[1],sDate)
                                    oPrjTask.SetDuration(l[1],l[0])
                                lEdit.append(docRef.getKeyNum(l[1]))
                        #print 'dPrjTask',vtLog.pformat(dPrjTask)
                        #print nEmployee
                        
            #print dT
            vtLog.vtLngCurCls(vtLog.DEBUG,'finished',self)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def __procPrjTimerReadBack__old(self,r,docRef,docRefHum,docRefGlbs,
                    srvXml,lAdd):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        if r is None:
            return None
        t=None
        try:
            idHum=r.employee_id
            nodeHum=docRefHum.getNodeByIdNum(idHum)
            oReg=docRef.GetReg('travel')
            oHum=docRefHum.GetRegByNode(nodeHum)
            sTag=oHum.GetName(nodeHum)
            oLogin=vtSecXmlLogin(usr=sTag,usrId=idHum)
            docRef.SetLogin(oLogin)
            nodeTmp=docRef.getBaseNode()
            n=None
            try:
                docRefHum.SetNetMaster(docRef)
                n=oReg.Create(nodeTmp,self.__setTrvData__,
                                r,docRef,sTag,idHum,docRefGlbs)
                idPar=docRef.getKeyNum(nodeTmp)
                idAdd=docRef.getKeyNum(n)
                lAdd.append(idAdd)
            except:
                vtLog.vtLngTB(self.__class__.__name__)
            if n is not None:
                try:
                    oAttr=docRef.GetReg('travelAttr')
                    nAttr=oAttr.Create(n,self.__setTrvAttrData__,
                                    r,docRef,sTag,idHum,docRefGlbs)
                    #idPar=docRef.getKeyNum(nodeTmp)
                    idAddAttr=docRef.getKeyNum(nAttr)
                    lAdd.append(idAddAttr)
                except:
                    vtLog.vtLngTB(self.__class__.__name__)
                
            docRefHum.SetNetMaster(None)
            idPrjTimer=self.doc.getKeyNum(n)
            t=(['vMES=%d'%idPrjTimer],
                    ['id=%s'%r.id])
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
        return t
    def __preProc__(self,dataInst,srvXml,docRef,d):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        try:
            if srvXml is not None:
                srvXml.acquire()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        self.StoreLastId(d)
        docRef.SetNetDocs(d)
        vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
    def __postProc__(self,dataInst,srvXml,docRef,lAdd,lEdit):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        try:
            if len(lAdd):
                docRef.ClearCache()
            for idAdd in lAdd:
                try:
                    n=docRef.getNodeByIdNum(idAdd)
                    docRef.calcFingerPrint(n)
                    nPar=docRef.getParent(n)
                    idPar=docRef.getKeyNum(nPar)
                    val=docRef.GetNodeXmlContent(n,
                                    bFull=False,bIncludeNodes=False)
                    iRet=srvXml.AddNode(docRef.GetApplAlias(),
                                    None,idPar,idAdd,val)
                except:
                    iRet=-1
                    vtLog.vtLngTB(self.__class__.__name__)
            for idEdit in lEdit:
                try:
                    n=docRef.getNodeByIdNum(idEdit)
                    docRef.calcFingerPrint(n)
                    iRet=srvXml.Update(docRef.GetApplAlias(),n)
                except:
                    iRet=-1
                    vtLog.vtLngTB(self.__class__.__name__)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            dataInst.Commit()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            if srvXml is not None:
                srvXml.release()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        vtLog.vtLngCurCls(vtLog.INFO,'done'%(),self)
    def SynchWithDB(self,node,dataInst,lang='en'):
        srvXml=None
        docRef=None
        lAdd=[]
        lEdit=[]
        d={}
        dT={}
        zDt=vtDateTime()
        zDt.Now()
        zStartEnd=vtDateTimeStartEnd()
        try:
            srvXml=self.GetRefDataInst(node)
            docRef=self.GetRefDoc(node)
            if docRef is None:
                vtLog.vtLngCurCls(vtLog.ERROR,'alias reference doc not found',self)
            nodeBase=docRef.getBaseNode()
            
            c=docRef.getChildByLst(docRef.getRoot(),['config','vHum','host'])
            alias=docRef.getNodeText(c,'alias')
            docRefHum=self.GetSubRefDoc(node,'vHum:%s'%alias)
            c=docRef.getChildByLst(docRef.getRoot(),['config','vGlobals','host'])
            alias=docRef.getNodeText(c,'alias')
            docRefGlbs=self.GetSubRefDoc(node,'vGlobals:%s'%alias)
            c=docRef.getChildByLst(docRef.getRoot(),['config','vPrj','host'])
            alias=docRef.getNodeText(c,'alias')
            docRefPrj=self.GetSubRefDoc(node,'vPrj:%s'%alias)
            c=docRef.getChildByLst(docRef.getRoot(),['config','vTask','host'])
            alias=docRef.getNodeText(c,'alias')
            docRefTask=self.GetSubRefDoc(node,'vTask:%s'%alias)
            d={ 'vHum':     {'doc':docRefHum,   'bNet':'False'},
                'vGlobals': {'doc':docRefGlbs,  'bNet':'False'},
                'vPrj':     {'doc':docRefPrj,   'bNet':'False'},
                'vTask':    {'doc':docRefTask,  'bNet':'False'},
                }
            
            #print d
            #oPrj=docRef.GetReg('prjtasks')
            #if docRef.hasNodeInfos2Get(self.__class__.__name__)==False:
            #    docRef.setNodeInfos2Get(['|id','|fid','name'],
            #            name=self.__class__.__name__)
            dataInst.Do(self.__preProc__,dataInst,srvXml,docRef,d)
            dataInst.SelectUpdate('timer_time',
                    u'select * from timer_time where locked="0"',
                    self.__procPrjTimerReadBack__,docRef,docRefHum,docRefGlbs,
                    srvXml,zDt,zStartEnd,dT,lAdd)
            dataInst.Do(self.__procPrjTimerCreateNodesReadBack__,
                    dataInst,srvXml,docRef,docRefHum,docRefGlbs,docRefPrj,docRefTask,
                    dT,d,zDt,lAdd,lEdit)
                #dataInst.Do(self.__procPrjTimerYearReadBack__,
            #docRef.procChildsKeysRec(10,nodeBase,self.__procPrj__,
            #            oPrj,docRef,dataInst,zStart,zEnd,lang=lang)
            #dataInst.Commit()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            dataInst.Do(self.__postProc__,dataInst,srvXml,docRef,lAdd,lEdit)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    # ---------------------------------------------------------
    # inheritance
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02bIDAT8\x8d\x8d\x93\xcfKTQ\x14\xc7?o\xeeL\xf9\xaby\xfa\x9c\x1ff\x88\
\x8a\x15\x1aD\xad\xa4\x16\xfe\x07\x11\x08\xe2\xa0\x16D \xb4j#\x8aE\xb4\xee\
\x0fHp\x91\x1a\xb6\n\x8c\x16\xb9-\xa1\xc0\xc0ED#\x13\x8e\x98\xda$\xea8\x8e3\
\xf3\x1c\xcd\xf7\xee{\xb7\x85\xccX:Bwu\xee\xe1\xde\xc39\xdf\xf3\xf9j\x9aGp\
\xfc\\}\xd6\xa5N$\x81\xe8\xf0\x94v<\xe7\xf9\xdf\x87\xa5r\x00h\x1e\x81\xe6\
\x11l\xe76\x95\xb4,eK\xa9\x94rU~\xff\xb7z3\xf5V%S)\xa5\x94RRJ%-K9\x96\xa5\
\x1e\x8e\xddS\x85\x7f\xc5\x0e\x8c]\x17a\xee\xe2\x15\x02\xd7U,\xc4bd3\xdb\xac\
\xc6\x17\xc9\xe5L\x84\x10\x88\x83\x03<;\x19\xaeWw\x14\x1b\xf0\x16\x82=\\\xf6\
\xcc,\xca\x91X\xb6\xc3\xca\xe2wjt?\x1bk\xab\x84\xea\xeb\x11\xc2\x83e\x9a\xc8\
\xfc>\xae\xe3\x1eiP\x10l\x87JL\xca\x91\xb6M\xd9\x19\x1f\x9d\x91^6\xd3\x19nuE\
\xa8\xab\xabc\xfa\xdd4\xcb\xeb\x1b\x88\x80\xc1\xd9\x8a\xb2\xa2\xd8\xdeB\xa0W\
\xea\x94\x07t\x84\xef\xb0\xb2\x94\x92lj\x8b\xd9\xd9\xcf\xc4\x17\x16\x98|5\
\xc9\xa5\x96\x8b\x0c\x0e\r1\xf7~\xae\xb8\xa9\xa3\x11\xf6\x93\xacodq\x1c\x87\
\x1a]g-\x9d\xc6?6\xc6\xc8\xa7\x8f\x04Z\xdb\xe8\x8d\xf4\x90\xcafx\xf4\xf4\t-\
\rM'5p\\\x03\xc3\x08R\x1b\x00\xc7q\t\x84C<\xbfy\x83+\xadm\xf4\xf5\xdd\xa1\
\xca0X\xfe\xfa\x85\x8e\xf6vF'^@\xf0\x18\x07\xc2caYYv\xd2il\xdbf~~\x9epc\x13\
\xdd\xdd\x11\x9a\x9a\x9b\xf8\x19\x8b\xe1\x8bFi\xae\x0f\xd3s\xf7\xfe\xbf E\
\x87\xa74\xd7\xb5\xc8\xe7w\xc9\x99&\xd2\x96\xcc\xcc|\xa0\xa5\xb1\x19C\xf7\
\xe3\x00U\xf3\xdf\xa8hh\xe4\xdc\xe5V**\xab\x8a`y\x8b\x84i~BA\x1d\x7f\xb5\x86\
e[,\xc5\x97\t\x9f\x0f\x11O$\xb8\xa6Ws\xe1v'\x9a\xcf\xc7\xd2\xd2\n{\xb9l\x91\
\xce\xe2\x08\xe1\xa0\x07\xbf~X\xcb'|\x0c\x0c\x0c\xb0\xb0\x18'c\xe6I$\xb7\xd8\
\xaf\xf4\xb3\x94\xc9 \xa5\xcb\xaf\xc4\xcaI\x11\x1f\xbf\x1e\xc4\xb0C(\xa5\xf0\
\n/\xb9\\\x96\xb5\xcdu&FG\xe8\xef\x7f\x80Y\xbbCj+\xc9\xea\xea\x0f^\x8e\x8f\
\x1fY\xa1\x94\x1b\x0bl\xfc}?\xcdL%\xddX\xca\xce\xa7Y\xfc\x0f*\xdc\x1e\x01?d\
\xcap\x00\x00\x00\x00IEND\xaeB`\x82" 
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeXmlDjangoSrvAliasRefPrjTimerPanel
        else:
            return None
