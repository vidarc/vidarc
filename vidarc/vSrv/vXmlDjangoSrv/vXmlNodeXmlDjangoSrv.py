#----------------------------------------------------------------------------
# Name:         vXmlNodeXmlDjangoSrv.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060708
# CVS-ID:       $Id: vXmlNodeXmlDjangoSrv.py,v 1.4 2008/02/04 16:35:08 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeTag import vtXmlNodeTag
from vidarc.vSrv.net.vsNetXmlNodeXmlSrv import vsNetXmlNodeXmlSrv
try:
    if vcCust.is2Import(__name__):
        from vidarc.vSrv.vXmlDjangoSrv.vXmlNodeXmlDjangoSrvPanel import vXmlNodeXmlDjangoSrvPanel
        #from vidarc.vSrv.net.vsNetXmlNodeSrvEditDialog import vsNetXmlNodeSrvEditDialog
        #from vidarc.vSrv.net.vsNetXmlNodeSrvAddDialog import vsNetXmlNodeSrvAddDialog
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeXmlDjangoSrv(vtXmlNodeTag):
    def __init__(self,tagName='vXmlDjangoSrv'):
        global _
        _=vtLgBase.assignPluginLang('vXmlDjangoSrv')
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'XML server django link')
    # ---------------------------------------------------------
    # specific
    def GetUpdateTime(self,node):
        return self.Get(node,'updateTm')
    def GetUpdateTimeFloat(self,node):
        s=self.GetUpdateTime(node)
        fUpd=-1.0
        if len(s)>0:
            if s=='5m':
                fUpd=5.0*60.0
            elif s=='15m':
                fUpd=15.0*60.0
            elif s=='30m':
                fUpd=30.0*60.0
            elif s=='1h':
                fUpd=60.0*60.0
            elif s=='6h':
                fUpd=6.0*60.0*60.0
            elif s=='12h':
                fUpd=12.0*60.0*60.0
        return fUpd
    def GetPortData(self,node):
        return self.GetVal(node,'dsn',str,'django')
    def GetPortService(self,node):
        return self.GetVal(node,'portService',int,50009)
    def GetServicePasswd(self,node):
        return self.Get(node,'servicePasswd')
    def SetPortService(self,node,val):
        return self.SetVal(node,'portService',val)
    def SetUpdateTime(self,node,val):
        self.Set(node,'updateTm',val)
    
    def GetServerCls(self):
        from vidarc.tool.net.vtNetSrv import vtNetSrv
        return vtNetSrv
    def GetDataSrvCls(self):
        from vidarc.tool.db.vtDbSrv import vtDbSrv
        return vtDbSrv
        from vidarc.tool.db.vtDbSrv import vtDbNetSrv
        return vtDbNetSrv
    def GetDataCls(self):
        return None
        from vidarc.tool.db.vtDbSrv import vtDbSrv
        return vtDbSrv
    def GetServiceSrvCls(self):
        from vidarc.tool.net.vtNetSrv import vtSrvSock
        return vtSrvSock
    def GetServiceCls(self):
        from vidarc.tool.net.vtNetSrv import vtServiceSock
        return vtServiceSock
    # ---------------------------------------------------------
    # fake normal server node
    def __procElem__(self,node,dataInst,lang=None):
        try:
            o=self.doc.GetRegByNode(node)
            if hasattr(o,'IsLinked2DB'):
                if o.IsLinked2DB():
                    o.SynchWithDB(node,dataInst,lang=lang)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return 0
    def Start(self,node,dataInst):
        #dataInst.acquire()
        try:
            if dataInst.IsEmpty()==False:
                vtLog.vtLngCurCls(vtLog.CRITICAL,'server data-instance is still busy'%(),self)
            dataInst.Do(
                        self.doc.procChildsKeysRec,10,node,self.__procElem__,dataInst,
                        lang='de')
            #vtLog.CallStack('')
            fUpd=self.GetUpdateTimeFloat(node)
            #print fUpd
            if fUpd>0:
                dataInst.SetCyclic(fUpd,
                        self._doSynch,self.doc.getKeyNum(node),dataInst)
            pass
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        #dataInst.release()
    def Stop(self,node,dataInst):
        pass
    def Setup(self,node,dataInst):
        try:
            #self.doc.procChildsKeysRec(10,node,self.__procElem__,dataInst,
            #        lang='de')
            pass
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def _doSynch(self,id,dataInst):
        try:
            vtLog.CallStack('')
            print id
            if self.doc is None:
                vtLog.vtLngCurCls(vtLog.CRITICAL,'doc is None'%(),self)
                return
            node=self.doc.getNodeByIdNum(id)
            if node is None:
                vtLog.vtLngCurCls(vtLog.CRITICAL,'id:%08d node is None'%(id),self)
                return
            self.Synch(node,dataInst)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def Synch(self,node,dataInst):
        #dataInst.acquire()
        try:
            if dataInst.IsEmpty()==False:
                vtLog.vtLngCurCls(vtLog.CRITICAL,'server data-instance is still busy'%(),self)
            dataInst.Do(
                    self.doc.procChildsKeysRec,10,node,self.__procElem__,dataInst,
                    lang='de')
            pass
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        #dataInst.release()
    def BuildLoginPossible(self,node,dataInst,docLogin=None,nodeLogin=None):
        pass
    def StartServer(self,node):
        try:
            sTag=self.doc.getTagName(node)
            if sTag!=self.GetTagName():
                vtLog.vtLngCurCls(vtLog.WARN,'tag:%s is not reg tag:%s'%(sTag,self.GetTagName()),self)
                return None
            vtLog.vtLngCurCls(vtLog.INFO,'tag:%s start server'%(sTag),self)
            if self.doc.HasServerInstanceByNode(node):
                netSrv=self.doc.GetServerInstanceByNode(node)
            else:
                iPortData=self.GetPortData(node)
                iPortService=self.GetPortService(node)
                sServicePasswd=self.GetServicePasswd(node)
                netSrv=self.GetServerCls()(self.GetDataSrvCls(),iPortData,self.GetDataCls(),
                                    self.GetServiceSrvCls(),iPortService,self.GetServiceCls(),2,1)
                netSrv.SetServicePasswd(sServicePasswd)
                netSrv.Serve()
                self.doc.AddServerInstanceByNode(node,netSrv)
                dataInst=netSrv.GetDataInstance()
                self.Start(node,dataInst)
            return netSrv
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def SetupServer(self,node):
            sTag=self.doc.getTagName(node)
            if sTag!=self.GetTagName():
                vtLog.vtLngCurCls(vtLog.WARN,'tag:%s is not reg tag:%s'%(sTag,self.GetTagName()),self)
                return
            vtLog.vtLngCurCls(vtLog.INFO,'tag:%s setup server'%(sTag),self)
            if self.doc.HasServerInstanceByNode(node):
                netSrv=self.doc.GetServerInstanceByNode(node)
                dataInst=netSrv.GetDataInstance()
                self.Setup(node,dataInst)
    def StopServer(self,node):
        try:
            sTag=self.doc.getTagName(node)
            if sTag!=self.GetTagName():
                vtLog.vtLngCurCls(vtLog.WARN,'tag:%s is not reg tag:%s'%(sTag,self.GetTagName()),self)
                return None
            vtLog.vtLngCurCls(vtLog.INFO,'tag:%s stop server'%(sTag),self)
            if self.doc.HasServerInstanceByNode(node):
                netSrv=self.doc.GetServerInstanceByNode(node)
                netSrv.Stop()
                dataInst=netSrv.GetDataInstance()
                self.Stop(node,dataInst)
                self.doc.DelServerInstanceByNode(node)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def BuildLoginPossibleServer(self,node):
        pass
    # ---------------------------------------------------------
    # inheritance
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xc5IDAT8\x8d\xa5\x93Mo\x12Q\x14\x86\x9f\x99;2\x0c_\xc5\x02b\xaa\x19\
b\x9b\x94\x90hL\x1b\x83\x89&\xee\xba\xf7\xa7vg\xa2\x1b7.d\xe1G\xd2\xa4\x1d\
\xd3\xc4\x80PA\x8aba\x18\xe6\xe3\xdeq\x81\x8e"\xc4\xc4p\x96\xe7\xbe\xf7\xb9\
\xf7}O\x8e\xa6\xe9\x82MJ\xdf\xe86`\xack:\x9d\xcb\xf8\xb5\xd3\xe7b\xe4\x02\
\xb0S\xca\xf2\xb0~\x93\xba]\xd6\xfe\xd6j\x7fZpg\xf3\xf8\xc5\xdb\x0e\'\x9d\
\xefl\x17\x0bX\xa6\t\x80\xe7\xfb|\x1d_q\xd7\xde\xe2\xe8\xc0&\x9bIkk\x01\xc7\
\xaf>\xc4\x17\xdf\xe6\xd4\xaaE\x8a\x19\x83\xc9\\"U\x9c\x9c\xb7\x07cv\xae\xa7\
y\xfax?\x01$\x19\x9c\xb6\x87\xf1YwL\xa5\x98e\xe6\x87\x14,\xc1\x83\xdd\x02)\
\x013?d\xe6\x87T\x8aY\xce\xbacN\xdb\xc3x\x05\xd0r\x06\xe4-\x13\xd7\x0bp\xbd\
\x800\x92dR\x82G\xfb\xdb\xd8\xa5t\xd2\xcf[&-g\xc0\n\xa0{9E)p\xbd\x10\xd7\x0b\
\tB\xf5\xd3#\xdc\xb3\xb7x\xd2\xa8\x10\xab\x18\xa5\x16\xda\xb5S\xf0\x82\x10M[\
\xd8\x8b\xa4ZJ;o\x19\xa4\x0c\r/\x08\x97\xfa\t\xe0v9G\xfb\xcb\x14!\x16\x9f\
\x92\xea7\xe0\xfc\xf3\x84\xe7\xefz\xf8\xa1BJE\xedFn\xd5B\xb3^%R\nC\xe8\x18BG\
\xd75"\x19\xf3\xf2\xa4\xcf\xb37=\xa4\x02C\xe8DJ\xd1\xacWW\x01\x8dZE;\xdc+#\
\xa5\xc42\r&\xf3\x88\xe3\xd6\'\xce\xfbS,\xd3\xc02\r\xa4\x94\x1c\xee\x95i\xd4\
*\xc9\x18\x9728:\xb0\x01x\xffq\x84\xa3\x14\xa9k\x02!4\x82Pr\xe5\xfa\xdc\xbfS\
J4\xbfJ[\xb7L\xa7\xeda\xdcr\x06\xf4F\x8b\xb4o\x95r4\xeb\xd5\xa5\x97\xff\t\
\xf8\x9f\xdax\x1b\x7f\x00\xf9\xfb\xbc\xe0\xc73\xe1\xf5\x00\x00\x00\x00IEND\
\xaeB`\x82' 
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeXmlDjangoSrvPanel
        else:
            return None
