#----------------------------------------------------------------------------
# Name:         vXmlNodeXmlDjangoSrvAliasRefPrj.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20070920
# CVS-ID:       $Id: vXmlNodeXmlDjangoSrvAliasRefPrj.py,v 1.3 2007/10/02 03:37:02 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.time.vtTime import vtDateTime

#from vidarc.vSrv.net.vsNetXmlNodeAliasRef import vsNetXmlNodeAliasRef
from vidarc.vSrv.vXmlDjangoSrv.vXmlNodeXmlDjangoSrvAliasRef import vXmlNodeXmlDjangoSrvAliasRef
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeXmlDjangoSrvAliasRefPrj(vXmlNodeXmlDjangoSrvAliasRef):
    def __init__(self,tagName='AliasRef_vPrj'):
        global _
        _=vtLgBase.assignPluginLang('vXmlDjangoSrv')
        vXmlNodeXmlDjangoSrvAliasRef.__init__(self,tagName)
        self.zStart=vtDateTime(bLocal=True)
        self.zEnd=vtDateTime(bLocal=True)
    def GetDescription(self):
        return _(u'project alias reference XML server')
    # ---------------------------------------------------------
    # specific
    def __procPrj__(self,node,oPrj,docRef,dataInst,zStart,zEnd,lang=None):
        try:
            sTag,infos=docRef.getNodeInfos(node,lang=lang,name=self.__class__.__name__)
            if sTag=='project':
                d={
                    'id':str(long(infos['|id'])),
                    'name':infos['name'][:],
                    'description':infos['description'][:],
                    'closed':infos['closed'][:],
                    }
                try:
                    sStart=infos['startdatetime'][:10]
                    zStart.SetDateStr(sStart)
                    #d['datestart']=sStart
                except:
                    sStart=''
                try:
                    sEnd=infos['enddatetime'][:10]
                    zEnd.SetDateStr(sStart)
                    #d['dateend']=sEnd
                except:
                    sEnd=''
                #d['datestart']='2007-09-24'#sStart
                #d['dateend']='2007-09-24'#sEnd
                #d['datestart']=sStart
                #d['dateend']=sEnd
                dataInst.UpdateInsert('prj_prj',d,['id'],
                        keys4upd=['name','description','closed',
                                #'datestart','dateend'
                                ],
                        keys4ins=['id','name','description',
                                'closed',
                                #'datestart','dateend'
                                ])
                
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return 0
    def __preProc__(self,dataInst,srvXml,docRef):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        try:
            if srvXml is not None:
                srvXml.acquire()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
    def __postProc__(self,dataInst,srvXml,docRef):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        try:
            dataInst.Commit()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            if srvXml is not None:
                srvXml.release()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        vtLog.vtLngCurCls(vtLog.INFO,'done'%(),self)
    def __procPrj__(self,nodeBase,oPrj,docRef,dataInst,zStart,zEnd,lang):
        docRef.procChildsKeysRec(10,nodeBase,self.__procPrj__,
                        oPrj,docRef,dataInst,zStart,zEnd,lang=lang)
    def SynchWithDB(self,node,dataInst,lang='en'):
        srvXml=None
        try:
            srvXml=self.GetRefDataInst(node)
            docRef=self.GetRefDoc(node)
            if docRef is None:
                vtLog.vtLngCurCls(vtLog.ERROR,'alias reference doc not found',self)
            nodeBase=docRef.getBaseNode()
            oPrj=docRef.GetReg('project')
            zStart=vtDateTime(bLocal=True)
            zEnd=vtDateTime(bLocal=True)
            if docRef.hasNodeInfos2Get(self.__class__.__name__)==False:
                docRef.setNodeInfos2Get(['|id','name','description',
                        #'startdatetime','enddatetime',
                        'closed'],
                        name=self.__class__.__name__)
            dataInst.Do(self.__preProc__,dataInst,srvXml,docRef)
            #dataInst.Do(docRef.__procPrj__,nodeBase,oPrj,docRef,dataInst,
            #            zStart,zEnd,lang)
            dataInst.Do(docRef.procChildsKeysRec,10,nodeBase,self.__procPrj__,
                        oPrj,docRef,dataInst,zStart,zEnd,lang=lang)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            dataInst.Do(self.__postProc__,dataInst,srvXml,docRef)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    # ---------------------------------------------------------
    # inheritance
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00nIDAT8\x8d\xcd\x93\xc1\n\x80 \x10D\xdf\xd6\x07\x1bt\x97\xe8.\xd4\x17\
\xdb\xc9\x10\xdd\x8d\xb4\xa0\xdeq\xd0q\\fE\x86\x91\x12\xe7C\xacD`_g)\xb5\xe1\
\xeeAM\x03\x90\x94\xc0z\xd5"\x19\xaa\tZxn\xd0\x1a=\xc7\xf9\x10eZ\xb6n\x03\
\xf8\xc5\x0c^1\xb0Jr\xc5\xd9\x83\x9e\xcb\xb9\xc9\xf73\x10m\x1b\xa1\xde\r\xeb\
\xabj\x02\xad\x9dVc\x0f\xa0T!\xbct\xaaL\xcd\x00\x00\x00\x00IEND\xaeB`\x82' 
