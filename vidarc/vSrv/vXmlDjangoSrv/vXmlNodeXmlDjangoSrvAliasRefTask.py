#----------------------------------------------------------------------------
# Name:         vXmlNodeXmlDjangoSrvAliasRefTask.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20070920
# CVS-ID:       $Id: vXmlNodeXmlDjangoSrvAliasRefTask.py,v 1.5 2008/02/04 16:35:08 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vSrv.vXmlDjangoSrv.vXmlNodeXmlDjangoSrvAliasRef import vXmlNodeXmlDjangoSrvAliasRef
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeXmlDjangoSrvAliasRefTask(vXmlNodeXmlDjangoSrvAliasRef):
    def __init__(self,tagName='AliasRef_vTask'):
        global _
        _=vtLgBase.assignPluginLang('vXmlDjangoSrv')
        vXmlNodeXmlDjangoSrvAliasRef.__init__(self,tagName)
    def GetDescription(self):
        return _(u'task alias reference XML server')
    # ---------------------------------------------------------
    # specific
    def __procTask__(self,node,oPrjTask,docRef,dataInst,lang=None):
        try:
            sTag,infos=docRef.getNodeInfos(node,lang=lang,name=self.__class__.__name__)
            if sTag=='task':
                    d={
                        'id':str(long(infos['|id'])),
                        'name':infos['name'][:],
                        }
                    dataInst.UpdateInsert('timer_task',d,['id'],
                            keys4upd=['name'],
                            keys4ins=['id','name'])
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return 0
    def __procPrjTasks__(self,node,oPrjTask,docRef,dataInst,lang=None):
        try:
            sTag,infos=docRef.getNodeInfos(node,lang=lang,name=self.__class__.__name__)
            if sTag=='prjtasks':
                if len(infos['|fid'])==0:
                    docRef.procChildsKeysRec(0,node,self.__procTask__,
                                oPrjTask,docRef,dataInst,lang=lang)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return 0
    def __preProc__(self,dataInst,srvXml,docRef):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        try:
            if srvXml is not None:
                srvXml.acquire()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
    def __postProc__(self,dataInst,srvXml,docRef):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        try:
            dataInst.Commit()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            if srvXml is not None:
                srvXml.release()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        vtLog.vtLngCurCls(vtLog.INFO,'done'%(),self)
    def SynchWithDB(self,node,dataInst,lang='en'):
        srvXml=None
        docRef=None
        try:
            #vtLog.CallStack('')
            srvXml=self.GetRefDataInst(node)
            docRef=self.GetRefDoc(node)
            if docRef is None:
                vtLog.vtLngCurCls(vtLog.ERROR,'alias reference doc not found',self)
            nodeBase=docRef.getBaseNode()
            oPrjTask=docRef.GetReg('prjtasks')
            if docRef.hasNodeInfos2Get(self.__class__.__name__)==False:
                docRef.setNodeInfos2Get(['|id','|fid','name'],
                        name=self.__class__.__name__)
            dataInst.Do(self.__preProc__,dataInst,srvXml,docRef)
            #dataInst.Do(self.__procTask__,nodeBase,oPrjTask,docRef,
            #                dataInst,lang)
            dataInst.Do(docRef.procChildsKeysRec,0,nodeBase,self.__procPrjTasks__,
                        oPrjTask,docRef,dataInst,lang=lang)
            #dataInst.Commit()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            dataInst.Do(self.__postProc__,dataInst,srvXml,docRef)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    # ---------------------------------------------------------
    # inheritance
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x97IDAT8\x8d\xadS\xb1\r\x800\x0cs\n\xef\xc0\xc8%H\xdc\xc0\xc2\xcc\t\
\x15#\x0bO\xf4\x12V\x0e\x82\tT5qU\x04\x19\x9d\xd8u#G\xc4UH\xab\xf1\xfd\xa9@\
\x00\xc7\x1c$\xc5\\\xe9\xa0\x85\x01\x80\xdc\x0e\xd8\xab\xacnA\xd3\xc1\x9b\
\xca\n\xac\xdd\x88\xb5\x1b\xf3\x02\xcczLd"\x8d\xefOi\x97A\t0\xc2\xb4o\xdaA)\
\x99\xf5\xfe_\xa2e3\xd7s\x80\x0e\x895\x98bO\x0eX\xc2b\x02su\xccAj\xea7C\x8c\
\xeb\xf3\x12\xc5\xbaF@\xdf\x06\xfb\xaa\xe9\xc0J\'K\xec\x05\xdb\xfa8\xfe|\x9b\
\r9\x00\x00\x00\x00IEND\xaeB`\x82' 
