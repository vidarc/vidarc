#Boa:FramePanel:vXmlNodeXmlDjangoSrvAliasRefTravelPanel
#----------------------------------------------------------------------------
# Name:         vsNetXmlNodeAliasRefPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20080103
# CVS-ID:       $Id: vXmlNodeXmlDjangoSrvAliasRefTravelPanel.py,v 1.2 2010/03/29 08:55:34 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputTree
import vidarc.tool.input.vtInputTreeInternal

import sys

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VXMLNODEXMLDJANGOSRVALIASREFTRAVELPANEL, 
 wxID_VXMLNODEXMLDJANGOSRVALIASREFTRAVELPANELLBLREF, 
 wxID_VXMLNODEXMLDJANGOSRVALIASREFTRAVELPANELLBLREFMSG, 
 wxID_VXMLNODEXMLDJANGOSRVALIASREFTRAVELPANELVIMSG, 
 wxID_VXMLNODEXMLDJANGOSRVALIASREFTRAVELPANELVIREF, 
] = [wx.NewId() for _init_ctrls in range(5)]

class vXmlNodeXmlDjangoSrvAliasRefTravelPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(3)
        parent.AddGrowableCol(0)
        parent.AddGrowableCol(1)

    def _init_coll_bxsRef_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblRef, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viRef, 2, border=4, flag=wx.EXPAND)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsRef, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsRefMsg, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)

    def _init_coll_bxsRefMsg_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblRefMsg, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viMsg, 2, border=0, flag=wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=4, vgap=0)

        self.bxsRef = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsRefMsg = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_bxsRef_Items(self.bxsRef)
        self._init_coll_bxsRefMsg_Items(self.bxsRefMsg)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self,
              id=wxID_VXMLNODEXMLDJANGOSRVALIASREFTRAVELPANEL,
              name=u'vXmlNodeXmlDjangoSrvAliasRefTravelPanel', parent=prnt,
              pos=wx.Point(0, 0), size=wx.Size(312, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 180))
        self.SetAutoLayout(True)

        self.lblRef = wx.StaticText(id=wxID_VXMLNODEXMLDJANGOSRVALIASREFTRAVELPANELLBLREF,
              label=u'alias reference', name=u'lblRef', parent=self,
              pos=wx.Point(4, 4), size=wx.Size(94, 24), style=wx.ALIGN_RIGHT)
        self.lblRef.SetMinSize((-1,-1))

        self.viRef = vidarc.tool.input.vtInputTreeInternal.vtInputTreeInternal(id=wxID_VXMLNODEXMLDJANGOSRVALIASREFTRAVELPANELVIREF,
              name=u'viRef', parent=self, pos=wx.Point(102, 4),
              size=wx.Size(197, 24), style=0)

        self.lblRefMsg = wx.StaticText(id=wxID_VXMLNODEXMLDJANGOSRVALIASREFTRAVELPANELLBLREFMSG,
              label=u'alias ref vMsg', name=u'lblRefMsg', parent=self,
              pos=wx.Point(4, 32), size=wx.Size(94, 26), style=wx.ALIGN_RIGHT)

        self.viMsg = vidarc.tool.input.vtInputTreeInternal.vtInputTreeInternal(id=wxID_VXMLNODEXMLDJANGOSRVALIASREFTRAVELPANELVIMSG,
              name=u'viMsg', parent=self, pos=wx.Point(102, 32),
              size=wx.Size(197, 26), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vSrvNet')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.viRef.AddTreeCall('init','SetNodeInfos',['tag','name'])
        self.viRef.AddTreeCall('init','SetGrouping',[],[('tag',''),('name','')])
        self.viRef.SetTagNames2Base([])
        self.viRef.SetTagNames('aliasRef','tag')
        self.viRef.SetShowFullName(True)
        
        self.viMsg.AddTreeCall('init','SetNodeInfos',['tag','name'])
        self.viMsg.AddTreeCall('init','SetGrouping',[],[('tag',''),('name','')])
        self.viMsg.SetTagNames2Base([])
        self.viMsg.SetTagNames('aliasRefMsg','tag')
        self.viMsg.SetShowFullName(True)
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def __Clear__(self):
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
        # add code here
        self.viRef.Clear()
    def SetNetDocs(self,d):
        self.__logCritical__('FIXME'%())
        return
        if d.has_key('vHum'):
            dd=d['vHum']
            self.docHum=dd['doc']
        # add code here
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viRef.SetDoc(doc,bNet)
        self.viMsg.SetDoc(doc,bNet)
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viRef.SetNode(node)
            self.viMsg.SetNode(node)
            
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viRef.GetNode(node)
            self.viMsg.GetNode(node)
            
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viRef.Close()
        self.viMsg.Close()
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viRef.Cancel()
        self.viMsg.Cancel()
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            # add code here
            self.viRef.Enable(False)
            self.viMsg.Enable(False)
        else:
            # add code here
            self.viRef.Enable(True)
            self.viMsg.Enable(True)
