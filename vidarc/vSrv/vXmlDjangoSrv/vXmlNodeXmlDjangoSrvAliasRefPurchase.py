#----------------------------------------------------------------------------
# Name:         vXmlNodeXmlDjangoSrvAliasRefPurchase.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20070924
# CVS-ID:       $Id: vXmlNodeXmlDjangoSrvAliasRefPurchase.py,v 1.2 2007/10/02 03:37:02 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.sec.vtSecXmlLogin import vtSecXmlLogin
from vidarc.tool.time.vtTime import vtDateTime

from vidarc.vSrv.vXmlDjangoSrv.vXmlNodeXmlDjangoSrvAliasRef import vXmlNodeXmlDjangoSrvAliasRef
try:
    if vcCust.is2Import(__name__):
        from vidarc.vSrv.vXmlDjangoSrv.vXmlNodeXmlDjangoSrvAliasRefPurchasePanel import vXmlNodeXmlDjangoSrvAliasRefPurchasePanel
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeXmlDjangoSrvAliasRefPurchase(vXmlNodeXmlDjangoSrvAliasRef):
    def __init__(self,tagName='AliasRef_vPurchase'):
        global _
        _=vtLgBase.assignPluginLang('vXmlDjangoSrv')
        vXmlNodeXmlDjangoSrvAliasRef.__init__(self,tagName)
        self.zDt=vtDateTime(True)
    def GetDescription(self):
        return _(u'purchase alias reference XML server')
    # ---------------------------------------------------------
    # specific
    def __addPurPartData__(self,oParts,node,r,docRef):
        #print node
        nD=docRef.getChildForced(node,'data')
        nRow=docRef.createSubNode(nD,'row')
        docRef.setNodeText(nRow,'count','%d'%r.countParts)
        docRef.setNodeText(nRow,'part',r.name)
        docRef.setNodeText(nRow,'estimated','True')
        docRef.setNodeText(nRow,'price','%6.2f'%r.price)
        docRef.setNodeText(nRow,'pricesum','%6.2f'%(r.price*r.countParts))
    def __procPurPartsReadBack__(self,r,docRef,docRefHum,docRefGlbs,
                    srvXml,srvMsg,lAdd):
        if r is None:
            return None
        nodePur=docRef.getNodeByIdNum(r.vMES)
        if nodePur is None:
            vtLog.vtLngCurCls(vtLog.CRITICAL,'id:%08d;node not found'%(r.vMES),self)
        oReg=docRef.GetReg('purchaseParts')
        nodePart=docRef.getChild(nodePur,'purchaseParts')
        if nodePart is None:
            try:
                nodePart=oReg.Create(nodePur,self.__addPurPartData__,r,docRef)
                idPar=docRef.getKeyNum(nodePur)
                idAdd=docRef.getKeyNum(nodePart)
                lAdd.append(idAdd)
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        else:
            self.__addPurPartData__(oReg,nodePart,r,docRef)
            docRef.AlignNode(nodePart,iRec=3)
    def __setPurData__(self,oPur,node,r,docRef,sTag,idHum,docRefGlbs):
        self.zDt.Now()
        sDt='now'
        sDtTm=self.zDt.GetDateTimeStr(' ')
        sDt=sDtTm[:10]
        oPur.SetTag(node,sTag+' '+sDt)
        oPur.SetName(node,r.description,lang='en')
        oPur.SetName(node,r.description,lang='de')
        sOrderType='Bestellart: ???'
        if r.orderType=='me':
            sOrderType='Bestellart: Anforderer bestellt selbst'
        elif r.orderType=='me':
            sOrderType='Bestellart: Sektritariat moege bestellen'
        sDeliverer='Lieferant: %s'%r.deliveredBy
        sDelivererContact='Kontakt: %s'%r.deliverContact
        oPur.SetDesc(node,u'\n'.join([sOrderType,sDeliverer,sDelivererContact]),lang='en')
        oPur.SetDesc(node,u'\n'.join([sOrderType,sDeliverer,sDelivererContact]),lang='de')
        oPur.SetPrjId(node,r.prj_id)
        oPur.SetOrderById(node,idHum)
        oPur.SetCurrency(node,u'EUR')
        #oPur.SetSupplierStr(node,r.deliveredBy)
        #oPur.SetSupplierContactStr(node,r.deliverContact)
        oPur.SetOrderDt(node,u''.join(r.date_order.isoformat().split(u'-')))
        oPur.SetDeliveryDt(node,u''.join(r.date_delivery.isoformat().split(u'-')))
        oPur.SetOwner(node,idHum)
        print node
    def __procPurReadBack__(self,r,docRef,docRefHum,docRefGlbs,
                    srvXml,srvMsg,lAdd):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        if r is None:
            return None
        t=None
        try:
            idHum=r.orderedBy_id
            nodeHum=docRefHum.getNodeByIdNum(idHum)
            oReg=docRef.GetReg('purchase')
            oHum=docRefHum.GetRegByNode(nodeHum)
            sTag=oHum.GetName(nodeHum)
            oLogin=vtSecXmlLogin(usr=sTag,usrId=idHum)
            docRef.SetLogin(oLogin)
            nodeTmp=docRef.getBaseNode()
            try:
                docRefHum.SetNetMaster(docRef)
                n=oReg.Create(nodeTmp,self.__setPurData__,
                                r,docRef,sTag,idHum,docRefGlbs)
                idPar=docRef.getKeyNum(nodeTmp)
                idAdd=docRef.getKeyNum(n)
                lAdd.append(idAdd)
            except:
                vtLog.vtLngTB(self.__class__.__name__)
            docRefHum.SetNetMaster(None)
            idPur=self.doc.getKeyNum(n)
            t=(['vMES=%d'%idPur],
                    ['id=%s'%r.id])
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
        return t
    def __procPurFinReadBack__(self,r,docRef,docRefHum,docRefGlbs,
                    srvXml,srvMsg,lAdd):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        if r is None:
            return None
        t=None
        try:
            nodePur=docRef.getNodeByIdNum(r.vMES)
            if nodePur is None:
                vtLog.vtLngCurCls(vtLog.CRITICAL,'id:%08d;node not found'%(r.vMES),self)
            else:
                t=(['status="chk"'],
                    ['id=%s'%r.id])
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
        return t
    def __postProc__(self,dataInst,srvXml,srvMsg,docRef,d,lAdd):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        try:
            docRefMsg=d['vMsg']['doc']
            
            if len(lAdd):
                docRef.ClearCache()
                docRefMsg.ClearCache()
            for idAdd in lAdd:
                try:
                    n=docRef.getNodeByIdNum(idAdd)
                    docRef.calcFingerPrint(n)
                    nPar=docRef.getParent(n)
                    idPar=docRef.getKeyNum(nPar)
                    val=docRef.GetNodeXmlContent(n,
                                    bFull=False,bIncludeNodes=False)
                    iRet=srvXml.AddNode(docRef.GetApplAlias(),
                                    None,idPar,idAdd,val)
                except:
                    iRet=-1
                    vtLog.vtLngTB(self.__class__.__name__)
                
            iLastIdMsgOld=d['vMsg']['lastId']
            iLastIdMsgNew=docRefMsg.getIds().GetLastId()
            iLastIdMsgOld+=1
            iLastIdMsgNew+=1
            for idAdd in range(iLastIdMsgOld,iLastIdMsgNew):
                n=docRefMsg.getNodeByIdNum(idAdd)
                if n is None:
                    continue
                nPar=docRefMsg.getParent(n)
                idPar=docRefMsg.getKeyNum(nPar)
                try:
                    val=docRefMsg.GetNodeXmlContent(n,
                            bFull=False,bIncludeNodes=False)
                    iRet=srvMsg.AddNode(docRefMsg.GetApplAlias(),
                                    None,idPar,idAdd,val)
                except:
                    vtLog.vtLngTB(self.__class__.__name__)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            self.releaseDict(d,['vHum','vMsg',],sLockName='dom')
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            dataInst.Commit()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            if srvXml is not None:
                srvXml.release()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            if srvMsg is not None:
                srvMsg.release()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        vtLog.vtLngCurCls(vtLog.INFO,'done'%(),self)
    def __procPurWriteResult__(self,r,docRef,docRefHum,srvXml,srvMsg):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        if r is None:
            return None
        t=None
        try:
            idPur=r.vMES
            n=None
            if idPur>=0:
                n=docRef.getNodeByIdNum(idPur)
                oReg=docRef.GetRegByNode(n)
                if oReg is not None:
                    if oReg.GetTagName()=='purchase':
                        # ok check result
                        iRes=oReg.GetResultInt(n)
                        if iRes==1:
                            # go on vacation
                            t=(['status="go"'],
                                ['id=%s'%r.id])
                        elif iRes==-1:
                            # no-go on vacation
                            t=(['status="ngo"'],
                                ['id=%s'%r.id])
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
        return t
    def __preProc__(self,dataInst,srvXml,srvMsg,docRef,d):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        try:
            if srvXml is not None:
                srvXml.acquire()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            if srvMsg is not None:
                srvMsg.acquire()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        if self.acquireDict(d,['vHum','vMsg',],
                        sLockName='dom',blocking=False)==False:
            vtLog.vtLngCur(vtLog.CRITICAL,'locks could not be acquired'%(),self.__class__.__name__)
            #dataInst.Do(self.__preProcClear__,dataInst,srvXml,srvMsg)
            #return
        self.StoreLastId(d)
        docRef.SetNetDocs(d)
        vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
    def __preProcClear__(self,dataInst,srvXml,srvMsg):
        vtLog.vtLngCurCls(vtLog.DEBUG,'do'%(),self)
        try:
            if srvXml is not None:
                srvXml.release()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            if srvMsg is not None:
                srvMsg.release()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        vtLog.vtLngCurCls(vtLog.DEBUG,'done'%(),self)
    def SynchWithDB(self,node,dataInst,lang='en'):
        vtLog.vtLngCurCls(vtLog.INFO,'id:%s;starting'%(self.doc.getKey(node)),self)
        srvXml=None
        srvMsg=None
        docRef=None
        lAdd=[]
        d={}
        try:
            srvXml=self.GetRefDataInst(node)
            srvMsg=self.GetRefDataInst(node,sTagAliasRef='aliasRefMsg')
            docRef=self.GetRefDoc(node)
            if docRef is None:
                vtLog.vtLngCurCls(vtLog.ERROR,'alias reference doc not found',self)
            nodeBase=docRef.getBaseNode()
            if docRef.hasNodeInfos2Get(self.__class__.__name__)==False:
                docRef.setNodeInfos2Get(['|id','tag','name','description'],
                        name=self.__class__.__name__)
            
            c=docRef.getChildByLst(docRef.getRoot(),['config','vHum','host'])
            alias=docRef.getNodeText(c,'alias')
            docRefHum=self.GetSubRefDoc(node,'vHum:%s'%alias)
            c=docRef.getChildByLst(docRef.getRoot(),['config','vGlobals','host'])
            alias=docRef.getNodeText(c,'alias')
            docRefGlbs=self.GetSubRefDoc(node,'vGlobals:%s'%alias)
            docRefMsg=self.GetRefDoc(node,sTagAliasRef='aliasRefMsg')
            d={ 'vHum':     {'doc':docRefHum,   'bNet':'False'},
                'vGlobals': {'doc':docRefGlbs,  'bNet':'False'},
                'vMsg':     {'doc':docRefMsg,   'bNet':'False'},}
            #docRef.procChildsKeysRec(0,nodeBase,self.__procGlobals__,
            #            docRef,dataInst,lang=lang)
            dataInst.Do(self.__preProc__,dataInst,srvXml,srvMsg,docRef,d)
            dataInst.SelectUpdate('purchase_purchaseadvanced',
                    u'select * from purchase_purchaseadvanced where status="req"',
                    self.__procPurReadBack__,docRef,docRefHum,docRefGlbs,
                                    srvXml,srvMsg,lAdd)
            dataInst.Commit()
            dataInst.SelectUpdate('purchase_purchaseadvanced',
                    u'select * from purchase_purchaseadvanced,purchase_partadvanced where status="req" and purchase_partadvanced.purchase_id=purchase_purchaseadvanced.id',
                    self.__procPurPartsReadBack__,docRef,docRefHum,docRefGlbs,
                                    srvXml,srvMsg,lAdd)
            dataInst.SelectUpdate('purchase_purchaseadvanced',
                    u'select * from purchase_purchaseadvanced where status="req"',
                    self.__procPurFinReadBack__,docRef,docRefHum,docRefGlbs,
                                    srvXml,srvMsg,lAdd)
            dataInst.SelectUpdate('purchase_purchaseadvanced',
                    u'select * from purchase_purchaseadvanced where status="chk"',
                    self.__procPurWriteResult__,docRef,docRefHum,srvXml,srvMsg)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        try:
            dataInst.Do(self.__postProc__,dataInst,srvXml,srvMsg,docRef,d,lAdd)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    # ---------------------------------------------------------
    # inheritance
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xaaIDAT8\x8d\xc5\x931\x0e\xc4 \x0c\x04\xd7\x1c\xdf\xe1^\x83\x94\x9f\
\xa04TiP~\x82\x94\xdf\xe4A\xa4\x88\x88\x90Y\xdf]w[\xae\xccz,\x1b\x11\xf7\x82\
V(\xb1M&\x80s=D{\xee\xd7B\xe6\x01\x80t\x02\xab\xab\xa5\x1e\xe8\xad\x82\x9c\
\xea\xe4m\xfb2yt\x04\xf6\xd8\xf2}(\xb1\x8d\xf3\x8dE\xbacN\x159\xd5\xc7\x0f%6\
y\xefK\xd3E\x16.\xd3\x13`a\x8fb\xa1\xff'\xf0\xc0\xbd\xd3M\x05}#8\xd7CB\x89\
\xcdY\x17\xc6\x88\xfa\x16\xc6\x10z\x89\x9f\xc6\xe9d\xbd1=$\x0b\x9fn\x81\xfdF\
M4v\xd4\xa2\x04\xeccY\x9f\xed\x02\xcc\x89Z2$\xae\xce\xe2\x00\x00\x00\x00IEND\
\xaeB`\x82" 
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeXmlDjangoSrvAliasRefPurchasePanel
        else:
            return None
