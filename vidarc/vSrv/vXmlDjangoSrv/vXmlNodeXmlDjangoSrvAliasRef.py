#----------------------------------------------------------------------------
# Name:         vXmlNodeXmlDjangoSrvAliasRef.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20070920
# CVS-ID:       $Id: vXmlNodeXmlDjangoSrvAliasRef.py,v 1.3 2008/02/04 16:35:08 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase
import time,sha,random

from vidarc.vSrv.net.vsNetXmlNodeAliasRef import vsNetXmlNodeAliasRef
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeXmlDjangoSrvAliasRef(vsNetXmlNodeAliasRef):
    Z_LOCK_ACQUIRE_SLEEP=0.1
    def __init__(self,tagName='AliasRef_vHum'):
        global _
        _=vtLgBase.assignPluginLang('vXmlDjangoSrv')
        vsNetXmlNodeAliasRef.__init__(self,tagName)
    def GetDescription(self):
        return _(u'alias reference XML server')
    # ---------------------------------------------------------
    # specific
    def GetRefDoc(self,node,sTagAliasRef='aliasRef'):
        try:
            netRefSrv=self.GetRefServerInstanceByNode(node,
                                sTagAliasRef=sTagAliasRef)
            #print netRefSrv
            if netRefSrv is not None:
                applAlias=self.GetApplAlias(node,sTagAliasRef=sTagAliasRef)
                if applAlias is not None:
                    srvXml=netRefSrv.GetDataInstance()
                    docRef=srvXml.GetServedXmlDom(applAlias)
                    return docRef
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return None
    def GetSubRefDoc(self,node,applAlias,sTagAliasRef='aliasRef'):
        try:
            netRefSrv=self.GetRefServerInstanceByNode(node,sTagAliasRef=sTagAliasRef)
            #print netRefSrv
            #vtLog.vtLngCurCls(vtLog.DEBUG,'netRef is None==%s'%(netRefSrv is None),self)
            if netRefSrv is not None:
                if applAlias is not None:
                    #vtLog.vtLngCurCls(vtLog.DEBUG,'data inst1'%(),self)
                    srvXml=netRefSrv.GetDataInstance()
                    #vtLog.vtLngCurCls(vtLog.DEBUG,'data inst2'%(),self)
                    docRef=srvXml.GetServedXmlDom(applAlias)
                    #vtLog.vtLngCurCls(vtLog.DEBUG,'doc gotten'%(),self)
                    return docRef
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return None
    def GetRefDataInst(self,node,sTagAliasRef='aliasRef'):
        try:
            netRefSrv=self.GetRefServerInstanceByNode(node,
                                sTagAliasRef=sTagAliasRef)
            if netRefSrv is not None:
                srvXml=netRefSrv.GetDataInstance()
                return srvXml
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return None
    def IsLinked2DB(self):
        return True
    def SynchWithDB(self,node,dataInst,lang='en'):
        pass
    def acquireDict(self,d,lName2Lock,sLockName=None,blocking=False,zTimeOut=30):
        bOk=True
        zNow=time.time()
        try:
            dLocks={}
            while 1:
                #for name,dd in d.iteritems():
                for name in lName2Lock:
                    if name not in dLocks:
                        dd=d[name]
                        if dd['doc'].acquire(sLockName,blocking=blocking)==True:
                            dLocks[name]=time.time()
                        else:
                            if blocking==True:
                                bOk=False
                if blocking==False:
                    if len(dLocks)==len(lName2Lock):
                        break
                    if (zNow+zTimeOut)>time.time():
                        bOk=False
                        break
                    time.sleep(self.Z_LOCK_ACQUIRE_SLEEP)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
            bOk=False
        if bOk==False:
            for name in dLocks.iterkeys():
                dd=d[name]
                dd['doc'].release(sLockName)
            return False
        return True
    def releaseDict(self,d,lName2Lock,sLockName=None):
        try:
            l=[]
            #for name,dd in d.iteritems():
            for name in lName2Lock:
                dd=d[name]
                dd['doc'].release(sLockName)
                l.append(name)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
            return False
        return True
    def StoreLastId(self,d):
        try:
            for name,dd in d.iteritems():
                doc=dd['doc']
                dd['lastId']=doc.getIds().GetLastId()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
