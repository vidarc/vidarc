#----------------------------------------------------------------------------
# Name:         __register__.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060728
# CVS-ID:       $Id: __register__.py,v 1.1 2006/08/29 10:58:39 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.vSrv.vFileSrv.vXmlNodeFileSrv import vXmlNodeFileSrv
from vidarc.vSrv.vFileSrv.vXmlNodeFileSrvAlias import vXmlNodeFileSrvAlias
from vidarc.vSrv.net.vXmlNodeHosts import vXmlNodeHosts
from vidarc.vSrv.net.vsNetXmlNodeAliasRef import vsNetXmlNodeAliasRef

def RegisterNodes(self,oRoot,bRegAsRoot=True):
    try:
        oFileSrv=vXmlNodeFileSrv()
        oFileSrvAlias=vXmlNodeFileSrvAlias()
        oHosts=vXmlNodeHosts()
        oRef=vsNetXmlNodeAliasRef()
        
        self.RegisterNode(oFileSrv,bRegAsRoot)
        self.RegisterNode(oFileSrvAlias,False)
        self.RegisterNode(oHosts,False)
        self.RegisterNode(oRef,False)
        self.LinkRegisteredNode(oRoot,oFileSrv)
        self.LinkRegisteredNode(oFileSrv,oFileSrvAlias)
        self.LinkRegisteredNode(oFileSrv,oRef)
        self.LinkRegisteredNode(oFileSrv,oHosts)
        self.LinkRegisteredNode(oFileSrvAlias,oHosts)
        self.LinkRegisteredNode(oFileSrvAlias,oRef)
        return oFileSrv
    except:
        vtLog.vtLngTB(__name__)
    return None
