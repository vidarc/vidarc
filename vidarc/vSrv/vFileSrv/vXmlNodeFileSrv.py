#----------------------------------------------------------------------------
# Name:         vXmlNodeFileSrv.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060728
# CVS-ID:       $Id: vXmlNodeFileSrv.py,v 1.3 2007/07/30 20:35:57 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vSrv.net.vsNetXmlNodeXmlSrv import vsNetXmlNodeXmlSrv
try:
    if vcCust.is2Import(__name__):
        from vidarc.vSrv.vXmlSrv.vXmlNodeXmlSrvPanel import vXmlNodeXmlSrvPanel
        from vidarc.vSrv.net.vsNetXmlNodeSrvEditDialog import vsNetXmlNodeSrvEditDialog
        from vidarc.vSrv.net.vsNetXmlNodeSrvAddDialog import vsNetXmlNodeSrvAddDialog
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeFileSrv(vsNetXmlNodeXmlSrv):
    def __init__(self,tagName='vFileSrv'):
        global _
        _=vtLgBase.assignPluginLang('vFileSrv')
        vsNetXmlNodeXmlSrv.__init__(self,tagName)
    def GetDescription(self):
        return _(u'XML File server')
    # ---------------------------------------------------------
    # specific
    def GetPortData(self,node):
        return self.GetVal(node,'portData',int,50006)
    def GetPortService(self,node):
        return self.GetVal(node,'portService',int,50007)
    def GetServerCls(self):
        from vidarc.tool.net.vtNetSrv import vtNetSrv
        return vtNetSrv
    def GetDataSrvCls(self):
        from vidarc.tool.net.vtNetFileSrv import vtSrvFileSock
        return vtSrvFileSock
    def GetDataCls(self):
        from vidarc.tool.net.vtNetFileSrv import vtNetFileSock
        return vtNetFileSock
    def GetServiceSrvCls(self):
        from vidarc.tool.net.vtNetSrv import vtSrvSock
        return vtSrvSock
    def GetServiceCls(self):
        from vidarc.tool.net.vtNetSrv import vtServiceSock
        return vtServiceSock
    def __procAliases__(self,node,srvFile,appl,bStart):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCurCls(vtLog.INFO,'appl:%s;bStart:%s;id:%s'%(appl,bStart,self.doc.getKey(node)),self)
        oReg=self.doc.GetRegByNode(node)
        if hasattr(oReg,'GetDoc2Serve'):
            if hasattr(oReg,'HasChilds2Serve'):
                self.doc.procChildsKeys(node,self.__procAliases__,
                            srvFile,oReg.GetTag(node),bStart)
                return 0
            alias=oReg.GetTag(node)
            sApplAlias=':'.join([appl,alias])
            if bStart:
                vtLog.PrintMsg(' '.join([self.GetDescription(),_('start'),sApplAlias]))
                doc=oReg.GetDoc2Serve(node)
                sDir=oReg.GetDN(node)
                lHosts=oReg.GetHosts(node)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurCls(vtLog.DEBUG,'dn:%s;doc:%s'%(sDir,doc),self)
                hosts=oReg.GetHosts(node)
                srvXml=None
                sApplAliasRef=None
                nodeAlias=self.doc.getChild(node,'AliasRef')
                if nodeAlias is None:
                    vtLog.vtLngCurCls(vtLog.ERROR,'alias reference not found',self)
                    return
                #print nodeAlias
                oReg=self.doc.GetRegByNode(nodeAlias)
                #print oReg
                if oReg is None:
                    vtLog.vtLngCurCls(vtLog.ERROR,'alias reference not registered',self)
                    return
                netRefSrv=oReg.GetRefServerInstanceByNode(nodeAlias)
                #print netRefSrv
                if netRefSrv is not None:
                    sApplAliasRef=oReg.GetApplAlias(nodeAlias)
                    if sApplAliasRef is not None:
                        srvXml=netRefSrv.GetDataInstance()
                        #docRef=srvXml.GetServedXmlDom(sApplAliasRef)
                srvFile.AddServedDN(sApplAlias,sDir,srvXml,sApplAliasRef)
                srvFile.SetServedHosts(sApplAlias,lHosts,None)
            else:
                vtLog.PrintMsg(' '.join([self.GetDescription(),_('stop'),sApplAlias]))
                srvFile.DelServedDN(sApplAlias)
        return 0
    def SetupServer(self,node):
        vtLog.vtLngCurCls(vtLog.INFO,'id:%s;'%(self.doc.getKey(node)),self)
        netSrv=self.doc.GetServerInstanceByNode(node)
        #vtLog.CallStack('')
        #print netSrv
        if netSrv is not None:
            nodeAlias=self.doc.getChild(node,'AliasRef')
            if nodeAlias is None:
                vtLog.vtLngCurCls(vtLog.ERROR,'alias reference not found',self)
                return
            #print nodeAlias
            oReg=self.doc.GetRegByNode(nodeAlias)
            #print oReg
            if oReg is None:
                vtLog.vtLngCurCls(vtLog.ERROR,'alias reference not registered',self)
                return
            netRefSrv=oReg.GetRefServerInstanceByNode(nodeAlias)
            #print netRefSrv
            if netRefSrv is not None:
                applAlias=oReg.GetApplAlias(nodeAlias)
                if applAlias is not None:
                    srvXml=netRefSrv.GetDataInstance()
                    docRef=srvXml.GetServedXmlDom(applAlias)
                    #print docRef
                    srvFile=netSrv.GetDataInstance()
                    srvFile.SetDocHum(docRef)
                else:
                    vtLog.vtLngCurCls(vtLog.ERROR,'alias:%s not served'%(applAlias),self)
            else:
                vtLog.vtLngCurCls(vtLog.ERROR,'referenced server not started or found',self)
    def Start(self,node,dataInst):
        appl=self.GetTag(node)
        vtLog.vtLngCurCls(vtLog.INFO,'id:%s;appl:%s'%(self.doc.getKey(node),appl),self)
        self.doc.procChildsKeys(node,self.__procAliases__,dataInst,appl,True)
    def Stop(self,node,dataInst):
        appl=self.GetTag(node)
        vtLog.vtLngCurCls(vtLog.INFO,'id:%s;appl:%s'%(self.doc.getKey(node),appl),self)
        self.doc.procChildsKeys(node,self.__procAliases__,dataInst,appl,False)
    # ---------------------------------------------------------
    # inheritance
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02RIDAT8\x8d}\x93OOSQ\x10\xc5\x7f\xf7\x15b\x8c]\x14Lh1\x12\xb4\xa6\x92\
.\x8c\xbd\x85\xb73\xf1\x1b([]\x984O4\xe1\x13\x10\x89K\xd1\xb8\x80\x15\x1a\
\xab\x8d{\x12!,$&.t\x0bO\xdekHI H\x8b\xf2_\xab\xa0\xb4Fx\xc0\xb8x\x05J\xaa\
\x9e\x9bIf3g\xce\xb93\xa3\x94\x11\xa0\x1ab\xed\n\xb8`j\xb0]\x00T\xc6T\xfc\
\x03u\xc7\nM?w\xba4\x00I[\x03.\x93{\xbb\x02\xd0\x1e\xa8\xab%RF\x00\xba\x1c!-\
~T=GD\x1c\x11\xd1.\xe2\xa4-a\x08QF\x80\xea0\x00_.@\xe8\xa8\x81\x83@\xf66VV\
\x91\xc1\xc223\xe8\xd8\xdfL\xa4E\x8e)\x18B\x18Bt\xa2\xa2\xc2\xb5D\xbb~\xae]j\
T\x18\xd8\xc7?\x8cM\x81M\xc1\xbd\x07ZC2\x91\x81\x14\xe8,\xb8s\xb5\xfd\r\xec\
\x83T\xa3M\x05]\x95\x002\xdd\x16\xba\xe2\x8e\x14\x0c,\x0f0\x16\x1cctdD\x8e\
\x08L\xe0\x89\x8b\xeeV\xbc\n\xe5q\xb2\x16\xces\x8b\xbc\x99\'\x19\xca\x00\xe0\
\xe2p\xe3\xe6cb\xb1\x18\xe5r\x99`0\xc8@\x7f\xbf\x00(\x12\x8e?>\x1b\xf4\xcb$\
\xee\x1c81\x8bP\xa8\x97\xa8\x1d\x85\xb7\x0e\xfd\xf1\xf7\xc4\xe3qJ[%\x1aO72\
\x9d\x9bf>_`\xbb8\x8b\xa1\xa6L\x85\r\x98\xe0\xa6\x9cCoQ;\x8a\xee\x83\xa7\xed\
\xe3\xe8\x84\xc6\xf3<"\x91\x08\xb9\\\x8eB\xa1@q}\x95\xb6f\xaf2F\x00\xdb/\xd6\
1H\xce\xf9\xd2\xbbo\xf5\x10\xbb\xd8FS8L\xcb\xd9\x16fff\xf8\xb4\xf0\x99/k+t\
\xb4\x96i\xd8\xcd\xfa\x04j\xeahU\xdd\xd4\x0b\x00\x86\x97\xefs5\xb8\x8d\x14l\
\x8a\xab\x05D)\xb6J%\xd6V\x16\xe9h-\x13\xfa=N~i\x13Us\x0b\xfb\xcf\x04`\xf4\
\xe1\x02m\xe5Y\xbe\xee\x97\x08\x9f\xbb\xc4\xb7\x86\x16^\x7f\x98\xa7Q>\xd2\
\xb03\xc1\xc2\xd2\x06^s\xe7\xd1-\x1c@\x19w\x15\x80\x92\x1e\xd9\xd8\xf9\x81\
\xbd:\xc1\x85\xf2"\x1dm\xed\\>\xb5\xce\xcf\xe2$\x0b+\xdf\xd9\t_\xe7\xd1\xe0\
\xb0\xaaQP\r\xfb\xc1\x1d\xa9\xaf_\xc6\xa3\xc4\x99\x13M\xbc\x9b~\xc3l\xb1\xcc\
^s\'}\x83\xc3\xca\xdf\x83\xff\xc0\xecM\xab\xe2\xc9+x\xbf\xce\xb3W\xf2X\xdf2\
\xf0"\xd7\x0e\x8b\x01\xfe\x00s`\x14\xf8\xf2C\xe9\xac\x00\x00\x00\x00IEND\xae\
B`\x82' 
    def GetEditDialogClass(self):
        if GUI:
            return vsNetXmlNodeSrvEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return vsNetXmlNodeSrvAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeXmlSrvPanel
        else:
            return None
