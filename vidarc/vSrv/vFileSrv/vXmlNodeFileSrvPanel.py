#----------------------------------------------------------------------------
# Name:         vXmlNodeFileSrvPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060728
# CVS-ID:       $Id: vXmlNodeFileSrvPanel.py,v 1.1 2006/08/29 10:58:39 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.vSrv.net.vsNetXmlNodeSrvPanel import vsNetXmlNodeSrvPanel

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.net.vtNetXml import *
from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

class vXmlNodeFileSrvPanel(vsNetXmlNodeSrvPanel):
    def Clear(self):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.Clear(self)
        
        # add code here
        self.viTag.Clear()
        self.viName.Clear()
        self.vsSrv.Clear()
        self.intData.SetValue(self.objRegNode.GetPortData(None))
        self.intService.SetValue(self.objRegNode.GetPortService(None))
        self.vsSrv.Enable(False)
        self.ClrBlockDelayed()
    def SetDoc(self,doc,bNet=False):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        
        # add code here
        self.viTag.SetDoc(doc)
        self.viName.SetDoc(doc)
        self.vsSrv.SetDoc(doc,bNet)
        self.vsSrv.SetDataSrvClass(vtSrvXmlSock)
        self.vsSrv.SetDataClass(vtNetXmlSock)
    def OnVsSrvVsnetSrvcontrolStarted(self, event):
        event.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
            netHum=self.doc.GetNetDoc('vHum')
            srv=self.vsSrv.GetNetSrv()
            srvXml=srv.GetDataInstance()
            srvXml.SetHumDoc4SecAcl(netHum)
            #self.objRegNode.AddServerInstanceByNode(self.node,srv)
        except:
            vtLog.vtLngTB(self.GetName())
        
