#----------------------------------------------------------------------------
# Name:         vXmlNodeFileSrvAlias.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060728
# CVS-ID:       $Id: vXmlNodeFileSrvAlias.py,v 1.1 2006/08/29 10:58:39 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vSrv.net.vsNetXmlNodeSrvAlias import vsNetXmlNodeSrvAlias
try:
    if vcCust.is2Import(__name__):
        from vidarc.vSrv.vFileSrv.vXmlNodeFileSrvAliasPanel import vXmlNodeFileSrvAliasPanel
        from vidarc.vSrv.vFileSrv.vXmlNodeFileSrvAliasEditDialog import vXmlNodeFileSrvAliasEditDialog
        from vidarc.vSrv.vFileSrv.vXmlNodeFileSrvAliasAddDialog import vXmlNodeFileSrvAliasAddDialog
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeFileSrvAlias(vsNetXmlNodeSrvAlias):
    def __init__(self,tagName='vFileSrvAlias'):
        global _
        _=vtLgBase.assignPluginLang('vFileSrv')
        vsNetXmlNodeSrvAlias.__init__(self,tagName)
    def GetDescription(self):
        return _(u'File server alias')
    # ---------------------------------------------------------
    # specific
    def GetDN(self,node):
        return self.Get(node,'dn')
    def SetDN(self,node,val):
        self.Set(node,'dn',val)
    def GetDoc2Serve(self,node,appl=None):
        if node is None:
            return None
        nodePar=self.doc.getParent(node)
        if nodePar is None:
            return None
        oReg=self.doc.GetRegByNode(nodePar)
        return oReg.GetDoc2Serve(nodePar)
    # ---------------------------------------------------------
    # inheritance
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xb0IDAT8\x8d\x8d\x93\xbfO\x13q\x18\xc6?\xdf\xfeP\x8c\x8d\xb67\xb4b8\
\x1b\x86\xb6,'\x83M$\r\x89.\x8e\x1a\x06\x12\x17\x07esppspp$\xfe\x112\xb1\xb1\
A\x0c\x1b\xa3A\xb4B(\xe1X\xbcC\n\x14\xecQ\xcd\xdd\x95\xc2\xb5\x9c\x83\x1e\
\xda\xfb\x11|\xc6\xf7\xc7\xf3}\x9e\xf7}\xbfB\xc4\xe2\xf8\xa1LO\xba\x81 \xb0\
\xfejN\xf8c\xb1\xff-\x0c\x8b\x01\x08OA\xd4\xabQ\xf0\x08\x13\x17\x15>K\x8eQ*\
\x95\xe8:]^o\xcd\x06\xf2\xa1\x16<<\xe1\x0e\x85B\x01\xdb\xb6I\xa5RL]\xaa\x04\
\t\xa2\xa4?M\xdcEQ\x14\x8e\xdb\xc7H\x92D\xadVC\xd3t*k\xe6y\x8d2=\xe9\x8a\xdb\
o\x1f\x07\x08\x9e_\xbb\xcfHq\x04\xd32\xc9\xa43T\xbfT\xd15\x9d\xc3\xc6>\xe5!\
\x83\x99\xac\x14m\xe1Eb\x98B\xb1D6\x97C\x1e\x92QU\x95m\xfd\x1b\x87\x8d=\xcay\
\x9bLw5z\x06o\x92E\xee\xa5Np\xb5\x15\x9a\xfb\x1a\xae\x10\x98\x96Eco\x87r\xde\
&\xddY\xe6k\xfdG\x1fA\xdf\x16\xe2\x1d\x973\xa3\xce\xc0\x99J\xaeg`\xb4d\x8e\
\x0e\xb6)\xe7\xdb\xa4O>\xa2\xef\xb6\xe8\x0eN\x04\x15x;\x15\xaeK\xeb\xf4'+;\
\x1f\xd8\xdcx\xcf\xad\xef\x9f\x18\xbd\xaar\xbd\xb3\x8c^78\xcd=dA\xfe-\xfa\
\xbc\xc7\x7fH\xefz\x12\xc9\xe4.\x0e\x167/gY\xdaXd\xabi\xd3\x1b\x9c`^\xfe\xeb\
\xd8#\x08\x0cq*~D\xf3\xca8N{\x98\x9e\xe5p`\xc6pn<\xeak\x8e\x9c\x81\x87\x97\
\xf6g\x18\xf8\x93\x1e}\x10\xda\xe8A\x84\xfd\xc6\x7f-\xf9%\xfb\x11\xaa+\xec:\
\xa3.\xf6\x17lQ\xa8\xa4#)3\x96\x00\x00\x00\x00IEND\xaeB`\x82" 
    def GetEditDialogClass(self):
        if GUI:
            return vXmlNodeFileSrvAliasEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return vXmlNodeFileSrvAliasAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeFileSrvAliasPanel
        else:
            return None
