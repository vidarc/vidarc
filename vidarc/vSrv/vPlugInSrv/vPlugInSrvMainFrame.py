#Boa:Frame:vPlugInSrvMainFrame
#----------------------------------------------------------------------------
# Name:         vPlugInSrvMainFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060627
# CVS-ID:       $Id: vPlugInSrvMainFrame.py,v 1.1 2006/07/17 10:41:32 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.xml.vtXmlNodeRegSelector
import vidarc.tool.sec.vtSecAclFileTreeButtons
import wx.lib.filebrowsebutton
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons
import getpass,time

import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.vApps.common.vAboutDialog as vAboutDialog
import vidarc.vApps.common.vSystem as vSystem
from vidarc.vApps.common.vMainFrame import vMainFrame
from vidarc.vApps.common.vStatusBarMessageLine import vStatusBarMessageLine
from vidarc.tool.log.vtLogFileViewerFrame import *

from vidarc.vSrv.vPlugInSrv.vXmlPlugInSrvTree  import *
from vidarc.vSrv.vPlugInSrv.vNetPlugInSrv import *
from vidarc.tool.net.vtNetFileSrv import *
#from vidarc.vApps.vPlugInSrv.vPlugInSrvListBook import *
from vidarc.tool.net.vtNetSecXmlGuiMaster import *
import vidarc.tool.xml.vtXmlDomConsumer as vtXmlDomConsumer
import vidarc.tool.InOut.fnUtil as fnUtil
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt

from vidarc.vApps.vHum.vNetHum import *

import vidarc.vSrv.vPlugInSrv.images as images

def create(parent):
    return vPlugInSrvMainFrame(parent)

[wxID_VGFPRJMAIN, wxID_VGFPRJMAINSPWMAIN, 
] = map(lambda _init_ctrls: wx.NewId(), range(2))

[wxID_VPLUGINSRVMAINFRAME, wxID_VPLUGINSRVMAINFRAMECBSTART, 
 wxID_VPLUGINSRVMAINFRAMECBSTOP, wxID_VPLUGINSRVMAINFRAMEPNDATA, 
 wxID_VPLUGINSRVMAINFRAMESBSTATUS, wxID_VPLUGINSRVMAINFRAMESLWNAV, 
 wxID_VPLUGINSRVMAINFRAMEVIREGSEL, 
] = [wx.NewId() for _init_ctrls in range(7)]

STATUS_TEXT_POS=2
STATUS_LOG_POS=3
STATUS_CLK_POS=4

def getPluginImage():
    return images.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon

[wxID_VGFPRJTIMERMAINTOOLBAR1TOOLS0] = [wx.NewId() for _init_coll_toolBar1_Tools in range(1)]

[wxID_VPLUGINSRVMAINFRAMEMNHELPMN_HELP_ABOUT, 
 wxID_VPLUGINSRVMAINFRAMEMNHELPMN_HELP_HELP, 
 wxID_VPLUGINSRVMAINFRAMEMNHELPMN_HELP_LOG, 
] = [wx.NewId() for _init_coll_mnHelp_Items in range(3)]

[wxID_VPLUGINSRVMAINFRAMEMNFILEITEM_EXIT] = [wx.NewId() for _init_coll_mnFile_Items in range(1)]

class vPlugInSrvMainFrame(wx.Frame,vMainFrame,vStatusBarMessageLine):
    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbStart, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbStop, 0, border=0, flag=0)

    def _init_coll_bxsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER)
        parent.AddWindow(self.viRegSel, 1, border=4, flag=wx.EXPAND | wx.ALL)

    def _init_coll_mnFile_Items(self, parent):
        # generated method, don't edit

        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VPLUGINSRVMAINFRAMEMNFILEITEM_EXIT,
              kind=wx.ITEM_NORMAL, text=_(u'E&xit\tAlt+X'))
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_exitMenu,
              id=wxID_VPLUGINSRVMAINFRAMEMNFILEITEM_EXIT)

    def _init_coll_mbMain_Menus(self, parent):
        # generated method, don't edit

        parent.Append(menu=self.mnFile, title=_(u'&File'))
        parent.Append(menu=self.mnView, title=_(u'View'))
        parent.Append(menu=self.mnAnalyse, title=_(u'Analyse'))
        parent.Append(menu=self.mnTools, title=_(u'Tools'))
        parent.Append(menu=self.mnHelp, title=u'?')

    def _init_coll_mnHelp_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_VPLUGINSRVMAINFRAMEMNHELPMN_HELP_HELP,
              kind=wx.ITEM_NORMAL, text=u'?\tF1')
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VPLUGINSRVMAINFRAMEMNHELPMN_HELP_ABOUT,
              kind=wx.ITEM_NORMAL, text=_(u'About'))
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VPLUGINSRVMAINFRAMEMNHELPMN_HELP_LOG,
              kind=wx.ITEM_NORMAL, text=_(u'Log'))
        self.Bind(wx.EVT_MENU, self.OnMnHelpMn_help_helpMenu,
              id=wxID_VPLUGINSRVMAINFRAMEMNHELPMN_HELP_HELP)
        self.Bind(wx.EVT_MENU, self.OnMnHelpMn_help_aboutMenu,
              id=wxID_VPLUGINSRVMAINFRAMEMNHELPMN_HELP_ABOUT)
        self.Bind(wx.EVT_MENU, self.OnMnHelpMn_help_logMenu,
              id=wxID_VPLUGINSRVMAINFRAMEMNHELPMN_HELP_LOG)

    def _init_coll_sbStatus_Fields(self, parent):
        # generated method, don't edit
        parent.SetFieldsCount(5)

        parent.SetStatusText(number=0, text=u'')
        parent.SetStatusText(number=1, text=u'')
        parent.SetStatusText(number=2, text=u'')
        parent.SetStatusText(number=3, text=u'')
        parent.SetStatusText(number=4, text=u'')

        parent.SetStatusWidths([20, 120, -1, 100, 200])

    def _init_sizers(self):
        # generated method, don't edit
        self.bxsData = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_bxsData_Items(self.bxsData)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.pnData.SetSizer(self.bxsData)

    def _init_utils(self):
        # generated method, don't edit
        self.mbMain = wx.MenuBar()

        self.mnFile = wx.Menu(title=u'')

        self.mnAnalyse = wx.Menu(title=u'')

        self.mnTools = wx.Menu(title=u'')

        self.mnView = wx.Menu(title=u'')

        self.mnHelp = wx.Menu(title=u'')

        self._init_coll_mbMain_Menus(self.mbMain)
        self._init_coll_mnFile_Items(self.mnFile)
        self._init_coll_mnHelp_Items(self.mnHelp)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VPLUGINSRVMAINFRAME,
              name=u'vPlugInSrvMainFrame', parent=prnt, pos=wx.Point(115, 10),
              size=wx.Size(819, 394), style=wx.DEFAULT_FRAME_STYLE,
              title=u'VIDARC PluginSrv')
        self._init_utils()
        self.SetClientSize(wx.Size(811, 367))
        self.SetMenuBar(self.mbMain)
        self.Bind(wx.EVT_CLOSE, self.OnFrameClose)
        self.Bind(wx.EVT_ACTIVATE, self.OnFrameActivate)
        self.Bind(wx.EVT_SIZE, self.OnFrameSize)
        self.Bind(wx.EVT_MOVE, self.OnFrameMove)

        self.sbStatus = wx.StatusBar(id=wxID_VPLUGINSRVMAINFRAMESBSTATUS,
              name=u'sbStatus', parent=self, style=0)
        self._init_coll_sbStatus_Fields(self.sbStatus)
        self.SetStatusBar(self.sbStatus)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VPLUGINSRVMAINFRAMESLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(-600, 0),
              size=wx.Size(811, 328), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetDefaultSize(wx.Size(200, 1000))
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetAutoLayout(True)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetLabel(_(u'navigation'))
        self.slwNav.SetToolTipString(_(u'navigation'))
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VPLUGINSRVMAINFRAMESLWNAV)

        self.pnData = wx.Panel(id=wxID_VPLUGINSRVMAINFRAMEPNDATA,
              name=u'pnData', parent=self, pos=wx.Point(216, 8),
              size=wx.Size(584, 312), style=wx.TAB_TRAVERSAL)

        self.viRegSel = vidarc.tool.xml.vtXmlNodeRegSelector.vtXmlNodeRegSelector(id=wxID_VPLUGINSRVMAINFRAMEVIREGSEL,
              name=u'viRegSel', parent=self.pnData, pos=wx.Point(4, 42),
              size=wx.Size(576, 266), style=0)
        self.viRegSel.Bind(vidarc.tool.xml.vtXmlNodeRegSelector.vEVT_TOOL_XML_REG_SELECTOR_CANCEL,
              self.OnViRegSelToolXmlRegSelectorCancel,
              id=wxID_VPLUGINSRVMAINFRAMEVIREGSEL)
        self.viRegSel.Bind(vidarc.tool.xml.vtXmlNodeRegSelector.vEVT_TOOL_XML_REG_SELECTOR_APPLY,
              self.OnViRegSelToolXmlRegSelectorApply,
              id=wxID_VPLUGINSRVMAINFRAMEVIREGSEL)

        self.cbStart = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPLUGINSRVMAINFRAMECBSTART,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'start'), name=u'cbStart',
              parent=self.pnData, pos=wx.Point(212, 4), size=wx.Size(76, 30),
              style=0)
        self.cbStart.SetMinSize(wx.Size(-1, -1))
        self.cbStart.Enable(True)
        self.cbStart.Bind(wx.EVT_BUTTON, self.OnCbStartButton,
              id=wxID_VPLUGINSRVMAINFRAMECBSTART)

        self.cbStop = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPLUGINSRVMAINFRAMECBSTOP,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'stop'), name=u'cbStop',
              parent=self.pnData, pos=wx.Point(296, 4), size=wx.Size(76, 30),
              style=0)
        self.cbStop.SetMinSize(wx.Size(-1, -1))
        self.cbStop.Enable(False)
        self.cbStop.Bind(wx.EVT_BUTTON, self.OnCbStopButton,
              id=wxID_VPLUGINSRVMAINFRAMECBSTOP)

        self._init_sizers()

    def __init__(self, parent):
        self._init_ctrls(parent)
        vMainFrame.__init__(self)
        vStatusBarMessageLine.__init__(self,self.sbStatus,STATUS_TEXT_POS,5000)
        self.bActivated=False
        
        self.nodeCurStarted=None
        self.nodeCurStartedTreeItem=None
        self.nodeLock=None
        
        self.baseDoc=None
        self.baseSettings=None
        
        self.xdCfg=vtXmlDom(appl='cfg')
        
        icon = getApplicationIcon()
        self.SetIcon(icon)
        
        appls=['vPlugIn','vHum']
        
        rect = self.sbStatus.GetFieldRect(0)
        self.netMaster=vtNetSecXmlGuiMaster(self.sbStatus,'Master',pos=(rect.x+2, rect.y+2),
                    size=(rect.width-4, rect.height-4),verbose=1)
        
        self.netPlugInSrv=self.netMaster.AddNetControl(vNetPlugInSrv,'vPlugIn',synch=True,verbose=0)
        self.netHum=self.netMaster.AddNetControl(vNetHum,'vHum',synch=True,verbose=0)
        self.netMaster.AddNetControl(None,None,verbose=0)
        
        self.netMaster.SetCfg(self.xdCfg,[],appls,'vPlugInSrvAppl')
        EVT_NET_XML_OPEN_OK(self.netPlugInSrv,self.OnOpenOk)
        
        EVT_NET_XML_SYNCH_PROC(self.netPlugInSrv,self.OnSynchProc)
        EVT_NET_XML_SYNCH_FINISHED(self.netPlugInSrv,self.OnSynchFinished)
        EVT_NET_XML_SYNCH_ABORTED(self.netPlugInSrv,self.OnSynchAborted)
        
        EVT_NET_XML_GOT_CONTENT(self.netPlugInSrv,self.OnGotContent)
        
        self.bAutoConnect=True
        
        self.vgpTree=vXmlPlugInSrvTree(self.slwNav,wx.NewId(),
                pos=(0,0),size=(198,529),style=0,name="trPlugIn",
                master=True,verbose=0)
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        self.vgpTree.SetDoc(self.netPlugInSrv,True)
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.vgpTree,self.OnAddElementsFin)
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.vgpTree,self.OnAddElementsProgress)
        
        #self.nbData=vPlugInSrvListBook(self.slwTop,wx.NewId(),
        #        pos=(4,4),size=(470, 188),style=0,name="nbPlugInSrv")
        #self.nbData.SetConstraints(
        #    anchors.LayoutAnchors(self.nbData, True, True, True, True)
        #    )
        #self.nbData.SetDoc(self.netPlugInSrv,True)
        
        oPlugInSrv=self.netPlugInSrv.GetRegisteredNode('vPlugInSrv')
        pnCls=oPlugInSrv.GetPanelClass()
        pn=pnCls(self.viRegSel,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="pnPlugInSrv")
        pn.SetDoc(self.netPlugInSrv,True)
        self.viRegSel.AddWidgetDependent(pn,oPlugInSrv)
        pn.Show(False)
        
        oPlugIn=self.netPlugInSrv.GetRegisteredNode('vPlugInSrvDir')
        pnCls=oPlugIn.GetPanelClass()
        pn=pnCls(self.viRegSel,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="pnPlugIn")
        pn.SetDoc(self.netPlugInSrv,True)
        self.viRegSel.AddWidgetDependent(pn,oPlugIn)
        pn.Show(False)
        
        oPlugInSub=self.netPlugInSrv.GetRegisteredNode('vPlugInSrvDirSub')
        pnCls=oPlugInSub.GetPanelClass()
        pn=pnCls(self.viRegSel,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="pnPlugInSub")
        pn.SetDoc(self.netPlugInSrv,True)
        self.viRegSel.AddWidgetDependent(pn,oPlugInSub)
        pn.Show(False)
        
        oHosts=self.netPlugInSrv.GetRegisteredNode('Hosts')
        pnCls=oHosts.GetPanelClass()
        pn=pnCls(self.viRegSel,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="pnHosts")
        pn.SetDoc(self.netPlugInSrv,True)
        self.viRegSel.AddWidgetDependent(pn,oHosts)
        pn.Show(False)

        self.viRegSel.SetDoc(self.netPlugInSrv,True)
        self.viRegSel.SetRegNode(oPlugInSrv)
        self.viRegSel.SetRegNode(oPlugIn)
        self.viRegSel.SetRegNode(oPlugInSub)
        self.viRegSel.SetRegNode(oHosts)
        
        self.cbStart.SetBitmapLabel(images.getRunningBitmap())
        self.cbStop.SetBitmapLabel(images.getStoppedBitmap())
        
        self.netFileSrv=None
        
        self.iLogOldSum=0
        self.zLogUnChanged=0
        # setup statusbar
        self.timer = wx.PyTimer(self.Notify)
        self.timer.Start(1000)
        self.Notify()
        
        rect = self.sbStatus.GetFieldRect(1)
        self.gProcess = wx.Gauge(
                    self.sbStatus, -1, 50, (rect.x+2, rect.y+2), 
                    (rect.width-4, rect.height-4), 
                    wx.GA_HORIZONTAL|wx.GA_SMOOTH
                    )
        self.gProcess.SetRange(100)
        self.gProcess.SetValue(0)
        
        vMainFrame.AddMenus(self , self , self.vgpTree , self.netMaster,
                self.mnFile , self.mnView , self.mnTools)
        self.Layout()
        self.Fit()
        self.OpenCfgFile()
        self.__makeTitle__()
        #self.__startSrv__()
    def Notify(self):
        try:
            t = time.localtime(time.time())
            st = time.strftime(_("%b-%d-%Y   %H:%M:%S wk:%U"), t)
            self.SetStatusText(st, STATUS_CLK_POS)
            self.__makeTitle__()
            
            s=vtLog.vtLngGetNumDiffOrderedStr()
            self.SetStatusText(s, STATUS_LOG_POS)
            
            i=vtLog.vtLngGetNumDiffSum()
            if i>self.iLogOldSum:
                self.iLogOldSum=i
                self.zLogUnChanged=0
            else:
                self.zLogUnChanged+=1
                if self.zLogUnChanged>30:
                    self.iLogOldSum=i
                    self.zLogUnChanged=0
                    vtLog.vtLngNumStore()
        except:
            pass
        
    def __makeTitle__(self):
        sOldTitle=self.GetTitle()
        s=_("VIDARC PlugInSrv")
        fn=self.netPlugInSrv.GetFN()
        if fn is None:
            s=s+_(" (undef*)")
        else:
            s=s+" ("+fn
            if self.netMaster.IsModified():
                s=s+"*)"
            else:
                s=s+")"
        if s!=sOldTitle:
            self.SetTitle(s)
    def __setModified__(self,state):
        self.__makeTitle__()
    def OnAddElementsProgress(self,evt):
        if self.gProcess is not None:
            self.gProcess.SetValue(evt.GetValue())
            if evt.GetCount()>=0:
                self.gProcess.SetRange(evt.GetCount())
        evt.Skip()
    def OnAddElementsFin(self,evt):
        if self.gProcess is not None:
            self.gProcess.SetValue(0)
        self.PrintMsg(_('generated.'))
        evt.Skip()
    def OnTreeItemSel(self,event):
        node=event.GetTreeNodeData()
        self.viRegSel.SetNode(node)
        event.Skip()
    def OnOpenStart(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('file opened ...'))
        evt.Skip()
    def OnOpenOk(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('file opened and parsed.'))
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnOpenFault(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('file open fault.'))
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnSynchStart(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('synch started ...')+' %07d - %07d'%(0,evt.GetCount()))
        evt.Skip()
    def OnSynchProc(self,evt):
        #if VERBOSE:
        #    vtLog.vtLogCallDepth(self,'')
        self.gProcess.SetRange(evt.GetCount())
        self.gProcess.SetValue(evt.GetAct())
        self.PrintMsg(_('synch started ...')+' %07d - %07d'%(evt.GetAct(),evt.GetCount()))
        evt.Skip()
    def OnSynchFinished(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('synch finished.'))
        self.__getXmlBaseNodes__()
        self.gProcess.SetValue(0)
        evt.Skip()
    def OnSynchAborted(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('synch aborted.'))
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnConnect(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.__makeTitle__()
        self.PrintMsg(_('connection established ... please wait'))
        evt.Skip()
    def OnDisConnect(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('connection lost.'))
        evt.Skip()
    def OnLogin(self,evt):
        self.PrintMsg(_('login ... '))
        evt.Skip()
    def OnLoggedin(self,evt):
        self.PrintMsg(_('get content ... '))
        evt.Skip()
    def OnSetupChanged(self,evt):
        if self.netPlugInSrv.ReConnect2Srv()==0:
            #self.netHum.ReConnect2Srv()
            pass
    def OnGotContent(self,evt):
        self.PrintMsg(_('content got. generate ...'))
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnClosed(self,evt):
        self.PrintMsg(_('connection closed. '))
        evt.Skip()
    def OnConnect(self,evt):
        self.__makeTitle__()
        evt.Skip()
    def OnChanged(self,evt):
        self.__setModified__(True)
        evt.Skip()
    def OpenFile(self,fn):
        if self.netPlugInSrv.ClearAutoFN()>0:
            self.netPlugInSrv.Open(fn)
    def OpenCfgFile(self,fn=None):
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                self.netMaster.SetCfgNode()
                self.__makeTitle__()
                self.__setCfg__()
                return
        self.xdCfg.New(root='config')
        if fn is not None:
            self.xdCfg.SetFN(fn)
            self.netMaster.SetCfgNode()
        self.__setCfg__()
    def CreateNew(self):
        self.netPlugInSrv.New()
        
    def OnTreeItemAdd(self,event):
        #print "added",event.GetVgpTree(),event.GetTreeNode(),event.GetTreeNodeData()
        #vtXmlDomAlign(self.dom)
        self.__setModified__(True)
        event.Skip()
    def __getSettings__(self):
        return
    def __getXmlBaseNodes__(self):
        self.baseDoc=None
        self.baseSettings=None
        
        self.baseDoc=self.netPlugInSrv.getBaseNode()
        self.baseSettings=self.netPlugInSrv.getChildForced(self.netPlugInSrv.getRoot(),'settings')
        self.__getSettings__()
    def OnMnFileItem_exitMenu(self, event):
        self.Close()
        event.Skip()

    def OnMnFileItem_settingMenu(self, event):
        nodeSetting=self.netPlugInSrv.getChild(self.netPlugInSrv.getRoot(),'settings')
        if nodeSetting is not None:
            dlg=vPlugInSrvSettingsDialog(self)
            dlg.SetXml(self.netPlugInSrv,nodeSetting)
            ret=dlg.ShowModal()
            if ret>0:
                dlg.GetXml(self.netPlugInSrv,nodeSetting)
                self.__getSettings__()
            dlg.Destroy()
        event.Skip()

    def OnFrameClose(self, event):
        if self.netMaster.IsModified()==True:
            dlg = wx.MessageDialog(self,
                        _(u'Unsaved data present! Do you want to save data?'),
                        u'vPlugInSrv'+u' '+_(u'Close'),
                        wx.OK | wx.CANCEL | wx.ICON_QUESTION
                        #wx.YES_NO | wx.NO_DEFAULT | wx.CANCEL | wx.ICON_INFORMATION
                        )
            if dlg.ShowModal()==wx.ID_OK:
                self.netMaster.Save(bModified=True)
        self.netMaster.ShutDown()
        event.Skip()

    def OnFrameActivate(self, event):
        if self.IsShown():
            if self.bActivated==False:
                self.netMaster.ShowPopup()
                self.bActivated=True
        event.Skip()
    def __setCfg__(self):
        try:
            node=self.xdCfg.getChildForced(None,'size')
            iX=self.xdCfg.GetValue(node,'x',int,20)
            iY=self.xdCfg.GetValue(node,'y',int,20)
            iWidth=self.xdCfg.GetValue(node,'width',int,800)
            iHeight=self.xdCfg.GetValue(node,'height',int,600)
            iX,iY,iWidth,iHeight=vSystem.LimitWindowToScreen(iX,iY,iWidth,iHeight)
            self.Move((iX,iY))
            self.SetSize((iWidth,iHeight))
            
            node=self.xdCfg.getChildForced(None,'layout')
            
            i=self.xdCfg.GetValue(node,'nav_sash',int,200)
            self.slwNav.SetDefaultSize((i, 1000))
            
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            
            self.xdCfg.AlignDoc()
            self.xdCfg.Save()
            self.bBlockCfgEventHandling=False
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSlwNavSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            node=self.xdCfg.getChildForced(None,'layout')
            iWidth=event.GetDragRect().width
            #if iWidth>80:
            #    iWidth=80
            self.xdCfg.SetValue(node,'nav_sash',iWidth)
            self.xdCfg.Save()
            
            self.slwNav.SetDefaultSize((iWidth, 1000))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnFrameSize(self, event):
        event.Skip()
        try:
            bMod=False
            sz=event.GetSize()
            node=self.xdCfg.getChildForced(None,'size')
            self.xdCfg.SetValue(node,'width',sz[0])
            self.xdCfg.SetValue(node,'height',sz[1])
            self.xdCfg.Save()
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnFrameMove(self, event):
        event.Skip()
        try:
            pos=self.GetPosition()
            node=self.xdCfg.getChildForced(None,'size')
            self.xdCfg.SetValue(node,'x',pos[0])
            self.xdCfg.SetValue(node,'y',pos[1])
            self.xdCfg.Save()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbApplyButton(self, event):
        event.Skip()

    def OnCbCancelButton(self, event):
        event.Skip()

    def OnMnHelpMn_help_helpMenu(self, event):
        try:
            self.hlp=wx.html.HtmlHelpController()
            self.hlp.AddBook('vPlugInSrvHelpBook.hhp')
            self.hlp.DisplayContents()
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()

    def OnMnHelpMn_help_aboutMenu(self, event):
        dlg=vAboutDialog.create(self,images.getApplicationBitmap())
        dlg.Centre()
        dlg.ShowModal()
        dlg.Destroy()
        event.Skip()

    def OnMnHelpMn_help_logMenu(self, event):
        frm=vtLogFileViewerFrame(self)
        frm.Centre()
        sFN=vtLog.vtLngGetFN()
        if sFN is not None:
            frm.OpenFile(sFN)
        frm.Show()
        event.Skip()
    def SetCfgData(self,func,l,d):
        self.cfgFunc=func
        self.cfgOrigin=l
        pass
    def GetCfgData(self):
        d={}
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,d)

    def OnViRegSelToolXmlRegSelectorCancel(self, event):
        event.Skip()

    def OnViRegSelToolXmlRegSelectorApply(self, event):
        event.Skip()

    def OnCbStartButton(self, event):
        event.Skip()
        self.__startSrv__()
    def OnCbStopButton(self, event):
        event.Skip()
        self.__stopSrv__()
    def setDataConfig(self):
        pass
    def __getHier__(self,node,lst):
        o=self.netPlugInSrv.GetRegByNode(node)
        sTag=o.GetTag(node)
        lst.append(sTag)
        if o.GetTagName()!='PlugInSrvDir':
            self.__getHier__(self.netPlugInSrv.getParent(node),lst)
    def __getHosts__(self,node):
        if node is None:
            return None
        o=self.netPlugInSrv.GetRegByNode(node)
        if o.GetTagName()!='PlugInSrvDir':
            return self.__getHosts__(self.netPlugInSrv.getParent(node))
        else:
            nodeTmp=self.netPlugInSrv.getChild(node,'Hosts')
            oHosts=self.netPlugInSrv.GetRegByNode(nodeTmp)
            return oHosts.GetHosts(nodeTmp)
    def __procElem__(self,node,srvFile,lHier):
        o=self.netPlugInSrv.GetRegByNode(node)
        try:
            if o.GetTagName() in ['PlugInSrvDir','PlugInSrvDirSub']:
                sTag=self.netPlugInSrv.GetHierTag(node)
                sApplAlias=':'.join(['vPlugInFile',sTag])
                sDir=o.GetDir(node)
                srvFile.AddServedDN(sApplAlias,sDir)
                lHosts=self.__getHosts__(node)
                oAcl=o.GetAcl(node)
                srvFile.SetServedHosts(sApplAlias,lHosts,oAcl)
        except:
            vtLog.vtLngTB(self.GetName())
        return 0
    def __startSrv__(self):
        try:
            if self.netFileSrv is None:
                dataPort=50002
                servicePort=50003
                self.netFileSrv=vtNetSrv(vtSrvFileSock,dataPort,vtNetFileSock,
                                vtSrvSock,servicePort,vtServiceSock,2,1)
                self.netFileSrv.SetServicePasswd('VidNixDarc')
                self.netFileSrv.Serve()
                docHum=self.netPlugInSrv.GetNetDoc('vHum')
                srvFile=self.netFileSrv.GetDataInstance()
                srvFile.SetDocHum(docHum)
                
            srvFile=self.netFileSrv.GetDataInstance()
            srvFile.SetAppl('plugInSrv')
            node=self.netPlugInSrv.getBaseNode()
            l=[]
            self.netPlugInSrv.procChildsKeysRec(10,node,self.__procElem__,srvFile,l)
            #srvXml.SetSettings(self.xdCfg)
            #self.setDataConfig()
            self.cbStart.Enable(False)
            self.cbStop.Enable(True)
        except:
            vtLog.vtLngTB(self.GetName())
    def __stopSrv__(self):
        try:
            srvXml=self.netFileSrv.GetDataInstance()
            #self.netFileSrv.Stop()
            srvFile=self.netFileSrv.GetDataInstance()
            if srvFile is not None:
                srvFile.ClearServedDN()
            self.cbStart.Enable(True)
            self.cbStop.Enable(False)
        except:
            vtLog.vtLngTB(self.GetName())
        
