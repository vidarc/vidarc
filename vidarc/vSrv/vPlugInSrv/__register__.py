#----------------------------------------------------------------------------
# Name:         __register__.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060077
# CVS-ID:       $Id: __register__.py,v 1.2 2007/02/19 12:32:10 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.vSrv.vPlugInSrv.vXmlNodePlugInSrv import vXmlNodePlugInSrv
from vidarc.vSrv.vPlugInSrv.vXmlNodePlugInSrvDir import vXmlNodePlugInSrvDir
from vidarc.vSrv.vPlugInSrv.vXmlNodePlugInSrvDirSub import vXmlNodePlugInSrvDirSub
from vidarc.vSrv.net.vXmlNodeHosts import vXmlNodeHosts
from vidarc.vSrv.net.vsNetXmlNodeAliasRef import vsNetXmlNodeAliasRef

def RegisterNodes(self,oRoot,bRegAsRoot=True):
    try:
        oPlugInSrv=vXmlNodePlugInSrv()
        oPlugInSrvDir=vXmlNodePlugInSrvDir()
        oPlugInSrvSub=vXmlNodePlugInSrvDirSub()
        oHosts=vXmlNodeHosts()
        oRef=vsNetXmlNodeAliasRef()
        
        self.RegisterNode(oPlugInSrv,bRegAsRoot)
        self.RegisterNode(oPlugInSrvDir,False)
        self.RegisterNode(oHosts,False)
        self.RegisterNode(oPlugInSrvSub,False)
        self.RegisterNode(oRef,False)
        
        self.LinkRegisteredNode(oRoot,oPlugInSrv)
        self.LinkRegisteredNode(oPlugInSrv,oPlugInSrvDir)
        self.LinkRegisteredNode(oPlugInSrv,oRef)
        self.LinkRegisteredNode(oPlugInSrv,oHosts)
        self.LinkRegisteredNode(oPlugInSrvDir,oPlugInSrvSub)
        self.LinkRegisteredNode(oPlugInSrvDir,oHosts)
        self.LinkRegisteredNode(oPlugInSrvSub,oPlugInSrvSub)
        return oPlugInSrv
    except:
        vtLog.vtLngTB(__name__)
