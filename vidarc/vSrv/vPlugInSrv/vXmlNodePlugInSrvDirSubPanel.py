#Boa:FramePanel:vXmlNodePlugInSrvDirSubPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodePlugInSrvPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060627
# CVS-ID:       $Id: vXmlNodePlugInSrvDirSubPanel.py,v 1.5 2010/03/29 08:55:34 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputTextML
import vidarc.tool.input.vtInputText
import vidarc.tool.sec.vtSecAclFileTree
import wx.lib.filebrowsebutton

import sys,os,stat

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VXMLNODEPLUGINSRVDIRSUBPANEL, wxID_VXMLNODEPLUGINSRVDIRSUBPANELCHCDIR, 
 wxID_VXMLNODEPLUGINSRVDIRSUBPANELLBLACL, 
 wxID_VXMLNODEPLUGINSRVDIRSUBPANELLBLDIR, 
 wxID_VXMLNODEPLUGINSRVDIRSUBPANELLBLNAME, 
 wxID_VXMLNODEPLUGINSRVDIRSUBPANELVIACL, 
 wxID_VXMLNODEPLUGINSRVDIRSUBPANELVINAME, 
] = [wx.NewId() for _init_ctrls in range(7)]

class vXmlNodePlugInSrvDirSubPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_bxsName_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblName, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viName, 2, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsDir, 0, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsName, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblACL, 0, border=4,
              flag=wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP)
        parent.AddWindow(self.viACL, 0, border=4,
              flag=wx.EXPAND | wx.BOTTOM | wx.TOP | wx.RIGHT | wx.LEFT)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(3)
        parent.AddGrowableCol(0)

    def _init_coll_bxsDir_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblDir, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.chcDir, 2, border=4, flag=wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=0, rows=4, vgap=0)

        self.bxsName = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsDir = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsName_Items(self.bxsName)
        self._init_coll_bxsDir_Items(self.bxsDir)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEPLUGINSRVDIRSUBPANEL,
              name=u'vXmlNodePlugInSrvDirSubPanel', parent=prnt, pos=wx.Point(0,
              0), size=wx.Size(312, 303),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 276))
        self.SetAutoLayout(True)

        self.lblDir = wx.StaticText(id=wxID_VXMLNODEPLUGINSRVDIRSUBPANELLBLDIR,
              label=u'directory', name=u'lblDir', parent=self, pos=wx.Point(4,
              4), size=wx.Size(94, 21), style=wx.ALIGN_RIGHT)

        self.chcDir = wx.Choice(choices=[],
              id=wxID_VXMLNODEPLUGINSRVDIRSUBPANELCHCDIR, name=u'chcDir',
              parent=self, pos=wx.Point(102, 4), size=wx.Size(197, 21),
              style=0)

        self.lblName = wx.StaticText(id=wxID_VXMLNODEPLUGINSRVDIRSUBPANELLBLNAME,
              label=_(u'name'), name=u'lblName', parent=self, pos=wx.Point(4,
              29), size=wx.Size(94, 30), style=wx.ALIGN_RIGHT)

        self.viName = vidarc.tool.input.vtInputTextML.vtInputTextML(id=wxID_VXMLNODEPLUGINSRVDIRSUBPANELVINAME,
              name=u'viName', parent=self, pos=wx.Point(102, 29),
              size=wx.Size(197, 30), style=0)

        self.lblACL = wx.StaticText(id=wxID_VXMLNODEPLUGINSRVDIRSUBPANELLBLACL,
              label=_(u'ACL'), name=u'lblACL', parent=self, pos=wx.Point(4, 63),
              size=wx.Size(296, 13), style=0)

        self.viACL = vidarc.tool.sec.vtSecAclFileTree.vtSecAclFileTree(id=wxID_VXMLNODEPLUGINSRVDIRSUBPANELVIACL,
              name=u'viACL', parent=self, pos=wx.Point(4, 80), size=wx.Size(296,
              192), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vPlugInSrv')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.viName.SetTagName('name')
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def __Clear__(self):
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
        # add code here
        self.chcDir.Clear()
        self.viName.Clear()
        self.viACL.Clear()
    def SetNetDocs(self,d):
        self.__logCritical__('FIXME'%())
        return
        if d.has_key('vHum'):
            dd=d['vHum']
            self.docHum=dd['doc']
            self.viACL.SetDocHum(dd['doc'],dd['bNet'])
        # add code here
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viName.SetDoc(doc)
        self.viACL.SetDoc(doc,bNet)
        if dDocs is not None:
            if 'vHum' in dDocs:
                dd=dDocs['vHum']
                self.viACL.SetDocHum(dd['doc'],dd['bNet'])
    def __showDir__(self,sDir,sSub,lDirs):
        self.chcDir.Clear()
        lFiles=os.listdir(sDir)
        lFiles.sort()
        i=0
        iSel=0
        for f in lFiles:
            if f in lDirs:
                continue
            sFN=os.path.join(sDir,f)
            s=os.stat(sFN)
            if stat.S_ISDIR(s[stat.ST_MODE]):
                self.chcDir.Append(f)
                if f==sSub:
                    iSel=i
                i+=1
        try:
            self.chcDir.SetSelection(iSel)
        except:
            pass
    def __procElem__(self,node,lst):
        sTag=self.doc.getTagName(node)
        if sTag==self.objRegNode.GetTagName():
            sDir=self.objRegNode.GetTag(node)
            lst.append(sDir)
        return 0
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.__logInfo__('id:%s'%(self.doc.getKey(node)))
            self.viName.SetNode(self.node)
            nodePar=self.doc.getParent(self.node)
            oPar=self.doc.GetRegByNode(nodePar)
            sDirPar=oPar.GetDir(nodePar)
            sSub=self.objRegNode.GetTag(self.node)
            l=[]
            self.doc.procChildsKeysRec(0,nodePar,self.__procElem__,l)
            try:
                l.remove(sSub)
            except:
                pass
            self.__showDir__(sDirPar,sSub,l)
            # 090627:wro set human doc referenced
            nodeSrv=self.objRegNode.GetPlugInSrv(node)
            if nodeSrv is not None:
                oSrv=self.doc.GetRegByNode(nodeSrv)
            else:
                oSrv=None
            if oSrv is not None:
                docHum=oSrv.GetDocHumRef(nodeSrv)
                self.viACL.SetDocHum(docHum,False)
            else:
                self.viACL.SetDocHum(None,False)
            self.viACL.SetNode(self.node)
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            sSub=self.chcDir.GetStringSelection()
            self.objRegNode.SetTag(node,sSub)
            self.viName.GetNode(node)
            nodePar=self.doc.getParent(node)
            oPar=self.doc.GetRegByNode(nodePar)
            sDirPar=oPar.GetDir(nodePar)
            self.objRegNode.SetDir(node,'/'.join([sDirPar,sSub]))
            self.viACL.GetNode(node)
            l=[]
            self.doc.procChildsKeysRec(0,nodePar,self.__procElem__,l)
            try:
                l.remove(sSub)
            except:
                pass
            self.__showDir__(sDirPar,sSub,l)
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        pass
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        pass
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            # add code here
            pass
        else:
            # add code here
            pass
