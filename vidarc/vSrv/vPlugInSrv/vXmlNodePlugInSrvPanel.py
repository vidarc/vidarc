#----------------------------------------------------------------------------
# Name:         vXmlNodePlugInSrvPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060707
# CVS-ID:       $Id: vXmlNodePlugInSrvPanel.py,v 1.3 2010/03/29 08:55:34 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.vSrv.net.vsNetXmlNodeSrvPanel import vsNetXmlNodeSrvPanel

    from vidarc.tool.net.vtNetFileSrv import *
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vXmlNodePlugInSrvPanel(vsNetXmlNodeSrvPanel):
    def __Clear__(self):
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
        # add code here
        self.viTag.Clear()
        self.viName.Clear()
        self.vsSrv.Clear()
        self.intData.SetValue(50002)
        self.intService.SetValue(50003)
        self.vsSrv.Enable(False)
        self.ClrBlockDelayed()
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viTag.SetDoc(doc)
        self.viName.SetDoc(doc)
        self.vsSrv.SetDoc(doc,bNet)
        self.vsSrv.SetDataSrvClass(vtSrvFileSock)
        self.vsSrv.SetDataClass(vtNetFileSock)
    def OnVsSrvVsnetSrvcontrolStarted(self, event):
        event.Skip()
        try:
            self.__logInfo__(''%())
            netHum=self.doc.GetNetDoc('vHum')
            srv=self.vsSrv.GetNetSrv()
            if srv is None:
                self.__logError__('server is None')
                return
            srvFile=srv.GetDataInstance()
            if srvFile is None:
                self.__logError__('server data-instance is None')
                return
            srvFile.SetDocHum(netHum)
            #self.objRegNode.AddServerInstanceByNode(self.node,srv)
        except:
            self.__logTB__()
