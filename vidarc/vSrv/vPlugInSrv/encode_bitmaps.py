#!/usr/bin/env python
#----------------------------------------------------------------------
#----------------------------------------------------------------------------
# Name:         encode_bitmaps.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      2006
# CVS-ID:       $Id: encode_bitmaps.py,v 1.1 2006/07/17 10:41:32 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    "-u -i -n Application img/Vid_PlugInSrv_16.ico images.py",
    "-a -u -n Plugin img/Vid_PlugInSrv_16.png images.py",
    "-a -u -n NodePlugin img/VidMod_PlugInSrv_16.png images.py",
    "-a -u -n Running img/Running01_16.png images.py",
    "-a -u -n Stopped img/Stopped01_16.png images.py",
        
    "-u -i -n Splash img/splashPlugInSrv01.png images_splash.py",
    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)

