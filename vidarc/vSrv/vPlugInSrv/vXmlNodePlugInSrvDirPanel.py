#Boa:FramePanel:vXmlNodePlugInSrvDirPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodePlugInSrvPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060627
# CVS-ID:       $Id: vXmlNodePlugInSrvDirPanel.py,v 1.4 2010/03/29 08:55:34 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputTextML
import vidarc.tool.input.vtInputText
import vidarc.tool.sec.vtSecAclFileTree
import wx.lib.filebrowsebutton

import sys

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VXMLNODEPLUGINSRVDIRPANEL, wxID_VXMLNODEPLUGINSRVDIRPANELDBBDIR, 
 wxID_VXMLNODEPLUGINSRVDIRPANELLBLACL, wxID_VXMLNODEPLUGINSRVDIRPANELLBLNAME, 
 wxID_VXMLNODEPLUGINSRVDIRPANELLBLTAG, wxID_VXMLNODEPLUGINSRVDIRPANELVIACL, 
 wxID_VXMLNODEPLUGINSRVDIRPANELVINAME, wxID_VXMLNODEPLUGINSRVDIRPANELVITAG, 
] = [wx.NewId() for _init_ctrls in range(8)]

class vXmlNodePlugInSrvDirPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_bxsName_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblName, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viName, 2, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsTag, 0, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsName, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.dbbDir, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblACL, 0, border=4,
              flag=wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP)
        parent.AddWindow(self.viACL, 0, border=4,
              flag=wx.EXPAND | wx.BOTTOM | wx.TOP | wx.RIGHT | wx.LEFT)

    def _init_coll_bxsTag_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblTag, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viTag, 2, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(4)
        parent.AddGrowableCol(0)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=0, rows=5, vgap=0)

        self.bxsTag = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsName = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsTag_Items(self.bxsTag)
        self._init_coll_bxsName_Items(self.bxsName)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEPLUGINSRVDIRPANEL,
              name=u'vXmlNodePlugInSrvDirPanel', parent=prnt, pos=wx.Point(0,
              0), size=wx.Size(312, 303),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 276))
        self.SetAutoLayout(True)

        self.lblTag = wx.StaticText(id=wxID_VXMLNODEPLUGINSRVDIRPANELLBLTAG,
              label=_(u'tag'), name=u'lblTag', parent=self, pos=wx.Point(4, 4),
              size=wx.Size(94, 21), style=wx.ALIGN_RIGHT)

        self.viTag = vidarc.tool.input.vtInputText.vtInputText(id=wxID_VXMLNODEPLUGINSRVDIRPANELVITAG,
              name=u'viTag', parent=self, pos=wx.Point(102, 4),
              size=wx.Size(197, 21), style=0)

        self.viName = vidarc.tool.input.vtInputTextML.vtInputTextML(id=wxID_VXMLNODEPLUGINSRVDIRPANELVINAME,
              name=u'viName', parent=self, pos=wx.Point(102, 29),
              size=wx.Size(197, 30), style=0)

        self.lblName = wx.StaticText(id=wxID_VXMLNODEPLUGINSRVDIRPANELLBLNAME,
              label=_(u'name'), name=u'lblName', parent=self, pos=wx.Point(4,
              29), size=wx.Size(94, 30), style=wx.ALIGN_RIGHT)

        self.dbbDir = wx.lib.filebrowsebutton.DirBrowseButton(buttonText='...',
              dialogTitle=_(u'Choose a directory'),
              id=wxID_VXMLNODEPLUGINSRVDIRPANELDBBDIR,
              labelText=_(u'Select a directory:'), parent=self, pos=wx.Point(4,
              63), size=wx.Size(296, 29), startDirectory='.', style=0)

        self.lblACL = wx.StaticText(id=wxID_VXMLNODEPLUGINSRVDIRPANELLBLACL,
              label=_(u'ACL'), name=u'lblACL', parent=self, pos=wx.Point(4, 96),
              size=wx.Size(296, 13), style=0)

        self.viACL = vidarc.tool.sec.vtSecAclFileTree.vtSecAclFileTree(id=wxID_VXMLNODEPLUGINSRVDIRPANELVIACL,
              name=u'viACL', parent=self, pos=wx.Point(4, 113),
              size=wx.Size(296, 159), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vPlugInSrv')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.viTag.SetTagName('tag')
        self.viName.SetTagName('name')
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def __Clear__(self):
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
        # add code here
        self.viTag.Clear()
        self.viName.Clear()
        self.viACL.Clear()
    def SetNetDocs(self,d):
        self.__logCritical__('FIXME'%())
        return
        if d.has_key('vHum'):
            dd=d['vHum']
            self.docHum=dd['doc']
            self.viACL.SetDocHum(dd['doc'],dd['bNet'])
        # add code here
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viTag.SetDoc(doc)
        self.viName.SetDoc(doc)
        self.viACL.SetDoc(doc,bNet)
        if dDocs is not None:
            if 'vHum' in dDocs:
                dd=dDocs['vHum']
                self.viACL.SetDocHum(dd['doc'],dd['bNet'])
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viTag.SetNode(self.node)
            self.viName.SetNode(self.node)
            sDir=self.objRegNode.GetDir(self.node)
            self.dbbDir.SetValue(sDir)
            # 090627:wro set human doc referenced
            nodeSrv=self.objRegNode.GetPlugInSrv(node)
            if nodeSrv is not None:
                oSrv=self.doc.GetRegByNode(nodeSrv)
            else:
                oSrv=None
            if oSrv is not None:
                docHum=oSrv.GetDocHumRef(nodeSrv)
                self.viACL.SetDocHum(docHum,False)
            else:
                self.viACL.SetDocHum(None,False)
            self.viACL.SetNode(self.node)
            #oTrans=self.doc.GetRegisteredNode(self.objRegNode.GetTagNameTransition())
            
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viTag.GetNode(node)
            self.viName.GetNode(node)
            sDir=self.dbbDir.GetValue()
            sDir=sDir.replace('\\','/')
            self.objRegNode.SetDir(node,sDir)
            self.viACL.GetNode(node)
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            # add code here
            pass
        else:
            # add code here
            pass
