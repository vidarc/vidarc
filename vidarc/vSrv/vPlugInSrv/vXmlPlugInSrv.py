#----------------------------------------------------------------------------
# Name:         vXmlPlugInSrv.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060627
# CVS-ID:       $Id: vXmlPlugInSrv.py,v 1.2 2007/07/30 20:35:58 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vidarc.vSrv.vPlugInSrv.vXmlNodePlugInSrvRoot import vXmlNodePlugInSrvRoot

import vidarc.vSrv.vPlugInSrv.__register__ as __register__

class vXmlPlugInSrv(vtXmlDomReg):
    VERBOSE=1
    TAGNAME_REFERENCE='tag'
    TAGNAME_ROOT='PlugInSrvRoot'
    def __init__(self,appl='vPlugInSrv',attr='id',skip=[],synch=False,verbose=0,
                    audit_trail=True):
        self.dSrv={}
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        try:
            vtXmlDomReg.__init__(self,appl=appl,attr=attr,skip=skip,synch=synch,
                            verbose=verbose,audit_trail=audit_trail)
            oRoot=vXmlNodePlugInSrvRoot()
            self.RegisterNode(oRoot,False)
            
            __register__.RegisterNodes(self,oRoot,True)
        except:
            vtLog.vtLngTB(self.appl)
        self.SetDftLanguages()
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'vPlugIns')
    #def New(self,revision='1.0',root='PlugInSrvRoot',bConnection=False):
    #    vtXmlDomReg.New(self,revision,root)
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elem=self.createSubNode(self.getRoot(),'PlugIns')
        if bConnection==False:
            self.CreateReq()
        #self.createSubNodeTextAttr(elem,'cfg','','id','')
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        #self.AlignDoc()
    def __getHier__(self,node,lst):
        o=self.GetRegByNode(node)
        sTag=o.GetTag(node)
        lst.append(sTag)
        if o.GetTagName()!='PlugInSrvDir':
            self.__getHier__(self.getParent(node),lst)
    def GetHierTag(self,node):
        lTag=[]
        self.__getHier__(node,lTag)
        lTag.reverse()
        return '/'.join(lTag)
    def AddServerInstanceByNode(self,node,srv):
        id=self.getKey(node)
        self.AddServerInstance(id,srv)
    def AddServerInstance(self,id,srv):
        try:
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s;dSrv%s'%(id,vtLog.pformat(self.dSrv)),self.appl)
            iID=long(id)
            if iID in self.dSrv:
                vtLog.vtLngCurCls(vtLog.ERROR,'server for id:%08d already added'%(iID),self)
            self.dSrv[iID]=srv
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s;dSrv%s'%(id,vtLog.pformat(self.dSrv)),self.appl)
        except:
            vtLog.vtLngTB(self.doc.GetAppl())
    def HasServerInstanceByNode(self,node):
        id=self.getKey(node)
        return self.HasServerInstance(id)
    def HasServerInstance(self,id):
        try:
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s;dSrv%s'%(id,vtLog.pformat(self.dSrv)),self.appl)
            iID=long(id)
            return iID in self.dSrv
        except:
            vtLog.vtLngTB(self.doc.GetAppl())
        return False
    def GetServerInstanceByNode(self,node):
        id=self.getKey(node)
        return self.GetServerInstance(id)
    def GetServerInstance(self,id):
        try:
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s;dSrv%s'%(id,vtLog.pformat(self.dSrv)),self.appl)
            iID=long(id)
            if iID not in self.dSrv:
                return None
                #vtLog.vtLngCur(vtLog.DEBUG,'server for id:%08d not added'%(iID),self.appl)
            return self.dSrv[iID]
        except:
            vtLog.vtLngTB(self.doc.GetAppl())
        return None
    def DelServerInstanceByNode(self,node):
        id=self.getKey(node)
        self.DelServerInstance(id)
    def DelServerInstance(self,id):
        try:
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s;dSrv%s'%(id,vtLog.pformat(self.dSrv)),self.appl)
            iID=long(id)
            if iID in self.dSrv:
                del self.dSrv[iID]
        except:
            vtLog.vtLngTB(self.doc.GetAppl())
