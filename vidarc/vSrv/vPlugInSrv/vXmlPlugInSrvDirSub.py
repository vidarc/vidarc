#----------------------------------------------------------------------------
# Name:         vXmlPlugInSrv.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060627
# CVS-ID:       $Id: vXmlPlugInSrvDirSub.py,v 1.1 2006/07/17 10:41:32 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vidarc.vSrv.vPlugInSrv.vXmlNodePlugInSrvRoot import vXmlNodePlugInSrvRoot
from vidarc.vSrv.vPlugInSrv.vXmlNodePlugInSrv import vXmlNodePlugInSrv

from vidarc.tool.xml.vtXmlNodeLog import vtXmlNodeLog
from vidarc.tool.xml.vtXmlNodeCfg import vtXmlNodeCfg
from vidarc.tool.xml.vtXmlNodeAttrCfgML import vtXmlNodeAttrCfgML

class vXmlPlugInSrv(vtXmlDomReg):
    VERBOSE=1
    TAGNAME_REFERENCE='tag'
    def __init__(self,appl='vPlugInSrv',attr='id',skip=[],synch=False,verbose=0):
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        try:
            vtXmlDomReg.__init__(self,appl=appl,attr=attr,skip=skip,synch=synch,verbose=verbose)
            oRoot=vXmlNodePlugInSrvRoot()
            self.RegisterNode(oRoot,False)
            oPlugInSrv=vXmlNodePlugInSrv()
            
            oLog=vtXmlNodeLog()
            oCfg=vtXmlNodeCfg()
            self.RegisterNode(oPlugInSrv,True)
            self.RegisterNode(oLog,False)
            self.RegisterNode(oCfg,True)
            self.RegisterNode(vtXmlNodeAttrCfgML(),False)
            
            self.LinkRegisteredNode(oRoot,oPlugInSrv)
            self.LinkRegisteredNode(oPlugInSrv,oPlugInSrv)
            #self.LinkRegisteredNode(oPlugInSrv,oLog)
        except:
            vtLog.vtLngTB(self.appl)
        self.SetDftLanguages()
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'PlugInSrvRoot')
    def New(self,revision='1.0',root='PlugInSrvRoot',bConnection=False):
        vtXmlDomReg.New(self,revision,root)
        elem=self.createSubNode(self.getRoot(),'PlugInSrv')
        self.createSubNodeTextAttr(elem,'cfg','','id','')
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        self.AlignDoc()
