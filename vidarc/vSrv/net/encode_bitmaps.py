#!/usr/bin/env python
#----------------------------------------------------------------------
#----------------------------------------------------------------------------
# Name:         encode_bitmaps.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060628
# CVS-ID:       $Id: encode_bitmaps.py,v 1.1 2006/07/17 10:38:07 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    "-u -i -n Host img/Client03_16.png images.py",
    
    "-u -i -n Hum           img/VidMod_Hum_16.png           img_appl.py",
    "-a -u -n Bill          img/VidMod_Billing_16.png       img_appl.py",
    "-a -u -n Contact       img/VidMod_Contact_16.png       img_appl.py",
    "-a -u -n Customer      img/VidMod_Customer_16.png      img_appl.py",
    "-a -u -n Loc           img/VidMod_Loc_16.png           img_appl.py",
    "-a -u -n Doc           img/VidMod_Doc_16.png           img_appl.py",
    "-a -u -n Engineering   img/VidMod_Engineering_16.png   img_appl.py",
    "-a -u -n Globals       img/VidMod_Globals_16.png       img_appl.py",
    "-a -u -n Msg           img/VidMod_Msg_16.png           img_appl.py",
    "-a -u -n Task          img/VidMod_Task_16.png          img_appl.py",
    "-a -u -n Prj           img/VidMod_Prj_16.png           img_appl.py",
    "-a -u -n PrjDoc        img/VidMod_PrjDoc_16.png        img_appl.py",
    "-a -u -n PrjEng        img/VidMod_PrjEng_16.png        img_appl.py",
    "-a -u -n PrjPlan       img/VidMod_PrjPlan_16.png       img_appl.py",
    "-a -u -n PrjRes        img/VidMod_PrjRes_16.png        img_appl.py",
    "-a -u -n PrjTimer      img/VidMod_PrjTimer_16.png      img_appl.py",
    "-a -u -n Purchase      img/VidMod_Purchase_16.png      img_appl.py",
    "-a -u -n Ressource     img/VidMod_Ressource_16.png     img_appl.py",
    "-a -u -n Software      img/VidMod_Software_16.png      img_appl.py",
    "-a -u -n Supplier      img/VidMod_Supplier_16.png      img_appl.py",
    #"-a -u -n Plugin img/VidMod_XXX_16.png images.py",
        
    #"-u -i -n Splash img/splashXXX01.png images_splash.py",
    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)

