#Boa:FramePanel:vsNetXmlLoginCacheNodeAliasPanel
#----------------------------------------------------------------------------
# Name:         vsNetXmlLoginCacheNodeAliasPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20070708
# CVS-ID:       $Id: vsNetXmlLoginCacheNodeAliasPanel.py,v 1.2 2010/03/10 22:41:57 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug("import;start",__name__)
try:
    import wx
    
    import sys
    
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel
    
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,"__VERBOSE__")
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug("import;done",__name__)

[wxID_VSNETXMLLOGINCACHENODEALIASPANEL] = [wx.NewId() for _init_ctrls in range(1)]

class vsNetXmlLoginCacheNodeAliasPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(3)
        parent.AddGrowableCol(0)
        parent.AddGrowableCol(1)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=2, hgap=0, rows=4, vgap=0)

        self._init_coll_fgsLog_Growables(self.fgsLog)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=id,
              name=u'vsNetXmlLoginCacheNodeAliasPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(312, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 180))
        self.SetAutoLayout(True)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vSrvNet')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def __Clear__(self):
        if VERBOSE>0:
            self.__logDebug__(""%())
        # add code here
        
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE:
            self.__logDebug__(''%())
        # add code here
        
    def __SetNode__(self,node):
        try:
            if VERBOSE:
                self.__logDebug__(''%())
            # add code here
            
            #oTrans=self.doc.GetRegisteredNode(self.objRegNode.GetTagNameTransition())
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE:
                self.__logDebug__(''%())
            # add code here
            
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Lock__(self,flag):
        if VERBOSE:
            self.__logDebug__(''%())
        if flag:
            # add code here
            pass
        else:
            # add code here
            pass
