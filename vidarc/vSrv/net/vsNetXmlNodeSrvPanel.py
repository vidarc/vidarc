#Boa:FramePanel:vsNetXmlNodeSrvPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeMsgSrvPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060703
# CVS-ID:       $Id: vsNetXmlNodeSrvPanel.py,v 1.9 2010/03/10 22:41:57 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import wx
    import wx.lib.buttons
    import vidarc.vSrv.net.vsNetSrvControl
    import vidarc.tool.input.vtInputText
    import wx.lib.intctrl
    import vidarc.tool.input.vtInputTextML

    import sys

    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase

    from vidarc.tool.net.vtNetXml import *
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VSNETXMLNODESRVPANEL, wxID_VSNETXMLNODESRVPANELCBLOGINPOSS, 
 wxID_VSNETXMLNODESRVPANELINTDATA, wxID_VSNETXMLNODESRVPANELINTSERVICE, 
 wxID_VSNETXMLNODESRVPANELLBLLIC, wxID_VSNETXMLNODESRVPANELLBLNAME, 
 wxID_VSNETXMLNODESRVPANELLBLPORTDATA, 
 wxID_VSNETXMLNODESRVPANELLBLPORTSERVICE, wxID_VSNETXMLNODESRVPANELLBLTAG, 
 wxID_VSNETXMLNODESRVPANELTXTLICAVAIL, wxID_VSNETXMLNODESRVPANELTXTLICUSED, 
 wxID_VSNETXMLNODESRVPANELVINAME, wxID_VSNETXMLNODESRVPANELVITAG, 
 wxID_VSNETXMLNODESRVPANELVSSRV, 
] = [wx.NewId() for _init_ctrls in range(14)]

class vsNetXmlNodeSrvPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_bxsName_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblName, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viName, 2, border=4, flag=wx.EXPAND)

    def _init_coll_bxsLic_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblLic, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.txtLicUsed, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtLicAvail, 1, border=4,
              flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbLoginPos, 0, border=0, flag=0)
        parent.AddWindow(self.vsSrv, 0, border=4, flag=wx.LEFT)

    def _init_coll_bxsPortData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPortData, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.intData, 2, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsTag, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsName, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsPortData, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsPortService, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsLic, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)

    def _init_coll_bxsPortService_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPortService, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.intService, 2, border=0, flag=wx.EXPAND)

    def _init_coll_bxsTag_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblTag, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.viTag, 2, border=4, flag=wx.EXPAND)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(7)
        parent.AddGrowableCol(0)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=0, rows=7, vgap=0)

        self.bxsName = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsPortData = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsPortService = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsTag = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsLic = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsName_Items(self.bxsName)
        self._init_coll_bxsPortData_Items(self.bxsPortData)
        self._init_coll_bxsPortService_Items(self.bxsPortService)
        self._init_coll_bxsTag_Items(self.bxsTag)
        self._init_coll_bxsLic_Items(self.bxsLic)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VSNETXMLNODESRVPANEL,
              name=u'vsNetXmlNodeSrvPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(293, 279),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(285, 252))
        self.SetAutoLayout(True)

        self.lblName = wx.StaticText(id=wxID_VSNETXMLNODESRVPANELLBLNAME,
              label=_(u'name'), name=u'lblName', parent=self, pos=wx.Point(4,
              29), size=wx.Size(88, 30), style=wx.ALIGN_RIGHT)
        self.lblName.SetMinSize(wx.Size(-1, -1))

        self.lblPortData = wx.StaticText(id=wxID_VSNETXMLNODESRVPANELLBLPORTDATA,
              label=_(u'data port'), name=u'lblPortData', parent=self,
              pos=wx.Point(4, 63), size=wx.Size(88, 21), style=wx.ALIGN_RIGHT)
        self.lblPortData.SetMinSize(wx.Size(-1, -1))

        self.lblPortService = wx.StaticText(id=wxID_VSNETXMLNODESRVPANELLBLPORTSERVICE,
              label=_(u'serive port'), name=u'lblPortService', parent=self,
              pos=wx.Point(4, 88), size=wx.Size(88, 21), style=wx.ALIGN_RIGHT)
        self.lblPortService.SetMinSize(wx.Size(-1, -1))

        self.lblTag = wx.StaticText(id=wxID_VSNETXMLNODESRVPANELLBLTAG,
              label=_(u'alias'), name=u'lblTag', parent=self, pos=wx.Point(4,
              4), size=wx.Size(88, 21), style=wx.ALIGN_RIGHT)
        self.lblTag.SetMinSize(wx.Size(-1, -1))

        self.viName = vidarc.tool.input.vtInputTextML.vtInputTextML(id=wxID_VSNETXMLNODESRVPANELVINAME,
              name=u'viName', parent=self, pos=wx.Point(96, 29),
              size=wx.Size(184, 30), style=0)

        self.intData = wx.lib.intctrl.IntCtrl(allow_long=False,
              allow_none=False, default_color=wx.BLACK,
              id=wxID_VSNETXMLNODESRVPANELINTDATA, limited=True, max=65535,
              min=512, name=u'intData', oob_color=wx.RED, parent=self,
              pos=wx.Point(96, 63), size=wx.Size(184, 21), style=0,
              value=50004)
        self.intData.SetMinSize(wx.Size(-1, -1))

        self.intService = wx.lib.intctrl.IntCtrl(allow_long=False,
              allow_none=False, default_color=wx.BLACK,
              id=wxID_VSNETXMLNODESRVPANELINTSERVICE, limited=True, max=65535,
              min=512, name=u'intService', oob_color=wx.RED, parent=self,
              pos=wx.Point(96, 88), size=wx.Size(184, 21), style=0,
              value=50005)
        self.intService.SetMinSize(wx.Size(-1, -1))

        self.viTag = vidarc.tool.input.vtInputText.vtInputText(id=wxID_VSNETXMLNODESRVPANELVITAG,
              name=u'viTag', parent=self, pos=wx.Point(96, 4), size=wx.Size(184,
              21), style=0)

        self.vsSrv = vidarc.vSrv.net.vsNetSrvControl.vsNetSrvControl(id=wxID_VSNETXMLNODESRVPANELVSSRV,
              name=u'vsSrv', parent=self, pos=wx.Point(105, 121),
              size=wx.Size(58, 30), style=0)
        self.vsSrv.Bind(vidarc.vSrv.net.vsNetSrvControl.vEVT_VSNET_SRVCONTROL_STOPPED,
              self.OnVsSrvVsnetSrvcontrolStopped,
              id=wxID_VSNETXMLNODESRVPANELVSSRV)
        self.vsSrv.Bind(vidarc.vSrv.net.vsNetSrvControl.vEVT_VSNET_SRVCONTROL_STARTED,
              self.OnVsSrvVsnetSrvcontrolStarted,
              id=wxID_VSNETXMLNODESRVPANELVSSRV)

        self.lblLic = wx.StaticText(id=wxID_VSNETXMLNODESRVPANELLBLLIC,
              label=_(u'licenses'), name=u'lblLic', parent=self, pos=wx.Point(4,
              155), size=wx.Size(88, 21), style=wx.ALIGN_RIGHT)
        self.lblLic.SetMinSize(wx.Size(-1, -1))

        self.txtLicUsed = wx.TextCtrl(id=wxID_VSNETXMLNODESRVPANELTXTLICUSED,
              name=u'txtLicUsed', parent=self, pos=wx.Point(96, 155),
              size=wx.Size(88, 21), style=wx.TE_READONLY, value=u'')
        self.txtLicUsed.SetMinSize(wx.Size(-1, -1))
        self.txtLicUsed.Enable(False)

        self.txtLicAvail = wx.TextCtrl(id=wxID_VSNETXMLNODESRVPANELTXTLICAVAIL,
              name=u'txtLicAvail', parent=self, pos=wx.Point(192, 155),
              size=wx.Size(88, 21), style=wx.TE_READONLY, value=u'')
        self.txtLicAvail.SetMinSize(wx.Size(-1, -1))
        self.txtLicAvail.Enable(False)

        self.cbLoginPos = wx.lib.buttons.GenBitmapTextButton(id=wxID_VSNETXMLNODESRVPANELCBLOGINPOSS,
              bitmap=vtArt.getBitmap(vtArt.Build), label=_(u'login possible'),
              name=u'cbLoginPos', parent=self, pos=wx.Point(4, 121),
              size=wx.Size(97, 30), style=0)
        self.cbLoginPos.Bind(wx.EVT_BUTTON, self.OnCbLoginPosButton,
              id=wxID_VSNETXMLNODESRVPANELCBLOGINPOSS)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vSrvNet')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.viTag.SetTagName('tag')
        self.viName.SetTagName('name')
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def __Clear__(self):
        if VERBOSE:
            self.__logDebug__(''%())
        # add code here
        self.viTag.Clear()
        self.viName.Clear()
        self.vsSrv.Clear()
        self.intData.SetValue(50004)
        self.intService.SetValue(50005)
        self.vsSrv.Enable(False)
        #self.ClrBlockDelayed()
    def SetRegNode(self,obj):
        vtXmlNodePanel.SetRegNode(self,obj)
        self.vsSrv.SetRegNode(obj)
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viTag.SetDoc(doc)
        self.viName.SetDoc(doc)
        self.vsSrv.SetDoc(doc,bNet)
        self.vsSrv.SetDataSrvClass(vtSrvXmlSock)
        self.vsSrv.SetDataClass(vtNetXmlSock)
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viTag.SetNode(self.node)
            self.viName.SetNode(self.node)
            self.vsSrv.SetNode(self.node)
            self.vsSrv.SetNetSrv(self.doc.GetServerInstanceByNode(self.node))
                
            self.intData.SetValue(self.objRegNode.GetPortData(node))
            self.intService.SetValue(self.objRegNode.GetPortService(node))
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viTag.GetNode(node)
            self.viName.GetNode(node)
            self.objRegNode.SetPortData(node,self.intData.GetValue())
            self.objRegNode.SetPortService(node,self.intService.GetValue())
        except:
            self.__logTB__()
    def __Close__(self):
        # add code here
        self.viName.Close()
        self.vsSrv.Close()
    def __Cancel__(self):
        # add code here
        pass
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            # add code here
            self.viTag.Enable(False)
            self.viName.Enable(False)
            self.intData.Enable(False)
            self.intService.Enable(False)
            self.cbLoginPos.Enable(False)
        else:
            # add code here
            self.viTag.Enable(True)
            self.viName.Enable(True)
            self.intData.Enable(True)
            self.intService.Enable(True)
            self.cbLoginPos.Enable(True)

    def OnVsSrvVsnetSrvcontrolStopped(self, event):
        event.Skip()
        try:
            self.__logInfo__(''%())
            #self.doc.StopServer(self,node)
            #self.doc.DelServerInstanceByNode(self.node)
            self.__logPrintMsg__(_(u'%s:%s stopped')%(self.objRegNode.GetDescription(),
                    self.objRegNode.GetTag(self.node)))
        except:
            self.__logTB__()

    def OnVsSrvVsnetSrvcontrolStarted(self, event):
        event.Skip()
        try:
            self.__logInfo__(''%())
            netHum=self.doc.GetNetDoc('vHum')
            srv=self.vsSrv.GetNetSrv()
            if srv is None:
                self.__logError__('server is None')
                return
            srvXml=srv.GetDataInstance()
            if srvXml is None:
                self.__logError__('server data-instance is None')
                return
            srvXml.SetHumDoc4SecAcl(netHum)
            #vtLog.PrintMsg(_(u'server:%s started')%(self.GetTag(self.node)),self)
            self.__logPrintMsg__(_(u'%s:%s started')%(self.objRegNode.GetDescription(),
                    self.objRegNode.GetTag(self.node)))
            #self.objRegNode.AddServerInstanceByNode(self.node,srv)
        except:
            self.__logTB__()

    def OnCbLoginPosButton(self, event):
        event.Skip()
        #srvInst=self.doc.GetServerInstanceByNode(self.node)
        self.objRegNode.BuildLoginPossibleServer(self.node)
        self.doc.OpenLogin()
        self.objRegNode.SetLoginPossibleServer(self.node)
