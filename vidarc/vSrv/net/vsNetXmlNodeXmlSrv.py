#----------------------------------------------------------------------------
# Name:         vsNetXmlNodeXmlSrv.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060703
# CVS-ID:       $Id: vsNetXmlNodeXmlSrv.py,v 1.13 2009/01/24 22:04:31 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.vSrv.net.vsNetXmlNodeSrv import vsNetXmlNodeSrv
from vidarc.vSrv.net.vsNetXmlLoginCache import vsNetXmlLoginCache
from vidarc.tool.sec.vtSecXmlAclList import vtSecXmlAclList
from vidarc.tool.sec.vtSecXmlAclEntry import vtSecXmlAclEntry
import vidarc.tool.log.vtLog as vtLog

class vsNetXmlNodeXmlSrv(vsNetXmlNodeSrv):
    def GetPortData(self,node):
        return self.GetVal(node,'portData',int,50000)
    def GetPortService(self,node):
        return self.GetVal(node,'portService',int,50001)
    def __procAliases__(self,node,dataInst,appl,bStart,bLog=True):
        oReg=self.doc.GetRegByNode(node)
        if oReg is None:
            vtLog.vtLngCurCls(vtLog.ERROR,'tagname:%s not registered'%(self.doc.getTagName(node)),self)
            return 0
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCurCls(vtLog.INFO,'appl:%s;start:%s;id:%s;reg:%s'%
                    (appl,bStart,self.doc.getKey(node),oReg.GetTagName()),self)
        if hasattr(oReg,'GetDoc2Serve'):
            if hasattr(oReg,'HasChilds2Serve'):
                self.doc.procChildsKeys(node,self.__procAliases__,dataInst,oReg.GetTag(node),bStart)
                return 0
            alias=oReg.GetTag(node)
            applAlias=':'.join([appl,alias])
            if bStart:
                if bLog:
                    vtLog.PrintMsg(' '.join([self.GetDescription(),_('start'),applAlias]))
                doc=oReg.GetDoc2Serve(node,appl=applAlias)
                fn=oReg.GetFN(node)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurCls(vtLog.DEBUG,'fn:%s;doc:%s'%(fn,doc),self)
                hosts=oReg.GetHosts(node)
                dataInst.AddServedXml(applAlias,fn,doc,[],id=self.doc.getKeyNum(node))
                dataInst.SetServedHosts(applAlias,hosts)
            else:
                if dataInst.IsServedXml(applAlias):
                    vtLog.PrintMsg(' '.join([self.GetDescription(),_('save'),applAlias]))
                    #dataInst.SaveXml(applAlias)
                    vtLog.PrintMsg(' '.join([self.GetDescription(),_('stop'),applAlias]))
                    dataInst.DelServedXml(applAlias)
        return 0
    def Start(self,node,dataInst):
        appl=self.GetTag(node)
        vtLog.vtLngCurCls(vtLog.INFO,'id:%s;appl:%s'%(self.doc.getKey(node),appl),self)
        dataInst.SetPar(self)
        try:
            if hasattr(self.doc,'GetLoginCache'):
                par=self.doc.getParent(node)
                oReg=self.doc.GetRegByNode(par)
                applMain=oReg.GetTag(par)
                self.SetLoginCache(dataInst,applMain,appl)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        self.doc.procChildsKeys(node,self.__procAliases__,dataInst,appl,True,True)
    def Stop(self,node,dataInst):
        appl=self.GetTag(node)
        vtLog.vtLngCurCls(vtLog.INFO,'id:%s;appl:%s'%(self.doc.getKey(node),appl),self)
        self.doc.procChildsKeys(node,self.__procAliases__,dataInst,appl,False,True)
    def SetLoginCache(self,dataInst,sSrvMain,sSrvSub):
        vtLog.vtLngCurCls(vtLog.INFO,'srvMain:%s;srvSub:%s'%(sSrvMain,sSrvSub),self)
        try:
            dLogin=self.doc.GetLoginCache(sSrvMain,sSrvSub)
            if dLogin is None:
                vtLog.vtLngCurCls(vtLog.WARN,'mainServer:%s server:%s'%(sSrvMain,sSrvSub),self)
            else:
                dataInst.acquire()
                try:
                    if hasattr(dataInst,'SetLoginCache'):
                        dataInst.SetLoginCache(dLogin)
                except:
                    vtLog.vtLngTB(self.__class__.__name__)
                dataInst.release()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def __procLoginPossible__(self,node,dataInst,applMain):
        oReg=self.doc.GetRegByNode(node)
        if oReg is None:
            vtLog.vtLngCurCls(vtLog.ERROR,'tagname:%s not registered'%(self.doc.getTagName(node)),self)
            return 0
        if hasattr(oReg,'GetDoc2Serve'):
            pass
        else:
            if hasattr(self.doc,'HasServerInstanceByNode'):
                if self.doc.HasServerInstanceByNode(node):
                    netSrv=self.doc.GetServerInstanceByNode(node)
                    if netSrv is not None:
                        dataInstSub=netSrv.GetDataInstance()
                        appl=self.GetTag(node)
                        self.SetLoginCache(dataInstSub,applMain,appl)
        return 0
    def SetLoginPossible(self,node,dataInst):
        appl=self.GetTag(node)
        vtLog.vtLngCurCls(vtLog.INFO,'id:%s;appl:%s'%(self.doc.getKey(node),appl),self)
        try:
            if hasattr(self.doc,'GetLoginCache'):
                par=self.doc.getParent(node)
                oReg=self.doc.GetRegByNode(par)
                applMain=oReg.GetTag(par)
                self.SetLoginCache(dataInst,applMain,appl)
                self.doc.procChildsKeys(node,self.__procLoginPossible__,dataInst,appl)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def __createLoginCacheAlias(self,oReg,node,appl,id):
        oReg.SetTag(node,appl)
        oReg.SetAliasKeyNum(node,id)
    def __checkLoginCacheAliasKeyNum(self,node,docLogin,id):
        if docLogin.getTagName(node)!='LoginCacheAlias':
            return 0
        oReg=docLogin.GetRegByNode(node)
        if oReg.GetTagName()=='LoginCacheAlias':
            if oReg.GetAliasKeyNum(node)==id:
                return 1
        return 0
    def __createLoginCacheUsr(self,oReg,node,sLogin,id,sAlias):
        #vtLog.CallStack('')
        #print id,sAlias
        oReg.SetTag(node,sLogin)
        oReg.SetHumAlias(node,sAlias)
        oReg.SetHumKeyNum(node,id)
        #print node
    def __checkLoginCacheUsrKeyNum(self,node,docLogin,id,alias):
        if docLogin.getTagName(node)!='LoginCacheUsr':
            return 0
        oReg=docLogin.GetRegByNode(node)
        if oReg.GetHumKeyNum(node)==id:
            if oReg.GetHumAlias(node)==alias:
                #print oReg.GetHumKeyNum(node),id
                #print 'found'
                return 1
        #print 'not found'
        return 0
    def __procBuildLogin__(self,node,dataInst,appl,nodeSrv,docLogin,nodeLogin,dProcIds):
        oReg=self.doc.GetRegByNode(node)
        iID=self.doc.getKeyNum(node)
        if oReg is None:
            vtLog.vtLngCurCls(vtLog.ERROR,'tagname:%s not registered'%(self.doc.getTagName(node)),self)
            dProcIds[iID]=-1
            return 0
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCurCls(vtLog.INFO,'appl:%s;id:%s;reg:%s'%
                    (appl,self.doc.getKey(node),oReg.GetTagName()),self)
        try:
            if hasattr(oReg,'GetDoc2Serve'):
                dProcIds[iID]=0
                alias=oReg.GetTag(node)
                fn=oReg.GetFN(node)
                tup=docLogin.findChildsKeys(nodeLogin,None,
                            self.__checkLoginCacheAliasKeyNum,docLogin,iID)
                nodeTmp=None
                if tup[0]>0:
                    nodeTmp=tup[2]
                    nodeAlias=nodeTmp
                if nodeTmp is None:
                    oRegAlias,nodeTmp=docLogin.CreateNode(nodeLogin,'LoginCacheAlias',
                                self.__createLoginCacheAlias,alias,iID)
                    nodeAlias=nodeTmp
                    dProcIds[iID]=1
                else:
                    oRegAlias=docLogin.GetRegByNode(nodeAlias)
                    oRegAlias.SetTag(nodeAlias,alias)
                    dProcIds[iID]=0
                if hasattr(oReg,'HasChilds2Serve'):
                    #nodeTmp=docLogin.getChildForced(nodeLogin,alias)
                    self.doc.procChildsKeys(node,self.__procBuildLogin__,dataInst,alias,nodeSrv,
                            docLogin,nodeAlias,dProcIds)
                    return 0
                applAlias=':'.join([appl,alias])
                #nodeAlias=docLogin.getChildForced(nodeLogin,alias)
                if hasattr(dataInst,'IsServedXml'):
                    if dataInst.IsServedXml(applAlias):
                        dUsed={}
                        docTmp=dataInst.GetServedXmlDom(applAlias)
                        tup=docTmp.GetPossibleAcl()
                        dAcl,lst=tup
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCurCls(vtLog.DEBUG,'appl:%s;dAcl:%s'%(applAlias,vtLog.pformat(dAcl)),self)
                        nodeSec=docTmp.getChild(docTmp.getRoot(),'security_acl')
                        #nodeAcl=docTmp.getChild(docTmp.getRoot(),'acl')
                        docTmp.GetLoginPossible(dataInst=dataInst,dUsed=dUsed)
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCurCls(vtLog.DEBUG,'appl:%s;dUsed:%s'%(applAlias,vtLog.pformat(dUsed)),self)
                        
                        lNodes=docLogin.getChilds(nodeAlias,'LoginCacheUsr')
                        dNodes={}
                        for n in lNodes:
                            #dNodes[self.doc.getKeyNum(n)]=n
                            fid,appl=docLogin.getForeignKeyNumAppl(n,appl='vHum')
                            if fid>=0:
                                dNodes[fid]=n
                            else:
                                if None in dNodes:
                                    l=dNodes[None]
                                else:
                                    dNodes[None]=[]
                                l.append(n)
                        if None in dNodes:
                            lNodes=dNodes[None]
                            for n in lNodes:
                                docLogin.deleteNode(n,nodeAlias)
                        
                        dLogin={}
                        for sHumAlias,d in dUsed.items():
                            l=d.values()
                            for id,sLogin in d.items():
                                if sLogin not in dLogin:
                                    dLogin[sLogin]=(id,sHumAlias)
                        keys=dLogin.keys()
                        keys.sort()
                        for sLogin in keys:
                            #sLoginConv=sLogin.encode('ascii','replace').replace('?','_')
                            #print sLogin,
                            #nodeUsr=docLogin.getChildForced(nodeAlias,sLogin)
                            idHum,sHumAlias=dLogin[sLogin]
                            if idHum in dNodes:
                                nodeTmp=dNodes[idHum]
                                del dNodes[idHum]
                            else:
                                tup=docLogin.findChildsKeys(nodeAlias,None,
                                            self.__checkLoginCacheUsrKeyNum,docLogin,idHum,sHumAlias)
                                nodeTmp=None
                                #print tup
                                if tup[0]>0:
                                    nodeTmp=tup[2]
                                    #print nodeTmp
                            if nodeTmp is None:
                                oRegUsr,nodeTmp=docLogin.CreateNode(nodeAlias,'LoginCacheUsr',
                                            self.__createLoginCacheUsr,sLogin,idHum,sHumAlias)
                                #nodeAlias=nodeTmp
                            else:
                                oRegUsr=docLogin.GetRegByNode(nodeTmp)
                                oRegUsr.SetTag(nodeTmp,sLogin)
                                oRegUsr.SetHumAlias(nodeTmp,sHumAlias)
                            #nodeUsr=docLogin.getChildForced(nodeLogin,sLogin)
                            #nodeAppl=docLogin.getChildForced(nodeUsr,appl)
                            #nodeAlias=docLogin.getChildForced(nodeAppl,alias)
                        lNodes=dNodes.values()
                        for n in lNodes:
                            docLogin.deleteNode(n,nodeAlias)
                        #aclLst=vtSecXmlAclList(docTmp,nodeSec)
                        #lAct=aclLst.GetAclActive()
                        #for sAclName,dAclAct in lAct:
                        #    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        #        vtLog.vtLngCurCls(vtLog.DEBUG,'appl:%s;dAcl:%s'%(applAlias,vtLog.pformat(dAclAct)),self)
                        #    humAlias=':'.join(['vHum',sAclName])
                        #    docHum=dataInst.GetServedXmlDom(humAlias)
                        #    if docHum is not None:
                        #        kAcl=dAclAct.keys()
                        #        for kAcl,d in dAclAct.items():
                        #            dUsr=d['user']
                        #            for idUsr in dUsr.keys():
                        #                nodeUsr=docHum.getNodeByIdNum(idUsr)
                        #                if nodeUsr is not None:
                        #                    pass
                    else:
                        dProcIds[iID]=-2
                        #docLogin.deleteNode(nodeAlias,nodeLogin)
                else:
                    dProcIds[iID]=100
            elif hasattr(oReg,'BuildLoginPossible'):
                if hasattr(self.doc,'HasServerInstanceByNode'):
                    if self.doc.HasServerInstanceByNode(node):
                        netSrv=self.doc.GetServerInstanceByNode(node)
                        dataInstSub=netSrv.GetDataInstance()
                else:
                    dataInstSub=dataInst
                oReg.BuildLoginPossible(node,dataInstSub,docLogin=docLogin,nodeLogin=nodeLogin,dProcIds=dProcIds)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return 0
    def __createLoginCacheSrv(self,oReg,node,appl,id):
        oReg.SetTag(node,appl)
        oReg.SetServerKeyNum(node,id)
    def __checkLoginCacheServerKeyNum(self,node,docLogin,id):
        oReg=docLogin.GetRegByNode(node)
        if oReg is None:
            return 0
        if oReg.GetTagName()=='LoginCacheSrv':
            if oReg.GetServerKeyNum(node)==id:
                return 1
        return 0
    def __checkLoginCacheKeyNum(self,node,docLogin,id):
        oReg=docLogin.GetRegByNode(node)
        if oReg is None:
            return 0
        if oReg.GetTagName()=='LoginCacheSrv':
            if oReg.GetServerKeyNum(node)==id:
                return 1
        elif oReg.GetTagName()=='LoginCacheAlias':
            if oReg.GetAliasKeyNum(node)==id:
                return 1
        return 0
    def UpdateLoginPossible(self,node,dataInst,appl,nodeSrv,docLogin):
        sId=self.doc.getKey(node)
        if docLogin is None:
            vtLog.vtLngCurCls(vtLog.ERROR,'id:%s;docLogin is None'%(sId),self)
        nodeLogin=docLogin.getBaseNode()
        nodeLoginNew=None
        tup=docLogin.findChildsKeys(nodeLogin,[],
                self.__checkLoginCacheKeyNum,docLogin,self.doc.getKeyNum(node))
        if tup[0]>0:
            nodeLogin=tup[2]
            if nodeLogin is not None:
                nodeTmp=docLogin.getParent(nodeLogin)
                #docLogin.deleteNode(nodeLogin)
                nodeLogin=nodeTmp
                #oRegSrv,nodeLoginNew=docLogin.CreateNode(nodeTmp,'LoginCacheSrv',
                #        self.__createLoginCacheSrv,appl,self.doc.getKeyNum(node))
            else:
                nodePar=self.doc.getParent(node)
                oReg=self.doc.GetRegByNode(nodePar)
                if nodePar is not None:
                    self.UpdateLoginPossible(nodePar,dataInst,appl,nodeSrv,docLogin)
                    return
                vtLog.vtLngCurCls(vtLog.ERROR,'id:%s;parent node is None'%(sId),self)
                return 
            #tup=docLogin.findChildsKeys(nodeLogin,None,
            #        self.__checkLoginCacheServerKeyNum,docLogin,self.doc.getKeyNum(node))
            #oRegSrv,nodeLogin=docLogin.CreateNode(nodeTmp,'LoginCacheSrv',
            #        self.__createLoginCacheSrv,appl,self.doc.getKeyNum(node))
        else:
            nodePar=self.doc.getParent(node)
            oReg=self.doc.GetRegByNode(nodePar)
            if nodePar is not None:
                self.UpdateLoginPossible(nodePar,dataInst,appl,nodeSrv,docLogin)
            else:
                vtLog.vtLngCurCls(vtLog.ERROR,'id:%s;parent node is None'%(sId),self)
            return
        if nodeLogin is None:
            vtLog.vtLngCurCls(vtLog.ERROR,'id:%s;nodeLogin is None'%(sId),self)
        dProcIds={}
        iRet=self.__procBuildLogin__(node,dataInst,appl,nodeSrv,docLogin,nodeLogin,dProcIds)
        docLogin.AlignNode(nodeLogin,iRec=5)
    def BuildLoginPossible(self,node,dataInst,docLogin=None,nodeLogin=None,dProcIds=None):
        appl=self.GetTag(node)
        vtLog.vtLngCurCls(vtLog.INFO,'id:%s;appl:%s'%(self.doc.getKey(node),appl),self)
        #self.doc.acquire('dom')
        bBuild=False
        if docLogin is None:
            sFN=self.doc.GetFN()+'.loginCache.xml'
            docLogin=vsNetXmlLoginCache(appl='loginCache',audit_trail=False)
            if docLogin.Open(sFN)<0:
                docLogin.New()
            #nodeLogin=docLogin.getChildForced(docLogin.getBaseNode(),appl)
            #nodeLogin=docLogin.getBaseNode()
            #oReg,nodeLogin=docLogin.CreateNode(None,'LoginCacheSrv',
            #        self.__createLoginCacheSrv,appl,self.doc.getKeyNum(node))
            
            bBuild=True
        if nodeLogin is None:
            nodeLogin=docLogin.getBaseNode()
        tup=docLogin.findChildsKeys(nodeLogin,[],
                self.__checkLoginCacheServerKeyNum,docLogin,self.doc.getKeyNum(node))
        nodeTmp=None
        if tup[0]>0:
            nodeTmp=tup[2]
            nodeLogin=nodeTmp
        if nodeTmp is None:
            oRegSrv,nodeTmp=docLogin.CreateNode(nodeLogin,'LoginCacheSrv',
                    self.__createLoginCacheSrv,appl,self.doc.getKeyNum(node))
            nodeLogin=nodeTmp
            #nodeLogin=docLogin.getChildForced(nodeLogin,appl)
        else:
            oRegSrv=docLogin.GetRegByNode(nodeLogin)
            oRegSrv.SetTag(nodeLogin,appl)
        if dProcIds is None:
            bDelete=True
            dProcIds={}
        else:
            bDelete=False
        try:
            self.doc.procChildsKeys(node,self.__procBuildLogin__,dataInst,appl,node,
                    docLogin=docLogin,nodeLogin=nodeLogin,dProcIds=dProcIds)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurCls(vtLog.DEBUG,'id:%s;appl:%s;dProcIds:%s'%(self.doc.getKey(node),
                                appl,vtLog.pformat(dProcIds)),self)
            if bDelete:
                docLogin.DeleteDeadCacheByDict(nodeLogin,dProcIds)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        
        if bBuild:
            docLogin.AlignDoc()
            docLogin.Save(sFN)
            docLogin.Close()
            del docLogin
        #self.doc.release('dom')
    def __procSetup__(self,node,dataInst,appl,bLog=True):
        oReg=self.doc.GetRegByNode(node)
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCurCls(vtLog.INFO,'appl:%s;id:%s;reg:%s'%
                    (appl,self.doc.getKey(node),oReg.GetTagName()),self)
        if hasattr(oReg,'GetDoc2Serve'):
            if hasattr(oReg,'HasChilds2Serve'):
                self.doc.procChildsKeys(node,self.__procSetup__,dataInst,oReg.GetTag(node),bLog)
                return 0
            alias=oReg.GetTag(node)
            applAlias=':'.join([appl,alias])
            if bLog:
                vtLog.PrintMsg(' '.join([self.GetDescription(),_('setup'),applAlias]))
            if dataInst.IsServedXml(applAlias):
                oReg.Setup(node,dataInst,appl)
            #vtLog.vtLngCurCls(vtLog.INFO,'%s'%(applAlias),self)
        return 0
    def Setup(self,node,dataInst):
        appl=self.GetTag(node)
        vtLog.vtLngCurCls(vtLog.INFO,'id:%s;appl:%s'%(self.doc.getKey(node),appl),self)
        self.doc.procChildsKeys(node,self.__procSetup__,dataInst,appl,True)
    def NotifyClone(self,sAliasSrc,sAliasDst,sFN,id):
        vtLog.vtLngCurCls(vtLog.INFO,'src:%s;dst:%s;fn:%s;id:%08d'%(sAliasSrc,sAliasDst,sFN,id),self)
        if id>=0:
            node=self.doc.getNodeByIdNum(id)
            oReg=self.doc.GetRegByNode(node)
            tmp=self.doc.cloneNode(node,True)
            appl,alias=sAliasDst.split(':')
            oReg.SetTag(tmp,alias)
            oReg.SetFN(tmp,sFN)
            par=self.doc.getParent(node)
            self.doc.appendChild(par,tmp)
            self.doc.addNode(par,tmp)
            
            nodePar=node
            while nodePar is not None:
                if self.doc.getTagName(nodePar)==self.GetTagName():
                    break
                nodePar=self.doc.getParent(nodePar)
            if nodePar is not None:
                netSrv=self.doc.GetServerInstanceByNode(nodePar)
                dataInst=netSrv.GetDataInstance()
                self.__procAliases__(tmp,dataInst,appl,True,False)
    def NotifyStart(self,sAliasSrc,id):
        vtLog.vtLngCurCls(vtLog.INFO,'src:%s;id:%08d'%(sAliasSrc,id),self)
        if id>=0:
            node=self.doc.getNodeByIdNum(id)
            nodePar=node
            while nodePar is not None:
                if self.doc.getTagName(nodePar)==self.GetTagName():
                    break
                nodePar=self.doc.getParent(nodePar)
            if nodePar is not None:
                netSrv=self.doc.GetServerInstanceByNode(nodePar)
                dataInst=netSrv.GetDataInstance()
                self.__procAliases__(node,dataInst,appl,True,False)
    def NotifyStop(self,sAliasSrc,id):
        vtLog.vtLngCurCls(vtLog.INFO,'src:%s;id:%08d'%(sAliasSrc,id),self)
        if id>=0:
            node=self.doc.getNodeByIdNum(id)
            nodePar=node
            while nodePar is not None:
                if self.doc.getTagName(nodePar)==self.GetTagName():
                    break
                nodePar=self.doc.getParent(nodePar)
            if nodePar is not None:
                netSrv=self.doc.GetServerInstanceByNode(nodePar)
                dataInst=netSrv.GetDataInstance()
                self.__procAliases__(node,dataInst,appl,False,False)

