#----------------------------------------------------------------------------
# Name:         vsNetXmlNodeSrv.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060703
# CVS-ID:       $Id: vsNetXmlNodeSrv.py,v 1.8 2010/03/06 20:31:06 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeBase import vtXmlNodeBase
try:
    if vcCust.is2Import(__name__):
        from vsNetXmlNodeSrvPanel import vsNetXmlNodeSrvPanel
        from vsNetXmlNodeSrvEditDialog import vsNetXmlNodeSrvEditDialog
        from vsNetXmlNodeSrvAddDialog import vsNetXmlNodeSrvAddDialog
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vsNetXmlNodeSrv(vtXmlNodeBase):
    NODE_ATTRS=[
            ('Tag',None,'tag',None),
            ('Name',None,'name',None),
        ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='Srv'):
        global _
        _=vtLgBase.assignPluginLang('vSrvNet')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'server')
    # ---------------------------------------------------------
    # specific
    def GetTag(self,node):
        return self.Get(node,'tag')
    def GetName(self,node):
        return self.GetML(node,'name')
    def GetPortData(self,node):
        return self.GetVal(node,'portData',int,50004)
    def GetPortService(self,node):
        return self.GetVal(node,'portService',int,50005)
    def GetServicePasswd(self,node):
        return self.Get(node,'servicePasswd')
    def SetTag(self,node,val):
        self.Set(node,'tag',val)
    def SetName(self,node,val):
        self.SetML(node,'name',val)
    def SetPortData(self,node,val):
        return self.SetVal(node,'portData',val)
    def SetPortService(self,node,val):
        return self.SetVal(node,'portService',val)
    def SetServicePasswd(self,node,passwd):
        if len(passwd)>0:
            m=sha.new()
            m.update(passwd)
            passwd=m.hexdigest()
        else:
            passwd=''
        self.Set(node,'servicePasswd',passwd)
    def SetServicePasswdEncoded(self,node,passwd):
        self.Set(node,'servicePasswd',passwd)
    def GetServerCls(self):
        from vidarc.tool.net.vtNetSrv import vtNetSrv
        return vtNetSrv
    def GetDataSrvCls(self):
        from vidarc.tool.net.vtNetXml import vtSrvXmlSock
        return vtSrvXmlSock
    def GetDataCls(self):
        from vidarc.tool.net.vtNetXml import vtNetXmlSock
        return vtNetXmlSock
    def GetServiceSrvCls(self):
        from vidarc.tool.net.vtNetSrv import vtSrvSock
        return vtSrvSock
    def GetServiceCls(self):
        from vidarc.tool.net.vtNetSrv import vtServiceSock
        return vtServiceSock
    def Start(self,node,dataInst):
        pass
    def Stop(self,node,dataInst):
        pass
    def Setup(self,node,dataInst):
        pass
    def BuildLoginPossible(self,node,dataInst,docLogin=None,nodeLogin=None):
        pass
    def SetLoginPossible(self,node,dataInst):
        pass
    def StartServer(self,node):
        try:
            sTag=self.doc.getTagName(node)
            if sTag!=self.GetTagName():
                vtLog.vtLngCurCls(vtLog.WARN,'tag:%s is not reg tag:%s'%(sTag,self.GetTagName()),self)
                return None
            vtLog.vtLngCurCls(vtLog.INFO,'tag:%s start server'%(sTag),self)
            vtLog.PrintMsg(_(u'%s %s starting')%(self.GetDescription(),sTag))
            if self.doc.HasServerInstanceByNode(node):
                netSrv=self.doc.GetServerInstanceByNode(node)
            else:
                iPortData=self.GetPortData(node)
                iPortService=self.GetPortService(node)
                sServicePasswd=self.GetServicePasswd(node)
                netSrv=self.GetServerCls()(self.GetDataSrvCls(),iPortData,self.GetDataCls(),
                                    self.GetServiceSrvCls(),iPortService,self.GetServiceCls(),2,1)
                netSrv.SetServicePasswd(sServicePasswd)
                netSrv.Serve()
                self.doc.AddServerInstanceByNode(node,netSrv)
                dataInst=netSrv.GetDataInstance()
                if hasattr(dataInst,'SetSaveDelay'):
                    self.doc.CallBack(dataInst.SetSaveDelay,5)
                self.Start(node,dataInst)
                #self.doc.DoSafe(self.Start,node,dataInst)
            vtLog.vtLngCurCls(vtLog.INFO,'tag:%s start server finished'%(sTag),self)
            vtLog.PrintMsg(_(u'%s %s started')%(self.GetDescription(),sTag))
            return netSrv
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return None
    def SetupServer(self,node):
        try:
            sTag=self.doc.getTagName(node)
            if sTag!=self.GetTagName():
                vtLog.vtLngCurCls(vtLog.WARN,'tag:%s is not reg tag:%s'%(sTag,self.GetTagName()),self)
                return
            vtLog.vtLngCurCls(vtLog.INFO,'tag:%s setup server'%(sTag),self)
            vtLog.PrintMsg(_(u'%s %s settingup')%(self.GetDescription(),sTag))
            if self.doc.HasServerInstanceByNode(node):
                netSrv=self.doc.GetServerInstanceByNode(node)
                dataInst=netSrv.GetDataInstance()
                self.Setup(node,dataInst)
                #self.doc.DoSafe(self.Setup,node,dataInst)
            vtLog.vtLngCurCls(vtLog.INFO,'tag:%s setup server finished'%(sTag),self)
            vtLog.PrintMsg(_(u'%s %s setup done')%(self.GetDescription(),sTag))
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def StopServer(self,node):
        try:
            sTag=self.doc.getTagName(node)
            if sTag!=self.GetTagName():
                vtLog.vtLngCurCls(vtLog.WARN,'tag:%s is not reg tag:%s'%(sTag,self.GetTagName()),self)
                return None
            vtLog.vtLngCurCls(vtLog.INFO,'tag:%s stop server'%(sTag),self)
            vtLog.PrintMsg(_(u'%s %s stopping')%(self.GetDescription(),sTag))
            if self.doc.HasServerInstanceByNode(node):
                netSrv=self.doc.GetServerInstanceByNode(node)
                netSrv.Stop()
                dataInst=netSrv.GetDataInstance()
                self.Stop(node,dataInst)
                self.doc.DelServerInstanceByNode(node)
            vtLog.vtLngCurCls(vtLog.INFO,'tag:%s stop server finished'%(sTag),self)
            vtLog.PrintMsg(_(u'%s %s stopped')%(self.GetDescription(),sTag))
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def BuildLoginPossibleServer(self,node):
        try:
            sTag=self.doc.getTagName(node)
            if sTag!=self.GetTagName():
                vtLog.vtLngCurCls(vtLog.WARN,'tag:%s is not reg tag:%s'%(sTag,self.GetTagName()),self)
                return None
            vtLog.vtLngCurCls(vtLog.INFO,'tag:%s'%(sTag),self)
            if self.doc.HasServerInstanceByNode(node):
                netSrv=self.doc.GetServerInstanceByNode(node)
                dataInst=netSrv.GetDataInstance()
                self.BuildLoginPossible(node,dataInst)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def SetLoginPossibleServer(self,node):
        try:
            sTag=self.doc.getTagName(node)
            if sTag!=self.GetTagName():
                vtLog.vtLngCurCls(vtLog.WARN,'tag:%s is not reg tag:%s'%(sTag,self.GetTagName()),self)
                return None
            vtLog.vtLngCurCls(vtLog.INFO,'tag:%s'%(sTag),self)
            if self.doc.HasServerInstanceByNode(node):
                netSrv=self.doc.GetServerInstanceByNode(node)
                dataInst=netSrv.GetDataInstance()
                self.SetLoginPossible(node,dataInst)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtXmlNodeBase.ACL_MSK_ALL#vtSecXmlAclDom.ACL_MSK_ATTR
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return [('tag',self.FTY_STR),
            ('name',self.FTY_STR),
            ('portData',self.FTY_INT),
            ('portService',self.FTY_INT),]
    def GetTranslation(self,name):
        _(u'tag'),_(u'name'),_(u'portData'),_(u'portService')
        return _(name)
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xa9IDAT8\x8d\x85\x91\xbfk\xdb@\x14\xc7?\xfe\x81\xa1 J\xa1t0mp\x83\
\xc9\xe2\x8e\xddD\x89\xcf\x832\x15:\x04-\xce\x9c)\x1d5\t\x8aN\x1d\x02\xc5\
\x1d\xdbB\x08)\x19K3\x15\xbc\xf7i\xf1?P\xec\xbf\xa1\xb4\x8bAK4\xb8\xaf\x83bU\
\x96\xe2\xe6\xc1\x83{p\x9f\xcf\xdd}\x8fF\xb3E\xb5\xad\x8fZ\x1f-\xcf\xb7\xedk\
4[\xd4\xe1\x13\xa3i\x9aj\x10\x04Z\x15U\x0f\xa9\t\xec\x89\xd1rm\x13\x95\xd7\
\xff\x85\xef\x12\x15\x82*,"\x1a\x04\x81\x06Ap\xb7(<v5\xfa\xf0\x9dN\xa7\x03@\
\x92$L\xa7S\xaa5\x99L\xb8N\x12\x96GG\xb4\xcf\xcex\'\xc2K\xe7=\xed\xcerF\xfcz\
\x84\xfb*\xc4\xf3<D\x04\xc7q\x00\xe8v\xbb\x8c\xc7\xe3B~}~\x0e{{\xdc\xf7<:\
\xdfN\xc1\xa1\x12\xa2EU\xf3\xb6\x16\xb5\xd6j\x18\x86\x1b\xcf\xcb\xb2L\xc3cW\
\xc5\xde\xfc\xc2\xfa-"VUm!PE\xe5#j\x8c\xd9\n\xff\x0b\xd1G\xf5\xebM\xcf\xad\
\xaa\x1aU\xc9\xe70\x0c5\xcb\xb2\x1a\\\x84\xd8h\xb6\xf8\xf3e\xa5\xc9\x02\xe4\
\n\x8c\x0fCc\xe0\x97\x000\x8a\xc1u]\x00\x0e\x1e\xcf\x18\xbdm5\xca\xe1\xb6\
\xcb\xc3\x12\x909\x0c\x079\x1c\x7f\xcag\x98a\x060\xba\xd8\x847\x042\x87\x07\
\x8f\xc0<\xa3\x80#\x1fD\r\xfd~\x1f\xd2\x8b\xda\xd7\x024\x01\xe2\xab\x1c4\x00\
\x02\xf1\xc2\x12\xf9\x90\xfc\x86^\xaf\xc7\xee\xd3\xdd[a \xcf\x00 :\\\xe9\xfa\
\xf4\xe1\x00\x92\x05\x9c\xce\x0e\xd8\x7f\xb1\xcf\x1b\x1b\xd5\xae^\x13\xac+:\
\\\xe9\x8f\x9f\x0fI\xef=g\xe7\xc9\x0e\x9f//\xb7\xc2\x00\x7f\x01:T?mT\x8b\x93\
\x9a\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return vsNetXmlNodeSrvEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return vsNetXmlNodeSrvAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vsNetXmlNodeSrvPanel
        else:
            return None
