#----------------------------------------------------------------------------
# Name:         vXmlNodeHosts.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060628
# CVS-ID:       $Id: vXmlNodeHosts.py,v 1.4 2010/03/06 20:31:06 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeBase import vtXmlNodeBase
try:
    if vcCust.is2Import(__name__):
        from vXmlNodeHostsPanel import *
        from vXmlNodeHostsEditDialog import *
        from vXmlNodeHostsAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeHosts(vtXmlNodeBase):
    NODE_ATTRS=[
            ('Tag',None,'tag',None),
            ('Name',None,'name',None),
        ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='Hosts'):
        global _
        _=vtLgBase.assignPluginLang('vSrvNet')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'hosts')
    # ---------------------------------------------------------
    # specific
    def GetTag(self,node):
        #return self.Get(node,'tag')
        return 'hosts'
    def GetName(self,node):
        #return self.GetML(node,'name')
        return _(u'hosts')
    def GetHosts(self,node):
        lHosts=[]
        for c in self.doc.getChilds(node,'host'):
            sHost=self.doc.getNodeText(c,'name')
            sUsr=self.doc.getNodeText(c,'user')
            sPasswd=self.doc.getNodeText(c,'passwd')
            lHosts.append([sHost,sUsr,sPasswd])
        return lHosts
    def SetTag(self,node,val):
        self.Set(node,'tag',self.GetTagName())
    def SetName(self,node,val):
        #self.SetML(node,'name',val)
        pass
    def SetHosts(self,node,lHosts):
        for c in self.doc.getChilds(node,'host'):
            self.doc.deleteNode(c,node)
        for tup in lHosts:
            self.doc.createSubNodeDict(node,{
                    'tag':'host','lst':[
                        {'tag':'name','val':tup[0]},
                        {'tag':'user','val':tup[1]},
                        {'tag':'passwd','val':tup[2]},
                        ]
                    })
    # ---------------------------------------------------------
    # inheritance
    def __createPost__(self,node,*args,**kwargs):
        self.SetTag(node,'')
    def GetPossibleAcl(self):
        return vtXmlNodeBase.ACL_MSK_ALL#vtSecXmlAclDom.ACL_MSK_ATTR
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return False
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02ZIDAT8\x8d\x95\x93\xbdk\xd6W\x14\xc7?\xf7\xe4z\xf3\xcb\xd3\xb4>M\x8c\
\xe2KR\xc1\xa1\x93C\xe9\xd4@\x12QTtqh\x05\'\xff\x0b\xa7\xd2)\xd0\xc9\xb5]tQ\
\xba\xb5\xd0IPJ)\x9a\xa0C\x1b_\xc0\xc5\xd2\xa4\xd5\x1801/\xbf\xc4\xdfs\x9f\
\xfb\xdc\x1c\xcf\xaf\xc3SI0Yz\xe0\x0c\x07\xbe|\xce\x0b\xdf\xe3\x9c\xf4\x000v\
z\xaa\xe6\x7f\xc4\xd4/c\x0e\xc09\xe9a\xec\xf4T}\xeb\xa7\xcf\x00\xddE\xea\xc9\
\x92\xc99\x92\xb5\x82\xd4\xe0\xf9_\xf3\x8c\x9f\xba\x8dc\xd2\xf9w2\x13\xe3\
\xbbk\xdf\x137\xd6\xba\xb56\x11\x0f\xa6%\xa6`"t:-\xce\x9d\xbc@(\xb6\xe3\xb7\
\xc5\xbd\xdf\x1e3$\x87)\x9a\xfbi\xf8\x00Td\rD5Z\xaf^\xb3\xc7\x0b:a\x04\xdb\
\x05 &\x84\xa2`\xe0\xa3#\x14\xcd!\xc4{r\xf5\x06\xd5e\x1aI\xe9\x0c\x0c\xe2V\
\xd7qZ\xa3\xb1\xde\t01\xfaB@<hjo\tT\xc8\xb9\xa27\x03\x01*\x8b|H\xdf.\x13\x88\
\xd1B\x89UEQ\x80\x14\x194\x90\x15\xa2\xf4\xd3\xa1D\xe8\xa5\xc0\xb3i\xba\x13`\
&\x903o\xaaU\xd6\xf7G>X\xf2\xa4`\xd4\xa6\x18\x8aTmjo\xa8*`;\x01\x00\xba\x9c\
\xc8\xb4\xe1e\x8b\x95F\x80\x04>f2J\xc0C\x16bm\xf4wv9\xe2F\\\xe5\xdb\xab_\xe3\
\xbdGQ\xb2\x80t2\x1a!\xd6\x15f\x90\xaa\xc4z\xef&,\xbd\x07x>;\xcc\xcf?\xde!\
\xa6\x0cR\x92\xb2\xe0\xb3\x92\xebn\x02\x90\x94\x1c3\x0b\x9d\x16\xfb\xd4C}\
\x11\xdcd\x17\xf0b\xee\xa8\x9b[\xb8U\x9f?#H\x08\xac\xcc\xbf&)\xac-o03\xf3\
\x84\xf9\xf2OF\x8f\x8frh\xe4\x08\x8d\xb2d\xfa\xee\t\x9c\x9b\xd8\xb2\xf2\x95o\
~\xad\xbf\xf8<2\xf4q\x93\xc5r\x03m\xb7\x10\xdf\x07(\xc6\xdb\xff\x1cn\x88\xef\
c\xe6\x8f\x87\x1c;z\x80\xe9\xe9&7\x7f\xb8\xe4\xdc\xf8\xd9\xfb\xf5\x8d\xeb\
\x9f\x80\x80i\xc6\xcc\xe3ek\xc7v\x84V\x8e8\xe9\xa0\x9b\x8a9ON\x89\xf9W\x0b\\\
\xfe\xf2\xab\xee/<]\x9eex\xe0\x00\x83\xa1\x81\x01f\x90S\xb7k\xd66.l\xf2\xf7\
\xec3\x96\xe6JV\xe2"\x9f\x8e\x1cg=\x95[G\xfc\xe7\xf7G\xcc\xbe\xff\x88Z\xa3(j\
\x99\xbd\xfd\x07Y\\Z i\x82d<z\xfa\x00\t\r\x00\xfe\x05\xec\xac4\xea9?23\x00\
\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return vXmlNodeHostsEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return vXmlNodeHostsAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeHostsPanel
        else:
            return None
