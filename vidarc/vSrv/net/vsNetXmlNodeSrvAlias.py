#----------------------------------------------------------------------------
# Name:         vsNetXmlNodeSrvAlias.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060703
# CVS-ID:       $Id: vsNetXmlNodeSrvAlias.py,v 1.4 2010/03/06 20:31:06 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeBase import vtXmlNodeBase
try:
    if vcCust.is2Import(__name__):
        from vsNetXmlNodeSrvAliasPanel import *
        from vsNetXmlNodeSrvAliasEditDialog import *
        from vsNetXmlNodeSrvAliasAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vsNetXmlNodeSrvAlias(vtXmlNodeBase):
    NODE_ATTRS=[
            ('Tag',None,'tag',None),
            ('Name',None,'name',None),
        ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='SrvAlias'):
        global _
        _=vtLgBase.assignPluginLang('vSrvNet')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'server alias')
    # ---------------------------------------------------------
    # specific
    def GetTag(self,node):
        return self.Get(node,'tag')
    def GetName(self,node):
        return self.GetML(node,'name')
    def GetFN(self,node):
        return self.Get(node,'fn')
    def SetTag(self,node,val):
        self.Set(node,'tag',val)
    def SetName(self,node,val):
        self.SetML(node,'name',val)
    def SetFN(self,node,val):
        self.Set(node,'fn',val)
    def GetDoc2Serve(self,node):
        return None
    def GetHosts(self,node):
        nodeHost=self.doc.getChild(node,'Hosts')
        if nodeHost is None:
            return []
        oReg=self.doc.GetReg('Hosts')
        return oReg.GetHosts(nodeHost)
    def GetServerObjNode(self,node):
        nodePar=self.doc.getParent(node)
        if nodePar is None:
                return None,None,None
        o=self.doc.GetRegByNode(nodePar)
        if hasattr(o,'GetServerObjNode'):
            return o.GetServerObjNode(nodePar)
        return o,nodePar,nodePar
    def Setup(self,node,dataInst,appl):
        pass
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtXmlNodeBase.ACL_MSK_ALL#.ACL_MSK_ATTR
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return [('tag',vtXmlFilterType.FILTER_TYPE_STRING),
            ('name',vtXmlFilterType.FILTER_TYPE_STRING),
            ('fn',vtXmlFilterType.FILTER_TYPE_STRING),]
    def GetTranslation(self,name):
        _(u'tag'),_(u'name'),_('fn')
        return _(name)
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01<IDAT8\x8d\x8d\x93\xbdK\xc3P\x14\xc5\x7f/\xba\xe8b\xadt\xeelK]\xfcX\
\xcd \xe2\x1c\x1a2\xfao\x88\x0e/YJqT\xf0\x7f\x08\x95BA\xdd\x84d\xe9\xd2E\x90\
\xd6\xc9\xc1\xcdF\x10A\xe8Z\x87|\xd0\xf4\xbd\x87=\xd3{\xf7\xdes\xeey\xc9\xbd\
BXk,\xa3\xd5m\xcf\x95 \xf0zq/\x96c\xd6\xaa\x85\xba\x18\x80\xc8\x1d\x98\xba\
\x9a\x90\x0bj\x1d,\xe2`l\xb15\xf80\xe6\xd7M\tg\x16"ez\x0e\x02`vF\x7f\xf3M\
\xa9\xb3t\xd6o\x8ev\x91\xd2/\xeeR\x82]\xf3\xa9>MKu\xadn{.\xf6\xae\xbd\x92\
\x803\n\x91nvi\xfa\xd0\x88 \x8e \x81\xab\x97K\x1e*\xefe\x07\xcb\xdd\xa5\x0b\
\xf1$\xb5\x1d\xf7|\x88\x81$\xcd\r\x07\x1d\xf5\tJ$\xc3\x0f\x10\x8d\x81$\x02 \
\xb8\x83\xefso5\x81h\x0c\x95\x1a\xd8M\n\xb2t\xa1\xfa8Uj-(\x0fI\xd0K\x896@\
\x04\xc1\xc4O\x9f\xf5\x05\xf5z\xbd \xe6\x1c\xed 9\xa3\xb0\xe8~\xdcH\xbfIgx\
\xca\xe7\xc9\xb6"\xa0\x9d\x83\xfe\xa1G?\x13\xba}\xde\xe1wc\xbfD^\x84q\x90r\
\xa1\xff t\xdb\x08\xean\x98\x96I\xfb\x17t\xd3iZ\xb6?\xdd>hR\xb6\xda\x84F\x00\
\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return vsNetXmlNodeSrvAliasEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return vsNetXmlNodeSrvAliasAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vsNetXmlNodeSrvAliasPanel
        else:
            return None
