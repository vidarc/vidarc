#----------------------------------------------------------------------------
# Name:         vsNetXmlLoginCache.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20061227
# CVS-ID:       $Id: vsNetXmlLoginCache.py,v 1.4 2008/05/10 20:04:19 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from wx import DateTime as wxDateTime
import vidarc.tool.log.vtLog as vtLog

from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vidarc.vSrv.net.vsNetXmlLoginCacheNodeRoot import vsNetXmlLoginCacheNodeRoot
from vidarc.vSrv.net.vsNetXmlLoginCacheNodeSrv import vsNetXmlLoginCacheNodeSrv
from vidarc.vSrv.net.vsNetXmlLoginCacheNodeAlias import vsNetXmlLoginCacheNodeAlias
from vidarc.vSrv.net.vsNetXmlLoginCacheNodeUsr import vsNetXmlLoginCacheNodeUsr

class vsNetXmlLoginCache(vtXmlDomReg):
    def __init__(self,*kw,**kwargs):
        vtXmlDomReg.__init__(self,*kw,**kwargs)
        try:
            self.dt=wxDateTime()
            self.dLogin={}
            oRoot=vsNetXmlLoginCacheNodeRoot()
            oSrv=vsNetXmlLoginCacheNodeSrv()
            oAlias=vsNetXmlLoginCacheNodeAlias()
            oUsr=vsNetXmlLoginCacheNodeUsr()
            
            self.RegisterNode(oRoot,False)
            self.RegisterNode(oSrv,True)
            self.RegisterNode(oAlias,False)
            self.RegisterNode(oUsr,False)
            
            self.LinkRegisteredNode(oRoot,oSrv)
            self.LinkRegisteredNode(oSrv,oAlias)
            self.LinkRegisteredNode(oAlias,oUsr)
            
        except:
            vtLog.vtLngTB(self.appl)
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'LoginCache')
    def New(self,revision='1.0',root='LoginCacheRoot',bConnection=False):
        vtXmlDomReg.New(self,revision,root)
        elem=self.createSubNode(self.getRoot(),'LoginCache')
        if bConnection==False:
            #e=self.createSubNodeTextAttr(elem,'cfg','','id','')
            #self.checkId(e)
            self.processMissingId()
            self.clearMissing()
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        self.AlignDoc()
    def __procUser__(self,node,oRegUsr,d,sApplAlias):
        sTagName=self.getTagName(node)
        if sTagName==oRegUsr.GetTagName():
            sUsr=oRegUsr.GetTag(node)
            if sUsr in d:
                l=d[sUsr]
            else:
                l=[]
                d[sUsr]=l
            l.append(sApplAlias)
        return 0
    def __procAlias__(self,node,oRegAlias,oRegUsr,d,sAppl):
        sTagName=self.getTagName(node)
        if sTagName==oRegAlias.GetTagName():
            sAlias=oRegAlias.GetTag(node)
            if sAppl is None:
                self.procChildsKeysRec(0,node,self.__procAlias__,
                            oRegAlias,oRegUsr,d,sAlias)
            else:
                self.procChildsKeysRec(0,node,self.__procUser__,
                            oRegUsr,d,':'.join([sAppl,sAlias]))
        return 0
    def __procSrv__(self,node,oRegSrv,oRegAlias,oRegUsr,d,lv):
        sTagName=self.getTagName(node)
        if sTagName==oRegSrv.GetTagName():
            sTag=oRegSrv.GetTag(node)
            if sTag in d:
                vtLog.vtLngCur(vtLog.ERROR,'server:%s id:%s already registered'%(sTag,self.getKey(node)),self.GetOrigin())
            else:
                dd={}
                d[sTag]=dd
                if lv==0:
                    self.procChildsKeysRec(0,node,self.__procSrv__,
                            oRegSrv,oRegAlias,oRegUsr,dd,1)
                else:
                    self.procChildsKeysRec(0,node,self.__procAlias__,
                            oRegAlias,oRegUsr,dd,None)
        return 0
    def Build(self):
        """ this method is called automatically after the XML-file is read.
        """
        try:
            vtLog.vtLngCur(vtLog.INFO,'',self.GetOrigin())
            vtXmlDomReg.Build(self)
            oRegSrv=self.GetReg('LoginCacheSrv')
            oRegAlias=self.GetReg('LoginCacheAlias')
            oRegUsr=self.GetReg('LoginCacheUsr')
            self.dLogin={}
            self.procChildsKeysRec(0,self.getBaseNode(),self.__procSrv__,
                        oRegSrv,oRegAlias,oRegUsr,self.dLogin,0)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'dLogin:%s'%(vtLog.pformat(self.dLogin)),self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def GetLoginCache(self,sSrvMain,sSrvSub):
        try:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCur(vtLog.INFO,'main:%s;sub:%s;dLogin:%s'%(sSrvMain,sSrvSub,vtLog.pformat(self.dLogin)),self.GetOrigin())
            if sSrvMain in self.dLogin:
                d=self.dLogin[sSrvMain]
                if sSrvSub in d:
                    return d[sSrvSub]
        except:
            vtLog.vtLngTB(self.GetOrigin())
        return None
    def __procAliasOnly__(self,node,oRegSrv,oRegAlias,l,sAppl):
        sTagName=self.getTagName(node)
        if sTagName==oRegSrv.GetTagName():
            iFID,sAppl=self.getForeignKeyNumAppl(node)
            l.append((iFID,self.getKeyNum(node),{}))
            self.procChildsKeysRec(0,node,self.__procAliasOnly__,
                            oRegSrv,oRegAlias,l,None)
        if sTagName==oRegAlias.GetTagName():
            sAlias=oRegAlias.GetTag(node)
            if sAppl is None:
                d=l[-1][2]
                iFID,sAppl=self.getForeignKeyNumAppl(node)
                d[iFID]=[self.getKeyNum(node),None]
                self.procChildsKeysRec(0,node,self.__procAliasOnly__,
                            oRegSrv,oRegAlias,d[iFID],sAlias)
            else:
                if l[1] is None:
                    d={}
                    l[1]=d
                else:
                    d=l[1]
                iFID,sAppl=self.getForeignKeyNumAppl(node)
                d[iFID]=self.getKeyNum(node)
        return 0
    def DeleteDeadCacheByDict(self,nodeSrv,dProcIds):
        try:
            oRegSrv=self.GetReg('LoginCacheSrv')
            oRegAlias=self.GetReg('LoginCacheAlias')
            l=[]
            sTagName=self.getTagName(nodeSrv)
            if sTagName==oRegSrv.GetTagName():
                iFID,sAppl=self.getForeignKeyNumAppl(nodeSrv)
                l.append((iFID,self.getKeyNum(nodeSrv),{}))
            self.procChildsKeysRec(0,nodeSrv,self.__procAliasOnly__,
                            oRegSrv,oRegAlias,l,None)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'dLogin:%s;l:%s;dProcIds:%s;'%(vtLog.pformat(self.dLogin),
                            vtLog.pformat(l),vtLog.pformat(dProcIds)),self.GetOrigin())
            for iSrvFID,iSrvID,d in l:
                if d is None:
                    continue
                nSrv=self.getNodeByIdNum(iSrvID)
                if nSrv is None:
                    continue
                
                for iID, dd in d.itervalues():
                    if dd is None:
                        continue
                    bDel=True
                    for fid in dProcIds.iterkeys():
                        if fid in dd:
                            del dd[fid]
                            bDel=False
                    
                    if bDel:
                        n=self.getNodeByIdNum(iID)
                        if n is not None:
                            self.deleteNode(n,nSrv)
                    else:
                        lVal=dd.values()
                        lVal.sort()
                        n=self.getNodeByIdNum(iID)
                        if n is not None:
                            for iChildID in dd:
                                nC=self.getNodeByIdNum(iChildID)
                                self.__deleteNode__(nC,n)
        except:
            vtLog.vtLngTB(self.GetOrigin())
