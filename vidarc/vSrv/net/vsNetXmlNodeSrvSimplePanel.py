#Boa:FramePanel:vsNetXmlNodeSrvSimplePanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeMsgSrvPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060703
# CVS-ID:       $Id: vsNetXmlNodeSrvSimplePanel.py,v 1.4 2010/03/10 22:41:58 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import wx
    import wx.lib.buttons
    import vidarc.vSrv.net.vsNetSrvControl
    import vidarc.tool.input.vtInputText
    import wx.lib.intctrl
    import vidarc.tool.input.vtInputTextML
    
    import sys
    
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel
    
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    from vidarc.tool.net.vtNetXml import *
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VSNETXMLNODESRVSIMPLEPANEL, wxID_VSNETXMLNODESRVSIMPLEPANELINTDATA, 
 wxID_VSNETXMLNODESRVSIMPLEPANELINTSERVICE, 
 wxID_VSNETXMLNODESRVSIMPLEPANELLBLNAME, 
 wxID_VSNETXMLNODESRVSIMPLEPANELLBLPORTDATA, 
 wxID_VSNETXMLNODESRVSIMPLEPANELLBLPORTSERVICE, 
 wxID_VSNETXMLNODESRVSIMPLEPANELLBLTAG, wxID_VSNETXMLNODESRVSIMPLEPANELVINAME, 
 wxID_VSNETXMLNODESRVSIMPLEPANELVITAG, 
] = [wx.NewId() for _init_ctrls in range(9)]

class vsNetXmlNodeSrvSimplePanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_bxsName_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblName, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viName, 2, border=4, flag=wx.EXPAND)

    def _init_coll_bxsPortData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPortData, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.intData, 2, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsTag, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsName, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsPortData, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsPortService, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)

    def _init_coll_bxsPortService_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPortService, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.intService, 2, border=0, flag=wx.EXPAND)

    def _init_coll_bxsTag_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblTag, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.viTag, 2, border=4, flag=wx.EXPAND)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(7)
        parent.AddGrowableCol(0)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=0, rows=4, vgap=0)

        self.bxsName = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsPortData = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsPortService = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsTag = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsName_Items(self.bxsName)
        self._init_coll_bxsPortData_Items(self.bxsPortData)
        self._init_coll_bxsPortService_Items(self.bxsPortService)
        self._init_coll_bxsTag_Items(self.bxsTag)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VSNETXMLNODESRVSIMPLEPANEL,
              name=u'vsNetXmlNodeSrvSimplePanel', parent=prnt, pos=wx.Point(0,
              0), size=wx.Size(293, 279),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(285, 252))
        self.SetAutoLayout(True)

        self.lblName = wx.StaticText(id=wxID_VSNETXMLNODESRVSIMPLEPANELLBLNAME,
              label=_(u'name'), name=u'lblName', parent=self, pos=wx.Point(4,
              29), size=wx.Size(88, 30), style=wx.ALIGN_RIGHT)
        self.lblName.SetMinSize(wx.Size(-1, -1))

        self.lblPortData = wx.StaticText(id=wxID_VSNETXMLNODESRVSIMPLEPANELLBLPORTDATA,
              label=_(u'data port'), name=u'lblPortData', parent=self,
              pos=wx.Point(4, 63), size=wx.Size(88, 21), style=wx.ALIGN_RIGHT)
        self.lblPortData.SetMinSize(wx.Size(-1, -1))

        self.lblPortService = wx.StaticText(id=wxID_VSNETXMLNODESRVSIMPLEPANELLBLPORTSERVICE,
              label=_(u'serive port'), name=u'lblPortService', parent=self,
              pos=wx.Point(4, 88), size=wx.Size(88, 21), style=wx.ALIGN_RIGHT)
        self.lblPortService.SetMinSize(wx.Size(-1, -1))

        self.lblTag = wx.StaticText(id=wxID_VSNETXMLNODESRVSIMPLEPANELLBLTAG,
              label=_(u'alias'), name=u'lblTag', parent=self, pos=wx.Point(4,
              4), size=wx.Size(88, 21), style=wx.ALIGN_RIGHT)
        self.lblTag.SetMinSize(wx.Size(-1, -1))

        self.viName = vidarc.tool.input.vtInputTextML.vtInputTextML(id=wxID_VSNETXMLNODESRVSIMPLEPANELVINAME,
              name=u'viName', parent=self, pos=wx.Point(96, 29),
              size=wx.Size(184, 30), style=0)

        self.intData = wx.lib.intctrl.IntCtrl(allow_long=False,
              allow_none=False, default_color=wx.BLACK,
              id=wxID_VSNETXMLNODESRVSIMPLEPANELINTDATA, limited=True,
              max=65535, min=512, name=u'intData', oob_color=wx.RED,
              parent=self, pos=wx.Point(96, 63), size=wx.Size(184, 21), style=0,
              value=50004)
        self.intData.SetMinSize(wx.Size(-1, -1))

        self.intService = wx.lib.intctrl.IntCtrl(allow_long=False,
              allow_none=False, default_color=wx.BLACK,
              id=wxID_VSNETXMLNODESRVSIMPLEPANELINTSERVICE, limited=True,
              max=65535, min=512, name=u'intService', oob_color=wx.RED,
              parent=self, pos=wx.Point(96, 88), size=wx.Size(184, 21), style=0,
              value=50005)
        self.intService.SetMinSize(wx.Size(-1, -1))

        self.viTag = vidarc.tool.input.vtInputText.vtInputText(id=wxID_VSNETXMLNODESRVSIMPLEPANELVITAG,
              name=u'viTag', parent=self, pos=wx.Point(96, 4), size=wx.Size(184,
              21), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vSrvNet')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.viTag.SetTagName('tag')
        self.viName.SetTagName('name')
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def __Clear__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viTag.Clear()
        self.viName.Clear()
        self.intData.SetValue(self.objRegNode.GetPortData(None))
        self.intService.SetValue(self.objRegNode.GetPortService(None))
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viTag.SetDoc(doc)
        self.viName.SetDoc(doc)
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viTag.SetNode(self.node)
            self.viName.SetNode(self.node)
                
            self.intData.SetValue(self.objRegNode.GetPortData(node))
            self.intService.SetValue(self.objRegNode.GetPortService(node))
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viTag.GetNode(node)
            self.viName.GetNode(node)
            self.objRegNode.SetPortData(node,self.intData.GetValue())
            self.objRegNode.SetPortService(node,self.intService.GetValue())
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viName.Close()
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            # add code here
            self.viTag.Enable(False)
            self.viName.Enable(False)
            self.intData.Enable(False)
            self.intService.Enable(False)
        else:
            # add code here
            self.viTag.Enable(True)
            self.viName.Enable(True)
            self.intData.Enable(True)
            self.intService.Enable(True)
