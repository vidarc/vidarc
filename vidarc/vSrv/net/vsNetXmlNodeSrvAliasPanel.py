#Boa:FramePanel:vsNetXmlNodeSrvAliasPanel
#----------------------------------------------------------------------------
# Name:         vsNetXmlNodeSrvAliasPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060703
# CVS-ID:       $Id: vsNetXmlNodeSrvAliasPanel.py,v 1.7 2010/03/10 22:41:57 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import wx
    import vidarc.tool.time.vtTimeInputDateTime
    import wx.lib.buttons
    import wx.lib.filebrowsebutton
    import vidarc.tool.input.vtInputText
    import wx.lib.intctrl
    import vidarc.tool.input.vtInputTextML

    import sys,time

    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase

    from vidarc.tool.sec.vtSecAclDomDialog import *
    from vidarc.vSrv.net.vsNetXmlConfigDialog import vsNetXmlConfigDialog
    import vidarc.vSrv.net.vsNetSrvControl as vsNetSrvControl
    from vidarc.tool.net.vtNetXmlLocksPanel import *
    import vidarc.vSrv.net.images as imgSrvNet
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VSNETXMLNODESRVALIASPANEL, wxID_VSNETXMLNODESRVALIASPANELCBACL, 
 wxID_VSNETXMLNODESRVALIASPANELCBCFG, 
 wxID_VSNETXMLNODESRVALIASPANELCBCONNCLOSE, 
 wxID_VSNETXMLNODESRVALIASPANELCBHOST, wxID_VSNETXMLNODESRVALIASPANELCBSAVE, 
 wxID_VSNETXMLNODESRVALIASPANELCBSERVE, wxID_VSNETXMLNODESRVALIASPANELFBBFN, 
 wxID_VSNETXMLNODESRVALIASPANELLBLLASTACCESS, 
 wxID_VSNETXMLNODESRVALIASPANELLBLLASTSAVE, 
 wxID_VSNETXMLNODESRVALIASPANELLBLLASTUSE, 
 wxID_VSNETXMLNODESRVALIASPANELLBLNAME, wxID_VSNETXMLNODESRVALIASPANELLBLTAG, 
 wxID_VSNETXMLNODESRVALIASPANELLSTCONN, wxID_VSNETXMLNODESRVALIASPANELNBINFOS, 
 wxID_VSNETXMLNODESRVALIASPANELPNCONN, wxID_VSNETXMLNODESRVALIASPANELPNLOCKS, 
 wxID_VSNETXMLNODESRVALIASPANELPNTIMES, wxID_VSNETXMLNODESRVALIASPANELVINAME, 
 wxID_VSNETXMLNODESRVALIASPANELVITAG, 
 wxID_VSNETXMLNODESRVALIASPANELVTLASTACCESS, 
 wxID_VSNETXMLNODESRVALIASPANELVTLASTSAVE, 
 wxID_VSNETXMLNODESRVALIASPANELVTLASTUSE, 
] = [wx.NewId() for _init_ctrls in range(23)]

class vsNetXmlNodeSrvAliasPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_bxsName_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblName, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viName, 2, border=4, flag=wx.EXPAND)

    def _init_coll_bxsLastSave_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblLastSave, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.vtLastSave, 2, border=4, flag=wx.EXPAND)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsTag, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsName, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 16), border=0, flag=0)
        parent.AddWindow(self.fbbFN, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsBt, 0, border=4, flag=wx.TOP)
        parent.AddWindow(self.nbInfos, 0, border=4, flag=wx.TOP | wx.EXPAND)

    def _init_coll_bxsConnBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbConnClose, 0, border=0, flag=0)

    def _init_coll_fgsTimes_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbAcl, 0, border=4, flag=wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.cbCfg, 0, border=4, flag=wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.cbHost, 0, border=4, flag=wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.cbSave, 0, border=4, flag=wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.cbServe, 0, border=4, flag=wx.RIGHT | wx.LEFT)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(5)
        parent.AddGrowableCol(0)

    def _init_coll_fgsTimes_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsLastAccess, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsLastSave, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsLastUse, 0, border=0, flag=wx.EXPAND)

    def _init_coll_bxsLastUse_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblLastUse, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.vtLastUse, 2, border=4, flag=wx.EXPAND)

    def _init_coll_bxsLastAccess_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblLastAccess, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.vtLastAccess, 2, border=4, flag=wx.EXPAND)

    def _init_coll_bxsTag_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblTag, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.viTag, 2, border=4, flag=wx.EXPAND)

    def _init_coll_bxsConn_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsConnBt, 0, border=4, flag=wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.lstConn, 1, border=0, flag=wx.EXPAND)

    def _init_coll_nbInfos_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.pnLocks, select=True,
              text=_(u'locks'))
        parent.AddPage(imageId=-1, page=self.pnConn, select=False,
              text=_(u'connections'))
        parent.AddPage(imageId=-1, page=self.pnTimes, select=False,
              text=_(u'times'))

    def _init_coll_lstConn_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_RIGHT,
              heading=_(u'nr'), width=40)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'host'), width=100)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_RIGHT,
              heading=_(u'port'), width=60)
        parent.InsertColumn(col=3, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'usr'), width=60)
        parent.InsertColumn(col=4, format=wx.LIST_FORMAT_RIGHT,
              heading=_(u'sec lv'), width=40)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=0, rows=6, vgap=0)

        self.bxsName = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsTag = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsLastAccess = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsLastUse = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsLastSave = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsConn = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsConnBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.fgsTimes = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=4)

        self.bxsLocks = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsName_Items(self.bxsName)
        self._init_coll_bxsTag_Items(self.bxsTag)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsLastAccess_Items(self.bxsLastAccess)
        self._init_coll_bxsLastUse_Items(self.bxsLastUse)
        self._init_coll_bxsLastSave_Items(self.bxsLastSave)
        self._init_coll_bxsConn_Items(self.bxsConn)
        self._init_coll_bxsConnBt_Items(self.bxsConnBt)
        self._init_coll_fgsTimes_Items(self.fgsTimes)
        self._init_coll_fgsTimes_Growables(self.fgsTimes)

        self.SetSizer(self.fgsLog)
        self.pnLocks.SetSizer(self.bxsLocks)
        self.pnConn.SetSizer(self.bxsConn)
        self.pnTimes.SetSizer(self.fgsTimes)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VSNETXMLNODESRVALIASPANEL,
              name=u'vsNetXmlNodeSrvAliasPanel', parent=prnt, pos=wx.Point(0,
              0), size=wx.Size(312, 322),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 295))
        self.SetAutoLayout(True)

        self.lblName = wx.StaticText(id=wxID_VSNETXMLNODESRVALIASPANELLBLNAME,
              label=_(u'name'), name=u'lblName', parent=self, pos=wx.Point(4,
              29), size=wx.Size(94, 30), style=wx.ALIGN_RIGHT)
        self.lblName.SetMinSize(wx.Size(-1, -1))

        self.lblTag = wx.StaticText(id=wxID_VSNETXMLNODESRVALIASPANELLBLTAG,
              label=_(u'alias'), name=u'lblTag', parent=self, pos=wx.Point(4,
              4), size=wx.Size(94, 21), style=wx.ALIGN_RIGHT)
        self.lblTag.SetMinSize(wx.Size(-1, -1))

        self.viName = vidarc.tool.input.vtInputTextML.vtInputTextML(id=wxID_VSNETXMLNODESRVALIASPANELVINAME,
              name=u'viName', parent=self, pos=wx.Point(102, 29),
              size=wx.Size(197, 30), style=0)

        self.viTag = vidarc.tool.input.vtInputText.vtInputText(id=wxID_VSNETXMLNODESRVALIASPANELVITAG,
              name=u'viTag', parent=self, pos=wx.Point(102, 4),
              size=wx.Size(197, 21), style=0)

        self.fbbFN = wx.lib.filebrowsebutton.FileBrowseButton(buttonText='...',
              dialogTitle=_(u'Choose a file'), fileMask=u'*.xml',
              id=wxID_VSNETXMLNODESRVALIASPANELFBBFN, labelText=u'File Entry:',
              parent=self, pos=wx.Point(0, 75), size=wx.Size(304, 29),
              startDirectory='.', style=0)

        self.cbAcl = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VSNETXMLNODESRVALIASPANELCBACL,
              bitmap=vtArt.getBitmap(vtArt.Acl), label=_(u'ACL'), name=u'cbAcl',
              parent=self, pos=wx.Point(4, 108), size=wx.Size(76, 30), style=0)
        self.cbAcl.SetMinSize(wx.Size(-1, -1))
        self.cbAcl.Bind(wx.EVT_BUTTON, self.OnCbAclButton,
              id=wxID_VSNETXMLNODESRVALIASPANELCBACL)

        self.cbCfg = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VSNETXMLNODESRVALIASPANELCBCFG,
              bitmap=vtArt.getBitmap(vtArt.Config), label=u'Config',
              name=u'cbCfg', parent=self, pos=wx.Point(88, 108),
              size=wx.Size(76, 30), style=0)
        self.cbCfg.SetMinSize(wx.Size(-1, -1))
        self.cbCfg.Bind(wx.EVT_BUTTON, self.OnCbCfgButton,
              id=wxID_VSNETXMLNODESRVALIASPANELCBCFG)

        self.cbHost = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VSNETXMLNODESRVALIASPANELCBHOST,
              bitmap=wx.EmptyBitmap(16,16), label=_(u'Host'), name=u'cbHost', parent=self,
              pos=wx.Point(172, 108), size=wx.Size(76, 30), style=0)
        self.cbHost.Bind(wx.EVT_BUTTON, self.OnCbHostButton,
              id=wxID_VSNETXMLNODESRVALIASPANELCBHOST)

        self.cbSave = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VSNETXMLNODESRVALIASPANELCBSAVE,
              bitmap=vtArt.getBitmap(vtArt.Save), label=_(u'Save'),
              name=u'cbSave', parent=self, pos=wx.Point(256, 108),
              size=wx.Size(76, 30), style=0)
        self.cbSave.Bind(wx.EVT_BUTTON, self.OnCbSaveButton,
              id=wxID_VSNETXMLNODESRVALIASPANELCBSAVE)

        self.cbServe = wx.lib.buttons.GenBitmapToggleButton(ID=wxID_VSNETXMLNODESRVALIASPANELCBSERVE,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbServe', parent=self,
              pos=wx.Point(340, 108), size=wx.Size(31, 30), style=0)
        self.cbServe.Bind(wx.EVT_BUTTON, self.OnCbServeButton,
              id=wxID_VSNETXMLNODESRVALIASPANELCBSERVE)

        self.nbInfos = wx.Notebook(id=wxID_VSNETXMLNODESRVALIASPANELNBINFOS,
              name=u'nbInfos', parent=self, pos=wx.Point(0, 142),
              size=wx.Size(304, 153), style=0)

        self.pnConn = wx.Panel(id=wxID_VSNETXMLNODESRVALIASPANELPNCONN,
              name=u'pnConn', parent=self.nbInfos, pos=wx.Point(0, 0),
              size=wx.Size(296, 127), style=wx.TAB_TRAVERSAL)

        self.lstConn = wx.ListCtrl(id=wxID_VSNETXMLNODESRVALIASPANELLSTCONN,
              name=u'lstConn', parent=self.pnConn, pos=wx.Point(39, 0),
              size=wx.Size(257, 127), style=wx.LC_REPORT)
        self._init_coll_lstConn_Columns(self.lstConn)

        self.cbConnClose = wx.lib.buttons.GenBitmapButton(ID=wxID_VSNETXMLNODESRVALIASPANELCBCONNCLOSE,
              bitmap=vtArt.getBitmap(vtArt.Del), name=u'cbConnClose',
              parent=self.pnConn, pos=wx.Point(4, 0), size=wx.Size(31, 30),
              style=0)
        self.cbConnClose.Bind(wx.EVT_BUTTON, self.OnCbConnCloseButton,
              id=wxID_VSNETXMLNODESRVALIASPANELCBCONNCLOSE)

        self.pnTimes = wx.Panel(id=wxID_VSNETXMLNODESRVALIASPANELPNTIMES,
              name=u'pnTimes', parent=self.nbInfos, pos=wx.Point(0, 0),
              size=wx.Size(296, 127), style=wx.TAB_TRAVERSAL)

        self.lblLastAccess = wx.StaticText(id=wxID_VSNETXMLNODESRVALIASPANELLBLLASTACCESS,
              label=_(u'access'), name=u'lblLastAccess', parent=self.pnTimes,
              pos=wx.Point(0, 0), size=wx.Size(94, 24), style=wx.ALIGN_RIGHT)
        self.lblLastAccess.SetMinSize(wx.Size(-1, -1))

        self.lblLastUse = wx.StaticText(id=wxID_VSNETXMLNODESRVALIASPANELLBLLASTUSE,
              label=_(u'use'), name=u'lblLastUse', parent=self.pnTimes,
              pos=wx.Point(0, 56), size=wx.Size(94, 24), style=wx.ALIGN_RIGHT)
        self.lblLastUse.SetMinSize(wx.Size(-1, -1))

        self.lblLastSave = wx.StaticText(id=wxID_VSNETXMLNODESRVALIASPANELLBLLASTSAVE,
              label=_(u'save'), name=u'lblLastSave', parent=self.pnTimes,
              pos=wx.Point(0, 28), size=wx.Size(94, 24), style=wx.ALIGN_RIGHT)
        self.lblLastSave.SetMinSize(wx.Size(-1, -1))

        self.vtLastAccess = vidarc.tool.time.vtTimeInputDateTime.vtTimeInputDateTime(id=wxID_VSNETXMLNODESRVALIASPANELVTLASTACCESS,
              name=u'vtLastAccess', parent=self.pnTimes, pos=wx.Point(98, 0),
              size=wx.Size(197, 24), style=0)

        self.vtLastUse = vidarc.tool.time.vtTimeInputDateTime.vtTimeInputDateTime(id=wxID_VSNETXMLNODESRVALIASPANELVTLASTUSE,
              name=u'vtLastUse', parent=self.pnTimes, pos=wx.Point(98, 56),
              size=wx.Size(197, 24), style=0)

        self.vtLastSave = vidarc.tool.time.vtTimeInputDateTime.vtTimeInputDateTime(id=wxID_VSNETXMLNODESRVALIASPANELVTLASTSAVE,
              name=u'vtLastSave', parent=self.pnTimes, pos=wx.Point(98, 28),
              size=wx.Size(197, 24), style=0)

        self.pnLocks = wx.Panel(id=wxID_VSNETXMLNODESRVALIASPANELPNLOCKS,
              name=u'pnLocks', parent=self.nbInfos, pos=wx.Point(0, 0),
              size=wx.Size(296, 127), style=wx.TAB_TRAVERSAL)

        self._init_coll_nbInfos_Pages(self.nbInfos)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vSrvNet')
        self._init_ctrls(parent, id)
        
        self.dlgSecAcl=None
        self.dlgConfig=None
        self.sAclSecName=None
        self.sConfigName=None
        self.netSrv=None
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.viTag.SetTagName('tag')
        self.viName.SetTagName('name')
        
        self.cbServe.SetBitmapLabel(vsNetSrvControl.getStopBitmap())
        self.cbServe.SetBitmapSelected(vsNetSrvControl.getStartBitmap())
        self.cbHost.SetBitmapLabel(imgSrvNet.getHostBitmap())
        
        self.pnXmlLocks=vtNetXmlLocksPanel(parent=self.pnLocks,id=-1,
                pos=(0,0),size=wx.DefaultSize,style=0,name='pnXmlLocks')
        EVT_VTNET_XML_LOCKS_REQUEST(self.pnXmlLocks,self.OnXmlLocksReq)
        EVT_VTNET_XML_LOCKS_FORCE(self.pnXmlLocks,self.OnXmlLocksForce)
        EVT_VTNET_XML_LOCKS_UPDATE(self.pnXmlLocks,self.OnXmlLocksUpdate)
        self.bxsLocks.AddWindow(self.pnXmlLocks, 1, border=4, flag=wx.EXPAND)
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def __getSrvXml__(self):
        try:
            if self.node is None:
                return None
            o,nodeSrv,nodePar=self.objRegNode.GetServerObjNode(self.node)
            if o is None:
                return None
            netSrv=self.doc.GetServerInstanceByNode(nodeSrv)
            if netSrv is not None:
                srvXml=netSrv.GetDataInstance()
                return srvXml
        except:
            self.__logTB__()
        return None
    def __getSrvXmlAppl__(self):
        try:
            if self.node is None:
                return None
            o,nodeSrv,nodePar=self.objRegNode.GetServerObjNode(self.node)
            if o is None:
                return None,None
            netSrv=self.doc.GetServerInstanceByNode(nodeSrv)
            if netSrv is not None:
                sTagAppl=o.GetTag(nodePar)
                srvXml=netSrv.GetDataInstance()
                return srvXml,sTagAppl
        except:
            self.__logTB__()
        return None,None
    def OnXmlLocksReq(self,evt):
        evt.Skip()
        try:
            alias=evt.GetApplAlias()
            sId=evt.GetID()
            self.__logDebug__('alias:%s;id:%s'%(alias,sId))
            srvXml=self.__getSrvXml__()
            if srvXml is not None:
                bRet=srvXml.IsServedXml(alias)
                if bRet:
                    srvXml.RequestLock(alias,None,sId)
        except:
            self.__logTB__()
    def OnXmlLocksForce(self,evt):
        evt.Skip()
        try:
            alias=evt.GetApplAlias()
            sId=evt.GetID()
            self.__logDebug__('alias:%s;id:%s'%(alias,sId))
            srvXml=self.__getSrvXml__()
            if srvXml is not None:
                bRet=srvXml.IsServedXml(alias)
                if bRet:
                    srvXml.ForceLock(alias,None,sId)
        except:
            self.__logTB__()
    def OnXmlLocksUpdate(self,evt):
        evt.Skip()
        try:
            alias=evt.GetApplAlias()
            self.__logDebug__('alias:%s'%(alias))
            srvXml=self.__getSrvXml__()
            self.lstConn.DeleteAllItems()
            if srvXml is not None:
                bRet=srvXml.IsServedXml(alias)
                if bRet:
                    lLocks=srvXml.GetLockLst(alias)
                    self.pnXmlLocks.SetLocks(alias,lLocks)
                    lConns=srvXml.GetServedXmlConnections(alias)
                    for t in lConns:
                        idx=self.lstConn.InsertStringItem(sys.maxint,str(t[0]))
                        for i in xrange(1,5):
                            self.lstConn.SetStringItem(idx,i,str(t[i]))
                    del lConns
                else:
                    self.pnXmlLocks.SetLocks(alias,[])
                zLast=srvXml.GetServedXmlInfo(alias,'last_access')
                zSave=srvXml.GetServedXmlInfo(alias,'last_save')
                zUse=srvXml.GetServedXmlInfo(alias,'last_modify')
                def conv(zTmp):
                    if zTmp is None:
                        return ''
                    return time.strftime('%Y-%m-%dT%H:%M:%S',time.localtime(zTmp))
                self.vtLastAccess.SetValue(conv(zLast))
                self.vtLastUse.SetValue(conv(zUse))
                self.vtLastSave.SetValue(conv(zSave))
            else:
                self.pnXmlLocks.SetLocks(alias,[])
                self.vtLastAccess.SetValue('')
                self.vtLastUse.SetValue('')
                self.vtLastSave.SetValue('')
        except:
            self.__logTB__()
    def __Clear__(self):
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
        # add code here
        self.viTag.Clear()
        self.viName.Clear()
        self.fbbFN.SetValue('')
        self.pnXmlLocks.SetLocks('',[])
        self.lstConn.DeleteAllItems()
        self.vtLastAccess.SetValue('')
        self.vtLastUse.SetValue('')
        self.vtLastSave.SetValue('')
    def SetNetDocs(self,d):
        self.__logCritical__('FIXME'%())
        return
        if d.has_key('vHum'):
            dd=d['vHum']
            self.docHum=dd['doc']
        # add code here
        
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viTag.SetDoc(doc)
        self.viName.SetDoc(doc)
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.lstConn.DeleteAllItems()
            self.viTag.SetNode(self.node)
            self.viName.SetNode(self.node)
            self.fbbFN.SetValue(self.objRegNode.GetFN(self.node))
            if node is None:
                self.__logDebug__('node is None')
                self.cbServe.Enable(False)
                self.cbServe.SetValue(False)
                return
            if self.__isLogDebug__():
                self.__logDebug__('node:%s,%s'%(self.doc.getTagName(self.node),self.doc.getKey(self.node)))
            o,nodeSrv,nodePar=self.objRegNode.GetServerObjNode(self.node)
            if o is None:
                sMsg=_(u'Server node not found!')
                self.__logPrintMsg__(sMsg)
                self.cbServe.Enable(False)
                self.cbServe.SetValue(False)
                return
            if self.__isLogDebug__():
                self.__logDebug__('oReg:%s;nodeSrv:%s,%s;nodePar:%s,%s'%(o,
                            self.doc.getTagName(nodeSrv),self.doc.getKey(nodeSrv),
                            self.doc.getTagName(nodePar),self.doc.getKey(nodePar)))
            netSrv=self.doc.GetServerInstanceByNode(nodeSrv)
            if netSrv is None:
                sMsg=_(u'Server not started yet!')
                self.__logPrintMsg__(sMsg)
                self.cbServe.SetValue(False)
                self.cbServe.Enable(False)
                self.pnXmlLocks.SetLocks('',[])
                self.vtLastAccess.SetValue('')
                self.vtLastUse.SetValue('')
                self.vtLastSave.SetValue('')
            else:
                self.cbServe.Enable(True)
                sTag=self.objRegNode.GetTag(self.node)
                sTagAppl=o.GetTag(nodePar)
                alias=':'.join([sTagAppl,sTag])
                srvXml=netSrv.GetDataInstance()
                bRet=srvXml.IsServedXml(alias)
                if bRet:
                    self.cbServe.SetValue(True)
                    lLocks=srvXml.GetLockLst(alias)
                    self.pnXmlLocks.SetLocks(alias,lLocks)
                    lConns=srvXml.GetServedXmlConnections(alias)
                    for t in lConns:
                        idx=self.lstConn.InsertStringItem(sys.maxint,str(t[0]))
                        for i in xrange(1,5):
                            self.lstConn.SetStringItem(idx,i,str(t[i]))
                    del lConns
                else:
                    self.cbServe.SetValue(False)
                    self.pnXmlLocks.SetLocks(alias,[])
                zLast=srvXml.GetServedXmlInfo(alias,'last_access')
                zSave=srvXml.GetServedXmlInfo(alias,'last_save')
                zUse=srvXml.GetServedXmlInfo(alias,'last_modify')
                def conv(zTmp):
                    if zTmp is None:
                        return ''
                    return time.strftime('%Y-%m-%dT%H:%M:%S',time.localtime(zTmp))
                self.vtLastAccess.SetValue(conv(zLast))
                self.vtLastUse.SetValue(conv(zUse))
                self.vtLastSave.SetValue(conv(zSave))
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viTag.GetNode(node)
            self.viName.GetNode(node)
            self.objRegNode.SetFN(node,self.fbbFN.GetValue())
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viName.Close()
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            # add code here
            self.viTag.Enable(False)
            self.viName.Enable(False)
        else:
            # add code here
            self.viTag.Enable(True)
            self.viName.Enable(True)
    def SetNetSrv(self,netSrv):
        self.netSrv=netSrv
    def __showSrvErr__(self,sMsg):
        self.ShowMsgDlg(self.OK|self.EXCLAMATION,sMsg,
                            u'vXmlNodeMsgSrvAlias',None)
    def OnCbAclButton(self, event):
        event.Skip()
        try:
            if self.sAclSecName is not None:
                sMsg=_(u'acl alias already in use.')
                self.__logPrintMsg__(sMsg)
                self.__logError__('acl alias name already in use:%s'%(self.sAclSecName))
                try:
                    self.dlgSecAcl.SetFocus()
                except:
                    pass
                return
            o,nodeSrv,nodePar=self.objRegNode.GetServerObjNode(self.node)
            if o is None:
                self.__logError__('server node not found;objReg:%s'%(self.doc.getTagName(self.node)))
                return
            netSrv=self.doc.GetServerInstanceByNode(nodeSrv)
            if netSrv is None:
                sMsg=_(u'Server not started yet!')
                self.__logPrintMsg__(sMsg)
                #self.__showSrvErr__(sMsg)
                self.__logError__('server instance not found')
                return
            if self.dlgSecAcl is None:
                #self.dlgSecAcl=vSecAclDialog(self)
                self.dlgSecAcl=vtSecAclDomDialog(self)
                EVT_VTSEC_ACLDOM_PANEL_APPLIED(self.dlgSecAcl,self.OnSecAclApplied)
                EVT_VTSEC_ACLDOM_PANEL_CANCELED(self.dlgSecAcl,self.OnSecAclCanceled)
            sTag=self.objRegNode.GetTag(self.node)
            sTagAppl=o.GetTag(nodePar)
            self.sAclSecName=':'.join([sTagAppl,sTag])
            srvXml=netSrv.GetDataInstance()
            bRet=srvXml.IsServedXml(self.sAclSecName)
            if bRet:
                doc=srvXml.GetServedXmlDom(self.sAclSecName)
                if self.__isLogDebug__():
                    self.__logDebug__('found:%s'%self.sAclSecName)
            else:
                sMsg=_(u'Alias has to be started!')
                self.__logPrintMsg__(sMsg)
                #dlg=wx.MessageDialog(self,sMsg ,
                #            u'vXmlNodeMsgSrvAlias',
                #            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                #dlg.ShowModal()
                #dlg .Destroy()
                self.sAclSecName=None
                return

            self.dlgSecAcl.Centre()
            docHum=self.netMaster.GetNetDoc('vHum')
            #humDocs=[docHum]
            node=doc.getChildForced(doc.getRoot(),'security_acl')
            self.dlgSecAcl.SetDoc(doc)
            lHum=srvXml.GetHumDoc4SecAclLst()
            self.dlgSecAcl.SetDocHums(lHum,False)
            self.dlgSecAcl.SetNode(node)
            iRet=self.dlgSecAcl.Show(True)
            
        except:
            self.__logTB__()

    def OnSecAclApplied(self,event):
        event.Skip()
        try:
            if self.sAclSecName is not None:
                o,nodeSrv,nodePar=self.objRegNode.GetServerObjNode(self.node)
                if o is None:
                    return
                sTag=self.objRegNode.GetTag(self.node)
                sTagAppl=o.GetTag(nodePar)
                sAclSecName=':'.join([sTagAppl,sTag])
                
                netSrv=self.doc.GetServerInstanceByNode(nodeSrv)
                srvXml=netSrv.GetDataInstance()
                
                try:
                    if hasattr(self.doc,'docLogin'):
                        oSrv=self.doc.GetRegByNode(nodeSrv)
                        sSrvSub=oSrv.GetTag(nodeSrv)
                        nodeSrvMain=self.doc.getParent(nodeSrv)
                        oSrvMain=self.doc.GetRegByNode(nodeSrvMain)
                        sSrvMain=oSrvMain.GetTag(nodeSrvMain)
                        docLogin=self.doc.docLogin
                        oSrv.UpdateLoginPossible(self.node,srvXml,
                                    sTagAppl,nodeSrv,docLogin)
                        docLogin.Save()
                        self.doc.BuildLoginCache()
                        oSrv.SetLoginCache(srvXml,sSrvMain,sSrvSub)
                except:
                    self.__logTB__()
                
                #srvXml=self.netSrv.GetDataInstance()
                srvXml.ModifyServedXmlAcl(self.sAclSecName)
        except:
            self.__logTB__()
        self.sAclSecName=None
        
    def OnSecAclCanceled(self,event):
        event.Skip()
        self.sAclSecName=None

    def OnCbCfgButton(self, event):
        event.Skip()
        try:
            o,nodeSrv,nodePar=self.objRegNode.GetServerObjNode(self.node)
            if o is None:
                return
            netSrv=self.doc.GetServerInstanceByNode(nodeSrv)
            if netSrv is None:
                return
            #if self.sConfigName is not None:
            #    return
            if self.dlgConfig is None:
                #self.dlgSecAcl=vSecAclDialog(self)
                self.dlgConfig=vsNetXmlConfigDialog(self)
                self.dlgConfig.SetupImages(self.doc.GetPluginImages(bIncludeSrv=True))
            sTag=self.objRegNode.GetTag(self.node)
            sTagAppl=o.GetTag(nodePar)
            self.sConfigName=':'.join([sTagAppl,sTag])
            srvXml=netSrv.GetDataInstance()
            bRet=srvXml.IsServedXml(self.sConfigName)
            if bRet:
                doc=srvXml.GetServedXmlDom(self.sConfigName)
                if self.__isLogDebug__():
                    self.__logDebug__('found:%s'%self.sConfigName)
            else:
                sMsg=_(u'Alias has to be started!')
                self.__logPrintMsg__(sMsg)
                #dlg=wx.MessageDialog(self,sMsg ,
                #            u'vXmlNodeMsgSrvAlias',
                #            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                #dlg.ShowModal()
                #dlg .Destroy()
                self.sConfigName=None
                return
            self.dlgConfig.Centre()
            node=doc.getChildForced(doc.getRoot(),'config')
            self.dlgConfig.SetNode(doc,node,self.sConfigName,doc.APPL_REF)
            iRet=self.dlgConfig.ShowModal()
            if iRet>0:
                self.dlgConfig.GetNode()
        except:
            self.__logTB__()

    def OnCbSaveButton(self, event):
        event.Skip()
        try:
            o,nodeSrv,nodePar=self.objRegNode.GetServerObjNode(self.node)
            if o is None:
                return
            netSrv=self.doc.GetServerInstanceByNode(nodeSrv)
            if netSrv is None:
                return
            sTag=self.objRegNode.GetTag(self.node)
            o=self.doc.GetRegByNode(nodePar)
            sTagAppl=o.GetTag(nodePar)
            sApplName=':'.join([sTagAppl,sTag])
            srvXml=netSrv.GetDataInstance()
            bRet=srvXml.IsServedXml(sApplName)
            if bRet:
                srvXml.SaveXml(sApplName)
            else:
                sMsg=_(u'Alias has to be started!')
                self.__logPrintMsg__(sMsg)
                #dlg=wx.MessageDialog(self,sMsg ,
                #            u'vXmlNodeMsgSrvAlias',
                #            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                #dlg.ShowModal()
                #dlg .Destroy()
                return        
        except:
            self.__logTB__()
    def OnCbConnCloseButton(self, event):
        event.Skip()
        idx=-1
        for i in xrange(self.lstConn.GetItemCount()):
            if self.lstConn.GetItemState(i,wx.LIST_STATE_SELECTED):
                idx=i
                break
        if idx>=0:
            self.__logInfo__('idx:%d'%(idx))
            sHost=self.lstConn.GetItem(idx,1).m_text
            sPort=self.lstConn.GetItem(idx,2).m_text
            sTag=self.objRegNode.GetTag(self.node)
            o,nodeSrv,nodePar=self.objRegNode.GetServerObjNode(self.node)
            if o is None:
                return
            netSrv=self.doc.GetServerInstanceByNode(nodeSrv)
            if netSrv is None:
                pass
            else:
                sTagAppl=o.GetTag(nodePar)
                alias=':'.join([sTagAppl,sTag])
                srvXml=netSrv.GetDataInstance()
                srvXml.CloseConnection(alias,sHost,int(sPort))
            
    def OnCbServeButton(self, event):
        event.Skip()
        try:
            if self.doc is None:
                return
            if self.node is None:
                return
            bServe=self.cbServe.GetValue()
            sTag=self.objRegNode.GetTag(self.node)
            self.__logInfo__('tag:%s;val:%d'%(sTag,bServe))
            o,nodeSrv,nodePar=self.objRegNode.GetServerObjNode(self.node)
            if o is None:
                return
            netSrv=self.doc.GetServerInstanceByNode(nodeSrv)
            if netSrv is None:
                self.cbServe.SetValue(False)
                self.cbServe.Enable(False)
            else:
                self.cbServe.Enable(True)
                sTagAppl=o.GetTag(nodePar)
                alias=':'.join([sTagAppl,sTag])
                srvXml=netSrv.GetDataInstance()
                bRet=srvXml.IsServedXml(alias)
                if bRet:
                    if bServe==False:
                        #srvXml.StopServedXml(alias)   # wor 060821
                        srvXml.SaveXml(alias)
                        srvXml.DelServedXml(alias)
                    else:
                        #srvXml.StartServedXml(alias)   # wor 060821
                        pass
                else:
                    doc=self.objRegNode.GetDoc2Serve(self.node,appl=alias)
                    fn=self.objRegNode.GetFN(self.node)
                    srvXml.AddServedXml(alias,fn,doc,[])
                    hosts=self.objRegNode.GetHosts(self.node)
                    srvXml.SetServedHosts(alias,hosts)
        except:
            self.__logTB__()

    def OnCbHostButton(self, event):
        event.Skip()
        try:
            sTag=self.objRegNode.GetTag(self.node)
            self.__logDebug__('alias:%s'%(sTag))
            srvXml,sAppl=self.__getSrvXmlAppl__()
            if srvXml is not None:
                sAlias=':'.join([sAppl,sTag])
                bRet=srvXml.IsServedXml(sAlias)
                if bRet:
                    lHost=self.objRegNode.GetHosts(self.node)
                    self.__logDebug__('hosts:%s'%(self.__logFmt__(lHost)))
                    srvXml.DoCmdThreadSafe(sAlias,'SetServedHosts',sAlias,lHost)
                    #srvXml.ForceLock(alias,None,sId)
        except:
            self.__logTB__()

