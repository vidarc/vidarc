#----------------------------------------------------------------------------
# Name:         vsNetXmlNodeSrvAliasSummary.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20061015
# CVS-ID:       $Id: vsNetXmlNodeSrvAliasSummary.py,v 1.3 2009/01/25 12:47:48 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vSrv.net.vsNetXmlNodeSrvAlias import vsNetXmlNodeSrvAlias
try:
    if vcCust.is2Import(__name__):
        from vsNetXmlNodeSrvAliasSummaryPanel import vsNetXmlNodeSrvAliasSummaryPanel
        #from vsNetXmlNodeSrvAliasEditDialog import *
        #from vsNetXmlNodeSrvAliasAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vsNetXmlNodeSrvAliasSummary(vsNetXmlNodeSrvAlias):
    def __init__(self,tagName='SrvAlias'):
        global _
        _=vtLgBase.assignPluginLang('vSrvNet')
        vsNetXmlNodeSrvAlias.__init__(self,tagName)
    def GetDescription(self):
        return _(u'server alias')
    # ---------------------------------------------------------
    # specific
    def SetAliasSummarized(self,node,l):
        for c in self.doc.getChilds(node,'not_summarized'):
            self.doc.deleteNode(c,node)
        for s in l:
            self.doc.createSubNodeText(node,'not_summarized',s)
    def GetAliasSummarized(self,node):
        l=[]
        for c in self.doc.getChilds(node,'not_summarized'):
            l.append(self.doc.getText(c))
        return l
    def GetServerObjNodeAlias(self,node):
        nodePar=self.doc.getParent(node)
        if nodePar is None:
                return None,None,None
        o=self.doc.GetRegByNode(nodePar)
        if hasattr(o,'GetServerObjNode'):
            return o.GetServerObjNode(nodePar)
        return o,nodePar,nodePar
    def Setup(self,node,dataInst,appl):
        alias=self.GetTag(node)
        applAlias=':'.join([appl,alias])
        vtLog.vtLngCurCls(vtLog.INFO,'alias:%s'%(applAlias),self)
        if dataInst.IsServedXml(applAlias)==False:
            vtLog.vtLngCurCls(vtLog.ERROR,'alias:%s not served'%(applAlias),self)
        docAliases=dataInst.GetServedXmlDom(applAlias)
        #nodeAliasesBase=docAliases.getBaseNode()
        docAliases.BuildCache()
        o,nodeSrv,nodePar=self.GetServerObjNode(node)
        oRegCont=self.doc.GetRegByNode(nodePar)
        lSum=self.GetAliasSummarized(node)
        sAliasSum=oRegCont.GetAliasName()
        return #FIXME
        l=[]
        self.doc.procChildsKeys(nodeSrv,self.__procAliasSum__,sAliasSum,l)
        l.sort()
        for s in l:
            if s not in lSum:
                # add a;ias
                aliasSummarized=':'.join([sAliasSum,s])
                bRet=dataInst.IsServedXml(aliasSummarized)
                if bRet:
                    docSum=dataInst.GetServedXmlDom(aliasSummarized)
                    docAliases.AddSummarizedByDoc(aliasSummarized,docSum)
            else:
                # del alias
                pass
    def __procAliasSumChild__(self,node,l):
        oReg=self.doc.GetRegByNode(node)
        sAlias=oReg.GetTag(node)
        l.append(sAlias)
        return 0
    def __procAliasSum__(self,node,sAliasSum,l):
        if self.doc.getTagName(node)==sAliasSum:
            self.doc.procChildsKeys(node,self.__procAliasSumChild__,l)
        return 0

    # ---------------------------------------------------------
    # inheritance
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01<IDAT8\x8d\x8d\x93\xbdK\xc3P\x14\xc5\x7f/\xba\xe8b\xadt\xeelK]\xfcX\
\xcd \xe2\x1c\x1a2\xfao\x88\x0e/YJqT\xf0\x7f\x08\x95BA\xdd\x84d\xe9\xd2E\x90\
\xd6\xc9\xc1\xcdF\x10A\xe8Z\x87|\xd0\xf4\xbd\x87=\xd3{\xf7\xdes\xeey\xc9\xbd\
BXk,\xa3\xd5m\xcf\x95 \xf0zq/\x96c\xd6\xaa\x85\xba\x18\x80\xc8\x1d\x98\xba\
\x9a\x90\x0bj\x1d,\xe2`l\xb15\xf80\xe6\xd7M\tg\x16"ez\x0e\x02`vF\x7f\xf3M\
\xa9\xb3t\xd6o\x8ev\x91\xd2/\xeeR\x82]\xf3\xa9>MKu\xadn{.\xf6\xae\xbd\x92\
\x803\n\x91nvi\xfa\xd0\x88 \x8e \x81\xab\x97K\x1e*\xefe\x07\xcb\xdd\xa5\x0b\
\xf1$\xb5\x1d\xf7|\x88\x81$\xcd\r\x07\x1d\xf5\tJ$\xc3\x0f\x10\x8d\x81$\x02 \
\xb8\x83\xefso5\x81h\x0c\x95\x1a\xd8M\n\xb2t\xa1\xfa8Uj-(\x0fI\xd0K\x896@\
\x04\xc1\xc4O\x9f\xf5\x05\xf5z\xbd \xe6\x1c\xed 9\xa3\xb0\xe8~\xdcH\xbfIgx\
\xca\xe7\xc9\xb6"\xa0\x9d\x83\xfe\xa1G?\x13\xba}\xde\xe1wc\xbfD^\x84q\x90r\
\xa1\xff t\xdb\x08\xean\x98\x96I\xfb\x17t\xd3iZ\xb6?\xdd>hR\xb6\xda\x84F\x00\
\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetPanelClass(self):
        if GUI:
            return vsNetXmlNodeSrvAliasSummaryPanel
        else:
            return None
