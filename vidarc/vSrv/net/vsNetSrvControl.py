#----------------------------------------------------------------------
# Name:         vvsNetSrvControl.py
# Purpose:      server control with tree popup window
#
# Author:       Walter Obweger
#
# Created:      20060703
# CVS-ID:       $Id: vsNetSrvControl.py,v 1.5 2010/03/10 22:41:57 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import wx
    import wx.lib.buttons

    import cStringIO

    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.log.vtLog as vtLog

    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel
    from vidarc.tool.net.vtNetSrv import *
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

#----------------------------------------------------------------------
def getStartData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\x1aIDAT8\x8d\xc5\x93\xbbJ\x03a\x10\x85\xbf\xc9\xeez[H\x14Ql\x12\x88\
\xa4\x13\\DD\x1b;_\xc0\xd6\xceV\xf0\tl}\x01\x1f!\x9d\xb5\x95\x8d\xa0\x88`\
\xa9M\x1a\x89A\xac\x14\x14\x137\x9b\xb8\xff\xfe\x19\xabl\n/IH\x91\xa9N3\x1f\
\x873gD2\x0e\xa3Lf\xa4m\xc0\xed\n=\xb1\xca\x07P\x06ytdh\x07\xb9i\x9f\xc5|\
\x0e\xf6A\x0f\xac\x0e\r\x10\x85\xdd\x8d-\n\xc5,\xb3+\x93\xe8\x91U\xdd\xeb\
\x0fJ\x01&NX/\x15\x91\x8c\x90_\xca\xb2\x1a,\xe0\x07\x13\xe8\xa1U\xdd\xfe\x1b\
\x94f\x80Q\xc2\xe8\x8d\x9d`\x8dJ\xad\xca\x94\xe7P\xd8\x9c\xe3\xbd\x14q7\xffB\
\xb4lT\xca?\xb3\xe9\x85\xd8\x11Lbh\xb6\x0c\xb5\xd7:Il\x89cK\x1cZL#\x81\xa7~\
\x0e\xac\xe293\x9c^\xdd@\x07\xf8\x02>\x81{\xe0\x02\xc4\xfc~\x19\xb7\'\x84\
\xcb\xca\x03\x18\xa0\t\xd4\x803\x90\xc6\xff\'\x95n\x13\xfdcO\xad\x0b\xadj\
\x1b\xceA\x9e\x07\xebB\xea \xac\xb7\xe1\x1a\xb8\x05a\xf0"\xc9\xd8\x7fa\xfc\
\x80o\'\xc8a\xc3\xd8~\xca\xc2\x00\x00\x00\x00IEND\xaeB`\x82' 

def getStartBitmap():
    return wx.BitmapFromImage(getStartImage())

def getStartImage():
    stream = cStringIO.StringIO(getStartData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getStopData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01[IDAT8\x8d\xc5\x93\xbbJ\x03A\x14\x86\xbf\xc9&nL\x14\x04u\xed\xb4\x10\
R\x18Q\x1b\xb1V\x0b\xb1Li\'\xe4\x85|\x02\x9f@+km\x83\x8d6i\x8c\x17PD!$$1{\
\x9b\xdd\x99\x1d\x8b@\xbclRH\nO9\xf0\x7f\xf0\x7f\xe7\x8c\x10\x19\x8bI&3Q\x1a\
\xc8\xfe~\xf0K\xda\xb4CPS\x82PA\xf3\xc3\xf0\x10C\xb5o\x89Q\x00\xf1\xbdB\xb4\
\xa5\x8dYr\xb0\x0e\xf6\xc8\xd8\xd3\xd0\xf3\x89_\xde\xb8\xbf\xba\xe5\xfa\xd5\
\xa5\xea\xa7!\xc3\n~I\x1b\x1c\x87\xdc\xf1\x11\x99\xf99\xd0\x1a\x94"7Sdm\xb7\
\xcc\xe6b\x9e\x8a\xa5\xcdX@+\x00q\xb8\x0fRB\xac@\'`\x0c(\r\x91\xa2\\v\xd8\
\x19al\xe8@\xd9\x02\xabP\x007\x18\x84\x02\t2\x06\x15\x81R\xd86,\xe7\x80x\x0c\
 \x8c\x80\x8e\x07JA\x92\x0c\xc2~\x00a\x002\x808"N\xd2\x1e\x87\x80\xa6kX}~\'7\
[\x18\xf4\x8f"\x08<\xf0\\\x08|\xc2\xb6G]\xa6\x14|9x\x8c\xa1qy\x03\xfd\x1et;\
\xd0\xedB\xaf\x0f\xae\x07\xd2\xa7v\xe7s\x96\xa4\x1d\xfcX\xe3iQ\x9b\x8d\x85<\
\xe5u\x07;/@F\x84m\x8fZ\xc3\xe3\xa4\xa5\xb9\x10\xe95\x8a\xdf\xa7\\\xb1\xb4\
\xd9\x16\xb0\x92\x05\x85\xa0.\r\xe7\t<\x8d\x08\x8f\x04\xfcu&\xfe\x0b\xff\x0f\
\xf8\x04eW\x99\x1b\xe0)I@\x00\x00\x00\x00IEND\xaeB`\x82' 

def getStopBitmap():
    return wx.BitmapFromImage(getStopImage())

def getStopImage():
    stream = cStringIO.StringIO(getStopData())
    return wx.ImageFromStream(stream)

wxEVT_VSNET_SRVCONTROL_STARTED=wx.NewEventType()
vEVT_VSNET_SRVCONTROL_STARTED=wx.PyEventBinder(wxEVT_VSNET_SRVCONTROL_STARTED,1)
def EVT_VSNET_SRVCONTROL_STARTED(win,func):
    win.Connect(-1,-1,wxEVT_VSNET_SRVCONTROL_STARTED,func)
class vsNetSrvControlStarted(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VSNET_SRVCONTROL_STARTED(<widget_name>, self.OnStarted)
    """

    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VSNET_SRVCONTROL_STARTED)

wxEVT_VSNET_SRVCONTROL_STOPPED=wx.NewEventType()
vEVT_VSNET_SRVCONTROL_STOPPED=wx.PyEventBinder(wxEVT_VSNET_SRVCONTROL_STOPPED,1)
def EVT_VSNET_SRVCONTROL_STOPPED(win,func):
    win.Connect(-1,-1,wxEVT_VSNET_SRVCONTROL_STOPPED,func)
class vsNetSrvControlStopped(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_VSNET_SRVCONTROL_STOPPED(<widget_name>, self.OnStopped)
    """

    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VSNET_SRVCONTROL_STOPPED)

class vsNetSrvControlTransientPopup(wx.Dialog):
    def __init__(self, parent, size,style,name=''):
        self.SIZE_PN_SCROLL=wx.SystemSettings.GetMetric(wx.SYS_VSCROLL_X)
        wx.Dialog.__init__(self, parent,style=wx.RESIZE_BORDER)#wx.BORDER_SIMPLE)#|wx.STAY_ON_TOP)
        id=wx.NewId()
        wx.EVT_SIZE(self,self.OnSize)
        self.cbCancel = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Cancel), name=u'cbCancel',
              parent=self, pos=(0,0), size=(30,30), style=wx.BU_AUTODRAW)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              self.cbCancel)
              
        self.cbApply = wx.BitmapButton(id=-1,
              bitmap=vtArt.getBitmap(vtArt.Apply), name=u'cbApply',
              parent=self, pos=wx.Point(32, 00), size=wx.Size(30, 30),
              style=wx.BU_AUTODRAW)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              self.cbApply)
        if size[1]<40:
            size=(size[0],40)
        i=0
        iY=40
        iH=size[1]+4
        iSzHeight=0
        id=wx.NewId()
        self.lstConnData = wx.ListCtrl(id=id, name=u'lstDataConn',
              parent=self, pos=wx.Point(4, 40), size=wx.Size(222,86),
              style=wx.LC_REPORT)
        #self.lstStates.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstBitsListColClick,id=id)
        #self.lstStates.Bind(wx.EVT_LIST_ITEM_DESELECTED,self.OnLstBitsListItemDeselected, id=id)
        #self.lstStates.Bind(wx.EVT_LIST_ITEM_SELECTED,self.OnLstBitsListItemSelected, id=id)
        self.lstConnData.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
                                    heading=_('addr'), width=80)
        self.lstConnData.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT,
                                    heading=_('port'), width=60)
        self.lstConnData.InsertColumn(col=3, format=wx.LIST_FORMAT_LEFT,
                                    heading=_('login'), width=80)
        self.selIdx=-1
        
        self.szOrig=(240,140)
        self.SetSize(self.szOrig)
    def OnLstConnDataListColClick(self,evt):
        self.selIdx=-1
    def OnLstConnDataListItemDeselected(self,evt):
        self.selIdx=-1
    def OnLstConnDataListItemSelected(self,evt):
        self.selIdx=evt.GetIndex()
    def OnSize(self,evt):
        iW,iH=self.GetSize()
        if iW<self.szOrig[0]:
            iW=self.szOrig[0]
        #if iH<self.szMin[1]:
        #    iH=self.szMin[1]
        if iH<self.szOrig[1]:
            iH=self.szOrig[1]
        self.SetSize((iW,iH))
        #if self.trInfo is not None:
        #    self.trInfo.SetSize((iW-16,iH-52))
        evt.Skip()
    def OnCbCancelButton(self,evt):
        self.Show(False)
    def OnCbApplyButton(self,evt):
        try:
            self.Apply()
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        self.Show(False)
    def Show(self,flag):
        try:
            if flag:
                par=self.GetParent()
                self.lstConnData.DeleteAllItems()
            else:
                par=self.GetParent()
                par.cbPopup.SetValue(False)
        except:
            vtLog.vtLngTB(self.GetParent().GetName())
        wx.Dialog.Show(self,flag)
    def Apply(self):
        try:
            if self.selIdx<0:
                return
            par=self.GetParent()
        except:
            vtLog.vtLngTB(self.GetParent().GetName())

class vsNetSrvControl(wx.Panel,vtXmlNodePanel):
    def __init__(self,*_args,**_kwargs):
        try:
            sz=_kwargs['size']
        except:
            sz=wx.Size(80,30)
            _kwargs['size']=sz
        try:
            self.dataClass=_kwargs['data_class']
            del _kwargs['data_class']
        except:
            self.dataClass=None
        try:
            self.dataSrvClass=_kwargs['data_srv_class']
            del _kwargs['data_srv_class']
        except:
            self.dataSrvClass=vtSrvSock
        try:
            self.serviceClass=_kwargs['service_class']
            del _kwargs['service_class']
        except:
            self.serviceClass=vtServiceSock
        try:
            self.serviceSrvClass=_kwargs['service_srv_class']
            del _kwargs['service_srv_class']
        except:
            self.serviceSrvClass=vtSrvSock
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        self.SetAutoLayout(True)
        
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        id=wx.NewId()
        self.cbStart=wx.lib.buttons.GenBitmapToggleButton(ID=id,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbStart',
              parent=self, pos=(0,0), size=(30,30), style=wx.BU_AUTODRAW)
        self.cbStart.Bind(wx.EVT_BUTTON,self.OnStartButton,self.cbStart)
        self.cbStart.SetBitmapLabel(getStopBitmap())
        self.cbStart.SetBitmapSelected(getStartBitmap())
        bxs.AddWindow(self.cbStart, 1, border=0, flag=wx.EXPAND|wx.ALL)
        
        id=wx.NewId()
        self.cbPopup = wx.lib.buttons.GenBitmapToggleButton(ID=id,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbPopup',
              parent=self, pos=(40,0), size=(24,24), style=wx.BU_AUTODRAW)
        bxs.AddSpacer(wx.Size(4, 8), border=0, flag=0)
        bxs.AddWindow(self.cbPopup, 0, border=0, flag=wx.ALIGN_CENTER)
        self.SetSizer(bxs)
        self.cbPopup.SetBitmapLabel(vtArt.getBitmap(vtArt.Down))
        self.cbPopup.SetBitmapSelected(vtArt.getBitmap(vtArt.Down))
        self.cbPopup.Bind(wx.EVT_BUTTON,self.OnPopupButton,self.cbPopup)
        self.Layout()
        
        self.popWin=None
        self.netSrv=None
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.SetSuppressNetNotify(True)
        #vtLog.vtLngGetRelevantNamesWX(self)
    def Enable(self,flag):
        self.cbStart.Enable(flag)
        self.cbPopup.Enable(flag)
        self.Refresh()
    def GetNetSrv(self):
        return self.netSrv
    def SetNetSrv(self,srv):
        self.netSrv=srv
        if self.netSrv is not None:
            self.cbStart.SetValue(True)
    def SetDataClass(self,dataClass):
        self.dataClass=dataClass
    def SetDataSrvClass(self,dataSrvClass):
        self.dataSrvClass=dataSrvClass
    def SetServiceClass(self,serviceClass):
        self.serviceClass=serviceClass
    def SetServiceSrvClass(self,serviceSrvClass):
        self.serviceSrvClass=serviceSrvClass
    def __createPopup__(self):
        if self.__isLogDebug__():
            self.__logDebug__('__createPopup__')
        if self.popWin is None:
            sz=self.GetSize()
            sz=(sz[0],sz[1]-20)
            try:
                self.popWin=vsNetSrvControlTransientPopup(self,sz,wx.SIMPLE_BORDER)
                #self.popWin.SetDocTree(self.docTreeTup[0],self.docTreeTup[1])
                #self.popWin.SetNodeTree(self.nodeTree)
                #if self.trSetNode is not None:
                #    self.popWin.SetNode(self.trSetNode(self.doc,self.node))
            except:
                self.__logTB__()
            #self.popWin.SetVal(self.txtVal.GetValue(),self.doc.GetLang())
        else:
            pass
    def OnStartButton(self,evt):
        try:
            iStart=self.cbStart.GetValue()
            if self.__isLogInfo__():
                self.__logInfo__('start/stop(1/0):%d'%(iStart))
            if iStart:
                self.Start()
            else:
                self.Stop()
        except:
            self.__logTB__()
    def OnPopupButton(self,evt):
        self.__createPopup__()
        if self.cbPopup.GetValue()==True:
            btn=self
            iX,iY = btn.ClientToScreen( (0,0) )
            iW,iH =  btn.GetSize()
            iPopW,iPopH=self.popWin.GetSize()
            iScreenW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            iScreenH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            if iX+iPopW>iScreenW:
                iX=iScreenW-iPopW-4
                if iX<0:
                    iX=0
            iY+=iH
            if iY+iPopH>iScreenH:
                iY=iScreenH-iPopH
                if iY<0:
                    iY=0
            
            self.popWin.Move((iX,iY))
            #self.popWin.Move((pos[0],pos[1]+sz[1]))
            self.popWin.Show(True)
        else:
            self.popWin.Show(False)
        evt.Skip()
    def Stop(self):
        if self.netSrv is None:
            return
        if self.node is None:
            if self.doc is None:
                return
            if self.doc.IsShutDown():
                return
            self.__logError__('node not set')
            return
        try:
            self.doc.DoSafe(self.doStop,self.doc.getKeyNum(self.node))
        except:
            self.__logTB__()
        return
        dataInst=self.netSrv.GetDataInstance()
        #self.objRegNode.Stop(self.node,dataInst)
        self.objRegNode.StopServer(self.node)
        
        #self.netSrv.Stop()
        self.netSrv=None
        wx.PostEvent(self,vsNetSrvControlStopped(self))
    def doStop(self,iId):
        try:
            node=self.doc.getNodeByIdNum(iId)
            if node is None:
                self.__logError__('node not set')
            else:
                dataInst=self.netSrv.GetDataInstance()
                self.objRegNode.StopServer(node)
                self.netSrv=None
                wx.PostEvent(self,vsNetSrvControlStopped(self))
        except:
            self.__logTB__()
    def Start(self):
        if self.netSrv is not None:
            return
        if self.node is None:
            self.__logError__('node not set')
            return
        try:
            self.doc.DoSafe(self.doStart,self.doc.getKeyNum(self.node))
        except:
            self.__logTB__()
    def doStart(self,iId):
        try:
            node=self.doc.getNodeByIdNum(iId)
            if node is None:
                self.__logError__('node not set')
            else:
                self.netSrv=self.objRegNode.StartServer(node)
                wx.PostEvent(self,vsNetSrvControlStarted(self))
            return
            sTag=self.doc.getTagName(self.node)
            if sTag!=self.objRegNode.GetTagName():
                self.__logWarn__('tag:%s is not reg tag:%s'%(sTag,
                        self.objRegNode.GetTagName()))
                return
            iPortData=self.objRegNode.GetPortData(self.node)
            iPortService=self.objRegNode.GetPortService(self.node)
            sServicePasswd=self.objRegNode.GetServicePasswd(self.node)
            self.netSrv=vtNetSrv(self.dataSrvClass,iPortData,self.dataClass,
                                self.serviceSrvClass,iPortService,self.serviceClass,2,1)
            self.netSrv.SetServicePasswd(sServicePasswd)
            self.netSrv.Serve()
            dataInst=self.netSrv.GetDataInstance()
            self.objRegNode.Start(self.node,dataInst)
            wx.PostEvent(self,vsNetSrvControlStarted(self))
        except:
            self.__logTB__()
    def GetDataInstance(self):
        if self.netSrv is not None:
            return self.netSrv.GetDataInstance()
        return None
    def IsBusy(self):
        if self.netSrv is None:
            return False
        srv=self.netSrv.GetDataInstance()
        if srv.IsServing():
        #if self.netSrv.IsServing():
            self.__logDebug__('true')
            return True
        else:
            self.__logDebug__('false')
            return False
    def __Clear__(self):
        self.cbStart.SetValue(False)
        self.Enable(False)
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        
    def __SetNode__(self,node):
        try:
            if VERBOSE:
                self.__logDebug__(''%())
            if self.netSrv is not None:
                self.cbStart.SetValue(True)
            else:
                self.cbStart.SetValue(False)
            # add code here
            self.Enable(True)
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            #self.GetNodeFin(node)
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        if self.popWin is not None:
            if self.cbPopup.GetValue():
                self.popWin.Show(False)
                self.cbPopup.SetValue(False)
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            # add code here
            pass
        else:
            # add code here
            pass
            

