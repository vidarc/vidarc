#Boa:FramePanel:vXmlNodeConfigPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeConfigPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060705
# CVS-ID:       $Id: vXmlNodeConfigPanel.py,v 1.3 2010/03/10 22:41:57 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import wx
    import wx.lib.buttons

    import sys,sha

    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    import vidarc.vSrv.net.img_appl as img_appl
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VXMLNODECONFIGPANEL, wxID_VXMLNODECONFIGPANELCBADD, 
 wxID_VXMLNODECONFIGPANELCBDEL, wxID_VXMLNODECONFIGPANELCHCAPPL, 
 wxID_VXMLNODECONFIGPANELLBLALIAS, wxID_VXMLNODECONFIGPANELLBLAPPL, 
 wxID_VXMLNODECONFIGPANELLBLHOST, wxID_VXMLNODECONFIGPANELLBLPORT, 
 wxID_VXMLNODECONFIGPANELLSTHOST, wxID_VXMLNODECONFIGPANELTXTALIAS, 
 wxID_VXMLNODECONFIGPANELTXTHOST, wxID_VXMLNODECONFIGPANELTXTPORT, 
] = [wx.NewId() for _init_ctrls in range(12)]

class vXmlNodeConfigPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_bxsPort_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPort, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtPort, 2, border=0, flag=wx.EXPAND)

    def _init_coll_bxsHost_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblHost, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.txtHost, 2, border=0, flag=wx.EXPAND)

    def _init_coll_bxsAppl_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblAppl, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.chcAppl, 2, border=0, flag=wx.EXPAND)

    def _init_coll_bxsAlias_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblAlias, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtAlias, 2, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstHost, 0, border=4,
              flag=wx.LEFT | wx.TOP | wx.EXPAND)
        parent.AddWindow(self.cbDel, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsAppl, 1, border=4,
              flag=wx.LEFT | wx.TOP | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsHost, 0, border=4,
              flag=wx.LEFT | wx.TOP | wx.EXPAND)
        parent.AddWindow(self.cbAdd, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsPort, 0, border=4,
              flag=wx.TOP | wx.EXPAND | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsAlias, 0, border=4,
              flag=wx.TOP | wx.LEFT | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_lstHost_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'appl'), width=100)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'host'), width=100)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'port'), width=80)
        parent.InsertColumn(col=3, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'alias'), width=80)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=2, hgap=0, rows=4, vgap=0)

        self.bxsHost = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsPort = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsAlias = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsAppl = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsHost_Items(self.bxsHost)
        self._init_coll_bxsPort_Items(self.bxsPort)
        self._init_coll_bxsAlias_Items(self.bxsAlias)
        self._init_coll_bxsAppl_Items(self.bxsAppl)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODECONFIGPANEL,
              name=u'vXmlNodeConfigPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(312, 227),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 200))
        self.SetAutoLayout(True)

        self.lstHost = wx.ListCtrl(id=wxID_VXMLNODECONFIGPANELLSTHOST,
              name=u'lstHost', parent=self, pos=wx.Point(4, 4),
              size=wx.Size(261, 87), style=wx.LC_REPORT)
        self.lstHost.SetMinSize(wx.Size(-1, -1))
        self._init_coll_lstHost_Columns(self.lstHost)
        self.lstHost.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstHostListColClick,
              id=wxID_VXMLNODECONFIGPANELLSTHOST)
        self.lstHost.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstHostListItemDeselected,
              id=wxID_VXMLNODECONFIGPANELLSTHOST)
        self.lstHost.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstHostListItemSelected,
              id=wxID_VXMLNODECONFIGPANELLSTHOST)

        self.cbDel = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODECONFIGPANELCBDEL,
              bitmap=vtArt.getBitmap(vtArt.Del), name=u'cbDel', parent=self,
              pos=wx.Point(269, 4), size=wx.Size(31, 30), style=0)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VXMLNODECONFIGPANELCBDEL)

        self.lblAppl = wx.StaticText(id=wxID_VXMLNODECONFIGPANELLBLAPPL,
              label=u'appl', name=u'lblAppl', parent=self, pos=wx.Point(4, 95),
              size=wx.Size(83, 21), style=wx.ALIGN_RIGHT)

        self.chcAppl = wx.Choice(choices=[], id=wxID_VXMLNODECONFIGPANELCHCAPPL,
              name=u'chcAppl', parent=self, pos=wx.Point(91, 95),
              size=wx.Size(174, 21), style=0)

        self.cbAdd = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODECONFIGPANELCBADD,
              bitmap=vtArt.getBitmap(vtArt.Add), name=u'cbAdd', parent=self,
              pos=wx.Point(269, 120), size=wx.Size(31, 30), style=0)
        self.cbAdd.Bind(wx.EVT_BUTTON, self.OnCbAddButton,
              id=wxID_VXMLNODECONFIGPANELCBADD)

        self.txtHost = wx.TextCtrl(id=wxID_VXMLNODECONFIGPANELTXTHOST,
              name=u'txtHost', parent=self, pos=wx.Point(91, 120),
              size=wx.Size(174, 30), style=0, value=u'')

        self.lblHost = wx.StaticText(id=wxID_VXMLNODECONFIGPANELLBLHOST,
              label=_(u'host'), name=u'lblHost', parent=self, pos=wx.Point(4,
              120), size=wx.Size(83, 30), style=wx.ALIGN_RIGHT)
        self.lblHost.SetMinSize(wx.Size(-1, -1))

        self.lblPort = wx.StaticText(id=wxID_VXMLNODECONFIGPANELLBLPORT,
              label=u'port', name=u'lblPort', parent=self, pos=wx.Point(4, 154),
              size=wx.Size(83, 21), style=wx.ALIGN_RIGHT)
        self.lblPort.SetMinSize(wx.Size(-1, -1))

        self.lblAlias = wx.StaticText(id=wxID_VXMLNODECONFIGPANELLBLALIAS,
              label=u'alias', name=u'lblAlias', parent=self, pos=wx.Point(4,
              179), size=wx.Size(83, 21), style=wx.ALIGN_RIGHT)
        self.lblAlias.SetMinSize(wx.Size(-1, -1))

        self.txtPort = wx.TextCtrl(id=wxID_VXMLNODECONFIGPANELTXTPORT,
              name=u'txtPort', parent=self, pos=wx.Point(91, 154),
              size=wx.Size(174, 21), style=0, value=u'')

        self.txtAlias = wx.TextCtrl(id=wxID_VXMLNODECONFIGPANELTXTALIAS,
              name=u'txtAlias', parent=self, pos=wx.Point(91, 179),
              size=wx.Size(174, 21), style=0, value=u'')

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vSrvNet')
        self._init_ctrls(parent, id)
        self.docExt=None
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        
        self.imgLstAppl=wx.ImageList(16,16)
        
        self.dictImg={}
        for appl in ['Bill','Contact','Customer','Doc','Engineering','Globals',
                    'Hum','Loc','Prj','PrjDoc','PrjEng','PrjPlan','PrjTimer',
                    'Purchase','Ressource','Software','Supplier']:
            try:
                self.dictImg['v%s'%appl]=self.imgLstAppl.Add(getattr(img_appl,'get%sBitmap'%appl)())
            except:
                self.__logTB__()
        #self.lstHost.SetImageList(self.imgLstAppl,wx.IMAGE_LIST_NORMAL)
        self.lstHost.SetImageList(self.imgLstAppl,wx.IMAGE_LIST_SMALL)
        self.dCfg={}
        self.iSelHost=-1
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def __Clear__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.lstHost.DeleteAllItems()
        self.dCfg={}
        self.iSelHost=-1
        self.__clearInt__()
    def __clearInt__(self):
        self.txtHost.SetValue('')
        self.txtPort.SetValue('')
        self.txtAlias.SetValue('')
        self.ClrBlockDelayed()
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def SetDocServed(self,doc):
        self.docExt=doc
    def __showCfg__(self):
        self.lstHost.DeleteAllItems()
        self.iSelHost=-1
        keys=self.dCfg.keys()
        keys.sort()
        for k in keys:
            lHosts=self.dCfg[k]
            lHosts.sort()
            for tup in lHosts:
                idx=self.lstHost.InsertImageStringItem(sys.maxint, k, -1)
                self.lstHost.SetStringItem(idx,1,tup[0],-1)
                self.lstHost.SetStringItem(idx,2,tup[1],-1)
                self.lstHost.SetItemData(idx,3,tup[2],-1)
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.dCfg=self.objRegNode.GetCfg(self.node)
            self.__showCfg__()
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            #self.objRegNode.SetCfg(node,self.dCfg)
            
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            # add code here
            self.cbAdd.Enable(False)
            self.cbDel.Enable(False)
            self.lstHost.Enable(False)
            self.txtHost.Enable(False)
            self.txtPort.Enable(False)
            self.txtAlias.Enable(False)
        else:
            # add code here
            self.cbAdd.Enable(True)
            self.cbDel.Enable(True)
            self.lstHost.Enable(True)
            self.txtHost.Enable(True)
            self.txtPort.Enable(True)
            self.txtAlias.Enable(True)

    def OnLstHostListColClick(self, event):
        event.Skip()

    def OnLstHostListItemDeselected(self, event):
        event.Skip()
        self.iSelHost=-1

    def OnLstHostListItemSelected(self, event):
        event.Skip()
        self.iSelHost=event.GetIndex()

    def OnCbDelButton(self, event):
        event.Skip()
        if self.iSelHost>=0:
            del self.lHosts[self.iSelHost]

    def OnCbAddButton(self, event):
        event.Skip()
        sHost=self.txtHost.GetValue()
        sUser=self.txtUsr.GetValue()
        sPasswd=self.txtPasswd.GetValue()
        if len(sHost)==0:
            self.ShowMsgDlg(self.OK|self.EXCLAMATION,
                            _(u'Please enter a host name.') ,
                            'vXmlNodeHostsPanel',None)
            dlg.ShowModal()
            dlg.Destroy()
            return
        if len(sUser)==0:
            self.ShowMsgDlg(self.OK|self.EXCLAMATION,
                            _(u'Please enter a user name ("__srv__","__vHum__","xxx").') ,
                            'vXmlNodeHostsPanel',None)
            return
        if len(sPasswd)>0:
            m5=sha.new()
            m5.update(sPasswd)
            sPasswd=m5.hexdigest()
        else:
            sPasswd=''
        self.lHosts.append([sHost,sUser,sPasswd])
        self.__showHosts__()
