#----------------------------------------------------------------------------
# Name:         vsNetXmlLoginCacheNodeUsr.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20061227
# CVS-ID:       $Id: vsNetXmlLoginCacheNodeUsr.py,v 1.3 2010/03/06 20:31:06 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeBase import vtXmlNodeBase
try:
    if vcCust.is2Import(__name__):
        from vsNetXmlLoginCacheNodeUsrPanel import *
        from vsNetXmlLoginCacheNodeUsrEditDialog import *
        from vsNetXmlLoginCacheNodeUsrAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vsNetXmlLoginCacheNodeUsr(vtXmlNodeBase):
    NODE_ATTRS=[
            ('Tag',None,'tag',None),
            ('HumAlias',None,'aliasHum',None),
        ]
    FUNCS_GET_SET_4_LST=['Tag','HumAlias']
    def __init__(self,tagName='LoginCacheUsr'):
        global _
        _=vtLgBase.assignPluginLang('vSrvNet')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'login cached user')
    # ---------------------------------------------------------
    # specific
    def GetTag(self,node):
        return self.Get(node,'tag')
    #def GetName(self,node):
    #    return self.GetML(node,'name')
    def GetHumAlias(self,node):
        return self.Get(node,'aliasHum')
    def GetHum(self,node):
        return self.GetAttrStr(node,'fid')
    def GetHumKeyNum(self,node):
        return self.GetForeignKey(node,'fid','vHum')
    def SetTag(self,node,val):
        self.Set(node,'tag',val)
    def SetHumAlias(self,node,val):
        self.Set(node,'aliasHum',val)
    #def SetName(self,node,val):
    #    self.SetML(node,'name',val)
    def SetHum(self,node,val):
        return self.SetAttrStr(node,'fid',val)
    def SetHumKeyNum(self,node,val):
        return self.SetForeignKey(node,'fid',val,'vHum')
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtXmlNodeBase.ACL_MSK_NORMAL#vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x1cIDAT8\x8dcddbf\xa0\x040Q\xa4{\xd4\x80Q\x03F\r\x18D\x06\x00\x00]b\
\x00&\x87\xd5\x92\xeb\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return vsNetXmlLoginCacheNodeUsrEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return vsNetXmlLoginCacheNodeUsrAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vsNetXmlLoginCacheNodeUsrPanel
        else:
            return None
    def Build(self):
        """ this method is called automatically after the XML-file is read.
        """
        pass
