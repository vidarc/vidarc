#----------------------------------------------------------------------------
# Name:         vsNetXmlNodeAliasRef.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060711
# CVS-ID:       $Id: vsNetXmlNodeAliasRef.py,v 1.4 2010/03/06 20:31:06 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeBase import vtXmlNodeBase
try:
    if vcCust.is2Import(__name__):
        from vsNetXmlNodeAliasRefPanel import vsNetXmlNodeAliasRefPanel
        from vsNetXmlNodeAliasRefAddDialog import vsNetXmlNodeAliasRefAddDialog
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vsNetXmlNodeAliasRef(vtXmlNodeBase):
    NODE_ATTRS=[
            ('Tag',None,'tag',None),
            ('Name',None,'name',None),
        ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='AliasRef'):
        global _
        _=vtLgBase.assignPluginLang('vSrvNet')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'alias reference')
    # ---------------------------------------------------------
    # specific
    def GetTag(self,node):
        return self.Get(node,'tag')
    def GetName(self,node):
        return self.GetML(node,'name')
    def SetTag(self,node,val):
        self.Set(node,'tag',self.GetTagName())
    def SetName(self,node,val):
        self.SetML(node,'name',val)
    def GetRefServerInstanceByNode(self,node,sTagAliasRef='aliasRef'):
        if self.doc is None:
            return None
        if node is None:
            return None
        fid=self.GetChildAttr(node,sTagAliasRef,'fid')
        nodeRef=self.doc.getNodeByIdNum(fid)
        return self.__getServerInstanceByNode__(nodeRef)
    def __getServerInstanceByNode__(self,node):
        netSrv=self.doc.GetServerInstanceByNode(node)
        if netSrv is None:
            n=self.doc.getParent(node)
            if n is None:
                return None
            return self.__getServerInstanceByNode__(n)
        return netSrv
    def GetApplAlias(self,node,sTagAliasRef='aliasRef'):
        if self.doc is None:
            return None
        if node is None:
            return None
        fid=self.GetChildAttr(node,sTagAliasRef,'fid')
        nodeRef=self.doc.getNodeByIdNum(fid)
        if nodeRef is None:
            return None
        sAlias=self.doc.getNodeText(nodeRef,'tag')
        nodePar=self.doc.getParent(nodeRef)
        sAppl=self.doc.getNodeText(nodePar,'tag')
        return ':'.join([sAppl,sAlias])
    # ---------------------------------------------------------
    # inheritance
    def __createPost__(self,node,*args,**kwargs):
        self.SetTag(node,'')
    def GetPossibleAcl(self):
        return vtXmlNodeBase.ACL_MSK_ALL#vtSecXmlAclDom.ACL_MSK_ATTR
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return False
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xd9IDAT8\x8d\xc5S[\x0e\x84 \x0c\x9c\xaa\xa7\xd2\x18#\xe7\xf2\x81l\
\xcf\xe5\xc6\x10\xb9V\xf7c-!\xbb\xf0\xb1\xf1c\x9b4<\xa6\x94\x99R\x88\xaa\x1a\
w\xac\xbau\x1a@\x03\x00\x0f\xbbI\x0e\x9c\xedJ%\\1\xb0c\t>\x08U5Rg\xc7\x92\
\x8e\xa9\x07\x1f\x04\x80\xb0ci\x00\xa0\x1bz\xca\xdd\xa2{\x9fX7\xf4\x04@\xa2\
\x04\x00\x98\x96I\x19\x15\xf5jLZ\xf8&\r`\xc70\xa3\xd1\x1b\xbe\x8c\x1d\xcb\
\xa5?\x9f`\xb6+\x99\xd1H\xa9\xa8\x1a\x93\xaec\x82\x8b\xba\xec\xcf=R\xcd0\xc0\
\xc3n\x92&i\x00\xe0<\xbc\xa4\xb4\x83\x0f\xb2?\xf7x\xd0\x8c&\xce\xbb\xa1\xa7\
\xf3\xf0\xd2\xf6-\x00\x80T\xd7\xaf6-\xd3\x9b\xf5UQ\xc9\xb9\xf6@\t\x8b}@U\x9d\
\xed\x03\xd5Zz\xda\xd9\xaeD\x7f\xffL\xb7\x13\xbc\x00\x18sp\xe2\xf1\xf6\x1a`\
\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        return None
        if GUI:
            return vsNetXmlNodeXXXEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return vsNetXmlNodeAliasRefAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vsNetXmlNodeAliasRefPanel
        else:
            return None
