#Boa:FramePanel:vsNetXmlNodeAliasRefPanel
#----------------------------------------------------------------------------
# Name:         vsNetXmlNodeAliasRefPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060711
# CVS-ID:       $Id: vsNetXmlNodeAliasRefPanel.py,v 1.3 2010/03/10 22:41:57 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import wx
    import vidarc.tool.input.vtInputTree
    import vidarc.tool.input.vtInputTreeInternal
    
    import sys
    
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel
    
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VSNETXMLNODEALIASREFPANEL, wxID_VSNETXMLNODEALIASREFPANELLBLREF, 
 wxID_VSNETXMLNODEALIASREFPANELVIREF, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vsNetXmlNodeAliasRefPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(3)
        parent.AddGrowableCol(0)
        parent.AddGrowableCol(1)

    def _init_coll_bxsRef_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblRef, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viRef, 2, border=4, flag=wx.EXPAND)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsRef, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=4, vgap=0)

        self.bxsRef = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_bxsRef_Items(self.bxsRef)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VSNETXMLNODEALIASREFPANEL,
              name=u'vsNetXmlNodeAliasRefPanel', parent=prnt, pos=wx.Point(0,
              0), size=wx.Size(312, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 180))
        self.SetAutoLayout(True)

        self.lblRef = wx.StaticText(id=wxID_VSNETXMLNODEALIASREFPANELLBLREF,
              label=u'alias reference', name=u'lblRef', parent=self,
              pos=wx.Point(4, 4), size=wx.Size(94, 26), style=wx.ALIGN_RIGHT)
        self.lblRef.SetMinSize((-1,-1))

        self.viRef = vidarc.tool.input.vtInputTreeInternal.vtInputTreeInternal(id=wxID_VSNETXMLNODEALIASREFPANELVIREF,
              name=u'viRef', parent=self, pos=wx.Point(102, 4),
              size=wx.Size(197, 26), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vSrvNet')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.viRef.AddTreeCall('init','SetNodeInfos',['tag','name'])
        self.viRef.AddTreeCall('init','SetGrouping',[],[('tag',''),('name','')])
        self.viRef.SetTagNames2Base([])
        self.viRef.SetTagNames('aliasRef','tag')
        self.viRef.SetShowFullName(True)
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def __Clear__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viRef.Clear()
        
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viRef.SetDoc(doc,bNet)
        
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viRef.SetNode(node)
            
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viRef.GetNode(node)
            
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viRef.Close()
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viRef.Cancel()
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            # add code here
            self.viRef.Enable(False)
        else:
            # add code here
            self.viRef.Enable(True)
