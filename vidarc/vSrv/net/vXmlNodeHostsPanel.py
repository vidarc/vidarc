#Boa:FramePanel:vXmlNodeHostsPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeHostsPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060628
# CVS-ID:       $Id: vXmlNodeHostsPanel.py,v 1.3 2010/03/10 22:41:57 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import wx
    import wx.lib.buttons

    import sys,sha

    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VXMLNODEHOSTSPANEL, wxID_VXMLNODEHOSTSPANELCBADD, 
 wxID_VXMLNODEHOSTSPANELCBDEL, wxID_VXMLNODEHOSTSPANELLBLHOST, 
 wxID_VXMLNODEHOSTSPANELLBLPASSWD, wxID_VXMLNODEHOSTSPANELLBLUSR, 
 wxID_VXMLNODEHOSTSPANELLSTHOST, wxID_VXMLNODEHOSTSPANELTXTHOST, 
 wxID_VXMLNODEHOSTSPANELTXTPASSWD, wxID_VXMLNODEHOSTSPANELTXTUSR, 
] = [wx.NewId() for _init_ctrls in range(10)]

class vXmlNodeHostsPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_bxsUsr_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblUsr, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtUsr, 2, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstHost, 0, border=4,
              flag=wx.LEFT | wx.TOP | wx.EXPAND)
        parent.AddWindow(self.cbDel, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsHost, 0, border=4,
              flag=wx.LEFT | wx.TOP | wx.EXPAND)
        parent.AddWindow(self.cbAdd, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsUsr, 0, border=4,
              flag=wx.TOP | wx.EXPAND | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsPasswd, 0, border=4,
              flag=wx.TOP | wx.LEFT | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)

    def _init_coll_bxsPasswd_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPasswd, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtPasswd, 2, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_bxsHost_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblHost, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.txtHost, 2, border=0, flag=wx.EXPAND)

    def _init_coll_lstHost_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT, heading=_(u'host'),
              width=100)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT, heading=_(u'user'),
              width=80)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=2, hgap=0, rows=4, vgap=0)

        self.bxsHost = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsUsr = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsPasswd = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsHost_Items(self.bxsHost)
        self._init_coll_bxsUsr_Items(self.bxsUsr)
        self._init_coll_bxsPasswd_Items(self.bxsPasswd)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEHOSTSPANEL,
              name=u'vXmlNodeHostsPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(312, 216),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 189))
        self.SetAutoLayout(True)

        self.lstHost = wx.ListCtrl(id=wxID_VXMLNODEHOSTSPANELLSTHOST,
              name=u'lstHost', parent=self, pos=wx.Point(4, 4),
              size=wx.Size(261, 101), style=wx.LC_REPORT)
        self.lstHost.SetMinSize(wx.Size(-1, -1))
        self._init_coll_lstHost_Columns(self.lstHost)
        self.lstHost.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstHostListColClick,
              id=wxID_VXMLNODEHOSTSPANELLSTHOST)
        self.lstHost.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstHostListItemDeselected,
              id=wxID_VXMLNODEHOSTSPANELLSTHOST)
        self.lstHost.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstHostListItemSelected,
              id=wxID_VXMLNODEHOSTSPANELLSTHOST)

        self.cbDel = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODEHOSTSPANELCBDEL,
              bitmap=vtArt.getBitmap(vtArt.Del), name=u'cbDel', parent=self,
              pos=wx.Point(269, 4), size=wx.Size(31, 30), style=0)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VXMLNODEHOSTSPANELCBDEL)

        self.cbAdd = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODEHOSTSPANELCBADD,
              bitmap=vtArt.getBitmap(vtArt.Add), name=u'cbAdd', parent=self,
              pos=wx.Point(269, 109), size=wx.Size(31, 30), style=0)
        self.cbAdd.Bind(wx.EVT_BUTTON, self.OnCbAddButton,
              id=wxID_VXMLNODEHOSTSPANELCBADD)

        self.txtHost = wx.TextCtrl(id=wxID_VXMLNODEHOSTSPANELTXTHOST,
              name=u'txtHost', parent=self, pos=wx.Point(91, 109),
              size=wx.Size(174, 30), style=0, value=u'')

        self.lblHost = wx.StaticText(id=wxID_VXMLNODEHOSTSPANELLBLHOST,
              label=_(u'host'), name=u'lblHost', parent=self, pos=wx.Point(4,
              109), size=wx.Size(83, 30), style=wx.ALIGN_RIGHT)
        self.lblHost.SetMinSize(wx.Size(-1, -1))

        self.lblUsr = wx.StaticText(id=wxID_VXMLNODEHOSTSPANELLBLUSR,
              label=_(u'user'), name=u'lblUsr', parent=self, pos=wx.Point(4,
              143), size=wx.Size(83, 21), style=wx.ALIGN_RIGHT)
        self.lblUsr.SetMinSize(wx.Size(-1, -1))

        self.lblPasswd = wx.StaticText(id=wxID_VXMLNODEHOSTSPANELLBLPASSWD,
              label=_(u'password'), name=u'lblPasswd', parent=self,
              pos=wx.Point(4, 168), size=wx.Size(83, 21), style=wx.ALIGN_RIGHT)
        self.lblPasswd.SetMinSize(wx.Size(-1, -1))

        self.txtUsr = wx.TextCtrl(id=wxID_VXMLNODEHOSTSPANELTXTUSR,
              name=u'txtUsr', parent=self, pos=wx.Point(91, 143),
              size=wx.Size(174, 21), style=0, value=u'')

        self.txtPasswd = wx.TextCtrl(id=wxID_VXMLNODEHOSTSPANELTXTPASSWD,
              name=u'txtPasswd', parent=self, pos=wx.Point(91, 168),
              size=wx.Size(174, 21), style=wx.TE_PASSWORD, value=u'')

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vSrvNet')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        
        self.imgLstSec=wx.ImageList(16,16)
        self.imgLstSec.Add(vtArt.getBitmap(vtArt.SecLocked))
        self.imgLstSec.Add(vtArt.getBitmap(vtArt.SecUnlocked))
        self.dictImg={}
        self.lstHost.SetImageList(self.imgLstSec,wx.IMAGE_LIST_NORMAL)
        self.lstHost.SetImageList(self.imgLstSec,wx.IMAGE_LIST_SMALL)
        self.lHosts=[]
        self.iSelHost=-1
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def __Clear__(self):
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
        # add code here
        self.lstHost.DeleteAllItems()
        self.lHosts=[]
        self.iSelHost=-1
        self.__clearInt__()
    def __clearInt__(self):
        self.txtHost.SetValue('')
        self.txtUsr.SetValue('')
        self.txtPasswd.SetValue('')
        self.ClrBlockDelayed()
    def SetNetDocs(self,d):
        self.__logCritical__('FIXME'%())
        return
        if d.has_key('vHum'):
            dd=d['vHum']
            self.docHum=dd['doc']
        # add code here
        
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        
    def __showHosts__(self):
        self.lstHost.DeleteAllItems()
        self.iSelHost=-1
        self.lHosts.sort()
        for tup in self.lHosts:
            if len(tup[2])>0:
                i=0
            else:
                i=-1
            if tup[1] in ['__vHum__','__srv__']:
                i=0
            
            idx=self.lstHost.InsertImageStringItem(sys.maxint, tup[0], i)
            self.lstHost.SetStringItem(idx,1,tup[1],-1)
            #self.lstHost.SetItemData(idx,len(self.lstHostNode)-1)
    def __SetNode__(self,node):
        try:
            if VERBOSE or self.VERBOSE:
                self.__logDebug__(''%())
            
            # add code here
            self.lHosts=self.objRegNode.GetHosts(self.node)
            self.__showHosts__()
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE:
                self.__logDebug__(''%())
            # add code here
            self.objRegNode.SetHosts(node,self.lHosts)
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            # add code here
            self.cbAdd.Enable(False)
            self.cbDel.Enable(False)
            self.lstHost.Enable(False)
            self.txtHost.Enable(False)
            self.txtUsr.Enable(False)
            self.txtPasswd.Enable(False)
        else:
            # add code here
            self.cbAdd.Enable(True)
            self.cbDel.Enable(True)
            self.lstHost.Enable(True)
            self.txtHost.Enable(True)
            self.txtUsr.Enable(True)
            self.txtPasswd.Enable(True)

    def OnLstHostListColClick(self, event):
        event.Skip()

    def OnLstHostListItemDeselected(self, event):
        event.Skip()
        self.iSelHost=-1

    def OnLstHostListItemSelected(self, event):
        event.Skip()
        self.iSelHost=event.GetIndex()

    def OnCbDelButton(self, event):
        event.Skip()
        if self.iSelHost>=0:
            del self.lHosts[self.iSelHost]

    def OnCbAddButton(self, event):
        event.Skip()
        sHost=self.txtHost.GetValue()
        sUser=self.txtUsr.GetValue()
        sPasswd=self.txtPasswd.GetValue()
        if len(sHost)==0:
            dlg=wx.MessageDialog(self,_(u'Please enter a host name.') ,
                            'vXmlNodeHostsPanel',
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            return
        if len(sUser)==0:
            dlg=wx.MessageDialog(self,_(u'Please enter a user name ("__srv__","__vHum__","xxx").') ,
                            'vXmlNodeHostsPanel',
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            return
        if len(sPasswd)>0:
            m5=sha.new()
            m5.update(sPasswd)
            sPasswd=m5.hexdigest()
        else:
            sPasswd=''
        self.lHosts.append([sHost,sUser,sPasswd])
        self.__showHosts__()

