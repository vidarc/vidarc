#Boa:FramePanel:vInfoPanel
#----------------------------------------------------------------------------
# Name:         vInfoPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vInfoPanel.py,v 1.5 2006/06/07 10:35:09 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
from wx.lib.anchors import LayoutAnchors

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt

from vidarc.vSrv.vXmlSrv.vHostDialog import *
from vidarc.vSrv.vXmlSrv.vLocksDialog import *
import sys,string,os.path
import images

#import boa.tool.xml.vtXmlDom as vtXmlDomTree

[wxID_VINFOPANEL, wxID_VINFOPANELCBADD, wxID_VINFOPANELCBAPPLY, 
 wxID_VINFOPANELCBCFG, wxID_VINFOPANELCBDEL, wxID_VINFOPANELCBSECACL, 
 wxID_VINFOPANELCBSET, wxID_VINFOPANELGCBBBROWSE, wxID_VINFOPANELGCBBDEL, 
 wxID_VINFOPANELLBBBITMAP, wxID_VINFOPANELLBLALAIS, wxID_VINFOPANELLSTINFO, 
 wxID_VINFOPANELLSTSEC, wxID_VINFOPANELTXTALIAS, 
] = [wx.NewId() for _init_ctrls in range(14)]

# defined event for vgpData item selected
wxEVT_INFO_CHANGED=wx.NewEventType()
def EVT_INFO_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_INFO_CHANGED,func)
class vgpInfoChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_INFO_CHANGED(<widget_name>, self.OnInfoChanged)
    """

    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_INFO_CHANGED)
        self.obj=obj
    def GetObj(self):
        return self.obj

# defined event for vgpData item selected
wxEVT_INFO_ADDED=wx.NewEventType()
def EVT_INFO_ADDED(win,func):
    win.Connect(-1,-1,wxEVT_INFO_ADDED,func)
class vgpInfoAdded(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_INFO_ADDED(<widget_name>, self.OnInfoAdded)
    """

    def __init__(self,obj,name,image):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_INFO_ADDED)
        self.obj=obj
        self.name=name
        self.image=image
    def GetObj(self):
        return self.obj
    def GetName(self):
        return self.name
    def GetImage(self):
        return self.image

# defined event for vgpData item selected
wxEVT_INFO_DELETED=wx.NewEventType()
def EVT_INFO_DELETED(win,func):
    win.Connect(-1,-1,wxEVT_INFO_DELETED,func)
class vgpInfoDeleted(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_INFO_DELETED(<widget_name>, self.OnInfoDeleted)
    """

    def __init__(self,obj,name):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_INFO_DELETED)
        self.obj=obj
        self.name=name
    def GetObj(self):
        return self.obj
    def GetName(self):
        return self.name



# defined event for vgpData item selected
wxEVT_INFO_SERVE=wx.NewEventType()
def EVT_INFO_SERVE(win,func):
    win.Connect(-1,-1,wxEVT_INFO_SERVE,func)
class vgpInfoServe(wx.PyEvent):
    """
    Posted Events:
        Project Info seve event
            EVT_INFO_SERVE(<widget_name>, self.OnInfoServe)
    """

    def __init__(self,obj,name,fn,flag):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_INFO_SERVE)
        self.obj=obj
        self.name=name
        self.fn=fn
        self.flag=flag
    def GetObj(self):
        return self.obj
    def GetName(self):
        return self.name
    def GetFN(self):
        return self.fn
    def GetFlag(self):
        return self.flag

# defined event for vgpData item selected
wxEVT_INFO_CONFIG=wx.NewEventType()
def EVT_INFO_CONFIG(win,func):
    win.Connect(-1,-1,wxEVT_INFO_CONFIG,func)
class vgpInfoConfig(wx.PyEvent):
    """
    Posted Events:
        Project Info seve event
            EVT_INFO_CONFIG(<widget_name>, self.OnInfoConfig)
    """

    def __init__(self,obj,name,fn,flag):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_INFO_CONFIG)
        self.obj=obj
        self.name=name
        self.fn=fn
        self.flag=flag
    def GetObj(self):
        return self.obj
    def GetName(self):
        return self.name
    def GetFN(self):
        return self.fn
    def GetFlag(self):
        return self.flag

# defined event for vgpData item selected
wxEVT_INFO_SEC_ACL=wx.NewEventType()
def EVT_INFO_SEC_ACL(win,func):
    win.Connect(-1,-1,wxEVT_INFO_SEC_ACL,func)
class vgpInfoSecAcl(wx.PyEvent):
    """
    Posted Events:
        Project Info seve event
            EVT_INFO_SEC_ACL(<widget_name>, self.OnInfoSecAcl)
    """

    def __init__(self,obj,name,fn,flag):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_INFO_SEC_ACL)
        self.obj=obj
        self.name=name
        self.fn=fn
        self.flag=flag
    def GetObj(self):
        return self.obj
    def GetName(self):
        return self.name
    def GetFN(self):
        return self.fn
    def GetFlag(self):
        return self.flag

class vFileDropTarget(wx.FileDropTarget):
    def __init__(self, window):
        wx.FileDropTarget.__init__(self)
        self.window = window
    def OnDropFiles(self, x, y, filenames):
        for file in filenames:
            self.window.AddFile(u'',file)
            pass

class vImageDropTarget(wx.FileDropTarget):
    def __init__(self, window):
        wx.FileDropTarget.__init__(self)
        self.window = window
    def OnDropFiles(self, x, y, filenames):
        for file in filenames:
            self.window.SetImageFile(file)
            pass

class vInfoPanel(wx.Panel):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableRow(4)
        parent.AddGrowableCol(0)

    def _init_coll_bxsInfo_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.gcbbDel, 0, border=0, flag=0)
        parent.AddWindow(self.cbCfg, 0, border=0, flag=0)
        parent.AddWindow(self.cbSecAcl, 0, border=0, flag=0)

    def _init_coll_bxsNet_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbAdd, 0, border=0, flag=0)
        parent.AddWindow(self.cbSet, 0, border=0, flag=0)
        parent.AddWindow(self.cbDel, 0, border=0, flag=0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.fgsAlias, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.gcbbBrowse, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.lstInfo, 0, border=4, flag=wx.EXPAND | wx.LEFT)
        parent.AddSizer(self.bxsInfo, 0, border=4, flag=wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.lstSec, 0, border=4,
              flag=wx.EXPAND | wx.BOTTOM | wx.LEFT)
        parent.AddSizer(self.bxsNet, 0, border=4, flag=wx.RIGHT | wx.LEFT)

    def _init_coll_fgsAlias_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblAlais, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtAlias, 2, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbApply, 0, border=0, flag=0)
        parent.AddWindow(self.lbbBitmap, 0, border=0, flag=0)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=2, hgap=0, rows=5, vgap=0)

        self.bxsInfo = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsNet = wx.BoxSizer(orient=wx.VERTICAL)

        self.fgsAlias = wx.FlexGridSizer(cols=4, hgap=0, rows=1, vgap=0)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsInfo_Items(self.bxsInfo)
        self._init_coll_bxsNet_Items(self.bxsNet)
        self._init_coll_fgsAlias_Items(self.fgsAlias)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VINFOPANEL, name=u'vInfoPanel',
              parent=prnt, pos=wx.Point(347, 220), size=wx.Size(339, 268),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(331, 241))
        self.SetAutoLayout(True)

        self.lstInfo = wx.ListView(id=wxID_VINFOPANELLSTINFO, name=u'lstInfo',
              parent=self, pos=wx.Point(4, 48), size=wx.Size(248, 92),
              style=wx.LC_REPORT|wx.LC_SORT_ASCENDING|wx.LC_SINGLE_SEL)
        self.lstInfo.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstInfoListItemSelected, id=wxID_VINFOPANELLSTINFO)
        self.lstInfo.Bind(wx.EVT_LEFT_DCLICK, self.OnLstInfoLeftDclick)
        self.lstInfo.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstInfoListItemDeselected, id=wxID_VINFOPANELLSTINFO)
        self.lstInfo.Bind(wx.EVT_RIGHT_DOWN, self.OnLstInfoRightDown)

        self.gcbbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VINFOPANELGCBBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Del', name=u'gcbbDel',
              parent=self, pos=wx.Point(256, 48), size=wx.Size(76, 30),
              style=0)
        self.gcbbDel.Bind(wx.EVT_BUTTON, self.OnGcbbDelButton,
              id=wxID_VINFOPANELGCBBDEL)

        self.gcbbBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VINFOPANELGCBBBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbBrowse', parent=self, pos=wx.Point(256, 4),
              size=wx.Size(76, 30), style=0)
        self.gcbbBrowse.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseButton,
              id=wxID_VINFOPANELGCBBBROWSE)

        self.cbCfg = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VINFOPANELCBCFG,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cfg', name=u'cbCfg',
              parent=self, pos=wx.Point(256, 78), size=wx.Size(76, 30),
              style=0)
        self.cbCfg.Bind(wx.EVT_BUTTON, self.OnCbCfgButton,
              id=wxID_VINFOPANELCBCFG)

        self.cbSecAcl = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VINFOPANELCBSECACL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'ACL', name=u'cbSecAcl',
              parent=self, pos=wx.Point(256, 108), size=wx.Size(76, 30),
              style=0)
        self.cbSecAcl.Bind(wx.EVT_BUTTON, self.OnCbSecAclButton,
              id=wxID_VINFOPANELCBSECACL)

        self.txtAlias = wx.TextCtrl(id=wxID_VINFOPANELTXTALIAS,
              name=u'txtAlias', parent=self, pos=wx.Point(30, 4),
              size=wx.Size(136, 36), style=0, value=u'')

        self.lblAlais = wx.StaticText(id=wxID_VINFOPANELLBLALAIS,
              label=u'Alias', name=u'lblAlais', parent=self, pos=wx.Point(4, 4),
              size=wx.Size(22, 36), style=0)

        self.cbApply = wx.BitmapButton(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VINFOPANELCBAPPLY, name=u'cbApply', parent=self,
              pos=wx.Point(166, 4), size=wx.Size(24, 24), style=wx.BU_AUTODRAW)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VINFOPANELCBAPPLY)

        self.lbbBitmap = wx.StaticBitmap(bitmap=wx.EmptyBitmap(32, 32),
              id=wxID_VINFOPANELLBBBITMAP, name=u'lbbBitmap', parent=self,
              pos=wx.Point(190, 4), size=wx.Size(36, 36),
              style=wx.ST_NO_AUTORESIZE)

        self.lstSec = wx.ListView(id=wxID_VINFOPANELLSTSEC, name=u'lstSec',
              parent=self, pos=wx.Point(4, 148), size=wx.Size(248, 89),
              style=wx.LC_REPORT|wx.LC_SORT_ASCENDING)
        self.lstSec.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstSecListItemDeselected, id=wxID_VINFOPANELLSTSEC)
        self.lstSec.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstSecListItemSelected, id=wxID_VINFOPANELLSTSEC)

        self.cbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VINFOPANELCBADD,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'cbAdd',
              parent=self, pos=wx.Point(256, 148), size=wx.Size(76, 30),
              style=0)
        self.cbAdd.Bind(wx.EVT_BUTTON, self.OnCbAddButton,
              id=wxID_VINFOPANELCBADD)

        self.cbSet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VINFOPANELCBSET,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Set', name=u'cbSet',
              parent=self, pos=wx.Point(256, 178), size=wx.Size(76, 30),
              style=0)
        self.cbSet.Bind(wx.EVT_BUTTON, self.OnCbSetButton,
              id=wxID_VINFOPANELCBSET)

        self.cbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VINFOPANELCBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Del', name=u'cbDel',
              parent=self, pos=wx.Point(256, 208), size=wx.Size(76, 30),
              style=0)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VINFOPANELCBDEL)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        
        self.bLockPost=False
        self.idxSel=-1
        self.files=[]
        self.dlgHost=None
        self.dlgLocks=None
        self.srvXml=None
        
        dt = vFileDropTarget(self)
        self.lstInfo.SetDropTarget(dt)
        
        dt=vImageDropTarget(self)
        self.lbbBitmap.SetDropTarget(dt)
        
        self.gcbbBrowse.SetBitmapLabel(images.getBrowseBitmap())
        self.gcbbDel.SetBitmapLabel(images.getDelBitmap())
        self.cbSecAcl.SetBitmapLabel(images.getSecAclBitmap())
        self.cbApply.SetBitmapLabel(images.getApplyBitmap())
        self.lbbBitmap.SetBitmap(images.getDropTargetBitmap())
        
        self.cbAdd.SetBitmapLabel(images.getAddBitmap())
        self.cbSet.SetBitmapLabel(images.getApplyBitmap())
        self.cbDel.SetBitmapLabel(images.getDelBitmap())
        self.cbCfg.SetBitmapLabel(images.getSettingsSmallBitmap())
        
        self.lstInfo.InsertColumn(0,u'Alais',wx.LIST_FORMAT_LEFT,120)
        self.lstInfo.InsertColumn(1,u'Name',wx.LIST_FORMAT_LEFT,140)
        self.lstInfo.InsertColumn(2,u'Path',wx.LIST_FORMAT_LEFT,200)
        
        self.imgLst=wx.ImageList(16,16)
        self.imgLst.Add(images.getRunningBitmap())
        self.imgLst.Add(images.getStoppedBitmap())
        self.dictImg={}
        self.lstInfo.SetImageList(self.imgLst,wx.IMAGE_LIST_NORMAL)
        self.lstInfo.SetImageList(self.imgLst,wx.IMAGE_LIST_SMALL)
        
        self.lstSec.InsertColumn(0,u'Host',wx.LIST_FORMAT_LEFT,140)
        self.lstSec.InsertColumn(1,u'User',wx.LIST_FORMAT_LEFT,70)
        
        self.imgLstSec=wx.ImageList(16,16)
        self.imgLstSec.Add(images.getSecBitmap())
        self.dictImg={}
        self.lstSec.SetImageList(self.imgLstSec,wx.IMAGE_LIST_NORMAL)
        self.lstSec.SetImageList(self.imgLstSec,wx.IMAGE_LIST_SMALL)
    def SetSrvXml(self,srvXml,appl):
        self.appl=appl
        self.srvXml=srvXml
    def GetAppl(self):
        return self.appl
    def SetNode(self,node,doc):
        self.idxImgFile=-1
        self.imgNew=None
        self.sImgFN=''
        
        self.node=node
        self.doc=doc
        self.bLockPost=True
        self.lstInfo.DeleteAllItems()
        self.idxSel=-1
        self.lstServeNode=[]
        self.lstHostNode=[]
        for c in self.doc.getChilds(node,'file'):
            self.lstServeNode.append(c)
            self.AddFile(self.doc.getNodeText(c,'alias'),
                    self.doc.getNodeText(c,'name'),
                    self.doc.getNodeText(c,'image'))
        self.bLockPost=False
        pass
        
    def GetNode(self):
        if self.node is None:
            return
        childs=self.doc.getChilds(self.node,'file')
        for idx in range(0,self.lstInfo.GetItemCount()):
            it=self.lstInfo.GetItem(idx,0)
            sAlias=it.m_text
            iImgIdx=it.m_image
            it=self.lstInfo.GetItem(idx,1)
            sFN=it.m_text
            it=self.lstInfo.GetItem(idx,2)
            sDN=it.m_text
            sFull=os.path.join(sDN,sFN)
            bFound=False
            for c in childs:
                if self.doc.getNodeText(c,'name')==sFull:
                    bFound=True
                    break
            if bFound==False:
                c=self.doc.createSubNode(self.node,'file',False)
            self.doc.setNodeText(c,'alias',sAlias)
            self.doc.setNodeText(c,'name',sFull)
            sImage=u''
            for k in self.dictImg.keys():
                if self.dictImg[k]==iImgIdx:
                    sImage=k
                    break
            self.doc.setNodeText(c,'image',sImage)
        self.doc.AlignNode(self.node,iRec=2)
        pass
    def AddFile(self,alias,file,image=''):
        try:
            i=self.files.index(file)
        except:
            sDN,sFN=os.path.split(file)
            i=1
            if 1==0:
                if len(image)>0:
                    if self.dictImg.has_key(image):
                        i=self.dictImg[image]
                    else:
                        try:
                            bmp=wx.Image(image).ConvertToBitmap()
                            i=self.imgLst.Add(bmp)
                            self.dictImg[image]=i
                        except:
                            i=-1
            idx=self.lstInfo.InsertImageStringItem(sys.maxint, alias, i)
            self.lstInfo.SetStringItem(idx,1,sFN,-1)
            self.lstInfo.SetStringItem(idx,2,sDN,-1)
            self.lstInfo.SetItemData(idx,len(self.lstServeNode)-1)
            self.files.append(file)
            if self.bLockPost==False:
                if self.idxSel>=0:
                    self.lstInfo.SetItemState(self.idxSel,0,wx.LIST_STATE_SELECTED)
                    self.idxSel=-1
                wx.PostEvent(self,vgpInfoChanged(self))
    def SetImageFile(self,file):
        if len(file)>0:
            if self.dictImg.has_key(file):
                i=self.dictImg[file]
                bmp=self.imgLst.GetBitmap(i)
                self.idxImgFile=i
                self.imgNew=None
            else:
                try:
                    bmp=wx.Image(file).ConvertToBitmap()
                    self.sImgFN=file
                    self.idxImgFile=-1
                    self.imgNew=bmp
                except:
                    self.idxImgFile=-1
                    self.sImgFN=''
                    self.imgNew=None
                    return
        self.lbbBitmap.SetBitmap(wx.EmptyBitmap(32, 32))
        self.lbbBitmap.Refresh()
        self.lbbBitmap.SetBitmap(bmp)
        self.lbbBitmap.Refresh()
        pass
    def OnGcbbDelButton(self, event):
        it=self.lstInfo.GetItem(self.idxSel,1)
        sFN=it.m_text
        it=self.lstInfo.GetItem(self.idxSel,2)
        sDN=it.m_text
        sFull=os.path.join(sDN,sFN)
        try:
            idx=self.files.index(sFull)
            self.files.remove(sFull)
        except:
            pass
        idx=self.lstInfo.GetItemData(self.idxSel)
        node=self.lstServeNode[idx]
        sAlias=self.doc.getNodeText(node,'alias')
        if node is not None:
            self.doc.deleteNode(node)
        self.lstInfo.DeleteItem(self.idxSel)
        self.idxSel=-1
        #wx.PostEvent(self,vgpInfoChanged(self))
        wx.PostEvent(self,vgpInfoDeleted(self,sAlias))
        event.Skip()

    def OnGcbbBrowseButton(self, event):
        #vtLog.CallStack('')
        dlg = wx.FileDialog(self, "Open", ".", "", 
                    "XML files (*.xml)|*.xml|all files (*.*)|*.*", wx.OPEN)
        try:
            #dlg.Centre()
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                for it in self.lstServeNode:
                    if self.doc.getNodeText(it,'name')==filename:
                        return
                #self.lstServeNode.append(None)
                c=self.doc.createSubNode(self.node,'file',False)
                self.doc.setNodeText(c,'alias','')
                self.doc.setNodeText(c,'name',filename)
                self.lstServeNode.append(c)
                self.AddFile('',filename)
                self.__showHosts__()
                wx.PostEvent(self,vgpInfoChanged(self))
        finally:
            dlg.Destroy()
        event.Skip()

    def OnGcbbUpButton(self, event):
        event.Skip()

    def OnGcbbDownButton(self, event):
        event.Skip()


    def OnLstInfoLeftDclick(self, event):
        it=self.lstInfo.GetItem(self.idxSel,0)
        sAlias=it.m_text
        it=self.lstInfo.GetItem(self.idxSel,1)
        sFN=it.m_text
        it=self.lstInfo.GetItem(self.idxSel,2)
        sDN=it.m_text
        sFull=os.path.join(sDN,sFN)
        wx.PostEvent(self,vgpInfoServe(self,sAlias,sFull,True))
        event.Skip()
    def GenLunch(self):
        if self.idxSel>=0:
            it=self.lstInfo.GetItem(self.idxSel,1)
            sFN=it.m_text
            it=self.lstInfo.GetItem(self.idxSel,2)
            sDN=it.m_text
            sFull=os.path.join(sDN,sFN)
            wx.PostEvent(self,vgpInfoLunch(self,sFull))
    def GetLunchs(self):
        d={}
        for c in self.doc.getChilds(self.node,'file'):
            dInfo={}
            for tag in ['alias','name']:
                dInfo[tag]=self.doc.getNodeText(c,tag)
            lstHost=[]
            for cSec in self.doc.getChilds(c,'host'):
                tup=(self.doc.getNodeText(cSec,'name'),
                    self.doc.getNodeText(cSec,'user'),
                    self.doc.getNodeText(cSec,'passwd'))
                lstHost.append(tup)
            dInfo['hosts']=lstHost
            d[dInfo['name']]=dInfo
        return d
    def Start(self,fn,alias):
        for idx in range(0,self.lstInfo.GetItemCount()):
            it=self.lstInfo.GetItem(idx,0)
            sAlias=it.m_text
            iImgIdx=it.m_image
            it=self.lstInfo.GetItem(idx,1)
            sFN=it.m_text
            it=self.lstInfo.GetItem(idx,2)
            sDN=it.m_text
            sFull=os.path.join(sDN,sFN)
            if sFull==fn:
                if sAlias==alias:
                    self.lstInfo.SetItemImage(idx,0)
    def Stop(self,fn,alias):
        for idx in range(0,self.lstInfo.GetItemCount()):
            it=self.lstInfo.GetItem(idx,0)
            sAlias=it.m_text
            iImgIdx=it.m_image
            it=self.lstInfo.GetItem(idx,1)
            sFN=it.m_text
            it=self.lstInfo.GetItem(idx,2)
            sDN=it.m_text
            sFull=os.path.join(sDN,sFN)
            if sFull==fn:
                if sAlias==alias:
                    self.lstInfo.SetItemImage(idx,1)
    def OnCbApplyButton(self, event):
        if self.idxSel<0:
            return
        if self.idxImgFile<0:
            if self.imgNew is not None:
                self.idxImgFile=self.imgLst.Add(self.imgNew)
                self.dictImg[self.sImgFN]=self.idxImgFile
        iFind=self.lstInfo.FindItem(-1,self.txtAlias.GetValue())
        if iFind>=0:
            sMsg=u'Alias exists already, procedure aborted!'
            dlg=wx.MessageDialog(self,sMsg ,
                    u'vgaSrv',
                    wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg .Destroy()
            return
        it=self.lstInfo.GetItem(self.idxSel,0)
        sAlias=it.m_text
        if self.srvXml is not None:
            bRet=self.srvXml.IsServedXml(self.appl+':'+sAlias)
            if bRet:
                sMsg=u'Alias is currently served! \n\nStop it to perform action.'
                dlg=wx.MessageDialog(self,sMsg ,
                        u'vgaSrv',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg .Destroy()
                return
        self.lstInfo.SetStringItem(self.idxSel,0,self.txtAlias.GetValue(),
                            self.idxImgFile)
            
        self.txtAlias.SetValue('')
        self.idxImgFile=-1
        self.imgNew=None
        self.lbbBitmap.SetBitmap(wx.EmptyBitmap(32, 32))
        #self.lbbBitmap.Refresh()
        self.lbbBitmap.SetBitmap(images.getDropTargetBitmap())
        #self.lbbBitmap.Refresh()
        wx.PostEvent(self,vgpInfoChanged(self))
        event.Skip()

    def OnLstInfoListItemSelected(self, event):
        self.idxSel=event.GetIndex()
        it=self.lstInfo.GetItem(self.idxSel,0)
        self.txtAlias.SetValue(it.m_text)
        self.__showHosts__()
        event.Skip()
    def __showHosts__(self):
        self.lstSec.DeleteAllItems()
        self.lstHostNode=[]
        if self.idxSel<0:
            return
        idx=self.lstInfo.GetItemData(self.idxSel)
        node=self.lstServeNode[idx]
        childs=self.doc.getChilds(node,'host')
        for c in childs:
            self.lstHostNode.append(c)
            sHost=self.doc.getNodeText(c,'name')
            sUsr=self.doc.getNodeText(c,'user')
            sPasswd=self.doc.getNodeText(c,'passwd')
            i=-1
            if len(sPasswd)>0:
                i=0
            idx=self.lstSec.InsertImageStringItem(sys.maxint, sHost, i)
            self.lstSec.SetStringItem(idx,1,sUsr,-1)
            self.lstSec.SetItemData(idx,len(self.lstHostNode)-1)
    def OnLstInfoListItemDeselected(self, event):
        self.idxHost=-1
        self.lstSec.DeleteAllItems()
        event.Skip()

    def OnLstSecListItemDeselected(self, event):
        event.Skip()

    def OnLstSecListItemSelected(self, event):
        self.idxHost=event.GetIndex()
        event.Skip()

    def OnCbAddButton(self, event):
        if self.idxSel<0:
            return
        if self.dlgHost is None:
            self.dlgHost=vHostDialog(self)
            self.dlgHost.SetDoc(self.doc)
        self.dlgHost.Centre()
        nodeAdd=self.doc.createNode('host')
        self.doc.createSubNode(nodeAdd,'name')
        self.doc.createSubNode(nodeAdd,'user')
        self.doc.createSubNode(nodeAdd,'passwd')
        self.dlgHost.SetNode(nodeAdd)
        iRet=self.dlgHost.ShowModal()
        if iRet>0:
            self.dlgHost.GetNode()
            idx=self.lstInfo.GetItemData(self.idxSel)
            node=self.lstServeNode[idx]
            self.doc.appendChild(node,nodeAdd)
            self.doc.AlignNode(nodeAdd,iRec=2)
            if self.dlgHost.IsApplyAll():
                
                for i in range(len(self.lstServeNode)):
                    if i==idx:
                        continue
                    serveNode=self.lstServeNode[i]
                    tmpNode=self.doc.cloneNode(nodeAdd,True)
                    self.doc.appendChild(serveNode,tmpNode)
                    self.doc.AlignNode(serveNode,iRec=2)
                        
            self.__showHosts__()
            try:
                wx.PostEvent(self,vgpInfoChanged(self))
            except:
                pass
            
        event.Skip()

    def OnCbDelButton(self, event):
        #idx=self.lstInfo.GetItemData(self.idxSel)
        #node=self.lstServeNode[idx]
        idx=self.lstSec.GetItemData(self.idxHost)
        nodeHost=self.lstHostNode[idx]
        self.doc.deleteNode(nodeHost)
        self.lstHostNode=self.lstHostNode[:idx]+self.lstHostNode[idx+1:]
        self.__showHosts__()
        try:
            wx.PostEvent(self,vgpInfoChanged(self))
        except:
            pass
        event.Skip()

    def OnCbSetButton(self, event):
        if self.idxHost<0:
            return
        if self.dlgHost is None:
            self.dlgHost=vHostDialog(self)
            self.dlgHost.SetDoc(self.doc)
        self.dlgHost.Centre()
        idx=self.lstSec.GetItemData(self.idxHost)
        self.dlgHost.SetNode(self.lstHostNode[idx])
        iRet=self.dlgHost.ShowModal()
        if iRet>0:
            self.dlgHost.GetNode()
            self.__showHosts__()
            try:
                wx.PostEvent(self,vgpInfoChanged(self))
            except:
                pass
        event.Skip()

    def OnCbCfgButton(self, event):
        if self.idxSel<0:
            return
        it=self.lstInfo.GetItem(self.idxSel,0)
        sAlias=it.m_text
        it=self.lstInfo.GetItem(self.idxSel,1)
        sFN=it.m_text
        it=self.lstInfo.GetItem(self.idxSel,2)
        sDN=it.m_text
        sFull=os.path.join(sDN,sFN)
        wx.PostEvent(self,vgpInfoConfig(self,sAlias,sFull,True))
        event.Skip()
    def OnCbSecAclButton(self, event):
        if self.idxSel<0:
            return
        it=self.lstInfo.GetItem(self.idxSel,0)
        sAlias=it.m_text
        it=self.lstInfo.GetItem(self.idxSel,1)
        sFN=it.m_text
        it=self.lstInfo.GetItem(self.idxSel,2)
        sDN=it.m_text
        sFull=os.path.join(sDN,sFN)
        wx.PostEvent(self,vgpInfoSecAcl(self,sAlias,sFull,True))
        event.Skip()
    def OnLstInfoRightDown(self, event):
        if self.idxSel<0:
            return
        
        self.x=event.GetX()
        self.y=event.GetY()
        if not hasattr(self,'popupIdServeStop'):
            self.popupIdServeStart=wx.NewId()
            self.popupIdServeStop=wx.NewId()
            self.popupIdSave=wx.NewId()
            self.popupIdLocks=wx.NewId()
            wx.EVT_MENU(self,self.popupIdServeStart,self.OnXmlServeStart)
            wx.EVT_MENU(self,self.popupIdServeStop,self.OnXmlServeStop)
            wx.EVT_MENU(self,self.popupIdSave,self.OnXmlSave)
            wx.EVT_MENU(self,self.popupIdLocks,self.OnLocks)
        if self.srvXml is not None:
            menu=wx.Menu()
            it=self.lstInfo.GetItem(self.idxSel,0)
            sAlias=it.m_text
            bRet=self.srvXml.IsServedXml(self.appl+':'+sAlias)
            mnIt=wx.MenuItem(menu,self.popupIdServeStop,'Stop')
            mnIt.SetBitmap(images.getStoppedBitmap())
            menu.AppendItem(mnIt)
            
            mnIt=wx.MenuItem(menu,self.popupIdServeStart,'Start')
            mnIt.SetBitmap(images.getRunningBitmap())
            menu.AppendItem(mnIt)

            mnIt=wx.MenuItem(menu,self.popupIdSave,'Save')
            mnIt.SetBitmap(images.getSaveBitmap())
            menu.AppendItem(mnIt)
            if bRet:
                menu.Enable(self.popupIdServeStart,False)
            else:
                menu.Enable(self.popupIdServeStop,False)
                menu.Enable(self.popupIdSave,False)
            
            menu.AppendSeparator()
            mnIt=wx.MenuItem(menu,self.popupIdLocks,'Locks')
            mnIt.SetBitmap(vtArt.getBitmap(vtArt.Pin))
            menu.AppendItem(mnIt)
                        
            self.PopupMenu(menu,wx.Point(event.GetX(), event.GetY()))
            menu.Destroy()
        event.Skip()
    def OnXmlServeStart(self,evt):
        it=self.lstInfo.GetItem(self.idxSel,0)
        sAlias=it.m_text
        it=self.lstInfo.GetItem(self.idxSel,1)
        sFN=it.m_text
        it=self.lstInfo.GetItem(self.idxSel,2)
        sDN=it.m_text
        sFull=os.path.join(sDN,sFN)
        wx.PostEvent(self,vgpInfoServe(self,sAlias,sFull,True))
        pass
    def OnXmlServeStop(self,evt):
        it=self.lstInfo.GetItem(self.idxSel,0)
        sAlias=it.m_text
        it=self.lstInfo.GetItem(self.idxSel,1)
        sFN=it.m_text
        it=self.lstInfo.GetItem(self.idxSel,2)
        sDN=it.m_text
        sFull=os.path.join(sDN,sFN)
        wx.PostEvent(self,vgpInfoServe(self,sAlias,sFull,True))
        pass
    def OnXmlSave(self,evt):
        if self.idxSel<0:
            return
        if self.srvXml is not None:
            it=self.lstInfo.GetItem(self.idxSel,0)
            sAlias=it.m_text
            bRet=self.srvXml.SaveXml(self.appl+':'+sAlias)
    def OnLocks(self,evt):
        if self.dlgLocks is None:
            self.dlgLocks=vLocksDialog(self)
            self.dlgLocks.Centre()
            EVT_VTNET_XML_LOCKS_REQUEST(self.dlgLocks.pnLocks,self.OnLockRequest)
            EVT_VTNET_XML_LOCKS_FORCE(self.dlgLocks.pnLocks,self.OnLockForce)
            EVT_VTNET_XML_LOCKS_UPDATE(self.dlgLocks,self.OnLockUpdate)
        l=[]
        if self.idxSel>=0:
            it=self.lstInfo.GetItem(self.idxSel,0)
            sAlias=it.m_text
            sApplAlias=self.appl+':'+sAlias
            if self.srvXml is not None:
                l=self.srvXml.GetLockLst(sApplAlias)
        else:
            sApplAlias=self.appl+':'
        self.dlgLocks.SetLocks(sApplAlias,l)
        self.dlgLocks.Show()
    def OnLockRequest(self,evt):
        try:
            sApplAlias=evt.GetApplAlias()
            sId=evt.GetID()
            if self.srvXml is not None:
                self.srvXml.RequestLock(sApplAlias,None,sId)
        except:
            vtLog.vtLngTB(self.appl)
    def OnLockForce(self,evt):
        try:
            sApplAlias=evt.GetApplAlias()
            sId=evt.GetID()
            if self.srvXml is not None:
                self.srvXml.ForceLock(sApplAlias,None,sId)
        except:
            vtLog.vtLngTB(self.appl)
    def OnLockUpdate(self,evt):
        sApplAlias=evt.GetApplAlias()
        l=[]
        if self.srvXml is not None:
            l=self.srvXml.GetLockLst(sApplAlias)
        self.dlgLocks.SetLocks(sApplAlias,l)
        

