#----------------------------------------------------------------------------
# Name:         __register__.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060708
# CVS-ID:       $Id: __register__.py,v 1.2 2006/11/21 21:16:20 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.vSrv.vXmlSrv.vXmlNodeXmlSrv import vXmlNodeXmlSrv
from vidarc.vSrv.vXmlSrv.vXmlNodeXmlSrvAlias import vXmlNodeXmlSrvAlias
from vidarc.vSrv.vXmlSrv.vXmlNodeXmlSrvAliasSummary import vXmlNodeXmlSrvAliasSummary
from vidarc.vSrv.net.vXmlNodeHosts import vXmlNodeHosts

def RegisterNodes(self,oRoot,bRegAsRoot=True):
    try:
        oXmlSrv=vXmlNodeXmlSrv()
        oXmlSrvAlias=vXmlNodeXmlSrvAlias()
        oXmlSrvAliasSummary=vXmlNodeXmlSrvAliasSummary()
        oHosts=vXmlNodeHosts()
        
        self.RegisterNode(oXmlSrv,bRegAsRoot)
        self.RegisterNode(oXmlSrvAlias,False)
        self.RegisterNode(oXmlSrvAliasSummary,False)
        self.RegisterNode(oHosts,False)
        self.LinkRegisteredNode(oRoot,oXmlSrv)
        self.LinkRegisteredNode(oXmlSrv,oXmlSrvAlias)
        self.LinkRegisteredNode(oXmlSrv,oXmlSrvAliasSummary)
        self.LinkRegisteredNode(oXmlSrv,oHosts)
        self.LinkRegisteredNode(oXmlSrvAlias,oHosts)
        self.LinkRegisteredNode(oXmlSrvAliasSummary,oHosts)
        return oXmlSrv
    except:
        vtLog.vtLngTB(__name__)
    return None
