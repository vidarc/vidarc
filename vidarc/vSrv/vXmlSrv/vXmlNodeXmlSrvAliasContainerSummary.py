#----------------------------------------------------------------------------
# Name:         vXmlNodeXmlSrvAliasContainerSummary.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20061015
# CVS-ID:       $Id: vXmlNodeXmlSrvAliasContainerSummary.py,v 1.1 2006/11/21 21:28:20 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vSrv.net.vsNetXmlNodeSrvAlias import vsNetXmlNodeSrvAlias
try:
    if vcCust.is2Import(__name__):
        from vidarc.vSrv.vXmlSrv.vXmlNodeXmlSrvAliasContainerSummaryPanel import vXmlNodeXmlSrvAliasContainerSummaryPanel
        from vidarc.vSrv.vXmlSrv.vXmlNodeXmlSrvAliasContainerSummaryEditDialog import vXmlNodeXmlSrvAliasContainerSummaryEditDialog
        from vidarc.vSrv.vXmlSrv.vXmlNodeXmlSrvAliasContainerSummaryAddDialog import vXmlNodeXmlSrvAliasContainerSummaryAddDialog
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeXmlSrvAliasContainerSummary(vsNetXmlNodeSrvAlias):
    def __init__(self,tagName='XmlSrvAliasContainerSummary',clsDoc=None,imgData=None,aliasName=None):
        global _
        _=vtLgBase.assignPluginLang('vXmlSrv')
        vsNetXmlNodeSrvAlias.__init__(self,tagName)
        self.clsDoc=clsDoc
        self.imgData=imgData
        self.aliasName=aliasName
    def GetDescription(self):
        return _(u'XML server alias container summary')
    # ---------------------------------------------------------
    # specific
    def GetAliasName(self):
        return self.aliasName
    def GetDoc2Serve(self,node,appl=None):
        if appl is not None:
            return self.clsDoc(appl=appl)
        return self.clsDoc()
    def HasChilds2Serve(self,node):
        return True
    def GetServerObjNode(self,node):
        nodePar=self.doc.getParent(node)
        if nodePar is None:
                return None,None
        o=self.doc.GetRegByNode(nodePar)
        if hasattr(o,'GetServerObjNode'):
            return o.GetServerObjNode(nodePar)
        return o,nodePar,node
    # ---------------------------------------------------------
    # inheritance
    def __createPost__(self,node,*args,**kwargs):
        self.SetTag(node,'')
    def GetTag(self,node):
        return self.tagName
    def GetName(self,node):
        return self.GetDescription()
    def SetTag(self,node,val):
        self.Set(node,'tag',self.GetTagName())
    def SetName(self,node,val):
        pass
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return False
    def getImageData(self):
        if self.imgData is not None:
            return self.imgData
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00pIDAT8\x8dcddbf@\x07\xba\x1d!\xff1\x04\x19\x18\x18.W\xacaD\x17c"V!61\
\x06\x06\x06\x06F\x98\x0bp\xd9\x8a\x0b\xc0\x0c\xc4\xea\x02R\x00\xe5\x06\x90\
\xeatd\xa0\xdb\x11\xf2\x9fQ\xaf+\x9cl\x03\x18\x18\x06E\x18P\xc5\x00\\\x89\
\x04\x1f\x80\xa7\x03r4#\x1b2\xf0a\xc0\x88-720`\xe6\r\\^\xc5\xea\x02l\xa9\x13\
W\x8a\x05\x00b\xb7"-7\xce\xbc\x10\x00\x00\x00\x00IEND\xaeB`\x82' 
    def GetEditDialogClass(self):
        if GUI:
            return vXmlNodeXmlSrvAliasContainerSummaryEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return vXmlNodeXmlSrvAliasContainerSummaryAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeXmlSrvAliasContainerSummaryPanel
        else:
            return None
