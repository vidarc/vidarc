#Boa:Dialog:vLocksDialog

import wx
import wx.lib.buttons
import vidarc.tool.net.vtNetXmlLocksPanel

from vidarc.tool.net.vtNetXmlLocksPanel import *

import vidarc.tool.art.vtArt as vtArt

def create(parent):
    return vLocksDialog(parent)

[wxID_VLOCKSDIALOG, wxID_VLOCKSDIALOGCBCANCEL, wxID_VLOCKSDIALOGCBUPDATE, 
 wxID_VLOCKSDIALOGPNLOCKS, 
] = [wx.NewId() for _init_ctrls in range(4)]

class vLocksDialog(wx.Dialog):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbUpdate, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.pnLocks, 1, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VLOCKSDIALOG, name=u'vLocksDialog',
              parent=prnt, pos=wx.Point(132, 132), size=wx.Size(400, 250),
              style=wx.DEFAULT_DIALOG_STYLE, title=u'vXmlSrv Locks Dialog')
        self.SetClientSize(wx.Size(392, 223))

        self.pnLocks = vidarc.tool.net.vtNetXmlLocksPanel.vtNetXmlLocksPanel(id=wxID_VLOCKSDIALOGPNLOCKS,
              name=u'pnLocks', parent=self, pos=wx.Point(4, 0),
              size=wx.Size(384, 177), style=0)

        self.cbUpdate = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VLOCKSDIALOGCBUPDATE,
              bitmap=vtArt.getBitmap(vtArt.Synch), label=u'Update',
              name=u'cbUpdate', parent=self, pos=wx.Point(104, 189),
              size=wx.Size(76, 30), style=0)
        self.cbUpdate.Bind(wx.EVT_BUTTON, self.OnCbUpdateButton,
              id=wxID_VLOCKSDIALOGCBUPDATE)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VLOCKSDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=u'Cancel',
              name=u'cbCancel', parent=self, pos=wx.Point(212, 189),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VLOCKSDIALOGCBCANCEL)

        self._init_sizers()

    def __init__(self, parent):
        self._init_ctrls(parent)

    def SetLocks(self,sApplAlias,l):
        self.SetTitle('vXmlSrv %s Locks Dialog'%sApplAlias)
        self.applAlias=sApplAlias
        self.pnLocks.SetLocks(sApplAlias,l)
        
    def OnCbUpdateButton(self, event):
        wx.PostEvent(self,vtNetXmlLocksUpdate(self,self.applAlias))
        event.Skip()

    def OnCbCancelButton(self, event):
        self.Show(False)
        event.Skip()
