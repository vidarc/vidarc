#----------------------------------------------------------------------------
# Name:         vXmlXmlSrv.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060708
# CVS-ID:       $Id: vXmlXmlSrv.py,v 1.2 2007/07/30 09:05:05 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vidarc.vSrv.vXmlSrv.vXmlNodeXmlSrvRoot import vXmlNodeXmlSrvRoot

import vidarc.vSrv.vXmlSrv.__register__ as __register__

class vXmlXmlSrv(vtXmlDomReg):
    VERBOSE=1
    TAGNAME_REFERENCE='tag'
    TAGNAME_ROOT='XmlSrvRoot'
    def __init__(self,appl='vXmlSrv',attr='id',skip=[],synch=False,verbose=0,
                    audit_trail=True):
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        try:
            vtXmlDomReg.__init__(self,appl=appl,attr=attr,skip=skip,synch=synch,verbose=verbose,
                        audit_trail=audit_trail)
            oRoot=vXmlNodeXmlSrvRoot()
            self.RegisterNode(oRoot,False)
            
            __register__.RegisterNodes(self,oRoot,True)
        except:
            vtLog.vtLngTB(self.appl)
        self.SetDftLanguages()
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'XmlSrvs')
    #def New(self,revision='1.0',root='XmlSrvRoot',bConnection=False):
    #    vtXmlDomReg.New(self,revision,root)
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elem=self.createSubNode(self.getRoot(),'XmlSrvs')
        if bConnection==False:
            self.CreateReq()
        #self.createSubNodeTextAttr(elem,'cfg','','id','')
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        elem=self.createSubNode(self.getRoot(),'security_acl')
        self.__checkReqBase__()
        #self.AlignDoc()
