#Boa:Frame:vgfMain

import wx
import wx.lib.buttons
from wx.lib.anchors import LayoutAnchors
import time
import images
from boa.tool.xml.vtXmlDom import *
from boa.tool.net.vtNetXmlClt import vtNetXmlCltSock
from boa.tool.net.vtNetClt import vtNetClt
from boa.tool.net.vNetPlaceDialog import vNetPlaceDialog
import string,os,os.path,sys,sha,binascii
import tool.InOut.fnUtil as fnUtil

#from boa.vApps.vLunch.vgpInfo import *

def create(parent):
    return vgfMain(parent)

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon
    
[wxID_VGFMAIN, wxID_VGFMAINCBCLOSE, wxID_VGFMAINCBNETPLACE, 
 wxID_VGFMAINCBSEND, wxID_VGFMAINCBSHUTDOWN, wxID_VGFMAINCHCAPPL, 
 wxID_VGFMAINCHCPROTOCOL, wxID_VGFMAINCHKPARAM0, wxID_VGFMAINCHKPARAM1, 
 wxID_VGFMAINCHKPARAM2, wxID_VGFMAINCHKPARAM3, wxID_VGFMAINLBLALIAS, 
 wxID_VGFMAINLBLAPPL, wxID_VGFMAINLBLCONNSTATUS, wxID_VGFMAINLBLHOST, 
 wxID_VGFMAINLBLPASS, wxID_VGFMAINLBLPORT, wxID_VGFMAINLBLPROTOCOL, 
 wxID_VGFMAINLBLRESULT, wxID_VGFMAINLBLTIME, wxID_VGFMAINLBLUSR, 
 wxID_VGFMAINLSTCONNECTIONS, wxID_VGFMAINLSTNODES, wxID_VGFMAINPNINFO, 
 wxID_VGFMAINTGCONNECT, wxID_VGFMAINTXTALIAS, wxID_VGFMAINTXTHOST, 
 wxID_VGFMAINTXTPARAM0, wxID_VGFMAINTXTPARAM1, wxID_VGFMAINTXTPARAM2, 
 wxID_VGFMAINTXTPARAM3, wxID_VGFMAINTXTPASSWD, wxID_VGFMAINTXTPORT, 
 wxID_VGFMAINTXTUSR, 
] = [wx.NewId() for _init_ctrls in range(34)]

[wxID_VGFMAINTBMAINTOOL_LEFT_BOTTOM, wxID_VGFMAINTBMAINTOOL_LEFT_TOP, 
 wxID_VGFMAINTBMAINTOOL_RIGHT_BOTTOM, wxID_VGFMAINTBMAINTOOL_RIGHT_TOP, 
] = [wx.NewId() for _init_coll_tbMain_Tools in range(4)]

dictLunchApps={
    u'vPrjTimer':[u'vPrjTimer/vgaPrjTimer.py',u'vPrjTimer/vProjectTimer.exe'],
    u'vPrjEng':[u'vPrjEng/vgaPrjEng.py',u'vPrjEng/vPrjEng.exe'],
    u'vPrj':[u'vPrj/vgaPrj.py',u'vProject/vProject.exe'],
    u'vPrjDoc':[u'vPrjDoc/vgaPrjDoc.py',u'vPrjDoc/vPrjDoc.exe'],
    u'vHum':[u'vHum/vgaHum.py',u'vHum/vHuman.exe'],
    u'vDoc':[u'vDoc/vgaDoc.py',u'vDoc/vDoc.exe'],
    u'vSec':[],
    u'Net':[],
    u'Tools':[],
    u'vVSAS':[u'vVSAS/vgaVSAS.py',u'vVSAS/vVSAS.exe']
                }
class vgfMain(wx.Frame):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VGFMAIN, name=u'vgfMain', parent=prnt,
              pos=wx.Point(295, 139), size=wx.Size(556, 360),
              style=wx.MINIMIZE_BOX | wx.CLOSE_BOX | wx.SYSTEM_MENU | wx.CAPTION,
              title=u'vTestClient')
        self.SetClientSize(wx.Size(548, 333))
        self.Bind(wx.EVT_CLOSE, self.OnVgfMainClose)
        self.Bind(wx.EVT_PAINT, self.OnVgfMainPaint)
        self.Bind(wx.EVT_SIZE, self.OnVgfMainSize)

        self.pnInfo = wx.Panel(id=wxID_VGFMAINPNINFO, name=u'pnInfo',
              parent=self, pos=wx.Point(0, 0), size=wx.Size(548, 333),
              style=wx.TAB_TRAVERSAL)
        self.pnInfo.SetAutoLayout(True)

        self.lblTime = wx.StaticText(id=wxID_VGFMAINLBLTIME, label=u'',
              name=u'lblTime', parent=self.pnInfo, pos=wx.Point(392, 316),
              size=wx.Size(150, 13), style=0)
        self.lblTime.SetConstraints(LayoutAnchors(self.lblTime, False, False,
              True, True))

        self.lblHost = wx.StaticText(id=wxID_VGFMAINLBLHOST, label=u'Host',
              name=u'lblHost', parent=self.pnInfo, pos=wx.Point(8, 8),
              size=wx.Size(22, 13), style=0)

        self.txtHost = wx.TextCtrl(id=wxID_VGFMAINTXTHOST, name=u'txtHost',
              parent=self.pnInfo, pos=wx.Point(40, 4), size=wx.Size(64, 21),
              style=0, value=u'localhost')

        self.lblPort = wx.StaticText(id=wxID_VGFMAINLBLPORT, label=u'Port',
              name=u'lblPort', parent=self.pnInfo, pos=wx.Point(112, 8),
              size=wx.Size(19, 13), style=0)

        self.txtPort = wx.TextCtrl(id=wxID_VGFMAINTXTPORT, name=u'txtPort',
              parent=self.pnInfo, pos=wx.Point(136, 4), size=wx.Size(80, 21),
              style=0, value=u'')

        self.lblUsr = wx.StaticText(id=wxID_VGFMAINLBLUSR, label=u'User',
              name=u'lblUsr', parent=self.pnInfo, pos=wx.Point(224, 8),
              size=wx.Size(22, 13), style=0)

        self.txtUsr = wx.TextCtrl(id=wxID_VGFMAINTXTUSR, name=u'txtUsr',
              parent=self.pnInfo, pos=wx.Point(250, 4), size=wx.Size(60, 21),
              style=0, value=u'')

        self.txtPasswd = wx.TextCtrl(id=wxID_VGFMAINTXTPASSWD,
              name=u'txtPasswd', parent=self.pnInfo, pos=wx.Point(360, 4),
              size=wx.Size(76, 21), style=wx.TE_PASSWORD, value=u'')
        self.txtPasswd.SetToolTipString(u'password')

        self.lblPass = wx.StaticText(id=wxID_VGFMAINLBLPASS, label=u'Password',
              name=u'lblPass', parent=self.pnInfo, pos=wx.Point(312, 8),
              size=wx.Size(46, 13), style=0)

        self.cbClose = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGFMAINCBCLOSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Close', name=u'cbClose',
              parent=self.pnInfo, pos=wx.Point(344, 36), size=wx.Size(84, 30),
              style=0)
        self.cbClose.Bind(wx.EVT_BUTTON, self.OnCbCloseButton,
              id=wxID_VGFMAINCBCLOSE)

        self.lstConnections = wx.ListCtrl(id=wxID_VGFMAINLSTCONNECTIONS,
              name=u'lstConnections', parent=self.pnInfo, pos=wx.Point(232, 78),
              size=wx.Size(312, 116), style=wx.LC_REPORT)

        self.lblResult = wx.StaticText(id=wxID_VGFMAINLBLRESULT,
              label=u'Result', name=u'lblResult', parent=self.pnInfo,
              pos=wx.Point(232, 62), size=wx.Size(30, 13), style=0)

        self.cbShutDown = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGFMAINCBSHUTDOWN,
              bitmap=wx.EmptyBitmap(16, 16), label=u'ShutDown',
              name=u'cbShutDown', parent=self.pnInfo, pos=wx.Point(464, 36),
              size=wx.Size(84, 30), style=0)
        self.cbShutDown.Bind(wx.EVT_BUTTON, self.OnCbShutDownButton,
              id=wxID_VGFMAINCBSHUTDOWN)

        self.lblConnStatus = wx.StaticBitmap(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VGFMAINLBLCONNSTATUS, name=u'lblConnStatus',
              parent=self.pnInfo, pos=wx.Point(444, 8), size=wx.Size(16, 16),
              style=0)

        self.cbSend = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGFMAINCBSEND,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Send', name=u'cbSend',
              parent=self.pnInfo, pos=wx.Point(152, 60), size=wx.Size(76, 30),
              style=0)
        self.cbSend.Bind(wx.EVT_BUTTON, self.OnCbSendButton,
              id=wxID_VGFMAINCBSEND)

        self.tgConnect = wx.lib.buttons.GenBitmapTextToggleButton(ID=wxID_VGFMAINTGCONNECT,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Connect',
              name=u'tgConnect', parent=self.pnInfo, pos=wx.Point(464, 2),
              size=wx.Size(84, 30), style=0)
        self.tgConnect.SetToggle(False)
        self.tgConnect.Bind(wx.EVT_BUTTON, self.OnTgConnectButton,
              id=wxID_VGFMAINTGCONNECT)

        self.chcProtocol = wx.Choice(choices=[u'---', u'count_key',
              u'count_element', u'count_node', u'get_content', u'set_content',
              u'get_node', u'set_node', u'lock_node', u'unlock_node'],
              id=wxID_VGFMAINCHCPROTOCOL, name=u'chcProtocol',
              parent=self.pnInfo, pos=wx.Point(40, 64), size=wx.Size(106, 21),
              style=0)

        self.chcAppl = wx.Choice(choices=[u'vPrjEng'], id=wxID_VGFMAINCHCAPPL,
              name=u'chcAppl', parent=self.pnInfo, pos=wx.Point(40, 32),
              size=wx.Size(88, 21), style=0)

        self.txtAlias = wx.TextCtrl(id=wxID_VGFMAINTXTALIAS, name=u'txtAlias',
              parent=self.pnInfo, pos=wx.Point(192, 32), size=wx.Size(100, 21),
              style=0, value=u'')

        self.lblAppl = wx.StaticText(id=wxID_VGFMAINLBLAPPL, label=u'Appl',
              name=u'lblAppl', parent=self.pnInfo, pos=wx.Point(8, 32),
              size=wx.Size(21, 13), style=0)

        self.lblAlias = wx.StaticText(id=wxID_VGFMAINLBLALIAS, label=u'Alias',
              name=u'lblAlias', parent=self.pnInfo, pos=wx.Point(168, 32),
              size=wx.Size(22, 13), style=0)

        self.lblProtocol = wx.StaticText(id=wxID_VGFMAINLBLPROTOCOL,
              label=u'Proto.', name=u'lblProtocol', parent=self.pnInfo,
              pos=wx.Point(8, 68), size=wx.Size(28, 13), style=0)

        self.chkParam0 = wx.CheckBox(id=wxID_VGFMAINCHKPARAM0, label=u'Param0',
              name=u'chkParam0', parent=self.pnInfo, pos=wx.Point(8, 100),
              size=wx.Size(73, 13), style=0)
        self.chkParam0.SetValue(False)

        self.txtParam0 = wx.TextCtrl(id=wxID_VGFMAINTXTPARAM0,
              name=u'txtParam0', parent=self.pnInfo, pos=wx.Point(88, 96),
              size=wx.Size(100, 21), style=0, value=u'')

        self.chkParam1 = wx.CheckBox(id=wxID_VGFMAINCHKPARAM1, label=u'Param1',
              name=u'chkParam1', parent=self.pnInfo, pos=wx.Point(8, 124),
              size=wx.Size(73, 13), style=0)
        self.chkParam1.SetValue(False)

        self.txtParam1 = wx.TextCtrl(id=wxID_VGFMAINTXTPARAM1,
              name=u'txtParam1', parent=self.pnInfo, pos=wx.Point(88, 120),
              size=wx.Size(100, 21), style=0, value=u'')

        self.chkParam2 = wx.CheckBox(id=wxID_VGFMAINCHKPARAM2, label=u'Param2',
              name=u'chkParam2', parent=self.pnInfo, pos=wx.Point(8, 148),
              size=wx.Size(73, 13), style=0)
        self.chkParam2.SetValue(False)

        self.txtParam2 = wx.TextCtrl(id=wxID_VGFMAINTXTPARAM2,
              name=u'txtParam2', parent=self.pnInfo, pos=wx.Point(88, 144),
              size=wx.Size(100, 21), style=0, value=u'')

        self.chkParam3 = wx.CheckBox(id=wxID_VGFMAINCHKPARAM3, label=u'Param3',
              name=u'chkParam3', parent=self.pnInfo, pos=wx.Point(8, 172),
              size=wx.Size(73, 13), style=0)
        self.chkParam3.SetValue(False)

        self.txtParam3 = wx.TextCtrl(id=wxID_VGFMAINTXTPARAM3,
              name=u'txtParam3', parent=self.pnInfo, pos=wx.Point(88, 168),
              size=wx.Size(100, 21), style=0, value=u'')

        self.lstNodes = wx.ListView(id=wxID_VGFMAINLSTNODES, name=u'lstNodes',
              parent=self.pnInfo, pos=wx.Point(8, 200), size=wx.Size(216, 120),
              style=wx.LC_REPORT)

        self.cbNetPlace = wx.Button(id=wxID_VGFMAINCBNETPLACE,
              label=u'Net Place', name=u'cbNetPlace', parent=self.pnInfo,
              pos=wx.Point(448, 208), size=wx.Size(75, 23), style=0)
        self.cbNetPlace.Bind(wx.EVT_BUTTON, self.OnCbNetPlaceButton,
              id=wxID_VGFMAINCBNETPLACE)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.dom=vtXmlDom()
        self.host=None
        self.port=None
        self.srvSocket=None
        #img=images.getRunningBitmap()
        #self.cbLT.SetBitmapLabel(img)
        
        img=images.getStoppedBitmap()
        self.lblConnStatus.SetBitmap(img)
        
        img=images.getCloseBitmap()
        self.cbClose.SetBitmapLabel(img)
        
        img=images.getShutDownBitmap()
        self.cbShutDown.SetBitmapLabel(img)
        
        img=images.getUpdateBitmap()
        self.cbSend.SetBitmapLabel(img)
        
        self.tgConnect.SetBitmapLabel(images.getConnectBitmap())
        self.tgConnect.SetBitmapSelected(images.getConnectBitmap())
        
        self.lstConnections.InsertColumn(0,u'Nr',wx.LIST_FORMAT_RIGHT,60)
        self.lstConnections.InsertColumn(1,u'Value',wx.LIST_FORMAT_LEFT,230)
        
        self.lstNodes.InsertColumn(0,u'ID',wx.LIST_FORMAT_RIGHT,40)
        self.lstNodes.InsertColumn(1,u'tag',wx.LIST_FORMAT_LEFT,60)
        self.lstNodes.InsertColumn(2,u'tagname',wx.LIST_FORMAT_LEFT,100)
        
        self.cbClose.Enable(False)
        
        icon = getApplicationIcon()
        self.SetIcon(icon)
        if wx.Platform == '__WXMSW__o':
            self.TBMENU_RESTORE=wx.NewId()
            self.TBMENU_CLOSE=wx.NewId()
            # setup a taskbar icon, and catch some events from it
            self.tbicon = wx.TaskBarIcon()
            self.tbicon.SetIcon(icon, "VIDARC Luncher")
            wx.EVT_TASKBAR_LEFT_DCLICK(self.tbicon, self.OnTaskBarActivate)
            wx.EVT_TASKBAR_RIGHT_UP(self.tbicon, self.OnTaskBarMenu)
            wx.EVT_MENU(self.tbicon, self.TBMENU_RESTORE, self.OnTaskBarActivate)
            wx.EVT_MENU(self.tbicon, self.TBMENU_CLOSE, self.OnTaskBarClose)

        # setup statusbar
        self.timer = wx.PyTimer(self.Notify)
        self.timer.Start(1000)
        self.Notify()
        self.iPos=0
        self.txtHost.SetValue('kant')
        self.txtPort.SetValue('50000')
        self.txtUsr.SetValue('wal')
        self.txtPasswd.SetValue('wal')
        self.chcAppl.SetSelection(0)
        self.txtAlias.SetValue('vTst')
        self.chcProtocol.SetSelection(0)
    def Notify(self):
        t = time.localtime(time.time())
        st = time.strftime("%d-%b-%Y   %H:%M:%S wk:%U", t)
        self.lblTime.SetLabel(st)
        pass
    def OnTaskBarActivate(self, evt):
        if self.IsIconized():
            self.Iconize(False)
        if not self.IsShown():
            self.Show(True)
        self.Raise()

    def OnTaskBarMenu(self, evt):
        menu = wx.Menu()
        #menu.Append(self.TBMENU_RESTORE, "Restore VIDARC Luncher")
        it=wx.MenuItem(menu,help='', id=self.TBMENU_RESTORE,
                kind=wx.ITEM_NORMAL, text=u"Restore VIDARC Luncher")
        it.SetBitmap(images.getOpenBitmap())
        menu.AppendItem(it)
        menu.AppendSeparator()
        #menu.Append(self.TBMENU_CLOSE,   "Close")
        it=wx.MenuItem(menu,help='', id=self.TBMENU_CLOSE,
                kind=wx.ITEM_NORMAL, text=u"Close")
        it.SetBitmap(images.getCancelBitmap())
        menu.AppendItem(it)
        self.tbicon.PopupMenu(menu)
        menu.Destroy()

    #---------------------------------------------
    def OnTaskBarClose(self, evt):
        self.Close()
        #self.Destroy()

    def OnVgfMainClose(self, event):
        if hasattr(self, "tbicon"):
            #del self.tbicon
            self.tbicon.Destroy()
            #del self.tbicon
        self.Destroy()
        
    def OnVgfMainPaint(self, event):
        #self.__update__()
        event.Skip()

    def OnVgfMainSize(self, event):
        size=event.GetSize()
        #size=event.GetClientSize()
        #dw=wsize.GetWidth()-size.GetWidth()
        #dy=wsize.GetWidth()-size.GetHeight()
        w=size.GetWidth()
        h=size[1]
        bMod=False
        if w<400:
            w=300
            bMod=True
        if h<200:
            h=200
            bMod=True
        dispSize=wx.ClientDisplayRect()
        if w>dispSize[2]:
            w=dispSize[2]
            bMod=True
        if h>dispSize[3]:
            h=dispSize[3]
            bMod=True
        if  bMod==True:
            self.SetSize(wx.Size(w,h))
            #self.__update__()
        event.Skip()
    def OnCbCloseButton(self, event):
        
        event.Skip()

    def OnCbShutDownButton(self, event):
        if self.srvSock is not None:
            self.srvSock.ShutDown()
        event.Skip()

    def OnTgConnectButton(self, event):
        if self.tgConnect.GetValue():
            bConnect=False
            bFlt=False
            if self.srvSocket is None:
                bConnect=True
            try:
                host=self.txtHost.GetValue()
                port=int(self.txtPort.GetValue())
            except:
                bFlt=True
            if self.host is None:
                bConnect=True
                self.host=host
                self.port=port
            else:
                if bFlt:
                    return
                if host!=self.host:
                    self.host=host
                    bConnect=True
                if port!=self.port:
                    self.port=port
                    bConnect=True
            if bConnect:
                print 'connect',self.host,self.port
                self.cltServ=vtNetClt(self,self.host,self.port,vtNetXmlGuiCltSock,1)
                self.cltServ.Connect()
                self.srvSock=self.cltServ.GetSocketInstance()
                if self.srvSock is None:
                    print '   refused'
                    self.tgConnect.SetValue(0)
                    return
                print '   accepted'
                self.srvSock.SetDoc(self.dom)
                self.srvSock.SetParent(self)
                img=images.getLoginBitmap()
                self.lblConnStatus.SetBitmap(img)
                self.srvSock.Login(self.txtUsr.GetValue(),self.txtPasswd.GetValue())
                self.srvSock.Alias(self.chcAppl.GetStringSelection(),self.txtAlias.GetValue())
        else:
            #img=images.getStoppedBitmap()
            #self.lblConnStatus.SetBitmap(img)
            self.cltServ.Close()
            #self.__connectionClosed__()
            #del self.cltServ
            #self.cltServ=None
            #self.srvSock=None
            #self.host=None
            #self.port=None
        event.Skip()
    def __connectionClosed__(self):
        del self.cltServ
        self.cltServ=None
        self.srvSock=None
        self.host=None
        self.port=None
        self.tgConnect.SetValue(0)
        img=images.getStoppedBitmap()
        self.lblConnStatus.SetBitmap(img)
    def OnCbSendButton(self, event):
        if self.chcProtocol.GetSelection()==0:
            return
        sel=self.chcProtocol.GetStringSelection()
        if sel in [u'count_key',u'count_element',u'count_node',
                    u'get_content']:
            self.srvSock.SendTelegram(sel)
        elif sel in [u'get_node',u'lock_node',u'unlock_node']:
            self.srvSock.SendTelegram(sel+'|'+self.txtParam0.GetValue())
        elif sel==u'set_node':
            self.srvSock.set_node(self.txtParam0.GetValue())
        elif sel==u'set_content':
            self.srvSock.set_content()
        event.Skip()
    def NotifyLogin(self):
        img=images.getLoginBitmap()
        self.lblConnStatus.SetBitmap(img)
        self.txtPasswd.SetFocus()
        pass
    def NotifyLoggedIn(self):
        img=images.getRunningBitmap()
        self.lblConnStatus.SetBitmap(img)
        pass
    def NotifyCountKey(self,count):
        self.lstConnections.DeleteAllItems()
        index = self.lstConnections.InsertImageStringItem(sys.maxint, str(count), -1)
    def NotifyCountElement(self,count):
        self.lstConnections.DeleteAllItems()
        index = self.lstConnections.InsertImageStringItem(sys.maxint, str(count), -1)
    def NotifyCountNode(self,count):
        self.lstConnections.DeleteAllItems()
        index = self.lstConnections.InsertImageStringItem(sys.maxint, str(count), -1)
    def NotifyContent(self):
        
        self.lstConnections.DeleteAllItems()
        self.lstNodes.DeleteAllItems()
        print '+++++++++++++++++++++++++'
        print '++  vtXmlDom'
        print self.dom.GetXmlContent()
        print '-------------------------'
        self.dom.genIds()
        ids=self.dom.getIds()
        print ids
        keys=ids.GetIDs()
        print keys
        keys.sort()
        print keys
        for k in keys:
            tup=ids.GetId(k)
            print tup
            index = self.lstNodes.InsertImageStringItem(sys.maxint, str(k), -1)
            try:
                self.lstNodes.SetStringItem(index,1,self.dom.getTagName(tup[1]),-1)
                self.lstNodes.SetStringItem(index,2,self.dom.getNodeText(tup[1],'tag'),-1)
            except:
                pass
    def NotifySetNode(self,content):
        self.lstConnections.DeleteAllItems()
        index = self.lstConnections.InsertImageStringItem(sys.maxint, 'set_node', -1)
        self.lstConnections.SetStringItem(index,1,content,-1)
    def NotifyLockNode(self,id,content):
        self.lstConnections.DeleteAllItems()
        index = self.lstConnections.InsertImageStringItem(sys.maxint, 'lock_node', -1)
        self.lstConnections.SetStringItem(index,1,content,-1)
    def NotifyUnLockNode(self,id,content):
        self.lstConnections.DeleteAllItems()
        index = self.lstConnections.InsertImageStringItem(sys.maxint, 'unlock_node', -1)
        self.lstConnections.SetStringItem(index,1,content,-1)
    def NotifyFault(self,origin,fault):
        pass

    def OnCbNetPlaceButton(self, event):
        dlg=vNetPlaceDialog(self)
        dlg.Centre()
        dlg.Set('vPrjEng','vTst','kant','50000','wal','wal')
        ret=dlg.ShowModal()
        dlg.Destroy()
        event.Skip()
    
class vtNetXmlGuiCltSock(vtNetXmlCltSock):
    def Alias(self,appl,alias):
        self.SendTelegram('alias|'+appl+':'+alias)
    def __datConn__(self,val):
        i=string.find(val,'|')
        if i>=0:
            lst=string.split(val[i+1:],',')
        else:
            lst=[]
        self.par.NotifyDatConn(lst)
    
