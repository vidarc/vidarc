#Boa:Dialog:vConfigDialog
#----------------------------------------------------------------------------
# Name:         vXmlSrvMainFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vConfigDialog.py,v 1.5 2006/07/17 10:36:37 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import images
import vidarc.tool.net.img_appl as img_appl
import vidarc.tool.log.vtLog as vtLog
import sys

VERBOSE=0

def create(parent):
    return vConfigDialog(parent)

[wxID_VCONFIGDIALOG, wxID_VCONFIGDIALOGCBADD, wxID_VCONFIGDIALOGCBAPPLY, 
 wxID_VCONFIGDIALOGCBCANCEL, wxID_VCONFIGDIALOGCBDEL, wxID_VCONFIGDIALOGCBSET, 
 wxID_VCONFIGDIALOGLBLALIAS, wxID_VCONFIGDIALOGLBLHOST, 
 wxID_VCONFIGDIALOGLBLNAME, wxID_VCONFIGDIALOGLBLPORT, 
 wxID_VCONFIGDIALOGLSTAPPLS, wxID_VCONFIGDIALOGTXTALIAS, 
 wxID_VCONFIGDIALOGTXTHOST, wxID_VCONFIGDIALOGTXTNAME, 
 wxID_VCONFIGDIALOGTXTPORT, 
] = [wx.NewId() for _init_ctrls in range(15)]

class vConfigDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VCONFIGDIALOG, name=u'vConfigDialog',
              parent=prnt, pos=wx.Point(392, 256), size=wx.Size(465, 270),
              style=wx.DEFAULT_DIALOG_STYLE, title=u'vServer Config Dialog')
        self.SetClientSize(wx.Size(457, 243))

        self.lblName = wx.StaticText(id=wxID_VCONFIGDIALOGLBLNAME,
              label=u'Name', name=u'lblName', parent=self, pos=wx.Point(8, 8),
              size=wx.Size(28, 13), style=0)

        self.txtName = wx.TextCtrl(id=wxID_VCONFIGDIALOGTXTNAME,
              name=u'txtName', parent=self, pos=wx.Point(40, 4),
              size=wx.Size(296, 21), style=0, value=u'')
        self.txtName.Enable(False)

        self.lblPort = wx.StaticText(id=wxID_VCONFIGDIALOGLBLPORT,
              label=u'Port', name=u'lblPort', parent=self, pos=wx.Point(182,
              36), size=wx.Size(19, 13), style=0)

        self.lblHost = wx.StaticText(id=wxID_VCONFIGDIALOGLBLHOST,
              label=u'Host', name=u'lblHost', parent=self, pos=wx.Point(78, 36),
              size=wx.Size(22, 13), style=0)

        self.lblAlias = wx.StaticText(id=wxID_VCONFIGDIALOGLBLALIAS,
              label=u'Alias', name=u'lblAlias', parent=self, pos=wx.Point(270,
              36), size=wx.Size(22, 13), style=0)

        self.txtHost = wx.TextCtrl(id=wxID_VCONFIGDIALOGTXTHOST,
              name=u'txtHost', parent=self, pos=wx.Point(72, 52),
              size=wx.Size(100, 21), style=0, value=u'')

        self.txtPort = wx.TextCtrl(id=wxID_VCONFIGDIALOGTXTPORT,
              name=u'txtPort', parent=self, pos=wx.Point(176, 52),
              size=wx.Size(76, 21), style=0, value=u'')

        self.txtAlias = wx.TextCtrl(id=wxID_VCONFIGDIALOGTXTALIAS,
              name=u'txtAlias', parent=self, pos=wx.Point(260, 52),
              size=wx.Size(100, 21), style=0, value=u'')

        self.cbSet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VCONFIGDIALOGCBSET,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Set', name=u'cbSet',
              parent=self, pos=wx.Point(376, 48), size=wx.Size(76, 30),
              style=0)
        self.cbSet.Bind(wx.EVT_BUTTON, self.OnCbSetButton,
              id=wxID_VCONFIGDIALOGCBSET)

        self.cbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VCONFIGDIALOGCBADD,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'cbAdd',
              parent=self, pos=wx.Point(376, 88), size=wx.Size(76, 30),
              style=0)
        self.cbAdd.Bind(wx.EVT_BUTTON, self.OnCbAddButton,
              id=wxID_VCONFIGDIALOGCBADD)

        self.cbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VCONFIGDIALOGCBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Del', name=u'cbDel',
              parent=self, pos=wx.Point(376, 128), size=wx.Size(76, 30),
              style=0)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VCONFIGDIALOGCBDEL)

        self.lstAppls = wx.ListCtrl(id=wxID_VCONFIGDIALOGLSTAPPLS,
              name=u'lstAppls', parent=self, pos=wx.Point(8, 80),
              size=wx.Size(352, 112), style=wx.LC_REPORT)
        self.lstAppls.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstApplsListItemDeselected, id=wxID_VCONFIGDIALOGLSTAPPLS)
        self.lstAppls.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstApplsListItemSelected, id=wxID_VCONFIGDIALOGLSTAPPLS)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VCONFIGDIALOGCBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Apply', name=u'cbApply',
              parent=self, pos=wx.Point(128, 208), size=wx.Size(76, 30),
              style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VCONFIGDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VCONFIGDIALOGCBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel', name=u'cbCancel',
              parent=self, pos=wx.Point(232, 208), size=wx.Size(76, 30),
              style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VCONFIGDIALOGCBCANCEL)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.doc=None
        self.node=None
        self.verbose=VERBOSE
        
        self.cbSet.SetBitmapLabel(images.getApplyBitmap())
        self.cbAdd.SetBitmapLabel(images.getAddBitmap())
        self.cbDel.SetBitmapLabel(images.getDelBitmap())
        self.cbApply.SetBitmapLabel(images.getApplyBitmap())
        self.cbCancel.SetBitmapLabel(images.getCancelBitmap())
        
        self.dictImg={}
        self.imgLst=wx.ImageList(16,16)
        self.dictImg['vLoc']=self.imgLst.Add(img_appl.getLocBitmap())
        self.dictImg['vHum']=self.imgLst.Add(img_appl.getHumBitmap())
        self.dictImg['vCustomer']=self.imgLst.Add(img_appl.getCustomerBitmap())
        self.dictImg['vDoc']=self.imgLst.Add(img_appl.getDocBitmap())
        self.dictImg['vMsg']=self.imgLst.Add(img_appl.getMsgBitmap())
        self.dictImg['vGlobals']=self.imgLst.Add(img_appl.getGlobalsBitmap())
        self.dictImg['vPrj']=self.imgLst.Add(img_appl.getPrjBitmap())
        self.dictImg['vTask']=self.imgLst.Add(img_appl.getTaskBitmap())
        self.dictImg['vPrjDoc']=self.imgLst.Add(img_appl.getPrjDocBitmap())
        self.dictImg['vPrjEng']=self.imgLst.Add(img_appl.getPrjEngBitmap())
        self.dictImg['vPlugIn']=self.imgLst.Add(img_appl.getPlugInBitmap())
        self.dictImg['vPlugInFile']=self.imgLst.Add(img_appl.getPlugInBitmap())
        self.lstAppls.SetImageList(self.imgLst,wx.IMAGE_LIST_NORMAL)
        self.lstAppls.SetImageList(self.imgLst,wx.IMAGE_LIST_SMALL)

        self.lstAppls.InsertColumn(0,u'Appl',wx.LIST_FORMAT_LEFT,90)
        self.lstAppls.InsertColumn(1,u'Host',wx.LIST_FORMAT_LEFT,90)
        self.lstAppls.InsertColumn(2,u'Port',wx.LIST_FORMAT_LEFT,50)
        self.lstAppls.InsertColumn(3,u'Alias',wx.LIST_FORMAT_LEFT,100)

    def SetNode(self,doc,node,name,appls):
        self.idxSel=-1
        self.txtName.SetValue(name)
        self.txtAlias.SetValue('')
        self.txtHost.SetValue('')
        self.txtPort.SetValue('')
        self.doc=doc
        self.node=node
        self.nodeCfg=None
        self.appls=appls
        self.hosts=None
        if self.verbose:
            vtLog.vtLogCallDepth(self,'node:%s'%self.node)
        self.lstAppls.DeleteAllItems()
        for a in appls:
            applNode=self.doc.getChild(self.node,a)
            childs=self.doc.getChilds(applNode,'host')
            #print a
            try:
                img=self.dictImg[a]
            except:
                img=-1
            #print self.dictImg
            #print img
            if len(childs)<=0:
                idx=self.lstAppls.InsertImageStringItem(sys.maxint, a, img)
            for n in childs:
                idx=self.lstAppls.InsertImageStringItem(sys.maxint, a, img)
                s=self.doc.getNodeText(n,'name')
                self.lstAppls.SetStringItem(idx,1,s,-1)
                
                s=self.doc.getNodeText(n,'port')
                self.lstAppls.SetStringItem(idx,2,s,-1)
                
                s=self.doc.getNodeText(n,'alias')
                self.lstAppls.SetStringItem(idx,3,s,-1)
    def GetAliasDict(self):
        d={}
        for a in self.appls:
            d[a]=[]
        for i in range(self.lstAppls.GetItemCount()):
            a=self.lstAppls.GetItemText(i)
            lst=d[a]
            sName=self.lstAppls.GetItem(i,1).m_text
            sPort=self.lstAppls.GetItem(i,2).m_text
            sAlias=self.lstAppls.GetItem(i,3).m_text
            lst.append([a,sName,sPort,sAlias])
        def cmpFunc(a,b):
            for i in range(4):
                res=cmp(a[i],b[i])
                if res!=0:
                    return res
            return 0
        for a in self.appls:
            lst=d[a]
            lst.sort(cmpFunc)
        return d
    def AddAlias(self,sAppl,sHost,sPort,sAlias):
        try:
            def add2Lst(sAppl,sHost,sPort,sAlias):
                idx=self.lstAppls.InsertStringItem(sys.maxint,sAppl)
                self.lstAppls.SetStringItem(idx,1,sHost)
                self.lstAppls.SetStringItem(idx,2,sPort)
                self.lstAppls.SetStringItem(idx,3,sAlias)
            def chk2Add(sAppl,sHost,sPort,sAlias,l):
                for tup in l:
                    if tup[0]!=sAppl:
                        continue
                    elif tup[1]!=sHost:
                        continue
                    elif tup[2]!=sPort:
                        continue
                    elif tup[3]!=sAlias:
                        continue
                    else:
                        return False
                return True
            def chkAdd(sAppl,sHost,sPort,sAlias,l):
                #vtLog.CallStack('')
                #print 'appl:%s host:%s port:%s alias:%s'%(sAppl,sHost,sPort,sAlias)
                iFree=0x0
                if sHost=='':
                    iFree|=0x1
                if sPort=='':
                    iFree|=0x2
                if sAlias=='':
                    iFree|=0x4
                for tup in l:
                    if (iFree&0x1)!=0:
                        sHost=tup[1]
                    if (iFree&0x2)!=0:
                        sPort=tup[2]
                    if (iFree&0x4)!=0:
                        sAlias=tup[3]
                    if chk2Add(sAppl,sHost,sPort,sAlias,l):
                        #print 'appl:%s host:%s port:%s alias:%s'%(sAppl,sHost,sPort,sAlias)
                        add2Lst(sAppl,sHost,sPort,sAlias)
                        l.append([sAppl,sHost,sPort,sAlias])
            d=self.GetAliasDict()
            #vtLog.CallStack('')
            #vtLog.pprint(d)
            if sAppl=='':
                for k in d.keys():
                    chkAdd(k,sHost,sPort,sAlias,d[k])
            else:
                chkAdd(sAppl,sHost,sPort,sAlias,d[sAppl])
            self.GetNode()
            return 1
        except:
            import traceback
            traceback.print_exc()
            return -1
    def GetNode(self):
        d=self.GetAliasDict()
        for a in self.appls:
            applNode=self.doc.getChildForced(self.node,a)
            hosts=self.doc.getChilds(applNode,'host')
            lst=d[a]
            for tup,n in zip(lst,hosts):
                self.doc.setNodeText(n,'name',tup[1])
                self.doc.setNodeText(n,'port',tup[2])
                self.doc.setNodeText(n,'alias',tup[3])
            if len(lst)>len(hosts):
                for tup in lst[len(hosts):]:
                    n=self.doc.createSubNode(applNode,'host')
                    self.doc.setNodeText(n,'name',tup[1])
                    self.doc.setNodeText(n,'port',tup[2])
                    self.doc.setNodeText(n,'alias',tup[3])
            elif len(hosts)>len(lst):
                for n in hosts[len(lst):]:
                    self.doc.deleteNode(n)
        self.doc.AlignNode(self.node,iRec=2)
        #if self.verbose:
        #    vtLog.vtLogCallDepth(self,'node:%s'%self.node)
        pass

    def OnCbSetButton(self, event):
        if self.idxSel<0:
            return
        sHost=self.txtHost.GetValue()
        sPort=self.txtPort.GetValue()
        if len(sHost)<=0:
            return
        if len(sPort)<=0:
            return
        try:
            i=int(sPort)
        except:
            sPort=u'50000'
        sAlias=self.txtAlias.GetValue()
        
        self.lstAppls.SetStringItem(self.idxSel,1,sHost,-1)
        self.lstAppls.SetStringItem(self.idxSel,2,sPort,-1)
        self.lstAppls.SetStringItem(self.idxSel,3,sAlias,-1)
        #self.txtHost.SetValue('')
        #self.txtPort.SetValue('')
        self.txtAlias.SetValue('')
        event.Skip()

    def OnLstApplsListItemDeselected(self, event):
        self.idxSel=-1
        event.Skip()

    def OnLstApplsListItemSelected(self, event):
        self.idxSel=event.GetIndex()
        for i in [1,2,3]:
            it=self.lstAppls.GetItem(self.idxSel,i)
            if len(it.m_text)>0:
                if i==1:
                    self.txtHost.SetValue(it.m_text)
                elif i==2:
                    self.txtPort.SetValue(it.m_text)
                elif i==3:
                    self.txtAlias.SetValue(it.m_text)
                
        event.Skip()

    def OnCbAddButton(self, event):
        if self.idxSel<0:
            return
        sHost=self.txtHost.GetValue()
        sPort=self.txtPort.GetValue()
        if len(sHost)<=0:
            return
        if len(sPort)<=0:
            return
        try:
            i=int(sPort)
        except:
            sPort=u'50000'
        sAlias=self.txtAlias.GetValue()
        # find last index
        idx=self.idxSel
        appl=self.lstAppls.GetItemText(idx)
        iCount=self.lstAppls.GetItemCount()
        idx+=1
        while (idx<iCount) and (appl==self.lstAppls.GetItemText(idx)):
            idx+=1
        try:
            img=self.dictImg[appl]
        except:
            img=-1
        idx=self.lstAppls.InsertImageStringItem(idx,appl,img)
        self.lstAppls.SetStringItem(idx,1,sHost,-1)
        self.lstAppls.SetStringItem(idx,2,sPort,-1)
        self.lstAppls.SetStringItem(idx,3,sAlias,-1)
        #self.txtHost.SetValue('')
        #self.txtPort.SetValue('')
        self.txtAlias.SetValue('')
        event.Skip()

    def OnCbDelButton(self, event):
        if self.idxSel<0:
            return
        appl=self.lstAppls.GetItemText(self.idxSel)
        idx=self.idxSel
        iCount=self.lstAppls.GetItemCount()
        idx+=1
        while (idx<iCount) and (appl==self.lstAppls.GetItemText(idx)):
            idx+=1
        idxStart=self.idxSel
        idxStart-=1
        while (idxStart>=0) and (appl==self.lstAppls.GetItemText(idxStart)):
            idxStart-=1
        idxStart+=1
        iCount=idx-idxStart
        #print self.idxSel,idxStart,idx,iCount
        
        if iCount>1:
            # delete
            self.lstAppls.DeleteItem(self.idxSel)
            self.idxSel=-1
        else:
            self.lstAppls.SetStringItem(self.idxSel,1,'',-1)
            self.lstAppls.SetStringItem(self.idxSel,2,'',-1)
            self.lstAppls.SetStringItem(self.idxSel,3,'',-1)
            
        event.Skip()

    def OnCbApplyButton(self, event):
        self.GetNode()
        self.EndModal(1)
        event.Skip()

    def OnCbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()

