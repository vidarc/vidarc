#Boa:Dialog:vCloseDialog
#----------------------------------------------------------------------------
# Name:         vCloseDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vCloseDialog.py,v 1.2 2007/07/30 09:05:05 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

import thread,traceback,time

def create(parent):
    return vCloseDialog(parent)

[wxID_VCLOSEDIALOG, wxID_VCLOSEDIALOGLBLMSG, wxID_VCLOSEDIALOGLISTBOX1, 
 wxID_VCLOSEDIALOGPBPROCESS, 
] = [wx.NewId() for _init_ctrls in range(4)]

class thdCloseDialog:
    def __init__(self,dlg,verbose=0):
        self.verbose=verbose
        self.dlg=dlg
        self.running=False
    def DoClose(self,srvXml,lunchers,func,*args,**kwargs):
        self.srvXml=srvXml
        self.lunchers=lunchers
        self.func=func
        self.args=args
        self.kwargs=kwargs
        self.keepGoing = self.running = True
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        thread.start_new_thread(self.Run, ())

    def Stop(self):
        self.keepGoing = False

    def IsRunning(self):
        return self.running
    
    def Run(self):
        try:
            iCount=0
            for appl,panel in self.lunchers:
                d=panel.GetLunchs()
                keys=d.keys()
                keys.sort()
                iCount+=1+len(keys)
                
            self.dlg.ProcessSize(iCount)
            iPos=0
            for appl,panel in self.lunchers:
                iPos+=1
                d=panel.GetLunchs()
                keys=d.keys()
                keys.sort()
                for k in keys:
                    iPos+=1
                    self.dlg.Process(iPos)
                    self.dlg.XmlClose(appl,d[k]['alias'])
                    bRet=self.srvXml.IsServedXml(appl+':'+d[k]['alias'])
                    if bRet:
                        self.srvXml.DelServedXml(appl+':'+d[k]['alias'])
                        panel.Stop(k,d[k]['alias'])
                    self.dlg.XmlClosed(appl,d[k]['alias'])
            self.dlg.Process(iCount)
            time.sleep(1)
            if self.func is not None:
                self.func(*self.args,**self.kwargs)
        except:
            traceback.print_exc()
        self.keepGoing = self.running = False


class vCloseDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VCLOSEDIALOG, name=u'vCloseDialog',
              parent=prnt, pos=wx.Point(324, 74), size=wx.Size(262, 273),
              style=wx.DEFAULT_DIALOG_STYLE, title=u'vSrv Close Dialog')
        self.SetClientSize(wx.Size(254, 246))

        self.lblMsg = wx.StaticText(id=wxID_VCLOSEDIALOGLBLMSG,
              label=_(u'stopping thread and closing connections please wait ...'),
              name=u'lblMsg', parent=self, pos=wx.Point(16, 8),
              size=wx.Size(225, 13), style=0)

        self.listBox1 = wx.ListBox(choices=[], id=wxID_VCLOSEDIALOGLISTBOX1,
              name='listBox1', parent=self, pos=wx.Point(8, 32),
              size=wx.Size(240, 184), style=0)

        self.pbProcess = wx.Gauge(id=wxID_VCLOSEDIALOGPBPROCESS,
              name=u'pbProcess', parent=self, pos=wx.Point(8, 224), range=60,
              size=wx.Size(240, 13), style=wx.GA_HORIZONTAL | wx.GA_SMOOTH)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.lunchers=None
        self.thdClose=thdCloseDialog(self)
    def SetLunchers(self,srvXml,lunchers):
        self.srvXml=srvXml
        self.lunchers=lunchers
    def ShowModal(self):
        self.thdClose.DoClose(self.srvXml,self.lunchers,self.OnFin)
        wx.Dialog.ShowModal(self)
    def XmlClose(self,appl,alias):
        msg=_(u'served xml')+u' '+appl+u':'+alias+u' '+_(u'closing')
        self.listBox1.Insert(msg,0)
    def XmlClosed(self,appl,alias):
        msg=_(u'served xml')+u' '+appl+u':'+alias+u' '+_(u'closed')
        self.listBox1.Insert(msg,0)
    def ProcessSize(self,iSize):
        self.pbProcess.SetValue(0)
        self.pbProcess.SetRange(iSize)
    def Process(self,iPos):
        self.pbProcess.SetValue(iPos)
    def OnFin(self):
        self.EndModal(1)
