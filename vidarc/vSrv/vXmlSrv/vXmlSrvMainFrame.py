#Boa:Frame:vXmlSrvMainFrame
#----------------------------------------------------------------------------
# Name:         vXmlSrvMainFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vXmlSrvMainFrame.py,v 1.11 2007/07/30 09:05:05 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
from wx.lib.anchors import LayoutAnchors
import time
import images

import vidarc.config.vcCust as vcCust
vcCust.set2Import('vidarc.ext',False)
vcCust.set2Import('vidarc.tool',False)
vcCust.set2Import('vidarc.vApps',False)

from vidarc.vApps.vHum.vXmlHum import *
from vidarc.vApps.vDoc.vXmlDoc import *
from vidarc.vApps.vGlobals.vXmlGlobals import *
from vidarc.vApps.vLoc.vXmlLoc import *
from vidarc.vApps.vPrj.vXmlPrj import *
from vidarc.vApps.vPrjDoc.vXmlPrjDoc import *
from vidarc.vApps.vPrjEng.vXmlPrjEng import *
from vidarc.vApps.vPrjTimer.vXmlTask import *
from vidarc.vApps.vPrjTimer.vXmlPrjTimer import *
from vidarc.vApps.vPrjPlan.vXmlPrjPlan import *
from vidarc.vApps.vBill.vXmlBill import *
from vidarc.vSrv.vPlugInSrv.vXmlPlugInSrv import *
from vidarc.vApps.vPurchase.vXmlPurchase import *
from vidarc.vApps.vSupplier.vXmlSupplier import *
from vidarc.vApps.vRessource.vXmlRessource import *
from vidarc.vApps.vSoftware.vXmlSoftware import *
from vidarc.vApps.vCustomer.vXmlCustomer import *
from vidarc.tool.net.vtNetXml import *
from vidarc.vSrv.vXmlSrv.vConfigDialog import *
#from vidarc.vSrv.vXmlSrv.vSecAclDialog import *
from vidarc.tool.sec.vtSecAclDomDialog import *

from vidarc.vSrv.vXmlSrv.vCloseDialog import *
from vidarc.tool.xml.vtXmlDom import *

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import getpass
import string,os,os.path
import vidarc.tool.InOut.fnUtil as fnUtil

from vidarc.vSrv.vXmlSrv.vInfoPanel import *
from vidarc.vSrv.vXmlSrv.vSettingsPanel import *
from vidarc.vSrv.vXmlSrv.vToolsPanel import *

from vidarc.vApps.vPrjEng.Elements import ELEMENTS as vPrjEngElem

VERBOSE=2

APPLS_MAP={
    'vCustomer':['vHum','vGlobals'],
    'vDoc':['vHum','vGlobals'],
    'vHum':['vGlobals','vMsg'],
    'vLoc':['vGlobals'],
    'vGlobals':['vHum'],
    'vTask':[],
    'vPrj':['vCustomer','vHum','vGlobals'],
    'vPrjDoc':['vHum','vDoc','vGlobals'],
    'vPrjEng':['vHum','vDoc','vPrj','vPrjDoc','vTask','vGlobals'],
    'vPrjPlan':['vHum','vDoc','vPrjDoc','vPrj','vTask','vGlobals'],
    'vPrjTimer':['vHum','vLoc','vDoc','vPrjDoc','vPrj','vTask','vGlobals'],
    'vRessource':['vHum','vDoc','vPrjDoc','vPrj','vGlobals'],
    'vSoftware':['vHum','vDoc','vPrjDoc','vPrj','vGlobals'],
    'vBill':['vPrjDoc','vPrj','vDoc','vHum','vGlobals'],
    'vPlugIn':['vHum','vPlugInFile'],
    'vPurchase':['vSupplier','vPrjDoc','vPrj','vDoc','vHum','vGlobals'],
    'vSoftware':['vCustomer','vPrjDoc','vPrj','vDoc','vHum','vGlobals'],
    'vSupplier':['vHum','vGlobals'],
    }    
def create(parent):
    return vXmlSrvMainFrame(parent)

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon
    
[wxID_VXMLSRVMAINFRAME, wxID_VXMLSRVMAINFRAMECBCLOSE, 
 wxID_VXMLSRVMAINFRAMECBHOLLOW, wxID_VXMLSRVMAINFRAMECBLB, 
 wxID_VXMLSRVMAINFRAMECBLT, wxID_VXMLSRVMAINFRAMECBLUNCH, 
 wxID_VXMLSRVMAINFRAMECBRB, wxID_VXMLSRVMAINFRAMECBRT, 
 wxID_VXMLSRVMAINFRAMECHCAUTOCOLLAPSE, wxID_VXMLSRVMAINFRAMELBLTIME, 
 wxID_VXMLSRVMAINFRAMEPNINFO, wxID_VXMLSRVMAINFRAMETGBEXPAND, 
] = [wx.NewId() for _init_ctrls in range(12)]

[wxID_VGFMAINTBMAINTOOL_LEFT_BOTTOM, wxID_VGFMAINTBMAINTOOL_LEFT_TOP, 
 wxID_VGFMAINTBMAINTOOL_RIGHT_BOTTOM, wxID_VGFMAINTBMAINTOOL_RIGHT_TOP, 
] = [wx.NewId() for _init_coll_tbMain_Tools in range(4)]

dictLunchApps={
    u'vPrjTimer':[u'vPrjTimer/vgaPrjTimer.py',u'vPrjTimer/vProjectTimer.exe'],
    u'vPrjEng':[u'vPrjEng/vgaPrjEng.py',u'vPrjEng/vPrjEng.exe'],
    u'vPrj':[u'vPrj/vgaPrj.py',u'vProject/vProject.exe'],
    u'vPrjDoc':[u'vPrjDoc/vgaPrjDoc.py',u'vPrjDoc/vPrjDoc.exe'],
    u'vPrjPlan':[u'vPrjDoc/vPrjPlanAppl.py',u'vPrjDoc/vPrjPlan.exe'],
    u'vHum':[u'vHum/vgaHum.py',u'vHum/vHuman.exe'],
    u'vDoc':[u'vDoc/vgaDoc.py',u'vDoc/vDoc.exe'],
    u'vSoftware':[u'vSoftware/vSoftwareAppl.py',u'vSoftware/vSoftware.exe'],
    u'vTask':[],
    u'vLoc':[],
    u'vCustomer':[],
    u'vRessource':[],
    u'vSec':[],
    u'vGlobals':[],
    u'Net':[],
    u'Tools':[],
    u'vVSAS':[u'vVSAS/vgaVSAS.py',u'vVSAS/vVSAS.exe']
                }
lstAppl=[u'all',u'service',
    u'vPrjTimer',u'vPrjEng',u'vPrj',u'vPrjDoc',u'vPrjPlan',
    u'vRessource',u'vBill',u'vPurchase',u'vSupplier',u'vSoftware',
    u'vHum',u'vDoc',u'vTask',u'vLoc','vGlobals']
    
class vXmlSrvMainFrame(wx.Frame):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VXMLSRVMAINFRAME,
              name=u'vXmlSrvMainFrame', parent=prnt, pos=wx.Point(210, 126),
              size=wx.Size(518, 360),
              style=wx.MINIMIZE_BOX | wx.CLOSE_BOX | wx.SYSTEM_MENU | wx.CAPTION | wx.RESIZE_BORDER | wx.FRAME_TOOL_WINDOW,
              title=u'vServer')
        self.SetClientSize(wx.Size(510, 333))
        self.Bind(wx.EVT_CLOSE, self.OnVgfMainClose)
        self.Bind(wx.EVT_PAINT, self.OnVgfMainPaint)
        self.Bind(wx.EVT_SIZE, self.OnVgfMainSize)

        self.pnInfo = wx.Panel(id=wxID_VXMLSRVMAINFRAMEPNINFO, name=u'pnInfo',
              parent=self, pos=wx.Point(0, 0), size=wx.Size(510, 333),
              style=wx.TAB_TRAVERSAL)
        self.pnInfo.SetAutoLayout(True)

        self.cbRT = wx.BitmapButton(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VXMLSRVMAINFRAMECBRT, name=u'cbRT', parent=self.pnInfo,
              pos=wx.Point(486, 0), size=wx.Size(24, 24), style=wx.BU_AUTODRAW)
        self.cbRT.SetConstraints(LayoutAnchors(self.cbRT, False, True, True,
              False))
        self.cbRT.Bind(wx.EVT_BUTTON, self.OnCbRTButton,
              id=wxID_VXMLSRVMAINFRAMECBRT)

        self.cbRB = wx.BitmapButton(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VXMLSRVMAINFRAMECBRB, name=u'cbRB', parent=self.pnInfo,
              pos=wx.Point(486, 309), size=wx.Size(24, 24),
              style=wx.BU_AUTODRAW)
        self.cbRB.SetConstraints(LayoutAnchors(self.cbRB, False, False, True,
              True))
        self.cbRB.Bind(wx.EVT_BUTTON, self.OnCbRBButton,
              id=wxID_VXMLSRVMAINFRAMECBRB)

        self.cbLB = wx.BitmapButton(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VXMLSRVMAINFRAMECBLB, name=u'cbLB', parent=self.pnInfo,
              pos=wx.Point(0, 309), size=wx.Size(24, 24), style=wx.BU_AUTODRAW)
        self.cbLB.SetConstraints(LayoutAnchors(self.cbLB, True, False, False,
              True))
        self.cbLB.Bind(wx.EVT_BUTTON, self.OnCbLBButton,
              id=wxID_VXMLSRVMAINFRAMECBLB)

        self.cbLT = wx.BitmapButton(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VXMLSRVMAINFRAMECBLT, name=u'cbLT', parent=self.pnInfo,
              pos=wx.Point(0, 0), size=wx.Size(24, 24), style=wx.BU_AUTODRAW)
        self.cbLT.SetConstraints(LayoutAnchors(self.cbLT, True, True, False,
              False))
        self.cbLT.Bind(wx.EVT_BUTTON, self.OnLeftTop,
              id=wxID_VXMLSRVMAINFRAMECBLT)

        self.cbClose = wx.BitmapButton(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VXMLSRVMAINFRAMECBCLOSE, name=u'cbClose',
              parent=self.pnInfo, pos=wx.Point(454, 0), size=wx.Size(24, 24),
              style=wx.BU_AUTODRAW)
        self.cbClose.SetConstraints(LayoutAnchors(self.cbClose, False, True,
              True, False))
        self.cbClose.Bind(wx.EVT_BUTTON, self.OnCbCloseButton,
              id=wxID_VXMLSRVMAINFRAMECBCLOSE)

        self.cbHollow = wx.BitmapButton(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VXMLSRVMAINFRAMECBHOLLOW, name=u'cbHollow',
              parent=self.pnInfo, pos=wx.Point(422, 0), size=wx.Size(24, 24),
              style=wx.BU_AUTODRAW)
        self.cbHollow.SetConstraints(LayoutAnchors(self.cbHollow, False, True,
              True, False))
        self.cbHollow.Bind(wx.EVT_BUTTON, self.OnCbHollowButton,
              id=wxID_VXMLSRVMAINFRAMECBHOLLOW)

        self.lblTime = wx.StaticText(id=wxID_VXMLSRVMAINFRAMELBLTIME, label=u'',
              name=u'lblTime', parent=self.pnInfo, pos=wx.Point(330, 316),
              size=wx.Size(150, 13), style=0)
        self.lblTime.SetConstraints(LayoutAnchors(self.lblTime, False, False,
              True, True))

        self.tgbExpand = wx.lib.buttons.GenBitmapToggleButton(ID=wxID_VXMLSRVMAINFRAMETGBEXPAND,
              bitmap=wx.EmptyBitmap(16, 16), name=u'tgbExpand',
              parent=self.pnInfo, pos=wx.Point(30, 0), size=wx.Size(36, 36),
              style=0)
        self.tgbExpand.Bind(wx.EVT_BUTTON, self.OnTgbExpandButton,
              id=wxID_VXMLSRVMAINFRAMETGBEXPAND)

        self.chcAutoCollapse = wx.CheckBox(id=wxID_VXMLSRVMAINFRAMECHCAUTOCOLLAPSE,
              label=u'Auto', name=u'chcAutoCollapse', parent=self.pnInfo,
              pos=wx.Point(72, 8), size=wx.Size(48, 13), style=0)
        self.chcAutoCollapse.SetValue(False)
        self.chcAutoCollapse.Bind(wx.EVT_CHECKBOX,
              self.OnChcAutoCollapseCheckbox,
              id=wxID_VXMLSRVMAINFRAMECHCAUTOCOLLAPSE)

        self.cbLunch = wx.BitmapButton(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VXMLSRVMAINFRAMECBLUNCH, name=u'cbLunch',
              parent=self.pnInfo, pos=wx.Point(256, 0), size=wx.Size(30, 30),
              style=wx.BU_AUTODRAW)
        self.cbLunch.Bind(wx.EVT_BUTTON, self.OnCbLunchButton,
              id=wxID_VXMLSRVMAINFRAMECBLUNCH)

    def __init__(self, parent):
        self.verbose=VERBOSE
        self._init_ctrls(parent)
        
        self.sAclSecName=None
        self.dlgCfg=None
        self.dlgSecAcl=None
        self.srv=None
        self.dom=vtXmlDom(appl='vXmlSrc_cfg')
        self.lbkInfo=wx.Listbook(style=wx.TAB_TRAVERSAL, name='', parent=self.pnInfo, 
                id=wx.NewId(), 
                pos=wx.Point(4,42),size=wx.Size(494, 261))
        self.lbkInfo.SetConstraints(LayoutAnchors(self.lbkInfo, True, True, True,
              True))
        self.Bind(wx.EVT_LISTBOOK_PAGE_CHANGED, self.OnPageChanged)
        
        self.lunchers=[u'vPrjTimer',u'vPrjEng',u'vPrj',u'vPrjDoc',
                u'vPrjPlan',u'vBill',u'vPurchase',u'vSupplier',
                u'vRessource',u'vSoftware',
                u'vHum',u'vDoc',
                u'vCustomer',
                u'vLoc',u'vTask',u'vGlobals',u'vPlugIn',
                u'vSec',u'Net',u'Tools',u'vVSAS']
        il = wx.ImageList(32, 32)
        for e in self.lunchers:
            s='v%sPanel'%e
            f=getattr(images, 'get%sBitmap' % e)
            bmp = f()
            il.Add(bmp)
        il.Add(images.getSettingsBitmap())
        #self.lbkInfo.AssignImageList(il)
        il.Add(images.getToolsBitmap())
        self.lbkInfo.AssignImageList(il)
        
        self.lstPanel=[]
        i=0
        for e in self.lunchers:
            panel=self.__genPanel__(self.lbkInfo,e)
            self.lbkInfo.AddPage(panel,e,False,i)
            self.lstPanel.append(panel)
            i=i+1
            
        #add config
        self.iProtect=i
        self.pnCfg=vSettingsPanel(id=wx.NewId(), name=u'pnSetting',
              parent=self.lbkInfo, pos=wx.Point(0, 0), size=wx.Size(192, 253),
              style=wx.TAB_TRAVERSAL)
        EVT_INFO_CHANGED(self.pnCfg,self.OnSetupChanged)
        self.lbkInfo.AddPage(self.pnCfg,'settings',False,i)
        
        self.pnTools=vToolsPanel(id=wx.NewId(), name=u'pnTools',
              parent=self.lbkInfo, pos=wx.Point(0, 0), size=wx.Size(192, 253),
              style=wx.TAB_TRAVERSAL)
        EVT_TOOLS_APPL_ALIAS_SELECTED(self.pnTools,self.OnApplAliasSelected)
        EVT_TOOLS_APPL_ALIAS_ADDED(self.pnTools,self.OnApplAliasAdded)
        self.lbkInfo.AddPage(self.pnTools,'tools',False,i+1)
        
        img=images.getPosLTBitmap()
        self.cbLT.SetBitmapLabel(img)
        
        img=images.getPosRTBitmap()
        self.cbRT.SetBitmapLabel(img)
        
        img=images.getPosLBBitmap()
        self.cbLB.SetBitmapLabel(img)
        
        img=images.getPosRBBitmap()
        self.cbRB.SetBitmapLabel(img)
        
        img=images.getCloseBitmap()
        self.cbClose.SetBitmapLabel(img)
        
        img=images.getHollowBitmap()
        self.cbHollow.SetBitmapLabel(img)
        
        self.tgbExpand.SetBitmapLabel(images.getCollapseBitmap())
        self.tgbExpand.SetBitmapSelected(images.getExpandBitmap())
        self.tgbExpand.SetValue(True)
        self.bExpand=True
        
        self.bRunning=False
        self.cbLunch.SetBitmapLabel(images.getRunningBitmap())
        
        icon = getApplicationIcon()
        self.SetIcon(icon)
        if wx.Platform == '__WXMSW__':
            self.TBMENU_RESTORE=wx.NewId()
            self.TBMENU_CLOSE=wx.NewId()
            # setup a taskbar icon, and catch some events from it
            self.tbicon = wx.TaskBarIcon()
            self.tbicon.SetIcon(icon, "VIDARC XML Server")
            wx.EVT_TASKBAR_LEFT_DCLICK(self.tbicon, self.OnTaskBarActivate)
            wx.EVT_TASKBAR_RIGHT_UP(self.tbicon, self.OnTaskBarMenu)
            wx.EVT_MENU(self.tbicon, self.TBMENU_RESTORE, self.OnTaskBarActivate)
            wx.EVT_MENU(self.tbicon, self.TBMENU_CLOSE, self.OnTaskBarClose)

        # setup statusbar
        self.timer = wx.PyTimer(self.Notify)
        self.timer.Start(1000)
        self.Notify()
        self.iPos=0
        self.OpenFile(os.path.join(os.getcwdu(),u'server.xml'))
        self.__startSrv__()
        self.__start__()
    def __genPanel__(self,parent,name):
        panel = vInfoPanel(id=wx.NewId(), name=u'pn%s'%name,
              parent=parent, pos=wx.Point(0, 0), size=wx.Size(192, 253),
              style=wx.TAB_TRAVERSAL)
        EVT_INFO_CHANGED(panel,self.OnInfoChanged)
        EVT_INFO_ADDED(panel,self.OnInfoAdded)
        EVT_INFO_SERVE(panel,self.OnInfoServe)
        EVT_INFO_CONFIG(panel,self.OnInfoConfig)
        EVT_INFO_SEC_ACL(panel,self.OnInfoSecAcl)
        panel.SetAutoLayout(True)
        return panel
    def Notify(self):
        t = time.localtime(time.time())
        st = time.strftime("%d-%b-%Y   %H:%M:%S wk:%U", t)
        self.lblTime.SetLabel(st)
        pass
    def OnTaskBarActivate(self, evt):
        if self.IsIconized():
            self.Iconize(False)
        if not self.IsShown():
            self.Show(True)
        self.Raise()

    def OnTaskBarMenu(self, evt):
        menu = wx.Menu()
        #menu.Append(self.TBMENU_RESTORE, "Restore VIDARC Luncher")
        it=wx.MenuItem(menu,help='', id=self.TBMENU_RESTORE,
                kind=wx.ITEM_NORMAL, text=u"Restore VIDARC XML Server")
        it.SetBitmap(images.getOpenBitmap())
        menu.AppendItem(it)
        menu.AppendSeparator()
        #menu.Append(self.TBMENU_CLOSE,   "Close")
        it=wx.MenuItem(menu,help='', id=self.TBMENU_CLOSE,
                kind=wx.ITEM_NORMAL, text=u"Close")
        it.SetBitmap(images.getCancelBitmap())
        menu.AppendItem(it)
        self.tbicon.PopupMenu(menu)
        menu.Destroy()

    #---------------------------------------------
    def OnTaskBarClose(self, evt):
        self.Close()
        #self.Destroy()

    def OnVgfMainClose(self, event):
        srvXml=self.srv.GetDataInstance()
        dlg=vCloseDialog(self)
        dlg.SetLunchers(srvXml,zip(self.lunchers,self.lstPanel))
        dlg.Centre()
        dlg.ShowModal()
        self.__stop__()
        if hasattr(self, "tbicon"):
            #del self.tbicon
            self.tbicon.Destroy()
            #del self.tbicon
        self.Destroy()

    def OnLeftTop(self, event):
        self.iPos=0
        self.__update__()
        event.Skip()
    def __update__(self):
        pos=self.GetPosition()
        if self.bExpand:
            self.size=self.GetSize()
            self.lblTime.Show(True)
            self.cbHollow.Show(True)
            self.cbClose.Show(True)
            self.chcAutoCollapse.Show(True)
            self.cbLunch.Show(True)
            
        else:
            self.lblTime.Show(False)
            self.cbHollow.Show(False)
            self.cbClose.Show(False)
            self.chcAutoCollapse.Show(False)
            self.cbLunch.Show(False)
            wsize=self.GetSize()
            size=self.GetClientSize()
            dw=wsize.GetWidth()-size.GetWidth()
            dy=wsize.GetHeight()-size.GetHeight()
            
            self.SetSize(wx.Size(dw+24+48+24,dy+24+24))
        size=self.GetSize()
        
        #self.SetPosition(wx.Point(0,0))
        dispSize=wx.ClientDisplayRect()
        if self.iPos==0:
            self.SetPosition(wx.Point(dispSize[0],dispSize[1]))
        elif self.iPos==1:
            dispSize=wx.ClientDisplayRect()
            self.SetPosition(wx.Point(dispSize[0]+dispSize[2]-size[0],0))
        elif self.iPos==2:
            dispSize=wx.ClientDisplayRect()
            self.SetPosition(wx.Point(dispSize[0],
                        dispSize[1]+dispSize[3]-size[1]))
        elif self.iPos==3:
            dispSize=wx.ClientDisplayRect()
            self.SetPosition(wx.Point(dispSize[0]+dispSize[2]-size[0],
                    dispSize[1]+dispSize[3]-size[1]))
        pos=self.GetPosition()
        size=self.GetSize()
        self.dom.setNodeText(self.baseNode,'pos',str(self.iPos))
        self.dom.setNodeText(self.baseNode,'auto_collapse',
                str(int(self.chcAutoCollapse.GetValue())))
        self.dom.setNodeText(self.baseNode,'geometry',
                '%d,%d,%d,%d'%(pos.x,pos.y,size.GetWidth(),size.GetHeight()))
        self.SaveFile()
    def OnCbRTButton(self, event):
        self.iPos=1
        self.__update__()
        event.Skip()

    def OnCbLBButton(self, event):
        self.iPos=2
        self.__update__()
        event.Skip()

    def OnCbRBButton(self, event):
        self.iPos=3
        self.__update__()
        event.Skip()

    def OnVgfMainPaint(self, event):
        self.__update__()
        event.Skip()

    def OnVgfMainSize(self, event):
        size=event.GetSize()
        #size=event.GetClientSize()
        #dw=wsize.GetWidth()-size.GetWidth()
        #dy=wsize.GetWidth()-size.GetHeight()
        w=size.GetWidth()
        h=size[1]
        bMod=False
        if self.bExpand:
            if w<400:
                w=300
                bMod=True
            if h<200:
                h=200
                bMod=True
        dispSize=wx.ClientDisplayRect()
        if w>dispSize[2]:
            w=dispSize[2]
            bMod=True
        if h>dispSize[3]:
            h=dispSize[3]
            bMod=True
        if  bMod==True:
            self.SetSize(wx.Size(w,h))
            self.__update__()
        event.Skip()
    def CreateNew(self):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        self.dom.Close()
        self.dom.New(root="serverentries")
        for e in self.lunchers:
            n=self.dom.createSubNode(self.dom.getRoot(),'serve',False)
            self.dom.setNodeText(n,'name',e)
        self.SaveFile()
    def OpenFile(self,fn):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        global dictLunchApps
        self.dom.Close()
        self.fn=fn
        if self.dom.Open(fn)<0:
            self.CreateNew()
        self.baseNode=self.dom.getRoot()
        luncherNodes={}
        for c in self.dom.getChilds(self.baseNode,'serve'):
            sName=self.dom.getNodeText(c,'name')
            if len(sName)<=0:
                continue
            try:
                idx=self.lunchers.index(sName)
                self.lstPanel[idx].SetNode(c,self.dom)
            except:
                panel=self.__genPanel__(self.lbkInfo,sName)
                #self.lbkInfo.AddPage(panel,sName,False,-1)
                sImage=self.dom.getNodeText(c,'image')
                idxImg=-1
                try:
                    if len(sImage)>0:
                        bmp=wx.Image(sImage).ConvertToBitmap()
                        imgLst=self.lbkInfo.GetImageList()
                        idxImg=imgLst.Add(bmp)
                except:
                    pass
                #iPgCount=self.lbkInfo.GetPageCount()-1
                #self.lbkInfo.InsertPage(iPgCount,panel,sName,False,idxImg)
                #self.lstPanel.append(panel)
                iPgCount=self.lbkInfo.GetPageCount()-1
                #self.lbkInfo.RemovePage(iPgCount)
                #self.lbkInfo.AddPage(panel,sName,False,idxImg)
                #self.lbkInfo.AddPage(self.pnCfg,u'Settings',False,self.iProtect)
        
                #self.lstPanel.insert(iPgCount,panel)
                panel.SetNode(c,self.dom)
                #self.lunchers.append(sName)
                self.lunchers.insert(iPgCount,sName)
                
                dictLunchApps[sName]=[]
            luncherNodes[sName]=c
            pass
        for s in self.lunchers:
            if luncherNodes.has_key(s)==False:
                n=self.dom.createSubNode(self.baseNode,'serve',False)
                self.dom.setNodeText(n,'name',s)
                luncherNodes[s]=n
                idx=self.lunchers.index(s)
                self.lstPanel[idx].SetNode(n,self.dom)
        self.pnCfg.SetAppls(lstAppl)
        self.pnCfg.SetNode(self.baseNode,self.dom)
        try:
            self.iPos=int(self.dom.getNodeText(self.baseNode,'pos'))
        except:
            self.iPos=0
        try:
            iColl=int(self.dom.getNodeText(self.baseNode,'auto_collapse'))
        except:
            iColl=0
        self.chcAutoCollapse.SetValue(iColl)
        self.__update__()
    def SaveFile(self,fn=None):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        if fn is not None:
            self.fn=fn
        self.dom.AlignDoc()
        self.dom.Save(self.fn)
        
    def OnInfoAdded(self,evt):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        global dictLunchApps
        sName=evt.GetName()
        sImage=evt.GetImage()
        pass
    def OnInfoDeleted(self,evt):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        #if evt.GetObj()==self.pnCfg:
        #    return
        sName=evt.GetName()
        pass
    def OnInfoChanged(self,evt):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        evt.GetObj().GetNode()
        self.SaveFile()
    def OnSetupChanged(self,evt):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        evt.GetObj().GetNode()
        self.SaveFile()
    def OnPageChanged(self,evt):
        evt.Skip()
        iSel= evt.GetSelection()
        try:
            if iSel==self.iProtect+1:
                self.setToolsAlias()
        except:
            pass
    def setDataConfig(self):
        srvXml=self.srv.GetDataInstance()
        if srvXml is None:
            return
        try:
            s=self.dom.getNodeText(self.baseNode,'save_delay')
            iSaveMin=int(s)
        except:
            iSaveMin=5
        srvXml.SetSaveDelay(iSaveMin)
        
        try:
            s=self.dom.getNodeText(self.baseNode,'close_delay')
            iCloseMin=int(s)
        except:
            iCloseMin=600
        s=self.dom.getNodeText(self.baseNode,'close_enable')
        if s=='1':
            pass
        else:
            iCloseMin=-1
        srvXml.SetCloseDelay(iCloseMin)
        
    def OnInfoServe(self,evt):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        #try:
        pnInfo=evt.GetObj()
        idx=self.lstPanel.index(pnInfo)
        sName=self.lunchers[idx]
        self.__startStopSingleAppl__(sName,pnInfo,evt.GetName())
        #except:
        #    pass
    def OnInfoConfig(self,evt):
        if self.verbose>0:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
        pnInfo=evt.GetObj()
        idx=self.lstPanel.index(pnInfo)
        appl=self.lunchers[idx]
        alias=evt.GetName()
        try:
            appls=APPLS_MAP[appl]
        except:
            appls=[]
        d=pnInfo.GetLunchs()
        keys=d.keys()
        keys.sort()
        srvXml=self.srv.GetDataInstance()
        bFound=False
        doc=None
        for k in keys:
            if d[k]['alias']!=alias:
                continue
            sName=appl+':'+d[k]['alias']
            bRet=srvXml.IsServedXml(sName)
            if bRet:
                bFound=True
                doc=srvXml.GetServedXmlDom(sName)
                if self.verbose:
                    vtLog.vtLngCallStack(self,vtLog.DEBUG,'found:%s'%sName)
                break
            else:
                sMsg=u'Alias has to be started!'
                dlg=wx.MessageDialog(self,sMsg ,
                        u'vgaSrv',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg .Destroy()
                return
        if bFound==False:
            return
        if self.dlgCfg is None:
            self.dlgCfg=vConfigDialog(self)
        self.dlgCfg.Centre()
        node=None
        node=doc.getChildForced(doc.getRoot(),'config')
        self.dlgCfg.SetNode(doc,node,sName,appls)
        iRet=self.dlgCfg.ShowModal()
        if iRet>0:
            srvXml.ModifyServedXml(sName)
        self.SaveFile()
    def setApplAliasCfg(self,appl,alias):
        #vtLog.CallStack('')
        #print appl,alias
        try:
            appls=APPLS_MAP[appl]
        except:
            appls=[]
        #keys=d.keys()
        #keys.sort()
        srvXml=self.srv.GetDataInstance()
        bFound=False
        doc=None
        sName=appl+':'+alias
        bRet=srvXml.IsServedXml(sName)
        if bRet:
                bFound=True
                doc=srvXml.GetServedXmlDom(sName)
                if self.verbose:
                    vtLog.vtLngCallStack(self,vtLog.DEBUG,'found:%s'%sName)
        else:
                sMsg=u'Alias has to be started!'
                dlg=wx.MessageDialog(self,sMsg ,
                        u'vgaSrv',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg .Destroy()
                return
        if bFound==False:
            return
        if self.dlgCfg is None:
            self.dlgCfg=vConfigDialog(self)
        node=None
        node=doc.getChildForced(doc.getRoot(),'config')
        self.dlgCfg.SetNode(doc,node,sName,appls)
        return self.dlgCfg.GetAliasDict()
    def OnApplAliasSelected(self,evt):
        sAppl=evt.GetAppl()
        sAlias=evt.GetAlias()
        if sAppl is not None and sAlias is not None:
            d=self.setApplAliasCfg(sAppl,sAlias)
            self.pnTools.SetApplAliasCfgAliasDict(sAppl,sAlias,d)
        else:
            self.pnTools.SetApplAliasCfgAliasDict(sAppl,sAlias,None)
    def OnApplAliasAdded(self,evt):
        appl=evt.GetAppl()
        alias=evt.GetAlias()
        sApplRef=evt.GetApplRef()
        sHost=evt.GetHost()
        sPort=evt.GetPort()
        sAliasRef=evt.GetAliasRef()
        try:
            appls=APPLS_MAP[appl]
        except:
            appls=[]
        sName=appl+':'+alias
        srvXml=self.srv.GetDataInstance()
        if srvXml is None:
            return
        sName=appl+':'+alias
        bRet=srvXml.IsServedXml(sName)
        if bRet:
                bFound=True
                doc=srvXml.GetServedXmlDom(sName)
                if self.verbose:
                    vtLog.vtLngCallStack(self,vtLog.DEBUG,'found:%s'%sName)
        else:
                sMsg=u'Alias has to be started!'
                dlg=wx.MessageDialog(self,sMsg ,
                        u'vgaSrv',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg .Destroy()
                return
        if bFound==False:
            return
        if self.dlgCfg is None:
            self.dlgCfg=vConfigDialog(self)
        node=None
        node=doc.getChildForced(doc.getRoot(),'config')
        self.dlgCfg.SetNode(doc,node,sName,appls)
        iRet=self.dlgCfg.AddAlias(sApplRef,sHost,sPort,sAliasRef)
        if iRet>0:
            srvXml.ModifyServedXml(sName)
            self.SaveFile()
    def getPanel(self,appl):
        for pn in self.lstPanel:
            if pn.GetAppl()==appl:
                return pn
        return None
    def OnInfoSecAcl(self,evt):
        try:
            if self.verbose>0:
                vtLog.vtLngCallStack(self,vtLog.DEBUG,'')
            pnInfo=evt.GetObj()
            idx=self.lstPanel.index(pnInfo)
            appl=self.lunchers[idx]
            alias=evt.GetName()
            try:
                appls=APPLS_MAP[appl]
            except:
                appls=[]
            d=pnInfo.GetLunchs()
            keys=d.keys()
            keys.sort()
            self.sAclSecName=None
            srvXml=self.srv.GetDataInstance()
            bFound=False
            doc=None
            for k in keys:
                if d[k]['alias']!=alias:
                    continue
                sName=appl+':'+d[k]['alias']
                bRet=srvXml.IsServedXml(sName)
                if bRet:
                    bFound=True
                    doc=srvXml.GetServedXmlDom(sName)
                    if self.verbose:
                        vtLog.vtLngCallStack(self,vtLog.DEBUG,'found:%s'%sName)
                    break
                else:
                    sMsg=u'Alias has to be started!'
                    dlg=wx.MessageDialog(self,sMsg ,
                            u'vgaSrv',
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                    dlg.ShowModal()
                    dlg .Destroy()
                    return
            if bFound==False:
                return
            if self.dlgSecAcl is None:
                #self.dlgSecAcl=vSecAclDialog(self)
                self.dlgSecAcl=vtSecAclDomDialog(self)
                EVT_VTSEC_ACLDOM_PANEL_APPLIED(self.dlgSecAcl,self.OnSecAclApplied)
                EVT_VTSEC_ACLDOM_PANEL_CANCELED(self.dlgSecAcl,self.OnSecAclCanceled)
            self.sAclSecName=sName
            
            self.dlgSecAcl.Centre()
            node=None
            pnHum=self.getPanel('vHum')
            humDocs=[]
            if pnHum is not None:
                dHum=pnHum.GetLunchs()
                for hum in dHum.values():
                    sAlias=hum['alias']
                    sHumName='vHum'+':'+sAlias
                    bRet=srvXml.IsServedXml(sHumName)
                    if bRet:
                        docHum=srvXml.GetServedXmlDom(sHumName)
                        humDocs.append((sAlias,docHum,hum,False))
            def compFunc(a,b):
                return cmp(a[0],b[0])
            humDocs.sort(compFunc)
            
            #vtLog.CallStack('')
            #vtLog.pprint(humDocs)
            
            node=doc.getChildForced(doc.getRoot(),'security_acl')
            self.dlgSecAcl.SetDoc(doc)
            self.dlgSecAcl.SetDocHums(humDocs,False)
            self.dlgSecAcl.SetNode(node)
            
            #self.dlgSecAcl.SetNode(doc,node,sName,[appl]+appls,applDocs,humDocs)
            iRet=self.dlgSecAcl.Show(True)
            
            #iRet=self.dlgSecAcl.ShowModal()
            #if iRet>0:
            #    srvXml.ModifyServedXml(sName)
            #self.SaveFile()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSecAclApplied(self,event):
        event.Skip()
        if self.sAclSecName is not None:
            srvXml=self.srv.GetDataInstance()
            srvXml.ModifyServedXml(self.sAclSecName)
        self.sAclSecName=None
        
    def OnSecAclCanceled(self,event):
        event.Skip()
        self.sAclSecName=None
    def OnCbCloseButton(self, event):
        self.Show(False)
        event.Skip()

    def OnCbHollowButton(self, event):
        event.Skip()

    def OnTgbExpandButton(self, event):
        self.bExpand=self.tgbExpand.GetValue()
        if self.bExpand:
            self.SetSize(self.size)
        self.__update__()    
            
        event.Skip()

    def OnChcAutoCollapseCheckbox(self, event):
        event.Skip()

    def OnCbLunchButton(self, event):
        #idx=self.lbkInfo.GetSelection()
        #sFN=self.lstPanel[idx].GenLunch()
        if self.bRunning:
            self.__stop__()
        else:
            self.__start__()
        event.Skip()
    def __startSrv__(self):
        dataPort=self.pnCfg.GetDataPort()
        servicePort=self.pnCfg.GetServicePort()
        self.srv=vtNetSrv(vtSrvXmlSock,dataPort,vtNetXmlSock,
                        vtSrvSock,servicePort,vtServiceSock,2,1)
        self.srv.SetServicePasswd(self.pnCfg.GetServicePasswd())
        srvXml=self.srv.GetDataInstance()
        srvXml.SetSettings(self.dom)
        self.setDataConfig()
        self.srv.Serve()
        for i in range(len(self.lunchers)):
            self.lstPanel[i].SetSrvXml(srvXml,self.lunchers[i])
    def getAlias(self,appl):
        try:
            idx=self.lunchers.index(appl)
            panel=self.lstPanel[idx]
            k=self.lunchers[idx]
            d=panel.GetLunchs()
            dA={}
            for it in d.values():
                dA[it['alias']]=0
            
            keys=dA.keys()
            keys.sort()
            return keys
        except:
            return []
    def setToolsAlias(self):
        try:
            #vtLog.CallStack('')
            idx=0
            dAlias={}
            srvXml=self.srv.GetDataInstance()
            for panel in self.lstPanel:
                appl=self.lunchers[idx]
                d=panel.GetLunchs()
                #vtLog.pprint(d)
                dA={}
                for it in d.values():
                    bRet=srvXml.IsServedXml(appl+':'+it['alias'])
                    dA[it['alias']]={'rng':bRet}
                
                keys=dA.keys()
                keys.sort()
                #print keys
                #vtLog.pprint(dA)
                try:
                    appls=APPLS_MAP[appl]
                except:
                    appls=None
                dAlias[appl]={'appls':appls,'aliases':dA}
                idx+=1
            #vtLog.pprint(dAlias)
            self.pnTools.SetApplAliasDict(dAlias)
        except:
            pass
    def __start__(self):
        self.bRunning=True
        self.cbLunch.SetBitmapLabel(images.getStoppedBitmap())
        for idx in range(0,len(self.lunchers)):
            panel=self.lstPanel[idx]
            k=self.lunchers[idx]
            self.__startAppl__(k,panel)
    def __stop__(self):
        self.bRunning=False
        self.cbLunch.SetBitmapLabel(images.getRunningBitmap())
        for idx in range(0,len(self.lunchers)):
            panel=self.lstPanel[idx]
            k=self.lunchers[idx]
            self.__stopAppl__(k,panel)
    def __startAppl__(self,appl,panel):
        vtLog.vtLngCallStack(self,vtLog.DEBUG,appl)
        skip=[]
        d=panel.GetLunchs()
        keys=d.keys()
        keys.sort()
        srvXml=self.srv.GetDataInstance()
        for k in keys:
            if appl=='vPrjEng':
                #for e in vPrjEngElem:
                #    d=e[1]
                #    bSkip=False
                #    try:
                #        d['__properties'].index('isSkip')
                #        bSkip=True
                #        d['__properties'].index('isAddIDs')
                #        bSkip=False
                #    except:
                #        pass
                #    if bSkip:
                #        skip.append(e[0])
                doc=vXmlPrjEng(skip=skip,verbose=VERBOSE)
            elif appl=='vCustomer':
                doc=vXmlCustomer(skip=skip,verbose=VERBOSE)
            elif appl=='vHum':
                doc=vXmlHum(skip=skip,verbose=VERBOSE)
            elif appl=='vDoc':
                doc=vXmlDoc(skip=skip,verbose=VERBOSE)
            elif appl=='vGlobals':
                doc=vXmlGlobals(skip=skip,verbose=VERBOSE)
            elif appl=='vLoc':
                doc=vXmlLoc(skip=skip,verbose=VERBOSE)
            elif appl=='vRessource':
                doc=vXmlRessource(skip=skip,verbose=VERBOSE)
            elif appl=='vSoftware':
                doc=vXmlSoftware(skip=skip,verbose=VERBOSE)
            elif appl=='vTask':
                doc=vXmlTask(skip=skip,verbose=VERBOSE)
            elif appl=='vPrj':
                doc=vXmlPrj(skip=skip,verbose=VERBOSE)
            elif appl=='vPrjDoc':
                doc=vXmlPrjDoc(skip=skip,verbose=VERBOSE)
            elif appl=='vPrjPlan':
                doc=vXmlPrjPlan(skip=skip,verbose=VERBOSE)
            elif appl=='vPrjTimer':
                doc=vXmlPrjTimer(skip=skip,verbose=VERBOSE)
            elif appl=='vBill':
                doc=vXmlBill(skip=skip,verbose=VERBOSE)
            elif appl=='vPurchase':
                doc=vXmlPurchase(skip=skip,verbose=VERBOSE)
            elif appl=='vPlugIn':
                doc=vXmlPlugInSrv(skip=skip,verbose=VERBOSE)
            elif appl=='vSupplier':
                doc=vXmlSupplier(skip=skip,verbose=VERBOSE)
            else:
                doc=vtXmlDom(skip=skip,verbose=VERBOSE)
            srvXml.AddServedXml(appl+':'+d[k]['alias'],d[k]['name'],doc,skip)
            srvXml.SetServedHosts(appl+':'+d[k]['alias'],d[k]['hosts'])
            panel.Start(k,d[k]['alias'])
        #self.setToolsAlias()
    def __stopAppl__(self,appl,panel):
        vtLog.vtLngCallStack(self,vtLog.DEBUG,appl)
        d=panel.GetLunchs()
        keys=d.keys()
        keys.sort()
        srvXml=self.srv.GetDataInstance()
        for k in keys:
            bRet=srvXml.IsServedXml(appl+':'+d[k]['alias'])
            if bRet:
                srvXml.DelServedXml(appl+':'+d[k]['alias'])
                panel.Stop(k,d[k]['alias'])
        #self.setToolsAlias()
    def __startStopSingleAppl__(self,appl,panel,alias):
        vtLog.vtLngCallStack(self,vtLog.DEBUG,appl)
        d=panel.GetLunchs()
        keys=d.keys()
        keys.sort()
        srvXml=self.srv.GetDataInstance()
        for k in keys:
            if d[k]['alias']!=alias:
                continue
            bRet=srvXml.IsServedXml(appl+':'+d[k]['alias'])
            if bRet:
                srvXml.DelServedXml(appl+':'+d[k]['alias'])
                panel.Stop(k,d[k]['alias'])
                break
            else:
                skip=[]
                if appl=='vPrjEng':
                    #for e in vPrjEngElem:
                    #    d2=e[1]
                    #    bSkip=False
                    #    try:
                    #        d2['__properties'].index('isSkip')
                    #        bSkip=True
                    #        d2['__properties'].index('isAddIDs')
                    #        bSkip=False
                    #    except:
                    #        pass
                    #    if bSkip:
                    #        skip.append(e[0])
                    doc=vXmlPrjEng(skip=skip,verbose=VERBOSE)
                elif appl=='vHum':
                    doc=vXmlHum(skip=skip,verbose=VERBOSE)
                elif appl=='vCustomer':
                    doc=vXmlCustomer(skip=skip,verbose=VERBOSE)
                elif appl=='vDoc':
                    doc=vXmlDoc(skip=skip,verbose=VERBOSE)
                elif appl=='vGlobals':
                    doc=vXmlGlobals(skip=skip,verbose=VERBOSE)
                elif appl=='vLoc':
                    doc=vXmlLoc(skip=skip,verbose=VERBOSE)
                elif appl=='vRessource':
                    doc=vXmlRessource(skip=skip,verbose=VERBOSE)
                elif appl=='vSoftware':
                    doc=vXmlSoftware(skip=skip,verbose=VERBOSE)
                elif appl=='vTask':
                    doc=vXmlTask(skip=skip,verbose=VERBOSE)
                elif appl=='vPrj':
                    doc=vXmlPrj(skip=skip,verbose=VERBOSE)
                elif appl=='vPrjDoc':
                    doc=vXmlPrjDoc(skip=skip,verbose=VERBOSE)
                elif appl=='vPrjPlan':
                    doc=vXmlPrjPlan(skip=skip,verbose=VERBOSE)
                elif appl=='vPrjTimer':
                    doc=vXmlPrjTimer(skip=skip,verbose=VERBOSE)
                elif appl=='vBill':
                    doc=vXmlBill(skip=skip,verbose=VERBOSE)
                elif appl=='vPurchase':
                    doc=vXmlPurchase(skip=skip,verbose=VERBOSE)
                elif appl=='vPlugIn':
                    doc=vXmlPlugInSrv(skip=skip,verbose=VERBOSE)
                elif appl=='vSupplier':
                    doc=vXmlSupplier(skip=skip,verbose=VERBOSE)
                else:
                    doc=vtXml(skip=skip,verbose=VERBOSE)
                srvXml.AddServedXml(appl+':'+d[k]['alias'],d[k]['name'],doc,skip)
                srvXml.SetServedHosts(appl+':'+d[k]['alias'],d[k]['hosts'])
                panel.Start(k,d[k]['alias'])
                break
