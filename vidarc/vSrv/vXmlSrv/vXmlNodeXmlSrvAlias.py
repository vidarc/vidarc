#----------------------------------------------------------------------------
# Name:         vXmlNodeXmlSrvAlias.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060708
# CVS-ID:       $Id: vXmlNodeXmlSrvAlias.py,v 1.2 2006/08/29 10:06:31 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vSrv.net.vsNetXmlNodeSrvAlias import vsNetXmlNodeSrvAlias
try:
    if vcCust.is2Import(__name__):
        from vidarc.vSrv.net.vsNetXmlNodeSrvAliasPanel import vsNetXmlNodeSrvAliasPanel
        from vidarc.vSrv.net.vsNetXmlNodeSrvAliasEditDialog import vsNetXmlNodeSrvAliasEditDialog
        from vidarc.vSrv.net.vsNetXmlNodeSrvAliasAddDialog import vsNetXmlNodeSrvAliasAddDialog
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeXmlSrvAlias(vsNetXmlNodeSrvAlias):
    def __init__(self,tagName='vXmlSrvAlias'):
        global _
        _=vtLgBase.assignPluginLang('vXmlSrv')
        vsNetXmlNodeSrvAlias.__init__(self,tagName)
    def GetDescription(self):
        return _(u'XML server alias')
    # ---------------------------------------------------------
    # specific
    def GetDoc2Serve(self,node,appl=None):
        if node is None:
            return None
        nodePar=self.doc.getParent(node)
        if nodePar is None:
            return None
        oReg=self.doc.GetRegByNode(nodePar)
        return oReg.GetDoc2Serve(nodePar,appl=appl)
    # ---------------------------------------------------------
    # inheritance
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00pIDAT8\x8dcddbf@\x07\xba\x1d!\xff1\x04\x19\x18\x18.W\xacaD\x17c"V!61\
\x06\x06\x06\x06F\x98\x0bp\xd9\x8a\x0b\xc0\x0c\xc4\xea\x02R\x00\xe5\x06\x90\
\xeatd\xa0\xdb\x11\xf2\x9fQ\xaf+\x9cl\x03\x18\x18\x06E\x18P\xc5\x00\\\x89\
\x04\x1f\x80\xa7\x03r4#\x1b2\xf0a\xc0\x88-720`\xe6\r\\^\xc5\xea\x02l\xa9\x13\
W\x8a\x05\x00b\xb7"-7\xce\xbc\x10\x00\x00\x00\x00IEND\xaeB`\x82' 
    def GetEditDialogClass(self):
        if GUI:
            return vsNetXmlNodeSrvAliasEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return vsNetXmlNodeSrvAliasAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vsNetXmlNodeSrvAliasPanel
        else:
            return None
