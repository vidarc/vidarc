#Boa:FramePanel:vToolsPanel
#----------------------------------------------------------------------------
# Name:         vToolsPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060530
# CVS-ID:       $Id: vToolsPanel.py,v 1.1 2006/06/07 10:35:09 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import sys

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt

wxEVT_TOOLS_APPL_ALIAS_SELECTED=wx.NewEventType()
def EVT_TOOLS_APPL_ALIAS_SELECTED(win,func):
    win.Connect(-1,-1,wxEVT_TOOLS_APPL_ALIAS_SELECTED,func)
class vToolsApplAliasSelected(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_TOOLS_APPL_ALIAS_SELECTED(<widget_name>, self.OnSelected)
    """

    def __init__(self,obj,appl,alias):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_TOOLS_APPL_ALIAS_SELECTED)
        self.obj=obj
        self.appl=appl
        self.alias=alias
    def GetObj(self):
        return self.obj
    def GetAppl(self):
        return self.appl
    def GetAlias(self):
        return self.alias

wxEVT_TOOLS_APPL_ALIAS_ADDED=wx.NewEventType()
def EVT_TOOLS_APPL_ALIAS_ADDED(win,func):
    win.Connect(-1,-1,wxEVT_TOOLS_APPL_ALIAS_ADDED,func)
class vToolsApplAliasAdded(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_TOOLS_APPL_ALIAS_ADDED(<widget_name>, self.OnSelected)
    """

    def __init__(self,obj,appl,alias,applRef,host,port,aliasRef):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_TOOLS_APPL_ALIAS_ADDED)
        self.obj=obj
        self.appl=appl
        self.alias=alias
        self.applRef=applRef
        self.host=host
        self.port=port
        self.aliasRef=aliasRef
    def GetObj(self):
        return self.obj
    def GetAppl(self):
        return self.appl
    def GetAlias(self):
        return self.alias
    def GetApplRef(self):
        return self.applRef
    def GetHost(self):
        return self.host
    def GetPort(self):
        return self.port
    def GetAliasRef(self):
        return self.aliasRef

[wxID_VTOOLSPANEL, wxID_VTOOLSPANELCBADDACCESS, wxID_VTOOLSPANELCBADDCFG, 
 wxID_VTOOLSPANELCBDELCFG, wxID_VTOOLSPANELCBSELALLALIASES, 
 wxID_VTOOLSPANELCBSETACCESS, wxID_VTOOLSPANELCBSETCFG, 
 wxID_VTOOLSPANELCBUNSELALLALIASES, wxID_VTOOLSPANELCBUPDATEALIASES, 
 wxID_VTOOLSPANELCHCAPPLCFG, wxID_VTOOLSPANELCHCAPPLREFCFG, 
 wxID_VTOOLSPANELCLSTALIASES, wxID_VTOOLSPANELLBLALIASCFG, 
 wxID_VTOOLSPANELLBLALIASES, wxID_VTOOLSPANELLBLAPPLCFG, 
 wxID_VTOOLSPANELLBLHOST, wxID_VTOOLSPANELLBLHOSTCFG, wxID_VTOOLSPANELLBLPASS, 
 wxID_VTOOLSPANELLBLPORTCFG, wxID_VTOOLSPANELLBLUSER, wxID_VTOOLSPANELLSTCFG, 
 wxID_VTOOLSPANELNBTOOLS, wxID_VTOOLSPANELPNACCESS, wxID_VTOOLSPANELPNALIASES, 
 wxID_VTOOLSPANELPNCFG, wxID_VTOOLSPANELTXTALIASCFG, wxID_VTOOLSPANELTXTHOST, 
 wxID_VTOOLSPANELTXTHOSTCFG, wxID_VTOOLSPANELTXTPASSWD, 
 wxID_VTOOLSPANELTXTPORTCFG, wxID_VTOOLSPANELTXTUSER, 
] = [wx.NewId() for _init_ctrls in range(31)]

class vToolsPanel(wx.Panel):
    def _init_coll_bxsBtCfg_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbAddCfg, 0, border=5, flag=wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.cbSetCfg, 0, border=4, flag=wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.cbDelCfg, 0, border=4, flag=wx.RIGHT | wx.LEFT)

    def _init_coll_gbsCfg_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblHostCfg, (0, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.txtHostCfg, (0, 1), border=0, flag=wx.EXPAND,
              span=(1, 2))
        parent.AddWindow(self.lblPortCfg, (1, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.txtPortCfg, (1, 1), border=0, flag=wx.EXPAND,
              span=(1, 2))
        parent.AddWindow(self.lblAliasCfg, (2, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.txtAliasCfg, (2, 1), border=0, flag=wx.EXPAND,
              span=(1, 2))
        parent.AddSizer(self.bxsBtCfg, (4, 0), border=0, flag=wx.ALIGN_CENTER,
              span=(1, 3))
        parent.AddWindow(self.lstCfg, (5, 0), border=0, flag=wx.EXPAND, span=(1,
              3))
        parent.AddWindow(self.lblApplCfg, (3, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.chcApplCfg, (3, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.chcApplRefCfg, (3, 2), border=0, flag=wx.EXPAND,
              span=(1, 1))

    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.nbTools, 1, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)

    def _init_coll_gbsAccess_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblHost, (0, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.txtHost, (0, 1), border=0, flag=wx.EXPAND,
              span=(1, 2))
        parent.AddWindow(self.lblUser, (1, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.txtUser, (1, 1), border=0, flag=wx.EXPAND,
              span=(1, 2))
        parent.AddWindow(self.lblPass, (2, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.txtPasswd, (2, 1), border=0, flag=wx.EXPAND,
              span=(1, 2))
        parent.AddSizer(self.bxsBtAccess, (3, 0), border=0,
              flag=wx.ALIGN_CENTER, span=(1, 3))

    def _init_coll_bxsBtAccess_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbAddAccess, 0, border=4, flag=wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.cbSetAccess, 0, border=4, flag=wx.RIGHT | wx.LEFT)

    def _init_coll_fgsAliases_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)
        parent.AddGrowableCol(0)

    def _init_coll_bxsBtAliases_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbSelAllAliases, 0, border=0, flag=0)
        parent.AddWindow(self.cbUnselAllAliases, 0, border=4, flag=wx.TOP)
        parent.AddWindow(self.cbUpdateAliases, 0, border=4, flag=wx.TOP)

    def _init_coll_fgsAliases_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblAliases, 0, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.clstAliases, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsBtAliases, 0, border=0, flag=0)

    def _init_coll_lstCfg_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT, heading=u'appl',
              width=50)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT, heading=u'alias',
              width=60)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT, heading=u'appl ref',
              width=50)
        parent.InsertColumn(col=3, format=wx.LIST_FORMAT_LEFT, heading=u'host',
              width=50)
        parent.InsertColumn(col=4, format=wx.LIST_FORMAT_LEFT, heading=u'port',
              width=50)
        parent.InsertColumn(col=5, format=wx.LIST_FORMAT_LEFT, heading=u'alias',
              width=60)

    def _init_coll_nbTools_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.pnAliases, select=True,
              text=u'Aliases')
        parent.AddPage(imageId=-1, page=self.pnCfg, select=False,
              text=u'Config')
        parent.AddPage(imageId=-1, page=self.pnAccess, select=False,
              text=u'Access')

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=1, vgap=0)

        self.bxsBtAccess = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.gbsAccess = wx.GridBagSizer(hgap=4, vgap=4)

        self.gbsCfg = wx.GridBagSizer(hgap=4, vgap=4)

        self.bxsBtCfg = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.fgsAliases = wx.FlexGridSizer(cols=2, hgap=0, rows=2, vgap=0)

        self.bxsBtAliases = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsBtAccess_Items(self.bxsBtAccess)
        self._init_coll_gbsAccess_Items(self.gbsAccess)
        self._init_coll_gbsCfg_Items(self.gbsCfg)
        self._init_coll_bxsBtCfg_Items(self.bxsBtCfg)
        self._init_coll_fgsAliases_Items(self.fgsAliases)
        self._init_coll_fgsAliases_Growables(self.fgsAliases)
        self._init_coll_bxsBtAliases_Items(self.bxsBtAliases)

        self.SetSizer(self.fgsData)
        self.pnAliases.SetSizer(self.fgsAliases)
        self.pnAccess.SetSizer(self.gbsAccess)
        self.pnCfg.SetSizer(self.gbsCfg)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTOOLSPANEL, name=u'pnCfg', parent=prnt,
              pos=wx.Point(247, 173), size=wx.Size(345, 272),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(337, 245))

        self.nbTools = wx.Notebook(id=wxID_VTOOLSPANELNBTOOLS, name=u'nbTools',
              parent=self, pos=wx.Point(4, 4), size=wx.Size(329, 237), style=0)

        self.pnCfg = wx.Panel(id=wxID_VTOOLSPANELPNCFG, name=u'pnTools',
              parent=self.nbTools, pos=wx.Point(0, 0), size=wx.Size(321, 211),
              style=wx.TAB_TRAVERSAL)

        self.pnAccess = wx.Panel(id=wxID_VTOOLSPANELPNACCESS, name=u'pnAccess',
              parent=self.nbTools, pos=wx.Point(0, 0), size=wx.Size(321, 211),
              style=wx.TAB_TRAVERSAL)

        self.lblHost = wx.StaticText(id=wxID_VTOOLSPANELLBLHOST, label=u'Host',
              name=u'lblHost', parent=self.pnAccess, pos=wx.Point(0, 0),
              size=wx.Size(56, 21), style=wx.ALIGN_RIGHT)
        self.lblHost.SetMinSize(wx.Size(-1, -1))

        self.txtHost = wx.TextCtrl(id=wxID_VTOOLSPANELTXTHOST, name=u'txtHost',
              parent=self.pnAccess, pos=wx.Point(60, 0), size=wx.Size(116, 21),
              style=0, value=u'')
        self.txtHost.SetMinSize(wx.Size(-1, -1))

        self.lblUser = wx.StaticText(id=wxID_VTOOLSPANELLBLUSER, label=u'User',
              name=u'lblUser', parent=self.pnAccess, pos=wx.Point(0, 25),
              size=wx.Size(56, 21), style=wx.ALIGN_RIGHT)
        self.lblUser.SetMinSize(wx.Size(-1, -1))

        self.txtUser = wx.TextCtrl(id=wxID_VTOOLSPANELTXTUSER, name=u'txtUser',
              parent=self.pnAccess, pos=wx.Point(60, 25), size=wx.Size(116, 21),
              style=0, value=u'')
        self.txtUser.SetMinSize(wx.Size(-1, -1))

        self.lblPass = wx.StaticText(id=wxID_VTOOLSPANELLBLPASS,
              label=u'Password', name=u'lblPass', parent=self.pnAccess,
              pos=wx.Point(0, 50), size=wx.Size(56, 21), style=wx.ALIGN_RIGHT)
        self.lblPass.SetMinSize(wx.Size(-1, -1))

        self.txtPasswd = wx.TextCtrl(id=wxID_VTOOLSPANELTXTPASSWD,
              name=u'txtPasswd', parent=self.pnAccess, pos=wx.Point(60, 50),
              size=wx.Size(116, 21), style=wx.TE_PASSWORD, value=u'')
        self.txtPasswd.SetMinSize(wx.Size(-1, -1))

        self.cbSetAccess = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTOOLSPANELCBSETACCESS,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=u'Set',
              name=u'cbSetAccess', parent=self.pnAccess, pos=wx.Point(92, 75),
              size=wx.Size(76, 30), style=0)
        self.cbSetAccess.SetMinSize(wx.Size(-1, -1))
        self.cbSetAccess.Bind(wx.EVT_BUTTON, self.OnCbSetAccessButton,
              id=wxID_VTOOLSPANELCBSETACCESS)

        self.cbAddAccess = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTOOLSPANELCBADDACCESS,
              bitmap=vtArt.getBitmap(vtArt.Add), label=u'Access',
              name=u'cbAddAccess', parent=self.pnAccess, pos=wx.Point(8, 75),
              size=wx.Size(76, 30), style=0)
        self.cbAddAccess.SetMinSize(wx.Size(-1, -1))
        self.cbAddAccess.Bind(wx.EVT_BUTTON, self.OnCbAddAccessButton,
              id=wxID_VTOOLSPANELCBADDACCESS)

        self.lblHostCfg = wx.StaticText(id=wxID_VTOOLSPANELLBLHOSTCFG,
              label=u'Host', name=u'lblHostCfg', parent=self.pnCfg,
              pos=wx.Point(0, 0), size=wx.Size(84, 21), style=wx.ALIGN_RIGHT)
        self.lblHostCfg.SetMinSize(wx.Size(-1, -1))

        self.txtHostCfg = wx.TextCtrl(id=wxID_VTOOLSPANELTXTHOSTCFG,
              name=u'txtHostCfg', parent=self.pnCfg, pos=wx.Point(88, 0),
              size=wx.Size(264, 21), style=0, value=u'')
        self.txtHostCfg.SetMinSize(wx.Size(-1, -1))

        self.lblPortCfg = wx.StaticText(id=wxID_VTOOLSPANELLBLPORTCFG,
              label=u'Port', name=u'lblPortCfg', parent=self.pnCfg,
              pos=wx.Point(0, 25), size=wx.Size(84, 21), style=wx.ALIGN_RIGHT)
        self.lblPortCfg.SetMinSize(wx.Size(-1, -1))

        self.txtPortCfg = wx.TextCtrl(id=wxID_VTOOLSPANELTXTPORTCFG,
              name=u'txtPortCfg', parent=self.pnCfg, pos=wx.Point(88, 25),
              size=wx.Size(264, 21), style=0, value=u'')
        self.txtPortCfg.SetMinSize(wx.Size(-1, -1))

        self.lblAliasCfg = wx.StaticText(id=wxID_VTOOLSPANELLBLALIASCFG,
              label=u'Alias', name=u'lblAliasCfg', parent=self.pnCfg,
              pos=wx.Point(0, 50), size=wx.Size(84, 21), style=wx.ALIGN_RIGHT)
        self.lblAliasCfg.SetMinSize(wx.Size(-1, -1))

        self.txtAliasCfg = wx.TextCtrl(id=wxID_VTOOLSPANELTXTALIASCFG,
              name=u'txtAliasCfg', parent=self.pnCfg, pos=wx.Point(88, 50),
              size=wx.Size(264, 21), style=0, value=u'')
        self.txtAliasCfg.SetMinSize((-1,-1))

        self.cbAddCfg = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTOOLSPANELCBADDCFG,
              bitmap=vtArt.getBitmap(vtArt.Add), label=u'Add', name=u'cbAddCfg',
              parent=self.pnCfg, pos=wx.Point(54, 100), size=wx.Size(76, 30),
              style=0)
        self.cbAddCfg.SetMinSize(wx.Size(-1, -1))
        self.cbAddCfg.Bind(wx.EVT_BUTTON, self.OnCbAddCfgButton,
              id=wxID_VTOOLSPANELCBADDCFG)

        self.cbSetCfg = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTOOLSPANELCBSETCFG,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=u'Set',
              name=u'cbSetCfg', parent=self.pnCfg, pos=wx.Point(139, 100),
              size=wx.Size(76, 30), style=0)
        self.cbSetCfg.SetMinSize(wx.Size(-1, -1))
        self.cbSetCfg.Bind(wx.EVT_BUTTON, self.OnCbSetCfgButton,
              id=wxID_VTOOLSPANELCBSETCFG)

        self.cbDelCfg = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTOOLSPANELCBDELCFG,
              bitmap=vtArt.getBitmap(vtArt.Del), label=u'Del', name=u'cbDelCfg',
              parent=self.pnCfg, pos=wx.Point(223, 100), size=wx.Size(76, 30),
              style=0)
        self.cbDelCfg.SetMinSize(wx.Size(-1, -1))
        self.cbDelCfg.Bind(wx.EVT_BUTTON, self.OnCbDelCfgButton,
              id=wxID_VTOOLSPANELCBDELCFG)

        self.lstCfg = wx.ListCtrl(id=wxID_VTOOLSPANELLSTCFG, name=u'lstCfg',
              parent=self.pnCfg, pos=wx.Point(0, 134), size=wx.Size(352, 80),
              style=wx.LC_REPORT)
        self.lstCfg.SetMinSize(wx.Size(-1, -1))
        self._init_coll_lstCfg_Columns(self.lstCfg)
        self.lstCfg.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstCfgListItemDeselected, id=wxID_VTOOLSPANELLSTCFG)
        self.lstCfg.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstCfgListItemSelected, id=wxID_VTOOLSPANELLSTCFG)

        self.lblApplCfg = wx.StaticText(id=wxID_VTOOLSPANELLBLAPPLCFG,
              label=u'appl', name=u'lblApplCfg', parent=self.pnCfg,
              pos=wx.Point(0, 75), size=wx.Size(84, 21), style=wx.ALIGN_RIGHT)
        self.lblApplCfg.SetMinSize(wx.Size(-1, -1))

        self.chcApplCfg = wx.Choice(choices=[], id=wxID_VTOOLSPANELCHCAPPLCFG,
              name=u'chcApplCfg', parent=self.pnCfg, pos=wx.Point(88, 75),
              size=wx.Size(130, 21), style=0)
        self.chcApplCfg.SetMinSize(wx.Size(-1, -1))
        self.chcApplCfg.Bind(wx.EVT_CHOICE, self.OnChcApplCfgChoice,
              id=wxID_VTOOLSPANELCHCAPPLCFG)

        self.chcApplRefCfg = wx.Choice(choices=[],
              id=wxID_VTOOLSPANELCHCAPPLREFCFG, name=u'chcApplRefCfg',
              parent=self.pnCfg, pos=wx.Point(222, 75), size=wx.Size(130, 21),
              style=0)
        self.chcApplRefCfg.SetMinSize(wx.Size(-1, -1))
        self.chcApplRefCfg.Bind(wx.EVT_CHOICE, self.OnChcApplRefCfgChoice,
              id=wxID_VTOOLSPANELCHCAPPLREFCFG)

        self.pnAliases = wx.Panel(id=wxID_VTOOLSPANELPNALIASES,
              name=u'pnAliases', parent=self.nbTools, pos=wx.Point(0, 0),
              size=wx.Size(321, 211), style=wx.TAB_TRAVERSAL)

        self.lblAliases = wx.StaticText(id=wxID_VTOOLSPANELLBLALIASES,
              label=u'aliases', name=u'lblAliases', parent=self.pnAliases,
              pos=wx.Point(0, 0), size=wx.Size(245, 13), style=0)

        self.clstAliases = wx.CheckListBox(choices=[],
              id=wxID_VTOOLSPANELCLSTALIASES, name=u'clstAliases',
              parent=self.pnAliases, pos=wx.Point(0, 13), size=wx.Size(245,
              198), style=0)

        self.cbSelAllAliases = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTOOLSPANELCBSELALLALIASES,
              bitmap=vtArt.getBitmap(vtArt.AttrSel), label=u'Sel All',
              name=u'cbSelAllAliases', parent=self.pnAliases, pos=wx.Point(245,
              13), size=wx.Size(76, 30), style=0)
        self.cbSelAllAliases.SetMinSize(wx.Size(-1, -1))
        self.cbSelAllAliases.Bind(wx.EVT_BUTTON, self.OnCbSelAllAliasesButton,
              id=wxID_VTOOLSPANELCBSELALLALIASES)

        self.cbUnselAllAliases = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTOOLSPANELCBUNSELALLALIASES,
              bitmap=vtArt.getBitmap(vtArt.AttrUnSel), label=u'UnSel All',
              name=u'cbUnselAllAliases', parent=self.pnAliases,
              pos=wx.Point(245, 47), size=wx.Size(76, 30), style=0)
        self.cbUnselAllAliases.SetMinSize(wx.Size(-1, -1))
        self.cbUnselAllAliases.Bind(wx.EVT_BUTTON,
              self.OnCbUnselAllAliasesButton,
              id=wxID_VTOOLSPANELCBUNSELALLALIASES)

        self.cbUpdateAliases = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTOOLSPANELCBUPDATEALIASES,
              bitmap=vtArt.getBitmap(vtArt.Synch), label=u'Update', name=u'cbUpdateAliases',
              parent=self.pnAliases, pos=wx.Point(245, 81), size=wx.Size(76,
              30), style=0)
        self.cbUpdateAliases.SetMinSize(wx.Size(-1, -1))
        self.cbUpdateAliases.Bind(wx.EVT_BUTTON, self.OnCbUpdateAliasesButton,
              id=wxID_VTOOLSPANELCBUPDATEALIASES)

        self._init_coll_nbTools_Pages(self.nbTools)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        self.gbsAccess.AddGrowableCol(0)
        self.gbsAccess.AddGrowableCol(2)
        self.gbsCfg.AddGrowableCol(0)
        self.gbsCfg.AddGrowableCol(2)
        self.gbsCfg.AddGrowableRow(5)
        self.dApplAliasCfg={}
        self.dApplAlias={}
    def __showAppl__(self):
        self.chcApplCfg.Clear()
        keys=self.dApplAlias.keys()
        keys.sort()
        self.chcApplCfg.Append('---')
        for k in keys:
            try:
                if self.dApplAlias[k]['appls'] is None:
                    continue
            except:
                continue
            self.chcApplCfg.Append(k)
        self.chcApplCfg.SetSelection(0)
        self.__showApplAlias__()
        #self.__showApplAliasLst__()
    def __showApplAlias__(self):
        sKey=self.chcApplCfg.GetStringSelection()
        self.chcApplRefCfg.Clear()
        self.chcApplRefCfg.Append('---')
        if sKey=='---':
            keys=self.dApplAlias.keys()
            keys.sort()
            for k in keys:
                self.chcApplRefCfg.Append(k)
        try:
            keys=self.dApplAlias[sKey]['appls']
            keys.sort()
            for k in keys:
                self.chcApplRefCfg.Append(k)
        except:
            pass
        self.chcApplRefCfg.SetSelection(0)
        self.__showApplAliasLst__()
    def __showApplAliasLst__(self):
        sApplCmp=self.chcApplCfg.GetStringSelection()
        sApplRef=self.chcApplRefCfg.GetStringSelection()
        self.lstCfg.DeleteAllItems()
        keys=self.dApplAliasCfg.keys()
        keys.sort()
        for k in keys:
            try:
                sAppl,sAlias=k.split(':')
                if sApplCmp!='---':
                    if sApplCmp!=sAppl:
                        continue
                d=self.dApplAliasCfg[k][2]
                kkeys=d.keys()
                kkeys.sort()
                for kk in kkeys:
                    l=d[kk]
                    for tup in l:
                        sHost=tup[1]
                        sPort=tup[2]
                        sAliasCfg=tup[3]
                        idx=self.lstCfg.InsertStringItem(sys.maxint,sAppl)
                        self.lstCfg.SetStringItem(idx,1,sAlias)
                        self.lstCfg.SetStringItem(idx,2,kk)
                        self.lstCfg.SetStringItem(idx,3,sHost)
                        self.lstCfg.SetStringItem(idx,4,sPort)
                        self.lstCfg.SetStringItem(idx,5,sAliasCfg)
                    
            except:
                import traceback
                traceback.print_exc()
                pass
        
    def __showApplAliasLstOld__(self):
        self.lstCfg.DeleteAllItems()
        sAppl=self.chcApplCfg.GetStringSelection()
        sAlias=self.chcApplRefCfg.GetStringSelection()
        def addAlias2Lst(kAppl):
            try:
                d=self.dApplAlias[kAppl]['aliases']
                keys=d.keys()
                keys.sort()
                for k in keys:
                    idx=self.lstCfg.InsertStringItem(sys.maxint,kAppl)
                    self.lstCfg.SetStringItem(idx,1,k)
            except:
                pass
        if sAppl=='---':
            keys=self.dApplAlias.keys()
            keys.sort()
            for k in keys:
                addAlias2Lst(k)
        else:
            addAlias2Lst(sAppl)
    def __showApplAliasChkLst__(self):
        self.dApplAliasCfg={}
        self.clstAliases.Clear()
        def addAlias2Lst(kAppl):
            try:
                d=self.dApplAlias[kAppl]['aliases']
                keys=d.keys()
                keys.sort()
                for k in keys:
                    self.clstAliases.Append(kAppl+':'+k)
            except:
                pass
        keys=self.dApplAlias.keys()
        keys.sort()
        for k in keys:
            addAlias2Lst(k)
        
    def SetApplAliasDict(self,dApplAlias):
        self.dApplAlias=dApplAlias
        self.__showAppl__()
        self.__showApplAliasChkLst__()
    def OnCbSetAccessButton(self, event):
        event.Skip()

    def OnCbAddAccessButton(self, event):
        event.Skip()

    def OnCbAddCfgButton(self, event):
        event.Skip()
        sApplCmp=self.chcApplCfg.GetStringSelection()
        sApplRef=self.chcApplRefCfg.GetStringSelection()
        sHost=self.txtHostCfg.GetValue()
        sPort=self.txtPortCfg.GetValue()
        sAliasRef=self.txtAliasCfg.GetValue()
        #vtLog.CallStack('')
        #print sApplCmp,sApplRef
        #print sHost,sPort,sAliasRef
        #if sApplRef=='---':
        #    return
        sName=''
        for idx in xrange(self.lstCfg.GetItemCount()):
            try:
                if self.lstCfg.GetItemState(idx,wx.LIST_STATE_SELECTED)==0:
                    continue
                sAppl=self.lstCfg.GetItem(idx,0).m_text
                if sApplCmp!='---':
                    if sAppl!=sApplCmp:
                        continue
                else:
                    sApplCmp=''
                    if sApplRef!='---':
                        try:
                            l=self.dApplAlias[sAppl]['appls']
                            l.index(sApplRef)
                        except:
                            continue
                    else:
                        sApplRef=''
                sAlias=self.lstCfg.GetItem(idx,1).m_text
                sNameNew=sAppl+':'+sAlias
                if sName==sNameNew:
                    continue
                sName=sNameNew
                wx.PostEvent(self,vToolsApplAliasAdded(self,sAppl,sAlias,
                    sApplRef,sHost,sPort,sAliasRef))
            except:
                import traceback
                traceback.print_exc()
    def OnCbSetCfgButton(self, event):
        event.Skip()

    def OnCbDelCfgButton(self, event):
        event.Skip()

    def OnChcApplCfgChoice(self, event):
        event.Skip()
        self.__showApplAlias__()
        #self.__showApplAliasLst__()

    def OnChcApplRefCfgChoice(self, event):
        event.Skip()

    def OnLstCfgListItemDeselected(self, event):
        event.Skip()

    def OnLstCfgListItemSelected(self, event):
        event.Skip()
        #idx=event.GetIndex()
        #sAppl=self.lstCfg.GetItem(idx,0).m_text
        #sAlias=self.lstCfg.GetItem(idx,1).m_text
        #wx.PostEvent(self,vToolsApplAliasSelected(self,sAppl,sAlias))

    def OnCbSelAllAliasesButton(self, event):
        iLen=self.clstAliases.GetCount()
        for i in range(iLen):
            self.clstAliases.Check(i,True)
        event.Skip()
    def OnCbUnselAllAliasesButton(self, event):
        iLen=self.clstAliases.GetCount()
        for i in range(iLen):
            self.clstAliases.Check(i,False)
        event.Skip()

    def OnCbUpdateAliasesButton(self, event):
        event.Skip()
        iLen=self.clstAliases.GetCount()
        self.dApplAliasCfg={}
        for i in range(iLen):
            if self.clstAliases.IsChecked(i):
                s=self.clstAliases.GetString(i)
                #print s
                try:
                    sAppl,sAlias=s.split(':')
                    self.dApplAliasCfg[s]=[sAppl,sAlias,None]
                    wx.PostEvent(self,vToolsApplAliasSelected(self,sAppl,sAlias))
                except:
                    pass
        wx.PostEvent(self,vToolsApplAliasSelected(self,None,None))
    def SetApplAliasCfgAliasDict(self,sAppl,sAlias,d):
        try:
            if sAppl is not None:
                self.dApplAliasCfg[sAppl+':'+sAlias][2]=d
            else:
                #vtLog.CallStack('')
                #vtLog.pprint(self.dApplAliasCfg)
                self.__showApplAliasLst__()
        except:
            pass
