#----------------------------------------------------------------------------
# Name:         vXmlNodeXmlSrv.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060708
# CVS-ID:       $Id: vXmlNodeXmlSrv.py,v 1.2 2006/08/29 10:06:31 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vSrv.net.vsNetXmlNodeXmlSrv import vsNetXmlNodeXmlSrv
try:
    if vcCust.is2Import(__name__):
        from vidarc.vSrv.vXmlSrv.vXmlNodeXmlSrvPanel import vXmlNodeXmlSrvPanel
        from vidarc.vSrv.net.vsNetXmlNodeSrvEditDialog import vsNetXmlNodeSrvEditDialog
        from vidarc.vSrv.net.vsNetXmlNodeSrvAddDialog import vsNetXmlNodeSrvAddDialog
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeXmlSrv(vsNetXmlNodeXmlSrv):
    def __init__(self,tagName='vXmlSrv'):
        global _
        _=vtLgBase.assignPluginLang('vXmlSrv')
        vsNetXmlNodeXmlSrv.__init__(self,tagName)
    def GetDescription(self):
        return _(u'XML server')
    # ---------------------------------------------------------
    # specific
    def GetPortData(self,node):
        return self.GetVal(node,'portData',int,50000)
    def GetPortService(self,node):
        return self.GetVal(node,'portService',int,50001)
    def GetServerCls(self):
        from vidarc.tool.net.vtNetSrv import vtNetSrv
        return vtNetSrv
    def GetDataSrvCls(self):
        from vidarc.tool.net.vtNetXml import vtSrvXmlSock
        return vtSrvXmlSock
    def GetDataCls(self):
        from vidarc.tool.net.vtNetXml import vtNetXmlSock
        return vtNetXmlSock
    def GetServiceSrvCls(self):
        from vidarc.tool.net.vtNetSrv import vtSrvSock
        return vtSrvSock
    def GetServiceCls(self):
        from vidarc.tool.net.vtNetSrv import vtServiceSock
        return vtServiceSock
    # ---------------------------------------------------------
    # inheritance
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01LIDAT8\x8d\xa5\x93\xddM\xc30\x14\x85?\xa7\x1d\x80\x19*\xf5=\x0e\xf2$\
LP\x99\x08\x89\x11\x18\x80\x1d\x88\x92\x05\xbaJ\x84\x1d\xf1Z)]\xa1\x03\x14\
\x0e\x0f\xf9/T\x80\xb8\xd1\x95\xae\x14\x9f{~,\x1b\x93\xac\xf8O\xad\xaf\xfd\
\x90?\x0b"8\x0bu\x04\xc0T\xce\xfc\xb8@\xfe,\\7\x87\xdc\x02\x90\xd5\x16\x88\
\xbc\xbe\x9f\x05p\xbbZO\x8bL\xb2\x1a\x9b<\x88B]\xcf\xbe )H\xb2\x11\x85\xc2\
\x8b=\x1a0\xc9\x82\xdeu\x8c\xdcL\x04\x01As\x8fo\x0c\x15\x1e\xef*\xecv\x86\
\x19\xd9\x0bi\xa1`\x8f\xd8#\x9b\xf6*\xa2\x97\x8d\xddl#\xa3\x8aIA\xbd\x0c\x8c\
\x93\xe0$\xe2\x13X\x0bYZ\xc1\x0el\x03\xf10\t\x98-\x18\x06\x8bu\x06\xf2\xbe\
\x81\xea\xd1c{w\xec\xfaS\xcf\x97\x16\xf2 \xd2 \x1bQ{l\x15\xa2W(\xbc\xdac;YI\
\x83H\xcbq\xbe\xb0\x008\x88\xbb\xc0\xddiCv\xa8\x18\xae\x13 \xba\xd0O\x19\xb1\
)\xa1\tK\x0b\xe6\xcd\x99\xf9\x92\xa16\xf5\xa6\x93[ww\x02\x01R0\xc9\x83Yf0*\
\xe9\xc0vK\xa7\x02\x88\x94=\x18h2h\xbe\x0bqP1H\xde\x95SXM6\x81\t#;\x80\xb9\
\xf6\x98\xf4\xf1"\x00RF\xc69\xf0\xc7\x05\xbf\xad\xaf\x19\xfc\xb1>\x01\'\xd5\
\xa8A\x03\x84\x96\xda\x00\x00\x00\x00IEND\xaeB`\x82' 
    def GetEditDialogClass(self):
        if GUI:
            return vsNetXmlNodeSrvEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return vsNetXmlNodeSrvAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeXmlSrvPanel
        else:
            return None
