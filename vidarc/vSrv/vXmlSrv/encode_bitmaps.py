#!/usr/bin/env python
#----------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    #"   -u -i -n Application Tools01_16.png images.py",
#    "   -u -i -n Mondrian mondrian.ico images.py",
    "-u -i -n Prj img/Prj03_16.png images.py",
    "-a -u -n PrjSel img/Prj02_16.png images.py",
    "-a -u -n Prjs img/Prjs01_16.png images.py",
    "-a -u -n PrjsSel img/Prjs01_16.png images.py",
    "-a -u -n Year img/Year03_16.png images.py",
    "-a -u -n Month img/Month03_16.png images.py",
    "-a -u -n Day img/Day03_16.png images.py",
    "-a -u -n User img/Usr02_16.png images.py",
    "-a -u -n Client img/Client01_16.png images.py",
    "-a -u -n Browse img/BrowseFile01_16.png images.py",
    "-a -u -n Build img/Build02_16.png images.py",
    "-a -u -n Add img/add.png images.py",
    "-a -u -n Cancel img/abort.png images.py",
    "-a -u -n Del img/waste.png images.py",
    "-a -u -n Apply img/ok.png images.py",
    "-a -u -n Hollow img/Windows01_16.png images.py",
    "-a -u -n Open img/Open01_16.png images.py",
    "-a -u -n Close img/Close01_16.png images.py",
    "-a -u -n SecAcl img/Safe02_16.png images.py",
    "-a -u -n Collapse img/Collapse01_32.png images.py",
    "-a -u -n Expand img/Expand01_32.png images.py",
    "-a -u -n Lunch img/vLunch01_32.png images.py",
    "-a -u -n DropTarget img/DropTarget01_32.png images.py",
    "-a -u -n Application img/vServer02_16.ico images.py",
    "-a -u -n Plugin img/vServer02_16.png images.py",
    "-a -u -n Module img/VidMod_XmlSrvAlias_16.png images.py",
    "-a -u -n Settings img/Settings02_32.png images.py",
    "-a -u -n Tools img/Tools01_32.png images.py",
    "-a -u -n SettingsSmall img/Settings02_16.png images.py",
    "-a -u -n vBill img/vLunchBill01_32.png images.py",
    "-a -u -n vCustomer img/vLunchCustomer01_32.png images.py",
    "-a -u -n vDoc img/vLunchDoc01_32.png images.py",
    "-a -u -n vHum img/vLunchHum01_32.png images.py",
    "-a -u -n vLoc img/vLunchLoc01_32.png images.py",
    "-a -u -n vTask img/vLunchTask01_32.png images.py",
    "-a -u -n vPrj img/vLunchPrj01_32.png images.py",
    "-a -u -n vPrjDoc img/vLunchPrjDoc01_32.png images.py",
    "-a -u -n vPrjEng img/vLunchPrjEng01_32.png images.py",
    "-a -u -n vPrjPlan img/vLunchPrjPlan01_32.png images.py",
    "-a -u -n vPrjTimer img/vLunchPrjTimer01_32.png images.py",
    "-a -u -n vPurchase img/vLunchPurchase01_32.png images.py",
    "-a -u -n vRessource img/vLunchRessource01_32.png images.py",
    "-a -u -n vSoftware img/vLunchSoftware01_32.png images.py",
    "-a -u -n vSupplier img/vLunchSupplier01_32.png images.py",
    "-a -u -n vSec img/vLunchSec01_32.png images.py",
    "-a -u -n vGlobals img/vLunchGlobals01_32.png images.py",
    "-a -u -n vPlugIn img/vLunchPlugIn01_32.png images.py",
    "-a -u -n Net img/vLunchNet01_32.png images.py",
    "-a -u -n Tools img/vLunchTools01_32.png images.py",
    "-a -u -n vVSAS img/vLunchVSAS_32.png images.py",
    #"-a -u -n vVSAS img/vLunchVSAS_32.png images.py",
    "-a -u -n Sec img/SecKey01_16.png images.py",
    "-a -u -n SecLocked img/SecLocked01_16.png images.py",
    "-a -u -n SecUnlocked img/SecUnlocked01_16.png images.py",
    "-a -u -n PosLT img/vLunchPosLT_16.png images.py",
    "-a -u -n PosRT img/vLunchPosRT_16.png images.py",
    "-a -u -n PosLB img/vLunchPosLB_16.png images.py",
    "-a -u -n PosRB img/vLunchPosRB_16.png images.py",
    
    "-a -u -n Connect img/Connect01_16.png images.py",
    "-a -u -n ShutDown img/SrvStop01_16.png images.py",
    "-a -u -n Login img/SrvLogin01_16.png images.py",
    "-a -u -n Running img/Running01_16.png images.py",
    "-a -u -n Stopped img/Stopped01_16.png images.py",
    "-a -u -n Update img/Synch05_16.png images.py",
    
    "-a -u -n Save img/Save01_16.png images.py",
    #"-a -u -n User Usr01_16.png hum_tree_images.py",
    #"-a -u -n UserSel Usr01_16.png hum_tree_images.py",
    #"-a -u -n Human Human01_16.png hum_tree_images.py",
    #"-a -u -n HumanSel Human01_16.png hum_tree_images.py",
    #"-a -u -n Add add.png images.py",
    #"-a -u -n Del waste.png images.py",
    #"-a -u -n Apply checkmrk.png images.py",
    # "-u -i -n Splash img/splashPrj01.png splash.py",
    
    #"-a -u -n NoIcon  img/noicon.png  images.py"
    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)

