#Boa:Dialog:vSecAclDialog
#----------------------------------------------------------------------------
# Name:         vSecAclDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vSecAclDialog.py,v 1.1 2005/12/13 14:44:25 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import images
import vidarc.tool.net.img_appl as img_appl
import vidarc.tool.log.vtLog as vtLog
import sys,traceback,string

from vidarc.vApps.vHum.vXmlHumGrpTreeList import *

VERBOSE=0

def create(parent):
    return vSecAclDialog(parent)

[wxID_VSECACLDIALOG, wxID_VSECACLDIALOGCBADDFILTER, wxID_VSECACLDIALOGCBAPPLY, 
 wxID_VSECACLDIALOGCBDEL, wxID_VSECACLDIALOGCBDELFILTER, 
 wxID_VSECACLDIALOGCBSET, wxID_VSECACLDIALOGCHCACLTYPE, 
 wxID_VSECACLDIALOGCHCAPPL, wxID_VSECACLDIALOGCHCFILTERVAL, 
 wxID_VSECACLDIALOGCHCHUM, wxID_VSECACLDIALOGCHKACTIVE, 
 wxID_VSECACLDIALOGLBLAPPL, wxID_VSECACLDIALOGLBLFILTER, 
 wxID_VSECACLDIALOGLBLHUM, wxID_VSECACLDIALOGLBLNAME, 
 wxID_VSECACLDIALOGLBLPOSACL, wxID_VSECACLDIALOGLSTCHKACL, 
 wxID_VSECACLDIALOGLSTFILTER, wxID_VSECACLDIALOGSPNFILTERNR, 
 wxID_VSECACLDIALOGSPNLEVEL, wxID_VSECACLDIALOGTXTFILTER, 
 wxID_VSECACLDIALOGTXTNAME, 
] = [wx.NewId() for _init_ctrls in range(22)]

class vSecAclDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VSECACLDIALOG, name=u'vSecAclDialog',
              parent=prnt, pos=wx.Point(423, 201), size=wx.Size(559, 371),
              style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vServer Security ACL Dialog')
        self.SetClientSize(wx.Size(551, 344))

        self.lblName = wx.StaticText(id=wxID_VSECACLDIALOGLBLNAME,
              label=u'Name', name=u'lblName', parent=self, pos=wx.Point(8, 8),
              size=wx.Size(28, 13), style=0)

        self.txtName = wx.TextCtrl(id=wxID_VSECACLDIALOGTXTNAME,
              name=u'txtName', parent=self, pos=wx.Point(40, 4),
              size=wx.Size(288, 21), style=0, value=u'')
        self.txtName.Enable(False)

        self.cbSet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VSECACLDIALOGCBSET,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Set', name=u'cbSet',
              parent=self, pos=wx.Point(472, 64), size=wx.Size(76, 30),
              style=0)
        self.cbSet.Bind(wx.EVT_BUTTON, self.OnCbSetButton,
              id=wxID_VSECACLDIALOGCBSET)

        self.cbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VSECACLDIALOGCBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Del', name=u'cbDel',
              parent=self, pos=wx.Point(472, 100), size=wx.Size(76, 30),
              style=0)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VSECACLDIALOGCBDEL)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VSECACLDIALOGCBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Apply', name=u'cbApply',
              parent=self, pos=wx.Point(232, 312), size=wx.Size(76, 30),
              style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VSECACLDIALOGCBAPPLY)

        self.lblAppl = wx.StaticText(id=wxID_VSECACLDIALOGLBLAPPL,
              label=u'Application', name=u'lblAppl', parent=self,
              pos=wx.Point(8, 36), size=wx.Size(80, 13), style=0)

        self.chcAppl = wx.Choice(choices=[], id=wxID_VSECACLDIALOGCHCAPPL,
              name=u'chcAppl', parent=self, pos=wx.Point(96, 32),
              size=wx.Size(112, 21), style=0)
        self.chcAppl.Bind(wx.EVT_CHOICE, self.OnChcApplChoice,
              id=wxID_VSECACLDIALOGCHCAPPL)

        self.lblPosAcl = wx.StaticText(id=wxID_VSECACLDIALOGLBLPOSACL,
              label=u'ACL', name=u'lblPosAcl', parent=self, pos=wx.Point(320,
              40), size=wx.Size(32, 16), style=0)

        self.lstchkAcl = wx.CheckListBox(choices=[],
              id=wxID_VSECACLDIALOGLSTCHKACL, name=u'lstchkAcl', parent=self,
              pos=wx.Point(320, 64), size=wx.Size(144, 72), style=0)

        self.chcHum = wx.Choice(choices=[], id=wxID_VSECACLDIALOGCHCHUM,
              name=u'chcHum', parent=self, pos=wx.Point(400, 4),
              size=wx.Size(112, 21), style=0)
        self.chcHum.Bind(wx.EVT_CHOICE, self.OnChcHumChoice,
              id=wxID_VSECACLDIALOGCHCHUM)

        self.lblHum = wx.StaticText(id=wxID_VSECACLDIALOGLBLHUM, label=u'Human',
              name=u'lblHum', parent=self, pos=wx.Point(336, 8),
              size=wx.Size(56, 13), style=0)

        self.chcAclType = wx.Choice(choices=[], id=wxID_VSECACLDIALOGCHCACLTYPE,
              name=u'chcAclType', parent=self, pos=wx.Point(360, 36),
              size=wx.Size(104, 21), style=0)
        self.chcAclType.Bind(wx.EVT_CHOICE, self.OnChcAclTypeChoice,
              id=wxID_VSECACLDIALOGCHCACLTYPE)

        self.chkActive = wx.CheckBox(id=wxID_VSECACLDIALOGCHKACTIVE, label=u'',
              name=u'chkActive', parent=self, pos=wx.Point(520, 8),
              size=wx.Size(16, 13), style=0)
        self.chkActive.SetValue(False)
        self.chkActive.Bind(wx.EVT_CHECKBOX, self.OnChkActiveCheckbox,
              id=wxID_VSECACLDIALOGCHKACTIVE)

        self.spnLevel = wx.SpinCtrl(id=wxID_VSECACLDIALOGSPNLEVEL, initial=0,
              max=16, min=0, name=u'spnLevel', parent=self, pos=wx.Point(472,
              36), size=wx.Size(50, 21), style=wx.SP_ARROW_KEYS)
        self.spnLevel.SetToolTipString(u'Security Level')
        self.spnLevel.Bind(wx.EVT_SPINCTRL, self.OnSpnLevelSpinctrl,
              id=wxID_VSECACLDIALOGSPNLEVEL)

        self.lblFilter = wx.StaticText(id=wxID_VSECACLDIALOGLBLFILTER,
              label=u'Filter', name=u'lblFilter', parent=self, pos=wx.Point(320,
              148), size=wx.Size(38, 13), style=0)

        self.txtFilter = wx.TextCtrl(id=wxID_VSECACLDIALOGTXTFILTER,
              name=u'txtFilter', parent=self, pos=wx.Point(320, 172),
              size=wx.Size(144, 21), style=0, value=u'*')

        self.chcFilterVal = wx.Choice(choices=[],
              id=wxID_VSECACLDIALOGCHCFILTERVAL, name=u'chcFilterVal',
              parent=self, pos=wx.Point(360, 144), size=wx.Size(104, 21),
              style=0)
        self.chcFilterVal.Bind(wx.EVT_CHOICE, self.OnChcFilterValChoice,
              id=wxID_VSECACLDIALOGCHCFILTERVAL)

        self.lstFilter = wx.ListCtrl(id=wxID_VSECACLDIALOGLSTFILTER,
              name=u'lstFilter', parent=self, pos=wx.Point(320, 200),
              size=wx.Size(144, 96), style=wx.LC_REPORT)
        self.lstFilter.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstFilterListItemDeselected,
              id=wxID_VSECACLDIALOGLSTFILTER)
        self.lstFilter.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstFilterListItemSelected, id=wxID_VSECACLDIALOGLSTFILTER)
        self.lstFilter.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstFilterListColClick,
              id=wxID_VSECACLDIALOGLSTFILTER)

        self.cbAddFilter = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VSECACLDIALOGCBADDFILTER,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'cbAddFilter',
              parent=self, pos=wx.Point(472, 172), size=wx.Size(76, 30),
              style=0)
        self.cbAddFilter.Bind(wx.EVT_BUTTON, self.OnCbAddFilterButton,
              id=wxID_VSECACLDIALOGCBADDFILTER)

        self.cbDelFilter = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VSECACLDIALOGCBDELFILTER,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Del', name=u'cbDelFilter',
              parent=self, pos=wx.Point(472, 208), size=wx.Size(76, 30),
              style=0)
        self.cbDelFilter.Bind(wx.EVT_BUTTON, self.OnCbDelFilterButton,
              id=wxID_VSECACLDIALOGCBDELFILTER)

        self.spnFilterNr = wx.SpinCtrl(id=wxID_VSECACLDIALOGSPNFILTERNR,
              initial=0, max=64, min=0, name=u'spnFilterNr', parent=self,
              pos=wx.Point(472, 144), size=wx.Size(50, 21),
              style=wx.SP_ARROW_KEYS)
        self.spnFilterNr.SetToolTipString(u'Security Level')
        self.spnFilterNr.Bind(wx.EVT_SPINCTRL, self.OnSpnLevelSpinctrl,
              id=wxID_VSECACLDIALOGSPNFILTERNR)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.doc=None
        self.node=None
        self.apps=[]
        self.humDocs=[]
        self.selHum=None
        
        self.trlstHuman=vXmlHumGrpTreeList(parent=self,id=wx.NewId(),pos=wx.Point(4,64),
            size=wx.Size(310,230),style=wx.TR_HAS_BUTTONS,name='trlstHumanAcl')
        self.verbose=VERBOSE
        EVT_VTXMLTREE_ITEM_SELECTED(self.trlstHuman,self.OnTreeLstItemSel)
        self.cbSet.SetBitmapLabel(images.getApplyBitmap())
        self.cbDel.SetBitmapLabel(images.getDelBitmap())
        self.cbAddFilter.SetBitmapLabel(images.getApplyBitmap())
        self.cbDelFilter.SetBitmapLabel(images.getDelBitmap())
        self.cbApply.SetBitmapLabel(images.getApplyBitmap())
        
        self.lstFilter.InsertColumn(0,u'nr',wx.LIST_FORMAT_LEFT,30)
        self.lstFilter.InsertColumn(1,u'value',wx.LIST_FORMAT_LEFT,60)
        self.lstFilter.InsertColumn(2,u'filter',wx.LIST_FORMAT_LEFT,200)
        
    def SetNode(self,doc,node,name,appls,applDocs,humDocs):
        self.idxSel=-1
        self.iSelFilter=-1
        self.txtName.SetValue(name)
        self.spnLevel.SetValue(0)
        self.doc=doc
        self.node=node
        self.nodeCfg=None
        self.appls=appls
        self.applDocs=applDocs
        self.humDocs=humDocs
        self.hosts=None
        self.selHumDoc=None
        self.selHum=None
        self.humAlias=''
        self.iMaxFilter=0
        self.dActAcl={}
        self.lstAclOrder=[]
        self.dActFilter={}
        self.lstActFilterVal=[]
        self.chcAppl.Clear()
        self.chcHum.Clear()
        # add human docs
        for tup in self.humDocs:
            self.chcHum.Append(tup[0])
        try:
            tup=self.applDocs['vHum']
            self.chcHum.SetStringSelection(tup[0])
            wx.CallAfter(self.__updateHuman__)
        except:
            try:
                self.chcHum.SetSelection(0)
            except:
                pass
        # add application docs
        keys=self.applDocs.keys()
        keys.sort()
        for k in keys:
            self.chcAppl.Append(k)
        try:
            sAppl=name[:string.find(name,':')]
            self.chcAppl.SetStringSelection(sAppl)
        except:
            self.chcAppl.SetSelection(0)
        wx.CallAfter(self.__updateApplAcl__)
        wx.CallAfter(self.__updateApplFilterVal__)
        if self.verbose:
            vtLog.vtLogCallDepth(self,'node:%s'%self.node)
    def GetNode(self):
        self.doc.AlignNode(self.node,iRec=2)
        pass
    def OnTreeLstItemSel(self,event):
        self.selHum=event.GetTreeNodeData()
        self.__updateSelHum__()
        event.Skip()
        
    def __updateSelHum__(self):
        tmpAlias,tmpAcl,tmpNode=self.__getSelAclNodes__()
        for i in range(self.lstchkAcl.GetCount()):
            self.lstchkAcl.Check(i,False)
        self.spnLevel.SetValue(0)
        if tmpAlias is None:
            return
        if tmpNode is None:
            return
        try:
            iLv=int(self.doc.getAttribute(tmpNode,'sec_level'))
            self.spnLevel.SetValue(iLv)
        except:
            self.spnLevel.SetValue(0)
        try:
            iAcl=long(self.doc.getAttribute(tmpNode,'acl'),16)
        except:
            return
        self.__setAclNum__(iAcl)
        
    def __getSelAclNodes__(self,bForce=False):
        if self.node is None:
            return None,None,None
        tmpAlias=self.doc.getChildForced(self.node,self.humAlias)
        if tmpAlias is None:
            return None,None,None
        if self.selHumDoc is None or self.selHum is None:
            return tmpAlias,None,None
        sTagName=self.selHumDoc.getTagName(self.selHum)
        id=self.selHumDoc.getKey(self.selHum)
        if len(id)<=0:
            return tmpAlias,None,None
        sAclType=self.chcAclType.GetStringSelection()
        tmpAcl=self.doc.getChildForced(tmpAlias,sAclType)
        if tmpAcl is None:
            return tmpAlias,None,None
        childs=self.doc.getChilds(tmpAcl,sTagName)
        tmpNode=None
        for c in childs:
            idChild=self.doc.getAttribute(c,'fid')
            try:
                if long(idChild)==long(id):
                    tmpNode=c
                    break
            except:
                pass
        if bForce:
            if tmpNode is None:
                tmpNode=self.doc.createSubNode(tmpAcl,sTagName)
                self.doc.setAttribute(tmpNode,'fid',id)
        if tmpNode is None:
            sName=self.selHumDoc.getNodeText(self.selHum,'name')
            self.doc.setNodeText(tmpNode,'name',sName)
        return tmpAlias,tmpAcl,tmpNode
    def __setAclNum__(self,iAcl):
        iMask=1
        for i in range(self.lstchkAcl.GetCount()):
            iMask=1<<self.doc.ACL_MAP2NUM[self.lstchkAcl.GetString(i)]
            if iAcl&iMask:
                self.lstchkAcl.Check(i,True)
            else:
                self.lstchkAcl.Check(i,False)
    def __getAclNum__(self):
        iAcl=0x0
        for i in range(self.lstchkAcl.GetCount()):
            if self.lstchkAcl.IsChecked(i):
                s=self.lstchkAcl.GetString(i)
                iNum=self.doc.ACL_MAP2NUM[s]
                iAcl|=1<<iNum
        return iAcl
    def OnCbSetButton(self, event):
        tmpAlias,tmpAcl,tmpNode=self.__getSelAclNodes__(True)
        if tmpAlias is None:
            return
        iAcl=self.__getAclNum__()
        self.doc.setAttribute(tmpNode,'acl','0x%08x'%iAcl)
        self.doc.AlignNode(tmpNode)
        vtLog.CallStack(tmpNode)
        event.Skip()

    def OnCbDelButton(self, event):
        tmpAlias,tmpAcl,tmpNode=self.__getSelAclNodes__()
        if tmpAlias is None:
            return
        if tmpNode is None:
            return
        self.doc.deleteNode(tmpNode)
        event.Skip()

    def OnCbApplyButton(self, event):
        iFound=0
        for c in self.doc.getChilds(self.node):
            if self.doc.getAttribute(c,'active')=='1':
                iFound+=1
        #if iFound>1:
        #    # error only one 
        
        self.GetNode()
        self.EndModal(1)
        event.Skip()
    def __updateFilter__(self):
        self.lstFilter.DeleteAllItems()
        tmpAlias,tmpAcl,tmpNode=self.__getSelAclNodes__()
        if tmpNode is None:
            return
        lstFltName=[]
        for it in self.lstActFilterVal:
            lstFltName.append(it[0])
        sAclType=self.chcAclType.GetStringSelection()
        sNode=sAclType
        lstFilter=[]
        iActFilter=0
        for c in self.doc.getChilds(tmpNode,'filter'):
            try:
                iNr=int(self.doc.getNodeText(c,'nr'))
            except:
                iNr=0
            for cc in self.doc.getChilds(c,'cmp'):
                #sNr=self.doc.getNodeText(c,'nr')
                sVal=self.doc.getNodeText(cc,'value')
                sFilter=self.doc.getNodeText(cc,'filter')
                #try:
                #    i=lstFltName.index(sNode)
                #except:
                #    i=sys.maxint
                i=sys.maxint
                lstFilter.append((iNr,i,sNode,sVal,sFilter,cc))
            #iActFilter+=1
        #self.iMaxFilter=iActFilter
        def compFunc(a,b):
            for i in range(5):
                iCmp=cmp(a[i],b[i])
                if iCmp!=0:
                    return iCmp
            return 0
        lstFilter.sort(compFunc)
        for iActFilter,i,sNode,sVal,sFilter,c in lstFilter:
            idx=self.lstFilter.InsertStringItem(sys.maxint,'%2d'%iActFilter)
            self.lstFilter.SetStringItem(idx,1,sVal,-1)
            self.lstFilter.SetStringItem(idx,2,sFilter,-1)
            
    def __updateApplFilterVal__(self):
        try:
            self.chcFilterVal.Clear()
            #sFilter=self.chcFilter.GetStringSelection()
            sAclType=self.chcAclType.GetStringSelection()
            lst=self.dActFilter[sAclType]
            self.lstActFilterVal=lst
            for it in lst:
                self.chcFilterVal.Append(it[0])
            self.__updateFilter__()
            self.spnFilterNr.SetValue(0)
        except:
            self.lstActFilterVal=[]
        if self.chcFilterVal.GetCount()<1:
            self.chcFilterVal.Enable(False)
            self.lstFilter.Enable(False)
            self.cbAddFilter.Enable(False)
            self.cbDelFilter.Enable(False)
            self.txtFilter.Enable(False)
        else:
            self.chcFilterVal.Enable(True)
            self.lstFilter.Enable(True)
            self.cbAddFilter.Enable(True)
            self.cbDelFilter.Enable(True)
            self.txtFilter.Enable(True)
            self.chcFilterVal.SetSelection(0)
            wx.CallAfter(self.__updateFilterInput__)
    def __updateApplAcl__(self):
        self.lstchkAcl.Clear()
        idx=self.chcAppl.GetSelection()
        sName=self.chcAppl.GetStringSelection()
        self.chcAclType.Clear()
        self.lstchkAcl.Clear()
        self.dActAcl={}
        self.lstAclOrder=[]
        try:
            sName,doc,tup=self.applDocs[sName]
            d,order=doc.GetPossibleAcl()
            self.dActFilter=doc.GetPossibleFilter()
            if d is None:
                return
            self.dActAcl=d
            self.lstAclOrder=order
            #keys=d.keys()
            #keys.sort()
            #for k in keys:
            #    self.chcAclType.Append(k)
            for k in self.lstAclOrder:
                self.chcAclType.Append(k)
            self.chcAclType.SetSelection(0)
            wx.CallAfter(self.__updateAclType__)
            wx.CallAfter(self.__updateApplFilterVal__)
        except:
            traceback.print_exc()
        pass
    def OnChcApplChoice(self, event):
        self.__updateApplAcl__()
        self.__updateApplFilterVal__()
        event.Skip()
    
    def __updateHuman__(self):
        try:
            i=self.chcHum.GetSelection()
            tup=self.humDocs[i]
            self.selHum=None
            self.humAlias=self.chcHum.GetStringSelection()
            tmpAlias,tmpAcl,tmpNode=self.__getSelAclNodes__()
            if tmpAlias is not None:
                if self.doc.getAttribute(tmpAlias,'active')=='1':
                    self.chkActive.SetValue(True)
                else:
                    self.chkActive.SetValue(False)
            else:
                self.chkActive.SetValue(False)
            self.selHumDoc=tup[1]
            self.trlstHuman.SetDoc(tup[1])
            self.trlstHuman.SetNode(None)
        except:
            traceback.print_exc()
        #for tup in self.humDocs:
        #    if tup[0]==sName
        #    self.chcHum.Append(tup[0])
    def OnChcHumChoice(self, event):
        self.__updateHuman__()
        event.Skip()

    def __updateAclType__(self):
        keys=self.dActAcl.keys()
        i=self.chcAclType.GetSelection()
        self.lstchkAcl.Clear()
        try:
            keys.sort()
            k=keys[i]
            for it in self.dActAcl[k]:
                self.lstchkAcl.Append(it)
        except:
            pass
        self.__updateSelHum__()
        
    def OnChcAclTypeChoice(self, event):
        self.__updateAclType__()
        self.__updateApplFilterVal__()
        event.Skip()

    def OnChkActiveCheckbox(self, event):
        tmpAlias,tmpAcl,tmpNode=self.__getSelAclNodes__()
        for c in self.doc.getChilds(self.node):
            self.doc.setAttribute(c,'active','0')
        if tmpAlias is not None:
            if self.chkActive.GetValue():
                self.doc.setAttribute(tmpAlias,'active','1')
            else:
                self.doc.setAttribute(tmpAlias,'active','0')
        event.Skip()

    def OnSpnLevelSpinctrl(self, event):
        tmpAlias,tmpAcl,tmpNode=self.__getSelAclNodes__()
        if tmpNode is not None:
            self.doc.setAttribute(tmpNode,'sec_level','%d'%self.spnLevel.GetValue())
        event.Skip()

    def __updateFilterInput__(self):
        try:
            i=self.chcFilterVal.GetSelection()
            it=self.lstActFilterVal[i]
            #if it[1]==self.doc.FILTER_TYPE_STRING:
            #    self.txtFilter.Show(True)
            #else:
            #    self.txtFilter.Show(False)
        except:
            pass
        
    def OnChcFilterValChoice(self, event):
        self.__updateFilterInput__()
        event.Skip()

    def __getFilterNode__(self,iNr):
        tmpAlias,tmpAcl,tmpNode=self.__getSelAclNodes__()
        if tmpNode is None:
            return tmpNode,None
        for c in self.doc.getChilds(tmpNode,'filter'):
            try:
                i=int(self.doc.getNodeText(c,'nr'))
            except:
                i=0
            if iNr!=i:
                continue
            return tmpNode,c
        return tmpNode,None
    def __getFilterCmpNode__(self,tmp):
        sNode,sVal,sFilter='','',''
        tmpCmp=None
        it=self.lstFilter.GetItem(self.iSelFilter,0)
        sNr=it.m_text
        it=self.lstFilter.GetItem(self.iSelFilter,1)
        sVal=it.m_text
        it=self.lstFilter.GetItem(self.iSelFilter,2)
        sFilter=it.m_text
        for cc in self.doc.getChilds(tmp,'cmp'):
        #if self.doc.getNodeText(c,'node')==sNode:
            if self.doc.getNodeText(cc,'value')==sVal:
                if self.doc.getNodeText(cc,'filter')==sFilter:
                    # found
                    tmpCmp=cc
                    break
        sNode=''
        #self.spnFilterNr.SetValue(iNr)
        return sNode,sVal,sFilter,tmp,tmpCmp
    def __getSelFilterNode__(self):
        sNode,sVal,sFilter='','',''
        tmp=None
        tmpCmp=None
        #iNr=self.spnFilterNr.GetValue()
        if self.iSelFilter<0:
            return sNode,sVal,sFilter,tmp,tmpCmp
        it=self.lstFilter.GetItem(self.iSelFilter,0)
        sNr=it.m_text
        it=self.lstFilter.GetItem(self.iSelFilter,1)
        sVal=it.m_text
        it=self.lstFilter.GetItem(self.iSelFilter,2)
        sFilter=it.m_text
        iNr=int(sNr)
        
        tmpAlias,tmpAcl,tmpNode=self.__getSelAclNodes__()
        vtLog.CallStack(tmpNode)
        if tmpNode is None:
            return sNode,sVal,sFilter,tmp,tmpCmp
        for c in self.doc.getChilds(tmpNode,'filter'):
            try:
                i=int(self.doc.getNodeText(c,'nr'))
            except:
                i=-1
            if iNr!=i:
                continue
            tmp=c
            for cc in self.doc.getChilds(c,'cmp'):
                if self.doc.getNodeText(cc,'value')==sVal:
                    if self.doc.getNodeText(cc,'filter')==sFilter:
                        # found
                        tmpCmp=cc
                        break
        sNode=''
        #self.spnFilterNr.SetValue(iNr)
        return sNode,sVal,sFilter,tmp,tmpCmp
    def OnCbAddFilterButton(self, event):
        try:
            sNode=sAclType=self.chcAclType.GetStringSelection()
            sVal=self.chcFilterVal.GetStringSelection()
            sFilter=self.txtFilter.GetValue()
        except:
            event.Skip()
            return
        iAcl=self.__getAclNum__()
        iFilterNr=self.spnFilterNr.GetValue()
        if self.iSelFilter==-1:
            # add
            tmpNode,tmp=self.__getFilterNode__(iFilterNr)
            
            idx=self.lstFilter.InsertStringItem(sys.maxint,'%2d'%self.iMaxFilter)
            self.iMaxFilter+=1
            self.lstFilter.SetStringItem(idx,1,sVal,-1)
            self.lstFilter.SetStringItem(idx,2,sFilter,-1)
            if tmpNode is not None:
                if tmp is None:
                    tmp=self.doc.createSubNode(tmpNode,'filter')    
                tmpCmp=self.doc.createSubNode(tmp,'cmp')
                self.doc.setAttribute(tmp,'acl','0x%08x'%iAcl)
                self.doc.setNodeText(tmp,'nr','%d'%iFilterNr)
                self.doc.setNodeText(tmpCmp,'value',sVal)
                self.doc.setNodeText(tmpCmp,'filter',sFilter)
                self.doc.AlignNode(tmpNode,iRec=2)
            self.__updateFilter__()
            pass
        else:
            # edit
            tmpAlias,tmpAcl,tmpNode=self.__getSelAclNodes__()
            sNodeOld,sValOld,sFilterOld,tmp,tmpCmp=self.__getSelFilterNode__()
            #print tmp
            #print tmpCmp
            #print sVal,sFilter
            if tmp is not None:
                self.doc.setAttribute(tmp,'acl','0x%08x'%iAcl)
                self.doc.setNodeText(tmp,'nr','%d'%iFilterNr)
                self.doc.setNodeText(tmpCmp,'node',sNode)
                self.doc.setNodeText(tmpCmp,'value',sVal)
                self.doc.setNodeText(tmpCmp,'filter',sFilter)
                self.doc.AlignNode(tmpNode,iRec=2)                
                self.__updateFilter__()
            self.iSelFilter=-1
            pass
        
        event.Skip()

    def OnCbDelFilterButton(self, event):
        if self.iSelFilter>=0:
            pass
        tmpAlias,tmpAcl,tmpNode=self.__getSelAclNodes__()
        if tmpNode is not None:
            if self.iSelFilter==-1:
                iFilterNr=self.spnFilterNr.GetValue()
                tmp,tmpFilter=self.__getFilterNode__(iFilterNr)
                self.doc.deleteNode(tmpFilter)
            else:
                sNodeOld,sValOld,sFilterOld,tmpFilter,tmpCmp=self.__getSelFilterNode__()
                self.doc.deleteNode(tmpCmp)
                if len(self.doc.getChilds(tmpFilter,'cmp'))==0:
                    self.doc.delteNode(tmpFilter)
            
            self.__updateFilter__()

        event.Skip()

    def OnLstFilterListItemDeselected(self, event):
        self.iSelFilter=-1
        event.Skip()

    def OnLstFilterListItemSelected(self, event):
        self.iSelFilter=event.GetIndex()
        sNode,sVal,sFilter,tmp,tmpCmp=self.__getSelFilterNode__()
        if tmp is not None:
            iAcl=self.doc.getAttribute(tmp,'acl')
            try:
                self.__setAclNum__(long(iAcl,16))
            except:
                pass
            
            try:
                iNum=int(self.doc.getNodeText(tmp,'nr'))
            except:
                iNum=0
            self.spnFilterNr.SetValue(iNum)
            try:
                self.chcFilterVal.SetStringSelection(sVal)
                self.txtFilter.SetValue(sFilter)
            except:
                pass
        event.Skip()

    def OnLstFilterListColClick(self, event):
        if self.iSelFilter!=-1:
            self.lstFilter.SetItemState(self.iSelFilter,wx.LIST_MASK_STATE,
                    wx.LIST_STATE_SELECTED)
        self.iSelFilter=-1
        event.Skip()

