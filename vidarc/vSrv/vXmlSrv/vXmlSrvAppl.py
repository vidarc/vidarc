#!/usr/bin/env python
#Boa:App:BoaApp
#----------------------------------------------------------------------------
# Name:         vXmlSrvAppl.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vXmlSrvAppl.py,v 1.4 2007/07/30 09:05:05 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import os
import sys
import locale
import getopt

import vidarc.vSrv.vSplashFrame as vSplashFrame
import __init__
if __init__.VIDARC_IMPORT:
    import vidImp

#import vXmlSrvMainFrame
import vtLogDef
import vtLgBase

modules ={u'vXmlSrvMainFrame': [1, 'Main frame of Application', u'vXmlSrvMainFrame.py']}

class BoaApp(wx.App):
    def __init__(self,num,sAppl,cfgFN,fn,iLogLv,iSockPort,bTime=False):
        self.fn=fn
        self.cfgFN=cfgFN
        self.iLogLv=iLogLv
        self.iSockPort=iSockPort
        self.sAppl=sAppl
        self.bTime=bTime
        wx.App.__init__(self,num)
    def OnInit(self):
        wx.InitAllImageHandlers()
        actionList=[]
        actionListPost=[]
        import __init__
        if __init__.VIDARC_IMPORT:
            d={'label':_(u'  import library ...'),
                'eval':'__import__("vidImp")'}
            actionList.append(d)
        else:
            d={'label':_(_(u'  import library ...')),
                'eval':'0'}
            actionList.append(d)
        d={'label':u'  import vXmlSrvMainFrame ...',
            'eval':'__import__("vidarc.vSrv.vXmlSrv.vXmlSrvMainFrame")'}
        actionList.append(d)
        d={'label':u'  Create XML server ...',
            'eval':'self.res[1].vSrv.vXmlSrv.vXmlSrvMainFrame'}
        actionList.append(d)
        d={'label':u'  Create XML server ...',
            'eval':'getattr(self.res[2],"create")'}
        actionList.append(d)
        d={'label':u'  Create XML server ...',
            'eval':'self.res[3](None)'}
        actionList.append(d)
        
        d={'label':u'  Finished.'}
        actionList.append(d)
        #import images_splash
        self.splash = vSplashFrame.create(None,
            'MES','XML Server',
            None,#images_splash.getSplashBitmap(),
            actionList,
            actionListPost,
            self.sAppl,
            self.iLogLv,
            self.iSockPort,
            self.bTime)
        vSplashFrame.EVT_SPLASH_ACTION(self.splash,self.OnSplashAction)
        vSplashFrame.EVT_SPLASH_PROCESSED(self.splash,self.OnSplashProcessed)
        vSplashFrame.EVT_SPLASH_FINISHED(self.splash,self.OnSplashFinished)
        vSplashFrame.EVT_SPLASH_ABORTED(self.splash,self.OnSplashAborted)
        self.splash.Show()
        
        self.fn=None
        self.cfgFN=None
        return True
    def OpenFile(self,fn):
        self.fn=fn
    def OpenCfgFile(self,fn):
        self.cfgFN=fn
    def OnSplashAction(self,evt):
        self.splash.DoProcess()
        evt.Skip()
    def OnSplashProcessed(self,evt):
        #if evt.GetIdx()==4:
        #    self.main=self.splash.res[3]
        #    self.SetTopWindow(self.main)
        #    if self.fn is not None:
        #        self.main.OpenFile(self.fn)
        self.splash.DoAction()
        evt.Skip()
    def OnSplashFinished(self,evt):
        self.main=self.splash.res[4]
        self.SetTopWindow(self.main)
        self.main.Show()
        self.splash.Destroy()
        self.splash=None
        evt.Skip()
    def OnSplashAborted(self,evt):
        try:
            self.main=self.splash.res[4]
            self.main.Destroy()
        except:
            pass
        self.splash.Destroy()
        self.splash=None
        evt.Skip()
def showHelp():
    print _('help')
    print sys.version
    print "  ",_("valid flags:")
    print "  ","-h"
    print "  ",_("--help")
    print "        ",_("show this screen")
    print "  ","-l"
    print "  ",_("--lang <language id according ISO639>")
    print "  ",_("--log <level>")
    print "       ",_("0 = debug")
    print "       ",_("1 = information")
    print "       ",_("2 = waring")
    print "       ",_("3 = error")
    print "       ",_("4 = critical")
    print "       ",_("5 = fatal")

def main():
    optshort,optlong='l:h',['lang=','log=','help']
    vtLgBase.initAppl(optshort,optlong,'vXmlSrv')
    fn=None
    cfgFN='vXmlSrvCfg.xml'
    iLogLv=vtLogDef.ERROR
    try:
        optlist , args = getopt.getopt(sys.argv[1:],'h',['log=','help'])
        for o in optlist:
            if o[0] in ['-h','--help']:
                showHelp()
                return
            if o[0] in ['--log']:
                if o[1]=='0':
                    iLogLv=vtLogDef.DEBUG
                elif o[1]=='1':
                    iLogLv=vtLogDef.INFO
                elif o[1]=='2':
                    iLogLv=vtLogDef.WARN
                elif o[1]=='3':
                    iLogLv=vtLogDef.ERROR
                elif o[1]=='4':
                    iLogLv=vtLogDef.CRITICAL
                elif o[1]=='5':
                    iLogLv=vtLogDef.FATAL
    except:
        showHelp()
    #vtLog.vtLngInit('vXmlSrv','vXmlSrv.log',iLogLv,iSockPort=60000,bTime=True)
    
    application = BoaApp(0,'vXmlSrv',cfgFN,fn,iLogLv,60000,bTime=True)
    application.MainLoop()

if __name__ == '__main__':
    main()
