#Boa:FramePanel:vSettingsPanel
#----------------------------------------------------------------------------
# Name:         vSettingsPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vSettingsPanel.py,v 1.3 2008/02/02 16:11:23 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
from wx.lib.anchors import LayoutAnchors

import sys,string,os.path,sha,binascii
import images
import vidarc.tool.net.img_appl as img_appl

from vidarc.vSrv.vXmlSrv.vInfoPanel import vgpInfoChanged
import vidarc.tool.log.vtLog as vtLog

VERBOSE=0
#import boa.tool.xml.vtXmlDom as vtXmlDomTree

[wxID_VSETTINGSPANEL, wxID_VSETTINGSPANELCBADD, 
 wxID_VSETTINGSPANELCBAPPLYSERVICE, wxID_VSETTINGSPANELCBBROWSEDIR, 
 wxID_VSETTINGSPANELCBDEL, wxID_VSETTINGSPANELCHCAPPL, 
 wxID_VSETTINGSPANELCHCCLOSE, wxID_VSETTINGSPANELLBBSERVICELOCKED, 
 wxID_VSETTINGSPANELLBLAPPL, wxID_VSETTINGSPANELLBLCLOSEDELAY, 
 wxID_VSETTINGSPANELLBLCLOSEUNIT, wxID_VSETTINGSPANELLBLDATAPORT, 
 wxID_VSETTINGSPANELLBLDIR, wxID_VSETTINGSPANELLBLLOGIN, 
 wxID_VSETTINGSPANELLBLSAVE, wxID_VSETTINGSPANELLBLSAVEUNIT, 
 wxID_VSETTINGSPANELLBLSERVICEPORT, wxID_VSETTINGSPANELLSTLOGIN, 
 wxID_VSETTINGSPANELNBSETTINGS, wxID_VSETTINGSPANELPNACCESS, 
 wxID_VSETTINGSPANELPNFILE, wxID_VSETTINGSPANELPNXML, 
 wxID_VSETTINGSPANELSPDATAPORT, wxID_VSETTINGSPANELSPNCLOSE, 
 wxID_VSETTINGSPANELSPNLV, wxID_VSETTINGSPANELSPNSAVE, 
 wxID_VSETTINGSPANELSPSERVICEPORT, wxID_VSETTINGSPANELTGBPASSWD, 
 wxID_VSETTINGSPANELTXTDATAPASSWD, wxID_VSETTINGSPANELTXTFILESRVDN, 
 wxID_VSETTINGSPANELTXTLOGIN, wxID_VSETTINGSPANELTXTPASSWD, 
 wxID_VSETTINGSPANELTXTSERVICEPASSWD, 
] = [wx.NewId() for _init_ctrls in range(33)]


class vFileDropTarget(wx.FileDropTarget):
    def __init__(self, window):
        wx.FileDropTarget.__init__(self)
        self.window = window
    def OnDropFiles(self, x, y, filenames):
        for file in filenames:
            self.window.AddFile(u'',file)
            pass

class vImageDropTarget(wx.FileDropTarget):
    def __init__(self, window):
        wx.FileDropTarget.__init__(self)
        self.window = window
    def OnDropFiles(self, x, y, filenames):
        for file in filenames:
            self.window.SetImageFile(file)
            pass

class vSettingsPanel(wx.Panel):
    def _init_coll_nbSettings_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.pnXml, select=True, text=u'XML')
        parent.AddPage(imageId=-1, page=self.pnAccess, select=False,
              text=u'Access')
        parent.AddPage(imageId=-1, page=self.pnFile, select=False, text=u'File')

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VSETTINGSPANEL, name=u'vSettingsPanel',
              parent=prnt, pos=wx.Point(347, 220), size=wx.Size(409, 268),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(401, 241))
        self.SetAutoLayout(True)

        self.nbSettings = wx.Notebook(id=wxID_VSETTINGSPANELNBSETTINGS,
              name=u'nbSettings', parent=self, pos=wx.Point(8, 8),
              size=wx.Size(392, 232), style=0)
        self.nbSettings.SetConstraints(LayoutAnchors(self.nbSettings, True,
              True, True, True))

        self.pnXml = wx.Panel(id=wxID_VSETTINGSPANELPNXML, name=u'pnXml',
              parent=self.nbSettings, pos=wx.Point(0, 0), size=wx.Size(384,
              206), style=wx.TAB_TRAVERSAL)
        self.pnXml.SetAutoLayout(True)

        self.lblDataPort = wx.StaticText(id=wxID_VSETTINGSPANELLBLDATAPORT,
              label=u'Data Port', name=u'lblDataPort', parent=self.pnXml,
              pos=wx.Point(0, 12), size=wx.Size(45, 13), style=0)

        self.spDataPort = wx.SpinCtrl(id=wxID_VSETTINGSPANELSPDATAPORT,
              initial=0, max=65535, min=3000, name=u'spDataPort',
              parent=self.pnXml, pos=wx.Point(64, 8), size=wx.Size(80, 21),
              style=wx.SP_ARROW_KEYS)

        self.lblServicePort = wx.StaticText(id=wxID_VSETTINGSPANELLBLSERVICEPORT,
              label=u'Service Port', name=u'lblServicePort', parent=self.pnXml,
              pos=wx.Point(0, 36), size=wx.Size(58, 13), style=0)

        self.spServicePort = wx.SpinCtrl(id=wxID_VSETTINGSPANELSPSERVICEPORT,
              initial=0, max=65535, min=3000, name=u'spServicePort',
              parent=self.pnXml, pos=wx.Point(64, 32), size=wx.Size(80, 21),
              style=wx.SP_ARROW_KEYS)

        self.txtServicePasswd = wx.TextCtrl(id=wxID_VSETTINGSPANELTXTSERVICEPASSWD,
              name=u'txtServicePasswd', parent=self.pnXml, pos=wx.Point(152,
              32), size=wx.Size(100, 21), style=wx.TE_PASSWORD, value=u'')

        self.txtDataPasswd = wx.TextCtrl(id=wxID_VSETTINGSPANELTXTDATAPASSWD,
              name=u'txtDataPasswd', parent=self.pnXml, pos=wx.Point(152, 8),
              size=wx.Size(100, 21), style=wx.TE_PASSWORD, value=u'')

        self.lblSave = wx.StaticText(id=wxID_VSETTINGSPANELLBLSAVE,
              label=u'Save Delay', name=u'lblSave', parent=self.pnXml,
              pos=wx.Point(0, 64), size=wx.Size(55, 13), style=0)

        self.lbbServiceLocked = wx.StaticBitmap(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VSETTINGSPANELLBBSERVICELOCKED, name=u'lbbServiceLocked',
              parent=self.pnXml, pos=wx.Point(256, 36), size=wx.Size(16, 16),
              style=0)

        self.cbApplyService = wx.BitmapButton(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VSETTINGSPANELCBAPPLYSERVICE, name=u'cbApplyService',
              parent=self.pnXml, pos=wx.Point(280, 0), size=wx.Size(24, 24),
              style=wx.BU_AUTODRAW)
        self.cbApplyService.SetConstraints(LayoutAnchors(self.cbApplyService,
              True, False, False, False))
        self.cbApplyService.Bind(wx.EVT_BUTTON, self.OnCbApplyServiceButton,
              id=wxID_VSETTINGSPANELCBAPPLYSERVICE)

        self.spnSave = wx.SpinCtrl(id=wxID_VSETTINGSPANELSPNSAVE, initial=0,
              max=100, min=0, name=u'spnSave', parent=self.pnXml,
              pos=wx.Point(64, 60), size=wx.Size(80, 21),
              style=wx.SP_ARROW_KEYS)
        self.spnSave.SetValue(5)
        self.spnSave.Bind(wx.EVT_SPINCTRL, self.OnSpnSaveSpinctrl,
              id=wxID_VSETTINGSPANELSPNSAVE)
        self.spnSave.Bind(wx.EVT_TEXT, self.OnSpnSaveText,
              id=wxID_VSETTINGSPANELSPNSAVE)

        self.lblCloseDelay = wx.StaticText(id=wxID_VSETTINGSPANELLBLCLOSEDELAY,
              label=u'Close Delay', name=u'lblCloseDelay', parent=self.pnXml,
              pos=wx.Point(0, 96), size=wx.Size(56, 13), style=0)

        self.spnClose = wx.SpinCtrl(id=wxID_VSETTINGSPANELSPNCLOSE, initial=0,
              max=1000, min=10, name=u'spnClose', parent=self.pnXml,
              pos=wx.Point(64, 92), size=wx.Size(80, 21),
              style=wx.SP_ARROW_KEYS)
        self.spnClose.SetValue(60)
        self.spnClose.Bind(wx.EVT_SPINCTRL, self.OnSpnCloseSpinctrl,
              id=wxID_VSETTINGSPANELSPNCLOSE)
        self.spnClose.Bind(wx.EVT_TEXT, self.OnSpnCloseText,
              id=wxID_VSETTINGSPANELSPNCLOSE)

        self.chcClose = wx.CheckBox(id=wxID_VSETTINGSPANELCHCCLOSE,
              label=u'enable', name=u'chcClose', parent=self.pnXml,
              pos=wx.Point(176, 96), size=wx.Size(73, 13), style=0)
        self.chcClose.SetValue(False)
        self.chcClose.Bind(wx.EVT_CHECKBOX, self.OnChcCloseCheckbox,
              id=wxID_VSETTINGSPANELCHCCLOSE)

        self.lblSaveUnit = wx.StaticText(id=wxID_VSETTINGSPANELLBLSAVEUNIT,
              label=u'min', name=u'lblSaveUnit', parent=self.pnXml,
              pos=wx.Point(152, 64), size=wx.Size(16, 13), style=0)

        self.lblCloseUnit = wx.StaticText(id=wxID_VSETTINGSPANELLBLCLOSEUNIT,
              label=u'min', name=u'lblCloseUnit', parent=self.pnXml,
              pos=wx.Point(152, 96), size=wx.Size(16, 13), style=0)

        self.pnAccess = wx.Panel(id=wxID_VSETTINGSPANELPNACCESS,
              name=u'pnAccess', parent=self.nbSettings, pos=wx.Point(0, 0),
              size=wx.Size(384, 206), style=wx.TAB_TRAVERSAL)
        self.pnAccess.SetAutoLayout(True)

        self.chcAppl = wx.Choice(choices=[], id=wxID_VSETTINGSPANELCHCAPPL,
              name=u'chcAppl', parent=self.pnAccess, pos=wx.Point(64, 6),
              size=wx.Size(130, 21), style=0)

        self.lblLogin = wx.StaticText(id=wxID_VSETTINGSPANELLBLLOGIN,
              label=u'Login', name=u'lblLogin', parent=self.pnAccess,
              pos=wx.Point(0, 40), size=wx.Size(48, 13), style=0)

        self.txtLogin = wx.TextCtrl(id=wxID_VSETTINGSPANELTXTLOGIN,
              name=u'txtLogin', parent=self.pnAccess, pos=wx.Point(64, 38),
              size=wx.Size(72, 21), style=0, value=u'')

        self.txtPasswd = wx.TextCtrl(id=wxID_VSETTINGSPANELTXTPASSWD,
              name=u'txtPasswd', parent=self.pnAccess, pos=wx.Point(140, 38),
              size=wx.Size(56, 21), style=wx.TE_PASSWORD, value=u'')
        self.txtPasswd.Bind(wx.EVT_TEXT, self.OnTxtPasswdText,
              id=wxID_VSETTINGSPANELTXTPASSWD)

        self.spnLv = wx.SpinCtrl(id=wxID_VSETTINGSPANELSPNLV, initial=16,
              max=32, min=0, name=u'spnLv', parent=self.pnAccess,
              pos=wx.Point(236, 38), size=wx.Size(60, 21),
              style=wx.SP_ARROW_KEYS)

        self.cbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VSETTINGSPANELCBADD,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Set', name=u'cbAdd',
              parent=self.pnAccess, pos=wx.Point(305, 34), size=wx.Size(76, 30),
              style=0)
        self.cbAdd.SetConstraints(LayoutAnchors(self.cbAdd, False, True, True,
              False))
        self.cbAdd.Bind(wx.EVT_BUTTON, self.OnCbAddButton,
              id=wxID_VSETTINGSPANELCBADD)

        self.lstLogin = wx.ListView(id=wxID_VSETTINGSPANELLSTLOGIN,
              name=u'lstLogin', parent=self.pnAccess, pos=wx.Point(8, 72),
              size=wx.Size(288, 128), style=wx.LC_REPORT)
        self.lstLogin.SetConstraints(LayoutAnchors(self.lstLogin, True, True,
              True, True))
        self.lstLogin.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstLoginListItemDeselected,
              id=wxID_VSETTINGSPANELLSTLOGIN)
        self.lstLogin.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstLoginListItemSelected, id=wxID_VSETTINGSPANELLSTLOGIN)

        self.cbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VSETTINGSPANELCBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Del', name=u'cbDel',
              parent=self.pnAccess, pos=wx.Point(305, 70), size=wx.Size(76, 30),
              style=0)
        self.cbDel.SetConstraints(LayoutAnchors(self.cbDel, False, True, True,
              False))
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VSETTINGSPANELCBDEL)

        self.lblAppl = wx.StaticText(id=wxID_VSETTINGSPANELLBLAPPL,
              label=u'Application', name=u'lblAppl', parent=self.pnAccess,
              pos=wx.Point(0, 8), size=wx.Size(64, 13), style=0)

        self.tgbPasswd = wx.lib.buttons.GenBitmapToggleButton(ID=wxID_VSETTINGSPANELTGBPASSWD,
              bitmap=wx.EmptyBitmap(16, 16), name=u'tgbPasswd',
              parent=self.pnAccess, pos=wx.Point(200, 34), size=wx.Size(31, 30),
              style=0)

        self.pnFile = wx.Panel(id=wxID_VSETTINGSPANELPNFILE, name=u'pnFile',
              parent=self.nbSettings, pos=wx.Point(0, 0), size=wx.Size(384,
              206), style=wx.TAB_TRAVERSAL)
        self.pnFile.SetAutoLayout(True)

        self.lblDir = wx.StaticText(id=wxID_VSETTINGSPANELLBLDIR,
              label=u'Directory', name=u'lblDir', parent=self.pnFile,
              pos=wx.Point(8, 16), size=wx.Size(72, 13), style=wx.ALIGN_RIGHT)

        self.txtFileSrvDN = wx.TextCtrl(id=wxID_VSETTINGSPANELTXTFILESRVDN,
              name=u'txtFileSrvDN', parent=self.pnFile, pos=wx.Point(88, 12),
              size=wx.Size(208, 21), style=0, value=u'')
        self.txtFileSrvDN.SetConstraints(LayoutAnchors(self.txtFileSrvDN, True,
              True, True, False))

        self.cbBrowseDir = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VSETTINGSPANELCBBROWSEDIR,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'cbBrowseDir', parent=self.pnFile, pos=wx.Point(304, 8),
              size=wx.Size(76, 30), style=0)
        self.cbBrowseDir.SetConstraints(LayoutAnchors(self.cbBrowseDir, False,
              True, True, False))
        self.cbBrowseDir.Bind(wx.EVT_BUTTON, self.OnCbBrowseDirButton,
              id=wxID_VSETTINGSPANELCBBROWSEDIR)

        self._init_coll_nbSettings_Pages(self.nbSettings)

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        
        self.verbose=VERBOSE
        self.bLockPost=False
        self.idxSel=-1
        self.files=[]
        
        #dt = vFileDropTarget(self)
        #self.lstInfo.SetDropTarget(dt)
        
        #dt=vImageDropTarget(self)
        #self.lbbBitmap.SetDropTarget(dt)
        
        self.cbApplyService.SetBitmapLabel(images.getApplyBitmap())
        self.lbbServiceLocked.SetBitmap(images.getSecBitmap())
        
        self.spDataPort.SetValue(9000)
        self.spServicePort.SetValue(9001)
        
        self.cbAdd.SetBitmapLabel(images.getApplyBitmap())
        self.cbDel.SetBitmapLabel(images.getDelBitmap())
        
        self.tgbPasswd.SetBitmapLabel(images.getSecUnlockedBitmap())
        self.tgbPasswd.SetBitmapSelected(images.getSecLockedBitmap())
        
        self.dictImgAppl={}
        self.imgLstAppl=wx.ImageList(16,16)
        self.dictImgAppl['vLoc']=self.imgLstAppl.Add(img_appl.getLocBitmap())
        self.dictImgAppl['vHum']=self.imgLstAppl.Add(img_appl.getHumBitmap())
        self.dictImgAppl['vDoc']=self.imgLstAppl.Add(img_appl.getDocBitmap())
        self.dictImgAppl['vPrj']=self.imgLstAppl.Add(img_appl.getPrjBitmap())
        self.dictImgAppl['vTask']=self.imgLstAppl.Add(img_appl.getTaskBitmap())
        self.dictImgAppl['vPrjDoc']=self.imgLstAppl.Add(img_appl.getPrjDocBitmap())
        self.dictImgAppl['vPrjEng']=self.imgLstAppl.Add(img_appl.getPrjEngBitmap())
        self.dictImgAppl['vPrjTimer']=self.imgLstAppl.Add(img_appl.getPrjTimerBitmap())
        self.dictImgAppl['sec']=self.imgLstAppl.Add(images.getSecBitmap())
        self.lstLogin.SetImageList(self.imgLstAppl,wx.IMAGE_LIST_NORMAL)
        self.lstLogin.SetImageList(self.imgLstAppl,wx.IMAGE_LIST_SMALL)
        
        self.lstLogin.InsertColumn(0,u'appl',wx.LIST_FORMAT_LEFT,80)
        self.lstLogin.InsertColumn(1,u'login',wx.LIST_FORMAT_LEFT,80)
        self.lstLogin.InsertColumn(2,u'passwd',wx.LIST_FORMAT_LEFT,60)
        self.lstLogin.InsertColumn(3,u'level',wx.LIST_FORMAT_RIGHT,60)
        self.dApplLogins={}
        
        self.cbBrowseDir.SetBitmapLabel(images.getBrowseBitmap())
    def SetAppls(self,lst):
        self.lstAppls=lst
        self.chcAppl.Clear()
        for s in lst:
            self.chcAppl.Append(s)
        self.chcAppl.SetSelection(0)
        
    def SetNode(self,node,doc):
        self.dApplLogins={}
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'')
        self.idxImgFile=-1
        self.imgNew=None
        self.sImgFN=''
        self.hosts=[]
        self.sServicePasswd=''
        self.sFileSrvDN=''
        self.lstLogin.DeleteAllItems()
        
        self.node=node
        self.doc=doc
        self.bLockPost=True
        # this is the config
        #self.gcbbBrowse.Show(False)
        try:
            port=self.doc.getNodeText(node,'data_port')
            port=int(port)
        except:
            port=9000
        self.spDataPort.SetValue(port)
        try:
            port=self.doc.getNodeText(node,'service_port')
            port=int(port)
        except:
            port=9001
        self.spServicePort.SetValue(port)
        self.sServicePasswd=self.doc.getNodeText(node,'service_passwd')
        if len(self.sServicePasswd)>0:
            self.lbbServiceLocked.Show(True)
        else:
            self.lbbServiceLocked.Show(False)
        
        try:
            iMin=self.doc.getNodeText(node,'save_delay')
            iMin=int(iMin)
        except:
            iMin=5
        self.spnSave.SetValue(iMin)
        try:
            iMin=self.doc.getNodeText(node,'close_delay')
            iMin=int(iMin)
        except:
            iMin=600
        self.spnClose.SetValue(iMin)
        s=self.doc.getNodeText(node,'close_enable')
        if s=='1':
            self.chcClose.SetValue(True)
        else:
            self.chcClose.SetValue(False)
            
        self.bLockPost=False
        
        tmp=self.doc.getChild(node,'access')
        if tmp is not None:
            for c in self.doc.getChilds(tmp):
                tagName=self.doc.getTagName(c)
                try:
                    i=self.lstAppls.index(tagName)
                except:
                    continue
                try:
                    d=self.dApplLogins[tagName]
                except:
                    d={}
                    self.dApplLogins[tagName]=d
                for nodeLogin in self.doc.getChilds(c,'login'):
                    sLogin=self.doc.getNodeText(nodeLogin,'login')
                    sPasswd=self.doc.getNodeText(nodeLogin,'passwd')
                    try:
                        iLevel=int(self.doc.getNodeText(nodeLogin,'sec_level'))
                    except:
                        iLevel=0
                    d[sLogin]={'passwd':sPasswd,'level':iLevel}
        self.sFileSrvDN=self.doc.getNodeText(self.node,'fileSrvDN')
        
        self.__updateLogins__()
        self.txtFileSrvDN.SetValue(self.sFileSrvDN)
        pass
    def GetNode(self):
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'')
        if self.node is None:
            return
        self.doc.setNodeText(self.node,'data_port',str(self.spDataPort.GetValue()))
        self.doc.setNodeText(self.node,'service_port',str(self.spServicePort.GetValue()))
        passwd=self.txtDataPasswd.GetValue()
        #if len(passwd)>0:
        #    m5=md5.new()
        #    m5.update(passwd)
        #    passwd=m5.digest()
        self.doc.setNodeText(self.node,'data_passwd',passwd)
        self.doc.setNodeText(self.node,'service_passwd',self.sServicePasswd)
        self.doc.setNodeText(self.node,'save_delay',str(self.spnSave.GetValue()))
        self.doc.setNodeText(self.node,'close_delay',str(self.spnClose.GetValue()))
        if self.chcClose.GetValue():
            s=u'1'
        else:
            s=u'0'
        self.doc.setNodeText(self.node,'close_enable',s)
        
        tmp=self.doc.getChildForced(self.node,'access')
        for k in self.lstAppls:
            try:
                d=self.dApplLogins[k]
                keys=d.keys()
                if len(keys)==0:
                    nodeAppl=self.doc.getChild(tmp,k)
                    if nodeAppl is not None:
                        self.doc.deleteNode(nodeAppl,tmp)
                        continue
                nodeAppl=self.doc.getChildForced(tmp,k)
                for c in self.doc.getChilds(nodeAppl):
                    self.doc.deleteNode(c,nodeAppl)
                keys.sort()
                for sName in keys:
                    nodeLogin=self.doc.createSubNode(nodeAppl,'login')
                    self.doc.setNodeText(nodeLogin,'login',sName)
                    self.doc.setNodeText(nodeLogin,'passwd',d[sName]['passwd'])
                    self.doc.setNodeText(nodeLogin,'sec_level',str(d[sName]['level']))
            except:
                nodeAppl=self.doc.getChild(tmp,k)
                if nodeAppl is not None:
                    self.doc.deleteNode(nodeAppl,tmp)
        self.sFileSrvDN=self.txtFileSrvDN.GetValue()
        self.doc.setNodeText(self.node,'fileSrvDN',self.sFileSrvDN)
        self.doc.AlignNode(self.node,iRec=2)
        pass
    def __updateLogins__(self):
        self.idxLogin=-1
        keys=self.dApplLogins.keys()
        self.lstLogin.DeleteAllItems()
        keys.sort()
        for k in keys:
            try:
                imgIdx=self.dictImgAppl[k]
            except:
                imgIdx=-1
            try:
                d=self.dApplLogins[k]
            except:
                continue
            kkeys=d.keys()
            kkeys.sort()
            for sLogin in kkeys:
                sPasswd=d[sLogin]['passwd']
                idx=self.lstLogin.InsertImageStringItem(sys.maxint,k,imgIdx)
                if len(sPasswd)==0:
                    imgIdxPass=-1
                    sPassLst=''
                else:
                    imgIdxPass=self.dictImgAppl['sec']
                    sPassLst='********'
                self.lstLogin.SetStringItem(idx,1,sLogin,imgIdxPass)
                self.lstLogin.SetStringItem(idx,2,sPassLst,imgIdxPass)
                self.lstLogin.SetStringItem(idx,3,str(d[sLogin]['level']),-1)
    def SetImageFile(self,file):
        if len(file)>0:
            if self.dictImg.has_key(file):
                i=self.dictImg[file]
                bmp=self.imgLst.GetBitmap(i)
                self.idxImgFile=i
                self.imgNew=None
            else:
                try:
                    bmp=wx.Image(file).ConvertToBitmap()
                    self.sImgFN=file
                    self.idxImgFile=-1
                    self.imgNew=bmp
                except:
                    self.idxImgFile=-1
                    self.sImgFN=''
                    self.imgNew=None
                    return
        self.lbbBitmap.SetBitmap(wx.EmptyBitmap(32, 32))
        self.lbbBitmap.Refresh()
        self.lbbBitmap.SetBitmap(bmp)
        self.lbbBitmap.Refresh()
        pass
    def OnCbApplyServiceButton(self, event):
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'')
        passwd=self.txtServicePasswd.GetValue()
        if len(passwd)>0:
            m5=sha.new()
            m5.update(passwd)
            passwd=m5.hexdigest()
        self.sServicePasswd=passwd
        if len(self.sServicePasswd)>0:
            self.lbbServiceLocked.Show(True)
        else:
            self.lbbServiceLocked.Show(False)
        wx.PostEvent(self,vgpInfoChanged(self))
        event.Skip()
    
    def GetDataPort(self):
        return self.spDataPort.GetValue()
    def GetServicePort(self):
        return self.spServicePort.GetValue()
    def GetServicePasswd(self):
        passwd=self.doc.getNodeText(self.node,'service_passwd')
        return passwd
    
    def OnSpnSaveSpinctrl(self, event):
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'')
        wx.PostEvent(self,vgpInfoChanged(self))
        event.Skip()

    def OnSpnCloseSpinctrl(self, event):
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'')
        wx.PostEvent(self,vgpInfoChanged(self))
        event.Skip()

    def OnChcCloseCheckbox(self, event):
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'')
        wx.PostEvent(self,vgpInfoChanged(self))
        event.Skip()

    def OnSpnSaveText(self, event):
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'')
        wx.PostEvent(self,vgpInfoChanged(self))
        event.Skip()

    def OnSpnCloseText(self, event):
        if self.verbose>0:
            vtLog.vtLogCallDepth(self,'')
        wx.PostEvent(self,vgpInfoChanged(self))
        event.Skip()

    def OnCbAddButton(self, event):
        sAppl=self.chcAppl.GetStringSelection()
        try:
            d=self.dApplLogins[sAppl]
        except:
            d={}
            self.dApplLogins[sAppl]=d
        sLogin=self.txtLogin.GetValue()
        if self.tgbPasswd.GetToggle():
            passwd=self.txtPasswd.GetValue()
            if len(passwd)>0:
                m5=sha.new()
                m5.update(passwd)
                passwd=m5.hexdigest()
            else:
                try:
                    passwd=d[sLogin]['passwd']
                except:
                    passwd=''
        else:
            passwd=''
        try:
            iLevel=self.spnLv.GetValue()
        except:
            iLevel=0
        d[sLogin]={'passwd':passwd,'level':iLevel}
        for k in self.lstAppls:
            if k==sAppl:
                continue
            try:
                dTmp=self.dApplLogins[k]
                try:
                    s=dTmp[sLogin]
                    dTmp[sLogin]=passwd
                except:
                    pass
            except:
                pass
        self.__updateLogins__()
        self.txtPasswd.SetValue('')
        wx.PostEvent(self,vgpInfoChanged(self))
        event.Skip()

    def OnCbDelButton(self, event):
        if self.idxLogin<0:
            return
        sAppl=self.lstLogin.GetItem(self.idxLogin,0).m_text
        sLogin=self.lstLogin.GetItem(self.idxLogin,1).m_text
        try:
            d=self.dApplLogins[sAppl]
        except:
            return
        try:
            del d[sLogin]
        except:
            pass
        self.__updateLogins__()
        wx.PostEvent(self,vgpInfoChanged(self))
        event.Skip()

    def OnLstLoginListItemDeselected(self, event):
        self.idxLogin=-1
        event.Skip()

    def OnLstLoginListItemSelected(self, event):
        self.idxLogin=event.GetIndex()
        sAppl=self.lstLogin.GetItem(self.idxLogin,0).m_text
        sLogin=self.lstLogin.GetItem(self.idxLogin,1).m_text
        try:
            iLevel=int(self.lstLogin.GetItem(self.idxLogin,3).m_text)
        except:
            iLevel=0
        try:
            self.chcAppl.SetStringSelection(sAppl)
        except:
            self.chcAppl.SetSelection(0)
        try:
            if len(self.dApplLogins[sAppl][sLogin]['passwd'])>0:
                self.tgbPasswd.SetToggle(True)
            else:
                self.tgbPasswd.SetToggle(False)
        except:
            self.tgbPasswd.SetToggle(False)
        self.txtLogin.SetValue(sLogin)
        self.txtPasswd.SetValue('')
        self.spnLv.SetValue(iLevel)
        event.Skip()

    def OnTxtPasswdText(self, event):
        if len(self.txtPasswd.GetValue())>0:
            self.tgbPasswd.SetToggle(True)
        event.Skip()

    def OnCbBrowseDirButton(self, event):
        dlg = wx.DirDialog(self)
        try:
            self.sFileSrvDN=self.txtFileSrvDN.GetValue()
            dlg.SetPath(self.sFileSrvDN)
            if dlg.ShowModal() == wx.ID_OK:
                dir = dlg.GetPath()
                # Your code
                self.sFileSrvDN=dir
                self.txtFileSrvDN.SetValue(dir)
                wx.PostEvent(self,vgpInfoChanged(self))
        finally:
            dlg.Destroy()
        event.Skip()
