#Boa:Dialog:vHostDialog
#----------------------------------------------------------------------------
# Name:         vHostDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vHostDialog.py,v 1.1 2005/12/13 14:44:25 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.masked.maskededit
import wx.lib.buttons

import sha
import images

def create(parent):
    return vHostDialog(parent)

[wxID_VHOSTDIALOG, wxID_VHOSTDIALOGCBAPPLY, wxID_VHOSTDIALOGCBCANCEL, 
 wxID_VHOSTDIALOGCHCAPPLYALL, wxID_VHOSTDIALOGLBLHOST, 
 wxID_VHOSTDIALOGLBLPASSWD, wxID_VHOSTDIALOGLBLUSR, wxID_VHOSTDIALOGTXTHOST, 
 wxID_VHOSTDIALOGTXTPASSWD, wxID_VHOSTDIALOGTXTUSR, 
] = [wx.NewId() for _init_ctrls in range(10)]

class vHostDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VHOSTDIALOG, name=u'vHostDialog',
              parent=prnt, pos=wx.Point(406, 218), size=wx.Size(205, 192),
              style=wx.DEFAULT_DIALOG_STYLE, title=u'vHost Dialog')
        self.SetClientSize(wx.Size(197, 165))

        self.lblHost = wx.StaticText(id=wxID_VHOSTDIALOGLBLHOST, label=u'Host',
              name=u'lblHost', parent=self, pos=wx.Point(32, 8),
              size=wx.Size(22, 13), style=0)

        self.txtHost = wx.TextCtrl(id=wxID_VHOSTDIALOGTXTHOST, name=u'txtHost',
              parent=self, pos=wx.Point(64, 4), size=wx.Size(100, 21), style=0,
              value=u'192.168.1.*')

        self.lblUsr = wx.StaticText(id=wxID_VHOSTDIALOGLBLUSR, label=u'User',
              name=u'lblUsr', parent=self, pos=wx.Point(32, 40),
              size=wx.Size(22, 13), style=0)

        self.txtUsr = wx.TextCtrl(id=wxID_VHOSTDIALOGTXTUSR, name=u'txtUsr',
              parent=self, pos=wx.Point(64, 36), size=wx.Size(100, 21), style=0,
              value=u'')

        self.lblPasswd = wx.StaticText(id=wxID_VHOSTDIALOGLBLPASSWD,
              label=u'Password', name=u'lblPasswd', parent=self, pos=wx.Point(8,
              72), size=wx.Size(46, 13), style=0)

        self.txtPasswd = wx.TextCtrl(id=wxID_VHOSTDIALOGTXTPASSWD,
              name=u'txtPasswd', parent=self, pos=wx.Point(64, 68),
              size=wx.Size(100, 21), style=wx.TE_PASSWORD, value=u'')

        self.chcApplyAll = wx.CheckBox(id=wxID_VHOSTDIALOGCHCAPPLYALL,
              label=u'apply to all', name=u'chcApplyAll', parent=self,
              pos=wx.Point(64, 96), size=wx.Size(112, 13), style=0)
        self.chcApplyAll.SetValue(False)
        
        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VHOSTDIALOGCBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Apply', name=u'cbApply',
              parent=self, pos=wx.Point(16, 128), size=wx.Size(76, 30),
              style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VHOSTDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VHOSTDIALOGCBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel', name=u'cbCancel',
              parent=self, pos=wx.Point(112, 128), size=wx.Size(76, 30),
              style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VHOSTDIALOGCBCANCEL)


    def __init__(self, parent):
        self._init_ctrls(parent)
        self.doc=None
        self.node=None
        self.cbApply.SetBitmapLabel(images.getApplyBitmap())
        self.cbCancel.SetBitmapLabel(images.getCancelBitmap())
        
    def SetDoc(self,doc):
        self.doc=doc
    def SetNode(self,node):
        if self.doc is None:
            self.node=None
            return
        self.node=node
        sHost=self.doc.getNodeText(self.node,'name')
        sUsr=self.doc.getNodeText(self.node,'user')
        sPasswd=self.doc.getNodeText(self.node,'passwd')
        self.txtHost.SetValue(sHost)
        self.txtUsr.SetValue(sUsr)
        self.txtPasswd.SetValue('')
        pass
    def GetNode(self):
        if self.node is None:
            return
        sHost=self.txtHost.GetValue()
        sUsr=self.txtUsr.GetValue()
        sPasswd=self.txtPasswd.GetValue()
        if len(sPasswd)>0:
            m5=sha.new()
            m5.update(sPasswd)
            sPasswd=m5.hexdigest()
        else:
            sPasswd=''
        self.doc.setNodeText(self.node,'name',sHost)
        self.doc.setNodeText(self.node,'user',sUsr)
        self.doc.setNodeText(self.node,'passwd',sPasswd)
        pass
    def GetNodes(self,lstNode):
        for n in lstNode:
            print n
            
    def IsApplyAll(self):
        return self.chcApplyAll.GetValue()
    def OnCbApplyButton(self, event):
        self.EndModal(1)
        event.Skip()

    def OnCbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
