#----------------------------------------------------------------------------
# Name:         vXmlNodeMsgSrvPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060707
# CVS-ID:       $Id: vXmlNodeMsgSrvPanel.py,v 1.2 2007/07/30 20:35:57 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.vSrv.net.vsNetXmlNodeSrvPanel import vsNetXmlNodeSrvPanel

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.net.vtNetXml import *
from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

class vXmlNodeMsgSrvPanel(vsNetXmlNodeSrvPanel):
    def Clear(self):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.Clear(self)
        
        # add code here
        self.viTag.Clear()
        self.viName.Clear()
        self.vsSrv.Clear()
        self.intData.SetValue(50004)
        self.intService.SetValue(50005)
        self.vsSrv.Enable(False)
        self.ClrBlockDelayed()
    def SetDoc(self,doc,bNet=False):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        
        # add code here
        self.viTag.SetDoc(doc)
        self.viName.SetDoc(doc)
        self.vsSrv.SetDoc(doc,bNet)
        self.vsSrv.SetDataSrvClass(vtSrvXmlSock)
        self.vsSrv.SetDataClass(vtNetXmlSock)
    def OnVsSrvVsnetSrvcontrolStarted(self, event):
        event.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
            netHum=self.doc.GetNetDoc('vHum')
            srv=self.vsSrv.GetNetSrv()
            if srv is None:
                vtLog.vtLngCurWX(vtLog.ERROR,'server is None',self)
                return
            srvXml=srv.GetDataInstance()
            if srvXml is None:
                vtLog.vtLngCurWX(vtLog.ERROR,'server data-instance is None',self)
                return
            srvXml.SetHumDoc4SecAcl(netHum)
            #self.objRegNode.AddServerInstanceByNode(self.node,srv)
        except:
            vtLog.vtLngTB(self.GetName())
        
