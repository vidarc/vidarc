#!/usr/bin/env python
#----------------------------------------------------------------------
#----------------------------------------------------------------------------
# Name:         encode_bitmaps.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060703
# CVS-ID:       $Id: encode_bitmaps.py,v 1.1 2006/07/17 10:40:23 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    "-u -i -n Application img/vServerMsg02_16.ico images.py",
    "-a -u -n Plugin img/vServerMsg02_16.png images.py",
    "-a -u -n Running img/Running01_16.png images.py",
    "-a -u -n Stopped img/Stopped01_16.png images.py",
    
    "-u -i -n Splash img/splashMsgSrv01.png images_splash.py",
    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)

