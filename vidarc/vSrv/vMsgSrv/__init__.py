#----------------------------------------------------------------------------
# Name:         __init__.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20060703
# CVS-ID:       $Id: __init__.py,v 1.2 2006/08/29 10:06:31 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

PLUGABLE='vMsgSrvMDIFrame'
PLUGABLE_FRAME=False
PLUGABLE_ACTIVE=0
PLUGABLE_SRV='vMsgSrv'
PLUGABLE_SRV_ORDER=1

import sys
if hasattr(sys, 'importers'):
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=1
else:
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=0
