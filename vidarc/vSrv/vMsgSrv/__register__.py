#----------------------------------------------------------------------------
# Name:         __register__.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060707
# CVS-ID:       $Id: __register__.py,v 1.1 2006/07/17 10:40:23 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.vSrv.vMsgSrv.vXmlNodeMsgSrv import vXmlNodeMsgSrv
from vidarc.vSrv.vMsgSrv.vXmlNodeMsgSrvAlias import vXmlNodeMsgSrvAlias
from vidarc.vSrv.net.vXmlNodeHosts import vXmlNodeHosts
from vidarc.vSrv.net.vsNetXmlNodeAliasRef import vsNetXmlNodeAliasRef

def RegisterNodes(self,oRoot,bRegAsRoot=True):
    try:
        oMsgSrv=vXmlNodeMsgSrv()
        oMsgSrvAlias=vXmlNodeMsgSrvAlias()
        oHosts=vXmlNodeHosts()
        oRef=vsNetXmlNodeAliasRef()
        
        self.RegisterNode(oMsgSrv,bRegAsRoot)
        self.RegisterNode(oMsgSrvAlias,False)
        self.RegisterNode(oHosts,False)
        self.RegisterNode(oRef,False)
        
        self.LinkRegisteredNode(oRoot,oMsgSrv)
        self.LinkRegisteredNode(oMsgSrv,oMsgSrvAlias)
        self.LinkRegisteredNode(oMsgSrv,oRef)
        self.LinkRegisteredNode(oMsgSrv,oHosts)
        self.LinkRegisteredNode(oMsgSrvAlias,oHosts)
        return oMsgSrv
    except:
        vtLog.vtLngTB(__name__)
    return None
