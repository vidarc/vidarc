#----------------------------------------------------------------------------
# Name:         vXmlMsgSrvTree.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060703
# CVS-ID:       $Id: vXmlMsgSrvTree.py,v 1.1 2006/07/17 10:40:23 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlGrpTree import *
from vidarc.vSrv.vMsgSrv.vNetMsgSrv import *

import vidarc.vSrv.vMsgSrv.images as imgMsgSrv

class vXmlMsgSrvTree(vtXmlGrpTree):
    def __init__(self, parent, id, pos, size, style, name,
                    master=False,controller=False,verbose=0):
        vtXmlGrpTree.__init__(self,id=id, name=name,master=master,
              controller=controller,verbose=verbose,
              parent=parent, pos=pos, size=size,style=style)
        self.bLangMenu=True
        self.bGrpMenu=True
        self.SetPossibleGrouping([
                ('normal'   , _(u'normal'), [] , 
                            {None:[('tag','',),('name','')],
                            #'user':[('name',''),('surname',''),('firstname','')],
                            #'group':[('name','')]
                            }),
                ('category'   , _(u'category'), [('category',''),] , 
                            {None:[('tag','',),('name','')],
                            }),
                ])
    def SetDftNodeInfos(self):
        self.SetNodeInfos(['tag','name','|id'])
    def SetupImageList(self):
        if vtXmlGrpTree.SetupImageList(self):
            self.__addElemImage2ImageList__('root',
                            imgMsgSrv.getPluginImage(),
                            imgMsgSrv.getPluginImage(),True)
            self.__addElemImage2ImageList__('MsgSrv',
                            imgMsgSrv.getPluginImage(),
                            imgMsgSrv.getPluginImage(),True)
            self.SetImageList(self.imgLstTyp)
