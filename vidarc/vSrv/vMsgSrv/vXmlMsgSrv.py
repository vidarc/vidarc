#----------------------------------------------------------------------------
# Name:         vXmlMsgSrv.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060703
# CVS-ID:       $Id: vXmlMsgSrv.py,v 1.2 2007/07/30 20:35:57 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vidarc.vSrv.vMsgSrv.vXmlNodeMsgSrvRoot import vXmlNodeMsgSrvRoot

import vidarc.vSrv.vMsgSrv.__register__ as __register__

class vXmlMsgSrv(vtXmlDomReg):
    VERBOSE=1
    TAGNAME_REFERENCE='tag'
    TAGNAME_ROOT='MsgSrvRoot'
    #APPL_REF=['vHum']
    def __init__(self,appl='vMsgSrv',attr='id',skip=[],synch=False,verbose=0,
                    audit_trail=True):
        self.dSrv={}
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        try:
            vtXmlDomReg.__init__(self,appl=appl,attr=attr,skip=skip,synch=synch,verbose=verbose,
                        audit_trail=audit_trail)
            oRoot=vXmlNodeMsgSrvRoot()
            self.RegisterNode(oRoot,False)
            
            __register__.RegisterNodes(self,oRoot,True)
        except:
            vtLog.vtLngTB(self.appl)
        self.SetDftLanguages()
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'MsgSrvs')
    #def New(self,revision='1.0',root='MsgSrvRoot',bConnection=False):
    #    vtXmlDomReg.New(self,revision,root)
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elem=self.createSubNode(self.getRoot(),'MsgSrvs')
        #self.createSubNodeTextAttr(elem,'cfg','','id','')
        if bConnection==False:
            self.CreateReq()
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        elem=self.createSubNode(self.getRoot(),'security_acl')
        self.__checkReqBase__()
        #self.AlignDoc()
    def GetPluginImages(self):
        l=[]
        return l
    def AddServerInstanceByNode(self,node,srv):
        id=self.getKey(node)
        self.AddServerInstance(id,srv)
    def AddServerInstance(self,id,srv):
        try:
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s;dSrv%s'%(id,vtLog.pformat(self.dSrv)),self.appl)
            iID=long(id)
            if iID in self.dSrv:
                vtLog.vtLngCurCls(vtLog.ERROR,'server for id:%08d already added'%(iID),self)
            self.dSrv[iID]=srv
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s;dSrv%s'%(id,vtLog.pformat(self.dSrv)),self.appl)
        except:
            vtLog.vtLngTB(self.doc.GetAppl())
    def HasServerInstanceByNode(self,node):
        id=self.getKey(node)
        return self.HasServerInstance(id)
    def HasServerInstance(self,id):
        try:
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s;dSrv%s'%(id,vtLog.pformat(self.dSrv)),self.appl)
            iID=long(id)
            return iID in self.dSrv
        except:
            vtLog.vtLngTB(self.doc.GetAppl())
        return False
    def GetServerInstanceByNode(self,node):
        id=self.getKey(node)
        return self.GetServerInstance(id)
    def GetServerInstance(self,id):
        try:
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s;dSrv%s'%(id,vtLog.pformat(self.dSrv)),self.appl)
            iID=long(id)
            if iID not in self.dSrv:
                return None
                #vtLog.vtLngCur(vtLog.DEBUG,'server for id:%08d not added'%(iID),self.appl)
            return self.dSrv[iID]
        except:
            vtLog.vtLngTB(self.doc.GetAppl())
        return None
    def DelServerInstanceByNode(self,node):
        id=self.getKey(node)
        self.DelServerInstance(id)
    def DelServerInstance(self,id):
        try:
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s;dSrv%s'%(id,vtLog.pformat(self.dSrv)),self.appl)
            iID=long(id)
            if iID in self.dSrv:
                del self.dSrv[iID]
        except:
            vtLog.vtLngTB(self.doc.GetAppl())
