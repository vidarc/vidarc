#----------------------------------------------------------------------------
# Name:         __register__.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20061015
# CVS-ID:       $Id: __register__.py,v 1.1 2007/01/07 17:51:26 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.vSrv.vPrjDocsSrv.vXmlNodePrjDocsSrv import vXmlNodePrjDocsSrv
from vidarc.vSrv.vPrjDocsSrv.vXmlNodePrjDocsSrvAlias import vXmlNodePrjDocsSrvAlias
from vidarc.vSrv.net.vXmlNodeHosts import vXmlNodeHosts
from vidarc.vSrv.net.vsNetXmlNodeAliasRef import vsNetXmlNodeAliasRef

def RegisterNodes(self,oRoot,bRegAsRoot=True):
    try:
        oPrjDocsSrv=vXmlNodePrjDocsSrv()
        oPrjDocsSrvAlias=vXmlNodePrjDocsSrvAlias()
        oHosts=vXmlNodeHosts()
        oRef=vsNetXmlNodeAliasRef()
        
        self.RegisterNode(oPrjDocsSrv,bRegAsRoot)
        self.RegisterNode(oPrjDocsSrvAlias,False)
        self.RegisterNode(oHosts,False)
        self.RegisterNode(oRef,False)
        self.LinkRegisteredNode(oRoot,oPrjDocsSrv)
        self.LinkRegisteredNode(oPrjDocsSrv,oPrjDocsSrvAlias)
        self.LinkRegisteredNode(oPrjDocsSrv,oRef)
        self.LinkRegisteredNode(oPrjDocsSrv,oHosts)
        self.LinkRegisteredNode(oPrjDocsSrvAlias,oHosts)
        self.LinkRegisteredNode(oPrjDocsSrvAlias,oRef)
        return oPrjDocsSrv
    except:
        vtLog.vtLngTB(__name__)
    return None
