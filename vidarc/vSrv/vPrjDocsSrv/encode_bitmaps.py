#!/usr/bin/env python
#----------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    "-u -i -n Application   img/vServerPrjDocs02_16.ico            images.py",
    "-a -u -n Plugin        img/vServerPrjDocs02_16.png            images.py",
    "-a -u -n PrjDocsAlias     img/VidMod_PrjDoc_16.png       images.py",
    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)

