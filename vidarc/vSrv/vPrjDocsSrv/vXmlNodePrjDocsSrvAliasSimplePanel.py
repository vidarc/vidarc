#Boa:FramePanel:vXmlNodePrjDocsSrvAliasSimplePanel
#----------------------------------------------------------------------------
# Name:         vXmlNodePrjDocsSrvAliasSimplePanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20061015
# CVS-ID:       $Id: vXmlNodePrjDocsSrvAliasSimplePanel.py,v 1.1 2007/01/07 17:51:26 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.time.vtTimeInputDateTime
import wx.lib.buttons
import wx.lib.filebrowsebutton
import vidarc.tool.input.vtInputText
import wx.lib.intctrl
import vidarc.tool.input.vtInputTextML

import sys

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.sec.vtSecAclDomDialog import *
from vidarc.vSrv.vXmlSrv.vConfigDialog import vConfigDialog

[wxID_VXMLNODEPRJDOCSSRVALIASSIMPLEPANEL, 
 wxID_VXMLNODEPRJDOCSSRVALIASSIMPLEPANELDBBDN, 
 wxID_VXMLNODEPRJDOCSSRVALIASSIMPLEPANELLBLNAME, 
 wxID_VXMLNODEPRJDOCSSRVALIASSIMPLEPANELLBLTAG, 
 wxID_VXMLNODEPRJDOCSSRVALIASSIMPLEPANELVINAME, 
 wxID_VXMLNODEPRJDOCSSRVALIASSIMPLEPANELVITAG, 
] = [wx.NewId() for _init_ctrls in range(6)]

class vXmlNodePrjDocsSrvAliasSimplePanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_bxsName_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblName, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viName, 2, border=4, flag=wx.EXPAND)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsTag, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsName, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 16), border=0, flag=0)
        parent.AddWindow(self.dbbDN, 0, border=0, flag=wx.EXPAND)

    def _init_coll_bxsTag_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblTag, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.viTag, 2, border=4, flag=wx.EXPAND)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=0, rows=4, vgap=0)

        self.bxsName = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsTag = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsName_Items(self.bxsName)
        self._init_coll_bxsTag_Items(self.bxsTag)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEPRJDOCSSRVALIASSIMPLEPANEL,
              name=u'vXmlNodePrjDocsSrvAliasSimplePanel', parent=prnt,
              pos=wx.Point(2, 2), size=wx.Size(312, 137),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 110))
        self.SetAutoLayout(True)

        self.lblName = wx.StaticText(id=wxID_VXMLNODEPRJDOCSSRVALIASSIMPLEPANELLBLNAME,
              label=_(u'name'), name=u'lblName', parent=self, pos=wx.Point(4,
              29), size=wx.Size(94, 30), style=wx.ALIGN_RIGHT)
        self.lblName.SetMinSize(wx.Size(-1, -1))

        self.lblTag = wx.StaticText(id=wxID_VXMLNODEPRJDOCSSRVALIASSIMPLEPANELLBLTAG,
              label=_(u'alias'), name=u'lblTag', parent=self, pos=wx.Point(4,
              4), size=wx.Size(94, 21), style=wx.ALIGN_RIGHT)
        self.lblTag.SetMinSize(wx.Size(-1, -1))

        self.viName = vidarc.tool.input.vtInputTextML.vtInputTextML(id=wxID_VXMLNODEPRJDOCSSRVALIASSIMPLEPANELVINAME,
              name=u'viName', parent=self, pos=wx.Point(102, 29),
              size=wx.Size(197, 30), style=0)

        self.viTag = vidarc.tool.input.vtInputText.vtInputText(id=wxID_VXMLNODEPRJDOCSSRVALIASSIMPLEPANELVITAG,
              name=u'viTag', parent=self, pos=wx.Point(102, 4),
              size=wx.Size(197, 21), style=0)

        self.dbbDN = wx.lib.filebrowsebutton.DirBrowseButton(buttonText='...',
              dialogTitle=_(u'Choose a directory'),
              id=wxID_VXMLNODEPRJDOCSSRVALIASSIMPLEPANELDBBDN,
              labelText=_(u'Select a directory:'), parent=self, pos=wx.Point(0,
              75), size=wx.Size(304, 29), startDirectory='.', style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vPrjDocsSrv')
        self._init_ctrls(parent, id)
        
        self.dlgSecAcl=None
        self.dlgConfig=None
        self.sAclSecName=None
        self.sConfigName=None
        self.netSrv=None
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.viTag.SetTagName('tag')
        self.viName.SetTagName('name')
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def Clear(self):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.Clear(self)
        
        # add code here
        self.viTag.Clear()
        self.viName.Clear()
        self.dbbDN.SetValue('')
        self.ClrBlockDelayed()
    def SetNetDocs(self,d):
        if d.has_key('vHum'):
            dd=d['vHum']
            self.docHum=dd['doc']
        # add code here
        
    def SetDoc(self,doc,bNet=False):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        
        # add code here
        self.viTag.SetDoc(doc)
        self.viName.SetDoc(doc)
    def SetNode(self,node):
        try:
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            
            # add code here
            self.viTag.SetNode(self.node)
            self.viName.SetNode(self.node)
            self.dbbDN.SetValue(self.objRegNode.GetDN(self.node))
        except:
            vtLog.vtLngTB(self.GetName())
        
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
                print node
            if node is None:
                return
            # add code here
            self.viTag.GetNode(node)
            self.viName.GetNode(node)
            self.objRegNode.SetDN(node,self.dbbDN.GetValue())
            self.GetNodeFin(node)
        except:
            vtLog.vtLngTB(self.GetName())
    def Close(self):
        # add code here
        self.viName.Close()
    def Cancel(self):
        # add code here
        pass
    def Lock(self,flag):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if flag:
            # add code here
            self.viTag.Enable(False)
            self.viName.Enable(False)
        else:
            # add code here
            self.viTag.Enable(True)
            self.viName.Enable(True)
    def SetNetSrv(self,netSrv):
        self.netSrv=netSrv
    def OnCbAclButton(self, event):
        event.Skip()
        try:
            if self.sAclSecName is not None:
                return
            if self.netSrv is None:
                return
            if self.dlgSecAcl is None:
                #self.dlgSecAcl=vSecAclDialog(self)
                self.dlgSecAcl=vtSecAclDomDialog(self)
                EVT_VTSEC_ACLDOM_PANEL_APPLIED(self.dlgSecAcl,self.OnSecAclApplied)
                EVT_VTSEC_ACLDOM_PANEL_CANCELED(self.dlgSecAcl,self.OnSecAclCanceled)
            sTag=self.objRegNode.GetTag(self.node)
            nodePar=self.doc.getParent(self.node)
            if nodePar is None:
                return
            o=self.doc.GetRegByNode(nodePar)
            sTagAppl=o.GetTag(nodePar)
            self.sAclSecName=':'.join([sTagAppl,sTag])
            srvXml=self.netSrv.GetDataInstance()
            bRet=srvXml.IsServedXml(self.sAclSecName)
            if bRet:
                doc=srvXml.GetServedXmlDom(self.sAclSecName)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'found:%s'%self.sAclSecName,self)
            else:
                sMsg=_(u'Alias has to be started!')
                dlg=wx.MessageDialog(self,sMsg ,
                            u'vXmlNodeMsgSrvAlias',
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg .Destroy()
                self.sAclSecName=None
                return

            self.dlgSecAcl.Centre()
            docHum=self.netMaster.GetNetDoc('vHum')
            #humDocs=[docHum]
            node=doc.getChildForced(doc.getRoot(),'security_acl')
            #print doc.getRoot()
            #print node
            #print doc.regNodes
            #print doc.GetPossibleAcl()
            self.dlgSecAcl.SetDoc(doc)
            lHum=srvXml.GetHumDoc4SecAclLst()
            #print lHum
            self.dlgSecAcl.SetDocHums(lHum,False)
            self.dlgSecAcl.SetNode(node)
            iRet=self.dlgSecAcl.Show(True)
            
        except:
            vtLog.vtLngTB(self.GetName())

    def OnSecAclApplied(self,event):
        event.Skip()
        if self.sAclSecName is not None:
            srvXml=self.netSrv.GetDataInstance()
            srvXml.ModifyServedXml(self.sAclSecName)
        self.sAclSecName=None
        
    def OnSecAclCanceled(self,event):
        event.Skip()
        self.sAclSecName=None

    def OnCbCfgButton(self, event):
        event.Skip()
        try:
            if self.netSrv is None:
                return
            if self.sConfigName is not None:
                return
            if self.dlgConfig is None:
                #self.dlgSecAcl=vSecAclDialog(self)
                self.dlgConfig=vConfigDialog(self)
            sTag=self.objRegNode.GetTag(self.node)
            nodePar=self.doc.getParent(self.node)
            if nodePar is None:
                return
            o=self.doc.GetRegByNode(nodePar)
            sTagAppl=o.GetTag(nodePar)
            self.sConfigName=':'.join([sTagAppl,sTag])
            srvXml=self.netSrv.GetDataInstance()
            bRet=srvXml.IsServedXml(self.sConfigName)
            if bRet:
                doc=srvXml.GetServedXmlDom(self.sConfigName)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'found:%s'%self.sConfigName,self)
            else:
                sMsg=_(u'Alias has to be started!')
                dlg=wx.MessageDialog(self,sMsg ,
                            u'vXmlNodeMsgSrvAlias',
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg .Destroy()
                self.sConfigName=None
                return

            self.dlgConfig.Centre()
            node=doc.getChildForced(doc.getRoot(),'config')
            #print doc.getRoot()
            #print node
            self.dlgConfig.SetNode(doc,node,self.sConfigName,['vHum'])
            iRet=self.dlgConfig.Show(True)
            if iRet>0:
                self.dlgConfig.GetNode(node)
        except:
            vtLog.vtLngTB(self.GetName())

    def OnCbSaveButton(self, event):
        event.Skip()

    def OnCbConnCloseButton(self, event):
        event.Skip()
