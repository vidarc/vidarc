#----------------------------------------------------------------------------
# Name:         vXmlNodePrjDocsSrv.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20061015
# CVS-ID:       $Id: vXmlNodePrjDocsSrv.py,v 1.2 2007/07/30 20:35:58 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vSrv.net.vsNetXmlNodeXmlSrv import vsNetXmlNodeXmlSrv
try:
    if vcCust.is2Import(__name__):
        from vidarc.vSrv.vXmlSrv.vXmlNodeXmlSrvPanel import vXmlNodeXmlSrvPanel
        from vidarc.vSrv.net.vsNetXmlNodeSrvEditDialog import vsNetXmlNodeSrvEditDialog
        from vidarc.vSrv.net.vsNetXmlNodeSrvAddDialog import vsNetXmlNodeSrvAddDialog
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodePrjDocsSrv(vsNetXmlNodeXmlSrv):
    def __init__(self,tagName='vPrjDocsSrv'):
        global _
        _=vtLgBase.assignPluginLang('vPrjDocsSrv')
        vsNetXmlNodeXmlSrv.__init__(self,tagName)
    def GetDescription(self):
        return _(u'XML Project Documents server')
    # ---------------------------------------------------------
    # specific
    def GetPortData(self,node):
        return self.GetVal(node,'portData',int,50008)
    def GetPortService(self,node):
        return self.GetVal(node,'portService',int,50009)
    def GetServerCls(self):
        from vidarc.tool.net.vtNetSrv import vtNetSrv
        return vtNetSrv
    def GetDataSrvCls(self):
        from vidarc.tool.net.vtNetXml import vtSrvXmlSock
        return vtSrvXmlSock
    def GetDataCls(self):
        from vidarc.tool.net.vtNetXml import vtNetXmlSock
        return vtNetXmlSock
    def GetServiceSrvCls(self):
        from vidarc.tool.net.vtNetSrv import vtSrvSock
        return vtSrvSock
    def GetServiceCls(self):
        from vidarc.tool.net.vtNetSrv import vtServiceSock
        return vtServiceSock
    def __procAliases__(self,node,srvFile,appl,bStart):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCurCls(vtLog.INFO,'appl:%s;bStart:%s;id:%s'%(appl,bStart,self.doc.getKey(node)),self)
        oReg=self.doc.GetRegByNode(node)
        if hasattr(oReg,'GetDoc2Serve'):
            if hasattr(oReg,'HasChilds2Serve'):
                self.doc.procChildsKeys(node,self.__procAliases__,
                            srvFile,oReg.GetTag(node),bStart)
                return 0
            alias=oReg.GetTag(node)
            sApplAlias=':'.join([appl,alias])
            if bStart:
                vtLog.PrintMsg(' '.join([self.GetDescription(),_('start'),sApplAlias]))
                doc=oReg.GetDoc2Serve(node)
                sDir=oReg.GetDN(node)
                lHosts=oReg.GetHosts(node)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurCls(vtLog.DEBUG,'dn:%s;doc:%s'%(sDir,doc),self)
                hosts=oReg.GetHosts(node)
                srvXml=None
                sApplAliasRef=None
                nodeAlias=self.doc.getChild(node,'AliasRef')
                if nodeAlias is None:
                    vtLog.vtLngCurCls(vtLog.ERROR,'alias reference not found',self)
                    return
                #print nodeAlias
                oReg=self.doc.GetRegByNode(nodeAlias)
                #print oReg
                if oReg is None:
                    vtLog.vtLngCurCls(vtLog.ERROR,'alias reference not registered',self)
                    return
                netRefSrv=oReg.GetRefServerInstanceByNode(nodeAlias)
                #print netRefSrv
                if netRefSrv is not None:
                    sApplAliasRef=oReg.GetApplAlias(nodeAlias)
                    if sApplAliasRef is not None:
                        srvXml=netRefSrv.GetDataInstance()
                        #docRef=srvXml.GetServedXmlDom(sApplAliasRef)
                srvFile.AddServedDN(sApplAlias,sDir,srvXml,sApplAliasRef)
                srvFile.SetServedHosts(sApplAlias,lHosts,None)
            else:
                vtLog.PrintMsg(' '.join([self.GetDescription(),_('stop'),sApplAlias]))
                srvFile.DelServedDN(sApplAlias)
        return 0
    def SetupServer(self,node):
        vtLog.vtLngCurCls(vtLog.INFO,'id:%s;'%(self.doc.getKey(node)),self)
        netSrv=self.doc.GetServerInstanceByNode(node)
        #vtLog.CallStack('')
        #print netSrv
        if netSrv is not None:
            nodeAlias=self.doc.getChild(node,'AliasRef')
            if nodeAlias is None:
                vtLog.vtLngCurCls(vtLog.ERROR,'alias reference not found',self)
                return
            #print nodeAlias
            oReg=self.doc.GetRegByNode(nodeAlias)
            #print oReg
            if oReg is None:
                vtLog.vtLngCurCls(vtLog.ERROR,'alias reference not registered',self)
                return
            netRefSrv=oReg.GetRefServerInstanceByNode(nodeAlias)
            #print netRefSrv
            if netRefSrv is not None:
                applAlias=oReg.GetApplAlias(nodeAlias)
                if applAlias is not None:
                    srvXml=netRefSrv.GetDataInstance()
                    docRef=srvXml.GetServedXmlDom(applAlias)
                    #print docRef
                    srvFile=netSrv.GetDataInstance()
                    srvFile.SetDocHum(docRef)
                else:
                    vtLog.vtLngCurCls(vtLog.ERROR,'alias:%s not served'%(applAlias),self)
            else:
                vtLog.vtLngCurCls(vtLog.ERROR,'referenced server not started or found',self)
    def Start(self,node,dataInst):
        appl=self.GetTag(node)
        vtLog.vtLngCurCls(vtLog.INFO,'id:%s;appl:%s'%(self.doc.getKey(node),appl),self)
        self.doc.procChildsKeys(node,self.__procAliases__,dataInst,appl,True)
    def Stop(self,node,dataInst):
        appl=self.GetTag(node)
        vtLog.vtLngCurCls(vtLog.INFO,'id:%s;appl:%s'%(self.doc.getKey(node),appl),self)
        self.doc.procChildsKeys(node,self.__procAliases__,dataInst,appl,False)
    # ---------------------------------------------------------
    # inheritance
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02PIDAT8\x8d}\x93\xcbk\x13Q\x14\xc6\x7fw\x1e\xcd\xa3i\x9b\xd6\x17\xd6\
\x85\xb4Z]\x08\xd2\x89d-\x08.\xeaB\x107nK,B\xff\x04\xdd\t\xeaJP\x04\x1f\xa5\
\xa1[iTp!\x14AA\x94\x82\x04gJq!\x16\xbaQhi\xb5N\x9bd\x123wr\\\xa4\xb1I\x8b~\
\x97\x03\x17.\xdf\xb9\xe7;\xe7|J\x19&\xed\x90\x9c\x16\xf0 \xeb@\xd1\x03@\xe5\
\xb3\x8a\x7f\xc0\xea f\x9bww\xc2\x01 St\x00\x8fO\x91\x16\x803\xa6\xb57\x912L\
\x98p\x85)iF\xdbqE\xc4\x15\x11\xc7C\xdc\xa9\x9c0\x8b(\xc3\xa4=\x0c\xa0Y.@z\
\xe7\x03\x17\x81\x85\xab\xe4\x16\x14yr\xe4\xb2y\x9c\x91\xbd\x12\x0cy\xac\xa5\
\xa5\x15_\xa0\x00\x14 \xe7(2\xa3yn~\x9b\xe5\xe3\xfc\x10\xf76\xe7\x9aR\x9fF\
\xd2\x91\x80bg\xc3\xf0\x05|\xc1\xbb\x0e\x8e\x03\xc9\xb8\xc9\x89c\x83\xd4\x83\
\x15\xee\xfe(0\x7fd\xae\xb3\x05\x8c\xba\xc2\xa4\x03Ep&\x15\xde\xe8\xf6K\x01\
\\?\xc7\xe6\xd11\x12\x12\xa0\xeb5\xba\x07R\x18q(\x95|\xaa5A\xd4\x00\x16Y\xe0\
\xa1\x873\x93\xe1yz\x19\x7f\xe1\x16\x14!}\xfe\x06+\xdf\x97\xb1*\x1b\x947JD:$\
\xd5\x93\xa2\xaf?I\xd2\xaa\xb1U]\xe7\xe5\xab\x0f\x18\x14\x81,x\xe3.\x97\xfda\
2KyZ\xe3l\xe82\xbd\xa9\x18]\xf1.b\xa9\x04\xb1\xee\x04\xb5J\x95(\xd4\xe8\x08\
\x86\x86Oc\xa8\xc5\xacjO\xd2\xc2\xda\xeaW\xfa{\rl[H\x1f\xe8a\xff\xa1~L\xcb@\
\xeb:\xa6%\x04\xd5\x90\xc3\x83\xc7\xb7\xc7\x08Pl\x92\x9d\x11\xc8,\xe5\xb1\
\xf8I\xcc\xac#\x8d\x80X\x1c,+\xa4\xfe\xdb\xc7\xb4#L[\x13T\x1b\x8c]\xb8\xa2\
\x0c\x00\xb5\xb8\xb3\xaa\xde\xf84\xef\x0fN\xb3/\xad\x11]\xa6\xd1\x08\x88t@\
\x18V\x10)\x13O\xd4\xa9\xd6\xb7\x10\xd5\x03\xb4\xad\xb2Z\xbc\xa6\x00\xa4\xf1\
DJ\xa5U\x82\xde8\x96\xd9\x85m+\x04\x90(D\x11a\xd8\x16\xfe\xaf\x12\xbd}\'\x9b\
\xbc\xddfj\xe1\xdd\xdb\x19\x89\xc2U\x921M"\x01\xb6\x01\xa6\xd9\xc0Nh>\x7f\
\xd9\xe2\xe2\xa5\xfb\xaa\xa3\x82\xdd8{n\xfc\xaf\xac7\xaf\x1f\x89\x925\x92\
\x89\n\xc1\xba\x8fe\x9fj\xdb\xa4]\xe6\xf8_\xbcx\xf6@\xee\xdc\x9e\xe80\xd4\
\x1f\xadr\x00\xd2\x94\x9d\x15B\x00\x00\x00\x00IEND\xaeB`\x82' 
    def GetEditDialogClass(self):
        if GUI:
            return vsNetXmlNodeSrvEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return vsNetXmlNodeSrvAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeXmlSrvPanel
        else:
            return None
