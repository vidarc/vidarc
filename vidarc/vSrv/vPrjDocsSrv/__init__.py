#----------------------------------------------------------------------------
# Name:         __init__.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20061015
# CVS-ID:       $Id: __init__.py,v 1.1 2007/01/07 17:51:26 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

PLUGABLE_SRV='vPrjDocsSrv'
PLUGABLE_SRV_ORDER=2

import sys
if hasattr(sys, 'importers'):
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=1
else:
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=0

