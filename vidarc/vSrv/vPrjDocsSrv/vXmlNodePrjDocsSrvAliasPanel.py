#Boa:FramePanel:vXmlNodePrjDocsSrvAliasPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodePrjDocsSrvAliasPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20061015
# CVS-ID:       $Id: vXmlNodePrjDocsSrvAliasPanel.py,v 1.1 2007/01/07 17:51:26 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.time.vtTimeInputDateTime
import wx.lib.buttons
import wx.lib.filebrowsebutton
import vidarc.tool.input.vtInputText
import wx.lib.intctrl
import vidarc.tool.input.vtInputTextML

import sys

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.sec.vtSecAclDomDialog import *
from vidarc.vSrv.net.vsNetXmlConfigDialog import vsNetXmlConfigDialog
import vidarc.vSrv.net.vsNetSrvControl as vsNetSrvControl

[wxID_VXMLNODEPRJDOCSSRVALIASPANEL, wxID_VXMLNODEPRJDOCSSRVALIASPANELCBACL, 
 wxID_VXMLNODEPRJDOCSSRVALIASPANELCBCFG, 
 wxID_VXMLNODEPRJDOCSSRVALIASPANELCBCONNCLOSE, 
 wxID_VXMLNODEPRJDOCSSRVALIASPANELCBSAVE, 
 wxID_VXMLNODEPRJDOCSSRVALIASPANELCBSERVE, 
 wxID_VXMLNODEPRJDOCSSRVALIASPANELDBBDN, 
 wxID_VXMLNODEPRJDOCSSRVALIASPANELLBLLAST, 
 wxID_VXMLNODEPRJDOCSSRVALIASPANELLBLLASTACCESS, 
 wxID_VXMLNODEPRJDOCSSRVALIASPANELLBLLASTSAVE, 
 wxID_VXMLNODEPRJDOCSSRVALIASPANELLBLLASTUSE, 
 wxID_VXMLNODEPRJDOCSSRVALIASPANELLBLNAME, 
 wxID_VXMLNODEPRJDOCSSRVALIASPANELLBLTAG, 
 wxID_VXMLNODEPRJDOCSSRVALIASPANELLSTCONN, 
 wxID_VXMLNODEPRJDOCSSRVALIASPANELVINAME, 
 wxID_VXMLNODEPRJDOCSSRVALIASPANELVITAG, 
 wxID_VXMLNODEPRJDOCSSRVALIASPANELVTLASTACCESS, 
 wxID_VXMLNODEPRJDOCSSRVALIASPANELVTLASTSAVE, 
 wxID_VXMLNODEPRJDOCSSRVALIASPANELVTLASTUSE, 
] = [wx.NewId() for _init_ctrls in range(19)]

class vXmlNodePrjDocsSrvAliasPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_bxsName_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblName, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viName, 2, border=4, flag=wx.EXPAND)

    def _init_coll_bxsLastSave_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblLastSave, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.vtLastSave, 2, border=4, flag=wx.EXPAND)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsTag, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsName, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 16), border=0, flag=0)
        parent.AddWindow(self.dbbDN, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsBt, 0, border=4, flag=wx.TOP)
        parent.AddWindow(self.lblLast, 0, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsLastAccess, 0, border=4,
              flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsLastUse, 0, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsLastSave, 0, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsConn, 0, border=4, flag=wx.EXPAND | wx.TOP)

    def _init_coll_bxsConnBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbConnClose, 0, border=0, flag=0)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbSave, 0, border=4, flag=wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.cbServe, 0, border=4, flag=wx.RIGHT | wx.LEFT)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(9)
        parent.AddGrowableCol(0)

    def _init_coll_bxsLastUse_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblLastUse, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.vtLastUse, 2, border=4, flag=wx.EXPAND)

    def _init_coll_bxsLastAccess_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblLastAccess, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.vtLastAccess, 2, border=4, flag=wx.EXPAND)

    def _init_coll_bxsTag_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblTag, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.viTag, 2, border=4, flag=wx.EXPAND)

    def _init_coll_bxsConn_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsConnBt, 0, border=4, flag=wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.lstConn, 1, border=0, flag=wx.EXPAND)

    def _init_coll_lstConn_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_RIGHT,
              heading=_(u'nr'), width=40)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'host'), width=100)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_RIGHT,
              heading=_(u'port'), width=60)
        parent.InsertColumn(col=3, format=wx.LIST_FORMAT_RIGHT,
              heading=_(u'usr'), width=60)
        parent.InsertColumn(col=4, format=wx.LIST_FORMAT_RIGHT,
              heading=_(u'sec lv'), width=40)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=0, rows=10, vgap=0)

        self.bxsName = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsTag = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsLastAccess = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsLastUse = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsLastSave = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsConn = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsConnBt = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsName_Items(self.bxsName)
        self._init_coll_bxsTag_Items(self.bxsTag)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsLastAccess_Items(self.bxsLastAccess)
        self._init_coll_bxsLastUse_Items(self.bxsLastUse)
        self._init_coll_bxsLastSave_Items(self.bxsLastSave)
        self._init_coll_bxsConn_Items(self.bxsConn)
        self._init_coll_bxsConnBt_Items(self.bxsConnBt)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEPRJDOCSSRVALIASPANEL,
              name=u'vXmlNodePrjDocsSrvAliasPanel', parent=prnt, pos=wx.Point(0,
              0), size=wx.Size(312, 322),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 295))
        self.SetAutoLayout(True)

        self.lblName = wx.StaticText(id=wxID_VXMLNODEPRJDOCSSRVALIASPANELLBLNAME,
              label=_(u'name'), name=u'lblName', parent=self, pos=wx.Point(4,
              29), size=wx.Size(94, 30), style=wx.ALIGN_RIGHT)
        self.lblName.SetMinSize(wx.Size(-1, -1))

        self.lblTag = wx.StaticText(id=wxID_VXMLNODEPRJDOCSSRVALIASPANELLBLTAG,
              label=_(u'alias'), name=u'lblTag', parent=self, pos=wx.Point(4,
              4), size=wx.Size(94, 21), style=wx.ALIGN_RIGHT)
        self.lblTag.SetMinSize(wx.Size(-1, -1))

        self.viName = vidarc.tool.input.vtInputTextML.vtInputTextML(id=wxID_VXMLNODEPRJDOCSSRVALIASPANELVINAME,
              name=u'viName', parent=self, pos=wx.Point(102, 29),
              size=wx.Size(197, 30), style=0)

        self.viTag = vidarc.tool.input.vtInputText.vtInputText(id=wxID_VXMLNODEPRJDOCSSRVALIASPANELVITAG,
              name=u'viTag', parent=self, pos=wx.Point(102, 4),
              size=wx.Size(197, 21), style=0)

        self.cbAcl = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODEPRJDOCSSRVALIASPANELCBACL,
              bitmap=vtArt.getBitmap(vtArt.Acl), label=_(u'ACL'), name=u'cbAcl',
              parent=self, pos=wx.Point(4, 108), size=wx.Size(76, 30), style=0)
        self.cbAcl.SetMinSize(wx.Size(-1, -1))
        self.cbAcl.Bind(wx.EVT_BUTTON, self.OnCbAclButton,
              id=wxID_VXMLNODEPRJDOCSSRVALIASPANELCBACL)

        self.cbCfg = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODEPRJDOCSSRVALIASPANELCBCFG,
              bitmap=vtArt.getBitmap(vtArt.Config), label=u'Config',
              name=u'cbCfg', parent=self, pos=wx.Point(4, 108), size=wx.Size(76,
              30), style=0)
        self.cbCfg.SetMinSize(wx.Size(-1, -1))
        self.cbCfg.Bind(wx.EVT_BUTTON, self.OnCbCfgButton,
              id=wxID_VXMLNODEPRJDOCSSRVALIASPANELCBCFG)

        self.cbSave = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODEPRJDOCSSRVALIASPANELCBSAVE,
              bitmap=vtArt.getBitmap(vtArt.Save), label=_(u'Save'),
              name=u'cbSave', parent=self, pos=wx.Point(4, 108),
              size=wx.Size(76, 30), style=0)
        self.cbSave.Bind(wx.EVT_BUTTON, self.OnCbSaveButton,
              id=wxID_VXMLNODEPRJDOCSSRVALIASPANELCBSAVE)

        self.lblLastAccess = wx.StaticText(id=wxID_VXMLNODEPRJDOCSSRVALIASPANELLBLLASTACCESS,
              label=_(u'access'), name=u'lblLastAccess', parent=self,
              pos=wx.Point(0, 159), size=wx.Size(97, 24), style=wx.ALIGN_RIGHT)
        self.lblLastAccess.SetMinSize(wx.Size(-1, -1))

        self.lblLast = wx.StaticText(id=wxID_VXMLNODEPRJDOCSSRVALIASPANELLBLLAST,
              label=_(u'last time infos'), name=u'lblLast', parent=self,
              pos=wx.Point(0, 142), size=wx.Size(304, 13),
              style=wx.ALIGN_CENTRE)
        self.lblLast.SetMinSize(wx.Size(-1, -1))

        self.lblLastUse = wx.StaticText(id=wxID_VXMLNODEPRJDOCSSRVALIASPANELLBLLASTUSE,
              label=_(u'use'), name=u'lblLastUse', parent=self, pos=wx.Point(0,
              187), size=wx.Size(97, 24), style=wx.ALIGN_RIGHT)
        self.lblLastUse.SetMinSize(wx.Size(-1, -1))

        self.lblLastSave = wx.StaticText(id=wxID_VXMLNODEPRJDOCSSRVALIASPANELLBLLASTSAVE,
              label=_(u'save'), name=u'lblLastSave', parent=self,
              pos=wx.Point(0, 215), size=wx.Size(97, 24), style=wx.ALIGN_RIGHT)
        self.lblLastSave.SetMinSize(wx.Size(-1, -1))

        self.vtLastAccess = vidarc.tool.time.vtTimeInputDateTime.vtTimeInputDateTime(id=wxID_VXMLNODEPRJDOCSSRVALIASPANELVTLASTACCESS,
              name=u'vtLastAccess', parent=self, pos=wx.Point(101, 159),
              size=wx.Size(202, 24), style=0)

        self.vtLastUse = vidarc.tool.time.vtTimeInputDateTime.vtTimeInputDateTime(id=wxID_VXMLNODEPRJDOCSSRVALIASPANELVTLASTUSE,
              name=u'vtLastUse', parent=self, pos=wx.Point(101, 187),
              size=wx.Size(202, 24), style=0)

        self.vtLastSave = vidarc.tool.time.vtTimeInputDateTime.vtTimeInputDateTime(id=wxID_VXMLNODEPRJDOCSSRVALIASPANELVTLASTSAVE,
              name=u'vtLastSave', parent=self, pos=wx.Point(101, 215),
              size=wx.Size(202, 24), style=0)

        self.lstConn = wx.ListCtrl(id=wxID_VXMLNODEPRJDOCSSRVALIASPANELLSTCONN,
              name=u'lstConn', parent=self, pos=wx.Point(39, 243),
              size=wx.Size(265, 52), style=wx.LC_REPORT)
        self._init_coll_lstConn_Columns(self.lstConn)

        self.cbConnClose = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODEPRJDOCSSRVALIASPANELCBCONNCLOSE,
              bitmap=vtArt.getBitmap(vtArt.Del), name=u'cbConnClose',
              parent=self, pos=wx.Point(4, 243), size=wx.Size(31, 30), style=0)
        self.cbConnClose.Bind(wx.EVT_BUTTON, self.OnCbConnCloseButton,
              id=wxID_VXMLNODEPRJDOCSSRVALIASPANELCBCONNCLOSE)

        self.cbServe = wx.lib.buttons.GenBitmapToggleButton(ID=wxID_VXMLNODEPRJDOCSSRVALIASPANELCBSERVE,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbServe', parent=self,
              pos=wx.Point(88, 108), size=wx.Size(31, 30), style=0)
        self.cbServe.Bind(wx.EVT_BUTTON, self.OnCbServeButton,
              id=wxID_VXMLNODEPRJDOCSSRVALIASPANELCBSERVE)

        self.dbbDN = wx.lib.filebrowsebutton.DirBrowseButton(buttonText='...',
              dialogTitle=_('Choose a directory'),
              id=wxID_VXMLNODEPRJDOCSSRVALIASPANELDBBDN,
              labelText=_('Select a directory:'), parent=self, pos=wx.Point(0,
              75), size=wx.Size(304, 29), startDirectory='.', style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vPrjDocsSrv')
        self._init_ctrls(parent, id)
        
        self.dlgSecAcl=None
        self.dlgConfig=None
        self.sAclSecName=None
        self.sConfigName=None
        self.netSrv=None
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.viTag.SetTagName('tag')
        self.viName.SetTagName('name')
        
        self.cbServe.SetBitmapLabel(vsNetSrvControl.getStopBitmap())
        self.cbServe.SetBitmapSelected(vsNetSrvControl.getStartBitmap())
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def Clear(self):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.Clear(self)
        
        # add code here
        self.viTag.Clear()
        self.viName.Clear()
        self.dbbDN.SetValue('')
        self.ClrBlockDelayed()
    def SetNetDocs(self,d):
        if d.has_key('vHum'):
            dd=d['vHum']
            self.docHum=dd['doc']
        # add code here
        
    def SetDoc(self,doc,bNet=False):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        
        # add code here
        self.viTag.SetDoc(doc)
        self.viName.SetDoc(doc)
    def SetNode(self,node):
        try:
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            
            # add code here
            self.viTag.SetNode(self.node)
            self.viName.SetNode(self.node)
            self.dbbDN.SetValue(self.objRegNode.GetDN(self.node))
            
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'node:%s,%s'%(self.doc.getTagName(self.node),self.doc.getKey(self.node)),self)
            o,nodeSrv,nodePar=self.objRegNode.GetServerObjNode(self.node)
            if o is None:
                return
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'oReg:%s;nodeSrv:%s,%s;nodePar:%s,%s'%(o,
                            self.doc.getTagName(nodeSrv),self.doc.getKey(nodeSrv),
                            self.doc.getTagName(nodePar),self.doc.getKey(nodePar)),self)
            netSrv=self.doc.GetServerInstanceByNode(nodeSrv)
            if netSrv is None:
                self.cbServe.SetValue(False)
                self.cbServe.Enable(False)
            else:
                self.cbServe.Enable(True)
                sTag=self.objRegNode.GetTag(self.node)
                sTagAppl=o.GetTag(nodePar)
                alias=':'.join([sTagAppl,sTag])
                srvFile=netSrv.GetDataInstance()
                bRet=srvFile.IsServedFile(alias)
                if bRet:
                    self.cbServe.SetValue(True)
                else:
                    self.cbServe.SetValue(False)
        except:
            vtLog.vtLngTB(self.GetName())
        
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
                print node
            if node is None:
                return
            # add code here
            self.viTag.GetNode(node)
            self.viName.GetNode(node)
            self.objRegNode.SetDN(node,self.dbbDN.GetValue())
            self.GetNodeFin(node)
        except:
            vtLog.vtLngTB(self.GetName())
    def Close(self):
        # add code here
        self.viName.Close()
    def Cancel(self):
        # add code here
        pass
    def Lock(self,flag):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if flag:
            # add code here
            self.viTag.Enable(False)
            self.viName.Enable(False)
        else:
            # add code here
            self.viTag.Enable(True)
            self.viName.Enable(True)
    def SetNetSrv(self,netSrv):
        self.netSrv=netSrv
    def OnCbAclButton(self, event):
        event.Skip()
        try:
            if self.sAclSecName is not None:
                vtLog.vtLngCurWX(vtLog.ERROR,'acl alias name already in use:%s'%(self.sAclSecName),self)
                return
            o,nodeSrv,nodePar=self.objRegNode.GetServerObjNode(self.node)
            if o is None:
                vtLog.vtLngCurWX(vtLog.ERROR,'server node not found;objReg:%s'%(self.doc.getTagName(self.node)),self)
                return
            netSrv=self.doc.GetServerInstanceByNode(nodeSrv)
            if netSrv is None:
                vtLog.vtLngCurWX(vtLog.ERROR,'server instance not found',self)
                return
            if self.dlgSecAcl is None:
                #self.dlgSecAcl=vSecAclDialog(self)
                self.dlgSecAcl=vtSecAclDomDialog(self)
                EVT_VTSEC_ACLDOM_PANEL_APPLIED(self.dlgSecAcl,self.OnSecAclApplied)
                EVT_VTSEC_ACLDOM_PANEL_CANCELED(self.dlgSecAcl,self.OnSecAclCanceled)
            sTag=self.objRegNode.GetTag(self.node)
            sTagAppl=o.GetTag(nodePar)
            self.sAclSecName=':'.join([sTagAppl,sTag])
            srvFile=netSrv.GetDataInstance()
            bRet=srvFile.IsServedFile(self.sAclSecName)
            if bRet:
                doc=srvFile.GetServedXmlDom(self.sAclSecName)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'found:%s'%self.sAclSecName,self)
            else:
                sMsg=_(u'Alias has to be started!')
                dlg=wx.MessageDialog(self,sMsg ,
                            u'vXmlNodeMsgSrvAlias',
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg .Destroy()
                self.sAclSecName=None
                return

            self.dlgSecAcl.Centre()
            docHum=self.netMaster.GetNetDoc('vHum')
            #humDocs=[docHum]
            node=doc.getChildForced(doc.getRoot(),'security_acl')
            self.dlgSecAcl.SetDoc(doc)
            lHum=srvFile.GetHumDoc4SecAclLst()
            self.dlgSecAcl.SetDocHums(lHum,False)
            self.dlgSecAcl.SetNode(node)
            iRet=self.dlgSecAcl.Show(True)
            
        except:
            vtLog.vtLngTB(self.GetName())

    def OnSecAclApplied(self,event):
        event.Skip()
        if self.sAclSecName is not None:
            o,nodeSrv,nodePar=self.objRegNode.GetServerObjNode(self.node)
            if o is None:
                return
            netSrv=self.doc.GetServerInstanceByNode(nodeSrv)
            srvFile=netSrv.GetDataInstance()
            
            #srvFile=self.netSrv.GetDataInstance()
            srvFile.ModifyServedXml(self.sAclSecName)
        self.sAclSecName=None
        
    def OnSecAclCanceled(self,event):
        event.Skip()
        self.sAclSecName=None

    def OnCbCfgButton(self, event):
        event.Skip()
        try:
            o,nodeSrv,nodePar=self.objRegNode.GetServerObjNode(self.node)
            if o is None:
                return
            netSrv=self.doc.GetServerInstanceByNode(nodeSrv)
            if netSrv is None:
                return
            #if self.sConfigName is not None:
            #    return
            if self.dlgConfig is None:
                #self.dlgSecAcl=vSecAclDialog(self)
                self.dlgConfig=vsNetXmlConfigDialog(self)
                self.dlgConfig.SetupImages(self.doc.GetPluginImages())
            sTag=self.objRegNode.GetTag(self.node)
            sTagAppl=o.GetTag(nodePar)
            self.sConfigName=':'.join([sTagAppl,sTag])
            srvFile=netSrv.GetDataInstance()
            bRet=srvFile.IsServedFile(self.sConfigName)
            if bRet:
                doc=srvFile.GetServedXmlDom(self.sConfigName)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'found:%s'%self.sConfigName,self)
            else:
                sMsg=_(u'Alias has to be started!')
                dlg=wx.MessageDialog(self,sMsg ,
                            u'vXmlNodeMsgSrvAlias',
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg .Destroy()
                self.sConfigName=None
                return
            self.dlgConfig.Centre()
            node=doc.getChildForced(doc.getRoot(),'config')
            self.dlgConfig.SetNode(doc,node,self.sConfigName,doc.APPL_REF)
            iRet=self.dlgConfig.ShowModal()
            if iRet>0:
                self.dlgConfig.GetNode()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnCbSaveButton(self, event):
        event.Skip()
        try:
            o,nodeSrv,nodePar=self.objRegNode.GetServerObjNode(self.node)
            if o is None:
                return
            netSrv=self.doc.GetServerInstanceByNode(nodeSrv)
            if netSrv is None:
                return
            sTag=self.objRegNode.GetTag(self.node)
            o=self.doc.GetRegByNode(nodePar)
            sTagAppl=o.GetTag(nodePar)
            sApplName=':'.join([sTagAppl,sTag])
            srvFile=netSrv.GetDataInstance()
            bRet=srvFile.IsServedFile(sApplName)
            if bRet:
                #srvFile.SaveXml(sApplName)
                pass
            else:
                sMsg=_(u'Alias has to be started!')
                dlg=wx.MessageDialog(self,sMsg ,
                            u'vXmlNodeMsgSrvAlias',
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg .Destroy()
                return        
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbConnCloseButton(self, event):
        event.Skip()

    def OnCbServeButton(self, event):
        event.Skip()
        try:
            if self.doc is None:
                return
            if self.node is None:
                return
            bServe=self.cbServe.GetValue()
            sTag=self.objRegNode.GetTag(self.node)
            vtLog.vtLngCurWX(vtLog.INFO,'tag:%s;val:%d'%(sTag,bServe),self)
            o,nodeSrv,nodePar=self.objRegNode.GetServerObjNode(self.node)
            if o is None:
                return
            netSrv=self.doc.GetServerInstanceByNode(nodeSrv)
            if netSrv is None:
                self.cbServe.SetValue(False)
                self.cbServe.Enable(False)
            else:
                self.cbServe.Enable(True)
                sTagAppl=o.GetTag(nodePar)
                alias=':'.join([sTagAppl,sTag])
                srvFile=netSrv.GetDataInstance()
                bRet=srvFile.IsServedFile(alias)
                if bRet:
                    srvFile.StopServed(alias)
                else:
                    srvFile.StartServed(alias)
        except:
            vtLog.vtLngTB(self.GetName())

