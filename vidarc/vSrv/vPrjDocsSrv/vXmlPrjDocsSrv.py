#----------------------------------------------------------------------------
# Name:         vXmlPrDocsSrv.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20061015
# CVS-ID:       $Id: vXmlPrjDocsSrv.py,v 1.2 2007/07/30 20:35:58 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vidarc.vSrv.vPrjDocsSrv.vXmlNodePrjDocsSrvRoot import vXmlNodePrjDocsSrvRoot

import vidarc.vSrv.vPrjDocsSrv.__register__ as __register__

class vPrjDocsSrv(vtXmlDomReg):
    VERBOSE=1
    TAGNAME_REFERENCE='tag'
    TAGNAME_ROOT='PrjDocsSrvRoot'
    def __init__(self,appl='vPrjDocsSrv',attr='id',skip=[],synch=False,verbose=0,
                    audit_trail=True):
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        try:
            vtXmlDomReg.__init__(self,appl=appl,attr=attr,skip=skip,synch=synch,verbose=verbose,
                            audit_trail=audit_trail)
            oRoot=vXmlNodePrjDocsSrvRoot()
            self.RegisterNode(oRoot,False)
            
            __register__.RegisterNodes(self,oRoot,True)
        except:
            vtLog.vtLngTB(self.appl)
        self.SetDftLanguages()
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'PrjDocsSrvs')
    #def New(self,revision='1.0',root='PrjDocsSrvRoot',bConnection=False):
    #    vtXmlDomReg.New(self,revision,root)
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elem=self.createSubNode(self.getRoot(),'PrjDocsSrvs')
        if bConnection==False:
            self.CreateReq()
        #self.createSubNodeTextAttr(elem,'cfg','','id','')
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        elem=self.createSubNode(self.getRoot(),'security_acl')
        self.__checkReqBase__()
        #self.AlignDoc()
