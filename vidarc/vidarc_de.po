# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2007-02-06 12:34+0100\n"
"PO-Revision-Date: 2005-09-20 14:49+0100\n"
"Last-Translator: Walter Obweger <walter.obweger@vidarc.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: German\n"
"X-Poedit-Country: GERMANY\n"
"X-Poedit-SourceCharset: utf-8\n"

#: __init__.py:633
msgid "Administration"
msgstr "0 Administration"

#: __init__.py:635
msgid "Analysis"
msgstr "8 Analyse"

#: __init__.py:649
msgid "Centralized Chronology"
msgstr "Zeiterfassung"

#: __init__.py:640
msgid "Contacts"
msgstr "Kontakte"

#: __init__.py:631
msgid "Core"
msgstr "9 Kern"

#: __init__.py:657
msgid "Customers"
msgstr "Kunden"

#: __init__.py:644
msgid "Document System"
msgstr "Dokumentensystem"

#: __init__.py:632
msgid "Documentation"
msgstr "7 Dokumentation"

#: __init__.py:642
msgid "Engineering"
msgstr "Engineering"

#: __init__.py:637
msgid "Finances"
msgstr "2 Finanzen"

#: __init__.py:645
msgid "Global Data"
msgstr "Globale Daten"

#: __init__.py:636
msgid "Implementation"
msgstr "5 Implementierung"

#: __init__.py:641
msgid "Locations"
msgstr "Örtlichkeiten"

#: __init__.py:643
msgid "Offers"
msgstr ""

#: __init__.py:639
msgid "Personnel"
msgstr "Personal"

#: __init__.py:634
msgid "Planning"
msgstr "1 Planung"

#: __init__.py:647
msgid "Project Documents"
msgstr "Projektdokumentation"

#: __init__.py:651
msgid "Project Planning"
msgstr "Projektplanung"

#: __init__.py:646
msgid "Projects"
msgstr "Projekte"

#: __init__.py:648
msgid "Projects Documents"
msgstr "Dokumentationen der Projekte"

#: __init__.py:650
msgid "Purchase"
msgstr "Einkauf"

#: __init__.py:652
msgid "Resources"
msgstr "Ressouren"

#: __init__.py:655
msgid "Software"
msgstr "Software"

#: __init__.py:658
msgid "Suppliers"
msgstr "Lieferanten"

#: __init__.py:653
msgid "Tasks"
msgstr "Tätigkeiten"
