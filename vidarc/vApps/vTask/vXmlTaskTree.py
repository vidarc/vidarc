#----------------------------------------------------------------------------
# Name:         vXmlCustomerTree.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060210
# CVS-ID:       $Id: vXmlTaskTree.py,v 1.6 2007/05/07 18:47:21 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlGrpTree import *
import images_task

class vXmlTaskTree(vtXmlGrpTree):
    def __init__(self, parent, id, pos, size, style, name,
                    master=False,controller=False,verbose=0):
        vtXmlGrpTree.__init__(self,id=id, name=name,master=master,
              controller=controller,verbose=verbose,
              parent=parent, pos=pos, size=size,style=style)
        self.bLangMenu=True
        #self.SetBuildOnDemand(False)
    def SetDftGrouping(self):
        self.grouping=[]
        self.bGrouping=False
        #self.label={None:[('name','')],'prjtasks':[('name@vPrj','')]}
        self.label={None:[('tag',''),('name','')],'prjtasks':self.__getPrjTaskLabel__}
    def SetDftNodeInfos(self):
        self.SetNodeInfos(['tag','name'])
        #self.SetNodeInfos({None:[('name','')],'prjtasks':[('name@vPrj','')]})
    def __getPrjTaskLabel__(self,parent=None,node=None,tagName='',infos=None):
        #try:
        #    print self.doc.getNodeInfoVal(node,'task|fid@vTask@name')
        #except:
        #    pass
        try:
            fid=self.doc.getForeignKey(node,'fid','vPrj')
            if len(fid)==0:
                return 'default'
            else:
                netMaster=self.doc.GetNetMaster()
                if netMaster is not None:
                    d=netMaster.GetNetDocNodeInfos('vPrj',fid,['name'])
                    return d['name']
        except:
            return '----'
        return 'project'
    def SetupImageList(self):
        if vtXmlGrpTree.SetupImageList(self):
            img=images_task.getTasksImage()
            imgSel=images_task.getTasksImage()
            self.__addElemImage2ImageList__('account_tasks',img,imgSel,False)
            self.__addElemImage2ImageList__('root',img,imgSel,False)
            
            img=images_task.getTaskImage()
            imgSel=images_task.getTaskSelImage()
            self.__addElemImage2ImageList__('task',img,imgSel,False)
            
            img=images_task.getSubTaskImage()
            imgSel=images_task.getSubTaskSelImage()
            self.__addElemImage2ImageList__('subtask',img,imgSel,False)
            
            img=images_task.getPrjImage()
            imgSel=images_task.getPrjSelImage()
            self.__addElemImage2ImageList__('prjtasks',img,imgSel,False)
    def getGroupingOverlayImages(self):
        return images_task.getGroupingImage(),images_task.getGroupingImage()
    def __addGroupingByInfos__(self,parent,o,tagName,infos):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;key:%s'%(tagName,self.doc.getKey(o)),self)
        #vtLog.CallStack('')
        #print tagName,infos
        tnparent=vtXmlGrpTree.__addGroupingByInfos__(self,parent,o,tagName,infos)
        try:
            if self.TREE_DATA_ID:
                parId=self.GetPyData(parent)
                parNode=self.doc.getNodeByIdNum(parId)
            else:
                parNode=self.GetPyData(parent)
                parId=self.doc.getKeyNum(parNode)
            strs=infos['tag'].split('.')
            try:
                grpItem=self.groupItems[parId]
            except:
                grpItem={}
                self.groupItems[parId]=grpItem
            #vtLog.CallStack('')
            #print tagName,strs
            for i in range(len(strs)-1):
                #ss='.'.join(strs[:i])
                #parIdExt=parId+'.'+ss
                #try:
                #    grpItem=self.groupItems[parIdExt]
                #except:
                #    tn=
                #    grpItem={tn,{}}
                #    self.groupItems[parIdExt]=grpItem
                val=strs[i]
                imgTuple=self.__getImgFormat__([tagName,''],strs[i])
                #print '   ',val
                #print grpItem
                #try:
                if val in grpItem:
                        act=grpItem[val]
                        grpItem=act[1]
                        tnparent=act[0]
                #except:
                else:
                        tid=wx.TreeItemData()
                        tid.SetData(None)
                        tn=self.AppendItem(tnparent,val,-1,-1,tid)
                        f=self.GetItemFont(tn)
                        f.SetWeight(wx.FONTWEIGHT_LIGHT)
                        f.SetStyle(wx.FONTSTYLE_SLANT)
                        self.SetItemFont(tn,f)
                        try:
                            self.SetItemImage(tn,imgTuple[0],wx.TreeItemIcon_Normal)
                            self.SetItemImage(tn,imgTuple[1],wx.TreeItemIcon_Selected)
                        except:
                            pass
                        act=(tn,{})
                        grpItem[val]=act
                        grpItem=act[1]
                        tnparent=tn
            
        except:
            vtLog.vtLngTB(self.GetName())
            pass
        return tnparent
