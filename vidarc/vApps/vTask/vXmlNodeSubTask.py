#----------------------------------------------------------------------------
# Name:         vXmlNodeSubTask.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060810
# CVS-ID:       $Id: vXmlNodeSubTask.py,v 1.2 2010/03/03 02:17:17 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeTag import *
try:
    if vcCust.is2Import(__name__):
        #from vXmlNodeTaskPanel import *
        #from vXmlNodeTaskEditDialog import *
        #from vXmlNodeTaskAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeSubTask(vtXmlNodeTag):
    def __init__(self,tagName='subtask'):
        global _
        _=vtLgBase.assignPluginLang('vTask')
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'subtask')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02\x1cIDAT8\x8d\xa5\x931h\x13a\x14\xc7\x7fw\xe9pc\x02\x1drP\x87@\x1d\
\x0e\\R\\\x1a\xe8`\x96\x92@\x87& \xf4\xa0Cq\xac\x1d$\x9b:\x95L%\xa3:ht\x91(\
\x08\xd7A\xb8\xeb\xd4,\x05;\x14\xaeC\xe1\xe2 ^\xa1\xe2'\xb6p\x81\x087\xd4\
\xe49\xa4\xd7$\xb6\xc5\xc17}\xef\xfb\xde\xff\xcf\xef=\xde\xa7iz\x8a\xff\x89\
\xa9\x9b\x1ed\xd0\x97\xe4\xac\xe9)-\xc95=\xa5\x8d\xd7\xe9\xd7\te\xd0\x17\xf7\
\xa3\x0b@c\xab\x81\x0c\xfaR[\xb1(\xdf\xcbO\x18O\x18\\\n=\x97(\x8a\xc8d2\x00\
\xa8SE\xf3e\x93\xc33\x93\xc6\x8b\xd6\x15B=I\\\xcf%\xee\xc5\x14\x17\x8aD\xdd\
\x08\xa5\x14\x00\xd6m\x8b\xfd\x83}j\x1b5\xacY\x8b\xe6\x9b&2\xe8\x8b\xb3}ar1D\
\x89{\xb1\xc4\xbdX\xa2(\x12\xff\xd0\x17@*\xcb\x15\xb1Wmq\xb6\x1d\x89{\xb1(\
\xa5d}\xa3(\xae\x87\xe4f\x11MO\x8dZh\xef\xb5\x01\x08\x8fC\xe6\xf2s<Y\xb50\
\x0c\x83\xear\x95\xf2\xe7\x0e\xeaLQ\xdf\xacPY\xfa\xc4\xebW\x16\xe1\x97\xd1\
\x84'(\x82N \xce\x07G\x00\xb1Wm\x89<W\xe4n^\xd67\xd6%\xe88b\xaf\xe4\xc5?\x18\
\x12N\x10$\x14f\xd6$\x9dN\x03\xd0\r\xdb\x84\xa6I\xf8\xde\xa1\x00<}\xdc\xa2\
\xb0\xf0\x00u\xaa\xd8m\xef\x8ef\xf0/\x8a\xaf\xefZ\x12\xdf\xb1\xc4?\xf4\xc5?\
\xf0%8\n\xa4\xb24/\xb9,re\x0f.)\xa6G\x14?n\xe5\xe8\xbcma\x18\x06\xe6\x8cI}\
\xf3!\x85\xe9\x100\x98 \xb8\x91b\xc5\x96\xe0(\x10u\xa2\xc4^.Jc-+\xb9\xac!\
\x9a\x9e\x1a\xadriqQ\x00\xbc\x1d\x8f\xf6^\x9b\xc2|\x01u2\xdc\x85\xcc\xef}\
\xea[u\xf8\x152\x97\x0ey\xbe\xd3\xe5\xf8\xe7\xb9\x06\xa0iz\xeaR\x9c\x84\xb7\
\xe3\x11\xf7b\xc2o!\x9d\xa3\x0e\xd5\xfbU\x1akY\x98b(\xfe~>\xfa\x0f\xe3\xf8\
\xe5RY\xca\xa5\xb2\x00\xe2z\xaeDQ$\x80\x00\xf2\xec\x91%\xb9\x99!\xf6D\xcb\
\x7f_$F\x89p|6\xd7\xd5\xfe\x01O\x1aG\x95#\x145\xf5\x00\x00\x00\x00IEND\xaeB`\
\x82" 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            d=vtXmlNodeTag.GetEditDialogClass(self).copy()
            d['pnCls']=vtXmlNodeTag.GetPanelClass(self)
            return d
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            d=vtXmlNodeTag.GetAddDialogClass(self).copy()
            d['pnCls']=vtXmlNodeTag.GetPanelClass(self)
            return d
        else:
            return None
