#Boa:FramePanel:vTaskMainPanel
#----------------------------------------------------------------------------
# Name:         vTaskPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060810
# CVS-ID:       $Id: vTaskMainPanel.py,v 1.5 2007/07/30 20:38:49 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.xml.vtXmlNodeRegSelector
import time

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.vApps.common.vSystem as vSystem

from vidarc.tool.net.vtNetSecXmlGuiMaster import *

from vidarc.tool.xml.vtXmlNodeRegListBook import *
from vidarc.vApps.vTask.vNetTask import *
from vidarc.vApps.vTask.vXmlTaskTree import *

from vidarc.vApps.vPrj.vNetPrj import *

from vidarc.tool.xml.vtXmlDom import vtXmlDom

import vidarc.vApps.vTask.images_task as imgTask

def create(parent):
    return vTaskMainPanel(parent)

[wxID_VTASKMAINPANEL, wxID_VTASKMAINPANELPNDATA, wxID_VTASKMAINPANELSLWNAV, 
 wxID_VTASKMAINPANELVIREGSEL, 
] = [wx.NewId() for _init_ctrls in range(4)]

def getPluginImage():
    return imgTask.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(imgTask.getPluginBitmap())
    return icon

class vTaskMainPanel(wx.Panel):
    VERBOSE=0
    def _init_coll_bxsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viRegSel, 1, border=4, flag=wx.ALL | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.bxsData = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_bxsData_Items(self.bxsData)

        self.pnData.SetSizer(self.bxsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VTASKMAINPANEL, name=u'vTaskMainPanel',
              parent=prnt, pos=wx.Point(325, 29), size=wx.Size(650, 400),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(642, 373))
        self.Bind(wx.EVT_SIZE, self.OnSize)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VTASKMAINPANELSLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 0), size=wx.Size(200,
              376), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetAutoLayout(True)
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetLabel(_(u'navigation'))
        self.slwNav.SetToolTipString(_(u'navigation'))
        self.slwNav.SetDefaultSize(wx.Size(200, 100))
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VTASKMAINPANELSLWNAV)

        self.pnData = wx.Panel(id=wxID_VTASKMAINPANELPNDATA, name=u'pnData',
              parent=self, pos=wx.Point(208, 8), size=wx.Size(424, 352),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)
        self.pnData.SetAutoLayout(True)

        self.viRegSel = vidarc.tool.xml.vtXmlNodeRegSelector.vtXmlNodeRegSelector(id=wxID_VTASKMAINPANELVIREGSEL,
              name=u'viRegSel', parent=self.pnData, pos=wx.Point(4, 4),
              size=wx.Size(412, 340), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vTask')
        self._init_ctrls(parent)
        self.SetName(name)
        self.bActivated=False
        self.dCfg={}
        #self.cfgFunc=None
        
        self.xdCfg=vtXmlDom(appl='vTaskCfg',audit_trail=False)
        
        appls=['vTask','vPrj']
        
        rect = parent.sbStatus.GetFieldRect(0)
        self.netMaster=vtNetSecXmlGuiMaster(parent.sbStatus,'Master',pos=(rect.x+2, rect.y+2),
                    size=(rect.width-4, rect.height-4),verbose=0)
        self.netTask=self.netMaster.AddNetControl(vNetTask,'vTask',synch=True,verbose=0,
                                            audit_trail=True)
        self.netPrj=self.netMaster.AddNetControl(vNetPrj,'vPrj',synch=True,verbose=0)
        self.netMaster.SetLang(lang=vtLgBase.getPluginLang())
        self.netMaster.AddNetControl(None,None,verbose=0)
        
        self.netMaster.SetCfg(self.xdCfg,[],appls,'vTaskMaster')
        
        self.vgpTree=vXmlTaskTree(self.slwNav,wx.NewId(),
                pos=(0,0),size=(198,529),style=0,name="trTask",
                master=True,verbose=0)
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        self.vgpTree.SetDoc(self.netTask,True)

        pn=self.viRegSel.CreateRegisteredPanel('prjtasks',self.netTask,True)
        
        #pn=self.viRegSel.CreateRegisteredPanel('task',self.netTask,True)
        self.nbTask=vtXmlNodeRegListBook(self.viRegSel,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="nbTask")
        oTask=self.netTask.GetRegisteredNode('task')
        self.nbTask.SetDoc(self.netTask,True)
        self.viRegSel.AddWidgetDependent(self.nbTask,oTask)
        self.nbTask.Show(False)
        
        #pn=self.viRegSel.CreateRegisteredPanel('subtask',self.netTask,True)
        self.nbSubTask=vtXmlNodeRegListBook(self.viRegSel,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="nbSubTask")
        oSubTask=self.netTask.GetRegisteredNode('subtask')
        self.nbSubTask.SetDoc(self.netTask,True)
        self.viRegSel.AddWidgetDependent(self.nbSubTask,oSubTask)
        self.nbSubTask.Show(False)
        
        self.viRegSel.SetDoc(self.netTask,True)
        #self.viRegSel.SetRegNode(oWrkPkg)
        self.viRegSel.SetRegNode(oTask,bIncludeBase=True)
        self.viRegSel.SetRegNode(oSubTask,bIncludeBase=True)
        self.viRegSel.SetNetDocs(self.netTask.GetNetDocs())

        self.Move(pos)
        self.SetSize(size)
    def GetDocMain(self):
        return self.netTask
    def GetNetMaster(self):
        return self.netMaster
    def GetTreeMain(self):
        return self.vgpTree
    def OnTreeItemSel(self,event):
        event.Skip()
        node=event.GetTreeNodeData()
        self.viRegSel.SetNode(node)
    def OpenFile(self,fn):
        if self.netTask.ClearAutoFN()>0:
            self.netTask.Open(fn)
    def OpenCfgFile(self,fn=None):
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                self.netMaster.SetCfgNode()
                self.__setCfg__()
                return
        self.xdCfg.New(root='config')
        if fn is not None:
            self.xdCfg.SetFN(fn)
            self.netMaster.SetCfgNode()
        self.__setCfg__()
    def __setCfg__(self):
        try:
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSlwNavSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iWidth=event.GetDragRect().width
            #if iWidth>80:
            #    iWidth=80
            self.dCfg['nav_sash']=str(iWidth)
            
            self.slwNav.SetDefaultSize((iWidth, 1000))
            
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            self.GetCfgData()
            #event.Skip()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSlwTopSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iHeight=event.GetDragRect().height
            #if iHeight>80:
            #    iHeight=80
            self.dCfg['nav_top']=str(iHeight)
            
            self.slwTop.SetDefaultSize((1000, iHeight))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            self.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnSize(self, event):
        event.Skip()
        try:
            bMod=False
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            
            self.pnData.Refresh()
            self.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())

    def SetCfgData(self,func,l,d):
        self.cfgFunc=func
        self.cfgOrigin=l
        self.dCfg=d
        try:
            i=int(self.dCfg['nav_sash'])
            self.slwNav.SetDefaultSize((i, 1000))
        except:
            pass
        try:
            i=int(self.dCfg['nav_top'])
            self.slwTop.SetDefaultSize((1000, i))
        except:
            pass
        try:
            pos=map(int,self.dCfg['pos'].split(','))
            self.Move(pos)
        except:
            pass
        try:
            sz=map(int,self.dCfg['size'].split(','))
            self.SetSize(sz)
        except:
            pass
    def GetCfgData(self):
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,self.dCfg)
    def GetAboutData(self):
        import __config__
        version=u' %d.%d.%d.%d'%(__config__.VER_MAJOR,__config__.VER_MINOR,
                    __config__.VER_RELEASE,__config__.VER_SUBREL)
        desc=_(u"""Task module.

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
""")
        return _(u'VIDARC Task'),desc,version
