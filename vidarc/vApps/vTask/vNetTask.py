#----------------------------------------------------------------------------
# Name:         vNetTask.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vNetTask.py,v 1.5 2007/07/30 20:38:49 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog

from vidarc.vApps.vTask.vXmlTask import vXmlTask
from vidarc.tool.net.vNetXmlWxGui import *

class vNetTask(vXmlTask,vNetXmlWxGui):
    def __init__(self,parent,appl,fn='tmpl.xml',attr='id',skip=[],
                pos=(0,0),size=(16,12),synch=False,verbose=0,audit_trail=False):
        vNetXmlWxGui.__init__(self,parent,appl,pos,size,synch=synch,verbose=verbose)
        vXmlTask.__init__(self,attr=attr,skip=skip,synch=synch,verbose=verbose,
                        audit_trail=audit_trail)
        vXmlTask.SetFN(self,fn)
    #def Stop(self):
    #    vNetXmlWxGui.Stop(self)
    #    vXmlTask.Stop(self)
    #def IsRunning(self):
    #    if vNetXmlWxGui.IsRunning(self):
    #        return True
    #    if vXmlTask.IsRunning(self):
    #        return True
    #    return False
    def New(self,root=None,bAcquire=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        vNetXmlWxGui.New(self)
        vXmlTask.New(self,root=root,
                bConnection=self.IsFlag(self.CONN_ACTIVE),
                bAcquire=bAcquire)
    def GetFN(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'GetFN',origin=self.appl)
        sFN=vNetXmlWxGui.GetFN(self)
        if sFN is not None:
            return sFN
        else:
            return vXmlTask.GetFN(self)
    def Open(self,fn=None,silent=False,bThread=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'Open',origin=self.appl)
        if fn is None:
            sFN=vNetXmlWxGui.GetFN(self)
            if sFN is not None:
                fn=sFN
        if fn is None:
            vXmlTask.New(self)
            return -2
        if bThread:
            self.silent=silent
            iRet=vXmlTask.Open(self,fn,True)
            #if iRet>=0:
            #    if silent==False:
            #        self.NotifyContent()
            #else:
            #    self.New()
        else:
            iRet=vXmlTask.Open(self,fn)
            if iRet>=0:
                if silent==False:
                    self.NotifyContent()
            else:
                self.New()
        return iRet
    def Save(self,fn=None,encoding='ISO-8859-1'):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'Save',origin=self.appl)
        sFN=vNetXmlWxGui.GetFN(self)
        if sFN is not None:
            fn=sFN
        vNetXmlWxGui.SaveApplDocs(self,encoding=encoding)
        return vXmlTask.Save(self,fn,encoding)
    def getSetupChildByLst(self,node,lst):
        if lst is None:
            lst=self.lstSetupNode
        return self.docSetup.getChildByLst(node,lst)
    def getChildByLst(self,node,lst):
        if lst is None:
            return None
        return vXmlTask.getChildByLst(self,node,lst)
    def addNode(self,par,node,func=None,**kwargs):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'addNode;id:%s'%self.getKey(node),origin=self.appl)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCallStack(None,vtLog.DEBUG,'%s'%node,origin=self.appl)
        vXmlTask.addNode(self,par,node)
        vNetXmlWxGui.addNode(self,par,node,func,**kwargs)
    def moveNode(self,nodePar,node):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'idPar:%s;id:%s'%(self.getKey(nodePar),self.getKey(node)),self.appl)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCallStack(None,vtLog.DEBUG,'%s'%node,origin=self.appl)
        vNetXmlWxGui.moveNode(self,nodePar,node)
        vXmlTask.moveNode(self,nodePar,node)
    def delNode(self,node):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%s'%self.getKey(node),self.appl)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'%s'%`node`,self.appl)
        vNetXmlWxGui.delNode(self,node)
        vXmlTask.deleteNode(self,node)
    def IsRunning(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'IsRunning',origin=self.appl)
        if vNetXmlWxGui.__isRunning__(self):
            return True
        if vXmlTask.__isRunning__(self):
            return True
        return False
    def IsBusy(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'IsBusy',origin=self.appl)
        return self.IsRunning()
    def __stop__(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'__stop__',origin=self.appl)
        vNetXmlWxGui.__stop__(self)
        vXmlTask.__stop__(self)
    def NotifyContent(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'NotifyContent',origin=self.appl)
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyContent(self)
    def NotifyGetNode(self,id):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'NotifyGetNode',origin=self.appl)
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyGetNode(self,id)
    def NotifyAddNode(self,idPar,id,content):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'NotifyAddNode',origin=self.appl)
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyAddNode(self,idPar,id,content)
    def NotifyRemoveNode(self,id):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'NotifyRemoveNode',origin=self.appl)
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyRemoveNode(self,id)
    def GenerateSortedIds(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'GenerateSortedIds',origin=self.appl)
        self.GenerateTaskIds()
        self.GenerateSortedTaskIds()
        self.GenerateSortedSubTaskIds()
    def GenerateSortedTaskIds(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'GenerateSortedTaskIds',origin=self.appl)
        self.tasks=self.getSortedNodeIds(['prjtasks'],
                        'task','fid',[('tag','%s')])
    def GenerateSortedSubTaskIds(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'GenerateSortedSubTaskIds',origin=self.appl)
        self.subTasks=self.getSortedNodeIds(['prjtasks'],
                        'task','fid',[('tag','%s')])
        for id in self.subTasks.GetIds():
            tup=self.subTasks.GetInfoById(id)
            self.subTasks.AddNode(tup[0],'subtask','fid',[])
    def __procSubTasks__(self,node,*args):
        tup=args[0][0]
        tup[1][self.getAttribute(node,'fid')]=node
        return 0
    def __procTasks__(self,node,*args):
        tup=args[0][0]
        tupNew=[node,{}]
        tup[1][self.getAttribute(node,'fid')]=tupNew
        self.procChildsAttr(node,'fid',self.__procSubTasks__,tupNew)
        return 0
    def __procPrjTasks__(self,node,*args):
        tup=[node,{}]
        self.taskId[self.getAttribute(node,'fid')]=tup
        self.procChildsAttr(node,'fid',self.__procTasks__,tup)
        return 0
    def GenerateTaskIds(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCallStack(None,vtLog.INFO,'GenerateTaskIds',origin=self.appl)
        self.taskId={}
        self.procChildsAttr(self.getRoot(),'fid',self.__procPrjTasks__)
    def GetTaskIds(self):
        try:
            return self.taskId
        except:
            self.GenerateTaskIds()
            return self.taskId
    def GetSortedTaskIds(self):
        try:
            return self.tasks
        except:
            self.GenerateSortedTaskIds()
            return self.tasks
    def GetSortedSubTaskIds(self):
        try:
            return self.subTasks
        except:
            self.GenerateSortedSubTaskIds()
            return self.subTasks
