#----------------------------------------------------------------------------
# Name:         vXmlTask.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vXmlTask.py,v 1.6 2008/02/02 16:10:55 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog

from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg

VERBOSE=0

def getPluginData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01UIDAT8\x8d\xa5\x931k\x021\x1c\xc5_r\xfd"=p\x11\xfa%\nE\xbcoPW\xb5_\
\xc0\xad\xda\xa5H\'u\x92.n\x05\xd7.E\x8b\x83\x1d\xed \xdc\r\x05;\x1cdqH\x87\
\x0e\x1do\xe8\xdds\xd0\xa4w\x9e\x1e\x14\xff\x90!$\xbf\x97\xbc\xf7O\x84\x90\
\x0eN)Y\xb4\xc8$&\x93\x98\xff\x160\xa0\n\x15\xb4\xd6\xd8\x17I\xcfe\x11x\xfe\
\xf2\x0c\x00P\xa1\xb2\x10\x93\x98\xad\xa6\xf7\'\xb2\xcb\x80\x00\xa8BE\xad59\
\xe8Q\x01\x9cL\'\x1c>\x0e\xa9\xb5\xa6\xd9\xd3jz\x04@!\x9d-k\x04T\xa8\xa8\x00\
\x0bs\xd0\xe3\xe2}a\xe1\xee};\x07[\x01#\xc2A\x8f\xbc\xba\xa4\x02\xa8\xd7\x9a\
z]\x0c\x0b\xe9@\x986\x1a\xef\xe9rK.\xdaw-D_\x9f\xe8\x8f\xa6X}\xacP\xbe(CHG\
\xe4B\x14\xd2\x11n\xc9\xcd\xc0\xb5\xeb\x1a~\xbe#\xf4GS\xa8P!\x08\x82|\xcb2~v\
Y\xcc\xdf\xe664\x00\xf4\x97>\xc7O\xe3\x83\x162\x13\xaf\xea\xd9n\x98\xe1/}v\
\x1f\xba\x87\xfdK\x07g\x00P\xadTr\xaf-\xfa\x8d\x10E\x11\xfc\xc0G\xe7\xb6\x93\
\xf1\x9d.\t\x00\xaf\xb3\x99]\xf4\xaa\x1e\xdc\x92\xbb\x85\x97>\x1a7\x8d\xa3p.\
\x83\xb4\x8dz\xb3~\xf4\xda\x07\xdb\xb8_Lb\x16\x9el.p\xeaw\xde\x00\xa6\xdc\
\xed\xf2\x8fgO\x8a\x00\x00\x00\x00IEND\xaeB`\x82'

from vidarc.vApps.vTask.vXmlNodeTaskRoot import vXmlNodeTaskRoot
from vidarc.vApps.vTask.vXmlNodePrjTask import vXmlNodePrjTask
from vidarc.vApps.vTask.vXmlNodeTask import vXmlNodeTask
from vidarc.vApps.vTask.vXmlNodeTaskAttr import vXmlNodeTaskAttr
from vidarc.vApps.vTask.vXmlNodeSubTask import vXmlNodeSubTask
from vidarc.vApps.vTask.vXmlNodeSubTaskAttr import vXmlNodeSubTaskAttr

from vidarc.tool.xml.vtXmlNodeLog import vtXmlNodeLog
from vidarc.tool.xml.vtXmlNodeCfg import vtXmlNodeCfg
from vidarc.tool.xml.vtXmlNodeAttrCfgML import vtXmlNodeAttrCfgML

class vXmlTask(vtXmlDomReg):
    TAGNAME_REFERENCE='name'
    TAGNAME_ROOT='account_tasks'
    APPL_REF=['vPrj']
    def __init__(self,attr='id',skip=[],synch=False,appl='vTask',verbose=0,
                    audit_trail=True):
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        vtXmlDomReg.__init__(self,attr=attr,skip=skip,synch=synch,
                        appl=appl,verbose=verbose,audit_trail=audit_trail)
        try:
            oRoot=vXmlNodeTaskRoot(tagName='root')
            self.RegisterNode(oRoot,False)
            oRoot=vXmlNodeTaskRoot()
            oPrjTask=vXmlNodePrjTask()
            oTask=vXmlNodeTask()
            oTaskAttr=vXmlNodeTaskAttr()
            oSubTask=vXmlNodeSubTask()
            oSubTaskAttr=vXmlNodeSubTaskAttr()
            oLog=vtXmlNodeLog()
            oCfg=vtXmlNodeCfg()
            self.RegisterNode(oRoot,False)
            self.RegisterNode(oPrjTask,True)
            self.RegisterNode(oTask,False)
            self.RegisterNode(oTaskAttr,False)
            self.RegisterNode(oSubTask,False)
            self.RegisterNode(oSubTaskAttr,False)
            self.RegisterNode(oLog,False)
            self.RegisterNode(oCfg,True)
            self.RegisterNode(vtXmlNodeAttrCfgML(),False)
            oTask.AddNotContainedChild(oSubTask.GetTagName())
            
            self.LinkRegisteredNode(oRoot,oPrjTask)
            self.LinkRegisteredNode(oPrjTask,oTask)
            self.LinkRegisteredNode(oTask,oTaskAttr)
            self.LinkRegisteredNode(oTask,oLog)
            self.LinkRegisteredNode(oTask,oSubTask)
            self.LinkRegisteredNode(oSubTask,oSubTaskAttr)
            self.LinkRegisteredNode(oSubTask,oLog)
        except:
            vtLog.vtLngTB(self.appl)
        self.SetDftLanguages()
    def __genIds__(self,c,node,ids):
        sTag=self.getTagName(c)
        if sTag in self.skip:
            return 0
        id=self.getAttribute(c,self.attr)
        if self.IsKeyValid(id):
            ids.CheckId(c,self,id)
            self.updateParentDict(node,c)
        elif sTag in ['prjtasks','task','subtask']:
            ids.CheckId(c,self,id)
            self.updateParentDict(node,c)
        #self.__genIds__(c,ids)
        self.iElemIDCount+=1
        self.procChildsKeys(c,self.__genIds__,c,ids)
        return 0
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'tasks')
    #def New(self,revision='1.0',root='account_tasks',bConnection=False):
    #    vtXmlDomReg.New(self,revision,root)
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elemHum=self.createSubNode(self.getRoot(),'tasks')
        if bConnection==False:
            self.CreateReq()
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        #self.AlignDoc()
    def __matchElemGetTaskNode__(self,node,*args,**kwargs):
        #print args
        #print kwargs
        fid=args[0]
        try:
            if long(self.getAttribute(node,'id'))==fid:
                return 1
        except:
            pass
        return 0
    def GetPrjTaskNode(self,node,fid):
        try:
            fid=long(fid)
            bFound,hier,node=self.findChildsKeys(node,[],self.__matchElemGetTaskNode__,fid)
            return node
        except:
            return None
    def GetPossibleAclOld(self):
        return {'all':self.ACL_MSK_NORMAL},['all']
    
    
