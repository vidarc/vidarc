#Boa:MDIChild:vTaskMDIFrame
#----------------------------------------------------------------------------
# Name:         vTaskMDIFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      2006
# CVS-ID:       $Id: vTaskMDIFrame.py,v 1.4 2007/07/30 20:38:49 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

MDI_CHILD_FRAME=True

from vidarc.vApps.common.vMDIFrame import vMDIFrame
import vidarc.vApps.vTask.images_task as imgTask
import vidarc.tool.log.vtLog as vtLog

from vidarc.vApps.vTask.vTaskMainPanel import *

def getPluginImage():
    return imgTask.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(imgTask.getApplicationBitmap())
    return icon

def create(parent, id, pos=wx.DefaultPosition, size=wx.DefaultSize,style=0, name='vHuman'):
    return vTaskMDIFrame(parent)


DOMAINS=['core','vTask']
def getDomainTransLst(lang):
    import vidarc.__init__ as vInit
    return vInit.getDomainTrans(DOMAINS,lang)
def getDomainImageData():
    import vidarc.__init__ as vInit
    return vInit.getDomainImageData(DOMAINS)

[wxID_VTASKMDIFRAME] = [wx.NewId() for _init_ctrls in range(1)]

class vTaskMDIFrame(wx.MDIChildFrame,vMDIFrame):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.MDIChildFrame.__init__(self, id=-1,
              name=u'vTaskMDIFrame', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(400, 250), style=wx.DEFAULT_FRAME_STYLE,
              title=u'vTask')
        self.SetClientSize(wx.Size(392, 223))

    def __init__(self, parent, id, pos, size,style,name):
        self._init_ctrls(parent)
        self.SetName(name)
        icon = getApplicationIcon()
        self.SetIcon(icon)
        
        vMDIFrame.__init__(self,u'Task')
        self.dCfg.update({'x':10,'y':10,'width':800,'height':300})
        
        try:
            self.pn=vTaskMainPanel(self,-1,wx.DefaultPosition,wx.DefaultSize,wx.TAB_TRAVERSAL,'vTaskPlugIn')
            self.pn.OpenCfgFile('vTaskCfg.xml')
            
            self.netMaster=self.pn.GetNetMaster()
            self.netDocMain=self.pn.GetDocMain()
            self.trMain=self.pn.GetTreeMain()
            EVT_NET_XML_OPEN_START(self.netDocMain,self.OnOpenStart)
            EVT_NET_XML_OPEN_OK(self.netDocMain,self.OnOpenOk)
            EVT_NET_XML_OPEN_FAULT(self.netDocMain,self.OnOpenFault)
            
            self.BindNetDocEvents(None,None)
            self.netMaster.BindEvents(
                    funcStart=self.OnMasterStart,
                    funcFinish=self.OnMasterFinish,
                    funcShutDown=self.OnMasterShutDown)
            
            EVT_NET_XML_GOT_CONTENT(self.netDocMain,self.OnGotContent)
            
            EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.trMain,self.OnAddElementsFin)
            EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.trMain,self.OnAddElementsProgress)
        
            self.AddMenus(self , self.trMain , self.netMaster,
                self.mnFile , self.mnView , self.mnTools)
        
        except:
            vtLog.vtLngTB(self.GetName())
        #self.Move(pos)
        #self.SetSize(size)
        #self.Layout()
        #self.Refresh()
    def __getPluginImage__(self):
        return imgTask.getPluginImage()
    def __getPluginBitmap__(self):
        return imgTask.getPluginBitmap()
