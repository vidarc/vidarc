#----------------------------------------------------------------------------
# Name:         vXmlNodePrjTask.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060810
# CVS-ID:       $Id: vXmlNodePrjTask.py,v 1.2 2010/03/03 02:17:17 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        from vXmlNodePrjTaskPanel import vXmlNodePrjTaskPanel
        #from vXmlNodePrjTaskEditDialog import *
        #from vXmlNodePrjTaskAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodePrjTask(vtXmlNodeBase):
    def __init__(self,tagName='prjtasks'):
        global _
        _=vtLgBase.assignPluginLang('vTask')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'project task')
    # ---------------------------------------------------------
    # specific
    def GetPrjID(self,node):
        return self.GetForeignKey(node,'fid','vPrj')
    def SetPrjID(self,node,val):
        self.SetForeignKeyStr(node,'fid',val,'vPrj')
    # ---------------------------------------------------------
    # inheritance
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xd3IDAT8\x8d}\x92\xcbk\x13Q\x18\xc5\x7f3\xed*\x05\x11\x8cJ\x9bi@\
\xe8\xac\x94.\x84\x0c(\xd8\\\xe8B\x8c\xe0\x03|\xadJ]\x97R\xdc\xd4U\xc9\xcc\
\x9f\xe0\x03Z\x08\x85V\\\xf8\xd8\'d\x11\xbd\xa1(\x92\x16DT\x04\'"\xa5D$YX\
\xf0\x81\xb50\x9f\x8bI\xc6\xd8Iz\xe0\xc0\xe5~\xf7\xfc\xee\xe1r1\xcc\x01z\x19\
\xb4(\xa5\x04\xb4\xf4;c\x98\x03\xf4\rk\x8d\x88 Z\xb3/h\xdfp\xb7\xb5F\xd4q\
\x15\x83\xc4\x00\xae\xabDD\xc5\x00\xa2\x11\xf7*1\x80\xc9^\xbd\xd7P\x05Z.\xa0\
\xc2\xbd*\xd0\x8c\x9d\x04`\xb0\xb3\x90\xa0"\x8e\xa3\x01\x17\x9a:\xf4\x11\x15\
B\x9a\xbaw\x1a\xc2\x06\x12Td\xf5\x01\xac\xac\xe6\xff\x9fv@]\xca\x0e\xdf\x10\
\t*\x12\x01:\xe1L&\xdb\xf7\x96H\xdb)\x9e_8\x88\xbex\x87\xb1\xe0\x8c\x00\x98\
\xf3\xf3\x1a\x95\xfd\x17n\x1du\xf1\x9e\xc6\xb3^!E~\xec\x12\x1c:\xc6\xceF\x8d\
\xd3\x84\xcd\xcc\xcd7\xb0Y\x83!\x13\x86\x1203\x93\xe7\xad\xdc\xc2\xb86\x11\
\x81\xbc\x87\'\xc8\x8f\xdf\x04\xeb$/\x1e\xad\xf0\xa7\xd1\xe0W\x1b<\x08\xd0\
\xda\n}x\x14^~X\xe6\xcb\xd7W\xc00\xb558\xbf\x06\x85I\x1b\xac$\x9f\xd7\x9f\
\xd1\xa8\xbf#\xb1\xf7\x11\xa3\xfam\x10\xc0h\xfa\x07\x00\x85\xc9\xcb\x8c\x1c\
\x18\x81\x8f>\xf8>\x00\t\xe0gw\x83^\xb2\xd2\xdfqv\x9c0\xdc\xa5\x04\x84\xf5m\
\x9b{\xb3sb>.\xc3\x93\xb2\x17\x03\xec\xfe\xde\xa5\x96L\xe2m\xac\xc7f\x8b\xb6\
Mnv\x0e\xbf\x9e\xc2\x08\xff?H\xb0 \xd7\xcf\x82\x95\xb6\xa8\xbe^bzj\x1a\x80b\
\xc9\xa1X*\xe2f\x8aL\x01\xb7\xb7\xbf\x91m\x87\xef\xde\xbfbD\x80\x8e$X\x90\
\xdc\xb9\x1c\x00~=E\xfdS\xda\xe85+\x95O\x19\x00\x7f\x01V\xe6\xd7\xed\xe5\x9e\
R\x08\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            #return vXmlNodePrjTaskEditDialog
            return {'name':self.__class__.__name__,
                    #'sz':(360, 140),
                    'pnName':'pnPrjTask',
                }
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            #return vXmlNodePrjTaskAddDialog
            return {'name':self.__class__.__name__,
                    #'sz':(360, 140),
                    'pnName':'pnPrjTask',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodePrjTaskPanel
        else:
            return None
