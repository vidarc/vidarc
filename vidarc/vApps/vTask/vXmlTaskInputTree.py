#----------------------------------------------------------------------
# Name:         vXmlTaskInputTree.py
# Purpose:      text control with tree popup window
#
# Author:       Walter Obweger
#
# Created:      200610210
# CVS-ID:       $Id: vXmlTaskInputTree.py,v 1.9 2012/12/28 14:30:33 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from vidarc.tool.input.vtInputTree import vtInputTree
from vidarc.tool.input.vtInputTree import vtInputTreeTransientPopup
from vidarc.vApps.vTask.vXmlTaskTree import vXmlTaskTree
import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.input.vtInputTree import vtInputTreeChanged

import images

if __name__ == "__main__":
    import vidarc.tool.lang.vtLgBase as vtLgBase
    from vidarc.tool.xml.vtXmlDom import *
    from vidarc.tool.input.vtInputTree import EVT_VTINPUT_TREE_CHANGED
    #import images_lang
    import sys
    
VERBOSE=0

class vXmlTaskInputTreeTransientPopup(vtInputTreeTransientPopup):
    def __init__(self, parent, size,style,name=''):
        vtInputTreeTransientPopup.__init__(self, parent, size,style,name='')
    def SetNodeTreeS(self,node):
        vtInputTreeTransientPopup.SetNodeTree(self,node)
    def SetNodeTree(self,node):
        if VERBOSE:
            vtLog.CallStack(node)
        #bSet=False
        if node is not None:
            self.trInfo.SetNode(node)
            #bSet=True
        return
        nodeDft=None
        par=self.GetParent()
        doc=par.__getDocTree__()
        if doc is not None:
            cc=doc.getChildsNoAttr(doc.getBaseNode(),'fid')
            for c in cc:
                if doc.getTagName(c)=='prjtasks':
                    nodeDft=c
                    break
            if nodeDft is not None:
                if bSet:
                    #self.trInfo.AddElement(None,nodeDft)
                    pass
                else:
                    self.trInfo.SetNode(nodeDft)
    def SetPrjNodeTree(self,node):
        if VERBOSE:
            vtLog.CallStack(node)
        #bSet=False
        self.trInfo.ClearDelayed()
        if node is not None:
            #self.trInfo.Clear()
            self.trInfo.SetNode(node)
            #bSet=True
        return
        nodeDft=None
        par=self.GetParent()
        doc=par.__getDocTree__()
        if doc is not None:
            cc=doc.getChildsNoAttr(doc.getBaseNode(),'fid')
            for c in cc:
                if doc.getTagName(c)=='prjtasks':
                    nodeDft=c
                    break
            if nodeDft is not None:
                if bSet:
                    #self.trInfo.AddElement(-1,nodeDft)
                    pass
                else:
                    self.trInfo.SetNode(nodeDft)

class vXmlTaskInputTree(vtInputTree):
    def __init__(self,*_args,**_kwargs):
        apply(vtInputTree.__init__,(self,) + _args,_kwargs)
        self.tagName='task'
        self.tagNameInt='tag'
        self.prjID=-1
        self.nodePrj=None
        self.trClass=self.__createTree__
        self.trShow=self.__showNode__
    def Clear(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'Clear',
        #                origin=self.GetName())
        if VERBOSE:
            vtLog.CallStack('')
        self.ClearLang()
        self.node=None
        self.fid=''
        self.nodeSel=None
        self.__markModified__(False)
    def ClearTree(self):
        vtInputTree.ClearTree(self)
        self.idDftNode=-1
    def SetDocTree(self,doc,bNet=False):
        vtInputTree.SetDocTree(self,doc,bNet=bNet)
        try:
            if self.docTreeTup is not None:
                doc,bNet=self.docTreeTup
                doc.DoSafe(self.__findPrjNode__,doc,-1)
        except:
            self.__logTB__()
    def __cmpPrjID__(self,n,doc,prjID,l):
        #print doc.getTagName(n),doc.getKey(n)
        if doc.getTagName(n)=='prjtasks':
        #try:
            fid,appl=doc.getForeignKeyNumAppl(n,'fid','vPrj')
            #print '   ',fid,appl
            if fid==prjID:
                l.append(n)
                return -1
        return 0
    def __cmpDft__(self,n,doc,prjID,l):
        #print doc.getTagName(n),doc.getKey(n)
        if doc.getTagName(n)=='prjtasks':
        #try:
            fid,appl=doc.getForeignKeyNumAppl(n,'fid','vPrj')
            #print '   ',fid,appl
            if fid<0:
                l.append(n)
                return -1
        return 0
    def __findPrjNode__(self,doc,prjID):
        try:
            l=[None]
            #print prjID
            if prjID>=0:
                doc.procChildsKeys(doc.getBaseNode(),self.__cmpPrjID__,doc,prjID,l)
            else:
                doc.procChildsKeys(doc.getBaseNode(),self.__cmpDft__,doc,prjID,l)
            #print l
            return l[-1]
        except:
            self.__logTB__()
        return None
    def __createTree__(*args,**kwargs):
        tr=vXmlTaskTree(**kwargs)
        tr.SetNodeInfos([args[0].tagNameInt,'name','|id'])
        tr.SetGrouping([],{None:[(args[0].tagNameInt,''),('name','')],'prjtasks':args[0].__getPrjTaskLabel__})
        tr.EnableLanguageMenu()
        return tr
    def __showNode__(self,doc,node,lang):
        l=[]
        if node is None:
            return ''
        tmp=node
        
        #sName=doc.getNodeTextLang(tmp,'name',None)
        while doc.getTagName(tmp)!='tasks':
            l.append(doc.getNodeText(tmp,self.tagNameInt))
            tmp=doc.getParent(tmp)
        l.reverse()
        #l[-1]=' '.join([l[-1],sName])
        return '.'.join(l)
    def __setNode__(self,node):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'__setNode__;node:%s'%(self.node),
        #                origin=self.GetName())
        try:
            docTree=self.docTreeTup[0]
            if self.nodeSel is None:
                sVal=self.txtVal.GetValue()
            else:
                sVal=self.__showNode__(docTree,self.nodeSel,self.doc.GetLang())
                #sVal=string.split(docTree.getNodeText(self.nodeSel,self.tagNameInt),'\n')[0]
            self.doc.setNodeText(node,self.tagName,sVal)
            tmp=self.doc.getChild(node,self.tagName)
            if tmp is not None:
                if len(self.fid)==0:
                    self.doc.removeAttribute(tmp,'fid')
                else:
                    self.doc.setForeignKey(tmp,'fid',self.fid,self.appl)
        except:
            #vtLog.vtLngTB(self.GetName())
            pass
    def __getSelNode__(self):
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCS(vtLog.DEBUG,'__getSelNode__',
        #                origin=self.GetName())
        if VERBOSE:
            vtLog.CallStack('')
        try:
            if self.doc is None:
                return
            if self.docTreeTup is not None:
                doc=self.docTreeTup[0]
            else:
                doc=self.doc
            doc.DoSafe(doc.DoCallBackWX,self.__getSelNode__Safe)
            #doc.DoCallBackWX(self.__getSelNode__Safe)
        except:
            self.__logTB__()
    def __getSelNode__Safe(self):
        try:
            self.fid=self.doc.getForeignKey(self.doc.getChild(self.node,self.tagName),'fid',self.appl)
            if VERBOSE:
                print 'fid',self.fid,'prjID',self.prjID
                print 'nodePrj',self.doc.getKey(self.nodePrj)
            if self.docTreeTup is not None:
                docTree=self.docTreeTup[0]
                if self.nodePrj is None:
                    self.nodeSel=None
                    self.__markModified__(True)
                else:
                    self.nodeSel=docTree.GetPrjTaskNode(self.nodePrj,self.fid)
                if VERBOSE:
                    print 'nodeSel',self.doc.getKey(self.nodeSel)
            if self.nodeSel is not None:
                sVal=self.txtVal.GetValue()
                self.bBlock=True
                self.__apply__(self.nodeSel)
                if sVal!=self.txtVal.GetValue():
                    self.__markModified__(True)
                    wx.PostEvent(self,vtInputTreeChanged(self,self.nodeSel,self.fid,self.txtVal.GetValue()))
            else:
                if self.node is not None:
                    vtLog.vtLngCurWX(vtLog.WARN,'%s;%s'%(self.fid,self.node),self)
                self.__markModified__(True)
        except:
            self.__logTB__()
    def __getPrjTaskLabel__(self,parent=None,node=None,tagName='',infos=None):
        #try:
        #    print self.doc.getNodeInfoVal(node,'task|fid@vTask@name')
        #except:
        #    pass
        try:
            fid=self.doc.getForeignKey(node,'fid','vPrj')
            if len(fid)==0:
                return 'default'
            else:
                if self.netMaster is not None:
                    d=self.netMaster.GetNetDocNodeInfos('vPrj',fid,['name'])
                    return d['name']
        except:
            return '----'
        return 'project'
        
    def __createPopup__(self):
        bDbg=self.GetVerboseDbg(0)
        if bDbg:
            self.__logDebug__('__createPopup__')
        if self.popWin is None:
            sz=self.GetSize()
            sz=(sz[0],sz[1]-20)
            try:
                self.popWin=vXmlTaskInputTreeTransientPopup(self,sz,wx.SIMPLE_BORDER)
                self.popWin.SetDocTree(self.docTreeTup[0],self.docTreeTup[1])
                self.popWin.SetPrjNodeTree(self.nodePrj)
                #self.popWin.SetNodeTree(self.nodeTree)
                if self.trSetNode is not None:
                    if VERBOSE:
                        vtLog.CallStack('')
                        self.trSetNode(self.doc,self.node)
                    self.popWin.SetNode(self.trSetNode(self.doc,self.node))
            except:
                self.__logTB__()
            #self.popWin.SetVal(self.txtVal.GetValue(),self.doc.GetLang())
        else:
            pass
    def SelectPrj(self,prjID):
        try:
            bDbg=self.GetVerboseDbg(0)
            bDbg=True
            if bDbg:
                self.__logDebug__(prjID)
            #self.fid=''
            #self.nodeSel=None
            self.GetDftNode()
            if len(prjID)==0:
                self.Clear()
                #self.prjID=''
                #return
                self.prjID='-1'
                if bDbg:
                    self.__logDebug__({'prjID':self.prjID})
            else:
                #prjID,sAppl=self.doc.convForeign(prjID)#long(prjID)
                try:
                    prjID=long(prjID)
                except:
                    prjID=-1
                if bDbg:
                    self.__logDebug__({'prjID':prjID})
            if bDbg:
                self.__logDebug__({'new prjID':prjID,'old prjID':prjID})
            if self.prjID==prjID:
                return
            self.prjID=prjID
            if self.docTreeTup is None:
                return
            doc=self.docTreeTup[0]
            if doc is None:
                return
            doc.DoSafe(self.__SelectPrj__,doc,self.prjID)
            return
            cc=doc.getChilds(doc.getBaseNode())
            for c in cc:
                if doc.getTagName(c)=='prjtasks':
                    #try:
                        fid,appl=doc.getForeignKeyNumAppl(c,'fid','vPrj')
                        if fid==self.prjID:
                        #if long(doc.getAttribute(c,'fid'))==self.prjID:
                            self.nodePrj=c
                            if self.popWin is not None:
                                self.popWin.SetPrjNodeTree(self.nodePrj)
                            break
                    #except:
                    #    pass
        except:
            self.__logTB__()
    def __SelectPrj__(self,doc,prjID):
        bDbg=self.GetVerboseDbg(0)
        bDbg=True
        if bDbg:
            self.__logDebug__({'prjID':prjID})
        n=None
        if prjID<0:
            if self.idDftNode>=0:
                n=doc.getNodeByIdNum(self.idDftNode)
        else:
            n=self.__findPrjNode__(doc,prjID)
            if n is None:
                if bDbg:
                    self.__logDebug__({'prjID':prjID,'n':'is None','idDftNode':self.idDftNode})
                if self.idDftNode>=0:
                    n=doc.getNodeByIdNum(self.idDftNode)
            else:
                if bDbg:
                    self.__logDebug__({'prjID':prjID,'n':'found','idDftNode':self.idDftNode})
        if bDbg:
            self.__logDebug__({'nodePrj':doc.getKeyNum(self.nodePrj),'n':doc.getKeyNum(n)})
        if n is not None:
            self.nodePrj=n
        else:
            self.nodePrj=None
        if VERBOSE:
            vtLog.CallStack('')
            print 'nodePrj is None:',self.nodePrj is None
        if self.popWin is not None:
                self.popWin.SetPrjNodeTree(self.nodePrj)
        else:
            if bDbg:
                self.__logDebug__('popWin is None')
    def __getDftNode__(self,doc):
        self.__logDebug__('start')
        n=self.__findPrjNode__(doc,-1)
        if n is not None:
            self.idDftNode=doc.getKeyNum(n)
            self.__logDebug__({'idDftNode':self.idDftNode})
        else:
            self.idDftNode=-1
            self.__logDebug__({'idDftNode':self.idDftNode})
    def GetDftNode(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            bDbg=True
            if bDbg:
                self.__logDebug__({'idDftNode':self.idDftNode})
            
            if self.docTreeTup is None:
                self.idDftNode=-1
                return
            doc=self.docTreeTup[0]
            if self.idDftNode>=0:
                n=doc.getNodeByIdNum(self.idDftNode)
                if n is None:
                    self.idDftNode=-1
            if self.idDftNode<0:
                if doc is None:
                    return
                doc.DoSafe(self.__getDftNode__,doc)
        except:
            self.__logTB__()

if __name__ == "__main__":
    class TestFrame(wx.Frame):
        def set(self,doc,node):
            #return doc.getNodeTextLang(node,'name',None)
            return doc.getAttribute(node,'fid')
        def get(self,doc,node,docTree,nodeSel,val=None):
            #return doc.getNodeTextLang(node,'name',None)
            try:
                doc.setAttribute(node,'fid',docTree.getKey(nodeSel))
            except:
                pass
            if val is not None:
                doc.setText(node,val)
        def getID(self,docTree,nodeSel):
            try:
                return docTree.getKey(nodeSel)
            except:
                return ''
        def show(self,doc,node,lang):
            return doc.getNodeTextLang(node,'name',lang)
        def __init__(self, parent, ID, title, pos=wx.DefaultPosition,
                     size=wx.DefaultSize, style=wx.DEFAULT_FRAME_STYLE):
            wx.Frame.__init__(self, parent, ID, title, pos, size, style)
            self.panel=wx.Panel(self,-1)
            self.inTxtML1=vXmlTaskInputTree(self.panel,id=-1,pos=(4,4),name='inTree1')
                    #tree_class=createXmlTree,
                    #set_node=self.set,
                    #get_node=self.get,
                    #get_id=self.getID,
                    #show_node_func=self.show)
            EVT_VTINPUT_TREE_CHANGED(self.inTxtML1,self.OnTxtMl1Changed)
            #self.inTxtML1.SetTreeFunc(createXmlTree,self.show)
            self.inTxtML2=vXmlTaskInputTree(self.panel,id=-1,pos=(4,140),size=(150,30),name='inTree2')
                    #tree_class=createXmlTree,
                    #show_node_func=self.show)
            #self.inTxtML2.SetTreeFunc(createXmlTree,self.show)
            self.inTxtML2.SetTagNames('type','name')
            self.cbSave = wx.Button(id=-1,
                  label='save', name=u'cbSave',
                  parent=self.panel, pos=wx.Point(150, 4), size=wx.Size(60, 30),
                  style=0)
            self.cbSave.Bind(wx.EVT_BUTTON, self.OnCbSaveButton,
                  self.cbSave)
            self.cbApply = wx.Button(id=-1,
                  label='apply', name=u'cbSave',
                  parent=self.panel, pos=wx.Point(170, 40), size=wx.Size(60, 30),
                  style=0)
            self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
                  self.cbApply)
            self.cbApply2 = wx.Button(id=-1,
                  label='apply2', name=u'cbSave',
                  parent=self.panel, pos=wx.Point(170, 80), size=wx.Size(60, 30),
                  style=0)
            self.cbApply2.Bind(wx.EVT_BUTTON, self.OnCbApply2Button,
                  self.cbApply2)
            doc=vtXmlDom()
            #doc.SetLanguages([
            #    ('english','en',images_lang.getLangEnBitmap()),
            #    ('german','de',images_lang.getLangDeBitmap()),
            #    ])
            #doc.SetLang('en')
            doc.Open('test/testTask.xml')
            doc1=vtXmlDom()
            #doc1.SetLanguages([
            #    ('english','en',images_lang.getLangEnBitmap()),
            #    ('german','de',images_lang.getLangDeBitmap()),
            #    ])
            #doc1.SetLang('en')
            doc1.Open('test/testTask1.xml')
            self.SetDoc(doc,doc1)
            
            docInfo=vtXmlDom()
            #docInfo.SetLanguages([
            #    ('english','en',images_lang.getLangEnBitmap()),
            #    ('german','de',images_lang.getLangDeBitmap()),
            #    ])
            #docInfo.SetLang('en')
            docInfo.Open('test/testTasksGrp.xml')
            self.inTxtML1.SetDocTree(docInfo)
            nodeTmp=docInfo.getChild(docInfo.getRoot(),'prjtasks')
            self.inTxtML1.SetNodeTree(nodeTmp)
            
            self.inTxtML2.SetDocTree(docInfo)
            self.inTxtML2.SetNodeTree(nodeTmp)
            
            self.zTime=20
            self.timer = wx.PyTimer(self.Notify)
            self.timer.Start(1000)
            self.Notify()
        def OnTxtMl1Changed(self,evt):
            vtLog.CallStack('')
            print evt.GetNode()
            print evt.GetId()
            print evt.GetValue()
        def OnCbSaveButton(self,evt):
            self.doc.Save('test/testTask1.xml')
        def OnCbApplyButton(self,evt):
            self.inTxtML1.GetNode()
        def OnCbApply2Button(self,evt):
            self.inTxtML2.GetNode()
        def Notify(self):
            if self.zTime>=0:
                self.zTime-=1
                print 'notify:%3d'%self.zTime
                if self.zTime==0:
                    self.doc.Open('test/testTask2.xml')
                    node=self.doc.getChildByLst(self.doc.getRoot(),['root','input01'])
                    self.inTxtML1.SetNode(node)
                    node=self.doc.getChildByLst(self.doc.getRoot(),['root','input02'])
                    self.inTxtML2.SetNode(node)
        def SetDoc(self,doc,doc1):
            self.doc=doc
            self.doc1=doc1
            self.inTxtML1.SetDoc(doc)
            self.inTxtML2.SetDoc(doc)
            node=doc.getChildByLst(doc.getRoot(),['root','input01'])
            self.inTxtML1.SetNode(node)
            node=doc.getChildByLst(doc.getRoot(),['root','input02'])
            self.inTxtML2.SetNode(node)
        def OnCloseMe(self, event):
            self.Close(True)
        def OnCloseWindow(self, event):
            self.Destroy()
        def getFileInfo(self):
            return self.panel
    def runTest(frame, nb, log):
        win = TestPanel(nb, -1, log)
        return win
    
    
    #---------------------------------------------------------------------------
    
    def MessageDlg(self, message, type = 'Message'):
        dlg = wxMessageDialog(self, message, type, wxOK | wxICON_INFORMATION)
        dlg.ShowModal()
        dlg.Destroy()
    
    #---------------------------------------------------------------------------
    
    def RunStandalone():
        app = wx.PySimpleApp()
        frame = TestFrame(None, -1, "vtInputTaskTree Test Frame", size=(240, 300))
        frame.Show(True)
        app.MainLoop()
    #----------------------------------------------------------------------------
if __name__ == "__main__":
    optshort,optlong='l:f:c:h',['lang=','file=','config=','log=','help']
    vtLgBase.initAppl(optshort,optlong,'vXmlTaskInputTree')
    vtLog.vtLngInit('vXmlTaskInputTree','vXmlTaskInputTree.log',
            vtLog.DEBUG,iSockPort=60091)
    RunStandalone()


overview="""<html><body>
<h2>Calender</h2>

<p>show calender
"""
