
import wx
import wx.lib.popupctl

from vidarc.vApps.vTask.vNetTask import *
import vidarc.tool.log.vtLog as vtLog

import time
import string
import images

VERBOSE=1

# defined event for vgpXmlTree item selected
wxEVT_TASK_CHANGED=wx.NewEventType()
def EVT_TASK_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_TASK_CHANGED,func)
class wxTaskChanged(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_TASK_CHANGED(<widget_name>, self.OnChanged)
    """

    def __init__(self,text,id):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_TASK_CHANGED)
        self.text=text
        self.id=id
    def GetText(self):
        return self.text
    def GetId(self):
        return self.id

# defined event for vgpXmlTree item selected
wxEVT_TASK_BUILD=wx.NewEventType()
def EVT_TASK_BUILD(win,func):
    win.Connect(-1,-1,wxEVT_TASK_BUILD,func)
class wxTaskBuild(wx.PyEvent):
    """
    Posted Events:
        Text changed event
            EVT_TASK_BUILD(<widget_name>, self.OnBuild)
    """

    def __init__(self):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_TASK_BUILD)

class vpwTask(wx.lib.popupctl.PopupControl):
    def __init__(self,*_args,**_kwargs):
        apply(wx.lib.popupctl.PopupControl.__init__,(self,) + _args,_kwargs)

        self.win = wx.Window(self,-1,pos = (2,2),style = 0)
        
        bt = wx.BitmapButton(bitmap=wx.EmptyBitmap(16, 16),
              id=-1, name=u'cbClose', parent=self.win,
              pos=wx.Point(170, 0), size=wx.Size(24, 24), style=wx.BU_AUTODRAW)
        wx.EVT_BUTTON(bt,bt.GetId(),self.OnCancel)
        bt.SetBitmapLabel(images.getCancelBitmap())
        
        bt = wx.BitmapButton(bitmap=wx.EmptyBitmap(16, 16),
              id=-1, name=u'cbOk', parent=self.win,
              pos=wx.Point(140, 0), size=wx.Size(24, 24), style=wx.BU_AUTODRAW)
        wx.EVT_BUTTON(bt,bt.GetId(),self.OnClose)
        bt.SetBitmapLabel(images.getOkBitmap())
        
        id=wx.NewId()
        self.trTask = wx.TreeCtrl(self.win,id,pos = (0,30),size=wx.Size(200,200),
                            style=wx.TR_HAS_BUTTONS)
        wx.EVT_TREE_SEL_CHANGED(self.trTask, id,self.OnTcNodeTreeSelChanged)
        bz = self.trTask.GetSize()
        self.win.SetSize(wx.Size(bz.GetWidth()+7,bz.GetHeight()+30+7))
        
        self.verbose=VERBOSE
        self.selectedID=u''
        self.selectedName=u''
        self.ClearSel()
        
        self.doc=None
        self.dictTasks={}
        self.prjId=u''
        self.bClearOnEdit=True
        
        id=self.textCtrl.GetId()
        wx.EVT_TEXT(self.textCtrl, id,self.OnTextChanged)
        # This method is needed to set the contents that will be displayed
        # in the popup
        self.SetPopupContent(self.win)
    def OnTextChanged(self,evt):
        if self.bClearOnEdit==True:
            self.ClearSel()
    def SetDoc(self,doc,bNet):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'net:%d'%bNet,origin='vpwTask')
        if self.doc is not None:
            self.doc.DelConsumer(self)
        self.doc=doc
        self.doc.AddConsumer(self,self.Clear)
        if bNet:
            EVT_NET_XML_CONTENT_CHANGED(doc,self.OnChanged)
            EVT_NET_XML_SYNCH_FINISHED(doc,self.OnChanged)
        self.__generateTasks__()
    def OnChanged(self,evt):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(None,vtLog.DEBUG,'OnChanged',origin='vpwTask')
        self.ClearSel()
        self.__generateTasks__()
        evt.Skip()
    def IsBusy(self):
        return False
    def Stop(self):
        pass
    def Clear(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCallStack(None,vtLog.DEBUG,'Clear',origin='vpwTask')
        self.trTask.DeleteAllItems()
        self.ClearSel()
        self.SetValue('')
    def ClearSel(self):
        self.selTreeItem=None
        self.selNode=None
        #self.selectedID=''
        #self.selectedName=''
    def OnCancel(self,evt):
        self.PopDown()
        self.Select(self.selectedID,self.selectedName)
        
        evt.Skip()
    def OnClose(self,evt):
        self.PopDown()
        id=self.GetSelID()
        if self.doc is not None:
            if self.doc.IsKeyValid(id):
                self.bClearOnEdit=False
                self.SetValue(self.GetHier(self.GetSelTreeItem()))
                self.bClearOnEdit=True
            else:
                self.SetValue(u'')
        else:
            self.SetValue(u'')
        evt.Skip()

    #def SetTasks(self,dictTask):
    #    self.dictTask=dictTask
    #    self.prjId=u''
    #    self.trTask.DeleteAllItems()
    def __generateTasks__(self):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'generate task',origin='vpwTask')
        self.dictTasks={}
        if self.doc is None:
            return
        #self.doc.acquire('access')
        prjTasks=self.doc.getChilds(self.doc.getBaseNode(),'prjtasks')
        for prj in prjTasks:
            if self.verbose:
                vtLog.vtLngCallStack(self,vtLog.DEBUG,'prjid:%s'%self.doc.getAttribute(prj,'fid'),origin='vpwTask')
            dictTask={}
            for task in self.doc.getChilds(prj,'task'):
                if self.verbose:
                    vtLog.vtLngCallStack(self,vtLog.DEBUG,'taskid:%s'%self.doc.getAttribute(task,'fid'),origin='vpwTask')
                dictSubTask={}
                for subTask in self.doc.getChilds(task,'subtask'):
                    if self.verbose:
                        vtLog.vtLngCallStack(self,vtLog.DEBUG,'subtaskid:%s'%self.doc.getAttribute(subTask,'fid'),origin='vpwTask')
                    dictSubTask[self.doc.getAttribute(subTask,'fid')]=(
                        self.doc.getNodeText(subTask,'tag'),subTask)
                dictTask[self.doc.getAttribute(task,'fid')]=(
                    self.doc.getNodeText(task,'tag'),task,dictSubTask)
            self.dictTasks[self.doc.getAttribute(prj,'fid')]=dictTask
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'prj keys:%s'%repr(self.dictTasks.keys()),origin='vpwTask')
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'finished',origin='vpwTask')
        prjId=self.prjId
        self.prjId=''
        #self.doc.release('access')
        wx.PostEvent(self,wxTaskBuild())
        wx.CallAfter(self.SetPrj,prjId)
    def __sortAllChildren__(self,ti):
        triChild=self.trTask.GetFirstChild(ti)
        while triChild[0].IsOk():
            self.__sortAllChildren__(triChild[0])
            triChild=self.trTask.GetNextChild(ti,triChild[1])
        self.trTask.SortChildren(ti)
    def __findItem__(self,ti,info):
        triChild=self.trTask.GetFirstChild(ti)
        while triChild[0].IsOk():
            if self.trTask.GetItemText(triChild[0])==info:
                return triChild[0]
            #retVal=self.__findNode__(triChild[0],node2Find)
            #if retVal is not None:
            #    return retVal
            triChild=self.trTask.GetNextChild(ti,triChild[1])
        return None
    def __findNodeByID__(self,ti,id2Find):
        if ti is None:
            return None
        try:
            o=self.trTask.GetPyData(ti)
        except:
            return None
        if o is not None:
            try:
                if long(self.doc.getAttribute(o,'fid'))==long(id2Find):
                    return (ti,o)
            except:
                pass
        triChild=self.trTask.GetFirstChild(ti)
        while triChild[0].IsOk():
            retVal=self.__findNodeByID__(triChild[0],id2Find)
            if retVal is not None:
                return retVal
            triChild=self.trTask.GetNextChild(ti,triChild[1])
        return None
    def GetHier(self,ti):
        if ti is None:
            return u''
        if ti==self.trTask.GetRootItem():
            return u''
        else:
            return self.GetHier(self.trTask.GetItemParent(ti))+u'.'+self.trTask.GetItemText(ti)
    def Select(self,id,name):
        self.bClearOnEdit=False
        self.selectedID=id
        self.selectedName=name
        tup=self.__findNodeByID__(self.trTask.GetRootItem(),id)
        if tup is not None:
            self.trTask.SelectItem(tup[0])
            self.trTask.EnsureVisible(tup[0])
            self.SetValue(self.GetHier(tup[0]))
        else:
            self.ClearSel()
            self.trTask.UnselectAll()
            self.SetValue(name)
        self.bClearOnEdit=True
    def __addItem__(self,ti,info,node):
        tif=self.__findItem__(ti,info)
        if tif is not None:
            return tif
        tid=wx.TreeItemData()
        tid.SetData(node)
        return self.trTask.AppendItem(ti,info,-1,-1,tid)
    def SetPrj(self,prjId):
        if self.verbose:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'prjid:%s old prjid:%s'%(prjId,self.prjId),origin='vpwTask')
        self.selTreeItem=None
        self.selNode=None
        self.selectedID=''
        self.SetValue('')
        if self.prjId!=prjId:
            self.trTask.DeleteAllItems()
            rootTi=self.trTask.AddRoot(u'task')
            self.prjId=prjId
            if self.dictTasks.has_key(self.prjId)==False:
                if self.verbose:
                    vtLog.vtLngCallStack(self,vtLog.DEBUG,'prjid not found',origin='vpwTask')
                return
            d=self.dictTasks[self.prjId]
            keys=d.keys()
            if self.verbose:
                vtLog.vtLngCallStack(self,vtLog.DEBUG,'keys:%s'%repr(keys),origin='vpwTask')
            for k in keys:
                strs=string.split(d[k][0],'.')
                ti=rootTi
                for s in strs[:-1]:
                    ti=self.__addItem__(ti,s,None)
                ti=self.__addItem__(ti,strs[-1],d[k][1])
                subDict=d[k][2]
                for subTask in subDict.keys():
                    self.__addItem__(ti,subDict[subTask][0],subDict[subTask][1])
            self.__sortAllChildren__(rootTi)
            self.trTask.Expand(rootTi)
    def GetSelNode(self):
        return self.selNode
    def GetSelTreeItem(self):
        return self.selTreeItem
    def GetSelID(self):
        if self.doc is None:
            return ''
        if self.selNode is not None:
            return self.doc.getAttribute(self.selNode,'fid')
        else:
            return ''
    def OnTcNodeTreeSelChanged(self,evt):
        try:
            tn=evt.GetItem()
            self.selTreeItem=tn
            self.selNode=self.trTask.GetPyData(tn)
        except:
            self.selTreeItem=None
            self.selNode=None
            self.SetValue(u'')
    def FormatContent(self):
        # I parse the value in the text part to resemble the correct date in
        # the calendar control
        #t=convertStrTime2Time(self.GetValue() + ' 000000')
        #if t is None:
        #    t=time.localtime(time.time())
        #else:
        #    t=time.localtime(t)
        #self.cal.SetDate(wxDateTimeFromDMY(t[2],t[1]-1,t[0]))
        #self.txt.SetValue("%02d:%02d:%02d"%(t[3],t[4],t[5]))
        pass
        
