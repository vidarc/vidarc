#----------------------------------------------------------------------------
# Name:         vXmlNodeSubTaskAttr.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060818
# CVS-ID:       $Id: vXmlNodeSubTaskAttr.py,v 1.1 2006/08/29 10:58:39 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeAttrML import *
try:
    if vcCust.is2Import(__name__):
        from vidarc.tool.input.vtInputAttrCfgMLPanel import vtInputAttrCfgMLPanel
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0
    
class vXmlNodeSubTaskAttr(vtXmlNodeAttrML):
    NODE_ATTRS=[
        ]
    FUNCS_GET_SET_4_LST=[]
    def __init__(self,tagName='subtaskAttr',cfgBase=['cfg']):
        global _
        _=vtLgBase.assignPluginLang('vTask')
        vtXmlNodeAttrML.__init__(self,tagName,cfgBase=cfgBase)
    def GetDescription(self):
        return _(u'subtask attributes')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def Is2Create(self):
        return True
    def Is2Add(self):
        return False
    def IsMultiple(self):
        return False
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return True
    def IsId2Add(self):
        return True

