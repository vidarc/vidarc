#Boa:FramePanel:vXmlNodePrjTaskPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeXXXPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      2006
# CVS-ID:       $Id: vXmlNodePrjTaskPanel.py,v 1.3 2010/03/03 02:17:17 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.vApps.vPrj.vXmlPrjInputTree

import sys

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_VXMLNODEPRJTASKPANEL, wxID_VXMLNODEPRJTASKPANELLBLPRJ, 
 wxID_VXMLNODEPRJTASKPANELVIPRJ, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vXmlNodePrjTaskPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsPrj, 0, border=0, flag=wx.EXPAND)

    def _init_coll_bxsPrj_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPrj, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viPrj, 2, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(3)
        parent.AddGrowableCol(0)
        parent.AddGrowableCol(1)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=0, rows=4, vgap=0)

        self.bxsPrj = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsPrj_Items(self.bxsPrj)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEPRJTASKPANEL,
              name=u'vXmlNodePrjTaskPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(312, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 180))
        self.SetAutoLayout(True)

        self.lblPrj = wx.StaticText(id=wxID_VXMLNODEPRJTASKPANELLBLPRJ,
              label=_(u'project'), name=u'lblPrj', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(97, 26), style=wx.ALIGN_RIGHT)
        self.lblPrj.SetMinSize(wx.Size(-1, -1))

        self.viPrj = vidarc.vApps.vPrj.vXmlPrjInputTree.vXmlPrjInputTree(id=wxID_VXMLNODEPRJTASKPANELVIPRJ,
              name=u'viPrj', parent=self, pos=wx.Point(101, 0),
              size=wx.Size(202, 26), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vTask')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def ClearWid(self):
        try:
            vtXmlNodePanel.ClearWid(self)
            # add code here
            self.viPrj.Clear()
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def SetNetDocs(self,d):
        # add code here
        if d.has_key('vPrj'):
            dd=d['vPrj']
            self.viPrj.SetDocTree(dd['doc'],dd['bNet'])
    def SetDoc(self,doc,bNet=False):
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        # add code here
        self.viPrj.SetDoc(self.doc)
    def SetNode(self,node):
        try:
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            # add code here
            idPrj=self.objRegNode.GetPrjID(self.node)
            self.viPrj.SetValueID('',idPrj)
            #oTrans=self.doc.GetRegisteredNode(self.objRegNode.GetTagNameTransition())
            
        except:
            self.__logTB__()
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if node is None:
                return
            # add code here
            idPrj=self.viPrj.GetID()
            self.objRegNode.SetPrjID(node,idPrj)
            self.viPrj.SetModified(False)
            
            self.GetNodeFin(node)
        except:
            self.__logTB__()
    def Close(self):
        vtXmlNodePanel.Close(self)
        # add code here
        self.viPrj.Close()
    def Cancel(self):
        vtXmlNodePanel.Cancel(self)
        # add code here
    def Lock(self,flag):
        vtXmlNodePanel.Lock(self,flag)
        if flag:
            # add code here
            self.viPrj.Enable(False)
        else:
            # add code here
            self.viPrj.Enable(True)
