#Boa:FramePanel:vGlobalsMainPanel
#----------------------------------------------------------------------------
# Name:         vGlobalsMainPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060514
# CVS-ID:       $Id: vGlobalsMainPanel.py,v 1.9 2007/11/29 11:51:10 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.xml.vtXmlNodeTagCmdPanel
import wx.lib.buttons
import  wx.lib.anchors as anchors
import getpass,time

import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.vApps.vGlobals.vXmlGlobalsTree  import *
from vidarc.vApps.vGlobals.vNetGlobals import *
from vidarc.vApps.vHum.vNetHum import *
from vidarc.tool.xml.vtXmlNodeTagPanel import *
from vidarc.tool.xml.vtXmlNodeRegListBook import *
from vidarc.tool.net.vtNetSecXmlGuiMaster import *
from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer
import vidarc.tool.InOut.fnUtil as fnUtil
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt

import images

VERBOSE=0

def create(parent):
    return vGlobalsMainPanel(parent)

[wxID_VGLOBALSMAINPANEL, wxID_VGLOBALSMAINPANELPNDATA, 
 wxID_VGLOBALSMAINPANELSBSTATUS, wxID_VGLOBALSMAINPANELSLWNAV, 
 wxID_VGLOBALSMAINPANELSLWTOP, wxID_VGLOBALSMAINPANELVITAG, 
] = [wx.NewId() for _init_ctrls in range(6)]

STATUS_TEXT_POS=2
STATUS_LOG_POS=3
STATUS_CLK_POS=4

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon

[wxID_VGFPRJTIMERMAINTOOLBAR1TOOLS0] = [wx.NewId() for _init_coll_toolBar1_Tools in range(1)]

[wxID_VGLOBALSMAINFRAMEMNVIEWITEM_LANG, 
 wxID_VGLOBALSMAINFRAMEMNVIEWITEM_VIEW_TREE, 
] = [wx.NewId() for _init_coll_mnView_Items in range(2)]

[wxID_VGLOBALSMAINFRAMEMNLANGITEM_LANG_DE, 
 wxID_VGLOBALSMAINFRAMEMNLANGITEM_LANG_EN, 
] = [wx.NewId() for _init_coll_mnLang_Items in range(2)]

[wxID_VGLOBALSMAINPANELMNHELPMN_HELP_ABOUT, 
 wxID_VGLOBALSMAINPANELMNHELPMN_HELP_HELP, 
 wxID_VGLOBALSMAINPANELMNHELPMN_HELP_LOG, 
] = [wx.NewId() for _init_coll_mnHelp_Items in range(3)]

[wxID_VGLOBALSMAINPANELMNFILEITEM_EXIT] = [wx.NewId() for _init_coll_mnFile_Items in range(1)]

[wxID_VGLOBALSMAINPANELMNTOOLSITEM_REQ_LOCK] = [wx.NewId() for _init_coll_mnTools_Items in range(1)]

class vGlobalsMainPanel(wx.Panel,vtXmlDomConsumer):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VGLOBALSMAINPANEL,
              name=u'vGlobalsMainPanel', parent=prnt, pos=wx.Point(115, 10),
              size=wx.Size(819, 555), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(811, 528))
        self.Bind(wx.EVT_SIZE, self.OnSize)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VGLOBALSMAINPANELSLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 0), size=wx.Size(200,
              489), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetDefaultSize(wx.Size(200, 1000))
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetAutoLayout(True)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetLabel(u'navigation')
        self.slwNav.SetToolTipString(u'navigation')
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VGLOBALSMAINPANELSLWNAV)

        self.slwTop = wx.SashLayoutWindow(id=wxID_VGLOBALSMAINPANELSLWTOP,
              name=u'slwTop', parent=self, pos=wx.Point(200, 0),
              size=wx.Size(608, 200), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwTop.SetDefaultSize(wx.Size(1000, 200))
        self.slwTop.SetAlignment(wx.LAYOUT_TOP)
        self.slwTop.SetAutoLayout(True)
        self.slwTop.SetSashVisible(wx.SASH_BOTTOM, True)
        self.slwTop.SetOrientation(wx.LAYOUT_HORIZONTAL)
        self.slwTop.SetLabel(u'information')
        self.slwTop.SetToolTipString(u'information')
        self.slwTop.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwTopSashDragged,
              id=wxID_VGLOBALSMAINPANELSLWTOP)

        self.pnData = wx.Panel(id=wxID_VGLOBALSMAINPANELPNDATA, name=u'pnData',
              parent=self, pos=wx.Point(208, 248), size=wx.Size(592, 192),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)
        self.pnData.SetAutoLayout(True)

        self.viTag = vidarc.tool.xml.vtXmlNodeTagCmdPanel.vtXmlNodeTagCmdPanel(id=wxID_VGLOBALSMAINPANELVITAG,
              name=u'viTag', parent=self.slwTop, pos=wx.Point(0, 0),
              size=wx.Size(392, 117), style=0)
        self.viTag.Bind(vidarc.tool.xml.vtXmlNodeTagCmdPanel.vEVT_TOOL_XML_TAG_APPLY,
              self.OnViTagToolXmlTagApply, id=wxID_VGLOBALSMAINPANELVITAG)
        self.viTag.Bind(vidarc.tool.xml.vtXmlNodeTagCmdPanel.vEVT_TOOL_XML_TAG_CANCEL,
              self.OnViTagToolXmlTagCancel, id=wxID_VGLOBALSMAINPANELVITAG)

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vGlobals')
        vtXmlDomConsumer.__init__(self)
        self._init_ctrls(parent)
        self.SetName(name)
        self.bActivated=False
        self.dCfg={}
        
        self.nodeCurStarted=None
        self.nodeCurStartedTreeItem=None
        self.nodeLock=None
        
        self.baseDoc=None
        self.baseSettings=None
        
        self.xdCfg=vtXmlDom(appl='vGlobalsCfg',audit_trail=False)
        
        appls=['vGlobals','vHum']
        
        rect = parent.sbStatus.GetFieldRect(0)
        self.netMaster=vtNetSecXmlGuiMaster(parent.sbStatus,'Master',pos=(rect.x+2, rect.y+2),
                    size=(rect.width-4, rect.height-4),verbose=0)
        
        self.netGlobals=self.netMaster.AddNetControl(vNetGlobals,'vGlobals',synch=True,
                                            verbose=0,audit_trail=True)
        self.netHum=self.netMaster.AddNetControl(vNetHum,'vHum',synch=True,verbose=0)
        self.netMaster.AddNetControl(None,None,verbose=0)
        
        vtXmlDomConsumer.SetDoc(self,self.netGlobals)
        #vtXmlDomConsumerLang.SetDoc(self,self.netGlobals)
        
        self.netGlobals.SetDftLanguages()
        self.netMaster.SetLang(lang=vtLgBase.getPluginLang())
        
        self.netMaster.SetCfg(self.xdCfg,[],appls,'vGlobalsAppl')
        
        self.bAutoConnect=True
        self.OpenCfgFile()
        
        self.vgpTree=vXmlGlobalsTree(self.slwNav,wx.NewId(),
                pos=(0,0),size=(198,489),style=0,name="vgpTree",
                master=True,verbose=0)
        self.vgpTree.SetConstraints(
            anchors.LayoutAnchors(self.vgpTree, True, True, True, True)
            )
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        self.vgpTree.SetDoc(self.netGlobals,True)
        self.vgpTree.EnableImportMenu(True)
        self.vgpTree.EnableExportMenu(True)
        
        self.nbData=vtXmlNodeRegListBook(self.pnData,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="nbRegs")
        self.nbData.SetDoc(self.netGlobals,True)
        self.nbData.Show(False)
        self.nbData.SetConstraints(
            anchors.LayoutAnchors(self.nbData, True, True, True, True)
            )
        
        self.nbLoc=vtXmlNodeRegListBook(self.pnData,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="nbRegs")
        self.nbLoc.SetDoc(self.netGlobals,True)
        self.nbLoc.Show(False)
        self.nbLoc.SetConstraints(
            anchors.LayoutAnchors(self.nbData, True, True, True, True)
            )
        
        self.nbPurchase=vtXmlNodeRegListBook(self.pnData,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="nbRegs")
        self.nbPurchase.SetDoc(self.netGlobals,True)
        self.nbPurchase.Show(False)
        self.nbPurchase.SetConstraints(
            anchors.LayoutAnchors(self.nbData, True, True, True, True)
            )
        
        self.nbTravel=vtXmlNodeRegListBook(self.pnData,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="nbRegs")
        self.nbTravel.SetDoc(self.netGlobals,True)
        self.nbTravel.Show(False)
        self.nbTravel.SetConstraints(
            anchors.LayoutAnchors(self.nbData, True, True, True, True)
            )
        
        self.nbTravelCost=vtXmlNodeRegListBook(self.pnData,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="nbRegs")
        self.nbTravelCost.SetDoc(self.netGlobals,True)
        self.nbTravelCost.Show(False)
        self.nbTravelCost.SetConstraints(
            anchors.LayoutAnchors(self.nbData, True, True, True, True)
            )
        
        oHolidays=self.netGlobals.GetRegisteredNode('holidays')
        self.nbHoliday=oHolidays.GetPanelClass()(self.pnData,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="nbHolidays")
        self.nbHoliday.SetDoc(self.netGlobals,True)
        self.nbHoliday.Show(False)
        self.nbHoliday.SetConstraints(
            anchors.LayoutAnchors(self.nbData, True, True, True, True)
            )
        
        self.nbEml=vtXmlNodeRegListBook(self.pnData,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="nbEml")
        self.nbEml.SetDoc(self.netGlobals,True)
        self.nbEml.Show(False)
        self.nbEml.SetConstraints(
            anchors.LayoutAnchors(self.nbEml, True, True, True, True)
            )
        
        self.nbSite=vtXmlNodeRegListBook(self.pnData,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="nbSite")
        self.nbSite.SetDoc(self.netGlobals,True)
        self.nbSite.Show(False)
        self.nbSite.SetConstraints(
            anchors.LayoutAnchors(self.nbSite, True, True, True, True)
            )
        
        oGlobals=self.netGlobals.GetRegisteredNode('globals')
        self.viTag.AddWidgetDependent(self.nbData,oGlobals)
        oGlobalsLoc=self.netGlobals.GetRegisteredNode('globalsLoc')
        self.viTag.AddWidgetDependent(self.nbLoc,oGlobalsLoc)
        oGlobalsPurchase=self.netGlobals.GetRegisteredNode('globalsPurchase')
        self.viTag.AddWidgetDependent(self.nbPurchase,oGlobalsPurchase)
        oGlobalsTravel=self.netGlobals.GetRegisteredNode('globalsTravel')
        self.viTag.AddWidgetDependent(self.nbTravel,oGlobalsTravel)
        oGlobalsTravelCost=self.netGlobals.GetRegisteredNode('globalsTravelCost')
        self.viTag.AddWidgetDependent(self.nbTravelCost,oGlobalsTravelCost)
        oHolidays=self.netGlobals.GetRegisteredNode('holidays')
        self.viTag.AddWidgetDependent(self.nbHoliday,oHolidays)
        oEml=self.netGlobals.GetRegisteredNode('email')
        self.viTag.AddWidgetDependent(self.nbEml,oEml)
        oSite=self.netGlobals.GetRegisteredNode('site')
        self.viTag.AddWidgetDependent(self.nbSite,oSite)
        oGlobalsArea=self.netGlobals.GetRegisteredNode('globalsArea')
        self.viTag.AddWidgetDependent(None,oGlobalsArea)
        oArea=self.netGlobals.GetRegisteredNode('area')
        self.viTag.AddWidgetDependent(None,oArea)
        oGlobalsDepartment=self.netGlobals.GetRegisteredNode('globalsDepartment')
        self.viTag.AddWidgetDependent(None,oGlobalsDepartment)
        
        for sTagAttr,sTag in [('hourlyRate','hourlyRate'),
                            ('globCostPrjPlan','cost'),
                            ('globCostPrjPlan','costInKind'),
                            ('globAttrPrjPlan','globalsPrjPlan'),
                            ('functionAttr','function'),
                            ('vacationAttr','vacation'),]:
            pn=self.viTag.CreateRegisteredPanel(sTagAttr,
                            self.netGlobals,True,
                            self.pnData)
            pn.Show(False)
            pn.SetConstraints(
                anchors.LayoutAnchors(self.nbSite, True, True, True, True)
                )
            o=self.netGlobals.GetRegisteredNode(sTag)
            self.viTag.AddWidgetDependent(pn,o)
        
        self.viTag.SetDoc(self.netGlobals,True)
        self.viTag.SetRegNode(oGlobals)
        self.viTag.SetRegNode(oGlobalsLoc)
        self.viTag.SetRegNode(oGlobalsPurchase)
        self.viTag.SetRegNode(oGlobalsTravel)
        self.viTag.SetRegNode(oGlobalsTravelCost)
        self.viTag.SetRegNode(oHolidays)
        self.viTag.SetRegNode(oEml)
        self.viTag.SetRegNode(oSite)
        self.viTag.SetRegNode(oGlobalsArea)
        self.viTag.SetRegNode(oArea)
        self.viTag.SetRegNode(oGlobalsDepartment)
        #self.viTag.SetRegNode(oHourlyRate)
        #self.nbData.SetRegNode(oGlobals)
        self.SetSize(size)

        #self.CreateNew()
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        #self.netMaster.Destroy()
        pass
    def GetDocMain(self):
        return self.netGlobals
    def GetNetMaster(self):
        return self.netMaster
    def GetTreeMain(self):
        return self.vgpTree
    def Clear(self):
        if self.node is not None:
            self.netGlobals.endEdit(self.node)
        vtXmlDomConsumer.Clear(self)
    def OnTreeItemSel(self,event):
        node=event.GetTreeNodeData()
        if self.node is not None:
            self.netGlobals.endEdit(self.node)
        if event.GetTreeNodeData() is not None:
            #self.netGlobals.startEdit(node)
            self.viTag.SetNode(node)
            #self.nbData.SetNode(node)
        else:
            # grouping tree item selected
            self.viTag.SetNode(node)
            #self.nbData.SetNode(None)
            pass
        event.Skip()
    def __getSettings__(self):
        return
        self.xmlLastPrj=None
        self.xmlLastTask=None
        self.xmlLastSubTask=None
        self.xmlLastDesc=None
        self.xmlLastLoc=None
        self.xmlLastPerson=None
        
        self.xmlPrjFN=self.netGlobals.getNodeText(self.baseSettings,'projectfn')
        self.xmlHumanFN=self.netGlobals.getNodeText(self.baseSettings,'humanfn')
        self.xmlLocationFN=self.netGlobals.getNodeText(self.baseSettings,'locationfn')
        self.xmlBaseTmplDN=self.netGlobals.getNodeText(self.baseSettings,'templatedn')
        self.xmlProjectDN=self.netGlobals.getNodeText(self.baseSettings,'projectdn')
        # get tree view settings
        sTreeViewGrp=self.netGlobals.getNodeText(self.baseSettings,'treeviewgrp')
        mnItems=self.mnViewTree.GetMenuItems()
        
        self.vglbDocGrpInfo.SetTemplateBaseDir(self.xmlBaseTmplDN)
        if sTreeViewGrp=='normal':
            mnItems[0].Check(True)
            self.__EnsureOneCheckMenuItem__(mnItems,0,0,2)
            self.vgpTree.SetNormalGrouping()
        elif sTreeViewGrp=='iddocgrp':
            mnItems[1].Check(True)
            self.__EnsureOneCheckMenuItem__(mnItems,1,0,2)
            self.vgpTree.SetIdGrouping()
        else:
            mnItems[0].Check(True)
            self.__EnsureOneCheckMenuItem__(mnItems,0,0,2)
            #self.vgpTree.SetGrpUsrDate()
            
    def __getXmlBaseNodes__(self):
        self.baseDoc=None
        self.baseSettings=None
        
        self.baseDoc=self.netGlobals.getBaseNode()
        self.baseSettings=self.netGlobals.getChildForced(self.netGlobals.getRoot(),'settings')
        #if self.baseSettings is None:
        #    self.baseSettings=self.netGlobals.createSubNode(self.netGlobals.getRoot(),'settings')
        #    self.netGlobals.AlignNode(self.baseSettings,iRec=1)
        self.__getSettings__()
        self.netGlobals.AlignNode(self.baseSettings,iRec=1)
    def OpenFile(self,fn):
        vtLog.vtLngCS(vtLog.INFO,'file:%s'%repr(fn))
        if self.netGlobals.ClearAutoFN()>0:
            self.netGlobals.Open(fn)
            
            self.__getXmlBaseNodes__()
    def OpenCfgFile(self,fn=None):
        vtLog.vtLngCS(vtLog.INFO,'file:%s'%repr(fn))
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                self.netMaster.SetCfgNode()
                self.__setCfg__()
                return
        self.xdCfg.New(root='config')
        if fn is not None:
            self.xdCfg.SetFN(fn)
            self.netMaster.SetCfgNode()
    def CreateNew(self):
        vtLog.vtLngCS(vtLog.INFO,'new')
        self.netGlobals.New()
        self.__getXmlBaseNodes__()
        self.vgpTree.SetNode(None)
    def __setCfg__(self):
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        self.pnData.Refresh()
        
        self.bBlockCfgEventHandling=False
        
    def OnSlwNavSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iWidth=event.GetDragRect().width
            #if iWidth>80:
            #    iWidth=80
            self.dCfg['nav_sash']=str(iWidth)
            
            self.slwNav.SetDefaultSize((iWidth, 1000))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnSlwTopSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iHeight=event.GetDragRect().height
            #if iHeight>80:
            #    iHeight=80
            self.dCfg['top_sash']=str(iHeight)
            
            self.slwTop.SetDefaultSize((1000, iHeight))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnSize(self, event):
        bMod=False
        try:
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            event.Skip()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnCbApplyButton(self, event):
        event.Skip()

    def OnCbCancelButton(self, event):
        event.Skip()

    def OnViTagToolXmlTagApply(self, event):
        event.Skip()

    def OnViTagToolXmlTagCancel(self, event):
        event.Skip()
    def SetCfgData(self,func,l,d):
        self.cfgFunc=func
        self.cfgOrigin=l
        self.dCfg=d
        try:
            i=int(self.dCfg['nav_sash'])
            self.slwNav.SetDefaultSize((i, 1000))
        except:
            pass
        try:
            i=int(self.dCfg['top_sash'])
            self.slwTop.SetDefaultSize((1000,i))
        except:
            pass
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        self.pnData.Refresh()
        try:
            sz=map(int,self.dCfg['size'].split(','))
            self.GetParent().SetSize(sz)
        except:
            pass
    def GetCfgData(self):
        try:
            sz=self.GetParent().GetSize()
            self.dCfg['size']='%d,%d'%(sz[0],sz[1])
        except:
            pass
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,self.dCfg)
    def GetAboutData(self):
        import __config__
        version=u' %d.%d.%d.%d'%(__config__.VER_MAJOR,__config__.VER_MINOR,
                    __config__.VER_RELEASE,__config__.VER_SUBREL)
        desc=_(u"""Globals module.

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
""")
        return _(u'VIDARC Globals'),desc,version
