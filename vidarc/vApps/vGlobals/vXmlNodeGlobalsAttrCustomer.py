#----------------------------------------------------------------------------
# Name:         vXmlNodeGlobalsAttrCustomer.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060703
# CVS-ID:       $Id: vXmlNodeGlobalsAttrCustomer.py,v 1.2 2010/03/03 02:17:12 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog

from vidarc.vApps.vGlobals.vXmlNodeGlobalsAttrML import vXmlNodeGlobalsAttrML

class vXmlNodeGlobalsAttrCustomer(vXmlNodeGlobalsAttrML):
    def __init__(self,tagName='globAttrCustomer',cfgBase=['cfg']):
        vXmlNodeGlobalsAttrML.__init__(self,tagName,cfgBase=cfgBase)
    def GetDescription(self):
        return _(u'customer global attributes')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x94IDAT8\x8d\xadS\xb1\r\x800\x0cs\n\xef\x00W\xf4\x03$^\xe9\t\x88[\
\xf8\x80+\x10\x07\xc1TT5q\x04\x82\x8cN\xec\xba\x91#\x12\x1a\xd4\xd5\xcd\xe3\
\xa9@\x00GZ\xa5\xc6\xc2\xd3A\x0b\x03\x00\xc9\x0e\xd8\xab\xac\xb2\xa0\xe9\xe0\
M\xb9\x02{\x8c\xd8c\xf4\x05\x98\xf5\x92\xc8D\xbay<\xa5_&%\xc0\x08\xc3\xb6i\
\x07O\xc9\xac\xf7\xff\x12-\x9b^/\x00:$\xd6`\x8d\xdd9`\t+\t\xcc\xd5\x91Vi\xa9\
_\x87X\xd6\xe7%\x8au\x8d\x80\xbe\r\xf6U\xd3\x81\x95N\x96\xd8\x0b\xe4r8\xd4\
\xd7{S\xed\x00\x00\x00\x00IEND\xaeB`\x82'  

