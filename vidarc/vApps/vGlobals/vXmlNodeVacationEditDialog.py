#Boa:Dialog:vXmlNodeVacationEditDialog
#----------------------------------------------------------------------------
# Name:         vXmlNodeVacationEditDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20070902
# CVS-ID:       $Id: vXmlNodeVacationEditDialog.py,v 1.2 2010/03/03 02:17:13 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

from vidarc.vApps.vGlobals.vXmlNodeAreaPanel import vXmlNodeAreaPanel

import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.xml.vtXmlNodeDialog import vtXmlNodeDialog

def create(parent):
    return vXmlNodeVacationEditDialog(parent)

[wxID_VXMLNODEVACATIONEDITDIALOG, wxID_VXMLNODEVACATIONEDITDIALOGCBAPPLY, 
 wxID_VXMLNODEVACATIONEDITDIALOGCBCANCEL, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vXmlNodeVacationEditDialog(wx.Dialog,vtXmlNodeDialog):
    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsBt, (1, 0), border=4,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER, span=(1, 1))

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=0, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_gbsData_Items(self.gbsData)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VXMLNODEVACATIONEDITDIALOG,
              name=u'vXmlNodeVacationEditDialog', parent=prnt, pos=wx.Point(348,
              174), size=wx.Size(360, 140), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vGlobals Vacation Edit Dialog')
        self.SetClientSize(wx.Size(352, 113))

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODEVACATIONEDITDIALOGCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'),
              name=u'cbApply', parent=self, pos=wx.Point(0, 24),
              size=wx.Size(76, 30), style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VXMLNODEVACATIONEDITDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODEVACATIONEDITDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(108, 24),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VXMLNODEVACATIONEDITDIALOGCBCANCEL)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vGlobals')
        self._init_ctrls(parent)
        vtXmlNodeDialog.__init__(self)
        try:
            self.gbsData.AddGrowableCol(0)
            self.gbsData.AddGrowableRow(0)
        
            self.pn=vXmlNodeAreaPanel(self,id=wx.NewId(),pos=(4,4),size=(340,70),
                        style=0,name=u'pnVacation')
            self.gbsData.AddWindow(self.pn, (0, 0), border=4,
                  flag=wx.BOTTOM | wx.TOP | wx.LEFT | wx.RIGHT | wx.EXPAND, span=(1, 1))
        except:
            self.__logTB__()
