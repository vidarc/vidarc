#Boa:FramePanel:vNodeGlobalsPanel
#----------------------------------------------------------------------------
# Name:         vNodeGlobalsPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060514
# CVS-ID:       $Id: vNodeGlobalsPanel.py,v 1.3 2010/03/29 08:54:43 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import sys

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import vidarc.tool.art.vtArt as vtArt
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VNODEGLOBALSPANEL] = [wx.NewId() for _init_ctrls in range(1)]

class vNodeGlobalsPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=1
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VNODEGLOBALSPANEL,
              name=u'vNodeGlobalsPanel', parent=prnt, pos=wx.Point(405, 257),
              size=wx.Size(378, 331), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(370, 304))

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,lWidgets=[])
        
        
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
    def __Clear__(self):
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
            # add code here
            
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def SetNetDocs(self,d):
        self.__logCritical__('FIXME'%())
        return
        if d.has_key('vHum'):
            dd=d['vHum']
            self.docHum=dd['doc']
        # add code here
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            
            #oTrans=self.doc.GetRegisteredNode(self.objRegNode.GetTagNameTransition())
            
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            try:
                # add code here
                pass
            except:
                self.__logTB__()
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            # add code here
            pass
        else:
            # add code here
            pass
