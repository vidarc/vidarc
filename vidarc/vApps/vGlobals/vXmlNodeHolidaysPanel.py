#Boa:FramePanel:vXmlNodeHolidaysPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeHolidaysPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060526
# CVS-ID:       $Id: vXmlNodeHolidaysPanel.py,v 1.7 2010/03/29 08:54:43 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.time.vtTimeDateGM
import vidarc.tool.input.vtInputText
import wx.lib.buttons

import sys

from vidarc.tool.time.vtTime import vtDateTime
from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VXMLNODEHOLIDAYSPANEL, wxID_VXMLNODEHOLIDAYSPANELCBADD, 
 wxID_VXMLNODEHOLIDAYSPANELCBDEL, wxID_VXMLNODEHOLIDAYSPANELLBLDATE, 
 wxID_VXMLNODEHOLIDAYSPANELLBLHOLIDAYS, wxID_VXMLNODEHOLIDAYSPANELLBLNAME, 
 wxID_VXMLNODEHOLIDAYSPANELLBLYEAR, wxID_VXMLNODEHOLIDAYSPANELLSTHOLIDAYS, 
 wxID_VXMLNODEHOLIDAYSPANELRDBHOURS, wxID_VXMLNODEHOLIDAYSPANELVIDATE, 
 wxID_VXMLNODEHOLIDAYSPANELVINAME, wxID_VXMLNODEHOLIDAYSPANELVIYEAR, 
] = [wx.NewId() for _init_ctrls in range(12)]

class vXmlNodeHolidaysPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_bxsDate_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblDate, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viDate, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsYear, 1, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.lblHolidays, 1, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.lstHolidays, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbDel, 0, border=0, flag=0)
        parent.AddSizer(self.bxsName, 1, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsDate, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbAdd, 0, border=0, flag=0)
        parent.AddWindow(self.rdbHours, 1, border=0, flag=wx.EXPAND)

    def _init_coll_bxsYear_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblYear, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viYear, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsName_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblName, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viName, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_lstHolidays_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'month'), width=60)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'day'), width=60)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'name'), width=180)
        parent.InsertColumn(col=3, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'kind'), width=180)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=2, hgap=0, rows=6, vgap=0)

        self.bxsYear = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsName = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsDate = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsYear_Items(self.bxsYear)
        self._init_coll_bxsName_Items(self.bxsName)
        self._init_coll_bxsDate_Items(self.bxsDate)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEHOLIDAYSPANEL,
              name=u'vXmlNodeHolidaysPanel', parent=prnt, pos=wx.Point(405,
              257), size=wx.Size(280, 297), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(272, 270))

        self.lblYear = wx.StaticText(id=wxID_VXMLNODEHOLIDAYSPANELLBLYEAR,
              label=_(u'year'), name=u'lblYear', parent=self, pos=wx.Point(0,
              0), size=wx.Size(80, 21), style=wx.ALIGN_RIGHT)
        self.lblYear.SetMinSize(wx.Size(-1, -1))

        self.viYear = vidarc.tool.input.vtInputText.vtInputText(id=wxID_VXMLNODEHOLIDAYSPANELVIYEAR,
              name=u'viYear', parent=self, pos=wx.Point(84, 0),
              size=wx.Size(156, 21), style=0)

        self.lblHolidays = wx.StaticText(id=wxID_VXMLNODEHOLIDAYSPANELLBLHOLIDAYS,
              label=_(u'holidays'), name=u'lblHolidays', parent=self,
              pos=wx.Point(0, 21), size=wx.Size(241, 13), style=0)
        self.lblHolidays.SetMinSize(wx.Size(-1, -1))

        self.lstHolidays = wx.ListCtrl(id=wxID_VXMLNODEHOLIDAYSPANELLSTHOLIDAYS,
              name=u'lstHolidays', parent=self, pos=wx.Point(0, 34),
              size=wx.Size(241, 103),
              style=wx.LC_REPORT) #wx.LC_SORT_ASCENDING | 
        self.lstHolidays.SetMinSize(wx.Size(-1, -1))
        self._init_coll_lstHolidays_Columns(self.lstHolidays)
        self.lstHolidays.Bind(wx.EVT_LIST_COL_CLICK,
              self.OnLstHolidaysListColClick,
              id=wxID_VXMLNODEHOLIDAYSPANELLSTHOLIDAYS)
        self.lstHolidays.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstHolidaysListItemDeselected,
              id=wxID_VXMLNODEHOLIDAYSPANELLSTHOLIDAYS)
        self.lstHolidays.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstHolidaysListItemSelected,
              id=wxID_VXMLNODEHOLIDAYSPANELLSTHOLIDAYS)

        self.cbDel = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODEHOLIDAYSPANELCBDEL,
              bitmap=vtArt.getBitmap(vtArt.Del), name=u'cbDel', parent=self,
              pos=wx.Point(241, 34), size=wx.Size(31, 30), style=0)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VXMLNODEHOLIDAYSPANELCBDEL)

        self.lblName = wx.StaticText(id=wxID_VXMLNODEHOLIDAYSPANELLBLNAME,
              label=_(u'name'), name=u'lblName', parent=self, pos=wx.Point(0,
              137), size=wx.Size(80, 21), style=wx.ALIGN_RIGHT)
        self.lblName.SetMinSize(wx.Size(-1, -1))

        self.viName = vidarc.tool.input.vtInputText.vtInputText(id=wxID_VXMLNODEHOLIDAYSPANELVINAME,
              name=u'viName', parent=self, pos=wx.Point(84, 137),
              size=wx.Size(156, 21), style=0)

        self.cbAdd = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODEHOLIDAYSPANELCBADD,
              bitmap=vtArt.getBitmap(vtArt.Add), name=u'cbAdd', parent=self,
              pos=wx.Point(241, 158), size=wx.Size(31, 30), style=0)
        self.cbAdd.Bind(wx.EVT_BUTTON, self.OnCbAddButton,
              id=wxID_VXMLNODEHOLIDAYSPANELCBADD)

        self.viDate = vidarc.tool.time.vtTimeDateGM.vtTimeDateGM(id=wxID_VXMLNODEHOLIDAYSPANELVIDATE,
              name=u'viDate', parent=self, pos=wx.Point(84, 158),
              size=wx.Size(156, 30), style=0)

        self.lblDate = wx.StaticText(id=wxID_VXMLNODEHOLIDAYSPANELLBLDATE,
              label=_(u'date'), name=u'lblDate', parent=self, pos=wx.Point(0,
              158), size=wx.Size(80, 30), style=wx.ALIGN_RIGHT)
        self.lblDate.SetMinSize(wx.Size(-1, -1))

        self.rdbHours = wx.RadioBox(choices=[_(u'full day (00:00 - 24:00)'),
              _(u'first half day (00:00 - 12:00)'),
              _(u'second half day (12:00 - 24:00)')],
              id=wxID_VXMLNODEHOLIDAYSPANELRDBHOURS, label=_(u'hours'),
              majorDimension=1, name=u'rdbHours', parent=self, point=wx.Point(0,
              184), size=wx.Size(241, 86), style=wx.RA_SPECIFY_COLS)
        self.rdbHours.SetMinSize(wx.Size(-1, -1))

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vGlobals')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,lWidgets=[self.lstHolidays])
        self.dt=vtDateTime()
        
        self.viYear.SetTagName('year')
        self.viYear.SetEnableMark(False)
        self.viName.SetEnableMark(False)
        self.viDate.SetEnableMark(False)
        
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
        self.fgsData.Layout()
        self.fgsData.Fit(self)
        self.Clear()
    def __del__(self):
        vtXmlNodePanel.__del__(self)
    def __Clear__(self):
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
        try:
            # add code here
            self.selIdx=-1
            self.lstHolidays.DeleteAllItems()
            self.lHolidays=[]
            self.viYear.Clear()
            self.viDate.Clear()
            self.viName.Clear()
            self.rdbHours.SetSelection(0)
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def SetNetDocs(self,d):
        self.__logCritical__('FIXME'%())
        return
        # add code here
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viYear.SetDoc(doc)
        self.viName.SetDoc(doc)
        self.viDate.SetDoc(doc)
    def __showHolidays__(self):
        self.lstHolidays.DeleteAllItems()
        for tup in self.lHolidays:
            try:
                self.dt.SetDateStr(tup[0])
                idx=self.lstHolidays.InsertStringItem(sys.maxint,'%02d'%self.dt.GetMonth())
                self.lstHolidays.SetStringItem(idx,1,'%02d'%self.dt.GetDay())
                self.lstHolidays.SetStringItem(idx,2,tup[1])
                if tup[2]=='0':
                    self.lstHolidays.SetStringItem(idx,3,_(u'full day (00:00 - 24:00)'))
                elif tup[2]=='1':
                    self.lstHolidays.SetStringItem(idx,3,_(u'first half day (00:00 - 12:00)'))
                elif tup[2]=='2':
                    self.lstHolidays.SetStringItem(idx,3,_(u'second half day (12:00 - 24:00)'))
            except:
                self.__logTB__()
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viYear.SetNode(self.node)
            self.lHolidays=self.objRegNode.GetHolidays(self.node)
            self.__showHolidays__()
            #self.viDate.SetNode(self.node)
            #oTrans=self.doc.GetRegisteredNode(self.objRegNode.GetTagNameTransition())
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viYear.GetNode(node)
            #self.viDate.GetNode(node)
            self.objRegNode.SetHolidays(node,self.lHolidays)
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viDate.Close()
        self.viName.Close()
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            # add code here
            self.viYear.Enable(False)
            self.viName.Enable(False)
            self.viDate.Enable(False)
            self.cbAdd.Enable(False)
            self.cbDel.Enable(False)
            self.lstHolidays.Enable(False)
        else:
            # add code here
            self.viYear.Enable(True)
            self.viName.Enable(True)
            self.viDate.Enable(True)
            self.cbAdd.Enable(True)
            self.cbDel.Enable(True)
            self.lstHolidays.Enable(True)

    def OnLstHolidaysListColClick(self, event):
        event.Skip()

    def OnLstHolidaysListItemDeselected(self, event):
        self.selIdx=-1
        event.Skip()

    def OnLstHolidaysListItemSelected(self, event):
        self.selIdx=event.GetIndex()
        event.Skip()

    def OnCbDelButton(self, event):
        event.Skip()
        try:
            if self.selIdx>=0:
                self.lHolidays=self.lHolidays[:self.selIdx]+self.lHolidays[self.selIdx+1:]
                self.SetModified(True,self.lstHolidays)
                self.__showHolidays__()
        except:
            self.__logTB__()
    def OnCbAddButton(self, event):
        event.Skip()
        try:
            sKind='%d'%self.rdbHours.GetSelection()
            sDt=self.viDate.GetValueStr()
            sName=self.viName.GetValue()
            bDtFlt=False
            try:
                self.dt.SetDateStr(sDt)
                if int(self.viYear.GetValue())!=self.dt.GetYear():
                    self.viDate.Clear()
                    self.dt.Now()
                    self.viDate.SetValueStr(self.dt.GetDateStr())
                    bDtFlt=True
            except:
                bDtFlt=True
                self.__logTB__()
            if bDtFlt==True:
                dlg=wx.MessageDialog(self,_(u'Please enter valid date.') ,
                            _(u'vGlobals Holidays'),
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg.Destroy()
                return
            self.lHolidays.append((sDt,sName,sKind))
            self.SetModified(True,self.lstHolidays)
            self.__showHolidays__()
        except:
            self.__logTB__()
