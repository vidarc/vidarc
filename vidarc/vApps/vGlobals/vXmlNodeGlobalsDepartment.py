#----------------------------------------------------------------------------
# Name:         vXmlNodeGlobalsDepartment.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060727
# CVS-ID:       $Id: vXmlNodeGlobalsDepartment.py,v 1.1 2006/08/29 10:58:39 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.vApps.vGlobals.vXmlNodeGlobals import vXmlNodeGlobals

class vXmlNodeGlobalsDepartment(vXmlNodeGlobals):
    #NODE_ATTRS=[
            #('name',None,'name',None),
            #('abbreviation',None,'abbreviation',None),
            #('region',None,'region',None),
            #('distance',None,'distance',None),
            #('coor',None,'coor',None),
    #    ]
    def __init__(self,tagName='globalsDepartment'):
        vXmlNodeGlobals.__init__(self,tagName)
    def GetDescription(self):
        return _(u'department globals')
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x00\x83IDAT(\x91c<p\xe0\x00\x03)\x80\x85\x81\x81\x81$=,\x10J\xdd\xcf\x82\
\xa0R\xc9O\x1c\x07\x0e\x1c`\x81\xf3\xdb\xf7\xce\xc7TT\xe9\x9c\x88&\xc2\x84\
\xdfTLSpj\xd8\x1e\xb5\x12\xab\x1e,\x1a\xb6G\xad\x84\xa8\xc6\xaa\x07\x8b\x06\
\xcfe\xe1x\x1c\x89\xddI\x9e\xcb\xc2qi\xc3\xe7i\xacz\x08\x84\x12&@\xc4\x03r\
\x90\xa3\x85\x0c\xb2\x14v\x1b\x90U\xa0\xc5\x1d\x0b\x86b\xec\xea\xd05H~\xe2\
\xc0\xa5\x13\xbb\x06\xe2\x13,\x00\x8c\x1c(\x1eErT\xcc\x00\x00\x00\x00IEND\
\xaeB`\x82' 
