#----------------------------------------------------------------------------
# Name:         vXmlNodeGlobalsAttrTravelCost.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20071122
# CVS-ID:       $Id: vXmlNodeGlobalsAttrTravelCost.py,v 1.2 2010/03/03 02:17:12 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog

from vidarc.vApps.vGlobals.vXmlNodeGlobalsAttrML import vXmlNodeGlobalsAttrML

class vXmlNodeGlobalsAttrTravelCost(vXmlNodeGlobalsAttrML):
    def __init__(self,tagName='globAttrTravelCost',cfgBase=['cfg']):
        vXmlNodeGlobalsAttrML.__init__(self,tagName,cfgBase=cfgBase)
    def GetDescription(self):
        return _(u'travel cost global attributes')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00iIDAT8\x8d\xc5\x93]\x0e\xc0 \x08\x83\x07z\xc6\x1d\xd2;\xfa\xf3\xe4b\
\xb05,,Y\x1f\x8b\xc5\x0f\xa2"\x9a\xae\x884\x94\xfe\xa2Af\x85\xd2j\xb7\xde\
\xadI\\\x04(\xcc|\xb1K\\\x0f\xd9\x1bgm\xf5\xe9\x08\x08\x17y\x0f\x01\xc3v78a\
\x9e\xf4?\x01}H\x88\xa8\xb4\xda\xad\xbf\x11\xb0\xf0\x94%\x83\x04\x0c\xdf\xb5\
\x83\xb7\n\x7f\xa6\x01RM3\xf1\xf6\x82\xd9@\x00\x00\x00\x00IEND\xaeB`\x82'

