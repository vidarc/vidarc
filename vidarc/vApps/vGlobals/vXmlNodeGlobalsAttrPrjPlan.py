#----------------------------------------------------------------------------
# Name:         vXmlNodeGlobalsAttrPrjPlan.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060907
# CVS-ID:       $Id: vXmlNodeGlobalsAttrPrjPlan.py,v 1.2 2010/03/03 02:17:12 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog

from vidarc.vApps.vGlobals.vXmlNodeGlobalsAttrML import vXmlNodeGlobalsAttrML

class vXmlNodeGlobalsAttrPrjPlan(vXmlNodeGlobalsAttrML):
    ATTR_HIDDEN=['tag','name','description']
    def __init__(self,tagName='globAttrPrjPlan',cfgBase=['cfg']):
        vXmlNodeGlobalsAttrML.__init__(self,tagName,cfgBase=cfgBase)
    def GetDescription(self):
        return _(u'project planning global attributes')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xb4IDAT8\x8d\xadS1\x0e\xc20\x0c<\x87N\xfc\x84!<\x06E\xe2\x17\x9d\
\xbb\x11\xb6\x88\xb1\xef\xa8\xf8L\x07~\xc2\x1a\xa6\x82\x93\x9c#\x90\xb8)\xb1\
\x1d\x9f\xcf\xb1E\xdc\x0e5|\n\xb91\x02X\xa7Ej\x9b\xfb6\x90\xd9\x00@\xb6\n4\
\xeb\xe9y`\xb1\xb8\xef\x1fM\xc2\x81F\x02\x18\xc7X\xdc\xe79\xd2\xb8F\x82\xc5n\
\xf9\x06\x9fBf\xfa,F\r\x9fB\x96\xe3\xed\x9c\xad\xec\x9b\xe6\x9e\xaf\xe8\x81\
\xd6]Wp\x89\x9f\xfbU\x9d\xe97\xfe\x82\xa2\x82\x9en\xcd\xaa\xe1\x00{Hzx\xcf\
\x81~l\xe9\x04x\x7f\xd6i\x91\xff\xf6\xc0\xd2\xa9Yk\x08\xdbF\xa0\xddH\xabOT\
\x02[gk\xc5_\xfe]G\x01\xf4V#\xe5\x00\x00\x00\x00IEND\xaeB`\x82'  

