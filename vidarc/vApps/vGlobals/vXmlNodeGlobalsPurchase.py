#----------------------------------------------------------------------------
# Name:         vXmlNodeGlobalsPurchase.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060727
# CVS-ID:       $Id: vXmlNodeGlobalsPurchase.py,v 1.1 2006/08/29 10:58:39 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.vApps.vGlobals.vXmlNodeGlobals import vXmlNodeGlobals

class vXmlNodeGlobalsPurchase(vXmlNodeGlobals):
    #NODE_ATTRS=[
            #('name',None,'name',None),
            #('abbreviation',None,'abbreviation',None),
            #('region',None,'region',None),
            #('distance',None,'distance',None),
            #('coor',None,'coor',None),
    #    ]
    def __init__(self,tagName='globalsPurchase'):
        vXmlNodeGlobals.__init__(self,tagName)
    def GetDescription(self):
        return _(u'purchase globals')
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xaaIDAT8\x8d\xc5\x931\x0e\xc4 \x0c\x04\xd7\x1c\xdf\xe1^\x83\x94\x9f\
\xa04TiP~\x82\x94\xdf\xe4A\xa4\x88\x88\x90Y\xdf]w[\xae\xccz,\x1b\x11\xf7\x82\
V(\xb1M&\x80s=D{\xee\xd7B\xe6\x01\x80t\x02\xab\xab\xa5\x1e\xe8\xad\x82\x9c\
\xea\xe4m\xfb2yt\x04\xf6\xd8\xf2}(\xb1\x8d\xf3\x8dE\xbacN\x159\xd5\xc7\x0f%6\
y\xefK\xd3E\x16.\xd3\x13`a\x8fb\xa1\xff'\xf0\xc0\xbd\xd3M\x05}#8\xd7CB\x89\
\xcdY\x17\xc6\x88\xfa\x16\xc6\x10z\x89\x9f\xc6\xe9d\xbd1=$\x0b\x9fn\x81\xfdF\
M4v\xd4\xa2\x04\xeccY\x9f\xed\x02\xcc\x89Z2$\xae\xce\xe2\x00\x00\x00\x00IEND\
\xaeB`\x82" 
