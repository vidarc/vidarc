#Boa:FramePanel:vXmlNodeAreaPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeAreaPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060216
# CVS-ID:       $Id: vXmlNodeAreaPanel.py,v 1.3 2010/03/29 08:54:43 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputText
import vidarc.tool.input.vtInputTextML

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VXMLNODEAREAPANEL, wxID_VXMLNODEAREAPANELLBLNAME, 
 wxID_VXMLNODEAREAPANELLBLTAG, wxID_VXMLNODEAREAPANELVINAME, 
 wxID_VXMLNODEAREAPANELVITAG, 
] = [wx.NewId() for _init_ctrls in range(5)]

class vXmlNodeAreaPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblName, (1, 0), border=4,
              flag=wx.RIGHT | wx.EXPAND, span=(1, 1))
        parent.AddWindow(self.viTag, (0, 1), border=0, flag=wx.EXPAND, span=(1,
              2))
        parent.AddWindow(self.lblTag, (0, 0), border=4,
              flag=wx.RIGHT | wx.EXPAND, span=(1, 1))
        parent.AddWindow(self.viName, (1, 1), border=0, flag=wx.EXPAND, span=(1,
              2))

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=0, vgap=0)

        self._init_coll_gbsData_Items(self.gbsData)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEAREAPANEL,
              name=u'vXmlNodeAreaPanel', parent=prnt, pos=wx.Point(286, 253),
              size=wx.Size(222, 83), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(214, 56))

        self.lblTag = wx.StaticText(id=wxID_VXMLNODEAREAPANELLBLTAG,
              label=u'tag', name=u'lblTag', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(72, 30), style=wx.ALIGN_RIGHT)

        self.viName = vidarc.tool.input.vtInputTextML.vtInputTextML(id=wxID_VXMLNODEAREAPANELVINAME,
              name=u'viName', parent=self, pos=wx.Point(76, 30),
              size=wx.Size(130, 30), size_button=wx.Size(31, 30),
              size_text=wx.Size(100, 22), style=0)

        self.lblName = wx.StaticText(id=wxID_VXMLNODEAREAPANELLBLNAME,
              label=u'name', name=u'lblName', parent=self, pos=wx.Point(0, 30),
              size=wx.Size(72, 30), style=wx.ALIGN_RIGHT)

        self.viTag = vidarc.tool.input.vtInputText.vtInputText(id=wxID_VXMLNODEAREAPANELVITAG,
              name=u'viTag', parent=self, pos=wx.Point(76, 0), size=wx.Size(130,
              30), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vGlobals')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.bModified=False
        self.gbsData.AddGrowableCol(1)
        
        self.viTag.SetTagName('tag')
        self.viName.SetTagName('name')
        
        self.objRegNode=None
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def SetNetDocs(self,d):
        self.__logCritical__('FIXME'%())
        return
    def SetRegNode(self,obj):
        self.objRegNode=obj
    def SetModified(self,state):
        self.bModified=state    
    def GetModified(self):
        return self.bModified
    def __isModified__(self):
        if self.bModified:
            return True
        return False
    def __Clear__(self):
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
        try:
            self.viTag.Clear()
            self.viName.Clear()
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viTag.SetDoc(doc)
        self.viName.SetDoc(doc)
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viTag.SetNode(self.node)
            self.viName.SetNode(self.node)
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viTag.GetNode(node)
            self.viName.GetNode(node)
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            self.viTag.Enable(False)
            self.viName.Enable(False)
        else:
            self.viTag.Enable(True)
            self.viName.Enable(True)
