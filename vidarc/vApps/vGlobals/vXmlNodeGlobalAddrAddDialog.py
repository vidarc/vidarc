#Boa:Dialog:vXmlNodeGlobalAddrAddDialog
#----------------------------------------------------------------------------
# Name:         vXmlNodeGlobalAddrAddDialog.py
# Purpose:      
#               derived from vtXmlNodeAddrSingle.py
# Author:       Walter Obweger
#
# Created:      20070820
# CVS-ID:       $Id: vXmlNodeGlobalAddrAddDialog.py,v 1.2 2010/03/03 02:17:12 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

from vXmlNodeGlobalAddrPanel import vXmlNodeGlobalAddrPanel

import vidarc.tool.art.vtArt as vtArt
from vidarc.tool.xml.vtXmlNodeAddDialog import vtXmlNodeAddDialog
import vidarc.tool.lang.vtLgBase as vtLgBase

def create(parent):
    return vtXmlNodeAddrAddDialog(parent)

[wxID_VTXMLNODEADDRADDDIALOG, wxID_VTXMLNODEADDRADDDIALOGCBAPPLY, 
 wxID_VTXMLNODEADDRADDDIALOGCBCANCEL, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vXmlNodeGlobalAddrAddDialog(wx.Dialog,vtXmlNodeAddDialog):
    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsBt, (1, 0), border=4,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER, span=(1, 1))

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=0, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_gbsData_Items(self.gbsData)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VTXMLNODEADDRADDDIALOG,
              name=u'vtXmlNodeAddrAddDialog', parent=prnt, pos=wx.Point(221,
              78), size=wx.Size(474, 356), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vLoc Customer Add Dialog')
        self.SetClientSize(wx.Size(466, 329))

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLNODEADDRADDDIALOGCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'), name=u'cbApply',
              parent=self, pos=wx.Point(136, 296), size=wx.Size(76, 30),
              style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VTXMLNODEADDRADDDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VTXMLNODEADDRADDDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'), name=u'cbCancel',
              parent=self, pos=wx.Point(248, 296), size=wx.Size(76, 30),
              style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VTXMLNODEADDRADDDIALOGCBCANCEL)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vtXml')
        self._init_ctrls(parent)
        vtXmlNodeAddDialog.__init__(self)
        try:
            self.gbsData.AddGrowableCol(0)
            self.gbsData.AddGrowableRow(0)
            self.pn=vXmlNodeGlobalAddrPanel(self,id=wx.NewId(),pos=(4,4),size=(340,70),
                        style=0,name=u'pnAddr')
            self.gbsData.AddWindow(self.pn, (0, 0), border=4,
                  flag=wx.BOTTOM | wx.TOP | wx.LEFT | wx.RIGHT | wx.EXPAND, span=(1, 1))
            self.gbsData.Layout()
            self.gbsData.Fit(self)
        except:
            self.__logTB__()
