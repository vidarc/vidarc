#----------------------------------------------------------------------------
# Name:         vXmlGlobalsInputTreePercent.py
# Purpose:      input widget for globals xml
#               text and popup with tree
# Author:       Walter Obweger
#
# Created:      20060725
# CVS-ID:       $Id: vXmlGlobalsInputTreePercent.py,v 1.1 2006/08/29 10:58:39 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from vidarc.tool.input.vtInputMultiplePercentTree import vtInputMultiplePercentTree
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.input.vtInputTreeMLInternal import vtInputTreeMLInternal

class vtInputTreePrjsPercent(vtInputMultiplePercentTree):
    def __init__(self,*_args,**_kwargs):
        global _
        _=vtLgBase.assignPluginLang('vGlobals')
        try:
            _kwargs['columns']
        except:
            _kwargs['columns']=[(_(u'name'),-1),(_(u'longName'),-1),('id',-1)]
        try:
            _kwargs['show_attrs']
        except:
            _kwargs['show_attrs']=['name','longName','|id']
        apply(vtInputMultiplePercentTree.__init__,(self,) + _args,_kwargs)
        self.tagGrp='projects'
        self.tagName='project'
        self.tagNameInt='project'
        self.trClass=self.__createTree__
        self.trShow=self.__showNode__
        self.SetAppl('vPrj')
    def __createTree__(*args,**kwargs):
        tr=vtInputTreeMLInternal(**kwargs)
        tr.EnableLanguageMenu()
        return tr
    def __showNode__(self,doc,node,lang):
        return doc.getNodeText(node,self.tagNameInt)
