#----------------------------------------------------------------------------
# Name:         vXmlNodeFunction.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060719
# CVS-ID:       $Id: vXmlNodeFunction.py,v 1.3 2010/03/03 02:17:12 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.config.vcCust as vcCust
from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        from vidarc.vApps.vGlobals.vXmlNodeAreaPanel import vXmlNodeAreaPanel
        #from vidarc.vApps.vGlobals.vXmlNodeFuncEditDialog import *
        #from vidarc.vApps.vGlobals.vXmlNodeFuncAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeFunction(vtXmlNodeBase):
    NODE_ATTRS=[
        ('Tag',None,'tag',None),
        ('Name','langId','name','language'),
        ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    FUNCS_GET_4_TREE=['Tag','Name']
    FMT_GET_4_TREE=[('Tag',''),('Name','')]
    def __init__(self,tagName='function'):
        global _
        _=vtLgBase.assignPluginLang('vGlobals')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'function')
    # ---------------------------------------------------------
    # specific
    def GetTag(self,node):
        return self.Get(node,'tag')
    def GetName(self,node,lang=None):
        return self.GetML(node,'name',lang)
    def SetTag(self,node,val):
        self.Set(node,'tag',val)
    def SetName(self,node,val,lang=None):
        self.SetML(node,'name',val,lang)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        return [('tag',vtXmlFilterType.FILTER_TYPE_STRING),
            ('name',vtXmlFilterType.FILTER_TYPE_STRING),
            ]
    def GetTranslation(self,name):
        _(u'tag'),_(u'name')
        return _(name)
    def Is2Create(self):
        return False
    def IsSkip(self):
        return False
    def IsMultiple(self):
        return True
    def Is2Add(self):
        return True
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\x1aIDAT8\x8d\x95\x92\xb1J\x03A\x10\x86\xbf\xbd\x13D\xb0N\x15+;\xcf\
\xd2\xc2F\xec\xa2\xd8Z\xd8^:!>\x80e^\xc0\xd8\xc4\xc2"\xef\xe0\xd9i/)E\xc8VrM\
\xce\xc2`\x93\x80\xc4B.\xbf\xc5\x9d\xa0\xdc\xee\x91\x0c\x0c\xcb\xb0\xf3\x7f\
\xf3\xb3;\x98 \xc4\x97\xe3n[\x1a&\x1aw\xdb\xf2\xf5x\xc5\x1a&\x92$e#i\x98H\
\xd7\x1d\'$`\x99xK\xc9fs\xe7\xd5\x9aO\x93=\xdc\xb1U\x8a\x01\xecd\xea\xec3&\
\x08\xbd\x83\x17W\xe7\xcafs\xecd\xca\xc9\xed\xbdY\xc9\x01\x80Y\xbf\x81\x06\
\xd0\x00\x83{\x90\xf7\r\xd4\xcf\xc5\x01\xd0*\xeb\xcb\\K\x03\x14\xe7\x8a\x0e#\
z\xbb=\xf8\xaa\xf3\xe8sp\x06\xec\xfc\xa9[@\xd3\xed\xa2\x02P\x9c+jF\x00\x0c\
\x18\xd4\x8fw:\xd8\x03\xbb\xb0\xd8\x91u\n\x14\xffw\xe1\xfe\x85\xef\xe2\xb0\
\xcf%d\xc3\xef\xc0\xbb\x07\xea\x97\x93\xf6\x81\x0f \x05s\x11Vv\xc1\x0bx=*\
\x00\xe9g\x91\x00\x9d\x97\x15\x00\x00\x8b\xd3\\\xbf\x80\xe3\xa7\xaa\x18j\x16\
\t\xe0\xf1\x1d\xb67\x8b\xf4\xc5\x0f\xbe\xf1l\xc9\x08\x9f\xc4\x98\x00\x00\x00\
\x00IEND\xaeB`\x82' 

    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            #return vXmlNodeFuncEditDialog
            return {'name':self.__class__.__name__,
                    'sz':(360, 140),'pnName':'pnFunc',
                }
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            #return vXmlNodeFuncAddDialog
            return {'name':self.__class__.__name__,
                    'sz':(360, 140),'pnName':'pnFunc',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeAreaPanel
        else:
            return None

