#----------------------------------------------------------------------------
# Name:         vXmlGlobalsFunctionDistribute.py
# Purpose:      input widget for function distribute xml
#               text and popup with tree
# Author:       Walter Obweger
#
# Created:      20060809
# CVS-ID:       $Id: vXmlGlobalsFunctionDistribute.py,v 1.1 2006/08/29 10:58:39 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
#from vidarc.tool.input.vtInputTreeMLInternal import vtInputTreeMLInternal
from vidarc.tool.input.vtInputDistributeTree import vtInputDistributeTree
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.vApps.vGlobals.vXmlGlobalsTree import vXmlGlobalsTree
#from vidarc.tool.input.vtInputTreeMLInternal import vtInputTreeMLInternal

class vXmlGlobalsFunctionDistribute(vtInputDistributeTree):
    def __init__(self,*_args,**_kwargs):
        global _
        _=vtLgBase.assignPluginLang('vGlobals')
        if 'columns' not in _kwargs:
            _kwargs['columns']=[(_(u'tag'),-1),(_(u'name'),-1),('id',-1)]
        if 'show_attrs' not in _kwargs:
            _kwargs['show_attrs']=['tag','name','|id']
        _kwargs['col_value']=_(u'value')
        _kwargs['tag_value']='value'
        apply(vtInputDistributeTree.__init__,(self,) + _args,_kwargs)
        self.tagGrp='glbsFunctions'
        self.tagName='function'
        self.tagNameInt='function'
        self.trClass=self.__createTree__
        self.trShow=self.__showNode__
        self.SetAppl('vGlobals')
    def __createTree__(*args,**kwargs):
        tr=vXmlGlobalsTree(**kwargs)
        tr.SetTagNames2Base(['globalsFunc'])
        tr.SetValidInfo(['function'])
        return tr
        tr=vtInputTreeMLInternal(**kwargs)
        #tr.AddTreeCall('init','SetGrouping')
        tr.AddTreeCall('init','SetNodeInfos',['tag','name'])
        tr.AddTreeCall('init','SetGrouping',[],[('tag',''),('name','')])
        tr.SetTagNames2Base(['globalsdata','globalsFunc'])
        #tr.EnableLanguageMenu()
        return tr
    def __showNode__(self,doc,node,lang):
        return doc.getNodeText(node,self.tagNameInt)
