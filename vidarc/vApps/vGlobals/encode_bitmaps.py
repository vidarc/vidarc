#!/usr/bin/env python
#----------------------------------------------------------------------
#----------------------------------------------------------------------------
# Name:         encode_bitmaps.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060514
# CVS-ID:       $Id: encode_bitmaps.py,v 1.5 2006/08/29 10:06:25 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    #"   -u -i -n Application Tools01_16.png images.py",
#    "   -u -i -n Mondrian mondrian.ico images.py",
    "-u -i -n Globals           img/VidMod_Globals_16.png           images.py",
    "-a -u -n Globe             img/Globe01_16.png                  images.py",
    "-a -u -n Tag               img/Tag01_16.png                    images.py",
    "-a -u -n Holiday           img/Holiday01_16.png                images.py",
    "-a -u -n Application       img/VidMod_Globals_16.ico           images.py",
    "-a -u -n Plugin            img/VidMod_Globals_16.png           images.py",
    "-a -u -n Function          img/Function01_16.png               images.py",
    "-a -u -n Department        img/Area03_16.png                   images.py",
    "-a -u -n HourlyRate        img/HourlyRate01_16.png             images.py",
    
    "-u -i -n Splash img/splashGlobals01.png images_splash.py",
    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)

