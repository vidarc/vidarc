#----------------------------------------------------------------------------
# Name:         vXmlNodeHourlyRate.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060803
# CVS-ID:       $Id: vXmlNodeHourlyRate.py,v 1.4 2010/03/03 02:17:13 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeDatedValues import *
try:
    if vcCust.is2Import(__name__):
        from vXmlNodeHourlyRatePanel import *
        #from vXmlNodeXXXEditDialog import *
        #from vXmlNodeXXXAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeHourlyRate(vtXmlNodeDatedValues):
    NODE_ATTRS=[
            ('Rate',None,'rate',None),
            #('Name',None,'name',None),
        ]
    FUNCS_GET_SET_4_LST=[]
    def __init__(self,tagName='hourlyRate'):
        global _
        _=vtLgBase.assignPluginLang('vGlobals')
        vtXmlNodeDatedValues.__init__(self,tagName)
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        pass
    def GetDescription(self):
        return _(u'hourly rate')
    # ---------------------------------------------------------
    # specific
    def GetRate(self,node):
        if self.IsDatedNode(node)==False:
            return '0.0'
        return self.Get(node,'rate_value')
    #def GetName(self,node):
    #    return self.GetML(node,'name')
    def SetRate(self,node,val):
        if self.IsDatedNode(node):
            self.Set(node,'rate_value',val)
    #def SetName(self,node,val):
    #    self.SetML(node,'name',val)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02jIDAT8\x8d}\x92\xdfK\xd3a\x14\xc6?\xef\xbb\xc1$\x91B\xaf\x84\xfc1%\
\xb4\x8b\xa8\xc4\x0b5\xd3\xdc\xbc*\x02\xb7\xa1x\x93f\xfd\x01\xa2x;\'yaQ,$o\
\x04\x15\x93H\xc8\xd4Y\xe1\x9d\xfae\xaeD\xc5`\x84WbsJ\x12D\xb0\x91?.\xca\x9d\
.\xbenj\x89\x07\xce\xcd\xfb\x9cs\x9e\xf3>\xcfQJ[8-\xaa\xab\xbc\x02\x06\x00\
\x86\xd1\x85\xd2NuZ\x9d>\xb5\xfb0\xea=\xf5\x14\x16\x14\x9eUb\x0e\xa8\xae\xf2\
J~^\x93\x00HbF\xf2\xf3\x9aD\xab \xe3\x13\xe3D"\x11\x1c5\x8f\x90\x847\x85\xdf\
on\x12I\xcc\x08\x80\x82.\xf9\xf0\xae\x94\xc9\xc0$\xb3s\t<\xae,Z[[\xffc\n\x87\
\xc3\xf8\x9f\xfb\x89\xc5\xae\xe1vg\xb2\xfeu\x9d\xe1\x97#\nS\x83.\xf1\xb8=\
\x12\n\x86Di\x0b\xc9\xf4\xfb\xfd\xd2\xd3\xd3\x93z\x8b\xc7\xe2\xd2\xd1\xde!%\
\xd7\xdb\x05\x0cQ\xdar\xa4\x81\xdb\xe5\xe6\xe6\xadj\xe5\xeb4W\x05\xb0Z\xaddd\
d\x00P\xd1[&9}9x;\xbd\xd8\xed\xd1#Q\x95\xb6\xe0q{d+\xba\x95b\x1a\x1a\x1a\x92\
\x81\x81\x01\x19{3&J[(\x7fV.<A\xf0!\xe9\xbetIn\xad\xb4\x05-\t\xaf\xb8]nr\xed\
\xf9)\x9b\xb4\xd6h\xa5ihlT\x95/*d\xc1\xb6`\x02\x17a\xf7\xd7.W\xdb\xae\x88\
\xd2\xdd\n\xc0\x9al\x9a\x9b\xfd*5\x8e\x02\x05\xd0\xf2\xe0\xa1\x02(}Z"\xf3\
\xe9\xf3\x10\x03n\x03\xd9\xa6\xec\xd9\x91l\xc2|9\xfb\x0en\xf8\xcbe\xf9\xf72|\
\x07r\x81\xacC\xe02\x1c\\88y\x07\x00Iv\x80\xca\xc7\x15\x12\xfa\x13\x82\x03\
\xa0\x10\x88\x03\x11`\x1b\x9c+N\x06\x1b\x06Y\xf8\xf4Q\x00\xacJw\xab\x89\xc9:\
\xd9\x8clHR\x07\xfdC\xc3y\xe0\xce!\xf3\xa69\xc0\xf9\xd3\xc9l\x9b\xa1V/\xad\
\xca\xe2\xd2"\x13\xe3o%u\x07\xd3\xef\xa7S.\x14\xfb\x8a\x85\xcf\x08\xfb\x08\
\xdb\x08+Hmo\xad\x18FT\xe2\xb1\xb88j\x1c\x12\x08\x04L7\x92Mv{\x8b\x84\x82!\
\x89\xc7\xe2\xa2\xb4\x85\xb4\xbe4\xe1\x1b\xc2\x12\xe2\xecu\x8a\xd2\x16F_\x8f\
J\x7f\x7f\xbf\x04\x83\xc1\x14Y\xca\x85\x8d\xe8\x88\xba\xd7\x8cx\\\xa6Zkw\xd7\
(zUD\xd9\xb92\x86\xeb\x86\tf\x06eo\x7f\x0f\x9b\xcd\xc6\xce\xce\xce\x91\xda\
\xc7O\xd7\xfc\x8e!\xff\xbe\x1d\xcf\xa9\xa9\xa9\x13\xf8_\xff\xe1\x03\x9a:\x8a\
\xd9\xce\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        return None
    def GetAddDialogClass(self):
        return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeHourlyRatePanel
        else:
            return None
