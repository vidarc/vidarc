#----------------------------------------------------------------------------
# Name:         vXmlNodeGlobalsRoot.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060607
# CVS-ID:       $Id: vXmlNodeGlobalsRoot.py,v 1.5 2010/03/03 02:17:12 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeRoot import *
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeGlobalsRoot(vtXmlNodeRoot):
    def __init__(self,tagName='globalsdata'):
        global _
        _=vtLgBase.assignPluginLang('vGlobals')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'globals root')
    # ---------------------------------------------------------
    # inheritance
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x8aIDAT8\x8d\xc5S\xc1\t\x800\x10\xcb\xd5y\x14\xbaK\xa1\x8b\xf8\xe8\
\x08"]Dp\x17\x1f\x1d\xa8\xbe*R/\xd8\xe2\xc3\xbcJ\xb8\xa49\xc8\x89\x98\x015\
\xc6\xc5\xe5\x07\t \x85]j\xce\xb4\x0ej\x1c\x00HI\xc0~e(\x86j\x82\x1e|7\xe8\
\x8d~\xc7\xb8\xb8,\xd3\xea\xa9\xc11o\x00\x00\x1b=O\xf0&\xae\xdf\xcd\x06\xad\
\xa0\x06\xf7\xd8\xaf+\xb0\x92\xd8\xe8\xa9\xf8\xea\x01\x13\xb7 \x85]\xfe/\x92\
h\xd7\x08<o\x83\xad\xaa&\xd0\xda\xc9\x1a{\x02\x80\xb5-U\xfa\xca\x90L\x00\x00\
\x00\x00IEND\xaeB`\x82' 
