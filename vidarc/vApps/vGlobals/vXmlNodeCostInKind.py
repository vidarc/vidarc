#----------------------------------------------------------------------------
# Name:         vXmlNodeCostInKind.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060907
# CVS-ID:       $Id: vXmlNodeCostInKind.py,v 1.2 2010/03/03 02:17:12 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.lang.vtLgBase as vtLgBase
#import vidarc.config.vcCust as vcCust
from vidarc.tool.xml.vtXmlNodeTag import *
try:
    if vcCust.is2Import(__name__):
        #from vidarc.vApps.vGlobals.vXmlNodeAreaPanel import *
        #from vidarc.vApps.vGlobals.vXmlNodeFuncEditDialog import *
        #from vidarc.vApps.vGlobals.vXmlNodeFuncAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeCostInKind(vtXmlNodeTag):
    #NODE_ATTRS=[
    #    ('Tag',None,'tag',None),
    #    ('Name','langId','name','language'),
    #    ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    FUNCS_GET_4_TREE=['Tag','Name']
    FMT_GET_4_TREE=[('Tag',''),('Name','')]
    ATTR_HIDDEN=['tag','name','description']
    def __init__(self,tagName='costInKind'):
        global _
        _=vtLgBase.assignPluginLang('vGlobals')
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'cost inkind')
    # ---------------------------------------------------------
    # specific
    #def GetTag(self,node):
    #    return self.Get(node,'tag')
    #def GetName(self,node,lang=None):
    #    return self.GetML(node,'name',lang)
    #def SetTag(self,node,val):
    #    self.Set(node,'tag',val)
    #def SetName(self,node,val,lang=None):
    #    self.SetML(node,'name',val,lang)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    #def GetAttrFilterTypes(self):
    #    return [('tag',vtXmlFilterType.FILTER_TYPE_STRING),
    #        ('name',vtXmlFilterType.FILTER_TYPE_STRING),
    #        ]
    #def GetTranslation(self,name):
    #    _(u'tag'),_(u'name')
    #    return _(name)
    def Is2Create(self):
        return False
    def IsSkip(self):
        return False
    def IsMultiple(self):
        return True
    def Is2Add(self):
        return True
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01sIDAT8\x8d\xa5\x93\xcb+\x84Q\x18\x87\x9f\xf3}\xa3a4\xca\xa0i\\\xa2Q\
\xa2\\\x16\x16\x16S\x9aR\xd8\x08\xc5\xcer6\xcaZ\x16\x16\x83\x95"\x16\xfe\x02\
\x0b\xac\xdd\x12a\xc4\x90\t#\xd4\x18\x85\x84\xc8\xadq\x9fq\x99\xcf\x02\xe5\
\xf2\xcd|\xcaS\xef\xe6\x9c~O\xef{N\xaf\x10\x92\x8c\x16\xb33\xd3J\xa4;\x9df\
\xfa\x03\xbb\xdd\xfe?\x01\x80\xd3\xe9\xd4\x16\x94\x97\xd5*\xa5yFZ\xfb\xfa\
\x85\x9a\xa4\xad\xbd\xe3\xdb\xb9\xf4KPa\xa32?HcuU\xc4\xb9\xa3\n\x02\x81S\x9e\
\x9e\xc2\x14\x9b\xf6hi(\xd3\x94\xe8\x00,f\xabb\xb5&\x93h2\xe1\xf3o\xb0<\xb9K\
a\x8aBQ\xe6%Mu5Q%R^n\xa1\xe2p\xd4#\xc7\xe8\xc9H\xcf\xc2\x98`\xc1\x1f\x90\x98\
\xf3_s\x17|\xa1\xc4|\xcc@wg\x94\x0e\x04\xb8\x97\xdc<\xdc?"\xcb\x12( \x10\x84\
_\xc3\x18\x0cq\x04C!\x8e\x8eN"w\xe0\xf3m\x88\xcd\xadCb\xf5:\xf6\x0f\xf6\xb8\
\xbd=!;\xe1\x99\xca\xe2T^\x9f\x83\x0c\xaf\xc7\xd0\xdc\xd3\x1bY\x00p~v \x16\
\x16\x97\xc5\xd8\xd8\x84\xc8\xcd)\xa0\xab\xd1FF\xfc\x0bS\xdb!FVVU\xbf\xf3\
\x9b\xe0+\xc9Ii\\]^0\xb2e`h~\'j\xf8\xfd\r~080\xca\x92\xf1\x86q\xafW3\xac*\
\xf0\xac\xcd\x08\xcf_\x92\x9f\x08I\xd6,\x97\xcb\xa5\x00\xaa\xf5\xe7eR[$\x807\
\xea\xe0|3mD/4\x00\x00\x00\x00IEND\xaeB`\x82' 

    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return vtXmlNodeTag.GetEditDialogClass(self)
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return vtXmlNodeTag.GetAddDialogClass(self)
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vtXmlNodeTag.GetPanelClass(self)
        else:
            return None

