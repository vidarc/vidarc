#----------------------------------------------------------------------------
# Name:         vXmlNodeGlobalAddrSingle.py
# Purpose:      
#               derived from vtXmlNodeAddrSingle.py
# Author:       Walter Obweger
#
# Created:      20070820
# CVS-ID:       $Id: vXmlNodeGlobalAddrSingle.py,v 1.2 2010/03/03 02:17:12 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlNodeAddrSingle import *
from vidarc.tool.loc.vLocCountries import COUNTRIES
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

try:
    if vcCust.is2Import(__name__):
        from vidarc.vApps.vGlobals.vXmlNodeGlobalAddrPanel import vXmlNodeGlobalAddrPanel
        #from vidarc.vApps.vGlobals.vXmlNodeGlobalAddrEditDialog import *
        #from vidarc.vApps.vGlobals.vXmlNodeGlobalAddrAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0


class vXmlNodeGlobalAddrSingle(vtXmlNodeAddr):
    def Is2Create(self):
        return False
    def Is2Add(self):
        return False
    def IsMultiple(self):
        return False
    def IsImportExportBase(self):
        return False
    def IsSkip(self):
        return True
    def GetEditDialogClass(self):
        if GUI:
            #return vXmlNodeGlobalAddrEditDialog
            return {'name':self.__class__.__name__,
                    'sz':(466, 356),'pnName':'pnLoc',
                }
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            #return vXmlNodeGlobalAddrAddDialog
            return {'name':self.__class__.__name__,
                    'sz':(466, 356),'pnName':'pnLoc',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeGlobalAddrPanel
        else:
            return None

