#----------------------------------------------------------------------------
# Name:         vXmlNodeHolidays.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060526
# CVS-ID:       $Id: vXmlNodeHolidays.py,v 1.6 2010/03/03 02:17:13 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
from vidarc.tool.xml.vtXmlNodeTag import *
try:
    if vcCust.is2Import(__name__):
        from vidarc.vApps.vGlobals.vXmlNodeHolidaysPanel import vXmlNodeHolidaysPanel
        #from vidarc.vApps.vGlobals.vXmlNodeHolidaysEditDialog import *
        #from vidarc.vApps.vGlobals.vXmlNodeHolidaysAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeHolidays(vtXmlNodeTag):
    NODE_ATTRS=vtXmlNodeTag.NODE_ATTRS+[
            ('Year',None,'year',None),
        ]
    FUNCS_GET_SET_4_LST=vtXmlNodeTag.FUNCS_GET_SET_4_LST+['Year']
    def __init__(self,tagName='holidays'):
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'holidays')
    # ---------------------------------------------------------
    # specific
    def GetYear(self,node):
        return self.Get(node,'year')
    def __getHoliday__(self,node,*args):
        sTag=self.doc.getTagName(node)
        if sTag=='holiday':
            l=args[0][0]
            sName=self.Get(node,'name')
            sDt=self.Get(node,'date')
            sKind=self.Get(node,'hours')
            l.append((sDt,sName,sKind))
        return 0
    def GetHolidays(self,node):
        l=[]
        self.doc.procChilds(node,self.__getHoliday__,l)
        return l
    def GetHolidaysDict(self,node,d=None):
        l=sef.GetHolidays(node)
        return self.GetHolidaysDictByLst(l,d=d)
    def GetHolidaysDictByLst(self,l,d=None):
        if l is None:
            return None
        if d is None:
            d={}
        for t in l:
            try:
                sDate=t[0]
                iY,iM,iD=[int(k) for k in sDate.split('-')]
                if iY in d:
                    dY=d[iY]
                else:
                    dY={}
                    d[iY]=dY
                if iM in dY:
                    dM=dY[iM]
                else:
                    dM={}
                    dY[iM]=dM
                if iD in dM:
                    pass
                else:
                    dM[iD]=t
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        return d
    def SetYear(self,node,val):
        self.Set(node,'year',val)
    def SetHolidays(self,node,l):
        cc=self.doc.getChilds(node,'holiday')
        for c in cc:
            self.doc.deleteNode(c,node)
        for tup in l:
            self.doc.createSubNodeDict(node,{'tag':'holiday','lst':[
                {'tag':'date','val':tup[0]},
                {'tag':'name','val':tup[1]},
                {'tag':'hours','val':tup[2]},
                ]})
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        return [('tag',vtXmlFilterType.FILTER_TYPE_STRING),
            ('year',vtXmlFilterType.FILTER_TYPE_INT),
            ]
    def GetTranslation(self,name):
        _(u'tag'),_(u'year')
        return _(name)
    def Is2Create(self):
        return True
    def Is2Add(self):
        return True
    def IsMultiple(self):
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return False
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xf3IDAT8\x8d\x95\x92\xedm\x840\x0c\x86\x9f@\x17HF #\xb83t\x13\x96\
\xbaQ\xca\x08\xf7\xb1\x01\x8c\xe0L\x00\xee\x8f+\x08\x0e\xe7\xd4Z\x8a\x14+o\
\x1e\xbf\xb1\x13B\xd3\xe2\x85-\xb3\xed\xf3\xd0\xb4\xc1\xd35\xb5\xcb&\x82\xf5\
=&\x82:\xc0=\xf9\xb0\x003\x11\xb3\xeb\xf5\xb9\xfa\xde\x14L\xc1\x00{\xd5\x7fx\
\xd0r\xbf\x13/\x97g\xf2x\x00ps\xcbW\x1c\xac\x15Md\xdb\xd7\x1c\x04\xaf\x89\
\xb6\xcc\xf6\r|\xee*\x7f\xe17\xd2\x05\xac\x10U\xa5\x94B\xce\xf9\xffSP\xd5-\
\x1f\xc7\xb1:\x85\x13`\xbd\x1cct\x9d\xfd\xc9\x01@)\xe5\x90\xab\xaa\xeb\xe2\
\x00x\xb5\xfe\x0e\xb8\x86\xff\x0f~\xc5\xd34m\xfb\x18#\xaaJJ\xc9\xf6\r\xdd\
\x00\xb6\xcc6\x0c\x03\x00]\xd7QJAD\x0e0/\x0eO\x10\x11W\xb4B\xdf>!4mH)\x19\
\x80\xd9yb9\xe7M\xe7\x02\xf6\x87!\x84\x13\xa1\xf6\x91~\x00!y\x92Y\xee\xd8\
\xa5\xc1\x00\x00\x00\x00IEND\xaeB`\x82'  
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            #return vXmlNodeHolidaysEditDialog
            return {'name':self.__class__.__name__,
                    'sz':(400, 420),'pnName':'pnHolidays',
                }
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            #return vXmlNodeHolidaysAddDialog
            return {'name':self.__class__.__name__,
                    'sz':(400, 420),'pnName':'pnHolidays',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeHolidaysPanel
        else:
            return None

