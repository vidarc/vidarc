#----------------------------------------------------------------------------
# Name:         vXmlGlobals.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060220
# CVS-ID:       $Id: vXmlGlobals.py,v 1.20 2008/04/15 23:56:06 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog

from vidarc.tool.time.vtTime import vtDateTime

from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vidarc.vApps.vGlobals.vXmlNodeGlobalsRoot import vXmlNodeGlobalsRoot
from vidarc.vApps.vGlobals.vXmlNodeGlobals import vXmlNodeGlobals
from vidarc.vApps.vGlobals.vXmlNodeGlobalsAttrML import vXmlNodeGlobalsAttrML
from vidarc.vApps.vGlobals.vXmlNodeGlobalsContact import vXmlNodeGlobalsContact
from vidarc.vApps.vGlobals.vXmlNodeGlobalsAttrContact import vXmlNodeGlobalsAttrContact
from vidarc.vApps.vGlobals.vXmlNodeGlobalsCustomer import vXmlNodeGlobalsCustomer
from vidarc.vApps.vGlobals.vXmlNodeGlobalsAttrCustomer import vXmlNodeGlobalsAttrCustomer
from vidarc.vApps.vGlobals.vXmlNodeGlobalsHum import vXmlNodeGlobalsHum
from vidarc.vApps.vGlobals.vXmlNodeGlobalsAttrHum import vXmlNodeGlobalsAttrHum
from vidarc.vApps.vGlobals.vXmlNodeGlobalsLoc import vXmlNodeGlobalsLoc
from vidarc.vApps.vGlobals.vXmlNodeGlobalsAttrLoc import vXmlNodeGlobalsAttrLoc
from vidarc.vApps.vGlobals.vXmlNodeGlobalsPrjPlan import vXmlNodeGlobalsPrjPlan
from vidarc.vApps.vGlobals.vXmlNodeGlobalsAttrPrjPlan import vXmlNodeGlobalsAttrPrjPlan
from vidarc.vApps.vGlobals.vXmlNodeGlobalsCostPrjPlan import vXmlNodeGlobalsCostPrjPlan
from vidarc.vApps.vGlobals.vXmlNodeCost import vXmlNodeCost
from vidarc.vApps.vGlobals.vXmlNodeCostInKind import vXmlNodeCostInKind
from vidarc.vApps.vGlobals.vXmlNodeGlobalsPurchase import vXmlNodeGlobalsPurchase
from vidarc.vApps.vGlobals.vXmlNodeGlobalsAttrPurchase import vXmlNodeGlobalsAttrPurchase
from vidarc.vApps.vGlobals.vXmlNodeGlobalsArea import vXmlNodeGlobalsArea
from vidarc.vApps.vGlobals.vXmlNodeGlobalsTravel import vXmlNodeGlobalsTravel
from vidarc.vApps.vGlobals.vXmlNodeGlobalsAttrTravel import vXmlNodeGlobalsAttrTravel
from vidarc.vApps.vGlobals.vXmlNodeGlobalsTravelCost import vXmlNodeGlobalsTravelCost
from vidarc.vApps.vGlobals.vXmlNodeGlobalsAttrTravelCost import vXmlNodeGlobalsAttrTravelCost
from vidarc.vApps.vGlobals.vXmlNodeArea import vXmlNodeArea
from vidarc.vApps.vGlobals.vXmlNodeGlobalsDepartment import vXmlNodeGlobalsDepartment
from vidarc.vApps.vGlobals.vXmlNodeGlobalsFunction import vXmlNodeGlobalsFunction
from vidarc.vApps.vGlobals.vXmlNodeFunction import vXmlNodeFunction
from vidarc.vApps.vGlobals.vXmlNodeFunctionAttr import vXmlNodeFunctionAttr
from vidarc.vApps.vGlobals.vXmlNodeHourlyRate import vXmlNodeHourlyRate
from vidarc.vApps.vGlobals.vXmlNodeGlobalsVacation import vXmlNodeGlobalsVacation
from vidarc.vApps.vGlobals.vXmlNodeVacation import vXmlNodeVacation
from vidarc.vApps.vGlobals.vXmlNodeVacationAttr import vXmlNodeVacationAttr

#from vidarc.vApps.vGlobals.vXmlNodeGlobalsData import vXmlNodeGlobalsData

from vidarc.vApps.vGlobals.vXmlNodeHolidays import vXmlNodeHolidays

#from vidarc.vApps.vCustomer.vXmlNodeArea import vXmlNodeArea
#from vidarc.tool.xml.vtXmlNodeAddrSingle import vtXmlNodeAddrSingle
from vidarc.vApps.vGlobals.vXmlNodeGlobalAddrSingle import vXmlNodeGlobalAddrSingle

from vidarc.tool.xml.vtXmlNodeLog import vtXmlNodeLog
from vidarc.tool.xml.vtXmlNodeCfg import vtXmlNodeCfg
from vidarc.tool.xml.vtXmlNodeAttrCfgML import vtXmlNodeAttrCfgML
from vidarc.tool.xml.vtXmlNodeCalc import vtXmlNodeCalc

from vidarc.ext.xml.veXmlNodeEmail import veXmlNodeEmail
from vidarc.ext.xml.veXmlNodeAttrEmailML import veXmlNodeAttrEmailML

from vidarc.ext.xml.veXmlNodeSite import veXmlNodeSite
from vidarc.ext.xml.veXmlNodeSiteAttrML import veXmlNodeSiteAttrML

import traceback
VERBOSE=1

def getPluginData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x8aIDAT8\x8d\xc5S\xc1\t\x800\x10\xcb\xd5y\x14\xbaK\xa1\x8b\xf8\xe8\
\x08"]Dp\x17\x1f\x1d\xa8\xbe*R/\xd8\xe2\xc3\xbcJ\xb8\xa49\xc8\x89\x98\x015\
\xc6\xc5\xe5\x07\t \x85]j\xce\xb4\x0ej\x1c\x00HI\xc0~e(\x86j\x82\x1e|7\xe8\
\x8d~\xc7\xb8\xb8,\xd3\xea\xa9\xc11o\x00\x00\x1b=O\xf0&\xae\xdf\xcd\x06\xad\
\xa0\x06\xf7\xd8\xaf+\xb0\x92\xd8\xe8\xa9\xf8\xea\x01\x13\xb7 \x85]\xfe/\x92\
h\xd7\x08<o\x83\xad\xaa&\xd0\xda\xc9\x1a{\x02\x80\xb5-U\xfa\xca\x90L\x00\x00\
\x00\x00IEND\xaeB`\x82' 

class vXmlGlobals(vtXmlDomReg):
    TAGNAME_REFERENCE='tag'
    TAGNAME_ROOT='globalsroot'
    NODE_ATTRS=[
            #('name',None,'name',None),
            ]
    APPL_REF=['vHum']
    def __init__(self,attr='id',skip=[],synch=False,appl='vHum',verbose=0,
                    audit_trail=True):
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        try:
            vtXmlDomReg.__init__(self,attr=attr,skip=skip,synch=synch,
                        appl=appl,verbose=verbose,audit_trail=audit_trail)
            self.verbose=verbose
            
            oLog=vtXmlNodeLog()
            oCfg=vtXmlNodeCfg()
            oAttrCfgML=vtXmlNodeAttrCfgML()
            oCalc=vtXmlNodeCalc()
            self.RegisterNode(oLog,False)
            self.RegisterNode(oCfg,True)
            self.RegisterNode(oAttrCfgML,False)
            self.RegisterNode(oCalc,False)
            
            oRoot=vXmlNodeGlobalsRoot(tagName='root')
            self.RegisterNode(oRoot,False)
            oRoot=vXmlNodeGlobalsRoot()
            oGlobals=vXmlNodeGlobals()
            oGlobalsAttrML=vXmlNodeGlobalsAttrML()
            self.RegisterNode(oRoot,False)
            self.RegisterNode(oGlobals,False)
            self.RegisterNode(oGlobalsAttrML,False)
            self.LinkRegisteredNode(oRoot,oGlobals)
            self.LinkRegisteredNode(oGlobals,oGlobalsAttrML)
            self.LinkRegisteredNode(oGlobals,oLog)
            self.LinkRegisteredNode(oGlobals,oGlobals)
            
            oGlobalsArea=vXmlNodeGlobalsArea()
            oArea=vXmlNodeArea()
            self.RegisterNode(oGlobalsArea,True)
            self.RegisterNode(oArea,False)
            self.LinkRegisteredNode(oRoot,oGlobalsArea)
            self.LinkRegisteredNode(oGlobalsArea,oArea)
            self.LinkRegisteredNode(oArea,oArea)
            oGlobalsDepartment=vXmlNodeGlobalsDepartment()
            self.RegisterNode(oGlobalsDepartment,True)
            self.LinkRegisteredNode(oGlobalsDepartment,oArea)
            self.LinkRegisteredNode(oArea,oArea)
            
            oGlobalsFunction=vXmlNodeGlobalsFunction()
            oFunction=vXmlNodeFunction()
            oFunctionAttr=vXmlNodeFunctionAttr()
            oHourlyRate=vXmlNodeHourlyRate()
            self.RegisterNode(oGlobalsFunction,True)
            self.RegisterNode(oFunction,False)
            self.RegisterNode(oFunctionAttr,False)
            self.RegisterNode(oHourlyRate,False)
            self.LinkRegisteredNode(oRoot,oGlobalsFunction)
            self.LinkRegisteredNode(oGlobalsFunction,oFunction)
            self.LinkRegisteredNode(oFunction,oFunction)
            self.LinkRegisteredNode(oFunction,oHourlyRate)
            
            oGlobalsContact=vXmlNodeGlobalsContact()
            oGlobalsAttrContact=vXmlNodeGlobalsAttrContact()
            self.RegisterNode(oGlobalsContact,True)
            self.RegisterNode(oGlobalsAttrContact,False)
            self.LinkRegisteredNode(oRoot,oGlobalsContact)
            self.LinkRegisteredNode(oGlobalsContact,oGlobalsAttrContact)
            self.LinkRegisteredNode(oGlobalsContact,oLog)
            self.LinkRegisteredNode(oGlobalsContact,oGlobalsContact)
            
            oGlobalsCustomer=vXmlNodeGlobalsCustomer()
            oGlobalsAttrCustomer=vXmlNodeGlobalsAttrCustomer()
            self.RegisterNode(oGlobalsCustomer,True)
            self.RegisterNode(oGlobalsAttrCustomer,False)
            self.LinkRegisteredNode(oRoot,oGlobalsCustomer)
            self.LinkRegisteredNode(oGlobalsCustomer,oGlobalsAttrCustomer)
            self.LinkRegisteredNode(oGlobalsCustomer,oLog)
            self.LinkRegisteredNode(oGlobalsCustomer,oGlobalsCustomer)
            
            oGlobalsHum=vXmlNodeGlobalsHum()
            oGlobalsAttrHum=vXmlNodeGlobalsAttrHum()
            self.RegisterNode(oGlobalsHum,True)
            self.RegisterNode(oGlobalsAttrHum,False)
            self.LinkRegisteredNode(oRoot,oGlobalsHum)
            self.LinkRegisteredNode(oGlobalsHum,oGlobalsAttrHum)
            self.LinkRegisteredNode(oGlobalsHum,oLog)
            self.LinkRegisteredNode(oGlobalsHum,oGlobalsHum)
            
            oGlobalsLoc=vXmlNodeGlobalsLoc()
            oGlobalsAttrLoc=vXmlNodeGlobalsAttrLoc()
            self.RegisterNode(oGlobalsLoc,True)
            self.RegisterNode(oGlobalsAttrLoc,False)
            self.LinkRegisteredNode(oRoot,oGlobalsLoc)
            self.LinkRegisteredNode(oGlobalsLoc,oGlobalsAttrLoc)
            self.LinkRegisteredNode(oGlobalsLoc,oLog)
            self.LinkRegisteredNode(oGlobalsLoc,oGlobalsLoc)
            
            oGlobalsPrjPlan=vXmlNodeGlobalsPrjPlan()
            oPrjPlanAttr=vXmlNodeGlobalsAttrPrjPlan()
            oCost=vXmlNodeCost()
            oCostInKind=vXmlNodeCostInKind()
            oCostAttr=vXmlNodeGlobalsCostPrjPlan()
            self.RegisterNode(oGlobalsPrjPlan,True)
            self.RegisterNode(oPrjPlanAttr,False)
            self.RegisterNode(oCost,False)
            self.RegisterNode(oCostInKind,False)
            self.RegisterNode(oCostAttr,False)
            #self.LinkRegisteredNode(oGlobalsPrjPlan,oCostAttr)
            #self.LinkRegisteredNode(oGlobalsPrjPlan,oLog)
            self.LinkRegisteredNode(oRoot,oGlobalsPrjPlan)
            self.LinkRegisteredNode(oGlobalsPrjPlan,oCost)
            self.LinkRegisteredNode(oCost,oCost)
            self.LinkRegisteredNode(oGlobalsPrjPlan,oCostInKind)
            
            oGlobalsPurchase=vXmlNodeGlobalsPurchase()
            oGlobalsAttrPurchase=vXmlNodeGlobalsAttrPurchase()
            self.RegisterNode(oGlobalsPurchase,True)
            self.RegisterNode(oGlobalsAttrPurchase,False)
            self.LinkRegisteredNode(oRoot,oGlobalsPurchase)
            self.LinkRegisteredNode(oGlobalsPurchase,oGlobalsAttrPurchase)
            self.LinkRegisteredNode(oGlobalsPurchase,oLog)
            self.LinkRegisteredNode(oGlobalsPurchase,oGlobalsPurchase)
            
            oGlobalsTravel=vXmlNodeGlobalsTravel()
            oGlobalsAttrTravel=vXmlNodeGlobalsAttrTravel()
            self.RegisterNode(oGlobalsTravel,True)
            self.RegisterNode(oGlobalsAttrTravel,False)
            self.LinkRegisteredNode(oRoot,oGlobalsTravel)
            self.LinkRegisteredNode(oGlobalsTravel,oGlobalsAttrTravel)
            self.LinkRegisteredNode(oGlobalsTravel,oLog)
            self.LinkRegisteredNode(oGlobalsTravel,oGlobalsTravel)
            
            oGlobalsTravelCost=vXmlNodeGlobalsTravelCost()
            oGlobalsAttrTravelCost=vXmlNodeGlobalsAttrTravelCost()
            self.RegisterNode(oGlobalsTravelCost,True)
            self.RegisterNode(oGlobalsAttrTravelCost,False)
            self.LinkRegisteredNode(oRoot,oGlobalsTravelCost)
            self.LinkRegisteredNode(oGlobalsTravelCost,oGlobalsAttrTravelCost)
            self.LinkRegisteredNode(oGlobalsTravelCost,oLog)
            self.LinkRegisteredNode(oGlobalsTravelCost,oGlobalsTravelCost)
            
            oHolidays=vXmlNodeHolidays()
            self.RegisterNode(oHolidays,True)
            self.LinkRegisteredNode(oRoot,oHolidays)
            self.LinkRegisteredNode(oHolidays,oHolidays)
            
            oEmail=veXmlNodeEmail()
            oEmailAttr=veXmlNodeAttrEmailML()
            self.RegisterNode(oEmail,True)
            self.RegisterNode(oEmailAttr,False)
            self.LinkRegisteredNode(oRoot,oEmail)
            self.LinkRegisteredNode(oEmail,oEmailAttr)
            self.LinkRegisteredNode(oEmail,oLog)
            
            oSite=veXmlNodeSite()
            oSiteAttrML=veXmlNodeSiteAttrML()
            #oAddr=vtXmlNodeAddrSingle()
            oAddr=vXmlNodeGlobalAddrSingle()
            self.RegisterNode(oSite,True)
            self.RegisterNode(oSiteAttrML,False)
            self.RegisterNode(oAddr,False)
            self.LinkRegisteredNode(oRoot,oSite)
            self.LinkRegisteredNode(oSite,oSiteAttrML)
            self.LinkRegisteredNode(oSite,oAddr)
            self.LinkRegisteredNode(oSite,oLog)
            self.LinkRegisteredNode(oSite,oSite)
            
            oGlobalsVacation=vXmlNodeGlobalsVacation()
            oVacation=vXmlNodeVacation()
            oVacationAttr=vXmlNodeVacationAttr()
            self.RegisterNode(oGlobalsVacation,True)
            self.RegisterNode(oVacation,False)
            self.RegisterNode(oVacationAttr,False)
            self.LinkRegisteredNode(oRoot,oGlobalsVacation)
            self.LinkRegisteredNode(oGlobalsVacation,oVacation)
            #self.LinkRegisteredNode(oVacation,oVacation)
            #self.LinkRegisteredNode(oVacation,oHourlyRate)
            
            #oGlobalsData=vXmlNodeGlobalsData()
            #self.RegisterNode(oGlobalsData,False)
            #self.LinkRegisteredNode(oCfg,oArea)
            #self.LinkRegisteredNode(oArea,oArea)
            self.dHolidays={}
        except:
            traceback.print_exc()
        self.SetDftLanguages()
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        vtXmlDomReg.__del__(self)
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'globalsdata')
    def genIds(self,node=None,bAcquire=True):
        if node is None:
            self.dHolidays={}
        vtXmlDomReg.genIds(self,node,bAcquire=bAcquire)
    #def New(self,revision='1.0',root='globalsroot',bConnection=False):
    #    vtXmlDomReg.New(self,revision,root)
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elem=self.createSubNode(self.getRoot(),'globalsdata')
        if bConnection==False:
            self.CreateReq()
        #if bConnection==False:
        #    self.createSubNodeTextAttr(elem,'cfg','','id','')
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        #self.genIds()
        #self.AlignDoc()
    def __findGlobal__(self,node,*args,**kwargs):
        if self.getTagName(node)==kwargs['tagName']:
            if self.getNodeText(node,'tag')==kwargs['tag']:
                self.nodeFindGlobal=node
                return -1
        return 0
    def __setGlobal__(self,oGlb,node,*args,**kwargs):
        oGlb.SetTag(node,kwargs['tag'])
        oGlb.SetName(node,kwargs['tag'])
    def __setGlobalAttr__(self,oGlbAttr,node,*args,**kwargs):
        oGlbAttr.Set(node,kwargs['tag'],'')
    def GetGlobal(self,lst,tagName,tagNameAttr,tagNameChild=None,dft='',bAcquire=True):
        """
        inputs:
        lst             ... list of tagnames to destination node (e.g. ['vLoc'])
        tagName         ... XML-tag of destination node
        tagNameAttr     ... XML-tag of attribute
        tagNameChild    ... XML-tag of childs to walk (None -> first registered node of destination node)
        dft             ... fallback value
        return:
        value           ... attribute value
        """
        if self.verbose:
            vtLog.CallStack('')
        node=self.getBaseNode()
        if node is None:
            return ''
        if bAcquire:
            self.acquire('dom')
        try:
            oGlb=self.GetRegisteredNode(tagName)
            for sTag in lst:
                self.nodeFindGlobal=None
                self.procChildsKeys(node,self.__findGlobal__,tagName=tagName,tag=sTag)
                if self.nodeFindGlobal is not None:
                    if self.verbose:
                        print 'found'
                else:
                    self.nodeFindGlobal=oGlb.Create(node,
                            self.__setGlobal__,tag=sTag)
                    if self.verbose:
                        print self.nodeFindGlobal
                    #self.addNode(c)
                node=self.nodeFindGlobal
                if node is None:
                    return dft
            #oGlbAttr=self.GetRegisteredNode('globAttr')
            #c=self.getChild(node,'globAttr')
            if tagNameChild is None:
                oGlbAttr=self.GetRegisteredNode(oGlb.GetValidChild()[0])
                c=self.getChild(node,oGlbAttr.GetTagName())
                if c is None:
                    c=oGlbAttr.Create(node,
                                self.__setGlobalAttr__,tag=tagNameAttr)
            else:
                if tagNameChild=='':
                    c=node
                    oGlbAttr=self.GetRegByNode(node)
                else:
                    oGlbAttr=self.GetRegisteredNode(tagNameChild)
                    c=self.getChild(node,oGlbAttr.GetTagName())
                    if c is None:
                        c=oGlbAttr.Create(node,
                                    self.__setGlobalAttr__,tag=tagNameAttr)
            cAttr=self.getChild(c,tagNameAttr)
            if self.verbose:
                print cAttr
            if cAttr is None:
                if self.IsNodeAclOk(node,self.ACL_MSK_WRITE):      # 070820:wro check acl
                    try:
                        self.startEdit(c)
                    except:
                        pass
                    oGlbAttr.Set(c,tagNameAttr,dft)
                    try:
                        self.doEdit(c)
                        self.endEdit(c)
                    except:
                        pass
                return dft
            try:
                return self.getText(cAttr)
            except:
                return dft
        finally:
            if bAcquire:
                self.release('dom')
    def __getGlobalDict__(self,node,nodeBase,oGlbAttr,tagNameAttr,dft,d):
        if self.getTagName(node)==oGlbAttr.GetTagName():
            cAttr=self.getChild(node,tagNameAttr)
            if self.verbose:
                print cAttr
            if cAttr is None:
                if self.IsNodeAclOk(node,self.ACL_MSK_WRITE):      # 070820:wro check acl
                    try:
                        self.startEdit(node)
                    except:
                        pass
                    oGlbAttr.Set(node,tagNameAttr,dft)
                    cAttr=self.getChild(node,tagNameAttr)
                    try:
                        self.doEdit(node)
                        self.endEdit(node)
                    except:
                        pass
                    sVal=self.getText(cAttr)
                else:
                    sVal=dft
            else:
                sVal=self.getText(cAttr)
            d[self.getKeyNum(node)]=[self.GetTagNames(node,nodeBase),sVal]
            
        return 0
    def GetGlobalRecDict(self,lst,tagName,tagNameAttr,tagNameChild=None,dft='',bAcquire=True):
        """
        inputs:
        lst             ... list of tagnames to destination node (e.g. ['vLoc'])
        tagName         ... XML-tag of destination node
        tagNameAttr     ... XML-tag of attribute
        tagNameChild    ... XML-tag of childs to walk (None -> first registered node of destination node)
        dft             ... fallback value
        return:
        dictionary      ... {12:[tagNames to destination node,attribute value]}
        """
        if self.verbose:
            vtLog.CallStack('')
        node=self.getBaseNode()
        if node is None:
            return None
        if bAcquire:
            self.acquire('dom')
        try:
            oGlb=self.GetRegisteredNode(tagName)
            for sTag in lst:
                self.nodeFindGlobal=None
                self.procChildsKeys(node,self.__findGlobal__,tagName=tagName,tag=sTag)
                if self.nodeFindGlobal is not None:
                    if self.verbose:
                        print 'found'
                else:
                    self.nodeFindGlobal=oGlb.Create(node,
                            self.__setGlobal__,tag=sTag)
                    if self.verbose:
                        print self.nodeFindGlobal
                    #self.addNode(c)
                node=self.nodeFindGlobal
                if node is None:
                    return {}
            #oGlbAttr=self.GetRegisteredNode('globAttr')
            #c=self.getChild(node,'globAttr')
            d={}
            
            #vtLog.CallStack('')
            #print node
            #print oGlb.GetValidChild()[0]
            if tagNameChild is None:
                oGlbAttr=self.GetRegisteredNode(oGlb.GetValidChild()[0])
                self.procChildsKeysRec(100,node,self.__getGlobalDict__,node,
                                oGlbAttr,tagNameAttr,dft,d)
            else:
                if tagNameChild=='':
                    # has no sense
                    #c=node
                    pass
                else:
                    oGlbAttr=self.GetRegisteredNode(tagNameChild)
                    self.procChildsKeysRec(100,node,self.__getGlobalDict__,node,
                                oGlbAttr,tagNameAttr,dft,d)
            return d
        finally:
            if bAcquire:
                self.release('dom')
    def __getHolidays__(self,node,*args):
        if self.verbose:
            print args
        if self.getTagName(node)=='holidays':
            if self.getNodeText(node,'tag')==args[0][0]:
                self.nodeHolidays=node
                return -1
            else:
                self.nodeHolidays=node
        return 0
    def __getHolidaysYear__(self,node,*args):
        if self.getTagName(node)=='holidays':
            if self.getNodeText(node,'year')==args[0][0]:
                self.nodeHolidays=node
                return -1
        return 0
    def GetHolidays(self,name,iYear,bAcquire=True):
        node=self.getBaseNode()
        if node is None:
            return None
        try:
            if bAcquire:
                self.acquire('dom')
            oHoliday=self.GetRegisteredNode('holidays')
            self.nodeHolidays=None
            self.procChilds(node,self.__getHolidays__,name)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'holidays:%s;id:%s'%(name,
                            self.getKey(self.nodeHolidays)),self.appl)
            if self.nodeHolidays is None:
                return None
            node=self.nodeHolidays
            self.nodeHolidays=None
            self.procChilds(node,self.__getHolidaysYear__,'%04d'%iYear)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'holiday:%s;id:%s'%(name,
                            self.getKey(self.nodeHolidays)),self.appl)
            if self.nodeHolidays is None:
                return None
            return oHoliday.GetHolidays(self.nodeHolidays)
        finally:
            if bAcquire:
                self.release('dom')
    def GetHolidaysDict(self,name,iYear,d=None,bAcquire=True):
        l=self.GetHolidays(name,iYear,bAcquire=bAcquire)
        oHoliday=self.GetRegisteredNode('holidays')
        return oHoliday.GetHolidaysDictByLst(l,d=d)
    def GetHoliday(self,name,iYear,iMonth,iDay):
        if name in self.dHolidays:
            if iYear in self.dHolidays[name]:
                d=self.dHolidays[name][iYear]
                if d is None:
                    return None
                if iMonth in d:
                    dd=d[iMonth]
                    if iDay in dd:
                        return dd[iDay]
                return None
            else:
                dYear=self.dHolidays[name]
        else:
            dYear={}
            self.dHolidays[name]=dYear
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'holiday:%s date:%04d-%02d-%02d'%(name,iYear,iMonth,iDay),self.appl)
        #try:
        #    d=self.dHolidays[name][iYear]
        #except:
        try:
            l=self.GetHolidays(name,iYear)
            if l is None:
                dYear[iYear]=None
                return None
            #if not self.dHolidays.has_key(name):
            #    dYear={}
            #    self.dHolidays[name]=dYear
            #else:
            #    dYear=self.dHolidays[name]
            d={}
            dt=vtDateTime()
            dYear[iYear]=d
            for tup in l:
                dt.SetDateStr(tup[0])
                iMh=dt.GetMonth()
                iDy=dt.GetDay()
                if d.has_key(iMh):
                    d[iMh][iDy]=tup
                else:
                    d[iMh]={}
                    d[iMh][iDy]=tup
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'new holiday:%s year:%04d created;%s'%(name,iYear,vtLog.pformat(d)),self.appl)
                vtLog.vtLngCur(vtLog.DEBUG,'holidays cached;%s'%vtLog.pformat(self.dHolidays),self.appl)
            if iMonth in d:
                dd=d[iMonth]
                if iDay in dd:
                    return dd[iDay]
            return None
            return d[iMonth][iDay]
        except:
            return None
    def IsHoliday(self,name,iYear,iMonth,iDay):
        tup=self.GetHoliday(name,iYear,iMonth,iDay)
        if tup is None:
            return -1
        else:
            return int(tup[2])
