#----------------------------------------------------------------------------
# Name:         vXmlNodeGlobalAddrPanel.py
# Purpose:      
#               derived from vtXmlNodeAddrSingle.py
# Author:       Walter Obweger
#
# Created:      20070820
# CVS-ID:       $Id: vXmlNodeGlobalAddrPanel.py,v 1.3 2010/03/29 08:54:43 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel
    from vidarc.tool.xml.vtXmlNodeAddrPanel import vtXmlNodeAddrPanel
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vXmlNodeGlobalAddrPanel(vtXmlNodeAddrPanel):
    def __Clear__(self):
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
        try:
            #self.bBlock=True
            #self.SetModified(False)
            #if self.doc is not None and self.node is not None:
            #    self.doc.endEdit(self.node)
            self.txtName.SetValue('')
            self.txtStreet.SetValue('')
            self.txtCity.SetValue('')
            self.txtZip.SetValue('')
            #self.chcCountry.SetSelection(-1)
            try:
                docGlb=self.doc.GetNetDoc('vGlobals')
                sCountry=docGlb.GetGlobal(['vLoc','location'],'globalsLoc','country',bAcquire=False)
                self.chcCountry.SetStringSelection(COUNTRIES[sCountry])
            except:
                pass
            self.txtRegion.SetValue('')
            self.spnDist.SetValue(0)
            #wx.CallAfter(self.__clearBlock__)
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            self.txtName.SetValue(self.doc.getNodeText(self.node,'name'))
            self.txtStreet.SetValue(self.doc.getNodeText(self.node,'street'))
            self.txtCity.SetValue(self.doc.getNodeText(self.node,'city'))
            self.txtZip.SetValue(self.doc.getNodeText(self.node,'zip'))
            try:
                sCountry=self.doc.getNodeText(self.node,'country')
                self.chcCountry.SetStringSelection(COUNTRIES[sCountry])
            except:
                try:
                    docGlb=self.doc.GetNetDoc('vGlobals')
                    sCountry=docGlb.GetGlobal(['vLoc','location'],'globalsLoc','country')#,bAcquire=False)
                    self.chcCountry.SetStringSelection(COUNTRIES[sCountry])
                except:
                    pass
            self.txtRegion.SetValue(self.doc.getNodeText(self.node,'region'))
            try:
                iDist=long(self.doc.getNodeText(self.node,'distance'))
            except:
                iDist=0
            self.spnDist.SetValue(iDist)
        except:
            self.__logTB__()
        #wx.CallAfter(self.__clearBlock__)

