#!/usr/bin/env python
#Boa:PyApp:main
#----------------------------------------------------------------------------
# Name:         vGlobalsAppl.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060514
# CVS-ID:       $Id: vGlobalsAppl.py,v 1.5 2007/07/30 20:38:47 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os,sys,getopt,traceback
import wx

import vidarc.vApps.common.vSplashFrame as vSplashFrame
import vtLogDef
import vtLgBase

import images_splash
modules ={u'vGlobalsMainFrame': [0, '', u'vGlobalsMainFrame.py']}

class BoaApp(wx.App):
    def __init__(self,num,sAppl,cfgFN,fn,iLogLv,iSockPort):
        self.fn=fn
        self.cfgFN=cfgFN
        self.iLogLv=iLogLv
        self.iSockPort=iSockPort
        self.sAppl=sAppl
        wx.App.__init__(self,num)
    def OnInit(self):
        wx.InitAllImageHandlers()
        actionList=[]
        actionListPost=[]
        import __init__
        if __init__.VIDARC_IMPORT:
            d={'label':_(u'  import library ...'),
                'eval':'__import__("vidImp")'}
            actionList.append(d)
        else:
            d={'label':_(_(u'  import library ...')),
                'eval':'0'}
            actionList.append(d)
        d={'label':_(u'  import vGlobalsMain ...'),
            'eval':'__import__("vidarc.vApps.vGlobals.vGlobalsMainFrame")'}
        actionList.append(d)
        d={'label':_(u'  Create Globals ...'),
            'eval':'self.res[1].vApps.vGlobals.vGlobalsMainFrame'}
        actionList.append(d)
        d={'label':_(u'  Create Globals ...'),
            'eval':'getattr(self.res[2],"create")'}
        actionList.append(d)
        d={'label':_(u'  Create Globals ...'),
            'eval':'self.res[3](None)'}
        actionList.append(d)
        
        d={'label':_(u'  Finished.')}
        actionList.append(d)
        
        d={'label':_(u'  Open Config (%s)...')%self.cfgFN,
            'eval':'self.res[4].OpenCfgFile("%s")'%self.cfgFN}
        actionListPost.append(d)
        
        d={'label':_(u'  Open File (%s)...')%self.fn,
            'eval':'self.res[4].OpenFile("%s")'%self.fn}
        actionListPost.append(d)
        
        self.splash = vSplashFrame.create(None,'VIDARC Automation GmbH','Globals',
            images_splash.getSplashBitmap(),
            actionList,
            actionListPost,
            self.sAppl,
            self.iLogLv,
            self.iSockPort)
        vSplashFrame.EVT_SPLASH_ACTION(self.splash,self.OnSplashAction)
        vSplashFrame.EVT_SPLASH_PROCESSED(self.splash,self.OnSplashProcessed)
        vSplashFrame.EVT_SPLASH_FINISHED(self.splash,self.OnSplashFinished)
        vSplashFrame.EVT_SPLASH_ABORTED(self.splash,self.OnSplashAborted)
        self.splash.Show()
        
        return True
    def OnSplashAction(self,evt):
        self.splash.DoProcess()
        evt.Skip()
    def OnSplashProcessed(self,evt):
        #if evt.GetIdx()==4:
        #    self.main=self.splash.res[3]
        #    self.SetTopWindow(self.main)
        #    if self.fn is not None:
        #        self.main.OpenFile(self.fn)
        self.splash.DoAction()
        evt.Skip()
    def OnSplashFinished(self,evt):
        self.main=self.splash.res[4]
        self.SetTopWindow(self.main)
        self.main.Show()
        self.splash.Destroy()
        self.splash=None
        evt.Skip()
    def OnSplashAborted(self,evt):
        try:
            self.main=self.splash.res[4]
            self.main.Destroy()
        except:
            pass
        self.splash.Destroy()
        self.splash=None
        evt.Skip()

def showHelp():
    print _('help')
    print sys.version
    print "  ",_("valid flags:")
    print "  ",_("-c <filename>")
    print "  ",_("--config <filename>")
    print "        ",_("open the <filename> to configure application")
    print "  ",_("-f <filename>")
    print "  ",_("--file <filename>")
    print "        ",_("open the <filename> at start")
    print "  ","-h"
    print "  ",_("--help")
    print "        ",_("show this screen")
    print "  ","-l"
    print "  ",_("--lang <language id according ISO639>")
    print "  ",_("--log <level>")
    print "       ",_("0 = debug")
    print "       ",_("1 = information")
    print "       ",_("2 = waring")
    print "       ",_("3 = error")
    print "       ",_("4 = critical")
    print "       ",_("5 = fatal")
    
def main():
    optshort,optlong='l:f:c:h',['lang=','file=','config=','log=','help']
    vtLgBase.initAppl(optshort,optlong,'vGlobals')
        
    fn=None
    cfgFN='vGlobalsCfg.xml'
    iLogLv=vtLogDef.ERROR
    try:
        optlist , args = getopt.getopt(sys.argv[1:],optshort,optlong)
        for o in optlist:
            if o[0] in ['-h','--help']:
                showHelp()
                return
            if o[0] in ['--file','-f']:
                fn=o[1]
            if o[0] in ['--config','-c']:
                cfgFN=o[1]
            if o[0] in ['--log']:
                if o[1]=='0':
                    iLogLv=vtLogDef.DEBUG
                elif o[1]=='1':
                    iLogLv=vtLogDef.INFO
                elif o[1]=='2':
                    iLogLv=vtLogDef.WARN
                elif o[1]=='3':
                    iLogLv=vtLogDef.ERROR
                elif o[1]=='4':
                    iLogLv=vtLogDef.CRITICAL
                elif o[1]=='5':
                    iLogLv=vtLogDef.FATAL

    except:
        showHelp()
        traceback.print_exc()
    application = BoaApp(0,'vGlobals',cfgFN,fn,iLogLv,60090)
    application.MainLoop()

if __name__ == '__main__':
    main()
