#----------------------------------------------------------------------------
# Name:         vXmlNodeGlobals.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060514
# CVS-ID:       $Id: vXmlNodeGlobals.py,v 1.5 2006/08/29 10:06:25 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlNodeTag import *

class vXmlNodeGlobals(vtXmlNodeTag):
    #NODE_ATTRS=[
            #('name',None,'name',None),
            #('abbreviation',None,'abbreviation',None),
            #('region',None,'region',None),
            #('distance',None,'distance',None),
            #('coor',None,'coor',None),
    #    ]
    def __init__(self,tagName='globals'):
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'globals')
    def IsSkip(self):
        return False
    def Is2Create(self):
        return False
    def Is2Add(self):
        return True
    def IsId2Add(self):
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return False
    def IsNotContained(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x8fIDAT8\x8d\xadS\xc9\r\x80 \x10\x9cE\xdb\xf1a/&6\xc2\xc3\x12|\xd8\
\x08\x89\xbd\xf0\xa0 }a&\xe8n\x14\x99\x17\xd7\xce\xb1\x80\x88\xeb\xf0\x07\
\xbd\xb61\xac\xd3\xc1\xf3\xb4\xec\xf2t\xcei\x04\\\xa0\x15\x03\x80p\x84R\xd5"\
\x1f\xd6\xe9H\xcb.\xaa\x03\x0b,TE\xc0p%##\xfa\x80\xe8\xc3m\x9d{r9(I\xb8\xd0"\
i\x13\xa1\xb4\x05\x00\xe36?\x8e3\xb2\xe3\xaakd\xd1v\x11\xbe*\xe7\xb1X\x9f)G\
\xb2\x9e\xb2\xea\x80\xfba\xf5\xc6t\xf0\x06\'\x8c\xd36\x14\xacb\x08\xb8\x00\
\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
