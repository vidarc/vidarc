#----------------------------------------------------------------------------
# Name:         vXmlNodeGlobalsArea.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060719
# CVS-ID:       $Id: vXmlNodeGlobalsArea.py,v 1.1 2006/08/29 10:58:39 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.vApps.vGlobals.vXmlNodeGlobals import vXmlNodeGlobals

class vXmlNodeGlobalsArea(vXmlNodeGlobals):
    #NODE_ATTRS=[
            #('name',None,'name',None),
            #('abbreviation',None,'abbreviation',None),
            #('region',None,'region',None),
            #('distance',None,'distance',None),
            #('coor',None,'coor',None),
    #    ]
    def __init__(self,tagName='globalsArea'):
        vXmlNodeGlobals.__init__(self,tagName)
    def GetDescription(self):
        return _(u'area globals')
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x00kIDAT(\x91c\\~n\x07\x03)\x80\x89$\xd5\x0c\x0c\x0c,\x0c\x0c\x0c\xed{\xe7\
\xe3WT\xe9\x9cH\xbe\r\xd4\xd3p\xbex\xe5\xf9\xe2\x95\x106\xb2\x9b\xb1h@V\x8a\
\xa9\x07]\x03\\\x05&\x80\xe8\xa1\xcc\x0fx\x8c\x87\x03\x16d\x8eao8i6\x10\x03\
\x18\xd1\xd2\x12\x9eX\x87\xc47\xba\r\xc8\xa9\x00\xab8\xba\r\x98V\xa1\x19\x81\
S\x03.\x00\x00E\x11(:\x9b\xdde3\x00\x00\x00\x00IEND\xaeB`\x82' 
