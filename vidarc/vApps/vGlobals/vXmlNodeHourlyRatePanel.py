#Boa:FramePanel:vXmlNodeHourlyRatePanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeHourlyRatePanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060803
# CVS-ID:       $Id: vXmlNodeHourlyRatePanel.py,v 1.5 2010/03/29 08:54:43 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import vidarc.tool.input.vtInputFloat
import vidarc.tool.time.vtTimeDatedValues

import sys

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VXMLNODEHOURLYRATEPANEL, wxID_VXMLNODEHOURLYRATEPANELCBDEC, 
 wxID_VXMLNODEHOURLYRATEPANELCBINC, wxID_VXMLNODEHOURLYRATEPANELLBLDATED, 
 wxID_VXMLNODEHOURLYRATEPANELLBLPERCENTAGE, 
 wxID_VXMLNODEHOURLYRATEPANELLBLRATE, wxID_VXMLNODEHOURLYRATEPANELSPNPER, 
 wxID_VXMLNODEHOURLYRATEPANELVIDATED, wxID_VXMLNODEHOURLYRATEPANELVIRATE, 
] = [wx.NewId() for _init_ctrls in range(9)]

class vXmlNodeHourlyRatePanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=1
    def _init_coll_bxsRateVal_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viRate, 1, border=4, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.spnPer, 1, border=4, flag=wx.EXPAND | wx.LEFT)
        parent.AddWindow(self.lblPercentage, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.cbDec, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.cbInc, 0, border=4, flag=wx.LEFT)

    def _init_coll_bxsRate_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblRate, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsRateVal, 2, border=4, flag=wx.EXPAND | wx.LEFT)

    def _init_coll_bxsDated_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblDated, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viDated, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsDated, 0, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsRate, 0, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(3)
        parent.AddGrowableCol(0)
        parent.AddGrowableCol(1)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=0, rows=4, vgap=4)

        self.bxsDated = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsRate = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsRateVal = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsDated_Items(self.bxsDated)
        self._init_coll_bxsRate_Items(self.bxsRate)
        self._init_coll_bxsRateVal_Items(self.bxsRateVal)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEHOURLYRATEPANEL,
              name=u'vXmlNodeHourlyRatePanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(312, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 180))
        self.SetAutoLayout(True)

        self.viDated = vidarc.tool.time.vtTimeDatedValues.vtTimeDatedValues(id=wxID_VXMLNODEHOURLYRATEPANELVIDATED,
              name=u'viDated', parent=self, pos=wx.Point(105, 0),
              size=wx.Size(198, 24), style=0)

        self.lblDated = wx.StaticText(id=wxID_VXMLNODEHOURLYRATEPANELLBLDATED,
              label=_(u'valid by'), name=u'lblDated', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(101, 24), style=wx.ALIGN_RIGHT)
        self.lblDated.SetMinSize(wx.Size(-1, -1))

        self.lblRate = wx.StaticText(id=wxID_VXMLNODEHOURLYRATEPANELLBLRATE,
              label=_(u'hourly rate'), name=u'lblRate', parent=self,
              pos=wx.Point(0, 40), size=wx.Size(101, 30), style=wx.ALIGN_RIGHT)
        self.lblRate.SetMinSize(wx.Size(-1, -1))

        self.viRate = vidarc.tool.input.vtInputFloat.vtInputFloat(default=0.0,
              fraction_width=2, id=wxID_VXMLNODEHOURLYRATEPANELVIRATE,
              integer_width=4, maximum=1000, minimum=0.00, name=u'viRate',
              parent=self, pos=wx.Point(105, 40), size=wx.Size(52, 30),
              style=0)

        self.cbDec = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODEHOURLYRATEPANELCBDEC,
              bitmap=vtArt.getBitmap(vtArt.Down), name=u'cbDec', parent=self,
              pos=wx.Point(236, 40), size=wx.Size(31, 30), style=0)
        self.cbDec.Bind(wx.EVT_BUTTON, self.OnCbDecButton,
              id=wxID_VXMLNODEHOURLYRATEPANELCBDEC)

        self.cbInc = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODEHOURLYRATEPANELCBINC,
              bitmap=vtArt.getBitmap(vtArt.Up), name=u'cbInc', parent=self,
              pos=wx.Point(271, 40), size=wx.Size(31, 30), style=0)
        self.cbInc.Bind(wx.EVT_BUTTON, self.OnCbIncButton,
              id=wxID_VXMLNODEHOURLYRATEPANELCBINC)

        self.lblPercentage = wx.StaticText(id=wxID_VXMLNODEHOURLYRATEPANELLBLPERCENTAGE,
              label=u'%', name=u'lblPercentage', parent=self, pos=wx.Point(221,
              40), size=wx.Size(11, 13), style=wx.ALIGN_RIGHT)
        self.lblPercentage.SetMinSize(wx.Size(-1, -1))

        self.spnPer = vidarc.tool.input.vtInputFloat.vtInputFloat(default=0.0,
              fraction_width=2, id=wxID_VXMLNODEHOURLYRATEPANELSPNPER,
              integer_width=3, maximum=100.0, minimum=0.0, name=u'spnPer',
              parent=self, pos=wx.Point(169, 40), size=wx.Size(48, 30),
              style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vGlobals')
        self._init_ctrls(parent, id)
        
        self.spnPer.SetValue(5.0)
        self.spnPer.SetEnableMark(False)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        
        self.viDated.SetCB(self.__showDated__)
        self.viRate.SetTagName('rate_value')
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def __del__(self):
        vtXmlNodePanel.__del__(self)
    def SetModified(self,flag):
        self.viRate.SetModified(flag)
    def GetModified(self):
        if self.viRate.IsModified():
            return True
        return False
    def __clearDated__(self):
        self.viRate.Clear()
    def __enableDated__(self,flag):
        self.viRate.Enable(flag)
        self.spnPer.Enable(flag)
        self.cbDec.Enable(flag)
        self.cbInc.Enable(flag)
    def __Clear__(self):
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
        try:
            # add code here
            self.viDated.Clear()
            self.__clearDated__()
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def SetNetDocs(self,d):
        self.__logCritical__('FIXME'%())
        return
        if d.has_key('vHum'):
            dd=d['vHum']
            self.docHum=dd['doc']
        # add code here
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viDated.SetDoc(doc)
    def __showDated__(self,node,*args,**kwargs):
        if node is None:
            self.__clearDated__()
            self.__enableDated__(False)
        else:
            self.__enableDated__(True)
            self.viRate.SetNode(node,self.doc)
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viDated.SetNode(self.node)
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            nodeDated=self.viDated.GetNode(node)
            if nodeDated is not None:
                self.viRate.GetNode(nodeDated,self.doc)
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viDated.Close()
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            # add code here
            self.viDated.Enable(False)
            self.__enableDated__(False)
        else:
            # add code here
            self.viDated.Enable(True)
            self.viDated.SetNode(self.node)
            #self.__enableDated__(True)
    def OnCbIncButton(self, event):
        event.Skip()
        try:
            fVal=self.viRate.GetValue()
            iVal=self.spnPer.GetValue()
            fVal=fVal*(100.0+iVal)/100.0
            self.viRate.SetValue(fVal)
            self.viRate.SetModified(True)
        except:
            self.__logTB__()
    def OnCbDecButton(self, event):
        event.Skip()
        try:
            fVal=self.viRate.GetValue()
            iVal=self.spnPer.GetValue()
            fVal=fVal*(100.0-iVal)/100.0
            self.viRate.SetValue(fVal)
            self.viRate.SetModified(True)
        except:
            self.__logTB__()
