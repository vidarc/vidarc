#----------------------------------------------------------------------------
# Name:         vXmlNodeGlobalsAttrHum.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060703
# CVS-ID:       $Id: vXmlNodeGlobalsAttrHum.py,v 1.2 2010/03/03 02:17:12 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog

from vidarc.vApps.vGlobals.vXmlNodeGlobalsAttrML import vXmlNodeGlobalsAttrML

class vXmlNodeGlobalsAttrHum(vXmlNodeGlobalsAttrML):
    def __init__(self,tagName='globAttrHum',cfgBase=['cfg']):
        vXmlNodeGlobalsAttrML.__init__(self,tagName,cfgBase=cfgBase)
    def GetDescription(self):
        return _(u'human global attributes')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x98IDAT8\x8d\xadS\xb1\r\x800\x0cs\n\xaf0\xc2\x13}\x01\x89\x89?8\xa1\
\xe2\rV^\xe0\nF\x8e\x81\tT5qU\x04\x19\x9d\xd8u#G\xc4UH\xab\r\xfd\xa9@\x00\
\xfb\xb4J\x8a\xb9\xd2A\x0b\x03\x00\xb9\x1d\xb0WY\xdd\x82\xa6\x837\x95\x15\
\x18\xfd\x81\xd1\x1fy\x01f=&2\x916\xf4\xa7t\xf3\xa0\x04\x18a\xd9\x1a\xed\xa0\
\x94\xccz\xff/\xd1\xb2\x99\xeb9@\x87\xc4\x1aL\xb1'\x07,a1\x81\xb9\xda\xa7Uj\
\xea7C\x8c\xeb\xf3\x12\xc5\xbaF@\xdf\x06\xfb\xaa\xe9\xc0J'K\xec\x05\xafK8\
\xb4%M\x8aV\x00\x00\x00\x00IEND\xaeB`\x82"  

