#----------------------------------------------------------------------------
# Name:         vXmlNodeOfferRoot.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060620
# CVS-ID:       $Id: vXmlNodeOfferRoot.py,v 1.1 2006/08/29 10:58:39 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeRoot import *
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeOfferRoot(vtXmlNodeRoot):
    def __init__(self,tagName='offers'):
        global _
        _=vtLgBase.assignPluginLang('vOffer')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'offer root')
    # ---------------------------------------------------------
    # inheritance
    def getImageData(self):
        return  \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xaaIDAT8\x8d\xc5\x93\xb1\rC!\x0cD\xcf\x84u\xc8,\x14\x96\xb2\xcaoR\
\xd1\xa0\xccB\x91]\xfe@\xa4\x88\xf8"\xe6\x9c\xa4\xfbW\x9e\xcc\xf9Y6"\xe1\x02\
\xabT\xb5/&\x80}kb\xbd\xf0o!\xf3\x00@\x06\x81\xd7\xd5\xd3\x08\x8c^A\xcbe\xf1\
\xf4y_<:\x02{\xec\xf91U\xed\xf3|s\x91\xed\xd8rA\xcb\xe5\xf0S\xd5.\xd7\xc7\
\xad\xdb"\x0f\x97\xe9\x08\xf0\xb0g\xb1\xd0\xf3\t"\xf0\xde\xa9\x02\x1fA\xbf\
\x08\xf6\xadI\xaa\xda\x83wa\x8chla\x0e\xa1\x97\xf8m\x9cA6\x1a\xd3C\xf2\xf0\
\xe9\x16\xd8o\xb4DsG+J\xc0>\x96\xf7\xd9^\xf7\x04X\xae\x9fo\xba\xc6\x00\x00\
\x00\x00IEND\xaeB`\x82'
