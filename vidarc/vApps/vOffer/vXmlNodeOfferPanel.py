#Boa:FramePanel:vXmlNodeOfferPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeOfferPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060620
# CVS-ID:       $Id: vXmlNodeOfferPanel.py,v 1.3 2007/08/21 18:13:41 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.vApps.vContact.vXmlContactInputTreeCustomer
import vidarc.vApps.vContact.vXmlContactInputTreeContact
import vidarc.tool.input.vtInputTree
import vidarc.tool.input.vtInputMultipledValues
import vidarc.tool.input.vtInputUnique
import vidarc.ext.state.veStateInput
import vidarc.tool.input.vtInputText
import vidarc.tool.time.vtTimeDateGM
import vidarc.vApps.vHum.vXmlHumInputTree
import vidarc.vApps.vPrj.vXmlPrjInputTree

import sys

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

[wxID_VXMLNODEOFFERPANEL, wxID_VXMLNODEOFFERPANELLBLCURRENCY, 
 wxID_VXMLNODEOFFERPANELLBLCUST, wxID_VXMLNODEOFFERPANELLBLCUSTCONT, 
 wxID_VXMLNODEOFFERPANELLBLNR, wxID_VXMLNODEOFFERPANELLBLOFF, 
 wxID_VXMLNODEOFFERPANELLBLOFFDT, wxID_VXMLNODEOFFERPANELLBLPRJ, 
 wxID_VXMLNODEOFFERPANELLBLSTATE, wxID_VXMLNODEOFFERPANELLBLSUM, 
 wxID_VXMLNODEOFFERPANELLBLVALID, wxID_VXMLNODEOFFERPANELTXTSUM, 
 wxID_VXMLNODEOFFERPANELVICURRENCY, wxID_VXMLNODEOFFERPANELVICUSTCONT, 
 wxID_VXMLNODEOFFERPANELVICUSTOMER, wxID_VXMLNODEOFFERPANELVINR, 
 wxID_VXMLNODEOFFERPANELVIOFFERDT, wxID_VXMLNODEOFFERPANELVIOFFERING, 
 wxID_VXMLNODEOFFERPANELVIPRJ, wxID_VXMLNODEOFFERPANELVISTATE, 
 wxID_VXMLNODEOFFERPANELVIVALIDDT, 
] = [wx.NewId() for _init_ctrls in range(21)]

class vXmlNodeOfferPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPrj, (0, 0), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.viPrj, (0, 1), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.lblNr, (0, 2), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.viNr, (0, 3), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.lblCust, (1, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viCustomer, (1, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.lblCustCont, (1, 2), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viCustCont, (1, 3), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.lblOff, (2, 0), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.viOffering, (2, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.lblOffDt, (2, 2), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viOfferDt, (2, 3), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.lblValid, (2, 4), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viValidDt, (2, 5), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.lblSum, (3, 0), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.txtSum, (3, 1), border=0, flag=wx.EXPAND, span=(1,
              3))
        parent.AddWindow(self.lblCurrency, (3, 4), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viCurrency, (3, 5), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.lblState, (4, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viState, (4, 1), border=0, flag=wx.EXPAND,
              span=(1, 5))

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=4, vgap=4)

        self._init_coll_gbsData_Items(self.gbsData)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEOFFERPANEL,
              name=u'vXmlNodeOfferPanel', parent=prnt, pos=wx.Point(333, 130),
              size=wx.Size(538, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(530, 180))
        self.SetAutoLayout(True)

        self.lblPrj = wx.StaticText(id=wxID_VXMLNODEOFFERPANELLBLPRJ,
              label=_(u'project'), name=u'lblPrj', parent=self, pos=wx.Point(0,
              0), size=wx.Size(74, 24), style=wx.ALIGN_RIGHT)
        self.lblPrj.SetMinSize(wx.Size(-1, -1))

        self.viPrj = vidarc.vApps.vPrj.vXmlPrjInputTree.vXmlPrjInputTree(id=wxID_VXMLNODEOFFERPANELVIPRJ,
              name=u'viPrj', parent=self, pos=wx.Point(78, 0), size=wx.Size(90,
              24), style=0)
        self.viPrj.Bind(vidarc.tool.input.vtInputTree.vEVT_VTINPUT_TREE_CHANGED,
              self.OnViPrjVtinputTreeChanged, id=wxID_VXMLNODEOFFERPANELVIPRJ)

        self.lblNr = wx.StaticText(id=wxID_VXMLNODEOFFERPANELLBLNR,
              label=u'number', name=u'lblNr', parent=self, pos=wx.Point(172, 0),
              size=wx.Size(63, 24), style=wx.ALIGN_RIGHT)
        self.lblNr.SetMinSize(wx.Size(-1, -1))

        self.viNr = vidarc.tool.input.vtInputUnique.vtInputUnique(id=wxID_VXMLNODEOFFERPANELVINR,
              name=u'viNr', parent=self, pos=wx.Point(239, 0), size=wx.Size(146,
              24), style=0)

        self.lblCust = wx.StaticText(id=wxID_VXMLNODEOFFERPANELLBLCUST,
              label=u'department', name=u'lblCust', parent=self, pos=wx.Point(0,
              28), size=wx.Size(74, 24), style=wx.ALIGN_RIGHT)
        self.lblCust.SetMinSize(wx.Size(-1, -1))

        self.viCustomer = vidarc.vApps.vContact.vXmlContactInputTreeCustomer.vXmlContactInputTreeCustomer(id=wxID_VXMLNODEOFFERPANELVICUSTOMER,
              name=u'viCustomer', parent=self, pos=wx.Point(78, 28),
              size=wx.Size(90, 24), style=0)
        self.viCustomer.Bind(vidarc.tool.input.vtInputTree.vEVT_VTINPUT_TREE_CHANGED,
              self.OnViCustomerVtinputTreeChanged,
              id=wxID_VXMLNODEOFFERPANELVICUSTOMER)

        self.lblCustCont = wx.StaticText(id=wxID_VXMLNODEOFFERPANELLBLCUSTCONT,
              label=u'contact', name=u'lblCustCont', parent=self,
              pos=wx.Point(172, 28), size=wx.Size(63, 24),
              style=wx.ALIGN_RIGHT)
        self.lblCustCont.SetMinSize(wx.Size(-1, -1))

        self.viCustCont = vidarc.vApps.vContact.vXmlContactInputTreeContact.vXmlContactInputTreeContact(id=wxID_VXMLNODEOFFERPANELVICUSTCONT,
              name=u'viCustCont', parent=self, pos=wx.Point(239, 28),
              size=wx.Size(146, 24), style=0)

        self.lblOff = wx.StaticText(id=wxID_VXMLNODEOFFERPANELLBLOFF,
              label=_(u'offering person'), name=u'lblOff', parent=self,
              pos=wx.Point(0, 56), size=wx.Size(74, 24), style=wx.ALIGN_RIGHT)
        self.lblOff.SetMinSize(wx.Size(-1, -1))

        self.viOffering = vidarc.vApps.vHum.vXmlHumInputTree.vXmlHumInputTree(id=wxID_VXMLNODEOFFERPANELVIOFFERING,
              name=u'viOffering', parent=self, pos=wx.Point(78, 56),
              size=wx.Size(90, 24), style=0)

        self.lblOffDt = wx.StaticText(id=wxID_VXMLNODEOFFERPANELLBLOFFDT,
              label=_(u'offering date'), name=u'lblOffDt', parent=self,
              pos=wx.Point(172, 56), size=wx.Size(63, 24),
              style=wx.ALIGN_RIGHT)
        self.lblOffDt.SetMinSize(wx.Size(-1, -1))

        self.viOfferDt = vidarc.tool.time.vtTimeDateGM.vtTimeDateGM(id=wxID_VXMLNODEOFFERPANELVIOFFERDT,
              name=u'viOfferDt', parent=self, pos=wx.Point(239, 56),
              size=wx.Size(146, 24), style=0)

        self.lblValid = wx.StaticText(id=wxID_VXMLNODEOFFERPANELLBLVALID,
              label=_(u'valid until'), name=u'lblValid', parent=self,
              pos=wx.Point(389, 56), size=wx.Size(45, 24),
              style=wx.ALIGN_RIGHT)
        self.lblValid.SetMinSize(wx.Size(-1, -1))

        self.viValidDt = vidarc.tool.time.vtTimeDateGM.vtTimeDateGM(id=wxID_VXMLNODEOFFERPANELVIVALIDDT,
              name=u'viValidDt', parent=self, pos=wx.Point(438, 56),
              size=wx.Size(100, 24), style=0)

        self.lblSum = wx.StaticText(id=wxID_VXMLNODEOFFERPANELLBLSUM,
              label=_(u'sum'), name=u'lblSum', parent=self, pos=wx.Point(0, 84),
              size=wx.Size(74, 21), style=wx.ALIGN_RIGHT)
        self.lblSum.SetMinSize(wx.Size(-1, -1))

        self.txtSum = wx.TextCtrl(id=wxID_VXMLNODEOFFERPANELTXTSUM,
              name=u'txtSum', parent=self, pos=wx.Point(78, 84),
              size=wx.Size(307, 21), style=wx.TE_READONLY | wx.TE_RIGHT,
              value=u'')
        self.txtSum.Enable(False)
        self.txtSum.SetMinSize(wx.Size(-1, -1))

        self.lblCurrency = wx.StaticText(id=wxID_VXMLNODEOFFERPANELLBLCURRENCY,
              label=_(u'currency'), name=u'lblCurrency', parent=self,
              pos=wx.Point(389, 84), size=wx.Size(45, 21),
              style=wx.ALIGN_RIGHT)
        self.lblCurrency.SetMinSize(wx.Size(-1, -1))

        self.viCurrency = vidarc.tool.input.vtInputText.vtInputText(id=wxID_VXMLNODEOFFERPANELVICURRENCY,
              name=u'viCurrency', parent=self, pos=wx.Point(438, 84),
              size=wx.Size(100, 21), style=0)

        self.lblState = wx.StaticText(id=wxID_VXMLNODEOFFERPANELLBLSTATE,
              label=_(u'state'), name=u'lblState', parent=self, pos=wx.Point(0,
              109), size=wx.Size(74, 24), style=wx.ALIGN_RIGHT)
        self.lblState.SetMinSize(wx.Size(-1, -1))

        self.viState = vidarc.ext.state.veStateInput.veStateInput(id=wxID_VXMLNODEOFFERPANELVISTATE,
              name=u'viState', parent=self, pos=wx.Point(78, 109),
              size=wx.Size(460, 24), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vOffer')
        self._init_ctrls(parent, id)
        self.bSupFirst=False
        
        vtXmlNodePanel.__init__(self,applName=_(u'vOffer')+' '+_(u'offer'),
                lWidgets=[],
                lEvent=[])
        self.viCustomer.SetTagNames('customer','tag')
        self.viCustCont.SetTagNames('contact','tag')
        self.viOffering.SetTagNames('offeredBy','name')
        self.viOfferDt.SetTagName('offerDt')
        self.viValidDt.SetTagName('validDt')
        self.viCurrency.SetTagName('currency')
        self.viNr.SetTagName('offerID')
        self.viState.SetNodeAttr('offer','offState')

        self.gbsData.AddGrowableCol(0)
        self.gbsData.AddGrowableCol(1)
        self.gbsData.AddGrowableCol(2)
        self.gbsData.AddGrowableCol(3)
        self.gbsData.AddGrowableCol(4)
        self.gbsData.AddGrowableCol(5)
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def Clear(self):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.Clear(self)
        
        # add code here
        self.bSupFirst=False
        self.viPrj.Clear()
        self.viNr.Clear()
        self.viCustomer.Clear()
        self.viCustCont.Clear()
        self.viOffering.Clear()
        self.viOfferDt.Clear()
        self.viValidDt.Clear()
        self.viCurrency.Clear()
        self.viState.Clear()
        try:
            docGlb=self.doc.GetNetDoc('vGlobals')
            sCur=docGlb.GetGlobal(['vOffer','offer'],'globalsOffer','currency',bAcquire=False)
            self.viCurrency.SetValue(sCur)
        except:
            self.viCurrency.SetValue('')
        
        self.ClrBlockDelayed()
    def SetNetDocs(self,d):
        # add code here
        if d.has_key('vHum'):
            dd=d['vHum']
            self.viOffering.SetDocTree(dd['doc'],dd['bNet'])
        if d.has_key('vContact'):
            dd=d['vContact']
            self.viCustomer.SetDocTree(dd['doc'],dd['bNet'])
            self.viCustCont.SetDocTree(dd['doc'],dd['bNet'])
        if d.has_key('vPrj'):
            dd=d['vPrj']
            self.viPrj.SetDocTree(dd['doc'],dd['bNet'])
    def SetDependentPartsPanel(self,pn):
        pn.BindPartsChanged(self.OnPartsChanged)
    def OnPartsChanged(self,evt):
        evt.Skip()
        self.viState.SetNode(self.node)
        self.viState.Enable(False)
        pn=self.GetParent().GetPanelByName('offerParts')
        #print pn.GetSum()
        self.txtSum.SetValue(pn.GetSum())
    def GetModified(self):
        if vtXmlNodePanel.GetModified(self):
            return True
        if self.viPrj.IsModified():
            return True
        if self.viCustCont.IsModified():
            return True
        if self.viOffering.IsModified():
            return True
        if self.viOfferDt.IsModified():
            return True
        if self.viValidDt.IsModified():
            return True
        if self.viCurrency.IsModified():
            return True
        if self.viNr.IsModified():
            return True
        #if self.viState.IsModified():
        #    return True
        return False
    def SetDoc(self,doc,bNet=False):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        
        # add code here
        self.viPrj.SetDoc(self.doc)
        self.viCustomer.SetDoc(self.doc)
        self.viCustCont.SetDoc(self.doc)
        self.viOffering.SetDoc(self.doc)
        self.viOfferDt.SetDoc(self.doc)
        self.viValidDt.SetDoc(self.doc)
        self.viCurrency.SetDoc(self.doc)
        self.viNr.SetDoc(self.doc)
        self.viNr.SetUniqueFunc(self.doc.GetSortedOfferNrIds)
        self.viState.SetDoc(self.doc)
        
    def SetNode(self,node):
        try:
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            
            # add code here
            self.viPrj.SetNode(self.node)
            custFid=self.objRegNode.GetCustomer(self.node)
            id,appl=self.doc.convForeign(custFid)
            #self.viCustomer.SetValueID('',id)
            netCust,nodeCust=self.doc.GetNode(custFid)
            self.viCustomer.SetNodeTree(nodeCust)
            self.viCustomer.SetNode(self.node)
            self.viCustCont.SetNodeTree(nodeCust)
            self.viCustCont.SetNode(self.node)
            
            self.viOffering.SetNode(self.node)
            self.viOfferDt.SetNode(self.node)
            self.viValidDt.SetNode(self.node)
            self.viCurrency.SetNode(self.node)
            self.viNr.SetNode(self.node)
            self.viState.SetNode(self.node)
            self.txtSum.SetValue(self.objRegNode.GetSum(self.node))
            
            sCur=self.viCurrency.GetValue()
            if len(sCur)==0:
                try:
                    docGlb=self.doc.GetNetDoc('vGlobals')
                    sCur=docGlb.GetGlobal(['vOffer','offer'],'globalsOffer','currency',bAcquire=False)
                    self.viCurrency.SetValue(sCur)
                    self.viCurrency.__markModified__()
                except:
                    pass
        except:
            vtLog.vtLngTB(self.GetName())
        
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
                print node
            if node is None:
                return
            # add code here
            self.viPrj.GetNode(node)
            self.viCustomer.GetNode(node)
            self.viCustCont.GetNode(node)
            #self.objRegNode.SetCustomerContact(node,self.viCustCont.GetForeignID())
            
            self.viOffering.GetNode(node)
            
            self.viOfferDt.GetNode(node)
            self.viValidDt.GetNode(node)
            self.viCurrency.GetNode(node)
            self.viNr.GetNode(node)
            self.viState.GetNode(node)
            self.viState.Enable(True)
            self.viState.SetNode(node)      # Refresh
            
            self.GetNodeFin(node)
        except:
            vtLog.vtLngTB(self.GetName())
    def Close(self):
        # add code here
        self.viPrj.Close()
        self.viCustomer.Close()
        self.viCustCont.Close
        self.viOffering.Close()
        self.viOfferDt.Close()
        self.viValidDt.Close()
        self.viCurrency.Close()
        self.viState.Close()
    def Cancel(self):
        # add code here
        self.SetNode(self.node)
    def Lock(self,flag):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if flag:
            # add code here
            self.viPrj.Enable(False)
            self.viNr.Enable(False)
            self.viCustomer.Enable(False)
            self.viCustCont.Enable(False)
            self.viOffering.Enable(False)
            self.viOfferDt.Enable(False)
            self.viValidDt.Enable(False)
            self.viCurrency.Enable(False)
            self.viNr.Enable(False)
            self.viState.Enable(False)
        else:
            # add code here
            self.viPrj.Enable(True)
            self.viNr.Enable(True)
            self.viCustomer.Enable(True)
            self.viCustCont.Enable(True)
            self.viOffering.Enable(True)
            self.viOfferDt.Enable(True)
            self.viValidDt.Enable(True)
            self.viCurrency.Enable(True)
            self.viNr.Enable(True)
            self.viState.Enable(True)

    def OnViCustomerVtinputTreeChanged(self, event):
        event.Skip()
        fid=self.viCustomer.GetForeignID()
        netCont,nodeCust=self.doc.GetNode(fid)
        if nodeCust is None:
            custFid=self.objRegNode.GetCustomer(self.node)
            id,appl=self.doc.convForeign(custFid)
            #self.viCustomer.SetValueID('',id)
            netCust,nodeCust=self.doc.GetNode(custFid)
            
        self.viCustCont.SetNodeTree(nodeCust)
    def __refreshContact__(self,fid):
        self.viCustomer.Clear()
        self.viCustCont.Clear()
        netPrj,nodePrj=self.doc.GetNode(fid)
        if nodePrj is not None:
            oPrj=netPrj.GetRegByNode(nodePrj)
            if oPrj is not None:
                cltFid=oPrj.GetClient(nodePrj)
                netCust,nodeCust=self.doc.GetNode(cltFid)
                #print nodeCust
                if nodeCust is not None:
                    self.viCustomer.SetNodeTree(nodeCust)
                    self.viCustCont.SetNodeTree(nodeCust)
                    return
            #self.viCustomer.Enable(True)
    def OnViPrjVtinputTreeChanged(self, event):
        event.Skip()
        fid=self.viPrj.GetForeignID()
        prjFid=self.objRegNode.GetProject(self.node)
        if fid!=prjFid:
            self.viCustomer.Clear()
            self.viCustCont.Clear()
            #self.__refreshContact__(fid)
