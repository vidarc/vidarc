#----------------------------------------------------------------------------
# Name:         vXmlNodeOfferParts.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060815
# CVS-ID:       $Id: vXmlNodeOfferParts.py,v 1.3 2008/02/02 16:10:54 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        from vXmlNodeOfferPartsPanel import *
        #from vXmlNodeOfferPartsEditDialog import *
        #from vXmlNodeOfferPartsAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeOfferParts(vtXmlNodeBase):
    NODE_ATTRS=[
            ('Sum',None,'sum',None),
    #        ('Count',None,'Count',None),
        ]
    #FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='offerParts'):
        global _
        _=vtLgBase.assignPluginLang('vOffer')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'offer parts')
    # ---------------------------------------------------------
    # specific
    def GetSetup(self):
        return [
            {'tag':'count'      ,'trans':'count','label':_('count'),'typedef':{'type':'int','min':'0','max':'100000'},'is2Report':1,'isCurrency':0,'stretch':0.1},
            {'tag':'part'       ,'trans':'part','label':_('part'),'typedef':{'type':'string'},'is2Report':1,'isCurrency':0,'stretch':0.4},
            #{'tag':'estimated'  ,'trans':'estimated','label':_('estimated'),'typedef':{'type':'bool',},'is2Report':0,'isCurrency':0},
            {'tag':'price'      ,'trans':'price','label':_('price'),'typedef':{'type':'float','digits':'4','comma':'2','fmt':'%4.2f','min':'0','max':'10000000'},'is2Report':1,'isCurrency':1,'stretch':0.2},
            {'tag':'pricesum'   ,'trans':'sum price','label':_('sum price'),'typedef':{'type':'float','digits':'4','comma':'2','fmt':'%4.2f','min':'0','max':'10'},'readonly':1,'calc':'#0# * #2#','total':1,'is2Report':1,'isCurrency':1,'stretch':0.2},
            ]
    def GetLaTexTableFmt(self):
        return '{|@{\\extracolsep\\fill}r|r|p{0.5\\linewidth}|r|r|}'
    def GetValues(self,node):
        try:
            lVal=[]
            if node is None:
                return lVal
            lSetup=self.GetSetup()
            lMap=[d['tag'] for d in lSetup]
            
            nodeData=self.doc.getChild(node,'data')
            if nodeData is not None:
                for cRow in self.doc.getChilds(nodeData,'row'):
                    lCol=['' for i in lMap]
                    for cCol in self.doc.getChilds(cRow):
                        sTag=self.doc.getTagName(cCol)
                        if sTag in lMap:
                            iNum=lMap.index(sTag)
                            lCol[iNum]=self.doc.getText(cCol)
                    lVal.append(lCol)
            nodeSum=self.doc.getChild(nodeData,'sum')
            if nodeSum is None:
                lSum=None
            else:
                lSum=['' for i in lMap]
                for cCol in self.doc.getChilds(nodeSum):
                    sTag=self.doc.getTagName(cCol)
                    if sTag in lMap:
                        iNum=lMap.index(sTag)
                        lSum[iNum]=self.doc.getText(cCol)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return lVal,lSum
    def GetSum(self,node):
        return self.Get(node,'sum')
    def SetValues(self,node,lVal,lSum):
        if node is None:
            return
        try:
            nodeData=self.doc.getChild(node,'data')
            if nodeData is not None:
                self.doc.deleteNode(nodeData,node)
            nodeData=self.doc.getChildForced(node,'data')
            lSetup=self.GetSetup()
            lMap=[d['tag'] for d in lSetup]
            for lRow in lVal:
                if len(lRow[0])==0:
                    continue
                nodeRow=self.doc.createSubNode(nodeData,'row')
                for sTag,sVal in zip(lMap,lRow):
                    self.doc.createSubNodeText(nodeRow,sTag,unicode(sVal))
            if lSum is not None:
                nodeRow=self.doc.createSubNode(nodeData,'sum')
                for sTag,sVal in zip(lMap,lSum):
                    self.doc.createSubNodeText(nodeRow,sTag,unicode(sVal))
                self.SetSum(node,lSum[-1])
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def SetSum(self,node,val):
        self.Set(node,'sum',val)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return [('sum',vtXmlFilterType.FILTER_TYPE_FLOAT),
            ]
    def GetTranslation(self,name):
        if name=='sum':
            return _(u'sum')
        return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return True
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return False
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return True
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02\x18IDAT8\x8d\x95\x93\xbdkSa\x14\xc6\x7f\xf7^o\xdb[\xdalY\x92\x16\
\x1a\x08I{\tQ\xc8\x9c-\x9d\x8c\x93\xbaT\xf3\x17\x14uR\xc7\xa6\x8b\xba\x17\
\xd4A\xabS *t5N*1C\xbd\x92\x9b\xd26)%\x94@\xe9G(\xc9\r\xb9\xa4\r\xf9x\xdd\
\xa4\xc57\x88g{\x0f\xcfy8\xfc\x9e\xf3*\x8a\xaa!\xab\xa7O\x1e\x8br\xb9L\xaf\
\xd7czz\x9a\x0f\x1f?)2\x9d*\x9d\x06NNNH$\x12\x98\xa6\x89\xa2Hg\xffm\xa0i\x1a\
\xba\xaeS,\x16\xff\xcf\xe0\xee\x9d\xdb"\x14\n177\x87i\x9a\xf8\xfd~R\xf7\xef\
\t\x99\xf6\xda\xe5\xc7\xa3\x87\x0fD0\x18\xa4\xd9lR(\x14\x98\x9d\x9d\xa5\xd7\
\xeb\xe18\x0e\x8b\x8b\x8b\xbc\x7f\xb7.\xf2\xf9<o\xde\xae+R\x83X,F2\x99dww\
\x97n\xb7\xcb\xde\xde\x1eB\x08\x96\x96\x96\x08\x87\xc3lnnR*\x95FopvvF\xbd^gf\
f\x86\xe5\xe5e\xfa\xfd>\xaa\xaa\xe28\x0ekkkLNN\x12\n\x85\xf8i\xfd\x923\xd8\
\xdf\xdf\'\x97\xcb1\x1c\x0ei4\x1aT\xabU\xea\xf5:\xb6mS\xab\xd5\xfe\xf4/\x97"\
\xbb\x83\xd5\xf4\x8a8==e8\x1c\x02\xd0n\xb7\xa9\xd5j\xe4\x7f\x14\xfe\xcaS\x9a\
B\xa5R\xe1\xf0\xf0\x90H$\x82\xd7\xeb\xa5R\xa90??/\x93^e\xf0\xe2\xf93\xd1l6\
\xe9t:LMM\x11\x08\x04\xf0x<\xe8\xba\x8e\xc7\xe3a5\xbd"V\xd2\xab\xcaH\x03\xc3\
0H\xa5RX\x96\xc5\xc6\xc6\x06\x99L\x06\xc7q\x08\x87\xc3\xc4\xe3q\\\xd7\xe5\
\xc6\xf5\xa8\xb0K[\xf2\x18\r\xc3\xe0\xe2\xe2\x82h4\x8ai\x9a\xb8\xae\x8b\xaa\
\xaaLLL\x90\xcdf\xd9\xde\xdefaa\x01\xbb\xb4%g`\xdb6\xe5r\x99\xf1\xf1q\x84\
\x10\x0c\x06\x03\xc6\xc6\xc6\xd8\xd9\xd9\xc1\xb2,b\xb1\x18\xc7\xc7\xc7\xa3\
\x19\xbc|\xf5Z\xd1u]\x1c\x1c\x1c\xd0\xef\xf7i\xb5Z\x18\x86\xc1\xd1\xd1\x11\
\xd5j\x15\x9f\xcf\xc7\xd7o\xdf\xaf0\x90\xc6\xf8%\xf7Y\x9c\x9f\x9f\xa3\xaa*B\
\x084M\xe3f\xf2\x96\xf4K\xfe\x06]\x94\xe5T\x9d\xef;\x17\x00\x00\x00\x00IEND\
\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        return None
        if GUI:
            return vXmlNodeOfferPartsEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        return None
        if GUI:
            return vXmlNodeOfferPartsAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeOfferPartsPanel
        else:
            return None
