#Boa:FramePanel:vXmlNodeOfferPartsPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeOfferPartsPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060815
# CVS-ID:       $Id: vXmlNodeOfferPartsPanel.py,v 1.1 2006/08/29 10:58:39 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputGridSetup

import sys

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_VXMLNODEOFFERPARTSPANEL, wxID_VXMLNODEOFFERPARTSPANELVIPARTS, 
] = [wx.NewId() for _init_ctrls in range(2)]

class vXmlNodeOfferPartsPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viParts, 0, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=0, rows=1, vgap=0)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEOFFERPARTSPANEL,
              name=u'vXmlNodeOfferPartsPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(312, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 180))
        self.SetAutoLayout(True)

        self.viParts = vidarc.tool.input.vtInputGridSetup.vtInputGridSetup(id=wxID_VXMLNODEOFFERPARTSPANELVIPARTS,
              name=u'viParts', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(304, 180))
        self.viParts.Bind(vidarc.tool.input.vtInputGridSetup.vEVT_VTINPUT_GRIDSETUP_INFO_CHANGED,
              self.OnViPartsVtinputGridsetupInfoChanged,
              id=wxID_VXMLNODEOFFERPARTSPANELVIPARTS)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vOffer')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.viParts.SetAutoStretch(True)
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def Clear(self):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.Clear(self)
        
        # add code here
        self.viParts.Clear()
        self.ClrBlockDelayed()
    def SetRegNode(self,obj):
        vtXmlNodePanel.SetRegNode(self,obj)
        self.viParts.Setup(obj.GetSetup(),None)
    def SetNetDocs(self,d):
        if d.has_key('vHum'):
            dd=d['vHum']
            self.docHum=dd['doc']
        # add code here
        
    def SetDoc(self,doc,bNet=False):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        
        # add code here
        
    def SetNode(self,node):
        try:
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            
            # add code here
            tup=self.objRegNode.GetValues(node)
            self.viParts.SetValue(tup[0])
            self.viParts.AutoSize()
            self.viParts.SetModified(False)
        except:
            vtLog.vtLngTB(self.GetName())
        
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
                print node
            if node is None:
                return
            # add code here
            tup=self.viParts.GetValueStr()
            self.objRegNode.SetValues(node,tup[0],tup[1])
            self.viParts.SetModified(False)
            self.GetNodeFin(node)
        except:
            vtLog.vtLngTB(self.GetName())
    def GetSum(self):
        tup=self.viParts.GetValueStr()
        return tup[1][-1]
    def Close(self):
        # add code here
        pass
    def Cancel(self):
        # add code here
        pass
    def Lock(self,flag):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if flag:
            # add code here
            self.viParts.Enable(False)
        else:
            # add code here
            self.viParts.Enable(True)
    def BindPartsChanged(self,func):
        self.viParts.Bind(vidarc.tool.input.vtInputGridSetup.vEVT_VTINPUT_GRIDSETUP_INFO_CHANGED,
              func,self.viParts)

    def OnViPartsVtinputGridsetupInfoChanged(self, event):
        event.Skip()
        #vtLog.CallStack('')
