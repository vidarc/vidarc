#----------------------------------------------------------------------------
# Name:         vXmlNodeOfferDepartment.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060815
# CVS-ID:       $Id: vXmlNodeOfferDepartment.py,v 1.1 2006/08/29 10:58:39 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeTag import *
try:
    if vcCust.is2Import(__name__):
        from vidarc.vApps.vOffer.vXmlNodeOfferDepartmentPanel import *
        #from vidarc.vApps.vOffer.vXmlNodeOfferDepartmentEditDialog import *
        #from vidarc.vApps.vOffer.vXmlNodeOfferDepartmentAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0


class vXmlNodeOfferDepartment(vtXmlNodeTag):
    FUNCS_GET_4_TREE=['Tag','Name']
    FMT_GET_4_TREE=[('Tag',''),('Name','')]
    GRP_GET_4_TREE=None
    def __init__(self,tagName='offerDepartment'):
        global _
        _=vtLgBase.assignPluginLang('vOffer')
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'offer department')
    # ---------------------------------------------------------
    # specific
    def GetRole(self,node,role,idOwner=None):
        oSM=self.doc.GetRegisteredNode('smOffer')
        return oSM.GetRole(node,role,idOwner)
    def GetRolesDict(self,node,idOwner=None):
        oSM=self.doc.GetRegisteredNode('smOffer')
        return oSM.GetRolesDict(node,idOwner)
    def GetPossibleRoles(self):
        oSM=self.doc.GetRegisteredNode('smOffer')
        return oSM.GetPossibleRoles()
    def GetConfigurableRoles(self):
        oSM=self.doc.GetRegisteredNode('smOffer')
        return oSM.GetConfigurableRoles()
    def GetPossibleRoleTranslation(self,role):
        oSM=self.doc.GetRegisteredNode('smOffer')
        return oSM.GetPossibleRoleTranslation()
    def GetPossibleRoleTranslationLst(self):
        oSM=self.doc.GetRegisteredNode('smOffer')
        return oSM.GetPossibleRoleTranslationLst()
    def GetConfigurableRoleTranslationLst(self):
        oSM=self.doc.GetRegisteredNode('smOffer')
        return oSM.GetConfigurableRoleTranslationLst()
    def SetRole(self,node,role,lst):
        oSM=self.doc.GetRegisteredNode('smOffer')
        return oSM.SetRole(node,role,lst)
    def SetRolesDict(self,node,d):
        oSM=self.doc.GetRegisteredNode('smOffer')
        return oSM.SetRolesDict(node,d)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def Is2Create(self):
        return False
    def IsSkip(self):
        return False
    def IsMultiple(self):
        return True
    def Is2Add(self):
        return True
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xa1IDAT8\x8dc\xd4\xeb\x0e\xff\xcf@\x00\\._\xc3\x88K\x8e\t\x9b\xe0\
\xf6\xa8\x95\x0c\xdb\xa3V\xc2\xf9\xba\x9d!8-A1\x00]#1\x86\xb0`S\x0c\x03\x9e\
\xcb\xc2\xf1:\x1f\xc5\x00\\\x9a\xa7O\x9b\x8aasfV6\xdcP&l\xb6\xe3\xd3\xcc\xc0\
\xc0\xc0\x80,\xce\x88-\x16\xd05#\xdb\x88l@fV6#\x0b\xbaF|\xce\xc5&\xc6\xc8\
\xc8\xc4\xcc\xc0\xc0\x00\te|\xce&h\x00:@v&>\xc30\x0c\xa0\xbb\x0b\xb0&eB.\x9a\
>m\xea\x7f\x988N\x17\xe0\xd2\x0c\x030\x97\xe1u\x01.\xe7\x13\x15\x06\xc4\x02\
\x00\xde\xbdZ\xe6\x94:`\xcd\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClassOld(self):
        if GUI:
            return vXmlNodeOfferDepartmentEditDialog
        else:
            return None
    def GetAddDialogClassOld(self):
        if GUI:
            return vXmlNodeOfferDepartmentAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeOfferDepartmentPanel
        else:
            return None

