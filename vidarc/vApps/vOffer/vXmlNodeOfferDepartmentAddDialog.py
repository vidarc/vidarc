#Boa:Dialog:vXmlNodeOfferDepartmentAddDialog
#----------------------------------------------------------------------------
# Name:         vXmlNodeOfferDepartmentAddDialog.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060815
# CVS-ID:       $Id: vXmlNodeOfferDepartmentAddDialog.py,v 1.1 2006/08/29 10:58:39 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.xml.vtXmlNodeAddDialog import *

from vidarc.vApps.vOffer.vXmlNodeOfferDepartmentPanel import *

def create(parent):
    return vXmlNodeOfferDepartmentAddDialog(parent)

[wxID_VXMLNODEOFFERDEPARTMENTADDDIALOG, 
 wxID_VXMLNODEOFFERDEPARTMENTADDDIALOGCBAPPLY, 
 wxID_VXMLNODEOFFERDEPARTMENTADDDIALOGCBCANCEL, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vXmlNodeOfferDepartmentAddDialog(wx.Dialog,vtXmlNodeAddDialog):
    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsBt, (1, 0), border=4,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER, span=(1, 1))

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=0, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_gbsData_Items(self.gbsData)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VXMLNODEOFFERDEPARTMENTADDDIALOG,
              name=u'vXmlNodeOfferDepartmentAddDialog', parent=prnt,
              pos=wx.Point(348, 174), size=wx.Size(400, 420),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE,
              title=u'vOffer Department Add Dialog')
        self.SetClientSize(wx.Size(392, 393))

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODEOFFERDEPARTMENTADDDIALOGCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'),
              name=u'cbApply', parent=self, pos=wx.Point(0, 24),
              size=wx.Size(76, 30), style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VXMLNODEOFFERDEPARTMENTADDDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODEOFFERDEPARTMENTADDDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(108, 24),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VXMLNODEOFFERDEPARTMENTADDDIALOGCBCANCEL)

        self._init_sizers()

    def __init__(self, parent):
        global _
        _=vtLgBase.assignPluginLang('vOffer')
        self._init_ctrls(parent)
        vtXmlNodeAddDialog.__init__(self)
        try:
            self.doc=None
            self.node=None
            self.gbsData.AddGrowableCol(0)
            self.gbsData.AddGrowableRow(0)
            self.pn=vXmlNodeOfferDepartmentPanel(self,id=wx.NewId(),pos=(0,0),size=(440,285),
                            style=0,name=u'pnDepartment')
            self.gbsData.AddWindow(self.pn, (0, 0), border=4,
                  flag=wx.BOTTOM | wx.TOP | wx.LEFT | wx.RIGHT | wx.EXPAND, span=(1, 1))
            self.gbsData.Layout()
            self.gbsData.Fit(self)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbApplyButton(self,evt):
        evt.Skip()
        try:
            self.pn.Close()
            if self.IsModal():
                self.EndModal(1)
            else:
                node=self.objReg.Create(self.nodePar)
                self.pn.GetNode(node)
                self.pn.Clear()
                self.Show(False)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbCancelButton(self,evt):
        evt.Skip()
        try:
            self.pn.Close()
            if self.IsModal():
                self.EndModal(0)
            else:
                self.Show(False)
        except:
            vtLog.vtLngTB(self.GetName())
