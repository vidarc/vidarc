#Boa:FramePanel:vXmlNodeOfferDepartmentPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeOfferDepartmentPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060815
# CVS-ID:       $Id: vXmlNodeOfferDepartmentPanel.py,v 1.1 2006/08/29 10:58:39 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.vApps.vHum.vXmlHumInputTreeGrp
import vidarc.vApps.vHum.vXmlHumInputTree
import vidarc.tool.input.vtInputTextML
import vidarc.tool.input.vtInputTextMultiLineML
import vidarc.tool.input.vtInputFloat
import vidarc.tool.input.vtInputAttrValue
import vidarc.tool.input.vtInputTreeMLInternal
import vidarc.tool.input.vtInputTreeInternal
import vidarc.tool.input.vtInputTree
import wx.lib.buttons
from wx.lib.anchors import LayoutAnchors
from gettext import *
from vidarc.tool.xml.vtXmlTree import *

from vidarc.tool.xml.vtXmlNodePanel import *
from vidarc.ext.state.veRoleDialog import *

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

import vidarc.vApps.vOffer.__config__ as __config__

VERBOSE=0

[wxID_VXMLNODEOFFERDEPARTMENTPANEL, wxID_VXMLNODEOFFERDEPARTMENTPANELCBROLE, 
 wxID_VXMLNODEOFFERDEPARTMENTPANELLBLAREA, 
 wxID_VXMLNODEOFFERDEPARTMENTPANELLBLROLE, 
 wxID_VXMLNODEOFFERDEPARTMENTPANELVITRAREA, 
] = [wx.NewId() for _init_ctrls in range(5)]

class vXmlNodeOfferDepartmentPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsArea, 0, border=0, flag=wx.EXPAND)

    def _init_coll_bxsArea_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblArea, 1, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.vitrArea, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblRole, 1, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbRole, 1, border=0, flag=wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.bxsArea = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=1, vgap=0)

        self._init_coll_bxsArea_Items(self.bxsArea)
        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEOFFERDEPARTMENTPANEL,
              name=u'vXmlNodeOfferDepartmentPanel', parent=prnt,
              pos=wx.Point(319, 211), size=wx.Size(316, 160),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(308, 133))
        self.SetAutoLayout(True)

        self.lblArea = wx.StaticText(id=wxID_VXMLNODEOFFERDEPARTMENTPANELLBLAREA,
              label=_(u'department'), name=u'lblArea', parent=self,
              pos=wx.Point(4, 0), size=wx.Size(69, 30), style=wx.ALIGN_RIGHT)
        self.lblArea.SetMinSize(wx.Size(-1, -1))

        self.vitrArea = vidarc.tool.input.vtInputTreeMLInternal.vtInputTreeMLInternal(id=wxID_VXMLNODEOFFERDEPARTMENTPANELVITRAREA,
              name=u'vitrArea', parent=self, pos=wx.Point(77, 0),
              size=wx.Size(77, 30), style=0)

        self.lblRole = wx.StaticText(id=wxID_VXMLNODEOFFERDEPARTMENTPANELLBLROLE,
              label=_(u'role'), name=u'lblRole', parent=self, pos=wx.Point(158,
              0), size=wx.Size(69, 30), style=wx.ALIGN_RIGHT)
        self.lblRole.SetMinSize(wx.Size(-1, -1))

        self.cbRole = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODEOFFERDEPARTMENTPANELCBROLE,
              bitmap=vtArt.getBitmap(vtArt.Role), label=_(u'role'),
              name=u'cbRole', parent=self, pos=wx.Point(231, 0),
              size=wx.Size(77, 30), style=0)
        self.cbRole.SetMinSize(wx.Size(-1, -1))
        self.cbRole.Bind(wx.EVT_BUTTON, self.OnCbRoleButton,
              id=wxID_VXMLNODEOFFERDEPARTMENTPANELCBROLE)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vOffer')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,lWidgets=[self.cbRole],
                lEvent=[],
                iSecLvEdit=__config__.SEC_LEVEL_LIMIT_ADMIN)
                
        self.dlgRole=veRoleDialog(self)
        self.bRoleShowOnce=False
        self.vitrArea.SetTagNames('mainDepartment','name')
        self.vitrArea.AddTreeCall('init','SetNodeInfos',['name'])
        self.vitrArea.AddTreeCall('init','SetGrouping',[],[('name','')])
        self.vitrArea.SetTagNames2Base(['globalsdata','globalsDepartment'])
        self.vitrArea.SetShowFullName(False)
        
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
        
    def SetNetDocs(self,d):
        if d.has_key('vHum'):
            dd=d['vHum']
            self.dlgRole.SetDocHum(dd['doc'],dd['bNet'])
        if d.has_key('vGlobals'):
            dd=d['vGlobals']
            self.vitrArea.SetDocTree(dd['doc'],dd['bNet'])
    def GetModified(self):
        if vtXmlNodePanel.GetModified(self):
            return True
        if self.vitrArea.IsModified():
            return True
        return False
    def Close(self):
        self.vitrArea.Close()
    def Clear(self):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.Clear(self)
        
        # add code here
        self.Close()
        self.dRoles={}
        self.vitrArea.Clear()
        self.ClrBlockDelayed()
    def SetDoc(self,doc,bNet=False):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        self.vitrArea.SetDoc(doc,bNet,bExternal=True)
        self.dlgRole.SetDoc(doc,bNet)
    def SetNode(self,node):
        try:
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            # add code here
            self.vitrArea.SetNode(self.node)
            self.dRoles=self.objRegNode.GetRolesDict(self.node)
        except:
            vtLog.vtLngTB(self.GetName())
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
                print node
            if node is None:
                return
            self.vitrArea.GetNode(node)
            self.objRegNode.SetRolesDict(node,self.dRoles)
            self.GetNodeFin(node)
        except:
            vtLog.vtLngTB(self.GetName())
    def Lock(self,flag):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'flag:%d'%(flag),self)
        if flag:
            self.vitrArea.Enable(False)
            self.cbRole.Enable(False)
        else:
            self.vitrArea.Enable(True)
            self.cbRole.Enable(True)

    def OnCbRoleButton(self, event):
        if self.bRoleShowOnce==False:
            self.dlgRole.Centre()
        self.dlgRole.SetRolePossible(self.objRegNode.GetConfigurableRoleTranslationLst())
        self.dlgRole.SetRoles(self.dRoles)
        iRet=self.dlgRole.ShowModal()
        if iRet>0:
            self.dRoles=self.dlgRole.GetRoles()
            self.SetModified(True,self.cbRole)
        event.Skip()
