#----------------------------------------------------------------------------
# Name:         vXmlOffer.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060620
# CVS-ID:       $Id: vXmlOffer.py,v 1.4 2007/07/30 20:38:48 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vidarc.vApps.vOffer.vXmlNodeOfferRoot import vXmlNodeOfferRoot
from vidarc.tool.xml.vtXmlNodeCfg import vtXmlNodeCfg

import vidarc.vApps.vOffer.__register__

def getPluginData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xaaIDAT8\x8d\xc5\x93\xb1\rC!\x0cD\xcf\x84u\xc8,\x14\x96\xb2\xcaoR\
\xd1\xa0\xccB\x91]\xfe@\xa4\x88\xf8"\xe6\x9c\xa4\xfbW\x9e\xcc\xf9Y6"\xe1\x02\
\xabT\xb5/&\x80}kb\xbd\xf0o!\xf3\x00@\x06\x81\xd7\xd5\xd3\x08\x8c^A\xcbe\xf1\
\xf4y_<:\x02{\xec\xf91U\xed\xf3|s\x91\xed\xd8rA\xcb\xe5\xf0S\xd5.\xd7\xc7\
\xad\xdb"\x0f\x97\xe9\x08\xf0\xb0g\xb1\xd0\xf3\t"\xf0\xde\xa9\x02\x1fA\xbf\
\x08\xf6\xadI\xaa\xda\x83wa\x8chla\x0e\xa1\x97\xf8m\x9cA6\x1a\xd3C\xf2\xf0\
\xe9\x16\xd8o\xb4DsG+J\xc0>\x96\xf7\xd9^\xf7\x04X\xae\x9fo\xba\xc6\x00\x00\
\x00\x00IEND\xaeB`\x82'

class vXmlOffer(vtXmlDomReg):
    VERBOSE=1
    TAGNAME_REFERENCE='tag'
    TAGNAME_ROOT='offerroot'
    APPL_REF=['vContact','vPrjDoc','vPrj','vDoc','vHum','vGlobals','vMsg']
    def __init__(self,appl='vOffer',attr='id',skip=[],synch=False,verbose=0,
                    audit_trail=True):
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        try:
            vtXmlDomReg.__init__(self,appl=appl,attr=attr,skip=skip,synch=synch,
                                verbose=verbose,audit_trail=audit_trail)
            oRoot=vXmlNodeOfferRoot()
            self.RegisterNode(oRoot,True)
            #oOffer=vXmlNodeOffer()
            
            #oLog=vtXmlNodeLog()
            oCfg=vtXmlNodeCfg()
            #self.RegisterNode(oOffer,True)
            #self.RegisterNode(oLog,False)
            self.RegisterNode(oCfg,True)
            #self.RegisterNode(vtXmlNodeAttrCfgML(),False)
            
            #self.LinkRegisteredNode(oRoot,oOffer)
            #self.LinkRegisteredNode(oOffer,oLog)
            vidarc.vApps.vOffer.__register__.RegisterNodes(self,bRegAsRoot=True)
            
            oDataColl=self.GetReg('dataCollector')
            self.LinkRegisteredNode(oRoot,oDataColl)
            #oPurchase=self.GetReg('purchase')
            #self.LinkRegisteredNode(oRoot,oPurchase)
            oOffer=self.GetReg('offer')
            oOfferDepartment=self.GetReg('offerDepartment')
            self.LinkRegisteredNode(oRoot,oOffer)
            self.LinkRegisteredNode(oRoot,oOfferDepartment)
            self.LinkRegisteredNode(oOfferDepartment,oDataColl)
            
            oDocs=self.GetReg('Documents')
            self.LinkRegisteredNode(oRoot,oDocs)

        except:
            vtLog.vtLngTB(self.appl)
        self.SetDftLanguages()
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'offers')
    #def New(self,revision='1.0',root='offerroot',bConnection=False):
    #    vtXmlDomReg.New(self,revision,root)
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elem=self.createSubNode(self.getRoot(),'offers')
        if bConnection==False:
            self.CreateReq()
        #if bConnection==False:
        #    e=self.createSubNodeTextAttr(elem,'cfg','','id','')
        #    self.checkId(e)
        #    self.processMissingId()
        #    self.clearMissing()
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        #self.AlignDoc()
    def Build(self):
        vtXmlDomReg.Build(self)
        oSMlinker=self.GetRegisteredNode('stateMachineLinker')
        tmp=self.getChildByLst(None,['cfg','stateMachineLinker'])
        oSMlinker.SetNode(tmp)
        self.GenerateSortedOfferNrIds()
    def GetStateMachine(self,nodeName,attrName):
        oSMlinker=self.GetRegisteredNode('stateMachineLinker')
        return oSMlinker.GetRunner(nodeName,attrName)
    def GenerateSortedOfferNrIds(self):
        self.offerNr=self.getSortedNodeIdsRec([],
                        'offer','offerID',None)
    def GetSortedOfferNrIds(self):
        try:
            return self.offerNr
        except:
            self.GenerateSortedOfferNrIds()
            return self.offerNr
