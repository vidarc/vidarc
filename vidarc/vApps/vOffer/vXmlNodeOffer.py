#----------------------------------------------------------------------------
# Name:         vXmlNodeOffer.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060620
# CVS-ID:       $Id: vXmlNodeOffer.py,v 1.3 2008/03/26 23:10:39 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

import os,copy
import vidarc.tool.report.vRepLatex as latex

from vidarc.tool.time.vtTime import vtDateTime
from vidarc.tool.xml.vtXmlNodeTag import *
from vidarc.ext.state.veStateAttr import veStateAttr
from vidarc.ext.report.veRepDoc import veRepDoc
from vidarc.tool.lang.vtLgDictML import vtLgDictML
from vidarc.ext.data.veDataCollectorCfg import veDataCollectorCfg

try:
    if vcCust.is2Import(__name__):
        from vXmlNodeOfferPanel import *
        from vXmlNodeOfferEditDialog import *
        from vXmlNodeOfferAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeOffer(vtXmlNodeTag,veStateAttr,veDataCollectorCfg):
    NODE_ATTRS=vtXmlNodeTag.NODE_ATTRS+[
            #('Sum',None,'sum',None),
            ('Currency',None,'currency',None),
            ('PurState',None,'purState',None),
        ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='offer',dSrcWid=None,srcAppl=None,bChilds=True,
                    dataProc=None):
        global _
        _=vtLgBase.assignPluginLang('vOffer')
        vtXmlNodeTag.__init__(self,tagName)
        veStateAttr.__init__(self,'smOffer','offState')
        veDataCollectorCfg.__init__(self,tagName,dSrcWid=dSrcWid,srcAppl=srcAppl,bChilds=bChilds,
                    dataProc=dataProc)
        self.oSMlinker=None
        self.oRep=veRepDoc()
        self.dt=vtDateTime(True)
        self.lgDict=None
        if GUI:
            self.__getTranslation__()
    def __getTranslation__(self):
        if self.lgDict is not None:
            return
        try:
            
            #domain=vtLgBase.getApplDomain()
            domain=__name__.split('.')[-2]
            dn=vtLgBase.getApplBaseDN()
            l=vtLgBase.getPossibleLang(domain,dn)
            if l is not None:
                try:
                    self.lgDict=vtLgDictML(domain,langs=l)
                except:
                    self.lgDict=vtLgDictML(domain,dn=os.path.split(__file__)[0],langs=l)
            else:
                self.lgDict=None
                vtLog.vtLngCurCls(vtLog.WARN,'translation not found',self)
        except:
            self.lgDict=None
            vtLog.vtLngTB(self.__class__.__name__)
    def GetDescription(self):
        return _(u'offer')
    # ---------------------------------------------------------
    # specific
    def GetSum(self,node):
        c=self.doc.getChild(node,'offerParts')
        if c is not None:
            sVal=self.doc.GetNodeAttribute(c,'GetSum')
            if len(sVal)==0:
                return '0.00'
            return sVal
        return '0.00'
        #return self.Get(node,'sum')
    def GetSumFloat(self,node):
        try:
            return float(self.GetSum(node))
        except:
            return 0.0
    def GetProject(self,node):
        return self.GetChildAttrStr(node,'project','fid')
    def __getCustomer__(self,node):
        cltFid=''
        netPrj,nodePrj=self.doc.GetNode(self.GetProject(node))
        if netPrj is not None and nodePrj is not None:
            oPrj=netPrj.GetRegByNode(nodePrj)
            if oPrj is not None:
                cltFid=oPrj.GetClient(nodePrj)
            else:
                vtLog.vtLngCurCls(vtLog.ERROR,'tag:%s not registered in vPrj'%(netPrj.getTagName(nodePrj)),self)
        return cltFid
    def GetCustomer(self,node):
        #vtLog.CallStack('')
        fid=self.GetChildAttrStr(node,'customer','fid')
        cltFid=self.__getCustomer__(node)
        #print fid,cltFid
        return cltFid
        return fid
    def GetCustomerAddress(self,node):
        return self.GetChildAttrStr(node,'customer','address')
    def GetCustomerContact(self,node):
        return self.GetChildAttrStr(node,'customer','contact')
    def GetOfferedBy(self,node):
        return self.GetChildAttrStr(node,'offeredBy','fid')
    def GetCurrency(self,node):
        return self.Get(node,'currency')
    #def GetName(self,node):
    #    return self.GetML(node,'name')
    def SetSum(self,node,val):
        self.Set(node,'sum',val)
    def SetSumFloat(self,node,val):
        self.SetSum(node,str(val))
    def SetCustomerAddress(self,node,val):
        self.SetChildAttrStr(node,'customer','address',val)
    def SetCustomerContact(self,node,val):
        self.SetChildAttrStr(node,'customer','contact',val)
    def SetCurrency(self,node,val):
        self.Set(node,'currency',val)
    # ---------------------------------------------------------
    # state machine related
    def GetOwner(self,node):
        return self.GetChildForeignKey(node,'offState','owner','vHum')
    def GetPurState(self,node):
        if self.oSMlinker is None:
            self.oSMlinker=self.doc.GetRegisteredNode('stateMachineLinker')
        try:
            oRun=self.oSMlinker.GetRunner(self.tagName,self.smAttr)
            id=self.GetChildAttr(node,self.smAttr,'fid')
            state=oRun.GetState(id)
            return state
            #print oRun.GetTransitions(id)
            #return self.Get(node,'state01')
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def SetPurState(self,node,val):
        if self.oSMlinker is None:
            self.oSMlinker=self.doc.GetRegisteredNode('stateMachineLinker')
        try:
            oRun=self.oSMlinker.GetRunner(self.tagName,self.smAttr)
            c=self.GetChildForced(node,self.smAttr)
            oLogin=self.doc.GetLogin()
            if oLogin is not None:
                idLogin=oLogin.GetUsrId()
            else:
                vtLog.vtLngCurCls(vtLog.ERROR,'non loggedin',self)
                idLogin=-1
            self.SetAttr(c,'fid',oRun.GetInitID())
            self.SetOwner(node,idLogin)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def SetOwner(self,node,val):
        self.SetChildForeignKey(node,self.smAttr,'owner',val,'vHum')
    def GetActionCmdDict(self):
        #return {
        #    'Clr':(_('clear'),None),
        #    'Go':(_('go on vacation'),None),
        #    'NoGo':(_('do not go on vacation'),None),
        #    }
        return {}
    def GetMsgPrior(self,node):
        return '0'#str(self.GetPriorIdx(node))
    def GetMsgTag(self,node,lang=None):
        return self.GetTag(node)
    def GetMsgName(self,node,lang=None):
        return self.GetName(node,lang)
    def GetMsgInfo(self,node,lang=None):
        return ''
        try:
            self.dt.SetStr(self.GetStartDt(node))
        except:
            pass
        dtS=self.dt.GetDateStr()+' '+self.dt.GetTimeStr()
        try:
            self.dt.SetStr(self.GetEndDt(node))
        except:
            pass
        dtE=self.dt.GetDateStr()+' '+self.dt.GetTimeStr()
        return _(u'vacation')+' '+_('from:')+dtS+' '+_('to:')+dtE
    # ---------------------------------------------------------
    # document machine related
    def Process(self,f,*args,**kwargs):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurCls(vtLog.DEBUG,'args:%s,kwargs:%s'%(vtLog.pformat(args),vtLog.pformat(kwargs)),self)
            node=kwargs['node']
            lang=kwargs['lang']
            self.oRep.__setLang__(lang)
            
            iLevel=kwargs['level']
            sName=self.GetName(node,lang)
            sDesc=self.GetDesc(node,lang)
            sCurrency=self.GetCurrency(node)
            
            f.write(os.linesep)
            f.write(self.oRep.__getHeadLine__(iLevel,sName))
            f.write(os.linesep)
            
            kkwargs=copy.copy(kwargs)
            kkwargs.update({'bSkipHeadLine':True,'bSkipSummary':True})
            veDataCollectorCfg.Process(self,f,*args,**kkwargs)
            
            self.__getTranslation__()
            f.write(os.linesep.join([latex.texReplace(s) for s in sDesc.split('\n')]))
            oParts=self.doc.GetReg('offerParts')
            lSetup=oParts.GetSetup()
            lTrans=[self.lgDict.Get('pos',lang)]
            lFmt=[]
            def convBool(s):
                if s=='1':
                    return 'X'
                else:
                    return ''
            def convOrig(s):
                return s
            def convFloat(s):
                try:
                    return vtLgBase.format('%6.2f',float(s))
                    #return '%6.2f'%float(s)
                except:
                    return ''
            def convEmpty(s):
                return ''
            lConv=[]
            lCurrency=['']
            for d in lSetup:
                if 'is2Report' in d:
                    if d['is2Report']==0:
                        lConv.append(None)
                        #lFmt.append('c')
                        continue
                if 'trans' in d:
                    lTrans.append(self.lgDict.Get(d['trans'],lang))
                else:
                    lTrans.append('')
                if 'isCurrency' in d:
                    if d['isCurrency']==1:
                        lCurrency.append('[ '+sCurrency+' ]')
                    else:
                        lCurrency.append('')
                else:
                    lCurrency.append('')
                if 'typedef' in d:
                    dType=d['typedef']
                    sType=dType['type']
                    if sType=='bool':
                        func=convBool
                        sFmt='c'
                    elif sType=='string':
                        func=convOrig
                        sFmt='l'
                    elif sType=='int':
                        func=convOrig
                        sFmt='r'
                    elif sType=='float':
                        func=convFloat
                        sFmt='r'
                    else:
                        func=convEmpty
                        sFmt='c'
                    lConv.append(func)
                    lFmt.append(sFmt)
                else:
                    lConv.append(convEmpty)
                    lFmt.append('c')
            nodeParts=self.doc.getChild(node,'offerParts')
            lVal,lSum=oParts.GetValues(nodeParts)
            sFmt=oParts.GetLaTexTableFmt()
            sCurrencyTable=' & '.join(lCurrency)
            strs=[]
            strs.append('\\begin{center}')
            s='  \\begin{longtable}{%s}'%sFmt
            strs.append(s)
            s='\\hline '+' & '.join(lTrans)+'\\\\ %s \\\\ \\hline'%sCurrencyTable
            strs.append(s)
            strs.append('  \\endfirsthead')
            strs.append(s)
            strs.append('  \\endhead')
            iNum=1
            for lRow in lVal:
                lVal=[str(iNum)]
                for func,val in zip(lConv,lRow):
                    if func is not None:
                        lVal.append(func(val))
                s=' & '.join(lVal)+'\\\\'
                #s=' & '.join([func(val) for func,val in zip(lConv,lRow)])+'\\\\'
                strs.append(s)
                iNum+=1
            if lSum is not None:
                strs.append('\\hline \\hline')
                lVal=['']
                for func,val in zip(lConv,lSum):
                    if func is not None:
                        lVal.append(func(val))
                s=' & '.join(lVal)+'\\\\'
                #s=' & '.join([func(val) for func,val in zip(lConv,lSum)])+'\\\\'
                strs.append(s)
            strs.append('\\hline')
            sPrefix=' '.join([self.lgDict.Get('offer',lang),sName])
            strs.append('  \\caption{%s}'%latex.texReplace(sPrefix))
            strs.append('  \\label{tab:%s}'%latex.texReplace4Lbl(sPrefix))
            strs.append('  \\end{longtable}')
            strs.append('\\end{center}')
            f.write(os.linesep.join(strs))
            f.write(os.linesep)
            
            #f.write(''.join([os.linesep,'\\begin{flushleft}',os.linesep,'\\vspace{0.2cm}',os.linesep,]))
            #f.write(''.join([os.linesep,'\\begin{tabular}{ll}',os.linesep,]))
                #f.write(''.join([os.linesep,'\\vspace{1cm}',os.linesep,]))
            #f.write(''.join([os.linesep,'\\end{tabular}',os.linesep,'\\end{flushleft}',os.linesep,]))
            f.write(os.linesep)
            
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    # ---------------------------------------------------------
    # inheritance
    def GetAttrValueToDict(self,d,node,*args,**kwargs):
        if node is None:
            return d
        if d is None:
            d={}
        if 'lang' in kwargs:
            lang=kwargs['lang']
        else:
            lang=self.lang
        netPrj,nodePrj=self.doc.GetNode(self.GetProject(node))
        if netPrj is not None:
            if nodePrj is not None:
                dPrj=netPrj.GetAttrValueToDict(nodePrj,*args,**kwargs)
                for k,v in dPrj.items():
                    d[':'.join(['vPrj',k])]=v
        return d
    def GetAttributeValFullLang(self,node,tagName,lang=None):
        if tagName[:5]=='vPrj:':
            netPrj,nodePrj=self.doc.GetNode(self.GetProject(node))
            if netPrj is not None:
                if nodePrj is not None:
                    return netPrj.getNodeAttributeValFullLang(nodePrj,tagName[5:],lang=lang)
        return ''
    def SetDoc(self,doc,bNet=False):
        vtXmlNodeBase.SetDoc(self,doc)
        if self.doc is None:
            self.lgDict=None
            return
        if vcCust.is2Import(__name__):
            l=[]
            for tup in self.doc.GetLanguages():
                l.append(tup[1])
            if len(l)==0:
                l=['en','de','fr']
            try:
                self.lgDict=vtLgDictML('vOffer',langs=l)
            except:
                try:
                    self.lgDict=vtLgDictML('vOffer',dn=os.path.split(__file__)[0],langs=l)
                except:
                    self.lgDict=None
                    vtLog.vtLngCurCls(vtLog.ERROR,'translation veReportDoc not found',self)
                    vtLog.vtLngTB(self.__class__.__name__)
    def __createPost__(self,node,func=None,*args,**kwargs):
        try:
            if self.doc is None:
                return
            oLogin=self.doc.GetLogin()
            if oLogin is None:
                vtLog.vtLngCurCls(vtLog.ERROR,'no login info',self)
                return
            if oLogin.GetUsrId()<0:
                vtLog.vtLngCurCls(vtLog.ERROR,'invalid usr id:%d'%oLogin.GetUsrId(),self)
                return
            if self.oSMlinker is None:
                self.oSMlinker=self.doc.GetRegisteredNode('stateMachineLinker')
            if self.oSMlinker is not None:
                oRun=self.oSMlinker.GetRunner(self.tagName,self.smAttr)
            else:
                oRun=None
            if oRun is None:
                vtLog.vtLngCurCls(vtLog.ERROR,'no state runner for %s:%s found'%(self.smName,self.smAttr),self)
                return
            if oRun.GetInitID()<0:
                vtLog.vtLngCurCls(vtLog.ERROR,'faulty initial id:%d'%(oRun.GetInitID()),self)
                return
            if node is not None:
                self.dt.Now()
                self.SetDtTm(node,self.dt.GetDateSmallStr())
                
                oRun.Log(oRun.STATE_INIT,self.doc,node,oLogin)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return vtXmlNodeTag.GetAttrFilterTypes(self)+[
                ('sum',vtXmlFilterType.FILTER_TYPE_FLOAT),
                ('supplier',vtXmlFilterType.FILTER_TYPE_FLOAT),
                ('contact',vtXmlFilterType.FILTER_TYPE_FLOAT),
                ('|offerID',vtXmlFilterType.FILTER_TYPE_LONG),
                #('projects',vtXmlFilterType.FILTER_TYPE_STRING),
            ]
    def GetTranslation(self,name):
        return _(name)
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00jIDAT8\x8d\xc5\x93Q\x0e\x80 \x0cC\x1dzY\xf5<\xc0q\xf1\x0bCjKffb?;:\
\xde\x160K\xeb\x12Q\n\xa5\xbfh\xb0\xa9B-\xb9\xa1\xb7\x1f\xa7\xb9\x08XX\xf9\
\x86K\x1c\x0f\xe1\x8d\xbd6\xfar\x04\x86\xcb\xbc\x9b@a\xbb\x1b\xcc0g\xfa\x9f@\
>$FTKn\xe8?\x08T\xb8\x0b\xc9(\x81\xc2w\xed\xe0\xad\xc2\x9f\xe9\x02\x0703\x0e\
\xec\xf9u\xf3\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        return veDataCollectorCfg.GetEditDialogClass(self)
        if GUI:
            return vXmlNodeOfferEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return vXmlNodeOfferAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeOfferPanel
        else:
            return None
    def GetCollectorPanelClass(self):
        return veDataCollectorCfg.GetPanelClass(self)

_('sum'),_('projects'),_('supplier'),_('contact'),_(u'offer'),_(u'|offerID')
