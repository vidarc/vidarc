#----------------------------------------------------------------------------
# Name:         vXmlOfferInputTree.py
# Purpose:      input widget for Offer xml
#               text and popup with tree
# Author:       Walter Obweger
#
# Created:      2006
# CVS-ID:       $Id: vXmlOfferInputTree.py,v 1.1 2006/08/29 10:58:39 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from vidarc.tool.input.vtInputTree import vtInputTree
import vidarc.tool.log.vtLog as vtLog
from vidarc.vApps.vOffer.vXmlOfferTree import vXmlOfferTree

class vXmlOfferInputTree(vtInputTree):
    def __init__(self,*_args,**_kwargs):
        apply(vtInputTree.__init__,(self,) + _args,_kwargs)
        self.tagName='offer'
        self.tagNameInt='tag'
        self.trClass=self.__createTree__
        self.trShow=self.__showNode__
        self.SetAppl('vOffer')
    def __createTree__(*args,**kwargs):
        tr=vXmlOfferTree(**kwargs)
        return tr
    def __showNode__(self,doc,node,lang):
        return doc.getNodeText(node,self.tagNameInt)
