#Boa:FramePanel:vOfferMainPanel
#----------------------------------------------------------------------------
# Name:         vOfferPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060818
# CVS-ID:       $Id: vOfferMainPanel.py,v 1.5 2007/07/30 20:38:48 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.xml.vtXmlNodeRegListBook
import vidarc.tool.xml.vtXmlNodeTagCmdPanel
import time

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.vApps.common.vSystem as vSystem

from vidarc.tool.net.vtNetSecXmlGuiMaster import *

from vidarc.vApps.vOffer.vNetOffer import *
from vidarc.vApps.vOffer.vXmlOfferTree import *

from vidarc.tool.xml.vtXmlDom import vtXmlDom

from vidarc.vApps.vContact.vNetContact import *
from vidarc.vApps.vPrjDoc.vNetPrjDoc import *
from vidarc.vApps.vPrj.vNetPrj import *
from vidarc.vApps.vDoc.vNetDoc import *
from vidarc.vApps.vHum.vNetHum import *
from vidarc.vApps.vGlobals.vNetGlobals import *
from vidarc.tool.net.vtNetMsg import vtNetMsg

import vidarc.vApps.vOffer.images as imgOffer

def create(parent):
    return vOfferMainPanel(parent)

[wxID_VOFFERMAINPANEL, wxID_VOFFERMAINPANELNBDATA, wxID_VOFFERMAINPANELPNDATA, 
 wxID_VOFFERMAINPANELSLWNAV, wxID_VOFFERMAINPANELSLWTOP, 
 wxID_VOFFERMAINPANELVITAG, 
] = [wx.NewId() for _init_ctrls in range(6)]

def getPluginImage():
    return imgOffer.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(imgOffer.getPluginBitmap())
    return icon

class vOfferMainPanel(wx.Panel):
    VERBOSE=0
    def _init_coll_bxsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.nbData, 1, border=4, flag=wx.ALL | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.bxsData = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_bxsData_Items(self.bxsData)

        self.pnData.SetSizer(self.bxsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VOFFERMAINPANEL,
              name=u'vOfferMainPanel', parent=prnt, pos=wx.Point(325, 29),
              size=wx.Size(650, 400), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(642, 373))
        self.Bind(wx.EVT_SIZE, self.OnSize)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VOFFERMAINPANELSLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 0), size=wx.Size(200,
              376), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetAutoLayout(True)
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetLabel(_(u'navigation'))
        self.slwNav.SetToolTipString(_(u'navigation'))
        self.slwNav.SetDefaultSize(wx.Size(200, 100))
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VOFFERMAINPANELSLWNAV)

        self.slwTop = wx.SashLayoutWindow(id=wxID_VOFFERMAINPANELSLWTOP,
              name=u'slwTop', parent=self, pos=wx.Point(200, 0),
              size=wx.Size(440, 100), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwTop.SetDefaultSize(wx.Size(1000, 200))
        self.slwTop.SetAlignment(wx.LAYOUT_TOP)
        self.slwTop.SetAutoLayout(True)
        self.slwTop.SetSashVisible(wx.SASH_BOTTOM, True)
        self.slwTop.SetOrientation(wx.LAYOUT_HORIZONTAL)
        self.slwTop.SetLabel(_(u'information'))
        self.slwTop.SetToolTipString(_(u'information'))
        self.slwTop.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwTopSashDragged,
              id=wxID_VOFFERMAINPANELSLWTOP)

        self.pnData = wx.Panel(id=wxID_VOFFERMAINPANELPNDATA, name=u'pnData',
              parent=self, pos=wx.Point(208, 108), size=wx.Size(424, 252),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)

        self.viTag = vidarc.tool.xml.vtXmlNodeTagCmdPanel.vtXmlNodeTagCmdPanel(applName='vtXmlNodeTagCmdPanel',
              id=wxID_VOFFERMAINPANELVITAG, name=u'viTag', parent=self.slwTop,
              pos=wx.Point(0, 0), size=wx.Size(440, 97), style=0)
        self.viTag.Bind(vidarc.tool.xml.vtXmlNodeTagCmdPanel.vEVT_TOOL_XML_TAG_SELECTED,
              self.OnViTagToolXmlTagSelected, id=wxID_VOFFERMAINPANELVITAG)

        self.nbData = vidarc.tool.xml.vtXmlNodeRegListBook.vtXmlNodeRegListBook(applName='vtXmlNodeTagCmdPanel',
              id=wxID_VOFFERMAINPANELNBDATA, name=u'nbData', parent=self.pnData,
              pos=wx.Point(4, 4), size=wx.Size(412, 240), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vOffer')
        self._init_ctrls(parent)
        self.SetName(name)
        self.bActivated=False
        self.dCfg={}
        
        self.xdCfg=vtXmlDom(appl='vOfferCfg',audit_trail=False)
        
        appls=['vOffer','vContact','vPrjDoc','vPrj','vDoc','vHum','vGlobals','vMsg']
        
        rect = parent.sbStatus.GetFieldRect(0)
        self.netMaster=vtNetSecXmlGuiMaster(parent.sbStatus,'Master',pos=(rect.x+2, rect.y+2),
                    size=(rect.width-4, rect.height-4),verbose=0)
        self.netOffer=self.netMaster.AddNetControl(vNetOffer,'vOffer',synch=True,verbose=0,
                                            audit_trail=True)
        self.netContact=self.netMaster.AddNetControl(vNetContact,'vContact',synch=True,verbose=0)
        self.netPrjDoc=self.netMaster.AddNetControl(vNetPrjDoc,'vPrjDoc',synch=True,verbose=0)
        self.netPrj=self.netMaster.AddNetControl(vNetPrj,'vPrj',synch=True,verbose=0)
        self.netDoc=self.netMaster.AddNetControl(vNetDoc,'vDoc',synch=True,verbose=0)
        self.netHum=self.netMaster.AddNetControl(vNetHum,'vHum',synch=True,verbose=0)
        self.netGlb=self.netMaster.AddNetControl(vNetGlobals,'vGlobals',synch=True,verbose=0)
        self.netMsg=self.netMaster.AddNetControl(vtNetMsg,'vMsg',synch=True,verbose=0)
        self.netMaster.SetLang(lang=vtLgBase.getPluginLang())
        self.netMaster.AddNetControl(None,None,verbose=0)
        
        self.netMaster.SetCfg(self.xdCfg,[],appls,'vOfferMaster')
        
        self.vgpTree=vXmlOfferTree(self.slwNav,wx.NewId(),
                pos=(0,0),size=(198,529),style=0,name="trOffer",
                master=True,verbose=0)
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        self.vgpTree.SetDoc(self.netOffer,True)
        
        self.nbData.Show(False)
        self.nbData.SetDoc(self.netOffer,True)
        oOffer=self.netOffer.GetReg('offer')
        
        oDoc=self.netOffer.GetReg('Doc')
        oDoc.SetTreeClass(vXmlOfferTree)
        pnCls=oDoc.GetPanelClass()
        pn=pnCls(self.pnData,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="pnDoc")
        pn.SetRegNode(oDoc)
        pn.SetDoc(self.netOffer,True)
        self.viTag.AddWidgetDependent(pn,oDoc)
        pn.Show(False)
        self.bxsData.AddWindow(pn, 1, border=4, flag=wx.ALL | wx.EXPAND)
        
        pn=self.viTag.CreateRegisteredPanel('offerDepartment',
                        self.netOffer,True,
                        self.pnData)
        pn.Show(False)
        self.bxsData.AddWindow(pn, 1, border=4, flag=wx.ALL | wx.EXPAND)
        
        oDataCollector=self.netOffer.GetReg('dataCollector')
        self.viTag.AddWidgetDependent(None,oDataCollector)
        self.viTag.AddWidgetDependent(self.nbData,oOffer)
        self.viTag.SetDoc(self.netOffer,True)
        self.viTag.SetRegNode(oOffer,bIncludeBase=True)
        pnPur=self.nbData.GetPanelByName('offer')
        pnParts=self.nbData.GetPanelByName('offerParts')
        pnPur.SetDependentPartsPanel(pnParts)

        self.Move(pos)
        self.SetSize(size)
    def GetDocMain(self):
        return self.netOffer
    def GetNetMaster(self):
        return self.netMaster
    def GetTreeMain(self):
        return self.vgpTree
    def OnTreeItemSel(self,event):
        event.Skip()
        node=event.GetTreeNodeData()
        self.viTag.SetNode(node)
    def OpenFile(self,fn):
        if self.netOffer.ClearAutoFN()>0:
            self.netOffer.Open(fn)
    def OpenCfgFile(self,fn=None):
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                self.netMaster.SetCfgNode()
                self.__setCfg__()
                return
        self.xdCfg.New(root='config')
        if fn is not None:
            self.xdCfg.SetFN(fn)
            self.netMaster.SetCfgNode()
        self.__setCfg__()
    def __setCfg__(self):
        try:
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSlwNavSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iWidth=event.GetDragRect().width
            #if iWidth>80:
            #    iWidth=80
            self.dCfg['nav_sash']=str(iWidth)
            
            self.slwNav.SetDefaultSize((iWidth, 1000))
            
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            self.GetCfgData()
            #event.Skip()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSlwTopSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iHeight=event.GetDragRect().height
            #if iHeight>80:
            #    iHeight=80
            self.dCfg['nav_top']=str(iHeight)
            
            self.slwTop.SetDefaultSize((1000, iHeight))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            self.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnSize(self, event):
        event.Skip()
        try:
            bMod=False
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            
            self.pnData.Refresh()
            self.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())

    def SetCfgData(self,func,l,d):
        self.cfgFunc=func
        self.cfgOrigin=l
        self.dCfg=d
        try:
            i=int(self.dCfg['nav_sash'])
            self.slwNav.SetDefaultSize((i, 1000))
        except:
            pass
        try:
            i=int(self.dCfg['nav_top'])
            self.slwTop.SetDefaultSize((1000, i))
        except:
            pass
        try:
            pos=map(int,self.dCfg['pos'].split(','))
            self.Move(pos)
        except:
            pass
        try:
            sz=map(int,self.dCfg['size'].split(','))
            self.SetSize(sz)
        except:
            pass
    def GetCfgData(self):
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,self.dCfg)
    def GetAboutData(self):
        import __config__
        version=u' %d.%d.%d.%d'%(__config__.VER_MAJOR,__config__.VER_MINOR,
                    __config__.VER_RELEASE,__config__.VER_SUBREL)
        desc=_(u"""Offer module.

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
""")
        return _(u'VIDARC Offer'),desc,version
    def OnViTagToolXmlTagSelected(self, event):
        event.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            lWid=event.GetListWidget()
            self.bxsData.Layout()
            #if lWid is not None:
            #    for wid in lWid:
            #        wid.Fit()
                    
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        except:
            vtLog.vtLngTB(self.GetName())
