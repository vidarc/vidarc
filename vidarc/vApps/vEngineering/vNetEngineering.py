#----------------------------------------------------------------------------
# Name:         vNetEngineering.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: vNetEngineering.py,v 1.6 2007/10/13 17:00:25 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog

from vidarc.vApps.vEngineering.vXmlEngineering import vXmlEngineering
from vidarc.tool.net.vNetXmlWxGui import *

class vNetEngineering(vXmlEngineering,vNetXmlWxGui):
    def __init__(self,parent,appl,fn='tmpl.xml',attr='id',skip=[],
                pos=(0,0),size=(16,12),synch=False,verbose=0,audit_trail=False):
        vNetXmlWxGui.__init__(self,parent,appl,pos,size,synch=synch,verbose=verbose)
        vXmlEngineering.__init__(self,appl=appl,attr=attr,skip=skip,synch=synch,
                                verbose=verbose,audit_trail=audit_trail)
        vXmlEngineering.SetFN(self,fn)
    #def Stop(self):
    #    vNetXmlWxGui.Stop(self)
    #    vXmlCustomer.Stop(self)
    #def IsRunning(self):
    #    if vNetXmlWxGui.IsRunning(self):
    #        return True
    #    if vXmlCustomer.IsRunning(self):
    #        return True
    #    return False
    #def New(self,root='projectengineerings'):
    def New(self,root=None,bAcquire=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        vNetXmlWxGui.New(self)
        vXmlEngineering.New(self,root=root,
                bConnection=self.IsFlag(self.CONN_ACTIVE),
                bAcquire=bAcquire)
    def GetFN(self):
        sFN=vNetXmlWxGui.GetFN(self)
        if sFN is not None:
            return sFN
        else:
            return vXmlEngineering.GetFN(self)
    def Open(self,fn=None,silent=False,bThread=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCS(vtLog.INFO,'file:%s;slient:%d;thread:%d'%(repr(fn),silent,bThread),origin=self.appl)
        if fn is None:
            sFN=vNetXmlWxGui.GetFN(self)
            if sFN is not None:
                fn=sFN
        if fn is None:
            vXmlEngineering.New(self)
            return -2
        if bThread:
            self.silent=silent
            iRet=vXmlEngineering.Open(self,fn,True)
        else:
            iRet=vXmlEngineering.Open(self,fn)
            if iRet>=0:
                if silent==False:
                    self.NotifyContent()
            else:
                self.New()
        return iRet
    def Save(self,fn=None,encoding='ISO-8859-1'):
        #sFN=vNetXmlWxGui.GetFN(self)
        #if sFN is not None:
        #    if fn is None:
        #        fn=sFN
        vNetXmlWxGui.SaveApplDocs(self,encoding=encoding)
        return vXmlEngineering.Save(self,fn,encoding)
    def getSetupChildByLst(self,node,lst):
        if lst is None:
            lst=self.lstSetupNode
        return self.docSetup.getChildByLst(node,lst)
    def getChildByLst(self,node,lst):
        if lst is None:
            return None
        return vXmlEngineering.getChildByLst(self,node,lst)
    def addNode(self,par,node,func=None,**kwargs):
        vXmlEngineering.addNode(self,par,node)
        vNetXmlWxGui.addNode(self,par,node,func,**kwargs)
    def delNode(self,node):
        vNetXmlWxGui.delNode(self,node)
        vXmlEngineering.delNode(self,node)
    def moveNode(self,nodePar,node):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'idPar:%s;id:%s'%(self.getKey(nodePar),self.getKey(node)),self.appl)
        vNetXmlWxGui.moveNode(self,nodePar,node)
        vXmlEngineering.moveNode(self,nodePar,node)
    def IsRunning(self):
        if vNetXmlWxGui.__isRunning__(self):
            return True
        if vXmlEngineering.__isRunning__(self):
            return True
        return False
    def __stop__(self):
        vNetXmlWxGui.__stop__(self)
        vXmlEngineering.__stop__(self)
    def NotifyContent(self):
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyContent(self)
    def NotifyGetNode(self,id):
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyGetNode(self,id)
    def NotifySetNode(self,id,idNew,content):
        actIds=self.GetSortedActNrIds()
        if actIds is not None:
            actIds.RemoveId(id)
        vNetXmlWxGui.NotifySetNode(self,id,idNew,content)
        try:
            if actIds is not None:
                node=self.getNodeById(idNew)
                actIds.AddNodeSingle(node,'actID',None,self,True)
        except:
            vtLog.vtLngTB(self.appl)
    def NotifyAddNode(self,idPar,id,content):
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyAddNode(self,idPar,id,content)
        actIds=self.GetSortedActNrIds()
        try:
            if actIds is not None:
                node=self.getNodeById(id)
                actIds.AddNodeSingle(node,'actID',None,self,True)
        except:
            vtLog.vtLngTB(self.appl)
    def NotifyRemoveNode(self,id):
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyRemoveNode(self,id)
        actIds=self.GetSortedActNrIds()
        if actIds is not None:
            actIds.RemoveId(id)
    def GenerateSortedIds(self):
        self.customers=self.getSortedNodeIds(['customers'],
                        'customer','id',[('name','%s')])
    def GetSortedIds(self):
        try:
            return self.customers
        except:
            self.GenerateSortedIds()
            return self.customers
