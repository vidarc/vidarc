#----------------------------------------------------------------------------
# Name:         vNodeToDo.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20130520
# CVS-ID:       $Id: vNodeTool.py,v 1.2 2014/06/15 19:54:36 wal Exp $
# Copyright:    (c) 2013 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeTool(vNodeEngBase):
    def __init__(self,tagName='Tool'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'tool')
    # ---------------------------------------------------------
    # specific
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'serveriy':[
                    _(u'41 serverity'),u'serverity',-41,
                    {'val':u'+0','type':u'choice','it00':'+2','it01':'+1','it02':'+0','it03':'-1','it04':'-2'},
                    {'val':u'+0','type':u'choice','it00':'+2','it01':'+1','it02':'+0','it03':'-1','it04':'-2'}],
            u'priority':[
                    _(u'42 priority'),u'priority',-42,
                    {'val':u'+0','type':u'choice','it00':'+2','it01':'+1','it02':'+0','it03':'-1','it04':'-2'},
                    {'val':u'+0','type':u'choice','it00':'+2','it01':'+1','it02':'+0','it03':'-1','it04':'-2'}],
            u'system':[
                    _(u'43 system'),u'system',-43,
                    {'val':u'','type':u'text'},{'val':u'','type':u'text'}],
            u'origin':[
                    _(u'44 origin'),u'origin',-44,
                    {'val':u'','type':u'text'},{'val':u'','type':u'text'}],
            u'zFound':[
                    _(u'45 time found'),u'zFound',-45,
                    {'val':u'','type':u'datetime',},
                    {'val':u'','type':u'datetime',}],
            u'zReport':[
                    _(u'46 time reported'),u'zReport',-46,
                    {'val':u'','type':u'datetime',},
                    {'val':u'','type':u'datetime',}],
            u'zDeadLine':[
                    _(u'48 deadline'),u'zDeadLine',-48,
                    {'val':u'','type':u'datetime',},
                    {'val':u'','type':u'datetime',}],
            u'zFix':[
                    _(u'49 time fixed'),u'zFix',-49,
                    {'val':u'','type':u'datetime',},
                    {'val':u'','type':u'datetime',}],
            u'pType':[
                    _(u'21 type'),u'pType',-21,
                    {'val':u'','type':u'text'},{'val':u'','type':u'text'}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02(IDAT8\x8du\x92_H\xd3Q\x14\xc7?\xf7\xb7\x7f\xea\xe6F\xcb\x94\xb14~\
\xed!\x85U\xb2\xb00z\xd3\x87@|H\x82B\xa2\x88\xa0\x87\x05=\xec\xad\xb1\x17\
\x91\xb4\x07\x91\xa2(\x88\xf6\xe0\x9e&\xe4\x93a\x14\x04\x114\xd2l\xbd\x88l\
\x88\xad\x87\x86\xa1\xb1\x85\x7f\xda\x1a\xd3\xdb\xc3\x8f\xdf\xf2\xb7\xcd\x03\
\x97\xcb9\xf7\x9e\xcf\xfd\x9es.]\xbd\x11\xd9\xd5\x1b\x91\xfdC\x13R(&\xf4\xf5\
\xe4\xc5;y\xe3N\xd4\x10\x1b\x7f8'\x85b\xa2\x7fhBN\xc5?J\xa1\x98P\x00\xce\\\
\x1ef\xcfs\x8cj[Hf\x0c~l:\x01@\xb3\xa3\x91\x07\x8f^369+\x15\x80&w\x03.\xf7\
\xe1\x1a\x00\xc0\xe3\xe7o%\xc0\xd8\xe4\xac\x04\xe8\xe9\x1b\x91\xd9\xb5<\xad\
\x81\x00\xb1\xe9\x84\xa6\xc0{Z\xc5l3&~\xfe\xfa\xbd.pk\xbbH\x01\x0b\xeb\xc9$\
\xcd\x8e\x06\r`q\x80\xc9b\xbc\x98]\xcb\x1b\xfc?;%\x14\xab\x95fG\x03\xc3\x03'\
Q;Z\xb8~\xa5W\x03X\xed`\xb2V\x03~\x03\x90Z\xf9\t@\x93\xdd\xca^\xa9\xc4\xdd\
\x9b\x179\xd5y\x94_\xb9m\xb6\xb6\xffj\x80\xa9k\xf7Y\x99KT\x92\xf5zu\xc9\x00\
\xe1\xd0\xa08\x1bPIg~\xf0\xe9\xcb7.\r\x04\x08\x87\x06\x85\xd6\x83\x0b\xe7)\
\x94\xeb\x96\xccB2CdtF\x02\xc4\x9e\xdd\x16\x85B\x99\xa5t\x96phP\x00\x9a\x82\
\xd2\xe6&\xe5\xa2\xf6RdtF\xc6\xa6\x13\xb8}*\x00n\x9f\xca\xcbW\x8b\x15H\xb5)\
\x00k\x0b\x8b\x86\xa0\xdb\xa7\x92[\xcd\xd0\xedo'\xb7\x9a\xc1\xedS\xc9o\xee\
\xd4U\xa8\xa4\xe6\xc7\x05\x80\xbe\xbfy\xbf\x84\xcd\xe5\xa4\xdb\xdfN<\x1a\x14\
\xdd\xfevl.g\xcd\xa72(\xd8o\xbbv'\x1b\xcbi\xe2\xd1\xa0\x00\x88G\x83bc9\xcd\
\xae\xddY\xb9\xe3?\xe1=\x18P.\x16i4\x1bc\x8df*=\xaaQp\xf5\xd6S\xd9v\xc4IO\
\xdfH\xdd&\xe9\xb6\xb7[\xffX\x01\xf0\xb4\xb9\xf0z\x0eU\x82\xfa\xec\x0f\xf2\
\x0b\xfbfn\x8eG\x83\xa2\xf3\xdc=\xa97\xf1x\xab\x1dZ\xed\xa4\xe6\xff'\xa8\x1d\
-(\x8a`\t\xf80\x9f2\xc0\xfe\x01\xd3\x8d\xc4h\x0e\xf9\x07\xf9\x00\x00\x00\x00\
IEND\xaeB`\x82" 
