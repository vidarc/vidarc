#----------------------------------------------------------------------------
# Name:         vNodeBug.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120530
# CVS-ID:       $Id: vNodeBug.py,v 1.2 2014/06/15 19:53:39 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeBug(vNodeEngBase):
    def __init__(self,tagName='Bug'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'bug')
    # ---------------------------------------------------------
    # specific
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'serveriy':[
                    _(u'41 serverity'),u'serverity',-41,
                    {'val':u'+0','type':u'choice','it00':'+2','it01':'+1','it02':'+0','it03':'-1','it04':'-2'},
                    {'val':u'+0','type':u'choice','it00':'+2','it01':'+1','it02':'+0','it03':'-1','it04':'-2'}],
            u'priority':[
                    _(u'42 priority'),u'priority',-42,
                    {'val':u'+0','type':u'choice','it00':'+2','it01':'+1','it02':'+0','it03':'-1','it04':'-2'},
                    {'val':u'+0','type':u'choice','it00':'+2','it01':'+1','it02':'+0','it03':'-1','it04':'-2'}],
            u'system':[
                    _(u'43 system'),u'system',-43,
                    {'val':u'','type':u'text'},{'val':u'','type':u'text'}],
            u'origin':[
                    _(u'44 origin'),u'origin',-44,
                    {'val':u'','type':u'text'},{'val':u'','type':u'text'}],
            u'zFound':[
                    _(u'45 time found'),u'zFound',-45,
                    {'val':u'','type':u'datetime',},
                    {'val':u'','type':u'datetime',}],
            u'zReport':[
                    _(u'46 time reported'),u'zReport',-46,
                    {'val':u'','type':u'datetime',},
                    {'val':u'','type':u'datetime',}],
            u'zDeadLine':[
                    _(u'48 deadline'),u'zDeadLine',-48,
                    {'val':u'','type':u'datetime',},
                    {'val':u'','type':u'datetime',}],
            u'zFix':[
                    _(u'49 time fixed'),u'zFix',-49,
                    {'val':u'','type':u'datetime',},
                    {'val':u'','type':u'datetime',}],
            u'pType':[
                    _(u'21 type'),u'pType',-21,
                    {'val':u'','type':u'text'},{'val':u'','type':u'text'}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xf4IDAT8\x8d\x8d\x93\xcfk\x13A\x14\xc7?\xb3\xbb\rRi\xd4\x18\x8a\xab\
U/\t\x91R\x9a\xd0\x96\x18\xa1\xd0\x83 \xf1?\x90x\x15/\xd2\x887Q\xaf\xbdzH\
\x10\xff\x00!^Dz\x92\x82X\x11O\xd6\n]\tZ\xb2\xebE\xc5(\xb6IK\xcd6\xbf\xc7\
\x83\xce\xb2\xdb\x04\xf4\x0b\x03\xef\xfb\xe6\xbd\x997\xef}\x07\xa1\xe9\x1c\\\
\xab7/\xca\xff\xf1\tM\xc73R\x89\x84\xf4\xdb\xff\xe2\xca6\xf0!\x19\x8fI\xcbv\
\x84e;Bq\x00\xc5\xfd>\x05M\x19+\x8bi\xbf\x9fd<&\xefg\'\xb8wa| \xc9\x1f\xebU`\
\xe6K\xe2\xce|F\x02\xf2D\xf80\x8d\xf6\x08[;.\xbb\xfb\x06uF\xb8:\x93\x96\xe5\
\xbd\x1a\xe7\xa3\x11\xcc|\xc9\xabH\x08M\x0f\xdc\x9c9>.O\xf5\xba<||\x9b\xad\
\xaf\x0e\xe5G\x1b\x00,Uk\x1c\x1d5xem\n\x7f\xbc\xe6\'\xb7RI\xb9\x1f\t\xb3t\
\xe54\x95O\xdf\xd9\xfb\xb6\xed\xed\xdd5#\xec\xb8]\xae\xcf\xa5\x86\xf7\xa0Z\
\xc8\xc9\xd5F\x03\x80Jh\x82h\xf9\x05G\xaa\x0e\xd3\x93-\xa6\'[4\xe9\x01\xf0f\
\xf7\x17\xd5B\xce;\xc4\xebA\xb6\xb8\x86e;"\x19\x8f\xc9\xcc\xe5\x05\x00z\xf6\
\x06m\xbb\x82[\xeb{7Z\xb6#\xb2E\xbc\x034\x80\x1bs\xb3\x81\xb2\x00\xa2\x97\
\xae!\xcf\xce\xf2\xa3\xd3\xe7\xed\x17\xc1\xb2\xdb\t\xec\xab\x1c\x03\xe0\xc1\
\xfa;\xa1\x9e\x91-\xae\xf1q\xf9%gj\xdb\xfc|\xfd\x9c\xcd\x0f\x81\x9eQ-\xe4\
\xa4\x99/\t\xeb/\xf7\xa6\xa0\x92-\xdb\x11\xf3SSr\xc1l\x93h\x8d\x01\xf0\xa4\
\xd1\xc6\x10\x82\xa7\xeb\xefE2\x1e\x93+\x8bio\x94\x01%\xfa\x91h\x8d\xf1\xcc\
\xed\xd1\xa6O\xbd\x0bz\x7fx\\@\x07\xaa\n\x80c\x87F9\x19\xd2\xf9\xdc\xea`\x08\
\x8dz\xd3\x05\xfe\xa8p@Hj,\xea\t\x003\x89s2\x1c\n\x01Po\xba\xf8\xff\x87\x92\
\xb2\x99/\x89\x01%\xfa1\xec3\x1d\xc4o\xc4B\xd8\xf6\x8a\xb3\x80\x9a\x00\x00\
\x00\x00IEND\xaeB`\x82' 
