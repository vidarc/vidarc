#----------------------------------------------------------------------------
# Name:         vNodeCps.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20150228
# CVS-ID:       $Id: vNodeCps.py,v 1.3 2015/03/01 09:08:59 wal Exp $
# Copyright:    (c) 2015 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeObj import vNodeObj
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeCps(vNodeObj):
    def __init__(self,tagName='Cps'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeObj.__init__(self,tagName)
    def GetDescription(self):
        return _(u'components')
    def GetTransDescription(self):
        return u'components'
    # ---------------------------------------------------------
    # specific
    def GetProcessNodeInc(self):
        return [
            {
            'lTag':['Base','Obj','Cps','Struct','Dsg','Prd','Res','Res',
                    'Fct','Prc','Prop','itfc','var',
                    'ToDo','Tool',
                    ],
            'level':1,'level_max':4,},
            ]
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vNodeObj.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'inheritance':[
                    _(u'11 inheritance'),u'inheritance',-11,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'typical':[
                    _(u'12 typical'),u'typical',-12,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        import imgObj
        return imgObj.getObjectsCompData()
