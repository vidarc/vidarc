#----------------------------------------------------------------------
# Name:         vXmlNodeEngineeringLinkPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120515
# CVS-ID:       $Id: vXmlNodeEngineeringLinkPanel.py,v 1.9 2015/02/27 20:26:37 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import sys,os,os.path,shutil
    import vidarc.tool.InOut.fnUtil as fnUtil
    from vGuiFrmWX import vGuiFrmWX
    from vidarc.tool.xml.vtXmlNodeGuiPanel import vtXmlNodeGuiPanel
    #from vidarc.tool.vtThread import vtThread
    #from vidarc.vApps.vDoc.vDocSelDialog import vDocSelDialog
    from vidarc.vApps.vPrjDoc.vAddExternalFileDialog import vAddExternalFileDialog
    from vidarc.tool.input.vtInputTreeML import vtInputTreeML
    from vidarc.tool.input.vtInputTreeAttr import vtInputTreeAttr

    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    from vidarc.tool.vtProcess import vtProcess
    from vidarc.tool.misc.vtmMsgDialog import vtmMsgDialog
    
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)


class vXmlNodeEngineeringLinkPanel(vtXmlNodeGuiPanel):
    VERBOSE=0
    def __initCtrl__(self,*args,**kwargs):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        lWid=[
            #('lblCt',               (0,0),(1,1),{'name':'lblEng','label':_(u'engineering')},None),
            (vtInputTreeAttr,(0,0),(1,2),{'name':'viEng',
                            'tip':'engineering values',
                            #'guiStore':'trNode',
                            'mod':False,#'bSuppressNetNotify':True,
                            'lPropNode':1,'lPropAttr':1,'iOrientation':0,
                            },None),
            #(vtInputTreeML,         (0,1),(1,2),{'name':'trAlias','tip':'alias value'},None),
            ('lblRg',       (1,0),(1,1),{'name':'lblAlias',
                            'label':_(u'alias')},None),
            #('txtPopup',            (1,1),(1,1),{'name':'txtAlias','value':'','tip':'alias name','mod':False},None),
            ('szBoxHor',    (1,1),(1,1),{'name':'boxAlias',},[
            #    ('chc',                     2,4,{'name':'chcAlias','tip':'alias name','mod':False,
            #                                    'choices':['']},None),
                #('txt',                     1,4,{'name':'txtAlias','value':'','tip':'alias name','mod':False},None),
                #('tgPopup',                 0,0,{'name':'tgAlias'},None),
                ('txtPopup',3,4,{'name':'txtAlias','value':'',
                            'tip':'alias name',
                            'iArrange':2,'mod':False,},None),
                ('txt',     1,0,{'name':'txtAliasOfs','value':'',
                            'tip':'alias offet','mod':False,},None),
                ]),
            #('cbBmpLbl',            (0,4),(1,1),{'name':'cbAliasApply','label':_(u'Apply'),'bitmap':'Apply',},
            #    {'btn':self.OnAliasApply}),
            #(vtInputTreeAttr,       (2,0),(4,4),{'name':'trAliasV','tip':'alias value'},None),
            #('szBoxVert',           (1,1),(1,1),{'name':'boxBt',},[
            #    ('cbBmp',                   0,4,{'name':'cbAliasAdd','bitmap':'Right',},
            #        {'btn':self.OnAliasAdd}),
            #    ('cbBmp',                   0,4,{'name':'cbAliasDel','bitmap':'Left',},
            #        {'btn':self.OnAliasDel}),
            #    ]),
            #('lblCt',               (0,2),(1,1),{'name':'lblAlias','label':_(u'alias')},None),
            ('lstCtrl',     (2,0),(1,2),{'name':'lstAlias','tip':'alias value',
                            'cols':[
                                (_(u'alias'),-1,80,1),
                                (_(u'hierachy'),-1,-1,1),
                                (_(u'attr'),-1,80,1),
                                (_(u'value'),-1,120,1),
                                (_(u'ID'),-2,20,1),
                                ],
                            },{
                                'lst_item_sel':self.OnLstAliasSel,
                            }),
            ('cbBmpLbl',    (1,2),(1,1),{'name':'cbAliasAdd',
                            'label':_(u'Apply'),'bitmap':'Apply',
                            },{
                                'btn':self.OnAliasAdd,
                            }),
            ('szBoxVert',   (2,2),(1,1),{'name':'boxBtCmd',},[
                ('cbBmpLbl',0,4,{'name':'cbAliasDel',
                            'label':_(u'Delete'),'bitmap':'Del',
                            },{
                                'btn':self.OnAliasDel,
                            }),
                ]),
            ]
        lGrowRow=[(0,2),(2,1),]
        lGrowCol=[(0,1),(1,4),]
        vtXmlNodeGuiPanel.__initCtrl__(self,lWid=lWid,VERBOSE=VERBOSE)
        self.SetOriginByWid(self.GetWid())
        
        p=vGuiFrmWX(name='popAlias',parent=self.txtAlias.GetWid(),kind='popup',
                iArrange=0,widArrange=self.txtAlias.GetWid(),bAnchor=True,
                lGrowRow=[(0,1),(1,1)],
                lGrowCol=[(0,1),(1,1)],
                lWid=[
                        ('lstCtrl', (0,0),(2,2),{'name':'lstAlias','size':(300,200),
                                'multiple_sel':False,
                                'cols':[
                                    #['',-3,20,0],
                                    [_(u'name'),-1,-1,1],
                                    [_(u'tag'),-1,80,1],
                                    ],
                                'map':[('name',1),('result',2)],
                                'tip':_(u'action'),
                                },None),
                        ])
        p.BindEvent('ok',self.OnAliasPopupOk)
        p.GetName()
        self.txtAlias.SetWidPopup(p)
        p.__logDebug__(''%())
        #self.SetOriginByWid(self.GetWid())
        
        self.lWid.append(self.lstAlias.GetWid())
        self.__logDebug__({'lWid':self.lWid})
        return lGrowRow,lGrowCol
    def OnAliasApply(self,evt):
        try:
            if self._iVerbose>0:
                self.__logDebug__(''%())
        except:
            self.__logTB__()
    def OnAliasPopupOk(self,evt):
        try:
            if self._iVerbose>0:
                self.__logDebug__(''%())
            p=self.txtAlias.GetWidPopup()
            l=p.lstAlias.GetSelected([-2,-1,0,1])
            #print l
            if len(l)>0:
                sAttr=l[0][1]
                self.txtAlias.SetValue(sAttr)
        except:
            self.__logTB__()
    def doAliasUpdate(self,sID,d):
        try:
            self.__logDebug__({'sID':sID,'d':d})
            node=self.doc.getNodeById(sID)
            if node is not None:
                dVal=self.doc.GetAttrValueToDict(node)#,lang=self.lang)
            else:
                dVal={}
            lK=d.keys()
            for sK in lK:
                if sK in dVal:
                    d[sK]=dVal[sK]
                else:
                    d[sK]='???'
        except:
            self.__logTB__()
    def showAliasOld(self,dAlias):
        try:
            self.__logDebug__({'dAlias:':dAlias})
            lD=[]
            lID=dAlias.keys()
            for sID in lID:
                node=self.doc.getNodeById(sID)
                nBase=self.doc.GetBaseNode(self.node)
                nRel=self.doc.GetRelBaseNode(self.node)
                sT=self.doc.GetTagNames(node,nBase,nRel)
                d=dAlias[sID]
                self.doAliasUpdate(sID,d)
                lK=d.keys()
                for sK in lK:
                    lD.append([(sID,sK),[sT,sK,sID]])
            #print lD
            self.doc.CallBack(self.lstAlias.SetValue,lD)
        except:
            self.__logTB__()
    def showAlias(self,dAlias):
        try:
            self.__logDebug__({'dAlias:':dAlias})
            lD=[]
            lA=dAlias.keys()
            lA.sort()
            dID={}
            for sA in lA:
                t=dAlias[sA]
                sID=t[0]
                sK=t[1]
                if sID not in dID:
                    node=self.doc.getNodeById(sID)
                    nBase=self.doc.GetBaseNode(self.node)
                    nRel=self.doc.GetRelBaseNode(self.node)
                    sT=self.doc.GetTagNames(node,nBase,nRel)
                    dID[sID]=sT
                else:
                    sT=dID[sID]
                    node=self.doc.getNodeById(sID)
                #d=dAlias[sID]
                #self.doAliasUpdate(sID,d)
                #lK=d.keys()
                sV=self.doc.getNodeAttributeValFullLang(node,sK)
                lD.append([sA,[sA,sT,sK,sV,sID]])
            #print lD
            self.doc.CallBack(self.lstAlias.SetValue,lD)
        except:
            self.__logTB__()
    def showAliasPos(self):
        try:
            p=self.txtAlias.GetWidPopup()
            if hasattr(self.objRegNode,'GetLinkList')==False:
                self.doc.CallBack(p.lstAlias.SetValue,[])
                return
            lLink=self.objRegNode.GetLinkList(None,None)
            l=[]
            for t in lLink:
                l.append([t[0],[t[1],t[0]]])
            #print self.objRegNode.GetTagName(),lLink
            self.doc.CallBack(p.lstAlias.SetValue,l)
        except:
            self.__logTB__()
    def OnAliasAdd(self,evt):
        try:
            lAlias=self.txtAlias.GetValue()
            sAlias=lAlias[0]
            sA=sAlias.strip()
            sAliasOfs=self.txtAliasOfs.GetValue()
            sAOfs=sAliasOfs.strip()
            if len(sAOfs)>0:
                sA='_'.join([sA,sAOfs])
            if len(sA)>0:
                sID,lV=self.viEng.GetValue()
                if self._iVerbose>0:
                    self.__logDebug__({'sID':sID,'lV':lV})
                if lV is not None:
                    
                    #if sID in self.dAlias:
                    #    d=self.dAlias[sID]
                    #else:
                    #    d={}
                    #    self.dAlias[sID]=d
                    for sK in lV:
                    #    if sK not in d:
                    #        d[sK]=None
                        self.dAlias[sA]=(sID,sK)
                    #self.doc.DoSafe(self.doAliasUpdate,d)
                    self.doc.DoSafe(self.showAlias,self.dAlias)
                    self.doc.DoCallBack(self.SetModified,True,self.lstAlias.GetWid())
        except:
            self.__logTB__()
    def OnAliasDel(self,evt):
        try:
            sID,lV=self.viEng.GetValue()
            if self._iVerbose>0:
                self.__logDebug__({'sID':sID,'lV':lV})
            lVal=self.lstAlias.GetSelected([-1])
            if len(lVal)>0:
                for lV in lVal:
                    sK=lV[0]
                    if sK in self.dAlias:
                        del self.dAlias[sK]
                self.doc.DoSafe(self.showAlias,self.dAlias)
                self.doc.DoCallBack(self.SetModified,True,self.lstAlias.GetWid())
        except:
            self.__logTB__()
    def OnLstAliasSel(self,evt):
        try:
            if self._iVerbose>0:
                self.__logDebug__('')
            lVal=self.lstAlias.GetSelected([-1])
            if len(lVal)>0:
                sK=lVal[-1][0]
                if sK in self.dAlias:
                    t=self.dAlias[sK]
                    iOfs=sK.find('_')
                    if iOfs>=0:
                        self.txtAlias.SetValue(sK[:iOfs])
                        self.txtAliasOfs.SetValue(sK[iOfs+1:])
                    else:
                        self.txtAlias.SetValue(sK)
                        self.txtAliasOfs.SetValue('')
                    sID=t[0]
                    sA=t[1]
                    self.viEng.SetValue(sID,sA)
        except:
            self.__logTB__()
    def __Clear__(self):
        if self._iVerbose>0:
            self.__logDebug__(''%())
        # add code here
        self.dAlias={}
        self.txtAlias.SetValue('')
        self.txtAliasOfs.SetValue('')
        self.lstAlias.SetValue([])
        #self.doc.CallBack(self.SetModified,True,self.lstAlias.GetWid())
        #self.lFN={}
        #self.lstFN.DeleteAllItems()
        #self.iSelIdx=-1
    def SetNetDocs(self,d):
        self.__logCritical__('FIXME'%())
        return
    def SetRegNode(self,obj):
        self.__logDebug__(''%())
        #self.objRegNode=obj
        vtXmlNodeGuiPanel.SetRegNode(self,obj)
        self.doc.DoSafe(self.showAliasPos)
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        try:
            if self._iVerbose>0:
                self.__logDebug__(''%())
            # add code here
            self.__logDebug__(''%())
            #self.viEng.__SetDoc__(doc,bNet,dDocs)
            self.viEng.SetDoc(doc,bNet)
        except:
            self.__logTB__()
        return
        self.netPrjDoc=None
        self.netDocument=None
        self.netPrj=None
        if dDocs is not None:
            if 'vPrjDoc' in dDocs:
                dd=dDocs['vPrjDoc']
                #self.prjDN=dd['doc'].GetPrjDN()
                self.netPrjDoc=dd['doc']
            if 'vPrj' in dDocs:
                dd=dDocs['vPrj']
                #self.prjDN=dd['doc'].GetPrjDN()
                self.netPrj=dd['doc']
            if 'vDoc' in dDocs:
                self.netDocument=dDocs['vDoc']['doc']
    def __showFiles__(self):
        return
        self.viAlias.Clear()
        self.txtFN.SetValue('')
        self.lstFN.DeleteAllItems()
        self.iSelIdx=-1
        keys=self.lFN.keys()
        keys.sort()
        for k in keys:
            dInfo=self.lFN[k]
            idx=self.lstFN.InsertStringItem(sys.maxint,k)
            i=1
            for kk in ['FN','DN']:
                if kk in dInfo:
                    self.lstFN.SetStringItem(idx,i,dInfo[kk])
                i+=1
    def __SetNode__(self,node):
        try:
            if self._iVerbose>0:
                self.__logDebug__(''%())
                vpc.CallStack()
            if hasattr(self.objRegNode,'GetAttrLinkName')==False:
                return
            # add code here
            for o in self.doc.getChildsNoAttr(self.node,self.doc.attr):
                sAttr=self.doc.getTagName(o)
                sA=self.objRegNode.GetAttrLinkName(sAttr)
                if sA is not None:
                    if self._iVerbose>0:
                        print '    link',sAttr
                    
                    dInfo={}
                    sK=self.doc.getText(o)
                    #sK=self.doc.getNodeText(o,'val')
                    sID=self.doc.getForeignKey(o)
                    lK=sK.split(':')
                    self.dAlias[sA]=(sID,lK[-1])
            
            self.doc.DoSafe(self.showAlias,self.dAlias)
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if self._iVerbose>0:
                self.__logDebug__(''%())
            if hasattr(self.objRegNode,'IsAttrLink')==False:
                return
            
            node=self.GetNodeStart(node)
            if self._iVerbose>0:
                vpc.CallStack()
                print node
            if node is None:
                return
            # add code here
            for o in self.doc.getChildsNoAttr(self.node,self.doc.attr):
                sAttr=self.doc.getTagName(o)
                if self.objRegNode.IsAttrLink(sAttr):
                    #self.doc.deleteNode(o,self.node)
                    self.doc.__deleteNode__(o,self.node)
            keys=self.dAlias.keys()
            keys.sort()
            nBase=self.doc.GetBaseNode(node)
            nRel=self.doc.GetRelBaseNode(node)
            for k in keys:
                t=self.dAlias[k]
                
                sID=t[0]
                n=self.doc.getNodeById(sID)
                if n is not None:
                    sT=self.doc.GetTagNames(n,nBase,nRel)

                    self.doc.createSubNodeDict(node,{
                            'tag':'__link_'+k,'val':':'.join([sT,t[1]]),'attr':('fid',sID),
                            'lst0':[
                                {'tag':'type','val':'link'},
                                {'tag':'val','val':t[1]},
                                ]
                            })
        except:
            self.__logTB__()
    def __Close__(self):
        if self._iVerbose>0:
            self.__logDebug__(''%())
        # add code here
        self.txtAlias.__Close__()
    def __Cancel__(self):
        if self._iVerbose>0:
            self.__logDebug__(''%())
        # add code here
    def __Lock__(self,flag):
        if self._iVerbose>0:
            self.__logDebug__(''%())
        if flag:
            # add code here
            pass
        else:
            # add code here
            pass
    def __setFN__(self,sAlias,dInfo):
        try:
            fn=fnUtil.getFilenameRel2BaseDir(filename,self.prjDN)
            dInfo['fullFN']=filename
            sDN,sFN=os.path.split(dInfo['fullFN'])
            dInfo['DN']=sDN
            dInfo['FN']=sFN
            self.__showFiles__()
        except:
            self.__logTB__()
    def OnCbBrowseButton(self, event):
        event.Skip()
        try:
            bNew=False
            dInfo=self.__getSelInfoDict__()
            if dInfo is not None:
                fn=dInfo['fullFN']
                #self.thdOpen.Do(self.__doOpen__,fn)
                self.prjDN=self.netPrjDoc.GetPrjDN()
                
                if self.VERBOSE:
                    vtLog.CallStack('')
                    print 'file:',fn
                    print 'dn:',self.prjDN
                if fn[0]=='.':
                    filename=fnUtil.getAbsoluteFilenameRel2BaseDir(fn,self.prjDN)
                else:
                    filename=fn
            else:
                bNew=True
                self.prjDN=self.netPrjDoc.GetPrjDN()
                filename=os.path.join(self.prjDN,'')
            if self.dlgFile is None:
                self.dlgFile = wx.FileDialog(self, _(u"Choose a file"), ".", "", _(u"all files (*.*)|*.*"), wx.OPEN)
            try:
                self.dlgFile.SetPath(filename)
                if self.dlgFile.ShowModal() == wx.ID_OK:
                    filename = self.dlgFile.GetPath()
                    # Your code
                    #self.__setFN(dInfo['alias'],filename)
                    #self.txtFN.SetValue('')
                    self.AddFile(self.dlgFile.GetPath(),bNew)
            except:
                self.__logTB__()
        except:
            self.__logTB__()
    def __getSelInfoDict__(self):
        try:
            if self.iSelIdx>=0:
                sAttr=self.lstFN.GetItem(self.iSelIdx).m_text
                if self.VERBOSE:
                    vtLog.CallStack('')
                    print sAttr
                return self.lFN[sAttr]
        except:
            self.__logTB__()
        return None
    def OnCbApplyButton(self, event):
        event.Skip()
        if self.iSelIdx<0:
            return
        try:
            dInfo=self.__getSelInfoDict__()
            if dInfo is not None:
                sAlias=''.join(['file',self.viAlias.GetValue()])
                self.lFN[sAlias]=dInfo
                del self.lFN[dInfo['alias']]
                dInfo['alias']=sAlias
                self.lstFN.SetStringItem(self.iSelIdx,0,sAlias)
                self.SetModified(True,self.lstFN)
            else:
                sAlias=''.join(['file',self.viAlias.GetValue()])
                self.iSelIdx=self.lstFN.InsertStringItem(sys.maxint,sAlias)
                self.AddFile(self.txtFN.GetValue())
                
        except:
            self.__logTB__()
    def OnLstFNListItemDeselected(self, event):
        event.Skip()
        self.iSelIdx=-1
        self.viAlias.Clear()
        self.txtFN.SetValue('')
    def OnLstFNListItemSelected(self, event):
        event.Skip()
        self.iSelIdx=event.GetIndex()
        try:
            sAttr=self.lstFN.GetItem(self.iSelIdx).m_text
            self.viAlias.SetValue(sAttr[4:])
            dInfo=self.__getSelInfoDict__()
            if dInfo is not None:
                self.txtFN.SetValue(dInfo['fullFN'])
        except:
            self.__logTB__()
    def __doOpen__(self,fn):
        try:
            self.netPrjDoc.acquire()
            self.prjDN=self.netPrjDoc.GetPrjDN()
        except:
            pass
        self.netPrjDoc.release()
        return 0
    def DoOpen(self,evt):
        pass
    def OnCbOpenButton(self, event):
        event.Skip()
        try:
            self.__openFile__()
        except:
            self.__logTB__()
    def OnCbDelButton(self, event):
        event.Skip()
        if self.iSelIdx<0:
            return
        try:
            dInfo=self.__getSelInfoDict__()
            if dInfo is not None:
                sAlias=''.join(['file',self.viAlias.GetValue()])
                del self.lFN[dInfo['alias']]
                self.lstFN.DeleteItem(self.iSelIdx)
                self.iSelIdx=-1
                self.viAlias.Clear()
                self.txtFN.SetValue('')
                self.SetModified(True,self.lstFN)
        except:
            self.__logTB__()
    def __getLastUsed__(self,attrBase):
        iLastUsed=-1
        for attrName in self.lFN.keys():
            if attrName[:len(attrBase)]==attrBase:
                if self.lFN[attrName] is not None:
                    try:
                        i=int(attrName[len(attrBase):])
                        if i>iLastUsed:
                            iLastUsed=i
                    except:
                        pass
        return iLastUsed
    def __getNextFree__(self,attrBase):
        i=self.__getLastUsed__(attrBase)
        return u'%s%02d'%(attrBase,i+1)
    def AddFile(self,filename,bNew=True):
        self.prjDN=self.netPrjDoc.GetPrjDN()
        if sys.platform=='win32':
            filename=filename.replace('\\','/')
            prjDN=self.prjDN.replace('\\','/')
        tupVal=fnUtil.getFilenameRel2BaseDir(filename,prjDN)
        if tupVal[0]!=u'.':
            if self.dlgDocSel is None:
                self.dlgDocSel=vAddExternalFileDialog(self)
                self.dlgDocSel.SetNetDocs(self.doc.GetNetDocs())
                self.dlgDocSel.SetLanguage(self.doc.GetLang())
                self.dlgDocSel.Centre()
            if self.dlgDocSel.SetFN(filename)>=0:
                ret=self.dlgDocSel.ShowModal()
            else:
                ret=-1
            if ret in [1,2]:
                node=self.dlgDocSel.GetSelDoc()
                if node is not None:
                    sDestFullFN=self.dlgDocSel.GetFN()
                    sDestDN,sDestFN=os.path.split(sDestFullFN)
                    try:
                        os.makedirs(sDestDN)
                    except:
                        pass
                    nFN=sDestFullFN
                    bOk=False
                    i=0
                    while bOk==False:
                        if i==0:
                            s=''
                        else:
                            s='%d.'%i
                        sExts=nFN.split('.')
                        sExt=sExts[-1]
                        nTestFN=nFN[:-len(sExt)]+s+sExt
                        try:
                            os.stat(nTestFN)
                            i+=1
                        except:
                            nFN=nTestFN
                            bOk=True
                    tupVal=fnUtil.getFilenameRel2BaseDir(nFN,prjDN)
                    if ret==1:
                        vtLog.PrintMsg(_(u'copy to %s ...')%(tupVal),self.widLogging)
                        try:
                            shutil.copy(filename,nFN)
                            vtLog.PrintMsg(_(u'copy to %s done.')%(tupVal),self.widLogging)
                        except:
                            self.__logTB__()
                            vtLog.PrintMsg(_(u'copy to %s failed.')%(tupVal),self.widLogging)
                            return
                    elif ret==2:
                        vtLog.PrintMsg(_(u'move to %s ...')%(tupVal),self.widLogging)
                        try:
                            shutil.move(filename,nFN)
                            vtLog.PrintMsg(_(u'move to %s done.')%(tupVal),self.widLogging)
                        except:
                            self.__logTB__()
                            vtLog.PrintMsg(_(u'move to %s failed.')%(tupVal),self.widLogging)
                            return
                    # add to prjdoc 
                    netPrjDoc=self.doc.GetNetDoc('vPrjDoc')
                    netDoc=self.doc.GetNetDoc('vDoc')
                    sTitle=sDestFN
                    netPrjDoc.SetDocument(netDoc,node,sTitle,str(i),tupVal)
            else:
                return
        if self.VERBOSE:
            vtLog.CallStack('')
            print '   new',bNew
            print '   fn',tupVal
            vtLog.pprint(self.lFN)
        if bNew:
            # generate new link
            sAttr=self.__getNextFree__(u'file')
            if sys.platform=='win32':
                tupVal=tupVal.replace('\\','/')
            dInfo={}
            dInfo['fullFN']=tupVal
            sDN,sFN=os.path.split(dInfo['fullFN'])
            dInfo['DN']=sDN
            dInfo['FN']=sFN
            dInfo['alias']=sAttr#self.doc.getNodeText(o,'alias')
            self.lFN[sAttr]=dInfo
            #self.lFN[sAttr]={'type':'file','val':tupVal}
            self.__showFiles__()
            self.SetModified(True,self.lstFN)
            #idx=self.lstAttr.InsertImageStringItem(sys.maxint, 
            #                                sAttr, -1)
            #self.lstAttr.SetStringItem(idx,1,tupVal,-1)
            #self.__setModified__(True)
        else:
            if sys.platform=='win32':
                tupVal=tupVal.replace('\\','/')
            self.lstFN.SetStringItem(self.iSelIdx,1,tupVal,-1)
            dInfo=self.__getSelInfoDict__()
            if dInfo is not None:
                dInfo['fullFN']=tupVal
                sDN,sFN=os.path.split(dInfo['fullFN'])
                dInfo['DN']=sDN
                dInfo['FN']=sFN
                #dInfo['alias']=sAttr
            self.__showFiles__()
            self.SetModified(True,self.lstFN)
        if self.VERBOSE:
            vtLog.CallStack('')
            vtLog.pprint(self.lFN)
        
    def __openFile__(self):
        try:
            dInfo=self.__getSelInfoDict__()
            if dInfo is not None:
                fn=dInfo['fullFN']
                #self.thdOpen.Do(self.__doOpen__,fn)
                self.prjDN=self.netPrjDoc.GetPrjDN()
                
                sExts=fn.split('.')
                sExt=sExts[-1]
                fileType = wx.TheMimeTypesManager.GetFileTypeFromExtension(sExt)
                if self.VERBOSE:
                    vtLog.CallStack('')
                    print 'file:',fn
                    print 'dn:',self.prjDN
                #vtLog.vtLngCurWX(vtLog.DEBUG,'fn:%s'%(fn),self)
                if fn[0]=='.':
                    filename=fnUtil.getAbsoluteFilenameRel2BaseDir(fn,self.prjDN)
                else:
                    filename=fn
                #vtLog.vtLngCurWX(vtLog.DEBUG,'filename:%s'%(filename),self)
                try:
                    bShell=False
                    bDetached=True
                    if fileType is None:
                        self.__logError__('fn:%s;mime type not found'%(filename))
                        cmd=filename
                        bShell=True
                        bDetached=True
                    else:
                        mime = fileType.GetMimeType()
                        if sys.platform=='win32':
                            filename=filename.replace('/','\\')
                        #vtLog.vtLngCurWX(vtLog.DEBUG,'filename:%s;mime:%r'%(filename,mime),self)
                        cmd = fileType.GetOpenCommand(filename, mime or '')
                        if cmd is None:
                            #cmd=self.ChooseCmd4Mime(fileType,filename,mime)
                            vtLog.PrintMsg(_(u'opening %s failed!')%filename,self.widLogging)
                            #if cmd==0:
                            #    return
                        if cmd is None:
                            sMsg=_(u"Command not found for\n\n%s!")%(filename)
                            dlg=vtmMsgDialog(self,sMsg ,
                                                u'vExplorer',
                                                wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                            dlg.ShowModal()
                            dlg.Destroy()
                            return
                        if cmd[-1]=='*':
                            cmd=filename
                            bShell=True
                            bDetached=True
                    vtLog.PrintMsg(_(u'opening %s ...')%filename,self.widLogging)
                    if cmd.startswith('WX_DDE#'):
                        wx.Execute(cmd)
                    else:
                        p=vtProcess(cmd,shell=bShell,detached=bDetached)
                    try:
                        vtLog.vtLngCurWX(vtLog.DEBUG,'cmd:%s'%(cmd),self)
                    except:
                        pass
                except:
                    self.__logTB__()
                    vtLog.PrintMsg(_(u'opening %s failed!')%filename,self.widLogging)
                    sMsg=u"Cann't open %s!"%(filename)
                    dlg=vtmMsgDialog(self,sMsg ,
                                    u'vXmlNodeEngineeringFileAttr',
                                    wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION)
                    dlg.ShowModal()
                    dlg.Destroy()
        except:
            self.__logTB__()
