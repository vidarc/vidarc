#----------------------------------------------------------------------------
# Name:         vNodeObj.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20150228
# CVS-ID:       $Id: vNodeObj.py,v 1.3 2015/03/01 09:08:59 wal Exp $
# Copyright:    (c) 2015 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeObj(vNodeEngBase):
    def __init__(self,tagName='Obj'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'object')
    def GetTransDescription(self):
        return u'object'
    # ---------------------------------------------------------
    # specific
    def GetProcessNodeInc(self):
        return [
            {
            'lTag':['Base','Obj','Cps','Dsg','Prd','Dsg','Struct','Res','Sec',
                    'Fct','Prc','Prop','itfc','var',
                    'ToDo','Tool',
                    ],
            'level':1,'level_max':4,},
            ]
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'typePrimitive':[
                    _(u'01 type primitive'),u'typePrimitive',-1,
                    {'val':u'int','type':'choice','it00':'int','it01':'char','it02':'float','it03':'long','it04':'bool','it05':'double','it06':'string','it07':'time','it08':'date','it09':'datetime','it10':'uint','it11':'uchar','it12':'ulong'},
                    {'val':u'int','type':'choice','it00':'int','it01':'char','it02':'float','it03':'long','it04':'bool','it05':'double','it06':'string','it07':'time','it08':'date','it09':'datetime','it10':'uint','it11':'uchar','it12':'ulong'},],
            u'typeName':[
                    _(u'10 type name'),u'typeName',-10,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'origin':[
                    _(u'15 origin'),u'origin',-15,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'iopTag':[
                    _(u'20 IO tag'),u'iopTag',-20,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'iopType':[
                    _(u'21 IO type'),u'iopType',-21,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'iopProp':[
                    _(u'22 IO property'),u'iopProp',-22,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'iopDrv':[
                    _(u'23 IO driver'),u'iopDrv',-23,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'iopAdr':[
                    _(u'24 IO address'),u'iopAdr',-24,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'iopTrc':[
                    _(u'26 IO tolerance'),u'iopTrc',-26,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'iopRate':[
                    _(u'27 IO rate'),u'iopRate',-27,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        import imgObj
        return imgObj.getObjectData()
