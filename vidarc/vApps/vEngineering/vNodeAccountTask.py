#----------------------------------------------------------------------------
# Name:         vNodeAccountTask.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: vNodeAccountTask.py,v 1.2 2006/08/29 10:06:25 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeAccountTask(vXmlNodeEngineering):
    def __init__(self,tagName='account_task'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'account task')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01UIDAT8\x8d\xa5\x931k\x021\x1c\xc5_r\xfd"=p\x11\xfa%\nE\xbcoPW\xb5_\
\xc0\xad\xda\xa5H\'u\x92.n\x05\xd7.E\x8b\x83\x1d\xed \xdc\r\x05;\x1cdqH\x87\
\x0e\x1do\xe8\xdds\xd0\xa4w\x9e\x1e\x14\xff\x90!$\xbf\x97\xbc\xf7O\x84\x90\
\x0eN)Y\xb4\xc8$&\x93\x98\xff\x160\xa0\n\x15\xb4\xd6\xd8\x17I\xcfe\x11x\xfe\
\xf2\x0c\x00P\xa1\xb2\x10\x93\x98\xad\xa6\xf7\'\xb2\xcb\x80\x00\xa8BE\xad59\
\xe8Q\x01\x9cL\'\x1c>\x0e\xa9\xb5\xa6\xd9\xd3jz\x04@!\x9d-k\x04T\xa8\xa8\x00\
\x0bs\xd0\xe3\xe2}a\xe1\xee};\x07[\x01#\xc2A\x8f\xbc\xba\xa4\x02\xa8\xd7\x9a\
z]\x0c\x0b\xe9@\x986\x1a\xef\xe9rK.\xdaw-D_\x9f\xe8\x8f\xa6X}\xacP\xbe(CHG\
\xe4B\x14\xd2\x11n\xc9\xcd\xc0\xb5\xeb\x1a~\xbe#\xf4GS\xa8P!\x08\x82|\xcb2~v\
Y\xcc\xdf\xe664\x00\xf4\x97>\xc7O\xe3\x83\x162\x13\xaf\xea\xd9n\x98\xe1/}v\
\x1f\xba\x87\xfdK\x07g\x00P\xadTr\xaf-\xfa\x8d\x10E\x11\xfc\xc0G\xe7\xb6\x93\
\xf1\x9d.\t\x00\xaf\xb3\x99]\xf4\xaa\x1e\xdc\x92\xbb\x85\x97>\x1a7\x8d\xa3p.\
\x83\xb4\x8dz\xb3~\xf4\xda\x07\xdb\xb8_Lb\x16\x9el.p\xeaw\xde\x00\xa6\xdc\
\xed\xf2\x8fgO\x8a\x00\x00\x00\x00IEND\xaeB`\x82' 
