#----------------------------------------------------------------------------
# Name:         __register__.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: __register__.py,v 1.10 2015/03/01 08:46:02 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust

#from vidarc.vApps.vEngineering.vNodeDocuments import vNodeDocuments
#from vidarc.vApps.vEngineering.vNodeDoc import vNodeDoc

if vcCust.is2Import(__name__,'__REG_NODE__',True):
    from vidarc.vApps.vEngineering.instance.vNodeInstance import vNodeInstance
    from vidarc.vApps.vEngineering.admin.vNodeAdmin import vNodeAdmin
    from vidarc.vApps.vEngineering.admin.vNodeEMailBox import vNodeEMailBox
    import vidarc.vApps.vEngineering.__link__
    
def RegisterNodes(doc,oRoot=None):
    try:
        if vcCust.is2Import(__name__,'__REG_NODE__',True):
            oBase=vNodeInstance()
            doc.RegisterNode(oBase,True)
            for o in []:
                doc.RegisterNode(o,False)
            vidarc.vApps.vEngineering.__link__.LinkNodes(doc,[
                    ('instance',['Base','Obj','Cps','Struct',
                            'Dsg','Prd','Res','Sec',
                            'Prop',
                            'hw','sw',
                            'Mod','S88_phy','S88_Prc','S88_procModel',
                            'Documents','drawTiming','drawNode',
                            'itfc','parameters',
                            'dataCollector','DocRef',
                            'Fct','Prc','Bug','Test','Tool','ToDo',]),
                ])
            if oRoot is not None:
                doc.LinkRegisteredNode(oRoot,oBase)
            if vcCust.is2Import(__name__,'__REG_ACCOUNT__',True):
                for sTag in ['instance','Base','Prop','hw','sw','Mod','S88_phy','S88_Prc','S88_procModel']:
                    vidarc.vApps.vEngineering.__link__.LinkNodes(doc,
                                        [(sTag,['account','drawTiming','drawNode','sw','hw',]),])
    except:
        vtLog.vtLngTB(__name__)
