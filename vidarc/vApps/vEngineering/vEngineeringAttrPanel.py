#----------------------------------------------------------------------------
# Name:         vEngineeringAttrPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060618
# CVS-ID:       $Id: vEngineeringAttrPanel.py,v 1.7 2011/01/11 23:50:26 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.tool.input.vtInputAttrCfgMLPanel import vtInputAttrCfgMLPanel
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    import vidarc.tool.log.vtLog as vtLog
    from vidarc.tool.input.vtInputValue import vtInputValue
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vEngineeringAttrPanel(vtInputAttrCfgMLPanel):
    VERBOSE=0
    def __init__(self, *args,**kwargs):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        apply(vtInputAttrCfgMLPanel.__init__,(self,) +args,kwargs)
        if VERBOSE>0:
            self.__logDebug__(''%())
    def __extendImageList__(self):
        keys=self.dCfg.keys()
        l=[]
        for k in keys:
            l.append(self.dCfg[k])
        l.sort()
        try:
            iSel=self.cmbAttrName.GetSelection()
            iIDSel=self.cmbAttrName.GetClientData(iSel)
        except:
            iSel=0
            iIDSel=-1
        self.cmbAttrName.Clear()
        iSel=0
        i=0
        self.imgLstTyp.Add(vtArt.getBitmap(vtArt.Error))
        for tup in l:
            nodeTmp=self.doc.getNodeByIdNum(tup[2])
            sImg=self.doc.getNodeText(nodeTmp,'img')
            if len(sImg)>0:
                if sImg[:3]=='wx.':
                    bmp = wx.ArtProvider_GetBitmap(eval(sImg), eval('wx.ART_TOOLBAR'), (16,16))
                elif sImg[:6]=='vtArt.':
                    bmp=vtArt.getBitmap(eval(sImg))
                else:
                    stream = cStringIO.StringIO(binascii.unhexlify(sImg))
                    bmp=wx.BitmapFromImage(wx.ImageFromStream(stream))
                self.imgDict[tup[1]]=self.imgLstTyp.Add(bmp)
            self.cmbAttrName.Append(tup[0],tup[2])
            if tup[2]==iIDSel:
                iSel=i
            i+=1
        self.lstAttr.SetImageList(self.imgLstTyp,wx.IMAGE_LIST_SMALL)
        
        self.cmbAttrName.SetSelection(iSel)
        wx.CallAfter(self.OnCmbAttrNameCombobox,None)

    def SetRegNode(self,obj):
        try:
            self.__logDebug__('tag:%s;class:%s'%(obj.GetTagName(),obj.__class__.__name__))
        except:
            pass
        self.objRegNode=obj
        if hasattr(obj,'GetCfgBase'):
            self.SetTagNames2Base(obj.GetCfgBase())
            if self.doc is not None:
                self.__buildCfg__()
                self.__extendImageList__()
    def __SetNode__(self,node):
        bDbg=False
        if VERBOSE>0:
            self.__logDebug__(''%())
            bDbg=True
        try:
            lang=self.viLgSel.GetValue()
            bCfgAdded=False
            for o in self.doc.getChildsNoAttr(self.node,self.doc.attr):
                sAttrName=self.doc.getTagName(o)
                sAttr,sAttrSuf=self.__getAttrNameSuff__(sAttrName)
                if self.funcIs2Skip is not None:
                    if self.funcIs2Skip(sAttr):
                        continue
                if self.objRegNode.IsAttrHidden(sAttr):
                    continue
                if self.__hasAttrKey__(sAttr):
                    if self.doc.hasChilds(o):
                        if self.__check2addCfg__(sAttr,o)==False:
                            #self.doc.deleteNode(o,self.node)
                            self.doc.__deleteNode__(o,self.node)
                            continue
                    s=self.doc.getText(o)
                    if self.attrs.has_key(sAttrName)==False:
                        self.attrs[sAttrName]={}
                    sLg=self.doc.getAttribute(o,'language')
                    self.attrs[sAttrName][sLg]=s
                else:
                    # add faulty
                    if bDbg:
                        self.__logDebug__('add faulty;tagname:%s'%(sAttr))
                    #s=self.doc.getText(o)
                    if self.__check2addCfg__(sAttr,o)==False:
                        #self.doc.deleteNode(o,self.node)
                        self.doc.__deleteNode__(o,self.node)
                        continue
                    else:
                        bCfgAdded=True
                    s=self.doc.getAttributeVal(o)
                    self.attrsFlt[sAttrName]=s
            if bCfgAdded:
                self.__buildCfg__()
                self.__extendImageList__()
            self.__showAttrs__()
        except:
            self.__logTB__()
    def __check2addCfg__(self,sAttr,o):
        try:
            bDbg=False
            if VERBOSE>0:
                self.__logDebug__('sAttr:%s;node:%s;dCfg:%s'%(sAttr,o,vtLog.pformat(self.dCfg)))
                bDbg=True
            if sAttr=='inheritance_type':
                return False
            #if sAttr.find('file')==0:
            #    sAttr2Cfg='file'
            #    sAttrIdx=sAttr[4:]
            #else:
            #    sAttr2Cfg=sAttr
            iIdx=sAttr.find('_')
            if iIdx>0:
                sAttr2Cfg=sAttr[:iIdx]
                sAttrIdx=sAttr[iIdx+1:]
            elif iIdx==0:
                return False
            else:
                sAttr2Cfg=sAttr
                sAttrIdx=''
            nodeAttr=None
            if sAttr2Cfg in self.dCfg:
                iID=self.dCfg[sAttr2Cfg][2]
                nodeAttr=self.doc.getNodeByIdNum(iID)
            #iID=self.dCfg[sAttr2Cfg][2]
            #node=self.doc.getNodeByIdNum(iID)
            if nodeAttr is None:
                nodeCfg=self.doc.getChildByLst(self.doc.getBaseNode(),self.lTagNames2Base)
                if 0:
                    try:
                        if self.doc.IsLocked(nodeCfg)==False:
                            if bDbg:
                                self.__logDebug__('node cfg:%s is not locked'%(nodeCfg))
                            return
                        if bDbg:
                            self.__logDebug__('node cfg:%s is locked'%(nodeCfg))
                    except:
                        pass
                    if bDbg:
                        self.__logDebug__('node cfg:%s'%(nodeCfg))
                #nodeAttrML=self.doc.getChild(nodeCfg,'attrML')
                if bDbg:
                    self.__logDebug__('node attrML:%s'%(nodeAttr))
                if nodeAttr is None:
                    lTypes=vtInputValue.TYPES
                    sType=self.doc.getNodeText(o,'type')
                    if bDbg:
                        self.__logDebug__('type:%s types:%s'%(sType,vtLog.pformat(lTypes)))
                    if sType in lTypes:
                        dTypes=vtInputValue.TYPE_DICT
                        d2Create={}
                        l2Create=[]
                        if sType in dTypes:
                            lAttrs=dTypes[sType]
                            for sA in lAttrs:
                                if sA.find('it')==0:
                                    continue
                                dTmp={'tag':sA,'val':''}
                                l2Create.append(dTmp)
                                d2Create[sA]=dTmp
                        sVal=''
                        for c in self.doc.getChilds(o):
                            sTagA=self.doc.getTagName(c)
                            if sTagA.find('it')==0:
                                dTmp={'tag':sTagA,'val':''}
                                l2Create.append(dTmp)
                                d2Create[sTagA]=dTmp
                                d2Create[sTagA]['val']=self.doc.getText(c)
                            if sTagA in d2Create:
                                d2Create[sTagA]['val']=self.doc.getText(c)
                                if sTagA=='val':
                                    sVal=d2Create[sTagA]['val']
                        l2Create.append({'tag':'type','val':sType})
                        if bDbg:
                            self.__logDebug__('node attrML:%s;l2Create:%s;d2Create:%s'%(nodeAttr,vtLog.pformat(l2Create),vtLog.pformat(d2Create)))
                        l2Cfg=[
                                {'tag':'tag','val':sAttr2Cfg},
                                {'tag':'img'},]
                        langIds=self.doc.GetLanguageIds()
                        for lang in langIds:
                            l2Cfg.append({'tag':'name','attr':('language',lang),'val':sAttr2Cfg})
                        l2Cfg.append({'tag':sAttr2Cfg,'lst':l2Create})
                        nodeAttr=self.doc.createSubNodeDict(nodeCfg,{'tag':'attrML','lst':l2Cfg})
                        #self.dlgCfg.GetNode(c)
                        self.doc.AlignNode(nodeAttr)
                        self.doc.addNode(nodeCfg,nodeAttr)
                        if bDbg:
                            self.__logDebug__('node attrMLCfg:%s'%(c))
                        fid=self.doc.getKey(nodeAttr)
                        for c in self.doc.getChilds(o):
                            #self.doc.deleteNode(c,o)
                            self.doc.__deleteNode__(c,o)
                        self.doc.setAttribute(o,'fid',fid)
                        self.doc.setText(o,sVal)
                        return True
            else:
                if bDbg:
                    self.__logDebug__('node:%s;node attrMLCfg:%s'%(o,nodeAttr))
                sVal=self.doc.getNodeText(o,'val')
                fid=self.doc.getKey(nodeAttr)
                for c in self.doc.getChilds(o):
                    #self.doc.deleteNode(c,o)
                    self.doc.__deleteNode__(c,o)
                self.doc.setAttribute(o,'fid',fid)
                self.doc.setText(o,sVal)
                if bDbg:
                    self.__logDebug__('node:%s;node attrMLCfg:%s'%(o,nodeAttr))
                return True
        except:
            self.__logTB__()
        return False
