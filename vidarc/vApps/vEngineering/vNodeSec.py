#----------------------------------------------------------------------------
# Name:         vNodeSec.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20150228
# CVS-ID:       $Id: vNodeSec.py,v 1.2 2015/03/01 09:08:59 wal Exp $
# Copyright:    (c) 2015 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeSec(vNodeEngBase):
    def __init__(self,tagName='Sec'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'section')
    def GetTransDescription(self):
        return u'section'
    # ---------------------------------------------------------
    # specific
    def GetProcessNodeInc(self):
        return [
            {
            'lTag':['Base','Obj','Cps','Struct','Dsg','Prd','Res','Sec',
                    'Fct','Prc','Prop','itfc','var',
                    'ToDo','Tool',
                    ],
            'level':1,'level_max':4,},
            ]
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            #u'typical':[
            #        _(u'12 typical'),u'typical',-12,
            #        {'val':u'','type':'text',},
            #        {'val':u'','type':'text',}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        import imgObj
        return imgObj.getSectionsData()
