#----------------------------------------------------------------------------
# Name:         __register__.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: __register__.py,v 1.9 2015/03/01 08:46:02 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust

#from vidarc.vApps.vEngineering.vNodeDocuments import vNodeDocuments
#from vidarc.vApps.vEngineering.vNodeDoc import vNodeDoc

if vcCust.is2Import(__name__,'__REG_NODE__',True):
    from vidarc.vApps.vEngineering.admin.vNodeAdmin import vNodeAdmin
    from vidarc.vApps.vEngineering.admin.vNodeEMailBox import vNodeEMailBox
    import vidarc.vApps.vEngineering.__link__
    
def RegisterNodes(doc,oRoot=None):
    try:
        if vcCust.is2Import(__name__,'__REG_NODE__',True):
            oBase=vNodeAdmin()
            doc.RegisterNode(oBase,True)
            for o in [vNodeEMailBox()]:
                doc.RegisterNode(o,False)
            vidarc.vApps.vEngineering.__link__.LinkNodes(doc,[
                    ('admin',['eMailBox','Documents','dataCollector',
                        'Base','Cps','Obj','Dsg','Prd','Res','Struct',
                        'Prc','Fct',
                        'DocRef','Prop','parameters',
                        'Exp','Bug','Test','Tool','ToDo',
                        'hw','sw','itfc','var','S88_phy',]),
                ])
            if oRoot is not None:
                doc.LinkRegisteredNode(oRoot,oBase)
            if vcCust.is2Import(__name__,'__REG_ACCOUNT__',True):
                for sTag in ['admin']:
                    vidarc.vApps.vEngineering.__link__.LinkNodes(doc,
                                        [(sTag,['account',]),])
    except:
        vtLog.vtLngTB(__name__)
