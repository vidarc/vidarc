#----------------------------------------------------------------------------
# Name:         vNodeAdmin.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: vNodeAdmin.py,v 1.2 2006/08/29 10:06:25 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeAdmin(vXmlNodeEngineering):
    def __init__(self,tagName='admin'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'administration')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsBase(self):
        "is base node"
        return True
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\x8dIDAT8\x8d\xa5\x93\xb1j\xdbP\x14\x86?\xc9\x1e\xee\xe0\x80.t\x90\
\xa0\x8b\x87\x0e\x81>@e\xec\x07hFC\x1e\xa0\x98N\x1d\xf3\x1e\x9d\xf2\x16\x01o\
q\xa7N.\xca\x98N\xd1P\x83\x86\x0e24\xa0\x0b\r\xf8\x0c\x92N\x87\xd4\xc2\xf6U\
\xa0\xd0\x03g\xf9\xe1\xff\xef9\xff\x7fn\x10\x84\x03\xfe\xa7\x86}\xa0\xb6\x8d\
\x9ebA8\x08\xfeI@\xdbFoW\xb7\xdc\x7f\xbf\'[g\x94e\x89s\x8e\xcaU\xea\x9c\xf3\
\x84\xc2S\xf2\xc5\xfb\x0b\x92$a\xfcz\xdc\xe1\xc6\x18ld\x89\xe3\xd8\x9b\xeeH\
\x00@j\xa1,K\xa2W\xd1\x11\xbe\xabw}\x1b\x10\x1c\x9a\xa8m\xa3\xe7o\xcf\x11\
\x11\xe28f\xbb\xddR=VH-\x18c\xa0\x06o\x8d \x1ct\r\xe85\xa8\xbeC\xf5\xf3\'U\
\xd0\x12\xf4+\xe8\x15(\x7f\xfb\x90\xe3M\xb0\xf8\xb0 \x9d\xa5\xc8\x93<\x9b\
\xf7\xbbBD(6\x05\xab/\xab\xfd\xa3\xdd\x04\x9d\x80\xb6\x8d.>.x\xc8\x1f\xb0\
\xd6b\x86\x06\xa9\x05\xeag_\xaa_\x15\x00\xe37c\x967\xcbNh\xb8\'\xa7\xd3\x94\
\xea\xb1\xc2ZK\xf9\xb3$\xdf\xe4\xddd\xd1("\x89\x13\xcc\xc8P\x14\x05\x00\xd9:\
c2\x9bhw\x07\xf6\xcc""\xe4?rv\xb2#\x1a\x1d\xa7\xe0\x9c\x83\'(6\x05\xf3\xcb9\
\x93\xd9\x84 \x1c\x04G+\xa4\xd3\x14\xe7\x1c"\xd2\x1b\xd9\x9e\xbc\xbcYv>x&\
\x02\xa4\xd3\xd4#\xdf}\xbb\xf3\xc8^\x8c\x87q\x9e\xf6\xfcr\xeeG\x18\x0e\xfa\
\x05N\xc5\xb2u\xd6K\xf6\xee\xe0\xa5\xd2\xb6\xd1\x97~\xe3\x1f\x16\xa8\xcd\x91\
.\xa9r\xb1\x00\x00\x00\x00IEND\xaeB`\x82' 
