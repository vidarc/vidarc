#----------------------------------------------------------------------------
# Name:         vXmlNodeEngineeringActivity.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060814
# CVS-ID:       $Id: vXmlNodeEngineeringActivity.py,v 1.10 2013/06/17 11:51:05 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.lang.vtLgDictML import vtLgDictML
from vidarc.ext.state.veStateAttr import veStateAttr
from vidarc.tool.time.vtTime import vtDateTime
import os,os.path

from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        from vXmlNodeEngineeringActivitiesPanel import *
        #from vNodeXXXEditDialog import *
        #from vNodeXXXAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
    if GUI>0:
        import vidarc.vApps.vHum.vHumReg as vHumReg
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeEngineeringActivity(vtXmlNodeBase,veStateAttr):
    VERBOSE=0
    NODE_ATTRS=[
            #('Tag',None,'tag',None),
            #('DtTm',None,'datetime',None),
            #('ActState',None,'actState',None),
            #('Nr',None,'nr',None),
            ('Name',None,'name',None),
            ('Desc',None,'desc',None),
            ('Start',None,'start',None),
            ('End',None,'end',None),
            ('Prior',None,'prior',None),
            ('Severity',None,'severity',None),
            ('Usr','UsrId','usr','fid'),
        ]
    FUNCS_GET_SET_4_LST=['ActStateTag','Nr','Name','Prior','Severity','Start','End','DtTm','Author','Prev']
    SORT_ORDER=[3,4,0,6,2]
    MAP_PRIOR_SORT={u'highest':2,u'higher':1,u'normal':0,u'lower':-1,u'lowest':-2}
    MAP_PRIOR_IDX={u'highest':0,u'higher':1,u'normal':2,u'lower':3,u'lowest':4}
    MAP_SEVERITY_SORT={u'blocker':0, u'critical':1, u'major':2,
                  u'normal':3, u'minor':4, u'trivial':5, u'enhancement':6}
    AUTHOR_INFOS=['surname','firstname']
    AUTHOR_LOGIN_INFOS=['name']
    
    def __init__(self,tagName='Activity'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vtXmlNodeBase.__init__(self,tagName)
        veStateAttr.__init__(self,'smActivity','actState')
        self.__origAuthorInfos=':'.join([self.GetClsName(),'author'])
        self.__origAuthorLoginInfos=':'.join([self.GetClsName(),'authorLogin'])
        self.oSMlinker=None
        self.dt=vtDateTime(True)
        self.lang=None
        self.lgDict=None
        if GUI>0:
            self.__getTranslation__()
    def __getTranslation__(self):
        if self.lgDict is not None:
            return
        try:
            domain=__name__.split('.')[-2]
            dn=vtLgBase.getApplBaseDN()
            l=vtLgBase.getPossibleLang(domain,dn)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurCls(vtLog.DEBUG,'domain:%s;langs:%s'%(domain,vtLog.pformat(l)),self)
            if l is not None:
                try:
                    self.lgDict=vtLgDictML(domain,langs=l)
                except:
                    self.lgDict=vtLgDictML(domain,dn=os.path.split(__file__)[0],langs=l)
            else:
                self.lgDict=None
                vtLog.vtLngCurCls(vtLog.WARN,'translation not found',self)
            if self.VERBOSE:
                vtLog.CallStack('')
                print domain
                print l
                vtLog.pprint(self.lgDict)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurCls(vtLog.DEBUG,'lgDict:%s'%(vtLog.pformat(self.lgDict)),self)
        except:
            self.lgDict=None
            vtLog.vtLngTB(self.__class__.__name__)
    def SetLang(self,lang):
        self.lang=lang
    def GetTrans(self,msg):
        if self.VERBOSE:
            vtLog.CallStack('')
            vtLog.pprint(self.lgDict)
            print msg
        if self.lgDict is None:
            return msg
        if self.VERBOSE:
            print msg,self.lang,self.lgDict.Get(msg,self.lang)
        return self.lgDict.Get(msg,self.lang)
    def SetTransDict(self,lgDict):
        self.lgDict=lgDict
    def GetSeverityTrans(self):
        try:
            lSev=self.MAP_SEVERITY_SORT.items()
            def cmpFunc(a,b):
                return cmp(a[1],b[1])
            lSev.sort(cmpFunc)
            l=[self.GetTrans(tup[0]) for tup in lSev]
            return l
        except:
            return None
    def GetPriorTrans(self):
        try:
            lSort=self.MAP_PRIOR_SORT.items()
            def cmpFunc(a,b):
                return cmp(a[1],b[1])
            lSort.sort(cmpFunc)
            l=[self.GetTrans(tup[0]) for tup in lSort]
            return l
        except:
            return None
    def GetDescription(self):
        return _(u'activities')
    # ---------------------------------------------------------
    # specific
    def GetDtTm(self,node):
        return self.Get(node,'datetime')
    def GetNr(self,node):
        return str(self.GetAttr(node,'actID'))
    def GetPrev(self,node):
        return self.Get(node,'prev')
    def GetTag(self,node):
        nodePar=self.doc.getParent(node)
        k=self.doc.getKey(nodePar)
        return '_'.join([k,self.GetNr(node)])
    def GetName(self,node,lang=None):
        return self.GetML(node,'name',lang)
    def GetDesc(self,node,lang=None):
        return self.GetML(node,'desc',lang)
    def GetStart(self,node):
        return self.Get(node,'start')
    def GetEnd(self,node):
        return self.Get(node,'end')
    def GetPrior(self,node):
        return self.Get(node,'prior')
    def GetPriorIdx(self,node):
        if node is None:
            return 2
        sVal=self.GetPrior(node)
        if sVal in self.MAP_PRIOR_IDX:
            return self.MAP_PRIOR_IDX[sVal]
        else:
            return 2
    def GetSeverity(self,node):
        return self.Get(node,'severity')
    def GetSeverityIdx(self,node):
        if node is None:
            return 3
        sVal=self.GetSeverity(node)
        if sVal in self.MAP_SEVERITY_SORT:
            return self.MAP_SEVERITY_SORT[sVal]
        else:
            return 3
    def GetUsrID(self,node):
        return self.GetChildForeignKey(node,'usr','fid',appl='vHum')
    def GetAuthor(self,node):
        id=self.doc.getChildAttribute(node,'usr','fid')
        if id.find('@')<0:
            id+='@vHum'
        tag,d=self.doc.GetInfos(id,name=self.__origAuthorInfos)
        if tag!='user':
            return '???'
        if len(d)==0:
            return '???'
        try:
            return  ' '.join(map(d.__getitem__,self.AUTHOR_INFOS))
        except:
            return '???'
    def GetAuthorLogin(self,node):
        id=self.doc.getChildAttribute(node,'usr','fid')
        if id.find('@')<0:
            id+='@vHum'
        tag,d=self.doc.GetInfos(id,name=self.__origAuthorLoginInfos)
        if tag!='user':
            return '???'
        if len(d)==0:
            return '???'
        try:
            return  ' '.join(map(d.__getitem__,self.AUTHOR_LOGIN_INFOS))
        except:
            return '???'
    def GetTagPar(self,node):
        nodePar=self.doc.getParent(node)
        o=self.doc.GetRegByNode(nodePar)
        try:
            return o.GetTag(nodePar)
        except:
            return '???'
        return self.Get(nodePar,'tag')
    def GetTagHier(self,node,nodeBase=None,nodeRel=None):
        if nodeBase is None:
            nodeBase=self.doc.getBaseNode()
        if nodeRel is None:
            nodeRel=self.doc.getBaseNode()
        nodePar=self.doc.getParent(node)
        return self.doc.GetTagNames(nodePar,nodeBase=nodeBase,nodeRel=nodeRel)
    def GetNamePar(self,node,lang=None):
        nodePar=self.doc.getParent(node)
        o=self.doc.GetRegByNode(nodePar)
        try:
            return o.GetName(nodePar,lang=lang)
        except:
            return '???'
        return self.GetML(nodePar,'name',lang)
    def GetDescPar(self,node,lang=None):
        nodePar=self.doc.getParent(node)
        o=self.doc.GetRegByNode(nodePar)
        try:
            return o.GetDesc(nodePar,lang=lang)
        except:
            return '???'
        return self.GetML(nodePar,'description',lang)
    
    def AddValueToDictByDict(self,node,dDef,dVal,bMultiple,*args,**kwargs):
        try:
            if 'lang' in kwargs:
                lang=kwargs['lang']
            else:
                lang=None
        except:
            lang=None
        #print kwargs
        #print kwargs.keys()
        if 'nr' in dDef:
            dVal['nr']=self.GetNr(node)
        if 'prev' in dDef:
            dVal['prev']=self.GetPrev(node)
        if 'author' in dDef:
            dVal['author']=self.GetAuthor(node)
        if 'authorLogin' in dDef:
            dVal['authorLogin']=self.GetAuthorLogin(node)
        if 'tagPar' in dDef:
            dVal['tagPar']=self.GetTagPar(node)
        if 'tagHier' in dDef:
            dVal['tagHier']=self.GetTagHier(node)
        if 'namePar' in dDef:
            dVal['namePar']=self.GetNamePar(node,lang)
        if 'descPar' in dDef:
            dVal['descPar']=self.GetDescPar(node,lang)
        if 'priorIdx' in dDef:
            dVal['priorIdx']=self.GetPriorIdx(node)
        if 'severityIdx' in dDef:
            dVal['severityIdx']=self.GetSeverityIdx(node)
        self.doc.__AddValueToDictByDict__(node,dDef,dVal,bMultiple,*args,**kwargs)
        return 0
    def SetInfos2Get(self):
        self.doc.SetInfos2Get(self.AUTHOR_INFOS,foreign='vHum',name=self.__origAuthorInfos)
        self.doc.SetInfos2Get(self.AUTHOR_LOGIN_INFOS,foreign='vHum',name=self.__origAuthorLoginInfos)
    def SetDtTm(self,node,val):
        return self.Set(node,'datetime',val)
    def SetNr(self,node,val):
        self.SetAttr(node,'actID',val)
    def SetPrev(self,node,val):
        self.Set(node,'prev',val)
    def SetName(self,node,val):
        self.SetML(node,'name',val)
    def SetDesc(self,node,val):
        self.SetML(node,'desc',val)
    def SetStart(self,node,val):
        self.Set(node,'start',val)
    def SetEnd(self,node,val):
        self.Set(node,'end',val)
    def SetPrior(self,node,val):
        self.Set(node,'prior',val)
    def SetPriorIdx(self,node,idx):
        if node is None:
            return 
        sVal=self.GetPrior(node)
        for k,v in self.MAP_PRIOR_IDX.items():
            if v==idx:
                self.SetPrior(node,k)
                return
    def SetSeverity(self,node,val):
        self.Set(node,'severity',val)
    def SetSeverityIdx(self,node,idx):
        if node is None:
            return
        for k,v in self.MAP_SEVERITY_SORT.items():
            if v==idx:
                self.SetSeverity(node,k)
                return
    def SetUsr(self,node,val):
        return self.Set(node,'usr',val)
    def SetUsrID(self,node,val):
        return self.SetChildForeignKey(node,'usr','fid',val,appl='vHum')
    def SortAndTranslateActivities(self,logs,sortIdx=None,iMode=0):
        if self.VERBOSE:
            vtLog.CallStack('')
            vtLog.pprint(logs)
        ll=[]
        if sortIdx is None:
            sortIdx=self.SORT_ORDER
        def cmpFunc(a,b):
            for idx in sortIdx:
                iRet=cmp(a[0][idx],b[0][idx])
                if iRet!=0:
                    return iRet
            return 0
        logs.sort(cmpFunc)
        for log,id in logs:
            if iMode==1:
                l=[-1]
            else:
                l=[]
            for i in xrange(len(self.FUNCS_GET_SET_4_LST)):
                if i==3:
                    # severity
                    if len(log[i])>0:
                        val=self.GetTrans(log[i])
                    else:
                        val=self.GetTrans(_(u'normal'))
                elif i==4:
                    if len(log[i])>0:
                        val=self.GetTrans(log[i])
                    else:
                        val=self.GetTrans(_(u'normal'))
                else:
                    val=log[i]
                l.append(val)
            if iMode==1:
                ll.append([id,l])
            else:
                l.append(id)
                ll.append(l)
            
        return ll
    def GetActStateTag(self,node):
        state=self.GetActState(node)
        if state is None:
            return '???'
        else:
            return state[0]
    # ---------------------------------------------------------
    # state machine related
    def GetOwner(self,node):
        return self.GetChildForeignKey(node,self.smAttr,'owner','vHum')
    def GetActState(self,node):
        if self.oSMlinker is None:
            self.oSMlinker=self.doc.GetRegisteredNode('stateMachineLinker')
        try:
            oRun=self.oSMlinker.GetRunner(self.tagName,self.smAttr)
            id=self.GetChildAttr(node,self.smAttr,'fid')
            state=oRun.GetState(id)
            return state
            #print oRun.GetTransitions(id)
            #return self.Get(node,'state01')
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return None
    def SetActState(self,node,val):
        if self.oSMlinker is None:
            self.oSMlinker=self.doc.GetRegisteredNode('stateMachineLinker')
        try:
            oRun=self.oSMlinker.GetRunner(self.tagName,self.smAttr)
            c=self.GetChildForced(node,self.smAttr)
            oLogin=self.doc.GetLogin()
            if oLogin is not None:
                idLogin=oLogin.GetUsrId()
            else:
                vtLog.vtLngCurCls(vtLog.ERROR,'non loggedin',self)
                idLogin=-1
            self.SetAttr(c,'fid',oRun.GetInitID())
            self.SetOwner(node,idLogin)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def SetOwner(self,node,val):
        self.SetChildForeignKey(node,self.smAttr,'owner',val,'vHum')
    
    def GetActionCmdDict(self):
        return {
            'Clr':(_('clear'),None),
            'Go':(_('activity go'),None),
            'NoGo':(_('activity no go'),None),
            'Done':(_('activity done'),None),
            'Rejected':(_('activity rejected'),None),
            }
    def DoActionCmdClr(self,node,*args,**kwargs):
        vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
        #print node
        self.Set(node,'result','0')
    def DoActionCmdGo(self,node,*args,**kwargs):
        vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
        #print node
        self.Set(node,'result','1')
    def DoActionCmdNoGo(self,node,*args,**kwargs):
        vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
        #print node
        self.Set(node,'result','-1')
    def DoActionCmdDone(self,node,*args,**kwargs):
        vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
        #print node
        self.Set(node,'result','10')
    def DoActionCmdRejected(self,node,*args,**kwargs):
        vtLog.vtLngCurCls(vtLog.DEBUG,'',self)
        #print node
        self.Set(node,'result','19')
    
    def GetMsgPrior(self,node):
        return str(self.GetPriorIdx(node))
    def GetMsgTag(self,node,lang=None):
        return self.GetTag(node)
    def GetMsgName(self,node,lang=None):
        return self.GetName(node,lang)
    def GetMsgInfo(self,node,lang=None):
        return ''
    
    def IsActive(self,node):
        iNum=self.GetVal(node,'result',int,0)
        if iNum<0:
            return False
        elif iNum<10:
            return True
        return False
    # ---------------------------------------------------------
    # inheritance
    def SetDoc(self,doc,bNet=False):
        vtXmlNodeBase.SetDoc(self,doc)
        if self.doc is None:
            self.lgDict=None
            return
        if GUI<=0:
            return
        if vcCust.is2Import(__name__):
            l=[]
            for tup in self.doc.GetLanguages():
                l.append(tup[1])
            if len(l)==0:
                l=['en','de','fr']
            try:
                self.lgDict=vtLgDictML('vEngineering',langs=l)
            except:
                try:
                    self.lgDict=vtLgDictML('vEngineering',dn=os.path.split(__file__)[0],langs=l)
                except:
                    self.lgDict=None
                    vtLog.vtLngCurCls(vtLog.ERROR,'translation veReportDoc not found',self)
                    vtLog.vtLngTB(self.__class__.__name__)
        vHumReg.Register4Collector(doc,self.GetTagName(),'Activity:author',_('activities')+_(u'author'))
    def __createPost__(self,node,func=None,*args,**kwargs):
        try:
            if self.doc is None:
                return
            if node is not None:
                self.dt.Now()
                self.SetDtTm(node,self.dt.GetDateSmallStr())
                actIds=self.doc.GetSortedActNrIds()
                self.SetNr(node,actIds.GetFirstFreeId())
                actIds.AddNodeSingle(node,'actID',None,self.doc,bSort=True)
            oLogin=self.doc.GetLogin()
            if oLogin is None:
                vtLog.vtLngCurCls(vtLog.ERROR,'no login info',self)
                return
            if oLogin.GetUsrId()<0:
                vtLog.vtLngCurCls(vtLog.ERROR,'invalid usr id:%d'%oLogin.GetUsrId(),self)
                return
            self.SetUsr(node,oLogin.GetUsr())
            self.SetUsrID(node,oLogin.GetUsrId())
            if self.oSMlinker is None:
                self.oSMlinker=self.doc.GetRegisteredNode('stateMachineLinker')
            if self.oSMlinker is not None:
                oRun=self.oSMlinker.GetRunner(self.tagName,self.smAttr)
            else:
                oRun=None
            if oRun is None:
                vtLog.vtLngCurCls(vtLog.ERROR,'no state runner for %s:%s found'%(self.smName,self.smAttr),self)
                return
            if oRun.GetInitID()<0:
                vtLog.vtLngCurCls(vtLog.ERROR,'faulty initial id:%d'%(oRun.GetInitID()),self)
                return
            if node is not None:
                oRun.Log(oRun.STATE_INIT,self.doc,node,oLogin)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL|vtSecXmlAclDom.ACL_MSK_MOVE
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return [
                ('datetime',vtXmlFilterType.FILTER_TYPE_DATE_TIME),
                ('nr',vtXmlFilterType.FILTER_TYPE_INT),
                ('name',vtXmlFilterType.FILTER_TYPE_STRING),
                ('desc',vtXmlFilterType.FILTER_TYPE_STRING),
                ('start',vtXmlFilterType.FILTER_TYPE_DATE),
                ('end',vtXmlFilterType.FILTER_TYPE_DATE),
                ('prior',vtXmlFilterType.FILTER_TYPE_STRING),
                ('priorIdx',vtXmlFilterType.FILTER_TYPE_INT),
                ('severity',vtXmlFilterType.FILTER_TYPE_STRING),
                ('severityIdx',vtXmlFilterType.FILTER_TYPE_INT),
                ('prev',vtXmlFilterType.FILTER_TYPE_INT),
                ('author',vtXmlFilterType.FILTER_TYPE_STRING),
                ('authorLogin',vtXmlFilterType.FILTER_TYPE_STRING),
                ('tagPar',vtXmlFilterType.FILTER_TYPE_STRING),
                ('tagHier',vtXmlFilterType.FILTER_TYPE_STRING),
                ('namePar',vtXmlFilterType.FILTER_TYPE_STRING),
                ('descPar',vtXmlFilterType.FILTER_TYPE_STRING),
                
                #('projects',vtXmlFilterType.FILTER_TYPE_STRING),
            ]
    def GetTranslation(self,name):
        return _(name)
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return False
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return True
    def IsSkip2Collect(self):
        "do not display node in tree but collect"
        return True
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00 \x00\x00\x00 \x08\x02\x00\
\x00\x00\xfc\x18\xed\xa3\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\
\x00\x04\x9aIDATH\x89\xb5VOH#W\x18\xff\x8d\x9b\xc3\xb8(\x8d\xad)\x13P0\xe0n\
\x99\xd0?daK'\x98\x05\xa7X0\xa5\x05#\x1e6baYh\x0f=\xb4\x94\xe0a\x8d{\xc8\x8e\
{\x10\xf7Ph\xf7P7BW\xb4\xa0$\x82`<\xc8N\x0e\x96XX\xa8\xa5\xc2\x8c`0\x85=d\
\xa0A\x87V\x98\x07\x0e}=\xbc$\x13\xf3O)\xf4wH\xde|\xdf\xef}\xdf{\xbf\xef\x9b\
7\x0f\x94R\xfco\xa0\x94\xba8\x8e\x0b\xab\x12\xbaAl\x02\x1b8'\x00,\x1b\x00`;T\
\xf3\x90\xf0\x057)\x11\xef\xa0\xbb\xe8+8\x0e\x9b\x94\x996.\xc0\x05=ar\x1c\
\x07\x00\xf2^`\xe5\xe0\x19m\x85s\xcb\xa2tfv\xe6E\xe6E\xf1Uqfv\xa6%\x93Rzn\
\xd1sK\x9cu\x8b\x8fy\xfe\xedJ\xb2\xf0K\xa9\xdd\x1cJ)\xa5\x96emmn)\x8f\x95K\
\x99\x0c\xd2\xbd\x00\x06\x01\xa0\x03\x00\x8f\xce\xcb\xe5\xe4y\x00\x9a\xae\
\x81\\M~\xc2\xb3\xff\x0e\x00\xc4\xb6\x00`#\x8d|\x01Y\xb5\x9eyF\x92\x8bIb\x02\
\x00!\x04.$\x17\x93\xfaa\x01\x8dh\x16\xa1\x03\xa8\xa9\x8f\xe0\x85i\xd6\xcdZ\
\xf9i%\xb3\xbe\x1a\xfb<\xe8\xf5\xf0\xc9\xa7\xc9\xe4\xe2\x93\xcc\xd2\x93\xcc\
\xfaj\xf3\x857Dp9\xc3\xb1\x88\xf3[\x03\xef\x80\xcf;(\xde\xffl\xb2\xf8\xe7)\\\
\x85\x8fG#\xfb\xfb\xba\xf8\x81\xd4$z\x8b\x08\x08\xabm\x8b|N\x8b\xaf\x8a\x94\
\xd2gK+\x91\xb1\xc8q\xe1\xb8jiW\xe4\xbb\x12+\xb2\xb3\x83\xf4z\xba\xe9\xa6#\
\x13\x11\xa1O\x00\xe0\xeb\x13zz{|\x03>\x00\xcc\xd2f\xcaE\x89lRgm\nb\xd77\xd0\
\xa5SP.2\x00 \xbda\x1a\x06\xf6\x7f\xbbt\x8a\x83\xb9Gs\\\x05s\x8f\xe6Z\xf2\
\xe4L\x80Rz|\xc4\xe4n)kn7\x07 \xb7\x9b\xabZ\x00(\te\xe5\xf9\n\x1b\xd71Y\r\
\x9c\x04\xed\xc1\xe6\xd0J\x8er\x88\x06T]JBa\x16\xa7\x06\xed\x11\x0c\x05s\x00\
BA\xba\x9b\xe3BAA\x10\xa2SQ\xff\r\x7f-G;\xd2\xc2\x13\x11\xd30\x94\x84\xe2\
\x1b\xf0QJ9\x8e\xbbR\x17\xb1\xa5\xb1\x1c\xc1P0:\x15\xf5\x8b~\xd8\x90\x87\xe5\
:\xb2_\xf4k\xba\x16\x7f\x18g\xd1\xab;p\x02\xb5\x824$\xb1\x1cl\xe1rHfF\x00\
\xb1\xe9\xd8\xc2\xfc\x82C\xb5\x11\x9d\x8ar\x1c\x87A \x7f\xb1\x8b\n\x7f@\xddi\
\xa7R9\xfa\xb0,\rI\xa9\xcdT\xd9~;\xc8\x06\xa9\xcdT`H\x92\x87e\xff\r\x7ft*\
\x8a<P\xfb\xa2\xb9\xbb\xdc\x9d<:[\x1c\xac{?\xef\t\x82\xe0\x17\xfdrHf\x0b_\
\x98_H\xaf\xa7#\x13\x11\xfdP\x07\xb0\xd7\xdd\xa9\xfcm\x01\x08\x0cI,\xb0\xbbW\
0KF\x07*\xdf/y\x04\x82\x00i\xa8y\x025\xab\xca#2\x1336\x1dc\xc6\xa6\xaa\xc6\
\x99\xd7Fx\xb4R!iM\xbc\xb4M\x01(\t\xa5\xfa\x12\xa4\xd6RU\x97\x92Pr]\xbcE\xa9\
Eij-e\x9d\xd3_\x8b\xa7\xb9\xdd\x1c\xebT\xa7\xc8W9X\xda\x19m\x00\x087\x1c\xa5\
N\r\xaer\xb0\x00\x88M\xc7\x82\xb7\x83\xb5dy^\t\x9cZ\xd5\xc7\xccF:\xf727\xfe\
\xe9xM\x02\x1b\x002\xdb\xb8\x15@n\x97D&\xf8\xda\x88jvO\xdd\xd9\xaa>V;\xb2\
\xf6\xe5\xac\x1d\xcbc\x11_H&y\x9d=:mJ\x88\xc9\xbb\xcc@\xe0Bt\x00\x85\xbc\xb6\
\xb7\xe3|\x05\xf7\r\x93\xd4\xeai\xa3v\xf9\xeaF\xbd\xce\xd7\x00\xf4\x7f\xe2\
\xb9\x1f\xf8R\x14y\xfe:\xdf\xf3z\x1d\x01\xc6\x89\xd9\xf3\x86\xf7N(\xa8\x1dig\
\x7f\x9d\xf5\xbf\xd6\xfd\xfdw\xdf~\xf1\xf5We\xf7?U\t\x00\xc0'\x8a\xca\x83\
\xd8M\x8fG\xcd\xaa\xda\x91v\xf0\xfbAG\xdd\x06\x1b\x11\x1e\x91'\xef\x8e\xcb\
\xc3rfGe\x8aN~\x13g\xaeLM_T\xd7^\xf6\xba\x90\xd9VQ\xb9\xb6\\\x02\xa1O\x90\
\x86$\xd304]S\xb3*\xc9\xeb\xbaa\xc6\xa7c\xacg2\x1bi\x00\xf2XDy\x10+\x94L\x92\
\xd7\xd5\xac\xaa\xe9\x9aY2*\x12\x8d{:\xaf\xf1\xef\xbcy\xabM\x8e\xf8\xc3\xf8\
\xbb\xef\xbd\xbf\xb4\xf8\x83\xd0'0\xa1\xfco\xf9M\xbe\xdbs\x9d\xffq9y\xe7\xc3\
\x8f\n%\xf3\xa6\xc7c\x9f\x94\x988\xab\xcb\xab\x18\x04N\xc0\x01\x08<w\x17\x8e\
\x08~\xe1a5\x8fN\x08\x11\xfb\xc5\x1e\xe2uw\xf1\xe9\xf5t\xab\xd3T\xcd\xaapA\
\xd3\xb5\xd5\xe5U%\xa1\xc4\x9f\xc6a\x80+\x9f\xab\x02@\x1a.\xb0u8\x03\x13\x17\
6\xdc\xbdBxTn\xfc\x1ed\xb6U\xb3d\xa0\xab\xcc\xa7\x94r\xf4\xbf^\xdf\xcb7\xe7\
\x8bh\x8c\xf6/B\xf0U\xf3\xac\x01tt\x00\x00\x00\x00IEND\xaeB`\x82" 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        return None
        if GUI:
            return vNodeXXXEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        return None
        if GUI:
            return vNodeXXXAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeEngineeringActivitiesPanel
        else:
            return None
_(u'datetime'),_(u'nr'),_(u'start'),_(u'end'),_(u'prior'),_(u'priorIdx'),
_(u'severity'),_(u'severityNr'),_(u'name'),_(u'desc')
_(u'highest'),_(u'higher'),_(u'normal'),_(u'lower'),_(u'lowest')
_(u'blocker'),_(u'critical'),_(u'major')
_(u'normal'),_(u'minor'),_(u'trivial'),_(u'enhancement')

