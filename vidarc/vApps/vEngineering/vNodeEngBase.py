#----------------------------------------------------------------------------
# Name:         vNodeEngBase.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20101220
# CVS-ID:       $Id: vNodeEngBase.py,v 1.21 2015/04/27 04:25:53 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import fnmatch
import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.ext.report.veRepDocBase import veRepDocBase
from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeEngBase(veRepDocBase,vXmlNodeEngineering):
    def __init__(self,tagName='Base'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
        veRepDocBase.__init__(self,self.__class__.__name__)
    def GetDescription(self):
        return _(u'engineering base')
    def GetTransDescription(self):
        return u''
    # ---------------------------------------------------------
    # specific
    def IsSkipProcessing(self,node):
        if self.Get(node,'skipProc') in ['true']:
            return True
        return False
    def IsHideOverView(self,node):
        if self.Get(node,'hideOverView') in ['true']:
            return True
        return False
    def GetOrder(self,node):
        return self.Get(node,'order')
    def GetOrderVal(self,node):
        return self.GetVal(node,'order',long,fallback=-1)
    def IsOrderProcessing(self,node):
        if self.Get(node,'orderProc') in ['true']:
            return True
        return False
    def IsNameProcessing(self,node):
        if self.Get(node,'nameProc') in ['true']:
            return True
        return False
    def IsGrpProcessing(self,node):
        if self.Get(node,'grpProc') in ['true']:
            return True
        return False
    def IsGrpProcessingChild(self,node):
        if self.Get(node,'grpChild') in ['true']:
            return True
        return False
    def GetVisibility(self,node):
        return self.Get(node,'visibility')
    
    def IsNewPageStart(self,node):
        if self.Get(node,'newPageStart') in ['true']:
            return True
        return False
    def IsNewPage(self,node):
        if self.Get(node,'newPage') in ['true']:
            return True
        return False
    
    def GetLevelAdd(self,node):
        return self.Get(node,'levelAdd')
    def GetLevelAddVal(self,node):
        return self.GetVal(node,'levelAdd',long,fallback=0)
    
    def SetOrder(self,node,val):
        return self.Set(node,'order',val)
    def SetVisibility(self,node,val):
        return self.Set(node,'visibility',val)
    
    def BuildLang(self):
        vXmlNodeEngineering.BuildLang(self)
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vXmlNodeEngineering.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'visibility':[
                    _(u'70 visibility'),u'visibility',-70,
                    {'val':u'public','type':'choice','it00':'public','it01':'protected','it02':'private','it03':'friend','it04':'package'},
                    {'val':u'public','type':'choice','it00':'public','it01':'protected','it02':'private','it03':'friend','it04':'package'}],
            u'baseRef':[
                    _(u'71 reference'),u'baseRef',-71,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'baseCom':[
                    _(u'72 communication'),u'baseCom',-72,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'skipProc':[
                    _(u'90 skip processing'),u'skipProc',-90,
                    {'val':u'true','type':'choice','it00':'true','it01':'false'},
                    {'val':u'true','type':'choice','it00':'true','it01':'false'}],
            u'orderProc':[
                    _(u'91 order processing'),u'orderProc',-91,
                    {'val':u'true','type':'choice','it00':'true','it01':'false'},
                    {'val':u'true','type':'choice','it00':'true','it01':'false'}],
            u'nameProc':[
                    _(u'92 name processing'),u'nameProc',-92,
                    {'val':u'true','type':'choice','it00':'true','it01':'false'},
                    {'val':u'true','type':'choice','it00':'true','it01':'false'}],
            u'hideOverView':[
                    _(u'93 hide in overview'),u'hideOverView',-93,
                    {'val':u'true','type':'choice','it00':'true','it01':'false'},
                    {'val':u'true','type':'choice','it00':'true','it01':'false'}],
            u'grpChild':[
                    _(u'94 grouping children'),u'grpChild',-94,
                    {'val':u'true','type':'choice','it00':'true','it01':'false'},
                    {'val':u'true','type':'choice','it00':'true','it01':'false'}],
            u'grpProc':[
                    _(u'95 group processing'),u'grpProc',-95,
                    {'val':u'true','type':'choice','it00':'true','it01':'false'},
                    {'val':u'true','type':'choice','it00':'true','it01':'false'}],
            u'newPageStart':[
                    _(u'96 new page at start'),u'newPageStart',-96,
                    {'val':u'true','type':'choice','it00':'true','it01':'false'},
                    {'val':u'true','type':'choice','it00':'true','it01':'false'}],
            u'newPage':[
                    _(u'97 new page'),u'newPage',-97,
                    {'val':u'true','type':'choice','it00':'true','it01':'false'},
                    {'val':u'true','type':'choice','it00':'true','it01':'false'}],
            u'levelAdd':[
                    _(u'98 level additional'),u'levelAdd',-98,
                    {'val':u'','type':'long'},{'val':u'','type':'long'}],
            u'order':[
                    _(u'99 order'),u'order',-99,
                    {'val':u'','type':'long'},{'val':u'','type':'long'}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    def GetGrpDict(self):
        return {
            None:0,
            self.GetTagName():[-1,self.GetDescription(),self.GetOrderVal]
            }
    def GetProcessNodeInc(self):
        return [
            {'lTag':['Base','Fct','Prc','drawNode','itfc','api',
                    'account','account_task','account_subtask','Bug',
                    'DocRef','parameters','Prop','Test','Tool','ToDo',],
                    'level':1,'level_max':8,},
            ]
    
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02'IDAT8\x8d\x8d\x93\xbdNTA\x1c\xc5\x7f3s\xe7\xde\xbd\xdf\xb0@\xc0%B\
\x83\x85\x89\xedZ \xc6G\xf0\x19,i|\x01k\x1b_\x80J\x13*c\x8c1\xb6>\x82\x14&\
\xa2\xd1\x82\x04\x90\xb0\x92\x05v\xd7e\xbf\xef\xc7X\\X\xbe\x13O3\x99\xf3\x9f\
9s\xf2\xff\xcf\x11B*n\xc3j\x15sq\xbf\xb6\x81\xb8zF\xdc&\xb0Z\xc5\x94+\x01/?u\
\x04\xc0\x8b\xa7\x81i\xd4:\x97\xce\xacm \x84\x90\xea\xdaK\x00\xe5J\x80\x06\
\x92\xa4(\xb9\x9e!\x0c\x0c\x81;\xc4r\x14\x8b\x0b\x8awo\x07X\xabU\xcc\xf2r\
\x95f\xab\xcfp\xf0\x83\x92\xbf\xc0Dl\xe1z{LOI\x82 \xc5+\t<O\x92\xe7\x0e\xa0\
\xc92An,`\x80\x05\xd0Kl\xe2\xa8G\xea=\xe0\xd1\xb3\xe7\xf8G\xaf\xe86<\xb2L\
\xd3\xeb\xdbt{\xd0h\xe4\xa4\xa9&I\x8a6hm\x80\xe3B@\xe5]P`\xdc\n\xe9\xde\x07\
\xbe}\xaf\x03e\x942d\xd9y\xdf\xd2T`\x8c&I@\xeb\x82\xb3\x00\xb2L\x10\xc7\x9a\
\x13\xb1\xcf\xd6\xd7\x84\xe1p\x0e)\x0b\xfeL M\xe5x5F\xe18\xfa\\\xe0\x0c\xedZ\
\x1d\x19UHS\x18\x8d\x0cR\x18\x84T\x0cG\t\x00\x8e\x1f\xa3\xdc\x80l\xd0\xe1\
\xa0\xde`m\x03qI\xe0\xf7\xce\t\xf6\x92!\x0cm\x1c\xa7\xe0\x86\xaa\x8cwZ?n\xb6\
9\xde\xfe\xc9\xfea\x9bH\xb4\xae;\xa8\xb5`\xfb\xcb\x16\xf7\xeeN\x8d\xb9?G[<y\
\xfc\x90hv\x8e\xf7\x1f?\x13\x86>\xae\xab!\xbf\xd0\x03\xa5\x8aY\x97BC\xfb0\
\xe5\xd7N\x1d\xa5\x14^I\xd3\x1b$\xe4\xa2\xc9D$YZ\x9c%\xf2\x0bk\xadz\xf3\xba\
\x83)\xc7\xc6\xafx\x94\x9c\x82\xb6\xad\xe2\x97\x1e\xec\x1e\xf1\xb7v\xc2\x9d\
\xe9\x08G+|\xd7\xa6U\xdf\xbd*\xd0'.O3yJ9Z1\x92%&\x9d\x1c\xa5$J\x19\xe2\xb8\
\x8f\xe7\x81m\xb7\xd8\xdc\xbc8F\xe9\x93e\x03&|\x0f\xcf\xb5p\xdd!\xb6\x9d\x13\
\x86\x03\x82(\xc1\x92\x1d\xd6\xd7\x0f.\x9a\x1d\x07k\x9c\x85\x95\x95\xfb\xcc\
\xcct\x99\x9f\xef\xd3\xeb\xdb\xbcy\xbd\x7f\xe3\x85\xab\x18\xa7\xf1\x7f\xa2{\
\x13\xfe\x01\xe4x\xcc\xfav\tu\xf1\x00\x00\x00\x00IEND\xaeB`\x82" 
    def ProcessFmtChilds(self,doc,node,bGrp,lNode,lang,strs):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurCls(vtLog.DEBUG,'tag:%s;id:%8d;sTag:%s'%(\
                            doc.getTagName(node),
                            doc.getKeyNum(node),
                            doc.getNodeText(node,'tag')),self)
            sTagNameOld=None
            iOfs=0
            strs.append('  \\addtocounter{table}{-1}')
            strs.append('\\begin{longtable}{p{0.05\linewidth}p{0.2\linewidth}p{0.65\linewidth}}')
            strs.append('  \\endfirsthead')
            strs.append('  \\endhead')
            #strs.append('  \\multicolumn{2}{l}{\\textbf{%s}} \\\\'%(self.__tex__(self.GetTrans('method'))))
            for sTag,n in lNode:
                oReg=self.doc.GetRegByNode(n)
                if hasattr(oReg,'IsSkipProcessing'):
                    if oReg.IsSkipProcessing(n):
                        continue
                elif self.IsSkipProcessing(n):
                    continue
                if hasattr(oReg,'IsHideOverView'):
                    if oReg.IsHideOverView(n):
                        continue
                sDesc=oReg.GetDesc(n,lang=lang)
                if len(sDesc)==0:
                    continue
                if hasattr(oReg,'IsNameProcessing'):
                    if oReg.IsNameProcessing(n):
                        sName=oReg.GetName(n,lang=lang)
                    else:
                        sName=oReg.GetTag(n)
                else:
                    sName=oReg.GetTag(n)
                iOfs+=1
                if bGrp:
                    sTagName=oReg.GetTagName()
                    if sTagName!=sTagNameOld:
                        strs.append('  \\multicolumn{2}{l}{\\textbf{%s}} & \\textbf{%s} \\\\'%(self.__tex__(sTagName),
                                    self.__tex__(oReg.GetDescription())))
                        sTagNameOld=sTagName
                strs.append(u''.join([u'  \\hfill ',str(iOfs),u' & ',
                        self.__tex__(sName),u' & ',
                        sDesc,u' \\\\']))
            if iOfs>0:
                strs.append('\\end{longtable}')
                strs.append('')
            else:
                for iOfs in xrange(4):
                    strs.pop()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def Process(self,f,*args,**kwargs):
        try:
            bDbg=False
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                bDbg=True
                vtLog.vtLngCurCls(vtLog.DEBUG,
                            'args:%s;kwargs:%s'%(vtLog.pformat(args),
                            vtLog.pformat(kwargs)),self)
            node=kwargs['node']
            if self.IsSkipProcessing(node):
                return
            if 'lang' not in kwargs:
                kwargs['lang']=self.lang
            else:
                self.__setLang__(kwargs['lang'])
            if bDbg:
                vtLog.vtLngCurCls(vtLog.DEBUG,
                            'args:%s,kwargs:%s'%(vtLog.pformat(args),
                            vtLog.pformat(kwargs)),self)
            lang=kwargs['lang']
            self.__setLang__(lang)
            self.__getTranslation__()
            iLevel=kwargs['level']
            strs=[]
            if 'sMatchTag' in kwargs:       # 20150425 wro:
                sMatchTag=kwargs['sMatchTag']
            else:
                sMatchTag=None
            if self.IsNameProcessing(node):
                sTag=self.GetName(node,lang=lang)
                s=sTag
            else:
                sTag=self.GetTag(node)
                sTmp=self.GetTransDescription()
                if len(sTmp)>0:
                    s=': '.join([sTmp,sTag])
                else:
                    s=sTag
            iLevelAddTmp=self.GetLevelAddVal(node)
            bGrpChild=False
            if sMatchTag is None:       # 20150425 wro:tag matching
                if (iLevelAddTmp==-9) or self.IsGrpProcessingChild(node):
                    bGrpChild=True
                if (iLevelAddTmp>0):
                    strs.append(self.__getHeadLine__(iLevel+iLevelAddTmp,self.__tex__(s)))
                else:
                    strs.append(self.__getHeadLine__(iLevel,self.__tex__(s)))
                strs.append(self.__getLabel__(**kwargs))
                strs.append('')
                #strs.append(self.__tex__(self.GetDesc(node,lang=lang)))
                strs.append(self.GetDesc(node,lang=lang))
                lTagFull=self.GetValidChild()
                if bDbg:
                    self.__logDebug__(['lTagFull',lTagFull])
                lNode=[]
                self.GetNodeGrpOrder(node,self.doc,lTagFull,lNode,sLang=lang)
                bGrp=False
                if self.IsGrpProcessing(node):
                    bGrp=True
                if len(lNode)>0:
                    self.ProcessFmtChilds(self.doc,node,bGrp,lNode,lang,strs)
                if bDbg:
                    self.__logDebug__(strs)
                bWrite=True
                if len(strs)<4:
                    if len(strs[1].strip())<1:
                        bWrite=False
                if bGrpChild==True:        # skip processing
                    bWrite=False
                if bDbg:
                    self.__logDebug__('bWrite:%d'%(bWrite))
                if bWrite==True:
                    self.__writeLst__(f,['%% ++++++++++++++++++++++++++++++++++++++++','%%  %s'%(sTag),'%% ++++++++++++++++++++++++++++++++++++++++',''])
                    if self.IsNewPageStart(node):
                        self.__writeLst__(f,['\\newpage',''])
                    self.__writeLst__(f,strs)
                #if len(strs)>2:
                #    self.__writeLst__(f,strs)
                #else:
                #    if len(strs[1].strip())>1:
                #        self.__writeLst__(f,strs)
            else:
                if bDbg:
                    vtLog.vtLngCurCls(vtLog.DEBUG,
                            'sMatchTag:%s,sTag:%s'%(sMatchTag,sTag),self)
                if fnmatch.fnmatchcase(sTag,sMatchTag)==True:
                    strs.append(self.GetDesc(node,lang=lang))
                lTagFull=self.GetValidChild()
                lNode=[]
                self.GetNodeGrpOrder(node,self.doc,lTagFull,lNode,sLang=lang)
                bGrp=False
                bWrite=True
            strs=None
            self.__includeDoc__(f,*args,**kwargs)
            if bWrite==False:
                if bGrpChild==True:        # skip processing
                    iLevel=iLevel-1
                else:
                    if bDbg:
                        vtLog.vtLngCurCls(vtLog.DEBUG,'nothing written and no grouping selected, so exit',self)
                    return
            lNodeInc=self.GetProcessNodeInc()
            if bDbg:
                self.__logDebug__(lNode)
                self.__logDebug__(lNodeInc)
                vtLog.vtLngCurCls(vtLog.DEBUG,'args:%s,kwargs:%s'%(vtLog.pformat(args),vtLog.pformat(kwargs)),self)
            for it in lNodeInc:
                lTag=it.get('lTag',[])
                if len(lTag)<=0:
                    continue
                iLevelAdd=it.get('level',1)
                iLevelMax=it.get('level_max',None)
                if iLevelMax is not None:
                    if (iLevel+iLevelAdd)>=iLevelMax:
                        continue
                kkwargs=kwargs.copy()
                sH=it.get('headline',None)
                if sH is not None:
                    strs=['']
                    strs.append(self.__getHeadLine__(kwargs['level']+1,self.GetTrans(sH)))
                    strs.append('')
                    self.__writeLst__(f,strs)
                    strs=None
                kkwargs.update({'level':iLevel+iLevelAdd})
                #lTag=[k for k in lTagFull if k not in ['Documents','Doc']]
                if bGrp:
                    self.__includeNodesGrp__(lTag,f,*args,**kkwargs)
                else:
                    self.__includeNodes__(lTag,f,*args,**kkwargs)
            if self.IsNewPage(node):
                self.__writeLst__(f,['\\newpage',''])
            if bWrite==True:
                self.__writeLst__(f,['%% ----------------------------------------','%%  %s'%(sTag),'%% ----------------------------------------',''])
        except:
            vtLog.vtLngTB(self.__class__.__name__)
