#Boa:FramePanel:vEngineeringMainPanel
#----------------------------------------------------------------------------
# Name:         vEngineeringPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060810
# CVS-ID:       $Id: vEngineeringMainPanel.py,v 1.16 2013/06/17 11:51:05 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.xml.vtXmlNodeTagCmdPanel
import time

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.vApps.common.vSystem as vSystem

try:
    from vGuiFrmWX import vGuiFrmWX
    from vidarc.tool.net.vtNetSecXmlGuiMaster import *
    
    from vidarc.vApps.vEngineering.vNetEngineering import *
    from vidarc.vApps.vEngineering.vXmlEngineeringTree import *
    from vidarc.vApps.vEngineering.vEngineeringListBook import *
    from vidarc.vApps.vEngineering.vEngineeringActivitiesDlg import vEngineeringActivitiesDlg
    from vidarc.vApps.vEngineering.vXmlNodeEngineeringActivityPanel import vXmlNodeEngineeringActivityPanel
    from vidarc.vApps.vEngineering.vXmlNodeEngineeringActSelPanel import vXmlNodeEngineeringActSelPanel
    from vidarc.vApps.vDoc.vNetDoc import *
    from vidarc.vApps.vPrjDoc.vNetPrjDoc import *
    from vidarc.vApps.vPrj.vNetPrj import *
    from vidarc.vApps.vHum.vNetHum import *
    from vidarc.vApps.vGlobals.vNetGlobals import *
    from vidarc.tool.net.vtNetMsg import vtNetMsg
    
    from vidarc.tool.xml.vtXmlDom import vtXmlDom
    from vidarc.tool.xml.vtXmlFindQuickThread import vtXmlFindQuickThread
    from vidarc.tool.xml.vtXmlFindQuickThread import EVT_VTXML_FIND_QUICK_FOUND
    
    import vidarc.vApps.vEngineering.images as imgEngineering
except:
    vtLog.vtLngTB('import')
    
def create(parent):
    return vEngineeringMainPanel(parent)

[wxID_VENGINEERINGMAINPANEL, wxID_VENGINEERINGMAINPANELPNDATA, 
 wxID_VENGINEERINGMAINPANELSLWNAV, wxID_VENGINEERINGMAINPANELSLWTOP, 
 wxID_VENGINEERINGMAINPANELVITAG, 
] = [wx.NewId() for _init_ctrls in range(5)]

def getPluginImage():
    return imgEngineering.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(imgEngineering.getPluginBitmap())
    return icon

class vEngineeringMainPanel(wx.Panel):
    VERBOSE=0
    def _init_sizers(self):
        # generated method, don't edit
        self.bxsData = wx.BoxSizer(orient=wx.VERTICAL)

        self.pnData.SetSizer(self.bxsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VENGINEERINGMAINPANEL,
              name=u'vEngineeringMainPanel', parent=prnt, pos=wx.Point(325, 29),
              size=wx.Size(650, 400), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(642, 373))
        self.Bind(wx.EVT_SIZE, self.OnSize)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VENGINEERINGMAINPANELSLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 0), size=wx.Size(200,
              376), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetAutoLayout(True)
        self.slwNav.SetLabel(u'Navigation')
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetToolTipString(u'navigation')
        self.slwNav.SetDefaultSize(wx.Size(200, 100))
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VENGINEERINGMAINPANELSLWNAV)

        self.slwTop = wx.SashLayoutWindow(id=wxID_VENGINEERINGMAINPANELSLWTOP,
              name=u'slwTop', parent=self, pos=wx.Point(200, 0),
              size=wx.Size(440, 100), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwTop.SetDefaultSize(wx.Size(1000, 200))
        self.slwTop.SetAlignment(wx.LAYOUT_TOP)
        self.slwTop.SetAutoLayout(True)
        self.slwTop.SetSashVisible(wx.SASH_BOTTOM, True)
        self.slwTop.SetOrientation(wx.LAYOUT_HORIZONTAL)
        self.slwTop.SetLabel(_(u'information'))
        self.slwTop.SetToolTipString(_(u'information'))
        self.slwTop.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwTopSashDragged,
              id=wxID_VENGINEERINGMAINPANELSLWTOP)

        self.pnData = wx.Panel(id=wxID_VENGINEERINGMAINPANELPNDATA,
              name=u'pnData', parent=self, pos=wx.Point(208, 108),
              size=wx.Size(424, 252),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)

        self.viTag = vidarc.tool.xml.vtXmlNodeTagCmdPanel.vtXmlNodeTagCmdPanel(applName='vtXmlNodeTagCmdPanel',
              id=wxID_VENGINEERINGMAINPANELVITAG, name=u'viTag',
              parent=self.slwTop, pos=wx.Point(0, 0), size=wx.Size(440, 97),
              style=0)
        self.viTag.Bind(vidarc.tool.xml.vtXmlNodeTagCmdPanel.vEVT_TOOL_XML_TAG_SELECTED,
              self.OnViTagToolXmlTagSelected,
              id=wxID_VENGINEERINGMAINPANELVITAG)
        self.viTag.Bind(vidarc.tool.xml.vtXmlNodeTagCmdPanel.vEVT_TOOL_XML_TAG_CANCEL,
              self.OnViTagToolXmlTagCancel, id=wxID_VENGINEERINGMAINPANELVITAG)
        self.viTag.Bind(vidarc.tool.xml.vtXmlNodeTagCmdPanel.vEVT_TOOL_XML_TAG_APPLY,
              self.OnViTagToolXmlTagApply, id=wxID_VENGINEERINGMAINPANELVITAG)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        self._init_ctrls(parent)
        self.SetName(name)
        self.bActivated=False
        self.dCfg={}
        self.dlgActivities=None
        self.frmActEdit=None
        
        self.xdCfg=vtXmlDom(appl='vEngineeringCfg',audit_trail=False)
        
        appls=['vEngineering','vPrjDoc','vDoc','vPrj','vHum','vGlobals','vMsg']
        
        rect = parent.sbStatus.GetFieldRect(0)
        self.netMaster=vtNetSecXmlGuiMaster(parent.sbStatus,'Master',pos=(rect.x+2, rect.y+2),
                    size=(rect.width-4, rect.height-4),verbose=0)
        self.netEngineering=self.netMaster.AddNetControl(vNetEngineering,'vEngineering',synch=True,verbose=0,
                                                audit_trail=True)
        self.netPrjDoc=self.netMaster.AddNetControl(vNetPrjDoc,'vPrjDoc',synch=True,verbose=0)
        self.netDoc=self.netMaster.AddNetControl(vNetDoc,'vDoc',synch=True,verbose=0)
        self.netPrj=self.netMaster.AddNetControl(vNetPrj,'vPrj',synch=True,verbose=0)
        self.netHum=self.netMaster.AddNetControl(vNetHum,'vHum',synch=True,verbose=0)
        self.netGlb=self.netMaster.AddNetControl(vNetGlobals,'vGlobals',synch=True,verbose=0)
        self.netMsg=self.netMaster.AddNetControl(vtNetMsg,'vMsg',synch=True,verbose=0)
        self.netMaster.SetLang(lang=vtLgBase.getPluginLang())
        self.netMaster.AddNetControl(None,None,verbose=0)
        
        self.netMaster.SetCfg(self.xdCfg,[],appls,'vEngineeringMaster')
        
        self.vgpTree=vXmlEngineeringTree(self.slwNav,wx.NewId(),
                pos=(0,0),size=(198,529),style=0,name="trEng",
                master=True,verbose=0)
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        #EVT_VTXMLTREE_ITEM_ADDED(self.vgpTree,self.OnTreeItemAdd)
        self.vgpTree.SetDoc(self.netEngineering,True)
        self.vgpTree.EnableMoveMenu(True)
        self.vgpTree.EnableClipMenu(True)
        self.vgpTree.SetView([
            ( 'activities', _(u'open activities') , 
                imgEngineering.getActivitiesBitmap() , (self.ShowActivities ,)) ,
            ( 'activitiesEdit', _(u'edit activities') , 
                imgEngineering.getActivitiesBitmap() , (self.EditActivities ,)) ,
            #( 'refresh_diagram', _(u'refresh diagram') , vtArt.getBitmap(vtArt.Synch) , (self.RefreshDiagram ,)) ,
            ])
        self.vgpTree.SetAnalyse([
            ( 'activities', _(u'open activities') , 
                imgEngineering.getActivitiesBitmap() , (self.ShowActivities ,)) ,
            #( 'refresh_diagram', _(u'refresh diagram') , vtArt.getBitmap(vtArt.Synch) , (self.RefreshDiagram ,)) ,
            ])
        
        oDoc=self.netEngineering.GetReg('Doc')
        oDoc.SetTreeClass(vXmlEngineeringTree)
        
        self.nbData=vEngineeringListBook(self.pnData,wx.NewId(),
                pos=(4,4),size=(470, 188),style=0,name="nbEngineering")
        self.bxsData.AddWindow(self.nbData, 1, border=4, flag=wx.EXPAND | wx.ALL)
        
        oInst=self.netEngineering.GetRegisteredNode('instance')
        self.nbData.SetDoc(self.netEngineering,True)
        
        self.viTag.AddWidgetDependent(self.nbData,None)
        #self.viTag.AddWidgetDependent(self.nbData,oDoc)
        self.viTag.SetDoc(self.netEngineering,True)
        self.viTag.SetRegNode(oDoc)
        
        pn=self.viTag.CreateRegisteredPanel('Doc',parent=self.pnData,
                        name='pnDoc')
        pn.Show(False)
        self.bxsData.AddWindow(pn, 1, border=4, flag=wx.EXPAND | wx.ALL)
        
        pn=self.viTag.CreateRegisteredPanel('drawNode',parent=self.pnData,
                        name='pnDrawNode')
        if pn is not None:
            pn.Show(False)
            self.bxsData.AddWindow(pn, 1, border=4, flag=wx.EXPAND | wx.ALL)
            pn.SetDocExt(self.netEngineering,True)
        
        pn=self.viTag.CreateRegisteredPanel('drawTiming',parent=self.pnData,
                        name='pnDrawTiming')
        if pn is not None:
            pn.Show(False)
            self.bxsData.AddWindow(pn, 1, border=4, flag=wx.EXPAND | wx.ALL)
            #pn.SetDocExt(self.netEngineering,True)
        
        pn=self.viTag.CreateRegisteredPanel('MetricPy',parent=self.pnData,
                        name='pnMetricPy')
        if pn is not None:
            pn.Show(False)
            self.bxsData.AddWindow(pn, 1, border=4, flag=wx.EXPAND | wx.ALL)
        
        pn=self.viTag.CreateRegisteredPanel('dataCollector',parent=self.pnData,
                        name='pnCollector')
        if pn is not None:
            pn.Show(False)
            self.bxsData.AddWindow(pn, 1, border=4, flag=wx.EXPAND | wx.ALL)
        
        self.thdFindQuick=vtXmlFindQuickThread(self.vgpTree,bPost=True,
                lFindTagName=['tag','name','description'],origin='vEngineering:thdFindQuick')
        EVT_VTXML_FIND_QUICK_FOUND(self.vgpTree,self.OnFindQuickFound)
        self.idQuickFoundLast=-1
        
        self.Move(pos)
        self.SetSize(size)
    def GetDocMain(self):
        return self.netEngineering
    def GetNetMaster(self):
        return self.netMaster
    def GetTreeMain(self):
        return self.vgpTree
    def PopulateToolBar(self,parent):
        id=wx.NewId()
        sz=self.GetClientSize()
        self.tbMain = wx.ToolBar(id=id,
                name=u'tbContact', parent=parent, pos=wx.Point(0,0),
                size=wx.DefaultSize, style=wx.TB_HORIZONTAL | wx.NO_BORDER)
        self.tbMain.SetToolBitmapSize(wx.Size(20,20))
        self.dToolBar={}
        self.vgpTree.PopulateToolBar(parent,self.tbMain,self.dToolBar)
        #self.tbMain.AddSimpleTool(id, vtArt.getBitmap(vtArt.Duplicate), _(u"Duplicate"), 
        #                _(u"duplicate"))
        #self.Bind(wx.EVT_TOOL, self.OnToolDupRecipeClick, id=id)
        self.tbMain.AddSeparator()
        id=wx.NewId()
        self.dToolBar['quickFind']=id
        txtCtrl=wx.TextCtrl(self.tbMain, id, "", size=(150, -1))
        self.tbMain.AddControl(txtCtrl)
        self.tbMain.Bind(wx.EVT_TEXT, self.OnQuickFind, id=id)
        self.tbMain.Bind(wx.EVT_TEXT_ENTER, self.OnQuickFindAgain, id=id)
        self.txtQuickFind=txtCtrl
        
        parent.SetToolBar(self.tbMain)
        self.tbMain.Realize()
        return self.tbMain
    def OnQuickFind(self,event):
        sVal=self.txtQuickFind.GetValue()
        self.idQuickFoundLast=-1
    def OnQuickFindAgain(self,event):
        sVal=self.txtQuickFind.GetValue()
        id=self.vgpTree.GetMoveTargetID()
        self.thdFindQuick.Do(id,self.idQuickFoundLast,sVal,100)
    def OnFindQuickFound(self,evt):
        evt.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.INFO,'id:%08d'%(evt.GetID()),self)
            self.idQuickFoundLast=evt.GetID()
            self.vgpTree.SelectByID('%08d'%self.idQuickFoundLast)
        except:
            vtLog.vtLngTB(self.GetName())
            vtLog.vtLngCurWX(vtLog.ERROR,'id:%08d'%(evt.GetID()),self)
    def OnTreeItemSel(self,event):
        event.Skip()
        node=event.GetTreeNodeData()
        self.viTag.SetNode(node)
        if self.frmActEdit is not None:
            self.frmActEdit.SetNode(node)
    def OpenFile(self,fn):
        if self.netEngineering.ClearAutoFN()>0:
            self.netEngineering.Open(fn)
    def OpenCfgFile(self,fn=None):
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                self.netMaster.SetCfgNode()
                self.__setCfg__()
                return
        self.xdCfg.New(root='config')
        if fn is not None:
            self.xdCfg.SetFN(fn)
            self.netMaster.SetCfgNode()
        self.__setCfg__()
    def __setCfg__(self):
        try:
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSlwNavSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iWidth=event.GetDragRect().width
            #if iWidth>80:
            #    iWidth=80
            self.dCfg['nav_sash']=str(iWidth)
            
            self.slwNav.SetDefaultSize((iWidth, 1000))
            
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            self.GetCfgData()
            #event.Skip()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSlwTopSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iHeight=event.GetDragRect().height
            #if iHeight>80:
            #    iHeight=80
            self.dCfg['nav_top']=str(iHeight)
            
            self.slwTop.SetDefaultSize((1000, iHeight))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            self.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnSize(self, event):
        event.Skip()
        try:
            bMod=False
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            
            self.pnData.Refresh()
            #self.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())

    def SetCfgData(self,func,l,d):
        self.cfgFunc=func
        self.cfgOrigin=l
        self.dCfg=d
        try:
            i=int(self.dCfg['nav_sash'])
            self.slwNav.SetDefaultSize((i, 1000))
        except:
            pass
        try:
            i=int(self.dCfg['nav_top'])
            self.slwTop.SetDefaultSize((1000, i))
        except:
            pass
        try:
            pos=map(int,self.dCfg['pos'].split(','))
            self.Move(pos)
        except:
            pass
        try:
            sz=map(int,self.dCfg['size'].split(','))
            self.SetSize(sz)
        except:
            pass
    def GetCfgData(self):
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,self.dCfg)
    def GetAboutData(self):
        import __config__
        version=u' %d.%d.%d.%d'%(__config__.VER_MAJOR,__config__.VER_MINOR,
                    __config__.VER_RELEASE,__config__.VER_SUBREL)
        desc=_(u"""Engineering module.

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
""")
        return _(u'VIDARC Engineering'),desc,version
    def __showActivitiesCmd__(self,evt):
        evt.Skip()
        try:
            sCmd=evt.GetCmd()
            data=evt.GetData()
            self.vgpTree.SelectByID(data)
        except:
            vtLog.vtLngTB(self.GetName())
    def ShowActivities(self,*args,**kwargs):
        #self.thdDrawing.Do(self.__showDiagram__)
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
            if self.dlgActivities is None:
                self.dlgActivities=vEngineeringActivitiesDlg(self)
                self.dlgActivities.BindEvent('cmd',self.__showActivitiesCmd__)
            iRet=self.dlgActivities.DoShow(self.netEngineering,
                    self.vgpTree.GetSelectedIDNum())
        except:
            vtLog.vtLngTB(self.GetName())
    def OnActEditCmd(self,evt):
        try:
            #bDbg=self.GetVerboseDbg(0)
            bDbg=True
            #if bDbg:
            #    self.__logDebug__('')
            sCmd=evt.GetCmd()
            dDat=evt.GetData()
            #self.__logDebugAdd__('sCmd:%s'%(sCmd),dDat)
            if sCmd=='cut':
                #self.__logInfoAdd__({'cmd':sCmd},dDat)
                self.vgpTree.DoNodeCut(dDat.get('iId',-1))
            elif sCmd=='copy':
                #self.__logInfoAdd__({'cmd':sCmd},dDat)
                self.vgpTree.DoNodeCopy(dDat.get('iId',-1))
            elif sCmd=='paste':
                #self.__logInfoAdd__({'cmd':sCmd},dDat)
                self.vgpTree.DoNodePaste(dDat.get('iId',-1))
            elif sCmd=='move':
                moveId=self.vgpTree.GetMoveTargetID()
                if moveId>=0:
                    selId=dDat.get('iId',-1)
                    if selId>=0:
                        self.frmActEdit.pn.DoMove(moveId,selId)
            else:
                #self.__logError__({'cmd not recognized':sCmd})
                pass
        except:
            #self.__logTB__()
            vtLog.vtLngTB(self.GetName())
    def EditActivities(self,*args,**kwargs):
        try:
            #bDbg=self.GetVerboseDbg(0)
            #if bDbg:
            #    self.__logDebug__('')
            if self.frmActEdit is None:
                oAct=self.netEngineering.GetReg('Activity')
                if oAct is not None:
                    img=oAct.GetBitmap()
                    sTitle=oAct.GetDescription()
                else:
                    img=None
                    sTitle=_(u'Activities')
                p=vGuiFrmWX(name='frmActEdit',parent=self,kind='frmCls',
                    iArrange=10,widArrange=self,bAnchor=True,
                    bmp=img,title=sTitle,
                    lGrowRow=[(0,1),],
                    lGrowCol=[(0,1),(1,1)],
                    #kwTopCmd={'name':'pnTopCmd','bFlexGridSizer':-1,'lWid':lWidTop},
                    lWid=vXmlNodeEngineeringActSelPanel)
                #p.BindEvent('ok',self.OnActEditOk,
                #    )
                p.pn.BindEvent('cmd',self.OnActEditCmd)
                p.SetDoc(self.netEngineering,True)
                self.frmActEdit=p
            if self.frmActEdit is not None:
                self.frmActEdit.Show(True)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnViTagToolXmlTagSelected(self, event):
        event.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            #lWid=event.GetListWidget()
            #for wid in lWid:
            #    wid.Layout()
            self.bxsData.Layout()
            #self.bxsData.FitInside(self.pnData)
            #wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnViTagToolXmlTagCancel(self, event):
        event.Skip()

    def OnViTagToolXmlTagApply(self, event):
        event.Skip()
