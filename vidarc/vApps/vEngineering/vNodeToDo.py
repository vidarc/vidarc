#----------------------------------------------------------------------------
# Name:         vNodeToDo.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20130520
# CVS-ID:       $Id: vNodeToDo.py,v 1.2 2014/06/15 19:54:36 wal Exp $
# Copyright:    (c) 2013 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeToDo(vNodeEngBase):
    def __init__(self,tagName='ToDo'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'to do')
    # ---------------------------------------------------------
    # specific
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'serveriy':[
                    _(u'41 serverity'),u'serverity',-41,
                    {'val':u'+0','type':u'choice','it00':'+2','it01':'+1','it02':'+0','it03':'-1','it04':'-2'},
                    {'val':u'+0','type':u'choice','it00':'+2','it01':'+1','it02':'+0','it03':'-1','it04':'-2'}],
            u'priority':[
                    _(u'42 priority'),u'priority',-42,
                    {'val':u'+0','type':u'choice','it00':'+2','it01':'+1','it02':'+0','it03':'-1','it04':'-2'},
                    {'val':u'+0','type':u'choice','it00':'+2','it01':'+1','it02':'+0','it03':'-1','it04':'-2'}],
            u'system':[
                    _(u'43 system'),u'system',-43,
                    {'val':u'','type':u'text'},{'val':u'','type':u'text'}],
            u'origin':[
                    _(u'44 origin'),u'origin',-44,
                    {'val':u'','type':u'text'},{'val':u'','type':u'text'}],
            u'zFound':[
                    _(u'45 time found'),u'zFound',-45,
                    {'val':u'','type':u'datetime',},
                    {'val':u'','type':u'datetime',}],
            u'zReport':[
                    _(u'46 time reported'),u'zReport',-46,
                    {'val':u'','type':u'datetime',},
                    {'val':u'','type':u'datetime',}],
            u'zDeadLine':[
                    _(u'48 deadline'),u'zDeadLine',-48,
                    {'val':u'','type':u'datetime',},
                    {'val':u'','type':u'datetime',}],
            u'zFix':[
                    _(u'49 time fixed'),u'zFix',-49,
                    {'val':u'','type':u'datetime',},
                    {'val':u'','type':u'datetime',}],
            u'pType':[
                    _(u'21 type'),u'pType',-21,
                    {'val':u'','type':u'text'},{'val':u'','type':u'text'}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x03!IDAT8\x8d}\x92\x7fh\x94u\x1c\xc7_\xcf\xf7\xb9\xbb\xe7\xee\xd6\xc2\
\x93\x1cn\xca\xa6\xfd\xd8-\xa7k\xbf\xca\xb2\xec\x8f"B\xa5 +G\x10\r\x12$\x08\
\x8b$(\x98\xa1A\xa8d?\x88\r\x97\x83\xfe\x88AIN\x9c\x1a\xa3\x82\x0c\xb6\xa9S\
\x1b\x9bZ\xfeX\xdb\xdc\xddy\x13w\xdb\xddn\xf7\xdcv?\x9e\xe7\xf9\xf4\xc7\xa5\
\x11Eox\xff\xfb\xe6\xcd\xeb\xfdFS:\x96eIUU\x95\x00RQ\xbeD\x0c\x8f\x92\x92\
\xb2\xe7\xe5\x95m\'DS:\xffg\x05\xd0\xd6\xd6Fhb\x9c]\xbb\xb6s\xf4\xd8\xbb|\
\xd0R\xc5\xd3\x9bj\t\xd6\xdd\xcf\x0b;~\x92\x95\xf5\xedrv`B\xf8/iJ\xa7\xab\
\xabK\x1a\x1f\xae\x113\xdd)b5\xc9\xe4\xc4\xa3b\x9aWED\xa4s\xf0\xa6\x1c\xf8!$\
\x95\xb5\xad\xd2\xdd\xfd\xfb\xbf\x1a)\x80\x97\xb74i\xf5\xebw\x10\x99\xca\x92\
J^#m=GQQ\x10\xc4ax\xdcD\xcaJy\xff\xf3\xcd\xec\xdcs\x86\xd6\xd6\xfe\x7f4\xd15\
M\x912\xb3r9\xec\xe7\xec\x05\x1f\x9a\xf6\x18{?\x9e\x06<T?\xb8\x8c#CqN\x85m\
\x8c\x80\x9fuO\xd5\xf0uG/\x95\xcb\xe7w\x87\xc3\xe3\x1f\x02\xb8\x00f\xe2\xf3\
\xf8=\xf3\xb4}:D{\xc2$\x15\xefe2\x1cb\xe3\x865\xac.\xf3\x92\x18\x8d\x13\ty\
\x889If\x13\x87\x88F\xfa(.2$\x95\xcej.\x80\xf2e\xc5\xf8\x8a\x0c\x82\xd5\x8b\
\x99\x8adY\xf1\xe434\xbf\xb6\x16\xe5\xf5\xd0\xfcx)/=\xb4\x08\xc30\xe88\xd8\
\x8eS\xe3\xe1\xad\xafv\xd2\xdf\x7f\x92={\x07\xe5\x0e\x8c\xca\xf5\x1d\xf2\xfd\
\xe9[\x12I\xe7\xe5z2\'\xb3"\x12\xcb\x89\x98\x8e\x88-\x05E#C\x922\x0f\x8a\xc8\
\xdb22Z\'O<\x1b\x14u\x1bFt\xe48\xd3sI\x94\xcf\x85\xb8\xddL\xcfA&\x0f\xe9\x1c\
$-0\x05\x96.\xaf\xe5.o1\xb7\xe6\xae\x10\xfbe\x8cw\xd6m(@ll\xa8\x97\x1b\x13\
\xe7\xb8z\xa9\x97\xd8L\x9aUuu(\xdd\x85m\x17\xc2\xe5/Zv4\x8cv\xea\x0f\\V9\xb9\
\x03!\x96\x1e\xfb\xb1\x100;k\xee\xde\xfef\x13\xdb\x9a\x970\xf6[\x0f9c\r++W\
\x90\xc9\xdc\xde\nD9\xf8\xfb\xbbq\x7f\xb9\x0f\xf9\xee4\xf6\xd4M\x12\x98\x85\
\x15\xdex\xbd\x81\xfd\x9fl\x06\x0eSQ\xe1\xe7Z\xeenf\x92\xe0\x96\xbf\x1b8J\
\xb1P\xfd\x08\xe6\x03\x8d8\xc3\xdf\xe2\xf7\t\xb6[\xa1iJ\x07`2\xf6\xaa\xb8\
\xe7.2\x96}\x91L\xa0\x85\xfc\x02x]`\xb8\xc0\xa3\x83K\x87\xc5\x01\xb8\x12]\
\xa0e\xd3~\x0e\xdfw\x92\x92\x8f\xbe(<q_\xcfV9~A\x98R[\xf8\xec\xc4%\x86G\x86\
\xf0\xfa /`\tX\x80\xed\x80\x93\x87\x80r\xb8\xe8\x0bRr\xe8g\x8c\xd5\r\x85\x1f\
\xb8\xf4\x0c\x03\x91\x11\xbe\xf9\xb5\x8f3\xe7\xae\x13\x9a4)\xdd\xda\x89\xe1^\
DJrxt\r\xb7\x02\xcb\xd1I\xc4\xb3\x18\xb6\xc9\xc0\xe5\x04w\x9ex\xef=5\x1c\x19\
<\xca\xe8t\x8a\xe2\x00\x9c\xef\xbb\xc1{\xe7{0\xbc\x06"Y4MC\xd3\xc0\xa5+l\xcb\
\xc6g\xd9\xac]\x15\x00\xe0OXBu\xf8\xa7\xd4v\xda\x00\x00\x00\x00IEND\xaeB`\
\x82' 
