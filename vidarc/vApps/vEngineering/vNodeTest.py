#----------------------------------------------------------------------------
# Name:         vNodeTest.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120530
# CVS-ID:       $Id: vNodeTest.py,v 1.2 2014/06/15 19:54:36 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeTest(vNodeEngBase):
    def __init__(self,tagName='Test'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'test')
    # ---------------------------------------------------------
    # specific
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'serveriy':[
                    _(u'31 serverity'),u'serverity',-31,
                    {'val':u'+0','type':u'choice','it00':'+2','it01':'+1','it02':'+0','it03':'-1','it04':'-2'},
                    {'val':u'+0','type':u'choice','it00':'+2','it01':'+1','it02':'+0','it03':'-1','it04':'-2'}],
            u'priority':[
                    _(u'32 priority'),u'priority',-32,
                    {'val':u'+0','type':u'choice','it00':'+2','it01':'+1','it02':'+0','it03':'-1','it04':'-2'},
                    {'val':u'+0','type':u'choice','it00':'+2','it01':'+1','it02':'+0','it03':'-1','it04':'-2'}],
            u'system':[
                    _(u'33 system'),u'system',-33,
                    {'val':u'','type':u'text'},{'val':u'','type':u'text'}],
            u'origin':[
                    _(u'34 origin'),u'origin',-34,
                    {'val':u'','type':u'text'},{'val':u'','type':u'text'}],
            u'zCreate':[
                    _(u'35 time created'),u'zCreate',-35,
                    {'val':u'','type':u'datetime',},
                    {'val':u'','type':u'datetime',}],
            u'zApproved':[
                    _(u'36 time approved'),u'zApproved',-36,
                    {'val':u'','type':u'datetime',},
                    {'val':u'','type':u'datetime',}],
            u'result':[
                    _(u'37 result'),u'result',-37,
                    {'val':u'','type':u'text',},
                    {'val':u'','type':u'text',}],
            u'zExec':[
                    _(u'39 time executed'),u'zExec',-39,
                    {'val':u'','type':u'datetime',},
                    {'val':u'','type':u'datetime',}],
            u'pType':[
                    _(u'21 type'),u'pType',-21,
                    {'val':u'','type':u'text'},{'val':u'','type':u'text'}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xfeIDAT8\x8d\x9d\x92?J\x03A\x14\x87\xbf\xdf\xect*(\x8b\xa5w\xb0\xb3\
\x15,D\xf0\x08\xb66\x8af\x0b\x0f\x90V\xb0H\x08\xe6$\x1e\xc0\x03Xx\x87tQ\x90T\
\x81H|\x163\xfb/qa\xdc\xd7\xcc\xccc\xde\xf7\xfb\x86]\xc9e4\xeb\xe0\xf4\xd6H\
\xa8\xaf\xd7g\x01\xf8\xcd\xe1\xb3\x87G\xe6\x1fK\xbeWk\xe6\x9fK\xd6?\xc6b\xb1\
\xaa/\xbdMZ \x95\x06\xa9\xc9\x97wc^&\x83\xbf\rN\xae\xc7d\x1e\xf2\x1c\xbc\x87\
\xfc\x10|f\xec\xec\x82\x80\xa7\xabb\x0b\xe87\x1b\x02,n\x84U\xfb.=\xd7~O\xb9\
\x1aN\xe1\xac\x08\xed"\xb8\xad\x8e\xac\xe1\xa1\x00\xb5\x08I1\x10\xc2\xb90!\
\x17\xf5+\x8d\x04\x03\xa9\x91\x8c\xd5\xfaIO\x10HBX\\\xcb^}\xa5\xf9\tC\xe0?\
\xff\x83\xe6p\x0b\xd0\xb7\xfc\xfd\xf9^Rr\'\x00`tsT5\x8a\xe9,\xf9\\Lghp\xb1o\
\xa3\xe1q\xaf\xf4b\xf8\x1e\x00\xbd\xa6c\xfd\x02xaNhVnEg\x00\x00\x00\x00IEND\
\xaeB`\x82' 
