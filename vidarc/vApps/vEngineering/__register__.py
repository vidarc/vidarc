#----------------------------------------------------------------------------
# Name:         __register__.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: __register__.py,v 1.19 2015/03/01 08:46:02 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust

from vidarc.vApps.vEngineering.vNodeDocuments import vNodeDocuments
from vidarc.vApps.vEngineering.vNodeDoc import vNodeDoc
from vidarc.vApps.vEngineering.vNodeDocRef import vNodeDocRef
from vidarc.vApps.vEngineering.vXmlNodeEngineeringActivity import vXmlNodeEngineeringActivity
from vidarc.vApps.vEngineering.vEngineeringDrawNode import vEngineeringDrawNode
from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
from vidarc.vApps.vEngineering.vNodeProp import vNodeProp
from vidarc.vApps.vEngineering.vNodeParameters import vNodeParameters
from vidarc.vApps.vEngineering.vNodeItfc import vNodeItfc
from vidarc.vApps.vEngineering.vNodeObj import vNodeObj
from vidarc.vApps.vEngineering.vNodePrd import vNodePrd
from vidarc.vApps.vEngineering.vNodeSec import vNodeSec
from vidarc.vApps.vEngineering.vNodeDsg import vNodeDsg
from vidarc.vApps.vEngineering.vNodeRes import vNodeRes
from vidarc.vApps.vEngineering.vNodeStruct import vNodeStruct
from vidarc.vApps.vEngineering.vNodeCps import vNodeCps
from vidarc.vApps.vEngineering.vNodeExp import vNodeExp
from vidarc.vApps.vEngineering.vNodeBug import vNodeBug
from vidarc.vApps.vEngineering.vNodeTest import vNodeTest
from vidarc.vApps.vEngineering.vNodeFct import vNodeFct
from vidarc.vApps.vEngineering.vNodePrc import vNodePrc
from vidarc.vApps.vEngineering.vNodeToDo import vNodeToDo
from vidarc.vApps.vEngineering.vNodeTool import vNodeTool
from vidarc.tool.xml.vtXmlNodeAttrCfgML import vtXmlNodeAttrCfgML

from vidarc.tool.draw.vtDrawTimingNode import vtDrawTimingNode

import vidarc.vApps.vEngineering.__link__
import vidarc.vApps.vEngineering.admin.__register__
import vidarc.vApps.vEngineering.instance.__register__
import vidarc.vApps.vEngineering.research.__register__
import vidarc.vApps.vEngineering.type.__register__

import vidarc.ext.state.__register__
from vidarc.ext.state.veStateStateMachine import veStateStateMachine
from vidarc.ext.state.veStateTransition import veStateTransition
from vidarc.tool.xml.vtXmlFilterType import vtXmlFilterType

import vidarc.ext.data.__register__

if vcCust.is2Import(__name__,'__REG_ACCOUNT__',True):
    from vidarc.vApps.vEngineering.vNodeAccount import vNodeAccount
    from vidarc.vApps.vEngineering.vNodeAccountTask import vNodeAccountTask
    from vidarc.vApps.vEngineering.vNodeAccountTaskSub import vNodeAccountTaskSub
def __regAccount__(doc):
    oAcc=vNodeAccount()
    oAccTask=vNodeAccountTask()
    oAccTaskSub=vNodeAccountTaskSub()
    doc.RegisterNode(oAcc,False)
    doc.RegisterNode(oAccTask,False)
    doc.RegisterNode(oAccTaskSub,False)
    doc.LinkRegisteredNode(oAcc,oAccTask)
    doc.LinkRegisteredNode(oAccTask,oAccTaskSub)
    
def RegisterNodes(self,oRoot=None):
    try:
        oAttrML=vtXmlNodeAttrCfgML()
        oDocs=vNodeDocuments()
        oDoc=vNodeDoc()
        vidarc.ext.data.__register__.RegisterNodes(self,'vEngineering',bRegCollectorAsRoot=True,bRegDocAsRoot=True)
        oActivity=vXmlNodeEngineeringActivity()
        #oDoc=doc.GetReg('Doc')
        self.RegisterNode(oAttrML,False)
        self.RegisterNode(oDocs,False)
        self.RegisterNode(oDoc,False)
        self.RegisterNode(oActivity,False)
        self.LinkRegisteredNode(oDocs,oDoc)
        
        oBase=vNodeEngBase()
        self.RegisterNode(oBase,False)
        self.RegisterNode(vNodeCps(),False)
        self.RegisterNode(vNodeObj(),False)
        self.RegisterNode(vNodePrd(),False)
        self.RegisterNode(vNodeRes(),False)
        self.RegisterNode(vNodeSec(),False)
        self.RegisterNode(vNodeStruct(),False)
        self.RegisterNode(vNodeProp(),False)
        self.RegisterNode(vNodeParameters(),False)
        self.RegisterNode(vNodeFct(),False)
        self.RegisterNode(vNodePrc(),False)
        self.RegisterNode(vNodeDocRef(),False)
        self.RegisterNode(vNodeItfc(),False)
        self.RegisterNode(vNodeToDo(),False)
        self.RegisterNode(vNodeExp(),False)
        self.RegisterNode(vNodeTest(),False)
        self.RegisterNode(vNodeBug(),False)
        self.RegisterNode(vNodeTool(),False)
        self.RegisterNode(vNodeDsg(),False)
        
        oTiming=vtDrawTimingNode()
        self.RegisterNode(oTiming,False)
        oDrawNode=vEngineeringDrawNode()
        self.RegisterNode(oDrawNode,False)
        
        vidarc.vApps.vEngineering.__link__.LinkNodes(self,[
                    ('Documents',['dataCollector',]),
                    ])
        lDoc=['Documents','Doc','DocRef',]
        lLnk=['Base','Cps','Struct','Prop','drawTiming',
                'drawNode','dataCollector','itfc','parameters',
                'Prc','Fct','Bug','Test','ToDo','Tool',
                'Obj','Sec','Prd','Dsg','Res','Struct',
                ]
        vidarc.vApps.vEngineering.__link__.LinkNodes(self,[
                    ('Base',lLnk+lDoc+[
                        ]),
                    ('Obj',lLnk+lDoc+[
                        ]),
                    ('Sec',lLnk+lDoc+[
                        ]),
                    ('Cps',lLnk+lDoc+[
                        ]),
                    ('Dsg',lLnk+lDoc+[
                        ]),
                    ('Prd',lLnk+lDoc+[
                        ]),
                    ('Res',lLnk+lDoc+[
                        ]),
                    ('Struct',lLnk+lDoc+[
                        ]),
                    ('Fct',lLnk+lDoc+[
                        ]),
                    ('Prc',lLnk+lDoc+[
                        ]),
                    ('ToDo',lLnk+lDoc+[
                        ]),
                    ('Tool',lLnk+lDoc+[
                        ]),
                    ('parameters',lDoc+['drawTiming',
                        'drawNode','dataCollector','Prop',]),
                    ('itfc',lDoc+['drawTiming','drawNode',
                        'dataCollector','Exp','Bug','Test','ToDo','Tool',]),
                    ('Prop',lDoc+['drawTiming','drawNode',
                        'dataCollector',]),
                    ('Exp',lLnk+lDoc+[
                        ]),
                    ('Bug',lLnk+lDoc+[
                        ]),
                    ('Test',lLnk+lDoc+[
                        ]),
                    ])
        
        if vcCust.is2Import(__name__,'__REG_ACCOUNT__',True):
            __regAccount__(self)
        
        try:
            import vidarc.vApps.vEngineering.HW.__register__ as regHW
            regHW.RegisterNodes(self)
        except:
            pass
        try:
            import vidarc.vApps.vEngineering.SW.__register__ as regSW
            regSW.RegisterNodes(self)
        except:
            pass
        try:
            import vidarc.vApps.vEngineering.S88.__register__ as regS88
            regS88.RegisterNodes(self,oRoot=oRoot)
        except:
            pass
        
        vidarc.vApps.vEngineering.admin.__register__.RegisterNodes(self,oRoot=oRoot)
        vidarc.vApps.vEngineering.instance.__register__.RegisterNodes(self,oRoot=oRoot)
        vidarc.vApps.vEngineering.type.__register__.RegisterNodes(self,oRoot=oRoot)
        vidarc.vApps.vEngineering.research.__register__.RegisterNodes(self,oRoot=oRoot)
        
        try:
            import vidarc.vApps.vEngineering.SW.db.regDB as regDB
            regDB.RegisterNodes(self,oRoot=oRoot)
            #self.__logWarn__('SW.db extension not available'%())
        except:
            #self.__logWarn__('SW.db extension not available'%())
            vtLog.vtLngTB('import')
        try:
            import vidarc.vApps.vEngineering.SW.mii.regMII as regMII
            regMII.RegisterNodes(self,oRoot=oRoot)
        except:
            vtLog.vtLngTB('import')
        
        vidarc.ext.state.__register__.RegisterNodes(self,bRegStateAsRoot=True)
        
        flt={None:[('start',vtXmlFilterType.FILTER_TYPE_DATE_TIME),
                    ('end',vtXmlFilterType.FILTER_TYPE_DATE_TIME),
                    ]}
        oTransAct=self.GetReg('transActivity',bLogFlt=False)
        if oTransAct is None:
            oTransAct=veStateTransition(tagName='transActivity',filter=flt)
            self.RegisterNode(oTransAct,False)
        oSMactivity=self.GetReg('smActivity',bLogFlt=False)
        if oSMactivity is None:
            oSMactivity=veStateStateMachine(tagName='smActivity',
                    tagNameState='state',tagNameTrans='transActivity',
                    filter=flt,
                    trans={None:{None:_(u'activity'),
                            'GetSum':_(u'sum'),
                            'orderDt':_(u'ordering date'),
                            'deliveryDt':_(u'delivery date'),
                            } },
                    cmd=oActivity.GetActionCmdDict())
        oStateMachineLinker=self.GetReg('stateMachineLinker',bLogFlt=False)
        if oStateMachineLinker is not None:
            oStateMachineLinker.AddPossible({
                    'Activity':[('actState','smActivity')],
                    })
            self.RegisterNode(oSMactivity,False)
            self.LinkRegisteredNode(oStateMachineLinker,oSMactivity)
            oState=self.GetReg('state')
            self.LinkRegisteredNode(oSMactivity,oState)
        else:
            vtLog.vtLngCur(vtLog.ERROR,'statemachine linker not registered;you are not supposed to be here.',__name__)
    except:
        vtLog.vtLngTB(__name__)
