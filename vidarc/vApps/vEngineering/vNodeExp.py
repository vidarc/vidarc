#----------------------------------------------------------------------------
# Name:         vNodeException.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20130602
# CVS-ID:       $Id: vNodeExp.py,v 1.1 2013/06/02 20:34:04 wal Exp $
# Copyright:    (c) 2013 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeExp(vNodeEngBase):
    def __init__(self,tagName='Exp'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'exception')
    def GetTransDescription(self):
        return u'exception'
    # ---------------------------------------------------------
    # specific
    def GetProcessNodeInc(self):
        return [
            {'lTag':['Base','Fct','Prc','Prop','itfc','var','ToDo','Tool',],'level':1,'level_max':4,},
            ]
    # ---------------------------------------------------------
    # inheritance
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return False
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01<IDAT8\x8d\xa5\x93\xb1N\x02A\x10\x86\xbf\xbb\xa33tP\x80-\t\xb14\xb6r\
k\x7f\t\x05\t\xbe\x07\xd1Z\xe4\x01,\x8d\xc9\x85\xca\x82\xce\x18\x16j\xf6Nbb\
\xa2\xc6\x9a\x07\x10b-\xed9\x16\x07\xe8\x85;D\x9dd\xb2\x99I\xbe\xfc\xff\xce\
\xecZ\x96\xed\xf0\x970\xda\x13z\x1a\xfb?0e\xc8\xfd\x1a\x04\xd4\xeb\x84\xb0\
\x0cgO.X\xb6\xb3U\x06]D\x9e\xeb"\xe7Hp\x8a\xb8G\xaeX\xb6\xf3\xb3\x03\xe3G\
\xb1\xea~\x1d\x02M8\x8f\x95\xc7w\xf7\x16\xb0\xd9A\xd0E\xe4e\xa1|\x91T^e\xd6\
\x16\x8c\x1f\x89:X\xefw\xda\x8bs\xe8X\x901\xc4,\x98\x11\xb4\xd5\xb2\x88\xa43\
t\xac\xb55\x1a?\x12\xf5\x01<\x02;\xf5\xb89\x05\xfa@\x1e\xc2\x19\xa8\x81\xbbr\
\x90}\x05\xed\x89\xda\xd50Z4\xf2p|]\xe4-\xb7\xf75@H\x7fHF{\xa2\xa6:\xa9\xdas\
\x89J\xb5\x04\x9c\xea\xc0\\y\xa2\xe6\x1a\xf2q\x9d\xa6\xfa=rYpg\x00\xa3w\x97B\
\xa9\xc0\xf8\xa6\x9f\n\'\x1c\x98VETyB8\x83\xcb\x87"Q\xa9\xc6\xed\x060\xe1\
\xc0\xb4*\xa2\xaa\x93\x84\xea60\x00\xc1IU\xc4G\x9a\x87Ei4\x1b\xb2\xed\xdfX\
\xe6\'\x10Z\x93L\x15\xfc\xb9I\x00\x00\x00\x00IEND\xaeB`\x82' 
