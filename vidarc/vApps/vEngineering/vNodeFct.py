#----------------------------------------------------------------------------
# Name:         vNodeMod.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20130520
# CVS-ID:       $Id: vNodeFct.py,v 1.12 2013/10/27 05:38:25 wal Exp $
# Copyright:    (c) 2013 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeFct(vNodeEngBase):
    def __init__(self,tagName='Fct'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'function')
    def GetTransDescription(self):
        return u'function'
    # ---------------------------------------------------------
    # specific
    def GetProcessNodeInc(self):
        return [
            {'lTag':['Base','Fct','Prc','Prop','Exp','itfc','var','ToDo','Tool',
                    'parameters',],'level':1,'level_max':4,},
            #{'lTag':['Mod','array','class','struct'],'level':1,'headline':u'module'},
            ]
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'module':[
                    _(u'01 module'),u'module',-1,{'val':u'','type':u'text'},{'val':u'','type':u'text'}],
            u'fnSrc':[
                    _(u'02 file'),u'fnSrc',-2,{'val':u'','type':'text'},None],
            u'line':[
                    _(u'03 line'),u'line',-3,{'val':u'','type':u'long'},{'val':u'','type':u'text'}],
            u'argDef':[
                    _(u'11 argument definition (name|type|desc|comment)'),u'argDef',-11,{'val':u'','type':u'text'},{'val':u'','type':u'text'}],
            u'return':[
                    _(u'51 return (value|desc)'),u'return',-51,
                    {'val':u'','type':u'text'},{'val':u'','type':u'text'}],
            u'exception':[
                    _(u'80 exceptions (name|desc)'),u'exception',-80,
                    {'val':u'','type':u'text'},{'val':u'','type':u'text'}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01EIDAT8\x8d\xc5\x93AN\x1bA\x10E_\x8fA\xc8Afa\xc9c\xc8\x92\x04\t\x82\
\xc4\x8e\x9c\x83\x05\xc7@p\x05\xb69\x007`\x99\xbb\x84U\x12n\x80-\xb0#\xc03\
\xdd]\xd5U\xec\x90\x1c\xc7\x08\t\xa4\xd4\xb6\xf4\x9f\xf4\x7f\xfd\n\xa1\xea\
\xf0\x96\xa9\xde\xa4~\x0f\xc0\xca\xb2\xc5\xe1\xf9\xae\xc7\xfe\x1f\xa2\n\xab\
\xa3\x01?\xbf\xfd\x0e\xaf\x06\x1c]\xed\xf8\xe6Z\x9f&\xae\xf3\xd0\xb4L\xdbG\
\xc6\x87\x95\xdf\x1e\xdb\x02d\xc1\xc2\xd1\x8f\x1d?\xe8}\xa2\xca\x81\x92\x0cI\
\x05O\x81\xcf\x9d!\xbdK\xf3\x17\x01_\xcf\xf7\xfdcw\xc0\xdd\xec\x01E\xb1`P9\
\x16\n\xad\x08\x83\x95\x1e\xfb\xa7{s\x909\x0bM\x7fB\x13\xbbH,X0b\xce$U\xa4\
\x14\xd4\x157c\xb4>Z\x9eA\x12\xe1\xbei\x90dx0\x92*m\xce$\x11\xb2\x16rQ\xb0\
\x17B\\\x1b\xd7L\xdb\x19\x1e\xc1*CT\x89"49\x93\x92\xd0\xba\xb05\x1br\xcb\xe4\
Y\x13\xfenb\xfd\xbd\xf2\xed\xce\x90\xa8\x82z!\x89\x92\x92\x90T\xb8\xc9\x99p\
\xd2\x99\xbb\xc4\x02\x00\xa0wY\xbc^\xdd\xc0\x8a\x91\xb5\xd0j\xe6\xae)\x0b\
\xe2\xa5\x00\x80/g\xbb>\xfe0\xc6\xcd\xa9\x1fk~]\\\xff\xb3HK\x01\xaf\x9d\xff\
\xffLO=-\xb2L\xb1q\xa7\xf6\x00\x00\x00\x00IEND\xaeB`\x82' 
    def procDefLst(self,node,sT,iLenT,doc,l):
        sTag=doc.getTagName(node)
        if sTag.startswith(sT):
            if len(sTag)>iLenT:
                sNum=sTag[iLenT:]
            else:
                sNum='-1'
            try:
                iNum=int(sNum)
            except:
                iNum=sNum
            try:
                l.append((iNum,doc.getText(node).split('|')))
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        return 0
    def GetDefLst(self,node,sT):
        try:
            l=[]
            iLenT=len(sT)+1
            self.doc.procChildsNoKeys(node,self.procDefLst,sT,iLenT,self.doc,l)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
            vtLog.vtLngCurCls(vtLog.ERROR,'sT;%s;l:%s'%(sT,vtLog.pformat(l)),self)
        return l
    def Process(self,f,*args,**kwargs):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurCls(vtLog.DEBUG,'args:%s;kwargs:%s'%(vtLog.pformat(args),
                            vtLog.pformat(kwargs)),self)
            node=kwargs['node']
            if self.IsSkipProcessing(node):
                return
            if 'lang' not in kwargs:
                kwargs['lang']=self.lang
            else:
                self.__setLang__(kwargs['lang'])
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurCls(vtLog.DEBUG,'args:%s,kwargs:%s'%(vtLog.pformat(args),vtLog.pformat(kwargs)),self)
            lang=kwargs['lang']
            nodeBase=kwargs.get('__base_node',None)
            nodeRel=kwargs.get('__rel_node',None)
            self.__setLang__(lang)
            self.__getTranslation__()
            bWrite=True
            iLevel=kwargs['level']
            sTag=self.GetTag(node)
            self.__writeLst__(f,['%% ++++++++++++++++++++++++++++++++++++++++','%%  %s'%(sTag),'%% ++++++++++++++++++++++++++++++++++++++++',''])
            if self.IsNewPageStart(node):
                self.__writeLst__(f,['\\newpage',''])
            strs=[]
            s=': '.join([self.GetTrans(self.GetTransDescription()),sTag])
            iLevelAddTmp=self.GetLevelAddVal(node)
            if (iLevelAddTmp>0):
                strs.append(self.__getHeadLine__(iLevel+iLevelAddTmp,self.__tex__(s)))
            else:
                strs.append(self.__getHeadLine__(iLevel,self.__tex__(s)))
            #strs.append(self.__tex__(self.GetDesc(node,lang=lang)))
            strs.append(self.GetDesc(node,lang=lang))
            
            strs.append('\\addtocounter{table}{-1}')
            strs.append('\\begin{longtable}{|p{0.2\linewidth}|p{0.7\linewidth}|}')
            strs.append('  \\endfirsthead')
            strs.append('  \\endhead')
            strs.append(' \\hline \\multicolumn{2}{|l|}{\\textbf{%s}} \\\\ \\hline'%(self.__tex__(sTag)))
            sTmp=self.GetName(node,lang=lang)
            if len(sTmp.strip())>0:
                strs.append('  %s & %s \\\\ \\hline'%(self.__tex__(self.GetTrans('name')),
                        self.__tex__(sTmp)))
            sTmp=self.GetVisibility(node)
            if len(sTmp.strip())>0:
                strs.append('  %s: & %s \\\\ \\hline'%(self.__tex__(self.GetTrans('visibility')),
                        self.__tex__(sTmp)))
            for sT,sFmt,sTransName,sTransDesc in [
                        ('argDef','\\hfill',
                                self.__tex__(self.GetTrans('arguments')),
                                self.__tex__('/'.join([self.GetTrans('type'),
                                        self.GetTrans('description/comment')]))),
                        ('return','\\hfill',
                                self.__tex__(self.GetTrans('return')),
                                self.__tex__(self.GetTrans('description'))),
                        ('exception','\\hfill',
                                self.__tex__(self.GetTrans('exceptions')),
                                self.__tex__(self.GetTrans('description'))),
                        ]:
                lDef=self.GetDefLst(node,sT)
                iLen=len(lDef)
                if iLen>0:
                    lDef.sort()
                    strs.append('  %s &  %s \\\\ \\hline'%(sTransName,sTransDesc))
                    for iNum,lIt in lDef:
                        iLenIt=len(lIt)
                        if iLenIt>=1:
                            sN=lIt[0]
                        else:
                            sN=''
                        if iLenIt>=2:
                            sT=lIt[1]
                        else:
                            sT=''
                        strs.append('  \\hspace{0.3cm} %s %s & %s \\\\'%(sFmt,self.__tex__(sN),
                                self.__tex__(sT)))
                        for iOfs in xrange(2,iLenIt):
                            strs.append('  & %s \\\\'%(self.__tex__(lIt[iOfs])))
                        strs.append('\\hline')
            strs.append('\\end{longtable}')
            self.__writeLst__(f,strs)
            self.__includeDoc__(f,*args,**kwargs)
            self.__includeNodes__(['func','method',
                    'Prop','Param','t_free','itfc','Exp','api','var','const',],f,*args,**kwargs)
            if self.IsNewPage(node):
                self.__writeLst__(f,['\\newpage',''])
            if bWrite==True:
                self.__writeLst__(f,['%% ----------------------------------------','%%  %s'%(sTag),'%% ----------------------------------------',''])
        except:
            vtLog.vtLngTB(self.__class__.__name__)
if 0:
    _('name'),_('arguments'),_('description/comment'),_('type'),_('function')
