#----------------------------------------------------------------------------
# Name:         vNodePrc.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20130520
# CVS-ID:       $Id: vNodePrc.py,v 1.4 2013/06/03 05:26:34 wal Exp $
# Copyright:    (c) 2013 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeFct import vNodeFct
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodePrc(vNodeFct):
    def __init__(self,tagName='Prc'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeFct.__init__(self,tagName)
    def GetDescription(self):
        return _(u'process')
    def GetTransDescription(self):
        return u'process'
    # ---------------------------------------------------------
    # specific
    def GetProcessNodeInc(self):
        return [
            {'lTag':['Base','Fct','Prc','Prop','Exp','itfc','var','ToDo','Tool',
                    'parameters',],'level':1,'level_max':4,},
            #{'lTag':['Mod','array','class','struct'],'level':1,'headline':u'module'},
            ]
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\x1eIDAT8\x8d\xd5\xd3MK\x02Q\x14\xc6\xf1\xff\x9d\x19\xad\x8c\xc2\x97\
\xf0\xa5\x02)\xd0\x16A\x8b\n\x04\xe9\xbb\xb4i\xd7\xa2U\xf4\x19\xdaD\xfbp\xd5\
\xe7\x89\x90 \x08M\xda\x88\xa6\x14\xa43\xbe\xcd\xcc=\xad\\LbF\xb6\xe9\xec\
\xee\xe2\xf9\xf1\xdc\x03G)\xc3d\x9e1\xe6J\xff9ptR\x94\xe2YA~\xdf`qH\xb3X%~\
\x1e\xfd1\x12\x00\x0cQ\x14\xb2[tb\x1fD\xae}\xc9^dfB\x01`\xe4y$ca\xb6\x13)\
\xfa]\x03{\xb3N\xfc\xca\x97\xdd\xd3\xfcT(\xf8\x05\xadp\x06]\x0e\xf2)2\xc90+\
\xe1\x10k\xab\x16\xeea\x85\xf5KS\xf6\x8f\xf7& +\xf0\x12p]\x17\xfc\x10\xbcY8#\
\x8f\xf7\x81f\xa8}\xf05\x89\x907\xd1 \x00h\xad1\r\x8b\xbbr\x8bF\xa5\x87,\t\
\xa2\x85\x9c\x93\xa3z[S\x0f<~\x0fX\xca\xe0\xb5\xe5\xf0\xfc\xd2F\xa2\x9a\xb4\
\x9d\xa6y\xd3VUj\xd3V\xf0\xa5\x01\xc2\xfdS\x83\xe5^\x84N\xc9VM\xdaS\x83\xe3\
\t,Q\xf5\x17\xd8(\xef\xd0)\xd9jfr\x9c\xf9\xff\xc7\xf4\t\x14\x0ee\x99\xbb\xe2\
\xbb\xde\x00\x00\x00\x00IEND\xaeB`\x82' 
