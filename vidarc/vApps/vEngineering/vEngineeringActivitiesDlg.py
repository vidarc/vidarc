#----------------------------------------------------------------------------
# Name:         vEngineeringActivitiesDlg.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20110110
# CVS-ID:       $Id: vEngineeringActivitiesDlg.py,v 1.3 2011/01/11 00:58:43 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import time
    
    from vidarc.tool.gui.vtgDlg import vtgDlg
    from vidarc.tool.gui.vtgPanel import vtgPanel
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    from vidarc.tool.misc.vtmListCtrl import vtmListCtrl
    
    import vidarc.tool.InOut.vtInOutFileInfo as vtioFI
    import vidarc.tool.InOut.vtInOutFileTools as vtioFT
    
    import vidarc.vApps.vEngineering.images as imgEngineering
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vEngineeringActivitiesDlg(vtgDlg):
    def __init__(self, parent,name=None,
                pos=None,sz=None,iArrange=-1,widArrange=None,
                bmp=None,title=None):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        lWid=[
            ('lblCt',       (0,1),(1,2),{'name':'lblNodes','label':_(u'nodes')},None),
            (vtmListCtrl,   (1,1),(4,2),{'name':'lstNodes','cols':[
                        ['hierarchy',      -1,  0.5,    1],
                        ['tag',            -1,  0.2,    1],
                        ['act',            -2,   60,    1],
                        ['name',           -1,  0.3,    1],
                        ['id',             -2,   60,    1],
                        ],
                        'map':[('hier',0),('tag',1),('cnt',2),('name',3),('id',4)],
                        #'value':[],
                    },None),
            ('cbBmp',       (1,3),(1,1),{'name':'cbRefresh','bitmap':vtArt.getBitmap(vtArt.Refresh),},
                {'btn':self.OnRefresh}),
            ('cbBmp',       (2,3),(1,1),{'name':'cbNav','bitmap':vtArt.getBitmap(vtArt.Navigate),},
                {'btn':self.OnNav}),
            ]
        vtgDlg.__init__(self,parent,vtgPanel,name=name or 'dlgEngActivities',
                pos=pos,sz=sz,iArrange=iArrange,widArrange=widArrange,
                kind='frmSngCls',
                bmp=bmp or imgEngineering.getActivitiesBitmap(),
                title=title or _(u'VIDARC Engineering Activities'),
                pnPos=None,pnSz=None,pnStyle=None,
                pnName='pnExplorerCreateNew',
                lGrowCol=[(1,1)],lGrowRow=[(3,1)],
                bEnMark=False,bEnMod=False,lWid=lWid)
        try:
            pn=self.GetPanel()
            self.imgLst,self.dImgLst=pn.lstNodes.CreateImageList([
                ['_invis',  vtArt.getBitmap(vtArt.Invisible)],
                ['_sortAsc',vtArt.getBitmap(vtArt.UpSmall)],
                ['_sortDsc',vtArt.getBitmap(vtArt.DownSmall)],
                ])
            pn.lstNodes.SetImageList(self.imgLst,0,1,2)
            pn.lstNodes.SetSortDef({
                    0:{'iSort':1},
                    1:{'iSort':1},
                    2:{'iSort':1},
                    3:{'iSort':1},
                    4:{'iSort':1},
                    })
            pn.lstNodes.SetColumns([
                        ['hierarchy',      -1,  0.5,    1],
                        ['tag',            -1,  0.2,    1],
                        ['act',            -2,   60,    1],
                        ['name',           -1,  0.3,    1],
                        ['id',             -2,   60,    1],
                        ])
            pn.lstNodes.SetSort(0,1,1)
        except:
            self.__logTB__()
    def DoShow(self,doc,nodeID=-1):
        try:
            pn=self.GetPanel()
            self.doc=doc
            self.nodeID=nodeID
            self.Refresh(None,None)
            #iRet=self.ShowModal()
            self.Show(True)
            #if iRet>0:
            #    pass
            #self.doc=None
            #self.node=None
            #return iRet
        except:
            self.__logTB__()
    def __procNodes__(self,node,doc,nodeBase,oReg,lVal=[]):
        try:
            lC=doc.getChilds(node,'Activity')
            iCnt=len(lC)
            if iCnt>0:
                iAct=0
                nodeID=doc.getKeyNum(node)
                for c in lC:
                    if oReg.IsActive(c):
                        iAct+=1
                d={
                    -1:         nodeID,
                    'hier':     doc.GetTagNames(node,nodeBase,nodeBase),
                    'tag':      doc.getNodeText(node,'tag'),
                    'name':     doc.getNodeTextLang(node,'name',None),
                    'act':      '%d'%(iAct),
                    'cnt':      '%d'%(iCnt),
                    'id':       '%d'%nodeID,
                    }
                lVal.append(d)
        except:
            self.__logTB__()
        return 0
    def Refresh(self,doc=None,nodeID=None):
        try:
            self.__logDebug__(''%())
            pn=self.GetPanel()
            #pn.lstNodes.Clear()
            if doc is None:
                doc=self.doc
            if nodeID is None:
                nodeID=self.nodeID
            if nodeID is None:
                node=doc.getBaseNode()
            else:
                node=doc.getNodeByIdNum(nodeID)
            lVal=[]
            oReg=doc.GetReg('Activity')
            if doc.IsNodeKeyValid(node):
                doc.procKeysRec(10,node,self.__procNodes__,doc,node,oReg,lVal=lVal)
            else:
                doc.procChildsKeysRec(10,node,self.__procNodes__,doc,node,oReg,lVal=lVal)
            pn.lstNodes.SetValue(lVal)
            pn.lstNodes.Sort()
            #for t in l:
            #    sNew=t[1]
            #    pn.lstPreView.Append(sNew)
        except:
            self.__logTB__()
    def OnRefresh(self,evt):
        try:
            self.__logDebug__(''%())
            self.Refresh()
        except:
            self.__logTB__()
    def OnNav(self,evt):
        try:
            self.__logDebug__(''%())
            pn=self.GetPanel()
            lData=pn.lstNodes.GetSelected([4])
            self.__logDebug__('lData;%s'%(self.__logFmt__(lData)))
            self.Post('nav',lData[0][0])
        except:
            self.__logTB__()
