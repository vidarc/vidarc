#----------------------------------------------------------------------------
# Name:         vXmlNodeEngineering.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: vXmlNodeEngineering.py,v 1.7 2014/06/15 19:54:17 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeTag import *
from vidarc.tool.xml.vtXmlNodeAttrML import *
try:
    if vcCust.is2Import(__name__):
        from vidarc.vApps.vEngineering.vEngineeringAttrPanel import vEngineeringAttrPanel
        #from vtXmlNodeTagPanel import *
        from vidarc.tool.xml.vtXmlNodeTagEditDialog import *
        from vidarc.tool.xml.vtXmlNodeTagAddDialog import *
        #from vidarc.vApps.vEngineering.vXmlNodeEngineeringEditDialog import *
        #from vidarc.vApps.vEngineering.vXmlNodeEngineeringAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeEngineering(vtXmlNodeTag,vtXmlNodeAttrML):
    def __init__(self,tagName='engineering',cfgBase=['cfg']):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vtXmlNodeTag.__init__(self,tagName)
        vtXmlNodeAttrML.__init__(self,tagName,cfgBase=cfgBase)
    def GetDescription(self):
        return _(u'engineering')
    # ---------------------------------------------------------
    # specific
    def GetNodeValues(self):
        lTag=vtXmlNodeTag.GetNodeValues(self)
        lAttr=vtXmlNodeAttrML.GetNodeValues(self)
        return lTag + lAttr
    def GetAttributeValFullLang(self,node,tagName,lang=None):
        #print 'GetAttributeValFullLang',tagName
        self.__logDebug__('tagName:%s'%(tagName))
        #if self.doc.getForeignKeyNumAppl(node
        
        return self.doc.GetNodeAttributeValFullLang(node,tagName,lang)
    def GetTagHier(self,node,nodeBase=None,nodeRel=None):
        if nodeBase is None:
            nodeBase=self.doc.getBaseNode()
        if nodeRel is None:
            nodeRel=self.doc.getBaseNode()
        nodePar=self.doc.getParent(node)
        return self.doc.GetTagNames(nodePar,nodeBase=nodeBase,nodeRel=nodeRel)
    def GetTagPar(self,node,iCnt=1):
        nodePar=node
        for i in xrange(iCnt):
            nodePar=self.doc.getParent(nodePar)
        if nodePar is None:
            return '???'
        try:
            o=self.doc.GetRegByNode(nodePar)
            return o.GetTag(nodePar)
        except:
            return '???'
        return self.Get(nodePar,'tag')
    def GetNamePar(self,node,lang=None,iCnt=1):
        nodePar=node
        for i in xrange(iCnt):
            nodePar=self.doc.getParent(nodePar)
        if nodePar is None:
            return '???'
        try:
            o=self.doc.GetRegByNode(nodePar)
            return o.GetName(nodePar,lang=lang)
        except:
            return '???'
        return self.GetML(nodePar,'name',lang)
    def GetDescPar(self,node,lang=None,iCnt=1):
        nodePar=node
        for i in xrange(iCnt):
            nodePar=self.doc.getParent(nodePar)
        if nodePar is None:
            return '???'
        try:
            o=self.doc.GetRegByNode(nodePar)
            return o.GetDesc(nodePar,lang=lang)
        except:
            return '???'
        return self.GetML(nodePar,'description',lang)
    
    def AddValueToDictByDict(self,node,dDef,dVal,bMultiple,*args,**kwargs):
        try:
            if 'lang' in kwargs:
                lang=kwargs['lang']
            else:
                lang=None
        except:
            lang=None
        #print kwargs
        #print kwargs.keys()
        #if 'nr' in dDef:
        #    dVal['nr']=self.GetNr(node)
        #if 'prev' in dDef:
        #    dVal['prev']=self.GetPrev(node)
        #if 'author' in dDef:
        #    dVal['author']=self.GetAuthor(node)
        #if 'authorLogin' in dDef:
        #    dVal['authorLogin']=self.GetAuthorLogin(node)
        if 'tag (hier)' in dDef:
            dVal['tag (hier)']=self.GetTagHier(node)
        if 'tag (par)' in dDef:
            dVal['tag (par)']=self.GetTagPar(node)
        if 'name (par)' in dDef:
            dVal['name (par)']=self.GetNamePar(node,lang)
        if 'desc (par)' in dDef:
            dVal['desc (par)']=self.GetDescPar(node,lang)
        if 'tag (par.par)' in dDef:
            dVal['tag (par.par)']=self.GetTagPar(node,iCnt=2)
        if 'name (par.par)' in dDef:
            dVal['name (par.par)']=self.GetNamePar(node,lang,iCnt=2)
        if 'desc (par.par)' in dDef:
            dVal['desc (par.par)']=self.GetDescPar(node,lang,iCnt=2)
        self.doc.__AddValueToDictByDict__(node,dDef,dVal,bMultiple,*args,**kwargs)
        return 0
    # ---------------------------------------------------------
    # inheritance
    def BuildLang(self):
        vtXmlNodeAttrML.BuildLang(self)
    def IsAttrFile(self,sTag):
        iRet=sTag.find('file')
        if iRet==0:
            if len(sTag)>4:
                if sTag[4]!='_':
                    return True
            return False
    def IsAttrLink(self,sTag):
        iRet=sTag.find('__link_')
        if iRet==0:
            if len(sTag)>7:
                if sTag[7]!='_':
                    return True
            return False
    def GetAttrLinkName(self,sTag):
        if self.IsAttrLink(sTag):
            return sTag[7:]
        else:
            return None
    def IsAttrHidden(self,sTag):
        if self.IsAttrFile(sTag):
            return True
        return vtXmlNodeTag.IsAttrHidden(self,sTag)
    def GetAttrFilterTypes(self):
        lTag=vtXmlNodeTag.GetAttrFilterTypes(self)
        lPar=[
                ('tag (hier)',vtXmlFilterType.FILTER_TYPE_STRING),
                ('tag (par)',vtXmlFilterType.FILTER_TYPE_STRING),
                ('name (par)',vtXmlFilterType.FILTER_TYPE_STRING),
                ('desc (par)',vtXmlFilterType.FILTER_TYPE_STRING),
                ('tag (par.par)',vtXmlFilterType.FILTER_TYPE_STRING),
                ('name (par.par)',vtXmlFilterType.FILTER_TYPE_STRING),
                ('desc (par.par)',vtXmlFilterType.FILTER_TYPE_STRING),
            ]
        lTag=lTag + lPar
        lAttr=vtXmlNodeAttrML.GetAttrFilterTypes(self)
        if lAttr is None:
            return lTag
        else:
            return lTag + lAttr
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_ALL
    def GetTranslation(self,name):
        #vtXmlNodeTag.GetTranslation(self,name)
        return vtXmlNodeAttrML.GetTranslation(self,name)
    def Is2Create(self):
        return False
    def IsSkip(self):
        return False
    def Is2Add(self):
        return True
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x01&IDAT(\x91}P=S\x83@\x10]\x1cJ\xc0\xd6\xa3u\x1ck\x89\xd6\xd2;\x93\x1f\xa0\
\xa5q\xe8\x83}\x8e\x9fq9)-l3cM\x92&\x05\xe1\xb0T\xb0\x15\xd2\xc2Q:\x83\x05\
\x8c\x1c\x1fa\xab\xbdw\xfbv\xdf{\xd2\xcb\xb2\x84\xa1\xfa\x8a6\xfe\xfe\xd9\
\xf3\x96"X\xa4\x89\x0c\x00;?\xec\x13\\j\x96|\x0b\x1fL\x04c\x7fw2\xb8>d\x04c\
\x0b\x0ei\xff\xabEp\xa9q\xaa\xa9\x00\xc0\x02\xb2\xb0\x1f\xa0\xe0\x0eY9\xaf\
\xdeQ\x02\x00h\xaa\xd2]\xaf(\xe2\x80,>\xe6v\x9c\xf3"M\x03\xd3\xb4\xa0\xe0\
\x00\xb0\xb0\xa6p\x86Dm\xad\x0bY\xce\xb3\x9c\xa7\xc9\xfe\xf6Ro\xd0\xb6\x93a\
\xd3#%\x0f\xa2\x12\xba\xab\x9a2~\xab\xb4\x8d\x11\x1e\x9f\xea\xec]j\x80\xa2t\
\x08\x8d$\x97\x1a!#}\xbeCV\xd2\xd5\x0c\x14\xb5{an\xc7, .5:\xda0\xb6J\xbe\xfd\
\xb7\xde\x10\xb2\x9c\x9f_\xdc\x1b\x13KS\xeb\xe01F\xf5\xe8wt\xd4C\x95l\xd5#\
\xfdz\xf3\xbenE<\x1e+B\x93\xf5\xe7O\x07\x94\x93\xe8\xc0(\x1b$\x00\xdc\xe0`6\
\xd5\x7fE\xe8\x0f\xd2B\x7f\n\x9b\xaf\xe8\x9f\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return vtXmlNodeTagEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return vtXmlNodeTagAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vEngineeringAttrPanel
        else:
            return None

