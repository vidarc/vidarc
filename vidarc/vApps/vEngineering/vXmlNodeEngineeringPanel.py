#Boa:FramePanel:vXmlNodeXXXPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeXXXPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      
# CVS-ID:       $Id: vXmlNodeEngineeringPanel.py,v 1.3 2008/05/29 01:22:37 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from vidarc.tool.net.vNetXmlWxGui import *
import vidarc.tool.xml.vtXmlDom as vtXmlDom
import vidarc.tool.xml.vtXmlDomConsumer as vtXmlDomConsumer
import vidarc.tool.lang.vtLgBase as vtLgBase

import sys

[wxID_VXMLNODEXXXPANEL] = [wx.NewId() for _init_ctrls in range(1)]

class vXmlNodeXXXPanel(wx.Panel,vtXmlDomConsumer.vtXmlDomConsumer):
    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(3)
        parent.AddGrowableCol(0)
        parent.AddGrowableCol(1)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=2, hgap=0, rows=4, vgap=0)

        self._init_coll_fgsLog_Growables(self.fgsLog)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEXXXPANEL,
              name=u'vXmlNodeXXXPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(312, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 180))
        self.SetAutoLayout(True)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        self._init_ctrls(parent)
        self.selIdx=-1
        
        vtXmlDomConsumer.vtXmlDomConsumer.__init__(self)
        
        self.bModified=False
        self.bAutoApply=False
        self.bBlock=False
        
        self.lstMarkObjs=[]
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def Lock(self,flag):
        if flag:
            pass
        else:
            pass
    def SetAutoApply(self,flag):
        self.bAutoApply=flag
    def __clearBlock__(self):
        self.bBlock=False
    def SetModified(self,state,obj=None):
        if Thread_IsMain()==False:
            vtLog.vtLngCurCls(vtLog.CRITICAL,'called by thread'%(),self)
        if state:
            if obj is None:
                for obj in self.lstMarkObjs:
                    f=obj.GetFont()
                    f.SetWeight(wx.FONTWEIGHT_BOLD)
                    obj.SetFont(f)
                    obj.Refresh()
            else:
                f=obj.GetFont()
                f.SetWeight(wx.FONTWEIGHT_BOLD)
                obj.SetFont(f)
                obj.Refresh()
        else:
            if obj is None:
                for obj in self.lstMarkObjs:
                    f=obj.GetFont()
                    f.SetWeight(wx.FONTWEIGHT_NORMAL)
                    obj.SetFont(f)
                    obj.Refresh()
                #self.viHum.__markModified__(False)
            else:
                f=obj.GetFont()
                f.SetWeight(wx.FONTWEIGHT_NORMAL)
                obj.SetFont(f)
                obj.Refresh()
        self.bModified=state    
    def GetModified(self):
        return self.bModified
    def __isModified__(self):
        if self.bModified:
            return True
        return False
    def Clear(self):
        self.bBlock=True
        self.SetModified(False)
        vtXmlDomConsumer.vtXmlDomConsumer.Clear(self)
        self.selIdx=-1
        wx.CallAfter(self.__clearBlock__)
    def SetDoc(self,doc,bNet):
        vtXmlDomConsumer.vtXmlDomConsumer.SetDoc(self,doc)
    def SetNode(self,node):
        #if self.bModified==True:
        #    if self.bAutoApply:
        #        self.GetNode(self.node)
        #    else:
                # ask
        #        dlg=wx.MessageDialog(self,u'Do you to apply modified data?' ,
        #                    _(u'vLog Info'),
        #                    wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
        #        if dlg.ShowModal()==wx.ID_YES:
        #            self.GetNode(self.node)
        self.bBlock=True
        self.SetModified(False)
        vtXmlDomConsumer.vtXmlDomConsumer.SetNode(self,node)
        wx.CallAfter(self.__clearBlock__)
    def GetNode(self,node=None):
        if node is None:
            node=self.node
        if node is not None:
            pass
        self.SetModified(False)
    def Apply(self,bDoApply=False):
        #self.SetModified(False)
        return False
    def Cancel(self):
        self.SetModified(False)
