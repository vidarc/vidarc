#----------------------------------------------------------------------------
# Name:         vNodeType.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: vNodeType.py,v 1.3 2006/09/27 04:20:51 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeType(vXmlNodeEngineering):
    def __init__(self,tagName='type'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'type ')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x8aIDAT8\x8d\x9dSI\x12\xc0 \x0c\n\x89\xff\xffq\x9b\x9e\xe2P\xc6m\
\xccIA\x10\x8d\x02\x1eV\x95\xef\x935\x86\x07\xec\xa0\x1a\x0bY\xa4\x18\x9b3\
\xdef;\xaaP\xf9|\x9f\x84\x07`f9"w\x89\nk&U\xce\x8as"\xe6\xfdD\xacF|\x1f\xdd\
\xe0D<4\x84\xc7\x95\xb84\xbe_\xba./\'\xed\xf3\xb1\xc1\x8d\x887\xed\x06\x05\
\xec\x92(\xffK\x00\x0f\xac\x8e3zL\xd3.(6\x9b7\x8e\xcf\xee\x9ad\xd6f\xf0w\xd6\
3\x8e\xfe\x82r\x1f\xe9\x18\x85I\xcc\xb7\x88{\x00\x00\x00\x00IEND\xaeB`\x82' 
