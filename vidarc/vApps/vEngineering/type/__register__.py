#----------------------------------------------------------------------------
# Name:         __register__.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: __register__.py,v 1.5 2015/03/01 08:51:14 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust

if vcCust.is2Import(__name__,'__REG_NODE__',True):
    from vidarc.vApps.vEngineering.type.vNodeTypeContainer import vNodeTypeContainer
    from vidarc.vApps.vEngineering.type.vNodeTypeGrp import vNodeTypeGrp
    from vidarc.vApps.vEngineering.type.vNodeType import vNodeType
    import vidarc.vApps.vEngineering.__link__

def RegisterNodes(doc,oRoot=None):
    try:
        if vcCust.is2Import(__name__,'__REG_NODE__',True):
            oBase=vNodeTypeContainer()
            doc.RegisterNode(oBase,True)
            for o in [vNodeTypeGrp(),vNodeType()]:
                doc.RegisterNode(o,False)
            vidarc.vApps.vEngineering.__link__.LinkNodes(doc,[
                    ('typeContainer',['sw','hw','Mod',
                            'Base','Cps','Obj','Dsg','Prd','Res','Struct',
                            'Prc','Fct',
                            'DocRef','Prop','parameters',
                            'Exp','Bug','Test','Tool','ToDo',
                            'S88_phy','S88_Prc','S88_procModel',
                            'Documents','typeGrp',
                            'dataCollector',]),
                    ('typeGrp',['sw','hw','Mod',
                            'Base','Cps','Obj','Dsg','Prd','Res','Struct',
                            'Prc','Fct',
                            'DocRef','Prop','parameters',
                            'Exp','Bug','Test','Tool','ToDo',
                            'S88_phy','S88_Prc','S88_procModel',
                            'Documents','type',
                            'dataCollector',]),
                ])
            if oRoot is not None:
                doc.LinkRegisteredNode(oRoot,oBase)
    except:
        vtLog.vtLngTB(__name__)
