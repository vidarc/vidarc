#!/usr/bin/env python
#----------------------------------------------------------------------
#----------------------------------------------------------------------------
# Name:         encode_bitmaps.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: encode_bitmaps.py,v 1.1 2006/10/09 11:25:20 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    #"   -u -i -n Application Tools01_16.png images.py",
#    "   -u -i -n Mondrian mondrian.ico images.py",
    "-u -i -n TypeGrp           img/TypeGrp01_16.png            images.py",
    "-a -u -n Type              img/Type01_16.png               images.py",
    #"-a -u -n Activities img/ActivitySchedule01_16.png images.py",
    
    #"-u -i -n Splash img/splashEngineering01.png images_splash.py",
    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)

