#----------------------------------------------------------------------------
# Name:         vNodeTypeContainer.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: vNodeTypeContainer.py,v 1.3 2006/09/27 04:20:51 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeTypeContainer(vXmlNodeEngineering):
    def __init__(self,tagName='typeContainer'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'type container')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsBase(self):
        "is base node"
        return True
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xa5IDAT8\x8d\x95\x93\xeb\n\xc5 \x0c\x83\x93\xfa\xdc\x0e9>\xb7\xe6\
\xfc\x99c\xab\xb7\xad0p\xd0~ic%-\xa0\x85jT;\xd32\xf1"H\x0b\x8f\xc2.\xe1\x04\
\xe9(\x8f\x1c\xfe\x02\x01\xc0\x96t\xcb\xd4Q\xe4\x8b\xef@\x02i\xab>\xea\xe0\
\xca\x19\x01V\xf3{P7\xc2\xce\xbc6\xfb\x10\xf0\xd6y\xa7\x18\x00$\xd1\x02\xbe|\
H\x10-\xe0\xf1\xf3\tp\x8aZ3e\xe6\xf2.\x96{0\x8b\xb6x\xaaQDB\x7f\x8d\xce\xe9Q\
\xf1\xb2\x83\xd98\xa3\x95\xb7\x99\x9a\x87\x8c\x8ai\x99\xb6jYG\x91j\xd4\xf2\
\xb1\xdd\x9f\xb3W\xbe\x83=\xa4-\xdd\x1f\xd4\xb9_\x99\x8d\x1a\xd4\xe7\x00\x00\
\x00\x00IEND\xaeB`\x82' 
