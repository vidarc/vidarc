#----------------------------------------------------------------------------
# Name:         vXmlEngineering.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: vXmlEngineering.py,v 1.6 2007/10/31 15:56:03 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vidarc.tool.xml.vtXmlFilterType import vtXmlFilterType

#from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
#from vidarc.vApps.vCustomer.vXmlNodeArea import vXmlNodeArea
from vidarc.tool.xml.vtXmlNodeAddr import vtXmlNodeAddr
from vidarc.tool.xml.vtXmlNodeCalc import vtXmlNodeCalc
from vidarc.tool.xml.vtXmlNodeLog import vtXmlNodeLog
from vidarc.tool.xml.vtXmlNodeCfg import vtXmlNodeCfg

from vidarc.tool.xml.vtXmlNodeFilter import vtXmlNodeFilter
from vidarc.tool.xml.vtXmlNodeMessages import vtXmlNodeMessages
from vidarc.tool.xml.vtXmlNodeMsg import vtXmlNodeMsg
from vidarc.ext.state.veStateStateMachine import veStateStateMachine
from vidarc.ext.state.veStateStateMachineLinker import veStateStateMachineLinker
from vidarc.ext.state.veStateState import veStateState
from vidarc.ext.state.veStateTransition import veStateTransition
import vidarc.ext.state.__register__
import vidarc.ext.report.__register__

import vidarc.vApps.vEngineering.__register__
from vidarc.vApps.vEngineering.vXmlNodeEngineeringRoot import vXmlNodeEngineeringRoot

#from vidarc.vApps.vEngineering.vNodeSW import vNodeSW

def getPluginData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xd3IDAT8\x8d\x8d\x93KH[Q\x10\x86\xbf\x9b\x9bF\x08Z\x03\xc1\x9b\x04k\
\xa1\xc6W\xc0G#\x16\xa4\xab,\x94B\xb7>\x96\x85\xd2M)\xd2\xe0F\xec\xa2"\xba\
\xb1\xdb\x82\xb8\x14\x14\xec"J\x15*\xa5\xa5 \xb8\x08T\x17\x11l\x8c`\x93\x06T\
\xbc\xcd\xb5!b\xaa1\xd7`\x17\x9a\x18\xcd\x89uV\x87\x99\xf9g\xfe\x99\xf3\x8f$\
\x19d\xae[\xd3X\xf7Y\x81\x13X\x1f\x9c\x95\xae\xfb\x0c\xb7M\x14\xf9\x00\xa4,\
\x03Q\xd7\x0e\xd5\xc57{H\x84\xcb\x15\x142\xc8\xb7\xb7\x0f^\xdf\x187\xde\x04\
\x94\xaaMh\xda\x1e#u\xcf\xd0u\x0b\xa3\xbf\xde\x17\xe4\x19D\xd4\xdfT\xbd\xe2\
\xe8\xc8\xc4\xc2\x82\x9fP(A \xa0\x13\x0c\xfe\xc0k}z\x85Q\xd3X\xf7\x991\xfb\
\xb8\xec\xec%\x93)\xc7\xe7\xfb\x80\xae\x87q:\x1f#\xcb\'(\xca=\xd6\xd6\xbeS[\
\xabS\xbfj\x84\x0b\xcc\x95\x11\x86\xaa\xfb\t\x87wX\\\x9cFQ\x0e\xb0X\xcaq8Z9=\
M\xa1\xaa;47\xbbI&\x8dD"\xbf\xe1\x91\xb5p\x07\x0eG\x156\x9b\x95X,\x02hlm\xa9\
\xcc\xcf\x7f&\x9dN\xd0\xd7\xf7\x12U\x8d27\xf7\x89x\xfc/e\x9c\x17\x90m\x9d\
\x8d\xc3\xd9\x02\xf7\x13\x0f\x81;x<\x1e\x1a\x1aZ\xd9\xdf\xff\xc3\xeen\x00\
\xafw\x00\xb3\xd9\xcc\xd4\xd4GJK\xe38\x9dvb\xf6\xf4\xf9\x12\xe1\xf2O\'\x0e\
\xdf\xb1\xb1\x11\xc5\xef\xff\xc9\xf6\xf6!\xbd\xbd/\x18\x1f\x9fF\xd73\xcc\xcc\
|%\x95\xd2ho\xafa\xbd%\x99\xc3\x14\x15R\xcf\xc9s\x14\xc5\x8e,\x1f\xb3\xb2\
\x12$\x1a]\xc5\xe52\xb1T\x19#\xbfiQ\x1d\xf8J&\xe1\x00\x9eh=ln.\xe3v\xdf\xcd\
\x81\xf3\xed\xbfJ\xfcR\xe1\xa3\xab\xab\x8d\xa5JM\x18\x97D\xd7(\x1a\xa9\xd81\
\t\x19\x88\xd4Y\xec\xc4\xff\x01\x10H\xb2uo\x93p*\x00\x00\x00\x00IEND\xaeB`\
\x82'

class vXmlEngineering(vtXmlDomReg):
    VERBOSE=1
    TAGNAME_REFERENCE='tag'
    TAGNAME_ROOT='projectengineerings'
    #NODE_ATTRS=[
    #        ('name',None,'name',None),
    #        ('country',None,'contry',None),
    #        ('region',None,'region',None),
    #        ('distance',None,'distance',None),
    #        ('coor',None,'coor',None),
    #        ]
    APPL_REF=['vPrjDoc','vDoc','vPrj','vHum','vGlobals','vMsg']
    def __init__(self,appl='vEngineering',attr='id',skip=[],synch=False,verbose=0,
                audit_trail=True):
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        try:
            vtXmlDomReg.__init__(self,appl=appl,attr=attr,skip=skip,synch=synch,
                                verbose=verbose,audit_trail=audit_trail)
            self.actNr=None
            oRoot=vXmlNodeEngineeringRoot(tagName='root')
            self.RegisterNode(oRoot,False)
            oRoot=vXmlNodeEngineeringRoot()
            self.RegisterNode(oRoot,False)
            
            vidarc.ext.state.__register__.RegisterNodes(self,bRegStateAsRoot=True)
            vidarc.ext.report.__register__.RegisterNodes(self,bRegDocAsRoot=True)
            
            vidarc.vApps.vEngineering.__register__.RegisterNodes(self,oRoot=oRoot)
            
            #oEngineering=vXmlNodeEngineering()
            oAddr=vtXmlNodeAddr()
            oLog=vtXmlNodeLog()
            #oCfg=vtXmlNodeCfg()
            #oArea=vXmlNodeArea()
            #self.RegisterNode(oCfg,True)
            self.RegisterNode(oAddr,False)
            self.RegisterNode(oLog,False)
            #self.RegisterNode(oArea,False)
            #self.LinkRegisteredNode(oInst,oLog)
            #self.LinkRegisteredNode(oInst,oAcc)
            #self.LinkRegisteredNode(oCust,oAddr)
            #self.LinkRegisteredNode(oCfg,oArea)
            #self.LinkRegisteredNode(oArea,oArea)
            
        except:
            vtLog.vtLngTB(self.appl)
        self.SetDftLanguages()
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'prjengs')
    #def New(self,revision='1.0',root='projectengineerings',bConnection=False):
    #    vtXmlDomReg.New(self,revision,root)
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elem=self.createSubNode(self.getRoot(),'prjengs')
        if bConnection==False:
            self.CreateReq()
        #if bConnection==False:
        #    e=self.createSubNodeTextAttr(elem,'cfg','','id','')
        #    self.checkId(e)
        #    self.processMissingId()
        #    self.clearMissing()
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        #self.AlignDoc()
    def SetNetDocs(self,d):
        vtXmlDomReg.SetNetDocs(self,d)
        self.SetInfos2Get4RegisteredNodes()
    def CreateIds(self,attr='id'):
        self.actNr=None
        vtXmlDomReg.CreateIds(self,attr)
    def Build(self):
        vtXmlDomReg.Build(self)
        oSMlinker=self.GetRegisteredNode('stateMachineLinker')
        tmp=self.getChildByLst(None,['cfg','stateMachineLinker'])
        oSMlinker.SetNode(tmp)
        self.GenerateSortedActNrIds()
    def GetStateMachine(self,nodeName,attrName):
        oSMlinker=self.GetRegisteredNode('stateMachineLinker')
        return oSMlinker.GetRunner(nodeName,attrName)
    def GenerateSortedActNrIds(self):
        self.actNr=self.getSortedNodeIdsRec([],
                        'Activity','actID',None)
    def GetSortedActNrIds(self):
        try:
            return self.actNr
        except:
            self.GenerateSortedActNrIds()
            return self.actNr
