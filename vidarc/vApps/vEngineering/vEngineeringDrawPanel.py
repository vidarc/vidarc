#----------------------------------------------------------------------------
# Name:         vEngineeringDrawPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060924
# CVS-ID:       $Id: vEngineeringDrawPanel.py,v 1.2 2009/10/18 09:03:11 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.draw.vtDrawCanvasXmlPanel import *
from vidarc.tool.draw.vtDrawCanvasXmlGroup import *

from vidarc.tool.draw.vtDrawCanvasRectangle import *
from vidarc.tool.draw.vtDrawCanvasLine import *
from vidarc.tool.draw.vtDrawCanvasText import *
from vidarc.tool.draw.vtDrawCanvasTextMultiLine import *
from vidarc.tool.draw.vtDrawCanvasBlock import *
from vidarc.tool.draw.vtDrawCanvasBlockIO import *

import os
import vidarc.tool.xml.vtXmlHierarchy as vtXmlHierarchy
import vidarc.tool.log.vtLog as vtLog

import vidarc.config.vcLog as vcLog
VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')

def xmlCMcreate(parent,canvas):
    r=vtDrawCanvasRectangle(id=-1,name='rect000',x=0,y=0,w=1,h=1,layer=parent.GetLayer())
    parent.AddXml(r)
    r=vtDrawCanvasRectangle(id=-1,name='rect001',x=1,y=1,w=1,h=1,layer=parent.GetLayer())
    parent.AddXml(r)
    iX,iY,iW,iH=parent.calcExtend(canvas)
    parent.SetWidth(iW)
    parent.SetHeight(iH)

def xmlBlkCreate(node,canvas):
    layer=0
    doc=canvas.GetDocExt()
    nodeExt=canvas.GetNodeExt()
    sName=vtXmlHierarchy.getTagNamesWithRel(doc,nodeExt,None,node)
    print sName
    id=doc.getKey(node)
    o=vtDrawCanvasBlock(id=long(id),name=sName,x=0,y=0,w=8,h=10,layer=layer)
    return o
def xmlBlkAddAttr(obj,node,attr,dfts,canvas):
    layer=0
    doc=canvas.GetDocExt()
    sAttrName=doc.getTagName(attr)
    sAttrVal=doc.getAttributeVal(attr)
    iTyp=vtDrawCanvasBlockIO.TYPE_IN
    iDsp=vtDrawCanvasBlockIO.DISPLAY_VAL
    obj.AddAttr(sAttrName,sAttrVal,defaults,iTyp,iDsp,canvas)
    pass
def xmlBlkDelAttr(o,node,attr,canvas):
    doc=canvas.GetDocExt()
    sAttrName=self.docExt.getTagName(attr)
    pass

class vEngineeringDrawPanel(vtDrawCanvasXmlPanel):
    DFT_COLOR=[
        (178, 107, 000),
        ( 94, 154,  94),
        (148, 148, 148),
        #( 79,  79,  59),
        (210,   0,   0),
        (120,  78, 146),
        ( 31, 156, 127),
        (178, 100, 178),
        (  0,   0,   0),
        ]
    DFT_DIS_COLOR=[
        (167, 100, 000),
        (167, 136, 000),
        ( 67, 135, 112),
        (116, 130, 147),
        (100, 166, 100),
        (150, 114,  22),
        (147,  82, 147),
        (  0,   0,   0),
        ]
    SELECTED_LAYER=3
    SELECTED_WIDTH=10
    SELECTED_HEIGHT=10
    
    FOCUS_LAYER=3
    FOCUS_WIDTH=6
    FOCUS_HEIGHT=6
    
    VERBOSE=0
    def __init__(self, parent, 
                id=wx.NewId(), pos=wx.DefaultPosition, size=wx.Size(100, 50), 
                style=wx.TAB_TRAVERSAL, name=u'vEngineeringDrawPanel',bTex=False):
        vtDrawCanvasXmlPanel.__init__(self,parent,id=id,pos=pos,size=size,style=style,name=name)
        
        obj=vtDrawCanvasObjectBase()
        if bTex:
            drv=obj.DRV_LATEX
        else:
            drv=obj.DRV_WX
        self.SetConfig(obj,drv,max=(100,100),grid=(20,20))
        self.RegisterObject(vtDrawCanvasLine())
        self.RegisterObject(vtDrawCanvasRectangle())
        self.RegisterObject(vtDrawCanvasText())
        self.RegisterObject(vtDrawCanvasTextMultiLine())
        self.RegisterObject(vtDrawCanvasGroup())
        self.RegisterObject(vtDrawCanvasXmlGroup())
        self.RegisterObject(vtDrawCanvasBlock())
        self.RegisterObject(vtDrawCanvasBlockIO())
        for s in ['hw','sw','App','Mod','HW',
                    'S88_uproc','S88_proc','S88_op','S88_ph',
                    'S88_ET','S88_ST','S88_AR','S88_PRC','S88_UT','S88_EM','S88_CM',
                    'S88_Prc','S88_PrcS','S88_PrcO','S88_PrcA',
                    'struct','class','array',
                    't_class','t_array','t_char','t_int',
                    't_long','t_float','t_string',
                    'method']:
            self.RegisterXmlActions(s,xmlBlkCreate,xmlBlkAddAttr,xmlBlkDelAttr)
        self.SetActivateEnable(True)
    def DeactivateDevices(self,lst):
        for o in self.objs:
            #print o
            try:
                lst.index(o.GetId())
                o.SetActive(False)
            except:
                o.SetActive(True)
                pass
            self.ShowObj(o,False)
        #self.ShowAll()
    def dummy(self):
        pass
    def __calcSize__(self):
        #vtLog.CallStack('')
        #print 'grid:',self.grid
        #print 'max :',self.max
        vtDrawCanvasXmlPanel.__calcSize__(self)
    def SetDoc(self,doc,bNet=False):
        vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtDrawCanvasXmlPanel.SetDoc(self,doc,bNet)
        self.SetDocExt(doc,bNet)
    def __SetNode__(self,node):
        vtDrawCanvasXmlPanel.__SetNode__(self,node,bBlockCheck=True)
        nodeExt=self.doc.GetRelBaseNode(self.node)
        idExtNew=''
        if self.doc is not None:
            if nodeExt is not None:
                idExtNew=self.doc.getKey(nodeExt)
        idExtOld=''
        if self.docExt is not None:
            if self.nodeExt is not None:
                idExtOld=self.docExt.getKey(self.nodeExt)
        if self.VERBOSE>0 or VERBOSE>0:
            self.__logDebug__('idExtNew:%s;idExtOld:%s'%(idExtNew,idExtOld))
        try:
            if self.nodeExt is not None:
                if self.doc.isSameKey(nodeExt,self.docExt.getKey(self.nodeExt))==False:
                    vtDrawCanvasXmlPanel.SetNodeExt(self,nodeExt)
            else:
                vtDrawCanvasXmlPanel.SetNodeExt(self,nodeExt)
        except:
            vtLog.vtLngTB(self.GetName())
