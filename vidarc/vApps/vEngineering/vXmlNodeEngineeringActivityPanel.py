#----------------------------------------------------------------------------
# Name:         vXmlNodeEngineeringActivityPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20130501
# CVS-ID:       $Id: vXmlNodeEngineeringActivityPanel.py,v 1.1 2013/06/17 12:58:26 wal Exp $
# Copyright:    (c) 2013 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import vLang
    from vGuiFrmWX import vGuiFrmWX
    
    import vidarc.tool.art.vtArt as vtArt
    from vidarc.tool.input.vtInputUnique import vtInputUnique
    from vidarc.tool.input.vtInputTextML import vtInputTextML
    from vidarc.tool.input.vtInputTextMultiLineML import vtInputTextMultiLineML
    from vidarc.vApps.vHum.vXmlHumInputTreeUsr import vXmlHumInputTreeUsr
    from vidarc.vApps.vHum.vXmlHumInputTreeGroupsUsers import vXmlHumInputTreeGroupsUsers
    from vidarc.tool.xml.vtXmlNodeLogPanel import vtXmlNodeLogPanel
    from vidarc.ext.state.veStateInput import veStateInput
    from vidarc.tool.time.vtTimeDateGM import vtTimeDateGM
    from vidarc.tool.xml.vtXmlNodeGuiPanel import vtXmlNodeGuiPanel
    from vidarc.tool.xml.vtXmlDomConsumerLang import vtXmlDomConsumerLang
    
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vXmlNodeEngineeringActivityPanel(vtXmlNodeGuiPanel,vtXmlDomConsumerLang):
    def __initCtrl__(self,*args,**kwargs):
        global _
        _=vLang.assignPluginLang('vEngineering')
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        self.SetVerbose(__name__,iVerbose=-1,kwargs=kwargs)
        self.__clr__()
        lWid=[]
        iRow=0
        lWid.append(('lbl',(iRow,0),(1,1),{'name':'lblNr',
                'label':_(u'number'),
                },None))
        lWid.append((vtInputUnique,(iRow,1),(1,1),{'name':'viNr',
                #'tip':_(u'unique number'),
                #'value':''
                },{
                #'ent':self.OnNrEnter,
                #'txt':self.OnNrText,
                }))
        lWid.append(('lbl',(iRow,2),(1,1),{'name':'lblAuthor',
                'label':_(u'author'),
                },None))
        lWid.append((vXmlHumInputTreeUsr,(iRow,3),(1,1),{'name':'viAuthor',
                #'tip':_(u'unique number'),
                #'value':''
                },{
                #'ent':self.OnNrEnter,
                #'txt':self.OnNrText,
                }))
        iRow+=1
        lWid.append(('lbl',(iRow,0),(1,1),{'name':'lblSer',
                'label':_(u'severity'),
                },None))
        lWid.append(('chc',(iRow,1),(1,1),{'name':'chcSeverity',
                'choices':[u'blocker', u'critical', u'major',
                        u'normal', u'minor', u'trivial', u'enhancement'],
                },None))
        lWid.append(('lbl',(iRow,2),(1,1),{'name':'lblPrior',
                'label':_(u'priority'),
                },None))
        lWid.append(('chc',(iRow,3),(1,1),{'name':'chcPrior',
                'choices':[u'highest', u'higher', u'normal',
                        u'lower', u'lowest'],
                },None))
        iRow+=1
        lWid.append(('lbl',(iRow,0),(1,1),{'name':'lblStartDT',
                'label':_(u'start date'),
                },None))
        lWid.append((vtTimeDateGM,(iRow,1),(1,1),{'name':'viStartDt',
                #'tip':_(u'unique number'),
                #'value':''
                },{
                #'ent':self.OnNrEnter,
                #'txt':self.OnNrText,
                }))
        lWid.append(('lbl',(iRow,2),(1,1),{'name':'lblEndDT',
                'label':_(u'end date'),
                },None))
        lWid.append((vtTimeDateGM,(iRow,3),(1,1),{'name':'viEndDt',
                #'tip':_(u'unique number'),
                #'value':''
                },{
                #'ent':self.OnNrEnter,
                #'txt':self.OnNrText,
                }))
        iRow+=1
        lWid.append(('lbl',(iRow,0),(1,1),{'name':'lblName',
                'label':_(u'name'),
                },None))
        lWid.append((vtInputTextML,(iRow,1),(1,3),{'name':'viName',
                #'tip':_(u'unique number'),
                #'value':''
                },{
                #'ent':self.OnNrEnter,
                #'txt':self.OnNrText,
                }))
        iRow+=1
        lWid.append(('lbl',(iRow,0),(1,1),{'name':'lblState',
                'label':_(u'state'),
                },None))
        lWid.append((veStateInput,(iRow,1),(1,3),{'name':'viState',
                #'tip':_(u'unique number'),
                #'value':''
                },{
                #'ent':self.OnNrEnter,
                #'txt':self.OnNrText,
                }))
        iRow+=1
        lWid.append(
            ('szBoxHor',(iRow,0),(1,2), {'name':'bxsPopup'},[
                ('tgPopup',     0,4,{'name':'tgAssign','size':(32,32),
                        'tip':_(u'Assignment'),
                        'bmp':vtArt.getBitmap(vtArt.Engineer)},None),
                ('tgPopup',     0,16,{'name':'tgLog','size':(32,32),
                        'tip':_(u'Log'),
                        'bmp':vtArt.getBitmap(vtArt.Log)},None),
                ]))
        iRow+=1
        lWid.append(('lbl',(iRow,0),(1,1),{'name':'lblDesc',
                'label':_(u'description'),
                },None))
        lWid.append((vtInputTextMultiLineML,(iRow,1),(2,3),{'name':'viDesc',
                #'tip':_(u'unique number'),
                #'value':''
                },{
                #'ent':self.OnNrEnter,
                #'txt':self.OnNrText,
                }))
        vtXmlNodeGuiPanel.__initCtrl__(self,lWid=lWid)
        vtXmlDomConsumerLang.__init__(self)
        self.viName.SetTagName('name')
        self.viDesc.SetTagName('desc')
        self.viState.SetNodeAttr('Activity','actState')
        self.viStartDt.SetTagName('start')
        self.viEndDt.SetTagName('end')
        self.viAuthor.SetTagNames('usr','name')
        self.viNr.SetTagName('actID')
        #self.PrintMsg(u'vXmlNodeEngineeringActivity init')
        self.__setUpPopUp__()
        return [(6,1),],[(1,1),(3,1)]
    def ClearLang(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            
        except:
            self.__logTB__()
    def UpdateLang(self):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        try:
            self.objRegNode.SetLang(self.doc.GetLang())
            l=self.objRegNode.GetSeverityTrans()
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__({'lang':self.doc.GetLang(),'l':l})
            if l is not None:
                self.chcSeverity.Clear()
                for s in l:
                    self.chcSeverity.Append(s)
            l=self.objRegNode.GetPriorTrans()
            if self.VERBOSE:
                print l
            if l is not None:
                self.chcPrior.Clear()
                for s in l:
                    self.chcPrior.Append(s)
        except:
            self.__logTB__()
    def __setUpPopUp__(self):
        if self.IsMainThread()==False:
            return
        try:
            self.__setUpPopUpAssign__()
            self.__setUpPopUpLog__()
        except:
            self.__logTB__()
    def __setUpPopUpAssign__(self):
        if self.IsMainThread()==False:
            return
        try:
            p=vGuiFrmWX(name='popAssign',parent=self.tgAssign.GetWid(),
                    kind='popup',iArrange=0,
                    widArrange=self.tgAssign.GetWid(),bAnchor=True,
                    pnSz=(300,120),
                    lWid=vXmlHumInputTreeGroupsUsers)
            #p.BindEvent('ok',self.OnSrcPopupOk)
            p.GetName()
            self.popAssign=p
            self.viAssign=p.pn
            self.tgAssign.SetWidPopup(p)
        except:
            self.__logTB__()
    def __setUpPopUpLog__(self):
        if self.IsMainThread()==False:
            return
        try:
            p=vGuiFrmWX(name='popLog',parent=self.tgLog.GetWid(),
                    kind='popup',iArrange=0,
                    widArrange=self.tgLog.GetWid(),bAnchor=True,
                    pnSz=(300,120),
                    lWid=vtXmlNodeLogPanel)
            #p.BindEvent('ok',self.OnSrcPopupOk)
            p.GetName()
            self.popLog=p
            self.pnLog=p.pn
            self.tgLog.SetWidPopup(p)
        except:
            self.__logTB__()
    def __Clear__(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            #self.popSrc.lstInp.DeleteAllItems()
            #self.SetModified(False,self.popSrc.lstInp)
            self.viName.Clear()
            self.viDesc.Clear()
            self.viAuthor.Clear()
            self.viAssign.Clear()
            self.viNr.Clear()
            try:
                self.chcSeverity.SetSelection(self.objRegNode.GetSeverityIdx(None))
                self.chcPrior.SetSelection(self.objRegNode.GetPriorIdx(None))
            except:
                self.chcSeverity.SetSelection(3)
                self.chcPrior.SetSelection(2)
            self.viStartDt.Clear()
            self.viEndDt.Clear()
            self.pnLog.Clear()
            self.__clr__()
        except:
            self.__logTB__()
        try:
            self.chcSeverity.SetSelection(self.objRegNode.GetSeverityIdx(None))
            self.chcPrior.SetSelection(self.objRegNode.GetPriorIdx(None))
        except:
            self.chcSeverity.SetSelection(3)
            self.chcPrior.SetSelection(2)
    def __Close__(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            self.CallWidChild(-1,'__Close__')
            
        except:
            self.__logTB__()
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            oAct=doc.GetReg('Activity')
            self.SetRegNode(oAct)
            vtXmlDomConsumerLang.SetDoc(self,doc)
            
            self.viNr.SetDoc(doc)
            self.viName.SetDoc(doc)
            self.viDesc.SetDoc(doc)
            self.viState.SetDoc(doc)
            self.viStartDt.SetDoc(doc)
            self.viEndDt.SetDoc(doc)
            self.viAuthor.SetDoc(doc,bNet)
            self.viAssign.SetDoc(doc)
            
            oLog=doc.GetReg('log')
            self.pnLog.SetRegNode(oLog)
            self.pnLog.SetDoc(doc,bNet=bNet)
            
            if doc is not None:
                self.viNr.SetUniqueFunc(doc.GetSortedActNrIds)
            if bDbg or 1:
                self.__logDebug__(dDocs)
            
            if dDocs is not None:
                if 'vHum' in dDocs:
                    dd=dDocs['vHum']
                    self.viAuthor.SetDocTree(dd['doc'],dd['bNet'])
                    self.viAssign.SetDocTree(dd['doc'],dd['bNet'])
            #self.UpdateLang()
        except:
            self.__logTB__()
    def __SetNode__(self,node):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            self.viName.SetNode(node)
            self.viDesc.SetNode(node)
            self.viState.SetNode(node)
            self.viStartDt.SetNode(node)
            self.viEndDt.SetNode(node)
            self.viAuthor.SetNode(node)
            self.viAssign.SetNode(node)
            self.viNr.SetNode(node)
            self.chcSeverity.SetSelection(self.objRegNode.GetSeverityIdx(node))
            self.chcPrior.SetSelection(self.objRegNode.GetPriorIdx(node))
            self.pnLog.SetNode(node)
        except:
            self.__logTB__()
    def __copyActivityVal__(self,node,srcId):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            nodeSrc=self.doc.getNodeByIdNum(srcId)
            if nodeSrc is None:
                return
            o=self.objRegNode
            #o.SetName(node,o.GetName(nodeSrc))
            #o.SetDesc(node,o.GetDesc(nodeSrc))
            #o.SetStart(node,o.GetStart(nodeSrc))
            #o.SetEnd(node,o.GetEnd(nodeSrc))
            ##o.SetAuthor(node,o.GetAuthor(nodeSrc))
            #o.SetSeverityIdx(node,o.GetSeverityIdx(nodeSrc))
            #o.SetPriorIdx(node,o.GetPriorIdx(nodeSrc))
            self.doc.AlignNode(node)
        except:
            self.__logTB__()
    def __getActivityVal__(self,o,node,prevId):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            self.__GetNode__(node)
            if prevId>=0:
                nodeOld=self.doc.getNodeByIdNum(prevId)
                if nodeOld is not None:
                    self.objRegNode.SetPrev(node,self.objRegNode.GetNr(nodeOld))
            self.doc.AlignNode(node)
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__({})
            self.viName.GetNode(node)
            self.viDesc.GetNode(node)
            self.viState.GetNode(node)
            self.viStartDt.GetNode(node)
            self.viEndDt.GetNode(node)
            self.viAuthor.GetNode(node)
            self.viAssign.GetNode(node)
            self.viNr.GetNode(node)
            self.objRegNode.SetSeverityIdx(node,self.chcSeverity.GetSelection())
            self.objRegNode.SetPriorIdx(node,self.chcPrior.GetSelection())
            #self.SetModified(False,self.viAssign)
        except:
            self.__logTB__()
    def __Lock__(self,bFlag):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if bFlag:
                self.viNr.Enable(False)
                self.viName.Enable(False)
                self.viDesc.Enable(False)
                self.viState.Enable(False)
                self.viStartDt.Enable(False)
                self.viEndDt.Enable(False)
                self.viAuthor.Enable(False)
                self.chcSeverity.Enable(False)
                self.chcPrior.Enable(False)
                self.viAssign.Enable(False)
                self.pnLog.Enable(False)
                pass
            else:
                self.viNr.Enable(True)
                self.viName.Enable(True)
                self.viDesc.Enable(True)
                self.viState.Enable(True)
                self.viStartDt.Enable(True)
                self.viEndDt.Enable(True)
                self.viAuthor.Enable(True)
                self.chcSeverity.Enable(True)
                self.chcPrior.Enable(True)
                self.viAssign.Enable(True)
                self.pnLog.Enable(True)
                pass
        except:
            self.__logTB__()
            
    def __clr__(self):
        self.dCfg={}
