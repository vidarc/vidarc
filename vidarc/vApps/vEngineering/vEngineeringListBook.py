#----------------------------------------------------------------------------
# Name:         vEngineeringListBook.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: vEngineeringListBook.py,v 1.9 2012/05/16 00:07:06 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------


import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import wx
    
    import vidarc.vApps.vEngineering.vXmlNodeEngineeringPanel as vXmlNodeEngineeringPanel
    #import vidarc.tool.xml.vtXmlNodeAddrPanel as vtXmlNodeAddrPanel
    import vidarc.vApps.vEngineering.vEngineeringAttrPanel as vEngineeringAttrPanel
    import vidarc.vApps.vEngineering.vXmlNodeEngineeringLinkPanel as vXmlNodeEngineeringLinkPanel
    #import vidarc.ext.report.veRepDocPanel as veRepDocPanel
    import vidarc.vApps.vEngineering.vXmlNodeEngineeringFileAttrPanel as vXmlNodeEngineeringFileAttrPanel
    import vidarc.vApps.vEngineering.vXmlNodeEngineeringActivitiesPanel as vXmlNodeEngineeringActivitiesPanel
    import vidarc.tool.xml.vtXmlNodeLogPanel as vtXmlNodeLogPanel
    import types
    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.xml.vtXmlDomConsumer as vtXmlDomConsumer

    import vidarc.vApps.vEngineering.images as imgEng
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class vEngineeringListBook(wx.Notebook,vtXmlDomConsumer.vtXmlDomConsumer):
    VERBOSE=0
    def _init_ctrls(self, prnt,id, pos, size, style, name):
        wx.Notebook.__init__(self, style=style, name=name, parent=prnt, pos=pos, id=id, size=size)

    def __init__(self, parent, id=wx.NewId(), pos=wx.DefaultPosition, 
                    size=wx.Size(100, 50), style=wx.TAB_TRAVERSAL, name=''):
        vtXmlDomConsumer.vtXmlDomConsumer.__init__(self)
        self._init_ctrls(parent,id, pos, size, style, name)
        
        self.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGED, self.OnPageChanged)
        self.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGING, self.OnPageChanging)
        
        self.doc=None
        self.sTagOld=None
        
        edit=[u'vEngineeringAttr',#u'veRepDoc',
                u'vtXmlNodeLog',
                u'vXmlNodeEngineeringFileAttr','vXmlNodeEngineeringActivities',
                u'vXmlNodeEngineeringLink',
                ]
        editName=[_(u'Attributes'),#_(u'Document'),
                _(u'Log'),_(u'File'),_(u'Activities'),
                _(u'Link'),
                ]
        bmps=[vtArt.getBitmap(vtArt.Attr),#vtArt.getBitmap(vtArt.PdfOut),
                vtArt.getBitmap(vtArt.Log),
                vtArt.getBitmap(vtArt.Browse),imgEng.getActivitiesBitmap(),
                vtArt.getBitmap(vtArt.Link),
                ]
        self.lstTagName=[None,None,None,None,None]
        il = wx.ImageList(16, 16)
        for bmp in bmps:
            il.Add(bmp)
        self.AssignImageList(il)
        
        self.lstName=edit
        self.lstPanel=[]
        i=0
        for e in edit:
            try:
                s='%sPanel'%e
                f=getattr(eval('%sPanel'%e),'%sPanel'%e)
                panel=f(self,id=wx.NewId(),
                        pos=(0,0),size=(190,340),style=0,name=s)
                if hasattr(panel,'GetWid'):
                    self.AddPage(panel.GetWid(),editName[i],False,i)
                else:
                    self.AddPage(panel,editName[i],False,i)
                self.lstPanel.append(panel)
                i=i+1
            except:
                vtLog.vtLngTB(self.GetName())
        self.lstPanel[0].SetFuncIs2Skip(self.__is2Skip__)
        #vtXmlFilter.EVT_THREAD_FILTER_ELEMENTS(panel,self.OnFilterElement)
    def __is2Skip__(self,sTag):
        if sTag in ['tag','name','description']:
            return True
        if sTag.find('__')==0:
            return True
        return False
    def Clear(self):
        for pn in self.lstPanel:
            pn.Clear()
        vtXmlDomConsumer.vtXmlDomConsumer.Clear(self)

    def Lock(self,flag):
        for pn in self.lstPanel:
            pn.Lock(flag)
    def SetModified(self,flag):
        for pn in self.lstPanel:
            pn.SetModified(flag)
    def __isModified__(self):
        for pn in self.lstPanel:
            if pn.GetModified():
                return True
        return False
    def GetModified(self):
        return self.__isModified__()
    def SetNetDocHuman(self,doc,bNet=False):
        try:
            idx=self.lstName.index('vtXmlNodeLog')
            self.lstPanel[idx].SetNetDocHuman(doc,bNet)
        except:
            pass
            #vtLog.vtLngTB(self.GetName())
    def SetRegNode(self,obj):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'tagname:%s'%(obj.GetTagName()),self)
        #if obj.GetTagName()=='Doc':
        #    self.lstPanel[1].SetRegNode(obj)
        if obj.GetTagName()=='Activity':
            self.lstPanel[3].SetRegNode(obj)
    def SetNetDocs(self,d):
        for pn in self.lstPanel:
            if hasattr(pn,'SetNetDocs'):
                pn.SetNetDocs(d)
    def SetDoc(self,doc,bNet=False):
        bDbg=True
        vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
        vtXmlDomConsumer.vtXmlDomConsumer.SetDoc(self,doc)
        #if bNet:
        #    EVT_NET_XML_LOCK(doc,self.OnLock)
        #    EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        #self.doc=doc
        #oDoc=doc.GetReg('Doc')
        #self.lstPanel[1].SetRegNode(oDoc)
        oLog=doc.GetReg('log')
        self.lstPanel[1].SetRegNode(oLog)
        dDocs=doc.GetNetDocs()
        #for i in xrange(0,len(self.lstName)):
        #    w=self.lstPanel[i]
        for w in self.lstPanel:
            if bDbg:
                vtLog.vtLngCurWX(vtLog.DEBUG,'cls:%s'%(w.__class__.__name__),self)
            if hasattr(w,'SetDoc'):
                w.SetDoc(doc,bNet)
            #else:
            #    if hasattr(w,'SetDoc'):
            #        w.SetDoc(doc,bNet)
            #    if hasattr(w,'SetNetDocs'):
            #        w.SetNetDocs(dDocs)
        #self.SetNetDocs(doc.GetNetDocs())
        
    def OnLock(self,evt):
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                self.Lock(False)
            else:
                self.Lock(True)
        evt.Skip()
    def OnUnLock(self,evt):
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['released']:
                self.Lock(False)
        evt.Skip()
        
    def SetNode(self,node):
        if self.__isModified__():
            # ask
            dlg=wx.MessageDialog(self,_(u'Do you to apply modified data?') ,
                        u'vEngineering',
                        wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            if dlg.ShowModal()==wx.ID_YES:
                self.GetNode()
                #wx.PostEvent(self,vgpDataChanged(self,self.node))
        self.SetModified(False)
        if 0:
            sTagName=self.doc.getTagName(node)
            
            if sTagName in ['Engineering']:
                dNode={}
                for sName in self.lstTagName:
                    dNode[sName]=None
                if 1:#sTagName=='Engineering':
                    dNode[None]=node
                    #dNode['address']=self.doc.getChild(node,'address')
                #elif sTagName=='address':
                #    dNode['address']=node
                #    dNode[None]=self.doc.getParent(node)
                self.node=node
                
                i=0
                for pn in self.lstPanel:
                    try:
                        tmp=dNode[self.lstTagName[i]]
                    except:
                        tmp=None
                    pn.SetNode(tmp)
                    i+=1
                return
            else:
                for pn in self.lstPanel:
                    pn.SetNode(None)
                return
        
        vtXmlDomConsumer.vtXmlDomConsumer.SetNode(self,node)
        if self.node is not None:
            
            sTag=self.doc.getTagName(self.node)
            if sTag!=self.sTagOld:
                obj=self.doc.GetReg(sTag)
                if obj is None:
                    vtLog.vtLngCurWX(vtLog.ERROR,'reg:%s not found'%(sTag),self)
                else:
                    self.lstPanel[0].SetRegNode(obj)
                    self.lstPanel[2].SetRegNode(obj)
                    self.lstPanel[4].SetRegNode(obj)
                    self.sTagOld=sTag
        i=0
        bDbg=False
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'id:%s'%(self.doc.getKey(node)),self)
            bDbg=True
        for pn in self.lstPanel:
            if bDbg:
                vtLog.vtLngCurWX(vtLog.DEBUG,
                        'i:%d;tagName:%s'%(i,self.lstTagName[i]),self)
            try:
                if self.lstTagName[i] is None:
                    tmp=node
                elif node is None:
                    tmp=node
                else:
                    tmp=self.doc.getChild(node,self.lstTagName[i])
                    if tmp is None:
                        try:
                            vtLog.vtLngCS(vtLog.WARN,'attribute node %s not found in;%s'%(self.lstTagName[i],repr(node)),
                                    origin=self.GetName())
                            oReg=self.doc.GetRegisteredNode(self.lstTagName[i])
                            if oReg is not None:
                                o,tmp=self.doc.CreateNode(node,self.lstTagName[i])
                            else:
                                tmp=self.doc.createSubNode(node,self.lstTagName[i],False)
                                vtLog.vtLngCS(vtLog.INFO,'no registered node %s'%(self.lstTagName[i]),
                                        origin=self.GetName())
                                
                        except:
                            vtLog.vtLngTB(self.GetName())
                #vtLog.vtLngCurWX(vtLog.DEBUG,'tagname:%s id:%s;node:%s'%(self.doc.getTagName(tmp),self.doc.getKey(tmp),repr(node)),self)
                pn.SetNode(tmp)
            except:
                vtLog.vtLngTB(self.GetName())
            i+=1
        
    def GetNode(self,node=None):
        #sel = self.GetSelection()
        #self.lstPanel[sel].GetNode(node)
        i=0
        for pn in self.lstPanel:
            try:
                pn.GetNode(node)
            except:
                vtLog.vtLngCurWX(vtLog.ERROR,'panel:%s;i:%s'%(pn.GetName(),i),self)
                vtLog.vtLngTB(self.GetName())
            i+=1
    def Apply(self):
        self.GetNode()
    def Cancel(self):
        self.SetModified(False)
        self.SetNode(self.node)
    def OnPageChanged(self, event):
        old = event.GetOldSelection()
        sel = event.GetSelection()
        if sel>=len(self.lstPanel):
            return
        #try:
        #    self.nodeSet.index(sel)
        #except:
        #    self.lstPanel[sel].SetNode(self.lstName[sel],self.node)
        #    self.nodeSet.append(sel)
        event.Skip()

    def OnPageChanging(self, event):
        sel = event.GetSelection()
        event.Skip()
    def __getPanel__(self,name):
        try:
            idx=self.lstName.index(name)
            panel=self.lstPanel[idx]
            return panel
        except:
            return None
