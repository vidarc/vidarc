#----------------------------------------------------------------------------
# Name:         vNodeProceduralOperation.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060923
# CVS-ID:       $Id: vNodeProceduralOperation.py,v 1.2 2013/10/19 21:35:00 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeProceduralOperation(vNodeEngBase):
    def __init__(self,tagName='S88_op'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'operation (S88)')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xfdIDAT8\x8d\xa5\x92\xd1m\xc30\x0cD\x1f\x9d\xccQq\x13\xd1\xe8"\xcc&\
\xf2$\xf1&\x96\x07\t\xc0L\xe2~\xa4\x11\xa0\x04M\x9b\xfa>E\xdd\xe3\x91\x92\
\xc8p`\x8f\x04\xea\x06\xf5\x1fVC\x86O9B\xa5\x94\xf7\xed\xb5\x16".\xdb\xb1\
\xf1\xcc(\x0f\xa4R\n9\xe7\x1f\x00\x15\xd5\xa0\x03\xb8;\xee\x0e@D`fl\xdb\xf62\
\xc9\xf1N\x03P\xd5\xd61\xe7\x8c\xbb#"\x00\xb8;\x11\x01\xc0\xb2,=\xe0/RU\xce\
\xe73"\xc2\xe9tj\xe7\xc3\xbd\xf8\xca\xf8\xa8{\x92\x0e`f\xcc\xf3\xcc\xf5z\x05\
`\x1cGT\xb5\x01"\x82u]\x9f\xa0m\x84eYX\xd7\xb5-QU\xdb\xdc\xb5V"\x82R\n\xee\
\xde\x9au\x00\xb8-n\x9e\xe7\x161\xe7\xcc4M\xc0\xed\x95\xcc\xac\xbb\xf7\x04\
\x00H)\x91R\xea\xe653"\x02U\xedj\xf0\xfd\x95\xcd\xca\xd3\xa2~\x93\xaa\x12\
\xe1\x88\x0c\x07\xd2\xc7eS\x8d\xb7!\x11z\x03\xec\xd1\xb0\xcb\r|\x01\x15\xd9k\
\x89\xa4Z\xca\xaa\x00\x00\x00\x00IEND\xaeB`\x82' 
