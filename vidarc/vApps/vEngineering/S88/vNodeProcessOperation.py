#----------------------------------------------------------------------------
# Name:         vNodeProcessOperation.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060923
# CVS-ID:       $Id: vNodeProcessOperation.py,v 1.2 2013/10/19 21:35:00 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeProcessOperation(vNodeEngBase):
    def __init__(self,tagName='S88_PrcO'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'process operation (S88)')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x00\xa0IDAT(\x91\x95PA\x0e\xc3 \x0cs\xa6|\xaa\xd18\xac|\xab\xdaa\xea\xb7\
\xba\xcbP\xf3,vH\x151*\x18\xf5)\x18[vB9g\\\x01\xcd\x8fy\\\xbd\xbd7\x06\xb0\
\xbe\xd6\x11\xf5\xf2\\4)\x03\x90 \xce\xee\x9f\x1d\xc0t\x9fZ\xb6\x9b\xebL\xfa\
\x17\x87A\x82x\x8e\x04\xd1\xa4DDDg\x03\x97M\x1c\x12\xc4\x18MZ\xd5c\x9f\xca\
\x0f\xdf\xc4B\xca\xd33\x1a0\xb59}8v\xa8\xfaT\xcfS%\xaa\xa9\xe6M\xa9[\xc9\xaa\
\xdb\x0eef\xcfPJ=\x96\x01\xc4\x18\xfb\xb6\x9f^9gMz\xcd0\xae\x06\xf0\x05S\xf0\
D\x89\xc7\xc3\xf9\xaf\x00\x00\x00\x00IEND\xaeB`\x82' 
