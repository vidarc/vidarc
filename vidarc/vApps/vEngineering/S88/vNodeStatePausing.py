#----------------------------------------------------------------------------
# Name:         vNodeStatePausing.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060923
# CVS-ID:       $Id: vNodeStatePausing.py,v 1.2 2013/10/19 21:35:00 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeStatePausing(vNodeEngBase):
    def __init__(self,tagName='S88_paung'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'state pausing (S88)')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return False
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x0f\x08\x06\
\x00\x00\x00\xedsO/\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\x00\
\x01vIDAT(\x91\xc5\x93\xbfK\x02a\x1c\xc6?\xaf\x1a\x15\x0eN]\x8bp8\xdd(H\xa5\
\xdc\xe0`\xb5\x94\xee\x87\x7f\x80\xb3e\x8dR\xfd\x01\xd5\xffP\x81n\x19T\xb7\
\x88\xc9\r7\x98?6o\xd3\xe5\x10\xac\xe1\xc6\n\x8b\xb7!\xfaaA\x19\r=\xd3\x0b_\
\x9e\x87\x0f\xdf\xef\xf3\n\xe1\xf3\xf3\x17\xf9\xfe\xe4\x06\x84i\x9aRQ\x14\
\x16\x16\x97\xc4\xe7\xe1Y\xe5T\xd6\xebu\x00\xd2\xe94\xcb+\xab\xe2\xe4\xf8H\
\xb6Z-\x00\xb2\xd9,\xbe~\xbf\x8f\xae\xeb\x1c\x1e\xec\xcb\xa7\xc7\x91\xfc\x18\
`Y\x16\x8e\xe3\xd0h4h6\x9b\x00\x98\xa6\x89\xeb\xbaT\xabU\xba\xdd.\x01\x00]\
\xd7)\x97\xcb\xd4j5no\x86rN\x99\x7f\xa3\x89\xc7\xe3\x0c\x87\xc31\xb2T*\xc5h4\
z\xdfA8\x1c\xc6\xb2,4M#\x16\x8bQ\xbf\xaa\x8d\x91|\xa7\xc0\xebczfV\x00\\^\x9c\
K\xc30\xd8\xdb\xdd\x91\x9e\xe7M\x1e\xf0\xaa\xb5\xf5\xb4h^7d"\x91 \x18\x0c\
\x92\xcf\xe7\x7f\x17`^^\xc8L&C\xb1Xd\x12\x82\xb7\x1e<\xdc\xdf\xc9\xed\xad\
\x82\xcc\xe5r\x94J%vv\xf7\x84\xdf\xffs\xc9\x02\x00\xae\xeb\x92L&Q\x14\x85v\
\xbb\xcd\xc7+LD`\xdb6\x86aP\xa9T\xbe\x98\x1d\xc7\xa1\xd7\xeb\x8d\x99:\x9d\
\x0e\x83\xc1\xe0% \x12\x89`\xdb6\x1b\x9b\x05\xe1\x0fL\x8d\x99\xa3\xd1(RJB\
\xa1\x10\x9a\xa6\x01/\xbd\xf0<\x0fUUQU\x15\xf1\xef\x9f\xe9\x19\xa9h\x8b>\x1a\
/Z\xb1\x00\x00\x00\x00IEND\xaeB`\x82' 
