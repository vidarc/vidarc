#----------------------------------------------------------------------------
# Name:         vNodeStateComplete.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060923
# CVS-ID:       $Id: vNodeStateComplete.py,v 1.2 2013/10/19 21:35:00 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeStateComplete(vNodeEngBase):
    def __init__(self,tagName='S88_cmpl'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'state complete (S88)')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return False
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01&IDAT8\x8d\xa5\x931n\x83@\x10E\xff\xee\x8a\r\x05\x17pgw\xf8\x06t\x88\
{Pp\x01N\xe0{\xb8\xc1G0\x15%\x12B\t\x92qCR\xb8r\xc5\x15\x92\x08\xac\x04\x89\
\x9f"\nJ\xe2\xc4\x10g\xa4i\xfe\xcc{\xc5H#\x84T\xf8O\xc9k\xa0\xe7\xa7G\x16w\
\xb7\xbcJ\xf0p_\xd1\xf3<t]\xf7\x1e\x08\xa9&w\x1c\xc7\x9c\xcdf\xdc\xef\xf7\
\xfc\xc8&\xc3\xeb\xf5\x9a\xa6i2\xcb2~\xce\'\xc1\x9b\xcd\x86B\x08FQ\xc4\xef\
\xb3Q8I\x12*\xa5\x18\x04\xc1\x19<*8\x1c\x0e\xb4,\x8b\xb6m\xb3i\x9a\xcb\x82\
\xe3\xf1\xc8\xa2(\x86\xa5\xa6i\xb8\\.\xa9\xb5fUU?\xc2\x83 \x8ecj\xad\xa9\x94\
b\x9e\xe7\x14R!\x0cC\x02\xe0j\xb5\xfa\x15\x1e\x04\xa7\xd3\x89\x9e\xe7\x11\
\x00]\xd7\xe5n\xb7\xa3\x94\x92\x8b\xc5\x82m\xdb\x8e\x0b\x84T\xa8\xeb\x9a\x86\
a\x10\x00\xe7\xf39\x01p\xbb\xdd^\x84\xcf\x8e\xe8\xfb>\x01\x10\x00\x1d\xc7\
\x19\x85\xcf\x04Y\x96\x11\x00\xb5\xd6L\xd3\xf4\xef\x82\xbe\xef\xe9\xba.\xcb\
\xb2\x9c\x04\x0b\xa9 \x84\xfc\xfa\xce\xdd\xeb\x0b\r}#\xa6>\xd7\x1b"5\x8e"F\
\x87\xad\xf4\x00\x00\x00\x00IEND\xaeB`\x82' 
