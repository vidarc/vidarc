#----------------------------------------------------------------------------
# Name:         vNodeStateExceptionMaterial.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060923
# CVS-ID:       $Id: vNodeStateExceptionMaterial.py,v 1.2 2013/10/19 21:35:00 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeStateExceptionMaterial(vNodeEngBase):
    def __init__(self,tagName='S88_excem'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'state exception material (S88)')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return False
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\x83IDAT8\x8d\x9d\x931kSQ\x14\xc7\x7f7\xc9Gx7\xa5Kq+7/ \xd8tLr\xbf@\
\x97@#8u\xe8G\x88\xa3\xcd]:\t\x82KA\xe9\xd4\xbd\x888\x0b7v\xac"B\xeck2\xbaHy\
\xd7A4\xedR=\x0em\xda\xf7\xb8\x01K\x0f\x1c8\x07\xfe\xff\xdf\xe5\x1c\xceU\xaa\
R\xe5>!\x7f\xdf\x0bx*\xf71w;\xdb\xe2\x9f\xd8y\xeb\x04\x88RU\xaa\x14\x13|\xa4\
1\xa9\x91\x82\x00\xd1u]\x10x)\x9bmd\xee\xd8\x1d)\x8d\x90\xe8\xa4\xd0\xb9\xeb\
Y\x9f\x89\xdf\xb7\x80\xc7\xa4\xa64J\x9e\xa7\xe5\x1d\x98FQ\xe0\xe9>j\x8b|v\
\xf8\xd3\xd5\xc8<\x8f\x05K\xb4\xb7U\xcb_\xa1\xfc\x04\xad5\xcd\x95p\x17\x80\
\xbf\xadNW\x19\xfd|\xca\xd7o\x1a\xdb\x82\xb4\x96\xff\x1f\xe06\n(?\xc1\r\x9e\
\x93\xe8\x84\xf4"6/\x06\xbcs\xa51\xfc\xc7\xab\xdd\xf4\x1ff\xf8\xe9\x1d\x00`q\
\x83\xef7\x9dI\rK\x97\x19\x8d\x97\x86\xb3Z\xbc\xc8\n\xcc\xcf\x12\xb2\x93\x0c\
\xff\xea\x05\xc3\xe5IItV3h\xbdI\xc8\x9b\x11\xa0\x8a\x0c\x05\xb6\x00\x08y\xe0\
\xc1\xef\t\xb6\x05\xa3)\xcc\xaa\x9a\xb5\xf55\xb2\x93\xc7|:\xfe\xc1\xf9l\xef\
\xc6\x18\xf2\x80R\n\xa5\xebciwwJ\xd4\x90\x07\x12\x9d\x10\xf2&G\x1fv\x15@\x92\
|\x89t\x00\xd7\xa7\xeaD^#n\x03\xe9\xd8\x8e\xf46{\xa2\xeb\xe3\xe8?,\xca\xda\
\x1c\xd4?\xd0\xfcYn\xc3\xaf&o\x0ew\x15\xbc\x8d_[\x10\xff\x00\xd4\x01\x98_G\
\xd4k\xb8\x00\x00\x00\x00IEND\xaeB`\x82' 
