#----------------------------------------------------------------------------
# Name:         vNodeProcessStage.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060923
# CVS-ID:       $Id: vNodeProcessStage.py,v 1.2 2013/10/19 21:35:00 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeProcessStage(vNodeEngBase):
    def __init__(self,tagName='S88_PrcS'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'process stage (S88)')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x00\xa3IDAT(\x91\x95PA\x0e\xc20\x0csP>\xb5\x88\x1eX\xbf5q@\xfb\xd6\xb8P-\
\xcf*\x87N\xa6*Z)>5\xa9\x1d;\x91\x9c3\xfe\x81\xcc\xb7y\x9c\xbd=7\x05\xb0>\
\xd6\x11\xf6r_<\xb9\x02\xb0`\xec\xee\xaf\x1d\xc0t\x9d\xced\x17\xf2\n\xf5\'\
\x0e\x81\x05\xa3\x8f\x05\xf3\xe4"""\xdf\x02\xad\x93\x10\x16\xact<y\x13O\xf9\
\xaa?\xb8\tMx}\xc5\t\n\x9b\xce\x9e\xfc#h\xf2\xd4%\x17cS!\xed\x11Y\xe6\x9c\
\xcb\xe0C&\xddH\x00\xb8@\xed\xd9\x13\xd0\xa1\xb6U\x001\xc6\x8e\xac\xb5\xad\
\xc7\x8c\n\xc6\xd9\x00\xde\xa5PL\xe0\x8b(%\n\x00\x00\x00\x00IEND\xaeB`\x82' 
