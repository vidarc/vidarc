#----------------------------------------------------------------------------
# Name:         vNodeStateStopping.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060923
# CVS-ID:       $Id: vNodeStateStopping.py,v 1.2 2013/10/19 21:35:00 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeStateStopping(vNodeEngBase):
    def __init__(self,tagName='S88_stpng'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'state stopping (S88)')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return False
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x9bIDAT8\x8d\xedSA\n\xc3 \x10\x1c5\x1f07\xed\xcd\x17\xe55\xfe\xc0\
\xcf\xf8\x0c?\xe0\x17\xc4\x9b\xd8\xde<\x04<lOI\xd3\x92B \x85\\:\xb00,3\xb3\
\xb0\xec2\xc6\x05\xce\x80\x9fr\xff"`X\xc8\xe3^i\x9a\xa6C&\xef=\x94\xbe\xb1\
\xb7\x80\xde;B\x08p\xceAJ\xb9kl\xad\xc1Z\x8by\x9e_M\xc6\x05\x18\x17(\xa5\x10\
\x00\xca9\xd3\xd2\xfb\xacZ+\x01\xa0\x94\xd2\xaa\xb9~\x89\xff\x80\xcd\x1d,0\
\xc6`\x10\x9c\xf6\xc4Z\xeb\xef\x01\xe38"\xc6xh\xaaRj\xe5\xec\xf2o|\x02\xf3\
\xfc*\xf2\xb2\xa1\x9b-\x00\x00\x00\x00IEND\xaeB`\x82' 
