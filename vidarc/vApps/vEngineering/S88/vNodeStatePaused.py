#----------------------------------------------------------------------------
# Name:         vNodeStatePaused.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060923
# CVS-ID:       $Id: vNodeStatePaused.py,v 1.2 2013/10/19 21:35:00 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeStatePaused(vNodeEngBase):
    def __init__(self,tagName='S88_paused'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'state paused (S88)')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return False
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x0f\x08\x06\
\x00\x00\x00\xedsO/\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\x00\
\x01.IDAT(\x91\xc5\x93?O\xc2`\x10\x87\x9f\xb7/IW\x16\xebBB\x98\x18a\x00\xbaj\
\xd5\x8d\x90N\x1d\x08\x0b\xdf\x00\x95\x99\x7f#S'>@u\xb7\xb2\xb0\xd5N\xddH\
\xf8\x10,\xca'\x80\xa4\x9c\x93&\x88\x11\x08\x83\x97\xdc\xf2\xfb\xe5\x9e\xdc]\
\xee\x9424\xe7\x84qV5\xa0f\xb3\x99X\x96E\xa5ZS?\xcd\xd7\xf0E\xe28\x06\xa0^\
\xafss{\xa7\x9e\x9f\x02\x99\xcf\xe7\x004\x9bM\x98L&b\x9a\xa6\xf8\xbe/\xdb\
\xedV\x94\xa1\xf9\xcan\xb7+\x80\x002\x1e\x8fE\x19\x9aV\xab\xf5\xad\x05A \x06\
\xc0z\xbd\xa6\xd3\xe9\xe0\xba.\xab\x8fw9e\x84\x9d\x1dL\xa7S\xca\xe52\xf1[t4d\
o\x89\xcb\xe5\x12\xc7q\x18\x0e\xfa\x92\xa6\xe9A@\xe671MSz\xbd\x1e\xd9l\xf6\
\xf4\x0e\x00\xb4\xd6\x8cF#\xda\xed\xf6\xe9\x80\\.G\x14E\xf4\x07C\xa5\xf5\xe1\
#\xdb\x014\x1a\r\x16\x8b\x05W\xd7\xce\xdeM\xfc\t0M\x13\xdf\xf7\t\xc3\x90\x0b\
\xeb\xf2\xe8b\x80L\xa1P I\x12*\xd5\x9a\xba\x7fx\xdc1K\xa5\x12\x9e\xe7\x01P,\
\x16\x01\xb0m\x9b\xcdf\x03@>\x9fG\xfd\xfb3}\x02\x8ei`\x00\x1b\xb5L\x14\x00\
\x00\x00\x00IEND\xaeB`\x82" 
