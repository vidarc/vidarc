#----------------------------------------------------------------------------
# Name:         vNodeEM.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060923
# CVS-ID:       $Id: vNodeEM.py,v 1.3 2013/10/20 00:34:39 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeEM(vNodeEngBase):
    def __init__(self,tagName='S88_EM'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'equipment module (S88)')
    # ---------------------------------------------------------
    # specific
    def GetProcessNodeInc(self):
        return [
            {'lTag':['S88_ET','S88_ST','S88_AR','S88_CL','S88_UT','S88_EM','S88_CM',
                    'Base','Fct','Prc','drawNode','itfc','api',
                    'account','account_task','account_subtask','Bug',
                    'DocRef','parameters','Prop','Test','Tool','ToDo',],
                    'level':1,'level_max':8,},
            ]
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x018IDAT8\x8d\xa5S\xb1J\xc4@\x10}\x93\x04\raS\t\x92\xf4{\xa8\x08\xe1\
\x8a\xc0\x15\xd1\x1f\xb1?R\xf8\t\xd6\xc2\xfd\x80\x85\x08\xf6\xfe\x87\x16\x81\
\x14G\xce\xee\x82\x9d\xdc\xa2vY\x0e\x94\xc0Z\x1c\xbb&wr\xae\xfa`agv\xe7\xbdy\
\xb3,\xc8q\xf1\x9b\xe5\xfb\x13\xd5\x8d\x1d\xfc\x13D\x8eku\x91\x05\x85b\xac\
\x84\xe7=\xa0m3\x00\x80x9\'\xcfVI.G\xc4y\xa2\xc20D\xd3<B\x88\x1c\x00~\xb6\
\xc0\x82B\xb1\xa0P\x00L\x91V\xb7#`%\x18+MQ\xd34="+\x0bQt\x8d(J\x94\xb1#Ssf\
\xf5\nB\xe4\x98V\x15\xd5\xf5\x8d!\xdc\xe8`\x98|)\x00\xc0\xb4\xaah]M\x17\x0e\
\x06\xcf\x10\xa2Pr9""\xc7\x05\x0bVA\xb4\x7f\xa5\xb4W\x00&\xaf\xf7\x9c\x8f\r\
\x99\x94)\xea\xa7\xdb\x15\xc10ITw0R\xa6\xe0|\x0c!rC\xa6\xb1\xbbs\xa9\xde?.L\
\x8e|\x7f\xa2\x8e\x0e\xef6|\xc7q\x8c\xd9l\x0f\xafo\xc7\xbd\xbc\xe7\xdd\xa3mO\
LL\xe4\xb88\xcd2\x05\x00\xf3\xf9Y\xcf\xebw\x1d\xac\xc3\x03\x80\xc5\xe2\x00B\
\xe4\xd0~\xa5\xec\xfb\xdd\x86\xad\x7f\xa1;\xc4?\x11\xd8\xe0\x13\tL\x81E\x14{\
\x12\xb1\x00\x00\x00\x00IEND\xaeB`\x82' 
