#----------------------------------------------------------------------------
# Name:         vNodeUnit.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060923
# CVS-ID:       $Id: vNodeUnit.py,v 1.3 2013/10/20 00:34:39 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeUnit(vNodeEngBase):
    def __init__(self,tagName='S88_UT'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'unit (S88)')
    # ---------------------------------------------------------
    # specific
    def GetProcessNodeInc(self):
        return [
            {'lTag':['S88_ET','S88_ST','S88_AR','S88_CL','S88_UT','S88_EM','S88_CM',
                    'Base','Fct','Prc','drawNode','itfc','api',
                    'account','account_task','account_subtask','Bug',
                    'DocRef','parameters','Prop','Test','Tool','ToDo',],
                    'level':1,'level_max':8,},
            ]
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xecIDAT8\x8d\xa5\x93KkSa\x10\x86\x9f\xef;\xa96!\xe4j\xd0\xa4h\x88\
\x10MJ\xad\x97.\x8a\x8a-HQ\xa4`\x17\xa5\x8b\xee\xf4\x07\xf8\x0b\xdc\x8b?\xc0\
Mw\xe2B\x10\x11ZAE(\x05\x11\x04\xf1r\x8cX\x8dhm\xc5\x86T\x9a&\'\xcd\xc9\xc9\
\xcds>\x17V!\xd6\x88\x92\xd9\xce\xcc\xc3;\xef\xcc\x08!5\xba\t\xd9U7\xe0\xfa\
\x97\xa2>\xff35s\xe51\xf5J\x99KW\xcf\x937\x86\xc4\xcf\x9c\xf8}\x84@\xf0\x8d\
\x8a\'\nLM\xe7\xa8l\x16)\x18\r\x86#u\xfa"\x1e\xcc\x95\x8fX\x86\xc1\xc5\x99\
\x9b\xbf\x00m\n&&o\xa83\xe3\x05\xf6\'b\xc4c\xfdd>\x14\xa8TmV>-\xf3\xf6\xc9=\
\xd2\xae"\x0f\x9f\x07:\x8f\x90\xeco\x91:\xb8\x8fX\xc4\x8b\xdf\xd7\x8b\xdd\
\xb2)\x1a5,\xdb\x8f\x11\x9b\xe4\xf6\x92N\xc9\xb5\xd1\x19`V*\xcc\xcf?"\x14\
\x0eaV\xeb|^]\xa3lX\x18\xe5*\xe5J\x15\xc7\xfeFj\xb0\x97\x17z\x07@z`\x80\xb3\
\xa3GI\xec\xf5\xf1~y\x83\x85\xa79\x9aM\x87\xc5\xa5\x12\x96\xd5\xc4(\x16\x11"\
\xdb\xa6\xa0m\x8d?\x9cQ\x80\xc2A\xe08\xe0\xd8\n\xe5\x08\x94r\xb6rt\x06|Y]E\n\
\x10B QH!\x90\x9aDJ\x85\x10\xd0\xb0J4k\x7f\xf1\xe0\xfe\\\x8el\xf6\x1a\xc3C)4\
\t\xfa\xbb\xaf\xd86\x98\xa6EqMg\xd3\x84\xa9\x0b#\xdc\xb9\xd5A\xc1\xa2~Y\x9c<\
\xec\xa1Q+16v\x8a\xf1s\xa79\x90>F\x8f\'\xcc\xf4\xc4\t\x8e$5\xc2\xe1]\xdc\x9d\
\x9bU\x83\x87\xd6\xd56\x05\x00RJb!\xc5\x83\xd9\xeb\xe4\xf3\xeb8N\x8b\xe3\xc9\
8\x9a\xb6\x93ht7\x8df\x8d\x1d\xcd\x1e\xd4\x96\x1d\xdb.qtdA\xc5\xa2{\xf0z\xbd\
\x08!\xd0\\\x1an\xb7\x9bW\xfak\x82\xc1\x00>\xbf\x8fL&\xc0K=(\xfe\x08\xf8\xdf\
\xe8\xfa\x1b\xbf\x03YM\xbcI\xed\x84\x19{\x00\x00\x00\x00IEND\xaeB`\x82' 
