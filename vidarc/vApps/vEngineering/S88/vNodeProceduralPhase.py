#----------------------------------------------------------------------------
# Name:         vNodeProceduralPhase.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060923
# CVS-ID:       $Id: vNodeProceduralPhase.py,v 1.2 2013/10/19 21:35:00 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeProceduralPhase(vNodeEngBase):
    def __init__(self,tagName='S88_ph'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'operation (S88)')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xfaIDAT8\x8d\xa5\x93\xddm\xc30\x0c\x84?\xd9\x1e \x13\xd4\xf4$a&\x89\
61\x83\xceQ\xc0\x9aD\xcc\x06Y\xa0\x806q\x1fZ\x19.\xaa\xf4/\xf7$\x1cy\xc7\xa3\
 \x85\xd0\xf5<\x82\x00\xbe\x82\xffO\xdc=\x87\x01\x1c\xb3\xbf\x8b\xdd\x1d\x91\
\x97u\xa8\xc4<\xcf\xcd\xc6\xcb\xe5\xf2M\xbd0\x00\x94R\x98\xa6\x89R\n\xaa\xba\
\x95\xed#\x9a\x99!"\x9c\xcf\xe7/\x16\x1d\x80\x88\x10c\x04@U\xc99\xe3\xee\x98\
\x19\xa5\x14\xea\x90\x16\x86\x16y\xbd^\xa9\xc6\x15\xee\x8e\xbb\x03\x90s\xbeo\
P\x1bk\xec:\xb9\xaeff\xa4\x94\xee\x1b\xa8*\xaa\x8a\x880\x8e\xe3v\x89{\xec\
\xd7i\xaep<\x1e[t\x13=\xa8\x89\x14n\xb7\xdb\x16\xf9t:q8\x1c\x00H)m\xe7\xda\
\x03l\\\x00[\xcd \xc6\xf8)\xda>E\xbd\xd4=\x96eAD\xde\x9f\xb2\xaa\xfd:\xf2\
\x1e\xaaJ\x08]\xcf\xf8\xf4\xba\x8a\x94\x1f\x05-\x84G\x7fc\xf7\x90\x1ax\x03\'\
\x07d\x05fp\xce\xcf\x00\x00\x00\x00IEND\xaeB`\x82' 
