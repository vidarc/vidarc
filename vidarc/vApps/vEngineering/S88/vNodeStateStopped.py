#----------------------------------------------------------------------------
# Name:         vNodeStateStopped.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060923
# CVS-ID:       $Id: vNodeStateStopped.py,v 1.2 2013/10/19 21:35:00 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeStateStopped(vNodeEngBase):
    def __init__(self,tagName='S88_stped'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'state stopped (S88)')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return False
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00ZIDAT8\x8dcddbf\xa0\x040Q\xa4\x9b\x1a\x06\xb0 s\x9e?{\xfa\xff\xe5\
\xcb\x97x5\x88\x89\x891HI\xcb0\xc2\x05\x18\x99\x98\xe1\xb8\xa1\xa1\xe1?\x03\
\x03\x03^\\QQ\xf1\x1fY\xcf\xc0\x87\xc1\xa8\x01\xd4NH\x86\x86\x86\x0cIIIx5\
\x98\x98\x98\xa0\xf0\x19\x87~f\x02\x00\x10\x10\x16\x84gm\xb4\x8c\x00\x00\x00\
\x00IEND\xaeB`\x82' 
