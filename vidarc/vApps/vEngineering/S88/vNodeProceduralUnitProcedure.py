#----------------------------------------------------------------------------
# Name:         vNodeProceduralUnitProcedure.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060923
# CVS-ID:       $Id: vNodeProceduralUnitProcedure.py,v 1.2 2013/10/19 21:35:00 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeProceduralUnitProcedure(vNodeEngBase):
    def __init__(self,tagName='S88_uproc'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'unit procedure (S88)')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xf8IDAT8\x8d\xad\x93\xe1M\xc30\x14\x84?\xa7\xcc\x01\xaf\x83$~\x89:F\
\x91\x929*\x14\x07u\x11\xab\x1d\xa3$^\x04\xc9a\x91\xf0\x03%\xb4\x90\x02U\xb8\
_~\xf7|\xa7;K6&Y\xb1\x04\xc9"\xf5\x7f\x18\x18p\x83j\xb8Y\x18\xa3\xd0\xbf\x1d\
\xcc\xb7\x04\xaa\x8as\x0eU\x9d\x15\x8e{\x91\x88\xcd^\x86d$C\xf8La\xad\xa5\
\xae\xebi\x0e!\xb0)68\xe7p\xce\xe1\xbd\x9fv\xb3\t\xce\xcf\xe7\xb3\xb5\x16\
\x80>\xf6\xd7\r\xae\xa1\xedZ\xf2<\xa7\xc8\x0b\x8a\xbc\x98\xf8\xbb\xbf\x1a\
\x8c\xa2\xccf\xa4Y\xca\xa9=\xfd\x9e\xe0\xfc]\x00vO;\xd2,\xbd\xe0\x92\xaf\x97\
\xcb\xb2\xa4i\x1a\xaa\xaaBD\x10\x91\xa9\xc2\x1c\xa6\n\xd6Z\xba\xae\xc3{O\x8c\
\x11\x11AUY\xcb\x9a\xfd\xf3\x1e\x80\xe3\xe1\xc8\xf6q{a` \x0c\xaa\xee\xa7&\
\xb3\x10\x11b,1&Y\xf1p\xff:\x88\xc4\x9bMb\x94\x0f\x83%X\xfc\x99\xde\x01\xee\
\xa8P\xdaF\xa4\x9e\xde\x00\x00\x00\x00IEND\xaeB`\x82' 
