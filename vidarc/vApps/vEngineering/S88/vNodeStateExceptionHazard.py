#----------------------------------------------------------------------------
# Name:         vNodeStateExceptionHazard.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060923
# CVS-ID:       $Id: vNodeStateExceptionHazard.py,v 1.2 2013/10/19 21:35:00 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeStateExceptionHazard(vNodeEngBase):
    def __init__(self,tagName='S88_exceh'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'state exception hazard (S88)')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return False
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\x85IDAT8\x8d\x9d\x911k\xdc@\x10\x85\xbf\xd9K\x93"\x8d\xd1\x1e$\xf9\
\x01\xf1F\x86\x80I\xab\x93 \xb5\x9b\x83\xbb\xc2M\x1a\xe32\x7f\xc0\xd8Q\x08\
\xfa\x13I\xe7\xee\x8a\x10\xe2\xde\x89\x90\xda@\x1a\xdb\xb2\rn\x03F\xd7%\xa5O\
\xe3B\x96l\xe1\xbb\xc3xaX\xde\xbe\x9dy3oDL\x8f\xc7\x1c\xad\x0e\x15R\xccc\x92\
\xc3\xc1\x96\xa6\x9b\x11\x00F\xabC\xd5*P\xadfmx\xde\x8a\xae\xbam\xad\x95v;\
\x9cV3\xbd,3\x8e\x9fZ\x9c\x7f\x89\x11\xf3N\xc4\xe4rW!\x08\x03\xa0V\x10\xf3Y \
n9\xe7;\xac\xb5L.\xea{\xe1\x08\xa7\xc5\xa64\x1dDo\xe3\x0e\xe7Y\x0f\x88\xc8\
\xb3D\x96z\x10\xae\x07\xaa\x7fb\xa2\xe8U\xe7}Z\xae\x91g\x89\x00\x8b;\xf8\xb8\
=\xd3__\xd3\x1a\xfc?\xebpe\xe9\xdf\x021=nV\xa9\x0f\t\xe7;u\xfeD\x9b\xbc\'\
\xf3\xd4\x87\xa3a\x07\x17\'\x05\xc5q\xd1b\xf7z\x82\xb5;\x9ag\x89\xcc-\x00\
\xf0\xe1\xfd\x15\xe1\xcb\x03\xf8\t\x9f\xca\xbb{\xa8=(N\xc6@2\xbf\x03\x80\xf0\
\xef\x01\xfc\x06^\x00\xcf\xba\\Y\xfaL\xa7o\x04X\\\x00\xea\xe4\xf1\xbe%=\x07(\
\x17\xfdJ\x15\xa2\x8eQ\xb6o5\xdeh\x0cs\xf7\x8c\xb4\xfdQk\xa2\x88\xe9\x11\x0c\
v\xd4\xb3G\xf7J7\xb3\x06\xe1\xde\\.\xcf\x12\xb9Ya\xac\xfa\x05\x8d7\xd0A4\xd0\
\xe1h\xa8\xb6\x7ft\xab\xb2$Z\x0f\xc6\xfb\x96\xd9\xf3\x00\xfe\xad\xf1\xfd["\
\xf0c\xa9=\xcd\xb9\x06\xbb\xd9\xb2"\xf3\xf4\xef3\x00\x00\x00\x00IEND\xaeB`\
\x82' 
