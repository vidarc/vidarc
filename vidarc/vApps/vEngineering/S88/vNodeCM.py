#----------------------------------------------------------------------------
# Name:         vNodeCM.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060923
# CVS-ID:       $Id: vNodeCM.py,v 1.3 2013/10/20 00:34:39 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeCM(vNodeEngBase):
    def __init__(self,tagName='S88_CM'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'control module (S88)')
    # ---------------------------------------------------------
    # specific
    def GetProcessNodeInc(self):
        return [
            {'lTag':['S88_ET','S88_ST','S88_AR','S88_CL','S88_UT','S88_EM','S88_CM',
                    'Base','Fct','Prc','drawNode','itfc','api',
                    'account','account_task','account_subtask','Bug',
                    'DocRef','parameters','Prop','Test','Tool','ToDo',],
                    'level':1,'level_max':8,},
            ]
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xa4IDAT8\x8d\xedR1\n\xc2@\x10\x9c\xbb\xd8\xb9\xa9\xbd\xab\xbd\xb4j\
\x11\xf0?B\xb0\xf0=B\xc4\xda\xc7\x08\xf7\x84<\xe0\xd2\x8f\xf5Y\x04\x05\xf5\
\x0e\x02iR\xb8\xb0\xb0\xcc\xec\x0c\xbb\xcb*\xa5\x0bL\t=I=\x0b\x83E\x8e0\xabs\
|\xd5d\r>\xf6j\xd4\x04\xb2\xbc\xbf\x85\xc6\\\xb2\\\xd6\xc0\xb9\x06n}\x88d\r\
\x00\x08\xfdI\x89x\xec\xb6\x9b(\xe2\xc7\xad \xe2\xe1\xdc\xd0<\x08o\x00$\xd5\
\x9a>\xa21\x06d\x8d\xae\xbb"\x84#H\x82d\xd2\x00J\x17\x1fYUM,K\x1f\xbfqk\xdbh\
m\xfb\x83\xab\xff\'\xce\xc0\xe0\t\x9e\x81-\x16\xb4\xee\x98\xee\x00\x00\x00\
\x00IEND\xaeB`\x82' 
