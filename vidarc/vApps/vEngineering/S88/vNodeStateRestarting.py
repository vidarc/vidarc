#----------------------------------------------------------------------------
# Name:         vNodeStateRestarting.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060923
# CVS-ID:       $Id: vNodeStateRestarting.py,v 1.2 2013/10/19 21:35:00 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeStateRestarting(vNodeEngBase):
    def __init__(self,tagName='S88_restng'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'state restarting (S88)')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return False
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x0f\x08\x06\
\x00\x00\x00\xedsO/\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\x00\
\x01EIDAT(\x91\xc5\x93\xb1N\xc2`\x14\x85\xbf\xbf6E\xebHD'\xeb\xc6^\x1a\x1e\
\x80\x81\x11\x166F6V\x125Q7ub\xaf\xa6\xd0U]\xbb\xab\x8b\xf5\x05\x88\x125Bu\
\x80\xa8\x10F\xaa\xcb\xefd\xb5\xd1D\x08&\x9e\xed\xdc\xdc\xf3\xe5$7W\x08e\x8e\
Y\xa4\xcc\x94\xfe\x13@\xb3\xe1H+cJ+c\xca\xbb\xdb\x1b\xd9\xed\xdcG\xfe\xf0\
\xc0\x96\x00\x9b\x1b\xeb\xf2\xe5\xf9I\xfe\x08\xe8\xf7\xfb\x14\x8bE4Mc<\x1e\
\x13\x86!B\x08J\xa5\x12\xbd^\x0f\x00\xcf\xf3\xc8f\xb3\x9c\x9f\x9d~\x83(\x00\
\xba\xae\xa3\xaaj4TU\x15]\xd7c\x8b\xae\xebR\xab\xd5\xd8\xd9\xde\x92o\xaf\xa1\
\x8c\x01&Q:\x9d\xc6\xf7}F\xa3\x11\xb9\\\x8e\xa0\xdb\x91\x00\xeao\xc1\xafZ\
\xd0\x17\x05\xc0\xc9\xf1\x914M\x93K\xffBN\x05\x00h__\xc9r\xb9L\xb5Z\xc5\xb2\
\xac\xe9\xce\xe86\x1b\xb2P(P\xaf\xd7\xd9\xdd\xdb\x17Zb^L\xdc\xa0R\xa9\xa0(\n\
\xbe\xef\x93Z^\x11\x1fs\x05\xc0q\x1cZ\xad\xd6g\xcdv\x1b\xdb\xb6#\x9fH$\xc8\
\xe7\xf3x\x9e\x17\x0b\x03\x88\xe1p(\x07\x83\x01\x00\x86a \x84 \x08\x02\x00\
\x92\xc9$K\xa9e\xf1\xf8\x10\xc8Uc-\x16\x8c\x00\xff\xfeL\xefL\x8erz~^\xe4\x93\
\x00\x00\x00\x00IEND\xaeB`\x82" 
