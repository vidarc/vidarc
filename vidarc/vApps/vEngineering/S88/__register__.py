#----------------------------------------------------------------------------
# Name:         __register__.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: __register__.py,v 1.11 2015/03/01 08:46:02 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust

#from vidarc.vApps.vEngineering.vNodeDocuments import vNodeDocuments
#from vidarc.vApps.vEngineering.vNodeDoc import vNodeDoc

if vcCust.is2Import(__name__,'__REG_NODE__',True):
    from vidarc.vApps.vEngineering.S88.vNodePhysical import vNodePhysical
    from vidarc.vApps.vEngineering.S88.vNodeEnterprise import vNodeEnterprise
    from vidarc.vApps.vEngineering.S88.vNodeSite import vNodeSite
    from vidarc.vApps.vEngineering.S88.vNodeArea import vNodeArea
    from vidarc.vApps.vEngineering.S88.vNodeCell import vNodeCell
    from vidarc.vApps.vEngineering.S88.vNodeUnit import vNodeUnit
    from vidarc.vApps.vEngineering.S88.vNodeEM import vNodeEM
    from vidarc.vApps.vEngineering.S88.vNodeCM import vNodeCM
    
    from vidarc.vApps.vEngineering.S88.vNodeProcessModel import vNodeProcessModel
    from vidarc.vApps.vEngineering.S88.vNodeProcessStage import vNodeProcessStage
    from vidarc.vApps.vEngineering.S88.vNodeProcessOperation import vNodeProcessOperation
    from vidarc.vApps.vEngineering.S88.vNodeProcessAction import vNodeProcessAction
    
    from vidarc.vApps.vEngineering.S88.vNodeProceduralModel import vNodeProceduralModel
    from vidarc.vApps.vEngineering.S88.vNodeProceduralUnitProcedure import vNodeProceduralUnitProcedure
    from vidarc.vApps.vEngineering.S88.vNodeProceduralProcedure import vNodeProceduralProcedure
    from vidarc.vApps.vEngineering.S88.vNodeProceduralOperation import vNodeProceduralOperation
    from vidarc.vApps.vEngineering.S88.vNodeProceduralPhase import vNodeProceduralPhase
    from vidarc.vApps.vEngineering.S88.vNodeStateAborted import vNodeStateAborted
    from vidarc.vApps.vEngineering.S88.vNodeStateAborting import vNodeStateAborting
    from vidarc.vApps.vEngineering.S88.vNodeStateComplete import vNodeStateComplete
    from vidarc.vApps.vEngineering.S88.vNodeStateExceptionHazard import vNodeStateExceptionHazard
    from vidarc.vApps.vEngineering.S88.vNodeStateExceptionMaterial import vNodeStateExceptionMaterial
    from vidarc.vApps.vEngineering.S88.vNodeStateExceptionProduction import vNodeStateExceptionProduction
    from vidarc.vApps.vEngineering.S88.vNodeStateHeld import vNodeStateHeld
    from vidarc.vApps.vEngineering.S88.vNodeStateHolding import vNodeStateHolding
    from vidarc.vApps.vEngineering.S88.vNodeStateIdle import vNodeStateIdle
    from vidarc.vApps.vEngineering.S88.vNodeStatePaused import vNodeStatePaused
    from vidarc.vApps.vEngineering.S88.vNodeStatePausing import vNodeStatePausing
    from vidarc.vApps.vEngineering.S88.vNodeStateRestarting import vNodeStateRestarting
    from vidarc.vApps.vEngineering.S88.vNodeStateRunning import vNodeStateRunning
    from vidarc.vApps.vEngineering.S88.vNodeStateStopped import vNodeStateStopped
    from vidarc.vApps.vEngineering.S88.vNodeStateStopping import vNodeStateStopping
    
    import vidarc.vApps.vEngineering.__link__
    
def RegisterNodes(doc,oRoot=None):
    try:
        if vcCust.is2Import(__name__,'__REG_NODE__',True):
            oBase=vNodePhysical()
            doc.RegisterNode(oBase,False)
            for o in [vNodeEnterprise(),vNodeSite(),vNodeArea(),vNodeCell(),
                        vNodeUnit(),vNodeEM(),vNodeCM()]:
                doc.RegisterNode(o,False)
            lSig=['Input','Output','InOut','Param','hw','sw','Mod',
                    'drawTiming','drawNode','DocRef',
                    'Base','Obj','Cps','Struct','Dsg','Prd','Res','Sec',
                    'Prop','Doc','dataCollector','itfc','api','parameters',
                    'Fct','Prc','Bug','Test','Tool','ToDo',]
            vidarc.vApps.vEngineering.__link__.LinkNodes(doc,[
                    ('S88_phy',['S88_ET','S88_ST','S88_AR','S88_CL','S88_UT',
                            'S88_EM','S88_CM','Documents',]+lSig),
                    ('S88_ET',['S88_ST','S88_AR','S88_CL','S88_UT','S88_EM',
                            'S88_CM','Documents']+lSig),
                    ('S88_ST',['S88_AR','S88_CL','S88_UT','S88_EM','S88_CM',
                            'Documents']+lSig),
                    ('S88_AR',['S88_CL','S88_UT','S88_EM','S88_CM','Documents']+
                            lSig),
                    ('S88_CL',['S88_CL','S88_UT','S88_EM','S88_CM','Documents']+
                            lSig),
                    ('S88_UT',['S88_UT','S88_EM','S88_CM','Documents']+lSig),
                    ('S88_EM',['S88_EM','S88_CM','Documents']+lSig),
                    ('S88_CM',['Documents']+lSig),
                ])
            if oRoot is not None:
                doc.LinkRegisteredNode(oRoot,oBase)
            oBase=vNodeProcessModel()
            doc.RegisterNode(oBase,False)
            for o in [vNodeProcessStage(),vNodeProcessOperation(),vNodeProcessAction()]:
                doc.RegisterNode(o,False)
            lSig=['Input','Output','InOut','Param','sw','Mod','Base','Prop','Doc',
                    'itfc','api','parameters']
            vidarc.vApps.vEngineering.__link__.LinkNodes(doc,[
                    ('S88_Prc',['S88_PrcS','S88_PrcO','S88_PrcA',
                            'Documents'] + lSig),
                    ('S88_PrcS',['S88_PrcS','S88_PrcO','S88_PrcA',
                            'Documents'] + lSig),
                    ('S88_PrcO',['S88_PrcO','S88_PrcA','Documents'] + lSig),
                    ('S88_PrcA',['S88_PrcA','Documents'] + lSig),
                ])
            if oRoot is not None:
                doc.LinkRegisteredNode(oRoot,oBase)
            oBase=vNodeProceduralModel()
            doc.RegisterNode(oBase,False)
            for o in [vNodeProceduralProcedure(),vNodeProceduralUnitProcedure(),
                    vNodeProceduralOperation(),vNodeProceduralPhase(),
                    vNodeStateAborted(),vNodeStateAborting(),vNodeStateComplete(),
                    vNodeStateExceptionHazard(),vNodeStateExceptionMaterial(),vNodeStateExceptionProduction(),
                    vNodeStateHeld(),vNodeStateHolding(),vNodeStateIdle(),
                    vNodeStatePaused(),vNodeStatePausing(),vNodeStateRestarting(),
                    vNodeStateRunning(),vNodeStateStopped(),
                    vNodeStateStopping()]:
                doc.RegisterNode(o,False)
            lState=['S88_abed','S88_abng','S88_cmpl','S88_held','S88_holng',
                    'S88_idle','S88_paused','S88_paung','S88_restng','S88_stped',
                    'S88_stpng','S88_restng','S88_rng',
                    'S88_exceh','S88_excem','S88_excep']
            lSig=['Input','Output','InOut','Param','sw','Base','Prop','Doc']
            lTypes=['t_array','t_class','t_list','t_struct',
                't_bool','t_int','t_long','t_float','t_string']
            vidarc.vApps.vEngineering.__link__.LinkNodes(doc,[
                    ('S88_procModel',['S88_uproc','S88_proc','S88_op',
                            'S88_ph','Documents']+lSig),
                    ('S88_uproc',['S88_proc','S88_op','S88_ph',
                            'Documents'] + lState + lSig + lTypes),
                    ('S88_proc',['S88_op','S88_ph',
                            'Documents'] + lState + lSig + lTypes),
                    ('S88_op',['S88_ph','Documents'] + lState + lSig + lTypes),
                    ('S88_ph',['Documents'] + lState + lSig + lTypes),
                ])
            if oRoot is not None:
                doc.LinkRegisteredNode(oRoot,oBase)
    except:
        vtLog.vtLngTB(__name__)
