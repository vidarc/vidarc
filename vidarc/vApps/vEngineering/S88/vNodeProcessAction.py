#----------------------------------------------------------------------------
# Name:         vNodeProcessAction.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060923
# CVS-ID:       $Id: vNodeProcessAction.py,v 1.2 2013/10/19 21:35:00 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeProcessAction(vNodeEngBase):
    def __init__(self,tagName='S88_PrcA'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'process action (S88)')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x00\xa6IDAT(\x91\x95RA\x0e\x830\x0c\xb3Q?\xd5\x88\x1e\x06\xdfB;L|\x8b]V\x91\
gu\x87NQV@\xeb|Kd\xc7\xaeU\x96R\xf0\x0f8\xdd\xa6~\xf6\xf6\xdc\x02\x80\xf5\
\xb1\xf6\xb0\x97\xfb\xa2Y\x03\x00Ib\xdb\xfd\xb5\x03\x88c\xbc\x92\r\xc6\xab\
\xd4\x9f\xf8\x08$\x89\xf9H\x12\xcdJ\x92\xe4Q\x10|\x12\x83$\xa9\x1b\xcd\x1a\
\xc7H\xd2\xca\x0cF\xf2\xb9\xfdK\xaa\x8ff\xfd\x8at\x84\xb1\x1b\xf3\xe1\x98\
\xc7\xc6z\xdbw\x08 \x80m\x89\xfe\xb6O\x05^G\xf2\xec\x06\xe7\x82\xe6\x83\xf91\
\x00\x98\xe7\xf9Tvn^J\xb1\xcaz\x05\xfdl\x00o\x8c\xafH=\x0283W\x00\x00\x00\
\x00IEND\xaeB`\x82' 
