#----------------------------------------------------------------------------
# Name:         vNodeCell.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060923
# CVS-ID:       $Id: vNodeCell.py,v 1.3 2013/10/19 22:11:58 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeCell(vNodeEngBase):
    def __init__(self,tagName='S88_CL'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'cell (S88)')
    # ---------------------------------------------------------
    # specific
    def GetProcessNodeInc(self):
        return [
            {'lTag':['S88_ET','S88_ST','S88_AR','S88_CL','S88_UT','S88_EM','S88_CM',
                    'Base','Fct','Prc','drawNode','itfc','api',
                    'account','account_task','account_subtask','Bug',
                    'DocRef','parameters','Prop','Test','Tool','ToDo',],
                    'level':1,'level_max':8,},
            ]
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xd6IDAT8\x8d\x8d\xd3\xbd\x8e\xda@\x10\xc0\xf1\xbf\xd7\xf6\x0162\x02\
\x04\x88\x02K DEC\xcbc%\xe9\xef\xee\r\x92\xf4\xc9%\xaf\x90PP %\x94\xd0\xd1P\
\xf2Q\x80\x042\xc2 \x10\x06co\x9a\x1c\x08\xc1\x9d\x98j\xb5;\xfb\x9b\xd5\xec\
\xae\xa2\x08\x95{\xe2\xe5\xfb7)\x84`4\x1a\xf1\xf8\xf4\xac\xbc\xce+\xf7\x00\
\x8d\xdf\xbf$@\xa5R\xa1\xd9l2\x9b\xcd\xd8n\xb7|\xfe\xf2U\xd1\xee\xa9\xdej\
\xb5(\x95JL\xa7Sv\xbb\x1d\xfb\xfd\x9e\xddn\x07\xc0\x15\xd0\xfe\xfbGF\xa3Q\
\xda\xed6\x86a\x10\x86!\xae\xeb\x92\xcf\xe7\xd1u\x1dM\xd3\xf0}\x1f\xcf\xf3.\
\x81\xe7\xa7GY,\x16\xf1<\x8f\xe3\xf1\x88\xae\xeb\x0c\x06\x03,\xcb"\x16\x8b\
\x01 \xa5\xbc:\x9dx\x1d$\x93I\xfa\xfd>\x9a\xa61\x9dNI$\x12d\xb3Y\x1e\x1e\x1e\
\x88D"77_\x00\xaa\xaaR\xadV1\x0c\x03\xd34\x89D"\x08!\x10B\x90J\xa5P\x14\xe5}\
 \x08\x02\x1c\xc7\xb9J\x08\x82\x80\xd5j\x85\xaa\xde\xbe\xad\x13 \xa5\xc4u\
\xdd\xab\x04UU\xb1,\x0b\xdf\xf7o\x02\xa7&\x06A\x80i\x9a\x8c\xc7c<\xcfC\xd7u\
\x00\xc20$\x0cC\x84\x10o\x03?\x7f\xbcH\xdb\xb6\xa9\xd5jt:\x1d\xba\xdd.\x8e\
\xe3`\x18\xc6\xa9\xfa[M\xd4\x00\x1a\x8d\x06\xb6m\xd3\xeb\xf5\x98\xcf\xe7\x0c\
\x87CTU\xe5x<b\xdb6\xd1h\xf4\n\x08\xc3\xf0\x0c\x94\xcbe\xea\xf5:\x96e\xb1X,\
\xc8\xe5rl6\x1b\x0e\x87\x03\xa6i2\x9f\xcf)\x14\n\x17@\x10\x04g \x1e\x8f\x13\
\x8f\xc7O\x8bRJ\xa4\x94l6\x1b\x1c\xc7a2\x99\xa0(\n\x9a\xa6\xe1\xba.\xdb\xed\
\x96t:}\x06|\xdfg<\x1ec\x18\x06\xcb\xe5\x92\xf5z\x8d\xe7yd2\x19>|\xfct\xfb\
\x01\xfc\x8f\xbb~\xe3{\xf1\x0f8\xb4\xc9\xd2Jq\xdd9\x00\x00\x00\x00IEND\xaeB`\
\x82' 
