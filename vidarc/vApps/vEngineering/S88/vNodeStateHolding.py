#----------------------------------------------------------------------------
# Name:         vNodeStateHolding.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060923
# CVS-ID:       $Id: vNodeStateHolding.py,v 1.2 2013/10/19 21:35:00 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeStateHolding(vNodeEngBase):
    def __init__(self,tagName='S88_holng'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'state holding (S88)')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return False
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xb5IDAT8\x8d\xed\x931\x0e\x82P\x0c@\xdfWo\xa2n\xe8\x9f`#\x9c\x80\
\x8d\x99\x9d{p\x07\x8e@\xc2\xc6\x11>\x89:\x91\xaf!a\xe6\x02\x1c\x81\xd4\x8dH\
\xbeN\x0c:\xd8\xad/\xedK\xd3\xb4Jm\xb6\xac\x89\xcd\xaan`\xf7\x9a<\xeeV\xfa\
\xbe\x07\xc0\xf3<Ng\xad\x00n\xd7\x8b\x0c\xc3\x00@\x10\x04\xec\x0fG\xf5VPU\
\x15eY""\xa4i:\xf3\xa2(h\xdb\x96q\x1c\xc9\xf3\xfc\xf3\x04\x00I\x920M\x933j\
\x96e\x18c\x1c\xbez\x07\x7f\xc1O\n\x8c14M\xe3\x14\xd6uM\xd7u\x0e_\x1cR\x14E\
\x88\x08\x00a\x18\xce<\x8ec\xac\xb5\xf8\xbe\x8f\xd6z!P_\xff\xc6\'\xb0\xc683+\
~\nW\x00\x00\x00\x00IEND\xaeB`\x82' 
