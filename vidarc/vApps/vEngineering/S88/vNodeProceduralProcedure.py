#----------------------------------------------------------------------------
# Name:         vNodeProceduralProcedure.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060923
# CVS-ID:       $Id: vNodeProceduralProcedure.py,v 1.2 2013/10/19 21:35:00 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeProceduralProcedure(vNodeEngBase):
    def __init__(self,tagName='S88_proc'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'procedure (S88)')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xdfIDAT8\x8d\xad\x93\xd1m\xc30\x0cD\x9f\x1c\xcfQq\x13\xdf(\x1a\x85\
\xd9D@\xe7(,\x0fR\x80\xee"\xeeG\x11!q\xe3\x16\x85{\x7f\xa2\xc8\x87;BJi\xb8pF\
\xc3\xa9\xe9\xff\x00$\xf0Mj\x7f\x1e\x8c0\xd6\x8f\xd74\x02H\xa2\xb5\xef\x10w?\
\x04\xb8;fo\xdbx+\xcc\xf3LJ\tI\xb8;\xee\x8e$"\x82\x9c\xf3!h<\xba\xb8\xb9\xaa\
\xb5>\x9c%1M\xd31 "\xa8\xb5\xd2Z\xa3\x94\x82$$af\x98\x19\xeeND\x1c\x03\xee\
\x1b%u\xfbf\x86$\x80\xee\xea0\xc2\xde\xe6}\xbd\x94BD\xf4\xa5\xf7wp\xbd^{\x84\
{\x8b\xcb\xb2\xf4:@\xce\xf9\xf9\x0eJ)\x0f{X\xd7\xb57\xd7Z;x\xef,A\xdb$\x7f\
\x96\xe4G\x99\x19\x11\x85\x94\x86\x0b\xf9\xe5}3\x8b_\x87\xf6\x8a\xb0/\xc0\
\x19\x9d\xfeL\x9f\xc1\xa9e\xa4\xb8\n\xa5,\x00\x00\x00\x00IEND\xaeB`\x82' 
