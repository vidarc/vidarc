#----------------------------------------------------------------------------
# Name:         vNodeCommandReset.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060923
# CVS-ID:       $Id: vNodeCommandReset.py,v 1.2 2013/10/19 21:35:00 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeCommandReset(vNodeEngBase):
    def __init__(self,tagName='S88_cmd_reset'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'command reset (S88)')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x8cIDAT8\x8d\xed\x931\x0e\xc20\x0cE\x9fS\x8e\x91\r\xd8\n\xd9\xbaE9A\
\xce\xd0\x8bd\xc8Er\x1c`\x04\x96\xcc\xbd\x89\x99\x1a\x15\xd6\x0c,\xb5\xe4\
\xc1Oz_\xb2d\x8b\x98\x81\x9e2]6p\xd8\x0e\xef\xd7Sk\xad\x00\x8c\xe3\xc8\xe5\
\xea\x04\xe0q\xbf\xe9\xb2,\x00L\xd3\xc4\xf1t\x96&\x89\x19Z\xa7\x94\x14P@s\
\xce\xba\xf2y\x9e\x1b/\xa5\xe8\xd6\xe9^a\x0f\xd8\x03\xe0\xe7\x94C\x08\xa8*\
\x00\xde\xfb\xc6c\x8cXk\x01p\xce}\x05\xc8\xdf\xbf\xf1\x03=\n"v\x89\x8b\x9aI\
\x00\x00\x00\x00IEND\xaeB`\x82' 
