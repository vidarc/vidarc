#----------------------------------------------------------------------------
# Name:         vNodeStateRunning.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060923
# CVS-ID:       $Id: vNodeStateRunning.py,v 1.2 2013/10/19 21:35:00 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeStateRunning(vNodeEngBase):
    def __init__(self,tagName='S88_rng'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'state complete (S88)')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return False
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xd6IDAT8\x8d\xd5\x93-\x0e\x83@\x10Fg\x97\x92 J\x05\t\x92#`\xb9\x02\
\x1e\xc7\x01PH<w\xc0s\x80v\x15\n\x87\xe7\x04hL[A0\xa5\x86\x84\xf0UA\xda\x10(\
\x84\x9aN\xf2\xd4d^\xe6\'\xc3\x18\x97hO\xf0]\xd5?\x15\x88\xcb\x19\xf7\xdb\
\x15\x9b\r\x8cK\xc4\xb8D\x96eA\x96e8\x8e\x834M\xd1u\x1d\x86\xdc\x12\x1f\x02"\
\x1a1\x0c\x03a\x18\xa2,\xcbE\xd1\xac`\x80s\x0e\xdb\xb6!\x84@\xdb\xb6\x13\xd9\
W\xc1;\xba\xae#\x08\x02\x14E1\x8a\x0e[\xf6UU\x15%IB\x9a\xa6\xd1\xb3y\xe0\xa8\
\x9e\xd8\xaa\x0e\x14E\x81\xeb\xba\xc8\xb2\x0c}\xdfc\xf5\x08\xa6i"\x8a"\xd4u=\
\xbb\xc8\x89@UUx\x9e\x87<\xcf\xb7\x9d\xd1\xf7}\xc4q\x8c\xa6iV\x15\x8e\xfc\
\xff3\xbd\x00\x8fw{\xedC\x9e\x9c\xaa\x00\x00\x00\x00IEND\xaeB`\x82' 
