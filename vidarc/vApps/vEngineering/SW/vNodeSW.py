#----------------------------------------------------------------------------
# Name:         vNodeSW.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: vNodeSW.py,v 1.4 2013/06/02 20:29:35 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeSW(vNodeEngBase):
    def __init__(self,tagName='sw'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'software')
    def GetTransDescription(self):
        return u'software'
    # ---------------------------------------------------------
    # specific
    def GetProcessNodeInc(self):
        return [
            {'lTag':['Mod','array','class','struct','Exp',],'level':1,'level_max':4,},
            #{'lTag':['Mod','array','class','struct'],'level':1,'headline':u'module'},
            ]
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02\x01IDAT8\x8d\x95\x92=o\x13A\x10\x86\x9f=\xbb@\xa2F\xbaXI\\\xa2|I\
\xd8\xa9\xf8Hd\xa0\xa0!\x0em\x10%\xc2\xa1\xa0\xa2\x02\x94\xec\x9d\xf2\x1b(,~\
E\x9c\x16Eg\'\x80D\x1c\xa7\xa1\x88\x01+\xf2\x19q+S\x00\xd1B\x83\xe3\xa1\xf0G\
\xec\x10\x10\x19i43\xab\x99w\x1f\x8dF%\x93uI\x8e\xd7\xf9?\x0b\xba\x0e\xad\
\xd6S\xde\xee\x9c#\x9e\x1c\xaf\xb3\xf5\xea\xba\xea\xb5H{E:Y\xa6\xdf\x0c\x19\
\x94sSI{E\xe4\xd2W|c\xf0\x9b\xb7\xd4\xf4\xd4\xa48\'\xff\x98\x9f+\xf1\xecI\
\xbc3\xbc\xec\xc1\x9e7 \x04\xbe1\x04\xd6\xf6\xeb\xf8I\x81zx\x00\xdb\x9b\xc0<\
\x94{\xaf\x1en\xca\x13\x1b\x05\xf8\xa6\xa9\x06\xfb\xff\x10\x08\x1b\xa1\n\x1b\
!\xca)B%\x06\x15\x10V\xc4MkL\x05\xac\xd9\xe2\x9f\x02\xa7[\x86\xbd\x17~7\x1f\
\xdc\xd1#\x98\x9f+\tx\x92^@F."\xdelZ2\x89\x91~TN\x8cA\xf7<D\x04\x01Offf\xa4O\
\x90Z\x006@\xbb)\xa8\x80Nwb\xd14\x87X\x82`\x98\xad/0\xb6:B\xcdZf\xef\x8d\x11\
\xa9\x1a\xc4\xbe\x10\xa8\x1f<H\x1d\xc9n\x04\xed6D\x16\x8a\xa5\x98*\x96@9k\
\xc0\xe41\x81v]0\x86FNw\xea\xdc(P {T%_\x81\xd6\x11`\xc0\xfc\xfc\x0b\xc1\xe2\
\x1dC5\xb0,\xad&\xa8\xbd\xb7\xcc\xf2\x98\xa8|H\xa5\x9c\xa5J\x95%\xf7<5\x1b1w\
uB\xea\xe1\x01a#TC\x02Y\xedR\xc0\xa0\x13.l\x1c\x93d\xcb\xa3\x14(\xa0\xdd\x0b\
`\xe0\xd7\xb5\x1b\xb0\xbdI\xd8\x08\x87\t>\xf9{\x1c\x06\xb0\xbc\x9fc\xd7\x06\
\xb4\xd7\x13D\xfb\x96\xfb\xcbw\xa1\xfc\x19\xff\xf6\x0eA`\xc9\xbc{\x8e\xd3<\
\xe5\x12\xb5\xee\x92|\xd3\xe4\r\xb4\xf4w\xc0\x10-\xe6\x80<Zw\x96\xd0#,~\xe8,\
C]\xb9\xfcR^\xbf\xe9\xde\xfe\x19L9kjzjRT<\xbe-\x13\x13\x0f\xcf4\xdc\xb3\x8f\
\xd5<\xbf\x01\xf7#\xe5J\'=\xe8,\x00\x00\x00\x00IEND\xaeB`\x82' 
