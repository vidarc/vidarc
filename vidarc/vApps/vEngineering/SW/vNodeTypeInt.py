#----------------------------------------------------------------------------
# Name:         vNodeTypeInt.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060619
# CVS-ID:       $Id: vNodeTypeInt.py,v 1.3 2006/09/27 04:20:51 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeTypeInt(vXmlNodeEngineering):
    def __init__(self,tagName='t_int'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'software integer type')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x8dIDAT8\x8d\x9dS\xc1\x16\x80 \x08c\xe0\xff\xffq\xd9\x89\xdeZR\x14\
\'\x1doc\x08\x02\x1e\x961\xf7m\xe6\x19\x1e\xb0F\x0c&2I1\x16g\x1c\xf0\xb0\xb9\
osU\xf1\xc9Qr`f7r\xc7QbcUUm\xc3\x03\x8c\xb1\x88W\xe4*\xe0\x01n\xedt\xb0"w&\
\xd1z\xc4*\x07\x0f\xb8&\xbe\x86\xa7\x92\xce\xb9-\xf0\x87\xc4EO\x81\x04\xde\
\x9ch\xfe\xb2\x07:\xeb\xce\x82\x95SP\xac\xba\x0f\xb6\xcf\xea\xfa\xb0\xd5N\
\x80\xbf\xb3\xf6\xb8\xfa\x0b\x9a;\x00ps\x91P\xd6\xd0\x85\xef\x00\x00\x00\x00\
IEND\xaeB`\x82' 
