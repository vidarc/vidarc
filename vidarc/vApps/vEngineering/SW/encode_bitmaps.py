#!/usr/bin/env python
#----------------------------------------------------------------------
#----------------------------------------------------------------------------
# Name:         encode_bitmaps.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: encode_bitmaps.py,v 1.2 2012/04/07 16:58:48 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

command_lines = [
    #"   -u -i -n Application Tools01_16.png images.py",
#    "   -u -i -n Mondrian mondrian.ico images.py",
    "-u -i -n TypeArray         img/TypeArray03_16.png            images.py",
    "-a -u -n TypeBool          img/TypeBool01_16.png             images.py",
    "-a -u -n TypeChar          img/TypeChar01_16.png             images.py",
    "-a -u -n TypeClass         img/TypeClass03_16.png            images.py",
    "-a -u -n TypeFloat         img/TypeFloat01_16.png            images.py",
    "-a -u -n TypeInt           img/TypeInt01_16.png              images.py",
    "-a -u -n TypeList          img/TypeList03_16.png             images.py",
    "-a -u -n TypeLong          img/TypeLong01_16.png             images.py",
    "-a -u -n TypeString        img/TypeString01_16.png           images.py",
    "-a -u -n TypeStruct        img/TypeStruct03_16.png           images.py",
    "-a -u -n Array             img/TypeArray01_16.png            images.py",
    "-a -u -n Class             img/TypeClass02_16.png            images.py",
    "-a -u -n List              img/TypeList02_16.png             images.py",
    "-a -u -n Struct            img/TypeStruct02_16.png           images.py",
    "-a -u -n TypeFree          img/TypeFree01_16.png             images.py",
    #"-a -u -n Activities img/ActivitySchedule01_16.png images.py",
    
    #"-u -i -n Splash img/splashEngineering01.png images_splash.py",
    ]
