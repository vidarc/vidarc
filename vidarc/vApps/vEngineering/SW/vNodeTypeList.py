#----------------------------------------------------------------------------
# Name:         vNodeTypeList.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060619
# CVS-ID:       $Id: vNodeTypeList.py,v 1.3 2006/09/27 04:20:51 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeTypeList(vXmlNodeEngineering):
    def __init__(self,tagName='t_list'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'software list type')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x95IDAT8\x8d\x95SA\x0e\x800\x08\xa3\xb0\xff\xffX\xf1"\xa6\xa9\x10\'\
\xc9\xe2RV( \x80\x87\x95\xe5yd\xdd\xe1\x01\xdb\xb0\xc5D&)\xc6\xc1\x19_SF%\
\xaa?\xcf#\xe1\x01\x98Yv\x8f\xeb\x81\x924\xb8O\xb5\xfd\xea\xc1dZ\xb7\xca73\
\x1b\x15L\xe4\x97\xddc\xcc\xfav\xf7:\xea\x87\xc7\xac`\x1a\x9f\x9aw\xdd\xfe\
\x13\xa4m\xe2N\xfd5jW\xb0\xb2\xb2*\xc6Tm\xab\xa0\x0b\xc2\xca\x18\xf7\x92\xf2\
U\xc6\xf3\xebR_\xe0\x81\xc5\xf5h\xf6\x9d\xed\x04\xaf\xb3f\xee\xb6S}\x17i\x12\
u\xa1,"J4\x00\x00\x00\x00IEND\xaeB`\x82' 
