#----------------------------------------------------------------------------
# Name:         vNodeIumGrid.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120407
# CVS-ID:       $Id: vNodeIumGrid.py,v 1.3 2012/07/07 16:41:38 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeIumGrid(vNodeEngBase):
    def __init__(self,tagName='xMIIIumGrid'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'xMII iGrid')
    def GetTransDescription(self):
        return u'xMII iGrid'
    # ---------------------------------------------------------
    # specific
    def GetGrpDict(self):
        return {
            None:0,
            'method':[-1,None,]
            }
    def GetProcessNodeInc(self):
        return [
            {'lTag':['Mod','array','class','struct'],'level':1,'level_max':4,},
            #{'lTag':['Mod','array','class','struct'],'level':1,'headline':u'module'},
            ]
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'dataSrv':[
                    _(u'10 data server'),u'dataSrv',-10,
                    {'val':u'','type':'string',},
                    {'val':u'','type':'string',}],
            u'qryTmpl':[
                    _(u'30 query template'),u'qryTmpl',-30,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x011IDAT8\x8d\xa5\x93=N\x031\x10\x85\x9f\x93\x1c\x80\n\xa5\xa5F\xa2\xe2\
\x00\x91VJ\xe5*\x1dw\xd8\x86k\x00\x05\xcbO\x01\xe9\xa0\xa5\x1c\x85H\x9b\x9a\
\x82r\x8f\x80(\xa88\x00\xd9\x99\xa1X\xff\x92\r \xc5\xd2H\x96=\xfe<\xefyl\xcc\
`\x88]\xc6\x08\x00no\xae\x95\x99\xf13D\xa4w\xee\xe3\xb2\xba2#O*\xcb\x12\xcc\
\x003@D\x98\x8d\x9f\x00\x11@\x15\x10\xc1\xe3\xdb\t\x8a\xc2\x86\x9c\xaa:\x8d\
\x15\xa8*\x98\xbb\xdc0\x983\xc0\xaf\x12<\x00\x88g\xb0^Gb\x0f@\xdd^\x00\x10\
\x91\xdb\xe8\x82^\xde\xdd\xed]\xa2\x1e\x00uM\x10\xf1\x97$\x00\x00\x98Nm\x00,\
\x16\x04{\xbc\x1f\xcbW\xc5\xc3'2\x0f\x9a\xa6\xee\x97\xe0+\x08\xa68\x88\x9f\
\xc6\xd0~@\x90\xdc\xb6Q\xbbjZ\xcc&\xc0\x9b\x1e\rJ^\xc1A< Y\xca+X.)\x03\xd0\
\xebGg\xa0\n \n=\x04V+\xda.\xa1(l\x00\xd45\xc1\x1e\xede}0\xff\x02&\x13\x1b\
\x96\x9a\xe6y\xbb\x04\x11t}\xe0ku&\xa6}\xf5\xb7\x89\xd2&]\x95=H\xe6\x811\x83\
!.\xce\xcf\xf4\xbf\x1f(\x8d\xbb\xfb\xb91\xbb~\xe7o\xaeJY\xd1\xc9P]\x0c\x00\
\x00\x00\x00IEND\xaeB`\x82" 
