#----------------------------------------------------------------------------
# Name:         vNodeQryAgg.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120407
# CVS-ID:       $Id: vNodeQryAgg.py,v 1.3 2012/09/09 08:04:27 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeQryAgg(vNodeEngBase):
    def __init__(self,tagName='xMIIQryAgg'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'xMII aggregation query')
    def GetTransDescription(self):
        return u'xMII aggregation query'
    # ---------------------------------------------------------
    # specific
    def GetGrpDict(self):
        return {
            None:0,
            'method':[-1,None,]
            }
    def GetProcessNodeInc(self):
        return [
            {'lTag':['Mod','array','class','struct'],'level':1,'level_max':4,},
            #{'lTag':['Mod','array','class','struct'],'level':1,'headline':u'module'},
            ]
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'dataSrv':[
                    _(u'10 data server'),u'dataSrv',-10,
                    {'val':u'','type':'string',},
                    {'val':u'','type':'string',}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02\xaeIDAT8\x8d\xa5\x92YH\x94a\x14\x86\x9fo\xe6\x1fi\x16Qs\xca\\\xd2\
\x94\xc9\xdcJ\rJ*\x8a\xb2\xe8\xa2\xa0\xc2$\xe8\xc2\x8a\xac.\xc2\n\xba(B*\xc4\
HH!\xa2h\x13""\xb0"*\xf0V\x8b\x16sAs\xc9\xfc\xb5\xb4\xd2q\xd7\xc2\x1agq\xc6\
\x7f\xbe.\xa2\xc5\xf1&\xe8\\\x9e\xc39\x9c\xe7}_!tz\xfe\xa7DR)R\xcc\x87\xd5\
\xdf\xe1\xd61\xbd\xf8\x97\xa5\xfd\x974\xf9<\x04V\xf5\x81\xb2\xfe+\x0c\x8eBU\
\xb2\x99m\xb5qrI\xfb8\xa5\x07\xbf\x88\xe2#\x9a\x8cy\x1b\xcc\xa4\xdf\xcfh\xba\
\x93\xe2\xcbzq\xa2<\\~H\xb3\xf2\xe2]/\xa9mN"\x8d \xfeF8T eDl&\x99\xc3V\xc2_\
\x8f\xb2,.\t\xa3\xc5Bc{#}Ysy\xb1p\x82\xc9\xdef\xee^\x11\xbf?\x15\x81\x1a\x14\
\xe6hrS[\n\xeb\x0e\x1f@:\x9dh--\xf4\xa9*\xe5\xed*\xf6\\I\xc5\xa3\x99\x98J \
\x9f\xcb\x07\xd3\xdd\x1dT\x9e;GbF&\x95\xb5u<\xf3y1\xe8$\xc27[\x0f]`#:c)\xdd7\
\xf2(\xc9YLgu5\xcd.\'\xc3\xf9+H\xbb\x9eGr\xc6\xd2\xd9.\xfcB(*\x8f\x95\xae\
\xd5\xcbQ;\xc6\x89\xb9\xd2A\xc4\xcb16\xa3\xa3\xc2/\xa9\xd5I&\xd7\x86\x10U\
\x90\xc6\x9a\x94(\x94\xba7\x9c\xc9\xff,\x00\xc4\x96{A\xd2\xae\xd7\x98\xe8\
\xd1X\xe5\x80\x14\x03\x88\xb1 B\xeb\xe7\x10\xd71\xc9\x1d\xfc8S\xc0\x96\x05\
\xf3\xe6A\xa7\x17Z,\x10\x92\xa0\xb0r\xc4\x8f\xd8t\x12\x19\xfe\xcd\xc8\x83\
\x9b^\x91\xb3OH\xfd7+VM\xe0\xd6$\x8e\x8f&\xec\xc2M\xbcM\xc3\xaby\x91J\x10:\
\xab\x0f\x8fWR\xf3Q#;\xd1\x81R]\xaa\x17\xe0\x05 B\xbf\x9b\xa2\x1b\x17\xe9\
\x1f\x1b&,4\x94\xb0\xb9\xe1hn\x0f\xaa\xaab\xb3\xd90\x1bM\xa8\xad*\x16\x8b\
\x85~\xbb\x9d\xdb\x15\x9bg\xba`\n\xf5\x13\x12l"y\xf1\x1arw\xed\xe2\xea\xb5\
\xebl\xdd\xbe\x83\xec\xec\r476\xe0\x9d\xf6\xd1\xd5\xd9\xc5\x82\xc8\x08\x16\
\xc6.\xc2/\x03\\0\x19\x839_r\x81\xd4e\xe9\xbc\xaa\xa9\xe5\xf1\xe3\'tv\xbd\'>\
\xc1FKk;\xf7\xef=$>\xde\xc6\x97\xf1\t>\xf5|\x86\xc0\x03\x1e\x8f\x9b\xa1\xa1A\
\x9e>\xad\xa2\xb0\xf0\x14\x03\x03v\x8a\x8a\xce\xd2\xd0P\xcf\xd4\x94\x87\xb2\
\xb2RFF\x86\x19\x1c\xec\xc7d\x9a\x83\x10\x01I\xcc\xdd\xb9Q\x1e=z\x1a\xb7\xdb\
\x8d\xc1`@Q\x14\x84\x10\xb8\\.\xccf3\x00\x0e\x87\x03\xa3\xd1HSS\x13\xf5\xf5\
\xc7g\x1e\xd8\xbbG\x93SS \x04H9;u\x7f\xc2\xf3s\x1e\x1d\r?\x00\xf2$\x13/\x831\
+\x89\x00\x00\x00\x00IEND\xaeB`\x82' 
