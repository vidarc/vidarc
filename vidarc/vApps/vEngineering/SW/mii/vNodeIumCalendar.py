#----------------------------------------------------------------------------
# Name:         vNodeIumCalendar.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120407
# CVS-ID:       $Id: vNodeIumCalendar.py,v 1.2 2012/04/07 22:49:24 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeIumCalendar(vNodeEngBase):
    def __init__(self,tagName='xMIIIumCalendar'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'xMII iCalendar')
    def GetTransDescription(self):
        return u'xMII iCalendar'
    # ---------------------------------------------------------
    # specific
    def GetGrpDict(self):
        return {
            None:0,
            'method':[-1,None,]
            }
    def GetProcessNodeInc(self):
        return [
            {'lTag':['Mod','array','class','struct'],'level':1,'level_max':4,},
            #{'lTag':['Mod','array','class','struct'],'level':1,'headline':u'module'},
            ]
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'dataSrv':[
                    _(u'10 data server'),u'dataSrv',-10,
                    {'val':u'','type':'string',},
                    {'val':u'','type':'string',}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xf0IDAT8\x8dc\xf4M1\xf9o\xe6\xa6\xca@*8\xb5\xeb6\xc3\x96y\xe7\x19Y\
\x8c\x9d\x95\x18\x0c\x14\x1cI6\xe0\xaf\xf3_\x86-\xf3\xce3\xb0\xfc\xfd\xf3\
\x87\xe1\xec\xdd=\xa4\x1b\xf0\xe7\x0f\x03\x03\x03\x03\x03\xcb\x9f\xdf\x7f\
\x18\x1e\xdd{H\xb2\x01R\xb2\xd2\x10\x03~\xff\xfe\xcd\xb0\xa0\xee\x0c\xc9\x06\
\x94\xce\xf2\x82\x1a\xf0\xeb\x17\\\xf0\xec\xd9\xb3\x045\x1a\x1b\x1b30000\xc0\
\xf41\xfd\xfa\xf9\x0b\xab\x02\\4\x0c\xc0\xf41!\xbb\x00\xd9\x15\xb8h\x18\xa0\
\x9a\x0bX\xd0\r8{\xf6,\x03\xe3\xba\x19\x0c\xe7\xd6100200\x9ce\xc8\xc0\xea\
\x02\x9c^\x80\xd9d\xd4:\x1b\xaf\x0bpz\x01f\xd3\xb9\xeaT\x14>I.\xf8\x1f\x94\
\xc1`\xd4:\x9b\xe1\x7fP\x06a\x17\xfc\xfe\xfd\x1b\xab\x0b\x08\xc6\x02T\x1f\
\xcb\x9f\xdf\x7f0\xfcO\x0c\x80\xe9cyr\xe3;\x83m\x04/\xd1\x1aa\xe0\xf9\x1d\
\x88\x0b\x00QN\x98\xd1')\xdf\xc3\x00\x00\x00\x00IEND\xaeB`\x82" 
