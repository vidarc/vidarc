#----------------------------------------------------------------------------
# Name:         vNodeQryXac.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120407
# CVS-ID:       $Id: vNodeQryXac.py,v 1.4 2012/09/09 08:04:27 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeQryXac(vNodeEngBase):
    def __init__(self,tagName='xMIIQryXac'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'xMII xacute query')
    def GetTransDescription(self):
        return u'xMII xacute query'
    # ---------------------------------------------------------
    # specific
    def GetGrpDict(self):
        return {
            None:0,
            'method':[-1,None,]
            }
    def GetProcessNodeInc(self):
        return [
            {'lTag':['Mod','array','class','struct'],'level':1,'level_max':4,},
            #{'lTag':['Mod','array','class','struct'],'level':1,'headline':u'module'},
            ]
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'dataSrv':[
                    _(u'10 data server'),u'dataSrv',-10,
                    {'val':u'','type':'string',},
                    {'val':u'','type':'string',}],
            u'qryUsed':[
                    _(u'30 query'),u'qryUsed',-30,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02fIDAT8\x8d\xa5\x93]H\x93q\x14\xc6\x7f\xffm\x99\xef\xdc\x8c\xd6P"\xd7\
\xd2\x12\xc2\xca\x1b\x0b,*\xc9\xe8\xa6\x1c\xd4E \x15^\x98\xa2y\xd1U\x1fPFeY\
\x91\x04iw%\x14JE\xd1\x97P6\xd14\xc844/\xca\x08\xace\xe8`\x96\xb4/5\xf7\xee]\
\xef\xfe]D\x91%\x1atn\x9f\xf3<<\xe79\xe7\x08a0\xf2?e\xf8\x97\xa6\xb3g\xca$@\
\xd5\xc9\x8b\xf2Xe\xb9\xfc\x1d\x13\xb39\xa8>\xbdOn\xdf^\xc9\x9b7#,\\h\xa3\
\xbb\xbb\x17\x9f\xef\x1a\xb5u\x8f\x05\x80i&\xf2\x89\xe3\x1b\xa4\xc1\xb0\x86H\
$Nj\xaa\x85\x89\x89\x08\xb9\xb9\xab\x19\x1f_A0h\x97\r\x8d\x8db\xc6\x11\xec\
\xf6\xad\x14\x15\xed\xc4fS0\x9b\x05\xbd\xbd\x17QU\x18\x1a\xf2a\xb1\xbcbZ\x07\
w\xef\xb4\xc9\xec\xecL\xda\xda\xae\xf3qh\x8c\x0b\xb5\xa7H\x9c\x93\x80s\xf1R\
\\\xae*\xe2qI 0\x1f\xbf\xdf\t\xbc\x9e*P}\xbaX\xe6\xe4\xac\xa2\xf3Y++\xf3w\
\xb2+3\x9d\x01\x15\xde\x87 \xd0\xd1LOO\x0bii\xab\xe8\xee\xeeG\xca\xf1\xa9\
\x0e\x9ev\x0c\xc8\xf4\xf4\x05\x18\x8d&\xdez<l\xd9]\x88.!2\xa9\xa1\x8d\xeb\
\x18Voe\xb0\xbe\x12\x87#\x83\xbaK\xc5\xe2\'\xefW\x06MM\xad\x84\xc3\x1a&\x93 \
\x12\x89\xd0\x1f\x84\x97\xa3QFB1&&5\xb4\x18\xc4\xf4\xafl+\xd8(~wm\xfa\x91\
\xf6\r\xe9r\xad#\x1e\x97\xc4b\x1a"\xae\xf2.\x08B\xd5PU\x9d\xb1\x98\x81E\x8a$\
\xc1\x14\xfd+h\x03@4\x1a\xc6\xe3\x19%\x16\xd3\xd1uA\x86s9\xd1\xbe\'|\xf9f\
\xe5\xabq\x1e)\xc9V\x02\xad\xf5\xd8R\x96\xe2>\xea\x98\xfe\x90\xf6\xec\xde/KJ\
*HJ2c\xb3)\xb8\xdd\x0f\xf0\x0e\x7f@\xd5\xa2D&5\x1cK\x1c\xec\xd8 X\xd6~\x95.-\
\xc8\xa6\xea\xcfb\x8a\xc0\xa1\x83Wd^\xdeZ|\xbe0YYN\x14EGQ\x12P\x94\xb9\x84B\
\x83x\xef\xec`\xb3>\x87\xc4\xb2\x02\xbe\xddl\xa7\xd3?L\xfe\x851\xf1k\x0b\xe7\
kJ\xc5\xf0p\x85\xb4Z\x9f\xe3\xf7W18\xe8EU\x1b\xb0XJ\xf9\xf4\xa9\x99{\xf7\xbd\
\xa2\xe5\x88]\xae\xbf\xdc\x8c\xb2\xb7\x90\xae\xa6d\xe0\xd0\xf4\xbf\xe0*(\x97\
f\xb3\x97[\xb7\x1f\x89?\xb1\x87\x07\x14\xd9\xb7\xe00\x13a75\xe7^\x88Y\x9fi\
\xb6\xfa\x0e\xd2 \xfc\xa5\xd1\x90\xf7P\x00\x00\x00\x00IEND\xaeB`\x82' 
