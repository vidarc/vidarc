#----------------------------------------------------------------------------
# Name:         vNodeWebMisc.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120412
# CVS-ID:       $Id: vNodeWebMisc.py,v 1.1 2012/04/13 18:24:27 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeWebMisc(vNodeEngBase):
    def __init__(self,tagName='xMIIWebMisc'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'xMII Web misc')
    def GetTransDescription(self):
        return u'xMII Web misc'
    # ---------------------------------------------------------
    # specific
    def GetGrpDict(self):
        return {
            None:0,
            'method':[-1,None,]
            }
    def GetProcessNodeInc(self):
        return [
            {'lTag':['Mod','array','class','struct','itfc','api','var'],'level':1,'level_max':4,},
            #{'lTag':['Mod','array','class','struct'],'level':1,'headline':u'module'},
            ]
    def GetCfgDictOld(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'dataSrv':[
                    _(u'10 data server'),u'dataSrv',-10,
                    {'val':u'','type':'string',},
                    {'val':u'','type':'string',}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\x15IDAT8\x8d\x9d\x931\x8e\x830\x10E\x1f\x069\x813 \xd1 _\x806R\xae\
\x82(\xa8(\xd3\xef\xde#\xc5\x9e*\rw\xa0J\x85\x8c3)VX\x04L\xb2\xda_!\xcd\xff\
\x8f\xf9#\x88X\xa9\xaa\xaa\xef\xd3\xe9\xf4\x95\xe7\xf9z\xe4\xd5\xf7=\xd7\xeb\
5\x02H\x96\x83\xf3\xf9\xfcc\x8c\xa9\xf3<\xe7r\xb9\xec\x02\xda\xb6\xf5\xcfj\
\x1d\xae\xeb\x9a\xbe\xef\x19\x86\x81\xfb\xfd\xce8\x8e8\xe7p\xce\xf1x<6\xb0d\
\x196\xc60\x0c\x03\x00Zk\x0e\x87\x03q\x1c{\xb3\x88l\x00\n\xc0\x18S\x17E\x81\
\xb5\x96\xdb\xedFY\x96\x7f\n\xbf\xdc\xa0i\x1a\xff\xd64M7\xc6(\x8a\x82\x00\
\x7f\x839\xac\xb5\x0e\x1a\xdfV\x98\x01Z\xeb\x97\xb5\x81\xe0\xe1\x82\x80\xb9\
\xf3^\xd7\x8f\x00\xa5\x14"\xb2\xe9\xaa\x94\xb7\xbc\xaf\xf0_y\x80\x88|\\?4O\
\x02>\x00\x9cs\x88\x08I\xf2k\xb1\xd6\x06}\x1e0\xd3\xe7\xe0,k-"\xc24M\x00\x1c\
\x8f\xc7\xfd\r\x96\xc1%\xd0Z\xcb4M\xc1\x8f\xc9\x03\xba\xae\xdbk\xe3\x95e\x19\
\xeb\xdf\xfc\t\x93\x19{\x95Me\x99\xaa\x00\x00\x00\x00IEND\xaeB`\x82' 
