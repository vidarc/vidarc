#----------------------------------------------------------------------------
# Name:         vNodeWebHtml.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120412
# CVS-ID:       $Id: vNodeWebHtml.py,v 1.1 2012/04/13 18:24:27 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeWebHtml(vNodeEngBase):
    def __init__(self,tagName='xMIIWebHtml'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'xMII Web html')
    def GetTransDescription(self):
        return u'xMII Web html'
    # ---------------------------------------------------------
    # specific
    def GetGrpDict(self):
        return {
            None:0,
            'method':[-1,None,]
            }
    def GetProcessNodeInc(self):
        return [
            {'lTag':['Mod','array','class','struct','itfc','api','var'],'level':1,'level_max':4,},
            #{'lTag':['Mod','array','class','struct'],'level':1,'headline':u'module'},
            ]
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'dataSrv':[
                    _(u'10 data server'),u'dataSrv',-10,
                    {'val':u'','type':'string',},
                    {'val':u'','type':'string',}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\x1bIDAT8\x8d\x9d\x931\x8a\x850\x14E\xaf\x1a\xf2Up\x07\x82\x8dd\x03\
\xb6\x82[\x11\x0b+K\xfb\x99}X\xcc\xaal\xb2\x07+\x0b\x91$\xe6W\t\x1a\xe3\x9fa\
^\x15x\xef\x9ewo\x8c\x01\x9c\xaa\xaa\xea\xbb\xae\xeb\xaf<\xcf\xdd\x96-\xce9\
\xa6i\n\x00\x80\x9c\x1bM\xd3\xfc0\xc6\xda<\xcf1\x8e\xe3#\xa0\xef{{\x0e]q\xdb\
\xb6\xe0\x9ccY\x16\xac\xeb\x8a}\xdf\xa1\x94\x82R\n\xc7q\xdc`\xe4,f\x8caY\x16\
\x00\x00\xa5\x14\xaf\xd7\x0bQ\x14\xd9a\xad\xf5\r\x10\x02\x00c\xac-\x8a\x02B\
\x08\xcc\xf3\x8c\xb2,\xff$\xbe\xdcA\xd7uvk\x92$\xb7\xc1 \x08>\x03\x8c8\xcb2\
\xdb\x94R^\x86\x1f#\x18\x00\xa5\xf4&\xf2]\x9c\xd7\x81\xc9|\xdeB\x08\xf1:\xf1\
:\x08\xc3\x10Z\xebKV#4\xa0\x8f\x11\xfe[\x16\xa0\xb5~\xfcT\xe7\x99_\x1d\x18\
\xbb\xc05;!\xe4\xf9%\x9e\xe9\xfb\xbe_6m\xdb\x06\xad5\xa4\x94\x10B \x8ec?\xc0\
\xb5h\xceJ)\x08! \xa5\xf4>&\x0b\x18\x86\xe1\xd6t+MS\xb8\xbf\xf9\x1b\xf3@\x87\
s\xd5\x86\xa0\xb8\x00\x00\x00\x00IEND\xaeB`\x82' 
