#----------------------------------------------------------------------------
# Name:         vNodeSvg.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120407
# CVS-ID:       $Id: vNodeSvg.py,v 1.3 2012/09/09 08:04:27 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeSvg(vNodeEngBase):
    def __init__(self,tagName='xMIISvg'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'xMII SVG')
    def GetTransDescription(self):
        return u'xMII SVG'
    # ---------------------------------------------------------
    # specific
    def GetGrpDict(self):
        return {
            None:0,
            'method':[-1,None,]
            }
    def GetProcessNodeInc(self):
        return [
            {'lTag':['Mod','array','class','struct'],'level':1,'level_max':4,},
            #{'lTag':['Mod','array','class','struct'],'level':1,'headline':u'module'},
            ]
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'dataSrv':[
                    _(u'10 data server'),u'dataSrv',-10,
                    {'val':u'','type':'string',},
                    {'val':u'','type':'string',}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xdeIDAT8\x8d\xcd\x93\xbdk\x93q\x10\xc7?\xbf\xdf\xf3$\xb5\xa51\x91\
\xbeHMZ\xacX*)\tR\xd2\x18p\x10:tS\x8a\x9b\xe0\x1f\xd0Aj\xeb\xd8\xc5 ".\xd2\
\xe2\xe0P\xf0\x15u\xd0Al\xb5\x05\x11q\x10\'A\xc1\xc1\x17\x88\x82N\x12Lc\x93\
\'\xc9\x93\xe7\xf9=\xe7P\rX]\xa4\x83\xder\xc7q\xf7\xe1\xf8\xde\x9dR\xdab+\
\xa6\xb7\xd4\xfd_\x00\xec?%\xef\xde\xb9$Zw\xa3\xb5\x8d\xeb\xba\xd4jU*\x95"\'\
g\xe6\xd4\xe6Z\xb5Y\xc4\xe5\xa5\xfb\xe2z\x8aHG\x84h,\x82\x1d\xb2q\x1b\r\x1c\
\xa7\xca\xe7O\x1f(\x14^p\xee\xfcb\x0b\xd4\x9a\xe0\xda\xd53\x12\x8b\xa5h\xd4=\
\xc6R)\xe2\xc3{\xc0S\x88\x12,\x1bj\x8e\xcb\xae\x81>\xec\xf6\x0ef\xa6k\xb2p\
\xf1\xa6\xfaE\x83\xae\xae4N\xb5\xc1\x91\xa3\x87\xe9-\xbe\xa7\xb0\xf2\x84/\
\x1e\x94\x8aM\xca\xeb\x01\xe1f\x8d\xe4\x83\x05\x0e\x0e\xc5\x19\xd8\x9be\xfaD\
ZZ\x80\xdb\xb7\xe6\xa5T\xaa0>~\x08\xd3\xd4Tv\x1f\xa0\xe7\xdeY\xbc\xa7\x8f\
\xe9\xedk\xa33\xac1\xdb\xa3\x98\xc4>\x06W\x17I\x8efPd\x00\xb0\xf2\xa7\x8fK<>\
\x86e\x85\xc9fG)\x7fmb\xed\x8c\xd0Y\xfb\xc6\xf2|\x9e\xa55\xc5\xf0`?;bQd$\x87\
\xbe~\x81\xd0P\x92Wk>\x99\x91\xd7y\xed8\r\x94R$\x12\t\x8c1Xa!b|\xde\xa6\'x\
\xbe\x7f\x82\x97\xcf\x1e\xf1pu\x05\t|<\xcf\xc5?6K\xb4\xbb\x87m!M\xa5ZFMMMJ.7\
\t@\x10\x04\x04\x81!\x08\x84\xc0\x18,\xcbBiM\xbd^\xc7\xf3<\x02c0m\xed(c\xf8\
\xf8\xee\r\xf5\xc6\x8d\x8d5\x9e\x9a\xed\x17\xdf\xf7\x11\x11D\x82\x1f^0f#\xfe\
\t\x17\x11\xc4\xf8\x08\xa0-\x9b\xcbW\xd6\xd5ow\xf0\xb7\xf6\xef\x7f\xe1;\xc5J\
\xcb\r5\xf3M\xfd\x00\x00\x00\x00IEND\xaeB`\x82' 
