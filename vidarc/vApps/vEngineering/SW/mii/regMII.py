#----------------------------------------------------------------------------
# Name:         regMII.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20120407
# CVS-ID:       $Id: regMII.py,v 1.13 2013/05/20 12:08:36 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust

try:
    if vcCust.is2Import(__name__,'__REG_NODE__',True):
        from vidarc.vApps.vEngineering.SW.mii.vNodeMII import vNodeMII
        from vidarc.vApps.vEngineering.SW.mii.vNodeModxMII import vNodeModxMII
        from vidarc.vApps.vEngineering.SW.mii.vNodeGrpxMII import vNodeGrpxMII
        from vidarc.vApps.vEngineering.SW.mii.vNodeTransaction import vNodeTransaction
        from vidarc.vApps.vEngineering.SW.mii.vNodeSvg import vNodeSvg
        from vidarc.vApps.vEngineering.SW.mii.vNodeQryAgg import vNodeQryAgg
        from vidarc.vApps.vEngineering.SW.mii.vNodeQryAlm import vNodeQryAlm
        from vidarc.vApps.vEngineering.SW.mii.vNodeQryOLAP import vNodeQryOLAP
        from vidarc.vApps.vEngineering.SW.mii.vNodeQrySql import vNodeQrySql
        from vidarc.vApps.vEngineering.SW.mii.vNodeQryTag import vNodeQryTag
        from vidarc.vApps.vEngineering.SW.mii.vNodeQryXac import vNodeQryXac
        from vidarc.vApps.vEngineering.SW.mii.vNodeQryXml import vNodeQryXml
        from vidarc.vApps.vEngineering.SW.mii.vNodeIumGrid import vNodeIumGrid
        from vidarc.vApps.vEngineering.SW.mii.vNodeIumBrowser import vNodeIumBrowser
        from vidarc.vApps.vEngineering.SW.mii.vNodeIumCalendar import vNodeIumCalendar
        from vidarc.vApps.vEngineering.SW.mii.vNodeIumChart import vNodeIumChart
        from vidarc.vApps.vEngineering.SW.mii.vNodeIumChartSPC import vNodeIumChartSPC
        from vidarc.vApps.vEngineering.SW.mii.vNodeIumCmd import vNodeIumCmd
        from vidarc.vApps.vEngineering.SW.mii.vNodeIumTicker import vNodeIumTicker
        
        from vidarc.vApps.vEngineering.SW.mii.vNodeWebIrpt import vNodeWebIrpt
        from vidarc.vApps.vEngineering.SW.mii.vNodeWebHtml import vNodeWebHtml
        from vidarc.vApps.vEngineering.SW.mii.vNodeWebXml import vNodeWebXml
        from vidarc.vApps.vEngineering.SW.mii.vNodeWebMisc import vNodeWebMisc
        from vidarc.vApps.vEngineering.SW.mii.vNodeWebJavaScript import vNodeWebJavaScript
        
        import vidarc.vApps.vEngineering.__link__
except:
    vtLog.vtLngTB('import')

def RegisterNodes(doc,oRoot=None):
    try:
        if vcCust.is2Import(__name__,'__REG_NODE__',True):
            oBase=vNodeMII()
            doc.RegisterNode(oBase,True)
            for o in [vNodeModxMII(),vNodeGrpxMII(),
                    vNodeTransaction(),vNodeQrySql(),vNodeWebIrpt(),
                    vNodeWebHtml(),vNodeWebXml(),vNodeWebMisc(),vNodeWebJavaScript(),
                    vNodeIumGrid(),vNodeIumBrowser(),
                    vNodeIumChart(),vNodeIumChartSPC(),vNodeIumCmd(),
                    vNodeQryAgg(),vNodeQryAlm(),vNodeQryOLAP(),
                    vNodeQryTag(),vNodeQryXac(),vNodeQryXml(),
                    vNodeIumCalendar(),
                    vNodeIumTicker(),vNodeSvg(),]:
                doc.RegisterNode(o,False)
            vidarc.vApps.vEngineering.__link__.LinkNodes(doc,[
                    ('sw',['xMII',]),
                    ('xMII',['Mod','Base','Param','Documents','itfc','api',
                        'var','const','account','xMIIMod','xMIIGrp',
                        'xMIIQryAgg','xMIIQryAlm','xMIIQryOLAP','xMIIQrySql',
                        'xMIIQryTag','xMIIQryXac','xMIIQryXml','xMIISvg',
                        'xMIIIumGrid','xMIIIumBrowser','xMIIIumCalendar',
                        'xMIIIumChart','xMIIIumChartSPC','xMIIIumCmd',
                        'xMIIIumTicker','xMIIWebIrpt','xMIITransaction',
                        'drawTiming','drawNode','Prop','dataCollector',
                        'Bug','Test','ToDo','Tool','sw','database',]),
                    ('xMIIMod',['t_free','t_array','t_class','t_struct','t_list',
                            't_bool','t_char','t_float','t_int','t_long','t_string',
                            'Mod','Base','Param','Prop','itfc','api','var','const','account',
                            'Documents','Doc','drawTiming','drawNode',
                            'Bug','Test','ToDo','Tool','sw','database',]),
                    ('xMIIGrp',['t_free','t_array','t_class','t_struct','t_list',
                            't_bool','t_char','t_float','t_int','t_long','t_string',
                            'Mod','Base','Param','Prop','itfc','api','var','const','account',
                            'Documents','Doc','drawTiming','drawNode',
                            'Bug','Test','ToDo','Tool','sw','database',]),
                    ('xMIIQryAgg',['t_free','t_array','t_class','t_struct','t_list',
                            't_bool','t_char','t_float','t_int','t_long','t_string',
                            'Base','Param','Prop','itfc','api','var','const',
                            'Documents','Doc','drawTiming','drawNode',
                            'Bug','Test','sw','database',]),
                    ('xMIIQryAlm',['t_free','t_array','t_class','t_struct','t_list',
                            't_bool','t_char','t_float','t_int','t_long','t_string',
                            'Base','Param','Prop','itfc','api','var','const',
                            'Documents','Doc','drawTiming','drawNode',
                            'Bug','Test','sw','database',]),
                    ('xMIIQryOLAP',['t_free','t_array','t_class','t_struct','t_list',
                            't_bool','t_char','t_float','t_int','t_long','t_string',
                            'Base','Param','Prop','itfc','api','var','const',
                            'Documents','Doc','drawTiming','drawNode',
                            'Bug','Test','sw','database',]),
                    ('xMIIQrySql',['t_free','t_array','t_class','t_struct','t_list',
                            't_bool','t_char','t_float','t_int','t_long','t_string',
                            'Base','Param','Prop','itfc','api','var','const',
                            'Documents','Doc','drawTiming','drawNode',
                            'Bug','Test','sw','database',]),
                    ('xMIIQryTag',['t_free','t_array','t_class','t_struct','t_list',
                            't_bool','t_char','t_float','t_int','t_long','t_string',
                            'Base','Param','Prop','itfc','api','var','const',
                            'Documents','Doc','drawTiming','drawNode',
                            'Bug','Test',]),
                    ('xMIIQryXac',['t_free','t_array','t_class','t_struct','t_list',
                            't_bool','t_char','t_float','t_int','t_long','t_string',
                            'Base','Param','Prop','itfc','api','var','const',
                            'Documents','Doc','drawTiming','drawNode',
                            'Bug','Test',]),
                    ('xMIIQryXml',['t_free','t_array','t_class','t_struct','t_list',
                            't_bool','t_char','t_float','t_int','t_long','t_string',
                            'Base','Param','Prop','itfc','api','var','const',
                            'Documents','Doc','drawTiming','drawNode',
                            'Bug','Test',]),
                    ('xMIIIumGrid',['t_free','t_array','t_class','t_struct','t_list',
                            't_bool','t_char','t_float','t_int','t_long','t_string',
                            'Base','Param','Prop','itfc','api','var','const',
                            'Documents','Doc','drawTiming','drawNode',
                            'Bug','Test',]),
                    ('xMIIIumBrowser',['t_free','t_array','t_class','t_struct','t_list',
                            't_bool','t_char','t_float','t_int','t_long','t_string',
                            'Base','Param','Prop','itfc','api','var','const',
                            'Documents','Doc','drawTiming','drawNode',
                            'Bug','Test',]),
                    ('xMIIIumCalendar',['t_free','t_array','t_class','t_struct','t_list',
                            't_bool','t_char','t_float','t_int','t_long','t_string',
                            'Base','Param','Prop','itfc','api','var','const',
                            'Documents','Doc','drawTiming','drawNode',
                            'Bug','Test',]),
                    ('xMIIIumChart',['t_free','t_array','t_class','t_struct','t_list',
                            't_bool','t_char','t_float','t_int','t_long','t_string',
                            'Base','Param','Prop','itfc','api','var','const',
                            'Documents','Doc','drawTiming','drawNode',
                            'Bug','Test',]),
                    ('xMIIIumChartSPC',['t_free','t_array','t_class','t_struct','t_list',
                            't_bool','t_char','t_float','t_int','t_long','t_string',
                            'Base','Param','Prop','itfc','api','var','const',
                            'Documents','Doc','drawTiming','drawNode',
                            'Bug','Test',]),
                    ('xMIIIumCmd',['t_free','t_array','t_class','t_struct','t_list',
                            't_bool','t_char','t_float','t_int','t_long','t_string',
                            'Base','Param','Prop','itfc','api','var','const',
                            'Documents','Doc','drawTiming','drawNode',
                            'Bug','Test',]),
                    ('xMIIIumTicker',['t_free','t_array','t_class','t_struct','t_list',
                            't_bool','t_char','t_float','t_int','t_long','t_string',
                            'Base','Param','Prop','itfc','api','var','const',
                            'Documents','drawTiming','drawNode',
                            'Bug','Test',]),
                    ('xMIIWebIrpt',['t_free','t_array','t_class','t_struct','t_list',
                            't_bool','t_char','t_float','t_int','t_long','t_string',
                            'itfc','api','var','const','Fct','Prc',
                            'Base','Param','Prop',
                            'Documents','Doc','drawTiming','drawNode',
                            'Bug','Test','ToDo','Tool',]),
                    ('xMIITransaction',['t_free','t_array','t_class','t_struct','t_list',
                            't_bool','t_char','t_float','t_int','t_long','t_string',
                            'itfc','api','var','const','Fct','Prc',
                            'Base','Param','Prop',
                            'Documents','Doc','drawTiming','drawNode',
                            'Bug','Test','ToDo','Tool',]),
                    ('xMIISvg',['t_free','t_array','t_class','t_struct','t_list',
                            't_bool','t_char','t_float','t_int','t_long','t_string',
                            'itfc','api','var','const',
                            'Base','Param','Prop',
                            'Documents','Doc','drawTiming','drawNode',
                            'Bug','Test',]),
                    ('xMIIWebHtml',['t_free','t_array','t_class','t_struct','t_list',
                            't_bool','t_char','t_float','t_int','t_long','t_string',
                            'itfc','api','var','const','Fct','Prc',
                            'Base','Param','Prop',
                            'Documents','Doc','drawTiming','drawNode',
                            'Bug','Test','ToDo','Tool',]),
                    ('xMIIWebJavaScript',['t_free','t_array','t_class','t_struct','t_list',
                            't_bool','t_char','t_float','t_int','t_long','t_string',
                            'itfc','api','var','const','Fct','Prc',
                            'func','method',
                            'Base','Param','Prop',
                            'Documents','Doc','drawTiming','drawNode',
                            'Bug','Test','ToDo','Tool',]),
                    ('xMIIWebMisc',['t_free','t_array','t_class','t_struct','t_list',
                            't_bool','t_char','t_float','t_int','t_long','t_string',
                            'itfc','api','var','const',
                            'Base','Param','Prop',
                            'Documents','Doc','drawTiming','drawNode',
                            'Bug','Test',]),
                    ('xMIIWebXml',['t_free','t_array','t_class','t_struct','t_list',
                            't_bool','t_char','t_float','t_int','t_long','t_string',
                            'itfc','api','var','const',
                            'Base','Param','Prop',
                            'Documents','Doc','drawTiming','drawNode',
                            'Bug','Test',]),
                ])
            if oRoot is not None:
                doc.LinkRegisteredNode(oRoot,oBase)
            for sBase in ['admin','instance','research','sw','hw',
                    'typeContainer','typeGrp',
                    'Mod','Base','itfc','api','parameters',
                    'S88_phy','S88_ET','S88_ST','S88_AR','S88_CL','S88_UT','S88_EM','S88_CM',
                    'S88_Prc','S88_PrcS','S88_PrcO','S88_PrcA',
                    'S88_procModel','S88_uproc','S88_proc','S88_op','S88_ph',
                    ]:
                vidarc.vApps.vEngineering.__link__.LinkNodes(doc,[
                    (sBase,['xMII',]),])
            lMII=[  'xMIIMod','xMIIGrp',
                    'xMIIQryAgg','xMIIQryAlm','xMIIQryOLAP','xMIIQrySql',
                    'xMIIQryTag','xMIIQryXac','xMIIQryXml','xMIISvg',
                    'xMIIIumGrid','xMIIIumBrowser','xMIIIumCalendar',
                    'xMIIIumChart','xMIIIumChartSPC','xMIIIumCmd',
                    'xMIIIumTicker','xMIIWebIrpt','xMIITransaction',
                    'xMIIWebHtml','xMIIWebJavaScript','xMIIWebMisc','xMIIWebXml',
                    'drawTiming','drawNode','Prop','dataCollector',
                    'Fct','Prc','Bug','Test','Tool','ToDo',]

            for sBase in ['Mod','xMIIMod','xMIIGrp',]:
                vidarc.vApps.vEngineering.__link__.LinkNodes(doc,[
                    (sBase,lMII),])
    except:
        vtLog.vtLngTB(__name__)
