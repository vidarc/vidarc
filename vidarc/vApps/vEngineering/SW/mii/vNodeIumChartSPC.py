#----------------------------------------------------------------------------
# Name:         vNodeIumChartSPC.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120407
# CVS-ID:       $Id: vNodeIumChartSPC.py,v 1.4 2012/09/09 08:04:27 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeIumChartSPC(vNodeEngBase):
    def __init__(self,tagName='xMIIIumChartSPC'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'xMII iSPCChart')
    def GetTransDescription(self):
        return u'xMII iSPCChart'
    # ---------------------------------------------------------
    # specific
    def GetGrpDict(self):
        return {
            None:0,
            'method':[-1,None,]
            }
    def GetProcessNodeInc(self):
        return [
            {'lTag':['Mod','array','class','struct'],'level':1,'level_max':4,},
            #{'lTag':['Mod','array','class','struct'],'level':1,'headline':u'module'},
            ]
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'dataSrv':[
                    _(u'10 data server'),u'dataSrv',-10,
                    {'val':u'','type':'string',},
                    {'val':u'','type':'string',}],
            u'qryTmpl':[
                    _(u'30 query template'),u'qryTmpl',-30,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02\x8cIDAT8\x8d\xa5\x93\xcdkTW\x18\x87\x9fs\xce\xfd\x98\xafd\x92\xeb$%\
J\x87"\x08\xee\x14i\xd7n\x04]\x17\x8a[7V\xdc\xb8h\x17\xfdXtU\xa8.\xa4t\xa1.\
\xdc\xe8F\x17\xb6\xa5R\xa8\x88\x88XZ\xaa\xa4*bm1\xa5-\r\xd3\xd8Lf\xa2\x99$\
\xf7\x9e{>\xeeqa q\xed\xfb\x07<\xbf\x97\xe7}\x7fBH\xc5\xebL\x04p\xea\xe4\xa7\
abb\x07\xd6\x96\x18\xa3\x11\x02\xbc7\x18c\x00\xf0\xdea\xad\x01*B\xf08\xb7\
\xcah\xf46g\xce\x9e\x10\x11@\xab5\xce\xd1\xa3\xc7\x00\x18\x0cV\xe8t\xdax\x0f\
\xfd\xfe\x90,\xdbFU\xc1\xe2b\x9f,\x9bB)\x01\xc0\xe9\xd3\xe767\x08Al$\x81\xb5\
\x96\xb2\x0cx\x1f\xd0\xda\xa0\xb5\xc5\xb9\x80.\x0cE\xa1\x89T\x0c@6\xf9\xe6&@\
k\xc3\xc2B\x1fk-KK\x03\xb4\xd6x/\x18\x0e\x878W\x11*\xc9h\x90\xd3\x08S\xa8(z\
\x19VV\x9b\x80$I\x99\x99\x99\xa6,\x03\xa0\xe8t\xa6\xa8\xaa\x80@\x90MN\x93\
\xb8\x88^\xfdW\xbe.\xbe\xe2~~\x0b\x1dk\xc2!\x01\x1fm\x00@\xe2=x_\x11\xbc$u\n\
\x05\xac\x84\x16\r"\x1ep\x8b\x1b\xedS\\\xff\xedw\x9a\xb2\xc6X\x9a\xbcz\x05]8\
\xe6\xff}J\xe5=\xa3\xff-\x8f\xb6\x7f\xc9\x95?\xbfa\xdf\xe00\xef\xc6\xefq\xc9\
\x7f\xc8\xec\x83\x05\xea!&\x8d\rQ\xbc\xfa* n\nv\xee\x98!\x18xT\x7f\xc8\xcd\
\xe7\x97\xf9\xf6\xde,\xd7\xd2\x9f9\xbf\xf61Y\xd2\xc6\xd8\x92zR\xd0U\'h,~\xc6\
Bo\x19\xd8\x89\x04p{\xff\xe1\x9d\x9f\xdeb%\x87\xef\xe5\'\xfc\xf0\xcb\x1fL7\
\xc6y\xa3\xd6!\rch\xe7\x88\xe3\x9c\xf7g4\xabs\'\xb9\xf7\xd7\x1a\x9a\xa7[$\
\x9a&C\xd3c\xf7m\xc1\xaem\x13\xac\xbb5\xb2\xa4F\x9c\xe4\xc4\xb4A\xe4\xec\xcf\
\xbe`~\x11\x9e\xf4\xe7\x99lI\x94\xf0\x1b\xf6\x80\xd2\x94lW{h\xcb\x0e\xa9\xdb\
\xc5\xf1\xee\x1d\x0e\xda\x02;w\x91\xb4\xb6L3\xad\xd1Y\xff\x80\xab\xf7{4R\x90\
\x02\x08[\x1c\xe4~\x99#\xdd\xbb|~\xc10hy\xae\x8f\x15<\xf9o\x8efr\x80j\xf6G\
\xc6\xdb\x7f\xf3\xdd\xda\x12`\x89#\x89T\x02\x15\xb6\xfc\x81\xb6\x05\xc4\xcfx\
f\x06TF\xf0|\x19&\xc6\x04J\x8e\x90\xa6K>\xe8\x92&\xeb$\x91@I\x81B\x12K\xb7\t\
\x90\x8f\xe7\xe8\x85\x1bL\xd5\xbbL&\x1aEA,s\x12Q\x92\x8a\x82Hh\xd2J\x93\x90\
\x13\xb9\x92T\x15\xd4\xaa\x97\x12\xc5\xeb\xd6\xf9\x05\xf1e)\xd0\xab\x12+\xda\
\x00\x00\x00\x00IEND\xaeB`\x82' 
