#----------------------------------------------------------------------------
# Name:         vNodeQrySql.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120407
# CVS-ID:       $Id: vNodeQrySql.py,v 1.6 2012/09/09 08:04:27 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeQrySql(vNodeEngBase):
    def __init__(self,tagName='xMIIQrySql'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'xMII SQL query')
    def GetTransDescription(self):
        return u'xMII SQL query'
    # ---------------------------------------------------------
    # specific
    def GetGrpDict(self):
        return {
            None:0,
            'Param':[-1,None,]
            }
    def GetProcessNodeInc(self):
        return [
            {'lTag':['Mod','class','struct'],'level':1,'level_max':4,},
            #{'lTag':['Mod','array','class','struct'],'level':1,'headline':u'module'},
            ]
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'dataSrv':[
                    _(u'10 data server'),u'dataSrv',-10,
                    {'val':u'','type':'string',},
                    {'val':u'','type':'string',}],
            u'qryMode':[
                    _(u'12 query mode'),u'qryMode',-12,
                    {'val':u'FixedQuery','type':'choice','it02':'FixedQuery','it01':'Command','it00':'ColumnList','it03':'FixedQueryWithOutput','it04':'ModeList','it05':'Query','it06':'TableList'},
                    {'val':u'false','type':'choice','it00':'true','it01':'false'}],
            u'rowCnt':[
                    _(u'20 row count'),u'rowCnt',-20,
                    {'val':100,'type':'int',},
                    {'val':100,'type':'int',}],
            u'itvCnt':[
                    _(u'21 intervall count'),u'itvCnt',-21,
                    {'val':1,'type':'int',},
                    {'val':1,'type':'int',}],
            u'duration':[
                    _(u'22 duration'),u'duration',-22,
                    {'val':1,'type':'int',},
                    {'val':1,'type':'int',}],
            u'qryUsedTbl':[
                    _(u'30 database table'),u'qryUsedTbl',-30,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02\xe0IDAT8\x8d\xa5\x92]h[u\x14\xc0\x7f\xff{o\x1a\x9a\xcf6MmZ\xda\xc1`\
e\x9b[;t]\x87+*bYu\xb2nP\n\xfb\x08\xb3\x0f+S*\xb5\xb01\x0b\xc59\xf5A\x0b>\
\x89\xaf\xdd\x83:tBE7G+\xeb\x8a\xac\xee\xa3\xdd\xba-\xf3\xa3.\xa6_\xb7\x1d\
\x9467eIXs\x93\x9b\xfb\xf7A\xd8\xec\x98\xf8\xe0y<\xe7\xf0\xe3\xfc\xce9B(*\
\xff'\x94\x7f+\xb4\xb5\xf9\xe4[\x1d\xf5\xf2\xbf\x00\xda\x93\x92\xed\x87\xddR\
\xd7\x9f&\x1c~\x13U-\x97\xcb\xcb\xe7\xf8\xfc\x0b[<\xa9W<\xae\xd0~\xd8%ggjx\
\xbb\xab\x87T*EII\x10UU9{\xf63\xe2\xf1\xef\xf8\xf24\xab@\xab\x14\x9aw\x07dQQ\
\x98#o\x1cE\xd7u*+\xab\x18\x1d\xbd\x86m\xdb<[\xd7\x8aqo;\xdf|\xda\xbeJ\xeb\
\xe1\x04\xaf4=%\x9b\x9a\xde\xa1\xbaz\x1d\xa6ibY6\xb9\x9cIii)\x03\x03\x03,\
\x19\xcbD\xc6F8\xf6\x92\x8fK\x0b\x16\xa7\xbe\x9f\x10\x0fw\x10\x0ek\xf2\xc5\
\xe7?\xc0\xe7wcf\xb38\x9dN \x8b\xd7\x1b\xe4\xc2\x85!\x12q\x83\xe8\xd4<{_\xde\
\x86\xaf(\xc1\xdc\xcd+\x8f\x14\xf6\xb5\xaar\xf6w?\x9bkkhl\xdcI\x91\xdf\x8fa$\
p\xbb]\xdc\xbay\x9d\xf9\xf9{\xfc\x16\x9bc\xe73\x15\xd4\x07\xd3\x18K\x0b(\xf8\
\x1e]\xe1\xbd\x96n\xae\xdf\x99\xe0\x87\xf3?\xf2\xda\xab\x82\r\x1b6R[\xbb\x85\
\xe9\xe9iF~\xbeL2m\xb2\xa7\xae\x8c\xedA\x83\xd9Y\x9d\xc5t\x16OY\rp\xf1o\xc0\
\xa5\xd1q^\xd8\xba\x9e:\x87\xe4\xe4'\x1fS\x1c,\xa38PL \x10`q)\x8eC\x15\xe4L\
\x1f\xa7~\xd2\xf1\xba]L,(\x14W)\xec?\xb8N~u:&4\x8f\xd7Glr\x8a\xa9\xe8\x04kB\
\r\x1c?\xf9!\xa9T\x12]\x9f\xa3\xb3\xb3\x93T*\xc5\x8d\xdb\xbf\xd2U_\x8f\x91H\
\xe0\xd4\x04~\x8f\x8b\xe1\xe1a\xf2\xd9\x16\xa9y<n\xd4t\x92Xl\n\xb1\xe59\x82\
\xc1\x12N\x9cx\x97h\xf4Ozzz\xe8\xeb\xebc[\xddV\xce}{\x06\xc30\xd8\xd1\xb0\
\x83\x8e\x8e\x0e\n]\x85h\x9a@\xb9\xf5\xc7\x0c\x9a\xd3\xcf\x91C\x07\xf0z\xdcX\
V\x9e\xcd\x9b6a\xdb6\x99L\x86\xc1\xc1A\xae^\x1b%r\xe7\x17\x96\xe2\x06++&\xb6\
m\x93\xcb\xe5\xb1m\x896\x93\x1fblD%\x90_\xc3\xda\xda](\x8a\xc4Q\xe0 \x14\nQU\
UIo\xefG\xf4\xf7\xf7\xd3\xda\xda\x82\xa2(\xdc\xbd\x1b%\x99\xbcOyy\x08!\x1e{\
\xe5=\xcd\x8d\xb2\xbb\xfb}\n\n\n\xc8\xe5r\x08!\xb0,\x0bEQH\xa7\xd38\x1c\x0e2\
\x99\x0c\x00\x91H\x84\xf1\xf1\xe3\xab\x01\xaf\x1f\xcaK\xd3\x84\xc9I\x18\x1b\
\x83\xe6\xe6\xb5TT\x18d\xb3\x16++\x0f\xfe\xf1\xbe %|}F\x15\x7f\x01u!%i\x83\
\x0f)\xb0\x00\x00\x00\x00IEND\xaeB`\x82" 
