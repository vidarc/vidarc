#!/usr/bin/env python
#----------------------------------------------------------------------
#----------------------------------------------------------------------------
# Name:         encode_bitmaps.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120407
# CVS-ID:       $Id: encode_bitmaps.py,v 1.4 2012/09/09 08:04:27 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

command_lines = [
    #"   -u -i -n Application Tools01_16.png images.py",
#    "   -u -i -n Mondrian mondrian.ico images.py",
    "-u -i -n ModuleMii         img/FlowBlockAS_16.png                  images.py",
    "-a -u -n GrpMii            img/Grp_16.png                          images.py",
    "-a -u -n ModMii            img/Mod_16.png                          images.py",
    
    "-a -u -n SvgObj            img/AnimationEditor_16.png              images.py",
    "-a -u -n Transaction       img/TransactionEditor_16.png            images.py",
    
    "-a -u -n QryAggregate      img/IlluminatorAggregateQuery_16.png    images.py",
    "-a -u -n QryAlm            img/IlluminatorAlarmQuery_16.png        images.py",
    "-a -u -n QryOLAP           img/IlluminatorOLAPQuery_16.png         images.py",
    "-a -u -n QrySql            img/IlluminatorSQLQuery_16.png          images.py",
    "-a -u -n QryTag            img/IlluminatorTagQuery_16.png          images.py",
    "-a -u -n QryXac            img/XacuteQuery.png                     images.py",
    "-a -u -n QryXml            img/IlluminatorXMLQuery_16.png          images.py",
    
    "-a -u -n iGrid             img/iGrid.png                           images.py",
    "-a -u -n iTicker           img/iTicker.png                         images.py",
    "-a -u -n iChart            img/iChart.png                          images.py",
    "-a -u -n iSPCChart         img/iChartSPC.png                       images.py",
    "-a -u -n iBrowser          img/iBrowser.png                        images.py",
    "-a -u -n iCmd              img/iCmd.png                            images.py",
    "-a -u -n iCalendar         img/Calendar02_16.png                   images.py",
    
    "-a -u -n FileEmpty         img/FileEmpty_16.png                    images.py",
    "-a -u -n FileText          img/FileText_16.png                     images.py",
    "-a -u -n FileHtml          img/FileHtml_16.png                     images.py",
    "-a -u -n FileXml           img/FileXml_16.png                      images.py",
    #"-u -i -n Splash img/splashEngineering01.png images_splash.py",
    ]
