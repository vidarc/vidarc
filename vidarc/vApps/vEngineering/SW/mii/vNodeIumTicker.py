#----------------------------------------------------------------------------
# Name:         vNodeIumTicker.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120407
# CVS-ID:       $Id: vNodeIumTicker.py,v 1.3 2012/09/09 08:04:27 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeIumTicker(vNodeEngBase):
    def __init__(self,tagName='xMIIIumTicker'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'xMII iTicker')
    def GetTransDescription(self):
        return u'xMII iTicker'
    # ---------------------------------------------------------
    # specific
    def GetGrpDict(self):
        return {
            None:0,
            'method':[-1,None,]
            }
    def GetProcessNodeInc(self):
        return [
            {'lTag':['Mod','array','class','struct'],'level':1,'level_max':4,},
            #{'lTag':['Mod','array','class','struct'],'level':1,'headline':u'module'},
            ]
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'dataSrv':[
                    _(u'10 data server'),u'dataSrv',-10,
                    {'val':u'','type':'string',},
                    {'val':u'','type':'string',}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02\x1fIDAT8\x8d\xa5\x92]H\x93a\x18\x86\xafw\xdf\xdc7\xb7\xf9\xb3\xca5j\
\x88\xd5HC\r\x89\x94\xea\xa8\xec4\x90\x82\x8a\xc2\x93\n\x8b\xc0\x03\xa9\xa3"\
\xa8N\x83\x8a\x82B\x0c:\x08\xa2\xa0\xf2\xa8$SL\x90\xe8 :\xe8g\xd5<p\xe6\xcf\
\x96\x7fK?\xf7\xcd\xf6\xfd\xec\xed(\xc3\xb6E\xd0}\xfa>\\\xef\xfd\xdc\xcf-\
\x84C\xe1\x7f\xe4,\xf4\xd0\xf7\xa2SjZ)\xc3\xc3\xc3\xc4b\xef\xe9\xba\xd3-\xf2\
\xcd9\n\x01\xc6\xc7\xa70\x0c\x89e\t\x92I\xbd\xa0\x83\x82\x80\xe3\'.\x8aD|\
\x92\xa5\xb4\xce\xe3\'\xbdy\x7f\x07\x10\x7ffp\xf3\xc6~Y_w\n\xa7S\xc5VTl\xcb&\
\x95\xfc\xc6\xd7\xd1\x18Y\x14:\xce\x9c]\x01S\x84\xf8m\xe2\xf6\xadcro\xf3yL\
\x87\x97`(L\xa9\x7f\r\xa8~4\xdf&\xca\x02\x95Hm\x0c\x8f\xc7y\xe9c$z9\xef\nM\
\x8dm$\x16Ljk\xaa\xd9\xb8\xde\x8f\xc7S\x8c\xcb\xb7\n\xaf\xa7\x84\xa17\x1f(\
\xdfv\x88\xfa\xda\xad\xf93\xb8~\xed\xa4Lg$U\xe1\xcd8\\E\xa4\x0c(\xf2\xa9\xe0\
tp\xa5\xe3\x00\xdb\x9b[\x18],\xa2\xaeq7\xadG\x1ad\x0e\xc0_^\xcb\xbc\xa5\xe2v\
\xb9\x99\xd2`\xd6\x80{w\xef\xd3\xb6o\'\x07;\xae"\\\x02UX,\x96\xd7\x90\xb5*\
\x96\x1d,\xf7\xc02%\x19K\xb0\x90\x81i\x1d<>\xf8\xf4\xaa\x87\xcaP\x90\xc0\x86\
0\x89y\xc8\xbaT2\xc2\xc64\xec\\\xc0\xcc\xcc\x17*\xaa\x1a\x18\x8c\xcc\xb06X\
\x81f\xc2\x9e\xf6.4]g6\x05I\x03\x14w1\xa1\xa5\x11\xf4\x1f\x89\xdc\x0c\xce]\
\xe8\x14\xe6\xdc(RQ\x88\x8dM3\xa5\x83\xaexI{\x03L\xa4\xa0\xcceb\x9a\x92h\xff\
Czz?\x8b\x1c\x00\xc0\xdbw\x03\x84f_\x82\xb7\x84\xc8\xe4\x1c\xf1\xe4<\x99\xb4\
N\xc6\t\xdfm\x85\xf0\xc8\x03\xfa\x86\xbaW\\!\xa7H\xed\xa7[d]\xf5.\x02[v0\xed\
\xae\xc4Fa\x9d\x15\'2\xf0\x88\xc1\xd7O\xe9\x1f\x88\x8a\xbf\x02~\xe9\xe8\xe1&\
\xe9`5\x96i\xb0\x98\x9e\xe0\xd9\xf3h\xde:\x17\x04\xfc\xab~\x02\n/\xca\x95U\
\x95\x89\xb8\x00\x00\x00\x00IEND\xaeB`\x82' 
