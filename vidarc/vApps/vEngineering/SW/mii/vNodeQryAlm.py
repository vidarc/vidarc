#----------------------------------------------------------------------------
# Name:         vNodeQryAlm.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120407
# CVS-ID:       $Id: vNodeQryAlm.py,v 1.3 2012/09/09 08:04:27 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeQryAlm(vNodeEngBase):
    def __init__(self,tagName='xMIIQryAlm'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'xMII alarm query')
    def GetTransDescription(self):
        return u'xMII alarm query'
    # ---------------------------------------------------------
    # specific
    def GetGrpDict(self):
        return {
            None:0,
            'method':[-1,None,]
            }
    def GetProcessNodeInc(self):
        return [
            {'lTag':['Mod','array','class','struct'],'level':1,'level_max':4,},
            #{'lTag':['Mod','array','class','struct'],'level':1,'headline':u'module'},
            ]
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'dataSrv':[
                    _(u'10 data server'),u'dataSrv',-10,
                    {'val':u'','type':'string',},
                    {'val':u'','type':'string',}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02\x96IDAT8\x8du\x91MkSy\x18\xc5\x7f\xf7\x7fonb\xbd\xbetR\xdb\xa2iQW\
\xc3t6b\xc6\x8d\x9a)\xda\x8f\xa0]\x88\xb6"U\x06\xa7\xc1.FK\xa1\xa3u\x107\xa2\
[\x17~\x81VE\x89\x1f\xc0\xe2\xe0\x0bRARh@\xad%eJ\x86\xf8R\xcb\\o\x9a6\xb9\
\xf7\xcc\xa2\x1b\xa5\xe9\x03gw\x9e\xc3y\xc126\x8d01\x81\x1e=:\xaa\xd3\xa7\
\xb7j#\x8eel\x0c\rn\xf8\x92\xabt\x1a2\x99\x02\xb5\xdaO\x9c\x1d\xf8Y\x8dx@c\
\x81tz\x99T\xca\xb0y\xf3\xbf\x0c\x0e~bq\xf1\x97\x8d\xfe\xd7\x0b\\\xba\x18j\
\xff~p\x9c\x08\xc7\x81\x03\x07\x8a47\xbf\xe5\xf7\xf3}\r]\xac\x13H\xa7\xa1\
\xa3\xc3\xe5\xf9\xf3N\xee\xdcq\xa9TR\x9c9\xb3\xc0\xc2\xc2\x06)\xbe-\xe4\xda5\
T,\xa20\xb4\xb5k\xd7vY\x96\xd1\xcd\x9b?(\x8a\x9a40\xf0\xab.\\\xe8_W\xe8w\x0e\
::\xd6\xb0\xb2\x12\x12EK8N\xc4\x97/\x8b@\x85\x13\'\xfea~\xde\xda8\xc2\xd5\
\xb1P\x87\x0eA\x18Z\x18c\xb3s\xa7G\xadf\xb3{\xf7\x16\xc2\xd0p\xf0`\x99\x96\
\x96y.\xfe\xd1\xaf\x86\x02{\xf7\xae\xc1\xb6E<\x1e\xd2\xdb\xbb\r\xc7\t9yr\x1b\
\x8e\x13\xe1\xba\x01\x83\x83o\x98\x9d\xad}_\x81el\xfe\x1c\ru\xee\x1c\xe4r\t|\
\x7f;MM.\xef\xde\xad\xf0\xe2\x05\xf4\xf7\x1b\xaaUp\x1c\x07\xdb\xf6x\xf0\xa0\
\x95T\xcacuu\x9a{\xf7K\x16\x96\xb1\xb9{\xd7V\xa5\x82\x86\x86\x8e\xab\\\xae\
\xe9\xd9\xb3\x82fg?(\x08V\xf5\xf2\xe5\x9cJ\xa5\xaa\xa6\xa7\x17455\'\xdf\xffO\
\xf9|^\xe3\xe3\xe3\xea\xedE\x8c\x8c\xa0r\xd9Q\xb5\x8an\xdc8\xa5z}Y--[\x94\
\xcd\x9e\x97$\xed\xd9\x93\x92$e\xb3\xbf)\x95j\xd3\xe4\xe4\xa4\x1c\xc7httD}}\
\xc8d\xb3\x16\xc9\xa4E\x10Xl\xda\xe4q\xf9\xf2-\xba\xba\xf6\xf1\xf8\xf1\x13r\
\xb9\x1c\xd5j\x08\xc0\xd7\xaf+\x94J\x1f\xe9\xe9\xe9A\x82\xa5%\x1f\xdf\x8fa\
\x1f;\xc6XggD"\x01O\x9fv\xf1\xfe}\x85\x87\x0f\'hkkgff\x86d2I\x10\x04x\x9eGww\
7\x99\xcca\x8e\x1c9J"\x11\xa7Xt\xb1\x86\x86\\\xb5\xb6637\x17\xe3\xf3\xe7\x1f\
\x19\x1e\xfe\x8b X&\x1e\x8fc\xcc\xdaH\xbe\xef\xe3y\x1e\x00Q\x14a\x8c\xc5\xd4\
\xd4+n\xdf\xbe\xbe\xb6\x02\xc0\xd8\x95\x8c\xf2\xf9\x1d\x14\n\xaf\x89\xc5\x12\
\xd4\xebu\xa2(B\x12\xb1X\x8c0\x0c\x91\x841\x16\x92howy\xf2w\xc1\xfa\x1f\xad\
\x82,\x89\x83\xca8\xe1\x00\x00\x00\x00IEND\xaeB`\x82' 
