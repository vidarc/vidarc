#----------------------------------------------------------------------------
# Name:         vNodeIumCmd.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120407
# CVS-ID:       $Id: vNodeIumCmd.py,v 1.3 2012/09/09 08:04:27 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeIumCmd(vNodeEngBase):
    def __init__(self,tagName='xMIIIumCmd'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'xMII iCommand')
    def GetTransDescription(self):
        return u'xMII iCommand'
    # ---------------------------------------------------------
    # specific
    def GetGrpDict(self):
        return {
            None:0,
            'method':[-1,None,]
            }
    def GetProcessNodeInc(self):
        return [
            {'lTag':['Mod','array','class','struct'],'level':1,'level_max':4,},
            #{'lTag':['Mod','array','class','struct'],'level':1,'headline':u'module'},
            ]
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'dataSrv':[
                    _(u'10 data server'),u'dataSrv',-10,
                    {'val':u'','type':'string',},
                    {'val':u'','type':'string',}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xd5IDAT8\x8d\xa5\x92\xbfk\x14A\x14\xc7\xbf\xbb{k\xc2\x15I\xb3\xe1\
\x8c1!\x88\xb1\x174\x7f\x81V\x82\xa5\x87 \x18D\x8b\xa8\xa0DP#\xd8\xa8h\xae\
\x88!\xfe\x01A\xc5F\x82Z\xa6\xd3?\xc1F!\x17L!)\x02\x91\x93\xe8mvf\xdf\x8f\
\x19\x0b\xe3\xe2\xb1wW\xe8\x83)f\xe6\xcd\xe7}\xbfo^\x10\x84\x11\xfe\'\xc2~\
\x97\xf7\xe6\xef\xfb\xc6\xc2\x8a\xefK\x08\xc2\xa8\xebj4\x9e\xfb\xedm\xef\xd7\
\xd7\xf7\xfc\xea\xea\'\xdf+\xaf\xab\x82\xf9\xbb7|\xbd~\x01\x1b\x1b\xdf\xb0\
\xb5\xf5\x1d##\t\x9e<~\xddUI\xd0\xad\x07\x1f\xde\x7f\xf5\x03\x03U\xa4i\x06f\
\x85s\x8a8\x8e\xb1\xb6\xf6\x00\xcb\xcfV\x82\xbe=x\xf4p\xc9\xd7j\t\xb2\xccB\
\xc4ADAD\xc82\x83\xc9\xc9\xf3\xa5b%\xc0\xf4\xf4Y\xec\xec\xfc\x043\x81\x99\
\xc1,`v0\xc6bt\xf4 \xae\xce\xce\xf8\x9e\x80;\xb7o\xfa$I`\x8cE\x9e\x0bD\x14"\
\x02f\x81\xb5\x8c4\xcd04t\xba\xa3`\xe5\xef\xcd\xc4\xc4\x19\xb4Z?`-AU\x0b\x05\
D\n\xa2\x1c"\x0e\xb5\xda\xa1\xde\x16\xa6\xa6\x8e"\xcb\x0cD\x08D\x04"\x85\xb5\
\x02ksX+\xc82\x03"\xc1\x95\xcbs\x85\x8dB\xc1\xad\xb9Y_\xa9D\xb06\x87\x88\x82\
Y\xf7m\x10\xf2\x9cA\xc4`f\x84\xa1\xc2\xb9#e\x0b\xc3\xc3\'\x91\xa6f?\xd1A\xd5\
\xc1{\x85*\x8a^\xa8\xfe\xfe\x91j\xb5Z\x06\x0c\x0e\x1e\x861\x06"\x02\x11\xc1\
\xeen\x0b\x9b\x9b\x9f\xe1\\\x1bcc\'P\xa9\x1c( q\x1c\x94\x01\xcc{\x08\x82\x10\
\xce\t\x9a\xcd&\x16\x9f^\xea\x18\x98\x99\x8b\r?>~\x0c\xaa\x0ei\x9a\x16\xe7\
\x1d\x93x\xfd\xda\xa2o\xb7?\xe2\xc5\xcbW\x1d\x8f\xffD\xfd\xdc)\x9f\xe7\xc7\
\x11E_\xf0\xe6\xed\xbb\xa0\x04\xf8\x97\xf8\x05i\xa5\x13\xf6\xc8\xb1U\x8f\x00\
\x00\x00\x00IEND\xaeB`\x82' 
