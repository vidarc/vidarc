#----------------------------------------------------------------------------
# Name:         vNodeTransaction.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120407
# CVS-ID:       $Id: vNodeTransaction.py,v 1.5 2012/09/09 08:04:27 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeTransaction(vNodeEngBase):
    def __init__(self,tagName='xMIITransaction'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'xMII transaction')
    def GetTransDescription(self):
        return u'xMII transaction'
    # ---------------------------------------------------------
    # specific
    def GetGrpDict(self):
        return {
            None:0,
            'method':[-1,None,]
            }
    def GetProcessNodeInc(self):
        return [
            {'lTag':['Mod','Prop','docs','doc','itfc','api','var'],'level':1,'level_max':4,},
            #{'lTag':['Mod','array','class','struct'],'level':1,'headline':u'module'},
            ]
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'dataSrv':[
                    _(u'10 data server'),u'dataSrv',-10,
                    {'val':u'','type':'string',},
                    {'val':u'','type':'string',}],
            u'trnUsed':[
                    _(u'30 used'),u'trnUsed',-30,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'trnUsedQry':[
                    _(u'31 query'),u'trnUsedQry',-31,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'trnUsedTrn':[
                    _(u'32 transaction'),u'trnUsedTrn',-32,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'trnUsedXml':[
                    _(u'33 xml'),u'trnUsedXml',-33,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x020IDAT8\x8d\x95\x91KHTq\x18\xc5\x7f\xf7\xde\xe9\x8e\x8e\xdc\xab\xa8%\
\xea\xa6\xb0a\x98@\x14K\\\x08B\xd8\xce\x8d\x19\xb3(\xa2E\x9b|\xed4(-K1\x0c\
\xdbV\n\xa5\xa0\x10\xb9\x8b\x16\xb5\x99\x95(\x82\x89\xc3`\x0f\xc6\x073\xe2 \
\x82\xcc\xc3\xc7uF\xc7\x99\xfbo\xd5\xa48\xa4\x9d\xe5\xf7q\xcew\xcew$IV8+\xba\
\x1e\xdf\x10\xaaz\x99\xe7\xbd\xc3\xd2\x9f\x99\xe5,\xc4\x91\xf7\xef\x84aD\xd1\
u;\xa6\x99\r\x0c\xa7w\xd2i\x0e^\xf4\xdf\x13\r\r\xfd\xa4R\nBHln\x86\xf0z\xa7\
\xe8\xean\x93\x00\xe4\x7f\x91\x9f\xf5\\\x17N\xe7\x1d$)E0\x18da\xe1\x07996jk\
\x1b\xe8y:!N\x15\x88\xc754\xed"YY\xd9D"!|\xbe1\xe2\xf1C"\x91\x10[[s\x99\x7f0\
\xf1\xf1\xb30\xcd\x18~\xff\x07l6\x17n\xf7\x1c.W-\xd5\xd5W\xb1\xdb\xaf \xcb\
\xa0\xaaV4\xcdq\\`x\xa8G4\xb7\xf4I%%\xa5h\xda\x05T\xb5\x0c\xbb\xbd\x94\xc9\
\xc9\xef\xcc\xce.\xa3\xeby\x94\x95\x9d\'\x10\x08\xe2\xf5\xfa\xd9\xdf_\xfc\
\xfb\xc4\xbe\xde\xdb\xa2\xb1q\x10E\x91\x89\xc5\x92\xe4\xe6\xda\x90\xe5s\x84\
\xc3\xdb\xac\xaen\x10\x8d\x1a\xf8|~\xf2\xf3\xf30\x0c\x83\xc1W\xf7\xd35\xa6[\
\x18\x1d\xf9$jj\xae\x91H$\x99\x99\xf9\x05$\xa8\xa8(\xc7jU\t\x87\r\xdc\xeeqB\
\xa1!\xc6\xc6\xb7\xa5\xa3\x91\xe5\'\xdd\x8f\xc4\xd7/S\xa2\xb2\xb2\x1cPXYY\
\xc7\xe3i\xa5\xad\xfd\xa6\x14\x08\xac\x02\x12kk\x1bD\xa3\x81\x13d\x00\xd9\
\xef\xf7\x11\x0c&\xd9\xdd\x05\xc3\xd8\xa7\xb8\xb8\x08\x87\xa3\x95\xb7o\xdaDA\
A\x11\x07\x07\x078\x1c\x97\xa8\xaaj\xc9\xd8T:\xc2\xc3\xce\x01\xd1\xd4\xe4BQd\
\xc0B"!\xb0XR\x84\xc3\x06\xd3\xd3^\n\x0bw\xe8\xe8l?\xe1 \xdd\x82\xd3Y\xce\
\xceN\x02\xd3T\xd1\xf5\x14\xb1\xd8.\xa6i#\x99\x94\xd9\xdb[a\xe0e\xdf\t\xf21\
\x81\xa5\xa5ox<\xcb(\x8aA}\xfd-\xe6\xe7\x7f\xb2\xb88I]\xdd]LS\xcfh\xffX\x84\
\xa3h~\xf0Z\x1c\x1e\xae32:\x90\xf1\xea\xa9\x02\xff\x83\xdf\x89\x89\xdf7\xb7|\
?M\x00\x00\x00\x00IEND\xaeB`\x82' 
