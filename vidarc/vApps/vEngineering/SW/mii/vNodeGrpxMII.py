#----------------------------------------------------------------------------
# Name:         vNodeGrpxMii.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120508
# CVS-ID:       $Id: vNodeGrpxMII.py,v 1.4 2013/01/23 19:06:18 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeGrpxMII(vNodeEngBase):
    def __init__(self,tagName='xMIIGrp'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'xMII grouping')
    def GetTransDescription(self):
        return u'xMII grouping'
    # ---------------------------------------------------------
    # specific
    def GetGrpDict(self):
        return {
            None:0,
            #'xMIIQrySql':0,
            'xMIIGrp':[-1,None,],
            'xMIIQrySql':[-1,None,],
            'xMIIQryAgg':[-1,None,],
            'xMIIQryAlm':[-1,None,],
            'xMIIQryOLAP':[-1,None,],
            'xMIIQrySql':[-1,None,],
            'xMIIQryTag':[-1,None,],
            'xMIIQryXac':[-1,None,],
            'xMIIQryXml':[-1,None,],
            'xMIISvg':[-1,None,],
            'xMIIIumGrid':[-1,None,],
            'xMIIIumBrowser':[-1,None,],
            'xMIIIumCalendar':[-1,None,],
            'xMIIIumChart':[-1,None,],
            'xMIIIumChartSPC':[-1,None,],
            'xMIIIumCmd':[-1,None,],
            'xMIIIumTicker':[-1,None,],
            'xMIIWebIrpt':[-1,None,],
            'xMIIWebXml':[-1,None,],
            'xMIIWebMisc':[-1,None,],
            'xMIIWebHtml':[-1,None,],
            'xMIIWebJavaScript':[-1,None,],
            'xMIITransaction':[-1,None,],
            }
    def GetProcessNodeInc(self):
        return [
            {'lTag':['xMIIMod','xMIIGrp','itfc','api',
                    'xMIIQryAgg','xMIIQryAlm','xMIIQryOLAP','xMIIQrySql',
                    'xMIIQryTag','xMIIQryXac','xMIIQryXml','xMIISvg',
                    'xMIIIumGrid','xMIIIumBrowser','xMIIIumCalendar',
                    'xMIIIumChart','xMIIIumChartSPC','xMIIIumCmd',
                    'xMIIIumTicker','xMIIWebIrpt','xMIITransaction',
                    'xMIIWebXml','xMIIWebMisc','xMIIWebHtml',
                    'xMIIWebJavaScript'],'level':1,'level_max':8,},
            #{'lTag':['Mod','array','class','struct'],'level':1,'headline':u'module'},
            ]
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02EIDAT8\x8d}\x92\xbfO\x13a\x18\xc7?\xef\xf5\xeezpm\xb94M\x80\x06\x18\
\xca@X\x1a\x92\xb2\xb3\x14\xfc\x07\xd4E\x1d\xd4E\x81M\x9d\xd4\x85Y\x9d\xd4\
\x155R\x8c\x02Nl\xda\x84\x9d4\x01\x12\x12Jh\xae4\\\xd3\x810@\x817\xc7\xdd\
\xeb\xa0@B[\xdf\xe4Y\xde|\x9e\xef\xf3\xeb+\x84\x16\xe1\xe6\xfb\xb9\xb2\xac\
\xf6\xf6\xf6\x08\x82\x80\xf3\xf3s\x82  \x9b\xcdr\xfb\xce]q\x93\xd5[\xb2\x81r\
\xb9Loo/\x8e\xe3\xe0\xfb>\xd5j\x95\xf5\xf5\xf5vh{\x01\x80\xa3\xa3#\xa4\x94\
\x04A\xc0\xc9\xc9\t\xba\xde\x1e\xd5\xda}\xe6r9\x0c\xc3\xc0u]\xca\xe52\xb1X\
\x8c\x89\x89\x89\xf6\x95\x84\x16\xe9\x18\xf3\xf3\xf3jnnN\xfd\x8fi\xdb\x01\
\xc0\xca\xf2\x92\x12B\x10\x8f\xc7Y\xfa\xf1]u\xe2:\n\xd8\xb6M\xadV\xe3\xf8\
\xf8\x98X,\xd6\tC\x08-B\xf1\xf7/U\xaf\xd7\xf1}\x1f\xc30\xb8w\xff\x81\x00x\
\xf7\xf6\x8d:;;\xe3\xe5\xab\xd7\x02\xe0\xdbbA\x9d\x9e\x9e\x02044D~rJ\xe8\x00\
J)|\xdf\xc7\xb6mvvvX,,\xa8\x83\x83\x03zzz0M\x93\x17\xcf\x9f\xa9l6K\xa3\xd1 \
\x9dNS\xadV\xe9\xef\xef\xbf\x1eA\x08\x81i\x9aloo\x93L&q]\x17\xcb\xb2H\xa7\
\xd3d2\x19\x1c\xc7ass\x13\xc30\xa8T*\x98\xa6\x89\xa6i\xd7\x02\x9e\xe7q\xd9\
\xc9\xf4\xcc\xac\x18\x1f\x1f\xe7\xe2\xe2\x82R\xa9\xc4\xda\xda\x1aa\x18\x92\
\xcf\xe7\x99\x9e\x99\x15a\x18\xd2\xd5\xd5E\xadV\x03\xfe\x19\xc9q\x1c666H$\
\x12\x00LN\xdd\x12\x00\x85\x85\xafJJ\xc9\xc3G\x8f\xaf,lY\x16\xf5z\x9d\xb1\
\xb1\xb1\xbfK\\]]U\xfb\xfb\xfb\x1c\x1e\x1e"\x84 \x95J\xf1\xe4\xe9t\x8b\xe7/\
\x97\xe8y\x1e\xcdf\x93x<\xce\xc8\xc8\x08\xda\xd6\xd6\x16\xd1h\x94\xe1\xe1aR\
\xa9\x14\x95J\x85\x8f\x1f\xde\xb7\xdc\xfd\xcb\xe7O\xaa\xd1h\xe08\x0e\x03\x03\
\x034\x9bM\x8a\xc5"Z$\x12!\x0cC,\xcb\xc2\xb2,l\xdbFJ\xd9R]J\x89\xae\xebtww\
\x93L&\xaf\xc6\xd5\xfb\xfa\xfap]\x97\xdd\xdd]\x82  \x91H0::\xda"088H\xa9T\
\xc2\xf3<\xa4\x94(\xa5\xc8d2\xfc\x01\xa5E\xe7\x83hJr\x1a\x00\x00\x00\x00IEND\
\xaeB`\x82'
