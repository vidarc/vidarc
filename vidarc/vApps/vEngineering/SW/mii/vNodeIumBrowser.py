#----------------------------------------------------------------------------
# Name:         vNodeIumBrowser.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120407
# CVS-ID:       $Id: vNodeIumBrowser.py,v 1.4 2012/09/09 08:04:27 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeIumBrowser(vNodeEngBase):
    def __init__(self,tagName='xMIIIumBrowser'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'xMII iBrowser')
    def GetTransDescription(self):
        return u'xMII iBrowser'
    # ---------------------------------------------------------
    # specific
    def GetGrpDict(self):
        return {
            None:0,
            'method':[-1,None,]
            }
    def GetProcessNodeInc(self):
        return [
            {'lTag':['Mod','array','class','struct'],'level':1,'level_max':4,},
            #{'lTag':['Mod','array','class','struct'],'level':1,'headline':u'module'},
            ]
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'dataSrv':[
                    _(u'10 data server'),u'dataSrv',-10,
                    {'val':u'','type':'string',},
                    {'val':u'','type':'string',}],
            u'qryTmpl':[
                    _(u'30 query template'),u'qryTmpl',-30,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xb8IDAT8\x8d\xa5\x931LSQ\x14\x86\xbf\xdb\xbe\xb4 E*\x81\x802\x98\
\x98\x18\xe9\xa0BJ\xd4\x85t\xc2A\x17Ypt48\x18G\x9cX\x99\x8d\t\x12\x81\x10\
\xd3\x84\xa4\x93\x8b\x1b\x89\xb1\x1a\xabQ\x13#\x89\x8a.\x856/D\xdb>\xa1y\xda\
r\xdf}\x97\xe1\xe5\xbd\n\x85\x88\xe9IN\xce\x1d\xce\xf9\xce\xf9O\xee\x11"\x14\
\xa6\x153\xfc\x87\x9eu\xf4\xff\x14\x8aIC\x00\x08\x11\n\xa3g\x1d=\x97<zqz\x1d\
^\x96<H0\xc1\xed\x0f@\xa9t4B,\x06mm\xc0_\x12>\x8f~\xd9\x93\xa3\x14H\xe9E\xc7\
i\xb8\x94\xe08?\xb9VH\xec\x05$\x12\x89\xa0\xd8V\x9a\xf4\xc75\x9e\xe7\x8b\x14\
~\x9c\xe3b\\qg\xe4\x14\xf1\xf6\x08Rz\x10\n^nh\xfftU\t3\xdfnQP\xefH]Jct\xc4x\
\x94\xb7\xb8\xbe\x9ca\xb3RoR\xd3\x04\xc8|\x7f\x82Y>\xcf\xe5\xee\t\x86\xc4<C\
\xbdQ\xfaN\xbcfC\xaf\xb2\xf4v\xf1p\x80\xaf1k\xae`D60\xa3i\xde\x17\r\xf2\xdb6\
\xbf*7 \xfe\x907\xeb9\\\x17\\\xb7\x01\x08v \xa5\x17M\xcb\xa2\xbfk\x85\xaf\
\xf6\x0b\xb2\x9f&(\xdauj\xe1\xa7P=\x8b\xd8\x1aFk\xd0\xfa\x00\x80\xe3x\xf1t\
\xfd.\x99\xdc+":\x89U-\xe1\xd6l\xa8\x8cr\xbc\xff1\x17\xce\xec4M\x10H\xf0\xb7\
{\xefJ\x8ac\xdb\x7f(\x97\xb3\xb8\xa6\x05\x9b\x15\xbak&#\xbf\xc7\xb8\x99\x9cD\
\xa9\x7fH\xe8\xed\x8c\xf0l|\x9a\x85\xdc\x03V\xb7\xd6\x88\xb2\xc3\xe0\xc0I\
\xc6S\xd3\x0c\xf4t\x06\x7f\xe3P\x00@_W\x07SW\xef\xa3\xc7<i~W\xa5\x1a\xee\x9b\
\xf0\xaf\xb1\xa5cj\xc5v\x01\xb1\xe0\xc7\x1e5Wz!\x00\x00\x00\x00IEND\xaeB`\
\x82' 
