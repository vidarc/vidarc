#----------------------------------------------------------------------------
# Name:         vNodeMII.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120407
# CVS-ID:       $Id: vNodeMII.py,v 1.2 2012/04/07 21:56:58 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeMII(vNodeEngBase):
    def __init__(self,tagName='xMII'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'SAP xMII')
    def GetTransDescription(self):
        return u'SAP xMII'
    # ---------------------------------------------------------
    # specific
    def GetProcessNodeInc(self):
        return [
            {'lTag':['Mod','array','class','struct'],'level':1,'level_max':4,},
            #{'lTag':['Mod','array','class','struct'],'level':1,'headline':u'module'},
            ]
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02|IDAT8\x8d}\x92]H\x93a\x14\xc7\x7f\xcf\xbbw\x9bcK7\xcdlZ\x16\x8a\xa9\
A\xdf]DT\xf41\x17DA\x10\x11\xe1E\x91\x18e\x89\x14\xd5UW]t\xd1M\x17Q\x10Q \
\x15i\xa9+-\x894\xa1\xa4\x8bJ\xa3\xc2f\x19\xe6\xca\x8f\xc64sM\xb7\xdc\xfbnO\
\x17\n\x91\x9b\x1d8w\xbf\xe7\xcf9\xbf\xf3\x08\xa1\x18\x98Y\xf5\x1e\x8f\xac\
\x19\xb0\xf2.\xb2\x8c\xc8x\x9ce\xf1\xd7\x1cX\x11c\xdf\xde=b&\xab&\xbc\x06\
\xea|\n]E[Yj\x011\x1e\xa7\xab{+\xb7:\xee\'C\x93\x078\xb4\x10\xa1^\xc9\x90YC\
\xd15"\xa1\x10\xe9j8i\x80H\xb6Bk\xcb\x13\xf9\xec\xe3(\xedA\x9d\xe8D\x98\x8d\
\x0e#\xae\x95\xd9\x94\xb8\xb7\'\xac\x80P\x0c\xb3\xf6\xb9\x0f\xe7e\xe5\xbdJ\
\xf9?FI:\x17P\xf7\xe0\x9e\x9c\xc8\n#\x0b\x05\xb5\xf7k\xe4l\\R\x07\x00s\xcc6\
\xf0\xe8\x98U\x13i9\xa9\xb3aS\x0e\xda\xdaZ\xe5\'\xfd\x0b\xd1X\x14\x9bn\xa1lW\
\x99\x008q\xf1\x8e\x8cND\xb8|\xf6\x90\x00\xa8n\xac\x96c\xe2\x17\x8a\xa6P\x9c\
Z\x80\xcb\xe5\x16*\x80n\x88\xd1\xb7\xd2G\xaa5\x85\x9e\xc7\x01n\xd4\xde\x95u\
\xc3\xf3i\x9b\xb7\x1e9\xac\xb1\xe5\xa4U\x96\xae\x8a\xd1m\xf1b\xdbdb\xc0\xeb\
\'\xdf\xbf\x08`\xca\x81\x12WP\xac*\x81\xba\x00K\x02E<\xecK\xc5kY\xcf\x8a\\X\
\xb3\xdcH\xafc\x1b\xf5o\xa38\xfb\xb2\x18k\x0ea\xb0\x1a0L_O\x01\xe8\x1f\xed\
\x07U\'.$U\xe5\xc7\xc5\x91\xd5\n\xbb\x83M<o\x19\xe1ic\x17;\xa5\x87\xaa\x92\
\xf9T\x1d\xae\x14\xf1X\x1cc\x86J\x9f\xdf\xf7W\xe2\\s&\x9d\x8d\xef\xc9\x9c\
\xcc\x01\xc0\xedv\x0b\x80k\xb7=R\x9b\xfcM\xc5\xa1rqeZ\x9ac"\x83\xc1\xf6\x01\
\x9c\xe9\xce)\x89M\x8f\x9ae\xcb\x90\x19o\xc0H\x1aa\xd6\xcd\x1d\xe3\xcc\xd1\
\xd2\xc4\x0f\x03\\\xaf\xf1\xc8\x17~\x13#!I\xa1-\xcc\xe6B\x1b\xc2u\xe9\x99\
\x1cX\xbc\x89l \xe8\x8f1\xd8\xd3\xcb\xa9\xbcW\x9c>v\xf0\x9f\x90\xab7k\xe4\
\x85\xb8\x0b\xab\xcd\x8e5\x02C\xbeQ\n~\xb6\xa0\xfc`!\xe6q\xc8\xb4\xe88l\x1aF\
k\x1a\xc1\xc9\xc4\x01\x02Q\x131\xec8M\x90\xef\xd4I\xb7\xa7\xf0\x8d\\\xd4\n{\
\x07\r\x9f\xbf\xe2\xedT1\xc9qv\xa4\r\xb1\xa1xAB\xc0\xda\xec\x14\xbe\xbci\xe0\
ew\x1e=\x93\x1aY|g\x7f~\x90?R\x8b\xf2\x91V\xef\x96)\x00\x00\x00\x00IEND\xaeB\
`\x82' 
