#----------------------------------------------------------------------------
# Name:         vNodeApi.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120407
# CVS-ID:       $Id: vNodeApi.py,v 1.1 2012/04/07 21:56:57 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeApi(vNodeEngBase):
    def __init__(self,tagName='api'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'software API')
    def GetTransDescription(self):
        return u'API'
    # ---------------------------------------------------------
    # specific
    def GetGrpDict(self):
        return {
            None:0,
            'method':[-1,None,]
            }
    def GetProcessNodeInc(self):
        return [
            {'lTag':['method'],'level':1,},
            #{'lTag':['method'],'level':10,'headline':u'method(s)'},
            ]
    def GetCfgDictOld(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'skipProc':[
                    _(u'90 skip processing'),u'skipProc',-90,
                    {'val':u'false','type':'choice','it00':'true','it01':'false'},
                    {'val':u'false','type':'choice','it00':'true','it01':'false'}],
            u'orderProc':[
                    _(u'91 order processing'),u'orderProc',-91,
                    {'val':u'false','type':'choice','it00':'true','it01':'false'},
                    {'val':u'false','type':'choice','it00':'true','it01':'false'}],
            u'grpProc':[
                    _(u'95 group processing'),u'skipProc',-95,
                    {'val':u'false','type':'choice','it00':'true','it01':'false'},
                    {'val':u'false','type':'choice','it00':'true','it01':'false'}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xfaIDAT8\x8d\x95\x93MkSA\x14\x86\x9f\xb976\xe9-\xb9\x92\xb45\x16SnL\
\xfch6\x11\x14\xd4\x95\x88\x85\x82\xb8(RE\xb4\x82\x9b\xfe\x00\xa1\x9b\x88\
\xe2Z\xa4\x1b\xa1\xe0B\\(\x08E\xbah\r\xe2:\xd6X\xfbEA\xdc\xb4\xb6i\x03\x91\
\xb66M\xd2`>\xa1w\\\xe5\x924\xa6\xd2\x07\xceff\xde\xf7\x9c3g\x06\xa1\xa8\xd4\
\xc6\xe0\xe0\xb0\xec?\x83\xdc\xbf\xde,\x14j\xb8w\xf7\xa1\x0c?\xf0p\x18,\x83\
\xaa\xf8\xcf\xf2\xc2\xa1\x0c\x84PTK\xfc\xe6\xc5+\xbeo\x0bt#@\xb9\xa4\x90I\
\xa7\x89\xc7Sll\xc5E\xd3\n\xaa\xe2/c\xef\x88.\xed\xb2\x94Up\xea]t{}\xa8G\xec\
\x0c\r\xdd&\xd8\x13\x92\xcd\x0cl\xb1\x8f\xe3\xcc\xb4\xb9\xd1\xb4V\xcc=\x13\
\xa1\x08\x90\xa0\xaa\n\x85|\x81\xd8t\xec\xff-\x18\xbaW>\xbd\xe3bu\xc7\xe4\
\xdbz\x01\xd7\xc9\xd3\x94J\n\xbb\x99\x0cK+\x9bl\xffN\x88\xbe\xde\x9b\xf2J\
\xd0\xc9\x93\xd1\xb7\xa2\xc1\x00\xc0\xd0\xbd2\xdc\xdf\x86\xad\xc5N$\xba\xc8\
\x87\x15\xb5\xee\xe0\xc8\xf3\x11yU\x9f\xe5\xf5\xa7"/\'#\xd6\x9e5\x85D.)\x9eM\
\xe6\x9b\x96\x9a\xcdnR\xa9\x98\\p\xc7\t\xdf\xef\xb5\xee\xc4\xaa\xa0\x8a\xa1{\
e\xd5\xb0\xcb\xe3\x97~\x7f\x07.\xb7\x1b\xbb\xc3$\x97X%\xd4)9g\xe8\xcc\xa6|\
\x8c\x8eO\x08\xdb\xfeL\x89\\R\x00\x04{B\xf2\xd6\xc0u\xa2S_\xe9\xf6\xfa(V\x8a\
\xcce\xd7\xc8\xa4\xd3\x04<\x1a\x97<\xbf(\xdf\xe8\x93\r\x06\xb5\xc4\xa6c\x14\
\xf2ETU\x01\t\x02\x81\xb9g\xa2i\xad\x94\xcae\x92\xc9\x8d\xc6\x16j\xe9<f\xc8\
\xb3\xa7\x8es\xd4\xe5\xc2\xe10\xc9\xac\xfd\xe4\xb2O#\xd0\xae0\xb1\xd8Bd~A\
\x1chP\xcb\xe3G\xc3r\xc0\xbf\xc5\xcc\xe79\xa2\xeb\x92\xb1\xa9e\x01\xd4\x7f\
\xa6\x83\xe8h?Az\'E\xe4\x87f\x89\xe1\x1fSh\xc6\xc5\xf3\xd7\xa4\xcf\x99\xe3}t\
\xbe\xee}\xfc\x05\xe3=\xbb\xba\x19\xef\x8b\x8b\x00\x00\x00\x00IEND\xaeB`\x82\
' 
#if 0:
#    _(u'method(s)')