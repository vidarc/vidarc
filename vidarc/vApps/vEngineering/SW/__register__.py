#----------------------------------------------------------------------------
# Name:         __register__.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: __register__.py,v 1.19 2015/03/01 08:26:18 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust

try:
    if vcCust.is2Import(__name__,'__REG_NODE__',True):
        from vidarc.vApps.vEngineering.SW.vNodeSW import vNodeSW
        from vidarc.vApps.vEngineering.SW.vNodeMod import vNodeMod
        from vidarc.vApps.vEngineering.SW.vNodeApi import vNodeApi
        from vidarc.vApps.vEngineering.SW.vNodeVar import vNodeVar
        from vidarc.vApps.vEngineering.SW.vNodeConst import vNodeConst
        from vidarc.vApps.vEngineering.SW.vNodeParam import vNodeParam
        from vidarc.vApps.vEngineering.SW.vNodeArray import vNodeArray
        from vidarc.vApps.vEngineering.SW.vNodeClass import vNodeClass
        from vidarc.vApps.vEngineering.SW.vNodeStruct import vNodeStruct
        from vidarc.vApps.vEngineering.SW.vNodeList import vNodeList
        from vidarc.vApps.vEngineering.SW.vNodeTypeArray import vNodeTypeArray
        from vidarc.vApps.vEngineering.SW.vNodeTypeClass import vNodeTypeClass
        from vidarc.vApps.vEngineering.SW.vNodeTypeStruct import vNodeTypeStruct
        from vidarc.vApps.vEngineering.SW.vNodeTypeList import vNodeTypeList
        from vidarc.vApps.vEngineering.SW.vNodeTypeBool import vNodeTypeBool
        from vidarc.vApps.vEngineering.SW.vNodeTypeChar import vNodeTypeChar
        from vidarc.vApps.vEngineering.SW.vNodeTypeFloat import vNodeTypeFloat
        from vidarc.vApps.vEngineering.SW.vNodeTypeInt import vNodeTypeInt
        from vidarc.vApps.vEngineering.SW.vNodeTypeLong import vNodeTypeLong
        from vidarc.vApps.vEngineering.SW.vNodeTypeString import vNodeTypeString
        from vidarc.vApps.vEngineering.SW.vNodeTypeFree import vNodeTypeFree
        from vidarc.vApps.vEngineering.SW.vNodeMethod import vNodeMethod
        from vidarc.vApps.vEngineering.SW.vNodeFunc import vNodeFunc
        import vidarc.vApps.vEngineering.__link__
except:
    vtLog.vtLngTB('import')

def RegisterNodes(doc):
    try:
        if vcCust.is2Import(__name__,'__REG_NODE__',True):
            oBase=vNodeSW()
            doc.RegisterNode(oBase,False)
            for o in [vNodeParam(),vNodeMod(),vNodeApi(),
                    vNodeVar(),vNodeConst(),
                    vNodeTypeFree(),
                    vNodeArray(),vNodeClass(),vNodeStruct(), vNodeList(), \
                    vNodeTypeArray(),vNodeTypeClass(),vNodeTypeStruct(),vNodeTypeList(), \
                    vNodeTypeBool(),vNodeTypeChar(),vNodeTypeFloat(),vNodeTypeInt(),vNodeTypeLong(), \
                    vNodeTypeString(),vNodeMethod(),vNodeFunc(),]:
                doc.RegisterNode(o,False)
            lTypes=['t_free','array','class','struct','list','Base','Param','Documents',
                'drawTiming','drawNode','Prop','dataCollector','Bug','Test']
            lParamTypes=['Doc','DocRef','t_array','t_class','t_struct','t_list',
                't_bool','t_char','t_float','t_int','t_long','t_string','t_free']
            vidarc.vApps.vEngineering.__link__.LinkNodes(doc,[
                    ('sw',['Mod','Base','Obj','Cps','Struct',
                            'Prd','Res','Sec',
                            'Prc','Fct','sw',
                            'dataCollector',
                            'itfc','api','var','const','Param',
                            'Exp','Bug','Test','Tool','ToDo',]),
                    ('Mod',['Mod','Base','Obj','Cps','Struct',
                            'Dsg','Prd','Res','Sec',
                            'Prc','Fct','func',
                            'dataCollector',
                            'itfc','api','var','const','Param',
                            'Exp','Bug','Test','Tool','ToDo',]),
                    #('Fct',['Mod','Prc','Fct','func','dataCollector','itfc','api','var','const','Param','Bug','Test',]),
                    #('Prc',['Mod','Prc','Fct','func','dataCollector','itfc','api','var','const','Param','Bug','Test',]),
                    ('var',['Documents',]),
                    ('const',['Documents','const']),
                    ('api',['Documents','Param','method','func','Prc','Fct',]),
                    ('Prop',['Documents','Param']+lTypes),
                    ('parameters',['Documents','Param']),
                    ('class',['Documents','method','var','const','Param','Prop','Mod','api','itfc']+lTypes),
                    ('method',['Documents','Doc','Prop','Param','t_free','itfc','api','var','const','Exp','Bug','Test',]),
                    ('func',['Documents','Doc','Prop','Param','t_free','itfc','api','var','const','Exp','Bug','Test',]),
                ])
            for sBase in ['sw','Mod','Prc','Fct','var','const','itfc','api',
                    'parameters']:
                vidarc.vApps.vEngineering.__link__.LinkNodes(doc,[
                        (sBase,lTypes),
                        (sBase,lParamTypes),
                    ])
            for sBase in ['Mod','itfc','api']:
                vidarc.vApps.vEngineering.__link__.LinkNodes(doc,[
                        (sBase,['method','func',]),
                        ])
            for sBase in lTypes:
                vidarc.vApps.vEngineering.__link__.LinkNodes(doc,[
                        (sBase,lParamTypes),
                    ])
            try:
                import vidarc.vApps.vEngineering.SW.python.regPython as regPython
                regPython.RegisterNodes(doc)
            except:
                vtLog.vtLngTB('import')
    except:
        vtLog.vtLngTB(__name__)
