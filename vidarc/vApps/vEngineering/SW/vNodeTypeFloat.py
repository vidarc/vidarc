#----------------------------------------------------------------------------
# Name:         vNodeTypeFloat.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060619
# CVS-ID:       $Id: vNodeTypeFloat.py,v 1.3 2006/09/27 04:20:51 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeTypeFloat(vXmlNodeEngineering):
    def __init__(self,tagName='t_float'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'software float type')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x8eIDAT8\x8d\x9dSI\x12\xc0 \x0c\n\xd1\xff\xffX\xed)\x1dL\xd1\xda\
\xe6\xa48\x10\xb2\x08x\xb1\x88\xd1\xdb\x883\xbc\xc0\x0e\xa22\x91I\x19cq\xc6\
\x01/6z\x1b*\xe3\xceQp`f\x0f\xf2\x89\xa3\xc0\xaa\xca\xaal3\xc6"~B\xce\x0e\
\xf8\xcd\x15\x99\x03^\xb0\x9b\x88l\xe2\xaa\xe3\xca\xad\xe7\x87\xaf\xe1\xa1\
\xb4\x1a\xd9\xdbB\xfdr\xc0Io\x81\x00r\xfd\x8a\xcc\xf7i\x0fV\xb3\xce\xe4\xa9\
\xc4\xd5*\xab\xc9\xa8{e\xfb\xac\xbek\xec\xe4\x9a\xbfs\xae\xf1d7.\xb8\x8e\x99\
G\x94q\xe4\xcf\x00\x00\x00\x00IEND\xaeB`\x82' 
