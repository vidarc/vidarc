#----------------------------------------------------------------------------
# Name:         vNodeTypeLong.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060619
# CVS-ID:       $Id: vNodeTypeLong.py,v 1.3 2006/09/27 04:20:51 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeTypeLong(vXmlNodeEngineering):
    def __init__(self,tagName='t_long'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'software long type')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x95IDAT8\x8d\x95SA\x12\x840\x0c\x82\xa4\xff\xff\xb1v\x0f\x0e\x0e\
\x13\xd3\xd5\xe6d\xc9@\xc0\xb4d$T\xf3<\xa6\xbe\x19I|\xa8\xe1D'U\xcc\xc5\x1d'\
#1\xcfcv\x13\xff9\x12\x87\x00\x1e\xe4/\x8e\x84E7\x95\x91d$\xab\x83\x8a\x01@t\
\xe4.\xb3\x0by/:\xf2N\x85\x14w\xc8\xcey\xfc\x83m\x07RZe\x06\xaex\xab\xfe\xab\
\x83\xd5\xfa4\xf4\x16\x10\xd0\x11:A\xd5\xf0\x83\xafP{\x7fs\xb3\xbc\xca\x15[\
\x9d\x87\xdbw\xf5\xee&v\x91\xe8\xcf\xb9f\xec\xdeB\xed\xfd\x00\xe1\xca\x9d@\
\x98\x95(\x96\x00\x00\x00\x00IEND\xaeB`\x82" 
