#----------------------------------------------------------------------------
# Name:         vNodeArray.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: vNodeArray.py,v 1.3 2006/09/27 04:20:51 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeArray(vXmlNodeEngineering):
    def __init__(self,tagName='array'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'software array')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xb1IDAT8\x8d\x8dS\xd1\n\xc4 \x0cK\xaa\xbf=\x10\xf6\xdd[\xef\xe1\xa8\
vY=. \xb3\xb1\xb6Y\xa8\xa45\x04\xfc\xbe\x1c\t\xb4F\xe5\x83\x0b\xf4\x9c\xa0\
\x87\x15\xaf\\\xaf*k\x92\xdf\x97\xc7^9\x02p\x95\xaaJ~\xa9\xe99\xa8.V~\xe4<{w\
:<V\xc4\xf9Kk\xcc\xa6N\x13\xbf\x07\x87k\xb1:\x1e\x93\xb3J6\xed$\xed\xdc\xfe\
\x0e0f\xd3\xbeO\xaa\x11\x85\xfd\x86\x03\x80-\xf9\xcf!\xfa\x17\x0f\x13W\xf5eb\
\x85\xdct;\x07Z {\xa2\xaa\x9d\xd6\x90\x97r\xc0\xf0\xea\x0c\x80\x93\xd6\xca!R\
n7\xde}I~&\xa8\xb1\xbb)e~\xce\xb9\x93^\xda=\xf5\x0f\xfd\x9e\xb0\xe1\x04\x03\
\x0b\x86\x00\x00\x00\x00IEND\xaeB`\x82' 
