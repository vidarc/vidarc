#----------------------------------------------------------------------------
# Name:         vNodeTypeFree.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120407
# CVS-ID:       $Id: vNodeTypeFree.py,v 1.4 2015/02/27 20:27:35 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeTypeFree(vNodeEngBase):
    def __init__(self,tagName='t_free'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'software free type')
    def GetTransDescription(self):
        return u'type free'
    # ---------------------------------------------------------
    # specific
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'typePrimitive':[
                    _(u'01 type primitive'),u'typePrimitive',-1,
                    {'val':u'int','type':'choice','it00':'int','it01':'char','it02':'float','it03':'long','it04':'bool','it05':'double','it06':'string','it07':'time','it08':'date','it09':'datetime','it10':'uint','it11':'uchar','it12':'ulong'},
                    {'val':u'int','type':'choice','it00':'int','it01':'char','it02':'float','it03':'long','it04':'bool','it05':'double','it06':'string','it07':'time','it08':'date','it09':'datetime','it10':'uint','it11':'uchar','it12':'ulong'},],
            u'typeName':[
                    _(u'10 type name'),u'typeName',-10,
                    {'val':u'','type':'string',},
                    {'val':u'','type':'string',}],
            u'fieldStart':[
                    _(u'15 field start'),u'fieldStart',-15,
                    {'val':u'0','type':'int',},
                    {'val':u'0','type':'int',}],
            u'fieldSize':[
                    _(u'16 field size'),u'fieldSize',-16,
                    {'val':u'0','type':'int',},
                    {'val':u'0','type':'int',}],
            u'iopTag':[
                    _(u'20 IO tag'),u'iopTag',-20,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'iopType':[
                    _(u'21 IO type'),u'iopType',-21,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'iopProp':[
                    _(u'22 IO property'),u'iopProp',-22,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'iopDrv':[
                    _(u'23 IO driver'),u'iopDrv',-23,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'iopAdr':[
                    _(u'24 IO address'),u'iopAdr',-24,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'iopTrc':[
                    _(u'26 IO tolerance'),u'iopTrc',-26,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'iopRate':[
                    _(u'27 IO rate'),u'iopRate',-27,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'priority':[
                    _(u'40 priority'),u'priority',-40,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'valMin':[
                    _(u'50 minimum value'),u'valMin',-50,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'valMax':[
                    _(u'51 maximum value'),u'valMax',-51,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'valDft':[
                    _(u'52 default value'),u'valDft',-52,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'valConst':[
                    _(u'59 constant'),u'valConst',-59,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x9eIDAT8\x8d\x9dSA\x12\xc4 \x08K`\x1f\xce\xc7[\xf7\xe00\x88\xe8\xd6\
.'\x05\x13bDR\x14\x1e\xed\xbe\x9a\xaf)J\x1c\xc4'\x01-\n\xcdz\xce\x89F\xf21O\
\x8a\xf6\xa2\xa1\x86m7\x00\x0c\x14%\x01T\xb0\xd5k\x84\x02K\x07e\xd5\x95\xa2\
\x9c=\x88\\\xee\x96\t\xec\xd9\xbc\x99$\x08\x0e\xc0KB7\xf1-\xd81\xd5\x83\x97!\
\xdd]+\xef|L\xf0\x0f\xc8\xe7\xa6\xddW\x1b\x08\xba\x8a'%s}R`\xf8u\x9d\xd5\xc8\
\xc7(oFu\x96=\xf6\xa2(\xe9\xbfq7\xaa\xab\xe5\x92\xa0\xc8\xc4\xee/\xe4\xda\
\x17\x83\xd2f\xacz)\x91\x98\x00\x00\x00\x00IEND\xaeB`\x82" 
