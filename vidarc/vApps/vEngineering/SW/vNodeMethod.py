#----------------------------------------------------------------------------
# Name:         vNodeMethodPy.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20061118
# CVS-ID:       $Id: vNodeMethod.py,v 1.6 2013/06/03 05:24:57 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeFct import vNodeFct
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeMethod(vNodeFct):
    NODE_ATTRS=vNodeFct.NODE_ATTRS+[
        ('File',None,'fnSrc',None),
        ('Line',None,'line',None),
        ('Module',None,'module',None),
        ]
    def __init__(self,tagName='method'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeFct.__init__(self,tagName)
    def GetDescription(self):
        return _(u'software method')
    def GetTransDescription(self):
        return u'method'
    # ---------------------------------------------------------
    # specific
    def GetFile(self,node):
        return self.Get(node,'fnSrc')
    def GetLine(self,node):
        return self.Get(node,'line')
    def GetModule(self,node):
        return self.GetML(node,'module')
    def GetArgDef(self,node):
        return self.Get(node,'argDef')
    def GetReturn(self,node):
        return self.Get(node,'return')
    def GetExceptions(self,node):
        return self.Get(node,'exceptions')
    def IsSkipProcessing(self,node):
        if self.Get(node,'skipProc') in ['true']:
            return True
        return False
    def GetOrder(self,node):
        return self.Get(node,'order')
    def GetOrderVal(self,node):
        return self.GetVal(node,'order',long,fallback=-1)
    def IsOrderProcessing(self,node):
        if self.Get(node,'orderProc') in ['true']:
            return True
        return False
    def IsGrpProcessing(self,node):
        if self.Get(node,'grpProc') in ['true']:
            return True
        return False
    def SetFile(self,node,val):
        self.Set(node,'fnSrc',val)
    def SetLine(self,node,val):
        self.Set(node,'line',val)
    def SetModule(self,node,val):
        self.Set(node,'module',val)
    def SetArgDef(self,node,val):
        self.Set(node,'argDef',val)
    def SetReturn(self,node,val):
        return self.Set(node,'return',val)
    def SetOrder(self,node,val):
        return self.Set(node,'order',val)
    def SetExceptions(self,node,val):
        return self.Set(node,'exceptions',val)
    def BuildLang(self):
        vNodeFct.BuildLang(self)
        #print self.dCfg
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vNodeFct.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            #u'mod':[
            #        _(u'01 module'),u'mod',-1,
            #        {'val':u'','type':'text'},{'val':u'','type':'text'}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01EIDAT8\x8d\xc5\x93AN\x1bA\x10E_\x8fA\xc8Afa\xc9c\xc8\x92\x04\t\x82\
\xc4\x8e\x9c\x83\x05\xc7@p\x05\xb69\x007`\x99\xbb\x84U\x12n\x80-\xb0#\xc03\
\xdd]\xd5U\xec\x90\x1c\xc7\x08\t\xa4\xd4\xb6\xf4\x9f\xf4\x7f\xfd\n\xa1\xea\
\xf0\x96\xa9\xde\xa4~\x0f\xc0\xca\xb2\xc5\xe1\xf9\xae\xc7\xfe\x1f\xa2\n\xab\
\xa3\x01?\xbf\xfd\x0e\xaf\x06\x1c]\xed\xf8\xe6Z\x9f&\xae\xf3\xd0\xb4L\xdbG\
\xc6\x87\x95\xdf\x1e\xdb\x02d\xc1\xc2\xd1\x8f\x1d?\xe8}\xa2\xca\x81\x92\x0cI\
\x05O\x81\xcf\x9d!\xbdK\xf3\x17\x01_\xcf\xf7\xfdcw\xc0\xdd\xec\x01E\xb1`P9\
\x16\n\xad\x08\x83\x95\x1e\xfb\xa7{s\x909\x0bM\x7fB\x13\xbbH,X0b\xce$U\xa4\
\x14\xd4\x157c\xb4>Z\x9eA\x12\xe1\xbei\x90dx0\x92*m\xce$\x11\xb2\x16rQ\xb0\
\x17B\\\x1b\xd7L\xdb\x19\x1e\xc1*CT\x89"49\x93\x92\xd0\xba\xb05\x1br\xcb\xe4\
Y\x13\xfenb\xfd\xbd\xf2\xed\xce\x90\xa8\x82z!\x89\x92\x92\x90T\xb8\xc9\x99p\
\xd2\x99\xbb\xc4\x02\x00\xa0wY\xbc^\xdd\xc0\x8a\x91\xb5\xd0j\xe6\xae)\x0b\
\xe2\xa5\x00\x80/g\xbb>\xfe0\xc6\xcd\xa9\x1fk~]\\\xff\xb3HK\x01\xaf\x9d\xff\
\xffLO=-\xb2L\xb1q\xa7\xf6\x00\x00\x00\x00IEND\xaeB`\x82' 
