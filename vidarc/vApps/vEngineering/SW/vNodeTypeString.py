#----------------------------------------------------------------------------
# Name:         vNodeTypeString.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060619
# CVS-ID:       $Id: vNodeTypeString.py,v 1.3 2006/09/27 04:20:51 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeTypeString(vXmlNodeEngineering):
    def __init__(self,tagName='t_string'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'software string type')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x91IDAT8\x8d\x8dSA\x0e\xc0 \x08\x03\xf4\xff?Vv\xea\xd2\xb0\xca$Y\
\xa2`K+\xce=\x86!r\xaf\xc4\xdac\xb8]\xc4d \x83j\x8e\xc99\xef\x1e\xc3r\xafT\
\x1d;E\xc0\xb8\x99}\xc07\x8a\x90\x9b\xaa\xabR\xc3V\xb8\x1e\x1d8\xf7J|L\xc4\
\xfb\xe8\xc0U\x85\x8a\xe3%*\xcfJm\xa8"\x03\xff\x94\x04\x98\xd4\xc1\x1b\x92\
\xcf\x14XbWC\xd3\xd7\x02\x12\x9d\x12E<\xd5\xc1\xbf\x97\xc9\xb5v\n\xb5\xb3\
\xdaO\x96\xcf\xec\xf5bO\xe3t\xfe\x9d\xab\xc7\xce;j\x0f\xd3\xc4\xa9&\x84\xb9y\
\x94\x00\x00\x00\x00IEND\xaeB`\x82' 
