#----------------------------------------------------------------------------
# Name:         vNodeConst.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120407
# CVS-ID:       $Id: vNodeConst.py,v 1.3 2013/01/11 06:36:06 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeConst(vNodeEngBase):
    def __init__(self,tagName='const'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'software constants')
    def GetTransDescription(self):
        return u'constants'
    # ---------------------------------------------------------
    # specific
    def GetGrpDict(self):
        return {
            None:0,
            'method':[-1,None,]
            }
    def GetProcessNodeInc(self):
        return [
            {'lTag':['method'],'level':1,},
            #{'lTag':['method'],'level':10,'headline':u'method(s)'},
            ]
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'typePrimitive':[
                    _(u'01 type primitive'),u'typePrimitive',-1,
                    {'val':u'int','type':'choice','it00':'int','it01':'char','it02':'float','it03':'long','it04':'bool','it05':'double','it06':'string','it07':'time','it08':'date','it09':'datetime','it10':'uint','it11':'uchar','it12':'ulong'},
                    {'val':u'int','type':'choice','it00':'int','it01':'char','it02':'float','it03':'long','it04':'bool','it05':'double','it06':'string','it07':'time','it08':'date','it09':'datetime','it10':'uint','it11':'uchar','it12':'ulong'},],
            u'typeName':[
                    _(u'10 type name'),u'typeName',-10,
                    {'val':u'','type':'string',},
                    {'val':u'','type':'string',}],
            u'valConst':[
                    _(u'59 constant'),u'valDft',-59,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02tIDAT8\x8d\x9d\x93\xcdK\xd3q\x1c\xc7_\xdf\xdf\xc3\xa6\xd6l\x05Z\xa6k\
Ybe%b\x05\xd2\x03\x85`]*\xa1\xe8\x1a\x04\x9d*\xea\xd2\xa5c\x87:\xd4\xa1S\xd0\
?\x10F\xa7\xa0\x82\x88N\xd6%\x90\x88 \xd9|\xcaY\xe8\xa6\xdbt\xf3\xe7\xe6\xef\
\xb7\xefC\x07\xc9\x9cv\xea}\xfb\xc0\xe7\xf5\xe6\xf3(\x84e\xb3^SO.\x1a\x99\
\x19\xc62\xa0\x9d0n\xd3Ab\xb7^\x8a\r\x89\x80Xk\xf0\xe3\xe1\x19S\'=B\x91z\xdc\
H\x14\xb4\x02\xdb\xa6R,P.\xe4P\r\xfb\x89\xdd|!\xfei0~\'f\xa2\xcdqL~\x96b&\
\x8b\x1f(*R\xe3:\x16\xe1\x90M\xb4\xfd\x00\x12M1\x9f\xa3\xed\xf1\x88\xa82H\
\xde\xdee\xb65\xec\xc0\x9fN\x91_\x12L_x\x84\xef\xd4a;.a\xa1\xd9\xf2\xec\x12\
\x1d\xd7\xee\xb0\xf0\xe1\x05v\xe3N\x96pi\xbb\xffY\x008\x005N\x08\x95\xcf\xb0\
\xa0\xea)_\x7f\xc6\xbe\x96\xdd\xc4\xe3q\x00\x86\xfa]\xf6\x9c\xed#7\xf8\x1aQ\
\x13\xc1..\x80\x94\xab-X\x89\xbb\x9d\xc6\t\x85\xc8g\xf2T\xae<\xa2q\x1d\xdc\
\xda{\x1ao$\tF\x93\x1e\x1d#\x9f\x9e\xc3\t\xd72\xf6\xe0\x9c\x01\xb0\xd4\xecOd\
\xb9\x8c\xdc\xb4\x83\xe8\xde\xce*x\xf7\xc9\xe3x\xc9$Fk2\x13\x93\xa4\xae\x0e\
\xe0\x07\x1a\x13\x04\x94S\xdfW*\xd0\xc6\xe0/z\x84\x9b\xda\xaa\xe0X\xcfQ\x8a#\
\t\x8cV\xccM\xfd"uu\x80\xd6\xd6Vpk\t\x16\x97\x90\xa5\xd2\x8a\x81\xf4%Rj\xa47\
\xbf\n7\x1f\xe9\xc4K$@)\xb23s\xabp\xd7\xa1\x0etP\xa6\xa2\x14fe|X\xd2\xaa\xc5\
h\x85?\xf9\x8d\xa1~\x97\x9d]\x1dx\x89$Z)\xf2\xb9\xc2*\xdc\xdd}\x84\xe2\x97\
\xf7\x08\x0cJ*\x9cm-\x7f\xd7\xf8\xf5r\x9d\xa9\x8d\xb7\xd3x\xe80\x99\xb7\xcf\
\xc1\xb2)\x96e\x15,\x8bY\x12\xf7z\xf1\xd3\xe3\x80\xcd\xb1W\xa5\xbfk\xf4\x96-\
D*\xc1\xe8x\x82\xcd\x96\xc6\xf3M\x15\xec\r\x7fb\xf2\xe9\r\x96g\xc6q,A\xe0\
\xd6\x03\xa5\xeaK\x1c\xec\x0b\x99H\x9dA"\x10\x96C\xb8i/nt;~z\x82`>\rZ"\x94\
\xa1,]N\xbd+U_\xe2\x1f}<\xbf\xd58\xc1"\xe1\x1a\x81\x10`\x84@\x18\x83\xd1\xb0\
T2\xb8\x8d\xcd\xf4\x0c\xa4\xaa~\xc1Z\x1b\x9cz3/*\xfbN\xb0\xa8#d\xb3\x9a\xf9\
\xac"\x973\x14\xd8\x82w\xb8o\x03\xbc\xa1\x82\xff\xd1o\xe3\x9c+\xd2\xff\x91\
\xef\x1c\x00\x00\x00\x00IEND\xaeB`\x82' 
#if 0:
#    _(u'method(s)')