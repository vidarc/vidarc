#----------------------------------------------------------------------------
# Name:         vNodeTypeClass.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: vNodeTypeClass.py,v 1.3 2006/09/27 04:20:51 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeTypeClass(vXmlNodeEngineering):
    def __init__(self,tagName='t_class'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'software class type')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x9fIDAT8\x8d\x8dS\xd1\x0e\xc40\x08B\xdb\xff\xff\xe3\xcd{\xb8xG\x89d\
k\xb2\xa4\x03\xb5PmD.\xf4\xaa\xfb\xaa\xdeG\xae\xc0\x8b\xb59\x91\x93\x14\xe3\
\xe2\x8co\x05\\\xa2*\xaa\xfb\xaa\xc8\x15\x01\xa08\xd8\xa9P\xae\xf9\xd4\x8a\
\xce\xab\xe3\x0e\x0bN\xae*\xe2\x98\x9cH\xbd0\x87u%\x00(|\xef\xe2\xf8g|\x8a\
\x89\\\x7f\x05\xeaWm\xb8v\xa6\x93;\xe1\xaeH=\xc9\x9e\xbe\xe6\x7f\x16Xr\xef\
\xd9\xca\xc4\x03\xd4F%\xde\xe0\x00\x90\xdc\xff\xc3\x97\xfa\xec\xd1\x95\xa9\
\xdd}\x82\x0e\x91\x16\xb6\xea\xf89\xeb\xc9\xd3\xbbP\xee\x03'\xb9\x8f\x94ky\
\xf1\xd4\x00\x00\x00\x00IEND\xaeB`\x82" 
