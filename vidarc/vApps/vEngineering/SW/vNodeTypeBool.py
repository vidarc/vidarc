#----------------------------------------------------------------------------
# Name:         vNodeTypeBool.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060619
# CVS-ID:       $Id: vNodeTypeBool.py,v 1.3 2006/09/27 04:20:51 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeTypeBool(vXmlNodeEngineering):
    def __init__(self,tagName='t_bool'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'software boolean type')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x93IDAT8\x8d\x8dS\xc1\x16\x800\x08\x12\xdd\xff\xffq\xad\x93=#\xb4<5\
\x1a\x88\xf8\x06xX\xd6>\x8f\x9d\xdf\xf0\x80\xfd\xa8U\x89\x95\xc4X\x15\xaf8\
\xe0a\xfb<\xb6\xea89J\x0e\xcc\xecE\xfe\xe3(1W]\x95mx\x00\x1e\xe0Q\xbc#w\xc5"\
\xb7\xc0\x94\x03w}\x08v!*\x12\xcf\x0f\x0f\xbc2\x98H\xaa<\x95&\x9b\xa3\xc0\
\xd7\x05%\\\x9b\xde\x02\t\xa8}\xd73\x0b\xae\xeeR\xee]\xb9\xa9\xf8\xb8\x05\
\x95:\x9fW\xb5_\xd59\xd8n\x1b\xa8\xcf\x99gTo\x81\xff]\xfdA\x9d>h\xb7\xd1\x86\
\x00\x00\x00\x00IEND\xaeB`\x82' 
