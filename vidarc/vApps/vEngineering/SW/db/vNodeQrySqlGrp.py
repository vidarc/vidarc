#----------------------------------------------------------------------------
# Name:         vNodeQrySqlGrp.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20130110
# CVS-ID:       $Id: vNodeQrySqlGrp.py,v 1.2 2013/01/10 22:25:23 wal Exp $
# Copyright:    (c) 2013 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import vLang
    
    from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
    
    import vidarc.config.vcCust as vcCust
    if vcCust.is2Import(__name__):
        GUI=1
        vpc.logDebug('GUI objects imported',__name__)
    else:
        GUI=0
        vpc.logDebug('GUI objects import skipped',__name__)
except:
    vpc.logTB(__name__)
    GUI=0
vpc.logDebug('import;done',__name__)

class vNodeQrySqlGrp(vNodeEngBase):
    def __init__(self,tagName='dbQrySqlGrp'):
        global _
        _=vLang.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'database SQL query group')
    def GetTransDescription(self):
        return u'database SQL query group'
    # ---------------------------------------------------------
    # specific
    def GetGrpDict(self):
        return {
            None:0,
            'dbTblsGrp':[-1,None,],
            'dbTblGrp':[-1,None,],
            'dbViewGrp':[-1,None,],
            'dbQrySqlGrp':[-1,None,],
            'dbTbls':[-1,None,],
            'dbTbl':[-1,None,],
            'dbView':[-1,None,],
            'dbQrySql':[-1,None,],
            'Mod':[-1,None,],
            'Base':[-1,None,],
            'Param':[-1,None,],
            'Prop':[-1,None,],
            'itfc':[-1,None,],
            'api':[-1,None,],
            'var':[-1,None,],
            'const':[-1,None,],
            }
    def GetProcessNodeInc(self):
        return [
            {
            'lTag':['dbTblsGrp','dbTbls','dbTblGrp','dbTbl',
                    'dbView','dbViewGrp','dbQrySql','dbQrySqlGrp',
                    'Mod','itfc','api',],
            'level':1,'level_max':4,},
            #{'lTag':['Mod','array','class','struct'],'level':1,'headline':u'module'},
            ]
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'dataSrv':[
                    _(u'10 data server'),u'dataSrv',-10,
                    {'val':u'','type':'string',},
                    {'val':u'','type':'string',}],
            u'qryMode':[
                    _(u'12 query mode'),u'qryMode',-12,
                    {'val':u'FixedQuery','type':'choice','it02':'FixedQuery','it01':'Command','it00':'ColumnList','it03':'FixedQueryWithOutput','it04':'ModeList','it05':'Query','it06':'TableList'},
                    {'val':u'false','type':'choice','it00':'true','it01':'false'}],
            u'rowCnt':[
                    _(u'20 row count'),u'rowCnt',-20,
                    {'val':100,'type':'int',},
                    {'val':100,'type':'int',}],
            u'itvCnt':[
                    _(u'21 intervall count'),u'itvCnt',-21,
                    {'val':1,'type':'int',},
                    {'val':1,'type':'int',}],
            u'duration':[
                    _(u'22 duration'),u'duration',-22,
                    {'val':1,'type':'int',},
                    {'val':1,'type':'int',}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xa3IDAT8\x8d\xad\x93\xbfk\x14A\x14\xc7?\xb33\xbb\xb7\xb7\xb7\xd9\
\xf3\xd2\x88\xa6P\xf0\x1f\x08\xa4\xb2K\x95H\x8cnH\x13S\xa5H\n\x11\xb1\x14D\
\xac\xd2\xa6\xb1\xd8ta\x0b\t! Fa\x91pU@\x0e\xf4\xe4L\xc8\x15ir`~UJL0x\x9a\
\xbb\xdd\xb1X\xb3\x10H\x95\xf5U\xef\xfbf\xe6\xbd\xcfw\x98\x11\xc2\x90\xe4\tu\
\x96\xbc}\xffQ\xe7j\x000x\xfb\x16I\x92\xe0\x95{\xb3ZTm0:4p\xe1\xe1\xa8\xda@\
\x04\xa1\x7f\xa9\xc9\x0f\xa7V\x88\xaa\x8d\x94\xe0\xce\xe8\x8dla5\xdaa\xb0\
\xffQ\xa6\xd76\xe6\x99\x1c\x7f\x91\xe9\xdd\xaf\xfb\xd4\xbe\x84\xfc8\xfa\x05p\
y\x82I?\xe4C\xbd\x95\x12\xdc\x1dz\x80]\xb0Q\xaa\xcc\xf2\xcaK\xc6F\x9e\xe0\
\xd8\xbdH\xa3\xc0\xab\xd7\xcf\x18\x1b~\x8a\x90\x16E\xcb\xa5\xd5Z\xe7Ss\x99\
\x04\x0b\xf8w\x89\x96*a\x9a\x0eJz\xa4\xba\x07\xbbP\x01QH\xb5\xe9"\xa4\x85TE\
\x94\xe9\x00`p\x9a\xcf\xc2\x84\xbfH\xad\xbe\x95\x12\xdc\x1b\x9e\xc1\xb6\x1c\
\x94Yf\xe9\xcd,\xe3#\xcfq\xdd\xab\x98\xb2\xc4\xc2\xd24\x13\xf7\xe70U\x11aH\
\xf6\xb67Y\xdb\x98\xcf\x1a)\x00\xb7t\x8d\xa2\xed\xa1d\x05\x00\xcf\xbb\x8ec\
\xf7e\x9b*Wnb\x18\xe9\x93q\xad\x83\xffk\x81 \xf4\xf5\xd1qS\xff>=\xd0\xdd\xf8\
D\x07\xa1\xaf\xdb\xedo\xfa,\x82\xd0\xd7q\x92d\xba\xb9\xb3\xaf\x83\xd0\xd7\
\xdf\x0fO\xf4\xbb\xd5\xcf\xda\x00\xe8tb\xba\x9d.q\x9cb\xfdiw\xceM\xfby\xdc\
\xcerq\x98\x02\xd7\xea[\xf9,<\x9e\x8e\x04\x80\xc8\xfb\x9d\xff\x02\x1a^\xa0\
\x952\x02C\xa2\x00\x00\x00\x00IEND\xaeB`\x82' 
