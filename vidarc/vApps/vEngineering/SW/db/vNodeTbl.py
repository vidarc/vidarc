#----------------------------------------------------------------------------
# Name:         vNodeTbl.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120407
# CVS-ID:       $Id: vNodeTbl.py,v 1.6 2014/03/30 14:23:31 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import vLang
    
    from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
    
    import vidarc.config.vcCust as vcCust
    if vcCust.is2Import(__name__):
        GUI=1
        vpc.logDebug('GUI objects imported',__name__)
    else:
        GUI=0
        vpc.logDebug('GUI objects import skipped',__name__)
except:
    vpc.logTB(__name__)
    GUI=0
vpc.logDebug('import;done',__name__)

class vNodeTbl(vNodeEngBase):
    def __init__(self,tagName='dbTbl'):
        global _
        _=vLang.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'database table')
    def GetTransDescription(self):
        return u'database table'
    # ---------------------------------------------------------
    # specific
    def GetGrpDict(self):
        return {
            None:0,
            'Base':[-1,None,],
            'Param':[-1,None,],
            'Prop':[-1,None,],
            'itfc':[-1,None,],
            'api':[-1,None,],
            'var':[-1,None,],
            't_free':[-1,None,],
            }
    def GetProcessNodeInc(self):
        return [
            {
            'lTag':['Mod','itfc','api',],
            'level':1,'level_max':4,},
            #{'lTag':['Mod','array','class','struct'],'level':1,'headline':u'module'},
            ]
    def GetCfgDictOld(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'dataSrv':[
                    _(u'10 data server'),u'dataSrv',-10,
                    {'val':u'','type':'string',},
                    {'val':u'','type':'string',}],
            u'qryMode':[
                    _(u'12 query mode'),u'qryMode',-12,
                    {'val':u'FixedQuery','type':'choice','it02':'FixedQuery','it01':'Command','it00':'ColumnList','it03':'FixedQueryWithOutput','it04':'ModeList','it05':'Query','it06':'TableList'},
                    {'val':u'false','type':'choice','it00':'true','it01':'false'}],
            u'rowCnt':[
                    _(u'20 row count'),u'rowCnt',-20,
                    {'val':100,'type':'int',},
                    {'val':100,'type':'int',}],
            u'itvCnt':[
                    _(u'21 intervall count'),u'itvCnt',-21,
                    {'val':1,'type':'int',},
                    {'val':1,'type':'int',}],
            u'duration':[
                    _(u'22 duration'),u'duration',-22,
                    {'val':1,'type':'int',},
                    {'val':1,'type':'int',}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x01\xb5IDAT(\x91\x8d\x92\xbbnSa\x10\x84\xbf\xfd}\x88\x03\xc61\xc4\xe6"Q\x90\
\x80\x00\x03E\x80\x8a\x8e\n!A\xc5K\xd1\xf3\x00\xf0\x02t\xbc\x01-\x12\x17\t%@\
\x8c\x90\x88\xecH\xce\x85\xc4DI||\xce\xeeP\xd8\x8e"\x94\x82)V\xa3\xa9\xf6\
\x1b\x8d\x9dy\xd5\xbdp\xa7\xa9d-\xe2\xcb\xbb_\x97\x1f-"\xaej\xbf\xf7\xf5a{\
\xe9zJ3l\xa6\x95\xee\xf2\xed\xa5\xb6\xc9\xee\xcd>\xc8\x0e\n[\xabTIv(\x07\xba\
\x95*P\xf5\x02\xe8\xd3KTO\xc7Y\xa0o\xebIi\xc0N\x86$\x81\x08\t\x90\x00B\x06\
\x84\x02\x85\xa4\xa9GR\x86\x19\x06\x86a\xc0\xe4\x18\x02\x9b\x8ac>\xa3l\xe0\
\x86\xc8U\x01p\x10\x05\xb5\n\x98o%\xcd\xca\x0b \xf9\xb6E\x8a\x18X\xeb\xe5\
\x8bA\xfe\x9c\xff\x96\xdd|\xd3\x9a_hx*\x9bv\xf1\xfb\xe7\xdd\xe6R\x9eee\xffC\
\xad7\xbf:Z\x04qw\x97\xd5\xf7\xdf\x8a\xc7m\x0b\x9e\x9d"+\xcaF\x1es\x8e\xe7V\
\x87\xdd<\x1a\xa5\x97P&\xed\x11uD\xf8\x1e@@\x101\xcc\xec\x98\x8e\xe0\xc6\xdc\
\xd3\x02&e\x98afYDx\xb8\xcb#\x05\xe0\xee\xe0@x\xe0"&ESF\x88"\xc9\x16_\xdf8\
\x11\xae\xbb\xf5\xf6\xc4<K5\xce_;G%jE}m\xb97\x7f\x7f\xcel\xb4\xfdq8s\xeb\xca\
p\xa1N\xe2Rgc\xf3\xc7\xefx\xd2N\xce\xd3,\x1f\xbfT\xca\xc2\xe5@\x19\xa5Y\tx\
\xc8\x03\xc4\xf8#w\xc2)\x92&\xd0\x9c\x00=]\xc0?\xd0:&`2\xac\xb1\x11G\xebB\
\x8c\x83\xecpcd\xb6Ob\xa8\x00\xf6~\x1e\x9a\xe50\xe3\x9duv\x0e\xb0\xf8\xb35\
\x00\xe3SO\xa2S\xd3_\xb3\xd5\x04\xe0\x82q\x8a\x8c\x00\x00\x00\x00IEND\xaeB`\
\x82' 
