#----------------------------------------------------------------------------
# Name:         regMII.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20120407
# CVS-ID:       $Id: regDB.py,v 1.6 2014/03/30 14:23:01 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust

try:
    if vcCust.is2Import(__name__,'__REG_NODE__',True):
        from vidarc.vApps.vEngineering.SW.db.vNodeDB import vNodeDB
        from vidarc.vApps.vEngineering.SW.db.vNodeTbls import vNodeTbls
        from vidarc.vApps.vEngineering.SW.db.vNodeTblsGrp import vNodeTblsGrp
        from vidarc.vApps.vEngineering.SW.db.vNodeTbl import vNodeTbl
        from vidarc.vApps.vEngineering.SW.db.vNodeTblGrp import vNodeTblGrp
        from vidarc.vApps.vEngineering.SW.db.vNodeView import vNodeView
        from vidarc.vApps.vEngineering.SW.db.vNodeViewGrp import vNodeViewGrp
        from vidarc.vApps.vEngineering.SW.db.vNodeQrySql import vNodeQrySql
        from vidarc.vApps.vEngineering.SW.db.vNodeQrySqlGrp import vNodeQrySqlGrp
        
        import vidarc.vApps.vEngineering.__link__
except:
    vtLog.vtLngTB('import')

def RegisterNodes(doc,oRoot=None):
    try:
        if vcCust.is2Import(__name__,'__REG_NODE__',True):
            oBase=vNodeDB()
            doc.RegisterNode(oBase,True)
            for o in [
                    vNodeTbls(),vNodeTblsGrp(),
                    vNodeTbl(),vNodeTblGrp(),
                    vNodeView(),vNodeViewGrp(),
                    vNodeQrySql(),vNodeQrySqlGrp(),
                    ]:
                doc.RegisterNode(o,False)
            lGrp=['Mod','Base','Param','Prop','itfc','api','var','const','account',
                    'Documents','Doc','drawTiming','drawNode',
                    'Fct','Prc','Bug','Test','Tool','ToDo',]
            vidarc.vApps.vEngineering.__link__.LinkNodes(doc,[
                    ('database',['dbTblsGrp','dbTbls','dbTblGrp','dbTbl',
                        'dbView','dbViewGrp','dbQrySql','dbQrySqlGrp',
                        'hw','sw','dataCollector',]+lGrp),
                    ('dbTbl',['t_free','t_array','t_class','t_struct','t_list',
                            't_bool','t_char','t_float','t_int','t_long','t_string',
                            ]+lGrp),
                    ('dbView',['t_free','t_array','t_class','t_struct','t_list',
                            't_bool','t_char','t_float','t_int','t_long','t_string',
                            'dbTbl','dbTbls','dbTblGrp','dbTblsGrp',
                            'dbView','dbViewGrp','dbQrySql','dbQrySqlGrp',]+lGrp),
                    ('dbQrySql',['t_free','t_array','t_class','t_struct','t_list',
                            't_bool','t_char','t_float','t_int','t_long','t_string',
                            'dbTbl','dbTbls','dbTblGrp','dbTblsGrp',
                            'dbView','dbViewGrp','dbQrySql','dbQrySqlGrp',]+lGrp),
                    ('dbTbls',['dbTblGrp','dbTbl','dbTblsGrp','dbTbls',
                            'dbViewGrp','dbView','dbQrySqlGrp','dbQrySql',]+lGrp),
                    ('dbTblsGrp',['dbTblGrp','dbTbl','dbTblsGrp','dbTbls',
                            'dbViewGrp','dbView','dbQrySqlGrp','dbQrySql',]+lGrp),
                    ('dbTblGrp',['dbTblGrp','dbTbl',
                            'dbViewGrp','dbView','dbQrySqlGrp','dbQrySql',]+lGrp),
                    ('dbViewGrp',['dbView','dbTblGrp','dbTbl',
                            'dbTblsGrp','dbTbls','dbQrySql','dbQrySqlGrp',]+lGrp),
                    ('dbQrySqlGrp',['dbQrySql','dbTblGrp','dbTbl',]+lGrp),
                    ('Mod',['dbTblsGrp','dbTbls','dbTblGrp','dbTbl','dbViewGrp','dbView',
                            'dbQrySqlGrp','dbQrySql',]+lGrp),
                ])
            if oRoot is not None:
                doc.LinkRegisteredNode(oRoot,oBase)
            for sBase in ['admin','instance','research','sw','hw',
                    'typeContainer','typeGrp',
                    'Mod','Base','itfc','api','parameters',
                    'Fct','Prc','Bug','Test','Tool','ToDo',
                    'S88_phy','S88_ET','S88_ST','S88_AR','S88_CL','S88_UT','S88_EM','S88_CM',
                    'S88_Prc','S88_PrcS','S88_PrcO','S88_PrcA',
                    'S88_procModel','S88_uproc','S88_proc','S88_op','S88_ph',
                    ]:
                vidarc.vApps.vEngineering.__link__.LinkNodes(doc,[
                    (sBase,['database',]),])
    except:
        vtLog.vtLngTB(__name__)
