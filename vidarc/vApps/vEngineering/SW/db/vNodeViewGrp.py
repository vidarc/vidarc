#----------------------------------------------------------------------------
# Name:         vNodeView.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120407
# CVS-ID:       $Id: vNodeViewGrp.py,v 1.2 2013/01/10 22:25:23 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import vLang
    
    from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
    
    import vidarc.config.vcCust as vcCust
    if vcCust.is2Import(__name__):
        GUI=1
        vpc.logDebug('GUI objects imported',__name__)
    else:
        GUI=0
        vpc.logDebug('GUI objects import skipped',__name__)
except:
    vpc.logTB(__name__)
    GUI=0
vpc.logDebug('import;done',__name__)

class vNodeViewGrp(vNodeEngBase):
    def __init__(self,tagName='dbViewGrp'):
        global _
        _=vLang.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'database view group')
    def GetTransDescription(self):
        return u'database view group'
    # ---------------------------------------------------------
    # specific
    def GetGrpDict(self):
        return {
            None:0,
            'dbTblsGrp':[-1,None,],
            'dbTblGrp':[-1,None,],
            'dbViewGrp':[-1,None,],
            'dbQrySqlGrp':[-1,None,],
            'dbTbls':[-1,None,],
            'dbTbl':[-1,None,],
            'dbView':[-1,None,],
            'dbQrySql':[-1,None,],
            'Mod':[-1,None,],
            'Base':[-1,None,],
            'Param':[-1,None,],
            'Prop':[-1,None,],
            'itfc':[-1,None,],
            'api':[-1,None,],
            'var':[-1,None,],
            'const':[-1,None,],
            }
    def GetProcessNodeInc(self):
        return [
            {
            'lTag':['dbTblsGrp','dbTbls','dbTblGrp','dbTbl',
                    'dbView','dbViewGrp','dbQrySql','dbQrySqlGrp',
                    'Mod','itfc','api',],
            'level':1,'level_max':4,},
            #{'lTag':['Mod','array','class','struct'],'level':1,'headline':u'module'},
            ]
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'dataSrv':[
                    _(u'10 data server'),u'dataSrv',-10,
                    {'val':u'','type':'string',},
                    {'val':u'','type':'string',}],
            u'viewUsedTbl':[
                    _(u'30 database table'),u'viewUsedTbl',-30,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'viewUsedTblSrc':[
                    _(u'31 table source column'),u'viewUsedTblSrc',-31,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'viewUsedTblDst':[
                    _(u'32 table destination column'),u'viewUsedTblDst',-32,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\x8fIDAT8\x8d\xad\x93\xbb\xca"A\x10F\x8fc\xd3v{A\xbc\x80\x91\xb9\xa1\
\xb9/\xa1\x91\x81\x884\xc6>\x85>\x81\x89\x99\xec\xee\x13\x98\xee\xae\x98\x99\
\x18\x88 \x03\xe2\x84\nj&(\xa3\xcc\xe8\xf4\x06\xcb\xfa\xe7\xab\x15UQ\xf0\xd5\
\xf9\x8a\xaaX\xcc\x89\xf3N\x88\x7f\xc9\xef_?\xed[\x02\x00\xe5r\x99(\x8ax>\
\x9f\x1c\x8fG\x8a\xc5"A\x10\x10\x86!\xb7\xdb\x8dl6\xcb\xe3\xf1\xc0q\x1c.\x97\
\x0b\x001c\xcc\x7fMn\xb5Z_\x04\xb5Z\xed\xd5\x98\xcf\xe7T\xabU\x1c\xc7!\x8a"V\
\xab\x15\xf5z\x1d)%\x8e\xe3\xb0^\xafq]\x97J\xa5\xc2f\xb3\xf9\x10A\xa3\xd1@k\
\x8dR\x8a\xf1xL\xbb\xdd&\x93\xc9 \x84`8\x1c\xd2\xe9t\x10B\x90L&q]\x97\xd9l\
\xf6"\x10\x00J)\x94Rh\xad\x01\xd0Z\x93N\xa7\x11B\xbcj!\x04RJ\x12\x89\x04\x00\
\x9b\xcd\x06xc\x89\xfd~\xff\x8b\xa0\xd9l\xa2\xb5Fk\xcdh4\xa2\xdb\xed\x92\xcb\
\xe5PJ1\x18\x0c\xe8\xf5zH)\x89\xc7\xe3l\xb7[&\x93\xc9KH\x00\xe4\xf3yR\xa9\
\x14\xc9d\x12\x80b\xb1H\xa1P@)\x05@\xa9TBk\x8d\xb5\x96\xd3\xe9\xf4Y\x0b\x18c\
\xecr\xb9\xb4\x9e\xe7\xd9\xddng\x8d1\xd6\xf3<{>\x9f\xad\xef\xfb\xd6\x18c\x0f\
\x87\x83\r\x82\xc0\xfa\xbeo\x17\x8b\x855\xc6\xd8\xfd~o\xa7\xd3\xa9\x15\x00a\
\x18\x12\x86!\xf1\xf8\xdf\xc7\n\x82\xe0u\xc2\x00\xd7\xeb\x15)%Q\x14q\xbf\xdf\
?c\xe1\xdb\xf7\x1f1\x80\xd8\xbb\xef\xfc\x07\xec1\xbaw\x87\xd6\xa8p\x00\x00\
\x00\x00IEND\xaeB`\x82' 
