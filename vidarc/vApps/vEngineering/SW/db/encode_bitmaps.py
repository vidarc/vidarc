#!/usr/bin/env python
#----------------------------------------------------------------------
#----------------------------------------------------------------------------
# Name:         encode_bitmaps.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20130110
# CVS-ID:       $Id: encode_bitmaps.py,v 1.1 2013/01/10 20:21:58 wal Exp $
# Copyright:    (c) 2013 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

command_lines = [
    "-u -i -n GrpTbls           img/TablesGrp_16.png                    images.py",
    "-a -u -n Tbls              img/Tables_16.png                     images.py",
    "-a -u -n GrpTbl            img/TableGrp_16.png                     images.py",
    "-a -u -n GrpView           img/ViewGrp_16.png                      images.py",
    "-a -u -n View              img/View_16.png                         images.py",
    "-a -u -n GrpSQL            img/SQLgrp_16.png                       images.py",
    "-a -u -n SQL               img/SQL_16.png                          images.py",
    ]
