#----------------------------------------------------------------------------
# Name:         vNodeView.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120407
# CVS-ID:       $Id: vNodeView.py,v 1.5 2013/01/10 22:25:23 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import vLang
    
    from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
    
    import vidarc.config.vcCust as vcCust
    if vcCust.is2Import(__name__):
        GUI=1
        vpc.logDebug('GUI objects imported',__name__)
    else:
        GUI=0
        vpc.logDebug('GUI objects import skipped',__name__)
except:
    vpc.logTB(__name__)
    GUI=0
vpc.logDebug('import;done',__name__)

class vNodeView(vNodeEngBase):
    def __init__(self,tagName='dbView'):
        global _
        _=vLang.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'database view')
    def GetTransDescription(self):
        return u'database view'
    # ---------------------------------------------------------
    # specific
    def GetGrpDict(self):
        return {
            None:0,
            'dbTblsGrp':[-1,None,],
            'dbTblGrp':[-1,None,],
            'dbViewGrp':[-1,None,],
            'dbQrySqlGrp':[-1,None,],
            'dbTbls':[-1,None,],
            'dbTbl':[-1,None,],
            'dbView':[-1,None,],
            'dbQrySql':[-1,None,],
            'Mod':[-1,None,],
            'Base':[-1,None,],
            'Param':[-1,None,],
            'Prop':[-1,None,],
            'itfc':[-1,None,],
            'api':[-1,None,],
            'var':[-1,None,],
            'const':[-1,None,],
            }
    def GetProcessNodeInc(self):
        return [
            {'lTag':['Mod','itfc','api',
                'dbTbl','dbTbls','dbTblGrp','dbTblsGrp',
                'dbView','dbViewGrp','dbQrySql','dbQrySqlGrp',],
            'level':1,'level_max':4,},
            #{'lTag':['Mod','array','class','struct'],'level':1,'headline':u'module'},
            ]
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'dataSrv':[
                    _(u'10 data server'),u'dataSrv',-10,
                    {'val':u'','type':'string',},
                    {'val':u'','type':'string',}],
            u'viewUsedTbl':[
                    _(u'30 database table'),u'viewUsedTbl',-30,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'viewUsedTblSrc':[
                    _(u'31 table source column'),u'viewUsedTblSrc',-31,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'viewUsedTblDst':[
                    _(u'32 table destination column'),u'viewUsedTblDst',-32,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xbaIDAT8\x8d\xad\x93\xb1kSQ\x14\x87\xbfw\xdf{\xf7\xbe\xf7\x92\x14\
\x92\xdaZ\xd4I\x07G\xff\x05A\xdct\x11}\x83\xa26$\x83\rN\xe2\xe8\xa4\xe2"\xa2\
\x1dD\xea"n\x82\xa0\x08\x82\x01k\xba8H\\l+H\xb5F*4\xa5U\x11\x89I\xfan\xd2\
\x1c\x87j\xba\x9b\x9c\xe9\x1c\x0e\xfc\xce\xf7;\x9c\xe38\xcae\x90\xf0\xfe%\
\x95\xd9\xe72\x90\x00\xc0\xc4\xde\x03t\xc5!\xe9Bcc\x99\xecD\x0e\xdb\xb1$\xdd\
\x0e\xb6\x910\x965lI\x82\xe7\n\xdf\xbf9\x008q\xa1\xf4_\x93\xa7\xce\x1c\xdf!P\
G\xa7\xfa\x8d\xde\xcb{\x1c9\xd1\xc1j\x8d\xb6\x96WO|.\xe7\xf7\x11d:(\xa5(W|\
\xaa\x0b_\xd9\x7f\xf0\x10\xb5\xa5\xf9!\x11\x1c>u\x0e?J\x916\x9a\xa7\xf7\xa79\
9Y$\x93\xc9\xa0=\xc5\xcc\xed[\x9c\xcdO\xe2\xbb\x1eQ\x14\xb0\xbc\xf8\x8e\xf2\
\xdc\xeb>\x81\x07\x10\x86\x1a\x1d\x18R\x81\x06 \n\x0c#\xe9\x08\xadT\xbf\xf6]\
\x8f\xc0\xf7\t\x8c\x01\xa0\xb64\x0f\x0c\xb0\xc4\x9bW\xaf\xec\x10\x1c;}\x1e\
\x13E\xa4\x8d\xe1\xe1\xdd;\xe4\x8bEr\xd9,\x91\xf1\xb9v\xfd\x06\x97.^\xc0\x18\
\x8dR.\x9f?~\xe0\xd1\xb3r_\xc8\x03\xc8\xe5r\x84\xe9\x0c#\x7f-\x8c\xee\x1ac|4\
KJ\xfb\x00\xec\xd9=N\x18\x86\xf4D\xf8\xb1\xbe:\\\x0b\xc4\x85\x92\xcc.|\x927_\
\xd6\xe5}\xfd\xa7\xc4\x85\x92,\xd6Vd\xe3WK\x1a\xedM\x89\x0b%Y[\xab\x8b\xb5VZ\
\xad\x96T\xdfV%.\x94de\xb5.s\x95\x17\xb2}\xca\xb6\r\xd6GT\x0f\x00\xd9l\xb2\
\x95\xa4h\xdbm\xb8f\xf37Zk\xa4\'$\xed\xf6p,<~0\xe3\x008\x83\xbe\xf3\x1f\x9f\
\r\xb6\x83z\xd2\xcc\x05\x00\x00\x00\x00IEND\xaeB`\x82' 
