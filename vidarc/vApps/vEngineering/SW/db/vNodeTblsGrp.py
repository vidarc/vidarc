#----------------------------------------------------------------------------
# Name:         vNodeTblsGrp.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20130110
# CVS-ID:       $Id: vNodeTblsGrp.py,v 1.2 2013/01/10 22:25:23 wal Exp $
# Copyright:    (c) 2013 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import vLang
    
    from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
    
    import vidarc.config.vcCust as vcCust
    if vcCust.is2Import(__name__):
        GUI=1
        vpc.logDebug('GUI objects imported',__name__)
    else:
        GUI=0
        vpc.logDebug('GUI objects import skipped',__name__)
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vNodeTblsGrp(vNodeEngBase):
    def __init__(self,tagName='dbTblsGrp'):
        global _
        _=vLang.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'database tables group')
    def GetTransDescription(self):
        return u'database tables group'
    # ---------------------------------------------------------
    # specific
    def GetGrpDict(self):
        return {
            None:0,
            'dbTblsGrp':[-1,None,],
            'dbTblGrp':[-1,None,],
            'dbViewGrp':[-1,None,],
            'dbQrySqlGrp':[-1,None,],
            'dbTbls':[-1,None,],
            'dbTbl':[-1,None,],
            'dbView':[-1,None,],
            'dbQrySql':[-1,None,],
            'Mod':[-1,None,],
            'Base':[-1,None,],
            'Param':[-1,None,],
            'Prop':[-1,None,],
            'itfc':[-1,None,],
            'api':[-1,None,],
            'var':[-1,None,],
            'const':[-1,None,],
            }
    def GetProcessNodeInc(self):
        return [
            {'lTag':['dbTblsGrp','dbTbls','dbTblGrp','dbTbl',
                    'dbView','dbViewGrp','dbQrySql','dbQrySqlGrp',
                    'Mod','itfc','api',
                    ],'level':1,'level_max':4,},
            ]
    # ---------------------------------------------------------
    # inheritance
    def DoExec(self,parent,node,nodePar):
        try:
            self.__logInfo__(''%())
        except:
            self.__logTB__()
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        if GUI:
            return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01iIDAT8\x8d\xc5\x93\xbf\x8a*1\x18\xc5\x7f\x89\x0b\xfe\xa9\x14;\x1b\
\xc17\xd1F\xb0\xb4\x16\x1b[\xedDv\xef\x03\xdc\xbb\x13\x1f@Al\xa7\x14m\xe65\
\xac\xc4\xde\xc6\xce\xe8\xccXL"\x93[\xb8\xba\xc8\xbd\xdb\xb8\xc5\x1e\xf8 \'\
\x90\x93s\xbe/\x11Bf\xf8\x0e\xe4\xb7N\x03/\xb7\xc5\xfb\x9f\xdf\xee)\x05!3\
\x08\x99a2\x99\xb8\xc3\xe1\xe0\x8c1\xceZ\xeb6\x9b\x8d\xf3<\xcfYk\x9d1\xc6\
\x9dN\xa7;\x8f\xa2\xc8\xc5q\xec<\xcfs\xc2\xf3\xbc\xa7n\x1e\x8dF(\xa5\xae\x11\
:\x9d\x0e\x00i\x9a\xe2\xfb>\xedv\x9b\\.\x07\x80\xef\xfb4\x9bM\xca\xe52B\x08\
\xce\xe73\xcb\xe5\x12\xa5\xd4g\x0f*\x95\xca\x83z\xadVC\xca\xcf\xfeV\xabU\x8a\
\xc5"\x00I\x92\x00\xd0\xeb\xf5\x98\xcf\xe7<\x1d\xa1\xd5j\x11\x04\xc1\xd5\xc1\
p8\xc49\x87\x94\x92\xf1xL\xbf\xdf\'\x9f\xcf#\x84@)E\xb7\xdb\xa5T*\x11\xc71Zk\
\x16\x8b\x05A\x10\x00\x1f\xef\xe0r\xb9\xdc\xeb\x7f<\x0cC\xb4\xd6\x1c\x8fG\
\xb4\xd6\x00\xd4\xebu\x80\xe7#\xdc\xf0\x02\xd7\x91\xdc\xa0\x94b0\x18\x00`\
\xade:\x9d\xd2h4(\x14\nDQD\x18\x86\xac\xd7k^\xdf~\x89\xbb\xc0\xcd\x96\x10\
\x02\x80\xddnG\x9a\xa6\xf7\x08\xfb\xfd\x9el6K\x92$\x18c\xfeu0\x9b\xcd\x1e6W\
\xab\xd5\x03\xdfn\xb7_F\x10?\xfe\x1b\xff\x02\x91J\xb93\xb7\x15]\x9f\x00\x00\
\x00\x00IEND\xaeB`\x82' 
        else:
            return None
