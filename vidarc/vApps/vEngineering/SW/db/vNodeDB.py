#----------------------------------------------------------------------------
# Name:         vNodeDB.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120407
# CVS-ID:       $Id: vNodeDB.py,v 1.3 2013/10/24 06:28:23 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeDB(vNodeEngBase):
    def __init__(self,tagName='database'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'database')
    def GetTransDescription(self):
        return u'database'
    # ---------------------------------------------------------
    # specific
    def GetGrpDict(self):
        return {
            None:0,
            'method':[-1,None,]
            }
    def GetProcessNodeInc(self):
        return [
            {
            'lTag':['Mod','itfc','api','Fct','Prc','Tool',
                    'dbTblsGrp','dbTbls','dbTblGrp','dbTbl',
                    'dbView','dbViewGrp','dbQrySql','dbQrySqlGrp',
                    'struct'],
            'level':1,'level_max':4,},
            #{'lTag':['Mod','array','class','struct'],'level':1,'headline':u'module'},
            ]
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'dataSrv':[
                    _(u'10 data server'),u'dataSrv',-10,
                    {'val':u'','type':'string',},
                    {'val':u'','type':'string',}],
            u'dataSize':[
                    _(u'20 row count'),u'dataSz',-20,
                    {'val':100,'type':'int',},
                    {'val':100,'type':'int',}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02\x87IDAT8\x8d\xa5\x93K\x8b\x14g\x14\x86\x9f\xfa\xfa\xab\xea\x9a\x9a\
\x99\xb6gz\x92\x0cC\x12!\x82\x10\xa2"\xb8\x90\x04wn\\$\xf9\r\xb3\xce*\xe0\
\xef\xc8\xde\xa5\x90?\x91\x95\x8b\x90\x80\x84\xe0FH\x94Qt\xb4\xa7\xd5\xe9\
\x9a\xae\xfb\xe5\xbbT}YTO\x12w\x81\x9c\xcd9pn\xbc\x0f\xbc\x9e\'F\x9c\xc7\xe1\
\x0f?9\xfeC\xdc\xbf{\xc7;\xaf\xe5\xbf\x17\xbf\xfb\xf66Q0\xf4\xbc\xc0\xd1\xe2\
Q\xc9a\xd0\xf9\xd0\x02\x9d\x04\x18\xe6\xef\xdf\xbd\xe3y\xb7\xbe\xff\xd1}\xf3\
\xd5\r\xbe\xfe\xf2\xf3\xf7\xbe(aizA\x1f\t\xda~}d<\xe4N\xc2\xcf\x0f\x1e\xf3\
\xe8\xd7g\xc8N\xc3oO\x9ea\x8c\xe1\xe0\xc26\xc1n\xc48\x90\x8c7B\x00D9B\x87\
\x82F\x1bJ\xa3\x89\xe7\xa7\x1c/\x96\x1c\x1f-\xc9\xb3j\x90\xb0\xe1oqpa\x9bO\
\xf6\'\xf8\x9b#\xd2R\xa3W\x19\xa9\xee\xa8JEQ\xd6 \x03j\xdb\xa0S\xc7\x84\x88\
\x0f&S\xaaf}\xc0\xd6>\x8d\xea\xd8\x08\x03\xae~\xba\xfd\xb7\x8c\xca\xc2"\xa9Y\
T\x8e,NY\xa5\x92e\xdf\x92\xd6\x86\xd6\rp\xa465o\xab\x94\xb8\xdd\xc1\x891\xb6\
\x87Vwxr\xc4\x8b\xbc\'i\x04\x95\xd2\x14\xa3\x80&th\rI\xa6xu\xb2\xa2\xd352\
\xf0#fS\xc7\xe9\x9b\x82\x07\x1c\xf1\xe8hL\xe8\x05\xc8\xa9O\xa3,\x99qd\x95&Y\
\x95$g\x05q\x9c1\x9b:v&=y\x16\xfd\xc3\xe0\xe6\x95\x03\xae\\\xdae+\nX\xe45\
\xa6\xea86\x96\xceh\xa43L\xa6!\x1fo\xf9pq\x17\xd3;^\xce\x97\xef3\xc8kG\xad\
\x1c\x17\xf7\x03f\x93\x00)\xe0\x0b;\xe3m\x0bIV\xb3x\x93\xb0\xc8\x15EZ\x13\
\x1bG\xeb$\x9d\x06q\xce\xe0]\x9cS\xaaA\xffJ\x0f\x00\xd3\xd6\x91\x14\x96\xa4u\
\xe4\x9b\x1b\xd8\x91$Ep\x16\x97\xbc:Y\xa1\xcd\x9a\xc1\xfe\xe6\x94\xc6\x18~\
\x7f\xf2\x82\xa7\xf3\x08]9\xd2P\xd0(\xcb*o)kM\xd3\x1a\x00Bs\x86\'\xa7|\xb6;c\
^\x14\x83\x04\x19\x19\xae]\xda\xe3\xfa\xe5=>\xdc\t\xc9MG^\x1b\x92\xb8!k\x14u\
e\xd1\xbeGU*Z\xb5C\x99Z\xfe\x8cS(\xd6^\x000\xbd$\xaf;\xf6f\x82\xad\xb1\xe0\
\xa3M\x9f\xd3\xed\x88\xa2V\xccs\xcb\xf2,\xa7\x15\x86Bu\xc4\xeb\xadN\x83xx\
\xef\xd0{\xfe\xfa\x84_\x9e\xfeA\xa9\xc0\xd9\x0eg;l\x0f\xc6:R9\xc6F>JH\x1aoD\
\x8a@e\xef8\x99\x1f\xf3\xf0\xde\xa1\xe7\xfd_;\xff\x05\xd0U{t\x0f\x94B\xe7\
\x00\x00\x00\x00IEND\xaeB`\x82' 
