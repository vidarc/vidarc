#----------------------------------------------------------------------------
# Name:         vNodeTbls.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20130110
# CVS-ID:       $Id: vNodeTbls.py,v 1.2 2013/01/10 22:25:23 wal Exp $
# Copyright:    (c) 2013 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import vLang
    
    from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
    
    import vidarc.config.vcCust as vcCust
    if vcCust.is2Import(__name__):
        GUI=1
        vpc.logDebug('GUI objects imported',__name__)
    else:
        GUI=0
        vpc.logDebug('GUI objects import skipped',__name__)
except:
    vpc.logTB(__name__)
    GUI=0
vpc.logDebug('import;done',__name__)

class vNodeTbls(vNodeEngBase):
    def __init__(self,tagName='dbTbls'):
        global _
        _=vLang.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'database tables')
    def GetTransDescription(self):
        return u'database tables'
    # ---------------------------------------------------------
    # specific
    def GetGrpDict(self):
        return {
            None:0,
            'dbTblsGrp':[-1,None,],
            'dbTblGrp':[-1,None,],
            'dbViewGrp':[-1,None,],
            'dbQrySqlGrp':[-1,None,],
            'dbTbls':[-1,None,],
            'dbTbl':[-1,None,],
            'dbView':[-1,None,],
            'dbQrySql':[-1,None,],
            'Mod':[-1,None,],
            'Base':[-1,None,],
            'Param':[-1,None,],
            'Prop':[-1,None,],
            'itfc':[-1,None,],
            'api':[-1,None,],
            'var':[-1,None,],
            'const':[-1,None,],
            }
    def GetProcessNodeInc(self):
        return [
            {
            'lTag':['dbTblsGrp','dbTbls','dbTblGrp','dbTbl',
                    'dbView','dbViewGrp','dbQrySql','dbQrySqlGrp',
                    'Mod','itfc','api',],
            'level':1,
            'level_max':4,
            #'headline':u'tables'
            },
            #{'lTag':['Mod','array','class','struct'],'level':1,'headline':u'module'},
            ]
    def GetCfgDictOld(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'dataSrv':[
                    _(u'10 data server'),u'dataSrv',-10,
                    {'val':u'','type':'string',},
                    {'val':u'','type':'string',}],
            u'qryMode':[
                    _(u'12 query mode'),u'qryMode',-12,
                    {'val':u'FixedQuery','type':'choice','it02':'FixedQuery','it01':'Command','it00':'ColumnList','it03':'FixedQueryWithOutput','it04':'ModeList','it05':'Query','it06':'TableList'},
                    {'val':u'false','type':'choice','it00':'true','it01':'false'}],
            u'rowCnt':[
                    _(u'20 row count'),u'rowCnt',-20,
                    {'val':100,'type':'int',},
                    {'val':100,'type':'int',}],
            u'itvCnt':[
                    _(u'21 intervall count'),u'itvCnt',-21,
                    {'val':1,'type':'int',},
                    {'val':1,'type':'int',}],
            u'duration':[
                    _(u'22 duration'),u'duration',-22,
                    {'val':1,'type':'int',},
                    {'val':1,'type':'int',}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\x8bIDAT8\x8d\xc5\x93\xb1k\x14A\x18\xc5\x7f\xb3{\x92\x88\xd1\x80\x95\
)\x8c\x85\x82\xa0\xc5\x81\n\x8a{E\xc0\xde.\x01+\xc1B\x88\x8d`\xa3\xfe\tZ\xc8\
^k!hc\x91V,b\x93B\xac\x0c\xec6\xc1\x83$\x84\x035\t\xd1\xd3\xdcy\xbbw3\xb3\
\xcfb\xbd\x0bi7\x85\x0f>\x98W\xcc\x9b\xf7\xde\xf0\x19\x13\x84\x1c\x05\xc1\
\x91n\x03\xb5\xd1\xe1\xe6\x8bUUR0A\x88\tB\xe6_\xa5\xfa\xda\xc9\x94[\xa7\xdcz\
}\xf8\xb2\xa7(N\x94[\xaf\xdc:\xedtsEq\xa2\x81\xf5\xda\xeb\r\xf4\xa37T\x14'2Q\
\x9cTz\xf9\xe3\xc3:\x8dfZFx\xb9p\x1e\x00'\xf1`i\x93\xe7\xb7g99q\x0c\x80\xc5\
\xa5\r\x9e\xdc\x9aa\xf6\xf4q\x02c\xe8\xf4-O\xdf\xb5i4\xd3\x83\x0e.\xcdL\x1dR\
\xbf~n\x9a08\xe8\xf7\xda\xd9S\x9c\x99\x9e\x04\xa0?t\x00\xbc\xbeS\xe7\xee\xdb\
\x94\xca\x11\x1e\xcf]\xe4\xd9J\x0b\xa28\x91\xf7^\xcey\x15*\x14\xc5\x89~\xf5\
\x07*\x8aB\xfa\xc7[\xdb]\xf52\xaf\xcd]\xab\x95V\xa6(N\xc6\x13\x00\x0c\\Q\x8e\
-\xcd\x8c\xb9+\xf9\xce\xbeg\xebgA\xbb#\xda\x1d\x0f\xc0\xbd+u\x80\xea\x11F\
\xa8A\xf9%#4\x9a)\xef\xef_\xa6(jdC\x98\x7f\x93\xb2x\xe3\x02S\x13'\xd8\xdd\
\x17\xdf\xbb\x8e\xe5\xf55>=\xbaj\xc6\x02\xdf~gH`L\xd9\xfc\xe7v\x86u!\xb9-\
\xf7dm;g\xb2\x16\xd0\xcb\xc5\x1f\xeb\x0e9\xa8\x1ca\xe4\xc0\xfc\xf7m\xfc\x0b\
\xa3\x82\xd9\xddd\xcf\xe2\x1e\x00\x00\x00\x00IEND\xaeB`\x82" 
