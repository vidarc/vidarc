#----------------------------------------------------------------------------
# Name:         vNodeList.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: vNodeList.py,v 1.3 2006/09/27 04:20:51 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeList(vXmlNodeEngineering):
    def __init__(self,tagName='list'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'software list')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xa6IDAT8\x8d\x95S[\x0e\xc30\x08\xb3I\xae=iR\xcf\xdd\xb2\x8f\xcd*a\
\xd0u\x96\xa2\xa6\x80yX\x84\xb4\x01\xc1\x8f\xddu\xa7\r\xe2\x06f$FR\xb6\xc5\
\xe4\xd1>\xbb\x8a\x99\x98\xfd~\xecN\x1b$\x00?\x83\x1f\x9f\xe0\x8d\nXI\xa7_I\
\xac\x9b\xed/\r:\xa8beW\x17m\x07\x1d\xf9\xab\x03\xda(\xe7\x15T)&\x8d\x1a]h\
\xb0-\xc2v\xb0;\xd5\xaf\x92\x94"\xde\x99_\xa3\x03\xef=@<\xc0\xd3\xf5\xd5\xbd\
:\x00|Y\xa4_\xa8\xd6\xdbb+Up\xfc\xa7\rF2mp\xe6y\x14\x90\x13\xb7B\xc7\xe7\x9c\
+W\xaf3\xfb^\xffB\x84\nz\xf1\x8a%\x00\x00\x00\x00IEND\xaeB`\x82' 
