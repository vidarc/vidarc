#----------------------------------------------------------------------------
# Name:         vNodeModPy.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20061118
# CVS-ID:       $Id: vNodeModPy.py,v 1.6 2009/01/24 23:52:09 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.ext.report.veRepDocBase import veRepDocBase
from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeModPy(veRepDocBase,vXmlNodeEngineering):
    def __init__(self,tagName='ModPy'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
        veRepDocBase.__init__(self,self.__class__.__name__)
    def GetDescription(self):
        return _(u'software module python')
    # ---------------------------------------------------------
    # specific
    def GetModule(self,node):
        return self.GetML(node,'mod')
    def GetFile(self,node):
        return self.Get(node,'fnSrc')
    def GetDN(self,node):
        return self.Get(node,'dn')
    def IsSkipProcessing(self,node):
        if self.Get(node,'skipProc') in ['true']:
            return True
        return False
    def SetModule(self,node,val):
        self.Set(node,'mod',val)
    def SetFile(self,node,val):
        self.Set(node,'fnSrc',val)
    def SetDN(self,node,val):
        self.Set(node,'dn',val)
    def GetCfgDict(self,nodeCfg,lang):
        return {
            u'mod':[
                    _(u'01 module'),u'mod',-1,{'val':u'','type':'text'},{'val':u'','type':'text'}],
            u'dn':[
                    _(u'02 directory'),u'dn',-2,{'val':u'','type':'text'},{'val':u'','type':'text'}],
            u'fnSrc':[
                    _(u'03 file'),u'fnSrc',-3,{'val':u'','type':'text'},{'val':u'','type':'text'}],
            u'skipProc':[
                    _(u'90 skip processing'),u'skipProc',-90,
                    {'val':u'false','type':'choice','it00':'true','it01':'false'},
                    {'val':u'false','type':'choice','it00':'true','it01':'false'}],
            }
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02\x97IDAT8\x8du\x93]H\x93q\x14\xc6\x7f\xff\xf7\x9d\xd3\x9a\x19\x89\
\xa5\x11e\x85\x90\x9a\x17Fe\x85`f\x17b\x9fPRR`B\x05\x85\x08QP\x98\x9aF`\x17}\
P\x04\xd5U\x19RT\x06QT\x17\xd5\xe6\n\x145\xd1ii\xe6g\x1f\x9a\xb9l67u\xba\xf7\
\xdd\xbf\x8bj\xb5\xb4\x03\xe7\xe2p\xceyx8\xcfs\x10\x8a\xcaTYRR"\xadV\xab\xfc\
_\xdf\x9fBQ\xf97\xf6\xe5g\xc9\xe8\xfd\xd0\xf4\xa2\x03\xa59\x92\x83\x9b\x8f\
\xb0.m\xbd\x984\x08\x93\x01r\x0b\xb3\xe5\x9aCQ|\xf5\x0c\xd2?2\x80\xdde\xa7\
\xfal\x17\xed\xb7\x1dS\x02\x18\xfe.\xf2\x8ardza\x1c\x1e\xef(F=\x04\x93\xc1\
\xc4\x87Z\x07}\xb6!*-fy\xf1\xf2s""\x14v\xefH\xf33R~/\x17\x9f(\x92]\xdb\xef\
\xd2\xf8\xad\x16\x83jdfP(\xafk\xba\xe8\xa8\xee\x85Px\xd9\xa0\xb1)\xb3\x80\
\xa5\x89\xf9\xdc\xb83J\xf6\x9e|9\x89\xc1\x98\xee\xe2\xe9\xfb\n\x1e4V\xb0\xd8\
\x97H\xeb\xab\x8f\xf4\xde\x1c\x17\x00\xc5\xf5\xe9\x94\xddwK\xc5\x00as\x97q\
\xad\xfc:\x01\x0c\x00F\xdc\xb0d8\x9e\xa3\xde\x9d\x0c\xbc\xfdD\xeb\x05\xa7X\
\x95\xa4\xcb\r\x19\xba\x04\x081\xc2\xb06A\x99\xed\x1d.\xefX @jj*\x0b\x1b\x92\
8\x1fWLRT4gc\x96\x03\x90\x92\x12\x8b\xdb\xbd\x17\x80\xac\x8d\xa1\xe2\\\x95\
\x1d\xe7\xa8\x0bt-\xf0\x88\x16\xf3#n\x1d\xb9\x806:\x88\x12\x1c\xc6\xcamf\xb1\
6E\x97\x8a\xd2Frr\x1b\xd3\xe6#\xbf\xbc\x81\xa6\xf2X!\x80\xb5\x9br\xe4\x8b\
\xbe_2\x16\x1c\xde"O\x15\xe6\x82&\xd0|\x12cT\xbaH\xdd\xaa\xcb\x98\x180\xcd\
\x83\xd6\xd9\xd0\x9c\x00\x0b*`\x85\x03\xae\\U\xfd\x92\xfed0b\xc3\xf3\xb1\x14\
e\xfaR\x0c\xe1\x19\x00t\xc6\xc3\xd8f\xb0\xb7\x83\xbd\x05L=`H\x00C{\xa0\x0f\
\xfc7\xd0t\x1f\x8e\xfe:\xdeV\x1d\x03\xa0\xf7\xb4*\x92\x0f\x81b\x01\xa3\x07F\
\x1eC\xf8\x0c\xf08\xa60\x92\xa6\xf9\x18\x1cr1\xe4t\xf3\xdd9\x01\xc0>\x9f.\
\x9f9\xe0s+\xcc\x8d\x85yy\x10m\x85gO\x02\x01\x84PT*-f\xf9\xb0\xe2\x0csL\xf5,\
Y$hj\x9b\xe0\xe4%\xa7\x00\x88\xbc\xac\xcb\x940Xh\x83\xeen\xe8\xec\x84\xa6fU,\
K\xd4\xa5\xcf\xf7\xcf/TZ\xcc\xf2\xde\xadR2w\x1d\xf7[5\xaeF\x97\xab[ \xb4\x0f\
\xbcA\xa0J\x18\xb4A\x8f\x02u\xb7U1\xe57\xfe\x1d\xb3\x0e\xe8\xd2\x14\r\xdeJ\
\x18\x1f\x87`\x01\x03\xd6?*\xfc\x00\xd8\xe3\t\xe3"k\x86\x01\x00\x00\x00\x00I\
END\xaeB`\x82' 
    def Process(self,f,*args,**kwargs):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurCls(vtLog.DEBUG,'args:%s;kwargs:%s'%(vtLog.pformat(args),
                            vtLog.pformat(kwargs)),self)
            node=kwargs['node']
            if self.IsSkipProcessing(node):
                return
            if 'lang' not in kwargs:
                kwargs['lang']=self.lang
            else:
                self.__setLang__(kwargs['lang'])
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurCls(vtLog.DEBUG,'args:%s,kwargs:%s'%(vtLog.pformat(args),vtLog.pformat(kwargs)),self)
            lang=kwargs['lang']
            nodeBase=kwargs.get('__base_node',None)
            nodeRel=kwargs.get('__rel_node',None)
            self.__setLang__(lang)
            self.__getTranslation__()
            iLevel=kwargs['level']
            strs=[]
            s=':'.join([self.GetTrans('module'),self.GetTag(node)])
            strs.append(self.__getHeadLine__(iLevel,self.__tex__(s)))
            if 1:
                strs.append('\\begin{center}')
                strs.append('  \\begin{tabular}{p{0.3\linewidth}p{0.7\linewidth}}')
                strs.append('  %s: & %s \\\\'%(self.__tex__(self.GetTrans('name')),
                            self.__tex__(self.GetName(node,lang=lang))))
                strs.append('  %s: & %s \\\\'%(self.__tex__(self.GetTrans('directory')),
                            self.__tex__(self.GetDN(node))))
                strs.append('  %s: & %s \\\\'%(self.__tex__(self.GetTrans('file')),
                            self.__tex__(self.GetFile(node))))
                #s=self.GetInheritance(node)
                #l=s.split(',')
                #if len(l)>0:
                #    strs.append('  \\textbf{%s}: &  \\\\'%(self.__tex__(self.GetTrans('inheritance'))))
                #    for s in l:
                #        strs.append(' \\hspace{0.3cm} & %s \\\\'%(self.__tex__(s)))
                strs.append('  \\end{tabular}')
                strs.append('\\end{center}')
            else:
                strs.append('\\begin{verbatim}')
                strs.append('  %-25s: %s'%(self.__tex__(self.GetTrans('name')),
                            self.__tex__(self.GetName(node,lang=lang),True)))
                strs.append('  %-25s: %s'%(self.__tex__(self.GetTrans('directory')),
                            self.__tex__(self.GetDN(node),True)))
                strs.append('  %-25s:  %s'%(self.__tex__(self.GetTrans('file')),
                            self.__tex__(self.GetFile(node),True)))
                strs.append('\\end{verbatim}')
            strs.append(self.__tex__(self.GetDesc(node,lang=lang)))
            #strs.append(self.__getHeadLine__(iLevel,
            #        self.__tex__(self.GetTag(node))))
            self.__writeLst__(f,strs)
            self.__includeDoc__(f,*args,**kwargs)
            #self.__includeNodes__(['funcPy','ModPy','classPy'],f,*args,**kwargs)
            kkwargs=kwargs.copy()
            kkwargs.update({'level':kwargs['level']+1})
            self.__includeNodes__(['funcPy','classPy'],f,*args,**kkwargs)
            self.__includeNodes__(['ModPy'],f,*args,**kwargs)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
