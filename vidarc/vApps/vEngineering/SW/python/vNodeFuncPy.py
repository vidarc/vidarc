#----------------------------------------------------------------------------
# Name:         vNodeFuncPy.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20061118
# CVS-ID:       $Id: vNodeFuncPy.py,v 1.6 2009/02/22 18:04:51 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.ext.report.veRepDocBase import veRepDocBase
from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeFuncPy(veRepDocBase,vXmlNodeEngineering):
    NODE_ATTRS=vXmlNodeEngineering.NODE_ATTRS+[
        ('Line',None,'line',None),
        ('Module',None,'module',None),
        ]
    def __init__(self,tagName='funcPy'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
        veRepDocBase.__init__(self,self.__class__.__name__)
    def GetDescription(self):
        return _(u'software function python')
    # ---------------------------------------------------------
    # specific
    def GetFile(self,node):
        return self.Get(node,'fileSrc')
    def GetLine(self,node):
        return self.Get(node,'line')
    def GetModule(self,node):
        return self.GetML(node,'module')
    def GetArgs(self,node):
        return self.Get(node,'args')
    def GetArgsDesc(self,node):
        return self.Get(node,'argsDesc')
    def GetReturn(self,node):
        return self.Get(node,'return')
    def GetExceptions(self,node):
        return self.Get(node,'exceptions')
    def IsSkipProcessing(self,node):
        if self.Get(node,'skipProc') in ['true']:
            return True
        return False
    def SetModule(self,node,val):
        self.Set(node,'module',val)
    def SetFile(self,node,val):
        self.Set(node,'fileSrc',val)
    def SetLine(self,node,val):
        self.Set(node,'line',val)
    def SetArgs(self,node,val):
        self.Set(node,'args',val)
    def SetArgsDesc(self,node,val):
        self.Set(node,'argsDesc',val)
    def SetReturn(self,node,val):
        return self.Set(node,'return',val)
    def SetExceptions(self,node,val):
        return self.Set(node,'exceptions',val)
    def BuildLang(self):
        vXmlNodeEngineering.BuildLang(self)
        #print self.dCfg
    def GetCfgDict(self,nodeCfg,lang):
        return {
            u'module':[
                    _(u'01 module'),u'module',-1,{'val':u'','type':u'text'},{'val':u'','type':u'text'}],
            u'fileSrc':[
                    _(u'02 file'),u'fileSrc',-2,{'val':u'','type':'text'},None],
            u'line':[
                    _(u'03 line'),u'line',-3,{'val':u'','type':u'long'},{'val':u'','type':u'text'}],
            u'args':[
                    _(u'04 arguments'),u'args',-4,{'val':u'','type':u'text'},{'val':u'','type':u'text'}],
            u'argsDesc':[
                    _(u'05 arguments description'),u'argsDesc',-5,
                    {'val':u'','type':u'text'},{'val':u'','type':u'text'}],
            u'return':[
                    _(u'06 return'),u'return',-6,
                    {'val':u'','type':u'text'},{'val':u'','type':u'text'}],
            u'exceptions':[
                    _(u'07 exceptions'),u'exception',-7,
                    {'val':u'','type':u'text'},{'val':u'','type':u'text'}],
            u'skipProc':[
                    _(u'90 skip processing'),u'skipProc',-90,
                    {'val':u'false','type':'choice','it00':'true','it01':'false'},
                    {'val':u'false','type':'choice','it00':'true','it01':'false'}],
            }
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01?IDAT8\x8d\xa5\x91\xbdJ\x03A\x14\x85\xbf\x99]\x15\xb2*\xc4 \x16\x82\
\xb1H\xa1\x92\xc2?4 \xac\xbe\x80\xf8\x10Z\xd8\x08>\x81\x88\x8do\xa0\x16>\x80\
`m,\x84\x98R,$\x06\x93"\x04\x17D%\x84\x90*bB\x98\x1d\x8b dc\xd6]\xf4\x963\
\xe7~\xf7\xdc{\x84\x90\x06\xdf\xa5\x0f\x94\x06\x10G\x86 d\x89n@7$,Hp\x88\x0e\
\x12\xfd\x06\x0b\r\xf0\x83\x99\x7fi\xf2\xbc\xff\xf7\x06\x1e\x07\xfdRX]Q:\x16\
\x83\xf4u\x7f\x98\xec\xb5\xd9;\xd5\xb6gh4\xb6}\x1d\xfcX\xa1\xbb\xd6m\xa5S)\
\x90\x12\x1e^\xa0\xf2\x04\x8fy\xef\x00_\xc0\xc6\x96\xd2\x89\x04X\x93P\x1c\
\x87|\x12\xa6.a\xb9\x0e\xa7g!R(\xcf\xc1\xe7&TKP-\x80\xe5\x80\x99\x04\xb3\xe4\
\xd5\xc9\xfe\xed\xf0zl\x88\xb5}\x90\x19\x18l\xc2\xc7\x15\x8c\x8d@\xb3\x1e\
\x12\xb0\xe3*}S\x87\xf7"Dk\xb0\xb8\x07\xf1,d\xd3^\x9d\xef\n\xe7\xd2\x10\x94a\
\xe2D\xe9\xa5Q\x98\xce\xc1\xb3\x03\x91H\xe7\x7fa^i\xd7\rH\x01`\xf6N\xe9T\x01\
\x86\xdf\xa0=\x00\x86\x86Z\x0e\x1c\t\xf7\x17\x86\x08\x04Dw\x95\xb6\xe2\xd0\
\xbe\x85V\x0b\x86\x04T2 d\'\x89/C\xa2e\\N\xcej\xd0\x00\x00\x00\x00IEND\xaeB`\
\x82' 
    def Process(self,f,*args,**kwargs):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurCls(vtLog.DEBUG,'args:%s;kwargs:%s'%(vtLog.pformat(args),
                            vtLog.pformat(kwargs)),self)
            node=kwargs['node']
            if self.IsSkipProcessing(node):
                return
            if 'lang' not in kwargs:
                kwargs['lang']=self.lang
            else:
                self.__setLang__(kwargs['lang'])
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurCls(vtLog.DEBUG,'args:%s,kwargs:%s'%(vtLog.pformat(args),vtLog.pformat(kwargs)),self)
            lang=kwargs['lang']
            nodeBase=kwargs.get('__base_node',None)
            nodeRel=kwargs.get('__rel_node',None)
            self.__setLang__(lang)
            self.__getTranslation__()
            iLevel=kwargs['level']
            strs=[]
            s=':'.join([self.GetTrans('function'),self.GetTag(node)])
            strs.append(self.__getHeadLine__(iLevel,self.__tex__(s)))
            if 1:
                strs.append('\\begin{center}')
                strs.append('  \\begin{tabular}{p{0.3\linewidth}p{0.7\linewidth}}')
                strs.append('  %s: & %s \\\\'%(self.__tex__(self.GetTrans('name')),
                            self.__tex__(self.GetName(node,lang=lang))))
                #strs.append('  \\textbf{%s}: & %s \\\\'%(self.__tex__(self.GetTrans('file')),
                #            self.__tex__(self.GetFile(node))))
                #strs.append('  %s: & %s \\\\'%(self.__tex__(self.GetTrans('line')),
                #            self.__tex__(self.GetLine(node))))
                s=self.GetArgs(node)
                l=s.split(',')
                sDesc=self.GetArgsDesc(node)
                lDesc=sDesc.split(',')
                iLen1=len(l)
                if iLen1>0:
                    strs.append('  %s: &  \\\\'%(self.__tex__(self.GetTrans('arguments'))))
                    iLen2=len(lDesc)
                    for iOfs in xrange(iLen1):
                        s=l[iOfs]
                        if iOfs<iLen2:
                            sDesc=lDesc[iOfs]
                        else:
                            sDesc=u''
                        strs.append('  \\hspace{0.3cm} %s & %s \\\\'%(self.__tex__(s),self.__tex__(sDesc)))
                strs.append('  \\end{tabular}')
                strs.append('\\end{center}')
            else:
                strs.append('\\begin{verbatim}')
                strs.append('  %-25s: %s'%(self.__tex__(self.GetTrans('name')),
                            self.__tex__(self.GetName(node,lang=lang),True)))
                #strs.append('  \\textbf{%s}: & %s \\\\'%(self.__tex__(self.GetTrans('file')),
                #            self.__tex__(self.GetFile(node))))
                #strs.append('  %s: & %s \\\\'%(self.__tex__(self.GetTrans('line')),
                #            self.__tex__(self.GetLine(node))))
                s=self.GetArgs(node)
                l=s.split(',')
                sDesc=self.GetArgsDesc(node)
                lDesc=sDesc.split(',')
                iLen1=len(l)
                if iLen1>0:
                    strs.append('  %-25s: '%(self.__tex__(self.GetTrans('arguments'))))
                    iLen2=len(lDesc)
                    for iOfs in xrange(iLen1):
                        s=l[iOfs]
                        if iOfs<iLen2:
                            sDesc=lDesc[iOfs]
                        else:
                            sDesc=u''
                        strs.append('    %-23s: %s '%(self.__tex__(s),self.__tex__(sDesc)))
                strs.append('\\end{verbatim}')
            strs.append(self.__tex__(self.GetDesc(node,lang=lang)))
            #,lang=lang
            self.__writeLst__(f,strs)
            self.__includeDoc__(f,*args,**kwargs)
            self.__includeNodes__(['funcPy','methodPy'],f,*args,**kwargs)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
