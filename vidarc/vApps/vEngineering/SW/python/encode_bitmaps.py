#!/usr/bin/env python
#----------------------------------------------------------------------
#----------------------------------------------------------------------------
# Name:         encode_bitmaps.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20061118
# CVS-ID:       $Id: encode_bitmaps.py,v 1.1 2006/11/21 21:22:25 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    #"   -u -i -n Application Tools01_16.png images.py",
#    "   -u -i -n Mondrian mondrian.ico images.py",
    "-u -i -n ModulePy          img/ModulePy01_16.png             images.py",
    "-a -u -n TypeClassPy       img/TypeClassPy03_16.png          images.py",
    "-a -u -n MethodPy          img/MethodPy01_16.png             images.py",
    "-a -u -n FuncPy            img/FuncPy01_16.png               images.py",
    "-a -u -n Documents         img/DocGrp01_16.png               images.py",
    "-a -u -n Doc               img/Doc03_16.png                  images.py",
    #"-a -u -n Activities img/ActivitySchedule01_16.png images.py",
    
    #"-u -i -n Splash img/splashEngineering01.png images_splash.py",
    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)

