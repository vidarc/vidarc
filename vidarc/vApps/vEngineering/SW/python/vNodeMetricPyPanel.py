#Boa:FramePanel:vNodeMetricPyPanel
#----------------------------------------------------------------------------
# Name:         vNodeMetricPyPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20061118
# CVS-ID:       $Id: vNodeMetricPyPanel.py,v 1.8 2010/03/29 08:54:42 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.gizmos
import vidarc.tool.time.vtTimeInputDateTime
import wx.lib.buttons
import vidarc.tool.input.vtInputTextML
import wx.lib.filebrowsebutton

import sys,os,types

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel
    from vidarc.tool.xml.vtXmlFilterType import vtXmlFilterString
    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    from vidarc.tool.vtThread import *
    #from vidarc.tool.net.vtNetTransactionThread import *
    from vidarc.tool.net.vtNetTransactionProcThread import *

    import vidarc.vApps.vEngineering.SW.python.images as imgPy
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VNODEMETRICPYPANEL, wxID_VNODEMETRICPYPANELCBBUILD, 
 wxID_VNODEMETRICPYPANELCBREFRESH, wxID_VNODEMETRICPYPANELDBBSRCDN, 
 wxID_VNODEMETRICPYPANELLBLDATE, wxID_VNODEMETRICPYPANELTRLSTRES, 
 wxID_VNODEMETRICPYPANELVIDATE, 
] = [wx.NewId() for _init_ctrls in range(7)]

class vNodeMetricPyPanel(wx.Panel,vtXmlNodePanel):
    LST_SRC_EXT=['.py']
    def _init_coll_bxsRes_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.trlstRes, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsBt, 0, border=4, flag=wx.LEFT)

    def _init_coll_bxsDate_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblDate, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viDate, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbRefresh, 0, border=0, flag=0)
        parent.AddWindow(self.cbBuild, 0, border=4, flag=wx.TOP)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsDate, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.dbbSrcDN, 1, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsRes, 1, border=4,
              flag=wx.BOTTOM | wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_trlstRes_Columns(self, parent):
        # generated method, don't edit

        parent.AddColumn(text=_(u'name'))
        parent.AddColumn(text=_(u'proc'))
        parent.AddColumn(text=_(u'type'))
        parent.AddColumn(text=_(u'filename'))
        parent.AddColumn(text=_(u'line'))

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self.bxsDate = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsRes = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsDate_Items(self.bxsDate)
        self._init_coll_bxsRes_Items(self.bxsRes)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VNODEMETRICPYPANEL,
              name=u'vNodeMetricPyPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(312, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 180))
        self.SetAutoLayout(True)

        self.lblDate = wx.StaticText(id=wxID_VNODEMETRICPYPANELLBLDATE,
              label=_(u'date'), name=u'lblDate', parent=self, pos=wx.Point(4,
              4), size=wx.Size(98, 26), style=wx.ALIGN_RIGHT)

        self.viDate = vidarc.tool.time.vtTimeInputDateTime.vtTimeInputDateTime(id=wxID_VNODEMETRICPYPANELVIDATE,
              name=u'viDate', parent=self, pos=wx.Point(106, 4),
              size=wx.Size(193, 26), style=0)

        self.dbbSrcDN = wx.lib.filebrowsebutton.DirBrowseButton(buttonText='...',
              dialogTitle=_('Choose a directory'),
              id=wxID_VNODEMETRICPYPANELDBBSRCDN,
              labelText=_(u'source directory'), parent=self, pos=wx.Point(4,
              34), size=wx.Size(296, 29), startDirectory='.', style=0)
        self.dbbSrcDN.SetMinSize(wx.Size(-1, -1))

        self.trlstRes = wx.gizmos.TreeListCtrl(id=wxID_VNODEMETRICPYPANELTRLSTRES,
              name=u'trlstRes', parent=self, pos=wx.Point(4, 67),
              size=wx.Size(216, 109), style=wx.TR_HAS_BUTTONS)
        self._init_coll_trlstRes_Columns(self.trlstRes)

        self.cbRefresh = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VNODEMETRICPYPANELCBREFRESH,
              bitmap=vtArt.getBitmap(vtArt.Synch), label=_(u'Refresh'),
              name=u'cbRefresh', parent=self, pos=wx.Point(224, 67),
              size=wx.Size(76, 30), style=0)
        self.cbRefresh.SetMinSize(wx.Size(-1, -1))
        self.cbRefresh.Bind(wx.EVT_BUTTON, self.OnCbRefreshButton,
              id=wxID_VNODEMETRICPYPANELCBREFRESH)

        self.cbBuild = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VNODEMETRICPYPANELCBBUILD,
              bitmap=vtArt.getBitmap(vtArt.Build), label=_(u'Build'),
              name=u'cbBuild', parent=self, pos=wx.Point(224, 101),
              size=wx.Size(76, 30), style=0)
        self.cbBuild.SetMinSize(wx.Size(-1, -1))
        self.cbBuild.Bind(wx.EVT_BUTTON, self.OnCbBuildButton,
              id=wxID_VNODEMETRICPYPANELCBBUILD)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.viDate.SetTagName('dtRefresh')
        self.thdFind=vtThread(self,True,origin='vNodeMetricPyPanel_thdFind')
        EVT_VT_THREAD_FINISHED(self,self.OnFindFinished)
        EVT_VT_THREAD_ABORTED(self,self.OnFindAborted)
        wx.EVT_TREE_ITEM_RIGHT_CLICK(self, self.trlstRes.GetId(),self.OnTreeNodeTreeRight)
        self.cbBuild.Enable(False)
        
        self.widPrint=vtLog.GetPrintMsgWid(self)
        
        #self.thdBuild=vtNetTransactionThread(self,True,verbose=1,origin='vNodeMetricPyPanel_thdBuild')
        #EVT_VT_NET_TRANSACTION_THREAD_FINISHED(self,self.OnBuildFinished)
        #EVT_VT_NET_TRANSACTION_THREAD_ABORTED(self,self.OnBuildAborted)
        self.thdBuild=vtNetTransactionProcThread(self,True,verbose=0,
            origin='vNodeMetricPyPanel_thdBuild',name='vNodeMetricPyPanel')
        EVT_VT_NET_TRANSACTION_PROC_THREAD_FINISHED(self,self.OnBuildFinished)
        EVT_VT_NET_TRANSACTION_PROC_THREAD_ABORTED(self,self.OnBuildAborted)
        
        self.imgDict={}
        self.imgLstTyp=wx.ImageList(16,16)
        self.imgDict['ModPy']=self.imgLstTyp.Add(imgPy.getModulePyBitmap())
        self.imgDict['classPy']=self.imgLstTyp.Add(imgPy.getTypeClassPyBitmap())
        self.imgDict['methodPy']=self.imgLstTyp.Add(imgPy.getMethodPyBitmap())
        self.imgDict['funcPy']=self.imgLstTyp.Add(imgPy.getFuncPyBitmap())
        self.imgDict['Documents']=self.imgLstTyp.Add(imgPy.getDocumentsBitmap())
        self.imgDict['Doc']=self.imgLstTyp.Add(imgPy.getDocBitmap())
        self.trlstRes.SetImageList(self.imgLstTyp)
        self.trlstRes.SetColumnWidth(0,230)
        self.trlstRes.SetColumnWidth(1,30)
        self.trlstRes.SetColumnWidth(2,80)
        self.trlstRes.SetColumnWidth(3,100)
        self.trlstRes.SetColumnWidth(4,80)
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def __Clear__(self):
        if VERBOSE or self.VERBOSE:
            self.__logDebug__(''%())
        # add code here
        self.viDate.Clear()
        self.dbbSrcDN.SetValue('')
        self.trlstRes.DeleteAllItems()
        self.lMetric={}
    def SetNetDocs(self,d):
        self.__logCritical__('FIXME'%())
        return
        if d.has_key('vHum'):
            dd=d['vHum']
            self.docHum=dd['doc']
        # add code here
        
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viDate.SetDoc(doc)
        self.thdBuild.SetDoc(doc)
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.viDate.SetNode(node)
            sSrcDN=self.objRegNode.GetSrcDN(self.node)
            self.dbbSrcDN.SetValue(sSrcDN)
            self.cbRefresh.Enable(not self.thdFind.IsRunning())
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            sSrcDN=self.dbbSrcDN.GetValue()
            self.objRegNode.SetSrcDN(node,sSrcDN)
            
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            # add code here
            self.viDate.Enable(False)
            self.dbbSrcDN.Enable(False)
        else:
            # add code here
            self.viDate.Enable(True)
            self.dbbSrcDN.Enable(True)

    def OnCbRefreshButton(self, event):
        event.Skip()
        if self.thdFind.IsRunning():
            return
        
        self.cbRefresh.Enable(False)
        self.cbBuild.Enable(False)
        self.iCount=0
        self.iAct=0
        self.files=[]
        sSrcDN=self.dbbSrcDN.GetValue()
        sDN,sMod=os.path.split(sSrcDN)
        self.dSum={}
        self.dRes={
            'id':self.doc.getKeyNum(self.doc.getParent(self.node)),
            'type':{
                'ModPy':[('tag',None)],
                'Documents':[('tag',None)],
                'Doc':[('tag',None)],
                'classPy':[('tag',None)],
                'methodPy':[('tag',None)],
                'funcPy':[('tag',None)],
                },
            'summary':self.dSum,
            'data':{('ModPy',(sMod,)):{
                        '__':{
                            'tag':sMod,
                            'dn':sSrcDN.replace('\\','/'),
                            'fn':sMod,}
                        }}
            }
        self.thdFind.Do(self._refresh,sSrcDN)
        self.trlstRes.DeleteAllItems()
    def OnCbBuildButton(self, event):
        event.Skip()
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'dRes%s'%(vtLog.pformat(self.dRes)),self)
        if self.thdFind.IsRunning():
            vtLog.vtLngCurWX(vtLog.ERROR,'thdFind still running',self)
            return
        #if self.thdBuild.IsRunning():
        #if self.thdBuild.IsBusy():
        #    vtLog.vtLngCurWX(vtLog.ERROR,'thdBuild still running',self)
        #    return
        self.thdBuild.Start()
        self.thdBuild.DoProc(self.dRes,False)
        #self.thdBuild.DoEdit(self.dRes['id'],self.dRes['data'][k]['__'])
        return
        sSrcDN=self.dRes['dn']
        id=self.dRes['id']
        node=self.doc.getNodeByIdNum(id)
        if node is not None:
            self.cbRefresh.Enable(False)
            self.cbBuild.Enable(False)
            nodeStart=self.doc.getParent(node)
            id=self.doc.getKeyNum(nodeStart)
            ti=self.trlstRes.GetRootItem()
            #self._build(ti,nodeStart,sSrcDN,self.dRes)
            self.thdBuild.Do('edit',id,self._doEdit,nodeStart,ti)
            #self.thdFind.Do(self._build,ti,nodeStart,sSrcDN,self.dRes)
    def OnBuildFinished(self,evt):
        self.cbRefresh.Enable(True)
        self.cbBuild.Enable(False)
        self.trlstRes.DeleteAllItems()
        self.dRes={}
    def OnBuildAborted(self,evt):
        self.cbRefresh.Enable(True)
        self.cbBuild.Enable(False)
        self.trlstRes.DeleteAllItems()
        self.dRes={}
    def _doEdit(self,id,node,ti):
        if wx.Thread_IsMain()==False:
            vtLog.vtLngCurWX(vtLog.CRITICAL,'called by thread'%(),self)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'id:%08d'%(id),self)
        if 1:
        #if self.doc.IsLocked(node):
            self.doc.acquire('dom')
            try:
                obj=self.trlstRes.GetPyData(ti)
                oReg=self.doc.GetRegByNode(node)
                sName=self.trlstRes.GetItemText(ti,0)
                sName=self.doc.CallBackWX(self.trlstRes.GetItemText,ti,0)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                self._doSet(oReg,node,sName,oReg.GetTagName(),obj)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'set done',self)
                self._build(ti,node)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'build fin',self)
            except:
                pass
            self.doc.release('dom')
            #oReg.SetTag(node,sName)
            #oReg.SetTag(node,obj['name'])
            self.doc.doEdit(node)
            self.doc.endEdit(node)
        return id
    def _doAdd(self,id,ti):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'id:%08d'%(id),self)
        node=self.doc.getNodeByIdNum(id)
        if node is None:
            return None
        obj=self.trlstRes.GetPyData(ti)
        sName=self.trlstRes.GetItemText(ti,0)
        sType=self.trlstRes.GetItemText(ti,2)
        oReg=self.doc.GetReg(sType)
        #c=oReg.Create(node,self._doSet,sName,sType,obj)
        c=oReg.Create(None,self._doSet,sName,sType,obj)
        return self.doc.getKeyNum(c),node,c,self._doAddResp,(ti,),{}
    def _doSet(self,oReg,node,sName,sType,obj):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'id:%s'%(self.doc.getKey(node)),self)
        oReg.SetTag(node,sName)
        #for k in ['fn','line','mod']:
        #    if k in obj:
        #        print k,obj[k]
        if sType in ['ModPy','classPy','methodPy','funcPy']:
            #for sTag,k in [('fileSrc','fn'),('line','line'),('mod','mod')]:
            if 'fn' in obj:
                self.doc.createSubNodeDict(node,{
                            'tag':'fnSrc',
                            'lst':[
                                {'tag':'type','val':'file'},
                                {'tag':'val','val':obj['fn']},
                                    ]})
        if sType in ['ModPy','classPy','methodPy','funcPy']:
            if 'line' in obj:
                oReg.SetLine(node,str(obj['line']))
            if 'mod' in obj:
                oReg.SetModule(node,obj['mod'])
            #self.doc.setNodeText(node,'line',str(obj['line']))
            pass
    def _doAddResp(self,id,idNew,ti):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'id:%08d'%(id),self)
        self.doc.acquire('dom')
        try:
            node=self.doc.getNodeByIdNum(idNew)
            if node is None:
                return None
            #obj=self.trlstRes.GetPyData(ti)
            #nodePar=self.doc.getParent(node)
            #wx.CallAfter(self.doc.NotifyAddNode,self.doc.getKey(nodePar),self.doc.getKey(node),'ok')
            if self.trlstRes.GetChildrenCount(ti)>0:
                self._build(ti,node)
        except:
            vtLog.vtLngTB(self.GetName())
        self.doc.release('dom')
    def _build(self,ti,node):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'dRes:%s'%(vtLog.pformat(dRes)),self)
        #self.doc.acquire('dom')
        try:
            obj=self.trlstRes.GetPyData(ti)
            triChild=self.trlstRes.GetFirstChild(ti)
            while triChild[0].IsOk():
                sName=self.trlstRes.GetItemText(triChild[0],0)
                sType=self.trlstRes.GetItemText(triChild[0],2)
                #lChilds=self.doc.getChilds(node,sType)
                lChilds=[]
                n=None
                #print sType
                for c in lChilds:
                    #print '  ',self.doc.getKeyNum(c),self.doc.getNodeText(c,'tag'),sName
                    if self.doc.getNodeText(c,'tag')==sName:
                        n=c
                        break
                if self.trlstRes.GetItemText(triChild[0],1)=='X':
                    if n is None:
                        # add
                        id=self.doc.getKeyNum(node)
                        self.thdBuild.Do('add',id,self._doAdd,id,triChild[0])
                        pass
                    else:
                        # edit
                        id=self.doc.getKeyNum(n)
                        #vtLog.CallStack('')
                        #print id
                        #print n
                        self.thdBuild.Do('edit',id,self._doEdit,n,triChild[0])
                triChild=self.trlstRes.GetNextChild(ti,triChild[1])
        except:
            vtLog.vtLngTB(self.GetName())
        #self.doc.release('dom')
    def _buildModDoc(self,tipp):
        img=self.imgDict['Documents']
        tid=wx.TreeItemData()
        tid.SetData({'type':'Documents','name':'docs'})
        tip=self.trlstRes.AppendItem(tipp,'man',-1,-1,tid)
        self.trlstRes.SetItemImage(tip,img,0,which=wx.TreeItemIcon_Normal)
        self.trlstRes.SetItemText(tip,'X',1)
        self.trlstRes.SetItemText(tip,'Documents',2)
        img=self.imgDict['Doc']
        tid=wx.TreeItemData()
        tid.SetData({'type':'Doc','name':'man'})
        tic=self.trlstRes.AppendItem(tip,'man',-1,-1,tid)
        self.trlstRes.SetItemImage(tic,img,0,which=wx.TreeItemIcon_Normal)
        self.trlstRes.SetItemText(tic,'X',1)
        self.trlstRes.SetItemText(tic,'Doc',2)
        img=self.imgDict['Doc']
        tid=wx.TreeItemData()
        tid.SetData({'type':'Doc','name':'man'})
        tic=self.trlstRes.AppendItem(tip,'oper',-1,-1,tid)
        self.trlstRes.SetItemImage(tic,img,0,which=wx.TreeItemIcon_Normal)
        self.trlstRes.SetItemText(tic,'X',1)
        self.trlstRes.SetItemText(tic,'Doc',2)
    def _buildClassDoc(self,tipp):
        img=self.imgDict['Documents']
        tid=wx.TreeItemData()
        tid.SetData({'type':'Documents','name':'docs'})
        tip=self.trlstRes.AppendItem(tipp,'man',-1,-1,tid)
        self.trlstRes.SetItemImage(tip,img,0,which=wx.TreeItemIcon_Normal)
        self.trlstRes.SetItemText(tip,'X',1)
        self.trlstRes.SetItemText(tip,'Documents',2)
        img=self.imgDict['Doc']
        tid=wx.TreeItemData()
        tid.SetData({'type':'Doc','name':'man'})
        tic=self.trlstRes.AppendItem(tip,'man',-1,-1,tid)
        self.trlstRes.SetItemImage(tic,img,0,which=wx.TreeItemIcon_Normal)
        self.trlstRes.SetItemText(tic,'X',1)
        self.trlstRes.SetItemText(tic,'Doc',2)
        img=self.imgDict['Doc']
        tid=wx.TreeItemData()
        tid.SetData({'type':'Doc','name':'man'})
        tic=self.trlstRes.AppendItem(tip,'oper',-1,-1,tid)
        self.trlstRes.SetItemImage(tic,img,0,which=wx.TreeItemIcon_Normal)
        self.trlstRes.SetItemText(tic,'X',1)
        self.trlstRes.SetItemText(tic,'Doc',2)
    def _buildMethodDoc(self,tipp):
        img=self.imgDict['Documents']
        tid=wx.TreeItemData()
        tid.SetData({'type':'Documents','name':'docs'})
        tip=self.trlstRes.AppendItem(tipp,'man',-1,-1,tid)
        self.trlstRes.SetItemImage(tip,img,0,which=wx.TreeItemIcon_Normal)
        self.trlstRes.SetItemText(tip,'X',1)
        self.trlstRes.SetItemText(tip,'Documents',2)
        img=self.imgDict['Doc']
        tid=wx.TreeItemData()
        tid.SetData({'type':'Doc','name':'man'})
        tic=self.trlstRes.AppendItem(tip,'man',-1,-1,tid)
        self.trlstRes.SetItemImage(tic,img,0,which=wx.TreeItemIcon_Normal)
        self.trlstRes.SetItemText(tic,'X',1)
        self.trlstRes.SetItemText(tic,'Doc',2)
        img=self.imgDict['Doc']
        tid=wx.TreeItemData()
        tid.SetData({'type':'Doc','name':'man'})
        tic=self.trlstRes.AppendItem(tip,'oper',-1,-1,tid)
        self.trlstRes.SetItemImage(tic,img,0,which=wx.TreeItemIcon_Normal)
        self.trlstRes.SetItemText(tic,'X',1)
        self.trlstRes.SetItemText(tic,'Doc',2)
    def _buildFuncDoc(self,tipp):
        img=self.imgDict['Documents']
        tid=wx.TreeItemData()
        tid.SetData({'type':'Documents','name':'docs'})
        tip=self.trlstRes.AppendItem(tipp,'man',-1,-1,tid)
        self.trlstRes.SetItemImage(tip,img,0,which=wx.TreeItemIcon_Normal)
        self.trlstRes.SetItemText(tip,'X',1)
        self.trlstRes.SetItemText(tip,'Documents',2)
        img=self.imgDict['Doc']
        tid=wx.TreeItemData()
        tid.SetData({'type':'Doc','name':'man'})
        tic=self.trlstRes.AppendItem(tip,'man',-1,-1,tid)
        self.trlstRes.SetItemImage(tic,img,0,which=wx.TreeItemIcon_Normal)
        self.trlstRes.SetItemText(tic,'X',1)
        self.trlstRes.SetItemText(tic,'Doc',2)
        img=self.imgDict['Doc']
        tid=wx.TreeItemData()
        tid.SetData({'type':'Doc','name':'man'})
        tic=self.trlstRes.AppendItem(tip,'oper',-1,-1,tid)
        self.trlstRes.SetItemImage(tic,img,0,which=wx.TreeItemIcon_Normal)
        self.trlstRes.SetItemText(tic,'X',1)
        self.trlstRes.SetItemText(tic,'Doc',2)
    def _buildResTree(self,tip,d):
        keys=d.keys()
        keys.sort()
        for k in keys:
            if k=='__':
                continue
            sType,tupKey=k
            dd=d[k]
            tid=wx.TreeItemData()
            tid.SetData(dd)
            if sType not in self.imgDict:
                vtLog.vtLngCurWX(vtLog.ERROR,'k:%s;dd:%s'%(repr(k),vtLog.pformat(d)),self)
                continue
            img=self.imgDict[sType]
            tic=self.trlstRes.AppendItem(tip,' '.join(tupKey),-1,-1,tid)
            self.trlstRes.SetItemImage(tic,img,0,which=wx.TreeItemIcon_Normal)
            self.trlstRes.SetItemText(tic,sType,2)
            if '__' in dd:
                self.trlstRes.SetItemText(tic,'X',1)
                ddd=dd['__']
                ddd['__']=True
                if 'fn' in ddd:
                    self.trlstRes.SetItemText(tic,ddd['fn'],3)
                if 'line' in ddd:
                    self.trlstRes.SetItemText(tic,str(ddd['line']),4)
                #if func is not None:
                #    func(tic)
                self._buildResTree(tic,dd)
        return
        for sType,sImg,func in [
                    ['__modules','ModPy',self._buildModDoc],
                    ['classPy','classPy',self._buildClassDoc],
                    ['methodPy','methodPy',None],#self._buildMethodDoc],
                    ['funcPy','funcPy',None],#self._buildFuncDoc],
                ]:
            if sType in d:
                keys=d[sType].keys()
                keys.sort()
                for k in keys:
                    dd=d[sType][k]
                    tid=wx.TreeItemData()
                    tid.SetData(dd)
                    img=self.imgDict[sImg]
                    tic=self.trlstRes.AppendItem(tip,k,-1,-1,tid)
                    self.trlstRes.SetItemImage(tic,img,0,which=wx.TreeItemIcon_Normal)
                    self.trlstRes.SetItemText(tic,'X',1)
                    self.trlstRes.SetItemText(tic,sImg,2)
                    if 'fn' in dd:
                        self.trlstRes.SetItemText(tic,dd['fn'],3)
                    if 'line' in dd:
                        self.trlstRes.SetItemText(tic,str(dd['line']),4)
                    if func is not None:
                        func(tic)
                    self._buildResTree(tic,dd)
            
    def OnFindFinished(self,evt):
        self.cbRefresh.Enable(True)
        self.cbBuild.Enable(True)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'dRes:%s'%(vtLog.pformat(self.dRes)),self)
        self.trlstRes.DeleteAllItems()
        img=self.imgDict['ModPy']
        tid=wx.TreeItemData()
        tid.SetData(self.dRes)
        k=self.dRes['data'].keys()[0]
        ti=self.trlstRes.AddRoot(' '.join(k[1]),-1,-1,tid)
        self.trlstRes.SetItemImage(ti,img,0,which=wx.TreeItemIcon_Normal)
        self.trlstRes.SetItemText(ti,'X',1)
        self.trlstRes.SetItemText(ti,'ModPy',2)
        #self.trlstRes.SetItemText(ti,self.dRes[k]['__']['fn'],3)
        #self._buildModDoc(ti)
        self._buildResTree(ti,self.dRes['data'][k])
        self.trlstRes.Expand(ti)
    def OnFindAborted(self,evt):
        self.cbRefresh.Enable(True)
        self.cbBuild.Enable(True)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'dRes:%s'%(vtLog.pformat(self.dRes)),self)
        self.trlstRes.DeleteAllItems()
    def _add(self,key,iVal):
        try:
            val=self.dSum[key]
        except:
            val=0
        self.dSum[key]=val+iVal
    def _calcArgs(self,sSrc):
        s=sSrc.strip()
        i=s.find('(')
        if i>0:
            j=s.find(')')
            sArgs=s[i+1:j].split(',')
            sName=s[:i]
            lArgs=[[sT.strip() for sT in sTmp.split('=')] for sTmp in sArgs]
        else:
            sName=s[:-1]
            lArgs=None
        return sName,lArgs
    def _analyse(self,fn,sDN):
        try:
            if vtLog.vtLngIsLogged(vtLog.INFO):
                vtLog.vtLngCurWX(vtLog.INFO,'dn:%s;fn:%s'%(sDN,fn),self)
            sFN=fn.replace('\\','/')
            f=open(fn)
            lines=f.readlines()
            for sExt in self.LST_SRC_EXT:
                iLen=len(sExt)
                if fn[-iLen:]==sExt:
                    ext=sExt[1:]
                    break
            iLines=len(lines)
            self._add('_lines',iLines)
            self._add('_files',1)
            self._add('%s_lines'%ext,iLines)
            self._add('%s_files'%ext,1)
            if ext=='py':
                sRelFN=sFN[len(sDN)+1:]
                sModules=sRelFN[:-3].replace('/','.')
                lModules=sModules.split('.')
                vtLog.PrintMsg(_('analyse (%05d - %05d) %s')%(self.iAct,self.iCount,sRelFN),self.widPrint)
                lRes=[]
                
                iIndent=0
                def calcIndent(s):
                    try:
                        i=0
                        while s[i] in ['\t',' ']:
                            i+=1
                        return i
                    except:
                        i=0
                        return 0
                iLine=0
                for iLine in xrange(iLines):
                    l=lines[iLine]
                    i=calcIndent(l)
                    iIndentNew=i
                    iIndent=iIndentNew
                    if l[i:i+6]=='class ':
                        self._add('%s_objs'%ext,1)
                        iLineAdd=0
                        s=l[i+6:]
                        while s.find(':')<0:
                            iLineAdd+=1
                            if (iLineAdd+iLine)>=iLines:
                                iLineAdd=-1
                                break
                            s=s+lines[iLine+iLineAdd].strip()
                        if iLineAdd>=0:
                            sName,lArgs=self._calcArgs(s)
                            lRes.append(
                                [('classPy',(sName,)),
                                {'tag':sName,
                                 'mod':sModules,
                                 'fnSrc':sRelFN,
                                 'line':iLine,
                                 'indent':iIndentNew,
                                 'inheritance':lArgs}])
                            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                vtLog.vtLngCur(vtLog.DEBUG,'fn:%s;line:%5d;indent:%d;type:class;name:%s;args:%s'%
                                    (fn,iLine,iIndentNew,sName,vtLog.pformat(lArgs)),
                                    'vNodeMetricPyPanel_thdFind')
                    if l[i:i+4]=='def ':
                        self._add('%s_methodes'%ext,1)
                        iLineAdd=0
                        s=l[i+4:]
                        while s.find(':')<0:
                            iLineAdd+=1
                            if (iLineAdd+iLine)>=iLines:
                                iLineAdd=-1
                                break
                            s=s+lines[iLine+iLineAdd].strip()
                        if iLineAdd>=0:
                            sName,lArgs=self._calcArgs(s)
                            lRes.append(
                                [('funcPy',(sName,)),
                                {'tag':sName,
                                 'mod':sModules,
                                 'fnSrc':sRelFN,
                                 'line':iLine,
                                 'indent':iIndentNew,
                                 'args':lArgs}])
                            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                vtLog.vtLngCur(vtLog.DEBUG,'fn:%s;line:%5d;indent:%d;type:func/method;name:%s;args:%s'%
                                    (fn,iLine,iIndentNew,sName,vtLog.pformat(lArgs)),
                                    'vNodeMetricPyPanel_thdFind')
                    iLine+=1
                d=self.dRes['data']#['ModPy']
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'fn:%s;lModules:%s;lRes:%s'%
                            (fn,vtLog.pformat(lModules),vtLog.pformat(lRes)),
                            'vNodeMetricPyPanel_thdFind')
                #for sMod in lModules[:-1]:
                for iOfs in xrange(len(lModules)):
                    #dMod=d['__modules']
                    sMod=lModules[iOfs]
                    kMod=('ModPy',(sMod,))
                    if kMod not in d:
                        if iOfs>0:
                            sModules=u'.'.join(lModules[:iOfs+1])
                            sRelModFN=u'/'.join(lModules[:iOfs+1])
                        else:
                            sModules=u''
                            sRelModFN=sRelFN
                        d[kMod]={'__':{'tag':sMod,'fnSrc':sRelModFN,'mod':sModules},
                            ('Documents',('docs',)):{'__':{'tag':'docs'},
                                ('Doc',('man',)):{'__':{
                                        'tag':'man',('name','language','en'):'manual',('name','language','de'):'Handbuch'}},
                                ('Doc',('oper',)):{'__':{
                                        'tag':'oper',('name','language','en'):'operation',('name','language','de'):'Funktionsweise'}},
                                }
                            }
                    d=d[kMod]
                #d['classPy']={}
                #d['funcPy']={}
                iCount=len(lRes)
                lIndent=[0]
                lType=['']
                lTag=['']
                for i in xrange(iCount):
                    tup,item=lRes[i]
                    type,keys=tup
                    iIndent=item['indent']
                    
                    while lIndent[-1]>iIndent:
                        del lIndent[-1]
                        del lType[-1]
                        del lTag[-1]
                        #lIndent=lIndent[:-1]
                        #lType=lType[:-1]
                    if lIndent[-1]<iIndent:
                        lIndent.append(iIndent)
                        lType.append(type)
                        lTag.append(keys)#['tag'])
                    else:
                        lType[-1]=type#item['type']
                        lTag[-1]=keys#['tag']
                    if len(lType)>1:
                        if lType[-1]=='funcPy':
                            if lType[-2]=='classPy':
                                #item['type']='methodPy'
                                lRes[i][0]=('methodPy',keys)
                                lType[-1]='methodPy'
                    dd=d
                    #if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    #    vtLog.vtLngCur(vtLog.DEBUG,'fn:%s;lType:%s;lTag:%s'%
                    #            (fn,vtLog.pformat(lType),vtLog.pformat(lTag)),
                    #            'vNodeMetricPyPanel_thdFind')
                    for kTup in zip(lType,lTag):
                        if kTup==('',''):
                            continue
                        if kTup not in dd:
                            dd[kTup]={}
                        #ddd=dd[sType]
                        #if sTag not in ddd:
                        #    ddd[sTag]={}
                        dd=dd[kTup]
                    #dd.update(item)
                    dd['__']=item
                    del item['indent']
                    item['line']=unicode(item['line'])
                    if 'args' in item:
                        item['args']=' , '.join([' = '.join(t) for t in item['args']])
                    if 'inheritance' in item:
                        if item['inheritance'] is None:
                            del item['inheritance']
                        else:
                            item['inheritance']=' , '.join([' = '.join(t) for t in item['inheritance']])
                    if 1:
                        if type in ['classPy']:#,'methodPy','funcPy']:
                            dd[('Documents',('docs',))]={'__':{'tag':'docs'},
                                ('Doc',('man',)):{'__':{'tag':'man',
                                    ('name','language','en'):'manual',('name','language','de'):'Handbuch'}},
                                ('Doc',('oper',)):{'__':{'tag':'oper',
                                    ('name','language','en'):'operation',('name','language','de'):'Funktionsweise'}},
                                }
                            
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCur(vtLog.DEBUG,'fn:%s;lModules:%s;d:%s'%
                            (fn,vtLog.pformat(lModules),vtLog.pformat(d)),
                            'vNodeMetricPyPanel_thdFind')
                    #vtLog.vtLngCur(vtLog.DEBUG,'fn:%s;lModules:%s;dRes:%s'%
                    #        (fn,vtLog.pformat(lModules),vtLog.pformat(self.dRes)),
                    #        'vNodeMetricPyPanel_thdFind')
                time.sleep(0.05)
        except:
            vtLog.vtLngTB('vNodeMetricPyPanel_thdFind')
    def _refresh(self,sDN):
        try:
            self.iCount=0
            self.iAct=0
            vtLog.PrintMsg(_('find files'),self.widPrint)
            l=sDN.split(os.path.sep)
            iSkip=len(l)-1
            #lMod=[l[-1]]
            for root, dirs, files in os.walk(sDN):
                #self.iCount+=len(files)
                #print root
                #l=root.split(os.path.sep)
                #sMod=u'.'.join(l[iSkip:])
                #print '   ',dirs
                #print '   ',files
                for fn in files:
                    for sExt in self.LST_SRC_EXT:
                        iLen=len(sExt)
                        if fn[-iLen:]==sExt:
                            #if sExt=='.py':
                            #    if fn[:6]=='images':
                            #        break
                            #self.files.append((os.path.join(root,fn),sMod))
                            self.files.append(os.path.join(root,fn))
                            break
                if 'CVS' in dirs:
                    dirs.remove('CVS')
            self.iCount=len(self.files)
            self.iAct=0
            for fn in self.files:
                sTmpDN=sDN.replace('\\','/')
                i=sTmpDN.rfind('/')
                if i>0:
                    self._analyse(fn,sTmpDN[:i])
                self.iAct+=1
            vtLog.PrintMsg(_('analyse finished'),self.widPrint)
        except:
            vtLog.vtLngTB('vNodeMetricPyPanel_thdFind')
    def OnTreeNodeTreeRight(self,evt):
        tn=evt.GetItem()
        tid=self.trlstRes.GetPyData(tn)
        sVal=self.trlstRes.GetItemText(tn,1)
        if len(sVal)==0:
            self.trlstRes.SetItemText(tn,'X',1)
            self.__setChildsSel__(tn,True)
            try:
                tid['__']['__']=True
            except:
                pass
        else:
            self.trlstRes.SetItemText(tn,'',1)
            self.__setChildsSel__(tn,False)
            try:
                tid['__']['__']=False
            except:
                pass
    def __setChildsSel__(self,ti,flag):
        triChild=self.trlstRes.GetFirstChild(ti)
        while triChild[0].IsOk():
            if flag:
                self.trlstRes.SetItemText(triChild[0],'X',1)
            else:
                self.trlstRes.SetItemText(triChild[0],'',1)
            tid=self.trlstRes.GetPyData(triChild[0])
            try:
                tid['__']['__']=flag
            except:
                pass
            if self.trlstRes.ItemHasChildren(triChild[0]):
                self.__setChildsSel__(triChild[0],flag)
            triChild=self.trlstRes.GetNextChild(ti,triChild[1])
