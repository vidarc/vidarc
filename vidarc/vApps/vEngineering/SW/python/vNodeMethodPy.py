#----------------------------------------------------------------------------
# Name:         vNodeMethodPy.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20061118
# CVS-ID:       $Id: vNodeMethodPy.py,v 1.6 2009/01/24 23:52:09 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.ext.report.veRepDocBase import veRepDocBase
from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeMethodPy(veRepDocBase,vXmlNodeEngineering):
    NODE_ATTRS=vXmlNodeEngineering.NODE_ATTRS+[
        ('File',None,'fnSrc',None),
        ('Line',None,'line',None),
        ('Module',None,'module',None),
        ]
    def __init__(self,tagName='methodPy'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
        veRepDocBase.__init__(self,self.__class__.__name__)
    def GetDescription(self):
        return _(u'software method python')
    # ---------------------------------------------------------
    # specific
    def GetFile(self,node):
        return self.Get(node,'fnSrc')
    def GetLine(self,node):
        return self.Get(node,'line')
    def GetModule(self,node):
        return self.GetML(node,'module')
    def GetArgs(self,node):
        return self.Get(node,'args')
    def GetArgsDesc(self,node):
        return self.Get(node,'argsDesc')
    def GetReturn(self,node):
        return self.Get(node,'return')
    def GetExceptions(self,node):
        return self.Get(node,'exceptions')
    def IsSkipProcessing(self,node):
        if self.Get(node,'skipProc') in ['true']:
            return True
        return False
    def SetFile(self,node,val):
        self.Set(node,'fnSrc',val)
    def SetLine(self,node,val):
        self.Set(node,'line',val)
    def SetModule(self,node,val):
        self.Set(node,'module',val)
    def SetArgs(self,node,val):
        self.Set(node,'args',val)
    def SetArgsDesc(self,node,val):
        self.Set(node,'argsDesc',val)
    def SetReturn(self,node,val):
        return self.Set(node,'return',val)
    def SetExceptions(self,node,val):
        return self.Set(node,'exceptions',val)
    def BuildLang(self):
        vXmlNodeEngineering.BuildLang(self)
        #print self.dCfg
    def GetCfgDict(self,nodeCfg,lang):
        return {
            u'mod':[
                    _(u'01 module'),u'mod',-1,
                    {'val':u'','type':'text'},{'val':u'','type':'text'}],
            u'fnSrc':[
                    _(u'02 file'),u'fnSrc',-2,
                    {'val':u'','type':'text'},{'val':u'','type':'text'}],
            u'line':[
                    _(u'03 line'),u'line',-3,
                    {'val':u'','type':'long'},{'val':u'','type':'long'}],
            u'args':[
                    _(u'04 arguments'),u'args',-4,
                    {'val':u'','type':u'text'},{'val':u'','type':u'text'}],
            u'argsDesc':[
                    _(u'05 arguments description'),u'argsDesc',-5,
                    {'val':u'','type':u'text'},{'val':u'','type':u'text'}],
            u'return':[
                    _(u'06 return'),u'return',-6,
                    {'val':u'','type':u'text'},{'val':u'','type':u'text'}],
            u'exceptions':[
                    _(u'07 exceptions'),u'exception',-7,
                    {'val':u'','type':u'text'},{'val':u'','type':u'text'}],
            u'skipProc':[
                    _(u'90 skip processing'),u'skipProc',-90,
                    {'val':u'false','type':'choice','it00':'true','it01':'false'},
                    {'val':u'false','type':'choice','it00':'true','it01':'false'}],
            }
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01?IDAT8\x8d\xa5\x92\xbdJ\x03A\x14\x85\xbf\x99]\x15\x12\x15b\x10\x0b\
\xc1X\xa4PI\xe1\x1f\x1a\x10V_@|\x08-l\x04\xdf@\xad|\x03\xb5\xf0\x01\x04kc!\
\xc4\x94b!1\x98\x14!\x18\x10\x95\x10\xc2V\x11\x13\xc2\xecX\xa8\x90\x8d\xbb\
\xee\xa2\xa7\x9ds\xbf{\xee\xbd#\x844\xf8\xd6\x9e\xa34\xc0\x814\x04!%\xba\x01\
\xdd\x90\xb0 \xb1\x0f:\xc8\xf4\x1b,4\xc0\x0ff\xfe\xa5\xc8\x95\xe0\xbf;p%\xf0\
\xba\xc2\xf2\x92\xd2\xf18d.\xbda\xb27foW\xcb\x9a\xa2\xd9\xdc\xf4M\xf0c\x84n\
\xadZJ\xa7\xd3 %\xdc=A\xed\x01\xee\x0b\xee\x06\xbe\x80\xb5\r\xa5\x93I\x88\
\x8eCi\x14\n)\x988\x87E\x1b\x8eOB\\\xa12\x03\xef\xebP/C\xbd\x08\xd1*\x98)0\
\xcbn\x9f\xf4.\x87\xe7CC\xac\xec\x82\xccB\x7f\x0b\xde.`d\x08ZvH\xc0\x96\xa3\
\xf4\x95\r\xaf%\x885`~\x07\x129\xc8e\xdc>\xdf\x11N\xa5!\xa8\xc0\xd8\x91\xd2\
\x0b\xc30\x99\x87\xc7*D"\x9f\xefs\xb3J;N\xc0\x15\x00\xa6o\x94N\x17a\xf0\x05:\
}`hh\xe4\xa1*\xe1\xf6\xcc\x10\x81\x80\xd8\xb6\xd2\xd1\x04t\xae\xa1\xdd\x86\
\x01\x01\xb5,\x88\xaf\xff\xf2\x01G"e\\\x14\xc2\x0f\xbe\x00\x00\x00\x00IEND\
\xaeB`\x82' 
    def Process(self,f,*args,**kwargs):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurCls(vtLog.DEBUG,'args:%s;kwargs:%s'%(vtLog.pformat(args),
                            vtLog.pformat(kwargs)),self)
            node=kwargs['node']
            if self.IsSkipProcessing(node):
                return
            if 'lang' not in kwargs:
                kwargs['lang']=self.lang
            else:
                self.__setLang__(kwargs['lang'])
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurCls(vtLog.DEBUG,'args:%s,kwargs:%s'%(vtLog.pformat(args),vtLog.pformat(kwargs)),self)
            lang=kwargs['lang']
            nodeBase=kwargs.get('__base_node',None)
            nodeRel=kwargs.get('__rel_node',None)
            self.__setLang__(lang)
            self.__getTranslation__()
            iLevel=kwargs['level']
            strs=[]
            s=':'.join([self.GetTrans('method'),self.GetTag(node)])
            strs.append(self.__getHeadLine__(iLevel,self.__tex__(s)))
            if 1:
                strs.append('\\begin{center}')
                strs.append('  \\begin{tabular}{p{0.3\linewidth}p{0.7\linewidth}}')
                strs.append('  %s: & %s \\\\'%(self.__tex__(self.GetTrans('name')),
                            self.__tex__(self.GetName(node,lang=lang))))
                #strs.append('  \\textbf{%s}: & %s \\\\'%(self.__tex__(self.GetTrans('file')),
                #            self.__tex__(self.GetFile(node))))
                #strs.append('  %s: & %s \\\\'%(self.__tex__(self.GetTrans('line')),
                #            self.__tex__(self.GetLine(node))))
                s=self.GetArgs(node)
                l=s.split(',')
                sDesc=self.GetArgsDesc(node)
                lDesc=sDesc.split(',')
                iLen1=len(l)
                if iLen1>0:
                    #print s,l
                    strs.append('    %s: &  \\\\'%(self.__tex__(self.GetTrans('arguments'))))
                    iLen2=len(lDesc)
                    for iOfs in xrange(iLen1):
                        s=l[iOfs]
                        if iOfs<iLen2:
                            sDesc=lDesc[iOfs]
                        else:
                            sDesc=u''
                        strs.append('  \\hspace{0.3cm} %s & %s \\\\'%(self.__tex__(s),self.__tex__(sDesc)))
                s=self.GetReturn(node)
                if len(s)>0:
                    l=s.split(',')
                    if len(l)>1:
                        strs.append('  %s: &  \\\\'%(self.__tex__(self.GetTrans('return'))))
                        for s in l:
                            strs.append('  \\hspace{0.3cm} %s & %s \\\\'%(self.__tex__(''),
                                        self.__tex__(s)))
                        
                    else:
                        strs.append('  %s: & %s \\\\'%(self.__tex__(self.GetTrans('return')),
                                    self.__tex__(s)))
                s=self.GetExceptions(node)
                if len(s)>0:
                    l=s.split(',')
                    strs.append('  %s: &  \\\\'%(self.__tex__(self.GetTrans('exceptions'))))
                    for s in l:
                        strs.append('  \\hspace{0.3cm} %s & %s \\\\'%(self.__tex__(s),
                                    self.__tex__('')))      # FIXME: add reference
                strs.append('  \\end{tabular}')
                strs.append('\\end{center}')
            else:
                strs.append('\\begin{verbatim}')
                strs.append('%-25s: %s '%(self.__tex__(self.GetTrans('name')),
                            self.__tex__(self.GetName(node,lang=lang),True)))
                s=self.GetArgs(node)
                l=s.split(',')
                sDesc=self.GetArgsDesc(node)
                lDesc=sDesc.split(',')
                iLen1=len(l)
                if iLen1>0:
                    #print s,l
                    strs.append('%-25s:'%(self.__tex__(self.GetTrans('arguments'))))
                    iLen2=len(lDesc)
                    for iOfs in xrange(iLen1):
                        s=l[iOfs]
                        if iOfs<iLen2:
                            sDesc=lDesc[iOfs]
                        else:
                            sDesc=u''
                        strs.append('  %-23s: %s'%(self.__tex__(s),self.__tex__(sDesc)))
                s=self.GetReturn(node)
                if len(s)>0:
                    l=s.split(',')
                    if len(l)>1:
                        strs.append('%-25s: '%(self.__tex__(self.GetTrans('return'))))
                        for s in l:
                            strs.append('  %-23s: %s'%(self.__tex__(''),
                                        self.__tex__(s)))
                        
                    else:
                        strs.append('%-25s: %s '%(self.__tex__(self.GetTrans('return')),
                                    self.__tex__(s)))
                s=self.GetExceptions(node)
                if len(s)>0:
                    l=s.split(',')
                    strs.append('%-25s: '%(self.__tex__(self.GetTrans('exceptions'))))
                    for s in l:
                        strs.append('  %-23s: %s'%(self.__tex__(s),
                                    self.__tex__('')))      # FIXME: add reference
                strs.append('\\end{verbatim}')
            strs.append(self.__tex__(self.GetDesc(node,lang=lang)))
            #,lang=lang
            self.__writeLst__(f,strs)
            self.__includeDoc__(f,*args,**kwargs)
            self.__includeNodes__(['funcPy','methodPy'],f,*args,**kwargs)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
