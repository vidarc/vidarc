#----------------------------------------------------------------------------
# Name:         regPython.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20061118
# CVS-ID:       $Id: regPython.py,v 1.5 2013/05/20 12:08:22 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust

if vcCust.is2Import(__name__,'__REG_NODE__',True):
    from vidarc.vApps.vEngineering.SW.python.vNodeModPy import vNodeModPy
    from vidarc.vApps.vEngineering.SW.python.vNodeClassPy import vNodeClassPy
    from vidarc.vApps.vEngineering.SW.python.vNodeMethodPy import vNodeMethodPy
    from vidarc.vApps.vEngineering.SW.python.vNodeFuncPy import vNodeFuncPy
    from vidarc.vApps.vEngineering.SW.python.vNodeMetricPy import vNodeMetricPy
    import vidarc.vApps.vEngineering.__link__

def RegisterNodes(doc):
    try:
        if vcCust.is2Import(__name__,'__REG_NODE__',True):
            #oBase=doc.GetReg(vNodeSW()
            #doc.RegisterNode(oBase,False)
            for o in [vNodeModPy(),vNodeClassPy(),vNodeMethodPy(),vNodeFuncPy(),vNodeMetricPy(),]:
                doc.RegisterNode(o,False)
            vidarc.vApps.vEngineering.__link__.LinkNodes(doc,[
                    ('sw',['ModPy',]),
                    ('ModPy',['ModPy','classPy','funcPy','MetricPy','Documents','Doc',
                    'Fct','Prc','Bug','Test','Tool','ToDo',]),
                    ('classPy',['methodPy','t_array','t_class','t_struct','t_list',
                            't_bool','t_char','t_float','t_int','t_long','t_string',
                            'Documents','Doc','drawTiming','drawNode',
                            'Fct','Prc','Bug','Test','Tool','ToDo',]),
                    ('methodPy',['Documents','Doc',
                            'Fct','Prc','Bug','Test','Tool','ToDo',]),
                    ('funcPy',['Documents','Doc',
                            'Fct','Prc','Bug','Test','Tool','ToDo',]),
                ])
    except:
        vtLog.vtLngTB(__name__)
