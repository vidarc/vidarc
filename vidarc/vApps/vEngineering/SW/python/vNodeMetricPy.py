#----------------------------------------------------------------------------
# Name:         vNodeModPy.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20061118
# CVS-ID:       $Id: vNodeMetricPy.py,v 1.1 2006/11/21 21:22:25 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import platform

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeTag import *
try:
    if vcCust.is2Import(__name__):
        from vNodeMetricPyPanel import *
        from vNodeMetricPyEditDialog import *
        from vNodeMetricPyAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeMetricPy(vtXmlNodeTag):
    def __init__(self,tagName='MetricPy'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'software python metric')
    # ---------------------------------------------------------
    # specific
    def GetSrcDN(self,node):
        tup=platform.uname()
        sHost=tup[1]
        sDN=self.GetAttrVal(node,'srcDN','host',sHost)
        if len(sDN)>0:
            return sDN
        else:
            return self.GetVal(node,'srcDN',unicode,'')
    def SetSrcDN(self,node,sDN):
        tup=platform.uname()
        sHost=tup[1]
        sDN=self.SetAttrVal(node,'srcDN',sDN,'host',sHost)
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02\x98IDAT8\x8d\x8d\x93\xcbK\xd4q\x14\xc5?\xdf\xdfc\x9aq\x98\x1c\x1du\
\xb4tF\xf0\x95\x8e\x9a\x8e%>\xb2\x07-\xd2$h\x91\xb9\t\x83h\xd3\xffP\x10\xad\
\xa3 \x84V\x05\x11\x11\xa2DA\xe5`\xd2"zJR\x99\x8ec>0\xb2R\xc8\x07\x8a\xe38\
\xbf\xf9\xfd\xbe-\x02\xc5\x86\x1e\x07\xee\xe6\xc2\xe1\x9c{\xef\xb9\x08E\xe5\
\x7f\xaa\xa8\xa4H\xb6\x9dh\x97\x95e\xe5\xd2\xe3\xf1\xc8;7\xee\xca\xda\x9a=R\
\x15B\xe1_\xc8\xf2d\xc8\x8e\xb3gx\xfb\xfa\r\r\x8dML|\x1agvy\x8e\xf7\xef\xde#\
\x84\xa2\xfe\x91\xd8\xd1qJ\x86zC4\xb76\xa3\x99\x90\xeau3\xf1n\x92\xb4\xbcT\
\x9e\xf5\xbf\xe4\xeb\xecW\xfe*?2<\xc2\xc1C\x07)\xc8/\xe0\xe9\x8b\xe7\\\xbd\
\xdc)j\x0f\xd71\x1a\xfeB]S\x03\xba\xa6\xa3\x01\xe8\x9a*\x15\xd5Gl\xcd\x81\
\xa6;0\xcdA\xd1\xda\xda"\x0b\xf3\x0b\xe9\x7f\xd2O8\x12fzjZ\xf8|\xb9\xd2\x85\
\x83\xfa\xbd\xd5\xf4>~D4\x16\x13\xc2\xe9tJ[\xfa9\x16\x97J@\xd8\xa8*\xb3S\xb3\
k\x8c\xc1\x0f\xf7\t\x0f\x0fc\x98\xa6\x00h9vD\x06\x03A\x1e\x86\xfa\x18\x1b\
\xfbHl-.\x004\xe9ib\xd1\xdf\x88\xc7\xee`\x9fo\x8e\xf9\xf8\x02\xb7?\x07\x88\
\xcf\x8f \xcc\x0fbc\x9c\xc1\x112=9D\xc6\x86X_36\xfaZ4\xc7\x0fe~\xe2\xf6\x05\
\x1e\x18\xa5\x90\xa2@\x8a\x06\xb6\x00\xcc\xf4l\xec\xe3x{\x1b=\xdd\xdd[\xc8\
\x00\x1a\x99N\x08f\xb2bz\xd1z.\x91\xea\xd6\xf0}\xb9Gt1A<\xcf)1U\xbcy9\xd8Q\
\xc9\xc9H\xf0}FJ\x89\xd8t\xe0\x9d\x1b`\xb12\x0b\xbd\xff\x1eG\x13\n\x13\x917\
\x14\x97\xb8\xa1H!Eh\xa0X\xd4\xd6\x1f#\xd4\x17\xc2\xed\xb6\xa3\xe8`\x1a\x9b\
\x0eTc~\xf6\xa2+\xf2\x8a\x8a\xd9\x18U\xd9\xe5\x14{\x82\xcc,\xfd P\x1ce[\xba\
\x8e\xd7\xe5"\xba\xea\xc2\x88~cu\xc5b|ja\xcb\x08\x8aa\x98B\x1b\x1a\xe5t\xe5I\
J\xb3k((\xdcM{\xf5y\xe6\xa6+\xf1\xa7\xb9\xd8YR\xcb\xe4\xe4\x00q\xd5b\x7f\xbd\
7)+\x9a\xddf\x93\xcd\xf5m\x04\xbc\x15Xj\x02%a\xc3R\x13\x14\xec\xe8\xe4\xe1\
\xd0\x05,W\x18\x7f\xae\x83"\xbf\x1d\x9b+9w\xda\xba\xb1\xcevw\x16]o\xafcY\xfc\
\x8a\xa6\x02\x08\xc0\xca&\xa6\x0e\xd0\xde\x9c\x81\x01\xe8"\x89\x9f\xfc\x0b\
\x87\x0f\xe4\xcakW\x1a\xc1\xd2A5\xc0\xd0A7\xc0TA5\t\x04\xbb~;\xe3o\xf8<\xb3\
\xcc\xcd[\x91d\xa9?\xe0\'\xae\xc4\xef~n\xdb\'\xc5\x00\x00\x00\x00IEND\xaeB`\
\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return vNodeMetricPyEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return vNodeMetricPyAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vNodeMetricPyPanel
        else:
            return None
