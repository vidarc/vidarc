#----------------------------------------------------------------------------
# Name:         vNodeClassPy.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20061118
# CVS-ID:       $Id: vNodeClassPy.py,v 1.7 2009/01/24 23:52:09 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.ext.report.veRepDocBase import veRepDocBase
from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeClassPy(veRepDocBase,vXmlNodeEngineering):
    NODE_ATTRS=vXmlNodeEngineering.NODE_ATTRS+[
        ('File',None,'fnSrc',None),
        ('Line',None,'line',None),
        ('Module',None,'mod',None),
        ('Inheritance',None,'inheritance',None),
        ]
    def __init__(self,tagName='classPy'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
        veRepDocBase.__init__(self,self.__class__.__name__)
    def GetDescription(self):
        return _(u'software class python')
    # ---------------------------------------------------------
    # specific
    def GetFile(self,node):
        return self.Get(node,'fnSrc')
    def GetLine(self,node):
        return self.Get(node,'line')
    def GetModule(self,node):
        return self.GetML(node,'mod')
    def GetInheritance(self,node):
        return self.Get(node,'inheritance')
    def IsSkipProcessing(self,node):
        if self.Get(node,'skipProc') in ['true']:
            return True
        return False
    def SetFile(self,node,val):
        self.Set(node,'fnSrc',val)
    def SetLine(self,node,val):
        self.Set(node,'line',val)
    def SetModule(self,node,val):
        self.Set(node,'mod',val)
    def SetInheritance(self,node,val):
        self.Set(node,'inheritance',val)
    def BuildLang(self):
        vXmlNodeEngineering.BuildLang(self)
        #print self.dCfg
    def GetCfgDict(self,nodeCfg,lang):
        return {
            u'mod':[
                    _(u'01 module'),u'mod',-1,
                    {'val':u'','type':'text'},{'val':u'','type':'text'}],
            u'fnSrc':[
                    _(u'02 file'),u'fnSrc',-2,
                    {'val':u'','type':'text'},{'val':u'','type':'text'}],
            u'line':[
                    _(u'03 line'),u'line',-3,
                    {'val':u'','type':'long'},{'val':u'','type':'long'}],
            u'inheritance':[
                    _(u'04 inheritance'),u'inheritance',-4,
                    {'val':u'','type':'text'},{'val':u'','type':'text'}],
            u'skipProc':[
                    _(u'90 skip processing'),u'skipProc',-90,
                    {'val':u'false','type':'choice','it00':'true','it01':'false'},
                    {'val':u'false','type':'choice','it00':'true','it01':'false'}],
            }
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01aIDAT8\x8d\x8d\x93\xbdJ\x03A\x14\x85\xbf\x99]\x15\x12\x15"\x88\x85`,\
RHH\xe1_\xb1 D,\x15,|\x06-l\x04\xdf\xc27H\xe3\x0b\x08\xd6\xda\x081u\nY\xc4\
\xa4\x08!\x01Q\t!\xa4\x8a\x98\x10f\xc7B\xd7\xcc\x8e\x0ex\xab\xcb=\xf7\xef\
\x9c;#\x84\xf4\x88MGJ\xc7\xbe\x90\x9e\xe0\x1f\xe6\x9b\x85f\x91\x1d3\x9b\x9bq\
\xdf\x0e\xb8\n\xed\x8dt\xa4\xb4\x90\x9e\x10\x806\x93][\xd8X\x8cK\xbb\xa3\x8b\
\xab\x0bKPp\xadkod\xe6\xc8\xbf@\xd3\x0f\x028\xd8W\xda\x1616iO\xb6\xc5+\x16\
\xd7\x18\x0c\x8e\x13TLM\x84\x90\x9eS\xe9\xdd\xa2\xd2A\x00R\xc2\xc33t\x9e \
\x0c\x93\xa2\n\xe0\x17/\x1d)\xbdw\x04\xb9\x1c\xa4\x97\xa1\xbe\x08\x8f\x05X\
\xb9\x86\xed>\x94J\x93|\xdf\x14\xd0\xe4\xd9\xcc\xc3\xc7!t\x1b\xd0\xadA\xba\r\
~\x01\xfcFRtiO\x8f\xfd\x97\x0bO\xec\x9c\x83,\xc3\xf4\x10\xdeo`a\x0e\x86\xfdd\
\xeeO\x03[\xc4\x93H\xe9\xbb>\xbc\xd5!\xd3\x83\xcd3\xc8V\xa0r\x9b\xbc\x82\xef\
ze\x97\x80hzb\xa9\xa4\xf4\xd6<\xac\x86\xd0jC*\xf5\x85o\xac+\x1dE \xe2\xdf\
\xe8\xfaP\xf9*\x045\x98}\x85\xf1\x14x\x1az!\xb4%T\xaf\xbe\xcfh\x9a\xfd\xda2\
\xa7J\xa7\xb30\xbe\x87\xd1\x08f\x04t\xca\x93A\x9f\x8b\x12\xc4\xe8\x1f\xcb\
\xb4=\x00\x00\x00\x00IEND\xaeB`\x82' 
    def Process(self,f,*args,**kwargs):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurCls(vtLog.DEBUG,'args:%s;kwargs:%s'%(vtLog.pformat(args),
                            vtLog.pformat(kwargs)),self)
            node=kwargs['node']
            if self.IsSkipProcessing(node):
                return
            if 'lang' not in kwargs:
                kwargs['lang']=self.lang
            else:
                self.__setLang__(kwargs['lang'])
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurCls(vtLog.DEBUG,'args:%s,kwargs:%s'%(vtLog.pformat(args),vtLog.pformat(kwargs)),self)
            lang=kwargs['lang']
            nodeBase=kwargs.get('__base_node',None)
            nodeRel=kwargs.get('__rel_node',None)
            self.__setLang__(lang)
            self.__getTranslation__()
            iLevel=kwargs['level']
            strs=[]
            s=':'.join([self.GetTrans('class'),self.GetTag(node)])
            strs.append('\\newpage')
            strs.append(self.__getHeadLine__(iLevel,self.__tex__(s)))
            if 1:
                strs.append('\\begin{center}')
                strs.append('  \\begin{tabular}{p{0.3\linewidth}p{0.7\linewidth}}')
                strs.append('  %s: & %s \\\\'%(self.__tex__(self.GetTrans('name')),
                            self.__tex__(self.GetName(node,lang=lang))))
                strs.append('  %s: & %s \\\\'%(self.__tex__(self.GetTrans('module')),
                            self.__tex__(self.GetModule(node))))
                strs.append('  %s: & %s \\\\'%(self.__tex__(self.GetTrans('file')),
                            self.__tex__(self.GetFile(node))))
                #strs.append('  %s: & %s \\\\'%(self.__tex__(self.GetTrans('line')),
                #            self.__tex__(self.GetLine(node))))
                s=self.GetInheritance(node)
                l=s.split(',')
                if len(l)>0:
                    if len(l[0].strip())>0:
                        strs.append('  %s: & %s \\\\'%(self.__tex__(self.GetTrans('inheritance')),
                                    self.__tex__(s)))
                        for s in l[1:]:
                            if len(s.strip())>0:
                                strs.append('  & %s \\\\'%(self.__tex__(s)))
                strs.append('  \\end{tabular}')
                strs.append('\\end{center}')
            else:
                strs.append('\\begin{verbatim}')
                strs.append('%-25s: %s'%(self.__tex__(self.GetTrans('name')),
                            self.__tex__(self.GetName(node,lang=lang))))
                strs.append('%-25s: %s'%(self.__tex__(self.GetTrans('module')),
                            self.__tex__(self.GetModule(node))))
                strs.append('%-25s: %s'%(self.__tex__(self.GetTrans('file')),
                            self.__tex__(self.GetFile(node),True)))
                strs.append('%-25s: %s'%(self.__tex__(self.GetTrans('line')),
                            self.__tex__(self.GetLine(node))))
                s=self.GetInheritance(node)
                l=s.split(',')
                if len(l)>0:
                    strs.append('%-25s:'%(self.__tex__(self.GetTrans('inheritance'))))
                    for s in l:
                        strs.append('            %s'%(self.__tex__(s)))
                strs.append('\\end{verbatim}')
            strs.append(self.__tex__(self.GetDesc(node,lang=lang)))
            #,lang=lang
            self.__writeLst__(f,strs)
            self.__includeDoc__(f,*args,**kwargs)
            kkwargs=kwargs.copy()
            kkwargs.update({'level':kwargs['level']+10})
            self.__includeNodes__(['funcPy','methodPy'],f,*args,**kkwargs)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
