#----------------------------------------------------------------------------
# Name:         vNodeTypeArray.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: vNodeTypeArray.py,v 1.3 2006/09/27 04:20:51 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeTypeArray(vXmlNodeEngineering):
    def __init__(self,tagName='t_array'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'software array type')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x98IDAT8\x8d\x95SA\x0e\x800\x08\xa3\x8c\xff\xffX\xf1"KSAc\x93%[Ah\
\xd9\x04|Y!\xcf#\x8d\x00_P\xbe\xb8Bp\x82\x06;^\xb9\xe8*kR\x9eG\xd6^9\x98Y\
\xaaTU\xf2\xa6&\xf8\xd0}\xd8\xcd\x83\xf3\xbc\xebT\x8b;\xb3\x02\x1dv\xde6\xf6\
^W\x17+\xce;\xd9\xf0\x857;l\xe5a\xe1\x0bZ\xdc\xab\x92\xfa\xfa\x83\xedg\x9a\
\x03\xf3\x9a\xbb-\xb0\x8a\xeeENs\t>Lw\xcd\xf64\x06\xf8j\x1f\xd1TD\x9b\xc4$_\
\x07;]+\xf8wV\xb9\x93\r\x8e]GN\xa5vh\xa0\x99M\x00\x00\x00\x00IEND\xaeB`\x82' 
