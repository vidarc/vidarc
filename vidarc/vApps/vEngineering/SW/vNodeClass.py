#----------------------------------------------------------------------------
# Name:         vNodeClass.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: vNodeClass.py,v 1.8 2013/10/27 05:38:25 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeClass(vNodeEngBase):
    def __init__(self,tagName='class'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'software class')
    def GetTransDescription(self):
        return u'class'
    # ---------------------------------------------------------
    # specific
    def GetGrpDict(self):
        return {
            None:0,
            'method':[-1,None,]
            }
    def GetProcessNodeInc(self):
        return [
            {'lTag':['method','itfc','api','var','const','Prop','Mod','Base'],'level':1,},
            #{'lTag':['method'],'level':10,'headline':u'method(s)'},
            ]
    def GetCfgDictOld(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'skipProc':[
                    _(u'90 skip processing'),u'skipProc',-90,
                    {'val':u'false','type':'choice','it00':'true','it01':'false'},
                    {'val':u'false','type':'choice','it00':'true','it01':'false'}],
            u'orderProc':[
                    _(u'91 order processing'),u'orderProc',-91,
                    {'val':u'false','type':'choice','it00':'true','it01':'false'},
                    {'val':u'false','type':'choice','it00':'true','it01':'false'}],
            u'grpProc':[
                    _(u'95 group processing'),u'skipProc',-95,
                    {'val':u'false','type':'choice','it00':'true','it01':'false'},
                    {'val':u'false','type':'choice','it00':'true','it01':'false'}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xb0IDAT8\x8d\x8dS\xd1\x0e\xc4 \x08+\xe8o/Y\xb2\xef\x9e\xdc\x83\xa7\
\xab\r\xee\xce'\xd7\xd2\x02\x03\xcd\xbc`\x9chw\x8c\xbby1\xfcq*\x0bY\xa4\x18\
\x9b3^\x15\xd8\t\xb5\xa2hw\x98\x173\x00\xc1\xc1k\x15\xc7W|\x99r#\xde\xd5q\
\xd7\xeb\x8e[Z\xe0\x8cY\xf0\xe09\xc63\x92\xefo\x18\x00\xd4,s\xb4#v\x82'\xa6\
\xb7\xec\xbb m\x83\xcd\x19w\x9d\xef[\x05\x99\xc9\x1c\xa3\n\xfa\xe8\xf26\x1e\
\xbe'\x0f\xf3\x82\xbe\xd2g\x00\xe7\xfcf<\xe3\x01\xc4\xb2H\xbfN\xb6\xa5\xde\
\xcb\xcf\xfe\xc3\x8a\xcd\xd5\x95\xad\xad\xc3Q\xdd\xd5xW\xa5\xf1s\xd6\xcc\xd9\
\xebT\xee\x03\xd6\xaf\xa6\xe3z\x16A\x8a\x00\x00\x00\x00IEND\xaeB`\x82" 
    def ProcessOld(self,f,*args,**kwargs):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurCls(vtLog.DEBUG,'args:%s;kwargs:%s'%(vtLog.pformat(args),
                            vtLog.pformat(kwargs)),self)
            node=kwargs['node']
            if self.IsSkipProcessing(node):
                return
            if 'lang' not in kwargs:
                kwargs['lang']=self.lang
            else:
                self.__setLang__(kwargs['lang'])
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurCls(vtLog.DEBUG,'args:%s,kwargs:%s'%(vtLog.pformat(args),vtLog.pformat(kwargs)),self)
            lang=kwargs['lang']
            self.__setLang__(lang)
            self.__getTranslation__()
            iLevel=kwargs['level']
            strs=[]
            #s=':'.join([self.GetTrans('class'),self.GetTag(node)])
            #strs.append(self.__getHeadLine__(iLevel,self.__tex__(s)))
            strs.append(self.__tex__(self.GetDesc(node,lang=lang)))
            lTagFull=self.GetValidChild()
            lTagPrev=['method']
            #,lang=lang
            lNode=[]
            self.GetNodeGrpOrder(node,self.doc,lTagFull,lNode,sLang=lang)
            #self.doc.procChildsKeys(node,self.__procNode__,self.doc,
            #            lTagPrev,lNode)
            #lNode.sort()
            bMethod=False
            if len(lNode)>0:
                bMethod=True
                strs.append('\\begin{longtable}{p{0.2\linewidth}p{0.7\linewidth}}')
                strs.append('  \\multicolumn{2}{l}{\\textbf{%s}} \\\\'%(self.__tex__(self.GetTrans('method'))))
                for sTag,n in lNode:
                    oReg=self.doc.GetRegByNode(n)
                    if hasattr(oReg,'IsSkipProcessing'):
                        if oReg.IsSkipProcessing(n):
                            continue
                    elif self.IsSkipProcessing(n):
                        continue
                    sDesc=oReg.GetDesc(n,lang=lang)
                    if len(sDesc)==0:
                        continue
                    strs.append(u''.join([u'  ',
                            self.__tex__(oReg.GetTag(n)),u' & ',
                            sDesc,u' \\\\ & \\\\']))
                strs.append('\\end{longtable}')
            lTag=[k for k in lTagFull if k not in lTagPrev]
            lNode=[]
            bGrp=False
            if self.IsGrpProcessing(node):
                bGrp=True
                dGrp={}
                lNode=[]
                self.doc.procChildsKeys(node,self.__procNodeGrp__,self.doc,
                        lTag,lNode,dGrp,sLang=lang)
                lNode.sort()
                if len(lNode)>0:
                    sTagNameOld=None
                    strs.append('\\begin{longtable}{p{0.2\linewidth}p{0.7\linewidth}}')
                    for sTagName,sTag,n in lNode:
                        oReg=self.doc.GetRegByNode(n)
                        if hasattr(oReg,'IsSkipProcessing'):
                            if oReg.IsSkipProcessing(n):
                                continue
                        elif self.IsSkipProcessing(n):
                            continue
                        sDesc=oReg.GetDesc(n,lang=lang)
                        if len(sDesc)==0:
                            continue
                        if sTagNameOld!=sTagName:
                            strs.append('  \\textbf{%s} & \\textbf{%s} \\\\'%(self.__tex__(sTagName),
                                    self.__tex__(dGrp[sTagName][1])))
                            sTagNameOld=sTagName
                        strs.append(u''.join([u'  ',
                                self.__tex__(oReg.GetTag(n)),u' & ',
                                sDesc,u' \\\\ & \\\\']))
                    strs.append('\\end{longtable}')
            else:
                self.doc.procChildsKeys(node,self.__procNode__,self.doc,
                        lTag,lNode)
                lNode.sort()
                if len(lNode)>0:
                    strs.append('\\begin{longtable}{p{0.2\linewidth}p{0.7\linewidth}}')
                    for sTag,n in lNode:
                        oReg=self.doc.GetRegByNode(n)
                        if hasattr(oReg,'IsSkipProcessing'):
                            if oReg.IsSkipProcessing(n):
                                continue
                        elif self.IsSkipProcessing(n):
                            continue
                        sDesc=oReg.GetDesc(n,lang=lang)
                        if len(sDesc)==0:
                            continue
                        strs.append(u''.join([u'  ',
                                self.__tex__(oReg.GetTag(n)),u' & ',
                                sDesc,u' \\\\ & \\\\']))
                    strs.append('\\end{longtable}')
            strs.append(self.__tex__(self.GetDesc(node,lang=lang)))
            self.__writeLst__(f,strs)
            self.__includeDoc__(f,*args,**kwargs)
            kkwargs=kwargs.copy()
            kkwargs.update({'level':kwargs['level']+1})
            lTag=[k for k in lTagFull if k not in ['Documents','Doc']]
            if bGrp:
                self.__includeNodesGrp__(lTag,f,*args,**kkwargs)
            else:
                self.__includeNodes__(lTag,f,*args,**kkwargs)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
if 0:
    _(u'method(s)')