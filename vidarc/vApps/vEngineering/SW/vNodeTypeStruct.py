#----------------------------------------------------------------------------
# Name:         vNodeTypeStruct.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: vNodeTypeStruct.py,v 1.3 2006/09/27 04:20:51 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeTypeStruct(vXmlNodeEngineering):
    def __init__(self,tagName='t_struct'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'software structure type')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x97IDAT8\x8d\x95SA\x0e\x800\x08\xa3\xb0\xff\xffX\xf1"\x86@Y&\xc9\
\xa2\xe2\xbav]\x07\xa8I\x94\xdf\x97\xc7;\xd4 \x07\xb520\x83j//\x9e\xfb\x80\
\x9a\xf8}9c\xdc)\n\x0cD\xa4\x81w\xecU\xa52V\xa8\x81)\x82\x1a\xeaV\xe4U \xaf\
\x99\x1e\xcfi\xd4y\x9f\x82`f\x0c\xbbj&\x9e\x9cH&l\x1eL\xc76\x952\xe9\x7f\x17\
i\xc6\x081\xad~7\x13\x99\x81_\xda6\xb1^\x0c\xc0\x929\xfd\x1b\xa3\\{,\x9dP\
\xc3\xaa\xf2\xb3\xec\x93\xdb\x89|\x9d3S\x05M\xfe<\xb8/\x9b\x83C\x1c\xe8k\x00\
\x00\x00\x00IEND\xaeB`\x82' 
