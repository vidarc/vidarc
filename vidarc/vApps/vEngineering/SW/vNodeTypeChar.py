#----------------------------------------------------------------------------
# Name:         vNodeTypeChar.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060619
# CVS-ID:       $Id: vNodeTypeChar.py,v 1.3 2006/09/27 04:20:51 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeTypeChar(vXmlNodeEngineering):
    def __init__(self,tagName='t_char'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'software character type')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x98IDAT8\x8d\x8dSA\x0e\xc0 \x08\xa3\xb8\xff\xffX\xd9e,\x8d\x16\xa2'\
\x03\x96\x96\x82\x80\x0f\xcb\x13kF\xde\xe1\x03vq\x1e\x062h\x8fqq\x8e\x03>,\
\xd6\x0c\xc5\xd8)J\x0c\xcc\xec\x00\xdf(\xca\xd8\xa3X\x95\x1an\x85\xf3\xde\
\x81c\xcdP\xbds\xcc;\xb0\xba\x1f\xca\x94\x89\xd5\x04\xf67\xf0\x81\xc3\x03\
\xc9\xd2\xec\x84g\xa5N\xa6\xf2\xe2/\xd012P\xc9\x8f5\xc3\x19T\x01*s\xcd\xbeUV\
\xcc\xf0\x81\x9b\x05+WYM\xa6\x9c\x02\x9b\xf8\x7f\x92\xcd\xd8j\x12\xe0\xef\
\xbc\xf7\xd8\xf5\x9e\xb9\x17\xd5z\xb14X\xb0\xdf\xae\x00\x00\x00\x00IEND\xaeB\
`\x82" 
