#----------------------------------------------------------------------------
# Name:         vNodeStruct.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: vNodeStruct.py,v 1.3 2006/09/27 04:20:51 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeStruct(vXmlNodeEngineering):
    def __init__(self,tagName='struct'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'software structure')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xa8IDAT8\x8d\x9dS\xd1\x0e\xc4 \x08\xa3\xe0o\x9b,\xd9w+\xf7rd\x9d\
\xe0\xc5\\\x9f\x16\xb4\xb40\x0b\xa8I\xc0\xe7\xf0\xf8\x86\x1a\xe4\x00\x8d\x89\
LZk\xdc\x9c\xeb\x80\x9a\xf8\x1c^)\xfer\x14\x1c\x88H"g\xf5\xfemt%\x97Z\xa9B\r\
\x95#\xa8!\x8d\xc2\x0e\x82\xfc(f@o<\xf7\xc8\xc1\t\xb9B\x8bN\xabe\xe8M\xb3v\
\xe7\x1as\xd2\x0e\xe2\xd2\xa9\x13\xe5y\xfeiR\xfc\x85\xee;b\xd4\xcb%>.\xae4\
\x0e\xefc\xc5\xf6!\xed\xb2\xc0g>\x87o\x9f\xf2Z\xab\xb2\x015\xb4\xb7}\nIZl\
\xed\x08\x1cgVZI\xbb4~\x00\xde\x94\xabCf\xd3\xea\t\x00\x00\x00\x00IEND\xaeB`\
\x82' 
