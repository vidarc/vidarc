#----------------------------------------------------------------------------
# Name:         vNodeParam.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060924
# CVS-ID:       $Id: vNodeParam.py,v 1.3 2014/06/15 19:54:52 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeParam(vNodeEngBase):
    def __init__(self,tagName='Param'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'parameter')
    # ---------------------------------------------------------
    # specific
    def GetCfgDict(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'pType':[
                    _(u'21 type'),u'pType',-21,
                    {'val':u'','type':u'text'},{'val':u'','type':u'text'}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00nIDAT8\x8d\xed\x93Q\n\x800\x0cC_\xa7\xd7\x12\xbc\xd8`\xe0\xb9\x84\
\x81\xe7r\xf1c"\xa8\x1b(\x82\xf8a\xa04\xfdHZZj\xe6\x1a\x9e\xc0=R\x7f\xcf@i\
\x94R\'\xa5y\xcd~\xe3U\x07s\xcd. \x08\x10\x04\x95\xeac\xbc\xb1\x83\x08\xf4\
\x98\x1b\xec\xa6AD\xc9+\x8b\xa7\xa2\x18\xa0\xad\x1b\xd4\xbb^\x9c\xe0\x1aNg\
\xcc,\x1c\xea:\xec\xff\x05\x16O\xc70\xb0>\xac0\x8b\x00\x00\x00\x00IEND\xaeB`\
\x82' 
