#----------------------------------------------------------------------------
# Name:         vXmlEngineeringTree.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: vXmlEngineeringTree.py,v 1.2 2006/08/29 10:06:25 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlGrpTree import *
from vidarc.vApps.vEngineering.vNetEngineering import *

import vidarc.tool.lang.vtLgBase as vtLgBase
import images

class vXmlEngineeringTree(vtXmlGrpTree):
    def __init__(self, parent, id, pos, size, style, name,
                    master=False,controller=False,verbose=0):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vtXmlGrpTree.__init__(self,id=id, name=name,master=master,
              controller=controller,verbose=verbose,
              parent=parent, pos=pos, size=size,style=style)
        self.bLangMenu=True
        self.SetPossibleGrouping([
                ('normal'   , _(u'normal'), [] , 
                            {None:[('tag',''),('name','')],
                            #'user':[('name',''),('surname',''),('firstname','')],
                            #'group':[('name','')]
                            }),
                ])
    def SetDftNodeInfos(self):
        self.SetNodeInfos(['tag','name','|id'])
    def SetupImageList(self):
        if vtXmlGrpTree.SetupImageList(self):
            self.__addElemImage2ImageList__('root',
                            images.getPluginImage(),
                            images.getPluginImage(),True)
            self.__addElemImage2ImageList__('prjengs',
                            images.getPluginImage(),
                            images.getPluginImage(),True)
            self.SetImageList(self.imgLstTyp)

