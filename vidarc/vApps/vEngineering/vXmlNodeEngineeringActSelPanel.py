#----------------------------------------------------------------------------
# Name:         vXmlNodeEngineeringActSelPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20130616
# CVS-ID:       $Id: vXmlNodeEngineeringActSelPanel.py,v 1.1 2013/06/17 12:58:26 wal Exp $
# Copyright:    (c) 2013 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import vLang
    from vGuiFrmWX import vGuiFrmWX
    
    import vidarc.tool.art.vtArt as vtArt
    
    from vidarc.tool.xml.vtXmlNodeGuiPanel import vtXmlNodeGuiPanel
    from vidarc.tool.xml.vtXmlDomConsumerLang import vtXmlDomConsumerLang
    from vidarc.vApps.vEngineering.vXmlNodeEngineeringActivityPanel import vXmlNodeEngineeringActivityPanel
    
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vXmlNodeEngineeringActSelPanel(vtXmlNodeGuiPanel):
    def __initCtrl__(self,*args,**kwargs):
        global _
        _=vLang.assignPluginLang('vEngineering')
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        self.SetVerbose(__name__,iVerbose=-1,kwargs=kwargs)
        self.__clr__()
        self.selIdx=-1
        self.selId=-1
        self.cutId=-1
        self.copyId=-1
        self.iSortCol=-1
        lWid=[]
        iRow=0
        lWid.append(
            ('szBoxHor',(iRow,0),(1,1), {'name':'bxsPopup'},[
                ('cbBmp',0,4,{'name':'cbActCut',
                        'tip':_(u'cut activity'),
                        'bitmap':vtArt.getBitmap(vtArt.Cut)},
                        {   'btn':self.OnActCutButton
                        }),
                ('cbBmp',0,4,{'name':'cbActCopy',
                        'tip':_(u'copy activity'),
                        'bitmap':vtArt.getBitmap(vtArt.Copy)},
                        {   'btn':self.OnActCopyButton
                        }),
                ('cbBmp',0,4,{'name':'cbActPaste',
                        'tip':_(u'paste activity'),
                        'bitmap':vtArt.getBitmap(vtArt.Paste)},
                        {   'btn':self.OnActPasteButton
                        }),
                ('cbBmp',0,4,{'name':'cbActMove',
                        'tip':_(u'move activity'),
                        'bitmap':vtArt.getBitmap(vtArt.Move)},
                        {   'btn':self.OnActMoveButton
                        }),
                ]))
        iRow+=1
        lWid.append(
            ('lstCtrl',(iRow,0),(1,1),{'name':'lstDat','size':(100,80),
                        'multiple_sel':False,'mark':False,
                        'cols':[
                            (_(u'img'),-3,20,0),
                            (_(u'State'),-1,80,0),
                            (_(u'Nr'),-2,60,0),
                            (_(u'Name'),-1,200,1),
                            (_(u'Prior'),-1,80,0),
                            (_(u'Severity'),-1,60,0),
                            (_(u'Start'),-1,80,0),
                            (_(u'End'),-1,80,0),
                            (_(u'Date'),-1,80,0),
                            (_(u'Author'),-1,80,0),
                            (_(u'Prev'),-2,60,0),
                            ]
                        },
                        {   'lst_item_sel':    self.OnLstDatItemSelected,
                            'lst_item_desel':  self.OnLstDatItemDeselected,
                        }))
        lWid.append(
            ('szBoxVert',(iRow,1),(1,1), {'name':'bxsPopup'},[
                ('tgPopup',     0,16,{'name':'tgFilter','size':(32,32),
                        'tip':_(u'filter'),
                        'bmp':vtArt.getBitmap(vtArt.Filter)},None),
                ]))
        iRow+=1
        lWid.append(
            (vXmlNodeEngineeringActivityPanel,(iRow,0),(1,1),{'name':'pnAct'
                },{
                }))
        lWid.append(
            ('szBoxVert',(iRow,1),(1,1),{'name':'bxsActBt',},[
                ('cbBmp',0,20,{'name':'cbActApply',
                        'tip':_(u'apply activity'),
                        'bitmap':vtArt.getBitmap(vtArt.Apply)},
                        {   'btn':self.OnActApplyButton
                        }),
                ('cbBmp',0,4,{'name':'cbActNew',
                        'tip':_(u'new activity'),
                        'bitmap':vtArt.getBitmap(vtArt.New)},
                        {   'btn':self.OnActNewButton
                        }),
                ('cbBmp',0,16,{'name':'cbActAdd',
                        'tip':_(u'add activity'),
                        'bitmap':vtArt.getBitmap(vtArt.Add)},
                        {   'btn':self.OnActAddButton
                        }),
                ]))
        vtXmlNodeGuiPanel.__initCtrl__(self,lWid=lWid)
        imgLst,dImg=self.lstDat.CreateImageList([
            ['empty',   vtArt.getBitmap(vtArt.Invisible)],
            ['busy',    vtArt.getBitmap(vtArt.Build)],
            ['ok',      vtArt.getBitmap(vtArt.Okay)],
            ['no',      vtArt.getBitmap(vtArt.No)],
            ])
        #self.PrintMsg(u'EngineeringActSel init')
        self.__setUpPopUp__()
        return [(1,1),],[(0,1),]
    def OnActCutButton(self,evt):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if self.selId>=0:
                self.cutId=self.selId
                self.copyId=-1
                #self.Post('cut',{'iId':self.selId})
        except:
            self.__logTB__()
    def OnActCopyButton(self,evt):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if self.selId>=0:
                self.cutId=-1
                self.copyId=self.selId
                #self.Post('copy',{'iId':self.selId})
        except:
            self.__logTB__()
    def OnActPasteButton(self,evt):
        try:
            if evt is not None:
                evt.Skip()
            bDbg=self.GetVerbose(0)
            if self.objRegNode.IsAclOk(self.doc.ACL_MSK_ADD)==False:
                self.__logError__({'missing permissions on nodeId':self.nodeId})
                return
            if self.nodeId>=0:
                if self.cutId>=0:
                    node=self.doc.getNodeByIdNum(self.cutId)
                    self.cutId=-1
                    self.pnAct.Clear()
                    self.__clr__()
                    #self.doc.moveNode(self.node,node)
                    self.doc.DoSafe(self.doc.moveNode,self.node,node)
                    self.doc.DoCallBackWX(self.__updateInfo__,self.node)
                if self.copyId>=0:
                    nodeSrc=self.doc.getNodeByIdNum(self.copyId)
                    #tmp=self.doc.cloneNode(nodeSrc,True)
                    #self.doc.appendChild(self.node,tmp)
                    #self.doc.addNode(self.node,tmp)
                    
                    n=self.objRegNode.CloneSafe(nodeSrc,self.node,
                                self.pnAct.__copyActivityVal__,self.copyId)
                    #iId=self.objRegNode.GetKey()
                    self.__addInfo__(n)
                    #self.doc.DoCallBackWX(self.__showInfo__,-1)
                    self.__showInfo__(-1)
                    #self.doc.DoCallBackWX(self.__updateInfo__)
                #self.Post('paste',{'iId':self.nodeId})
        except:
            self.__logTB__()
    def DoMove(self,moveId,selId):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if moveId>=0:
                if selId>=0:
                    node=self.doc.getNodeByIdNum(selId)
                    nodeMove=self.doc.getNodeByIdNum(moveId)
                    self.doc.moveNode(nodeMove,node)
                    #self.doc.DoSafe(self.doc.moveNode,nodeMove,node)
                    self.doc.DoCallBackWX(self.__updateInfo__,self.node)
        except:
            self.__logTB__()
    def OnActMoveButton(self,evt):
        try:
            if evt is not None:
                evt.Skip()
            bDbg=self.GetVerbose(0)
            self.Post('move',{'iId':self.selId})
            
        except:
            self.__logTB__()
    def OnActApplyButton(self,evt):
        try:
            if evt is not None:
                evt.Skip()
            bDbg=self.GetVerbose(0)
            if self.selId>=0:
                self.pnAct.GetNode(None)
                n=self.doc.getNodeByIdNum(self.selId)
                if n is not None:
                    self.__addInfo__(n)
                    self.CB(self.__showInfo__,self.selId)
        except:
            self.__logTB__()
    def CreateNew(self,prevId):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if self.doc is None:
                return
            if self.objRegNode.IsAclOk(self.doc.ACL_MSK_ADD)==False:
                return
            if self.node is None:
                return
            n=self.objRegNode.Create(self.node,
                        self.pnAct.__getActivityVal__,prevId)
            if n is not None:
                self.__addInfo__(n)
                self.selId=self.objRegNode.GetKey(n)
                self.CB(self.__showInfo__,-1)
                #wx.CallAfter(self.__showInfo__,sNr)
        except:
            self.__logTB__()
    def OnActNewButton(self,evt):
        try:
            if evt is not None:
                evt.Skip()
            bDbg=self.GetVerbose(0)
            self.CreateNew(-1)
        except:
            self.__logTB__()
    def OnActAddButton(self,evt):
        try:
            if evt is not None:
                evt.Skip()
            bDbg=self.GetVerbose(0)
            self.CreateNew(self.selId)
            #if self.selId>=0:
            #    self.pnAct.SetPrev(self.selId)
        except:
            self.__logTB__()
    def __setUpPopUp__(self):
        if self.IsMainThread()==False:
            return
        try:
            self.__setUpPopUpFilter__()
        except:
            self.__logTB__()
    def __setUpPopUpFilter__(self):
        if self.IsMainThread()==False:
            return
        try:
            p=vGuiFrmWX(name='popFilter',parent=self.tgFilter.GetWid(),
                    kind='popupCls',bAnchor=True,
                    iArrange=0,widArrange=self.tgFilter.GetWid(),
                    lGrowRow=[(1,1),],
                    lGrowCol=[(0,1),(1,1)],
                    #kwTopCmd={'name':'pnTopCmd','bFlexGridSizer':-1,'lWid':lWidTop},
                    lWid=[
                        ])
            #p.BindEvent('ok',self.OnSrcPopupOk)
            p.GetName()
            self.popFilter=p
            self.tgFilter.SetWidPopup(p)
            
        except:
            self.__logTB__()
    def __Clear__(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            self.lstDat.DeleteAllItems()
            #self.popSrc.lstInp.DeleteAllItems()
            #self.SetModified(False,self.popSrc.lstInp)
            self.__clr__()
        except:
            self.__logTB__()
    def __Close__(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            self.CallWidChild(-1,'__Close__')
            
        except:
            self.__logTB__()
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            oAct=doc.GetReg('Activity')
            self.SetRegNode(oAct)
            #self.pnAct.__SetDoc__(doc,bNet=bNet,dDocs=dDocs)
            self.pnAct.SetDoc(doc,bNet=bNet)
        except:
            self.__logTB__()
    def __addInfo__(self,node):
        try:
            l=self.objRegNode.AddValuesToLst(node)
            id=self.doc.getKeyNum(node)
            self.logs[id]=l
        except:
            self.__logTB__()
    def __showInfo__(self,iNr=-1):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            try:
                iNr=long(sNr)
            except:
                iNr=-1
            id=-1
            self.objRegNode.SetLang(self.doc.GetLang())
            if self.selId>=0:
                idOld=self.selId
            else:
                idOld=-1
            #self.__unlockSel__()
            self.selIdx=-1
            self.selId=-1
            self.lstDat.DeleteAllItems()
            if self.iSortCol==-1:
                sortKeys=None
            else:
                sortKeys=[self.iSortCol]
            ll=self.objRegNode.SortAndTranslateActivities(zip(self.logs.values(),self.logs.keys()),sortKeys,iMode=1)
            if bDbg:
                self.__logDebug__(ll)
            self.lstDat.SetValue(ll)
            self.OnLstDatItemDeselected(None)
            if idOld>=0 and 0:
                lFind=self.lstDat.FindValue([idOld],[-1],[-2,-1])
                self.__logDebug__({'selId':self.selId,'idOld':idOld,'lFind':lFind})
                self.doc.DoCallBackWX(self.lstDat.SetSelected,
                                [l[0] for l in lFind])
                #if self.objRegNode.IsAclOk(self.doc.ACL_MSK_WRITE):
                #    node=self.doc.getNodeByIdNum(idOld)
                #    if node is not None:
                #        self.doc.startEdit(node)
            return
            for l in ll:
                index = self.lstDat.InsertImageStringItem(sys.maxint, l[0], -1)
                for i in xrange(1,10):
                    self.lstDat.SetStringItem(index,i, l[i], -1)
                self.lstDat.SetItemData(index,l[-1])
                try:
                    if long(l[1])==iNr:
                        id=long(l[-1])
                        self.lstDat.SetItemState(index,wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
                        self.selIdx=index
                        #node=self.doc.getNodeByIdNum(id)
                        #self.pnLog.SetNode(node)
                except:
                    pass
            if id!=idOld:
                self.__unlockSel__()
                if id>=0:
                    if self.objRegNode.IsAclOk(self.doc.ACL_MSK_WRITE):
                        node=self.doc.getNodeByIdNum(id)
                        if node is not None:
                            self.doc.startEdit(node)
                        node=self.doc.getNodeByIdNum(id)
                        #self.pnLog.SetNode(node)
        except:
            self.__logTB__()
    def __updateInfo__(self,node):
        try:
            bDbg=self.GetVerboseDbg(0)
            #self.lstDat.DeleteAllItems()
            self.__clr__()
            iId=self.doc.getKeyNum(node)
            if bDbg:
                self.__logDebug__('iId:%d',iId)
            self.doc.procChildsKeys(node,self.__procInfo__,self.doc,self.name)
            self.__showInfo__()
            return
            #self.lstDat.DeleteAllItems()
            #self.__clr__()
            childs=self.doc.getChilds(node,self.name)
            if self.objRegNode.IsAclOk(self.doc.ACL_MSK_READ):
                for c in childs:
                    self.__addInfo__(c)
                self.__showInfo__()
        except:
            self.__logTB__()
    def __procInfo__(self,c,doc,sName):
        try:
            self.__logDebug__({'sName':sName})
            sTag=doc.getTagName(c)
            if sTag==sName:
                self.__addInfo__(c)
        except:
            self.__logTB__()
        return 1
    def __SetNode__(self,node):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            self.name=self.objRegNode.GetTagName()
            #self.doc.procChildsKeys(node,self.doc,self.name,self.__procInfo__)
            #self.__showInfo__()
            self.__updateInfo__(node)
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__({'name':self.name})
            l=self.popSrc.lstInp.GetValue()
            if bDbg:
                self.__logDebug__(l)
            self.objRegNode.SetInputFN(node,self.sInpFN)
            self.SetModified(False,self.popSrc.lstInp)
        except:
            self.__logTB__()
    def __Lock__(self,bFlag):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if bFlag:
                #self.lstDat.Enable(False)
                #self.popSrc.lstInp.Enable(False)
                #self.popSrc.cbInpAdd.Enable(False)
                #self.popSrc.cbInpDel.Enable(False)
                pass
            else:
                #self.lstDat.Enable(True)
                #self.popSrc.lstInp.Enable(True)
                #self.popSrc.cbInpAdd.Enable(True)
                #self.popSrc.cbInpDel.Enable(True)
                pass
        except:
            self.__logTB__()
            
    def __clr__(self):
        self.logs={}
        self.selIdx=-1
        self.selId=-1
    def OnLstDatItemSelected(self,evt):
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__('')
            lSel=self.lstDat.GetSelected([-1,-2])
            if len(lSel)>0:
                self.selId=lSel[0][0]
                self.selIdx=lSel[0][1]
                self.__logDebug__('selId:%d,selIdx:%d'%(self.selId,self.selIdx))
                n=self.doc.getNodeByIdNum(self.selId)
                self.pnAct.SetNode(n)
        except:
            self.__logTB__()
    def OnLstDatItemDeselected(self,evt):
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__('')
            self.selIdx=-1
            self.pnAct.Clear()
            self.pnAct.__Lock__(False)
        except:
            self.__logTB__()
