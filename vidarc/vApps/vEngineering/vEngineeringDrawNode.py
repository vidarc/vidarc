#----------------------------------------------------------------------------
# Name:         vEngineeringDrawNode.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060924
# CVS-ID:       $Id: vEngineeringDrawNode.py,v 1.2 2009/10/18 09:04:12 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeTag import vtXmlNodeTag
try:
    if vcCust.is2Import(__name__):
        from vEngineeringDrawPanel import *
        #from vtXmlNodeTagEditDialog import *
        #from vtXmlNodeTagAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vEngineeringDrawNode(vtXmlNodeTag):
    #NODE_ATTRS=[
    #        ('Tag',None,'tag',None),
    #        ('Name',None,'name',None),
    #        ('Desc',None,'description',None),
    #    ]
    #FUNCS_GET_SET_4_LST=['Tag','Name']
    #FUNCS_GET_4_TREE=['Tag','Name']
    #FMT_GET_4_TREE=[('Tag',''),('Name','')]
    def __init__(self,tagName='drawNode'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'node diagram')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def Is2Create(self):
        return False
    def Is2Add(self):
        return True
    def IsMultiple(self):
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return False
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x80IDAT8\x8d\xb5S\xd1\x0e\x80 \x08<\xd0\xcf6\xa3\xf5\xdb*=\xe9\\\
\xa9e-6\x87\xdevpr\x83\x88\r\xbe\x84\x05\x00MQ\xdf\x90\x89\rq\xfd 6$"\xe5>\
\xc22\x8f\xdb\xb5\xe7d\x00\x80\xe6\x9c\x0f\xb1A\x0f\xab9\xb6\xf5/MQw\xd9\x14\
\x00\x9cw\x17,\xa4\x00\x11y\xae\xa0\xceC\x05\xf5pZ\xb1\xfa\xa5\xb8\xf5\x8f\
\x82\xde\x0c\xa6\x14\x9c\xbbM+\xf8e\x06gl\xca\x85;gJ\x81\xd7\x0b\xf5u\x9d\
\x0f\xfa\xafW=\xdc\x1d\xd9`\x00\x00\x00\x00IEND\xaeB`\x82' 
  
    def getSelImageData(self):
        return self.getImageData()

    def GetEditDialogClass(self):
        if GUI:
            return vtXmlNodeTag.GetEditDialogClass(self)
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return {'name':self.__class__.__name__,
                    'pnCls':vtXmlNodeTag.GetPanelClass(self),
                    'pnName':'pnNodeTag',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vEngineeringDrawPanel
        else:
            return None

