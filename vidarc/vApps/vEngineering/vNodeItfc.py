#----------------------------------------------------------------------------
# Name:         vNodeItfc.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120407
# CVS-ID:       $Id: vNodeItfc.py,v 1.2 2013/05/20 12:08:36 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeItfc(vNodeEngBase):
    def __init__(self,tagName='itfc'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'interface')
    def GetTransDescription(self):
        return u'interface'
    # ---------------------------------------------------------
    # specific
    def GetGrpDict(self):
        return {
            None:0,
            'method':[-1,None,]
            }
    def GetProcessNodeInc(self):
        return [
            {'lTag':['method','ToDo','Tool',],'level':1,},
            #{'lTag':['method'],'level':10,'headline':u'method(s)'},
            ]
    def GetCfgDictOld(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'skipProc':[
                    _(u'90 skip processing'),u'skipProc',-90,
                    {'val':u'false','type':'choice','it00':'true','it01':'false'},
                    {'val':u'false','type':'choice','it00':'true','it01':'false'}],
            u'orderProc':[
                    _(u'91 order processing'),u'orderProc',-91,
                    {'val':u'false','type':'choice','it00':'true','it01':'false'},
                    {'val':u'false','type':'choice','it00':'true','it01':'false'}],
            u'grpProc':[
                    _(u'95 group processing'),u'skipProc',-95,
                    {'val':u'false','type':'choice','it00':'true','it01':'false'},
                    {'val':u'false','type':'choice','it00':'true','it01':'false'}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\x8dIDAT8\x8d\x95\x92AKTa\x14\x86\x9fo\x129w6\r1^F\x18\xe3&\xa6\x84i\
\x9b\xb8\x8e\xfb\xf0o\x0c\xee\xc2]\x85d\xb5\xca\xa8\x88ZK\x7f\xa1\x9a_0\xb8\
\x10w\xb9\x11\x82\xc1\x9b\xf7".\x06\x94qp\xbe\x81\x11\x0f!|-\xa4\x8b\x13t\
\xbd\xbd\xcbs\xf8\x9e\xf3\xbe\xe7;\xc6\x14n\xf0?\xfa\xf6\xf5\x8b\x93Q\xa1\
\xec\x979\x1b\x9c1\x92\xe7\xd1^\xab\xe5l\xdf\xd2\xedt\xd9\xda\xdeb\xfe\xc1<^\
\xd1\xa3g{\x98,\x07\x9b\xcd\xa6K\x92\x84\x93\xee\t\xaa\x8a\xaa\xa6\xbd\x8a_A\
\x7f)\x85\xac\xc9\x8f\x96\x96L\x9c\xc4\x00LT\'\xa8\xd5ji/\xfa\x191V\x1e\xcbv\
\xf0\xb7>~\xfa\xe0\x0e\x0f\x0e\x01\x08\x17B\xea\xf5e\x93\xe9\xe0\xaa>ol8{j\
\xd1\xf3\xcb\x18\xe1\xc3\x10 ;\xc2U=^Y1\x00\xc1\x9d\x00\x11I\xeb)`\xb3\xd9t\
\xd7A\xde\xbe{oT\x15\x11\xe1\xde\xec\xac\x19\x02\xec\xfe\xd8\xe5\xd9\xd3\'\
\xb9 \xd33\xd3\xc3\x0e\xf6Z-gO-\x15\xbf\x92+N\xbd\xbel\x86\x00\xb6oQU\xfcq\
\x1f\xb8\\X.\xd2\x1f@\xbb\xdd\x06\xa0\xe8\x15y\xb3\xfe\xda\xc5I\xcc\xab\x97/\
rAL\xa3\xd1pQ\x14\xa5\x97f{\x16\xf1\x84p!\xa4s\xd4au\xed\xb9\xc9\x02\x14dTX\
\xac-\x02\xa0\xaa\x88\'\x88\x08;\xdfw8\xee\x1c_\x1bgd\xea\xee\x14\x83\xf3\
\x01\xa5[%\xac\xb5\x04\x93\x01\xc1\xed\x80j\xb5J\xe9f)\xfd\xae\x7f\x02l\xdf\
\xb2\x1f\xef\xc3\x05\xcc\xdd\x9fK\x0f&\xaf~\x03E\x8d\x98a\xb0\x08\xfb\x07\
\x00\x00\x00\x00IEND\xaeB`\x82' 
#if 0:
#    _(u'method(s)')