#----------------------------------------------------------------------------
# Name:         __register__.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: __register__.py,v 1.8 2015/03/01 08:51:14 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust

if vcCust.is2Import(__name__,'__REG_NODE__',True):
    from vidarc.vApps.vEngineering.research.vNodeResearch import vNodeResearch
    import vidarc.vApps.vEngineering.__link__

def RegisterNodes(doc,oRoot=None):
    try:
        if vcCust.is2Import(__name__,'__REG_NODE__',True):
            oBase=vNodeResearch()
            doc.RegisterNode(oBase,True)
            for o in []:
                doc.RegisterNode(o,False)
            vidarc.vApps.vEngineering.__link__.LinkNodes(doc,[
                    ('research',['hw','sw','Mod',
                            'Base','Cps','Obj','Dsg','Prd','Res','Struct',
                            'Prc','Fct',
                            'DocRef','Prop','parameters',
                            'Exp','Bug','Test','Tool','ToDo',
                            'S88_phy','S88_Prc','S88_procModel',
                            'Documents','dataCollector',
                            'Fct','Prc','Bug','Test','Tool','ToDo',]),
                ])
            if oRoot is not None:
                doc.LinkRegisteredNode(oRoot,oBase)
            if vcCust.is2Import(__name__,'__REG_ACCOUNT__',True):
                for sTag in ['instance','Base','Prop','hw','sw','Mod','S88_phy','S88_Prc','S88_procModel']:
                    vidarc.vApps.vEngineering.__link__.LinkNodes(doc,
                                        [(sTag,['account']),])
            lResearch=['research']
            for sTag in ['instance','Base','Cps','Obj','Dsg','Prd','Res',
                            'Struct','Prc','Fct',
                            'DocRef','Prop','hw','sw','Mod',
                    'Fct','Prc','Bug','Tool','Test',
                    'S88_phy','S88_Prc','S88_procModel']:
                vidarc.vApps.vEngineering.__link__.LinkNodes(doc,[
                        (sTag,lResearch),
                    ])
    except:
        vtLog.vtLngTB(__name__)
