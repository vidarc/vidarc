#----------------------------------------------------------------------------
# Name:         vNodeOutBool.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060924
# CVS-ID:       $Id: vNodeOutBool.py,v 1.1 2006/10/09 11:25:20 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeOutBool(vXmlNodeEngineering):
    def __init__(self,tagName='OutBool'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'output boolean')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00jIDAT8\x8d\xa5\x92Q\x0e\xc0 \x08C)x\xff\x1bo\xeecs1\x88[\t|\x99F\x9a\
\x97R@M*\xd3\xbc\xd0\xcf\xa3\x8f7\xd4\xf0g\xa0^`\x96>\t\xb24\x0b\x81\xa7\x19\
\x8b\xb3\x19m\xc0L\xd9\x80\xce\xa0L\xb0\x0b\x11\xd5"\xa9\xc8\x8d\x1a\xe1\xee\
\xf4y\xc2\x0c2m\x04\xd4\xa8\xb0\x96\xc5\xc7\xb8Eb\x86 \xbc\x02\xd3\xc0\xf7o\
\xf5\n\x17d(0?K\x15T\x80\x00\x00\x00\x00IEND\xaeB`\x82' 
