#----------------------------------------------------------------------------
# Name:         vNodeOutput.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060924
# CVS-ID:       $Id: vNodeOutput.py,v 1.1 2006/10/09 11:25:20 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeOutput(vXmlNodeEngineering):
    def __init__(self,tagName='Output'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'output')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x9dIDAT8\x8d\xedSA\n\xc40\x08\x1c\xcd\xbb\n\xc2\xbe\xab\x10\xd8w-\
\xe4c\xcd\xf4`\xb25i{\xeae\x0f+\x04\xd1\x19u\x14"\xa2\tOL\x1fU\xffN\x03\xd6\
\x95\xac\x0b\xddod\xdd\x18I#\xbe\x90\xf5\x13\xf1B\x00\x14M\xe8/\xc6\x80q\x8c\
3\x01\x10(l\xdc\x9e\xb0\x90<\xbcc\xf94\x000\x8a&(`]\t\x00\x83\xcb/!wg\x8e\
\xa9\xe8K<\xc8\x01\xcc`]y.\xba\xb4\xfc\x95~%;\xca\xbd\xc1\xe7\xa3\x94\xe9he\
\xc2mh(\x8d0\xec+\xfa\x96Y\xe7\xb1\x92\xc1\xd7n\xdc\xff_\xc0\x0eRuk\xd1\xc8\
\xc7c\xaf\x00\x00\x00\x00IEND\xaeB`\x82' 
