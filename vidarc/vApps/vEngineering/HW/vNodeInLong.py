#----------------------------------------------------------------------------
# Name:         vNodeInLong.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060924
# CVS-ID:       $Id: vNodeInLong.py,v 1.1 2006/10/09 11:25:20 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeInLong(vXmlNodeEngineering):
    def __init__(self,tagName='InLong'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'input long')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00aIDAT8\x8d\xc5\x92\xc1\n\xc00\x08C\x17\xeb\xff\xffqkO\x82l\x8d\x1e\
\xa4\xcc\x93\x88&\x0f\x0c \xe3\xe9\x94\x9e\x86\xb6\xa6y\x0f\x19\xc8\x04\xa4e\
\xcf\x04*\xd7\xfb\x04\xff\x0b\xc4/\xc4\xfe\n\x01\xbaA\xa2\x04\xb6\xa6\x9d\
\xf0\xdfs*\xe0Y`B^\xeaK\x15*\xdb\xd1\xe8\x96\x1dE\xa2\x8f@v\\\xc5\xba\xfd\
\x85\r%\xcf8\x00\xd7\xc0\xa5\t\x00\x00\x00\x00IEND\xaeB`\x82' 
