#----------------------------------------------------------------------------
# Name:         vNodeInput.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060924
# CVS-ID:       $Id: vNodeInput.py,v 1.1 2006/10/09 11:25:20 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeInput(vXmlNodeEngineering):
    def __init__(self,tagName='Input'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'input')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x9eIDAT8\x8d\xcdR[\n\xc30\x0c\x93\x9d]k\x10\xd8\xb9\n\x81\x9d\xab\
\x90\x8b5\xda\x87\x9d\xd6\x19e+\xedO\xf5c\xc7\xb6\xe4\x07\x11\xd1\x84+\xd0K\
\xec\xfb\t\xb0Md{\xd2\xecB\xb6\x85\x16\x9f=\xbe\xb8\x9d\xb9\x92D\x13\xec\x90\
\x95\x00\xd8\xdf~\\n~!\x00\x02e\xa8\t\x13T\x9f"v\xa8\x7fWxln\x0eB\x19l\xe0\
\x11\x81u\x02\xd1\x97\x18\xa1\x84t\x01\xdb\xc4o\xd2\xae@/\x14}\x8bh\x92#\xdd\
\x07\x01C\xc6pa\x17\xfc% \xfd+\xef\x1d\xae\x93-\x17\'\xca\xber\x108\x8b\x9b}\
\xe53\xf8\x00\xf8\x97RU\xac\xd5\x0c@\x00\x00\x00\x00IEND\xaeB`\x82' 
