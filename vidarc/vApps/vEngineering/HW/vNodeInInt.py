#----------------------------------------------------------------------------
# Name:         vNodeInInt.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060924
# CVS-ID:       $Id: vNodeInInt.py,v 1.1 2006/10/09 11:25:20 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeInInt(vXmlNodeEngineering):
    def __init__(self,tagName='InInt'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'input integer')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00cIDAT8\x8d\xc5RA\x0e\xc0 \x08\x1b\xc8\xff\x7f\xacx2!\x1bP\x95\xc3zDZ\
\xdb\x14"nO\x05\x1c=\xe8\xe8\xaa\xa3\xeb\xb5\xc0.\xa8\x1aA\xbc\xa1\xb5N\xdc(\
\x13p# \x12\x148\x81+`#\xa0&\xfeo\xe1\xf8\x90\xde\xf3P`5\x81.R\xd6\x12\xb2\
\x1a\xed\x88\xfd-#YG\x1f\x81\x8c\x8c\x8e\xaa\xdc\xc2\x04\x86\xf2?\xf7\x18\
\xfa\x804\x00\x00\x00\x00IEND\xaeB`\x82' 
