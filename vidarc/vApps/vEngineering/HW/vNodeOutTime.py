#----------------------------------------------------------------------------
# Name:         vNodeOutTime.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060924
# CVS-ID:       $Id: vNodeOutTime.py,v 1.1 2006/10/09 11:25:20 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeOutTime(vXmlNodeEngineering):
    def __init__(self,tagName='OutTime'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'output time')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00lIDAT8\x8d\xcd\x92K\n\xc00\x08D\x1d\xed\xfdo\x9c\xd8\x95\xa1\xd4\x0f\
\x82\x14\xea*\x88\xa3\xcf1\x00\x0bM\x82Gj"\xba\xde\t\xddK\xa3B\xb0\xe0\x13\
\x02T\x1e\x18M6\xbd$\xe8\x88\xd3\x06]1Q\xb0\xc2\xbfL\xec\xc4!\xd0\xbd4\xc2\
\xcf\xf2\x16\xee#=\x85\xf6\xae\xcc<+TSR1\x0b\x1c\x81M\xeb\x12\xa4W\x00\x0b\
\xa2f\xaenz\x85\x1b\xf9N47\x83;\xfd\x7f\x00\x00\x00\x00IEND\xaeB`\x82' 
