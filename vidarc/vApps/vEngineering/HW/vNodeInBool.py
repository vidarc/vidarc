#----------------------------------------------------------------------------
# Name:         vNodeInBool.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: vNodeInBool.py,v 1.2 2006/08/29 10:06:25 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeInBool(vXmlNodeEngineering):
    def __init__(self,tagName='InBool'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'input boolean')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00kIDAT8\x8d\xa5\x93Q\x0e\x001\x04D\x97\xba\xff\x8d[\xfb%\x11\xbb\x83\
\x86\xafF\xc6\xf4\x15%\xe2\xf5LBbB\xcfV;\x13/\xaa\x0c8&:E)\xc1-\xcd\x87 \xd2\
X\xa17k\x1btbl\xd0\xee\xc1\x98\x005\x91\xa6\x8b\x04\t\xf4l\xfd{B\xccC\x03?\
\xbe\xac\x17b\xa2\n\x15i\xc4\xdf\x96\x15\xa1\x85\x82c4a\xf57\xc6Sx\x01\xfat<\
\x19\x97)\xb3\xf9\x00\x00\x00\x00IEND\xaeB`\x82' 
