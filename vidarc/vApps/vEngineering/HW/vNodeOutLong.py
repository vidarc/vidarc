#----------------------------------------------------------------------------
# Name:         vNodeOutLong.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060924
# CVS-ID:       $Id: vNodeOutLong.py,v 1.1 2006/10/09 11:25:20 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeOutLong(vXmlNodeEngineering):
    def __init__(self,tagName='OutLong'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'output long')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00^IDAT8\x8d\xc5\x93\xc1\x0e\xc0 \x08Ci\xf1\xff\xffxs\x07\xb7\x84C\x9d\
\x87\xc6\xc8\x894\xf2h\x04\x00f8\xd1\x94\xd8\xef\xab\x7f9\x98\xf8\x03\xd0j?\
\x03\xac\xba\xeewp\x1eP\xa7P\xf3-\x0e\xe0.\x12#\x86Meu\xa6\xd7\xb07\x11`.?J\
\x16\xbe\xe0\xa6D\xfb\x16\xc0\x84\x82\xc9\xb7\xee\x14\x1e\x8ft,&r\xdch\xfb\
\x00\x00\x00\x00IEND\xaeB`\x82' 
