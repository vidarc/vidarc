#----------------------------------------------------------------------------
# Name:         vNodeInTime.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060924
# CVS-ID:       $Id: vNodeInTime.py,v 1.1 2006/10/09 11:25:20 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeInTime(vXmlNodeEngineering):
    def __init__(self,tagName='InTime'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'input time')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00eIDAT8\x8d\xcdRA\n\xc00\x08\x8b\xd6\xff\xffx\xebNB\xe9\x8c\x15d\xb0\
\x1c\xc5\xc4$\xad\x88\x0et\xa0-6\x00\xdb\x07\xf3\xbef\xb4(:\xe4\x13\x07\x92u\
\xe0n\xd8\xf5\xd4A\x85L\x05\xaad \x88\xf0\xaf\x12+HK\x8c\xe2\xecs*\xe0\x99\
\x99\x90\xc3|\xe9d\x95\xed\xd8z-#\xad\x8e^\x02\x19\xf9\xf4\x17\xda\xaf\xf0\
\x00\x0f\x80@\x11\xd1\xc5[\xb5\x00\x00\x00\x00IEND\xaeB`\x82' 
