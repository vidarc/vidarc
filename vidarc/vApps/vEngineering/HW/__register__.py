#----------------------------------------------------------------------------
# Name:         __register__.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: __register__.py,v 1.5 2013/05/20 12:08:36 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust

try:
    if vcCust.is2Import(__name__,'__REG_NODE__',True):
        from vidarc.vApps.vEngineering.HW.vNodeHW import vNodeHW
        from vidarc.vApps.vEngineering.HW.vNodeInput import vNodeInput
        from vidarc.vApps.vEngineering.HW.vNodeInBool import vNodeInBool
        from vidarc.vApps.vEngineering.HW.vNodeInInt import vNodeInInt
        from vidarc.vApps.vEngineering.HW.vNodeInLong import vNodeInLong
        from vidarc.vApps.vEngineering.HW.vNodeInFloat import vNodeInFloat
        from vidarc.vApps.vEngineering.HW.vNodeInTime import vNodeInTime
        from vidarc.vApps.vEngineering.HW.vNodeOutput import vNodeOutput
        from vidarc.vApps.vEngineering.HW.vNodeOutBool import vNodeOutBool
        from vidarc.vApps.vEngineering.HW.vNodeOutInt import vNodeOutInt
        from vidarc.vApps.vEngineering.HW.vNodeOutLong import vNodeOutLong
        from vidarc.vApps.vEngineering.HW.vNodeOutFloat import vNodeOutFloat
        from vidarc.vApps.vEngineering.HW.vNodeOutTime import vNodeOutTime
        from vidarc.vApps.vEngineering.HW.vNodeInOut import vNodeInOut
        import vidarc.vApps.vEngineering.__link__
except:
    vtLog.vtLngTB('import')

def RegisterNodes(doc):
    try:
        if vcCust.is2Import(__name__,'__REG_NODE__',True):
            oBase=vNodeHW()
            doc.RegisterNode(oBase,False)
            for o in [vNodeInput(),
                    vNodeInBool(),vNodeInInt(),vNodeInLong(),vNodeInFloat(),vNodeInTime(),
                    vNodeOutput(),
                    vNodeOutBool(),vNodeOutInt(),vNodeOutLong(),vNodeOutFloat(),vNodeOutTime(),
                    vNodeInOut()]:
                doc.RegisterNode(o,False)
                doc.LinkRegisteredNode(oBase,o)
            lTypes=['Input','InBool','InInt','InLong','InFloat','InTime',
                        'Output','OutBool','OutInt','OutLong','OutFloat','OutTime',
                        'InOut','dataCollector',
                        'Fct','Prc','Bug','Test','Tool','ToDo',]
            lParamTypes=['Base','Prop','Documents','drawTiming','drawNode',
                        'Documents','Doc',]
            vidarc.vApps.vEngineering.__link__.LinkNodes(doc,[
                    ('hw',lTypes+lParamTypes),
                    ])
            for sBase in lTypes:
                vidarc.vApps.vEngineering.__link__.LinkNodes(doc,[
                    (sBase,lParamTypes),
                    ])
    except:
        vtLog.vtLngTB(__name__)
