#----------------------------------------------------------------------------
# Name:         vNodeInFloat.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060924
# CVS-ID:       $Id: vNodeInFloat.py,v 1.1 2006/10/09 11:25:20 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeInFloat(vXmlNodeEngineering):
    def __init__(self,tagName='InFloat'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'input float')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00oIDAT8\x8d\xbd\x92Q\x0e\xc0 \x08C\x0bz\xff\x1bo\xee\x8b\x840\x0b,&\
\xeb\x97Qx\xad\xa8\x88\x0edZ\xf7\xb5\x00@t\xc8\xee\\;\xcdq\xdd\x02xg\xe6^&\
\xf0b\x10\x893`Q\xdb\x80\x08\xca\xe2\x03\x1f\xae\xf0/\xa0\xf3|&:\x83\xa3\x04\
\xe6\xbcs\x8f\xfb\x14`\xd3g \xd3\xb4\xa2**\xab\x99\xde-k\xf2\x89^\x80\xac\
\xb9\xfaH\xc7\xaf\xf0\x00\xbb\x90L\x01\xd6I\x03X\x00\x00\x00\x00IEND\xaeB`\
\x82' 
