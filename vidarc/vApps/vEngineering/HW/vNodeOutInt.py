#----------------------------------------------------------------------------
# Name:         vNodeOutInt.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060924
# CVS-ID:       $Id: vNodeOutInt.py,v 1.1 2006/10/09 11:25:20 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeOutInt(vXmlNodeEngineering):
    def __init__(self,tagName='OutInt'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'output integer')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00`IDAT8\x8d\xc5\x92\xb1\x12\xc0 \x08CI\xf0\xff\xffX\xe9\xd2\xc1!\x16-\
\xbdkF\x94w\xc6\x04\xa0[E\\\x1d\xc4\xe8\x11\xa3\xc7k\xc0\xaeP\xb5\xd0\xd4p~:\
\xe8x\x02H\x0b\xd9R\n8\x91\x04\xcc\x16\xb2$\xfeO\x81f\xeb\xd2\xec\x94\xa9\
\x1c#@O?J.\xde\xe0\xa6\x86\x9f\x14I\xc1\xe4\xddj\n\x17\xf0\x974\x1d\xaf\xc9\
\x8dz\x00\x00\x00\x00IEND\xaeB`\x82' 
