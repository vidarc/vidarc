#----------------------------------------------------------------------------
# Name:         vNodeOutFloat.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060924
# CVS-ID:       $Id: vNodeOutFloat.py,v 1.1 2006/10/09 11:25:20 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeOutFloat(vXmlNodeEngineering):
    def __init__(self,tagName='OutFloat'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'output float')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00nIDAT8\x8d\xbdRA\x0e\xc0 \x08k\xd1\xff\xffX\xf1\xb2%\xc4\x80\xe0a\
\xeb\xc9@Z\n\x95\x94\x86\x13t\x0e\x05\x00J\xa3\xd7\x97\ny\x7f\x97\x04\xec\
\xe4hz\xea\xc0"\x12\xe1~\x83\xc8jY`\x17:\xd9\x07.V\xf8W\xa0\x12\xdf\x8b\xf0\
\x06W\x0et\x0e\xf5&Eu\x8b\xee\x15-)K\x81\x94\x96\xee\xe9\x12\x1f\xe1\xee\x15\
o\x1c\xb8)\xd8\xff\xffy\n\x0b%D@\'\xfb\x1f\x07\x9c\x00\x00\x00\x00IEND\xaeB`\
\x82' 
