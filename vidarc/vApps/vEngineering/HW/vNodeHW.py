#----------------------------------------------------------------------------
# Name:         vNodeHW.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: vNodeHW.py,v 1.2 2006/08/29 10:06:25 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeHW(vXmlNodeEngineering):
    def __init__(self,tagName='hw'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'hardware')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x96IDAT8\x8d\xedSA\n\xc4 \x0c\x9c\xe8~k\xc1\x8f\x15\x02}W!\x1f\xab\
\xd3C\xa3\x1b/n\xa1\x97\x1e*\x04\x93L2\xc6\x11ER\xc6\x9d\x95nu?\x83\x80\xf5K\
\xd6\x9d\xe7\xbe\x91u\xf1x\'\xeb\xc2\x0b\x1cJ\x00\x04\x94\x922\\T\x02\xe8\
\xf1\xcc>3\xea8\x81\xa4UZN\xd2*\r\x0b\x04\x16\x1a\xd4s\xc5\r\x00\xb2\xe3\nV#\
`\r\x9b_\x01(\x8e\x1b\x7f~\xe9\xf5\x17^\xa1M\xa0\xc1\xb7X\xf0_\xc4q"\x1d\x05\
\xf6D8m`G\x14\xec\xf47\x02\xd6E\x95\xf7/\xe0\x00\xa1\xe9dJ*\x90f\xc5\x00\x00\
\x00\x00IEND\xaeB`\x82' 
