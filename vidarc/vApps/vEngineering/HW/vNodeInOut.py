#----------------------------------------------------------------------------
# Name:         vNodeInOut.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060924
# CVS-ID:       $Id: vNodeInOut.py,v 1.1 2006/10/09 11:25:20 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vXmlNodeEngineering import vXmlNodeEngineering
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeInOut(vXmlNodeEngineering):
    def __init__(self,tagName='InOut'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vXmlNodeEngineering.__init__(self,tagName)
    def GetDescription(self):
        return _(u'in-/output')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xc6IDAT8\x8d\xa5\x93a\xaa\xc3 \x10\x84?M\xae\xf5`\xa1\xe7*\x08=Wa\
\xa1\xe7\xaa\xd3\x1f\x9ah\xe5\xa55ta1\xbb\xee\x0c\x99QC\x88\x0b\xbf\xc4z\xb4\
\xa1|\x17x\xd71B\xbc\x84q.\x1e\x11\xb4\xe14\xd4\x93\x04\xb3\x11\x01\x94\xafR\
\xfeSY\x9fR~j\x96`-Z\x8d\x10\x97\x00\x0f`\xe1\x0cA\xdc\x8c*\x7fp\xaf@\x9f\
\x96\xb0\x82\xd1@\x862:C@\xb9\x07.H\x02\xbaL\x02\xab\xdfV3\xedk\x88K\xc5\x92\
\x04\xde5|'\xf8\xaf\xae \x81\xa9#`'\xd9\x00m\xd8\xde\xea\xd2k\x98X<p\xc0Q\
\xbe\n\xbc\x9e\xc8\x16\x9f\xfcp\xd6\xa3\x1bv\xca\xc4O9J\x1ce}%h\x00\x1bL\xad\
\x84\xb3\xcf\xb9\x7f\x9d!\xdev\xd9/\xf8\x90u\xbb\xca\x10U\x80\x00\x00\x00\
\x00IEND\xaeB`\x82" 
