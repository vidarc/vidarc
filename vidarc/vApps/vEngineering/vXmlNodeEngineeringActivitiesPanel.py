#Boa:FramePanel:vXmlNodeEngineeringActivitiesPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeEngineeringActivitiesPanel.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051213
# CVS-ID:       $Id: vXmlNodeEngineeringActivitiesPanel.py,v 1.10 2011/01/17 18:03:13 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputText
import vidarc.tool.input.vtInputUnique
import vidarc.vApps.vHum.vXmlHumInputTreeGroupsUsers
import vidarc.vApps.vHum.vXmlHumInputTreeUsr
import vidarc.tool.input.vtInputTextMultiLineML
import vidarc.tool.input.vtInputTextML
import vidarc.tool.time.vtTimeDateGM
import vidarc.ext.state.veStateInput
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons
import  wx.lib.anchors as anchors

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.tool.xml.vtXmlNodePanel import *
    from vidarc.tool.xml.vtXmlDomConsumerLang import vtXmlDomConsumerLang
    import vidarc.tool.lang.vtLgBase as vtLgBase
    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt

    from vidarc.tool.xml.vtXmlNodeLogPanel import vtXmlNodeLogPanel
    from vidarc.tool.net.vNetXmlWxGui import *
    import vidarc.tool.xml.vtXmlDom as vtXmlDom
    import sys,string
    import images
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VXMLNODEENGINEERINGACTIVITIESPANEL, 
 wxID_VXMLNODEENGINEERINGACTIVITIESPANELCHCPRIOR, 
 wxID_VXMLNODEENGINEERINGACTIVITIESPANELCHCSEVERITY, 
 wxID_VXMLNODEENGINEERINGACTIVITIESPANELGCBBADD, 
 wxID_VXMLNODEENGINEERINGACTIVITIESPANELGCBBSET, 
 wxID_VXMLNODEENGINEERINGACTIVITIESPANELLBLAUTHOR, 
 wxID_VXMLNODEENGINEERINGACTIVITIESPANELLBLENDDT, 
 wxID_VXMLNODEENGINEERINGACTIVITIESPANELLBLNAME, 
 wxID_VXMLNODEENGINEERINGACTIVITIESPANELLBLNR, 
 wxID_VXMLNODEENGINEERINGACTIVITIESPANELLBLPRIOR, 
 wxID_VXMLNODEENGINEERINGACTIVITIESPANELLBLSER, 
 wxID_VXMLNODEENGINEERINGACTIVITIESPANELLBLSTARTDT, 
 wxID_VXMLNODEENGINEERINGACTIVITIESPANELLBLSTATE, 
 wxID_VXMLNODEENGINEERINGACTIVITIESPANELLSTACT, 
 wxID_VXMLNODEENGINEERINGACTIVITIESPANELNBACT, 
 wxID_VXMLNODEENGINEERINGACTIVITIESPANELPNASSIGN, 
 wxID_VXMLNODEENGINEERINGACTIVITIESPANELPNDESC, 
 wxID_VXMLNODEENGINEERINGACTIVITIESPANELPNGEN, 
 wxID_VXMLNODEENGINEERINGACTIVITIESPANELPNLOG, 
 wxID_VXMLNODEENGINEERINGACTIVITIESPANELVIASSIGN, 
 wxID_VXMLNODEENGINEERINGACTIVITIESPANELVIAUTHOR, 
 wxID_VXMLNODEENGINEERINGACTIVITIESPANELVIDESC, 
 wxID_VXMLNODEENGINEERINGACTIVITIESPANELVIENDDT, 
 wxID_VXMLNODEENGINEERINGACTIVITIESPANELVINAME, 
 wxID_VXMLNODEENGINEERINGACTIVITIESPANELVINR, 
 wxID_VXMLNODEENGINEERINGACTIVITIESPANELVISTARTDT, 
 wxID_VXMLNODEENGINEERINGACTIVITIESPANELVISTATE, 
] = [wx.NewId() for _init_ctrls in range(27)]

class vXmlNodeEngineeringActivitiesPanel(wx.Panel,vtXmlNodePanel,vtXmlDomConsumerLang):
    VERBOSE=0
    def _init_coll_bxsAssign_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viAssign, 1, border=0, flag=wx.EXPAND)

    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstAct, (0, 0), border=0, flag=wx.EXPAND, span=(4,
              5))
        parent.AddWindow(self.gcbbSet, (4, 4), border=0, flag=0, span=(1, 1))
        parent.AddWindow(self.gcbbAdd, (5, 4), border=0, flag=0, span=(1, 1))
        parent.AddWindow(self.nbAct, (4, 0), border=0, flag=wx.EXPAND, span=(4,
              4))

    def _init_coll_gbsGen_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblNr, (0, 0), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.viNr, (0, 1), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.lblState, (1, 2), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viState, (1, 3), border=0, flag=wx.EXPAND,
              span=(1, 3))
        parent.AddWindow(self.lblAuthor, (1, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viAuthor, (1, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.lblSer, (2, 0), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.chcSeverity, (2, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.lblPrior, (2, 2), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.chcPrior, (2, 3), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.lblStartDT, (3, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viStartDt, (3, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.lblEndDT, (3, 2), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viEndDt, (3, 3), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.lblName, (0, 2), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viName, (0, 3), border=0, flag=wx.EXPAND, span=(1,
              3))

    def _init_coll_bxsDesc_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viDesc, 1, border=0, flag=wx.EXPAND)

    def _init_coll_nbAct_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.pnGen, select=True,
              text=_(u'General'))
        parent.AddPage(imageId=-1, page=self.pnDesc, select=False,
              text=_(u'Description'))
        parent.AddPage(imageId=-1, page=self.pnAssign, select=False,
              text=_(u'Assignment'))

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=4, vgap=4)

        self.gbsGen = wx.GridBagSizer(hgap=4, vgap=4)

        self.bxsDesc = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsAssign = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_gbsData_Items(self.gbsData)
        self._init_coll_gbsGen_Items(self.gbsGen)
        self._init_coll_bxsDesc_Items(self.bxsDesc)
        self._init_coll_bxsAssign_Items(self.bxsAssign)

        self.SetSizer(self.gbsData)
        self.pnAssign.SetSizer(self.bxsAssign)
        self.pnGen.SetSizer(self.gbsGen)
        self.pnDesc.SetSizer(self.bxsDesc)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEENGINEERINGACTIVITIESPANEL,
              name=u'vXmlNodeEngineeringActivitiesPanel', parent=prnt,
              pos=wx.Point(0, 0), size=wx.Size(507, 290),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(499, 263))
        self.SetAutoLayout(True)

        self.lstAct = wx.ListView(id=wxID_VXMLNODEENGINEERINGACTIVITIESPANELLSTACT,
              name=u'lstAct', parent=self, pos=wx.Point(0, 0), size=wx.Size(476,
              92), style=wx.LC_REPORT)
        self.lstAct.SetMinSize(wx.Size(-1, -1))
        self.lstAct.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstActListItemSelected,
              id=wxID_VXMLNODEENGINEERINGACTIVITIESPANELLSTACT)
        self.lstAct.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstActListColClick,
              id=wxID_VXMLNODEENGINEERINGACTIVITIESPANELLSTACT)
        self.lstAct.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstActListItemDeselected,
              id=wxID_VXMLNODEENGINEERINGACTIVITIESPANELLSTACT)

        self.gcbbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODEENGINEERINGACTIVITIESPANELGCBBADD,
              bitmap=vtArt.getBitmap(vtArt.Add), label=u'Add', name=u'gcbbAdd',
              parent=self, pos=wx.Point(400, 134), size=wx.Size(76, 30),
              style=0)
        self.gcbbAdd.SetMinSize(wx.Size(-1, -1))
        self.gcbbAdd.Bind(wx.EVT_BUTTON, self.OnGcbbAddButton,
              id=wxID_VXMLNODEENGINEERINGACTIVITIESPANELGCBBADD)

        self.gcbbSet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODEENGINEERINGACTIVITIESPANELGCBBSET,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=u'Set',
              name=u'gcbbSet', parent=self, pos=wx.Point(400, 96),
              size=wx.Size(76, 30), style=0)
        self.gcbbSet.SetMinSize(wx.Size(-1, -1))
        self.gcbbSet.Bind(wx.EVT_BUTTON, self.OnGcbbSetButton,
              id=wxID_VXMLNODEENGINEERINGACTIVITIESPANELGCBBSET)

        self.nbAct = wx.Notebook(id=wxID_VXMLNODEENGINEERINGACTIVITIESPANELNBACT,
              name=u'nbAct', parent=self, pos=wx.Point(0, 96), size=wx.Size(396,
              148), style=0)

        self.pnGen = wx.Panel(id=wxID_VXMLNODEENGINEERINGACTIVITIESPANELPNGEN,
              name=u'pnGen', parent=self.nbAct, pos=wx.Point(0, 0),
              size=wx.Size(388, 122), style=wx.TAB_TRAVERSAL)

        self.lblNr = wx.StaticText(id=wxID_VXMLNODEENGINEERINGACTIVITIESPANELLBLNR,
              label=u'number', name=u'lblNr', parent=self.pnGen, pos=wx.Point(0,
              0), size=wx.Size(48, 30), style=wx.ALIGN_RIGHT)
        self.lblNr.SetMinSize(wx.Size(-1, -1))

        self.viNr = vidarc.tool.input.vtInputUnique.vtInputUnique(id=wxID_VXMLNODEENGINEERINGACTIVITIESPANELVINR,
              name=u'viNr', parent=self.pnGen, pos=wx.Point(52, 0),
              size=wx.Size(100, 30), style=0)

        self.lblName = wx.StaticText(id=wxID_VXMLNODEENGINEERINGACTIVITIESPANELLBLNAME,
              label=u'name', name=u'lblName', parent=self.pnGen,
              pos=wx.Point(156, 0), size=wx.Size(43, 30), style=wx.ALIGN_RIGHT)
        self.lblName.SetMinSize(wx.Size(-1, -1))

        self.viName = vidarc.tool.input.vtInputTextML.vtInputTextML(id=wxID_VXMLNODEENGINEERINGACTIVITIESPANELVINAME,
              name=u'viName', parent=self.pnGen, pos=wx.Point(203, 0),
              size=wx.Size(174, 30), style=0)

        self.viAuthor = vidarc.vApps.vHum.vXmlHumInputTreeUsr.vXmlHumInputTreeUsr(id=wxID_VXMLNODEENGINEERINGACTIVITIESPANELVIAUTHOR,
              name=u'viAuthor', parent=self.pnGen, pos=wx.Point(52, 34),
              size=wx.Size(100, 24), style=0)

        self.lblAuthor = wx.StaticText(id=wxID_VXMLNODEENGINEERINGACTIVITIESPANELLBLAUTHOR,
              label=u'author', name=u'lblAuthor', parent=self.pnGen,
              pos=wx.Point(0, 34), size=wx.Size(48, 24), style=wx.ALIGN_RIGHT)
        self.lblAuthor.SetMinSize(wx.Size(-1, -1))

        self.viState = vidarc.ext.state.veStateInput.veStateInput(id=wxID_VXMLNODEENGINEERINGACTIVITIESPANELVISTATE,
              name=u'viState', parent=self.pnGen, pos=wx.Point(203, 34),
              size=wx.Size(174, 24), style=0)

        self.lblSer = wx.StaticText(id=wxID_VXMLNODEENGINEERINGACTIVITIESPANELLBLSER,
              label=u'severity', name=u'lblSer', parent=self.pnGen,
              pos=wx.Point(0, 62), size=wx.Size(48, 21), style=wx.ALIGN_RIGHT)
        self.lblSer.SetMinSize(wx.Size(-1, -1))

        self.chcSeverity = wx.Choice(choices=[u'blocker', u'critical', u'major',
              u'normal', u'minor', u'trivial', u'enhancement'],
              id=wxID_VXMLNODEENGINEERINGACTIVITIESPANELCHCSEVERITY,
              name=u'chcSeverity', parent=self.pnGen, pos=wx.Point(52, 62),
              size=wx.Size(100, 21), style=0)
        self.chcSeverity.SetSelection(3)
        self.chcSeverity.SetLabel(u'')
        self.chcSeverity.SetMinSize(wx.Size(-1, -1))

        self.chcPrior = wx.Choice(choices=[u'highest', u'higher', u'normal',
              u'lower', u'lowest'],
              id=wxID_VXMLNODEENGINEERINGACTIVITIESPANELCHCPRIOR,
              name=u'chcPrior', parent=self.pnGen, pos=wx.Point(203, 62),
              size=wx.Size(80, 21), style=0)
        self.chcPrior.SetSelection(2)
        self.chcPrior.SetLabel(u'')
        self.chcPrior.SetMinSize(wx.Size(-1, -1))

        self.lblStartDT = wx.StaticText(id=wxID_VXMLNODEENGINEERINGACTIVITIESPANELLBLSTARTDT,
              label=u'start date', name=u'lblStartDT', parent=self.pnGen,
              pos=wx.Point(0, 87), size=wx.Size(48, 24), style=wx.ALIGN_RIGHT)
        self.lblStartDT.SetMinSize(wx.Size(-1, -1))

        self.viStartDt = vidarc.tool.time.vtTimeDateGM.vtTimeDateGM(id=wxID_VXMLNODEENGINEERINGACTIVITIESPANELVISTARTDT,
              name=u'viStartDt', parent=self.pnGen, pos=wx.Point(52, 87),
              size=wx.Size(100, 24), style=0)

        self.lblEndDT = wx.StaticText(id=wxID_VXMLNODEENGINEERINGACTIVITIESPANELLBLENDDT,
              label=u'end date', name=u'lblEndDT', parent=self.pnGen,
              pos=wx.Point(156, 87), size=wx.Size(43, 24),
              style=wx.ALIGN_RIGHT)
        self.lblEndDT.SetMinSize(wx.Size(-1, -1))

        self.viEndDt = vidarc.tool.time.vtTimeDateGM.vtTimeDateGM(id=wxID_VXMLNODEENGINEERINGACTIVITIESPANELVIENDDT,
              name=u'viEndDt', parent=self.pnGen, pos=wx.Point(203, 87),
              size=wx.Size(80, 24), style=0)

        self.pnDesc = wx.Panel(id=wxID_VXMLNODEENGINEERINGACTIVITIESPANELPNDESC,
              name=u'pnDesc', parent=self.nbAct, pos=wx.Point(0, 0),
              size=wx.Size(388, 122), style=wx.TAB_TRAVERSAL)

        self.viDesc = vidarc.tool.input.vtInputTextMultiLineML.vtInputTextMultiLineML(id=wxID_VXMLNODEENGINEERINGACTIVITIESPANELVIDESC,
              name=u'viDesc', parent=self.pnDesc, pos=wx.Point(0, 0),
              size=wx.Size(388, 122), style=0)

        self.pnAssign = wx.Panel(id=wxID_VXMLNODEENGINEERINGACTIVITIESPANELPNASSIGN,
              name=u'pnAssign', parent=self.nbAct, pos=wx.Point(0, 0),
              size=wx.Size(388, 122), style=wx.TAB_TRAVERSAL)

        self.viAssign = vidarc.vApps.vHum.vXmlHumInputTreeGroupsUsers.vXmlHumInputTreeGroupsUsers(id=wxID_VXMLNODEENGINEERINGACTIVITIESPANELVIASSIGN,
              name=u'viAssign', parent=self.pnAssign, pos=wx.Point(0, 0),
              size=wx.Size(388, 122), style=0)

        self.lblPrior = wx.StaticText(id=wxID_VXMLNODEENGINEERINGACTIVITIESPANELLBLPRIOR,
              label=u'priority', name=u'lblPrior', parent=self.pnGen,
              pos=wx.Point(156, 62), size=wx.Size(43, 21),
              style=wx.ALIGN_RIGHT)
        self.lblPrior.SetMinSize(wx.Size(-1, -1))

        self.lblState = wx.StaticText(id=wxID_VXMLNODEENGINEERINGACTIVITIESPANELLBLSTATE,
              label=u'state', name=u'lblState', parent=self.pnGen,
              pos=wx.Point(156, 34), size=wx.Size(43, 24),
              style=wx.ALIGN_RIGHT)
        self.lblState.SetMinSize(wx.Size(-1, -1))

        self._init_coll_nbAct_Pages(self.nbAct)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,lWidgets=[self.chcSeverity,self.chcPrior],
                lEvent=[
                        #(self.txtLogName       , wx.EVT_TEXT),
                        (self.chcSeverity       , wx.EVT_CHOICE),
                        (self.chcPrior          , wx.EVT_CHOICE),
                ])
        vtXmlDomConsumerLang.__init__(self)
        self.Move(pos)
        self.SetSize(size)
        
        self.gbsData.AddGrowableCol(0)
        self.gbsData.AddGrowableCol(1)
        self.gbsData.AddGrowableCol(2)
        self.gbsData.AddGrowableCol(3)
        self.gbsData.AddGrowableRow(3)
        self.gbsData.AddGrowableRow(7)
        self.gbsData.AddGrowableRow(10)

        self.gbsGen.AddGrowableCol(0)
        self.gbsGen.AddGrowableCol(1)
        self.gbsGen.AddGrowableCol(2)
        self.gbsGen.AddGrowableCol(3)
        self.gbsGen.AddGrowableCol(4)

        self.lstSort=[]
        self.bSortAsc=False
        self.iSortCol=-1
        self.selIdx=-1
        self.nodeIdAct=-1
        self.selAssign=-1
        self.humDoc=None
        self.grps=None
        self.colOrder=['status','nr','prev','name','assign','prior','severity',
                'date','start','end','usr']
        self.lstAct.InsertColumn(0,_(u'State'),wx.LIST_FORMAT_LEFT,80)
        self.lstAct.InsertColumn(1,_(u'Nr'),wx.LIST_FORMAT_RIGHT,60)
        self.lstAct.InsertColumn(2,_(u'Name'),wx.LIST_FORMAT_LEFT,130)
        #self.lstAct.InsertColumn(4,u'Assign',wx.LIST_FORMAT_LEFT,70)
        self.lstAct.InsertColumn(3,_(u'Prior'),wx.LIST_FORMAT_LEFT,100)
        self.lstAct.InsertColumn(4,_(u'Severity'),wx.LIST_FORMAT_LEFT,100)
        self.lstAct.InsertColumn(5,_(u'Start'),wx.LIST_FORMAT_LEFT,80)
        self.lstAct.InsertColumn(6,_(u'End'),wx.LIST_FORMAT_LEFT,80)
        self.lstAct.InsertColumn(7,_(u'Date'),wx.LIST_FORMAT_LEFT,120)
        self.lstAct.InsertColumn(8,_(u'Author'),wx.LIST_FORMAT_LEFT,70)
        self.lstAct.InsertColumn(9,_(u'Prev'),wx.LIST_FORMAT_LEFT,40)
        self.viName.SetTagName('name')
        self.viDesc.SetTagName('desc')
        self.viState.SetNodeAttr('Activity','actState')
        self.viStartDt.SetTagName('start')
        self.viEndDt.SetTagName('end')
        self.viAuthor.SetTagNames('usr','name')
        self.viNr.SetTagName('actID')
        #self.viAssign.SetTagName(doc)
        
        self.pnLog=vtXmlNodeLogPanel(parent=self.nbAct,id=-1,
            pos=wx.DefaultPosition,size=wx.DefaultSize,style=wx.TAB_TRAVERSAL,
            name='pnActLog')
        self.pnLog.SetSuppressNetNotify(True)
        self.nbAct.AddPage(imageId=-1, page=self.pnLog, select=False,
              text=_(u'Log'))
        
        self.bModified=False
        self.netHum=None
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
    def IsSame(self,id):
        if VERBOSE>0:
            self.__logDebug__('   nodeId:%d;nodeIdAct:%d;       id:%d'%(self.nodeId,self.nodeIdAct,long(id)))
        if self.nodeIdAct>=0:
            if self.nodeIdAct==long(id):
                return True
        return False
    def IsSameNum(self,id):
        if VERBOSE>0:
            self.__logDebug__('   nodeId:%d;nodeIdAct:%d;       id:%d'%(self.nodeId,self.nodeIdAct,id))
        if self.nodeIdAct>=0:
            if self.nodeIdAct==id:
                return True
        return False
    def SetNetDocs(self,d):
        if 'vHum' in d:
            dd=d['vHum']
            #self.netHum=dd['doc']
            #self.viHum.SetDocTree(dd['doc'],dd['bNet'])
            self.viAuthor.SetDocTree(dd['doc'],dd['bNet'])
            self.viAssign.SetDocTree(dd['doc'],dd['bNet'])
        self.pnLog.SetNetDocs(d)
    def SetRegNode(self,obj):
        if self.VERBOSE:
            vtLog.CallStack('')
            print obj
        self.objRegNode=obj
    def SetAutoApply(self,flag):
        pass
    def __setModified__(self,state):
        self.bModified=state
    def ClearLang(self):
        pass
    def UpdateLang(self):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        try:
            self.objRegNode.SetLang(self.doc.GetLang())
            l=self.objRegNode.GetSeverityTrans()
            if self.VERBOSE:
                vtLog.CallStack('')
                print self.doc.GetLang()
                print l
            if l is not None:
                self.chcSeverity.Clear()
                for s in l:
                    self.chcSeverity.Append(s)
            l=self.objRegNode.GetPriorTrans()
            if self.VERBOSE:
                print l
            if l is not None:
                self.chcPrior.Clear()
                for s in l:
                    self.chcPrior.Append(s)
        except:
            vtLog.vtLngTB(self.GetName())
    def __Clear__(self):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        self.__setModified__(False)
        self.__unlockSel__()
        self.selIdx=-1
        self.nodeIdAct=-1
        self.lstAct.DeleteAllItems()
        self.__clear__()
    def __clear__(self):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        self.viName.Clear()
        self.viDesc.Clear()
        self.viAuthor.Clear()
        self.viAssign.Clear()
        self.viNr.Clear()
        try:
            self.chcSeverity.SetSelection(self.objRegNode.GetSeverityIdx(None))
            self.chcPrior.SetSelection(self.objRegNode.GetPriorIdx(None))
        except:
            self.chcSeverity.SetSelection(3)
            self.chcPrior.SetSelection(2)
        self.viStartDt.Clear()
        self.viEndDt.Clear()
        self.pnLog.Clear()
        self.__Lock__(False)
    def __Lock__(self,bFlag):
        self.__logDebug__('bFlag:%d'%(bFlag))
        if bFlag:
            self.viName.Enable(False)
            self.viDesc.Enable(False)
            self.viAuthor.Enable(False)
            self.viAssign.Enable(False)
            self.chcSeverity.Enable(False)
            self.chcPrior.Enable(False)
            self.viAssign.Enable(False)
            self.viState.Enable(False)
            self.viStartDt.Enable(False)
            self.viEndDt.Enable(False)
            self.viNr.Enable(False)
            self.pnLog.Enable(False)
            self.gcbbAdd.Enable(False)
            self.gcbbSet.Enable(False)
        else:
            self.viName.Enable(True)
            self.viDesc.Enable(True)
            self.viAssign.Enable(True)
            self.chcSeverity.Enable(True)
            self.chcPrior.Enable(True)
            self.viAssign.Enable(True)
            self.viState.Enable(True)
            self.viStartDt.Enable(True)
            self.viEndDt.Enable(True)
            if self.doc is None:
                self.viNr.Enable(False)
                self.viAuthor.Enable(False)
                self.gcbbAdd.Enable(False)
                self.gcbbAdd.Enable(False)
                return
            if self.doc.GetLoggedInSecLv()>=24:
                self.viNr.Enable(True)
                self.viAuthor.Enable(True)
            else:
                self.viNr.Enable(False)
                self.viAuthor.Enable(False)
            self.pnLog.Enable(True)
            if self.objRegNode.IsAclOk(self.doc.ACL_MSK_ADD):
                self.gcbbAdd.Enable(True)
            else:
                self.gcbbAdd.Enable(False)
            if self.objRegNode.IsAclOk(self.doc.ACL_MSK_WRITE):
                self.gcbbSet.Enable(True)
            else:
                self.gcbbSet.Enable(False)
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        vtXmlDomConsumerLang.SetDoc(self,doc)
        #self.doc=doc
        oAct=doc.GetReg('Activity')
        self.SetRegNode(oAct)
        
        oLog=doc.GetReg('log')
        self.pnLog.SetRegNode(oLog)
        self.pnLog.SetDoc(doc,bNet=bNet)
        
        self.viName.SetDoc(doc)
        self.viDesc.SetDoc(doc)
        self.viState.SetDoc(doc)
        self.viStartDt.SetDoc(doc)
        self.viEndDt.SetDoc(doc)
        self.viAuthor.SetDoc(doc,bNet)
        self.viAssign.SetDoc(doc)
        self.viNr.SetDoc(doc)
        if doc is not None:
            self.viNr.SetUniqueFunc(doc.GetSortedActNrIds)
        if dDocs is not None:
            if 'vHum' in dDocs:
                dd=dDocs['vHum']
                #self.netHum=dd['doc']
                #self.viHum.SetDocTree(dd['doc'],dd['bNet'])
                self.viAuthor.SetDocTree(dd['doc'],dd['bNet'])
                self.viAssign.SetDocTree(dd['doc'],dd['bNet'])
            #self.pnLog.SetNetDocs(d)
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            self.pnLog.Clear()
            if self.bModified==True:
                # ask
                dlg=wx.MessageDialog(self,_(u'Do you to apply modified data?') ,
                            u'vEngineering Activities',
                            wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                if dlg.ShowModal()==wx.ID_YES:
                    self.GetNode(self.node)
            self.__setModified__(False)
            self.node=node
            self.name=self.objRegNode.GetTagName()
            self.__clear__()
            childs=self.doc.getChilds(self.node,self.name)
            self.logs={}
            if self.objRegNode.IsAclOk(self.doc.ACL_MSK_READ):
                for c in childs:
                    self.__addInfo__(c)
                self.__showInfo__()
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
        except:
            self.__logTB__()
    def Apply(self,bDoApply=False):
        self.__setModified__(False)
        return False
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viName.Close()
        self.viDesc.Close()
        self.viState.Close()
        self.viStartDt.Close()
        self.viEndDt.Close()
        self.viAuthor.Close()
        #self.viNr.SetTagName('actID')
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.__setModified__(False)
    def __getActivityVal__(self,o,node):
        self.viName.GetNode(node)
        self.viDesc.GetNode(node)
        self.viStartDt.GetNode(node)
        self.viEndDt.GetNode(node)
        self.viAuthor.GetNode(node)
        self.viAssign.GetNode(node)
        if o is None:
            self.viState.GetNode(node)
            self.viNr.GetNode(node)
        else:
            if self.selIdx>=0:
                idOld=self.lstAct.GetItemData(self.selIdx)
                nodeOld=self.doc.getNodeByIdNum(idOld)
                if nodeOld is not None:
                    self.objRegNode.SetPrev(node,self.objRegNode.GetNr(nodeOld))
        self.objRegNode.SetSeverityIdx(node,self.chcSeverity.GetSelection())
        self.objRegNode.SetPriorIdx(node,self.chcPrior.GetSelection())
        self.doc.AlignNode(node)
        return self.viNr.GetValue()
    def OnGcbbAddButton(self, event):
        event.Skip()
        try:
            if self.doc is None:
                return
            if self.objRegNode.IsAclOk(self.doc.ACL_MSK_ADD)==False:
                return
            if self.node is None:
                return
            #self.viNr.Clear()
            n=self.objRegNode.Create(self.node,self.__getActivityVal__)
            if n is not None:
                self.__addInfo__(n)
                sNr=self.objRegNode.GetNr(n)
                wx.CallAfter(self.__showInfo__,sNr)
        except:
            vtLog.vtLngTB(self.GetName())
    def __addInfo__(self,node):
        try:
            l=self.objRegNode.AddValuesToLst(node)
            id=long(self.doc.getKey(node))
            self.logs[id]=l
        except:
            vtLog.vtLngTB(self.GetName())
    def __showInfo__(self,sNr=''):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        try:
            try:
                iNr=long(sNr)
            except:
                iNr=-1
            id=-1
            self.objRegNode.SetLang(self.doc.GetLang())
            if self.selIdx>=0:
                idOld=self.lstAct.GetItemData(self.selIdx)
            else:
                idOld=-1
            #self.__unlockSel__()
            self.selIdx=-1
            self.lstAct.DeleteAllItems()
            if self.iSortCol==-1:
                sortKeys=None
            else:
                sortKeys=[self.iSortCol]
            ll=self.objRegNode.SortAndTranslateActivities(zip(self.logs.values(),self.logs.keys()),sortKeys)
            if self.VERBOSE:
                vtLog.CallStack('')
                vtLog.pprint(ll)
            for l in ll:
                index = self.lstAct.InsertImageStringItem(sys.maxint, l[0], -1)
                for i in xrange(1,10):
                    self.lstAct.SetStringItem(index,i, l[i], -1)
                self.lstAct.SetItemData(index,l[-1])
                try:
                    if long(l[1])==iNr:
                        id=long(l[-1])
                        self.lstAct.SetItemState(index,wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
                        self.selIdx=index
                        #node=self.doc.getNodeByIdNum(id)
                        #self.pnLog.SetNode(node)
                except:
                    pass
            if id!=idOld:
                self.__unlockSel__()
                if id>=0:
                    if self.objRegNode.IsAclOk(self.doc.ACL_MSK_WRITE):
                        node=self.doc.getNodeByIdNum(id)
                        if node is not None:
                            self.doc.startEdit(node)
                        node=self.doc.getNodeByIdNum(id)
                        self.pnLog.SetNode(node)
        except:
            vtLog.vtLngTB(self.GetName())
    def __guiData2NodeDict__(self,n,d):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        dn={}
        try:
            dn['nr']=d['nr']
        except:
            dn['nr']=''
        dn['name']=self.txtActName.GetValue()
        dn['desc']=self.txtActDesc.GetValue()
        dn['usr']=self.chcUsr.GetStringSelection()
        dn['status']=self.chcStatus.GetStringSelection()
        dn['severity']=self.chcSeverity.GetStringSelection()
        dn['prior']=self.chcPrior.GetStringSelection()
        dn['start']=self.zStart.GetValue()
        dn['end']=self.zEnd.GetValue()
        
        lstGrp=[]
        lstUsr=[]
        iLen=self.lstAssign.GetItemCount()
        hums=self.netDocHuman.GetSortedUsrIds()
        grps=self.netDocHuman.GetSortedGrpIds()
        for i in range(iLen):
            it=self.lstAssign.GetItem(i,0)
            k=it.m_text
            if k=='g':
                it=self.lstAssign.GetItem(i,1)
                sGrp=it.m_text
                grpID=grps.GetIdByName(sGrp)[0]
                lstGrp.append(grpID)
            elif k=='u':
                it=self.lstAssign.GetItem(i,1)
                sUsr=it.m_text
                usrID=hums.GetIdByName(sUsr)[0]
                lstUsr.append(usrID)
        dn['assign_grp']=string.join(lstGrp,',')
        dn['assign_usr']=string.join(lstUsr,',')
        hums=self.netDocHuman.GetSortedUsrIds()
        for k in dn.keys():
            d[k]=dn[k]
            if n is not None:
                self.doc.setNodeText(n,k,dn[k])
                if k in ['usr']:
                    tmpNode=self.doc.getChild(n,k)
                    if k=='usr':
                        self.doc.setAttribute(tmpNode,'fid',
                                        hums.GetIdByName(dn[k]))
        if d['nr']=='':
            settingsNode=self.doc.getChild(self.doc.getRoot(),'settings')
            tNode=self.doc.getChild(settingsNode,'activities')
            if tNode is None:
                lst=[{'tag':'current','val':'0'},
                     {'tag':'max','val':'10'}
                    ]
                tNode=self.doc.createChildByLst(settingsNode,'activities',lst)
                self.doc.AlignNode(tNode)
            try:
                sVal=self.doc.getNodeText(tNode,'current')
                sMax=self.doc.getNodeText(tNode,'max')
                iVal=long(sVal)
                iMax=long(sMax)
                iVal+=1
                if iVal<iMax:
                    self.doc.setNodeText(tNode,'current',str(iVal))
                else:
                    return -2
                d['nr']=str(iVal)
                return 0
            except:
                return -1
        return 0
    def __setGuiData__(self,id):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        try:
            self.__clear__()
            node=self.doc.getNodeByIdNum(id)
            if node is None:
                return
            self.viName.SetNode(node)
            self.viDesc.SetNode(node)
            self.viState.SetNode(node)
            self.viStartDt.SetNode(node)
            self.viEndDt.SetNode(node)
            self.viAuthor.SetNode(node)
            self.viAssign.SetNode(node)
            self.viNr.SetNode(node)
            self.chcSeverity.SetSelection(self.objRegNode.GetSeverityIdx(node))
            self.chcPrior.SetSelection(self.objRegNode.GetPriorIdx(node))
        except:
            vtLog.vtLngTB(self.GetName())
    def OnGcbbSetButton(self, event):
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        if self.selIdx<0:
            return
        try:
            if self.doc is None:
                return
            if self.objRegNode.IsAclOk(self.doc.ACL_MSK_WRITE)==False:
                return
            id=self.lstAct.GetItemData(self.selIdx)
            node=self.doc.getNodeByIdNum(id)
            if node is None:
                return
            if self.doc.IsLocked(node):
                self.__getActivityVal__(None,node)
                self.doc.doEdit(node)
                self.__addInfo__(node)
                sNr=self.objRegNode.GetNr(node)
                self.__showInfo__(sNr)
            self.SetModified(False)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnLstActListItemSelected(self, event):
        event.Skip()
        try:
            if self.IsMainThread()==False:
                self.__logCritical__('called by thread'%())
            idx=event.GetIndex()
            self.nodeIdAct=self.lstAct.GetItemData(idx)
            if self.objRegNode.IsAclOk(self.doc.ACL_MSK_WRITE):
                node=self.doc.getNodeByIdNum(self.nodeIdAct)
                if node is not None:
                    self.doc.startEdit(node)
            #nr=self.logs[id]
            self.__setGuiData__(self.nodeIdAct)
            node=self.doc.getNodeByIdNum(self.nodeIdAct)
            self.pnLog.SetNode(node)
            self.selIdx=idx
        except:
            self.__logTB__()
    def OnLstActListColClick(self, event):
        event.Skip()
        try:
            if self.IsMainThread()==False:
                self.__logCritical__('called by thread'%())
            iCol=event.GetColumn()
            if iCol==self.iSortCol:
                if self.bSortAsc:
                    self.bSortAsc=False
                else:
                    self.iSortCol=-1
                    self.bSortAsc=True
            else:
                self.iSortCol=iCol
                self.bSortAsc=True
            self.__showInfo__()
        except:
            self.__logTB__()
    def OnCbAddAssignButton(self, event):
        event.Skip()
        try:
            if self.IsMainThread()==False:
                self.__logCritical__('called by thread'%())
            iGrp=self.chcAssignGrp.GetSelection()
            if iGrp>0:
                # add group
                sGrp=self.chcAssignGrp.GetStringSelection()
                #grpID=self.grps.GetIdByName(sGrp)[0]
                index = self.lstAssign.InsertImageStringItem(sys.maxint, 'g', 0)
                self.lstAssign.SetStringItem(index,1, sGrp, -1)
                self.chcAssignGrp.SetSelection(0)
            iUsr=self.chcAssignUsr.GetSelection()
            if iUsr>0:
                sUsr=self.chcAssignUsr.GetStringSelection()
                #usrID=self.humans.GetIdByName(sUsr)[0]
                index = self.lstAssign.InsertImageStringItem(sys.maxint, 'u', 1)
                self.lstAssign.SetStringItem(index,1, sUsr, -1)
                self.chcAssignUsr.SetSelection(0)
        except:
            self.__logTB__()
    def OnCbAssignDelButton(self, event):
        event.Skip()
        try:
            if self.IsMainThread()==False:
                self.__logCritical__('called by thread'%())
            if self.selAssign>=0:
                self.lstAssign.DeleteItem(self.selAssign)
                self.selAssign=-1
        except:
            self.__logTB__()
    def __unlockSel__(self):
        if self.selIdx>=0:
            self.nodeIdAct=self.lstAct.GetItemData(self.selIdx)
            node=self.doc.getNodeByIdNum(self.nodeIdAct)
            self.nodeIdAct=-1
            if node is None:
                return
            if self.doc.IsLocked(node):
                self.doc.endEdit(node)
    def OnLstActListItemDeselected(self, event):
        event.Skip()
        try:
            self.__unlockSel__()
            self.selIdx=-1
            self.__clear__()
        except:
            self.__logTB__()
    def OnLstAssignListItemSelected(self, event):
        event.Skip()
        try:
            self.selAssign=event.GetIndex()
        except:
            self.__logTB__()
    def OnLstAssignListItemDeselected(self, event):
        event.Skip()
        try:
            self.selAssign=-1
        except:
            self.__logTB__()
