#!/usr/bin/env python
#----------------------------------------------------------------------
#----------------------------------------------------------------------------
# Name:         encode_dat.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120210
# CVS-ID:       $Id: encode_obj.py,v 1.1 2015/02/28 20:32:47 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

command_lines = [
    "-u -i -n Design            imgObj/Design.png           imgObj.py",
    "-a -u -n Diagram           imgObj/Diagram.png          imgObj.py",
    "-a -u -n Object            imgObj/Object.png           imgObj.py",
    "-a -u -n Objects           imgObj/Objects.png          imgObj.py",
    "-a -u -n ObjectsComp       imgObj/ObjectsComp.png      imgObj.py",
    "-a -u -n Pack              imgObj/Pack.png             imgObj.py",
    "-a -u -n Package           imgObj/Package.png          imgObj.py",
    "-a -u -n Product           imgObj/Product.png          imgObj.py",
    "-a -u -n Properties        imgObj/Properties.png       imgObj.py",
    "-a -u -n Resources         imgObj/Resources.png        imgObj.py",
    "-a -u -n RGB               imgObj/RGB.png              imgObj.py",
    "-a -u -n Sections          imgObj/Sections.png         imgObj.py",
    "-a -u -n Store             imgObj/Store.png            imgObj.py",
    "-a -u -n Structure         imgObj/Structure.png        imgObj.py",
    "-a -u -n Symbol            imgObj/Symbol.png           imgObj.py",
    ]
