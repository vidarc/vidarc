#----------------------------------------------------------------------------
# Name:         vNodeProperties.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120407
# CVS-ID:       $Id: vNodeParameters.py,v 1.4 2013/05/20 21:19:44 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vApps.vEngineering.vNodeEngBase import vNodeEngBase
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vNodeParameters(vNodeEngBase):
    def __init__(self,tagName='parameters'):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        vNodeEngBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'parameters')
    def GetTransDescription(self):
        return u'parameters'
    # ---------------------------------------------------------
    # specific
    def GetGrpDict(self):
        return {
            None:0,
            'method':[-1,None,]
            }
    def GetProcessNodeInc(self):
        return [
            {'lTag':['method',],'level':1,},
            #'t_free','t_char','t_float','t_int','t_long',
            #    't_struct','t_class','t_bool','t_string','t_list',
            #{'lTag':['method'],'level':10,'headline':u'method(s)'},
            ]
    def GetCfgDictOld(self,nodeCfg,lang):
        dCfg=vNodeEngBase.GetCfgDict(self,nodeCfg,lang)
        dCfgBase={
            u'typePrimitive':[
                    _(u'01 type primitive'),u'typePrimitive',-1,
                    {'val':u'int','type':'choice','it00':'int','it01':'char','it02':'float','it03':'long','it04':'bool','it05':'double','it06':'string','it07':'time','it08':'date','it09':'datetime','it10':'uint','it11':'uchar','it12':'ulong'},
                    {'val':u'int','type':'choice','it00':'int','it01':'char','it02':'float','it03':'long','it04':'bool','it05':'double','it06':'string','it07':'time','it08':'date','it09':'datetime','it10':'uint','it11':'uchar','it12':'ulong'},],
            u'typeName':[
                    _(u'10 type name'),u'typeName',-10,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'fieldStart':[
                    _(u'15 field start'),u'fieldStart',-15,
                    {'val':u'0','type':'int',},
                    {'val':u'0','type':'int',}],
            u'fieldSize':[
                    _(u'16 field size'),u'fieldSize',-16,
                    {'val':u'0','type':'int',},
                    {'val':u'0','type':'int',}],
            u'valMin':[
                    _(u'50 minimum value'),u'valMin',-50,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'valMax':[
                    _(u'51 maximum value'),u'valMax',-51,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'valDft':[
                    _(u'52 default value'),u'valDft',-52,
                    {'val':u'','type':'text',},
                    {'val':u'','type':'text',}],
            u'skipProc':[
                    _(u'90 skip processing'),u'skipProc',-90,
                    {'val':u'false','type':'choice','it00':'true','it01':'false'},
                    {'val':u'false','type':'choice','it00':'true','it01':'false'}],
            u'orderProc':[
                    _(u'91 order processing'),u'orderProc',-91,
                    {'val':u'false','type':'choice','it00':'true','it01':'false'},
                    {'val':u'false','type':'choice','it00':'true','it01':'false'}],
            u'grpProc':[
                    _(u'95 group processing'),u'skipProc',-95,
                    {'val':u'false','type':'choice','it00':'true','it01':'false'},
                    {'val':u'false','type':'choice','it00':'true','it01':'false'}],
            }
        dCfgBase.update(dCfg)
        return dCfgBase
    # ---------------------------------------------------------
    # inheritance
    def IsRelBase(self):
        "is a relative base, for hierarchy name calculation"
        return True
    def HasGraphics(self):
        "images are defined"
        return GUI
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xeeIDAT8\x8d\xb5\x93An\x83@\x0cE\x9fgz\x91\\#R\x0e\x90\xe4(!9\x0c\
\x98SDY\xa2*\xcb\n\x95ez\x04\x96\x9c\x02\xdc\x05\x1d\xc4@\xa0R\xa3~\xc9\xd2\
\xd8c\x7f\xfd\xf1\xd7\x888\xcf+x\x03\xb0\xae\xb5\xbf\x0c\x8b\xf3\xe2BbV\x0c\
\xa1\xa9\x0eM\x9ajt\x17"R0F\x9e\xd5$\x97\x84\xaa\xac\x00\xd8\xee\xb6\x80r:o\
\x16e\xf4\x02\xac0\xb3\xc2\x00\xab\xca\xca\x00\x03l|\x1fj\xe1,\xce\xe3\x9e\
\xd3\xf68\xec\x0fQ>\x95\x0f\xcc\t4\xd5\x1f\xd9\xf0~\xbf\x8bu\xad\x8dw2\xc5\
\x8c\xe0t\xde\x0cK\x0c\xc3\x8b\xef\x07D\x9c\xc7\xba\xd6\xa6\xd2~\x83\xc81\
\xb6q\x8c<\xab\xfb\x069\x92g\xf5*\xd1\xff\xd9(\xce\xd34\x8di\xaa\x91uLl\x9c)\
\x08\xf8\xfc(\xedv\xbdE\xb5g{Z\xb4\xf1\xf1\xf5\x00 \xb9$\xac\xd98\xb8\xb0\
\xd8\xb1\x02q^\xe4\xd5\xef\xfc\r\x8fB\x9f\x10\xf9\xdb\xd4\xbc\x00\x00\x00\
\x00IEND\xaeB`\x82' 
#if 0:
#    _(u'method(s)')