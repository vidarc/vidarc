#----------------------------------------------------------------------------
# Name:         vXmlNodeSupplierProvide.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060619
# CVS-ID:       $Id: vXmlNodeSupplierProvide.py,v 1.2 2006/08/29 10:06:30 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeAttrML import *
try:
    if vcCust.is2Import(__name__):
        from vidarc.tool.input.vtInputAttrCfgMLPanel import vtInputAttrCfgMLPanel
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeSupplierProvide(vtXmlNodeAttrML):
    NODE_ATTRS=[
        ]
    FUNCS_GET_SET_4_LST=[]
    def __init__(self,tagName='provide',cfgBase=['cfg']):
        global _
        _=vtLgBase.assignPluginLang('vSupplier')
        vtXmlNodeAttrML.__init__(self,tagName,cfgBase=cfgBase)
    def GetDescription(self):
        return _(u'provide')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def Is2Create(self):
        return False
    def Is2Add(self):
        return True
    def IsMultiple(self):
        return False
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return True
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xcaIDAT8\x8d\xa5\x93\xd1\x11\xc2 \x10D\xf7\x924`\tZ\x82\x8c\x1d\xc4\
\x12,\xc1\xa4\x04[1\xa4\x15;\xd0\xb3\x14\xad@\xf1#^<H \xcc\xb8_0\xf0v\xf6\
\xb8\x83\xa8(\xf1\x8f\xaa\xd4\xa1{\xbf\x9c\xdeSQRx\x87\xe6\x12h\xd0]\xbb\xe1\
\xe2\xae\x9d5\xf2\x0c\xe6@\x00`f\xd8\xfb\xb0\xb6\xd6zFDE\x99\x05\xd6u\r\x008\
\xac\x9f^\xa2J\xe0\\P\xd4\x1d\r\xda\x9e\x87G\xbc\x9d\x9blP\x97\x00\xa8.\x08l\
\x1cc\xb5?-\x82\x13\x03\xad\xcd\xe3\x02\xacM\x12L\x1aH"fN\xc2\x13\x83f\x0b\
\x00f\x11\xcaJ\x90R\xdb\xff\x92\x11\x80q\x06\xa4\x1b\xa2\xb0\x04\x0f\xfcN\
\xe38\x89z\x98t[c\xe0\xb8\x0f\xffB\xf8\x81b`\xd4 4\x8a\x81\xa2\x0f\xa7Bkjs\
\x1a\x01\xfa\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        return None
    def GetAddDialogClass(self):
        return None
    def GetPanelClass(self):
        if GUI:
            return vtInputAttrCfgMLPanel
        else:
            return None

