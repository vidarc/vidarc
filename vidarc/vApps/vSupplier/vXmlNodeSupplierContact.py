#----------------------------------------------------------------------------
# Name:         vXmlNodeSupplierContact.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060619
# CVS-ID:       $Id: vXmlNodeSupplierContact.py,v 1.1 2006/07/17 11:25:01 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlNodeContactMultiple import vtXmlNodeContactMultiple

class vXmlNodeSupplierContact(vtXmlNodeContactMultiple):
    # ---------------------------------------------------------
    # inheritance
    def Is2Create(self):
        return False
    def Is2Add(self):
        return False
    def IsMultiple(self):
        return False
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return True
