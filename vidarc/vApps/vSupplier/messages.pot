# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-07-04 19:42+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: vSupplierAppl.py:43 vSupplierAppl.py:47
msgid "  import library ..."
msgstr ""

#: vSupplierMainFrame.py:360
msgid " (undef*)"
msgstr ""

#: vSupplierMainFrame.py:335
msgid "%b-%d-%Y   %H:%M:%S wk:%U"
msgstr ""

#: ../../tool/log/vtLogFileViewerFrame.py:668
msgid "%b-%d-%Y   %H:%M:%S wk:%W"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMaster.py:705
#: ../../tool/net/vtNetSecXmlGuiMaster.py:731
msgid "&Browse\tAlt+B"
msgstr ""

#: vSupplierMainFrame.py:101
msgid "&File"
msgstr ""

#: vSupplierAppl.py:121
msgid "--config <filename>"
msgstr ""

#: vSupplierAppl.py:124
msgid "--file <filename>"
msgstr ""

#: vSupplierAppl.py:127
msgid "--help"
msgstr ""

#: vSupplierAppl.py:130
msgid "--lang <language id according ISO639>"
msgstr ""

#: vSupplierAppl.py:131
msgid "--log <level>"
msgstr ""

#: vSupplierAppl.py:120
msgid "-c <filename>"
msgstr ""

#: vSupplierAppl.py:123
msgid "-f <filename>"
msgstr ""

#: vSupplierAppl.py:132
msgid "0 = debug"
msgstr ""

#: vSupplierAppl.py:133
msgid "1 = information"
msgstr ""

#: vSupplierAppl.py:134
msgid "2 = waring"
msgstr ""

#: vSupplierAppl.py:135
msgid "3 = error"
msgstr ""

#: vSupplierAppl.py:136
msgid "4 = critical"
msgstr ""

#: vSupplierAppl.py:137
msgid "5 = fatal"
msgstr ""

#: vSupplierMainFrame.py:114
msgid "About"
msgstr ""

#: ../../tool/input/vtInputAttrCfgMLDialog.py:286
#: ../../tool/xml/vtXmlGrpTree.py:1591
msgid "Add"
msgstr ""

#: ../../tool/net/vtNetLockRequestDialog.py:141
msgid "Agree"
msgstr ""

#: ../../tool/net/vNetLoginDialog.py:129
#: ../../tool/net/vtNetSecLoginAliasPanel.py:76
#: ../../tool/net/vtNetSecLoginDialog.py:227
#: ../../tool/net/vtNetSecXmlGuiMasterBrowsePanel.py:185
msgid "Alias"
msgstr ""

#: ../../tool/xml/vtXmlGrpTree.py:1643
msgid "Align"
msgstr ""

#: ../../tool/xml/vtXmlGrpTree.py:1337
msgid "Align XML"
msgstr ""

#: vSupplierMainFrame.py:103
msgid "Analyse"
msgstr ""

#: ../../tool/log/vtLogFileViewerFrame.py:503
#: ../../tool/net/vtNetSecLoginAliasPanel.py:74
msgid "Appl"
msgstr ""

#: ../../tool/net/vtNetSecLoginDialog.py:217
#: ../../tool/net/vtNetSecXmlGuiMasterBrowsePanel.py:220
msgid "Application"
msgstr ""

#: ../../tool/input/vtInputAttrCfgMLDialog.py:309
#: ../../tool/input/vtInputAttrCfgMLPanel.py:155
#: ../../tool/net/vNetLoginDialog.py:182
#: ../../tool/net/vNetXmlSynchEditResolveDialog.py:98
#: ../../tool/net/vtNetSecCfgDialog.py:95
#: ../../tool/net/vtNetSecLoginDialog.py:267
#: ../../tool/xml/vtXmlFilterPanel.py:301
msgid "Apply"
msgstr ""

#: ../../tool/input/vtInputAttrCfgMLDialog.py:238
msgid "Attribute"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMaster.py:616
#: ../../tool/xml/vtXmlImportOfficeDialog.py:1096
msgid "Browse"
msgstr ""

#: ../../tool/xml/vtXmlImportOfficeDialog.py:871
#: ../../tool/xml/vtXmlImportOfficeDialog.py:1302
msgid "Build"
msgstr ""

#: ../../tool/log/vtLogFileViewerFrame.py:348
msgid "Call Stack"
msgstr ""

#: ../../tool/input/vtInputAttrCfgMLDialog.py:301
#: ../../tool/net/vNetLoginDialog.py:190
#: ../../tool/net/vtNetSecCfgDialog.py:103
#: ../../tool/net/vtNetSecLoginDialog.py:275
#: ../../tool/net/vtNetSecXmlGuiMasterBrowsePanel.py:285
#: ../../tool/xml/vtXmlExportNodeDialog.py:95
#: ../../tool/xml/vtXmlImportOfficeDialog.py:970
msgid "Cancel"
msgstr ""

#: ../../tool/xml/vtXmlExportNodeDialog.py:83
msgid "Choose a file"
msgstr ""

#: ../../tool/xml/vtXmlNodeAddrPanel.py:134
msgid "City"
msgstr ""

#: ../../tool/xml/vtXmlImportOfficeDialog.py:1310
msgid "Clear"
msgstr ""

#: vSupplierMainFrame.py:585
#: ../../tool/net/vtNetSecXmlGuiMasterLogDialog.py:69
#: ../../tool/net/vtNetSecXmlGuiMasterStatusDialog.py:69
#: ../../tool/xml/vtXmlFilterPanel.py:294 ../../tool/xml/vtXmlFindPanel.py:330
msgid "Close"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMaster.py:1249
msgid "Close connection to use this feautre."
msgstr ""

#: ../../tool/xml/vtXmlImportOfficeDialog.py:849
#: ../../tool/xml/vtXmlImportOfficeDialog.py:879
#: ../../tool/xml/vtXmlImportOfficeDialog.py:1139
msgid "Col"
msgstr ""

#: ../../tool/xml/vtXmlImportOfficeDialog.py:858
#: ../../tool/xml/vtXmlImportOfficeDialog.py:1263
msgid "Compare"
msgstr ""

#: ../../tool/input/vtInputAttrCfgMLPanel.py:183
#: ../../tool/xml/vtXmlGrpTree.py:1646
msgid "Config"
msgstr ""

#: ../../tool/xml/vtXmlGrpTree.py:1342
msgid "Configuration"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMasterBrowsePanel.py:277
msgid "Connect"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMaster.py:726
msgid "Connect\tAlt+C"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMaster.py:1249
msgid "Connection still active!"
msgstr ""

#: ../../tool/xml/vtXmlImportOfficeDialog.py:983
msgid "Count"
msgstr ""

#: ../../tool/xml/vtXmlNodeAddrPanel.py:158
msgid "Country"
msgstr ""

#: ../../tool/xml/vtXmlFindPanel.py:436
msgid "Date"
msgstr ""

#: ../../tool/xml/vtXmlFindPanel.py:436
msgid "Date Time"
msgstr ""

#: ../../tool/input/vtInputAttrCfgMLPanel.py:163
#: ../../tool/xml/vtXmlImportOfficeDialog.py:1043
msgid "Del"
msgstr ""

#: ../../tool/input/vtInputAttrCfgMLDialog.py:293
#: ../../tool/xml/vtXmlGrpTree.py:1610
msgid "Delete"
msgstr ""

#: ../../tool/xml/vtXmlGrpTree.py:1763
msgid "Delete selected node?"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMaster.py:623
msgid "Disconnect"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMaster.py:735
msgid "Disconnect\tAlt+D"
msgstr ""

#: ../../tool/xml/vtXmlNodeAddrPanel.py:184
msgid "Distance"
msgstr ""

#: ../common/vSplashFrame.py:343
msgid "Document"
msgstr ""

#: ../../tool/xml/vtXmlGrpTree.py:1598
msgid "Duplicate"
msgstr ""

#: vSupplierMainFrame.py:94
msgid "E&xit\tAlt+X"
msgstr ""

#: ../../tool/xml/vtXmlGrpTree.py:1606
msgid "Edit"
msgstr ""

#: ../../tool/xml/vtXmlImportOfficeDialog.py:1177
msgid "Estimated"
msgstr ""

#: ../../tool/xml/vtXmlImportOfficeDialog.py:1996
msgid "Excel files (*.xls)|*.xls|StarOffice files (*.sxc)|*.sxc|all files (*.*)|*.*"
msgstr ""

#: ../../tool/log/vtLogFileViewerFrame.py:324
msgid "Exit"
msgstr ""

#: ../../tool/xml/vtXmlExportNodeDialog.py:103
#: ../../tool/xml/vtXmlGrpTree.py:1330
msgid "Export"
msgstr ""

#: ../../tool/xml/vtXmlExportNodeDialog.py:84
msgid "Export File:"
msgstr ""

#: ../../tool/log/vtLogFileViewerFrame.py:333
msgid "File"
msgstr ""

#: ../../tool/xml/vtXmlImportOfficeDialog.py:1085
msgid "Filename"
msgstr ""

#: ../../tool/xml/vtXmlFindPanel.py:434
msgid "Filter"
msgstr ""

#: ../../tool/xml/vtXmlGrpTree.py:1635
msgid "Find"
msgstr ""

#: ../../tool/xml/vtXmlFindPanel.py:435
msgid "Float"
msgstr ""

#: ../../tool/xml/vtXmlImportOfficeDialog.py:865
msgid "General"
msgstr ""

#: ../../tool/log/vtLogFileViewerFrame.py:527
msgid "Generate"
msgstr ""

#: ../../tool/xml/vtXmlGrpTree.py:1556 ../../tool/xml/vtXmlGrpTree.py:1582
msgid "Grouping"
msgstr ""

#: ../../tool/net/vtNetSecLoginAliasPanel.py:85
msgid "Groups"
msgstr ""

#: ../../tool/xml/vtXmlImportOfficeDialog.py:856
msgid "Hierarchy"
msgstr ""

#: ../../tool/net/vNetLoginDialog.py:134
#: ../../tool/net/vtNetSecLoginAliasPanel.py:78
#: ../../tool/net/vtNetSecLoginDialog.py:182
#: ../../tool/net/vtNetSecXmlGuiMasterBrowsePanel.py:187
msgid "Host"
msgstr ""

#: ../../tool/net/vtNetXmlLocksPanel.py:124
msgid "IP-adr"
msgstr ""

#: ../../tool/xml/vtXmlImportOfficeDialog.py:1160
#: ../../tool/xml/vtXmlGrpTree.py:1325
msgid "Import"
msgstr ""

#: ../../tool/input/vtInputAttrCfgMLDialog.py:725
msgid "Import Image"
msgstr ""

#: ../../tool/xml/vtXmlImportOfficeDialog.py:867
msgid "Input"
msgstr ""

#: ../../tool/xml/vtXmlFindPanel.py:435
msgid "Integer"
msgstr ""

#: ../../tool/xml/vtXmlImportOfficeDialog.py:859
msgid "Key"
msgstr ""

#: ../../tool/xml/vtXmlGrpTree.py:1472 ../../tool/xml/vtXmlGrpTree.py:1517
msgid "Language"
msgstr ""

#: ../../tool/xml/vtXmlGrpTree.py:1449
msgid "Languages"
msgstr ""

#: ../../tool/log/vtLogFileViewerFrame.py:494
msgid "Level"
msgstr ""

#: ../../tool/net/vNetXmlSynchEditResolveDialog.py:54
msgid "Local"
msgstr ""

#: vSupplierMainFrame.py:117
msgid "Log"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMaster.py:773
msgid "Log\tAlt+L"
msgstr ""

#: ../common/vSplashFrame.py:377
msgid "Log Name"
msgstr ""

#: ../../tool/net/vtNetSecLoginAliasPanel.py:80
msgid "Login"
msgstr ""

#: ../../tool/xml/vtXmlFindPanel.py:435
msgid "Long"
msgstr ""

#: ../../tool/net/vtNetSecLoginAliasPanel.py:82
msgid "Lv"
msgstr ""

#: ../../tool/xml/vtXmlImportOfficeDialog.py:857
msgid "Map"
msgstr ""

#: ../../tool/xml/vtXmlImportOfficeDialog.py:869
msgid "Mapping"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMaster.py:765
msgid "Master\tAlt+M"
msgstr ""

#: ../../tool/log/vtLogFileViewerFrame.py:350
#: ../../tool/net/vtNetLockRequestDialog.py:103
msgid "Message"
msgstr ""

#: ../../tool/xml/vtXmlGrpTree.py:1347
msgid "Messages"
msgstr ""

#: ../../tool/input/vtInputAttrCfgMLDialog.py:192
#: ../../tool/xml/vtXmlImportOfficeDialog.py:877
#: ../../tool/xml/vtXmlImportOfficeDialog.py:1012
#: ../../tool/xml/vtXmlNodeAddrPanel.py:98
msgid "Name"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMaster.py:742
msgid "New\tCtrl+N"
msgstr ""

#: ../../tool/net/vtNetLockRequestDialog.py:136
msgid "Node"
msgstr ""

#: ../../tool/xml/vtXmlImportOfficeDialog.py:962
msgid "Ok"
msgstr ""

#: ../../tool/log/vtLogFileViewerFrame.py:321
#: ../../tool/net/vtNetSecLoginAliasPanel.py:247
#: ../../tool/net/vtNetSecXmlGuiMasterBrowsePanel.py:269
#: ../../tool/xml/vtXmlImportOfficeDialog.py:1996
msgid "Open"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMaster.py:746
msgid "Open\tCtrl+O"
msgstr ""

#: ../../tool/log/vtLogFileViewerFrame.py:465
msgid "Origin"
msgstr ""

#: ../../tool/input/vtInputAttrCfgMLDialog.py:725
msgid "PNG files (*.png)|*.png|all files (*.*)|*.*"
msgstr ""

#: ../../tool/net/vNetLoginDialog.py:149
#: ../../tool/net/vtNetSecLoginDialog.py:192
msgid "Password"
msgstr ""

#: ../common/vSplashFrame.py:561 ../common/vSplashFrame.py:597
#, possible-python-format
msgid "Please change logging name (%s)."
msgstr ""

#: ../common/vSplashFrame.py:565 ../common/vSplashFrame.py:601
#, possible-python-format
msgid "Please change logging port (%s)."
msgstr ""

#: ../../tool/xml/vtXmlExportNodeDialog.py:177
msgid "Please select a filename."
msgstr ""

#: ../../tool/xml/vtXmlExportNodeDialog.py:167
#: ../../tool/xml/vtXmlGrpTree.py:1935 ../../tool/xml/vtXmlGrpTree.py:1972
msgid "Please select a node to export."
msgstr ""

#: ../../tool/xml/vtXmlGrpTree.py:1911
msgid "Please select a node to import at."
msgstr ""

#: ../../tool/xml/vtXmlExportNodeDialog.py:188
msgid "Please select a valid filename."
msgstr ""

#: ../common/vSplashFrame.py:552 ../common/vSplashFrame.py:588
#, possible-python-format
msgid "Please setup logging port (%s)."
msgstr ""

#: ../common/vSplashFrame.py:421 ../../tool/net/vNetLoginDialog.py:139
#: ../../tool/net/vtNetSecXmlGuiMasterBrowsePanel.py:189
msgid "Port"
msgstr ""

#: ../../tool/xml/vtXmlImportOfficeDialog.py:1069
msgid "Read"
msgstr ""

#: ../../tool/xml/vtXmlNodeAddrPanel.py:146
msgid "Region"
msgstr ""

#: ../../tool/net/vtNetLockRequestDialog.py:149
msgid "Reject"
msgstr ""

#: ../../tool/xml/vtXmlGrpTree.py:1621
msgid "Release"
msgstr ""

#: ../../tool/log/vtLogFileViewerFrame.py:514
msgid "Reload"
msgstr ""

#: ../../tool/net/vNetXmlSynchEditResolveDialog.py:56
msgid "Remote"
msgstr ""

#: ../../tool/net/vtNetLockRequestDialog.py:120
#: ../../tool/xml/vtXmlGrpTree.py:1625
msgid "Request"
msgstr ""

#: ../../tool/xml/vtXmlImportOfficeDialog.py:1117
#: ../../tool/xml/vtXmlImportOfficeDialog.py:2087
msgid "Row"
msgstr ""

#: ../../tool/xml/vtXmlImportOfficeDialog.py:1323
msgid "Rows"
msgstr ""

#: ../../tool/xml/vtXmlImportOfficeDialog.py:1077
msgid "Save"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMaster.py:750
msgid "Save\tCtrl+S"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMaster.py:754
msgid "Save As\tCtrl+SHIFT+S"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMaster.py:758
msgid "Save Config\tCtrl+Alt+S"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMaster.py:838
msgid "Save Config File As"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMaster.py:1256
msgid "Save File As"
msgstr ""

#: ../../tool/log/vtLogFileViewerFrame.py:474
msgid "Sel"
msgstr ""

#: ../../tool/xml/vtXmlFindPanel.py:434
msgid "Select Filter"
msgstr ""

#: ../../tool/net/vtNetSecCfgDialog.py:84
msgid "Select local directory"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMaster.py:769
msgid "Setting\tAlt+S"
msgstr ""

#: ../../tool/xml/vtXmlImportOfficeDialog.py:1104
msgid "Sheet"
msgstr ""

#: ../../tool/xml/vtXmlGrpTree.py:1924 ../../tool/xml/vtXmlGrpTree.py:1949
#: ../../tool/xml/vtXmlGrpTree.py:1961 ../../tool/xml/vtXmlGrpTree.py:1985
#: ../../tool/xml/vtXmlGrpTree.py:1997 ../../tool/xml/vtXmlGrpTree.py:2009
msgid "Sorry, this feature is not released yet!"
msgstr ""

#: ../../tool/xml/vtXmlGrpTree.py:1640
msgid "Sort"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMaster.py:777
msgid "Status\tAlt+T"
msgstr ""

#: ../../tool/xml/vtXmlNodeAddrPanel.py:110
msgid "Street"
msgstr ""

#: ../../tool/xml/vtXmlFindPanel.py:435
msgid "String"
msgstr ""

#: ../../tool/xml/vtXmlImportOfficeDialog.py:1030
msgid "Template"
msgstr ""

#: ../../tool/xml/vtXmlImportOfficeDialog.py:1172
#: ../../tool/xml/vtXmlFindPanel.py:436
msgid "Time"
msgstr ""

#: vSupplierMainFrame.py:104
msgid "Tools"
msgstr ""

#: ../../tool/input/vtInputAttrCfgMLDialog.py:206
msgid "Translation"
msgstr ""

#: ../../tool/input/vtInputAttrCfgMLDialog.py:211
msgid "Type"
msgstr ""

#: vSupplierMainFrame.py:584
msgid "Unsaved data present! Do you want to save data?"
msgstr ""

#: ../../tool/log/vtLogFileViewerFrame.py:481
msgid "Unsel"
msgstr ""

#: ../../tool/net/vNetLoginDialog.py:144
#: ../../tool/net/vtNetSecLoginDialog.py:187
#: ../../tool/net/vtNetSecXmlGuiMasterBrowsePanel.py:191
msgid "User"
msgstr ""

#: vSupplierMainFrame.py:357
msgid "VIDARC Supplier"
msgstr ""

#: ../../tool/xml/vtXmlImportOfficeDialog.py:851
#: ../../tool/xml/vtXmlImportOfficeDialog.py:881
msgid "Value"
msgstr ""

#: vSupplierMainFrame.py:102
msgid "View"
msgstr ""

#: ../../tool/net/vtNetSecLoginAliasPanel.py:247
#: ../../tool/net/vtNetSecXmlGuiMaster.py:838
#: ../../tool/net/vtNetSecXmlGuiMaster.py:1256
msgid "XML files (*.xml)|*.xml|all files (*.*)|*.*"
msgstr ""

#: ../../tool/xml/vtXmlNodeAddrPanel.py:122
msgid "ZIP / Postal Code"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMasterLogPanel.py:54
#: ../../tool/net/vtNetSecXmlGuiMasterStatusPanel.py:40
#: ../../tool/net/vtNetSecXmlGuiMasterStatusPanel.py:147
msgid "access"
msgstr ""

#: ../../tool/xml/vtXmlNodeAddr.py:49
msgid "address"
msgstr ""

#: ../../tool/net/vtNetSecLoginDialog.py:243
msgid "all"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMasterLogPanel.py:135
msgid "appl"
msgstr ""

#: ../../tool/input/vtInputAttrCfgMLPanel.py:91
#: ../../tool/input/vtInputAttrCfgMLPanel.py:127
#: ../../tool/xml/vtXmlFilterPanel.py:157
#: ../../tool/xml/vtXmlFilterPanel.py:286 ../../tool/xml/vtXmlFindPanel.py:171
#: ../../tool/xml/vtXmlFindPanel.py:312
msgid "attribute"
msgstr ""

#: ../../tool/xml/vtXmlImportOfficeDialog.py:1051
msgid "auto open"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMasterLogPanel.py:55
#: ../../tool/net/vtNetSecXmlGuiMasterStatusPanel.py:41
msgid "blocked"
msgstr ""

#: ../../tool/xml/vtXmlNodeAddr.py:99
msgid "city"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMasterLogPanel.py:56
#: ../../tool/net/vtNetSecXmlGuiMasterStatusPanel.py:42
msgid "closed"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMasterBrowseDialog.py:167
msgid "config"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMasterStatusPanel.py:150
msgid "configuation"
msgstr ""

#: ../../tool/net/vtNetSecLoginDialog.py:524
msgid "connecting"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMasterStatusPanel.py:152
msgid "connection"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterCloseDialog.py:161
#: ../../tool/net/vtNetSecLoginDialog.py:543
msgid "connection closed"
msgstr ""

#: vSupplierMainFrame.py:468
msgid "connection closed. "
msgstr ""

#: ../../tool/net/vtNetSecLoginDialog.py:532
msgid "connection established"
msgstr ""

#: vSupplierMainFrame.py:446
msgid "connection established ... please wait"
msgstr ""

#: ../../tool/net/vtNetSecLoginDialog.py:527
msgid "connection fault"
msgstr ""

#: vSupplierMainFrame.py:451
msgid "connection lost."
msgstr ""

#: vSupplierMainFrame.py:464
msgid "content got. generate ..."
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMasterLogPanel.py:58
#: ../../tool/net/vtNetSecXmlGuiMasterStatusPanel.py:44
msgid "copy"
msgstr ""

#: ../../tool/xml/vtXmlNodeAddr.py:99
msgid "country"
msgstr ""

#: ../../tool/xml/vtXmlTreeAddDialog.py:63
msgid "description"
msgstr ""

#: ../../tool/net/vNetXmlSynchTreeList.py:362
msgid "difference"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMasterLogPanel.py:59
#: ../../tool/net/vtNetSecXmlGuiMasterStatusPanel.py:45
msgid "disconnected"
msgstr ""

#: ../../tool/xml/vtXmlNodeAddr.py:100
msgid "distance"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMasterLogPanel.py:59
#: ../../tool/net/vtNetSecXmlGuiMasterStatusPanel.py:45
msgid "established"
msgstr ""

#: ../../tool/xml/vtXmlGrpTree.py:1265 ../../tool/xml/vtXmlGrpTree.py:1283
msgid "export"
msgstr ""

#: ../../tool/xml/vtXmlGrpTree.py:1371
msgid "export CSV"
msgstr ""

#: ../../tool/xml/vtXmlGrpTree.py:1368
msgid "export Excel"
msgstr ""

#: ../../tool/xml/vtXmlGrpTree.py:1365
msgid "export XML"
msgstr ""

#: ../../tool/xml/vtXmlGrpTree.py:1387
msgid "export/import"
msgstr ""

#: ../../tool/log/vtLogFileViewerFrame.py:649
msgid "file"
msgstr ""

#: vSupplierMainFrame.py:414
msgid "file open fault."
msgstr ""

#: vSupplierMainFrame.py:403
msgid "file opened ..."
msgstr ""

#: vSupplierMainFrame.py:408
msgid "file opened and parsed."
msgstr ""

#: vSupplierMainFrame.py:380
msgid "generated."
msgstr ""

#: vSupplierMainFrame.py:457
msgid "get content ... "
msgstr ""

#: vSupplierAppl.py:117
msgid "help"
msgstr ""

#: ../../tool/net/vtNetXmlLocksPanel.py:122
msgid "id"
msgstr ""

#: ../../tool/net/vtNetSecLoginAliasPanel.py:117
msgid "identical login"
msgstr ""

#: ../../tool/xml/vtXmlGrpTree.py:1238
msgid "import"
msgstr ""

#: ../../tool/xml/vtXmlGrpTree.py:1383
msgid "import CSV"
msgstr ""

#: ../../tool/xml/vtXmlGrpTree.py:1380
msgid "import Excel"
msgstr ""

#: ../../tool/xml/vtXmlGrpTree.py:1377
msgid "import XML"
msgstr ""

#: ../../tool/xml/vtXmlExportNodeDialog.py:89
msgid "include children"
msgstr ""

#: vSupplierMainFrame.py:204 vSupplierMainFrame.py:205
msgid "information"
msgstr ""

#: ../../tool/xml/vtXmlImportOfficeDialog.py:1249
#: ../../tool/xml/vtXmlImportOfficeDialog.py:1738
#: ../../tool/xml/vtXmlImportOfficeDialog.py:2492
#: ../../tool/xml/vtXmlImportOfficeDialog.py:2594
msgid "key"
msgstr ""

#: ../../tool/log/vtLogFileViewerFrame.py:644
msgid "level"
msgstr ""

#: ../../tool/log/vtLogFileViewerFrame.py:648
msgid "line"
msgstr ""

#: ../../tool/net/vNetXmlSynchTreeList.py:40
#: ../../tool/net/vNetXmlSynchTreeList.py:371
msgid "local"
msgstr ""

#: ../../tool/net/vtNetSecCfgDialog.py:85
msgid "local directory:"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMasterBrowseDialog.py:174
msgid "log"
msgstr ""

#: ../../tool/net/vtNetSecLoginDialog.py:538
msgid "logged in"
msgstr ""

#: ../../tool/net/vtNetSecLoginDialog.py:535
msgid "login"
msgstr ""

#: vSupplierMainFrame.py:454
msgid "login ... "
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMasterBrowseDialog.py:182
msgid "master"
msgstr ""

#: ../../tool/log/vtLogFileViewerFrame.py:650
msgid "methode"
msgstr ""

#: ../../tool/log/vtLogFileViewerFrame.py:645
#: ../../tool/net/vtNetSecXmlGuiMasterLogPanel.py:136
msgid "msg"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMasterStatusPanel.py:144
#: ../../tool/xml/vtXmlNodeAddr.py:99
msgid "name"
msgstr ""

#: vSupplierMainFrame.py:191 vSupplierMainFrame.py:192
msgid "navigation"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMasterLogPanel.py:54
#: ../../tool/net/vtNetSecXmlGuiMasterStatusPanel.py:40
msgid "no access"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMasterLogPanel.py:55
#: ../../tool/net/vtNetSecXmlGuiMasterStatusPanel.py:41
#: ../../tool/net/vtNetSecXmlGuiMasterStatusPanel.py:148
msgid "notify"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMasterStatusPanel.py:149
msgid "open"
msgstr ""

#: vSupplierAppl.py:125
msgid "open the <filename> at start"
msgstr ""

#: vSupplierAppl.py:122
msgid "open the <filename> to configure application"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMasterLogPanel.py:56
#: ../../tool/net/vtNetSecXmlGuiMasterStatusPanel.py:42
msgid "opened"
msgstr ""

#: ../../tool/xml/vtXmlFilterPanel.py:161 ../../tool/xml/vtXmlFindPanel.py:175
msgid "operator"
msgstr ""

#: ../../tool/log/vtLogFileViewerFrame.py:643
msgid "origin"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMasterLogPanel.py:60
#: ../../tool/net/vtNetSecXmlGuiMasterStatusPanel.py:46
#: ../../tool/net/vtNetSecXmlGuiMasterStatusPanel.py:153
msgid "paused"
msgstr ""

#: ../../tool/net/vtNetXmlLocksPanel.py:126
msgid "port"
msgstr ""

#: ../../tool/xml/vtXmlNodeAddr.py:99
msgid "region"
msgstr ""

#: ../../tool/net/vNetXmlSynchTreeList.py:40
#: ../../tool/net/vNetXmlSynchTreeList.py:380
msgid "remote"
msgstr ""

#: ../../tool/log/vtLogFileViewerFrame.py:535
msgid "reverse"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMasterLogPanel.py:60
#: ../../tool/net/vtNetSecXmlGuiMasterStatusPanel.py:46
msgid "running"
msgstr ""

#: vSupplierMainFrame.py:512
msgid "save file ... "
msgstr ""

#: vSupplierMainFrame.py:514
msgid "save file finished."
msgstr ""

#: vSupplierAppl.py:128
msgid "show this screen"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMasterStatusPanel.py:146
msgid "state"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMasterBrowseDialog.py:189
#: ../../tool/net/vtNetSecXmlGuiMasterLogPanel.py:137
msgid "status"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMasterBrowsePanel.py:295
msgid "stop"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterCloseDialog.py:128
msgid "stopping thread and closing connections please wait ..."
msgstr ""

#: ../../tool/net/vtNetSecLoginAliasPanel.py:125
msgid "store password"
msgstr ""

#: ../../tool/xml/vtXmlNodeAddr.py:100
msgid "street"
msgstr ""

#: ../../tool/xml/vtXmlImportOfficeDialog.py:1615
msgid "structure"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMasterStatusPanel.py:151
msgid "synch"
msgstr ""

#: vSupplierMainFrame.py:439
msgid "synch aborted."
msgstr ""

#: vSupplierMainFrame.py:432
msgid "synch finished."
msgstr ""

#: vSupplierMainFrame.py:420 vSupplierMainFrame.py:427
msgid "synch started ..."
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMasterLogPanel.py:58
#: ../../tool/net/vtNetSecXmlGuiMasterStatusPanel.py:44
msgid "synchable"
msgstr ""

#: ../../tool/net/vNetXmlSynchTreeList.py:40
#: ../../tool/xml/vtXmlTreeAddDialog.py:62
msgid "tag"
msgstr ""

#: ../../tool/xml/vtXmlFilterPanel.py:155
#: ../../tool/xml/vtXmlFilterPanel.py:198 ../../tool/xml/vtXmlFindPanel.py:169
#: ../../tool/xml/vtXmlFindPanel.py:215
msgid "tagname"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterCloseDialog.py:157
msgid "thread stopped"
msgstr ""

#: ../../tool/log/vtLogFileViewerFrame.py:646
#: ../../tool/net/vtNetSecXmlGuiMasterLogPanel.py:134
#: ../../tool/net/vtNetXmlLocksPanel.py:130
msgid "time"
msgstr ""

#: ../../tool/net/vtNetSecLoginDialog.py:568
#: ../../tool/net/vtNetSecXmlGuiMasterLogPanel.py:57
#: ../../tool/net/vtNetSecXmlGuiMasterStatusPanel.py:43
msgid "unknown"
msgstr ""

#: ../../tool/net/vtNetXmlLocksPanel.py:128
msgid "usr"
msgstr ""

#: ../../tool/net/vNetXmlGuiMasterCloseDialog.py:124
msgid "vNet XML Master Close Dialog"
msgstr ""

#: ../../tool/xml/vtXmlNodeAddrPanel.py:194
msgid "vTool Address"
msgstr ""

#: ../../tool/net/vtNetSecXmlGuiMasterLogPanel.py:57
#: ../../tool/net/vtNetSecXmlGuiMasterStatusPanel.py:43
msgid "valid"
msgstr ""

#: vSupplierAppl.py:119
msgid "valid flags:"
msgstr ""

#: ../../tool/input/vtInputAttrCfgMLPanel.py:93
#: ../../tool/input/vtInputAttrCfgMLPanel.py:145
#: ../../tool/net/vtNetSecXmlGuiMasterStatusPanel.py:145
#: ../../tool/xml/vtXmlFilterPanel.py:159 ../../tool/xml/vtXmlFindPanel.py:173
msgid "value"
msgstr ""

#: ../../tool/input/vtInputAttrCfgMLPanel.py:202
msgid "vtInput Attr ML"
msgstr ""

#: ../../tool/log/vtLogFileViewerFrame.py:393
msgid "vtLogFileViewerFrame"
msgstr ""

#: ../../tool/xml/vtXmlFindDialog.py:20
msgid "vtXml Find"
msgstr ""

#: ../../tool/xml/vtXmlNodeAddr.py:100
msgid "zip"
msgstr ""
