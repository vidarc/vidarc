#----------------------------------------------------------------------------
# Name:         vXmlSupplierTree.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: vXmlSupplierTree.py,v 1.2 2006/08/29 10:06:30 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlGrpTree import *
from vidarc.vApps.vSupplier.vNetSupplier import *

import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.vApps.vSupplier.images as imgSupp

class vXmlSupplierTree(vtXmlGrpTree):
    def __init__(self, parent, id, pos, size, style, name,
                    master=False,controller=False,verbose=0):
        global _
        _=vtLgBase.assignPluginLang('vSupplier')
        vtXmlGrpTree.__init__(self,id=id, name=name,master=master,
              controller=controller,verbose=verbose,
              parent=parent, pos=pos, size=size,style=style)
        self.bLangMenu=True
        self.bGrpMenu=True
        #self.bImportMenu=True
        #self.bExportMenu=True
        #self.bEditMenu=True
        self.SetPossibleGrouping([
                ('normal'   , _(u'normal'), [] , 
                            {None:[('tag','',),('name','')],
                            #'user':[('name',''),('surname',''),('firstname','')],
                            #'group':[('name','')]
                            }),
                ('category'   , _(u'category'), [('category',''),] , 
                            {None:[('tag','',),('name','')],
                            }),
                ])
    def SetDftNodeInfos(self):
        self.SetNodeInfos(['tag','name','category','|id'])
    def SetupImageList(self):
        if vtXmlGrpTree.SetupImageList(self):
            self.__addElemImage2ImageList__('root',
                            imgSupp.getPluginImage(),
                            imgSupp.getPluginImage(),True)
            self.__addElemImage2ImageList__('suppliers',
                            imgSupp.getPluginImage(),
                            imgSupp.getPluginImage(),True)
            self.__addElemImage2ImageList__('category',
                            imgSupp.getPluginImage(),
                            imgSupp.getPluginImage(),True)
            self.SetImageList(self.imgLstTyp)

