#Boa:FramePanel:vSupplierPanel
#----------------------------------------------------------------------------
# Name:         vupplieranel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060620
# CVS-ID:       $Id: vSupplierPanel.py,v 1.4 2007/07/30 20:38:49 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.xml.vtXmlNodeRegListBook
import vidarc.tool.xml.vtXmlNodeTagCmdPanel
import time

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.vApps.common.vSystem as vSystem

from vidarc.vApps.vSupplier.vNetSupplier import *
from vidarc.vApps.vSupplier.vXmlSupplierTree import *
from vidarc.tool.net.vtNetSecXmlGuiMaster import *

from vidarc.vApps.vHum.vNetHum import *
from vidarc.vApps.vGlobals.vNetGlobals import *

from vidarc.tool.xml.vtXmlDom import vtXmlDom

import vidarc.vApps.vSupplier.images as images

def create(parent):
    return vSupplierPanel(parent)

[wxID_VSUPPLIERPANEL, wxID_VSUPPLIERPANELNBDATA, wxID_VSUPPLIERPANELPNDATA, 
 wxID_VSUPPLIERPANELSLWNAV, wxID_VSUPPLIERPANELSLWTOP, 
 wxID_VSUPPLIERPANELVITAG, 
] = [wx.NewId() for _init_ctrls in range(6)]

def getPluginImage():
    return images.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getPluginBitmap())
    return icon

class vSupplierPanel(wx.Panel):
    VERBOSE=0
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.nbData, 0, border=4, flag=wx.ALL | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=1, vgap=0)

        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_fgsData_Items(self.fgsData)

        self.pnData.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VSUPPLIERPANEL, name=u'vSupplierPanel',
              parent=prnt, pos=wx.Point(325, 29), size=wx.Size(650, 400),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(642, 373))
        self.Bind(wx.EVT_SIZE, self.OnSize)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VSUPPLIERPANELSLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 0), size=wx.Size(200,
              376), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetAutoLayout(True)
        self.slwNav.SetLabel(u'Navigation')
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetToolTipString(u'navigation')
        self.slwNav.SetDefaultSize(wx.Size(200, 100))
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VSUPPLIERPANELSLWNAV)

        self.slwTop = wx.SashLayoutWindow(id=wxID_VSUPPLIERPANELSLWTOP,
              name=u'slwTop', parent=self, pos=wx.Point(200, 0),
              size=wx.Size(440, 100), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwTop.SetDefaultSize(wx.Size(1000, 200))
        self.slwTop.SetAlignment(wx.LAYOUT_TOP)
        self.slwTop.SetAutoLayout(True)
        self.slwTop.SetSashVisible(wx.SASH_BOTTOM, True)
        self.slwTop.SetOrientation(wx.LAYOUT_HORIZONTAL)
        self.slwTop.SetLabel(_(u'information'))
        self.slwTop.SetToolTipString(_(u'information'))
        self.slwTop.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwTopSashDragged,
              id=wxID_VSUPPLIERPANELSLWTOP)

        self.pnData = wx.Panel(id=wxID_VSUPPLIERPANELPNDATA, name=u'pnData',
              parent=self, pos=wx.Point(208, 108), size=wx.Size(424, 252),
              style=wx.TAB_TRAVERSAL)

        self.viTag = vidarc.tool.xml.vtXmlNodeTagCmdPanel.vtXmlNodeTagCmdPanel(id=wxID_VSUPPLIERPANELVITAG,
              name=u'viTag', parent=self.slwTop, pos=wx.Point(0, 0),
              size=wx.Size(400, 90), style=0)

        self.nbData = vidarc.tool.xml.vtXmlNodeRegListBook.vtXmlNodeRegListBook(id=wxID_VSUPPLIERPANELNBDATA,
              name=u'nbData', parent=self.pnData, pos=wx.Point(4, 4),
              size=wx.Size(416, 244), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vSupplier')
        self._init_ctrls(parent)
        self.SetName(name)
        self.bActivated=False
        self.dCfg={}
        
        self.xdCfg=vtXmlDom(appl='vSupplierCfg',audit_trail=False)
        
        appls=['vSupplier','vHum','vGlobals']
        
        rect = parent.sbStatus.GetFieldRect(0)
        self.netMaster=vtNetSecXmlGuiMaster(parent.sbStatus,'Master',pos=(rect.x+2, rect.y+2),
                    size=(rect.width-4, rect.height-4),verbose=0)
        self.netSupplier=self.netMaster.AddNetControl(vNetSupplier,'vSupplier',synch=True,verbose=0,
                                            audit_trail=True)
        self.netHum=self.netMaster.AddNetControl(vNetHum,'vHum',synch=True,verbose=0)
        self.netGlb=self.netMaster.AddNetControl(vNetGlobals,'vGlobals',synch=True,verbose=0)
        self.netMaster.AddNetControl(None,None,verbose=0)
        
        self.netMaster.SetCfg(self.xdCfg,[],appls,'vSupplierAppl')
        
        self.vgpTree=vXmlSupplierTree(self.slwNav,wx.NewId(),
                pos=(0,0),size=(198,529),style=0,name="trSupplier",
                master=True,verbose=0)
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        #EVT_VTXMLTREE_ITEM_ADDED(self.vgpTree,self.OnTreeItemAdd)
        self.vgpTree.SetDoc(self.netSupplier,True)
        self.vgpTree.EnableImportMenu(True)
        self.vgpTree.EnableExportMenu(True)
        
        self.nbData.SetDoc(self.netSupplier,True)
        oSupplier=self.netSupplier.GetReg('supplier')
        
        self.viTag.AddWidgetDependent(self.nbData,oSupplier)
        self.viTag.SetDoc(self.netSupplier,True)
        self.viTag.SetRegNode(oSupplier,bIncludeBase=True)
        
        self.Move(pos)
        self.SetSize(size)
    def GetDocMain(self):
        return self.netSupplier
    def GetNetMaster(self):
        return self.netMaster
    def GetTreeMain(self):
        return self.vgpTree
    def OpenFile(self,fn):
        if self.netSupplier.ClearAutoFN()>0:
            self.netSupplier.Open(fn)
    def OpenCfgFile(self,fn=None):
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                self.netMaster.SetCfgNode()
                self.__setCfg__()
                return
        self.xdCfg.New(root='config')
        if fn is not None:
            self.xdCfg.SetFN(fn)
            self.netMaster.SetCfgNode()
        self.__setCfg__()
    def __setCfg__(self):
        try:
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnTreeItemSel(self,event):
        node=event.GetTreeNodeData()
        self.viTag.SetNode(node)
    def OnSlwNavSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iWidth=event.GetDragRect().width
            #if iWidth>80:
            #    iWidth=80
            self.dCfg['nav_sash']=str(iWidth)
            
            self.slwNav.SetDefaultSize((iWidth, 1000))
            
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            self.GetCfgData()
            #event.Skip()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSlwTopSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iHeight=event.GetDragRect().height
            #if iHeight>80:
            #    iHeight=80
            self.dCfg['nav_top']=str(iHeight)
            
            self.slwTop.SetDefaultSize((1000, iHeight))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            self.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnSize(self, event):
        event.Skip()
        try:
            bMod=False
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            
            self.pnData.Refresh()
            self.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())

    def SetCfgData(self,func,l,d):
        self.cfgFunc=func
        self.cfgOrigin=l
        self.dCfg=d
        try:
            i=int(self.dCfg['nav_sash'])
            self.slwNav.SetDefaultSize((i, 1000))
        except:
            pass
        try:
            i=int(self.dCfg['nav_top'])
            self.slwTop.SetDefaultSize((1000, i))
        except:
            pass
        try:
            pos=map(int,self.dCfg['pos'].split(','))
            self.Move(pos)
        except:
            pass
        try:
            sz=map(int,self.dCfg['size'].split(','))
            self.SetSize(sz)
        except:
            pass
    def GetCfgData(self):
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,self.dCfg)
    def GetAboutData(self):
        import __config__
        version=u' %d.%d.%d.%d'%(__config__.VER_MAJOR,__config__.VER_MINOR,
                    __config__.VER_RELEASE,__config__.VER_SUBREL)
        desc=_(u"""Supplier module.

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
""")
        return _(u'VIDARC Supplier'),desc,version
