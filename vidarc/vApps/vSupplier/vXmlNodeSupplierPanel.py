#Boa:FramePanel:vXmlNodeSupplierPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeSupplierPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060620
# CVS-ID:       $Id: vXmlNodeSupplierPanel.py,v 1.3 2006/08/29 10:06:30 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputTreeMLInternal
import vidarc.tool.input.vtInputTextML
import wx.lib.buttons

import sys

from vidarc.tool.xml.vtXmlNodePanel import *

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_VXMLNODESUPPLIERPANEL, wxID_VXMLNODESUPPLIERPANELLBLAREA, 
 wxID_VXMLNODESUPPLIERPANELLBLCAT, wxID_VXMLNODESUPPLIERPANELVIAREA, 
 wxID_VXMLNODESUPPLIERPANELVICAT, 
] = [wx.NewId() for _init_ctrls in range(5)]

class vXmlNodeSupplierPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=1
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)

    def _init_coll_bxsCat_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblCat, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viCat, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsCat, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsArea, 0, border=0, flag=wx.EXPAND)

    def _init_coll_bxsArea_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblArea, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viArea, 2, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=1, vgap=0)

        self.bxsArea = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsCat = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsArea_Items(self.bxsArea)
        self._init_coll_bxsCat_Items(self.bxsCat)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODESUPPLIERPANEL,
              name=u'vXmlNodeSupplierPanel', parent=prnt, pos=wx.Point(405,
              257), size=wx.Size(378, 331), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(370, 304))

        self.lblCat = wx.StaticText(id=wxID_VXMLNODESUPPLIERPANELLBLCAT,
              label=_(u'category'), name=u'lblCat', parent=self, pos=wx.Point(0,
              0), size=wx.Size(123, 30), style=wx.ALIGN_RIGHT)
        self.lblCat.SetMinSize(wx.Size(-1, -1))

        self.viCat = vidarc.tool.input.vtInputTextML.vtInputTextML(id=wxID_VXMLNODESUPPLIERPANELVICAT,
              name=u'viCat', parent=self, pos=wx.Point(127, 0),
              size=wx.Size(242, 30), style=0)

        self.lblArea = wx.StaticText(id=wxID_VXMLNODESUPPLIERPANELLBLAREA,
              label=_(u'area'), name=u'lblArea', parent=self, pos=wx.Point(0,
              30), size=wx.Size(123, 24), style=wx.ALIGN_RIGHT)
        self.lblArea.SetMinSize(wx.Size(-1, -1))

        self.viArea = vidarc.tool.input.vtInputTreeMLInternal.vtInputTreeMLInternal(id=wxID_VXMLNODESUPPLIERPANELVIAREA,
              name=u'viArea', parent=self, pos=wx.Point(127, 30),
              size=wx.Size(242, 24), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vSupplier')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,lWidgets=[])
        
        self.viCat.SetTagName('category')
        self.viArea.SetTagNames('mainArea','name')
        self.viArea.AddTreeCall('init','SetNodeInfos',['name'])
        self.viArea.AddTreeCall('init','SetGrouping',[],[('name','')])
        self.viArea.SetTagNames2Base(['globalsdata','globalsArea'])
        
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
    def Clear(self):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.Clear(self)
        
        # add code here
        self.viCat.Clear()
        self.viArea.Clear()
        
        self.ClrBlockDelayed()
    def SetNetDocs(self,d):
        # add code here
        if d.has_key('vGlobals'):
            dd=d['vGlobals']
            self.viArea.SetDocTree(dd['doc'],dd['bNet'])

    def SetDoc(self,doc,bNet=False):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        
        # add code here
        self.viCat.SetDoc(doc)
        self.viArea.SetDoc(doc,bNet,bExternal=True)
        
    def SetNode(self,node):
        try:
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            
            # add code here
            self.viCat.SetNode(self.node)
            self.viArea.SetNode(self.node)
            
        except:
            vtLog.vtLngTB(self.GetName())
        
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
                print node
            if node is None:
                return
            # add code here
            self.viCat.GetNode(node)
            self.viArea.GetNode(node)
            self.GetNodeFin(node)
        except:
            vtLog.vtLngTB(self.GetName())
    def Close(self):
        # add code here
        self.viCat.Close()
    def Cancel(self):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.Cancel(self)
        # add code here
        self.viCat.Clear()
        self.viArea.Close()
    def Lock(self,flag):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if flag:
            # add code here
            self.viCat.Enable(False)
            self.viArea.Enable(False)
        else:
            # add code here
            self.viCat.Enable(True)
            self.viArea.Enable(True)
