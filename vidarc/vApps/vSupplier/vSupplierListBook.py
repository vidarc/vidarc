#----------------------------------------------------------------------------
# Name:         vSupplierListBook.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060619
# CVS-ID:       $Id: vSupplierListBook.py,v 1.2 2007/07/30 20:38:49 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

import vidarc.vApps.vSupplier.images as images

#import vidarc.vApps.vSupplier.vXmlNodeSupplierPanel as vXmlNodeSupplierPanel
#import vidarc.tool.xml.vtXmlNodeAddrPanel as vtXmlNodeAddrPanel
import vidarc.vApps.vSupplier.vSupplierAttrPanel as vSupplierAttrPanel
import vidarc.ext.report.veRepDocPanel as veRepDocPanel
import vidarc.tool.xml.vtXmlNodeLogPanel as vtXmlNodeLogPanel
import types
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.xml.vtXmlDomConsumer as vtXmlDomConsumer


class vSupplierListBook(wx.Notebook,vtXmlDomConsumer.vtXmlDomConsumer):
    VERBOSE=0
    def _init_ctrls(self, prnt,id, pos, size, style, name):
        wx.Notebook.__init__(self, style=style, name=name, parent=prnt, pos=pos, id=id, size=size)

    def __init__(self, parent, id=wx.NewId(), pos=wx.DefaultPosition, 
                    size=wx.Size(100, 50), style=wx.TAB_TRAVERSAL, name=''):
        vtXmlDomConsumer.vtXmlDomConsumer.__init__(self)
        self._init_ctrls(parent,id, pos, size, style, name)
        
        self.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGED, self.OnPageChanged)
        self.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGING, self.OnPageChanging)
        
        self.doc=None
        self.sTagOld=None
        
        edit=[u'vSupplierAttr',u'vtXmlNodeLog']
        editName=[_(u'Attributes'),_(u'Log')]
        bmps=[vtArt.getBitmap(vtArt.Attr),vtArt.getBitmap(vtArt.Log)]
        self.lstTagName=[None,None]
        il = wx.ImageList(16, 16)
        for bmp in bmps:
            il.Add(bmp)
        self.AssignImageList(il)
        
        self.lstName=edit
        self.lstPanel=[]
        i=0
        for e in edit:
            s='%sPanel'%e
            f=getattr(eval('%sPanel'%e),'%sPanel'%e)
            panel=f(self,wx.NewId(),
                    pos=(0,0),size=(190,340),style=0,name=s)
            self.AddPage(panel,editName[i],False,i)
            self.lstPanel.append(panel)
            i=i+1
        self.lstPanel[0].SetFuncIs2Skip(self.__is2Skip__)
        #vtXmlFilter.EVT_THREAD_FILTER_ELEMENTS(panel,self.OnFilterElement)
    def __is2Skip__(self,sTag):
        if sTag in ['tag','name','description']:
            return True
        if sTag.find('__')==0:
            return True
        return False
    def Clear(self):
        for pn in self.lstPanel:
            pn.Clear()
        vtXmlDomConsumer.vtXmlDomConsumer.Clear(self)

    def Lock(self,flag):
        for pn in self.lstPanel:
            pn.Lock(flag)
    def SetModified(self,flag):
        for pn in self.lstPanel:
            pn.SetModified(flag)
    def __isModified__(self):
        for pn in self.lstPanel:
            if pn.GetModified():
                return True
        return False
    def GetModified(self):
        return self.__isModified__()
    def SetNetDocHuman(self,doc,bNet=False):
        try:
            idx=self.lstName.index('vtXmlNodeLog')
            self.lstPanel[idx].SetNetDocHuman(doc,bNet)
        except:
            pass
            #vtLog.vtLngTB(self.GetName())
    def SetRegNode(self,obj):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'tagname:%s'%(obj.GetTagName()),self)
        if obj.GetTagName()=='Doc':
            self.lstPanel[1].SetRegNode(obj)
    def SetNetDocs(self,d):
        for pn in self.lstPanel:
            if hasattr(pn,'SetNetDocs'):
                pn.SetNetDocs(d)
    def SetDoc(self,doc,bNet=False):
        vtXmlDomConsumer.vtXmlDomConsumer.SetDoc(self,doc)
        #if bNet:
        #    EVT_NET_XML_LOCK(doc,self.OnLock)
        #    EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        #self.doc=doc
        oDoc=doc.GetReg('Doc')
        self.lstPanel[1].SetRegNode(oDoc)
        for i in range(0,len(self.lstName)):
            self.lstPanel[i].SetDoc(doc,bNet)
        self.SetNetDocs(doc.GetNetDocs())
        
    def OnLock(self,evt):
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                self.Lock(False)
            else:
                self.Lock(True)
        evt.Skip()
    def OnUnLock(self,evt):
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['released']:
                self.Lock(False)
        evt.Skip()
        
    def SetNode(self,node):
        if self.__isModified__():
            # ask
            dlg=wx.MessageDialog(self,_(u'Do you to apply modified data?') ,
                        u'vSupplier',
                        wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            if dlg.ShowModal()==wx.ID_YES:
                self.GetNode()
                #wx.PostEvent(self,vgpDataChanged(self,self.node))
        self.SetModified(False)
        
        vtXmlDomConsumer.vtXmlDomConsumer.SetNode(self,node)
        if self.node is not None:
            sTag=self.doc.getTagName(self.node)
            if sTag!=self.sTagOld:
                obj=self.doc.GetReg(sTag)
                if obj is None:
                    vtLog.vtLngCurWX(vtLog.ERROR,'reg:%s not found'%(sTag),self)
                else:
                    self.lstPanel[0].SetRegNode(obj)
                    self.sTagOld=sTag
        i=0
        for pn in self.lstPanel:
            if self.lstTagName[i] is None:
                tmp=node
            elif node is None:
                tmp=node
            else:
                tmp=self.doc.getChild(node,self.lstTagName[i])
                if tmp is None:
                    try:
                        vtLog.vtLngCS(vtLog.WARN,'attribute node %s not found in;%s'%(self.lstTagName[i],repr(node)),
                                origin=self.GetName())
                        oReg=self.doc.GetRegisteredNode(self.lstTagName[i])
                        if oReg is not None:
                            o,tmp=self.doc.CreateNode(node,self.lstTagName[i])
                        else:
                            tmp=self.doc.createSubNode(node,self.lstTagName[i],False)
                            vtLog.vtLngCS(vtLog.INFO,'no registered node %s'%(self.lstTagName[i]),
                                    origin=self.GetName())
                            
                    except:
                        vtLog.vtLngTB(self.GetName())
            #vtLog.vtLngCurWX(vtLog.DEBUG,'tagname:%s id:%s;node:%s'%(self.doc.getTagName(tmp),self.doc.getKey(tmp),repr(node)),self)
            pn.SetNode(tmp)
            i+=1
        
    def GetNode(self,node=None):
        #sel = self.GetSelection()
        #self.lstPanel[sel].GetNode(node)
        for pn in self.lstPanel:
            pn.GetNode(node)
    def Apply(self):
        self.GetNode()
    def Cancel(self):
        self.SetModified(False)
        self.SetNode(self.node)
    def OnPageChanged(self, event):
        old = event.GetOldSelection()
        sel = event.GetSelection()
        if sel>=len(self.lstPanel):
            return
        event.Skip()

    def OnPageChanging(self, event):
        sel = event.GetSelection()
        event.Skip()
    def __getPanel__(self,name):
        try:
            idx=self.lstName.index(name)
            panel=self.lstPanel[idx]
            return panel
        except:
            return None
