#----------------------------------------------------------------------------
# Name:         vXmlNodeSupplier.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060619
# CVS-ID:       $Id: vXmlNodeSupplier.py,v 1.3 2006/08/29 10:06:30 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeTag import *
try:
    if vcCust.is2Import(__name__):
        from vidarc.vApps.vSupplier.vXmlNodeSupplierPanel import vXmlNodeSupplierPanel
        from vidarc.tool.xml.vtXmlNodeTagEditDialog import *
        from vidarc.tool.xml.vtXmlNodeTagAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeSupplier(vtXmlNodeTag):
    NODE_ATTRS=vtXmlNodeTag.NODE_ATTRS+[
            ('Category',None,'category',None),
        ]
    FUNCS_GET_SET_4_LST=vtXmlNodeTag.FUNCS_GET_SET_4_LST+['Category',]
    FUNCS_GET_4_TREE=vtXmlNodeTag.FUNCS_GET_4_TREE+['Category',]
    FMT_GET_4_TREE=vtXmlNodeTag.FMT_GET_4_TREE+[('Category','')]
    GRP_GET_4_TREE=None
    def __init__(self,tagName='supplier'):
        global _
        _=vtLgBase.assignPluginLang('vSupplier')
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'supplier')
    # ---------------------------------------------------------
    # specific
    def GetCategory(self,node):
        return self.GetML(node,'category')
    def SetCategory(self,node,val,lang=None):
        self.SetML(node,'category',val,lang)
    def GetArea(self,node):
        return self.GetML(node,'area')
    def SetArea(self,node,val,lang=None):
        self.SetML(node,'area',val,lang)
    # ---------------------------------------------------------
    # inheritance
    def Is2Create(self):
        return False
    def IsSkip(self):
        return False
    def Is2Add(self):
        return True
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02=IDAT8\x8d\xa5\x93\xdfK\x93q\x14\xc6?_\xdf\xe1|\xb7\xb1MS\'dZw\xae\
\xb5!^\xcd\xd1\x08*\x03\xf3>\xcc;\xff\x82D\x8bm\x8aQ\x10bQ\x17\x06u\x91\x08v\
)\xc14M\xa2\x1fV\x96L\n\x0c\x7fn\xb1II\x86V.\xcdW\x99\xe9\xf6\xee\xed\xd2\
\xfcER\xcf\xe5\xe1\x9c\xe7<\xe7\x9c\xe7\x08\x91!\xf1?\xc8\xd8o\xe2\x8d\xeb\
\xad\xdanq\xf17\x05\x01\xbfOs\xb9\\\xc8\xb2\xcc\xd0\xd0\x10\x16\xab\x95\xe6\
\xe6\xcb\xe2\xaf\x04\x8d\x01\xbff\xb7\xdb\xf1x<\xc8\xb2L:\x9d&\xa5\xaa\x84\
\xa7&\x19\x1e\x0ea4\x99\xf1\xfb\x03B\xb7\xbd\xf0bC\xbd\xe6t:\xf1z\xbd\x18\
\x0c\x06\x14Eay\xf9\'\x92\x94\x89^/\xe1\xb0\x1fej*L,\x1a\xdb\xa9\xa0\xa9\xd1\
\xa7\xd5]\xa8C\x9fe`~~\x0eUU\xd1\xe9t\xe8\xf52\xbf\xd6\xbe\xf3bp\x82\x94\x94\
$\xf4r\x90\xda\xdaZ\xceTT\x08I\x88\xcd=f[s\xaeLOO#\x84\xe0PQ\x11\xb2,\xb3\
\xbe\x9e$\x12\x1e\xa5\xbb\xfb3w\xfb\xb3Qps\xfc\xe0\x08s_\x16x\xfe\xea\xf5\
\xd5-#\xa4\x92\x1b(\x8aB0\x18$\x12\x89\xe0p8\xe8\xeb\xed\xe3\xdb\xe2,\xa7\
\xcf\xde\xe2\xe4\xda\x12\x1e\xa9\x9b\xc9~\x1bQ}\x18\xf8\xe3\x8c\xe7\xab\xab\
\xb5\x8dd\x12\xb3\xd9\x8c\xcb\xe5bEQx?:NG\xe7}\xb2\xd67\xc8\x8dv\x91;\xf2\
\x98\x9b\x9dafJ\x8c\x18\x0b\xa5M\x82\xd6\x96k\x9a\xdfWO[\xdbm\xf2\xf3\xf2\
\x08>\xec#\x9eX\xc1f\xd0@K\xf0i\xcc\xcc\xdb\xb9\x13|,1c+\x8b\xb1\xf0\xa1\x03\
w\x99s\x93 \x99R)(,\xe2Hq\x01\xd555\xb4\xf8\xeb0\x8d\xcf0\xf6 \x81\xa6\x1a\
\xc9s[(\xad\xfa\xc1R\xac\x13\xbd\xc8\xa4\xe4X9\xbe@\x93\x00\xd0\x01$VW\x99\
\x1d\xe8"\xdf{\x8eXt\x8c7\x8ff\xe9\r\x15cr/S`\xb3039L\xe4p>\x83\xa1\t\xc16\
\xe8\x00\x16\xe2q\xee<\x89S\xf5u\x91\xf2S\x95<=\x10\xc1\xe0z\x875\xadRZY\xc5\
\xbd\xf6v1:\x11\xde\xd5p[|\xe0\xbf\xd4\xa0\x99Lf\x06\x06\x9e\x01\x19Xrr\xe9\
\xe9\t\xee\xe8\xba\'\xc1\xbf`\xdf\xdf\xb8\x17~\x03\xec\x0f\xd4\\\xa6\xc8\xea\
(\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return vtXmlNodeTagEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return vtXmlNodeTagAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeSupplierPanel
        else:
            return None

