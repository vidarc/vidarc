#----------------------------------------------------------------------------
# Name:         vXmlSupplier.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060619
# CVS-ID:       $Id: vXmlSupplier.py,v 1.3 2007/07/30 20:38:49 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vidarc.tool.xml.vtXmlFilterType import vtXmlFilterType

from vidarc.vApps.vSupplier.vXmlNodeSupplier import vXmlNodeSupplier
from vidarc.vApps.vSupplier.vXmlNodeSupplierProvide import vXmlNodeSupplierProvide
from vidarc.vApps.vSupplier.vXmlNodeSupplierContact import vXmlNodeSupplierContact
from vidarc.vApps.vSupplier.vXmlNodeSupplierAddr import vXmlNodeSupplierAddr

from vidarc.tool.xml.vtXmlNodeLog import vtXmlNodeLog
from vidarc.tool.xml.vtXmlNodeCfg import vtXmlNodeCfg
from vidarc.tool.xml.vtXmlNodeAttrCfgML import vtXmlNodeAttrCfgML

from vidarc.vApps.vSupplier.vXmlNodeSupplierRoot import vXmlNodeSupplierRoot

def getPluginData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02\x0fIDAT8\x8d\x8d\x93OH\x93q\x1c\xc6?\xef\xbf\xbd\x9b\x9b\x11\x18*b\
\x16i\xd4\x08\x17;\tZ\x03\xfb#]2\xa2?#\x92\xbat\xaa{z\xa8.uX;x(\x8a\x08\x0b;\
D\x88\x15\x08\xd5\xa1<H\x81\x8b\x0e\x8e\xd7\x95\xcd%:\xa6nn\xb5\xa6Y\xe4\xde\
\xe6\xd6\xa16\xa7\xbe\x03\x9f\xdb\xef\xe1\xe1\xfb\xe7\xf7}\x1eA\x10%\xd6\xa2\
\xd1s2\xb7\x8e\x04\x02]O\x85\xb5\x9c\xb8Q\xa1\x11\x07 \xe4\'(\xd5\xb5\x14\
\xf2\x05\r\'\xc8c\xda\xfb\x96}\xdaw\xe4\x07>\x0e\xc5j\x0c5\xb2\x11i\x1d\x98E\
Y\x8cp\xfd\xc6\x19\xda]v\xee<\xf6\xd1\xfdl\x90xd\x82\xd6\x83mD\xf6\x9b\x0bZ\
\xb1x\xf4\xd4\xadwd\x1f\xfa\xe8\xbd\xd4\xcc\x8b{\x9d\xd4m*\xe3\xe2m?\xd9\n;\
\xbd\xddW\x89\x0e\xdf\xa5"=\xc5\xec\xcd\xc1\xc2\xda\x82\xc3\xeb\xce\x01\x8cw\
\xf6\xd1s\xad\x83\x8e\xb3\x87y=4\xc1\x87\xc9y\x16\xe4j\xdc-\xdb\xd8\xb5E\xa0\
\\\xf9\xc9\xd8\xa7q\x923a\\\x9d\xcfqx\xdd\xabW\xd8\xbb\xbb\x96\xd6=U\xdc\x7f\
4Dp\xfa\x1b\xc7N\x9c\xa6y\xbb\x84\xb2\xbc\xc8\xe7\xc0G\xb4\xd11b\xb1(\xe5V3\
\x16\xd3\xca\xe9\x0b\x05\xf4L\x8ed*E\xf8\x87\xc8\x0es\x12\xbb\x10b>"0\xf0\
\xea\rzz\x89\xe5\xcc\x1f\xac6\x1b\x8a,R|\xaeB\x01\x8b*\xa3\xa8\x16\x9c5i\xe4\
t5\xd1\xf8WF\xfc\x1aK\xbf\x7f!I\n\x962\x0b&IF\x95\xb2\xe8\xfa\xf2\xca\'\xc2\
\xbf\x9b\xce\xc5\xe6\x08E\x128\xb7\x96\xd1\xd8PK:\x93C1\xa9H\xa2\xc8f\x9b\
\x8a \x88\xbc|\x1f\xe4J\x7f\x90\xca\xaa\xca\x82\x0fV\x19i\xa9\xc7O\x9d\xba\
\xc0\xf9\xf6\x16\x0e8\xeb\xd14\x8d\xc9\x99\x04#_\xe2<\x19\x8e\xd1\xd6~\x9c\
\xa9f\x13\xf9\xa6\xeb|`\xbe\xe0$\x01\x9c\xeb\xea\xe7hS=\xba\x9e&\x10\x8a\xd0\
\xd0t\x84\x9d\x1e\x17S\x1b5R\xa3\xe7\x14a`\xf4r\x1f\x0e\xaf\x9b\x94\x91\xe8?\
\x04\xa34\xe6W*~\x97\n\x93a\x16\x8c\x82U*l\x7f\x01"\x11\xc2\xba\xab\xb8\xa6\
\xc8\x00\x00\x00\x00IEND\xaeB`\x82' 

class vXmlSupplier(vtXmlDomReg):
    VERBOSE=1
    TAGNAME_REFERENCE='tag'
    TAGNAME_ROOT='suppliersroot'
    APPL_REF=['vHum','vGlobals']
    def __init__(self,appl='vSupplier',attr='id',skip=[],synch=False,verbose=0,
                    audit_trail=True):
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        try:
            vtXmlDomReg.__init__(self,appl=appl,attr=attr,skip=skip,synch=synch,
                            verbose=verbose,audit_trail=audit_trail)
            oRoot=vXmlNodeSupplierRoot()
            self.RegisterNode(oRoot,False)
            
            oSupplier=vXmlNodeSupplier()
            oProvide=vXmlNodeSupplierProvide()
            oContacts=vXmlNodeSupplierContact()
            oAddrs=vXmlNodeSupplierAddr()
            oLog=vtXmlNodeLog()
            oCfg=vtXmlNodeCfg()
            
            #oArea=vXmlNodeArea()
            self.RegisterNode(oRoot,True)
            self.RegisterNode(oSupplier,False)
            self.RegisterNode(oProvide,False)
            self.RegisterNode(oContacts,False)
            self.RegisterNode(oAddrs,False)
            self.RegisterNode(oLog,False)
            self.RegisterNode(oCfg,True)
            self.RegisterNode(vtXmlNodeAttrCfgML(),False)
            
            self.LinkRegisteredNode(oRoot,oSupplier)
            self.LinkRegisteredNode(oSupplier,oProvide)
            self.LinkRegisteredNode(oSupplier,oContacts)
            self.LinkRegisteredNode(oSupplier,oAddrs)
            self.LinkRegisteredNode(oSupplier,oLog)
            #self.LinkRegisteredNode(oCfg,oArea)
            #self.LinkRegisteredNode(oArea,oArea)
            
        except:
            vtLog.vtLngTB(self.appl)
        self.SetDftLanguages()
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'suppliers')
    #def New(self,revision='1.0',root='suppliersroot',bConnection=False):
    #    vtXmlDomReg.New(self,revision,root)
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elem=self.createSubNode(self.getRoot(),'suppliers')
        if bConnection==False:
            self.CreateReq()
        #self.createSubNodeTextAttr(elem,'cfg','','id','')
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        #self.AlignDoc()
    def SetNetDocs(self,d):
        vtXmlDomReg.SetNetDocs(self,d)
        self.SetInfos2Get4RegisteredNodes()
