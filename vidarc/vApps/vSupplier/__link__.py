#----------------------------------------------------------------------------
# Name:         __link__.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060619
# CVS-ID:       $Id: __link__.py,v 1.1 2006/07/17 11:25:01 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

def LinkNodes(doc,lLink):
    for sBase,lSub in lLink:
        oBase=doc.GetReg(sBase)
        if oBase is None:
            continue
        for sSub in lSub:
            oSub=doc.GetReg(sSub)
            if oSub is not None:
                doc.LinkRegisteredNode(oBase,oSub)

