#----------------------------------------------------------------------------
# Name:         __register__.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060619
# CVS-ID:       $Id: __register__.py,v 1.1 2006/07/17 11:25:01 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust

from vidarc.vApps.vSupplier.vNodeDocuments import vNodeDocuments
from vidarc.vApps.vSupplier.vNodeDoc import vNodeDoc
from vidarc.tool.xml.vtXmlNodeAttrCfgML import vtXmlNodeAttrCfgML

import vidarc.vApps.vSupplier.admin.__register__
import vidarc.vApps.vSupplier.HW.__register__
import vidarc.vApps.vSupplier.instance.__register__
import vidarc.vApps.vSupplier.research.__register__
import vidarc.vApps.vSupplier.S88.__register__
import vidarc.vApps.vSupplier.SW.__register__
import vidarc.vApps.vSupplier.type.__register__

if vcCust.is2Import(__name__,'__REG_ACCOUNT__',True):
    from vidarc.vApps.vSupplier.vNodeAccount import vNodeAccount
    from vidarc.vApps.vSupplier.vNodeAccountTask import vNodeAccountTask
    from vidarc.vApps.vSupplier.vNodeAccountTaskSub import vNodeAccountTaskSub
def __regAccount__(doc):
    oAcc=vNodeAccount()
    oAccTask=vNodeAccountTask()
    oAccTaskSub=vNodeAccountTaskSub()
    doc.RegisterNode(oAcc,False)
    doc.RegisterNode(oAccTask,False)
    doc.RegisterNode(oAccTaskSub,False)
    doc.LinkRegisteredNode(oAcc,oAccTask)
    doc.LinkRegisteredNode(oAccTask,oAccTaskSub)
    
def RegisterNodes(doc):
    try:
        oAttrML=vtXmlNodeAttrCfgML()
        oDocs=vNodeDocuments()
        oDoc=vNodeDoc()
        #oDoc=doc.GetReg('Doc')
        doc.RegisterNode(oAttrML,False)
        doc.RegisterNode(oDocs,False)
        doc.RegisterNode(oDoc,False)
        doc.LinkRegisteredNode(oDocs,oDoc)
        if vcCust.is2Import(__name__,'__REG_ACCOUNT__',True):
            __regAccount__(doc)
        
        vidarc.vApps.vSupplier.HW.__register__.RegisterNodes(doc)
        vidarc.vApps.vSupplier.S88.__register__.RegisterNodes(doc)
        vidarc.vApps.vSupplier.SW.__register__.RegisterNodes(doc)
        
        vidarc.vApps.vSupplier.admin.__register__.RegisterNodes(doc)
        vidarc.vApps.vSupplier.instance.__register__.RegisterNodes(doc)
        vidarc.vApps.vSupplier.type.__register__.RegisterNodes(doc)
        vidarc.vApps.vSupplier.research.__register__.RegisterNodes(doc)
    except:
        vtLog.vtLngTB(__name__)
