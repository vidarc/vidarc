#----------------------------------------------------------------------------
# Name:         vXmlSupplierInputTree.py
# Purpose:      input widget for supplier xml
#               text and popup with tree
# Author:       Walter Obweger
#
# Created:      20060620
# CVS-ID:       $Id: vXmlSupplierInputTree.py,v 1.1 2006/07/17 11:25:01 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from vidarc.tool.input.vtInputTree import vtInputTree
import vidarc.tool.log.vtLog as vtLog
from vidarc.vApps.vSupplier.vXmlSupplierTree import vXmlSupplierTree

class vXmlSupplierInputTree(vtInputTree):
    def __init__(self,*_args,**_kwargs):
        apply(vtInputTree.__init__,(self,) + _args,_kwargs)
        self.tagName='supplier'
        self.tagNameInt='tag'
        self.trClass=self.__createTree__
        self.trShow=self.__showNode__
        self.SetAppl('vSupplier')
    def __createTree__(*args,**kwargs):
        tr=vXmlSupplierTree(**kwargs)
        return tr
    def __showNode__(self,doc,node,lang):
        return doc.getNodeText(node,self.tagNameInt)
