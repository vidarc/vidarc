#Boa:Frame:vSupplierMainFrame
#----------------------------------------------------------------------------
# Name:         vSupplierMainFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060619
# CVS-ID:       $Id: vSupplierMainFrame.py,v 1.4 2007/07/30 20:38:49 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.xml.vtXmlNodeRegListBook
import vidarc.tool.xml.vtXmlNodeTagCmdPanel
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons
import getpass,time

import vtLgBase
import vidarc.vApps.common.vAboutDialog as vAboutDialog
import vidarc.vApps.common.vSystem as vSystem
from vidarc.vApps.common.vMainFrame import vMainFrame
from vidarc.vApps.common.vStatusBarMessageLine import vStatusBarMessageLine
from vidarc.tool.log.vtLogFileViewerFrame import *
from vidarc.vApps.vSupplier.vXmlSupplierTree  import *
from vidarc.vApps.vSupplier.vNetSupplier import *
from vidarc.vApps.vSupplier.vSupplierListBook import *
from vidarc.tool.xml.vtXmlNodeRegListBook import *
from vidarc.tool.net.vtNetSecXmlGuiMaster import *
from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer
import vidarc.tool.InOut.fnUtil as fnUtil
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt

from vidarc.vApps.vHum.vNetHum import *
from vidarc.vApps.vGlobals.vNetGlobals import *

import vidarc.vApps.vSupplier.images as images

VERBOSE=0

def create(parent):
    return vSupplierMainFrame(parent)

[wxID_VGFPRJMAIN, wxID_VGFPRJMAINSPWMAIN, 
] = map(lambda _init_ctrls: wx.NewId(), range(2))

[wxID_VSUPPLIERMAINFRAME, wxID_VSUPPLIERMAINFRAMENBDATA, 
 wxID_VSUPPLIERMAINFRAMEPNDATA, wxID_VSUPPLIERMAINFRAMEPNTOP, 
 wxID_VSUPPLIERMAINFRAMESBSTATUS, wxID_VSUPPLIERMAINFRAMESLWNAV, 
 wxID_VSUPPLIERMAINFRAMESLWTOP, wxID_VSUPPLIERMAINFRAMEVITAG, 
] = [wx.NewId() for _init_ctrls in range(8)]

STATUS_TEXT_POS=2
STATUS_LOG_POS=3
STATUS_CLK_POS=4

def getPluginImage():
    return images.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon

[wxID_VGFPRJTIMERMAINTOOLBAR1TOOLS0] = [wx.NewId() for _init_coll_toolBar1_Tools in range(1)]

[wxID_VSUPPLIERMAINFRAMEMNHELPMN_HELP_ABOUT, 
 wxID_VSUPPLIERMAINFRAMEMNHELPMN_HELP_HELP, 
 wxID_VSUPPLIERMAINFRAMEMNHELPMN_HELP_LOG, 
] = [wx.NewId() for _init_coll_mnHelp_Items in range(3)]

[wxID_VSUPPLIERMAINFRAMEMNFILEITEM_EXIT] = [wx.NewId() for _init_coll_mnFile_Items in range(1)]

class vSupplierMainFrame(wx.Frame,vMainFrame,vtXmlDomConsumer,vStatusBarMessageLine):
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_bxTop_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viTag, 1, border=4, flag=wx.ALL | wx.EXPAND)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.nbData, 1, border=4, flag=wx.ALL | wx.EXPAND)

    def _init_coll_mnFile_Items(self, parent):
        # generated method, don't edit

        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VSUPPLIERMAINFRAMEMNFILEITEM_EXIT,
              kind=wx.ITEM_NORMAL, text=_(u'E&xit\tAlt+X'))
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_exitMenu,
              id=wxID_VSUPPLIERMAINFRAMEMNFILEITEM_EXIT)

    def _init_coll_mbMain_Menus(self, parent):
        # generated method, don't edit

        parent.Append(menu=self.mnFile, title=_(u'&File'))
        parent.Append(menu=self.mnView, title=_(u'View'))
        parent.Append(menu=self.mnAnalyse, title=_(u'Analyse'))
        parent.Append(menu=self.mnTools, title=_(u'Tools'))
        parent.Append(menu=self.mnHelp, title=u'?')

    def _init_coll_mnHelp_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_VSUPPLIERMAINFRAMEMNHELPMN_HELP_HELP,
              kind=wx.ITEM_NORMAL, text=u'?\tF1')
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VSUPPLIERMAINFRAMEMNHELPMN_HELP_ABOUT,
              kind=wx.ITEM_NORMAL, text=_(u'About'))
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VSUPPLIERMAINFRAMEMNHELPMN_HELP_LOG,
              kind=wx.ITEM_NORMAL, text=_(u'Log'))
        self.Bind(wx.EVT_MENU, self.OnMnHelpMn_help_helpMenu,
              id=wxID_VSUPPLIERMAINFRAMEMNHELPMN_HELP_HELP)
        self.Bind(wx.EVT_MENU, self.OnMnHelpMn_help_aboutMenu,
              id=wxID_VSUPPLIERMAINFRAMEMNHELPMN_HELP_ABOUT)
        self.Bind(wx.EVT_MENU, self.OnMnHelpMn_help_logMenu,
              id=wxID_VSUPPLIERMAINFRAMEMNHELPMN_HELP_LOG)

    def _init_coll_sbStatus_Fields(self, parent):
        # generated method, don't edit
        parent.SetFieldsCount(5)

        parent.SetStatusText(number=0, text=u'')
        parent.SetStatusText(number=1, text=u'')
        parent.SetStatusText(number=2, text=u'')
        parent.SetStatusText(number=3, text=u'')
        parent.SetStatusText(number=4, text=u'')

        parent.SetStatusWidths([20, 120, -1, 100, 200])

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=1, vgap=0)

        self.bxTop = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_bxTop_Items(self.bxTop)

        self.pnData.SetSizer(self.fgsData)
        self.pnTop.SetSizer(self.bxTop)

    def _init_utils(self):
        # generated method, don't edit
        self.mbMain = wx.MenuBar()

        self.mnFile = wx.Menu(title=u'')

        self.mnAnalyse = wx.Menu(title=u'')

        self.mnTools = wx.Menu(title=u'')

        self.mnView = wx.Menu(title=u'')

        self.mnHelp = wx.Menu(title=u'')

        self._init_coll_mbMain_Menus(self.mbMain)
        self._init_coll_mnFile_Items(self.mnFile)
        self._init_coll_mnHelp_Items(self.mnHelp)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VSUPPLIERMAINFRAME,
              name=u'vSupplierMainFrame', parent=prnt, pos=wx.Point(115, 10),
              size=wx.Size(816, 555), style=wx.DEFAULT_FRAME_STYLE,
              title=u'VIDARC Supplier')
        self._init_utils()
        self.SetClientSize(wx.Size(808, 528))
        self.SetMenuBar(self.mbMain)
        self.Bind(wx.EVT_CLOSE, self.OnFrameClose)
        self.Bind(wx.EVT_ACTIVATE, self.OnFrameActivate)
        self.Bind(wx.EVT_SIZE, self.OnFrameSize)
        self.Bind(wx.EVT_MOVE, self.OnFrameMove)

        self.sbStatus = wx.StatusBar(id=wxID_VSUPPLIERMAINFRAMESBSTATUS,
              name=u'sbStatus', parent=self, style=0)
        self._init_coll_sbStatus_Fields(self.sbStatus)
        self.SetStatusBar(self.sbStatus)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VSUPPLIERMAINFRAMESLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 0), size=wx.Size(200,
              489), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetDefaultSize(wx.Size(200, 1000))
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetAutoLayout(True)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetLabel(_(u'navigation'))
        self.slwNav.SetToolTipString(_(u'navigation'))
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VSUPPLIERMAINFRAMESLWNAV)

        self.slwTop = wx.SashLayoutWindow(id=wxID_VSUPPLIERMAINFRAMESLWTOP,
              name=u'slwTop', parent=self, pos=wx.Point(200, 0),
              size=wx.Size(608, 200), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwTop.SetDefaultSize(wx.Size(1000, 200))
        self.slwTop.SetAlignment(wx.LAYOUT_TOP)
        self.slwTop.SetAutoLayout(True)
        self.slwTop.SetSashVisible(wx.SASH_BOTTOM, True)
        self.slwTop.SetOrientation(wx.LAYOUT_HORIZONTAL)
        self.slwTop.SetLabel(_(u'information'))
        self.slwTop.SetToolTipString(_(u'information'))
        self.slwTop.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwTopSashDragged,
              id=wxID_VSUPPLIERMAINFRAMESLWTOP)

        self.pnData = wx.Panel(id=wxID_VSUPPLIERMAINFRAMEPNDATA, name=u'pnData',
              parent=self, pos=wx.Point(208, 208), size=wx.Size(592, 272),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)

        self.nbData = vidarc.tool.xml.vtXmlNodeRegListBook.vtXmlNodeRegListBook(id=wxID_VSUPPLIERMAINFRAMENBDATA,
              name=u'nbData', parent=self.pnData, pos=wx.Point(4, 4),
              size=wx.Size(580, 260), style=0)

        self.pnTop = wx.Panel(id=wxID_VSUPPLIERMAINFRAMEPNTOP, name=u'pnTop',
              parent=self.slwTop, pos=wx.Point(0, 0), size=wx.Size(600, 192),
              style=wx.SUNKEN_BORDER)

        self.viTag = vidarc.tool.xml.vtXmlNodeTagCmdPanel.vtXmlNodeTagCmdPanel(id=wxID_VSUPPLIERMAINFRAMEVITAG,
              name=u'viTag', parent=self.pnTop, pos=wx.Point(4, 4),
              size=wx.Size(588, 180), style=wx.TAB_TRAVERSAL)
        self.viTag.Bind(vidarc.tool.xml.vtXmlNodeTagCmdPanel.vEVT_TOOL_XML_TAG_CANCEL,
              self.OnViTagToolXmlTagCancel, id=wxID_VSUPPLIERMAINFRAMEVITAG)
        self.viTag.Bind(vidarc.tool.xml.vtXmlNodeTagCmdPanel.vEVT_TOOL_XML_TAG_APPLY,
              self.OnViTagToolXmlTagApply, id=wxID_VSUPPLIERMAINFRAMEVITAG)

        self._init_sizers()

    def __init__(self, parent):
        vtXmlDomConsumer.__init__(self)
        vMainFrame.__init__(self)
        self._init_ctrls(parent)
        vStatusBarMessageLine.__init__(self,self.sbStatus,STATUS_TEXT_POS,-1)#5000)
        self.sbStatus.Bind(wx.EVT_RIGHT_UP, self.OnSbMainRightUp)
        self.zMsgLast=time.clock()
        self.bActivated=False
        
        self.nodeCurStarted=None
        self.nodeCurStartedTreeItem=None
        self.nodeLock=None
        
        self.baseDoc=None
        self.baseSettings=None
        
        self.xdCfg=vtXmlDom(appl='cfg')
        
        icon = getApplicationIcon()
        self.SetIcon(icon)
        
        appls=['vSupplier','vHum','vGlobals']
        
        rect = self.sbStatus.GetFieldRect(0)
        self.netMaster=vtNetSecXmlGuiMaster(self.sbStatus,'Master',pos=(rect.x+2, rect.y+2),
                    size=(rect.width-4, rect.height-4),verbose=1)
        
        self.netSupplier=self.netMaster.AddNetControl(vNetSupplier,'vSupplier',synch=True,verbose=0)
        self.netHum=self.netMaster.AddNetControl(vNetHum,'vHum',synch=True,verbose=0)
        self.netGlb=self.netMaster.AddNetControl(vNetGlobals,'vGlobals',synch=True,verbose=0)
        self.netMaster.AddNetControl(None,None,verbose=0)
        #print vtLgBase.getApplLang()
        
        import vtLgBase
        self.netSupplier.SetLang(vtLgBase.getApplLang())
        
        self.netMaster.SetCfg(self.xdCfg,[],appls,'vSupplierAppl')
        EVT_NET_XML_OPEN_OK(self.netSupplier,self.OnOpenOk)
        
        EVT_NET_XML_SYNCH_PROC(self.netSupplier,self.OnSynchProc)
        EVT_NET_XML_SYNCH_FINISHED(self.netSupplier,self.OnSynchFinished)
        EVT_NET_XML_SYNCH_ABORTED(self.netSupplier,self.OnSynchAborted)
        
        EVT_NET_XML_GOT_CONTENT(self.netSupplier,self.OnGotContent)
        
        self.bAutoConnect=True
        
        self.vgpTree=vXmlSupplierTree(self.slwNav,wx.NewId(),
                pos=(0,0),size=(198,529),style=0,name="trSupplier",
                master=True,verbose=0)
        #self.vgpTree.SetConstraints(
        #    anchors.LayoutAnchors(self.vgpTree, True, True, True, True)
        #    )
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        self.vgpTree.SetDoc(self.netSupplier,True)
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.vgpTree,self.OnAddElementsFin)
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.vgpTree,self.OnAddElementsProgress)
        self.vgpTree.EnableImportMenu(True)
        self.vgpTree.EnableExportMenu(True)
        
        #self.nbData=vSupplierListBook(self.pnData,wx.NewId(),
        #        pos=(4,4),size=(470, 188),style=0,name="nbSupplier")
        #self.fgsData.AddWindow(self.nbData, 0, border=8, flag=wx.EXPAND | wx.ALL)

        #self.nbData.SetConstraints(
        #    anchors.LayoutAnchors(self.nbData, True, True, True, True)
        #    )
        #self.nbData.SetDoc(self.netSupplier,True)
        #self.nbData=vtXmlNodeRegListBook(self.pnData,wx.NewId(),
        #        pos=(8,8),size=(576, 256),style=0,name="nbData")
        #oInst=self.netSupplier.GetRegisteredNode('instance')
        #self.nbData.SetDoc(self.netSupplier,True)
        #self.nbData.Show(False)
        #self.nbData.SetConstraints(
        #    LayoutAnchors(self.nbData, True, True, True, True)
        #    )
        #self.nbResHum.SetDoc(self.netRessource,True)
        
        self.nbData.SetDoc(self.netSupplier,True)
        oSupplier=self.netSupplier.GetReg('supplier')
        #self.nbData.SetRegNode(oSupplier)
        
        self.viTag.AddWidgetDependent(self.nbData,oSupplier)
        #self.viTag.AddWidgetDependent(self.nbData,oDoc)
        self.viTag.SetDoc(self.netSupplier,True)
        self.viTag.SetRegNode(oSupplier,bIncludeBase=True)
        
        self.iLogOldSum=0
        self.zLogUnChanged=0
        # setup statusbar
        self.timer = wx.PyTimer(self.Notify)
        self.timer.Start(1000)
        self.Notify()
        
        rect = self.sbStatus.GetFieldRect(1)
        self.gProcess = wx.Gauge(
                    self.sbStatus, -1, 50, (rect.x+2, rect.y+2), 
                    (rect.width-4, rect.height-4), 
                    wx.GA_HORIZONTAL|wx.GA_SMOOTH
                    )
        self.gProcess.SetRange(100)
        self.gProcess.SetValue(0)
        
        vMainFrame.AddMenus(self , self , self.vgpTree , self.netMaster,
                self.mnFile , self.mnView , self.mnTools)
        self.Layout()
        self.Fit()
        self.OpenCfgFile()
        #self.slwNav.Layout()
        #self.slwNav.Refresh()
        self.__makeTitle__()
    def Notify(self):
        try:
            t = time.localtime(time.time())
            st = time.strftime(_("%b-%d-%Y   %H:%M:%S wk:%U"), t)
            self.SetStatusText(st, STATUS_CLK_POS)
            self.__makeTitle__()
            
            tup=self.GetPrintMsgTup()
            if tup is not None:
                bIsShown=self.IsShownPopupStatusBar()
                self.zMsgLast=time.clock()
                while tup is not None:
                    if bIsShown:
                        self.AddMsg2PopupStatusBar(tup,0)
                    self.sb.SetStatusText(tup[0], self.iNum)
                    self.rbMsg.put(tup)
                    tup=self.GetPrintMsgTup()
            else:
                if (time.clock()-self.zMsgLast)>5:
                    self.sb.SetStatusText('', self.iNum)
            
            s=vtLog.vtLngGetNumDiffOrderedStr()
            self.SetStatusText(s, STATUS_LOG_POS)
            
            i=vtLog.vtLngGetNumDiffSum()
            if i>self.iLogOldSum:
                self.iLogOldSum=i
                self.zLogUnChanged=0
            else:
                self.zLogUnChanged+=1
                if self.zLogUnChanged>30:
                    self.iLogOldSum=i
                    self.zLogUnChanged=0
                    vtLog.vtLngNumStore()
        except:
            pass
        
    def __makeTitle__(self):
        sOldTitle=self.GetTitle()
        s=_("VIDARC Supplier")
        fn=self.netSupplier.GetFN()
        if fn is None:
            s=s+_(" (undef*)")
        else:
            s=s+" ("+fn
            if self.netMaster.IsModified():
                s=s+"*)"
            else:
                s=s+")"
        if s!=sOldTitle:
            self.SetTitle(s)
    def __setModified__(self,state):
        self.__makeTitle__()
    def OnAddElementsProgress(self,evt):
        if self.gProcess is not None:
            self.gProcess.SetValue(evt.GetValue())
            if evt.GetCount()>=0:
                self.gProcess.SetRange(evt.GetCount())
        evt.Skip()
    def OnAddElementsFin(self,evt):
        if self.gProcess is not None:
            self.gProcess.SetValue(0)
        self.PrintMsg(_('generated.'))
        evt.Skip()
    def Clear(self):
        if self.node is not None:
            self.netSupplier.endEdit(self.node)
        vtXmlDomConsumer.vtXmlDomConsumer.Clear(self)
    def OnTreeItemSel(self,event):
        node=event.GetTreeNodeData()
        self.viTag.SetNode(node)
        
        #if self.node is not None:
        #    self.netSupplier.endEdit(self.node)
        #if event.GetTreeNodeData() is not None:
        #    self.netSupplier.startEdit(node)
        #    self.nbData.SetNode(node)
        #else:
            # grouping tree item selected
        #    self.nbData.SetNode(None)
        #    pass
        event.Skip()
    def OnOpenStart(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('file opened ...'))
        evt.Skip()
    def OnOpenOk(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('file opened and parsed.'))
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnOpenFault(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('file open fault.'))
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnSynchStart(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('synch started ...')+' %07d - %07d'%(0,evt.GetCount()))
        evt.Skip()
    def OnSynchProc(self,evt):
        #if VERBOSE:
        #    vtLog.vtLogCallDepth(self,'')
        self.gProcess.SetRange(evt.GetCount())
        self.gProcess.SetValue(evt.GetAct())
        self.PrintMsg(_('synch started ...')+' %07d - %07d'%(evt.GetAct(),evt.GetCount()))
        evt.Skip()
    def OnSynchFinished(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('synch finished.'))
        self.__getXmlBaseNodes__()
        self.gProcess.SetValue(0)
        evt.Skip()
    def OnSynchAborted(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('synch aborted.'))
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnConnect(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.__makeTitle__()
        self.PrintMsg(_('connection established ... please wait'))
        evt.Skip()
    def OnDisConnect(self,evt):
        if VERBOSE:
            vtLog.vtLogCallDepth(self,'')
        self.PrintMsg(_('connection lost.'))
        evt.Skip()
    def OnLogin(self,evt):
        self.PrintMsg(_('login ... '))
        evt.Skip()
    def OnLoggedin(self,evt):
        self.PrintMsg(_('get content ... '))
        evt.Skip()
    def OnSetupChanged(self,evt):
        if self.netSupplier.ReConnect2Srv()==0:
            #self.netHum.ReConnect2Srv()
            pass
    def OnGotContent(self,evt):
        self.PrintMsg(_('content got. generate ...'))
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnClosed(self,evt):
        self.PrintMsg(_('connection closed. '))
        evt.Skip()
    def OnLock(self,evt):
        if evt.GetResponse()=='ok':
            try:
                self.nodeLock=self.netSupplier.getNodeById(evt.GetID())
            except:
                self.nodeLock=None
        else:
            self.nodeLock=None
        evt.Skip()
    def OnUnLock(self,evt):
        if evt.GetResponse()=='ok':
            self.nodeLock=None
        evt.Skip()
    def OnConnect(self,evt):
        self.__makeTitle__()
        evt.Skip()
    def OnChanged(self,evt):
        self.__setModified__(True)
        evt.Skip()
    def OpenFile(self,fn):
        if self.netSupplier.ClearAutoFN()>0:
            self.netSupplier.Open(fn)
    def OpenCfgFile(self,fn=None):
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                self.netMaster.SetCfgNode()
                self.__makeTitle__()
                self.__setCfg__()
                return
        self.xdCfg.New(root='config')
        if fn is not None:
            self.xdCfg.SetFN(fn)
            self.netMaster.SetCfgNode()
        self.__setCfg__()
    def SaveFile(self,fn=None):
        vtLog.vtLngCS(vtLog.INFO,'file:%s'%repr(fn))
        sCurFN=self.netSupplier.GetFN()
        if (sCurFN is None) and (fn is None):
            self.OnMnFileItem_saveasMenu(None)
            #self.__setModified__(False)
            return
        self.PrintMsg(_('save file ... '))
        self.netSupplier.Save(fn)
        self.PrintMsg(_('save file finished.'))
    def CreateNew(self):
        self.netSupplier.New()
        
    def OnTreeItemAdd(self,event):
        #print "added",event.GetVgpTree(),event.GetTreeNode(),event.GetTreeNodeData()
        #vtXmlDomAlign(self.dom)
        self.__setModified__(True)
        event.Skip()
    def __getSettings__(self):
        return
        self.xmlLastPrj=None
        self.xmlLastTask=None
        self.xmlLastSubTask=None
        self.xmlLastDesc=None
        self.xmlLastLoc=None
        self.xmlLastPerson=None
        
        self.xmlPrjFN=self.netSupplier.getNodeText(self.baseSettings,'projectfn')
        self.xmlHumanFN=self.netSupplier.getNodeText(self.baseSettings,'humanfn')
        self.xmlLocationFN=self.netSupplier.getNodeText(self.baseSettings,'locationfn')
        self.xmlBaseTmplDN=self.netSupplier.getNodeText(self.baseSettings,'templatedn')
        self.xmlProjectDN=self.netSupplier.getNodeText(self.baseSettings,'projectdn')
        # get tree view settings
        sTreeViewGrp=self.netSupplier.getNodeText(self.baseSettings,'treeviewgrp')
        mnItems=self.mnViewTree.GetMenuItems()
        
        self.vglbDocGrpInfo.SetTemplateBaseDir(self.xmlBaseTmplDN)
        if sTreeViewGrp=='normal':
            mnItems[0].Check(True)
            self.__EnsureOneCheckMenuItem__(mnItems,0,0,2)
            self.vgpTree.SetNormalGrouping()
        elif sTreeViewGrp=='iddocgrp':
            mnItems[1].Check(True)
            self.__EnsureOneCheckMenuItem__(mnItems,1,0,2)
            self.vgpTree.SetIdGrouping()
        else:
            mnItems[0].Check(True)
            self.__EnsureOneCheckMenuItem__(mnItems,0,0,2)
            #self.vgpTree.SetGrpUsrDate()
            
    def __getXmlBaseNodes__(self):
        self.baseDoc=None
        self.baseSettings=None
        
        self.baseDoc=self.netSupplier.getBaseNode()
        self.baseSettings=self.netSupplier.getChildForced(self.netSupplier.getRoot(),'settings')
        #if self.baseSettings is None:
        #    self.baseSettings=self.netSupplier.createSubNode(self.netSupplier.getRoot(),'settings')
        #    self.netSupplier.AlignNode(self.baseSettings,iRec=1)
        self.__getSettings__()
    def OnMnFileItem_exitMenu(self, event):
        self.Close()
        event.Skip()

    def OnMnFileItem_settingMenu(self, event):
        nodeSetting=self.netSupplier.getChild(self.netSupplier.getRoot(),'settings')
        if nodeSetting is not None:
            dlg=vSupplierSettingsDialog(self)
            dlg.SetXml(self.netSupplier,nodeSetting)
            ret=dlg.ShowModal()
            if ret>0:
                dlg.GetXml(self.netSupplier,nodeSetting)
                self.__getSettings__()
            dlg.Destroy()
        event.Skip()

    def OnFrameClose(self, event):
        if self.netMaster.IsModified()==True:
            dlg = wx.MessageDialog(self,
                        _(u'Unsaved data present! Do you want to save data?'),
                        u'vSupplier'+u' '+_(u'Close'),
                        wx.OK | wx.CANCEL | wx.ICON_QUESTION
                        #wx.YES_NO | wx.NO_DEFAULT | wx.CANCEL | wx.ICON_INFORMATION
                        )
            if dlg.ShowModal()==wx.ID_OK:
                self.SaveFile()
                pass
        self.netMaster.ShutDown()
        
        event.Skip()

    def OnFrameActivate(self, event):
        if self.IsShown():
            if self.bActivated==False:
                self.netMaster.ShowPopup()
                self.bActivated=True
        event.Skip()
    def __setCfg__(self):
        try:
            node=self.xdCfg.getChildForced(None,'size')
            iX=self.xdCfg.GetValue(node,'x',int,20)
            iY=self.xdCfg.GetValue(node,'y',int,20)
            iWidth=self.xdCfg.GetValue(node,'width',int,800)
            iHeight=self.xdCfg.GetValue(node,'height',int,600)
            iX,iY,iWidth,iHeight=vSystem.LimitWindowToScreen(iX,iY,iWidth,iHeight)
            self.Move((iX,iY))
            self.SetSize((iWidth,iHeight))
            
            node=self.xdCfg.getChildForced(None,'layout')
            
            i=self.xdCfg.GetValue(node,'nav_sash',int,200)
            self.slwNav.SetDefaultSize((i, 1000))
            
            i=self.xdCfg.GetValue(node,'top_sash',int,100)
            self.slwTop.SetDefaultSize((1000, i))
            
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            
            self.xdCfg.AlignDoc()
            self.xdCfg.Save()
            self.bBlockCfgEventHandling=False
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSlwNavSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            node=self.xdCfg.getChildForced(None,'layout')
            iWidth=event.GetDragRect().width
            #if iWidth>80:
            #    iWidth=80
            self.xdCfg.SetValue(node,'nav_sash',iWidth)
            self.xdCfg.Save()
            
            self.slwNav.SetDefaultSize((iWidth, 1000))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSlwTopSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            node=self.xdCfg.getChildForced(None,'layout')
            iHeight=event.GetDragRect().height
            #if iHeight>80:
            #    iHeight=80
            self.xdCfg.SetValue(node,'top_sash',iHeight)
            self.xdCfg.Save()
            
            self.slwTop.SetDefaultSize((1000, iHeight))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnFrameSize(self, event):
        event.Skip()
        try:
            bMod=False
            sz=event.GetSize()
            node=self.xdCfg.getChildForced(None,'size')
            self.xdCfg.SetValue(node,'width',sz[0])
            self.xdCfg.SetValue(node,'height',sz[1])
            self.xdCfg.Save()
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnFrameMove(self, event):
        event.Skip()
        try:
            pos=self.GetPosition()
            node=self.xdCfg.getChildForced(None,'size')
            self.xdCfg.SetValue(node,'x',pos[0])
            self.xdCfg.SetValue(node,'y',pos[1])
            self.xdCfg.Save()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbApplyButton(self, event):
        event.Skip()

    def OnCbCancelButton(self, event):
        event.Skip()

    def OnMnHelpMn_help_helpMenu(self, event):
        try:
            self.hlp=wx.html.HtmlHelpController()
            self.hlp.AddBook('vXXXHelpBook.hhp')
            self.hlp.DisplayContents()
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()

    def OnMnHelpMn_help_aboutMenu(self, event):
        dlg=vAboutDialog.create(self,images.getApplicationBitmap())
        dlg.Centre()
        dlg.ShowModal()
        dlg.Destroy()
        event.Skip()

    def OnMnHelpMn_help_logMenu(self, event):
        frm=vtLogFileViewerFrame(self)
        frm.Centre()
        sFN=vtLog.vtLngGetFN()
        if sFN is not None:
            frm.OpenFile(sFN)
        frm.Show()
        event.Skip()
    def SetCfgData(self,func,l,d):
        self.cfgFunc=func
        self.cfgOrigin=l
        pass
    def GetCfgData(self):
        d={}
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,d)

    def OnViTagToolXmlTagCancel(self, event):
        event.Skip()
        #self.nbData.SetNode(self.node)

    def OnViTagToolXmlTagApply(self, event):
        event.Skip()
        #self.nbData.GetNode()
    def OnSbMainRightUp(self, event):
        event.Skip()
        try:
            if self.ShowPopupStatusBar():
                self.AddMsg2PopupStatusBar((None,None))
                self.rbMsg.proc(self.AddMsg2PopupStatusBar)
        except:
            vtLog.vtLngTB(self.GetName())
