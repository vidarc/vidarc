#----------------------------------------------------------------------------
# Name:         vXmlNodeWorkPackageAttr.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: vXmlNodeWorkPackageAttr.py,v 1.1 2006/09/15 12:53:12 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeTag import *
from vidarc.tool.xml.vtXmlNodeAttrML import *
try:
    if vcCust.is2Import(__name__):
        from vidarc.vApps.vPrjPlan.vWorkPackageAttrPanel import vWorkPackageAttrPanel
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported','import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped','import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeWorkPackageAttr(vtXmlNodeTag,vtXmlNodeAttrML):
    ATTR_HIDDEN=['tag','name','description','idx',
            'dateTimeStart','dateTimeEnd','progress','status','manager']
    def __init__(self,tagName='WorkPackageAttr',cfgBase=['cfg']):
        global _
        _=vtLgBase.assignPluginLang('vPrjPlan')
        vtXmlNodeTag.__init__(self,tagName)
        vtXmlNodeAttrML.__init__(self,tagName,cfgBase=cfgBase)
    def GetDescription(self):
        return _(u'attributes')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def IsAttrFile(self,sTag):
        iRet=sTag.find('file')
        if iRet==0:
            if len(sTag)>4:
                if sTag[4]!='_':
                    return True
            return False
    def IsAttrHidden(self,sTag):
        if self.IsAttrFile(sTag):
            return True
        return vtXmlNodeTag.IsAttrHidden(self,sTag)
    def GetAttrFilterTypes(self):
        #lTag=vtXmlNodeTag.GetAttrFilterTypes(self)
        lAttr=vtXmlNodeAttrML.GetAttrFilterTypes(self)
        return lAttr
    def GetTranslation(self,name):
        #vtXmlNodeTag.GetTranslation(self,name)
        return vtXmlNodeAttrML.GetTranslation(self,name)
    def Is2Create(self):
        return False
    def IsSkip(self):
        return False
    def Is2Add(self):
        return False
    def IsId2Add(self):
        return False
    def UseBaseNode(self):
        "GUI object use base node"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\nIDAT8\x8d\x85S1\xae\xc20\x0c\xb5S\x06\xfe\xc8\xd0dG\xfa\xc7\xfaK\
\xa5\x96# \x14u\xaaz\x00\x04S\x848\xc8?\x07\x1b\\\xa000!1 \x99\x01\x8c\x9c\
\xc4\x80\xa5\x0e~\xb2\x9f\xeb\xe7\x17DS\x80\x16\xb6\xacH\xe6\xc7\xd3\x16\xb5\
\xba\xd1\xbb\xe6\xbe\x0b\x11\xe6[ \x95\x04M\x11}\xce\xd5\xb4\tD\x9b@\xe4\\Mi\
\x9e\xd6\x7fl\xfe\x86\xa3)\xc0h+\xf8\xb6\x89v>\x9e\xb6\xe8\xdbF\xd5*#\xe0B[V\
\xc4B\xa6\x82\xca|$\xc1T8\x16O\x1d-\t\xb8\xf9\xddo\xca\x18\x8f\x7f\xe1z\xdd\
\x83-\xab\xc7UX Mq\x899W\xd3t\xba\xcc\xb0H\x03)V\xdf\x05\x90+\xf5]\x80\xcbe\
\x97\x19*3\xd2\x83$\xde\xdb\x983,\xfc\\u\xa3zF\x9e8\x0c\x87g6y\xe1\xa9\xd8F6\
\xc8\xd3\xddn\x00\xeb\xd5?\x00\x00,|\x935\xb3W\x10M\x91\xb1J\x1d\xb4\x90FC~\
\x8d<\x99\x9b\xea\xd9\x1fX\xfb\x93\x11\xa5.\xc5\xf49K\x97q\xa1\x86q\xdc\x01\
\xdcj\xbfrM}\n \x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        return None
    def GetAddDialogClass(self):
        return None
    def GetPanelClass(self):
        if GUI:
            return vWorkPackageAttrPanel
        else:
            return None

