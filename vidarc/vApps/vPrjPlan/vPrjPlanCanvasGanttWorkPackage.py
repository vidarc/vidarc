#----------------------------------------------------------------------------
# Name:         vPrjPlanCanvasGanttWorkPackage.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060410
# CVS-ID:       $Id: vPrjPlanCanvasGanttWorkPackage.py,v 1.1 2006/04/11 17:43:02 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.draw.vtDrawCanvasGroup import *
from vidarc.tool.draw.vtDrawCanvasRectangle import *
from vidarc.tool.draw.vtDrawCanvasRectangleFilled import *
from vidarc.tool.draw.vtDrawCanvasText import *
from vidarc.tool.time.vtTime import vtDateTime

class vPrjPlanCanvasGanttWorkPackage(vtDrawCanvasGroup):
    def __init__(self,doc=None,node=None,x=0,y=0,w=0,h=0,layer=0):
        id=-1
        name=''
        self.dtStart=vtDateTime(True)
        self.dtEnd=vtDateTime(True)
        self.iDays=0
        sProg=''
        iStatus=0
        if doc is not None and node is not None:
            id=doc.getKey(node)
            name=doc.getNodeText(node,'tag')
            global dtStruct
            self.dtStart.SetStr(doc.getNodeText(node,'dateTimeStart'))
            self.dtEnd.SetStr(doc.getNodeText(node,'dateTimeEnd'))
            sProg=doc.getNodeText(node,'progress')+'% '
            sStatus=doc.getNodeText(node,'status')
            iStatus=0
            if sStatus=='green':
                iStatus=10
            elif sStatus=='yellow':
                iStatus=11
            elif sStatus=='red':
                iStatus=12
            dtStart=vtDateTime(True)
            dtEnd=vtDateTime(True)
            dtStart.SetStr(self.dtStart.GetDateStr()+' 00:00:00')
            dtEnd.SetStr(self.dtEnd.GetDateStr()+' 00:00:00')
            zDiff=dtEnd.dt-dtStart.dt
            print zDiff
            self.iDays=zDiff.days+1
            if self.iDays<=0:
                self.iDays=1
        w=self.iDays
        vtDrawCanvasGroup.__init__(self,id=id,name=name,x=x,y=y,w=w,h=h,layer=layer)
        self.objs.append(vtDrawCanvasRectangleFilled(id=-1,name='',x=0,y=0,w=w,h=h,layer=iStatus))
        #self.objs.append(vtDrawCanvasText(id=-1,text=sProg,x=0,y=0,w=w,h=h,
        #            layer=layer))
        self.bActive=True
    def AlignDateStart(self,dtStart):
        dtEnd=vtDateTime(True)
        dtEnd.SetStr(self.dtStart.GetDateStr()+' 00:00:00')
        zDiff=dtEnd.dt-dtStart.dt
        iX=zDiff.days+self.GetX()
        self.SetX(iX)
    def GetDateStart(self):
        return self.dtStart
    def GetDateEnd(self):
        return self.dtEnd
    

