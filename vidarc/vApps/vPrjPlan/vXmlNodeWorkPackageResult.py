#----------------------------------------------------------------------------
# Name:         vXmlNodeWorkPackageResult.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060410
# CVS-ID:       $Id: vXmlNodeWorkPackageResult.py,v 1.3 2010/03/03 02:17:15 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.lang.vtLgBase as vtLgBase
#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
from vidarc.tool.xml.vtXmlNodeTag import *
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0


class vXmlNodeWorkPackageResult(vtXmlNodeTag):
    MAP_STATUS=[u'undefined', u'happy', u'cool', u'angry']
    def __init__(self,tagName='WrkPkgResult'):
        global _
        _=vtLgBase.assignPluginLang('vPrjPlan')
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'result')
    # ---------------------------------------------------------
    # specific
    def GetStatus(self,node):
        iStatus=self.GetVal(node,'status',int,0)
        return iStatus
    def SetStatus(self,node,val):
        self.SetVal(node,'status',str(val))
    def IsFinished(self,node):
        try:
            if self.GetStatus(node)>0:
                return True
            else:
                return False
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return False
    # ---------------------------------------------------------
    # inheritance
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsId2Add(self):
        "node id is managed"
        return True
    def IsNotContained(self):
        "do not add to listbook"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00TIDAT8\x8d\xcd\x93A\x0e\x00\x10\x0c\x04m\xeb\xff?\xa6.\x9a\x88h\x14\
\x11\xf6l\xc6f\x93\x02\xc4\xe1$tD\xff)\x90\x9cDr\x12\xaf\x00\xd6\x88*\x011\
\xb6\x04^Ql\x1f\r\x7f\xa8\xa0%\x9a6\xb0\x1a\xa9\xec~\x03\xd7\x06;\xa0)\xf0\
\x82\x9a\xe5\x11\xfb\xbc\xbf\x85\x02\xda\xc009\xedt\x8d\x88\x00\x00\x00\x00I\
END\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            d=vtXmlNodeTag.GetAddDialogClass(self).copy()
            d['pnCls']=vtXmlNodeTag.GetPanelClass(self)
            return d
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            d=vtXmlNodeTag.GetAddDialogClass(self).copy()
            d['pnCls']=vtXmlNodeTag.GetPanelClass(self)
            return d
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vtXmlNodeTag.GetPanelClass(self)
        else:
            return None
