#----------------------------------------------------------------------------
# Name:         vPrjPlanCanvasStructureWorkPackage.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060410
# CVS-ID:       $Id: vPrjPlanCanvasStructureWorkPackage.py,v 1.1 2006/04/11 17:43:02 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.draw.vtDrawCanvasGroup import *
from vidarc.tool.draw.vtDrawCanvasRectangle import *
from vidarc.tool.draw.vtDrawCanvasRectangleFilled import *
from vidarc.tool.draw.vtDrawCanvasText import *
from vidarc.tool.time.vtTime import vtDateTime

dtStruct=vtDateTime(True)

class vPrjPlanCanvasStructureWorkPackage(vtDrawCanvasGroup):
    def __init__(self,doc=None,node=None,x=0,y=0,w=0,h=0,layer=0):
        id=-1
        name=''
        sDtStart=''
        sDtEnd=''
        sTimeAct=''
        sCostAct=''
        sProg=''
        iStatus=0
        if doc is not None and node is not None:
            id=doc.getKey(node)
            name=doc.getNodeText(node,'tag')
            global dtStruct
            dtStruct.SetStr(doc.getNodeText(node,'dateTimeStart'))
            sDtStart=dtStruct.GetDateStr()
            dtStruct.SetStr(doc.getNodeText(node,'dateTimeEnd'))
            sDtEnd=dtStruct.GetDateStr()
            sProg=doc.getNodeText(node,'progress')+'% '
            sStatus=doc.getNodeText(node,'status')
            iStatus=0
            if sStatus=='green':
                iStatus=10
            elif sStatus=='yellow':
                iStatus=11
            elif sStatus=='red':
                iStatus=12
        dW=w>>2
        vtDrawCanvasGroup.__init__(self,id=id,name=name,x=x,y=y,w=w,h=h,layer=layer)
        self.objs.append(vtDrawCanvasRectangle(id=-1,name='',x=0,y=0,w=w,h=h,layer=layer))
        self.objs.append(vtDrawCanvasText(id=-1,text=name,x=0,y=0,w=dW<<1,h=1,layer=layer))
        self.objs.append(vtDrawCanvasText(id=-1,text=sDtStart,x=0,y=1,w=dW<<1,h=1,layer=layer))
        self.objs.append(vtDrawCanvasText(id=-1,text=sDtEnd,x=dW<<1,y=1,w=dW<<1,h=1,layer=layer))
        self.objs.append(vtDrawCanvasRectangleFilled(id=-1,name='',x=dW<<1,y=0,w=dW,h=1,layer=iStatus))
        self.objs.append(vtDrawCanvasText(id=-1,text=sProg,x=(dW<<1)+dW,y=0,w=dW,h=1,
                        align_horizontal=vtDrawCanvasText.ALIGN_RIGHT,layer=layer))
        self.bActive=True

