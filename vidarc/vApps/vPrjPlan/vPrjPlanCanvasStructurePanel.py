#----------------------------------------------------------------------------
# Name:         vPrjPlanStructureCanvasPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060410
# CVS-ID:       $Id: vPrjPlanCanvasStructurePanel.py,v 1.2 2010/03/03 02:17:14 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.draw.vtDrawCanvasXmlPanel import *
from vidarc.tool.draw.vtDrawCanvasXmlGroup import *

from vidarc.tool.draw.vtDrawCanvasRectangle import *
from vidarc.tool.draw.vtDrawCanvasLine import *
from vidarc.tool.draw.vtDrawCanvasText import *

from vPrjPlanCanvasStructureWorkPackage import *

import os
import vidarc.tool.xml.vtXmlHierarchy as vtXmlHierarchy
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtColor as vtColor

class vPrjPlanCanvasStructurePanel(vtDrawCanvasPanel):
    DFT_COLOR=[
        (178, 107, 000),
        ( 94, 154,  94),
        (148, 148, 148),
        ( 79,  79,  59),
        (210,   0,   0),
        (120,  78, 146),
        ( 31, 156, 127),
        (178, 100, 178),
        (  0,   0,   0),
        (  0,   0,   0),
        vtColor.GREEN4,
        vtColor.YELLOW2,
        vtColor.RED3,
        ]
    DFT_DIS_COLOR=[
        (167, 100, 000),
        (167, 136, 000),
        ( 67, 135, 112),
        (116, 130, 147),
        (100, 166, 100),
        (150, 114,  22),
        (147,  82, 147),
        (  0,   0,   0),
        (  0,   0,   0),
        vtColor.GREEN4,
        vtColor.YELLOW2,
        vtColor.RED3,
        ]
    SELECTED_LAYER=3
    SELECTED_WIDTH=10
    SELECTED_HEIGHT=10
    
    FOCUS_LAYER=3
    FOCUS_WIDTH=6
    FOCUS_HEIGHT=6

    def __init__(self, parent, 
                id=wx.NewId(), pos=wx.DefaultPosition, size=wx.Size(100, 50), 
                style=wx.TAB_TRAVERSAL, name=u'StatusDevicePanel',bTex=False):
        vtDrawCanvasPanel.__init__(self,parent,id,pos,size,style,name)
        obj=vtDrawCanvasObjectBase()
        if bTex:
            drv=obj.DRV_LATEX
        else:
            drv=obj.DRV_WX
        self.SetConfig(obj,drv,max=(100,100),grid=(15,15))
        self.RegisterObject(vtDrawCanvasLine())
        self.RegisterObject(vtDrawCanvasRectangle())
        self.RegisterObject(vtDrawCanvasText())
        #self.RegisterObject(StatusDeviceMFC())
        self.SetActivateEnable(True)
    def __setDftColors__(self):
        def convColor2Act(a):
            a=int(a*1.0)
            if a<0:
                a=0
            if a>255:
                a=255
            return a
        self.color=[]
        self.colorAct=[]
        if hasattr(self,'DFT_ACT_COLOR'):
            for c in self.DFT_ACT_COLOR:
                self.colorAct.append(wx.Colour(c[0], c[1], c[2]))
            for c in self.DFT_COLOR:
                self.color.append(wx.Colour(c[0], c[1], c[2]))
        else:
            for c in self.DFT_COLOR:
                self.colorAct.append(wx.Colour(c[0], c[1], c[2]))
            for c in self.DFT_COLOR:
                r,g,b=map(convColor2Act,c)
                self.color.append(wx.Colour(r, g, b))
            
        self.colorDis=[]
        self.colorActDis=[]
        if hasattr(self,'DFT_ACT_DIS_COLOR'):
            for c in self.DFT_ACT_DIS_COLOR:
                self.colorActDis.append(wx.Colour(c[0], c[1], c[2]))
        else:
            for c in self.DFT_DIS_COLOR:
                self.colorActDis.append(wx.Colour(c[0], c[1], c[2]))
        for c in self.DFT_DIS_COLOR:
            r,g,b=map(convColor2Act,c)
            self.colorDis.append(wx.Colour(r, g, b))
        self.colorBkg=self.scwDrawing.GetBackgroundColour()
        
        self.__setDftBrush__()
        self.__setDftPen__()
    def GetDeviceIds(self):
        lst=[]
        for o in self.objs:
            id=o.GetId()
            if id>=0:
                try:
                    lst.index(id)
                except:
                    lst.append(id)
        return lst
    def DeactivateDevices(self,lst):
        for o in self.objs:
            #print o
            try:
                lst.index(o.GetId())
                o.SetActive(False)
            except:
                o.SetActive(True)
                pass
            self.ShowObj(o,False)
        #self.ShowAll()
    def GetDlg(self):
        return None
    
    def __build__(self,node,iLv):
        vtLog.CallStack('')
        print iLv,self.iActX,self.iActY
        self.iActX=iLv*2#(self.iActW>>1)
        o=vPrjPlanCanvasStructureWorkPackage(doc=self.doc,node=node,
                x=self.iActX,y=self.iActY,
                w=self.iActW,h=self.iActH)
        self.objs.append(o)
        self.objsHier.append(o)
        #if iLv%2:
        #    self.iActX+=self.iActW+(self.iActW>>1)
        #else:
        #    self.iActY+=self.iActH+(self.iActH>>1)
        self.iActY+=self.iActH+1#(self.iActH>>1)
        cc=self.doc.getChilds(node,'WorkPackage')
        l=[]
        for c in cc:
            oT=self.__build__(c,iLv+1)
            l.append(oT)
        return (o,l)
        
    def Build(self):
        self.objs=[]
        self.objsHier=[]
        self.ClearInt()
        
        self.iActW=16
        self.iActH=2
        self.iActX=0
        self.iActY=3
        node=self.doc.getBaseNode()
        
        o=vPrjPlanCanvasStructureWorkPackage(w=self.iActW,h=self.iActH)
        self.objs.append(o)
        self.objsHier.append(o)
        l=[]
        cc=self.doc.getChilds(node,'WorkPackage')
        for c in cc:
            oT=self.__build__(c,1)
            l.append(oT)
        
        def addLines(o,l):
            xH=o.GetX()
            yH=-1
            for oo,ll in l:
                yH=oo.GetY()
                oLine=vtDrawCanvasLine(x=xH+1,y=yH+1,w=1,h=0)
                self.objs.append(oLine)
                addLines(oo,ll)
            if yH>0:
                y=o.GetY()
                oLine=vtDrawCanvasLine(x=xH+1,y=y+2,w=0,h=yH-y-1)
                self.objs.append(oLine)
                
        addLines(self.objs[0],l)
        #for o in self.objsHier:
        iLen=iLenNew=0
        for i in range(iLen,iLenNew):
            obj=self.objs[i]
            print obj
            if isinstance(obj,vPrjPlanCanvasStructureWorkPackage):
                yH=obj.GetY()
                print yH
                o=vtDrawCanvasLine(x=self.iActX-1,y=yH+1,w=1,h=0)
                self.objs.append(o)
            
        self.ShowAll()
