#Boa:FramePanel:vXmlNodeKplusShortPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodePurchasePartsPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060621
# CVS-ID:       $Id: vXmlNodeKplusShortPanel.py,v 1.6 2013/06/03 06:11:47 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputFloat
import vidarc.tool.input.vtInputTree
import vidarc.tool.input.vtInputTreeInternal
import wx.lib.buttons
import vidarc.tool.input.vtInputGridSetup

import sys

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.time.vtTime import vtDateTime

[wxID_VXMLNODEKPLUSSHORTPANEL, wxID_VXMLNODEKPLUSSHORTPANELCBUPDATE, 
 wxID_VXMLNODEKPLUSSHORTPANELGRDCOST, 
 wxID_VXMLNODEKPLUSSHORTPANELGRDINKINDINDUST, 
 wxID_VXMLNODEKPLUSSHORTPANELGRDINKINDSCIENCE, 
 wxID_VXMLNODEKPLUSSHORTPANELGRDPERS, wxID_VXMLNODEKPLUSSHORTPANELLBLCOSTACT, 
 wxID_VXMLNODEKPLUSSHORTPANELLBLCOSTCALC, 
 wxID_VXMLNODEKPLUSSHORTPANELLBLINKINDIND, 
 wxID_VXMLNODEKPLUSSHORTPANELLBLINKINDINDUST, 
 wxID_VXMLNODEKPLUSSHORTPANELLBLINKINDSC, 
 wxID_VXMLNODEKPLUSSHORTPANELLBLINKINDSCIENCE, 
 wxID_VXMLNODEKPLUSSHORTPANELLBLSUM, wxID_VXMLNODEKPLUSSHORTPANELLBLSUMOTHER, 
 wxID_VXMLNODEKPLUSSHORTPANELLBLSUMPERS, wxID_VXMLNODEKPLUSSHORTPANELNBCALC, 
 wxID_VXMLNODEKPLUSSHORTPANELPNCFG, wxID_VXMLNODEKPLUSSHORTPANELPNCOST, 
 wxID_VXMLNODEKPLUSSHORTPANELPNINKIND, wxID_VXMLNODEKPLUSSHORTPANELPNWORK, 
 wxID_VXMLNODEKPLUSSHORTPANELSPINKINDINDUST, 
 wxID_VXMLNODEKPLUSSHORTPANELSPINKINDSCIENCE, 
 wxID_VXMLNODEKPLUSSHORTPANELTXTINKINDINDUST, 
 wxID_VXMLNODEKPLUSSHORTPANELTXTINKINDSCIENCE, 
 wxID_VXMLNODEKPLUSSHORTPANELVICOSTACT, 
 wxID_VXMLNODEKPLUSSHORTPANELVICOSTCALC, wxID_VXMLNODEKPLUSSHORTPANELVISUM, 
 wxID_VXMLNODEKPLUSSHORTPANELVISUMOTHER, 
 wxID_VXMLNODEKPLUSSHORTPANELVISUMPERS, 
] = [wx.NewId() for _init_ctrls in range(29)]

class vXmlNodeKplusShortPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_bxsPers_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.grdPers, 1, border=0, flag=wx.EXPAND)

    def _init_coll_gbsCfg_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblCostAct, (0, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viCostAct, (0, 1), border=0, flag=wx.EXPAND,
              span=(1, 2))
        parent.AddWindow(self.lblCostCalc, (1, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viCostCalc, (1, 1), border=0, flag=wx.EXPAND,
              span=(1, 2))
        parent.AddWindow(self.lblInKindIndust, (2, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.spInKindIndust, (2, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.lblInKindScience, (3, 0), border=0,
              flag=wx.EXPAND, span=(1, 1))
        parent.AddWindow(self.spInKindScience, (3, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))

    def _init_coll_bxsCost_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.grdCost, 1, border=0, flag=wx.EXPAND)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbUpdate, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.lblSumPers, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viSumPers, 2, border=8, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.lblSumOther, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viSumOther, 2, border=8,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.lblSum, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viSum, 2, border=0, flag=wx.EXPAND)

    def _init_coll_gbsInKind_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblInKindInd, (0, 0), border=0, flag=wx.EXPAND,
              span=(1, 2))
        parent.AddWindow(self.grdInKindIndust, (1, 0), border=0, flag=wx.EXPAND,
              span=(2, 4))
        parent.AddWindow(self.lblInKindSc, (3, 0), border=0, flag=wx.EXPAND,
              span=(1, 2))
        parent.AddWindow(self.grdInKindScience, (4, 0), border=0,
              flag=wx.EXPAND, span=(2, 4))
        parent.AddWindow(self.txtInKindIndust, (0, 2), border=0, flag=wx.EXPAND,
              span=(1, 2))
        parent.AddWindow(self.txtInKindScience, (3, 2), border=0,
              flag=wx.EXPAND, span=(1, 2))

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.nbCalc, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsBt, 1, border=4,
              flag=wx.TOP | wx.EXPAND | wx.BOTTOM)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_nbCalc_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.pnWork, select=False,
              text=_(u'personnel'))
        parent.AddPage(imageId=-1, page=self.pnCost, select=False,
              text=_(u'other cost'))
        parent.AddPage(imageId=-1, page=self.pnInKind, select=True,
              text=_(u'inkind'))
        parent.AddPage(imageId=-1, page=self.pnCfg, select=False,
              text=_(u'config'))

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=0, rows=2, vgap=4)

        self.bxsPers = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsCost = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.gbsCfg = wx.GridBagSizer(hgap=4, vgap=4)

        self.gbsInKind = wx.GridBagSizer(hgap=4, vgap=4)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsPers_Items(self.bxsPers)
        self._init_coll_bxsCost_Items(self.bxsCost)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_gbsCfg_Items(self.gbsCfg)
        self._init_coll_gbsInKind_Items(self.gbsInKind)

        self.SetSizer(self.fgsLog)
        self.pnInKind.SetSizer(self.gbsInKind)
        self.pnWork.SetSizer(self.bxsPers)
        self.pnCfg.SetSizer(self.gbsCfg)
        self.pnCost.SetSizer(self.bxsCost)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEKPLUSSHORTPANEL,
              name=u'vXmlNodeKplusShortPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(312, 264),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 237))
        self.SetAutoLayout(True)

        self.nbCalc = wx.Notebook(id=wxID_VXMLNODEKPLUSSHORTPANELNBCALC,
              name=u'nbCalc', parent=self, pos=wx.Point(0, 0), size=wx.Size(304,
              195), style=0)

        self.pnWork = wx.Panel(id=wxID_VXMLNODEKPLUSSHORTPANELPNWORK,
              name=u'pnWork', parent=self.nbCalc, pos=wx.Point(0, 0),
              size=wx.Size(296, 169), style=wx.TAB_TRAVERSAL)

        self.pnCost = wx.Panel(id=wxID_VXMLNODEKPLUSSHORTPANELPNCOST,
              name=u'pnCost', parent=self.nbCalc, pos=wx.Point(0, 0),
              size=wx.Size(296, 169), style=wx.TAB_TRAVERSAL)

        self.grdPers = vidarc.tool.input.vtInputGridSetup.vtInputGridSetup(id=wxID_VXMLNODEKPLUSSHORTPANELGRDPERS,
              name=u'grdPers', parent=self.pnWork, pos=wx.Point(0, 0),
              size=wx.Size(296, 169))
        self.grdPers.Bind(vidarc.tool.input.vtInputGridSetup.vEVT_VTINPUT_GRIDSETUP_INFO_CHANGED,
              self.OnViPartsVtinputGridsetupInfoChanged,
              id=wxID_VXMLNODEKPLUSSHORTPANELGRDPERS)

        self.grdCost = vidarc.tool.input.vtInputGridSetup.vtInputGridSetup(id=wxID_VXMLNODEKPLUSSHORTPANELGRDCOST,
              name=u'grdCost', parent=self.pnCost, pos=wx.Point(0, 0),
              size=wx.Size(296, 169))

        self.cbUpdate = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODEKPLUSSHORTPANELCBUPDATE,
              bitmap=vtArt.getBitmap(vtArt.Synch), label=_(u'update'),
              name=u'cbUpdate', parent=self, pos=wx.Point(0, 203),
              size=wx.Size(76, 30), style=0)
        self.cbUpdate.SetMinSize(wx.Size(-1, -1))
        self.cbUpdate.Bind(wx.EVT_BUTTON, self.OnCbUpdateButton,
              id=wxID_VXMLNODEKPLUSSHORTPANELCBUPDATE)

        self.pnCfg = wx.Panel(id=wxID_VXMLNODEKPLUSSHORTPANELPNCFG,
              name=u'pnCfg', parent=self.nbCalc, pos=wx.Point(0, 0),
              size=wx.Size(296, 169), style=wx.TAB_TRAVERSAL)

        self.lblCostAct = wx.StaticText(id=wxID_VXMLNODEKPLUSSHORTPANELLBLCOSTACT,
              label=_(u'actual cost'), name=u'lblCostAct', parent=self.pnCfg,
              pos=wx.Point(0, 0), size=wx.Size(73, 24), style=wx.ALIGN_RIGHT)
        self.lblCostAct.SetMinSize(wx.Size(-1, -1))

        self.viCostAct = vidarc.tool.input.vtInputTreeInternal.vtInputTreeInternal(id=wxID_VXMLNODEKPLUSSHORTPANELVICOSTACT,
              name=u'viCostAct', parent=self.pnCfg, pos=wx.Point(77, 0),
              size=wx.Size(254, 24), style=0)

        self.lblCostCalc = wx.StaticText(id=wxID_VXMLNODEKPLUSSHORTPANELLBLCOSTCALC,
              label=_(u'calculation cost'), name=u'lblCostCalc',
              parent=self.pnCfg, pos=wx.Point(0, 28), size=wx.Size(73, 24),
              style=wx.ALIGN_RIGHT)
        self.lblCostCalc.SetMinSize(wx.Size(-1, -1))

        self.viCostCalc = vidarc.tool.input.vtInputTreeInternal.vtInputTreeInternal(id=wxID_VXMLNODEKPLUSSHORTPANELVICOSTCALC,
              name=u'viCostCalc', parent=self.pnCfg, pos=wx.Point(77, 28),
              size=wx.Size(254, 24), style=0)

        self.lblInKindScience = wx.StaticText(id=wxID_VXMLNODEKPLUSSHORTPANELLBLINKINDSCIENCE,
              label=_(u'inkind scientific'), name=u'lblInKindScience',
              parent=self.pnCfg, pos=wx.Point(0, 81), size=wx.Size(73, 21),
              style=wx.ALIGN_RIGHT)
        self.lblInKindScience.SetMinSize(wx.Size(-1, -1))

        self.spInKindScience = wx.SpinCtrl(id=wxID_VXMLNODEKPLUSSHORTPANELSPINKINDSCIENCE,
              initial=0, max=100, min=0, name=u'spInKindScience',
              parent=self.pnCfg, pos=wx.Point(77, 81), size=wx.Size(125, 21),
              style=wx.SP_ARROW_KEYS)
        self.spInKindScience.SetMinSize(wx.Size(-1, -1))

        self.lblInKindIndust = wx.StaticText(id=wxID_VXMLNODEKPLUSSHORTPANELLBLINKINDINDUST,
              label=_(u'inkind industrial'), name=u'lblInKindIndust',
              parent=self.pnCfg, pos=wx.Point(0, 56), size=wx.Size(73, 21),
              style=wx.ALIGN_RIGHT)
        self.lblInKindIndust.SetMinSize(wx.Size(-1, -1))

        self.spInKindIndust = wx.SpinCtrl(id=wxID_VXMLNODEKPLUSSHORTPANELSPINKINDINDUST,
              initial=0, max=100, min=0, name=u'spInKindIndust',
              parent=self.pnCfg, pos=wx.Point(77, 56), size=wx.Size(125, 21),
              style=wx.SP_ARROW_KEYS)
        self.spInKindIndust.SetMinSize(wx.Size(-1, -1))

        self.pnInKind = wx.Panel(id=wxID_VXMLNODEKPLUSSHORTPANELPNINKIND,
              name=u'pnInKind', parent=self.nbCalc, pos=wx.Point(0, 0),
              size=wx.Size(296, 169), style=wx.TAB_TRAVERSAL)

        self.lblInKindInd = wx.StaticText(id=wxID_VXMLNODEKPLUSSHORTPANELLBLINKINDIND,
              label=_(u'industrial'), name=u'lblInKindInd',
              parent=self.pnInKind, pos=wx.Point(0, 0), size=wx.Size(72, 21),
              style=0)
        self.lblInKindInd.SetMinSize(wx.Size(-1, -1))

        self.grdInKindIndust = vidarc.tool.input.vtInputGridSetup.vtInputGridSetup(id=wxID_VXMLNODEKPLUSSHORTPANELGRDINKINDINDUST,
              name=u'grdInKindIndust', parent=self.pnInKind, pos=wx.Point(0,
              25), size=wx.Size(180, 44))
        self.grdInKindIndust.SetMinSize(wx.Size(-1, -1))

        self.lblInKindSc = wx.StaticText(id=wxID_VXMLNODEKPLUSSHORTPANELLBLINKINDSC,
              label=_(u'scientific'), name=u'lblInKindSc', parent=self.pnInKind,
              pos=wx.Point(0, 73), size=wx.Size(72, 21), style=0)
        self.lblInKindSc.SetMinSize(wx.Size(-1, -1))

        self.grdInKindScience = vidarc.tool.input.vtInputGridSetup.vtInputGridSetup(id=wxID_VXMLNODEKPLUSSHORTPANELGRDINKINDSCIENCE,
              name=u'grdInKindScience', parent=self.pnInKind, pos=wx.Point(0,
              98), size=wx.Size(180, 44))
        self.grdInKindScience.SetMinSize(wx.Size(-1, -1))

        self.txtInKindScience = wx.TextCtrl(id=wxID_VXMLNODEKPLUSSHORTPANELTXTINKINDSCIENCE,
              name=u'txtInKindScience', parent=self.pnInKind, pos=wx.Point(76,
              73), size=wx.Size(104, 21), style=wx.TE_RIGHT, value=u'')
        self.txtInKindScience.Enable(False)
        self.txtInKindScience.SetMinSize(wx.Size(-1, -1))

        self.txtInKindIndust = wx.TextCtrl(id=wxID_VXMLNODEKPLUSSHORTPANELTXTINKINDINDUST,
              name=u'txtInKindIndust', parent=self.pnInKind, pos=wx.Point(76,
              0), size=wx.Size(104, 21), style=wx.TE_RIGHT, value=u'')
        self.txtInKindIndust.Enable(False)
        self.txtInKindIndust.SetMinSize(wx.Size(-1, -1))

        self.lblSumPers = wx.StaticText(id=wxID_VXMLNODEKPLUSSHORTPANELLBLSUMPERS,
              label=_(u'personnel'), name=u'lblSumPers', parent=self,
              pos=wx.Point(84, 203), size=wx.Size(20, 30),
              style=wx.ALIGN_RIGHT)

        self.lblSumOther = wx.StaticText(id=wxID_VXMLNODEKPLUSSHORTPANELLBLSUMOTHER,
              label=_(u'other'), name=u'lblSumOther', parent=self,
              pos=wx.Point(156, 203), size=wx.Size(20, 30),
              style=wx.ALIGN_RIGHT)

        self.viSumPers = vidarc.tool.input.vtInputFloat.vtInputFloat(default=0.0,
              fraction_width=2, id=wxID_VXMLNODEKPLUSSHORTPANELVISUMPERS,
              integer_width=8, maximum=100000000, minimum=0.0,
              name=u'viSumPers', parent=self, pos=wx.Point(108, 203),
              size=wx.Size(40, 30), style=0)

        self.viSumOther = vidarc.tool.input.vtInputFloat.vtInputFloat(default=0.0,
              fraction_width=2, id=wxID_VXMLNODEKPLUSSHORTPANELVISUMOTHER,
              integer_width=8, maximum=100000000, minimum=0.0,
              name=u'viSumOther', parent=self, pos=wx.Point(180, 203),
              size=wx.Size(40, 30), style=0)

        self.lblSum = wx.StaticText(id=wxID_VXMLNODEKPLUSSHORTPANELLBLSUM,
              label=_(u'sum'), name=u'lblSum', parent=self, pos=wx.Point(228,
              203), size=wx.Size(20, 30), style=wx.ALIGN_RIGHT)

        self.viSum = vidarc.tool.input.vtInputFloat.vtInputFloat(default=0.0,
              fraction_width=2, id=wxID_VXMLNODEKPLUSSHORTPANELVISUM,
              integer_width=9, maximum=1000000000, minimum=0.0, name=u'viSum',
              parent=self, pos=wx.Point(252, 203), size=wx.Size(48, 30),
              style=0)

        self._init_coll_nbCalc_Pages(self.nbCalc)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vPrjPlan')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.grdPers.SetAutoStretch(True)
        self.grdCost.SetAutoStretch(True)
        self.grdInKindIndust.SetAutoStretch(True)
        self.grdInKindScience.SetAutoStretch(True)
        
        self.spInKindIndust.SetValue(20)
        self.spInKindScience.SetValue(5)
        
        from vidarc.vApps.vPrjPlan.vXmlPrjPlanTree import vXmlPrjPlanTree
        self.viCostAct.SetTreeClass(vXmlPrjPlanTree)
        self.viCostAct.AddTreeCall('init','SetNodeInfos',['tag','name'])
        #self.viCostAct.AddTreeCall('init','SetValidInfo',['WorkPackage','CostDefine'])
        self.viCostAct.AddTreeCall('init','SetGrouping',[],[('tag',''),('name','')])
        self.viCostAct.SetTagNames('costAct','tag')
        
        self.viCostCalc.SetTreeClass(vXmlPrjPlanTree)
        self.viCostCalc.AddTreeCall('init','SetNodeInfos',['tag','name'])
        #self.viCostCalc.AddTreeCall('init','SetValidInfo',['WorkPackage','CostDefine'])
        self.viCostCalc.AddTreeCall('init','SetGrouping',[],[('tag',''),('name','')])
        self.viCostCalc.SetTagNames('costCalc','tag')
        
        self.gbsInKind.AddGrowableCol(1)
        self.gbsInKind.AddGrowableCol(3)
        self.gbsInKind.AddGrowableRow(2)
        self.gbsInKind.AddGrowableRow(5)
        
        self.viSumPers.Enable(False)
        self.viSumOther.Enable(False)
        self.viSum.Enable(False)
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def ClearInt(self):
        try:
            vtXmlNodePanel.ClearInt(self)
            # add code here
            self.grdPers.Clear()
            self.grdCost.Clear()
            self.grdInKindIndust.Clear()
            self.grdInKindScience.Clear()
            self.txtInKindScience.SetValue('')
            self.txtInKindIndust.SetValue('')
            self.viCostAct.Clear()
            self.viCostCalc.Clear()
            self.viSumPers.SetValue(0.0)
            self.viSumOther.SetValue(0.0)
            self.viSum.SetValue(0.0)
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def SetRegNode(self,obj):
        vtXmlNodePanel.SetRegNode(self,obj)
        self.grdPers.Setup(obj.GetSetup(kind='personnel'),None)
        self.grdCost.Setup(obj.GetSetup(kind='otherCost'),None)
        self.grdInKindIndust.Setup(obj.GetSetup(kind='inkindIndustrial'),None)
        self.grdInKindScience.Setup(obj.GetSetup(kind='inkindScientific'),None)
    def SetNetDocs(self,d):
        if d.has_key('vHum'):
            dd=d['vHum']
            self.docHum=dd['doc']
        # add code here
    def OnGotContent(self,evt):
        evt.Skip()
        try:
            self.viCostAct.SetNodeTree(self.doc.getBaseNode())
            self.viCostCalc.SetNodeTree(self.doc.getBaseNode())
        except:
            vtLog.vtLngTB(self.GetName())
    def SetDoc(self,doc,bNet=False):
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        # add code here
        self.viCostAct.SetDoc(doc,bNet)
        self.viCostAct.SetDocTree(doc,bNet)
        self.viCostCalc.SetDoc(doc,bNet)
        self.viCostCalc.SetDocTree(doc,bNet)
    def SetNode(self,node):
        try:
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            # add code here
            for kind,obj in [('personnel',self.grdPers),
                            ('otherCost',self.grdCost),
                            ('inkindIndustrial',self.grdInKindIndust),
                            ('inkindScientific',self.grdInKindScience),
                            ]:
                tup=self.objRegNode.GetValues(self.node,kind=kind)
                if len(tup)>0:
                    obj.SetValue(tup[0])
                obj.SetModified(False)
            self.viCostAct.SetNode(self.node)
            self.viCostCalc.SetNode(self.node)
        except:
            self.__logTB__()
        self.__showSum__()
    def __showSum__(self):
        try:
            tup=self.grdPers.GetValueStr()
            self.viSumPers.SetValue(tup[-1][2])
            fSum=float(tup[-1][3])
            tup=self.grdCost.GetValueStr()
            self.viSumOther.SetValue(tup[-1][1])
            fSum+=float(tup[-1][1])
            self.viSum.SetValue(fSum)
        except:
            self.__logTB__()
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if node is None:
                return
            # add code here
            for kind,obj in [('personnel',self.grdPers),
                            ('otherCost',self.grdCost),
                            ('inkindIndustrial',self.grdInKindIndust),
                            ('inkindScientific',self.grdInKindScience),
                            ]:
                tup=obj.GetValueStr()
                self.objRegNode.SetValues(node,tup[0],tup[1],kind=kind)
                obj.SetModified(False)
            self.viCostAct.GetNode(node)
            self.viCostCalc.GetNode(node)
            self.GetNodeFin(node)
        except:
            self.__logTB__()
    def GetSum(self):
        tup=self.grdPers.GetValueStr()
        return tup[1][-1]
    def Close(self):
        vtXmlNodePanel.Close(self)
        # add code here
    def Cancel(self):
        vtXmlNodePanel.Cancel(self)
        # add code here
    def Lock(self,flag):
        vtXmlNodePanel.Lock(self,flag)
        if flag:
            # add code here
            self.grdPers.Enable(False)
        else:
            # add code here
            self.grdPers.Enable(True)
    def BindPartsChanged(self,func):
        self.viParts.Bind(vidarc.tool.input.vtInputGridSetup.vEVT_VTINPUT_GRIDSETUP_INFO_CHANGED,
              func,self.grdPers)

    def OnViPartsVtinputGridsetupInfoChanged(self, event):
        event.Skip()
        #vtLog.CallStack('')

    def OnCbUpdateButton(self, event):
        event.Skip()
        try:
            #self.grdPers.Get()
            
            lDate=None
            dFunc=None
            dFuncVol=None
            dWrkPkgCost=None
            fidAct=self.objRegNode.GetCostActualID(self.node)
            fidCalc=self.objRegNode.GetCostCalculationID(self.node)
            dtStart=vtDateTime(True)
            dtEnd=vtDateTime(True)
            dtTmp=vtDateTime(True)
            #vtLog.CallStack('')
            #print fidAct,fidCalc
            if self.node is not None:
                node=self.doc.getParent(self.node)
                lDate=self.doc.GetDateLimit(node)
                dtStart.SetDateStr(lDate[0])
                dtEnd.SetDateStr(lDate[1])
                dFunc=self.doc.GetFuncDict(node)
                dFuncVol=self.doc.GetFuncVolumeDict(node)
                dWrkPkgCost=self.doc.GetWorkPackageCostDict(node)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'lDate:%s'%(vtLog.pformat(lDate)),self)
                    vtLog.vtLngCurWX(vtLog.DEBUG,'dFunc:%s'%(vtLog.pformat(dFunc)),self)
                    vtLog.vtLngCurWX(vtLog.DEBUG,'dFuncVol:%s'%(vtLog.pformat(dFuncVol)),self)
                    vtLog.vtLngCurWX(vtLog.DEBUG,'dWrkPkgCost:%s'%(vtLog.pformat(dWrkPkgCost)),self)
            lMon=[]
            iYrS=dtStart.GetYear()
            iMonS=dtStart.GetMonth()
            iYrE=dtEnd.GetYear()
            iMonE=dtEnd.GetMonth()
            for iYear in xrange(iYrS,iYrE+1):
                if iYear==iYrS:
                    iMonST=iMonS
                else:
                    iMonST=1
                if iYear==iYrE:
                    iMonET=iMonE
                else:
                    iMonET=12
                for iMon in xrange(iMonST,iMonET+1):
            #for iYear in xrange(dtStart.GetYear(),dtEnd.GetYear()+1):
            #    for iMon in xrange(dtStart.GetMonth(),dtEnd.GetMonth()+1):
                    dtTmp.SetDate(iYear,iMon,1)
                    s=dtTmp.GetDateStr()[:-3]
                    lMon.append(s)
            iMonCount=len(lMon)
            lCfgCostAct=[]
            nodeAct=self.doc.getNodeByIdNum(fidAct)
            if nodeAct is not None:
                try:
                    oReg=self.doc.GetRegByNode(nodeAct)
                    dCfgCostAct=oReg.GetValues(nodeAct)
                    lCostActDate,dCostActVal=oReg.GetValues4FuncDict(dCfgCostAct)
                except:
                    pass
            lCfgCostCalc=[]
            nodeCalc=self.doc.getNodeByIdNum(fidCalc)
            if nodeCalc is not None:
                try:
                    oReg=self.doc.GetRegByNode(nodeCalc)
                    dCfgCostCalc=oReg.GetValues(nodeCalc)
                    lCostCalcDate,dCostCalcVal=oReg.GetValues4FuncDict(dCfgCostCalc)
                except:
                    pass
            docGlb=self.doc.GetNetDoc('vGlobals')
            dOrder=docGlb.GetGlobalRecDict(['func'],'globalsFunc','ordKplus','function','0')
            fHrsPerMon=float(docGlb.GetGlobal(['vPrjPlan'],'globalsPrjPlan','hours','','135'))
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'dOrder:%s;lDate:%s;dFunc:%s;dFuncVol:%s;fHrsPerDay:%f'\
                        %(vtLog.pformat(dOrder),vtLog.pformat(lDate),
                        vtLog.pformat(dFunc),vtLog.pformat(dFuncVol),fHrsPerMon),self)
                vtLog.vtLngCurWX(vtLog.DEBUG,'lCostActDate:%s;dCostActVal:%s;dCfgCostAct:%s'\
                        %(vtLog.pformat(lCostActDate),vtLog.pformat(dCostActVal),
                        vtLog.pformat(dCfgCostAct)),self)
                vtLog.vtLngCurWX(vtLog.DEBUG,'lCostCalcDate:%s;dCostCalcVal:%s;dCfgCostCalc:%s'\
                        %(vtLog.pformat(lCostCalcDate),vtLog.pformat(dCostCalcVal),
                        vtLog.pformat(dCfgCostCalc)),self)
            keysFunc=dFunc.keys()
            def findStartEndIdx(sStart,sEnd,lDates):
                iStart=0
                iEnd=len(lDates)
                i=0
                for sDate in lDates:
                    if sDate<sStart:
                        iStart=i
                    if sDate>sEnd:
                        iEnd=i
                        break
                    i+=1
                return iStart,iEnd
            def findNextIdx(iStart,iEnd,sMon,lDates):
                while iStart<iEnd:
                    if sMon<lDates[iStart]:
                        return iStart
                    iStart+=1
                return iStart
            lang=self.doc.GetLang()
            lValues=[]
            lFuncOrder=[(v[1],v[0],k) for k,v in dOrder.items()]
            lFuncOrder.sort()
            fSum=0.0
            for iOrder,sTag,iFID in lFuncOrder:
                if iFID in dFunc:
            #for iFID in keysFunc:
                    lVal=[]
                    fid='%08d@vGlobals'%iFID
                    #netGlb,node=self.doc.GetNode(fid)
                    #o=netGlb.GetRegByNode(node)
                    #sName=o.GetName(node,lang)
                    l=dFunc[iFID]
                    lVal.append('.'.join([sTag,l[1]]))
                    #lVal.append(l[1])
                    l=dFuncVol[iFID]
                    iVolume=int(round(l[0]))
                    lVal.append(str(iVolume))
                    #fVolumeMon=(round(l[0])/float(iMonCount))*fHrsPerMon
                    fVolumeMon=(l[0]/float(iMonCount))*fHrsPerMon
                    fCalc=0.0
                    if iFID in dCostCalcVal:
                        lCostVal=[float(s) for s in dCostCalcVal[iFID]]
                        iStart,iEnd=findStartEndIdx(lDate[0],lDate[1],lCostCalcDate)
                        iEnd-=1
                        for sMon in lMon:
                            iStart=findNextIdx(iStart,iEnd,sMon,lCostCalcDate)
                            fCalc+=fVolumeMon*lCostVal[iStart]
                    lVal.append(str(fCalc))
                    fSum+=fCalc
                    fAct=0.0
                    if iFID in dCostActVal:
                        lCostVal=[float(s) for s in dCostActVal[iFID]]
                        iStart,iEnd=findStartEndIdx(lDate[0],lDate[1],lCostActDate)
                        iEnd-=1
                        for sMon in lMon:
                            iStart=findNextIdx(iStart,iEnd,sMon,lCostActDate)
                            fAct+=fVolumeMon*lCostVal[iStart]
                    lVal.append(str(fAct))
                    lVal.append(str(int(l[0]*fHrsPerMon)))
                    lVal.append(fid)
                    lValues.append(lVal)
                    #self.objRegNode.SetRowColVal(self.dCfg,iFID,tup[0],val)
            self.grdPers.SetValue(lValues)
            
            # calculate other costs
            dOrder=docGlb.GetGlobalRecDict(['vPrjPlan'],'globalsPrjPlan','ordKplus',None,'0')
            lCostOrder=[(v[1],v[0],k) for k,v in dOrder.items()]
            lCostOrder.sort()
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'dOrder:%s;dWrkPkgCost:%s'\
                        %(vtLog.pformat(dOrder),vtLog.pformat(dWrkPkgCost)),self)
            lValues=[]
            for iOrder,sTag,iFID in lCostOrder:
                if iFID in dWrkPkgCost:
                    fid='%08d@vGlobals'%iFID
                    lVal=[]
                    l=dWrkPkgCost[iFID]
                    lVal.append('.'.join([sTag,l[1][1]]))
                    lVal.append('%4.2f'%l[0])
                    fSum+=l[0]
                    lVal.append(fid)
                    lValues.append(lVal)
            self.grdCost.SetValue(lValues)
            
            dOrder=docGlb.GetGlobalRecDict(['vPrjPlan'],'globalsPrjPlan','ordKplus','costInKind','0')
            lCostOrder=[(v[1],v[0],k) for k,v in dOrder.items()]
            lCostOrder.sort()
            fPer=float(self.spInKindIndust.GetValue())/100.0
            fVal=fSum*fPer
            self.txtInKindIndust.SetValue('%4.2f'%(fVal))
            lValues=[]
            for iOrder,sTag,iFID in lCostOrder:
                fid='%08d@vGlobals'%iFID
                netGlb,node=self.doc.GetNode(fid)
                o=netGlb.GetRegByNode(node)
                sName=o.GetName(node,lang)
                lValues.append([sName,'%4.2f'%(fVal),fid])
                fVal=0.0
            self.grdInKindIndust.SetValue(lValues)
            
            fPer=float(self.spInKindScience.GetValue())/100.0
            fVal=fSum*fPer
            self.txtInKindScience.SetValue('%4.2f'%(fVal))
            lValues=[]
            for iOrder,sTag,iFID in lCostOrder:
                fid='%08d@vGlobals'%iFID
                netGlb,node=self.doc.GetNode(fid)
                o=netGlb.GetRegByNode(node)
                sName=o.GetName(node,lang)
                lValues.append([sName,'%4.2f'%(fVal),fid])
                fVal=0.0
            self.grdInKindScience.SetValue(lValues)
        except:
            self.__logTB__()
        self.__showSum__()
