#Boa:FramePanel:vXmlNodeWorkPackageVolumeWorkPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeWorkPackageVolumeWorkPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060803
# CVS-ID:       $Id: vXmlNodeWorkPackageVolumeWorkPanel.py,v 1.5 2013/06/03 06:11:57 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputFloat
import vidarc.tool.input.vtInputMultipleTree
import vidarc.tool.input.vtInputDistributeTree
import vidarc.vApps.vGlobals.vXmlGlobalsFunctionDistribute

import sys

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_VXMLNODEWORKPACKAGEVOLUMEWORKPANEL, 
 wxID_VXMLNODEWORKPACKAGEVOLUMEWORKPANELLBLVOLUMNE, 
 wxID_VXMLNODEWORKPACKAGEVOLUMEWORKPANELVIFUNC, 
 wxID_VXMLNODEWORKPACKAGEVOLUMEWORKPANELVIVOLUMNE, 
] = [wx.NewId() for _init_ctrls in range(4)]

class vXmlNodeWorkPackageVolumeWorkPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsVolumne, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.viFunc, 0, border=0, flag=wx.EXPAND)

    def _init_coll_bxsVolumne_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblVolumne, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viVolumne, 2, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)
        parent.AddGrowableCol(0)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=0, rows=4, vgap=4)

        self.bxsVolumne = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsVolumne_Items(self.bxsVolumne)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEWORKPACKAGEVOLUMEWORKPANEL,
              name=u'vXmlNodeWorkPackageVolumeWorkPanel', parent=prnt,
              pos=wx.Point(0, 0), size=wx.Size(300, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(292, 180))
        self.SetAutoLayout(True)

        self.lblVolumne = wx.StaticText(id=wxID_VXMLNODEWORKPACKAGEVOLUMEWORKPANELLBLVOLUMNE,
              label=_(u'volume [MM]'), name=u'lblVolumne', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(93, 22), style=wx.ALIGN_RIGHT)
        self.lblVolumne.SetMinSize(wx.Size(-1, -1))

        self.viFunc = vidarc.vApps.vGlobals.vXmlGlobalsFunctionDistribute.vXmlGlobalsFunctionDistribute(id=wxID_VXMLNODEWORKPACKAGEVOLUMEWORKPANELVIFUNC,
              name=u'viFunc', parent=self, pos=wx.Point(0, 26),
              size=wx.Size(292, 154), style=0)

        self.viVolumne = vidarc.tool.input.vtInputFloat.vtInputFloat(default=0.0,
              fraction_width=1,
              id=wxID_VXMLNODEWORKPACKAGEVOLUMEWORKPANELVIVOLUMNE,
              integer_width=4, maximum=10000.0, minimum=0.0, name=u'viVolumne',
              parent=self, pos=wx.Point(97, 0), size=wx.Size(194, 22), style=0)
        self.viVolumne.Bind(vidarc.tool.input.vtInputFloat.vEVT_VTINPUT_FLOAT_CHANGED,
              self.OnViVolumneVtinputFloatChanged,
              id=wxID_VXMLNODEWORKPACKAGEVOLUMEWORKPANELVIVOLUMNE)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vPrjPlan')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.viVolumne.SetTagName('volume')
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def ClearInt(self):
        try:
            vtXmlNodePanel.ClearInt(self)
            # add code here
            self.viFunc.Clear()
            self.viVolumne.Clear()
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def SetNetDocs(self,d):
        if d.has_key('vHum'):
            dd=d['vHum']
            self.docHum=dd['doc']
        # add code here
        if d.has_key('vGlobals'):
            dd=d['vGlobals']
            self.viFunc.SetDocTree(dd['doc'],dd['bNet'])
    def SetDoc(self,doc,bNet=False):
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        
        # add code here
        self.viFunc.SetDoc(doc)
        
    def SetNode(self,node):
        try:
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            
            # add code here
            self.viVolumne.SetNode(self.node,self.doc)
            self.viFunc.SetLimit(self.viVolumne.GetValue())
            self.viFunc.SetNode(self.node)
            #oTrans=self.doc.GetRegisteredNode(self.objRegNode.GetTagNameTransition())
            
        except:
            self.__logTB__()
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if node is None:
                return
            # add code here
            self.viVolumne.GetNode(node,self.doc)
            self.viFunc.GetNode(node)
            self.GetNodeFin(node)
        except:
            self.__logTB__()
    def Close(self):
        vtXmlNodePanel.Close(self)
        # add code here
        pass
    def Cancel(self):
        vtXmlNodePanel.Cancel(self)
        # add code here
        pass
    def Lock(self,flag):
        vtXmlNodePanel.Lock(self,flag)
        if flag:
            # add code here
            pass
        else:
            # add code here
            pass

    def OnViVolumneVtinputFloatChanged(self, event):
        event.Skip()
        try:
            fLimit=self.viVolumne.GetValue()
            self.viFunc.SetLimit(fLimit)
        except:
            vtLog.vtLngTB(self.GetName())
