#Boa:FramePanel:vXmlNodeWorkPackagePanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeWorkPackagePanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      
# CVS-ID:       $Id: vXmlNodeWorkPackagePanel.py,v 1.6 2010/03/03 02:17:15 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputTextMultiLineML
import vidarc.vApps.vHum.vXmlHumInputTreeUsr
import vidarc.tool.input.vtInputFloat
import vidarc.tool.time.vtTimeInputDateTime
import wx.lib.intctrl
#from vidarc.tool.net.vNetXmlWxGui import *
#import vidarc.tool.xml.vtXmlDom as vtXmlDom

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

import vidarc.tool.art.vtColor as vtColor

import sys

[wxID_VXMLNODEWORKPACKAGEPANEL, wxID_VXMLNODEWORKPACKAGEPANELCHCSTATUS, 
 wxID_VXMLNODEWORKPACKAGEPANELGAGPROG, wxID_VXMLNODEWORKPACKAGEPANELLBLEND, 
 wxID_VXMLNODEWORKPACKAGEPANELLBLIDX, wxID_VXMLNODEWORKPACKAGEPANELLBLMNG, 
 wxID_VXMLNODEWORKPACKAGEPANELLBLPROG, wxID_VXMLNODEWORKPACKAGEPANELLBLSTART, 
 wxID_VXMLNODEWORKPACKAGEPANELLBLSTATUS, wxID_VXMLNODEWORKPACKAGEPANELVIIDX, 
 wxID_VXMLNODEWORKPACKAGEPANELVIPROG, wxID_VXMLNODEWORKPACKAGEPANELVIUSR, 
 wxID_VXMLNODEWORKPACKAGEPANELVTTEND, wxID_VXMLNODEWORKPACKAGEPANELVTTSTART, 
] = [wx.NewId() for _init_ctrls in range(14)]

class vXmlNodeWorkPackagePanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblIdx, (0, 0), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.viIdx, (0, 1), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.lblProg, (1, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viProg, (1, 1), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.gagProg, (2, 0), border=0, flag=wx.EXPAND,
              span=(1, 2))
        parent.AddWindow(self.lblStatus, (1, 2), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.chcStatus, (1, 3), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.lblStart, (3, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.vttStart, (3, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.lblEnd, (3, 2), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.vttEnd, (3, 3), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.lblMng, (4, 0), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.viUsr, (4, 1), border=0, flag=wx.EXPAND, span=(1,
              1))

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=4, vgap=4)

        self._init_coll_gbsData_Items(self.gbsData)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEWORKPACKAGEPANEL,
              name=u'vXmlNodeWorkPackagePanel', parent=prnt, pos=wx.Point(264,
              88), size=wx.Size(306, 164),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(298, 137))
        self.SetAutoLayout(True)

        self.lblIdx = wx.StaticText(id=wxID_VXMLNODEWORKPACKAGEPANELLBLIDX,
              label=_(u'Index'), name=u'lblIdx', parent=self, pos=wx.Point(0,
              0), size=wx.Size(50, 22), style=wx.ALIGN_RIGHT)
        self.lblIdx.SetMinSize(wx.Size(-1, -1))

        self.viIdx = vidarc.tool.input.vtInputFloat.vtInputFloat(default=0.0,
              fraction_width=0, id=wxID_VXMLNODEWORKPACKAGEPANELVIIDX,
              integer_width=4, maximum=100.0, minimum=0.0, name=u'viIdx',
              parent=self, pos=wx.Point(54, 0), size=wx.Size(106, 22), style=0)

        self.lblProg = wx.StaticText(id=wxID_VXMLNODEWORKPACKAGEPANELLBLPROG,
              label=_(u'Progress'), name=u'lblProg', parent=self,
              pos=wx.Point(0, 26), size=wx.Size(50, 22), style=wx.ALIGN_RIGHT)
        self.lblProg.SetMinSize(wx.Size(-1, -1))

        self.viProg = vidarc.tool.input.vtInputFloat.vtInputFloat(default=0.0,
              fraction_width=1, id=wxID_VXMLNODEWORKPACKAGEPANELVIPROG,
              integer_width=3, maximum=100.0, minimum=0.0, name=u'viProg',
              parent=self, pos=wx.Point(54, 26), size=wx.Size(106, 22),
              style=0)
        self.viProg.Bind(vidarc.tool.input.vtInputFloat.vEVT_VTINPUT_FLOAT_CHANGED,
              self.OnViProgVtinputFloatChanged,
              id=wxID_VXMLNODEWORKPACKAGEPANELVIPROG)

        self.lblStatus = wx.StaticText(id=wxID_VXMLNODEWORKPACKAGEPANELLBLSTATUS,
              label=_(u'Status'), name=u'lblStatus', parent=self,
              pos=wx.Point(164, 26), size=wx.Size(31, 22),
              style=wx.ALIGN_RIGHT)
        self.lblStatus.SetMinSize(wx.Size(-1, -1))

        self.chcStatus = wx.Choice(choices=[_(u'green'), _(u'yellow'),
              _(u'red')], id=wxID_VXMLNODEWORKPACKAGEPANELCHCSTATUS,
              name=u'chcStatus', parent=self, pos=wx.Point(199, 26),
              size=wx.Size(88, 21), style=0)
        self.chcStatus.SetMinSize(wx.Size(-1, -1))
        self.chcStatus.Bind(wx.EVT_CHOICE, self.OnChcStatusChoice,
              id=wxID_VXMLNODEWORKPACKAGEPANELCHCSTATUS)

        self.gagProg = wx.Gauge(id=wxID_VXMLNODEWORKPACKAGEPANELGAGPROG,
              name=u'gagProg', parent=self, pos=wx.Point(0, 52), range=1000,
              size=wx.Size(160, 13), style=wx.GA_SMOOTH | wx.GA_HORIZONTAL)
        self.gagProg.SetMinSize(wx.Size(-1, 13))
        self.gagProg.SetMaxSize(wx.Size(-1, 13))

        self.lblStart = wx.StaticText(id=wxID_VXMLNODEWORKPACKAGEPANELLBLSTART,
              label=_(u'Start'), name=u'lblStart', parent=self, pos=wx.Point(0,
              76), size=wx.Size(50, 24), style=wx.ALIGN_RIGHT)
        self.lblStart.SetMinSize(wx.Size(-1, -1))

        self.vttStart = vidarc.tool.time.vtTimeInputDateTime.vtTimeInputDateTime(id=wxID_VXMLNODEWORKPACKAGEPANELVTTSTART,
              name=u'vttStart', parent=self, pos=wx.Point(54, 76),
              size=wx.Size(106, 24), style=0)

        self.lblEnd = wx.StaticText(id=wxID_VXMLNODEWORKPACKAGEPANELLBLEND,
              label=_(u'End'), name=u'lblEnd', parent=self, pos=wx.Point(164,
              76), size=wx.Size(31, 24), style=wx.ALIGN_RIGHT)
        self.lblEnd.SetMinSize(wx.Size(-1, -1))

        self.vttEnd = vidarc.tool.time.vtTimeInputDateTime.vtTimeInputDateTime(id=wxID_VXMLNODEWORKPACKAGEPANELVTTEND,
              name=u'vttEnd', parent=self, pos=wx.Point(199, 76),
              size=wx.Size(88, 24), style=0)

        self.lblMng = wx.StaticText(id=wxID_VXMLNODEWORKPACKAGEPANELLBLMNG,
              label=_(u'Manager'), name=u'lblMng', parent=self, pos=wx.Point(0,
              104), size=wx.Size(50, 24), style=wx.ALIGN_RIGHT)
        self.lblMng.SetMinSize(wx.Size(-1, -1))

        self.viUsr = vidarc.vApps.vHum.vXmlHumInputTreeUsr.vXmlHumInputTreeUsr(id=wxID_VXMLNODEWORKPACKAGEPANELVIUSR,
              name=u'viUsr', parent=self, pos=wx.Point(54, 104),
              size=wx.Size(106, 24), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vPrjPlan')
        self._init_ctrls(parent)
        self.selIdx=-1
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        
        #self.bModified=False
        #self.bAutoApply=False
        #self.bBlock=False
        
        self.vttStart.SetTagNames('dateTimeStart')
        self.vttEnd.SetTagNames('dateTimeEnd')
        self.viProg.SetTagName('progress')
        self.viIdx.SetTagName('idx')
        self.viUsr.SetTagNames('manager','name')
        self.vttStart.SetDftTime('00:00')
        self.vttEnd.SetDftTime('23:55')
        
        self.gbsData.AddGrowableCol(0)
        self.gbsData.AddGrowableCol(1)
        self.gbsData.AddGrowableCol(2)
        self.gbsData.AddGrowableCol(3)
        
        self.Move(pos)
        self.SetSize(size)
    def ClearWid(self):
        vtXmlNodePanel.ClearWid(self)
        # add code here
        self.gagProg.SetValue(0)
    def SetNetDocs(self,d):
        # add code here
        if d.has_key('vHum'):
            dd=d['vHum']
            self.viUsr.SetDocTree(dd['doc'],dd['bNet'])
        
    def SetDoc(self,doc,bNet=False):
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        # add code here
        self.vttStart.SetDoc(doc)
        self.vttEnd.SetDoc(doc)
        self.viUsr.SetDoc(doc)
        
    #def SetAutoApply(self,flag):
    #    self.bAutoApply=flag
    def __clearBlock__(self):
        self.bBlock=False
    def SetModifiedOld(self,state,obj=None):
        if state:
            if obj is None:
                for obj in self.lstMarkObjs:
                    f=obj.GetFont()
                    f.SetWeight(wx.FONTWEIGHT_BOLD)
                    obj.SetFont(f)
                    obj.Refresh()
            else:
                f=obj.GetFont()
                f.SetWeight(wx.FONTWEIGHT_BOLD)
                obj.SetFont(f)
                obj.Refresh()
        else:
            if obj is None:
                for obj in self.lstMarkObjs:
                    f=obj.GetFont()
                    f.SetWeight(wx.FONTWEIGHT_NORMAL)
                    obj.SetFont(f)
                    obj.Refresh()
                #self.viHum.__markModified__(False)
            else:
                f=obj.GetFont()
                f.SetWeight(wx.FONTWEIGHT_NORMAL)
                obj.SetFont(f)
                obj.Refresh()
        self.bModified=state    
    def GetModifiedOld(self):
        return self.bModified
    def __isModified__Old(self):
        if self.bModified:
            return True
        return False
    def ClearOld(self):
        self.bBlock=True
        self.SetModified(False)
        vtXmlDomConsumer.vtXmlDomConsumer.Clear(self)
        self.selIdx=-1
        wx.CallAfter(self.__clearBlock__)
    def SetNetDocHuman(self,doc,bNet=False):
        self.viUsr.SetDocTree(doc,bNet)
    def SetNode(self,node):
        #if self.bModified==True:
        #    if self.bAutoApply:
        #        self.GetNode(self.node)
        #    else:
                # ask
        #        dlg=wx.MessageDialog(self,u'Do you to apply modified data?' ,
        #                    _(u'vLog Info'),
        #                    wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
        #        if dlg.ShowModal()==wx.ID_YES:
        #            self.GetNode(self.node)
        try:
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            # add code here
            #self.bBlock=True
            #self.SetModified(False)
            #vtXmlDomConsumer.vtXmlDomConsumer.SetNode(self,node)
            
            self.viIdx.SetNode(self.node,self.doc)
            
            sStatus=self.doc.getNodeText(self.node,'status')
            try:
                idx=['green','yellow','red'].index(sStatus)
            except:
                idx=0
            self.chcStatus.SetSelection(idx)
            
            self.vttStart.SetNode(self.node)
            self.vttEnd.SetNode(self.node)
            
            self.viProg.SetNode(self.node,self.doc)
            self.viUsr.SetNode(self.node)
            self.gagProg.SetValue(int(self.viProg.GetValue()*10))
            
            wx.CallAfter(self.OnChcStatusChoice,None)
            #wx.CallAfter(self.__clearBlock__)
        except:
            self.__logTB__()
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if node is None:
                return
            # add code here
            self.viIdx.GetNode(node,self.doc)
            
            idx=self.chcStatus.GetSelection()
            try:
                sStatus=['green','yellow','red'][idx]
            except:
                sStatus='green'
            sStatus=self.doc.setNodeText(self.node,'status',sStatus)
            
            self.vttStart.GetNode(node)
            self.vttEnd.GetNode(node)
            self.viProg.GetNode(node,self.doc)
            self.viUsr.GetNode(node)
            
            self.GetNodeFin(node)
        except:
            self.__logTB__()
    def Apply(self,bDoApply=False):
        if vtXmlNodePanel.Apply(self,bDoApply=False)==False:
            return False
        # add code here
        return False
    def Close(self):
        vtXmlNodePanel.Close(self)
        # add code here
    def Cancel(self):
        vtXmlNodePanel.Cancel(self)
        # add code here
        #self.SetModified(False)
    def OnChcStatusChoice(self, event):
        try:
            color=[vtColor.GREEN4,vtColor.YELLOW2,vtColor.RED3][self.chcStatus.GetSelection()]
        except:
            color=vtColor.GREEN
        self.chcStatus.SetBackgroundColour(color)
        self.chcStatus.Refresh()
        if event is not None:
            event.Skip()
    def Lock(self,flag):
        vtXmlNodePanel.Lock(self,flag)
        if flag:
            # add code here
            pass
        else:
            # add code here
            pass

    def OnViProgVtinputFloatChanged(self, event):
        event.Skip()
        fVal=event.GetValue()
        self.gagProg.SetValue(int(fVal*10))
