#----------------------------------------------------------------------------
# Name:         __init__.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20051230
# CVS-ID:       $Id: __init__.py,v 1.6 2008/01/08 10:33:14 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import __config__
import images as imgPlg

VERSION=__config__.VERSION
getPluginData=imgPlg.getPluginData

PLUGABLE='vPrjPlanMDIFrame'
PLUGABLE_FRAME=True
#PLUGABLE_ACTIVE=0
PLUGABLE_SRV='vXmlPrjPlan@vXmlSrv'
DOMAINS=['planning','vPrjPlan']

__all__=['DOMAINS','VERSION','PLUGABLE','PLUGABLE_FRAME','PLUGABLE_SRV',
        'getPluginData',]

import sys
if hasattr(sys, 'importers'):
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=1
else:
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=0
