#----------------------------------------------------------------------------
# Name:         vXmlNodeWorkPackageCost.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060907
# CVS-ID:       $Id: vXmlNodeWorkPackageCost.py,v 1.3 2010/03/03 02:17:14 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase
import types
import vidarc.tool.xml.vtXmlHierarchy as vtXmlHierarchy#import getTagNames

from vidarc.tool.xml.vtXmlNodeTag import *
try:
    if vcCust.is2Import(__name__):
        from vXmlNodeWorkPackageCostPanel import vXmlNodeWorkPackageCostPanel
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeWorkPackageCost(vtXmlNodeTag):
    #NODE_ATTRS=[
    #        ('Tag',None,'tag',None),
    #        ('Name',None,'name',None),
    #    ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='WrkPkgCost'):
        global _
        _=vtLgBase.assignPluginLang('vPrjPlan')
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'work package cost')
    # ---------------------------------------------------------
    # specific
    def GetFuncTagNameByID(self,fid):
        netGlb,node=self.doc.GetNode(fid)
        if node is not None:
            return netGlb.GetTagNames(node)
        else:
            return '???'
    def GetColCount(self,dCfg):
        if 'lCol' in dCfg:
            return len(dCfg['lCol'])
        else:
            return 0
    def AddRow(self,dCfg,fid,sTag=None,sData=None):
        if sTag is None:
            sTag=self.GetFuncTagNameByID(fid)
        if type(fid)==types.StringType:
            fid,appl=self.doc.convForeign(fid)
        if 'dRowID' not in dCfg:
            dCfg['dRowID']={}
        dCfg['dRowID'][fid]=sTag
        dCfg['lRowMap']=[(v,k) for k,v in dCfg['dRowID'].items()]
        dCfg['lRowMap'].sort()
        dCfg['lRow']=[v for v,k in dCfg['lRowMap']]
        if 'dRowVal' not in dCfg:
            dCfg['dRowVal']={}
        if sData is None:
            dCfg['dRowVal'][fid]=['0.0' for i in xrange(self.GetColCount(dCfg))]
        else:
            sVals=sData.split(',')
            iCols=self.GetColCount(dCfg)
            for i in xrange(len(sVals),iCols):
                sVals.append('0.0')
            dCfg['dRowVal'][fid]=sVals[:iCols]
        dCfg['val']=[dCfg['dRowVal'][k] for v,k in dCfg['lRowMap']]
    def DelRowByIdx(self,dCfg,i):
        try:
            v,fid=dCfg['lRowMap'][i]
            del dCfg['dRowID'][fid]
            del dCfg['lRow'][i]
            del dCfg['lRowMap'][i]
            del dCfg['val'][i]
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def AddCol(self,dCfg,sDate):
        lCol=dCfg['lCol']
        if sDate in lCol:
            return lCol.index(sDate)
        iPos=-1
        for i in xrange(len(lCol)):
            if lCol[i]>sDate:
                iPos=i
                break
        if iPos==-1:
            lCol.append(sDate)
            for lVal in dCfg['val']:
                lVal.append('0.0')
            return len(lCol)-1
        else:
            lCol.insert(iPos,sDate)
            for lVal in dCfg['val']:
                lVal.insert(iPos,'0.0')
        return iPos
    def CopyCol(self,dCfg,iColSrc,iColDest,fFact=1.0):
        for lVal in dCfg['val']:
            lVal[iColDest]=str(float(lVal[iColSrc])*fFact)
    def DelColByIdx(self,dCfg,i):
        try:
            del dCfg['lCol'][i]
            for lVal in dCfg['val']:
                del lVal[i]
        except:
            pass
    def GetCol(self,dCfg,i):
        try:
            if 'lCol' in dCfg:
                return dCfg['lCol'][i]
        except:
            return None
    def GetRow(self,dCfg,i):
        try:
            return dCfg['lRowMap'][i][1]
        except:
            return None
    
    def SetRowColVal(self,dCfg,fid,sDate,val):
        lCol=dCfg['lCol']
        if sDate in lCol:
            iCol=lCol.index(sDate)
            if 'dRowVal' in dCfg:
                d=dCfg['dRowVal']
                if fid in d:
                    d[fid][iCol]=val
                    return 0
            return -1
        return -2
    def GetValues(self,node):
        lRow=[]
        dRowID={}
        lCol=[]
        lVal=[]
        dCfg={  'lRow':lRow,
                'dRowID':dRowID,
                'lRowMap':[],
                'lCol':lCol,
                'val':lVal,
            }
        try:
            nodeData=self.doc.getChild(node,'data')
            if nodeData is not None:
                lVal=self.doc.getNodeText(nodeData,'col')
                for sVal in lVal.split(','):
                    self.AddCol(dCfg,sVal)
                for cRow in self.doc.getChilds(nodeData,'row'):
                    fid=self.doc.getAttribute(cRow,'fid')
                    sVal=self.doc.getText(cRow)
                    #print fid,sVal
                    self.AddRow(dCfg,fid,sData=sVal)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return dCfg
    def SetValues(self,node,dCfg):
        if node is None:
            return
        try:
            nodeData=self.doc.getChild(node,'data')
            if nodeData is not None:
                self.doc.deleteNode(nodeData,node)
            nodeData=self.doc.getChildForced(node,'data')
            cCol=self.doc.createSubNode(nodeData,'col')
            self.doc.setText(cCol,','.join(dCfg['lCol']))
            for v,k in dCfg['lRowMap']:
                lVal=dCfg['dRowVal'][k]
                cRow=self.doc.createSubNode(nodeData,'row')
                #self.doc.setAttribute(cRow,'fid',k)
                self.doc.setForeignKey(cRow,'fid',k,'vGlobals')
                self.doc.setText(cRow,','.join(lVal))
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def GetValues4FuncDict(self,dCfg):
        d={}
        try:
            lRowMap=dCfg['lRowMap']
            i=0
            for t in lRowMap:
                fid='%08d@vGlobals'%t[1]
                netDoc,node=self.doc.GetNode(fid)
                if node is not None:
                    nodeFunc=netDoc.getParent(node)
                    d[netDoc.getKeyNum(nodeFunc)]=dCfg['val'][i]
                i+=1
            return dCfg['lCol'],d
        except:
            pass
        return None,None
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    #def GetAttrFilterTypesOld(self):
    #    """ shall return something like:
    #         [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
    #          ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
    #    """
    #    return None
    #def GetTranslation(self,name):
    #    return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsNotContained(self):
        "do not add to listbook"
        return True
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02?IDAT8\x8dm\x93\xdfKSa\x18\xc7?\xe7,\xb2\x8e\x0c\xcf\xce\xfc\xb1p\
\xb4\r\xb1\xa3\xa1)\xf3\xa2D\xb2\xa0\x0b\x87\x17\x16+\x88.\xba1\xea\xaa\xdb \
\x88.\x85\xa2\xbf@\x8d\xfd\x07A\xe9M\x05\xc6R\xa2\xa0\x06%\n;\x11m\x8au\xb2\
\xe9\xd9\x8c\xa4\xa9\xed\xbc]\x1c\xd6\xdc9>\xf0\xf2\xbc|\xf9\xbe\xcf\xf3}~\
\xbcH\xb2\x0f\xf7\th\x1fE\xd5\xef\xbf\x1f\xc4\x95$\xd9\xc7~\x8b\xc6R\xe2\xe6\
\xf87\xe6\xdf\xc8\x9c?\xeb`\xe9\x05\x18\x1e\xb2\x99N\xb5\x93\xcf\x8dK\xfb\
\xf9\x87pY\xa9\xd8\x87,\x9b\xdc\xbds\x86\xee\x13\x1d\xe4VW8\xd9m1\x97\x9e#\
\x9f\xbb/\xb9\xf9\xde\x00\xa5\x01i\xf2qJ\xf8\xe4\xf7 \xd9\x18\x86A\xd1\xdabf\
6\xee\xa6\x1e\x1c\x00 \x9f\x1b\x97\xd6\xcc\xdb\xc2\x9c1\xa9\xd8\x15|\xb2\x8f\
|\xee\x9e';\x80\xec\x06T5#F\x12\x13\x02\xa0bW\xa8\xfa\x91\xc4\x84P\xd5\x8cp\
\xf3\xeb\x9a8\x92\x98\x10\x17\xce\xf9\x08\xb5\x85him\xe1\xc3\xec4\x00\xf1\
\xd1\x1b\xec\xee\xed\x925\xb2\xa4\x17\xe0\xc5\xf3\x9a\x9a\xff%\x84\xc3S\xe2j\
R\xa6\xa3\xb3\x83\xe6\xd6f\xbe\x1b\x19z\xdb\x9e\x00\xa0HW\x08Et\xec\x8aM[\
\xcb\x16\xcbKSbm\xed\x96TWB2\xb9HSS\x13ZPc\xb7\xf0\x95p\xf9:z\x17\xe8]\x10\
\xfa}\x8d_\xa6A\xf8x\x18\xbf\xdfO2\xb9X\xdf\x83h,%\xe2\xa7\xe2hA\x8d\xa0\x16\
`e\xe9\x1d\xba\xee\x10\x8c,\xcc<\x85\xad\xd5eTMEQ\x14\xfa{\xfa\x89\xc6R\xc2\
\xd3D\xa5Q\x01@\x1f\x18\xe6\xd1C\x07\x1b\xbb\x08c\x97\x1c\xechC\x03J\xa3\x82\
,\xcb\xf5\n\xaaV.\x979\xd2p\x18#3_\xcb\xfe\x0ct\x1d\x8c\xcc<\x7fvv\xdcCp\x9a\
X*\xf6\xb1^xI$\x1a\xc1\xfcY \xd2s\x1a]\xab\x91\x0c\x03\x82\xb1^\xca\xe52\xd6\
\xa6\x85\xb9nR*&j\nJ\xa5\x01\xe9\xd5k\x1bk\xd3\xc2*X\x94\xfe\xfa\xf9\\\x99\
\xc4\xc8:*\xbe\xd8\xd3(\xad\x9dl\xfc\xd8`{{\x9b\xf4\x82\xf3\xc6\xb3\x07\xa3\
\xa3\x0f\xc4\xd0\xe0\x1e\xed\xc7\xda\x01\xc8\xbdu\xc6\x18\x1b\xbc\x0c\x80\
\xb9nz\xf6\xc0\xf3\x1bU5#\xd4\xc0'O\xad\xd5R\xab\x99\xab\xf6\x0f\x9d>\xddw*\
\xdc\xa42\x00\x00\x00\x00IEND\xaeB`\x82" 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            d=vtXmlNodeTag.GetAddDialogClass(self).copy()
            d['pnCls']=vtXmlNodeTag.GetPanelClass(self)
            return d
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            d=vtXmlNodeTag.GetAddDialogClass(self).copy()
            d['pnCls']=vtXmlNodeTag.GetPanelClass(self)
            return d
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeWorkPackageCostPanel
        else:
            return None
