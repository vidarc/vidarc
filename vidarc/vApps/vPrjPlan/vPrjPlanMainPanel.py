#Boa:FramePanel:vPrjPlanMainPanel
#----------------------------------------------------------------------------
# Name:         vPrjPlanMainPanel.py
# Purpose:      
#               derived from vPrjPlanMainFrame.py
# Author:       Walter Obweger
#
# Created:      20060606
# CVS-ID:       $Id: vPrjPlanMainPanel.py,v 1.9 2007/08/21 18:14:40 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.xml.vtXmlNodeRegListBook
import vidarc.tool.xml.vtXmlNodeTagCmdPanel
import getpass,time

import vidarc.vApps.vHum.impMin

try:
    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    import vidarc.config.vcCust as vcCust
    vcCust.set2Import('vidar.vApps.vPrjPlan',False,'__REG_ACCOUNT__')
    
    from vidarc.vApps.vPrjPlan.vXmlPrjPlanTree  import *
    from vidarc.vApps.vPrjPlan.vNetPrjPlan import *
    from vidarc.vApps.vPrjPlan.vPrjPlanListBook import *
    from vidarc.vApps.vPrjPlan.vPrjPlanDrawListBook import *
    from vidarc.vApps.vPrjPlan.vPrjPlanCanvasStructurePanel import *
    
    from vidarc.tool.net.vtNetSecXmlGuiMaster import *
    from vidarc.tool.xml.vtXmlDomConsumer import vtXmlDomConsumer
    import vidarc.tool.InOut.fnUtil as fnUtil
    
    from vidarc.vApps.vContact.vNetContact import *
    from vidarc.vApps.vPrjDoc.vNetPrjDoc import *
    from vidarc.vApps.vPrj.vNetPrj import *
    from vidarc.vApps.vDoc.vNetDoc import *
    from vidarc.vApps.vHum.vNetHum import *
    from vidarc.vApps.vGlobals.vNetGlobals import *
    from vidarc.tool.net.vtNetMsg import vtNetMsg
    
    from vidarc.tool.xml.vtXmlFindQuickThread import vtXmlFindQuickThread
    from vidarc.tool.xml.vtXmlFindQuickThread import EVT_VTXML_FIND_QUICK_FOUND
except:
    vtLog.vtLngTB('import')

VERBOSE=0

def create(parent):
    return vPrjPlanMainPanel(parent)

[wxID_VPRJPLANMAINPANEL, wxID_VPRJPLANMAINPANELPNDATA, 
 wxID_VPRJPLANMAINPANELSLWNAV, wxID_VPRJPLANMAINPANELSLWTOP, 
 wxID_VPRJPLANMAINPANELVITAG, 
] = [wx.NewId() for _init_ctrls in range(5)]

STATUS_TEXT_POS=2
STATUS_LOG_POS=3
STATUS_CLK_POS=4

import vidarc.vApps.vPrjPlan.images as imgPrjPlan

def getPluginImage():
    return imgPrjPlan.getPluginImage()

[wxID_VGFPRJTIMERMAINTOOLBAR1TOOLS0] = [wx.NewId() for _init_coll_toolBar1_Tools in range(1)]

[wxID_VPRJPLANMAINFRAMEMNVIEWITEM_LANG, 
 wxID_VPRJPLANMAINFRAMEMNVIEWITEM_VIEW_TREE, 
] = [wx.NewId() for _init_coll_mnView_Items in range(2)]

[wxID_VPRJPLANMAINFRAMEMNLANGITEM_LANG_DE, 
 wxID_VPRJPLANMAINFRAMEMNLANGITEM_LANG_EN, 
] = [wx.NewId() for _init_coll_mnLang_Items in range(2)]

[wxID_VPRJPLANMAINFRAMEMNHELPMN_HELP_ABOUT, 
 wxID_VPRJPLANMAINFRAMEMNHELPMN_HELP_HELP, 
 wxID_VPRJPLANMAINFRAMEMNHELPMN_HELP_LOG, 
] = [wx.NewId() for _init_coll_mnHelp_Items in range(3)]

[wxID_VPRJPLANMAINFRAMEMNANALYSEMN_ANALYSE_BUILD, 
 wxID_VPRJPLANMAINFRAMEMNANALYSEMN_ANALYSE_CALC_DATE, 
] = [wx.NewId() for _init_coll_mnAnalyse_Items in range(2)]

class vPrjPlanMainPanel(wx.Panel,vtXmlDomConsumer):
    def _init_sizers(self):
        # generated method, don't edit
        self.bxsData = wx.BoxSizer(orient=wx.VERTICAL)

        self.pnData.SetSizer(self.bxsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VPRJPLANMAINPANEL,
              name=u'vPrjPlanMainPanel', parent=prnt, pos=wx.Point(115, 10),
              size=wx.Size(819, 516), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(811, 489))
        self.Bind(wx.EVT_SIZE, self.OnSize)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VPRJPLANMAINPANELSLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 0), size=wx.Size(200,
              489), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetDefaultSize(wx.Size(200, 1000))
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetAutoLayout(True)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetLabel(u'navigation')
        self.slwNav.SetToolTipString(u'navigation')
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VPRJPLANMAINPANELSLWNAV)

        self.slwTop = wx.SashLayoutWindow(id=wxID_VPRJPLANMAINPANELSLWTOP,
              name=u'slwTop', parent=self, pos=wx.Point(200, 0),
              size=wx.Size(608, 104), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwTop.SetDefaultSize(wx.Size(1000, 108))
        self.slwTop.SetAlignment(wx.LAYOUT_TOP)
        self.slwTop.SetAutoLayout(True)
        self.slwTop.SetSashVisible(wx.SASH_BOTTOM, True)
        self.slwTop.SetOrientation(wx.LAYOUT_HORIZONTAL)
        self.slwTop.SetLabel(u'information')
        self.slwTop.SetToolTipString(u'information')
        self.slwTop.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwTopSashDragged,
              id=wxID_VPRJPLANMAINPANELSLWTOP)

        self.pnData = wx.Panel(id=wxID_VPRJPLANMAINPANELPNDATA, name=u'pnData',
              parent=self, pos=wx.Point(208, 112), size=wx.Size(592, 376),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)
        self.pnData.SetAutoLayout(True)

        self.viTag = vidarc.tool.xml.vtXmlNodeTagCmdPanel.vtXmlNodeTagCmdPanel(applName='vtXmlNodeTagCmdPanel',
              id=wxID_VPRJPLANMAINPANELVITAG, name=u'viTag', parent=self.slwTop,
              pos=wx.Point(0, 0), size=wx.Size(608, 101), style=0)
        self.viTag.Bind(vidarc.tool.xml.vtXmlNodeTagCmdPanel.vEVT_TOOL_XML_TAG_CANCEL,
              self.OnViTagToolXmlTagCancel, id=wxID_VPRJPLANMAINPANELVITAG)
        self.viTag.Bind(vidarc.tool.xml.vtXmlNodeTagCmdPanel.vEVT_TOOL_XML_TAG_APPLY,
              self.OnViTagToolXmlTagApply, id=wxID_VPRJPLANMAINPANELVITAG)
        self.viTag.Bind(vidarc.tool.xml.vtXmlNodeTagCmdPanel.vEVT_TOOL_XML_TAG_SELECTED,
              self.OnViTagToolXmlTagSelected, id=wxID_VPRJPLANMAINPANELVITAG)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vPrjPlan')
        vtXmlDomConsumer.__init__(self)
        self._init_ctrls(parent)
        self.SetName(name)
        self.bActivated=False
        self.dCfg={}
        self.cfgFunc=None
        self.tbMain=None
        
        self.nodeCurStarted=None
        self.nodeCurStartedTreeItem=None
        self.nodeLock=None
        
        self.baseDoc=None
        self.baseSettings=None
        
        self.xdCfg=vtXmlDom(appl='vPrjPlanCfg',audit_trail=False)
        
        appls=['vPrjPlan','vContact','vPrjDoc','vPrj','vDoc','vHum','vGlobals','vMsg']
        
        rect = parent.sbStatus.GetFieldRect(0)
        self.netMaster=vtNetSecXmlGuiMaster(parent.sbStatus,'Master',pos=(rect.x+2, rect.y+2),
                    size=(rect.width-4, rect.height-4),verbose=1)
        
        self.netPrjPlan=self.netMaster.AddNetControl(vNetPrjPlan,'vPrjPlan',synch=True,verbose=0,
                                                audit_trail=True)
        self.netContact=self.netMaster.AddNetControl(vNetContact,'vContact',synch=True,verbose=0)
        self.netPrjDoc=self.netMaster.AddNetControl(vNetPrjDoc,'vPrjDoc',synch=True,verbose=0)
        self.netPrj=self.netMaster.AddNetControl(vNetPrj,'vPrj',synch=True,verbose=0)
        self.netDoc=self.netMaster.AddNetControl(vNetDoc,'vDoc',synch=True,verbose=0)
        self.netHum=self.netMaster.AddNetControl(vNetHum,'vHum',synch=True,verbose=0)
        self.netGlb=self.netMaster.AddNetControl(vNetGlobals,'vGlobals',synch=True,verbose=0)
        self.netMsg=self.netMaster.AddNetControl(vtNetMsg,'vMsg',synch=True,verbose=0)
        self.netMaster.AddNetControl(None,None,verbose=0)
        self.netMaster.SetLang(lang=vtLgBase.getPluginLang())
        
        self.netMaster.SetCfg(self.xdCfg,[],appls,'vPrjPlanAppl')
        self.bAutoConnect=True
        self.OpenCfgFile()
        
        self.vgpTree=vXmlPrjPlanTree(self.slwNav,wx.NewId(),
                pos=(0,0),size=(198,489),style=0,name="vgpTree",
                master=True,verbose=0)
        #self.vgpTree.SetConstraints(
        #    anchors.LayoutAnchors(self.vgpTree, True, True, True, True)
        #    )
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        self.vgpTree.SetDoc(self.netPrjPlan,True)
        
        oDoc=self.netPrjPlan.GetReg('Doc')
        oDoc.SetTreeClass(vXmlPrjPlanTree)
        
        self.nbData=vidarc.tool.xml.vtXmlNodeRegListBook.vtXmlNodeRegListBook(self.pnData,wx.NewId(),
                pos=wx.DefaultPosition,size=wx.DefaultSize,style=0,name="nbPrjPlan")
        self.bxsData.AddWindow(self.nbData, 1, border=4, flag=wx.EXPAND | wx.ALL)
        self.nbData.Show(False)
        self.nbData.SetDoc(self.netPrjPlan,True)
        oWkgPkg=self.netPrjPlan.GetReg('WorkPackage')
        
        #self.nbData.SetNetDocHuman(self.netHum,True)
        oDoc=self.netPrjPlan.GetReg('Doc')
        oDoc.SetTreeClass(vXmlPrjPlanTree)
        pnCls=oDoc.GetPanelClass()
        pn=pnCls(self.pnData,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="pnDoc")
        pn.SetRegNode(oDoc)
        pn.SetDoc(self.netPrjPlan,True)
        self.viTag.AddWidgetDependent(pn,oDoc)
        pn.Show(False)
        self.bxsData.AddWindow(pn, 1, border=4, flag=wx.ALL | wx.EXPAND)
        
        for sTag in ['VolWrkDist','CostDefine','WrkPkgVolWrk','KplusShort',
                'WrkPkgCost','WrkPkgMileStone']:
            pn=self.viTag.CreateRegisteredPanel(sTag,
                            self.netPrjPlan,True,
                            self.pnData)
            pn.Show(False)
            self.bxsData.AddWindow(pn, 1, border=4, flag=wx.ALL | wx.EXPAND)
        
        #self.viTag.AddWidgetDependent(self.nbData,None)
        #self.viTag.AddWidgetDependent(self.nbData,oDoc)
        oDataCollector=self.netPrjPlan.GetReg('dataCollector')
        self.viTag.AddWidgetDependent(None,oDataCollector)
        self.viTag.AddWidgetDependent(self.nbData,oWkgPkg)
        self.viTag.SetDoc(self.netPrjPlan,True)
        self.viTag.SetRegNode(oWkgPkg,bIncludeBase=True)
        self.viTag.SetNetDocs(self.netPrjPlan.GetNetDocs())
        self.viTag.SetRegNode(oDoc)

        if 0:
            if 1:
                self.nbDraw=vPrjPlanDrawListBook(self.pnData,wx.NewId(),
                        pos=(4,4),size=(580, 220),style=0,name="nbPrjPlanDraw")
                self.nbDraw.SetConstraints(
                    anchors.LayoutAnchors(self.nbDraw, True, True, True, True)
                    )
                self.nbDraw.SetDoc(self.netPrjPlan,True)
                #self.nbDraw.SetNetDocHuman(self.netHum,True)
            else:
                self.drStruct=vPrjPlanCanvasStructurePanel(self.pnData,wx.NewId(),
                        pos=(4,4),size=(580, 220),style=0,name="nbPrjPlanDraw")
                self.drStruct.SetConstraints(
                    anchors.LayoutAnchors(self.drStruct, True, True, True, True)
                    )
                self.drStruct.SetDoc(self.netPrjPlan,True)
            
        self.thdFindQuick=vtXmlFindQuickThread(self.vgpTree,bPost=True,origin='vContact:thdFindQuick')
        EVT_VTXML_FIND_QUICK_FOUND(self.vgpTree,self.OnFindQuickFound)
        self.idQuickFoundLast=-1
        
        self.SetSize(size)
        #self.CreateNew()
    def GetDocMain(self):
        return self.netPrjPlan
    def GetNetMaster(self):
        return self.netMaster
    def GetTreeMain(self):
        return self.vgpTree
    def PopulateToolBar(self,parent):
        id=wx.NewId()
        sz=self.GetClientSize()
        self.tbMain = wx.ToolBar(id=id,
                name=u'tbContact', parent=parent, pos=wx.Point(0,0),
                size=wx.DefaultSize, style=wx.TB_HORIZONTAL | wx.NO_BORDER)
        self.tbMain.SetToolBitmapSize(wx.Size(20,20))
        self.dToolBar={}
        self.vgpTree.PopulateToolBar(parent,self.tbMain,self.dToolBar)
        #self.tbMain.AddSimpleTool(id, vtArt.getBitmap(vtArt.Duplicate), _(u"Duplicate"), 
        #                _(u"duplicate"))
        #self.Bind(wx.EVT_TOOL, self.OnToolDupRecipeClick, id=id)
        self.tbMain.AddSeparator()
        id=wx.NewId()
        self.dToolBar['quickFind']=id
        txtCtrl=wx.TextCtrl(self.tbMain, id, "", size=(150, -1))
        self.tbMain.AddControl(txtCtrl)
        self.tbMain.Bind(wx.EVT_TEXT, self.OnQuickFind, id=id)
        self.tbMain.Bind(wx.EVT_TEXT_ENTER, self.OnQuickFindAgain, id=id)
        self.txtQuickFind=txtCtrl
        
        parent.SetToolBar(self.tbMain)
        self.tbMain.Realize()
        return self.tbMain
    def OnQuickFind(self,event):
        sVal=self.txtQuickFind.GetValue()
        self.idQuickFoundLast=-1
    def OnQuickFindAgain(self,event):
        sVal=self.txtQuickFind.GetValue()
        id=self.vgpTree.GetMoveTargetID()
        self.thdFindQuick.Do(id,self.idQuickFoundLast,sVal,100)
    def OnFindQuickFound(self,evt):
        evt.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.INFO,'id:%08d'%(evt.GetID()),self)
            self.idQuickFoundLast=evt.GetID()
            self.vgpTree.SelectByID('%08d'%self.idQuickFoundLast)
        except:
            vtLog.vtLngTB(self.GetName())
            vtLog.vtLngCurWX(vtLog.ERROR,'id:%08d'%(evt.GetID()),self)
    def Clear(self):
        if self.node is not None:
            self.netPrjPlan.endEdit(self.node)
        vtXmlDomConsumer.Clear(self)
    def OnTreeItemSel(self,event):
        node=event.GetTreeNodeData()
        self.viTag.SetNode(node)
        return
        if self.node is not None:
            self.netPrjPlan.endEdit(self.node)
        if event.GetTreeNodeData() is not None:
            self.netPrjPlan.startEdit(node)
            self.nbData.SetNode(node)
        else:
            # grouping tree item selected
            self.nbData.SetNode(None)
            pass
        event.Skip()
    def OnGotContent(self,evt):
        self.PrintMsg(_('content got. generate ...'))
        #cfgNode=self.netPrjPlan.getChild(self.netPrjPlan.getRoot(),'config')
        #self.netHum.SetCfgNode(cfgNode)
        #self.netHum.AutoConnect2Srv()
        if 1:
            self.nbDraw.Build()
        else:
            self.drStruct.Build()
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnLock(self,evt):
        if evt.GetResponse()=='ok':
            try:
                self.nodeLock=self.netPrjPlan.getNodeById(evt.GetID())
            except:
                self.nodeLock=None
        else:
            self.nodeLock=None
        evt.Skip()
    def OnUnLock(self,evt):
        if evt.GetResponse()=='ok':
            self.nodeLock=None
        evt.Skip()
    def __getSettings__(self):
        self.xmlLastPrj=None
        self.xmlLastTask=None
        self.xmlLastSubTask=None
        self.xmlLastDesc=None
        self.xmlLastLoc=None
        self.xmlLastPerson=None
        
        self.xmlPrjFN=self.netPrjPlan.getNodeText(self.baseSettings,'projectfn')
        self.xmlHumanFN=self.netPrjPlan.getNodeText(self.baseSettings,'humanfn')
        self.xmlLocationFN=self.netPrjPlan.getNodeText(self.baseSettings,'locationfn')
        self.xmlBaseTmplDN=self.netPrjPlan.getNodeText(self.baseSettings,'templatedn')
        self.xmlProjectDN=self.netPrjPlan.getNodeText(self.baseSettings,'projectdn')
        # get tree view settings
        sTreeViewGrp=self.netPrjPlan.getNodeText(self.baseSettings,'treeviewgrp')
        mnItems=self.mnViewTree.GetMenuItems()
    def __getXmlBaseNodes__(self):
        self.baseDoc=None
        self.baseSettings=None
        
        self.baseDoc=self.netPrjPlan.getBaseNode()
        self.baseSettings=self.netPrjPlan.getChildForced(self.netPrjPlan.getRoot(),'settings')
        #if self.baseSettings is None:
        #    self.baseSettings=self.netPrjPlan.createSubNode(self.netPrjPlan.getRoot(),'settings')
        #    self.netPrjPlan.AlignNode(self.baseSettings,iRec=1)
        self.__getSettings__()
        self.netPrjPlan.AlignNode(self.baseSettings,iRec=1)
    def OpenFile(self,fn):
        vtLog.vtLngCS(vtLog.INFO,'file:%s'%repr(fn))
        if self.netPrjPlan.ClearAutoFN()>0:
            self.netPrjPlan.Open(fn)
            
            self.__getXmlBaseNodes__()
        
    def OpenCfgFile(self,fn=None):
        vtLog.vtLngCS(vtLog.INFO,'file:%s'%repr(fn))
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                self.netMaster.SetCfgNode()
                self.__setCfg__()
                return
        self.xdCfg.New(root='config')
        if fn is not None:
            self.xdCfg.SetFN(fn)
            self.netMaster.SetCfgNode()
    def CreateNew(self):
        vtLog.vtLngCS(vtLog.INFO,'new')
        self.netPrjPlan.New()
        self.__getXmlBaseNodes__()
        self.vgpTree.SetNode(None)
    def OnMnFileItem_settingMenu(self, event):
        nodeSetting=self.netPrjPlan.getChild(self.netPrjPlan.getRoot(),'settings')
        if nodeSetting is not None:
            dlg=vPrjPlanSettingsDialog(self)
            dlg.SetXml(self.netPrjPlan,nodeSetting)
            ret=dlg.ShowModal()
            if ret>0:
                dlg.GetXml(self.netPrjPlan,nodeSetting)
                self.__getSettings__()
            dlg.Destroy()
        event.Skip()

    def __setCfg__(self):
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        self.pnData.Refresh()
        
        self.bBlockCfgEventHandling=False
        
    def OnSlwNavSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            node=self.xdCfg.getChildForced(None,'layout')
            iWidth=event.GetDragRect().width
            #if iWidth>80:
            #    iWidth=80
            self.dCfg['nav_sash']=str(iWidth)
            
            self.slwNav.SetDefaultSize((iWidth, 1000))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnSlwTopSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            node=self.xdCfg.getChildForced(None,'layout')
            iHeight=event.GetDragRect().height
            #if iHeight>80:
            #    iHeight=80
            self.dCfg['top_sash']=str(iHeight)
            
            self.slwTop.SetDefaultSize((1000, iHeight))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnSize(self, event):
        #event.Skip()
        bMod=False
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)

    def OnCbApplyButton(self, event):
        self.nbData.GetNode()
        self.vgpTree.RefreshSelected()
        event.Skip()

    def OnCbCancelButton(self, event):
        self.nbData.SetNode(self.node)
        event.Skip()

    def OnMnAnalyseMn_analyse_buildMenu(self, event):
        if 1:
            self.nbDraw.Build()
        else:
            self.drStruct.Build()
        event.Skip()

    def OnMnAnalyseMn_analyse_calc_dateMenu(self, event):
        self.netPrjPlan.CalcDates(False)
        self.nbDraw.Build()
        event.Skip()

    def OnMnToolsMn_tools_req_lockMenu(self, event):
        node=self.vgpTree.GetSelected()
        self.netPrjPlan.reqLock(node)
        event.Skip()
    def SetCfgData(self,func,l,d):
        self.cfgFunc=func
        self.cfgOrigin=l
        self.dCfg=d
        try:
            i=int(self.dCfg['nav_sash'])
            self.slwNav.SetDefaultSize((i, 1000))
        except:
            pass
        try:
            i=int(self.dCfg['top_sash'])
            self.slwTop.SetDefaultSize((1000,i))
        except:
            pass
        try:
            sz=map(int,self.dCfg['size'].split(','))
            self.GetParent().SetSize(sz)
        except:
            pass
    def GetCfgData(self):
        try:
            sz=self.GetParent().GetSize()
            self.dCfg['size']='%d,%d'%(sz[0],sz[1])
        except:
            pass
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,self.dCfg)
    def GetAboutData(self):
        import __config__
        version=u' %d.%d.%d.%d'%(__config__.VER_MAJOR,__config__.VER_MINOR,
                    __config__.VER_RELEASE,__config__.VER_SUBREL)
        desc=_(u"""Project planning module.

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
""")
        return _(u'VIDARC Project Plan'),desc,version

    def OnViTagToolXmlTagCancel(self, event):
        event.Skip()

    def OnViTagToolXmlTagApply(self, event):
        event.Skip()

    def OnViTagToolXmlTagSelected(self, event):
        event.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            lWid=event.GetListWidget()
            self.bxsData.Layout()
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        except:
            vtLog.vtLngTB(self.GetName())
