#Boa:FramePanel:vXmlNodeWorkPackageFileAttrPanel
#----------------------------------------------------------------------
# Name:         vXmlNodeWorkPackageFileAttrPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060906
# CVS-ID:       $Id: vXmlNodeWorkPackageFileAttrPanel.py,v 1.5 2013/06/03 06:11:57 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputTag
import vidarc.tool.input.vtInputText
import wx.lib.buttons

import sys,os,os.path,shutil

import vidarc.tool.InOut.fnUtil as fnUtil
from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel
#from vidarc.tool.vtThread import vtThread
from vidarc.vApps.vDoc.vDocSelDialog import vDocSelDialog

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_VXMLNODEWORKPACKAGEFILEATTRPANEL, 
 wxID_VXMLNODEWORKPACKAGEFILEATTRPANELCBAPPLY, 
 wxID_VXMLNODEWORKPACKAGEFILEATTRPANELCBBROWSE, 
 wxID_VXMLNODEWORKPACKAGEFILEATTRPANELCBDEL, 
 wxID_VXMLNODEWORKPACKAGEFILEATTRPANELCBOPEN, 
 wxID_VXMLNODEWORKPACKAGEFILEATTRPANELLBLALIAS, 
 wxID_VXMLNODEWORKPACKAGEFILEATTRPANELLBLFN, 
 wxID_VXMLNODEWORKPACKAGEFILEATTRPANELLSTFN, 
 wxID_VXMLNODEWORKPACKAGEFILEATTRPANELTXTFN, 
 wxID_VXMLNODEWORKPACKAGEFILEATTRPANELVIALIAS, 
] = [wx.NewId() for _init_ctrls in range(10)]

class vFileDropTarget(wx.FileDropTarget):
    def __init__(self, window):
        wx.FileDropTarget.__init__(self)
        self.window = window
    def OnDropFiles(self, x, y, filenames):
        for file in filenames:
            self.window.AddFile(file)
            pass

class vXmlNodeWorkPackageFileAttrPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbDel, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cbOpen, 0, border=0, flag=0)

    def _init_coll_bxsAlias_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblAlias, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.viAlias, 4, border=0, flag=wx.EXPAND)

    def _init_coll_bxsFN_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblFN, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtFN, 4, border=0, flag=wx.EXPAND)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsFN, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbBrowse, 0, border=4, flag=wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsAlias, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbApply, 0, border=4, flag=wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.lstFN, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsBt, 0, border=4, flag=wx.RIGHT | wx.LEFT)

    def _init_coll_lstFN_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'attr'), width=-1)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_RIGHT,
              heading=_(u'FN'), width=200)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT, heading=_(u'DN'),
              width=250)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=2, hgap=0, rows=4, vgap=4)

        self.bxsFN = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsAlias = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsFN_Items(self.bxsFN)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsAlias_Items(self.bxsAlias)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEWORKPACKAGEFILEATTRPANEL,
              name=u'vXmlNodeWorkPackageFileAttrPanel', parent=prnt,
              pos=wx.Point(110, 110), size=wx.Size(335, 214),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(327, 187))

        self.lblFN = wx.StaticText(id=wxID_VXMLNODEWORKPACKAGEFILEATTRPANELLBLFN,
              label=_(u'filename'), name=u'lblFN', parent=self, pos=wx.Point(0,
              0), size=wx.Size(44, 30), style=wx.ALIGN_RIGHT)
        self.lblFN.SetMinSize(wx.Size(-1, -1))

        self.txtFN = wx.TextCtrl(id=wxID_VXMLNODEWORKPACKAGEFILEATTRPANELTXTFN,
              name=u'txtFN', parent=self, pos=wx.Point(48, 0), size=wx.Size(194,
              30), style=0, value=u'')
        self.txtFN.SetMinSize(wx.Size(-1, -1))

        self.cbBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODEWORKPACKAGEFILEATTRPANELCBBROWSE,
              bitmap=vtArt.getBitmap(vtArt.Browse), label=u'Browse',
              name=u'cbBrowse', parent=self, pos=wx.Point(247, 0),
              size=wx.Size(76, 30), style=0)
        self.cbBrowse.SetMinSize(wx.Size(-1, -1))
        self.cbBrowse.Bind(wx.EVT_BUTTON, self.OnCbBrowseButton,
              id=wxID_VXMLNODEWORKPACKAGEFILEATTRPANELCBBROWSE)

        self.lblAlias = wx.StaticText(id=wxID_VXMLNODEWORKPACKAGEFILEATTRPANELLBLALIAS,
              label=_(u'alias'), name=u'lblAlias', parent=self, pos=wx.Point(0,
              34), size=wx.Size(44, 30), style=wx.ALIGN_RIGHT)
        self.lblAlias.SetMinSize(wx.Size(-1, -1))

        self.viAlias = vidarc.tool.input.vtInputTag.vtInputTag(id=wxID_VXMLNODEWORKPACKAGEFILEATTRPANELVIALIAS,
              name=u'viAlias', parent=self, pos=wx.Point(48, 34),
              size=wx.Size(194, 30), style=0)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODEWORKPACKAGEFILEATTRPANELCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=u'Apply',
              name=u'cbApply', parent=self, pos=wx.Point(247, 34),
              size=wx.Size(76, 30), style=0)
        self.cbApply.SetMinSize(wx.Size(-1, -1))
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VXMLNODEWORKPACKAGEFILEATTRPANELCBAPPLY)

        self.lstFN = wx.ListCtrl(id=wxID_VXMLNODEWORKPACKAGEFILEATTRPANELLSTFN,
              name=u'lstFN', parent=self, pos=wx.Point(0, 68), size=wx.Size(243,
              119), style=wx.LC_REPORT)
        self.lstFN.SetMinSize(wx.Size(-1, -1))
        self._init_coll_lstFN_Columns(self.lstFN)
        self.lstFN.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstFNListItemDeselected,
              id=wxID_VXMLNODEWORKPACKAGEFILEATTRPANELLSTFN)
        self.lstFN.Bind(wx.EVT_LIST_ITEM_SELECTED, self.OnLstFNListItemSelected,
              id=wxID_VXMLNODEWORKPACKAGEFILEATTRPANELLSTFN)

        self.cbOpen = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODEWORKPACKAGEFILEATTRPANELCBOPEN,
              bitmap=vtArt.getBitmap(vtArt.Open), label=_(u'Open'),
              name=u'cbOpen', parent=self, pos=wx.Point(247, 106),
              size=wx.Size(76, 30), style=0)
        self.cbOpen.SetMinSize(wx.Size(-1, -1))
        self.cbOpen.Bind(wx.EVT_BUTTON, self.OnCbOpenButton,
              id=wxID_VXMLNODEWORKPACKAGEFILEATTRPANELCBOPEN)

        self.cbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODEWORKPACKAGEFILEATTRPANELCBDEL,
              bitmap=vtArt.getBitmap(vtArt.Del), label=_(u'Delete'),
              name=u'cbDel', parent=self, pos=wx.Point(247, 68),
              size=wx.Size(76, 30), style=0)
        self.cbDel.SetMinSize(wx.Size(-1, -1))
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VXMLNODEWORKPACKAGEFILEATTRPANELCBDEL)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vEngineering')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,applName=_(u'vEngineering')+' '+_(u'files'),
                lWidgets=[self.lstFN],
                lEvent=[])
        self.viAlias.SetEnableMark(False)
        #self.thdOpen=vtThread(self,True)
        #EVT_VT_THREAD_FINISHED(self,self.DoOpen)
        self.dlgDocSel=None
        self.dlgFile=None
        
        dt = vFileDropTarget(self)
        self.lstFN.SetDropTarget(dt)

        self.Clear()
        self.netPrjDoc=None
        self.netPrj=None
        self.netDocument=None
        self.SetSuppressNetNotify(False)
        
    def ClearInt(self):
        try:
            vtXmlNodePanel.ClearInt(self)
            # add code here
            self.lFN={}
            self.lstFN.DeleteAllItems()
            self.iSelIdx=-1
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def SetNetDocs(self,d):
        # add code here
        if self.VERBOSE:
            vtLog.CallStack('')
            print d
        if 'vPrjDoc' in d:
            dd=d['vPrjDoc']
            #self.prjDN=dd['doc'].GetPrjDN()
            self.netPrjDoc=dd['doc']
        if 'vPrj' in d:
            dd=d['vPrj']
            #self.prjDN=dd['doc'].GetPrjDN()
            self.netPrj=dd['doc']
        if 'vDoc' in d:
            self.netDocument=d['vDoc']['doc']
    def SetDoc(self,doc,bNet=False):
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        # add code here
    def __showFiles__(self):
        self.viAlias.Clear()
        self.txtFN.SetValue('')
        self.lstFN.DeleteAllItems()
        self.iSelIdx=-1
        keys=self.lFN.keys()
        keys.sort()
        for k in keys:
            dInfo=self.lFN[k]
            idx=self.lstFN.InsertStringItem(sys.maxint,k)
            i=1
            for kk in ['FN','DN']:
                if kk in dInfo:
                    self.lstFN.SetStringItem(idx,i,dInfo[kk])
                i+=1
    def SetNode(self,node):
        try:
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            if hasattr(self.objRegNode,'IsAttrFile')==False:
                return
            # add code here
            for o in self.doc.getChildsNoAttr(self.node,self.doc.attr):
                sAttr=self.doc.getTagName(o)
                if self.objRegNode.IsAttrFile(sAttr):
                    dInfo={}
                    dInfo['fullFN']=self.doc.getNodeText(o,'val')
                    sDN,sFN=os.path.split(dInfo['fullFN'])
                    dInfo['DN']=sDN
                    dInfo['FN']=sFN
                    dInfo['alias']=sAttr#self.doc.getNodeText(o,'alias')
                    self.lFN[sAttr]=dInfo
            self.__showFiles__()
        except:
            self.__logTB__()
    def GetNode(self,node=None):
        try:
            if hasattr(self.objRegNode,'IsAttrFile')==False:
                return
            
            node=self.GetNodeStart(node)
            if node is None:
                return
            # add code here
            for o in self.doc.getChildsNoAttr(self.node,self.doc.attr):
                sAttr=self.doc.getTagName(o)
                if self.objRegNode.IsAttrFile(sAttr):
                    self.doc.deleteNode(o,self.node)
            keys=self.lFN.keys()
            keys.sort()
            for k in keys:
                self.doc.createSubNodeDict(node,{
                        'tag':k,'lst':[
                            {'tag':'type','val':'file'},
                            {'tag':'val','val':self.lFN[k]['fullFN']},
                            ]
                        })
            self.GetNodeFin(node)
        except:
            self.__logTB__()
    def Close(self):
        vtXmlNodePanel.Close(self)
        # add code here
    def Lock(self,flag):
        vtXmlNodePanel.Lock(self,flag)
        if flag:
            # add code here
            pass
        else:
            # add code here
            pass
    def __setFN__(self,sAlias,dInfo):
        try:
            fn=fnUtil.getFilenameRel2BaseDir(filename,self.prjDN)
            dInfo['fullFN']=filename
            sDN,sFN=os.path.split(dInfo['fullFN'])
            dInfo['DN']=sDN
            dInfo['FN']=sFN
            self.__showFiles__()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbBrowseButton(self, event):
        event.Skip()
        try:
            bNew=False
            dInfo=self.__getSelInfoDict__()
            if dInfo is not None:
                fn=dInfo['fullFN']
                #self.thdOpen.Do(self.__doOpen__,fn)
                self.prjDN=self.netPrjDoc.GetPrjDN()
                
                if self.VERBOSE:
                    vtLog.CallStack('')
                    print 'file:',fn
                    print 'dn:',self.prjDN
                if fn[0]=='.':
                    filename=fnUtil.getAbsoluteFilenameRel2BaseDir(fn,self.prjDN)
                else:
                    filename=fn
            else:
                bNew=True
                self.prjDN=self.netPrjDoc.GetPrjDN()
                filename=os.path.join(self.prjDN,'')
            if self.dlgFile is None:
                self.dlgFile = wx.FileDialog(self, _(u"Choose a file"), ".", "", _(u"all files (*.*)|*.*"), wx.OPEN)
            try:
                self.dlgFile.SetPath(filename)
                if self.dlgFile.ShowModal() == wx.ID_OK:
                    filename = self.dlgFile.GetPath()
                    # Your code
                    #self.__setFN(dInfo['alias'],filename)
                    #self.txtFN.SetValue('')
                    self.AddFile(self.dlgFile.GetPath(),bNew)
            except:
                self.__logTB__()
        except:
            self.__logTB__()
    def __getSelInfoDict__(self):
        try:
            if self.iSelIdx>=0:
                sAttr=self.lstFN.GetItem(self.iSelIdx).m_text
                if self.VERBOSE:
                    vtLog.CallStack('')
                    print sAttr
                return self.lFN[sAttr]
        except:
            self.__logTB__()
        return None
    def OnCbApplyButton(self, event):
        event.Skip()
        if self.iSelIdx<0:
            return
        try:
            dInfo=self.__getSelInfoDict__()
            if dInfo is not None:
                sAlias=''.join(['file',self.viAlias.GetValue()])
                self.lFN[sAlias]=dInfo
                del self.lFN[dInfo['alias']]
                dInfo['alias']=sAlias
                self.lstFN.SetStringItem(self.iSelIdx,0,sAlias)
                self.SetModified(True,self.lstFN)
            else:
                sAlias=''.join(['file',self.viAlias.GetValue()])
                self.iSelIdx=self.lstFN.InsertStringItem(sys.maxint,sAlias)
                self.AddFile(self.txtFN.GetValue())
                
        except:
            self.__logTB__()
    def OnLstFNListItemDeselected(self, event):
        event.Skip()
        self.iSelIdx=-1
        self.viAlias.Clear()
        self.txtFN.SetValue('')
    def OnLstFNListItemSelected(self, event):
        event.Skip()
        self.iSelIdx=event.GetIndex()
        try:
            sAttr=self.lstFN.GetItem(self.iSelIdx).m_text
            self.viAlias.SetValue(sAttr[4:])
            dInfo=self.__getSelInfoDict__()
            if dInfo is not None:
                self.txtFN.SetValue(dInfo['fullFN'])
        except:
            self.__logTB__()
    def __doOpen__(self,fn):
        try:
            self.netPrjDoc.acquire()
            self.prjDN=self.netPrjDoc.GetPrjDN()
        except:
            pass
        self.netPrjDoc.release()
        return 0
    def DoOpen(self,evt):
        pass
    def OnCbOpenButton(self, event):
        event.Skip()
        try:
            self.__openFile__()
        except:
            self.__logTB__()
    def OnCbDelButton(self, event):
        event.Skip()
        if self.iSelIdx<0:
            return
        try:
            dInfo=self.__getSelInfoDict__()
            if dInfo is not None:
                sAlias=''.join(['file',self.viAlias.GetValue()])
                del self.lFN[dInfo['alias']]
                self.lstFN.DeleteItem(self.iSelIdx)
                self.iSelIdx=-1
                self.viAlias.Clear()
                self.txtFN.SetValue('')
                self.SetModified(True,self.lstFN)
        except:
            self.__logTB__()
    def __getLastUsed__(self,attrBase):
        iLastUsed=-1
        for attrName in self.lFN.keys():
            if attrName[:len(attrBase)]==attrBase:
                if self.lFN[attrName] is not None:
                    try:
                        i=int(attrName[len(attrBase):])
                        if i>iLastUsed:
                            iLastUsed=i
                    except:
                        pass
        return iLastUsed
    def __getNextFree__(self,attrBase):
        i=self.__getLastUsed__(attrBase)
        return u'%s%02d'%(attrBase,i+1)
    def AddFile(self,filename,bNew=True):
        self.prjDN=self.netPrjDoc.GetPrjDN()
        if sys.platform=='win32':
            filename=filename.replace('\\','/')
            prjDN=self.prjDN.replace('\\','/')
        tupVal=fnUtil.getFilenameRel2BaseDir(filename,prjDN)
        if tupVal[0]!=u'.':
            dlg=wx.MessageDialog(self,_(u'Do you to import data?') ,
                        _(u'vXmlNodeEngineeringFileAttr'),
                        wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            if dlg.ShowModal()==wx.ID_YES:
                if self.dlgDocSel is None:
                    self.dlgDocSel=vDocSelDialog(self)
                    self.dlgDocSel.SetLanguage(self.doc.GetLang())
                    self.dlgDocSel.SetDoc(self.netDocument,True)
                    #self.dlgDocSel.SetDocFN(self.docFN)
                #self.dlgDocSel.Centre()
                ret=self.dlgDocSel.ShowModal()
                if ret==1:
                    node=self.dlgDocSel.GetSelNode()
                    if node is not None:
                        if self.netPrj is not None:
                            prjid=self.doc.getAttribute(self.doc.getBaseNode(),'prjfid')
                            prjnode=self.netPrj.GetPrj(prjid)
                        else:
                            prjnode=None
                        if prjnode is not None:
                            sShort=self.netPrj.GetPrjName(prjnode)
                            sClient=self.netPrj.GetCltShort(prjnode)
                        else:
                            sShort=u''
                            sClient=u''
                        docNr=self.doc.getAttribute(node,'id')
                        dn,fn=os.path.split(filename)
                        lst=self.dlgDocSel.GetDocPath()
                        sDocPath=os.path.sep.join(lst)
                        dn=os.path.join(prjDN,sDocPath)
                        try:
                            os.makedirs(dn)
                        except:
                            pass
                        nFN=os.path.join(dn,sClient+sShort+'_'+docNr+'_'+fn)
                        bOk=False
                        #fnUtil.shiftFile(nFN,5)
                        #bOk=True
                        i=0
                        while bOk==False:
                            if i==0:
                                s=''
                            else:
                                s='%d.'%i
                            sExts=nFN.split('.')
                            sExt=sExts[-1]
                            nTestFN=nFN[:-len(sExt)]+s+sExt
                            try:
                                os.stat(nTestFN)
                                i+=1
                            except:
                                nFN=nTestFN
                                bOk=True
                        shutil.copy(filename,nFN)
                        tupVal=fnUtil.getFilenameRel2BaseDir(nFN,prjDN)
            dlg.Destroy()
        if bNew:
            # generate new link
            sAttr=self.__getNextFree__(u'file')
            if sys.platform=='win32':
                tupVal=tupVal.replace('\\','/')
            dInfo={}
            dInfo['fullFN']=tupVal
            sDN,sFN=os.path.split(dInfo['fullFN'])
            dInfo['DN']=sDN
            dInfo['FN']=sFN
            dInfo['alias']=sAttr#self.doc.getNodeText(o,'alias')
            self.lFN[sAttr]=dInfo
            #self.lFN[sAttr]={'type':'file','val':tupVal}
            self.__showFiles__()
            self.SetModified(True,self.lstFN)
            #idx=self.lstAttr.InsertImageStringItem(sys.maxint, 
            #                                sAttr, -1)
            #self.lstAttr.SetStringItem(idx,1,tupVal,-1)
            #self.__setModified__(True)
        else:
            if sys.platform=='win32':
                tupVal=tupVal.replace('\\','/')
            self.lstFN.SetStringItem(self.iSelIdx,1,tupVal,-1)
            dInfo=self.__getSelInfoDict__()
            if dInfo is not None:
                dInfo['fullFN']=tupVal
                sDN,sFN=os.path.split(dInfo['fullFN'])
                dInfo['DN']=sDN
                dInfo['FN']=sFN
                #dInfo['alias']=sAttr
            self.__showFiles__()
            self.SetModified(True,self.lstFN)
        
    def __openFile__(self):
        try:
            dInfo=self.__getSelInfoDict__()
            if dInfo is not None:
                fn=dInfo['fullFN']
                #self.thdOpen.Do(self.__doOpen__,fn)
                self.prjDN=self.netPrjDoc.GetPrjDN()
                
                sExts=fn.split('.')
                sExt=sExts[-1]
                fileType = wx.TheMimeTypesManager.GetFileTypeFromExtension(sExt)
                if fn[0]=='.':
                    filename=fnUtil.getAbsoluteFilenameRel2BaseDir(fn,self.prjDN)
                else:
                    filename=fn
                try:
                    mime = fileType.GetMimeType()
                    if sys.platform=='win32':
                        filename=filename.replace('/','\\')
                    cmd = fileType.GetOpenCommand(filename, mime)
                    wx.Execute(cmd)
                    vtLog.PrintMsg(_(u'opening %s ...')%filename,self.widLogging)
                    try:
                        vtLog.vtLngCurWX(vtLog.DEBUG,'cmd:%s'%(cmd),self)
                    except:
                        pass
                except:
                    sMsg=u"Cann't open %s!"%(filename)
                    dlg=wx.MessageDialog(self,sMsg ,
                                u'vXmlNodeEngineeringFileAttr',
                                wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                    dlg.ShowModal()
                    dlg.Destroy()
        except:
            self.__logTB__()
