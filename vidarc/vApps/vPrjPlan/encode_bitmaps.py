#!/usr/bin/env python
#----------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    #"   -u -i -n Application Tools01_16.png images.py",
#    "   -u -i -n Mondrian mondrian.ico images.py",
    "-u -i -n Element img/Box01_16.png images.py",
    "-a -u -n PrjPlan img/VidMod_PrjPlan_16.png images.py",
    "-a -u -n prjPlanSel img/VidMod_PrjPlan_16.png images.py",
    "-a -u -n Text img/Note01_16.png images.py",
    "-a -u -n TextSel img/Note01_16.png images.py",
    
    "-a -u -n vXmlNodeWorkPackage img/WorkPackage01_16.png images.py",
    "-a -u -n vXmlNodeWorkPackageContents img/NewDocRev01_16.png images.py",
    "-a -u -n vXmlNodeWorkPackageResult img/Briefcase03_16.png images.py",
    "-a -u -n vXmlNodeWorkPackageFollowUps img/Link03_16.png images.py",
    "-a -u -n vXmlNodeWorkPackageVolumeWork img/Timer02_16.png images.py",
    "-a -u -n FollowUpAtStart               img/WrkPkgAtStart01_16.png    images.py",
    "-a -u -n FollowUpAtEnd                 img/WrkPkgAtEnd01_16.png      images.py",
    "-a -u -n Result                        img/WrkPkgResult01_16.png     images.py",
    "-a -u -n MileStone                     img/WorkPackageMileStone01_16.png     images.py",
    "-a -u -n MileStoneOk                   img/WorkPackageMileStone02_16.png     images.py",
    
    "-a -u -n TimeDistribute img/TimeDistribute01_16.png images.py",
    
    "-a -u -n vPrjPlanCanvasStructure img/PrjStructurePlan01_16.png images.py",
    "-a -u -n vPrjPlanCanvasGantt img/PrjPlanGantt01_16.png images.py",
    "-a -u -n vtXmlNodeLog img/Note01_16.png images.py",
    
    "-a -u -n Add img/add.png images.py",
    "-a -u -n Cancel img/abort.png images.py",
    "-a -u -n Del img/waste.png images.py",
    "-a -u -n Apply img/ok.png images.py",
    #"-a -u -n Browse img/BrowseFile01_16.png images.py",
    #"-a -u -n Open img/DocEdit01_16.png images.py",
    "-a -u -n Application img/VidMod_PrjPlan_16.ico images.py",
    "-a -u -n Plugin img/VidMod_PrjPlan_16.png images.py",
    "-a -u -n Launch img/Launch01_32.png images.py",
    
    
    "-u -i -n Lang00 img/Lang_en_16.png images_lang.py",
    "-a -u -n Lang01 img/Lang_de_16.png images_lang.py",
    "-a -u -n Lang00Big img/Lang_en_32.png images_lang.py",
    "-a -u -n Lang01Big img/Lang_de_32.png images_lang.py",
    "-a -u -n ApplyMultiple img/ApplyMultiple01_32.png images_lang.py",
    #"-a -u -n User Usr01_16.png hum_tree_images.py",
    #"-a -u -n UserSel Usr01_16.png hum_tree_images.py",
    #"-a -u -n Human Human01_16.png hum_tree_images.py",
    #"-a -u -n HumanSel Human01_16.png hum_tree_images.py",
    #"-a -u -n Add add.png images.py",
    #"-a -u -n Del waste.png images.py",
    #"-a -u -n Apply checkmrk.png images.py",

    "-u -i -n Splash img/splashTmpl02.png images_splash.py",
    "-a -u -n Logo img/Vidarc_Logo_Brief.png images_splash.py",
    
    "-a -u -n NoIcon  img/noicon.png  images.py"
    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)

