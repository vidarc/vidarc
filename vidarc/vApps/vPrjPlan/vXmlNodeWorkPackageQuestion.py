#----------------------------------------------------------------------------
# Name:         vXmlNodeWorkPackageContense.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060410
# CVS-ID:       $Id: vXmlNodeWorkPackageQuestion.py,v 1.2 2010/03/03 02:17:15 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.lang.vtLgBase as vtLgBase
#import vidarc.config.vcCust as vcCust
from vidarc.tool.xml.vtXmlNodeTag import *
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeWorkPackageQuestion(vtXmlNodeTag):
    MAP_STATUS=[u'sleep', u'active', u'finished']
    def __init__(self,tagName='WrkPkgQuestion'):
        global _
        _=vtLgBase.assignPluginLang('vPrjPlan')
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'question')
    # ---------------------------------------------------------
    # specific
    def GetStatus(self,node):
        iStatus=self.GetVal(node,'status',int,0)
        return iStatus
    def SetStatus(self,node,val):
        self.SetVal(node,'status',str(val))
    def IsFinished(self,node):
        try:
            if self.GetStatus(node)==2:
                return True
            else:
                return False
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return False
    # ---------------------------------------------------------
    # inheritance
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsId2Add(self):
        "node id is managed"
        return True
    def IsNotContained(self):
        "do not add to listbook"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01TIDAT8\x8d\x85\x931O\x02A\x10\x85\xdf\x1ev"\xfd\x1ak\xaf\xbf\xf3Z\
\x9d\xc4\x12\xb8\x1ej\x13z\xa3%\xc9&\xd4\xfe\x0b"?@)\r\xfb\x078j{\xc3R\x0b\
\xb6\xf7,N.,w\'\x9bL7\xef\x9b\x997\xb3J\x05-4=\xe6c\x8a8\x888\x00\x9f\x00\
\x00c\x06P\xc1D\x95I*hU\x0204&%\x99r\xbb\x16\xbae\xc4\xedZ\x93\x04IPDX\xe6\
\xfaB{ \xd4\x9c\xbdD\xec\x8b0\x8aSFqJk\x0b\x80\xb5 `\xe9\x01\x00KkS\x92(\x85\
\xdd\xde3\xa38\xe3agd\xc8\xedZ3\x8aS\x1f`LX\x8a\x8f\x85\xfb\x10\x19\x91\x0c\
\xf94\xd2\xec\xf6\xe6>@D8\xea\x17b}\xe9X\xefIX\x16\xa8\xf5 \x8a3O\xec\x9b\
\xa9K\xb1W\xa0i\x8dw\xb7\x0f\\,:\xd8\xb9)\xdeg\x1a\xafo\x1d\xe4\xed\x04Y\xf6\
\x88\xcd\xe6\xaa\\c-\x80\xf9\x98\xe4\x0fvn\x8a\xe10D\xdeN\xe0\xdc\x00\xabU\
\xa2*\xc9uwP\xb4\x8d\xbfM\xcc+fz\xfe\xd4\x1dQ\x9dYM\x11\xd4\xcd\xbfs\xdf\xb0\
\x16pn\xd0x\xe6\xfbW\x01\xa8`\xa2\xaeoR|\xf1\x1e\x9d\x0b}\x12P1\x91\xf9\x07\
\x01\xeb\x7f\x98\x7f\t\x15\x03\x8bS\x05\xcc\xc9\xf9U\xd0\xc2\xd91\xd0Z\r\xb7\
<\x07 \x00&\'\x1b\xf8\x05\x03\xc3\xdd\xa6\xb4\x0biW\x00\x00\x00\x00IEND\xaeB\
`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            d=vtXmlNodeTag.GetAddDialogClass(self).copy()
            d['pnCls']=vtXmlNodeTag.GetPanelClass(self)
            return d
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            d=vtXmlNodeTag.GetAddDialogClass(self).copy()
            d['pnCls']=vtXmlNodeTag.GetPanelClass(self)
            return d
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vtXmlNodeTag.GetPanelClass(self)
        else:
            return None
