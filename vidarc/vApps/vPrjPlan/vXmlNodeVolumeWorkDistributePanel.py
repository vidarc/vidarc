#Boa:FramePanel:vXmlNodeVolumeWorkDistributePanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeVolumeWorkDistributePanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060824
# CVS-ID:       $Id: vXmlNodeVolumeWorkDistributePanel.py,v 1.7 2013/06/03 06:11:57 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputFloat
import vidarc.tool.input.vtInputGrid
import wx.lib.buttons
import vidarc.tool.time.vtTimeDateGM
import vidarc.tool.input.vtInputGridSmall
import vidarc.tool.input.vtInputTree
import vidarc.tool.input.vtInputTextML
import vidarc.tool.input.vtInputText

import sys

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.time.vtTime import vtDateTime
from vidarc.tool.xml.vtXmlHierarchy import getTagNames

[wxID_VXMLNODEVOLUMEWORKDISTRIBUTEPANEL, 
 wxID_VXMLNODEVOLUMEWORKDISTRIBUTEPANELCBEQUAL, 
 wxID_VXMLNODEVOLUMEWORKDISTRIBUTEPANELCBSEARCH, 
 wxID_VXMLNODEVOLUMEWORKDISTRIBUTEPANELCHCEQUAL, 
 wxID_VXMLNODEVOLUMEWORKDISTRIBUTEPANELCHCFUNC, 
 wxID_VXMLNODEVOLUMEWORKDISTRIBUTEPANELGRDVOLUMN, 
 wxID_VXMLNODEVOLUMEWORKDISTRIBUTEPANELLBLFUNC, 
 wxID_VXMLNODEVOLUMEWORKDISTRIBUTEPANELLBLVOLUME, 
 wxID_VXMLNODEVOLUMEWORKDISTRIBUTEPANELVIVOLDIST, 
 wxID_VXMLNODEVOLUMEWORKDISTRIBUTEPANELVIVOLUME, 
] = [wx.NewId() for _init_ctrls in range(10)]

class vXmlNodeVolumeWorkDistributePanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbSearch, (0, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.cbEqual, (0, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.chcEqual, (0, 2), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.lblFunc, (1, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.chcFunc, (1, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.grdVolumn, (3, 0), border=0, flag=wx.EXPAND,
              span=(2, 4))
        parent.AddWindow(self.lblVolume, (2, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viVolume, (2, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viVolDist, (2, 2), border=0, flag=wx.EXPAND,
              span=(1, 1))

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=4, vgap=4)

        self._init_coll_gbsData_Items(self.gbsData)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEVOLUMEWORKDISTRIBUTEPANEL,
              name=u'vXmlNodeVolumeWorkDistributePanel', parent=prnt,
              pos=wx.Point(0, 0), size=wx.Size(481, 210),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(473, 183))
        self.SetAutoLayout(True)

        self.grdVolumn = vidarc.tool.input.vtInputGridSmall.vtInputGridSmall(id=wxID_VXMLNODEVOLUMEWORKDISTRIBUTEPANELGRDVOLUMN,
              name=u'grdVolumn', parent=self, pos=wx.Point(0, 85),
              size=wx.Size(358, 44))
        self.grdVolumn.SetMinSize(wx.Size(-1, -1))
        self.grdVolumn.Bind(vidarc.tool.input.vtInputGrid.vEVT_VTINPUT_GRID_INFO_SELECTED,
              self.OngrdVolumnVtinputGridInfoSelected,
              id=wxID_VXMLNODEVOLUMEWORKDISTRIBUTEPANELGRDVOLUMN)
        self.grdVolumn.Bind(vidarc.tool.input.vtInputGrid.vEVT_VTINPUT_GRID_INFO_CHANGED,
              self.OngrdVolumnVtinputGridInfoChanged,
              id=wxID_VXMLNODEVOLUMEWORKDISTRIBUTEPANELGRDVOLUMN)

        self.cbSearch = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODEVOLUMEWORKDISTRIBUTEPANELCBSEARCH,
              bitmap=vtArt.getBitmap(vtArt.Find), label=_(u'Search'),
              name=u'cbSearch', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(76, 30), style=0)
        self.cbSearch.SetMinSize(wx.Size(-1, -1))
        self.cbSearch.Bind(wx.EVT_BUTTON, self.OnCbSearchButton,
              id=wxID_VXMLNODEVOLUMEWORKDISTRIBUTEPANELCBSEARCH)

        self.lblFunc = wx.StaticText(id=wxID_VXMLNODEVOLUMEWORKDISTRIBUTEPANELLBLFUNC,
              label=u'functions', name=u'lblFunc', parent=self, pos=wx.Point(0,
              34), size=wx.Size(76, 21), style=wx.ALIGN_RIGHT)
        self.lblFunc.SetMinSize(wx.Size(-1, -1))

        self.chcFunc = wx.Choice(choices=[],
              id=wxID_VXMLNODEVOLUMEWORKDISTRIBUTEPANELCHCFUNC, name=u'chcFunc',
              parent=self, pos=wx.Point(80, 34), size=wx.Size(130, 21),
              style=wx.CB_SORT)
        self.chcFunc.SetMinSize(wx.Size(-1, -1))
        self.chcFunc.Bind(wx.EVT_CHOICE, self.OnChcFuncChoice,
              id=wxID_VXMLNODEVOLUMEWORKDISTRIBUTEPANELCHCFUNC)

        self.lblVolume = wx.StaticText(id=wxID_VXMLNODEVOLUMEWORKDISTRIBUTEPANELLBLVOLUME,
              label=u'volume [MM]', name=u'lblVolume', parent=self,
              pos=wx.Point(0, 59), size=wx.Size(76, 22), style=wx.ALIGN_RIGHT)
        self.lblVolume.SetMinSize(wx.Size(-1, -1))

        self.viVolume = vidarc.tool.input.vtInputFloat.vtInputFloat(default=0.0,
              fraction_width=2,
              id=wxID_VXMLNODEVOLUMEWORKDISTRIBUTEPANELVIVOLUME,
              integer_width=6, maximum=100000, minimum=-100.0, name=u'viVolume',
              parent=self, pos=wx.Point(80, 59), size=wx.Size(130, 22),
              style=0)

        self.viVolDist = vidarc.tool.input.vtInputFloat.vtInputFloat(default=0.0,
              fraction_width=2,
              id=wxID_VXMLNODEVOLUMEWORKDISTRIBUTEPANELVIVOLDIST,
              integer_width=6, maximum=100000, minimum=0.0, name=u'viVolDist',
              parent=self, pos=wx.Point(214, 59), size=wx.Size(106, 22),
              style=0)
        self.viVolDist.Enable(False)

        self.cbEqual = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODEVOLUMEWORKDISTRIBUTEPANELCBEQUAL,
              bitmap=vtArt.getBitmap(vtArt.Build), name=u'cbEqual', parent=self,
              pos=wx.Point(80, 0), size=wx.Size(130, 30), style=0)
        self.cbEqual.SetMinSize(wx.Size(-1, -1))
        self.cbEqual.Bind(wx.EVT_BUTTON, self.OnCbEqualButton,
              id=wxID_VXMLNODEVOLUMEWORKDISTRIBUTEPANELCBEQUAL)

        self.chcEqual = wx.CheckBox(id=wxID_VXMLNODEVOLUMEWORKDISTRIBUTEPANELCHCEQUAL,
              label=u'equalized', name=u'chcEqual', parent=self,
              pos=wx.Point(214, 0), size=wx.Size(106, 30), style=0)
        self.chcEqual.SetValue(False)
        self.chcEqual.SetMinSize(wx.Size(-1, -1))

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vPrjPlan')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        
        self.viVolume.SetEnableMark(False)
        self.viVolDist.SetEnableMark(False)
        #self.viCost.SetShowFullName(False)
        #self.viCostDef.SetAutoStretch(True)
        self.sStartDate=''
        self.sEndDate=''
        self.lLblRow=[]
        self.lLblCol=[]
        self.lVal=[]
        self.grdVolumn.SetColumnSize([150,100])
        self.dFunc={}
        
        self.gbsData.AddGrowableCol(1)
        self.gbsData.AddGrowableCol(2)
        #self.gbsData.AddGrowableCol(4)
        #self.gbsData.AddGrowableRow(0)
        self.gbsData.AddGrowableRow(4)
        
        self.dtStart=vtDateTime(True)
        self.dtEnd=vtDateTime(True)
        self.dtTmp=vtDateTime(True)
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
        self.gbsData.Layout()
        self.gbsData.Fit(self)
    def ClearInt(self):
        try:
            vtXmlNodePanel.ClearInt(self)
            # add code here
            #self.viCostDef.Clear()
            self.sStartDate=''
            self.sEndDate=''
            self.lLblRow=[]
            self.lLblCol=[]
            self.lVal=[]
            self.iRow=-1
            self.iCol=-1
            self.grdVolumn.SetUp(self.lVal,self)
            self.dFunc={}
            self.chcFunc.Clear()
            #self.grdVolumn.Clear()
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def SetRegNode(self,obj):
        vtXmlNodePanel.SetRegNode(self,obj)
        #self.grdVolumn.Setup(obj.GetSetup(),None)
    def SetNetDocs(self,d):
        # add code here
        if d.has_key('vGlobals'):
            dd=d['vGlobals']
            #self.viFunc.SetDocTree(dd['doc'],dd['bNet'])
            pass
    def SetDoc(self,doc,bNet=False):
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        
        # add code here
        #self.viFunc.SetDoc(doc,bExternal=True)
        
    def SetNode(self,node):
        try:
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            
            # add code here
            #tup=self.objRegNode.GetValues(node)
            #self.viCostDef.SetValue(tup[0])
            #self.viCostDef.SetModified(False)
            #oTrans=self.doc.GetRegisteredNode(self.objRegNode.GetTagNameTransition())
            self.dCfg=self.objRegNode.GetValues(self.node)
            
            #self.lLblRow=dCfg['lRow']
            #self.lLblCol=dCfg['lCol']
            self.lVal=[]
            self.__setup__()

        except:
            self.__logTB__()
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if node is None:
                return
            # add code here
            self.grdVolumn.Get()
            self.objRegNode.SetValues(node,self.dCfg)
            self.GetNodeFin(node)
        except:
            self.__logTB__()
    def Close(self):
        vtXmlNodePanel.Close(self)
        # add code here
    def Cancel(self):
        vtXmlNodePanel.Cancel(self)
        # add code here
    def Lock(self,flag):
        vtXmlNodePanel.Lock(self,flag)
        if flag:
            # add code here
            pass
        else:
            # add code here
            pass

    def OnCbSearchButton(self, event):
        event.Skip()
        try:
            if self.node is not None:
                
                node=self.doc.getParent(self.node)
                lDate=self.doc.GetDateLimit(node)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'id:%s;lDate:%s'%
                            (self.doc.getKey(self.node),vtLog.pformat(lDate)),
                            self)
                if lDate[0] is None:
                    self.sStartDate=''
                else:
                    self.sStartDate=lDate[0]
                if lDate[1] is None:
                    self.sEndDate=''
                else:
                    self.sEndDate=lDate[1]
                self.dFunc=self.doc.GetFuncVolumeDict(node)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'dFunc:%s'%(vtLog.pformat(self.dFunc)),self)
                self.chcFunc.Clear()
                for tup in self.dFunc.items():
                    s=' '.join(tup[1][1])
                    #iFID,sAppl=self.doc.convForeign(tup[0])
                    iFID=tup[0]
                    self.chcFunc.Append(s,iFID)
                try:
                    self.chcFunc.SetSelection(0)
                    wx.CallAfter(self.OnChcFuncChoice,None)
                except:
                    pass
            else:
                vtLog.vtLngCurWX(vtLog.INFO,'no node selected',self)
        except:
            self.__logTB__()
    def OnChcFuncChoice(self, event):
        if event is not None:
            event.Skip()
        idx=self.chcFunc.GetSelection()
        if idx>=0:
            id=self.chcFunc.GetClientData(idx)
            #self.viFunc.SetValueID('',id)
            tupVal=self.dFunc[id]
            #print self.dFunc
            #print tupVal
            self.viVolume.SetValue(tupVal[0])
            #netGlb=self.doc.GetNetDoc('vGlobals')
            #nodeFunc=netGlb.
    def GetRowColType(self,row,col):
        """required for vtInputGrid"""
        if col==-1:
            return self.doc,None,'string'
        try:
            return self.doc,self.node,'float'
        except:
            pass
        return self.doc,self.node,'float'
    def GetRowColValue(self,row,col):
        """required for vtInputGrid"""
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'row:%02d col:%02d'%(row,col),self)
        try:
            return self.lVal[row][col]
        except:
            return None
    def SetRowColValue(self,row,col,val):
        """required for vtInputGrid"""
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'row:%02d col:%02d val:%s'%(row,col,str(val)),self)
        try:
            self.lVal[row][col]=val
        except:
            pass
    def GetColLabelValue(self,col,row=-1):
        """required for vtInputGrid"""
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'row:%02d col:%02d'%(row,col),self)
        try:
            return self.lLblCol[col]
        except:
            return ''
    def GetRowLabelValue(self,row):
        """required for vtInputGrid"""
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'row:%02d'%(row),self)
        try:
            return self.lLblRow[row]
        except:
            return ''
    def GetDataNodes(self):
        """required for vtInputGrid"""
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        return self.lVal
    def GetDataNode(self,row,col):
        """required for vtInputGrid"""
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'row:%02d col:%02d'%(row,col),self)
        return None
    def __setup__(self):
        lColSize=[]
        #self.dAttrs=self.viSetup.GetValueDict()
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%vtLog.pformat(self.dCfg),self)
        self.lLblRow=self.dCfg['lRow']
        self.lLblCol=self.dCfg['lCol']
        iRow=len(self.lLblRow)
        iCol=len(self.lLblCol)
        self.iCol=-1
        self.iRow=-1
        self.lVal=self.dCfg['val']
        if 0:
            for i in xrange(0,iRow+1):
                try:
                    self.lLblRow.append(dRow[i])
                except:
                    self.lLblRow.append(str(i))
                l=[]
                for j in xrange(0,iCol+1):
                    try:
                        l.append(self.lVal[i][j])
                    except:
                        l.append('0.0')
                ll.append(l)
        lCol=[150]
        for j in xrange(0,iCol+1):
            #try:
            #    self.lLblCol.append(dCol[j])
            #except:
            #    self.lLblCol.append(str(j))
            lCol.append(100)
        #if self.VERBOSE:
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%vtLog.pprint(ll),self)
        #self.lVal=ll
        #self.lVal=[]
        self.grdVolumn.SetColumnSize(lCol)
        self.grdVolumn.SetUp(self.lVal,self)

    def OnCbDelDateButton(self, event):
        event.Skip()
        if self.iCol>=0:
            self.objRegNode.DelColByIdx(self.dCfg,self.iCol)
            self.__setup__()

    def OnCbDelFuncButton(self, event):
        event.Skip()
        if self.iRow>=0:
            self.objRegNode.DelRowByIdx(self.dCfg,self.iRow)
            self.__setup__()

    def OngrdVolumnVtinputGridInfoSelected(self, event):
        event.Skip()
        #vtLog.CallStack('')
        self.iRow=event.GetRow()
        self.iCol=event.GetCol()
        self.__updateVolDist__()
    def __updateVolDist__(self):
        try:
            fSum=0.0
            #vtLog.CallStack('')
            iCount=self.grdVolumn.GetNumberCols()
            for iCol in xrange(iCount):
                fVal=self.grdVolumn.GetRowColValueActual(self.iRow,iCol)
                #print 'row:%3d,col:%3d val:%s'%(self.iRow,iCol,fVal)
                fSum+=float(fVal)
            #print 'sum',fSum
            self.viVolDist.SetValue(fSum)
        except:
            vtLog.vtLngTB(self.GetName())
            #self.viPreview.SetValue(0.0)
    def OngrdVolumnVtinputGridInfoChanged(self, event):
        event.Skip()
        #vtLog.CallStack('')
        self.iRow=event.GetRow()
        self.iCol=event.GetCol()
        self.__updateVolDist__()

    def OnCbEqualButton(self, event):
        event.Skip()
        try:
            node=self.doc.getParent(self.node)
            lWrkPkg=self.doc.GetWorkPackagesHier(node)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'lWrkPkg:%s'%(vtLog.pformat(lWrkPkg)),self)
            d=self.doc.GetWorkVolumePerMonthPerFunc(node)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'d:%s'%(vtLog.pformat(d)),self)
            dFunc=d['func']
            dWrkPkg=d['workpackages']
            dVal=d['func_month_values']
            self.dCfg=self.objRegNode.GetValuesEmpty()
            dtStart=vtDateTime(True)
            dtEnd=vtDateTime(True)
            dtTmp=vtDateTime(True)
            dtStart.SetDateStr(self.sStartDate)
            dtEnd.SetDateStr(self.sEndDate)
            lMon=[]
            iYrS=dtStart.GetYear()
            iMonS=dtStart.GetMonth()
            iYrE=dtEnd.GetYear()
            iMonE=dtEnd.GetMonth()
            for iYear in xrange(iYrS,iYrE+1):
                if iYear==iYrS:
                    iMonST=iMonS
                else:
                    iMonST=1
                if iYear==iYrE:
                    iMonET=iMonE
                else:
                    iMonET=12
                for iMon in xrange(iMonST,iMonET+1):
                    dtTmp.SetDate(iYear,iMon,1)
                    s=dtTmp.GetDateStr()[:-3]
                    self.objRegNode.AddCol(self.dCfg,s)
                    lMon.append(s)
            for fid in dFunc.keys():
                self.objRegNode.AddRow(self.dCfg,fid,' '.join(dFunc[fid][1]))
            vtLog.vtLngCurWX(vtLog.DEBUG,'dCfg:%s'%(vtLog.pformat(self.dCfg)),self)
            iCountMon=len(lMon)
            bEqual=self.chcEqual.GetValue()
            for fid in dFunc.keys():
                dMonth=dVal[fid]
                #for iMon in xrange(iCountMon):
                #    self.objRegNode.SetRowColValIdx(self.dCfg,fid,iMon,dMonth[lMon[iMon]]['sum'])
                fGes=0.0
                for sMon in dMonth.keys():
                    #print dMonth[sMon]
                    #print sMon in lMon,sMon
                    #print fid
                    fGes+=dMonth[sMon]['sum']
                    if bEqual==False:
                        self.objRegNode.SetRowColVal(self.dCfg,fid,sMon,str(dMonth[sMon]['sum']))
                if bEqual:
                    fVal=fGes/float(iCountMon)
                    for sMon in lMon:
                        self.objRegNode.SetRowColVal(self.dCfg,fid,sMon,str(fVal))
            self.__setup__()
        except:
            self.__logTB__()
