#Boa:FramePanel:vXmlNodeWorkPackageMileStonesPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeWorkPackageMileStonesPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060908
# CVS-ID:       $Id: vXmlNodeWorkPackageMileStonesPanel.py,v 1.5 2013/06/03 06:11:57 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.vApps.vPrjPlan.images as imgPrjPlan

import sys

[wxID_VXMLNODEWORKPACKAGEMILESTONESPANEL, 
 wxID_VXMLNODEWORKPACKAGEMILESTONESPANELCBDN, 
 wxID_VXMLNODEWORKPACKAGEMILESTONESPANELCBUP, 
 wxID_VXMLNODEWORKPACKAGEMILESTONESPANELLSTRESULT, 
] = [wx.NewId() for _init_ctrls in range(4)]

class vXmlNodeWorkPackageMileStonesPanel(wx.Panel,vtXmlNodePanel):
    MAP_STATUS=[u'not reached', u'reached']
    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbUp, 0, border=0, flag=0)
        parent.AddWindow(self.cbDn, 0, border=4, flag=wx.TOP)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstResult, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.LEFT)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.TOP | wx.LEFT | wx.RIGHT | wx.ALIGN_CENTER)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_lstResult_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT, heading=u'',
              width=20)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'Description'), width=220)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=2, hgap=0, rows=2, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEWORKPACKAGEMILESTONESPANEL,
              name=u'vXmlNodeWorkPackageMileStonesPanel', parent=prnt,
              pos=wx.Point(0, 0), size=wx.Size(348, 180),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(340, 153))
        self.SetAutoLayout(True)

        self.lstResult = wx.ListCtrl(id=wxID_VXMLNODEWORKPACKAGEMILESTONESPANELLSTRESULT,
              name=u'lstResult', parent=self, pos=wx.Point(4, 4),
              size=wx.Size(297, 149), style=wx.LC_REPORT)
        self._init_coll_lstResult_Columns(self.lstResult)
        self.lstResult.Bind(wx.EVT_LIST_COL_CLICK,
              self.OnLstContenseListColClick,
              id=wxID_VXMLNODEWORKPACKAGEMILESTONESPANELLSTRESULT)
        self.lstResult.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstContenseListItemDeselected,
              id=wxID_VXMLNODEWORKPACKAGEMILESTONESPANELLSTRESULT)
        self.lstResult.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstContenseListItemSelected,
              id=wxID_VXMLNODEWORKPACKAGEMILESTONESPANELLSTRESULT)

        self.cbUp = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODEWORKPACKAGEMILESTONESPANELCBUP,
              bitmap=vtArt.getBitmap(vtArt.Up), name=u'cbUp', parent=self,
              pos=wx.Point(305, 46), size=wx.Size(31, 30), style=0)
        self.cbUp.Bind(wx.EVT_BUTTON, self.OnCbUpButton,
              id=wxID_VXMLNODEWORKPACKAGEMILESTONESPANELCBUP)

        self.cbDn = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODEWORKPACKAGEMILESTONESPANELCBDN,
              bitmap=vtArt.getBitmap(vtArt.Down), name=u'cbDn', parent=self,
              pos=wx.Point(305, 80), size=wx.Size(31, 30), style=0)
        self.cbDn.Bind(wx.EVT_BUTTON, self.OnCbDnButton,
              id=wxID_VXMLNODEWORKPACKAGEMILESTONESPANELCBDN)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vPrjPlan')
        self._init_ctrls(parent)
        self.selIdx=-1
        
        vtXmlNodePanel.__init__(self,lWidgets=[self.lstResult])
        
        self.bModified=False
        self.bAutoApply=False
        self.bBlock=False
        
        self.lResult=[]
        #self.lstMarkObjs=[]
        
        self.imgLst=wx.ImageList(16,16)
        self.imgLst.Add(imgPrjPlan.getMileStoneBitmap())
        self.imgLst.Add(imgPrjPlan.getMileStoneOkBitmap())
        self.lstResult.SetImageList(self.imgLst,wx.IMAGE_LIST_NORMAL)
        self.lstResult.SetImageList(self.imgLst,wx.IMAGE_LIST_SMALL)
        
        self.Move(pos)
        self.SetSize(size)
    def ClearInt(self):
        try:
            vtXmlNodePanel.ClearInt(self)
            # add code here
            self.selIdx=-1
            self.lstResult.DeleteAllItems()
            self.lResult=[]
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def __showResult__(self):
        self.selIdx=-1
        self.lstResult.DeleteAllItems()
        for l in self.lResult:
            if l[0]:
                iImg=1
            else:
                iImg=0
            idx=self.lstResult.InsertImageStringItem(sys.maxint,'',iImg)
            iCol=1
            for s in l[1]:
                self.lstResult.SetStringItem(idx,iCol,s)
                iCol+=1
    def SetNetDocs(self,d):
        # add code here
        if d.has_key('vHum'):
            #self.viUsr.SetDocTree(dd['doc'],dd['bNet'])
            pass
    def SetDoc(self,doc,bNet):
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        # add code here
        oReg=self.doc.GetReg('WrkPkgMileStone')
        l=oReg.GetTranslation4List()
        iCount=self.lstResult.GetColumnCount()
        for i in xrange(1,iCount):
            self.lstResult.DeleteColumn(1)
        iCol=1
        for s in l:
            if iCol==1:
                iWidth=120
            else:
                iWidth=240
            self.lstResult.InsertColumn(col=iCol, format=wx.LIST_FORMAT_LEFT,
                      heading=s, width=iWidth)
            iCol+=1
    def SetNode(self,node):
        try:
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            # add code here
            lResults=self.doc.getChilds(self.doc.getParent(self.node),'WrkPkgMileStone')
            dResultIDs={}
            oReg=self.doc.GetReg('WrkPkgMileStone')
            for c in lResults:
                try:
                    dResultIDs[long(self.doc.getKey(c))]=oReg.AddValuesToLst(c)
                except:
                    pass
            for c in self.doc.getChilds(self.node,'milestone'):
                sFid=self.doc.getAttribute(c,'fid')
                try:
                    fid=long(sFid)
                    nodeTarget=self.doc.getNodeByIdNum(fid)
                    oTarget=self.doc.GetRegByNode(nodeTarget)
                    try:
                        iStatus=oTarget.IsFinished(nodeTarget)
                    except:
                        del dResultIDs[fid]
                        continue
                    self.lResult.append([iStatus,dResultIDs[fid],fid])
                    del dResultIDs[fid]
                except:
                    pass
            keys=dResultIDs.keys()
            keys.sort()
            for k in keys:
                nodeTarget=self.doc.getNodeByIdNum(k)
                oTarget=self.doc.GetRegByNode(nodeTarget)
                try:
                    iStatus=oTarget.IsFinished(nodeTarget)
                except:
                    continue
                self.lResult.append([iStatus,dResultIDs[k],k])
                self.SetModified(True)
            self.__showResult__()
        except:
            self.__logTB__()
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if node is None:
                return
            # add code here
            for c in self.doc.getChilds(node,'milestone'):
                self.doc.deleteNode(c,node)
            for l in self.lResult:
                self.doc.createSubNodeDict(node,
                    {'tag':'milestone','val':self.MAP_STATUS[l[0]],'attr':('fid','%08d'%l[2])},
                    #{'tag':'desc','val':l[1]},
                    )
            self.GetNodeFin(node)
        except:
            self.__logTB__()
    def Apply(self,bDoApply=False):
        if vtXmlNodePanel.Apply(self,bDoApply=False)==False:
            return False
        # add code here
        return False
    def Cancel(self):
        vtXmlNodePanel.Cancel(self)
        # add code here
        self.SetModified(False)
    def OnChcStatusChoice(self, event):
        event.Skip()

    def OnCbApplyButton(self, event):
        #l=[self.chcStatus.GetSelection(),self.txtDesc.GetValue()]
        if self.selIdx<0:
            #self.lResult.append(l)
            return
        else:
            l=self.lResult[self.selIdx]
            #l[0]=self.chcStatus.GetSelection()
            self.lResult=self.lResult[:self.selIdx]+[l]+self.lResult[self.selIdx+1:]
        self.__showResult__()
        event.Skip()

    def OnCbDelButton(self, event):
        return
        iLen=len(self.lResult)
        if self.selIdx<0 or self.selIdx>=iLen:
            return
        self.lResult=self.lResult[:self.selIdx]+self.lResult[self.selIdx+1:]
        self.__showResult__()
        event.Skip()

    def OnCbUpButton(self, event):
        iLen=len(self.lResult)
        if self.selIdx<1 or self.selIdx>=iLen:
            return
        self.lResult=self.lResult[:self.selIdx-1]+[self.lResult[self.selIdx]]+[self.lResult[self.selIdx-1]]+self.lResult[self.selIdx+1:]
        self.__showResult__()
        self.SetModified(True)
        event.Skip()

    def OnCbDnButton(self, event):
        iLen=len(self.lResult)
        if self.selIdx<0 or self.selIdx>=(iLen-1):
            return
        self.lResult=self.lResult[:self.selIdx]+[self.lResult[self.selIdx+1]]+[self.lResult[self.selIdx]]+self.lResult[self.selIdx+2:]
        self.__showResult__()
        self.SetModified(True)
        event.Skip()

    def OnLstContenseListColClick(self, event):
        self.selIdx=-1
        event.Skip()

    def OnLstContenseListItemDeselected(self, event):
        self.selIdx=-1
        event.Skip()

    def OnLstContenseListItemSelected(self, event):
        self.selIdx=event.GetIndex()
        l=self.lResult[self.selIdx]
        #self.chcStatus.SetSelection(l[0])
        #self.txtDesc.SetValue(l[1][0])
        event.Skip()
    def Lock(self,flag):
        vtXmlNodePanel.Lock(self,flag)
        if flag:
            # add code here
            self.cbDn.Enable(False)
            self.cbUp.Enable(False)
            self.lstResult.Enable(False)
        else:
            # add code here
            self.cbDn.Enable(True)
            self.cbUp.Enable(True)
            self.lstResult.Enable(True)
