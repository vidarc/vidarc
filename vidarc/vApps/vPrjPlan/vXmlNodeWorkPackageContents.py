#----------------------------------------------------------------------------
# Name:         vXmlNodeWorkPackageContense.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060410
# CVS-ID:       $Id: vXmlNodeWorkPackageContents.py,v 1.2 2006/09/15 12:40:14 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.config.vcCust as vcCust
from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        from vidarc.vApps.vPrjPlan.vXmlNodeWorkPackageContentsPanel import vXmlNodeWorkPackageContentsPanel
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeWorkPackageContents(vtXmlNodeBase):
    NODE_ATTRS=[
            #('idx',None,'idx',None),
        ]
    def __init__(self,tagName='WrkPkgContents'):
        global _
        _=vtLgBase.assignPluginLang('vPrjPlan')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'contents')
    # ---------------------------------------------------------
    # specific
    def GetContentsList(self,node,lang=None):
        l=[]
        for c in self.doc.getChilds(node,'question'):
            fid=self.doc.getAttribute(c,'fid')
            nodeQuest=self.doc.getNodeByIdNum(long(fid))
            oReg=self.doc.GetRegByNode(nodeQuest)
            ll=[oReg.GetName(nodeQuest,lang=lang),
                oReg.GetDesc(nodeQuest,lang=lang)]
            l.append(ll)
        return l
    # ---------------------------------------------------------
    # inheritance
    def Is2Create(self):
        "create automatically if parent node is created"
        return True
    def IsSkip(self):
        "do not display node in tree"
        return True
    def Is2Add(self):
        "node can be created by tree content menu"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xeeIDAT8\x8d\xb5\x93An\x83@\x0cE\x9fgz\x91\\#R\x0e\x90\xe4(!9\x0c\
\x98SDY\xa2*\xcb\n\x95ez\x04\x96\x9c\x02\xdc\x05\x1d\xc4@\xa0R\xa3~\xc9\xd2\
\xd8c\x7f\xfd\xf1\xd7\x888\xcf+x\x03\xb0\xae\xb5\xbf\x0c\x8b\xf3\xe2BbV\x0c\
\xa1\xa9\x0eM\x9ajt\x17"R0F\x9e\xd5$\x97\x84\xaa\xac\x00\xd8\xee\xb6\x80r:o\
\x16e\xf4\x02\xac0\xb3\xc2\x00\xab\xca\xca\x00\x03l|\x1fj\xe1,\xce\xe3\x9e\
\xd3\xf68\xec\x0fQ>\x95\x0f\xcc\t4\xd5\x1f\xd9\xf0~\xbf\x8bu\xad\x8dw2\xc5\
\x8c\xe0t\xde\x0cK\x0c\xc3\x8b\xef\x07D\x9c\xc7\xba\xd6\xa6\xd2~\x83\xc81\
\xb6q\x8c<\xab\xfb\x069\x92g\xf5*\xd1\xff\xd9(\xce\xd34\x8di\xaa\x91uLl\x9c)\
\x08\xf8\xfc(\xedv\xbdE\xb5g{Z\xb4\xf1\xf1\xf5\x00 \xb9$\xac\xd98\xb8\xb0\
\xd8\xb1\x02q^\xe4\xd5\xef\xfc\r\x8fB\x9f\x10\xf9\xdb\xd4\xbc\x00\x00\x00\
\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        #return vXmlNodeWorkPackageEditDialog
        return None
    def GetAddDialogClass(self):
        #return vXmlNodeWorkPackageAddDialog
        return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeWorkPackageContentsPanel
        else:
            return None
