#Boa:FramePanel:vXmlNodeWorkPackageMileStonePanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeXXXPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      
# CVS-ID:       $Id: vXmlNodeWorkPackageMileStonePanel.py,v 1.5 2013/06/03 06:11:57 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputTree
import vidarc.tool.input.vtInputTreeInternal
import wx.lib.buttons

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

import vidarc.vApps.vPrjPlan.images as imgPrjPlan

import sys

[wxID_VXMLNODEWORKPACKAGEMILESTONEPANEL, 
 wxID_VXMLNODEWORKPACKAGEMILESTONEPANELCBAPPLY, 
 wxID_VXMLNODEWORKPACKAGEMILESTONEPANELCBDEL, 
 wxID_VXMLNODEWORKPACKAGEMILESTONEPANELCHCSTYLE, 
 wxID_VXMLNODEWORKPACKAGEMILESTONEPANELLBLWRKPKG, 
 wxID_VXMLNODEWORKPACKAGEMILESTONEPANELLSTFOLLOWUPS, 
 wxID_VXMLNODEWORKPACKAGEMILESTONEPANELVIWRKPKG, 
] = [wx.NewId() for _init_ctrls in range(7)]

class vXmlNodeWorkPackageMileStonePanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    MAP_STYLE=['reached','open']
    def _init_coll_bxsInfo_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.chcStyle, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.lblWrkPkg, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.viWrkPkg, 2, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbApply, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.cbDel, 0, border=4, flag=wx.LEFT)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstFollowUps, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsInfo, 1, border=4,
              flag=wx.EXPAND | wx.BOTTOM | wx.TOP | wx.RIGHT | wx.LEFT)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_lstFollowUps_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT, heading=u'Style',
              width=80)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=u'Work Package', width=120)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT, heading=u'id',
              width=80)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=0, rows=2, vgap=0)

        self.bxsInfo = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsInfo_Items(self.bxsInfo)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEWORKPACKAGEMILESTONEPANEL,
              name=u'vXmlNodeWorkPackageMileStonePanel', parent=prnt,
              pos=wx.Point(76, 49), size=wx.Size(345, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(337, 180))
        self.SetAutoLayout(True)

        self.lstFollowUps = wx.ListCtrl(id=wxID_VXMLNODEWORKPACKAGEMILESTONEPANELLSTFOLLOWUPS,
              name=u'lstFollowUps', parent=self, pos=wx.Point(4, 4),
              size=wx.Size(329, 138), style=wx.LC_REPORT)
        self._init_coll_lstFollowUps_Columns(self.lstFollowUps)
        self.lstFollowUps.Bind(wx.EVT_LIST_COL_CLICK,
              self.OnLstFollowUpsListColClick,
              id=wxID_VXMLNODEWORKPACKAGEMILESTONEPANELLSTFOLLOWUPS)
        self.lstFollowUps.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstFollowUpsListItemDeselected,
              id=wxID_VXMLNODEWORKPACKAGEMILESTONEPANELLSTFOLLOWUPS)
        self.lstFollowUps.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstFollowUpsListItemSelected,
              id=wxID_VXMLNODEWORKPACKAGEMILESTONEPANELLSTFOLLOWUPS)

        self.lblWrkPkg = wx.StaticText(id=wxID_VXMLNODEWORKPACKAGEMILESTONEPANELLBLWRKPKG,
              label=_(u'question / result'), name=u'lblWrkPkg', parent=self,
              pos=wx.Point(72, 146), size=wx.Size(68, 13),
              style=wx.ALIGN_RIGHT)

        self.cbApply = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODEWORKPACKAGEMILESTONEPANELCBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbApply', parent=self,
              pos=wx.Point(265, 146), size=wx.Size(31, 30), style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VXMLNODEWORKPACKAGEMILESTONEPANELCBAPPLY)

        self.cbDel = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODEWORKPACKAGEMILESTONEPANELCBDEL,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbDel', parent=self,
              pos=wx.Point(300, 146), size=wx.Size(31, 30), style=0)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VXMLNODEWORKPACKAGEMILESTONEPANELCBDEL)

        self.chcStyle = wx.Choice(choices=[_(u'open'), _(u'reached')],
              id=wxID_VXMLNODEWORKPACKAGEMILESTONEPANELCHCSTYLE,
              name=u'chcStyle', parent=self, pos=wx.Point(4, 146),
              size=wx.Size(64, 21), style=0)

        self.viWrkPkg = vidarc.tool.input.vtInputTreeInternal.vtInputTreeInternal(id=wxID_VXMLNODEWORKPACKAGEMILESTONEPANELVIWRKPKG,
              name=u'viWrkPkg', parent=self, pos=wx.Point(136, 146),
              size=wx.Size(125, 30), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vPrjPlan')
        self._init_ctrls(parent)
        self.selIdx=-1
        
        vtXmlNodePanel.__init__(self,lWidgets=[self.lstFollowUps])
        
        self.viWrkPkg.AddTreeCall('init','SetNodeInfos',['tag','name'])
        #self.viWrkPkg.AddTreeCall('init','SetValidInfo',['WorkPackage','Question'])
        self.viWrkPkg.AddTreeCall('init','SetGrouping',[],[('tag',''),('name','')])
        #self.viWrkPkg.SetTagNames2Base(['globalsdata','globalsDepartment'])
        #self.viWrkPkg.SetShowFullName(False)
        
        self.bModified=False
        self.bAutoApply=False
        self.bBlock=False
        
        self.lFollowUps=[]
        
        self.imgLst=wx.ImageList(16,16)
        self.imgLst.Add(vtArt.getBitmap(vtArt.Invisible))
        #self.imgLst.Add(imgPrjPlan.getFollowUpAtEndBitmap())
        self.imgLst.Add(vtArt.getBitmap(vtArt.Apply))
        self.imgLst.Add(imgPrjPlan.getFollowUpAtStartBitmap())
        self.lstFollowUps.SetImageList(self.imgLst,wx.IMAGE_LIST_NORMAL)
        self.lstFollowUps.SetImageList(self.imgLst,wx.IMAGE_LIST_SMALL)
        
        self.cbApply.SetBitmapLabel(vtArt.getBitmap(vtArt.ApplyAttr))
        self.cbDel.SetBitmapLabel(vtArt.getBitmap(vtArt.DelAttr))
        
        self.Move(pos)
        self.SetSize(size)
    def ClearInt(self):
        try:
            vtXmlNodePanel.ClearInt(self)
            # add code here
            self.selIdx=-1
            self.lstFollowUps.DeleteAllItems()
            self.chcStyle.SetSelection(0)
            self.viWrkPkg.Clear()
            self.lFollowUps=[]
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def __showFollowUps__(self):
        self.selIdx=-1
        self.lstFollowUps.DeleteAllItems()
        for l in self.lFollowUps:
            if l[0]<3:
                iImg=l[0]
            else:
                iImg=0
            idx=self.lstFollowUps.InsertImageStringItem(sys.maxint,'',iImg)
            if l[1]>=0:
                nodeTmp=self.doc.getNodeByIdNum(l[1])
            else:
                nodeTmp=None
            if nodeTmp is not None:
                sName=self.doc.getNodeText(nodeTmp,'tag')
            else:
                sName='????'
            self.lstFollowUps.SetStringItem(idx,1,sName)
            self.lstFollowUps.SetStringItem(idx,2,'%08d'%l[1])
    def SetNetDocs(self,d):
        # add code here
        if d.has_key('vHum'):
            #dd=d['vHum']
            #self.viUsr.SetDocTree(dd['doc'],dd['bNet'])
            pass
    def SetDoc(self,doc,bNet):
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        # add code here
        self.viWrkPkg.SetDoc(doc,bNet)
        self.viWrkPkg.SetDocTree(doc,bNet)
    def OnGotContent(self,evt):
        evt.Skip()
        try:
            self.viWrkPkg.SetNodeTree(self.doc.getBaseNode())
        except:
            vtLog.vtLngTB(self.GetName())
    def SetNode(self,node):
        try:
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            # add code here
            for c in self.doc.getChilds(self.node,'WrkPkgQuestRes'):
                try:
                    iStyle=self.MAP_STYLE.index(self.doc.getText(c))
                except:
                    iStyle=0
                #iId=self.doc.getNodeText(c,'WrkPkg')
                try:
                    sId=self.doc.getAttribute(c,'fid')
                    iId=long(sId)
                except:
                    iId=-2
                if iId<0:
                    continue
                if self.doc.getNodeByIdNum(iId) is None:
                    continue
                self.lFollowUps.append([iStyle,iId])
            self.__showFollowUps__()
        except:
            self.__logTB__()
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if node is None:
                return
            # add code here
            for c in self.doc.getChilds(node,'WrkPkgQuestRes'):
                self.doc.deleteNode(c,node)
            #c=self.doc.createSubNode(node,'followUps')
            c=node
            for l in self.lFollowUps:
                self.doc.createSubNodeDict(c,
                    {'tag':'WrkPkgQuestRes','val':self.MAP_STYLE[l[0]],'attr':['fid','%08d'%l[1]]},
                    )
            self.GetNodeFin(node)
        except:
            self.__logTB__()
    def Apply(self,bDoApply=False):
        if vtXmlNodePanel.Apply(self,bDoApply=False)==False:
            return False
        # add code here
        return False
    def Cancel(self):
        vtXmlNodePanel.Cancel(self)
        # add code here
        self.SetModified(False)
    def OnLstFollowUpsListColClick(self, event):
        event.Skip()

    def OnLstFollowUpsListItemDeselected(self, event):
        event.Skip()

    def OnLstFollowUpsListItemSelected(self, event):
        self.selIdx=event.GetIndex()
        l=self.lFollowUps[self.selIdx]
        self.chcStyle.SetSelection(l[0])
        self.viWrkPkg.SetValueID('',l[1])
        event.Skip()

    def OnCbApplyButton(self, event):
        event.Skip()
        try:
            id=long(self.viWrkPkg.GetID())
        except:
            return
        bFound=False
        for l in self.lFollowUps:
            if l[1]==id:
                # edit
                l[0]=self.chcStyle.GetSelection()
                bFound=True
                break
        if bFound==False:
            l=[self.chcStyle.GetSelection(),id]
            self.lFollowUps.append(l)
        self.__showFollowUps__()
        self.SetModified(True)
    def OnCbDelButton(self, event):
        iLen=len(self.lFollowUps)
        if self.selIdx<0 or self.selIdx>=iLen:
            return
        self.lFollowUps=self.lFollowUps[:self.selIdx]+self.lFollowUps[self.selIdx+1:]
        self.__showFollowUps__()
        self.SetModified(True)
        event.Skip()
    def Lock(self,flag):
        vtXmlNodePanel.Lock(self,flag)
        if flag:
            # add code here
            self.chcStyle.Enable(False)
            self.cbDel.Enable(False)
            self.cbApply.Enable(False)
            self.lstFollowUps.Enable(False)
        else:
            # add code here
            self.chcStyle.Enable(True)
            self.cbDel.Enable(True)
            self.cbApply.Enable(True)
            self.lstFollowUps.Enable(True)
