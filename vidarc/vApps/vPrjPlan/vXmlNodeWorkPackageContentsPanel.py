#Boa:FramePanel:vXmlNodeWorkPackageContentsPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeWorkPackageContentsPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      
# CVS-ID:       $Id: vXmlNodeWorkPackageContentsPanel.py,v 1.5 2013/06/03 06:11:57 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

#from vidarc.tool.net.vNetXmlWxGui import *
#import vidarc.tool.xml.vtXmlDom as vtXmlDom
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import sys

[wxID_VXMLNODEWORKPACKAGECONTENTSPANEL, 
 wxID_VXMLNODEWORKPACKAGECONTENTSPANELCBAPPLY, 
 wxID_VXMLNODEWORKPACKAGECONTENTSPANELCBDEL, 
 wxID_VXMLNODEWORKPACKAGECONTENTSPANELCBDN, 
 wxID_VXMLNODEWORKPACKAGECONTENTSPANELCBUP, 
 wxID_VXMLNODEWORKPACKAGECONTENTSPANELCHCSTATUS, 
 wxID_VXMLNODEWORKPACKAGECONTENTSPANELLSTCONTENTS, 
 wxID_VXMLNODEWORKPACKAGECONTENTSPANELTXTDESC, 
] = [wx.NewId() for _init_ctrls in range(8)]

class vXmlNodeWorkPackageContentsPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    MAP_STATUS=[u'sleep', u'active', u'finished']
    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbUp, 0, border=0, flag=0)
        parent.AddWindow(self.cbDn, 0, border=4, flag=wx.TOP)

    def _init_coll_bxsInfo_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.chcStatus, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.txtDesc, 2, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbApply, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.cbDel, 0, border=4, flag=wx.LEFT)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstContents, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.LEFT)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.TOP | wx.LEFT | wx.RIGHT | wx.ALIGN_CENTER)
        parent.AddSizer(self.bxsInfo, 1, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_lstContents_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT, heading=u'',
              width=20)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=u'Description', width=220)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=2, hgap=0, rows=2, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsInfo = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsInfo_Items(self.bxsInfo)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEWORKPACKAGECONTENTSPANEL,
              name=u'vXmlNodeWorkPackageContentsPanel', parent=prnt,
              pos=wx.Point(0, 0), size=wx.Size(348, 180),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(340, 153))
        self.SetAutoLayout(True)

        self.lstContents = wx.ListCtrl(id=wxID_VXMLNODEWORKPACKAGECONTENTSPANELLSTCONTENTS,
              name=u'lstContents', parent=self, pos=wx.Point(4, 4),
              size=wx.Size(297, 111), style=wx.LC_REPORT)
        self._init_coll_lstContents_Columns(self.lstContents)
        self.lstContents.Bind(wx.EVT_LIST_COL_CLICK,
              self.OnlstContentsListColClick,
              id=wxID_VXMLNODEWORKPACKAGECONTENTSPANELLSTCONTENTS)
        self.lstContents.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnlstContentsListItemDeselected,
              id=wxID_VXMLNODEWORKPACKAGECONTENTSPANELLSTCONTENTS)
        self.lstContents.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnlstContentsListItemSelected,
              id=wxID_VXMLNODEWORKPACKAGECONTENTSPANELLSTCONTENTS)

        self.chcStatus = wx.Choice(choices=[_(u'sleep'), _(u'active'),
              _(u'finished')],
              id=wxID_VXMLNODEWORKPACKAGECONTENTSPANELCHCSTATUS,
              name=u'chcStatus', parent=self, pos=wx.Point(4, 119),
              size=wx.Size(74, 21), style=0)
        self.chcStatus.Bind(wx.EVT_CHOICE, self.OnChcStatusChoice,
              id=wxID_VXMLNODEWORKPACKAGECONTENTSPANELCHCSTATUS)

        self.txtDesc = wx.TextCtrl(id=wxID_VXMLNODEWORKPACKAGECONTENTSPANELTXTDESC,
              name=u'txtDesc', parent=self, pos=wx.Point(82, 119),
              size=wx.Size(144, 30), style=0, value=u'')

        self.cbApply = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODEWORKPACKAGECONTENTSPANELCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.ApplyAttr), name=u'cbApply', parent=self,
              pos=wx.Point(230, 119), size=wx.Size(31, 30), style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VXMLNODEWORKPACKAGECONTENTSPANELCBAPPLY)

        self.cbDel = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODEWORKPACKAGECONTENTSPANELCBDEL,
              bitmap=vtArt.getBitmap(vtArt.DelAttr), name=u'cbDel', parent=self,
              pos=wx.Point(265, 119), size=wx.Size(31, 30), style=0)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VXMLNODEWORKPACKAGECONTENTSPANELCBDEL)

        self.cbUp = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODEWORKPACKAGECONTENTSPANELCBUP,
              bitmap=vtArt.getBitmap(vtArt.Up), name=u'cbUp', parent=self,
              pos=wx.Point(305, 27), size=wx.Size(31, 30), style=0)
        self.cbUp.Bind(wx.EVT_BUTTON, self.OnCbUpButton,
              id=wxID_VXMLNODEWORKPACKAGECONTENTSPANELCBUP)

        self.cbDn = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODEWORKPACKAGECONTENTSPANELCBDN,
              bitmap=vtArt.getBitmap(vtArt.Down), name=u'cbDn', parent=self,
              pos=wx.Point(305, 61), size=wx.Size(31, 30), style=0)
        self.cbDn.Bind(wx.EVT_BUTTON, self.OnCbDnButton,
              id=wxID_VXMLNODEWORKPACKAGECONTENTSPANELCBDN)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        self._init_ctrls(parent)
        self.selIdx=-1
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        
        self.bModified=False
        self.bAutoApply=False
        self.bBlock=False
        
        self.lContents=[]
        
        self.imgLst=wx.ImageList(16,16)
        self.imgLst.Add(vtArt.getBitmap(vtArt.Invisible))
        self.imgLst.Add(vtArt.getBitmap(vtArt.Edit))
        self.imgLst.Add(vtArt.getBitmap(vtArt.Apply))
        self.lstContents.SetImageList(self.imgLst,wx.IMAGE_LIST_NORMAL)
        self.lstContents.SetImageList(self.imgLst,wx.IMAGE_LIST_SMALL)
        
        self.Move(pos)
        self.SetSize(size)
    def Lock(self,flag):
        vtXmlNodePanel.Lock(self,flag)
        if flag:
            pass
        else:
            pass
    def ClearInt(self):
        try:
            vtXmlNodePanel.ClearInt(self)
            self.selIdx=-1
            self.lstContents.DeleteAllItems()
            self.chcStatus.SetSelection(0)
            self.txtDesc.SetValue('')
            self.lContents=[]
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def __showContents__(self):
        self.selIdx=-1
        self.lstContents.DeleteAllItems()
        for l in self.lContents:
            if l[0]<3:
                iImg=l[0]
            else:
                iImg=0
            idx=self.lstContents.InsertImageStringItem(sys.maxint,'',iImg)
            iCol=1
            for s in l[1]:
                self.lstContents.SetStringItem(idx,iCol,s)
                iCol+=1
            self.lstContents.SetItemData(idx,l[2])
    def SetDoc(self,doc,bNet):
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        # add code here
        oReg=self.doc.GetReg('WrkPkgQuestion')
        l=oReg.GetTranslation4List()
        iCount=self.lstContents.GetColumnCount()
        for i in xrange(1,iCount):
            self.lstContents.DeleteColumn(1)
        iCol=1
        for s in l:
            self.lstContents.InsertColumn(col=iCol, format=wx.LIST_FORMAT_LEFT,
                      heading=s, width=-1)
            iCol+=1
    def SetNode(self,node):
        #if self.bModified==True:
        #    if self.bAutoApply:
        #        self.GetNode(self.node)
        #    else:
                # ask
        #        dlg=wx.MessageDialog(self,u'Do you to apply modified data?' ,
        #                    _(u'vLog Info'),
        #                    wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
        #        if dlg.ShowModal()==wx.ID_YES:
        #            self.GetNode(self.node)
        try:
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            # add code here
            lQuestions=self.doc.getChilds(self.doc.getParent(self.node),'WrkPkgQuestion')
            dQuestionIDs={}
            oReg=self.doc.GetReg('WrkPkgQuestion')
            for c in lQuestions:
                try:
                    dQuestionIDs[long(self.doc.getKey(c))]=oReg.AddValuesToLst(c)
                except:
                    pass
            for c in self.doc.getChilds(self.node,'question'):
                try:
                    iStatus=self.MAP_STATUS.index(self.doc.getText(c))
                except:
                    iStatus=0
                sFid=self.doc.getAttribute(c,'fid')
                try:
                    fid=long(sFid)
                    self.lContents.append([iStatus,dQuestionIDs[fid],fid])
                    del dQuestionIDs[fid]
                except:
                    pass
            keys=dQuestionIDs.keys()
            keys.sort()
            for k in keys:
                self.lContents.append([0,dQuestionIDs[k],k])
            self.__showContents__()
        except:
            self.__logTB__()
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if node is None:
                return
            # add code here
            for c in self.doc.getChilds(node,'question'):
                self.doc.deleteNode(c,node)
            for l in self.lContents:
                self.doc.createSubNodeDict(node,
                    {'tag':'question','val':self.MAP_STATUS[l[0]],'attr':('fid','%08d'%l[2])},
                    )
            self.GetNodeFin(node)
        except:
            self.__logTB__()
    def Apply(self,bDoApply=False):
        if vtXmlNodePanel.Apply(self,bDoApply=False)==False:
            return False
        # add code here
        return False
    def Cancel(self):
        vtXmlNodePanel.Cancel(self)
        # add code here
        self.SetModified(False)
    def OnChcStatusChoice(self, event):
        event.Skip()

    def OnCbApplyButton(self, event):
        #l=[self.chcStatus.GetSelection(),self.txtDesc.GetValue()]
        if self.selIdx<0:
            #self.lContents.append(l)
            return
        else:
            l=self.lContents[self.selIdx]
            l[0]=self.chcStatus.GetSelection()
            #n=self.doc.getNodeByIdNum(self.lContents[self.selIdx][2])
            #if n is not None:
            #    o=self.doc.GetRegByNode(n)
            #    o.SetStatus(n,l[0])
            self.lContents=self.lContents[:self.selIdx]+[l]+self.lContents[self.selIdx+1:]
        self.__showContents__()
        event.Skip()

    def OnCbDelButton(self, event):
        return
        iLen=len(self.lContents)
        if self.selIdx<0 or self.selIdx>=iLen:
            return
        self.lContents=self.lContents[:self.selIdx]+self.lContents[self.selIdx+1:]
        self.__showContents__()
        event.Skip()

    def OnCbUpButton(self, event):
        iLen=len(self.lContents)
        if self.selIdx<1 or self.selIdx>=iLen:
            return
        self.lContents=self.lContents[:self.selIdx-1]+[self.lContents[self.selIdx]]+[self.lContents[self.selIdx-1]]+self.lContents[self.selIdx+1:]
        self.__showContents__()
        event.Skip()

    def OnCbDnButton(self, event):
        iLen=len(self.lContents)
        if self.selIdx<0 or self.selIdx>=(iLen-1):
            return
        self.lContents=self.lContents[:self.selIdx]+[self.lContents[self.selIdx+1]]+[self.lContents[self.selIdx]]+self.lContents[self.selIdx+2:]
        self.__showContents__()
        event.Skip()

    def OnlstContentsListColClick(self, event):
        self.selIdx=-1
        event.Skip()

    def OnlstContentsListItemDeselected(self, event):
        self.selIdx=-1
        event.Skip()

    def OnlstContentsListItemSelected(self, event):
        self.selIdx=event.GetIndex()
        l=self.lContents[self.selIdx]
        self.chcStatus.SetSelection(l[0])
        self.txtDesc.SetValue(l[1][0])
        event.Skip()
