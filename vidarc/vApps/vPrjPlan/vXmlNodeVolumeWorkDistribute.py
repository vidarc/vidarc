#----------------------------------------------------------------------------
# Name:         vXmlNodeVolumeWorkDistribute.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060824
# CVS-ID:       $Id: vXmlNodeVolumeWorkDistribute.py,v 1.4 2010/03/03 02:17:14 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase
import types

import vidarc.tool.xml.vtXmlHierarchy as vtXmlHierarchy#import getTagNames

from vidarc.tool.xml.vtXmlNodeTag import *
try:
    if vcCust.is2Import(__name__):
        from vXmlNodeVolumeWorkDistributePanel import *
        #from vXmlNodeVolumeWorkDistributeEditDialog import *
        #from vXmlNodeVolumeWorkDistributeAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeVolumeWorkDistribute(vtXmlNodeTag):
    #NODE_ATTRS=[
    #        ('Tag',None,'tag',None),
    #        ('Name',None,'name',None),
    #    ]
    FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='VolWrkDist'):
        global _
        _=vtLgBase.assignPluginLang('vPrjPlan')
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'volume work distribute')
    # ---------------------------------------------------------
    # specific
    def GetFuncTagNameByID(self,fid):
        netGlb,node=self.doc.GetNode(fid)
        if node is not None:
            return netGlb.GetTagNames(node)
        else:
            return '???'
    def GetColCount(self,dCfg):
        if 'lCol' in dCfg:
            return len(dCfg['lCol'])
        else:
            return 0
    def AddRow(self,dCfg,fid,sTag=None,sData=None):
        if sTag is None:
            sTag=self.GetFuncTagNameByID(fid)
        if type(fid)==types.StringType:
            fid,appl=self.doc.convForeign(fid)
        if 'dRowID' not in dCfg:
            dCfg['dRowID']={}
        dCfg['dRowID'][fid]=sTag
        dCfg['lRowMap']=[(v,k) for k,v in dCfg['dRowID'].items()]
        dCfg['lRowMap'].sort()
        dCfg['lRow']=[v for v,k in dCfg['lRowMap']]
        if 'dRowVal' not in dCfg:
            dCfg['dRowVal']={}
        if sData is None:
            dCfg['dRowVal'][fid]=['0.0' for i in xrange(self.GetColCount(dCfg))]
        else:
            sVals=sData.split(',')
            iCols=self.GetColCount(dCfg)
            for i in xrange(len(sVals),iCols):
                sVals.append('0.0')
            dCfg['dRowVal'][fid]=sVals[:iCols]
        dCfg['val']=[dCfg['dRowVal'][k] for v,k in dCfg['lRowMap']]
    def DelRowByIdx(self,dCfg,i):
        try:
            v,fid=dCfg['lRowMap'][i]
            del dCfg['dRowID'][fid]
            del dCfg['lRow'][i]
            del dCfg['lRowMap'][i]
            del dCfg['val'][i]
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def AddCol(self,dCfg,sDate):
        lCol=dCfg['lCol']
        if sDate in lCol:
            return lCol.index(sDate)
        iPos=-1
        for i in xrange(len(lCol)):
            if lCol[i]>sDate:
                iPos=i
                break
        if iPos==-1:
            lCol.append(sDate)
            for lVal in dCfg['val']:
                lVal.append('0.0')
            return len(lCol)-1
        else:
            lCol.insert(iPos,sDate)
            for lVal in dCfg['val']:
                lVal.insert(iPos,'0.0')
        return iPos
    def CopyCol(self,dCfg,iColSrc,iColDest,fFact=1.0):
        for lVal in dCfg['val']:
            lVal[iColDest]=str(float(lVal[iColSrc])*fFact)
    def DelColByIdx(self,dCfg,i):
        try:
            del dCfg['lCol'][i]
            for lVal in dCfg['val']:
                del lVal[i]
        except:
            pass
    def GetCol(self,dCfg,i):
        try:
            if 'lCol' in dCfg:
                return dCfg['lCol'][i]
        except:
            return None
    def GetRow(self,dCfg,i):
        try:
            return dCfg['lRowMap'][i][1]
        except:
            return None
    def SetRowColValIdx(self,dCfg,fid,iCol,val):
        if 'dRowVal' in dCfg:
            d=dCfg['dRowVal']
            if fid in d:
                d[fid][iCol]=val
                return 0
        return -1
    def SetRowColVal(self,dCfg,fid,sDate,val):
        lCol=dCfg['lCol']
        if sDate in lCol:
            iCol=lCol.index(sDate)
            if 'dRowVal' in dCfg:
                d=dCfg['dRowVal']
                if fid in d:
                    d[fid][iCol]=val
                    return 0
            return -1
        return -2
    def GetValuesEmpty(self):
        dCfg={  'lRow':[],
                'dRowID':{},
                'dRowVal':{},
                'lRowMap':[],
                'lCol':[],
                'val':[],
            }
        return dCfg
    def GetValues(self,node):
        lRow=[]
        dRowID={}
        lCol=[]
        lVal=[]
        dCfg={  'lRow':lRow,
                'dRowID':dRowID,
                'lRowMap':[],
                'lCol':lCol,
                'val':lVal,
            }
        try:
            nodeData=self.doc.getChild(node,'data')
            if nodeData is not None:
                lVal=self.doc.getNodeText(nodeData,'col')
                for sVal in lVal.split(','):
                    self.AddCol(dCfg,sVal)
                for cRow in self.doc.getChilds(nodeData,'row'):
                    fid=self.doc.getAttribute(cRow,'fid')
                    sVal=self.doc.getText(cRow)
                    #print fid,sVal
                    self.AddRow(dCfg,fid,sData=sVal)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return dCfg
    def SetValues(self,node,dCfg):
        if node is None:
            return
        try:
            nodeData=self.doc.getChild(node,'data')
            if nodeData is not None:
                self.doc.deleteNode(nodeData,node)
            nodeData=self.doc.getChildForced(node,'data')
            cCol=self.doc.createSubNode(nodeData,'col')
            self.doc.setText(cCol,','.join(dCfg['lCol']))
            for v,k in dCfg['lRowMap']:
                lVal=dCfg['dRowVal'][k]
                cRow=self.doc.createSubNode(nodeData,'row')
                #self.doc.setAttribute(cRow,'fid',k)
                self.doc.setForeignKey(cRow,'fid',k,'vGlobals')
                self.doc.setText(cRow,','.join(lVal))
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    #def GetAttrFilterTypes(self):
    #    """ shall return something like:
    #         [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
    #          ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
    #    """
    #    return None
    #def GetTranslation(self,name):
    #    return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return False
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsNotContained(self):
        "do not add to listbook"
        return True
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01tIDAT8\x8d\x95\x93ON\xc2@\x18\xc5\xdf7\xe3\tt\xaf\xfc\xb9\x80\x1a\
\xd7\x1a\xf4\x02\xda\x86-\xea\tH\x08[Bq\xc3\xae\xb8\xe0\x00\xfe\x89K\xc4\xa8\
a\xa7\x12\xc2\x058\x80\x12\xe3\x05\xda\x03t\x9e\x0b\x98\n\xb4\xd5\xf8\x92/\
\x9d\xcc\xbc\xef7\x93\xef\xa5"J#M\x07\xfb\r\x02C\x00\xc0p\xe8A\xd4\x91\xa4\
\xf9Tj\xf7\\e\xb7\x8cb\xa1\xf8\x9be\x068\xd8o0\xb7U!\x00\xd0\xbc0\xb7U\xa1\
\x92\x11z\xf7=L\xa7S\x1c\x96.@\xd3\x88\xcf\xcfN+\xa4y!\x00\x08\xe0\xf1\xf9q\
\x0f\xfd\x87>^\xdf\x0c\xdc\x93\rT\xab\xd5\xc4M\x93\xc9\x04~\xc7G\x10l\xc3q\
\xd6\xf1\xfe\xf1\x8e\xab\xeb\x1b\xc1l\x06\x1e]\xc7\xe5x4\xa6(\r[\xbe\xef\xb3\
\xddn\xc7{a\x10\xb2^\xabsw\xa7F`HQ\x1a1\xe0\xee\xf6.6\x02\x88\xd7\xddnw\t\
\x1a\x06!]\xc7\xfd\xf1\x8a\xd2p\x1d\x97_\x9f_\xa9\x80\xac\x02<\xce\xbd\xc9\
\xdbm-6x\x00\xbd\x14p\x1c\xe3\xdb\xeb\xc7l\xaaJ\xcb\xe2\xb7i"6M\xc4\x96\xd2\
\xd2RZ\x9a&\xe2\xe2p\xd7\xb2\xf2\xb5\xc6\xd6\x1cde!v?\x06\x94\x0e\x0b\tc\x16\
|\xf1%"J\xc39>\xe6e\xe7\x12\x9b\xf9\x9c\x00\x00MDxY\xed+\xb2\x13\x1d<\r\xfe\
\x95\xc2R\x8c\xa24\xf2\xf9s\x8eGc\x86A\x98H \x19!\x98\x00X\xc8j\x94\x7f\x95d\
\xfd\xce\xbf\x89&\xa2\x8d\xf9\x1b\x03\x98\xb5\x9e\x17^\x81[\x00\x00\x00\x00I\
END\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            d=vtXmlNodeTag.GetAddDialogClass(self).copy()
            d['pnCls']=vtXmlNodeTag.GetPanelClass(self)
            return d
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            d=vtXmlNodeTag.GetAddDialogClass(self).copy()
            d['pnCls']=vtXmlNodeTag.GetPanelClass(self)
            return d
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeVolumeWorkDistributePanel
        else:
            return None
