#Boa:FramePanel:vXmlNodeWorkPackageCostPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeWorkPackageCostPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060907
# CVS-ID:       $Id: vXmlNodeWorkPackageCostPanel.py,v 1.4 2013/06/03 06:11:57 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputFloat
import vidarc.tool.input.vtInputGrid
import wx.lib.buttons
import vidarc.tool.time.vtTimeDateGM
import vidarc.tool.input.vtInputGridSmall
import vidarc.tool.input.vtInputTree
import vidarc.tool.input.vtInputTreeInternal
import vidarc.tool.input.vtInputTreeMLInternal
import vidarc.tool.input.vtInputTextML
import vidarc.tool.input.vtInputText

import sys

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.time.vtTime import vtDateTime
from vidarc.tool.xml.vtXmlHierarchy import getTagNames

[wxID_VXMLNODEWORKPACKAGECOSTPANEL, 
 wxID_VXMLNODEWORKPACKAGECOSTPANELCBADDDATE, 
 wxID_VXMLNODEWORKPACKAGECOSTPANELCBADDFUNC, 
 wxID_VXMLNODEWORKPACKAGECOSTPANELCBDELDATE, 
 wxID_VXMLNODEWORKPACKAGECOSTPANELCBDELFUNC, 
 wxID_VXMLNODEWORKPACKAGECOSTPANELGRDCOST, 
 wxID_VXMLNODEWORKPACKAGECOSTPANELLBLCOST, 
 wxID_VXMLNODEWORKPACKAGECOSTPANELLBLDATE, 
 wxID_VXMLNODEWORKPACKAGECOSTPANELVICOST, 
 wxID_VXMLNODEWORKPACKAGECOSTPANELVIDATE, 
] = [wx.NewId() for _init_ctrls in range(10)]

class vXmlNodeWorkPackageCostPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_bxsDateBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbAddDate, 0, border=0, flag=0)
        parent.AddWindow(self.cbDelDate, 0, border=4, flag=wx.LEFT)

    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblDate, (0, 3), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viCost, (0, 1), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.viDate, (0, 4), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.lblCost, (0, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.grdCost, (1, 0), border=0, flag=wx.EXPAND,
              span=(2, 6))
        parent.AddSizer(self.bxsDateBt, (0, 5), border=0, flag=0, span=(1, 1))
        parent.AddSizer(self.bxsFuncBt, (0, 2), border=0, flag=0, span=(1, 1))

    def _init_coll_bxsFuncBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbAddFunc, 0, border=0, flag=0)
        parent.AddWindow(self.cbDelFunc, 0, border=4, flag=wx.LEFT)

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=4, vgap=4)

        self.bxsDateBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsFuncBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_gbsData_Items(self.gbsData)
        self._init_coll_bxsDateBt_Items(self.bxsDateBt)
        self._init_coll_bxsFuncBt_Items(self.bxsFuncBt)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEWORKPACKAGECOSTPANEL,
              name=u'vXmlNodeWorkPackageCostPanel', parent=prnt, pos=wx.Point(0,
              0), size=wx.Size(469, 118),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(461, 91))
        self.SetAutoLayout(True)

        self.viCost = vidarc.tool.input.vtInputTreeMLInternal.vtInputTreeMLInternal(id=wxID_VXMLNODEWORKPACKAGECOSTPANELVICOST,
              name=u'viCost', parent=self, pos=wx.Point(26, 0),
              size=wx.Size(122, 30), style=0)

        self.grdCost = vidarc.tool.input.vtInputGridSmall.vtInputGridSmall(id=wxID_VXMLNODEWORKPACKAGECOSTPANELGRDCOST,
              name=u'grdCost', parent=self, pos=wx.Point(0, 34),
              size=wx.Size(416, 44))
        self.grdCost.SetMinSize(wx.Size(-1, -1))
        self.grdCost.Bind(vidarc.tool.input.vtInputGrid.vEVT_VTINPUT_GRID_INFO_SELECTED,
              self.OnGrdCostVtinputGridInfoSelected,
              id=wxID_VXMLNODEWORKPACKAGECOSTPANELGRDCOST)
        self.grdCost.Bind(vidarc.tool.input.vtInputGrid.vEVT_VTINPUT_GRID_INFO_CHANGED,
              self.OnGrdCostVtinputGridInfoChanged,
              id=wxID_VXMLNODEWORKPACKAGECOSTPANELGRDCOST)

        self.viDate = vidarc.tool.time.vtTimeDateGM.vtTimeDateGM(id=wxID_VXMLNODEWORKPACKAGECOSTPANELVIDATE,
              name=u'viDate', parent=self, pos=wx.Point(248, 0),
              size=wx.Size(98, 30), style=0)

        self.lblDate = wx.StaticText(id=wxID_VXMLNODEWORKPACKAGECOSTPANELLBLDATE,
              label=u'date', name=u'lblDate', parent=self, pos=wx.Point(222, 0),
              size=wx.Size(22, 30), style=wx.ALIGN_RIGHT)
        self.lblDate.SetMinSize(wx.Size(-1, -1))

        self.cbAddFunc = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODEWORKPACKAGECOSTPANELCBADDFUNC,
              bitmap=vtArt.getBitmap(vtArt.Add), name=u'cbAddFunc', parent=self,
              pos=wx.Point(152, 0), size=wx.Size(31, 30), style=0)
        self.cbAddFunc.Bind(wx.EVT_BUTTON, self.OnCbAddFuncButton,
              id=wxID_VXMLNODEWORKPACKAGECOSTPANELCBADDFUNC)

        self.cbDelFunc = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODEWORKPACKAGECOSTPANELCBDELFUNC,
              bitmap=vtArt.getBitmap(vtArt.Del), name=u'cbDelFunc', parent=self,
              pos=wx.Point(187, 0), size=wx.Size(31, 30), style=0)
        self.cbDelFunc.Bind(wx.EVT_BUTTON, self.OnCbDelFuncButton,
              id=wxID_VXMLNODEWORKPACKAGECOSTPANELCBDELFUNC)

        self.lblCost = wx.StaticText(id=wxID_VXMLNODEWORKPACKAGECOSTPANELLBLCOST,
              label=u'cost', name=u'lblCost', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(22, 30), style=wx.ALIGN_RIGHT)
        self.lblCost.SetMinSize(wx.Size(-1, -1))

        self.cbAddDate = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODEWORKPACKAGECOSTPANELCBADDDATE,
              bitmap=vtArt.getBitmap(vtArt.Add), name=u'cbAddDate', parent=self,
              pos=wx.Point(350, 0), size=wx.Size(31, 30), style=0)
        self.cbAddDate.Bind(wx.EVT_BUTTON, self.OnCbAddDateButton,
              id=wxID_VXMLNODEWORKPACKAGECOSTPANELCBADDDATE)

        self.cbDelDate = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODEWORKPACKAGECOSTPANELCBDELDATE,
              bitmap=vtArt.getBitmap(vtArt.Del), name=u'cbDelDate', parent=self,
              pos=wx.Point(385, 0), size=wx.Size(31, 30), style=0)
        self.cbDelDate.Bind(wx.EVT_BUTTON, self.OnCbDelDateButton,
              id=wxID_VXMLNODEWORKPACKAGECOSTPANELCBDELDATE)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vPrjPlan')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        
        #self.vitrArea.SetTagNames('mainDepartment','name')
        self.viCost.AddTreeCall('init','SetNodeInfos',['name'])
        self.viCost.AddTreeCall('init','SetGrouping',[],[('name','')])
        self.viCost.SetTagNames2Base(['globalsdata','globalsPrjPlan'])
        self.viCost.SetEnableMark(False)
        self.viCost.SetAppl('vGlobals')
        self.viDate.SetEnableMark(False)
        
        self.lLblRow=[]
        self.lLblCol=[]
        self.lVal=[]
        self.grdCost.SetColumnSize([150,100])
        
        self.gbsData.AddGrowableCol(0)
        self.gbsData.AddGrowableCol(1)
        self.gbsData.AddGrowableCol(4)
        #self.gbsData.AddGrowableRow(0)
        self.gbsData.AddGrowableRow(2)
        self.gbsData.Layout()
        self.gbsData.Fit(self)
        
        self.dtStart=vtDateTime(True)
        self.dtEnd=vtDateTime(True)
        self.dtTmp=vtDateTime(True)
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def ClearInt(self):
        try:
            vtXmlNodePanel.ClearInt(self)
            # add code here
            #self.viCostDef.Clear()
            self.lLblRow=[]
            self.lLblCol=[]
            self.lVal=[]
            self.iRow=-1
            self.iCol=-1
            self.grdCost.SetUp(self.lVal,self)
            #self.grdCost.Clear()
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def SetRegNode(self,obj):
        vtXmlNodePanel.SetRegNode(self,obj)
        #self.grdCost.Setup(obj.GetSetup(),None)
    def SetNetDocs(self,d):
        # add code here
        if d.has_key('vGlobals'):
            dd=d['vGlobals']
            self.viCost.SetDocTree(dd['doc'],dd['bNet'])
    def SetDoc(self,doc,bNet=False):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        
        # add code here
        self.viCost.SetDoc(doc,bExternal=True)
        
    def SetNode(self,node):
        try:
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            # add code here
            #tup=self.objRegNode.GetValues(node)
            #self.viCostDef.SetValue(tup[0])
            #self.viCostDef.SetModified(False)
            #oTrans=self.doc.GetRegisteredNode(self.objRegNode.GetTagNameTransition())
            self.dCfg=self.objRegNode.GetValues(node)
            
            #self.lLblRow=dCfg['lRow']
            #self.lLblCol=dCfg['lCol']
            self.lVal=[]
            self.__setup__()

        except:
            self.__logTB__()
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if node is None:
                return
            # add code here
            self.grdCost.Get()
            self.objRegNode.SetValues(node,self.dCfg)
            self.GetNodeFin(node)
        except:
            self.__logTB__()
    def Close(self):
        vtXmlNodePanel.Close(self)
        # add code here
        pass
    def Cancel(self):
        vtXmlNodePanel.Cancel(self)
        # add code here
        pass
    def Lock(self,flag):
        vtXmlNodePanel.Lock(self,flag)
        if flag:
            # add code here
            pass
        else:
            # add code here
            pass

    def OnCbSearchButton(self, event):
        event.Skip()
        if self.node is not None:
            
            node=self.doc.getParent(self.node)
            lDate=self.doc.GetDateLimit(node)
            if lDate[0] is None:
                self.txtStartDate.SetValue('')
            else:
                self.txtStartDate.SetValue(lDate[0])
            if lDate[1] is None:
                self.txtEndDate.SetValue('')
            else:
                self.txtEndDate.SetValue(lDate[1])
            dFunc=self.doc.GetFuncDict(node)
            self.chcFunc.Clear()
            for tup in dFunc.items():
                s=' '.join(tup[1])
                #iFID,sAppl=self.doc.convForeign(tup[0])
                iFID=tup[0]
                self.chcFunc.Append(s,iFID)
            try:
                self.chcFunc.SetSelection(0)
                wx.CallAfter(self.OnChcFuncChoice,None)
            except:
                pass
    def OnViCostDefVtinputGridsetupInfoSelected(self, event):
        event.Skip()

    def OnViCostDefVtinputGridsetupInfoChanged(self, event):
        event.Skip()

    def OnCbAddDateButton(self, event):
        event.Skip()
        sVal=self.viDate.GetValue()
        try:
            self.dtTmp.SetDateSmallStr(sVal)
            self.grdCost.Get()
            iCol=self.objRegNode.AddCol(self.dCfg,self.dtTmp.GetDateStr())
            #if self.iCol>=0:
            #    fFact=1.0+(self.viRate.GetValue()/100.0)
            #    self.objRegNode.CopyCol(self.dCfg,self.iCol,iCol,fFact)
            self.__setup__()
        except:
            self.__logTB__()
    def OnChcFuncChoice(self, event):
        if event is not None:
            event.Skip()
        idx=self.chcFunc.GetSelection()
        if idx>=0:
            id=self.chcFunc.GetClientData(idx)
            self.viCost.SetValueID('',id)
            #netGlb=self.doc.GetNetDoc('vGlobals')
            #nodeFunc=netGlb.
            
    def OnCbAddFuncButton(self, event):
        if event is not None:
            event.Skip()
        self.grdCost.Get()
        fid=self.viCost.GetForeignID()
        self.objRegNode.AddRow(self.dCfg,fid)
        iFID,sAppl=self.doc.convForeign(fid)
        sStart=self.viDate.GetValue()
        sEnd=self.viDate.GetValue()
        if len(sStart)>0 and len(sEnd)>0:
            netGlb,node=self.doc.GetNode(fid)
            if node is not None:
                try:
                    o=netGlb.GetRegByNode(node)
                    lDates=o.GetDatedLst(node)
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurWX(vtLog.DEBUG,'lDates:%s'%(vtLog.pformat(lDates)),self)
                    iStart=0
                    iEnd=len(lDates)
                    i=0
                    for tup in lDates:
                        if tup[0]<sStart:
                            iStart=i
                        if tup[0]>sEnd:
                            iEnd=i
                            break
                        i+=1
                    
                    for tup in lDates[iStart:iEnd]:
                        #if tup[0]>=sStart and tup[0]<=sEnd:
                            iCol=self.objRegNode.AddCol(self.dCfg,tup[0])
                            #if iCol>=0:
                            val='0.00'#o.GetRate(tup[1])
                            #self.objRegNode.SetRowColVal(self.dCfg,fid,tup[0],val)
                            self.objRegNode.SetRowColVal(self.dCfg,iFID,tup[0],val)
                except:
                    lDates=None
        
        #sTag=self.objRegNode.GetFuncTagNameByID(fid)
        #vtLog.CallStack('')
        #print fid
        #vtLog.pprint(self.dCfg)
        self.__setup__()
        
    def GetRowColType(self,row,col):
        """required for vtInputGrid"""
        if col==-1:
            return self.doc,None,'string'
        try:
            return self.doc,self.node,'float'
        except:
            pass
        return self.doc,self.node,'float'
    def GetRowColValue(self,row,col):
        """required for vtInputGrid"""
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'row:%02d col:%02d'%(row,col),self)
        try:
            return self.lVal[row][col]
        except:
            return None
    def SetRowColValue(self,row,col,val):
        """required for vtInputGrid"""
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'row:%02d col:%02d val:%s'%(row,col,str(val)),self)
        try:
            self.lVal[row][col]=val
        except:
            pass
    def GetColLabelValue(self,col,row=-1):
        """required for vtInputGrid"""
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'row:%02d col:%02d'%(row,col),self)
        try:
            return self.lLblCol[col]
        except:
            return ''
    def GetRowLabelValue(self,row):
        """required for vtInputGrid"""
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'row:%02d'%(row),self)
        try:
            return self.lLblRow[row]
        except:
            return ''
    def GetDataNodes(self):
        """required for vtInputGrid"""
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        return self.lVal
    def GetDataNode(self,row,col):
        """required for vtInputGrid"""
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'row:%02d col:%02d'%(row,col),self)
        return None
    def __setup__(self):
        lColSize=[]
        #self.dAttrs=self.viSetup.GetValueDict()
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%vtLog.pformat(self.dCfg),self)
        self.lLblRow=self.dCfg['lRow']
        self.lLblCol=self.dCfg['lCol']
        iRow=len(self.lLblRow)
        iCol=len(self.lLblCol)
        self.iCol=-1
        self.iRow=-1
        self.lVal=self.dCfg['val']
        if 0:
            for i in xrange(0,iRow+1):
                try:
                    self.lLblRow.append(dRow[i])
                except:
                    self.lLblRow.append(str(i))
                l=[]
                for j in xrange(0,iCol+1):
                    try:
                        l.append(self.lVal[i][j])
                    except:
                        l.append('0.0')
                ll.append(l)
        lCol=[150]
        for j in xrange(0,iCol+1):
            #try:
            #    self.lLblCol.append(dCol[j])
            #except:
            #    self.lLblCol.append(str(j))
            lCol.append(100)
        #if self.VERBOSE:
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%vtLog.pprint(ll),self)
        #self.lVal=ll
        #self.lVal=[]
        self.grdCost.SetColumnSize(lCol)
        self.grdCost.SetUp(self.lVal,self)

    def OnCbDelDateButton(self, event):
        event.Skip()
        if self.iCol>=0:
            self.objRegNode.DelColByIdx(self.dCfg,self.iCol)
            self.__setup__()

    def OnCbDelFuncButton(self, event):
        event.Skip()
        if self.iRow>=0:
            self.objRegNode.DelRowByIdx(self.dCfg,self.iRow)
            self.__setup__()

    def OnGrdCostVtinputGridInfoSelected(self, event):
        event.Skip()
        #vtLog.CallStack('')
        self.iRow=event.GetRow()
        self.iCol=event.GetCol()
        self.__updatePreview__()
    def __updatePreview__(self):
        try:
            fVal=self.grdCost.GetRowColValueActual(self.iRow,self.iCol)
            #vtLog.CallStack('')
            #print fVal,self.iRow,self.iCol
            #self.viPreview.SetValue(float(fVal)*(1.0+self.viRate.GetValue()/100.0))
        except:
            #vtLog.vtLngTB(self.GetName())
            #self.viPreview.SetValue(0.0)
            pass
    def OnGrdCostVtinputGridInfoChanged(self, event):
        event.Skip()
        #vtLog.CallStack('')
        self.__updatePreview__()
    def OnViRateVtinputFloatChanged(self, event):
        event.Skip()
        self.__updatePreview__()
        


