#----------------------------------------------------------------------------
# Name:         vPrjPlanGanttCanvasPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060410
# CVS-ID:       $Id: vPrjPlanCanvasGanttPanel.py,v 1.2 2010/03/03 02:17:14 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.draw.vtDrawCanvasXmlPanel import *
from vidarc.tool.draw.vtDrawCanvasXmlGroup import *

from vidarc.tool.draw.vtDrawCanvasRectangle import *
from vidarc.tool.draw.vtDrawCanvasLine import *
from vidarc.tool.draw.vtDrawCanvasText import *

from vPrjPlanCanvasGanttWorkPackage import *

import os,datetime
import vidarc.tool.xml.vtXmlHierarchy as vtXmlHierarchy
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtColor as vtColor

class vPrjPlanCanvasGanttPanel(vtDrawCanvasPanel):
    DFT_COLOR=[
        vtColor.BLACK,
        ( 94, 154,  94),
        (148, 148, 148),
        ( 79,  79,  59),
        (210,   0,   0),
        (120,  78, 146),
        ( 31, 156, 127),
        (178, 100, 178),
        (  0,   0,   0),
        (  0,   0,   0),
        vtColor.GREEN4,
        vtColor.YELLOW2,
        vtColor.RED3,
        ]
    DFT_DIS_COLOR=[
        vtColor.BLACK,
        (167, 136, 000),
        ( 67, 135, 112),
        (116, 130, 147),
        (100, 166, 100),
        (150, 114,  22),
        (147,  82, 147),
        (  0,   0,   0),
        (  0,   0,   0),
        vtColor.GREEN4,
        vtColor.YELLOW2,
        vtColor.RED3,
        ]
    SELECTED_LAYER=3
    SELECTED_WIDTH=10
    SELECTED_HEIGHT=10
    
    FOCUS_LAYER=3
    FOCUS_WIDTH=6
    FOCUS_HEIGHT=6
    
    DATE_WIDTH = 8
    OFS_X_PSP          = 1
    PSP_WIDTH          = 8
    OFS_X_WRKPKG_NAME  = OFS_X_PSP + PSP_WIDTH
    WRKPKG_WIDTH       = 16
    OFS_X_DATE_START   = OFS_X_WRKPKG_NAME + WRKPKG_WIDTH
    OFS_X_DATE_END     = OFS_X_DATE_START + DATE_WIDTH
    OFS_WRK_PKG        = OFS_X_DATE_END + DATE_WIDTH
    
    START_OFS_X        = OFS_X_DATE_START + DATE_WIDTH + DATE_WIDTH
    START_OFS_Y        = 2
    def __init__(self, parent, 
                id=wx.NewId(), pos=wx.DefaultPosition, size=wx.Size(100, 50), 
                style=wx.TAB_TRAVERSAL, name=u'StatusDevicePanel',bTex=False):
        vtDrawCanvasPanel.__init__(self,parent,id,pos,size,style,name)
        obj=vtDrawCanvasObjectBase()
        if bTex:
            drv=obj.DRV_LATEX
        else:
            drv=obj.DRV_WX
        self.SetConfig(obj,drv,max=(100,100),grid=(8,15))
        self.RegisterObject(vtDrawCanvasLine())
        self.RegisterObject(vtDrawCanvasRectangle())
        self.RegisterObject(vtDrawCanvasText())
        #self.RegisterObject(StatusDeviceMFC())
        self.SetActivateEnable(True)
    def GetDeviceIds(self):
        lst=[]
        for o in self.objs:
            id=o.GetId()
            if id>=0:
                try:
                    lst.index(id)
                except:
                    lst.append(id)
        return lst
    def DeactivateDevices(self,lst):
        for o in self.objs:
            #print o
            try:
                lst.index(o.GetId())
                o.SetActive(False)
            except:
                o.SetActive(True)
                pass
            self.ShowObj(o,False)
        #self.ShowAll()
    def GetDlg(self):
        return None
    def __setDftColors__(self):
        def convColor2Act(a):
            a=int(a*1.0)
            if a<0:
                a=0
            if a>255:
                a=255
            return a
        self.color=[]
        self.colorAct=[]
        if hasattr(self,'DFT_ACT_COLOR'):
            for c in self.DFT_ACT_COLOR:
                self.colorAct.append(wx.Colour(c[0], c[1], c[2]))
            for c in self.DFT_COLOR:
                self.color.append(wx.Colour(c[0], c[1], c[2]))
        else:
            for c in self.DFT_COLOR:
                self.colorAct.append(wx.Colour(c[0], c[1], c[2]))
            for c in self.DFT_COLOR:
                r,g,b=map(convColor2Act,c)
                self.color.append(wx.Colour(r, g, b))
            
        self.colorDis=[]
        self.colorActDis=[]
        if hasattr(self,'DFT_ACT_DIS_COLOR'):
            for c in self.DFT_ACT_DIS_COLOR:
                self.colorActDis.append(wx.Colour(c[0], c[1], c[2]))
        else:
            for c in self.DFT_DIS_COLOR:
                self.colorActDis.append(wx.Colour(c[0], c[1], c[2]))
        for c in self.DFT_DIS_COLOR:
            r,g,b=map(convColor2Act,c)
            self.colorDis.append(wx.Colour(r, g, b))
        self.colorBkg=self.scwDrawing.GetBackgroundColour()
        
        self.__setDftBrush__()
        self.__setDftPen__()
    def __getPSP__(self,node):
        sTag=self.doc.getTagName(node)
        if sTag!='WorkPackage':
            return []
        else:
            iRet=self.doc.getNodeText(node,'idx')
            return self.__getPSP__(self.doc.getParent(node)) + [iRet]
    def __build__(self,node,iLv):
        vtLog.CallStack('')
        sName=self.doc.getNodeText(node,'tag')
        o=vtDrawCanvasText(text=sName,
                x=self.OFS_X_WRKPKG_NAME,y=self.iActY,
                w=self.WRKPKG_WIDTH,h=self.iActH,
                align_horizontal=vtDrawCanvasText.ALIGN_LEFT)
        self.objs.append(o)
        
        sName='.'.join(self.__getPSP__(node))
        o=vtDrawCanvasText(text=sName,
                x=self.OFS_X_PSP,y=self.iActY,
                w=self.PSP_WIDTH,h=self.iActH,
                align_horizontal=vtDrawCanvasText.ALIGN_LEFT)
        self.objs.append(o)
        
        o=vPrjPlanCanvasGanttWorkPackage(doc=self.doc,node=node,
                x=self.iActX,y=self.iActY,
                w=self.iActW,h=self.iActH)
        self.objs.append(o)
        self.objsHier.append(o)
        self.dWrkPkg[self.doc.getKey(node)]=len(self.lWrkPkg)
        self.lWrkPkg.append((self.iActY,o,node,[]))
        
        oDt=vtDrawCanvasText(text=o.GetDateStart().GetDateStr(),
                x=self.OFS_X_DATE_START,y=self.iActY,w=self.DATE_WIDTH,h=self.iActH,
                align_horizontal=vtDrawCanvasText.ALIGN_LEFT)
        self.objs.append(oDt)
        
        oDt=vtDrawCanvasText(text=o.GetDateEnd().GetDateStr(),
                x=self.OFS_X_DATE_END,y=self.iActY,w=self.DATE_WIDTH,h=self.iActH,
                align_horizontal=vtDrawCanvasText.ALIGN_LEFT)
        self.objs.append(oDt)
        
        #if iLv%2:
        #    self.iActX+=self.iActW+(self.iActW>>1)
        #else:
        #    self.iActY+=self.iActH+(self.iActH>>1)
        self.iActY+=self.iActH
        cc=self.doc.getChilds(node,'WorkPackage')
        l=[]
        for c in cc:
            oT=self.__build__(c,iLv+1)
            l.append(oT)
        return (o,l)
    def Build(self):
        self.objs=[]
        self.objsHier=[]
        self.lWrkPkg=[]
        self.dWrkPkg={}
        self.ClearInt()
        
        self.iActW=7
        self.iActH=1
        self.iActX=self.START_OFS_X
        
        self.iActY=self.START_OFS_Y
        node=self.doc.getBaseNode()
        
        l=[]
        cc=self.doc.getChilds(node,'WorkPackage')
        for c in cc:
            oT=self.__build__(c,1)
            l.append(oT)
        sDateMin=None
        sDateMax=None
        for tup in self.lWrkPkg:
            sDt=tup[1].GetDateStart().GetDateStr()
            if sDateMin is None:
                sDateMin=sDt
            else:
                if sDt<sDateMin:
                    sDateMin=sDt
            sDt=tup[1].GetDateEnd().GetDateStr()
            if sDateMax is None:
                sDateMax=sDt
            else:
                if sDt>sDateMax:
                    sDateMax=sDt
        dtStart=vtDateTime(True)
        dtStart.SetStr(sDateMin+' 00:00:00')
        for tup in self.lWrkPkg:
            tup[1].AlignDateStart(dtStart)
        # label date
        sDt=sDateMin
        dtTmp=vtDateTime(True)
        dtTmp.SetStr(sDateMin+' 00:00:00')
        dtEnd=vtDateTime(True)
        #dtStart.SetStr(sDateMax+' 00:00:00')
        def calcDiffDay(dtEnd,dtStart):
            zDiff=dtEnd.dt-dtStart.dt
            return zDiff.days
        while sDt<sDateMax:
            x=calcDiffDay(dtTmp,dtStart)
            o=vtDrawCanvasText(text=sDt,
                    x=self.START_OFS_X+x,y=0,w=7,h=1,
                    align_horizontal=vtDrawCanvasText.ALIGN_LEFT)
            self.objs.append(o)
            iYear,iWk,iDay=dtTmp.GetCalendarISO()
            o=vtDrawCanvasText(text='%02d'%iWk,
                    x=self.START_OFS_X+x,y=1,w=7,h=1,
                    align_horizontal=vtDrawCanvasText.ALIGN_LEFT)
            self.objs.append(o)
            dtTmp.dt+=datetime.timedelta(7)
            sDt=dtTmp.GetDateStr()
        def calc():
            for tup in self.lWrkPkg:
                pass
        def addLines(o,l):
            xH=o.GetX()
            yH=-1
            for oo,ll in l:
                yH=oo.GetY()
                oLine=vtDrawCanvasLine(x=xH+1,y=yH+1,w=1,h=0)
                self.objs.append(oLine)
                addLines(oo,ll)
            if yH>0:
                y=o.GetY()
                oLine=vtDrawCanvasLine(x=xH+1,y=y+2,w=0,h=yH-y-1)
                self.objs.append(oLine)
                
        #addLines(self.objs[0],l)
        self.ShowAll()
