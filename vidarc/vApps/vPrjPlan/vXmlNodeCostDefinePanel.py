#Boa:FramePanel:vXmlNodeCostDefinePanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeCostDefinePanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060823
# CVS-ID:       $Id: vXmlNodeCostDefinePanel.py,v 1.8 2013/06/03 06:11:36 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputFloat
import vidarc.tool.input.vtInputGrid
import wx.lib.buttons
import vidarc.tool.time.vtTimeDateGM
import vidarc.tool.input.vtInputGridSmall
import vidarc.tool.input.vtInputTree
import vidarc.tool.input.vtInputTreeInternal
import vidarc.tool.input.vtInputTreeMLInternal
import vidarc.tool.input.vtInputTextML
import vidarc.tool.input.vtInputText

import sys

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.time.vtTime import vtDateTime
from vidarc.tool.xml.vtXmlHierarchy import getTagNames

from vidarc.vApps.vGlobals.vXmlGlobalsTree import vXmlGlobalsTree

[wxID_VXMLNODECOSTDEFINEPANEL, wxID_VXMLNODECOSTDEFINEPANELCBADDDATE, 
 wxID_VXMLNODECOSTDEFINEPANELCBADDFUNC, wxID_VXMLNODECOSTDEFINEPANELCBDELDATE, 
 wxID_VXMLNODECOSTDEFINEPANELCBDELFUNC, wxID_VXMLNODECOSTDEFINEPANELCBSEARCH, 
 wxID_VXMLNODECOSTDEFINEPANELCHCFUNC, wxID_VXMLNODECOSTDEFINEPANELGRDCOSTDEF, 
 wxID_VXMLNODECOSTDEFINEPANELLBLDATE, wxID_VXMLNODECOSTDEFINEPANELLBLFUNC, 
 wxID_VXMLNODECOSTDEFINEPANELLBLINCREASE, 
 wxID_VXMLNODECOSTDEFINEPANELTXTENDDATE, 
 wxID_VXMLNODECOSTDEFINEPANELTXTSTARTDATE, wxID_VXMLNODECOSTDEFINEPANELVICOST, 
 wxID_VXMLNODECOSTDEFINEPANELVIDATE, wxID_VXMLNODECOSTDEFINEPANELVIPREVIEW, 
 wxID_VXMLNODECOSTDEFINEPANELVIRATE, 
] = [wx.NewId() for _init_ctrls in range(17)]

class vXmlNodeCostDefinePanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_bxsDateBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbAddDate, 0, border=0, flag=0)
        parent.AddWindow(self.cbDelDate, 0, border=4, flag=wx.LEFT)

    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbSearch, (0, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.lblDate, (1, 2), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.txtStartDate, (1, 3), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.txtEndDate, (1, 4), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viCost, (2, 1), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.viDate, (2, 3), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.lblFunc, (1, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.chcFunc, (1, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.grdCostDef, (3, 0), border=0, flag=wx.EXPAND,
              span=(2, 5))
        parent.AddSizer(self.bxsDateBt, (2, 4), border=0, flag=0, span=(1, 1))
        parent.AddSizer(self.bxsFuncBt, (2, 2), border=0, flag=0, span=(1, 1))
        parent.AddWindow(self.lblIncrease, (0, 2), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.viRate, (0, 3), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.viPreview, (0, 4), border=0, flag=wx.EXPAND,
              span=(1, 1))

    def _init_coll_bxsFuncBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbAddFunc, 0, border=0, flag=0)
        parent.AddWindow(self.cbDelFunc, 0, border=4, flag=wx.LEFT)

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=4, vgap=4)

        self.bxsDateBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsFuncBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_gbsData_Items(self.gbsData)
        self._init_coll_bxsDateBt_Items(self.bxsDateBt)
        self._init_coll_bxsFuncBt_Items(self.bxsFuncBt)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODECOSTDEFINEPANEL,
              name=u'vXmlNodeCostDefinePanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(481, 210),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(473, 183))
        self.SetAutoLayout(True)

        self.viCost = vidarc.tool.input.vtInputTreeMLInternal.vtInputTreeMLInternal(id=wxID_VXMLNODECOSTDEFINEPANELVICOST,
              name=u'viCost', parent=self, pos=wx.Point(48, 87),
              size=wx.Size(130, 30), style=0)

        self.grdCostDef = vidarc.tool.input.vtInputGridSmall.vtInputGridSmall(id=wxID_VXMLNODECOSTDEFINEPANELGRDCOSTDEF,
              name=u'grdCostDef', parent=self, pos=wx.Point(0, 121),
              size=wx.Size(468, 44))
        self.grdCostDef.SetMinSize(wx.Size(-1, -1))
        self.grdCostDef.Bind(vidarc.tool.input.vtInputGrid.vEVT_VTINPUT_GRID_INFO_SELECTED,
              self.OnGrdCostDefVtinputGridInfoSelected,
              id=wxID_VXMLNODECOSTDEFINEPANELGRDCOSTDEF)
        self.grdCostDef.Bind(vidarc.tool.input.vtInputGrid.vEVT_VTINPUT_GRID_INFO_CHANGED,
              self.OnGrdCostDefVtinputGridInfoChanged,
              id=wxID_VXMLNODECOSTDEFINEPANELGRDCOSTDEF)

        self.viDate = vidarc.tool.time.vtTimeDateGM.vtTimeDateGM(id=wxID_VXMLNODECOSTDEFINEPANELVIDATE,
              name=u'viDate', parent=self, pos=wx.Point(252, 87),
              size=wx.Size(106, 30), style=0)

        self.lblDate = wx.StaticText(id=wxID_VXMLNODECOSTDEFINEPANELLBLDATE,
              label=u'date', name=u'lblDate', parent=self, pos=wx.Point(182,
              62), size=wx.Size(66, 21), style=wx.ALIGN_RIGHT)
        self.lblDate.SetMinSize(wx.Size(-1, -1))

        self.cbAddFunc = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODECOSTDEFINEPANELCBADDFUNC,
              bitmap=vtArt.getBitmap(vtArt.Add), name=u'cbAddFunc', parent=self,
              pos=wx.Point(182, 87), size=wx.Size(31, 30), style=0)
        self.cbAddFunc.Bind(wx.EVT_BUTTON, self.OnCbAddFuncButton,
              id=wxID_VXMLNODECOSTDEFINEPANELCBADDFUNC)

        self.cbDelFunc = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODECOSTDEFINEPANELCBDELFUNC,
              bitmap=vtArt.getBitmap(vtArt.Del), name=u'cbDelFunc', parent=self,
              pos=wx.Point(217, 87), size=wx.Size(31, 30), style=0)
        self.cbDelFunc.Enable(False)
        self.cbDelFunc.Bind(wx.EVT_BUTTON, self.OnCbDelFuncButton,
              id=wxID_VXMLNODECOSTDEFINEPANELCBDELFUNC)

        self.txtStartDate = wx.TextCtrl(id=wxID_VXMLNODECOSTDEFINEPANELTXTSTARTDATE,
              name=u'txtStartDate', parent=self, pos=wx.Point(252, 62),
              size=wx.Size(106, 21), style=0, value=u'')
        self.txtStartDate.SetMinSize(wx.Size(-1, -1))
        self.txtStartDate.Enable(False)

        self.txtEndDate = wx.TextCtrl(id=wxID_VXMLNODECOSTDEFINEPANELTXTENDDATE,
              name=u'txtEndDate', parent=self, pos=wx.Point(362, 62),
              size=wx.Size(106, 21), style=0, value=u'')
        self.txtEndDate.SetMinSize(wx.Size(-1, -1))
        self.txtEndDate.Enable(False)

        self.cbSearch = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODECOSTDEFINEPANELCBSEARCH,
              bitmap=vtArt.getBitmap(vtArt.Find), label=_(u'Search'),
              name=u'cbSearch', parent=self, pos=wx.Point(48, 28),
              size=wx.Size(130, 30), style=0)
        self.cbSearch.SetMinSize(wx.Size(-1, -1))
        self.cbSearch.Bind(wx.EVT_BUTTON, self.OnCbSearchButton,
              id=wxID_VXMLNODECOSTDEFINEPANELCBSEARCH)

        self.lblFunc = wx.StaticText(id=wxID_VXMLNODECOSTDEFINEPANELLBLFUNC,
              label=u'functions', name=u'lblFunc', parent=self, pos=wx.Point(0,
              62), size=wx.Size(44, 21), style=wx.ALIGN_RIGHT)
        self.lblFunc.SetMinSize(wx.Size(-1, -1))

        self.chcFunc = wx.Choice(choices=[],
              id=wxID_VXMLNODECOSTDEFINEPANELCHCFUNC, name=u'chcFunc',
              parent=self, pos=wx.Point(48, 62), size=wx.Size(130, 21),
              style=wx.CB_SORT)
        self.chcFunc.SetMinSize(wx.Size(-1, -1))
        self.chcFunc.Bind(wx.EVT_CHOICE, self.OnChcFuncChoice,
              id=wxID_VXMLNODECOSTDEFINEPANELCHCFUNC)

        self.cbAddDate = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODECOSTDEFINEPANELCBADDDATE,
              bitmap=vtArt.getBitmap(vtArt.Add), name=u'cbAddDate', parent=self,
              pos=wx.Point(362, 87), size=wx.Size(31, 30), style=0)
        self.cbAddDate.Bind(wx.EVT_BUTTON, self.OnCbAddDateButton,
              id=wxID_VXMLNODECOSTDEFINEPANELCBADDDATE)

        self.cbDelDate = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODECOSTDEFINEPANELCBDELDATE,
              bitmap=vtArt.getBitmap(vtArt.Del), name=u'cbDelDate', parent=self,
              pos=wx.Point(397, 87), size=wx.Size(31, 30), style=0)
        self.cbDelDate.Enable(False)
        self.cbDelDate.Bind(wx.EVT_BUTTON, self.OnCbDelDateButton,
              id=wxID_VXMLNODECOSTDEFINEPANELCBDELDATE)

        self.lblIncrease = wx.StaticText(id=wxID_VXMLNODECOSTDEFINEPANELLBLINCREASE,
              label=u'increase [%]', name=u'lblIncrease', parent=self,
              pos=wx.Point(182, 28), size=wx.Size(66, 30),
              style=wx.ALIGN_RIGHT)

        self.viRate = vidarc.tool.input.vtInputFloat.vtInputFloat(default=0.0,
              fraction_width=2, id=wxID_VXMLNODECOSTDEFINEPANELVIRATE,
              integer_width=4, maximum=100.0, minimum=-100.0, name=u'viRate',
              parent=self, pos=wx.Point(252, 28), size=wx.Size(106, 30),
              style=0)
        self.viRate.Bind(vidarc.tool.input.vtInputFloat.vEVT_VTINPUT_FLOAT_CHANGED,
              self.OnViRateVtinputFloatChanged,
              id=wxID_VXMLNODECOSTDEFINEPANELVIRATE)

        self.viPreview = vidarc.tool.input.vtInputFloat.vtInputFloat(default=0.0,
              fraction_width=2, id=wxID_VXMLNODECOSTDEFINEPANELVIPREVIEW,
              integer_width=6, maximum=10000, minimum=0.0, name=u'viPreview',
              parent=self, pos=wx.Point(362, 28), size=wx.Size(106, 30),
              style=0)
        self.viPreview.Enable(False)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vPrjPlan')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        
        #self.vitrArea.SetTagNames('mainDepartment','name')
        self.viCost.SetTreeClass(vXmlGlobalsTree)
        self.viCost.AddTreeCall('init','SetNodeInfos',['name'])
        self.viCost.AddTreeCall('init','SetGrouping',[],[('name','')])
        self.viCost.SetTagNames2Base(['globalsdata','globalsFunc'])
        self.viCost.SetEnableMark(False)
        self.viCost.SetAppl('vGlobals')
        self.viDate.SetEnableMark(False)
        self.viRate.SetEnableMark(False)
        #self.viCost.SetShowFullName(False)
        #self.viCostDef.SetAutoStretch(True)
        self.lLblRow=[]
        self.lLblCol=[]
        self.lVal=[]
        self.grdCostDef.SetColumnSize([150,100])
        
        self.gbsData.AddGrowableCol(0)
        self.gbsData.AddGrowableCol(2)
        self.gbsData.AddGrowableCol(3)
        #self.gbsData.AddGrowableRow(0)
        self.gbsData.AddGrowableRow(4)
        self.gbsData.Layout()
        self.gbsData.Fit(self)
        
        self.dtStart=vtDateTime(True)
        self.dtEnd=vtDateTime(True)
        self.dtTmp=vtDateTime(True)
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def ClearInt(self):
        try:
            vtXmlNodePanel.ClearInt(self)
            # add code here
            #self.viCostDef.Clear()
            self.lLblRow=[]
            self.lLblCol=[]
            self.lVal=[]
            self.iRow=-1
            self.iCol=-1
            self.grdCostDef.SetUp(self.lVal,self)
            #self.grdCostDef.Clear()
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def SetRegNode(self,obj):
        vtXmlNodePanel.SetRegNode(self,obj)
        #self.grdCostDef.Setup(obj.GetSetup(),None)
    def SetNetDocs(self,d):
        # add code here
        if d.has_key('vGlobals'):
            dd=d['vGlobals']
            self.viCost.SetDocTree(dd['doc'],dd['bNet'])
    def SetDoc(self,doc,bNet=False):
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        # add code here
        self.viCost.SetDoc(doc,bExternal=True)
    def SetNode(self,node):
        try:
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            # add code here
            #tup=self.objRegNode.GetValues(node)
            #self.viCostDef.SetValue(tup[0])
            #self.viCostDef.SetModified(False)
            #oTrans=self.doc.GetRegisteredNode(self.objRegNode.GetTagNameTransition())
            self.dCfg=self.objRegNode.GetValues(self.node)
            
            #self.lLblRow=dCfg['lRow']
            #self.lLblCol=dCfg['lCol']
            self.lVal=[]
            self.__setup__()
        except:
            self.__logTB__()
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if node is None:
                return
            # add code here
            self.grdCostDef.Get()
            self.objRegNode.SetValues(node,self.dCfg)
            self.GetNodeFin(node)
        except:
            self.__logTB__()
    def Close(self):
        vtXmlNodePanel.Close(self)
        # add code here
        self.viRate.Close()
        self.viDate.Close()
    def Cancel(self):
        vtXmlNodePanel.Cancel(self)
        # add code here
    def Lock(self,flag):
        vtXmlNodePanel.Lock(self,flag)
        if flag:
            # add code here
            self.grdCostDef.Enable(False)
            self.cbAddFunc.Enable(False)
            self.cbDelFunc.Enable(False)
            self.cbAddDate.Enable(False)
            self.cbDelDate.Enable(False)
        else:
            # add code here
            self.grdCostDef.Enable(True)
            self.cbAddFunc.Enable(True)
            self.cbDelFunc.Enable(True)
            self.cbAddDate.Enable(True)
            self.cbDelDate.Enable(True)

    def OnCbSearchButton(self, event):
        event.Skip()
        try:
            if self.node is not None:
                
                node=self.doc.getParent(self.node)
                lDate=self.doc.GetDateLimit(node)
                if lDate[0] is None:
                    self.txtStartDate.SetValue('')
                else:
                    self.txtStartDate.SetValue(lDate[0])
                if lDate[1] is None:
                    self.txtEndDate.SetValue('')
                else:
                    self.txtEndDate.SetValue(lDate[1])
                dFunc=self.doc.GetFuncDict(node)
                self.chcFunc.Clear()
                for tup in dFunc.items():
                    s=' '.join(tup[1])
                    #iFID,sAppl=self.doc.convForeign(tup[0])
                    iFID=tup[0]
                    self.chcFunc.Append(s,iFID)
                try:
                    self.chcFunc.SetSelection(0)
                    wx.CallAfter(self.OnChcFuncChoice,None)
                except:
                    pass
            else:
                vtLog.vtLngCurWX(vtLog.INFO,'no node selected',self)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnViCostDefVtinputGridsetupInfoSelected(self, event):
        event.Skip()

    def OnViCostDefVtinputGridsetupInfoChanged(self, event):
        event.Skip()

    def OnCbAddDateButton(self, event):
        event.Skip()
        sVal=self.viDate.GetValue()
        try:
            self.dtTmp.SetDateSmallStr(sVal)
            self.grdCostDef.Get()
            iCol=self.objRegNode.AddCol(self.dCfg,self.dtTmp.GetDateStr())
            if self.iCol>=0:
                fFact=1.0+(self.viRate.GetValue()/100.0)
                self.objRegNode.CopyCol(self.dCfg,self.iCol,iCol,fFact)
            self.__setup__()
        except:
            self.__logTB__()
        
    def OnChcFuncChoice(self, event):
        if event is not None:
            event.Skip()
        idx=self.chcFunc.GetSelection()
        if idx>=0:
            id=self.chcFunc.GetClientData(idx)
            self.viCost.SetValueID('',id)
            #netGlb=self.doc.GetNetDoc('vGlobals')
            #nodeFunc=netGlb.
            
    def OnCbAddFuncButton(self, event):
        if event is not None:
            event.Skip()
        self.grdCostDef.Get()
        fid=self.viCost.GetForeignID()
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCurWX(vtLog.INFO,'fid:%s'%(fid),self)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'dCfg:%s'%
                        (vtLog.pformat(self.dCfg)),self)
        self.objRegNode.AddRow(self.dCfg,fid)
        iFID,sAppl=self.doc.convForeign(fid)
        sStart=self.txtStartDate.GetValue()
        sEnd=self.txtEndDate.GetValue()
        if len(sStart)>0 and len(sEnd)>0:
            netGlb,node=self.doc.GetNode(fid)
            if node is not None:
                try:
                    o=netGlb.GetRegByNode(node)
                    lDates=o.GetDatedLst(node)
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurWX(vtLog.DEBUG,'lDates:%s'%(vtLog.pformat(lDates)),self)
                    iStart=0
                    iEnd=len(lDates)
                    i=0
                    for tup in lDates:
                        if tup[0]<sStart:
                            iStart=i
                        if tup[0]>sEnd:
                            iEnd=i
                            break
                        i+=1
                    for tup in lDates[iStart:iEnd]:
                        #if tup[0]>=sStart and tup[0]<=sEnd:
                            iCol=self.objRegNode.AddCol(self.dCfg,tup[0])
                            #if iCol>=0:
                            val=o.GetRate(tup[1])
                            #self.objRegNode.SetRowColVal(self.dCfg,fid,tup[0],val)
                            self.objRegNode.SetRowColVal(self.dCfg,iFID,tup[0],val)
                    self.objRegNode.FillBlankRowVal(self.dCfg,iFID)
                    #iColCount=self.objRegNode.GetColCount()
                    #valOld=self.objRegNode.GetRowColVal(self.dCfg,iFID,tup[0],val)
                    #for i in xrange(iColCount):
                    #    
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurWX(vtLog.DEBUG,'dCfg:%s'%
                                    (vtLog.pformat(self.dCfg)),self)
                except:
                    lDates=None
                    vtLog.vtLngTB(self.GetName())
        
        #sTag=self.objRegNode.GetFuncTagNameByID(fid)
        #vtLog.CallStack('')
        #print fid
        #vtLog.pprint(self.dCfg)
        self.__setup__()
        
    def GetRowColType(self,row,col):
        """required for vtInputGrid"""
        if col==-1:
            return self.doc,None,'string'
        try:
            return self.doc,self.node,'float'
        except:
            pass
        return self.doc,self.node,'float'
    def GetRowColValue(self,row,col):
        """required for vtInputGrid"""
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'row:%02d col:%02d'%(row,col),self)
        try:
            return self.lVal[row][col]
        except:
            return None
    def SetRowColValue(self,row,col,val):
        """required for vtInputGrid"""
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'row:%02d col:%02d val:%s'%(row,col,str(val)),self)
        try:
            self.lVal[row][col]=val
        except:
            pass
    def GetColLabelValue(self,col,row=-1):
        """required for vtInputGrid"""
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'row:%02d col:%02d'%(row,col),self)
        try:
            return self.lLblCol[col]
        except:
            return ''
    def GetRowLabelValue(self,row):
        """required for vtInputGrid"""
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'row:%02d'%(row),self)
        try:
            return self.lLblRow[row]
        except:
            return ''
    def GetDataNodes(self):
        """required for vtInputGrid"""
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        return self.lVal
    def GetDataNode(self,row,col):
        """required for vtInputGrid"""
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'row:%02d col:%02d'%(row,col),self)
        return None
    def __setup__(self):
        lColSize=[]
        #self.dAttrs=self.viSetup.GetValueDict()
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%vtLog.pformat(self.dCfg),self)
        self.lLblRow=self.dCfg['lRow']
        self.lLblCol=self.dCfg['lCol']
        iRow=len(self.lLblRow)
        iCol=len(self.lLblCol)
        self.iCol=-1
        self.iRow=-1
        self.lVal=self.dCfg['val']
        if 0:
            for i in xrange(0,iRow+1):
                try:
                    self.lLblRow.append(dRow[i])
                except:
                    self.lLblRow.append(str(i))
                l=[]
                for j in xrange(0,iCol+1):
                    try:
                        l.append(self.lVal[i][j])
                    except:
                        l.append('0.0')
                ll.append(l)
        lCol=[150]
        for j in xrange(0,iCol+1):
            #try:
            #    self.lLblCol.append(dCol[j])
            #except:
            #    self.lLblCol.append(str(j))
            lCol.append(100)
        #if self.VERBOSE:
        #    vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%vtLog.pprint(ll),self)
        #self.lVal=ll
        #self.lVal=[]
        self.grdCostDef.SetColumnSize(lCol)
        self.grdCostDef.SetUp(self.lVal,self)

    def OnCbDelDateButton(self, event):
        event.Skip()
        if self.iCol>=0:
            self.objRegNode.DelColByIdx(self.dCfg,self.iCol)
            self.__setup__()

    def OnCbDelFuncButton(self, event):
        event.Skip()
        if self.iRow>=0:
            self.objRegNode.DelRowByIdx(self.dCfg,self.iRow)
            self.__setup__()

    def OnGrdCostDefVtinputGridInfoSelected(self, event):
        event.Skip()
        try:
            self.iRow=event.GetRow()
            self.iCol=event.GetCol()
            self.cbDelFunc.Enable(True)
            self.cbDelDate.Enable(True)
            self.__updatePreview__()
        except:
            vtLog.vtLngTB(self.GetName())
    def __updatePreview__(self):
        try:
            fVal=self.grdCostDef.GetRowColValueActual(self.iRow,self.iCol)
            #vtLog.CallStack('')
            #print fVal,self.iRow,self.iCol
            self.viPreview.SetValue(float(fVal)*(1.0+self.viRate.GetValue()/100.0))
        except:
            #vtLog.vtLngTB(self.GetName())
            self.viPreview.SetValue(0.0)
    def OnGrdCostDefVtinputGridInfoChanged(self, event):
        event.Skip()
        #vtLog.CallStack('')
        self.__updatePreview__()
    def OnViRateVtinputFloatChanged(self, event):
        event.Skip()
        self.__updatePreview__()
        


