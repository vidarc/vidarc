#----------------------------------------------------------------------------
# Name:         vXmlPrjPlanInputTree.py
# Purpose:      input widget for project plan xml
#               text and popup with tree
# Author:       Walter Obweger
#
# Created:      20060408
# CVS-ID:       $Id: vXmlPrjPlanInputTree.py,v 1.1 2006/04/11 17:43:02 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from vidarc.tool.input.vtInputTree import vtInputTree
import vidarc.tool.log.vtLog as vtLog
from vidarc.vApps.vPrjPlan.vXmlPrjPlanTree import vXmlPrjPlanTree

class vXmlPrjPlanInputTree(vtInputTree):
    def __init__(self,*_args,**_kwargs):
        apply(vtInputTree.__init__,(self,) + _args,_kwargs)
        self.tagName='WrkPkg'
        self.tagNameInt='tag'
        self.trClass=self.__createTree__
        self.trShow=self.__showNode__
    def __createTree__(*args,**kwargs):
        tr=vXmlPrjPlanTree(**kwargs)
        #tr.SetNodeInfos([args[0].tagNameInt,'|id','country','region'])
        #tr.SetGrouping([],[(args[0].tagNameInt,'')])
        tr.EnableLanguageMenu()
        return tr
    def __showNode__(self,doc,node,lang):
        return doc.getNodeText(node,self.tagNameInt)
