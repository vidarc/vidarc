#----------------------------------------------------------------------------
# Name:         vWorkPackageAttrPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060906
# CVS-ID:       $Id: vWorkPackageAttrPanel.py,v 1.4 2010/03/03 02:17:14 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

from vidarc.tool.input.vtInputAttrCfgMLPanel import vtInputAttrCfgMLPanel
from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.input.vtInputValue import vtInputValue

class vWorkPackageAttrPanel(vtInputAttrCfgMLPanel):
    VERBOSE=0
    def __init__(self, *args,**kwargs):
        global _
        _=vtLgBase.assignPluginLang('vPrjPlan')
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        apply(vtInputAttrCfgMLPanel.__init__,(self,) +args,kwargs)
    def __extendImageList__(self):
        keys=self.dCfg.keys()
        l=[]
        for k in keys:
            l.append(self.dCfg[k])
        l.sort()
        try:
            iSel=self.cmbAttrName.GetSelection()
            iIDSel=self.cmbAttrName.GetClientData(iSel)
        except:
            iSel=0
            iIDSel=-1
        self.cmbAttrName.Clear()
        iSel=0
        i=0
        self.imgLstTyp.Add(vtArt.getBitmap(vtArt.Error))
        for tup in l:
            nodeTmp=self.doc.getNodeByIdNum(tup[2])
            sImg=self.doc.getNodeText(nodeTmp,'img')
            if len(sImg)>0:
                if sImg[:3]=='wx.':
                    bmp = wx.ArtProvider_GetBitmap(eval(sImg), eval('wx.ART_TOOLBAR'), (16,16))
                elif sImg[:6]=='vtArt.':
                    bmp=vtArt.getBitmap(eval(sImg))
                else:
                    stream = cStringIO.StringIO(binascii.unhexlify(sImg))
                    bmp=wx.BitmapFromImage(wx.ImageFromStream(stream))
                self.imgDict[tup[1]]=self.imgLstTyp.Add(bmp)
            self.cmbAttrName.Append(tup[0],tup[2])
            if tup[2]==iIDSel:
                iSel=i
            i+=1
        self.lstAttr.SetImageList(self.imgLstTyp,wx.IMAGE_LIST_SMALL)
        
        self.cmbAttrName.SetSelection(iSel)
        wx.CallAfter(self.OnCmbAttrNameCombobox,None)

    def SetRegNode(self,obj):
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s;class:%s'%(obj.GetTagName(),obj.__class__.__name__),self)
        except:
            pass
        self.objRegNode=obj
        if hasattr(obj,'GetCfgBase'):
            self.SetTagNames2Base(obj.GetCfgBase())
            if self.doc is not None:
                self.__buildCfg__()
                self.__extendImageList__()
    def SetNode(self,node):
        try:
            self.Clear()
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            lang=self.viLgSel.GetValue()
            bCfgAdded=False
            for o in self.doc.getChildsNoAttr(self.node,self.doc.attr):
                sAttr=self.doc.getTagName(o)
                if self.funcIs2Skip is not None:
                    if self.funcIs2Skip(sAttr):
                        continue
                if self.objRegNode.IsAttrHidden(sAttr):
                    continue
                if self.__hasAttrKey__(sAttr):
                    if self.doc.hasChilds(o):
                        if self.__check2addCfg__(sAttr,o)==False:
                            self.doc.deleteNode(o,self.node)
                            continue
                    s=self.doc.getText(o)
                    if self.attrs.has_key(sAttr)==False:
                        self.attrs[sAttr]={}
                    sLg=self.doc.getAttribute(o,'language')
                    self.attrs[sAttr][sLg]=s
                else:
                    # add faulty
                    #s=self.doc.getText(o)
                    if self.__check2addCfg__(sAttr,o)==False:
                        self.doc.deleteNode(o,self.node)
                        continue
                    else:
                        bCfgAdded=True
                    s=self.doc.getAttributeVal(o)
                    self.attrsFlt[sAttr]=s
            if bCfgAdded:
                self.__buildCfg__()
                self.__extendImageList__()
            self.__showAttrs__()
        except:
            self.__logTB__()
    def __check2addCfg__(self,sAttr,o):
        try:
            if sAttr=='inheritance_type':
                return False
            #if sAttr.find('file')==0:
            #    sAttr2Cfg='file'
            #    sAttrIdx=sAttr[4:]
            #else:
            #    sAttr2Cfg=sAttr
            iIdx=sAttr.find('_')
            if iIdx>0:
                sAttr2Cfg=sAttr[:iIdx]
                sAttrIdx=sAttr[iIdx+1:]
            elif iIdx==0:
                return False
            else:
                sAttr2Cfg=sAttr
                sAttrIdx=''
            nodeAttr=None
            if sAttr2Cfg in self.dCfg:
                iID=self.dCfg[sAttr2Cfg][2]
                nodeAttr=self.doc.getNodeByIdNum(iID)
            #iID=self.dCfg[sAttr2Cfg][2]
            #node=self.doc.getNodeByIdNum(iID)
            if nodeAttr is None:
                nodeCfg=self.doc.getChildByLst(self.doc.getBaseNode(),self.lTagNames2Base)
                #nodeAttrML=self.doc.getChild(nodeCfg,'attrML')
                if nodeAttr is None:
                    lTypes=vtInputValue.TYPES
                    sType=self.doc.getNodeText(o,'type')
                    if sType in lTypes:
                        dTypes=vtInputValue.TYPE_DICT
                        d2Create={}
                        l2Create=[]
                        if sType in dTypes:
                            lAttrs=dTypes[sType]
                            for sA in lAttrs:
                                if sA.find('it')==0:
                                    continue
                                dTmp={'tag':sA,'val':''}
                                l2Create.append(dTmp)
                                d2Create[sA]=dTmp
                        sVal=''
                        for c in self.doc.getChilds(o):
                            sTagA=self.doc.getTagName(c)
                            if sTagA.find('it')==0:
                                dTmp={'tag':sTagA,'val':''}
                                l2Create.append(dTmp)
                                d2Create[sTagA]=dTmp
                                d2Create[sTagA]['val']=self.doc.getText(c)
                            if sTagA in d2Create:
                                d2Create[sTagA]['val']=self.doc.getText(c)
                                if sTagA=='val':
                                    sVal=d2Create[sTagA]['val']
                        l2Create.append({'tag':'type','val':sType})
                        l2Cfg=[
                                {'tag':'tag','val':sAttr2Cfg},
                                {'tag':'img'},]
                        langIds=self.doc.GetLanguageIds()
                        for lang in langIds:
                            l2Cfg.append({'tag':'name','attr':('language',lang),'val':sAttr2Cfg})
                        l2Cfg.append({'tag':sAttr2Cfg,'lst':l2Create})
                        nodeAttr=self.doc.createSubNodeDict(nodeCfg,{'tag':'attrML','lst':l2Cfg})
                        #self.dlgCfg.GetNode(c)
                        self.doc.AlignNode(nodeAttr)
                        self.doc.addNode(nodeCfg,nodeAttr)
                        fid=self.doc.getKey(nodeAttr)
                        for c in self.doc.getChilds(o):
                            self.doc.deleteNode(c,o)
                        self.doc.setAttribute(o,'fid',fid)
                        self.doc.setText(o,sVal)
                        return True
            else:
                sVal=self.doc.getNodeText(o,'val')
                fid=self.doc.getKey(nodeAttr)
                for c in self.doc.getChilds(o):
                    self.doc.deleteNode(c,o)
                self.doc.setAttribute(o,'fid',fid)
                self.doc.setText(o,sVal)
                return True
        except:
            self.__logTB__()
        return False
