#----------------------------------------------------------------------------
# Name:         __register__.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060617
# CVS-ID:       $Id: __register__.py,v 1.4 2007/08/21 18:14:40 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust

import vidarc.tool.xml.__link__ as __link__
#from vidarc.vApps.vEngineering.vNodeDocuments import vNodeDocuments
from vidarc.vApps.vEngineering.vXmlNodeEngineeringActivity import vXmlNodeEngineeringActivity
from vidarc.tool.xml.vtXmlNodeAttrCfgML import vtXmlNodeAttrCfgML

from vidarc.vApps.vPrjPlan.vXmlNodeWorkPackage import vXmlNodeWorkPackage
from vidarc.vApps.vPrjPlan.vXmlNodeWorkPackageAttr import vXmlNodeWorkPackageAttr
from vidarc.vApps.vPrjPlan.vXmlNodeWorkPackageFileAttr import vXmlNodeWorkPackageFileAttr
from vidarc.vApps.vPrjPlan.vXmlNodeWorkPackageQuestion import vXmlNodeWorkPackageQuestion
from vidarc.vApps.vPrjPlan.vXmlNodeWorkPackageContents import vXmlNodeWorkPackageContents
from vidarc.vApps.vPrjPlan.vXmlNodeWorkPackageResult import vXmlNodeWorkPackageResult
from vidarc.vApps.vPrjPlan.vXmlNodeWorkPackageResults import vXmlNodeWorkPackageResults
from vidarc.vApps.vPrjPlan.vXmlNodeWorkPackageFollowUps import vXmlNodeWorkPackageFollowUps
from vidarc.vApps.vPrjPlan.vXmlNodeWorkPackageMileStone import vXmlNodeWorkPackageMileStone
from vidarc.vApps.vPrjPlan.vXmlNodeWorkPackageMileStones import vXmlNodeWorkPackageMileStones
from vidarc.vApps.vPrjPlan.vXmlNodeWorkPackageVolumeWork import vXmlNodeWorkPackageVolumeWork
from vidarc.vApps.vPrjPlan.vXmlNodeCostDefine import vXmlNodeCostDefine
from vidarc.vApps.vPrjPlan.vXmlNodeVolumeWorkDistribute import vXmlNodeVolumeWorkDistribute
from vidarc.vApps.vPrjPlan.vXmlNodeWorkPackageCost import vXmlNodeWorkPackageCost
from vidarc.vApps.vHum.vUserNodeWorkingTimes import vUserNodeWorkingTimes
from vidarc.vApps.vPrjPlan.vXmlNodePrjPlanDoc import vXmlNodePrjPlanDoc
from vidarc.vApps.vPrjPlan.vXmlNodeKplusShort import vXmlNodeKplusShort

import vidarc.ext.state.__register__
from vidarc.ext.state.veStateStateMachine import veStateStateMachine
from vidarc.ext.state.veStateTransition import veStateTransition
from vidarc.tool.xml.vtXmlFilterType import vtXmlFilterType

import vidarc.ext.data.__register__

from vidarc.tool.xml.vtXmlNodeLog import vtXmlNodeLog

if vcCust.is2Import(__name__,'__REG_ACCOUNT__',True):
    from vidarc.vApps.vEngineering.vNodeAccount import vNodeAccount
    from vidarc.vApps.vEngineering.vNodeAccountTask import vNodeAccountTask
    from vidarc.vApps.vEngineering.vNodeAccountTaskSub import vNodeAccountTaskSub
def __regAccount__(doc):
    oAcc=vNodeAccount()
    oAccTask=vNodeAccountTask()
    oAccTaskSub=vNodeAccountTaskSub()
    doc.RegisterNode(oAcc,False)
    doc.RegisterNode(oAccTask,False)
    doc.RegisterNode(oAccTaskSub,False)
    doc.LinkRegisteredNode(oAcc,oAccTask)
    doc.LinkRegisteredNode(oAccTask,oAccTaskSub)
    
def RegisterNodes(self,oRoot):
    try:
        oAttrML=self.GetReg('attrML',bLogFlt=False)
        if oAttrML is None:
            self.RegisterNode(vtXmlNodeAttrCfgML(),False)
        #oDocs=vNodeDocuments()
        oDoc=vXmlNodePrjPlanDoc()
        self.RegisterNode(oDoc,False)
        
        oActivity=vXmlNodeEngineeringActivity()
        #oDoc=doc.GetReg('Doc')
        #self.RegisterNode(oAttrML,False)
        #self.RegisterNode(oDocs,False)
        #self.RegisterNode(oDoc,False)
        self.RegisterNode(oActivity,False)
        #self.LinkRegisteredNode(oDocs,oDoc)
        #print __name__
        if vcCust.is2Import(__name__,'__REG_ACCOUNT__',True):
            #print 'aacc'
            __regAccount__(self)
        
        #oRoot=self.GetReg('root')
        oLog=self.GetReg('log',bLogFlt=False)
        if oLog is None:
            oLog=vtXmlNodeLog()
            self.RegisterNode(oLog,False)
        #oPrjPlan=vXmlNodeWorkPackage()
        oWrkPkg=vXmlNodeWorkPackage()
        oWrkPkgAttr=vXmlNodeWorkPackageAttr()
        oWrkPkgFileAttr=vXmlNodeWorkPackageFileAttr()
        oWrkPkgQuestion=vXmlNodeWorkPackageQuestion()
        oWrkPkgContents=vXmlNodeWorkPackageContents()
        oWrkPkgResult=vXmlNodeWorkPackageResult()
        oWrkPkgResults=vXmlNodeWorkPackageResults()
        oWrkPkgFollowUps=vXmlNodeWorkPackageFollowUps()
        oWrkPkgMileStone=vXmlNodeWorkPackageMileStone()
        oWrkPkgMileStones=vXmlNodeWorkPackageMileStones()
        oVolWrk=vXmlNodeWorkPackageVolumeWork()
        oCostDef=vXmlNodeCostDefine()
        oVolWrkDist=vXmlNodeVolumeWorkDistribute()
        oWrkTime=vUserNodeWorkingTimes()
        oWrkPkgCost=vXmlNodeWorkPackageCost()
        
        oKplusShort=vXmlNodeKplusShort()
        
        #self.RegisterNode(oPrjPlan,True)
        self.RegisterNode(oWrkPkg,True)
        self.RegisterNode(oWrkPkgAttr,False)
        self.RegisterNode(oWrkPkgFileAttr,False)
        self.RegisterNode(oWrkPkgQuestion,False)
        self.RegisterNode(oWrkPkgContents,False)
        self.RegisterNode(oWrkPkgResult,False)
        self.RegisterNode(oWrkPkgResults,False)
        self.RegisterNode(oWrkPkgFollowUps,False)
        self.RegisterNode(oWrkPkgMileStone,False)
        self.RegisterNode(oWrkPkgMileStones,False)
        self.RegisterNode(oWrkPkgCost,False)
        self.RegisterNode(oVolWrk,False)
        self.RegisterNode(oCostDef,True)
        self.RegisterNode(oVolWrkDist,True)
        self.RegisterNode(oWrkTime,False)
        
        self.RegisterNode(oKplusShort,True)
        #self.LinkRegisteredNode(oRoot,oPrjPlan)
        #self.LinkRegisteredNode(oRoot,oWrkPkg)
        #self.LinkRegisteredNode(oPrjPlan,oWrkPkgQuestion)
        #self.LinkRegisteredNode(oPrjPlan,oWrkPkgContents)
        #self.LinkRegisteredNode(oPrjPlan,oWrkPkgResult)
        #self.LinkRegisteredNode(oPrjPlan,oWrkPkgResults)
        #self.LinkRegisteredNode(oPrjPlan,oWrkPkgFollowUps)
        #self.LinkRegisteredNode(oPrjPlan,oLog)
        #self.LinkRegisteredNode(oRoot,oCostDef)
        #self.LinkRegisteredNode(oPrjPlan,oCostDef)
        #self.LinkRegisteredNode(oRoot,oVolWrkDist)
        #self.LinkRegisteredNode(oPrjPlan,oVolWrkDist)
        #self.LinkRegisteredNode(oPrjPlan,oContense)
        #self.LinkRegisteredNode(oPrjPlan,oPrjPlan)
        #self.LinkRegisteredNode(oPrjPlan,oVolWrk)
        __link__.LinkNodes(self,[
                    ('root',['WorkPackage',
                        'VolWrkDist','CostDefine',
                        'KplusShort'
                        ]),
                ])
        __link__.LinkNodes(self,[
                    ('PrjPlan',['WorkPackage',
                        'VolWrkDist','CostDefine',
                        'KplusShort'
                        ]),
                ])
        __link__.LinkNodes(self,[
                    ('WorkPackage',['WrkPkgQuestion','WrkPkgContents',
                        'WrkPkgResult','WrkPkgResults','WrkPkgMileStones','WrkPkgFollowUps',
                        'WrkPkgMileStone',
                        'VolWrkDist','CostDefine','WrkPkgVolWrk','WrkPkgCost',
                        'log','Doc','WorkPackage']),
                ])
        
        vidarc.ext.state.__register__.RegisterNodes(self,bRegStateAsRoot=True)
        vidarc.ext.report.__register__.RegisterNodes(self,bRegDocAsRoot=True)
        
        flt={None:[('start',vtXmlFilterType.FILTER_TYPE_DATE_TIME),
                    ('end',vtXmlFilterType.FILTER_TYPE_DATE_TIME),
                    ]}
        oTransAct=self.GetReg('transActivity',bLogFlt=False)
        if oTransAct is None:
            oTransAct=veStateTransition(tagName='transActivity',filter=flt)
            self.RegisterNode(oTransAct,False)
        oSMactivity=self.GetReg('smActivity',bLogFlt=False)
        if oSMactivity is None:
            oSMactivity=veStateStateMachine(tagName='smActivity',
                    tagNameState='state',tagNameTrans='transActivity',
                    filter=flt,
                    trans={None:{None:_(u'activity'),
                            'GetSum':_(u'sum'),
                            'orderDt':_(u'ordering date'),
                            'deliveryDt':_(u'delivery date'),
                            } },
                    cmd=oActivity.GetActionCmdDict())
        oStateMachineLinker=self.GetReg('stateMachineLinker',bLogFlt=False)
        if oStateMachineLinker is not None:
            oStateMachineLinker.AddPossible({
                    'Activity':[('actState','smActivity')],
                    })
            self.RegisterNode(oSMactivity,False)
            self.LinkRegisteredNode(oStateMachineLinker,oSMactivity)
            oState=self.GetReg('state')
            self.LinkRegisteredNode(oSMactivity,oState)
        else:
            vtLog.vtLngCur(vtLog.ERROR,'statemachine linker not registered;you are not supposed to be here.',__name__)
        
        vidarc.ext.data.__register__.RegisterNodes(self,'vPrjPlan',bRegCollectorAsRoot=True,bRegDocAsRoot=True)
        #vidarc.ext.data.__register__.RegisterDateSmall(self,'purchase','orderDt',_('ordering date'))
        #vidarc.ext.data.__register__.RegisterDateSmall(self,'purchase','deliveryDt',_('delivery date'))
        #oDataColl=self.GetReg('dataCollector')
    except:
        vtLog.vtLngTB(__name__)
