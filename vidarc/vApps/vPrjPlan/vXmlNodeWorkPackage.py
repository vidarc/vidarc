#----------------------------------------------------------------------------
# Name:         vXmlNodeWorkPackage.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060410
# CVS-ID:       $Id: vXmlNodeWorkPackage.py,v 1.6 2010/03/03 02:17:14 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlNodeTag import *
from vidarc.tool.xml.vtXmlNodeAttrML import *
#from vidarc.vApps.vLoc.vXmlLoc import COUNTRIES
#from vidarc.vApps.vPrjPlan.vXmlNodeWorkPackageEditDialog import *
#from vidarc.vApps.vPrjPlan.vXmlNodeWorkPackageAddDialog import *
from vidarc.ext.report.veRepDoc import veRepDoc
import vidarc.tool.report.vRepLatex as latex
from vidarc.tool.lang.vtLgDictML import vtLgDictML
import copy,os

try:
    if vcCust.is2Import(__name__):
        GUI=1
        from vidarc.vApps.vPrjPlan.vXmlNodeWorkPackagePanel import vXmlNodeWorkPackagePanel
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0


class vXmlNodeWorkPackage(vtXmlNodeTag,vtXmlNodeAttrML):
    NODE_ATTRS=[
            ('Idx',None,'idx',None),
            ('Tag',None,'tag',None),
            #('desc',None,'desc',None),
            #('dateTimeStart',None,'dateTimeStart',None),
            #('dateTimeEnd',None,'dateTimeEnd',None),
            #('manager',None,'manager',None),
            #('WorkPkgContense',None,'WrkPkgContense',None),
            #('results',None,'results',None),
            #('progress',None,'progress',None),
            #('progressQuants',None,'progressQuants',None),
            #('status',None,'status',None),
            #('staff',None,'staff',None),
            #('time',None,'time',None),
            #('tasks',None,'tasks',None),
            #('followUps',None,'followUps',None),
        ]
    FUNCS_GET_SET_4_LST=['Idx','Tag']
    FUNCS_GET_4_TREE=['Idx','Tag','Name']
    FMT_GET_4_TREE=[('Idx',''),('Tag',''),('Name','')]
    def __init__(self,tagName='WorkPackage',cfgBase=['cfg']):
        global _
        _=vtLgBase.assignPluginLang('vPrjPlan')
        vtXmlNodeTag.__init__(self,tagName)
        vtXmlNodeAttrML.__init__(self,tagName,cfgBase=cfgBase)
        self.oRep=veRepDoc()
        self.lgDict=None
        if GUI:
            self.__getTranslation__()
    def __getTranslation__(self):
        if self.lgDict is not None:
            return
        try:
            #domain=vtLgBase.getApplDomain()
            domain=__name__.split('.')[-2]
            dn=vtLgBase.getApplBaseDN()
            l=vtLgBase.getPossibleLang(domain,dn)
            if l is not None:
                try:
                    self.lgDict=vtLgDictML(domain,langs=l)
                except:
                    self.lgDict=vtLgDictML(domain,dn=os.path.split(__file__)[0],langs=l)
            else:
                self.lgDict=None
                vtLog.vtLngCurCls(vtLog.WARN,'translation not found',self)
        except:
            self.lgDict=None
            vtLog.vtLngTB(self.__class__.__name__)
    def GetDescription(self):
        return _(u'WorkPackage')
    # ---------------------------------------------------------
    # specific
    def GetIdx(self,node):
        return self.Get(node,'idx')
    def SetIdx(self,node,val):
        self.Set(node,'idx',val)
    def GetFullIdx(self,node):
        l=[]
        n=node
        sTagName=self.doc.getTagName(node)
        while n is not None:
            sTag=self.doc.getTagName(n)
            if sTag==sTagName:
                l.append(self.GetIdx(n))
                n=self.doc.getParent(n)
            else:
                break
        l.reverse()
        return '.'.join(l)
    def SetFullIdx(self,node,val):
        pass
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def IsAttrFile(self,sTag):
        iRet=sTag.find('file')
        if iRet==0:
            if len(sTag)>4:
                if sTag[4]!='_':
                    return True
            return False
    def IsAttrHidden(self,sTag):
        if self.IsAttrFile(sTag):
            return True
        return vtXmlNodeTag.IsAttrHidden(self,sTag)
    def GetAttrFilterTypes(self):
        lAdd=[('FullIdx',vtXmlFilterType.FILTER_TYPE_STRING),]
        lTag=vtXmlNodeTag.GetAttrFilterTypes(self)
        lAttr=vtXmlNodeAttrML.GetAttrFilterTypes(self)
        if lAttr is None:
            return lTag + lAdd
        else:
            return lTag + lAttr + lAdd
    def AddValueToDictByDict(self,node,dDef,dVal,bMultiple,*args,**kwargs):
        if 'FullIdx' in dDef:
            dVal['FullIdx']=self.GetFullIdx(node)
        self.doc.__AddValueToDictByDict__(node,dDef,dVal,bMultiple,*args,**kwargs)
        return 0
    def GetTranslation(self,name):
        #vtXmlNodeTag.GetTranslation(self,name)
        return vtXmlNodeAttrML.GetTranslation(self,name)
    def GetTranslationOld(self,name):
        if name in ['tag','name','decription']:
            return vtXmlNodeTag.GetTranslation(self,name)
        _(u'tag'),_(u'name')
        return _(name)
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def IsSkip(self):
        return False
    def Is2Add(self):
        return True
    def IsNotContained(self):
        "do not add to listbook"
        return True
    def IsId2Add(self):
        return True
    def GetTagNames2IncludeBase(self):
        "return a list of tagnames to be included in GUI-node display widgets"
        return ['WorkPackage','WorkPackageAttr','WorkPackageFileAttr']
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02\x85IDAT8\x8d\x95\x93\xcdN\x13a\x14\x86\x9f\xe9\xb4\x1d~\x9cR\xa0cQ\
\xabAJjQ\x82\x80R\xd4\x95a\xe3\xc2\x951\xae\x8cW\xe1\rx\x07^\x821\xee(\xae4\
\xa9\x01*4\x045\x01,\xa0\x05)\xd4J[\n\xe9\x8f\r\xcc\x00\x05\xa6\xfd\\\x98h\
\xb4u\xe1\xd9\x9es\xde\x9c\xf7\xe4y%\xc9"S\xaf\x9e?{ \x0e\xf7\x13\xb8\xcf<\
\xe4\xde\xfd\xc7R\xdd!\xc0\xf2\xaf\x86"\x1f\xd2\xd7\xa9\xa3\x98\xaf\x99\x0c=\
\x15\xff-\xb0g\xf4pr|\x8bs\xa7,d\xd6\xa7y;\x19\xaa+"\xd5\xb3\xb0\xb4\x18\x15\
\x13\x13\xe3\\\xf1w\xe1q[YOd\xc8\x16*\xb4\xb5\xb9\xf0\xfb\xfd\x0c^\xbb\xfe\
\xcbR\xdd\x0bVWW\xd9\xdf?@u\xba\xb9\xe8\x1f\xe1\xd6\xed\xfb\x0c\r\rS\xadV\t\
\x87\xc3D\xa6\xa7~]c\xfd{y\xea\xed\xa4\xd0\xf5=TUE\x92$\xac6\x1b\x9a\xa6\xe1\
t:q\xb9\\D"\x11\x0c\xc3\xa8\xff\x83\xb1\xe0\xa8\x88\xc5>c\x18\xbbT\xab\x15\
\x92\xc9$\xf1x\x1c\xc30\xb0Z\xad8\x9dN\x14EA\xd7\xf7j\x05\xc6\x82\xa3"\x91H`\
\xb3)\x94\xcb&\x95J\x95x<N(\x14"\x1c\x0e\xb3\xb1\xb1\x81\xae\xeb\x98\xe6\t\
\xc9\xe4\x17&\xc6_\t\x00ie%&r\xb9mR\xa9\x04\x9b\x9bY.\\\xe8B\xd34*\x95\n\xf9\
|\x9e\\.\xc7\xc1\xc1\x01\x0e\x87\x03M\xd3(\x95\x8a\x08Q\xc2no\xc4\xe7\x1bD\
\xee\xed\xbd\xfc\xe4\xe8\xa8\x80\xcf\xd7\x8a\xc5R&\x9b\xcdR,~\xc7no\xc0\xe5\
\xfa\xe9\xbdP(\x90\xcdn\xa1\xeb{\x14\n\x19\xd2\xe9\xaf\xec\xec|\xa7\xa1\xa1\
\x05\xab\xd7\xdb\xcd\xfc\xfc{\xd6\xd6\x96\x08\x04<\xdc\xb8\xd1D&\xb3C:\x9df{\
\xbb\x03Uuc\x18\xbb\x08Q\xa2\xbd\xdd\xce\xd1\x91\x84,\xcb\xe4\xf3:\xa9T\xea\
\'\x07\x0b\xf3s"\x18|\x81a$\xf0\xfb\x1dx\xbd6\x9a\x9a\xf6\xd9\xda*\xb1\xb9)\
\xd3\xdc\xdcHg\xa7B,vL2)\xd1\xd2\xd2\xc1\xa5K\xbd\xb8\xdd\xe7\x7f\x83\xb4\
\x18]\x10o\xde\xbc\xc44\xbf\xe1p\x94il\xdc\xc1\xe3\xc9r\xfa\xb4\x8e,+\xcc\
\xcc\xb4\xb1\xb6\xd6C\x7f\xff\x08\x81\xc0M\xfa\xae\xf6K5$./}\x14\xb3\xb3/im\
\xcd`\xb1\x98,/\xaf"D\nUm\x06\xee22\xf2\x88\xc0\xf0\xcd?\x82U\x83\xf2\xa7\
\xe5\x05\xf1\xe1\xc3(\xba\xbeN\xb1\xa8b\x9a\xa7hoo\xe3\xce\x9d{\\\xed\x1f\
\xacIe\xdd,\xbc\x9b\x8d\x88ht\x8e\x81\x81\x00\x1d\x1dg\xe9\xf2v\xff3\xce?\
\x00\x07w\x17\xa9;\x90(\xd6\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            d=vtXmlNodeTag.GetAddDialogClass(self).copy()
            d['pnCls']=vtXmlNodeTag.GetPanelClass(self)
            return d
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            d=vtXmlNodeTag.GetAddDialogClass(self).copy()
            d['pnCls']=vtXmlNodeTag.GetPanelClass(self)
            return d
        else:
            return None
    def GetPanelClass(self):
        return vXmlNodeWorkPackagePanel
    # ---------------------------------------------------------
    # document machine related
    def Process(self,f,*args,**kwargs):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurCls(vtLog.DEBUG,'args:%s,kwargs:%s'%(vtLog.pformat(args),vtLog.pformat(kwargs)),self)
            node=kwargs['node']
            lang=kwargs['lang']
            self.oRep.__setLang__(lang)
            
            sName=self.GetName(node,lang)
            
            iLevel=kwargs['level']
            f.write(os.linesep)
            f.write(self.oRep.__getHeadLine__(iLevel,sName))
            self.__getTranslation__()
            
            sDesc=self.GetDesc(node,lang)
            sDesc=latex.texReplaceFull(sDesc)
            f.write(os.linesep.join(sDesc.split('\n')))
            
            kkwargs=copy.copy(kwargs)
            if '__doc_id' in kwargs:
                docID=kwargs['__doc_id']
                oReg=self.doc.GetReg('Doc')
                for c in self.doc.getChilds(node,'Doc'):
                    if oReg.GetDocID(c)==docID:
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCurCls(vtLog.DEBUG,'matching sub doc found;id:%d'%(docID),self)
                        kkwargs.update({'node':c})
                        oReg.Process(f,*args,**kkwargs)
                        #oReg.__procDoc__(f,*args,**kwargs)
            oTmp=self.doc.GetReg('WrkPkgContents')
            nodeTmp=self.doc.getChild(node,'WrkPkgContents')
            lCont=oTmp.GetContentsList(nodeTmp,lang)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurCls(vtLog.DEBUG,'lCont:%s'%(lCont),self)
            if len(lCont)>0:
                f.write(os.linesep)
                l=[     '\\paragraph*{%s}'%self.lgDict.Get('main activities',lang),
                        '\\begin{itemize}',
                        ]
                for t in lCont:
                    l.append('  \\item %s'%t[0])
                l.append('\\end{itemize}')
                f.write(os.linesep.join(l))
            
            oTmp=self.doc.GetReg('WrkPkgResults')
            nodeTmp=self.doc.getChild(node,'WrkPkgResults')
            lRes=oTmp.GetResultsList(nodeTmp,lang)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurCls(vtLog.DEBUG,'lRes:%s'%(lCont),self)
            if len(lRes)>0:
                f.write(os.linesep)
                l=[     '\\paragraph*{%s}'%self.lgDict.Get('results',lang),
                        '\\begin{itemize}',
                        ]
                for t in lRes:
                    l.append('  \\item %s'%t[0])
                l.append('\\end{itemize}')
                f.write(os.linesep.join(l))
            
            
            kkwargs.update({'level':kwargs['level']+1})
            for c in self.doc.getChilds(node,self.GetTagName()):
                kkwargs.update({'node':c})
                self.Process(f,*args,**kkwargs)
            
        except:
            vtLog.vtLngTB(self.__class__.__name__)

_(u'main activities'),_(u'results'),_(u'milestones'),
