#----------------------------------------------------------------------------
# Name:         vXmlNodeWorkPackageMileStone.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060908
# CVS-ID:       $Id: vXmlNodeWorkPackageMileStone.py,v 1.2 2010/03/03 02:17:14 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.lang.vtLgBase as vtLgBase
#import vidarc.config.vcCust as vcCust
from vidarc.tool.xml.vtXmlNodeTag import *
try:
    if vcCust.is2Import(__name__):
        from vXmlNodeWorkPackageMileStonePanel import vXmlNodeWorkPackageMileStonePanel
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeWorkPackageMileStone(vtXmlNodeTag):
    def __init__(self,tagName='WrkPkgMileStone'):
        global _
        _=vtLgBase.assignPluginLang('vPrjPlan')
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'milestone')
    # ---------------------------------------------------------
    # specific
    def IsFinished(self,node):
        try:
            bFin=True
            for c in self.doc.getChilds(node,'WrkPkgQuestRes'):
                try:
                    fid=long(self.doc.getAttribute(c,'fid'))
                    n=self.doc.getNodeByIdNum(fid)
                    o=self.doc.GetRegByNode(n)
                    if hasattr(o,'IsFinished'):
                        if o.IsFinished(n)==False:
                            return False
                except:
                    pass
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return False
    # ---------------------------------------------------------
    # inheritance
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsId2Add(self):
        "node id is managed"
        return True
    def IsNotContained(self):
        "do not add to listbook"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00lIDAT8\x8d\xad\x92K\x0e\x80 \x0cD\xdbrn\xb5\x17S\xb9\x98\x0e\x1b4\
\xc6P>\xda&\xac\x98\xf72\x812K\xa0?#\xb5\xcb}[\x81\xf3\xc02O0C,\xa1xb\x8c "\
\xe4\x86PU\x94rM\xf8\x0e\x1a\x92.\xb8&\xe9\x86-\xc9\x10\\\x92\x0c\xc3O\x89_\
\x03\x977p\xf9\x05\x97=p\xd9\xc4\xb7\xc4\x82\x9b\x82KR\xcd\xe4\x8a\x9f\'\x01\
yJj\x13\x05\x84\x8bp\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            d=vtXmlNodeTag.GetAddDialogClass(self).copy()
            d['pnCls']=vtXmlNodeTag.GetPanelClass(self)
            return d
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            d=vtXmlNodeTag.GetAddDialogClass(self).copy()
            d['pnCls']=vtXmlNodeTag.GetPanelClass(self)
            return d
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeWorkPackageMileStonePanel
        else:
            return None
