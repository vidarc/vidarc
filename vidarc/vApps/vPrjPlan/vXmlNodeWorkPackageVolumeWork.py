#----------------------------------------------------------------------------
# Name:         vXmlNodeWorkPackageVolumeWork.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060803
# CVS-ID:       $Id: vXmlNodeWorkPackageVolumeWork.py,v 1.3 2010/03/03 02:17:15 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeTag import *
try:
    if vcCust.is2Import(__name__):
        from vXmlNodeWorkPackageVolumeWorkPanel import *
        #from vXmlNodeXXXEditDialog import *
        #from vXmlNodeXXXAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0


class vXmlNodeWorkPackageVolumeWork(vtXmlNodeTag):
    NODE_ATTRS=[
            #('idx',None,'idx',None),
        ]
    def __init__(self,tagName='WrkPkgVolWrk'):
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'work package volume of work')
    # ---------------------------------------------------------
    # specific
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return False
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsNotContained(self):
        "do not add to listbook"
        return True
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xbdIDAT8\x8d}SMN\xdbP\x18\x9c\xf7\xcc\xbaH\xb0Eq\\\x0e\x90\xa6\xea:\
V\xd2\x0b$\xb6\xd8\xa6\xed\x01P\nb\x1b\xc5\x81=\xbd\x00\x95J\x81e\x9c\xe2F\
\xec\x92<[\xb9\x80\xd55\x8d\x02\x1c\xc0\xbe\x80\xa7\x8b\xd4&&1\xb3z?3\xf3~\
\xe6\xfb\x84\x90\x1a6\xc1\xacu\t(\x00\x80R\x0e\x84\xfc(6\xf1\xe4F\xf5\x7f\
\x1c\xd8\x07\xd8\x7f\xbb\xff\x1aei`\xd6\xba,\xebm\x02\x00\x931\xcbz\x9bR\x04\
\x18\xb8\x03\xcc\xe7s4\xea\xa7`\xd2\xcd\xf6?\x7fj\x93\xc9\x98\x00 \x00\x87#\
\xef\x03\x86\xbf\x86\x98L\x13\xd8\xad]t:\x9d\xb5\x93\xc20\xc4\xf9\xb7sD\xd1;\
X\xd6\x0e\xee\xff\xde\xe3\xc7\xe5O\x81\xe5\x1f8\xb4-\x9b\xb3`\xc6Ux\xb7\x7f\
\x18Gq6\x8f\xa3\x98'\xc7'|_=&\xa0(\xa4\x86\xcc\xe0\xe6\xea\x86$\xe98N&PJ\xe5\
\x0c\x95Z0\x8eb\xda\x96M!5d\x06\xb6e\xf3q\xf1\x98\x11S\x93U\xb3\xcc\x84\xe4\
\xc8\x1b\x11p(\xa4\x06\xc9\xa4K\xabea\xaf\xb4\x97\xbd\xb7\xd7\xeb\xa1\xdf\
\xef\x17\xfe|\xa5RA\xa3\x1e\xe4c\xf4\xfd\x87\x1c\xe9\xe8\xeb\x11\xbc[\x0f\
\xd7W\xd7y\xf5\x0b^q\x1d\xf8\n\xd5j\xb5p;\xc5V:0\xcd\xd2\x8a\xd8\xc7\x9b\xed\
m\\|\xbfXW\x98%\xe0\xe1\xe9\xf9\x06B\x9e\tw\xe8\xe2)]\xf4\xfd\xd4\xb1\xf0\
\xd40\x0c1\x99\xd6\x96\x934\xc6\xbb\xdfwYTEHcl\xd4\x1b\xf9\x18\x85\xd4`\x18_\
8\x0bf\xb9\xc2)*\xa4\xb4\x88\x84\xd4 V\xbb\xb1\xac\xb7i\xb7v\xd1l6\xa1\xebz\
\xee\xdaA\x10\xc0\x1d\xba\x18\xb8\x87\xb9\xce\x14/\xdb\x99\xc9\x98\x86q\t\
\xa3\xbc\xc8\xadO\xa65\x08y\xb6\xd6\xd2\xff\x00?\xd0J\xf0\xa9e\xb0\xa6\x00\
\x00\x00\x00IEND\xaeB`\x82" 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            d=vtXmlNodeTag.GetAddDialogClass(self).copy()
            d['pnCls']=vtXmlNodeTag.GetPanelClass(self)
            return d
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            d=vtXmlNodeTag.GetAddDialogClass(self).copy()
            d['pnCls']=vtXmlNodeTag.GetPanelClass(self)
            return d
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeWorkPackageVolumeWorkPanel
        else:
            return None
