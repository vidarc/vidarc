#----------------------------------------------------------------------------
# Name:         vXmlNodeWorkPackageMileStones.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060908
# CVS-ID:       $Id: vXmlNodeWorkPackageMileStones.py,v 1.2 2010/03/03 02:17:15 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.lang.vtLgBase as vtLgBase
#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        from vidarc.vApps.vPrjPlan.vXmlNodeWorkPackageMileStonesPanel import vXmlNodeWorkPackageMileStonesPanel
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0


class vXmlNodeWorkPackageMileStones(vtXmlNodeBase):
    NODE_ATTRS=[
            #('idx',None,'idx',None),
        ]
    def __init__(self,tagName='WrkPkgMileStones'):
        global _
        _=vtLgBase.assignPluginLang('vPrjPlan')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'milestones')
    # ---------------------------------------------------------
    # specific
    def GetResultsList(self,node,lang=None):
        l=[]
        for c in self.doc.getChilds(node,'milestone'):
            fid=self.doc.getAttribute(c,'fid')
            nodeQuest=self.doc.getNodeByIdNum(long(fid))
            oReg=self.doc.GetRegByNode(nodeQuest)
            ll=[oReg.GetName(nodeQuest,lang=lang),
                oReg.GetDesc(nodeQuest,lang=lang)]
            l.append(ll)
        return l
    # ---------------------------------------------------------
    # inheritance
    def Is2Create(self):
        "create automatically if parent node is created"
        return True
    def IsSkip(self):
        "do not display node in tree"
        return True
    def Is2Add(self):
        "node can be created by tree content menu"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00lIDAT8\x8d\xad\x92K\x0e\x80 \x0cD\xdbrn\xb5\x17S\xb9\x98\x0e\x1b4\
\xc6P>\xda&\xac\x98\xf72\x812K\xa0?#\xb5\xcb}[\x81\xf3\xc02O0C,\xa1xb\x8c "\
\xe4\x86PU\x94rM\xf8\x0e\x1a\x92.\xb8&\xe9\x86-\xc9\x10\\\x92\x0c\xc3O\x89_\
\x03\x977p\xf9\x05\x97=p\xd9\xc4\xb7\xc4\x82\x9b\x82KR\xcd\xe4\x8a\x9f\'\x01\
yJj\x13\x05\x84\x8bp\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        #return vXmlNodeWorkPackageEditDialog
        return None
    def GetAddDialogClass(self):
        #return vXmlNodeWorkPackageAddDialog
        return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeWorkPackageMileStonesPanel
        else:
            return None
