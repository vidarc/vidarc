#----------------------------------------------------------------------------
# Name:         vXmlNodeWorkPackageFollowUps.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060410
# CVS-ID:       $Id: vXmlNodeWorkPackageFollowUps.py,v 1.3 2010/03/03 02:17:14 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.lang.vtLgBase as vtLgBase
#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        from vidarc.vApps.vPrjPlan.vXmlNodeWorkPackageFollowUpsPanel import vXmlNodeWorkPackageFollowUpsPanel
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0


class vXmlNodeWorkPackageFollowUps(vtXmlNodeBase):
    NODE_ATTRS=[
            #('idx',None,'idx',None),
        ]
    def __init__(self,tagName='WrkPkgFollowUps'):
        global _
        _=vtLgBase.assignPluginLang('vPrjPlan')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'follow ups')
    def Is2Create(self):
        "create automatically if parent node is created"
        return True
    def IsSkip(self):
        "do not display node in tree"
        return True
    def Is2Add(self):
        "node can be created by tree content menu"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xd9IDAT8\x8d\xc5S[\x0e\x84 \x0c\x9c\xaa\xa7\xd2\x18#\xe7\xf2\x81l\
\xcf\xe5\xc6\x10\xb9V\xf7c-!\xbb\xf0\xb1\xf1c\x9b4<\xa6\x94\x99R\x88\xaa\x1a\
w\xac\xbau\x1a@\x03\x00\x0f\xbbI\x0e\x9c\xedJ%\\1\xb0c\t>\x08U5Rg\xc7\x92\
\x8e\xa9\x07\x1f\x04\x80\xb0ci\x00\xa0\x1bz\xca\xdd\xa2{\x9fX7\xf4\x04@\xa2\
\x04\x00\x98\x96I\x19\x15\xf5jLZ\xf8&\r`\xc70\xa3\xd1\x1b\xbe\x8c\x1d\xcb\
\xa5?\x9f`\xb6+\x99\xd1H\xa9\xa8\x1a\x93\xaec\x82\x8b\xba\xec\xcf=R\xcd0\xc0\
\xc3n\x92&i\x00\xe0<\xbc\xa4\xb4\x83\x0f\xb2?\xf7x\xd0\x8c&\xce\xbb\xa1\xa7\
\xf3\xf0\xd2\xf6-\x00\x80T\xd7\xaf6-\xd3\x9b\xf5UQ\xc9\xb9\xf6@\t\x8b}@U\x9d\
\xed\x03\xd5Zz\xda\xd9\xaeD\x7f\xffL\xb7\x13\xbc\x00\x18sp\xe2\xf1\xf6\x1a`\
\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        #return vXmlNodeWorkPackageEditDialog
        return None
    def GetAddDialogClass(self):
        #return vXmlNodeWorkPackageAddDialog
        return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeWorkPackageFollowUpsPanel
        else:
            return None
