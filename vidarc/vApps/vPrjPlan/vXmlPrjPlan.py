#----------------------------------------------------------------------------
# Name:         vXmlPrjPlan.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060410
# CVS-ID:       $Id: vXmlPrjPlan.py,v 1.10 2007/07/30 20:38:48 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg

import vidarc.vApps.vPrjPlan.__register__
from vidarc.vApps.vPrjPlan.vXmlNodePrjPlanRoot import vXmlNodePrjPlanRoot
from vidarc.vApps.vPrjPlan.vXmlNodePrjPlan import vXmlNodePrjPlan

#from vidarc.vApps.vCustomer.vXmlNodeArea import vXmlNodeArea
#from vidarc.tool.xml.vtXmlNodeAddr import vtXmlNodeAddr
from vidarc.tool.xml.vtXmlNodeLog import vtXmlNodeLog
from vidarc.tool.xml.vtXmlNodeCfg import vtXmlNodeCfg
from vidarc.tool.time.vtTime import vtDateTime
import operator

from vidarc.tool.xml.vtXmlNodeFilter import vtXmlNodeFilter
from vidarc.tool.xml.vtXmlNodeMessages import vtXmlNodeMessages
from vidarc.tool.xml.vtXmlNodeMsg import vtXmlNodeMsg
from vidarc.ext.state.veStateStateMachine import veStateStateMachine
from vidarc.ext.state.veStateStateMachineLinker import veStateStateMachineLinker
from vidarc.ext.state.veStateState import veStateState
from vidarc.ext.state.veStateTransition import veStateTransition
import vidarc.ext.state.__register__
import vidarc.ext.report.__register__

VERBOSE=1

def getPluginData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xb4IDAT8\x8d\xadS1\x0e\xc20\x0c<\x87N\xfc\x84!<\x06E\xe2\x17\x9d\
\xbb\x11\xb6\x88\xb1\xef\xa8\xf8L\x07~\xc2\x1a\xa6\x82\x93\x9c#\x90\xb8)\xb1\
\x1d\x9f\xcf\xb1E\xdc\x0e5|\n\xb91\x02X\xa7Ej\x9b\xfb6\x90\xd9\x00@\xb6\n4\
\xeb\xe9y`\xb1\xb8\xef\x1fM\xc2\x81F\x02\x18\xc7X\xdc\xe79\xd2\xb8F\x82\xc5n\
\xf9\x06\x9fBf\xfa,F\r\x9fB\x96\xe3\xed\x9c\xad\xec\x9b\xe6\x9e\xaf\xe8\x81\
\xd6]Wp\x89\x9f\xfbU\x9d\xe97\xfe\x82\xa2\x82\x9en\xcd\xaa\xe1\x00{Hzx\xcf\
\x81~l\xe9\x04x\x7f\xd6i\x91\xff\xf6\xc0\xd2\xa9Yk\x08\xdbF\xa0\xddH\xabOT\
\x02[gk\xc5_\xfe]G\x01\xf4V#\xe5\x00\x00\x00\x00IEND\xaeB`\x82' 

class vXmlPrjPlan(vtXmlDomReg):
    NODE_ATTRS=[
            ('name',None,'name',None),
            ('country',None,'contry',None),
            ('region',None,'region',None),
            ('distance',None,'distance',None),
            ('coor',None,'coor',None),
            ]
    TAGNAME_ROOT='PrjPlanRoot'
    APPL_REF=['vContact','vPrjDoc','vPrj','vDoc','vHum','vGlobals','vMsg']
    def __init__(self,attr='id',skip=[],synch=False,appl='vPrjPlan',verbose=0,
                    audit_trail=True):
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        try:
            vtXmlDomReg.__init__(self,attr=attr,skip=skip,synch=synch,
                        appl=appl,verbose=verbose,audit_trail=audit_trail)
            #self.verbose=verbose
            oRoot=vXmlNodePrjPlanRoot()
            self.RegisterNode(oRoot,False)
            oBase=vXmlNodePrjPlan()
            self.RegisterNode(oBase,True)
            self.LinkRegisteredNode(oRoot,oBase)
            self.LinkRegisteredNode(oBase,oBase)
            
            vidarc.vApps.vPrjPlan.__register__.RegisterNodes(self,oBase)
            
            vidarc.ext.state.__register__.RegisterNodes(self,bRegStateAsRoot=True)
            vidarc.ext.report.__register__.RegisterNodes(self,bRegDocAsRoot=True)
            
            #oAddr=vtXmlNodeAddr()
            oCfg=vtXmlNodeCfg()
            #oContense=vXmlNodeWorkPackageContents()
            #oArea=vXmlNodeArea()
            #self.RegisterNode(oAddr,False)
            
            self.RegisterNode(oCfg,True)
            #self.RegisterNode(oArea,False)
            #self.LinkRegisteredNode(oCust,oAddr)
            #self.LinkRegisteredNode(oCfg,oArea)
            #self.LinkRegisteredNode(oArea,oArea)
            
        except:
            vtLog.vtLngTB(self.appl)
        self.SetDftLanguages()
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'PrjPlan')
    #def New(self,revision='1.0',root='PrjPlanRoot',bConnection=False):
    #    vtXmlDomReg.New(self,revision,root)
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elem=self.createSubNode(self.getRoot(),'PrjPlan')
        if bConnection==False:
            self.CreateReq()
        #if bConnection==False:
        #    e=self.createSubNodeTextAttr(elem,'cfg','','id','')
        #    self.checkId(e)
        #    self.processMissingId()
        #    self.clearMissing()
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        #self.genIds()
        #self.AlignDoc()
    def GetWorkPackagesHier(self,node=None,lWrkPkg=None):
        if node is None:
            node=self.getBaseNode()
            #l=[]
            #cc=self.getChilds(node,'WorkPackage')
            #for c in cc:
            return self.GetWorkPackagesHier(node,lWrkPkg)[2]
        l=[]
        cc=self.getChilds(node,'WorkPackage')
        for c in cc:
            if lWrkPkg is not None:
                lWrkPkg.append(c)
            oT=self.GetWorkPackagesHier(c,lWrkPkg)
            l.append(oT)
        #return l
        return (self.getKey(node),node,l)
    
    def __procDateLimit__(self,node,dtStart,dtEnd,l):
        if self.getTagName(node)=='WorkPackage':
            try:
                dtStart.SetStr(self.getNodeText(node,'dateTimeStart'))
            except:
                vtLog.vtLngCur(vtLog.DEBUG,'datetime:%s faulty'%(self.getNodeText(node,'dateTimeStart')),self.appl)
                return 0
            try:
                sDt=dtStart.GetDateStr()
                if l[0] is None:
                    l[0]=sDt
                else:
                    if sDt<l[0]:
                        l[0]=sDt
                dtEnd.SetStr(self.getNodeText(node,'dateTimeEnd'))
                sDt=dtEnd.GetDateStr()
                if l[1] is None:
                    l[1]=sDt
                else:
                    if sDt>l[1]:
                        l[1]=sDt
            except:
                pass
        return 0
    def GetDateLimit(self,node=None):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(self.getKey(node)),self.appl)
        if node is None:
            node=self.getBaseNode()
            return self.GetDateLimit(node)
        l=[None,None]
        dtStart=vtDateTime(True)
        dtEnd=vtDateTime(True)
        if self.IsNodeKeyValid(node):
            self.procKeysRec(1000,node,self.__procDateLimit__,dtStart,dtEnd,l)
        else:
            self.procChildsKeysRec(1000,node,self.__procDateLimit__,dtStart,dtEnd,l)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'dates:%s'%(vtLog.pformat(l)),self.appl)
        return l
    def __procFuncSubDict__(self,node,d):
        if self.getTagName(node)=='function':
            fid=self.getAttribute(node,'fid')
            iFID,sAppl=self.convForeign(fid)
            #if fid not in d:
            if iFID not in d:
                l=[]
                netGlb,nodeFunc=self.GetNode(fid)
                if nodeFunc is not None:
                    o=netGlb.GetRegByNode(nodeFunc)
                    o.AddValuesToLst(nodeFunc,l)
                #d[fid]=l
                d[iFID]=l
        return 0
    def __procFuncDict__(self,node,d):
        if self.getTagName(node)=='WrkPkgVolWrk':
            tmp=self.getChild(node,'glbsFunctions')
            if tmp is not None:
                self.procChildsExt(tmp,self.__procFuncSubDict__,d)
        return 0
    def GetFuncDict(self,node=None):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(self.getKey(node)),self.appl)
        if node is None:
            node=self.getBaseNode()
            return self.GetFuncDict(node)
        d={}
        if self.IsNodeKeyValid(node):
            self.procKeysRec(1000,node,self.__procFuncDict__,d)
        else:
            self.procChildsKeysRec(1000,node,self.__procFuncDict__,d)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'funcs:%s'%(vtLog.pformat(d)),self.appl)
        return d
    def __procFuncVolumeSubDict__(self,node,d):
        if self.getTagName(node)=='function':
            fid=self.getAttribute(node,'fid')
            iFID,sAppl=self.convForeign(fid)
            #if fid not in d:
            if iFID not in d:
                l=[]
                netGlb,nodeFunc=self.GetNode(fid)
                if nodeFunc is not None:
                    o=netGlb.GetRegByNode(nodeFunc)
                    o.AddValuesToLst(nodeFunc,l)
                #d[fid]=[0.0,l]
                d[iFID]=[0.0,l]
            try:
                fVal=float(self.getNodeText(node,'value'))
                d[iFID][0]=d[iFID][0]+fVal
            except:
                vtLog.vtLngTB(self.appl)
                pass
        return 0
    def __procFuncVolumeDict__(self,node,d):
        if self.getTagName(node)=='WrkPkgVolWrk':
            tmp=self.getChild(node,'glbsFunctions')
            if tmp is not None:
                self.procChildsExt(tmp,self.__procFuncVolumeSubDict__,d)
        return 0
    def GetFuncVolumeDict(self,node=None):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(self.getKey(node)),self.appl)
        if node is None:
            node=self.getBaseNode()
            return self.GetFuncVolumeDict(node)
        d={}
        if self.IsNodeKeyValid(node):
            self.procKeysRec(1000,node,self.__procFuncVolumeDict__,d)
        else:
            self.procChildsKeysRec(1000,node,self.__procFuncVolumeDict__,d)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'funcs:%s'%(vtLog.pformat(d)),self.appl)
        return d
    def __procWorkPackageCostSubDict__(self,node,d):
        if self.getTagName(node)=='row':
            fid=self.getAttribute(node,'fid')
            iFID,sAppl=self.convForeign(fid)
            #if fid not in d:
            if iFID not in d:
                l=[]
                netGlb,nodeCost=self.GetNode(fid)
                if nodeCost is not None:
                    o=netGlb.GetRegByNode(nodeCost)
                    o.AddValuesToLst(nodeCost,l)
                #d[fid]=[0.0,l]
                d[iFID]=[0.0,l]
            try:
                fVal=float(reduce(operator.add,[float(v) for v in self.getText(node).split(',')]))
                d[iFID][0]=d[iFID][0]+fVal
            except:
                vtLog.vtLngTB(self.appl)
                pass
        return 0
    def __procWorkPackageCostDict__(self,node,d):
        if self.getTagName(node)=='WrkPkgCost':
            tmp=self.getChild(node,'data')
            if tmp is not None:
                self.procChildsExt(tmp,self.__procWorkPackageCostSubDict__,d)
        return 0
    def GetWorkPackageCostDict(self,node=None):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(self.getKey(node)),self.appl)
        if node is None:
            node=self.getBaseNode()
            return self.GetWorkPackageCostDict(node)
        d={}
        if self.IsNodeKeyValid(node):
            self.procKeysRec(1000,node,self.__procWorkPackageCostDict__,d)
        else:
            self.procChildsKeysRec(1000,node,self.__procWorkPackageCostDict__,d)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'funcs:%s'%(vtLog.pformat(d)),self.appl)
        return d
    def __procWorkVolumePerMonthVolumeDict__(self,node,d):
        if self.getTagName(node)=='WrkPkgVolWrk':
            tmp=self.getChild(node,'glbsFunctions')
            if tmp is not None:
                self.procChildsExt(tmp,self.__procFuncVolumeSubDict__,d)
            pass
        return 0
    def __procWorkVolumePerMonthDict__(self,node,dtStart,dtEnd,dtTmp,dFunc,dWrkPkg,dMonth,d):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(self.getKey(node)),self.appl)
        if self.getTagName(node)=='WorkPackage':
            try:
                dtStart.SetStr(self.getNodeText(node,'dateTimeStart'))
            except:
                vtLog.vtLngCur(vtLog.DEBUG,'datetime:%s faulty'%(self.getNodeText(node,'dateTimeStart')),self.appl)
                return 0
            try:
                dtEnd.SetStr(self.getNodeText(node,'dateTimeEnd'))
            except:
                vtLog.vtLngCur(vtLog.DEBUG,'datetime:%s faulty'%(self.getNodeText(node,'dateTimeStart')),self.appl)
                return 0
            nodeVol=self.getChild(node,'WrkPkgVolWrk')
            if nodeVol is not None:
                idWrkPkg=long(self.getKey(node))
                oWrkPkg=self.GetRegByNode(node)
                dWrkPkg[idWrkPkg]=oWrkPkg.AddValuesToLst(node)
                self.__procWorkVolumePerMonthVolumeDict__(nodeVol,dFunc)
                oWrkTime=self.GetReg('workingtimes')
                l=[]
                fGes=0.0
                iCountGes=0
                iYrS=dtStart.GetYear()
                iMonS=dtStart.GetMonth()
                iYrE=dtEnd.GetYear()
                iMonE=dtEnd.GetMonth()
                for iYear in xrange(iYrS,iYrE+1):
                    if iYear==iYrS:
                        iMonST=iMonS
                    else:
                        iMonST=1
                    if iYear==iYrE:
                        iMonET=iMonE
                    else:
                        iMonET=12
                    for iMon in xrange(iMonST,iMonET+1):
                #for iYear in xrange(dtStart.GetYear(),dtEnd.GetYear()+1):
                #    for iMon in xrange(dtStart.GetMonth(),dtEnd.GetMonth()+1):
                        dtTmp.SetDate(iYear,iMon,1)
                        f=0.0
                        iCount=0
                        if (iYear==dtStart.GetYear()) and (iMon==dtStart.GetMonth()):
                            iDayStart=dtStart.GetDay()
                            iDayEnd=32
                        elif (iYear==dtStart.GetYear()) and (iMon==dtEnd.GetMonth()):
                            iDayStart=1
                            iDayEnd=dtEnd.GetDay()
                        else:
                            iDayStart=1
                            iDayEnd=32
                        for iDay in xrange(iDayStart,iDayEnd):
                            try:
                                f+=oWrkTime.GetTimeNormalProp(None,iYear,iMon,iDay)
                                iCount+=1
                            except:
                                break
                        #fFact=f/float(iCount)
                        fGes+=f
                        iCountGes+=iCount
                        l.append((dtTmp.GetDateStr()[:-3],f))
                        vtLog.vtLngCur(vtLog.DEBUG,'f:%4.2f count:%d'%\
                                        (f,iCount),self.appl)
                vtLog.vtLngCur(vtLog.DEBUG,'f:%4.2f count:%d;dFunc:%s;l:%s'%\
                                (fGes,iCountGes,vtLog.pformat(dFunc),vtLog.pformat(l)),self.appl)
                for k in dFunc.keys():
                    if k not in d:
                        d[k]={}
                    dMon=d[k]
                    vtLog.vtLngCur(vtLog.DEBUG,'f:%4.2f count:%d;dFunc:%s;dMon:%s'%\
                                (fGes,iCountGes,vtLog.pformat(dFunc[k]),vtLog.pformat(dMon)),self.appl)
                    f=dFunc[k][0]
                    for tup in l:
                        if tup[0] in dMon:
                            dSum=dMon[tup[0]]
                            fSum=dSum['sum']
                        else:
                            dSum={'sum':0.0}
                            dMon[tup[0]]=dSum
                            fSum=0.0
                        fVal=f*(tup[1]/fGes)
                        dSum[idWrkPkg]=fVal
                        dMon[tup[0]]['sum']=fSum+fVal
                        if tup[0] in dMonth:
                            dMonth[tup[0]]=dMonth[tup[0]]+fVal#dMon[tup[0]]['sum']+
                        else:
                            dMonth[tup[0]]=fVal#dMon[tup[0]]['sum']
                    vtLog.vtLngCur(vtLog.DEBUG,'f:%4.2f count:%d;dFunc:%s;dMon:%s'%\
                                (fGes,iCountGes,vtLog.pformat(dFunc[k]),vtLog.pformat(dMon)),self.appl)
                    dFunc[k][0]=0.0
                
        return 0
    def GetWorkVolumePerMonthPerFunc(self,node):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s'%(self.getKey(node)),self.appl)
        if node is None:
            node=self.getBaseNode()
            return self.GetWorkVolumePerMonthPerFunc(node)
        d={}
        dFunc={}
        dWrkPkg={}
        dMon={}
        #for k in dFuncDef.keys():
        #    dFunc[k]=0.0
        #    d[k]={}
        dtStart=vtDateTime(True)
        dtEnd=vtDateTime(True)
        dtTmp=vtDateTime(True)
        if self.IsNodeKeyValid(node):
            self.procKeysRec(1000,node,self.__procWorkVolumePerMonthDict__,
                    dtStart,dtEnd,dtTmp,dFunc,dWrkPkg,dMon,d)
        else:
            self.procChildsKeysRec(1000,node,self.__procWorkVolumePerMonthDict__,
                    dtStart,dtEnd,dtTmp,dFunc,dWrkPkg,dMon,d)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'funcs:%s;dWrkPkg:%s;dMon:%s;d:%s'%\
                        (vtLog.pformat(dFunc),vtLog.pformat(dWrkPkg),
                         vtLog.pformat(dMon),vtLog.pformat(d)),self.appl)
        return {'func':dFunc,'workpackages':dWrkPkg,'month':dMon,'func_month_values':d}
    def CalcDates(self,bRet=True):
        lWrkPkg=[]
        l=self.GetWorkPackagesHier(None,lWrkPkg)
        #print lWrkPkg
        #print
        #print l
        dtStart=vtDateTime(True)
        dtEnd=vtDateTime(True)
        dtTmp=vtDateTime(True)
        
        def calcDiffDay(dtEnd,dtStart):
            zDiff=dtEnd.dt-dtStart.dt
            return zDiff.days
        d={}
        sDateMin=None
        sDateMax=None
        for tup in lWrkPkg:
            dtStart=vtDateTime(True)
            dtEnd=vtDateTime(True)
            dtStart.SetStr(self.getNodeText(tup,'dateTimeStart'))
            sDt=dtStart.GetDateStr()
            if sDateMin is None:
                sDateMin=sDt
            else:
                if sDt<sDateMin:
                    sDateMin=sDt
            dtEnd.SetStr(self.getNodeText(tup,'dateTimeEnd'))
            sDt=dtEnd.GetDateStr()
            if sDateMax is None:
                sDateMax=sDt
            else:
                if sDt>sDateMax:
                    sDateMax=sDt
            lFollow=[]
            nodeTmp=self.getChild(tup,'followUps')
            for c in self.getChilds(nodeTmp,'followUps'):
                sStyle=self.getNodeText(c,'style')
                cc=self.getChild(c,'WrkPkg')
                try:
                    iStyle=['at end','at start'].index(sStyle)
                    fid=long(self.getAttribute(cc,'fid'))
                    lFollow.append((fid,iStyle))
                except:
                    pass
            id=long(self.getKey(tup))
            #dtS=vtDateTime(True)
            #dtE=vtDateTime(True)
            d[id]=[dtStart,dtEnd,calcDiffDay(dtEnd,dtStart),lFollow,False]
        #print 'start',sDateMin,sDateMax
        dtStart=vtDateTime(True)
        dtEnd=vtDateTime(True)
        dtStart.SetStr(sDateMin+' 00:00:00')
        dtEnd.SetStr(sDateMax+' 00:00:00')
        
        dPath={}
        keys=d.keys()
        bMod=True
        while bMod==True:
            bMod=False
            for k in keys:
                def updateTimes(iActId,iPrevId,style):
                    dTmp=vtDateTime(True)
                    tPrev=d[iPrevId]
                    tAct=d[iActId]
                        
                    if style==0:
                        # attach to end
                        #dTmp.SetStr(tPrev[1].GetDateTimeStr())
                        iDiffDay=calcDiffDay(tAct[0],tPrev[1])
                        #print iPrevId,iActId,iDiffDay
                        if iDiffDay!=0:
                            #print '  ',tAct[0]
                            #print '  ',tAct[1]
                            tAct[0].SetStr(tPrev[1].GetDateTimeStr())
                            tAct[1].SetStr(tPrev[1].GetDateTimeStr())
                            tAct[1].dt+=datetime.timedelta(tAct[2])
                            #print '    ',tAct[0]
                            #print '    ',tAct[1]
                            tAct[-1]=True
                            return True
                    elif style==1:
                        # attach to start
                        #dTmp.SetStr(tPrev[0].GetDateTimeStr())
                        iDiffDay=calcDiffDay(tAct[0],tPrev[0])
                        if iDiffDay!=0:
                            #print '  ',tAct[0]
                            #print '  ',tAct[1]
                            tAct[0].SetStr(tPrev[0].GetDateTimeStr())
                            tAct[1].SetStr(tPrev[0].GetDateTimeStr())
                            tAct[1].dt+=datetime.timedelta(tAct[2])
                            #print '    ',tAct[0]
                            #print '    ',tAct[1]
                            tAct[-1]=True
                            return True
                    else:
                        iDiffDay=0
                        
                    return False
                lStack=[]
                def walk(id,lStack):
                    #print '  '*len(lStack),'walk id:',id,'stack:',lStack
                    act=d[id]
                    lFollow=act[3]
                    lStack.append(id)
                    bMod=False
                    for idNext,style in lFollow:
                        try:
                            bF=lStack.index(idNext)
                            # circular dependency
                            #print 'circle'
                            continue
                        except:
                            # ok
                            pass
                        if updateTimes(idNext,id,style):
                            bMod=True
                        if walk(idNext,lStack):
                            bMod=True
                    lStack.pop()
                    return bMod
                bMod=walk(k,lStack)
        for k in keys:
            nodeTmp=self.getNodeByIdNum(k)
            self.setNodeText(nodeTmp,'dateTimeStart',d[k][0].GetDateTimeStr())
            self.setNodeText(nodeTmp,'dateTimeEnd',d[k][1].GetDateTimeStr())
    def Build(self):
        vtXmlDomReg.Build(self)
        oSMlinker=self.GetRegisteredNode('stateMachineLinker')
        tmp=self.getChildByLst(None,['cfg','stateMachineLinker'])
        oSMlinker.SetNode(tmp)
        #self.GenerateSortedActNrIds()
    def GetStateMachine(self,nodeName,attrName):
        oSMlinker=self.GetRegisteredNode('stateMachineLinker')
        return oSMlinker.GetRunner(nodeName,attrName)
