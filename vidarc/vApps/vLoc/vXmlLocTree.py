#----------------------------------------------------------------------------
# Name:         vXmlLocTree.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051230
# CVS-ID:       $Id: vXmlLocTree.py,v 1.5 2006/06/07 10:12:46 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlGrpTree import *
from vidarc.vApps.vLoc.vNetLoc import *
from vidarc.tool.loc.vLocCountries import COUNTRIES
import images

class vXmlLocTree(vtXmlGrpTree):
    def __init__(self, parent, id, pos, size, style, name,
                    master=False,controller=False,verbose=0):
        vtXmlGrpTree.__init__(self,id=id, name=name,master=master,
              controller=controller,verbose=verbose,
              parent=parent, pos=pos, size=size,style=style)
        self.bLangMenu=True
        self.bGrpMenu=True
        self.iGrp=0
        self.grpMenu=[
                ('normal',_(u'normal'),
                    [],
                    [('name','')],
                ),
                ('country',_(u'country'),
                    [('country','')],[('name','')],
                    [('name','')],
                ),
                #('region',_(u'region'),
                #    [('country',''),('region','')],[('name','')],
                #    [('name','')],
                #),
                ]
    def SetDftGrouping(self):
        self.grouping=[]
        self.label=[('name','')]
    def SetDftNodeInfos(self):
        self.SetNodeInfos(['name','|id','country','region'])
    def SetGroupingByNameOld(self,sGrp):
        if sGrp=='normal':
            return self.SetNormalGrouping()
        elif sGrp=='country':
            return self.SetCountryGrouping()
        return False
    def SetNormalGrouping(self):
        if self.iGrp==0:
            return False
        self.iGrp=0
        self.SetGrouping([],[('name','')])
        return True
    def SetCountryGrouping(self):
        if self.iGrp==1:
            return False
        self.iGrp=1
        self.SetGrouping([('country','')],[('name','')])
        return True
    def SetupImageList(self):
        self.imgDict={'elem':{},'txt':{},'cdata':{},'comm':{},'grp':{}}
        self.imgLstTyp=wx.ImageList(16,16)
        img=images.getElementBitmap()
        self.imgDict['elem']['dft']=[self.imgLstTyp.Add(img)]
        img=images.getElementSelBitmap()
        self.imgDict['elem']['dft'].append(self.imgLstTyp.Add(img))
        
        img=images.getPluginBitmap()
        self.imgDict['elem']['locations']=[self.imgLstTyp.Add(img)]
        self.imgDict['elem']['root']=[self.imgLstTyp.Add(img)]
        img=images.getPluginBitmap()
        self.imgDict['elem']['locations'].append(self.imgLstTyp.Add(img))
        self.imgDict['elem']['root'].append(self.imgLstTyp.Add(img))
        
        img=images.getLocBitmap()
        self.imgDict['elem']['location']=[self.imgLstTyp.Add(img)]
        img=images.getLocSelBitmap()
        self.imgDict['elem']['location'].append(self.imgLstTyp.Add(img))
        self.SetImageList(self.imgLstTyp)
        
        img=images.getCountryBitmap()
        self.imgDict['grp']['country']=[self.imgLstTyp.Add(img)]
        img=images.getCountrySelBitmap()
        self.imgDict['grp']['country'].append(self.imgLstTyp.Add(img))
        self.SetImageList(self.imgLstTyp)
    def __getValFormat__(self,g,val,infos=None):
        try:
            if val is None:
                return '---'
            val=string.strip(val)
            if g[0]=='country':
                return COUNTRIES[val]
        except:
            pass
        return vtXmlGrpTree.__getValFormat__(self,g,val,infos)
        
    def __getImgFormat__(self,g,val):
        #vtLog.CallStack(val)
        #vtLog.pprint(g)
        try:
            val=string.strip(val)
            if len(g[1])>0:
                sFormat=g[1]
                if g[0]=='date':
                    if sFormat=='YYYY':
                        img=self.imgDict['grp']['YYYY'][0]
                        imgSel=self.imgDict['grp']['YYYY'][0]
                        return (img,imgSel)
                    elif sFormat=='MM':
                        img=self.imgDict['grp']['MM'][0]
                        imgSel=self.imgDict['grp']['MM'][0]
                        return (img,imgSel)
                    elif sFormat=='DD':
                        img=self.imgDict['grp']['DD'][0]
                        imgSel=self.imgDict['grp']['DD'][0]
                        return (img,imgSel)
        except:
            pass
        try:
            img=self.imgDict['grp'][g[0]][0]
            imgSel=self.imgDict['grp'][g[0]][1]
            return (img,imgSel)
        except:
            pass
        if g[0]=='attributes:clientshort':
            #if sFormat=='':
            img=self.imgDict['grp']['attributes:clientshort'][0]
            imgSel=self.imgDict['grp']['attributes:clientshort'][0]
            return (img,imgSel)
        #tagName=self.doc.getTagName(o)
        try:
            img=self.imgDict['elem'][g[0]][0]
        except:
            img=self.imgDict['elem']['dft'][0]
        try:
            imgSel=self.imgDict['elem'][g[0]][1]
        except:
            imgSel=self.imgDict['elem']['dft'][1]
        return (img,imgSel)
