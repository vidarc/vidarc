#----------------------------------------------------------------------------
# Name:         vXmlLoc.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vXmlLoc.py,v 1.10 2008/02/02 16:10:53 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog

from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vidarc.vApps.vLoc.vXmlNodeLocRoot import vXmlNodeLocRoot
from vidarc.vApps.vLoc.vXmlNodeLocation import vXmlNodeLocation
VERBOSE=1

def getPluginData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x95IDAT8\x8d\xadS\xb1\r\x800\x0cs\n_p\x03\xac<R\x89WzB\xc5-}\x84\
\xb97p\x07L\xa0\xaa\x89+\x10dtb\xd7\x8d\x1c\x11\xd7\xa1\xae1\xfaC\x81\x00rHR\
c\xee\xe9\xa0\x85\x01\x80\\\x0e\xd8\xab\xac.A\xd3\xc1\x9bj\n\xa4aC\x1a\xb6\
\xb6\x00\xb3^\x12\x99\xc8\x18\xfd!\xd3\xba(\x01F\xf0\xfb\xac\x1d<%\xb3\xde\
\xffK\xb4l\xb6z\x0e\xd0!\xb1\x06k\xec\xce\x01KXI`\xaerH\xd2S\xbf\rbY\x9f\x97\
(\xd65\x02\xfa6\xd8WM\x07V:YbO3_8\x9f\xa1\x08]G\x00\x00\x00\x00IEND\xaeB`\
\x82' 

class vXmlLoc(vtXmlDomReg):
    TAGNAME_REFERENCE='name'
    TAGNAME_ROOT='locationsroot'
    NODE_ATTRS=[
            ('name',None,'name',None),
            ('country',None,'contry',None),
            ('region',None,'region',None),
            ('distance',None,'distance',None),
            ('coor',None,'coor',None),
            ]
    APPL_REF=['vGlobals']
    def __init__(self,attr='id',skip=[],synch=False,appl='vLoc',verbose=0,
                    audit_trail=True):
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        vtXmlDomReg.__init__(self,attr=attr,skip=skip,synch=synch,appl=appl,verbose=verbose,
                    audit_trail=audit_trail)
        self.verbose=verbose
        try:
            oRoot=vXmlNodeLocRoot()
            oLoc=vXmlNodeLocation()
            self.RegisterNode(vXmlNodeLocRoot(tagName='root'),False)
            self.RegisterNode(oRoot,False)
            self.RegisterNode(oLoc,True)
            
            self.LinkRegisteredNode(oRoot,oLoc)
        except:
            vtLog.vtLngTB(self.appl)
    def __genIds__(self,c,node,ids):
        sTag=self.getTagName(c)
        if sTag in self.skip:
            return 0
        id=self.getAttribute(c,self.attr)
        if self.IsKeyValid(id):
            ids.CheckId(c,self,id)
            self.updateParentDict(node,c)
        elif sTag in ['location']:
            ids.CheckId(c,self,id)
            self.updateParentDict(node,c)
        #self.__genIds__(c,ids)
        self.procChildsKeys(c,self.__genIds__,c,ids)
        return 0
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'locations')
    #def New(self,revision='1.0',root='locationsroot',bConnection=False):
    #    vtXmlDomReg.New(self,revision,root)
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elem=self.createSubNode(self.getRoot(),'locations')
        if bConnection==False:
            self.CreateReq()
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        #self.AlignDoc()
    

