#----------------------------------------------------------------------------
# Name:         vXmlLocInputTree.py
# Purpose:      input widget for human / user xml
#               text and popup with tree
# Author:       Walter Obweger
#
# Created:      20060203
# CVS-ID:       $Id: vXmlLocInputTree.py,v 1.2 2006/03/02 10:56:04 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from vidarc.tool.input.vtInputTree import vtInputTree
import vidarc.tool.log.vtLog as vtLog
from vidarc.vApps.vLoc.vXmlLocTree import vXmlLocTree

class vXmlLocInputTree(vtInputTree):
    def __init__(self,*_args,**_kwargs):
        apply(vtInputTree.__init__,(self,) + _args,_kwargs)
        self.tagName='loc'
        #self.tagNameInt='name'
        self.trClass=self.__createTree__
        self.trShow=self.__showNode__
    def __createTree__(*args,**kwargs):
        tr=vXmlLocTree(**kwargs)
        #tr.SetNodeInfos([args[0].tagNameInt,'|id','country','region'])
        tr.SetGrouping([],[(args[0].tagNameInt,'')])
        tr.EnableLanguageMenu()
        return tr
    def __showNode__(self,doc,node,lang):
        return doc.getNodeText(node,self.tagNameInt)
