#----------------------------------------------------------------------------
# Name:         vXmlNodeLocRoot.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060607
# CVS-ID:       $Id: vXmlNodeLocRoot.py,v 1.4 2007/04/18 16:00:22 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeRoot import *
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeLocRoot(vtXmlNodeRoot):
    def __init__(self,tagName='locations'):
        global _
        _=vtLgBase.assignPluginLang('vLoc')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'location root')
    # ---------------------------------------------------------
    # inheritance
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x95IDAT8\x8d\xadS\xb1\r\x800\x0cs\n_p\x03\xac<R\x89WzB\xc5-}\x84\
\xb97p\x07L\xa0\xaa\x89+\x10dtb\xd7\x8d\x1c\x11\xd7\xa1\xae1\xfaC\x81\x00rHR\
c\xee\xe9\xa0\x85\x01\x80\\\x0e\xd8\xab\xac.A\xd3\xc1\x9bj\n\xa4aC\x1a\xb6\
\xb6\x00\xb3^\x12\x99\xc8\x18\xfd!\xd3\xba(\x01F\xf0\xfb\xac\x1d<%\xb3\xde\
\xffK\xb4l\xb6z\x0e\xd0!\xb1\x06k\xec\xce\x01KXI`\xaerH\xd2S\xbf\rbY\x9f\x97\
(\xd65\x02\xfa6\xd8WM\x07V:YbO3_8\x9f\xa1\x08]G\x00\x00\x00\x00IEND\xaeB`\
\x82' 
