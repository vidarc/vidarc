#Boa:Dialog:vXmlNodeLocationEditDialog

import wx
import wx.lib.buttons

from vLocPanel import *

import vidarc.tool.art.vtArt as vtArt

def create(parent):
    return vXmlNodeLocationEditDialog(parent)

[wxID_VXMLNODELOCATIONEDITDIALOG, wxID_VXMLNODELOCATIONEDITDIALOGCBAPPLY, 
 wxID_VXMLNODELOCATIONEDITDIALOGCBCANCEL, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vXmlNodeLocationEditDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VXMLNODELOCATIONEDITDIALOG,
              name=u'vXmlNodeLocationEditDialog', parent=prnt, pos=wx.Point(348,
              174), size=wx.Size(327, 230), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vLoc Localtion Edit Dialog')
        self.SetClientSize(wx.Size(319, 203))

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODELOCATIONEDITDIALOGCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'),
              name=u'cbApply', parent=self, pos=wx.Point(72, 168),
              size=wx.Size(76, 30), style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VXMLNODELOCATIONEDITDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODELOCATIONEDITDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(184, 168),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VXMLNODELOCATIONEDITDIALOGCBCANCEL)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.doc=None
        self.node=None
        
        self.pnLoc=vLocPanel(self,id=wx.NewId(),pos=(0,0),size=(310,150),
                        style=0,name=u'pnLoc')
    def SetRegNode(self,obj):
        self.pnLoc.SetRegNode(obj)
    def SetDoc(self,doc,bNet=False):
        self.pnLoc.SetDoc(doc,bNet)
    def SetNode(self,node):
        self.pnLoc.SetNode(node)
    def GetNode(self):
        self.pnLoc.GetNode()
    def OnCbApplyButton(self, event):
        self.GetNode()
        self.EndModal(1)
        event.Skip()
    def OnCbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
    
