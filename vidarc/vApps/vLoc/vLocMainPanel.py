#Boa:FramePanel:vLocMainPanel
#----------------------------------------------------------------------------
# Name:         vLocMainPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060529
# CVS-ID:       $Id: vLocMainPanel.py,v 1.9 2007/07/30 20:38:47 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons
import  wx.lib.anchors as anchors
import getpass,time

try:
    import vidarc.tool.log.vtLog as vtLog
    from vidarc.vApps.vLoc.vXmlLocTree  import *
    from vidarc.vApps.vLoc.vNetLoc import *
    from vidarc.vApps.vLoc.vLocPanel import *
    from vidarc.vApps.vGlobals.vNetGlobals import *
    from vidarc.tool.net.vtNetSecXmlGuiMaster import *
    import vidarc.tool.InOut.fnUtil as fnUtil
    import vidarc.tool.lang.vtLgBase as vtLgBase
except:
    vtLog.vtLngTB('import')
import images
import images_lang

VERBOSE=0

def create(parent):
    return vLocMainFrame(parent)

[wxID_VGFPRJMAIN, wxID_VGFPRJMAINSPWMAIN, 
] = map(lambda _init_ctrls: wx.NewId(), range(2))

[wxID_VGFPRJMAINMNFILEITEMS_OPEN, wxID_VGFPRJMAINMNFILEITEM_EXIT, 
 wxID_VGFPRJMAINMNFILEITEM_SAVE, wxID_VGFPRJMAINMNFILEITEM_SAVEAS, 
] = map(lambda _init_coll_mnFile_Items: wx.NewId(), range(4))

[wxID_VLOCMAINFRAME, wxID_VLOCMAINFRAMECBAPPLY, wxID_VLOCMAINFRAMECBCANCEL, 
 wxID_VLOCMAINFRAMEPNDATA, wxID_VLOCMAINFRAMESBSTATUS, 
 wxID_VLOCMAINFRAMESLWNAV, 
] = [wx.NewId() for _init_ctrls in range(6)]

[wxID_VLOCMAINFRAMEMNFILEITEMS_OPEN, wxID_VLOCMAINFRAMEMNFILEITEM_EXIT, 
 wxID_VLOCMAINFRAMEMNFILEITEM_NEW, wxID_VLOCMAINFRAMEMNFILEITEM_OPEN_CFG, 
 wxID_VLOCMAINFRAMEMNFILEITEM_SAVE, wxID_VLOCMAINFRAMEMNFILEITEM_SAVEAS, 
 wxID_VLOCMAINFRAMEMNFILEITEM_SAVE_AS_CFG, 
 wxID_VLOCMAINFRAMEMNFILEITEM_SETTING, 
] = [wx.NewId() for _init_coll_mnFile_Items in range(8)]

[wxID_VLOCMAINFRAMEMNIMPORTITEM_IMPORT_CSV, 
 wxID_VLOCMAINFRAMEMNIMPORTITEM_IMPORT_XML, 
] = [wx.NewId() for _init_coll_mnImport_Items in range(2)]

[wxID_VLOCMAINFRAMEMNEXPORTITEM_EXPORT_CSV, 
 wxID_VLOCMAINFRAMEMNEXPORTITEM_EXPORT_XML, 
] = [wx.NewId() for _init_coll_mnExport_Items in range(2)]

[wxID_VLOCMAINFRAMEMNTOOLSITEM_ALIGN, wxID_VLOCMAINFRAMEMNTOOLSITEM_MNEXPORT, 
 wxID_VLOCMAINFRAMEMNTOOLSITEM_MN_IMPORT, 
 wxID_VLOCMAINFRAMEMNTOOLSMN_TOOL_REQ, 
] = [wx.NewId() for _init_coll_mnTools_Items in range(4)]

[wxID_VLOCMAINFRAMEMNVIEWTREEITEM_DATE_SHORT_USR_GRP, 
 wxID_VLOCMAINFRAMEMNVIEWTREEITEM_DATE_USR_GRP, 
] = [wx.NewId() for _init_coll_mnViewTree_Items in range(2)]

STATUS_TEXT_POS=2
STATUS_LOG_POS=3
STATUS_CLK_POS=4

[wxID_VGFPRJTIMERMAINTOOLBAR1TOOLS0] = [wx.NewId() for _init_coll_toolBar1_Tools in range(1)]

[wxID_VLOCMAINFRAMEMNVIEWITEM_LANG, wxID_VLOCMAINFRAMEMNVIEWITEM_VIEW_TREE, 
] = [wx.NewId() for _init_coll_mnView_Items in range(2)]

[wxID_VLOCMAINFRAMEMNLANGITEM_LANG_DE, wxID_VLOCMAINFRAMEMNLANGITEM_LANG_EN, 
] = [wx.NewId() for _init_coll_mnLang_Items in range(2)]

[wxID_VLOCMAINFRAMEMNHELPMN_HELP_ABOUT, wxID_VLOCMAINFRAMEMNHELPMN_HELP_HELP, 
 wxID_VLOCMAINFRAMEMNHELPMN_HELP_LOG, 
] = [wx.NewId() for _init_coll_mnHelp_Items in range(3)]

class vLocMainPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VLOCMAINFRAME, name=u'vLocMainFrame',
              parent=prnt, pos=wx.Point(115, 10), size=wx.Size(672, 330),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(664, 303))
        self.Bind(wx.EVT_SIZE, self.OnSize)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VLOCMAINFRAMESLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 0), size=wx.Size(200,
              264), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetAutoLayout(True)
        self.slwNav.SetLabel(u'Navigation')
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetToolTipString(u'navigation')
        self.slwNav.SetDefaultSize(wx.Size(200, 100))
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VLOCMAINFRAMESLWNAV)

        self.pnData = wx.Panel(id=wxID_VLOCMAINFRAMEPNDATA, name=u'pnData',
              parent=self, pos=wx.Point(208, 10), size=wx.Size(448, 246),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)
        self.pnData.SetAutoLayout(True)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VLOCMAINFRAMECBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Apply'), name=u'cbApply',
              parent=self.pnData, pos=wx.Point(365, 8), size=wx.Size(76, 30),
              style=0)
        self.cbApply.SetConstraints(LayoutAnchors(self.cbApply, False, True,
              True, False))
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VLOCMAINFRAMECBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VLOCMAINFRAMECBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Cancel'), name=u'cbCancel',
              parent=self.pnData, pos=wx.Point(365, 48), size=wx.Size(76, 30),
              style=0)
        self.cbCancel.SetConstraints(LayoutAnchors(self.cbCancel, False, True,
              True, False))
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VLOCMAINFRAMECBCANCEL)

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vLoc')
        self._init_ctrls(parent)
        self.SetName(name)
        self.bActivated=False
        self.dCfg={}
        self.cfgFunc=None
        
        self.nodeCurStarted=None
        self.nodeCurStartedTreeItem=None
        self.node=None
        self.nodeLock=None
        
        self.baseDoc=None
        self.baseSettings=None
        
        self.xdCfg=vtXmlDom('vLocCfg',audit_trail=False)
        
        appls=['vLoc','vGlobals']
        
        rect = parent.sbStatus.GetFieldRect(0)
        self.netMaster=vtNetSecXmlGuiMaster(parent.sbStatus,'Master',pos=(rect.x+2, rect.y+2),
                    size=(rect.width-4, rect.height-4),verbose=0)
        
        self.netLoc=self.netMaster.AddNetControl(vNetLoc,'vLoc',synch=True,verbose=0,audit_trail=True)
        self.netGlb=self.netMaster.AddNetControl(vNetGlobals,'vGlobals',synch=True,verbose=0)
        self.netMaster.AddNetControl(None,None,verbose=0)
        
        self.netLoc.SetDftLanguages()
        self.netMaster.SetCfg(self.xdCfg,[],appls,'vLocAppl')
        self.netMaster.SetLang(lang=vtLgBase.getPluginLang())
        
        self.vgpTree=vXmlLocTree(self.slwNav,wx.NewId(),
                pos=(0,0),size=(200,482),style=0,name="trLoc",
                master=True,verbose=0)
        self.vgpTree.SetConstraints(
            anchors.LayoutAnchors(self.vgpTree, True, True, True, True)
            )
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        self.vgpTree.SetDoc(self.netLoc,True)
        
        self.cbApply.SetBitmapLabel(images.getApplyBitmap())
        self.cbCancel.SetBitmapLabel(images.getCancelBitmap())
        self.pnLoc=vLocPanel(self.pnData,wx.NewId(),
            pos=(8,8),size=(348, 346),style=0,
            name='vLocPanel')
        self.pnLoc.SetConstraints(
            anchors.LayoutAnchors(self.pnLoc, True, True, True, True)
            )
        self.pnLoc.SetDoc(self.netLoc,True)
        self.pnLoc.SetNode(None)
        
        self.Move(pos)
        self.SetSize(size)
        #self.actLanguage='en'
        #self.vgpTree.SetLanguage(self.actLanguage)
        #self.CreateNew()
        
    def GetDocMain(self):
        return self.netLoc
    def GetNetMaster(self):
        return self.netMaster
    def GetTreeMain(self):
        return self.vgpTree

    def OnTreeItemSel(self,event):
        #vtLog.CallStack('')
        self.netLoc.endEdit(None)
        node=event.GetTreeNodeData()
        if event.GetTreeNodeData() is not None:
            self.netLoc.startEdit(node)
            self.pnLoc.SetNode(node)
        else:
            # grouping tree item selected
            self.pnLoc.SetNode(None)
            pass
        event.Skip()
    def OnTreeItemAdd(self,event):
        #print "added",event.GetVgpTree(),event.GetTreeNode(),event.GetTreeNodeData()
        #vtXmlDomAlign(self.dom)
        event.Skip()
    def __getSettings__(self):
        return
        self.xmlLastPrj=None
        self.xmlLastTask=None
        self.xmlLastSubTask=None
        self.xmlLastDesc=None
        self.xmlLastLoc=None
        self.xmlLastPerson=None
        
        self.xmlPrjFN=self.netDoc.getNodeText(self.baseSettings,'projectfn')
        self.xmlHumanFN=self.netDoc.getNodeText(self.baseSettings,'humanfn')
        self.xmlLocationFN=self.netDoc.getNodeText(self.baseSettings,'locationfn')
        self.xmlBaseTmplDN=self.netDoc.getNodeText(self.baseSettings,'templatedn')
        self.xmlProjectDN=self.netDoc.getNodeText(self.baseSettings,'projectdn')
        # get tree view settings
        sTreeViewGrp=self.netDoc.getNodeText(self.baseSettings,'treeviewgrp')
        mnItems=self.mnViewTree.GetMenuItems()
        
        self.vglbDocGrpInfo.SetTemplateBaseDir(self.xmlBaseTmplDN)
        if sTreeViewGrp=='normal':
            mnItems[0].Check(True)
            self.__EnsureOneCheckMenuItem__(mnItems,0,0,2)
            self.vgpTree.SetNormalGrouping()
        elif sTreeViewGrp=='iddocgrp':
            mnItems[1].Check(True)
            self.__EnsureOneCheckMenuItem__(mnItems,1,0,2)
            self.vgpTree.SetIdGrouping()
        else:
            mnItems[0].Check(True)
            self.__EnsureOneCheckMenuItem__(mnItems,0,0,2)
            #self.vgpTree.SetGrpUsrDate()
            
    def __getXmlBaseNodes__(self):
        self.baseDoc=None
        self.baseSettings=None
        
        self.baseDoc=self.netLoc.getBaseNode()
        self.baseSettings=self.netLoc.getChildForced(self.netLoc.getRoot(),'settings')
        #if self.baseSettings is None:
        #    self.baseSettings=self.netDoc.createSubNode(self.netDoc.getRoot(),'settings')
        #    self.netDoc.AlignNode(self.baseSettings,iRec=1)
        self.__getSettings__()
        self.netLoc.AlignNode(self.baseSettings,iRec=1)
    def OpenFile(self,fn):
        if self.netLoc.ClearAutoFN()>0:
            self.netLoc.Open(fn)
            
            self.__setModified__(False)
            self.__getXmlBaseNodes__()
        #if self.bAutoConnect:
        #    self.netDoc.Connect2SrvByNode(None,None)
        #    self.netHum.Connect2SrvByNode(None,None)
        
            self.vgpTree.SetNode(None)
        #self.docs=vtXmlDomTree.SortedNodeIds()
        #self.docs.SetNode(self.baseDoc,'docgroup','id',
        #                [('key','%s'),('title','%s')])
        
            self.PrintMsg(_('open file finished. '))
    def OpenCfgFile(self,fn=None):
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                self.netMaster.SetCfgNode()
                self.__setCfg__()
                return
        self.xdCfg.New(root='config')
        if fn is not None:
            self.xdCfg.SetFN(fn)
            self.netMaster.SetCfgNode()
        self.__setCfg__()
    def CreateNew(self):
        self.netLoc.New()
        self.__getXmlBaseNodes__()
        self.vgpTree.SetNode(None)
        
    def OnFrameActivate(self, event):
        if self.bActivated==False:
            self.netMaster.ShowPopup()
            self.bActivated=True
        event.Skip()

    def OnCbApplyButton(self, event):
        self.pnLoc.GetNode()
        node=self.vgpTree.GetSelected()
        if node is not None:
            self.netLoc.doEdit(node)
        event.Skip()

    def OnCbCancelButton(self, event):
        event.Skip()
        self.pnLoc.Cancel()
    def __setCfg__(self):
        wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        self.pnData.Refresh()
        
        self.bBlockCfgEventHandling=False

    def OnSlwNavSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iWidth=event.GetDragRect().width
            self.dCfg['nav_sash']=str(iWidth)
            
            self.slwNav.SetDefaultSize((iWidth, 1000))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnMove(self, event):
        event.Skip()
        try:
            pos=self.GetPosition()
            node=self.xdCfg.getChildForced(None,'size')
            self.xdCfg.SetValue(node,'x',pos[0])
            self.xdCfg.SetValue(node,'y',pos[1])
            self.xdCfg.Save()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnSize(self, event):
        event.Skip()
        try:
            bMod=False
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        except:
            vtLog.vtLngTB(self.GetName())

    def OnSbStatusSize(self, event):
        try:
            rect = self.sbStatus.GetFieldRect(1)
            self.gProcess.SetSize((rect.width-4, rect.height-4))
        except:
            pass
        event.Skip()
    def SetCfgData(self,func,l,d):
        self.cfgFunc=func
        self.cfgOrigin=l
        self.dCfg=d
        try:
            i=int(self.dCfg['nav_sash'])
            self.slwNav.SetDefaultSize((i, 1000))
        except:
            pass
        try:
            i=int(self.dCfg['top_sash'])
            self.slwTop.SetDefaultSize((1000,i))
        except:
            pass
        #wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
        #self.pnData.Refresh()
        #try:
        #    sz=map(int,self.dCfg['size'].split(','))
        #    self.GetParent().SetSize(sz)
        #except:
        #    pass
    def GetCfgData(self):
        #try:
        #    sz=self.GetParent().GetSize()
        #    self.dCfg['size']='%d,%d'%(sz[0],sz[1])
        #except:
        #    pass
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,self.dCfg)
    def GetAboutData(self):
        import __config__
        version=u' %d.%d.%d.%d'%(__config__.VER_MAJOR,__config__.VER_MINOR,
                    __config__.VER_RELEASE,__config__.VER_SUBREL)
        desc=_(u"""Location module.

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
""")
        return _(u'VIDARC Location'),desc,version
