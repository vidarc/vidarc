#----------------------------------------------------------------------------
# Name:         vXmlNodeLocation.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060101
# CVS-ID:       $Id: vXmlNodeLocation.py,v 1.8 2010/03/03 02:17:13 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlNodeBase import *
#from vidarc.vApps.vLoc.vXmlLoc import COUNTRIES
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

try:
    if vcCust.is2Import(__name__):
        from vidarc.vApps.vLoc.vLocPanel import vLocPanel
        #from vidarc.vApps.vLoc.vXmlNodeLocationEditDialog import *
        #from vidarc.vApps.vLoc.vXmlNodeLocationAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0


class vXmlNodeLocation(vtXmlNodeBase):
    NODE_ATTRS=[
            ('Name',None,'name',None),
            ('Country',None,'contry',None),
            ('Region',None,'region',None),
            ('Distance',None,'distance',None),
            ('Coor',None,'coor',None),
        ]
    def __init__(self,tagName='location'):
        global _
        _=vtLgBase.assignPluginLang('vLoc')
        vtXmlNodeBase.__init__(self)
        self.tagName=tagName
        self.lstChilds=[]
        self.dDlgEdit={}
        self.dDlgAdd={}
        self.doc=None
    def GetDescription(self):
        return _(u'location')
        
    def GetName(self,node):
        return self.Get(node,'name')
    def GetCountry(self,node):
        return self.Get(node,'country')
    def GetCoor(self,node):
        return self.Get(node,'coor')
    def GetDistance(self,node):
        return self.Get(node,'distance')
    def GetRegion(self,node):
        return self.Get(node,'region')
    def SetName(self,node,val):
        self.Set(node,'name',val)
    def SetCountry(self,node,val):
        self.Set(node,'country',val)
    def SetRegion(self,node,val):
        self.Set(node,'region',val)
    def SetDistance(self,node,val):
        self.Set(node,'distance',val)
    def SetCoor(self,node,val):
        self.Set(node,'coor',val)
    
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        return [('name',vtXmlFilterType.FILTER_TYPE_STRING),
            ('country',vtXmlFilterType.FILTER_TYPE_STRING),
            ('region',vtXmlFilterType.FILTER_TYPE_STRING),
            ('distance',vtXmlFilterType.FILTER_TYPE_INT),
            ]
    def GetTranslation(self,name):
        _(u'name'),_(u'country'),_(u'region'),_(u'distance')
        return _(name)
    def IsSkip(self):
        return False
    def IsId2Add(self):
        return False
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x00\xe1IDAT(\x91}\x92Av\xc3@\x08C\xbf\x92\x1e\x8c\x9c\xc4G\x198Y\x9d\x93\
\xa9\x0b\xec\xf14i\xea\x05\x0fd=\x10\x1a\x04d\xe6K|/\'(\xc06\xa8*\x811\x06\
\x08\xfcg\xac\xca\x1bPU\xe01\xba\xd9G6\xb8\x7f\x1f\x13\xfe\xe5-\x132\xb3\xeb\
\x9e\xb32\x1a\x99xk\xbe\x01\'\x83\x17\xf6\xc8<\xa2ti\xceL\xfb\xf2\xc4\xc6v#\
\x99i0ti{\x9d0\x1d\xbc\xf6\x1b\xa7\xa7U\xd3\xbd\xa5k\xf7X\xf3\x04\xdb\xfe\
\xad\xe2k\xe9\x9as\x871\x0c\x1a\xed\x9e=\x0e\xbcl\xb7$ID\x84\xc4\'\xc7Z\x8f$\
\xcew8\xbe\xa9\xea=^g\xd2\xfc}\xdf;\x7f\xd9dE\x80; eDl\xdb&)\xe2{j\xe8\xbc*#\
\x1e\x8dHy\x07"\xa2][\xd9\x93\x17\xf1\x90\x90\xf4|\xc6t\xa9\xef\xef8^P\xcb\
\xadJ\xa8\xf5\xc03\xf3\x07\xcf\xbe\xeaG@2\x15e\x00\x00\x00\x00IEND\xaeB`\x82\
'
    def GetEditDialogClass(self):
        if GUI:
            #return vXmlNodeLocationEditDialog
            return {'name':self.__class__.__name__,
                    'sz':(327, 230),'pnName':'pnLoc',
                }
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            #return vXmlNodeLocationAddDialog
            return {'name':self.__class__.__name__,
                    'sz':(327, 230),'pnName':'pnLoc',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vLocPanel
        else:
            return None

