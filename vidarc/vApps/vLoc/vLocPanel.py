#Boa:FramePanel:vLocPanel

import wx
from wx.lib.anchors import LayoutAnchors

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    from vidarc.tool.xml.vtXmlTree import *
    from vidarc.tool.loc.vLocCountries import COUNTRIES

    import vidarc.tool.log.vtLog as vtLog

    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VLOCPANEL, wxID_VLOCPANELCHCCOUNTRY, wxID_VLOCPANELLBLCOUNTRY, 
 wxID_VLOCPANELLBLDIST, wxID_VLOCPANELLBLDISTUNIT, wxID_VLOCPANELLBLLOC, 
 wxID_VLOCPANELLBLREGION, wxID_VLOCPANELSPNDIST, wxID_VLOCPANELTXTLOC, 
 wxID_VLOCPANELTXTREGION, 
] = [wx.NewId() for _init_ctrls in range(10)]

class vLocPanel(wx.Panel,vtXmlNodePanel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VLOCPANEL, name=u'vLocPanel',
              parent=prnt, pos=wx.Point(323, 286), size=wx.Size(240, 146),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(232, 119))
        self.SetAutoLayout(True)

        self.lblLoc = wx.StaticText(id=wxID_VLOCPANELLBLLOC, label=_(u'Location'),
              name=u'lblLoc', parent=self, pos=wx.Point(8, 8), size=wx.Size(80,
              13), style=wx.ALIGN_RIGHT)

        self.txtLoc = wx.TextCtrl(id=wxID_VLOCPANELTXTLOC, name=u'txtLoc',
              parent=self, pos=wx.Point(96, 4), size=wx.Size(128, 21), style=0,
              value=u'')
        self.txtLoc.SetConstraints(LayoutAnchors(self.txtLoc, True, True, True,
              False))

        self.chcCountry = wx.Choice(choices=[], id=wxID_VLOCPANELCHCCOUNTRY,
              name=u'chcCountry', parent=self, pos=wx.Point(96, 32),
              size=wx.Size(130, 21), style=0)
        self.chcCountry.SetConstraints(LayoutAnchors(self.chcCountry, True,
              True, True, False))
        self.chcCountry.Bind(wx.EVT_CHOICE, self.OnChcCountryChoice,
              id=wxID_VLOCPANELCHCCOUNTRY)

        self.txtRegion = wx.TextCtrl(id=wxID_VLOCPANELTXTREGION,
              name=u'txtRegion', parent=self, pos=wx.Point(96, 60),
              size=wx.Size(128, 21), style=0, value=u'')
        self.txtRegion.SetConstraints(LayoutAnchors(self.txtRegion, True, True,
              True, False))

        self.spnDist = wx.SpinCtrl(id=wxID_VLOCPANELSPNDIST, initial=0,
              max=40000, min=0, name=u'spnDist', parent=self, pos=wx.Point(96,
              88), size=wx.Size(108, 21), style=wx.SP_ARROW_KEYS)
        self.spnDist.SetConstraints(LayoutAnchors(self.spnDist, True, True,
              True, False))

        self.lblDistUnit = wx.StaticText(id=wxID_VLOCPANELLBLDISTUNIT,
              label=u'km', name=u'lblDistUnit', parent=self, pos=wx.Point(208,
              92), size=wx.Size(14, 13), style=0)
        self.lblDistUnit.SetConstraints(LayoutAnchors(self.lblDistUnit, False,
              True, True, False))

        self.lblCountry = wx.StaticText(id=wxID_VLOCPANELLBLCOUNTRY,
              label=_(u'Country'), name=u'lblCountry', parent=self, pos=wx.Point(8,
              36), size=wx.Size(80, 13), style=wx.ALIGN_RIGHT)

        self.lblRegion = wx.StaticText(id=wxID_VLOCPANELLBLREGION,
              label=_(u'Region'), name=u'lblRegion', parent=self, pos=wx.Point(8,
              64), size=wx.Size(80, 13), style=wx.ALIGN_RIGHT)

        self.lblDist = wx.StaticText(id=wxID_VLOCPANELLBLDIST,
              label=_(u'Distance'), name=u'lblDist', parent=self, pos=wx.Point(8,
              92), size=wx.Size(80, 13), style=wx.ALIGN_RIGHT)

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vLoc')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,lWidgets=[self.txtLoc,self.txtRegion,
                self.chcCountry,self.spnDist],
                lEvent=[(self.txtLoc,wx.EVT_TEXT),
                        (self.txtRegion,wx.EVT_TEXT),
                        (self.chcCountry,wx.EVT_CHOICE),
                        (self.spnDist,wx.EVT_TEXT),
                        ])

        self.node=None
        self.doc=None
        self.bModified=False
        self.objRegNode=None
        
        self.lstCountries=[]
        for k in COUNTRIES.keys():
            self.lstCountries.append((COUNTRIES[k],k))
        def cmpFunc(a,b):
            return cmp(a[0],b[0])
        self.lstCountries.sort(cmpFunc)
        
        for it in self.lstCountries:
            self.chcCountry.Append(it[0])
            
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
    def SetNetDocs(self,d):
        self.__logCritical__('FIXME'%())
        return
    def SetRegNode(self,obj):
        self.objRegNode=obj
    def OnChcCountryChoice(self, event):
        event.Skip()
        
    #def SetModified(self,state):
    #    self.bModified=state    
    #def __isModified__(self):
    #    if self.bModified:
    #        return True
    #    return False
    def __Clear__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        try:
            self.SetModified(False)
            self.txtLoc.SetValue('')
            self.chcCountry.SetSelection(-1)
            self.txtRegion.SetValue('')
            self.spnDist.SetValue(0)
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def IsBusy(self):
        return False
    def Stop(self):
        pass
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        #if doc is None:
        #    return
        #if bNet==True:
        #    EVT_NET_XML_GOT_CONTENT(doc,self.OnGotContent)
        #    EVT_NET_XML_GET_NODE(doc,self.OnGetNode)
        #    EVT_NET_XML_REMOVE_NODE(doc,self.OnRemoveNode)
        #    EVT_NET_XML_LOCK(doc,self.OnLock)
        #    EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        #if self.doc is not None:
        #    self.doc.DelConsumer(self)
        #self.doc=doc
        #self.doc.AddConsumer(self,self.Clear)
        pass
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            self.txtLoc.SetValue(self.doc.getNodeText(self.node,'name'))
            try:
                sCountry=self.doc.getNodeText(self.node,'country')
                self.chcCountry.SetStringSelection(COUNTRIES[sCountry])
            except:
                try:
                    docGlb=self.doc.GetNetDoc('vGlobals')
                    sCountry=docGlb.GetGlobal(['vLoc','location'],'globalsLoc','country')
                    self.chcCountry.SetStringSelection(COUNTRIES[sCountry])
                except:
                    pass
            self.txtRegion.SetValue(self.doc.getNodeText(self.node,'region'))
            try:
                iDist=long(self.doc.getNodeText(self.node,'distance'))
            except:
                iDist=0
            self.spnDist.SetValue(iDist)
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            self.doc.setNodeText(node,'name',self.txtLoc.GetValue())
            try:
                i=self.chcCountry.GetSelection()
                sCountry=self.lstCountries[i][1]
            except:
                sCountry='ch'
            self.doc.setNodeText(node,'country',sCountry)
            self.doc.setNodeText(node,'region',self.txtRegion.GetValue())
            self.doc.setNodeText(node,'distance',str(self.spnDist.GetValue()))
            self.doc.AlignNode(node,iRec=2)
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            self.txtLoc.Enable(False)
            self.chcCountry.Enable(False)
            self.txtRegion.Enable(False)
            self.spnDist.Enable(False)
        else:
            self.txtLoc.Enable(True)
            self.chcCountry.Enable(True)
            self.txtRegion.Enable(True)
            self.spnDist.Enable(True)
