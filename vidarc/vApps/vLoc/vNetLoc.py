#----------------------------------------------------------------------------
# Name:         vNetLoc.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vNetLoc.py,v 1.7 2007/07/30 20:38:47 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog

from vidarc.vApps.vLoc.vXmlLoc import vXmlLoc
from vidarc.tool.net.vNetXmlWxGui import *

class vNetLoc(vXmlLoc,vNetXmlWxGui):
    def __init__(self,parent,appl,fn='tmpl.xml',attr='id',skip=[],
                pos=(0,0),size=(16,12),synch=False,verbose=0,audit_trail=False):
        vNetXmlWxGui.__init__(self,parent,appl,pos,size,synch=synch,verbose=verbose)
        vXmlLoc.__init__(self,appl=appl,attr=attr,skip=skip,synch=synch,
                        verbose=verbose,audit_trail=audit_trail)
        vXmlLoc.SetFN(self,fn)
    #def Stop(self):
    #    vNetXmlWxGui.Stop(self)
    #    vXmlLoc.Stop(self)
    #def IsRunning(self):
    #    if vNetXmlWxGui.IsRunning(self):
    #        return True
    #    if vXmlLoc.IsRunning(self):
    #        return True
    #    return False
    def New(self,root=None,bAcquire=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        vNetXmlWxGui.New(self)
        vXmlLoc.New(self,root=root,
                bConnection=self.IsFlag(self.CONN_ACTIVE),
                bAcquire=bAcquire)
    def GetFN(self):
        sFN=vNetXmlWxGui.GetFN(self)
        if sFN is not None:
            return sFN
        else:
            return vXmlLoc.GetFN(self)
    def Open(self,fn=None,silent=False,bThread=True):
        if fn is None:
            sFN=vNetXmlWxGui.GetFN(self)
            if sFN is not None:
                fn=sFN
        if fn is None:
            vXmlLoc.New(self)
            return -2
        
        if bThread:
            self.silent=silent
            iRet=vXmlLoc.Open(self,fn,True)
        else:
            iRet=vXmlLoc.Open(self,fn)
            if iRet>=0:
                if silent==False:
                    self.NotifyContent()
            else:
                self.New()
        return iRet
    def Save(self,fn=None,encoding='ISO-8859-1'):
        sFN=vNetXmlWxGui.GetFN(self)
        if sFN is not None:
            fn=sFN
        vNetXmlWxGui.SaveApplDocs(self,encoding=encoding)
        return vXmlLoc.Save(self,fn,encoding)
    def getSetupChildByLst(self,node,lst):
        if lst is None:
            lst=self.lstSetupNode
        return self.docSetup.getChildByLst(node,lst)
    def getChildByLst(self,node,lst):
        if lst is None:
            return None
        return vXmlLoc.getChildByLst(self,node,lst)
    def addNode(self,par,node,func=None,**kwargs):
        vXmlLoc.addNode(self,par,node)
        vNetXmlWxGui.addNode(self,par,node,func,**kwargs)
    def delNode(self,node):
        vNetXmlWxGui.delNode(self,node)
        vXmlLoc.delNode(self,node)
    def IsRunning(self):
        if vNetXmlWxGui.__isRunning__(self):
            return True
        if vXmlLoc.__isRunning__(self):
            return True
        return False
    def __stop__(self):
        vNetXmlWxGui.__stop__(self)
        vXmlLoc.__stop__(self)
    def NotifyContent(self):
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyContent(self)
    def NotifyGetNode(self,id):
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyGetNode(self,id)
    def NotifyAddNode(self,idPar,id,content):
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyAddNode(self,idPar,id,content)
    def NotifyRemoveNode(self,id):
        self.GenerateSortedIds()
        vNetXmlWxGui.NotifyRemoveNode(self,id)
    def GenerateSortedIds(self):
        self.locs=self.getSortedNodeIds(['locations'],
                        'location','id',[('name','%s')])
    def GetSortedIds(self):
        try:
            return self.locs
        except:
            self.GenerateSortedIds()
            return self.locs
            