#!/usr/bin/env python
#----------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    #"   -u -i -n Application Tools01_16.png images.py",
#    "   -u -i -n Mondrian mondrian.ico images.py",
    "-u -i -n Element img/Box01_16.png images.py",
    "-a -u -n ElementSel img/Box02_16.png images.py",
    "-a -u -n Locations img/vidarcAppIconLoc_16.png images.py",
    "-a -u -n LocationsSel img/vidarcAppIconLoc_16.png images.py",
    "-a -u -n Country img/MapEarth01_16.png images.py",
    "-a -u -n CountrySel img/MapEarth01_16.png images.py",
    "-a -u -n Loc img/MapCity02_16.png images.py",
    "-a -u -n LocSel img/MapCity03_16.png images.py",
    "-a -u -n Add img/add.png images.py",
    "-a -u -n Cancel img/abort.png images.py",
    "-a -u -n Del img/waste.png images.py",
    "-a -u -n Apply img/ok.png images.py",
    "-a -u -n Browse img/BrowseFile01_16.png images.py",
    "-a -u -n Open img/DocEdit01_16.png images.py",
    "-a -u -n Application img/VidMod_Loc_16.ico images.py",
    "-a -u -n Plugin img/VidMod_Loc_16.png images.py",
    
    "-u -i -n Lang00 img/Lang_en_16.png images_lang.py",
    "-a -u -n Lang01 img/Lang_de_16.png images_lang.py",
    "-a -u -n Lang00Big img/Lang_en_32.png images_lang.py",
    "-a -u -n Lang01Big img/Lang_de_32.png images_lang.py",
    "-a -u -n ApplyMultiple img/ApplyMultiple01_32.png images_lang.py",
    
    "-u -i -n Splash img/splashTmpl02.png images_splash.py",
    
    "-a -u -n NoIcon  img/noicon.png  images.py"
    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)

