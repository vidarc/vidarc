#Boa:Dialog:vXmlNodeLocationAddDialog

import wx
import wx.lib.buttons

from vLocPanel import *

import vidarc.tool.art.vtArt as vtArt

def create(parent):
    return vXmlNodeLocationAddDialog(parent)

[wxID_VXMLNODELOCATIONADDDIALOG, wxID_VXMLNODELOCATIONADDDIALOGCBAPPLY, 
 wxID_VXMLNODELOCATIONADDDIALOGCBCANCEL, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vXmlNodeLocationAddDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VXMLNODELOCATIONADDDIALOG,
              name=u'vXmlNodeLocationAddDialog', parent=prnt, pos=wx.Point(348,
              174), size=wx.Size(327, 230), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vLoc Localtion Add Dialog')
        self.SetClientSize(wx.Size(319, 203))

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODELOCATIONADDDIALOGCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'),
              name=u'cbApply', parent=self, pos=wx.Point(72, 168),
              size=wx.Size(76, 30), style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VXMLNODELOCATIONADDDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODELOCATIONADDDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(184, 168),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VXMLNODELOCATIONADDDIALOGCBCANCEL)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.doc=None
        self.node=None
        
        self.pnLoc=vLocPanel(self,id=wx.NewId(),pos=(0,0),size=(310,150),
                        style=0,name=u'pnLoc')
    def SetRegNode(self,obj):
        self.pnLoc.SetRegNode(obj)
    def SetDoc(self,doc,bNet=False):
        self.pnLoc.SetDoc(doc,bNet)
    def SetNode(self,nodeObj,nodePar,nodeDft=None):
        self.nodeObj=nodeObj
        self.nodePar=nodePar
        #self.pnLoc.Clear()
        self.pnLoc.SetNode(nodeDft)
    def GetNode(self,node):
        self.pnLoc.GetNode(node)
    def OnCbApplyButton(self, event):
        self.EndModal(1)
        event.Skip()
    def OnCbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
    
