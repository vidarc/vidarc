#Boa:FramePanel:vModifyPanel
#----------------------------------------------------------------------------
# Name:         vModifyPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vModifyPanel.py,v 1.4 2006/10/09 11:11:39 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

from vidarc.tool.time.vTimeDateGM import *
from vidarc.tool.time.vTimeTimeEdit import *
import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.net.vNetModifyThread import *
from vidarc.vApps.vTask.vpwTask import *

import thread,time,fnmatch,sys,types

import images

VERBOSE=0
FAULTY_PRJ=0x1
FAULTY_TSK=0x2
FAULTY_LOC=0x4
FAULTY_USR=0x8


class thdModify(thdNetModifyThread):
    def __getApplyItem__(self):
        try:
            idx=self.dApply['list'][self.dApply['iAct']]
            return self.dApply['found_list'][idx]
        except Exception,list:
            if self.verbose:
                vtLog.vtLogCallDepth(self,'exception:%s'%list)
            return (None,0)
    def __getApplyNode__(self):
        tup=self.__getApplyItem__()
        return tup[0]
    def __apply__(self):
        node,iRes=self.__getApplyItem__()
        
        id=self.doc.getKey(node)
        if self.verbose:
            vtLog.vtLogCallDepth(self,'id:%s'%id)
        if self.dApply.has_key('prj'):
            tup=self.dApply['prj']
            n=self.doc.getChild(node,'project')
            if n is not None:
                self.doc.setText(n,tup[1])
                self.doc.setAttribute(n,'fid',tup[2])
        if self.dApply.has_key('task'):
            tup=self.dApply['task']
            n=self.doc.getChild(node,'task')
            if n is not None:
                self.doc.setText(n,tup[1])
                self.doc.setAttribute(n,'fid',tup[2])
        if self.dApply.has_key('loc'):
            tup=self.dApply['loc']
            n=self.doc.getChild(node,'loc')
            if n is not None:
                self.doc.setText(n,tup[1])
                self.doc.setAttribute(n,'fid',tup[2])
        if self.dApply.has_key('usr'):
            tup=self.dApply['usr']
            n=self.doc.getChild(node,'person')
            if n is not None:
                self.doc.setText(n,tup[1])
                self.doc.setAttribute(n,'fid',tup[2])
        #if self.verbose:
        #    vtLog.vtLogCallDepth(self,'node:%s'%node)
        self.doc.doEdit(node)
        pass
    
    
[wxID_VMODIFYPANEL, wxID_VMODIFYPANELCBAPPLY, wxID_VMODIFYPANELCHCLOC, 
 wxID_VMODIFYPANELCHCPRJ, wxID_VMODIFYPANELCHCUSR, wxID_VMODIFYPANELGPROCESS, 
 wxID_VMODIFYPANELLBLLOC, wxID_VMODIFYPANELLBLPRJ, wxID_VMODIFYPANELLBLTASK, 
 wxID_VMODIFYPANELLBLUSR, wxID_VMODIFYPANELLSTRES, 
] = [wx.NewId() for _init_ctrls in range(11)]

class vModifyPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VMODIFYPANEL, name=u'vModifyPanel',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(613, 324),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(605, 290))
        self.SetToolTipString(u'vModifyPanel')

        self.lstRes = wx.ListCtrl(id=wxID_VMODIFYPANELLSTRES, name=u'lstRes',
              parent=self, pos=wx.Point(8, 8), size=wx.Size(440, 256),
              style=wx.LC_REPORT)

        self.chcPrj = wx.Choice(choices=[], id=wxID_VMODIFYPANELCHCPRJ,
              name=u'chcPrj', parent=self, pos=wx.Point(456, 24),
              size=wx.Size(130, 21), style=0)
        self.chcPrj.Bind(wx.EVT_CHOICE, self.OnChcPrjChoice,
              id=wxID_VMODIFYPANELCHCPRJ)

        self.chcLoc = wx.Choice(choices=[], id=wxID_VMODIFYPANELCHCLOC,
              name=u'chcLoc', parent=self, pos=wx.Point(456, 112),
              size=wx.Size(130, 21), style=0)
        self.chcLoc.Bind(wx.EVT_CHOICE, self.OnChcLocChoice,
              id=wxID_VMODIFYPANELCHCLOC)

        self.chcUsr = wx.Choice(choices=[], id=wxID_VMODIFYPANELCHCUSR,
              name=u'chcUsr', parent=self, pos=wx.Point(456, 152),
              size=wx.Size(130, 21), style=0)
        self.chcUsr.Bind(wx.EVT_CHOICE, self.OnChcUsrChoice,
              id=wxID_VMODIFYPANELCHCUSR)

        self.lblPrj = wx.StaticText(id=wxID_VMODIFYPANELLBLPRJ,
              label=u'project', name=u'lblPrj', parent=self, pos=wx.Point(456,
              8), size=wx.Size(32, 13), style=0)

        self.lblTask = wx.StaticText(id=wxID_VMODIFYPANELLBLTASK, label=u'task',
              name=u'lblTask', parent=self, pos=wx.Point(456, 48),
              size=wx.Size(20, 13), style=0)

        self.lblLoc = wx.StaticText(id=wxID_VMODIFYPANELLBLLOC,
              label=u'location', name=u'lblLoc', parent=self, pos=wx.Point(456,
              96), size=wx.Size(37, 13), style=0)

        self.lblUsr = wx.StaticText(id=wxID_VMODIFYPANELLBLUSR, label=u'person',
              name=u'lblUsr', parent=self, pos=wx.Point(456, 136),
              size=wx.Size(32, 13), style=0)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VMODIFYPANELCBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Apply', name=u'cbApply',
              parent=self, pos=wx.Point(480, 232), size=wx.Size(76, 30),
              style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VMODIFYPANELCBAPPLY)

        self.gProcess = wx.Gauge(id=wxID_VMODIFYPANELGPROCESS, name=u'gProcess',
              parent=self, pos=wx.Point(8, 268), range=100, size=wx.Size(440,
              16), style=wx.GA_HORIZONTAL | wx.GA_SMOOTH)

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        self.verbose=VERBOSE
        
        self.thdModify=thdModify(self)
        EVT_NET_MODIFY_THREAD_ELEMENTS_ABORTED(self,self.OnAnalyseAbort)
        EVT_NET_MODIFY_THREAD_ELEMENTS_FINISHED(self,self.OnAnalyseFin)
        EVT_NET_MODIFY_THREAD_ELEMENTS(self,self.OnAnalyseProgress)
        
        self.doc=None
        self.netHum=None
        self.netLoc=None
        self.netTask=None
        self.netPrj=None

        id=wx.NewId()
        self.vpwTask=vpwTask(self,id,
                pos=(456, 62),size=(130,22),style=0,name="vpwTask")
        EVT_TASK_BUILD(self.vpwTask,self.OnTaskChanged)
        
        self.lstRes.InsertColumn(0,u'Y',wx.LIST_FORMAT_RIGHT,40)
        self.lstRes.InsertColumn(1,u'M',wx.LIST_FORMAT_RIGHT,30)
        self.lstRes.InsertColumn(2,u'D',wx.LIST_FORMAT_RIGHT,30)
        self.lstRes.InsertColumn(3,u'start',wx.LIST_FORMAT_RIGHT,60)
        self.lstRes.InsertColumn(4,u'end',wx.LIST_FORMAT_RIGHT,60)
        self.lstRes.InsertColumn(5,u'prj',wx.LIST_FORMAT_LEFT,60)
        self.lstRes.InsertColumn(6,u'task',wx.LIST_FORMAT_LEFT,85)
        self.lstRes.InsertColumn(7,u'client',wx.LIST_FORMAT_LEFT,85)
        self.lstRes.InsertColumn(8,u'loc',wx.LIST_FORMAT_LEFT,85)
        
        self.cbApply.SetBitmapLabel(images.getOkBitmap())
        
        self.Move(pos)
        
        self.lstFound=[]
    def Clear(self):
        self.chcPrj.SetSelection(-1)
        self.vpwTask.ClearSel()
        self.chcLoc.SetSelection(-1)
        self.chcUsr.SetSelection(-1)
        self.lstRes.DeleteAllItems()
        self.lstFound=[]
    def SetFound(self,lst):
        self.lstFound=lst
    def OnAnalyseProgress(self,evt):
        if evt.GetCount()>0:
            self.gProcess.SetRange(evt.GetCount())
        #else:
        #    self.gProcess.SetRange(self.iSize)
        self.gProcess.SetValue(evt.GetValue())
    def OnAnalyseFin(self,evt):
        #self.txtNodeCount.SetValue(str(self.thdAnalyse.GetCount()))
        #self.txtCount.SetValue(str(self.thdAnalyse.GetFound()))
        #self.cbFind.Enable(True)
        pass
    def OnAnalyseAbort(self,evt):
        self.gProcess.SetValue(0)
        sMsg=u'Modification aborted due timeout!'
        dlg=wx.MessageDialog(self,sMsg ,
                    u'vModifyPanel',
                    wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
        dlg.ShowModal()
        dlg.Destroy()
        pass
    def SetDocs(self,doc,netHum,netLoc,netPrj,netTask,bNet=False):
        self.doc=doc
        self.netHum=netHum
        self.netLoc=netLoc
        self.netPrj=netPrj
        self.netTask=netTask
        self.vpwTask.SetDoc(netTask,bNet)
        if bNet==True:
            EVT_NET_XML_GOT_CONTENT(doc,self.OnGotContent)
            EVT_NET_XML_CONTENT_CHANGED(netHum,self.OnHumanChanged)
            EVT_NET_XML_CONTENT_CHANGED(netLoc,self.OnLocChanged)
            EVT_NET_XML_CONTENT_CHANGED(netPrj,self.OnPrjChanged)
            EVT_NET_XML_GOT_CONTENT(doc,self.OnTaskChanged)
            EVT_NET_XML_LOCK(doc,self.thdModify.OnLock)
            
        self.__prjChanged__()
        self.__locChanged__()
        self.__humanChanged__()
    def OnGotContent(self,evt):
        
        evt.Skip()
    def OnHumanChanged(self,evt):
        self.__humanChanged__()
        evt.Skip()
    def OnLocChanged(self,evt):
        self.__locChanged__()
        evt.Skip()
    def OnPrjChanged(self,evt):
        self.__prjChanged__()
        evt.Skip()

    def __humanChanged__(self):
        self.chcUsr.Clear()
        if self.netHum is None:
            return
        usrs=self.netHum.GetSortedUsrIds()
        
        keys=usrs.GetKeys()
        for k in keys:
            self.chcUsr.Append(k)
    def __locChanged__(self):
        self.chcLoc.Clear()
        if self.netLoc is None:
            return
        ids=self.netLoc.GetSortedIds()
        for k in ids.GetKeys():
            self.chcLoc.Append(k)
    def __prjChanged__(self):
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        self.chcPrj.Clear()
        prjs=self.netPrj.GetSortedIds()
        for k in prjs.GetKeys():
            self.chcPrj.Append(k)
    def OnTaskChanged(self,evt):
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        evt.Skip()
    def OnTaskBuild(self,evt):
        evt.Skip()
    def __updateTask__(self):
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        idx=self.chcPrj.GetSelection()
        ids=self.netPrj.GetSortedIds()
        if (idx<0) or (ids is None):
            return
        ids=self.netPrj.GetSortedIds()
        prjNode,sPrj,iPrjId=ids.GetInfoByIdx(idx)
        self.vpwTask.SetPrj(str(iPrjId))
    def __setSelect__(self,ids,sName,sId,chcObj):
        if ids is None:
            chcObj.SetSelection(-1)
            return
        if len(sId)>0:
            idx=ids.GetIdxById(sId)
        else:
            idx=ids.GetIdxByName(sName)
        chcObj.SetSelection(idx)
    def OnCbGenButton(self, event):
        self.lstSel=[]
        d={}
        d['hum']=self.netHum
        d['loc']=self.netLoc
        d['prj']=self.netPrj
        d['task']=self.netTask
        d['timer']=self.doc
        
        self.chcPrj.SetSelection(-1)
        self.vpwTask.ClearSel()
        self.chcLoc.SetSelection(-1)
        self.chcUsr.SetSelection(-1)
        
        self.thdConsistance.Find(d)
        event.Skip()

    def OnCbFindButton(self, event):
        event.Skip()
    def getGrp(self):
        grp=[]
        for i in range(self.lstGroup.GetCount()):
            if self.lstGroup.IsChecked(i):
                grp.append(i)
        return grp
    #def getGrp(self):
    #    return [7,5]

    def OnChcPrjChoice(self, event):
        self.__updateTask__()
        event.Skip()

    def OnChcLocChoice(self, event):
        event.Skip()

    def OnChcUsrChoice(self, event):
        event.Skip()

    def OnCbApplyButton(self, event):
        if self.thdModify.IsRunning():
            return
        i=0
        self.dApply={}
        idx=self.chcPrj.GetSelection()
        if idx>=0:
            ids=self.netPrj.GetSortedIds()
            self.dApply['prj']=ids.GetInfoByIdx(idx)
        
        idx=self.chcUsr.GetSelection()
        if idx>=0:
            ids=self.netHum.GetSortedUsrIds()
            self.dApply['usr']=ids.GetInfoByIdx(idx)
        
        idx=self.chcLoc.GetSelection()
        if idx>=0:
            ids=self.netLoc.GetSortedIds()
            self.dApply['loc']=ids.GetInfoByIdx(idx)
        
        node=self.vpwTask.GetSelNode()
        if node is not None:
            taskId=self.vpwTask.GetSelID()
            sTask=self.vpwTask.GetValue()
            self.dApply['task']=(node,sTask,taskId)
        if self.verbose:
            for k in self.dApply.keys():
                v=self.dApply[k]
                vtLog.vtLogCallDepth(self,'%s  :%s %s'%(k,v[1],v[2]))
        self.dApply['iAct']=0
        iCount=self.lstRes.GetItemCount()
        lst=[]
        while i<iCount:
            if self.lstRes.GetItemState(i,wx.LIST_STATE_SELECTED)!=0:
                iNum=self.lstRes.GetItemData(i)
                #tup=self.thdConsistance.GetFoundTup(iNum)
                lst.append(iNum)
            i+=1
        self.dApply['iCount']=len(lst)
        self.dApply['list']=lst
        self.dApply['found_list']=self.lstFound
        self.thdModify.Apply(self.doc,self.dApply,True)
        
        event.Skip()
