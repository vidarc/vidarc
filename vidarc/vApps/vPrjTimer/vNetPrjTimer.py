#----------------------------------------------------------------------------
# Name:         vNetPrjTimer.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vNetPrjTimer.py,v 1.11 2008/02/02 16:10:54 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog

from vidarc.vApps.vPrjTimer.vXmlPrjTimer import vXmlPrjTimer
from vidarc.tool.net.vNetXmlWxGui import *

class vNetPrjTimer(vXmlPrjTimer,vNetXmlWxGui):
    def __init__(self,parent,appl,fn='tmpl.xml',attr='id',skip=[],
                pos=(0,0),size=(16,12),synch=False,verbose=0,audit_trail=False):
        vNetXmlWxGui.__init__(self,parent,appl,pos,size,synch=synch,verbose=verbose)
        vXmlPrjTimer.__init__(self,appl=appl,attr=attr,skip=skip,synch=synch,verbose=verbose,
                            audit_trail=audit_trail)
        vXmlPrjTimer.SetFN(self,fn)
    #def Stop(self):
    #    vNetXmlWxGui.Stop(self)
    #    vXmlPrjTimer.Stop(self)
    #def IsRunning(self):
    #    if vNetXmlWxGui.IsRunning(self):
    #        return True
    #    if vXmlPrjTimer.IsRunning(self):
    #        return True
    #    return False
    def New(self,root=None,bAcquire=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        vNetXmlWxGui.New(self)
        vXmlPrjTimer.New(self,root=root,
                bConnection=self.IsFlag(self.CONN_ACTIVE),
                bAcquire=bAcquire)
    def GetFN(self):
        #if vtLog.vtLngIsLogged(vtLog.INFO):
        #    vtLog.vtLngCur(vtLog.INFO,'GetFN',self.appl)
        sFN=vNetXmlWxGui.GetFN(self)
        if sFN is not None:
            return sFN
        else:
            return vXmlPrjTimer.GetFN(self)
    def Open(self,fn=None,silent=False,bThread=True):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'Open',self.appl)
        if fn is None:
            sFN=vNetXmlWxGui.GetFN(self)
            if sFN is not None:
                fn=sFN
        if fn is None:
            vXmlPrjTimer.New(self)
            return -2
        if self.verbose:
            vtLog.vtLogCallDepth(self,'fn:%s'%(fn))
        #vNetXmlWxGui.OpenApplDocs(self)
        if bThread:
            self.silent=silent
            iRet=vXmlPrjTimer.Open(self,fn,True)
        else:
            iRet=vXmlPrjTimer.Open(self,fn,False)
            if iRet>=0:
                if silent==False:
                    self.NotifyContent()
            else:
                self.New()
        return iRet
    def Save(self,fn=None,encoding='ISO-8859-1'):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',self.appl)
        sFN=vNetXmlWxGui.GetFN(self)
        if sFN is not None:
            fn=sFN
        vNetXmlWxGui.SaveApplDocs(self,encoding=encoding)
        return vXmlPrjTimer.Save(self,fn,encoding)
    def getSetupChildByLst(self,node,lst):
        if lst is None:
            lst=self.lstSetupNode
        return self.docSetup.getChildByLst(node,lst)
    def getChildByLst(self,node,lst):
        if lst is None:
            return None
        return vXmlPrjTimer.getChildByLst(self,node,lst)
    def addNode(self,par,node,func=None,**kwargs):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'addNode;id:%s'%self.getKey(node),self.appl)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'%s'%`node`,self.appl)
        vXmlPrjTimer.addNode(self,par,node)
        vNetXmlWxGui.addNode(self,par,node,func,**kwargs)
    def moveNode(self,nodePar,node):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'idPar:%s;id:%s'%(self.getKey(nodePar),self.getKey(node)),self.appl)
        #if vtLog.vtLngIsLogged(vtLog.DEBUG):
        #    vtLog.vtLngCallStack(None,vtLog.DEBUG,'%s'%node,origin=self.appl)
        vNetXmlWxGui.moveNode(self,nodePar,node)
        vXmlPrjTimer.moveNode(self,nodePar,node)
    def delNode(self,node,nodePar=None):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'id:%s'%self.getKey(node),self.appl)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'%s'%`node`,self.appl)
        vNetXmlWxGui.delNode(self,node,nodePar=nodePar)
        vXmlPrjTimer.deleteNode(self,node,nodePar=nodePar)
    def IsRunning(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',self.appl)
        if vNetXmlWxGui.__isRunning__(self):
            return True
        if vXmlPrjTimer.__isRunning__(self):
            return True
        return False
    def __stop__(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',self.appl)
        vNetXmlWxGui.__stop__(self)
        vXmlPrjTimer.__stop__(self)
    def NotifyContent(self):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',self.appl)
        #self.GenerateSortedIds()
        vNetXmlWxGui.NotifyContent(self)
    def NotifyGetNode(self,id):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',self.appl)
        #self.GenerateSortedIds()
        vNetXmlWxGui.NotifyGetNode(self,id)
    def NotifyAddNode(self,idPar,id,content):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',self.appl)
        #self.GenerateSortedIds()
        vNetXmlWxGui.NotifyAddNode(self,idPar,id,content)
    def NotifyRemoveNode(self,id):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',self.appl)
        #self.GenerateSortedIds()
        vNetXmlWxGui.NotifyRemoveNode(self,id)
    def GenerateSortedIds(self):
        vtLog.vtLngCur(vtLog.CRITICAL,'',self.appl)
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',self.appl)
        self.docs=self.getSortedNodeIds(['users'],
                        'docgroup','id',[('key','%s'),('title','%s')])
    def GetSortedIds(self):
        vtLog.vtLngCur(vtLog.CRITICAL,'',self.appl)
        try:
            return self.docs
        except:
            self.GenerateSortedIds()
            return self.docs
    def NotifyContentOld(self):
        vNetXmlWxGui.NotifyContent(self)
        oSMlinker=self.GetRegisteredNode('stateMachineLinker')
        tmp=self.getChildByLst(None,['cfg','stateMachineLinker'])
        oSMlinker.SetNode(tmp)
        #oSMlinker.BuildRunners(tmp)
    def IsLoggedInSelfOld(self,node,usrId):
        vtLog.vtLngCurWX(vtLog.DEBUG,'node:%s'%(self.getKey(node)),self)
        try:
            if node is not None:
                oEmployee=self.GetReg('employee')
                if self.getTagName(node)=='employee':
                    tmp=node
                elif self.getTagName(node)=='prjtime':
                    tmp=self.getParent(node)
                else:
                    return False
                if self.IsConnActive():
                    iUsrId=oEmployee.GetEmployeeID(tmp)
                    if self.GetLoggedInUsrId()==iUsrId:
                        return True
                else:
                    return True
                #if self.getKeyNum(tmp)==iUsrId:
                #    return True
        except:
            pass
        return False
