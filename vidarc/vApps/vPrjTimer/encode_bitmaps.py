#!/usr/bin/env python
#----------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    #"   -u -i -n Application Tools01_16.png images.py",
#    "   -u -i -n Mondrian mondrian.ico images.py",
    #"-u -i -n Ok img/ok_2.png images.py",
    #"-a -u -n Cancel img/abort_2.png images.py",
    
    "-u -i -n Element img/Box01_16.png images.py",
    "-a -u -n ElementSel img/Box02_16.png images.py",
    "-a -u -n Text img/Note01_16.png images.py",
    "-a -u -n TextSel img/Note01_16.png images.py",
    "-a -u -n PrjTimes img/vidarcAppIconPrjTimer_16.png images.py",
    "-a -u -n PrjTimesSel img/vidarcAppIconPrjTimer_16.png images.py",
    "-a -u -n Prj img/Prj03_16.png images.py",
    "-a -u -n PrjSel img/Prj02_16.png images.py",
    "-a -u -n Prjs img/Prjs01_16.png images.py",
    "-a -u -n PrjsSel img/Prjs01_16.png images.py",
    "-a -u -n Year img/Year03_16.png images.py",
    "-a -u -n Month img/Month03_16.png images.py",
    "-a -u -n Day img/Day02_16.png images.py",
    "-a -u -n User img/Usr01_16.png images.py",
    "-a -u -n UserRecent img/Usr03_16.png images.py",
    "-a -u -n TimeEntry img/TimeEntry01_16.png images.py",
    "-a -u -n Timer img/Timer02_16.png images.py",
    "-a -u -n Note img/Note04_16.png images.py",
    #"-a -u -n UserSel Usr01_16.png hum_tree_images.py",
    #"-a -u -n Human Human01_16.png hum_tree_images.py",
    #"-a -u -n HumanSel Human01_16.png hum_tree_images.py",
    "-a -u -n Build img/Build02_16.png images.py",
    "-a -u -n ReportBuild img/Build01_16.png images.py",
    "-a -u -n Add img/add.png images.py",
    "-a -u -n Del img/waste.png images.py",
    "-a -u -n Apply img/checkmrk.png images.py",
    #"-a -u -n Open img/open.png images.py",
    "-a -u -n Ok img/ok.png images.py",
    "-a -u -n Cancel img/abort.png images.py",
    "-a -u -n Browse img/BrowseFile01_16.png images.py",
    "-a -u -n Application img/VidMod_PrjTimer_16.ico images.py",
    "-a -u -n Plugin img/VidMod_PrjTimer_16.png images.py",
    
    "-u -i -n Find data/Search02_16.png images_data.py",
    "-a -u -n Group data/DocGrp01_16.png images_data.py",
    "-a -u -n Report data/Build01_16.png images_data.py",
    "-a -u -n Summary data/Measure01_16.png images_data.py",
    "-a -u -n ReportBuild data/Build01_16.png images_data.py",
    
    "-u -i -n Apply img2/ok.png images2.py",
    "-a -u -n Cancel img2/abort.png images2.py",
    "-a -u -n Browse img2/BrowseFile01_16.png images2.py",
    
    "-u -i -n Splash img/splashPrjTimer01.png images_splash.py",
    
    "-a -i -n NoIcon  img/noicon.png  images.py"
    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)

