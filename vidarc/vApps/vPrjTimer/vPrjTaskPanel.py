#Boa:FramePanel:vPrjTaskPanel
#----------------------------------------------------------------------------
# Name:         vPrjTaskPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vPrjTaskPanel.py,v 1.2 2006/01/17 12:17:47 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import sys
import wx
import wx.lib.buttons
import cStringIO

[wxID_VPRJTASKPANEL, wxID_VPRJTASKPANELGCBBADD, wxID_VPRJTASKPANELGCBBADDSUB, 
 wxID_VPRJTASKPANELGCBBDEL, wxID_VPRJTASKPANELGCBBDELSUB, 
 wxID_VPRJTASKPANELLBLSUBTASK, wxID_VPRJTASKPANELLBLTASK, 
 wxID_VPRJTASKPANELLSTSUBTASK, wxID_VPRJTASKPANELLSTTASK, 
 wxID_VPRJTASKPANELTXTSUBTASK, wxID_VPRJTASKPANELTXTTASK, 
] = [wx.NewId() for _init_ctrls in range(11)]


#----------------------------------------------------------------------
def getAddData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x00\x87IDAT(\x91\xa5RA\x0e\x80 \x0c+\x84\xcf!\xcf \xbc\x8a\xec\x19\xe8\xf3\
\xf4\xb0\x04\xe6D\x84\xd8\x13,\xdd\xdae5\xc0\t\x81\xcd\x07\xdc\xb1\x1fE~\x9d\
\xa2\xc6\x14\xa1\x11d\x9ba\x85\xcd\x87\x1e\xb5\x812q\x8f\xed\xb2)\x13e\x92\
\x95\x98"[\xb0\x83\xa9]\xb8O3R\x04\x08\xeb\n\xf5\xa5L\xabJu\xf1CAn\xc2\xb3\
\xbb\xbb-(\xf0)\xec~\x94\xa7\xfb\x01f\x15\xea\xa5\xa7\xa2Q\xd9\xad\x01/\xe1c\
\xb72\xb0f\x1co\x95m\x00\x17\xd7\x95A\xb5\xe1.\xb5f\x00\x00\x00\x00IEND\xaeB\
`\x82' 

def getAddBitmap():
    return wx.BitmapFromImage(getAddImage())

def getAddImage():
    stream = cStringIO.StringIO(getAddData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getDelData():
    return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x00\xe2IDAT(\x91\x85\x92Ar\x830\x0cE\x9f\x19.\x95N\xbb\x08\xcb\xe6F\xa0\xb2\
pEo\x94.\xc9\x82\x8es,u\xa1D1eH\xff0\x1a\xc9\xfa_\xdf6N`\xdc\xa1y\x8a\\\xc6\
\x81\x1d\x18\x98f\xd5\xacf\xe6\xd1\x13\xcd\xea\xdd\xd5\x17T'\x01e)e){\xb2\
\x16\x18>\x06`\xfa\xbc\xedg\xbe\xcc\xdd\xb1\xbb\xfe\\\xa3\x05Sl2i\xd6\xee\
\xd8\x01\x87\xd7\x03\x90Rr\x1f_tD\xeb\xe6\xe0\xf0\x91fV\xbb\x05;VZ\x19\x07\
\xcdS\xcc;\xbd\x9f\x80\xf3\xf79\x04\xb5\x18h\xebB\xbfTz\t\x99\xf42_\xe6?w\
\xda\xd4\xec\x88[\xc8(\xfe\xc7\x1e\x02\x9f\x1d\xd1\x93-\x1a@\xc6\xa1\xb6~n\
\xd5\xd4\x85\xf4\xf2\x9c\r\xa4\xfb[J@Y\n\xd5\xd1\x01w\x8e\x03P\xdd\x92\x01/o\
\x0f\xd9\x1e\xdau\xf9\xbf,\xd5\xcf{\xd3Z\rr\xfc\x02\x1fr\x8f\xf6KRW\xed\x00\
\x00\x00\x00IEND\xaeB`\x82" 

def getDelBitmap():
    return wx.BitmapFromImage(getDelImage())

def getDelImage():
    stream = cStringIO.StringIO(getDelData())
    return wx.ImageFromStream(stream)


class vPrjTaskPanel(wx.Panel):
    def _init_coll_lstTask_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT, heading=u'Tasks',
              width=180)

    def _init_coll_lstSubTask_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading=u'Sub Task', width=180)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VPRJTASKPANEL, name=u'vPrjTaskPanel',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(407, 234),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(399, 207))

        self.lblTask = wx.StaticText(id=wxID_VPRJTASKPANELLBLTASK,
              label=u'Task', name=u'lblTask', parent=self, pos=wx.Point(8, 8),
              size=wx.Size(24, 13), style=0)

        self.txtTask = wx.TextCtrl(id=wxID_VPRJTASKPANELTXTTASK,
              name=u'txtTask', parent=self, pos=wx.Point(40, 8),
              size=wx.Size(152, 21), style=0, value=u'')

        self.lblSubTask = wx.StaticText(id=wxID_VPRJTASKPANELLBLSUBTASK,
              label=u'Sub Task', name=u'lblSubTask', parent=self,
              pos=wx.Point(208, 8), size=wx.Size(46, 13), style=0)

        self.txtSubTask = wx.TextCtrl(id=wxID_VPRJTASKPANELTXTSUBTASK,
              name=u'txtSubTask', parent=self, pos=wx.Point(264, 8),
              size=wx.Size(120, 21), style=0, value=u'')

        self.gcbbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJTASKPANELGCBBADD,
              bitmap=wx.EmptyBitmap(16,16), label=u'Add', name=u'gcbbAdd', parent=self,
              pos=wx.Point(16, 40), size=wx.Size(76, 30), style=0)
        self.gcbbAdd.Bind(wx.EVT_BUTTON, self.OnGcbbAddButton,
              id=wxID_VPRJTASKPANELGCBBADD)

        self.gcbbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJTASKPANELGCBBDEL,
              bitmap=wx.EmptyBitmap(16,16), label=u'Del', name=u'gcbbDel', parent=self,
              pos=wx.Point(112, 40), size=wx.Size(76, 30), style=0)
        self.gcbbDel.Bind(wx.EVT_BUTTON, self.OnGcbbDelButton,
              id=wxID_VPRJTASKPANELGCBBDEL)

        self.gcbbAddSub = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJTASKPANELGCBBADDSUB,
              bitmap=wx.EmptyBitmap(16,16), label=u'Add', name=u'gcbbAddSub',
              parent=self, pos=wx.Point(216, 40), size=wx.Size(76, 30),
              style=0)
        self.gcbbAddSub.Bind(wx.EVT_BUTTON, self.OnGcbbAddSubButton,
              id=wxID_VPRJTASKPANELGCBBADDSUB)

        self.gcbbDelSub = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJTASKPANELGCBBDELSUB,
              bitmap=wx.EmptyBitmap(16,16), label=u'Del', name=u'gcbbDelSub',
              parent=self, pos=wx.Point(300, 40), size=wx.Size(76, 30),
              style=0)
        self.gcbbDelSub.Bind(wx.EVT_BUTTON, self.OnGcbbDelSubButton,
              id=wxID_VPRJTASKPANELGCBBDELSUB)

        self.lstTask = wx.ListCtrl(id=wxID_VPRJTASKPANELLSTTASK,
              name=u'lstTask', parent=self, pos=wx.Point(8, 80),
              size=wx.Size(184, 120),
              style=wx.LC_REPORT | wx.LC_SORT_ASCENDING)
        self.lstTask.SetToolTipString(u'Tasks')
        self._init_coll_lstTask_Columns(self.lstTask)
        self.lstTask.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstTaskListItemSelected, id=wxID_VPRJTASKPANELLSTTASK)

        self.lstSubTask = wx.ListCtrl(id=wxID_VPRJTASKPANELLSTSUBTASK,
              name=u'lstSubTask', parent=self, pos=wx.Point(208, 80),
              size=wx.Size(184, 120),
              style=wx.LC_REPORT | wx.LC_SORT_ASCENDING)
        self.lstSubTask.SetToolTipString(u'Tasks')
        self._init_coll_lstSubTask_Columns(self.lstSubTask)
        self.lstSubTask.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstSubTaskListItemSelected,
              id=wxID_VPRJTASKPANELLSTSUBTASK)

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        
        self.selectedTask=None
        self.selectedSubTask=None

        img=getAddBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.gcbbAdd.SetBitmapLabel(img)
        self.gcbbAddSub.SetBitmapLabel(img)
        
        img=getDelBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.gcbbDel.SetBitmapLabel(img)
        self.gcbbDelSub.SetBitmapLabel(img)

    def SetXmlTask(self,node):
        self.lstTask.DeleteAllItems()
        if node is None:
            return
        n=node.getElementsByTagName('task')
        if len(n)>0:
            for o in n:
                s=''
                for cn in o.childNodes:
                    if cn.nodeType==Element.TEXT_NODE:
                        s=s+cn.nodeValue
                self.lstTask.InsertStringItem(sys.maxint,s)
            pass
    def GetXmlTask(self,node):
        if node is None:
            return
        if node.hasChildNodes():
            try:
                for i in range(0,len(node.childNodes)):
                    cn=node.childNodes[0]
                    node.removeChild(cn)
                    cn.unlink()
            except:
                pass
        doc=vtXmlDomTree.__getDocument__(node)
        iLen=self.lstTask.GetItemCount()
        for i in range(0,iLen):
            s=self.lstTask.GetItemText(i)
            elem=doc.createElement('task')
            elem.appendChild(doc.createTextNode(s))
            node.appendChild(elem)
        pass
    def SetXmlSubTask(self,node):
        self.lstSubTask.DeleteAllItems()
        if node is None:
            return
        n=node.getElementsByTagName('subtask')
        if len(n)>0:
            for o in n:
                s=''
                for cn in o.childNodes:
                    if cn.nodeType==Element.TEXT_NODE:
                        s=s+cn.nodeValue
                self.lstSubTask.InsertStringItem(sys.maxint,s)
            pass
    def GetXmlSubTask(self,node):
        if node is None:
            return
        if node.hasChildNodes():
            try:
                for i in range(0,len(node.childNodes)):
                    cn=node.childNodes[0]
                    node.removeChild(cn)
                    cn.unlink()
            except:
                pass
        doc=vtXmlDomTree.__getDocument__(node)
        iLen=self.lstSubTask.GetItemCount()
        for i in range(0,iLen):
            s=self.lstSubTask.GetItemText(i)
            elem=doc.createElement('subtask')
            elem.appendChild(doc.createTextNode(s))
            node.appendChild(elem)
    def OnGcbbAddButton(self, event):
        s=self.txtTask.GetValue()
        if len(s)>0:
            if self.lstTask.FindItem(-1,s)<0:
                self.lstTask.InsertStringItem(sys.maxint,s)
            self.txtTask.SetValue('')
        event.Skip()

    def OnGcbbDelButton(self, event):
        if self.selectedTask is not None:
            self.lstTask.DeleteItem(self.selectedTask)
        else:
            self.selectedTask=None
        event.Skip()

    def OnGcbbAddSubButton(self, event):
        s=self.txtSubTask.GetValue()
        if len(s)>0:
            if self.lstSubTask.FindItem(-1,s)<0:
                self.lstSubTask.InsertStringItem(sys.maxint,s)
            self.txtSubTask.SetValue('')
        event.Skip()

    def OnGcbbDelSubButton(self, event):
        if self.selectedSubTask is not None:
            self.lstSubTask.DeleteItem(self.selectedSubTask)
        else:
            self.selectedSubTask=None
        event.Skip()

    def OnLstTaskListItemSelected(self, event):
        self.selectedTask=event.GetIndex()
        event.Skip()

    def OnLstSubTaskListItemSelected(self, event):
        self.selectedSubTask=event.GetIndex()
        event.Skip()
