#----------------------------------------------------------------------------
# Name:         vReportGroupingPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vReportGroupingPanel.py,v 1.1 2005/12/13 13:20:08 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import images_data

import vidarc.vApps.vPrjTimer.vFindPanel as vFindPanel
import vidarc.vApps.vPrjTimer.vGroupPanel as vGroupPanel
import vidarc.vApps.vPrjTimer.vSummaryPanel as vSummaryPanel
import vidarc.vApps.vPrjTimer.vReportBuildPanel as vReportBuildPanel
import vidarc.tool.time.vTimeLimit as vTimeLimit
import types
import vidarc.tool.log.vtLog as vtLog

VERBOSE=0
if VERBOSE>0:
    import pprint
    pp=pprint.PrettyPrinter(indent=2)

class vReportGroupingPanel(wx.Notebook):
    def _init_ctrls(self, prnt,id=wx.NewId(), pos=wx.DefaultPosition, size=wx.Size(100, 50), style=wx.TAB_TRAVERSAL, name=''):
        wx.Notebook.__init__(self, style=style, name=name, parent=prnt, pos=pos, id=id, size=size)

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent,id, pos, size, style, name)
        self.doc=None
        self.netHum=None
        self.netLoc=None
        self.netDoc=None
        self.netPrjDoc=None
        self.netPrj=None
        self.netTask=None
        
        self.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGED, self.OnPageChanged)
        self.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGING, self.OnPageChanging)
        
        self.verbose=VERBOSE
        
        edit=[u'Find',u'Group',u'Summary',u'ReportBuild']
        editName=[u'Find',u'Group',u'Summary',u'Build']
        il = wx.ImageList(16, 16)
        for e in edit:
            s='v%sPanel'%e
            f=getattr(images_data, 'get%sBitmap' % e)
            bmp = f()
            il.Add(bmp)
        self.AssignImageList(il)
        
        self.lstName=edit
        self.lstPanel=[]
        i=0
        for e in edit:
            s='v%sPanel'%e
            f=getattr(eval('v%sPanel'%e),'v%sPanel'%e)
            panel=f(self,wx.NewId(),
                    pos=(0,0),size=(190,340),style=0,name=s)
            self.AddPage(panel,editName[i],False,i)
            self.lstPanel.append(panel)
            i=i+1
        panel=self.lstPanel[0]
        #vtXmlFilter.EVT_THREAD_FILTER_ELEMENTS(panel,self.OnFilterElement)
        vFindPanel.vtXmlFilter.EVT_THREAD_FILTER_ELEMENTS_FINISHED(panel,self.OnFilterFinished)
        vGroupPanel.vtXmlGrpAdd2List.EVT_THREAD_GROUP_ADD2LIST_ELEMENTS_FINISHED(self.GetGroupEventPoster(),self.OnGroupAdd2ListFinished)
        
    def GetFindEventPoster(self):
        return self.lstPanel[0]
    def GetGroupEventPoster(self):
        return self.lstPanel[1]
    def GetGroupTreeEventPoster(self):
        return self.lstPanel[3]
    def GetLimit(self):
        def getLimit(tup):
            if self.verbose:
                vtLog.vtLogCallDepth(self,'tup:%s'%tup,callstack=False)
                #vtLog.vtLogCallDepth(self,'dLimits:%s'%(dLimits),callstack=False)
            limit=0.0
            for year in dLimits.keys():
                if tup[0]==-1:
                    pass
                else:
                    if year!=tup[0]:
                        continue
                dYear=dLimits[year]
                if tup[1]==-1 and tup[2]==-1 and tup[3]==-1:
                    limit+=dYear['sum']
                else:
                    if tup[3]==-1:
                        dMon=dYear['month']
                        for mon in dMon.keys():
                            if tup[1]==-1:
                                pass
                            else:
                                if mon!=tup[1]:
                                    continue
                            
                            if tup[2]==-1:
                                return dMon[mon]['sum']
                            else:
                                dDay=dMon[mon]['day']
                                for day in dDay.keys():
                                    if tup[2]==-1:
                                        pass
                                    else:
                                        if tup[2]==day:
                                            limit+=dDay[day]
                                
                    else:
                        dWeek=dYear['week']
                        for week in dWeek.keys():
                            if week==tup[3]:
                                return dWeek[week]
            return limit
        def calcDict(d,dd,level,prefix):
            if self.verbose:
                vtLog.vtLogCallDepth(self,'level:%d prefix:%s'%(level,prefix),callstack=False)
            keys=d.keys()
            #vtLog.CallStack('')
            #pp.pprint(d)
            #pp.pprint(dd)
            #pp.pprint(prefix)
            #print keys
            bHumans=False
            sumTotal=0.0
            idx=grpIdxs[level]
            if idx==0:
                # humans
                humIds=self.netHum.GetSortedUsrIds()
                #ids=humIds.GetIdByName(k)
                #for id in ids:
                #    tup=humIds.GetInfoById(id)
                #    dictTimes=self.netHum.GetDictTimes(tup[0])
                #    dictRanges=self.netHum.ConvertDictTimesToDayRanges(dictTimes)
                #    ranges=dictRanges['normal']
                #    dLimits=vTimeLimit.getNormalTimes(sStart,sEnd,ranges)
                bHumans=True
                pass
            for k in keys:
                v=d[k]
                if types.DictType==type(v):
                    dd[k]=[0.0,{},1]
                    sum=calcDict(v,dd[k][1],level+1,prefix+[k])
                    dd[k][0]=sum
                    #sumTotal+=sum
                    #else:
                    #    dd[k]=[0.0,{}]
                    #    sum=0
                    #print '              '[:level],prefix
                    tup=[-1,-1,-1,-1]  # year,mon,day.week
                    bValid=False
                    tmpPrefix=prefix+[k]
                    for i in range(level+1):
                        idx=grpIdxs[i]
                        #print '              '[:level+1],idx,grpIdxs
                        if idx in [1,2,3,4]:#<4:
                            tup[idx-1]=int(tmpPrefix[i])
                            bValid=True
                    if bValid:
                        sum=getLimit(tup)
                    else:
                        dSub=dd[k][1]
                        sum=0.0
                        for subk in dSub.keys():
                            sum+=dSub[subk][0]
                            if dSub[subk][2]==0:
                                break
                        dd[k][2]=0
                        #sum=0
                        
                    dd[k][0]=sum
                    #print '              '[:level+1],tup,sum        
                    
                else:
                    dd[k]=[0.0,{},1]
                    bValid=False
                    tup=[-1,-1,-1,-1]  # year,mon,day.week
                    tmpPrefix=prefix+[k]
                    for i in range(level+1):
                        idx=grpIdxs[i]
                        if idx in [1,2,3,4]:#<4:
                            tup[idx-1]=int(tmpPrefix[i])
                            bValid=True
                    if bValid:
                        sum=getLimit(tup)
                    else:
                        sum=getLimit(tup)
                        dd[k][2]=0
                        
                    dd[k][0]=sum
                    #sumTotal+=sum
                    pass
                        
            return sumTotal
        pnFind=self.__getPanel__(u'Find')
        sStart=pnFind.GetStartDate()
        sEnd=pnFind.GetEndDate()
        ranges=[('--------','--------',[8.5, 8.5, 8.5, 8.5, 8.5, 0.0 ,0.0])]
        dLimits=vTimeLimit.getNormalTimes(sStart,sEnd,ranges)
        pnGrp=self.__getPanel__(u'Group')
        if self.verbose:
            vtLog.vtLogCallDepth(self,'start:%s end:%s'%(sStart,sEnd))
            #vtLog.vtLogCallDepth(self,'grouped:%s'%(pnGrp.GetGrouped()),callstack=False)
            vtLog.vtLogCallDepth(self,'grouped info:%s'%(pnGrp.GetGroupingInfo()),callstack=False)
            vtLog.vtLogCallDepth(self,'grouped showing:%s'%(pnGrp.GetGroupShowing()),callstack=False)
            vtLog.vtLogCallDepth(self,'grouped sorting:%s'%(pnGrp.GetGroupSorting()),callstack=False)
            vtLog.vtLogCallDepth(self,'group names:%s'%(pnGrp.getGrpNames()),callstack=False)
            vtLog.vtLogCallDepth(self,'group names:%s'%(pnGrp.getGrpIdxs()),callstack=False)
        fSum=0.0
        grpNames=pnGrp.getGrpNames()
        grpIdxs=pnGrp.getGrpIdxs()
        
        grps=pnGrp.GetGrouped()
        dGrpLimits={}
        #vtLog.CallStack('')
        if grpNames[0]=='person':
            #process limits
            if self.netHum is None:
                return dGrpLimits
            humIds=self.netHum.GetSortedUsrIds()
            keys=grps.keys()
            keys.sort()
            #pp.pprint(grps)
            #pp.pprint(grpNames)
            #pp.pprint(grpIdxs)
            i=0
            for k in keys:
                ids=humIds.GetIdByName(k)
                for id in ids:
                    tup=humIds.GetInfoById(id)
                    dictTimes=self.netHum.GetDictTimes(tup[0])
                    dictRanges=self.netHum.ConvertDictTimesToDayRanges(dictTimes)
                    ranges=dictRanges['normal']
                    dLimits=vTimeLimit.getNormalTimes(sStart,sEnd,ranges)
                    #pp.pprint(ranges)
                    #pp.pprint(dLimits)
                    if self.verbose:
                        for year in dLimits.keys():
                            vtLog.vtLogCallDepth(self,'year:%s'%(year),callstack=False)
                            for week in dLimits[year]['week'].keys():
                                print '   week:%s %4.2f'%(week,dLimits[year]['week'][week])
                            for month in dLimits[year]['month'].keys():
                                print '   month:%s %4.2f'%(month,dLimits[year]['month'][month]['sum'])
                    dGrpLimits[k]=[0.0,{},0]
                    calcDict(grps[k],dGrpLimits[k][1],1,[k])
            for k in keys:
                d=dGrpLimits[k][1]
                bValid=dGrpLimits[k][2]
                fLimit=0.0
                for v in d.values():
                    fLimit+=v[0]
                    if bValid==0:
                        break
                dGrpLimits[k][0]=fLimit
            return dGrpLimits
        else:
            calcDict(grps,dGrpLimits,0,[])
        return dGrpLimits
    def Find(self):
        for i in range(1,4):
            self.lstPanel[i].Clear()
        self.lstPanel[0].Find()
    def SetDocs(self,doc,netHum,netLoc,netDoc,netPrjDoc,netPrj,netTask):
        self.doc=doc
        self.netHum=netHum
        self.netLoc=netLoc
        self.netDoc=netDoc
        self.netPrjDoc=netPrjDoc
        self.netPrj=netPrj
        self.netTask=netTask
        for i in range(0,len(self.lstName)):
            self.lstPanel[i].SetDocs(doc,netHum,netLoc,netDoc,netPrjDoc,netPrj,netTask)
    def UpdateInfos(self):
        for i in range(0,len(self.lstName)):
            self.lstPanel[i].UpdateInfos()
    def Clear(self):
        for i in range(0,len(self.lstName)):
            self.lstPanel[i].Clear()
    def OnPageChanged(self, event):
        old = event.GetOldSelection()
        sel = self.GetSelection()
        if sel>=len(self.lstPanel):
            return
        #try:
        #    self.nodeSet.index(sel)
        #except:
        #    self.lstPanel[sel].SetNode(self.lstName[sel],self.node)
        #    self.nodeSet.append(sel)
        event.Skip()

    def OnPageChanging(self, event):
        sel = self.GetSelection()
        event.Skip()
    def OnFilterElement(self,evt):
        #if self.verbose:
        #    vtLog.vtLogCallDepth(self,'')
        evt.Skip()
    def __getPanel__(self,name):
        try:
            idx=self.lstName.index(name)
            panel=self.lstPanel[idx]
            return panel
        except:
            return None
    def OnFilterFinished(self,evt):
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        pnFind=self.__getPanel__(u'Find')
        pnGrp=self.__getPanel__(u'Group')
        if pnFind is not None and pnGrp is not None:
            pnGrp.SetFound(pnFind.GetFound())
        evt.Skip()
    def OnGroupAdd2ListFinished(self,evt):
        limits=self.GetLimit()
        pnGrp=self.__getPanel__(u'Group')
        pnRep=self.__getPanel__(u'ReportBuild')
        pnRep.SetLimit(limits)
        pnRep.SetGrouped(pnGrp.GetGrouped(),
                    pnGrp.GetGroupingInfo(),
                    pnGrp.GetGroupShowing(),
                    pnGrp.GetGroupSorting(),
                    pnGrp.getGrpNames(),
                    pnGrp.getGrpIdxs())