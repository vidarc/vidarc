#!/usr/bin/env python
#Boa:PyApp:main
#----------------------------------------------------------------------------
# Name:         vPrjTimerAppl.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vPrjTimerAppl.py,v 1.6 2007/07/30 20:38:48 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os,sys,getopt,traceback
import wx
import vidarc.vApps.common.vSplashFrame as vSplashFrame
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase

import images_splash
modules ={u'vPrjTimerMainFrame': [0, '', u'vPrjTimerMainFrame.py']}

class BoaApp(wx.App):
    def __init__(self,num,sAppl,cfgFN,fn,iLogLv,iSockPort):
        self.fn=fn
        self.cfgFN=cfgFN
        self.iLogLv=iLogLv
        self.iSockPort=iSockPort
        self.sAppl=sAppl
        wx.App.__init__(self,num)
    def OnInit(self):
        wx.InitAllImageHandlers()
        actionList=[]
        actionListPost=[]
        d={'label':u'  import vPrjTimerMainFrame ...',
            'eval':'__import__("vidarc.vApps.vPrjTimer.vPrjTimerMainFrame")'}
        actionList.append(d)
        d={'label':u'  Create Project Timer ...',
            'eval':'self.res[0].vApps.vPrjTimer.vPrjTimerMainFrame'}
        actionList.append(d)
        d={'label':u'  Create Project Timer ...',
            'eval':'getattr(self.res[1],"create")'}
        actionList.append(d)
        d={'label':u'  Create Project Timer ...',
            'eval':'self.res[2](None)'}
        actionList.append(d)
        
        d={'label':u'  Finished.'}
        actionList.append(d)
        
        d={'label':u'  Open Config (%s)...'%self.cfgFN,
            'eval':'self.res[3].OpenCfgFile("%s")'%self.cfgFN}
        actionListPost.append(d)
        
        d={'label':u'  Open File (%s)...'%self.fn,
            'eval':'self.res[3].OpenFile("%s")'%self.fn}
        actionListPost.append(d)
        
        self.splash = vSplashFrame.create(None,'VIDARC','Project Timer',
            images_splash.getSplashBitmap(),
            actionList,
            actionListPost,
            self.sAppl,
            self.iLogLv,
            self.iSockPort)
        vSplashFrame.EVT_SPLASH_ACTION(self.splash,self.OnSplashAction)
        vSplashFrame.EVT_SPLASH_PROCESSED(self.splash,self.OnSplashProcessed)
        vSplashFrame.EVT_SPLASH_FINISHED(self.splash,self.OnSplashFinished)
        vSplashFrame.EVT_SPLASH_ABORTED(self.splash,self.OnSplashAborted)
        self.splash.Show()
        
        return True
    def OpenFile(self,fn):
        self.fn=fn
    def OpenCfgFile(self,fn):
        self.cfgFN=fn
    def OnSplashAction(self,evt):
        self.splash.DoProcess()
        evt.Skip()
    def OnSplashProcessed(self,evt):
        self.splash.DoAction()
        evt.Skip()
    def OnSplashFinished(self,evt):
        self.main=self.splash.res[3]
        self.SetTopWindow(self.main)
        self.main.Show()
        self.splash.Destroy()
        self.splash=None
        evt.Skip()
    def OnSplashAborted(self,evt):
        self.main=self.splash.res[3]
        self.main.Destroy()
        self.splash.Destroy()
        self.splash=None
        evt.Skip()
    #def OnAssert(self,fn,line,cond,msg):
    #    vtLog.vtLngCur(vtLog.DEBUG,
    #            'file:%s;line:%d;condition:%s;message:%s'%(fn,line,cond,msg),'Appl')
    def OnExceptionInMainLoop(self):
        vtLog.vtLngCur(vtLog.FATAL,'','Appl')
    def OnFatalException(self):
        vtLog.vtLngCur(vtLog.FATAL,'','Appl')
    def OnUnhandledException(self):
        vtLog.vtLngCur(vtLog.FATAL,'','Appl')
    
def showHelp():
    print _('help')
    print sys.version
    print "  ",_("valid flags:")
    print "  ",_("-c <filename>")
    print "  ",_("--config <filename>")
    print "        ",_("open the <filename> to configure application")
    print "  ",_("-f <filename>")
    print "  ",_("--file <filename>")
    print "        ",_("open the <filename> at start")
    print "  ","-h"
    print "  ",_("--help")
    print "        ",_("show this screen")
    print "  ","-l"
    print "  ",_("--lang <language id according ISO639>")
    print "  ",_("--log <level>")
    print "       ",_("0 = debug")
    print "       ",_("1 = information")
    print "       ",_("2 = waring")
    print "       ",_("3 = error")
    print "       ",_("4 = critical")
    print "       ",_("5 = fatal")
    
def main():
    optshort,optlong='l:f:c:h',['lang=','file=','config=','log=','help']
    vtLgBase.initAppl(optshort,optlong,'vPrjTimer')

    fn=None
    cfgFN='vPrjTimerCfg.xml'
    iLogLv=vtLog.ERROR
    try:
        optlist , args = getopt.getopt(sys.argv[1:],optshort,optlong)
        for o in optlist:
            if o[0] in ['-h','--help']:
                showHelp()
            
                return
            if o[0] in ['--file','-f']:
                fn=o[1]
            if o[0] in ['--config','-c']:
                cfgFN=o[1]
            if o[0] in ['--log']:
                if o[1]=='0':
                    iLogLv=vtLog.DEBUG
                elif o[1]=='1':
                    iLogLv=vtLog.INFO
                elif o[1]=='2':
                    iLogLv=vtLog.WARN
                elif o[1]=='3':
                    iLogLv=vtLog.ERROR
                elif o[1]=='4':
                    iLogLv=vtLog.CRITICAL
                elif o[1]=='5':
                    iLogLv=vtLog.FATAL
    except:
        showHelp()
    application = BoaApp(0,'vPrjTimer',cfgFN,fn,iLogLv,60007)
    
    #vtLog.vtLngInit('vPrjTimer','vPrjTimer.log',iLogLv,iSockPort=60007)
    
    #wxl=wxLocale()
    #wxl.Init(wxLANGUAGE_DEFAULT)
    # Hack to get the locale directory
    #basepath = os.path.abspath(os.path.dirname(sys.argv[0]))
    #localedir = os.path.join(basepath, "locale")
    #localedir = os.path.join(basepath, "locale")

    #langid = wx.LANGUAGE_DEFAULT    # use OS default; or use LANGUAGE_JAPANESE, etc.
    #domain = "messages"             # the translation file is messages.mo

    # Set locale for wxWidgets
    #mylocale = wx.Locale(langid)
    #mylocale.AddCatalogLookupPathPrefix(localedir)
    #mylocale.AddCatalog(domain)

    #application = BoaApp(0)
    #if cfgFN is not None:
    #    application.OpenCfgFile(cfgFN)
    #elif fn is not None:
    #    application.OpenFile(fn)
    #else:
    #    application.OpenCfgFile('config.xml')
    application.MainLoop()

if __name__ == '__main__':
    main()
    