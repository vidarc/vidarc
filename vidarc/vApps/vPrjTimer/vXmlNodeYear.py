#----------------------------------------------------------------------------
# Name:         vXmlNodeYear.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060722
# CVS-ID:       $Id: vXmlNodeYear.py,v 1.2 2008/01/23 23:57:11 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

#from vidarc.tool.xml.vtXmlNodeTag import *
from vidarc.ext.data.veDataCollectorCfg import veDataCollectorCfg
try:
    if vcCust.is2Import(__name__):
        #from vXmlNodeXXXPanel import *
        #from vXmlNodeXXXEditDialog import *
        #from vXmlNodeXXXAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeYear(veDataCollectorCfg):
    #NODE_ATTRS=[
    #        ('Tag',None,'tag',None),
    #        ('Name',None,'name',None),
    #    ]
    #FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='year',dSrcWid=None,srcAppl=None,bChilds=True,
                    dataProc=None):
        global _
        _=vtLgBase.assignPluginLang('vPrjTimer')
        veDataCollectorCfg.__init__(self,tagName,dSrcWid=dSrcWid,srcAppl=srcAppl,bChilds=bChilds,
                    dataProc=dataProc)
    def GetDescription(self):
        return _(u'year')
    # ---------------------------------------------------------
    # specific
    def GetYear(self,node):
        return self.GetVal(node,'tag',int,-1)
    def GetXmlGroupingData(self,node):
        return self.GetYear(node)
    def SetYear(self,node,val):
        self.SetVal(node,'tag',unicode(val))
    def SetXmlGroupingData(self,node,val):
        self.SetTag(node,'%04d'%val)
    # ---------------------------------------------------------
    # inheritance
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x9aIDAT8\x8d\xd5\x93\xd1\r\x830\x0cD\xefh\xf7\x080\x08(\x9b0\x1a\
\x9bD0H\x8b'1\x1f)U\xd2\x18\x84D\xfbQ\xffD\x17\xd9w/QBV7\\\xa9\xea\xd2\xf47\
\x0c\xee\xa9\xd0aT\xd4\x12\xc5\xe2\x80\xd0\x81\xd2\x12\x00\xb4\x0f\x8aF\xb2\
\xbd\x82\x80\xe3\xc0L'\x8d\x00\x8aa\xfb\x08\xa1\x8bk-1u\xabFJC\xcb\x80\x93'\
\x16\x17\x85\x9f\xb1\xe1\x7f\xd2\xed\x13\xa4\x14\xafa\xb3\xe7\xc8\x80\xd2f\
\x14\x9c\xbc\x99\xbeO\x00\x00Ow\x14|\xc2\xe0d\xfd\xc6\xe0\xfdh\x12\xad\xeea^\
&\xff\xff3\xad\xc4\xa722\x95\x7f;\xd6\x00\x00\x00\x00IEND\xaeB`\x82" 
    def getSelImageData(self):
        return self.getImageData()
