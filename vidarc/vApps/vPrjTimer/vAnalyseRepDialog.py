#Boa:Dialog:vAnalyseRepDialog
#----------------------------------------------------------------------------
# Name:         vAnalyseRepDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vAnalyseRepDialog.py,v 1.2 2006/01/17 12:17:47 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

from vidarc.tool.time.vTimeDateGM import *
from vidarc.tool.time.vTimeTimeEdit import *
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.xml.vtXmlFilter as vtXmlFilter
from vidarc.vApps.vPrjTimer.vReportGroupingPanel import *
import vidarc.tool.xml.vtXmlGrp as vtXmlGrp
import vidarc.tool.xml.vtXmlGrpAdd2List as vtXmlGrpAdd2List
import vidarc.tool.xml.vtXmlGroupTreeList as vtXmlGroupTreeList

import thread,time,fnmatch,sys,types

import images

VERBOSE=0

# defined event for vgpXmlTree item selected
wxEVT_THREAD_ANALYSE_ELEMENTS=wx.NewEventType()
def EVT_THREAD_ANALYSE_ELEMENTS(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_ANALYSE_ELEMENTS,func)
class wxThreadAnalyseElements(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_ANALYSE_ELEMENTS(<widget_name>, self.OnItemSel)
    """

    def __init__(self,iStep,iVal,iCount=-1):
        wx.PyEvent.__init__(self)
        self.step=iStep
        self.val=iVal
        self.count=iCount
        self.SetEventType(wxEVT_THREAD_ANALYSE_ELEMENTS)
    def GetStep(self):
        return self.step
    def GetValue(self):
        return self.val
    def GetCount(self):
        return self.count
# defined event for vgpXmlTree item selected
wxEVT_THREAD_ANALYSE_ELEMENTS_FINISHED=wx.NewEventType()
def EVT_THREAD_ANALYSE_ELEMENTS_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_ANALYSE_ELEMENTS_FINISHED,func)
class wxThreadAnalyseElementsFinished(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_ANALYSE_ELEMENTS_FINISHED(<widget_name>, self.OnItemSel)
    """
    def __init__(self):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_THREAD_ANALYSE_ELEMENTS_FINISHED)
    

class thdAnalyse:
    def __init__(self,par):
        self.tree=par
        self.lstSort=None
        self.running=False
    def Find(self,dDoc,prjId,usrId,locId,lstTasks):
        self.grp=self.tree.getGrp()
        self.dDoc=dDoc
        self.prjId=prjId
        self.usrId=usrId
        self.locId=locId
        self.zStartDate=self.tree.dteStartDate.GetValue()
        self.zEndDate=self.tree.dteEndDate.GetValue()
        self.zStartTime=self.tree.tmeStart.GetTimeStr()
        self.zEndTime=self.tree.tmeEnd.GetTimeStr()
        
        self.lstTasks=lstTasks
        if len(self.lstTasks)>0:
            self.bTask=True
        else:
            self.bTask=False
        self.bFind=True
        self.lstFound=[]
        self.iFound=0
        self.iCount=self.tree.doc.GetElementCount()
        self.Start()
        self.lstSort=None
    def Clear(self):
        self.iCount=0
        self.iFound=0
            
        self.dElem={}
        if self.lstSort is not None:
            del self.lstSort
        self.lstSort=[]
        self.iAct=0
            
    def Show(self):
        self.bFind=False
        if self.running==False:
            self.Start()
    def Start(self):
        self.keepGoing = self.running = True
        thread.start_new_thread(self.Run, ())

    def Stop(self):
        self.keepGoing = False

    def IsRunning(self):
        return self.running
    def __procNode__(self,node,*args):
        if self.keepGoing==False:
            return
        bFound=True
        sTag=None
        sHier=None
        sTagName=None
        sName=None
        self.iAct+=1
        wx.PostEvent(self.tree,wxThreadAnalyseElements(1,self.iAct,self.iCount))
        zStart=self.tree.doc.getNodeText(node,'starttime')
        if zStart=='--:--:--':
            bFound=False
        #print node
        if self.prjId>=0:
            n=self.tree.doc.getChild(node,'project')
            if self.tree.doc.getAttribute(n,'fid')!=self.prjId:
                bFound=False
        if bFound and (self.usrId>=0):
            n=self.tree.doc.getChild(node,'person')
            if self.tree.doc.getAttribute(n,'fid')!=self.usrId:
                bFound=False
        if bFound and (self.locId>=0):
            n=self.tree.doc.getChild(node,'loc')
            if self.tree.doc.getAttribute(n,'fid')!=self.locId:
                bFound=False
        if bFound and self.bTask:
            n=self.tree.doc.getChild(node,'task')
            tId=self.tree.doc.getAttribute(n,'fid')
            try:
                self.lstTasks.index(long(tId))
            except:
                bFound=False
        if bFound:
            sDate=self.tree.doc.getNodeText(node,'date')
            if self.zStartDate<=sDate and sDate<=self.zEndDate:
                pass
            else:
                bFound=False            
        if bFound:
            self.iFound+=1
            self.lstFound.append(node)
        #self.iCount+=1
        return 0
    def __showNodes__(self):
        self.tree.lstRes.DeleteAllItems()
        if self.lstSort is not None:
            del self.lstSort
        self.lstSort=[]
        self.iAct=0
        locIds=self.tree.netLoc.GetSortedIds()
        prjIds=self.tree.netPrj.GetSortedIds()
        taskIds=self.tree.netTask.GetSortedSubTaskIds()
        #tup=ids.GetInfoByIdx(i)
        
        #self.lstRes.InsertColumn(0,u'Week',wx.LIST_FORMAT_LEFT,40)
        #self.lstRes.InsertColumn(1,u'Date',wx.LIST_FORMAT_LEFT,85)
        #self.lstRes.InsertColumn(2,u'Client',wx.LIST_FORMAT_LEFT,85)
        #self.lstRes.InsertColumn(3,u'Prj',wx.LIST_FORMAT_LEFT,85)
        #self.lstRes.InsertColumn(4,u'Task',wx.LIST_FORMAT_LEFT,85)
        #self.lstRes.InsertColumn(5,u'Sum',wx.LIST_FORMAT_LEFT,85)
        dt=wx.DateTime()
        dRes={}
        #vtLog.vtLogCallDepth(self,'found')
        #vtLog.vtLogCallDepth(self,'found:%d'%self.iFound)
        for node in self.lstFound:
            #print node
            n=self.tree.doc.getChild(node,'project')
            sId=self.tree.doc.getAttribute(n,'fid')
            sPrj='---'
            try:
                tup=prjIds.GetInfoById(sId)
                if tup[0] is None:
                    sPrj=self.tree.doc.getNodeText(node,'project')
                else:
                    sPrj=tup[1]
            except:
                sPrj=self.tree.doc.getNodeText(node,'project')
            try:
                n=self.tree.netPrj.getChild(tup[0],'attributes')
                if n is None:
                    sClient='---'
                else:
                    sClient=self.tree.netPrj.getNodeText(n,'client')
            except:
                sClient='---'
            
            n=self.tree.doc.getChild(node,'task')
            sId=self.tree.doc.getAttribute(n,'fid')
            sTask='---'
            try:
                tup=taskIds.GetInfoById(sId)
                if tup[0] is None:
                    sTask=self.tree.doc.getNodeText(node,'task')
                else:
                    sTask=tup[1]
            except:
                sTask=self.tree.doc.getNodeText(node,'task')
            
            n=self.tree.doc.getChild(node,'loc')
            sId=self.tree.doc.getAttribute(n,'fid')
            sLoc='---'
            try:
                tup=locIds.GetInfoById(sId)
                if tup[0] is None:
                    sLoc=self.tree.doc.getNodeText(node,'loc')
                else:
                    sLoc=tup[1]
            except:
                sLoc=self.tree.doc.getNodeText(node,'loc')
            sDate=self.tree.doc.getNodeText(node,'date')
            dt.ParseFormat(sDate,'%Y%m%d')
            sYear=dt.Format('%Y')
            sMon=dt.Format('%m')
            sDay=dt.Format('%d')
            iWeek=dt.GetWeekOfYear()
            
            #if self.locId>=0:
            #    n=self.tree.doc.getChild(node,'loc')
            #    sId=self.tree.doc.getAttribute(n,'fid')
            #    tup=locIds.GetInfoById(sId)
            
            
            zStart=self.tree.doc.getNodeText(node,'starttime')
            zEnd=self.tree.doc.getNodeText(node,'endtime')
            if 1==0:
                if zStart<self.zStartTime:
                    if zEnd<self.zStartTime:
                        print 'out of range',zStart,zEnd,self.zStart,self.zEndTime
                        zEnd=zStart
                    elif zEnd>self.zEndTime:
                        print 'change end ',zEnd,self.zEndTime
                        zEnd=self.zEndTime
                        
                else:
                    if zStart>self.zEndTime:
                        print 'out of range',zStart,zEnd,self.zStart,self.zEndTime
                        zStart=zEnd
                    else:
                        print 'change start',zStart,self.zStartTime
                        zStart=self.zStartTime
                        if zEnd>self.zEndTime:
                            print 'change end ',zEnd,self.zEndTime
                            zEnd=self.zEndTime
                
            try:
                hr=int(zStart[:2])
                min=int(zStart[2:4])
                sHr=hr+min/60.0
                
                hr=int(zEnd[:2])
                min=int(zEnd[2:4])
                eHr=hr+min/60.0
                if sHr<eHr:
                    sum=(eHr-sHr)
                else:
                    sum=(sHr-eHr)
            except:
                sum=0
            
            tup=[sYear,sMon,sDay,'%02d'%iWeek,sPrj,sTask,sClient,sLoc,sum]
            
            d=dRes
            for g in self.grp[:-1]:
                k=tup[g]
                try:
                    dF=d[k]
                    d=dF
                except:
                    dF={}
                    d[k]=dF
                    d=dF
            k=tup[self.grp[-1]]
            try:
                lst=d[k]
                lst.append(tup)
            except:
                d[k]=[tup]
            self.iAct+=1
            
            wx.PostEvent(self.tree,wxThreadAnalyseElements(2,self.iAct,self.iFound))
            if self.keepGoing==False:
                return
        d=dRes
        self.iAct=0
        def addDict(d,level):
            keys=d.keys()
            keys.sort()
            #print keys
            sumTotal=0
            for k in keys:
                v=d[k]
                if types.DictType==type(v):
                    sum=addDict(v,level+1)
                    idx=self.grp[level]
                    #index = self.tree.lstRes.InsertImageStringItem(sys.maxint, '', -1)
                    #self.tree.lstRes.SetStringItem(index,idx,'sum',-1)
                    #self.tree.lstRes.SetStringItem(index,4,'%6.2f'%sum,-1)
                    #sumTotal+=sum
                else:
                    #print level,v
                    sum=0.0
                    tupRes=[None,None,None,None,None,None,None,None,0.0]
                    for tup in v:
                        for i in range(9):
                            if i==8:
                                tupRes[i]+=tup[i]
                            else:
                                if tupRes[i] is None:
                                    tupRes[i]=tup[i]
                                else:
                                    if tupRes[i]!=tup[i]:
                                        tupRes[i]='*'
                        sum+=tup[-1]
                        #wx.PostEvent(self.tree,wxThreadAnalyseElements(4,index,self.iFound))
                    for i in range(9):
                        if tupRes[i] is None:
                            tupRes[i]=''
                    self.lstSort.append(tupRes)
                    #index = self.tree.lstRes.InsertImageStringItem(sys.maxint, tupRes[0], -1)
                    #self.tree.lstRes.SetStringItem(index,1,tupRes[1],-1)
                    #self.tree.lstRes.SetStringItem(index,2,tupRes[2],-1)
                    #self.tree.lstRes.SetStringItem(index,3,str(tupRes[3]),-1)
                    #self.tree.lstRes.SetStringItem(index,4,'%6.2f'%tupRes[4],-1)
                    #self.tree.lstRes.SetStringItem(index,5,tupRes[5],-1)
                    #self.tree.lstRes.SetStringItem(index,6,tupRes[6],-1)
                    #self.tree.lstRes.SetStringItem(index,7,tupRes[7],-1)
                    #idx=grp[level]
                    #index = self.tree.lstRes.InsertImageStringItem(sys.maxint, '', -1)
                    #self.tree.lstRes.SetStringItem(index,idx,'sum',-1)
                    #self.tree.lstRes.SetStringItem(index,4,'%6.2f'%sum,-1)
                    sumTotal+=sum
                    wx.PostEvent(self.tree,wxThreadAnalyseElements(3,self.iAct,self.iFound))
                if self.keepGoing==False:
                    return -1
            return sumTotal
        sum=addDict(dRes,0)
        def cmpFunc(a,b):
            for g in self.grp:
                i=cmp(a[g],b[g])
                if i!=0:
                    return i
            return 0
        self.lstSort.sort(cmpFunc)
        sum=0.0
        self.iAct=0
        wx.PostEvent(self.tree,wxThreadAnalyseElements(3,self.iAct,self.iFound))
        for tup in self.lstSort:
            index = self.tree.lstRes.InsertImageStringItem(sys.maxint, tup[0], -1)
            self.tree.lstRes.SetStringItem(index,1,tup[1],-1)
            self.tree.lstRes.SetStringItem(index,2,tup[2],-1)
            self.tree.lstRes.SetStringItem(index,3,str(tup[3]),-1)
            self.tree.lstRes.SetStringItem(index,4,'%6.2f'%tup[8],-1)
            self.tree.lstRes.SetStringItem(index,5,tup[4],-1)
            self.tree.lstRes.SetStringItem(index,6,tup[5],-1)
            self.tree.lstRes.SetStringItem(index,7,tup[6],-1)
            self.tree.lstRes.SetStringItem(index,8,tup[7],-1)
            sum+=tup[-1]
            
        index = self.tree.lstRes.InsertImageStringItem(sys.maxint, 'total', -1)
        self.tree.lstRes.SetStringItem(index,4,'%6.2f'%sum,-1)
        
        index = self.tree.lstRes.InsertImageStringItem(sys.maxint, 'avg', -1)
        try:
            avg='%6.2f'%(sum/(len(self.lstSort)+0.0))
        except:
            avg=''
        self.tree.lstRes.SetStringItem(index,4,avg,-1)
                    
    def Run(self):
        if self.bFind:
            self.Clear()
            self.tree.doc.procChildsAttr(self.tree.doc.getBaseNode(),'id',
                    self.__procNode__,None)

        if self.keepGoing:
            self.__showNodes__()
        if self.keepGoing==False:
            self.tree.lstRes.DeleteAllItems()
        self.keepGoing = self.running = False
        wx.PostEvent(self.tree,wxThreadAnalyseElements(0,0))
        wx.PostEvent(self.tree,wxThreadAnalyseElementsFinished())
    def GetCount(self):
        return self.iCount
    def GetFound(self):
        return self.iFound
    def GetFoundElements(self):
        return self.dElem
    def GetNode(self,idx):
        if self.lstSort is None:
            return None
        try:
            return self.lstSort[idx][1][3]
        except:
            return None

def create(parent):
    return vAnalyseRepDialog(parent)

[wxID_VANALYSEREPDIALOG, wxID_VANALYSEREPDIALOGCBGEN, 
 wxID_VANALYSEREPDIALOGCBOK, wxID_VANALYSEREPDIALOGGPROCESS, 
 wxID_VANALYSEREPDIALOGGSTEP, 
] = [wx.NewId() for _init_ctrls in range(5)]

GROUPING_LST=[u'year', u'month', u'day',
                        u'week', u'project', u'task', 
                        u'client', u'location']
GROUPING_DICT={
    u'year':'',
    u'month':'', 
    u'day':'',
    u'week':'', 
    u'project':'', 
    u'task':'', 
    u'client':'', 
    u'location':''}

class vAnalyseRepDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VANALYSEREPDIALOG,
              name=u'vAnalyseRepDialog', parent=prnt, pos=wx.Point(439, 131),
              size=wx.Size(760, 526), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vPrjTimer Analyse Report Dialog')
        self.SetClientSize(wx.Size(752, 492))

        self.cbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VANALYSEREPDIALOGCBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'cbOk',
              parent=self, pos=wx.Point(344, 456), size=wx.Size(76, 30),
              style=0)
        self.cbOk.Bind(wx.EVT_BUTTON, self.OnCbOkButton,
              id=wxID_VANALYSEREPDIALOGCBOK)

        self.cbGen = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VANALYSEREPDIALOGCBGEN,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Generate', name=u'cbGen',
              parent=self, pos=wx.Point(104, 428), size=wx.Size(76, 30),
              style=0)
        self.cbGen.Bind(wx.EVT_BUTTON, self.OnCbGenButton,
              id=wxID_VANALYSEREPDIALOGCBGEN)

        self.gProcess = wx.Gauge(id=wxID_VANALYSEREPDIALOGGPROCESS,
              name=u'gProcess', parent=self, pos=wx.Point(288, 432), range=100,
              size=wx.Size(448, 18), style=wx.GA_HORIZONTAL | wx.GA_SMOOTH)

        self.gStep = wx.Gauge(id=wxID_VANALYSEREPDIALOGGSTEP, name=u'gStep',
              parent=self, pos=wx.Point(192, 432), range=4, size=wx.Size(88,
              18), style=wx.GA_HORIZONTAL | wx.GA_SMOOTH)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.verbose=VERBOSE
        
        self.thdAnalyse=thdAnalyse(self)
        EVT_THREAD_ANALYSE_ELEMENTS_FINISHED(self,self.OnAnalyseFin)
        EVT_THREAD_ANALYSE_ELEMENTS(self,self.OnAnalyseProgress)
        
        self.docFilter=vtXmlFilter.vtXmlFilter(verbose=1)
        vtXmlFilter.EVT_THREAD_FILTER_ELEMENTS(self,self.OnFilterElement)
        vtXmlFilter.EVT_THREAD_FILTER_ELEMENTS_FINISHED(self,self.OnFilterFinished)

        self.doc=None
        self.netHum=None
        self.netLoc=None
        self.netDoc=None
        self.netPrjDoc=None
        self.netTask=None
        self.netPrj=None
        
        id=wx.NewId()
        self.vlbReport=vReportGroupingPanel(parent=self,id=id,
                pos=(0,0),size=(740, 400),style=0,name="vlbReport")
        
        eventer=self.vlbReport.GetFindEventPoster()
        vtXmlFilter.EVT_THREAD_FILTER_ELEMENTS(eventer,self.OnFilterElement)
        vtXmlFilter.EVT_THREAD_FILTER_ELEMENTS_FINISHED(eventer,self.OnFilterFinished)

        eventer=self.vlbReport.GetGroupEventPoster()
        vtXmlGrp.EVT_THREAD_GROUP_ELEMENTS(eventer,self.OnGroupElement)
        vtXmlGrp.EVT_THREAD_GROUP_ELEMENTS_FINISHED(eventer,self.OnGroupFinished)
        vtXmlGrpAdd2List.EVT_THREAD_GROUP_ADD2LIST_ELEMENTS(eventer,self.OnGroupAdd2ListElement)
        vtXmlGrpAdd2List.EVT_THREAD_GROUP_ADD2LIST_ELEMENTS_FINISHED(eventer,self.OnGroupAdd2ListFinished)
        
        eventer=self.vlbReport.GetGroupTreeEventPoster()
        vtXmlGroupTreeList.EVT_THREAD_GROUP_TREELIST_ELEMENTS(eventer,self.OnGroupTreeList)
        vtXmlGroupTreeList.EVT_THREAD_GROUP_TREELIST_ELEMENTS_FINISHED(eventer,self.OnGroupTreeListFinished)
        
        self.cbGen.SetBitmapLabel(images.getBuildBitmap())
        self.cbOk.SetBitmapLabel(images.getOkBitmap())
    def OnAnalyseProgress(self,evt):
        self.gStep.SetValue(evt.GetStep())
        if evt.GetCount()>0:
            self.gProcess.SetRange(evt.GetCount())
        #else:
        #    self.gProcess.SetRange(self.iSize)
        self.gProcess.SetValue(evt.GetValue())
    def OnAnalyseFin(self,evt):
        #self.txtNodeCount.SetValue(str(self.thdAnalyse.GetCount()))
        #self.txtCount.SetValue(str(self.thdAnalyse.GetFound()))
        #self.cbFind.Enable(True)
        pass
    def SetDocs(self,doc,netHum,netLoc,netDoc,netPrjDoc,netPrj,netTask):
        self.doc=doc
        self.netHum=netHum
        self.netLoc=netLoc
        self.netDoc=netDoc
        self.netPrjDoc=netPrjDoc
        self.netPrj=netPrj
        self.netTask=netTask
        self.vlbReport.SetDocs(doc,netHum,netLoc,netDoc,netPrjDoc,netPrj,netTask)
    def UpdateInfos(self):
        self.vlbReport.UpdateInfos()
        self.gProcess.SetRange(self.doc.GetElementAttrCount())
        return

    def OnCbOkButton(self, event):
        self.thdAnalyse.Stop()
        while self.thdAnalyse.IsRunning():
            time.sleep(1)
        self.Show(False)
        #self.EndModal(1)
        event.Skip()

    def OnCbGenButton(self, event):
        self.gStep.SetValue(1)
        self.vlbReport.Find()
        event.Skip()
    
    def OnFilterElement(self,evt):
        #if self.verbose:
        #    vtLog.vtLogCallDepth(self,'')
        self.gProcess.SetValue(evt.GetValue())
        evt.Skip()
    def OnFilterFinished(self,evt):
        if self.verbose:
            vtLog.vtLogCallDepth(self,'found:%d'%self.docFilter.getFoundCount())
        evt.Skip()
    def OnGroupElement(self,evt):
        self.gStep.SetValue(2)
        self.gProcess.SetValue(evt.GetValue())
        evt.Skip()
    def OnGroupFinished(self,evt):
        self.gProcess.SetValue(0)
        evt.Skip()
    def OnGroupAdd2ListElement(self,evt):
        self.gStep.SetValue(3)
        self.gProcess.SetValue(evt.GetValue())
        evt.Skip()
    def OnGroupAdd2ListFinished(self,evt):
        self.gProcess.SetValue(0)
        self.gStep.SetValue(4)
        evt.Skip()
    def OnGroupTreeList(self,evt):
        self.gProcess.SetValue(evt.GetValue())
        evt.Skip()
    def OnGroupTreeListFinished(self,evt):
        self.gProcess.SetValue(0)
        self.gStep.SetValue(0)
        evt.Skip()
        