#----------------------------------------------------------------------------
# Name:         vXmlNodeMonth.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060722
# CVS-ID:       $Id: vXmlNodeMonth.py,v 1.3 2008/01/23 23:57:11 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

#from vidarc.tool.xml.vtXmlNodeTag import *
from vidarc.ext.data.veDataCollectorCfg import veDataCollectorCfg

try:
    if vcCust.is2Import(__name__):
        #from vXmlNodeMonthPanel import *
        #from vXmlNodeXXXEditDialog import *
        #from vXmlNodeXXXAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeMonth(veDataCollectorCfg):
    #NODE_ATTRS=[
    #        ('Tag',None,'tag',None),
    #        ('Name',None,'name',None),
    #    ]
    #FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='month',dSrcWid=None,srcAppl=None,bChilds=True,
                    dataProc=None):
        global _
        _=vtLgBase.assignPluginLang('vPrjTimer')
        veDataCollectorCfg.__init__(self,tagName,dSrcWid=dSrcWid,srcAppl=srcAppl,bChilds=bChilds,
                    dataProc=dataProc)
    def GetDescription(self):
        return _(u'month')
    # ---------------------------------------------------------
    # specific
    def GetMonth(self,node):
        return self.GetVal(node,'tag',int,-1)
    def GetSetup(self,node=None):
        return [
            {'tag':'day'      ,'trans':'day','label':_('day'),'typedef':{'type':'int','min':'1','max':'31'},'is2Report':1,'isCurrency':0},
            {'tag':'login'    ,'trans':'login','label':_('login'),'typedef':{'type':'string'},'is2Report':1,'isCurrency':0},
            {'tag':'sum'      ,'trans':'sum','label':_('sum'),'typedef':{'type':'float','digits':'2','comma':'2','min':'0','max':'24'},'is2Report':1,'isCurrency':0},
            {'tag':'surname'  ,'trans':'surname','label':_('surname'),'typedef':{'type':'string'},'is2Report':1,'isCurrency':0},
            {'tag':'firstname','trans':'firstname','label':_('firstname'),'typedef':{'type':'string'},'is2Report':1,'isCurrency':0},
            ]
    def GetXmlGroupingData(self,node):
        return self.GetMonth(node)
    def SetMonth(self,node,val):
        self.SetVal(node,'tag',unicode(val))
    def SetXmlGroupingData(self,node,val):
        self.SetTag(node,'%02d'%val)
    # ---------------------------------------------------------
    # inheritance
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\xbaIDAT8\x8d\xc5\x93\xbb\r\xc20\x10\x86\xbf\xbb\xa4\x0fRz\x90\x98\
\xc0L\xc0*d\xb20\x92\'@\n}$\xb2@B\x91\x87\x1c\xbf(Rp\x95e\xdf}\xff\xfdg[D\
\x0b\x8e\x84\x1e\xaa\x06J\x80\xc7\xf8\x9a\xdc\xcd\xa7^\xc5O4c?\x19\x06\xfc<u\
\x0b\x1a:\x1a\xba\x00\x08`\x18\xb6s\xb7fg\xc1rb\x05\xf9\xea\x96*ja\x07h9ok\
\xb7\x0b\xc3\x80\xd5:\xb0\x15v\xe0$\xdd\x16\xbff\xec\x03;I\xc0\xdc\xc5eQ\xfd\
l\x83\x8b\r5\tp\x93\x1b\xde9\xf18\xc0\xef"\xa7\x9e\x04\xac\x13_A\xb9(\xa3\
\x00\xad\xe5\xfe\xb3t\x0e\x11-vWf\xa9\x82+\x8b\xbdB\x98\xe7%\x7f\xffL\x87\
\x01_wT=\xa9\x88K\x01\xde\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetPanelClassOld(self):
        if GUI:
            return vXmlNodeMonthPanel
        else:
            return None
