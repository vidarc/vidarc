#Boa:FramePanel:vGroupPanel
#----------------------------------------------------------------------------
# Name:         vGroupPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vGroupPanel.py,v 1.1 2005/12/13 13:20:07 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

from vidarc.tool.time.vTimeDateGM import *
from vidarc.tool.time.vTimeTimeEdit import *
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.xml.vtXmlGrp as vtXmlGrp
import vidarc.tool.xml.vtXmlGrpAdd2List as vtXmlGrpAdd2List

import thread,time,fnmatch,sys,types

import images

VERBOSE=0
    
def __convYear__(val):
    return val[:4]
    
def __convMonth__(val):
    return val[4:6]
    
def __convDay__(val):
    return val[6:8]
    
def __convWeek__(val):
    dt.ParseFormat(val,'%Y%m%d')
    return '%02d'%dt.GetWeekOfYear()
    
GROUPING_LST=[u'person',u'year', u'month', u'day',
                        u'week', u'project', u'task', 
                        u'client', u'location']
GROUPING_CONV=[None,__convYear__,__convMonth__,__convDay__,
            __convWeek__,None,None,None,None]
SHOWING_LST=[u'person', u'year', u'month', u'day', u'week', u'sum',
                        u'project', u'task', 
                        u'client', u'location']
SHOWING_CONV=[None,None,None,
            None,None,None,None,None,None,None]
GROUPING_INFO=['person','date','date','date',
            'date','project|fid','task|fid','loc|fid','starttime','endtime']

# defined event for vgpXmlTree item selected
wxEVT_ADD_GROUP_THREAD_ELEMENTS=wx.NewEventType()
def EVT_ADD_GROUP_THREAD_ELEMENTS(win,func):
    win.Connect(-1,-1,wxEVT_ADD_GROUP_THREAD_ELEMENTS,func)
class wxAddGroupThreadElements(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_ADD_GROUP_THREAD_ELEMENTS(<widget_name>, self.OnItemSel)
    """

    def __init__(self,iVal,iCount=-1):
        wx.PyEvent.__init__(self)
        self.val=iVal
        self.count=iCount
        self.SetEventType(wxEVT_ADD_GROUP_THREAD_ELEMENTS)
    def GetValue(self):
        return self.val
    def GetCount(self):
        return self.count
# defined event for vgpXmlTree item selected
wxEVT_ADD_GROUP_THREAD_ANALYSE_ELEMENTS_FINISHED=wx.NewEventType()
def EVT_ADD_GROUP_THREAD_ELEMENTS_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_ADD_GROUP_THREAD_ELEMENTS_FINISHED,func)
class wxAddGroupThreadElementsFinished(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_ADD_GROUP_THREAD_ELEMENTS_FINISHED(<widget_name>, self.OnItemSel)
    """
    def __init__(self):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_ADD_GROUP_THREAD_ELEMENTS_FINISHED)
    
class vtXmlGroupTimerList(vtXmlGrpAdd2List.vtXmlGroupAdd2List):
    def __procGroup__(self,node,tagName,infos):
        locIds=self.master.netLoc.GetSortedIds()
        prjIds=self.master.netPrj.GetSortedIds()
        taskIds=self.master.netTask.GetTaskIds()
        humIds=self.master.netHum.getIds()
        sClient='---'
        n=self.master.doc.getChild(node,'project')
        sPrjId=self.master.doc.getAttribute(n,'fid')
        lst=[]
        for grp in self.showing:
            if grp[0]==u'year':
                sDate=infos['date']
                lst.append(sDate[:4])
            elif grp[0]==u'month':
                sDate=infos['date']
                lst.append(sDate[4:6])
            elif grp[0]==u'day':
                sDate=infos['date']
                lst.append(sDate[6:8])
            elif grp[0]==u'week':
                sDate=infos['date']
                dt.ParseFormat(sDate,'%Y%m%d')
                iWeek=dt.GetWeekOfYear()
                lst.append('%02d'%iWeek)
            elif grp[0]==u'project':
                n=self.master.doc.getChild(node,'project')
                sId=self.master.doc.getAttribute(n,'fid')
                sPrj='---'
                try:
                    tup=prjIds.GetInfoById(sId)
                    if tup[0] is None:
                        sPrj=self.master.doc.getNodeText(node,'project')
                    else:
                        sPrj=tup[1]
                except:
                    sPrj=self.master.doc.getNodeText(node,'project')
                try:
                    n=self.master.netPrj.getChild(tup[0],'attributes')
                    if n is None:
                        sClient='---'
                    else:
                        sClient=self.master.netPrj.getNodeText(n,'client')
                except:
                    sClient='---'
                lst.append(sPrj)
            elif grp[0]==u'task':
                n=self.master.doc.getChild(node,'task')
                sId=self.master.doc.getAttribute(n,'fid')
                sTask='---'
                try:
                    bTaskFound=False
                    prjTasks=taskIds[sPrjId][1]
                    try:
                        tup=prjTasks[sId]
                        bTaskFound=True
                        sTask=self.master.doc.getNodeText(tup[0])
                    except:
                        for v in prjTasks.values():
                            try:
                                tup=v[1][sId]
                                bTaskFound=True
                                sTask=self.master.doc.getNodeText(v[0],'name')
                                sTask+=':'+self.master.doc.getNodeText(tup,'name')
                                break
                            except:
                                pass
                    if bTaskFound:
                        pass
                    else:
                        sTask=self.master.doc.getNodeText(node,'task')
                except:
                    sTask=self.master.doc.getNodeText(node,'task')
                lst.append(sTask)
            elif grp[0]==u'client':
                lst.append(sClient)
            elif grp[0]==u'location':
                n=self.master.doc.getChild(node,'loc')
                sId=self.master.doc.getAttribute(n,'fid')
                sLoc='---'
                try:
                    tup=locIds.GetInfoById(sId)
                    if tup[0] is None:
                        sLoc=self.master.doc.getNodeText(node,'loc')
                    else:
                        sLoc=tup[1]
                except:
                    sLoc=self.master.doc.getNodeText(node,'loc')
                lst.append(sLoc)
            elif grp[0]==u'person':
                n=self.master.doc.getChild(node,'person')
                sId=self.master.doc.getAttribute(n,'fid')
                sHum='---'
                try:
                    tup=humIds.GetIdStr(sId)
                    if tup[0] is None:
                        sHum=self.master.doc.getNodeText(node,'person')
                    else:
                        sHum=self.master.doc.getNodeText(tup[1],'name')+'|'+sId
                except:
                    sHum=self.master.doc.getNodeText(node,'person')
                lst.append(sHum)
            elif grp[0]==u'sum':
                zStart=infos['starttime']
                zEnd=infos['endtime']
                try:
                    hr=int(zStart[:2])
                    min=int(zStart[2:4])
                    sHr=hr+min/60.0
                    
                    hr=int(zEnd[:2])
                    min=int(zEnd[2:4])
                    eHr=hr+min/60.0
                    if sHr<eHr:
                        sum=(eHr-sHr)
                    else:
                        sum=(sHr-eHr)
                except:
                    sum=0.0
                lst.append('%6.2f'%sum)
        return lst
    def __outputNodes__(self,lst,prefix):
        def compFunc(a,b):
            for i in self.sorting:
                res=cmp(a[i],b[i])
                if res!=0:
                    return res
            return 0
        lst.sort(compFunc)
        
        tupRes=[]
        for it in self.showing:
            tupRes.append(None)
        if self.idxSum>=0:
            tupRes[self.idxSum]=0.0
        iCount=len(self.showing)
        for tup in lst:
            for i in range(iCount):
                if i==self.idxSum:
                    tupRes[i]+=float(tup[i])
                else:
                    if tupRes[i] is None:
                        tupRes[i]=tup[i]
                    else:
                        if tupRes[i]!=tup[i]:
                            tupRes[i]='*'
            if self.master.IsAll2Show():
                i=string.find(tup[0],'|')
                if i>0:
                    val=tup[0][:i]
                else:
                    val=tup[0]
                index = self.lstCtrl.InsertImageStringItem(sys.maxint, val, -1)
                i=1
                for t in tup[1:]:
                    self.lstCtrl.SetStringItem(index,i,t,-1)
                    i+=1
        for i in range(iCount):
            if tupRes[i] is None:
                tupRes[i]=''
        i=string.find(tupRes[0],'|')
        if i>0:
            val=tupRes[0][:i]
        else:
            val=tupRes[0]
        index = self.lstCtrl.InsertImageStringItem(sys.maxint, val, -1)
        i=1
        for t in tupRes[1:]:
            if self.idxSum==i:
                self.lstCtrl.SetStringItem(index,i,'%6.2f'%t,-1)
            else:
                self.lstCtrl.SetStringItem(index,i,t,-1)
            i+=1
        if self.idxSum>=0:
            sum=tupRes[self.idxSum]
        else:
            sum=0.0
        self.outputs.append([prefix,sum,lst,tupRes])
        if self.master.IsAll2Show():
            index = self.lstCtrl.InsertImageStringItem(sys.maxint, '', -1)
        return sum

def create(parent):
    return vGroupPanel(parent)

[wxID_VGROUPPANEL, wxID_VGROUPPANELCHCSHOWALL, wxID_VGROUPPANELLBLGRP, 
 wxID_VGROUPPANELLBLRESULTS, wxID_VGROUPPANELLBLSORT, 
 wxID_VGROUPPANELLSTGROUP, wxID_VGROUPPANELLSTRES, wxID_VGROUPPANELLSTSORT, 
] = [wx.NewId() for _init_ctrls in range(8)]



GROUPING_DICT={
    u'year':'',
    u'month':'', 
    u'day':'',
    u'week':'', 
    u'project':'', 
    u'task':'', 
    u'client':'', 
    u'location':''}

dt=wx.DateTime()

class vGroupPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VGROUPPANEL, name=u'vFindPanel',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(665, 443),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(657, 409))

        self.lstRes = wx.ListCtrl(id=wxID_VGROUPPANELLSTRES, name=u'lstRes',
              parent=self, pos=wx.Point(96, 16), size=wx.Size(632, 354),
              style=wx.LC_REPORT)

        self.lstGroup = wx.CheckListBox(choices=[u'0 person', u'1 year', u'2 month',
              u'_ day', u'_ week', u'_ project', u'_ task', u'_ client', u'_ location'],
              id=wxID_VGROUPPANELLSTGROUP, name=u'lstGroup', parent=self,
              pos=wx.Point(4, 16), size=wx.Size(88, 144), style=0)
        self.lstGroup.Bind(wx.EVT_CHECKLISTBOX, self.OnLstGroupChecklistbox,
              id=wxID_VGROUPPANELLSTGROUP)

        self.lblGrp = wx.StaticText(id=wxID_VGROUPPANELLBLGRP, label=u'Groups',
              name=u'lblGrp', parent=self, pos=wx.Point(8, 0), size=wx.Size(34,
              13), style=0)

        self.lblResults = wx.StaticText(id=wxID_VGROUPPANELLBLRESULTS,
              label=u'Results', name=u'lblResults', parent=self,
              pos=wx.Point(96, 0), size=wx.Size(35, 13), style=0)

        self.chcShowAll = wx.CheckBox(id=wxID_VGROUPPANELCHCSHOWALL,
              label=u'Show All', name=u'chcShowAll', parent=self,
              pos=wx.Point(8, 384), size=wx.Size(73, 13), style=0)
        self.chcShowAll.SetValue(False)
        self.chcShowAll.Bind(wx.EVT_CHECKBOX, self.OnChcShowAllCheckbox,
              id=wxID_VGROUPPANELCHCSHOWALL)

        self.lblSort = wx.StaticText(id=wxID_VGROUPPANELLBLSORT,
              label=u'Sorting', name=u'lblSort', parent=self, pos=wx.Point(8,
              170), size=wx.Size(33, 13), style=0)

        self.lstSort = wx.CheckListBox(choices=[u'0 person', u'1 year', u'2 month',
              u'3 day', u'_ week', u'_ sum', u'4 project', u'5 task', u'_ client',
              u'_ location'], id=wxID_VGROUPPANELLSTSORT, name=u'lstSort',
              parent=self, pos=wx.Point(4, 186), size=wx.Size(88, 160),
              style=0)
        self.lstSort.Bind(wx.EVT_CHECKLISTBOX, self.OnLstSortChecklistbox,
              id=wxID_VGROUPPANELLSTSORT)

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        self.verbose=VERBOSE
        
        self.docGrp=vtXmlGrp.vtXmlGroup()
        self.docGrpAdd2List=vtXmlGroupTimerList()
        
        vtXmlGrp.EVT_THREAD_GROUP_ELEMENTS(self,self.OnGroupElement)
        vtXmlGrp.EVT_THREAD_GROUP_ELEMENTS_FINISHED(self,self.OnGroupFinished)
        vtXmlGrpAdd2List.EVT_THREAD_GROUP_ADD2LIST_ELEMENTS(self,self.OnGroupAdd2ListElement)
        vtXmlGrpAdd2List.EVT_THREAD_GROUP_ADD2LIST_ELEMENTS_FINISHED(self,self.OnGroupAdd2ListFinished)
        
        self.doc=None
        self.netHum=None
        self.netLoc=None
        self.netTask=None
        self.netPrj=None
        
        self.lstRes.InsertColumn(0,u'Person',wx.LIST_FORMAT_LEFT,30)
        self.lstRes.InsertColumn(1,u'Y',wx.LIST_FORMAT_RIGHT,40)
        self.lstRes.InsertColumn(2,u'M',wx.LIST_FORMAT_RIGHT,30)
        self.lstRes.InsertColumn(3,u'D',wx.LIST_FORMAT_RIGHT,30)
        self.lstRes.InsertColumn(4,u'W',wx.LIST_FORMAT_RIGHT,30)
        self.lstRes.InsertColumn(5,u'Sum',wx.LIST_FORMAT_RIGHT,60)
        self.lstRes.InsertColumn(6,u'Prj',wx.LIST_FORMAT_LEFT,60)
        self.lstRes.InsertColumn(7,u'Task',wx.LIST_FORMAT_LEFT,85)
        self.lstRes.InsertColumn(8,u'Client',wx.LIST_FORMAT_LEFT,85)
        self.lstRes.InsertColumn(9,u'Location',wx.LIST_FORMAT_LEFT,85)
        
        
        self.lstGroup.Check(0,True)
        self.lstGroup.Check(1,True)
        self.lstGroup.Check(2,True)
        self.lstGroupingIdx=[0,1,2]
        
        self.lstSort.Check(0,True)
        self.lstSort.Check(1,True)
        self.lstSort.Check(2,True)
        self.lstSort.Check(3,True)
        self.lstSort.Check(6,True)
        self.lstSort.Check(7,True)
        self.lstSortingIdx=[0,1,2,3,6,7]
        self.Move(pos)
        
    def __convPrj__(self,val):
        prjIds=self.tree.netPrj.GetSortedIds()
        
        sPrj='---'
        try:
            tup=prjIds.GetInfoById(sId)
            if tup[0] is None:
                return '???'
            else:
                return tup[1]
        except:
            return '???'
        return val

    def __convTask__(self,val):
        taskIds=self.tree.netTask.GetSortedSubTaskIds()
        try:
            tup=taskIds.GetInfoById(sId)
            if tup[0] is None:
                return '???'
            else:
                return tup[1]
        except:
            return '???'
        return val
    
    def __convClt__(self,val):
        return val
    
    def __convLoc__(self,val):
        locIds=self.netLoc.GetSortedIds()
        sLoc='---'
        try:
            tup=locIds.GetInfoById(sId)
            if tup[0] is None:
                return '???'
            else:
                return tup[1]
        except:
            return '???'
        return val
    
    def OnAnalyseProgress(self,evt):
        self.gStep.SetValue(evt.GetStep())
        if evt.GetCount()>0:
            self.gProcess.SetRange(evt.GetCount())
        #else:
        #    self.gProcess.SetRange(self.iSize)
        self.gProcess.SetValue(evt.GetValue())
    def OnAnalyseFin(self,evt):
        #self.txtNodeCount.SetValue(str(self.thdAnalyse.GetCount()))
        #self.txtCount.SetValue(str(self.thdAnalyse.GetFound()))
        #self.cbFind.Enable(True)
        pass
    def SetDocs(self,doc,netHum,netLoc,netDoc,netPrjDoc,netPrj,netTask):
        self.doc=doc
        self.netHum=netHum
        self.netLoc=netLoc
        self.netPrj=netPrj
        self.netTask=netTask
    def UpdateInfos(self):
        self.lstRes.DeleteAllItems()
    
    def Clear(self):
        self.lstRes.DeleteAllItems()
        
    def SetFound(self,lst):
        self.lstFound=lst
        infos,grp,idxs=self.getGrp()
        self.docGrp.setNodeInfos2Get(infos)
        self.docGrp.setGroup(grp,idxs)
        self.docGrp.Group(lst,self)
        #self.gStep.SetValue(1)
    def GetGrouped(self):
        #return self.docGrp.GetGrouped()
        return self.docGrpAdd2List.GetGrouped()
    def GetGroupingInfo(self):
        return GROUPING_INFO
    def GetGroupShowing(self):
        return self.docGrpAdd2List.GetShowing()
    def GetGroupSorting(self):
        return self.docGrpAdd2List.GetSorting()
    def getGrp(self):
        grp=[]
        dInfo={}
        if 1==0:
            for i in range(self.lstGroup.GetCount()):
                if self.lstGroup.IsChecked(i):
                    info=GROUPING_INFO[i]
                    func=GROUPING_CONV[i]
                    grp.append((info,func))
                    dInfo[GROUPING_INFO[i]]=0
        else:
            for i in self.lstGroupingIdx:
                info=GROUPING_INFO[i]
                func=GROUPING_CONV[i]
                grp.append((info,func))
                dInfo[GROUPING_INFO[i]]=0
            
        return dInfo.keys(),grp,self.lstGroupingIdx
    def getGrpNames(self):
        lst=[]
        for i in self.lstGroupingIdx:
            lst.append(self.lstGroup.GetString(i)[2:])
        return lst
    def getGrpIdxs(self):
        return self.lstGroupingIdx
    def IsAll2Show(self):
        return self.chcShowAll.GetValue()
    def IsRunning(self):
        if self.docGrpAdd2List.IsRunning():
            return True
        if self.docGrp.IsRunning():
            return True
        return False
    def OnGroupElement(self,evt):
        evt.Skip()
    def OnGroupFinished(self,evt):
        grp=[]
        i=0
        for show in SHOWING_LST:
            grp.append((show,None))
            i+=1
        self.docGrpAdd2List.setNodeInfos2Get(GROUPING_INFO)
        self.docGrpAdd2List.SetGrouping(self.docGrp.GetGrouping(),self.docGrp.GetGroupingIdx())
        self.docGrpAdd2List.setShow(grp)
        self.docGrpAdd2List.Add2List(self,self.docGrp.GetGrouped(),
                                    self.lstSortingIdx, 5, self.lstRes, self)
        if evt is not None:
            evt.Skip()
    def OnGroupAdd2ListElement(self,evt):
        evt.Skip()
    def OnGroupAdd2ListFinished(self,evt):
        evt.Skip()

    def OnChcShowAllCheckbox(self, event):
        if self.IsRunning()==False:
            self.OnGroupFinished(None)
        event.Skip()

    def OnLstGroupChecklistbox(self, event):
        idx=event.GetSelection()
        if self.lstGroup.IsChecked(idx):
            try:
                self.lstGroupingIdx.index(idx)
            except:
                self.lstGroupingIdx.append(idx)
            if idx==4:
                # this is week -> uncheck month, day
                for i in [2,3]:
                    self.lstGroup.Check(i,0)
                    try:
                        self.lstGroupingIdx.remove(i)
                    except:
                        pass
            if idx in [2,3]:
                # this is month or day -> uncheck week
                for i in [4]:
                    self.lstGroup.Check(i,0)
                    try:
                        self.lstGroupingIdx.remove(i)
                    except:
                        pass
        else:
            try:
                self.lstGroupingIdx.remove(idx)
            except:
                pass
        for idx in range(self.lstGroup.GetCount()):
            sVal=self.lstGroup.GetString(idx)
            if sVal[0]!=' ' and sVal[0]!='_':
                self.lstGroup.SetString(idx,u'_'+sVal[1:])
        i=0
        for idx in self.lstGroupingIdx:
            sVal=self.lstGroup.GetString(idx)
            self.lstGroup.SetString(idx,'%d'%i+sVal[1:])
            i+=1
        event.Skip()

    def OnLstSortChecklistbox(self, event):
        idx=event.GetSelection()
        if self.lstSort.IsChecked(idx):
            try:
                self.lstSortingIdx.index(idx)
            except:
                self.lstSortingIdx.append(idx)
        else:
            try:
                self.lstSortingIdx.remove(idx)
            except:
                pass
        for idx in range(self.lstSort.GetCount()):
            sVal=self.lstSort.GetString(idx)
            if sVal[0]!=' ' and sVal[0]!='_':
                self.lstSort.SetString(idx,u'_'+sVal[1:])
        i=0
        for idx in self.lstSortingIdx:
            sVal=self.lstSort.GetString(idx)
            self.lstSort.SetString(idx,'%d'%i+sVal[1:])
            i+=1
            
        event.Skip()
        
