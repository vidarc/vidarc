#Boa:FramePanel:vSummaryPanel
#----------------------------------------------------------------------------
# Name:         vSummaryPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vSummaryPanel.py,v 1.1 2005/12/13 13:20:08 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

[wxID_VSUMMARYPANEL] = [wx.NewId() for _init_ctrls in range(1)]

class vSummaryPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VSUMMARYPANEL, name=u'vSummaryPanel',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(400, 165),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(392, 131))

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)

        self.Move(pos)

    def SetDocs(self,doc,netHum,netLoc,netDoc,netPrjDoc,netPrj,netTask):
        self.doc=doc
        self.netHum=netHum
        self.netLoc=netLoc
        self.netPrj=netPrj
        self.netTask=netTask
        
    def UpdateInfos(self):
        pass
    
    def Clear(self):
        pass
    
    def SetGrouped(self,grp):
        pass
