#----------------------------------------------------------------------------
# Name:         vXmlPrjTimerTree.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vXmlPrjTimerTree.py,v 1.14 2012/12/23 20:21:04 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlGrpTree import *

import vidarc.tool.log.vtLog as vtLog

import string

import images


class vXmlPrjTimerTree(vtXmlGrpTree):
    def __init__(self, parent, id, pos, size, style, name,master=False,
                    controller=False,verbose=0):
        vtXmlGrpTree.__init__(self,id=id, name=name,master=master,
              controller=controller,verbose=verbose,
              parent=parent, pos=pos, size=size,style=style)
        #self.SetGrpUsrDate()
        #self.SetLblTmePrj()
        #self.grouping=[('date','YYYY'),('date','MM'),('person','')]
        #self.label=[('date',''),('starttime',''),('project',''),('person','')]
        self.groupItems={}
        self.nodeKey='|id'
        #self.SetNodeInfos({'dataCollector':['tag','name'],
        #        'prjtime':['|id','date','starttime','endtime','person','project']})
        self.SetNodeInfos(['tag','name','date','starttime','endtime','person','project']) #'|id',
        #self.SetNodeInfos(['tag','name','|id']) #'|id',
        self.tnProjTimes=None
        self.iBlockTreeSel=0
        self.bGrpMenu=True
        self.SetPossibleGrouping([
                ('normal'   , _(u'normal'), [] , 
                            {None:[('tag','',),('name','')],
                            'prjtime':[('person',''),('date','YYYY-MM-DD'),('starttime','HH:MM'),('project','')],
                            'prjtasksum':[('person',''),('date','YYYY-MM-DD'),('project','')],
                            #'group':[('name','')]
                            }),
                ('dft'   , _(u'normal'), [] , 
                            {None:[('tag','',),('name','')],
                            #'user':[('name',''),('surname',''),('firstname','')],
                            #'group':[('name','')]
                            }),
                ])
        return
        self.grpMenu=[
            ('normal',_(u'normal'),
                #[('person',''),('date','YYYY'),('date','MM'),('date','DD')],
                [],
                [('date','YYYY-MM-DD'),('starttime','HH:MM'),('project','')]
                ),
            ('dateusr',_(u'date user'),
                [('date','YYYY'),('date','MM'),('date','DD'),('person','')],
                [('starttime','HH:MM'),('project','')]
                ),
            ('dateshortuser',_(u'date short user'),
                [('date','YYYY'),('date','MM'),('person','')],
                [('date','DD'),('starttime','HH:MM'),('project','')]
                ),
            ('usrdate',_(u'user date'),
                [('person',''),('date','YYYY'),('date','MM'),('date','DD')],
                [('starttime','HH:MM'),('project','')]
                ),
            ('usrdateshort',_(u'user date short'),
                [('person',''),('date','YYYY'),('date','MM')],
                [('date','DD'),('starttime','HH:MM'),('project','')]
                ),
            ]
        self.bGrouping=True
        self.bGroupingDict=False
        self.iGrp=3
        #self.grouping={None:[],
        #        'prjtime':[('person',''),('date','YYYY'),('date','MM'),('date','DD')]}
        self.SetGrouping({None:[],
                'prjtime':[('person',''),('date','YYYY'),('date','MM'),('date','DD')],
                'prjtask':[('person',''),('date','YYYY-MM-DD'),('project','')],}
                [('tag',''),('name',''),('date','YYYY-MM-DD'),('starttime','HH:MM'),('project',''),('person','')]),
        #self.SetGroupingByNum(0)
        
    def SetLblFull(self):
        self.label=[('date','YYYY-MM-DD'),('starttime','HH:MM'),('project',''),('person','')]
    def SetLblDayTmePrj(self):
        self.label=[('date','DD'),('starttime','HH:MM'),('project','')]
    def SetLblTmePrj(self):
        self.label=[('starttime','HH:MM'),('project','')]
    def OnAddFinished(self,evt):
        vtXmlGrpTree.OnAddFinished(self,evt)
        #self.SelectLastAdded()
    def SetupImageList(self):
        if vtXmlGrpTree.SetupImageList(self):
        #self.imgDict={'elem':{},'txt':{},'cdata':{},'comm':{},'grp':{}}
        #self.imgLstTyp=wx.ImageList(16,16)
        #img=images.getElementBitmap()
        #mask=wx.Mask(img,wx.BLACK)
        #img.SetMask(mask)
        #self.imgDict['elem']['dft']=[self.imgLstTyp.Add(img)]
        
        #img=images.getElementSelBitmap()
        #mask=wx.Mask(img,wx.BLACK)
        #img.SetMask(mask)
        #self.imgDict['elem']['dft'].append(self.imgLstTyp.Add(img))
        
            img=images.getPluginBitmap()
            self.imgDict['elem']['projtimes']=[self.imgLstTyp.Add(img)]
            self.imgDict['elem']['root']=[self.imgLstTyp.Add(img)]
            imgSel=images.getPluginBitmap()
            self.imgDict['elem']['projtimes'].append(self.imgLstTyp.Add(imgSel))
            self.imgDict['elem']['root'].append(self.imgLstTyp.Add(imgSel))
            
            self.imgDict['elem']['projtimes'].append(self.imgLstTyp.Add(img))
            self.imgDict['elem']['root'].append(self.imgLstTyp.Add(img))
            self.imgDict['elem']['projtimes'].append(self.imgLstTyp.Add(imgSel))
            self.imgDict['elem']['root'].append(self.imgLstTyp.Add(imgSel))
            self.imgDict['elem']['projtimes'].append(self.imgLstTyp.Add(img))
            self.imgDict['elem']['root'].append(self.imgLstTyp.Add(img))
            self.imgDict['elem']['projtimes'].append(self.imgLstTyp.Add(imgSel))
            self.imgDict['elem']['root'].append(self.imgLstTyp.Add(imgSel))
            if 1==0:
                imgTmp=images.getPrjTimesImage()
                img=self.__convImg__(imgTmp,self.MAP_IMG_ALPHA[2])
                self.imgDict['elem']['projtimes'].append(self.imgLstTyp.Add(img))
                self.imgDict['elem']['root']=[self.imgLstTyp.Add(img)]
                imgTmp=images.getPrjTimesSelImage()
                img=self.__convImg__(imgTmp,self.MAP_IMG_ALPHA[3])
                self.imgDict['elem']['projtimes'].append(self.imgLstTyp.Add(img))
                self.imgDict['elem']['root']=[self.imgLstTyp.Add(img)]
                imgTmp=images.getPrjTimesImage()
                img=self.__convImg__(imgTmp,self.MAP_IMG_ALPHA[4])
                self.imgDict['elem']['projtimes'].append(self.imgLstTyp.Add(img))
                self.imgDict['elem']['root']=[self.imgLstTyp.Add(img)]
                imgTmp=images.getPrjTimesSelImage()
                img=self.__convImg__(imgTmp,self.MAP_IMG_ALPHA[5])
                self.imgDict['elem']['projtimes'].append(self.imgLstTyp.Add(img))
                self.imgDict['elem']['root']=[self.imgLstTyp.Add(img)]
            
        
        
            img=images.getPrjBitmap()
            #mask=wx.Mask(img,wx.BLACK)
            #img.SetMask(mask)
            self.imgDict['elem']['project']=[self.imgLstTyp.Add(img)]
            img=images.getPrjSelBitmap()
            #mask=wx.Mask(img,wx.BLUE)
            #img.SetMask(mask)
            self.imgDict['elem']['project'].append(self.imgLstTyp.Add(img))
            imgTmp=images.getPrjImage()
            img=self.__convImg__(imgTmp,self.MAP_IMG_ALPHA[2])
            self.imgDict['elem']['project'].append(self.imgLstTyp.Add(img))
            imgTmp=images.getPrjSelImage()
            img=self.__convImg__(imgTmp,self.MAP_IMG_ALPHA[3])
            self.imgDict['elem']['project'].append(self.imgLstTyp.Add(img))
            imgTmp=images.getPrjImage()
            img=self.__convImgInvalid__(imgTmp,self.MAP_IMG_ALPHA[4])
            self.imgDict['elem']['project'].append(self.imgLstTyp.Add(img))
            imgTmp=images.getPrjSelImage()
            img=self.__convImgInvalid__(imgTmp,self.MAP_IMG_ALPHA[5])
            self.imgDict['elem']['project'].append(self.imgLstTyp.Add(img))
        
            img=images.getPrjsBitmap()
            self.imgDict['elem']['projects']=[self.imgLstTyp.Add(img)]
            img=images.getPrjsSelBitmap()
            self.imgDict['elem']['projects'].append(self.imgLstTyp.Add(img))
            imgTmp=images.getPrjsImage()
            img=self.__convImg__(imgTmp,self.MAP_IMG_ALPHA[2])
            self.imgDict['elem']['projects'].append(self.imgLstTyp.Add(img))
            imgTmp=images.getPrjsSelImage()
            img=self.__convImg__(imgTmp,self.MAP_IMG_ALPHA[3])
            self.imgDict['elem']['projects'].append(self.imgLstTyp.Add(img))
            imgTmp=images.getPrjsImage()
            img=self.__convImgInvalid__(imgTmp,self.MAP_IMG_ALPHA[4])
            self.imgDict['elem']['projects'].append(self.imgLstTyp.Add(img))
            imgTmp=images.getPrjsSelImage()
            img=self.__convImgInvalid__(imgTmp,self.MAP_IMG_ALPHA[5])
            self.imgDict['elem']['projects'].append(self.imgLstTyp.Add(img))
        
            img=images.getTimeEntryBitmap()
            self.imgDict['elem']['prjtime']=[self.imgLstTyp.Add(img)]
            img=images.getTimeEntryBitmap()
            self.imgDict['elem']['prjtime'].append(self.imgLstTyp.Add(img))
            # add images for active time entrys (no end time set yet)
            img=images.getTimerBitmap()
            mask=wx.Mask(img,wx.BLUE)
            img.SetMask(mask)
            self.imgDict['elem']['prjtime'].append(self.imgLstTyp.Add(img))
            img=images.getTimerBitmap()
            mask=wx.Mask(img,wx.BLUE)
            img.SetMask(mask)
            self.imgDict['elem']['prjtime'].append(self.imgLstTyp.Add(img))
            # add images for note project time entrys (no start time set yet)
            img=images.getNoteBitmap()
            mask=wx.Mask(img,wx.BLUE)
            img.SetMask(mask)
            self.imgDict['elem']['prjtime'].append(self.imgLstTyp.Add(img))
            img=images.getNoteBitmap()
            mask=wx.Mask(img,wx.BLUE)
            img.SetMask(mask)
            self.imgDict['elem']['prjtime'].append(self.imgLstTyp.Add(img))
            return True
        return False
        #img=images.getTextBitmap()
        #img=images.getElementBitmap()
        #mask=wx.Mask(img,wx.BLUE)
        #img.SetMask(mask)
        #self.imgDict['txt']['dft']=[self.imgLstTyp.Add(img)]
        
        #img=images.getTextSelBitmap()
        #img=images.getElementSelBitmap()
        #mask=wx.Mask(img,wx.BLUE)
        #img.SetMask(mask)
        #self.imgDict['txt']['dft'].append(self.imgLstTyp.Add(img))
        
        img=images.getYearBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.imgDict['grp']['YYYY']=[self.imgLstTyp.Add(img)]
        
        img=images.getMonthBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.imgDict['grp']['MM']=[self.imgLstTyp.Add(img)]
        
        img=images.getDayBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.imgDict['grp']['DD']=[self.imgLstTyp.Add(img)]
        
        img=images.getUserBitmap()
        mask=wx.Mask(img,wx.BLACK)
        img.SetMask(mask)
        self.imgDict['grp']['person']=[self.imgLstTyp.Add(img)]
        
        #icon=images.getApplicationIcon()
        #self.SetIcon(icon)
        self.SetImageList(self.imgLstTyp)
    def __getValFormat__(self,g,val,infos=None):
        val=string.strip(val)
        if len(g[1])>0:
            sFormat=g[1]
            if g[0]=='date':
                if sFormat=='YYYY':
                    val=val[:4]
                elif sFormat=='MM':
                    val=val[4:6]
                elif sFormat=='DD':
                    val=val[6:8]
                elif sFormat=='-DD':
                    val='-'+val[6:8]
                elif sFormat=='YYYY-MM-DD':
                    val=val[:4]+'-'+val[4:6]+'-'+val[6:8]
                elif sFormat=='MM-DD':
                    val=val[4:6]+'-'+val[6:8]
            if g[0]=='starttime':
                if sFormat=='HH':
                    val=val[:2]
                elif sFormat=='MM':
                    val=val[2:4]
                elif sFormat=='SS':
                    val=val[4:6]
                elif sFormat=='HHMM':
                    val=val[:4]
                elif sFormat=='HH:MM':
                    val=val[:2]+':'+val[2:4]
            if g[0]=='endtime':
                if sFormat=='HH':
                    val=val[:2]
                elif sFormat=='MM':
                    val=val[2:4]
                elif sFormat=='SS':
                    val=val[4:6]
                elif sFormat=='HHMM':
                    val=val[:4]
                elif sFormat=='HH:MM':
                    val=val[:2]+':'+val[2:4]
                
        if len(val)<=0:
            val="---"
        return val
    def __getImgFormat__(self,g,val):
        val=string.strip(val)
        if len(g[1])>0:
            sFormat=g[1]
            if g[0]=='date':
                if sFormat=='YYYY':
                    img=self.imgDict['grp']['YYYY'][0]
                    imgSel=self.imgDict['grp']['YYYY'][0]
                    return (img,imgSel)
                elif sFormat=='MM':
                    img=self.imgDict['grp']['MM'][0]
                    imgSel=self.imgDict['grp']['MM'][0]
                    return (img,imgSel)
                elif sFormat=='DD':
                    img=self.imgDict['grp']['DD'][0]
                    imgSel=self.imgDict['grp']['DD'][0]
                    return (img,imgSel)
        if g[0]=='person':
            #if sFormat=='':
            img=self.imgDict['grp']['person'][0]
            imgSel=self.imgDict['grp']['person'][0]
            return (img,imgSel)
        try:
            img=self.imgDict['elem'][o.tagName][0]
        except:
            img=self.imgDict['elem']['dft'][0]
        try:
            imgSel=self.imgDict['elem'][o.tagName][1]
        except:
            imgSel=self.imgDict['elem']['dft'][1]
        return (img,imgSel)
    def __getImagesByInfos__(self,tagName,infos):
        # check project entry type
        #if tagName in ['projtimes']:
        #    try:
        #        img=self.imgDict['elem'][tagName][0]
        #    except:
        #        img=self.imgDict['elem']['dft'][0]
        #    try:
        #        imgSel=self.imgDict['elem'][tagName][1]
        #    except:
        #        imgSel=self.imgDict['elem']['dft'][1]
        if tagName!='prjtime':
            return vtXmlGrpTree.__getImagesByInfos__(self,tagName,infos)
        if (len(infos['endtime'])<=0) or (infos['endtime']=='--:--:--'):
            # this is a active project time entry
            try:
                img=self.imgDict['elem'][tagName][2]
            except:
                img=self.imgDict['elem']['dft'][0]
            try:
                imgSel=self.imgDict['elem'][tagName][3]
            except:
                imgSel=self.imgDict['elem']['dft'][1]
        
        elif (len(infos['starttime'])<=0) or (infos['starttime']=='--:--:--'):
            # this is a note project time entry
            try:
                img=self.imgDict['elem'][tagName][4]
            except:
                img=self.imgDict['elem']['dft'][0]
            try:
                imgSel=self.imgDict['elem'][tagName][5]
            except:
                imgSel=self.imgDict['elem']['dft'][1]
        else:
            try:
                img=self.imgDict['elem'][tagName][0]
            except:
                img=self.imgDict['elem']['dft'][0]
            try:
                imgSel=self.imgDict['elem'][tagName][1]
            except:
                imgSel=self.imgDict['elem']['dft'][1]
        return img,imgSel
    def __refreshLabel__(self,ti):
        vtXmlGrpTree.__refreshLabel__(self,ti,self.__refreshLabelFin__)
    def __refreshLabelFin__(self,ti):
        if self.TREE_DATA_ID:
            id=self.GetPyData(ti)
            if o is None:
                return
            o=self.doc.getNodeByIdNum(id)
        else:
            o=self.GetPyData(ti)
        if o is None:
            return
        self.doc.acquire()
        tagName,infos=self.doc.getNodeInfos(o,self.lang)
        img,imgSel=self.__getImagesByInfos__(tagName,infos)
        self.SetItemImage(ti,img,wx.TreeItemIcon_Normal)
        self.SetItemImage(ti,imgSel,wx.TreeItemIcon_Expanded)
        self.SetItemImage(ti,imgSel,wx.TreeItemIcon_Selected)
        self.doc.release()
        return ti
    #def OnGotContent(self,evt):
    #    self.SetNode(self.doc.getChild(None,'projtimes'))
    #    evt.Skip()
    def Clear(self):
        #vtLog.vtLogCallDepth(None,'',verbose=2)
        self.tiLastPrjTime=None
        vtXmlGrpTree.Clear(self)
    def SetNode(self,node):
        self.tiLastPrjTime=None
        vtXmlGrpTree.SetNode(self,node)
    def OnAddElementsFinOlk(self,evt):
        #idPar=self.doc.getKey(self.rootNode)
        #if self.groupItems.has_key(idPar):
        #    self.__sortAllGrpItems__(self.groupItems[idPar])
        #self.SortChildren(self.tiRoot)
        #self.tnProjTimes=self.thdAddElements.lastTreeItem
        #if self.thdAddElements.lastTreeItem is not None:
        #    self.SelectItem(self.thdAddElements.lastTreeItem)
        #self.Expand(self.tiRoot)
        evt.Skip()

    def OnTcNodeRightDownD(self, event):
        self.x=event.GetX()
        self.y=event.GetY()
        if not hasattr(self,'popupIdASort'):
            self.popupIdSort=wx.NewId()
            #self.popupIdAddUsr=wx.NewId()
            #EVT_MENU(self,self.popupIdAddUsr,self.OnTreeAddUsr)
            EVT_MENU(self,self.popupIdSort,self.OnTreeSort)
        menu=wx.Menu()
        #menu.Append(self.popupIdAddUsr,'Add User')
        menu.Append(self.popupIdSort,'Sort')
        
        self.PopupMenu(menu,wx.Point(self.x, self.y))
        menu.Destroy()
        event.Skip()
    #def __addMenuEdit__(self,menu):
    #    return False
    def RefreshSelected(self):
        self.iBlockTreeSel=1
        vtXmlGrpTree.RefreshSelected(self)
        wx.PostEvent(self,vtXmlTreeItemEdited(self,
                self.triSelected,self.objTreeItemSel))  # forward double click event
        self.iBlockTreeSel=0
    def AddNode(self,node):
        vtLog.vtLngCurWX(vtLog.CRITICAL,'FIXME: should not be called'%(),self)
        return
        tri=self.__addElement__(self.tiRoot,node,bRet=True)
        self.triSelected=tri
        self.objTreeItemSel=node
        #self.SortChildren(self.GetItemParent(self.triSelected))
        self.SelectItem(tri)
        wx.PostEvent(self,vtXmlTreeItemEdited(self,
                self.triSelected,self.objTreeItemSel))  # forward double click event
    def OnTreeSort(self,event):
        try:
            self.SortChildren(self.GetItemParent(self.triSelected))
        except:
            pass
        pass
    def AddProjectTimerNode(self,node):
        vtLog.vtLngCurWX(vtLog.CRITICAL,'FIXME: should not be called'%(),self)
        return
        if self.doc is None:
            return
        prj=self.doc.createSubNode(node,'projecttimer')
        self.doc.setNodeText(prj,'name','???')
        self.doc.setNodeText(prj,'longname','???')
        self.doc.setNodeText(prj,'description','???')
        self.doc.setNodeText(prj,'startdatetime','???')
        self.doc.setNodeText(prj,'enddatetime','???')
        # append attributes
        prjAttrs=self.doc.createSubNode(node,'attributes')
        for attr in ['manager','client','prjnumber']:
            prjAttr=doc.setNodeText(prjAttrs,attr,'---')
        
        # update tree
        triRoot=self.GetRootItem()
        triChild=self.GetFirstChild(triRoot)
        if self.GetItemText(triChild[0])=='projects':
            bFound=True
            triPar=triChild[0]
        else:
            bFound=False
        while bFound==False:
            triChild=self.GetNextChild(triChild[0],triChild[1])
            if self.GetItemText(triChild[0])=='projects':
                bFound=True
                triPar=triChild[0]
            if triChild[0].IsOk()==False:
                if bFound==False:
                    return -1
                
        self.DeleteChildren(triPar)
        self.__addElements__(triPar,node)
        self.Expand(triPar)
        wx.PostEvent(self,vtXmlTreeItemAdded(self,
                            triPar,node))
        return 0
    def __cleanUpXmlNodes__(self,ti):
        if self.GetChildrenCount(ti)>0:
            cid=self.GetFirstChild(ti)
            bFound=cid[0].IsOk()
            while bFound:
                self.__cleanUpXmlNodes__(cid[0])
                cid=self.GetNextChild(ti,cid[1])
                bFound=cid[0].IsOk()
            #self.__cleanUpXmlNodes__(self.triSelected)
        else:
            td=self.GetItemData(ti)
            o=td.GetData()
            
            #if o is not None:
            #    o.parentNode.removeChild(o)
    def DeleteNode(self):
        vtLog.vtLngCurWX(vtLog.CRITICAL,'FIXME: should not be called'%(),self)
        return
        if self.triSelected is not None:
            #self.__cleanUpXmlNodes__(self.triSelected)
            self.doc.delNode(self.objTreeItemSel)
            if self.tiCache is not None:
                self.tiCache.DelID(self.doc.getKey(self.objTreeItemSel))
            self.Delete(self.triSelected)
            
            ti=self.GetSelection()
            tic=self.GetLastChild(ti)
            try:
                if tic is not None:
                    self.SelectItem(tic)
                self.objTreeItemSel=self.GetPyData(tic)
                self.triSelected=tic
            except:
                self.objTreeItemSel=self.GetPyData(ti)
                self.triSelected=None
