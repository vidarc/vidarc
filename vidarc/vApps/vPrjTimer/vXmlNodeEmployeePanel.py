#Boa:FramePanel:vXmlNodeEmployeePanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeEmployyePanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20061002
# CVS-ID:       $Id: vXmlNodeEmployeePanel.py,v 1.4 2010/03/29 08:54:44 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.vApps.vHum.vXmlHumInputTreeUsr

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import sys

    from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.art.vtArt as vtArt
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    import vidarc.config.vcLog as vcLog
    VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

[wxID_VXMLNODEEMPLOYEEPANEL, wxID_VXMLNODEEMPLOYEEPANELLBLHUM, 
 wxID_VXMLNODEEMPLOYEEPANELVIUSR, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vXmlNodeEmployeePanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblHum, (0, 0), border=0, flag=0, span=(1, 1))
        parent.AddWindow(self.viUsr, (0, 1), border=0, flag=wx.EXPAND, span=(1,
              1))

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=4, vgap=4)

        self._init_coll_gbsData_Items(self.gbsData)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEEMPLOYEEPANEL,
              name=u'vXmlNodeEmployeePanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(312, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 180))
        self.SetAutoLayout(True)

        self.lblHum = wx.StaticText(id=wxID_VXMLNODEEMPLOYEEPANELLBLHUM,
              label=u'employee', name=u'lblHum', parent=self, pos=wx.Point(0,
              0), size=wx.Size(46, 13), style=wx.ALIGN_RIGHT)
        self.lblHum.SetMinSize(wx.Size(-1, -1))

        self.viUsr = vidarc.vApps.vHum.vXmlHumInputTreeUsr.vXmlHumInputTreeUsr(id=wxID_VXMLNODEEMPLOYEEPANELVIUSR,
              name=u'viUsr', parent=self, pos=wx.Point(50, 0), size=wx.Size(94,
              24), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vPrjTimer')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        
        self.viUsr.SetTagNames('tag','name')
        self.viUsr.SetAppl('vHum')
        
        self.gbsData.AddGrowableCol(1)
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def __Clear__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        try:
            # add code here
            self.viUsr.Clear()
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def SetNetDocs(self,d):
        self.__logCritical__('FIXME'%())
        return
        # add code here
        if d.has_key('vHum'):
            dd=d['vHum']
            self.viUsr.SetDocTree(dd['doc'],dd['bNet'])
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viUsr.SetDoc(doc)
        if dDocs is not None:
            if 'vHum' in dDocs:
                dd=dDocs['vHum']
                self.viUsr.SetDocTree(dd['doc'],dd['bNet'])
    def __SetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            fid=self.objRegNode.GetEmployeeID(self.node)
            sName=self.objRegNode.GetEmployee(self.node)
            self.viUsr.SetValueID(sName,fid)
            #oTrans=self.doc.GetRegisteredNode(self.objRegNode.GetTagNameTransition())
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            # add code here
            fid=self.viUsr.GetID()
            self.objRegNode.SetEmployeeID(node,fid)
            sName=self.viUsr.GetValue()
            self.objRegNode.SetEmployee(node,sName)
        except:
            self.__logTB__()
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        self.viUsr.Close()
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
        pass
    def __Lock__(self,flag):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if flag:
            # add code here
            self.viUsr.Enable(False)
        else:
            # add code here
            self.viUsr.Enable(True)
