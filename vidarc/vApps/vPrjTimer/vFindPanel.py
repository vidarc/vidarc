#Boa:FramePanel:vFindPanel
#----------------------------------------------------------------------------
# Name:         vFindDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vFindPanel.py,v 1.2 2006/01/17 12:17:47 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

from vidarc.tool.time.vTimeDateGM import *
from vidarc.tool.time.vTimeTimeEdit import *
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.xml.vtXmlFilter as vtXmlFilter

import thread,time,fnmatch,sys,types

import images

VERBOSE=0


def create(parent):
    return vFindPanel(parent)

[wxID_VFINDPANEL, wxID_VFINDPANELCHCCLIENT, wxID_VFINDPANELCHCLOC, 
 wxID_VFINDPANELCHCPRJ, wxID_VFINDPANELCHCUSR, wxID_VFINDPANELLBLCLIENT, 
 wxID_VFINDPANELLBLCOUNT, wxID_VFINDPANELLBLENDDATE, 
 wxID_VFINDPANELLBLENDTIME, wxID_VFINDPANELLBLFOUND, wxID_VFINDPANELLBLLOC, 
 wxID_VFINDPANELLBLPRJ, wxID_VFINDPANELLBLSTARTDATE, 
 wxID_VFINDPANELLBLSTARTTIME, wxID_VFINDPANELLBLTASKS, wxID_VFINDPANELLBLUSR, 
 wxID_VFINDPANELLSTTASKS, wxID_VFINDPANELTXTCOUNT, wxID_VFINDPANELTXTFOUND, 
] = [wx.NewId() for _init_ctrls in range(19)]

GROUPING_LST=[u'year', u'month', u'day',
                        u'week', u'project', u'task', 
                        u'client', u'location']
GROUPING_DICT={
    u'year':'',
    u'month':'', 
    u'day':'',
    u'week':'', 
    u'project':'', 
    u'task':'', 
    u'client':'', 
    u'location':''}

class vFindPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VFINDPANEL, name=u'vFindPanel',
              parent=prnt, pos=wx.Point(0, 0), size=wx.Size(536, 355),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(528, 321))

        self.lblClient = wx.StaticText(id=wxID_VFINDPANELLBLCLIENT,
              label=u'Client', name=u'lblClient', parent=self, pos=wx.Point(8,
              8), size=wx.Size(26, 13), style=0)

        self.chcClient = wx.Choice(choices=[], id=wxID_VFINDPANELCHCCLIENT,
              name=u'chcClient', parent=self, pos=wx.Point(40, 4),
              size=wx.Size(64, 21), style=0)
        self.chcClient.Bind(wx.EVT_CHOICE, self.OnChcClientChoice,
              id=wxID_VFINDPANELCHCCLIENT)

        self.lblPrj = wx.StaticText(id=wxID_VFINDPANELLBLPRJ, label=u'Project',
              name=u'lblPrj', parent=self, pos=wx.Point(112, 8),
              size=wx.Size(33, 13), style=0)

        self.chcPrj = wx.Choice(choices=[], id=wxID_VFINDPANELCHCPRJ,
              name=u'chcPrj', parent=self, pos=wx.Point(152, 4),
              size=wx.Size(64, 21), style=0)
        self.chcPrj.Bind(wx.EVT_CHOICE, self.OnChcPrjChoice,
              id=wxID_VFINDPANELCHCPRJ)

        self.lblUsr = wx.StaticText(id=wxID_VFINDPANELLBLUSR, label=u'User',
              name=u'lblUsr', parent=self, pos=wx.Point(224, 8),
              size=wx.Size(22, 13), style=0)

        self.chcUsr = wx.Choice(choices=[], id=wxID_VFINDPANELCHCUSR,
              name=u'chcUsr', parent=self, pos=wx.Point(256, 4),
              size=wx.Size(72, 21), style=0)
        self.chcUsr.Bind(wx.EVT_CHOICE, self.OnChcUsrChoice,
              id=wxID_VFINDPANELCHCUSR)

        self.lblLoc = wx.StaticText(id=wxID_VFINDPANELLBLLOC, label=u'Location',
              name=u'lblLoc', parent=self, pos=wx.Point(344, 8),
              size=wx.Size(41, 13), style=0)

        self.chcLoc = wx.Choice(choices=[], id=wxID_VFINDPANELCHCLOC,
              name=u'chcLoc', parent=self, pos=wx.Point(392, 4),
              size=wx.Size(130, 21), style=0)
        self.chcLoc.Bind(wx.EVT_CHOICE, self.OnChcLocChoice,
              id=wxID_VFINDPANELCHCLOC)

        self.lblTasks = wx.StaticText(id=wxID_VFINDPANELLBLTASKS,
              label=u'Tasks', name=u'lblTasks', parent=self, pos=wx.Point(16,
              64), size=wx.Size(29, 13), style=0)

        self.lstTasks = wx.ListCtrl(id=wxID_VFINDPANELLSTTASKS,
              name=u'lstTasks', parent=self, pos=wx.Point(8, 80),
              size=wx.Size(272, 208), style=wx.LC_REPORT|wx.LC_SORT_ASCENDING)
        self.lstTasks.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstTasksListItemDeselected, id=wxID_VFINDPANELLSTTASKS)
        self.lstTasks.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstTasksListItemSelected, id=wxID_VFINDPANELLSTTASKS)

        self.lblStartDate = wx.StaticText(id=wxID_VFINDPANELLBLSTARTDATE,
              label=u'Start Date', name=u'lblStartDate', parent=self,
              pos=wx.Point(8, 44), size=wx.Size(48, 13), style=0)

        self.lblEndDate = wx.StaticText(id=wxID_VFINDPANELLBLENDDATE,
              label=u'EndDate', name=u'lblEndDate', parent=self,
              pos=wx.Point(140, 44), size=wx.Size(42, 13), style=0)

        self.lblStartTime = wx.StaticText(id=wxID_VFINDPANELLBLSTARTTIME,
              label=u'Start Time', name=u'lblStartTime', parent=self,
              pos=wx.Point(276, 44), size=wx.Size(48, 13), style=0)

        self.lblEndTime = wx.StaticText(id=wxID_VFINDPANELLBLENDTIME,
              label=u'End Time', name=u'lblEndTime', parent=self,
              pos=wx.Point(410, 44), size=wx.Size(45, 13), style=0)

        self.lblCount = wx.StaticText(id=wxID_VFINDPANELLBLCOUNT,
              label=u'Count', name=u'lblCount', parent=self, pos=wx.Point(288,
              88), size=wx.Size(28, 13), style=0)

        self.txtCount = wx.TextCtrl(id=wxID_VFINDPANELTXTCOUNT,
              name=u'txtCount', parent=self, pos=wx.Point(328, 84),
              size=wx.Size(64, 21), style=0, value=u'')

        self.lblFound = wx.StaticText(id=wxID_VFINDPANELLBLFOUND,
              label=u'Found', name=u'lblFound', parent=self, pos=wx.Point(288,
              120), size=wx.Size(30, 13), style=0)

        self.txtFound = wx.TextCtrl(id=wxID_VFINDPANELTXTFOUND,
              name=u'txtFound', parent=self, pos=wx.Point(328, 116),
              size=wx.Size(64, 21), style=0, value=u'')

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        self.verbose=VERBOSE
        
        self.docFilter=vtXmlFilter.vtXmlFilter()
        vtXmlFilter.EVT_THREAD_FILTER_ELEMENTS(self,self.OnFilterElement)
        vtXmlFilter.EVT_THREAD_FILTER_ELEMENTS_FINISHED(self,self.OnFilterFinished)
        
        self.doc=None
        self.netHum=None
        self.netLoc=None
        self.netTask=None
        self.netPrj=None
        
        zNow=time.localtime(time.time())
        zStartDate='%04d%02d%02d'%(zNow[0],1,1)
        zEndDate='%04d%02d%02d'%(zNow[0],12,31)
        
        id=wx.NewId()
        self.dteStartDate=vTimeDateGM(self,id,
                pos=(60,40),size=(75,22),style=0,name="tmeStartDate")
        
        id=wx.NewId()
        self.dteEndDate=vTimeDateGM(self,id,
                pos=(190,40),size=(75,22),style=0,name="tmeEndDate")
        
        id=wx.NewId()
        self.tmeStart=vTimeTimeEdit(self,id,
                pos=(332,40),size=(70,22),style=0,name="tmeStartTime")
        
        id=wx.NewId()
        self.tmeEnd=vTimeTimeEdit(self,id,
                pos=(465,40),size=(70,22),style=0,name="tmeEndTime")
        
        self.dteStartDate.SetValue(zStartDate)
        self.dteEndDate.SetValue(zEndDate)
        self.tmeStart.SetTimeStr('000000')
        self.tmeEnd.SetTimeStr('240000')
        
        self.txtCount.SetEditable(False)
        self.txtFound.SetEditable(False)
        
        self.lstTasks.InsertColumn(0,u'Task',wx.LIST_FORMAT_LEFT,250)
        
        self.Move(pos)
    def GetStartDate(self):
        return self.dteStartDate.GetValue()
    def GetEndDate(self):
        return self.dteEndDate.GetValue()
    def SetDocs(self,doc,netHum,netLoc,netDoc,netPrjDoc,netPrj,netTask):
        self.doc=doc
        self.netHum=netHum
        self.netLoc=netLoc
        self.netPrj=netPrj
        self.netTask=netTask
    def UpdateInfos(self):
        self.txtCount.SetValue('%12d'%-1)
        self.txtFound.SetValue('%12d'%-1)
        
        self.chcPrj.Clear()
        self.chcClient.Clear()
        self.chcUsr.Clear()
        self.chcLoc.Clear()
        self.lstTasks.DeleteAllItems()
        
        self.chcClient.Append(u'*')
        self.chcPrj.Append(u'*')
        self.chcUsr.Append(u'*')
        self.chcLoc.Append(u'*')
        self.chcPrj.SetClientData(0,-1)
        
        self.chcClient.SetSelection(0)
        self.chcPrj.SetSelection(0)
        self.chcUsr.SetSelection(0)
        self.chcLoc.SetSelection(0)
        
        if self.netHum is not None:
            ids=self.netHum.GetSortedUsrIds()
            for k in ids.GetKeys():
                self.chcUsr.Append(k)
        if self.netLoc is not None:
            ids=self.netLoc.GetSortedIds()
            for k in ids.GetKeys():
                self.chcLoc.Append(k)
        if self.netPrj is not None:
            ids=self.netPrj.GetSortedIds()
            i=0
            for k in ids.GetKeys():
                self.chcPrj.Append(k)
                self.chcPrj.SetClientData(i+1,i)
                i+=1
        if self.netPrj is not None:
            ids=self.netPrj.GetSortedCltLongIds()
            i=0
            for k in ids.GetKeys():
                self.chcClient.Append(k)
                i+=1
    def IsRunning(self):
        return self.thdAnalyse.IsRunning()
        
    def OnChcPrjChoice(self, event):
        self.__generateTasks__()
        event.Skip()

    def OnChcUsrChoice(self, event):
        event.Skip()

    def OnChcLocChoice(self, event):
        event.Skip()

    def OnLstTasksListItemDeselected(self, event):
        event.Skip()

    def OnLstTasksListItemSelected(self, event):
        event.Skip()

    def OnCbOkButton(self, event):
        self.thdAnalyse.Stop()
        while self.thdAnalyse.IsRunning():
            time.sleep(1)
        self.Show(False)
        #self.EndModal(1)
        event.Skip()
    def OnChcClientChoice(self, event):
        idx=self.chcClient.GetSelection()
        if idx<0:
            return
        self.chcPrj.Clear()
        self.chcPrj.Append(u'*')
        self.chcPrj.SetClientData(0,-1)
        self.chcPrj.SetSelection(0)
        sName=self.chcClient.GetStringSelection()
        if self.netPrj is not None:
            ids=self.netPrj.GetSortedIds()
            i=0
            j=0
            for k in ids.GetKeys():
                tup=ids.GetInfoByIdx(i)
                attr=self.netPrj.getChild(tup[0],'attributes')
                sClt=self.netPrj.getNodeText(attr,'client')
                if fnmatch.fnmatch(sClt,sName):
                    self.chcPrj.Append(k)
                    j+=1
                    self.chcPrj.SetClientData(j,i)
                i+=1
        event.Skip()
    def __generateTasks__(self):
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        if self.netTask is None:
            return
        prjId=self.getPrjId()
        self.lstTasks.DeleteAllItems()
        prjTasks=self.netTask.getChilds(self.netTask.getBaseNode(),'prjtasks')
        for prj in prjTasks:
            if self.netTask.getAttribute(prj,'fid')!=prjId:
                continue
            dictTask={}
            for task in self.netTask.getChilds(prj,'task'):
                sTask=self.netTask.getNodeText(task,'name')
                dictSubTask={}
                for subTask in self.netTask.getChilds(task,'subtask'):
                    dictSubTask[self.doc.getAttribute(subTask,'fid')]=(
                        self.doc.getNodeText(subTask,'name'),subTask)
                dictTask[self.doc.getAttribute(task,'fid')]=(
                    self.doc.getNodeText(task,'name'),task,dictSubTask)
                    
            lst=[]
            for k in dictTask.keys():
                v=dictTask[k]
                sTask=v[0]
                lst.append((sTask,k))
                for kk in v[2].keys():
                    vv=v[2][kk]
                    lst.append((sTask+':'+vv[0],kk))
                    
            def compFunc(a,b):
                return cmp(a[0],b[0])
            lst.sort(compFunc)
            for it in lst:
                index = self.lstTasks.InsertImageStringItem(sys.maxint, it[0],-1)
                self.lstTasks.SetItemData(index,long(it[1]))
                
    def getTasks(self):
        lst=[]
        iCount=self.lstTasks.GetItemCount()
        idx=0
        while idx<iCount:
            iState=self.lstTasks.GetItemState(idx,wx.LIST_STATE_SELECTED)
            if iState==wx.LIST_STATE_SELECTED:
                id=self.lstTasks.GetItemData(idx)
                lst.append(id)
            idx+=1
        return lst

    def getPrjId(self):
        idx=self.chcPrj.GetSelection()
        if idx<1:
            return -1
        i=self.chcPrj.GetClientData(idx)
        ids=self.netPrj.GetSortedIds()
        tup=ids.GetInfoByIdx(i)
        return tup[2]
    def getUsrId(self):
        idx=self.chcUsr.GetSelection()
        if idx<1:
            return -1
        i=self.chcPrj.GetClientData(idx)
        ids=self.netHum.GetSortedUsrIds()
        tup=ids.GetInfoByIdx(i)
        return tup[2]
        
    def getLocId(self):
        idx=self.chcLoc.GetSelection()
        if idx<1:
            return -1
        i=self.chcLoc.GetClientData(idx)
        ids=self.netLoc.GetSortedIds()
        tup=ids.GetInfoByIdx(i)
        return tup[2]
    
    def getGrp(self):
        grp=[]
        for i in range(self.lstGroup.GetCount()):
            if self.lstGroup.IsChecked(i):
                grp.append(i)
        return grp
    def getGrpInfos(self):
        grp=[]
        for i in range(self.lstGroup.GetCount()):
            if self.lstGroup.IsChecked(i):
                try:
                    s=[i]
                    
                    grp.append(s)
                except:
                    pass
        return grp
    def Find(self):
        self.txtCount.SetValue('%12d'%self.doc.GetElementAttrCount())
        self.txtFound.SetValue('%12d'%-1)
        
        def __checkDate__(infos,filters,key):
            tup=filters[key]
            if tup[0]<=infos[key] and infos[key]<=tup[1]:
                return True
            else:
                return False
        def __checkTime__(infos,filters,key):
            if infos['starttime']=='--:--:--':
                return False
            return True
        def __checkTask__(infos,filters,key):
            try:
                filters[key].index(long(infos['task|fid']))
                return True
            except:
                return False
            return False
        
        prjId=self.getPrjId()
        usrId=self.getUsrId()
        locId=self.getLocId()
        dFilter={}
        dCmp={}
        lstInfo=['|id']
        if prjId!=-1:
            lstInfo.append('project|fid')
            dFilter['project|fid']=[prjId]
        if usrId!=-1:
            lstInfo.append('person|fid')
            dFilter['person|fid']=[usrId]
        if locId!=-1:
            lstInfo.append('loc|fid')
            dFilter['loc|fid']=[locId]
        lstIds=self.getTasks()
        if len(lstIds)>0:
            lstInfo.append('task|fid')
            dFilter['task|fid']=lstIds
            dCmp['task|fid']=__checkTask__
        #'task|id','date','starttime','endtime'
        lstInfo.append('date')
        dFilter['date']=[self.dteStartDate.GetValue(),self.dteEndDate.GetValue()]
        dCmp['date']=__checkDate__
        
        lstInfo.append('starttime')
        dFilter['starttime']=[self.tmeStart.GetTimeStr(),self.tmeEnd.GetTimeStr()]
        dCmp['starttime']=__checkTime__
        
        lstInfo.append('endtime')
        #dFilter['endtime']
        #dCmp['endtime']=self.__checkTime__
        
        lst=self.getTasks()
        if self.verbose:
            vtLog.vtLogCallDepth(self,'prjid:%s usrid:%s locid:%s'%(prjId,usrId,locId))
            vtLog.vtLogCallDepth(self,'tasks:%d  %s'%(len(lst),lst),callstack=False)
            
        d={}
        d['hum']=self.netHum
        d['loc']=self.netLoc
        d['prj']=self.netPrj
        d['task']=self.netTask
        d['timer']=self.doc
        
        self.docFilter.setNodeInfos2Get(lstInfo)
        self.docFilter.setFilter(dFilter,dCmp)
        self.docFilter.Search(self.doc.getBaseNode(),self)
        
        #self.thdAnalyse.Find(d,prjId,usrId,locId,lst)
    def GetFound(self):
        return self.docFilter.GetFound()
    def OnFilterElement(self,evt):
        evt.Skip()
    def OnFilterFinished(self,evt):
        if self.verbose:
            vtLog.vtLogCallDepth(self,'found:%d'%self.docFilter.GetFoundCount())
        self.txtFound.SetValue('%12d'%self.docFilter.GetFoundCount())
        evt.Skip()
