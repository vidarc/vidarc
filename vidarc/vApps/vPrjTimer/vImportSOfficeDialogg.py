#Boa:Dialog:vImportSOfficeDialog
#----------------------------------------------------------------------------
# Name:         vImportSOfficeDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vImportSOfficeDialogg.py,v 1.2 2006/01/17 12:17:47 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.intctrl
import wx.lib.buttons
import sys
#from xml.dom.minidom import getDOMImplementation,parse,parseString,Element
#import boa.apps.xmlBase.vtXmlDomTree as vtXmlDomTree
import os,string
from vidarc.tool.office.vOfficeSCalc import *

import images

def create(parent):
    return vImportSOfficeDialog(parent)

[wxID_VIMPORTSOFFICEDIALOG, wxID_VIMPORTSOFFICEDIALOGGCBBBROWSEFN, 
 wxID_VIMPORTSOFFICEDIALOGGCBBBUILD, wxID_VIMPORTSOFFICEDIALOGGCBBCANCEL, 
 wxID_VIMPORTSOFFICEDIALOGGCBBIMPORT, wxID_VIMPORTSOFFICEDIALOGGCBBLINK, 
 wxID_VIMPORTSOFFICEDIALOGGCBBOK, wxID_VIMPORTSOFFICEDIALOGINTCOLEND, 
 wxID_VIMPORTSOFFICEDIALOGINTCOLSTART, wxID_VIMPORTSOFFICEDIALOGINTROWSTART, 
 wxID_VIMPORTSOFFICEDIALOGINTROWSTOP, wxID_VIMPORTSOFFICEDIALOGLBLCOL, 
 wxID_VIMPORTSOFFICEDIALOGLBLROW, wxID_VIMPORTSOFFICEDIALOGLSTELEMENT, 
 wxID_VIMPORTSOFFICEDIALOGLSTMAP, wxID_VIMPORTSOFFICEDIALOGTXTFN, 
 wxID_VIMPORTSOFFICEDIALOGTXTMAP, 
] = [wx.NewId() for _init_ctrls in range(17)]

class vImportSOfficeDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VIMPORTSOFFICEDIALOG,
              name=u'vImportSOfficeDialog', parent=prnt, pos=wx.Point(152, 47),
              size=wx.Size(551, 437), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vgd VSAS Import SOffice')
        self.SetClientSize(wx.Size(543, 410))

        self.lstElement = wx.ListView(id=wxID_VIMPORTSOFFICEDIALOGLSTELEMENT,
              name=u'lstElement', parent=self, pos=wx.Point(16, 104),
              size=wx.Size(264, 206), style=wx.LC_REPORT)

        self.gcbbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VIMPORTSOFFICEDIALOGGCBBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'gcbbOk',
              parent=self, pos=wx.Point(180, 376), size=wx.Size(76, 30),
              style=0)
        self.gcbbOk.Bind(wx.EVT_BUTTON, self.OnGcbbOkButton,
              id=wxID_VIMPORTSOFFICEDIALOGGCBBOK)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VIMPORTSOFFICEDIALOGGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(288, 376),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VIMPORTSOFFICEDIALOGGCBBCANCEL)

        self.txtFN = wx.TextCtrl(id=wxID_VIMPORTSOFFICEDIALOGTXTFN,
              name=u'txtFN', parent=self, pos=wx.Point(48, 8), size=wx.Size(384,
              21), style=0, value=u'')

        self.gcbbBrowseFN = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VIMPORTSOFFICEDIALOGGCBBBROWSEFN,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbBrowseFN', parent=self, pos=wx.Point(440, 4),
              size=wx.Size(76, 30), style=0)
        self.gcbbBrowseFN.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseFNButton,
              id=wxID_VIMPORTSOFFICEDIALOGGCBBBROWSEFN)

        self.intRowStart = wx.lib.intctrl.IntCtrl(allow_long=False,
              allow_none=False, default_color=wx.BLACK,
              id=wxID_VIMPORTSOFFICEDIALOGINTROWSTART, limited=False, max=None,
              min=None, name=u'intRowStart', oob_color=wx.RED, parent=self,
              pos=wx.Point(48, 40), size=wx.Size(100, 21), style=0, value=0)

        self.lblRow = wx.StaticText(id=wxID_VIMPORTSOFFICEDIALOGLBLROW,
              label=u'Row', name=u'lblRow', parent=self, pos=wx.Point(8, 40),
              size=wx.Size(22, 13), style=0)

        self.intRowStop = wx.lib.intctrl.IntCtrl(allow_long=False,
              allow_none=False, default_color=wx.BLACK,
              id=wxID_VIMPORTSOFFICEDIALOGINTROWSTOP, limited=False, max=None,
              min=None, name=u'intRowStop', oob_color=wx.RED, parent=self,
              pos=wx.Point(160, 40), size=wx.Size(100, 21), style=0, value=0)

        self.lblCol = wx.StaticText(id=wxID_VIMPORTSOFFICEDIALOGLBLCOL,
              label=u'Col', name=u'lblCol', parent=self, pos=wx.Point(8, 64),
              size=wx.Size(15, 13), style=0)

        self.intColStart = wx.lib.intctrl.IntCtrl(allow_long=False,
              allow_none=False, default_color=wx.BLACK,
              id=wxID_VIMPORTSOFFICEDIALOGINTCOLSTART, limited=False, max=None,
              min=None, name=u'intColStart', oob_color=wx.RED, parent=self,
              pos=wx.Point(48, 65), size=wx.Size(100, 21), style=0, value=0)

        self.intColEnd = wx.lib.intctrl.IntCtrl(allow_long=False,
              allow_none=False, default_color=wx.BLACK,
              id=wxID_VIMPORTSOFFICEDIALOGINTCOLEND, limited=False, max=None,
              min=None, name=u'intColEnd', oob_color=wx.RED, parent=self,
              pos=wx.Point(160, 65), size=wx.Size(100, 21), style=0, value=0)

        self.gcbbImport = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VIMPORTSOFFICEDIALOGGCBBIMPORT,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Import',
              name=u'gcbbImport', parent=self, pos=wx.Point(280, 40),
              size=wx.Size(76, 30), style=0)
        self.gcbbImport.Bind(wx.EVT_BUTTON, self.OnGcbbImportButton,
              id=wxID_VIMPORTSOFFICEDIALOGGCBBIMPORT)

        self.lstMap = wx.ListCtrl(id=wxID_VIMPORTSOFFICEDIALOGLSTMAP,
              name=u'lstMap', parent=self, pos=wx.Point(296, 104),
              size=wx.Size(240, 176), style=wx.LC_REPORT)
        self.lstMap.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstMapListItemSelected,
              id=wxID_VIMPORTSOFFICEDIALOGLSTMAP)

        self.txtMap = wx.TextCtrl(id=wxID_VIMPORTSOFFICEDIALOGTXTMAP,
              name=u'txtMap', parent=self, pos=wx.Point(408, 288),
              size=wx.Size(100, 21), style=0, value=u'')

        self.gcbbLink = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VIMPORTSOFFICEDIALOGGCBBLINK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Link', name=u'gcbbLink',
              parent=self, pos=wx.Point(416, 320), size=wx.Size(76, 30),
              style=0)
        self.gcbbLink.Bind(wx.EVT_BUTTON, self.OnGcbbLinkButton,
              id=wxID_VIMPORTSOFFICEDIALOGGCBBLINK)

        self.gcbbBuild = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VIMPORTSOFFICEDIALOGGCBBBUILD,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Build', name=u'gcbbBuild',
              parent=self, pos=wx.Point(280, 72), size=wx.Size(76, 30),
              style=0)
        self.gcbbBuild.Bind(wx.EVT_BUTTON, self.OnGcbbBuildButton,
              id=wxID_VIMPORTSOFFICEDIALOGGCBBBUILD)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.idxMap=-1
        
        self.lstElement.InsertColumn(0,u'Name',wx.LIST_FORMAT_LEFT,20)
        self.lstElement.InsertColumn(1,u'Type',wx.LIST_FORMAT_LEFT,180)
        
        self.lstMap.InsertColumn(0,u'Name',wx.LIST_FORMAT_LEFT,100)
        self.lstMap.InsertColumn(1,u'Col',wx.LIST_FORMAT_LEFT,100)
        
        img=images.getApplyBitmap()
        self.gcbbOk.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
        
        img=images.getBrowseBitmap()
        self.gcbbBrowseFN.SetBitmapLabel(img)
        
        img=images.getImportBitmap()
        self.gcbbImport.SetBitmapLabel(img)
        
        img=images.getLinkBitmap()
        self.gcbbLink.SetBitmapLabel(img)
        
        img=images.getBuildBitmap()
        self.gcbbBuild.SetBitmapLabel(img)
        
    def __updateElem__(self):
        self.lstElement.DeleteAllItems()
        for e in ELEMENTS:
            try:
                index = self.lstElement.InsertImageStringItem(sys.maxint, e[0], -1)
                self.lstElement.SetStringItem(index,1,e[1]['__type'],-1)
            except:
                pass
    def GetFilter(self):
        lst=[]
        for i in range(0,self.lstElement.GetItemCount()):
            if self.lstElement.GetItemState(i,wx.LIST_STATE_SELECTED)>0:
                it=self.lstElement.GetItem(i,0)
                lst.append(it.m_text)
        return lst
    def __getTag__(self,base,node,lst):
        childs=vtXmlDomTree.getChilds(node)
        for c in childs:
            if c.nodeType==Element.ELEMENT_NODE:
                if len(base)>0:
                    s=base+'>'+c.tagName
                else:
                    s=c.tagName
                lst.append((s,c))
                self.__getTag__(s,c,lst)
    def Set(self,node):
        lst=[]
        self.lstNewNodes=[]
        self.__getTag__('',node,lst)
        self.node=node
        for item in lst:
            index = self.lstMap.InsertImageStringItem(sys.maxint, item[0], -1)
            self.lstMap.SetStringItem(index,1,'',-1)
            #it=self.lstElement.GetItem(i,0)
            #try:
            #    lstFilter.index(it.m_text)
            #    self.lstElement.SetItemState(i,wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
            #except:
            #    pass
    def OnGcbbLinkButton(self,event):
        if self.idxMap<0:
            return
        it=self.lstMap.GetItem(self.idxMap,1)
        it.m_text=self.txtMap.GetValue()
        it.m_mask=wx.LIST_MASK_TEXT
        self.lstMap.SetItem(it)
        
        pass
    def GetNodes(self):
        return self.lstNewNodes
    def OnGcbbOkButton(self, event):
        self.EndModal(1)
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()

    def OnGcbbBrowseFNButton(self, event):
        dlg = wx.FileDialog(self, "Open", ".", "", "XLS files (*.xls)|*.xls|SXC files (*.sxc)|*.sxc|all files (*.*)|*.*", wx.OPEN)
        try:
            dlg.Centre()
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.txtFN.SetValue(filename)
                
                self.appl=SCalc()
                self.appl.openFile(filename)
                self.appl.selSheet()
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()

    def OnGcbbImportButton(self, event):
        filename=self.txtFN.GetValue()
        
        #appl=SCalc()
        #appl.openFile(filename)
        self.infos=self.appl.getCellRange(self.intRowStart.GetValue()-1,
                                    self.intColStart.GetValue()-1,
                                    self.intRowStop.GetValue()-1,
                                    self.intColEnd.GetValue()-1)
        self.lstElement.DeleteAllItems()
        i=0
        for item in self.infos[0]:
            index = self.lstElement.InsertImageStringItem(sys.maxint, '%d'%i, -1)
            self.lstElement.SetStringItem(index,1,item,-1)
            i=i+1
        event.Skip()

    def OnLstMapListItemSelected(self, event):
        idx=event.GetIndex()
        self.idxMap=idx
        event.Skip()

    def OnGcbbBuildButton(self, event):
        lst=[]
        for i in range(0,self.lstMap.GetItemCount()):
            it=self.lstMap.GetItem(i,0)
            sTag=it.m_text
            it=self.lstMap.GetItem(i,1)
            sNr=it.m_text
            lst.append((sTag,sNr))
        for row in self.infos:
            node=self.node.cloneNode(True)
            childs=vtXmlDomTree.getChilds(node)
            i=0
            for sTag,sNr in lst:
                if len(sNr)>0:
                    try:
                        s=string.split(sNr,',')
                        if len(s)>1:
                            idx=int(s[1])
                            sPref=s[0]+' '
                        else:
                            idx=int(sNr)
                            sPref=''
                        vtXmlDomTree.setText(childs[i],sPref+string.join(
                            map(string.capitalize,string.split(row[idx],' ')),' '))
                    except:
                        vtXmlDomTree.setText(childs[i],sNr)
                i=i+1
            self.lstNewNodes.append(node)
        event.Skip()
        
