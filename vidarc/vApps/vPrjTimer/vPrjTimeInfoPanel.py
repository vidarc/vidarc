#Boa:FramePanel:vPrjTimeInfoPanel
#----------------------------------------------------------------------------
# Name:         vPrjTimeInfoPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vPrjTimeInfoPanel.py,v 1.19 2007/08/21 18:15:16 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.vApps.vPrj.vXmlPrjInputTree
import vidarc.tool.input.vtInputTextMultiLine
import vidarc.tool.time.vtTimeInputDateTimeStartEnd
import vidarc.tool.time.vtTimeTimeEdit
import vidarc.tool.time.vtTimeDateGM
import vidarc.vApps.vTask.vXmlTaskInputTree
import vidarc.vApps.vHum.vXmlHumInputTreeUsr
import vidarc.vApps.vLoc.vXmlLocInputTree
import vidarc.tool.input.vtInputTree
import wx.grid
import wx.lib.buttons
import cStringIO
import vidarc.tool.xml.vtXmlDom as vtXmlDom
#from vidarc.vApps.vPrjTimer.vpwTask import *

from vidarc.tool.time.vTimeDateGM import *
from vidarc.tool.time.vTimeTimeEdit import *

from vidarc.vApps.vPrj.vXmlPrjGrpTree import *

from vidarc.tool.xml.vtXmlNodePanel import *
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

import string

[wxID_VPRJTIMEINFOPANEL, wxID_VPRJTIMEINFOPANELGCBBADD, 
 wxID_VPRJTIMEINFOPANELGCBBAPPLY, wxID_VPRJTIMEINFOPANELGCBBCANCEL, 
 wxID_VPRJTIMEINFOPANELGCBBDEL, wxID_VPRJTIMEINFOPANELGCBBNOTE, 
 wxID_VPRJTIMEINFOPANELGCBTSTART, wxID_VPRJTIMEINFOPANELLBLDATE, 
 wxID_VPRJTIMEINFOPANELLBLLOC, wxID_VPRJTIMEINFOPANELLBLPERSON, 
 wxID_VPRJTIMEINFOPANELLBLPRJDESC, wxID_VPRJTIMEINFOPANELLBLPRJNAME, 
 wxID_VPRJTIMEINFOPANELLBLPRJTASK, wxID_VPRJTIMEINFOPANELTXTDESC, 
 wxID_VPRJTIMEINFOPANELVINDATETIME, wxID_VPRJTIMEINFOPANELVITRHUM, 
 wxID_VPRJTIMEINFOPANELVITRLOC, wxID_VPRJTIMEINFOPANELVITRPRJ, 
 wxID_VPRJTIMEINFOPANELVITRTASK, 
] = [wx.NewId() for _init_ctrls in range(19)]

#----------------------------------------------------------------------
def getOkData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00nIDATx\x9c\xa5\x93\xc9\r\xc0 \x0c\x04\x17D\x01\xd0^\n\xa4=\xd2\x01yD\
\x96L\x08\xe0c\xdf\xecx\x0c"\x00\xbd\xc3\x91h)\xe5Z\x90k\x01\x00$m\x91r_M\
\x07\xa02\x15)\xa2\x15Ve1`U\x16\x01\xf8\xde&\xc0)[\xc0n\xf7\x01\xc0\xdf\xd5e\
`\x01E\xe0U\xfcjJ\xf4\'\x03:\xac\xb1\x98.\x91O<M\xff\x05h\x13\xbc\xdf\xf9\
\x01\x97y&\xadH\xfc\xe0%\x00\x00\x00\x00IEND\xaeB`\x82' 

def getOkBitmap():
    return wx.BitmapFromImage(getOkImage())

def getOkImage():
    stream = cStringIO.StringIO(getOkData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getCancelData():
    return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00lIDATx\x9c\xad\x93\xc1\r\xc00\x08\x03M\xb7\xe8\xfe\xc3e\x0c\xfa\x8bB\
bC$\xca\x9b;\x01N\x0cpG\xa3\x9e\x0e\xfc\xaf`\xc00`%\xb0\xf7M\xc1\x0b\x9f\r\
\x19\xbc\xf6\x06A%a\xf0!P\x12\x05\x03\x80\xa9\x18\xf7)\x18L'`\x80\x82S\x01[\
\xe1Z\xb0\xee\\\xa5s\x08\xd8\xc12I\x10d\xd7V\x92\xf0\x12\x15\x9cId\x8c\xb7\
\xd5\xfeL\x1f$\x07+\xb8\xd6Q\x0bp\x00\x00\x00\x00IEND\xaeB`\x82" 

def getCancelBitmap():
    return wx.BitmapFromImage(getCancelImage())

def getCancelImage():
    stream = cStringIO.StringIO(getCancelData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getAddData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x00\x87IDAT(\x91\xa5RA\x0e\x80 \x0c+\x84\xcf!\xcf \xbc\x8a\xec\x19\xe8\xf3\
\xf4\xb0\x04\xe6D\x84\xd8\x13,\xdd\xdae5\xc0\t\x81\xcd\x07\xdc\xb1\x1fE~\x9d\
\xa2\xc6\x14\xa1\x11d\x9ba\x85\xcd\x87\x1e\xb5\x812q\x8f\xed\xb2)\x13e\x92\
\x95\x98"[\xb0\x83\xa9]\xb8O3R\x04\x08\xeb\n\xf5\xa5L\xabJu\xf1CAn\xc2\xb3\
\xbb\xbb-(\xf0)\xec~\x94\xa7\xfb\x01f\x15\xea\xa5\xa7\xa2Q\xd9\xad\x01/\xe1c\
\xb72\xb0f\x1co\x95m\x00\x17\xd7\x95A\xb5\xe1.\xb5f\x00\x00\x00\x00IEND\xaeB\
`\x82' 

def getAddBitmap():
    return wx.BitmapFromImage(getAddImage())

def getAddImage():
    stream = cStringIO.StringIO(getAddData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getDelData():
    return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x00\xe2IDAT(\x91\x85\x92Ar\x830\x0cE\x9f\x19.\x95N\xbb\x08\xcb\xe6F\xa0\xb2\
pEo\x94.\xc9\x82\x8es,u\xa1D1eH\xff0\x1a\xc9\xfa_\xdf6N`\xdc\xa1y\x8a\\\xc6\
\x81\x1d\x18\x98f\xd5\xacf\xe6\xd1\x13\xcd\xea\xdd\xd5\x17T'\x01e)e){\xb2\
\x16\x18>\x06`\xfa\xbc\xedg\xbe\xcc\xdd\xb1\xbb\xfe\\\xa3\x05Sl2i\xd6\xee\
\xd8\x01\x87\xd7\x03\x90Rr\x1f_tD\xeb\xe6\xe0\xf0\x91fV\xbb\x05;VZ\x19\x07\
\xcdS\xcc;\xbd\x9f\x80\xf3\xf79\x04\xb5\x18h\xebB\xbfTz\t\x99\xf42_\xe6?w\
\xda\xd4\xec\x88[\xc8(\xfe\xc7\x1e\x02\x9f\x1d\xd1\x93-\x1a@\xc6\xa1\xb6~n\
\xd5\xd4\x85\xf4\xf2\x9c\r\xa4\xfb[J@Y\n\xd5\xd1\x01w\x8e\x03P\xdd\x92\x01/o\
\x0f\xd9\x1e\xdau\xf9\xbf,\xd5\xcf{\xd3Z\rr\xfc\x02\x1fr\x8f\xf6KRW\xed\x00\
\x00\x00\x00IEND\xaeB`\x82" 

def getDelBitmap():
    return wx.BitmapFromImage(getDelImage())

def getDelImage():
    stream = cStringIO.StringIO(getDelData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getTimerData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x01cIDAT(\x91u\x92\xb1n\xc20\x10\x86\xffT\x1d:\xc2\xc6\x18\xbfA\xc3\x86\xc4\
B\xde \xe6\t*\xf2\x024l\xed\x84\xcc\x88\xfa\x02\xd0J\xad`K"E\xca\x9a\x0c\xa9\
\x92\x8dn]S\x89J\xe9\xc6\x90\xc1\xa3;8\x18S\xd2[l\xf9\xee;\xeb\xbf\xff\x0c@\
\xe0<\xec\xd1\x02H\x00$)\x03\x06\x7f\xb2Wh\x0b\xeaPb\x92\xd6\xd4\x95lIL\x17\
\x00P\x10\xd3\x05\x92 \x0c\xca\xaf\xd2\x1e=\x02\x0b\xf9>\xb9s\x81\x02\x80\
\x01\xb08\xb2\x820HRP\xa73\xbb\x9f\xe9\xfdv\x1f\xbb\xe5\xd3\xf2p\xb0\xc6\xb4\
[\x96\xe5\xfae\x05@\x00\x8c:4\xcfrq\x8c8\xfa\xe45\x97w^so\xeaY\xb7\x1e\x90K\
\xc1\x02`\x9b\xd7\x8d\x10\x82\xcd\x99,\xd2\xe1<\xabx\xcd\xa9C\x01\xd1\x00\
\xd4\xa1\xd5\xbe\x92i\xc9(\xb2a\x84\x88\xa3\x18`\x12h\xda\xab`sv\tT\xfb\xca\
\x1e\xd9\x80h\xc6Z\xbc\xff(\xa1\xde\xd4\xf3\x03\x7f\xfb\xb6=i\xd7\xb2m>\xa4I\
\xdf\xea\xb7\x9a\x00\xe0Z\x1e\x83a\xef\xd8\xac\xb8\xe9vW\xcf\xab\xb3\xaaa\
\x0f\xdf\xa7O4\xd1Y.\xb4\xf9\xfc\'Z\x00,\x8eb9\xc1\x96\xea\xac\xe25\x97\x8a\
\x15 \x889\xc9\xb3\\\x99ui\xdc\xd15a\xa8m%\xa6K\x9d\xce\x98\x8e\xf5\xb5K\xd2\
\xc4\x0f\xfd \x9c\xa9\xb55\xce\xd7\xbb \xe6\x9a\x90R\x03l\xe0A\xd7\xff\x0b!O\
8\xa2\xb2p\x83\xe8\x00\x00\x00\x00IEND\xaeB`\x82' 

def getTimerBitmap():
    return wx.BitmapFromImage(getTimerImage())

def getTimerImage():
    stream = cStringIO.StringIO(getTimerData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getNoteData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x01\xf7IDAT(\x91u\xd11h\x13Q\x18\x07\xf0\x7f\xc2\t\xdf\x83\x13^\xa0\xc3=\
\xb0\x90\x07\x19\xbc@\xc0\x84\n^\xa8\x83\x11\x97H\x07#\xba\xdcT\xb3\x95\xe2\
\xd2\x8en\x82\x93\x8e:Hj\xa7&\xa0\xd0\xb1\x0e\x85sPr\x83\x92\x0eB\xce!\x90\
\xc1\xc0\x05*\xbc\x03\x03\xf9\x86@\x1c\x9a\xa4\t\xea\xc7\x1b\x1e|\xff\xdf\
\xc7\xf7x)`\x8a\xff\x94\xb7\x89R!af\x10\xc088t\x00\xa4\x16\xa0\xf1\xceo\xb6\
\x9a\xf8\r]\x80\x94\xa0\x91\xc4\x04b\x1dD$\xb3\x9c\xfcB\x1c?|\xf5\xbca-\xe6\
\x85\xa7C\xef\x1c\xfa)\x95\nd\xbe&\xe5m"\x12\xf3\xa6\x00p\xb7\xd2\x07p\td+\
\xd8\x07\xeaO8\x0f\x06P\xff\xac+\x0f"\x7f\xeb\x92\x91M+\x807\x8a/\xbf\x9d\
\xd1\x16\x82s\xc4=\xec\xee\xf4];\x03\x14\x01\x05 I\xdaR\xca\x15`\x8aj\xef\
\x03\xb4U\xeeO^\x87\x1f\xe1\x8c\x87d\xd7\x88\xd4\xac\xdbC\xde\xcd{\x9bK #\
\x95\xcej\x8c\xa0z\xb5\xea\xed>M4\xe5fi\x8c\x10~Wn\xce5\x1e\xa7\x17 \x1e\x18\
\x00\xb0A9%e\x99n(\xd8\xf3\xde\x00\xc1iGk\xcd\xa3%\xc0\x1c\xcfn6pm\xe9;F@\
\x82\xceO\xc1\xcc\x8c9\xf0n\x05\x95\xe5\xd0r%88\xe9\xab\x8ck\x8c\x19\'\xe34\
\x00\x9d\x1d\x8a\xab\xfe\xde\x8b\n \xff:\x84\x01\x9e\x1dv\xaaw\xaa\x17\xdcr\
\xaf\'R\xd6\x1aova\xd3?\xc6\xf7\xcc\xfe\xdb\xa8T,e\x9c\x0c\x00!E\x9a\xf9DY|\
\xdc\xea\x04\x9f\x18\xbc\x9af\x84\x91n\x0fc\xff\x91\x0f\x80\x88\x08\x94\x02\
\xa6G\xcd\xa1\xe3t\x9b\xadf7\xea\x8a+B\xafA\xe7\xc8\xbbW.\xaf\xcb\xfb;\xc7\
\xf5\xed:\x81\xc8&\xa5\xd4\xd1{w\x06\\76\xc603&\x00@6E?\xa2v\xd8V\x8e\xf2\
\x1f\xfb\xccLDjM)\xed\xa4\x80\xa9\xb7\xc1\xa5\x9b\xcc`0\xc6<\xbeXGHA\xd6\xec\
U<a\xb2\xa8s&\xc3/\xf8\x03\xc0\xb3\xbe\xf0^\xb4\x95\xcb\x00\x00\x00\x00IEND\
\xaeB`\x82' 

def getNoteBitmap():
    return wx.BitmapFromImage(getNoteImage())

def getNoteImage():
    stream = cStringIO.StringIO(getNoteData())
    return wx.ImageFromStream(stream)

# defined event for vgpXmlTree item selected
wxEVT_VGPPROJECT_INFO_CHANGED=wx.NewEventType()
def EVT_VGPPROJECT_INFO_CHANGED(win,func):
    win.Connect(-1,-1,wxEVT_VGPPROJECT_INFO_CHANGED,func)
class vgpProjectInfoChanged(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VGPPROJECT_INFO_CHANGED(<widget_name>, self.OnInfoChanged)
    """

    def __init__(self,obj,node):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPPROJECT_INFO_CHANGED)
        self.obj=obj
        self.node=node
    def GetVgpProjectInfo(self):
        return self.obj
    def GetNode(self):
        return self.node

# defined event for vgpXmlTree item selected
wxEVT_VGPPROJECT_INFO_ADDED=wx.NewEventType()
def EVT_VGPPROJECT_INFO_ADDED(win,func):
    win.Connect(-1,-1,wxEVT_VGPPROJECT_INFO_ADDED,func)
class vgpProjectInfoAdded(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VGPPROJECT_INFO_ADDED(<widget_name>, self.OnInfoAdded)
    """

    def __init__(self,obj,node):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPPROJECT_INFO_ADDED)
        self.obj=obj
        self.node=node
    def GetVgpProjectInfo(self):
        return self.obj
    def GetNode(self):
        return self.node

# defined event for vgpXmlTree item selected
wxEVT_VGPPROJECT_INFO_DELETED=wx.NewEventType()
def EVT_VGPPROJECT_INFO_DELETED(win,func):
    win.Connect(-1,-1,wxEVT_VGPPROJECT_INFO_DELETED,func)
class vgpProjectInfoDeleted(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VGPPROJECT_INFO_DELETED(<widget_name>, self.OnInfoDeleted)
    """

    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPPROJECT_INFO_DELETED)
        self.obj=obj
    def GetVgpProjectInfo(self):
        return self.obj


# defined event for vgpXmlTree item selected
wxEVT_VGPPROJECT_INFO_STARTED=wx.NewEventType()
def EVT_VGPPROJECT_INFO_STARTED(win,func):
    win.Connect(-1,-1,wxEVT_VGPPROJECT_INFO_STARTED,func)
class vgpProjectInfoStarted(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VGPPROJECT_INFO_STARTED(<widget_name>, self.OnInfoStarted)
    """

    def __init__(self,obj,node):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPPROJECT_INFO_STARTED)
        self.obj=obj
        self.node=node
    def GetVgpProjectInfo(self):
        return self.obj
    def GetNode(self):
        return self.node

# defined event for vgpXmlTree item selected
wxEVT_VGPPROJECT_INFO_STOPPED=wx.NewEventType()
def EVT_VGPPROJECT_INFO_STOPPED(win,func):
    win.Connect(-1,-1,wxEVT_VGPPROJECT_INFO_STOPPED,func)
class vgpProjectInfoStopped(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VGPPROJECT_INFO_STOPPED(<widget_name>, self.OnInfoStopped)
    """

    def __init__(self,obj,node):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPPROJECT_INFO_STOPPED)
        self.obj=obj
        self.node=node
    def GetVgpProjectInfo(self):
        return self.obj
    def GetNode(self):
        return self.node

# defined event for vgpXmlTree item selected
wxEVT_VGPPROJECT_INFO_NOTE_ADDED=wx.NewEventType()
def EVT_VGPPROJECT_INFO_NOTE_ADDED(win,func):
    win.Connect(-1,-1,wxEVT_VGPPROJECT_INFO_NOTE_ADDED,func)
class vgpProjectInfoNoteAdded(wx.PyEvent):
    """
    Posted Events:
        Project Info changed event
            EVT_VGPPROJECT_INFO_NOTE_ADDED(<widget_name>, self.OnInfoNoteAdded)
    """

    def __init__(self,obj,node):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPPROJECT_INFO_NOTE_ADDED)
        self.obj=obj
        self.node=node
    def GetVgpProjectInfo(self):
        return self.obj
    def GetNode(self):
        return self.node



class vPrjTimeInfoPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=1
    AddBitmap=getAddBitmap()
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(3)
        parent.AddGrowableCol(1)
        parent.AddGrowableCol(3)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPrjName, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.vitrPrj, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblPrjTask, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.vitrTask, 2, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.gcbbApply, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.gcbbCancel, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblPerson, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.vitrHum, 1, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.gcbbAdd, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.gcbbDel, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblLoc, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.vitrLoc, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblDate, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.vinDateTime, 2, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.gcbtStart, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.gcbbNote, 1, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.lblPrjDesc, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.txtDesc, 2, border=0, flag=wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=6, hgap=4, rows=4, vgap=4)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VPRJTIMEINFOPANEL,
              name=u'vPrjTimeInfoPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(579, 154), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(571, 127))

        self.lblPrjName = wx.StaticText(id=wxID_VPRJTIMEINFOPANELLBLPRJNAME,
              label=_(u'project'), name=u'lblPrjName', parent=self, pos=wx.Point(0,
              0), size=wx.Size(56, 30), style=wx.ALIGN_RIGHT)

        self.lblPrjDesc = wx.StaticText(id=wxID_VPRJTIMEINFOPANELLBLPRJDESC,
              label=_(u'description'), name=u'lblPrjDesc', parent=self,
              pos=wx.Point(179, 102), size=wx.Size(60, 25),
              style=wx.ALIGN_RIGHT)

        self.lblPrjTask = wx.StaticText(id=wxID_VPRJTIMEINFOPANELLBLPRJTASK,
              label=_(u'task'), name=u'lblPrjTask', parent=self, pos=wx.Point(179,
              0), size=wx.Size(60, 30), style=wx.ALIGN_RIGHT)

        self.lblLoc = wx.StaticText(id=wxID_VPRJTIMEINFOPANELLBLLOC,
              label=_(u'location'), name=u'lblLoc', parent=self, pos=wx.Point(0,
              68), size=wx.Size(56, 30), style=wx.ALIGN_RIGHT)

        self.lblPerson = wx.StaticText(id=wxID_VPRJTIMEINFOPANELLBLPERSON,
              label=_(u'person'), name=u'lblPerson', parent=self, pos=wx.Point(0,
              34), size=wx.Size(56, 30), style=wx.ALIGN_RIGHT)

        self.lblDate = wx.StaticText(id=wxID_VPRJTIMEINFOPANELLBLDATE,
              label=_(u'date'), name=u'lblDate', parent=self, pos=wx.Point(179,
              68), size=wx.Size(60, 30), style=wx.ALIGN_RIGHT)

        self.vitrTask = vidarc.vApps.vTask.vXmlTaskInputTree.vXmlTaskInputTree(id=wxID_VPRJTIMEINFOPANELVITRTASK,
              name=u'vitrTask', parent=self, pos=wx.Point(243, 0),
              size=wx.Size(181, 30), style=0)

        self.vitrHum = vidarc.vApps.vHum.vXmlHumInputTreeUsr.vXmlHumInputTreeUsr(id=wxID_VPRJTIMEINFOPANELVITRHUM,
              parent=self, pos=wx.Point(60, 34), size=wx.Size(115, 30),
              style=0)

        self.vitrLoc = vidarc.vApps.vLoc.vXmlLocInputTree.vXmlLocInputTree(id=wxID_VPRJTIMEINFOPANELVITRLOC,
              name=u'vitrLoc', parent=self, pos=wx.Point(60, 68),
              size=wx.Size(115, 30), style=0)

        self.vinDateTime = vidarc.tool.time.vtTimeInputDateTimeStartEnd.vtTimeInputDateTimeStartEnd(id=wxID_VPRJTIMEINFOPANELVINDATETIME,
              name=u'vinDateTime', parent=self, pos=wx.Point(243, 68),
              size=wx.Size(181, 30), style=0)

        self.txtDesc = vidarc.tool.input.vtInputTextMultiLine.vtInputTextMultiLine(id=wxID_VPRJTIMEINFOPANELTXTDESC,
              name=u'txtDesc', parent=self, pos=wx.Point(243, 102),
              size=wx.Size(181, 25), style=0)

        self.gcbbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJTIMEINFOPANELGCBBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'), name=u'gcbbApply',
              parent=self, pos=wx.Point(428, 0), size=wx.Size(76, 30), style=0)
        self.gcbbApply.Bind(wx.EVT_BUTTON, self.OnGcbbApplyButton,
              id=wxID_VPRJTIMEINFOPANELGCBBAPPLY)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJTIMEINFOPANELGCBBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'gcbbCancel', parent=self, pos=wx.Point(508, 0),
              size=wx.Size(63, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VPRJTIMEINFOPANELGCBBCANCEL)

        self.gcbbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJTIMEINFOPANELGCBBADD,
              bitmap=vtArt.getBitmap(vtArt.Add), label=_(u'Add'), name=u'gcbbAdd',
              parent=self, pos=wx.Point(428, 34), size=wx.Size(76, 30),
              style=0)
        self.gcbbAdd.Bind(wx.EVT_BUTTON, self.OnGcbbAddButton,
              id=wxID_VPRJTIMEINFOPANELGCBBADD)

        self.gcbbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJTIMEINFOPANELGCBBDEL,
              bitmap=vtArt.getBitmap(vtArt.Del), label=_(u'Delete'), name=u'gcbbDel',
              parent=self, pos=wx.Point(508, 34), size=wx.Size(63, 30),
              style=0)
        self.gcbbDel.Bind(wx.EVT_BUTTON, self.OnGcbbDelButton,
              id=wxID_VPRJTIMEINFOPANELGCBBDEL)

        self.gcbtStart = wx.lib.buttons.GenBitmapTextToggleButton(ID=wxID_VPRJTIMEINFOPANELGCBTSTART,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Start'), name=u'gcbtStart',
              parent=self, pos=wx.Point(428, 68), size=wx.Size(76, 30),
              style=0)
        self.gcbtStart.SetToggle(False)
        self.gcbtStart.Bind(wx.EVT_BUTTON, self.OnGcbtStartButton,
              id=wxID_VPRJTIMEINFOPANELGCBTSTART)

        self.gcbbNote = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJTIMEINFOPANELGCBBNOTE,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Note'), name=u'gcbbNote',
              parent=self, pos=wx.Point(508, 68), size=wx.Size(63, 30),
              style=0)
        self.gcbbNote.Bind(wx.EVT_BUTTON, self.OnGcbbNoteButton,
              id=wxID_VPRJTIMEINFOPANELGCBBNOTE)

        self.vitrPrj = vidarc.vApps.vPrj.vXmlPrjInputTree.vXmlPrjInputTree(id=wxID_VPRJTIMEINFOPANELVITRPRJ,
              name=u'vitrPrj', parent=self, pos=wx.Point(60, 0),
              size=wx.Size(115, 30), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vPrjTimer')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,applName=_(u'vPrjTimer PrjTime'),
                lWidgets=[self.txtDesc,])
        self.rootNode=None
        self.doc=None
        self.netHum=None
        self.netLoc=None
        self.netPrj=None
        self.netMaster=None
        self.node=None
        self.nodeCurStarted=None
        self.selPrj=None
        self.prjs=None
        self.locs=None
        self.persons=None
        self.dictTask={}
        self.bTaskContent=False
        self.tree=None
        
        #self.vitrPrj.SetTreeFunc(self.__createPrjTree__,self.__showPrjNode__)
        #self.vitrPrj.SetTagNames('project','name')
        vidarc.tool.input.vtInputTree.EVT_VTINPUT_TREE_CHANGED(self.vitrPrj,self.OnPrjSelChanged)
        self.vitrHum.SetTagNames('person','name')
        self.vitrHum.SetAppl('vHum')
        self.vitrTask.SetTagNames('task','name')
        self.vitrTask.SetAppl('vTask')
        self.vitrLoc.SetAppl('vLoc')
        self.vitrPrj.SetAppl('vPrj')
        self.vinDateTime.SetTagNames('date','starttime','endtime')
        self.txtDesc.SetTagName('desc')

        img=getTimerBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.gcbtStart.SetBitmapLabel(img)
        
        img=getNoteBitmap()
        mask=wx.Mask(img,wx.BLUE)
        img.SetMask(mask)
        self.gcbbNote.SetBitmapLabel(img)
        
    def Lock(self,flag):
        if flag:
            self.vitrPrj.Enable(False)
            self.vitrHum.Enable(False)
            self.vitrLoc.Enable(False)
            self.vitrTask.Enable(False)
            self.vinDateTime.Enable(False)
            self.txtDesc.Enable(False)
            self.gcbbApply.Enable(False)
            self.gcbbCancel.Enable(False)
            self.gcbbDel.Enable(False)
            self.gcbbAdd.Enable(False)
            self.gcbbNote.Enable(False)
            self.gcbtStart.Enable(False)
        else:
            self.vitrPrj.Enable(True)
            self.vitrHum.Enable(True)
            self.vitrLoc.Enable(True)
            self.vitrTask.Enable(True)
            self.vinDateTime.Enable(True)
            self.txtDesc.Enable(True)
            self.gcbbApply.Enable(True)
            self.gcbbCancel.Enable(True)
            self.gcbbDel.Enable(True)
            self.gcbbAdd.Enable(True)
            self.gcbbNote.Enable(True)
            self.gcbtStart.Enable(True)
    def CreateNode(self):
        #vtLog.vtCallStack(4)
        
        #root node found
        # add prj time
        node=self.doc.getBaseNode()
        if node is None:
            return None
            
        # get data from gui
        #idx=self.chcPrj.GetSelection()
        #prjNode,sPrj,iPrjId=self.prjs.GetInfoByIdx(idx)
        #sTask=self.vpwTask.GetValue()
        #iTaskId=self.vpwTask.GetSelID()
        #idx=self.chcLoc.GetSelection()
        #locNode,sLoc,iLocId=self.locs.GetInfoByIdx(idx)
        #idx=self.chcPerson.GetSelection()
        #personNode,sPerson,iPersonId=self.persons.GetInfoByIdx(idx)
        sDesc=self.txtDesc.GetValue()
        #sDate=self.dteDate.GetValue()
        #sStartTime=self.tmeStart.GetTimeStr()
        #sEndTime=self.tmeEnd.GetTimeStr()
        
        #if len(iTaskId)<=0:
        #    iTaskId=None
        #if len(iLocId)<=0:
        #    iLocId=None
        #if len(iPersonId)<=0:
        #    iPersonId=None
        try:
            sPrj=self.vitrPrj.GetValue()
            iPrjId=self.vitrPrj.GetID()
            sTask=self.vitrTask.GetValue()
            iTaskId=self.vitrTask.GetID()
            sLoc=self.vitrLoc.GetValue()
            iLocId=self.vitrLoc.GetID()
            sPerson=self.vitrHum.GetValue()
            iPersonId=self.vitrHum.GetID()
            sDate,sStartTime,sEndTime=self.vinDateTime.GetValueStore()
            #sDate=self.vtDate.GetValue()
            #sStartTime=self.vtStartTime.GetValue()
            #sEndTime=self.vtEndTime.GetValue()
        except:
            vtLog.vtLngTB(self.GetName())
        if 1==1:
            elemPrjTime=self.doc.CreateNode(node,prj=sPrj,prjId=iPrjId,
                    task=sTask,taskId=iTaskId,
                    loc=sLoc,locId=iLocId,
                    person=sPerson,personId=iPersonId,
                    desc=sDesc,date=sDate,
                    starttime=sStartTime,endtime=sEndTime)
        else:
            elemPrjTime=self.doc.CreateNode(node,desc=sDesc)
            self.GetNode(node)
        
        if self.tree is None:
            self.doc.addNode(None,elemPrjTime)
        else:
            self.doc.addNode(None,elemPrjTime,self.tree.SetIdManual)
        return elemPrjTime
        
    def SetRootNode(self,root):
        self.rootNode=root
        
    def __setSelect__(self,ids,sName,sId,chcObj):
        if ids is None:
            chcObj.SetSelection(-1)
            return
        if len(sId)>0:
            idx=ids.GetIdxById(sId)
        else:
            idx=ids.GetIdxByName(sName)
        chcObj.SetSelection(idx)
    def IsBusy(self):
        return False
    def Stop(self):
        pass
    def Clear(self):
        if self.VERBOSE:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'Clear',origin='vgpPrjTimeInfo')
        #self.txtDesc.SetValue('')
        vtXmlNodePanel.Clear(self)
        try:
            self.vitrPrj.Clear()
            self.vitrHum.Clear()
            self.vitrLoc.Clear()
            self.vitrTask.Clear()
            self.vinDateTime.Clear()
            self.txtDesc.Clear()
        except:
            vtLog.vtLngTB(self.GetName())
    def SetDoc(self,doc,bNet=False):
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        if bNet==True and 0:
            EVT_NET_XML_GOT_CONTENT(doc,self.OnGotContent)
            EVT_NET_XML_GET_NODE(doc,self.OnGetNode)
            #EVT_NET_XML_ADD_NODE(doc,self.OnAddNode)
            #EVT_NET_XML_DEL_NODE(doc,self.OnDelNode)
            EVT_NET_XML_REMOVE_NODE(doc,self.OnRemoveNode)
            EVT_NET_XML_LOCK(doc,self.OnLock)
            EVT_NET_XML_UNLOCK(doc,self.OnUnLock)
        self.vitrPrj.SetDoc(doc)
        self.vitrHum.SetDoc(doc)
        self.vitrLoc.SetDoc(doc)
        self.vitrTask.SetDoc(doc)
        self.vinDateTime.SetDoc(doc)
        self.txtDesc.SetDoc(doc)
    def SetTree(self,tree):
        self.tree=tree
    def SetNetMaster(self,netMaster):
        self.vitrTask.SetNetMaster(netMaster)
    def OnGotContent(self,evt):
        evt.Skip()
    def OnGetNode(self,evt):
        evt.Skip()
        if self.node is None:
            return
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            self.SetNode(self.node)
    def OnSetNode(self,evt):
        evt.Skip()
    def OnAddNode(self,evt):
        evt.Skip()
    def OnDelNode(self,evt):
        evt.Skip()
        if self.node is None:
            return
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            vtLog.CallStack('')
    def OnRemoveNode(self,evt):
        evt.Skip()
        if self.node is None:
            return
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            vtLog.CallStack('')
    def OnLock(self,evt):
        evt.Skip()
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['ok','already locked']:
                self.Lock(False)
            else:
                self.Lock(True)
    def OnUnLock(self,evt):
        evt.Skip()
        id=self.doc.getKey(self.node)
        if self.doc.isSameKey(self.node,evt.GetID()):
            resp=evt.GetResponse()
            if resp in  ['released']:
                self.Lock(False)
    def SetDocHum(self,netDoc,bNet=False):
        self.netHum=netDoc
        self.vitrHum.SetDocTree(netDoc,bNet)

        if bNet:
            EVT_NET_XML_GOT_CONTENT(netDoc,self.OnHumanChanged)
            EVT_NET_XML_CONTENT_CHANGED(netDoc,self.OnHumanChanged)
        #self.__humanChanged__()
    def OnHumanChanged(self,evt):
        if self.VERBOSE:
            vtLog.vtLngCallStack(self,vtLog.INFO,'changed',origin='vgpPrjTimeInfo')
        node=self.netHum.getChild(None,'users')
        self.vitrHum.SetNodeTree(node)
        evt.Skip()
    def SetDocLoc(self,netDoc,bNet=False):
        self.netLoc=netDoc
        self.vitrLoc.SetDocTree(netDoc,bNet)
        if bNet:
            EVT_NET_XML_CONTENT_CHANGED(netDoc,self.OnLocChanged)
            #EVT_NET_XML_CONTENT_CHANGED(netDoc,self.OnLocChanged)
        #self.__locChanged__()
    def OnLocContent(self,evt):
        #self.__locChanged__()
        evt.Skip()
    def OnLocChanged(self,evt):
        #self.__locChanged__()
        node=self.netLoc.getBaseNode()
        self.vitrLoc.SetNodeTree(node)
        evt.Skip()
    def SetDocPrj(self,netDoc,bNet=False):
        self.netPrj=netDoc
        self.vitrPrj.SetDocTree(netDoc,bNet)
        self.vitrPrj.SetNodeTree(None)
        if bNet:
            EVT_NET_XML_CONTENT_CHANGED(netDoc,self.OnPrjChanged)
        #self.__prjChanged__()
    def __createPrjTree__(*args,**kwargs):
        tr=vXmlPrjGrpTree(**kwargs)
        tr.SetNodeInfos(['name','|id'])
        tr.SetGrouping([],[('name','')])
        tr.EnableLanguageMenu()
        return tr
    def __showPrjNode__(self,doc,node,lang):
        #self.vitrPrj.SetTreeNode()
        self.vitrTask.SelectPrj(doc.getAttribute(node,'id'))
        return doc.getNodeText(node,'name')
    def OnPrjSelChanged(self,evt):
        evt.Skip()
        #vtLog.CallStack('')
        id=evt.GetId()
        #print id
        self.vitrTask.SelectPrj(id)
        #node=evt.GetNode()
        #self.vitrTask.SelectPrj(doc.getAttribute(node,'id'))
    def OnPrjContent(self,evt):
        evt.Skip()
    def OnPrjChanged(self,evt):
        evt.Skip()
    def SetDocTask(self,doc,bNet=False):
        #self.vpwTask.SetDoc(doc,bNet)
        self.vitrTask.SetDocTree(doc,bNet)
        
        if bNet:
            EVT_NET_XML_GOT_CONTENT(doc,self.OnTaskChanged)
    def OnTaskChanged(self,evt):
        self.bTaskContent=True
        if self.VERBOSE:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'task content',origin='vgpPrjTimeInfo')
        #wx.CallAfter(self.__selectTask__)
        #self.__selectTask__()
        evt.Skip()
    def OnTaskBuild(self,evt):
        self.__selectTask__()
        evt.Skip()
    def SetNode(self,node):
        try:
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'%s'%(node),self)
                vtLog.CallStack('')
                print node
            if self.doc.getTagName(node)!='prjtime':
                node=None
            if vtXmlNodePanel.SetNode(self,node)<0:
                if self.VERBOSE:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'exit',self)
                    print 'exit'
                return
            self.selPrj=None
            self.node=node
            # set data to xml-node
            #sDesc=self.doc.getNodeText(node,'desc')
            #sDate=self.doc.getNodeText(node,'date')
            #sStartTime=self.doc.getNodeText(node,'starttime')
            #sEndTime=self.doc.getNodeText(node,'endtime')
            
            #self.txtDesc.SetValue(sDesc)
            #self.dteDate.SetValue(sDate)
            #self.tmeStart.SetTimeStr(sStartTime)
            #self.tmeEnd.SetTimeStr(sEndTime)
            self.txtDesc.SetNode(node)#,'desc')
            self.vitrPrj.SetNode(node)
            self.vitrHum.SetNode(node)
            self.vitrLoc.SetNode(node)
            prjId=self.vitrPrj.GetID()
            self.vitrTask.SelectPrj(prjId)
            self.vitrTask.SetNode(node)
            self.vinDateTime.SetNode(node)
        except:
            vtLog.vtLngTB(self.GetName())
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
                print node
            if node is None:
                return
            self.txtDesc.GetNode(node)
            self.vitrPrj.GetNode(node)
            self.vitrHum.GetNode(node)
            self.vitrLoc.GetNode(node)
            self.vitrTask.GetNode(node)
            self.vinDateTime.GetNode(node)
            self.GetNodeFin(node)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnGcbbNoteButton(self, event):
        self.vinDateTime.Now()
        sDate,sStartTime,sEndTime=self.vinDateTime.GetValueStore()
        self.vinDateTime.SetValueStore(' '.join([sDate,'------',sEndTime]))
        elem=self.CreateNode()
        wx.PostEvent(self,vgpProjectInfoNoteAdded(self,elem))
        event.Skip()
        
    def OnGcbbApplyButton(self, event):
        self.txtDesc.SetFocus()
        self.GetNode(self.node)
        wx.PostEvent(self,vgpProjectInfoChanged(self,self.node))
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.SetNode(self.node)
        event.Skip()

    def OnGcbbAddButton(self, event):
        elem=self.CreateNode()
        wx.PostEvent(self,vgpProjectInfoAdded(self,elem))
        event.Skip()

    def OnGcbbDelButton(self, event):
        if self.doc is None:
            return
        if self.nodeCurStarted==self.node:
            self.nodeCurStarted=None
            self.gcbtStart.SetValue(False)
        node=self.node
        #self.doc.deleteNode(self.node)
        self.Clear()
        #self.doc.delNode(node)
        wx.PostEvent(self,vgpProjectInfoDeleted(self))
        event.Skip()
        
    def SetStarted(self,node):
        self.nodeCurStarted=node
        if node is None:
            self.gcbtStart.SetValue(False)
            
    def OnGcbtStartButton(self, event):
        if self.gcbtStart.GetValue()==True:
            #start
            self.vinDateTime.Now()
            sDate,sStartTime,sEndTime=self.vinDateTime.GetValueStore()
            self.vinDateTime.SetValueStore(' '.join([sDate,sStartTime,'------']))
            #dtToDay=wx.DateTime.Today()
            #sDate=dtToDay.Format("%Y%m%d")
            #self.dteDate.SetValue(sDate)
            
            #dtNow=wx.DateTime.Now()
            #sTime=dtNow.Format("%H%M%S")
            #self.tmeStart.SetTimeStr(sTime)
            
            #dtNow=wx.DateTime.Now()
            #sTime=dtNow.Format("%H%M%S")
            #self.tmeStart.SetTimeStr(sTime)
            #self.tmeEnd.SetTimeStr('')
            
            #get current login
            #self.cmbPerson.SetValue(getpass.getuser())
            
            self.nodeCurStarted=self.CreateNode()
            wx.PostEvent(self,vgpProjectInfoStarted(self,self.nodeCurStarted))
            pass
        else:
            #stop
            if self.nodeCurStarted is None:
                return
            self.SetNode(self.nodeCurStarted)
            self.doc.startEdit(self.nodeCurStarted)
            
            sDate,sStartTime,sEndTime=self.vinDateTime.GetValueStore()
            self.vinDateTime.Now()
            sDateNew,sStartTimeNew,sEndTimeNew=self.vinDateTime.GetValueStore()
            self.vinDateTime.SetValueStore(' '.join([sDate,sStartTime,sEndTimeNew]))
            
            #dtNow=wx.DateTime.Now()
            #sTime=dtNow.Format("%H%M%S")
            #self.tmeEnd.SetTimeStr(sTime)
            self.GetNode(self.nodeCurStarted)
            self.doc.doEdit(self.nodeCurStarted)
            wx.PostEvent(self,vgpProjectInfoStopped(self,self.nodeCurStarted))
            self.nodeCurStarted=None
            pass
        event.Skip()
    def __getTaskId__(self):
        if self.VERBOSE:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'get task id',origin='vgpPrjTimeInfo')
        sTask=self.chcTask.GetStringSelection()
        idx=self.chcPrj.GetSelection()
        prjNode,sPrj,iPrjId=self.prjs.GetInfoByIdx(idx)
        try:
            d=self.dictTask[str(iPrjId)]
            for k in d.keys():
                if sTask==d[k][0]:
                    return k
        except:
            pass
        return ''
    def OnChcPrjChoice(self, event):
        if self.VERBOSE:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'prj selected',origin='vgpPrjTimeInfo')
        self.__updateTask__()
        event.Skip()
    def __updateTask__(self):
        if self.VERBOSE:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'update task',origin='vgpPrjTimeInfo')
        idx=self.chcPrj.GetSelection()
        if (idx<0) or (self.prjs is None):
            return
        prjNode,sPrj,iPrjId=self.prjs.GetInfoByIdx(idx)
        self.vpwTask.SetPrj(str(iPrjId))
        wx.CallAfter(self.__selectTask__)
    def __selectTaskTree__(self,id,name):
        if self.VERBOSE:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'select task tree',origin='vgpPrjTimeInfo')
        self.vpwTask.Select(id,name)

    def OnVitrPrjVtinputTreeChanged(self, event):
        event.Skip()
        
