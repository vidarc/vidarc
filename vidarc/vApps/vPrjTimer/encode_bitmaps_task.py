#!/usr/bin/env python
#----------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    "-u -i -n Task data/Task01_16.png images_task.py",
    "-a -u -n TaskSel data/Task02_16.png images_task.py",
    "-a -u -n SubTask data/SubTask01_16.png images_task.py",
    "-a -u -n SubTaskSel data/SubTask02_16.png images_task.py",
    "-a -u -n Tasks data/vidarcAppIconTask_16.png  images_task.py",
    "-a -u -n Prj data/Prj03_16.png  images_task.py",
    "-a -u -n PrjSel data/Prj02_16.png  images_task.py",
    "-a -u -n Grouping data/TreeGrouping04_16.png  images_task.py",
    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)
