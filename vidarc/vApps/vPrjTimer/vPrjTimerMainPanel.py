#Boa:FramePanel:vPrjTimerMainPanel
#----------------------------------------------------------------------------
# Name:         vPrjTimerMainPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060718
# CVS-ID:       $Id: vPrjTimerMainPanel.py,v 1.15 2012/12/23 20:21:04 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
from wx.lib.anchors import LayoutAnchors
import vidarc.tool.xml.vtXmlNodeTagCmdPanel
import vidarc.tool.xml.vtXmlNodeRegSelector
import time

from vidarc.tool.xml.vtXmlNodeRegListBook import *

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.vApps.common.vSystem as vSystem

from vidarc.tool.net.vtNetSecXmlGuiMaster import *

from vidarc.vApps.vPrjTimer.vNetPrjTimer import *
from vidarc.vApps.vPrjTimer.vXmlPrjTimerTree import *

from vidarc.vApps.vHum.vNetHum import *
from vidarc.vApps.vDoc.vNetDoc import *
from vidarc.vApps.vPrjDoc.vNetPrjDoc import *
from vidarc.vApps.vLoc.vNetLoc import *
from vidarc.vApps.vPrj.vNetPrj import *
from vidarc.vApps.vTask.vNetTask import *
from vidarc.vApps.vGlobals.vNetGlobals import *

from vidarc.tool.xml.vtXmlDom import vtXmlDom
from vidarc.tool.vtThread import vtThread

from vidarc.vApps.vPrjTimer.vPrjTimerReportDialog import vPrjTimerReportDialog
from vidarc.vApps.vPrjTimer.vPrjTimerReportImportDialog import vPrjTimerReportImportDialog
from vidarc.ext.xml.veXmlConsistanceDialog import veXmlConsistanceDialog
from vidarc.ext.xml.veXmlValidateDialog import veXmlValidateDialog
from vidarc.tool.time.vtTime import vtDateTime
from vidarc.tool.time.vtTime import vtTime

import vidarc.vApps.vPrjTimer.images as imgPrjTimerMain

def create(parent):
    return vPrjTimerMainPanel(parent)

[wxID_VPRJTIMERMAINPANEL, wxID_VPRJTIMERMAINPANELPNDATA, 
 wxID_VPRJTIMERMAINPANELPNTOP, wxID_VPRJTIMERMAINPANELSLWNAV, 
 wxID_VPRJTIMERMAINPANELSLWTOP, wxID_VPRJTIMERMAINPANELVITAG, 
] = [wx.NewId() for _init_ctrls in range(6)]

def getPluginImage():
    return imgPrjTimerMain.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(imgPrjTimerMain.getPluginBitmap())
    return icon

class vPrjTimerMainPanel(wx.Panel):
    VERBOSE=0
    def _init_coll_bxsTop_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viTag, 1, border=4, flag=wx.EXPAND | wx.ALL)

    def _init_sizers(self):
        # generated method, don't edit
        self.bxsTop = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsData = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_bxsTop_Items(self.bxsTop)

        self.pnData.SetSizer(self.bxsData)
        self.pnTop.SetSizer(self.bxsTop)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VPRJTIMERMAINPANEL,
              name=u'vPrjTimerMainPanel', parent=prnt, pos=wx.Point(87, 33),
              size=wx.Size(806, 400), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(798, 373))
        self.Bind(wx.EVT_SIZE, self.OnSize)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VPRJTIMERMAINPANELSLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 0), size=wx.Size(200,
              376), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetAutoLayout(True)
        self.slwNav.SetLabel(u'Navigation')
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetToolTipString(u'navigation')
        self.slwNav.SetDefaultSize(wx.Size(200, 100))
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VPRJTIMERMAINPANELSLWNAV)

        self.slwTop = wx.SashLayoutWindow(id=wxID_VPRJTIMERMAINPANELSLWTOP,
              name=u'slwTop', parent=self, pos=wx.Point(200, 0),
              size=wx.Size(600, 168), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwTop.SetDefaultSize(wx.Size(1000, 200))
        self.slwTop.SetAlignment(wx.LAYOUT_TOP)
        self.slwTop.SetAutoLayout(True)
        self.slwTop.SetSashVisible(wx.SASH_BOTTOM, True)
        self.slwTop.SetOrientation(wx.LAYOUT_HORIZONTAL)
        self.slwTop.SetLabel(_(u'information'))
        self.slwTop.SetToolTipString(_(u'information'))
        self.slwTop.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwTopSashDragged,
              id=wxID_VPRJTIMERMAINPANELSLWTOP)

        self.pnData = wx.Panel(id=wxID_VPRJTIMERMAINPANELPNDATA, name=u'pnData',
              parent=self, pos=wx.Point(208, 176), size=wx.Size(584, 192),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)

        self.pnTop = wx.Panel(id=wxID_VPRJTIMERMAINPANELPNTOP, name=u'pnTop',
              parent=self.slwTop, pos=wx.Point(0, 0), size=wx.Size(600, 165),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)

        self.viTag = vidarc.tool.xml.vtXmlNodeTagCmdPanel.vtXmlNodeTagCmdPanel(id=wxID_VPRJTIMERMAINPANELVITAG,
              name=u'viTag', parent=self.pnTop, pos=wx.Point(4, 4),
              size=wx.Size(588, 153), style=0)
        self.viTag.Bind(vidarc.tool.xml.vtXmlNodeTagCmdPanel.vEVT_TOOL_XML_TAG_SELECTED,
              self.OnViTagToolXmlTagSelected, id=wxID_VPRJTIMERMAINPANELVITAG)
        self.viTag.Bind(vidarc.tool.xml.vtXmlNodeTagCmdPanel.vEVT_TOOL_XML_TAG_CANCEL,
              self.OnViTagToolXmlTagCancel, id=wxID_VPRJTIMERMAINPANELVITAG)
        self.viTag.Bind(vidarc.tool.xml.vtXmlNodeTagCmdPanel.vEVT_TOOL_XML_TAG_APPLY,
              self.OnViTagToolXmlTagApply, id=wxID_VPRJTIMERMAINPANELVITAG)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vPrjTimer')
        self._init_ctrls(parent)
        self.SetName(name)
        self.bActivated=False
        self.dCfg={}
        self.cfgFunc=None
        self.tbMain=None
        self.thdGrp=vtThread(self)
        
        self.dlgConsistancy=None
        self.dlgReport=None
        self.dlgReportImp=None
        self.dlgConsistance=None
        self.dlgValidate=None
        
        self.xdCfg=vtXmlDom(appl='vPrjTimerCfg',audit_trail=False)
        
        appls=['vPrjTimer','vTask','vDoc','vPrjDoc','vPrj','vLoc','vHum','vGlobals']
        
        rect = parent.sbStatus.GetFieldRect(0)
        self.netMaster=vtNetSecXmlGuiMaster(parent.sbStatus,'Master',pos=(rect.x+2, rect.y+2),
                    size=(rect.width-4, rect.height-4),verbose=0)
        self.netPrjTimer=self.netMaster.AddNetControl(vNetPrjTimer,'vPrjTimer',synch=True,verbose=0,
                                            audit_trail=True)
        self.netTask=self.netMaster.AddNetControl(vNetTask,'vTask',synch=True,verbose=0)
        self.netDoc=self.netMaster.AddNetControl(vNetDoc,'vDoc',synch=True,verbose=0)
        self.netPrjDoc=self.netMaster.AddNetControl(vNetPrjDoc,'vPrjDoc',synch=True,verbose=0)
        self.netPrj=self.netMaster.AddNetControl(vNetPrj,'vPrj',synch=True,verbose=0)
        self.netLoc=self.netMaster.AddNetControl(vNetLoc,'vLoc',synch=True,verbose=0)
        self.netHum=self.netMaster.AddNetControl(vNetHum,'vHum',synch=True,verbose=0)
        self.netGlb=self.netMaster.AddNetControl(vNetGlobals,'vGlobals',synch=True,verbose=0)
        self.netMaster.AddNetControl(None,None,verbose=0)
        self.netMaster.SetLang(lang=vtLgBase.getPluginLang())
        
        self.netMaster.SetCfg(self.xdCfg,[],appls,'vPrjTimerMaster')
        
        self.vgpTree=vXmlPrjTimerTree(self.slwNav,wx.NewId(),
                pos=(0,0),size=(200,264),style=0,name="trPrjTimer",
                master=1,verbose=0)
        self.vgpTree.SetBuildOnDemand(1)
        self.vgpTree.SetConstraints(
            LayoutAnchors(self.vgpTree, True, True, True, True)
            )
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        #EVT_VTXMLTREE_ITEM_ADDED(self.vgpTree,self.OnTreeItemAdd)
        self.vgpTree.SetDoc(self.netPrjTimer,True)
        self.vgpTree.EnableImportMenu(True)
        self.vgpTree.EnableExportMenu(True)
        
        self.vgpTree.SetAnalyse([
                ( 'group', _(u'group') , vtArt.getBitmap(vtArt.FolderHanger) , (self.GroupData ,(),{},),{}) ,
                #( 'archive', _(u'archive') , vtArt.getBitmap(vtArt.Clip) , [
                #        ( 'mon2', _(u'2 month') , None , (self.ArchiveMonth , (2,),{})) ,
                #        ( 'mon3', _(u'3 month') , None , (self.ArchiveMonth , (3,),{})) ,
                #        ] ),
                ( 'report', _(u'send report') , vtArt.getBitmap(vtArt.EMail) , (self.ReportData ,(),{},)) ,
                ( 'reportimp', _(u'import report') , vtArt.getBitmap(vtArt.ImportFree) , (self.ReportDataImport ,(),{})) ,
                ( 'consistance', _(u'consistance') , vtArt.getBitmap(vtArt.RubberBand) , (self.Consistance ,(),{})) ,
                ( 'validate', _(u'validate') , vtArt.getBitmap(vtArt.Apply) , (self.Validate ,(),{},)) ,
                ])
        
        oDoc=self.netPrjTimer.GetReg('Doc')
        oDoc.SetTreeClass(vXmlPrjTimerTree)
        if 0:
            self.nbDoc=vtXmlNodeRegListBook(self.pnData,wx.NewId(),
                    pos=(8,8),size=(576, 176),style=0,name="nbDoc")
            self.nbDoc.SetDoc(self.netPrjTimer,True)
            self.viTag.AddWidgetDependent(self.nbDoc,oDoc)
            self.nbDoc.Show(False)
            self.bxsData.AddWindow(self.nbDoc, 1, border=4, flag=wx.EXPAND | wx.ALL)
            #self.viTag.AddWidgetDependent(self.nbDoc,None)
            self.viTag.SetRegNode(oDoc,bIncludeBase=True)
        else:
            pnCls=oDoc.GetPanelClass()
            pnDoc=pnCls(self.pnData,wx.NewId(),
                    pos=(8,8),size=(576, 176),style=0,name="pnDoc")
            pnDoc.SetDoc(self.netPrjTimer,True)
            #wx.CallAfter(pnDoc.SetRegNode,oDoc)
            self.bxsData.AddWindow(pnDoc, 1, border=4, flag=wx.EXPAND | wx.ALL)
            #self.bxsData.AddWindow(pnDoc.GetWid(), 1, border=4, flag=wx.EXPAND | wx.ALL)
            pnDoc.Show(False)
            self.viTag.AddWidgetDependent(pnDoc,oDoc)
            wx.CallAfter(self.viTag.SetRegNode,oDoc)
            wx.CallAfter(pnDoc.SetDoc,self.netPrjTimer,True)
        
        oPrjTime=self.netPrjTimer.GetRegisteredNode('prjtime')
        pnCls=oPrjTime.GetPanelClass()
        pn=pnCls(self.pnData,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="pnPrjTime")
        pn.SetDoc(self.netPrjTimer,True)
        pn.SetTree(self.vgpTree)
        self.viTag.AddWidgetDependent(pn,None)
        self.viTag.AddWidgetDependent(pn,oPrjTime)
        pn.Show(False)
        self.bxsData.AddWindow(pn, 1, border=4, flag=wx.EXPAND | wx.ALL)
        
        oPrjTaskSum=self.netPrjTimer.GetRegisteredNode('prjtasksum')
        pnCls=oPrjTaskSum.GetPanelClass()
        pn=pnCls(self.pnData,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="pnPrjTaskSum")
        pn.SetDoc(self.netPrjTimer,True)
        self.viTag.AddWidgetDependent(pn,oPrjTaskSum)
        pn.Show(False)
        self.bxsData.AddWindow(pn, 1, border=4, flag=wx.EXPAND | wx.ALL)
        
        oEmployee=self.netPrjTimer.GetRegisteredNode('employee')
        pnCls=oEmployee.GetPanelClass()
        pn=pnCls(self.pnData,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="pnEmployee")
        pn.SetDoc(self.netPrjTimer,True)
        self.viTag.AddWidgetDependent(pn,oEmployee)
        pn.Show(False)
        self.bxsData.AddWindow(pn, 1, border=4, flag=wx.EXPAND | wx.ALL)
        
        oEmployeeRecent=self.netPrjTimer.GetRegisteredNode('employeeRecent')
        pnCls=oEmployeeRecent.GetPanelClass()
        pn=pnCls(parent=self.pnData,
                pos=(8,8),size=(576, 176),style=0,name="pnEmployeeRecent")
        pn.SetRegNode(oEmployeeRecent)
        pn.SetDoc(self.netPrjTimer,True)
        self.viTag.AddWidgetDependent(pn,oEmployeeRecent)
        pn.Show(False)
        self.bxsData.AddWindow(pn.GetWid(), 1, border=4, flag=wx.EXPAND | wx.ALL)
        
        oMonth=self.netPrjTimer.GetRegisteredNode('month')
        pnCls=oMonth.GetPanelClass()
        pn=pnCls(self.pnData,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="pnMonth")
        pn.SetDoc(self.netPrjTimer,True)
        self.viTag.AddWidgetDependent(pn,oMonth)
        pn.Show(False)
        self.bxsData.AddWindow(pn, 1, border=4, flag=wx.EXPAND | wx.ALL)
        
        oYear=self.netPrjTimer.GetRegisteredNode('year')
        pnCls=oYear.GetPanelClass()
        pn=pnCls(self.pnData,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="pnYear")
        pn.SetRegNode(oYear)
        self.viTag.AddWidgetDependent(pn,oYear)
        pn.SetDoc(self.netPrjTimer,True)
        pn.Show(False)
        self.bxsData.AddWindow(pn, 1, border=4, flag=wx.EXPAND | wx.ALL)
        
        oDataCollector=self.netPrjTimer.GetReg('dataCollector')
        pnCls=oDataCollector.GetPanelClass()
        pn=pnCls(self.pnData,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="pnYear")
        pn.SetRegNode(oDataCollector)
        self.viTag.AddWidgetDependent(pn,oDataCollector)
        pn.SetDoc(self.netPrjTimer,True)
        pn.Show(False)
        self.bxsData.AddWindow(pn, 1, border=4, flag=wx.EXPAND | wx.ALL)
        #self.viTag.AddWidgetDependent(None,oDataCollector)
        self.viTag.SetDoc(self.netPrjTimer,True)
        #self.viTag.SetRegNode(oDataCollector)
        self.viTag.SetRegNode(oPrjTime)
        self.viTag.SetRegNode(oPrjTaskSum)
        self.viTag.SetRegNode(oMonth)
        self.viTag.SetRegNode(oEmployee)
        self.viTag.SetNetDocs(self.netPrjTimer.GetNetDocs())
        
        #self.Move(pos)
        #self.SetSize(size)
    def GetDocMain(self):
        return self.netPrjTimer
    def GetNetMaster(self):
        return self.netMaster
    def GetTreeMain(self):
        return self.vgpTree
    def PopulateToolBar(self,parent):
        id=wx.NewId()
        sz=self.GetClientSize()
        self.tbMain = wx.ToolBar(id=id,
                name=u'tbPrjTimer', parent=parent, pos=wx.Point(0,0),
                size=wx.DefaultSize, style=wx.TB_HORIZONTAL | wx.NO_BORDER)
        self.tbMain.SetToolBitmapSize(wx.Size(20,20))
        self.dToolBar={}
        self.vgpTree.PopulateToolBar(parent,self.tbMain,self.dToolBar)
        #self.tbMain.AddSimpleTool(id, vtArt.getBitmap(vtArt.Duplicate), _(u"Duplicate"), 
        #                _(u"duplicate"))
        #self.Bind(wx.EVT_TOOL, self.OnToolDupRecipeClick, id=id)
        self.tbMain.AddSeparator()
        id=wx.NewId()
        self.dToolBar['quickFind']=id
        txtCtrl=wx.TextCtrl(self.tbMain, id, "", size=(150, -1))
        self.tbMain.AddControl(txtCtrl)
        self.tbMain.Bind(wx.EVT_TEXT, self.OnQuickFind, id=id)
        self.tbMain.Bind(wx.EVT_TEXT_ENTER, self.OnQuickFindAgain, id=id)
        self.txtQuickFind=txtCtrl
        
        parent.SetToolBar(self.tbMain)
        self.tbMain.Realize()
        return self.tbMain
    def OnQuickFind(self,event):
        sVal=self.txtQuickFind.GetValue()
        self.idQuickFoundLast=-1
    def OnQuickFindAgain(self,event):
        sVal=self.txtQuickFind.GetValue()
        id=self.vgpTree.GetMoveTargetID()
        self.thdFindQuick.Do(id,self.idQuickFoundLast,sVal,100)
    def OnFindQuickFound(self,evt):
        evt.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.INFO,'id:%08d'%(evt.GetID()),self)
            self.idQuickFoundLast=evt.GetID()
            self.vgpTree.SelectByID('%08d'%self.idQuickFoundLast)
        except:
            vtLog.vtLngTB(self.GetName())
            vtLog.vtLngCurWX(vtLog.ERROR,'id:%08d'%(evt.GetID()),self)
    def OnTreeItemSel(self,event):
        node=event.GetTreeNodeData()
        self.viTag.SetNode(node)
    def OpenFile(self,fn):
        if self.netPrjTimer.ClearAutoFN()>0:
            self.netPrjTimer.Open(fn)
    def OpenCfgFile(self,fn=None):
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                self.netMaster.SetCfgNode()
                self.__setCfg__()
                return
        self.xdCfg.New(root='config')
        if fn is not None:
            self.xdCfg.SetFN(fn)
            self.netMaster.SetCfgNode()
        self.__setCfg__()
    def __setCfg__(self):
        try:
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSlwNavSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iWidth=event.GetDragRect().width
            #if iWidth>80:
            #    iWidth=80
            self.dCfg['nav_sash']=str(iWidth)
            
            self.slwNav.SetDefaultSize((iWidth, 1000))
            
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            self.GetCfgData()
            #event.Skip()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSlwTopSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iHeight=event.GetDragRect().height
            #if iHeight>80:
            #    iHeight=80
            self.dCfg['nav_top']=str(iHeight)
            
            self.slwTop.SetDefaultSize((1000, iHeight))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            self.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnSize(self, event):
        event.Skip()
        try:
            bMod=False
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            
            self.pnData.Refresh()
            self.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())

    def SetCfgData(self,func,l,d):
        self.cfgFunc=func
        self.cfgOrigin=l
        self.dCfg=d
        try:
            i=int(self.dCfg['nav_sash'])
            self.slwNav.SetDefaultSize((i, 1000))
        except:
            pass
        try:
            i=int(self.dCfg['nav_top'])
            self.slwTop.SetDefaultSize((1000, i))
        except:
            pass
        try:
            pos=map(int,self.dCfg['pos'].split(','))
            self.Move(pos)
        except:
            pass
        try:
            sz=map(int,self.dCfg['size'].split(','))
            self.SetSize(sz)
        except:
            pass
    def GetCfgData(self):
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,self.dCfg)
    def GetAboutData(self):
        import __config__
        version=u' %d.%d.%d.%d'%(__config__.VER_MAJOR,__config__.VER_MINOR,
                    __config__.VER_RELEASE,__config__.VER_SUBREL)
        desc=_(u"""Project timer module.

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
""")
        return _(u'VIDARC Project Timer'),desc,version

    def OnViTagToolXmlTagSelected(self, event):
        event.Skip()
        self.bxsData.Layout()

    def OnViTagToolXmlTagCancel(self, event):
        event.Skip()

    def OnViTagToolXmlTagApply(self, event):
        event.Skip()
    def __procGrouping__(self,node,d):
        sTag=self.netPrjTimer.getTagName(node)
        if sTag=='year':
            sYear=self.netPrjTimer.getNodeText(node,'tag')
            try:
                iYear=int(sYear)
                
                if iYear not in d:
                    d[iYear]=(node,{})
            except:
                pass
        elif sTag=='month':
            sMon=self.netPrjTimer.getNodeText(node,'tag')
            try:
                iMon=int(sMon)
            except:
                iMon=-1
                vtLog.vtLngCurWX(vtLog.WARN,'id:%s'%(self.netPrjTimer.getKey(node)),self)
                return 0
            nodePar=self.netPrjTimer.getParent(node)
            sYear=self.netPrjTimer.getNodeText(nodePar,'tag')
            try:
                iYear=int(sYear)
                
                if iYear not in d:
                    dMon={}
                    d[iYear]=(nodePar,dMon)
                else:
                    dMon=d[iYear][1]
                if iMon not in dMon:
                    dMon[iMon]=(node,{})
            except:
                pass
            pass
        return 0
    def __createYear__(self,oYear,node,iYear):
        oYear.SetTag(node,'%04d'%iYear)
    def __createMon__(self,oMon,node,iMon):
        oMon.SetTag(node,'%02d'%iMon)
    def __procGroup__(self,node,d,oPrjTime,dMove):
        try:
            sTag=self.netPrjTimer.getTagName(node)
            if sTag=='prjtime':
                #print
                #vtLog.CallStack('')
                dt=oPrjTime.GetDateCalc(node)
                iYear=dt.GetYear()
                iMon=dt.GetMonth()
                #print '__procGroup__',iYear,iMon
                if iYear in d:
                    nodeYear,dMon=d[iYear]
                else:
                    #print 'crate year',d.keys()
                    oYear=self.netPrjTimer.GetReg('year')
                    nodeYear=oYear.Create(self.netPrjTimer.getBaseNode(),
                            self.__createYear__,iYear)
                    #print nodeYear
                    dMon={}
                    d[iYear]=(nodeYear,dMon)
                if iMon in dMon:
                    nodeMon,dDay=dMon[iMon]
                else:
                    #print 'create mon',dMon.keys()
                    oMon=self.netPrjTimer.GetReg('month')
                    nodeMon=oMon.Create(nodeYear,
                            self.__createMon__,iMon)
                    #print nodeMon
                    dMon[iMon]=(nodeMon,{})
                #vtLog.CallStack('')
                #vtLog.pprint(d)
                nMon=self.netPrjTimer.getParent(node)
                if self.netPrjTimer.getTagName(nMon)!='month':
                    nMon=None
                if nMon is not None:
                    nYear=self.netPrjTimer.getParent(nMon)
                    if self.netPrjTimer.getTagName(nYear)!='year':
                        nYear=None
                else:
                    nYear=None
                bMove=False
                if nYear is None:
                    bMove=True
                if nMon is None:
                    bMove=True
                #print bMove,nYear is None,nMon is None
                if bMove:
                    idMon=self.netPrjTimer.getKey(nodeMon)
                    id=self.netPrjTimer.getKey(node)
                    if idMon in dMove:
                        dMove[idMon].append(id)
                    else:
                        l=[id]
                        dMove[idMon]=l
                    #self.netPrjTimer.moveNode(nodeMon,node)
                    #print nodeMon
                #print
                pass
        except:
            vtLog.vtLngTB(self.GetName())
        return 0
    def doGroup(self):
        d={}
        self.netPrjTimer.procChildsKeysRec(2,self.netPrjTimer.getBaseNode(),
                            self.__procGrouping__,d)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'grp:%s'%(vtLog.pformat(d)),self)
        
        dMove={}
        oPrjTime=self.netPrjTimer.GetReg('prjtime')
        self.netPrjTimer.procChildsKeysRec(10,self.netPrjTimer.getBaseNode(),
                            self.__procGroup__,d,oPrjTime,dMove)
        #vtLog.CallStack('')
        #vtLog.pprint(dMove)
        try:
            for idMon,lst in dMove.items():
                nodeMon=self.netPrjTimer.getNodeById(idMon)
                #print nodeMon
                for id in lst:
                    #print id
                    node=self.netPrjTimer.getNodeById(id)
                    self.netPrjTimer.moveNode(nodeMon,node)
        except:
            vtLog.vtLngTB(self.GetName())
    def GroupData(self,doc,node,*args,**kwargs):
        self.netPrjTimer.DoGrouping(None,None)
    def ArchiveMonth(self,doc,node,*args,**kwargs):
        vtLog.CallStack('')
        print args,kwargs
    def ReportData(self,doc,node,*args,**kwargs):
        try:
            if self.dlgReport is None:
                self.dlgReport=vPrjTimerReportDialog(self)
                self.dlgReport.Centre()
                self.dlgReport.SetDoc(self.netPrjTimer)
            nodeSetting=self.netPrjTimer.getChildForced(self.netPrjTimer.getRoot(),'settings')
            if nodeSetting is not None:
                self.dlgReport.SetNode(nodeSetting)
                if self.dlgReport.ShowModal()>0:
                    self.dlgReport.GetNode()
        except:
            vtLog.vtLngTB(self.GetName())
    def ReportDataImport(self,doc,node,*args,**kwargs):
        try:
            if self.dlgReportImp is None:
                self.dlgReportImp=vPrjTimerReportImportDialog(self)
                self.dlgReportImp.Centre()
                self.dlgReportImp.SetDoc(self.netPrjTimer)
                self.dlgReportImp.SetFunc(self.__doImport__)
            nodeSetting=self.netPrjTimer.getChildForced(self.netPrjTimer.getRoot(),'settings')
            if nodeSetting is not None:
                self.dlgReportImp.SetNode(nodeSetting)
                if self.dlgReportImp.ShowModal()>0:
                    self.dlgReportImp.GetNode()
        except:
            vtLog.vtLngTB(self.GetName())
    def __doImport__(self,sData):
        try:
            vtLog.vtLngCur(vtLog.DEBUG,'sData:%s'%(sData),self.GetName())
            iXmlStart=sData.find('<?xml')
            if iXmlStart>=0:
                nodeBase=self.netPrjTimer.getBaseNode()
                #print nodeBase
                #n=doc.AddNodeXmlContent(nodeBase,sData[iXmlStart:])
                self.netPrjTimer.acquire()
                idNew,idOld=self.netPrjTimer.AddNodeXmlContentExternal(nodeBase,sData[iXmlStart:])
                self.netPrjTimer.release()
                vtLog.vtLngCur(vtLog.DEBUG,'id:%s;impID:%s'%(idNew,idOld),self.GetName())
        except:
            vtLog.vtLngTB(self.GetName())
    def __showConsistanceNode__(self,doc,node,lang):
        s=''
        try:
            s=' '.join([doc.getNodeText(node,tag) for tag in ['person','date','project']])
        except:
            s='???'
        return s
    def __consistance__(self,node,oPrjTime):
        """ return iResult
    iResult shall have values:
        >= 1     .... inconsitance detected
           0     .... ok
        <=-1     .... fault -> stop search thread
"""
        #vtLog.CallStack('')
        #print node
        iRes=0
        sTag=self.netPrjTimer.getTagName(node)
        if sTag=='prjtime':
            fid=oPrjTime.GetPersonID(node)
            if fid<0:
                iRes|=0x001
            else:
                n=self.netHum.GetNodeByIdNum(fid)
                if n is None:
                    iRes|=0x002
            fid=oPrjTime.GetProjectID(node)
            if fid<0:
                iRes|=0x004
            else:
                n=self.netPrj.GetNodeByIdNum(fid)
                if n is None:
                    iRes|=0x008
            fid=oPrjTime.GetTaskID(node)
            if fid<0:
                iRes|=0x010
            else:
                n=self.netTask.GetNodeByIdNum(fid)
                if n is None:
                    iRes|=0x020
            fid=oPrjTime.GetLocID(node)
            if fid<0:
                iRes|=0x040
            else:
                n=self.netLoc.GetNodeByIdNum(fid)
                if n is None:
                    iRes|=0x080
            #dt=oPrjTime.GetDateCalc(node)
        return iRes
    def __equalize__(self,node,iRes,nodeRef,iVal2Set,bIncons,oPrjTime):
        #vtLog.CallStack('')
        #print node
        #print '0x%02x 0x%02x'%(iRes,iVal2Set)
        sTag=self.netPrjTimer.getTagName(node)
        if sTag=='prjtime':
            if bIncons:
                iMod=iRes&iVal2Set
            else:
                iMod=iVal2Set
            if iMod&0x001:
                id=oPrjTime.GetPersonID(nodeRef)
                oPrjTime.SetPersonID(node,id)
            if iMod&0x003:
                sVal=oPrjTime.GetPerson(nodeRef)
                oPrjTime.SetPerson(node,sVal)
            if iMod&0x004:
                id=oPrjTime.GetProjectID(nodeRef)
                oPrjTime.SetProjectID(node,id)
            if iMod&0x00C:
                sVal=oPrjTime.GetProject(nodeRef)
                oPrjTime.SetProject(node,sVal)
            if iMod&0x010:
                id=oPrjTime.GetTaskID(nodeRef)
                oPrjTime.SetTaskID(node,id)
            if iMod&0x030:
                sVal=oPrjTime.GetTask(nodeRef)
                oPrjTime.SetTask(node,sVal)
            if iMod&0x040:
                id=oPrjTime.GetLocID(nodeRef)
                oPrjTime.SetLocID(node,id)
            if iMod&0x0C0:
                sVal=oPrjTime.GetLoc(nodeRef)
                oPrjTime.SetLoc(node,sVal)
            if iMod&0x100:
                sDesc=oPrjTime.GetDesc(nodeRef)
                oPrjTime.SetDesc(node,sDesc)
            #print node
            #print '0x%02x'%(iMod)
        return 0
    def Consistance(self,doc,node,*args,**kwargs):
        try:
            vtLog.CallStack('')
            #print args,kwargs
            print node
            if self.dlgConsistance is None:
                oPrjTime=self.netPrjTimer.GetReg('prjtime')
                self.dlgConsistance=veXmlConsistanceDialog(self,
                        tree_class=vXmlPrjTimerTree,
                        tag_name='prjtime',
                        show_node_func=self.__showConsistanceNode__,
                        columns=[
                            (_(u'user'),80,wx.LIST_FORMAT_LEFT),
                            (_(u'project'),80,wx.LIST_FORMAT_LEFT),
                            (_(u'date'),140,wx.LIST_FORMAT_LEFT),
                            (_(u'start'),120,wx.LIST_FORMAT_LEFT),
                            (_(u'id'),80,wx.LIST_FORMAT_LEFT),
                                ],
                        show_attrs=[
                            'person','project','date','starttime','|id'
                            ],
                        check_result={
                            0x001:_(u'person ID not defined'),
                            0x002:_(u'person not found'),
                            0x004:_(u'project ID not defined'),
                            0x008:_(u'project not found'),
                            0x010:_(u'task ID not defined'),
                            0x020:_(u'task not found'),
                            0x040:_(u'location ID not defined'),
                            0x080:_(u'location not found'),
                            0x100:_(u'description'),
                            },
                        )
                self.dlgConsistance.Centre()
                self.dlgConsistance.SetDoc(self.netPrjTimer,True)
                self.dlgConsistance.SetFunc(self.__consistance__,oPrjTime)
                self.dlgConsistance.SetFuncEqualize(self.__equalize__,oPrjTime)
            self.dlgConsistance.SetNode(node)
            if self.dlgConsistance.ShowModal()>0:
                pass
        except:
            vtLog.vtLngTB(self.GetName())
    def __getRes__(self,dRes,l):
        d=dRes
        for it in l:
            if it in d:
                d=d[it]
            else:
                dd={}
                d[it]=dd
                d=dd
        return d
    def __calcDtTmValues__(self,node,oPrjTime,zDt,zStart,zEnd):
        sDt=oPrjTime.GetDate(node)
        try:
            zDt.SetDateSmallStr(sDt)
            
            sStart=oPrjTime.GetStartTime(node)
            sEnd=oPrjTime.GetEndTime(node)
            zStart.SetStr(sStart)
            zEnd.SetStr(sEnd)
            return True
        except:
            vtLog.vtLngTB(self.GetName())
            return False
    def __validate__(self,node,oPrjTime,zDt,zStart,zEnd,zDiff,dLimit,dRes):
        """ return iResult
    iResult shall have values:
        >= 1     .... inconsitance detected
           0     .... ok
        <=-1     .... fault -> stop search thread
"""
        #vtLog.CallStack('')
        #print node
        iRes=0
        if node is None:
            for k in dRes.keys():
                del dRes[k]
            return iRes
        else:
            sTag=self.netPrjTimer.getTagName(node)
            if sTag=='prjtime':
                fid=oPrjTime.GetPersonID(node)
                if fid<0:
                    vtLog.vtLngCurWX(vtLog.WARN,'id:%s person undefined'%(self.netPrjTimer.getKey(node)),self)
                    return 0
                else:
                    n=self.netHum.GetNodeByIdNum(fid)
                    if n is None:
                        vtLog.vtLngCurWX(vtLog.WARN,'id:%s personID:%08d missing link'%(self.netPrjTimer.getKey(node),fid),self)
                        return 0
                if self.__calcDtTmValues__(node,oPrjTime,zDt,zStart,zEnd):
                    zStart.CalcDiff(zEnd,zDiff)
                    
                    iYear=zDt.GetYear()
                    iMon=zDt.GetMonth()
                    iDay=zDt.GetDay()
                    
                    d=self.__getRes__(dRes,[fid,iYear,iMon,iDay])
                    if 'sum' in d:
                        fVal=d['sum']
                    else:
                        fVal=0.0
                    fVal+=zDiff.GetFloatHour()
                    d['sum']=fVal
                    if fVal>dLimit['day_limit']:#10.0:
                        iRes|=0x001
                    if 'ids' in d:
                        l=d['ids']
                    else:
                        l=[]
                    l.append(long(self.netPrjTimer.getKey(node)))
                    d['ids']=l
                    if 'rng' in d:
                        l=d['rng']
                    else:
                        l=[]
                    sStart=zStart.GetStr()
                    sEnd=zEnd.GetStr()
                    t=(sStart,sEnd)
                    for tup in l:
                        if (sStart>=tup[0] and sStart<tup[1]):
                            iRes|=0x002
                        if (sEnd>tup[0] and sEnd<=tup[1]):
                            iRes|=0x004
                        if (sStart==tup[0] and sEnd==tup[1]):
                            iRes|=0x008
                    l.append(t)
                    d['rng']=l
                    if zDiff.GetFloatHour()>dLimit['break_after']:#6.0:
                        iRes|=0x010
                    
                    if 'res' in d:
                        iResStore=d['res']
                    else:
                        iResStore=0
                    iResStore|=iRes
                    d['res']=iResStore
                    if 'res_node' in d:
                        l=d['res_node']
                    else:
                        l=[]
                        d['res_node']=l
                    l.append(iRes)
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurWX(vtLog.DEBUG,'dRes:%s'%(vtLog.pformat(dRes)),self)
            return iRes
    def __validateAutoCorrect__(self,node,oPrjTime,zDt,zStart,zEnd,zDiff,dLimit,dRes):
        if node is None:
            return 0
        else:
            sTag=self.netPrjTimer.getTagName(node)
            if sTag=='prjtime':
                fid=oPrjTime.GetPersonID(node)
                if fid<0:
                    vtLog.vtLngCurWX(vtLog.WARN,'id:%s person undefined'%(self.netPrjTimer.getKey(node)),self)
                    return 0
                else:
                    n=self.netHum.GetNodeByIdNum(fid)
                    if n is None:
                        vtLog.vtLngCurWX(vtLog.WARN,'id:%s personID:%08d missing link'%(self.netPrjTimer.getKey(node),fid),self)
                        return 0
                if self.__calcDtTmValues__(node,oPrjTime,zDt,zStart,zEnd):
                    iYear=zDt.GetYear()
                    iMon=zDt.GetMonth()
                    iDay=zDt.GetDay()
                    
                    d=self.__getRes__(dRes,[fid,iYear,iMon,iDay])
                    #if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    #    vtLog.vtLngCurWX(vtLog.DEBUG,'dRes:%s'%(vtLog.pformat(dRes)),self)
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurWX(vtLog.DEBUG,'d:%s'%(vtLog.pformat(d)),self)
                    id=long(self.netPrjTimer.getKey(node))
                    idx=d['ids'].index(id)
                    iRes=d['res_node'][idx]
                    if iRes & 0x008:
                        # delete duplicate
                        #self.netPrjTimer.delNode(node)
                        return -1
                    elif iRes & 0x002:
                        # set starttime to endtime of previous
                        idPrev=d['ids'][idx-1]
                        nodePrev=self.netPrjTimer.getNodeByIdNum(idPrev)
                        sEnd=zEnd.GetStr()
                        sEndPrev=oPrjTime.GetEndTime(nodePrev)
                        if sEnd<sEndPrev:
                            # delete it
                            return -1
                        else:
                            oPrjTime.SetStartTime(node,sEndPrev)
                    elif iRes & 0x004:
                        # set endtime to starttime of next
                        idNext=d['ids'][idx+1]
                        nodeNext=self.netPrjTimer.getNodeByIdNum(idNext)
                        sStart=zStart.GetStr()
                        sStartNext=oPrjTime.GetStartTime(nodeNext)
                        if sStart>sStartNext:
                            # delete it
                            return -1
                        else:
                            oPrjTime.SetEndTime(node,sStartNext)
                    elif iRes & 0x001:
                        # 
                        fVal=0.0
                        for i in xrange(idx):
                            nodeTmp=self.netPrjTimer.getNodeByIdNum(d['ids'][i])
                            self.__calcDtTmValues__(nodeTmp,oPrjTime,zDt,zStart,zEnd)
                            zStart.CalcDiff(zEnd,zDiff)
                            fVal+=zDiff.GetFloatHour()
                        if fVal>dLimit['day_limit']:
                            return -1
                        else:
                            # calc remaining
                            f=dLimit['day_limit']-fVal
                            self.__calcDtTmValues__(node,oPrjTime,zDt,zStart,zEnd)
                            iHr=zStart.GetHour()
                            iMn=zStart.GetMinute()
                            fMn=f%1.0
                            iHr+=f-fMn
                            iMn+=int(fMn*60)
                            if iMn>=60:
                                iMn-=60
                                iHr+=1
                            zEnd.SetHour(iHr)
                            zEnd.SetMinute(iMn)
                            sEnd=zEnd.GetStr()
                            oPrjTime.SetEndTime(node,sEnd)
                    elif iRes & 0x010:
                        zStart.CalcDiff(zEnd,zDiff)
                        iHrS=zStart.GetHour()
                        iMnS=zStart.GetMinute()
                        iHrE=zEnd.GetHour()
                        iMnE=zEnd.GetMinute()
                        
                        zEnd.SetHour(iHrS+6)
                        zEnd.SetMinute(iMnS)
                        oPrjTime.SetEndTime(node,zEnd.GetStr())
                        
                        tmp=self.netPrjTimer.cloneNode(node,True)
                        iHrS=zEnd.GetHour()
                        iMnS=zEnd.GetMinute()
                        iMnS+=30
                        if iMnS>=60:
                            iMnS-=60
                            iHrS+=1
                        zStart.SetHour(iHrS)
                        zStart.SetMinute(iMnS)
                        f=zDiff.GetFloatHour()
                        f-=6.0
                        fMn=f%1.0
                        iHrS+=f-fMn
                        iMnS+=int(fMn*60)
                        if iMnS>=60:
                            iMnS-=60
                            iHrS+=1
                        zEnd.SetHour(iHrS)
                        zEnd.SetMinute(iMnS)
                        oPrjTime.SetStartTime(tmp,zStart.GetStr())
                        oPrjTime.SetEndTime(tmp,zEnd.GetStr())
                        
                        par=self.netPrjTimer.getParent(node)
                        self.netPrjTimer.appendChild(par,tmp)
                        self.netPrjTimer.addNode(par,tmp)
        return 0
    def Validate(self,doc,node,*args,**kwargs):
        try:
            if self.dlgValidate is None:
                oPrjTime=self.netPrjTimer.GetReg('prjtime')
                zDt=vtDateTime(True)
                zStart=vtTime()
                zEnd=vtTime()
                zDiff=vtTime()
                dRes={}
                dLimit={'day_limit':10.0,   # maximal working hours per day
                        'break_after':6.0,  # break after x hours
                        }
                self.dlgValidate=veXmlValidateDialog(self,
                        tree_class=vXmlPrjTimerTree,
                        tag_name='prjtime',
                        show_node_func=self.__showConsistanceNode__,
                        columns=[
                            (_(u'user'),80,wx.LIST_FORMAT_LEFT),
                            (_(u'project'),80,wx.LIST_FORMAT_LEFT),
                            (_(u'date'),140,wx.LIST_FORMAT_LEFT),
                            (_(u'start'),120,wx.LIST_FORMAT_LEFT),
                            (_(u'id'),80,wx.LIST_FORMAT_LEFT),
                                ],
                        show_attrs=[
                            'person','project','date','starttime','|id'
                            ],
                        check_result={
                            0x001:_(u'daily limit exceeded'),
                            0x002:_(u'start time intersect'),
                            0x004:_(u'end time intersect'),
                            0x008:_(u'identical start/end times'),
                            0x010:_(u'missing break'),
                            },
                        )
                self.dlgValidate.Centre()
                self.dlgValidate.SetDoc(self.netPrjTimer,True)
                self.dlgValidate.SetFunc(self.__validate__,oPrjTime,zDt,
                        zStart,zEnd,zDiff,dLimit,dRes)
                self.dlgValidate.SetFuncCorrect(self.__validateAutoCorrect__,oPrjTime,zDt,
                        zStart,zEnd,zDiff,dLimit,dRes)
            self.dlgValidate.SetNode(node)
            self.dlgValidate.Show(True)
        except:
            vtLog.vtLngTB(self.GetName())
    def SendData(self,doc,node,*args,**kwargs):
        vtLog.CallStack('')
        print args,kwargs
        print node
        #veEmlSendPanel
