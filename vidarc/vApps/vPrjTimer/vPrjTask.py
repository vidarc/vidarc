#----------------------------------------------------------------------------
# Name:         vPrjTask.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vPrjTask.py,v 1.1 2005/12/13 13:20:08 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from xml.dom.minidom import Element
import string
import boa.apps.xmlBase.vtXmlDomTree as vtXmlDomTree

def GetXmlPrj(node):
    """return a sorted list of projects.
    """
    lst=[]
    n=node.getElementsByTagName('project')
    if len(n)>0:
        for o in n:
            id=o.getAttribute('id')
            s=vtXmlDomTree.getNodeText(o,'name')
            lst.append((s,id))
    return lst
def GetXmlHum(node):
    """return a sorted list of humans.
    """
    lst=[]
    n=node.getElementsByTagName('user')
    if len(n)>0:
        for o in n:
            s=vtXmlDomTree.getNodeText(o,'name')
            lst.append(s)
    lst.sort()
    return lst
def GetXmlTask(node):
    """return a sorted list of tasks.
    """
    lst=[]
    n=node.getElementsByTagName('task')
    if len(n)>0:
        for o in n:
            s=''
            for cn in o.childNodes:
                if cn.nodeType==Element.TEXT_NODE:
                    s=s+cn.nodeValue
            lst.append(s)
    lst.sort()
    return lst

def GetXmlSubTask(node):
    """return a sorted list of subtasks.
    """
    lst=[]
    n=node.getElementsByTagName('subtask')
    if len(n)>0:
        for o in n:
            s=''
            for cn in o.childNodes:
                if cn.nodeType==Element.TEXT_NODE:
                    s=s+cn.nodeValue
            lst.append(s)
    lst.sort()
    return lst
        