#Boa:FramePanel:vXmlNodeMonthPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeMonthPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060723
# CVS-ID:       $Id: vXmlNodeMonthPanel.py,v 1.2 2010/03/03 02:17:15 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.art.vtThrobber
import wx.lib.buttons
import vidarc.tool.input.vtInputGridSetup

import sys

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase
from vidarc.tool.vtThread import vtThread

[wxID_VXMLNODEMONTHPANEL, wxID_VXMLNODEMONTHPANELCBUPDATE, 
 wxID_VXMLNODEMONTHPANELCHKDETAIL, wxID_VXMLNODEMONTHPANELNBVAL, 
 wxID_VXMLNODEMONTHPANELPBPROC, wxID_VXMLNODEMONTHPANELPNGRAPH, 
 wxID_VXMLNODEMONTHPANELPNLIST, wxID_VXMLNODEMONTHPANELTHRBUILD, 
 wxID_VXMLNODEMONTHPANELVIPARTS, 
] = [wx.NewId() for _init_ctrls in range(9)]

class vXmlNodeMonthPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_bxsProc_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbUpdate, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.thrBuild, 0, border=0, flag=wx.ALIGN_CENTER)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.chkDetail, 0, border=0, flag=0)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.nbVal, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsProc, 0, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.EXPAND)
        parent.AddWindow(self.pbProc, 0, border=4,
              flag=wx.TOP | wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND)

    def _init_coll_bxsList_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viParts, 1, border=4,
              flag=wx.BOTTOM | wx.LEFT | wx.RIGHT | wx.TOP | wx.EXPAND)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_nbVal_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.pnGraph, select=True,
              text=u'Graph')
        parent.AddPage(imageId=-1, page=self.pnList, select=False, text=u'List')

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=0, rows=3, vgap=0)

        self.bxsProc = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsList = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsProc_Items(self.bxsProc)
        self._init_coll_bxsList_Items(self.bxsList)

        self.SetSizer(self.fgsLog)
        self.pnList.SetSizer(self.bxsList)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEMONTHPANEL,
              name=u'vXmlNodeMonthPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(312, 251),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 224))
        self.SetAutoLayout(True)

        self.pbProc = wx.Gauge(id=wxID_VXMLNODEMONTHPANELPBPROC, name=u'pbProc',
              parent=self, pos=wx.Point(4, 210), range=10000, size=wx.Size(296,
              10), style=wx.GA_HORIZONTAL)
        self.pbProc.SetMinSize(wx.Size(-1, 13))
        self.pbProc.SetMaxSize(wx.Size(-1, 13))

        self.cbUpdate = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODEMONTHPANELCBUPDATE,
              bitmap=vtArt.getBitmap(vtArt.Synch), name=u'cbUpdate',
              parent=self, pos=wx.Point(0, 172), size=wx.Size(31, 30), style=0)
        self.cbUpdate.Bind(wx.EVT_BUTTON, self.OnCbUpdateButton,
              id=wxID_VXMLNODEMONTHPANELCBUPDATE)

        self.thrBuild = vidarc.tool.art.vtThrobber.vtThrobberDelay(id=wxID_VXMLNODEMONTHPANELTHRBUILD,
              name=u'thrBuild', parent=self, pos=wx.Point(39, 179),
              size=wx.Size(15, 15), style=0)

        self.chkDetail = wx.CheckBox(id=wxID_VXMLNODEMONTHPANELCHKDETAIL,
              label=_(u'details'), name=u'chkDetail', parent=self, pos=wx.Point(62,
              172), size=wx.Size(50, 13), style=0)
        self.chkDetail.SetValue(False)
        self.chkDetail.SetMinSize(wx.Size(-1, -1))

        self.nbVal = wx.Notebook(id=wxID_VXMLNODEMONTHPANELNBVAL, name=u'nbVal',
              parent=self, pos=wx.Point(0, 0), size=wx.Size(304, 168), style=0)
        self.nbVal.SetMinSize(wx.Size(-1, -1))

        self.pnList = wx.Panel(id=wxID_VXMLNODEMONTHPANELPNLIST, name=u'pnList',
              parent=self.nbVal, pos=wx.Point(0, 0), size=wx.Size(296, 142),
              style=wx.TAB_TRAVERSAL)

        self.pnGraph = wx.Panel(id=wxID_VXMLNODEMONTHPANELPNGRAPH,
              name=u'pnGraph', parent=self.nbVal, pos=wx.Point(0, 0),
              size=wx.Size(296, 142), style=wx.TAB_TRAVERSAL)

        self.viParts = vidarc.tool.input.vtInputGridSetup.vtInputGridSetup(id=wxID_VXMLNODEMONTHPANELVIPARTS,
              name=u'viParts', parent=self.pnList, pos=wx.Point(4, 4),
              size=wx.Size(288, 134))
        self.viParts.SetMinSize(wx.Size(-1, -1))

        self._init_coll_nbVal_Pages(self.nbVal)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vPrjTimer')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        self.thdBuild=vtThread(self)
        self.SetSuppressNetNotify(True)
        self.dVal={}
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def IsBusy(self):
        return self.thdBuild.IsRunning()
    def ClearWid(self):
        try:
            vtXmlNodePanel.Clear(self)
            # add code here
            self.dVal={}
            self.viParts.Clear()
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def SetRegNode(self,obj):
        vtXmlNodePanel.SetRegNode(self,obj)
        self.viParts.Setup(obj.GetSetup(),None)
    def SetNetDocs(self,d):
        if d.has_key('vHum'):
            dd=d['vHum']
            self.docHum=dd['doc']
        # add code here
    def SetDoc(self,doc,bNet=False):
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        # add code here
    def __procValues__(self,node,oPrjTime,dVal,dUsrId,thd):
        id=self.doc.getKey(node)
        dt=oPrjTime.GetDateCalc(node)
        iDay=dt.GetDay()
        if iDay in dVal:
            dUsr=dVal[iDay]
        else:
            dUsr={}
            dVal[iDay]=dUsr
        #sUsr=oPrjTime.GetPerson(node)
        sUsr=oPrjTime.GetPersonIDStr(node)
        if sUsr not in dUsrId:
            dUsrId[sUsr]=[]
        if sUsr in dUsr:
            fSum=dUsr[sUsr]
        else:
            fSum=0.0
            dUsr[sUsr]=fSum
        fDur=oPrjTime.GetDuration(node)
        fSum+=fDur
        dUsr[sUsr]=fSum
        if thd.Is2Stop():
            return -1
        return 0
    def doProcValues(self,node,oPrjTime,dVal,thd):
        dUsrId={}
        self.doc.procChildsKeys(node,self.__procValues__,oPrjTime,dVal,dUsrId,thd)
        for usrId in dUsrId.keys():
            l=dUsrId[usrId]
            netHum,nodeUsr=self.doc.GetNode(usrId)
            for sTag in ['name','surname','firstname']:
                l.append(netHum.getNodeText(nodeUsr,sTag))
            l.append(0.0)
            #l.append(usrId)
        l=zip(dUsrId.values(),dUsrId.keys())
        l.sort()
        lVals=[]
        days=dVal.keys()
        days.sort()
        for day in days:
            dUsr=dVal[day]
            sDay='%02d'%day
            for tup,fid in l:
                #fid=tup[-1]
                if fid in dUsr:
                    fVal=dUsr[fid]
                    sVal='%4.2f'%fVal
                    tup[-1]=tup[-1]+fVal
                    lVal=[sDay,tup[0],sVal,tup[1],tup[2]]
                    lVals.append(lVal)
                    #lVal.append(sDay)
        self.viParts.SetValue(lVals)
    def SetNode(self,node):
        try:
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            dVal={}
            oPrjTime=self.doc.GetReg('prjtime')
            self.thdBuild.Do(self.doProcValues,self.node,oPrjTime,dVal,self.thdBuild)
            # add code here
            #tup=self.objRegNode.GetValues(node)
            #self.viParts.SetValue(tup[0])
            self.viParts.SetModified(False)
        except:
            self.__logTB__()
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if node is None:
                return
            # add code here
            #tup=self.viParts.GetValueStr()
            #self.objRegNode.SetValues(node,tup[0],tup[1])
            
            self.thdBuild.Stop()
            self.viParts.SetModified(False)
            self.GetNodeFin(node)
        except:
            self.__logTB__()
    def Close(self):
        vtXmlNodePanel.Close(self)
        # add code here
    def Cancel(self):
        vtXmlNodePanel.Cancel(self)
        # add code here
    def Lock(self,flag):
        vtXmlNodePanel.Lock(self,flag)
        if flag:
            # add code here
            pass
        else:
            # add code here
            pass
    def OnCbUpdateButton(self, event):
        event.Skip()
