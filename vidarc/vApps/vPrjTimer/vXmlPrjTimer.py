#----------------------------------------------------------------------------
# Name:         vXmlPrjTimer.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vXmlPrjTimer.py,v 1.19 2012/12/23 23:07:30 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import vidarc.tool.log.vtLog as vtLog
    import vidarc.tool.lang.vtLgBase as vtLgBase

    #from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
    from vidarc.ext.xml.veXmlDomRegGrp import veXmlDomRegGrp
    from vidarc.tool.xml.vtXmlFilterType import vtXmlFilterType

    from vidarc.tool.xml.vtXmlNodeCfg import vtXmlNodeCfg

    from vidarc.vApps.vPrjTimer.vXmlNodePrjTimerRoot import vXmlNodePrjTimerRoot
    from vidarc.vApps.vPrjTimer.vXmlNodeYear import vXmlNodeYear
    from vidarc.vApps.vPrjTimer.vXmlNodeMonth import vXmlNodeMonth
    from vidarc.vApps.vPrjTimer.vXmlNodeEmployee import vXmlNodeEmployee
    from vidarc.vApps.vPrjTimer.vXmlNodeEmployeeRecent import vXmlNodeEmployeeRecent
    from vidarc.vApps.vPrjTimer.vXmlNodePrjTime import vXmlNodePrjTime
    from vXmlNodePrjTaskSum import vXmlNodePrjTaskSum

    from vidarc.tool.xml.vtXmlNodeCalc import vtXmlNodeCalc
    from vidarc.ext.data.veDataCollector import veDataCollector
    #from vidarc.vApps.vPrj.vXmlPrjInputTree import vXmlPrjInputTree
    #from vidarc.vApps.vHum.vXmlHumInputTree import vXmlHumInputTree
    #from vidarc.vApps.vLoc.vXmlLocInputTree import vXmlLocInputTree
    #from vidarc.vApps.vDoc.vXmlDocInputCombTree import vXmlDocInputCombTree
    import vidarc.vApps.vPrj.vPrjReg as vPrjReg
    import vidarc.vApps.vHum.vHumReg as vHumReg
    import vidarc.vApps.vLoc.vLocReg as vLocReg
    from vidarc.tool.xml.vtXmlGrpTree import vtXmlGrpTree
    from vidarc.tool.time.vtTimeDateGM import vtTimeDateGM
    from vidarc.tool.time.vtTime import vtDateTime
    from vidarc.tool.time.vtTime import vtTime
    import vidarc.ext.data.__register__
    import vidarc.ext.report.__register__
    import vidarc.vApps.vPrjTimer.__config__ as __config__
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

def getPluginData():
    return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02bIDAT8\x8d\x8d\x93\xcfKTQ\x14\xc7?o\xeeL\xf9\xaby\xfa\x9c\x1ff\x88\
\x8a\x15\x1aD\xad\xa4\x16\xfe\x07\x11\x08\xe2\xa0\x16D \xb4j#\x8aE\xb4\xee\
\x0fHp\x91\x1a\xb6\n\x8c\x16\xb9-\xa1\xc0\xc0ED#\x13\x8e\x98\xda$\xea8\x8e3\
\xf3\x1c\xcd\xf7\xee{\xb7\x85\xccX:Bwu\xee\xe1\xde\xc39\xdf\xf3\xf9j\x9aGp\
\xfc\\}\xd6\xa5N$\x81\xe8\xf0\x94v<\xe7\xf9\xdf\x87\xa5r\x00h\x1e\x81\xe6\
\x11l\xe76\x95\xb4,eK\xa9\x94rU~\xff\xb7z3\xf5V%S)\xa5\x94RRJ%-K9\x96\xa5\
\x1e\x8e\xddS\x85\x7f\xc5\x0e\x8c]\x17a\xee\xe2\x15\x02\xd7U,\xc4bd3\xdb\xac\
\xc6\x17\xc9\xe5L\x84\x10\x88\x83\x03<;\x19\xaeWw\x14\x1b\xf0\x16\x82=\\\xf6\
\xcc,\xca\x91X\xb6\xc3\xca\xe2wjt?\x1bk\xab\x84\xea\xeb\x11\xc2\x83e\x9a\xc8\
\xfc>\xae\xe3\x1eiP\x10l\x87JL\xca\x91\xb6M\xd9\x19\x1f\x9d\x91^6\xd3\x19nuE\
\xa8\xab\xabc\xfa\xdd4\xcb\xeb\x1b\x88\x80\xc1\xd9\x8a\xb2\xa2\xd8\xdeB\xa0W\
\xea\x94\x07t\x84\xef\xb0\xb2\x94\x92lj\x8b\xd9\xd9\xcf\xc4\x17\x16\x98|5\
\xc9\xa5\x96\x8b\x0c\x0e\r1\xf7~\xae\xb8\xa9\xa3\x11\xf6\x93\xacodq\x1c\x87\
\x1a]g-\x9d\xc6?6\xc6\xc8\xa7\x8f\x04Z\xdb\xe8\x8d\xf4\x90\xcafx\xf4\xf4\t-\
\rM'5p\\\x03\xc3\x08R\x1b\x00\xc7q\t\x84C<\xbfy\x83+\xadm\xf4\xf5\xdd\xa1\
\xca0X\xfe\xfa\x85\x8e\xf6vF'^@\xf0\x18\x07\xc2caYYv\xd2il\xdbf~~\x9epc\x13\
\xdd\xdd\x11\x9a\x9a\x9b\xf8\x19\x8b\xe1\x8bFi\xae\x0f\xd3s\xf7\xfe\xbf E\
\x87\xa74\xd7\xb5\xc8\xe7w\xc9\x99&\xd2\x96\xcc\xcc|\xa0\xa5\xb1\x19C\xf7\
\xe3\x00U\xf3\xdf\xa8hh\xe4\xdc\xe5V**\xab\x8a`y\x8b\x84i~BA\x1d\x7f\xb5\x86\
e[,\xc5\x97\t\x9f\x0f\x11O$\xb8\xa6Ws\xe1v'\x9a\xcf\xc7\xd2\xd2\n{\xb9l\x91\
\xce\xe2\x08\xe1\xa0\x07\xbf~X\xcb'|\x0c\x0c\x0c\xb0\xb0\x18'c\xe6I$\xb7\xd8\
\xaf\xf4\xb3\x94\xc9 \xa5\xcb\xaf\xc4\xcaI\x11\x1f\xbf\x1e\xc4\xb0C(\xa5\xf0\
\n/\xb9\\\x96\xb5\xcdu&FG\xe8\xef\x7f\x80Y\xbbCj+\xc9\xea\xea\x0f^\x8e\x8f\
\x1fY\xa1\x94\x1b\x0bl\xfc}?\xcdL%\xddX\xca\xce\xa7Y\xfc\x0f*\xdc\x1e\x01?d\
\xcap\x00\x00\x00\x00IEND\xaeB`\x82" 

import wx
def convTimeDuration(doc,node,sVal,zStart,zEnd,zDiff,date=None,lang=None):
    #vtLog.CallStack('')
    #print node
    sStart=doc.getNodeText(node,'starttime')
    sEnd=doc.getNodeText(node,'endtime')
    zStart.SetStr(sStart)
    zEnd.SetStr(sEnd)
    zStart.CalcDiff(zEnd,zDiff)
    #print zStart.GetStr(),zEnd.GetStr(),zDiff.GetStr()
    return '%4.2f'%zDiff.GetFloatHour(),

class vXmlPrjTimer(veXmlDomRegGrp):
    VERBOSE=0
    TAGNAME_REFERENCE='tag'     #FIXME
    TAGNAME_ROOT='projecttimer'
    NODE_ATTRS=[
            ('prj','prjId','project','fid'),
            ('task','taskId','task','fid'),
            ('loc','locId','loc','fid'),
            ('person','personId','person','fid'),
            ('desc',None,'desc',None),
            ('date',None,'date',None),
            ('starttime',None,'starttime',None),
            ('endtime',None,'endtime',None),
            ]
    APPL_REF=['vHum','vLoc','vDoc','vPrjDoc','vPrj','vTask','vGlobals']
    def __init__(self,attr='id',skip=[],synch=False,appl='vPrjTimer',verbose=0,
                audit_trail=True):
        global _
        _=vtLgBase.assignPluginLang('vPrjTimer')
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        veXmlDomRegGrp.__init__(self,attr=attr,skip=skip,synch=synch,
                        appl=appl,verbose=verbose,audit_trail=audit_trail)
        try:
            vidarc.ext.data.__register__.RegisterNodes(self,'vPrjTimer',bRegCollectorAsRoot=True,bRegDocAsRoot=True)
            vidarc.ext.data.__register__.RegisterDateSmall(self,'prjtime','date',_('date'))
            vPrjReg.Register4Collector(self,'prjtime','vPrj',_('project'))
            vHumReg.Register4Collector(self,'prjtime','vHum',_('person'))
            vLocReg.Register4Collector(self,'prjtime','vLoc',_('location'))
            oDataGrp=self.GetReg('dataGrp')
            oDataColl=self.GetReg('dataCollector')
            oDataColl.UpdateDataProcDict('prjtime',
                        {'':({'conv':convTimeDuration,
                                'args':(vtTime(),vtTime(),vtTime(),),
                                'kwargs':{},
                                'values':['duration'],
                                },)})
            dSrcWid=oDataColl.GetSrcWidDict()
            dataProc=oDataColl.GetDataProcDict()
            
            oRoot=vXmlNodePrjTimerRoot('root')
            self.RegisterNode(oRoot,False)
            oBase=vXmlNodePrjTimerRoot()
            self.RegisterNode(oBase,True)
            
            oYear=vXmlNodeYear(dSrcWid=dSrcWid,srcAppl='vPrjTimer',dataProc=dataProc)
            oMonth=vXmlNodeMonth(dSrcWid=dSrcWid,srcAppl='vPrjTimer',dataProc=dataProc)
            oEmployee=vXmlNodeEmployee(dSrcWid=dSrcWid,srcAppl='vPrjTimer',dataProc=dataProc)
            oEmployeeRecent=vXmlNodeEmployeeRecent()
            oPrjTime=vXmlNodePrjTime()
            oPrjTaskSum=vXmlNodePrjTaskSum()
            self.RegisterNode(oEmployeeRecent,True)
            self.RegisterNode(oYear,True)
            self.RegisterNode(oMonth,False)
            self.RegisterNode(oEmployee,False)
            self.RegisterNode(oPrjTime,False)
            self.RegisterNode(oPrjTaskSum,False)
            #oCfg=vtXmlNodeCfg()
            #self.RegisterNode(oCfg,True)
            self.RegisterNode(vtXmlNodeCalc(),False)
            self.LinkRegisteredNode(oBase,oYear)
            self.LinkRegisteredNode(oBase,oEmployeeRecent)
            #self.LinkRegisteredNode(oRoot,oPrjTime)
            self.LinkRegisteredNode(oBase,oDataColl)
            self.LinkRegisteredNode(oBase,oDataGrp)
            self.LinkRegisteredNode(oYear,oDataColl)
            self.LinkRegisteredNode(oYear,oDataGrp)
            self.LinkRegisteredNode(oYear,oMonth)
            self.LinkRegisteredNode(oMonth,oEmployee)
            self.LinkRegisteredNode(oEmployee,oPrjTime)
            self.LinkRegisteredNode(oEmployee,oPrjTaskSum)
            vidarc.ext.report.__register__.RegisterNodes(self,bRegDocAsRoot=False)
            oDocs=self.GetReg('Documents')
            #self.LinkRegisteredNode(oBase,oDocs)
        except:
            vtLog.vtLngTB(self.appl)
        self.SetDftLanguages()
    def __genIds__(self,c,node,ids):
        sTag=self.getTagName(c)
        if sTag in self.skip:
            return 0
        id=self.getAttribute(c,self.attr)
        if self.IsKeyValid(id):
            ids.CheckId(c,self,id)
            self.updateParentDict(node,c)
        elif sTag in ['prjtime']:
            ids.CheckId(c,self,id)
            self.updateParentDict(node,c)
        self.iElemIDCount+=1
        #self.__genIds__(c,ids)
        self.procChildsKeys(c,self.__genIds__,c,ids)
        return 0
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'projtimes')
    #def New(self,revision='1.0',root='projecttimer',bConnection=False):
    #    if vtLog.vtLngIsLogged(vtLog.INFO):
    #        vtLog.vtLngCallStack(None,vtLog.INFO,'New',origin=self.appl)
    #    veXmlDomRegGrp.New(self,revision,root)
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elem=self.createSubNode(self.getRoot(),'projtimes')
        if bConnection==False:
            self.CreateReq()
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        #self.AlignDoc()
    def Build(self):
        vtLog.vtLngCur(vtLog.INFO,'',self.GetOrigin())
        veXmlDomRegGrp.Build(self)
        tmp=self.getChildByLst(None,['cfg','stateMachineLinker'])
        oSMlinker=self.GetRegisteredNode('stateMachineLinker')
        oSMlinker.SetNode(tmp)
    def GetStateMachine(self,nodeName,attrName):
        oSMlinker=self.GetRegisteredNode('stateMachineLinker')
        return oSMlinker.GetRunner(nodeName,attrName)
        
    def CreateNodeOld(self,parNode,**kwargs):
        if parNode is None:
            return None
        node=self.createSubNode(parNode,'prjtime')
        self.SetNode(node,**kwargs)
        self.AlignNode(node,iRec=2)
        return node
    def SetNode(self,node,**kwargs):
        if node is not None:
            for kwName,kwAttr,nodeName,attrName in self.NODE_ATTRS:
                try:
                    sName=kwargs[kwName]
                except:
                    sName=''
                bAddAttr=False
                try:
                    if kwAttr is not None:
                        sId=kwargs[kwAttr]
                        if sId is not None:
                            self.setNodeTextAttr(node,nodeName,sName,attrName,sId)
                            bAddAttr=True
                except:
                    pass
                if bAddAttr==False:
                    self.setNodeText(node,nodeName,sName)
    def GetPossibleAclOld(self):
        return {'all':self.ACL_MSK_NORMAL,
                'prjtime':self.ACL_MSK_NORMAL},['all','prjtime']
    def GetPossibleFilterOld(self):
        return {'prjtime':[('date',self.FILTER_TYPE_DATE),
                            ('person',self.FILTER_TYPE_STRING),
                            ('project',self.FILTER_TYPE_STRING),
                            ('task',self.FILTER_TYPE_STRING),
                            ('loc',self.FILTER_TYPE_STRING),
                            ('desc',self.FILTER_TYPE_STRING)]}
    def IsLoggedInSelf(self,node,usrId):
        #vtLog.vtLngCurWX(vtLog.DEBUG,'node:%s'%(self.getKey(node)),self)
        try:
            if node is not None:
                oEmployee=self.GetReg('employee')
                if self.getTagName(node)=='employee':
                    tmp=node
                elif self.getTagName(node)=='prjtime':
                    tmp=self.getParent(node)
                else:
                    return False
                id=oEmployee.GetEmployeeID(tmp)
                if usrId==id:
                    return True
        except:
            pass
        return False
        
    def __procGrouping__(self,node,d):
        sTag=self.getTagName(node)
        if sTag=='year':
            sYear=self.getNodeText(node,'tag')
            try:
                iYear=int(sYear)
                iId=self.getKeyNum(node)
                if iYear not in d:
                    d[iYear]=(iId,{})
            except:
                self.__logWarn__('id:%s'%(self.getKey(node)))
                pass
        elif sTag=='month':
            sMon=self.getNodeText(node,'tag')
            try:
                iMon=int(sMon)
            except:
                iMon=-1
                self.__logWarn__('id:%s'%(self.getKey(node)))
                return 0
            nodePar=self.getParent(node)
            sYear=self.getNodeText(nodePar,'tag')
            try:
                iYear=int(sYear)
                iId=self.getKeyNum(nodePar)
                
                if iYear not in d:
                    dMon={}
                    d[iYear]=(iId,dMon)
                else:
                    dMon=d[iYear][1]
                iId=self.getKeyNum(node)
                if iMon not in dMon:
                    dMon[iMon]=(iId,{})
            except:
                pass
            pass
        return 0
    def __createYear__(self,oYear,node,iYear):
        oYear.SetTag(node,'%04d'%iYear)
    def __createMon__(self,oMon,node,iMon):
        oMon.SetTag(node,'%02d'%iMon)
    def __createEmployee__(self,oEmployee,node,sFid,sTag):
        oEmployee.SetTag(node,sTag)
        oEmployee.SetAttrStr(node,'fid',sFid)
    def __createPrjTime__(self,oPrjTime,node,dVal):
        oPrjTime.SetProject(node,dVal['vPrjName'])
        oPrjTime.SetProjectID(node,dVal['vPrj'])
        oPrjTime.SetTask(node,dVal['vTaskName'])
        oPrjTime.SetTaskID(node,dVal['vTask'])
        oPrjTime.SetLoc(node,dVal['vLocName'])
        oPrjTime.SetLocID(node,dVal['vLoc'])
        oPrjTime.SetPerson(node,dVal['vHumName'])
        oPrjTime.SetPersonID(node,dVal['vHum'])
        oPrjTime.SetDesc(node,dVal['desc'])
        oPrjTime.SetDate(node,dVal['date'])
        oPrjTime.SetStartTime(node,dVal['start'])
        oPrjTime.SetEndTime(node,dVal['end'])
    def __procGetGrp__(self,node,iYr,iMh,iFid,dGrp):
        try:
            sTag=self.getTagName(node)
            if sTag=='year':
                sYr=self.getNodeText(node,'tag')
                iId=self.getKeyNum(node)
                try:
                    i=int(sYr)
                    if i==iYr:
                        dGrp['yr']=iId
                        self.procChildsKeysRec(0,node,
                                self.__procGetGrp__,iYr,iMh,iFid,dGrp)
                except:
                    self.__logError__('converstion fault;id%d,sYr:%s'(iId,sYr))
            elif sTag=='month':
                sMh=self.getNodeText(node,'tag')
                iId=self.getKeyNum(node)
                try:
                    i=int(sMh)
                    if i==iMh:
                        dGrp['mh']=iId
                        self.procChildsKeysRec(0,node,
                                self.__procGetGrp__,iYr,iMh,iFid,dGrp)
                except:
                    self.__logError__('converstion fault;id%d,sYr:%s'(iId,sYr))
            elif sTag=='employee':
                iId=self.getKeyNum(node)
                sFid=self.getAttribute(node,'fid')
                i,sAppl=self.getForeignKeyNumAppl(node,appl='vHum')
                if i==iFid:
                    dGrp['hum']=iId
        except:
            self.__logTB__()
        return 0
    def __createGroup__(self,iYr,iMh,sFid,sEmployee,dGrp):
        try:
            l=[]
            if dGrp['yr']<0:
                oYear=self.GetReg('year')
                c=oYear.Create(self.getBaseNode(),
                        self.__createYear__,iYr)
                l.append(c)
            else:
                l.append(self.getNodeByIdNum(dGrp['yr']))
            if dGrp['mh']<0:
                oMon=self.GetReg('month')
                c=oMon.Create(l[0],
                        self.__createMon__,iMh)
                l.append(c)
            else:
                l.append(self.getNodeByIdNum(dGrp['mh']))
            if dGrp['hum']<0:
                oEmployee=self.GetReg('employee')
                c=oEmployee.Create(l[1],
                        self.__createEmployee__,sFid,sEmployee)
                l.append(c)
            else:
                l.append(self.getNodeByIdNum(dGrp['hum']))
            return l
        except:
            vtLog.vtLngTB(self.GetOrigin())
    def CreatePrjTime(self,dVal,iKind=0):
        """
        iKind   ... 0 get open, 1 start, 2 stop
        """
        try:
            bDbg=False
            if VERBOSE>5:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    bDbg=True
                    vtLog.vtLngCur(vtLog.DEBUG,'%r'%(dVal),self.GetOrigin())
            #self.__logDebug__({'iKind':iKind,'dVal':dVal})
            vtLog.vtLngCur(vtLog.DEBUG,'iKind:%d;dVal:%r'%(iKind,dVal),self.GetOrigin())
            sDate=dVal['date']
            try:
                iYr=int(sDate[:4])
                iMh=int(sDate[4:6])
                dGrp={'yr':-1,'mh':-1,'hum':-1}
                iFid,sAppl=self.convForeign(dVal['vHum'])
                if iKind==1:
                    self.procChildsKeysRec(0,self.getBaseNode(),
                            self.__procGetGrp__,iYr,iMh,iFid,dGrp)
                    l=self.__createGroup__(iYr,iMh,dVal['vHum'],dVal['vHumName'],dGrp)
                    
                    oPrjTime=self.GetReg('prjtime')
                    c=oPrjTime.Create(l[2],self.__createPrjTime__,dVal)
                    iId=self.getKeyNum(c)
                    return iId
                elif iKind==2:
                    if 'id' in dVal:
                        node=self.getNodeByIdNum(dVal['id'])
                        if node is not None:
                            oPrjTime=self.GetReg('prjtime')
                            self.startEdit(node)
                            self.__createPrjTime__(oPrjTime,node,dVal)
                            self.doEdit(node)
                            self.endEdit(node)
                            return 1
                        else:
                            return -1
                    else:
                        return -1
                if bDbg:
                    #self.__logDebug__(dGrp)
                    vtLog.vtLngCur(vtLog.DEBUG,'%r'%(dGrp),self.GetOrigin())
            except:
                vtLog.vtLngTB(self.GetOrigin())
        except:
            vtLog.vtLngTB(self.GetOrigin())
