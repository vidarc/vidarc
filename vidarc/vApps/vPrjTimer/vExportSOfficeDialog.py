#Boa:Dialog:vExportSCalcDialog
#----------------------------------------------------------------------------
# Name:         vExportSOfficeDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vExportSOfficeDialog.py,v 1.2 2006/01/17 12:17:47 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.intctrl
import wx.lib.buttons
import sys
import os,string,calendar
from vidarc.tool.office.vtOfficeSCalc import *
from vidarc.tool.office.vtOfficeMSExcel import *

import images2

def create(parent):
    return vExportSCalcDialog(parent)

[wxID_VEXPORTSCALCDIALOG, wxID_VEXPORTSCALCDIALOGCHCDAY, 
 wxID_VEXPORTSCALCDIALOGGCBBBROWSEFN, wxID_VEXPORTSCALCDIALOGGCBBCANCEL, 
 wxID_VEXPORTSCALCDIALOGGCBBOK, wxID_VEXPORTSCALCDIALOGTXTFN, 
] = [wx.NewId() for _init_ctrls in range(6)]

class vExportSCalcDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VEXPORTSCALCDIALOG,
              name=u'vExportSCalcDialog', parent=prnt, pos=wx.Point(295, 159),
              size=wx.Size(551, 133), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vgd Export SCalc')
        self.SetClientSize(wx.Size(543, 106))

        self.gcbbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VEXPORTSCALCDIALOGGCBBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'gcbbOk',
              parent=self, pos=wx.Point(172, 72), size=wx.Size(76, 30),
              style=0)
        self.gcbbOk.Bind(wx.EVT_BUTTON, self.OnGcbbOkButton,
              id=wxID_VEXPORTSCALCDIALOGGCBBOK)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VEXPORTSCALCDIALOGGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(272, 72),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VEXPORTSCALCDIALOGGCBBCANCEL)

        self.txtFN = wx.TextCtrl(id=wxID_VEXPORTSCALCDIALOGTXTFN, name=u'txtFN',
              parent=self, pos=wx.Point(48, 8), size=wx.Size(384, 21), style=0,
              value=u'')

        self.gcbbBrowseFN = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VEXPORTSCALCDIALOGGCBBBROWSEFN,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbBrowseFN', parent=self, pos=wx.Point(440, 4),
              size=wx.Size(76, 30), style=0)
        self.gcbbBrowseFN.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseFNButton,
              id=wxID_VEXPORTSCALCDIALOGGCBBBROWSEFN)

        self.chcDay = wx.CheckBox(id=wxID_VEXPORTSCALCDIALOGCHCDAY,
              label=u'day', name=u'chcDay', parent=self, pos=wx.Point(48, 48),
              size=wx.Size(73, 13), style=0)
        self.chcDay.SetValue(False)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.idxMap=-1
        self.month=1
        self.appl=None
        
        img=images2.getApplyBitmap()
        self.gcbbOk.SetBitmapLabel(img)
        
        img=images2.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
        
        img=images2.getBrowseBitmap()
        self.gcbbBrowseFN.SetBitmapLabel(img)
        
    def __updateElem__(self):
        self.lstElement.DeleteAllItems()
        for e in ELEMENTS:
            try:
                index = self.lstElement.InsertImageStringItem(sys.maxint, e[0], -1)
                self.lstElement.SetStringItem(index,1,e[1]['__type'],-1)
            except:
                pass
    def GetFilter(self):
        lst=[]
        for i in range(0,self.lstElement.GetItemCount()):
            if self.lstElement.GetItemState(i,wx.LIST_STATE_SELECTED)>0:
                it=self.lstElement.GetItem(i,0)
                lst.append(it.m_text)
        return lst
    def __getTag__(self,base,node,lst):
        childs=vtXmlDomTree.getChilds(node)
        for c in childs:
            if c.nodeType==Element.ELEMENT_NODE:
                if len(base)>0:
                    s=base+'>'+c.tagName
                else:
                    s=c.tagName
                lst.append((s,c))
                self.__getTag__(s,c,lst)
    def Set(self,year,month,dPrj,lstPrj,dDay,node,doc):
        self.txtFN.SetValue('')
        self.year=year
        self.month=month
        self.lstPrj=lstPrj
        self.dPrj=dPrj
        self.dDay=dDay
    def OnGcbbOkButton(self, event):
        filename=self.txtFN.GetValue()
        
        sExts=string.split(filename,'.')
        if sExts[-1]=='sxc':
            bExcel=False
            self.appl=SCalc()
            self.appl.openFile(filename)
            self.appl.selSheet(u'%02d'%self.month)
        elif sExts[-1]=='xls':
            bExcel=True
            self.appl=Excel()
            self.appl.show()
            self.appl.openFile(filename)
            wrks=self.appl.getWorkbooks()
            print wrks
            self.appl.selWorkbook(wrks[0])
            shts=self.appl.getSheets()
            print shts
            self.appl.selSheet(u'%02d'%self.month)
        else:
            return
        if self.chcDay.GetValue()>0:
            days=calendar.monthcalendar(self.year,self.month)
            for w in days:
                i=0
                for d in w:
                    if d>0:
                        if bExcel:
                            self.appl.setCellValue(4+d,3,calendar.day_abbr[i])
                            self.appl.setCellValue(4+d,4,'%d'%d)
                        else:
                            self.appl.setCell(4+d,3,calendar.day_abbr[i])
                            self.appl.setCell(4+d,4,'%d'%d)
                    i+=1
        if self.appl is not None:
            for day in self.dDay.keys():
                d=self.dDay[day]
                for prj in d.keys():
                    idx=self.lstPrj.index(prj)
                    if bExcel:
                        self.appl.setCellValue(5+idx,4+day,'%4.2f'%d[prj])
                    else:
                        self.appl.setCellNum(4+day,5+idx,'%4.2f'%d[prj])
        idx=0
        for prjName in self.lstPrj:
            prj=self.dPrj[prjName]
            attr=vtXmlDomTree.getChild(prj[0],'attributes')
            prjId=vtXmlDomTree.getNodeText(attr,'prjnumber')
            clt=vtXmlDomTree.getNodeText(attr,'clientshort')
            if bExcel:
                self.appl.setCellValue(5+idx,0,prjId)
                self.appl.setCellValue(5+idx,2,clt)
                self.appl.setCellValue(5+idx,3,prjName)
            else:
                self.appl.setCell(0,5+idx,prjId)
                self.appl.setCell(2,5+idx,clt)
                self.appl.setCell(3,5+idx,prjName)
            
            idx+=1
        self.appl=None
        self.EndModal(1)
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()

    def OnGcbbBrowseFNButton(self, event):
        dlg = wx.FileDialog(self, "Open", ".", "", "XLS files (*.xls)|*.xls|SXC files (*.sxc)|*.sxc|all files (*.*)|*.*", wx.OPEN)
        try:
            dlg.Centre()
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.txtFN.SetValue(filename)
                
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()

        
