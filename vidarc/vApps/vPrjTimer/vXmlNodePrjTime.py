#----------------------------------------------------------------------------
# Name:         vXmlNodePrjTime.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060608
# CVS-ID:       $Id: vXmlNodePrjTime.py,v 1.8 2012/12/23 23:07:30 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#import vidarc.tool.log.vtLog as vtLog
#import vidarc.config.vcCust as vcCust
#import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        from vXmlNodePrjTimePanel import vXmlNodePrjTimePanel
        #from vXmlNodePrjTimeEditDialog import *
        #from vXmlNodePrjTimeAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

from vidarc.tool.time.vtTime import vtDateTime
from vidarc.tool.time.vtTime import vtTime

class vXmlNodePrjTime(vtXmlNodeBase):
    NODE_ATTRS=[
            #('Project',None,'project',None),
            #('Task',None,'task',None),
            #('Loc',None,'loc',None),
            #('Person',None,'person',None),
            #('Desc',None,'desc',None),
            #('Date',None,'date',None),
            #('StartTime',None,'starttime',None),
            #('EndTime',None,'endtime',None),
        ]
    FUNCS_GET_SET_4_LST=['SurName','FirstName','Name']
    XML_GROUPING=['year','month','employee']
    def __init__(self,tagName='prjtime'):
        global _
        _=vtLgBase.assignPluginLang('vPrjTimer')
        vtXmlNodeBase.__init__(self,tagName)
        self.dt=vtDateTime(True)
        self.zStart=vtTime()
        self.zEnd=vtTime()
        self.zDiff=vtTime()
    def GetDescription(self):
        return _(u'project time')
    # ---------------------------------------------------------
    # specific
    def GetProject(self,node):
        return self.Get(node,'project')
    def GetProjectID(self,node):
        return self.GetChildForeignKey(node,'project','fid','vPrj')
    def GetTask(self,node):
        return self.Get(node,'task')
    def GetTaskID(self,node):
        return self.GetChildForeignKey(node,'task','fid','vTask')
    def GetLoc(self,node):
        return self.Get(node,'loc')
    def GetLocID(self,node):
        return self.GetChildForeignKey(node,'loc','fid','vLoc')
    def GetPerson(self,node):
        fid=self.GetPersonIDStr(node)
        netDoc,nodeTmp=self.doc.GetNode(fid)
        if netDoc is None:
            return '???'
        if nodeTmp is None:
            return '???'
        return netDoc.getNodeText(nodeTmp,'name')
        #return self.Get(node,'name')
    def GetPersonID(self,node):
        return self.GetChildForeignKey(node,'person','fid','vHum')
    def GetPersonIDStr(self,node):
        return self.GetChildAttrStr(node,'person','fid')
    def GetDesc(self,node):
        return self.Get(node,'desc')
    def GetDate(self,node):
        return self.Get(node,'date')
    def GetStartTime(self,node):
        return self.Get(node,'starttime')
    def GetEndTime(self,node):
        return self.Get(node,'endtime')
    def GetID(self,node):
        return self.GetAttr(node,'id')
        
    def GetDateCalc(self,node):
        self.dt.SetDateSmallStr(self.GetDate(node))
        return self.dt
    def GetDuration(self,node):
        self.zStart.SetStr(self.GetStartTime(node))
        self.zEnd.SetStr(self.GetEndTime(node))
        self.zStart.CalcDiff(self.zEnd,self.zDiff)
        return self.zDiff.GetFloatHour()

    def SetProject(self,node,val):
        self.Set(node,'project',val)
    def SetProjectID(self,node,val):
        self.SetChildForeignKey(node,'project','fid',val,'vPrj')
    def SetTask(self,node,val):
        self.Set(node,'task',val)
    def SetTaskID(self,node,val):
        self.SetChildForeignKey(node,'task','fid',val,'vTask')
    def SetLoc(self,node,val):
        self.Set(node,'loc',val)
    def SetLocID(self,node,val):
        self.SetChildForeignKey(node,'loc','fid',val,'vLoc')
    def SetPerson(self,node,val):
        self.Set(node,'person',val)
    def SetPersonID(self,node,val):
        self.SetChildForeignKey(node,'person','fid',val,'vHum')
    def SetDesc(self,node,val):
        self.Set(node,'desc',val)
    def SetDate(self,node,val):
        self.Set(node,'date',val)
    def SetStartTime(self,node,val):
        self.Set(node,'starttime',val)
    def SetEndTime(self,node,val):
        self.Set(node,'endtime',val)
    
    def GetTimeNormal(self,node,iYear,iMon,iDay):
        oWrk=self.doc.GetRegisteredNode('workingtimes')
        if node is not None:
            c=self.doc.getChild(node,'workingtimes')
        else:
            c=None
        return oWrk.GetTimeNormal(c,iYear,iMon,iDay)
    
    def GetXmlGrouping(self):
        return self.XML_GROUPING
    def GetXmlGroupingData(self,node):
        self.GetDateCalc(node)
        sUsrID=self.GetPersonID(node)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurCls(vtLog.DEBUG,'tup:%s'%(vtLog.pformat([self.dt.GetYear(),self.dt.GetMonth(),sUsrID])),self)
        return [self.dt.GetYear(),self.dt.GetMonth(),sUsrID]
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL|vtSecXmlAclDom.ACL_MSK_MOVE
    def GetAttrFilterTypes(self):
        return [
                ('name',vtXmlFilterType.FILTER_TYPE_STRING),
                ('surname',vtXmlFilterType.FILTER_TYPE_STRING),
                ('firstname',vtXmlFilterType.FILTER_TYPE_STRING),
                ]
    def GetTranslation(self,name):
        _('name'),_('surname'),_('firstname')
        return _(name)
    def Is2Create(self):
        return False
    def Is2Add(self):
        return True
    def IsMultiple(self):
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        return False
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x02{IDAT(\x91U\x92\xddKSq\x18\xc7\xbf;\xfe\xb4m\xbe\xa6\xe78aiJ\x8a\xccn\
\xf4\xc2aA)\x04C+V\x910"\xf2\x85\xe8\x1f\xa8\x8b\x88\x08$\xf4\xc6\xf2\xa2\
\x1bC\xa2.J\xba\x08Z\xe0{\x104$\xd6\xa6s\x92FCK\x87\xb99\xdd9\x1b$\xe3L\xe7o\
\xcf\xe9b\xa6\xf9\xb9\xfc>\x9f\x8b\xef\xf3\xf0\xe8J\xea\xaf\x145\xfa\x88sJ\
\x13\x11\xa0\x81\xabDk\x8cI\x94\xd2\'\xa1\x11\x88\x90\x06\x00\xe8 \x7fj\xc2\
\xa9;\xe6\xadDD;\x80\xd2\xcb+\xbf\xa6\xc6\xa7V\x83\xab\xda\xff\xa4\xf7n\x0f]\
\x17D\x08\x0c\xac4\xc9\x10\xd9\x04\x00\x80\x08\xc7\xb2s\xfd\xdf\xfc\x06fP\
\xd5\x9dL\x08E\xc1\x96r\xa6\xa4\x8d\x08\x02\xa5\t\x00\x04!3\n,-\xbdx\xd6w\
\xc9f{\xf7\xfa\xf9\xcf\xe5%\x1c\xf0O`HC\xd1 \xc7b\x88\xc5\x00\x98D\xe9Q\xdf\
\xc0\x93\xa7\x03\xf7\xee>\x10\x04!\xf0#\x80\xa30\x10\x87\xae\xc8T&\x16\x17\
\x1f\xa6zc\x8e\xde\xa0\x070\xeb\x9b\r\x06\x83\xdd]\xdd\x15\'+\xdc\x017\x00\
\x81\x13\xf2rY\x9e\xf1\xd0\xde\xfe\xb3\xbd\xf1=0\xfcfxtd\xb4\xa7\xa7\'\xba\
\x15\xed\xec\xea\x0c\xfd\x0eM\xbe\x9dD\x12\x0c\xc4\x13\tE\x96\xe5\x8c-I\x92\
\xfb\xab\xbb\xe1\xd5\xcb\xb1HXo2\xf5>\xee\x05PYUy\xa3\xe3\xa6\xfd\xe2\xe5X<\
\x86*\x87$\xcb{\x91\x8d#7lmk\xed\xef\xefw}vi\x9a\xe6\xf1x\xfc~\xff\xf4\xf4\
\x17\xfbU;\x0c\x10H\x03\xcbb\x8c\x1dVZ\\X4\x99L\xd6FksK\xb3"+\xa9\xdd\xd4\
\xf1\xa9\t\xd1\\\xe6hw\x98\xc5rF\x04\xce\x15Y\xde/%I\x92\xf3\x83\xd3v\xc1\
\x96\xb1EI\\\xbf\xd5\xdb8\xf11\x1eUj\x1a\xac\xb5\xd55\x02\x08B\x96XXXk\xa9\
\xb3X\xea,,;\'\xb8\x1a\x04\xe0\xf5z\x8b%\x91\x03M\xe7\xces\x02\x07\xa2\xa15\
\x00\x02\x08D\xbc \x9f2}\xc2\x1bau[\rG\xc2;\x106e\x85\x13J\xef?\xe4@0\xb8\
\xc2\xb2\x98\x9aT\x05\xd2 \x8a,\xaf`\x7f\x89\xd3u\x96\xc1\xa1\xc1\x91\xf1\
\xb1\x9d\x84\x1a\x89\xc5\xe2Q%\xae(\xdff\xbdRy\xd5\xc2\xfc\x8co\xce\xaf3_+t\
\xb4w\x94\xee\x9a\x0f\x96\x0e\xaf\x87\x17\xa7\x03\x05\xf9FG\xbb\xa3\xdez6\
\x12Z\xe3\x84\x85\xf9\x19\xe7{\xa7\'\xe0\xd1\x01.\x18]\xe0G\x1f \x05\xe8`>\
\xb1R[\x1d\x06\xa0&U\xdf\x9c\x9d\xebRH\xb5\xfc\x05x{Qa\x841R\x9d\x00\x00\x00\
\x00IEND\xaeB`\x82'
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return {'name':self.__class__.__name__,
                    'pnName':'pnPrjTime',
                }
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return {'name':self.__class__.__name__,
                    'pnName':'pnPrjTime',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodePrjTimePanel
        else:
            return None

