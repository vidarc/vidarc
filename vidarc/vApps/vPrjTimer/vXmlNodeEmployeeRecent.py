#----------------------------------------------------------------------------
# Name:         vXmlNodeEmployeeRecent.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20121222
# CVS-ID:       $Id: vXmlNodeEmployeeRecent.py,v 1.2 2012/12/23 23:07:30 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import vLang
    
    from vidarc.tool.xml.vtXmlNodeTag import vtXmlNodeTag
    
    import vidarc.config.vcCust as vcCust
    if vcCust.is2Import(__name__):
        GUI=1
        import vidarc.vApps.vPrjTimer.images as imgPlg
        from vidarc.vApps.vPrjTimer.vXmlNodeEmployeeRecentPanel import vXmlNodeEmployeeRecentPanel
        vpc.logDebug('GUI objects imported',__name__)
    else:
        GUI=0
        vpc.logDebug('GUI objects import skipped',__name__)
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vXmlNodeEmployeeRecent(vtXmlNodeTag):
    #NODE_ATTRS=vtXmlNodeTag.NODE_ATTRS[
    #        ('Tag',None,'tag',None),
    #    ]
    #FUNCS_GET_SET_4_LST=['Tag','Name']
    #FUNCS_GET_4_TREE=['Tag','Name']
    #FMT_GET_4_TREE=[('Tag',''),('Name','')]
    def __init__(self,tagName='employeeRecent'):
        global _
        _=vLang.assignPluginLang('vPrjTimer')
        vtXmlNodeTag.__init__(self,tagName)
    def GetDescription(self):
        return _(u'employee recent')
    # ---------------------------------------------------------
    # specific
    def procEmployeeRecent(self,n,dCfg,bDbg=False):
        sTag=self.doc.getTagName(n)
        if sTag=='entryEmployeeRecent':
            try:
                d={}
                d['vPrj']=self.GetChildAttrStr(n,'project','fid')
                d['vTask']=self.GetChildAttrStr(n,'task','fid')
                d['vHum']=self.GetChildAttrStr(n,'person','fid')
                d['vLoc']=self.GetChildAttrStr(n,'loc','fid')
                d['name']=self.Get(n,'name')
                d['desc']=self.Get(n,'desc')
                if bDbg:
                    self.__logDebug__(d)
                dCfg[d['name']]=d
            except:
                self.__logTB__()
        return 1
    def GetEmployeeRecent(self,node):
        try:
            bDbg=False
            if VERBOSE>10:
                bDbg=True
            if bDbg:
                self.__logDebug__('')
            
            dCfg={}
            if self.doc is not None:
                c=self.GetChild(node,'cfgEmployeeRecent')
                if c is not None:
                    self.doc.procChildsNoKeys(c,self.procEmployeeRecent,dCfg,bDbg=bDbg)
        except:
            self.__logTB__()
        return dCfg
    def SetEmployeeRecent(self,node,dCfg):
        try:
            bDbg=False
            if VERBOSE>10:
                bDbg=True
            if bDbg:
                self.__logDebug__('')
            if self.doc is not None:
                c=self.GetChild(node,'cfgEmployeeRecent')
                if c is not None:
                    self.doc.__deleteNode__(c,node)
                c=self.doc.__createSubNodeLib__(node,'cfgEmployeeRecent')
                for k,v in dCfg.iteritems():
                    d={
                        'tag':'entryEmployeeRecent',
                        'lst':[
                            {'tag':'name','val':v['name']},
                            {'tag':'desc','val':v['desc']},
                            {'tag':'project','val':'','attr':('fid',v['vPrj'])},
                            {'tag':'task','val':'','attr':('fid',v['vTask'])},
                            {'tag':'loc','val':'','attr':('fid',v['vLoc'])},
                            {'tag':'person','val':'','attr':('fid',v['vHum'])},
                            ]
                        }
                    self.doc.createSubNodeDict(c,d)
        except:
            self.__logTB__()
    # ---------------------------------------------------------
    # inheritance
    def DoExec(self,parent,node,nodePar):
        try:
            self.__logInfo__(''%())
        except:
            self.__logTB__()
    def GetPossibleAcl(self):
        return vtXmlNodeTag._acl_all(self)
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def IsNotContained(self):
        "do not add to listbook"
        return True
    def getImageData(self):
        if GUI:
            return  imgPlg.getUserRecentData()
        else:
            return None
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return {'name':self.__class__.__name__,
                    'pnCls':vtXmlNodeTag.GetPanelClass(self),
                    'pnName':'pnNodeTag',
                    'pnClsAdd':self.GetPanelClass(),
                    'pnNameAdd':'pnNodeTagAdd',
                    
                }
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return {'name':self.__class__.__name__,
                    'pnCls':vtXmlNodeTag.GetPanelClass(self),
                    'pnName':'pnNodeTag',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeEmployeeRecentPanel
        else:
            return None
