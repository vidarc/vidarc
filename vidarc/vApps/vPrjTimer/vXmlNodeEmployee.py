#----------------------------------------------------------------------------
# Name:         vXmlNodeEmployee.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060905
# CVS-ID:       $Id: vXmlNodeEmployee.py,v 1.3 2010/03/03 02:17:15 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

#from vidarc.tool.xml.vtXmlNodeTag import *
from vidarc.ext.data.veDataCollectorCfg import veDataCollectorCfg

try:
    if vcCust.is2Import(__name__):
        from vXmlNodeEmployeePanel import vXmlNodeEmployeePanel
        #from vXmlNodeEmployeeEditDialog import *
        #from vXmlNodeEmployeeAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeEmployee(veDataCollectorCfg):
    #NODE_ATTRS=[
    #        ('Tag',None,'tag',None),
    #        ('Name',None,'name',None),
    #    ]
    #FUNCS_GET_SET_4_LST=['Tag','Name']
    def __init__(self,tagName='employee',dSrcWid=None,srcAppl=None,bChilds=True,
                    dataProc=None):
        global _
        _=vtLgBase.assignPluginLang('vPrjTimer')
        veDataCollectorCfg.__init__(self,tagName,dSrcWid=dSrcWid,srcAppl=srcAppl,bChilds=bChilds,
                    dataProc=dataProc)
    def GetDescription(self):
        return _(u'employee')
    # ---------------------------------------------------------
    # specific
    def GetEmployeeID(self,node):
        return self.GetForeignKey(node,'fid','vHum')
    def SetEmployeeID(self,node,fid):
        return self.SetForeignKey(node,'fid',fid,'vHum')
    def GetEmployeeIDStr(self,node):
        return self.GetAttrStr(node,'fid')
    def GetEmployee(self,node):
        fid=self.GetEmployeeIDStr(node)
        netDoc,nodeTmp=self.doc.GetNode(fid)
        if netDoc is None:
            return '???'
        if nodeTmp is None:
            return '???'
        return netDoc.getNodeText(nodeTmp,'name')
        #return self.Get(node,'name')
    def SetEmployee(self,node,val):
        self.SetTag(node,val)
    def GetSetup(self,node=None):
        return [
            {'tag':'day'      ,'trans':'day','label':_('day'),'typedef':{'type':'int','min':'1','max':'31'},'is2Report':1,'isCurrency':0},
            {'tag':'login'    ,'trans':'login','label':_('login'),'typedef':{'type':'string'},'is2Report':1,'isCurrency':0},
            {'tag':'sum'      ,'trans':'sum','label':_('sum'),'typedef':{'type':'float','digits':'2','comma':'2','min':'0','max':'24'},'is2Report':1,'isCurrency':0},
            {'tag':'surname'  ,'trans':'surname','label':_('surname'),'typedef':{'type':'string'},'is2Report':1,'isCurrency':0},
            {'tag':'firstname','trans':'firstname','label':_('firstname'),'typedef':{'type':'string'},'is2Report':1,'isCurrency':0},
            ]
    def GetXmlGroupingData(self,node):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurCls(vtLog.DEBUG,'id:%s;tagName:%s;val:%s'%\
                    (self.doc.getKey(node),self.doc.getTagName(node),self.GetForeignKey(node,'fid','vHum')),self)
        #print node
        return self.GetForeignKey(node,'fid','vHum')
    def SetXmlGroupingData(self,node,val):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurCls(vtLog.DEBUG,'id:%s;tagName:%s;val:%s;type:%s'%\
                    (self.doc.getKey(node),self.doc.getTagName(node),val,type(val)),self)
        self.SetForeignKey(node,'fid',val,'vHum')
        try:
            sVal=self.GetEmployee(node)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
            sVal=''
        self.SetTag(node,sVal)
    # ---------------------------------------------------------
    # inheritance
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01%IDAT8\x8d\xa5\x93=K\xc3P\x14\x86\x9f\xa4\x9a.\xb6D\xa9 ]\xa4C\xaa7\
\xa3uU3\x15\x07A\xc4\xbf\xe2\xac\xd9\xec\xd2\xdf\xa1\xa3\x83? U\xdc\xac\x9d4\
E\x11\xcc Q\x9a\xad]\xcbq\xa8\x15\x9a/\xac\x1e8p?\x0e\xcf}\xcf{\xef\xd54\xbd\
\xc0\x7fB\xcf\xda(\x1a\x8bb\x9ae\xf9\x13`\xbf\xd9\x94\xe0-\xa0u\xde\xa2RY\
\xc9\x85\xa4\x02,\xcb\xc2(\x1a\xd4\xd6k\x98\xcbf\xae\x82\x85\xb4\xc5\xf0\xf5\
\x82v\xbbL\xf8\x19\x12\r\xa2\\\x00\x9a^\x98I\xf7\xc4\x11\xe9\xba\xe26\x10\
\xb7\x81x\x07\x88\xb3\x8d\xc4\xeb\xa6\x99T0\xf2@\xf78\xbbv'\xf3\x07\x17\xe7\
\x14:\xf3\xb4\x00\xc0\xc77\xe0*\xbf\x83\xcck\xfcm$\x00\xde}\xb2h4\xcc!\xa4\
\x19#=dx\x89H\x0fy\xd9\xcd60\xdd\xc4\xe9\xa9}\xe0\x1d\xd6\xaa\xb0\xa9\xea\
\x02\xd0\xf7\x9f\xb5x]\xb6\x89\xc0R\t(M\xc6\xcaV([\x89\xff\xe4\xcf\x80\xe66Q\
\xd9\x8a\xa3\xe3\xc3\x9f\xe7\x9d\xaa\xa0\xd3\x85\x8d\x0c@\\\x81\x96\xf5\x9d\
\xf7\xb6\xc6\xe2T\xe11Xe\\\xdf!\x1aD\xdc\xde\xdc%<\xf8\x02;\x99Y\xc7\xf9\xc9\
\x06\xfb\x00\x00\x00\x00IEND\xaeB`\x82" 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return {'name':self.__class__.__name__,
                    'sz':(400, 420),'pnName':'pnEmployee',
                }
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return {'name':self.__class__.__name__,
                    'sz':(400, 420),'pnName':'pnEmployee',
                }
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeEmployeePanel
        else:
            return None
