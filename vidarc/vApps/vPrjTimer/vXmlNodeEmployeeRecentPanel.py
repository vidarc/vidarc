#----------------------------------------------------------------------------
# Name:         vXmlNodeEmployeeRecentPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20121222
# CVS-ID:       $Id: vXmlNodeEmployeeRecentPanel.py,v 1.2 2012/12/23 23:07:30 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import vLang
    from vGuiFrmWX import vGuiFrmWX
    import imgStat
    
    import vidarc.tool.art.vtArt as vtArt
    from vidarc.tool.input.vtInputTextML import vtInputTextML
    from vidarc.tool.input.vtInputText import vtInputText
    from vidarc.tool.input.vtInputTextMultiLine import vtInputTextMultiLine
    from vidarc.tool.time.vtTimeInputDateTimeStartEnd import vtTimeInputDateTimeStartEnd
    
    from vidarc.tool.xml.vtXmlNodeGuiPanel import vtXmlNodeGuiPanel
    
    from vidarc.vApps.vPrj.vXmlPrjInputTree import vXmlPrjInputTree
    from vidarc.vApps.vTask.vXmlTaskInputTree import vXmlTaskInputTree
    from vidarc.vApps.vHum.vXmlHumInputTreeUsr import vXmlHumInputTreeUsr
    from vidarc.vApps.vLoc.vXmlLocInputTree import vXmlLocInputTree
    from vidarc.tool.input.vtInputTree import vtInputTree
    
    import vidarc.vApps.vPrjTimer.__config__ as __config__
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vXmlNodeEmployeeRecentPanel(vtXmlNodeGuiPanel):
    def __initCtrl__(self,*args,**kwargs):
        global _
        _=vLang.assignPluginLang('vPrjTimer')
        if self.IsMainThread()==False:
            self.__logCritical__('called by thread'%())
        self.SetVerbose(__name__,iVerbose=-1,kwargs=kwargs)
        self.__clr__()
        lWid=[]
        iRow=0
        lWid.append(('lstCtrl',(iRow,0),(5,1),{'name':'lstDat','size':(100,80),
                'multiple_sel':False,'mark':True,
                'cols':[(_(u'name'),-1,-1,1),
                        #(_(u'person'),-1,60,0),
                        ]
                },{
                'lst_item_sel':    self.OnLstDatItemSelected,
                'lst_item_desel':  self.OnLstDatItemDeselected,
                }))
        lWid.append(('szBoxVert',(iRow,1),(5,1),{'name':'bxsDatBt',},[
                ('tgPopup',     0,16,{'name':'tgCfg','size':(32,32),
                        'iArrange':0,
                        'tip':_(u'configure source files'),
                        'bmp':vtArt.getBitmap(vtArt.Config)},None),
                ('cbBmp',0,4,{'name':'cbOpen',
                        'tip':_(u'open data'),
                        'bitmap':vtArt.getBitmap(vtArt.Open)},{
                        #'btn':self.OnOpenButton
                        }),
                ('cbBmp',0,20,{'name':'cbCfgClr',
                        'tip':_(u'clear configuration'),
                        'bitmap':vtArt.getBitmap(vtArt.Erase)},{
                        #'btn':self.OnCfgClrButton
                        }),
                ]))
        lWid.append(('szBoxVert',(iRow,6),(4,1),{'name':'bxsBt',},[
                ('cbBmp',0,32+4,{'name':'cbCfgAdd',
                        'tip':_(u'add configuration'),
                        'bitmap':vtArt.getBitmap(vtArt.Add)},{
                        'btn':self.OnCfgAddButton
                        }),
                ]))
        #iRow+=1
        iCol=2
        lWid.append(('lbl',             (iRow,iCol+0),(1,1),{'name':'lblName',
                'label':_(u'name'),
                },None))
        lWid.append(('txt',             (iRow,iCol+1),(1,1),{'name':'txtName',
                },None))
        iRow+=1
        lWid.append(('lbl',             (iRow,iCol+0),(1,1),{'name':'lblPrj',
                'label':_(u'project'),
                },None))
        lWid.append((vXmlPrjInputTree,  (iRow,iCol+1),(1,1),{'name':'vitrPrj',
                },None))
        lWid.append(('lbl',             (iRow,iCol+2),(1,1),{'name':'lblTask',
                'label':_(u'task'),
                },None))
        lWid.append((vXmlTaskInputTree,  (iRow,iCol+3),(1,1),{'name':'vitrTask',
                },None))
        iRow+=1
        lWid.append(('lbl',             (iRow,iCol+0),(1,1),{'name':'lblHum',
                'label':_(u'person'),
                },None))
        lWid.append((vXmlHumInputTreeUsr,  (iRow,iCol+1),(1,1),{'name':'vitrHum',
                },None))
        iRow+=1
        lWid.append(('lbl',             (iRow,iCol+0),(1,1),{'name':'lblLoc',
                'label':_(u'location'),
                },None))
        lWid.append((vXmlLocInputTree,  (iRow,iCol+1),(1,1),{'name':'vitrLoc',
                },None))
        lWid.append(('lbl',             (iRow,iCol+2),(1,1),{'name':'lblDate',
                'label':_(u'date'),
                },None))
        lWid.append((vtTimeInputDateTimeStartEnd,  (iRow,iCol+3),(1,1),{'name':'vinDateTime',
                },None))
        iRow+=1
        lWid.append(('lbl',             (iRow,iCol+0),(1,1),{'name':'lblDesc',
                'label':_(u'description'),
                },None))
        lWid.append(('txtLn',           (iRow,iCol+1),(1,3),{'name':'txtDesc',
                },None))
        lWid.append(('szBoxVert',(iRow,6),(1,1),{'name':'bxsBt',},[
                ('cbBmp',0,16+4,{'name':'cbAdd',
                        'tip':_(u'add'),
                        'bitmap':vtArt.getBitmap(vtArt.Add)},{
                        'btn':self.OnAddButton
                        }),
                ('cbBmp',0,4,{'name':'cbStart',
                        'tip':_(u'start'),
                        'bitmap':imgStat.getStartBitmap()},{
                        'btn':self.OnStartButton
                        }),
                ('cbBmp',0,4,{'name':'cbStop',
                        'tip':_(u'stop'),
                        'bitmap':imgStat.getStopBitmap()},{
                        'btn':self.OnStopButton
                        }),
                ]))
        iRow+=1
        lWid.append(('szBoxHor',(iRow,0),(1,1), {'name':'bxsPopup'},[
                ('cbBmp',       0, 4,{'name':'cbDatRd','size':(32,32),
                        'tip':_(u'read from PLC'),
                        'bitmap':vtArt.getBitmap(vtArt.ImportFree),},{
                        #'btn':self.OnDatRd
                        }),
                ]))
        vtXmlNodeGuiPanel.__initCtrl__(self,lWid=lWid)
        imgLst,dImg=self.lstDat.CreateImageList([
                ('busy',                vtArt.getBitmap(vtArt.Diagnose)),
                ],lDft=None,bSet=2)
        #self.PrintMsg(u'vXmlNodeEmployeeRecent init')
        self.__setUpPopUp__()
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            
            self.vitrHum.SetTagNames('person','name')
            self.vitrHum.SetAppl('vHum')
            self.vitrTask.SetTagNames('task','tag')
            self.vitrTask.SetAppl('vTask')
            self.vitrLoc.SetAppl('vLoc')
            self.vitrPrj.SetAppl('vPrj')
            self.vinDateTime.SetTagNames('date','starttime','endtime')
            #self.txtDesc.SetTagName('desc')
            
            self.vitrPrj.BindEvent('tr_chg',self.OnTreePrjChg)
        except:
            self.__logTB__()
        return [(4,1),(6,1)],[(0,1),]
    def __clr__(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            self.dCfg={}
            self.iIdStart=-1
        except:
            self.__logTB__()
    def __showDat__(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if self.IsMainThread()==False:
                return
            #self.lstDat.DeleteAllItems()
            l=[]
            for k,v in self.dCfg.iteritems():
                l.append([k,[k]])
            self.lstDat.SetValue(l)
        except:
            self.__logTB__()
    def __setUpPopUp__(self):
        if self.IsMainThread()==False:
            return
        try:
            self.__setUpPopUpCfg__()
        except:
            self.__logTB__()
    def __setUpPopUpCfg__(self):
        if self.IsMainThread()==False:
            return
        try:
            lWid=[]
            iRow=0
            lWid.append(('lbl',             (iRow,0),(1,1),{'name':'lblPrj',
                    'label':_(u'project'),
                    },None))
            p=vGuiFrmWX(name='popCfg',parent=self.tgCfg.GetWid(),kind='popupCls',
                    iArrange=0,widArrange=self.tgCfg.GetWid(),bAnchor=True,
                    #lGrowRow=[(1,1),],
                    lGrowCol=[(1,1),(3,1)],
                    #kwTopCmd={'name':'pnTopCmd','bFlexGridSizer':-1,'lWid':lWidTop},
                    lWid=lWid)
            #p.BindEvent('ok',self.OnSrcPopupOk)
            #self.vitrPrj=p.vitrPrj
            
            p.GetName()
            self.popCfg=p
            self.tgCfg.SetWidPopup(p)
            
        except:
            self.__logTB__()
    def __Clear__(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if self.IsMainThread()==False:
                return
            self.lstDat.DeleteAllItems()
            self.vitrPrj.Clear()
            self.bFixedUsr=False
            if self.doc is not None:
                if self.doc.GetLoggedInSecLv()<__config__.SEC_LEVEL_LIMIT_EXPERT:
                    if self.doc.IsConnActive():
                        usrID=self.doc.GetLoggedInUsrId()
                        sUsr=self.doc.GetLoggedInUsr()
                        self.bFixedUsr=True
                    else:
                        nodePar=self.doc.getParent(self.node)
                        if self.doc.getTagName(nodePar)=='employee':
                            oEmployee=self.doc.GetRegByNode(nodePar)
                            usrID=oEmployee.GetEmployeeID(nodePar)
                            sUsr=oEmployee.GetEmployee(nodePar)
                            self.bFixedUsr=True
            if self.bFixedUsr==True:
                self.vitrHum.SetValueID(sUsr,usrID)
            else:
                self.vitrHum.Clear()
            self.vitrLoc.Clear()
            self.vitrTask.Clear()
            self.vinDateTime.Clear()
            self.txtDesc.Clear()
            
            self.SetModified(False,self.lstDat)
        except:
            self.__logTB__()
        self.__clr__()
    def __Close__(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            self.CallWidChild(-1,'__Close__')
        except:
            self.__logTB__()
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            self.vitrPrj.SetDoc(doc)
            self.vitrHum.SetDoc(doc)
            self.vitrLoc.SetDoc(doc)
            self.vitrTask.SetDoc(doc)
            self.vinDateTime.SetDoc(doc)
            #self.txtDesc.SetDoc(doc)
            if dDocs is not None:
                if 'vHum' in dDocs:
                    dd=dDocs['vHum']
                    self.vitrHum.SetDocTree(dd['doc'],dd['bNet'])
                if 'vLoc' in dDocs:
                    dd=dDocs['vLoc']
                    self.vitrLoc.SetDocTree(dd['doc'],dd['bNet'])
                if 'vPrj' in dDocs:
                    dd=dDocs['vPrj']
                    self.vitrPrj.SetDocTree(dd['doc'],dd['bNet'])
                if 'vTask' in dDocs:
                    dd=dDocs['vTask']
                    self.vitrTask.SetDocTree(dd['doc'],dd['bNet'])
            
        except:
            self.__logTB__()
    def __SetNode__(self,node):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            self.dCfg=self.objRegNode.GetEmployeeRecent(node)
            self.__showDat__()
            #self.SetModified(False)
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            self.objRegNode.SetEmployeeRecent(node,self.dCfg)
            #self.SetModified(False)
        except:
            self.__logTB__()
    def __Lock__(self,bFlag):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if bFlag:
                pass
            else:
                pass
            
        except:
            self.__logTB__()
    def OnLstDatItemSelected(self,evt):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            lSel=self.lstDat.GetSelected([-1])
            if len(lSel)>0:
                d=self.dCfg[lSel[0][0]]
                self.txtName.SetValue(d.get('name',''))
                self.txtDesc.SetValue(d.get('desc',''))
                iId,sAppl=self.doc.convForeign(d.get('vPrj',-1))
                self.vitrPrj.SetValueID('',iId)
                iId,sAppl=self.doc.convForeign(d.get('vTask',-1))
                self.vitrTask.SetValueID('',iId)
                iId,sAppl=self.doc.convForeign(d.get('vLoc',-1))
                self.vitrLoc.SetValueID('',iId)
                iId,sAppl=self.doc.convForeign(d.get('vHum',-1))
                self.vitrHum.SetValueID('',iId)
                self.SetModified(False,[self.txtName,self.txtDesc])
        except:
            self.__logTB__()
    def OnLstDatItemDeselected(self,evt):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
        except:
            self.__logTB__()
    def OnCfgAddButton(self,evt):
        try:
            if evt is not None:
                evt.Skip()
            bDbg=self.GetVerbose(0)
            d={}
            d['vPrj']=self.vitrPrj.GetForeignID()
            d['vTask']=self.vitrTask.GetForeignID()
            d['vHum']=self.vitrHum.GetForeignID()
            d['vLoc']=self.vitrLoc.GetForeignID()
            d['name']=self.txtName.GetValue()
            d['desc']=self.txtDesc.GetValue()
            if bDbg:
                self.__logDebug__(d)
            if len(d['name'])>0:
                if d['name'] in self.dCfg:
                    bIns=False
                else:
                    bIns=True
                    self.lstDat.Insert([[d['name'],[d['name']]]],-1)
                self.dCfg[d['name']]=d
                self.SetModified(False,[self.txtName,self.txtDesc,])
                self.vitrPrj.SetModified(False)
                self.vitrTask.SetModified(False)
                self.vitrLoc.SetModified(False)
                self.vitrHum.SetModified(False)
                self.SetModified(True,self.lstDat)
        except:
            self.__logTB__()
    def OnTreePrjChg(self,evt):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            id=evt.GetId()
            self.vitrTask.SelectPrj(id)
        except:
            self.__logTB__()
    def GetValue(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            d={}
            d['vPrj']=self.vitrPrj.GetForeignID()
            d['vPrjName']=self.vitrPrj.GetValue()
            d['vTask']=self.vitrTask.GetForeignID()
            d['vTaskName']=self.vitrTask.GetValue()
            d['vHum']=self.vitrHum.GetForeignID()
            d['vHumName']=self.vitrHum.GetValue()
            d['vLoc']=self.vitrLoc.GetForeignID()
            d['vLocName']=self.vitrLoc.GetValue()
            d['name']=self.txtName.GetValue()
            d['desc']=self.txtDesc.GetValue()
            sDate,sStartTime,sEndTime=self.vinDateTime.GetValueStore()
            d['date']=sDate
            d['start']=sStartTime
            d['end']=sEndTime
            if self.iIdStart>=0:
                d['id']=self.iIdStart
            if bDbg:
                self.__logDebug__(d)
            
        except:
            self.__logTB__()
        return d
    def OnStartButton(self,evt):
        try:
            if evt is not None:
                evt.Skip()
            bDbg=self.GetVerbose(0)
            self.vinDateTime.Now()
            sDate,sStartTime,sEndTime=self.vinDateTime.GetValueStore()
            self.vinDateTime.SetValueStore(' '.join([sDate,sStartTime,'------']))
            d=self.GetValue()
            #self.vinDateTime.SetModified(True)
            #if self.doc is not None:
            #    iId=self.doc.CreatePrjTime(d,iKind=1)
            #    self.iIdStart=iId
        except:
            self.__logTB__()
    def OnStopButton(self,evt):
        try:
            if evt is not None:
                evt.Skip()
            bDbg=self.GetVerbose(0)
            sDate,sStartTime,sEndTime=self.vinDateTime.GetValueStore()
            self.vinDateTime.Now()
            sDate,sStartTime,sEndTime=self.vinDateTime.GetValueStore()
            self.vinDateTime.SetValueStore(' '.join([sDate,sStartTime,sEndTime]))
            d=self.GetValue()
            if self.doc is not None:
                self.doc.CreatePrjTime(d,iKind=1)
                self.SetModified(False,self.txtDesc)
                self.vitrPrj.SetModified(False)
                self.vitrTask.SetModified(False)
                self.vitrLoc.SetModified(False)
                self.vitrHum.SetModified(False)
                #if self.doc.CreatePrjTime(d,iKind=2)<0:
                #    self.doc.CreatePrjTime(d,iKind=1)
        except:
            self.__logTB__()
    def OnAddButton(self,evt):
        try:
            if evt is not None:
                evt.Skip()
            d=self.GetValue()
            if self.doc is not None:
                self.doc.CreatePrjTime(d,iKind=1)
                self.SetModified(False,self.txtDesc)
                sDateNew,sStartTimeNew,sEndTimeNew=self.vinDateTime.GetValueStore()
                self.vinDateTime.SetValueStore(' '.join([sDate,sStartTime,sEndTimeNew]))
                self.vitrPrj.SetModified(False)
                self.vitrTask.SetModified(False)
                self.vitrLoc.SetModified(False)
                self.vitrHum.SetModified(False)
        except:
            self.__logTB__()

