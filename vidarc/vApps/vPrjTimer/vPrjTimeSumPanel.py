#Boa:FramePanel:vPrjTimeSumPanel
#----------------------------------------------------------------------------
# Name:         vPrjTimeSumPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vPrjTimeSumPanel.py,v 1.1 2005/12/13 13:20:08 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import calendar,time
import string

#import boa.apps.xmlBase.vtXmlDomTree as vtXmlDomTree


[wxID_VPRJTIMESUMPANEL, wxID_VPRJTIMESUMPANELSCWMONTH, 
] = [wx.NewId() for _init_ctrls in range(2)]

BUFFERED=1

class vPrjTimeSumPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VPRJTIMESUMPANEL,
              name=u'vPrjTimeSumPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(500, 300), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(492, 273))

        self.scwMonth = wx.ScrolledWindow(id=wxID_VPRJTIMESUMPANELSCWMONTH,
              name=u'scwMonth', parent=self, pos=wx.Point(2, 2),
              size=wx.Size(500, 200), style=wx.HSCROLL | wx.VSCROLL)

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        
        self.__setDftPrjColors__()
        self.prjColor={}
        self.lst=None
        self.drawing=False
        self.selectableBlks=[]
        self.year=2005
        self.month=4
        #def grid x
        self.gridHr=12
        self.maxHrs=14
        self.gridX=self.gridHr*self.maxHrs
        
        #def grid y
        self.gridPrj=10
        self.maxPrj=3
        self.gridY=self.gridPrj*self.maxPrj
        self.countWidth=7
        self.countHeight=7
        self.maxWidth=self.gridX*self.countWidth
        self.maxHeight=self.gridY*self.countHeight
        self.scwMonth.SetVirtualSize((self.maxWidth+5, self.maxHeight+5))
        self.scwMonth.SetScrollRate(20,20)
        if BUFFERED:
            # Initialize the buffer bitmap.  No real DC is needed at this point.
            self.buffer = wx.EmptyBitmap(self.maxWidth+1, self.maxHeight+1)
            dc = wx.BufferedDC(None, self.buffer)
            dc.SetBackground(wx.Brush(self.scwMonth.GetBackgroundColour()))
            dc.Clear()
            self.DoDrawing(dc)
        self.scwMonth.Bind(wx.EVT_LEFT_DOWN, self.OnLeftButtonEvent)
        self.scwMonth.Bind(wx.EVT_LEFT_UP,   self.OnLeftButtonEvent)
        self.scwMonth.Bind(wx.EVT_MOTION,    self.OnLeftButtonEvent)
        
        self.scwMonth.Bind(wx.EVT_PAINT, self.OnPaint)
    def __setDftPrjColors__(self):
        self.colorPrj=[]
        self.colorPrj.append(wx.Colour(165, 145, 182))
        self.colorPrj.append(wx.Colour(169, 121, 165))
        self.colorPrj.append(wx.Colour(231, 125, 107))
        self.colorPrj.append(wx.Colour(255, 154, 000))
        self.colorPrj.append(wx.Colour(255, 207, 000))
        self.colorPrj.append(wx.Colour(25, 25, 112))
        self.colorPrj.append(wx.Colour(123, 0x20, 0x8F))
        self.colorPrj.append(wx.Colour(147, 112, 219))
        self.colorPrj.append(wx.Colour(186, 85, 211))
        self.colorPrj.append(wx.Colour(102, 205, 170))
        self.colorPrj.append(wx.Colour(176, 196, 222))
        self.colorPrj.append(wx.Colour(119, 136, 153))
        self.colorPrj.append(wx.Colour(144, 238, 144))
        self.colorPrj.append(wx.Colour(173, 216, 230))
        self.colorPrj.append(wx.Colour(75, 000, 130))
        self.colorPrj.append(wx.Colour(205, 92, 92))
        self.colorPrj.append(wx.Colour(218, 165, 32))
        self.colorPrj.append(wx.Colour(34, 139, 34))
        self.colorPrj.append(wx.Colour(178, 34, 34))
        self.colorPrj.append(wx.Colour(148, 000, 211))
        self.colorPrj.append(wx.Colour(47, 79, 79))
        self.colorPrj.append(wx.Colour(72, 61, 139))
        self.colorPrj.append(wx.Colour(85, 107, 47))
        self.colorPrj.append(wx.Colour(210, 105, 30))
        self.colorPrj.append(wx.Colour(000, 000, 128))
        self.colorPrj.append(wx.Colour(128, 128, 000))
        self.colorPrj.append(wx.Colour(218, 112, 214))
        self.colorPrj.append(wx.Colour(46, 139, 87))
        self.colorPrj.append(wx.Colour(192, 192, 192))
        self.colorPrj.append(wx.Colour(106, 90, 205))
        self.__setDftBrush__()
        self.__setDftPen__()
    def __setDftBrush__(self):
        self.brushPrj=[]
        for c in self.colorPrj:
            self.brushPrj.append(wx.Brush(c))
    def __setDftPen__(self):
        self.penPrj=[]
        for c in self.colorPrj:
            self.penPrj.append(wx.Pen(c,1))
    def SetXY(self, event):
        self.x, self.y = self.ConvertEventCoords(event)

    def ConvertEventCoords(self, event):
        xView, yView = self.scwMonth.GetViewStart()
        xDelta, yDelta = self.scwMonth.GetScrollPixelsPerUnit()
        return (event.GetX() + (xView * xDelta),
                event.GetY() + (yView * yDelta))
    def OnLeftButtonEvent(self, event):
        if event.LeftDown():
            #self.SetFocus()
            self.SetXY(event)
            #self.curLine = []
            #self.CaptureMouse()
            self.drawing = True
            for blks in self.selectableBlks:
                if (self.x>blks[0]) and (self.x<blks[2]):
                    if (self.y>blks[1]) and (self.y<blks[3]) and 1==0:
                        #print "found",blks[4]
                        if BUFFERED:
                            # If doing buffered drawing, create the buffered DC, giving it
                            # it a real DC to blit to when done.
                            cdc = wx.ClientDC(self.scwMonth)
                            self.PrepareDC(cdc)
                            dc = wx.BufferedDC(cdc, self.buffer)
                            dc.BeginDrawing()
                            dc.SetPen(wx.Pen('BLUE', 1))
                            dc.SetBrush(wx.LIGHT_GREY_BRUSH)
                            dc.DrawRectangle(blks[0], blks[1], 
                                    blks[2]-blks[0], blks[3]-blks[1])
                            dc.EndDrawing()
                            #self.scwMonth.Refresh()
            pass
        elif event.Dragging() and self.drawing:
            if BUFFERED:
                # If doing buffered drawing, create the buffered DC, giving it
                # it a real DC to blit to when done.
                cdc = wx.ClientDC(self)
                self.PrepareDC(cdc)
                dc = wx.BufferedDC(cdc, self.buffer)
            else:
                dc = wx.ClientDC(self)
                self.PrepareDC(dc)

            #dc.BeginDrawing()
            #dc.SetPen(wx.Pen('MEDIUM FOREST GREEN', 4))
            #coords = (self.x, self.y) + self.ConvertEventCoords(event)
            #self.curLine.append(coords)
            #dc.DrawLine(*coords)
            #self.SetXY(event)
            #dc.EndDrawing()


        elif event.LeftUp() and self.drawing:
            #self.lines.append(self.curLine)
            #self.curLine = []
            #self.ReleaseMouse()
            self.drawing = False
            #self.scwMonth.Refresh()
            pass

    def OnPaint(self, event):
        if BUFFERED:
            # Create a buffered paint DC.  It will create the real
            # wx.PaintDC and then blit the bitmap to it when dc is
            # deleted.  Since we don't need to draw anything else
            # here that's all there is to it.
            dc = wx.BufferedPaintDC(self.scwMonth, self.buffer, wx.BUFFER_VIRTUAL_AREA)
        else:
            dc = wx.PaintDC(self.scwMonth)
            self.PrepareDC(dc)
            # since we're not buffering in this case, we have to
            # paint the whole window, potentially very time consuming.
            self.DoDrawing(dc)


    def DoDrawing(self, dc, printing=False):
        dc.BeginDrawing()
        self.__drawGrid__(dc,printing=printing)
        self.__drawValues__(dc,printing=printing)
        self.__drawGridText__(dc,printing=printing)
        dc.EndDrawing()

    def oldDoDrawing(self):
        dc.SetPen(wx.Pen('RED'))
        dc.DrawRectangle(5, 5, 50, 50)

        dc.SetBrush(wx.LIGHT_GREY_BRUSH)
        dc.SetPen(wx.Pen('BLUE', 4))
        dc.DrawRectangle(15, 15, 50, 50)

        dc.SetFont(wx.Font(14, wx.SWISS, wx.NORMAL, wx.NORMAL))
        dc.SetTextForeground(wx.Colour(0xFF, 0x20, 0xFF))
        te = dc.GetTextExtent("Hello World")
        dc.DrawText("Hello World", 60, 65)

        dc.SetPen(wx.Pen('VIOLET', 4))
        dc.DrawLine(5, 65+te[1], 60+te[0], 65+te[1])

        lst = [(100,110), (150,110), (150,160), (100,160)]
        dc.DrawLines(lst, -60)
        dc.SetPen(wx.GREY_PEN)
        dc.DrawPolygon(lst, 75)
        dc.SetPen(wx.GREEN_PEN)
        dc.DrawSpline(lst+[(100,100)])

        #dc.DrawBitmap(self.bmp, 200, 20, True)
        #dc.SetTextForeground(wx.Colour(0, 0xFF, 0x80))
        #dc.DrawText("a bitmap", 200, 85)

##         dc.SetFont(wx.Font(14, wx.SWISS, wx.NORMAL, wx.NORMAL))
##         dc.SetTextForeground("BLACK")
##         dc.DrawText("TEST this STRING", 10, 200)
##         print dc.GetFullTextExtent("TEST this STRING")

        font = wx.Font(20, wx.SWISS, wx.NORMAL, wx.NORMAL)
        dc.SetFont(font)
        dc.SetTextForeground(wx.BLACK)

        for a in range(0, 360, 45):
            dc.DrawRotatedText("Rotated text...", 300, 300, a)

        dc.SetPen(wx.TRANSPARENT_PEN)
        dc.SetBrush(wx.BLUE_BRUSH)
        dc.DrawRectangle(50,500, 50,50)
        dc.DrawRectangle(100,500, 50,50)

        dc.SetPen(wx.Pen('RED'))
        dc.DrawEllipticArc(200,500, 50,75, 0, 90)

        if not printing:
            # This has troubles when used on a print preview in wxGTK,
            # probably something to do with the pen styles and the scaling
            # it does...
            y = 20

            for style in [wx.DOT, wx.LONG_DASH, wx.SHORT_DASH, wx.DOT_DASH, wx.USER_DASH]:
                pen = wx.Pen("DARK ORCHID", 1, style)
                if style == wx.USER_DASH:
                    pen.SetCap(wx.CAP_BUTT)
                    pen.SetDashes([1,2])
                    pen.SetColour("RED")
                dc.SetPen(pen)
                dc.DrawLine(300,y, 400,y)
                y = y + 10

        dc.SetBrush(wx.TRANSPARENT_BRUSH)
        dc.SetPen(wx.Pen(wx.Colour(0xFF, 0x20, 0xFF), 1, wx.SOLID))
        dc.DrawRectangle(450,50,  100,100)
        old_pen = dc.GetPen()
        new_pen = wx.Pen("BLACK", 5)
        dc.SetPen(new_pen)
        dc.DrawRectangle(470,70,  60,60)
        dc.SetPen(old_pen)
        dc.DrawRectangle(490,90, 20,20)

        #self.DrawSavedLines(dc)
        dc.EndDrawing()


    def __drawGrid__(self,dc,printing):
        dc.SetPen(wx.Pen('VIOLET', 1))
        for i in range(0,self.countHeight+1):
            y=i*self.gridY
            dc.DrawLine(0,y,self.maxWidth,y)
        for i in range(0,self.countWidth+1):
            x=i*self.gridX
            if i==0 or i==self.countWidth:
                y=0
            else:
                y=0
                #y=self.gridY
            dc.DrawLine(x,y,x,self.maxHeight)
        try:
            days=calendar.monthcalendar(self.year,self.month)
        except:
            t=time.localtime(time.time())
            days=calendar.monthcalendar(t[0],t[1])
            print "uupp"
        print self.year,self.month,days
        #dc.SetFont(wx.Font(8, wx.SWISS, wx.NORMAL, wx.NORMAL))
        #dc.SetTextForeground(wx.Colour(0x8F, 0x20, 0x8F))
        y=0
        #y=self.gridY
        gXh=self.gridX/2
        gYh=self.gridY/2
        #for i in range(0,7):
        #    sDay=calendar.day_abbr[i]
        #    te = dc.GetTextExtent(sDay)
        #    dc.DrawText(sDay, i*self.gridX +gXh-te[0]/2, y+gYh-te[1]/2)
        self.coorDay={}
        self.ofsDay={}
        for w in days:
            y=y+self.gridY
            i=0
            for d in w:
                if d>0:
                    sDay="%d"%d
                    self.coorDay[d]=(i*self.gridX,y)
                    self.ofsDay[d]=(8,0)
                    #te = dc.GetTextExtent(sDay)
                    #dc.DrawText(sDay, i*self.gridX +gXh-te[0]/2, y+gYh-te[1]/2)
                i=i+1
            #self.countWidth
        #dc.DrawLine(5, 65+te[1], 60+te[0], 65+te[1])

        #lst = [(100,110), (150,110), (150,160), (100,160)]
        #dc.DrawLines(lst,0)
        pass
    def __drawGridText__(self,dc,printing):
        dc.SetPen(wx.Pen('BLACK', 1))
        try:
            days=calendar.monthcalendar(self.year,self.month)
        except:
            t=time.localtime(time.time())
            days=calendar.monthcalendar(t[0],t[1])
            
        dc.SetFont(wx.Font(10, wx.SWISS, wx.NORMAL, wx.NORMAL))
        dc.SetTextForeground(wx.Colour(0x0, 0x0, 0x0))
        y=0
        #y=self.gridY
        gXh=self.gridX/2
        gYh=self.gridY/2
        for i in range(0,7):
            sDay=calendar.day_abbr[i]
            te = dc.GetTextExtent(sDay)
            dc.DrawText(sDay, i*self.gridX +gXh-te[0]/2, y+gYh-te[1]/2)
        dc.SetFont(wx.Font(10, wx.SWISS, wx.NORMAL, wx.BOLD))
        dc.SetTextForeground(wx.Colour(0x0, 0x0, 0x0))
        for w in days:
            y=y+self.gridY
            i=0
            for d in w:
                if d>0:
                    sDay="%d"%d
                    #self.coorDay[d]=(i*self.gridX,y)
                    #self.ofsDay[d]=(8,0)
                    te = dc.GetTextExtent(sDay)
                    dc.DrawText(sDay, i*self.gridX +gXh-te[0]/2, y+gYh-te[1]/2)
                i=i+1

    def __getCoor__(self,day):
        try:
            return self.coorDay[day]
        except:
            return (-1,-1)
    def __getOfsDay__(self,day):
        try:
            return self.ofsDay[day]
        except:
            return -1
    def SetYearMonth(self,year,month):
        self.year=year
        self.month=month
        if BUFFERED:
            # Initialize the buffer bitmap.  No real DC is needed at this point.
            self.buffer = wx.EmptyBitmap(self.maxWidth+2, self.maxHeight+2)
            dc = wx.BufferedDC(None, self.buffer)
            dc.SetBackground(wx.Brush(self.scwMonth.GetBackgroundColour()))
            dc.Clear()
            self.DoDrawing(dc)
        
        pass
    def SetDate(self,dt):
        if BUFFERED:
            # Initialize the buffer bitmap.  No real DC is needed at this point.
            self.buffer = wx.EmptyBitmap(self.maxWidth, self.maxHeight)
            dc = wx.BufferedDC(None, self.buffer)
            dc.SetBackground(wx.Brush(self.scwMonth.GetBackgroundColour()))
            dc.Clear()
            self.DoDrawing(dc)
        
        pass
    def SetValue(self,lst,year=None,month=None):
        if year is not None:
            self.year=year
        if month is not None:
            self.month=month
        self.lst=lst
        if BUFFERED:
            # Initialize the buffer bitmap.  No real DC is needed at this point.
            self.buffer = wx.EmptyBitmap(self.maxWidth+2, self.maxHeight+2)
            dc = wx.BufferedDC(None, self.buffer)
            dc.SetBackground(wx.Brush(self.scwMonth.GetBackgroundColour()))
            dc.Clear()
            self.DoDrawing(dc)
        self.scwMonth.Refresh()
        #self.OnPaint()
        
    def __drawValues__(self,dc,printing):
        if self.lst is None:
            return
        self.selectableBlks=[]
        self.processedPrj=[]
        for item in self.lst:
            print item
            coor=self.__getCoor__(item[0])
            ofs=self.__getOfsDay__(item[0])
            print item[0],ofs[0]
            ofsHr=ofs[0]
            ofsPrj=ofs[1]
            i=0
            yem=y=coor[1]
            drwPrj={}
            for prj in item[1]:
                # assign color first
                try:
                    indexColor=self.processedPrj.index([prj[0]])
                except:
                    indexColor=len(self.processedPrj)
                    self.processedPrj.append([prj[0]])
                try:
                    color=self.colorPrj[indexColor]
                    brush=self.brushPrj[indexColor]
                    pen=self.penPrj[indexColor]
                    #dc.SetPen(pen)
                    dc.SetPen(wx.Pen('BLACK', 1))
                    dc.SetBrush(brush)
                except:
                    dc.SetPen(wx.Pen('BLUE', 1))
                    dc.SetBrush(wx.LIGHT_GREY_BRUSH)
                try:
                    y=drwPrj[prj[0]]
                    if y<0:
                        continue
                except:
                    if len(drwPrj)>=(self.maxPrj+ofsPrj):
                        continue
                    if ofsPrj<=len(drwPrj):
                        #ok draw
                        drwPrj[prj[0]]=yem
                        y=yem
                        yem=yem+self.gridPrj
                    else:
                        #do not draw
                        drwPrj[prj[0]]=-1
                        continue
                ye=y+self.gridPrj
                hr=string.atoi(prj[1][:2])
                min=string.atoi(prj[1][2:4])
                min=round(min/5.0)
                xa=coor[0]
                if hr>=ofsHr:
                    xa=(hr-ofsHr)*self.gridHr+(min*self.gridHr/12)
                if xa<0:
                    xa=0
                hr=string.atoi(prj[2][:2])
                min=string.atoi(prj[2][2:4])
                min=round(min/5.0)
                xe=coor[0]
                if hr>=ofsHr:
                    xe=(hr-ofsHr)*self.gridHr+(min*self.gridHr/12)
                #dc.SetPen(wx.Pen('BLUE', 1))
                #dc.SetBrush(wx.LIGHT_GREY_BRUSH)
                if self.gridX <=xe:
                    xe=self.gridX
                dc.DrawRectangle(coor[0]+xa, y, xe-xa, self.gridPrj)
                self.selectableBlks.append((coor[0]+xa,y,coor[0]+xe,y+self.gridPrj,prj[-1]))
                y=ye
                    
    def __convertNode2InterVal__(self,node):
        sPrj=vtXmlDomTree.getNodeText(node,'project')
        #sTask=vtXmlDomTree.getNodeText(node,'task')
        #sSubTask=vtXmlDomTree.getNodeText(node,'subtask')
        #sLoc=vtXmlDomTree.getNodeText(node,'loc')
        sPerson=vtXmlDomTree.getNodeText(node,'person')
        #sDesc=vtXmlDomTree.getNodeText(node,'desc')
        sDate=vtXmlDomTree.getNodeText(node,'date')
        sStartTime=vtXmlDomTree.getNodeText(node,'starttime')
        sEndTime=vtXmlDomTree.getNodeText(node,'endtime')
        try:
            i=string.atoi(sStartTime[0])
            i=string.atoi(sEndTime[0])
        except:
            return None
        try:
            val=(string.atoi(sDate[-2:]),[sPrj,sStartTime,sEndTime,sPerson,node])
        except:
            val=None
        return val
    def SetNodes(self,nodes):
        lst=[]
        d={}
        firstNode=None
        for n in nodes:
            if firstNode is None:
                firstNode=n
                try:
                    sDate=vtXmlDomTree.getNodeText(firstNode,'date')
                    year=string.atoi(sDate[:4])
                    month=string.atoi(sDate[4:6])
                except:
                    firstNode=None
            intVal=self.__convertNode2InterVal__(n)
            if intVal is None:
                continue
            try:
                v=d[intVal[0]]
                v[1].append(intVal[1])
            except:
                d[intVal[0]]=(intVal[0],[intVal[1]])
        keys=d.keys()
        keys.sort()
        for k in keys:
            lst.append(d[k])
        self.SetValue(lst,year,month)
