#Boa:Dialog:vAnalyseConsistanceDialog
#----------------------------------------------------------------------------
# Name:         vAnalyseConsistanceDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vAnalyseConsistanceDialog.py,v 1.1 2005/12/13 13:20:07 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

from vidarc.tool.time.vTimeDateGM import *
from vidarc.tool.time.vTimeTimeEdit import *
import vidarc.tool.log.vtLog as vtLog
from vidarc.vApps.vPrjTimer.vModifyPanel import *

import thread,time,fnmatch,sys,types

import images

VERBOSE=0
FAULTY_PRJ=0x1
FAULTY_TSK=0x2
FAULTY_LOC=0x4
FAULTY_USR=0x8

# defined event for vgpXmlTree item selected
wxEVT_THREAD_CONSISTANCE_ELEMENTS=wx.NewEventType()
def EVT_THREAD_CONSISTANCE_ELEMENTS(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_CONSISTANCE_ELEMENTS,func)
class wxThreadConsistanceElements(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_CONSISTANCE_ELEMENTS(<widget_name>, self.OnItemSel)
    """

    def __init__(self,iStep,iVal,iCount=-1):
        wx.PyEvent.__init__(self)
        self.step=iStep
        self.val=iVal
        self.count=iCount
        self.SetEventType(wxEVT_THREAD_CONSISTANCE_ELEMENTS)
    def GetStep(self):
        return self.step
    def GetValue(self):
        return self.val
    def GetCount(self):
        return self.count
# defined event for vgpXmlTree item selected
wxEVT_THREAD_CONSISTANCE_ELEMENTS_FINISHED=wx.NewEventType()
def EVT_THREAD_CONSISTANCE_ELEMENTS_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_CONSISTANCE_ELEMENTS_FINISHED,func)
class wxThreadConsistanceElementsFinished(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_CONSISTANCE_ELEMENTS_FINISHED(<widget_name>, self.OnItemSel)
    """
    def __init__(self):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_THREAD_CONSISTANCE_ELEMENTS_FINISHED)
    

class thdConsistance:
    def __init__(self,par):
        self.verbose=VERBOSE
        self.tree=par
        self.lstSort=None
        self.dApply=None
        self.running=False
    def Find(self,dDoc):
        self.grp=self.tree.getGrp()
        self.dDoc=dDoc
        self.bFind=True
        
        self.ids={'hum':None,'loc':None,'prj':None,'task':None}
        for k in self.dDoc.keys():
            if k=='hum':
                self.ids[k]=self.dDoc[k].GetSortedUsrIds()
            elif k=='task':
                self.ids[k]=self.dDoc[k].GetTaskIds()
            else:
                self.ids[k]=self.dDoc[k].GetSortedIds()
                
        self.lstFound=[]
        self.iCount=self.tree.doc.GetElementCount()
        self.Start()
        self.lstSort=None
    def Apply(self,d):
        if len(self.lstFound)<=0:
            return
        self.dApply=d
        self.StartApply()
    def Clear(self):
        self.iCount=0
        self.iFound=0
            
        self.dElem={}
        if self.lstSort is not None:
            del self.lstSort
        self.lstSort=[]
        self.iAct=0
            
    def Show(self):
        self.bFind=False
        if self.running==False:
            self.Start()
    def Start(self):
        self.keepGoing = self.running = True
        thread.start_new_thread(self.Run, ())
    
    def Stop(self):
        self.keepGoing = False

    def IsRunning(self):
        return self.running
    def __procNode__(self,node,*args):
        if self.keepGoing==False:
            return
        bFound=False
        sTag=None
        sHier=None
        sTagName=None
        sName=None
        self.iAct+=1
        wx.PostEvent(self.tree,wxThreadConsistanceElements(1,self.iAct,self.iCount))
        #print node
        iRes=0
        n=self.tree.doc.getChild(node,'project')
        sId=self.tree.doc.getAttribute(n,'fid')
        if len(sId)<=0:
            iRes|=FAULTY_PRJ
            bFound=True
        else:
            tup=self.ids['prj'].GetInfoById(sId)
            if tup[0] is None:
                iRes|=FAULTY_PRJ
                bFound=True
        sPrjId=sId
        
        n=self.tree.doc.getChild(node,'person')
        sId=self.tree.doc.getAttribute(n,'fid')
        if len(sId)<=0:
            iRes|=FAULTY_USR
            bFound=True
        else:
            tup=self.ids['hum'].GetInfoById(sId)
            if tup[0] is None:
                iRes|=FAULTY_USR
                bFound=True
        
        n=self.tree.doc.getChild(node,'loc')
        sId=self.tree.doc.getAttribute(n,'fid')
        if len(sId)<=0:
            iRes|=FAULTY_LOC
            bFound=True
        else:
            tup=self.ids['loc'].GetInfoById(sId)
            if tup[0] is None:
                iRes|=FAULTY_LOC
                bFound=True
        
        n=self.tree.doc.getChild(node,'task')
        sId=self.tree.doc.getAttribute(n,'fid')
        if len(sId)<=0:
            iRes|=FAULTY_TSK
            bFound=True
        else:
            bTaskFound=False
            try:
                ids=self.ids['task']
                prjTasks=ids[sPrjId][1]
                try:
                    tup=prjTasks[sId]
                    bTaskFound=True
                except:
                    for v in prjTasks.values():
                        try:
                            tup=v[1][sId]
                            bTaskFound=True
                            break
                        except:
                            pass
            except:
                pass
            if bTaskFound==False:
                iRes|=FAULTY_TSK
                bFound=True
        if bFound:
            self.iFound+=1
            self.lstFound.append((node,iRes))
        return 0
    def __showNodes__(self):
        self.tree.lstRes.DeleteAllItems()
        self.iAct=0
        locIds=self.tree.netLoc.GetSortedIds()
        prjIds=self.tree.netPrj.GetSortedIds()
        #taskIds=self.tree.netTask.GetSortedIds()
        #tup=ids.GetInfoByIdx(i)
        
        #self.lstRes.InsertColumn(0,u'Week',wx.LIST_FORMAT_LEFT,40)
        #self.lstRes.InsertColumn(1,u'Date',wx.LIST_FORMAT_LEFT,85)
        #self.lstRes.InsertColumn(2,u'Client',wx.LIST_FORMAT_LEFT,85)
        #self.lstRes.InsertColumn(3,u'Prj',wx.LIST_FORMAT_LEFT,85)
        #self.lstRes.InsertColumn(4,u'Task',wx.LIST_FORMAT_LEFT,85)
        #self.lstRes.InsertColumn(5,u'Sum',wx.LIST_FORMAT_LEFT,85)
        dt=wx.DateTime()
        self.dRes={}
        
        for node,iRes in self.lstFound:
            #print node
            sPrj=self.tree.doc.getNodeText(node,'project')
            sDate=self.tree.doc.getNodeText(node,'date')
            sUsr=self.tree.doc.getNodeText(node,'person')
            sLoc=self.tree.doc.getNodeText(node,'loc')
            sTask=self.tree.doc.getNodeText(node,'task')
            zStart=self.tree.doc.getNodeText(node,'starttime')
            zEnd=self.tree.doc.getNodeText(node,'endtime')
            try:
                if zStart!='--:--:--':
                    zTmp='%s:%s:%s'%(zStart[0:2],zStart[2:4],zStart[4:6])
                else:
                    zTmp='--:--:--'
                zStart=zTmp
            except:
                pass
            try:
                zTmp='%s:%s:%s'%(zEnd[0:2],zEnd[2:4],zEnd[4:6])
                zEnd=zTmp
            except:
                pass
            dt.ParseFormat(sDate,'%Y%m%d')
            sYear=dt.Format('%Y')
            sMon=dt.Format('%m')
            sDay=dt.Format('%d')
            if (iRes & FAULTY_PRJ)!=0:
                sPrj=sPrj+' !'
            if (iRes & FAULTY_TSK)!=0:
                sTask=sTask+' !'
            if (iRes & FAULTY_USR)!=0:
                sUsr=sUsr+' !'
            if (iRes & FAULTY_LOC)!=0:
                sLoc=sLoc+' !'
            tup=[sYear,sMon,sDay,zStart,zEnd,sPrj,sTask,sUsr,sLoc,iRes,self.iAct]
            d=self.dRes
            for g in self.grp[:-1]:
                k=tup[g]
                try:
                    dF=d[k]
                    d=dF
                except:
                    dF={}
                    d[k]=dF
                    d=dF
            k=tup[self.grp[-1]]
            try:
                lst=d[k]
                lst.append(tup)
            except:
                d[k]=[tup]
                
            self.iAct+=1
            wx.PostEvent(self.tree,wxThreadConsistanceElements(2,self.iAct,self.iFound))
            if self.keepGoing==False:
                return
        d=self.dRes
        self.iAct=0
        def cmpFunc(a,b):
            for i in range(9):
                res=cmp(a[i],b[i])
                if res!=0:
                    return res
            return 0
        def addDict(d,level):
            keys=d.keys()
            keys.sort()
            #print keys
            for k in keys:
                v=d[k]
                if types.DictType==type(v):
                    addDict(v,level+1)
                    idx=self.grp[level]
                    #index = self.tree.lstRes.InsertImageStringItem(sys.maxint, '', -1)
                    #self.tree.lstRes.SetStringItem(index,idx,'sum',-1)
                    #self.tree.lstRes.SetStringItem(index,4,'%6.2f'%sum,-1)
                    #sumTotal+=sum
                else:
                    #print level,v
                    v.sort(cmpFunc)
                    for tup in v:
                        index = self.tree.lstRes.InsertImageStringItem(sys.maxint, tup[0], -1)
                        for i in range(1,9):
                            self.tree.lstRes.SetStringItem(index,i,tup[i],-1)
                        self.tree.lstRes.SetItemData(index,tup[-1])
                        #wx.PostEvent(self.tree,wxThreadAnalyseElements(4,index,self.iFound))
                        self.iAct+=1
                        wx.PostEvent(self.tree,wxThreadConsistanceElements(3,self.iAct,self.iFound))
                if self.keepGoing==False:
                    return -1
        addDict(self.dRes,0)
        self.iAct=0
        
    def Run(self):
        if self.bFind:
            self.Clear()
            self.tree.doc.procChildsAttr(self.tree.doc.getBaseNode(),'id',
                    self.__procNode__,None)
        if self.keepGoing:
            self.__showNodes__()
        if self.keepGoing==False:
            self.tree.lstRes.DeleteAllItems()
        self.keepGoing = self.running = False
        wx.PostEvent(self.tree,wxThreadConsistanceElements(0,0))
        wx.PostEvent(self.tree,wxThreadConsistanceElementsFinished())
    
    def GetCount(self):
        return self.iCount
    def GetFoundTup(self,idx):
        try:
            return self.lstFound[idx]
        except:
            return None
    def GetFound(self):
        return self.iFound
    def GetFoundLst(self):
        return self.lstFound
    def GetNode(self,idx):
        if self.lstSort is None:
            return None
        try:
            return self.lstSort[idx][1][3]
        except:
            return None

def create(parent):
    return vAnalyseConsistanceDialog(parent)

[wxID_VANALYSECONSISTANCEDIALOG, wxID_VANALYSECONSISTANCEDIALOGCBGEN, 
 wxID_VANALYSECONSISTANCEDIALOGCBOK, wxID_VANALYSECONSISTANCEDIALOGGPROCESS, 
 wxID_VANALYSECONSISTANCEDIALOGGSTEP, wxID_VANALYSECONSISTANCEDIALOGLSTGROUP, 
] = [wx.NewId() for _init_ctrls in range(6)]

class vAnalyseConsistanceDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VANALYSECONSISTANCEDIALOG,
              name=u'vAnalyseConsistanceDialog', parent=prnt, pos=wx.Point(457,
              188), size=wx.Size(613, 510), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vPrjTimer Analyse Consistance Dialog')
        self.SetClientSize(wx.Size(605, 476))

        self.cbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VANALYSECONSISTANCEDIALOGCBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'cbOk',
              parent=self, pos=wx.Point(296, 440), size=wx.Size(76, 30),
              style=0)
        self.cbOk.Bind(wx.EVT_BUTTON, self.OnCbOkButton,
              id=wxID_VANALYSECONSISTANCEDIALOGCBOK)

        self.cbGen = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VANALYSECONSISTANCEDIALOGCBGEN,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Find', name=u'cbGen',
              parent=self, pos=wx.Point(144, 8), size=wx.Size(76, 30), style=0)
        self.cbGen.Bind(wx.EVT_BUTTON, self.OnCbGenButton,
              id=wxID_VANALYSECONSISTANCEDIALOGCBGEN)

        self.gProcess = wx.Gauge(id=wxID_VANALYSECONSISTANCEDIALOGGPROCESS,
              name=u'gProcess', parent=self, pos=wx.Point(304, 12), range=100,
              size=wx.Size(288, 18), style=wx.GA_HORIZONTAL | wx.GA_SMOOTH)

        self.gStep = wx.Gauge(id=wxID_VANALYSECONSISTANCEDIALOGGSTEP,
              name=u'gStep', parent=self, pos=wx.Point(240, 12), range=3,
              size=wx.Size(60, 18), style=wx.GA_HORIZONTAL | wx.GA_SMOOTH)

        self.lstGroup = wx.CheckListBox(choices=[u'year', u'month', u'day',
              u'start', u'end', u'project', u'client', u'location'],
              id=wxID_VANALYSECONSISTANCEDIALOGLSTGROUP, name=u'lstGroup',
              parent=self, pos=wx.Point(8, 8), size=wx.Size(130, 128), style=0)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.verbose=VERBOSE
        
        self.thdConsistance=thdConsistance(self)
        EVT_THREAD_CONSISTANCE_ELEMENTS_FINISHED(self,self.OnAnalyseFin)
        EVT_THREAD_CONSISTANCE_ELEMENTS(self,self.OnAnalyseProgress)
        
        self.doc=None
        self.netHum=None
        self.netLoc=None
        self.netTask=None
        self.netPrj=None
        
        self.vgpModify=vModifyPanel(self,wx.NewId(),
                pos=(0,135),size=(613, 324),style=0,name="vModifyPanel")
        self.lstRes=self.vgpModify.lstRes
        
        self.cbGen.SetBitmapLabel(images.getBuildBitmap())
        self.cbOk.SetBitmapLabel(images.getOkBitmap())
        
        self.lstGroup.Check(5,True)
        self.lstGroup.Check(7,True)
    def OnCbOkButton(self,event):
        self.thdConsistance.Stop()
        while self.thdConsistance.IsRunning():
            time.sleep(1)
        self.EndModal(1)
        event.Skip()
    def OnAnalyseProgress(self,evt):
        self.gStep.SetValue(evt.GetStep())
        if evt.GetCount()>0:
            self.gProcess.SetRange(evt.GetCount())
        #else:
        #    self.gProcess.SetRange(self.iSize)
        self.gProcess.SetValue(evt.GetValue())
    def OnAnalyseFin(self,evt):
        #self.txtNodeCount.SetValue(str(self.thdAnalyse.GetCount()))
        #self.txtCount.SetValue(str(self.thdAnalyse.GetFound()))
        #self.cbFind.Enable(True)
        self.vgpModify.SetFound(self.thdConsistance.GetFoundLst())
        pass
    def SetDocs(self,doc,netHum,netLoc,netPrj,netTask,bNet=False):
        self.doc=doc
        self.netHum=netHum
        self.netLoc=netLoc
        self.netPrj=netPrj
        self.netTask=netTask
        #self.vpwTask.SetDoc(netTask,bNet)
        #if bNet==True:
        #    EVT_NET_XML_GOT_CONTENT(doc,self.OnGotContent)
        #    EVT_NET_XML_CONTENT_CHANGED(netHum,self.OnHumanChanged)
        #    EVT_NET_XML_CONTENT_CHANGED(netLoc,self.OnLocChanged)
        #    EVT_NET_XML_CONTENT_CHANGED(netPrj,self.OnPrjChanged)
        #    EVT_NET_XML_GOT_CONTENT(doc,self.OnTaskChanged)
        #    EVT_NET_XML_LOCK(doc,self.thdConsistance.OnLock)
            
        #self.__prjChanged__()
        #self.__locChanged__()
        #self.__humanChanged__()
        
        self.vgpModify.SetDocs(doc,netHum,netLoc,netPrj,netTask,bNet)
    def OnGotContent(self,evt):
        
        evt.Skip()
    def OnHumanChanged(self,evt):
        self.__humanChanged__()
        evt.Skip()
    def OnLocChanged(self,evt):
        self.__locChanged__()
        evt.Skip()
    def OnPrjChanged(self,evt):
        self.__prjChanged__()
        evt.Skip()

    def __humanChanged__(self):
        self.chcUsr.Clear()
        if self.netHum is None:
            return
        usrs=self.netHum.GetSortedUsrIds()
        
        keys=usrs.GetKeys()
        for k in keys:
            self.chcUsr.Append(k)
    def __locChanged__(self):
        self.chcLoc.Clear()
        if self.netLoc is None:
            return
        ids=self.netLoc.GetSortedIds()
        for k in ids.GetKeys():
            self.chcLoc.Append(k)
    def __prjChanged__(self):
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        self.chcPrj.Clear()
        prjs=self.netPrj.GetSortedIds()
        for k in prjs.GetKeys():
            self.chcPrj.Append(k)
    def OnTaskChanged(self,evt):
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        evt.Skip()
    def OnTaskBuild(self,evt):
        #print 'OnTaskBuild'
        evt.Skip()
    def __updateTask__(self):
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        idx=self.chcPrj.GetSelection()
        ids=self.netPrj.GetSortedIds()
        if (idx<0) or (ids is None):
            return
        ids=self.netPrj.GetSortedIds()
        prjNode,sPrj,iPrjId=ids.GetInfoByIdx(idx)
        print prjNode,sPrj,iPrjId
        self.vpwTask.SetPrj(str(iPrjId))
    def __setSelect__(self,ids,sName,sId,chcObj):
        if ids is None:
            chcObj.SetSelection(-1)
            return
        if len(sId)>0:
            idx=ids.GetIdxById(sId)
        else:
            idx=ids.GetIdxByName(sName)
        chcObj.SetSelection(idx)
    def OnCbGenButton(self, event):
        self.lstSel=[]
        d={}
        d['hum']=self.netHum
        d['loc']=self.netLoc
        d['prj']=self.netPrj
        d['task']=self.netTask
        d['timer']=self.doc
        
        self.vgpModify.Clear()
        
        self.thdConsistance.Find(d)
        event.Skip()

    def getGrp(self):
        grp=[]
        for i in range(self.lstGroup.GetCount()):
            if self.lstGroup.IsChecked(i):
                grp.append(i)
        return grp
    #def getGrp(self):
    #    return [7,5]

    def OnChcPrjChoiceS(self, event):
        self.__updateTask__()
        event.Skip()

    def OnChcLocChoiceS(self, event):
        event.Skip()

    def OnChcUsrChoiceS(self, event):
        event.Skip()

    def OnCbApplyButtonS(self, event):
        if self.thdConsistance.IsRunning():
            return
        i=0
        self.dApply={}
        idx=self.chcPrj.GetSelection()
        if idx>=0:
            ids=self.netPrj.GetSortedIds()
            self.dApply['prj']=ids.GetInfoByIdx(idx)
        
        idx=self.chcUsr.GetSelection()
        if idx>=0:
            ids=self.netHum.GetSortedUsrIds()
            self.dApply['usr']=ids.GetInfoByIdx(idx)
        
        idx=self.chcLoc.GetSelection()
        if idx>=0:
            ids=self.netLoc.GetSortedIds()
            self.dApply['loc']=ids.GetInfoByIdx(idx)
        
        node=self.vpwTask.GetSelNode()
        if node is not None:
            taskId=self.vpwTask.GetSelID()
            sTask=self.vpwTask.GetValue()
            self.dApply['task']=(node,sTask,taskId)
        if self.verbose:
            for k in self.dApply.keys():
                v=self.dApply[k]
                vtLog.vtLogCallDepth(self,'%s  :%s %s'%(k,v[1],v[2]))
        self.dApply['iAct']=0
        iCount=self.lstRes.GetItemCount()
        lst=[]
        while i<iCount:
            if self.lstRes.GetItemState(i,wx.LIST_STATE_SELECTED)!=0:
                iNum=self.lstRes.GetItemData(i)
                #tup=self.thdConsistance.GetFoundTup(iNum)
                lst.append(iNum)
            i+=1
        self.dApply['iCount']=len(lst)
        self.dApply['list']=lst
        self.thdConsistance.Apply(self.dApply)
        
        event.Skip()
