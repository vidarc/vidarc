#Boa:FramePanel:vXmlNodePrjTaskSumPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodePrjTaskSumPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080120
# CVS-ID:       $Id: vXmlNodePrjTaskSumPanel.py,v 1.4 2012/12/20 07:59:20 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputFloat
import vidarc.vApps.vPrj.vXmlPrjInputTree
import vidarc.tool.input.vtInputTextMultiLine
import vidarc.tool.time.vtTimeInputDateTimeStartEnd
import vidarc.tool.time.vtTimeTimeEdit
import vidarc.tool.time.vtTimeDateGM
import vidarc.vApps.vTask.vXmlTaskInputTree
import vidarc.vApps.vHum.vXmlHumInputTreeUsr
import vidarc.vApps.vLoc.vXmlLocInputTree
import vidarc.tool.input.vtInputTree
import wx.grid
import wx.lib.buttons
import cStringIO
import vidarc.tool.xml.vtXmlDom as vtXmlDom
#from vidarc.vApps.vPrjTimer.vpwTask import *

from vidarc.tool.time.vTimeDateGM import *
from vidarc.tool.time.vTimeTimeEdit import *

from vidarc.vApps.vPrj.vXmlPrjGrpTree import *

from vidarc.tool.xml.vtXmlNodePanel import *
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.vApps.vPrjTimer.__config__ as __config__

[wxID_VXMLNODEPRJTASKSUMPANEL, wxID_VXMLNODEPRJTASKSUMPANELCBAPPLY, 
 wxID_VXMLNODEPRJTASKSUMPANELCBCANCEL, wxID_VXMLNODEPRJTASKSUMPANELLBLPERSON, 
 wxID_VXMLNODEPRJTASKSUMPANELLBLPRJNAME, 
 wxID_VXMLNODEPRJTASKSUMPANELLBLPRJTASK, wxID_VXMLNODEPRJTASKSUMPANELLBLSUM, 
 wxID_VXMLNODEPRJTASKSUMPANELVISUM, wxID_VXMLNODEPRJTASKSUMPANELVITRHUM, 
 wxID_VXMLNODEPRJTASKSUMPANELVITRPRJ, wxID_VXMLNODEPRJTASKSUMPANELVITRTASK, 
] = [wx.NewId() for _init_ctrls in range(11)]

class vXmlNodePrjTaskSumPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_bxsHum_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPerson, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.vitrHum, 2, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.lblSum, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viSum, 3, border=4, flag=wx.EXPAND)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsPrjTask, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbApply, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsHum, 1, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddWindow(self.cbCancel, 1, border=4, flag=wx.TOP | wx.EXPAND)

    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)

    def _init_coll_bxsPrjTask_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPrjName, 1, border=4,
              flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.vitrPrj, 2, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.lblPrjTask, 1, border=4,
              flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.vitrTask, 3, border=4, flag=wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=2, hgap=4, rows=2, vgap=4)

        self.bxsPrjTask = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsHum = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsPrjTask_Items(self.bxsPrjTask)
        self._init_coll_bxsHum_Items(self.bxsHum)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODEPRJTASKSUMPANEL,
              name=u'vXmlNodePrjTaskPanel', parent=prnt, pos=wx.Point(3, 5),
              size=wx.Size(541, 124), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(533, 97))

        self.lblPrjName = wx.StaticText(id=wxID_VXMLNODEPRJTASKSUMPANELLBLPRJNAME,
              label=_(u'project'), name=u'lblPrjName', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(60, 30), style=wx.ALIGN_RIGHT)
        self.lblPrjName.SetMinSize(wx.Size(-1, -1))

        self.lblPrjTask = wx.StaticText(id=wxID_VXMLNODEPRJTASKSUMPANELLBLPRJTASK,
              label=_(u'task'), name=u'lblPrjTask', parent=self,
              pos=wx.Point(193, 0), size=wx.Size(60, 30), style=wx.ALIGN_RIGHT)
        self.lblPrjTask.SetMinSize(wx.Size(-1, -1))

        self.lblPerson = wx.StaticText(id=wxID_VXMLNODEPRJTASKSUMPANELLBLPERSON,
              label=_(u'person'), name=u'lblPerson', parent=self,
              pos=wx.Point(0, 38), size=wx.Size(60, 30), style=wx.ALIGN_RIGHT)
        self.lblPerson.SetMinSize(wx.Size(-1, -1))

        self.vitrPrj = vidarc.vApps.vPrj.vXmlPrjInputTree.vXmlPrjInputTree(id=wxID_VXMLNODEPRJTASKSUMPANELVITRPRJ,
              name=u'vitrPrj', parent=self, pos=wx.Point(64, 0),
              size=wx.Size(125, 30), style=0)
        self.vitrPrj.SetMinSize(wx.Size(-1, -1))

        self.vitrTask = vidarc.vApps.vTask.vXmlTaskInputTree.vXmlTaskInputTree(id=wxID_VXMLNODEPRJTASKSUMPANELVITRTASK,
              name=u'vitrTask', parent=self, pos=wx.Point(257, 0),
              size=wx.Size(194, 30), style=0)
        self.vitrTask.SetMinSize(wx.Size(-1, -1))

        self.vitrHum = vidarc.vApps.vHum.vXmlHumInputTreeUsr.vXmlHumInputTreeUsr(id=wxID_VXMLNODEPRJTASKSUMPANELVITRHUM,
              parent=self, pos=wx.Point(64, 38), size=wx.Size(125, 30),
              style=0)

        self.lblSum = wx.StaticText(id=wxID_VXMLNODEPRJTASKSUMPANELLBLSUM,
              label=u'sum', name=u'lblSum', parent=self, pos=wx.Point(193, 38),
              size=wx.Size(60, 30), style=wx.ALIGN_RIGHT)

        self.viSum = vidarc.tool.input.vtInputFloat.vtInputFloat(default=0.0,
              fraction_width=1, id=wxID_VXMLNODEPRJTASKSUMPANELVISUM,
              integer_width=4, maximum=744, minimum=0.0, name=u'viSum',
              parent=self, pos=wx.Point(257, 38), size=wx.Size(194, 30),
              style=0)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODEPRJTASKSUMPANELCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'),
              name=u'cbApply', parent=self, pos=wx.Point(457, 0),
              size=wx.Size(76, 30), style=0)
        self.cbApply.SetMinSize(wx.Size(-1, -1))
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VXMLNODEPRJTASKSUMPANELCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODEPRJTASKSUMPANELCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(457, 38),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.SetMinSize(wx.Size(-1, -1))
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VXMLNODEPRJTASKSUMPANELCBCANCEL)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vPrjTimer')
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,applName=_(u'vPrjTimer PrjTASK'),
                lWidgets=[])
        self.rootNode=None
        self.doc=None
        self.netHum=None
        self.netLoc=None
        self.netPrj=None
        self.netMaster=None
        self.node=None
        self.nodeCurStarted=None
        self.selPrj=None
        self.prjs=None
        self.locs=None
        self.persons=None
        self.dictTask={}
        self.bTaskContent=False
        self.tree=None
        
        #self.vitrPrj.SetTreeFunc(self.__createPrjTree__,self.__showPrjNode__)
        #self.vitrPrj.SetTagNames('project','name')
        vidarc.tool.input.vtInputTree.EVT_VTINPUT_TREE_CHANGED(self.vitrPrj,self.OnPrjSelChanged)
        self.vitrHum.SetTagNames('person','name')
        self.vitrHum.SetAppl('vHum')
        self.vitrTask.SetTagNames('task','tag')
        self.vitrTask.SetAppl('vTask')
        self.vitrPrj.SetAppl('vPrj')
        self.viSum.SetTagName('duration')
        
    def __del__(self):
        #vtLog.vtLngCur(vtLog.DEBUG,'',origin='del')
        pass
    def SetNetDocs(self,d):
        if d.has_key('vHum'):
            dd=d['vHum']
            self.vitrHum.SetDocTree(dd['doc'],dd['bNet'])
        if d.has_key('vPrj'):
            dd=d['vPrj']
            self.vitrPrj.SetDocTree(dd['doc'],dd['bNet'])
        if d.has_key('vTask'):
            dd=d['vTask']
            self.vitrTask.SetDocTree(dd['doc'],dd['bNet'])
    def __Close__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Cancel__(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # add code here
    def __Lock__(self,flag):
        if flag:
            self.vitrPrj.Enable(False)
            self.vitrHum.Enable(False)
            self.vitrTask.Enable(False)
            self.viSum.Enable(False)
        else:
            self.vitrPrj.Enable(True)
            if self.bFixedUsr:
                self.vitrHum.Enable(False)
            else:
                self.vitrHum.Enable(True)
            self.vitrTask.Enable(True)
            self.viSum.Enable(True)
    def __getNode__(self,o,node):
        self.GetNode(node)
        
    def __setSelect__(self,ids,sName,sId,chcObj):
        if ids is None:
            chcObj.SetSelection(-1)
            return
        if len(sId)>0:
            idx=ids.GetIdxById(sId)
        else:
            idx=ids.GetIdxByName(sName)
        chcObj.SetSelection(idx)
    def IsBusy(self):
        return False
    def Stop(self):
        pass
    def __Clear__(self):
        try:
            #vtXmlNodePanel.ClearWid(self)
            # add code here
            self.vitrPrj.Clear()
            #self.vitrHum.Clear()
            self.bFixedUsr=False
            if self.doc is not None:
                if self.doc.GetLoggedInSecLv()<__config__.SEC_LEVEL_LIMIT_EXPERT:
                    if self.doc.IsConnActive():
                        usrID=self.doc.GetLoggedInUsrId()
                        sUsr=self.doc.GetLoggedInUsr()
                        self.bFixedUsr=True
                    else:
                        nodePar=self.doc.getParent(self.node)
                        if self.doc.getTagName(nodePar)=='employee':
                            oEmployee=self.doc.GetRegByNode(nodePar)
                            usrID=oEmployee.GetEmployeeID(nodePar)
                            sUsr=oEmployee.GetEmployee(nodePar)
                            self.bFixedUsr=True
            if self.bFixedUsr==True:
                self.vitrHum.SetValueID(sUsr,usrID)
            else:
                self.vitrHum.Clear()
            self.vitrTask.Clear()
            self.viSum.Clear()
        except:
            self.__logTB__()
            self.__logCritical__(''%())
    def __SetDoc__(self,doc,bNet=False,dDocs=None):
        self.vitrPrj.SetDoc(doc)
        self.vitrHum.SetDoc(doc)
        self.vitrTask.SetDoc(doc)
        #self.viSum.SetDoc(doc)
    def SetTree(self,tree):
        self.tree=tree
    def SetNetMaster(self,netMaster):
        self.vitrTask.SetNetMaster(netMaster)
    def __createPrjTree__(*args,**kwargs):
        tr=vXmlPrjGrpTree(**kwargs)
        tr.SetNodeInfos(['name','|id'])
        tr.SetGrouping([],[('name','')])
        tr.EnableLanguageMenu()
        return tr
    def __showPrjNode__(self,doc,node,lang):
        #self.vitrPrj.SetTreeNode()
        self.vitrTask.SelectPrj(doc.getAttribute(node,'id'))
        return doc.getNodeText(node,'name')
    def OnPrjSelChanged(self,evt):
        evt.Skip()
        #vtLog.CallStack('')
        id=evt.GetId()
        #print id
        self.vitrTask.SelectPrj(id)
        #node=evt.GetNode()
        #self.vitrTask.SelectPrj(doc.getAttribute(node,'id'))
    def OnTaskBuild(self,evt):
        self.__selectTask__()
        evt.Skip()
    def __SetNode__(self,node):
        try:
            if self.doc.getTagName(node)!='prjtasksum':
                node=None
            #if vtXmlNodePanel.SetNode(self,node)<0:
            #    return
            self.selPrj=None
            # set data to xml-node
            #sDesc=self.doc.getNodeText(node,'desc')
            #sDate=self.doc.getNodeText(node,'date')
            #sStartTime=self.doc.getNodeText(node,'starttime')
            #sEndTime=self.doc.getNodeText(node,'endtime')
            
            #self.txtDesc.SetValue(sDesc)
            #self.dteDate.SetValue(sDate)
            #self.tmeStart.SetTimeStr(sStartTime)
            #self.tmeEnd.SetTimeStr(sEndTime)
            self.vitrPrj.SetNode(node)
            self.bFixedUsr=False
            if self.doc.GetLoggedInSecLv()<__config__.SEC_LEVEL_LIMIT_EXPERT:
                if self.doc.IsConnActive():
                    usrID=self.doc.GetLoggedInUsrId()
                    sUsr=self.doc.GetLoggedInUsr()
                    self.bFixedUsr=True
                else:
                    nodePar=self.doc.getParent(self.node)
                    if nodePar is not None:
                        if self.doc.getTagName(nodePar)=='employee':
                            oEmployee=self.doc.GetRegByNode(nodePar)
                            usrID=oEmployee.GetEmployeeID(nodePar)
                            sUsr=oEmployee.GetEmployee(nodePar)
                            self.bFixedUsr=True
                    else:
                        usrID=-1
                        sUsr=''
                        self.bFixedUsr=True
            if self.bFixedUsr==True:
                if usrID>=0:
                    self.vitrHum.SetValueID(sUsr,usrID)
                else:
                    self.vitrHum.Enable(False)
            else:
                self.vitrHum.SetNode(node)
            prjId=self.vitrPrj.GetID()
            self.vitrTask.SelectPrj(prjId)
            self.vitrTask.SetNode(node)
            self.viSum.SetNode(node,self.doc)
        except:
            self.__logTB__()
    def __GetNode__(self,node):
        try:
            self.vitrPrj.GetNode(node)
            self.bFixedUsr=False
            if self.doc.GetLoggedInSecLv()<__config__.SEC_LEVEL_LIMIT_EXPERT:
                if self.doc.IsConnActive():
                    usrID=self.doc.GetLoggedInUsrId()
                    sUsr=self.doc.GetLoggedInUsr()
                    self.bFixedUsr=True
                else:
                    nodePar=self.doc.getParent(node)
                    if self.doc.getTagName(nodePar)=='employee':
                        oEmployee=self.doc.GetRegByNode(nodePar)
                        usrID=oEmployee.GetEmployeeID(nodePar)
                        sUsr=oEmployee.GetEmployee(nodePar)
                        self.bFixedUsr=True
            if self.bFixedUsr==True:
                self.objRegNode.SetPersonID(node,usrID)
                self.objRegNode.SetPerson(node,sUsr)
            else:
                self.vitrHum.GetNode(node)
            self.vitrTask.GetNode(node)
            self.viSum.GetNode(node,self.doc)
        except:
            self.__logTB__()
    def OnGcbbApplyButton(self, event):
        self.GetNode(self.node)
        wx.PostEvent(self,vgpProjectInfoChanged(self,self.node))
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.SetNode(self.node)
        event.Skip()

    def __getTaskId__(self):
        if self.VERBOSE:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'get task id',origin='vgpPrjTimeInfo')
        sTask=self.chcTask.GetStringSelection()
        idx=self.chcPrj.GetSelection()
        prjNode,sPrj,iPrjId=self.prjs.GetInfoByIdx(idx)
        try:
            d=self.dictTask[str(iPrjId)]
            for k in d.keys():
                if sTask==d[k][0]:
                    return k
        except:
            pass
        return ''
    def __updateTask__(self):
        if self.VERBOSE:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'update task',origin='vgpPrjTimeInfo')
        idx=self.chcPrj.GetSelection()
        if (idx<0) or (self.prjs is None):
            return
        prjNode,sPrj,iPrjId=self.prjs.GetInfoByIdx(idx)
        self.vpwTask.SetPrj(str(iPrjId))
        wx.CallAfter(self.__selectTask__)
    def __selectTaskTree__(self,id,name):
        if self.VERBOSE:
            vtLog.vtLngCallStack(self,vtLog.DEBUG,'select task tree',origin='vgpPrjTimeInfo')
        self.vpwTask.Select(id,name)

    def OnCbApplyButton(self, event):
        event.Skip()
        self.GetNode(self.node)
        #wx.PostEvent(self,vgpProjectInfoChanged(self,self.node))
        
    def OnCbCancelButton(self, event):
        event.Skip()
        self.SetNode(self.node)

    def HideButtons(self):
        self.cbApply.Show(False)
        self.cbCancel.Show(False)
        self.cbAdd.Show(False)
        self.cbStart.Show(False)
