#Boa:FramePanel:vPrjTimerSettingsPanel
#----------------------------------------------------------------------------
# Name:         vPrjTimerSettingsPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vPrjTimerSettingsPanel.py,v 1.2 2006/01/17 12:17:47 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import cStringIO

[wxID_VPRJTIMERSETTINGSPANEL, wxID_VPRJTIMERSETTINGSPANELGCBBHUMANBROWSE, 
 wxID_VPRJTIMERSETTINGSPANELGCBBLOCBROWSE, 
 wxID_VPRJTIMERSETTINGSPANELGCBBPRJBROWSE, 
 wxID_VPRJTIMERSETTINGSPANELGCBBTASKBROWSE, 
 wxID_VPRJTIMERSETTINGSPANELLBLHUMANFN, wxID_VPRJTIMERSETTINGSPANELLBLLOC, 
 wxID_VPRJTIMERSETTINGSPANELLBLPRJFN, wxID_VPRJTIMERSETTINGSPANELLBLTASKFN, 
 wxID_VPRJTIMERSETTINGSPANELTXTHUMANFN, wxID_VPRJTIMERSETTINGSPANELTXTLOCFN, 
 wxID_VPRJTIMERSETTINGSPANELTXTPRJFN, wxID_VPRJTIMERSETTINGSPANELTXTTASKFN, 
] = [wx.NewId() for _init_ctrls in range(13)]

#----------------------------------------------------------------------
def getBrowseData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02zIDAT8\x8d\x85\x93?hSQ\x14\xc6\x7f\xf7\xf5!\xb7\xf0\x8a\t\xb4\xf0\
\x82\x0e\xbe\xaa`\x86\x0e\xaf\xb84\x10\xc4H\x15\n\xae-M\xc1\xc1E\xb0\x8bC\
\xb7 \xb6\x12\x14D\xc5\xc1\x16\xfc\x87S\x04\x15\x1d\x04+RM\x86B:\x14\x92j$)8\
\xbc\xc1\xc0{h\xa1o\x08\xe4\x82\x8f^\x874i\x0b\xa2\x17\xbe\xe9p~\xf7p\xce\
\xf7!\x8c>\xbaJ\x9eJj\xc8k\xc8k\xf7\xb4\xab!\xaf\xf7\xd7\xff&\x83}o&;\x83\
\xfe\x9d#\x7f\x13\xeeN\xa6x\xff\xd8\xe1\xf2\xd4\x19\xadw\x164\x80\xdeY\xd0\
\x1d\xad\xean\x8f\xd9-\n\xe3\x86\x80m0\x15\xb9\xeb9\x00\x1a\x1b\r&\x06\xc7\
\x98u\x8b\x14\xd6\xa7\xf4\xdct\x19\x19sY\xfc\x90\xe7M\xe9\x9a\xde\xa8<\x10}\
\x90\x99\xd7\xed\x1c\xe6\xa1/\xf3\xd2J0\xec\x98XV\x08\xf43d\x0f\x91L\x9eG\
\x1c\xbf\x00\xd6\x00\xbf~\x06\x98\xd2$y8b\xf4\xc8\x10\x85O\xb7\xe7\x05\x94\
\xf5\xe7w\xdb\xa4\xcef\x90Q\x15\xaf\xea\x11\xc6}bV\x12\xe7D\x1c\xb0\x81\x04 \
\x01(\xae\x14\xf1\xbe\xbd\xc0\x19t8w\t\x0ca\xa4\xc5Z\xa9\x80\x0cC\x88<\x9c\
\x11\x1b\xf7\xa8K\x828k\xaf+\x14KU\xc2\xad2\xe0\x01\x8a\xccx\x86\xa0\xe5P\
\xadU\x80Lg\x89\xc5\r\x05RB\x04D\x014\x03d\xe436\x92 \xe5H\xbc\xcd5\xde>_\
\x04\x1a@\x80\xf7\xc3\xa3\xb8\xa9\x10FZt\x00\xa5Qh5 \x94\x10\xb4\tb\x12\xa5$\
(E\xb8\xe5\x93\x94\xe0\x9eL\x11\x04\n\xa8\xe3\x07>\x8d\xef\t\x80\xee\x193,\
\xaf\x14@)\x14\x10S\nIH\x10\xb5\xb1M\x08\xb1q,E\xbdV$\x08B\xa4%\xf16\x9d=\
\x800\xd2\xa2\xb8\xee\xf5\xfc [\x1eD\x1e1\x13\x14`\x9b\xfd(\xda@\x05\xbf\xb9\
Lc\xbd\xbc{v\xf6\x8ct\xefI\n\x800\x02L\x1be\xda\x1d\x18\xfd\x04Q\x1bd\x1c\
\xa5Rx^\x19\x19K\xf4>\xdb\xe7\xc4\x0csK\x05\xa4\nP\xbb\x8d\x00\x8a61\x13P\
\xdb$\xc2\x06^\xb33\xe9\xd5+\xaf\xf4\xb1\xe1\xa6\xee\x01\x84\x91\x16\xf7\x9f\
}\x14\xf14\xe4\xef,\x12\xb6\xbc\xde\x04D\n\x00_I\x1a\xb5\x19\xe4\x80DZe&\xc6\
\xcb\x07\xb3\xd0\xb5\xf4\xad\x97_E<\rs\xbb\xa0\x10\t\x91"\xb4\x14\xf5Z\x1d\
\xc7vP\xad\x14K\x8f&\xc5?\x93&\x8c> \xafg\xa7]\xed\xaf>\xd4\xf5\xe5\x9c\x96R\
\xea\xec\xf4\xd3^J\xff\x0b\xd8\x0f\xca^t\xb5mg\x0fD\xfc\x0f\xe7R\x0f5\x91\
\xbd\xa0@\x00\x00\x00\x00IEND\xaeB`\x82' 

def getBrowseBitmap():
    return wx.BitmapFromImage(getBrowseImage())

def getBrowseImage():
    stream = cStringIO.StringIO(getBrowseData())
    return wx.ImageFromStream(stream)

class vPrjTimerSettingsPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VPRJTIMERSETTINGSPANEL,
              name=u'vPrjTimerSettingsPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(488, 244), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(480, 217))

        self.lblPrjFN = wx.StaticText(id=wxID_VPRJTIMERSETTINGSPANELLBLPRJFN,
              label=u'Project Filename', name=u'lblPrjFN', parent=self,
              pos=wx.Point(8, 8), size=wx.Size(78, 13), style=0)

        self.lblTaskFN = wx.StaticText(id=wxID_VPRJTIMERSETTINGSPANELLBLTASKFN,
              label=u'Task Filename', name=u'lblTaskFN', parent=self,
              pos=wx.Point(8, 32), size=wx.Size(69, 13), style=0)

        self.lblHumanFN = wx.StaticText(id=wxID_VPRJTIMERSETTINGSPANELLBLHUMANFN,
              label=u'Human Filename', name=u'lblHumanFN', parent=self,
              pos=wx.Point(8, 56), size=wx.Size(79, 13), style=0)

        self.txtPrjFN = wx.TextCtrl(id=wxID_VPRJTIMERSETTINGSPANELTXTPRJFN,
              name=u'txtPrjFN', parent=self, pos=wx.Point(96, 8),
              size=wx.Size(216, 21), style=0, value=u'')

        self.txtTaskFN = wx.TextCtrl(id=wxID_VPRJTIMERSETTINGSPANELTXTTASKFN,
              name=u'txtTaskFN', parent=self, pos=wx.Point(96, 32),
              size=wx.Size(216, 21), style=0, value=u'')

        self.txtHumanFN = wx.TextCtrl(id=wxID_VPRJTIMERSETTINGSPANELTXTHUMANFN,
              name=u'txtHumanFN', parent=self, pos=wx.Point(96, 56),
              size=wx.Size(216, 21), style=0, value=u'')

        self.gcbbPrjBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJTIMERSETTINGSPANELGCBBPRJBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbPrjBrowse', parent=self, pos=wx.Point(320, 8),
              size=wx.Size(76, 22), style=0)
        self.gcbbPrjBrowse.Bind(wx.EVT_BUTTON, self.OnGcbbPrjBrowseButton,
              id=wxID_VPRJTIMERSETTINGSPANELGCBBPRJBROWSE)

        self.gcbbTaskBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJTIMERSETTINGSPANELGCBBTASKBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbTaskBrowse', parent=self, pos=wx.Point(320, 32),
              size=wx.Size(76, 22), style=0)
        self.gcbbTaskBrowse.Bind(wx.EVT_BUTTON, self.OnGcbbTaskBrowseButton,
              id=wxID_VPRJTIMERSETTINGSPANELGCBBTASKBROWSE)

        self.gcbbHumanBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJTIMERSETTINGSPANELGCBBHUMANBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbHumanBrowse', parent=self, pos=wx.Point(320, 56),
              size=wx.Size(76, 22), style=0)
        self.gcbbHumanBrowse.Bind(wx.EVT_BUTTON, self.OnGcbbHumanBrowseButton,
              id=wxID_VPRJTIMERSETTINGSPANELGCBBHUMANBROWSE)

        self.lblLoc = wx.StaticText(id=wxID_VPRJTIMERSETTINGSPANELLBLLOC,
              label=u'Location Filename', name=u'lblLoc', parent=self,
              pos=wx.Point(8, 81), size=wx.Size(86, 13), style=0)

        self.txtLocFN = wx.TextCtrl(id=wxID_VPRJTIMERSETTINGSPANELTXTLOCFN,
              name=u'txtLocFN', parent=self, pos=wx.Point(96, 81),
              size=wx.Size(216, 21), style=0, value=u'')

        self.gcbbLocBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VPRJTIMERSETTINGSPANELGCBBLOCBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbLocBrowse', parent=self, pos=wx.Point(320, 81),
              size=wx.Size(76, 22), style=0)
        self.gcbbLocBrowse.Bind(wx.EVT_BUTTON, self.OnGcbbLocBrowseButton,
              id=wxID_VPRJTIMERSETTINGSPANELGCBBLOCBROWSE)

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        self.prjFN=None
        self.taskFN=None
        self.humanFN=None
        self.locFN=None
        self.bModified=False
        
        img=getBrowseBitmap()
        
        self.gcbbPrjBrowse.SetBitmapLabel(img)
        self.gcbbTaskBrowse.SetBitmapLabel(img)
        self.gcbbHumanBrowse.SetBitmapLabel(img)
        self.gcbbLocBrowse.SetBitmapLabel(img)
        
    def __setModified__(self):
        self.bModified=True
    def SetFilePrj(self,fn):
        self.prjFN=fn
        self.txtPrjFN.SetValue(self.prjFN)
        self.__setModified__()
    def SetFileTask(self,fn):
        self.taskFN=fn
        self.txtTaskFN.SetValue(self.taskFN)
        self.__setModified__()
    def SetFileHuman(self,fn):
        self.humanFN=fn
        self.txtHumanFN.SetValue(self.humanFN)
        self.__setModified__()
    def SetFileLoc(self,fn):
        self.locFN=fn
        self.txtLocFN.SetValue(self.locFN)
        self.__setModified__()
    def SetXml(self,node):
        self.SetFilePrj(vtXmlDomTree.getNodeText(node,'projectfn'))
        self.SetFileTask(vtXmlDomTree.getNodeText(node,'tasksfn'))
        self.SetFileHuman(vtXmlDomTree.getNodeText(node,'humanfn'))
        self.SetFileLoc(vtXmlDomTree.getNodeText(node,'locationfn'))
    def GetXml(self,node):
        vtXmlDomTree.setNodeText(node,'projectfn',self.txtPrjFN.GetValue())
        vtXmlDomTree.setNodeText(node,'tasksfn',self.txtTaskFN.GetValue())
        vtXmlDomTree.setNodeText(node,'humanfn',self.txtHumanFN.GetValue())
        vtXmlDomTree.setNodeText(node,'locationfn',self.txtLocFN.GetValue())
    def OnGcbbPrjBrowseButton(self, event):
        #'open'
        dlg = wx.FileDialog(self, "Open", ".", "", "XML files (*.xml)|*.xml|all files (*.*)|*.*", wx.OPEN)
        try:
            if self.prjFN is not None:
                dlg.SetDirectory(self.prjFN)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.SetFilePrj(filename)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()

    def OnGcbbTaskBrowseButton(self, event):
        #'open'
        dlg = wx.FileDialog(self, "Open", ".", "", "XML files (*.xml)|*.xml|all files (*.*)|*.*", wx.OPEN)
        try:
            if self.taskFN is not None:
                dlg.SetDirectory(self.taskFN)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.SetFileTask(filename)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()

    def OnGcbbHumanBrowseButton(self, event):
        #'open'
        dlg = wx.FileDialog(self, "Open", ".", "", "XML files (*.xml)|*.xml|all files (*.*)|*.*", wx.OPEN)
        try:
            if self.humanFN is not None:
                dlg.SetDirectory(self.humanFN)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.SetFileHuman(filename)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()
        

    def OnGcbbLocBrowseButton(self, event):
        #'open'
        dlg = wx.FileDialog(self, "Open", ".", "", "XML files (*.xml)|*.xml|all files (*.*)|*.*", wx.OPEN)
        try:
            if self.locFN is not None:
                dlg.SetDirectory(self.locFN)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.SetFileLoc(filename)
                #self.txtEditor.SaveFile(filename)
        finally:
            dlg.Destroy()
        event.Skip()
