#----------------------------------------------------------------------------
# Name:         __init__.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20050303
# CVS-ID:       $Id: __init__.py,v 1.2 2006/07/17 11:06:20 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

PLUGABLE='vPrjTimerMDIFrame'
PLUGABLE_FRAME=True
PLUGABLE_SRV='vXmlPrjTimer@vXmlSrv'

import sys
if hasattr(sys, 'importers'):
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=1
else:
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=0

