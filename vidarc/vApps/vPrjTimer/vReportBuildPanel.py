#Boa:FramePanel:vReportBuildPanel
#----------------------------------------------------------------------------
# Name:         vReportBuildPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vReportBuildPanel.py,v 1.3 2006/04/11 17:44:34 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import wx.gizmos
from wx.lib.anchors import LayoutAnchors

from vidarc.vApps.vDoc.vXmlDocTree  import *
from vidarc.tool.xml.vtXmlGroupTreeList import *
#from vidarc.tool.report.vRepDocTex import *
from vidarc.tool.report.vRepBuildTex import *
import vidarc.vApps.vDraw.latex as latex
import vidarc.tool.log.vtLog as vtLog

import thread,time,fnmatch,sys,types
import images

dt=wx.DateTime()

VERBOSE=0
import pprint
pp=pprint.PrettyPrinter(indent=2)

def __convYear__(val):
    return val[:4]
    
def __convMonth__(val):
    return val[4:6]
    
def __convDay__(val):
    return val[6:8]
    
def __convWeek__(val):
    dt.ParseFormat(val,'%Y%m%d')
    return '%02d'%dt.GetWeekOfYear()

GROUPING_LST=[u'person',u'year', u'month', u'day',
                        u'week', u'project', u'task', 
                        u'client', u'location']
GROUPING_CONV=[None,__convYear__,__convMonth__,__convDay__,
            __convWeek__,None,None,None,None,None]
SHOWING_LST=[u'person',u'year', u'month', u'day', u'week', u'sum',u'lim',
                        u'project', u'task', 
                        u'client', u'location']
SHOWING_CONV=[None,None,None,None,None,
            None,None,None,None,None,None]
GROUPING_INFO=['person','date','date','date',
            'date','project|fid','task|fid','loc|fid','starttime','endtime']

class vtXmlGroupTimerTreeList(vtXmlGroupTreeList):
    def __procGroup__(self,node,tagName,infos):
        locIds=self.master.netLoc.GetSortedIds()
        prjIds=self.master.netPrj.GetSortedIds()
        taskIds=self.master.netTask.GetTaskIds()
        humIds=self.master.netHum.getIds()
        n=self.master.doc.getChild(node,'project')
        sPrjId=self.master.doc.getAttribute(n,'fid')
        sClient='---'
        lst=[]
        for grp in self.showing:
            if grp[0]==u'year':
                sDate=infos['date']
                lst.append(sDate[:4])
            elif grp[0]==u'month':
                sDate=infos['date']
                lst.append(sDate[4:6])
            elif grp[0]==u'day':
                sDate=infos['date']
                lst.append(sDate[6:8])
            elif grp[0]==u'week':
                sDate=infos['date']
                dt.ParseFormat(sDate,'%Y%m%d')
                iWeek=dt.GetWeekOfYear()
                lst.append('%02d'%iWeek)
            elif grp[0]==u'project':
                n=self.master.doc.getChild(node,'project')
                sId=self.master.doc.getAttribute(n,'fid')
                sPrj='---'
                try:
                    tup=prjIds.GetInfoById(sId)
                    if tup[0] is None:
                        sPrj=self.master.doc.getNodeText(node,'project')
                    else:
                        sPrj=tup[1]
                except:
                    sPrj=self.master.doc.getNodeText(node,'project')
                try:
                    n=self.master.netPrj.getChild(tup[0],'attributes')
                    if n is None:
                        sClient='---'
                    else:
                        sClient=self.master.netPrj.getNodeText(n,'client')
                except:
                    sClient='---'
                lst.append(sPrj)
            elif grp[0]==u'task':
                n=self.master.doc.getChild(node,'task')
                sId=self.master.doc.getAttribute(n,'fid')
                sTask='---'
                try:
                    bTaskFound=False
                    prjTasks=taskIds[sPrjId][1]
                    try:
                        tup=prjTasks[sId]
                        bTaskFound=True
                        sTask=self.master.netTask.getNodeText(tup[0])
                    except:
                        for v in prjTasks.values():
                            try:
                                tup=v[1][sId]
                                bTaskFound=True
                                sTask=self.master.netTask.getNodeText(v[0],'name')
                                sTask+=':'+self.master.netTask.getNodeText(tup,'name')
                                break
                            except:
                                pass
                    if bTaskFound:
                        pass
                    else:
                        sTask=self.master.doc.getNodeText(node,'task')
                except:
                    sTask=self.master.doc.getNodeText(node,'task')
                lst.append(sTask)
            elif grp[0]==u'client':
                lst.append(sClient)
            elif grp[0]==u'location':
                n=self.master.doc.getChild(node,'loc')
                sId=self.master.doc.getAttribute(n,'fid')
                sLoc='---'
                try:
                    tup=locIds.GetInfoById(sId)
                    if tup[0] is None:
                        sLoc=self.master.doc.getNodeText(node,'loc')
                    else:
                        sLoc=tup[1]
                except:
                    sLoc=self.master.doc.getNodeText(node,'loc')
                lst.append(sLoc)
            elif grp[0]==u'person':
                n=self.master.doc.getChild(node,'person')
                sId=self.master.doc.getAttribute(n,'fid')
                sHum='---'
                try:
                    tup=humIds.GetIdStr(sId)
                    if tup[0] is None:
                        sHum=self.master.doc.getNodeText(node,'person')
                    else:
                        sHum=self.master.doc.getNodeText(tup[1],'name')
                except:
                    sHum=self.master.doc.getNodeText(node,'person')
                lst.append(sHum)
            elif grp[0]==u'sum':
                zStart=infos['starttime']
                zEnd=infos['endtime']
                try:
                    hr=int(zStart[:2])
                    min=int(zStart[2:4])
                    sHr=hr+min/60.0
                    
                    hr=int(zEnd[:2])
                    min=int(zEnd[2:4])
                    eHr=hr+min/60.0
                    if sHr<eHr:
                        sum=(eHr-sHr)
                    else:
                        sum=(sHr-eHr)
                except:
                    sum=0.0
                lst.append('%6.2f'%sum)
        return lst
#class vRepDocTexTimes(vRepDocTex):
class vRepDocTexTimes(vRepBuildTex):
    def __procList__(self,f,prefix,lst):
        strs=[]
        try:
            it0=lst[0]
            iCount=len(it0)
        except:
            return []
        strs.append('\\begin{center}')
        s='  \\begin{longtable}{'
        sFmt='  '
        tup=[]
        sPrefix=string.join(prefix,':')
        showing=self.args[0]['showing']
        mapping=self.args[0]['mapping']
        grouping=self.args[0]['grouping']
        iCount=len(showing)
        idxSum=self.args[0]['idxSum']
        idxLim=self.args[0]['idxLim']
        idxPer=self.args[0]['idxPer']
        iCols=0
        for i in range(iCount):
            idx=showing[i][1]
            if idx==idxSum:
                s+='r'
            elif idx==idxLim:
                continue
            elif idx==idxPer:
                continue
            else:
                s+='l'
            sFmt+='\\rotatebox{90}{'+latex.texReplace(showing[i][0])+'} & '
            iCols+=1
        s+=' }'
        strs.append(s)
        sFmt=sFmt[:-2]+'\\\\'
        sPrefix=string.join(prefix,':')
        s='    \\multicolumn{%d}{c}{%s} \\\\'%(iCols,latex.texReplace(sPrefix))
        strs.append(s)
        strs.append(sFmt+' \\hline')
        strs.append('  \\endfirsthead')
        strs.append(sFmt+' \\hline')
        strs.append('  \\endhead')
        
        sumTotal=0.0
        for it in lst:
            sVal=''
            for i in range(iCount):
                idxShow=showing[i][1]
                idx=mapping[idxShow]
                if idxShow==idxSum:
                    fVal=float(it[idx])
                    sum=fVal
                    sumTotal+=fVal
                    s='%6.2f '%fVal
                elif idxShow==idxLim:
                    continue
                elif idxShow==idxPer:
                    continue
                else:
                    val=it[idx]
                    s=latex.texReplace(val)
                sVal+=s+' &'
            strs.append(sVal[:-1]+'\\\\')
        sVal=''
        for i in range(iCount):
            #if bTask==False and i==6:
            #    continue
            #if bClt==False and i==7:
            #    continue
            #if bLoc==False and i==8:
            #    continue
            #if i!=idxSum:
            #    sVal+=' & '
            idxShow=showing[i][1]
            idx=mapping[idxShow]
            if idxShow==idxSum:
                sVal+=' \\textbf{%6.2f} & '%sumTotal
            elif idxShow==idxLim:
                pass
            elif idxShow==idxPer:
                pass
            else:
                sVal+=' & '
        strs.append('  \\hline')
        strs.append('  \\textbf{Sum}'+sVal[:-2]+'\\\\')
        
        if 1==0:
            sumTotal=0.0
            for it in lst:
                sVal=''
                for i in range(iCount):
                    if bTask==False and i==6:
                        continue
                    if bClt==False and i==7:
                        continue
                    if bLoc==False and i==8:
                        continue
                    if i!=idxSum:
                        sVal+=latex.texReplace(it[i])+' & '
                
                strs.append(sVal+it[idxSum]+'\\\\')
                sumTotal+=float(it[idxSum])
            sVal=''
            for i in range(iCount):
                if bTask==False and i==6:
                    continue
                if bClt==False and i==7:
                    continue
                if bLoc==False and i==8:
                    continue
                if i!=idxSum:
                    sVal+=' & '
            sVal+=' \\textbf{%6.2f} '%sumTotal
            strs.append('  \\hline')
            strs.append('  \\textbf{Sum}'+sVal+'\\\\')
        strs.append('  \\caption{%s}'%latex.texReplace(sPrefix))
        strs.append('  \\label{tab:%s}'%latex.texReplace4Lbl(sPrefix))
        
        strs.append('  \\end{longtable}')
        strs.append('\\end{center}')
        return strs
    
    def __addTableSummary__(self):
        strs=[]
        strs.append('\\begin{center}')
        s='  \\begin{longtable}{'
        sFmt='  '
        tup=[]
        showing=self.args[0]
        iCount=len(showing)
        idxSum=self.args[3]
        bTask=self.args[4]
        bClt=self.args[5]
        bLoc=self.args[6]
        for i in range(iCount):
            if bTask==False and i==6:
                continue
            if bClt==False and i==7:
                continue
            if bLoc==False and i==8:
                continue
            if i!=idxSum:
                s+='l'
                sFmt+='\\rotatebox{90}{'+latex.texReplace(showing[i][0])+'} & '
        s+='r }'
        sFmt+='\\rotatebox{90}{'+latex.texReplace(showing[idxSum][0])+'} \\\\'
        strs.append(s)
        sPrefix=''
        grouping=self.args[1]
        #for i in range(len(prefix)):
        #    sPrefix+=grouping[i]+' '+prefix[i]+' '
        s='    \\multicolumn{%d}{c}{%s} \\\\'%(iCount,'summary')
        #tup=('    \\textbf{Nr}','\\textbf{to}','\\textbf{Name}','','','','')
        strs.append(sFmt+' \\hline')
        strs.append('  \\endfirsthead')
        #tup=('    \\textbf{Nr}','\\textbf{to}','\\textbf{Name}','','','','')
        strs.append(sFmt+' \\hline')
        strs.append('  \\endhead')
    
    def __getHeadLine__(iLevel,val):
        if iLevel==1:
            return'\\section{%s}'%val
        elif iLevel==2:
            return '\\subsection{%s}'%val
        elif iLevel==3:
            return '\\subsubsection{%s}'%val
        elif iLevel==4:
            return '\\paragraph{%s}'%val
        else:
            return '\\paragraph*{%s}'%val
            
    def __procInfo__(self,f,info):
        f.write('\\section{Summary}')
        strs=[]
        strs.append('\\begin{center}')
        s='  \\begin{longtable}{'
        sFmt='  '
        tup=[]
        showing=self.args[0]['showing']
        mapping=self.args[0]['mapping']
        grouping=self.args[0]['grouping']
        iCount=len(showing)
        idxSum=self.args[0]['idxSum']
        idxLim=self.args[0]['idxLim']
        idxPer=self.args[0]['idxPer']
        for i in range(iCount):
            idx=showing[i][1]
            if idx==idxSum:
                s+='r'
            elif idx==idxLim:
                s+='r'
            elif idx==idxPer:
                s+='r'
            else:
                s+='l'
            sFmt+='\\rotatebox{90}{'+latex.texReplace(showing[i][0])+'} & '
        s+=' }'
        sFmt=sFmt[:-2]+'\\\\'
        strs.append(s)
        sPrefix=''
        s='    \\multicolumn{%d}{c}{%s} \\\\'%(iCount,'summary')
        strs.append(sFmt+' \\hline')
        strs.append('  \\endfirsthead')
        strs.append(sFmt+' \\hline')
        strs.append('  \\endhead')
        sVal=''
        sumTotal=0.0
        limTotal=0.0
        lstGrph=[]
        grpInfos=[]
        
        for grp in grouping:
            i=0
            for s in showing:
                if grp==s[0]:
                    grpInfos.append(i)
                    break
                i+=1
        for prefix,label,sum,lst,res in info:
            grpInf=[]
            sVal=''
            sum=0.0
            lim=0.0
            for i in range(iCount):
                idxShow=showing[i][1]
                idx=mapping[idxShow]
                if idxShow==idxSum:
                    fVal=res[idx]
                    sum=fVal
                    sumTotal+=fVal
                    s='%6.2f '%fVal
                elif idxShow==idxLim:
                    fVal=res[idx]
                    lim=fVal
                    limTotal+=fVal
                    s='%6.2f '%fVal
                elif idxShow==idxPer:
                    i1=-1
                    i2=-1
                    for i in range(iCount):
                        if showing[i][1]==idxSum:
                            i1=showing[i][1]
                        if showing[i][1]==idxLim:
                            i2=showing[i][1]
                    if i1>=0 and i2>=0:
                        try:
                            fVal=res[mapping[i1]]/res[mapping[i2]]*100.0
                        except:
                            fVal=-1.0
                        s='%6.2f '%fVal
                    else:
                        s=' '
                else:
                    val=res[idx]
                    #s=latex.texReplace(val)
                    s=latex.texReplace(val)
                sVal+=s+' &'
            lstGrph.append((string.join(prefix,':'),sum,lim)) # grpInf
            strs.append(sVal[:-1]+'\\\\')
        sVal=''
        for i in range(iCount):
            #if bTask==False and i==6:
            #    continue
            #if bClt==False and i==7:
            #    continue
            #if bLoc==False and i==8:
            #    continue
            #if i!=idxSum:
            #    sVal+=' & '
            idxShow=showing[i][1]
            idx=mapping[idxShow]
            if idxShow==idxSum:
                sVal+=' \\textbf{%6.2f} & '%sumTotal
            elif idxShow==idxLim:
                sVal+=' \\textbf{%6.2f} & '%limTotal
            elif idxShow==idxPer:
                try:
                    fPer=sumTotal/limTotal*100.0
                except:
                    fPer=0.0
                sVal+=' \\textbf{%6.2f} & '%fPer
            else:
                sVal+=' & '
        #sVal+=' \\textbf{%6.2f} '%sumTotal
        strs.append('  \\hline')
        strs.append('  \\textbf{Sum}'+sVal[:-2]+'\\\\')
        strs.append('  \\caption{%s}'%'summary')
        strs.append('  \\label{tab:%s}'%'summary')
            
        strs.append('  \\end{longtable}')
        strs.append('\\end{center}')
        f.write(string.join(strs,os.linesep))
        
        f.write('\\newpage')
        try:
            limit=self.args[7]
        except:
            limit=None
        strs=[]
        strs=self.__procGrph__('graphic summary',lstGrph,limit)
        f.write(string.join(strs,os.linesep))
        
        if self.args[0]['details']:
            f.write('\\newpage')
            f.write('\\section{Details}')
            for prefix,label,sum,lst,res in info:
                if self.verbose:
                    vtLog.vtLogCallDepth(self,'prefix:%s'%prefix,callstack=False)
                #genHeadLines(prefix+[label])
                strs=self.__procList__(f,prefix,lst)
                strs.append('\\newpage')
                f.write(string.join(strs,os.linesep))
        return 0
    

[wxID_VREPORTBUILDPANEL, wxID_VREPORTBUILDPANELCBBUILD, 
 wxID_VREPORTBUILDPANELCBOPEN, wxID_VREPORTBUILDPANELCHCCLIENT, 
 wxID_VREPORTBUILDPANELCHCDETAILS, wxID_VREPORTBUILDPANELCHCLOC, 
 wxID_VREPORTBUILDPANELCHCTASK, wxID_VREPORTBUILDPANELLBLDOC, 
 wxID_VREPORTBUILDPANELLBLTITLE, wxID_VREPORTBUILDPANELLSTREPORT, 
 wxID_VREPORTBUILDPANELTXTREV, wxID_VREPORTBUILDPANELTXTTITLE, 
] = [wx.NewId() for _init_ctrls in range(12)]

class vReportBuildPanel(wx.Panel):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VREPORTBUILDPANEL,
              name=u'vReportBuildPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(613, 400), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(605, 366))
        self.SetAutoLayout(True)

        self.lblDoc = wx.StaticText(id=wxID_VREPORTBUILDPANELLBLDOC,
              label=u'Documents', name=u'lblDoc', parent=self, pos=wx.Point(8,
              8), size=wx.Size(54, 13), style=0)

        self.cbBuild = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VREPORTBUILDPANELCBBUILD,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Build', name=u'cbBuild',
              parent=self, pos=wx.Point(184, 328), size=wx.Size(76, 30),
              style=0)
        self.cbBuild.Enable(False)
        self.cbBuild.Bind(wx.EVT_BUTTON, self.OnCbBuildButton,
              id=wxID_VREPORTBUILDPANELCBBUILD)

        self.cbOpen = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VREPORTBUILDPANELCBOPEN,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Open', name=u'cbOpen',
              parent=self, pos=wx.Point(272, 328), size=wx.Size(76, 30),
              style=0)
        self.cbOpen.Bind(wx.EVT_BUTTON, self.OnCbOpenButton,
              id=wxID_VREPORTBUILDPANELCBOPEN)

        self.lblTitle = wx.StaticText(id=wxID_VREPORTBUILDPANELLBLTITLE,
              label=u'Title', name=u'lblTitle', parent=self, pos=wx.Point(4,
              306), size=wx.Size(20, 13), style=0)

        self.txtTitle = wx.TextCtrl(id=wxID_VREPORTBUILDPANELTXTTITLE,
              name=u'txtTitle', parent=self, pos=wx.Point(32, 304),
              size=wx.Size(152, 21), style=wx.TE_RICH2, value=u'')

        self.txtRev = wx.TextCtrl(id=wxID_VREPORTBUILDPANELTXTREV,
              name=u'txtRev', parent=self, pos=wx.Point(188, 304),
              size=wx.Size(44, 21), style=wx.TE_RICH2, value=u'')

        self.chcDetails = wx.CheckBox(id=wxID_VREPORTBUILDPANELCHCDETAILS,
              label=u'Details', name=u'chcDetails', parent=self,
              pos=wx.Point(400, 304), size=wx.Size(56, 13), style=0)
        self.chcDetails.SetValue(False)

        self.chcTask = wx.CheckBox(id=wxID_VREPORTBUILDPANELCHCTASK,
              label=u'Task', name=u'chcTask', parent=self, pos=wx.Point(464,
              304), size=wx.Size(40, 13), style=0)
        self.chcTask.SetValue(False)

        self.chcClient = wx.CheckBox(id=wxID_VREPORTBUILDPANELCHCCLIENT,
              label=u'Client', name=u'chcClient', parent=self, pos=wx.Point(512,
              304), size=wx.Size(48, 13), style=0)
        self.chcClient.SetValue(False)

        self.chcLoc = wx.CheckBox(id=wxID_VREPORTBUILDPANELCHCLOC,
              label=u'Location', name=u'chcLoc', parent=self, pos=wx.Point(560,
              304), size=wx.Size(73, 13), style=0)
        self.chcLoc.SetValue(False)

        self.lstReport = wx.CheckListBox(choices=[u'00 person', u'01 year',
              u'02 month', u'03 day', u'04 week', u'05 project', u'__ task',
              u'__ client', u'06 location', u'07 sum', u'08 limit', u'09 percent'],
              id=wxID_VREPORTBUILDPANELLSTREPORT, name=u'lstReport',
              parent=self, pos=wx.Point(208, 8), size=wx.Size(130, 96),
              style=0)
        self.lstReport.Bind(wx.EVT_CHECKLISTBOX, self.OnLstReportChecklistbox,
              id=wxID_VREPORTBUILDPANELLSTREPORT)

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        self.verbose=VERBOSE
        
        self.doc=None
        self.netHum=None
        self.netLoc=None
        self.netDoc=None
        self.netPrjDoc=None
        self.netPrj=None
        self.netTask=None
        self.selDocNode=None
        self.dLimits=None
        self.docID=''
        
        self.vrdTex=vRepDocTexTimes(self)
        
        self.vgpTree=vXmlDocTree(self,wx.NewId(),
                pos=(4,4),size=(278, 220),style=0,name="vgpTree")
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.vgpTree,self.OnDocAddFinished)
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self, wx.Bottom, -26)
        lc.left.AsIs        ()
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.lblTitle.SetConstraints(lc)
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self, wx.Bottom, -28)
        lc.left.AsIs        ()
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.txtTitle.SetConstraints(lc)
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self, wx.Bottom, -28)
        lc.left.AsIs        ()
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.txtRev.SetConstraints(lc)
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self, wx.Bottom, -32)
        lc.left.SameAs      (self.txtRev, wx.Right, 10)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.cbBuild.SetConstraints(lc)
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self, wx.Bottom, -32)
        lc.left.SameAs      (self.cbBuild, wx.Right, 10)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.cbOpen.SetConstraints(lc)
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self, wx.Bottom, -28)
        lc.left.SameAs      (self.cbOpen, wx.Right, 4)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.chcDetails.SetConstraints(lc)
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self, wx.Bottom, -28)
        lc.left.SameAs      (self.chcDetails, wx.Right, 4)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.chcTask.SetConstraints(lc)
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self, wx.Bottom, -28)
        lc.left.SameAs      (self.chcTask, wx.Right, 4)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.chcClient.SetConstraints(lc)
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self, wx.Bottom, -28)
        lc.left.SameAs      (self.chcClient, wx.Right, 4)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.chcLoc.SetConstraints(lc)
        
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.lblDoc, wx.Bottom, 4)
        lc.left.SameAs      (self, wx.Left, 8)
        lc.bottom.SameAs    (self.txtTitle, wx.Top, 20)
        lc.width.PercentOf  (self, wx.Width, 35)
        self.vgpTree.SetConstraints(lc)
        
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.vgpTree, wx.Top, 0)
        lc.left.SameAs      (self.vgpTree, wx.Right, 8)
        lc.height.AsIs      ()
        lc.width.AsIs       ()
        self.lstReport.SetConstraints(lc)
        
        self.tlstOutputSel = vtXmlGroupTimerTreeList( parent=self, pos=wx.Point(104, 24),
              size=wx.Size(344, 100), style=wx.TR_HAS_BUTTONS,name=u'tlstOutputSel',
              verbose=0)
        EVT_THREAD_GROUP_TREELIST_ELEMENTS_FINISHED(self,self.OnGroupTreeListFinished)
        lc = wx.LayoutConstraints()
        lc.top.SameAs       (self.lstReport, wx.Bottom, 4)
        #lc.left.SameAs      (self.lstReport, wx.Left, 0)
        #lc.top.SameAs       (self.vgpTree, wx.Bottom, 4)
        lc.left.SameAs      (self.vgpTree, wx.Right, 8)
        lc.bottom.SameAs    (self.txtTitle, wx.Top, 20)
        lc.right.SameAs     (self, wx.Right, 4)
        self.tlstOutputSel.SetConstraints(lc)
        
        for s in [u'',u'Usr',u'Y',u'M',u'D',u'W',u'Sum',u'Lim',u'Project',u'Task',
                    u'Client',u'Location']:
            self.tlstOutputSel.AddColumn(s)
        self.tlstOutputSel.SetMainColumn(0)
        for i in range(1,8):
            self.tlstOutputSel.SetColumnAlignment(i,wx.LIST_FORMAT_RIGHT)
        i=0
        for size in [150,30,40,15,15,30,60,60,60,85,85,85]:
            self.tlstOutputSel.SetColumnWidth(i,size)
            i+=1
        img=images.getReportBuildBitmap()
        self.cbBuild.SetBitmapLabel(img)
        img=images.getBrowseBitmap()
        self.cbOpen.SetBitmapLabel(img)
        
        self.lstReportIdx=[0,1,2,3,4,5,8,9,10,11]
        for i in self.lstReportIdx:
            self.lstReport.Check(i,True)
        self.Move(pos)

    def SetDocs(self,doc,netHum,netLoc,netDoc,netPrjDoc,netPrj,netTask):
        self.doc=doc
        self.netHum=netHum
        self.netLoc=netLoc
        self.netDoc=netDoc
        self.netPrjDoc=netPrjDoc
        self.netPrj=netPrj
        self.netTask=netTask
        
        self.vrdTex.SetDocs(self.netDoc,self.netPrjDoc)
        self.vgpTree.SetDoc(self.netDoc,True)
        self.vgpTree.SetNode(None)
        
        nodeSetting=self.doc.getChild(self.doc.getRoot(),'settings')
        self.docID=self.doc.getNodeText(nodeSetting,'report_docid')
        sTitle=self.doc.getNodeText(nodeSetting,'last_title')
        sRev=self.doc.getNodeText(nodeSetting,'last_rev')
        self.txtTitle.SetValue(sTitle)
        self.txtRev.SetValue(sRev)
        
        self.vgpTree.SelectByID(self.docID)
        
    def OnDocAddFinished(self,evt):
        if len(self.docID)>0:
            self.vgpTree.SelectByID(self.docID)
            self.docID=''
        evt.Skip()
    def UpdateInfos(self):
        self.tlstOutputSel.DeleteAllItems()
        self.cbBuild.Enable(False)
    def Clear(self):
        self.tlstOutputSel.DeleteAllItems()
        self.cbBuild.Enable(False)
    def SetGrouped(self,grp):
        pass
    
    def OnTreeItemSel(self,evt):
        self.selDocNode=evt.GetTreeNodeData()
        evt.Skip()
        
    def OnGroupTreeListFinished(self,evt):
        outs=self.tlstOutputSel.GetOutputs()
        iLen=len(outs)
        if self.verbose:
            vtLog.vtLogCallDepth(self,'len:%s'%iLen)
        if iLen>0:
            self.cbBuild.Enable(True)
        else:
            self.cbBuild.Enable(False)
        
        evt.Skip()
        
    def SetGrouped(self,grouped,group_info,showing,sorting,grpNames,grpIdxs):
        #grp=[]
        #i=0
        #for show in SHOWING_LST:
        #    grp.append((show,None))
        #    i+=1
        #vtLog.CallStack('')
        #pp.pprint(grouped)
        #pp.pprint(self.dLimits)
        mapping={0:1,1:2,2:3,3:4,4:5,5:6,6:8,7:9,8:10,9:11}
        
        self.cbBuild.Enable(False)
        self.tlstOutputSel.setNodeInfos2Get(group_info)
        #self.tlstOutputSel.setShow(showing,mapping)
        self.tlstOutputSel.setShow(showing,mapping)
        self.tlstOutputSel.SetGrouping(grpNames,grpIdxs)
        self.tlstOutputSel.Build(self,grouped,self.dLimits,
                                sorting, 5, 7, self)
        
    def SetLimit(self,dLimits):
        #vtLog.CallStack('')
        #pp.pprint(dLimits)
        self.dLimits=dLimits
    
    def OnCbBuildButton(self, event):
        if self.verbose:
            vtLog.vtLogCallDepth(self,'')
        sRev=self.txtRev.GetValue()
        sTitle=self.txtTitle.GetValue()
        if self.netPrjDoc is None:
            return
        if self.selDocNode is None:
            sMsg=u'No document group selected!'
            dlg=wx.MessageDialog(self,sMsg ,
                        u'vgaPrjTimer',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            return
        iRes,sFN=self.vrdTex.GetFileName(
                                    sTitle,
                                    sRev,
                                    self.selDocNode,
                                    True,
                                    u'vgaPrjTimer',self)
        if iRes & FAULTY_VERSION:
            # revision is not possible
            if len(sRev)==0:
                sRev='??'
            self.txtRev.SetValue(sRev)
            self.txtRev.SetStyle(0, len(sRev), wx.TextAttr("RED", "YELLOW"))
            return
        else:
            self.txtRev.SetStyle(0, len(sRev), wx.TextAttr("BLACK", "WHITE"))
        if iRes & FAULTY_TITLE:
            # revision is not possible
            if len(sTitle)==0:
                sTitle='??'
            self.txtTitle.SetValue(sTitle)
            self.txtTitle.SetStyle(0, len(sTitle), wx.TextAttr("RED", "YELLOW"))
            return
        else:
            self.txtTitle.SetStyle(0, len(sTitle), wx.TextAttr("BLACK", "WHITE"))
        
        if iRes!=0:
            return
        sRelFN=self.vrdTex.GetRelativeFN()
        sRelOutFN=sRelFN[:-3]+'pdf'
        iRes=self.netPrjDoc.SetDocument(self.netDoc,self.selDocNode,sTitle,sRev,sRelOutFN)
        if iRes!=0:
            dlg=wx.MessageDialog(parent,u'Document could not be added!' ,
                                'vgaPrjTimer',
                                wx.NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.Destroy()
            return
        
        showing=[]
        for idx in self.lstReportIdx:
            sVal=self.lstReport.GetString(idx)[3:]
            showing.append((sVal,idx))
        mapping={0:0,   # person
                1:1,    # year
                2:2,    # mon
                3:3,    # day
                4:4,    # week
                5:6,    # project
                6:7,    # task
                7:8,    # client
                8:9,    # location
                9:5,    # sum
                10:10,  # limit
                11:11}  # percent
        sPrevRev=''
        self.vrdTex.SetReplaceInfo('author','WRO')
        self.vrdTex.SetReplaceInfo('author_company','VID')
        self.vrdTex.SetReplaceInfo('reviewer','')
        self.vrdTex.SetReplaceInfo('review_company','')
        self.vrdTex.SetReplaceInfo('review_date','')
        self.vrdTex.SetReplaceInfo('appoved_by','')
        self.vrdTex.SetReplaceInfo('approve_company','')
        self.vrdTex.SetReplaceInfo('approve_date','')
        self.vrdTex.SetReplaceInfo('replace_revision',sPrevRev)
        
        self.vrdTex.BuildGui(self.tlstOutputSel.GetOutputs(),
                    {'showing':showing,
                     'mapping':mapping,
                     'grouping':self.tlstOutputSel.GetGrouping(),
                     'details':self.chcDetails.GetValue(),
                     'idxSum':9,
                     'idxLim':10,
                     'idxPer':11,
                     'limits':self.dLimits})
        nodeSetting=self.doc.getChild(self.doc.getRoot(),'settings')
        if nodeSetting is not None:
            docID=self.doc.getAttribute(self.selDocNode,'id')
            self.doc.setNodeText(nodeSetting,'report_docid',docID)
            self.doc.setNodeText(nodeSetting,'last_title',sTitle)
            self.doc.setNodeText(nodeSetting,'last_rev',sRev)
            self.doc.AlignNode(nodeSetting,iRec=2)
        event.Skip()
        
    def __clearReport__(self):
        for idx in range(self.lstReport.GetCount()):
            sVal=self.lstReport.GetString(idx)
            if sVal[0]!=' ' and sVal[0]!='_':
                self.lstReport.SetString(idx,u'__'+sVal[2:])
        
    def __showReport__(self):
        i=0
        for idx in self.lstReportIdx:
            sVal=self.lstReport.GetString(idx)
            self.lstReport.SetString(idx,'%02d'%i+sVal[2:])
            i+=1

    def OnLstReportChecklistbox(self, event):
        idx=event.GetSelection()
        if self.lstReport.IsChecked(idx):
            try:
                self.lstReportIdx.index(idx)
            except:
                self.lstReportIdx.append(idx)
        else:
            try:
                self.lstReportIdx.remove(idx)
            except:
                pass
        self.__clearReport__()
        self.__showReport__()
        event.Skip()

    def OnCbOpenButton(self, event):
        sRev=self.txtRev.GetValue()
        sTitle=self.txtTitle.GetValue()
        if self.netPrjDoc is None:
            return
        if self.selDocNode is None:
            sMsg=u'No document group selected!'
            dlg=wx.MessageDialog(self,sMsg ,
                        u'vgaPrjTimer',
                        wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
            dlg.ShowModal()
            dlg.Destroy()
            return
        self.vrdTex.SetDocs(self.netDoc,self.netPrjDoc)
        iRes,sFN=self.vrdTex.GetFileName(sTitle,sRev,self.selDocNode,False,u'vgaPrjTimer',self)
        if iRes==0:
            self.vrdTex.Open()
        event.Skip()
    
