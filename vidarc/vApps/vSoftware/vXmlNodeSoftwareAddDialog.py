#Boa:Dialog:vXmlNodeSoftwareAddDialog
#----------------------------------------------------------------------------
# Name:         vXmlNodeSoftwareAddDialog.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      
# CVS-ID:       $Id: vXmlNodeSoftwareAddDialog.py,v 1.2 2006/04/21 14:42:53 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import vidarc.tool.art.vtArt as vtArt
from vXmlNodeSoftwarePanel import *

def create(parent):
    return vXmlNodeSoftwareAddDialog(parent)

[wxID_VXMLNODESoftwareADDDIALOG, wxID_VXMLNODESoftwareADDDIALOGCBAPPLY, 
 wxID_VXMLNODESoftwareADDDIALOGCBCANCEL, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vXmlNodeSoftwareAddDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VXMLNODESoftwareADDDIALOG,
              name=u'vXmlNodeSoftwareAddDialog', parent=prnt, pos=wx.Point(221, 78),
              size=wx.Size(474, 356), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vSoftware Add Dialog')
        self.SetClientSize(wx.Size(466, 329))

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODESoftwareADDDIALOGCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'), name=u'cbApply',
              parent=self, pos=wx.Point(136, 296), size=wx.Size(76, 30),
              style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VXMLNODESoftwareADDDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODESoftwareADDDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'), name=u'cbCancel',
              parent=self, pos=wx.Point(248, 296), size=wx.Size(76, 30),
              style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VXMLNODESoftwareADDDIALOGCBCANCEL)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.doc=None
        self.node=None
        
        self.pnSoftware=vtXmlNodeSoftwarePanel(self,id=wx.NewId(),pos=(0,0),size=(440,285),
                        style=0,name=u'pnSoftware')
    def SetRegNode(self,obj):
        self.pnSoftware.SetRegNode(obj)
    def SetDoc(self,doc,bNet=False):
        self.pnSoftware.SetDoc(doc,bNet)
    def SetNode(self,nodeObj,nodePar,nodeDft=None):
        self.nodeObj=nodeObj
        self.nodePar=nodePar
        #self.pnLoc.Clear()
        self.pnSoftware.SetNode(nodeDft)
    def GetNode(self,node):
        self.pnSoftware.GetNode(node)
    def OnCbApplyButton(self, event):
        self.EndModal(1)
        event.Skip()
    def OnCbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
