#----------------------------------------------------------------------------
# Name:         vXmlNodeLicenseHost.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060629
# CVS-ID:       $Id: vXmlNodeSoftwareLicenseHost.py,v 1.6 2007/09/02 14:56:55 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.tool.time.vtTime as vtTime
import base64,os,os.path

from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        from vXmlNodeSoftwareLicenseHostPanel import *
        from vXmlNodeSoftwareLicenseHostEditDialog import *
        from vXmlNodeSoftwareLicenseHostAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeSoftwareLicenseHost(vtXmlNodeBase):
    VERBOSE=1
    NODE_ATTRS=[
            #('Tag',None,'tag',None),
            ('Name',None,'name',None),
        ]
    #FUNCS_GET_SET_4_LST=['Tag','Name']
    FUNCS_GET_4_TREE=['Name']
    FMT_GET_4_TREE=[('Name','')]
    def __init__(self,tagName='hostLicense'):
        global _
        _=vtLgBase.assignPluginLang('vSoftware')
        vtXmlNodeBase.__init__(self,tagName)
        self.dt=vtTime.vtDateTime()
    def GetDescription(self):
        return _(u'host license')
    # ---------------------------------------------------------
    # specific
    def __procHost__(self,node,d):
        sTag=self.doc.getTagName(node)
        if sTag in d:
            d[sTag]=self.doc.getText(node)
        return 0
    def __getMachineDict__(self):
        return {
            'create_time':'',
            'uname':'',
            'python_build':'',
            'mid':'',
            'python_compiler':'',
            'python_version':'',
            'os_version':'',
            'pub_key':'',
            }
    def GetInfos(self,node):
        dMachine=self.__getMachineDict__()
        mid=self.doc.getAttribute(node,'mid')
        dMachine['mid']=mid
        self.doc.procChildsExt(node,self.__procHost__,dMachine)
        return dMachine
    def GetReleaseFN(self,node):
        return self.Get(node,'releaseFN')
    def SetReleaseFN(self,node,val):
        self.Set(node,'releaseFN',val)
    def GetReleaseDN(self,node):
        return self.Get(node,'releaseDN')
    def SetReleaseDN(self,node,val):
        self.Set(node,'releaseDN',val)
    def GetName(self,node):
        return self.Get(node,'name')
    def SetInfos(self,node,dHost):
        dMachine=self.__getMachineDict__()
        keys=dMachine.keys()
        keys.sort()
        #print dMachine
        #print dHost
        for k in keys:
            try:
                if k in dHost:
                    sVal=dHost[k]
                else:
                    sVal=''
            except:
                sVal=''
            try:
                self.doc.setNodeText(node,k,unicode(sVal))
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        sHost=''
        try:
            if 'uname' in dHost:
                strs=dHost['uname'].split(',')
                sHost=strs[1]
        except:
            pass
        #print 'uname',sHost
        self.SetName(node,sHost)
    def GetDict(self,node):
        d={}
        d['customer']=self.Get(node,'customer')
        d['customerID']=self.Get(node,'customerID')
        d['address']=self.Get(node,'address')
        d['billing']=self.Get(node,'billing')
        d['machine']=self.GetInfos(node)
        return d
    def SetDict(self,node,d):
        if 'customer' in d:
            sVal=d['customer']
        else:
            sVal=''
        self.Set(node,'customer',sVal)
        if 'customerID' in d:
            sVal=d['customerID']
        else:
            sVal=''
        self.Set(node,'customerID',sVal)
        if 'address' in d:
            sVal=d['address']
        else:
            sVal=''
        self.Set(node,'address',sVal)
        if 'billing' in d:
            sVal=d['billing']
        else:
            sVal=''
        self.Set(node,'billing',sVal)
        self.SetInfos(node,d['machine'])
    def SetName(self,node,val):
        self.Set(node,'name',val)
    def DecryptLicRequest(self,val):
        d={}
        lst=val.split('\n')
        if self.VERBOSE:
            vtLog.vtLngCurCls(vtLog.DEBUG,'lst:%s'%(vtLog.pformat(lst)),self)
        iIdxKeyStart=0
        for l in lst:
            if len(l)==80:
                break
            else:
                iIdxKeyStart+=1
        #print iIdxKeyStart
        if self.VERBOSE:
            vtLog.vtLngCurCls(vtLog.DEBUG,'key start:%d'%(iIdxKeyStart),self)
        lstAddress=None
        lstBilling=None
        for i in range(iIdxKeyStart):
            sFind='company: '
            iPos=lst[i].find(sFind)
            if iPos>=0:
                #print lst[i]
                #print iPos,len(sFind)
                #print lst[i][iPos+len(sFind):]
                #self.txtCustomer.SetValue(lst[i][iPos+len(sFind):])
                d['customer']=lst[i][iPos+len(sFind):]
                continue
            sFind='id: '
            iPos=lst[i].find(sFind)
            if iPos>=0:
                d['customerID']=lst[i][iPos+len(sFind):]
                continue
            
            sFind='address:'
            iPos=lst[i].find(sFind)
            if iPos>=0:
                lstAddress=[]#[lst[i][iPos+len(sFind):]]
                continue
            sFind='billing:'
            iPos=lst[i].find(sFind)
            if iPos>=0:
                if lstAddress is not None:
                    #self.txtAddress.SetValue(string.join(lstAddress,'\n'))
                    d['address']='\n'.join(lstAddress)
                lstAddress=None
                lstBilling=[]#[lst[i][iPos+len(sFind):]]
                continue
            if lstAddress is not None:
                lstAddress.append(lst[i][12:])
            if lstBilling is not None:
                lstBilling.append(lst[i][12:])
        if lstBilling is not None:
            #self.txtBill.SetValue(string.join(lstBilling,'\n'))
            d['billing']='\n'.join(lstBilling)
        lstKey=[]
        for i in range(iIdxKeyStart,len(lst)):
            lstKey.append(lst[i])
            if len(lst[i])!=80:
                break
        sKey=''.join(lstKey)
        sKeyDec=base64.b64decode(sKey)
        iPos=sKeyDec.find('\n\n\n')
        iPos+=4
        
        lstMachineInfo=sKeyDec[:iPos].split('\n')
        #print '--------------'
        #print lstMachineInfo
        dMachine={
            'create_time':'',
            'uname':'',
            'python_build':'',
            'mid':'',
            'python_compiler':'',
            'python_version':'',
            'os_version':'',
            'pub_key':'',
            }
        dMachine['create_time']=      lstMachineInfo[0]
        dMachine['uname']=            lstMachineInfo[1]
        dMachine['python_build']=     lstMachineInfo[2]
        dMachine['mid']=              lstMachineInfo[3]
        dMachine['python_compiler']=  lstMachineInfo[4]
        dMachine['python_version']=   lstMachineInfo[5]
        try:
            dMachine['os_version']=   lstMachineInfo[6]
        except:
            pass
        dMachine['pub_key']=          sKeyDec[iPos:]
        lMachine=[dMachine[k] for k in ['uname','mid','create_time','python_build','python_compiler','python_version']]
        d['machine']=dMachine
        d['machine_infos']=lMachine
        if self.VERBOSE:
            vtLog.vtLngCurCls(vtLog.DEBUG,'dMachine:%s;lMachine:%s'%(vtLog.pformat(dMachine),vtLog.pformat(lMachine)),self)
        #print sKeyDec[:200]
        #print iPos,len(sKeyDec),len(sKeyDec)-iPos
        #self.txtKey.SetValue(sKeyDec[iPos:])
        d['key']=sKeyDec[iPos:]
        return d
    def GetMachineInfosLst(self,dMachine):
        return [dMachine[k] for k in ['uname','mid','create_time','python_build','python_compiler','python_version']]
    def BuildLic(self,par,lDN,sLicDN,dHosts):
        try:
            wid2Log=vtLog.GetPrintMsgWid(par)
            self.dt.Now()
            sBuildFN=fnUtil.replaceSuspectChars('build_'+self.dt.GetDateSmallStr()+'_'+self.dt.GetTimeSmallStr()+'.log')
            sBuildErrFN=sBuildFN[:-3]+'err'
            sCWD=os.getcwd()
            keys=dHosts.keys()
            keys.sort()
            #sys.path.insert(0,sDN)
            #import __setup__
            
            for k in keys:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'host:%s'%(vtLog.pformat(dHosts[k])),par)
                tup=dHosts[k]
                sDNCust=tup[0]
                sHost=tup[2]
                #dHosts=dCustomer[k]['hosts']
                # build license here
                try:
                    sHostDN=fnUtil.replaceSuspectChars(sHost)
                    sID=fnUtil.replaceSuspectChars(sDNCust)
                    sMID=fnUtil.replaceSuspectChars(k)
                    sLicCustDN=os.path.join(sLicDN,sID,'_'.join([sHostDN,sMID]))
                    if os.path.exists(sLicCustDN)==False:
                        try:
                            os.makedirs(sLicCustDN)
                        except:
                            pass
                    sBuildOutFN=os.path.join(sLicDN,sID,sBuildFN)
                    sBuildOutErrFN=os.path.join(sLicDN,sID,sBuildErrFN)
                    #dHosts[keyHost]
                    sKeyFN=os.path.join(sLicCustDN,'pubkey.pub')
                    f=open(sKeyFN,'w')
                    f.write(tup[1])
                    f.close()
                    for sDN in lDN:
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCurWX(vtLog.DEBUG,'dn:%s;licCustDN:%s'%(sDN,sLicCustDN),par)
                        os.chdir(sDN)
                        fTmp=open(sBuildOutFN,'a')
                        fTmp.write(sDN+'\n')
                        fTmp.close()
                        fTmp=open(sBuildOutErrFN,'a')
                        fTmp.write(sDN+'\n')
                        fTmp.close()
                        vtLog.PrintMsg(' '.join([sHost,'build',sDN]),wid=wid2Log)
                        #__setup__.setup_args['options']['vidarc']['licencedir']=sLicCustDN
                        #__setup__.setup(**__setup__.setup_args)
                        #os.system('python __setup__.py vidarc --licencedir=%s --mid=%s --pubkey=%s >> %s 2>> %s'%(sLicCustDN,
                        #        k,sKeyFN,sBuildOutFN,sBuildOutErrFN))
                        lOut=os.popen('python __setup__.py vidarc --licencedir=%s --mid=%s --pubkey=%s >> %s 2>> %s'%(sLicCustDN,
                                k,sKeyFN,sBuildOutFN,sBuildOutErrFN))
                        lOut.close()
                        #self.__doBuild__(sDN,sLicDN,sDNCust,keyHost,dHosts[keyHost])
                except:
                    vtLog.vtLngTB(self.GetName())
        except:
            vtLog.vtLngTB(self.GetName())
        #sys.path=sys.path[1:]
        vtLog.PrintMsg(' '.join(['build finished']),wid=wid2Log)
        
        os.chdir(sCWD)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsNotContained(self):
        "do not add to listbook"
        return True
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01rIDAT8\x8d\xa5\x93\xbfN\x02A\x10\x87\xbf;\xac\x8cW\x1b\xaf\x87{\x80}\
\x00\xd6?\x15\t\xd6`m.\xb4@\xabl@K\xedEl\x8d>\x80%pG\x0f>\xc0=\x00\x17\xeb\
\xa3f,\x0e\x108(\x0c\x9bL\xb33\xbf/3\xbf\xd9\xb5,;\xc7!\xc7>H\r\x1c\xedK\x0c\
\x07}\t\xc3\x10\x80\xce\xc3\xa3\xf5/@\xab^\x12\xa2\x1a\xc4\tZ9p{*\xba\xfa\
\xce\xc5\xe5U\x06\x94\x19\xa1U/\x89.D\x84\xd3\n\x9d\xb7\x1f\x8b\xc2\x0b8\n\
\xa2\x1a\xc3A_\xb6\xeb\xadm\x13[\x95c!\xdf\xd8\xb8\xd3Z\x13\xf6\xae!\xdf\xc8\
\x8c\x93\xe9`4I \xeea\xca_\x10\xf7\xd0\xee'D\xb5}\x16lz0\x1c\xf4\x85\xa8FQ\
\x9d0\x9a\xccR\x18`\xfc3\x8a\xaa@\xfb\xe3;K\xb0\xec\xdc*\x8c\xef\xca|\xacD+G\
\x001\xbe+\x80\x00\x12t=\x99\x8f\x95\x18cd]\xf3'6&-\xf0]\xd1\xcaYA\xd6A\xcb\
\xfc: \xb3\xc6\xf6\xeb45N9\xcc\xc7\x8a\xd1(\xe6\xbc9]\xe4b\x98%\xfb=X\n\x01\
\x8a\xca\x81\xd9\x8c\xbc\xa4bS^\x14$\t\xb0\xb6\xb9e+A\x10H\xd0\xf5V\xadk\xe5\
\x88)#\x9e\x8bho\xe1\xc3\xb3+~U\xef\xf6\xc0\xb2s\x98fy\x13\xb2\x10j/\x8d\xa7\
*\x12\x04\xc1~\xc06d\t07\xceN\xb1e\xe7\xb2/\x11\xa0u\x7f'\xc4\xbd\xd4\xb0$!>\
\xd1T}\xb3\xf3/\xfc\x02\xed\x98\xb7\x99q\xffG\xb0\x00\x00\x00\x00IEND\xaeB`\
\x82" 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return vXmlNodeSoftwareLicenseHostEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return vXmlNodeSoftwareLicenseHostAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeSoftwareLicenseHostPanel
        else:
            return None
