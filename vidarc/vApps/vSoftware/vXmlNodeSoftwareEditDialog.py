#Boa:Dialog:vXmlNodeSoftwareEditDialog
#----------------------------------------------------------------------------
# Name:         vXmlNodeSoftwareEditDialog.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      
# CVS-ID:       $Id: vXmlNodeSoftwareEditDialog.py,v 1.2 2006/04/21 14:42:53 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import vidarc.tool.art.vtArt as vtArt
from vXmlNodeSoftwarePanel import *


def create(parent):
    return vXmlNodeSoftwareEditDialog(parent)

[wxID_VXMLNODESoftwareEDITDIALOG, wxID_VXMLNODESoftwareEDITDIALOGCBAPPLY, 
 wxID_VXMLNODESoftwareEDITDIALOGCBCANCEL, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vXmlNodeSoftwareEditDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VXMLNODESoftwareEDITDIALOG,
              name=u'vXmlNodeSoftwareEditDialog', parent=prnt, pos=wx.Point(348,
              174), size=wx.Size(466, 356), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vSoftware Edit Dialog')
        self.SetClientSize(wx.Size(458, 329))

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODESoftwareEDITDIALOGCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'), name=u'cbApply',
              parent=self, pos=wx.Point(136, 296), size=wx.Size(76, 30),
              style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VXMLNODESoftwareEDITDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODESoftwareEDITDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'), name=u'cbCancel',
              parent=self, pos=wx.Point(248, 296), size=wx.Size(76, 30),
              style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VXMLNODESoftwareEDITDIALOGCBCANCEL)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.doc=None
        self.node=None
        
        self.pnSoftware=vtXmlNodeSoftwarePanel(self,id=wx.NewId(),pos=(0,0),size=(440,285),
                        style=0,name=u'pnSoftware')
    def SetRegNode(self,obj):
        self.pnSoftware.SetRegNode(obj)
    def SetDoc(self,doc,bNet=False):
        self.pnSoftware.SetDoc(doc,bNet)
    def SetNode(self,node):
        self.pnSoftware.SetNode(node)
    def GetNode(self):
        self.pnSoftware.GetNode()
    def OnCbApplyButton(self, event):
        self.GetNode()
        self.EndModal(1)
        event.Skip()
    def OnCbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
    
