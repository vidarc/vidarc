#----------------------------------------------------------------------------
# Name:         vXmlNodeSoftware.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060220
# CVS-ID:       $Id: vXmlNodeSoftware.py,v 1.4 2007/05/07 18:46:44 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlNodeBase import *
#from vidarc.vApps.vLoc.vXmlLoc import COUNTRIES
try:
    from vidarc.vApps.vSoftware.vXmlNodeSoftwarePanel import *
    from vidarc.vApps.vSoftware.vXmlNodeSoftwareEditDialog import *
    from vidarc.vApps.vSoftware.vXmlNodeSoftwareAddDialog import *
    GUI=1
except:
    GUI=0


class vXmlNodeSoftware(vtXmlNodeBase):
    NODE_ATTRS=[
            ('Name',None,'name',None),
            ('Abbreviation',None,'abbreviation',None),
            #('region',None,'region',None),
            #('distance',None,'distance',None),
            #('coor',None,'coor',None),
        ]
    FUNCS_GET_4_TREE=['Name']
    FMT_GET_4_TREE=[('Name','')]
    def __init__(self,tagName='software'):
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'software')
    # ---------------------------------------------------------
    # specific
    def GetAbbreviation(self,node):
        return self.Get(node,'abbreviation')
    def GetDir(self,node):
        return self.Get(node,'dn')
    def GetName(self,node):
        return self.Get(node,'name')
    
    def SetAbbreviation(self,node,val):
        self.Set(node,'abbreviation',val)
    def SetDir(self,node,val):
        self.Set(node,'dn',val)
    def SetName(self,node,val):
        self.Set(node,'name',val)
    def __getSW__(self,node,l):
        if self.doc.getTagName(node)=='software':
            iId=self.doc.getKeyNum(node)
            l.append(iId)
        return 0
    def GetSWRecLst(self,node):
        l=[]
        self.doc.procKeysRec(100,node,self.__getSW__,l)
        return l
    # ---------------------------------------------------------
    # inheritance
    def IsSkip(self):
        return False
    def Is2Add(self):
        return True
    def IsId2Add(self):
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02\xd3IDAT8\x8d}\x93]h\x93w\x14\xc6\x7f\xff7\xe9Kb\xb2\xd85j\xdb\x14+q\
\xcd\xd2\xd5PK\x15\xbb\x0b\'\xd1BD\xad\n\x8e\x89\x03\x11\xb7y\xe1\x07\x03\
\xd9\x8d\xdf\x10P\x987\xbb\xe8\xbd\x9b\x0c\xfc\x98\xf8\x05\x8a\x1fD\xd3\x06\
\xd4:\xed:\xed\\\xb1Vc\xa7\xad\xdd^\xcc\x9b\xbcn\xb6\xa6i\xcd\xf1"\xf8\xd9\
\xba\x03\xcf\xd59\xcf9\xcf9\x9c\x07\xa5\xd9\x18\x0fs\xea\x03\x02Qy_\xfe%\xec\
\x8c\x13\x92\x8f\xcb\xae\x9d\x9d8\xddn6\x7f7]\xec\xb607;+\x800JkTo\xd6*\xa5\
\xd9\xc64\x98U\xfb\xb5\xb4\xb7\xefd\xdf\xfe\x16L\xebo\x96-\t30\xd0E"\xd6\xc6\
P\xce\xa2%\xe6\xe1\xe6m?J\xdb\xad\xb4\xf1\xa6\xcf\x99[\xc4\x81_\xce\xe2\xf6\
\xe8TW\xf9\xf9~o3\xd3*\xe7aw\x95\xb3\xf6\x9bU\\\xebX\xca\xa3\x9eR\x1afM\x921\
\r\xecEm\xac]\xbd\x86\xa9\xbei|\xf8\xfc.\x81R\x8do\xd7o"y\xaf\x07+c\x10\x08T\
\x80\x0c\xe3pe\xf0\x143\xf6\x06\x9b7\xdd\xe1\xdaoN\xa6\x0c\xb7\xb1r\xf9\n\
\xf0\x96248J\xec\x1f\x8b\x12/X\xa9;d\x87\xfa\x18\xfc\xcf$va\x036\xf5\xc6\x16\
\x0e}\xb7,^\xe2@D\xe3\xabO\xa1\xab\xa7\x9f\xee\xd4\x10\x81\xe0T\x8a\xf5\x12\
\xee\xf5\x8f\xd0\xdea\x90Iw\xe3q\r\xb3\xefg/\xaf\xd8\x92\x8f\xcb\x9a\xd5\xbf\
R^\x16\xa0\xa2\xac\x94\\\xd6\xe0\xca\xf5\xdfyt\xdf\x04\xcbb\x82\xca\x11\xfa\
\xa4\x06\x7f\xa5\x1f33J\xe2R\x1f\x10~\xad`\xf6L{\xf4\xd4\x99m\x04\xaa|\xc4\
\xe2\xed\xe8\xd9\x87\x84k|\xd4\x94\x8f\x905\x07\xe8LM\xe4\xd0\xb1\x1f\x08\
\x06\xd34\xd4\x97p\xf2\xf4\x1f\xdc\xeaj.\xb0%\xbfKZ[\x97\x81<\xc5H\xf5S\x1b\
\xaa\xa3\xd7\xde\xc8m\xe3\x19\x03F\x16\xab\xf83\xfe\xbco\x90W\x1a\xc1\x8f<\
\xe4\x87s$.W\x15\x8e.\xf9\xb8\x1c\xf9\xe9<\xee\xe2\x1c\xf0\x18+\xfd\x94\xe72\
\x81\x07\xfd\xbd\xb8C\x9f\xf3\xaf\xc3\xc5\x99\x1f\x8f\xb3uK\x94l.\xc7\xddd\
\x17\x9a2\xe9{x\x0e\x88\x8a\xcd\xef\x7f\x1c\x8d,\x1a\xa5\xfac\x1fJ\t\xe5>\'z\
Q\x8a\xa6\xa6j\xae\\60\xcd43\x82=tw_\xc5\xa6\x92\xd4\xd7y9p\xf0\x16\x1d7\x8c\
\x82\x82\xbf\x1e\x9cW_\xac\x8aK]m3;\xb6\xbbiZ\xd8@e\xc5\x07hj\x94\xc9S\xd2\
\xf4&\x93\x84j\xfc\x94\xf9&\xe1p9\xc9>\xc9\xd0\xdbW +\xed\x92z\xeb\x95%\x1f\
\x17H\xb0q\xdd1\xe67\xcedqd\x06v\x97\x17x\x86\xae;\x81\x11Z/\xb6\xb0 r\x02H\
\x14|\xf1>\x97AB *\x1b\xd7\x85\xe4\xe8\xe1/\xe5If\xaf\x0c\x9a{$\x12\x99,\x90\
x\xe5\xd2\xff\xb5\xeaK\xe8zB\xecEQY\xf8\x0eYi6^\x00\xc5\x89"B\xc3R\xdcd\x00\
\x00\x00\x00IEND\xaeB`\x82'
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return vXmlNodeSoftwareEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return vXmlNodeSoftwareAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeSoftwarePanel
        else:
            return None
