#Boa:Dialog:vSoftwareSettingsDialog
#----------------------------------------------------------------------------
# Name:         vSoftwareSettingsDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060629
# CVS-ID:       $Id: vSoftwareSettingsDialog.py,v 1.1 2006/07/17 11:25:01 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import wx.lib.filebrowsebutton

import vidarc.tool.art.vtArt as vtArt

def create(parent):
    return vSoftwareSettingsDialog(parent)

[wxID_VSOFTWARESETTINGSDIALOG, wxID_VSOFTWARESETTINGSDIALOGCBAPPLY, 
 wxID_VSOFTWARESETTINGSDIALOGCBCANCEL, wxID_VSOFTWARESETTINGSDIALOGDBBDN, 
] = [wx.NewId() for _init_ctrls in range(4)]

class vSoftwareSettingsDialog(wx.Dialog):
    def _init_coll_fgsDta_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)
        parent.AddGrowableCol(0)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(16, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_fgsDta_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.dbbDN, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsDta = wx.FlexGridSizer(cols=1, hgap=0, rows=4, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsDta_Items(self.fgsDta)
        self._init_coll_fgsDta_Growables(self.fgsDta)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.fgsDta)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VSOFTWARESETTINGSDIALOG,
              name=u'vSoftwareSettingsDialog', parent=prnt, pos=wx.Point(66,
              66), size=wx.Size(400, 250), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'vSoftware Settings Dialog')
        self.SetClientSize(wx.Size(392, 223))

        self.dbbDN = wx.lib.filebrowsebutton.DirBrowseButton(buttonText='...',
              dialogTitle=_('Choose a directory'),
              id=wxID_VSOFTWARESETTINGSDIALOGDBBDN,
              labelText=_('Select a directory:'), parent=self, pos=wx.Point(4,
              4), size=wx.Size(384, 29), startDirectory='.', style=0)

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VSOFTWARESETTINGSDIALOGCBAPPLY,
              bitmap=vtArt.getBitmap(vtArt.Apply), label=_(u'Apply'),
              name=u'cbApply', parent=self, pos=wx.Point(112, 189),
              size=wx.Size(76, 30), style=0)
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VSOFTWARESETTINGSDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VSOFTWARESETTINGSDIALOGCBCANCEL,
              bitmap=vtArt.getBitmap(vtArt.Cancel), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(204, 189),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VSOFTWARESETTINGSDIALOGCBCANCEL)

        self._init_sizers()

    def __init__(self, parent):
        self._init_ctrls(parent)

    def SetXml(self,doc,node):
        oSettings=doc.GetReg('Settings')
        sDir=oSettings.GetDir(node)
        self.dbbDN.SetValue(sDir)
    def GetXml(self,doc,node):
        oSettings=doc.GetReg('Settings')
        sDir=self.dbbDN.GetValue()
        oSettings.SetDir(node,sDir)
        
    def OnCbApplyButton(self, event):
        event.Skip()
        self.EndModal(1)

    def OnCbCancelButton(self, event):
        event.Skip()
        self.EndModal(0)
