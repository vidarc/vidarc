#----------------------------------------------------------------------------
# Name:         vXmlNodeSoftwareBuild.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      2006
# CVS-ID:       $Id: vXmlNodeSoftwareBuild.py,v 1.4 2008/02/02 16:10:54 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeSoftwareBuild(vtXmlNodeBase):
    NODE_ATTRS=[
            #('Tag',None,'tag',None),
            #('Name',None,'name',None),
        ]
    FUNCS_GET_SET_4_LST=[]
    def __init__(self,tagName='SoftwareBuild'):
        global _
        _=vtLgBase.assignPluginLang('vSoftware')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'software build')
    # ---------------------------------------------------------
    # specific
    def __procHosts__(self,node,d):
        sTag=self.doc.getTagName(node)
        if sTag=='hostLicense':
            oHost=self.doc.GetReg('hostLicense')
            dHost=oHost.GetInfos(node)
            mid=dHost['mid']
            if mid not in d:
                d[mid]=dHost
        return 0
    def __procCustomer__(self,node,d):
        sTag=self.doc.getTagName(node)
        if sTag=='customer':
            fid=self.doc.getAttribute(node,'fid')
            netDoc,nodeCust=self.doc.GetNode(fid)
            oCust=netDoc.GetRegByNode(nodeCust)
            sName=oCust.GetName(nodeCust)
            sNr=oCust.GetNumber(nodeCust)
            if sName not in d:
                dHosts={}
                d[sName]={'nr':sNr,'fid':fid,'hosts':dHosts}
                self.doc.procChildsExt(node,self.__procHosts__,dHosts)
        return 0
    def __procSoftwareCustomer__(self,nodeCust,d):
        sTag=self.doc.getTagName(nodeCust)
        if sTag=='SoftwareCustomer':
            sId=self.doc.getKey(nodeCust)
            oCust=self.doc.GetRegByNode(nodeCust)
            sName=oCust.GetName(nodeCust)
            sNr=oCust.GetClientNr(nodeCust)
            if sName not in d:
                dHosts=oCust.GetHostNameDict(nodeCust)
                dSW=oCust.GetSWLst(nodeCust)
                d[sName]={'id':sId,'nr':sNr,'hosts':dHosts,'sw':dSW}
        return 0
    def GetCustomerDict(self,node):
        if node is None:
            node=self.doc.getBaseNode()
        d={}
        self.doc.procChildsExt(node,self.__procSoftwareCustomer__,d)
        return d
    def GetName(self,node):
        return self.GetML(node,'name')
    def SetCustomerDict(self,node,d):
        for c in self.doc.getChilds(node,'customer'):
            self.doc.deleteNode(c,node)
        keys=d.keys()
        keys.sort()
        oHost=self.doc.GetReg('hostLicense')
        for k in keys:
            c=self.doc.createSubNodeTextAttr(node,'customer','','fid',d[k]['fid'])
            if 'hosts' in d[k]:
                dHosts=d[k]['hosts']
                kkeys=dHosts.keys()
                kkeys.sort()
                for kk in kkeys:
                    cc=self.doc.createSubNodeTextAttr(c,'hostLicense','','mid',kk)
                    oHost.SetInfos(cc,dHosts[kk])
            self.doc.AlignNode(c)
    def SetName(self,node,val):
        self.SetML(node,'name',val)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_ATTR
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return False
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return False
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return True
    def IsId2Add(self):
        "node id is managed"
        return False
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00\x1cIDAT8\x8dcddbf\xa0\x040Q\xa4{\xd4\x80Q\x03F\r\x18D\x06\x00\x00]b\
\x00&\x87\xd5\x92\xeb\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        return None
    def GetAddDialogClass(self):
        return None
    def GetPanelClass(self):
        return None
