#Boa:FramePanel:vXmlNodeSoftwareMetricPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeSoftwareMetricPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060328
# CVS-ID:       $Id: vXmlNodeSoftwareMetricPanel.py,v 1.6 2008/03/29 23:42:48 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.art.vtThrobber
import wx.lib.buttons
import wx.lib.filebrowsebutton
from vidarc.tool.net.vNetXmlWxGui import *
import vidarc.tool.xml.vtXmlDom as vtXmlDom
import vidarc.tool.xml.vtXmlDomConsumer as vtXmlDomConsumer
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.time.vtTime as vtTime

import sys,os,os.path
import threading,thread,time

[wxID_VXMLNODESOFTWAREMETRICPANEL, wxID_VXMLNODESOFTWAREMETRICPANELCBGEN, 
 wxID_VXMLNODESOFTWAREMETRICPANELCBSTOP, 
 wxID_VXMLNODESOFTWAREMETRICPANELCHCCOMPARE, 
 wxID_VXMLNODESOFTWAREMETRICPANELCHCSNAPSHOT, 
 wxID_VXMLNODESOFTWAREMETRICPANELLBLCMP, 
 wxID_VXMLNODESOFTWAREMETRICPANELLBLFILES, 
 wxID_VXMLNODESOFTWAREMETRICPANELLBLINFO, 
 wxID_VXMLNODESOFTWAREMETRICPANELLBLSNAPSHOT, 
 wxID_VXMLNODESOFTWAREMETRICPANELLSTINFO, 
 wxID_VXMLNODESOFTWAREMETRICPANELTHRPROC, 
] = [wx.NewId() for _init_ctrls in range(11)]

wxEVT_THREAD_METRIC_ELEMENTS=wx.NewEventType()
def EVT_THREAD_METRIC_ELEMENTS(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_METRIC_ELEMENTS,func)
class wxThreadMetricElements(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_METRIC_ELEMENTS(<widget_name>, self.OnItemSel)
    """

    def __init__(self,fn,iVal,iCount=-1):
        wx.PyEvent.__init__(self)
        self.fn=fn
        self.val=iVal
        self.count=iCount
        self.SetEventType(wxEVT_THREAD_METRIC_ELEMENTS)
    def GetFN(self):
        self.fn
    def GetValue(self):
        return self.val
    def GetCount(self):
        return self.count
    def GetStr(self):
        return '%08d-%08d %s'%(self.val,self.count,self.fn)
wxEVT_THREAD_METRIC_ELEMENTS_FINISHED=wx.NewEventType()
def EVT_THREAD_METRIC_ELEMENTS_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_METRIC_ELEMENTS_FINISHED,func)
class wxThreadMetricElementsFinished(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_METRIC_ELEMENTS_FINISHED(<widget_name>, self.OnItemSel)
    """
    def __init__(self):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_THREAD_METRIC_ELEMENTS_FINISHED)
wxEVT_THREAD_METRIC_ELEMENTS_ABORTED=wx.NewEventType()
def EVT_THREAD_METRIC_ELEMENTS_ABORTED(win,func):
    win.Connect(-1,-1,wxEVT_THREAD_METRIC_ELEMENTS_ABORTED,func)
class wxThreadMetricElementsAborted(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_THREAD_METRIC_ELEMENTS_ABORTED(<widget_name>, self.OnItemSel)
    """
    def __init__(self):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_THREAD_METRIC_ELEMENTS_ABORTED)

class vMetricThread:
    LST_SRC_EXT=['.py','.xml']
    def __init__(self,par=None):
        self.running=False
        self.keepGoing = False
        self.par=par
        self.dn=''
        self.files=[]
        self.dRes={}
        if self.par is not None:
            self.updateProcessbar=True
        else:
            self.updateProcessbar=False
        
    def Metric(self,dn):
        if self.running:
            return
        self.dn=dn
        self.dRes={}
        self.files=[]
        self.keepGoing = self.running=True
        thread.start_new_thread(self.Run, ())
    def Stop(self):
        self.keepGoing=False
    def IsRunning(self):
        return self.running
    def __add__(self,key,iVal):
        try:
            val=self.dRes[key]
        except:
            val=0
        self.dRes[key]=val+iVal
    def GetResult(self):
        return self.dRes
    def Analyse(self,fn):
        try:
            f=open(fn)
            lines=f.readlines()
            for sExt in self.LST_SRC_EXT:
                iLen=len(sExt)
                if fn[-iLen:]==sExt:
                    ext=sExt[1:]
                    break
            self.__add__('_lines',len(lines))
            self.__add__('_files',1)
            self.__add__('%s_lines'%ext,len(lines))
            self.__add__('%s_files'%ext,1)
            if ext=='py':
                iIndent=0
                def calcIndent(s):
                    try:
                        i=0
                        while s[i] in ['\t',' ']:
                            i+=1
                        return i
                    except:
                        i=0
                    return i
                for l in lines:
                    i=calcIndent(l)
                    iIndentNew=i
                    if l[i:i+6]=='class ':
                        self.__add__('%s_objs'%ext,1)
                    if l[i:i+4]=='def ':
                        self.__add__('%s_methodes'%ext,1)
        except:
            vtLog.vtLngTB('vMetricThread')
    def Run(self):
        try:
            self.iCount=0
            self.iAct=0
            for root, dirs, files in os.walk(self.dn):
                #self.iCount+=len(files)
                #print root
                #print '   ',dirs
                #print '   ',files
                for fn in files:
                    for sExt in self.LST_SRC_EXT:
                        iLen=len(sExt)
                        if fn[-iLen:]==sExt:
                            #if sExt=='.py':
                            #    if fn[:6]=='images':
                            #        break
                            self.files.append(os.path.join(root,fn))
                            break
                if 'CVS' in dirs:
                    dirs.remove('CVS')
            self.iCount=len(self.files)
            if self.updateProcessbar:
                wx.PostEvent(self.par,wxThreadMetricElements('',self.iAct,self.iCount))
            self.iAct=0
            for fn in self.files:
                self.Analyse(fn)
                #time.sleep(1)
                self.iAct+=1
                if self.updateProcessbar:
                    wx.PostEvent(self.par,wxThreadMetricElements(fn,self.iAct,self.iCount))
                if self.keepGoing==False:
                    break
            if self.keepGoing==False:
                if self.updateProcessbar:
                    wx.PostEvent(self.par,wxThreadMetricElementsAborted())
            else:
                if self.updateProcessbar:
                    wx.PostEvent(self.par,wxThreadMetricElementsFinished())
        except:
            if self.updateProcessbar:
                wx.PostEvent(self.par,wxThreadMetricElementsAborted())
            vtLog.vtLngTB('vMetricThread')
        self.keepGoing = self.running = False

class vXmlNodeSoftwareMetricPanel(wx.Panel,vtXmlDomConsumer.vtXmlDomConsumer):
    def _init_coll_bxsCmp_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblCmp, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.chcCompare, 3, border=4, flag=wx.EXPAND | wx.LEFT)

    def _init_coll_bxsSnapShot_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblSnapShot, 1, border=4,
              flag=wx.EXPAND | wx.LEFT)
        parent.AddWindow(self.chcSnapShot, 3, border=4,
              flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsInfo_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblInfo, 1, border=0, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lstInfo, 3, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsSnapShot, 1, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsCmp, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsInfo, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsButton, 1, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_bxsButton_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.thrProc, 0, border=0, flag=0)
        parent.AddWindow(self.cbGen, 0, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblFiles, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbStop, 0, border=4, flag=wx.LEFT)

    def _init_coll_lstInfo_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT, heading=u'name',
              width=-1)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_RIGHT,
              heading=u'count', width=-1)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_RIGHT, heading=u'diff',
              width=-1)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=0, rows=5, vgap=0)

        self.bxsButton = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsInfo = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsSnapShot = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsCmp = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsButton_Items(self.bxsButton)
        self._init_coll_bxsInfo_Items(self.bxsInfo)
        self._init_coll_bxsSnapShot_Items(self.bxsSnapShot)
        self._init_coll_bxsCmp_Items(self.bxsCmp)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODESOFTWAREMETRICPANEL,
              name=u'vXmlNodeSoftwareMetricPanel', parent=prnt, pos=wx.Point(5,
              8), size=wx.Size(312, 215),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 188))
        self.SetAutoLayout(True)

        self.lblInfo = wx.StaticText(id=wxID_VXMLNODESOFTWAREMETRICPANELLBLINFO,
              label=u'Info', name=u'lblInfo', parent=self, pos=wx.Point(0, 46),
              size=wx.Size(76, 114), style=wx.ALIGN_RIGHT)

        self.lstInfo = wx.ListCtrl(id=wxID_VXMLNODESOFTWAREMETRICPANELLSTINFO,
              name=u'lstInfo', parent=self, pos=wx.Point(80, 46),
              size=wx.Size(224, 114), style=wx.LC_REPORT)
        self._init_coll_lstInfo_Columns(self.lstInfo)

        self.cbGen = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODESOFTWAREMETRICPANELCBGEN,
              bitmap=wx.EmptyBitmap(16 , 16), name=u'cbGen', parent=self,
              pos=wx.Point(19, 160), size=wx.Size(31, 28), style=0)
        self.cbGen.Bind(wx.EVT_BUTTON, self.OnCbGenButton,
              id=wxID_VXMLNODESOFTWAREMETRICPANELCBGEN)

        self.cbStop = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODESOFTWAREMETRICPANELCBSTOP,
              bitmap=wx.EmptyBitmap(16 , 16), name=u'cbStop', parent=self,
              pos=wx.Point(273, 160), size=wx.Size(31, 30), style=0)
        self.cbStop.Bind(wx.EVT_BUTTON, self.OnCbStopButton,
              id=wxID_VXMLNODESOFTWAREMETRICPANELCBSTOP)

        self.thrProc = vidarc.tool.art.vtThrobber.vtThrobberDelay(id=wxID_VXMLNODESOFTWAREMETRICPANELTHRPROC,
              name=u'thrProc', parent=self, pos=wx.Point(0, 160),
              size=wx.Size(15, 15), style=0)

        self.lblSnapShot = wx.StaticText(id=wxID_VXMLNODESOFTWAREMETRICPANELLBLSNAPSHOT,
              label=u'snap shot', name=u'lblSnapShot', parent=self,
              pos=wx.Point(4, 4), size=wx.Size(72, 21), style=wx.ALIGN_RIGHT)

        self.chcSnapShot = wx.Choice(choices=[],
              id=wxID_VXMLNODESOFTWAREMETRICPANELCHCSNAPSHOT,
              name=u'chcSnapShot', parent=self, pos=wx.Point(80, 4),
              size=wx.Size(224, 21), style=0)
        self.chcSnapShot.Bind(wx.EVT_CHOICE, self.OnChcSnapShotChoice,
              id=wxID_VXMLNODESOFTWAREMETRICPANELCHCSNAPSHOT)

        self.lblCmp = wx.StaticText(id=wxID_VXMLNODESOFTWAREMETRICPANELLBLCMP,
              label=u'compare', name=u'lblCmp', parent=self, pos=wx.Point(4,
              25), size=wx.Size(72, 21), style=wx.ALIGN_RIGHT)

        self.chcCompare = wx.Choice(choices=[],
              id=wxID_VXMLNODESOFTWAREMETRICPANELCHCCOMPARE, name=u'chcCompare',
              parent=self, pos=wx.Point(80, 25), size=wx.Size(224, 21),
              style=0)
        self.chcCompare.Bind(wx.EVT_CHOICE, self.OnChcCompareChoice,
              id=wxID_VXMLNODESOFTWAREMETRICPANELCHCCOMPARE)

        self.lblFiles = wx.StaticText(id=wxID_VXMLNODESOFTWAREMETRICPANELLBLFILES,
              label=u'', name=u'lblFiles', parent=self, pos=wx.Point(54, 160),
              size=wx.Size(215, 28), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        self._init_ctrls(parent)
        
        vtXmlDomConsumer.vtXmlDomConsumer.__init__(self)
        
        self.thdMetric=vMetricThread(self)
        self.cbGen.SetBitmapLabel(vtArt.getBitmap(vtArt.Build))
        self.cbStop.SetBitmapLabel(vtArt.getBitmap(vtArt.Cancel))
        
        EVT_THREAD_METRIC_ELEMENTS(self,self.OnProc)
        EVT_THREAD_METRIC_ELEMENTS_FINISHED(self,self.OnFinished)
        EVT_THREAD_METRIC_ELEMENTS_ABORTED(self,self.OnAborted)
        
        self.bModified=False
        self.bAutoApply=False
        self.bBlock=False
        
        self.dSnapShot={}
        self.dt=vtTime.vtDateTime(True)
        
        self.lstMarkObjs=[]
        
        self.Move(pos)
        self.SetSize(size)
    
    def OnProc(self,evt):
        self.lblFiles.SetLabel(evt.GetStr())
        evt.Skip()
    def OnFinished(self,evt):
        self.lblFiles.SetLabel('finished')
        dRes=self.thdMetric.GetResult()
        self.dt.Now()
        sDt=self.dt.GetDateTimeStr(' ')
        self.dSnapShot[sDt]=dRes
        self.chcSnapShot.Append(sDt)
        self.chcCompare.Append(sDt)
        self.chcSnapShot.SetStringSelection(sDt)
        self.chcCompare.SetSelection(self.chcCompare.GetCount()-1)
        
        self.__showSnapShotRes__()
        
        evt.Skip()
    def OnAborted(self,evt):
        self.lblFiles.SetLabel('aborted')
        evt.Skip()
    
    def __showSnapShotRes__(self):
        self.lstInfo.DeleteAllItems()
        sDtAct=self.chcSnapShot.GetStringSelection()
        sDtCmp=self.chcCompare.GetStringSelection()
        try:
            dResAct=self.dSnapShot[sDtAct]
        except:
            dResAct=None
            return
        try:
            dResCmp=self.dSnapShot[sDtCmp]
        except:
            dResCmp=None
        keys=dResAct.keys()
        keys.sort()
        for k in keys:
            idx=self.lstInfo.InsertStringItem(sys.maxint,k)
            self.lstInfo.SetStringItem(idx,1,str(dResAct[k]))
            if dResCmp is not None:
                try:
                    self.lstInfo.SetStringItem(idx,2,str(dResAct[k]-dResCmp[k]))
                except:
                    pass
        
    def Lock(self,flag):
        if flag:
            pass
        else:
            pass
    def SetAutoApply(self,flag):
        self.bAutoApply=flag
    def __clearBlock__(self):
        self.bBlock=False
    def SetModified(self,state,obj=None):
        if state:
            if obj is None:
                for obj in self.lstMarkObjs:
                    f=obj.GetFont()
                    f.SetWeight(wx.FONTWEIGHT_BOLD)
                    obj.SetFont(f)
                    obj.Refresh()
            else:
                f=obj.GetFont()
                f.SetWeight(wx.FONTWEIGHT_BOLD)
                obj.SetFont(f)
                obj.Refresh()
        else:
            if obj is None:
                for obj in self.lstMarkObjs:
                    f=obj.GetFont()
                    f.SetWeight(wx.FONTWEIGHT_NORMAL)
                    obj.SetFont(f)
                    obj.Refresh()
                #self.viHum.__markModified__(False)
            else:
                f=obj.GetFont()
                f.SetWeight(wx.FONTWEIGHT_NORMAL)
                obj.SetFont(f)
                obj.Refresh()
        self.bModified=state    
    def GetModified(self):
        return self.bModified
    def __isModified__(self):
        if self.bModified:
            return True
        return False
    def Clear(self):
        self.bBlock=True
        self.SetModified(False)
        vtXmlDomConsumer.vtXmlDomConsumer.Clear(self)
        self.lstInfo.DeleteAllItems()
        self.chcSnapShot.Clear()
        self.chcCompare.Clear()
        self.lblFiles.SetLabel('')
        self.dSnapShot={}
        wx.CallAfter(self.__clearBlock__)
    def SetRegNode(self,o):
        pass
    def SetNetDocs(self,d):
        pass
    def SetDoc(self,doc,bNet):
        vtXmlDomConsumer.vtXmlDomConsumer.SetDoc(self,doc)
    def SetNode(self,node):
        #if self.bModified==True:
        #    if self.bAutoApply:
        #        self.GetNode(self.node)
        #    else:
                # ask
        #        dlg=wx.MessageDialog(self,u'Do you to apply modified data?' ,
        #                    _(u'vLog Info'),
        #                    wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
        #        if dlg.ShowModal()==wx.ID_YES:
        #            self.GetNode(self.node)
        self.bBlock=True
        self.SetModified(False)
        self.Clear()
        vtXmlDomConsumer.vtXmlDomConsumer.SetNode(self,node)
        if self.doc is not None:
            nodeTmp=self.doc.getChild(node,'snapshots')
            for c in self.doc.getChilds(nodeTmp,'snapshot'):
                d={}
                sDt=self.doc.getNodeText(c,'datetime')
                for cc in self.doc.getChilds(self.doc.getChild(c,'info')):
                    try:
                        d[self.doc.getTagName(cc)]=long(self.doc.getText(cc))
                    except:
                        pass
                self.dt.SetStr(sDt)
                sDt=self.dt.GetDateTimeStr(' ')
                self.dSnapShot[sDt]=d
        keys=self.dSnapShot.keys()
        keys.sort()
        keys.reverse()
        self.chcCompare.Append('---')
        for k in keys:
            self.chcSnapShot.Append(k)
            self.chcCompare.Append(k)
        self.chcCompare.SetSelection(0)
        try:
            self.chcSnapShot.SetSelection(0)
        except:
            pass
        self.__showSnapShotRes__()
        wx.CallAfter(self.__clearBlock__)
    def GetNode(self,node=None):
        if node is None:
            node=self.node
        if node is not None:
            pass
        self.SetModified(False)
        if self.doc is not None:
            nodeTmp=self.doc.getChild(node,'snapshots')
            self.doc.delNode(nodeTmp)
            nodeTmp=self.doc.getChildForced(node,'snapshots')
            keys=self.dSnapShot.keys()
            keys.sort()
            for k in keys:
                c=self.doc.createSubNode(nodeTmp,'snapshot')
                self.doc.setNodeText(c,'datetime',k)
                dRes=self.dSnapShot[k]
                kkeys=dRes.keys()
                kkeys.sort()
                cc=self.doc.getChildForced(c,'info')
                for kk in kkeys:
                    self.doc.setNodeText(cc,kk,str(dRes[kk]))
            self.doc.AlignNode(nodeTmp,iRec=4)
        pass
    def Apply(self,bDoApply=False):
        #self.SetModified(False)
        return False
    def Cancel(self):
        self.SetModified(False)

    def OnCbGenButton(self, event):
        try:
            dn=self.doc.getNodeText(self.node,'dn')
            self.thdMetric.Metric(dn)
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()

    def OnCbStopButton(self, event):
        self.thdMetric.Stop()
        event.Skip()

    def OnChcSnapShotChoice(self, event):
        self.__showSnapShotRes__()
        event.Skip()

    def OnChcCompareChoice(self, event):
        self.__showSnapShotRes__()
        event.Skip()
