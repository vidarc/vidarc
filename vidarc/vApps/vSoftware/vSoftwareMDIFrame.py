#Boa:MDIChild:vSoftwareMDIFrame
#----------------------------------------------------------------------------
# Name:         vSoftwareMDIFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20061116
# CVS-ID:       $Id: vSoftwareMDIFrame.py,v 1.4 2008/03/29 23:42:48 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

MDI_CHILD_FRAME=True

from vidarc.vApps.common.vMDIFrame import vMDIFrame
import vidarc.vApps.vSoftware.images as imgSoftware
import vidarc.tool.log.vtLog as vtLog

try:
    from vidarc.vApps.vSoftware.vSoftwareMainPanel import *
except:
    vtLog.vtLngTB('import')

def getPluginImage():
    return imgSoftware.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(imgSoftware.getApplicationBitmap())
    return icon

DOMAINS=['implementation','vSoftware']
def getDomainTransLst(lang):
    import vidarc.__init__ as vInit
    return vInit.getDomainTrans(DOMAINS,lang)
def getDomainImageData():
    import vidarc.__init__ as vInit
    return vInit.getDomainImageData(DOMAINS)

def create(parent, id, pos=wx.DefaultPosition, size=wx.DefaultSize,style=0, name='vHuman'):
    return vSoftwareMDIFrame(parent)

[wxID_VSOFTWAREMDIFRAME] = [wx.NewId() for _init_ctrls in range(1)]

class vSoftwareMDIFrame(wx.MDIChildFrame,vMDIFrame):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.MDIChildFrame.__init__(self, id=wxID_VSOFTWAREMDIFRAME,
              name=u'vSoftwareMDIFrame', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(400, 250), style=wx.DEFAULT_FRAME_STYLE,
              title=u'vSoftware')
        self.SetClientSize(wx.Size(392, 223))

    def __init__(self, parent, id, pos, size,style,name):
        self._init_ctrls(parent)
        self.SetName(name)
        icon = getApplicationIcon()
        self.SetIcon(icon)
        
        vMDIFrame.__init__(self,u'Software')
        self.dCfg.update({'x':10,'y':10,'width':800,'height':300})
        
        try:
            self.pn=vSoftwareMainPanel(self,-1,wx.DefaultPosition,wx.DefaultSize,wx.TAB_TRAVERSAL,'vSoftwarePlugIn')
            self.pn.OpenCfgFile('vSoftwareCfg.xml')
            
            self.netMaster=self.pn.GetNetMaster()
            self.netDocMain=self.pn.GetDocMain()
            self.trMain=self.pn.GetTreeMain()
            EVT_NET_XML_OPEN_START(self.netDocMain,self.OnOpenStart)
            EVT_NET_XML_OPEN_OK(self.netDocMain,self.OnOpenOk)
            EVT_NET_XML_OPEN_FAULT(self.netDocMain,self.OnOpenFault)
            
            self.BindNetDocEvents(None,None)
            self.netMaster.BindEvents(
                    funcStart=self.OnMasterStart,
                    funcFinish=self.OnMasterFinish,
                    funcShutDown=self.OnMasterShutDown)
            
            EVT_NET_XML_GOT_CONTENT(self.netDocMain,self.OnGotContent)
            
            EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.trMain,self.OnAddElementsFin)
            EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.trMain,self.OnAddElementsProgress)
        
            self.AddMenus(self , self.trMain , self.netMaster,
                self.mnFile , self.mnView , self.mnTools)
        
        except:
            vtLog.vtLngTB(self.GetName())
        #self.Move(pos)
        #self.SetSize(size)
        #self.Layout()
        #self.Refresh()
