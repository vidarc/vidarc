#Boa:FramePanel:vSoftwareMainPanel
#----------------------------------------------------------------------------
# Name:         vSoftwareMainPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20061116
# CVS-ID:       $Id: vSoftwareMainPanel.py,v 1.2 2007/07/30 20:38:49 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.xml.vtXmlNodeRegSelector
import time

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.vApps.common.vSystem as vSystem

try:
    from vidarc.tool.net.vtNetSecXmlGuiMaster import *
    
    from vidarc.vApps.vSoftware.vNetSoftware import *
    from vidarc.vApps.vSoftware.vXmlSoftwareTree import *
    from vidarc.tool.xml.vtXmlNodeRegListBook import *
    from vidarc.vApps.vSoftware.vSoftwareListBook import *
    from vidarc.vApps.vSoftware.vSoftwareSettingsDialog import *
    
    from vidarc.vApps.vCustomer.vNetCustomer import vNetCustomer
    from vidarc.vApps.vPrjDoc.vNetPrjDoc import vNetPrjDoc
    from vidarc.vApps.vPrj.vNetPrj import vNetPrj
    from vidarc.vApps.vDoc.vNetDoc import vNetDoc
    from vidarc.vApps.vHum.vNetHum import vNetHum
    from vidarc.vApps.vGlobals.vNetGlobals import vNetGlobals
    
    from vidarc.tool.xml.vtXmlDom import vtXmlDom
    
    import vidarc.vApps.vSoftware.images as imgSoftware
except:
    vtLog.vtLngTB('import')

def create(parent):
    return vSoftwareMainPanel(parent)

[wxID_VSOFTWAREMAINPANEL, wxID_VSOFTWAREMAINPANELPNDATA, 
 wxID_VSOFTWAREMAINPANELSLWNAV, wxID_VSOFTWAREMAINPANELSLWTOP, 
 wxID_VSOFTWAREMAINPANELVIREGSEL, 
] = [wx.NewId() for _init_ctrls in range(5)]

def getPluginImage():
    return imgSoftware.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(imgSoftware.getPluginBitmap())
    return icon

class vSoftwareMainPanel(wx.Panel):
    VERBOSE=0
    def _init_sizers(self):
        # generated method, don't edit
        self.bxsData = wx.BoxSizer(orient=wx.VERTICAL)

        self.pnData.SetSizer(self.bxsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VSOFTWAREMAINPANEL,
              name=u'vSoftwareMainPanel', parent=prnt, pos=wx.Point(325, 29),
              size=wx.Size(650, 400), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(642, 373))
        self.Bind(wx.EVT_SIZE, self.OnSize)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VSOFTWAREMAINPANELSLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 0), size=wx.Size(200,
              376), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetAutoLayout(True)
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetLabel(_(u'navigation'))
        self.slwNav.SetToolTipString(_(u'navigation'))
        self.slwNav.SetDefaultSize(wx.Size(200, 100))
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VSOFTWAREMAINPANELSLWNAV)

        self.slwTop = wx.SashLayoutWindow(id=wxID_VSOFTWAREMAINPANELSLWTOP,
              name=u'slwTop', parent=self, pos=wx.Point(200, 0),
              size=wx.Size(440, 100), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwTop.SetDefaultSize(wx.Size(440, 100))
        self.slwTop.SetAlignment(wx.LAYOUT_TOP)
        self.slwTop.SetAutoLayout(True)
        self.slwTop.SetSashVisible(wx.SASH_BOTTOM, True)
        self.slwTop.SetOrientation(wx.LAYOUT_HORIZONTAL)
        self.slwTop.SetLabel(_(u'information'))
        self.slwTop.SetToolTipString(_(u'information'))
        self.slwTop.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwTopSashDragged,
              id=wxID_VSOFTWAREMAINPANELSLWTOP)

        self.pnData = wx.Panel(id=wxID_VSOFTWAREMAINPANELPNDATA, name=u'pnData',
              parent=self, pos=wx.Point(208, 108), size=wx.Size(424, 252),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)

        self.viRegSel = vidarc.tool.xml.vtXmlNodeRegSelector.vtXmlNodeRegSelector(id=wxID_VSOFTWAREMAINPANELVIREGSEL,
              name=u'viRegSel', parent=self.slwTop, pos=wx.Point(0, 0),
              size=wx.Size(440, 97), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        global _
        _=vtLgBase.assignPluginLang('vSoftware')
        self._init_ctrls(parent)
        self.SetName(name)
        self.bActivated=False
        self.dCfg={}
        
        self.xdCfg=vtXmlDom(appl='vSoftwareCfg',audit_trail=False)
        
        appls=['vSoftware','vCustomer','vPrjDoc','vPrj','vDoc','vHum','vGlobals']
        
        rect = parent.sbStatus.GetFieldRect(0)
        self.netMaster=vtNetSecXmlGuiMaster(parent.sbStatus,'Master',pos=(rect.x+2, rect.y+2),
                    size=(rect.width-4, rect.height-4),verbose=0)
        self.netSoftware=self.netMaster.AddNetControl(vNetSoftware,'vSoftware',synch=True,verbose=0,
                                            audit_trail=True)
        self.netCust=self.netMaster.AddNetControl(vNetCustomer,'vCustomer',synch=True,verbose=0)
        self.netPrjDoc=self.netMaster.AddNetControl(vNetPrjDoc,'vPrjDoc',synch=True,verbose=0)
        self.netPrj=self.netMaster.AddNetControl(vNetPrj,'vPrj',synch=True,verbose=0)
        self.netDoc=self.netMaster.AddNetControl(vNetDoc,'vDoc',synch=True,verbose=0)
        self.netHum=self.netMaster.AddNetControl(vNetHum,'vHum',synch=True,verbose=0)
        self.netGlb=self.netMaster.AddNetControl(vNetGlobals,'vGlobals',synch=True,verbose=0)
        self.netMaster.AddNetControl(None,None,verbose=0)
        
        self.netMaster.SetCfg(self.xdCfg,[],appls,'vSoftwareMaster')
        
        self.vgpTree=vXmlSoftwareTree(self.slwNav,wx.NewId(),
                pos=(0,0),size=(198,529),style=0,name="trSoftware",
                master=True,verbose=0)
        EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
        self.vgpTree.SetDoc(self.netSoftware,True)
        self.vgpTree.EnableClipMenu(True)
        self.vgpTree.EnableMoveMenu(True)
        
        oSW=self.netSoftware.GetReg('software')
        self.nbData=vSoftwareListBook(self.viRegSel,wx.NewId(),
                pos=(8,8),size=(470, 109),style=0,name="nbSoftware")
        #self.nbData.SetConstraints(LayoutAnchors(self.nbData, True, True, True,
        #      True))
        self.nbData.SetDoc(self.netSoftware,True)
        self.viRegSel.AddWidgetDependent(self.nbData,oSW)
        self.nbData.Show(False)
        
        oCustomer=self.netSoftware.GetReg('SoftwareCustomer')
        self.nbCustomer=vtXmlNodeRegListBook(self.viRegSel,wx.NewId(),
                    pos=(8,8),size=(576, 176),style=0,name="nbCustomer")
        self.nbCustomer.SetDoc(self.netSoftware,True)
        self.viRegSel.AddWidgetDependent(self.nbCustomer,oCustomer)
        self.nbCustomer.Show(False)
        
        oLicHost=self.netSoftware.GetRegisteredNode('hostLicense')
        pnCls=oLicHost.GetPanelClass()
        pn=pnCls(self.viRegSel,wx.NewId(),
                pos=(8,8),size=(576, 176),style=0,name="pnLicHost")
        pn.SetDoc(self.netSoftware,True)
        self.viRegSel.AddWidgetDependent(pn,oLicHost)
        pn.Show(False)
        
        self.viRegSel.SetDoc(self.netSoftware,True)
        self.viRegSel.SetRegNode(oSW)
        self.viRegSel.SetRegNode(oCustomer,bIncludeBase=True)
        self.viRegSel.SetRegNode(oLicHost)
        self.viRegSel.SetNetDocs(self.netSoftware.GetNetDocs())
        
        self.Move(pos)
        self.SetSize(size)
    def GetDocMain(self):
        return self.netSoftware
    def GetNetMaster(self):
        return self.netMaster
    def GetTreeMain(self):
        return self.vgpTree
    def OnTreeItemSel(self,event):
        event.Skip()
        node=event.GetTreeNodeData()
        self.viRegSel.SetNode(node)
    def OpenFile(self,fn):
        if self.netSoftware.ClearAutoFN()>0:
            self.netSoftware.Open(fn)
    def OpenCfgFile(self,fn=None):
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                self.netMaster.SetCfgNode()
                self.__setCfg__()
                return
        self.xdCfg.New(root='config')
        if fn is not None:
            self.xdCfg.SetFN(fn)
            self.netMaster.SetCfgNode()
        self.__setCfg__()
    def __setCfg__(self):
        try:
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSlwNavSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iWidth=event.GetDragRect().width
            #if iWidth>80:
            #    iWidth=80
            self.dCfg['nav_sash']=str(iWidth)
            
            self.slwNav.SetDefaultSize((iWidth, 1000))
            
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            self.GetCfgData()
            #event.Skip()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSlwTopSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iHeight=event.GetDragRect().height
            #if iHeight>80:
            #    iHeight=80
            self.dCfg['nav_top']=str(iHeight)
            
            self.slwTop.SetDefaultSize((1000, iHeight))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            self.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())

    def OnSize(self, event):
        event.Skip()
        try:
            bMod=False
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            
            self.pnData.Refresh()
            self.GetCfgData()
        except:
            vtLog.vtLngTB(self.GetName())

    def SetCfgData(self,func,l,d):
        self.cfgFunc=func
        self.cfgOrigin=l
        self.dCfg=d
        try:
            i=int(self.dCfg['nav_sash'])
            self.slwNav.SetDefaultSize((i, 1000))
        except:
            pass
        try:
            i=int(self.dCfg['nav_top'])
            self.slwTop.SetDefaultSize((1000, i))
        except:
            pass
        try:
            pos=map(int,self.dCfg['pos'].split(','))
            self.Move(pos)
        except:
            pass
        try:
            sz=map(int,self.dCfg['size'].split(','))
            self.SetSize(sz)
        except:
            pass
    def GetCfgData(self):
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,self.dCfg)
