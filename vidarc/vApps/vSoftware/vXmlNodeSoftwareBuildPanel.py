#Boa:FramePanel:vXmlNodeSoftwareBuildPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeSoftwareBuildPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060629
# CVS-ID:       $Id: vXmlNodeSoftwareBuildPanel.py,v 1.5 2009/01/15 17:13:10 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.misc.vtmThreadCtrl
import wx.gizmos
import vidarc.vApps.vCustomer.vXmlCustomerInputTree
import vidarc.tool.input.vtInputTree
import vidarc.tool.art.vtThrobber
import wx.lib.buttons
import wx.lib.filebrowsebutton

from vidarc.tool.net.vNetXmlWxGui import *
import vidarc.tool.xml.vtXmlDom as vtXmlDom
import vidarc.tool.xml.vtXmlDomConsumer as vtXmlDomConsumer
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.time.vtTime as vtTime
from vidarc.tool.vtThread import vtThread
from vidarc.tool.vtProcess import vtProcess
import vidarc.install.identify as vIdentify
import vidarc.tool.InOut.fnUtil as fnUtil

import vidarc.vApps.vSoftware.images as imgSW

import sys,os,os.path,shutil
import threading,thread,time

[wxID_VXMLNODESOFTWAREBUILDPANEL, wxID_VXMLNODESOFTWAREBUILDPANELCBREFRESH, 
 wxID_VXMLNODESOFTWAREBUILDPANELTGLANG, 
 wxID_VXMLNODESOFTWAREBUILDPANELTGLANGBUILD, 
 wxID_VXMLNODESOFTWAREBUILDPANELTGLANGCOPY, 
 wxID_VXMLNODESOFTWAREBUILDPANELTGRECURSIVE, 
 wxID_VXMLNODESOFTWAREBUILDPANELTGSRC, 
 wxID_VXMLNODESOFTWAREBUILDPANELTGSRCBUILD, 
 wxID_VXMLNODESOFTWAREBUILDPANELTGSRCCLR, 
 wxID_VXMLNODESOFTWAREBUILDPANELTHDCTRLBUILD, 
 wxID_VXMLNODESOFTWAREBUILDPANELTRLSTHOST, 
] = [wx.NewId() for _init_ctrls in range(11)]

class vXmlNodeSoftwareBuildPanel(wx.Panel,vtXmlDomConsumer.vtXmlDomConsumer):
    VERBOSE=0
    def _init_coll_bxsToggle_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.tgRecursive, 1, border=4,
              flag=wx.EXPAND | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.cbRefresh, 1, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.EXPAND)

    def _init_coll_bxsSrc_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.tgSrc, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.tgSrcClr, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.tgSrcBuild, 1, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.trlstHost, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsToggle, 0, border=4, flag=wx.EXPAND | wx.TOP)
        parent.AddSizer(self.bxsSrc, 0, border=0, flag=0)
        parent.AddSizer(self.bxsLang, 0, border=0, flag=0)
        parent.AddWindow(self.thdCtrlBuild, 0, border=4,
              flag=wx.TOP | wx.EXPAND)

    def _init_coll_bxsLang_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.tgLang, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.tgLangBuild, 1, border=4,
              flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.tgLangCopy, 1, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_trlstHost_Columns(self, parent):
        # generated method, don't edit

        parent.AddColumn(text=u'name')
        parent.AddColumn(text=u'incl')
        parent.AddColumn(text=u'done')

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=4, rows=3, vgap=4)

        self.bxsToggle = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsSrc = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsLang = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsToggle_Items(self.bxsToggle)
        self._init_coll_bxsSrc_Items(self.bxsSrc)
        self._init_coll_bxsLang_Items(self.bxsLang)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODESOFTWAREBUILDPANEL,
              name=u'vXmlNodeSoftwareBuildPanel', parent=prnt, pos=wx.Point(5,
              8), size=wx.Size(312, 391),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 364))
        self.SetAutoLayout(True)

        self.trlstHost = wx.gizmos.TreeListCtrl(id=wxID_VXMLNODESOFTWAREBUILDPANELTRLSTHOST,
              name=u'trlstHost', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(304, 206), style=wx.TR_HAS_BUTTONS|wx.TR_HIDE_ROOT)
        self._init_coll_trlstHost_Columns(self.trlstHost)
        self.trlstHost.Bind(wx.EVT_TREE_ITEM_RIGHT_CLICK,
              self.OnTrlstHostRightDown)

        self.tgSrc = wx.lib.buttons.GenBitmapTextToggleButton(bitmap=vtArt.getBitmap(vtArt.Zip),
              id=wxID_VXMLNODESOFTWAREBUILDPANELTGSRC, label=u'source',
              name=u'tgSrc', parent=self, pos=wx.Point(0, 248), size=wx.Size(80,
              30), style=0)
        self.tgSrc.SetToggle(True)
        self.tgSrc.SetMinSize(wx.Size(-1, -1))

        self.tgSrcClr = wx.lib.buttons.GenBitmapTextToggleButton(bitmap=vtArt.getBitmap(vtArt.Erase),
              id=wxID_VXMLNODESOFTWAREBUILDPANELTGSRCCLR, label=u'clear',
              name=u'tgSrcClr', parent=self, pos=wx.Point(84, 248),
              size=wx.Size(76, 30), style=0)
        self.tgSrcClr.SetToggle(True)
        self.tgSrcClr.SetMinSize(wx.DefaultSize)

        self.tgSrcBuild = wx.lib.buttons.GenBitmapTextToggleButton(bitmap=vtArt.getBitmap(vtArt.MES),
              id=wxID_VXMLNODESOFTWAREBUILDPANELTGSRCBUILD, label=u'build',
              name=u'tgSrcBuild', parent=self, pos=wx.Point(160, 248),
              size=wx.Size(80, 30), style=0)
        self.tgSrcBuild.SetToggle(True)
        self.tgSrcBuild.SetMinSize(wx.Size(-1, -1))

        self.tgLang = wx.lib.buttons.GenBitmapTextToggleButton(bitmap=vtArt.getBitmap(vtArt.LangML),
              id=wxID_VXMLNODESOFTWAREBUILDPANELTGLANG, label=u'lang',
              name=u'tgLang', parent=self, pos=wx.Point(0, 282),
              size=wx.Size(80, 30), style=0)
        self.tgLang.SetToggle(False)
        self.tgLang.SetMinSize(wx.DefaultSize)

        self.tgLangBuild = wx.lib.buttons.GenBitmapTextToggleButton(bitmap=vtArt.getBitmap(vtArt.Build),
              id=wxID_VXMLNODESOFTWAREBUILDPANELTGLANGBUILD, label=u'lang',
              name=u'tgLangBuild', parent=self, pos=wx.Point(84, 282),
              size=wx.Size(76, 30), style=0)
        self.tgLangBuild.SetToggle(False)
        self.tgLangBuild.SetMinSize(wx.DefaultSize)

        self.tgLangCopy = wx.lib.buttons.GenBitmapTextToggleButton(bitmap=vtArt.getBitmap(vtArt.Save),
              id=wxID_VXMLNODESOFTWAREBUILDPANELTGLANGCOPY, label=u'copy',
              name=u'tgLangCopy', parent=self, pos=wx.Point(160, 282),
              size=wx.Size(80, 30), style=0)
        self.tgLangCopy.SetToggle(True)
        self.tgLangCopy.SetMinSize(wx.DefaultSize)

        self.tgRecursive = wx.lib.buttons.GenBitmapTextToggleButton(bitmap=vtArt.getBitmap(vtArt.Down),
              id=wxID_VXMLNODESOFTWAREBUILDPANELTGRECURSIVE, label=u'recursive',
              name=u'tgRecursive', parent=self, pos=wx.Point(4, 214),
              size=wx.Size(144, 30), style=0)
        self.tgRecursive.SetToggle(True)
        self.tgRecursive.SetMinSize(wx.DefaultSize)

        self.cbRefresh = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Synch),
              id=wxID_VXMLNODESOFTWAREBUILDPANELCBREFRESH, label=u'refresh',
              name=u'cbRefresh', parent=self, pos=wx.Point(156, 214),
              size=wx.Size(144, 30), style=0)
        self.cbRefresh.SetMinSize(wx.DefaultSize)
        self.cbRefresh.Bind(wx.EVT_BUTTON, self.OnCbRefreshButton,
              id=wxID_VXMLNODESOFTWAREBUILDPANELCBREFRESH)

        self.thdCtrlBuild = vidarc.tool.misc.vtmThreadCtrl.vtmThreadCtrl(id=wxID_VXMLNODESOFTWAREBUILDPANELTHDCTRLBUILD,
              name=u'thdCtrlBuild', parent=self, pos=wx.Point(0, 320),
              size=wx.Size(304, 44), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        self._init_ctrls(parent)
        
        vtXmlDomConsumer.vtXmlDomConsumer.__init__(self)
        
        self.thdBuild=vtThread(self,bPost=True)
        self.thdBuild.BindEvents(self.OnBuildProc,self.OnBuildFinished,self.OnBuildAborted)
        #self.cbGen.SetBitmapLabel(vtArt.getBitmap(vtArt.Build))
        #self.cbStop.SetBitmapLabel(vtArt.getBitmap(vtArt.Cancel))
        self.thdCtrlBuild.SetThread(self.thdBuild)
        self.thdCtrlBuild.SetFuncStart(self.Start)
        #self.thdCtrlBuild.BindEvents(self.OnBuildProc,self.OnBuildFinished,self.OnBuildAborted)
        
        self.trlstHost.SetColumnWidth(0,150)
        self.trlstHost.SetColumnWidth(1,30)
        self.trlstHost.SetColumnWidth(2,60)
        self.trlstHost.SetColumnAlignment(2,wx.gizmos.TL_ALIGN_RIGHT)
        self.imgDict={}
        self.imgLstTyp=wx.ImageList(16,16)
        self.imgDict['include']=self.imgLstTyp.Add(vtArt.getBitmap(vtArt.Apply))
        self.imgDict['exclude']=self.imgLstTyp.Add(vtArt.getBitmap(vtArt.Cancel))
        self.imgDict['Software']=self.imgLstTyp.Add(imgSW.getSoftwareBitmap())
        self.imgDict['Module']=self.imgLstTyp.Add(imgSW.getModuleBitmap())
        self.imgDict['Client']=self.imgLstTyp.Add(imgSW.getClientBitmap())
        self.imgDict['Host']=self.imgLstTyp.Add(vtArt.getBitmap(vtArt.Computer))
        self.trlstHost.SetImageList(self.imgLstTyp)
        
        self.bModified=False
        self.bAutoApply=False
        self.bBlock=False
        self.iSelCustomer=-1
        
        self.dCustomer={}
        self.dSnapShot={}
        self.dt=vtTime.vtDateTime(True)
        
        self.lstMarkObjs=[]
        
        self.Move(pos)
        self.SetSize(size)
    def OnBuildProc(self,evt):
        evt.Skip()
    def OnBuildFinished(self,evt):
        evt.Skip()
    def OnBuildAborted(self,evt):
        evt.Skip()
    def Lock(self,flag):
        if flag:
            #self.lstCustomer.Enable(False)
            #self.lstInfo.Enable(False)
            pass
        else:
            #self.lstCustomer.Enable(True)
            #self.lstInfo.Enable(True)
            pass
    def SetAutoApply(self,flag):
        self.bAutoApply=flag
    def __clearBlock__(self):
        self.bBlock=False
    def SetModified(self,state,obj=None):
        if state:
            if obj is None:
                for obj in self.lstMarkObjs:
                    f=obj.GetFont()
                    f.SetWeight(wx.FONTWEIGHT_BOLD)
                    obj.SetFont(f)
                    obj.Refresh()
            else:
                f=obj.GetFont()
                f.SetWeight(wx.FONTWEIGHT_BOLD)
                obj.SetFont(f)
                obj.Refresh()
        else:
            if obj is None:
                for obj in self.lstMarkObjs:
                    f=obj.GetFont()
                    f.SetWeight(wx.FONTWEIGHT_NORMAL)
                    obj.SetFont(f)
                    obj.Refresh()
                #self.viHum.__markModified__(False)
            else:
                f=obj.GetFont()
                f.SetWeight(wx.FONTWEIGHT_NORMAL)
                obj.SetFont(f)
                obj.Refresh()
        self.bModified=state    
    def GetModified(self):
        return self.bModified
    def __isModified__(self):
        if self.bModified:
            return True
        return False
    def Clear(self):
        self.bBlock=True
        self.SetModified(False)
        vtXmlDomConsumer.vtXmlDomConsumer.Clear(self)
        wx.CallAfter(self.__clearBlock__)
    def SetRegNode(self,o):
        pass
    def SetNetDocs(self,d):
        if d.has_key('vCustomer'):
            dd=d['vCustomer']
        pass
    def SetDoc(self,doc,bNet):
        vtXmlDomConsumer.vtXmlDomConsumer.SetDoc(self,doc)
        oBuild=self.doc.GetReg('SoftwareBuild')
        self.dCustomer=oBuild.GetCustomerDict(None)
        self.__showCustomer__()
    def SetNode(self,node):
        #if self.bModified==True:
        #    if self.bAutoApply:
        #        self.GetNode(self.node)
        #    else:
                # ask
        #        dlg=wx.MessageDialog(self,u'Do you to apply modified data?' ,
        #                    _(u'vLog Info'),
        #                    wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
        #        if dlg.ShowModal()==wx.ID_YES:
        #            self.GetNode(self.node)
        self.bBlock=True
        self.SetModified(False)
        self.Clear()
        vtXmlDomConsumer.vtXmlDomConsumer.SetNode(self,node)
        try:
            if self.doc is not None:
                oBuild=self.doc.GetReg('SoftwareBuild')
                nodeTmp=self.doc.getChild(node,'SoftwareBuild')
        except:
            vtLog.vtLngTB(self.GetName())
        wx.CallAfter(self.__clearBlock__)
    def GetNode(self,node=None):
        if node is None:
            node=self.node
        if node is not None:
            pass
        try:
            self.SetModified(False)
            if self.doc is not None:
                oBuild=self.doc.GetReg('SoftwareBuild')
                nodeTmp=self.doc.getChildForced(node,'SoftwareBuild')
                oBuild.SetCustomerDict(nodeTmp,self.dCustomer)
                self.doc.AlignNode(nodeTmp,iRec=4)
        except:
            vtLog.vtLngTB(self.GetName())
    def Apply(self,bDoApply=False):
        #self.SetModified(False)
        return False
    def Cancel(self):
        self.SetModified(False)
    def handleBuildStdOut(self,s,f):
        f.write(s)
    def handleBuildStdErr(self,s,f):
        f.write(s)
    def __cleanUp__(self,sDN,sOrigin):
        try:
            sCWD=os.getcwd()
            sBuildDN=os.path.join(sDN,'build')
            sDistDN=os.path.join(sDN,'dist')
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'buildDN:%s;distDNDN:%s'%(sBuildDN,sDistDN),sOrigin)
            shutil.rmtree(sBuildDN,True)
            shutil.rmtree(sDistDN,True)
            for root,dirs,files in os.walk(sDN):
                for sFN in files:
                    if sFN.endswith('.pyc'):
                        os.remove(os.path.join(sDN,sFN))
                    elif sFN.endswith('.po.new'):
                        os.remove(os.path.join(sDN,sFN))
                break
        except:
            vtLog.vtLngTB(sOrigin)
        os.chdir(sCWD)
    def __localeBuild__(self,sPyDir,sVidarcSrcDN,sSrcDN,fBuildOut,fBuildErr,sOrigin):
        try:
            sCWD=os.getcwd()
            d={}
            for root,dirs,files in os.walk(sSrcDN):
                for sFN in files:
                    iType=-1
                    if sFN.endswith('.fil'):
                        sDomain='.'.join(sFN.split('.')[:-1])
                        if sDomain=='app':
                            continue
                        iType=0
                    elif sFN.endswith('.pot'):
                        sDomain='.'.join(sFN.split('.')[:-1])
                        if sDomain=='messages':
                            continue
                        iType=1
                    elif sFN.endswith('.po'):
                        i=sFN.rfind('_')
                        if i>0:
                            sDomain=sFN[:i]
                            iType=2
                    if iType>=0:
                        if sDomain in d:
                            l=d[sDomain]
                        else:
                            l=[None,None,None]
                            d[sDomain]=l
                        if l[iType] is None:
                            l[iType]=sDomain
                break
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'sSrcDN:%s;d:%s'%(sSrcDN,vtLog.pformat(d)),sOrigin)
            os.chdir(sSrcDN)
            fBuildOut.write(sSrcDN+'\n')
            fBuildErr.write(sSrcDN+'\n')
            for sDomain,l in d.iteritems():
                fBuildOut.write(sDomain+'\n')
                fBuildErr.write(sDomain+'\n')
                for sArg in ['-p','-e -m']:
                    if l[0] is None:
                        sBuildCmd='%s//python %s/vidarc/tool/lang/mki18n.py %s --domain %s '%(sPyDir,
                                        sVidarcSrcDN,sArg,sDomain)
                    else:
                        sBuildCmd='%s//python %s/vidarc/tool/lang/mki18n.py %s --domain %s --fil %s'%(sPyDir,
                                        sVidarcSrcDN,sArg,sDomain,l[0])
                    prcBuild=vtProcess(sBuildCmd)
                    prcBuild.SetCB(1,None,self.handleBuildStdOut,fBuildOut)
                    prcBuild.SetCB(2,None,self.handleBuildStdErr,fBuildErr)
                    prcBuild.communicateLoop()
            fBuildOut.write(sSrcDN+' done\n')
            fBuildErr.write(sSrcDN+' done\n')
            pass
        except:
            vtLog.vtLngTB(sOrigin)
        os.chdir(sCWD)
    def __localeCopy__(self,sSrcDN,sDstDN,sOrigin):
        try:
            lMOs=[]
            for root,dirs,files in os.walk(os.path.join(sSrcDN,'locale')):
                    for f in files:
                        if f[-3:]=='.mo':
                            lMOs.append(os.path.join(root,f))
                    if 'CVS' in dirs:
                        dirs.remove('CVS')
            #if vtLog.vtLngIsLogged(vtLog.DEBUG):
            #    vtLog.vtLngCurWX(vtLog.DEBUG,'lDN:%s;lMOs:%s'%(lDN,lMOs),self)
            for fMO in lMOs:
                strs=fMO.split(os.path.sep)
                
                sTargetDN=os.path.join(sDstDN,strs[-4],strs[-3],strs[-2])
                if os.path.exists(sTargetDN)==False:
                    try:
                        os.makedirs(sTargetDN)
                    except:
                        pass
                shutil.copy(fMO,os.path.join(sTargetDN,strs[-1]))
                #if vtLog.vtLngIsLogged(vtLog.DEBUG):
                #    vtLog.vtLngCurWX(vtLog.DEBUG,'buildDN:%s;distDNDN:%s'%(sBuildDN,sDistDN),self)
        except:
            vtLog.vtLngTB(sOrigin)
        #self.lblFiles.SetValue(' '.join(['locale finished']))
    def __build__(self,dSoftwareDN,sLicDN,dCustBuild,
                    bSrcBuild,bSrcClr,bLgBuild,bLgCopy,sOrigin,thd,iCount):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCur(vtLog.DEBUG,'sLicDN:%s;flags:%d %d %d %d;dSoftwareDN:%s;dCustBuild:%s'%(
                    sLicDN,bSrcBuild,bSrcClr,bLgBuild,bLgCopy,
                    vtLog.pformat(dSoftwareDN),vtLog.pformat(dCustBuild)),sOrigin)
            bLogDbg=True
        else:
            bLogDbg=False
        try:
            iAct=0
            sCWD=os.getcwd()
            sPyDir=os.getenv('PythonDir')
            sVidarcSrcDN=os.getenv('vidarcSrc')
            sBuildFN=fnUtil.replaceSuspectChars('build_'+self.dt.GetDateSmallStr()+'_'+self.dt.GetTimeSmallStr()+'.log')
            sBuildErrFN=sBuildFN[:-3]+'err'
            try:
                os.makedirs(sLicDN)
            except:
                pass
            os.chdir(sLicDN)
            sBuildOutFN=os.path.join(sLicDN,sBuildFN)
            sBuildOutErrFN=os.path.join(sLicDN,sBuildErrFN)
            fBuildOut=open(sBuildOutFN,'ab')
            fBuildErr=open(sBuildOutErrFN,'ab')
            sStart=('+'*80)+'\n'
            sEnd=('-'*80)+'\n'
            keys=dCustBuild.keys()
            keys.sort()
            #sys.path.insert(0,sDN)
            #import __setup__
            if bSrcClr:
                for sSrcDN in dSoftwareDN.itervalues():
                    thd.Post('proc',_(u'cleanup:%s')%(sSrcDN),None)
                    iAct+=1
                    thd.Post('proc',iAct,iCount)
                    self.__cleanUp__(sSrcDN,sOrigin)
            if bLgBuild:
                for sSrcDN in dSoftwareDN.itervalues():
                    thd.Post('proc',_(u'build translations:%s')%(sSrcDN),None)
                    iAct+=1
                    thd.Post('proc',iAct,iCount)
                    self.__localeBuild__(sPyDir,sVidarcSrcDN,sSrcDN,fBuildOut,fBuildErr,sOrigin)
            if bLgCopy:
                for sSrcDN in dSoftwareDN.itervalues():
                    thd.Post('proc',_(u'copy translations:%s')%(sSrcDN),None)
                    iAct+=1
                    thd.Post('proc',iAct,iCount)
                    self.__localeCopy__(sSrcDN,sLicDN,sOrigin)
                
            
            for k in keys:
                if thd.Is2Stop():
                    break
                fBuildOut.write(sStart*3)
                fBuildErr.write(sStart*3)
                fBuildOut.write(k+'\n')
                fBuildErr.write(k+'\n')
                try:
                    if bLogDbg:
                        vtLog.vtLngCur(vtLog.DEBUG,'dCustBuild[%s]:%s'%(k,vtLog.pformat(dCustBuild[k])),sOrigin)
                    sDNCust=dCustBuild[k]['nr']
                    dHosts=dCustBuild[k]['hosts']
                    lSW=dCustBuild[k]['sw']
                    keysHost=dHosts.keys()
                    keysHost.sort()
                    for keyHost in keysHost:
                        # build license here
                        if thd.Is2Stop():
                            break
                        try:
                            thd.Post('proc',iAct,iCount)
                            sID=fnUtil.replaceSuspectChars(sDNCust)
                            sMID=fnUtil.replaceSuspectChars(keyHost)
                            sLicCustDN=os.path.join(sLicDN,sID,sMID)
                            if os.path.exists(sLicCustDN)==0:
                                try:
                                    os.makedirs(sLicCustDN)
                                except:
                                    pass
                            if bLogDbg:
                                vtLog.vtLngCur(vtLog.DEBUG,'licCustDN:%s'%(sLicCustDN),sOrigin)
                            sKeyFN=os.path.join(sLicCustDN,'pubkey.pub')
                            f=open(sKeyFN,'w')
                            f.write(dHosts[keyHost]['pub_key'])
                            f.close()
                            for sNameSW,iIdSW in lSW:
                                if thd.Is2Stop():
                                    break
                                iAct+=1
                                thd.Post('proc',iAct,iCount)
                                thd.Post('proc',u'%s,%s,%s'%(k,sMID,sNameSW),None)
                                if iIdSW in dSoftwareDN:
                                    sSrcDN=dSoftwareDN[iIdSW]
                                    if bLogDbg:
                                        vtLog.vtLngCur(vtLog.DEBUG,'sSrcDN:%s'%(sSrcDN),sOrigin)
                                else:
                                    continue
                                os.chdir(sSrcDN)
                                fBuildOut.write(sStart)
                                fBuildErr.write(sStart)
                                fBuildOut.write(sSrcDN+'\n')
                                fBuildErr.write(sSrcDN+'\n')
                                if bSrcBuild:
                                    try:
                                        os.chdir(sSrcDN)
                                        fBuildOut.write('%s:start %s\n'%(sNameSW,sSrcDN))
                                        fBuildErr.write('%s:start %s\n'%(sNameSW,sSrcDN))
                                        sBuildCmd='%s//python __setup__.py vidarc --licencedir=%s --mid=%s --pubkey=%s '%(sPyDir,
                                                sLicCustDN,keyHost,sKeyFN,)
                                        prcBuild=vtProcess(sBuildCmd)
                                        prcBuild.SetCB(1,None,self.handleBuildStdOut,fBuildOut)
                                        prcBuild.SetCB(2,None,self.handleBuildStdErr,fBuildErr)
                                        prcBuild.communicateLoop()
                                        fBuildOut.write('%s:done\n'%(sNameSW))
                                        fBuildErr.write('%s:done\n'%(sNameSW))
                                    except:
                                        vtLog.vtLngTB(sOrigin)
                                    os.chdir(sLicDN)
                                fBuildOut.write(sEnd)
                                fBuildErr.write(sEnd)
                        except:
                            vtLog.vtLngTB(sOrigin)
                except:
                    vtLog.vtLngTB(sOrigin)
                fBuildOut.write(sEnd*3)
                fBuildErr.write(sEnd*3)
                fBuildOut.write('\n'*3)
                fBuildErr.write('\n'*3)
            fBuildOut.close()
            fBuildErr.close()
        except:
            vtLog.vtLngTB(sOrigin)
        os.chdir(sCWD)
    def Start(self):
        try:
            self.dt.Now()
            if self.tgSrc.GetValue():
                bSrcBuild=self.tgSrcBuild.GetValue()
                bSrcClr=self.tgSrcClr.GetValue()
            else:
                bSrcBuild=False
                bSrcClr=False
            if self.tgLang.GetValue():
                bLgBuild=self.tgLangBuild.GetValue()
                bLgCopy=self.tgLangCopy.GetValue()
            else:
                bLgBuild=False
                bLgCopy=False
            
            dCustBuild={}
            def getCustSel(ti,iLevel,dCustomer,dCustBuild):
                triChild=self.trlstHost.GetFirstChild(ti)
                while triChild[0].IsOk():
                    sName=self.trlstHost.GetItemText(triChild[0],0)
                    sSel=self.trlstHost.GetItemText(triChild[0],1)
                    if len(sSel)>0:
                        dCustSub=dCustomer[sName]
                        if iLevel==0:
                            dd={}
                            dCustBuild[sName]={'id':dCustSub['id'],
                                    'nr':dCustSub['nr'],
                                    'hosts':dd}
                        elif iLevel==1:
                            dCustBuild[sName]=dCustSub
                            dd={}
                        else:
                            dCustBuild[sName]={}
                            dd={}
                        if self.trlstHost.ItemHasChildren(triChild[0]):
                            if iLevel==0:
                                d=dCustSub['hosts']
                            else:
                                d=dCustSub
                            getCustSel(triChild[0],iLevel+1,d,dd)
                    triChild=self.trlstHost.GetNextChild(ti,triChild[1])
            getCustSel(self.trlstHost.GetRootItem(),0,self.dCustomer,dCustBuild)
            if self.tgRecursive.GetValue():
                oSW=self.doc.GetRegByNode(self.node)
                lSW=oSW.GetSWRecLst(self.node)
            else:
                lSW=self.doc.getKeyNum(self.node)
            dSoftwareDN={}
            for id in lSW:
                n=self.doc.getNodeByIdNum(id)
                o=self.doc.GetRegByNode(n)
                sDN=o.GetDir(n)
                dSoftwareDN[id]=sDN
            if self.VERBOSE:
                vtLog.CallStack('')
                vtLog.pprint(dCustBuild)
            iCount=0
            keys=dCustBuild.keys()
            for k in keys:
                lBuildSW=[]
                if k in self.dCustomer:
                    lLicSW=self.dCustomer[k]['sw']
                    for sNameSW,id in lLicSW:
                        if id in lSW:
                            lBuildSW.append((sNameSW,id))
                lBuildSW.sort()
                iCount+=len(lBuildSW)*len(dCustBuild[k]['hosts'])
                
                dCustBuild[k]['sw']=lBuildSW
            if self.VERBOSE:
                vtLog.pprint(dCustBuild)
                vtLog.pprint(dSoftwareDN)
            nodeSetting=self.doc.getChild(self.doc.getRoot(),'settings')
            o=self.doc.GetReg('Settings')
            sLicDN=o.GetDir(nodeSetting)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'dCustomer:%s'%
                        (vtLog.pformat(dCustBuild)),self)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'dSoftwareDN:%s;licDN:%s'%
                        (vtLog.pformat(dSoftwareDN),sLicDN),self)
            iLen=len(dSoftwareDN)
            if bSrcClr:
                iCount+=iLen
            if bLgBuild:
                iCount+=iLen
            if bLgCopy:
                iCount+=iLen
            thd=self.thdCtrlBuild.GetThread()
            thd.Do(self.__build__,dSoftwareDN,sLicDN,dCustBuild,
                        bSrcBuild,bSrcClr,bLgBuild,bLgCopy,self.GetName(),
                        thd,iCount)
        except:
            vtLog.vtLngTB(self.GetName())

    def OnCbStopButton(self, event):
        self.thdBuild.Stop()
        event.Skip()

    def __showCustomer__(self):
        if self.VERBOSE:
            vtLog.CallStack('')
            print self.dCustomer
        self.trlstHost.DeleteAllItems()
        tip=self.trlstHost.AddRoot('customers')
        keys=self.dCustomer.keys()
        keys.sort()
        bSel=True#False
        imgClt=self.imgDict['Client']
        imgHost=self.imgDict['Host']
        for k in keys:
            ti=self.trlstHost.AppendItem(tip,k)
            self.trlstHost.SetItemImage(ti,imgClt,0,which=wx.TreeItemIcon_Normal)
            if bSel:
                self.trlstHost.SetItemText(ti,'X',1)
            
            d=self.dCustomer[k]['hosts']
            kkeys=d.keys()
            kkeys.sort()
            for kk in kkeys:
                tic=self.trlstHost.AppendItem(ti,kk)
                self.trlstHost.SetItemImage(tic,imgHost,0,which=wx.TreeItemIcon_Normal)
                if bSel:
                    self.trlstHost.SetItemText(tic,'X',1)
    def _modSel(self,tn,sSel):
        if len(sSel)>0:
            sSel=''
            #img=self.imgDict['exclude']
            bSel=False
        else:
            sSel='X'
            #img=self.imgDict['include']
            bSel=True
        def set(ti,sSel,bSel):
            triChild=self.trlstHost.GetFirstChild(ti)
            while triChild[0].IsOk():
                sPart=self.trlstHost.GetItemText(triChild[0],0)
                self.trlstHost.SetItemText(triChild[0],sSel,1)
                #self.trlstHost.SetItemImage(triChild[0],img,0,which=wx.TreeItemIcon_Normal)
                if self.trlstHost.ItemHasChildren(triChild[0]):
                    set(triChild[0],sSel,bSel)
                triChild=self.trlstHost.GetNextChild(ti,triChild[1])
        self.trlstHost.SetItemText(tn,sSel,1)
        set(tn,sSel,bSel)
    def OnTrlstHostRightDown(self, evt):
        try:
            tn=evt.GetItem()
            sSel=self.trlstHost.GetItemText(tn,1)
            self._modSel(tn,sSel)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbRefreshButton(self, event):
        event.Skip()
        try:
            oBuild=self.doc.GetReg('SoftwareBuild')
            self.dCustomer=oBuild.GetCustomerDict(None)
            self.__showCustomer__()
        except:
            vtLog.vtLngTB(self.GetName())
