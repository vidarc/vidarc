#----------------------------------------------------------------------------
# Name:         vXmlNodeSettings.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      2006
# CVS-ID:       $Id: vXmlNodeSettings.py,v 1.2 2006/08/29 10:06:30 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        from vXmlNodeSettingsPanel import *
        from vXmlNodeSettingsEditDialog import *
        from vXmlNodeSettingsAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeSettings(vtXmlNodeBase):
    NODE_ATTRS=[
            #('Tag',None,'tag',None),
            #('Name',None,'name',None),
        ]
    FUNCS_GET_SET_4_LST=[]
    def __init__(self,tagName='Settings'):
        global _
        _=vtLgBase.assignPluginLang('vSoftware')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'settings')
    # ---------------------------------------------------------
    # specific
    def GetDir(self,node):
        return self.Get(node,'dir')
    def GetName(self,node):
        return self.GetML(node,'name')
    def SetDir(self,node,val):
        self.Set(node,'dir',val)
    def SetName(self,node,val):
        self.SetML(node,'name',val)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_ATTR
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return False
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\xf3IDAT8\x8d\x8d\xd2_HSQ\x00\xc7\xf1\xef=\xbb\x1bn\x8d\xe5f6\xb0\
\x96\x14\x0c\x9b\x16$e\xd8\x10v\xe1J\xff\x1e\x0c\x0c\x0c\x12\x8a \x0c\r|\x8c\
\x1ez\x08\xf2\xa1\x1e\x0c\xf3%\xe8\xa9\x97\x1e\x16\xfd\xf1E\xb1\xd0Z`j\xa9\
\x15\xf4GJ\\E\\\xa2AVx\xbd\xb36=\xbd4\xb3\xe6\xdc~p^\x0e\x9c\x0f\xbf\x1f\x1c\
\x14aC\x116\x8e4]\x95\xb1\xa1\xcf\xb2\xeb\xda\x0f\xb9\xa1\xfc\x86\xcc\xdc\
\xe7;\x82?\x89\xde\x0c\xd1\xddy\x16u\xfe#\xa7\xdbJ\xf0\xac\x19\x93\x14\x10q\
\xa93*C[z\xa4"t\xe5\xce\xdd\x13L\x8c_G8=4\x1d\xedC.\x0e\xe6E\x84\xa6E8y\xc6\
\xcf\xf6\xca\x84T\x84\xae\xbc\xff\xf0\x06G:Mi H\xa9\xefU\xde\x06\xea\xd8\x88\
\x03Pii3x\xf2\xec\x82\xd4\xb4C\x18\t\x83\x05@U\x9d\xf9\'t]\xee\x87\xd9\xef\
\x98i\x93`e\x1dF\xc2\x80T\n\xd5\xee\xe6`\xe3W<\xde\xe7\xab\xceP\x14ac\x9d\
\xfb\x8a\xac\xaa\xb9O\xd9\xa6\x10v\x87\x93\x8d\x81j\xb6\x06~\x11{\xf8\x924E\
\xdc\x8a\xee\xc7\xfaY\xa3\xe4\x04\x00\\\xb6\x119\x97\xea\x03\xa0^\x9fdo\xc31\
&_O`I\x135)\xe8\x896b-\xec\xc9B\x96\x80\xe5\x91\x8b\x83\xb2~_7\r\x07Z\x98\
\x9az\xc0\x8c\xa52\xffm\x8e\xfe\xde\xe6,dE`9\xb2\xbb\xf60I\xf3-\x89\x19\x93/\
\t\x83\x81\xdeV\x14\xa1/!b\xc5\xd7\x80"te\xe0^;\xc3\xb1\xdb\xd8\xd5\xcd\xa8\
\xaeb\xfc\xbe\xf5\xd4\xe9\x17\xff\xf9\x1f9\x81\x0c\x12{\xd4\xce\xd3\xf1(\xc5\
E\x1e\xd2I\xf0y\xd7\xa2E\xfe"9\'\xfc?\xa76\xd2A\xd5\xae0X\x82\xa4\xf9\x89x|\
\x9a\xd1\xc7\xe7\x0b\x032\x88\x16\xee\xa0\xac\xa2\x1c9k\xa1\xda\xdd\x8c\xbe\
\x18Z}B\xd6\x9c\xe1s\xc4\xdfMS\xb1m\'\xd5;\xc2\xf8\xbd%\x857X\xde$\x18j\xc5\
\xa5:8\xde|\x8a\xdf1\xf3\xbc\xba\x1d\xd0\xc1"\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return vXmlNodeSettingsEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return vXmlNodeSettingsAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeSettingsPanel
        else:
            return None
