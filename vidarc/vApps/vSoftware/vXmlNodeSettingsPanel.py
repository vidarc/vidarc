#Boa:FramePanel:vXmlNodeSettingsPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeSettingsPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060629
# CVS-ID:       $Id: vXmlNodeSettingsPanel.py,v 1.2 2006/08/29 10:06:30 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.filebrowsebutton

import sys

from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.lang.vtLgBase as vtLgBase

[wxID_VXMLNODESETTINGSPANEL, wxID_VXMLNODESETTINGSPANELDBBDN, 
] = [wx.NewId() for _init_ctrls in range(2)]

class vXmlNodeSettingsPanel(wx.Panel,vtXmlNodePanel):
    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.dbbDN, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=0, rows=4, vgap=0)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt, id):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODESETTINGSPANEL,
              name=u'vXmlNodeSettingsPanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(312, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 180))
        self.SetAutoLayout(True)

        self.dbbDN = wx.lib.filebrowsebutton.DirBrowseButton(buttonText='...',
              dialogTitle=_('Choose a directory'),
              id=wxID_VXMLNODESETTINGSPANELDBBDN,
              labelText=_('Select a directory:'), parent=self,
              pos=wx.Point(4, 4), size=wx.Size(296, 29), startDirectory='.',
              style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        global _
        _=vtLgBase.assignPluginLang('vSoftware')
        self._init_ctrls(parent, id)
        
        vtXmlNodePanel.__init__(self,lWidgets=[])
        
        self.SetName(name)
        self.Move(pos)
        self.SetSize(size)
    def Clear(self):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.Clear(self)
        
        # add code here
        self.dbbDN.SetValue('')
        self.ClrBlockDelayed()
    def SetNetDocs(self,d):
        if d.has_key('vHum'):
            dd=d['vHum']
            self.docHum=dd['doc']
        # add code here
        
    def SetDoc(self,doc,bNet=False):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.SetDoc(self,doc,bNet)
        
        # add code here
        
    def SetNode(self,node):
        try:
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            
            # add code here
            sDir=self.objRegNode.GetDir(self.node)
            self.dbbDN.SetValue(sDir)
            #oTrans=self.doc.GetRegisteredNode(self.objRegNode.GetTagNameTransition())
            
        except:
            vtLog.vtLngTB(self.GetName())
        
    def GetNode(self,node=None):
        try:
            node=self.GetNodeStart(node)
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
                print node
            if node is None:
                return
            # add code here
            sDir=self.dbbDN.GetValue()
            self.objRegNode.SetDir(node,sDir)
            self.GetNodeFin(node)
        except:
            vtLog.vtLngTB(self.GetName())
    def Close(self):
        # add code here
        pass
    def Cancel(self):
        # add code here
        pass
    def Lock(self,flag):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if flag:
            # add code here
            self.dbbDN.Enable(False)
        else:
            # add code here
            self.dbbDN.Enable(True)
