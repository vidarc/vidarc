#Boa:FramePanel:vXmlNodeSoftwareCustomerPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeSoftwareBuildPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060629
# CVS-ID:       $Id: vXmlNodeSoftwareCustomerPanel.py,v 1.6 2008/03/29 23:42:48 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputTreeInternal
import vidarc.tool.input.vtInputText
import vidarc.vApps.vCustomer.vXmlCustomerInputTree
import vidarc.tool.input.vtInputTree
import vidarc.tool.art.vtThrobber
import wx.lib.buttons
import wx.lib.filebrowsebutton

from vidarc.tool.input.vtInputModifyFeedBack import vtInputModifyFeedBack

from vidarc.tool.net.vNetXmlWxGui import *
import vidarc.tool.xml.vtXmlDom as vtXmlDom
import vidarc.tool.xml.vtXmlDomConsumer as vtXmlDomConsumer
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.time.vtTime as vtTime
from vidarc.tool.vtThread import vtThread
import vidarc.install.identify as vIdentify
import vidarc.tool.InOut.fnUtil as fnUtil

import sys,os,os.path,shutil
import threading,thread,time

[wxID_VXMLNODESOFTWARECUSTOMERPANEL, wxID_VXMLNODESOFTWARECUSTOMERPANELCBADD, 
 wxID_VXMLNODESOFTWARECUSTOMERPANELCBCLEANUP, 
 wxID_VXMLNODESOFTWARECUSTOMERPANELCBDEL, 
 wxID_VXMLNODESOFTWARECUSTOMERPANELCBGEN, 
 wxID_VXMLNODESOFTWARECUSTOMERPANELCBLOCALE, 
 wxID_VXMLNODESOFTWARECUSTOMERPANELCBSTOP, 
 wxID_VXMLNODESOFTWARECUSTOMERPANELLBLCUSTOMER, 
 wxID_VXMLNODESOFTWARECUSTOMERPANELLBLFILES, 
 wxID_VXMLNODESOFTWARECUSTOMERPANELLBLSW, 
 wxID_VXMLNODESOFTWARECUSTOMERPANELLSTSW, 
 wxID_VXMLNODESOFTWARECUSTOMERPANELTHRPROC, 
 wxID_VXMLNODESOFTWARECUSTOMERPANELVICUSTOMER, 
 wxID_VXMLNODESOFTWARECUSTOMERPANELVINAME, 
 wxID_VXMLNODESOFTWARECUSTOMERPANELVISW, 
] = [wx.NewId() for _init_ctrls in range(15)]

class vXmlNodeSoftwareCustomerPanel(wx.Panel,vtXmlDomConsumer.vtXmlDomConsumer,vtInputModifyFeedBack):
    def _init_coll_bxsSW_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblSW, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.viSW, 3, border=0, flag=wx.EXPAND)

    def _init_coll_bxsSWlst_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstSW, 1, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsSWbt, 0, border=4, flag=wx.RIGHT | wx.LEFT)

    def _init_coll_bxsSnapShot_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblCustomer, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.viCustomer, 2, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.viName, 1, border=4, flag=wx.LEFT | wx.EXPAND)

    def _init_coll_bxsSWbt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbAdd, 0, border=0, flag=0)
        parent.AddWindow(self.cbDel, 0, border=4, flag=wx.TOP)
        parent.AddSpacer(wx.Size(8,16), border=0, flag=0)
        parent.AddWindow(self.cbCleanUp, 0, border=0, flag=0)
        parent.AddWindow(self.cbLocale, 0, border=4, flag=wx.TOP)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsSnapShot, 1, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsSW, 0, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsSWlst, 0, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 16), border=0, flag=0)
        parent.AddSizer(self.bxsButton, 1, border=4, flag=wx.TOP | wx.EXPAND)

    def _init_coll_bxsButton_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.thrProc, 0, border=0, flag=0)
        parent.AddWindow(self.cbGen, 0, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblFiles, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbStop, 0, border=4, flag=wx.LEFT)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_lstSW_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading=u'software', width=250)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT, heading=u'id',
              width=80)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=0, rows=5, vgap=0)

        self.bxsButton = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsSnapShot = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsSW = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsSWlst = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsSWbt = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsButton_Items(self.bxsButton)
        self._init_coll_bxsSnapShot_Items(self.bxsSnapShot)
        self._init_coll_bxsSW_Items(self.bxsSW)
        self._init_coll_bxsSWlst_Items(self.bxsSWlst)
        self._init_coll_bxsSWbt_Items(self.bxsSWbt)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODESOFTWARECUSTOMERPANEL,
              name=u'vXmlNodeSoftwareCustomerPanel', parent=prnt,
              pos=wx.Point(5, 8), size=wx.Size(312, 391),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 364))
        self.SetAutoLayout(True)

        self.cbGen = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODESOFTWARECUSTOMERPANELCBGEN,
              bitmap=wx.EmptyBitmap(16 , 16), name=u'cbGen', parent=self,
              pos=wx.Point(19, 333), size=wx.Size(31, 31), style=0)
        self.cbGen.SetMinSize(wx.Size(31, 31))
        self.cbGen.Bind(wx.EVT_BUTTON, self.OnCbGenButton,
              id=wxID_VXMLNODESOFTWARECUSTOMERPANELCBGEN)

        self.cbStop = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODESOFTWARECUSTOMERPANELCBSTOP,
              bitmap=wx.EmptyBitmap(16 , 16), name=u'cbStop', parent=self,
              pos=wx.Point(273, 333), size=wx.Size(31, 30), style=0)
        self.cbStop.Bind(wx.EVT_BUTTON, self.OnCbStopButton,
              id=wxID_VXMLNODESOFTWARECUSTOMERPANELCBSTOP)

        self.thrProc = vidarc.tool.art.vtThrobber.vtThrobberDelay(id=wxID_VXMLNODESOFTWARECUSTOMERPANELTHRPROC,
              name=u'thrProc', parent=self, pos=wx.Point(0, 333),
              size=wx.Size(15, 15), style=0)

        self.lblCustomer = wx.StaticText(id=wxID_VXMLNODESOFTWARECUSTOMERPANELLBLCUSTOMER,
              label=u'customer', name=u'lblCustomer', parent=self,
              pos=wx.Point(0, 4), size=wx.Size(72, 24), style=wx.ALIGN_RIGHT)
        self.lblCustomer.SetMinSize(wx.Size(-1, -1))

        self.viCustomer = vidarc.vApps.vCustomer.vXmlCustomerInputTree.vXmlCustomerInputTree(id=wxID_VXMLNODESOFTWARECUSTOMERPANELVICUSTOMER,
              name=u'viCustomer', parent=self, pos=wx.Point(76, 4),
              size=wx.Size(152, 24), style=0)

        self.viName = vidarc.tool.input.vtInputText.vtInputText(id=wxID_VXMLNODESOFTWARECUSTOMERPANELVINAME,
              name=u'viName', parent=self, pos=wx.Point(232, 4),
              size=wx.Size(72, 24), style=0)

        self.lblSW = wx.StaticText(id=wxID_VXMLNODESOFTWARECUSTOMERPANELLBLSW,
              label=u'SW', name=u'lblSW', parent=self, pos=wx.Point(0, 32),
              size=wx.Size(72, 24), style=wx.ALIGN_RIGHT)
        self.lblSW.SetMinSize(wx.Size(-1, -1))

        self.viSW = vidarc.tool.input.vtInputTreeInternal.vtInputTreeInternal(id=wxID_VXMLNODESOFTWARECUSTOMERPANELVISW,
              name=u'viSW', parent=self, pos=wx.Point(76, 32), size=wx.Size(228,
              24), style=0)

        self.lstSW = wx.ListCtrl(id=wxID_VXMLNODESOFTWARECUSTOMERPANELLSTSW,
              name=u'lstSW', parent=self, pos=wx.Point(0, 60), size=wx.Size(265,
              253), style=wx.LC_REPORT)
        self.lstSW.SetMinSize(wx.Size(-1, -1))
        self._init_coll_lstSW_Columns(self.lstSW)

        self.cbAdd = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODESOFTWARECUSTOMERPANELCBADD,
              bitmap=vtArt.getBitmap(vtArt.Add), name=u'cbAdd', parent=self,
              pos=wx.Point(269, 60), size=wx.Size(31, 30), style=0)
        self.cbAdd.Bind(wx.EVT_BUTTON, self.OnCbAddButton,
              id=wxID_VXMLNODESOFTWARECUSTOMERPANELCBADD)

        self.cbDel = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODESOFTWARECUSTOMERPANELCBDEL,
              bitmap=vtArt.getBitmap(vtArt.Del), name=u'cbDel', parent=self,
              pos=wx.Point(269, 94), size=wx.Size(31, 30), style=0)
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VXMLNODESOFTWARECUSTOMERPANELCBDEL)

        self.lblFiles = wx.TextCtrl(id=wxID_VXMLNODESOFTWARECUSTOMERPANELLBLFILES,
              name=u'lblFiles', parent=self, pos=wx.Point(54, 333),
              size=wx.Size(215, 31), style=wx.TE_READONLY, value=u'')

        self.cbCleanUp = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODESOFTWARECUSTOMERPANELCBCLEANUP,
              bitmap=vtArt.getBitmap(vtArt.Erase), name=u'cbCleanUp',
              parent=self, pos=wx.Point(269, 140), size=wx.Size(31, 30),
              style=0)
        self.cbCleanUp.Bind(wx.EVT_BUTTON, self.OnCbCleanUpButton,
              id=wxID_VXMLNODESOFTWARECUSTOMERPANELCBCLEANUP)

        self.cbLocale = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODESOFTWARECUSTOMERPANELCBLOCALE,
              bitmap=vtArt.getBitmap(vtArt.LangML), name=u'cbLocale', parent=self,
              pos=wx.Point(269, 174), size=wx.Size(31, 30), style=0)
        self.cbLocale.Bind(wx.EVT_BUTTON, self.OnCbLocaleButton,
              id=wxID_VXMLNODESOFTWARECUSTOMERPANELCBLOCALE)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        self._init_ctrls(parent)
        
        vtXmlDomConsumer.vtXmlDomConsumer.__init__(self)
        vtInputModifyFeedBack.__init__(self,lWidgets=[self.lstSW])
        
        self.thdBuild=vtThread(self,bPost=False)
        self.cbGen.SetBitmapLabel(vtArt.getBitmap(vtArt.Build))
        self.cbStop.SetBitmapLabel(vtArt.getBitmap(vtArt.Cancel))
        
        self.bModified=False
        self.bAutoApply=False
        self.bBlock=False
        self.viName.SetTagName('nameSuff')
        self.viSW.SetTreeFunc(None,self.__showSWint__)
        self.viSW.SetTagNames('sw','name')
        self.viSW.AddTreeCall('init','SetNodeInfos',['name'])
        self.viSW.AddTreeCall('init','SetGrouping',[],[('name','')])
        self.viSW.SetEnableMark(False)
        #self.viSW.SetTagNames2Base(['root','cfg'])
        
        self.dt=vtTime.vtDateTime()
        
        self.lSW=[]
        self.dt=vtTime.vtDateTime(True)
        
        self.lstMarkObjs=[]
        
        self.Move(pos)
        self.SetSize(size)
    def __getSWint__(self,node,l):
        if node is not None:
            if self.doc.IsNodeKeyValid(node):
                l.append(self.doc.getNodeText(node,'name'))
                self.__getSWint__(self.doc.getParent(node),l)
    def __showSWint__(self,doc,node,lang):
        #vtLog.CallStack('')
        l=[]
        self.__getSWint__(node,l)
        l.reverse()
        return '.'.join(l)
    def Lock(self,flag):
        if flag:
            self.viCustomer.Enable(False)
        else:
            self.viCustomer.Enable(True)
    def SetAutoApply(self,flag):
        self.bAutoApply=flag
    def __clearBlock__(self):
        self.bBlock=False
    def SetModifiedOld(self,state,obj=None):
        if state:
            if obj is None:
                for obj in self.lstMarkObjs:
                    f=obj.GetFont()
                    f.SetWeight(wx.FONTWEIGHT_BOLD)
                    obj.SetFont(f)
                    obj.Refresh()
            else:
                f=obj.GetFont()
                f.SetWeight(wx.FONTWEIGHT_BOLD)
                obj.SetFont(f)
                obj.Refresh()
        else:
            if obj is None:
                for obj in self.lstMarkObjs:
                    f=obj.GetFont()
                    f.SetWeight(wx.FONTWEIGHT_NORMAL)
                    obj.SetFont(f)
                    obj.Refresh()
                #self.viHum.__markModified__(False)
            else:
                f=obj.GetFont()
                f.SetWeight(wx.FONTWEIGHT_NORMAL)
                obj.SetFont(f)
                obj.Refresh()
        self.bModified=state    
    def GetModifiedOld(self):
        return self.bModified
    def __isModified__(self):
        return self.GetModified()
        if self.bModified:
            return True
        return False
    def Clear(self):
        self.bBlock=True
        self.SetModified(False)
        vtXmlDomConsumer.vtXmlDomConsumer.Clear(self)
        self.lstSW.DeleteAllItems()
        self.viCustomer.Clear()
        self.viName.Clear()
        wx.CallAfter(self.__clearBlock__)
    def SetRegNode(self,o):
        self.objRegNode=o
    def SetNetDocs(self,d):
        if d.has_key('vCustomer'):
            dd=d['vCustomer']
            self.viCustomer.SetDocTree(dd['doc'],dd['bNet'])
    def SetDoc(self,doc,bNet):
        vtXmlDomConsumer.vtXmlDomConsumer.SetDoc(self,doc)
        self.viCustomer.SetDoc(doc)
        self.viName.SetDoc(doc)
        self.viSW.SetDoc(doc)
    def SetNode(self,node):
        #if self.bModified==True:
        #    if self.bAutoApply:
        #        self.GetNode(self.node)
        #    else:
                # ask
        #        dlg=wx.MessageDialog(self,u'Do you to apply modified data?' ,
        #                    _(u'vLog Info'),
        #                    wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
        #        if dlg.ShowModal()==wx.ID_YES:
        #            self.GetNode(self.node)
        self.bBlock=True
        self.SetModified(False)
        self.Clear()
        vtXmlDomConsumer.vtXmlDomConsumer.SetNode(self,node)
        try:
            self.viName.SetNode(self.node)
            self.viCustomer.SetNode(self.node)
            self.lSW=self.objRegNode.GetSWLst(self.node)
            self.__showSW__()
        except:
            vtLog.vtLngTB(self.GetName())
        wx.CallAfter(self.__clearBlock__)
    def GetNode(self,node=None):
        if node is None:
            node=self.node
        if node is not None:
            pass
        try:
            self.viName.GetNode(node)
            self.viCustomer.GetNode(node)
            self.objRegNode.SetSWLst(node,self.lSW)
            self.doc.AlignNode(node)
            self.SetModified(False)
        except:
            vtLog.vtLngTB(self.GetName())
    def Apply(self,bDoApply=False):
        #self.SetModified(False)
        return False
    def Cancel(self):
        self.SetModified(False)
    
    def __build__(self,lDN,sLicDN,dHosts):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        try:
            self.dt.Now()
            sBuildFN=fnUtil.replaceSuspectChars('build_'+self.dt.GetDateSmallStr()+'_'+self.dt.GetTimeSmallStr()+'.log')
            sBuildErrFN=sBuildFN[:-3]+'err'
            sCWD=os.getcwd()
            keys=dHosts.keys()
            keys.sort()
            #sys.path.insert(0,sDN)
            #import __setup__
            
            for k in keys:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'host:%s'%(vtLog.pformat(dHosts[k])),self)
                tup=dHosts[k]
                sDNCust=tup[0]
                sHost=tup[2]
                #dHosts=dCustomer[k]['hosts']
                # build license here
                try:
                    sHostDN=fnUtil.replaceSuspectChars(sHost)
                    sID=fnUtil.replaceSuspectChars(sDNCust)
                    sMID=fnUtil.replaceSuspectChars(k)
                    sLicCustDN=os.path.join(sLicDN,sID,'_'.join([sHostDN,sMID]))
                    if os.path.exists(sLicCustDN)==False:
                        try:
                            os.makedirs(sLicCustDN)
                        except:
                            pass
                    sBuildOutFN=os.path.join(sLicDN,sID,sBuildFN)
                    sBuildOutErrFN=os.path.join(sLicDN,sID,sBuildErrFN)
                    #dHosts[keyHost]
                    sKeyFN=os.path.join(sLicCustDN,'pubkey.pub')
                    f=open(sKeyFN,'w')
                    f.write(tup[1])
                    f.close()
                    for sDN in lDN:
                        if vtLog.vtLngIsLogged(vtLog.DEBUG):
                            vtLog.vtLngCurWX(vtLog.DEBUG,'dn:%s;licCustDN:%s'%(sDN,sLicCustDN),self)
                        os.chdir(sDN)
                        fTmp=open(sBuildOutFN,'a')
                        fTmp.write(sDN+'\n')
                        fTmp.close()
                        fTmp=open(sBuildOutErrFN,'a')
                        fTmp.write(sDN+'\n')
                        fTmp.close()
                        self.lblFiles.SetValue(' '.join([sHost,'build',sDN]))
                        #__setup__.setup_args['options']['vidarc']['licencedir']=sLicCustDN
                        #__setup__.setup(**__setup__.setup_args)
                        os.system('python __setup__.py vidarc --licencedir=%s --mid=%s --pubkey=%s >> %s 2>> %s'%(sLicCustDN,
                                k,sKeyFN,sBuildOutFN,sBuildOutErrFN))
                        #self.__doBuild__(sDN,sLicDN,sDNCust,keyHost,dHosts[keyHost])
                except:
                    vtLog.vtLngTB(self.GetName())
        except:
            vtLog.vtLngTB(self.GetName())
        #sys.path=sys.path[1:]
        self.lblFiles.SetValue(' '.join(['build finished']))
        
        os.chdir(sCWD)
    def __getHosts__(self,node,d):
        sTagName=self.doc.getTagName(node)
        try:
            if sTagName=='hostLicense':
                o=self.doc.GetReg('hostLicense')
                dInfos=o.GetDict(node)
                dMachine=dInfos['machine']
                mid=dMachine['mid']
                sName=o.GetName(node)
                if mid not in d:
                    #if os.exists
                    d[mid]=(dInfos['customerID'],dMachine['pub_key'],sName)
        except:
            vtLog.vtLngTB(self.GetName())
        return 0
    def OnCbGenButton(self, event):
        try:
            #if self.tgMaster.GetValue():
            #    pass
            if self.node is None:
                return
            dHosts={}
            self.doc.procChildsExt(self.node,self.__getHosts__,dHosts)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'dHosts:%s'%(vtLog.pformat(dHosts)),self)
            nodeSetting=self.doc.getChild(self.doc.getRoot(),'settings')
            o=self.doc.GetReg('Settings')
            sLicDN=o.GetDir(nodeSetting)
            lSW=[]
            iCount=self.lstSW.GetItemCount()
            for i in xrange(iCount):
                if self.lstSW.GetItemState(i,wx.LIST_STATE_SELECTED)!=0:
                    lSW.append(self.lSW[i])
            if len(lSW)==0:
                lDN=self.objRegNode.getSWDirLst(self.lSW)
            else:
                lDN=self.objRegNode.getSWDirLst(lSW)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'lDN:%s;licDN:%s'%(vtLog.pformat(lDN),sLicDN),self)
            #self.thdBuild.Do(self.__build__,lDN,sLicDN,dHosts)
            oHostLic=self.doc.GetReg('hostLicense')
            self.thdBuild.Do(oHostLic.BuildLic,self,lDN,sLicDN,dHosts)
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()

    def OnCbStopButton(self, event):
        self.thdBuild.Stop()
        event.Skip()
    def __showSW__(self):
        self.lstSW.DeleteAllItems()
        self.lSW.sort()
        for s,id in self.lSW:
            idx=self.lstSW.InsertStringItem(sys.maxint,s)
            self.lstSW.SetStringItem(idx,1,'%08d'%id)
    def OnCbAddButton(self, event):
        event.Skip()
        s=self.viSW.GetValue()
        fid=self.viSW.GetID()
        if len(s)==0:
            return 
        try:
            iID=long(fid)
        except:
            return
        if s not in self.lSW:
            self.lSW.append((s,iID))
            self.SetModified(True,self.lstSW)
            self.__showSW__()

    def OnCbDelButton(self, event):
        event.Skip()
        iCount=self.lstSW.GetItemCount()
        for idx in xrange(iCount-1,-1,-1):
            if self.lstSW.GetItemState(idx,wx.LIST_STATE_SELECTED)!=0:
                del self.lSW[idx]
                self.SetModified(True,self.lstSW)
        self.__showSW__()
    def __cleanUp__(self,lDN):
        try:
            sCWD=os.getcwd()
            for sDN in lDN:
                self.lblFiles.SetValue(''.join(['clean DN:',sDN]))
                sBuildDN=os.path.join(sDN,'build')
                sDistDN=os.path.join(sDN,'dist')
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'buildDN:%s;distDNDN:%s'%(sBuildDN,sDistDN),self)
                shutil.rmtree(sBuildDN,True)
                shutil.rmtree(sDistDN,True)
                for root,dirs,files in os.walk(sDN):
                    for sFN in files:
                        if sFN.endswith('.pyc'):
                            os.remove(os.path.join(sDN,sFN))
                    break
        except:
            vtLog.vtLngTB(self.GetName())
        self.lblFiles.SetValue(' '.join(['clean finished']))
        os.chdir(sCWD)
    def OnCbCleanUpButton(self, event):
        event.Skip()
        try:
            lSW=[]
            iCount=self.lstSW.GetItemCount()
            for i in xrange(iCount):
                if self.lstSW.GetItemState(i,wx.LIST_STATE_SELECTED)!=0:
                    lSW.append(self.lSW[i])
            if len(lSW)==0:
                lDN=self.objRegNode.getSWDirLst(self.lSW)
            else:
                lDN=self.objRegNode.getSWDirLst(lSW)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'lDN:%s;'%(vtLog.pformat(lDN)),self)
            self.thdBuild.Do(self.__cleanUp__,lDN)
        except:
            vtLog.vtLngTB(self.GetName())

    def __locale__(self,lDN,sLicDN,lCustDN):
        try:
            lMOs=[]
            for sDN in lDN:
                for root,dirs,files in os.walk(sDN):
                    for f in files:
                        if f[-3:]=='.mo':
                            lMOs.append(os.path.join(root,f))
                    if 'CVS' in dirs:
                        dirs.remove('CVS')
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'lDN:%s;lMOs:%s'%(lDN,lMOs),self)
            for fMO in lMOs:
                strs=fMO.split(os.path.sep)
                
                for sCustDN in lCustDN:
                    self.lblFiles.SetValue(''.join(['locale',' ','FN:',fMO,' ','DN:',sCustDN]))
                    sTargetDN=os.path.join(sLicDN,sCustDN,strs[-4],strs[-3],strs[-2])
                    try:
                        os.makedirs(sTargetDN)
                    except:
                        pass
                    shutil.copy(fMO,os.path.join(sTargetDN,strs[-1]))
                    #if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    #    vtLog.vtLngCurWX(vtLog.DEBUG,'buildDN:%s;distDNDN:%s'%(sBuildDN,sDistDN),self)
                    pass
        except:
            vtLog.vtLngTB(self.GetName())
        self.lblFiles.SetValue(' '.join(['locale finished']))
    def OnCbLocaleButton(self, event):
        event.Skip()
        try:
            dHosts={}
            self.doc.procChildsExt(self.node,self.__getHosts__,dHosts)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'dHosts:%s'%(vtLog.pformat(dHosts)),self)
            nodeSetting=self.doc.getChild(self.doc.getRoot(),'settings')
            o=self.doc.GetReg('Settings')
            sLicDN=o.GetDir(nodeSetting)
            lSW=[]
            iCount=self.lstSW.GetItemCount()
            for i in xrange(iCount):
                if self.lstSW.GetItemState(i,wx.LIST_STATE_SELECTED)!=0:
                    lSW.append(self.lSW[i])
            if len(lSW)==0:
                lDN=self.objRegNode.getSWDirLst(self.lSW)
            else:
                lDN=self.objRegNode.getSWDirLst(lSW)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'lDN:%s;licDN:%s'%(vtLog.pformat(lDN),sLicDN),self)
            lCustDN=[]
            for tup in dHosts.values():
                if tup[1] not in lCustDN:
                    sCustDN=fnUtil.replaceSuspectChars(tup[0])
                    lCustDN.append(sCustDN)
            self.thdBuild.Do(self.__locale__,lDN,sLicDN,lCustDN)
        except:
            vtLog.vtLngTB(self.GetName())
