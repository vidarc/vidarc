#----------------------------------------------------------------------------
# Name:         vXmlSoftware.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060220
# CVS-ID:       $Id: vXmlSoftware.py,v 1.7 2008/03/29 23:42:48 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vidarc.vApps.vSoftware.vXmlNodeSoftwareRoot import vXmlNodeSoftwareRoot
from vidarc.vApps.vSoftware.vXmlNodeSoftware import vXmlNodeSoftware
from vidarc.vApps.vSoftware.vXmlNodeSettings import vXmlNodeSettings
from vidarc.vApps.vSoftware.vXmlNodeSoftwareLicenseHost import vXmlNodeSoftwareLicenseHost
from vidarc.vApps.vSoftware.vXmlNodeSoftwareBuild import vXmlNodeSoftwareBuild
from vidarc.vApps.vSoftware.vXmlNodeSoftwareCustomer import vXmlNodeSoftwareCustomer

#from vidarc.vApps.vCustomer.vXmlNodeArea import vXmlNodeArea
#from vidarc.tool.xml.vtXmlNodeAddr import vtXmlNodeAddr
from vidarc.tool.xml.vtXmlNodeLog import vtXmlNodeLog
from vidarc.tool.xml.vtXmlNodeCfg import vtXmlNodeCfg
import traceback

def getPluginData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x02CIDAT8\x8d\x8d\x93OH\x93q\x1c\xc6?\xef\xf6\xaa\xaf\x9b\xe6\xdf-\x9d\
\xff\x9a6\'\xa4`\x874J(\x04S\x0f\xe1!\xf1\xe4A:u\xa8\xcc\x8a\xf0\x10]"\xb0\
\xe8\x14AA\xe4A((\xed\x8f\x96x\x08=\xa4tp\xd5\xd4\xe9k\xb9i\xbdj\xe6BkS\xa3\
\xa6\xbe\xaeC8\xda\xf6\n\xfdN?\xbe<\xbf\xef\xefy\x1e\x9eG\x10tz"OI[}0j\x08\
\xb8Z\xbb\x84\xc8\x99\xee\x7f\x81Z3\x00a\x9bA\xe4\xaf\r\xbfjp:\'\xa9o\xa8@\
\xdd\xb2\x10\'\xe5qu\xe6t\xd4BM\x06\xf7\x8e\xdc\xc2\xe7\x93hl\xb414\xf4\x8a\
\xc1\xc17<z\xd0\xcd5{\x13-\xe9\xe7\xc2\xb0\xa2\xd6\x82\x8e\x0e\x99\x82|/\xe8\
2(\xddo\xc7lJ\xc1\xbb\xa8\x10\x08,\xb1\x15\x109\xb1\x96\x8ek\xdb\x83H\xea\
\x8f\x8f_ay\xf9\x13\xf9\xb9\xe9\x98b\x968\xb8/\x13\xa3\x94\xc8\xc6\xa6Je\xe5\
^\xea\xea`\xd8\xe1\x08\xc9\x16#\xf5OMA\xa1M!\xe8\xffMn\xaa\x81x\xb3\x85\xc4\
\xa48>\xcf\xc6\xe2\x18\x9e AZd|\xa2\x8a\x926k0\xca\x83\xeb\xc5\xb7io\xef\xc5\
lN%#I\x87\xe2\x1e\xc5#\x7f\xc0\x14\x1f\xc4b\xcaCQ\xd6q\x8e\xcec0\xd8Co\xc2\
\x16\xb8\xddoi>k\xe5Xu1_\xfc"\xc9\xc68\xf2\xc5\x05<S\xd38]}\x14\xec\xf1\xb2\
\xba\n\x9d]\x15\xd1&\x96}l\xc2X>\xc3\xd1\xca\x02dy\x8du)\x87\xe9\x8d\x18\xbc\
\xbeX\xd6\xc4\x04v%[\xd0\xeb\x16\x18\x1b\xff\xc1\xcc\xdc\x10\xd8\xfea\xe0j\
\xed\x12\xbey\xfbII\xf6\xe2\xf7\xcd\x91\x93\xfd\x93C\x87%ti\x99x\xfc\xeb\xa8\
\xc1\xf7\xe4f\x05\xf0\xf9\x03\xc8\x93\x93tw?\x0f\xe5 \x14\xa4\x81\xfe\xaf\
\xc1\xde\x97\xef\x10\xf5N\x8al[\x94\x95\xabll\xc6\xb0\xb2\xb2\x8a\xbd\xd0\
\x881A\xe2\xd9\x93\xd7\xb4\\\x9a\xa5\xaf\xaf\x93\x03eEB\x98\x84f\xc7\x19\xd8\
\r7K\xef\xa2(2/z=x\xdc\x0b\xd4T\x7f\'+\xdb\x80\xaa\xae\xf0\xb4G\xa5\xb6\xf6\
\x06\'\x07.\xef\x1c\xa4\x8b#\xa7\xfe^\xf4\xf0\xf0\xfc\x1d\xe4\x89y\xaa\xaa\
\x07\xb1\xe6\xf5\x90\x92v\x81\x91\xe2\xfbaxA\xab\x8d\x91\xd9\xd8\xf6I\x0b\
\xa7\xd9\x05\xad:\xefT\xf1?[\xc8\xda\x8at\xa0\xe5p\x00\x00\x00\x00IEND\xaeB`\
\x82'

class vXmlSoftware(vtXmlDomReg):
    TAGNAME_REFERENCE='name'
    TAGNAME_ROOT='Softwareroot'
    APPL_REF=['vHum','vDoc','vPrjDoc','vPrj','vGlobals']
    def __init__(self,appl='vSoftware',attr='id',skip=[],synch=False,verbose=0,
                    audit_trail=True):
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        try:
            vtXmlDomReg.__init__(self,appl=appl,attr=attr,skip=skip,synch=synch,verbose=verbose,
                                audit_trail=audit_trail)
            self.verbose=verbose
            oRoot=vXmlNodeSoftwareRoot('root')
            self.RegisterNode(oRoot,False)
            oRoot=vXmlNodeSoftwareRoot()
            self.RegisterNode(oRoot,False)
            
            oSoftware=vXmlNodeSoftware()
            oSettings=vXmlNodeSettings()
            oLicHost=vXmlNodeSoftwareLicenseHost()
            oBuild=vXmlNodeSoftwareBuild()
            oCustomer=vXmlNodeSoftwareCustomer()
            #oAddr=vtXmlNodeAddr()
            oLog=vtXmlNodeLog()
            oCfg=vtXmlNodeCfg()
            #oArea=vXmlNodeArea()
            self.RegisterNode(oSoftware,True)
            self.RegisterNode(oCustomer,True)
            self.RegisterNode(oBuild,False)
            self.RegisterNode(oLicHost,False)
            
            #self.RegisterNode(oAddr,False)
            self.RegisterNode(oLog,False)
            self.RegisterNode(oCfg,False)
            self.RegisterNode(oSettings,False)
            
            self.LinkRegisteredNode(oRoot,oSoftware)
            self.LinkRegisteredNode(oRoot,oCustomer)
            
            #self.RegisterNode(oArea,False)
            self.LinkRegisteredNode(oSoftware,oLog)
            self.LinkRegisteredNode(oSoftware,oSoftware)
            #self.LinkRegisteredNode(oCust,oAddr)
            #self.LinkRegisteredNode(oCfg,oSettings)
            #self.LinkRegisteredNode(oArea,oArea)
            self.LinkRegisteredNode(oCustomer,oLicHost)
            self.LinkRegisteredNode(oCustomer,oLog)
            
        except:
            vtLog.vtLngTB(self.appl)
        self.SetDftLanguages()
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'softwares')
    #def New(self,revision='1.0',root='softwareroot',bConnection=False):
    #    vtXmlDomReg.New(self,revision,root)
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elem=self.createSubNode(self.getRoot(),'softwares')
        if bConnection==False:
            self.CreateReq()
        #self.createSubNodeTextAttr(elem,'cfg','','id','')
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        #self.AlignDoc()
    #def Build(self):
    #    vtLog.vtLngCur(vtLog.DEBUG,'',self.appl)
    #    vtXmlDomReg.Build(self)
    #def BuildLang(self):
    #    vtLog.vtLngCur(vtLog.DEBUG,'',self.appl)
    #    vtXmlDomReg.BuildLang(self)
