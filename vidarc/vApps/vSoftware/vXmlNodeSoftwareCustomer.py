#----------------------------------------------------------------------------
# Name:         vXmlNodeSoftwareCustomer.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060630
# CVS-ID:       $Id: vXmlNodeSoftwareCustomer.py,v 1.5 2008/03/29 23:42:48 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.tool.xml.vtXmlNodeBase import *
try:
    if vcCust.is2Import(__name__):
        from vidarc.vApps.vSoftware.vXmlNodeSoftwareCustomerPanel import *
        from vidarc.vApps.vSoftware.vXmlNodeSoftwareCustomerEditDialog import *
        from vidarc.vApps.vSoftware.vXmlNodeSoftwareCustomerAddDialog import *
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeSoftwareCustomer(vtXmlNodeBase):
    NODE_ATTRS=[
            #('Tag',None,'tag',None),
            ('Name',None,'name',None),
        ]
    FUNCS_GET_SET_4_LST=['Name']
    FUNCS_GET_4_TREE=['Name']
    FMT_GET_4_TREE=[('Name','')]
    def __init__(self,tagName='SoftwareCustomer'):
        global _
        _=vtLgBase.assignPluginLang('vSoftware')
        vtXmlNodeBase.__init__(self,tagName)
    def GetDescription(self):
        return _(u'software customer')
    # ---------------------------------------------------------
    # specific
    def GetName(self,node):
        sSuff=self.Get(node,'nameSuff')
        try:
            fid=self.GetChildAttrStr(node,'client','fid')
            netDoc,nodeTmp=self.doc.GetNode(fid)
            if nodeTmp is not None:
                o=netDoc.GetRegByNode(nodeTmp)
                return ' '.join([o.GetName(nodeTmp),sSuff])
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return '***'
    def SetName(self,node,val):
        pass
    def __getSWint__(self,node,l):
        if node is not None:
            if self.doc.IsNodeKeyValid(node):
                l.append(self.doc.getNodeText(node,'name'))
                self.__getSWint__(self.doc.getParent(node),l)
    def __getSW__(self,node,l):
        fid=self.GetAttrStr(node,'fid')
        try:
            nodeTmp=self.doc.getNodeByIdNum(long(fid))
            if nodeTmp is not None:
                lName=[]
                self.__getSWint__(nodeTmp,lName)
                lName.reverse()
                sName='.'.join(lName)
                l.append((sName,long(fid)))
        except:
            pass
        return 0
    def GetSWLst(self,node):
        c=self.GetChild(node,'softwareLst')
        l=[]
        self.doc.procChildsExt(c,self.__getSW__,l)
        #l=self.Get(node,'softwareLst').split(';')
        return l
    def SetSWLst(self,node,l):
        c=self.GetChildForced(node,'softwareLst')
        for n in self.doc.getChilds(c):
            self.doc.deleteNode(n,c)
        for sName,fid in l:
            self.doc.createSubNodeTextAttr(c,'softwareModule',sName,'fid','%08d'%fid)
    def getSWDirLst(self,l):
        lDir=[]
        o=self.doc.GetReg('software')
        for tup in l:
            try:
                fid=tup[1]
                nodeTmp=self.doc.getNodeByIdNum(long(fid))
                if nodeTmp is not None:
                    if self.doc.getTagName(nodeTmp)=='software':
                        sDir=o.GetDir(nodeTmp)
                        lDir.append(sDir)
            except:
                pass
        return lDir
    def __procHosts__(self,node,d):
        sTag=self.doc.getTagName(node)
        if sTag=='hostLicense':
            oHost=self.doc.GetReg('hostLicense')
            dHost=oHost.GetInfos(node)
            mid=dHost['mid']
            if mid not in d:
                d[mid]=dHost
        return 0
    def GetHostDict(self,node):
        dHosts={}
        self.doc.procChildsExt(node,self.__procHosts__,dHosts)
        return dHosts
    def __procHostNames__(self,node,d):
        sTag=self.doc.getTagName(node)
        if sTag=='hostLicense':
            oHost=self.doc.GetReg('hostLicense')
            dHost=oHost.GetInfos(node)
            sName=oHost.GetName(node)
            if sName not in d:
                d[sName]=dHost
        return 0
    def GetHostNameDict(self,node):
        dHosts={}
        self.doc.procChildsExt(node,self.__procHostNames__,dHosts)
        return dHosts
    def GetClientID(self,node):
        fid=self.GetChildAttrStr(node,'client','fid')
        return fid
    def GetClientNr(self,node):
        fid=self.GetChildAttrStr(node,'client','fid')
        netDoc,nodeCust=self.doc.GetNode(fid)
        if nodeCust is None:
            return None
        oCust=netDoc.GetRegByNode(nodeCust)
        sNr=oCust.GetNumber(nodeCust)
        return sNr
    def __procCustomer__(self,node,d):
        sTag=self.doc.getTagName(node)
        if sTag=='customer':
            fid=self.doc.getAttribute(node,'fid')
            netDoc,nodeCust=self.doc.GetNode(fid)
            oCust=netDoc.GetRegByNode(nodeCust)
            sName=oCust.GetName(nodeCust)
            sNr=oCust.GetNumber(nodeCust)
            if sName not in d:
                dHosts={}
                d[sName]={'nr':sNr,'fid':fid,'hosts':dHosts}
                self.doc.procChildsExt(node,self.__procHosts__,dHosts)
        return 0
    def GetCustomerDict(self,node):
        d={}
        self.doc.procChildsExt(node,self.__procCustomer__,d)
        return d
    def SetCustomerDict(self,node,d):
        for c in self.doc.getChilds(node,'customer'):
            self.doc.deleteNode(c,node)
        keys=d.keys()
        keys.sort()
        oHost=self.doc.GetReg('hostLicense')
        for k in keys:
            c=self.doc.createSubNodeTextAttr(node,'customer','','fid',d[k]['fid'])
            if 'hosts' in d[k]:
                dHosts=d[k]['hosts']
                kkeys=dHosts.keys()
                kkeys.sort()
                for kk in kkeys:
                    cc=self.doc.createSubNodeTextAttr(c,'hostLicense','','mid',kk)
                    oHost.SetInfos(cc,dHosts[kk])
            self.doc.AlignNode(c)
    # ---------------------------------------------------------
    # inheritance
    def GetPossibleAcl(self):
        return vtSecXmlAclDom.ACL_MSK_NORMAL
    def GetAttrFilterTypes(self):
        """ shall return something like:
             [('attr01',vtXmlFilterType.FILTER_TYPE_STRING),
              ('attr02',vtXmlFilterType.FILTER_TYPE_INT),]
        """
        return None
    def GetTranslation(self,name):
        return name
    def Is2Create(self):
        "create automatically if parent node is created"
        return False
    def Is2Add(self):
        "node can be created by tree content menu"
        return True
    def IsMultiple(self):
        "serveral instances are allowed in one level"
        return True
    def IsMultiLang(self):
        return False
    def IsSkip(self):
        "do not display node in tree"
        return False
    def IsId2Add(self):
        "node id is managed"
        return True
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x01&IDAT(\x91}P=S\x83@\x10]\x1cJ\xc0\xd6\xa3u\x1ck\x89\xd6\xd2;\x93\x1f\xa0\
\xa5q\xe8\x83}\x8e\x9fq9)-l3cM\x92&\x05\xe1\xb0T\xb0\x15\xd2\xc2Q:\x83\x05\
\x8c\x1c\x1fa\xab\xbdw\xfbv\xdf{\xd2\xcb\xb2\x84\xa1\xfa\x8a6\xfe\xfe\xd9\
\xf3\x96"X\xa4\x89\x0c\x00;?\xec\x13\\j\x96|\x0b\x1fL\x04c\x7fw2\xb8>d\x04c\
\x0b\x0ei\xff\xabEp\xa9q\xaa\xa9\x00\xc0\x02\xb2\xb0\x1f\xa0\xe0\x0eY9\xaf\
\xdeQ\x02\x00h\xaa\xd2]\xaf(\xe2\x80,>\xe6v\x9c\xf3"M\x03\xd3\xb4\xa0\xe0\
\x00\xb0\xb0\xa6p\x86Dm\xad\x0bY\xce\xb3\x9c\xa7\xc9\xfe\xf6Ro\xd0\xb6\x93a\
\xd3#%\x0f\xa2\x12\xba\xab\x9a2~\xab\xb4\x8d\x11\x1e\x9f\xea\xec]j\x80\xa2t\
\x08\x8d$\x97\x1a!#}\xbeCV\xd2\xd5\x0c\x14\xb5{an\xc7, .5:\xda0\xb6J\xbe\xfd\
\xb7\xde\x10\xb2\x9c\x9f_\xdc\x1b\x13KS\xeb\xe01F\xf5\xe8wt\xd4C\x95l\xd5#\
\xfdz\xf3\xbenE<\x1e+B\x93\xf5\xe7O\x07\x94\x93\xe8\xc0(\x1b$\x00\xdc\xe0`6\
\xd5\x7fE\xe8\x0f\xd2B\x7f\n\x9b\xaf\xe8\x9f\x00\x00\x00\x00IEND\xaeB`\x82' 
    def getSelImageData(self):
        return self.getImageData()
    def GetEditDialogClass(self):
        if GUI:
            return vXmlNodeSoftwareCustomerEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return vXmlNodeSoftwareCustomerAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vXmlNodeSoftwareCustomerPanel
        else:
            return None
