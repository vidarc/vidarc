#----------------------------------------------------------------------------
# Name:         vXmlSoftwareTree.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060220
# CVS-ID:       $Id: vXmlSoftwareTree.py,v 1.3 2006/11/21 21:16:20 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.tool.xml.vtXmlGrpTreeReg import *
from vidarc.vApps.vSoftware.vNetSoftware import *

import vidarc.vApps.vSoftware.images as imgSoftware

class vXmlSoftwareTree(vtXmlGrpTreeReg):
    def __init__(self, parent, id, pos, size, style, name,
                    master=False,controller=False,verbose=0):
        vtXmlGrpTreeReg.__init__(self,id=id, name=name,master=master,
              controller=controller,verbose=verbose,
              parent=parent, pos=pos, size=size,style=style)
        self.bLangMenu=True
        #self.bGrpMenu=True
        #self.iGrp==0
        #self.grpMenu=[
        #        ('normal',_(u'normal'),
        #                [],
        #                [('name','')]),
        #        ('address:country',_(u'country'),
        #                [],
        #                [('address:country','')]),
        #        ]
    def SetDftGrouping(self):
        self.grouping=[]
        self.bGrouping=False
        self.label=[('name','')]
    def SetDftNodeInfos(self):
        self.SetNodeInfos(['name','|id'])
    def SetNormalGrouping(self):
        if self.iGrp==0:
            return False
        self.iGrp=0
        self.SetGrouping([],[('name','')])
        return True
    def SetCountryGrouping(self):
        if self.iGrp==1:
            return False
        self.iGrp=1
        self.SetGrouping({'customer':[('address:country','')]},[('name','')])
        return True
    def SetRegionGrouping(self):
        if self.iGrp==2:
            return False
        self.iGrp=2
        self.SetGrouping({'customer':[('address:country',''),('address:region','')]},[('name','')])
        return True
    def SetupImageList(self):
        if vtXmlGrpTree.SetupImageList(self):
            
            self.__addElemImage2ImageList__('softwares',
                            imgSoftware.getSoftwareImage(),
                            imgSoftware.getSoftwareImage(),True)
            self.__addElemImage2ImageList__('root',
                            imgSoftware.getSoftwareImage(),
                            imgSoftware.getSoftwareImage(),True)
            
            #imgTmp=images.getCountryBitmap()
            #imgGrp,imgGrpSel=self.getGroupingOverlayImages()
            #img=self.__buildGrpBmp__(imgTmp,imgGrp)
            #self.imgDict['grp']['address:country']=[self.imgLstTyp.Add(img)]
            #imgTmp=images.getCountrySelBitmap()
            #img=self.__buildGrpBmp__(imgTmp,imgGrpSel)
            #self.imgDict['grp']['address:country'].append(self.imgLstTyp.Add(img))
            
            self.SetImageList(self.imgLstTyp)
