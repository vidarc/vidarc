#Boa:FramePanel:vXmlNodeSoftwareCVSPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeSoftwareCVSPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20080329
# CVS-ID:       $Id: vXmlNodeSoftwareCVSPanel.py,v 1.4 2010/05/30 18:17:15 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import vidarc.tool.art.vtThrobber
import vidarc.tool.misc.vtmListCtrl

import sys,os

import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.art.vtArt as vtArt
from vidarc.tool.xml.vtXmlNodePanel import vtXmlNodePanel
from vidarc.tool.vtThread import vtThread
from vidarc.tool.vtProcess import vtProcess

[wxID_VXMLNODESOFTWARECVSPANEL, wxID_VXMLNODESOFTWARECVSPANELCBCHECK, 
 wxID_VXMLNODESOFTWARECVSPANELCBSTOP, wxID_VXMLNODESOFTWARECVSPANELCBTAG, 
 wxID_VXMLNODESOFTWARECVSPANELCHCREC, wxID_VXMLNODESOFTWARECVSPANELLSTSRC, 
 wxID_VXMLNODESOFTWARECVSPANELTHRPROC, wxID_VXMLNODESOFTWARECVSPANELTXTSTATUS, 
] = [wx.NewId() for _init_ctrls in range(8)]

class vXmlNodeSoftwareCVSPanel(wx.Panel,vtXmlNodePanel):
    VERBOSE=0
    def _init_coll_bxsTag_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbCheck, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbTag, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.chcRec, 1, border=4, flag=wx.EXPAND | wx.LEFT)

    def _init_coll_fgsMain_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsTag, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lstSrc, 0, border=0, flag=wx.EXPAND)
        parent.AddSizer(self.bxsButton, 0, border=0, flag=wx.EXPAND)

    def _init_coll_fgsMain_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)
        parent.AddGrowableCol(0)

    def _init_coll_bxsButton_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.thrProc, 0, border=0, flag=0)
        parent.AddWindow(self.txtStatus, 1, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbStop, 0, border=0, flag=0)

    def _init_coll_lstSrc_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'name'), width=-1)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=u'tag supposed', width=80)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT,
              heading=u'tag current', width=80)
        parent.InsertColumn(col=3, format=wx.LIST_FORMAT_LEFT,
              heading=u'rev working', width=80)
        parent.InsertColumn(col=4, format=wx.LIST_FORMAT_LEFT,
              heading=u'rev repository', width=80)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsMain = wx.FlexGridSizer(cols=1, hgap=4, rows=4, vgap=4)

        self.bxsButton = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsTag = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsMain_Items(self.fgsMain)
        self._init_coll_fgsMain_Growables(self.fgsMain)
        self._init_coll_bxsButton_Items(self.bxsButton)
        self._init_coll_bxsTag_Items(self.bxsTag)

        self.SetSizer(self.fgsMain)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODESOFTWARECVSPANEL,
              name=u'vXmlNodeSoftwareCVSPanel', parent=prnt, pos=wx.Point(47,
              47), size=wx.Size(343, 219), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(335, 192))

        self.lstSrc = vidarc.tool.misc.vtmListCtrl.vtmListCtrl(id=wxID_VXMLNODESOFTWARECVSPANELLSTSRC,
              name=u'lstSrc', parent=self, pos=wx.Point(0, 34),
              size=wx.Size(335, 124), style=wx.LC_REPORT)
        self._init_coll_lstSrc_Columns(self.lstSrc)

        self.thrProc = vidarc.tool.art.vtThrobber.vtThrobberDelay(id=wxID_VXMLNODESOFTWARECVSPANELTHRPROC,
              name=u'thrProc', parent=self, pos=wx.Point(0, 162),
              size=wx.Size(15, 15), style=0)

        self.txtStatus = wx.TextCtrl(id=wxID_VXMLNODESOFTWARECVSPANELTXTSTATUS,
              name=u'txtStatus', parent=self, pos=wx.Point(19, 162),
              size=wx.Size(281, 30), style=wx.TE_READONLY, value=u'')

        self.cbStop = wx.lib.buttons.GenBitmapButton(bitmap=vtArt.getBitmap(vtArt.Cancel),
              id=wxID_VXMLNODESOFTWARECVSPANELCBSTOP, name=u'cbStop',
              parent=self, pos=wx.Point(304, 162), size=wx.Size(31, 30),
              style=0)
        self.cbStop.Bind(wx.EVT_BUTTON, self.OnCbStopButton,
              id=wxID_VXMLNODESOFTWARECVSPANELCBSTOP)

        self.chcRec = wx.CheckBox(id=wxID_VXMLNODESOFTWARECVSPANELCHCREC,
              label=u'recursive', name=u'chcRec', parent=self, pos=wx.Point(226,
              0), size=wx.Size(107, 30), style=0)
        self.chcRec.SetValue(False)

        self.cbCheck = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Find),
              id=wxID_VXMLNODESOFTWARECVSPANELCBCHECK, label=u'check',
              name=u'cbCheck', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(111, 30), style=0)
        self.cbCheck.Bind(wx.EVT_BUTTON, self.OnCbCheckButton,
              id=wxID_VXMLNODESOFTWARECVSPANELCBCHECK)

        self.cbTag = wx.lib.buttons.GenBitmapTextButton(bitmap=vtArt.getBitmap(vtArt.Build),
              id=wxID_VXMLNODESOFTWARECVSPANELCBTAG, label=u'tag',
              name=u'cbTag', parent=self, pos=wx.Point(115, 0),
              size=wx.Size(107, 30), style=0)
        self.cbTag.Bind(wx.EVT_BUTTON, self.OnCbTagButton,
              id=wxID_VXMLNODESOFTWARECVSPANELCBTAG)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        vtXmlNodePanel.__init__(self,applName='vXmlNodeSoftwareCVS')
        self.thdBuild=vtThread(self,bPost=True)
        self.thdBuild.BindEvents(funcAborted=self.OnAborted,
                    funcFinished=self.OnFinished)
        self.lstSrc.SetStretchLst([(0,-1)])
        self.imgLst=wx.ImageList(16,16)
        self.imgDict={}
        img=wx.ArtProvider.GetBitmap(wx.ART_TICK_MARK, size=(16, 16))
        self.imgDict['def']=self.imgLst.Add(img)
        img=wx.ArtProvider.GetBitmap(wx.ART_WARNING, size=(16, 16))
        self.imgDict['warn']=self.imgLst.Add(img)
        img=wx.ArtProvider.GetBitmap(wx.ART_ERROR, size=(16, 16))
        self.imgDict['error']=self.imgLst.Add(img)
        img=vtArt.getBitmap(vtArt.Question)
        self.imgDict['quest']=self.imgLst.Add(img)
        img=vtArt.getBitmap(vtArt.Invisible)
        self.imgDict['empty']=self.imgLst.Add(img)
        img=vtArt.getBitmap(vtArt.Apply)
        self.imgDict['Up-to-date']=self.imgLst.Add(img)
        img=vtArt.getBitmap(vtArt.Left)
        self.imgDict['Locally Modified']=self.imgLst.Add(img)
        img=vtArt.getBitmap(vtArt.Right)
        self.imgDict['Needs Patch']=self.imgLst.Add(img)
        self.lstSrc.SetImageList(self.imgLst,wx.IMAGE_LIST_SMALL)
        
        self.lDN=[]
        self.dEntry={}
    def Clear(self):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        vtXmlNodePanel.Clear(self)
        # add code here
        self.lstSrc.DeleteAllItems()
        self.txtStatus.SetValue(u'')
        self.dEntry={}
    def SetNode(self,node):
        try:
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                vtLog.CallStack('')
            if vtXmlNodePanel.SetNode(self,node)<0:
                return
            # add code here
            sSrcDN=os.getenv('vidarcSrc')
            if sSrcDN is None:
                return
            sSrcDN=sSrcDN.replace('\\','/')
            sDN=self.objRegNode.GetDir(self.node)
            sDN=sDN.replace('\\','/')
            bRec=self.chcRec.GetValue()
            self.thdBuild.Start()
            self.thrProc.Start()
            self.thdBuild.Do(self.procSearch,sDN,sSrcDN,{},
                        bCheck=False,bTag=False,bRec=bRec)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnAborted(self,evt):
        self.thrProc.Stop()
        self.txtStatus.SetValue(_(u'aborted.'))
    def OnFinished(self,evt):
        self.thrProc.Stop()
        self.txtStatus.SetValue(_(u'done.'))
    def procSearch(self,sStartDN,sSrcDN,dEntry,bCheck=False,bTag=False,bRec=False):
        self.thdBuild.CallBack(self.txtStatus.SetValue,_(u'search...'))
        iLen=len(sSrcDN)+1
        sTagCur=''
        for sDN,dirs,files in os.walk(sStartDN):
            #for sFN in files:
            #    sFullFN=os.path.join(sDN,sFN)
            if self.VERBOSE:
                vtLog.vtLngCurWX(vtLog.DEBUG,'sDN:%s;dirs:%s;files:%s'%(sDN,
                        vtLog.pformat(dirs),vtLog.pformat(files)),self)
            else:
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurWX(vtLog.DEBUG,'sDN:%s;sStartDN:%s'%(sDN,sStartDN),self)
            if 'CVS' in dirs:
                dirs.remove('CVS')
                sDN=sDN.replace('\\','/')
                if '__config__.py' in files:
                    sRelDN=sDN[:]
                    sMod=sDN[iLen:].replace('/','.')
                    d={}
                    dEntry[sMod]=d
                    if sDN.startswith(sSrcDN):
                        try:
                            m=__import__(sMod+'.__config__',[],[],'VER_MAJOR,VER_MINOR,VER_RELEASE,VER_SUBREL')
                            sTagCur=u'_'.join(['ver',
                                            str(m.VER_MAJOR),
                                            str(m.VER_MINOR),
                                            str(m.VER_RELEASE),
                                            #m.VER_SUBREL
                                            ])
                        except:
                            sTagCur=''
                    if bRec==False:
                        if sDN!=sStartDN:
                            for s in dirs:
                                dirs.remove(s)
                            d[None]=(None,None)
                            continue
                    d[None]=(sRelDN,sTagCur)
                    if bCheck:
                        self.thdBuild.Do(self.procCheck,sMod,sDN,sDN,dEntry)
                else:
                    sModTmp=sDN[iLen:].replace('/','.')
                    lModTmp=sModTmp.split('.')
                    iModTmp=len(lModTmp)
                    sRelDN=None
                    for i in xrange(iModTmp-1,0,-1):
                        sModTmp='.'.join(lModTmp[:i])
                        if sModTmp in dEntry:
                            d=dEntry[sModTmp]
                            sRelDN,sTagCur=d[None]
                            sMod=sModTmp
                            break
                    if sRelDN is None:
                        continue
                    if bCheck:
                        self.thdBuild.Do(self.procCheck,sMod,sDN,sRelDN,dEntry)
                iSkip=iLen+len(sMod)
                if self.VERBOSE:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'sDN:%s;sMod:%s;len:%d;iLen:%d;iSkip:%d'%(sDN,sMod,
                            len(sDN),iLen,iSkip),self)
                for sFN in files:
                    sFullFN='/'.join([sDN[iSkip:],sFN])
                    d[sFullFN[1:]]={'revision':'','working':'','tag':'','tag_cur':sTagCur,'status':''}
                if self.VERBOSE:
                    vtLog.vtLngCurWX(vtLog.DEBUG,'dEntry:%s'%(vtLog.pformat(dEntry)),self)
            else:
                for s in dirs:
                    dirs.remove(s)
            #if bRec==False:
            #    for s in dirs:
            #        dirs.remove(s)
        if self.VERBOSE:
            print vtLog.pformat(dEntry)
        self.thdBuild.CallBack(self.txtStatus.SetValue,_(u'show...'))
        self.thdBuild.DoCallBack(self.showEntry,dEntry)
        self.thdBuild.DoCallBack(self.txtStatus.SetValue,_(u'done.'))
    def procCheck(self,sMod,sDN,sRelDN,dEntry):
        self.thdBuild.CallBack(self.txtStatus.SetValue,_(u'check CVS:%s')%sDN)
        sCurDN=os.getcwd()
        if sys.platform=='win32':
            os.chdir(sDN.replace('/','\\'))
            sCmd='cvs status -l -v'
        else:
            os.chdir(sDN)
            sCmd='cvs status -l -v'#%(sDN)
        p=vtProcess(sCmd,shell=True)
        def handleStdOut(s,l):
            l.append(s.strip())
        def handleStdErr(s,l):
            l.append(s.strip())
        lOut=[]
        lErr=[]
        p.SetCB(1,'\n',handleStdOut,lOut)
        p.SetCB(2,'\n',handleStdOut,lErr)
        p.communicateLoop()
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'sDN:%s;sCmd:%s;lOut;%s;;lErr;%s'%(sDN,sCmd,';'.join(lOut),';'.join(lErr)),self)
        os.chdir(sCurDN)
        d=dEntry[sMod]
        iStatus=-1
        sFN=''
        for sLine in lOut:
            if sLine.startswith('======='):
                iStatus=0
                sFN=''
            if sLine.startswith('? '):
                iStatus=-1
                sFN=sLine[2:]
                sRelFN='/'.join([sDN[len(sRelDN):],sFN])[1:]
                if sRelFN in d:
                    dd=d[sRelFN]
                    dd['status']='quest'
            if iStatus==0:
                if sLine.startswith('File: '):
                    i=sLine.find('Status: ')
                    if i>0:
                        sFN=sLine[6:i].strip()
                        sRelFN='/'.join([sDN[len(sRelDN):],sFN])[1:]
                        if sRelFN in d:
                            dd=d[sRelFN]
                            dd['status']=sLine[i+8:]
                    iStatus=1
            if iStatus==1:
                if sLine.startswith('Working revision:'):
                    if sRelFN in d:
                        dd=d[sRelFN]
                        dd['working']=sLine[17:].strip()
                elif sLine.startswith('Repository revision:'):
                    if sRelFN in d:
                        dd=d[sRelFN]
                        i=sLine.find('\t',21)
                        if i>0:
                            dd['revision']=sLine[20:i].strip()
                        else:
                            dd['revision']=sLine[20:].strip()
                elif sLine.startswith('Existing Tags:'):
                    iStatus=2
                    continue
            if iStatus==2:
                i=sLine.find(' ')
                if sRelFN in d:
                    dd=d[sRelFN]
                    dd['tag']=sLine[:i]
                
                iStatus=-1
    def procTag(self,sDN,sTag):
        self.thdBuild.CallBack(self.txtStatus.SetValue,_(u'tag CVS:%s')%sDN)
        sCurDN=os.getcwd()
        if sys.platform=='win32':
            os.chdir(sDN.replace('/','\\'))
            sCmd='cvs tag -l %s .'%sTag
        else:
            os.chdir(sDN)
            sCmd='cvs tag -l %s .'%(sTag)
        p=vtProcess(sCmd,shell=True)
        def handleStdOut(s,l):
            l.append(s.strip())
        def handleStdErr(s,l):
            l.append(s.strip())
        lOut=[]
        lErr=[]
        p.SetCB(1,'\n',handleStdOut,lOut)
        p.SetCB(2,'\n',handleStdOut,lErr)
        p.communicateLoop()
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'sDN:%s;sCmd:%s;lOut;%s;;lErr;%s'%(sDN,sCmd,';'.join(lOut),';'.join(lErr)),self)
        os.chdir(sCurDN)
    def showEntry(self,dEntry):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'dEntry"%s'%(vtLog.pformat(dEntry)),self)
        self.dEntry=dEntry
        keys=self.dEntry.keys()
        keys.sort()
        self.lstSrc.DeleteAllItems()
        for k in keys:
            d=self.dEntry[k]
            kkeys=d.keys()
            kkeys.sort()
            sDN=k.replace('.','/')
            for kk in kkeys:
                if kk is None:
                    continue
                dd=d[kk]
                if dd['status'] in self.imgDict:
                    imgIdx=self.imgDict[dd['status']]
                else:
                    imgIdx=-1
                idx=self.lstSrc.InsertStringItem(sys.maxint,u'/'.join([sDN,kk]),imgIdx)
                self.lstSrc.SetStringItem(idx,1,dd['tag_cur'])
                self.lstSrc.SetStringItem(idx,2,dd['tag'])
                self.lstSrc.SetStringItem(idx,3,dd['working'])
                self.lstSrc.SetStringItem(idx,4,dd['revision'])
    def GetNode(self,node=None):
        try:
            pass
        except:
            vtLog.vtLngTB(self.GetName())
    def Close(self):
        # add code here
        
        vtXmlNodePanel.Close(self)
    def Cancel(self):
        # add code here
        
        vtXmlNodePanel.Cancel(self)
    def Lock(self,flag):
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        if flag:
            # add code here
            pass
        else:
            # add code here
            pass
    def OnCbStopButton(self, event):
        event.Skip()
        self.thdBuild.Stop()
    def OnCbCheckButton(self, event):
        event.Skip()
        try:
            if self.node is None:
                return
            sSrcDN=os.getenv('vidarcSrc')
            if sSrcDN is None:
                return
            sSrcDN=sSrcDN.replace('\\','/')
            sDN=self.objRegNode.GetDir(self.node)
            sDN=sDN.replace('\\','/')
            if sDN is None:
                return
            bRec=self.chcRec.GetValue()
            self.thdBuild.Start()
            self.thrProc.Start()
            self.thdBuild.Do(self.procSearch,sDN,sSrcDN,{},
                            bCheck=True,bTag=False,bRec=bRec)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbTagButton(self, event):
        event.Skip()
        try:
            if self.node is None:
                return
            sSrcDN=os.getenv('vidarcSrc')
            if sSrcDN is None:
                return
            sSrcDN=sSrcDN.replace('\\','/')
            sDN=self.objRegNode.GetDir(self.node)
            sDN=sDN.replace('\\','/')
            if sDN is None:
                return
            dDN={}
            keys=self.dEntry.keys()
            keys.sort()
            for k in keys:
                d=self.dEntry[k]
                sRelDN,sTagCur=d[None]
                kkeys=d.keys()
                kkeys.sort()
                for kk in kkeys:
                    if kk is None:
                        continue
                    dd=d[kk]
                    sFullFN=u'/'.join([sRelDN,kk])
                    sDN,sFN=os.path.split(sFullFN)
                    if len(dd['revision'])==0:
                        continue
                    if dd['tag_cur']!=dd['tag']:
                        if sDN in dDN:
                            pass
                        else:
                            dDN[sDN]=dd['tag_cur']
            if self.VERBOSE:
                print vtLog.pformat(dDN)
            self.dEntry={}
            self.lstSrc.DeleteAllItems()
            self.thdBuild.Start()
            self.thrProc.Start()
            self.thdBuild.DoCallBack(self.txtStatus.SetValue,_(u'tagging ...'))
            keys=dDN.keys()
            keys.sort()
            #for sDN,sTag in dDN.iteritems():
            for sDN in keys:
                sTag=dDN[sDN]
                self.thdBuild.Do(self.procTag,sDN,sTag)
            self.thdBuild.DoCallBack(self.txtStatus.SetValue,_(u'tagging done.'))
        except:
            vtLog.vtLngTB(self.GetName())
