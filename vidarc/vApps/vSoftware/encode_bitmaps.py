#!/usr/bin/env python
#----------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    #"   -u -i -n Application Tools01_16.png images.py",
#    "   -u -i -n Mondrian mondrian.ico images.py",
    "-u -i -n Element                   img/Box01_16.png            images.py",
    "-a -u -n ElementSel                img/Box02_16.png            images.py",
    "-a -u -n Software                  img/VidMod_Software_16.png  images.py",
    "-a -u -n SoftwareSel               img/VidMod_Software_16.png  images.py",
    "-a -u -n Module                    img/Module01_16.png         images.py",
    "-a -u -n ModuleSel                 img/Module01_16.png         images.py",
    "-a -u -n Doc                       img/Doc01_16.png            images.py",
    "-a -u -n DocSel                    img/Doc01_16.png            images.py",
    "-a -u -n DocGrp                    img/DocGrp02_16.png         images.py",
    "-a -u -n DocGrpSel                 img/DocGrp02_16.png         images.py",
    "-a -u -n FileFil                   img/FileTypeFil01_16.png    images.py",
    "-a -u -n FilePo                    img/FileTypePo01_16.png     images.py",
    "-a -u -n FilePot                   img/FileTypePot01_16.png    images.py",
    "-a -u -n FilePy                    img/FileTypePy01_16.png     images.py",
    "-a -u -n FileXml                   img/FileTypeXml01_16.png    images.py",
    "-a -u -n User                      img/Usr02_16.png            images.py",
    "-a -u -n Client                    img/Client01_16.png         images.py",
    "-a -u -n vXmlNodeSoftware          img/Application01_16.png    images.py",
    "-a -u -n vXmlNodeSoftwareMetric    img/Measure01_16.png        images.py",
    "-a -u -n vXmlNodeSoftwareBuild     img/Find01_16.png           images.py",
    "-a -u -n vXmlNodeSoftwareCVS       img/Find01_16.png           images.py",
    "-a -u -n vtXmlNodeLog              img/Note01_16.png           images.py",
    "-a -u -n Add                       img/add.png                 images.py",
    "-a -u -n Cancel                    img/abort.png               images.py",
    "-a -u -n Del                       img/waste.png               images.py",
    "-a -u -n Apply                     img/ok.png                  images.py",
    #"-a -u -n Browse img/BrowseFile01_16.png images.py",
    #"-a -u -n Open img/DocEdit01_16.png images.py",
    "-a -u -n Application               img/VidMod_Software_16.ico  images.py",
    "-a -u -n Plugin                    img/VidMod_Software_16.png  images.py",
    "-a -u -n Launch                    img/Launch01_32.png         images.py",
    
    "-u -i -n Splash                    img/splashTmpl02.png        images_splash.py",
    "-a -u -n Logo                      img/Vidarc_Logo_Brief.png   images_splash.py",
    
    "-a -u -n NoIcon                    img/noicon.png              images.py"
    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)

