#Boa:FramePanel:vXmlNodeSoftwareLicenseHostPanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeSoftwareBuildPanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060629
# CVS-ID:       $Id: vXmlNodeSoftwareLicenseHostPanel.py,v 1.5 2007/05/07 18:46:44 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.vApps.vCustomer.vXmlCustomerInputTree
import vidarc.tool.input.vtInputTree
import vidarc.tool.art.vtThrobber
import wx.lib.buttons
import wx.lib.filebrowsebutton

from vidarc.tool.net.vNetXmlWxGui import *
import vidarc.tool.xml.vtXmlDom as vtXmlDom
import vidarc.tool.xml.vtXmlDomConsumer as vtXmlDomConsumer
import vidarc.tool.art.vtArt as vtArt
import vidarc.tool.log.vtLog as vtLog
import vidarc.tool.time.vtTime as vtTime
from vidarc.tool.vtThread import vtThread
import vidarc.install.identify as vIdentify
import vidarc.tool.InOut.fnUtil as fnUtil
#import ezPyCrypto
import vidCrypto

import sys,os,os.path
import threading,thread,time

[wxID_VXMLNODESOFTWARELICENSEHOSTPANEL, 
 wxID_VXMLNODESOFTWARELICENSEHOSTPANELCBBUILDREL, 
 wxID_VXMLNODESOFTWARELICENSEHOSTPANELCBBUILDRELDN, 
 wxID_VXMLNODESOFTWARELICENSEHOSTPANELCBDECODELIC, 
 wxID_VXMLNODESOFTWARELICENSEHOSTPANELCBGEN, 
 wxID_VXMLNODESOFTWARELICENSEHOSTPANELCBRELCOVERSRCPKG, 
 wxID_VXMLNODESOFTWARELICENSEHOSTPANELCBSTOP, 
 wxID_VXMLNODESOFTWARELICENSEHOSTPANELDBBRELDN, 
 wxID_VXMLNODESOFTWARELICENSEHOSTPANELFBBREL, 
 wxID_VXMLNODESOFTWARELICENSEHOSTPANELLBLCUSTOMER, 
 wxID_VXMLNODESOFTWARELICENSEHOSTPANELLBLCUSTOMERID, 
 wxID_VXMLNODESOFTWARELICENSEHOSTPANELLBLFILES, 
 wxID_VXMLNODESOFTWARELICENSEHOSTPANELLBLHOST, 
 wxID_VXMLNODESOFTWARELICENSEHOSTPANELLBLMISC, 
 wxID_VXMLNODESOFTWARELICENSEHOSTPANELLBLRECSRCPKG, 
 wxID_VXMLNODESOFTWARELICENSEHOSTPANELLSTINFO, 
 wxID_VXMLNODESOFTWARELICENSEHOSTPANELNBINFOS, 
 wxID_VXMLNODESOFTWARELICENSEHOSTPANELPNADDR, 
 wxID_VXMLNODESOFTWARELICENSEHOSTPANELPNBILL, 
 wxID_VXMLNODESOFTWARELICENSEHOSTPANELPNGEN, 
 wxID_VXMLNODESOFTWARELICENSEHOSTPANELPNMACHINE, 
 wxID_VXMLNODESOFTWARELICENSEHOSTPANELPNREL, 
 wxID_VXMLNODESOFTWARELICENSEHOSTPANELTHRPROC, 
 wxID_VXMLNODESOFTWARELICENSEHOSTPANELTXTADDR, 
 wxID_VXMLNODESOFTWARELICENSEHOSTPANELTXTBILL, 
 wxID_VXMLNODESOFTWARELICENSEHOSTPANELTXTCUSTOMER, 
 wxID_VXMLNODESOFTWARELICENSEHOSTPANELTXTCUSTOMERID, 
 wxID_VXMLNODESOFTWARELICENSEHOSTPANELTXTLIC, 
 wxID_VXMLNODESOFTWARELICENSEHOSTPANELTXTMISC, 
] = [wx.NewId() for _init_ctrls in range(29)]

class vXmlNodeSoftwareLicenseHostPanel(wx.Panel,vtXmlDomConsumer.vtXmlDomConsumer):
    VERBOSE=0
    BUF_SIZE=16384
    def _init_coll_bxsBtRelDN_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.dbbRelDN, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbBuildRelDN, 0, border=0, flag=0)

    def _init_coll_bxsAddr_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.txtAddr, 1, border=0, flag=wx.EXPAND)

    def _init_coll_bxsHost_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.txtLic, 1, border=4, flag=wx.EXPAND)
        parent.AddWindow(self.cbDecodeLic, 0, border=4, flag=wx.LEFT)

    def _init_coll_bxsRelRecover_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblRecSrcPkg, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbRelCoverSrcPkg, 0, border=0, flag=0)

    def _init_coll_fgsGen_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)

    def _init_coll_bxsHostAdd_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblHost, 1, border=0, flag=wx.EXPAND)

    def _init_coll_fgsRel_Items(self, parent):
        # generated method, don't edit

        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsRelRecover, 1, border=0, flag=wx.EXPAND)

    def _init_coll_fgsGen_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblCustomer, 1, border=4,
              flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.txtCustomer, 2, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblCustomerID, 0, border=4,
              flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.txtCustomerID, 0, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.lblMisc, 0, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.txtMisc, 0, border=0, flag=wx.EXPAND)

    def _init_coll_bxsBill_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.txtBill, 1, border=0, flag=wx.EXPAND)

    def _init_coll_bxsInfo_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstInfo, 1, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsHostAdd, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsHost, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.nbInfos, 0, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsButton, 1, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsBtRev, 0, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsBtRelDN, 0, border=4, flag=wx.TOP | wx.EXPAND)

    def _init_coll_fgsRel_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)

    def _init_coll_bxsBtRev_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.fbbRel, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.cbBuildRel, 0, border=0, flag=0)

    def _init_coll_bxsButton_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.thrProc, 0, border=0, flag=0)
        parent.AddWindow(self.cbGen, 0, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.lblFiles, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbStop, 0, border=4, flag=wx.LEFT)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(1)
        parent.AddGrowableRow(2)
        parent.AddGrowableCol(0)

    def _init_coll_nbInfos_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.pnGen, select=False,
              text=u'General')
        parent.AddPage(imageId=-1, page=self.pnMachine, select=False,
              text=u'Machine')
        parent.AddPage(imageId=-1, page=self.pnAddr, select=False,
              text=u'Address')
        parent.AddPage(imageId=-1, page=self.pnBill, select=False,
              text=u'Billing')
        parent.AddPage(imageId=-1, page=self.pnRel, select=True,
              text=u'Release')

    def _init_coll_lstInfo_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT, heading=u'name',
              width=120)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT, heading=u'value',
              width=250)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=0, rows=6, vgap=0)

        self.bxsButton = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsHost = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsHostAdd = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBill = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsAddr = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsInfo = wx.BoxSizer(orient=wx.VERTICAL)

        self.fgsGen = wx.FlexGridSizer(cols=1, hgap=0, rows=6, vgap=0)

        self.bxsBtRev = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBtRelDN = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.fgsRel = wx.FlexGridSizer(cols=1, hgap=4, rows=2, vgap=4)

        self.bxsRelRecover = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsButton_Items(self.bxsButton)
        self._init_coll_bxsHost_Items(self.bxsHost)
        self._init_coll_bxsHostAdd_Items(self.bxsHostAdd)
        self._init_coll_bxsBill_Items(self.bxsBill)
        self._init_coll_bxsAddr_Items(self.bxsAddr)
        self._init_coll_bxsInfo_Items(self.bxsInfo)
        self._init_coll_fgsGen_Items(self.fgsGen)
        self._init_coll_fgsGen_Growables(self.fgsGen)
        self._init_coll_bxsBtRev_Items(self.bxsBtRev)
        self._init_coll_bxsBtRelDN_Items(self.bxsBtRelDN)
        self._init_coll_fgsRel_Items(self.fgsRel)
        self._init_coll_fgsRel_Growables(self.fgsRel)
        self._init_coll_bxsRelRecover_Items(self.bxsRelRecover)

        self.SetSizer(self.fgsLog)
        self.pnAddr.SetSizer(self.bxsAddr)
        self.pnBill.SetSizer(self.bxsBill)
        self.pnMachine.SetSizer(self.bxsInfo)
        self.pnGen.SetSizer(self.fgsGen)
        self.pnRel.SetSizer(self.fgsRel)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODESOFTWARELICENSEHOSTPANEL,
              name=u'vXmlNodeSoftwareLicenseHostPanel', parent=prnt,
              pos=wx.Point(5, 200), size=wx.Size(312, 385),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 358))
        self.SetAutoLayout(True)

        self.cbGen = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODESOFTWARELICENSEHOSTPANELCBGEN,
              bitmap=wx.EmptyBitmap(16 , 16), name=u'cbGen', parent=self,
              pos=wx.Point(19, 258), size=wx.Size(31, 31), style=0)
        self.cbGen.SetMinSize(wx.Size(31, 31))
        self.cbGen.Bind(wx.EVT_BUTTON, self.OnCbGenButton,
              id=wxID_VXMLNODESOFTWARELICENSEHOSTPANELCBGEN)

        self.cbStop = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODESOFTWARELICENSEHOSTPANELCBSTOP,
              bitmap=wx.EmptyBitmap(16 , 16), name=u'cbStop', parent=self,
              pos=wx.Point(273, 258), size=wx.Size(31, 30), style=0)
        self.cbStop.Bind(wx.EVT_BUTTON, self.OnCbStopButton,
              id=wxID_VXMLNODESOFTWARELICENSEHOSTPANELCBSTOP)

        self.thrProc = vidarc.tool.art.vtThrobber.vtThrobberDelay(id=wxID_VXMLNODESOFTWARELICENSEHOSTPANELTHRPROC,
              name=u'thrProc', parent=self, pos=wx.Point(0, 258),
              size=wx.Size(15, 15), style=0)

        self.lblHost = wx.StaticText(id=wxID_VXMLNODESOFTWARELICENSEHOSTPANELLBLHOST,
              label=u'host', name=u'lblHost', parent=self, pos=wx.Point(4, 4),
              size=wx.Size(296, 13), style=0)
        self.lblHost.SetMinSize(wx.Size(-1, -1))

        self.lblFiles = wx.StaticText(id=wxID_VXMLNODESOFTWARELICENSEHOSTPANELLBLFILES,
              label=u'', name=u'lblFiles', parent=self, pos=wx.Point(54, 258),
              size=wx.Size(215, 31), style=0)
        self.lblFiles.SetMinSize(wx.Size(-1, -1))

        self.txtLic = wx.TextCtrl(id=wxID_VXMLNODESOFTWARELICENSEHOSTPANELTXTLIC,
              name=u'txtLic', parent=self, pos=wx.Point(4, 21),
              size=wx.Size(261, 47), style=wx.TE_MULTILINE, value=u'')
        self.txtLic.SetMinSize(wx.Size(-1, 40))

        self.cbDecodeLic = wx.lib.buttons.GenBitmapButton(ID=wxID_VXMLNODESOFTWARELICENSEHOSTPANELCBDECODELIC,
              bitmap=vtArt.getBitmap(vtArt.Build), name=u'cbDecodeLic',
              parent=self, pos=wx.Point(269, 21), size=wx.Size(31, 30),
              style=0)
        self.cbDecodeLic.Bind(wx.EVT_BUTTON, self.OnCbDecodeLicButton,
              id=wxID_VXMLNODESOFTWARELICENSEHOSTPANELCBDECODELIC)

        self.nbInfos = wx.Notebook(id=wxID_VXMLNODESOFTWARELICENSEHOSTPANELNBINFOS,
              name=u'nbInfos', parent=self, pos=wx.Point(4, 72),
              size=wx.Size(296, 182), style=0)

        self.pnMachine = wx.Panel(id=wxID_VXMLNODESOFTWARELICENSEHOSTPANELPNMACHINE,
              name=u'pnMachine', parent=self.nbInfos, pos=wx.Point(0, 0),
              size=wx.Size(288, 156), style=wx.TAB_TRAVERSAL)

        self.pnAddr = wx.Panel(id=wxID_VXMLNODESOFTWARELICENSEHOSTPANELPNADDR,
              name=u'pnAddr', parent=self.nbInfos, pos=wx.Point(0, 0),
              size=wx.Size(288, 156), style=wx.TAB_TRAVERSAL)

        self.pnBill = wx.Panel(id=wxID_VXMLNODESOFTWARELICENSEHOSTPANELPNBILL,
              name=u'pnBill', parent=self.nbInfos, pos=wx.Point(0, 0),
              size=wx.Size(288, 156), style=wx.TAB_TRAVERSAL)

        self.lstInfo = wx.ListCtrl(id=wxID_VXMLNODESOFTWARELICENSEHOSTPANELLSTINFO,
              name=u'lstInfo', parent=self.pnMachine, pos=wx.Point(0, 0),
              size=wx.Size(288, 156), style=wx.LC_REPORT)
        self.lstInfo.SetMinSize(wx.Size(-1, -1))
        self._init_coll_lstInfo_Columns(self.lstInfo)

        self.txtBill = wx.TextCtrl(id=wxID_VXMLNODESOFTWARELICENSEHOSTPANELTXTBILL,
              name=u'txtBill', parent=self.pnBill, pos=wx.Point(0, 0),
              size=wx.Size(288, 156), style=wx.TE_MULTILINE, value='')
        self.txtBill.SetMinSize(wx.Size(-1, -1))

        self.txtAddr = wx.TextCtrl(id=wxID_VXMLNODESOFTWARELICENSEHOSTPANELTXTADDR,
              name=u'txtAddr', parent=self.pnAddr, pos=wx.Point(0, 0),
              size=wx.Size(288, 156), style=wx.TE_MULTILINE, value=u'')
        self.txtAddr.SetMinSize(wx.Size(-1, -1))

        self.pnGen = wx.Panel(id=wxID_VXMLNODESOFTWARELICENSEHOSTPANELPNGEN,
              name=u'pnGen', parent=self.nbInfos, pos=wx.Point(0, 0),
              size=wx.Size(288, 156), style=wx.TAB_TRAVERSAL)

        self.lblCustomer = wx.StaticText(id=wxID_VXMLNODESOFTWARELICENSEHOSTPANELLBLCUSTOMER,
              label=u'customer', name=u'lblCustomer', parent=self.pnGen,
              pos=wx.Point(0, 0), size=wx.Size(284, 13), style=0)
        self.lblCustomer.SetMinSize(wx.Size(-1, -1))

        self.lblCustomerID = wx.StaticText(id=wxID_VXMLNODESOFTWARELICENSEHOSTPANELLBLCUSTOMERID,
              label=u'customer ID', name=u'lblCustomerID', parent=self.pnGen,
              pos=wx.Point(0, 34), size=wx.Size(284, 13), style=0)
        self.lblCustomerID.SetMinSize(wx.Size(-1, -1))

        self.lblMisc = wx.StaticText(id=wxID_VXMLNODESOFTWARELICENSEHOSTPANELLBLMISC,
              label=u'misc', name=u'lblMisc', parent=self.pnGen, pos=wx.Point(0,
              68), size=wx.Size(284, 13), style=0)
        self.lblMisc.SetMinSize(wx.Size(-1, -1))

        self.txtCustomer = wx.TextCtrl(id=wxID_VXMLNODESOFTWARELICENSEHOSTPANELTXTCUSTOMER,
              name=u'txtCustomer', parent=self.pnGen, pos=wx.Point(0, 13),
              size=wx.Size(288, 21), style=0, value=u'')
        self.txtCustomer.SetMinSize(wx.Size(-1, -1))

        self.txtCustomerID = wx.TextCtrl(id=wxID_VXMLNODESOFTWARELICENSEHOSTPANELTXTCUSTOMERID,
              name=u'txtCustomerID', parent=self.pnGen, pos=wx.Point(0, 47),
              size=wx.Size(288, 21), style=0, value=u'')
        self.txtCustomerID.SetMinSize(wx.Size(-1, -1))

        self.txtMisc = wx.TextCtrl(id=wxID_VXMLNODESOFTWARELICENSEHOSTPANELTXTMISC,
              name=u'txtMisc', parent=self.pnGen, pos=wx.Point(0, 81),
              size=wx.Size(288, 21), style=0, value=u'')
        self.txtMisc.SetMinSize(wx.Size(-1, -1))

        self.fbbRel = wx.lib.filebrowsebutton.FileBrowseButton(buttonText='...',
              dialogTitle=u'Choose a file', fileMask=u'*.zip',
              id=wxID_VXMLNODESOFTWARELICENSEHOSTPANELFBBREL,
              labelText=u'Release FN:', parent=self, pos=wx.Point(0, 293),
              size=wx.Size(228, 30), startDirectory='.', style=0)

        self.cbBuildRel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODESOFTWARELICENSEHOSTPANELCBBUILDREL,
              bitmap=vtArt.getBitmap(vtArt.Build), label=u'Release',
              name=u'cbBuildRel', parent=self, pos=wx.Point(228, 293),
              size=wx.Size(76, 30), style=0)
        self.cbBuildRel.SetMinSize(wx.Size(-1, -1))
        self.cbBuildRel.Bind(wx.EVT_BUTTON, self.OnCbBuildRelButton,
              id=wxID_VXMLNODESOFTWARELICENSEHOSTPANELCBBUILDREL)

        self.dbbRelDN = wx.lib.filebrowsebutton.DirBrowseButton(buttonText='...',
              dialogTitle='Choose a release source directory',
              id=wxID_VXMLNODESOFTWARELICENSEHOSTPANELDBBRELDN,
              labelText=u'Release DN:', parent=self, pos=wx.Point(0, 327),
              size=wx.Size(228, 30), startDirectory='.', style=0)

        self.cbBuildRelDN = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODESOFTWARELICENSEHOSTPANELCBBUILDRELDN,
              bitmap=vtArt.getBitmap(vtArt.Build), label=u'Release',
              name=u'cbBuildRelDN', parent=self, pos=wx.Point(228, 327),
              size=wx.Size(76, 30), style=0)
        self.cbBuildRelDN.SetMinSize(wx.Size(-1, -1))
        self.cbBuildRelDN.Bind(wx.EVT_BUTTON, self.OnCbBuildRelDNButton,
              id=wxID_VXMLNODESOFTWARELICENSEHOSTPANELCBBUILDRELDN)

        self.pnRel = wx.Panel(id=wxID_VXMLNODESOFTWARELICENSEHOSTPANELPNREL,
              name=u'pnRel', parent=self.nbInfos, pos=wx.Point(0, 0),
              size=wx.Size(288, 156), style=wx.TAB_TRAVERSAL)

        self.lblRecSrcPkg = wx.StaticText(id=wxID_VXMLNODESOFTWARELICENSEHOSTPANELLBLRECSRCPKG,
              label=u'recover source packages from vidarc packages',
              name=u'lblRecSrcPkg', parent=self.pnRel, pos=wx.Point(0, 12),
              size=wx.Size(197, 30), style=0)
        self.lblRecSrcPkg.SetMinSize(wx.Size(-1, -1))

        self.cbRelCoverSrcPkg = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VXMLNODESOFTWARELICENSEHOSTPANELCBRELCOVERSRCPKG,
              bitmap=vtArt.getBitmap(vtArt.Zip), label=u'vidarc -> zip',
              name=u'cbRelCoverSrcPkg', parent=self.pnRel, pos=wx.Point(197,
              12), size=wx.Size(91, 30), style=0)
        self.cbRelCoverSrcPkg.SetMinSize(wx.Size(-1, -1))
        self.cbRelCoverSrcPkg.Bind(wx.EVT_BUTTON, self.OnCbRelCoverSrcPkgButton,
              id=wxID_VXMLNODESOFTWARELICENSEHOSTPANELCBRELCOVERSRCPKG)

        self._init_coll_nbInfos_Pages(self.nbInfos)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        self._init_ctrls(parent)
        
        vtXmlDomConsumer.vtXmlDomConsumer.__init__(self)
        
        self.thdBuild=vtThread(self,bPost=False)
        self.cbGen.SetBitmapLabel(vtArt.getBitmap(vtArt.Build))
        self.cbStop.SetBitmapLabel(vtArt.getBitmap(vtArt.Cancel))
        
        self.bModified=False
        self.bAutoApply=False
        self.bBlock=False
        self.iSelCustomer=-1
        self.dHostCfg={}
        
        self.dt=vtTime.vtDateTime(True)
        
        self.lstMarkObjs=[self.lstInfo]
        
        self.widLogging=vtLog.GetPrintMsgWid(self)
        
        self.Move(pos)
        self.SetSize(size)
    
    def Lock(self,flag):
        if flag:
            self.txtLic.Enable(False)
            self.cbDecodeLic.Enable(False)
        else:
            self.txtLic.Enable(True)
            self.cbDecodeLic.Enable(True)
    def SetAutoApply(self,flag):
        self.bAutoApply=flag
    def __clearBlock__(self):
        self.bBlock=False
    def SetModified(self,state,obj=None):
        if state:
            if obj is None:
                for obj in self.lstMarkObjs:
                    f=obj.GetFont()
                    f.SetWeight(wx.FONTWEIGHT_BOLD)
                    obj.SetFont(f)
                    obj.Refresh()
            else:
                f=obj.GetFont()
                f.SetWeight(wx.FONTWEIGHT_BOLD)
                obj.SetFont(f)
                obj.Refresh()
        else:
            if obj is None:
                for obj in self.lstMarkObjs:
                    f=obj.GetFont()
                    f.SetWeight(wx.FONTWEIGHT_NORMAL)
                    obj.SetFont(f)
                    obj.Refresh()
                #self.viHum.__markModified__(False)
            else:
                f=obj.GetFont()
                f.SetWeight(wx.FONTWEIGHT_NORMAL)
                obj.SetFont(f)
                obj.Refresh()
        self.bModified=state    
    def GetModified(self):
        return self.bModified
    def __isModified__(self):
        if self.bModified:
            return True
        return False
    def Clear(self):
        self.bBlock=True
        self.SetModified(False)
        vtXmlDomConsumer.vtXmlDomConsumer.Clear(self)
        
        self.txtAddr.SetValue('')
        self.txtBill.SetValue('')
        self.txtCustomer.SetValue('')
        self.txtCustomerID.SetValue('')
        self.txtMisc.SetValue('')
        self.lstInfo.DeleteAllItems()
        self.txtLic.SetValue('')
        self.iSelCustomer=-1
        wx.CallAfter(self.__clearBlock__)
    def SetRegNode(self,o):
        self.objRegNode=o
    def SetNetDocs(self,d):
        pass
    def SetDoc(self,doc,bNet):
        vtXmlDomConsumer.vtXmlDomConsumer.SetDoc(self,doc)
    def SetNode(self,node):
        #if self.bModified==True:
        #    if self.bAutoApply:
        #        self.GetNode(self.node)
        #    else:
                # ask
        #        dlg=wx.MessageDialog(self,u'Do you to apply modified data?' ,
        #                    _(u'vLog Info'),
        #                    wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
        #        if dlg.ShowModal()==wx.ID_YES:
        #            self.GetNode(self.node)
        self.bBlock=True
        self.SetModified(False)
        self.Clear()
        vtXmlDomConsumer.vtXmlDomConsumer.SetNode(self,node)
        try:
            self.dHostCfg=self.objRegNode.GetDict(self.node)
        except:
            self.dHostCfg={}
            vtLog.vtLngTB(self.GetName())
        self.fbbRel.SetValue(self.objRegNode.GetReleaseFN(self.node))
        self.dbbRelDN.SetValue(self.objRegNode.GetReleaseDN(self.node))
        if self.VERBOSE:
            vtLog.CallStack('')
            print self.dHostCfg
            print self.node
        self.__showHost__()
        wx.CallAfter(self.__clearBlock__)
    def GetNode(self,node=None):
        if node is None:
            node=self.node
        if node is not None:
            pass
        try:
            if self.VERBOSE:
                vtLog.CallStack('')
                print self.dHostCfg
            self.objRegNode.SetDict(node,self.dHostCfg)
            self.objRegNode.SetReleaseFN(node,self.fbbRel.GetValue())
            self.objRegNode.SetReleaseDN(node,self.dbbRelDN.GetValue())
            self.doc.AlignNode(node)
            self.SetModified(False)
        except:
            vtLog.vtLngTB(self.GetName())
    def Apply(self,bDoApply=False):
        #self.SetModified(False)
        return False
    def Cancel(self):
        self.SetModified(False)
    
    def __doBuild__(self):
        try:
            sCWD=os.getcwd()
            sID=fnUtil.replaceSuspectChars(self.txtCustomerID.GetValue())
            sMID=fnUtil.replaceSuspectChars(self.dMachine['mid'])
            sLicDN=os.path.join(sCWD,'licences',sID,sMID)
            os.makedirs(sLicDN)
        except:
            pass
        try:
            sKeyFN=os.path.join(sLicDN,'pubkey.pub')
            f=open(sKeyFN,'w')
            f.write(self.dMachine['pub_key'])
            f.close()
            setup_args['vidarcfile']='../%s/vidarc.tool.%s.vidarc'%(sLicDN,version)
            setup_args['options']['vidarc']['pubkey']=sKeyFN
            setup_args['options']['vidarc']['licencedir']='%s'%(sLicDN)
        except:
            traceback.print_exc()
            return
        if self.VERBOSE:
            print sKeyFN
            print setup_args['pubkey']
        sDir=vIdentify.getCfg('dn',['source'])
        os.chdir(sDir+os.path.sep+'..')
        setup(**setup_args)
        os.chdir(sCWD)

    def __build__(self,sDN,sLicDN,dCustomer):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        try:
            sCWD=os.getcwd()
            os.chdir(sDN)
            keys=dCustomer.keys()
            keys.sort()
            #sys.path.insert(0,sDN)
            #import __setup__
            
            for k in keys:
                try:
                    if vtLog.vtLngIsLogged(vtLog.DEBUG):
                        vtLog.vtLngCurWX(vtLog.DEBUG,'dCustomer:%s'%(vtLog.pformat(dCustomer[k])),self)
                    sDNCust=dCustomer[k]['nr']
                    dHosts=dCustomer[k]['hosts']
                    keysHost=dHosts.keys()
                    keysHost.sort()
                    for keyHost in keysHost:
                        # build license here
                        try:
                            sID=fnUtil.replaceSuspectChars(sDNCust)
                            sMID=fnUtil.replaceSuspectChars(keyHost)
                            sLicCustDN=os.path.join(sLicDN,sID,sMID)
                            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                                vtLog.vtLngCurWX(vtLog.DEBUG,'dn:%s;licCustDN:%s'%(sDN,sLicCustDN),self)
                            #dHosts[keyHost]
                            sKeyFN=os.path.join(sLicCustDN,'pubkey.pub')
                            f=open(sKeyFN,'w')
                            f.write(dHosts[keyHost]['pub_key'])
                            f.close()
                            
                            #__setup__.setup_args['options']['vidarc']['licencedir']=sLicCustDN
                            #__setup__.setup(**__setup__.setup_args)
                            os.system('python __setup__.py vidarc --licencedir=%s --mid=%s --pubkey=%s'%(sLicCustDN,keyHost,sKeyFN))
                            #self.__doBuild__(sDN,sLicDN,sDNCust,keyHost,dHosts[keyHost])
                        except:
                            vtLog.vtLngTB(self.GetName())
                except:
                    vtLog.vtLngTB(self.GetName())
        except:
            vtLog.vtLngTB(self.GetName())
        #sys.path=sys.path[1:]
        os.chdir(sCWD)
    def OnCbGenButton(self, event):
        try:
            #if self.tgMaster.GetValue():
            #    pass
            
            #if self.tgAllCustomers.GetValue():
            #    d=self.dCustomer
            #else:
            #    d={}
            #    if self.iSelCustomer<0:
            #        return
            #    sCustomer=self.lstCustomer.GetItem(self.iSelCustomer,0).m_text
            #    d[sCustomer]=self.dCustomer[sCustomer]
            nodeSetting=self.doc.getChild(self.doc.getRoot(),'settings')
            o=self.doc.GetReg('Settings')
            sLicDN=o.GetDir(nodeSetting)
            o=self.doc.GetRegByNode(self.node)
            d={}
            dInfos=o.GetDict(self.node)
            dMachine=dInfos['machine']
            mid=dMachine['mid']
            sName=o.GetName(self.node)
            if mid not in d:
                #if os.exists
                d[mid]=(dInfos['customerID'],dMachine['pub_key'],sName)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'dCustomer:%s'%(vtLog.pformat(d)),self)
            #sDN=o.GetDir(self.node)
            #if vtLog.vtLngIsLogged(vtLog.DEBUG):
            #    vtLog.vtLngCurWX(vtLog.DEBUG,'DN:%s;licDN:%s'%(sDN,sLicDN),self)
            
            #self.thdBuild.Do(self.__build__,sDN,sLicDN,d)
            nodeCust=self.doc.getParent(self.node)
            oCust=self.doc.GetRegByNode(nodeCust)
            lSW=oCust.GetSWLst(nodeCust)
            lDN=oCust.getSWDirLst(lSW)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'DN:%s;licDN:%s'%(vtLog.pformat(lDN),sLicDN),self)
            self.thdBuild.Do(self.objRegNode.BuildLic,self,lDN,sLicDN,d)
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()

    def OnCbStopButton(self, event):
        self.thdBuild.Stop()
        event.Skip()

    def __showCustomer__(self):
        self.iSelCustomer=-1
        self.lstCustomer.DeleteAllItems()
        self.lstInfo.DeleteAllItems()
        keys=self.dCustomer.keys()
        keys.sort()
        for k in keys:
            idx=self.lstCustomer.InsertStringItem(sys.maxint,k)
            d=self.dCustomer[k]
            self.lstCustomer.SetStringItem(idx,1,d['nr'])
            self.lstCustomer.SetStringItem(idx,2,d['fid'])
    def __showHost__(self):
        self.lstInfo.DeleteAllItems()
        try:
            self.txtAddr.SetValue(self.dHostCfg['address'])
        except:
            self.txtAddr.SetValue('')
        try:
            self.txtBill.SetValue(self.dHostCfg['billing'])
        except:
            self.txtBill.SetValue('')
        try:
            self.txtCustomer.SetValue(self.dHostCfg['customer'])
        except:
            self.txtCustomer.SetValue('')
        try:
            self.txtCustomerID.SetValue(self.dHostCfg['customerID'])
        except:
            self.txtCustomerID.SetValue('')
        try:
            dMachine=self.dHostCfg['machine']
            keys=dMachine.keys()
            keys.sort()
            for k in keys:
                idx=self.lstInfo.InsertStringItem(sys.maxint,k)
                self.lstInfo.SetStringItem(idx,1,dMachine[k])
        except:
            pass
    def OnCbAddCustomerButton(self, event):
        event.Skip()
        try:
            sVal=self.viCustomer.GetValue()
            sID=self.viCustomer.GetForeignID()
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'customer:%s;fid:%s'%(sVal,sID),self)
            netDoc,node=self.doc.GetNode(sID)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'customer:%s;'%(node),self)
            oCust=netDoc.GetRegByNode(node)
            sName=oCust.GetName(node)
            sNr=oCust.GetNumber(node)
            if sName not in self.dCustomer:
                self.dCustomer[sName]={'nr':sNr,'fid':sID,'hosts':{}}
            self.SetModified(True,self.lstCustomer)
            self.__showCustomer__()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbDelCustomerButton(self, event):
        event.Skip()

    def OnCbDecodeLicButton(self, event):
        event.Skip()
        try:
            s=self.txtLic.GetValue()
            if len(s)==0:
                return
            d=self.objRegNode.DecryptLicRequest(s)
            lMachine=d['machine_infos']
            #mid=dMachine['mid']
            self.dHostCfg=d
            self.__showHost__()
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'dHostCfg:%s'%(vtLog.pformat(self.dHostCfg)),self)
        except:
            vtLog.vtLngTB(self.GetName())
        self.txtLic.SetValue('')
    def OnCbDelHostButton(self, event):
        event.Skip()
        try:
            if self.iSelCustomer<0:
                return
            sCustomerName=self.lstCustomer.GetItem(self.iSelCustomer,0).m_text
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'customer:%s;dCustomer:%s'%(sCustomerName,
                            vtLog.pformat(self.dCustomer)),self)
            if sCustomerName in self.dCustomer:
                dHosts=self.dCustomer[sCustomerName]['hosts']
            else:
                return
            iCount=self.lstInfo.GetItemCount()
            for idx in xrange(iCount-1,-1,-1):
                if self.lstInfo.GetItemState(wx.LIST_ITEM_SELECTED)!=0:
                    mid=self.lstInfo.GetItem(idx,1).m_text
                    del dHosts[mid]
                    self.lstInfo.DeleteItem(idx)
                self.SetModified(True,self.lstInfo)
                self.__showHosts__()
            
        except:
            vtLog.vtLngTB(self.GetName())
            
    def OnLstCustomerListColClick(self, event):
        event.Skip()

    def OnLstCustomerListItemDeselected(self, event):
        event.Skip()
        self.iSelCustomer=-1

    def OnLstCustomerListItemSelected(self, event):
        event.Skip()
        self.iSelCustomer=event.GetIndex()
        self.__showHosts__()

    def OnCbBuildRelButton(self, event):
        event.Skip()
        if self.node is None:
            return
        sCWD=os.getcwd()
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            nodeSetting=self.doc.getChild(self.doc.getRoot(),'settings')
            o=self.doc.GetReg('Settings')
            sLicDN=o.GetDir(nodeSetting)
            
            dInfos=self.objRegNode.GetDict(self.node)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'sLicDN:%s;dInfos:%s'%(sLicDN,vtLog.pformat(dInfos)),self)
            self.lblFiles.SetLabel('start build release ...')
            sDNCust=dInfos['customerID']
            mid=dInfos['machine']['mid']
            sID=fnUtil.replaceSuspectChars(sDNCust)
            sHostName=fnUtil.replaceSuspectChars(self.objRegNode.GetName(self.node))
            sMID=fnUtil.replaceSuspectChars(mid)
            sLicCustDN=os.path.join(sLicDN,sID,'_'.join([sHostName,sMID]))
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'sLicCustDN:%s;sMID:%s'%(sLicCustDN,sMID),self)
            
            if os.path.exists(sLicCustDN)==False:
                try:
                    os.makedirs(sLicCustDN)
                except:
                    pass
            if 1:
                sKeyFN=os.path.join(sLicCustDN,'pubkey.pub')
                pubprivkey=dInfos['machine']['pub_key']
                zip_filename=self.fbbRel.GetValue()
                inf=open(zip_filename,'rb')
                dn,fn=os.path.split(zip_filename)
                vifFN=os.path.join(sLicCustDN,fn)
                outf=open(vifFN[:-3]+'vidarc','wb')
                kEnc=vidCrypto.VidCryptoKey(pubprivkey,phrase=mid)
                kEnc.enc(inf,outf)
                outf.close()
                inf.close()
            self.lblFiles.SetLabel('finished build release.')
        except:
            vtLog.vtLngTB(self.GetName())
        os.chdir(sCWD)
    def OnCbBuildRelDNButton(self, event):
        event.Skip()
        if self.node is None:
            return
        sCWD=os.getcwd()
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            nodeSetting=self.doc.getChild(self.doc.getRoot(),'settings')
            o=self.doc.GetReg('Settings')
            sLicDN=o.GetDir(nodeSetting)
            sRelDN=self.objRegNode.GetReleaseDN(self.node)
            if len(sRelDN)==0:
                vtLog.PrintMsg('release directory not set, aborted.',self.widLogging)
                return
            
            dInfos=self.objRegNode.GetDict(self.node)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'sLicDN:%s;dInfos:%s'%(sLicDN,vtLog.pformat(dInfos)),self)
            self.lblFiles.SetLabel('start build release ...')
            sDNCust=dInfos['customerID']
            mid=dInfos['machine']['mid']
            sID=fnUtil.replaceSuspectChars(sDNCust)
            sHostName=fnUtil.replaceSuspectChars(self.objRegNode.GetName(self.node))
            sMID=fnUtil.replaceSuspectChars(mid)
            sLicCustDN=os.path.join(sLicDN,sID,'_'.join([sHostName,sMID]))
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'sLicCustDN:%s;sMID:%s'%(sLicCustDN,sMID),self)
            
            if os.path.exists(sLicCustDN)==False:
                try:
                    os.makedirs(sLicCustDN)
                except:
                    pass
            bOk=True
            if os.path.exists(sRelDN)==False:
                vtLog.PrintMsg('release directory(%s) does not exit not set, aborted.'%(sRelDN),self.widLogging)
                bOk=False
            if bOk==True:
                pubprivkey=dInfos['machine']['pub_key']
                kEnc=vidCrypto.VidCryptoKey(pubprivkey,phrase=mid)
                kEnc.importKeyPublic(pubprivkey)
                lFile=os.listdir(sRelDN)
                for sFN in lFile:
                    if sFN.endswith('.zip'):
                        vtLog.PrintMsg('build package %s file ...'%(sFN),self.widLogging)
                        self.lblFiles.SetLabel(u'recover package %s file ...'%(sFN))
                        sInFN=os.path.join(sRelDN,sFN)
                        sOutFN=os.path.join(sLicCustDN,''.join([sFN[:-3],'vidarc']))
                        inf=None
                        outf=None
                        try:
                            inf=open(sInFN,'rb')
                            outf=open(sOutFN,'wb')
                            kEnc.enc(inf,outf)
                        except:
                            vtLog.vtLngTB(self.GetName())
                        if inf is not None:
                            inf.close()
                        if outf is not None:
                            outf.close()
                self.lblFiles.SetLabel('finished build release.')
        except:
            vtLog.vtLngTB(self.GetName())
        os.chdir(sCWD)
    def OnCbRelCoverSrcPkgButton(self, event):
        event.Skip()
        if self.node is None:
            return
        sCWD=os.getcwd()
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            nodeSetting=self.doc.getChild(self.doc.getRoot(),'settings')
            o=self.doc.GetReg('Settings')
            sLicDN=o.GetDir(nodeSetting)
            
            sRelDN=self.objRegNode.GetReleaseDN(self.node)
            if len(sRelDN)==0:
                vtLog.PrintMsg('release directory not set, aborted.',self.widLogging)
                return
            dInfos=self.objRegNode.GetDict(self.node)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'sLicDN:%s;dInfos:%s'%(sLicDN,vtLog.pformat(dInfos)),self)
            self.lblFiles.SetLabel('start build release ...')
            sDNCust=dInfos['customerID']
            mid=dInfos['machine']['mid']
            sID=fnUtil.replaceSuspectChars(sDNCust)
            sHostName=fnUtil.replaceSuspectChars(self.objRegNode.GetName(self.node))
            sMID=fnUtil.replaceSuspectChars(mid)
            sLicCustDN=os.path.join(sLicDN,sID,'_'.join([sHostName,sMID]))
            bOk=True
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'sLicCustDN:%s;sMID:%s'%(sLicCustDN,sMID),self)
            if os.path.exists(sLicCustDN)==False:
                vtLog.PrintMsg('licence directory(%s) does not exit not set, aborted.'%(sLicCustDN),self.widLogging)
                bOk=False
            if os.path.exists(sRelDN)==False:
                vtLog.PrintMsg('release directory(%s) does not exit not set, aborted.'%(sRelDN),self.widLogging)
                bOk=False
            sKeyFN=os.path.join(sLicCustDN,'license.priv')
            if os.path.exists(sKeyFN)==True and bOk==True:
                f=open(sKeyFN,'rb')
                pubprivkey=f.read()
                f.close()
                vtLog.PrintMsg('import license file ...',self.widLogging)
                self.lblFiles.SetLabel(u'import license file ...')
                kEnc=vidCrypto.VidCryptoKey(pubprivkey,phrase=mid)
            else:
                vtLog.PrintMsg('license key file not found, aborted',self.widLogging)
            if bOk==True:
                lFile=os.listdir(sLicCustDN)
                for sFN in lFile:
                    if sFN.endswith('.vidarc'):
                        vtLog.PrintMsg('recover package %s file ...'%(sFN),self.widLogging)
                        self.lblFiles.SetLabel(u'recover package %s file ...'%(sFN))
                        sInFN=os.path.join(sLicCustDN,sFN)
                        sOutFN=os.path.join(sRelDN,''.join([sFN[:-6],'zip']))
                        inf=None
                        outf=None
                        try:
                            inf=open(sInFN,'rb')
                            outf=open(sOutFN,'wb')
                            kEnc.dec(inf,outf)
                        except:
                            vtLog.vtLngTB(self.GetName())
                        if inf is not None:
                            inf.close()
                        if outf is not None:
                            outf.close()
                self.lblFiles.SetLabel('finished build release.')
        except:
            vtLog.vtLngTB(self.GetName())
        os.chdir(sCWD)
