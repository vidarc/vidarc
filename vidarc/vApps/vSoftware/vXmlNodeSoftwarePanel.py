#Boa:FramePanel:vXmlNodeSoftwarePanel
#----------------------------------------------------------------------------
# Name:         vXmlNodeSoftwarePanel.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060328
# CVS-ID:       $Id: vXmlNodeSoftwarePanel.py,v 1.4 2008/03/30 12:41:56 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import vidarc.tool.input.vtInputAttrPanel
import wx.lib.filebrowsebutton
from vidarc.tool.net.vNetXmlWxGui import *
import vidarc.tool.xml.vtXmlDom as vtXmlDom
import vidarc.tool.xml.vtXmlDomConsumer as vtXmlDomConsumer

import sys

[wxID_VXMLNODESOFTWAREPANEL, wxID_VXMLNODESOFTWAREPANELDBBDIR, 
 wxID_VXMLNODESOFTWAREPANELLBLABBR, wxID_VXMLNODESOFTWAREPANELLBLNAME, 
 wxID_VXMLNODESOFTWAREPANELTXTABBR, wxID_VXMLNODESOFTWAREPANELTXTNAME, 
 wxID_VXMLNODESOFTWAREPANELVIATTR, 
] = [wx.NewId() for _init_ctrls in range(7)]

class vXmlNodeSoftwarePanel(wx.Panel,vtXmlDomConsumer.vtXmlDomConsumer):
    def _init_coll_bxsName_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblName, 1, border=4, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.txtName, 2, border=4, flag=wx.EXPAND | wx.LEFT)
        parent.AddWindow(self.lblAbbr, 1, border=4, flag=wx.EXPAND | wx.LEFT)
        parent.AddWindow(self.txtAbbr, 1, border=4, flag=wx.EXPAND | wx.LEFT)

    def _init_coll_fgsLog_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsName, 1, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 16), border=0, flag=0)
        parent.AddSizer(self.bxsDir, 1, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 16), border=0, flag=0)
        parent.AddWindow(self.viAttr, 1, border=0, flag=wx.EXPAND)

    def _init_coll_fgsLog_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(4)
        parent.AddGrowableCol(0)

    def _init_coll_bxsDir_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.dbbDir, 1, border=4, flag=wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsLog = wx.FlexGridSizer(cols=1, hgap=0, rows=5, vgap=0)

        self.bxsName = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsDir = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsLog_Growables(self.fgsLog)
        self._init_coll_fgsLog_Items(self.fgsLog)
        self._init_coll_bxsName_Items(self.bxsName)
        self._init_coll_bxsDir_Items(self.bxsDir)

        self.SetSizer(self.fgsLog)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VXMLNODESOFTWAREPANEL,
              name=u'vXmlNodeSoftwarePanel', parent=prnt, pos=wx.Point(0, 0),
              size=wx.Size(312, 207),
              style=wx.TAB_TRAVERSAL | wx.CLIP_CHILDREN | wx.FULL_REPAINT_ON_RESIZE)
        self.SetClientSize(wx.Size(304, 180))
        self.SetAutoLayout(True)

        self.lblName = wx.StaticText(id=wxID_VXMLNODESOFTWAREPANELLBLNAME,
              label=u'Name', name=u'lblName', parent=self, pos=wx.Point(4, 4),
              size=wx.Size(56, 29), style=wx.ALIGN_RIGHT)

        self.txtName = wx.TextCtrl(id=wxID_VXMLNODESOFTWAREPANELTXTNAME,
              name=u'txtName', parent=self, pos=wx.Point(64, 4),
              size=wx.Size(117, 29), style=0, value=u'')

        self.lblAbbr = wx.StaticText(id=wxID_VXMLNODESOFTWAREPANELLBLABBR,
              label=u'Abbrevation', name=u'lblAbbr', parent=self,
              pos=wx.Point(185, 4), size=wx.Size(56, 29), style=wx.ALIGN_RIGHT)

        self.txtAbbr = wx.TextCtrl(id=wxID_VXMLNODESOFTWAREPANELTXTABBR,
              name=u'txtAbbr', parent=self, pos=wx.Point(245, 4),
              size=wx.Size(56, 29), style=0, value=u'')

        self.dbbDir = wx.lib.filebrowsebutton.DirBrowseButton(buttonText='...',
              dialogTitle='Choose a directory',
              id=wxID_VXMLNODESOFTWAREPANELDBBDIR,
              labelText='Select a directory:', parent=self, pos=wx.Point(0, 49),
              size=wx.Size(304, 29), startDirectory='.', style=0)

        self.viAttr = vidarc.tool.input.vtInputAttrPanel.vtInputAttrPanel(id=wxID_VXMLNODESOFTWAREPANELVIATTR,
              name=u'viAttr', parent=self, pos=wx.Point(0, 94),
              size=wx.Size(304, 86), style=0)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style,name):
        self._init_ctrls(parent)
        
        vtXmlDomConsumer.vtXmlDomConsumer.__init__(self)
        
        self.bModified=False
        self.bAutoApply=False
        self.objRegNode=None
        self.bBlock=False
        
        self.lstMarkObjs=[]
        
        self.Move(pos)
        self.SetSize(size)
        self.SetName(name)
    def SetNetDocs(self,d):
        pass
    def SetRegNode(self,obj):
        self.objRegNode=obj
    def Lock(self,flag):
        if flag:
            pass
        else:
            pass
    def SetAutoApply(self,flag):
        self.bAutoApply=flag
    def __clearBlock__(self):
        self.bBlock=False
    def SetModified(self,state,obj=None):
        if state:
            if obj is None:
                for obj in self.lstMarkObjs:
                    f=obj.GetFont()
                    f.SetWeight(wx.FONTWEIGHT_BOLD)
                    obj.SetFont(f)
                    obj.Refresh()
            else:
                f=obj.GetFont()
                f.SetWeight(wx.FONTWEIGHT_BOLD)
                obj.SetFont(f)
                obj.Refresh()
        else:
            if obj is None:
                for obj in self.lstMarkObjs:
                    f=obj.GetFont()
                    f.SetWeight(wx.FONTWEIGHT_NORMAL)
                    obj.SetFont(f)
                    obj.Refresh()
                #self.viHum.__markModified__(False)
            else:
                f=obj.GetFont()
                f.SetWeight(wx.FONTWEIGHT_NORMAL)
                obj.SetFont(f)
                obj.Refresh()
        self.bModified=state    
    def GetModified(self):
        return self.bModified
    def __isModified__(self):
        if self.bModified:
            return True
        return False
    def Clear(self):
        self.bBlock=True
        self.SetModified(False)
        vtXmlDomConsumer.vtXmlDomConsumer.Clear(self)
        wx.CallAfter(self.__clearBlock__)
    def SetRegNode(self,o):
        pass
    def SetNetDocs(self,d):
        pass
    def SetDoc(self,doc,bNet):
        vtXmlDomConsumer.vtXmlDomConsumer.SetDoc(self,doc)
        self.viAttr.SetDoc(doc,bNet)
    def SetNode(self,node):
        #if self.bModified==True:
        #    if self.bAutoApply:
        #        self.GetNode(self.node)
        #    else:
                # ask
        #        dlg=wx.MessageDialog(self,u'Do you to apply modified data?' ,
        #                    _(u'vLog Info'),
        #                    wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
        #        if dlg.ShowModal()==wx.ID_YES:
        #            self.GetNode(self.node)
        self.bBlock=True
        self.SetModified(False)
        vtXmlDomConsumer.vtXmlDomConsumer.SetNode(self,node)
        self.txtName.SetValue(self.doc.getNodeText(node,'name'))
        self.txtAbbr.SetValue(self.doc.getNodeText(node,'abbreviation'))
        self.dbbDir.SetValue(self.doc.getNodeText(node,'dn'))
        self.viAttr.SetNode(node)
        
        wx.CallAfter(self.__clearBlock__)
    def GetNode(self,node=None):
        if node is None:
            node=self.node
        if node is not None:
            pass
        self.SetModified(False)
        self.doc.setNodeText(node,'name',self.txtName.GetValue())
        self.doc.setNodeText(node,'abbreviation',self.txtAbbr.GetValue())
        self.doc.setNodeText(node,'dn',self.dbbDir.GetValue())
        self.viAttr.GetNode(node)
        
        pass
    def Apply(self,bDoApply=False):
        #self.SetModified(False)
        return False
    def Cancel(self):
        self.SetModified(False)
