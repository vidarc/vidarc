#Boa:FramePanel:vgpDraw
#----------------------------------------------------------------------------
# Name:         vDrawFlowChartCanvasOGL.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vDrawFlowChartCanvasOGL.py,v 1.2 2008/02/02 16:10:53 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.ogl as ogl
import string,os,sys

import vidarc.vApps.vDraw.latex as latex
import vidarc.tool.xml.vtXmlDom as vtXmlDom
import vidarc.vApps.vPrjEng.Hierarchy as Hierarchy
import vidarc.tool.log.vtLog as vtLog

from vidarc.vApps.vDraw.voglFlowChartElement import voglFlowChartElement
from vidarc.vApps.vDraw.voglFlowChartData import voglFlowChartData
from vidarc.vApps.vDraw.voglFlowChartDataStored import voglFlowChartDataStored
from vidarc.vApps.vDraw.voglFlowChartDataInternal import voglFlowChartDataInternal
from vidarc.vApps.vDraw.voglFlowChartDataSeqAccess import voglFlowChartDataSeqAccess
from vidarc.vApps.vDraw.voglFlowChartDataDirectAccess import voglFlowChartDataDirectAccess
from vidarc.vApps.vDraw.voglFlowChartProcBasic import voglFlowChartProcBasic
from vidarc.vApps.vDraw.voglFlowChartProcPreDef import voglFlowChartProcPredefined
from vidarc.vApps.vDraw.voglFlowChartProcManual import voglFlowChartProcManual
from vidarc.vApps.vDraw.voglFlowChartProcPrep import voglFlowChartProcPreparation
from vidarc.vApps.vDraw.voglFlowChartDesicion import voglFlowChartDesicion
from vidarc.vApps.vDraw.voglFlowChartLoopStart import voglFlowChartLoopStart
from vidarc.vApps.vDraw.voglFlowChartLoopEnd import voglFlowChartLoopEnd
from vidarc.vApps.vDraw.voglFlowChartControlTransfer import voglFlowChartControlTransfer
from vidarc.vApps.vDraw.voglFlowChartCommLink import voglFlowChartCommLink
from vidarc.vApps.vDraw.voglFlowChartTerminator import voglFlowChartTerminator
from vidarc.vApps.vDraw.voglFlowChartConnector import voglFlowChartConnector
from vidarc.vApps.vDraw.voglFlowChartDisplay import voglFlowChartDisplay
from vidarc.vApps.vDraw.voglFlowChartPaperTape import voglFlowChartPaperTape
from vidarc.vApps.vDraw.voglFlowChartDocument import voglFlowChartDocument
from vidarc.vApps.vDraw.voglFlowChartInputManual import voglFlowChartInputManual
from vidarc.vApps.vDraw.voglFlowChartCard import voglFlowChartCard
from vidarc.vApps.vDraw.voglFlowChartAnnotation import voglFlowChartAnnotation

import images_iso5807
#----------------------------------------------------------------------
        

class DrawingEvtHandler(ogl.ShapeEvtHandler):
    def __init__(self):
        ogl.ShapeEvtHandler.__init__(self)
        
    def UpdateStatusBar(self, shape):
        x, y = shape.GetX(), shape.GetY()
        width, height = shape.GetBoundingBoxMax()
        

    def OnLeftClick(self, x, y, keys=0, attachment=0):
        shape = self.GetShape()
        canvas = shape.GetCanvas()
        dc = wx.ClientDC(canvas)
        canvas.PrepareDC(dc)

        if shape.Selected():
            shape.Select(False, dc)
            canvas.Redraw(dc)
        else:
            redraw = False
            shapeList = canvas.GetDiagram().GetShapeList()
            toUnselect = []

            for s in shapeList:
                if s.Selected():
                    # If we unselect it now then some of the objects in
                    # shapeList will become invalid (the control points are
                    # shapes too!) and bad things will happen...
                    toUnselect.append(s)
            shape.Select(True, dc)
            if toUnselect:
                for s in toUnselect:
                    s.Select(False, dc)
            if isinstance(shape,voglFlowChartElement):
                if keys==ogl.KEY_CTRL:
                    canvas.LinkSteps(shape)
            canvas.SetSelectStep(shape)
            canvas.Redraw(dc)
        self.UpdateStatusBar(shape)
    def OnStartDragLeft(self, x, y, keys=0, attachment=0):
        shape = self.GetShape()
        ogl.ShapeEvtHandler.OnEndDragLeft(self, x, y, keys, attachment)
        
    def OnEndDragLeft(self, x, y, keys=0, attachment=0):
        shape = self.GetShape()
        x,y=shape.GetCanvas().GetDiagram().Snap(x,y)
        ogl.ShapeEvtHandler.OnEndDragLeft(self, x, y, keys, attachment)
        if not shape.Selected():
            self.OnLeftClick(x, y, keys, attachment)
        self.UpdateStatusBar(shape)
    def OnSizingEndDragLeft(self, pt, x, y, keys, attch):
        ogl.ShapeEvtHandler.OnSizingEndDragLeft(self, pt, x, y, keys, attch)
        self.UpdateStatusBar(self.GetShape())
    def OnMovePost(self, dc, x, y, oldX, oldY, display):
        #print x,y,oldX,oldY
        shape=self.GetShape()
        canvas = shape.GetCanvas()
        dc = wx.ClientDC(canvas)
        canvas.PrepareDC(dc)
        #canvas.Erase(dc)
        ogl.ShapeEvtHandler.OnMovePost(self, dc, x, y, oldX, oldY, display)
        self.UpdateStatusBar(self.GetShape())
        if isinstance(self.GetShape(),voglFlowChartElement):
            shape=self.GetShape()
            shape.UpdateLinksRec(dc)
            #canvas = shape.GetCanvas()
            #dc = wx.ClientDC(canvas)
            #canvas.PrepareDC(dc)
            #canvas.GetDiagram().Clear(dc)
            #canvas.Erase(dc)
            #canvas.Redraw(dc)
    def OnRightClick(self, *dontcare):
        #print "evt handler right click"
        #self.log.WriteText("%s\n" % self.GetShape())
        shape=self.GetShape()
        canvas = shape.GetCanvas()
        dc = wx.ClientDC(canvas)
        canvas.PrepareDC(dc)
        if shape.DoEdit()>0:
            shape.__update__(dc)
            #if isinstance(shape,voglFlowChartElement):
            #    shape.FormatText(dc,'%s'%shape.info)
            canvas.GetDiagram().Clear(dc)
            canvas.Redraw(dc)
        pass

class voglSFCdiagram(ogl.Diagram):
    def __init__(self):
        ogl.Diagram(self)
    def Snap(self,x,y):
        """'Snaps' the coordinate to the nearest grid position, if
        snap-to-grid is on."""
        if self._snapToGrid:
            
            return self._gridSpacing * int(x / self._gridSpacing + 0.5), self._gridSpacing * int(y / self._gridSpacing + 0.5)
        return x, y
    
#----------------------------------------------------------------------

class vDrawFlowChartCanvasOGL(ogl.ShapeCanvas):
    def __init__(self, parent=None,id = -1, pos = wx.DefaultPosition, size = wx.DefaultSize, 
                    style = wx.BORDER, name = "ShapeCanvas",
                    gridSize=10,FCtype=''):
        ogl.ShapeCanvas.__init__(self, parent,id, pos, size, style, name)
        self.parent=parent
        self.FCtype=FCtype
        self.dlgBrowse=None
        self.dlgStepValBrowse=None
        self.dlgActionBrowse=None
        self.dlgActionValBrowse=None
    
        maxWidth  = 650
        maxHeight = 800
        #self.SetScrollbars(20, 20, maxWidth/20, maxHeight/20)
        self.SetScrollbars(1, 1, maxWidth, maxHeight)
        
        #self.log = log
        #self.frame = frame
        self.SetBackgroundColour("LIGHT GRAY")
        #self.SetBackgroundColour(wx.WHITE)
        self.diagram = ogl.Diagram()
        self.SetDiagram(self.diagram)
        self.diagram.SetCanvas(self)
        self.shapes = []
        self.save_gdi = []
        
        self.selStep=None
        self.dlgFlowChartElemt=None
        self.dlgFlowChartDesic=None
        self.dlgFlowChartAnnot=None
        self.dlgFlowChartConnector=None
        self.dlgSFCstepActions=None
        self.dlgSFCstepTransition=None
        self.relNode=None
        
        self.diagram.SetGridSpacing(gridSize)
        self.diagram.SetSnapToGrid(True)
        
        self.stepNrs=[]
        self.steps=[]
        self.stepIDs={}
        #rRectBrush = wx.Brush("MEDIUM TURQUOISE", wx.SOLID)
        #dsBrush = wx.Brush("WHEAT", wx.SOLID)
        #self.bModified=False
    def __setModified__(self,state):
        self.parent.__setModified__(state)
    def Clear(self):
        self.relNode=None
        unit=self.GetScrollPixelsPerUnit()
        size=self.GetVirtualSize()
        self.SetScrollbars(unit[0], unit[1], size[0], size[1])
        dc = wx.ClientDC(self)
        self.PrepareDC(dc)
        for s in self.steps:
            s.Delete()
        self.diagram.DeleteAllShapes()
        self.shapes=[]
        self.steps=[]
        self.stepNrs=[]
        self.stepIDs={}
        self.GetDiagram().Clear(dc)
        self.Redraw(dc)
        self.__setModified__(False)
    def SetModified(self,state=True):
        self.__setModified__(state)
    def GetStepNrs(self):
        return self.stepNrs
    def GetSteps(self):
        return self.steps
    def UpdateAxis(self):
        pass
    def AddShapeSimple(self, shape, x, y):
        x,y=self.GetDiagram().Snap(x,y)
        if isinstance(shape, ogl.CompositeShape):
            dc = wx.ClientDC(self)
            self.PrepareDC(dc)
            shape.Move(dc, x, y)
        else:
            shape.SetDraggable(False, False)
        shape.SetCanvas(self)
        shape.SetX(x)
        shape.SetY(y)
        self.diagram.AddShape(shape)
        shape.Show(True)

        evthandler = DrawingEvtHandler()
        evthandler.SetShape(shape)
        evthandler.SetPreviousHandler(shape.GetEventHandler())
        shape.SetEventHandler(evthandler)

        self.shapes.append(shape)
        return shape
    def MyAddShape(self, shape, x, y):
        # Composites have to be moved for all children to get in place
        if isinstance(shape, ogl.CompositeShape):
            dc = wx.ClientDC(self)
            self.PrepareDC(dc)
            shape.Move(dc, x, y)
        else:
            shape.SetDraggable(True, True)
        shape.SetCanvas(self)
        shape.SetX(x)
        shape.SetY(y)
        #if pen:    shape.SetPen(pen)
        #if brush:  shape.SetBrush(brush)
        #if text:
        #    for line in text.split('\n'):
        #        shape.AddText(line)
        #shape.SetShadowMode(ogl.SHADOW_RIGHT)
        self.diagram.AddShape(shape)
        shape.Show(True)

        evthandler = DrawingEvtHandler()
        evthandler.SetShape(shape)
        evthandler.SetPreviousHandler(shape.GetEventHandler())
        shape.SetEventHandler(evthandler)

        self.shapes.append(shape)
        return shape


    def OnRightClick(self,x,y,keys):
        #print "canvas right"
        if not hasattr(self,'popupIdAddData'):
            self.popupIdFCdata=wx.NewId()
            self.popupIdFCprog=wx.NewId()
            self.popupIdFCsys=wx.NewId()
            self.popupIdFCnet=wx.NewId()
            self.popupIdFCres=wx.NewId()
            self.popupIdAddData=wx.NewId()
            self.popupIdAddDataStored=wx.NewId()
            self.popupIdAddDataInternal=wx.NewId()
            self.popupIdAddDataDirectAccess=wx.NewId()
            self.popupIdAddDataSeqAccess=wx.NewId()
            self.popupIdDel=wx.NewId()
            self.popupIdAddProcBasic=wx.NewId()
            self.popupIdAddProcPreDef=wx.NewId()
            self.popupIdAddProcMan=wx.NewId()
            self.popupIdAddProcPrep=wx.NewId()
            self.popupIdAddDesicion=wx.NewId()
            self.popupIdAddLoopStart=wx.NewId()
            self.popupIdAddLoopEnd=wx.NewId()
            self.popupIdAddControlTransfer=wx.NewId()
            self.popupIdAddCommLink=wx.NewId()
            self.popupIdAddTerminator=wx.NewId()
            self.popupIdAddConnector=wx.NewId()
            self.popupIdAddDisplay=wx.NewId()
            self.popupIdAddPaperTape=wx.NewId()
            self.popupIdAddDocument=wx.NewId()
            self.popupIdAddInputManual=wx.NewId()
            self.popupIdAddCard=wx.NewId()
            self.popupIdAddAnnotation=wx.NewId()
            wx.EVT_MENU(self,self.popupIdFCdata,self.OnFCdata)
            wx.EVT_MENU(self,self.popupIdFCprog,self.OnFCprog)
            wx.EVT_MENU(self,self.popupIdFCsys,self.OnFCsys)
            wx.EVT_MENU(self,self.popupIdFCnet,self.OnFCnet)
            wx.EVT_MENU(self,self.popupIdFCres,self.OnFCres)
            wx.EVT_MENU(self,self.popupIdAddData,self.OnAddData)
            wx.EVT_MENU(self,self.popupIdAddDataStored,self.OnAddDataStored)
            wx.EVT_MENU(self,self.popupIdAddDataInternal,self.OnAddDataInternal)
            wx.EVT_MENU(self,self.popupIdAddDataSeqAccess,self.OnAddDataSeqAccess)
            wx.EVT_MENU(self,self.popupIdAddProcBasic,self.OnAddProcBasic)
            wx.EVT_MENU(self,self.popupIdAddProcPreDef,self.OnAddProcPreDef)
            wx.EVT_MENU(self,self.popupIdAddProcMan,self.OnAddProcMan)
            wx.EVT_MENU(self,self.popupIdAddProcPrep,self.OnAddProcPrep)
            wx.EVT_MENU(self,self.popupIdAddDesicion,self.OnAddDesicion)
            wx.EVT_MENU(self,self.popupIdAddLoopStart,self.OnAddLoopStart)
            wx.EVT_MENU(self,self.popupIdAddLoopEnd,self.OnAddLoopEnd)
            wx.EVT_MENU(self,self.popupIdAddControlTransfer,self.OnAddControlTransfer)
            wx.EVT_MENU(self,self.popupIdAddCommLink,self.OnAddCommLink)
            wx.EVT_MENU(self,self.popupIdAddTerminator,self.OnAddTerminator)
            wx.EVT_MENU(self,self.popupIdAddConnector,self.OnAddConnector)
            wx.EVT_MENU(self,self.popupIdAddDisplay,self.OnAddDisplay)
            wx.EVT_MENU(self,self.popupIdAddPaperTape,self.OnAddPaperTape)
            wx.EVT_MENU(self,self.popupIdAddDocument,self.OnAddDocument)
            wx.EVT_MENU(self,self.popupIdAddDataDirectAccess,self.OnAddDataDirectAccess)
            wx.EVT_MENU(self,self.popupIdAddInputManual,self.OnAddInputManual)
            wx.EVT_MENU(self,self.popupIdAddCard,self.OnAddCard)
            wx.EVT_MENU(self,self.popupIdAddAnnotation,self.OnAddAnnotation)
            wx.EVT_MENU(self,self.popupIdDel,self.OnDel)
        menu=wx.Menu()
        if len(self.steps)==0:
            menu.Append(self.popupIdFCdata,kind=wx.ITEM_CHECK, text=u'FC data')
            menu.Append(self.popupIdFCprog,kind=wx.ITEM_CHECK, text=u'FC program')
            menu.Append(self.popupIdFCsys,kind=wx.ITEM_CHECK, text=u'FC system')
            menu.Append(self.popupIdFCnet,kind=wx.ITEM_CHECK, text=u'FC network')
            menu.Append(self.popupIdFCres,kind=wx.ITEM_CHECK, text=u'FC resources')
            mnItems=menu.GetMenuItems()
            if self.FCtype=='Data':
                mnItems[0].Check(True)
            elif self.FCtype=='Prog':
                mnItems[1].Check(True)
            elif self.FCtype=='Sys':
                mnItems[2].Check(True)
            elif self.FCtype=='Net':
                mnItems[3].Check(True)
            elif self.FCtype=='Res':
                mnItems[4].Check(True)
        else:
            if self.FCtype=='Data':
                menu.Append(self.popupIdFCdata,kind=wx.ITEM_CHECK, text=u'FC data')
            elif self.FCtype=='Prog':
                menu.Append(self.popupIdFCprog,kind=wx.ITEM_CHECK, text=u'FC program')
            elif self.FCtype=='Sys':
                menu.Append(self.popupIdFCsys,kind=wx.ITEM_CHECK, text=u'FC system')
            elif self.FCtype=='Net':
                menu.Append(self.popupIdFCnet,kind=wx.ITEM_CHECK, text=u'FC network')
            elif self.FCtype=='Res':
                menu.Append(self.popupIdFCres,kind=wx.ITEM_CHECK, text=u'FC resources')
            mnItems=menu.GetMenuItems()
            mnItems[0].Check(True)
            mnItems[0].Enable(False)
        menu.AppendSeparator()
        
        if len(self.steps)>0 or len(self.FCtype)>0:
            if self.FCtype in ['Data','Prog','Sys','Net','Res']:
                #menu.Append(self.popupIdAddProcBasic,'processing basic')
                mnIt=wx.MenuItem(menu,self.popupIdAddProcBasic,'processing basic')
                mnIt.SetBitmap(images_iso5807.getProcBitmap())
                menu.AppendItem(mnIt)
            if self.FCtype in ['Prog','Sys','Net']:
                mnIt=wx.MenuItem(menu,self.popupIdAddProcPreDef,'processing predefined')
                mnIt.SetBitmap(images_iso5807.getProcPredefBitmap())
                menu.AppendItem(mnIt)
            if self.FCtype in ['Data','Sys','Net']:
                mnIt=wx.MenuItem(menu,self.popupIdAddProcMan,'processing manual')
                mnIt.SetBitmap(images_iso5807.getProcManBitmap())
                menu.AppendItem(mnIt)
            if self.FCtype in ['Data','Prog','Sys','Net']:
                mnIt=wx.MenuItem(menu,self.popupIdAddProcPrep,'processing preparation')
                mnIt.SetBitmap(images_iso5807.getProcPrepBitmap())
                menu.AppendItem(mnIt)
            if self.FCtype in ['Prog','Sys']:
                mnIt=wx.MenuItem(menu,self.popupIdAddDesicion,'desicion')
                mnIt.SetBitmap(images_iso5807.getDescBitmap())
                menu.AppendItem(mnIt)
            if self.FCtype in ['Prog','Sys']:
                mnIt=wx.MenuItem(menu,self.popupIdAddLoopStart,'loop begin')
                mnIt.SetBitmap(images_iso5807.getLoopStartBitmap())
                menu.AppendItem(mnIt)
            if self.FCtype in ['Prog','Sys']:
                mnIt=wx.MenuItem(menu,self.popupIdAddLoopEnd,'loop end')
                mnIt.SetBitmap(images_iso5807.getLoopEndBitmap())
                menu.AppendItem(mnIt)
            if self.FCtype in ['Net']:
                mnIt=wx.MenuItem(menu,self.popupIdAddControlTransfer,'control transfer')
                mnIt.SetBitmap(images_iso5807.getProcPredefBitmap())
                menu.AppendItem(mnIt)
            if self.FCtype in ['Data','Sys','Net','Res']:
                mnIt=wx.MenuItem(menu,self.popupIdAddCommLink,'communication link')
                mnIt.SetBitmap(images_iso5807.getCommLinkBitmap())
                menu.AppendItem(mnIt)
            if self.FCtype in ['Data','Prog','Sys']:
                mnIt=wx.MenuItem(menu,self.popupIdAddTerminator,'terminator')
                mnIt.SetBitmap(images_iso5807.getTermBitmap())
                menu.AppendItem(mnIt)
            if self.FCtype in ['Data','Prog','Sys','Net','Res']:
                mnIt=wx.MenuItem(menu,self.popupIdAddConnector,'connector')
                mnIt.SetBitmap(images_iso5807.getConnectBitmap())
                menu.AppendItem(mnIt)
            if self.FCtype in ['Data','Sys','Net','Res']:
                mnIt=wx.MenuItem(menu,self.popupIdAddDisplay,'display')
                mnIt.SetBitmap(images_iso5807.getDisplayBitmap())
                menu.AppendItem(mnIt)
            if self.FCtype in ['Data','Sys','Net','Res']:
                mnIt=wx.MenuItem(menu,self.popupIdAddPaperTape,'paper tape')
                mnIt.SetBitmap(images_iso5807.getPaperTapeBitmap())
                menu.AppendItem(mnIt)
            if self.FCtype in ['Data','Sys','Net','Res']:
                mnIt=wx.MenuItem(menu,self.popupIdAddDocument,'document')
                mnIt.SetBitmap(images_iso5807.getDocBitmap())
                menu.AppendItem(mnIt)
            if self.FCtype in ['Data','Sys','Net','Res']:
                mnIt=wx.MenuItem(menu,self.popupIdAddInputManual,'input manual')
                mnIt.SetBitmap(images_iso5807.getinManBitmap())
                menu.AppendItem(mnIt)
            if self.FCtype in ['Data','Sys','Net','Res']:
                mnIt=wx.MenuItem(menu,self.popupIdAddCard,'card')
                mnIt.SetBitmap(images_iso5807.getCardBitmap())
                menu.AppendItem(mnIt)
            if self.FCtype in ['Data','Prog','Sys','Net','Res']:
                mnIt=wx.MenuItem(menu,self.popupIdAddData,'data')
                mnIt.SetBitmap(images_iso5807.getDataBitmap())
                menu.AppendItem(mnIt)
            if self.FCtype in ['Data','Sys','Net','Res']:
                mnIt=wx.MenuItem(menu,self.popupIdAddDataInternal,'data internal')
                mnIt.SetBitmap(images_iso5807.getDataInternalBitmap())
                menu.AppendItem(mnIt)
            if self.FCtype in ['Data','Sys','Net','Res']:
                mnIt=wx.MenuItem(menu,self.popupIdAddDataStored,'data stored')
                mnIt.SetBitmap(images_iso5807.getDataStoredBitmap())
                menu.AppendItem(mnIt)
            if self.FCtype in ['Data','Sys','Net','Res']:
                mnIt=wx.MenuItem(menu,self.popupIdAddDataSeqAccess,'data sequential access')
                mnIt.SetBitmap(images_iso5807.getDataSeqBitmap())
                menu.AppendItem(mnIt)
            if self.FCtype in ['Data','Sys','Net','Res']:
                mnIt=wx.MenuItem(menu,self.popupIdAddDataDirectAccess,'data direct access')
                mnIt.SetBitmap(images_iso5807.getDataDirectBitmap())
                menu.AppendItem(mnIt)
            if self.selStep is not None:
                if self.selStep.HasAnnotation()==False:
                    if self.FCtype in ['Data','Prog','Sys','Net','Res']:
                        mnIt=wx.MenuItem(menu,self.popupIdAddAnnotation,'annotation')
                        mnIt.SetBitmap(images_iso5807.getAnnotationBitmap())
                        menu.AppendItem(mnIt)
        menu.AppendSeparator()
        menu.Append(self.popupIdDel,'Del')
        
        
        x,y=self.GetDiagram().Snap(x,y)
        self.popupPos=(x,y)
        
        p=self.GetViewStart()
        u=self.GetScrollPixelsPerUnit()
        self.PopupMenu(menu,wx.Point(x-p[0]*u[0], y-p[1]*u[1]))
        
        menu.Destroy()
    def OnFCdata(self,evt):
        self.FCtype='Data'
    def OnFCprog(self,evt):
        self.FCtype='Prog'
    def OnFCsys(self,evt):
        self.FCtype='Sys'
    def OnFCnet(self,evt):
        self.FCtype='Net'
    def OnFCres(self,evt):
        self.FCtype='Res'
    
    def __add__(self,objTmp):
        if hasattr(objTmp,'is'+self.FCtype):
            if getattr(objTmp,'is'+self.FCtype)()==False:
                return
        else:
            return
        dc = wx.ClientDC(self)
        self.PrepareDC(dc)
        
        ba=objTmp.GetBoundingBoxMax()
        x=self.popupPos[0]+ba[0]/2.0
        y=self.popupPos[1]+ba[1]/2.0
        x,y=self.GetDiagram().Snap(x,y)
        
        ds2 = self.AddShapeSimple(objTmp,x, y)
        #objTmp.SetCanvas(self)
        #objTmp.SetX(x)
        #objTmp.SetY(y)
        #self.diagram.AddShape(objTmp)
        #objTmp.Show(True)
        self.steps.append(ds2)
        
        self.Redraw(dc)
        self.SetModified()
    def OnAddData(self,evt):
        objTmp=voglFlowChartData(self,'','','???',90, 60)
        self.__add__(objTmp)
    def OnAddDataStored(self,evt):
        objTmp=voglFlowChartDataStored(self,'','','???',90, 60)
        self.__add__(objTmp)
    def OnAddDataInternal(self,evt):
        objTmp=voglFlowChartDataInternal(self,'','','???',90, 60)
        self.__add__(objTmp)
    def OnAddDataSeqAccess(self,evt):
        objTmp=voglFlowChartDataSeqAccess(self,'','','???',90, 60)
        self.__add__(objTmp)
    def OnAddProcBasic(self,evt):
        objTmp=voglFlowChartProcBasic(self,'','','???',90, 60)
        self.__add__(objTmp)
    def OnAddProcPreDef(self,evt):
        objTmp=voglFlowChartProcPredefined(self,'','','???',90, 60)
        self.__add__(objTmp)
    def OnAddProcMan(self,evt):
        objTmp=voglFlowChartProcManual(self,'','','???',90, 60)
        self.__add__(objTmp)
    def OnAddProcPrep(self,evt):
        objTmp=voglFlowChartProcPreparation(self,'','','???',90, 60)
        self.__add__(objTmp)
    def OnAddDesicion(self,evt):
        objTmp=voglFlowChartDesicion(self,'','','???',90, 60)
        self.__add__(objTmp)
    def OnAddLoopStart(self,evt):
        objTmp=voglFlowChartLoopStart(self,'','','???',90, 60)
        self.__add__(objTmp)
    def OnAddLoopEnd(self,evt):
        objTmp=voglFlowChartLoopEnd(self,'','','???',90, 60)
        self.__add__(objTmp)
    def OnAddControlTransfer(self,evt):
        objTmp=voglFlowChartControlTransfer(self,'','','???',90, 60)
        self.__add__(objTmp)
    def OnAddCommLink(self,evt):
        objTmp=voglFlowChartCommLink(self,'','','???',90, 60)
        self.__add__(objTmp)
    def OnAddTerminator(self,evt):
        objTmp=voglFlowChartTerminator(self,'','','???',90, 60)
        self.__add__(objTmp)
    def OnAddConnector(self,evt):
        objTmp=voglFlowChartConnector(self,'','','???',90, 60)
        self.__add__(objTmp)
    def OnAddDisplay(self,evt):
        objTmp=voglFlowChartDisplay(self,'','','???',90, 60)
        self.__add__(objTmp)
    def OnAddPaperTape(self,evt):
        objTmp=voglFlowChartPaperTape(self,'','','???',90, 60)
        self.__add__(objTmp)
    def OnAddDocument(self,evt):
        objTmp=voglFlowChartDocument(self,'','','???',90, 60)
        self.__add__(objTmp)
    def OnAddDataDirectAccess(self,evt):
        objTmp=voglFlowChartDataDirectAccess(self,'','','???',90, 60)
        self.__add__(objTmp)
    def OnAddInputManual(self,evt):
        objTmp=voglFlowChartInputManual(self,'','','???',90, 60)
        self.__add__(objTmp)
    def OnAddCard(self,evt):
        objTmp=voglFlowChartCard(self,'','','???',90, 60)
        self.__add__(objTmp)
    def OnAddAnnotation(self,evt):
        objTmp=voglFlowChartAnnotation(self,'','','???',90, 60)
        self.__add__(objTmp)
        self.selStep.AddLink(objTmp)
        dc = wx.ClientDC(self)
        self.PrepareDC(dc)
        self.selStep.UpdateLinks(dc)
    def LinkSteps(self,newstep):
        if isinstance(newstep,voglFlowChartElement)<1:
            return
        if self.selStep is not None:
            if isinstance(self.selStep,voglFlowChartElement):
                idxFrom=self.steps.index(self.selStep)
                idxTo=self.steps.index(newstep)
                self.selStep.AddLink(newstep)
                self.SetModified()
    def SetSelectStep(self,step):
        if step is not None:
            if step.Selected()>0:
                self.selStep=step
            else:
                self.selStep=None
        else:
            self.selStep=None
    def OnDel(self,evt):
        if self.selStep is None:
            return
        dc = wx.ClientDC(self)
        self.PrepareDC(dc)
        shapeList = self.GetDiagram().GetShapeList()
        for s in shapeList:
            if s.Selected():
                s.Select(False, dc)
        self.selStep.Move(dc, -300, -300)
        self.RemoveShape(self.selStep)
        self.selStep.Delete()
        idx=self.steps.index(self.selStep)
        self.steps.remove(self.selStep)
        self.shapes.remove(self.selStep)
        self.selStep=None
        self.SetModified()
    def SetBrowseDlg(self,dlg):
        self.dlgBrowse=dlg
    #def SetBrowseDesicDlg(self,dlg):
    #    self.dlgFlowChartDesic=dlg
    #def SetBrowseDlg(self,dlg):
    #    self.dlgFlowChartAnnot=dlg
    #def SetActionBrowseValDlg(self,dlg):
    #    self.dlgFlowChartConnector=dlg
        
    def GetBrowseDlg(self):
        return self.dlgBrowse
    def GetStepBrowseValDlg(self):
        return self.dlgStepValBrowse
    def GetActionBrowseDlg(self):
        return self.dlgActionBrowse
    def GetActionBrowseValDlg(self):
        return self.dlgActionValBrowse
        
    def __replace__(self,strOld,strNew):
        i=string.find(strOld,'<')
        if i>=0:
            j=string.find(strOld,'>',i+1)
            if j>0:
                return strOld[:i+1]+strNew+strOld[j:]
        return strOld
    def SetNodeStep(self,node):
        id=''
        nr=''
        name=''
        parallel=0
        if node is None:
            return
        id=self.doc.getAttribute(node,'id')
        x=self.doc.getNodeText(node,'x')
        y=self.doc.getNodeText(node,'y')
        type=self.doc.getNodeText(node,'type')
        sid=self.doc.getNodeText(node,'id')
        desc=self.doc.getNodeText(node,'desc')
        info=self.doc.getNodeText(node,'info')
        text=self.doc.getNodeText(node,'text')
        IDtext=self.doc.getNodeText(node,'IDtext')
        #try:
        #    self.parallel=int(vtXmlDomTree.getNodeText(node,'parallel'))
        #except:
        #    self.parallel=int(vtXmlDomTree.getNodeText(node,'nr'))
        x,y=self.GetDiagram().Snap(float(x), float(y))
        objTmp=None
        if type=='data':
            objTmp=voglFlowChartData(self,sid,desc,info,90, 60)
        elif type=='datastored':
            objTmp=voglFlowChartDataStored(self,sid,desc,info,90, 60)
        elif type=='datainternal':
            objTmp=voglFlowChartDataInternal(self,sid,desc,info,90, 60)
        elif type=='dataseqaccess':
            objTmp=voglFlowChartDataSeqAccess(self,sid,desc,info,90, 60)
        elif type=='procbasic':
            objTmp=voglFlowChartProcBasic(self,sid,desc,info,90, 60)
        elif type=='procpredef':
            objTmp=voglFlowChartProcPredefined(self,sid,desc,info,90, 60)
        elif type=='procman':
            objTmp=voglFlowChartProcManual(self,sid,desc,info,90, 60)
        elif type=='procprep':
            objTmp=voglFlowChartProcPreparation(self,sid,desc,info,90, 60)
        elif type=='descicion':
            objTmp=voglFlowChartDesicion(self,sid,desc,info,90, 60)
        elif type=='loopstart':
            objTmp=voglFlowChartLoopStart(self,sid,desc,info,90, 60)
        elif type=='loopend':
            objTmp=voglFlowChartLoopEnd(self,sid,desc,info,90, 60)
        elif type=='controltrans':
            objTmp=voglFlowChartControlTransfer(self,sid,desc,info,90, 60)
        elif type=='link':
            objTmp=voglFlowChartCommLink(self,sid,desc,info,90, 60)
        elif type=='termin':
            objTmp=voglFlowChartTerminator(self,sid,desc,info,90, 60)
        elif type=='conn':
            objTmp=voglFlowChartConnector(self,sid,desc,info,90, 60)
            connect=self.doc.getNodeText(node,'connect')
            IDconnect=self.doc.getNodeText(node,'IDconnect')
            objTmp.SetConnect(connect)
            objTmp.SetIDConnect(IDconnect)
        elif type=='disp':
            objTmp=voglFlowChartDisplay(self,sid,desc,info,90, 60)
        elif type=='paper':
            objTmp=voglFlowChartPaperTape(self,sid,desc,info,90, 60)
        elif type=='doc':
            objTmp=voglFlowChartDocument(self,sid,desc,info,90, 60)
        elif type=='datadirect':
            objTmp=voglFlowChartDataDirectAccess(self,sid,desc,info,90, 60)
        elif type=='inman':
            objTmp=voglFlowChartInputManual(self,sid,desc,info,90, 60)
        elif type=='card':
            objTmp=voglFlowChartCard(self,sid,desc,info,90, 60)
        elif type=='annotation':
            objTmp=voglFlowChartAnnotation(self,sid,desc,info,90, 60)
        else:
            objTmp=None
        if objTmp is not None:
            objTmp.SetText(text)
            objTmp.SetIDText(IDtext)
            objTmp.SetID(id)
            step = self.AddShapeSimple(objTmp, x, y)
        self.steps.append(step)
        self.stepIDs[id]=step
        pass
    def GetNodeStep(self,step,node):
        if step.getXmlType() is None:
            return
        self.doc.setNodeText(node,'x',str(step.symbol.GetX()))
        self.doc.setNodeText(node,'y',str(step.symbol.GetY()))
        self.doc.setNodeText(node,'type',step.getXmlType())
        self.doc.setNodeText(node,'id',step.id)
        self.doc.setNodeText(node,'desc',step.desc)
        self.doc.setNodeText(node,'info',step.info)
        self.doc.setNodeText(node,'text',step.text)
        self.doc.setNodeText(node,'IDtext',step.IDtext)
        #vtXmlDomTree.setNodeText(node,'parallel',str(step.parallel),doc)
        if step.getXmlType()=='conn':
            self.doc.setNodeText(node,'connect',step.connect)
            self.doc.setNodeText(node,'IDconnect',step.IDconnect)
    def SetLink(self,node,step):
        childs=self.doc.getChilds(node,'follow')
        for c in childs:
            id=self.doc.getAttribute(c,'fid')
            transChilds=self.doc.getChilds(c,'transition')
            lstTrans=[]
            for t in transChilds:
                sName=self.doc.getNodeText(t,'name')
                sVal=self.doc.getNodeText(t,'val')
                try:
                    nodeTransName=self.doc.getChild(t,'name')
                    sIDname=self.doc.getAttribute(nodeTransName,'fid')
                    if len(sIDname)>0:
                        sNew=Hierarchy.getTagNames(self.doc,self.baseNode,self.relNode,self.doc.GetIdStr(sIDname)[1])
                        sName=self.__replace__(sName,sNew)
                except:
                    sIDname=''
                try:
                    nodeTransVal=self.doc.getChild(t,'val')
                    sIDval=self.doc.getAttribute(nodeTransVal,'fid')
                    if len(sIDval)>0:
                        sNew=Hierarchy.getTagNames(self.doc,self.baseNode,self.relNode,self.doc.GetIdStr(sIDval)[1])
                        sVal=self.__replace__(sVal,sNew)
                except:
                    sIDval=''
                trans={'name':sName,
                    'fid_name':sIDname,
                    'operation':self.doc.getNodeText(t,'operation'),
                    'val':sVal,
                    'fid_val':sIDval,
                    'alternative':self.doc.getNodeText(t,'alternative'),
                    'desicNr':self.doc.getNodeText(t,'desicNr')}
                
                lstTrans.append(trans)
            if len(lstTrans)<1:
                lstTrans=None
            try:
                step.AddLink(self.stepIDs[id],lstTrans)
            except:
                pass
        pass
    def GetLink(self,node,step):
        if node is None:
            return
        childs=self.doc.getChilds(node,'follow')
        for c in childs:
            self.doc.deleteNode(c,node)
        for nextStep in step.nextElem:
            n=self.doc.createSubNode(node,'follow',False)
            self.doc.setAttribute(n,'fid',nextStep[0].GetID())
            for trans in nextStep[2]:
                nt=self.doc.createSubNode(n,'transition',False)
                keys=trans.keys()
                keys.sort()
                for k in keys:
                    if k[:4]=='fid_':
                        continue
                    self.doc.setNodeText(nt,k,trans[k])
    def SetDoc(self,doc,bNet):
        self.doc=doc
    def SetNode(self,node):
        #if self.bModified == True:
            # ask
        #    dlg=wx.MessageDialog(self,u'Do you to apply modified data?' ,
        #                u'vgaPrjEng Elements Info',
        #                wx.YES_NO|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
        #    if dlg.ShowModal()==wx.ID_YES:
        #        self.GetNode(self.node,self.ids,self.doc)
        self.Clear()
        self.node=node
        self.baseNode=self.doc.getChild(self.doc.getRoot(),'prjengs')
        self.baseNode=self.doc.getChild(self.baseNode,'instance')
        self.relNode=Hierarchy.getRelBaseNode(self.doc,node)
        self.FCtype=self.doc.getNodeText(node,'__type')
        #self.canvas
        dc = wx.ClientDC(self)
        self.PrepareDC(dc)
        for s in self.steps:
            s.Delete()
        self.diagram.DeleteAllShapes()
        self.shapes=[]
        self.steps=[]
        self.stepIDs={}
        childs=self.doc.getChilds(node,'elem')
        for c in childs:
            self.SetNodeStep(c)
        i=0
        for c in childs:
            self.SetLink(c,self.steps[i])
            i+=1
        self.GetDiagram().Clear(dc)
        self.Redraw(dc)
        self.SetModified(False)
    def __getNode__(self,childs,id):
        for c in childs:
            if self.doc.getAttribute(c,'id')==id:
                return c
        return None
    def GetNode(self,node):
        self.node=node
        if self.node is None:
            return
        self.stepIDs={}
        self.used=[]
        self.doc.setNodeText(node,'__type',self.FCtype)
        childs=self.doc.getChilds(node,'elem')
        for step in self.steps:
            if len(step.GetID())>0:
                n=self.__getNode__(childs,step.GetID())
                #n=vtXmlDomTree.createSubNode(node,'step',doc,False)
                #n.setAttribute('id',step.id)
                self.used.append(n)
            else:
                #new node
                n=self.doc.createSubNode(node,'elem',False)
                self.doc.setAttribute(n,'id','')
                self.doc.addNode(node,n)
                #self.ids.CheckId(n)
            if n is not None:
                self.GetNodeStep(step,n)
                #self.ids.ProcessMissing()
                #self.ids.ClearMissing()
                step.SetID(self.doc.getAttribute(n,'id'))
                self.stepIDs[step.GetID()]=step
        # delete not used
        for c in childs:
            try:
                idx=self.used.index(c)
            except:
                self.doc.deleteNode(c,node)
        
        childs=self.doc.getChilds(node,'elem')
        for step in self.steps:
            self.GetLink(self.__getNode__(childs,step.GetID()),step)
        
        self.doc.AlignNode(node,iRec=10)
        
        self.SetModified(False)
    def GenTexPic(self):
        xMax=0
        yMax=0
        xMin=sys.maxint
        yMin=sys.maxint
        scaleX=1.5
        scaleY=1.5
        strs=[]
        for step in self.steps:
            x=step.GetX()
            y=step.GetY()
            ba=step.GetBoundingBoxMax()
            xa=x-(ba[0]/2.0)-40.0
            ya=y-(ba[1]/2.0)-40.0
            xe=x+(ba[0]/2.0)+40.0
            ye=y+(ba[1]/2.0)+40.0
            if xa<xMin:
                xMin=xa
            if ya<yMin:
                yMin=ya
            if xe>xMax:
                xMax=xe
            if ye>yMax:
                yMax=ye
        xMin/=scaleX
        yMin/=scaleY
        xMax/=scaleX
        yMax/=scaleY
        strs=['  \\begin{picture}(%d,%d)'%(xMax,yMax)]
        for step in self.steps:
            xa,ya=step.appendLatexPic(strs,yMax,scaleX,scaleY)
            
            xal=x+(ba[0]/2.0/scaleX)
            yl=yMax-y
            
            for nextNode in step.nextElem:
                line=nextNode[1]
                bDashed=False
                if nextNode[2][0].has_key('alternative'):
                    if nextNode[2][0]['alternative']==u'1':
                        bDashed=True
                if hasattr(nextNode[0],'bAnnotation'):
                    bDashed=True
                points=line.GetLineControlPoints()
                for i in range(0,5):
                    xa=points[i][0]/scaleX
                    ya=yMax-points[i][1]/scaleY
                    xe=points[i+1][0]/scaleX
                    ye=yMax-points[i+1][1]/scaleY
                    dx=0
                    dy=0
                    if xa==xe:
                        if ya==ye:
                            continue
                        elif ya<ye:
                            dy=1
                            l=ye-ya
                        else:
                            dy=-1
                            l=ya-ye
                    elif xa<xe:
                        dx=1
                        l=xe-xa
                    else:
                        dx=-1
                        l=xa-xe
                    if 1==0:
                        tup=(xa,ya,sThick,dx,dy,l)
                        if i==4:
                            strs.append('    \\put(%4.1f,%4.1f){%s\\vector(%d,%d){%4.1f}}'%tup)
                        else:
                            strs.append('    \\put(%4.1f,%4.1f){%s\\line(%d,%d){%4.1f}}'%tup)
                    tup=(xa,ya,dx,dy,l)
                    def __simulateDashed__(strs,tup,bVector):
                        dw=10
                        iCount=int(round(tup[4]/dw))
                        for i in range(0,iCount-1):
                            tupTmp=(xa+i*dw*dx,ya+i*dw*dy,dx,dy,5)
                            strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tupTmp)
                        tupTmp=(xa+(l-dw)*dx,ya+(l-dw)*dy,dx,dy,10)
                        if bVector:
                            strs.append('    \\put(%4.1f,%4.1f){\\vector(%d,%d){%4.1f}}'%tupTmp)
                        else:
                            strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tupTmp)
                    if i==4:
                        if hasattr(nextNode[0],'bAnnotation')==True:
                            __simulateDashed__(strs,tup,False)
                        else:
                            if bDashed:
                                __simulateDashed__(strs,tup,True)
                            else:
                                strs.append('    \\put(%4.1f,%4.1f){\\vector(%d,%d){%4.1f}}'%tup)
                    else:
                        if bDashed:
                            __simulateDashed__(strs,tup,False)
                        else:
                            strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
                    
        strs.append('  \\end{picture}')
        return string.join(strs,os.linesep)
    def GenTexFigFloat(self):
        if self.node is not None:
            par=self.doc.getParent(self.node)
            #sHier=vtXmlDomTree.getNodeText(par,'tag') + ' ' + vtXmlDomTree.getNodeText(par,'name') + ' SFC speification'
            relNode=Hierarchy.getRelBaseNode(self.doc,par)
            sHier=Hierarchy.getTagNamesWithRel(self.doc,self.baseNode,relNode,par)+ ' ' + \
                        self.doc.getNodeText(par,'name') + ' FC ' + self.FCtype
            sHier=Hierarchy.getTagNames(self.doc,self.baseNode,None,par)+ ' ' + \
                        self.doc.getNodeText(par,'name') + ' FC'
        sCaption='  \\caption{%s}'%latex.texReplace(sHier)
        sRef='  \\label{fig:%s}'%latex.texReplace4Lbl(sHierFull)
        fig=['\\begin{figure}[!hb]',self.GenTexPic(),sCaption,sRef,'\\end{figure}']
        return string.join(fig,os.linesep)
    def GenTexFig(self):
        if self.node is not None:
            par=self.doc.getParent(self.node)
            relNode=Hierarchy.getRelBaseNode(self.doc,par)
            sHier=Hierarchy.getTagNamesWithRel(self.doc,self.baseNode,relNode,par)+ ' ' + \
                        self.doc.getNodeText(par,'name') + ' FC ' + self.FCtype
            sHierFull=Hierarchy.getTagNames(self.doc,self.baseNode,None,par)+ ' ' + \
                        self.doc.getNodeText(par,'name') + ' FC'
        sCaption='  \\caption{%s}'%latex.texReplace(sHier)
        sRef='  \\label{fig:%s}'%latex.texReplace4Lbl(sHierFull)
        fig=['\\begin{center}','\\begin{longtable}{c}',self.GenTexPic(),'  \\\\',sCaption,sRef,'\\end{longtable}','\\end{center}']
        return string.join(fig,os.linesep)
    def GenTexFigRef(self,node):
        if node is not None:
            par=self.doc.getParent(node)
            baseNode=self.doc.getChild(self.doc.getRoot(),'prjengs')
            baseNode=self.doc.getChild(baseNode,'instance')
            relNode=Hierarchy.getRelBaseNode(self.doc,par)
            sHier=Hierarchy.getTagNames(self.doc,baseNode,None,par)+ ' ' + \
                        self.doc.getNodeText(par,'name') + ' FC'
            sHier=latex.texReplace4Lbl(sHier)
            sRef='\\ref{fig:%s} \#\\pageref{fig:%s}'%(sHier,sHier)
            return sRef
        return ''
    def GenTexTab(self,sHier=None):
        if sHier is None:
            if self.node is not None:
                par=self.doc.getParent(self.node)
                #sHier=vtXmlDomTree.getNodeText(par,'tag') + ' ' + vtXmlDomTree.getNodeText(par,'name') + ' SFC speification'
                relNode=Hierarchy.getRelBaseNode(self.doc,par)
                sHier=Hierarchy.getTagNamesWithRel(self.doc,self.baseNode,relNode,par)+ ' ' + \
                            self.doc.getNodeText(par,'name') + ' FC ' + self.FCtype
                sHierFull=Hierarchy.getTagNames(self.doc,self.baseNode,None,par)+ ' ' + \
                            self.doc.getNodeText(par,'name') + ' FC'
            else:
                sHier=''
                sHierFull=''
        strs=[]
        strs.append('\\begin{center}')
        strs.append('  \\begin{longtable}{p{0.05\linewidth}|p{0.05\linewidth}|p{0.05\linewidth}|p{0.05\linewidth}|p{0.35\linewidth}')
        strs.append('       p{0.2\linewidth}p{0.02\linewidth}p{0.01\linewidth}}')
        strs.append('  \\multicolumn{6}{c}{\\textbf{%s %s}} \\\\ \\hline'%('flow chart',self.FCtype))
        tup=('    \\textbf{Nr}','\\textbf{ID}','\\textbf{to}','','\\textbf{Name}','\\textbf{Condition}','','')
        strs.append('    %s & %s & %s & %s & %s & %s & %s & %s \\\\ '%tup)
        tup=('Description')
        strs.append('     &  &  &  & \\multicolumn{4}{l}{\\hspace{1cm}%s}  \\\\ \\hline'%tup)
        strs.append('  \\endfirsthead')
        tup=('    \\textbf{Nr}','\\textbf{ID}','\\textbf{to}','','\\textbf{Name}','\\textbf{Condition}','','')
        strs.append('    %s & %s & %s & %s & %s & %s & %s & %s \\\\ '%tup)
        tup=('Description')
        strs.append('     &  &  &  & \\multicolumn{4}{l}{\\hspace{1cm}%s}  \\\\ \\hline'%tup)
        strs.append('  \\endhead')
        nrs={}
        i=0
        for step in self.steps:
            try:
                lst=nrs[step.id]
                lst.append(i)
            except:
                nrs[step.id]=[i]
            i+=1
        keys=nrs.keys()
        keys.sort()
        i=0
        for k in keys:
            for idx in nrs[k]:
                step=self.steps[idx]
                tup=('%03d'%i,step.id,latex.texReplace(step.desc),latex.texReplace(step.info))
                i+=1
                strs.append('    %s & \\textbf{%s} &  & %s & %s &   &   &   \\\\ '%tup)
                step.UpdateReference()
                #sText=latex.texReplace(step.text)
                #sText=string.replace(sText,'$','')
                #tup=(sText)
                #strs.append('     &  & &  & \\begin{tabular}[b]{l}%s\\end{tabular} \\\\'%tup)
                sText=step.text
                if string.strip(sText)>0:
                    strs.append('     &  & &  & \\multicolumn{4}{l}{\\hspace{1cm}%s} \\\\'%(latex.tableMultiLine(sText)))
                if step.getXmlType()=='conn':
                    strs.append('    \\cline{4-8}')
                    sConnect=string.replace(step.connect,'\n','\\\\ ')
                    sConnect=string.replace(sConnect,'$','')
                    tup=(sConnect,u'connect')
                    strs.append('     &  &  & & \\begin{tabular}[b]{l}%s\\end{tabular} & %s\\\\'%tup)
                    #strs.append('    \\cline{3-5}')
                    
                if len(step.nextElem)>0:
                    strs.append('    \\cline{2-8}')
                for nextNode in step.nextElem:
                    try:
                        sFID=nextNode[0].id
                    except:
                        sFID=''
                    try:
                        sFName=nextNode[0].info
                    except:
                        sFName=''
                        
                    for trans in nextNode[2]:
                        try:
                            sName=trans['name']
                        except:
                            sName=''
                        try:
                            if trans['alternative']=='1':
                                sAlternative='x'
                            else:
                                sAlternative='-'
                        except:
                            sAlternative=''
                        try:
                            sLv=trans['desicNr']
                        except:
                            sLv=''
                        tup=('',sFID,'',latex.texReplace(sFName),
                                latex.texReplace(sName),sAlternative,sLv)
                        strs.append('    %s & $\\to$ & %s & %s & %s & %s & %s & %s \\\\'%tup)
                        #strs.append('    %s & \\multicolumn{3}{l}{%s} \\\\'%tup)
                        #tup=('',sOp,sVal)
                        #strs.append('    %s  & %s & %s & %s \\\\'%tup)
                        #strs.append('    %s & %s & \\multicolumn{2}{l}{%s} \\\\'%tup)
                        #tup=('',sComb,sLv,'')
                        #strs.append('    %s & %s & %s & %s \\\\'%tup)
                strs.append('    \\hline ')
                        
        strs.append('  \\caption{table %s}'%latex.texReplace(sHier))
        strs.append('  \\label{tab:%s}'%latex.texReplace4Lbl(sHierFull))
            
        strs.append('  \\end{longtable}')
        strs.append('\\end{center}')
        return string.join(strs,os.linesep)
    def GenTexTabRef(self,node):
        if node is not None:
            par=self.doc.getParent(node)
            baseNode=self.doc.getChild(self.doc.getRoot(),'prjengs')
            baseNode=self.doc.getChild(baseNode,'instance')
            relNode=Hierarchy.getRelBaseNode(self.doc,par)
            sHier=Hierarchy.getTagNames(self.doc,baseNode,None,par)+ ' ' + \
                        self.doc.getNodeText(par,'name') + ' FC'
            sHier=latex.texReplace4Lbl(sHier)
            sRef='\\ref{tab:%s} \#\\pageref{tab:%s}'%(sHier,sHier)
            return sRef
        return ''
    