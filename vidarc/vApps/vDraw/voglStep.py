#Boa:Dialog:vgdTimeAxis
#----------------------------------------------------------------------------
# Name:         voglStep.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: voglStep.py,v 1.1 2005/12/13 13:19:34 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import wx.lib.ogl as ogl

import images

def create(parent):
    return vgdTimeAxis(parent)

[wxID_VGDTIMEAXIS, wxID_VGDTIMEAXISGCBBCANCEL, wxID_VGDTIMEAXISGCBBOK, 
 wxID_VGDTIMEAXISLBLCOUNT, wxID_VGDTIMEAXISSPNCOUNT, 
] = [wx.NewId() for _init_ctrls in range(5)]

class vgdTimeAxis(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VGDTIMEAXIS, name=u'vgdTimeAxis',
              parent=prnt, pos=wx.Point(325, 103), size=wx.Size(209, 148),
              style=wx.DEFAULT_DIALOG_STYLE, title=u'Time Axis')
        self.SetClientSize(wx.Size(201, 121))

        self.gcbbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDTIMEAXISGCBBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'gcbbOk',
              parent=self, pos=wx.Point(24, 80), size=wx.Size(76, 30), style=0)
        self.gcbbOk.Bind(wx.EVT_BUTTON, self.OnGcbbOkButton,
              id=wxID_VGDTIMEAXISGCBBOK)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDTIMEAXISGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(112, 80),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VGDTIMEAXISGCBBCANCEL)

        self.spnCount = wx.SpinCtrl(id=wxID_VGDTIMEAXISSPNCOUNT, initial=0,
              max=100, min=0, name=u'spnCount', parent=self, pos=wx.Point(64,
              16), size=wx.Size(117, 21), style=wx.SP_ARROW_KEYS)

        self.lblCount = wx.StaticText(id=wxID_VGDTIMEAXISLBLCOUNT,
              label=u'Count', name=u'lblCount', parent=self, pos=wx.Point(32,
              16), size=wx.Size(28, 13), style=0)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        img=images.getOkBitmap()
        self.gcbbOk.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)

    def OnGcbbOkButton(self, event):
        self.EndModal(1)
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
    def GetCount(self):
        return self.spnCount.GetValue()
    def SetCount(self,count):
        self.spnCount.SetValue(count)
class voglStep(ogl.DividedShape):
    def __init__(self, width, height, canvas):
        ogl.DividedShape.__init__(self, width, height)

        region1 = ogl.ShapeRegion()
        region1.SetText('DividedShape')
        region1.SetProportions(0.0, 0.2)
        region1.SetFormatMode(ogl.FORMAT_CENTRE_HORIZ)
        self.AddRegion(region1)

        region2 = ogl.ShapeRegion()
        region2.SetText('This is Region number two.')
        region2.SetProportions(0.0, 0.3)
        region2.SetFormatMode(ogl.FORMAT_CENTRE_HORIZ|ogl.FORMAT_CENTRE_VERT)
        self.AddRegion(region2)

        region3 = ogl.ShapeRegion()
        region3.SetText('Region 3\nwith embedded\nline breaks')
        region3.SetProportions(0.0, 0.5)
        region3.SetFormatMode(ogl.FORMAT_NONE)
        self.AddRegion(region3)

        self.SetRegionSizes()
        self.ReformatRegions(canvas)

    
    def ReformatRegions(self, canvas=None):
        rnum = 0

        if canvas is None:
            canvas = self.GetCanvas()

        dc = wx.ClientDC(canvas)  # used for measuring

        for region in self.GetRegions():
            text = region.GetText()
            self.FormatText(dc, text, rnum)
            rnum += 1


    def OnSizingEndDragLeft(self, pt, x, y, keys, attch):
        print "***", self
        ogl.DividedShape.OnSizingEndDragLeft(self, pt, x, y, keys, attch)
        self.SetRegionSizes()
        self.ReformatRegions()
        self.GetCanvas().Refresh()

    def IsSelectable(self):
        return 0
    def DoEdit(self):
        if self.dlg is None:
            self.dlg=vgdTimeAxis(self.parent)
        self.dlg.Centre()
        self.dlg.SetCount(self.count)
        if self.dlg.ShowModal()>0:
            oldCount=self.count
            self.count=self.dlg.GetCount()
            if oldCount!=self.count:
                self.ClearText()
                self.signals.append(('ll',0,1,1))
                self.__draw__()
                return 1
        return 0
    