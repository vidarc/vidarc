#Boa:Dialog:vgdSFCstep
#----------------------------------------------------------------------------
# Name:         voglSFCstep.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: voglSFCstep.py,v 1.1 2005/12/13 13:19:34 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import wx.lib.ogl as ogl

import sys,string
#import vidarc.apps.xmlBase.vtXmlDomTree as vtXmlDomTree

import images

from vidarc.vApps.vDraw.voglSFCstepActions import voglSFCstepActions

def create(parent):
    return vgdSFCstep(parent)

[wxID_VGDSFCSTEP, wxID_VGDSFCSTEPCHCCOMBINE, wxID_VGDSFCSTEPCHCFOLLOWTYPE, 
 wxID_VGDSFCSTEPCHCOPERATOR, wxID_VGDSFCSTEPGCBBADD, 
 wxID_VGDSFCSTEPGCBBBROWSE, wxID_VGDSFCSTEPGCBBBROWSEVAL, 
 wxID_VGDSFCSTEPGCBBDEL, wxID_VGDSFCSTEPGCBBDELETEFOLLOW, 
 wxID_VGDSFCSTEPGCBBOK, wxID_VGDSFCSTEPGCBBSET, wxID_VGDSFCSTEPLBLCOMB, 
 wxID_VGDSFCSTEPLBLLEVEL, wxID_VGDSFCSTEPLBLOP, wxID_VGDSFCSTEPLBLSTEPNAME, 
 wxID_VGDSFCSTEPLBLSTEPNR, wxID_VGDSFCSTEPLBLTAG, wxID_VGDSFCSTEPLBLVAL, 
 wxID_VGDSFCSTEPLSTFOLLOW, wxID_VGDSFCSTEPLSTTRANS, wxID_VGDSFCSTEPSPNLEVEL, 
 wxID_VGDSFCSTEPTXTNAME, wxID_VGDSFCSTEPTXTNR, wxID_VGDSFCSTEPTXTSTEPNAME, 
 wxID_VGDSFCSTEPTXTVAL, 
] = [wx.NewId() for _init_ctrls in range(25)]

class vgdSFCstep(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VGDSFCSTEP, name=u'vgdSFCstep',
              parent=prnt, pos=wx.Point(320, 89), size=wx.Size(480, 429),
              style=wx.DEFAULT_DIALOG_STYLE, title=u'SFC Step')
        self.SetClientSize(wx.Size(472, 402))

        self.txtNr = wx.TextCtrl(id=wxID_VGDSFCSTEPTXTNR, name=u'txtNr',
              parent=self, pos=wx.Point(32, 4), size=wx.Size(64, 21), style=0,
              value=u'')

        self.lblStepNr = wx.StaticText(id=wxID_VGDSFCSTEPLBLSTEPNR, label=u'Nr',
              name=u'lblStepNr', parent=self, pos=wx.Point(4, 6),
              size=wx.Size(11, 13), style=0)

        self.txtStepName = wx.TextCtrl(id=wxID_VGDSFCSTEPTXTSTEPNAME,
              name=u'txtStepName', parent=self, pos=wx.Point(144, 4),
              size=wx.Size(100, 21), style=0, value=u'')

        self.lblStepName = wx.StaticText(id=wxID_VGDSFCSTEPLBLSTEPNAME,
              label=u'Name', name=u'lblStepName', parent=self, pos=wx.Point(104,
              8), size=wx.Size(28, 13), style=0)

        self.chcFollowType = wx.CheckBox(id=wxID_VGDSFCSTEPCHCFOLLOWTYPE,
              label=u'parallel', name=u'chcFollowType', parent=self,
              pos=wx.Point(352, 8), size=wx.Size(53, 13), style=0)
        self.chcFollowType.SetValue(False)
        self.chcFollowType.Bind(wx.EVT_CHECKBOX, self.OnChcFollowTypeCheckbox,
              id=wxID_VGDSFCSTEPCHCFOLLOWTYPE)

        self.lstFollow = wx.ListView(id=wxID_VGDSFCSTEPLSTFOLLOW,
              name=u'lstFollow', parent=self, pos=wx.Point(8, 32),
              size=wx.Size(336, 88), style=wx.LC_REPORT)
        self.lstFollow.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstFollowListItemSelected, id=wxID_VGDSFCSTEPLSTFOLLOW)

        self.gcbbDeleteFollow = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPGCBBDELETEFOLLOW,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Delete',
              name=u'gcbbDeleteFollow', parent=self, pos=wx.Point(384, 40),
              size=wx.Size(76, 30), style=0)
        self.gcbbDeleteFollow.Bind(wx.EVT_BUTTON, self.OnGcbbDeleteFollowButton,
              id=wxID_VGDSFCSTEPGCBBDELETEFOLLOW)

        self.lstTrans = wx.ListView(id=wxID_VGDSFCSTEPLSTTRANS,
              name=u'lstTrans', parent=self, pos=wx.Point(8, 128),
              size=wx.Size(368, 100),
              style=wx.LC_REPORT | wx.LC_SORT_ASCENDING)
        self.lstTrans.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstTransListItemSelected, id=wxID_VGDSFCSTEPLSTTRANS)

        self.gcbbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPGCBBADD,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'gcbbAdd',
              parent=self, pos=wx.Point(384, 136), size=wx.Size(76, 30),
              style=0)
        self.gcbbAdd.Bind(wx.EVT_BUTTON, self.OnGcbbAddButton,
              id=wxID_VGDSFCSTEPGCBBADD)

        self.gcbbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPGCBBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Delete', name=u'gcbbDel',
              parent=self, pos=wx.Point(384, 174), size=wx.Size(76, 30),
              style=0)
        self.gcbbDel.Bind(wx.EVT_BUTTON, self.OnGcbbDelButton,
              id=wxID_VGDSFCSTEPGCBBDEL)

        self.txtName = wx.TextCtrl(id=wxID_VGDSFCSTEPTXTNAME, name=u'txtName',
              parent=self, pos=wx.Point(8, 248), size=wx.Size(368, 21), style=0,
              value=u'')

        self.gcbbBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPGCBBBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbBrowse', parent=self, pos=wx.Point(384, 244),
              size=wx.Size(76, 30), style=0)
        self.gcbbBrowse.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseButton,
              id=wxID_VGDSFCSTEPGCBBBROWSE)

        self.chcOperator = wx.Choice(choices=[u'==', u'<', u'<=', u'>', u'>=',
              u'!=', u'1==1', u'0==1', u'?'], id=wxID_VGDSFCSTEPCHCOPERATOR,
              name=u'chcOperator', parent=self, pos=wx.Point(8, 284),
              size=wx.Size(50, 21), style=0)
        self.chcOperator.SetSelection(0)
        self.chcOperator.SetToolTipString(u'operation')

        self.txtVal = wx.TextCtrl(id=wxID_VGDSFCSTEPTXTVAL, name=u'txtVal',
              parent=self, pos=wx.Point(64, 284), size=wx.Size(312, 21),
              style=0, value=u'')

        self.gcbbBrowseVal = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPGCBBBROWSEVAL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbBrowseVal', parent=self, pos=wx.Point(384, 280),
              size=wx.Size(76, 30), style=0)
        self.gcbbBrowseVal.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseValButton,
              id=wxID_VGDSFCSTEPGCBBBROWSEVAL)

        self.chcCombine = wx.Choice(choices=[u'and', u'nand', u'or', u'nor'],
              id=wxID_VGDSFCSTEPCHCCOMBINE, name=u'chcCombine', parent=self,
              pos=wx.Point(64, 316), size=wx.Size(50, 21), style=0)
        self.chcCombine.SetSelection(0)

        self.spnLevel = wx.SpinCtrl(id=wxID_VGDSFCSTEPSPNLEVEL, initial=0,
              max=10, min=0, name=u'spnLevel', parent=self, pos=wx.Point(160,
              316), size=wx.Size(50, 21), style=wx.SP_ARROW_KEYS)
        self.spnLevel.SetValue(0)

        self.gcbbSet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPGCBBSET,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Set', name=u'gcbbSet',
              parent=self, pos=wx.Point(384, 324), size=wx.Size(76, 30),
              style=0)
        self.gcbbSet.Bind(wx.EVT_BUTTON, self.OnGcbbSetButton,
              id=wxID_VGDSFCSTEPGCBBSET)

        self.lblTag = wx.StaticText(id=wxID_VGDSFCSTEPLBLTAG, label=u'Tag',
              name=u'lblTag', parent=self, pos=wx.Point(8, 232),
              size=wx.Size(22, 13), style=0)

        self.lblOp = wx.StaticText(id=wxID_VGDSFCSTEPLBLOP, label=u'Operation',
              name=u'lblOp', parent=self, pos=wx.Point(8, 270), size=wx.Size(46,
              13), style=0)

        self.lblVal = wx.StaticText(id=wxID_VGDSFCSTEPLBLVAL, label=u'Value',
              name=u'lblVal', parent=self, pos=wx.Point(68, 270),
              size=wx.Size(27, 13), style=0)

        self.lblLevel = wx.StaticText(id=wxID_VGDSFCSTEPLBLLEVEL,
              label=u'Level', name=u'lblLevel', parent=self, pos=wx.Point(128,
              320), size=wx.Size(26, 13), style=0)

        self.lblComb = wx.StaticText(id=wxID_VGDSFCSTEPLBLCOMB,
              label=u'Combine', name=u'lblComb', parent=self, pos=wx.Point(8,
              320), size=wx.Size(41, 13), style=0)

        self.gcbbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPGCBBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'gcbbOk',
              parent=self, pos=wx.Point(176, 368), size=wx.Size(76, 30),
              style=0)
        self.gcbbOk.Bind(wx.EVT_BUTTON, self.OnGcbbOkButton,
              id=wxID_VGDSFCSTEPGCBBOK)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        self.sortOrder=0
        self.dlgBrowse=None
        self.dlgBrowseVal=None
        self.IDname=''
        self.IDval=''
        
        img=images.getOkBitmap()
        self.gcbbOk.SetBitmapLabel(img)
        self.gcbbSet.SetBitmapLabel(img)
        
        img=images.getDelBitmap()
        self.gcbbDel.SetBitmapLabel(img)
        self.gcbbDeleteFollow.SetBitmapLabel(img)
        
        img=images.getAddBitmap()
        self.gcbbAdd.SetBitmapLabel(img)
        
        img=images.getBrowseBitmap()
        self.gcbbBrowse.SetBitmapLabel(img)
        self.gcbbBrowseVal.SetBitmapLabel(img)
        
        self.lstFollow.InsertColumn(0,u'Nr',wx.LIST_FORMAT_LEFT,30)
        self.lstFollow.InsertColumn(1,u'Name',wx.LIST_FORMAT_LEFT,200)
        
        self.lstTrans.InsertColumn(0,u'Nr',wx.LIST_FORMAT_LEFT,30)
        self.lstTrans.InsertColumn(1,u'Name',wx.LIST_FORMAT_LEFT,150)
        self.lstTrans.InsertColumn(2,u'Oper',wx.LIST_FORMAT_CENTER,50)
        self.lstTrans.InsertColumn(3,u'Value',wx.LIST_FORMAT_LEFT,70)
        self.lstTrans.InsertColumn(4,u'Comb',wx.LIST_FORMAT_LEFT,30)
        self.lstTrans.InsertColumn(5,u'Level',wx.LIST_FORMAT_LEFT,30)
        self.__setTransEnable__(False)
    def __setTransEnable__(self,state):
        #self.txtName.Enable(state)
        #self.chcOperator.Enable(state)
        #self.gcbbBrowse.Enable(state)
        #self.txtVal.Enable(state)
        #self.gcbbBrowseVal.Enable(state)
        #self.chcCombine.Enable(state)
        #self.spnLevel.Enable(state)
        self.gcbbSet.Enable(state)
    def __clear__(self):
        self.txtName.SetValue('')
        self.chcOperator.SetSelection(0)
        self.txtVal.SetValue('')    
    def SetNrs(self,nrs):
        self.nrs=nrs
    def SetInfos(self,voglSFCstep,voglDrawSFC):
        #self.spnStepNr.SetValue(voglSFCstep.nr)
        self.voglSFC=voglDrawSFC
        self.voglSFCstep=voglSFCstep
        self.txtNr.SetValue(self.voglSFCstep.nr)
        self.txtStepName.SetValue(self.voglSFCstep.name)
        if self.voglSFCstep.parallel:
            self.chcFollowType.SetValue(True)
        else:
            self.chcFollowType.SetValue(False)
        self.__updateFollowing__()
    def SetBrowseDlg(self,dlg):
        self.dlgBrowse=dlg
    def SetBrowseValDlg(self,dlg):
        self.dlgBrowseVal=dlg
    def __updateTrans__(self):
        self.selTrans=-1
        self.IDname=''
        self.IDval=''
        self.lstTrans.DeleteAllItems()
        self.__setTransEnable__(False)
        self.__clear__()
        if self.selFollow<0:
            return
        #self.txtName.SetValue('')
        #self.txtVal.SetValue('')
        self.selTrans=-1
        self.lstTrans.DeleteAllItems()
        transitionLst=self.voglSFCstep.nextSteps[self.selFollow][2]
        # build steps
        transNames=[]
        i=0
        for a in transitionLst:
            transNames.append((a['name'],i))
            i+=1
        def cmpAct(x,y):
            return cmp(x[0],y[0])
        transNames.sort(cmpAct)
        if self.sortOrder>0:
            transNames.reverse()
        i=0
        for a in transNames:
            trans=transitionLst[a[1]]
            index = self.lstTrans.InsertImageStringItem(sys.maxint, str(i),-1)
            self.lstTrans.SetStringItem(index,1,trans['name'], -1)
            self.lstTrans.SetStringItem(index,2,trans['operation'],-1)
            self.lstTrans.SetStringItem(index,3,trans['val'],-1)
            self.lstTrans.SetStringItem(index,4,trans['combine'],-1)
            self.lstTrans.SetStringItem(index,5,trans['level'],-1)
            self.lstTrans.SetItemData(index,a[1])
            i+=1
    def __updateFollowing__(self):
        self.selFollow=-1
        self.lstFollow.DeleteAllItems()
        i=0
        for s in self.voglSFCstep.nextSteps:
            index = self.lstFollow.InsertImageStringItem(sys.maxint, s[0].nr, -1)
            self.lstFollow.SetStringItem(index,1,s[0].GetName(),-1)
            self.lstFollow.SetItemData(index,i)
            i+=1
            pass
        self.__updateTrans__()
    def OnGcbbOkButton(self, event):
        self.voglSFCstep.nr=self.txtNr.GetValue()
        self.voglSFCstep.name=self.txtStepName.GetValue()
        self.OnGcbbSetButton(None)
        self.EndModal(1)
        event.Skip()
    def OnGcbbAddButton(self, event):
        #print self.spnLevel.GetValue(),type(self.spnLevel.GetValue())
        if self.selFollow<0:
            return
        trans={'name':self.txtName.GetValue(),
            'fid_name':self.IDname,
            'operation':self.chcOperator.GetStringSelection(),
            'val':self.txtVal.GetValue(),
            'fid_val':self.IDval,
            'combine':self.chcCombine.GetStringSelection(),
            'level':str(self.spnLevel.GetValue())}
        #i=len(self.voglSFCstep.nextSteps[self.selFollow][2])
        #index = self.lstTrans.InsertImageStringItem(sys.maxint, str(i), -1)
        #self.lstTrans.SetStringItem(index,1,trans['name'],-1)
        #self.lstTrans.SetStringItem(index,2,trans['operation'],-1)
        #self.lstTrans.SetStringItem(index,3,trans['val'],-1)
        #self.lstTrans.SetStringItem(index,4,trans['combine'],-1)
        #self.lstTrans.SetStringItem(index,5,trans['level'],-1)
        self.voglSFCstep.nextSteps[self.selFollow][2].append(trans)
        self.__updateTrans__()
        event.Skip()

    def OnLstFollowListItemSelected(self, event):
        self.selFollow=event.GetIndex()
        self.__updateTrans__()
        event.Skip()
    def OnGcbbDelButton(self, event):
        if self.selTrans<0:
            return
        idx=self.lstTrans.GetItemData(self.selTrans)
        #self.lstTrans.DeleteItem(self.selTrans)
        #i=self.lstFollow.GetItemData(self.selFollow)
        s=self.voglSFCstep.nextSteps[self.selFollow][0]
        l=self.voglSFCstep.nextSteps[self.selFollow][1]
        lst=self.voglSFCstep.nextSteps[self.selFollow][2]
        #lst=lst[:self.selTrans]+lst[self.selTrans+1:]
        lst=lst[:idx]+lst[idx+1:]
        self.voglSFCstep.nextSteps[self.selFollow]=(s,l,lst)
        self.__updateTrans__()
        self.selTrans=-1
        #self.__updateFollowing__()
        event.Skip()
    
    def __replace__(self,strOld,strNew):
        i=string.find(strOld,'<')
        if i>=0:
            j=string.find(strOld,'>',i+1)
            if j>0:
                return strOld[:i+1]+strNew+strOld[j:]
        return '<'+strNew+'>'
    def OnGcbbBrowseButton(self, event):
        if self.dlgBrowse is not None:
            self.dlgBrowse.Centre()
            if self.dlgBrowse.ShowModal()>0:
                s=self.dlgBrowse.GetTagName()
                self.IDname=self.dlgBrowse.GetId()
                sOld=self.txtName.GetValue()
                s=self.__replace__(sOld,s)
                self.txtName.SetValue(s)
        event.Skip()

    def OnGcbbCreateButton(self, event):
        self.EndModal(2)
        event.Skip()

    def OnChcFollowTypeCheckbox(self, event):
        event.Skip()

    def OnLstTransListItemSelected(self, event):
        self.selTrans=event.GetIndex()
        if self.selTrans<0:
            return
        self.__setTransEnable__(True)
        idx=event.GetIndex()
        it=self.lstTrans.GetItem(idx,1)
        self.txtName.SetValue(it.m_text)
        
        it=self.lstTrans.GetItem(idx,2)
        self.chcOperator.SetStringSelection(it.m_text)
        
        it=self.lstTrans.GetItem(idx,3)
        self.txtVal.SetValue(it.m_text)
        
        it=self.lstTrans.GetItem(idx,4)
        self.chcCombine.SetStringSelection(it.m_text)
        
        try:
            it=self.lstTrans.GetItem(idx,5)
            self.spnLevel.SetValue(int(it.m_text))
        except:
            self.spnLevel.SetValue(0)
        
        lst=self.voglSFCstep.nextSteps[self.selFollow][2]
        trans=lst[self.selTrans]
        try:
            self.IDname=trans['fid_name']
        except:
            self.IDname=''
        try:
            self.IDval=trans['fid_val']
        except:
            self.IDval=''
        event.Skip()

    def OnGcbbSetButton(self, event):
        if self.selFollow<0:
            return
        if self.selTrans<0:
            return
        trans={'name':self.txtName.GetValue(),
            'fid_name':self.IDname,
            'operation':self.chcOperator.GetStringSelection(),
            'val':self.txtVal.GetValue(),
            'fid_val':self.IDval,
            'combine':self.chcCombine.GetStringSelection(),
            'level':str(self.spnLevel.GetValue())}
        #index = self.selTrans
        #self.lstTrans.SetStringItem(index,1,trans['name'],-1)
        #self.lstTrans.SetStringItem(index,2,trans['operation'],-1)
        #self.lstTrans.SetStringItem(index,3,trans['val'],-1)
        #self.lstTrans.SetStringItem(index,4,trans['combine'],-1)
        #self.lstTrans.SetStringItem(index,5,trans['level'],-1)
        idx=self.lstTrans.GetItemData(self.selTrans)
        
        s=self.voglSFCstep.nextSteps[self.selFollow][0]
        l=self.voglSFCstep.nextSteps[self.selFollow][1]
        lst=self.voglSFCstep.nextSteps[self.selFollow][2]
        #lst=lst[:self.selTrans]+[trans]+lst[self.selTrans+1:]
        lst=lst[:idx]+[trans]+lst[idx+1:]
        self.voglSFCstep.nextSteps[self.selFollow]=(s,l,lst)
        self.__updateTrans__()
        if event is not None:
            event.Skip()

    def OnGcbbBrowseValButton(self, event):
        if self.dlgBrowseVal is not None:
            self.dlgBrowseVal.Centre()
            if self.dlgBrowseVal.ShowModal()>0:
                s=self.dlgBrowseVal.GetTagName()
                self.IDval=self.dlgBrowseVal.GetId()
                s=self.__replace__(self.txtVal.GetValue(),s)
                self.txtVal.SetValue(s)
        event.Skip()

    def OnGcbbDeleteFollowButton(self, event):
        if self.selFollow<0:
            return
        self.voglSFCstep.RemoveLink(self.selFollow)
        self.lstFollow.DeleteItem(self.selFollow)
        self.selFollow=-1
        self.__updateTrans__()
        event.Skip()


class voglSFCstep(ogl.CompositeShape):
    def __init__(self, nr,id,name,actions,width, height, canvas):
        ogl.CompositeShape.__init__(self)
        self.nr=nr
        self.id=id
        self.name=name
        self.parallel=0
        self.dlg=None
        self.canvas=canvas
        self.SetCanvas(canvas)
        self.prevStep=None
        self.nextSteps=[]
        self.prevSteps=[]
        
        # add a shape to the original division
        self.step = ogl.RectangleShape(60, 40)
        self.step.AddText('Nr: %s'%self.nr)
        self.step.SetDraggable(False)
        self.step.SetSensitivityFilter(0,True)
        self.AddChild(self.step)
        
        
        # and add a shape to the second division (and move it to the
        # centre of the division)
        self.actions = voglSFCstepActions(self,actions,canvas)
        b2=self.step.GetBoundingBoxMax()
        b3=self.actions.GetBoundingBoxMax()
        #shape3.SetBrush(wx.CYAN_BRUSH)
        #self.GetDivisions()[1].AddChild(shape3)
        self.actions.SetDraggable(False)
        self.actions.SetSensitivityFilter(0,True)
        self.AddChild(self.actions)
        grid=self.canvas.GetDiagram().GetGridSpacing()
        #grid=15
        self.actions.SetX(b2[0]/2.0+grid+b3[0]/2.0)
        self.actions.SetY(b3[1]/2.0-b2[1]/2.0)
        
        if self.actions.GetFirstRegion() is not None:
            line = ogl.LineShape()
            line.SetCanvas(canvas)
            line.SetPen(wx.BLACK_PEN)
            line.SetBrush(wx.BLACK_BRUSH)
            #line.AddArrow(ogl.ARROW_ARROW)
            line.MakeLineControlPoints(2)
            #shape2.AddLine(line, shape3.GetFirstRegion())
            #self.diagram.AddShape(line)
            #canvas.GetDiagram.AddShape(line)
            line.SetEnds(b2[0]/2.0,0,self.actions.GetX()-b3[0]/2.0+0.0,0)
            self.AddChild(line)
            line.Show(True)
        self.Recompute()
        #self.SetSelectable(True)
    def Update(self):
        b2=self.step.GetBoundingBoxMax()
        b3=self.actions.GetBoundingBoxMax()
        
        grid=self.canvas.GetDiagram().GetGridSpacing()
        
        self.actions.SetX(self.step.GetX()+b2[0]/2.0+grid+b3[0]/2.0)
        self.actions.SetY(self.step.GetY()+b3[1]/2.0-b2[1]/2.0)
        
        self.Recompute()
        
        self.canvas.Refresh()
        pass
    def Delete(self):
        for n in self.nextSteps:
            n[0].prevSteps.remove(self)
            #for l in n[0].nextSteps:
                #if l[0]==self:
                #    print "found"
            n[1].Detach()
            n[1].RemoveFromCanvas(self.canvas)
            n[1].Unlink()
        for n in self.prevSteps:
            for l in n.nextSteps:
                if l[0]==self:
                    l[1].Detach()
                    l[1].RemoveFromCanvas(self.canvas)
                    l[1].Unlink()
                    n.nextSteps.remove(l)
        #self.Show(False)
        #self.Detach()
        #self.step.Unlink()
        self.RemoveFromCanvas(self.canvas)
        #self.canvas.RemoveShape(self)
        #self.canvas.GetDiagram().RemoveShape(self)
    def GetName(self):
        return self.name
    def AddLink(self,step,trans=None):
        line = ogl.LineShape()
        line.SetCanvas(self.canvas)
        line.SetPen(wx.BLACK_PEN)
        line.SetBrush(wx.BLACK_BRUSH)
        line.AddArrow(ogl.ARROW_ARROW)
        
        line.MakeLineControlPoints(6)
        
        #self.step.AddLine(line, step.step)
        self.canvas.GetDiagram().AddShape(line)
        #line.SetFrom(self)
        #line.SetTo(step)
        if trans is None:
            trans=[{'name':'',
                'operation':'0==1',
                'val':'',
                'combine':'and',
                'level':'0'}]
        self.nextSteps.append((step,line,trans))
        step.prevSteps.append(self)
        #self.AddChild(line)
        line.Show(True)
        self.__processLines__(None)
        
    def __processLines__(self,dc):
        for tup in self.nextSteps:
            
            xa=self.step.GetX()
            ya=self.step.GetY()
            ba=self.step.GetBoundingBoxMax()
            
            xe=tup[0].step.GetX()
            ye=tup[0].step.GetY()
            be=tup[0].step.GetBoundingBoxMax()
            
            if dc is not None:
                tup[1].Erase(dc)
            #tup[1].Show(False)
            if ya>=ye:
                #tup[1].MakeLineControlPoints(6)
                
                if xa<xe:
                    x=xa-ba[0]/2.0-20
                else:
                    x=xe-be[0]/2.0-20
                
                # start processing
                #xa+=ba[0]/2.0
                ya+=ba[1]/2.0
                
                #xe+=be[0]/2.0
                ye-=be[1]/2.0
                controlPoints=tup[1].GetLineControlPoints()
                controlPoints[0][0]=xa
                controlPoints[0][1]=ya
                
                ba=self.GetBoundingBoxMax()
                controlPoints[1][0]=xa
                controlPoints[1][1]=ya+ba[1]/2.0+20
                
                controlPoints[2][0]=x
                controlPoints[2][1]=ya+ba[1]/2.0+20
                
                controlPoints[3][0]=x
                controlPoints[3][1]=ye-be[1]/2.0-20
                
                controlPoints[4][0]=xe
                controlPoints[4][1]=ye-be[1]/2.0-20
                
                controlPoints[5][0]=xe
                controlPoints[5][1]=ye
                
            else:
                #tup[1].MakeLineControlPoints(4)
                # start processing
                #xa+=ba[0]/2.0
                ya+=ba[1]/2.0
                
                #xe+=be[0]/2.0
                ye-=be[1]/2.0
                controlPoints=tup[1].GetLineControlPoints()
                controlPoints[0][0]=xa
                controlPoints[0][1]=ya
                
                ba=self.GetBoundingBoxMax()
                ym=self.GetY()+ba[1]/2.0+20
                controlPoints[1][0]=xa
                controlPoints[1][1]=ym#ya+ba[1]/2.0+20
                
                controlPoints[2][0]=xe
                controlPoints[2][1]=ym#ya+ba[1]/2.0+20
                
                controlPoints[3][0]=xe
                controlPoints[3][1]=ym#ya+ba[1]/2.0+20
                
                controlPoints[4][0]=xe
                controlPoints[4][1]=ym#ya+ba[1]/2.0+20
                
                controlPoints[5][0]=xe
                controlPoints[5][1]=ye
            #tup[1].Show(True)
            if dc is not None:
                tup[1].Draw(dc)
            #tup[1].OnDrawControlPoints(dc)
    def HideLinks(self,dc):
        for tup in self.nextSteps:
            tup[1].Show(False)
            tup[1].Draw(dc)
    def HideLinksRec(self,dc):
        self.HideLinks(dc)
        for s in self.prevSteps:
            s.HideLinks(dc)
            
    def UpdateLinks(self,dc):
        self.__processLines__(dc)
    def UpdateLinksRev(self,dc):
        self.__processLines__(dc)
        for s in self.prevSteps:
            s.__processLines__(dc)
    def RemoveLink(self,idx):
        if idx > len(self.nextSteps):
            return
        if idx<0:
            return
        n=self.nextSteps[idx]
        n[0].prevSteps.remove(self)
        n[1].Detach()
        n[1].RemoveFromCanvas(self.canvas)
        n[1].Unlink()
        self.nextSteps=self.nextSteps[:idx]+self.nextSteps[idx+1:]
    def ReformatRegions(self, canvas=None):
        rnum = 0
        if canvas is None:
            canvas = self.GetCanvas()
        dc = wx.ClientDC(canvas)  # used for measuring
        for region in self.GetRegions():
            text = region.GetText()
            self.FormatText(dc, text, rnum)
            rnum += 1
    #def OnMovePost(self, dc, x, y, oldX, oldY, display):
        #print 'mode post'
        #dc = wx.ClientDC(self.canvas)
        # self.canvas.PrepareDC(dc)
        # self.canvas.GetDiagram().Clear(dc)
        #self.HideLinks(dc)
        #for s in self.prevSteps:
        #    s.HideLinks(dc)
    #    ogl.CompositeShape.OnMovePost(self, dc, x, y, oldX, oldY, display)
        # self.UpdateLinks(dc)    
        # for s in self.prevSteps:
        #     s.UpdateLinks(dc)
        #self.Draw(dc)
        # self.canvas.Redraw(dc)
    def OnSizingEndDragLeft(self, pt, x, y, keys, attch):
        #print "***", self
        #ogl.DividedShape.OnSizingEndDragLeft(self, pt, x, y, keys, attch)
        #self.SetRegionSizes()
        #self.ReformatRegions()
        #self.GetCanvas().Refresh()
        pass
    def OnLeftClick(self, x, y, keys = 0, attachment = 0):
        #print "left click step"
        self.updateSelect()
    def updateSelect(self):
        shape = self
        canvas = self.canvas
        dc = wx.ClientDC(canvas)
        canvas.PrepareDC(dc)
        if shape.Selected():
            shape.Select(False, dc)
            canvas.Redraw(dc)
        else:
            redraw = False
            shapeList = canvas.GetDiagram().GetShapeList()
            toUnselect = []
        
            for s in shapeList:
                if s.Selected():
                    # If we unselect it now then some of the objects in
                    # shapeList will become invalid (the control points are
                    # shapes too!) and bad things will happen...
                    toUnselect.append(s)
            #try:
            #    if shape.IsSelectable()>0:
            #        shape.Select(True)
            #        print "selected 1",shape.Selected()
    def OnBeginDragLeft(self, x, y, keys=0, attachment=0):
        x,y=self.canvas.GetDiagram().Snap(x,y)
        ogl.CompositeShape.OnBeginDragLeft(self,x,y,keys,attachment)
    def OnEndDragLeft(self, x, y, keys=0, attachment=0):
        ogl.CompositeShape.OnEndDragLeft(self,x,y,keys,attachment)
    def OnRightClick(self, x, y, keys = 0, attachment = 0):
        #print "right clicked"
        pass
    def IsSelectable(self):
        return 0
    def DoEdit(self,nrs):
        if self.canvas.dlgSFCstep is None:
            self.canvas.dlgSFCstep=vgdSFCstep(self.canvas)
        self.canvas.dlgSFCstep.Centre()
        print 'dlg edit'
        self.canvas.dlgSFCstep.SetInfos(self,self.canvas)
        self.canvas.dlgSFCstep.SetBrowseDlg(self.canvas.GetStepBrowseDlg())
        self.canvas.dlgSFCstep.SetBrowseValDlg(self.canvas.GetStepBrowseValDlg())
        if self.canvas.dlgSFCstep.ShowModal()>0:
            self.canvas.__setModified__(True)
            return 1
        return 0
    
