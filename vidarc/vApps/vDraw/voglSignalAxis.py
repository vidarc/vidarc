#Boa:Dialog:vgdTimeAxis

import wx
import wx.lib.buttons
import wx.lib.ogl as ogl

import images

def create(parent):
    return vgdTimeAxis(parent)

[wxID_VGDTIMEAXIS, wxID_VGDTIMEAXISGCBBCANCEL, wxID_VGDTIMEAXISGCBBOK, 
 wxID_VGDTIMEAXISLBLCOUNT, wxID_VGDTIMEAXISSPNCOUNT, 
] = [wx.NewId() for _init_ctrls in range(5)]

class vgdTimeAxis(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VGDTIMEAXIS, name=u'vgdTimeAxis',
              parent=prnt, pos=wx.Point(325, 103), size=wx.Size(209, 148),
              style=wx.DEFAULT_DIALOG_STYLE, title=u'Time Axis')
        self.SetClientSize(wx.Size(201, 121))

        self.gcbbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDTIMEAXISGCBBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'gcbbOk',
              parent=self, pos=wx.Point(24, 80), size=wx.Size(76, 30), style=0)
        self.gcbbOk.Bind(wx.EVT_BUTTON, self.OnGcbbOkButton,
              id=wxID_VGDTIMEAXISGCBBOK)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDTIMEAXISGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(112, 80),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VGDTIMEAXISGCBBCANCEL)

        self.spnCount = wx.SpinCtrl(id=wxID_VGDTIMEAXISSPNCOUNT, initial=0,
              max=100, min=0, name=u'spnCount', parent=self, pos=wx.Point(64,
              16), size=wx.Size(117, 21), style=wx.SP_ARROW_KEYS)

        self.lblCount = wx.StaticText(id=wxID_VGDTIMEAXISLBLCOUNT,
              label=u'Count', name=u'lblCount', parent=self, pos=wx.Point(32,
              16), size=wx.Size(28, 13), style=0)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        img=images.getOkBitmap()
        self.gcbbOk.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)

    def OnGcbbOkButton(self, event):
        self.EndModal(1)
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
    def GetCount(self):
        return self.spnCount.GetValue()
    def SetCount(self,count):
        self.spnCount.SetValue(count)
class voglSignalAxis(ogl.CompositeShape):
    def __init__(self, canvas,signals,w=5, h=5,parent=None):
        self.dlg=None
        self.parent=parent
        self.w=w
        self.h=h
        self.signals=signals
        ogl.CompositeShape.__init__(self)
        
        self.SetCanvas(canvas)
        
        count=0
        for sig in signals:
            count+=sig[2]/sig[3]+1
            print count
        self.count=count
        self.signalShapes=[]
        
        line=ogl.LineShape()
        line.SetPen(wx.BLACK_PEN)
        line.SetBrush(wx.BLACK_BRUSH)
        line.MakeLineControlPoints(2)
        line.AddArrow(ogl.ARROW_ARROW)
        line.SetEnds(0,20+(self.count)*self.h,0,20)
        
        text=ogl.TextShape(60,40)
        text.AddText('Signal')
        text.SetX(0)
        text.SetY(0)
        self.text=text
                
        self.AddChild(line)
        self.AddChild(text)
        
        #constraint = ogl.Constraint(ogl.CONSTRAINT_MIDALIGNED_BOTTOM, constraining_shape, [constrained_shape1, constrained_shape2])
        #self.AddConstraint(constraint)
        self.Recompute()

        # If we don't do this, the shapes will be able to move on their
        # own, instead of moving the composite
        line.SetDraggable(False)
        text.SetDraggable(False)
        
        # If we don't do this the shape will take all left-clicks for itself
        line.SetSensitivityFilter(0)
        text.SetSensitivityFilter(0)
        self.line=line
        self.__process__()
    def __process__(self):
        count=0
        for sig in self.signals:
            count+=sig[2]/sig[3]+1
            print count
        self.count=count
        canvas=self.GetCanvas()
        diag=self.GetCanvas().GetDiagram()
        for shape in self.signalShapes:
            self.RemoveChild(shape)
            diag.RemoveShape(shape)
        self.signalShapes=[]
        b=self.line.GetBoundingBoxMax()
        x=self.line.GetX()-b[0]/2.0-self.w/2.0
        y=self.line.GetY()+b[1]/2.0#+self.count*self.h
        for sig in self.signals:
            print sig
            print x,y,self.w
            line=ogl.LineShape()
            line.SetPen(wx.BLACK_PEN)
            line.SetBrush(wx.BLACK_BRUSH)
            line.MakeLineControlPoints(2)
            line.SetEnds(x,y,x+self.w,y)
            self.signalShapes.append(line)
            self.AddChild(line)
            diag.AddShape(line)
            line.SetDraggable(False)
            line.SetSensitivityFilter(0)
            
            y-=(sig[2]/sig[3])*self.h
            
            line=ogl.LineShape()
            line.SetPen(wx.BLACK_PEN)
            line.SetBrush(wx.BLACK_BRUSH)
            line.MakeLineControlPoints(2)
            line.SetEnds(x,y,x+self.w,y)
            self.signalShapes.append(line)
            self.AddChild(line)
            canvas.AddShape(line)
            diag.AddShape(line)
            line.SetDraggable(False)
            line.SetSensitivityFilter(0)
            
            #text=ogl.TextShape(60,40)
            #text.AddText('Signal')
            #text.SetX(0)
            #text.SetY(0)
            #text.SetDraggable(False)
            #text.SetSensitivityFilter(0)
            #self.AddChild(text)
            
            y-=self.h
            print y
        #self.Recompute()
        self.CalculateSize()
    def GetOrigin(self):
        b=self.line.GetBoundingBoxMax()
        x=self.line.GetX()-b[0]/2.0
        y=self.line.GetY()+b[1]/2.0
        return (x,y)
    def __draw__(self):
        self.__process__()
        b=self.line.GetBoundingBoxMax()
        x=self.line.GetX()-b[0]/2.0
        y=self.line.GetY()-b[1]/2.0
        self.line.SetEnds(x,y+self.count*self.h,x,y)
        #self.text.SetX(x+self.count*self.w+30)
        self.Recompute()
    def IsSelectable(self):
        return 0
    def DoEdit(self):
        if self.dlg is None:
            self.dlg=vgdTimeAxis(self.parent)
        self.dlg.Centre()
        self.dlg.SetCount(self.count)
        if self.dlg.ShowModal()>0:
            oldCount=self.count
            self.count=self.dlg.GetCount()
            if oldCount!=self.count:
                self.ClearText()
                self.signals.append(('ll',0,1,1))
                self.__draw__()
                return 1
        return 0
    