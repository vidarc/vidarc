#Boa:Dialog:vgdFlowChartAnnot

import wx
from wx import WHITE_BRUSH
from wx import TRANSPARENT_BRUSH
from wx import BLACK_PEN
import wx.lib.ogl as ogl
import string,os,sys
import vidarc.vApps.vDraw.latex as latex
from vidarc.vApps.vDraw.voglFlowChartElement import voglFlowChartElement
import images


def create(parent):
    return vgdFlowChartAnnot(parent)

[wxID_VGDFLOWCHARTANNOT, wxID_VGDFLOWCHARTANNOTGCBBBROWSE, 
 wxID_VGDFLOWCHARTANNOTGCBBCANCEL, wxID_VGDFLOWCHARTANNOTGCBBOK, 
 wxID_VGDFLOWCHARTANNOTLBLINFO, wxID_VGDFLOWCHARTANNOTLBLSYMBOLDESC, 
 wxID_VGDFLOWCHARTANNOTLBLSYMBOLID, wxID_VGDFLOWCHARTANNOTLBLTEXT, 
 wxID_VGDFLOWCHARTANNOTTXTINFO, wxID_VGDFLOWCHARTANNOTTXTNAME, 
 wxID_VGDFLOWCHARTANNOTTXTSYMBOLDESC, wxID_VGDFLOWCHARTANNOTTXTSYMBOLID, 
 wxID_VGDFLOWCHARTANNOTTXTTEXT, 
] = [wx.NewId() for _init_ctrls in range(13)]

class vgdFlowChartAnnot(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VGDFLOWCHARTANNOT,
              name=u'vgdFlowChartAnnot', parent=prnt, pos=wx.Point(321, 69),
              size=wx.Size(440, 284), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'Flow Chart Annotation')
        self.SetClientSize(wx.Size(432, 257))

        self.txtInfo = wx.TextCtrl(id=wxID_VGDFLOWCHARTANNOTTXTINFO,
              name=u'txtInfo', parent=self, pos=wx.Point(48, 44),
              size=wx.Size(128, 44), style=wx.TE_MULTILINE, value=u'')

        self.lblInfo = wx.StaticText(id=wxID_VGDFLOWCHARTANNOTLBLINFO,
              label=u'Info', name=u'lblInfo', parent=self, pos=wx.Point(24, 48),
              size=wx.Size(18, 13), style=0)

        self.lblSymbolID = wx.StaticText(id=wxID_VGDFLOWCHARTANNOTLBLSYMBOLID,
              label=u'Identifier', name=u'lblSymbolID', parent=self,
              pos=wx.Point(8, 8), size=wx.Size(40, 13), style=0)

        self.txtSymbolID = wx.TextCtrl(id=wxID_VGDFLOWCHARTANNOTTXTSYMBOLID,
              name=u'txtSymbolID', parent=self, pos=wx.Point(56, 8),
              size=wx.Size(100, 21), style=0, value=u'')

        self.lblSymbolDesc = wx.StaticText(id=wxID_VGDFLOWCHARTANNOTLBLSYMBOLDESC,
              label=u'Description', name=u'lblSymbolDesc', parent=self,
              pos=wx.Point(160, 8), size=wx.Size(53, 13), style=0)

        self.txtSymbolDesc = wx.TextCtrl(id=wxID_VGDFLOWCHARTANNOTTXTSYMBOLDESC,
              name=u'txtSymbolDesc', parent=self, pos=wx.Point(216, 8),
              size=wx.Size(100, 21), style=0, value=u'')

        self.gcbbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTANNOTGCBBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'gcbbOk',
              parent=self, pos=wx.Point(112, 224), size=wx.Size(76, 30),
              style=0)
        self.gcbbOk.Bind(wx.EVT_BUTTON, self.OnGcbbOkButton,
              id=wxID_VGDFLOWCHARTANNOTGCBBOK)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTANNOTGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(224, 224),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VGDFLOWCHARTANNOTGCBBCANCEL)

        self.txtText = wx.TextCtrl(id=wxID_VGDFLOWCHARTANNOTTXTTEXT,
              name=u'txtText', parent=self, pos=wx.Point(48, 96),
              size=wx.Size(296, 120), style=wx.TE_MULTILINE, value=u'')
        self.txtText.Bind(wx.EVT_CHAR, self.OnTxtTextChar)

        self.lblText = wx.StaticText(id=wxID_VGDFLOWCHARTANNOTLBLTEXT,
              label=u'Text', name=u'lblText', parent=self, pos=wx.Point(16, 96),
              size=wx.Size(21, 13), style=0)

        self.txtName = wx.TextCtrl(id=wxID_VGDFLOWCHARTANNOTTXTNAME,
              name=u'txtName', parent=self, pos=wx.Point(328, 8),
              size=wx.Size(100, 21), style=0, value=u'')
        self.txtName.Enable(False)

        self.gcbbBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTANNOTGCBBBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbBrowse', parent=self, pos=wx.Point(352, 96),
              size=wx.Size(76, 30), style=0)
        self.gcbbBrowse.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseButton,
              id=wxID_VGDFLOWCHARTANNOTGCBBBROWSE)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        self.dlgBrowse=None
        self.dlgBrowseVal=None
        self.IDname=''
        self.IDval=''
        self.IDtext=[]
        
        img=images.getOkBitmap()
        self.gcbbOk.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
        
        img=images.getBrowseBitmap()
        self.gcbbBrowse.SetBitmapLabel(img)
        
    def __clear__(self):
        self.txtSymbolID.SetValue('')
        self.txtSymbolDesc.SetValue('')
        self.txtInfo.SetValue('')
        self.txtVal.SetValue('')
        self.txtName.SetValue('')
        self.txtText.SetValue('')
        self.IDtext=[]
    def SetBrowseDlg(self,dlg):
        self.dlgBrowse=dlg
    def SetInfos(self,voglFlowChartElement,voglFlowChart):
        #self.spnStepNr.SetValue(voglSFCstep.nr)
        self.voglFlowChart=voglFlowChart
        self.voglFlowChartElem=voglFlowChartElement
        self.txtSymbolID.SetValue(self.voglFlowChartElem.id)
        self.txtSymbolDesc.SetValue(self.voglFlowChartElem.desc)
        self.txtInfo.SetValue(self.voglFlowChartElem.info)
        #fix me
        #replace text
        if self.dlgBrowse is not None:
            self.IDtext=string.split(self.voglFlowChartElem.IDtext,u',')
            i,j=0,0
            s=self.voglFlowChartElem.text
            for id in self.IDtext:
                i=string.find(s,'$',j)
                if i>=0:
                    j=string.find(s,'$',i+1)
                    if j>0:
                        diff=j-i-1
                        sID=self.dlgBrowse.GetLinkInfoById(id)[1]
                        s=s[:i+1]+sID+s[j:]
                        j=j+1-diff+len(sID)
        self.txtText.SetValue(self.voglFlowChartElem.text)
        self.txtName.SetValue(self.voglFlowChartElem.getXmlType())
    def OnGcbbOkButton(self, event):
        #self.voglSFCstep.nr=self.txtNr.GetValue()
        self.voglFlowChartElem.id=self.txtSymbolID.GetValue()
        self.voglFlowChartElem.desc=self.txtSymbolDesc.GetValue()
        self.voglFlowChartElem.info=self.txtInfo.GetValue()
        self.voglFlowChartElem.text=self.txtText.GetValue()
        self.voglFlowChartElem.IDtext=string.join(self.IDtext,u',')
        self.EndModal(1)
        event.Skip()
    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
    def __replace__(self,strOld,strNew):
        i=string.find(strOld,'<')
        if i>=0:
            j=string.find(strOld,'>',i+1)
            if j>0:
                return strOld[:i+1]+strNew+strOld[j:]
        return '<'+strNew+'>'
    def __calcIdx__(self):
        s=self.txtText.GetValue()
        iLen=len(s)
        idx=0
        iPos=self.txtText.GetInsertionPoint()
        tup=self.txtText.PositionToXY(iPos)
        strs=string.split(s,'\n')
        iPos=0
        for i in range(0,tup[1]):
            iPos+=len(strs[i])+1
        iPos+=tup[0]
        i,j=0,0
        while i>=0:
            i=string.find(s,u'$',j)
            if i>=0:
                if i>=iPos:
                    return (idx,iPos,iPos)
                j=string.find(s,'$',i+1)
                if j>0:
                    if j<iPos:
                        idx+=1
                        j+=1
                    else:
                        return (idx,i,j+1)
                i=j
        return (idx,iPos,iPos)
    def OnGcbbBrowseButton(self, event):
        if self.dlgBrowse is not None:
            self.dlgBrowse.Centre()
            if self.dlgBrowse.ShowModal()>0:
                id,s=self.dlgBrowse.GetLinkInfo()
                idxTup=self.__calcIdx__()
                idx=idxTup[0]
                if idxTup[1]!=idxTup[2]:
                    self.IDtext.remove(self.IDtext[idx])
                self.IDtext.insert(idx,id)
                sOld=self.txtText.GetValue()
                sNew=sOld[:idxTup[1]]+u'$'+s+u'$'+sOld[idxTup[2]:]
                self.txtText.SetValue(sNew)
        event.Skip()
    def OnTxtTextChar(self, event):
        if event.GetKeyCode() in [8,127]:
            s=self.txtText.GetValue()
            iLen=len(s)
            idx=0
            iPos=self.txtText.GetInsertionPoint()
            tup=self.txtText.PositionToXY(iPos)
            strs=string.split(s,'\n')
            iPos=0
            for i in range(0,tup[1]):
                iPos+=len(strs[i])+1
            iPos+=tup[0]
            i,j=0,0
            lstIDs=[]
            while i>=0:
                i=string.find(s,u'$',j)
                if i>=0:
                    j=string.find(s,'$',i+1)
                    if j>0:
                        if j<iPos:
                            idx+=1
                            j+=1
                        else:
                            lstIDs.append((idx,i,j+1))
                    i=j
            idx=0
            for tup in lstIDs:
                iStart=tup[1]
                if event.GetKeyCode()==8:
                    iStart+=1
                if iPos>=iStart and iPos<=tup[2]:
                    self.IDtext.remove(self.IDtext[idx])
                    sOld=self.txtText.GetValue()
                    sNew=sOld[:tup[1]]+sOld[tup[2]:]
                    self.txtText.SetValue(sNew)
                    return
                idx+=1
        event.Skip()


#----------------------------------------------------------------------

class voglFlowChartAnnotation(voglFlowChartElement):
    def __init__(self, canvas,id,desc,info,w=0.0, h=0.0):
        voglFlowChartElement.__init__(self, canvas, id,desc,info, w, h)
        self.bAnnotation=True
        
        dw=w/6.0
        dh=h/4.0
        
        obj= ogl.DrawnShape()
        obj.SetDrawnPen(BLACK_PEN)
        dw=w/6.0
        dh=h/4.0
        obj.DrawLine((-dw*2.5, -dh*2.0), (-dw*3.0, -dh*2.0))
        obj.DrawLine((-dw*3.0, -dh*2.0), (-dw*3.0, +dh*2.0))
        obj.DrawLine((-dw*3.0, +dh*2.0), (-dw*2.5, +dh*2.0))
        obj.SetDraggable(False)
        obj.SetSensitivityFilter(0,True)
        obj.CalculateSize()
        self.AddChild(obj)
        
        self.__addSymbol__()
        self.SetSensitivityFilter(0)
        self.Recompute()
        self.SetDraggable(True)
    def getXmlType(self):
        return 'annotation'
    def appendLatexPic(self,strs,yMax,scaleX,scaleY):
        x=self.symbol.GetX()/scaleX
        y=self.symbol.GetY()/scaleY
        ba=self.symbol.GetBoundingBoxMax()
        xa=x-(ba[0]/2.0/scaleX)
        ya=y-(ba[1]/2.0/scaleY)
        xh1,yh1=xa+(self.w/6.0/scaleX),yMax-(y-(ba[1]/2.0/scaleY))
        xh2,yh2=xa+(self.w/6.0/scaleX),yMax-(y+(ba[1]/2.0/scaleY))
        
        tup=latex.lineFit(xh1,yh1,xa,yh1)
        strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
        tup=latex.lineFit(xa,yh1,xa,yh2)
        strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
        tup=latex.lineFit(xa,yh2,xh1,yh2)
        strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
        voglFlowChartElement.appendLatexPic(self,strs,yMax,scaleX,scaleY)
        return xa,ya
    def isData(self):
        return True
    def isProg(self):
        return True
    def isSys(self):
        return True
    def isNet(self):
        return True
    def isRes(self):
        return True
    def isAnnotation(self):
        return True
    def DoEdit(self):
        if self.canvas.dlgFlowChartAnnot is None:
            self.canvas.dlgFlowChartAnnot=vgdFlowChartAnnot(self.canvas)
        self.canvas.dlgFlowChartAnnot.Centre()
        self.canvas.dlgFlowChartAnnot.SetBrowseDlg(self.canvas.GetBrowseDlg())
        self.canvas.dlgFlowChartAnnot.SetInfos(self,self.canvas)
        if self.canvas.dlgFlowChartAnnot.ShowModal()>0:
            self.canvas.__setModified__(True)
            canvas = self.canvas
            dc = wx.ClientDC(canvas)
            canvas.PrepareDC(dc)
        
            self.UpdateLinksRec(dc)
            
            return 1
        return 0
    