#Boa:Frame:vDrawMainFrame
#----------------------------------------------------------------------------
# Name:         vDrawMainFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vDrawMainFrame.py,v 1.1 2005/12/13 13:19:34 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

from vidarc.vApps.vDraw.vDrawCanvasOGL import *
from vidarc.vApps.vDraw.vDrawTimingCanvasOGL import *
from vidarc.vApps.vDraw.vDrawSFCCanvasOGL import *
from vidarc.vApps.vDraw.vDrawSFCspecCanvasOGL import *
from vidarc.vApps.vDraw.vDrawS88stateCanvasOGL import *
from vidarc.vApps.vDraw.vDrawFlowChartDataCanvasOGL import *

def create(parent):
    return vDrawMainFrame(parent)

[wxID_VDRAWMAINFRAME] = [wx.NewId() for _init_ctrls in range(1)]

class vDrawMainFrame(wx.Frame):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VDRAWMAINFRAME, name=u'vDrawMainFrame',
              parent=prnt, pos=wx.Point(22, 22), size=wx.Size(600, 580),
              style=wx.DEFAULT_FRAME_STYLE, title=u'Draw Main')
        self.SetClientSize(wx.Size(592, 553))

    def __init__(self, parent):
        self._init_ctrls(parent)

        ogl.OGLInitialize()
        iTest=5
        if iTest==0:
            self.oglDraw=vDrawCanvasOGL(parent=self,pos=wx.Point(50,10),
                            size=wx.Size(300,200))
        elif iTest==1:
            self.oglDraw=vDrawTimingCanvasOGL(parent=self,pos=wx.Point(50,10),
                            size=wx.Size(300,200))
        elif iTest==2:
            self.oglDraw=vDrawSFCspecCanvasOGL(parent=self,pos=wx.Point(50,10),
                            size=wx.Size(300,200))
        elif iTest==3:
            self.oglDraw=vDrawSFCCanvasOGL(parent=self,pos=wx.Point(50,10),
                            size=wx.Size(300,200))
        elif iTest==4:
            self.oglDraw=vDrawS88stateCanvasOGL(parent=self,pos=wx.Point(50,10),
                            size=wx.Size(500,300))
        elif iTest==5:
            self.oglDraw=vDrawFlowChartDataCanvasOGL(parent=self,pos=wx.Point(50,10),
                            size=wx.Size(300,200))
            
