#Boa:Dialog:vgdSFCstepActions
#----------------------------------------------------------------------------
# Name:         voglSFCstepActions.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: voglSFCstepActions.py,v 1.1 2005/12/13 13:19:34 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import wx.lib.ogl as ogl

import sys,string
#import vidarc.apps.xmlBase.vtXmlDomTree as vtXmlDomTree

import images

def create(parent):
    return vgdSFCstepActions(parent)

[wxID_VGDSFCSTEPACTIONS, wxID_VGDSFCSTEPACTIONSCHCOPERATOR, 
 wxID_VGDSFCSTEPACTIONSGCBBADDACTION, wxID_VGDSFCSTEPACTIONSGCBBBROWSE, 
 wxID_VGDSFCSTEPACTIONSGCBBBROWSEVAL, wxID_VGDSFCSTEPACTIONSGCBBCANCEL, 
 wxID_VGDSFCSTEPACTIONSGCBBDELACTION, wxID_VGDSFCSTEPACTIONSGCBBOK, 
 wxID_VGDSFCSTEPACTIONSGCBBSET, wxID_VGDSFCSTEPACTIONSLBLDESC, 
 wxID_VGDSFCSTEPACTIONSLBLNAME, wxID_VGDSFCSTEPACTIONSLBLOPER, 
 wxID_VGDSFCSTEPACTIONSLBLVAL, wxID_VGDSFCSTEPACTIONSLSTACTION, 
 wxID_VGDSFCSTEPACTIONSTXTACTION, wxID_VGDSFCSTEPACTIONSTXTDESC, 
 wxID_VGDSFCSTEPACTIONSTXTVAL, 
] = [wx.NewId() for _init_ctrls in range(17)]

def lstSortActions(i,j,data):
    print i,j,data
    
    return 0

class vgdSFCstepActions(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VGDSFCSTEPACTIONS,
              name=u'vgdSFCstepActions', parent=prnt, pos=wx.Point(317, 95),
              size=wx.Size(458, 361), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'SFC Step Actions')
        self.SetClientSize(wx.Size(450, 334))

        self.gcbbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPACTIONSGCBBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'gcbbOk',
              parent=self, pos=wx.Point(144, 296), size=wx.Size(76, 30),
              style=0)
        self.gcbbOk.Bind(wx.EVT_BUTTON, self.OnGcbbOkButton,
              id=wxID_VGDSFCSTEPACTIONSGCBBOK)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPACTIONSGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(248, 296),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VGDSFCSTEPACTIONSGCBBCANCEL)

        self.txtAction = wx.TextCtrl(id=wxID_VGDSFCSTEPACTIONSTXTACTION,
              name=u'txtAction', parent=self, pos=wx.Point(64, 136),
              size=wx.Size(296, 21), style=0, value=u'')

        self.lstAction = wx.ListView(id=wxID_VGDSFCSTEPACTIONSLSTACTION,
              name=u'lstAction', parent=self, pos=wx.Point(16, 8),
              size=wx.Size(344, 100), style=wx.LC_REPORT)
        self.lstAction.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstActionListItemSelected,
              id=wxID_VGDSFCSTEPACTIONSLSTACTION)

        self.gcbbAddAction = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPACTIONSGCBBADDACTION,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add',
              name=u'gcbbAddAction', parent=self, pos=wx.Point(368, 48),
              size=wx.Size(76, 30), style=0)
        self.gcbbAddAction.Bind(wx.EVT_BUTTON, self.OnGcbbAddActionButton,
              id=wxID_VGDSFCSTEPACTIONSGCBBADDACTION)

        self.gcbbDelAction = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPACTIONSGCBBDELACTION,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Del',
              name=u'gcbbDelAction', parent=self, pos=wx.Point(368, 8),
              size=wx.Size(76, 30), style=0)
        self.gcbbDelAction.Bind(wx.EVT_BUTTON, self.OnGcbbDelActionButton,
              id=wxID_VGDSFCSTEPACTIONSGCBBDELACTION)

        self.gcbbBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPACTIONSGCBBBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbBrowse', parent=self, pos=wx.Point(368, 132),
              size=wx.Size(76, 30), style=0)
        self.gcbbBrowse.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseButton,
              id=wxID_VGDSFCSTEPACTIONSGCBBBROWSE)

        self.gcbbSet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPACTIONSGCBBSET,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Set', name=u'gcbbSet',
              parent=self, pos=wx.Point(368, 228), size=wx.Size(76, 30),
              style=0)
        self.gcbbSet.Bind(wx.EVT_BUTTON, self.OnGcbbSetButton,
              id=wxID_VGDSFCSTEPACTIONSGCBBSET)

        self.txtDesc = wx.TextCtrl(id=wxID_VGDSFCSTEPACTIONSTXTDESC,
              name=u'txtDesc', parent=self, pos=wx.Point(64, 224),
              size=wx.Size(296, 64), style=wx.TE_MULTILINE, value=u'')

        self.lblName = wx.StaticText(id=wxID_VGDSFCSTEPACTIONSLBLNAME,
              label=u'Name', name=u'lblName', parent=self, pos=wx.Point(24,
              140), size=wx.Size(28, 13), style=0)

        self.lblDesc = wx.StaticText(id=wxID_VGDSFCSTEPACTIONSLBLDESC,
              label=u'Description', name=u'lblDesc', parent=self,
              pos=wx.Point(8, 224), size=wx.Size(53, 13), style=0)

        self.chcOperator = wx.Choice(choices=[u'', u'=', u'?'],
              id=wxID_VGDSFCSTEPACTIONSCHCOPERATOR, name=u'chcOperator',
              parent=self, pos=wx.Point(64, 164), size=wx.Size(44, 21),
              style=0)

        self.txtVal = wx.TextCtrl(id=wxID_VGDSFCSTEPACTIONSTXTVAL,
              name=u'txtVal', parent=self, pos=wx.Point(64, 192),
              size=wx.Size(296, 21), style=0, value=u'')

        self.gcbbBrowseVal = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPACTIONSGCBBBROWSEVAL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbBrowseVal', parent=self, pos=wx.Point(368, 188),
              size=wx.Size(76, 30), style=0)
        self.gcbbBrowseVal.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseValButton,
              id=wxID_VGDSFCSTEPACTIONSGCBBBROWSEVAL)

        self.lblVal = wx.StaticText(id=wxID_VGDSFCSTEPACTIONSLBLVAL,
              label=u'Value', name=u'lblVal', parent=self, pos=wx.Point(32,
              196), size=wx.Size(27, 13), style=0)

        self.lblOper = wx.StaticText(id=wxID_VGDSFCSTEPACTIONSLBLOPER,
              label=u'Operation', name=u'lblOper', parent=self, pos=wx.Point(8,
              168), size=wx.Size(46, 13), style=0)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        self.sortOrder=0
        self.dlgBrowse=None
        self.dlgBrowseVal=None
        self.IDname=''
        self.IDval=''
        
        img=images.getOkBitmap()
        self.gcbbOk.SetBitmapLabel(img)
        self.gcbbSet.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
        
        img=images.getDelBitmap()
        self.gcbbDelAction.SetBitmapLabel(img)
        
        img=images.getAddBitmap()
        self.gcbbAddAction.SetBitmapLabel(img)
        
        img=images.getBrowseBitmap()
        self.gcbbBrowse.SetBitmapLabel(img)
        self.gcbbBrowseVal.SetBitmapLabel(img)
        
        self.lstAction.InsertColumn(0,u'Nr',wx.LIST_FORMAT_LEFT,40)
        self.lstAction.InsertColumn(1,u'Name',wx.LIST_FORMAT_LEFT,120)
        self.lstAction.InsertColumn(2,u'Oper',wx.LIST_FORMAT_LEFT,40)
        self.lstAction.InsertColumn(3,u'Value',wx.LIST_FORMAT_LEFT,120)
        
        self.__setEnable__(False)
        self.__clear__()
    def __setEnable__(self,state):
        #self.txtAction.Enable(state)
        #self.txtDesc.Enable(state)
        #self.txtVal.Enable(state)
        #self.chcOperator.Enable(state)
        self.gcbbSet.Enable(state)
        #self.gcbbBrowse.Enable(state)
        #self.gcbbBrowseVal.Enable(state)
    def __clear__(self):
        self.txtAction.SetValue('')
        self.txtDesc.SetValue('')
        self.txtVal.SetValue('')
        self.chcOperator.SetSelection(0)
    def SetInfos(self,voglSFCstepActions):
        #self.spnStepNr.SetValue(voglSFCstep.nr)
        self.voglSFCstepActions=voglSFCstepActions
        self.actions=self.voglSFCstepActions.GetActions()
        self.__updateActions__()
    def GetActions(self):
        return self.actions
    def SetBrowseDlg(self,dlg):
        self.dlgBrowse=dlg
    def SetBrowseValDlg(self,dlg):
        self.dlgBrowseVal=dlg
    def __updateActions__(self):
        # build steps
        self.selIdx=-1
        self.IDname=''
        self.IDval=''
        self.lstAction.DeleteAllItems()
        self.__setEnable__(False)
        self.__clear__()
        actNames=[]
        i=0
        for a in self.actions:
            actNames.append((a['name'],i))
            i+=1
        def cmpAct(x,y):
            return cmp(x[0],y[0])
        actNames.sort(cmpAct)
        if self.sortOrder>0:
            actNames.reverse()
        i=0
        for a in actNames:
            index = self.lstAction.InsertImageStringItem(sys.maxint,"%03d"%i , -1)
            self.lstAction.SetStringItem(index,1,a[0],-1)
            try:
                sOp=self.actions[a[1]]['operation']
            except:
                sOp=''
            self.lstAction.SetStringItem(index,2,sOp,-1)
            try:
                sVal=self.actions[a[1]]['val']
            except:
                sVal=''
            self.lstAction.SetStringItem(index,3,sVal,-1)
            self.lstAction.SetItemData(index,a[1])
            i+=1
            pass
        
    def OnGcbbOkButton(self, event):
        if self.selIdx>=0:
            self.lstAction.SetStringItem(self.idx,1,self.txtAction.GetValue())
            self.lstAction.SetStringItem(self.idx,2,self.chcOperator.GetStringSelection())
            self.lstAction.SetStringItem(self.idx,3,self.txtVal.GetValue())
            self.actions[self.selIdx]['name']=self.txtAction.GetValue()
            self.actions[self.selIdx]['desc']=self.txtDesc.GetValue()
            self.actions[self.selIdx]['operation']=self.chcOperator.GetStringSelection()
            self.actions[self.selIdx]['val']=self.txtVal.GetValue()
            self.actions[self.selIdx]['fid_name']=self.IDname
            self.actions[self.selIdx]['fid_val']=self.IDval
        
        actNames=[]
        i=0
        for a in self.actions:
            actNames.append((a['name'],i))
            i+=1
        def cmpAct(x,y):
            return cmp(x[0],y[0])
        actNames.sort(cmpAct)
        
        acts=[]
        for a in actNames:
            acts.append(self.actions[a[1]])
        self.actions=acts
        self.EndModal(1)
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
    def GetCount(self):
        return self.spnCount.GetValue()
    def SetCount(self,count):
        self.spnCount.SetValue(count)

    def OnGcbbAddButton(self, event):
        event.Skip()

    def OnGcbbDelButton(self, event):
        event.Skip()

    def OnLstActionListItemSelected(self, event):
        self.__setEnable__(True)
        self.idx=event.GetIndex()
        it=self.lstAction.GetItem(self.idx,1)
        self.txtAction.SetValue(it.m_text)
        self.selIdx=self.lstAction.GetItemData(self.idx)
        try:
            self.txtDesc.SetValue(self.actions[self.selIdx]['desc'])
        except:
            self.txtDesc.SetValue('')
        try:
            self.chcOperator.SetStringSelection(self.actions[self.selIdx]['operation'])
        except:
            self.chcOperator.SetSelection(0)
        try:
            self.txtVal.SetValue(self.actions[self.selIdx]['val'])
        except:
            self.txtVal.SetValue('')
        try:
            self.IDname=self.action[self.selIdx]['fid_name']
        except:
            self.IDname=''
        try:
            self.IDval=self.action[self.selIdx]['fid_val']
        except:
            self.IDval=''
        event.Skip()

    def OnGcbbAddActionButton(self, event):
        sName=self.txtAction.GetValue()
        sDesc=self.txtDesc.GetValue()
        sOper=self.chcOperator.GetStringSelection()
        sVal=self.txtVal.GetValue()
        self.actions.append({'name':sName,'fid_name':self.IDname,'operation':sOper,
                'val':sVal,'fid_val':self.IDval,'desc':sDesc})
        self.__updateActions__()
        event.Skip()

    def OnGcbbDelActionButton(self, event):
        if self.selIdx<0:
            return
        self.actions=self.actions[:self.selIdx]+self.actions[self.selIdx+1:]
        self.txtAction.SetValue('')
        self.txtDesc.SetValue('')
        self.txtVal.SetValue('')
        self.IDname=''
        self.IDval=''
        self.__updateActions__()
        event.Skip()
    def __replace__(self,strOld,strNew):
        i=string.find(strOld,'<')
        if i>=0:
            j=string.find(strOld,'>',i+1)
            if j>0:
                return strOld[:i+1]+strNew+strOld[j:]
        return '<'+strNew+'>'
    def OnGcbbBrowseButton(self, event):
        if self.dlgBrowse is not None:
            self.dlgBrowse.Centre()
            if self.dlgBrowse.ShowModal()>0:
                s=self.dlgBrowse.GetTagName()
                self.IDname=self.dlgBrowse.GetId()
                sOld=self.txtAction.GetValue()
                s=self.__replace__(sOld,s)
                self.txtAction.SetValue(s)
        event.Skip()

    def OnGcbbSetButton(self, event):
        if self.selIdx<0:
            return
        self.lstAction.SetStringItem(self.idx,1,self.txtAction.GetValue())
        self.lstAction.SetStringItem(self.idx,2,self.chcOperator.GetStringSelection())
        self.lstAction.SetStringItem(self.idx,3,self.txtVal.GetValue())
        self.actions[self.selIdx]['name']=self.txtAction.GetValue()
        self.actions[self.selIdx]['desc']=self.txtDesc.GetValue()
        self.actions[self.selIdx]['operation']=self.chcOperator.GetStringSelection()
        self.actions[self.selIdx]['val']=self.txtVal.GetValue()
        self.actions[self.selIdx]['fid_name']=self.IDname
        self.actions[self.selIdx]['fid_val']=self.IDval
        event.Skip()
    def OnGcbbBrowseValButton(self, event):
        if self.dlgBrowseVal is not None:
            self.dlgBrowseVal.Centre()
            if self.dlgBrowseVal.ShowModal()>0:
                s=self.dlgBrowseVal.GetTagName()
                self.IDval=self.dlgBrowseVal.GetId()
                s=self.__replace__(self.txtVal.GetValue(),s)
                self.txtVal.SetValue(s)
        event.Skip()
class voglSFCstepActions(ogl.DividedShape):
    def __init__(self, step,actions, canvas):
        w=0.0
        h=0.0
        self.step=step
        self.actions=actions
        self.canvas=canvas
        lstHeights=[]
        for a in actions:
            tmpHeight=h
            strs=string.split(a['name'],'\n')
            for s in strs:
                l=len(s)
                if l>w:
                    w=l
                h+=15.0
            h+=15.0
            lstHeights.append(h-tmpHeight)
        grid=self.canvas.GetDiagram().GetGridSpacing()
        #w=grid * int(w*8 / grid + 0.5)
        ba=step.GetBoundingBoxMax()
        wa=ba[0]/2.0+grid
        w=grid * int(w*8 / grid + 1.5)
        w=2.0 *grid*(int((wa+w)/2.0)/grid + 0.5)-wa
        #w=w*8
        #print w
        #print grid, w
        ogl.DividedShape.__init__(self, w, h)
        self.__updateActions__()
    def __updateActions__(self):
        w=0.0
        h=0.0
        lstHeights=[]
        for a in self.actions:
            tmpHeight=h
            strs=string.split(a['name'],'\n')
            for s in strs:
                l=len(s)
                if l>w:
                    w=l
                h+=15.0
            h+=15.0
            lstHeights.append(h-tmpHeight)
        grid=self.canvas.GetDiagram().GetGridSpacing()
        #w=grid * int(w*8 / grid + 1.5)
        ba=self.step.GetBoundingBoxMax()
        wa=ba[0]/2.0+grid
        w=grid * int(w*8 / grid + 1.5)
        w=2.0 *grid*(int((wa+w)/2.0)/grid + 0.5)-wa
        #w=grid * int(w*8 / grid + 0.5)
        #w=w*8
        self.SetSize(w,h)
        self.ClearRegions()
        for i in range(0,len(self.actions)):
            region1 = ogl.ShapeRegion()
            region1.SetText(self.actions[i]['name'])
            region1.SetProportions(0.0, lstHeights[i]/h)
            region1.SetFormatMode(ogl.FORMAT_CENTRE_HORIZ|ogl.FORMAT_CENTRE_VERT)
            self.AddRegion(region1)
            #region1.SetSensitivityFilter(0)
        self.SetRegionSizes()
        self.ReformatRegions(self.canvas)
    def GetActions(self):
        return self.actions
    def GetFirstRegion(self):
        try:
            return self.GetRegions()[0]
        except:
            return None
    def ReformatRegions(self, canvas=None):
        rnum = 0

        if canvas is None:
            canvas = self.GetCanvas()

        dc = wx.ClientDC(canvas)  # used for measuring

        for region in self.GetRegions():
            text = region.GetText()
            self.FormatText(dc, text, rnum)
            rnum += 1
            
    def OnSizingEndDragLeft(self, pt, x, y, keys, attch):
        #print "***", self
        #ogl.DividedShape.OnSizingEndDragLeft(self, pt, x, y, keys, attch)
        #self.SetRegionSizes()
        #self.ReformatRegions()
        #self.GetCanvas().Refresh()
        pass
    def OnLeftClick(self, x, y, keys = 0, attachment = 0):
        #print "left clicked"
        self.step.updateSelect()
    def OnRightClick(self, x, y, keys = 0, attachment = 0):
        #self.EditRegions()
        #print "right clicked"
        if self.canvas.dlgSFCstepActions is None:
            self.canvas.dlgSFCstepActions=vgdSFCstepActions(self.canvas)
        self.canvas.dlgSFCstepActions.Centre()
        self.canvas.dlgSFCstepActions.SetInfos(self)
        self.canvas.dlgSFCstepActions.SetBrowseDlg(self.canvas.GetActionBrowseDlg())
        self.canvas.dlgSFCstepActions.SetBrowseValDlg(self.canvas.GetActionBrowseValDlg())
        
        if self.canvas.dlgSFCstepActions.ShowModal()>0:
            self.actions=self.canvas.dlgSFCstepActions.GetActions()
            self.__updateActions__()
            self.step.Update()
            self.canvas.__setModified__(False)
        pass
    def SetNode(self,node,doc):
        self.node=node
        self.doc=doc
    def GetNode(self):
        pass
