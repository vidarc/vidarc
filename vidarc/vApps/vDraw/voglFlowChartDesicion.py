#Boa:Dialog:vgdFlowChartDesic

from wx import WHITE_BRUSH
from wx import TRANSPARENT_BRUSH
from wx import BLACK_PEN
import wx
import wx.lib.buttons
import wx.lib.ogl as ogl
import string,os,sys
import vidarc.vApps.vDraw.latex as latex
from vidarc.vApps.vDraw.voglFlowChartElement import voglFlowChartElement
import images


def create(parent):
    return vgdFlowChartDesic(parent)

[wxID_VGDFLOWCHARTDESIC, wxID_VGDFLOWCHARTDESICCHCMULTIPLE, 
 wxID_VGDFLOWCHARTDESICCHCPARALLEL, wxID_VGDFLOWCHARTDESICGCBBANNOTATION, 
 wxID_VGDFLOWCHARTDESICGCBBBROWSE, wxID_VGDFLOWCHARTDESICGCBBCANCEL, 
 wxID_VGDFLOWCHARTDESICGCBBDEL, wxID_VGDFLOWCHARTDESICGCBBOK, 
 wxID_VGDFLOWCHARTDESICGCBBSET, wxID_VGDFLOWCHARTDESICLBLCOND, 
 wxID_VGDFLOWCHARTDESICLBLINFO, wxID_VGDFLOWCHARTDESICLBLSYMBOLDESC, 
 wxID_VGDFLOWCHARTDESICLBLSYMBOLID, wxID_VGDFLOWCHARTDESICLBLTEXT, 
 wxID_VGDFLOWCHARTDESICLSTFOLLOW, wxID_VGDFLOWCHARTDESICSPNNUM, 
 wxID_VGDFLOWCHARTDESICTXTCONDITION, wxID_VGDFLOWCHARTDESICTXTINFO, 
 wxID_VGDFLOWCHARTDESICTXTNAME, wxID_VGDFLOWCHARTDESICTXTSYMBOLDESC, 
 wxID_VGDFLOWCHARTDESICTXTSYMBOLID, wxID_VGDFLOWCHARTDESICTXTTEXT, 
] = [wx.NewId() for _init_ctrls in range(22)]

class vgdFlowChartDesic(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VGDFLOWCHARTDESIC,
              name=u'vgdFlowChartDesic', parent=prnt, pos=wx.Point(321, 69),
              size=wx.Size(440, 446), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'Flow Chart Desicion')
        self.SetClientSize(wx.Size(432, 419))

        self.txtInfo = wx.TextCtrl(id=wxID_VGDFLOWCHARTDESICTXTINFO,
              name=u'txtInfo', parent=self, pos=wx.Point(48, 44),
              size=wx.Size(128, 44), style=wx.TE_MULTILINE, value=u'')

        self.lblInfo = wx.StaticText(id=wxID_VGDFLOWCHARTDESICLBLINFO,
              label=u'Info', name=u'lblInfo', parent=self, pos=wx.Point(24, 48),
              size=wx.Size(18, 13), style=0)

        self.lstFollow = wx.ListView(id=wxID_VGDFLOWCHARTDESICLSTFOLLOW,
              name=u'lstFollow', parent=self, pos=wx.Point(8, 232),
              size=wx.Size(336, 112), style=wx.LC_REPORT)
        self.lstFollow.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstFollowListItemSelected,
              id=wxID_VGDFLOWCHARTDESICLSTFOLLOW)

        self.gcbbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTDESICGCBBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Delete', name=u'gcbbDel',
              parent=self, pos=wx.Point(352, 238), size=wx.Size(76, 30),
              style=0)
        self.gcbbDel.Bind(wx.EVT_BUTTON, self.OnGcbbDelButton,
              id=wxID_VGDFLOWCHARTDESICGCBBDEL)

        self.lblSymbolID = wx.StaticText(id=wxID_VGDFLOWCHARTDESICLBLSYMBOLID,
              label=u'Identifier', name=u'lblSymbolID', parent=self,
              pos=wx.Point(8, 8), size=wx.Size(40, 13), style=0)

        self.txtSymbolID = wx.TextCtrl(id=wxID_VGDFLOWCHARTDESICTXTSYMBOLID,
              name=u'txtSymbolID', parent=self, pos=wx.Point(56, 8),
              size=wx.Size(100, 21), style=0, value=u'')

        self.lblSymbolDesc = wx.StaticText(id=wxID_VGDFLOWCHARTDESICLBLSYMBOLDESC,
              label=u'Description', name=u'lblSymbolDesc', parent=self,
              pos=wx.Point(160, 8), size=wx.Size(53, 13), style=0)

        self.txtSymbolDesc = wx.TextCtrl(id=wxID_VGDFLOWCHARTDESICTXTSYMBOLDESC,
              name=u'txtSymbolDesc', parent=self, pos=wx.Point(216, 8),
              size=wx.Size(100, 21), style=0, value=u'')

        self.gcbbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTDESICGCBBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'gcbbOk',
              parent=self, pos=wx.Point(112, 384), size=wx.Size(76, 30),
              style=0)
        self.gcbbOk.Bind(wx.EVT_BUTTON, self.OnGcbbOkButton,
              id=wxID_VGDFLOWCHARTDESICGCBBOK)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTDESICGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(224, 384),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VGDFLOWCHARTDESICGCBBCANCEL)

        self.txtText = wx.TextCtrl(id=wxID_VGDFLOWCHARTDESICTXTTEXT,
              name=u'txtText', parent=self, pos=wx.Point(48, 96),
              size=wx.Size(296, 120), style=wx.TE_MULTILINE, value=u'')
        self.txtText.Bind(wx.EVT_CHAR, self.OnTxtTextChar)

        self.lblText = wx.StaticText(id=wxID_VGDFLOWCHARTDESICLBLTEXT,
              label=u'Text', name=u'lblText', parent=self, pos=wx.Point(16, 96),
              size=wx.Size(21, 13), style=0)

        self.txtName = wx.TextCtrl(id=wxID_VGDFLOWCHARTDESICTXTNAME,
              name=u'txtName', parent=self, pos=wx.Point(328, 8),
              size=wx.Size(100, 21), style=0, value=u'')
        self.txtName.Enable(False)

        self.chcMultiple = wx.CheckBox(id=wxID_VGDFLOWCHARTDESICCHCMULTIPLE,
              label=u'Multiple', name=u'chcMultiple', parent=self,
              pos=wx.Point(352, 320), size=wx.Size(73, 13), style=0)
        self.chcMultiple.SetValue(False)
        self.chcMultiple.Bind(wx.EVT_CHECKBOX, self.OnChcMultipleCheckbox,
              id=wxID_VGDFLOWCHARTDESICCHCMULTIPLE)

        self.gcbbSet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTDESICGCBBSET,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Set', name=u'gcbbSet',
              parent=self, pos=wx.Point(352, 280), size=wx.Size(76, 30),
              style=0)
        self.gcbbSet.Bind(wx.EVT_BUTTON, self.OnGcbbSetButton,
              id=wxID_VGDFLOWCHARTDESICGCBBSET)

        self.gcbbAnnotation = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTDESICGCBBANNOTATION,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Annotation',
              name=u'gcbbAnnotation', parent=self, pos=wx.Point(352, 40),
              size=wx.Size(76, 30), style=0)
        self.gcbbAnnotation.Bind(wx.EVT_BUTTON, self.OnGcbbAnnotationButton,
              id=wxID_VGDFLOWCHARTDESICGCBBANNOTATION)

        self.chcParallel = wx.CheckBox(id=wxID_VGDFLOWCHARTDESICCHCPARALLEL,
              label=u'Parallel', name=u'chcParallel', parent=self,
              pos=wx.Point(216, 40), size=wx.Size(73, 13), style=0)
        self.chcParallel.SetValue(False)
        self.chcParallel.Bind(wx.EVT_CHECKBOX, self.OnChcParallelCheckbox,
              id=wxID_VGDFLOWCHARTDESICCHCPARALLEL)

        self.gcbbBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTDESICGCBBBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbBrowse', parent=self, pos=wx.Point(352, 96),
              size=wx.Size(76, 30), style=0)
        self.gcbbBrowse.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseButton,
              id=wxID_VGDFLOWCHARTDESICGCBBBROWSE)

        self.txtCondition = wx.TextCtrl(id=wxID_VGDFLOWCHARTDESICTXTCONDITION,
              name=u'txtCondition', parent=self, pos=wx.Point(64, 352),
              size=wx.Size(144, 21), style=0, value=u'')

        self.lblCond = wx.StaticText(id=wxID_VGDFLOWCHARTDESICLBLCOND,
              label=u'Condition', name=u'lblCond', parent=self, pos=wx.Point(8,
              356), size=wx.Size(44, 13), style=0)

        self.spnNum = wx.SpinCtrl(id=wxID_VGDFLOWCHARTDESICSPNNUM, initial=0,
              max=100, min=0, name=u'spnNum', parent=self, pos=wx.Point(224,
              352), size=wx.Size(60, 21), style=wx.SP_ARROW_KEYS)
        self.spnNum.SetValue(0)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        self.dlgBrowse=None
        self.dlgBrowseVal=None
        self.IDname=''
        self.IDval=''
        self.IDtext=[]
        
        img=images.getOkBitmap()
        self.gcbbOk.SetBitmapLabel(img)
        self.gcbbSet.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
        
        img=images.getDelBitmap()
        self.gcbbDel.SetBitmapLabel(img)
        
        img=images.getNoteBitmap()
        self.gcbbAnnotation.SetBitmapLabel(img)
        
        img=images.getBrowseBitmap()
        self.gcbbBrowse.SetBitmapLabel(img)
        
        self.lstFollow.InsertColumn(0,u'Nr',wx.LIST_FORMAT_LEFT,30)
        self.lstFollow.InsertColumn(1,u'ID',wx.LIST_FORMAT_LEFT,50)
        self.lstFollow.InsertColumn(2,u'desc',wx.LIST_FORMAT_LEFT,50)
        self.lstFollow.InsertColumn(3,u'info',wx.LIST_FORMAT_LEFT,80)
        self.lstFollow.InsertColumn(4,u'c.nr',wx.LIST_FORMAT_LEFT,30)
        self.lstFollow.InsertColumn(5,u'condition',wx.LIST_FORMAT_LEFT,80)
        
    def __clear__(self):
        self.txtSymbolID.SetValue('')
        self.txtSymbolDesc.SetValue('')
        self.txtInfo.SetValue('')
        self.txtVal.SetValue('')
        self.txtName.SetValue('')
        self.txtText.SetValue('')
        self.txtCondition.SetValue('')
        self.spnNum.SetValue(0)
        self.IDtext=[]
    def SetBrowseDlg(self,dlg):
        self.dlgBrowse=dlg
    def SetInfos(self,voglFlowChartElement,voglFlowChart):
        #self.spnStepNr.SetValue(voglSFCstep.nr)
        self.voglFlowChart=voglFlowChart
        self.voglFlowChartElem=voglFlowChartElement
        self.txtSymbolID.SetValue(self.voglFlowChartElem.id)
        self.txtSymbolDesc.SetValue(self.voglFlowChartElem.desc)
        self.txtInfo.SetValue(self.voglFlowChartElem.info)
        #fix me
        #replace text
        if self.dlgBrowse is not None:
            self.dlgBrowse.CheckNode2Set()
            self.IDtext=string.split(self.voglFlowChartElem.IDtext,u',')
            i,j=0,0
            s=self.voglFlowChartElem.text
            for id in self.IDtext:
                i=string.find(s,'$',j)
                if i>=0:
                    j=string.find(s,'$',i+1)
                    if j>0:
                        diff=j-i-1
                        sID=self.dlgBrowse.GetLinkInfoById(id)[1]
                        s=s[:i+1]+sID+s[j:]
                        j=j+1-diff+len(sID)
            self.voglFlowChartElem.text=s
        self.txtText.SetValue(self.voglFlowChartElem.text)
        self.txtName.SetValue(self.voglFlowChartElem.getXmlType())
        self.__updateFollowing__()
    def __updateFollowing__(self):
        self.selFollow=-1
        self.lstFollow.DeleteAllItems()
        self.spnNum.SetValue(0)
        self.txtCondition.SetValue(u'')
        i=0
        for s in self.voglFlowChartElem.nextElem:
            index = self.lstFollow.InsertImageStringItem(sys.maxint, "%d"%i, -1)
            self.lstFollow.SetStringItem(index,1,s[0].id,-1)
            self.lstFollow.SetStringItem(index,2,s[0].desc,-1)
            self.lstFollow.SetStringItem(index,3,s[0].info,-1)
            try:
                iDesicNr=int(s[2][0]['desicNr'])
            except:
                iDesicNr=0
                s[2][0]['desicNr']=u'%d'%iDesicNr
            
            self.lstFollow.SetStringItem(index,4,s[2][0]['desicNr'],-1)
            self.lstFollow.SetStringItem(index,5,s[2][0]['name'],-1)
            
            self.lstFollow.SetItemData(index,i)
            i+=1
            pass
    def OnGcbbOkButton(self, event):
        #self.voglSFCstep.nr=self.txtNr.GetValue()
        self.voglFlowChartElem.id=self.txtSymbolID.GetValue()
        self.voglFlowChartElem.desc=self.txtSymbolDesc.GetValue()
        self.voglFlowChartElem.info=self.txtInfo.GetValue()
        self.voglFlowChartElem.text=self.txtText.GetValue()
        self.voglFlowChartElem.IDtext=string.join(self.IDtext,u',')
        self.EndModal(1)
        event.Skip()
    
    def OnLstFollowListItemSelected(self, event):
        self.selFollow=event.GetIndex()
        if self.voglFlowChartElem.nextElem[self.selFollow][2][0]['alternative']=='1':
            bVal=True
        else:
            bVal=False
        self.chcMultiple.SetValue(bVal)
        try:
            iVal=int(self.voglFlowChartElem.nextElem[self.selFollow][2][0]['desicNr'])
        except:
            iVal=0
        self.spnNum.SetValue(iVal)
        sVal=self.voglFlowChartElem.nextElem[self.selFollow][2][0]['name']
        self.txtCondition.SetValue(sVal)
        #self.chcJoin.SetValue(self.voglFlowChartElem.nextElem[self.selFollow][0].bJoin)
        event.Skip()
    def OnGcbbDelButton(self, event):
        if self.selFollow<0:
            return
        self.voglFlowChartElem.RemoveLink(self.selFollow)
        self.lstFollow.DeleteItem(self.selFollow)
        self.selFollow=-1
        #self.__updateTrans__()
        event.Skip()
    
    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()

    def OnChcMultipleCheckbox(self, event):
        event.Skip()

    def OnGcbbSetButton(self, event):
        if self.selFollow<0:
            return
        if self.chcMultiple.GetValue()==True:
            sVal='1'
        else:
            sVal='0'
        self.voglFlowChartElem.nextElem[self.selFollow][2][0]['alternative']=sVal
        sVal=u'%d'%self.spnNum.GetValue()
        self.voglFlowChartElem.nextElem[self.selFollow][2][0]['desicNr']=sVal
        sVal=self.txtCondition.GetValue()
        self.voglFlowChartElem.nextElem[self.selFollow][2][0]['name']=sVal
        #self.voglFlowChartElem.nextElem[self.selFollow][0].bJoin=self.chcJoin.GetValue()
        self.voglFlowChartElem.setDesicion(self.voglFlowChartElem.nextElem[self.selFollow][2][0])
        self.__updateFollowing__()
        event.Skip()

    def OnGcbbAnnotationButton(self, event):
        event.Skip()

    def OnChcParallelCheckbox(self, event):
        event.Skip()

    def OnChcJoinCheckbox(self, event):
        event.Skip()
    def __replace__(self,strOld,strNew):
        i=string.find(strOld,'<')
        if i>=0:
            j=string.find(strOld,'>',i+1)
            if j>0:
                return strOld[:i+1]+strNew+strOld[j:]
        return '<'+strNew+'>'
    def __calcIdx__(self):
        s=self.txtText.GetValue()
        iLen=len(s)
        idx=0
        iPos=self.txtText.GetInsertionPoint()
        tup=self.txtText.PositionToXY(iPos)
        strs=string.split(s,'\n')
        iPos=0
        for i in range(0,tup[1]):
            iPos+=len(strs[i])+1
        iPos+=tup[0]
        i,j=0,0
        while i>=0:
            i=string.find(s,u'$',j)
            if i>=0:
                if i>=iPos:
                    return (idx,iPos,iPos)
                j=string.find(s,'$',i+1)
                if j>0:
                    if j<iPos:
                        idx+=1
                        j+=1
                    else:
                        return (idx,i,j+1)
                i=j
        return (idx,iPos,iPos)
    def OnGcbbBrowseButton(self, event):
        if self.dlgBrowse is not None:
            self.dlgBrowse.Centre()
            if self.dlgBrowse.ShowModal()>0:
                id,s=self.dlgBrowse.GetLinkInfo()
                idxTup=self.__calcIdx__()
                idx=idxTup[0]
                if idxTup[1]!=idxTup[2]:
                    self.IDtext.remove(self.IDtext[idx])
                self.IDtext.insert(idx,id)
                sOld=self.txtText.GetValue()
                sNew=sOld[:idxTup[1]]+u'$'+s+u'$'+sOld[idxTup[2]:]
                self.txtText.SetValue(sNew)
        event.Skip()
    def OnTxtTextChar(self, event):
        if event.GetKeyCode() in [8,127]:
            s=self.txtText.GetValue()
            iLen=len(s)
            idx=0
            iPos=self.txtText.GetInsertionPoint()
            tup=self.txtText.PositionToXY(iPos)
            strs=string.split(s,'\n')
            iPos=0
            for i in range(0,tup[1]):
                iPos+=len(strs[i])+1
            iPos+=tup[0]
            i,j=0,0
            lstIDs=[]
            while i>=0:
                i=string.find(s,u'$',j)
                if i>=0:
                    j=string.find(s,'$',i+1)
                    if j>0:
                        if j<iPos:
                            idx+=1
                            j+=1
                        else:
                            lstIDs.append((idx,i,j+1))
                    i=j
            idx=0
            for tup in lstIDs:
                iStart=tup[1]
                if event.GetKeyCode()==8:
                    iStart+=1
                if iPos>=iStart and iPos<=tup[2]:
                    self.IDtext.remove(self.IDtext[idx])
                    sOld=self.txtText.GetValue()
                    sNew=sOld[:tup[1]]+sOld[tup[2]:]
                    self.txtText.SetValue(sNew)
                    return
                idx+=1
        event.Skip()


#----------------------------------------------------------------------

class voglFlowChartDesicion(voglFlowChartElement):
    def __init__(self, canvas,id,desc,info,w=0.0, h=0.0):
        voglFlowChartElement.__init__(self, canvas, id,desc,info, w, h)
        
        self.bMultiDesic=False
        self.desicions={}
        self.desicionTextShapes={}
        
        dw=w/6.0
        dh=h/4.0
        
        obj=ogl.PolygonShape()
        obj.SetBrush(TRANSPARENT_BRUSH)
        dw=w/6.0
        dh=h/4.0
        points = [ (-dw*3.0,     0.0),
                   (    0.0, -dh*2.0),
                   (+dw*3.0,     0.0),
                   (    0.0, +dh*2.0)
                   ]

        obj.Create(points)
        obj.SetDraggable(False)
        obj.SetSensitivityFilter(0,True)
        self.AddChild(obj)
        
        self.__addSymbol__()
        self.SetSensitivityFilter(0)
        self.Recompute()
        self.SetDraggable(True)
    def getXmlType(self):
        return 'descicion'
    def appendLatexPic(self,strs,yMax,scaleX,scaleY):
        x=self.symbol.GetX()/scaleX
        y=self.symbol.GetY()/scaleY
        ba=self.symbol.GetBoundingBoxMax()
        xa=x-(ba[0]/2.0/scaleX)
        ya=y-(ba[1]/2.0/scaleY)
        #tup=(xa,yMax-(y+(ba[1]/2.0/scaleY)),ba[0]/scaleX,ba[1]/scaleY,self.info)
        #strs.append('    \\put(%4.1f,%4.1f){\\makebox(%4.1f,%4.1f){{\\footnotesize %s}}}'%tup)
        xh1,yh1=xa,yMax-(y+(ba[1]/2.0/scaleY))
        xh2,yh2=xa+(self.w/6.0*3.0/scaleX),yMax-(y-(ba[1]/2.0/scaleY))
        xh3,yh3=xa+(self.w/6.0*5.0/scaleX),yh2-(self.h/4.0*2.0/scaleY)
        xh4=xa+(self.w/scaleX)
        
        tup=latex.lineFit(xh1,yh3,xh2,yh1)
        strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
        tup=latex.lineFit(xh2,yh1,xh4,yh3)
        strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
        tup=latex.lineFit(xh4,yh3,xh2,yh2)
        strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
        tup=latex.lineFit(xh1,yh3,xh2,yh2)
        strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
        
        xa,ya=voglFlowChartElement.appendLatexPic(self,strs,yMax,scaleX,scaleY)
        for k in self.desicions.keys():
            lst=self.desicions[k]
            try:
                ts=self.desicionTextShapes[k]
                x=ts.GetX()/scaleX
                y=ts.GetY()/scaleY
                ba=ts.GetBoundingBoxMax()
                sVal=lst[0]['name']
                tup=(x-(ba[0]/2.0/scaleX),yMax-(y+(ba[1]/2.0/scaleY)),ba[0]/scaleX,ba[1]/scaleY,
                        latex.texReplace(sVal))
                strs.append('    \\put(%4.1f,%4.1f){\\makebox(%4.1f,%4.1f){{\\footnotesize %s}}}'%tup)
            except:
                pass
        return xa,ya
        
    def isProg(self):
        return True
    def isSys(self):
        return True
    def genDesicion(self,dc=None):
        #for val in self.desicionTextShapes.values():
        #    val.Detach()
        #    val.RemoveFromCanvas(self.canvas)
        #    self.RemoveChild(val)
        #    pass
        self.desicions={}
        desicNr={}
        iNr=0
        for tup in self.nextElem:
            if tup[2][0].has_key('desicNr'):
                try:
                    lst=self.desicions[tup[2][0]['desicNr']]
                    lst.append(tup[2][0])
                except:
                    self.desicions[tup[2][0]['desicNr']]=[tup[2][0]]
                try:
                    iDesic=int(tup[2][0]['desicNr'])
                except:
                    iDesic=0
                if iNr<iDesic:
                    iNr=iDesic
                desicNr[iDesic]=True
        for k in self.desicionTextShapes.keys():
            if desicNr.has_key(int(k))==False:
                val=self.desicionTextShapes[k]
                if val is not None:
                    #val.Delete()
                    if dc is not None:
                        self.Select(False,dc)
                        val.Erase(dc)
                        val.ClearText()
                        val.Show(False)
                        val.Draw(dc)
                        val.Move(dc,-1000,-100)
                        val.Draw(dc)
                    
                        self.RemoveChild(val)
                        val.Detach()
                        val.RemoveFromCanvas(self.canvas)
                        #self.canvas.diagram.RemoveShape(val)
                        self.desicionTextShapes[k]=None
                #val.Delete()
        if iNr>2:
            self.bMultiDesic=True
        else:
            self.bMultiDesic=False
    def setDesicion(self,desc):
        try:
            lst=self.desicions[desc['desicNr']]
            sName=desc['name']
            for item in lst:
                item['name']=sName
        except:
            pass
        self.genDesicion()
    #def AddLink(self,step,trans=None):
    #    voglFlowChartElement.AddLink(self,step,trans)
    #    self.genDesicion()
    #def RemoveLink(self,idx):
    #    voglFlowChartElement.RemoveLink(self,idx)
    #    self.genDesicion()
    def __processLines__(self,dc):
        self.genDesicion(dc)
        for tup in self.nextElem:
            if tup[2][0].has_key('alternative'):
                if tup[2][0]['alternative'] == '1':
                    tup[1].SetPen(wx.BLACK_DASHED_PEN)
                else:
                    tup[1].SetPen(wx.BLACK_PEN)
            else:
                tup[1].SetPen(wx.BLACK_PEN)
            if self.symbol is None:
                xa=self.GetX()
                ya=self.GetY()
                ba=self.GetBoundingBoxMax()
            else:
                xa=self.symbol.GetX()
                ya=self.symbol.GetY()
                ba=self.symbol.GetBoundingBoxMax()
            if tup[0].symbol is None:
                xe=tup[0].GetX()
                ye=tup[0].GetY()
                be=tup[0].GetBoundingBoxMax()
            else:
                xe=tup[0].symbol.GetX()
                ye=tup[0].symbol.GetY()
                be=tup[0].symbol.GetBoundingBoxMax()
            
            try:
                iDesic=int(tup[2][0]['desicNr'])
            except:
                iDesic=0
            if self.bMultiDesic:
                if xa<xe:
                    x=xa+ba[0]/2.0+40+iDesic*10.0
                else:
                    x=xe+be[0]/2.0+40+iDesic*10.0
                dX=0
                dX2=0
                dY=ba[1]/2.0
                dY2=iDesic*20.0+20.0
            else:
                if iDesic==0:
                    if xa<xe:
                        x=xa-ba[0]/2.0-40
                    else:
                        x=xe-be[0]/2.0-40
                    dX=0
                    dX2=0
                    dY=ba[1]/2.0
                    dY2=0.0
                elif iDesic==1:
                    dX=-ba[0]/2.0
                    dX2=-60
                    dY=0
                    dY2=0.0
                    x=xa+dX+dX2
                elif iDesic==2:
                    dX=ba[0]/2.0
                    dX2=60
                    dY=0
                    dY2=0.0
                    x=xa+dX+dX2
                else:
                    dX=0
                    dX2=0
                    dY=ba[1]/2.0
                    dY2=0.0
                    x=xa+dX2
            if dc is not None:
                tup[1].Erase(dc)
            #tup[1].Show(False)
            if ya>=ye:
                #tup[1].MakeLineControlPoints(6)
                
                
                # start processing
                #xa+=ba[0]/2.0
                xa+=dX
                ya+=dY
                
                #xe+=be[0]/2.0
                ye-=be[1]/2.0
                
                yMin=sys.maxint
                lstBelow=[]
                objMin=None
                for pe in tup[0].prevElem:
                    yH=pe.symbol.GetY()
                    if yH>ye:
                        if yMin>yH:
                            yMin=yH
                            objMin=pe
                        lstBelow.append(pe)
                    #for neTup in pe.nextElem:
                def cmpObj(o1,o2):
                    if o1.symbol.GetY()<o2.symbol.GetY():
                        return -1
                    elif o1.symbol.GetY()>o2.symbol.GetY():
                        return 1
                    else:
                        return 0
                    #return o2.symbol.GetY()-o1.symbol.GetY()
                lstBelow.sort(cmpObj)
                controlPoints=tup[1].GetLineControlPoints()
                controlPoints[0][0]=xa
                controlPoints[0][1]=ya
                
                ba=self.GetBoundingBoxMax()
                controlPoints[1][0]=xa+dX2
                controlPoints[1][1]=ya+dY2
                
                if objMin==self:
                    controlPoints[2][0]=x
                    controlPoints[2][1]=ya+dY2#+ba[1]/2.0+20
                    
                    controlPoints[3][0]=x
                    controlPoints[3][1]=ye-be[1]/2.0-20
                    
                    if len(tup[0].prevElem)>1:
                        controlPoints[4][0]=x
                        controlPoints[4][1]=ye-be[1]/2.0-20
                        
                        controlPoints[5][0]=xe
                        controlPoints[5][1]=ye-be[1]/2.0-20
                    else:
                        controlPoints[4][0]=xe
                        controlPoints[4][1]=ye-be[1]/2.0-20
                        
                        controlPoints[5][0]=xe
                        controlPoints[5][1]=ye
                else:
                    # join
                    grid=self.canvas.diagram.GetGridSpacing()
                    idx=lstBelow.index(self)
                    if idx>=1:
                        if self.symbol.GetY()<=(lstBelow[idx-1].symbol.GetY()+grid):
                            ya=lstBelow[idx-1].symbol.GetY()+grid+ba[1]/2.0
                    controlPoints[2][0]=xa+dX2
                    controlPoints[2][1]=ya+ba[1]/2.0+20

                    controlPoints[3][0]=xa+dX2
                    controlPoints[3][1]=ya+ba[1]/2.0+20
                    
                    controlPoints[4][0]=x
                    controlPoints[4][1]=ya+ba[1]/2.0+20
                    
                    bh=pe.symbol.GetBoundingBoxMax()
                    if idx>0:
                        for neTup in lstBelow[idx-1].nextElem:
                            if neTup[0]==tup[0]:
                                ch=neTup[1].GetLineControlPoints()
                                ye=ch[2][1]
                                break
                    controlPoints[5][0]=x
                    controlPoints[5][1]=ye#yMin+bh[1]+20
                    
            else:
                #tup[1].MakeLineControlPoints(4)
                # start processing
                #xa+=ba[0]/2.0
                xa+=dX
                ya+=dY
                
                #xe+=be[0]/2.0
                ye-=be[1]/2.0
                controlPoints=tup[1].GetLineControlPoints()
                controlPoints[0][0]=xa
                controlPoints[0][1]=ya
                
                ba=self.GetBoundingBoxMax()
                #ym=self.GetY()+ba[1]/2.0+20
                ym=ya+dY2
                controlPoints[1][0]=xa+dX2
                controlPoints[1][1]=ya#ya+ba[1]/2.0+20
                
                controlPoints[2][0]=xa+dX2
                controlPoints[2][1]=ym#ya+ba[1]/2.0+20
                
                controlPoints[3][0]=xa+dX2
                controlPoints[3][1]=ym#ya+ba[1]/2.0+20
                
                #for pe in tup[0].prevElem:
                #    print pe,tup[0].prevElem.index(pe),self
                if tup[0].prevElem.index(self)>0:
                    if tup[0].prevElem[0].symbol.GetY()>tup[0].GetY():
                        bMod=True
                        while bMod == True:
                            bMod=False
                            for pe in tup[0].prevElem[1:]:
                                if pe != self:
                                    for neTup in pe.nextElem:
                                        neCP=neTup[1].GetLineControlPoints()
                                        if neCP[4][1]==ym:
                                            ym+=self.canvas.GetGridSpacing()
                                            bMod=True
                        controlPoints[4][0]=xe
                        controlPoints[4][1]=ym
                        
                        controlPoints[5][0]=xe
                        controlPoints[5][1]=ye-be[1]/2.0-20
                    else:
                        bMod=True
                        while bMod == True:
                            bMod=False
                            neTup=tup[0].prevElem[0].nextElem[0]
                            neCP=neTup[1].GetLineControlPoints()
                            ylim=neCP[4][1]
                            for pe in tup[0].prevElem[1:]:
                                if pe != self:
                                    for neTup in pe.nextElem:
                                        neCP=neTup[1].GetLineControlPoints()
                                        if neCP[4][1]==ym:
                                            ym+=self.canvas.diagram.GetGridSpacing()
                                            bMod=True
                                        elif ylim>ym:
                                            ym=ylim
                                            bMod=True
                        controlPoints[4][0]=xa+dX2
                        controlPoints[4][1]=ym
                        
                        controlPoints[5][0]=xe
                        controlPoints[5][1]=ym
                else:
                    controlPoints[4][0]=xe
                    controlPoints[4][1]=ym
                    
                    controlPoints[5][0]=xe
                    controlPoints[5][1]=ye
            tup[1].ClearText()
            try:
                s=tup[2][0]['name']
                dx=0.0
                dy=0.0
                if self.bMultiDesic:
                    posLbl=(xa+dX,ya)
                    dx=30
                    dy=dY+dY2-40#ba[1]/2.0+iDesic*20
                else:
                    posLbl=tup[1].GetLabelPosition(1)
                    #print 'test'
                    if iDesic==0:
                        dy=15.0
                        dx=30.0
                    elif iDesic==1:
                        dy=-15.0
                        dx=-30.0
                    elif iDesic==2:
                        dy=-15.0
                        dx=30.0
                #dx=0.0
                #dy=0.0
                try:
                    ts=self.desicionTextShapes[tup[2][0]['desicNr']]
                    if dc is not None:
                        ts.Erase(dc)
                        ts.FormatText(dc,s)
                        ts.Move(dc,posLbl[0]+dx,posLbl[1]+dy)
                except:
                    ts=ogl.TextShape(60,25)
                    ts.SetBrush(wx.TRANSPARENT_BRUSH)
                    ts.AddText(s)
                    ts.SetDraggable(False, False)
                    ts.SetX(posLbl[0]+dx)
                    ts.SetY(posLbl[1]+dy)
                    self.AddChild(ts)
                    ts.Show(True)
                    self.desicionTextShapes[tup[2][0]['desicNr']]=ts
                    self.Recompute()
            except:
                pass
            #tup[1].Show(True)
            if dc is not None:
                tup[1].Draw(dc)
            #tup[1].OnDrawControlPoints(dc)
        self.Recompute()
    def DoEdit(self):
        if self.canvas.dlgFlowChartDesic is None:
            self.canvas.dlgFlowChartDesic=vgdFlowChartDesic(self.canvas)
        self.canvas.dlgFlowChartDesic.Centre()
        self.canvas.dlgFlowChartDesic.SetBrowseDlg(self.canvas.GetBrowseDlg())
        self.canvas.dlgFlowChartDesic.SetInfos(self,self.canvas)
        if self.canvas.dlgFlowChartDesic.ShowModal()>0:
            self.canvas.__setModified__(True)
            canvas = self.canvas
            dc = wx.ClientDC(canvas)
            canvas.PrepareDC(dc)
        
            self.UpdateLinksRec(dc)
            
            return 1
        return 0
        
