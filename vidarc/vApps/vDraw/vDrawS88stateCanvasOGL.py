#Boa:FramePanel:vgpDraw
#----------------------------------------------------------------------------
# Name:         vDrawS88stateCanvasOGL.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vDrawS88stateCanvasOGL.py,v 1.2 2008/02/02 16:10:53 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.ogl as ogl
import string,os,sys
import math

import vidarc.tool.xml.vtXmlDom as vtXmlDom
import vidarc.vApps.vPrjEng.Hierarchy as Hierarchy

import vidarc.vApps.vDraw.latex as latex
from vidarc.vApps.vDraw.voglS88state import voglS88state
from vidarc.vApps.vDraw.vDrawSFCspecDialog import vDrawSFCspecDialog

STATE_ORDER=['idle','running','complete','aborting','aborted',
                    'stopping','stopped','pausing','paused',
                    'holding','held','restarting','hazard',
                    'material','product']
        
# defined event for vgpXmlTree item selected
wxEVT_VGPDRAWS88STATE_SFC_ADDED=wx.NewEventType()
def EVT_VGPDRAWS88STATE_SFC_ADDED(win,func):
    win.Connect(-1,-1,wxEVT_VGPDRAWS88STATE_SFC_ADDED,func)
class vgpDrawS88StateSfcAdded(wx.PyEvent):
    """
    Posted Events:
        Tree Item added event, reports the parent node of the added one
            EVT_VGPDRAWS88STATE_SFC_ADDED(<widget_name>, self.OnItemAdd)
    """

    def __init__(self):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_VGPDRAWS88STATE_SFC_ADDED)

class DrawingEvtHandler(ogl.ShapeEvtHandler):
    def __init__(self):
        ogl.ShapeEvtHandler.__init__(self)
        
    def UpdateStatusBar(self, shape):
        x, y = shape.GetX(), shape.GetY()
        width, height = shape.GetBoundingBoxMax()
        

    def OnLeftClick(self, x, y, keys=0, attachment=0):
        #print keys,ogl.KEY_CTRL
        shape = self.GetShape()
        canvas = shape.GetCanvas()
        dc = wx.ClientDC(canvas)
        canvas.PrepareDC(dc)
        if shape.Selected():
            shape.Select(False, dc)
            canvas.Redraw(dc)
        else:
            redraw = False
            shapeList = canvas.GetDiagram().GetShapeList()
            toUnselect = []

            for s in shapeList:
                if s.Selected():
                    # If we unselect it now then some of the objects in
                    # shapeList will become invalid (the control points are
                    # shapes too!) and bad things will happen...
                    toUnselect.append(s)
            shape.Select(True, dc)
            if toUnselect:
                for s in toUnselect:
                    s.Select(False, dc)
            canvas.SetSelectStep(shape)
            canvas.Redraw(dc)
        self.UpdateStatusBar(shape)
    def OnStartDragLeft(self, x, y, keys=0, attachment=0):
        shape = self.GetShape()
        ogl.ShapeEvtHandler.OnEndDragLeft(self, x, y, keys, attachment)
        
    def OnEndDragLeft(self, x, y, keys=0, attachment=0):
        shape = self.GetShape()
        #x,y=shape.GetCanvas().GetDiagram().Snap(x,y)
        ogl.ShapeEvtHandler.OnEndDragLeft(self, x, y, keys, attachment)
        if not shape.Selected():
            self.OnLeftClick(x, y, keys, attachment)
        self.UpdateStatusBar(shape)
    def OnSizingEndDragLeft(self, pt, x, y, keys, attch):
        ogl.ShapeEvtHandler.OnSizingEndDragLeft(self, pt, x, y, keys, attch)
        self.UpdateStatusBar(self.GetShape())
    def OnMovePost(self, dc, x, y, oldX, oldY, display):
        #print x,y,oldX,oldY
        shape=self.GetShape()
        canvas = shape.GetCanvas()
        dc = wx.ClientDC(canvas)
        canvas.PrepareDC(dc)
        #canvas.Erase(dc)
        ogl.ShapeEvtHandler.OnMovePost(self, dc, x, y, oldX, oldY, display)
        self.UpdateStatusBar(self.GetShape())
        #print "moved"
        if isinstance(self.GetShape(),voglSFCstep):
            shape=self.GetShape()
            shape.UpdateLinksRev(dc)
            #canvas = shape.GetCanvas()
            #dc = wx.ClientDC(canvas)
            #canvas.PrepareDC(dc)
            #canvas.GetDiagram().Clear(dc)
            #canvas.Erase(dc)
            #canvas.Redraw(dc)
    def OnRightClick(self, *dontcare):
        #print "evt handler right click"
        #self.log.WriteText("%s\n" % self.GetShape())
        shape=self.GetShape()
        canvas = shape.GetCanvas()
        dc = wx.ClientDC(canvas)
        canvas.PrepareDC(dc)
        if shape.DoEdit()>0:
            canvas.GetDiagram().Clear(dc)
            canvas.Redraw(dc)
        pass
    
#----------------------------------------------------------------------

class vDrawS88stateCanvasOGL(ogl.ShapeCanvas):
    def __init__(self, parent=None,id = -1, pos = wx.DefaultPosition, size = wx.DefaultSize, 
                    style = wx.BORDER, name = "ShapeCanvas",
                    gridSize=10):
        ogl.ShapeCanvas.__init__(self, parent,id, pos, size, style, name)
        self.parent=parent
        
        self.dlgStepBrowse=None
        self.dlgStepValBrowse=None
        self.dlgSFCspec=None
        
        maxWidth  = 400
        maxHeight = 400
        self.SetScrollbars(gridSize, gridSize, 
                maxWidth/gridSize, maxHeight/gridSize)
        
        self.SetBackgroundColour("LIGHT GRAY")
        self.diagram = ogl.Diagram()
        self.SetDiagram(self.diagram)
        self.diagram.SetCanvas(self)
        self.shapes = []
        self.save_gdi = []
        
        self.selState=None
        self.dlgS88state=None
        self.dlgSFCspec=None
        
        self.diagram.SetGridSpacing(gridSize)
        self.diagram.SetSnapToGrid(True)
        
        self.state={}
        self.node=None
        self.relNode=None
        
        state_compl=voglS88state(self,'complete',80,40)
        state_compl.SetType(2)
        self.AddShapeSimple(state_compl,50,30)
        self.state['complete']=state_compl
        
        state_idle=voglS88state(self,'idle',80,40)
        state_idle.SetType(0)
        self.AddShapeSimple(state_idle,50,110)
        self.state['idle']=state_idle
        
        state_exph=voglS88state(self,'hazard',80,40)
        state_exph.SetType(3,False)
        self.AddShapeSimple(state_exph,50,370)
        self.state['hazard']=state_exph
        
        state_expm=voglS88state(self,'material',80,40)
        state_expm.SetType(3,False)
        self.AddShapeSimple(state_expm,150,370)
        self.state['material']=state_expm
        
        state_expp=voglS88state(self,'product',80,40)
        state_expp.SetType(3,False)
        self.AddShapeSimple(state_expp,250,370)
        self.state['product']=state_expp
        
        state_restart=voglS88state(self,'restarting',80,40)
        state_restart.SetType(0,False)
        self.AddShapeSimple(state_restart,150,30)
        self.state['restarting']=state_restart
        
        state_running=voglS88state(self,'running',80,40)
        state_running.SetType(0)
        self.AddShapeSimple(state_running,190,110)
        self.state['running']=state_running
        
        state_pausing=voglS88state(self,'pausing',80,40)
        state_pausing.SetType(0,False)
        self.AddShapeSimple(state_pausing,370,110)
        self.state['pausing']=state_pausing
        
        state_paused=voglS88state(self,'paused',80,40)
        state_paused.SetType(1,False)
        self.AddShapeSimple(state_paused,370,190)
        self.state['paused']=state_paused
        
        state_stopping=voglS88state(self,'stopping',80,40)
        state_stopping.SetType(0,False)
        self.AddShapeSimple(state_stopping,240,190)
        self.state['stopping']=state_stopping
        
        state_stopped=voglS88state(self,'stopped',80,40)
        state_stopped.SetType(2,False)
        self.AddShapeSimple(state_stopped,240,270)
        self.state['stopped']=state_stopped
        
        state_aborting=voglS88state(self,'aborting',80,40)
        state_aborting.SetType(0,False)
        self.AddShapeSimple(state_aborting,130,190)
        self.state['aborting']=state_aborting
        
        state_aborted=voglS88state(self,'aborted',80,40)
        state_aborted.SetType(2,False)
        self.AddShapeSimple(state_aborted,130,270)
        self.state['aborted']=state_aborted
        
        state_holding=voglS88state(self,'holding',80,40)
        state_holding.SetType(0,False)
        self.AddShapeSimple(state_holding,370,30)
        self.state['holding']=state_holding
        
        state_held=voglS88state(self,'held',80,40)
        state_held.SetType(1,False)
        self.AddShapeSimple(state_held,260,30)
        self.state['held']=state_held
        
        self.AddStateLink(state_compl,state_idle)
        self.AddStateLink(state_running,state_aborting,-10,-2)
        self.AddStateLink(state_aborting,state_aborted)
        self.AddStateLink(state_running,state_stopping,10,-2)
        self.AddStateLinkRight(state_running,state_pausing)
        self.AddStateLinkRight(state_idle,state_running)
        self.AddStateLink(state_stopping,state_stopped)
        self.AddStateLink(state_pausing,state_paused)
        self.AddStateLink(state_restart,state_running)
        self.AddStateLinkLeft(state_holding,state_held)
        self.AddStateLinkLeft(state_held,state_restart)
        self.AddStateLinkMid(state_running,state_compl)
        self.AddStateLinkMid(state_paused,state_running)
        self.AddStateLinkMidRight(state_running,state_holding)
        
        line = ogl.LineShape()
        line.SetCanvas(self)
        line.SetPen(wx.BLACK_PEN)
        line.SetBrush(wx.BLACK_BRUSH)
        line.AddArrow(ogl.ARROW_ARROW)
        
        line.MakeLineControlPoints(3)
        controlPoints=line.GetLineControlPoints()
        ba=state_aborted.GetBoundingBoxMax()
        controlPoints[0][0]=state_aborted.GetX()-ba[0]/2.0
        controlPoints[0][1]=state_aborted.GetY()
        ba=state_idle.GetBoundingBoxMax()
        controlPoints[1][0]=state_idle.GetX()+20
        controlPoints[1][1]=state_aborted.GetY()
        
        controlPoints[2][0]=state_idle.GetX()+20
        controlPoints[2][1]=state_idle.GetY()+ba[1]/2.0-3
        line.Show(True)
        self.diagram.AddShape(line)
        
        line = ogl.LineShape()
        line.SetCanvas(self)
        line.SetPen(wx.BLACK_PEN)
        line.SetBrush(wx.BLACK_BRUSH)
        line.AddArrow(ogl.ARROW_ARROW)
        
        line.MakeLineControlPoints(4)
        controlPoints=line.GetLineControlPoints()
        ba=state_stopped.GetBoundingBoxMax()
        controlPoints[0][0]=state_stopped.GetX()
        controlPoints[0][1]=state_stopped.GetY()+ba[1]/2.0
        
        controlPoints[1][0]=state_stopped.GetX()
        controlPoints[1][1]=state_stopped.GetY()+ba[1]/2.0+20
        
        controlPoints[2][0]=state_idle.GetX()-20
        controlPoints[2][1]=controlPoints[1][1]
        
        ba=state_idle.GetBoundingBoxMax()
        controlPoints[3][0]=state_idle.GetX()-20
        controlPoints[3][1]=state_idle.GetY()+ba[1]/2.0-3
        line.Show(True)
        self.diagram.AddShape(line)
        
    def __setModified__(self,state):
        self.parent.__setModified__(state)
    def AddStateLink(self,state_start,state_end,sdx=0.0,sdy=0.0):
        line = ogl.LineShape()
        line.SetCanvas(self)
        line.SetPen(wx.BLACK_PEN)
        line.SetBrush(wx.BLACK_BRUSH)
        line.AddArrow(ogl.ARROW_ARROW)
        
        line.MakeLineControlPoints(2)
        controlPoints=line.GetLineControlPoints()
        ba=state_start.GetBoundingBoxMax()
        controlPoints[0][0]=state_start.GetX()+sdx
        controlPoints[0][1]=state_start.GetY()+ba[1]/2.0+sdy
        ba=state_end.GetBoundingBoxMax()
        controlPoints[1][0]=state_end.GetX()
        controlPoints[1][1]=state_end.GetY()-ba[1]/2.0
        line.Show(True)
        self.diagram.AddShape(line)
    def AddStateLinkMid(self,state_start,state_end):
        line = ogl.LineShape()
        line.SetCanvas(self)
        line.SetPen(wx.BLACK_PEN)
        line.SetBrush(wx.BLACK_BRUSH)
        line.AddArrow(ogl.ARROW_ARROW)
        
        line.MakeLineControlPoints(2)
        controlPoints=line.GetLineControlPoints()
        ba=state_start.GetBoundingBoxMax()
        controlPoints[0][0]=state_start.GetX()-ba[0]/2.0+4
        controlPoints[0][1]=state_start.GetY()-13
        ba=state_end.GetBoundingBoxMax()
        controlPoints[1][0]=state_end.GetX()+ba[0]/2.0-4
        controlPoints[1][1]=state_end.GetY()+13
        line.Show(True)
        self.diagram.AddShape(line)
    def AddStateLinkMidRight(self,state_start,state_end):
        line = ogl.LineShape()
        line.SetCanvas(self)
        line.SetPen(wx.BLACK_PEN)
        line.SetBrush(wx.BLACK_BRUSH)
        line.AddArrow(ogl.ARROW_ARROW)
        
        line.MakeLineControlPoints(2)
        controlPoints=line.GetLineControlPoints()
        ba=state_start.GetBoundingBoxMax()
        controlPoints[0][0]=state_start.GetX()+ba[0]/2.0-4
        controlPoints[0][1]=state_start.GetY()-13
        ba=state_end.GetBoundingBoxMax()
        controlPoints[1][0]=state_end.GetX()-ba[0]/2.0+4
        controlPoints[1][1]=state_end.GetY()+13
        line.Show(True)
        self.diagram.AddShape(line)
    def AddStateLinkLeft(self,state_start,state_end):
        line = ogl.LineShape()
        line.SetCanvas(self)
        line.SetPen(wx.BLACK_PEN)
        line.SetBrush(wx.BLACK_BRUSH)
        line.AddArrow(ogl.ARROW_ARROW)
        
        line.MakeLineControlPoints(2)
        controlPoints=line.GetLineControlPoints()
        ba=state_start.GetBoundingBoxMax()
        controlPoints[0][0]=state_start.GetX()-ba[0]/2.0
        controlPoints[0][1]=state_start.GetY()
        ba=state_end.GetBoundingBoxMax()
        controlPoints[1][0]=state_end.GetX()+ba[0]/2.0
        controlPoints[1][1]=state_end.GetY()
        line.Show(True)
        self.diagram.AddShape(line)
    def AddStateLinkRight(self,state_start,state_end):
        line = ogl.LineShape()
        line.SetCanvas(self)
        line.SetPen(wx.BLACK_PEN)
        line.SetBrush(wx.BLACK_BRUSH)
        line.AddArrow(ogl.ARROW_ARROW)
        
        line.MakeLineControlPoints(2)
        controlPoints=line.GetLineControlPoints()
        ba=state_start.GetBoundingBoxMax()
        controlPoints[0][0]=state_start.GetX()+ba[0]/2.0
        controlPoints[0][1]=state_start.GetY()
        ba=state_end.GetBoundingBoxMax()
        controlPoints[1][0]=state_end.GetX()-ba[0]/2.0
        controlPoints[1][1]=state_end.GetY()
        line.Show(True)
        self.diagram.AddShape(line)
    def EditSFCspec(self,name):
        state=self.state[name]
        if self.node is None:
            return
        if self.dlgSFCspec is None:
            self.dlgSFCspec=vDrawSFCspecDialog(self)
            self.dlgSFCspec.SetDoc(self.doc,True)
        if self.dlgSFCspec is not None:
            self.dlgSFCspec.Centre()
            lstState=['idle','complete','running','aborting','aborted',
                    'stopping','stopped','pausing','paused',
                    'holding','held','restarting','hazard',
                    'material','product']
            lstNodeNames=['idle','cmpl','rng','abng','abed','stpng','stged',
                'paung','paued','holng','held','restng','exceh','excem','excep']
            #try:
            if 1==1:
                idx=lstState.index(name)
                parentNode=self.doc.getParent(self.node)
                node=self.doc.getChild(parentNode,lstNodeNames[idx])
                if node is None:
                    node=self.doc.createSubNode(parentNode,
                                lstNodeNames[idx],False)
                    self.doc.setAttribute(node,'id','')
                    self.doc.addNode(parentNode,node)
                    #self.ids.CheckId(node)
                    #self.ids.ProcessMissing()
                    #self.ids.ClearMissing()
                    c=self.doc.createSubNode(node,'description',False)
                    c=self.doc.createSubNode(node,'name',False)
                    self.doc.setText(c,name)
                    c=self.doc.createSubNode(node,'tag',False)
                    self.doc.setText(c,lstNodeNames[idx])
                    c=self.doc.createSubNode(node,'drawSFC',False)
                    self.doc.setAttribute(c,'id','')
                    self.doc.addNode(node,c)
                    #self.ids.CheckId(c)
                    #self.ids.ProcessMissing()
                    #self.ids.ClearMissing()
                    self.doc.AlignNode(node)
                    wx.PostEvent(self,vgpDrawS88StateSfcAdded())
                sfcNode=self.doc.getChild(node,'drawSFC')
                if sfcNode is None:
                    sfcNode=self.doc.createSubNode(node,'drawSFC',False)
                    self.doc.setAttribute(sfcNode,'id','')
                    self.doc.addNode(node,sfcNode)
                    #self.ids.CheckId(sfcNode)
                    #self.ids.ProcessMissing()
                    #self.ids.ClearMissing()
                    self.doc.AlignNode(node)
                    wx.PostEvent(self,vgpDrawS88StateSfcAdded())
                self.dlgSFCspec.SetNode(sfcNode)
                ret=self.dlgSFCspec.ShowModal()
                if ret==1:
                    self.dlgSFCspec.GetNode(sfcNode)
                    state.SetType(state.type,True)
            #except:
            #    node=None
            
    def Clear(self):
        #unit=self.GetScrollPixelsPerUnit()
        #size=self.GetVirtualSize()
        #self.SetScrollbars(unit[0], unit[1], size[0], size[1])
        #dc = wx.ClientDC(self)
        #self.PrepareDC(dc)
        #for s in self.steps:
        #    s.Delete()
        #self.diagram.DeleteAllShapes()
        #self.shapes=[]
        #self.steps=[]
        #self.stepNrs=[]
        #self.stepIDs={}
        #self.GetDiagram().Clear(dc)
        #self.Redraw(dc)
        for k in self.state.keys():
            if k in ['idle','running','complete']:
                used=True
            else:
                used=False
            self.state[k].SetUsed(used)
        pass
    def AddShapeSimple(self, shape, x, y):
        x,y=self.GetDiagram().Snap(x,y)
        if isinstance(shape, ogl.CompositeShape):
            dc = wx.ClientDC(self)
            self.PrepareDC(dc)
            shape.Move(dc, x, y)
        else:
            shape.SetDraggable(False, False)
        shape.SetCanvas(self)
        shape.SetX(x)
        shape.SetY(y)
        self.diagram.AddShape(shape)
        shape.Show(True)

        evthandler = DrawingEvtHandler()
        evthandler.SetShape(shape)
        evthandler.SetPreviousHandler(shape.GetEventHandler())
        shape.SetEventHandler(evthandler)

        self.shapes.append(shape)
        return shape
    
    def OnBeginDragLeft(self, x, y, keys):
        #print "OnBeginDragLeft: %s, %s, %s\n" % (x, y, keys)
        pass
    def OnEndDragLeft(self, x, y, keys):
        #print "OnEndDragLeft: %s, %s, %s\n" % (x, y, keys)
        pass
    def OnRightClick(self,x,y,keys):
        #print "canvas right"
        pass
    def SetSelectStep(self,step):
        if step is not None:
            if step.Selected()>0:
                self.selStep=step
            else:
                self.selStep=None
        else:
            self.selStep=None
    def SetBrowseDlg(self,dlg):
        self.dlgBrowse=dlg
    def SetBrowseValDlg(self,dlg):
        self.dlgValBrowse=dlg
    def SetSFCspecDlg(self,dlg):
        self.dlgS88spec=dlg
        
    def GetBrowseDlg(self):
        return self.dlgBrowse
    def GetBrowseValDlg(self):
        return self.dlgValBrowse
    def GetSFCspecDlg(self):
        return self.dlgS88spec
        
    def __replace__(self,strOld,strNew):
        i=string.find(strOld,'<')
        if i>=0:
            j=string.find(strOld,'>',i+1)
            if j>0:
                return strOld[:i+1]+strNew+strOld[j:]
        return strOld
    def SetNodeState(self,state,node):
        if node is None:
            return
        self.relNode=Hierarchy.getRelBaseNode(self.doc,node)
        state.name=self.doc.getNodeText(node,'name')
        state.desc=self.doc.getNodeText(node,'desc')
        try:
            sVal=self.doc.getNodeText(node,'used')
            if sVal=='True':
                state.used=True
            else:
                if state.name in ['idle','running','complete']:
                    state.used=True
                else:
                    state.used=False
        except:
            if state.name in ['idle','running','complete']:
                state.used=True
            else:
                state.used=False
        state.SetType(state.type,state.used)
        state.ClearTransitions()
        childs=self.doc.getChilds(node,'follow')
        for c in childs:
            nextName=self.doc.getNodeText(c,'name')
            
            transChilds=self.doc.getChilds(c,'transition')
            lstTrans=[]
            for t in transChilds:
                sName=self.doc.getNodeText(t,'name')
                sVal=self.doc.getNodeText(t,'val')
                try:
                    nodeTransName=self.doc.getChild(t,'name')
                    sIDname=self.doc.getAttribute(nodeTransName,'fid')
                    sNew=Hierarchy.getTagNames(self.doc,self.baseNode,self.relNode,self.doc.GetIdStr(sIDname)[1])
                    sName=self.__replace__(sName,sNew)
                except:
                    sIDname=''
                try:
                    nodeTransVal=self.doc.getChild(t,'val')
                    sIDval=self.doc.getAttribute(nodeTransVal,'fid')
                    sNew=Hierarchy.getTagNames(self.doc,self.baseNode,self.relNode,self.doc.GetIdStr(sIDval)[1])
                    sVal=self.__replace__(sVal,sNew)
                except:
                    sIDval=''
                trans={'name':sName,
                    'fid_name':sIDname,
                    'operation':self.doc.getNodeText(t,'operation'),
                    'val':sVal,
                    'desc':self.doc.getNodeText(t,'desc'),
                    'fid_val':sIDval,
                    'combine':self.doc.getNodeText(t,'combine'),
                    'level':self.doc.getNodeText(t,'level')}
                    
                lstTrans.append(trans)
            if state.transitions.has_key(nextName):
                state.transitions[nextName]=lstTrans
        pass
    def GetNodeState(self,state,node):
        self.doc.setNodeText(node,'name',state.name)
        self.doc.setNodeText(node,'desc',state.desc)
        self.doc.setNodeText(node,'used',str(state.used))
        
        childs=self.doc.getChilds(node,'follow')
        for c in childs:
            self.doc.deleteNode(c,node)
        keys=state.transitions.keys()
        keys.sort()
        for k in keys:
            n=self.doc.createSubNode(node,'follow',False)
            self.doc.setNodeText(n,'name',k)
            for trans in state.transitions[k]:
                nt=self.doc.createSubNode(n,'transition',False)
                keys=trans.keys()
                keys.sort()
                for k in keys:
                    if k[:4]=='fid_':
                        continue
                    self.doc.setNodeText(nt,k,trans[k])
                for k in keys:
                    if k[:4]=='fid_':
                        aNode=self.doc.getChild(nt,k[4:])
                        if aNode is not None:
                            self.doc.setAttribute(aNode,'fid',trans[k])
    def SetDoc(self,doc,bNet=False):
        self.doc=doc
    def SetNode(self,node):
        self.node=node
        self.baseNode=self.doc.getChild(self.doc.getRoot(),'prjengs')
        self.baseNode=self.doc.getChild(self.baseNode,'instance')
        #self.canvas
        self.Clear()
        keys=self.state.keys()
        keys.sort()
        for k in keys:
            child=self.doc.getChild(node,k)
            self.SetNodeState(self.state[k],child)
        self.__setModified__(False)
    def GetNode(self,node):
        self.node=node
        if self.node is None:
            return
        keys=self.state.keys()
        keys.sort()
        for k in keys:
            child=self.doc.getChild(node,k)
            if child is None:
                child=self.doc.createSubNode(node,k,False)
            self.GetNodeState(self.state[k],child)
        
        self.doc.AlignNode(node,iRec=10)
        self.__setModified__(False)
        #print self.GenTexPic()
    def GenTexPic(self,scaleX=1.75,scaleY=1.75):
        xMax=0
        yMax=0
        xMin=sys.maxint
        yMin=sys.maxint
        
        strs=[]
        for v in self.state.values():
            x=v.GetX()
            y=v.GetY()
            ba=v.GetBoundingBoxMax()
            xa=x-(ba[0]/2.0)-40.0
            ya=y-(ba[1]/2.0)-40.0
            xe=x+(ba[0]/2.0)+40.0
            ye=y+(ba[1]/2.0)+40.0
            if xa<xMin:
                xMin=xa
            if ya<yMin:
                yMin=ya
            if xe>xMax:
                xMax=xe
            if ye>yMax:
                yMax=ye
        xMin/=scaleX
        yMin/=scaleY
        xMax/=scaleX
        yMax/=scaleY
        strs=['  \\begin{picture}(%d,%d)'%(xMax,yMax)]
        for state in self.state.values():
            x=state.GetX()/scaleX
            y=state.GetY()/scaleY
            ba=state.GetBoundingBoxMax()
            #xa=x-(ba[0]/2.0/scaleX)
            xa=x
            #ya=y-(ba[1]/2.0/scaleY)
            tup=(xa,yMax-y,math.floor(ba[0]/scaleX),math.floor(ba[1]/scaleY))
            x1=x
            x2=x-math.floor(ba[0]/scaleX)
            y1=yMax-y-math.floor(ba[1]/2.0/scaleY)
            y2=yMax-y
            y3=yMax-y+math.floor(ba[1]/2.0/scaleY)
            tup=(x1,y1,x2,y2,x1,y3)
            #strs.append('    \\put(%4.1f,%4.1f){\\oval(%4.1f,%4.1f)}'%tup)
            strs.append('    \\qbezier(%4.1f,%4.1f)(%4.1f,%4.1f)(%4.1f,%4.1f)'%tup)
            x2=x+math.floor(ba[0]/scaleX)
            tup=(x1,y1,x2,y2,x1,y3)
            strs.append('    \\qbezier(%4.1f,%4.1f)(%4.1f,%4.1f)(%4.1f,%4.1f)'%tup)
            
            if state.used:
                tup=(xa,yMax-y,latex.texReplace(state.name))
                strs.append('    \\put(%4.1f,%4.1f){\\makebox(0,0){{\\footnotesize %s}}}'%tup)
        diag=self.GetDiagram()
        for shape in self.diagram.GetShapeList():
            if isinstance(shape,ogl.LineShape):
                points=shape.GetLineControlPoints()
                count=len(points)
                for i in range(0,count-1):
                    xa,ya=diag.Snap(points[i][0],points[i][1])
                    xa/=scaleX
                    ya=yMax-ya/scaleY
                    xe,ye=diag.Snap(points[i+1][0],points[i+1][1])
                    xe/=scaleX
                    ye=yMax-ye/scaleY
                    
                    tup=latex.lineFit(xa,ya,xe,ye)
                    if i==(count-2):
                        strs.append('    \\put(%4.1f,%4.1f){\\vector(%d,%d){%4.1f}}'%tup)
                    else:
                        strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
                    i+=1
        strs.append('  \\end{picture}')
        return string.join(strs,os.linesep)
    def GenTexFigFloat(self):
        if self.node is not None:
            par=self.doc.getParent(self.node)
            relNode=Hierarchy.getRelBaseNode(self.doc,par)
            sHier=Hierarchy.getTagNamesWithRel(self.doc,self.baseNode,relNode,par)+ ' ' + \
                            self.doc.getNodeText(par,'name') + ' S88 state'
            sHierFull=Hierarchy.getTagNames(self.doc,self.baseNode,None,par)+ ' ' + \
                            self.doc.getNodeText(par,'name') + ' S88 state'
        sCaption='  \\caption{%s}'%latex.texReplace(sHier)
        sRef='  \\label{fig:%s}'%latex.texReplace4Lbl(sHierFull)
        fig=['\\begin{figure}[!hb]','  \\begin{center}',self.GenTexPic(),sCaption,sRef,'  \\end{center}','\\end{figure}']
        return string.join(fig,os.linesep)
    def GenTexFig(self):
        if self.node is not None:
            par=self.doc.getParent(self.node)
            relNode=Hierarchy.getRelBaseNode(self.doc,par)
            sHier=Hierarchy.getTagNamesWithRel(self.doc,self.baseNode,relNode,par)+ ' ' + \
                            self.doc.getNodeText(par,'name') + ' S88 state figure'
            sHierFull=Hierarchy.getTagNames(self.doc,self.baseNode,None,par)+ ' ' + \
                            self.doc.getNodeText(par,'name') + ' S88 state figure'
        sCaption='  \\caption{%s}'%latex.texReplace(sHier)
        sRef='  \\label{fig:%s}'%latex.texReplace4Lbl(sHierFull)
        fig=['\\begin{longtable}{c}',self.GenTexPic(),'   \\\\',sCaption,sRef,'\\end{longtable}']
        return string.join(fig,os.linesep)
    def GenTexFigTab(self):
        if self.node is not None:
            par=self.doc.getParent(self.node)
            relNode=Hierarchy.getRelBaseNode(self.doc,par)
            sHier=Hierarchy.getTagNamesWithRel(self.doc,self.baseNode,relNode,par)+ ' ' + \
                            self.doc.getNodeText(par,'name') + ' S88 state'
            sHierFull=Hierarchy.getTagNames(self.doc,self.baseNode,None,par)+ ' ' + \
                            self.doc.getNodeText(par,'name') + ' S88 state'
        else:
            sHier=''
        strs=[]
        strs.append('\\begin{center}')
        strs.append('  \\begin{longtable}{p{0.05\linewidth}p{0.05\linewidth}p{0.31\linewidth}p{0.05\linewidth}')
        strs.append('       p{0.3\linewidth}p{0.02\linewidth}p{0.01\linewidth}}')
        strs.append('    \\multicolumn{7}{c}{%s} \\\\'%self.GenTexPic(1.25,2.75))
        strs.append('  \\multicolumn{7}{c}{\\textbf{%s}} \\\\ \\hline'%'S88 state')
        tup=('    \\textbf{Nr}','\\textbf{to}','\\textbf{Name}','','','','')
        strs.append('   %s & %s & %s & %s & %s & %s & %s \\\\ \\hline'%tup)
        strs.append('  \\endfirsthead')
        tup=('    \\textbf{Nr}','\\textbf{to}','\\textbf{Name}','','','','')
        strs.append('    %s & %s & %s & %s & %s & %s & %s \\\\ \\hline'%tup)
        strs.append('  \\endhead')
        
        nrs={}
        i=0
        iCount=len(strs)
        keys=self.state.keys()
        for k in STATE_ORDER:
            if not self.state.has_key(k):
                continue
            if self.state[k].used==False:
                continue
            tup=('%02d'%STATE_ORDER.index(k),latex.texReplace(k))
            strs.append('    \\textbf{%s} &  & \\multicolumn{5}{l}{\\textbf{%s}} \\\\'%tup)
            try:
                sDesc=self.state[k].desc
            except:
                sDesc=''
            if len(sDesc)>0:
                #sDesc=string.replace(sDesc,'\n','\\\\ ')
                tup=('',latex.tableMultiLine(sDesc))
                strs.append('    %s &  & \hspace{1cm}\\multicolumn{5}{l}{%s} \\\\'%tup)
            #tkeys=self.state[k].transitions.keys()
            #tkeys.sort()
            #for tk in tkeys:
            for tk in STATE_ORDER:
                if not self.state[k].transitions.has_key(tk):
                    continue
                lstTrans=self.state[k].transitions[tk]
                if len(lstTrans)>0:
                    strs.append('    \\cline{2-7}')
                    tup=('%02d'%STATE_ORDER.index(tk),latex.texReplace(tk))
                    strs.append('    $\\to$ & %s & \\multicolumn{5}{l}{%s} \\\\'%tup)
                for trans in lstTrans:
                    try:
                        sName=trans['name']
                    except:
                        sName=''
                    try:
                        sOp=trans['operation']
                    except:
                        sOp=''
                    try:
                        sVal=trans['val']
                    except:
                        sVal=''
                    try:
                        sComb=trans['combine']
                    except:
                        sComb=''
                    try:
                        sLv=trans['level']
                    except:
                        sLv=''
                    tup=('',latex.texReplace(sName),sOp,
                                    latex.texReplace(sVal),sComb,sLv)
                    strs.append('   & %s  & %s & %s & %s & %s & %s \\\\'%tup)
                    try:
                        sDesc=trans['desc']
                    except:
                        sDec=''
                    if len(sDesc)>0:
                        sDesc=latex.tableMultiLine(sDesc)
                        tup=('',sDesc)
                        strs.append('   & %s &  \\hspace{1cm}\\multicolumn{5}{l}{%s} \\\\'%tup)
            strs.append('    \\hline ')
            i+=1
        strs.append('  \\caption{%s}'%latex.texReplace(sHier))
        strs.append('  \\label{tab:%s}'%latex.texReplace4Lbl(sHierFull))
            
        strs.append('  \\end{longtable}')
        strs.append('\\end{center}')
        return string.join(strs,os.linesep)
    def GenTexTab(self,sHier=None):
        if sHier is None:
            if self.node is not None:
                par=self.doc.getParent(self.node)
                relNode=Hierarchy.getRelBaseNode(self.doc,par)
                sHier=Hierarchy.getTagNamesWithRel(self.doc,self.baseNode,relNode,par)+ ' ' + \
                            self.doc.getNodeText(par,'name') + ' S88 state transitions'
                sHierFull=Hierarchy.getTagNames(self.doc,self.baseNode,None,par)+ ' ' + \
                            self.doc.getNodeText(par,'name') + ' S88 state transitions'
            else:
                sHier=''
        strs=[]
        strs.append('\\begin{center}')
        strs.append('  \\begin{longtable}{p{0.05\linewidth}p{0.34\linewidth}p{0.05\linewidth}')
        strs.append('       p{0.34\linewidth}p{0.02\linewidth}p{0.01\linewidth}}')
        strs.append('  \\multicolumn{5}{c}{\\textbf{%s}} \\\\ \\hline'%'S88 state')
        tup=('    \\textbf{Nr}','\\textbf{Name}','','','','')
        strs.append('    %s & %s & %s & %s & %s & %s \\\\ \\hline'%tup)
        strs.append('  \\endfirsthead')
        tup=('    \\textbf{Nr}','\\textbf{Name}','','','','')
        strs.append('    %s & %s & %s & %s & %s & %s \\\\ \\hline'%tup)
        strs.append('  \\endhead')
        nrs={}
        i=0
        keys=self.state.keys()
        keys.sort()
        for k in keys:
            if self.state[k].used==False:
                continue
            tup=(i,latex.texReplace(k))
            strs.append('    \\textbf{%s} & \\multicolumn{5}{l}{\\textbf{%s}} \\\\'%tup)
            tkeys=self.state[k].transitions.keys()
            tkeys.sort()
            for tk in tkeys:
                for trans in self.state[k].transitions[tk]:
                    try:
                        sName=trans['name']
                    except:
                        sName=''
                    try:
                        sOp=trans['operation']
                    except:
                        sOp=''
                    try:
                        sVal=trans['val']
                    except:
                        sVal=''
                    try:
                        sComb=trans['combine']
                    except:
                        sComb=''
                    try:
                        sLv=trans['level']
                    except:
                        sLv=''
                    tup=('',latex.texReplace(sName),sOp,
                                    latex.texReplace(sVal),sComb,sLv)
                    strs.append('    %s  & %s & %s & %s & %s & %s \\\\'%tup)
            strs.append('    \\hline ')
            i+=1
        strs.append('  \\caption{%s}'%latex.texReplace(sHier))
        strs.append('  \\label{tab:%s}'%latex.texReplace4Lbl(sHierFull))
            
        strs.append('  \\end{longtable}')
        strs.append('\\end{center}')
        return string.join(strs,os.linesep)
    def GenTexFigRef(self,node,doc):
        if node is not None:
            baseNode=doc.getChild(doc.getRoot(),'prjengs')
            baseNode=doc.getChild(baseNode,'instance')
            par=doc.getParent(node)
            sHier=Hierarchy.getTagNames(doc,baseNode,None,par)+ ' ' + \
                        doc.getNodeText(par,'name') + ' S88 state'
            sHier=latex.texReplace4Lbl(sHier)
            sRef='\\ref{fig:%s} \#\\pageref{fig:%s}'%(sHier,sHier)
            return sRef
        return ''
    def GenTexFigTabRef(self,node,doc):
        if node is not None:
            baseNode=doc.getChild(doc.getRoot(),'prjengs')
            baseNode=doc.getChild(baseNode,'instance')
            #relNode=Hierarchy.getRelBaseNode(node)
            par=self.doc.getParent(node)
            #relNode=Hierarchy.getRelBaseNode(par)
            sHier=sHier=Hierarchy.getTagNames(doc,baseNode,None,par)+ ' ' + \
                        doc.getNodeText(par,'name') + ' S88 state'
            sHier=latex.texReplace4Lbl(sHier)
            sRef='\\ref{tab:%s} \#\\pageref{tab:%s}'%(sHier,sHier)
            return sRef
        return ''
    def GenTexTabRef(self,node,doc):
        if node is not None:
            baseNode=doc.getChild(doc.getRoot(),'prjengs')
            baseNode=doc.getChild(baseNode,'instance')
            #relNode=Hierarchy.getRelBaseNode(node)
            par=self.doc.getParent(node)
            #relNode=Hierarchy.getRelBaseNode(par)
            sHier=sHier=Hierarchy.getTagNames(doc,baseNode,None,par)+ ' ' + \
                        doc.getNodeText(par,'name') + ' S88 state'
            sHier=latex.texReplace4Lbl(sHier)
            sRef='\\ref{tab:%s} \#\\pageref{tab:%s}'%(sHier,sHier)
            return sRef
        return ''