#Boa:Dialog:vgdFlowChartElem

import wx
import wx.lib.buttons
import wx.lib.ogl as ogl
import string,os,sys

import vidarc.vApps.vDraw.latex as latex
import vidarc.vApps.vPrjEng.Hierarchy as Hierarchy
#import vidarc.apps.xmlBase.vtXmlDomTree as vtXmlDomTree

import images

def create(parent):
    return vgdFlowChartElem(parent)

[wxID_VGDFLOWCHARTELEM, wxID_VGDFLOWCHARTELEMCHCMULTIPLE, 
 wxID_VGDFLOWCHARTELEMCHCPARALLEL, wxID_VGDFLOWCHARTELEMGCBBANNOTATION, 
 wxID_VGDFLOWCHARTELEMGCBBBROWSE, wxID_VGDFLOWCHARTELEMGCBBCANCEL, 
 wxID_VGDFLOWCHARTELEMGCBBDEL, wxID_VGDFLOWCHARTELEMGCBBOK, 
 wxID_VGDFLOWCHARTELEMGCBBSET, wxID_VGDFLOWCHARTELEMLBLINFO, 
 wxID_VGDFLOWCHARTELEMLBLSYMBOLDESC, wxID_VGDFLOWCHARTELEMLBLSYMBOLID, 
 wxID_VGDFLOWCHARTELEMLBLTEXT, wxID_VGDFLOWCHARTELEMLSTFOLLOW, 
 wxID_VGDFLOWCHARTELEMTXTINFO, wxID_VGDFLOWCHARTELEMTXTNAME, 
 wxID_VGDFLOWCHARTELEMTXTSYMBOLDESC, wxID_VGDFLOWCHARTELEMTXTSYMBOLID, 
 wxID_VGDFLOWCHARTELEMTXTTEXT, 
] = [wx.NewId() for _init_ctrls in range(19)]

class vgdFlowChartElem(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VGDFLOWCHARTELEM,
              name=u'vgdFlowChartElem', parent=prnt, pos=wx.Point(320, 89),
              size=wx.Size(440, 414), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'Flow Chart Element')
        self.SetClientSize(wx.Size(432, 387))

        self.txtInfo = wx.TextCtrl(id=wxID_VGDFLOWCHARTELEMTXTINFO,
              name=u'txtInfo', parent=self, pos=wx.Point(48, 44),
              size=wx.Size(128, 44), style=wx.TE_MULTILINE, value=u'')

        self.lblInfo = wx.StaticText(id=wxID_VGDFLOWCHARTELEMLBLINFO,
              label=u'Info', name=u'lblInfo', parent=self, pos=wx.Point(24, 48),
              size=wx.Size(18, 13), style=0)

        self.lstFollow = wx.ListView(id=wxID_VGDFLOWCHARTELEMLSTFOLLOW,
              name=u'lstFollow', parent=self, pos=wx.Point(8, 232),
              size=wx.Size(336, 112), style=wx.LC_REPORT)
        self.lstFollow.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstFollowListItemSelected,
              id=wxID_VGDFLOWCHARTELEMLSTFOLLOW)

        self.gcbbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTELEMGCBBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Delete', name=u'gcbbDel',
              parent=self, pos=wx.Point(352, 238), size=wx.Size(76, 30),
              style=0)
        self.gcbbDel.Bind(wx.EVT_BUTTON, self.OnGcbbDelButton,
              id=wxID_VGDFLOWCHARTELEMGCBBDEL)

        self.lblSymbolID = wx.StaticText(id=wxID_VGDFLOWCHARTELEMLBLSYMBOLID,
              label=u'Identifier', name=u'lblSymbolID', parent=self,
              pos=wx.Point(8, 8), size=wx.Size(40, 13), style=0)

        self.txtSymbolID = wx.TextCtrl(id=wxID_VGDFLOWCHARTELEMTXTSYMBOLID,
              name=u'txtSymbolID', parent=self, pos=wx.Point(56, 8),
              size=wx.Size(100, 21), style=0, value=u'')

        self.lblSymbolDesc = wx.StaticText(id=wxID_VGDFLOWCHARTELEMLBLSYMBOLDESC,
              label=u'Description', name=u'lblSymbolDesc', parent=self,
              pos=wx.Point(160, 8), size=wx.Size(53, 13), style=0)

        self.txtSymbolDesc = wx.TextCtrl(id=wxID_VGDFLOWCHARTELEMTXTSYMBOLDESC,
              name=u'txtSymbolDesc', parent=self, pos=wx.Point(216, 8),
              size=wx.Size(100, 21), style=0, value=u'')

        self.gcbbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTELEMGCBBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'gcbbOk',
              parent=self, pos=wx.Point(112, 352), size=wx.Size(76, 30),
              style=0)
        self.gcbbOk.Bind(wx.EVT_BUTTON, self.OnGcbbOkButton,
              id=wxID_VGDFLOWCHARTELEMGCBBOK)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTELEMGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(224, 352),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VGDFLOWCHARTELEMGCBBCANCEL)

        self.txtText = wx.TextCtrl(id=wxID_VGDFLOWCHARTELEMTXTTEXT,
              name=u'txtText', parent=self, pos=wx.Point(48, 96),
              size=wx.Size(296, 120), style=wx.TE_MULTILINE, value=u'')
        self.txtText.Bind(wx.EVT_CHAR, self.OnTxtTextChar)

        self.lblText = wx.StaticText(id=wxID_VGDFLOWCHARTELEMLBLTEXT,
              label=u'Text', name=u'lblText', parent=self, pos=wx.Point(16, 96),
              size=wx.Size(21, 13), style=0)

        self.txtName = wx.TextCtrl(id=wxID_VGDFLOWCHARTELEMTXTNAME,
              name=u'txtName', parent=self, pos=wx.Point(328, 8),
              size=wx.Size(100, 21), style=0, value=u'')
        self.txtName.Enable(False)

        self.chcMultiple = wx.CheckBox(id=wxID_VGDFLOWCHARTELEMCHCMULTIPLE,
              label=u'Multiple', name=u'chcMultiple', parent=self,
              pos=wx.Point(352, 328), size=wx.Size(73, 13), style=0)
        self.chcMultiple.SetValue(False)
        self.chcMultiple.Bind(wx.EVT_CHECKBOX, self.OnChcMultipleCheckbox,
              id=wxID_VGDFLOWCHARTELEMCHCMULTIPLE)

        self.gcbbSet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTELEMGCBBSET,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Set', name=u'gcbbSet',
              parent=self, pos=wx.Point(352, 288), size=wx.Size(76, 30),
              style=0)
        self.gcbbSet.Bind(wx.EVT_BUTTON, self.OnGcbbSetButton,
              id=wxID_VGDFLOWCHARTELEMGCBBSET)

        self.gcbbAnnotation = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTELEMGCBBANNOTATION,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Annotation',
              name=u'gcbbAnnotation', parent=self, pos=wx.Point(352, 40),
              size=wx.Size(76, 30), style=0)
        self.gcbbAnnotation.Bind(wx.EVT_BUTTON, self.OnGcbbAnnotationButton,
              id=wxID_VGDFLOWCHARTELEMGCBBANNOTATION)

        self.chcParallel = wx.CheckBox(id=wxID_VGDFLOWCHARTELEMCHCPARALLEL,
              label=u'Parallel', name=u'chcParallel', parent=self,
              pos=wx.Point(216, 40), size=wx.Size(73, 13), style=0)
        self.chcParallel.SetValue(False)
        self.chcParallel.Bind(wx.EVT_CHECKBOX, self.OnChcParallelCheckbox,
              id=wxID_VGDFLOWCHARTELEMCHCPARALLEL)

        self.gcbbBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTELEMGCBBBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbBrowse', parent=self, pos=wx.Point(352, 96),
              size=wx.Size(76, 30), style=0)
        self.gcbbBrowse.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseButton,
              id=wxID_VGDFLOWCHARTELEMGCBBBROWSE)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        self.dlgBrowse=None
        self.dlgBrowseVal=None
        self.IDname=''
        self.IDval=''
        self.IDtext=[]
        
        img=images.getOkBitmap()
        self.gcbbOk.SetBitmapLabel(img)
        self.gcbbSet.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
        
        img=images.getDelBitmap()
        self.gcbbDel.SetBitmapLabel(img)
        
        img=images.getNoteBitmap()
        self.gcbbAnnotation.SetBitmapLabel(img)
        
        img=images.getBrowseBitmap()
        self.gcbbBrowse.SetBitmapLabel(img)
        
        self.lstFollow.InsertColumn(0,u'Nr',wx.LIST_FORMAT_LEFT,30)
        self.lstFollow.InsertColumn(1,u'ID',wx.LIST_FORMAT_LEFT,80)
        self.lstFollow.InsertColumn(2,u'desc',wx.LIST_FORMAT_LEFT,80)
        self.lstFollow.InsertColumn(3,u'info',wx.LIST_FORMAT_LEFT,120)
        
    def __clear__(self):
        self.txtSymbolID.SetValue('')
        self.txtSymbolDesc.SetValue('')
        self.txtInfo.SetValue('')
        self.txtVal.SetValue('')
        self.txtName.SetValue('')
        self.txtText.SetValue('')
        self.IDtext=[]
    def SetBrowseDlg(self,dlg):
        self.dlgBrowse=dlg
    def SetInfos(self,voglFlowChartElement,voglFlowChart):
        #self.spnStepNr.SetValue(voglSFCstep.nr)
        self.voglFlowChart=voglFlowChart
        self.voglFlowChartElem=voglFlowChartElement
        self.txtSymbolID.SetValue(self.voglFlowChartElem.id)
        self.txtSymbolDesc.SetValue(self.voglFlowChartElem.desc)
        self.txtInfo.SetValue(self.voglFlowChartElem.info)
        #fix me
        #replace text
        if self.dlgBrowse is not None:
            self.dlgBrowse.CheckNode2Set()
            self.IDtext=string.split(self.voglFlowChartElem.IDtext,u',')
            i,j=0,0
            s=self.voglFlowChartElem.text
            for id in self.IDtext:
                i=string.find(s,'$',j)
                if i>=0:
                    j=string.find(s,'$',i+1)
                    if j>0:
                        diff=j-i-1
                        sID=self.dlgBrowse.GetLinkInfoById(id)[1]
                        s=s[:i+1]+sID+s[j:]
                        j=j+1-diff+len(sID)
            self.voglFlowChartElem.text=s
        self.txtText.SetValue(self.voglFlowChartElem.text)
        self.txtName.SetValue(self.voglFlowChartElem.getXmlType())
        self.__updateFollowing__()
    def __updateFollowing__(self):
        self.selFollow=-1
        self.lstFollow.DeleteAllItems()
        i=0
        for s in self.voglFlowChartElem.nextElem:
            index = self.lstFollow.InsertImageStringItem(sys.maxint, "%d"%i, -1)
            self.lstFollow.SetStringItem(index,1,s[0].id,-1)
            self.lstFollow.SetStringItem(index,2,s[0].desc,-1)
            self.lstFollow.SetStringItem(index,3,s[0].info,-1)
            self.lstFollow.SetItemData(index,i)
            i+=1
            pass
    def OnGcbbOkButton(self, event):
        #self.voglSFCstep.nr=self.txtNr.GetValue()
        self.voglFlowChartElem.id=self.txtSymbolID.GetValue()
        self.voglFlowChartElem.desc=self.txtSymbolDesc.GetValue()
        self.voglFlowChartElem.info=self.txtInfo.GetValue()
        self.voglFlowChartElem.text=self.txtText.GetValue()
        self.voglFlowChartElem.IDtext=string.join(self.IDtext,u',')
        self.EndModal(1)
        event.Skip()
    
    def OnLstFollowListItemSelected(self, event):
        self.selFollow=event.GetIndex()
        if self.voglFlowChartElem.nextElem[self.selFollow][2][0]['alternative']=='1':
            bVal=True
        else:
            bVal=False
        self.chcMultiple.SetValue(bVal)
        #self.chcJoin.SetValue(self.voglFlowChartElem.nextElem[self.selFollow][0].bJoin)
        event.Skip()
    def OnGcbbDelButton(self, event):
        if self.selFollow<0:
            return
        self.voglFlowChartElem.RemoveLink(self.selFollow)
        self.lstFollow.DeleteItem(self.selFollow)
        self.selFollow=-1
        #self.__updateTrans__()
        event.Skip()
    
    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()

    def OnChcMultipleCheckbox(self, event):
        event.Skip()

    def OnGcbbSetButton(self, event):
        if self.selFollow<0:
            return
        if self.chcMultiple.GetValue()==True:
            sVal='1'
        else:
            sVal='0'
        self.voglFlowChartElem.nextElem[self.selFollow][2][0]['alternative']=sVal
        #self.voglFlowChartElem.nextElem[self.selFollow][0].bJoin=self.chcJoin.GetValue()
        event.Skip()

    def OnGcbbAnnotationButton(self, event):
        event.Skip()

    def OnChcParallelCheckbox(self, event):
        event.Skip()

    def OnChcJoinCheckbox(self, event):
        event.Skip()
    def __replace__(self,strOld,strNew):
        i=string.find(strOld,'<')
        if i>=0:
            j=string.find(strOld,'>',i+1)
            if j>0:
                return strOld[:i+1]+strNew+strOld[j:]
        return '<'+strNew+'>'
    def __calcIdx__(self):
        s=self.txtText.GetValue()
        iLen=len(s)
        idx=0
        iPos=self.txtText.GetInsertionPoint()
        tup=self.txtText.PositionToXY(iPos)
        strs=string.split(s,'\n')
        iPos=0
        for i in range(0,tup[1]):
            iPos+=len(strs[i])+1
        iPos+=tup[0]
        i,j=0,0
        while i>=0:
            i=string.find(s,u'$',j)
            if i>=0:
                if i>=iPos:
                    return (idx,iPos,iPos)
                j=string.find(s,'$',i+1)
                if j>0:
                    if j<iPos:
                        idx+=1
                        j+=1
                    else:
                        return (idx,i,j+1)
                i=j
        return (idx,iPos,iPos)
    def OnGcbbBrowseButton(self, event):
        if self.dlgBrowse is not None:
            self.dlgBrowse.Centre()
            if self.dlgBrowse.ShowModal()>0:
                id,s=self.dlgBrowse.GetLinkInfo()
                idxTup=self.__calcIdx__()
                idx=idxTup[0]
                if idxTup[1]!=idxTup[2]:
                    self.IDtext.remove(self.IDtext[idx])
                self.IDtext.insert(idx,id)
                sOld=self.txtText.GetValue()
                sNew=sOld[:idxTup[1]]+u'$'+s+u'$'+sOld[idxTup[2]:]
                self.txtText.SetValue(sNew)
        event.Skip()
    def OnTxtTextChar(self, event):
        if event.GetKeyCode() in [8,127]:
            s=self.txtText.GetValue()
            iLen=len(s)
            idx=0
            iPos=self.txtText.GetInsertionPoint()
            tup=self.txtText.PositionToXY(iPos)
            strs=string.split(s,'\n')
            iPos=0
            for i in range(0,tup[1]):
                iPos+=len(strs[i])+1
            iPos+=tup[0]
            i,j=0,0
            lstIDs=[]
            while i>=0:
                i=string.find(s,u'$',j)
                if i>=0:
                    j=string.find(s,'$',i+1)
                    if j>0:
                        if j<iPos:
                            idx+=1
                            j+=1
                        else:
                            lstIDs.append((idx,i,j+1))
                    i=j
            idx=0
            for tup in lstIDs:
                iStart=tup[1]
                if event.GetKeyCode()==8:
                    iStart+=1
                if iPos>=iStart and iPos<=tup[2]:
                    self.IDtext.remove(self.IDtext[idx])
                    sOld=self.txtText.GetValue()
                    sNew=sOld[:tup[1]]+sOld[tup[2]:]
                    self.txtText.SetValue(sNew)
                    return
                idx+=1
        event.Skip()


#----------------------------------------------------------------------


class voglFlowChartElement(ogl.CompositeShape):
    def __init__(self, canvas,id ,desc , info ,w=0.0, h=0.0):
        ogl.CompositeShape.__init__(self)
        #self.AddText(info)
        self.id=id
        self.desc=desc
        self.info=info
        self.text=''
        self.IDtext=''
        self.xmlID=''
        self.bParallel=False
        self.bJoin=False
        self.w=w
        self.h=h
        
        self.txtInfo=ogl.TextShape(w,h)
        for s in string.split(self.info,'\n'):
            self.txtInfo.AddText(s)
            #self.txtInfo.AddText(self.info)
        self.AddChild(self.txtInfo)
        
        #self.txtInfo.SetX(-ba[0]/2.0)
        #self.txtInfo.SetY(-30.0)
        #self.txtInfo=None
        self.txtID=None
        self.txtDesc=None
        self.symbol=None
        self.canvas=canvas
        self.SetCanvas(canvas)
        self.prevElem=[]
        self.nextElem=[]
        self.Recompute()
        self.ClearTransitions()
    def SetText(self,text):
        self.text=text
    def SetIDText(self,IDtext):
        self.IDtext=IDtext
    def SetID(self,id):
        self.xmlID=id
    def GetID(self):
        return self.xmlID
    def __addSymbol__(self,dc=None):
        self.Recompute()
        ba=self.GetBoundingBoxMax()
        if self.txtInfo is None:
            self.txtInfo=ogl.TextShape(ba[0],ba[1])
            #s=string.replace(self.info,'\n',os.linesep)
            #self.txtInfo.AddText(s)
            for s in string.split(self.info,'\n'):
                self.txtInfo.AddText(s)
            self.txtInfo.SetDraggable(False, False)
            self.AddChild(self.txtInfo)
        if self.txtID is None:
            self.symbol=self.GetChildren()[-1]
            self.txtID=ogl.TextShape(45.0,30.0)
            self.txtID.AddText(self.id)
            self.txtID.SetDraggable(False, False)
            self.txtID.SetX(-30.0)
            self.txtID.SetY(-40.0)
            self.AddChild(self.txtID)
        else:
            if dc is not None:
                self.txtID.Erase(dc)
                self.txtID.FormatText(dc,self.id)
        if self.txtDesc is None:
            self.txtDesc=ogl.TextShape(45.0,30.0)
            self.txtDesc.AddText(self.desc)
            self.txtDesc.SetDraggable(False, False)
            self.txtDesc.SetX(20.0)
            self.txtDesc.SetY(-40.0)
            self.AddChild(self.txtDesc)
        else:
            if dc is not None:
                self.txtDesc.Erase(dc)
                self.txtDesc.FormatText(dc,self.desc)
        pass
    def __update__(self,dc):
        #for s in string.split(self.info,'\n'):
            #self.txtInfo.AddText(s)
        if dc is not None:
            self.txtInfo.Erase(dc)
        self.txtInfo.FormatText(dc,self.info)
        self.__addSymbol__(dc)
    def ClearTransitions(self):
        self.transitions={}
        for n in self.nextElem:
            self.transitions[n]=[]
    def SetType(self,type,used=True):
        """ possible type occording S88 are
            type = 0 transient state
                 = 1 quiescent state
                 = 2 final state
                 = 3 exception state
        """
        if used==True:
            if type==0:
                br=wx.Brush(wx.Colour(0xDF,0xDF,0xDF), wx.SOLID)
            elif type==1:
                br=wx.Brush(wx.Colour(0xAF,0xAF,0xAF), wx.SOLID)
            elif type==2:
                br=wx.Brush(wx.Colour(0x8F,0x8F,0x8F), wx.SOLID)
            elif type==3:
                br=wx.Brush(wx.Colour(0xBF,0x00,0x00), wx.SOLID)
            else:
                br=wx.Brush(wx.Colour(0xBF,0x00,0x00), wx.VERTICAL_HATCH)
        else:
            br=wx.Brush(wx.Colour(0x4F,0x4F,0x4F), wx.SOLID)
        self.type=type
        self.used=used
        self.SetBrush(br)
    def SetUsed(self,used):
        self.SetType(self.type,used)
    def HasAnnotation(self):
        for nextElem in self.nextElem:
            if hasattr(nextElem[0],'bAnnotation'):
                return True
        return False
    def AddLink(self,step,trans=None):
        line = ogl.LineShape()
        line.SetCanvas(self.canvas)
        line.SetPen(wx.BLACK_PEN)
        line.SetBrush(wx.BLACK_BRUSH)
        if hasattr(step,'bAnnotation')==False:
            line.AddArrow(ogl.ARROW_ARROW)
        
        line.MakeLineControlPoints(6)
        
        #self.step.AddLine(line, step.step)
        self.canvas.GetDiagram().AddShape(line)
        #line.SetFrom(self)
        #line.SetTo(step)
        if trans is None:
            trans=[{'name':'',
                'operation':'0==1',
                'val':'',
                'alternative':'0'}]
        self.nextElem.append((step,line,trans))
        step.prevElem.append(self)
        #self.AddChild(line)
        line.Show(True)
        self.__processLines__(None)
    def RemoveLink(self,idx):
        if idx > len(self.nextElem):
            return
        if idx<0:
            return
        n=self.nextElem[idx]
        n[0].prevElem.remove(self)
        n[1].Detach()
        n[1].RemoveFromCanvas(self.canvas)
        n[1].Unlink()
        self.nextElem=self.nextElem[:idx]+self.nextElem[idx+1:]
    def __processLines__(self,dc):
        for tup in self.nextElem:
            if tup[2][0].has_key('alternative'):
                if tup[2][0]['alternative'] == '1':
                    tup[1].SetPen(wx.BLACK_DASHED_PEN)
                else:
                    tup[1].SetPen(wx.BLACK_PEN)
            else:
                tup[1].SetPen(wx.BLACK_PEN)
            if self.symbol is None:
                xa=self.GetX()
                ya=self.GetY()
                ba=self.GetBoundingBoxMax()
            else:
                xa=self.symbol.GetX()
                ya=self.symbol.GetY()
                ba=self.symbol.GetBoundingBoxMax()
            
            if tup[0].symbol is None:
                xe=tup[0].GetX()
                ye=tup[0].GetY()
                be=tup[0].GetBoundingBoxMax()
            else:
                xe=tup[0].symbol.GetX()
                ye=tup[0].symbol.GetY()
                be=tup[0].symbol.GetBoundingBoxMax()
            
            if dc is not None:
                tup[1].Erase(dc)
            #tup[1].Show(False)
            
            if hasattr(tup[0],'bAnnotation'):
                be=tup[0].GetBoundingBoxMax()
                tup[1].SetPen(wx.BLACK_DASHED_PEN)
                controlPoints=tup[1].GetLineControlPoints()
                controlPoints[0][0]=xa+ba[0]/2.0+20
                controlPoints[0][1]=ya#+ba[1]/2.0+20
                
                controlPoints[1][0]=xa+ba[0]/2.0+20
                controlPoints[1][1]=ya#+ba[1]/2.0+20
                
                controlPoints[2][0]=xa+ba[0]/2.0+20
                controlPoints[2][1]=ya#+ba[1]/2.0+20
                
                controlPoints[3][0]=xe-be[1]/2.0-20
                controlPoints[3][1]=ya#+ba[1]/2.0+20
                
                controlPoints[4][0]=xe-be[1]/2.0-20
                controlPoints[4][1]=ye#+ba[1]/2.0+20
                
                controlPoints[5][0]=xe-be[1]/2.0
                controlPoints[5][1]=ye
                if dc is not None:
                    tup[1].Draw(dc)
                
                continue
            if ya>=ye:
                #tup[1].MakeLineControlPoints(6)
                
                if xa<xe:
                    x=xa-ba[0]/2.0-20
                else:
                    x=xe-be[0]/2.0-20
                
                # start processing
                #xa+=ba[0]/2.0
                ya+=ba[1]/2.0
                
                #xe+=be[0]/2.0
                ye-=be[1]/2.0
                
                yMin=sys.maxint
                lstBelow=[]
                objMin=None
                for pe in tup[0].prevElem:
                    yH=pe.symbol.GetY()
                    if yH>ye:
                        if yMin>yH:
                            yMin=yH
                            objMin=pe
                        lstBelow.append(pe)
                    #for neTup in pe.nextElem:
                def cmpObj(o1,o2):
                    if o1.symbol.GetY()<o2.symbol.GetY():
                        return -1
                    elif o1.symbol.GetY()>o2.symbol.GetY():
                        return 1
                    else:
                        return 0
                    #return o2.symbol.GetY()-o1.symbol.GetY()
                lstBelow.sort(cmpObj)
                controlPoints=tup[1].GetLineControlPoints()
                controlPoints[0][0]=xa
                controlPoints[0][1]=ya
                
                ba=self.GetBoundingBoxMax()
                controlPoints[1][0]=xa
                controlPoints[1][1]=ya+ba[1]/2.0+20
                
                if objMin==self:
                    controlPoints[2][0]=x
                    controlPoints[2][1]=ya+ba[1]/2.0+20
                    
                    controlPoints[3][0]=x
                    controlPoints[3][1]=ye-be[1]/2.0-20
                    
                    if len(tup[0].prevElem)>1:
                        controlPoints[4][0]=x
                        controlPoints[4][1]=ye-be[1]/2.0-20
                        
                        controlPoints[5][0]=xe
                        controlPoints[5][1]=ye-be[1]/2.0-20
                    else:
                        controlPoints[4][0]=xe
                        controlPoints[4][1]=ye-be[1]/2.0-20
                        
                        controlPoints[5][0]=xe
                        controlPoints[5][1]=ye
                else:
                    # join
                    grid=self.canvas.diagram.GetGridSpacing()
                    idx=lstBelow.index(self)
                    if idx>=1:
                        if self.symbol.GetY()<=(lstBelow[idx-1].symbol.GetY()+grid):
                            ya=lstBelow[idx-1].symbol.GetY()+grid+ba[1]/2.0
                    controlPoints[2][0]=xa
                    controlPoints[2][1]=ya+ba[1]/2.0+20

                    controlPoints[3][0]=xa
                    controlPoints[3][1]=ya+ba[1]/2.0+20
                    
                    controlPoints[4][0]=x
                    controlPoints[4][1]=ya+ba[1]/2.0+20
                    
                    bh=pe.symbol.GetBoundingBoxMax()
                    if idx>0:
                        for neTup in lstBelow[idx-1].nextElem:
                            if neTup[0]==tup[0]:
                                ch=neTup[1].GetLineControlPoints()
                                ye=ch[2][1]
                                break
                    controlPoints[5][0]=x
                    controlPoints[5][1]=ye#yMin+bh[1]+20
                    
            else:
                #tup[1].MakeLineControlPoints(4)
                # start processing
                #xa+=ba[0]/2.0
                ya+=ba[1]/2.0
                
                #xe+=be[0]/2.0
                ye-=be[1]/2.0
                controlPoints=tup[1].GetLineControlPoints()
                controlPoints[0][0]=xa
                controlPoints[0][1]=ya
                
                ba=self.GetBoundingBoxMax()
                ym=self.GetY()+ba[1]/2.0+20
                controlPoints[1][0]=xa
                controlPoints[1][1]=ym#ya+ba[1]/2.0+20
                
                controlPoints[2][0]=xa
                controlPoints[2][1]=ym#ya+ba[1]/2.0+20
                
                controlPoints[3][0]=xa
                controlPoints[3][1]=ym#ya+ba[1]/2.0+20
                
                #for pe in tup[0].prevElem:
                #    print pe,tup[0].prevElem.index(pe),self
                if tup[0].prevElem.index(self)>0:
                    if tup[0].prevElem[0].symbol.GetY()>tup[0].GetY():
                        bMod=True
                        while bMod == True:
                            bMod=False
                            for pe in tup[0].prevElem[1:]:
                                if pe != self:
                                    for neTup in pe.nextElem:
                                        neCP=neTup[1].GetLineControlPoints()
                                        if neCP[4][1]==ym:
                                            ym+=self.canvas.GetGridSpacing()
                                            bMod=True
                        controlPoints[4][0]=xe
                        controlPoints[4][1]=ym
                        
                        controlPoints[5][0]=xe
                        controlPoints[5][1]=ye-be[1]/2.0-20
                    else:
                        bMod=True
                        while bMod == True:
                            bMod=False
                            neTup=tup[0].prevElem[0].nextElem[0]
                            neCP=neTup[1].GetLineControlPoints()
                            ylim=neCP[4][1]
                            for pe in tup[0].prevElem[1:]:
                                if pe != self:
                                    for neTup in pe.nextElem:
                                        neCP=neTup[1].GetLineControlPoints()
                                        if neCP[4][1]==ym:
                                            ym+=self.canvas.diagram.GetGridSpacing()
                                            bMod=True
                                        elif ylim>ym:
                                            ym=ylim
                                            bMod=True
                        controlPoints[4][0]=xa
                        controlPoints[4][1]=ym
                        
                        controlPoints[5][0]=xe
                        controlPoints[5][1]=ym
                else:
                    controlPoints[4][0]=xe
                    controlPoints[4][1]=ym
                    
                    controlPoints[5][0]=xe
                    controlPoints[5][1]=ye
            #tup[1].Show(True)
            if dc is not None:
                tup[1].Draw(dc)
            #tup[1].OnDrawControlPoints(dc)
    def HideLinks(self,dc):
        for tup in self.nextElem:
            tup[1].Show(False)
            tup[1].Draw(dc)
    def HideLinksRec(self,dc):
        self.HideLinks(dc)
        for s in self.prevElem:
            s.HideLinks(dc)
            
    def UpdateLinks(self,dc):
        self.__processLines__(dc)
    def UpdateLinksRec(self,dc):
        self.__processLines__(dc)
        for s in self.prevElem:
            s.__processLines__(dc)
    def OnLeftClick(self, x, y, keys = 0, attachment = 0):
        #print "left click step"
        self.updateSelect()
    def updateSelect(self):
        shape = self
        canvas = self.canvas
        dc = wx.ClientDC(canvas)
        canvas.PrepareDC(dc)
        if shape.Selected():
            shape.Select(False, dc)
            canvas.Redraw(dc)
        else:
            redraw = False
            shapeList = canvas.GetDiagram().GetShapeList()
            toUnselect = []
        
            for s in shapeList:
                if s.Selected():
                    # If we unselect it now then some of the objects in
                    # shapeList will become invalid (the control points are
                    # shapes too!) and bad things will happen...
                    toUnselect.append(s)
            #try:
            #    if shape.IsSelectable()>0:
            #        shape.Select(True)
            #        print "selected 1",shape.Selected()
    def OnBeginDragLeft(self, x, y, keys=0, attachment=0):
        x,y=self.canvas.GetDiagram().Snap(x,y)
        ogl.CompositeShape.OnBeginDragLeft(self,x,y,keys,attachment)
    def OnEndDragLeft(self, x, y, keys=0, attachment=0):
        #print 'elem end drag',x,y
        #ylst=[]
        #yOld=self.symbol.GetY()
        #for tup in self.nextElem:
        #    ylst.append(tup[0].symbol.GetY())
        ogl.CompositeShape.OnEndDragLeft(self,x,y,keys,attachment)
        canvas = self.canvas
        dc = wx.ClientDC(canvas)
        canvas.PrepareDC(dc)
        yNew=self.symbol.GetY()
        iRep=2
        #for yh in ylst:
        #    if yOld<yh:
        #        if yNew>yh:
        #            iRep=3
        for i in range(0,iRep):
            self.UpdateLinksRec(dc)
            for tup in self.nextElem:
                tup[0].UpdateLinksRec(dc)
        self.canvas.SetModified()
    #def OnMovePost(self, dc, x, y, oldX, oldY, display):
    #    print 'move pos elem',x,y,oldX,oldY
    # 
    #def OnRightClick(self, x, y, keys = 0, attachment = 0):
    #    print "right clicked"
    #    pass
    def DoEdit(self):
        if self.canvas.dlgFlowChartElemt is None:
            self.canvas.dlgFlowChartElemt=vgdFlowChartElem(self.canvas)
        self.canvas.dlgFlowChartElemt.Centre()
        self.canvas.dlgFlowChartElemt.SetBrowseDlg(self.canvas.GetBrowseDlg())
        self.canvas.dlgFlowChartElemt.SetInfos(self,self.canvas)
        if self.canvas.dlgFlowChartElemt.ShowModal()>0:
            self.canvas.__setModified__(True)
            canvas = self.canvas
            dc = wx.ClientDC(canvas)
            canvas.PrepareDC(dc)
        
            self.UpdateLinksRec(dc)
            
            return 1
        return 0
    def GetLinkInfoById(self,id):
        node=self.canvas.doc.getIds().GetIdStr(id)[1]
        return self.GetLinkInfo(node)
    def GetLinkInfo(self,node=None):
        if node is not None:
            sName=Hierarchy.getTagNames(self.canvas.doc,self.canvas.baseNode,self.canvas.relNode,node)
            sID=self.canvas.doc.getAttribute(node,'id')
            return sID,sName
        return ('','')
    def UpdateReference(self):
        IDtext=string.split(self.IDtext,u',')
        i,j=0,0
        s=self.text
        for id in IDtext:
            i=string.find(s,'$',j)
            if i>=0:
                j=string.find(s,'$',i+1)
                if j>0:
                    diff=j-i-1
                    sID=self.GetLinkInfoById(id)[1]
                    s=s[:i+1]+sID+s[j:]
                    j=j+1-diff+len(sID)
        self.text=s
    def isData(self):
        return False
    def isProg(self):
        return False
    def isSys(self):
        return False
    def isNet(self):
        return False
    def isRes(self):
        return False
    
    def appendLatexPic(self,strs,yMax,scaleX,scaleY):
        x=self.symbol.GetX()/scaleX
        y=self.symbol.GetY()/scaleY
        ba=self.symbol.GetBoundingBoxMax()
        xa=x-(ba[0]/2.0/scaleX)
        ya=y-(ba[1]/2.0/scaleY)
        if self.txtID is not None:
            x=self.txtID.GetX()/scaleX
            y=self.txtID.GetY()/scaleY
            ba=self.txtID.GetBoundingBoxMax()
            tup=(x-(ba[0]/2.0/scaleX),yMax-(y+(ba[1]/2.0/scaleY)),ba[0]/scaleX,ba[1]/scaleY,
                    latex.texReplace(self.id))
            strs.append('    \\put(%4.1f,%4.1f){\\makebox(%4.1f,%4.1f){{\\footnotesize %s}}}'%tup)
        if self.txtDesc is not None:
            x=self.txtDesc.GetX()/scaleX
            y=self.txtDesc.GetY()/scaleY
            ba=self.txtDesc.GetBoundingBoxMax()
            tup=(x-(ba[0]/2.0/scaleX),yMax-(y+(ba[1]/2.0/scaleY)),ba[0]/scaleX,ba[1]/scaleY,
                    latex.texReplace(self.desc))
            strs.append('    \\put(%4.1f,%4.1f){\\makebox(%4.1f,%4.1f){{\\footnotesize %s}}}'%tup)
        if self.txtInfo is not None:
            x=self.txtInfo.GetX()/scaleX
            y=self.txtInfo.GetY()/scaleY
            ba=self.txtInfo.GetBoundingBoxMax()
            strsInfo=string.split(self.info,'\n')
            yh=yMax-(y+(ba[1]/2.0/scaleY))
            dy=ba[1]/scaleY
            dy=20/scaleY
            yh+=dy*((len(strsInfo)/2.0)+0.5)
            for s in strsInfo:
                tup=(x-(ba[0]/2.0/scaleX),yh,ba[0]/scaleX,dy,latex.texReplace(s))
                strs.append('    \\put(%4.1f,%4.1f){\\makebox(%4.1f,%4.1f){{\\footnotesize %s}}}'%tup)
                yh-=dy
            #tup=(x-(ba[0]/2.0/scaleX),yh,ba[0]/scaleX,dy,self.info)
            #strs.append('    \\put(%4.1f,%4.1f){\\makebox(%4.1f,%4.1f){{\\footnotesize %s}}}'%tup)
        return xa,ya
    def getXmlType(self):
        return None
    
