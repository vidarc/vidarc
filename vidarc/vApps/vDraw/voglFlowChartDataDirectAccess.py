#Boa:Dialog:vgdFlowChartData

from wx import WHITE_BRUSH
from wx import TRANSPARENT_BRUSH
from wx import BLACK_PEN
import wx.lib.ogl as ogl
import string,os,sys
import vidarc.vApps.vDraw.latex as latex
from vidarc.vApps.vDraw.voglFlowChartElement import voglFlowChartElement
import images

#----------------------------------------------------------------------

class voglFlowChartDataDirectAccess(voglFlowChartElement):
    def __init__(self, canvas,id,desc,info,w=0.0, h=0.0):
        voglFlowChartElement.__init__(self, canvas, id,desc,info, w, h)
        
        dw=w/6.0
        dh=h/4.0
        
        obj= ogl.DrawnShape()
        obj.SetDrawnBrush(WHITE_BRUSH)
        obj.SetDrawnPen(BLACK_PEN)
        obj.DrawLine((-dw*2.5, +dh*2.0), (+dw*3.0, +dh*2.0))
        obj.DrawLine((-dw*2.5, -dh*2.0), (+dw*3.0, -dh*2.0))
        obj.SetDrawnBrush(TRANSPARENT_BRUSH)
        obj.DrawArc((-dw*0.0, 0.0) , (-dw*2.5, -dh*2.0), (-dw*2.5, +dh*2.0))
        obj.DrawArc((+dw*6.0, 0.0) , (dw*3.0, -dh*2.0), (dw*3.0, +dh*2.0))
        obj.DrawArc((+dw*0.0, 0.0) , (dw*3.0, +dh*2.0), (dw*3.0, -dh*2.0))
        obj.SetDraggable(False)
        obj.SetSensitivityFilter(0,True)
        obj.CalculateSize()
        self.AddChild(obj)
        
        self.__addSymbol__()
        self.SetSensitivityFilter(0)
        self.Recompute()
        self.SetDraggable(True)
    def getXmlType(self):
        return 'datadirect'
    def appendLatexPic(self,strs,yMax,scaleX,scaleY):
        x=self.symbol.GetX()/scaleX
        y=self.symbol.GetY()/scaleY
        ba=self.symbol.GetBoundingBoxMax()
        xa=x-(self.w/2.0/scaleX)
        ya=y-(self.h/2.0/scaleY)
        xh1,yh1=xa,yMax-(y+(self.h/2.0/scaleY))
        xh2,yh2=xa+(self.w/scaleX),yh1+(self.h/scaleY)
        yh3=yMax-y
        
        tup=(xh1,yh2,xh1-(self.w/6.0/scaleX),yh3,xh1,yh1)
        strs.append('    \\qbezier(%4.1f,%4.1f)(%4.1f,%4.1f)(%4.1f,%4.1f)'%tup)
        tup=latex.lineFit(xh1,yh2,xh2,yh2)
        strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
        tup=(xh2,yh2,xh2-(self.w/6.0/scaleX),yh3,xh2,yh1)
        strs.append('    \\qbezier(%4.1f,%4.1f)(%4.1f,%4.1f)(%4.1f,%4.1f)'%tup)
        tup=(xh2,yh2,xh2+(self.w/6.0/scaleX),yh3,xh2,yh1)
        strs.append('    \\qbezier(%4.1f,%4.1f)(%4.1f,%4.1f)(%4.1f,%4.1f)'%tup)
        tup=latex.lineFit(xh1,yh1,xh2,yh1)
        strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
        voglFlowChartElement.appendLatexPic(self,strs,yMax,scaleX,scaleY)
        return xa,ya
    def DoEdit3(self):
        if self.canvas.dlgS88state is None:
            self.canvas.dlgS88state=vgdS88state(self.canvas)
        self.canvas.dlgS88state.Centre()
        self.canvas.dlgS88state.SetInfos(self,self.canvas)
        self.canvas.dlgS88state.SetBrowseDlg(self.canvas.GetBrowseDlg())
        self.canvas.dlgS88state.SetBrowseValDlg(self.canvas.GetBrowseValDlg())
        if self.canvas.dlgS88state.ShowModal()>0:
            return 1
        return 0
    def isData(self):
        return True
    def isSys(self):
        return True
    def isNet(self):
        return True
    def isRes(self):
        return True

