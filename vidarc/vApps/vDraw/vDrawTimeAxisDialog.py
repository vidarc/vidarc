#Boa:Dialog:vDrawTimeAxisDialog

import wx
import wx.lib.buttons

import images

def create(parent):
    return vDrawTimeAxisDialog(parent)

[wxID_VDRAWTIMEAXISDIALOG, wxID_VDRAWTIMEAXISDIALOGGCBBCANCEL, 
 wxID_VDRAWTIMEAXISDIALOGGCBBOK, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vDrawTimeAxisDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VDRAWTIMEAXISDIALOG,
              name=u'vDrawTimeAxisDialog', parent=prnt, pos=wx.Point(325, 103),
              size=wx.Size(400, 380), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'Time Axis')
        self.SetClientSize(wx.Size(392, 353))

        self.gcbbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VDRAWTIMEAXISDIALOGGCBBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'gcbbOk',
              parent=self, pos=wx.Point(96, 312), size=wx.Size(76, 30),
              style=0)
        self.gcbbOk.Bind(wx.EVT_BUTTON, self.OnGcbbOkButton,
              id=wxID_VDRAWTIMEAXISDIALOGGCBBOK)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VDRAWTIMEAXISDIALOGGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(192, 312),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VDRAWTIMEAXISDIALOGGCBBCANCEL)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        img=images.getApplyBitmap()
        self.gcbbApply.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)

    def OnGcbbOkButton(self, event):
        self.EndModal(1)
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
        
