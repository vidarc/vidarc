#Boa:Dialog:vgdFlowChartData

from wx import TRANSPARENT_BRUSH
from wx import BLACK_PEN
import wx.lib.ogl as ogl
import string,os,sys
import vidarc.vApps.vDraw.latex as latex
from vidarc.vApps.vDraw.voglFlowChartElement import voglFlowChartElement
import images

#----------------------------------------------------------------------

class voglFlowChartDataSeqAccess(voglFlowChartElement):
    def __init__(self, canvas,id,desc,info,w=0.0, h=0.0):
        voglFlowChartElement.__init__(self, canvas, id,desc,info, w, h)
        
        dw=w/6.0
        dh=h/4.0
        
        obj= ogl.CircleShape(dw*4.15)
        obj.SetBrush(TRANSPARENT_BRUSH)
        obj.SetPen(BLACK_PEN)
        obj.SetDraggable(False)
        obj.SetSensitivityFilter(0,True)
        self.AddChild(obj)
        
        line = ogl.LineShape()
        line.SetCanvas(canvas)
        line.SetPen(BLACK_PEN)
        #line.SetBrush(wx.BLACK_BRUSH)
        line.MakeLineControlPoints(2)
        line.SetEnds(0,dh*2.0,dw*2.0,dh*2.0)
        self.AddChild(line)
        line.Show(True)
            
        self.__addSymbol__()
        self.SetSensitivityFilter(0)
        self.Recompute()
        self.SetDraggable(True)
    def getXmlType(self):
        return 'dataseqaccess'
    def appendLatexPic(self,strs,yMax,scaleX,scaleY):
        x=self.symbol.GetX()/scaleX
        y=self.symbol.GetY()/scaleY
        ba=self.symbol.GetBoundingBoxMax()
        xa=x#-(ba[0]/2.0/scaleX)
        ya=yMax-y
        xh1,yh1=xa,yMax-(y+(ba[1]/2.0/scaleY))
        xh2,yh2=xa-(self.w/6.0/scaleX),ya+(self.h/4.0*2.0/scaleY)
        
        tup=(xh2,yh2,self.w/6.0*4.0)
        strs.append('    \\put(%4.1f,%4.1f){\\circle{%4.1f}}'%tup)
        tup=latex.lineFit(xh2,yh1,xh2+(self.w/6.0*2.0/scaleX),yh1)
        strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
        voglFlowChartElement.appendLatexPic(self,strs,yMax,scaleX,scaleY)
        return xa,ya
    def DoEdit3(self):
        if self.canvas.dlgS88state is None:
            self.canvas.dlgS88state=vgdS88state(self.canvas)
        self.canvas.dlgS88state.Centre()
        self.canvas.dlgS88state.SetInfos(self,self.canvas)
        self.canvas.dlgS88state.SetBrowseDlg(self.canvas.GetBrowseDlg())
        self.canvas.dlgS88state.SetBrowseValDlg(self.canvas.GetBrowseValDlg())
        if self.canvas.dlgS88state.ShowModal()>0:
            return 1
        return 0
    def isData(self):
        return True
    def isProg(self):
        return True
    def isSys(self):
        return True
    def isNet(self):
        return True
    def isRes(self):
        return True

