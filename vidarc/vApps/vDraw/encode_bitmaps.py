#!/usr/bin/env python
#----------------------------------------------------------------------
#----------------------------------------------------------------------------
# Name:         encode_bitmaps.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: encode_bitmaps.py,v 1.1 2005/12/13 13:19:34 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

import sys
from wxPython.tools import img2py


command_lines = [
    #"   -u -i -n Application Tools01_16.png images.py",
#    "   -u -i -n Mondrian mondrian.ico images.py",
    "-u -i -n Cancel img/abort.png images.py",
    "-a -u -n Ok img/ok.png images.py",
    "-a -u -n Del img/waste.png images.py",
    "-a -u -n Add img/add.png images.py",
    "-a -u -n Draw img/Draw01_16.png images.py",
    "-a -u -n Note img/Note04_16.png images.py",
    "-a -u -n Browse img/BrowseFile01_16.png images.py",
    "-a -u -n Link img/Link03_16.png images.py",
    
    "-u -i -n Card iso5807/FC_card01_16.png images_iso5807.py",
    "-a -u -n CommLink iso5807/FC_commLink01_16.png images_iso5807.py",
    "-a -u -n controlTrans iso5807/FC_controlTrans01_16.png images_iso5807.py",
    "-a -u -n Data iso5807/FC_data01_16.png images_iso5807.py",
    "-a -u -n DataDirect iso5807/FC_dataDirect01_16.png images_iso5807.py",
    "-a -u -n DataInternal iso5807/FC_dataInternal01_16.png images_iso5807.py",
    "-a -u -n DataMan iso5807/FC_dataMan01_16.png images_iso5807.py",
    "-a -u -n DataSeq iso5807/FC_dataSeq01_16.png images_iso5807.py",
    "-a -u -n DataStored iso5807/FC_dataStored01_16.png images_iso5807.py",
    "-a -u -n Desc iso5807/FC_desc01_16.png images_iso5807.py",
    "-a -u -n Doc iso5807/FC_doc01_16.png images_iso5807.py",
    "-a -u -n Display iso5807/FC_disp01_16.png images_iso5807.py",
    "-a -u -n inMan iso5807/FC_inMan01_16.png images_iso5807.py",
    "-a -u -n LoopStart iso5807/FC_loopStart01_16.png images_iso5807.py",
    "-a -u -n LoopEnd iso5807/FC_loopEnd01_16.png images_iso5807.py",
    "-a -u -n PaperTape iso5807/FC_paperTape01_16.png images_iso5807.py",
    "-a -u -n Proc iso5807/FC_proc01_16.png images_iso5807.py",
    "-a -u -n ProcMan iso5807/FC_procMan01_16.png images_iso5807.py",
    "-a -u -n ProcPredef iso5807/FC_procPredef01_16.png images_iso5807.py",
    "-a -u -n ProcPrep iso5807/FC_procPrep01_16.png images_iso5807.py",
    "-a -u -n Connect iso5807/FC_connect01_16.png images_iso5807.py",
    "-a -u -n Term iso5807/FC_term01_16.png images_iso5807.py",
    "-a -u -n Annotation iso5807/FC_annotation01_16.png images_iso5807.py",
    "-a -u -n Ellipsis iso5807/FC_ellipsis01_16.png images_iso5807.py",

    ]


for line in command_lines:
    args = line.split()
    img2py.main(args)

