#Boa:Dialog:vgdFlowChartData

from wx import WHITE_BRUSH
from wx import TRANSPARENT_BRUSH
from wx import BLACK_PEN
import wx.lib.ogl as ogl
import string,os,sys

from vidarc.vApps.vDraw.voglFlowChartElement import voglFlowChartElement
import images

#----------------------------------------------------------------------

class voglFlowChartDataMaschine(voglFlowChartElement):
    def __init__(self, canvas,id,desc,info,w=0.0, h=0.0):
        voglFlowChartElement.__init__(self, canvas, id,desc,info, w, h)
        
        dw=w/6.0
        dh=h/4.0
        
        obj= ogl.DrawnShape()
        obj.SetDrawnBrush(WHITE_BRUSH)
        obj.SetDrawnPen(BLACK_PEN)
        obj.DrawLine((-dw*2.5, +dh*2.0), (+dw*3.0, +dh*2.0))
        obj.DrawLine((-dw*2.5, -dh*2.0), (+dw*3.0, -dh*2.0))
        obj.SetDrawnBrush(TRANSPARENT_BRUSH)
        obj.DrawArc((-dw*0.0, 0.0) , (-dw*2.5, -dh*2.0), (-dw*2.5, +dh*2.0))
        obj.DrawArc((+dw*6.0, 0.0) , (dw*3.0, -dh*2.0), (dw*3.0, +dh*2.0))
        #obj.DrawSpline([(-dw*2.5, -dh*2.0), (-dw*3.0, 0.0),(-dw*2.5, +dh*2.0)])
        #obj.DrawSpline([(+dw*3.0, -dh*2.0), (+dw*2.5, 0.0),(+dw*3.0, +dh*2.0)])
        obj.SetDraggable(False)
        obj.SetSensitivityFilter(0,True)
        self.AddChild(obj)
        
        self.__addSymbol__()
        self.SetSensitivityFilter(0)
        self.Recompute()
        self.SetDraggable(True)
    def isData(self):
        return True
    def isProg(self):
        return True
    def isSys(self):
        return True
    def isNet(self):
        return True
    def isRes(self):
        return True
