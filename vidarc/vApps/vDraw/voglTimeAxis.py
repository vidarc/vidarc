#Boa:Dialog:vgdTimeAxis

import wx
import wx.lib.buttons
import wx.lib.ogl as ogl

import images

def create(parent):
    return vgdTimeAxis(parent)

[wxID_VGDTIMEAXIS, wxID_VGDTIMEAXISGCBBCANCEL, wxID_VGDTIMEAXISGCBBOK, 
 wxID_VGDTIMEAXISLBLCOUNT, wxID_VGDTIMEAXISSPNCOUNT, 
] = [wx.NewId() for _init_ctrls in range(5)]

class vgdTimeAxis(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VGDTIMEAXIS, name=u'vgdTimeAxis',
              parent=prnt, pos=wx.Point(325, 103), size=wx.Size(209, 148),
              style=wx.DEFAULT_DIALOG_STYLE, title=u'Time Axis')
        self.SetClientSize(wx.Size(201, 121))

        self.gcbbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDTIMEAXISGCBBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'gcbbOk',
              parent=self, pos=wx.Point(24, 80), size=wx.Size(76, 30), style=0)
        self.gcbbOk.Bind(wx.EVT_BUTTON, self.OnGcbbOkButton,
              id=wxID_VGDTIMEAXISGCBBOK)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDTIMEAXISGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(112, 80),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VGDTIMEAXISGCBBCANCEL)

        self.spnCount = wx.SpinCtrl(id=wxID_VGDTIMEAXISSPNCOUNT, initial=0,
              max=100, min=0, name=u'spnCount', parent=self, pos=wx.Point(64,
              16), size=wx.Size(117, 21), style=wx.SP_ARROW_KEYS)

        self.lblCount = wx.StaticText(id=wxID_VGDTIMEAXISLBLCOUNT,
              label=u'Count', name=u'lblCount', parent=self, pos=wx.Point(32,
              16), size=wx.Size(28, 13), style=0)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        img=images.getOkBitmap()
        self.gcbbOk.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)

    def OnGcbbOkButton(self, event):
        self.EndModal(1)
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
    def GetCount(self):
        return self.spnCount.GetValue()
    def SetCount(self,count):
        self.spnCount.SetValue(count)
class voglTimeAxis(ogl.CompositeShape):
    def __init__(self, canvas,w=5, h=5,count=10,parent=None):
        self.dlg=None
        self.count=count
        self.parent=parent
        self.w=w
        self.h=h
        ogl.CompositeShape.__init__(self)
        
        self.SetCanvas(canvas)
        
        line=ogl.LineShape()
        line.SetPen(wx.BLACK_PEN)
        line.SetBrush(wx.BLACK_BRUSH)
        line.MakeLineControlPoints(2)
        line.AddArrow(ogl.ARROW_ARROW)
        line.SetEnds(0,0,200,0)
        
        text=ogl.TextShape(60,40)
        text.AddText('Time')
        text.SetX(230)
        text.SetY(0)
        self.text=text
                
        self.AddChild(line)
        self.AddChild(text)
        
        #constraint = ogl.Constraint(ogl.CONSTRAINT_MIDALIGNED_BOTTOM, constraining_shape, [constrained_shape1, constrained_shape2])
        #self.AddConstraint(constraint)
        self.Recompute()

        # If we don't do this, the shapes will be able to move on their
        # own, instead of moving the composite
        line.SetDraggable(False)
        text.SetDraggable(False)
        
        # If we don't do this the shape will take all left-clicks for itself
        line.SetSensitivityFilter(0)
        text.SetSensitivityFilter(0)
        self.line=line
    def old(self):
        child=ogl.PolygonShape()
        if w == 0.0:
            w = 60.0
        if h == 0.0:
            h = 60.0

        points = [ (0.0,    -50),
                   (100,  0.0),
                   (0.0,    50),
                   (-100, 0.0),
                   ]

        child.Create(points)
        self.AddChild(child)
        
        self.Recompute()
        self.SetSensitivityFilter()
        #self.AddText('text')
        #self.__draw__()
    def GetOrigin(self):
        b=self.line.GetBoundingBoxMax()
        x=self.line.GetX()-b[0]/2.0
        y=self.line.GetY()-b[1]/2.0
        return (x,y)
    def __draw__(self):
        b=self.line.GetBoundingBoxMax()
        x=self.line.GetX()-b[0]/2.0
        y=self.line.GetY()-b[1]/2.0
        self.line.SetEnds(x,y,x+self.count*self.w,y)
        self.text.SetX(x+self.count*self.w+30)
        self.Recompute()
        #self.CalculateSize()
        #self.Show(True)
    def dd(self):
        self.SetDrawnPen(wx.BLACK_PEN)
        h=self.h/2
        for i in range(1,self.count):
            self.DrawLine((i*self.w, -h), (i*self.w, h))

        self.DrawLine((0, 0), (self.count*self.w, 0))

        self.SetDrawnTextColour(wx.BLACK)
        self.SetDrawnFont(wx.Font(8, wx.SWISS, wx.NORMAL, wx.NORMAL))
        self.DrawText("time", (self.count*self.w+self.w/2, -h))
        self.CalculateSize()
        self.Show(True)
    def IsSelectable(self):
        return 0
    def DoEdit(self):
        if self.dlg is None:
            self.dlg=vgdTimeAxis(self.parent)
        self.dlg.Centre()
        self.dlg.SetCount(self.count)
        if self.dlg.ShowModal()>0:
            oldCount=self.count
            self.count=self.dlg.GetCount()
            if oldCount!=self.count:
                self.ClearText()
                self.__draw__()
                return 1
        return 0
    