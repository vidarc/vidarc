#Boa:Dialog:vgdFlowChart

import wx
import wx.lib.buttons
import wx.lib.ogl as ogl
import string,os,sys
#from xml.dom.minidom import Element

import vidarc.apps.xmlBase.vtXmlDomTree as vtXmlDomTree

import images

def create(parent):
    return vgdFlowChart(parent)

[wxID_VGDFLOWCHART, wxID_VGDFLOWCHARTCHCCOMBINE, wxID_VGDFLOWCHARTCHCOPERATOR, 
 wxID_VGDFLOWCHARTCHCUSED, wxID_VGDFLOWCHARTGCBBADD, 
 wxID_VGDFLOWCHARTGCBBBROWSE, wxID_VGDFLOWCHARTGCBBBROWSEVAL, 
 wxID_VGDFLOWCHARTGCBBDEL, wxID_VGDFLOWCHARTGCBBEDIT, wxID_VGDFLOWCHARTGCBBOK, 
 wxID_VGDFLOWCHARTGCBBSET, wxID_VGDFLOWCHARTLBLCOMB, 
 wxID_VGDFLOWCHARTLBLLEVEL, wxID_VGDFLOWCHARTLBLOP, 
 wxID_VGDFLOWCHARTLBLSTEPNAME, wxID_VGDFLOWCHARTLBLTAG, 
 wxID_VGDFLOWCHARTLBLVAL, wxID_VGDFLOWCHARTLSTFOLLOW, 
 wxID_VGDFLOWCHARTLSTTRANS, wxID_VGDFLOWCHARTSPNLEVEL, 
 wxID_VGDFLOWCHARTTXTNAME, wxID_VGDFLOWCHARTTXTSTATENAME, 
 wxID_VGDFLOWCHARTTXTVAL, 
] = [wx.NewId() for _init_ctrls in range(23)]

class vgdFlowChart(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VGDFLOWCHART, name=u'vgdFlowChart',
              parent=prnt, pos=wx.Point(320, 89), size=wx.Size(480, 429),
              style=wx.DEFAULT_DIALOG_STYLE, title=u'Flow Chart')
        self.SetClientSize(wx.Size(472, 402))

        self.txtStateName = wx.TextCtrl(id=wxID_VGDFLOWCHARTTXTSTATENAME,
              name=u'txtStateName', parent=self, pos=wx.Point(48, 4),
              size=wx.Size(100, 21), style=0, value=u'')

        self.lblStepName = wx.StaticText(id=wxID_VGDFLOWCHARTLBLSTEPNAME,
              label=u'Name', name=u'lblStepName', parent=self, pos=wx.Point(8,
              8), size=wx.Size(28, 13), style=0)

        self.lstFollow = wx.ListView(id=wxID_VGDFLOWCHARTLSTFOLLOW,
              name=u'lstFollow', parent=self, pos=wx.Point(8, 32),
              size=wx.Size(336, 96), style=wx.LC_REPORT)
        self.lstFollow.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstFollowListItemSelected, id=wxID_VGDFLOWCHARTLSTFOLLOW)

        self.gcbbEdit = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTGCBBEDIT,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Edit', name=u'gcbbEdit',
              parent=self, pos=wx.Point(384, 40), size=wx.Size(76, 30),
              style=0)
        self.gcbbEdit.Bind(wx.EVT_BUTTON, self.OnGcbbEditButton,
              id=wxID_VGDFLOWCHARTGCBBEDIT)

        self.lstTrans = wx.ListView(id=wxID_VGDFLOWCHARTLSTTRANS,
              name=u'lstTrans', parent=self, pos=wx.Point(8, 144),
              size=wx.Size(368, 84), style=wx.LC_REPORT | wx.LC_SORT_ASCENDING)
        self.lstTrans.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstTransListItemSelected, id=wxID_VGDFLOWCHARTLSTTRANS)

        self.gcbbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTGCBBADD,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'gcbbAdd',
              parent=self, pos=wx.Point(384, 144), size=wx.Size(76, 30),
              style=0)
        self.gcbbAdd.Bind(wx.EVT_BUTTON, self.OnGcbbAddButton,
              id=wxID_VGDFLOWCHARTGCBBADD)

        self.gcbbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTGCBBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Delete', name=u'gcbbDel',
              parent=self, pos=wx.Point(384, 182), size=wx.Size(76, 30),
              style=0)
        self.gcbbDel.Bind(wx.EVT_BUTTON, self.OnGcbbDelButton,
              id=wxID_VGDFLOWCHARTGCBBDEL)

        self.txtName = wx.TextCtrl(id=wxID_VGDFLOWCHARTTXTNAME, name=u'txtName',
              parent=self, pos=wx.Point(8, 248), size=wx.Size(368, 21), style=0,
              value=u'')

        self.gcbbBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTGCBBBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbBrowse', parent=self, pos=wx.Point(384, 244),
              size=wx.Size(76, 30), style=0)
        self.gcbbBrowse.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseButton,
              id=wxID_VGDFLOWCHARTGCBBBROWSE)

        self.chcOperator = wx.Choice(choices=[u'==', u'<', u'<=', u'>', u'>=',
              u'!=', u'1==1', u'0==1', u'?'], id=wxID_VGDFLOWCHARTCHCOPERATOR,
              name=u'chcOperator', parent=self, pos=wx.Point(8, 284),
              size=wx.Size(50, 21), style=0)
        self.chcOperator.SetSelection(0)
        self.chcOperator.SetToolTipString(u'operation')

        self.txtVal = wx.TextCtrl(id=wxID_VGDFLOWCHARTTXTVAL, name=u'txtVal',
              parent=self, pos=wx.Point(64, 284), size=wx.Size(312, 21),
              style=0, value=u'')

        self.gcbbBrowseVal = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTGCBBBROWSEVAL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbBrowseVal', parent=self, pos=wx.Point(384, 280),
              size=wx.Size(76, 30), style=0)
        self.gcbbBrowseVal.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseValButton,
              id=wxID_VGDFLOWCHARTGCBBBROWSEVAL)

        self.chcCombine = wx.Choice(choices=[u'and', u'nand', u'or', u'nor'],
              id=wxID_VGDFLOWCHARTCHCCOMBINE, name=u'chcCombine', parent=self,
              pos=wx.Point(64, 316), size=wx.Size(50, 21), style=0)
        self.chcCombine.SetSelection(0)

        self.spnLevel = wx.SpinCtrl(id=wxID_VGDFLOWCHARTSPNLEVEL, initial=0,
              max=10, min=0, name=u'spnLevel', parent=self, pos=wx.Point(160,
              316), size=wx.Size(50, 21), style=wx.SP_ARROW_KEYS)
        self.spnLevel.SetValue(0)

        self.gcbbSet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTGCBBSET,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Set', name=u'gcbbSet',
              parent=self, pos=wx.Point(384, 324), size=wx.Size(76, 30),
              style=0)
        self.gcbbSet.Bind(wx.EVT_BUTTON, self.OnGcbbSetButton,
              id=wxID_VGDFLOWCHARTGCBBSET)

        self.lblTag = wx.StaticText(id=wxID_VGDFLOWCHARTLBLTAG, label=u'Tag',
              name=u'lblTag', parent=self, pos=wx.Point(8, 232),
              size=wx.Size(22, 13), style=0)

        self.lblOp = wx.StaticText(id=wxID_VGDFLOWCHARTLBLOP,
              label=u'Operation', name=u'lblOp', parent=self, pos=wx.Point(8,
              270), size=wx.Size(46, 13), style=0)

        self.lblVal = wx.StaticText(id=wxID_VGDFLOWCHARTLBLVAL, label=u'Value',
              name=u'lblVal', parent=self, pos=wx.Point(68, 270),
              size=wx.Size(27, 13), style=0)

        self.lblLevel = wx.StaticText(id=wxID_VGDFLOWCHARTLBLLEVEL,
              label=u'Level', name=u'lblLevel', parent=self, pos=wx.Point(128,
              320), size=wx.Size(26, 13), style=0)

        self.lblComb = wx.StaticText(id=wxID_VGDFLOWCHARTLBLCOMB,
              label=u'Combine', name=u'lblComb', parent=self, pos=wx.Point(8,
              320), size=wx.Size(41, 13), style=0)

        self.gcbbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTGCBBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'gcbbOk',
              parent=self, pos=wx.Point(176, 368), size=wx.Size(76, 30),
              style=0)
        self.gcbbOk.Bind(wx.EVT_BUTTON, self.OnGcbbOkButton,
              id=wxID_VGDFLOWCHARTGCBBOK)

        self.chcUsed = wx.CheckBox(id=wxID_VGDFLOWCHARTCHCUSED, label=u'Used',
              name=u'chcUsed', parent=self, pos=wx.Point(168, 8),
              size=wx.Size(73, 13), style=0)
        self.chcUsed.SetValue(False)
        self.chcUsed.Bind(wx.EVT_CHECKBOX, self.OnChcUsedCheckbox,
              id=wxID_VGDFLOWCHARTCHCUSED)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        self.dlgBrowse=None
        self.dlgBrowseVal=None
        self.IDname=''
        self.IDval=''
        
        img=images.getOkBitmap()
        self.gcbbOk.SetBitmapLabel(img)
        self.gcbbSet.SetBitmapLabel(img)
        
        img=images.getDelBitmap()
        self.gcbbDel.SetBitmapLabel(img)
        img=images.getDrawBitmap()
        self.gcbbEdit.SetBitmapLabel(img)
        
        img=images.getAddBitmap()
        self.gcbbAdd.SetBitmapLabel(img)
        
        img=images.getBrowseBitmap()
        self.gcbbBrowse.SetBitmapLabel(img)
        self.gcbbBrowseVal.SetBitmapLabel(img)
        
        self.lstFollow.InsertColumn(0,u'Nr',wx.LIST_FORMAT_LEFT,30)
        self.lstFollow.InsertColumn(1,u'Name',wx.LIST_FORMAT_LEFT,200)
        
        self.lstTrans.InsertColumn(0,u'Nr',wx.LIST_FORMAT_LEFT,30)
        self.lstTrans.InsertColumn(1,u'Name',wx.LIST_FORMAT_LEFT,150)
        self.lstTrans.InsertColumn(2,u'Oper',wx.LIST_FORMAT_CENTER,50)
        self.lstTrans.InsertColumn(3,u'Value',wx.LIST_FORMAT_LEFT,70)
        self.lstTrans.InsertColumn(4,u'Comb',wx.LIST_FORMAT_LEFT,30)
        self.lstTrans.InsertColumn(5,u'Level',wx.LIST_FORMAT_LEFT,30)
        self.__setTransEnable__(False)
    def __setTransEnable__(self,state):
        self.txtName.Enable(state)
        self.chcOperator.Enable(state)
        self.gcbbBrowse.Enable(state)
        self.txtVal.Enable(state)
        self.gcbbBrowseVal.Enable(state)
        self.chcCombine.Enable(state)
        self.spnLevel.Enable(state)
        self.gcbbSet.Enable(state)
    def __clear__(self):
        self.txtName.SetValue('')
        self.chcOperator.SetSelection(0)
        self.txtVal.SetValue('')    
    def SetNrs(self,nrs):
        self.nrs=nrs
    def SetInfos(self,voglS88state,voglDrawS88):
        #self.spnStepNr.SetValue(voglSFCstep.nr)
        self.voglS88=voglDrawS88
        self.voglS88state=voglS88state
        self.txtStateName.SetValue(self.voglS88state.name)
        if self.voglS88state.name in ['idle','running','complete']:
            self.chcUsed.SetValue(True)
            self.chcUsed.Enable(False)
        else:
            self.chcUsed.SetValue(self.voglS88state.used)
            self.chcUsed.Enable(True)
        self.__updateUsed__()
        self.__updateFollowing__()
    def __updateUsed__(self):
        if self.chcUsed.IsChecked():
            self.lstFollow.Enable(True)
            self.lstTrans.Enable(True)
        else:
            self.lstFollow.Enable(False)
            self.lstTrans.Enable(False)
            self.__setTransEnable__(False)
    def SetBrowseDlg(self,dlg):
        self.dlgBrowse=dlg
    def SetBrowseValDlg(self,dlg):
        self.dlgBrowseVal=dlg
    def __updateTrans__(self):
        self.selTrans=-1
        self.IDname=''
        self.IDval=''
        self.lstTrans.DeleteAllItems()
        self.__setTransEnable__(False)
        self.__clear__()
        if self.selFollow<0:
            return
        #self.txtName.SetValue('')
        #self.txtVal.SetValue('')
        # build steps
        self.selTrans=-1
        self.lstTrans.DeleteAllItems()
        i=0
        for trans in self.voglS88state.transitions[self.voglS88state.nextState[self.selFollow]]:
            index = self.lstTrans.InsertImageStringItem(sys.maxint, str(i),-1)
            self.lstTrans.SetStringItem(index,1,trans['name'], -1)
            self.lstTrans.SetStringItem(index,2,trans['operation'],-1)
            self.lstTrans.SetStringItem(index,3,trans['val'],-1)
            self.lstTrans.SetStringItem(index,4,trans['combine'],-1)
            self.lstTrans.SetStringItem(index,5,trans['level'],-1)
            self.lstTrans.SetItemData(index,i)
            i+=1
    def __updateFollowing__(self):
        self.selFollow=-1
        self.lstFollow.DeleteAllItems()
        i=0
        for s in self.voglS88state.nextState:
            index = self.lstFollow.InsertImageStringItem(sys.maxint, "%d"%i, -1)
            self.lstFollow.SetStringItem(index,1,s,-1)
            self.lstFollow.SetItemData(index,i)
            i+=1
            pass
        self.__updateTrans__()
    def OnGcbbOkButton(self, event):
        #self.voglSFCstep.nr=self.txtNr.GetValue()
        #self.voglSFCstep.name=self.txtStepName.GetValue()
        self.OnGcbbSetButton(None)
        self.EndModal(1)
        event.Skip()
    def OnGcbbAddButton(self, event):
        #print self.spnLevel.GetValue(),type(self.spnLevel.GetValue())
        if self.selFollow<0:
            return
        trans={'name':self.txtName.GetValue(),
            'fid_name':self.IDname,
            'operation':self.chcOperator.GetStringSelection(),
            'val':self.txtVal.GetValue(),
            'fid_val':self.IDval,
            'combine':self.chcCombine.GetStringSelection(),
            'level':str(self.spnLevel.GetValue())}
        i=len(self.voglS88state.transitions[self.voglS88state.nextState[self.selFollow]])
        index = self.lstTrans.InsertImageStringItem(sys.maxint, str(i), -1)
        self.lstTrans.SetStringItem(index,1,trans['name'],-1)
        self.lstTrans.SetStringItem(index,2,trans['operation'],-1)
        self.lstTrans.SetStringItem(index,3,trans['val'],-1)
        self.lstTrans.SetStringItem(index,4,trans['combine'],-1)
        self.lstTrans.SetStringItem(index,5,trans['level'],-1)
        transLst=self.voglS88state.transitions[self.voglS88state.nextState[self.selFollow]]
        transLst.append(trans)
        event.Skip()

    def OnLstFollowListItemSelected(self, event):
        self.selFollow=event.GetIndex()
        self.__updateTrans__()
        self.__setTransEnable__(True)
        event.Skip()
    def OnGcbbDelButton(self, event):
        if self.selTrans<0:
            return
        self.lstTrans.DeleteItem(self.selTrans)
        transLst=self.voglS88state.transitions[self.voglS88state.nextState[self.selFollow]]
        transLst=transLst[:self.selTrans]+transLst[self.selTrans+1:]
        self.voglS88state.transitions[self.voglS88state.nextState[self.selFollow]]=transLst
        self.selTrans=-1
        #i=self.lstFollow.GetItemData(self.selFollow)
        #self.voglSFCstep.nextSteps=self.voglSFCstep.nextSteps[:i]+self.voglSFCstep.nextSteps[i+1:]
        #self.__updateFollowing__()
        event.Skip()
    
    def __replace__(self,strOld,strNew):
        i=string.find(strOld,'<')
        if i>=0:
            j=string.find(strOld,'>',i+1)
            if j>0:
                return strOld[:i+1]+strNew+strOld[j:]
        return '<'+strNew+'>'
    def OnGcbbBrowseButton(self, event):
        if self.dlgBrowse is not None:
            self.dlgBrowse.Centre()
            if self.dlgBrowse.ShowModal()>0:
                s=self.dlgBrowse.GetTagName()
                self.IDname=self.dlgBrowse.GetId()
                sOld=self.txtName.GetValue()
                s=self.__replace__(sOld,s)
                self.txtName.SetValue(s)
        event.Skip()

    def OnGcbbCreateButton(self, event):
        self.EndModal(2)
        event.Skip()

    def OnChcFollowTypeCheckbox(self, event):
        event.Skip()

    def OnLstTransListItemSelected(self, event):
        self.selTrans=event.GetIndex()
        if self.selTrans<0:
            return
        self.__setTransEnable__(True)
        idx=event.GetIndex()
        it=self.lstTrans.GetItem(idx,1)
        self.txtName.SetValue(it.m_text)
        
        it=self.lstTrans.GetItem(idx,2)
        self.chcOperator.SetStringSelection(it.m_text)
        
        it=self.lstTrans.GetItem(idx,3)
        self.txtVal.SetValue(it.m_text)
        
        it=self.lstTrans.GetItem(idx,4)
        self.chcCombine.SetStringSelection(it.m_text)
        
        try:
            it=self.lstTrans.GetItem(idx,5)
            self.spnLevel.SetValue(int(it.m_text))
        except:
            self.spnLevel.SetValue(0)
        
        #lst=self.voglSFCstep.nextSteps[self.selFollow][2]
        #trans=lst[self.selTrans]
        #try:
        #    self.IDname=trans['fid_name']
        #except:
        #    self.IDname=''
        #try:
        #    self.IDval=trans['fid_val']
        #except:
        #    self.IDval=''
        event.Skip()

    def OnGcbbSetButton(self, event):
        if self.selTrans<0:
            return
        trans={'name':self.txtName.GetValue(),
            'fid_name':self.IDname,
            'operation':self.chcOperator.GetStringSelection(),
            'val':self.txtVal.GetValue(),
            'fid_val':self.IDval,
            'combine':self.chcCombine.GetStringSelection(),
            'level':str(self.spnLevel.GetValue())}
        index = self.selTrans
        self.lstTrans.SetStringItem(index,1,trans['name'],-1)
        self.lstTrans.SetStringItem(index,2,trans['operation'],-1)
        self.lstTrans.SetStringItem(index,3,trans['val'],-1)
        self.lstTrans.SetStringItem(index,4,trans['combine'],-1)
        self.lstTrans.SetStringItem(index,5,trans['level'],-1)
        
        transLst=self.voglS88state.transitions[self.voglS88state.nextState[self.selFollow]]
        transLst=transLst[:self.selTrans]+[trans]+transLst[self.selTrans+1:]
        self.voglS88state.transitions[self.voglS88state.nextState[self.selFollow]]=transLst
        
        if event is not None:
            event.Skip()

    def OnGcbbBrowseValButton(self, event):
        if self.dlgBrowseVal is not None:
            self.dlgBrowseVal.Centre()
            if self.dlgBrowseVal.ShowModal()>0:
                s=self.dlgBrowseVal.GetTagName()
                self.IDval=self.dlgBrowseVal.GetId()
                s=self.__replace__(self.txtVal.GetValue(),s)
                self.txtVal.SetValue(s)
        event.Skip()

    def OnGcbbEditButton(self, event):
        if self.selFollow<0:
            self.voglS88.EditSFCspec(self.voglS88state.name)
            return
        self.voglS88.EditSFCspec(self.voglS88state.nextState[self.selFollow])
        
        #self.__updateTrans__()
        event.Skip()
    def OnChcUsedCheckbox(self, event):
        self.voglS88state.SetType(self.voglS88state.type,self.chcUsed.GetValue())
        self.__updateUsed__()
        event.Skip()


#----------------------------------------------------------------------

class voglS88state(ogl.EllipseShape):
    def __init__(self, canvas,name,w=0.0, h=0.0):
        ogl.EllipseShape.__init__(self, w, h)
        self.AddText(name)
        self.name=name
        self.canvas=canvas
        self.nextState=[]
        if name=='complete':
            self.nextState=['idle']
        elif name=='idle':
            self.nextState=['running']
        elif name=='running':
            self.nextState=['aborting','complete','holding','stopping','pausing']
        elif name=='aborting':
            self.nextState=['aborted']
        elif name=='aborted':
            self.nextState=['idle']
        elif name=='stopping':
            self.nextState=['stopped']
        elif name=='stopped':
            self.nextState=['idle']
        elif name=='pausing':
            self.nextState=['paused']
        elif name=='paused':
            self.nextState=['running']
        elif name=='holding':
            self.nextState=['held']
        elif name=='held':
            self.nextState=['restarting']
        elif name=='restarting':
            self.nextState=['running']
        elif name=='hazard':
            self.nextState=['aborting','aborted','holding','held',
                    'pausing','paused','stopping','stopped']
        self.ClearTransitions()
    def ClearTransitions(self):
        self.transitions={}
        for n in self.nextState:
            self.transitions[n]=[]
    def SetType(self,type,used=True):
        """ possible type occording S88 are
            type = 0 transient state
                 = 1 quiescent state
                 = 2 final state
                 = 3 exception state
        """
        if used==True:
            if type==0:
                br=wx.Brush(wx.Colour(0xDF,0xDF,0xDF), wx.SOLID)
            elif type==1:
                br=wx.Brush(wx.Colour(0xAF,0xAF,0xAF), wx.SOLID)
            elif type==2:
                br=wx.Brush(wx.Colour(0x8F,0x8F,0x8F), wx.SOLID)
            elif type==3:
                br=wx.Brush(wx.Colour(0xBF,0x00,0x00), wx.SOLID)
            else:
                br=wx.Brush(wx.Colour(0xBF,0x00,0x00), wx.VERTICAL_HATCH)
        else:
            br=wx.Brush(wx.Colour(0x4F,0x4F,0x4F), wx.SOLID)
        self.type=type
        self.used=used
        self.SetBrush(br)
    def SetUsed(self,used):
        self.SetType(self.type,used)
    def DoEdit(self):
        if self.canvas.dlgS88state is None:
            self.canvas.dlgS88state=vgdS88state(self.canvas)
        self.canvas.dlgS88state.Centre()
        self.canvas.dlgS88state.SetInfos(self,self.canvas)
        self.canvas.dlgS88state.SetBrowseDlg(self.canvas.GetBrowseDlg())
        self.canvas.dlgS88state.SetBrowseValDlg(self.canvas.GetBrowseValDlg())
        if self.canvas.dlgS88state.ShowModal()>0:
            return 1
        return 0
    
