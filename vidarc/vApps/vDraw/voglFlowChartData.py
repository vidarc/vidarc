#Boa:Dialog:vgdFlowChartData

import wx
import wx.lib.buttons
import wx.lib.ogl as ogl
import string,os,sys
import vidarc.vApps.vDraw.latex as latex
from vidarc.vApps.vDraw.voglFlowChartElement import voglFlowChartElement
import images

def create(parent):
    return vgdFlowChartData(parent)

[wxID_VGDFLOWCHARTDATA, wxID_VGDFLOWCHARTDATACHCUSED, 
 wxID_VGDFLOWCHARTDATAGCBBADD, wxID_VGDFLOWCHARTDATAGCBBBROWSE, 
 wxID_VGDFLOWCHARTDATAGCBBDEL, wxID_VGDFLOWCHARTDATAGCBBEDIT, 
 wxID_VGDFLOWCHARTDATAGCBBOK, wxID_VGDFLOWCHARTDATAGCBBSET, 
 wxID_VGDFLOWCHARTDATALBLSTEPNAME, wxID_VGDFLOWCHARTDATALSTFOLLOW, 
 wxID_VGDFLOWCHARTDATALSTTRANS, wxID_VGDFLOWCHARTDATATXTSTATENAME, 
] = [wx.NewId() for _init_ctrls in range(12)]

class vgdFlowChartData(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VGDFLOWCHARTDATA,
              name=u'vgdFlowChartData', parent=prnt, pos=wx.Point(320, 89),
              size=wx.Size(480, 429), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'Flow Chart Data')
        self.SetClientSize(wx.Size(472, 402))

        self.txtStateName = wx.TextCtrl(id=wxID_VGDFLOWCHARTDATATXTSTATENAME,
              name=u'txtStateName', parent=self, pos=wx.Point(48, 4),
              size=wx.Size(100, 21), style=0, value=u'')

        self.lblStepName = wx.StaticText(id=wxID_VGDFLOWCHARTDATALBLSTEPNAME,
              label=u'Name', name=u'lblStepName', parent=self, pos=wx.Point(8,
              8), size=wx.Size(28, 13), style=0)

        self.lstFollow = wx.ListView(id=wxID_VGDFLOWCHARTDATALSTFOLLOW,
              name=u'lstFollow', parent=self, pos=wx.Point(8, 32),
              size=wx.Size(336, 96), style=wx.LC_REPORT)
        self.lstFollow.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstFollowListItemSelected,
              id=wxID_VGDFLOWCHARTDATALSTFOLLOW)

        self.gcbbEdit = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTDATAGCBBEDIT,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Edit', name=u'gcbbEdit',
              parent=self, pos=wx.Point(384, 40), size=wx.Size(76, 30),
              style=0)
        self.gcbbEdit.Bind(wx.EVT_BUTTON, self.OnGcbbEditButton,
              id=wxID_VGDFLOWCHARTDATAGCBBEDIT)

        self.lstTrans = wx.ListView(id=wxID_VGDFLOWCHARTDATALSTTRANS,
              name=u'lstTrans', parent=self, pos=wx.Point(8, 144),
              size=wx.Size(368, 84), style=wx.LC_REPORT | wx.LC_SORT_ASCENDING)
        self.lstTrans.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstTransListItemSelected,
              id=wxID_VGDFLOWCHARTDATALSTTRANS)

        self.gcbbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTDATAGCBBADD,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'gcbbAdd',
              parent=self, pos=wx.Point(384, 144), size=wx.Size(76, 30),
              style=0)
        self.gcbbAdd.Bind(wx.EVT_BUTTON, self.OnGcbbAddButton,
              id=wxID_VGDFLOWCHARTDATAGCBBADD)

        self.gcbbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTDATAGCBBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Delete', name=u'gcbbDel',
              parent=self, pos=wx.Point(384, 182), size=wx.Size(76, 30),
              style=0)
        self.gcbbDel.Bind(wx.EVT_BUTTON, self.OnGcbbDelButton,
              id=wxID_VGDFLOWCHARTDATAGCBBDEL)

        self.gcbbBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTDATAGCBBBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbBrowse', parent=self, pos=wx.Point(384, 244),
              size=wx.Size(76, 30), style=0)
        self.gcbbBrowse.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseButton,
              id=wxID_VGDFLOWCHARTDATAGCBBBROWSE)

        self.gcbbSet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTDATAGCBBSET,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Set', name=u'gcbbSet',
              parent=self, pos=wx.Point(384, 324), size=wx.Size(76, 30),
              style=0)

        self.gcbbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTDATAGCBBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'gcbbOk',
              parent=self, pos=wx.Point(176, 368), size=wx.Size(76, 30),
              style=0)
        self.gcbbOk.Bind(wx.EVT_BUTTON, self.OnGcbbOkButton,
              id=wxID_VGDFLOWCHARTDATAGCBBOK)

        self.chcUsed = wx.CheckBox(id=wxID_VGDFLOWCHARTDATACHCUSED,
              label=u'Used', name=u'chcUsed', parent=self, pos=wx.Point(168, 8),
              size=wx.Size(73, 13), style=0)
        self.chcUsed.SetValue(False)
        self.chcUsed.Bind(wx.EVT_CHECKBOX, self.OnChcUsedCheckbox,
              id=wxID_VGDFLOWCHARTDATACHCUSED)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        self.dlgBrowse=None
        self.dlgBrowseVal=None
        self.IDname=''
        self.IDval=''
        
        img=images.getOkBitmap()
        self.gcbbOk.SetBitmapLabel(img)
        self.gcbbSet.SetBitmapLabel(img)
        
        img=images.getDelBitmap()
        self.gcbbDel.SetBitmapLabel(img)
        img=images.getDrawBitmap()
        self.gcbbEdit.SetBitmapLabel(img)
        
        img=images.getAddBitmap()
        self.gcbbAdd.SetBitmapLabel(img)
        
        img=images.getBrowseBitmap()
        self.gcbbBrowse.SetBitmapLabel(img)
        self.gcbbBrowseVal.SetBitmapLabel(img)
        
        self.lstFollow.InsertColumn(0,u'Nr',wx.LIST_FORMAT_LEFT,30)
        self.lstFollow.InsertColumn(1,u'Name',wx.LIST_FORMAT_LEFT,200)
        
        self.lstTrans.InsertColumn(0,u'Nr',wx.LIST_FORMAT_LEFT,30)
        self.lstTrans.InsertColumn(1,u'Name',wx.LIST_FORMAT_LEFT,150)
        self.lstTrans.InsertColumn(2,u'Oper',wx.LIST_FORMAT_CENTER,50)
        self.lstTrans.InsertColumn(3,u'Value',wx.LIST_FORMAT_LEFT,70)
        self.lstTrans.InsertColumn(4,u'Comb',wx.LIST_FORMAT_LEFT,30)
        self.lstTrans.InsertColumn(5,u'Level',wx.LIST_FORMAT_LEFT,30)
        self.__setTransEnable__(False)
    def __setTransEnable__(self,state):
        self.txtName.Enable(state)
        self.chcOperator.Enable(state)
        self.gcbbBrowse.Enable(state)
        self.txtVal.Enable(state)
        self.gcbbBrowseVal.Enable(state)
        self.chcCombine.Enable(state)
        self.spnLevel.Enable(state)
        self.gcbbSet.Enable(state)
    def __clear__(self):
        self.txtName.SetValue('')
        self.chcOperator.SetSelection(0)
        self.txtVal.SetValue('')    
    def SetNrs(self,nrs):
        self.nrs=nrs
    def SetInfos(self,voglS88state,voglDrawS88):
        #self.spnStepNr.SetValue(voglSFCstep.nr)
        self.voglS88=voglDrawS88
        self.voglS88state=voglS88state
        self.txtStateName.SetValue(self.voglS88state.name)
        if self.voglS88state.name in ['idle','running','complete']:
            self.chcUsed.SetValue(True)
            self.chcUsed.Enable(False)
        else:
            self.chcUsed.SetValue(self.voglS88state.used)
            self.chcUsed.Enable(True)
        self.__updateUsed__()
        self.__updateFollowing__()
    def __updateUsed__(self):
        if self.chcUsed.IsChecked():
            self.lstFollow.Enable(True)
            self.lstTrans.Enable(True)
        else:
            self.lstFollow.Enable(False)
            self.lstTrans.Enable(False)
            self.__setTransEnable__(False)
    def SetBrowseDlg(self,dlg):
        self.dlgBrowse=dlg
    def SetBrowseValDlg(self,dlg):
        self.dlgBrowseVal=dlg
    def __updateTrans__(self):
        self.selTrans=-1
        self.IDname=''
        self.IDval=''
        self.lstTrans.DeleteAllItems()
        self.__setTransEnable__(False)
        self.__clear__()
        if self.selFollow<0:
            return
        #self.txtName.SetValue('')
        #self.txtVal.SetValue('')
        # build steps
        self.selTrans=-1
        self.lstTrans.DeleteAllItems()
        i=0
        for trans in self.voglS88state.transitions[self.voglS88state.nextState[self.selFollow]]:
            index = self.lstTrans.InsertImageStringItem(sys.maxint, str(i),-1)
            self.lstTrans.SetStringItem(index,1,trans['name'], -1)
            self.lstTrans.SetStringItem(index,2,trans['operation'],-1)
            self.lstTrans.SetStringItem(index,3,trans['val'],-1)
            self.lstTrans.SetStringItem(index,4,trans['combine'],-1)
            self.lstTrans.SetStringItem(index,5,trans['level'],-1)
            self.lstTrans.SetItemData(index,i)
            i+=1
    def __updateFollowing__(self):
        self.selFollow=-1
        self.lstFollow.DeleteAllItems()
        i=0
        for s in self.voglS88state.nextState:
            index = self.lstFollow.InsertImageStringItem(sys.maxint, "%d"%i, -1)
            self.lstFollow.SetStringItem(index,1,s,-1)
            self.lstFollow.SetItemData(index,i)
            i+=1
            pass
        self.__updateTrans__()
    def OnGcbbOkButton(self, event):
        #self.voglSFCstep.nr=self.txtNr.GetValue()
        #self.voglSFCstep.name=self.txtStepName.GetValue()
        self.OnGcbbSetButton(None)
        self.EndModal(1)
        event.Skip()
    def OnGcbbAddButton(self, event):
        #print self.spnLevel.GetValue(),type(self.spnLevel.GetValue())
        if self.selFollow<0:
            return
        trans={'name':self.txtName.GetValue(),
            'fid_name':self.IDname,
            'operation':self.chcOperator.GetStringSelection(),
            'val':self.txtVal.GetValue(),
            'fid_val':self.IDval,
            'combine':self.chcCombine.GetStringSelection(),
            'level':str(self.spnLevel.GetValue())}
        i=len(self.voglS88state.transitions[self.voglS88state.nextState[self.selFollow]])
        index = self.lstTrans.InsertImageStringItem(sys.maxint, str(i), -1)
        self.lstTrans.SetStringItem(index,1,trans['name'],-1)
        self.lstTrans.SetStringItem(index,2,trans['operation'],-1)
        self.lstTrans.SetStringItem(index,3,trans['val'],-1)
        self.lstTrans.SetStringItem(index,4,trans['combine'],-1)
        self.lstTrans.SetStringItem(index,5,trans['level'],-1)
        transLst=self.voglS88state.transitions[self.voglS88state.nextState[self.selFollow]]
        transLst.append(trans)
        event.Skip()

    def OnLstFollowListItemSelected(self, event):
        self.selFollow=event.GetIndex()
        self.__updateTrans__()
        self.__setTransEnable__(True)
        event.Skip()
    def OnGcbbDelButton(self, event):
        if self.selTrans<0:
            return
        self.lstTrans.DeleteItem(self.selTrans)
        transLst=self.voglS88state.transitions[self.voglS88state.nextState[self.selFollow]]
        transLst=transLst[:self.selTrans]+transLst[self.selTrans+1:]
        self.voglS88state.transitions[self.voglS88state.nextState[self.selFollow]]=transLst
        self.selTrans=-1
        #i=self.lstFollow.GetItemData(self.selFollow)
        #self.voglSFCstep.nextSteps=self.voglSFCstep.nextSteps[:i]+self.voglSFCstep.nextSteps[i+1:]
        #self.__updateFollowing__()
        event.Skip()
    
    def __replace__(self,strOld,strNew):
        i=string.find(strOld,'<')
        if i>=0:
            j=string.find(strOld,'>',i+1)
            if j>0:
                return strOld[:i+1]+strNew+strOld[j:]
        return '<'+strNew+'>'
    def OnGcbbBrowseButton(self, event):
        if self.dlgBrowse is not None:
            self.dlgBrowse.Centre()
            if self.dlgBrowse.ShowModal()>0:
                s=self.dlgBrowse.GetTagName()
                self.IDname=self.dlgBrowse.GetId()
                sOld=self.txtName.GetValue()
                s=self.__replace__(sOld,s)
                self.txtName.SetValue(s)
        event.Skip()

    def OnGcbbCreateButton(self, event):
        self.EndModal(2)
        event.Skip()

    def OnChcFollowTypeCheckbox(self, event):
        event.Skip()

    def OnLstTransListItemSelected(self, event):
        self.selTrans=event.GetIndex()
        if self.selTrans<0:
            return
        self.__setTransEnable__(True)
        idx=event.GetIndex()
        it=self.lstTrans.GetItem(idx,1)
        self.txtName.SetValue(it.m_text)
        
        it=self.lstTrans.GetItem(idx,2)
        self.chcOperator.SetStringSelection(it.m_text)
        
        it=self.lstTrans.GetItem(idx,3)
        self.txtVal.SetValue(it.m_text)
        
        it=self.lstTrans.GetItem(idx,4)
        self.chcCombine.SetStringSelection(it.m_text)
        
        try:
            it=self.lstTrans.GetItem(idx,5)
            self.spnLevel.SetValue(int(it.m_text))
        except:
            self.spnLevel.SetValue(0)
        
        #lst=self.voglSFCstep.nextSteps[self.selFollow][2]
        #trans=lst[self.selTrans]
        #try:
        #    self.IDname=trans['fid_name']
        #except:
        #    self.IDname=''
        #try:
        #    self.IDval=trans['fid_val']
        #except:
        #    self.IDval=''
        event.Skip()

    def OnGcbbSetButton(self, event):
        if self.selTrans<0:
            return
        trans={'name':self.txtName.GetValue(),
            'fid_name':self.IDname,
            'operation':self.chcOperator.GetStringSelection(),
            'val':self.txtVal.GetValue(),
            'fid_val':self.IDval,
            'combine':self.chcCombine.GetStringSelection(),
            'level':str(self.spnLevel.GetValue())}
        index = self.selTrans
        self.lstTrans.SetStringItem(index,1,trans['name'],-1)
        self.lstTrans.SetStringItem(index,2,trans['operation'],-1)
        self.lstTrans.SetStringItem(index,3,trans['val'],-1)
        self.lstTrans.SetStringItem(index,4,trans['combine'],-1)
        self.lstTrans.SetStringItem(index,5,trans['level'],-1)
        
        transLst=self.voglS88state.transitions[self.voglS88state.nextState[self.selFollow]]
        transLst=transLst[:self.selTrans]+[trans]+transLst[self.selTrans+1:]
        self.voglS88state.transitions[self.voglS88state.nextState[self.selFollow]]=transLst
        
        if event is not None:
            event.Skip()

    def OnGcbbBrowseValButton(self, event):
        if self.dlgBrowseVal is not None:
            self.dlgBrowseVal.Centre()
            if self.dlgBrowseVal.ShowModal()>0:
                s=self.dlgBrowseVal.GetTagName()
                self.IDval=self.dlgBrowseVal.GetId()
                s=self.__replace__(self.txtVal.GetValue(),s)
                self.txtVal.SetValue(s)
        event.Skip()

    def OnGcbbEditButton(self, event):
        if self.selFollow<0:
            self.voglS88.EditSFCspec(self.voglS88state.name)
            return
        self.voglS88.EditSFCspec(self.voglS88state.nextState[self.selFollow])
        
        #self.__updateTrans__()
        event.Skip()
    def OnChcUsedCheckbox(self, event):
        self.voglS88state.SetType(self.voglS88state.type,self.chcUsed.GetValue())
        self.__updateUsed__()
        event.Skip()


#----------------------------------------------------------------------

class voglFlowChartData(voglFlowChartElement):
    def __init__(self, canvas,id,desc,info,w=0.0, h=0.0):
        voglFlowChartElement.__init__(self, canvas, id,desc,info, w, h)
        
        obj=ogl.PolygonShape()
        obj.SetBrush(wx.TRANSPARENT_BRUSH)
        dw=w/6.0
        dh=h/4.0
        points = [ (-dw*3.0, +dh*2.0),
                   (-dw*2.0, -dh*2.0),
                   (+dw*3.0, -dh*2.0),
                   (+dw*2.0, +dh*2.0)
                   ]

        obj.Create(points)
        obj.SetDraggable(False)
        obj.SetSensitivityFilter(0,True)
        self.AddChild(obj)
        
        self.__addSymbol__()
        self.SetSensitivityFilter(0)
        self.Recompute()
        self.SetDraggable(True)
    def getXmlType(self):
        return 'data'
    def appendLatexPic(self,strs,yMax,scaleX,scaleY):
        x=self.symbol.GetX()/scaleX
        y=self.symbol.GetY()/scaleY
        ba=self.symbol.GetBoundingBoxMax()
        xa=x-(ba[0]/2.0/scaleX)
        ya=y-(ba[1]/2.0/scaleY)
        #tup=(xa,yMax-(y+(ba[1]/2.0/scaleY)),ba[0]/scaleX,ba[1]/scaleY,self.info)
        #strs.append('    \\put(%4.1f,%4.1f){\\makebox(%4.1f,%4.1f){{\\footnotesize %s}}}'%tup)
        xh1,yh1=xa,yMax-(y+(ba[1]/2.0/scaleY))
        xh2,yh2=xa+(self.w/6.0/scaleX),yMax-(y-(ba[1]/2.0/scaleY))
        
        tup=latex.lineFit(xh1,yh1,xh2,yh2)
        strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
        tup=latex.lineFit(xh1,yh1,xh1+(self.w/scaleX),yh1)
        strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
        tup=latex.lineFit(xh2,yh2,xh2+(self.w/scaleX),yh2)
        strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
        tup=latex.lineFit(xh1+(self.w/scaleX),yh1,xh2+(self.w/scaleX),yh2)
        strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
        voglFlowChartElement.appendLatexPic(self,strs,yMax,scaleX,scaleY)
        return xa,ya
            
    def isData(self):
        return True
    def isProg(self):
        return True
    def isSys(self):
        return True
    def isNet(self):
        return True
    def isRes(self):
        return True
