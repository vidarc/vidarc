#Boa:Dialog:vgdSFCstep
#----------------------------------------------------------------------------
# Name:         voglSFCstepTransition.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: voglSFCstepTransition.py,v 1.1 2005/12/13 13:19:34 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import wx.lib.ogl as ogl

import sys,string
import images

def create(parent):
    return vgdSFCstep(parent)

[wxID_VGDSFCSTEP, wxID_VGDSFCSTEPCHCFOLLOWTYPE, wxID_VGDSFCSTEPCHCSTEPNR, 
 wxID_VGDSFCSTEPGCBBADD, wxID_VGDSFCSTEPGCBBADDACTION, 
 wxID_VGDSFCSTEPGCBBBROWSE, wxID_VGDSFCSTEPGCBBCANCEL, wxID_VGDSFCSTEPGCBBDEL, 
 wxID_VGDSFCSTEPGCBBDELACTION, wxID_VGDSFCSTEPGCBBOK, 
 wxID_VGDSFCSTEPLBLSTEPNR, wxID_VGDSFCSTEPLSTACTION, wxID_VGDSFCSTEPLSTFOLLOW, 
 wxID_VGDSFCSTEPSPNSTEPNR, wxID_VGDSFCSTEPTXTACTION, 
] = [wx.NewId() for _init_ctrls in range(15)]

class vgdSFCstep(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VGDSFCSTEP, name=u'vgdSFCstep',
              parent=prnt, pos=wx.Point(386, 152), size=wx.Size(477, 300),
              style=wx.DEFAULT_DIALOG_STYLE, title=u'SFC Step')
        self.SetClientSize(wx.Size(469, 273))

        self.gcbbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPGCBBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'gcbbOk',
              parent=self, pos=wx.Point(80, 232), size=wx.Size(76, 30),
              style=0)
        self.gcbbOk.Bind(wx.EVT_BUTTON, self.OnGcbbOkButton,
              id=wxID_VGDSFCSTEPGCBBOK)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(176, 232),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VGDSFCSTEPGCBBCANCEL)

        self.lblStepNr = wx.StaticText(id=wxID_VGDSFCSTEPLBLSTEPNR, label=u'Nr',
              name=u'lblStepNr', parent=self, pos=wx.Point(4, 6),
              size=wx.Size(11, 13), style=0)

        self.chcStepNr = wx.Choice(choices=[], id=wxID_VGDSFCSTEPCHCSTEPNR,
              name=u'chcStepNr', parent=self, pos=wx.Point(32, 44),
              size=wx.Size(60, 21), style=0)

        self.chcFollowType = wx.CheckBox(id=wxID_VGDSFCSTEPCHCFOLLOWTYPE,
              label=u'parallel', name=u'chcFollowType', parent=self,
              pos=wx.Point(104, 44), size=wx.Size(53, 13), style=0)
        self.chcFollowType.SetValue(False)
        self.chcFollowType.Bind(wx.EVT_CHECKBOX, self.OnChcFollowTypeCheckbox,
              id=wxID_VGDSFCSTEPCHCFOLLOWTYPE)

        self.spnStepNr = wx.SpinCtrl(id=wxID_VGDSFCSTEPSPNSTEPNR, initial=0,
              max=99, min=0, name=u'spnStepNr', parent=self, pos=wx.Point(32,
              4), size=wx.Size(117, 21), style=wx.SP_ARROW_KEYS)
        self.spnStepNr.Bind(wx.EVT_SPINCTRL, self.OnSpnStepNrSpinctrl,
              id=wxID_VGDSFCSTEPSPNSTEPNR)
        self.spnStepNr.Bind(wx.EVT_TEXT, self.OnSpnStepNrText,
              id=wxID_VGDSFCSTEPSPNSTEPNR)

        self.lstFollow = wx.ListView(id=wxID_VGDSFCSTEPLSTFOLLOW,
              name=u'lstFollow', parent=self, pos=wx.Point(24, 96),
              size=wx.Size(136, 100), style=wx.LC_REPORT)
        self.lstFollow.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstFollowListItemSelected, id=wxID_VGDSFCSTEPLSTFOLLOW)

        self.gcbbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPGCBBADD,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'gcbbAdd', parent=self,
              pos=wx.Point(168, 96), size=wx.Size(76, 30), style=0)
        self.gcbbAdd.Bind(wx.EVT_BUTTON, self.OnGcbbAddButton,
              id=wxID_VGDSFCSTEPGCBBADD)

        self.gcbbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPGCBBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Delete', name=u'gcbbDel',
              parent=self, pos=wx.Point(168, 136), size=wx.Size(76, 30),
              style=0)
        self.gcbbDel.Bind(wx.EVT_BUTTON, self.OnGcbbDelButton,
              id=wxID_VGDSFCSTEPGCBBDEL)

        self.txtAction = wx.TextCtrl(id=wxID_VGDSFCSTEPTXTACTION,
              name=u'txtAction', parent=self, pos=wx.Point(264, 24),
              size=wx.Size(100, 21), style=0, value=u'')

        self.lstAction = wx.ListView(id=wxID_VGDSFCSTEPLSTACTION,
              name=u'lstAction', parent=self, pos=wx.Point(256, 96),
              size=wx.Size(200, 100), style=wx.LC_REPORT)
        self.lstAction.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstActionListItemSelected, id=wxID_VGDSFCSTEPLSTACTION)

        self.gcbbAddAction = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPGCBBADDACTION,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Add', name=u'gcbbAddAction',
              parent=self, pos=wx.Point(376, 16), size=wx.Size(76, 30),
              style=0)
        self.gcbbAddAction.Bind(wx.EVT_BUTTON, self.OnGcbbAddActionButton,
              id=wxID_VGDSFCSTEPGCBBADDACTION)

        self.gcbbDelAction = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPGCBBDELACTION,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Del', name=u'gcbbDelAction',
              parent=self, pos=wx.Point(376, 56), size=wx.Size(76, 30),
              style=0)
        self.gcbbDelAction.Bind(wx.EVT_BUTTON, self.OnGcbbDelActionButton,
              id=wxID_VGDSFCSTEPGCBBDELACTION)

        self.gcbbBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDSFCSTEPGCBBBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse', name=u'gcbbBrowse',
              parent=self, pos=wx.Point(288, 56), size=wx.Size(76, 30),
              style=0)
        self.gcbbBrowse.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseButton,
              id=wxID_VGDSFCSTEPGCBBBROWSE)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        img=images.getOkBitmap()
        self.gcbbOk.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
        
        self.lstFollow.InsertColumn(0,u'Nr',wx.LIST_FORMAT_LEFT,30)
        self.lstFollow.InsertColumn(1,u'Name',wx.LIST_FORMAT_LEFT,200)
        
    def SetNrs(self,nrs):
        self.nrs=nrs
    def SetInfos(self,voglSFCstep,voglDrawSFC):
        #self.spnStepNr.SetValue(voglSFCstep.nr)
        self.voglDrawSFC=voglDrawSFC
        self.voglSFCstep=voglSFCstep
        # build steps
        self.lstFollow.DeleteAllItems()
        for s in voglDrawSFC.GetSteps():
            if s==self.voglSFCstep:
                continue
            index = self.lstFollow.InsertImageStringItem(sys.maxint, s.nr, -1)
            self.lstFollow.SetStringItem(index,1,s.GetName(),-1)
            
            pass
    def OnGcbbOkButton(self, event):
        self.OnGcbbSetButton(None)
        self.EndModal(1)
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
    def GetCount(self):
        return self.spnCount.GetValue()
    def SetCount(self,count):
        self.spnCount.SetValue(count)

    def OnChcFollowTypeCheckbox(self, event):
        event.Skip()

    def OnSpnStepNrSpinctrl(self, event):
        event.Skip()

    def OnSpnStepNrText(self, event):
        event.Skip()

    def OnLstFollowListItemSelected(self, event):
        event.Skip()

    def OnGcbbAddButton(self, event):
        event.Skip()

    def OnGcbbDelButton(self, event):
        event.Skip()

    def OnLstActionListItemSelected(self, event):
        event.Skip()

    def OnGcbbAddActionButton(self, event):
        event.Skip()

    def OnGcbbDelActionButton(self, event):
        event.Skip()

    def OnGcbbBrowseButton(self, event):
        event.Skip()
class voglSFCstepTransition(ogl.DividedShape):
    def __init__(self, actions, canvas):
        w=0.0
        h=0.0
        self.canvas=canvas
        self.actions=actions
        lstHeights=[]
        for a in actions:
            tmpHeight=h
            strs=string.split(a,'\n')
            for s in strs:
                l=len(s)
                if l>w:
                    w=l
                h+=20.0
            h+=10.0
            lstHeights.append(h-tmpHeight)
        ogl.DividedShape.__init__(self, w*10, h)
        
        for i in range(0,len(actions)):
            print i,actions[i],lstHeights[i]/h
            region1 = ogl.ShapeRegion()
            region1.SetText(actions[i])
            region1.SetProportions(0.0, lstHeights[i]/h)
            region1.SetFormatMode(ogl.FORMAT_CENTRE_HORIZ|ogl.FORMAT_CENTRE_VERT)
            self.AddRegion(region1)
            #region1.SetSensitivityFilter(0)
            print region1
        
        self.SetRegionSizes()
        #print type(self),isinstance(self,ogl.DividedShape)
        self.ReformatRegions(canvas)
        #self.SetRegionSizes()
    def GetName(self):
        return self.name
    def GetFirstRegion(self):
        try:
            return self.GetRegions()[0]
        except:
            return None
    def ReformatRegions(self, canvas=None):
        rnum = 0

        if canvas is None:
            canvas = self.GetCanvas()

        dc = wx.ClientDC(canvas)  # used for measuring

        for region in self.GetRegions():
            text = region.GetText()
            self.FormatText(dc, text, rnum)
            rnum += 1
            
    def OnSizingEndDragLeft(self, pt, x, y, keys, attch):
        print "***", self
        ogl.DividedShape.OnSizingEndDragLeft(self, pt, x, y, keys, attch)
        self.SetRegionSizes()
        self.ReformatRegions()
        self.GetCanvas().Refresh()
    def OnRightClick(self, x, y, keys = 0, attachment = 0):
        #self.EditRegions()
        print "right clicked"
        if self.canvas.dlgSFCstepActions is None:
            self.canvas.dlgSFCstepActions=vgdSFCstepActions(self.canvas)
        pass
class voglSFCstep(ogl.CompositeShape):
    def __init__(self, nr,id,name,actions,width, height, canvas):
        ogl.CompositeShape.__init__(self)
        self.nr=nr
        self.id=id
        self.name=name
        self.dlg=None
        self.parent=canvas
        self.SetCanvas(canvas)
        self.prevStep=None
        self.nextSteps=[]
        #self.MakeContainer()
        
        # add a shape to the original division
        shape2 = ogl.RectangleShape(60, 40)
        shape2.AddText('Nr: %s'%self.nr)
        shape2.SetDraggable(False)
        shape2.SetSensitivityFilter(0,True)
        #self.GetDivisions()[0].AddChild(shape2)
        self.AddChild(shape2)
        
        # now divide the division so we get 2
        #self.GetDivisions()[0].Divide(wx.HORIZONTAL)
        #self.GetDivisions()[0].Divide(wx.VERTICAL)

        # and add a shape to the second division (and move it to the
        # centre of the division)
        shape3 = voglSFCstepActions(actions,canvas)
        b2=shape2.GetBoundingBoxMax()
        b3=shape3.GetBoundingBoxMax()
        #shape3.SetBrush(wx.CYAN_BRUSH)
        #self.GetDivisions()[1].AddChild(shape3)
        shape3.SetDraggable(False)
        shape3.SetSensitivityFilter(0,True)
        self.AddChild(shape3)
        #shape3.SetX(self.GetDivisions()[1].GetX())
        shape3.SetX(b2[0]+50)
        shape3.SetY(b3[1]/2.0-b2[1]/2.0)
        
        if shape3.GetFirstRegion() is not None:
            line = ogl.LineShape()
            line.SetCanvas(canvas)
            line.SetPen(wx.BLACK_PEN)
            line.SetBrush(wx.BLACK_BRUSH)
            #line.AddArrow(ogl.ARROW_ARROW)
            line.MakeLineControlPoints(2)
            #shape2.AddLine(line, shape3.GetFirstRegion())
            #self.diagram.AddShape(line)
            #canvas.GetDiagram.AddShape(line)
            print shape2.GetX(),shape2.GetY(),b2
            print shape3.GetX(),shape3.GetY(),b3
            line.SetEnds(b2[0]/2.0,0,shape3.GetX()-b3[0]/2.0+0.0,0)
            self.AddChild(line)
            line.Show(True)
        self.Recompute()
        for division in self.GetDivisions():
            division.SetSensitivityFilter(0,True)
            division.SetDraggable(False)
        #self.SetSensitivityFilter()
        if 1==0:
            region1 = ogl.ShapeRegion()
            region1.SetText('DividedShape')
            region1.SetProportions(0.0, 0.2)
            region1.SetFormatMode(ogl.FORMAT_CENTRE_HORIZ)
            self.AddRegion(region1)
    
            region2 = ogl.ShapeRegion()
            region2.SetText('This is Region number two.')
            region2.SetProportions(0.0, 0.3)
            region2.SetFormatMode(ogl.FORMAT_CENTRE_HORIZ|ogl.FORMAT_CENTRE_VERT)
            self.AddRegion(region2)
    
            region3 = ogl.ShapeRegion()
            region3.SetText('Region 3\nwith embedded\nline breaks')
            region3.SetProportions(0.0, 0.5)
            region3.SetFormatMode(ogl.FORMAT_NONE)
            self.AddRegion(region3)
    
            self.SetRegionSizes()
            self.ReformatRegions(canvas)
    
    def GetName(self):
        return self.name
    def ReformatRegions(self, canvas=None):
        rnum = 0

        if canvas is None:
            canvas = self.GetCanvas()

        dc = wx.ClientDC(canvas)  # used for measuring

        for region in self.GetRegions():
            text = region.GetText()
            self.FormatText(dc, text, rnum)
            rnum += 1


    def OnSizingEndDragLeft(self, pt, x, y, keys, attch):
        print "***", self
        ogl.DividedShape.OnSizingEndDragLeft(self, pt, x, y, keys, attch)
        self.SetRegionSizes()
        self.ReformatRegions()
        self.GetCanvas().Refresh()
    def OnRightClick(self, x, y, keys = 0, attachment = 0):
        print "right clicked"
        pass
    def IsSelectable(self):
        return 0
    def DoEdit(self,nrs):
        if self.dlg is None:
            self.dlg=vgdSFCstep(self.parent)
        self.dlg.Centre()
        self.dlg.SetInfos(self,self.parent)
        #dlg.SetCount(self.count)
        if self.dlg.ShowModal()>0:
            oldCount=0
            self.canvas.__setModified__(False)
            #self.count=self.dlg.GetCount()
            #if oldCount!=self.count:
            #    self.ClearText()
            #    self.signals.append(('ll',0,1,1))
            #    self.__draw__()
            #    return 1
        return 0
    
