#----------------------------------------------------------------------------
# Name:         latex.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: latex.py,v 1.2 2006/02/07 12:54:50 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import math
import string

dictK=None

def __setupLine__():
    global dictK
    
    if dictK is None:
        dictK={}
        for x in range(1,5):
            for y in range(0,5):
                if y>10:
                    if x>y:
                        if x%y==0:
                            continue
                    elif y>x:
                        if y%x==0:
                            continue
                k=float(y)/float(x)
                if dictK.has_key(k)==False:
                    dictK[k]=(x,y)
        #print dictK
def lineFit(xa,ya,xe,ye,verbose=0):
    global dictK
    __setupLine__()
    dx=xe-xa
    dy=ye-ya
    adx=math.fabs(dx)
    ady=math.fabs(dy)
    if adx>0:
        if ady>0:
            ak=float(ady)/float(adx)
            mk=0.0
            err=100.0
            mv=(0,0)
            for k in dictK.keys():
                if math.fabs(ak-k)<err:
                    mk=k
                    mv=dictK[k]
                    err=math.fabs(ak-mk)
            l=adx
        else:
            mk=0.0
            ak=0.0
            mv=(1,0)
            l=adx
    else:
        mk=0.0
        ak=0.0
        mv=(0,1)
        l=ady
    if verbose>1:
        print 'line',xa,ya,xe,ye
    if verbose>1:
        print '    ',dx,dy,adx,ady
        print 'min fault: k=%4.2f,mk=%4.2f,err=%4.2f,(%d,%d)'%(ak,mk,math.fabs(ak-mk),mv[0],mv[1])
    if dx<0:
        dx=-mv[0]
    else:
        dx=mv[0]
    if dy<0:
        dy=-mv[1]
    else:
        dy=mv[1]
    tup=(xa,ya,dx,dy,l)
    if verbose>0:
        print 'line %3d,%3d,%3d,%3d -> %3d,%3d,%3d,%3d,%4.2f'%(xa,ya,xe,ye,tup[0],tup[1],tup[2],tup[3],tup[4])
    return tup
def lineFitPict2(xa,ya,xe,ye,verbose=0):
    dx=xe-xa
    dy=ye-ya
    adx=math.fabs(dx)
    ady=math.fabs(dy)
    if adx>0:
        if ady>0:
            ak=float(ady)/float(adx)
            mk=0.0
            err=100.0
            adx=int(adx)
            if adx>1000:
                adx=1000
            ady=int(ady)
            if ady>1000:
                ady=1000
            mv=(adx,ady)
            l=adx
        else:
            mk=0.0
            ak=0.0
            mv=(1,0)
            l=adx
    else:
        mk=0.0
        ak=0.0
        mv=(0,1)
        l=ady
    if verbose>1:
        print 'line',xa,ya,xe,ye
    if verbose>1:
        print '    ',dx,dy,adx,ady
        print 'min fault: k=%4.2f,mk=%4.2f,err=%4.2f,(%d,%d)'%(ak,mk,math.fabs(ak-mk),mv[0],mv[1])
    if dx<0:
        dx=-mv[0]
    else:
        dx=mv[0]
    if dy<0:
        dy=-mv[1]
    else:
        dy=mv[1]
    tup=(xa,ya,dx,dy,l)
    return tup

def texReplace4Lbl(s):
    #return string.replace(s,'_','\\_')
    return string.replace(s,'_','')

def texReplace(s):
    s=string.replace(s,'#','\#')
    s=string.replace(s,'$','\$')
    s=string.replace(s,'%','\%')
    s=string.replace(s,'_','\_')
    s=string.replace(s,'.','.\-')
    return s

def tableMultiLine(str,format='l'):
    strs=map(texReplace,string.split(str,'\n'))
    if len(strs)>0:
        return '\\begin{tabular}{'+format+'}'+string.join(strs,' \\\\ ')+'\\end{tabular}'
    return str
    
def tableMultiLineTup(str,format='l'):
    strs=map(texReplace,string.split(str,'\n'))
    if len(strs)>0:
        return (1,'\\begin{tabular}{'+format+'}'+string.join(strs,' \\\\ ')+'\\end{tabular}')
    return (0,str)
    
if __name__=='__main__':
    print '----- test start ----'
    lineFit(0,0,10,0,verbose=1)
    lineFit(0,0,-10,0,verbose=1)
    lineFit(0,0,0,10,verbose=1)
    lineFit(0,0,0,-10,verbose=1)

    lineFit(0,0,10,10,verbose=1)
    lineFit(0,0,10,12,verbose=1)
    lineFit(0,0,10,13,verbose=1)
    lineFit(0,0,10,-13,verbose=1)
    lineFit(0,0,-10,13,verbose=1)
    lineFit(0,0,-10,-13,verbose=1)
    lineFit(0,0,10,15,verbose=1)

    lineFit(0,0,10,20,verbose=1)
    lineFit(0,0,10,30,verbose=1)
    lineFit(0,0,10,40,verbose=1)
    lineFit(0,0,10,50,verbose=1)
    lineFit(0,0,10,60,verbose=2)
    lineFit(0,0,10,70,verbose=2)
    
    lineFit(0,0,20,10,verbose=1)
    lineFit(0,0,30,10,verbose=1)
    lineFit(0,0,40,10,verbose=1)
    lineFit(0,0,50,10,verbose=1)
    print '----- test end   ----'
    