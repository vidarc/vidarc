#Boa:Dialog:vgdFlowChartConnector

import wx
import wx.lib.buttons
from wx import TRANSPARENT_BRUSH
from wx import BLACK_PEN
import wx.lib.ogl as ogl
import string,os,sys
import vidarc.vApps.vDraw.latex as latex
from vidarc.vApps.vDraw.voglFlowChartElement import voglFlowChartElement
import images


def create(parent):
    return vgdFlowChartConnector(parent)

[wxID_VGDFLOWCHARTCONNECTOR, wxID_VGDFLOWCHARTCONNECTORCHCMULTIPLE, 
 wxID_VGDFLOWCHARTCONNECTORCHCPARALLEL, wxID_VGDFLOWCHARTCONNECTORGCBBBROWSE, 
 wxID_VGDFLOWCHARTCONNECTORGCBBCANCEL, wxID_VGDFLOWCHARTCONNECTORGCBBCONNECT, 
 wxID_VGDFLOWCHARTCONNECTORGCBBDEL, wxID_VGDFLOWCHARTCONNECTORGCBBOK, 
 wxID_VGDFLOWCHARTCONNECTORGCBBSET, wxID_VGDFLOWCHARTCONNECTORLBLINFO, 
 wxID_VGDFLOWCHARTCONNECTORLBLSYMBOLDESC, 
 wxID_VGDFLOWCHARTCONNECTORLBLSYMBOLID, wxID_VGDFLOWCHARTCONNECTORLBLTEXT, 
 wxID_VGDFLOWCHARTCONNECTORLSTFOLLOW, wxID_VGDFLOWCHARTCONNECTORTXTCONNECT, 
 wxID_VGDFLOWCHARTCONNECTORTXTINFO, wxID_VGDFLOWCHARTCONNECTORTXTNAME, 
 wxID_VGDFLOWCHARTCONNECTORTXTSYMBOLDESC, 
 wxID_VGDFLOWCHARTCONNECTORTXTSYMBOLID, wxID_VGDFLOWCHARTCONNECTORTXTTEXT, 
] = [wx.NewId() for _init_ctrls in range(20)]

class vgdFlowChartConnector(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VGDFLOWCHARTCONNECTOR,
              name=u'vgdFlowChartConnector', parent=prnt, pos=wx.Point(320, 89),
              size=wx.Size(440, 414), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'Flow Chart Connector')
        self.SetClientSize(wx.Size(432, 387))

        self.txtInfo = wx.TextCtrl(id=wxID_VGDFLOWCHARTCONNECTORTXTINFO,
              name=u'txtInfo', parent=self, pos=wx.Point(48, 44),
              size=wx.Size(128, 44), style=wx.TE_MULTILINE, value=u'')

        self.lblInfo = wx.StaticText(id=wxID_VGDFLOWCHARTCONNECTORLBLINFO,
              label=u'Info', name=u'lblInfo', parent=self, pos=wx.Point(24, 48),
              size=wx.Size(18, 13), style=0)

        self.lstFollow = wx.ListView(id=wxID_VGDFLOWCHARTCONNECTORLSTFOLLOW,
              name=u'lstFollow', parent=self, pos=wx.Point(8, 232),
              size=wx.Size(336, 112), style=wx.LC_REPORT)
        self.lstFollow.Bind(wx.EVT_LIST_ITEM_SELECTED,
              self.OnLstFollowListItemSelected,
              id=wxID_VGDFLOWCHARTCONNECTORLSTFOLLOW)

        self.gcbbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTCONNECTORGCBBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Delete', name=u'gcbbDel',
              parent=self, pos=wx.Point(352, 238), size=wx.Size(76, 30),
              style=0)
        self.gcbbDel.Bind(wx.EVT_BUTTON, self.OnGcbbDelButton,
              id=wxID_VGDFLOWCHARTCONNECTORGCBBDEL)

        self.lblSymbolID = wx.StaticText(id=wxID_VGDFLOWCHARTCONNECTORLBLSYMBOLID,
              label=u'Identifier', name=u'lblSymbolID', parent=self,
              pos=wx.Point(8, 8), size=wx.Size(40, 13), style=0)

        self.txtSymbolID = wx.TextCtrl(id=wxID_VGDFLOWCHARTCONNECTORTXTSYMBOLID,
              name=u'txtSymbolID', parent=self, pos=wx.Point(56, 8),
              size=wx.Size(100, 21), style=0, value=u'')

        self.lblSymbolDesc = wx.StaticText(id=wxID_VGDFLOWCHARTCONNECTORLBLSYMBOLDESC,
              label=u'Description', name=u'lblSymbolDesc', parent=self,
              pos=wx.Point(160, 8), size=wx.Size(53, 13), style=0)

        self.txtSymbolDesc = wx.TextCtrl(id=wxID_VGDFLOWCHARTCONNECTORTXTSYMBOLDESC,
              name=u'txtSymbolDesc', parent=self, pos=wx.Point(216, 8),
              size=wx.Size(100, 21), style=0, value=u'')

        self.gcbbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTCONNECTORGCBBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'gcbbOk',
              parent=self, pos=wx.Point(112, 352), size=wx.Size(76, 30),
              style=0)
        self.gcbbOk.Bind(wx.EVT_BUTTON, self.OnGcbbOkButton,
              id=wxID_VGDFLOWCHARTCONNECTORGCBBOK)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTCONNECTORGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(224, 352),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VGDFLOWCHARTCONNECTORGCBBCANCEL)

        self.txtText = wx.TextCtrl(id=wxID_VGDFLOWCHARTCONNECTORTXTTEXT,
              name=u'txtText', parent=self, pos=wx.Point(48, 96),
              size=wx.Size(296, 120), style=wx.TE_MULTILINE, value=u'')
        self.txtText.Bind(wx.EVT_CHAR, self.OnTxtTextChar)

        self.lblText = wx.StaticText(id=wxID_VGDFLOWCHARTCONNECTORLBLTEXT,
              label=u'Text', name=u'lblText', parent=self, pos=wx.Point(16, 96),
              size=wx.Size(21, 13), style=0)

        self.txtName = wx.TextCtrl(id=wxID_VGDFLOWCHARTCONNECTORTXTNAME,
              name=u'txtName', parent=self, pos=wx.Point(328, 8),
              size=wx.Size(100, 21), style=0, value=u'')
        self.txtName.Enable(False)

        self.chcMultiple = wx.CheckBox(id=wxID_VGDFLOWCHARTCONNECTORCHCMULTIPLE,
              label=u'Multiple', name=u'chcMultiple', parent=self,
              pos=wx.Point(352, 328), size=wx.Size(73, 13), style=0)
        self.chcMultiple.SetValue(False)
        self.chcMultiple.Bind(wx.EVT_CHECKBOX, self.OnChcMultipleCheckbox,
              id=wxID_VGDFLOWCHARTCONNECTORCHCMULTIPLE)

        self.gcbbSet = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTCONNECTORGCBBSET,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Set', name=u'gcbbSet',
              parent=self, pos=wx.Point(352, 288), size=wx.Size(76, 30),
              style=0)
        self.gcbbSet.Bind(wx.EVT_BUTTON, self.OnGcbbSetButton,
              id=wxID_VGDFLOWCHARTCONNECTORGCBBSET)

        self.gcbbConnect = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTCONNECTORGCBBCONNECT,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Connect',
              name=u'gcbbConnect', parent=self, pos=wx.Point(352, 60),
              size=wx.Size(76, 30), style=0)
        self.gcbbConnect.Bind(wx.EVT_BUTTON, self.OnGcbbConnectButton,
              id=wxID_VGDFLOWCHARTCONNECTORGCBBCONNECT)

        self.chcParallel = wx.CheckBox(id=wxID_VGDFLOWCHARTCONNECTORCHCPARALLEL,
              label=u'Parallel', name=u'chcParallel', parent=self,
              pos=wx.Point(216, 40), size=wx.Size(73, 13), style=0)
        self.chcParallel.SetValue(False)
        self.chcParallel.Bind(wx.EVT_CHECKBOX, self.OnChcParallelCheckbox,
              id=wxID_VGDFLOWCHARTCONNECTORCHCPARALLEL)

        self.gcbbBrowse = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VGDFLOWCHARTCONNECTORGCBBBROWSE,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Browse',
              name=u'gcbbBrowse', parent=self, pos=wx.Point(352, 96),
              size=wx.Size(76, 30), style=0)
        self.gcbbBrowse.Bind(wx.EVT_BUTTON, self.OnGcbbBrowseButton,
              id=wxID_VGDFLOWCHARTCONNECTORGCBBBROWSE)

        self.txtConnect = wx.TextCtrl(id=wxID_VGDFLOWCHARTCONNECTORTXTCONNECT,
              name=u'txtConnect', parent=self, pos=wx.Point(192, 64),
              size=wx.Size(152, 21), style=wx.TE_READONLY, value=u'')

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        self.dlgBrowse=None
        self.dlgBrowseVal=None
        self.IDname=''
        self.IDval=''
        self.IDtext=[]
        
        img=images.getOkBitmap()
        self.gcbbOk.SetBitmapLabel(img)
        self.gcbbSet.SetBitmapLabel(img)
        
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
        
        img=images.getDelBitmap()
        self.gcbbDel.SetBitmapLabel(img)
        
        img=images.getLinkBitmap()
        self.gcbbConnect.SetBitmapLabel(img)
        
        img=images.getBrowseBitmap()
        self.gcbbBrowse.SetBitmapLabel(img)
        
        self.lstFollow.InsertColumn(0,u'Nr',wx.LIST_FORMAT_LEFT,30)
        self.lstFollow.InsertColumn(1,u'ID',wx.LIST_FORMAT_LEFT,80)
        self.lstFollow.InsertColumn(2,u'desc',wx.LIST_FORMAT_LEFT,80)
        self.lstFollow.InsertColumn(3,u'info',wx.LIST_FORMAT_LEFT,120)
        
    def __clear__(self):
        self.txtSymbolID.SetValue('')
        self.txtSymbolDesc.SetValue('')
        self.txtInfo.SetValue('')
        self.txtVal.SetValue('')
        self.txtName.SetValue('')
        self.txtText.SetValue('')
        self.IDtext=[]
    def SetBrowseDlg(self,dlg):
        self.dlgBrowse=dlg
    def SetInfos(self,voglFlowChartElement,voglFlowChart):
        #self.spnStepNr.SetValue(voglSFCstep.nr)
        self.voglFlowChart=voglFlowChart
        self.voglFlowChartElem=voglFlowChartElement
        self.txtSymbolID.SetValue(self.voglFlowChartElem.id)
        self.txtSymbolDesc.SetValue(self.voglFlowChartElem.desc)
        self.txtInfo.SetValue(self.voglFlowChartElem.info)
        #fix me
        #replace text
        self.IDconnect=u''
        self.connect=u''
        if self.dlgBrowse is not None:
            self.dlgBrowse.CheckNode2Set()
            self.IDtext=string.split(self.voglFlowChartElem.IDtext,u',')
            i,j=0,0
            s=self.voglFlowChartElem.text
            for id in self.IDtext:
                i=string.find(s,'$',j)
                if i>=0:
                    j=string.find(s,'$',i+1)
                    if j>0:
                        diff=j-i-1
                        sID=self.dlgBrowse.GetLinkInfoById(id)[1]
                        s=s[:i+1]+sID+s[j:]
                        j=j+1-diff+len(sID)
            self.voglFlowChartElem.text=s
            try:
                self.IDconnect=self.voglFlowChartElem.IDconnect
                self.connect=self.dlgBrowse.GetLinkInfoById(self.IDconnect)[1]
                self.voglFlowChartElem.connect=self.connect
            except:
                self.IDconnect=u''
                self.connect=u''
                pass
        self.txtText.SetValue(self.voglFlowChartElem.text)
        self.txtName.SetValue(self.voglFlowChartElem.getXmlType())
        self.txtConnect.SetValue(self.connect)
        self.__updateFollowing__()
    def __updateFollowing__(self):
        self.selFollow=-1
        self.lstFollow.DeleteAllItems()
        i=0
        for s in self.voglFlowChartElem.nextElem:
            index = self.lstFollow.InsertImageStringItem(sys.maxint, "%d"%i, -1)
            self.lstFollow.SetStringItem(index,1,s[0].id,-1)
            self.lstFollow.SetStringItem(index,2,s[0].desc,-1)
            self.lstFollow.SetStringItem(index,3,s[0].info,-1)
            self.lstFollow.SetItemData(index,i)
            i+=1
            pass
    def OnGcbbOkButton(self, event):
        #self.voglSFCstep.nr=self.txtNr.GetValue()
        self.voglFlowChartElem.id=self.txtSymbolID.GetValue()
        self.voglFlowChartElem.desc=self.txtSymbolDesc.GetValue()
        self.voglFlowChartElem.info=self.txtInfo.GetValue()
        self.voglFlowChartElem.text=self.txtText.GetValue()
        self.voglFlowChartElem.IDtext=string.join(self.IDtext,u',')
        self.voglFlowChartElem.connect=self.connect
        self.voglFlowChartElem.IDconnect=self.IDconnect
        self.EndModal(1)
        event.Skip()
    
    def OnLstFollowListItemSelected(self, event):
        self.selFollow=event.GetIndex()
        if self.voglFlowChartElem.nextElem[self.selFollow][2][0]['alternative']=='1':
            bVal=True
        else:
            bVal=False
        self.chcMultiple.SetValue(bVal)
        #self.chcJoin.SetValue(self.voglFlowChartElem.nextElem[self.selFollow][0].bJoin)
        event.Skip()
    def OnGcbbDelButton(self, event):
        if self.selFollow<0:
            return
        self.voglFlowChartElem.RemoveLink(self.selFollow)
        self.lstFollow.DeleteItem(self.selFollow)
        self.selFollow=-1
        #self.__updateTrans__()
        event.Skip()
    
    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()

    def OnChcMultipleCheckbox(self, event):
        event.Skip()

    def OnGcbbSetButton(self, event):
        if self.selFollow<0:
            return
        if self.chcMultiple.GetValue()==True:
            sVal='1'
        else:
            sVal='0'
        self.voglFlowChartElem.nextElem[self.selFollow][2][0]['alternative']=sVal
        #self.voglFlowChartElem.nextElem[self.selFollow][0].bJoin=self.chcJoin.GetValue()
        event.Skip()

    def OnGcbbConnectButton(self, event):
        if self.dlgBrowse is not None:
            self.dlgBrowse.Centre()
            if self.dlgBrowse.ShowModal()>0:
                id,s=self.dlgBrowse.GetLinkInfo()
                #idxTup=self.__calcIdx__()
                #idx=idxTup[0]
                #if idxTup[1]!=idxTup[2]:
                #    self.IDtext.remove(self.IDtext[idx])
                #sOld=self.txtText.GetValue()
                sNew=u'$'+s+u'$'
                self.IDconnect=id
                self.connect=sNew
                self.txtConnect.SetValue(sNew)
        event.Skip()

    def OnChcParallelCheckbox(self, event):
        event.Skip()

    def OnChcJoinCheckbox(self, event):
        event.Skip()
    def __replace__(self,strOld,strNew):
        i=string.find(strOld,'<')
        if i>=0:
            j=string.find(strOld,'>',i+1)
            if j>0:
                return strOld[:i+1]+strNew+strOld[j:]
        return '<'+strNew+'>'
    def __calcIdx__(self):
        s=self.txtText.GetValue()
        iLen=len(s)
        idx=0
        iPos=self.txtText.GetInsertionPoint()
        tup=self.txtText.PositionToXY(iPos)
        strs=string.split(s,'\n')
        iPos=0
        for i in range(0,tup[1]):
            iPos+=len(strs[i])+1
        iPos+=tup[0]
        i,j=0,0
        while i>=0:
            i=string.find(s,u'$',j)
            if i>=0:
                if i>=iPos:
                    return (idx,iPos,iPos)
                j=string.find(s,'$',i+1)
                if j>0:
                    if j<iPos:
                        idx+=1
                        j+=1
                    else:
                        return (idx,i,j+1)
                i=j
        return (idx,iPos,iPos)
    def OnGcbbBrowseButton(self, event):
        if self.dlgBrowse is not None:
            self.dlgBrowse.Centre()
            if self.dlgBrowse.ShowModal()>0:
                id,s=self.dlgBrowse.GetLinkInfo()
                idxTup=self.__calcIdx__()
                idx=idxTup[0]
                if idxTup[1]!=idxTup[2]:
                    self.IDtext.remove(self.IDtext[idx])
                self.IDtext.insert(idx,id)
                sOld=self.txtText.GetValue()
                sNew=sOld[:idxTup[1]]+u'$'+s+u'$'+sOld[idxTup[2]:]
                self.txtText.SetValue(sNew)
        event.Skip()
    def OnTxtTextChar(self, event):
        if event.GetKeyCode() in [8,127]:
            s=self.txtText.GetValue()
            iLen=len(s)
            idx=0
            iPos=self.txtText.GetInsertionPoint()
            tup=self.txtText.PositionToXY(iPos)
            strs=string.split(s,'\n')
            iPos=0
            for i in range(0,tup[1]):
                iPos+=len(strs[i])+1
            iPos+=tup[0]
            i,j=0,0
            lstIDs=[]
            while i>=0:
                i=string.find(s,u'$',j)
                if i>=0:
                    j=string.find(s,'$',i+1)
                    if j>0:
                        if j<iPos:
                            idx+=1
                            j+=1
                        else:
                            lstIDs.append((idx,i,j+1))
                    i=j
            idx=0
            for tup in lstIDs:
                iStart=tup[1]
                if event.GetKeyCode()==8:
                    iStart+=1
                if iPos>=iStart and iPos<=tup[2]:
                    self.IDtext.remove(self.IDtext[idx])
                    sOld=self.txtText.GetValue()
                    sNew=sOld[:tup[1]]+sOld[tup[2]:]
                    self.txtText.SetValue(sNew)
                    return
                idx+=1
        event.Skip()

#----------------------------------------------------------------------

class voglFlowChartConnector(voglFlowChartElement):
    def __init__(self, canvas,id,desc,info,w=0.0, h=0.0):
        voglFlowChartElement.__init__(self, canvas, id,desc,info, w, h)
        
        self.IDconnect=u''
        self.connect=u''
        
        dw=w/6.0
        dh=h/4.0
        
        obj= ogl.CircleShape(dw*2.0)
        obj.SetBrush(TRANSPARENT_BRUSH)
        obj.SetPen(BLACK_PEN)
        obj.SetDraggable(False)
        obj.SetSensitivityFilter(0,True)
        self.AddChild(obj)
        
        self.__addSymbol__()
        self.SetSensitivityFilter(0)
        self.Recompute()
        self.SetDraggable(True)
    def SetConnect(self,val):
        self.connect=val
    def SetIDConnect(self,val):
        self.IDconnect=val
    def DoEdit(self):
        if self.canvas.dlgFlowChartConnector is None:
            self.canvas.dlgFlowChartConnector=vgdFlowChartConnector(self.canvas)
        self.canvas.dlgFlowChartConnector.Centre()
        self.canvas.dlgFlowChartConnector.SetBrowseDlg(self.canvas.GetBrowseDlg())
        self.canvas.dlgFlowChartConnector.SetInfos(self,self.canvas)
        if self.canvas.dlgFlowChartConnector.ShowModal()>0:
            self.canvas.__setModified__(True)
            canvas = self.canvas
            dc = wx.ClientDC(canvas)
            canvas.PrepareDC(dc)
        
            self.UpdateLinksRec(dc)
            
            return 1
        return 0
    def UpdateReference(self):
        voglFlowChartElement.UpdateReference(self)
        try:
            self.connect=self.GetLinkInfoById(self.IDconnect)[1]
        except:
            self.IDconnect=u''
            self.connect=u''
            pass
    def getXmlType(self):
        return 'conn'
    def appendLatexPic(self,strs,yMax,scaleX,scaleY):
        x=self.symbol.GetX()/scaleX
        y=self.symbol.GetY()/scaleY
        ba=self.symbol.GetBoundingBoxMax()
        xa=x#-(ba[0]/2.0/scaleX)
        ya=yMax-y
        
        tup=(xa,ya,self.w/6.0*2.0)
        strs.append('    \\put(%4.1f,%4.1f){\\circle{%4.1f}}'%tup)
        voglFlowChartElement.appendLatexPic(self,strs,yMax,scaleX,scaleY)
        return xa,ya
    def isData(self):
        return True
    def isProg(self):
        return True
    def isSys(self):
        return True
    def isNet(self):
        return True
    def isRes(self):
        return True

