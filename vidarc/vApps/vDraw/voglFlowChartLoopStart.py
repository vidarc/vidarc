#Boa:Dialog:vgdFlowChartData

from wx import WHITE_BRUSH
from wx import TRANSPARENT_BRUSH
from wx import BLACK_PEN
import wx.lib.ogl as ogl
import string,os,sys
import vidarc.vApps.vDraw.latex as latex
from vidarc.vApps.vDraw.voglFlowChartElement import voglFlowChartElement
import images

#----------------------------------------------------------------------

class voglFlowChartLoopStart(voglFlowChartElement):
    def __init__(self, canvas,id,desc,info,w=0.0, h=0.0):
        voglFlowChartElement.__init__(self, canvas, id,desc,info, w, h)
        
        dw=w/6.0
        dh=h/4.0
        
        obj=ogl.PolygonShape()
        obj.SetBrush(TRANSPARENT_BRUSH)
        dw=w/6.0
        dh=h/4.0
        points = [ (-dw*3.0,     0.0),
                   (-dw*2.0, -dh*2.0),
                   (+dw*2.0, -dh*2.0),
                   (+dw*3.0,     0.0),
                   (+dw*3.0, +dh*2.0),
                   (-dw*3.0, +dh*2.0)
                   ]

        obj.Create(points)
        obj.SetDraggable(False)
        obj.SetSensitivityFilter(0,True)
        self.AddChild(obj)
        
        self.__addSymbol__()
        self.SetSensitivityFilter(0)
        self.Recompute()
        self.SetDraggable(True)
    def getXmlType(self):
        return 'loopstart'
    def appendLatexPic(self,strs,yMax,scaleX,scaleY):
        x=self.symbol.GetX()/scaleX
        y=self.symbol.GetY()/scaleY
        ba=self.symbol.GetBoundingBoxMax()
        xa=x-(ba[0]/2.0/scaleX)
        ya=y-(ba[1]/2.0/scaleY)
        #tup=(xa,yMax-(y+(ba[1]/2.0/scaleY)),ba[0]/scaleX,ba[1]/scaleY,self.info)
        #strs.append('    \\put(%4.1f,%4.1f){\\makebox(%4.1f,%4.1f){{\\footnotesize %s}}}'%tup)
        xh1,yh1=xa,yMax-(y+(ba[1]/2.0/scaleY))
        xh2,yh2=xa+(self.w/6.0/scaleX),yMax-(y-(ba[1]/2.0/scaleY))
        xh3,yh3=xa+(self.w/6.0*5.0/scaleX),yh2-(self.h/4.0*2.0/scaleY)
        xh4=xa+(self.w/scaleX)
        
        tup=latex.lineFit(xh1,yh3,xh1,yh1)
        strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
        tup=latex.lineFit(xh1,yh1,xh4,yh1)
        strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
        tup=latex.lineFit(xh4,yh1,xh4,yh3)
        strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
        tup=latex.lineFit(xh4,yh3,xh3,yh2)
        strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
        tup=latex.lineFit(xh2,yh2,xh3,yh2)
        strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
        tup=latex.lineFit(xh1,yh3,xh2,yh2)
        strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
        voglFlowChartElement.appendLatexPic(self,strs,yMax,scaleX,scaleY)
        return xa,ya
    def isProg(self):
        return True
    def isSys(self):
        return True
    