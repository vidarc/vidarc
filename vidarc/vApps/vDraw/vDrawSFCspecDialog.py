#Boa:Dialog:vDrawSFCspecDialog
#----------------------------------------------------------------------------
# Name:         vSFCspecDialog.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051212
# CVS-ID:       $Id: vDrawSFCspecDialog.py,v 1.1 2005/12/13 13:19:34 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import  wx.lib.anchors as anchors

import images

from vidarc.vApps.vDraw.vDrawSFCCanvasOGL import vDrawSFCCanvasOGL

def create(parent):
    return vDrawSFCspecDialog(parent)

[wxID_VDRAWSFCSPECDIALOG, wxID_VDRAWSFCSPECDIALOGGCBBCANCEL, 
 wxID_VDRAWSFCSPECDIALOGGCBBOK, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vDrawSFCspecDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VDRAWSFCSPECDIALOG,
              name=u'vDrawSFCspecDialog', parent=prnt, pos=wx.Point(158, 15),
              size=wx.Size(642, 535), style=wx.DEFAULT_DIALOG_STYLE,
              title=u'Dialog SFC specification')
        self.SetClientSize(wx.Size(634, 508))

        self.gcbbOk = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VDRAWSFCSPECDIALOGGCBBOK,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Ok', name=u'gcbbOk',
              parent=self, pos=wx.Point(184, 472), size=wx.Size(76, 30),
              style=0)
        self.gcbbOk.Bind(wx.EVT_BUTTON, self.OnGcbbOkButton,
              id=wxID_VDRAWSFCSPECDIALOGGCBBOK)

        self.gcbbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VDRAWSFCSPECDIALOGGCBBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=u'Cancel',
              name=u'gcbbCancel', parent=self, pos=wx.Point(320, 472),
              size=wx.Size(76, 30), style=0)
        self.gcbbCancel.Bind(wx.EVT_BUTTON, self.OnGcbbCancelButton,
              id=wxID_VDRAWSFCSPECDIALOGGCBBCANCEL)

    def __init__(self, parent):
        self._init_ctrls(parent)
        
        self.oglDraw=vDrawSFCCanvasOGL(parent=self,pos=wx.Point(0,0),
                            size=wx.Size(634,460))
        self.oglDraw.SetConstraints(
            anchors.LayoutAnchors(self.oglDraw, True, True, True, True)
            )
        
        img=images.getOkBitmap()
        self.gcbbOk.SetBitmapLabel(img)
        img=images.getCancelBitmap()
        self.gcbbCancel.SetBitmapLabel(img)
    def SetDoc(self,doc,bNet):
        self.oglDraw.SetDoc(doc,bNet)
    def SetNode(self,node):
        self.oglDraw.SetNode(node)
        pass
    def GetNode(self,node):
        self.oglDraw.GetNode(node)
        pass
        
    def OnGcbbOkButton(self, event):
        self.EndModal(1)
        event.Skip()

    def OnGcbbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
