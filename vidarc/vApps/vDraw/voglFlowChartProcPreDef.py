#Boa:Dialog:vgdFlowChartData

from wx import WHITE_BRUSH
from wx import TRANSPARENT_BRUSH
from wx import BLACK_PEN
import wx.lib.ogl as ogl
import string,os,sys
import vidarc.vApps.vDraw.latex as latex
from vidarc.vApps.vDraw.voglFlowChartElement import voglFlowChartElement
import images

#----------------------------------------------------------------------

class voglFlowChartProcPredefined(voglFlowChartElement):
    def __init__(self, canvas,id,desc,info,w=0.0, h=0.0):
        voglFlowChartElement.__init__(self, canvas, id,desc,info, w, h)
        
        dw=w/6.0
        dh=h/4.0
        
        obj= ogl.DrawnShape()
        obj.SetDrawnPen(BLACK_PEN)
        xa=-dw*3.0
        xe=+dw*3.0
        ya=-dh*2.0
        ye=+dh*2.0
        obj.DrawLine((xa, ya), (xe, ya))
        obj.DrawLine((xa, ya), (xa, ye))
        obj.DrawLine((xa, ye), (xe, ye))
        obj.DrawLine((xe, ya), (xe, ye))
        xa=-dw*2.5
        obj.DrawLine((xa, ya), (xa, ye))
        xa=+dw*2.5
        obj.DrawLine((xa, ya), (xa, ye))
        obj.SetDraggable(False)
        obj.SetSensitivityFilter(0,True)
        self.AddChild(obj)
        
        self.__addSymbol__()
        self.SetSensitivityFilter(0)
        self.Recompute()
        self.SetDraggable(True)
    def getXmlType(self):
        return 'procpredef'
    def appendLatexPic(self,strs,yMax,scaleX,scaleY):
        x=self.symbol.GetX()/scaleX
        y=self.symbol.GetY()/scaleY
        ba=self.symbol.GetBoundingBoxMax()
        xa=x-(ba[0]/2.0/scaleX)
        ya=y-(ba[1]/2.0/scaleY)
        #tup=(xa,yMax-(y+(ba[1]/2.0/scaleY)),ba[0]/scaleX,ba[1]/scaleY,self.info)
        #strs.append('    \\put(%4.1f,%4.1f){\\makebox(%4.1f,%4.1f){{\\footnotesize %s}}}'%tup)
        xh1,yh1=xa,yMax-(y+(ba[1]/2.0/scaleY))
        xh2,yh2=xa+(self.w/scaleX),yh1+(self.h/scaleY)
        
        tup=latex.lineFit(xh1,yh1,xh1,yh2)
        strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
        tup=latex.lineFit(xh1+(self.w/12.0/scaleX),yh1,xh1+(self.w/12.0/scaleX),yh2)
        strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
        tup=latex.lineFit(xh1,yh2,xh2,yh2)
        strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
        tup=latex.lineFit(xh2,yh2,xh2,yh1)
        strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
        tup=latex.lineFit(xh2-(self.w/12.0/scaleX),yh2,xh2-(self.w/12.0/scaleX),yh1)
        strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
        tup=latex.lineFit(xh1,yh1,xh2,yh1)
        strs.append('    \\put(%4.1f,%4.1f){\\line(%d,%d){%4.1f}}'%tup)
        voglFlowChartElement.appendLatexPic(self,strs,yMax,scaleX,scaleY)
        return xa,ya
    def isProg(self):
        return True
    def isSys(self):
        return True
    def isNet(self):
        return True
    